


// ----------------------------------------------------- \\
// ----------------------------------------------------- \\
//                                                       \\
//                                                       \\
// FM_Horde_Controler.sc                                 \\
//                                                       \\
// ----------------------------------------------------- \\
//                                                       \\
// DESCRIPTION:                                          \\
//                                                       \\
// Runs horde mode.                                      \\
//                                                       \\
// Players must fight waves of enemies of increasing     \\
// difficulty.                                           \\
//                                                       \\
// Uses mission creator data.                            \\
//                                                       \\
// ----------------------------------------------------- \\
//                                                       \\
// AUTHOR:                                               \\
//                                                       \\
// William.Kennedy@RockstarNorth.com                     \\
//                                                    	 \\
// ----------------------------------------------------- \\
// ----------------------------------------------------- \\


 
USING "globals.sch"
//USING "net_lobby.sch"
USING "shared_hud_displays.sch"
USING "net_spawn.sch"
USING "net_objective_text.sch"
USING "freemode_header.sch"
USING "FMMC_HEADER.sch"
USING "screen_gang_on_mission.sch"
USING "leader_board_common.sch"
USING "net_spectator_cam.sch"
USING "weapons_public.sch"
USING "leader_board_common.sch"
USING "socialclub_leaderboard.sch"
USING "FMMC_Restart_Header.sch"
USING "FM_Playlist_Header.sch"
USING "net_leaderboards.sch"
USING "social_feed_controller.sch"
USING "net_script_tunables.sch"
//USING "net_save_create_eom_vehicle.sch"
USING "FMMC_next_job_screen.sch"
USING "net_celebration_screen.sch"
USING "net_wait_zero.sch"
USING "net_cash_transactions.sch"

#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF



// *********************************** \\
// *********************************** \\
//                                     \\
// 			 LOCAL VARIABLES           \\
//                                     \\
// *********************************** \\
// *********************************** \\

// Game States
CONST_INT SURVIVAL_GAME_STATE_INIT                                     	0
CONST_INT SURVIVAL_GAME_STATE_INIT_SPAWN                      			1
CONST_INT SURVIVAL_GAME_FADE_IN 										2
CONST_INT SURVIVAL_GAME_TEAMCUT_CUTSCENE								3
CONST_INT SURVIVAL_GAME_INTRO_CUTSCENE									4
CONST_INT SURVIVAL_GAME_INTRO_CAMERA_BLEND								5
CONST_INT SURVIVAL_GAME_STATE_RUNNING                             		6
CONST_INT SURVIVAL_GAME_STATE_WAITING_TO_LEAVE                          7
CONST_INT SURVIVAL_GAME_STATE_LEAVE                                 	8
CONST_INT SURVIVAL_GAME_STATE_END                                    	9

CONST_INT NUM_SURVIVAL_SUB_STAGES										3
CONST_INT NUMBER_HORDE_WAVES                                         	10
CONST_INT DELAY_BETWEEN_WAVES                                          	20000
CONST_INT GET_BACK_TO_HORDE_TIME_LIMIT                                	30000
CONST_INT TAKING_PART_XP                                              	100
CONST_FLOAT AT_HORDE_DISTANCE                                          	200.0
CONST_INT NUM_LAST_ENEMIES_TO_BLIP                                     	4
CONST_INT MAX_NUM_SURVIVAL_HELIS                                       	3
CONST_INT NUM_HELI_PEDS													3
CONST_INT SURVIVAL_HELI_0_START_CIRC_COUNT                             	0
CONST_INT SURVIVAL_HELI_1_START_CIRC_COUNT                              4
CONST_INT SURVIVAL_HELI_2_START_CIRC_COUNT                             	8
CONST_INT MAX_NUM_SQUAD_PEDS                                           	28
CONST_INT MAX_NUM_ENEMY_SQUADS											4*NUM_SURVIVAL_SUB_STAGES
CONST_INT MAX_NUM_PEDS_IN_SQUAD											7
CONST_INT NUM_LAND_VEH_PEDS												4
CONST_INT MAX_NUM_LAND_VEHICLES                                       	4
CONST_INT END_OF_WAVE_XP_MULTIPLIER										50

CONST_INT MAX_NUM_ENEMIES_SUBSTAGE_EASY									12
CONST_INT MAX_NUM_ENEMIES_SUBSTAGE_MEDIUM								24
CONST_INT MAX_NUM_ENEMIES_SUBSTAGE_HARD									28

CONST_INT BITSET_SERVER_CURRENT_WAVE_DEFEATED                           0
CONST_INT BITSET_SERVER_DID_FIRST_WAVE_SETUP                            1
CONST_INT BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM     		2
CONST_INT BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN                       3

CONST_INT BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK                      0
CONST_INT BITSET_PLAYER_READY_FOR_START_FADE_IN                         1
CONST_INT BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD        		2
CONST_INT BITSET_PLAYER_COMPLETED_START_FADE_IN                         3
CONST_INT BITSET_PLAYER_ABANDONED_HORDE                               	4
CONST_INT BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM                     5
CONST_INT BITSET_PLAYER_I_AM_ALIVE_AND_FADED_IN                         6
CONST_INT BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP            	7
CONST_INT BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS				8

//TWEAK_FLOAT SDPAD_X 							0.18
//TWEAK_FLOAT	SDPAD_Y 							0.377
//TWEAK_FLOAT SDPAD_H 							0.6
//TWEAK_FLOAT SDPAD_W 							0.28

SCRIPT_TIMER tdNetTimer  				//  The network timer
SCRIPT_TIMER stLeaveHordeTimer
SCRIPT_TIMER stTaxtTerminateFailsafe
SCRIPT_TIMER stJipTimer
SCRIPT_TIMER stEndOfWaveDelay
SCRIPT_TIMER stSurvivalTime

LBD_VARS lbdVars
//DPAD_VARS dpadVars
SAVE_OUT_UGC_PLAYER_DATA_VARS sSaveOutVars 
INT iLBPlayerSelection = -1
TIME_DATATYPE timeScrollDelay 

JOB_INTRO_CUT_DATA jobIntroData

BOOL bStartFadeInTriggered
BOOL bAllPlayersLostAllLivesFlag
BOOL bClearedObjectiveTextForRewards
BOOL bGotWaveProgressLabel
BOOL bGivenEndModeRewards
BOOL bGivenEndOfWaveXp
BOOL bGivenTakingPartXp
BOOL bIamQuittingCoronaSpectator
BOOL bDealtWithWasted
BOOL bModeEndedSpecCamTranisition
BOOL bSkipTaxiScriptCheck
BOOL bIWentFromSpectatingToPassedWaveStage
BOOL bTurnedOffSpecCam
BOOL bDoWaveCompleteHudPrints
BOOL bProcessedMostSurvivalKillsAward
BOOL bSavedMatchstartPlayStatsData
//BOOL bHaveFriendToSendTo
BOOL bGotFirstTimeForHoldValue
BOOL bBeenInPauseMenuTooLongOnWavePassed
BOOL bDisplayedNoVehsHelp
BOOL bAllAwardsBeenShown
BOOL bDidMatchEnd
BOOL bSetupInitialSpawnPos
BOOL bResetReminderTimer
BOOL bFinalCelebrationScreenHasDisplayed
BOOL bSetupSpecificSpawnCoords
BOOL bMutedNearbyRadios
BOOL bContinueToCut
BOOL bAlreadyTriggeredCelebrationPostFX
BOOL bPrintPartListInfo
BOOL bSetMissionOverAndOnLbForSctv
BOOL bSavedOutPlayerUgcData
BOOL bSetSctvTickerOn
BOOL bTurnedOffRoadNodes
BOOL bDoneMatchEnd
BOOL bDoUnhide

INT iStaggeredPickUpBlipCount
INT iStaggeredAlldidEndOfWaveHudCount
INT iRespawningStage
INT iTrackDeathsStage
INT iCountDownAudioStage
INT iWarpToStartPosStage
INT iCreatePickupsStage
INT iEndOfWaveHudStage
//INT iSpecCamRegSquad //removed for 1623032
//INT iSpecCamRegPedCount//removed for 1623032
//INT iRegisteredEnemyWithSpecCamBitset[MAX_NUM_ENEMY_SQUADS] //removed for 1623032
//INT iFightingPartListCount
INT iTimeInPauseMenu
INT iPopMultArea
INT iProcessPickupBlipsStage
INT iWeaponPickupCount
INT iFeedbackStage
INT iTotalWaveAwardXp
INT iStartHits
INT iStartShots
INT iPlayerAbandondedSurvivalTickerBit
INT iEndCelebrationScreenScore
INT iRejoinFromSpecCamStage
INT iPlayerIsNetOkBitset
INT iParticpantIsDeadBitset

FLOAT fFurthestTargetDist = 50.0
FLOAT fOldFurthestTargetDist = 50.0
FLOAT fPartDis
FLOAT fHeightMapHeight = -1.0
	
#IF IS_DEBUG_BUILD
INT iStaggeredPartCountDebug
#ENDIF

TIME_DATATYPE iTimeForHold

TEXT_LABEL_15 tl15_waveProgressLabel

AI_BLIP_STRUCT structSquadEnemyBlip[MAX_NUM_ENEMY_SQUADS][MAX_NUM_PEDS_IN_SQUAD]
AI_BLIP_STRUCT structLandVehicleEnemyBlip[MAX_NUM_LAND_VEHICLES][NUM_LAND_VEH_PEDS]
BLIP_INDEX biPickupBlip[ciMAX_CREATED_WEAPONS]
BLIP_INDEX biShootOutBlip
BLIP_INDEX biHeliBlip[MAX_NUM_SURVIVAL_HELIS]
OBJECT_INDEX oihordeProp[FMMC_MAX_NUM_PROPS]
OBJECT_INDEX oihordePropChildren[FMMC_MAX_NUM_PROP_CHILDREN]
PICKUP_INDEX piPickup[ciMAX_CREATED_WEAPONS]

SCENARIO_BLOCKING_INDEX sbiScenarioBlockingIndex

SPECTATOR_DATA_STRUCT specData

//Celebration Screen
CELEBRATION_SCREEN_DATA sCelebrationData
CAMERA_INDEX camCelebrationScreen

SCRIPT_TIMER jobUnderTenSecondsTimer

ENUM eHORDE_STAGE
    eHORDESTAGE_JIP_IN = 0,
    eHORDESTAGE_SETUP_MISSION,
    eHORDESTAGE_START_FADE_IN,
    eHORDESTAGE_FIGHTING,
    eHORDESTAGE_PASSED_WAVE,
    eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE,
    eHORDESTAGE_SPECTATING,
    eHORDESTAGE_SCTV,
    eHORDESTAGE_REWARDS_AND_FEEDACK
ENDENUM

ENUM eEND_HORDE_REASON
    eENDHORDEREASON_NOT_FINISHED_YET = 0,
    eENDHORDEREASON_BEAT_ALL_WAVES,
    eENDHORDEREASON_ALL_PLAYERS_DEAD
ENDENUM

ENUM eENEMY_STATE
    eENEMYSTATE_DORMANT = 0,
    eENEMYSTATE_SPAWNING,
    eENEMYSTATE_FIGHTING,
    eENEMYSTATE_DEAD
ENDENUM

ENUM eKILL_STREAK_STATE
    eKILLSTREAKSTATE_NONE = 0,
    eKILLSTREAKSTATE_SMALL,
    eKILLSTREAKSTATE_MEDIUM,
    eKILLSTREAKSTATE_LARGE
ENDENUM

ENUM eWAVE_STAGE
    eWAVESTAGE_EASY = 0,
    eWAVESTAGE_MEDIUM,
    eWAVESTAGE_HARD
ENDENUM

ENUM eDELAY_BEFORE_NEXT_WAVE_STAGE
    eDELAYBEFORENEXTWAVESTAGE_ADD_NEW_FIGHTING_PARTICIPANTS = 0,
    eDELAYBEFORENEXTWAVESTAGE_WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN,
    eDELAYBEFORENEXTWAVESTAGE_DO_COUNT_DOWN
ENDENUM

ENUM eMINI_LEADER_BOARD_STATE
    eMINILEADERBOARDSTATE_OFF = 0,
    eMINILEADERBOARDSTATE_DO_FIRST_SORT,
    eMINILEADERBOARDSTATE_ON
ENDENUM

ENUM eSURVIVAL_HELI_STATE
    eSURVIVALHELISTATE_NOT_EXIST = 0,
    eSURVIVALHELISTATE_SPAWNING,
	eSURVIVALHELISTATE_FLYING_TO_SURVIVAL,
    eSURVIVALHELISTATE_FLYING_FIGHTING,
	eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP,
    eSURVIVALHELISTATE_DEAD
ENDENUM

ENUM ENUM_MUSIC_EVENT
	eMUSICEVENT_START = 0,
	eMUSICEVENT_WAVE_40P,
	eMUSICEVENT_WAVE_70P,
	eMUSICEVENT_WAVE_COMPLETE,
	eMUSICEVENT_SILENT,
	eMUSICEVENT_STOP
ENDENUM

CONST_INT NUM_MISSION_MUSIC_EVENTS COUNT_OF(ENUM_MUSIC_EVENT)

ENUM eSQUAD_STATE
	eSQUADSTATE_DORMANT = 0,
	eSQUADSTATE_ACTIVE
ENDENUM

ENUM eVEHICLE_PED_STATE
	eVEHICLEPEDSTATE_DORMANT = 0,
	eVEHICLEPEDSTATE_SPAWNING,
	eVEHICLEPEDSTATE_DRIVING_TO_SURVIVAL,
	eVEHICLEPEDSTATE_FIGHTING,
	eVEHICLEPEDSTATE_DEAD
ENDENUM

ENUM eSURVIVAL_LAND_VEHICLE_STATE
	eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST = 0,
	eSURVIVALLANDVEHICLESTATE_SPAWNING,
	eSURVIVALLANDVEHICLESTATE_DRIVING,
	eSURVIVALLANDVEHICLESTATE_ARRIVED,
	eSURVIVALLANDVEHICLESTATE_DEAD
ENDENUM

ENUM eMIGHT_NOT_BE_PLAYING_FLAGS
	eMIGHTNOTBEPLAYINGFLAGS_NOT_DAMAGED_ENEMY = 0,
	eMIGHTNOTBEPLAYINGFLAGS_IDLE
ENDENUM

STRUCT STRUCT_MUSIC_EVENT
	ENUM_MUSIC_EVENT eEvent
	TEXT_LABEL_31 tl31Name
	BOOL bTriggered
ENDSTRUCT

STRUCT STRUCT_SURVIVAL_VEHICLE_PED
    NETWORK_INDEX netId
    MODEL_NAMES eModel = MP_S_M_Armoured_01
	eVEHICLE_PED_STATE eState
ENDSTRUCT

STRUCT STRUCT_SURVIVAL_HELI_DATA
    NETWORK_INDEX netId
    MODEL_NAMES eModel = BUZZARD
    eSURVIVAL_HELI_STATE eState
    STRUCT_SURVIVAL_VEHICLE_PED sPed[NUM_HELI_PEDS]
    VECTOR vSpawnCoords
    FLOAT fHeading
    BOOL bActive
    INT iCircCount
    INT iActivatingStag
	SCRIPT_TIMER stBlownUpTimer
ENDSTRUCT

STRUCT STRUCT_SURVIVAL_LAND_VEHICLE_DATA
	NETWORK_INDEX netId
	MODEL_NAMES eModel = SANDKING2
	eSURVIVAL_LAND_VEHICLE_STATE eState
	STRUCT_SURVIVAL_VEHICLE_PED sPed[NUM_LAND_VEH_PEDS]
	VECTOR vSpawnCoords
    FLOAT fHeading
	INT iActivatingStage
	INT iNumPeds = -1
ENDSTRUCT

STRUCT STRUCT_SQUAD_ID_DATA
	INT iSquad = -1
	INT iSquadSlot
ENDSTRUCT

STRUCT STRUCT_ENEMY_PED_HORDE_DATA
	NETWORK_INDEX netId
    eENEMY_STATE eState = eENEMYSTATE_DORMANT
    STRUCT_SQUAD_ID_DATA sSquadIdData
	SCRIPT_TIMER stDefensiveSphereTimer
    INT iAccuracy = 10
	VECTOR vCurrentSpawnCoords
	BOOL bPlayedAngryShout
	INT iEnemyTrackKillsStage
ENDSTRUCT

// Squad struct. Contains data about a squad.
STRUCT STRUCT_AI_SQUAD
	INT iSquadId = -1 // Optional property. Can be used by scripters to give a squad a unique id.
	eSQUAD_STATE eState
	STRUCT_ENEMY_PED_HORDE_DATA sPed[MAX_NUM_PEDS_IN_SQUAD]
	INT iSquadSize = -1
ENDSTRUCT

STRUCT STRUCT_WAVE_DATA
    INT iWaveCount = 1
    eWAVE_STAGE eStage
    INT iNumRequiredKills
    INt iNumRequiredSubstageKills[3]
    INT iNumPedsInSquad[NUM_SURVIVAL_SUB_STAGES]
    INT iNumSquads[NUM_SURVIVAL_SUB_STAGES]
    INT iMaintainSquadsStage
ENDSTRUCT

STRUCT STRUCT_HORDE_PARTICIPANT_DATA
    INT iParticipant[MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS]
    INT iNumFightingParticipants
	int iNumPotentialfightingParticipants
	INT iNumFightingParticipantsBeginningOfWave
    INT iAddedBitset
	INT iPotentialFighterBitset
    BOOL bListBeenInitialised
ENDSTRUCT

STRUCT STRUCT_HORDE_BOARD_DATA
    BOOL bPopulatedHudPlayerArray
    PLAYER_INDEX hudPlayers[4]
    INT iScores[4]
	INT iTeam[4]
	INT iDeaths[4]
	INT iHeadshots[4]
ENDSTRUCT
STRUCT_HORDE_BOARD_DATA sEndBoardData

STRUCT STRUCT_KILL_STREAK_DATA
    eKILL_STREAK_STATE eState
    INT iNumKillsInARow
    INT iBigMessageBitset
ENDSTRUCT
STRUCT_KILL_STREAK_DATA structKillStreakData

STRUCT STRUCT_DELAY_BEFORE_NEXT_WAVE_DATA
    eDELAY_BEFORE_NEXT_WAVE_STAGE eStage
ENDSTRUCT

STRUCT STRUCT_MINI_LEADERBOARD_DATA
    eMINI_LEADER_BOARD_STATE eMiniLeaderBoardState
    LEADERBOARD_PLACEMENT_TOOLS sPlacement
    THE_LEADERBOARD_STRUCT miniLeaderBoardStruct[MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS]
    STRUCT_HORDE_BOARD_DATA sMiniLeaderBoardSortedData
	DPAD_VARS sDpadVars
	SCALEFORM_INDEX siMovie
ENDSTRUCT
STRUCT STRUCT_ALLOW_WASED_MESSAGE_DATA
    INT iWastedMessageStage
    SCRIPT_TIMER stWastedDisplayTime
ENDSTRUCT

STRUCT STRUCT_SURVIVAL_BOUNDS_DATA
	
	VECTOR vMin
	VECTOR vMax
	FLOAT fWidth
	
ENDSTRUCT

STRUCT_SURVIVAL_BOUNDS_DATA 			sSurvivalBoundsData
STRUCT_ALLOW_WASED_MESSAGE_DATA 		sAllowWastedMessageData
//PED_WEAPONS_MP_STRUCT 					structPlayerWeapons
STRUCT_MINI_LEADERBOARD_DATA 			eMiniLeaderBoardData
STRUCT_MUSIC_EVENT 						structMusicEvent[NUM_MISSION_MUSIC_EVENTS]
FMMC_EOM_DETAILS 						sEndOfMission
PLAYER_CURRENT_STATUS_COUNTS 			sPlayerCounts
SC_LEADERBOARD_CONTROL_STRUCT 			scLB_control//, scLB_controlFriendComp

SCALEFORM_INDEX							scLB_scaleformID
//UGCStateUpdate_Data						sUgcStatData

//===Start Edit David G===
THE_LEADERBOARD_STRUCT 				HordeLeaderboard[NUM_NETWORK_PLAYERS]
LEADERBOARD_PLACEMENT_TOOLS 			LeaderboardPlacement
TEXT_LABEL_63 							tl63LeaderboardPlayerNames[NUM_NETWORK_PLAYERS]
//===End Edit David G===
INT iSavedRow[NUM_NETWORK_PLAYERS]
INT iSavedSpectatorRow[NUM_NETWORK_PLAYERS]
INT iSpectatorBit

// Might not be playing vars.
SCRIPT_TIMER stIdleTimer
SCRIPT_TIMER stNotDamagedEnemyTimer
BOOL bIHaveDamagedAnEnemy
CONST_INT MIGHT_NOT_BE_PLAYING_TIMER_LIMIT 2*60*1000
INT iPrintedLeecherRemindeTickerForfightingParticipant



// *********************************** \\
// *********************************** \\
//                                     \\
//				SERVER BD              \\
//                                     \\
// *********************************** \\
// *********************************** \\

STRUCT ServerBroadcastData
    
    INT iServerGameState
   	
    SCRIPT_TIMER TerminationTimer
    SCRIPT_TIMER ststarthordeTime
    SCRIPT_TIMER stNextWaveDelay
    SCRIPT_TIMER stNextSubStageTimer
    eHORDE_STAGE eStage
    eEND_HORDE_REASON eEndReason
	
    FMMC_SERVER_DATA_STRUCT sFMMC_SBD
	
    STRUCT_AI_SQUAD structAiSquad[MAX_NUM_ENEMY_SQUADS]
    
    STRUCT_WAVE_DATA sWaveData
    STRUCT_HORDE_PARTICIPANT_DATA sFightingParticipantsList
    STRUCT_DELAY_BEFORE_NEXT_WAVE_DATA sDelayBeforeNextWaveStageData
    STRUCT_SURVIVAL_HELI_DATA sSurvivalHeli[MAX_NUM_SURVIVAL_HELIS]
	STRUCT_SURVIVAL_LAND_VEHICLE_DATA sSurvivalLandVehicle[MAX_NUM_LAND_VEHICLES]
    SERVER_EOM_VARS sServerFMMC_EOM
	
    INT iKills
    INT iKillsThisWave
	INT iKillsThisWaveSubStage[NUM_SURVIVAL_SUB_STAGES]
	INT iNumCreatedEnemies
	
	INT iSearchingForPed = -1
	INT iSearchingForChopper = -1
	INT iSearchingForLandVehicle = -1
	INT iSquadSpawningStage
	
    INT iBitset
    
    INT iNumAliveSquadEnemies
    INT iNumAliveHelis
    INT iNumAliveLandVehiclePeds
	
	INT iMatchHistoryId[2]
	
    BOOL bRunLastSubStageEnemiesTimer
    
	BOOL bAllVehiclePedsStuckOutOfArea
	SCRIPT_TIMER stVehicleNotInAreaTimer
	BOOL bStartVehicleFailSafeTimer
	
	SCRIPT_TIMER stNoSpawningEnemiesTimer
	
	INT iNumKillsThisWaveFromEvents
	INT iNumKillsThisSubStageFromEvents[NUM_SURVIVAL_SUB_STAGES]
	
    #IF IS_DEBUG_BUILD
    INT iSfSkipParticipant = -1
    BOOL bSkipWave
    #ENDIF
	
	INT iTimeOfDay
	INT iWeather
    
ENDSTRUCT
ServerBroadcastData serverBD

// Dpad refresh
INT iStoredRefreshDpadValue 
INT iRefreshDpad 

// Speirs end Job start
//SERVER_NEXT_JOB_VARS Horde_serverBDEndJob
NEXT_JOB_STRUCT nextJobStruct
BOOL bNewJobScreens, bMoveToEnd
// end



// *********************************** \\
// *********************************** \\
//                                     \\
// 				PLAYER BD              \\
//                                     \\
// *********************************** \\
// *********************************** \\

STRUCT PlayerBroadcastData
        
    INT iGameState
    INT iBitset
    
    INT iMyKills
    INT iMyKillsThisWave
	INT iMyVehicleKills
    INT iDeaths
	INT iSurvivalScore = -1
	INT iMyHeadshots
	INT iXpGained
	
    eHORDE_STAGE eStage
    
	BOOL bCompletedPassedSurvivalCutscene
	
	INT iMightNotBePlayingBitset
	
    #IF IS_DEBUG_BUILD
        BOOL bUsedFSkip
        BOOL bUsedSSkip
        BOOL bSkipWave
    #ENDIF
    
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]



// *********************************** \\
// *********************************** \\
//                                     \\
//				FUNCTIONS              \\
//                                     \\
// *********************************** \\
// *********************************** \\

#IF IS_DEBUG_BUILD

BOOL bJSkipped
BOOL bDoingWarpToHorde
BOOL bForceWavePassedOffset
//BOOL bNoBlipsOutput
INT iCreateWidgetsStage
BOOL bCreateWidgets
BOOL bDoMiniBoardDebugPrints
BOOL bDoRespawnMissionPedPrints
BOOL bDoProgressBarPrints
WIDGET_GROUP_ID survivalWidgetsId
BOOL bWaveStageDataWidgets
BOOL bDoEndWaveWidgets
BOOL bBlockDistanceCheck
BOOL bPutEnemiesOutsideHordeArea
BOOL bPedSpawningBug
BOOL bForceEnemyBlips
VECTOR vHeliOffset
VECTOR vCamRotation
FLOAT fCamFov
BOOL bPauseSceneOnShot3
INT iEndOfWaveXp[NUMBER_HORDE_WAVES]
INT iKillsXp[NUMBER_HORDE_WAVES]
INT iVehsDestroyedXp[NUMBER_HORDE_WAVES]
INT iTakePartXp
BOOL bDebugBlipAllAi
BOOL bDebugUnblipAllAi = TRUE
INT iMyPartId
BOOL bForceMakeSureBlownUpState[MAX_NUM_SURVIVAL_HELIS]
INT iOverheadOverrideState
BOOL bUpdateOverheadOverride
BOOL bDoAllCompletedWavePrints

BLIP_INDEX biDebugFootEnemyBlip[MAX_NUM_ENEMY_SQUADS][MAX_NUM_PEDS_IN_SQUAD]
BLIP_INDEX biDebugHeliBlip[MAX_NUM_SURVIVAL_HELIS]
BLIP_INDEX biDebugHeliEnemyBlip[MAX_NUM_SURVIVAL_HELIS][NUM_HELI_PEDS]
BLIP_INDEX biDebugLandVehBlip[MAX_NUM_LAND_VEHICLES]
BLIP_INDEX biDebugLandVehEnemyBlip[MAX_NUM_LAND_VEHICLES][NUM_LAND_VEH_PEDS]

FUNC STRING GET_SQUAD_STATE_NAME(eSQUAD_STATE eState)
	
	SWITCH eState
		CASE eSQUADSTATE_DORMANT 	RETURN	"DORMANT"
		CASE eSQUADSTATE_ACTIVE 	RETURN	"ACTIVE"
	ENDSWITCH
	
	RETURN "NOT_IN_SWITCH"
	
ENDFUNC

PROC ADD_XP_BREAKDOWN_WIDGETS()
	
	INT i
	TEXT_LABEL_63 tlTemp
	
	tlTemp = "Take Part Xp"
	ADD_WIDGET_INT_READ_ONLY(tlTemp, iTakePartXp)
		
	REPEAT NUMBER_HORDE_WAVES i 
		
		tlTemp = "End of wave "
		tlTemp += i+1
		tlTemp += " XP"
		ADD_WIDGET_INT_READ_ONLY(tlTemp, iEndOfWaveXp[i])
		
		tlTemp = "Kill during wave "
		tlTemp += i+1
		tlTemp += " XP"
		ADD_WIDGET_INT_READ_ONLY(tlTemp, iKillsXp[i])
		
		tlTemp = "Vehs destroyed during wave "
		tlTemp += i+1
		tlTemp += " XP"
		ADD_WIDGET_INT_READ_ONLY(tlTemp, iVehsDestroyedXp[i])
		
	ENDREPEAT
	
ENDPROC

PROC ADD_SQUAD_AI_WIDGETS(STRUCT_AI_SQUAD &sSquadData, TEXT_WIDGET_ID &squadStateWidget)

	START_WIDGET_GROUP("Squad AI")
		squadStateWidget = ADD_TEXT_WIDGET("State")
		ADD_WIDGET_INT_READ_ONLY("ID", sSquadData.iSquadId)
		ADD_WIDGET_INT_READ_ONLY("Squad Size", sSquadData.iSquadSize)
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_SQUAD_AI_WIDGETS(STRUCT_AI_SQUAD &sSquadData, TEXT_WIDGET_ID &squadStateWidget)

	SET_CONTENTS_OF_TEXT_WIDGET(squadStateWidget, GET_SQUAD_STATE_NAME(sSquadData.eState))
	
ENDPROC

FUNC STRING GET_GAME_STATE_NAME(INT iState)
    
    SWITCH iState
    	CASE SURVIVAL_GAME_STATE_INIT                 	RETURN "INI"
        CASE SURVIVAL_GAME_STATE_INIT_SPAWN          	RETURN "INI_SPAWN"
        CASE SURVIVAL_GAME_STATE_RUNNING             	RETURN "RUNNING"
        CASE SURVIVAL_GAME_STATE_WAITING_TO_LEAVE       RETURN "WAITING_TO_LEAVE"
        CASE SURVIVAL_GAME_STATE_LEAVE                	RETURN "LEAVE"
        CASE SURVIVAL_GAME_STATE_END                  	RETURN "END"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_HORDE_STAGE_NAME(eHORDE_STAGE eStage)
        
    SWITCH eStage
        CASE eHORDESTAGE_JIP_IN                   	RETURN "JIP_IN"
        CASE eHORDESTAGE_SETUP_MISSION            	RETURN "SETUP_MISSION"
        CASE eHORDESTAGE_START_FADE_IN            	RETURN "START_FADE_IN"
        CASE eHORDESTAGE_FIGHTING                  	RETURN "FIGHTING"
        CASE eHORDESTAGE_PASSED_WAVE               	RETURN "PASSED_WAVE"
        CASE eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE		RETURN "DELAY_BEFORE_NEXT_WAVE"
        CASE eHORDESTAGE_SPECTATING               	RETURN "SPECTATING"
        CASE eHORDESTAGE_SCTV                     	RETURN "SCTV"
        CASE eHORDESTAGE_REWARDS_AND_FEEDACK   		RETURN "REWARDS_AND_FEEDACK"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
        
ENDFUNC

FUNC STRING GET_END_HORDE_REASON_NAME(eEND_HORDE_REASON eReason)
    
    SWITCH eReason
        CASE eENDHORDEREASON_NOT_FINISHED_YET 	RETURN "NOT_FINISHED_YET"
        CASE eENDHORDEREASON_BEAT_ALL_WAVES    	RETURN "BEAT_ALL_WAVES"
        CASE eENDHORDEREASON_ALL_PLAYERS_DEAD   RETURN "ALL_PLAYERS_DEAD"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_ENEMY_STATE_NAME(eENEMY_STATE eState)
    
    SWITCH eState
            CASE eENEMYSTATE_DORMANT                RETURN "DORMANT"
            CASE eENEMYSTATE_SPAWNING               RETURN "SPAWNING"
            CASE eENEMYSTATE_FIGHTING               RETURN "FIGHTING"
            CASE eENEMYSTATE_DEAD                   RETURN "DEAD"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_KILL_STREAK_STATE_NAME(eKILL_STREAK_STATE eState)
    
    SWITCH eState
            CASE eKILLSTREAKSTATE_NONE              RETURN "NONE"
            CASE eKILLSTREAKSTATE_SMALL             RETURN "SMALL"
            CASE eKILLSTREAKSTATE_MEDIUM    RETURN "MEDIUM"
            CASE eKILLSTREAKSTATE_LARGE             RETURN "LARGE"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_WAVE_SUB_STAGE_NAME(eWAVE_STAGE eWaveStage)
    
    SWITCH eWaveStage
            CASE eWAVESTAGE_EASY RETURN "EASY"
            CASE eWAVESTAGE_MEDIUM RETURN "MEDIUM"
            CASE eWAVESTAGE_HARD RETURN "HARD"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_DELAY_BEFORE_NEXT_WAVE_SUB_STAGE_NAME(eDELAY_BEFORE_NEXT_WAVE_STAGE eStage)
    
    SWITCH eStage
            CASE eDELAYBEFORENEXTWAVESTAGE_ADD_NEW_FIGHTING_PARTICIPANTS            RETURN "ADD_NEW_FIGHTING_PARTICIPANTS"
            CASE eDELAYBEFORENEXTWAVESTAGE_WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN         RETURN "WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN"
            CASE eDELAYBEFORENEXTWAVESTAGE_DO_COUNT_DOWN                            RETURN "DO_COUNT_DOWN" 
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_MINI_BOARD_STAGE_NAME(eMINI_LEADER_BOARD_STATE eState)
    
    SWITCH eState
            CASE eMINILEADERBOARDSTATE_OFF                          RETURN "OFF"
            CASE eMINILEADERBOARDSTATE_DO_FIRST_SORT        	RETURN "DO_FIRST_SORT"
            CASE eMINILEADERBOARDSTATE_ON                           RETURN "ON"     
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_SURVIVAL_HELI_STATE_NAME(eSURVIVAL_HELI_STATE eState)
    
    SWITCH eState
            CASE eSURVIVALHELISTATE_NOT_EXIST                       RETURN "NOT_EXIST"
            CASE eSURVIVALHELISTATE_SPAWNING                        RETURN "SPAWNING"
			CASE eSURVIVALHELISTATE_FLYING_TO_SURVIVAL				RETURN "FLYING_TO_SURVIVAL"
            CASE eSURVIVALHELISTATE_FLYING_FIGHTING                 RETURN "FLYING_FIGHTING"
			CASE eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP				RETURN "MAKE_SURE_BLOWN_UP"
            CASE eSURVIVALHELISTATE_DEAD                            RETURN "DEAD"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_LAND_VEHICLE_STATE_NAME(eSURVIVAL_LAND_VEHICLE_STATE eState)
	
	SWITCH eState
		CASE eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST	RETURN "DOES_NOT_EXIST"
		CASE eSURVIVALLANDVEHICLESTATE_SPAWNING			RETURN "SPAWNING"
		CASE eSURVIVALLANDVEHICLESTATE_DRIVING			RETURN "DRIVING"
		CASE eSURVIVALLANDVEHICLESTATE_ARRIVED			RETURN "ARRIVED"
		CASE eSURVIVALLANDVEHICLESTATE_DEAD				RETURN "DEAD"
	ENDSWITCH
	
	RETURN "NOT_IN_SWITCH"
	
ENDFUNC

#ENDIF

/// PURPOSE:
///    Helper function to get the servers game/mission state
FUNC INT GET_SERVER_GAME_STATE()
    RETURN serverBD.iServerGameState
ENDFUNC 

/// PURPOSE:
///    Sets the server game state.
/// PARAMS:
///    iState - state to set to.
PROC SET_SERVER_GAME_STATE(INT iState)
    IF serverBD.iServerGameState != iState
        serverBD.iServerGameState = iState
    ENDIF
ENDPROC

/// PURPOSE:
///    Gets the current value of the server stored horde stage.
/// RETURNS:
///    Mission stage.
FUNC eHORDE_STAGE GET_SERVER_HORDE_STAGE()
    RETURN serverBD.eStage
ENDFUNC

/// PURPOSE:
///    Sets the value of the aserver stored horde stage.
///    Server only command.
/// PARAMS:
///    eMissionStage - value to set stage to.
PROC SET_SERVER_HORDE_STAGE(eHORDE_STAGE eStage)
    #IF IS_DEBUG_BUILD
        IF eStage != GET_SERVER_HORDE_STAGE()
            PRINTLN("[Survival] - setting server horde stage to ", GET_HORDE_STAGE_NAME(eStage))
        ENDIF
    #ENDIF
    serverBD.eStage = eStage
ENDPROC

/// PURPOSE:
///    Helper function to get a clients game state
FUNC INT GET_CLIENT_GAME_STATE(INT iParticipant)
    RETURN playerBD[iParticipant].iGameState
ENDFUNC

/// PURPOSE:
///    Sets the local participant game state.
/// PARAMS:
///    iState - state to set to.
PROC SET_CLIENT_GAME_STATE(INT iState)
    IF playerBD[PARTICIPANT_ID_TO_INT()].iGameState != iState
        playerBD[PARTICIPANT_ID_TO_INT()].iGameState = iState
    ENDIF
ENDPROC

/// PURPOSE:
///    Getas the local horde stage of participant (stored in player BD).
/// PARAMS:
///    participant - participant to get horde stage for.
/// RETURNS:
///    Mission stage.
FUNC eHORDE_STAGE GET_CLIENT_HORDE_STAGE(PARTICIPANT_INDEX participant)
    RETURN playerBD[NATIVE_TO_INT(participant)].eStage
ENDFUNC

/// PURPOSE:
///     Sets the local horde stage of participant (in player BD).
/// PARAMS:
///    eMissionStage - horde stage to set to.
///    participant - participant to set stage for.
PROC SET_CLIENT_HORDE_STAGE(eHORDE_STAGE eStage)
    #IF IS_DEBUG_BUILD
        IF eStage != GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID())
            PRINTLN("[Survival] - setting client horde stage to ", GET_HORDE_STAGE_NAME(eStage))
        ENDIF
    #ENDIF
    playerBD[PARTICIPANT_ID_TO_INT()].eStage = eStage
ENDPROC

/// PURPOSE:
///    Gets the current end mode reason (win/lose condition).
///    Return eENDHORDEREASON_NOT_FINISHED_YET if the horde is still ongoing.
/// RETURNS:
///    Current end reason (stored on server).
FUNC eEND_HORDE_REASON GET_END_HORDE_REASON()
    RETURN serverBD.eEndReason
ENDFUNC

/// PURPOSE:
///    Sets the end horde reason.
/// PARAMS:
///    eReason - reason to set to.
PROC SET_END_HORDE_REASON(eEND_HORDE_REASON eReason)
    #IF IS_DEBUG_BUILD
        IF eReason != GET_END_HORDE_REASON()
            PRINTLN("[Survival] - setting server horde end reason to ", GET_END_HORDE_REASON_NAME(eReason))
        ENDIF
    #ENDIF
    serverBD.eEndReason = eReason
ENDPROC

/// PURPOSE:
///    Copies creator data into struct of boundaries data so we have meaningful names to work with.
PROC SETUP_SURVIVAL_BOUNDS_DATA()
	
	sSurvivalBoundsData.vMin	= << 0.0, 0.0, 0.0 >>
	sSurvivalBoundsData.vMax	= << 0.0, 0.0, 0.0 >>
	sSurvivalBoundsData.fWidth	= 0.0
	
	sSurvivalBoundsData.vMin	= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[2].vLoc[0]
	sSurvivalBoundsData.vMax	= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[3].vLoc[0]
	sSurvivalBoundsData.fWidth	= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[3].fRadius[0]
	
	PRINTLN("[Survival] - set min survival bound as ", sSurvivalBoundsData.vMin)
	PRINTLN("[Survival] - set max survival bound as ", sSurvivalBoundsData.vMax)
	PRINTLN("[Survival] - set survival bound width as ", sSurvivalBoundsData.fWidth)
	
ENDPROC

PROC ADD_TO_END_CELBRATION_SCORE(INT iAmountToAdd)

	iEndCelebrationScreenScore += iAmountToAdd
	PRINTLN("[Survival] - ADD_TO_END_CELBRATION_SCORE - iAmountToAdd = ", iAmountToAdd, ", iEndCelebrationScreenScore now equals ", iEndCelebrationScreenScore)

ENDPROC

FUNC BOOL ARE_ANY_SURVIVAL_CONTROLS_BEING_PRESSED()
	 
	IF NOT IS_ANY_BUTTON_PRESSED(PLAYER_CONTROL)
	AND NOT IS_ANY_BUTTON_PRESSED(FRONTEND_CONTROL)
	AND NOT IS_ANY_BUTTON_PRESSED(CAMERA_CONTROL)
		RETURN FALSE
	ENDIF

	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Gets if the local player should be considered for possible leecher checks.
///    Do not want spectators do be included in checks.
/// RETURNS:
///    TRUE/FALSE.
FUNC BOOL IS_LOCAL_PLAYER_MONITOR_FOR_LEECHER_CANDIDATE()
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SPECTATING(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Gets if the possible leecher triggers should be monitored on the local player.
///    Checks if the player is leecher check candidate and checks the survival is in the correct stage (fighting).
/// RETURNS:
///    
FUNC BOOL SHOULD_LEECHER_TRIGGERS_BE_MONITORED()
	
	IF (GET_SERVER_HORDE_STAGE() != eHORDESTAGE_FIGHTING)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_MONITOR_FOR_LEECHER_CANDIDATE()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Gets the name of a might not be playing flag as a string.
/// PARAMS:
///    eFlag - flag to get as a string.
/// RETURNS:
///    flag as a STRING.
FUNC STRING GET_MIGHT_NOT_BE_PLAYING_FLAG_NAME(eMIGHT_NOT_BE_PLAYING_FLAGS eFlag)
	SWITCH eFlag
		CASE eMIGHTNOTBEPLAYINGFLAGS_NOT_DAMAGED_ENEMY		RETURN "NOT_DAMAGED_ENEMY"
		CASE eMIGHTNOTBEPLAYINGFLAGS_IDLE					RETURN "IDLE"
	ENDSWITCH
	RETURN "NOT_IN_SWITCH"
ENDFUNC
#ENDIF

/// PURPOSE:
///    Sets the local players playerBD might not be playing bitset according to the flag passed.
/// PARAMS:
///    eFlag - bit to set (enum).
///    bOn - value to set bit to.
PROC SET_MIGHT_NOT_BE_PLAYING_FLAG(eMIGHT_NOT_BE_PLAYING_FLAGS eFlag, BOOL bOn)
//	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[Survival] - set might not be playing flag ", GET_MIGHT_NOT_BE_PLAYING_FLAG_NAME(eFlag), " to ", bOn)
//	#ENDIF
//	
	IF bOn
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iMightNotBePlayingBitset, ENUM_TO_INT(eFlag))
	ELSE
		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iMightNotBePlayingBitset, ENUM_TO_INT(eFlag))
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets the value of a participant's playerBD might not be playing bitset accoridng to the flag passed.
/// PARAMS:
///    iParticipant - participant to check.
///    eFlag - flag to check.
/// RETURNS:
///    TRUE/FALSE to indicate of bit is set.
FUNC BOOL GET_MIGHT_NOT_BE_PLAYING_FLAG(INT iParticipant, eMIGHT_NOT_BE_PLAYING_FLAGS eFlag)
	
	RETURN IS_BIT_SET(playerBD[iParticipant].iMightNotBePlayingBitset, ENUM_TO_INT(eFlag))
	
ENDFUNC

/// PURPOSE:
///    Gets if any might not be playing flag for a participant is set.
///    Loops through the size of the might not be playing flags enum and checks the if the bit is set in the participant's playerBD.
/// PARAMS:
///    iParticipant - participant to check.
/// RETURNS:
///    TRUE/FALSE to indicate if any flag is set.
FUNC BOOL GET_PARTICIPANT_IS_ANY_MIGHT_NOT_BE_PLAYING_FLAG_SET(INT iParticipant)
	
	INT i
	
	REPEAT COUNT_OF(eMIGHT_NOT_BE_PLAYING_FLAGS) i
		IF GET_MIGHT_NOT_BE_PLAYING_FLAG(iParticipant, INT_TO_ENUM(eMIGHT_NOT_BE_PLAYING_FLAGS, i))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Resets all of the local player's might not be playing flags.
PROC RESET_ALL_MIGHT_NOT_BE_PLAYING_FLAGS()
	
//	PRINTLN("[Survival] - RESET_ALL_MIGHT_NOT_BE_PLAYING_FLAGS() has been called.")
	
	INT i
	
	REPEAT COUNT_OF(eMIGHT_NOT_BE_PLAYING_FLAGS) i
		IF GET_MIGHT_NOT_BE_PLAYING_FLAG(PARTICIPANT_ID_TO_INT(), INT_TO_ENUM(eMIGHT_NOT_BE_PLAYING_FLAGS, i))
			SET_MIGHT_NOT_BE_PLAYING_FLAG(INT_TO_ENUM(eMIGHT_NOT_BE_PLAYING_FLAGS, i), FALSE)
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Updates the local players might not be playing state. Looks for each criteria and sets/unsets the bitset iMightNotBePlayingFlags in player BD.
PROC MAINTAIN_MY_MIGHT_NOT_BE_PLAYING_STATE()
	
	IF SHOULD_LEECHER_TRIGGERS_BE_MONITORED()
		
		// Monitor for the player going idle.
		IF NOT ARE_ANY_SURVIVAL_CONTROLS_BEING_PRESSED()
			IF NOT HAS_NET_TIMER_STARTED(stIdleTimer)
				START_NET_TIMER(stIdleTimer)
			ENDIF
		ELSE
			RESET_NET_TIMER(stIdleTimer)
			SET_MIGHT_NOT_BE_PLAYING_FLAG(eMIGHTNOTBEPLAYINGFLAGS_IDLE, FALSE)
		ENDIF
		IF HAS_NET_TIMER_STARTED(stIdleTimer)
			IF HAS_NET_TIMER_EXPIRED(stIdleTimer, MIGHT_NOT_BE_PLAYING_TIMER_LIMIT)
				PRINTLN("[Survival] - MAINTAIN_MY_MIGHT_NOT_BE_PLAYING_STATE -  idle timer has expired.")
				SET_MIGHT_NOT_BE_PLAYING_FLAG(eMIGHTNOTBEPLAYINGFLAGS_IDLE, TRUE)
			ENDIF
		ENDIF
		
		// Monitor for the player not damaging any enemies.
		IF NOT bIHaveDamagedAnEnemy
			IF NOT HAS_NET_TIMER_STARTED(stNotDamagedEnemyTimer)
				START_NET_TIMER(stNotDamagedEnemyTimer)
			ENDIF
		ELSE
			RESET_NET_TIMER(stNotDamagedEnemyTimer)
			SET_MIGHT_NOT_BE_PLAYING_FLAG(eMIGHTNOTBEPLAYINGFLAGS_NOT_DAMAGED_ENEMY, FALSE)
		ENDIF
		IF HAS_NET_TIMER_STARTED(stNotDamagedEnemyTimer)
			IF HAS_NET_TIMER_EXPIRED(stNotDamagedEnemyTimer, MIGHT_NOT_BE_PLAYING_TIMER_LIMIT)
				PRINTLN("[Survival] - MAINTAIN_MY_MIGHT_NOT_BE_PLAYING_STATE -  not damaged enemy timer has expired.")
				SET_MIGHT_NOT_BE_PLAYING_FLAG(eMIGHTNOTBEPLAYINGFLAGS_NOT_DAMAGED_ENEMY, TRUE)
			ENDIF
		ENDIF
	
	// reset all if not in the fighting stage.
	ELSE
		
		RESET_NET_TIMER(stIdleTimer)
		RESET_NET_TIMER(stNotDamagedEnemyTimer)
		RESET_ALL_MIGHT_NOT_BE_PLAYING_FLAGS()
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets how far away the furtherst remote player is from the local player.
/// PARAMS:
///    iParticipant - participant to check.
PROC CALCULATE_FURTHEST_PLAYER_DISTANCE(INT iParticipant)
	
	PARTICIPANT_INDEX partId
	PED_INDEX pedId
	PED_INDEX piMidPed = PLAYER_PED_ID()
	PED_INDEX tempPed
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
		
		partId = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
		pedId = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(partId))
		
		IF DOES_ENTITY_EXIST(pedId)
			IF NOT IS_ENTITY_DEAD(pedId)
				
				IF GET_CLIENT_HORDE_STAGE(partId) != eHORDESTAGE_SPECTATING
					
					IF IS_PLAYER_SPECTATING(PLAYER_ID())
						tempPed = GET_SPECTATOR_CURRENT_FOCUS_PED()
						IF DOES_ENTITY_EXIST(tempPed)
							IF NOT IS_ENTITY_DEAD(tempPed)
								piMidPed = tempPed
							ENDIF
						ENDIF
					ENDIF
					
					fPartDis = GET_DISTANCE_BETWEEN_ENTITIES(pedId, piMidPed)
					
					IF fPartDis > fFurthestTargetDist
						fFurthestTargetDist = fPartDis
					ENDIF
				
				ENDIF
				
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Updates the radar zoom so the boundary of the radar is slightly further than the furthest away remote player.
/// PARAMS:
///    bInstant - ask Rowan, he wrote this.
PROC CONTROL_RADAR_ZOOM(BOOL bInstant = FALSE)

	FLOAT fZoomLevel
	IF DO_RADAR_ZOOM()
		IF NOT bInstant
			
			IF fOldFurthestTargetDist > fFurthestTargetDist
				IF (fOldFurthestTargetDist - fFurthestTargetDist) > 5
					fOldFurthestTargetDist= fOldFurthestTargetDist-cfRADAR_ZOOM_SPEED
				ENDIF
			ELSE
				IF (fFurthestTargetDist - fOldFurthestTargetDist) > 5
					fOldFurthestTargetDist= fOldFurthestTargetDist+cfRADAR_ZOOM_SPEED
				ENDIF
			ENDIF
			
			fZoomLevel = fOldFurthestTargetDist/cfRADAR_ZOOM_ADJUST
			
			// Cap the zoom level
			IF (fZoomLevel >= cfRADAR_ZOOM_CAP)
				fZoomLevel = cfRADAR_ZOOM_CAP
			ELIF (fZoomLevel < cfRADAR_ZOOM_CAP_MIN)
				fZoomLevel = cfRADAR_ZOOM_CAP_MIN
			ENDIF
			
			IF DO_RADAR_ZOOM()
				SET_RADAR_ZOOM_TO_DISTANCE(fZoomLevel)
			ENDIF
			
		ELSE
		
			fOldFurthestTargetDist = fFurthestTargetDist
			fZoomLevel = fOldFurthestTargetDist/cfRADAR_ZOOM_ADJUST
			IF DO_RADAR_ZOOM()
				SET_RADAR_ZOOM_TO_DISTANCE(fZoomLevel)
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets the number of participants that are considered to be competing participants.
/// RETURNS:
///   	INT - number of fighting participants.
FUNC INT GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
    
    RETURN serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave
    
ENDFUNC

FUNC STRING GET_SURVIVAL_MUSIC_EVENT_STRING(ENUM_MUSIC_EVENT eEvent, INT iWave)
	
	SWITCH eEvent
	
		CASE eMUSICEVENT_START
			SWITCH iWave
				CASE 1
					RETURN "SM_W1_START_ALL"
				BREAK
				CASE 2
					RETURN "SM_W2_START"
				BREAK
				CASE 3
					RETURN "SM_W3_START"
				BREAK
				CASE 4
					RETURN "SM_W4_START"
				BREAK
				CASE 5
					RETURN "SM_W5_START"
				BREAK
				CASE 6
					RETURN "SM_W6_START"
				BREAK
				CASE 7
					RETURN "SM_W7_START"
				BREAK
				CASE 8
					RETURN "SM_W8_START"
				BREAK
				CASE 9
					RETURN "SM_W9_START"
				BREAK
				CASE 10
					RETURN "SM_W10_START"
				BREAK
			ENDSWITCH
		BREAK	
		
		CASE eMUSICEVENT_WAVE_40P
			SWITCH iWave
				CASE 1
					RETURN "SM_W1_MED"
				BREAK
				CASE 2
					RETURN "SM_W2_MED"
				BREAK
				CASE 3
					RETURN "SM_W3_MED"
				BREAK
				CASE 4
					RETURN "SM_W4_MED"
				BREAK
				CASE 5
					RETURN "SM_W5_MED"
				BREAK
				CASE 6
					RETURN "SM_W6_MED"
				BREAK
				CASE 7
					RETURN "SM_W7_MED"
				BREAK
				CASE 8
					RETURN "SM_W8_MED"
				BREAK
				CASE 9
					RETURN "SM_W9_MED"
				BREAK
				CASE 10
					RETURN "SM_W10_MED"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE eMUSICEVENT_WAVE_70P				
			SWITCH iWave
				CASE 1
					RETURN "SM_W1_HIGH"
				BREAK
				CASE 2
					RETURN "SM_W2_HIGH"
				BREAK
				CASE 3
					RETURN "SM_W3_HIGH"
				BREAK
				CASE 4
					RETURN "SM_W4_HIGH"
				BREAK
				CASE 5
					RETURN "SM_W5_HIGH"
				BREAK
				CASE 6
					RETURN "SM_W6_HIGH"
				BREAK
				CASE 7
					RETURN "SM_W7_HIGH"
				BREAK
				CASE 8
					RETURN "SM_W8_HIGH"
				BREAK
				CASE 9
					RETURN "SM_W9_HIGH"
				BREAK
				CASE 10
					RETURN "SM_W10_HIGH"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE eMUSICEVENT_WAVE_COMPLETE				
			SWITCH iWave
				CASE 1
					RETURN "SM_W1_END"
				BREAK
				CASE 2
					RETURN "SM_W2_END"
				BREAK
				CASE 3
					RETURN "SM_W3_END"
				BREAK
				CASE 4
					RETURN "SM_W4_END"
				BREAK
				CASE 5
					RETURN "SM_W5_END"
				BREAK
				CASE 6
					RETURN "SM_W6_END"
				BREAK
				CASE 7
					RETURN "SM_W7_END"
				BREAK
				CASE 8
					RETURN "SM_W8_END"
				BREAK
				CASE 9
					RETURN "SM_W9_END"
				BREAK
				CASE 10
					RETURN "SM_W10_END"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE eMUSICEVENT_SILENT
			RETURN "MP_SM_SILENT"
		BREAK
		
		CASE eMUSICEVENT_STOP			
			RETURN "MP_SM_STOP_TRACK"
		BREAK
		
	ENDSWITCH
	
	NET_SCRIPT_ASSERT("GET_SURVIVAL_MUSIC_EVENT_STRING returning invalid value.")
	RETURN "INVALID MUSIC EVENT"
	
ENDFUNC

FUNC BOOL IS_PED_MODEL_A_COP_MODEL(MODEL_NAMES eModel)

	SWITCH eModel
		CASE CSB_COP 				RETURN TRUE
		CASE CSB_ROCCOPELOSI 		RETURN TRUE
		CASE IG_ROCCOPELOSI 		RETURN TRUE
		CASE S_F_Y_COP_01 			RETURN TRUE
		CASE S_M_M_SNOWCOP_01 		RETURN TRUE
		CASE S_M_Y_COP_01 			RETURN TRUE
		CASE S_M_Y_HWAYCOP_01 		RETURN TRUE
		CASE S_M_Y_SWAT_01 			RETURN TRUE
		CASE SHERIFF 				RETURN TRUE                                                                     
		CASE SHERIFF2 				RETURN TRUE
		CASE S_F_Y_SHERIFF_01 		RETURN TRUE                                                   
		CASE S_M_Y_SHERIFF_01 		RETURN TRUE
		CASE S_M_M_PrisGuard_01 	RETURN TRUE
		CASE S_M_Y_Ranger_01 		RETURN TRUE
		CASE S_F_Y_Ranger_01 		RETURN TRUE
	ENDSWITCH

	RETURN FALSE
	
ENDFUNC

FUNC BOOL ARE_ENEMY_PEDS_COPS()
	
	RETURN IS_PED_MODEL_A_COP_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].mn)
	
ENDFUNC

FUNC MODEL_NAMES GET_LAND_VEHICLE_MODEL(BOOL bWantBigVehicle, BOOL bWantVan)
	
	IF ARE_ENEMY_PEDS_COPS()
		IF bWantBigVehicle
			RETURN POLICET
		ELSE
			RETURN POLICE3
		ENDIF
	ELSE
		IF bWantBigVehicle
			RETURN SANDKING2
		ELIF bWantVan
			RETURN SPEEDO
		ELSE
			RETURN CAVALCADE
		ENDIF
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
	
ENDFUNC

/// PURPOSE:
///    Sets up the 
PROC POPULATE_WAVE_SQUAD_DATA()
    
	INT iNumhHelis
	
    SWITCH serverBD.sWaveData.iWaveCount
        
        CASE 1
            
            SWITCH GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
                
                CASE 1
                    serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0
                BREAK
                
                CASE 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0
                BREAK
                
                CASE 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0
                BREAK
                
                CASE 4
                    serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0
                BREAK
                
            ENDSWITCH
            
        BREAK
        
        CASE 2
            
            SWITCH GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
                
                CASE 1
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0
                BREAK
                
                CASE 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0
                BREAK
                
                CASE 3
                    serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 5
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 5
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0
                BREAK
                
                CASE 4
                    serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 5
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 4
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 4
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0
                BREAK
                
            ENDSWITCH
            
        BREAK
    
        CASE 3
            
			serverBD.sSurvivalLandVehicle[0].iNumPeds 														= 2
			serverBD.sSurvivalLandVehicle[0].eModel 														= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
			serverBD.sSurvivalLandVehicle[1].iNumPeds 														= 2
			serverBD.sSurvivalLandVehicle[1].eModel 														= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
			serverBD.sSurvivalLandVehicle[2].iNumPeds 														= 0
			serverBD.sSurvivalLandVehicle[2].eModel 														= DUMMY_MODEL_FOR_SCRIPT
			serverBD.sSurvivalLandVehicle[3].iNumPeds 														= 0
			serverBD.sSurvivalLandVehicle[3].eModel 														= DUMMY_MODEL_FOR_SCRIPT
            SWITCH GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
                
                CASE 1
                    serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0
                BREAK
                
                CASE 2
                    serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 4
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 4
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0   
                BREAK
                
                CASE 3
                	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 4
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 4
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0  
                BREAK
                
                CASE 4
            		serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 4
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 5
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 0
                    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 0 
                BREAK
                
            ENDSWITCH
            
        BREAK
    
        CASE 4
            
			iNumhHelis = 1
			
            SWITCH GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
                
                CASE 1
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2
                BREAK
                
                CASE 2
                    serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 3
                BREAK
                
                CASE 3
            		serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 3    
                BREAK
                
                CASE 4
            		serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 3 
				BREAK
            ENDSWITCH
            
        BREAK
    
        CASE 5
			
            SWITCH GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
                
                CASE 1
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[2].eModel 												= DUMMY_MODEL_FOR_SCRIPT
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[3].eModel 												= DUMMY_MODEL_FOR_SCRIPT
                    serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2
                BREAK
                
                CASE 2
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[2].eModel 												= DUMMY_MODEL_FOR_SCRIPT
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[2].eModel 												= DUMMY_MODEL_FOR_SCRIPT
                    serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2
                BREAK
                
                CASE 3
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[3].eModel 												= DUMMY_MODEL_FOR_SCRIPT
           			serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2
                BREAK
                
                CASE 4
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[3].eModel 												= DUMMY_MODEL_FOR_SCRIPT
		            serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 6
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2
                BREAK
                
            ENDSWITCH
            
        BREAK
    
        CASE 6
            
            SWITCH GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
                
                CASE 1
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[3].eModel 												= DUMMY_MODEL_FOR_SCRIPT
                	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 2
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2 
                BREAK
                
                CASE 2
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[3].eModel 												= DUMMY_MODEL_FOR_SCRIPT
                   	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2     
                BREAK
                
                CASE 3
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[3].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
                	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2    
                BREAK
                
                CASE 4
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[3].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
                	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2    
                BREAK
                
            ENDSWITCH
            
        BREAK
    
        CASE 7
            
            SWITCH GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
                
                CASE 1
					iNumhHelis = 1
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 1
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2
                BREAK
                
                CASE 2
					iNumhHelis = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 3
                BREAK
                
                CASE 3
					iNumhHelis = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2
                BREAK
                
                CASE 4
					iNumhHelis = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 4
                BREAK
                
            ENDSWITCH
            
        BREAK
    
        CASE 8
			
            SWITCH GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
                    
                CASE 1
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[2].eModel              									= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[3].eModel 												= DUMMY_MODEL_FOR_SCRIPT
                	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 2
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2    
                BREAK
                
                CASE 2
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[3].eModel 												= DUMMY_MODEL_FOR_SCRIPT
                	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2     
                BREAK
                
                CASE 3
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[3].eModel 												= DUMMY_MODEL_FOR_SCRIPT
               		serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 3     
                BREAK
                
                CASE 4
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 4
					serverBD.sSurvivalLandVehicle[3].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
                	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 3   
                BREAK
                
            ENDSWITCH
            
        BREAK
    
        CASE 9
			
            SWITCH GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
                
                CASE 1
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[3].eModel 												= DUMMY_MODEL_FOR_SCRIPT
                	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2    
                BREAK
                
                CASE 2
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[3].eModel 												= DUMMY_MODEL_FOR_SCRIPT
                	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 2
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 3
                BREAK
                
                CASE 3
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 4
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 0
					serverBD.sSurvivalLandVehicle[3].eModel 												= DUMMY_MODEL_FOR_SCRIPT
                	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 3   
                BREAK
                
                CASE 4
					serverBD.sSurvivalLandVehicle[0].iNumPeds 												= 2
					serverBD.sSurvivalLandVehicle[0].eModel 												= GET_LAND_VEHICLE_MODEL(TRUE, FALSE)
					serverBD.sSurvivalLandVehicle[1].iNumPeds 												= 3
					serverBD.sSurvivalLandVehicle[1].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, FALSE)
					serverBD.sSurvivalLandVehicle[2].iNumPeds 												= 4
					serverBD.sSurvivalLandVehicle[2].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
					serverBD.sSurvivalLandVehicle[3].iNumPeds 												= 4
					serverBD.sSurvivalLandVehicle[3].eModel 												= GET_LAND_VEHICLE_MODEL(FALSE, TRUE)
               		serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 3   
                BREAK
                
            ENDSWITCH
            
        BREAK
    
        CASE 10
            
            SWITCH GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
                
                CASE 1
					iNumhHelis = 1
              		serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2  
                BREAK
                
                CASE 2
					iNumhHelis = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 2
                BREAK
                
                CASE 3
					iNumhHelis = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 2
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 3
                BREAK
                
                CASE 4
					iNumhHelis = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]                                     = 3
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]                                          = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]                                   = 4
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]                                        = 3
					serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]                                     = 5
		            serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]                                          = 3
                BREAK
                
            ENDSWITCH
            
        BREAK
                                                                                                                                                                                                        
    ENDSWITCH
    
	#IF IS_DEBUG_BUILD
		INT iDebug
		TEXT_LABEL_63 tl63Debug
		REPEAT COUNT_OF(eWAVE_STAGE) iDebug
			IF serverBD.sWaveData.iNumPedsInSquad[iDebug] > MAX_NUM_PEDS_IN_SQUAD
				tl63Debug = "iNumPedsInSquad > MAX_NUM_PEDS_IN_SQUAD. Wave = "
				tl63Debug += serverBD.sWaveData.iWaveCount
				tl63Debug += ", parts = "
				tl63Debug += GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
				NET_SCRIPT_ASSERT(tl63Debug)
			ENDIF
			IF serverBD.sWaveData.iNumSquads[iDebug] > MAX_NUM_ENEMY_SQUADS
				tl63Debug = "iNumSquads > MAX_NUM_ENEMY_SQUADS. Wave = "
				tl63Debug += serverBD.sWaveData.iWaveCount
				tl63Debug += ", parts = "
				tl63Debug += GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
				NET_SCRIPT_ASSERT(tl63Debug)
			ENDIF
		ENDREPEAT
	#ENDIF
	
	IF serverBD.sWaveData.iNumRequiredKills    = 0
		
		serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY] 		= serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY] * serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]
		PRINTLN("[Survival] - [Populate Enemy Data] - iNumRequiredSubstageKills[eWAVESTAGE_EASY] = ", serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY])
		
		serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM] 	= serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM] * serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]
		PRINTLN("[Survival] - [Populate Enemy Data] - iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM] = ", serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM])
		
		serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_HARD] 		= serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD] * serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]
		PRINTLN("[Survival] - [Populate Enemy Data] - iNumRequiredSubstageKills[eWAVESTAGE_HARD] = ", serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_HARD])
		
    	serverBD.sWaveData.iNumRequiredKills    							= ( serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY]   +
                                                                    			serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM] +
                                                                    			serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_HARD] 		)
		PRINTLN("[Survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumRequiredKills = ", serverBD.sWaveData.iNumRequiredKills)
		
		serverBD.sWaveData.iNumRequiredKills 								+= iNumhHelis
		PRINTLN("[Survival] - [Populate Enemy Data] - iNumhHelis = ", iNumhHelis)
		PRINTLN("[Survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumRequiredKills = ", serverBD.sWaveData.iNumRequiredKills)
		
		INT i
		
		REPEAT MAX_NUM_LAND_VEHICLES i
			IF serverBD.sSurvivalLandVehicle[i].iNumPeds > 0
				serverBD.sWaveData.iNumRequiredKills 							+= serverBD.sSurvivalLandVehicle[i].iNumPeds
				PRINTLN("[Survival] - [Populate Enemy Data] - serverBD.sSurvivalLandVehicle[", i, "].iNumPeds = ", serverBD.sSurvivalLandVehicle[i].iNumPeds)
				PRINTLN("[Survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumRequiredKills = ", serverBD.sWaveData.iNumRequiredKills)
			ENDIF
		ENDREPEAT
		
//		IF serverBD.sWaveData.iWaveCount = 1
//			serverBD.iKillsThisWaveSubStage[eWAVESTAGE_EASY] -= 2
//		ENDIF
//		
	ENDIF
    
ENDPROC

FUNC INT GET_ACTIVATE_NEXT_SUB_STAGE_TIME()
	
	IF serverBD.sWaveData.iWaveCount <= 4
		RETURN 5000
	ELIF serverBD.sWaveData.iWaveCount <= 6
		RETURN 7500
	ENDIF
		
	RETURN 10000
	
ENDFUNC

FUNC INT GET_START_NEXT_SUB_STAGE_TIMER_NUM_ENEMIES()
	
	IF serverBD.sWaveData.iWaveCount <= 4
		RETURN 3
	ENDIF
		
	RETURN 4
	
ENDFUNC

FUNC INT GET_NUM_ENEMIES_IN_SQUAD_FOR_WAVE()
    
    RETURN serverBD.sWaveData.iNumPedsInSquad
    
ENDFUNC

FUNC INT GET_NUM_SQUADS_FOR_WAVE()
    
    RETURN serverBD.sWaveData.iNumSquads
    
ENDFUNC

FUNC INT GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVE_STAGE eWaveStage)
    
    RETURN serverBD.sWaveData.iNumRequiredSubstageKills[ENUM_TO_INT(eWaveStage)]
    
ENDFUNC

PROC RESET_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
	
	SCRIPT_TIMER stTemp
	
	serverBD.bRunLastSubStageEnemiesTimer = FALSE
    RESET_NET_TIMER(serverBD.stNextSubStageTimer)
    serverBD.stNextSubStageTimer = stTemp
	
ENDPROC

FUNC BOOL HAVE_ENOUGH_ENEMIES_BEEN_CREATED_TO_START_SUB_STAGE_LAST_ENEMIES_CHECK()
	
	INT iTrigger
	
	SWITCH serverBD.sWaveData.eStage
		
		CASE eWAVESTAGE_EASY
			iTrigger = serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY]
		BREAK
		
		CASE eWAVESTAGE_MEDIUM
			iTrigger = serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY] + serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM]
		BREAK
		
		CASE eWAVESTAGE_HARD
			iTrigger = serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY] + serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM] + serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_HARD]
		BREAK
		
	ENDSWITCH
	
	RETURN (serverBD.iNumCreatedEnemies >= iTrigger)
	
ENDFUNC

FUNC BOOL MAINTAIN_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
	
	IF serverBD.bRunLastSubStageEnemiesTimer
		
		IF serverBD.iNumAliveSquadEnemies <= GET_START_NEXT_SUB_STAGE_TIMER_NUM_ENEMIES()
			
			// Run spawn overlap peds timer.
            IF NOT HAS_NET_TIMER_STARTED(serverBD.stNextSubStageTimer)
                START_NET_TIMER(serverBD.stNextSubStageTimer)
            ELSE
           	 	// If the timer has expired return true.
                IF HAS_NET_TIMER_EXPIRED(serverBD.stNextSubStageTimer, GET_ACTIVATE_NEXT_SUB_STAGE_TIME())
                    RETURN TRUE
                ENDIF
            ENDIF
			
		ENDIF
		
	ELSE
		
		// If we have met the consitions to run the overlap peds timer, set flag.
        IF HAVE_ENOUGH_ENEMIES_BEEN_CREATED_TO_START_SUB_STAGE_LAST_ENEMIES_CHECK()
            serverBD.bRunLastSubStageEnemiesTimer = TRUE
        ENDIF
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC ACTIVATE_WAVE_SUBSTAGE_SQUADS()
	
	INT i, j
	
	SWITCH serverBD.sWaveData.eStage
	
		CASE eWAVESTAGE_EASY
			REPEAT serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY] i
				serverBD.structAiSquad[i].eState = eSQUADSTATE_ACTIVE
				REPEAT serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY] j
					serverBD.structAiSquad[i].sPed[j].eState = eENEMYSTATE_SPAWNING
					serverBD.structAiSquad[i].iSquadSize = serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]
				ENDREPEAT
			ENDREPEAT
		BREAK
		
		CASE eWAVESTAGE_MEDIUM
			REPEAT serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM] i
				serverBD.structAiSquad[i+4].eState = eSQUADSTATE_ACTIVE
				REPEAT serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM] j
					serverBD.structAiSquad[i+4].sPed[j].eState = eENEMYSTATE_SPAWNING
					serverBD.structAiSquad[i+4].iSquadSize = serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]
				ENDREPEAT
			ENDREPEAT
		BREAK
		
		CASE eWAVESTAGE_HARD
			REPEAT serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD] i
				serverBD.structAiSquad[i+8].eState = eSQUADSTATE_ACTIVE
				REPEAT serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD] j
					serverBD.structAiSquad[i+8].sPed[j].eState = eENEMYSTATE_SPAWNING
					serverBD.structAiSquad[i+8].iSquadSize = serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]
				ENDREPEAT
			ENDREPEAT
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL ARE_ANY_ENEMY_PEDS_ALIVE()
	
	IF serverBD.iNumAliveSquadEnemies > 0
		RETURN TRUE
	ENDIF
	
	IF serverBD.iNumAliveHelis > 0
		RETURN TRUE
	ENDIF
	
	IF serverBd.iNumAliveLandVehiclePeds > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL ARE_WE_ONE_OFF_OF_SUB_STAGE_KILL_LIMIT_WITH_NO_PEDS_TO_KILL(eWAVE_STAGE eStage)
	
	IF NOT ARE_ANY_ENEMY_PEDS_ALIVE()
		IF serverBD.iKillsThisWaveSubStage[eStage] >= (serverBD.sWaveData.iNumRequiredSubstageKills[eStage] - 1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_WAVE_PROGRESSION()
    
    SWITCH serverBD.sWaveData.eStage
        
        CASE eWAVESTAGE_EASY
            
			SWITCH serverBD.sWaveData.iMaintainSquadsStage
			
				CASE 0
					serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] = 0
					serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] = 0
					RESET_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
					POPULATE_WAVE_SQUAD_DATA()
					ACTIVATE_WAVE_SUBSTAGE_SQUADS()
					serverBD.sWaveData.iMaintainSquadsStage++
				BREAK
				
				CASE 1
					
					#IF IS_DEBUG_BUILD
						IF bWaveStageDataWidgets
							PRINTLN("[Survival] - [Wave Progression] - PROCESS_WAVE_STAGE_DATA")
							PRINTLN("[Survival] - [Wave Progression] - iKillsThisWaveSubStage 								= ", serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage])
							PRINTLN("[Survival] - [Wave Progression] - GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_EASY) 	= ", GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_EASY))
							IF serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] >= GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_EASY)
							PRINTLN("[Survival] - [Wave Progression] - serverBD.iKillsThisWaveSubStage >= GET_COMPLETE_WAVE_STAGE_KILL_LIMIT")
							ELSE
							PRINTLN("[Survival] - [Wave Progression] - serverBD.iKillsThisWaveSubStage < GET_COMPLETE_WAVE_STAGE_KILL_LIMIT")
							ENDIF
						ENDIF
					#ENDIF
					
					IF serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] >= serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY]
					OR serverBD.iNumKillsThisWaveFromEvents >= serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY]
					OR ARE_WE_ONE_OFF_OF_SUB_STAGE_KILL_LIMIT_WITH_NO_PEDS_TO_KILL(eWAVESTAGE_EASY)
					OR MAINTAIN_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
						PRINTLN("[Survival] - [Wave Progression] - setting serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM")
			       		serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM
						serverBD.sWaveData.iMaintainSquadsStage = 0
						IF ARE_WE_ONE_OFF_OF_SUB_STAGE_KILL_LIMIT_WITH_NO_PEDS_TO_KILL(eWAVESTAGE_EASY)
							serverBD.iKills++
                			serverBD.iKillsThisWave++
                			serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage]++
						ENDIF
			      	ENDIF
					
				BREAK
			
			ENDSWITCH
			
        BREAK
        
        CASE eWAVESTAGE_MEDIUM
            
            SWITCH serverBD.sWaveData.iMaintainSquadsStage
                
                CASE 0
					serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] = 0
					serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] = 0
					RESET_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
					ACTIVATE_WAVE_SUBSTAGE_SQUADS()
                    serverBD.sWaveData.iMaintainSquadsStage++
                BREAK
                
                CASE 1
					
					#IF IS_DEBUG_BUILD
						IF bWaveStageDataWidgets
							PRINTLN("[Survival] - [Wave Progression] - PROCESS_WAVE_STAGE_DATA")
							PRINTLN("[Survival] - [Wave Progression] - iKillsThisWaveSubStage 								= ", serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage])
							PRINTLN("[Survival] - [Wave Progression] - GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_MEDIUM) 	= ", GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_MEDIUM))
							IF serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] >= GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_EASY)
							PRINTLN("[Survival] - [Wave Progression] - serverBD.iKillsThisWaveSubStage >= GET_COMPLETE_WAVE_STAGE_KILL_LIMIT")
							ELSE
							PRINTLN("[Survival] - [Wave Progression] - serverBD.iKillsThisWaveSubStage < GET_COMPLETE_WAVE_STAGE_KILL_LIMIT")
							ENDIF
						ENDIF
					#ENDIF
					
					IF serverBD.sWaveData.iNumRequiredSubstageKills[ENUM_TO_INT(eWAVESTAGE_HARD)] > 0
	                    IF serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] >= serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM]
						OR serverBD.iNumKillsThisWaveFromEvents >= serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM]
						OR ARE_WE_ONE_OFF_OF_SUB_STAGE_KILL_LIMIT_WITH_NO_PEDS_TO_KILL(eWAVESTAGE_MEDIUM)
						OR MAINTAIN_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
							PRINTLN("[Survival] - [Wave Progression] - setting serverBD.sWaveData.eStage = eWAVESTAGE_HARD")
	                        serverBD.sWaveData.eStage = eWAVESTAGE_HARD
	                        serverBD.sWaveData.iMaintainSquadsStage = 0
							IF ARE_WE_ONE_OFF_OF_SUB_STAGE_KILL_LIMIT_WITH_NO_PEDS_TO_KILL(eWAVESTAGE_MEDIUM)
								serverBD.iKills++
	                			serverBD.iKillsThisWave++
	                			serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage]++
							ENDIF
	                    ENDIF
					ENDIF
					
                BREAK
                
            ENDSWITCH
            
        BREAK
        
        CASE eWAVESTAGE_HARD
    
            SWITCH serverBD.sWaveData.iMaintainSquadsStage
                
                CASE 0
                    serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] = 0
					serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] = 0
					RESET_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
					ACTIVATE_WAVE_SUBSTAGE_SQUADS()
                    serverBD.sWaveData.iMaintainSquadsStage++
                BREAK
                
                CASE 1
                    // Do nothing, wait for end of wave.
                BREAK
                
            ENDSWITCH
            
        BREAK
        
    ENDSWITCH
    
ENDPROC

FUNC PICKUP_TYPE GET_PICKUP_TYPE_FOR_WAVE(INT iPickup)
    
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tl63_debugPrint
	#ENDIF
	
    // Always want health and armour to be the same.
    IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].pt = PICKUP_HEALTH_STANDARD
        RETURN PICKUP_HEALTH_STANDARD
    ENDIF
    
    IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].pt = PICKUP_ARMOUR_STANDARD
        RETURN PICKUP_ARMOUR_STANDARD
    ENDIF
	
    // Update weapon pickups depending on wave progress.
    SWITCH serverBD.sWaveData.iWaveCount
        CASE 1 
            SWITCH iWeaponPickupCount
                CASE 0                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_MICROSMG                        	BREAK
                CASE 1                 iWeaponPickupCount++ RETURN PICKUP_WEAPON_COMBATPISTOL                      	BREAK
                CASE 2                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_MICROSMG                          BREAK
                CASE 3                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_COMBATPISTOL                      BREAK
                CASE 4                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_MICROSMG                          BREAK
                CASE 5                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_COMBATPISTOL                      BREAK
                CASE 6                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_MICROSMG                          BREAK
            ENDSWITCH
        BREAK
		CASE 2
            SWITCH iWeaponPickupCount
                CASE 0                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_MICROSMG                      	BREAK
                CASE 1                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ASSAULTRIFLE                     BREAK
                CASE 2                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_MICROSMG 						BREAK
                CASE 3                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ASSAULTRIFLE                     BREAK
                CASE 4                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_MICROSMG                      	BREAK
                CASE 5                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ASSAULTRIFLE                     BREAK
                CASE 6                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_MICROSMG                     	BREAK
            ENDSWITCH
        BREAK
		CASE 3
            SWITCH iWeaponPickupCount
                CASE 0                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_COMBATMG                      	BREAK
                CASE 1                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_COMBATMG                      	BREAK
                CASE 2                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_COMBATMG 						BREAK
                CASE 3                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_PUMPSHOTGUN                      BREAK
                CASE 4                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_COMBATMG                      	BREAK
                CASE 5                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_PUMPSHOTGUN                     	BREAK
                CASE 6                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_COMBATMG                     	BREAK
            ENDSWITCH
        BREAK
		CASE 4
            SWITCH iWeaponPickupCount
                CASE 0                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_CARBINERIFLE                     BREAK
                CASE 1                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
                CASE 2                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_RPG		 						BREAK
                CASE 3                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
                CASE 4                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_CARBINERIFLE                     BREAK
                CASE 5                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
                CASE 6                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_CARBINERIFLE                    	BREAK
            ENDSWITCH
        BREAK
		CASE 5
            SWITCH iWeaponPickupCount
                CASE 0                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_SAWNOFFSHOTGUN                   BREAK
                CASE 1                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
                CASE 2                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_SAWNOFFSHOTGUN 					BREAK
                CASE 3                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
                CASE 4                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_SAWNOFFSHOTGUN                   BREAK
                CASE 5                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                   	BREAK
                CASE 6                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_SAWNOFFSHOTGUN                   BREAK
            ENDSWITCH
        BREAK
		CASE 6
            SWITCH iWeaponPickupCount
                CASE 0                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
                CASE 1                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_HEAVYSNIPER                     	BREAK
                CASE 2                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE 					BREAK
                CASE 3                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_GRENADE                     		BREAK
                CASE 4                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
                CASE 5                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_GRENADE                   		BREAK
                CASE 6                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
            ENDSWITCH
        BREAK
		CASE 7
            SWITCH iWeaponPickupCount
                CASE 0                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
                CASE 1                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_HEAVYSNIPER                     	BREAK
                CASE 2                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ASSAULTSHOTGUN 					BREAK
                CASE 3                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_RPG	                     		BREAK
                CASE 4                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_CARBINERIFLE                     BREAK
                CASE 5                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_GRENADE                   		BREAK
                CASE 6                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
            ENDSWITCH
        BREAK
		CASE 8
            SWITCH iWeaponPickupCount
                CASE 0                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
                CASE 1                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_GRENADELAUNCHER                  BREAK
                CASE 2                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ASSAULTSHOTGUN 					BREAK
                CASE 3                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_GRENADE                     		BREAK
                CASE 4                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_CARBINERIFLE                     BREAK
                CASE 5                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_STICKYBOMB                   	BREAK
                CASE 6                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
            ENDSWITCH
        BREAK
		CASE 9
            SWITCH iWeaponPickupCount
                CASE 0                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
                CASE 1                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_GRENADELAUNCHER                  BREAK
                CASE 2                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ASSAULTSHOTGUN 					BREAK
                CASE 3                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_GRENADE                     		BREAK
                CASE 4                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_MINIGUN                     		BREAK
                CASE 5                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_STICKYBOMB                   	BREAK
                CASE 6                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
            ENDSWITCH
        BREAK
		CASE 10
            SWITCH iWeaponPickupCount
                CASE 0                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
                CASE 1                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_GRENADELAUNCHER                  BREAK
                CASE 2                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_MINIGUN 							BREAK
                CASE 3                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_RPG			                  BREAK
                CASE 4                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_MINIGUN                     		BREAK
                CASE 5                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_STICKYBOMB                   	BREAK
                CASE 6                 iWeaponPickupCount++ RETURN  PICKUP_WEAPON_ADVANCEDRIFLE                    BREAK
            ENDSWITCH
        BREAK
    ENDSWITCH
    
	#IF IS_DEBUG_BUILD
		tl63_debugPrint = "GET_PICKUP_TYPE_FOR_WAVE - iWaveCount = "
		tl63_debugPrint += serverBD.sWaveData.iWaveCount
		tl63_debugPrint += ", iPickup = "
		tl63_debugPrint += iPickup
		NET_SCRIPT_ASSERT(tl63_debugPrint)
	#ENDIF
    
    iWeaponPickupCount++ 
	RETURN PICKUP_WEAPON_SMG
    
ENDFUNC

PROC MAINTAIN_HAVE_ALL_PARTICIPANTS_LOST_ALL_LIVES_FLAG(INT i, INT iPlayerId, PARTICIPANT_INDEX partId)
    
	IF IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
	    IF NOT IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
	        IF GET_CLIENT_HORDE_STAGE(partId) != eHORDESTAGE_SCTV
				IF GET_CLIENT_HORDE_STAGE(partId) != eHORDESTAGE_SPECTATING
	                IF IS_BIT_SET(iPlayerIsNetOkBitset, iPlayerId)
	                    IF NOT IS_BIT_SET(iParticpantIsDeadBitset, i)
	                   		bAllPlayersLostAllLivesFlag = FALSE   
	                    ENDIF
	                ENDIF
	            ENDIF
	        ENDIF
		ENDIF
	ELSE
		bAllPlayersLostAllLivesFlag = FALSE  // Don't initiate fail while we are still sorting out a new player's fighting status. Only when all players have done this can we check the all dead fail condition.
	ENDIF
	
ENDPROC

FUNC BOOL ALLOW_FOR_WASTED_MESSAGE()

    SWITCH sAllowWastedMessageData.iWastedMessageStage
            
            CASE 0
                    
                IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_WASTED)
                    PRINTLN("[Survival] - [Wasted] - [Respawning] - wasted big message being displayed - going to stage 1.")
                    sAllowWastedMessageData.iWastedMessageStage++
                ENDIF
                    
            BREAK
            
            CASE 1
                    
                IF NOT HAS_NET_TIMER_STARTED(sAllowWastedMessageData.stWastedDisplayTime)
                    PRINTLN("[Survival] - [Wasted] - [Respawning] - started display wasted message timer.")
                    START_NET_TIMER(sAllowWastedMessageData.stWastedDisplayTime)
                ELSE
                    IF HAS_NET_TIMER_EXPIRED(sAllowWastedMessageData.stWastedDisplayTime, 4000)
                        PRINTLN("[Survival] - [Wasted] - [Respawning] - display wasted message timer expired.")
						CLEAR_ALL_BIG_MESSAGES()
						CLEANUP_KILL_STRIP()
						RETURN TRUE     
                    ENDIF
                ENDIF
                
                IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_WASTED)
                    PRINTLN("[Survival] - [Wasted] - [Respawning] - wasted message no longer being displayed.")
                    RETURN TRUE     
                ENDIF
                    
            BREAK
            
    ENDSWITCH
    
    RETURN FALSE
    
ENDFUNC

PROC RESET_ALLOW_WASTED_MESSAGE_DATA()
    
    STRUCT_ALLOW_WASED_MESSAGE_DATA sTemp
    
    sAllowWastedMessageData = sTemp
    RESET_NET_TIMER(sAllowWastedMessageData.stWastedDisplayTime)
    
ENDPROC

PROC RESET_RESPAWNING_DATA_CLIENT(INT iDebugCall = 0, AFTERLIFE eAfterlife = AFTERLIFE_SET_SPECTATORCAM)
	
	IF iDebugCall = iDebugCall
		iDebugCall = iDebugCall
	ENDIF
	
    RESET_ALLOW_WASTED_MESSAGE_DATA()
	
	IF GET_END_HORDE_REASON() = eENDHORDEREASON_NOT_FINISHED_YET
		SET_GAME_STATE_ON_DEATH(eAfterlife)
	ELSE
		SET_GAME_STATE_ON_DEATH(AFTERLIFE_MANUAL_RESPAWN)
		SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
	ENDIF
	
    iRespawningStage = 0
	
    PRINTLN("[Survival] - [Respawning] - RESET_RESPAWNING_DATA_CLIENT - call ", iDebugCall)
	
ENDPROC

PROC TRACK_LOCAL_PLAYER_DEATHS()
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		
		SWITCH iTrackDeathsStage
			
			CASE 0
				IF IS_ENTITY_DEAD(PLAYER_PED_ID())
					playerBD[PARTICIPANT_ID_TO_INT()].iDeaths++
					iTrackDeathsStage++
					PRINTLN("[Survival] - player dead iDeaths = ", playerBD[PARTICIPANT_ID_TO_INT()].iDeaths)
				ENDIF
			BREAK
			
			CASE 1
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					iTrackDeathsStage = 0
					PRINTLN("[Survival] - player alive again.")
				ENDIF
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
ENDPROC

PROC PROCESS_RESPAWNING()
    
    IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	
        SWITCH iRespawningStage
		
            CASE 0
				
				IF GET_END_HORDE_REASON() = eENDHORDEREASON_NOT_FINISHED_YET
                    IF IS_ENTITY_DEAD(PLAYER_PED_ID())
                        Clear_Any_Objective_Text_From_This_Script()
                        IF ALLOW_FOR_WASTED_MESSAGE()
							IF GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) = eHORDESTAGE_FIGHTING
	                            IF g_SpawnData.bSafeForSpectatorCam
									IF NOT bAllPlayersLostAllLivesFlag
		                                PRINTLN("[Survival] - [Respawning] - going to iRespawningStage 1")
										iRejoinFromSpecCamStage = 0
										PRINTLN("[Survival] - [Respawning] - iRejoinFromSpecCamStage = 0, call 0.")
		                                SET_CLIENT_HORDE_STAGE(eHORDESTAGE_SPECTATING)
		                                iRespawningStage++
									ELSE
										IF GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) != eHORDESTAGE_FIGHTING
											SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_AUTO_REJOIN)
											iRespawningStage++
										ELSE
											PRINTLN("[Survival] - [Respawning] - bAllPlayersLostAllLivesFlag = TRUE, waiting for server to move from eHORDESTAGE_FIGHTING stage.")
										ENDIF
									ENDIF
	                            ELSE
	                                PRINTLN("[Survival] - [Respawning] - local player is dead but g_SpawnData.bSafeForSpectatorCam = FALSE.")
	                            ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								PRINTLN("[Survival] - [Respawning] - no longer in the fighting state, need to just respawn.")
								PRINTLN("[Survival] - [Respawning] - setting to auto_rejoin as horde stage is ")GET_HORDE_STAGE_NAME(GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()))
								#ENDIF
								SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_AUTO_REJOIN)
								iRespawningStage++
							ENDIF
                        ELSE
                            PRINTLN("[Survival] - [Respawning] - waiting for ALLOW_FOR_WASTED_MESSAGE to return TRUE.")
                        ENDIF
                    ENDIF
                ENDIF
				
            BREAK
			
            CASE 1
				
                IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
                    PRINTLN("[Survival] - [Respawning] - local player alive gain, going to iRespawningStage 0")
                    RESET_RESPAWNING_DATA_CLIENT(2)
                ENDIF
				
            BREAK
			
        ENDSWITCH
		
    ENDIF

ENDPROC

PROC INIT_SQUAD_PED(INT iSquad, INT iPed)
	
	serverBD.structAiSquad[iSquad].sPed[iPed].sSquadIdData.iSquad = iSquad
	serverBD.structAiSquad[iSquad].sPed[iPed].sSquadIdData.iSquadSlot = iPed
	serverBD.structAiSquad[iSquad].sPed[iPed].bPlayedAngryShout = FALSE
	
ENDPROC

PROC INIT_SQUAD(INT iSquad)
	
	STRUCT_AI_SQUAD sTemp
	serverBD.structAiSquad[iSquad] = sTemp
	serverBD.structAiSquad[iSquad].iSquadId = iSquad
	
ENDPROC

PROC INITIALSE_ENEMIES()
	
	INT i, j
	
	REPEAT MAX_NUM_ENEMY_SQUADS j
		
		// Squad data.
		INIT_SQUAD(j)
		
		// Data for each ped in the squad.
		REPEAT MAX_NUM_PEDS_IN_SQUAD i
		
			INIT_SQUAD_PED(j, i)
			
		ENDREPEAT
		
	ENDREPEAT
	
ENDPROC

PROC RESET_LAND_VEHICLE_DATA()
	
	INT i
	
	REPEAT MAX_NUM_LAND_VEHICLES i
		serverBD.sSurvivalLandVehicle[i].iNumPeds = 0
		serverBD.sSurvivalLandVehicle[i].iActivatingStage = 0
	ENDREPEAT
	
ENDPROC

PROC RESET_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_TIMER()
	
	RESET_NET_TIMER(serverBd.stVehicleNotInAreaTimer)
	serverBd.bStartVehicleFailSafeTimer = FALSE
	serverBd.bAllVehiclePedsStuckOutOfArea = FALSE
	
ENDPROC

PROC RESET_DATA_FOR_NEW_WAVE()
    
	// New wave not defeated yet, haven't fought it yet!
    CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
    RESET_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_TIMER()
	RESET_NET_TIMER(serverBD.stNoSpawningEnemiesTimer)
				
	// Reset this timer.
	RESET_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
	
	// Reset all our enemies in prep for the new wave.
    INITIALSE_ENEMIES()
    
	// Reset all data for managing waves, new wave needs new data.
    serverBD.sWaveData.iNumRequiredKills            = 0
    serverBD.sWaveData.eStage                   	= eWAVESTAGE_EASY
    serverBD.sWaveData.iMaintainSquadsStage         = 0
    serverBD.iNumAliveSquadEnemies                	= 0
    serverBD.iNumAliveHelis                      	= 0
	serverBD.iNumAliveLandVehiclePeds				= 0
    serverBD.iKillsThisWave                       	= 0
	
    serverBD.iKillsThisWaveSubStage[eWAVESTAGE_EASY]    = 0
	serverBD.iKillsThisWaveSubStage[eWAVESTAGE_MEDIUM]  = 0
	serverBD.iKillsThisWaveSubStage[eWAVESTAGE_HARD]    = 0
	
	serverBD.iNumCreatedEnemies						= 0
    serverBd.iSearchingForPed                       = (-1)
	serverBd.iSearchingForChopper 					= (-1)
	serverBd.iSearchingForLandVehicle 				= (-1)
	serverBd.iNumKillsThisWaveFromEvents 			= 0
	
	serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_EASY]		= 0
	serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_MEDIUM]		= 0
	serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_HARD]		= 0
	
	RESET_LAND_VEHICLE_DATA()
	
ENDPROC

PROC SETUP_STRAIGHT_TO_SPECTATOR_PLAYER()
    NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
	SET_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
ENDPROC

PROC CLEAR_STRAIGHT_TO_SPECTATOR_PLAYER()
    NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
ENDPROC

PROC INCREMENT_WAVE()
    
    IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
        serverBD.sWaveData.iWaveCount++
    ENDIF
    
    SET_BIT(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
    
ENDPROC

FUNC INT GET_NUM_REQUIRED_KILLS_FOR_WAVE()
    
    PRINTLN("[Survival] - [Wave Progression] - GET_NUM_REQUIRED_KILLS_FOR_WAVE - being called.")
    
    RETURN serverBD.sWaveData.iNumRequiredKills
    
ENDFUNC

FUNC INT GET_FIGHTING_PARTICIPANTS_AVERAGE_RANK()
    
    INT i
    INT iTotalRank
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        
        IF serverBD.sFightingParticipantsList.iParticipant[i] != (-1)
			IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
	            IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.sFightingParticipantsList.iParticipant[i]))
	                iTotalRank += GET_PLAYER_RANK(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.sFightingParticipantsList.iParticipant[i])))
	            ENDIF
			ENDIF
        ENDIF
            
    ENDREPEAT
    
    RETURN (iTotalRank/serverBD.sFightingParticipantsList.iNumFightingParticipants)
    
ENDFUNC

PROC SETUP_NEXT_WAVE()
	
	// Reset data to initial values in preperation for setting them again for the new wave.
    RESET_DATA_FOR_NEW_WAVE()
	
//	// Get the wave management values. Num kills required for each wave, num squads for each wave substage etc.
//    POPULATE_WAVE_SQUAD_DATA()
	
	// If we ghave called this function once, the first will have been setup.
    SET_BIT(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
	
ENDPROC

PROC SET_LOCAL_PLAYER_CAN_COLLECT_HORDE_PICKUPS(BOOL bCanCollect = TRUE)

    INT i
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
        IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos)
            IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt != NUM_PICKUPS
                SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt, bCanCollect)
            ENDIF
        ENDIF
    ENDREPEAT

ENDPROC

PROC SET_ROADS(BOOL bActive)
    
	PRINTLN("[Survival] - SET_ROADS_IN_AREA - being called.")
	
	VECTOR vMin = (g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos-<<1000.0, 1000.0, 250.0>>)
	VECTOR vMax = (g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos+<<1000.0, 1000.0, 250.0>>)
	
	IF NOT bActive
		
		SET_ROADS_IN_AREA(vMin, vMax, FALSE, FALSE)
		
		PRINTLN("[Survival] - SET_ROADS - SET_ROADS_IN_AREA - set nodes off. Min = ", vMin, ", Max = ", vMax)
	
	ELSE
	
		SET_ROADS_BACK_TO_ORIGINAL(vMin, vMax)
									
		PRINTLN("[Survival] - SET_ROADS - SET_ROADS_BACK_TO_ORIGINAL - Min = ", vMin, ", Max = ", vMax)
	
	ENDIF
	
ENDPROC

PROC SET_PICKUP_TYPES_FOR_WAVE()
    
    INT i
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
        g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt =     GET_PICKUP_TYPE_FOR_WAVE(i)
    ENDREPEAT
    
	iWeaponPickupCount = 0
	
ENDPROC

FUNC INT GET_FMMC_END_OF_MISSION_STATUS_FOR_END_HORDE_REASON()
    
	IF NETWORK_IS_GAME_IN_PROGRESS()
	
	    IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
	        SWITCH GET_END_HORDE_REASON()
	            CASE eENDHORDEREASON_BEAT_ALL_WAVES RETURN ciFMMC_END_OF_MISSION_STATUS_PASSED
	            CASE eENDHORDEREASON_ALL_PLAYERS_DEAD RETURN ciFMMC_END_OF_MISSION_STATUS_FAILED
	        ENDSWITCH
	    ENDIF
    
	ENDIF
	
    RETURN ciFMMC_END_OF_MISSION_STATUS_CANCELLED
    
ENDFUNC

FUNC BOOL HAS_SURVIVAL_OUT_OF_BOUNDS_DATA_BEEN_SETUP()
	
	IF IS_VECTOR_ZERO(sSurvivalBoundsData.vMin)
		RETURN FALSE
	ENDIF
	
	IF IS_VECTOR_ZERO(sSurvivalBoundsData.vMax)
		RETURN FALSE
	ENDIF
	
	IF (sSurvivalBoundsData.fWidth = 0.0)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(BOOL bForStartMode)
	
	IF bForStartMode
		bForStartMode = bForStartMode
	ENDIF
	
//	IF bForStartMode
//	OR NOT HAS_SURVIVAL_OUT_OF_BOUNDS_DATA_BEEN_SETUP()
		SETUP_SPECIFIC_SPAWN_LOCATION(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos, 0.0, 20.0, TRUE, 2.5, FALSE, FALSE)        
		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS, TRUE)
//	ELSE
//		SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA(sSurvivalBoundsData.vMin, sSurvivalBoundsData.vMax, sSurvivalBoundsData.fWidth, g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos, TRUE, FALSE, FALSE)   
//		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA, TRUE)
//	ENDIF
//	
	SET_PLAYERS_RESPAWN_TO_SPAWN_CLOSER_TO_ROADS_ON_THIS_MISSION(FALSE)
	
	SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<-86.293831,-759.790771,43.217434>>, <<-78.534744,-786.002869,36.354721>>, 12.000000)
	
ENDPROC

PROC GIVE_WEAPON_TO_PED_AND_EQUIP(PED_INDEX pedId, WEAPON_TYPE weapontype, INT iNumberOfClips)

	INT iAmmoToGive = (GET_MAX_AMMO_IN_CLIP(pedId, weapontype) * iNumberOfClips)
	
	GIVE_WEAPON_TO_PED(pedId, weapontype, iAmmoToGive)
	SET_CURRENT_PED_WEAPON(pedId, weapontype, TRUE)
	
ENDPROC

PROC SORT_PLAYER_STARTING_WEAPONS()
	
	SWITCH g_FMMC_STRUCT.iStartingWeapon
	
		CASE SURVIVAL_STAR_WEAPON_PISTOL
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_APPISTOL)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL, 2)
			ELIF  GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_COMBATPISTOL)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL, 2)
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 2)
			ENDIF
		BREAK
		
		CASE SURVIVAL_STAR_WEAPON_SMG	
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_SMG)) > 0 
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_SMG, 2)
			ELSE 
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG, 2)
			ENDIF
		BREAK	
		
		CASE SURVIVAL_STAR_WEAPON_RIFLE
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_MG)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_MG, 2) 
			ELIF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_COMBATMG)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG, 2) 
			ELIF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_ADVANCEDRIFLE)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_ADVANCEDRIFLE, 2)
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 2) 
			ENDIF
		BREAK 
		
		CASE SURVIVAL_STAR_WEAPON_SNIPER
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_HEAVYSNIPER)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, 2)  
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, 2)  
			ENDIF
		BREAK 	
		
		CASE SURVIVAL_STAR_WEAPON_SHOT
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_ASSAULTSHOTGUN)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTSHOTGUN, 2) 
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_SAWNOFFSHOTGUN, 2) 
			ENDIF
		BREAK
		
		CASE SURVIVAL_STAR_WEAPON_HEAVY
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_GRENADELAUNCHER)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_GRENADELAUNCHER, 2)  
			ELIF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_RPG)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_RPG, 2)  
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN, 2)  
			ENDIF
		BREAK	
		
		CASE SURVIVAL_STAR_WEAPON_PROJECT
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_GRENADE)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 8)  
			ELIF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_STICKYBOMB)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 8)   
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 8)
			ENDIF
		BREAK 
		
		CASE SURVIVAL_STAR_WEAPON_ALL_UNLOCKED
			
			GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_KNIFE, 1)
			
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_APPISTOL)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL, 2)
			ELIF  GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_COMBATPISTOL)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL, 2)
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 2)
			ENDIF
			
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_SMG)) > 0 
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_SMG, 2)
			ELSE 
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG, 2)
			ENDIF
			
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_MG)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_MG, 2) 
			ELIF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_COMBATMG)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG, 2) 
			ELIF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_ADVANCEDRIFLE)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_ADVANCEDRIFLE, 2)
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 2) 
			ENDIF
			
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_HEAVYSNIPER)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, 2)  
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, 2)  
			ENDIF
			
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_ASSAULTSHOTGUN)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTSHOTGUN, 2) 
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_SAWNOFFSHOTGUN, 2) 
			ENDIF
			
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_GRENADELAUNCHER)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_GRENADELAUNCHER, 2)  
			ELIF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_RPG)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_RPG, 2)  
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN, 2)  
			ENDIF
			
			IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_GRENADE)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 2)  
			ELIF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(WEAPONTYPE_STICKYBOMB)) > 0
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 2)   
			ELSE
				GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 2)
			ENDIF
			
		BREAK 
		
		CASE SURVIVAL_STAR_WEAPON_NONE
			GIVE_WEAPON_TO_PED_AND_EQUIP(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, 1)
		BREAK
		
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Loads text required for the Horde script.
/// RETURNS:
///    TRUE if text has been loaded, FALSE if not.
FUNC BOOL LOAD_HORDE_TEXT_BLOCK()
	
	REQUEST_ADDITIONAL_TEXT("FMHRD", MISSION_TEXT_SLOT)
	
    IF HAS_THIS_ADDITIONAL_TEXT_LOADED("FMHRD", MISSION_TEXT_SLOT)
        PRINTLN("[Survival] - loaded text FMHRD into MISSION_TEXT_SLOT")
        RETURN TRUE
    ELSE
        PRINTLN("[Survival] - loading text FMHRD into MISSION_TEXT_SLOT")
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

PROC HIDE_PLAYER(BOOL bHide)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), (NOT bHide))
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), bHide)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), bHide)
	ENDIF
	
ENDPROC

PROC SETUP_FIGHTING_PARTICIPANT_LIST_DEFAULT_VALUES()

	INT i

    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        serverBD.sFightingParticipantsList.iParticipant[i] = (-1)
    ENDREPEAT
    
ENDPROC

BOOL bSetBlockingAreas

/// PURPOSE:
///    Process pre game logic. Called once at beginning of script.
/// PARAMS:
///    structHordeMissionData - mission data.
/// RETURNS:
///    FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA &structHordeMissionData)

	INT i, j
	MODEL_NAMES gangModel
	
	//gdisablerankupmessage = TRUE
	SET_DISABLE_RANK_UP_MESSAGE(TRUE)
	
	serverBD.sServerFMMC_EOM.bAllowedToVote = FALSE
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_SURVIVAL
	FMMC_PROCESS_PRE_GAME_COMMON(structHordeMissionData, serverBD.sFMMC_SBD.iPlayerMissionToLoad, serverBD.sFMMC_SBD.iMissionVariation,NUM_NETWORK_PLAYERS, FALSE, FALSE)
	
	// For weapons in respawning.
	g_bOnHorde = TRUE
	
	WHILE NOT bSkipTaxiScriptCheck
		WAIT(0)
		IF NOT HAS_NET_TIMER_STARTED(stTaxtTerminateFailsafe)
			START_NET_TIMER(stTaxtTerminateFailsafe)
			PRINTLN("[Survival] - PROCESS_PRE_GAME - stTaxtTerminateFailsafe timer started.")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(stTaxtTerminateFailsafe, 10000)
				bSkipTaxiScriptCheck = TRUE
				PRINTLN("[Survival] - PROCESS_PRE_GAME - stTaxtTerminateFailsafe timer expired.")
			ENDIF
		ENDIF
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_TAXI", DEFAULT_MISSION_INSTANCE, TRUE)
		AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_CRATE_DROP", DEFAULT_MISSION_INSTANCE, TRUE)
			PRINTLN("[Survival] - PROCESS_PRE_GAME - AM_TAXI.sc and AM_CRATE_DROP.sc are no longer active! ")
			bSkipTaxiScriptCheck = TRUE
		ENDIF
	ENDWHILE
	
	SETUP_FIGHTING_PARTICIPANT_LIST_DEFAULT_VALUES()
	
	// Reserve!
	RESERVE_NETWORK_MISSION_PEDS(36) // Max num respawnable squad enemies + helicopter enemies + overlap enemies to keep end of waves interesting.
	RESERVE_NETWORK_MISSION_VEHICLES(MAX_NUM_SURVIVAL_HELIS+MAX_NUM_LAND_VEHICLES) // Helicopters.
	
	// For heli spawning, give each one a different location on the survival area circumference so they dopn;t spawn too near each other.
	serverBD.sSurvivalHeli[0].iCircCount = SURVIVAL_HELI_0_START_CIRC_COUNT
	serverBD.sSurvivalHeli[1].iCircCount = SURVIVAL_HELI_1_START_CIRC_COUNT
	serverBD.sSurvivalHeli[2].iCircCount = SURVIVAL_HELI_2_START_CIRC_COUNT
	
	DISABLE_CUSTOM_WEAPON_LOADOUT_FOR_MISSIONS()
	
	RESET_SERVER_NEXT_JOB_VARS()
	// Register broadcast data.
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(g_sMC_serverBDEndJob, SIZE_OF(g_sMC_serverBDEndJob))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))

	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	//      SET_FM_MISSION_LAUNCHED_SUCESS(structHordeMissionData.iFMCreatorID, structHordeMissionData.missionVariation, structHordeMissionData.iInstanceId)
	PRINTLN("[Survival] - PROCESS_PRE_GAME - iCurrentMissionType = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType)

	//CREATE_FM_MISSION_RELATIONSHIP_GROUPS()
//	CREATE_FM_DEFAULT_RELATIONSHIP_GROUPS()
	
	CAP_EDITOR_VARIABLES()

	CLEAR_HELP(TRUE)
	
	NETWORK_OVERRIDE_TEAM_RESTRICTIONS(0, TRUE)
	
	LOAD_HORDE_TEXT_BLOCK()
	
	TOGGLE_STRIPPER_AUDIO(FALSE)
	
	// B*1600250.
	IF INCREMENT_STARTING_ACTIVITY_CHEAT_STAT_BY() > 0
		INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_SURVIVAL_START)
		SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_SURVIVAL_STARTED)
		PRINTLN("[Survival] - SCRIPT_CLEANUP - called SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_SURVIVAL_STARTED).")
	ENDIF

	// B*1618888. 
	SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
	SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
	
	PRINTLN("[Survival] - PROCESS_PRE_GAME - player team = ", g_FMMC_STRUCT.iMyTeam)
	IF g_FMMC_STRUCT.iMyTeam != 0
	    g_FMMC_STRUCT.iMyTeam = 0
	    PRINTLN("[Survival] - PROCESS_PRE_GAME - player team now = ", g_FMMC_STRUCT.iMyTeam)
	ENDIF

	gangModel = GET_GANG_MODEL_FOR_AREA(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos, FALSE)
	
	REPEAT FMMC_MAX_PEDS i
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].mn = DUMMY_MODEL_FOR_SCRIPT)
			PRINTLN("[Survival] - PROCESS_PRE_GAME - model set in creator = dummy model, setting ped ", i, " model to gang model for area.")
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = gangModel
		ELSE
			PRINTLN("[Survival] - PROCESS_PRE_GAME - model set in creator = valid model, setting ped ", i, " to this model.")
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].mn
		ENDIF
	ENDREPEAT
	
	iStartHits = GET_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
	iStartShots = GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOTS)

	// Setup land vehicle ped models.
	REPEAT MAX_NUM_LAND_VEHICLES i
		REPEAT NUM_LAND_VEH_PEDS j
			serverBD.sSurvivalLandVehicle[i].sPed[j].eModel = g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].mn
		ENDREPEAT
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	REPEAT 10 i
		PRINTLN("[Survival] - placed vehicle ", i, " = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos)
	ENDREPEAT
	#ENDIF
	
	SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(TRUE)
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		SET_PLAYER_TEAM(PLAYER_ID(), 0)
	ENDIF
	//      SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgfm_PlayerTeam[g_FMMC_STRUCT.iMyTeam])
	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
	
	DISABLE_MP_PASSIVE_MODE(PMER_START_OF_A_MATCH)
	NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
    	IF NOT IS_ENTITY_DEAD((PLAYER_PED_ID()))
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
			SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID()))
		ENDIF
	ENDIF
	
	SET_MAX_WANTED_LEVEL(0)
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
	
	g_FMMC_STRUCT_ENTITIES.iPedRespawnTime = FMMC_PED_RESPAWN_TIME_RANDOM

	#IF IS_DEBUG_BUILD
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos)
	    NET_SCRIPT_ASSERT("[WJK] - Survival Mode - g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos = << 0.0, 0.0, 0.0 >>")
	ENDIF
	
	PRINTLN("[Survival] - g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos = ", g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos)
	
	REPEAT FMMC_MAX_PEDS i
		IF i < (MAX_NUM_ENEMY_SQUADS)
			PRINTLN("[Survival] - g_FMMC_STRUCT_ENTITIES.sPlacedPed[", i, "].vPos = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos)
			IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos)
				TEXT_LABEL_63 tl63
				tl63 = "sPlacedPed["
				tl63 += i
				tl63 += "] is zero. Cloud error?"
				NET_SCRIPT_ASSERT(tl63)
			ENDIF
		ENDIF
	ENDREPEAT
	
	#ENDIF
	
	IF NOT bSetBlockingAreas
		
		IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS((g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos-<<1000.0, 1000.0, 250.0>>), 
									(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos+<<1000.0, 1000.0, 250.0>>))
			sbiScenarioBlockingIndex = ADD_SCENARIO_BLOCKING_AREA(	(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos-<<1000.0, 1000.0, 250.0>>), 
										(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos+<<1000.0, 1000.0, 250.0>>), FALSE, TRUE, TRUE, TRUE)
		ELSE
			PRINTLN("[Survival] ADD_SCENARIO_BLOCKING_AREA - failed - blocking area already exists")
		ENDIF
									
		iPopMultArea = ADD_POP_MULTIPLIER_AREA(	(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos-<<1000.0, 1000.0, 250.0>>), 
									(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos+<<1000.0, 1000.0, 250.0>>), 
									0, 0, TRUE)
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(	(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos-<<1000.0, 1000.0, 250.0>>), 
													(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos+<<1000.0, 1000.0, 250.0>>),
													FALSE,FALSE)
	
		bSetBlockingAreas = TRUE
		
	ENDIF
	
	IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_JIP_IN
		CLEAR_AREA(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos, 2000.0, FALSE, TRUE, FALSE, FALSE)
	ENDIF
	
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)

//	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
//	    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//	        GET_PED_WEAPONS_MP(PLAYER_PED_ID(), structPlayerWeapons)
//	        REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
//	        GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 300, TRUE)
//	    ENDIF
//	ENDIF

	sEndBoardData.iScores[0] = (-1)
	sEndBoardData.iScores[1] = (-1)
	sEndBoardData.iScores[2] = (-1)
	sEndBoardData.iScores[3] = (-1)
	
	SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	
	// Fix for B*1661774 under instruction of Ryan B. 
	CLIENT_SET_UP_PROXIMITY_CHAT(CHAT_PROXIMITY_MAX)
	
	// Host only functions.
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		// For UGC.
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchHistoryId[1], serverBD.iMatchHistoryId[0])
		
		// Squad assignment.
		INITIALSE_ENEMIES()
		
	ENDIF
	
	// Setup spawning.
	SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(TRUE)
	
	// The survival area bounds.
	SETUP_SURVIVAL_BOUNDS_DATA()
	
	// So fleeing ambient peds don't drive through the middle of the survival shootout.
	PRINTLN("[Survival] - SET_ROADS_IN_AREA - about to be called. bTurnedOffRoadNodes = ", bTurnedOffRoadNodes)
	IF NOT bTurnedOffRoadNodes
		SET_ROADS(FALSE)
		bTurnedOffRoadNodes = TRUE
	ENDIF
	
	// Do initial setup of pickups for wave 1.
	SET_PICKUP_TYPES_FOR_WAVE()
	
	// Setup healthbar tags.
	g_iOverheadNamesState = g_FMMC_STRUCT.iHealthBar
//	
//	// Give player corona option starting weapons.
//	SORT_PLAYER_STARTING_WEAPONS()
//	
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
	
	g_bBlockAIKillXP = TRUE
	
	HIDE_PLAYER(TRUE)
	
	IF DID_I_JOIN_MISSION_AS_SPECTATOR()
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(500)
		ENDIF
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.iWeather = g_FMMC_STRUCT.iWeather	
		PRINTLN("[Survival] serverBD.iWeather=",serverBD.iWeather) 
		serverBD.iTimeOfDay = g_FMMC_STRUCT.iTimeOfDay	
		PRINTLN("[Survival] serverBD.iTimeOfDay=",serverBD.iTimeOfDay)
	ENDIF
	
	// Set the weather.
	SET_WEATHER_FOR_FMMC_MISSION(serverBD.iWeather)
	
	// If we're entering the mission at the beginning, set tod to corona option.
	IF GET_SERVER_HORDE_STAGE() <= eHORDESTAGE_START_FADE_IN
        CLIENT_OVERRIDE_CLOCK_TIME(serverBD.iTimeOfDay)
	ENDIF
	
	IF GET_SERVER_HORDE_STAGE() <= eHORDESTAGE_START_FADE_IN
		SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_INIT)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC INT GET_PARTICIPANT_SCORE(INT iParticipant, BOOL bGetTotalKills = FALSE)
    
    IF NOT bGetTotalKills
//      PRINTLN("[Survival] - GET_PARTICIPANT_SCORE - bGetTotalKills = FALSE - iMyKillsThisWave = ", playerBD[iParticipant].iMyKillsThisWave)
        RETURN playerBD[iParticipant].iMyKillsThisWave
    ENDIF
    
//  PRINTLN("[Survival] - GET_PARTICIPANT_SCORE - bGetTotalKills = TRUE - iMyKills = ", playerBD[iParticipant].iMyKills)
    RETURN playerBD[iParticipant].iMyKills
    
ENDFUNC

FUNC BOOL IS_PED_AT_HORDE_SHOOTOUT(PED_INDEX pedId)
    
	IF HAS_SURVIVAL_OUT_OF_BOUNDS_DATA_BEEN_SETUP()
		IF NOT IS_ENTITY_IN_ANGLED_AREA(pedId, sSurvivalBoundsData.vMin, sSurvivalBoundsData.vMax, sSurvivalBoundsData.fWidth)
			RETURN FALSE
		ENDIF
	ELSE
        IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedId), g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos) >= AT_HORDE_DISTANCE
            RETURN FALSE
        ENDIF
	ENDIF
	
    RETURN TRUE
    
ENDFUNC

PROC MAINTAIN_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_FAILSAFE()
	
	INT iPed, iVehicle
	
	serverBd.bStartVehicleFailSafeTimer = FALSE
	
	IF (serverBd.iNumAliveSquadEnemies <= 0)
	AND (serverBD.iNumAliveHelis <= 0)
		REPEAT MAX_NUM_LAND_VEHICLES iVehicle
			IF IS_NET_VEHICLE_DRIVEABLE(serverBd.sSurvivalLandVehicle[iVehicle].netId)
				REPEAT serverBd.sSurvivalLandVehicle[iVehicle].iNumPeds iPed
					IF NOT IS_NET_PED_INJURED(serverBd.sSurvivalLandVehicle[iVehicle].sPed[iPed].netId)
						IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverBd.sSurvivalLandVehicle[iVehicle].sPed[iPed].netId))
							IF NOT IS_PED_AT_HORDE_SHOOTOUT(NET_TO_PED(serverBd.sSurvivalLandVehicle[iVehicle].sPed[iPed].netId))
								IF NOT serverBd.bStartVehicleFailSafeTimer
									PRINTLN("[Survival] - set bStartVehicleFailSafeTimer for vehicle ", iVehicle, ", ped ", iPed)
									serverBd.bStartVehicleFailSafeTimer = TRUE
								ENDIF
							ENDIF		
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF serverBd.bStartVehicleFailSafeTimer
		IF NOT HAS_NET_TIMER_STARTED(serverBd.stVehicleNotInAreaTimer)
			START_NET_TIMER(serverBd.stVehicleNotInAreaTimer)
			PRINTLN("[Survival] - bStartVehicleFailSafeTimer = TRUE, starting timer stVehicleNotInAreaTimer.")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(serverBd.stVehicleNotInAreaTimer, 60000)
				PRINTLN("[Survival] - set stVehicleNotInAreaTimer been running for 30 seconds, setting bAllVehiclePedsStuckOutOfArea = TRUE.")
				serverBd.bAllVehiclePedsStuckOutOfArea = TRUE
			ENDIF
		ENDIF
	ELSE
		RESET_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_TIMER()
	ENDIF
	
ENDPROC

PROC GET_PLAYER_KILLS(STRUCT_HORDE_BOARD_DATA &sBoardData, BOOL bGetTotalKills = FALSE)
    
    INT i
    INT iIndex
    INT iScore
    
	IF NETWORK_IS_GAME_IN_PROGRESS()
	    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
			PRINTLN("[Survival] - [Wave Progression] - checking participant list index ", i)
	        IF ( serverBD.sFightingParticipantsList.iParticipant[i] > (-1) )
				PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] > (-1)")
				IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
					PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] is not just a potential fighter.")
		            IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.sFightingParticipantsList.iParticipant[i]))
						PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] is an active participant.")
						IF IS_BIT_SET(playerBD[serverBD.sFightingParticipantsList.iParticipant[i]].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
							PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] has completed the initial fighter status setup.")
			                IF NOT IS_BIT_SET(playerBD[serverBD.sFightingParticipantsList.iParticipant[i]].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
								PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] has completed the initial fighter status setup.")
			                    IF GET_CLIENT_HORDE_STAGE(INT_TO_PARTICIPANTINDEX(serverBD.sFightingParticipantsList.iParticipant[i])) != eHORDESTAGE_SCTV
									PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] is not sctv.")
			                        IF iIndex <= 3
										
										PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "], index = ", iIndex)
										
			                            sBoardData.iScores[iIndex] = (-1) // Do this to invalidate each score first. This makes sure 0 is considered a valid score in leaderboard sorting and is put above unitialised scores.
										
			                            iScore = GET_PARTICIPANT_SCORE(serverBD.sFightingParticipantsList.iParticipant[i], bGetTotalKills)
										
			                            sBoardData.iScores[iIndex]				= iScore
										sBoardData.iHeadshots[iIndex]			= playerBD[i].iMyHeadshots
										sBoardData.iTeam[iIndex]				= 0
										sBoardData.iDeaths[iIndex]				= playerBD[i].iDeaths
										sBoardData.iHeadshots[iIndex]			= playerBD[i].iMyHeadshots
										
			                            PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - participant ", i, " iScore = ", iScore, ", storing in sBoardData.iScores index ", iIndex)
			                            iIndex++
			                            PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - sBoardData.iScores index incremented to ", iIndex)
			                        ENDIF
			                    ENDIF
			                ENDIF
						ENDIF
		            ENDIF
				ENDIF
	        ENDIF
	    ENDREPEAT
    ENDIF
	
ENDPROC

FUNc BOOL HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(STRUCT_HORDE_BOARD_DATA &sBoardData, PLAYER_INDEX playerId)
    
    INT i
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
      	IF sBoardData.hudPlayers[i] = playerId
        	RETURN TRUE
      	ENDIF
    ENDREPEAT
    
    RETURN FALSE
    
ENDFUNC

PROC POPULATE_HUD_PLAYER_ARRAY(STRUCT_HORDE_BOARD_DATA &sBoardData, BOOL bDoDebugPrints = FALSE, BOOL bTotalModeScore = FALSE)
    
    INT i, j
    INT ilow1 = -1
    INT ihigh1 = -1
    INT imiddle1 = -1
    INT ilow2 = -1
    INT ihigh2 = -1
    INT ilowest = -1
    INT ihighest = -1
    INT imiddle2 = -1
    
    PARTICIPANT_INDEX partId
    PLAYER_INDEX playerId
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        sBoardData.hudPlayers[i] = INVALID_PLAYER_INDEX()
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayersindex ", i, " = ", NATIVE_TO_INT(sBoardData.hudPlayers[i]))
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.iScores index ", i, " = ", sBoardData.iScores[i])
        ENDIF
    ENDREPEAT
    
    IF sBoardData.iScores[0] < sBoardData.iScores[1] 
	    ilow1 = sBoardData.iScores[0]
	    ihigh1 = sBoardData.iScores[1]
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.iScores[0] < sBoardData.iScores[1]:")
            PRINTLN("[Survival] - [Hud Player Array] - ilow1 = sBoardData.iScores[0]")
            PRINTLN("[Survival] - [Hud Player Array] - ihigh1 = sBoardData.iScores[1]")
        ENDIF
    ELSE
	    ilow1 = sBoardData.iScores[1]
	    ihigh1 = sBoardData.iScores[0]
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.iScores[0] >= sBoardData.iScores[1]:")
            PRINTLN("[Survival] - [Hud Player Array] - ilow1 = sBoardData.iScores[1]")
            PRINTLN("[Survival] - [Hud Player Array] - ihigh1 = sBoardData.iScores[0]")
        ENDIF
   ENDIF
    
    IF sBoardData.iScores[2] < sBoardData.iScores[3] 
	    ilow2 = sBoardData.iScores[2]
	    ihigh2 = sBoardData.iScores[3]
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.iScores[2] < sBoardData.iScores[3]:")
           	PRINTLN("[Survival] - [Hud Player Array] - ilow2 = sBoardData.iScores[2]")
            PRINTLN("[Survival] - [Hud Player Array] - ihigh2 = sBoardData.iScores[3]")
        ENDIF
    ELSE
	    ilow2 = sBoardData.iScores[3]
	    ihigh2 = sBoardData.iScores[2]
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.iScores[2] >= sBoardData.iScores[3]:")
            PRINTLN("[Survival] - [Hud Player Array] - ilow2 = sBoardData.iScores[3]")
            PRINTLN("[Survival] - [Hud Player Array] - ihigh2 = sBoardData.iScores[2]")
        ENDIF
   ENDIF
    
    IF ilow1 < ilow2
	    ilowest = ilow1
	    imiddle1 = ilow2
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - ilow1 < ilow2:")
            PRINTLN("[Survival] - [Hud Player Array] - ilowest = ilow1")
           	PRINTLN("[Survival] - [Hud Player Array] - imiddle1 = ilow2")
        ENDIF
    ELSE
	    ilowest = ilow2
	    imiddle1 = ilow1
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - ilow1 >= ilow2:")
            PRINTLN("[Survival] - [Hud Player Array] - ilowest = ilow2")
            PRINTLN("[Survival] - [Hud Player Array] - imiddle1 = ilow1")
        ENDIF
    ENDIF
    
    IF ihigh1 > ihigh2
    	ihighest = ihigh1
   		imiddle2 = ihigh2
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - ihigh1 > ihigh2:")
            PRINTLN("[Survival] - [Hud Player Array] - ihighest = ihigh1")
            PRINTLN("[Survival] - [Hud Player Array] - imiddle2 = ihigh2")
        ENDIF
    ELSE
	    ihighest = ihigh2
	    imiddle2 = ihigh1
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - ihigh1 <= ihigh2:")
            PRINTLN("[Survival] - [Hud Player Array] - ihighest = ihigh2")
            PRINTLN("[Survival] - [Hud Player Array] - imiddle2 = ihigh1")
        ENDIF
    ENDIF
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS j
	
        IF ( serverBD.sFightingParticipantsList.iParticipant[j] != (-1) )
		
            i = serverBD.sFightingParticipantsList.iParticipant[j]
			
            IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				
				IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[j])
				OR NOT bTotalModeScore
					
                	partId = INT_TO_PARTICIPANTINDEX(i)
				
					IF IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
		                IF NOT IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
						
		                    IF GET_CLIENT_HORDE_STAGE(partId) != eHORDESTAGE_SCTV
		                        IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(partId), FALSE)
								
		                            playerId = NETWORK_GET_PLAYER_INDEX(partId)
									
		                            IF GET_PARTICIPANT_SCORE(i, bTotalModeScore) = ihighest
		                                IF bDoDebugPrints
		                                PRINTLN("[Survival] - [Hud Player Array] - participant ", i, " GET_PARTICIPANT_SCORE = ", ihighest, " = ihighest")
		                                ENDIF
		                                IF sBoardData.hudPlayers[0] = INVALID_PLAYER_INDEX()
		                                    IF bDoDebugPrints
		                                    PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[0] = INVALID_PLAYER_INDEX()")
		                                    ENDIF
		                                    IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                        IF bDoDebugPrints
		                                        PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[0].")
		                                    ENDIF
		                                        sBoardData.hudPlayers[0] = playerId
		                                        sBoardData.iScores[0] = ihighest
		                                    ENDIF
		                                ENDIF
		                            ENDIF
									
		                            IF GET_PARTICIPANT_SCORE(i, bTotalModeScore) = imiddle1
		                                IF bDoDebugPrints
		                                PRINTLN("[Survival] - [Hud Player Array] - participant ", i, " playerBD[i].iMyKillsThisWave = ", playerBD[i].iMyKillsThisWave, " = imiddle1")
		                                ENDIF
		                                IF imiddle1 > imiddle2
		                                    IF bDoDebugPrints
		                                    PRINTLN("[Survival] - [Hud Player Array] - imiddle1 > imiddle2.")
		                                    ENDIF
		                                    IF sBoardData.hudPlayers[1] = INVALID_PLAYER_INDEX()
		                                        IF bDoDebugPrints
		                                        PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[1] = INVALID_PLAYER_INDEX()")
		                                        ENDIF
		                                        IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                            IF bDoDebugPrints
			                                            PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[1].")
			                                  		ENDIF
		                                            sBoardData.hudPlayers[1] = playerId
		                                            sBoardData.iScores[1] = imiddle1
		                                        ENDIF
		                                    ENDIF
		                                ELSE
		                                    IF bDoDebugPrints
		                                    PRINTLN("[Survival] - [Hud Player Array] - imiddle1 <= imiddle2.")
		                                    ENDIF
		                                    IF sBoardData.hudPlayers[2] = INVALID_PLAYER_INDEX()
		                                        IF bDoDebugPrints
		                                        PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[2] = INVALID_PLAYER_INDEX()")
		                                        ENDIF
		                                        IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                            IF bDoDebugPrints
		                                            PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[2].")
		                           					ENDIF
		                                            sBoardData.hudPlayers[2] = playerId
		                                            sBoardData.iScores[2] = imiddle1
		                                        ENDIF
		                                    ENDIF
		                                ENDIF
		                            ENDIF
									
		                            IF GET_PARTICIPANT_SCORE(i, bTotalModeScore) = imiddle2
		                                IF bDoDebugPrints
		                                PRINTLN("[Survival] - [Hud Player Array] - participant ", i, " playerBD[i].iMyKillsThisWave = ", playerBD[i].iMyKillsThisWave, " = imiddle2")
		                                ENDIF
		                                IF imiddle1 > imiddle2
		                                    IF bDoDebugPrints
		                                    PRINTLN("[Survival] - [Hud Player Array] - imiddle1 > imiddle2.")
		                                    ENDIF
		                                    IF sBoardData.hudPlayers[2] = INVALID_PLAYER_INDEX()
		                                        IF bDoDebugPrints
		                                        PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[2] = INVALID_PLAYER_INDEX()")
		                                        ENDIF
		                                        IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                            IF bDoDebugPrints
		                                            PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[2].")
		                            				ENDIF
		                                            sBoardData.hudPlayers[2] = playerId
		                                            sBoardData.iScores[2] = imiddle2
		                                        ENDIF
		                                    ENDIF
		                            	ELSE
		                                    IF bDoDebugPrints
		                                    PRINTLN("[Survival] - [Hud Player Array] - imiddle1 <= imiddle2.")
		                                    ENDIF
		                                    IF sBoardData.hudPlayers[1] = INVALID_PLAYER_INDEX()
		                                        IF bDoDebugPrints
		                                        PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[1] = INVALID_PLAYER_INDEX()")
		                                        ENDIF
		                                        IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                            IF bDoDebugPrints
		                                            PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[1].")
		                           					ENDIF
		                                            sBoardData.hudPlayers[1] = playerId
		                                            sBoardData.iScores[1] = imiddle2
		                                        ENDIF
		                                    ENDIF
		                           		ENDIF
		                            ENDIF
									
		                        	IF GET_PARTICIPANT_SCORE(i, bTotalModeScore) = ilowest
		                               	IF bDoDebugPrints
		                              	PRINTLN("[Survival] - [Hud Player Array] - participant ", i, " playerBD[i].iMyKillsThisWave = ", playerBD[i].iMyKillsThisWave, " = ilowest")
		                               	ENDIF
		                               	IF sBoardData.hudPlayers[3] = INVALID_PLAYER_INDEX()
		                                   	IF bDoDebugPrints
		                                  	PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[3] = INVALID_PLAYER_INDEX()")
		                                  	ENDIF
		                                   	IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                       	IF bDoDebugPrints
		                                      	PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[3].")
		                                        ENDIF
		                                       	sBoardData.hudPlayers[3] = playerId
		                                       	sBoardData.iScores[3] = ilowest
		                                   	ENDIF
		                               	ENDIF
		                        	ENDIF
									
		                        ENDIF
		                    ENDIF
						ENDIF
					ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    #IF IS_DEBUG_BUILD
		IF bDoDebugPrints
        PRINTLN("[Survival] - [Hud Player Array] - ****************************************************************************")
        PRINTLN("[Survival] - [Hud Player Array] - 								FINAL POPULATED DATA ")
        PRINTLN("[Survival] - [Hud Player Array] - ****************************************************************************")
        REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
            IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - 						sBoardData.hudPlayersindex ", i, " = ", NATIVE_TO_INT(sBoardData.hudPlayers[i]))
            PRINTLN("[Survival] - [Hud Player Array] - 						sBoardData.iScores ", i, " = ", sBoardData.iScores[i])
            ENDIF
        ENDREPEAT
		PRINTLN("[Survival] - [Hud Player Array] - ****************************************************************************")
		ENDIF
    #ENDIF
    
ENDPROC

PROC CLEAR_HUD_PLAYER_ARRAY(STRUCT_HORDE_BOARD_DATA &sBoardData)
    
    STRUCT_HORDE_BOARD_DATA sTemp
    sBoardData = sTemp
    
    sBoardData.iScores[0] = (-1)
    sBoardData.iScores[1] = (-1)
    sBoardData.iScores[2] = (-1)
    sBoardData.iScores[3] = (-1)
    
ENDPROC

PROC DO_END_OF_MODE_LEADERBOARD_DATA_SORT()
	
	IF NOT sEndBoardData.bPopulatedHudPlayerArray
		GET_PLAYER_KILLS(sEndBoardData, TRUE)
		POPULATE_HUD_PLAYER_ARRAY(sEndBoardData, bDoWaveCompleteHudPrints, TRUE)
		sEndBoardData.bPopulatedHudPlayerArray = TRUE
	ENDIF
				
ENDPROC

FUNC INT GET_MY_LEADERBOARD_POSITION_FOR_PLAYSTATS()
	
	INT i
	
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
		IF sEndBoardData.hudPlayers[i] = PLAYER_ID()
			RETURN (i+1)
		ENDIF
	ENDREPEAT
	
	RETURN 0
	
ENDFUNC

FUNC BOOL IS_SCORE_CORONA_OPTION_ON()
	
	RETURN (g_FMMC_STRUCT.iRadio = SURVIVAL_SCORE_ON)
	
ENDFUNC

FUNC BOOL CLEANUP_ALL_ENEMIES()
	
	INT i, j
	BOOL bClean = TRUE
	
	REPEAT MAX_NUM_ENEMY_SQUADS j
		REPEAT MAX_NUM_PEDS_IN_SQUAD i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.structAiSquad[j].sPed[i].netId)
				CLEANUP_NET_ID(serverBD.structAiSquad[j].sPed[i].netId)
				bClean = FALSE
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	REPEAT MAX_NUM_SURVIVAL_HELIS i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalHeli[i].netId)
			CLEANUP_NET_ID(serverBD.sSurvivalHeli[i].netId)
			bClean = FALSE
		ENDIF
		REPEAT NUM_HELI_PEDS j
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalHeli[i].sPed[j].netId)
				CLEANUP_NET_ID(serverBD.sSurvivalHeli[i].sPed[j].netId)
				bClean = FALSE
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	REPEAT MAX_NUM_LAND_VEHICLES j
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalLandVehicle[j].netId)
			CLEANUP_NET_ID(serverBD.sSurvivalLandVehicle[j].netId)
			bClean = FALSE
		ENDIF
		REPEAT NUM_LAND_VEH_PEDS i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalLandVehicle[j].sPed[i].netId)
				CLEANUP_NET_ID(serverBD.sSurvivalLandVehicle[j].sPed[i].netId)
				bClean = FALSE
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	RETURN bClean
	
ENDFUNC


/// PURPOSE:
///    Performs script cleanup. Should be called upon script termination.
PROC SCRIPT_CLEANUP()
    
    INT i, j
    STRING sEvent
	
	#IF FEATURE_GEN9_EXCLUSIVE
	END_SERIES_UDS_ACTIVITY(g_FMMC_STRUCT.iRootContentIDHash)
	#ENDIF
	
	SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
	
	//gdisablerankupmessage = FALSE
	SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	
	UNLOAD_ALL_COMMON_LEADERBOARD_ELEMENTS(LeaderboardPlacement)
	CLEANUP_END_JOB_VOTING(nextJobStruct)
	
	CLEAR_RANK_REDICTION_DETAILS() 
	
	NETWORK_OVERRIDE_TEAM_RESTRICTIONS(0, FALSE)
	
	// Bugstar 2218090.
	SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(FALSE)
	
	CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
	SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
	SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(FALSE)
	ANIMPOSTFX_STOP("MP_Celeb_Win")
	ANIMPOSTFX_STOP("MP_Celeb_Lose")
	
	PRINTLN("[Survival] - stopping MP_Celeb_Win, call 0")
	
	IF DOES_CAM_EXIST(camCelebrationScreen)
		DESTROY_CAM(camCelebrationScreen, TRUE)
	ENDIF
	
//	SAVE_MY_EOM_VEHICLE_DATA()
	
	sEndOfMission.bBeingKickedFromSurvival = FALSE
	
    CLEAR_SPECIFIC_SPAWN_LOCATION()
    
	g_iOverheadNamesState = TAGS_OFF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		DO_END_OF_MODE_LEADERBOARD_DATA_SORT()
   		SET_LOCAL_PLAYER_CAN_COLLECT_HORDE_PICKUPS()
    ENDIF
	
	// B*1618888. 
	SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
	SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
	
	STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
	STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
	
	PRINTLN("[Survival] - alling STOP_AUDIO_SCENE(MP_CELEB_SCREEN_SCENE). Call A.")
	
	// Unregister enemy peds with spectator camera.
	CLEAR_ALL_SPECTATOR_TARGET_LISTS(specData.specHUDData)
	
    // Spectator cam
    IF NOT IS_THIS_SPECTATOR_CAM_OFF(specData.specCamData)
		FORCE_CLEANUP_SPECTATOR_CAM(specData, TRUE)
    ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(eMiniLeaderBoardData.siMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(eMiniLeaderBoardData.siMovie)
	ENDIF
	
	IF bSetBlockingAreas
	
		REMOVE_POP_MULTIPLIER_AREA(iPopMultArea, TRUE)
		REMOVE_SCENARIO_BLOCKING_AREA(sbiScenarioBlockingIndex, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(	(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos-<<1000.0, 1000.0, 250.0>>), 
												(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos+<<1000.0, 1000.0, 250.0>>),
												TRUE, FALSE)
		bSetBlockingAreas = FALSE
	ENDIF
	
    #IF IS_DEBUG_BUILD
    	PRINTLN("[Survival] - SCRIPT_CLEANUP - iCurrentMissionType = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType)
    #ENDIF
   	
    STOP_STREAM()
    
    SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
    
    SET_PLAYER_LOCKON(PLAYER_ID(), TRUE)
   	
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(scLB_control)
	IF HAS_SCALEFORM_MOVIE_LOADED(scLB_scaleformID)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scLB_scaleformID)
		PRINTLN("[Survival] - CLEANUP_SOCIAL_CLUB_LEADERBOARD marking SCLB scaleform movie as no longer needed")
	ENDIF
	
	//B*1600250.
	IF NETWORK_IS_SIGNED_ONLINE()  
		INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MC_OVER)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF PARTICIPANT_ID_TO_INT() >= 0
				IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
					INT IncrementCheatby = INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY()
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_END, IncrementCheatby)
					IF IncrementCheatby > 0
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_SURVIVAL_END)
					ENDIF
					
					PRINTLN("[Survival] - SCRIPT_CLEANUP - called INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_END, INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY()).")
				ENDIF
			ENDIF
		ENDIF
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_ENDED, 1)
	ENDIF
	
    // Player
    IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	
		//IF IS_MP_DECORATOR_BIT_SET(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
		//	CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
		//ENDIF
	
        IF NOT IS_ENTITY_DEAD((PLAYER_PED_ID()))
            SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE) 
        ENDIF
    ENDIF
    
    // Returns FALSE by the cleanup stage
    // Cleanup team targetting
   // PRINTLN("NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)")
    SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()	//NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
	    PRINTLN("[Survival] - SET_PLAYER_TEAM(PLAYER_ID(), -1)")
	    SET_PLAYER_TEAM(PLAYER_ID(), -1)
	ENDIF
    
    // Emergency services
    SET_MAX_WANTED_LEVEL(6)
    SET_WANTED_LEVEL_MULTIPLIER(1.0)
    ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)                  
    ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)       
	SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
    
    // HUD
    SET_WIDESCREEN_BORDERS(FALSE, -1)
    DISPLAY_RADAR(TRUE)
    DISPLAY_HUD(TRUE)
    
    // (414073)
    IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
        Enable_MP_Comms()
    ENDIF
    ENABLE_SELECTOR()
    CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
    DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
    ENABLE_ALL_MP_HUD()
    
	TOGGLE_STRIPPER_AUDIO(TRUE)
	
    // Delete mission entities if mission has finished.
    IF GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET
        CLEANUP_ALL_FMMC_ENTITIES(serverBD.sFMMC_SBD)
    ENDIF
    
    REPEAT GET_FMMC_MAX_NUM_PROPS() i
        IF DOES_ENTITY_EXIST(oiHordeProp[i])
            DELETE_OBJECT(oiHordeProp[i])
        ENDIF
    ENDREPEAT
	
	FOR i = 0 TO (FMMC_MAX_NUM_PROP_CHILDREN-1)
		IF DOES_ENTITY_EXIST(oihordePropChildren[i])
		   DELETE_OBJECT(oihordePropChildren[i])
		ENDIF
	ENDFOR
       
   	CLEAR_SPAWN_AREA()
	
    // Stop the players being invisible
	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
	    IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
	        IF NETWORK_IS_IN_TUTORIAL_SESSION()
	            NETWORK_END_TUTORIAL_SESSION()
	            #IF IS_DEBUG_BUILD
	                PRINTLN("[Survival] - NETWORK_END_TUTORIAL_SESSION")
	            #ENDIF
	        ENDIF
	    ENDIF
	 	SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
	ELSE
	    // Setup spawning.
	    SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AUTOMATIC, FALSE)
    ENDIF
	
    // Allowed to die again.
    IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
        IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
            SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
        ENDIF
    ENDIF
    
	IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
    	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bComingOutOfSuccessfulSurvival = TRUE
		PRINTLN("[Survival] - SCRIPT_CLEANUP - beat survival, setting flag for reward vehicles.")
	ENDIF
	
	// Make sure music stops.
	IF IS_SCORE_CORONA_OPTION_ON()
		REPEAT NUM_MISSION_MUSIC_EVENTS i
			FOR j = 1 TO NUMBER_HORDE_WAVES
				sEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(INT_TO_ENUM(ENUM_MUSIC_EVENT, i), j)
				IF NOT ARE_STRINGS_EQUAL(sEvent, "MP_SM_STOP_TRACK")
		  			CANCEL_MUSIC_EVENT(sEvent)
					PRINTLN("[Survival] - [Music] - cancelled music event ", sEvent)
				ENDIF
			ENDFOR
		ENDREPEAT
		PRINTLN("[Survival] - [Music] - cancelled all survival music events.")
		TRIGGER_MUSIC_EVENT(GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_STOP, 0))
		PRINTLN("[Survival] - [Music] - triggered eMUSICEVENT_STOP.")
	ELSE
		PRINTLN("[Survival] - [Music] - corona option says it's turned off.")
	ENDIF
	
    SET_ROADS(TRUE)
    
    RESET_FMMC_MISSION_VARIABLES(TRUE, FALSE, GET_FMMC_END_OF_MISSION_STATUS_FOR_END_HORDE_REASON())      
	
    //If the player is dead and on a playlist then force them to respawn
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_PLAYER_ON_A_PLAYLIST_INT(NATIVE_TO_INT(PLAYER_ID()))
			IF NOT IS_NET_PLAYER_OK(PLAYER_ID())			
		  		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
				FORCE_RESPAWN(TRUE)
			ENDIF
		ENDIF	
		//Force the player to respawn
		IF g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
		OR g_sFMMCEOM.iVoteStatus =  ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP
			IF NOT IS_NET_PLAYER_OK(PLAYER_ID())			
		  		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
				PRINTLN("[Survival] - [TS] RESET_FMMC_MISSION_VARIABLES - SPAWN_LOCATION_AT_CURRENT_POSITION - SET_PLAYER_NEXT_RESPAWN_LOCATION - FORCE_RESPAWN(TRUE)")
				FORCE_RESPAWN(TRUE)
			ENDIF
		ENDIF
	ENDIF	
	
    IF IS_NET_PLAYER_OK(PLAYER_ID())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
    ENDIF
    
//    SET_PED_WEAPONS_MP(PLAYER_PED_ID(), structPlayerWeapons)
    
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND)
	
    // Added at request of Kev B*924109.
    g_b_OnHordeWaveBoard = FALSE
	
	SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(FALSE)
	
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	
//	IF NETWORK_IS_IN_MP_CUTSCENE()
//		NETWORK_SET_IN_MP_CUTSCENE(FALSE)
//	ENDIF
	
	IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
		CLEANUP_ALL_ENEMIES()
	ENDIF
	
	g_bBlockAIKillXP = FALSE
	
	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		RESET_GAME_STATE_ON_DEATH()
	ENDIF
	
	SET_TIME_OF_DAY(TIME_OFF, TRUE)
	
	// Flag for David Gentles Spec cam stuff.
	ENABLE_SPECTATOR_FADES()
	// For weapons in respawning.
    g_bOnHorde = FALSE
	LBD_SCENE_CLEANUP()
	
	SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(FALSE)
	
	CLEAR_IDLE_KICK_TIME_OVERRIDDEN()
	
    TERMINATE_THIS_MULTIPLAYER_THREAD(serverBD.TerminationTimer)
ENDPROC


/// PURPOSE:
///    Gets if end game conditions have been fulfilled.
/// RETURNS:
///    TRUE if conditions met, FALSE if not.
FUNC BOOL HAVE_END_GAME_CONDITIONS_BEEN_MET()
    RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets if the time of day means the enemy helis should have their searchlights on.
/// RETURNS:
///    BOOL - TRUE if should have searchlights on, FALSE if not.
FUNC BOOL DOES_TOD_NEED_HELI_SEARCHLIGHTS()
	
	IF GET_CLOCK_HOURS() >= 22
		RETURN TRUE
	ENDIF
	
	IF GET_CLOCK_HOURS() <= 6
	AND GET_CLOCK_HOURS() >= 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


PROC CALCULATE_KILL_STREAK_STATE()
    
    // Work out current streak.
    IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
        IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
            
            IF structKillStreakData.iNumKillsInARow > 0 
                IF structKillStreakData.iNumKillsInARow >= SMALL_STREAK
                    IF structKillStreakData.iNumKillsInARow < MEDIUM_STREAK
                        structKillStreakData.eState = eKILLSTREAKSTATE_SMALL
                    ELSE
                        IF structKillStreakData.iNumKillsInARow < BIG_STREAK
                            structKillStreakData.eState = eKILLSTREAKSTATE_MEDIUM
                        ELSE
                            structKillStreakData.eState = eKILLSTREAKSTATE_LARGE
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
            
        ELSE    
                
            structKillStreakData.eState = eKILLSTREAKSTATE_NONE
            structKillStreakData.iNumKillsInARow = 0
            structKillStreakData.iBigMessageBitset = 0
            
        ENDIF   
    ENDIF
    
ENDPROC

FUNC INT GET_KILL_STREAK_XP_MULTIPLIER()
    
    SWITCH structKillStreakData.eState
        CASE eKILLSTREAKSTATE_NONE      RETURN 1        BREAK
        CASE eKILLSTREAKSTATE_SMALL     RETURN 2        BREAK
        CASE eKILLSTREAKSTATE_MEDIUM    RETURN 3        BREAK
        CASE eKILLSTREAKSTATE_LARGE     RETURN 4        BREAK
    ENDSWITCH
    
    RETURN 1
    
ENDFUNC

PROC MAINTAIN_KILL_STREAKS()
    
    // Set state.
    CALCULATE_KILL_STREAK_STATE()
    
    // Do state dependant logic.
    SWITCH structKillStreakData.eState
        
        CASE eKILLSTREAKSTATE_NONE
                
            // Do nothing.
                
        BREAK
        
        CASE eKILLSTREAKSTATE_SMALL
            
            IF NOT IS_BIT_SET(structKillStreakData.iBigMessageBitset, ENUM_TO_INT(eKILLSTREAKSTATE_SMALL))
                SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_KILLS_SMALL)
                SET_BIT(structKillStreakData.iBigMessageBitset, ENUM_TO_INT(eKILLSTREAKSTATE_SMALL))                            
            ENDIF
            
        BREAK
        
        CASE eKILLSTREAKSTATE_MEDIUM
            
            IF NOT IS_BIT_SET(structKillStreakData.iBigMessageBitset, ENUM_TO_INT(eKILLSTREAKSTATE_MEDIUM))
                SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_KILLS_MED)
                SET_BIT(structKillStreakData.iBigMessageBitset, ENUM_TO_INT(eKILLSTREAKSTATE_MEDIUM))                           
            ENDIF
            
        BREAK
        
        CASE eKILLSTREAKSTATE_LARGE
                
            IF NOT IS_BIT_SET(structKillStreakData.iBigMessageBitset, ENUM_TO_INT(eKILLSTREAKSTATE_LARGE))
                SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_KILLS_LARGE)
                SET_BIT(structKillStreakData.iBigMessageBitset, ENUM_TO_INT(eKILLSTREAKSTATE_LARGE))                            
            ENDIF
                
        BREAK
        
    ENDSWITCH
    
ENDPROC

PROC ADD_PICKUP_BLIP(INT iBlip)
	
	IF NOT DOES_BLIP_EXIST(biPickupBlip[iBlip])
        biPickupBlip[iBlip]=ADD_BLIP_FOR_COORD(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iBlip].vPos)
        SET_BLIP_SPRITE(biPickupBlip[iBlip], GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iBlip].pt))
        SET_BLIP_SCALE(biPickupBlip[iBlip], BLIP_SIZE_NETWORK_PICKUP_LARGE)
        SET_BLIP_AS_SHORT_RANGE(biPickupBlip[iBlip],TRUE)
        SHOW_HEIGHT_ON_BLIP(biPickupBlip[iBlip], FALSE)
		SET_BLIP_PRIORITY(biPickupBlip[iBlip], BLIPPRIORITY_LOWEST)
		IF IS_PICKUP_HEALTH(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iBlip].pt)
			SET_BLIP_COLOUR(biPickupBlip[iBlip], BLIP_COLOUR_GREEN)
		ELIF IS_PICKUP_ARMOUR(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iBlip].pt)
			SET_BLIP_COLOUR(biPickupBlip[iBlip], BLIP_COLOUR_BLUE)
		ENDIF
    ENDIF
	
ENDPROC

PROC REMOVE_PICKUP_BLIP(INT iBlip)
	
	IF DOES_BLIP_EXIST(biPickupBlip[iBlip])
        REMOVE_BLIP(biPickupBlip[iBlip])
    ENDIF
	
ENDPROC

PROC PROCESS_PICKUP_BLIPS()
    
	INT i
	
	SWITCH iProcessPickupBlipsStage
		
		// Initial add of blips so they don;t all appear in a messy stagger onto the radar.
		CASE 0
			
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
				IF DOES_PICKUP_EXIST(piPickup[i])
					ADD_PICKUP_BLIP(i)
				ENDIF
			ENDREPEAT
			
			iProcessPickupBlipsStage++
			
		BREAK
		
		// Staggered processing of pickup blips after initial add.
		CASE 1
			
			IF DOES_PICKUP_EXIST(piPickup[iStaggeredPickUpBlipCount])
		        IF NOT DOES_PICKUP_OBJECT_EXIST(piPickup[iStaggeredPickUpBlipCount])
		            REMOVE_PICKUP_BLIP(iStaggeredPickUpBlipCount)
		        ELSE
		            ADD_PICKUP_BLIP(iStaggeredPickUpBlipCount)
		        ENDIF
		    ELSE
		        REMOVE_PICKUP_BLIP(iStaggeredPickUpBlipCount)
		    ENDIF
		    
		    iStaggeredPickUpBlipCount++
		    
		    IF iStaggeredPickUpBlipCount >= g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
		        iStaggeredPickUpBlipCount = 0
		    ENDIF
			
		BREAK
		
	ENDSWITCH
	
    
    
ENDPROC

FUNC BOOL CREATE_PICKUPS()

    INT i
    INT iPlacementFlags = 0
    VECTOR vPos
   	INT iAmmo
	
    SET_FM_PICKUP_ORIENTATION_FLAGS(iPlacementFlags)
    SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
    
    IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
        RETURN TRUE
    ENDIF
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
        IF NOT DOES_PICKUP_EXIST(piPickup[i])
            vPos = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos
            IF NOT IS_VECTOR_ZERO(vPos)
            AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt != NUM_PICKUPS
                PRINTLN("[Survival] - making pickup: ", i, " at the coordinates: ", vPos)
                PRINTLN("[Survival] - making pickup: ", i, " - pickup type = ", ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt))
                IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt = PICKUP_WEAPON_GRENADELAUNCHER
				OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt = PICKUP_WEAPON_GRENADE
					iAmmo = 2
				ELSE
					iAmmo = GET_AMMO_AMOUNT_FOR_MP_PICKUP(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt))
				ENDIF
				// We give rpgs on the waves with helicopters. Give one more rocket than the number of helis there are.
				IF (g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt = PICKUP_WEAPON_RPG)
					SWITCH serverBd.sWaveData.iWaveCount
						CASE 4
							iAmmo = 2
						BREAK
						CASE 7
							IF serverBD.sFightingParticipantsList.iNumFightingParticipants > 1
								iAmmo = 3
							ENDIF
						BREAK
						CASE 10
							IF serverBD.sFightingParticipantsList.iNumFightingParticipants > 2
								iAmmo = 4
							ELIF serverBD.sFightingParticipantsList.iNumFightingParticipants > 1
								iAmmo = 3
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
				IF serverBD.sWaveData.iWaveCount <= 5
				OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt = PICKUP_HEALTH_STANDARD
				OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt = PICKUP_ARMOUR_STANDARD
			    	CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
				ELSE
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
				ENDIF
				piPickup[i] = CREATE_PICKUP_ROTATE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt, vPos + <<0,0,0.10>>, <<0,360,0>>, iPlacementFlags, iAmmo)
//                              SET_PICKUP_REGENERATION_TIME(piPickup[i], GET_MC_SPAWN_TIME_FROM_INT(g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime))
            ENDIF
        ENDIF
    ENDREPEAT
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
        IF NOT DOES_PICKUP_EXIST(piPickup[i])
            RETURN FALSE
        ENDIF
    ENDREPEAT
    
    RETURN TRUE
    
ENDFUNC

PROC REMOVE_PICKUPS()
    
    INT i
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
        IF DOES_PICKUP_EXIST(piPickup[i])
            PRINTLN("[Survival] - removing pickup: ", i)
            REMOVE_PICKUP(piPickup[i])
			REMOVE_PICKUP_BLIP(i)
        ENDIF
    ENDREPEAT
    
	iProcessPickupBlipsStage = 0 // For blip processing.
	
ENDPROC

PROC CREATE_PICKUPS_FOR_NEW_WAVE(INT &iCreateStage)
    
    SWITCH iCreateStage
        CASE 0
            SET_PICKUP_TYPES_FOR_WAVE()
            iCreateStage++
            PRINTLN("[Survival] - setup new pickups types. Going to iCreateStage ", iCreateStage)
        BREAK
        CASE 1
            IF CREATE_PICKUPS()
                iCreateStage++
                PRINTLN("[Survival] - created new pickups. Going to iCreateStage ", iCreateStage)
            ENDIF
        BREAK
        CASE 2
            // New pickups created!
        BREAK
    ENDSWITCH
    
ENDPROC

/// PURPOSE:
///    Gets if the local player is near the horde shootout. Just uses distance form start point just now
///    but will eventually have a specific radius set by the creator tool.
/// RETURNS:
///    BOOL - TRUE if near shootout, FALSE if not.
FUNC BOOL IS_PLAYER_AT_HORDE_SHOOTOUT(PLAYER_INDEX playerId)
    
	IF HAS_SURVIVAL_OUT_OF_BOUNDS_DATA_BEEN_SETUP()
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF NOT IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(playerId), sSurvivalBoundsData.vMin, sSurvivalBoundsData.vMax, sSurvivalBoundsData.fWidth)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_NET_PLAYER_OK(playerId)
	        IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(playerId), g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos) >= AT_HORDE_DISTANCE
	            RETURN FALSE
	        ENDIF
	    ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(playerId), <<-86.293831,-759.790771,43.217434>>, <<-78.534744,-786.002869,36.354721>>, 12.000000) // Hack to sort exploit in bug 1768177 - will get more blocking areas added to the creator to sort this properly. 
		RETURN FALSE
	ENDIF
	
    RETURN TRUE
    
ENDFUNC

PROC ADD_SHOOTOUT_BLIP()
    
    IF NOT DOES_BLIP_EXIST(biShootOutBlip)
        PRINTLN("[Survival] - ADD_SHOOTOUT_BLIP - g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos = ", g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos)           
        biShootOutBlip = ADD_BLIP_FOR_COORD(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos)
        SET_BLIP_ROUTE(biShootOutBlip, TRUE)
        SET_BLIP_SCALE(biShootOutBlip, BLIP_SIZE_NETWORK_COORD)
        SHOW_HEIGHT_ON_BLIP(biShootOutBlip, TRUE)
    ENDIF
    
ENDPROC

PROC REMOVE_SHOOTOUT_BLIP()
    
    IF DOES_BLIP_EXIST(biShootOutBlip)
        SET_BLIP_ROUTE(biShootOutBlip, FALSE)
        REMOVE_BLIP(biShootOutBlip)
    ENDIF
    
ENDPROC

PROC REMOVE_ON_FOOT_PED_BLIPS()
	
	INT i, j
	
	REPEAT MAX_NUM_ENEMY_SQUADS j
		REPEAT MAX_NUM_PEDS_IN_SQUAD i
        	CLEANUP_AI_PED_BLIP(structSquadEnemyBlip[j][i])
    	ENDREPEAT
	ENDREPEAT
	
ENDPROC

PROC REMOVE_VEHICLE_PED_BLIPS()
	
	INT i, j
	
	REPEAT MAX_NUM_LAND_VEHICLES j
		REPEAT NUM_LAND_VEH_PEDS i
			CLEANUP_AI_PED_BLIP(structLandVehicleEnemyBlip[j][i])
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

PROC REMOVE_HORDE_BLIPS(BOOL bRemoveGetToShootOutBlip = TRUE)
    
	INT i
	
    REMOVE_ON_FOOT_PED_BLIPS()
	REMOVE_VEHICLE_PED_BLIPS()
	
    REPEAT ciMAX_CREATED_WEAPONS i
        IF DOES_BLIP_EXIST(biPickupBlip[i])
            REMOVE_BLIP(biPickupBlip[i])
        ENDIF
    ENDREPEAT
    
    IF bRemoveGetToShootOutBlip
        REMOVE_SHOOTOUT_BLIP()
    ENDIF
    
ENDPROC

/// PURPOSE:
///    Tells if a ped has been killed by a head shot
/// PARAMS:
///    pedID - ped ID
/// RETURNS:
///    True or False
FUNC BOOL CHECK_HEADSHOT(PED_INDEX pedID)

    IF NOT DOES_ENTITY_EXIST(pedID)
        RETURN FALSE
    ENDIF
    
    PED_BONETAG ePedBonetag = BONETAG_NULL
    
    IF IS_ENTITY_DEAD(pedID) OR IS_PED_INJURED(pedID)
        IF NOT GET_PED_LAST_DAMAGE_BONE(pedID, ePedBonetag)     
            RETURN FALSE
        ENDIF

        RETURN ePedBonetag = BONETAG_HEAD OR ePedBonetag = BONETAG_NECK
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

/// PURPOSE:
///    Broadcasts a ticker to all players.
/// PARAMS:
///    aTicker - ticker to broadcast.
PROC BROADCAST_FMMC_TICKER_TO_ALL_PLAYERS(TICKER_EVENTS aTicker)

    SCRIPT_EVENT_DATA_TICKER_CUSTOM_MESSAGE TickerMessage
    TickerMessage.TickerEvent = aTicker
    BROADCAST_CUSTOM_TICKER_EVENT(TickerMessage, ALL_PLAYERS_ON_SCRIPT())
    
	PRINTLN("[Survival] - BROADCAST_FMMC_TICKER_TO_ALL_PLAYERS - sent killed ped ticker.")        
   	
ENDPROC

/// PURPOSE:
///    Fades the screen in.
/// RETURNS:
///    TRUE if screen is faded in, FALSE if not.
FUNC BOOL FADE_SCREEN_IN()
    
    IF IS_SCREEN_FADED_OUT()
    OR IS_SCREEN_FADING_OUT()
    
        DO_SCREEN_FADE_IN(500)
            
    ELIF IS_SCREEN_FADED_IN()
    
        RETURN TRUE
            
    ENDIF
    
    RETURN FALSE
    
ENDFUNC
    
PROC SETUP_PED_FOR_WAVE(INT iSquad, INT iPed, PED_INDEX piPassed, BOOL bLandVehiclePed = FALSE, BOOL bRearmingPed = FALSE)
    
    INT iAccuracy
    INT iArmour
    INT iHealth
    WEAPON_TYPE eWeapon
	WEAPON_TYPE eWeapon2 = WEAPONTYPE_PISTOL
    COMBAT_ABILITY_LEVEL eCombatAbility
    INT iPedSquadSlot = serverBD.structAiSquad[iSquad].sPed[iPed].sSquadIdData.iSquadSlot
    FLOAT fPedHealth
	
    PRINTLN("[Survival] - [Populate Enemy Data] - iPed = ", iPed, ", iPedSquadSlot = ", iPedSquadSlot)
    
    SWITCH serverBD.sWaveData.iWaveCount
        
        CASE 1
            
			iAccuracy	= 10
	        iArmour  	= 0
	        iHealth  	= 125
			
            eCombatAbility    	= CAL_POOR
            eWeapon           	= WEAPONTYPE_PISTOL
	        
        BREAK
        
        CASE 2
            
			iAccuracy	= 12
            iArmour  	= 0
            iHealth  	= 125
			
			eCombatAbility    	= CAL_AVERAGE
			
			IF NOT bLandVehiclePed
			
	            SWITCH iPedSquadSlot
	                CASE 0 eWeapon = WEAPONTYPE_PISTOL BREAK
	                CASE 1 eWeapon = WEAPONTYPE_SMG BREAK
	                CASE 2 eWeapon = WEAPONTYPE_PISTOL BREAK
	                CASE 3 eWeapon = WEAPONTYPE_PISTOL BREAK
	                CASE 4 eWeapon = WEAPONTYPE_SMG BREAK
	                CASE 5 eWeapon = WEAPONTYPE_PISTOL BREAK
	                CASE 6 eWeapon = WEAPONTYPE_PISTOL BREAK
	            ENDSWITCH
            
			ELSE
				
				eWeapon = WEAPONTYPE_PISTOL
				
			ENDIF
			
        BREAK
        
        CASE 3
           
			iAccuracy	= 15
            iArmour  	= 0
            iHealth  	= 125
			
			eCombatAbility     	= CAL_AVERAGE
			
			IF NOT bLandVehiclePed
			
	            SWITCH iPedSquadSlot
	                    CASE 0 eWeapon = WEAPONTYPE_SMG BREAK
	                    CASE 1 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	                    CASE 2 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	                    CASE 3 eWeapon = WEAPONTYPE_SMG BREAK
	                    CASE 4 eWeapon = WEAPONTYPE_SMG BREAK
	                    CASE 5 eWeapon = WEAPONTYPE_SMG BREAK
	                    CASE 6 eWeapon = WEAPONTYPE_SMG BREAK
	            ENDSWITCH
             
			ELSE
				
				eWeapon = WEAPONTYPE_SMG
				
			ENDIF
			
        BREAK
        
        CASE 4
			
			iAccuracy	= 16
            iArmour  	= 20
            iHealth  	= 150
			
            eCombatAbility          = CAL_AVERAGE
			
			IF NOT bLandVehiclePed
				
	            SWITCH iPedSquadSlot
	                    CASE 0 eWeapon = WEAPONTYPE_CARBINERIFLE BREAK
	                    CASE 1 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	                    CASE 2 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	                    CASE 3 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	                    CASE 4 eWeapon = WEAPONTYPE_SMG BREAK
	                    CASE 5 eWeapon = WEAPONTYPE_SMG BREAK
	                    CASE 6 eWeapon = WEAPONTYPE_SMG BREAK
	            ENDSWITCH
	        
			ELSE
				
				eWeapon = WEAPONTYPE_SMG
				
			ENDIF
			
        BREAK
        
        CASE 5
           	
			iAccuracy	= 18
            iArmour  	= 30
            iHealth  	= 150
			
			eCombatAbility          = CAL_AVERAGE
			
			IF NOT bLandVehiclePed
				
	            SWITCH iPedSquadSlot
	                    CASE 0 eWeapon = WEAPONTYPE_CARBINERIFLE BREAK
	                    CASE 1 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	                    CASE 2 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	                    CASE 3 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	                    CASE 4 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	                    CASE 5 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	                    CASE 6 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	            ENDSWITCH
	              
			ELSE
				
				eWeapon = WEAPONTYPE_ASSAULTSHOTGUN
				
			ENDIF
			
        BREAK
        
        CASE 6
            
			iAccuracy	= 20
            iArmour  	= 40
            iHealth  	= 150
			
			eCombatAbility          = CAL_AVERAGE
			
			IF NOT bLandVehiclePed
				
	            SWITCH iPedSquadSlot
	                    CASE 0 eWeapon = WEAPONTYPE_MG BREAK
	                    CASE 1 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	                    CASE 2 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	                    CASE 3 eWeapon = WEAPONTYPE_MG BREAK
	                    CASE 4 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	                    CASE 5 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	                    CASE 6 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	            ENDSWITCH
	        
			ELSE
				
				eWeapon = WEAPONTYPE_ASSAULTRIFLE
				
			ENDIF
					
        BREAK
        
        CASE 7
           	
			iAccuracy	= 22
            iArmour  	= 50
            iHealth  	= 150
			
			eCombatAbility          = CAL_PROFESSIONAL
			
			IF NOT bLandVehiclePed
				
	            SWITCH iPedSquadSlot
	                    CASE 0 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	                    CASE 1 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	                    CASE 2 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	                    CASE 3 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	                    CASE 4 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	                    CASE 5 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	                    CASE 6 eWeapon = WEAPONTYPE_ASSAULTSHOTGUN BREAK
	            ENDSWITCH
	           
			ELSE
				
				eWeapon = WEAPONTYPE_ASSAULTRIFLE
				
			ENDIF
			
        BREAK
        
        CASE 8
            
			iAccuracy	= 24
            iArmour  	= 60
            iHealth  	= 170
			
			eCombatAbility          = CAL_PROFESSIONAL
			
			IF NOT bLandVehiclePed
				
	            SWITCH iPedSquadSlot
	                    CASE 0 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 1 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	                    CASE 2 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 3 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 4 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	                    CASE 5 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 6 eWeapon = WEAPONTYPE_ASSAULTRIFLE BREAK
	            ENDSWITCH
	        
			ELSE
				
				eWeapon = WEAPONTYPE_ASSAULTRIFLE
				
			ENDIF
			
        BREAK
        
        CASE 9
            
			iAccuracy	= 26
            iArmour  	= 60
            iHealth  	= 170
			
			eCombatAbility          = CAL_PROFESSIONAL
			
			IF NOT bLandVehiclePed
				
	            SWITCH iPedSquadSlot
	                    CASE 0 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 1 eWeapon = WEAPONTYPE_MINIGUN BREAK
	                    CASE 2 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 3 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 4 eWeapon = WEAPONTYPE_MINIGUN BREAK
	                    CASE 5 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 6 eWeapon = WEAPONTYPE_COMBATMG BREAK
	            ENDSWITCH
	        
			ELSE
				
				eWeapon = WEAPONTYPE_COMBATMG
					
			ENDIF
			
        BREAK
        
        CASE 10
            
			iAccuracy	= 28
            iArmour  	= 75
            iHealth  	= 170
			
			eCombatAbility          = CAL_PROFESSIONAL
			
			IF NOT bLandVehiclePed
				
	            SWITCH iPedSquadSlot
	                    CASE 0 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 1 eWeapon = WEAPONTYPE_MINIGUN BREAK
	                    CASE 2 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 3 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 4 eWeapon = WEAPONTYPE_MINIGUN BREAK
	                    CASE 5 eWeapon = WEAPONTYPE_COMBATMG BREAK
	                    CASE 6 eWeapon = WEAPONTYPE_COMBATMG BREAK
	            ENDSWITCH
	       	
			ELSE
				
				eWeapon = WEAPONTYPE_COMBATMG
				
			ENDIF
			
        BREAK
        
    ENDSWITCH
    
	serverBD.structAiSquad[iSquad].sPed[iPed].iAccuracy = iAccuracy
	
    SET_PED_ACCURACY(piPassed, serverBD.structAiSquad[iSquad].sPed[iPed].iAccuracy)
    SET_PED_COMBAT_ABILITY(piPassed, eCombatAbility)
            
    //SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_WILL_SCAN_FOR_DEAD_PEDS, TRUE)
    SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_USE_COVER, TRUE)
    SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FIGHT, TRUE)      
    SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_FLANK, TRUE) 
    SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_CHARGE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_AGGRESSIVE, TRUE)
	
	SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS, TRUE)
	
	SET_PED_CONFIG_FLAG(piPassed, PCF_DisableLadderClimbing, TRUE)
	SET_PED_PATH_CAN_USE_LADDERS(piPassed, FALSE)
	
	SET_PED_CONFIG_FLAG(piPassed, PCF_GetOutBurningVehicle, FALSE)
	
    SET_PED_COMBAT_MOVEMENT(piPassed, CM_WILLADVANCE)
	
	SET_PED_COMBAT_RANGE(piPassed, CR_NEAR)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPassed, TRUE)
	
    //SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPassed, TRUE)  
    SET_PED_TO_INFORM_RESPECTED_FRIENDS(piPassed, 150, 20)
    SET_PED_TARGET_LOSS_RESPONSE(piPassed, TLR_NEVER_LOSE_TARGET)
    
	IF NOT bRearmingPed
	    IF iArmour > 0
	        SET_PED_ARMOUR(piPassed, iArmour)
	    ENDIF
		fPedHealth = iHealth*g_sMPTunables.fAiHealthModifier
		iHealth = ROUND(fPedHealth)
		SET_ENTITY_MAX_HEALTH(piPassed, iHealth)
	    SET_ENTITY_HEALTH(piPassed, iHealth)
	ENDIF
	
	BOOL bGiveGrenade
	BOOL bForceFirstWeapon = TRUE
	
	IF NOT bLandVehiclePed
		IF (serverBD.sWaveData.iWaveCount >= 6)
			IF (GET_RANDOM_INT_IN_RANGE(0,3) = 1)
				bGiveGrenade = TRUE
				PRINTLN("[Survival] - setting bGiveGrenade = TRUE.")
			ENDIF
		ENDIF
	ENDIF
	
	IF bLandVehiclePed
		IF IS_PED_IN_ANY_VEHICLE(piPassed, TRUE)
			bForceFirstWeapon = FALSE
		ENDIF
	ENDIF
	IF bGiveGrenade
		bForceFirstWeapon = FALSE
	ENDIF
	
	GIVE_WEAPON_TO_PED(piPassed, eWeapon, 25000, bForceFirstWeapon, bForceFirstWeapon)
	
	IF bLandVehiclePed
		GIVE_WEAPON_TO_PED(piPassed, eWeapon2, 25000, TRUE, TRUE)
	ENDIF
	
	IF bGiveGrenade
		GIVE_WEAPON_TO_PED(piPassed, WEAPONTYPE_GRENADE, 1, TRUE, TRUE)
	ENDIF
	
ENDPROC

PROC SET_UP_FMMC_PED_WK(PED_INDEX piPassed, INT iPedNumber,BOOL bGivePedWeapon = TRUE, BOOL bsetForCombat = TRUE)
    
   // INT iteamrel[FMMC_MAX_TEAMS]
    INT irepeat
    INT iAccuracy
    COMBAT_ABILITY_LEVEL eCombatAbility
    
    #IF IS_DEBUG_BUILD
            TEXT_LABEL_23 tl23
    #ENDIF
    IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL) // If in horde mode, we want the weapon to always start as the pistol so we can increase difficulty later by giving better weapons.
        g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_PISTOL
    ENDIF
    
    IF bGivePedWeapon
    	GIVE_DELAYED_WEAPON_TO_PED(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun, 25000, TRUE)
    ENDIF
    FOR irepeat = 0 TO (FMMC_MAX_TEAMS-1)
        IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_LIKE
            //iteamrel[irepeat] = FM_RelationshipLike 
            SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
            SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
        ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_DISLIKE
            //iteamrel[irepeat] = FM_RelationshipDislike
            SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
            SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
        ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_HATE
            //iteamrel[irepeat] = FM_RelationshipHate
            SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
            SET_ENTITY_IS_TARGET_PRIORITY(piPassed, TRUE)
        ENDIF
    ENDFOR
    
    //IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_GANGHIDEOUT)
    //OR (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL)
        //Always Dislike - They will be made to hate in the mission script
        SET_PED_RELATIONSHIP_GROUP_HASH(piPassed, rgFM_AiHate)  //rgFM_AiDislike)               
    //ELSE
    //    SET_PED_RELATIONSHIP_GROUP_HASH(piPassed,rgFM_AiPed[iteamrel[0]][iteamrel[1]][iteamrel[2]][iteamrel[3]][FM_RelationshipNothingCops])
    //ENDIF
    
    #IF IS_DEBUG_BUILD
        tl23 = "niPed "
        tl23 += iPedNumber 
        SET_PED_NAME_DEBUG(piPassed, tl23)
    #ENDIF
    
    //Set Behaviours
   // IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RAMPAGE // Make much easier for rampages, it's about loads of easy kills in a time limit, not skillful killing.
    IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL
        IF bsetForCombat
            iAccuracy = 10
            eCombatAbility = CAL_POOR
        ENDIF
    ELSE
        IF bsetForCombat
            IF g_FMMC_STRUCT.iDifficulity = FMMC_DIFFICULTY_EASY
                iAccuracy = 10
                eCombatAbility = CAL_POOR
            ELIF g_FMMC_STRUCT.iDifficulity = FMMC_DIFFICULTY_MEDIUM
                iAccuracy = 30
                eCombatAbility = CAL_AVERAGE
                ADD_ARMOUR_TO_PED(piPassed,50)
            ELIF g_FMMC_STRUCT.iDifficulity = FMMC_DIFFICULTY_HARD
                iAccuracy = 45
                eCombatAbility = CAL_PROFESSIONAL
                ADD_ARMOUR_TO_PED(piPassed,100)
            ENDIF
        ENDIF   
        SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPassed,FALSE)
    ENDIF
    
    IF bsetForCombat
        SET_PED_ACCURACY(piPassed, iAccuracy)
        SET_PED_COMBAT_ABILITY(piPassed, eCombatAbility)
        SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FIGHT, TRUE)
        SET_PED_COMBAT_MOVEMENT(piPassed, CM_WILLADVANCE)
    ENDIF
    
    //SET_PED_KEEP_TASK(piPassed,TRUE)
    SET_PED_DIES_WHEN_INJURED(piPassed,TRUE)
    SET_PED_TARGET_LOSS_RESPONSE(piPassed, TLR_NEVER_LOSE_TARGET)
    SET_PED_TO_INFORM_RESPECTED_FRIENDS(piPassed, 150, 20)
    
    IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_MINIGUN
        SET_PED_FIRING_PATTERN(piPassed,FIRING_PATTERN_FULL_AUTO)
    ENDIF
    
ENDPROC

FUNC BOOL CREATE_FMMC_PEDS_MP_WK(NETWORK_INDEX &niPed[], BOOL bGivePedWeapon = TRUE, BOOL bSetupForCombat = TRUE) //, INT iStage = 0 )
    INT i
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
        IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
        //AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStage = iStage
            IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed[i])
                IF CREATE_NET_PED(niPed[i], PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fHead)
                    //PED_INDEX piTemp = NET_TO_PED(niPed[i])
                    SET_UP_FMMC_PED_WK(NET_TO_PED(niPed[i]), i, bGivePedWeapon, bSetupForCombat)
                    #IF IS_DEBUG_BUILD
                    	PRINTLN("[Survival] - ped ", i, " created.")
                    #ENDIF
                ENDIF                           
            ENDIF
        ELSE
            PRINTLN("[Survival] - ped ", i, " is dummy model.")
        ENDIF   
    ENDREPEAT
    //Check we have created all Security Guards
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
        IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed[i])
        AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
        //AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStage = iStage
            RETURN FALSE
        ENDIF
    ENDREPEAT
    RETURN TRUE
ENDFUNC

FUNC BOOL LOAD_AND_CREATE_ALL_FMMC_ENTITIES_WK(FMMC_SERVER_DATA_STRUCT &sSDSpassed, BOOL bDontCreatePeds = FALSE, BOOL bGivePedsWeapon = TRUE, BOOL bSetupPedsForCombat = TRUE)

    IF REQUEST_LOAD_FMMC_MODELS()
		
		PRINTLN("[Survival] - [Create Entities] - REQUEST_LOAD_FMMC_MODELS = TRUE.")
		
        //Set the number of entities to load
        INT iNumberOfPeds       = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
        INT iNumberOfVehicles   = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
        INT iNumberOfObjects    = g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
        INT iNumberOfWeapons    = g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons

        IF CAN_REGISTER_MISSION_ENTITIES(iNumberOfPeds, iNumberOfVehicles, iNumberOfObjects,  iNumberOfWeapons)
			PRINTLN("[Survival] - [Create Entities] - CAN_REGISTER_MISSION_ENTITIES(", iNumberOfPeds, iNumberOfVehicles, iNumberOfObjects,  iNumberOfWeapons, ") = TRUE.")
            IF CREATE_FMMC_OBJECTS_MP(sSDSpassed.niObject)
				PRINTLN("[Survival] - [Create Entities] - CREATE_FMMC_OBJECTS_MP = TRUE.")
                IF NOT bDontCreatePeds
					PRINTLN("[Survival] - [Create Entities] - bDontCreatePeds = TRUE.")
                    IF CREATE_FMMC_PEDS_MP_WK(sSDSpassed.niPed, bGivePedsWeapon, bSetupPedsForCombat)
						PRINTLN("[Survival] - [Create Entities] - CREATE_FMMC_PEDS_MP_WK = TRUE.")
                    ELSE
                        PRINTLN("[Survival] - [Create Entities] - CREATE_FMMC_PEDS_MP_WK = FALSE.")
						RETURN FALSE
                    ENDIF
                ELSE
                    PRINTLN("[Survival] - [Create Entities] - bDontCreatePeds = TRUE.")
                ENDIF
            ELSE
                PRINTLN("[Survival] - [Create Entities] - CREATE_FMMC_OBJECTS_MP = FALSE.")
				RETURN FALSE
            ENDIF
        ELSE
			PRINTLN("[Survival] - [Create Entities] - CAN_REGISTER_MISSION_ENTITIES(", iNumberOfPeds, iNumberOfVehicles, iNumberOfObjects,  iNumberOfWeapons, ") = FALSE.")
            RETURN FALSE
        ENDIF
    ELSE
		PRINTLN("[Survival] - [Create Entities] - REQUEST_LOAD_FMMC_MODELS = FALSE.")
    ENDIF
	
    PRINTLN("[Survival] - [Create Entities] - LOAD_AND_CREATE_ALL_FMMC_ENTITIES_WK = TRUE.")
    RETURN TRUE
    
ENDFUNC

FUNC BOOL GET_SPAWN_COORDS_FOR_ENEMY_PED(VECTOR &vspawnloc, FLOAT &fspawnhead)
	
	VECTOR vMin, vMax
	
	vMin = ( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[0].vLoc[0] - << 0.0, 0.0, 5.0 >> )
	vMax = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[1].vLoc[0]
	
	SPAWN_SEARCH_PARAMS SpawnSearchParams
	SpawnSearchParams.vFacingCoords = g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos
	SpawnSearchParams.fMinDistFromPlayer=20
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[0].vLoc[0])
		RETURN GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos, 150.0, vspawnloc, fspawnhead, SpawnSearchParams)
	ENDIF
	
	RETURN GET_SAFE_COORDS_IN_ANGLED_AREA_FOR_CREATING_ENTITY(vMin, vMax, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[1].fRadius[0], vspawnloc, fspawnhead, SpawnSearchParams)
	
ENDFUNC

FUNC INT GET_SPAWN_PED_UNIQUE_ID(INT iPed, INT iSquad)
	
	RETURN (iPed*(iSquad+1))
	
ENDFUNC

FUNC BOOL RESPAWN_MISSION_PED(INT iped, INT iSquadId)
    
    FLOAT fspawnhead
    MODEL_NAMES eModel = g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].mn
    VECTOR vTemp
	
    IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.structAiSquad[iSquadId].sPed[iPed].netId)
        
        #IF IS_DEBUG_BUILD
        IF bDoRespawnMissionPedPrints
        PRINTLN("[Survival] - [AI Spawning] - found spawn coords for ped: ", iped, " net id does not exist.")
        ENDIF
        #ENDIF
        
        IF eModel != DUMMY_MODEL_FOR_SCRIPT
    
            #IF IS_DEBUG_BUILD
            IF bDoRespawnMissionPedPrints
            PRINTLN("[Survival] - [AI Spawning] - found spawn coords for ped: ",iped, " model != DUMMY_MODEL_FOR_SCRIPT.")
            ENDIF
            #ENDIF
            
            REQUEST_MODEL(eModel)
            
            IF HAS_MODEL_LOADED(eModel)
                
                #IF IS_DEBUG_BUILD
                IF bDoRespawnMissionPedPrints
                PRINTLN("[Survival] - [AI Spawning] - found spawn coords for ped: ", iped, " model loaded.")
                ENDIF
                #ENDIF
                
                IF CAN_REGISTER_MISSION_PEDS(1)
                    
                    #IF IS_DEBUG_BUILD
                    IF bDoRespawnMissionPedPrints
                    PRINTLN("[Survival] - [AI Spawning] - found spawn coords for ped: ", iped, " CAN_REGISTER_MISSION_PEDS(1) = TRUE.")
                    ENDIF
                    #ENDIF
                    
                    serverBd.iSearchingForPed = GET_SPAWN_PED_UNIQUE_ID(iped, iSquadId)
                    
                    // Get spawn point.
                    IF IS_VECTOR_ZERO(serverBD.structAiSquad[iSquadId].sPed[iPed].vCurrentSpawnCoords)
                
                        #IF IS_DEBUG_BUILD
                        IF bDoRespawnMissionPedPrints
                        PRINTLN("[Survival] - [AI Spawning] - serverBd.iSearchingForPed = ", serverBd.iSearchingForPed)
                        ENDIF
                        #ENDIF
						
                        IF GET_SPAWN_COORDS_FOR_ENEMY_PED(vTemp, fspawnhead)
                        	
							serverBD.structAiSquad[iSquadId].sPed[iPed].vCurrentSpawnCoords = vTemp
							
							#IF IS_DEBUG_BUILD
                            IF bDoRespawnMissionPedPrints
                            PRINTLN("[Survival] - [AI Spawning] - found spawn coords for ped: ", iped, " at coords: ", serverBD.structAiSquad[iSquadId].sPed[iPed].vCurrentSpawnCoords)
							ENDIF
							#ENDIF
                        
	                        IF CREATE_NET_PED(serverBD.structAiSquad[iSquadId].sPed[iPed].netId, PEDTYPE_MISSION, eModel, serverBD.structAiSquad[iSquadId].sPed[iPed].vCurrentSpawnCoords, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fHead)
	                           	
								SET_PED_ANGLED_DEFENSIVE_AREA(	NET_TO_PED(serverBD.structAiSquad[iSquadId].sPed[iPed].netId), (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[0].vLoc[0] - << 0.0, 0.0, 5.0 >>), g_FMMC_STRUCT_ENTITIES.sGotoLocationData[1].vLoc[0],
																g_FMMC_STRUCT_ENTITIES.sGotoLocationData[1].fRadius[0] )
								
								SET_UP_FMMC_PED_WK(NET_TO_PED(serverBD.structAiSquad[iSquadId].sPed[iPed].netId), iped, FALSE)
	                            SETUP_PED_FOR_WAVE(iSquadId, iped, NET_TO_PED(serverBD.structAiSquad[iSquadId].sPed[iPed].netId))
								
	                            SET_PED_DIES_WHEN_INJURED(NET_TO_PED(serverBD.structAiSquad[iSquadId].sPed[iPed].netId), TRUE)
								SET_PED_RANDOM_COMPONENT_VARIATION(NET_TO_PED(serverBD.structAiSquad[iSquadId].sPed[iPed].netId))
								SET_PED_AS_ENEMY(NET_TO_PED(serverBD.structAiSquad[iSquadId].sPed[iPed].netId), TRUE)
								
	                            SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
								
	                            serverBd.iSearchingForPed = -1
	                            PRINTLN("[Survival] - [AI Spawning] - spawning bug - successfully respawned ped: ", iped, " at coords: ", serverBD.structAiSquad[iSquadId].sPed[iPed].vCurrentSpawnCoords)
	                            serverBD.structAiSquad[iSquadId].sPed[iPed].vCurrentSpawnCoords = << 0.0, 0.0, 0.0 >>
								
	                            RETURN TRUE
	                        ENDIF
							
                        ENDIF
                
                // If got spawn point, create!
                   	ELSE
                    
                        #IF IS_DEBUG_BUILD
                        IF bDoRespawnMissionPedPrints
                        PRINTLN("[Survival] - [AI Spawning] - calling create respawn ped: ", iped, "at coords: ", serverBD.structAiSquad[iSquadId].sPed[iPed].vCurrentSpawnCoords)
                        ENDIF
						#ENDIF
                    ENDIF
                        
                ELSE
                
                    #IF IS_DEBUG_BUILD
                    IF bDoRespawnMissionPedPrints
                    PRINTLN("[Survival] - [AI Spawning] - found spawn coords for ped: ", iped, " CAN_REGISTER_MISSION_PEDS(1) = FALSE.")
                    ENDIF
                    #ENDIF
                        
                ENDIF
                
            ELSE
        
                #IF IS_DEBUG_BUILD
                IF bDoRespawnMissionPedPrints
                PRINTLN("[Survival] - [AI Spawning] - found spawn coords for ped: ", iped, " - loading model.")
                ENDIF
                #ENDIF
                
            ENDIF
            
        ENDIF
		
    ELSE
	
		IF NOT IS_PED_INJURED(NET_TO_PED(serverBD.structAiSquad[iSquadId].sPed[iPed].netId))
			
            serverBD.structAiSquad[iSquadId].sPed[iPed].vCurrentSpawnCoords = << 0.0, 0.0, 0.0 >>
            PRINTLN("[Survival] - [AI Spawning] - spawning bug - ped: ", iped, " already exists.")
            RETURN TRUE
			
		ENDIF
		
    ENDIF
    
    RETURN FALSE

ENDFUNC

FUNC INT GET_ENEMY_RESPAWN_TIME(BOOL bOverlapPed = FALSE)
    
    INT iReturnValue = g_FMMC_STRUCT_ENTITIES.iPedRespawnTime
    
    IF g_FMMC_STRUCT_ENTITIES.iPedRespawnTime = FMMC_PED_RESPAWN_TIME_RANDOM
		IF bOverlapPed
			iReturnValue = GET_RANDOM_INT_IN_RANGE(FMMC_PED_RESPAWN_TIME_8_SECONDS, FMMC_PED_RESPAWN_TIME_12_SECONDS)
		ELSE
            iReturnValue = GET_RANDOM_INT_IN_RANGE(FMMC_PED_RESPAWN_TIME_2_SECONDS, FMMC_PED_RESPAWN_TIME_6_SECONDS)
		ENDIF
    ENDIF
    
    RETURN (iReturnValue*1000)
    
ENDFUNC

FUNC eWAVE_STAGE GET_WAVE_SUB_STAGE_LIMIT(INT iWave)
	
	IF iWave <= 3
		RETURN 	eWAVESTAGE_MEDIUM
	ENDIF
	
	RETURN eWAVESTAGE_HARD
	
ENDFUNC

FUNC BOOL ON_CURRENT_WAVE_LAST_SUB_STAGE()
	
	RETURN ( serverBD.sWaveData.eStage = GET_WAVE_SUB_STAGE_LIMIT(serverBD.sWaveData.iWaveCount) )
	
ENDFUNC

FUNC BOOL HAS_DEFENSIVE_SPHERE_EXISTED_FOR_TIME(INT iSquad, INT iPed, INT iMaxDuration)
	
	IF HAS_NET_TIMER_STARTED(serverBD.structAiSquad[iSquad].sPed[iPed].stDefensiveSphereTimer)
		IF HAS_NET_TIMER_EXPIRED(serverBD.structAiSquad[iSquad].sPed[iPed].stDefensiveSphereTimer, iMaxDuration)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL FORCE_ENEMY_BLIPS_ON()
    
    IF (serverBD.sWaveData.iNumRequiredKills - serverBD.iKillsThisWave) <= NUM_LAST_ENEMIES_TO_BLIP
    	RETURN TRUE
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

#IF IS_DEBUG_BUILD
    
    PROC PRINT_INJURED_PED_DATA(NETWORK_INDEX &netId)
            
            INT iNetId, iDamager, iPedId
            TEXT_LABEL_31 tl31_weapon
            WEAPON_TYPE weapon
            PLAYER_INDEX damager 
            
            iNetId = NATIVE_TO_INT(netId)
            iPedId = NATIVE_TO_INT(NET_TO_PED(netId))
            damager = NETWORK_GET_DESTROYER_OF_NETWORK_ID(netId, weapon)
            iDamager = NATIVE_TO_INT(damager)
            tl31_weapon = GET_WEAPON_NAME(weapon)
            
            PRINTLN("[Survival] - [AI Spawning] - ||---------------------------------------||")
            PRINTLN("[Survival] - [AI Spawning] - ||  BUG 893474  						   ||")
            PRINTLN("[Survival] - [AI Spawning] - ||---------------------------------------||")
            PRINTLN("[Survival] - [AI Spawning] - ||  Dead ped net ID:       ", iNetId)
            PRINTLN("[Survival] - [AI Spawning] - ||  Dead ped ped ID:       ", iPedId)
            PRINTLN("[Survival] - [AI Spawning] - ||  Dead ped damager:      ", iDamager)
            PRINTLN("[Survival] - [AI Spawning] - ||  Dead ped killed with:  ", tl31_weapon)
            PRINTLN("[Survival] - [AI Spawning] - ||---------------------------------------||")
            
    ENDPROC
    
#ENDIF

#IF IS_DEBUG_BUILD
    BOOL bDo893474Prints
#ENDIF

PROC TRACK_SQUAD_ENEMY_KILL_STATS(INT iSquad, INT iPed, BOOL bInjured)
	
	SWITCH serverBD.structAiSquad[iSquad].sPed[iPed].iEnemyTrackKillsStage
		
		CASE 0
			IF NOT bInjured
				serverBD.iNumCreatedEnemies++
				serverBD.structAiSquad[iSquad].sPed[iPed].iEnemyTrackKillsStage++
				PRINTLN("[Survival] - [AI Spawning] - TRACK_SQUAD_ENEMY_KILL_STATS - ", iPed, " bInjured = FALSE")
				PRINTLN("[Survival] - [AI Spawning] - TRACK_SQUAD_ENEMY_KILL_STATS - ", iPed, " iNumCreatedEnemies = ", serverBD.iNumCreatedEnemies)
				PRINTLN("[Survival] - [AI Spawning] - TRACK_SQUAD_ENEMY_KILL_STATS - ", iPed, " iEnemyTrackKillsStage = ", serverBD.structAiSquad[iSquad].sPed[iPed].iEnemyTrackKillsStage)
			ENDIF
		BREAK
		
		CASE 1
			IF bInjured
				serverBD.iKills++
                serverBD.iKillsThisWave++
                serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage]++
				serverBD.structAiSquad[iSquad].sPed[iPed].iEnemyTrackKillsStage = 0
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " bInjured = TRUE")
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iKills = ", serverBD.iKills)
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iKillsThisWave = ", serverBD.iKillsThisWave)
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iKillsThisWaveSubStage = ", serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage])
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iNumRequiredKills = ", serverBD.sWaveData.iNumRequiredKills)
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iNumRequiredSubstageKills = ", serverBD.sWaveData.iNumRequiredSubstageKills[ENUM_TO_INT(serverBD.sWaveData.eStage)])
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iEnemyTrackKillsStage = ", serverBD.structAiSquad[iSquad].sPed[iPed].iEnemyTrackKillsStage)
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC PROCESS_ENEMY_PED_BRAINS()
    
    INT i, j
	BOOL bPedInjured
	
	// If we are not in the fighting stage, setup searching for the first ped for when we do go into the fighting stage again.
    IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_PASSED_WAVE
    	serverBd.iSearchingForPed = (-1)
    ENDIF
    
	// Start of enemy loop, num enemies = 0, this will get counted as the loops progresses.
	serverBD.iNumAliveSquadEnemies = 0
	
	REPEAT MAX_NUM_ENEMY_SQUADS j
		
		// Process this squad if it active.
        IF serverBD.structAiSquad[j].eState = eSQUADSTATE_ACTIVE
			
			SWITCH serverBD.iSquadSpawningStage
				
				CASE 0
					
					#IF IS_DEBUG_BUILD
						IF bPedSpawningBug
							PRINTLN("[Survival] - [AI Spawning] - squad state = eSQUADSTATE_ACTIVE. Squad = ", j)
						ENDIF
					#ENDIF
								
					// Enemy loop.
				    REPEAT MAX_NUM_PEDS_IN_SQUAD i
				       	
						// Store if the enemy is injured so we only need to call this once in the loop for the current ped.
						bPedInjured = IS_NET_PED_INJURED(serverBD.structAiSquad[j].sPed[i].netId)
						
						// If the enemy is not injured, increment num alive enemies count.
						IF NOT bPedInjured
							serverBD.iNumAliveSquadEnemies++
						ENDIF
						
						// Track the number of enemies created and killed. this is needed for end of wave calculations elsewhere.
						TRACK_SQUAD_ENEMY_KILL_STATS(j, i, bPedInjured)
						
						#IF IS_DEBUG_BUILD
							IF bPedSpawningBug
								PRINTLN("[Survival] - [AI Spawning] - vSquadSpawnPoint != zero. Ped = ", i)
							ENDIF
						#ENDIF
						
						// Enemy state machine switch.
				        SWITCH serverBD.structAiSquad[j].sPed[i].eState
				                
				            // Spawning state. This is where we create our enemies.
				            CASE eENEMYSTATE_SPAWNING
				                
								#IF IS_DEBUG_BUILD
									IF bPedSpawningBug
										PRINTLN("[Survival] - [AI Spawning] - in enemy state spawning. Ped = ", i)
										PRINTLN("[Survival] - [AI Spawning] - iSearchingForPed = ", serverBd.iSearchingForPed)
										PRINTLN("[Survival] - [AI Spawning] - iSearchingForChopper = ", serverBd.iSearchingForChopper)
										PRINTLN("[Survival] - [AI Spawning] - iSearchingForLandVehicle = ", serverBd.iSearchingForLandVehicle)
									ENDIF
								#ENDIF
								
								// If we are searching for this ped.
								IF ((serverBd.iSearchingForPed = -1) AND (serverBd.iSearchingForChopper = -1) AND (serverBd.iSearchingForLandVehicle = -1))
		                        OR ((serverBd.iSearchingForPed = GET_SPAWN_PED_UNIQUE_ID(i, j)) AND (serverBd.iSearchingForChopper = -1) AND (serverBd.iSearchingForLandVehicle = -1))
									
									#IF IS_DEBUG_BUILD
										IF bPedSpawningBug
											PRINTLN("[Survival] - [AI Spawning] - iSearchingForPed = GET_SPAWN_PED_UNIQUE_ID(i, j) or = (-1). Ped = ", i)
										ENDIF
									#ENDIF
										
									// Spawn the ped.
	                                IF RESPAWN_MISSION_PED(i, j)
										START_NET_TIMER(serverBD.structAiSquad[j].sPed[i].stDefensiveSphereTimer)
	                                    serverBD.structAiSquad[j].sPed[i].eState = eENEMYSTATE_FIGHTING
	                                    serverBd.iSearchingForPed = -1
										serverBD.iSquadSpawningStage = 0
	                                    PRINTLN("[Survival] - [AI Spawning] - ped ", i, " brain, going to eENEMYSTATE_FIGHTING.")
	                                ENDIF
										
								ENDIF
								
			                BREAK
			                
			                CASE eENEMYSTATE_FIGHTING
			                    
								// If the ped is injured.
			                    IF bPedInjured
			                        
									// Set to the dead state.
		                           	serverBD.structAiSquad[j].sPed[i].eState = eENEMYSTATE_DEAD
		                            PRINTLN("[Survival] - [AI Spawning] - ped ", i, " brain, ped net injured, going to state eENEMYSTATE_DEAD.") 
		                           
		                            #IF IS_DEBUG_BUILD
		                            IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.structAiSquad[j].sPed[i].netId)
		                                PRINT_INJURED_PED_DATA(serverBD.structAiSquad[j].sPed[i].netId)
		                            ENDIF
		                            #ENDIF
			                    	
								ELSE
									
									#IF IS_DEBUG_BUILD
										IF bPedSpawningBug
											VECTOR vTempVector
											vTempVector = GET_ENTITY_COORDS(NET_TO_PED(serverBD.structAiSquad[j].sPed[i].netId))
											PRINTLN("[Survival] - [AI Spawning] - alive ped = ", i)
											PRINTLN("[Survival] - [AI Spawning] - alive ped coords = ", vTempVector)
										ENDIF
									#ENDIF
									
			                    ENDIF
			                    
			                BREAK
			                
			                // Process spawning stage every frame because HAS_NET_TIMER_EXPIRED() needs to be called every frame.
			                CASE eENEMYSTATE_DEAD
							
								// Do nothing for now, ped is dead.
								#IF IS_DEBUG_BUILD
									IF bPedSpawningBug
										PRINTLN("[Survival] - [AI Spawning] - ped ", i, " in state eENEMYSTATE_DEAD.")
									ENDIF
								#ENDIF
					
			                BREAK
							
		                ENDSWITCH
						
			 		ENDREPEAT
					
				BREAK
				
				CASE 1
					
					serverBD.iSquadSpawningStage = 0
					
				BREAK
				
			ENDSWITCH
			
		ENDIF
	    
	ENDREPEAT
	
ENDPROC

PROC MAINTAIN_ANGRY_SHOUTING(PED_INDEX pedId, BOOL &bPlayedAngryShout)
	
	IF NOT bPlayedAngryShout
		
		PLAYER_INDEX playerId
		VECTOR vPedCoords, vPlayerCoords
		INT iPlayer
		
		iPlayer = GET_NEAREST_PLAYER_TO_ENTITY(pedId)
		playerId = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		
		IF IS_NET_PLAYER_OK(playerId)
			
			vPedCoords = GET_ENTITY_COORDS(pedId)
			vPlayerCoords = GET_ENTITY_COORDS(GET_PLAYER_PED(playerId))
			
			IF VDIST2(vPedCoords, vPlayerCoords) <= 400
				IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
					PLAY_PED_AMBIENT_SPEECH(pedId, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE_SHOUTED)
					bPlayedAngryShout = TRUE
				ELSE
					bPlayedAngryShout = TRUE
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_CHALLENGE_TIMER()
	
	IF IS_PLAYLIST_DOING_CHALLENGE()
		INT iCurrentScoreToBeat = GET_CURRENT_MISSION_TIME_SCORE_TO_BEAT()
		DRAW_GENERIC_SCORE(iCurrentScoreToBeat, "SCORE_CHALLSC")
	ENDIF
	
ENDPROC

PROC PROCESS_SQUAD_ENEMY_PED_BODIES()

    INT i, j
    PED_INDEX pedId
    BOOL bFrustratedAdvance
	
	REPEAT MAX_NUM_ENEMY_SQUADS j
		
		// Process this squad if it active.
        IF serverBD.structAiSquad[j].eState = eSQUADSTATE_ACTIVE
			
			// Enemy loop.
		    REPEAT MAX_NUM_PEDS_IN_SQUAD i
				
		      	UPDATE_ENEMY_NET_PED_BLIP(	serverBD.structAiSquad[j].sPed[i].netId, 
											structSquadEnemyBlip[j][i],
											DEFAULT,
											FALSE, 
											FORCE_ENEMY_BLIPS_ON() )
		      	
		        IF NOT IS_NET_PED_INJURED(serverBD.structAiSquad[j].sPed[i].netId)
		            
					pedId = NET_TO_PED(serverBD.structAiSquad[j].sPed[i].netId)
					
					// Always make sure the ped has a weapon if they don't have one.
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.structAiSquad[j].sPed[i].netId)
						IF GET_PEDS_CURRENT_WEAPON(pedId) = WEAPONTYPE_UNARMED
							SETUP_PED_FOR_WAVE(j,i,pedId,FALSE,TRUE)
							PRINTLN("[Survival] - ped has become unarmed, giving weapon to ped. Squad = ", j, ", Ped = ", i)
						ENDIF
					ENDIF
					
		            SWITCH serverBD.structAiSquad[j].sPed[i].eState
		                    
		                CASE eENEMYSTATE_FIGHTING
		            		         
		                    IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.structAiSquad[j].sPed[i].netId)
								
								MAINTAIN_ANGRY_SHOUTING(pedId, serverBD.structAiSquad[j].sPed[i].bPlayedAngryShout)
								
								// Make fight.
		                        IF NOT IS_PED_IN_COMBAT(pedId)
									IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK
		                            	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
		                            	TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.0)
									ENDIF
		                        ENDIF
								
								// If the ped gets stuck waiting opn collision, load it around them so they can get moving again.
								IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(pedId)
									SET_ENTITY_LOAD_COLLISION_FLAG(pedId, TRUE)
								ENDIF
								
								// If on foot and with no weapon in hand, put a weapon in hand so they don;t flee.
								
								
								// Be frustrated if only a few commrades left.
								IF serverBD.iNumAliveSquadEnemies <= 3
									bFrustratedAdvance = TRUE
								ENDIF
								
								SET_PED_COMBAT_ATTRIBUTES(pedId, CA_CAN_USE_FRUSTRATED_ADVANCE, bFrustratedAdvance)
								
		                    ENDIF
		                    
		                BREAK
		                
		            ENDSWITCH
		            
		        ENDIF
		        
		    ENDREPEAT
			
		ENDIF
		
	ENDREPEAT
 	
ENDPROC

/// PURPOSE:
///    Gets if the end mission reason is one that requires the local player to go to SURVIVAL_GAME_STATE_WAITING_TO_LEAVE.
/// RETURNS:
///    TRUE if should go to SURVIVAL_GAME_STATE_WAITING_TO_LEAVE, FALSE if not (most means should go straight to SURVIVAL_GAME_STATE_END).
FUNC BOOL SHOULD_GO_TO_WAITING_TO_LEAVE_STATE()
//    
//	// If a debug pass was used def go to wait to leave state.
//    #IF IS_DEBUG_BUILD
//        IF serverBD.iSfSkipParticipant != (-1)
//            RETURN TRUE
//        ENDIF
//    #ENDIF
//    
//    IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
//        IF NOT bIamQuittingCoronaSpectator
//            SWITCH GET_END_HORDE_REASON()
//                CASE eENDHORDEREASON_BEAT_ALL_WAVES             RETURN TRUE
//                CASE eENDHORDEREASON_ALL_PLAYERS_DEAD   RETURN TRUE
//            ENDSWITCH
//        ENDIF
//    ENDIF
//            
    RETURN TRUE
    
ENDFUNC

/// PURPOSE:
///    Gets if all script participants are in SURVIVAL_GAME_STATE_WAITING_TO_LEAVE.
/// RETURNS:
///    TRUE if all participants are in SURVIVAL_GAME_STATE_WAITING_TO_LEAVE, FALSE if not.
FUNC BOOL ARE_ALL_PARTICIPANTS_WAITING_TO_LEAVE()
    
    INT i
    
    REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
        IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
            IF GET_CLIENT_GAME_STATE(i) != SURVIVAL_GAME_STATE_WAITING_TO_LEAVE
                RETURN FALSE
            ENDIF
        ENDIF
    ENDREPEAT
    
    RETURN TRUE
    
ENDFUNC

FUNC BOOL ARE_ALL_PARTICIPANTS_READY_TO_DO_START_FADE_IN()
    
	INT i, iPart
    PLAYER_INDEX playerID
	
    REPEAT NUM_NETWORK_PLAYERS i
//		PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - checking player ", i)
		IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i))
//			PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " is net ok")
			playerID = INT_TO_NATIVE(PLAYER_INDEX, i)
			IF NOT IS_PLAYER_SCTV(playerID)
//				PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " is not sctv")
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
			        IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(playerID))
	//					PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " part is active")
						iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerID))
			            IF NOT IS_BIT_SET(playerBD[iPart].iBitset, BITSET_PLAYER_READY_FOR_START_FADE_IN)
	//						PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " BITSET_PLAYER_READY_FOR_START_FADE_IN is not set.")
			                RETURN FALSE
			            ENDIF
			        ENDIF
				ENDIF
			ENDIF
		ENDIF
    ENDREPEAT
    
    RETURN TRUE
	
ENDFUNC

FUNC BOOL HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN()
    
    INT i, iPart
    PLAYER_INDEX playerID
	
    REPEAT NUM_NETWORK_PLAYERS i
//		PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - checking player ", i)
		IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i))
//			PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " is net ok")
			playerID = INT_TO_NATIVE(PLAYER_INDEX, i)
			IF NOT IS_PLAYER_SCTV(playerID)
//				PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " is not sctv")
		        IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(playerID))
//					PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " part is active")
					iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerID))
		            IF NOT IS_BIT_SET(playerBD[iPart].iBitset, BITSET_PLAYER_COMPLETED_START_FADE_IN)
//						PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " BITSET_PLAYER_COMPLETED_START_FADE_IN is not set.")
		                RETURN FALSE
		            ENDIF
		        ENDIF
			ENDIF
		ENDIF
    ENDREPEAT
    
    RETURN TRUE
    
ENDFUNC

PROC DISPLAY_SCOREBOARD(STRUCT_HORDE_BOARD_DATA &sBoardData, FLOAT fRespectUpOffset, INT iXp, BOOL bDoEndModeBoard)
    
    INT iScoretoDisplay
    
    // Xp reward.
    SET_TEXT_FONT(FONT_CURSIVE)
    SET_TEXT_SCALE(0.6000, 0.6000)
    SET_TEXT_COLOUR(255, 255, 255, 255)
    SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
    DISPLAY_TEXT(0.38, 0.4250+fRespectUpOffset, "HRD_ENDWVXP")
    
    SET_TEXT_FONT(FONT_CURSIVE)
    SET_TEXT_SCALE(0.6000, 0.6000)
    SET_TEXT_COLOUR(255, 255, 255, 255)
    SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
    DISPLAY_TEXT_WITH_NUMBER(0.60, 0.4250+fRespectUpOffset, "HRD_KILLSPLYAM", iXp)
    
    // Team kills.
    SET_TEXT_FONT(FONT_CURSIVE)
    SET_TEXT_SCALE(0.6000, 0.6000)
    SET_TEXT_COLOUR(255, 255, 255, 255)
    SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
    DISPLAY_TEXT(0.38, 0.4750+fRespectUpOffset, "HRD_KILLSTEAM")
    
    SET_TEXT_FONT(FONT_CURSIVE)
    SET_TEXT_SCALE(0.6000, 0.6000)
    SET_TEXT_COLOUR(255, 255, 255, 255)
    SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
    
    IF bDoEndModeBoard
    	iScoreToDisplay = serverBD.iKills
    ELSE
    	iScoreToDisplay = serverBD.iKillsThisWave
    ENDIF
    
    DISPLAY_TEXT_WITH_NUMBER(0.60, 0.4750+fRespectUpOffset, "HRD_KILLSPLYAM", iScoreToDisplay)
    
    // Player in 1st.
    IF sBoardData.hudPlayers[0] != INVALID_PLAYER_INDEX()
            IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[0], FALSE)
                    
                    SET_TEXT_FONT(FONT_CURSIVE)
                    SET_TEXT_SCALE(0.6000, 0.6000)
                    SET_TEXT_COLOUR(255, 255, 255, 255)
                    SET_TEXT_CENTRE(FALSE)
                    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
                    SET_TEXT_EDGE(0, 0, 0, 0, 0)
                    DISPLAY_TEXT_WITH_PLAYER_NAME(0.38, 0.5250+fRespectUpOffset, "HRD_KILLSPLYNM", GET_PLAYER_NAME(sBoardData.hudPlayers[0]), HUD_COLOUR_WHITE)
                    
                    SET_TEXT_FONT(FONT_CURSIVE)
                    SET_TEXT_SCALE(0.6000, 0.6000)
                    SET_TEXT_COLOUR(255, 255, 255, 255)
                    SET_TEXT_CENTRE(FALSE)
                    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
                    SET_TEXT_EDGE(0, 0, 0, 0, 0)
                    DISPLAY_TEXT_WITH_NUMBER(0.60, 0.5250+fRespectUpOffset, "HRD_KILLSPLYAM", sBoardData.iScores[0])
                    
            ENDIF
    ENDIF
    
    // Player in 2nd.
    IF sBoardData.hudPlayers[1] != INVALID_PLAYER_INDEX()
            IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[1], FALSE)
                    
                    SET_TEXT_FONT(FONT_CURSIVE)
                    SET_TEXT_SCALE(0.6000, 0.6000)
                    SET_TEXT_COLOUR(255, 255, 255, 255)
                    SET_TEXT_CENTRE(FALSE)
                    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
                    SET_TEXT_EDGE(0, 0, 0, 0, 0)
                    DISPLAY_TEXT_WITH_PLAYER_NAME(0.38, 0.5750+fRespectUpOffset, "HRD_KILLSPLYNM",GET_PLAYER_NAME(sBoardData.hudPlayers[1]), HUD_COLOUR_WHITE)
                    
                    SET_TEXT_FONT(FONT_CURSIVE)
                    SET_TEXT_SCALE(0.6000, 0.6000)
                    SET_TEXT_COLOUR(255, 255, 255, 255)
                    SET_TEXT_CENTRE(FALSE)
                    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
                    SET_TEXT_EDGE(0, 0, 0, 0, 0)
                    DISPLAY_TEXT_WITH_NUMBER(0.60, 0.5750+fRespectUpOffset, "HRD_KILLSPLYAM", sBoardData.iScores[1])
                    
            ENDIF
    ENDIF
    
    // Player in 3rd.
    IF sBoardData.hudPlayers[2] != INVALID_PLAYER_INDEX()
            IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[2], FALSE)
                    
                    SET_TEXT_FONT(FONT_CURSIVE)
                    SET_TEXT_SCALE(0.6000, 0.6000)
                    SET_TEXT_COLOUR(255, 255, 255, 255)
                    SET_TEXT_CENTRE(FALSE)
                    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
                    SET_TEXT_EDGE(0, 0, 0, 0, 0)
                    DISPLAY_TEXT_WITH_PLAYER_NAME(0.38, 0.6250+fRespectUpOffset, "HRD_KILLSPLYNM", GET_PLAYER_NAME(sBoardData.hudPlayers[2]), HUD_COLOUR_WHITE)
                    
                    SET_TEXT_FONT(FONT_CURSIVE)
                    SET_TEXT_SCALE(0.6000, 0.6000)
                    SET_TEXT_COLOUR(255, 255, 255, 255)
                    SET_TEXT_CENTRE(FALSE)
                    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
                    SET_TEXT_EDGE(0, 0, 0, 0, 0)
                    DISPLAY_TEXT_WITH_NUMBER(0.60, 0.6250+fRespectUpOffset, "HRD_KILLSPLYAM", sBoardData.iScores[2])
                    
            ENDIF
    ENDIF
    
    // Player in 4th.
    IF sBoardData.hudPlayers[3] != INVALID_PLAYER_INDEX()
            IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[3], FALSE)
                    
                    SET_TEXT_FONT(FONT_CURSIVE)
                    SET_TEXT_SCALE(0.6000, 0.6000)
                    SET_TEXT_COLOUR(255, 255, 255, 255)
                    SET_TEXT_CENTRE(FALSE)
                    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
                    SET_TEXT_EDGE(0, 0, 0, 0, 0)
                    DISPLAY_TEXT_WITH_PLAYER_NAME(0.38, 0.6750+fRespectUpOffset, "HRD_KILLSPLYNM", GET_PLAYER_NAME(sBoardData.hudPlayers[3]), HUD_COLOUR_WHITE)
                    
                    SET_TEXT_FONT(FONT_CURSIVE)
                    SET_TEXT_SCALE(0.6000, 0.6000)
                    SET_TEXT_COLOUR(255, 255, 255, 255)
                    SET_TEXT_CENTRE(FALSE)
                    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
                    SET_TEXT_EDGE(0, 0, 0, 0, 0)
                    DISPLAY_TEXT_WITH_NUMBER(0.60, 0.6750+fRespectUpOffset, "HRD_KILLSPLYAM",sBoardData.iScores[3])
                    
            ENDIF
    ENDIF
    
    
ENDPROC

FUNC INT GET_END_OF_WAVE_XP_AMOUNT()

    // If failed then XP amount to display is zero.
    IF GET_END_HORDE_REASON() = eENDHORDEREASON_ALL_PLAYERS_DEAD
    	RETURN 0
    ENDIF
    
    RETURN serverBD.sWaveData.iWaveCount*END_OF_WAVE_XP_MULTIPLIER
    
ENDFUNC

PROC HANDLE_END_OF_WAVE_XP(INT &iXp)
    
    IF NOT bGivenEndOfWaveXp
		
		iXp = ROUND((iXp*g_sMPTunables.fxp_tunable_Survival_Wave_reached))
		PRINTLN("[Survival] - [Cash_Xp] - HANDLE_END_OF_WAVE_XP - applying tuneable fxp_tunable_Survival_Wave_reached to iXp. tuneable = ", g_sMPTunables.fxp_tunable_Survival_Wave_reached, ", new xp value = ", iXp)
		
		IF iXp > g_sMptunables.iSurvivalRPCapPerWave
			iXp = g_sMptunables.iSurvivalRPCapPerWave
			PRINTLN("[Survival] - [Cash_Xp] - HANDLE_END_OF_WAVE_XP - iXp > g_sMptunables.iSurvivalRPCapPerWave, setting iXp = g_sMptunables.iSurvivalRPCapPerWave. g_sMptunables.iSurvivalRPCapPerWave = ", g_sMptunables.iSurvivalRPCapPerWave)
		ENDIF
		
       	iXp = GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_JOBFM, "XPT_ENDHRD", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_SURVIVAL_WAVE, iXp, 1, -1, TRUE)
		playerBD[PARTICIPANT_ID_TO_INT()].iXpGained += iXp
		iTotalWaveAwardXp += iXp
		ADD_TO_END_CELBRATION_SCORE(iXp)
		
		#IF IS_DEBUG_BUILD
		IF (serverBD.sWaveData.iWaveCount-1) >= 0
		AND (serverBD.sWaveData.iWaveCount-1) < NUMBER_HORDE_WAVES
			iEndOfWaveXp[(serverBD.sWaveData.iWaveCount-1)] = iXp
		ENDIF
        #ENDIF
		
		bGivenEndOfWaveXp = TRUE
		
    ENDIF
    
ENDPROC

PROC SETUP_MISSION_DATA_BOX(STRUCT_HORDE_BOARD_DATA &sBoardData, BIG_MESSAGE_TYPE eMessageType, INT iWaveXp, INT iCash, INT iTotalXp = 0)
	
	INT i
	INT iScoretoDisplay
	INT iNumPlayersToBeDisplayed
	
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
		
		IF sBoardData.hudPlayers[i] != INVALID_PLAYER_INDEX()
       		IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[i], FALSE)
				iScoretoDisplay += sBoardData.iScores[i]
				iNumPlayersToBeDisplayed++
			ENDIF
		ENDIF
	ENDREPEAT
	
	ADD_MISSION_SUMMARY_TITLE("FMMC_MPM_TY4")
	
	// If we have finished the mode, display the total XP earned.
	IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
		ADD_MISSION_SUMMARY_STAT_WITH_INT("MBOX_SURTOT_XP", iTotalXp)
	ENDIF
	
	ADD_MISSION_SUMMARY_STAT_WITH_INT("MBOX_WAVE_XP", iWaveXp)
	
//	ADD_MISSION_SUMMARY_END_COMPLETION_XP("MBOX_XP", iXp)
	
	IF eMessageType != BIG_MESSAGE_WAVE_COMPLETE
		ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("HRD_CASHGIVEN", iCash)
		ADD_MISSION_SUMMARY_STAT_WITH_INT("HRD_WVRCHED", serverBD.sWaveData.iWaveCount)
	ELSE
		ADD_MISSION_SUMMARY_STAT_STRING_WITH_TWO_INTS("HRD_WAVEOUT", serverBD.sWaveData.iWaveCount, NUMBER_HORDE_WAVES)
	ENDIF
	
	IF iNumPlayersToBeDisplayed >= 2
		ADD_MISSION_SUMMARY_STAT_WITH_INT("HRD_KILLSTEAM", iScoreToDisplay)
	ENDIF
	
	IF sBoardData.hudPlayers[0] != INVALID_PLAYER_INDEX()
        IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[0], FALSE)
			ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(sBoardData.hudPlayers[0], sBoardData.iScores[0])
		ENDIF
	ENDIF
	
	IF sBoardData.hudPlayers[1] != INVALID_PLAYER_INDEX()
	    IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[1], FALSE)
			ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(sBoardData.hudPlayers[1], sBoardData.iScores[1])
		ENDIF
	ENDIF
	
	IF sBoardData.hudPlayers[2] != INVALID_PLAYER_INDEX()
	    IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[2], FALSE)
			ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(sBoardData.hudPlayers[2], sBoardData.iScores[2])
		ENDIF
	ENDIF
	
	IF sBoardData.hudPlayers[3] != INVALID_PLAYER_INDEX()
	    IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[3], FALSE)
			ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(sBoardData.hudPlayers[3], sBoardData.iScores[3])
		ENDIF
	ENDIF
	
	SETUP_NEW_BIG_MESSAGE(eMessageType) //SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(eMessageType, iMessageValue, iXp, sStrapline) 
	
ENDPROC

/// PURPOSE:
///    Gets the wave the player survived until. sibtracts one from the current wave, unless the survival has been completed.
/// RETURNS:
///    INT - wave survived to.
FUNC INT GET_WAVE_SURVIVED_UNTIL()
	
	// If we have completed the survival, we have survived to max wave count.
	IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
		RETURN NUMBER_HORDE_WAVES
	ENDIF
	
	// One less than the current wave (if we died on the current wave then we didn't survive it).
	RETURN (serverBD.sWaveData.iWaveCount - 1)
	
ENDFUNC

FUNC INT GET_END_SURVIVAL_CASH_AWARD()
	
	INT iReturn
	FLOAT fReturn
	
	IF GET_END_HORDE_REASON() = eENDHORDEREASON_NOT_FINISHED_YET
		RETURN 0
	ENDIF
	
	iReturn = (GET_WAVE_SURVIVED_UNTIL()*serverBD.sWaveData.iWaveCount)*(g_sMPTunables.iSurvival_Cash_Reward)
	
	IF GET_WAVE_SURVIVED_UNTIL() >= 10
		fReturn = TO_FLOAT(iReturn)
		fReturn = fReturn*g_sMPTunables.FSURVIVAL_COMPLETE_BONUS
		iReturn = ROUND(fReturn)
		PRINTLN("[Survival] - survived until wave 10 - multiplaying RP by ", g_sMPTunables.FSURVIVAL_COMPLETE_BONUS, " RP = ", iReturn)
	ENDIF
	
	RETURN iReturn
	
ENDFUNC

FUNC PLAYER_INDEX GET_PLAYER_WITH_MOST_KILLS(STRUCT_HORDE_BOARD_DATA &sBoardData)
	INT iMostKills = -1
	PLAYER_INDEX bestPlayer
	
	INT i = 0
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
		IF sBoardData.hudPlayers[i] != INVALID_PLAYER_INDEX()
			IF sBoardData.iScores[i] > iMostKills
				iMostKills = sBoardData.iScores[i]
				bestPlayer = sBoardData.hudPlayers[i]
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bestPlayer
ENDFUNC

/// PURPOSE:
///    Runs the celebration screen showing the end stats.
///    IF bEndOfWaveSummary = TRUE then this will set up the end of wave stats, otherwise it'll set up the end of job stats.
/// RETURNS:
///    TRUE once the sequence has finished.
FUNC BOOL HAS_CELEBRATION_SUMMARY_FINISHED(STRUCT_HORDE_BOARD_DATA &sBoardData, BOOL bJobPassed, BOOL bEndOfMode = FALSE)
	//Check if the screen should be displayed (e.g. if the player quit early then it should be skipped.
	
	#IF IS_DEBUG_BUILD
		SWITCH sCelebrationData.eCurrentStage
			CASE CELEBRATION_STAGE_SETUP						PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_SETUP")  						BREAK
			CASE CELEBRATION_STAGE_TRANSITIONING				PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_TRANSITIONING")  				BREAK
			CASE CELEBRATION_STAGE_DOING_FLASH					PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_DOING_FLASH")  					BREAK
			CASE CELEBRATION_STAGE_PLAYING						PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_PLAYING")  						BREAK
			CASE CELEBRATION_STAGE_END_TRANSITION				PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_END_TRANSITION")  				BREAK
			CASE CELEBRATION_STAGE_FINISHED						PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_FINISHED")  					BREAK
			CASE CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM	PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM") 	BREAK
		ENDSWITCH
	#ENDIF
	
	IF (sCelebrationData.eCurrentStage < CELEBRATION_STAGE_END_TRANSITION)
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
		OR DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(specData.specCamData)
		OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
		OR IS_PLAYER_SCTV(PLAYER_ID())
			IF NOT sCelebrationData.sCelebrationTimer.bInitialisedTimer
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
			ENDIF
		
			SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(TRUE)
		
			sCelebrationData.eCurrentStage = CELEBRATION_STAGE_END_TRANSITION
		ENDIF
	ENDIF
	
	IF sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP
		HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
	ENDIF
	
	//Handle the screen flow.
	SWITCH sCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
			BOOL bReadyToProgress
		
			SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
			SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
			//Sort out any cameras if necessary.
			IF NOT bEndOfMode
				IF NOT bAlreadyTriggeredCelebrationPostFX //Don't do any of this if we're transitioning out of spectator cam.
					//Place a camera in front of the player if safe to do so.
					BOOL bPlacedCameraSuccessfully
				
					IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camCelebrationScreen, bPlacedCameraSuccessfully)				
						IF bPlacedCameraSuccessfully
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
						ELSE
							//1626331 - If the celebration cam didn't successfully create then turn the ped to face the game cam.
							IF IS_NET_PLAYER_OK(PLAYER_ID())
							AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								TASK_TURN_PED_TO_FACE_COORD(PLAYER_PED_ID(), GET_FINAL_RENDERED_CAM_COORD(), 2000)
							ENDIF
						ENDIF
						
						//Reset the action mode to the default one.
						IF IS_NET_PLAYER_OK(PLAYER_ID())
							SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
							SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1, "DEFAULT_ACTION")
							
							ANIMPOSTFX_STOP_ALL()
						ENDIF
						
						IF bJobPassed
						OR NOT bEndOfMode
							PLAY_CELEB_WIN_POST_FX()
							PRINTLN("[Survival] - playing MP_Celeb_Win, call 1")
						ELSE
							PLAY_CELEB_LOSE_POST_FX()
							PRINTLN("[Survival] - playing MP_Celeb_Lose, call 2")
						ENDIF
						PLAY_SOUND_FRONTEND(-1, "MP_WAVE_COMPLETE", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						
						bReadyToProgress = TRUE
					ENDIF
				ENDIF
			ELSE
				bReadyToProgress = TRUE
			ENDIF
			
			IF bReadyToProgress
				CLEAR_CELEBRATION_SCREEN_MOVIE_IDS(sCelebrationData)
				REQUEST_CELEBRATION_SCREEN(sCelebrationData)
				SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
				KILL_UI_FOR_CELEBRATION_SCREEN()
				HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				//1636397 - Wait a bit before triggering the scaleform
				RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
				
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_TRANSITIONING
			ENDIF
		BREAK
	
		CASE CELEBRATION_STAGE_TRANSITIONING
			REQUEST_CELEBRATION_SCREEN(sCelebrationData)
	
			//PLAYER_INDEX winningPlayer
			BOOL bFetchedStatsFromServer, bPlayerInSameCrewAsWinner
			GAMER_HANDLE sWinnerHandle
			
			IF NOT bEndOfMode
				bFetchedStatsFromServer = TRUE
				bPlayerInSameCrewAsWinner = FALSE
			ELSE
				//winningPlayer = GET_PLAYER_WITH_MOST_KILLS(sBoardData)
				//sWinnerHandle = GET_GAMER_HANDLE_PLAYER(winningPlayer)
				IF HordeLeaderboard[0].iParticipant > -1
					sWinnerHandle = lbdVars.aGamer[HordeLeaderboard[0].iParticipant] //This should be able to grab handles for players that have left already left the game.
				ENDIF
				bPlayerInSameCrewAsWinner = IS_PLAYER_IN_SAME_CREW_AS_ME(sWinnerHandle, bFetchedStatsFromServer)
			ENDIF
				
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)		
			AND bFetchedStatsFromServer
			AND IS_SCREEN_FADED_IN()
			AND HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTransitionTimer, 500)
			AND NOT ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
				
				STRING strScreenName, strBackgroundColour
				INT iCurrentRP, iCash, iCurrentLvl, iNextLvl, iRPToReachCurrentLvl, iRPToReachNextLvl, iTotalKills, iChallengePart, iTotalChallengeParts, i, iNumValidPlayers
				BOOL bWonChallengePart
				TEXT_LABEL strPlayerKills, strTotalKills
			
				//Retrieve all the required stats for the race end screen.
				strScreenName = "SUMMARY"
				strBackgroundColour = "HUD_COLOUR_BLACK"
				sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
				
				REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
					IF sBoardData.hudPlayers[i] != INVALID_PLAYER_INDEX()
			       		IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[i], FALSE)
							iTotalKills += sBoardData.iScores[i]
						ENDIF
					ENDIF
				ENDREPEAT
				
				//Build the screen
				CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour)
								
				IF NOT bEndOfMode
					ADD_WAVE_REACHED_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, serverBD.sWaveData.iWaveCount, TRUE)
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
					
					//ADD_TOTAL_KILLS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, iTotalKills)
					
					CREATE_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenName, "SURVIVAL_TABLE")
					
					BOOL bAlignLeft
					
					bAlignLeft = FALSE
					REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
						IF sBoardData.hudPlayers[i] != INVALID_PLAYER_INDEX()
							//ADD_GAMERTAG_KILLS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, GET_PLAYER_NAME(sBoardData.hudPlayers[i]), sBoardData.iScores[i], TRUE, bAlignLeft)
							strPlayerKills = sBoardData.iScores[i]
							ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenName, "SURVIVAL_TABLE", GET_PLAYER_NAME(sBoardData.hudPlayers[i]), strPlayerKills, TRUE, TRUE, FALSE)
							iNumValidPlayers++
							bAlignLeft = NOT bAlignLeft
						ENDIF
					ENDREPEAT
					
					strTotalKills = iTotalKills
					ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenName, "SURVIVAL_TABLE", "CELEB_TOTAL_KILLS", strTotalKills, FALSE, TRUE, TRUE)
					ADD_CELEBRATION_STAT_TABLE_TO_CURRENT_SCREEN(sCelebrationData, strScreenName, "SURVIVAL_TABLE")
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
				ELSE
					iCash = MULTIPLY_CASH_BY_TUNABLE(GET_END_SURVIVAL_CASH_AWARD())
					iCurrentRP = GET_PLAYER_FM_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) - iTotalWaveAwardXp
					iCurrentLvl = GET_FM_RANK_FROM_XP_VALUE(iCurrentRP) //GET_PLAYER_GLOBAL_RANK(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) //We don't want the current level, but their level before the XP was given.
					iNextLvl = iCurrentLvl + 1
					iRPToReachCurrentLvl = GET_XP_NEEDED_FOR_FM_RANK(iCurrentLvl)
					iRPToReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(iNextLvl)
					iChallengePart = g_sCurrentPlayListDetails.iPlaylistProgress
					iTotalChallengeParts = g_sCurrentPlayListDetails.iLength
					
					ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_SURVIVAL, bJobPassed, "", "", "")
					ADD_WAVE_REACHED_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, serverBD.sWaveData.iWaveCount, FALSE)
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME * 2
					
					IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
						ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_PART_HEAD_TO_HEAD, iChallengePart, iTotalChallengeParts, bPlayerInSameCrewAsWinner)
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
					ELIF IS_PLAYLIST_DOING_CHALLENGE()
						IF playerBD[PARTICIPANT_ID_TO_INT()].iSurvivalScore >= g_sCurrentPlayListDetails.sLoadedMissionDetails[g_sCurrentPlayListDetails.iCurrentPlayListPosition].iBestScore
							bWonChallengePart = TRUE
						ENDIF
					
						ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_PART_CREW, iChallengePart, iTotalChallengeParts, bWonChallengePart)
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
					ENDIF
					
					IF iCash != 0
						ADD_CASH_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, iCash)
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
					ENDIF
					
					IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
						ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, iEndCelebrationScreenScore, iCurrentRP, 
												   	  	   				  iRPToReachCurrentLvl, iRPToReachNextLvl, iCurrentLvl, iNextLvl)
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
						
						IF iCurrentRP + iEndCelebrationScreenScore > iRPToReachNextLvl
							sCelebrationData.iEstimatedScreenDuration += ((CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 2) + (CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 4))
						ENDIF
					ENDIF
					
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_WIPE_TIME
				ENDIF				
				
				ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)

				//Audio scenes: see B*1642903 for implementation notes.
				IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
					START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
				ENDIF

				IF bEndOfMode
					IF NOT bJobPassed
						INVISIBLE_FOR_LBD()
					ENDIF
					
					SET_SKYFREEZE_FROZEN()
				
					STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
					START_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
					PRINTLN("[Survival] - called START_AUDIO_SCENE(MP_JOB_CHANGE_RADIO_MUTE).")
					
					SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(TRUE)
				ELSE
					//Leave the kills table up a bit longer.
					ADD_PAUSE_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_SCREEN_STAT_DISPLAY_TIME + (CELEBRATION_SCREEN_STAT_WIPE_TIME * iNumValidPlayers))
					sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_WIPE_TIME * (iNumValidPlayers + 1))
				ENDIF
				
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
				RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
				
			ELSE
				
				IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
					IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.timerDeathPostFxDelay)
						START_NET_TIMER(sCelebrationData.timerDeathPostFxDelay)
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, started timer timerDeathPostFxDelay.")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sCelebrationData.timerDeathPostFxDelay, 1000)
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, timer timerDeathPostFxDelay >= 1000. Calling CLEAR_KILL_STRIP_DEATH_EFFECTS.")
							CLEAR_KILL_STRIP_DEATH_EFFECTS()
						ENDIF
					ENDIF	
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
			
			//Detach the cam on the player if they're stationary to stop the weird swings if the player turns slightly.
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND DOES_CAM_EXIST(camCelebrationScreen)
				IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 0.1
					DETACH_CAM(camCelebrationScreen)
					STOP_CAM_POINTING(camCelebrationScreen)
				ENDIF
			ENDIF
			
			//Once the timer finishes then progress: this depends on how many elements were added to the screen.
//			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration - (CELEBRATION_SCREEN_STAT_WIPE_TIME / 2))
			IF HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()	
				ANIMPOSTFX_STOP_ALL()
			
				IF NOT bEndOfMode
					ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
					PRINTLN("[Survival] - playing MP_Celeb_Win, call 3")
					PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
					START_CELEBRATION_CAMERA_ZOOM_TRANSITION(camCelebrationScreen)
				ELIF bJobPassed
					ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
					PRINTLN("[Survival] - playing MP_Celeb_Win, call 4")
				ELSE
					ANIMPOSTFX_PLAY("MP_Celeb_Lose_Out", 0, FALSE)
				ENDIF
			
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
			
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_END_TRANSITION
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_END_TRANSITION
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)

			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 1000)
			OR bEndOfMode
				BOOL bReadyToContinue
			
				IF bEndOfMode
					CDEBUG1LN(DEBUG_MISSION, "[Survival] - [NETCELEBRATION] Survival has ended, starting skyswoop")
					
					IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
						STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						PRINTLN("[Survival] - MP_CELEB_SCREEN_SCENE is active, calling STOP_AUDIO_SCENE(MP_CELEB_SCREEN_SCENE). Call B.")
					ENDIF
					
					IF SET_SKYSWOOP_UP(TRUE, TRUE, FALSE, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
						//1632888 - Keep the screen blurry and frozen at the top of the skyswoop (this should be cleaned up when the skyswoop starts descending).
						//Currently commented out as part of 1638857.
						//SET_SKYBLUR_BLURRY() 
						//SET_SKYFREEZE_FROZEN()
					
						bReadyToContinue = TRUE
					ELSE
						IF GET_SKYFREEZE_STAGE() = SKYFREEZE_FROZEN
							IF IS_SWITCH_TO_MULTI_FIRSTPART_FINISHED()
								SET_SKYFREEZE_CLEAR(TRUE)
								SET_SKYBLUR_CLEAR()
								TOGGLE_RENDERPHASES(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_MISSION, "[Survival] - [NETCELEBRATION] Survival hasn't ended, removing scripted celebration cam.")
				
					IF DOES_CAM_EXIST(camCelebrationScreen)
						DESTROY_CAM(camCelebrationScreen, TRUE)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					ENDIF
					
					bReadyToContinue = TRUE
				ENDIF				
			
				IF bReadyToContinue
					PRINTLN("[Survival] - [NETCELEBRATION] bReadyToContinue = TRUE.")
					CLEAR_ALL_BIG_MESSAGES()
					STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
					CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
					SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
					
					IF NOT bEndOfMode
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
					
					sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
				
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT INCREMENT_POSITION_FOR_PLAYLIST_LEADERBOARD(INT iValueToAdjust)
	
	RETURN (iValueToAdjust+1)
	
ENDFUNC

FUNC BOOL IS_INT_WITHIN_PARTICIPANT_RANGE(INT iInt)
	
	IF iInt >= 0
	AND iInt <= (NETWORK_GET_MAX_NUM_PARTICIPANTS()-1)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DISPLAY_END_OF_WAVE_TEXT(BOOL bEndOfMode, BOOL bPassedFailed)
   	
    INT iXp, iCounter, iCash, iParticipant, iScore
	BOOL bMoveOn = TRUE
	
	// No weapon wheel during end of wave HUD.
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		ENDIF
	ENDIF
	
	SWITCH iEndOfWaveHudStage
		
		CASE 0
			
			IF bEndOfMode
				
				IF NOT sEndBoardData.bPopulatedHudPlayerArray
					
					GET_PLAYER_KILLS(sEndBoardData, bEndOfMode)
					POPULATE_HUD_PLAYER_ARRAY(sEndBoardData, bDoWaveCompleteHudPrints, bEndOfMode)
					sEndBoardData.bPopulatedHudPlayerArray = TRUE
				
				ENDIF
				
				IF sEndBoardData.bPopulatedHudPlayerArray
					
					REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS iCounter
						IF sEndBoardData.hudPlayers[iCounter] != INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(sEndBoardData.hudPlayers[iCounter], FALSE)
								iParticipant = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(sEndBoardData.hudPlayers[iCounter]))
								IF IS_INT_WITHIN_PARTICIPANT_RANGE(iParticipant)
									IF playerBD[iParticipant].iSurvivalScore <= (-1)
										PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - case 0 - particpant ", iParticipant, " has not calculated their survival score yet, bMoveOn = FALSE.")
										bMoveOn = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
						PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - case 0 - BITSET_PLAYER_ABANDONED_HORDE is set, bMoveOn = TRUE.")
						bMoveOn = TRUE
					ENDIF
					
					IF DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(specData.specCamData)
						PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - case 0 - all participants have calculated their survival score, bMoveOn = TRUE.")
						bMoveOn = TRUE
					ENDIF
					
					IF bMoveOn
						PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - case 0 - DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE is TRUE, bMoveOn = TRUE.")
						iEndOfWaveHudStage++
					ENDIF
					
				ENDIF
				
			ELSE
			
				PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - case 0 - bEndOfMode = FALSE.")
				iEndOfWaveHudStage++
			
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF NOT sEndBoardData.bPopulatedHudPlayerArray
				
				GET_PLAYER_KILLS(sEndBoardData, bEndOfMode)
				POPULATE_HUD_PLAYER_ARRAY(sEndBoardData, bDoWaveCompleteHudPrints, bEndOfMode)
				sEndBoardData.bPopulatedHudPlayerArray = TRUE
			
			ELSE
				
				IF bEndOfMode
					
					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
					AND NOT  DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(specData.specCamData)
					
						REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS iCounter
							IF sEndBoardData.hudPlayers[iCounter] != INVALID_PLAYER_INDEX()
								IF IS_NET_PLAYER_OK(sEndBoardData.hudPlayers[iCounter], FALSE)
									
									iScore = 0
									iParticipant = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(sEndBoardData.hudPlayers[iCounter]))
									
									PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - checking participant ", iParticipant)
									
									IF IS_INT_WITHIN_PARTICIPANT_RANGE(iParticipant)
										iScore = playerBD[iParticipant].iSurvivalScore
										PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - survival score = ", iScore)
									ENDIF
									
									IF iScore > (-1)
										WRITE_MISSION_LB_DATA_TO_GLOBALS(	sEndBoardData.hudPlayers[iCounter],
																			sEndBoardData.iTeam[iCounter],
																			INCREMENT_POSITION_FOR_PLAYLIST_LEADERBOARD(iCounter),
																			-1,
																			-1,
																		    sEndBoardData.iScores[iCounter],
																			sEndBoardData.iDeaths[iCounter],
																			sEndBoardData.iHeadshots[iCounter], 
																			DUMMY_MODEL_FOR_SCRIPT, 
																			MCT_METALLIC, 
																			0, 
																			iScore	)
									ENDIF
									
								ENDIF
							ENDIF
						ENDREPEAT
						
					ENDIF
					
				ENDIF
				
				IF NOT bFinalCelebrationScreenHasDisplayed
					iEndOfWaveHudStage++
					PRINTLN("[Survival] - iEndOfWaveHudStage = ", iEndOfWaveHudStage)
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 2
			
			iXp = GET_END_OF_WAVE_XP_AMOUNT()
            iCash = MULTIPLY_CASH_BY_TUNABLE(GET_END_SURVIVAL_CASH_AWARD())
			
	        // Give xp for beating wave.
	        IF iXp > 0
	            HANDLE_END_OF_WAVE_XP(iXp)
	        ENDIF
	        
			IF iCash > 0
				
				//NETWORK_EARN_FROM_MISSION(iCash, g_FMMC_STRUCT.tl31LoadedContentID)
				IF USE_SERVER_TRANSACTIONS()
					INT iScriptTransactionIndex
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOBS, iCash, iScriptTransactionIndex, DEFAULT, DEFAULT)
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
				ELSE
					NETWORK_EARN_FROM_JOB(iCash, g_FMMC_STRUCT.tl31LoadedContentID)
				ENDIF
				PRINTLN("[Survival] - [Cash_Xp] - given player $", iCash)
				
				ADD_TO_JOB_CASH_EARNINGS(iCash)
				PRINTLN("[Survival] - [Cash_Xp] - called ADD_TO_JOB_CASH_EARNINGS with $", iCash)
				
			ENDIF
			
			PRINTLN("[Survival] - [Music] - called PLAY_SOUND_FRONTEND(MP_WAVE_COMPLETE, HUD_FRONTEND_DEFAULT_SOUNDSET).")
			
			g_b_OnHordeWaveBoard = TRUE
			iEndOfWaveHudStage++
			PRINTLN("[Survival] - iEndOfWaveHudStage = ", iEndOfWaveHudStage)
			
		BREAK
		
		CASE 3
			IF DID_I_JOIN_MISSION_AS_SPECTATOR()
				IF HAS_CELEBRATION_SUMMARY_FINISHED(sEndBoardData, bPassedFailed, bEndOfMode)
					//Cleanup audio scenes: see B*1642903 for implementation notes
					IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
						STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						PRINTLN("[Survival] - MP_CELEB_SCREEN_SCENE is active, calling STOP_AUDIO_SCENE(MP_CELEB_SCREEN_SCENE). Call C.")
					ENDIF
				
					RETURN TRUE
				ENDIF
			ENDIF
			IF g_b_OnHordeWaveBoard
			AND NOT bFinalCelebrationScreenHasDisplayed
				IF HAS_CELEBRATION_SUMMARY_FINISHED(sEndBoardData, bPassedFailed, bEndOfMode)
					//Cleanup audio scenes: see B*1642903 for implementation notes
					IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
						STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						PRINTLN("[Survival] - MP_CELEB_SCREEN_SCENE is active, calling STOP_AUDIO_SCENE(MP_CELEB_SCREEN_SCENE). Call D.")
					ENDIF
				
					g_b_OnHordeWaveBoard = FALSE
					TOGGLE_RENDERPHASES(TRUE)
					IF bEndOfMode
						IF NOT IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
							START_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
						ENDIF
					
						bFinalCelebrationScreenHasDisplayed = TRUE
					ENDIF
					
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		BREAK		
	ENDSWITCH
	
    RETURN FALSE
    
ENDFUNC

FUNC BOOL HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD()
    
	#IF IS_DEBUG_BUILD
	
		IF bDoAllCompletedWavePrints
		
			BOOL bPartActive
			BOOL bDidInitialFighterStatusSetup
			BOOL bWentStraightToSpecCam
			BOOL bWentSpecCamForOutOfBounds
			BOOL bInStageSCTV
			BOOL bCompletedEndOfWaveHud
			
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - ************** participant ", iStaggeredAlldidEndOfWaveHudCount)
			
			bPartActive = NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredAlldidEndOfWaveHudCount))
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bPartActive = ", bPartActive)
			
			bDidInitialFighterStatusSetup = IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bDidInitialFighterStatusSetup = ", bDidInitialFighterStatusSetup)
			
		    bWentStraightToSpecCam = IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bWentStraightToSpecCam = ", bWentStraightToSpecCam)
			
			bWentSpecCamForOutOfBounds = IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bWentSpecCamForOutOfBounds = ", bWentSpecCamForOutOfBounds)
			
	        bInStageSCTV = GET_CLIENT_HORDE_STAGE(INT_TO_PARTICIPANTINDEX(iStaggeredAlldidEndOfWaveHudCount)) != eHORDESTAGE_SCTV
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bInStageSCTV = ", bInStageSCTV)
			
	        bCompletedEndOfWaveHud = IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bCompletedEndOfWaveHud = ", bCompletedEndOfWaveHud)
			
		ENDIF
	
	#ENDIF
	
    IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredAlldidEndOfWaveHudCount))
		IF IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
	        IF NOT IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
				IF NOT IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
		            IF GET_CLIENT_HORDE_STAGE(INT_TO_PARTICIPANTINDEX(iStaggeredAlldidEndOfWaveHudCount)) != eHORDESTAGE_SCTV
		                IF NOT IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
		                    iStaggeredAlldidEndOfWaveHudCount = 0
		                    RETURN FALSE
		                ENDIF
		            ENDIF
				ENDIF
	        ENDIF
		ENDIF
    ENDIF
    
    iStaggeredAlldidEndOfWaveHudCount++
    
    IF iStaggeredAlldidEndOfWaveHudCount >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
        iStaggeredAlldidEndOfWaveHudCount = 0
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

PROC PLAY_COUNTDOWN_BEEP()
    
    IF HAS_SOUND_FINISHED(-1)
        PLAY_SOUND_FRONTEND(-1,"HORDE_COOL_DOWN_TIMER", "HUD_FRONTEND_DEFAULT_SOUNDSET")
    ENDIF
    
ENDPROC

PROC MAINTAIN_COUNTDOWN_AUDIO(SCRIPT_TIMER &sTimer)
    
    INT iTime = (DELAY_BETWEEN_WAVES - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTimer))
    
    SWITCH iCountDownAudioStage
            
            CASE 0
                            
                    IF iTime <= 10000
                            PLAY_COUNTDOWN_BEEP()
                            iCountDownAudioStage++
                    ENDIF
            
            BREAK
            
            CASE 1
                            
                    IF iTime <= 9000
                            PLAY_COUNTDOWN_BEEP()
                            iCountDownAudioStage++
                    ENDIF
            
            BREAK
            
            CASE 2
                            
                    IF iTime <= 8000
                            PLAY_COUNTDOWN_BEEP()
                            iCountDownAudioStage++
                    ENDIF
            
            BREAK
            
            CASE 3
                            
                    IF iTime <= 7000
                            PLAY_COUNTDOWN_BEEP()
                            iCountDownAudioStage++
                    ENDIF
            
            BREAK
            
            CASE 4
                            
                    IF iTime <= 6000
                            PLAY_COUNTDOWN_BEEP()
                            iCountDownAudioStage++
                    ENDIF
            
            BREAK
            
            CASE 5
                            
                    IF iTime <= 5000
                            PLAY_COUNTDOWN_BEEP()
                            iCountDownAudioStage++
                    ENDIF
            
            BREAK
            
            CASE 6
                            
                    IF iTime <= 4000
                            PLAY_COUNTDOWN_BEEP()
                            iCountDownAudioStage++
                    ENDIF
            
            BREAK
            
            CASE 7
                            
                    IF iTime <= 3000
                            PLAY_COUNTDOWN_BEEP()
                            iCountDownAudioStage++
                    ENDIF
            
            BREAK
            
            CASE 8
                            
                    IF iTime <= 2000
                            PLAY_COUNTDOWN_BEEP()
                            iCountDownAudioStage++
                    ENDIF
            
            BREAK
            
            CASE 9
                            
                    IF iTime <= 1000
                            PLAY_COUNTDOWN_BEEP()
                            iCountDownAudioStage++
                    ENDIF
            
            BREAK
            
    ENDSWITCH
    
ENDPROC

FUNC STRING GET_WAVE_PROGRESS_LABEL()
    
    SWITCH serverBD.sWaveData.iWaveCount
            CASE 1 RETURN "HRD_WAVE1PROG"
            CASE 2 RETURN "HRD_WAVE2PROG"
            CASE 3 RETURN "HRD_WAVE3PROG"
            CASE 4 RETURN "HRD_WAVE4PROG"
            CASE 5 RETURN "HRD_WAVE5PROG"
            CASE 6 RETURN "HRD_WAVE6PROG"
            CASE 7 RETURN "HRD_WAVE7PROG"
            CASE 8 RETURN "HRD_WAVE8PROG"
            CASE 9 RETURN "HRD_WAVE9PROG"
            CASE 10 RETURN "HRD_WAVE10PROG"
    ENDSWITCH
    
    RETURN "HRD_WAVE1PROG"
    
ENDFUNC

PROC MAINTAIN_WAVE_PROGRESS_BAR(HUDORDER eOrder)
    
    IF NOT bGotWaveProgressLabel
    
        #IF IS_DEBUG_BUILD
        IF bDoProgressBarPrints
        PRINTLN("[Survival] - bGotWaveProgressLabel = FALSE.")
        ENDIF
        #ENDIF
        
        tl15_waveProgressLabel = GET_WAVE_PROGRESS_LABEL()
        bGotWaveProgressLabel = TRUE
            
    ELSE
        
        #IF IS_DEBUG_BUILD
        IF bDoProgressBarPrints
        PRINTLN("[Survival] - bGotWaveProgressLabel = TRUE.")
        ENDIF
        #ENDIF
        
		IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)				//If spectator not on
		OR ((NOT IS_SPECTATOR_HUD_HIDDEN())//specData.specHUDData)				//or spectator is on but it's not hidden or on news
			AND (specData.specHUDData.eCurrentFilter <> SPEC_HUD_FILTER_NEWS)
			AND (specData.specHUDData.eCurrentFilter <> SPEC_HUD_FILTER_GTAOTV))
            
            #IF IS_DEBUG_BUILD
            IF bDoProgressBarPrints
            PRINTLN("[Survival] - IS_SPECTATOR_HUD_HIDDEN() bit is not set - calling DRAW_GENERIC_METER().")
            ENDIF
            #ENDIF
            
            DRAW_GENERIC_METER(serverBD.iKillsThisWave, serverBD.sWaveData.iNumRequiredKills, tl15_waveProgressLabel, HUD_COLOUR_WHITE, (-1), eOrder, -1, -1, FALSE, TRUE, HUDFLASHING_NONE, 0, TRUE)
            
        #IF IS_DEBUG_BUILD
        ELSE
    
            IF bDoProgressBarPrints
            PRINTLN("[Survival] - IS_SPECTATOR_HUD_HIDDEN() bit is set.")
            ENDIF
            #ENDIF
                
        ENDIF
    ENDIF
    
ENDPROC

PROC MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(BOOL bAtShootout)
	
	INT iTime
	HUD_COLOURS eHudColour
	
	IF bAtShootout
			
		// Remove get to shootout blip.
	    REMOVE_SHOOTOUT_BLIP()
		
	    // Reset leave horde timer.
	    IF HAS_NET_TIMER_STARTED(stLeaveHordeTimer)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVING_SURVIVAL)
	        RESET_NET_TIMER(stLeaveHordeTimer)
	    ENDIF
		
	ELSE
			
	    // Add get to shootout blip.
	    ADD_SHOOTOUT_BLIP()
	    
	    // Say get back to shootout.
	    Print_Objective_Text("HRD_GOSHT")
	   	
	    // Run away from shootout timer. If away for too long, set bit to kick off horde mode.
	    IF NOT HAS_NET_TIMER_STARTED(stLeaveHordeTimer)
	            
	        // Start the timer.
	        START_NET_TIMER(stLeaveHordeTimer)
	        
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_LEAVING_SURVIVAL)
			
	    ELSE
	        
	        iTime = GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stLeaveHordeTimer)
	        
			IF iTime <= 0
				iTime = 0
			ENDIF
			
			IF (GET_BACK_TO_HORDE_TIME_LIMIT-iTime) < 10000
				eHudColour = HUD_COLOUR_RED
			ELSE
				eHudColour = HUD_COLOUR_WHITE
			ENDIF
			
			STRING stTimeTitle
			IF NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
				stTimeTitle = "HRD_LVHRD"
			ELSE
				stTimeTitle = "HRD_LVWAVE"
			ENDIF
			
	    	DRAW_GENERIC_TIMER((GET_BACK_TO_HORDE_TIME_LIMIT-iTime),stTimeTitle,0,TIMER_STYLE_DONTUSEMILLISECONDS,-1,PODIUMPOS_NONE,HUDORDER_BOTTOM,FALSE,eHudColour)
			
	        // If stay away for long enough, I have abandoned horde mode.
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
					
		        IF HAS_NET_TIMER_EXPIRED(stLeaveHordeTimer, GET_BACK_TO_HORDE_TIME_LIMIT)
		            Clear_Any_Objective_Text_From_This_Script()
					IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
						iRejoinFromSpecCamStage = 0
						PRINTLN("[Survival] - iRejoinFromSpecCamStage = 0, call 20.")
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
			        	SET_CLIENT_HORDE_STAGE(eHORDESTAGE_SPECTATING)
					ELSE
			            SET_CLIENT_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
			            SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)       
					ENDIF
		        ENDIF
	        
			ENDIF
			
	    ENDIF
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_NEXT_WAVE_HUD(HUDORDER eOrder, BOOL bdrawObj = TRUE)
    
    TEXT_LABEL_15 tl15_waveText
    TEXT_LABEL_15 tl15_objective
	BOOL bPlayerAtShootout = IS_PLAYER_AT_HORDE_SHOOTOUT(PLAYER_ID())
	
	// Tell player to get back to the shoot out if not near it.
	MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(bPlayerAtShootout)
	
    IF HAS_NET_TIMER_STARTED(serverBD.stNextWaveDelay)
        MAINTAIN_COUNTDOWN_AUDIO(serverBD.stNextWaveDelay)
        IF NOT HAS_NET_TIMER_EXPIRED(serverBD.stNextWaveDelay, DELAY_BETWEEN_WAVES)
			IF bPlayerAtShootout
				IF (serverBD.sWaveData.iWaveCount <= 1)
	                tl15_waveText = "HRD_FIRST"
	                tl15_objective = "HRD_PREP0"
	            ELSE
	                tl15_waveText = "HRD_NEXT"
	                tl15_objective = "HRD_PREP1"
	            ENDIF
	            IF bdrawObj
	                Print_Objective_Text(tl15_objective)
	            ENDIF 
				DRAW_GENERIC_TIMER((DELAY_BETWEEN_WAVES-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stNextWaveDelay)), tl15_waveText, 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, eOrder)
			ENDIF
        ELSE
            Clear_Any_Objective_Text_From_This_Script()
        ENDIF
    ENDIF
    
ENDPROC

PROC MAINTAIN_SPECTATOR_HORDE_HUD()
    IF NOT g_bCelebrationScreenIsActive 
	
		INT iHordeSpecHudDisplayState // 0 = fightingstage, 1 = delay for enxt wave stage, 2 = get back to shoot out.
		PED_INDEX pedId = GET_SPECTATOR_CURRENT_FOCUS_PED()
		PLAYER_INDEX specCamFocusPlayer = INVALID_PLAYER_INDEX()
		
		IF DOES_ENTITY_EXIST(pedId)
			IF NOT IS_ENTITY_DEAD(pedId)
				IF IS_PED_A_PLAYER(pedId)
					specCamFocusPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_FIGHTING
			iHordeSpecHudDisplayState = 0
			IF specCamFocusPlayer !=  INVALID_PLAYER_INDEX()
				IF NOT IS_PLAYER_AT_HORDE_SHOOTOUT(specCamFocusPlayer)
					iHordeSpecHudDisplayState = 2
				ENDIF
			ENDIF
		ELIF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
			iHordeSpecHudDisplayState = 1
		ENDIF
		
		SWITCH iHordeSpecHudDisplayState
			
			CASE 0
				//SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
			    MAINTAIN_WAVE_PROGRESS_BAR(HUDORDER_BOTTOM) // Wave progress bar.
				MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(TRUE)
			BREAK
			
			CASE 1
				bGotWaveProgressLabel = FALSE
				//SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
			   	MAINTAIN_NEXT_WAVE_HUD(HUDORDER_BOTTOM, FALSE) // Next wave countdown.
				MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(TRUE)
			BREAK
			
			CASE 2
				MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(FALSE) // Tell player to get back to the shoot out if not near it.
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

// Horde widget variables.
TEXT_WIDGET_ID twID_HordeStageServer
TEXT_WIDGET_ID twID_HordeStageClient
TEXT_WIDGET_ID twID_endReason
//TEXT_WIDGET_ID twID_pedState[MAX_NUM_SQUAD_PEDS]
TEXT_WIDGET_ID twID_gameStateServer
TEXT_WIDGET_ID twID_gameStateClient
TEXT_WIDGET_ID twID_killStreakState
TEXT_WIDGET_ID twID_squadState[MAX_NUM_ENEMY_SQUADS]
//TEXT_WIDGET_ID twID_pedSquadState[MAX_NUM_SQUAD_PEDS]
TEXT_WIDGET_ID twID_WaveSubStage
TEXT_WIDGET_ID twID_NextWaveDelayStage
TEXT_WIDGET_ID twID_minBoardState
TEXT_WIDGET_ID twID_heliState[MAX_NUM_SURVIVAL_HELIS]
TEXT_WIDGET_ID twID_landVehicleState[MAX_NUM_LAND_VEHICLES]
BOOL bActivateOverlapPeds

BLIP_INDEX biEnemy[MAX_NUM_ENEMY_SQUADS][MAX_NUM_PEDS_IN_SQUAD]

//BOOL bUpdateEnemiesWidgets
BOOL bCurrentWaveDefeated
BOOL bDidFirstWaveSetup
BOOL bDealtWithFeedback
BOOL bReadyForstartFadeIn
BOOL bCompletedEndWaveHud
BOOL bForceBlipAllEnemies
BOOL bForceBlippedEnemies

INT iHudPlayers[4]
INT iWaveStageKillLimit

//STRING strRespawnTimer[MAX_NUM_SQUAD_PEDS]

PROC MAINTAIN_S_AND_F_SKIPS()

    IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredPartCountDebug))
        
        IF playerBD[iStaggeredPartCountDebug].bUsedSSkip
            
            PRINTLN("[Survival] - participant ", iStaggeredPartCountDebug, "used S skip." )
            
            SET_END_HORDE_REASON(eENDHORDEREASON_BEAT_ALL_WAVES)
			serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE
            SET_SERVER_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
            serverBD.iSfSkipParticipant = iStaggeredPartCountDebug
            
        ELIF playerBD[iStaggeredPartCountDebug].bUsedFSkip
            
            PRINTLN("[Survival] - participant ", iStaggeredPartCountDebug, "used F skip." )
            
            SET_END_HORDE_REASON(eENDHORDEREASON_ALL_PLAYERS_DEAD)
			serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE
            SET_SERVER_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
            serverBD.iSfSkipParticipant = iStaggeredPartCountDebug
            
        ELIF playerBD[iStaggeredPartCountDebug].bSkipWave
                
            IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_FIGHTING
                serverBD.bSkipWave = TRUE
            ENDIF
        
        ENDIF
        
    ENDIF
    
    iStaggeredPartCountDebug++
    
    IF iStaggeredPartCountDebug >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
        iStaggeredPartCountDebug = 0
    ENDIF
    
ENDPROC

PROC MAINTAIN_CLIENT_J_SKIPS()
    
    // S and F skipping for quick pass/fail, J skipping.
    #IF IS_DEBUG_BUILD
        IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
            IF NOT bJSkipped
                playerBD[PARTICIPANT_ID_TO_INT()].bUsedSSkip = TRUE
            ENDIF
        ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
            IF NOT bJSkipped
                playerBD[PARTICIPANT_ID_TO_INT()].bUsedFSkip = TRUE
            ENDIF
        ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
            bDoingWarpToHorde = TRUE
        ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_SHIFT, "Skip Wave")
            bJSkipped = TRUE        
        ENDIF
        IF NOT bJSkipped
            IF bDoingWarpToHorde
                IF NET_WARP_TO_COORD(g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos, 0.0)
                    bDoingWarpToHorde = FALSE
                ENDIF 
            ENDIF
        ENDIF
        IF NOT bDoingWarpToHorde
            IF bJSkipped
                playerBD[PARTICIPANT_ID_TO_INT()].bSkipWave = TRUE
                bJSkipped = FALSE
            ENDIF
        ENDIF
    #ENDIF
    
ENDPROC

PROC REMOVE_PREVIOUS_WAVE_ENTITIES_DEBUG()

	INT i, j
	BOOL bAllCleared = TRUE

	IF serverBD.bSkipWave
	    
		REPEAT MAX_NUM_ENEMY_SQUADS j
			REPEAT MAX_NUM_PEDS_IN_SQUAD i
		        IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.structAiSquad[j].sPed[i].netId)
		                bAllCleared = FALSE
		            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.structAiSquad[j].sPed[i].netId)
		                DELETE_NET_ID(serverBD.structAiSquad[j].sPed[i].netId)
		            ELSE
		                NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.structAiSquad[j].sPed[i].netId)
		            ENDIF
		        ENDIF
		    ENDREPEAT
	    ENDREPEAT
		
	    REPEAT MAX_NUM_SURVIVAL_HELIS i
	        IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalHeli[i].netId)
	            bAllCleared = FALSE
	            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sSurvivalHeli[i].netId)
	                DELETE_NET_ID(serverBD.sSurvivalHeli[i].netId)
	            ELSE
	                NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.sSurvivalHeli[i].netId)
	            ENDIF
	        ENDIF
			REPEAT NUM_HELI_PEDS j
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalHeli[i].sPed[j].netId)
		            bAllCleared = FALSE
		            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sSurvivalHeli[i].sPed[j].netId)
		                DELETE_NET_ID(serverBD.sSurvivalHeli[i].sPed[j].netId)
		            ELSE
		                NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.sSurvivalHeli[i].sPed[j].netId)
		            ENDIF
		        ENDIF
			ENDREPEAT
	    ENDREPEAT
	    
		REPEAT MAX_NUM_LAND_VEHICLES i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalLandVehicle[i].netId)
	            bAllCleared = FALSE
	            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sSurvivalLandVehicle[i].netId)
	                DELETE_NET_ID(serverBD.sSurvivalLandVehicle[i].netId)
	            ELSE
	                NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.sSurvivalLandVehicle[i].netId)
	            ENDIF
	        ENDIF
			REPEAT NUM_LAND_VEH_PEDS j
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalLandVehicle[i].sPed[j].netId)
		            bAllCleared = FALSE
		            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sSurvivalLandVehicle[i].sPed[j].netId)
		                DELETE_NET_ID(serverBD.sSurvivalLandVehicle[i].sPed[j].netId)
		            ELSE
		                NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.sSurvivalLandVehicle[i].sPed[j].netId)
		            ENDIF
		        ENDIF
			ENDREPEAT
		ENDREPEAT
		
	    IF bAllCleared
	        serverBD.bSkipWave = FALSE
	    ENDIF

	ENDIF
	    
ENDPROC

PROC DEBUG_FORCE_BLIP_ALL_ENEMIES(BOOL bOnOff)
    
    INT i, j
    
    IF bOnOff
		
		REPEAT MAX_NUM_ENEMY_SQUADS j
			REPEAT MAX_NUM_PEDS_IN_SQUAD i
	            IF NOT IS_NET_PED_INJURED(serverBD.structAiSquad[j].sPed[i].netId)
	                IF NOT DOES_BLIP_EXIST(biEnemy[j][i])
	                    biEnemy[j][i] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD.structAiSquad[j].sPed[i].netId))
	                ENDIF
	            ENDIF
			ENDREPEAT
        ENDREPEAT

    ELSE
    
       REPEAT MAX_NUM_ENEMY_SQUADS j
			REPEAT MAX_NUM_PEDS_IN_SQUAD i
	            IF DOES_BLIP_EXIST(biEnemy[j][i])
	                REMOVE_BLIP(biEnemy[j][i])
	            ENDIF
			ENDREPEAT
        ENDREPEAT
    
    ENDIF
    
ENDPROC

/// PURPOSE:
///    Updates Horde widgets.
PROC UPDATE_WIDGETS()
    
    INT i, j
//    INT iSquad
    
	iMyPartId = PARTICIPANT_ID_TO_INT()
	
    SET_CONTENTS_OF_TEXT_WIDGET(twID_HordeStageServer, GET_HORDE_STAGE_NAME(GET_SERVER_HORDE_STAGE()))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_HordeStageClient, GET_HORDE_STAGE_NAME(GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID())))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_endReason, GET_END_HORDE_REASON_NAME(GET_END_HORDE_REASON()))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_gameStateServer, GET_GAME_STATE_NAME(GET_SERVER_GAME_STATE()))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_gameStateClient, GET_GAME_STATE_NAME(GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT())))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_killStreakState, GET_KILL_STREAK_STATE_NAME(structKillStreakData.eState))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_WaveSubStage, GET_WAVE_SUB_STAGE_NAME(serverBD.sWaveData.eStage))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_NextWaveDelayStage, GET_DELAY_BEFORE_NEXT_WAVE_SUB_STAGE_NAME(serverBD.sDelayBeforeNextWaveStageData.eStage))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_minBoardState, GET_MINI_BOARD_STAGE_NAME(eMiniLeaderBoardData.eMiniLeaderBoardState))
    
	IF bUpdateOverheadOverride
		IF (INT_TO_ENUM(eOVERHEAD_OVERRIDE_STATE, iOverheadOverrideState) != MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.eOverheadOverrideState[0])
			MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.eOverheadOverrideState[0] = INT_TO_ENUM(eOVERHEAD_OVERRIDE_STATE, iOverheadOverrideState)
		ENDIF
		bUpdateOverheadOverride = FALSE
	ENDIF
	
	REPEAT MAX_NUM_SURVIVAL_HELIS i
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF bForceMakeSureBlownUpState[i]
				IF serverBD.sSurvivalHeli[i].eState < eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
					RESET_NET_TIMER(serverBD.sSurvivalHeli[i].stBlownUpTimer)
					START_NET_TIMER(serverBD.sSurvivalHeli[i].stBlownUpTimer)
		            serverBD.sSurvivalHeli[i].eState = eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
    REPEAT MAX_NUM_SURVIVAL_HELIS i
        SET_CONTENTS_OF_TEXT_WIDGET(twID_heliState[i], GET_SURVIVAL_HELI_STATE_NAME(serverBD.sSurvivalHeli[i].eState))
    ENDREPEAT
	
	REPEAT MAX_NUM_LAND_VEHICLES i
		SET_CONTENTS_OF_TEXT_WIDGET(twID_landVehicleState[i], GET_LAND_VEHICLE_STATE_NAME(serverBD.sSurvivalLandVehicle[i].eState))
	ENDREPEAT
	
    bCurrentWaveDefeated = IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
    bDidFirstWaveSetup = IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
	
	bForceEnemyBlips = FORCE_ENEMY_BLIPS_ON()
	
    bDealtWithFeedback = IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK)
    bReadyForstartFadeIn = IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_READY_FOR_START_FADE_IN)
    bCompletedEndWaveHud = IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
    
    iWaveStageKillLimit = GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(serverBD.sWaveData.eStage)
    
//    IF bUpdateEnemiesWidgets
//		
//        REPEAT MAX_NUM_SQUAD_PEDS i
//            SET_CONTENTS_OF_TEXT_WIDGET(twID_pedState[i], GET_ENEMY_STATE_NAME(serverBD.s))
//            iSquad = GET_SQUAD_ID(serverBD.sEnemyPedhordeData[i].sSquadIdData)
//            IF iSquad != (-1)
//                SET_CONTENTS_OF_TEXT_WIDGET(twID_pedSquadState[i], GET_SQUAD_STATE_NAME(serverBD.structAiSquad[iSquad].eState))
//            ENDIF
//        ENDREPEAT
//    ENDIF
    
    REPEAT MAX_NUM_ENEMY_SQUADS i
        UPDATE_SQUAD_AI_WIDGETS(serverBD.structAiSquad[i], twID_squadState[i])
    ENDREPEAT
    
    IF bForceBlipAllEnemies
        IF NOT bForceBlippedEnemies
            DEBUG_FORCE_BLIP_ALL_ENEMIES(TRUE)
            bForceBlippedEnemies = TRUE
        ENDIF
    ELSE
        IF bForceBlippedEnemies
            DEBUG_FORCE_BLIP_ALL_ENEMIES(FALSE)
            bForceBlippedEnemies = FALSE
        ENDIF
    ENDIF
	
    IF bPutEnemiesOutsideHordeArea
		REPEAT MAX_NUM_ENEMY_SQUADS j
			REPEAT MAX_NUM_PEDS_IN_SQUAD i
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.structAiSquad[j].sPed[i].netId)
					SET_ENTITY_COORDS(NET_TO_PED(serverBD.structAiSquad[j].sPed[i].netId), <<-85.4480, 2803.1404, 52.3173>>)
				ENDIF
			ENDREPEAT
		ENDREPEAT
		bPutEnemiesOutsideHordeArea = FALSE
	ENDIF
	
	IF bDebugBlipAllAi
		
		bDebugUnblipAllAi = TRUE
		
		REPEAT MAX_NUM_ENEMY_SQUADS i
			REPEAT MAX_NUM_PEDS_IN_SQUAD j
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.structAiSquad[i].sPed[j].netId)
					IF NOT IS_NET_PED_INJURED(serverBD.structAiSquad[i].sPed[j].netId)
						IF NOT DOES_BLIP_EXIST(biDebugFootEnemyBlip[i][j])
							biDebugFootEnemyBlip[i][j] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD.structAiSquad[i].sPed[j].netId))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(biDebugFootEnemyBlip[i][j],HUD_COLOUR_NET_PLAYER22)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(biDebugFootEnemyBlip[i][j])
							REMOVE_BLIP(biDebugFootEnemyBlip[i][j])
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(biDebugFootEnemyBlip[i][j])
						REMOVE_BLIP(biDebugFootEnemyBlip[i][j])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
		REPEAT MAX_NUM_SURVIVAL_HELIS i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalHeli[i].netId)
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sSurvivalHeli[i].netId)
					IF NOT DOES_BLIP_EXIST(biDebugHeliBlip[i])
						biDebugHeliBlip[i] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.sSurvivalHeli[i].netId))
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(biDebugHeliBlip[i],HUD_COLOUR_NET_PLAYER22)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(biDebugHeliBlip[i])
						REMOVE_BLIP(biDebugHeliBlip[i])
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(biDebugHeliBlip[i])
					REMOVE_BLIP(biDebugHeliBlip[i])
				ENDIF
			ENDIF
			REPEAT NUM_HELI_PEDS j
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalHeli[i].sPed[j].netId)
					IF NOT IS_NET_PED_INJURED(serverBD.sSurvivalHeli[j].sPed[i].netId)
						IF NOT DOES_BLIP_EXIST(biDebugHeliEnemyBlip[i][j])
							biDebugHeliEnemyBlip[i][j] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD.sSurvivalHeli[i].sPed[j].netId))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(biDebugHeliEnemyBlip[i][j],HUD_COLOUR_NET_PLAYER22)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(biDebugHeliEnemyBlip[i][j])
							REMOVE_BLIP(biDebugHeliEnemyBlip[i][j])
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(biDebugHeliEnemyBlip[i][j])
						REMOVE_BLIP(biDebugHeliEnemyBlip[i][j])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
				
		REPEAT MAX_NUM_LAND_VEHICLES i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalLandVehicle[i].netId)
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.sSurvivalLandVehicle[i].netId)
					IF NOT DOES_BLIP_EXIST(biDebugLandVehBlip[i])
						biDebugLandVehBlip[i] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD.sSurvivalLandVehicle[i].netId))
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(biDebugLandVehBlip[i],HUD_COLOUR_NET_PLAYER22)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(biDebugLandVehBlip[i])
						REMOVE_BLIP(biDebugLandVehBlip[i])
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(biDebugLandVehBlip[i])
					REMOVE_BLIP(biDebugLandVehBlip[i])
				ENDIF
			ENDIF
			REPEAT NUM_LAND_VEH_PEDS j
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sSurvivalLandVehicle[i].sPed[j].netId)
					IF NOT IS_NET_PED_INJURED(serverBD.sSurvivalLandVehicle[i].sPed[j].netId)
						IF NOT DOES_BLIP_EXIST(biDebugLandVehEnemyBlip[i][j])
							biDebugLandVehEnemyBlip[i][j] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD.sSurvivalLandVehicle[i].sPed[j].netId))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(biDebugLandVehEnemyBlip[i][j],HUD_COLOUR_NET_PLAYER22)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(biDebugLandVehEnemyBlip[i][j])
							REMOVE_BLIP(biDebugLandVehEnemyBlip[i][j])
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(biDebugLandVehEnemyBlip[i][j])
						REMOVE_BLIP(biDebugLandVehEnemyBlip[i][j])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
	ELSE
		
		IF bDebugUnblipAllAi
			
			REPEAT MAX_NUM_ENEMY_SQUADS j
				REPEAT MAX_NUM_PEDS_IN_SQUAD i
					IF DOES_BLIP_EXIST(biDebugFootEnemyBlip[j][i])
						REMOVE_BLIP(biDebugFootEnemyBlip[j][i])
					ENDIF
				ENDREPEAT
			ENDREPEAT
			
			REPEAT MAX_NUM_SURVIVAL_HELIS i
				IF DOES_BLIP_EXIST(biDebugHeliBlip[i])
					REMOVE_BLIP(biDebugHeliBlip[i])
				ENDIF
				REPEAT NUM_HELI_PEDS j
					IF DOES_BLIP_EXIST(biDebugHeliEnemyBlip[i][j])
						REMOVE_BLIP(biDebugHeliEnemyBlip[i][j])
					ENDIF
				ENDREPEAT
			ENDREPEAT
					
			REPEAT MAX_NUM_LAND_VEHICLES i
				IF DOES_BLIP_EXIST(biDebugLandVehBlip[i])
					REMOVE_BLIP(biDebugLandVehBlip[i])
				ENDIF
				REPEAT NUM_LAND_VEH_PEDS j
					IF DOES_BLIP_EXIST(biDebugLandVehEnemyBlip[i][j])
						REMOVE_BLIP(biDebugLandVehEnemyBlip[i][j])
					ENDIF
				ENDREPEAT
			ENDREPEAT
				
			bDebugUnblipAllAi = FALSE
			
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Creates Horde widgets.
PROC MAINTAIN_WIDGETS()
    
    INT i
    TEXT_LABEL_63 tl63
    
    SWITCH iCreateWidgetsStage
            
            // Create widgets widget.
            CASE 0
                    
                    survivalWidgetsId = START_WIDGET_GROUP("Survival")
                            ADD_WIDGET_BOOL("Create Survival Widgets", bCreateWidgets)
                    STOP_WIDGET_GROUP()
                    
                    iCreateWidgetsStage++
                    
            BREAK
            
            CASE 1
                    
                    IF bCreateWidgets
                            
                            DELETE_WIDGET_GROUP(survivalWidgetsId)
                            
                            survivalWidgetsId = START_WIDGET_GROUP("Survival")
                                    
									ADD_WIDGET_INT_SLIDER("Overhead Override", iOverheadOverrideState, 0, 2, 1)
									ADD_WIDGET_BOOL("Update Overhead override", bUpdateOverheadOverride)
									
									ADD_WIDGET_INT_READ_ONLY("My Participant ID", iMyPartId)
									
									START_WIDGET_GROUP("Blip All")
										ADD_WIDGET_BOOL("Blip All Enemies", bDebugBlipAllAi)
									STOP_WIDGET_GROUP()
									
									START_WIDGET_GROUP("XP Breakdown")
										ADD_XP_BREAKDOWN_WIDGETS()
									STOP_WIDGET_GROUP()
									
									ADD_WIDGET_BOOL("All Completed End of Wave Prints", bDoAllCompletedWavePrints)
									
									ADD_WIDGET_BOOL("bDoWaveCompleteHudPrints", bDoWaveCompleteHudPrints)
                                    ADD_WIDGET_BOOL("bDo893474Prints", bDo893474Prints)
                                    ADD_WIDGET_BOOL("bDoMiniBoardDebugPrints", bDoMiniBoardDebugPrints)
                                    ADD_WIDGET_BOOL("bDoRespawnMissionPedPrints", bDoRespawnMissionPedPrints)
                                    ADD_WIDGET_BOOL("bDoProgressBarPrints", bDoProgressBarPrints)
                                    ADD_WIDGET_BOOL("bWaveStageDataWidgets", bWaveStageDataWidgets)
									ADD_WIDGET_BOOL("bDoEndWaveWidgets", bDoEndWaveWidgets)
									ADD_WIDGET_BOOL("bPutEnemiesOutsideHordeArea", bPutEnemiesOutsideHordeArea)
									ADD_WIDGET_BOOL("bPedSpawningBug", bPedSpawningBug)
									ADD_WIDGET_BOOL("bForceEnemyBlips", bForceEnemyBlips)
									ADD_WIDGET_BOOL("bPrintPartListInfo", bPrintPartListInfo)
									
									ADD_WIDGET_BOOL("bPauseSceneOnShot3", bPauseSceneOnShot3)
									ADD_WIDGET_VECTOR_SLIDER("Cam Offset From Heli", vHeliOffset, -10000.0, 10000.0, 0.01)
									ADD_WIDGET_VECTOR_SLIDER("Cam Rotation", vCamRotation, -10000.0, 10000.0, 0.01)
									ADD_WIDGET_FLOAT_SLIDER("Cam FOV", fCamFov, -10000.0, 10000.0, 0.01)
									
									ADD_WIDGET_INT_READ_ONLY("My Deaths", playerBD[PARTICIPANT_ID_TO_INT()].iDeaths)
									START_WIDGET_GROUP("Distance Fail Checks")
										ADD_WIDGET_BOOL("Block check", bBlockDistanceCheck)
									STOP_WIDGET_GROUP()
									
                                    START_WIDGET_GROUP("Overlap Peds")
                                            ADD_WIDGET_BOOL("Activate Overlap Peds", bActivateOverlapPeds)
                                    STOP_WIDGET_GROUP()
                                    
                                    START_WIDGET_GROUP("AI Helis")
                                            REPEAT MAX_NUM_SURVIVAL_HELIS i
                                                    twID_heliState[i] = ADD_TEXT_WIDGET("State")
                                                    ADD_WIDGET_BOOL("Spawn Heli", serverBD.sSurvivalHeli[i].bActive)
													ADD_WIDGET_BOOL("bForceMakeSureBlownUpState", bForceMakeSureBlownUpState[i])
                                            ENDREPEAT
                                    STOP_WIDGET_GROUP()
                                    
									START_WIDGET_GROUP("AI Land Vehicles")
                                            REPEAT MAX_NUM_LAND_VEHICLES i
                                                twID_landVehicleState[i] = ADD_TEXT_WIDGET("State")
												ADD_WIDGET_INT_READ_ONLY("Num Peds", serverBD.sSurvivalLandVehicle[i].iNumPeds)
                                            ENDREPEAT
                                    STOP_WIDGET_GROUP()
									
                                    START_WIDGET_GROUP("Mini Board")
                                            twID_minBoardState = ADD_TEXT_WIDGET("State")
                                    STOP_WIDGET_GROUP()
                                    
                                    START_WIDGET_GROUP("HUD")
                                            ADD_WIDGET_BOOL("bGotWaveProgressLabel", bGotWaveProgressLabel)
                                            ADD_WIDGET_BOOL("Force End Wave Text Respect Offset", bForceWavePassedOffset)
                                    STOP_WIDGET_GROUP()
                                    
                                    START_WIDGET_GROUP("Kill Streaks")
                                            twID_killStreakState = ADD_TEXT_WIDGET("State")
                                    STOP_WIDGET_GROUP()
                                    
                                    START_WIDGET_GROUP("Fighting Participants Data")
                                            ADD_WIDGET_INT_READ_ONLY("Num Fighting Participants", serverBD.sFightingParticipantsList.iNumFightingParticipants)
											ADD_WIDGET_INT_READ_ONLY("Num Potential Fighting Participants", serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants)
											ADD_WIDGET_INT_READ_ONLY("Num Start Wave Fighting Participants", serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave)
                                            REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
                                                    tl63 = "fighting Participant Slot "
                                                    tl63 += i
                                                    ADD_WIDGET_INT_READ_ONLY(tl63, serverBD.sFightingParticipantsList.iParticipant[i])
                                            ENDREPEAT
                                    STOP_WIDGET_GROUP()
                                    
                                    START_WIDGET_GROUP("Board Data")
                                            ADD_WIDGET_INT_READ_ONLY("My Kills", playerBD[PARTICIPANT_ID_TO_INT()].iMyKills)
                                            ADD_WIDGET_BOOL("Populated Player Array", sEndBoardData.bPopulatedHudPlayerArray)
                                            ADD_WIDGET_INT_READ_ONLY("Player 0", iHudPlayers[0])
                                            ADD_WIDGET_INT_READ_ONLY("Score", sEndBoardData.iScores[0])
                                            ADD_WIDGET_INT_READ_ONLY("Player 1", iHudPlayers[1])
                                            ADD_WIDGET_INT_READ_ONLY("Score", sEndBoardData.iScores[1])
                                            ADD_WIDGET_INT_READ_ONLY("Player 2", iHudPlayers[2])
                                            ADD_WIDGET_INT_READ_ONLY("Score", sEndBoardData.iScores[2])
                                            ADD_WIDGET_INT_READ_ONLY("Player 3", iHudPlayers[3])
                                            ADD_WIDGET_INT_READ_ONLY("Score", sEndBoardData.iScores[3])
                                    STOP_WIDGET_GROUP()
                                    
                                    START_WIDGET_GROUP("Bitsets and Flags")
											START_WIDGET_GROUP("Non Broadcast")
												ADD_WIDGET_BOOL("bIWentFromSpectatingToPassedWaveStage", bIWentFromSpectatingToPassedWaveStage)
											STOP_WIDGET_GROUP()
                                            START_WIDGET_GROUP("Client")
                                                    ADD_WIDGET_BOOL("Dealt with EOM Feedback", bDealtWithFeedback)
                                                    ADD_WIDGET_BOOL("Ready For Start Fade In", bReadyForstartFadeIn)
                                                    ADD_WIDGET_BOOL("Did End Of Wave Hud", bCompletedEndWaveHud)
                                            STOP_WIDGET_GROUP()
                                            START_WIDGET_GROUP("Server")
                                                    ADD_WIDGET_BOOL("Current Wave Defeated", bCurrentWaveDefeated)
                                                    ADD_WIDGET_BOOL("First Wave Setup", bDidFirstWaveSetup)
                                            STOP_WIDGET_GROUP()
                                    STOP_WIDGET_GROUP()
                                    
                                    START_WIDGET_GROUP("Respawning")
                                            ADD_WIDGET_INT_READ_ONLY("Ped Respawn Check", serverBd.iSearchingForPed)
                                            ADD_WIDGET_INT_READ_ONLY("Respawn Stage", iRespawningStage)
                                    STOP_WIDGET_GROUP()
                                    
                                    START_WIDGET_GROUP("Wave Data")
                                            ADD_WIDGET_INT_SLIDER("Wave Count", serverBD.sWaveData.iWaveCount, 1, 20, 1)
											ADD_WIDGET_INT_READ_ONLY("Num Created Enemies", serverBD.iNumCreatedEnemies)
											ADD_WIDGET_INT_READ_ONLY("Alive Squad Enemies", serverBD.iNumAliveSquadEnemies)
                                            ADD_WIDGET_INT_READ_ONLY("Alive Helis", serverBD.iNumAliveHelis)
											ADD_WIDGET_INT_READ_ONLY("Alive Bike Peds", serverBD.iNumAliveLandVehiclePeds)
											ADD_WIDGET_INT_READ_ONLY("Wave Kills Required", serverBD.sWaveData.iNumRequiredKills)
                                            ADD_WIDGET_INT_READ_ONLY("Kills This Wave", serverBD.iKillsThisWave)
											ADD_WIDGET_INT_READ_ONLY("Kills This Wave Events", serverBd.iNumKillsThisWaveFromEvents)
                                            twID_WaveSubStage = ADD_TEXT_WIDGET("Sub Stage")
                                            ADD_WIDGET_INT_READ_ONLY("Sub Stage Kills Required", iWaveStageKillLimit)
                                            ADD_WIDGET_INT_READ_ONLY("Sub Stage Easy Kills", serverBD.iKillsThisWaveSubStage[eWAVESTAGE_EASY])
											ADD_WIDGET_INT_READ_ONLY("Sub Stage Easy Kills", serverBD.iKillsThisWaveSubStage[eWAVESTAGE_MEDIUM])
											ADD_WIDGET_INT_READ_ONLY("Sub Stage Easy Kills", serverBD.iKillsThisWaveSubStage[eWAVESTAGE_HARD])
											ADD_WIDGET_INT_READ_ONLY("Kills This Sub Stage Events", serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_EASY])
											ADD_WIDGET_INT_READ_ONLY("Kills This Sub Stage Events", serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_MEDIUM])
											ADD_WIDGET_INT_READ_ONLY("Kills This Sub Stage Events", serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_HARD])
                                            ADD_WIDGET_INT_READ_ONLY("My Kills This Wave", playerBD[PARTICIPANT_ID_TO_INT()].iMyKillsThisWave)
                                            ADD_WIDGET_INT_READ_ONLY("Total Mode Kills", serverBd.iKills)
                                    STOP_WIDGET_GROUP()
                                    
                                    START_WIDGET_GROUP("Stages")
                                            twID_HordeStageServer = ADD_TEXT_WIDGET("Server Main")
                                            twID_NextWaveDelayStage = ADD_TEXT_WIDGET("Server Delay Next Wave")
                                            twID_HordeStageClient = ADD_TEXT_WIDGET("Local")
                                    STOP_WIDGET_GROUP()
                                    
                                    START_WIDGET_GROUP("Game States")
                                            twID_gameStateServer = ADD_TEXT_WIDGET("Server")
                                            twID_gameStateClient = ADD_TEXT_WIDGET("Client")
                                    STOP_WIDGET_GROUP()
                                    
                                    START_WIDGET_GROUP("End Reason")
                                            twID_endReason = ADD_TEXT_WIDGET("Reason")
                                    STOP_WIDGET_GROUP()
                                    
                                    REPEAT MAX_NUM_ENEMY_SQUADS i
                                            ADD_SQUAD_AI_WIDGETS(serverBD.structAiSquad[i], twID_squadState[i])
                                    ENDREPEAT
                                    
									CREATE_CELEBRATION_SCREEN_WIDGETS()
									
//                                    START_WIDGET_GROUP("Enemies")
//                                            ADD_WIDGET_BOOL("Toggle Force Blip All Enemies", bForceBlipAllEnemies)
//                                            ADD_WIDGET_BOOL("No Blips Output", bNoBlipsOutput)
//                                            ADD_WIDGET_BOOL("Update Enemies Widgets", bUpdateEnemiesWidgets)
//                                            REPEAT MAX_NUM_SQUAD_PEDS i
//												ADD_WIDGET_BOOL("bDoDefSpherePrints", bDoDefSpherePrints[i])
//                                                twID_pedState[i] = ADD_TEXT_WIDGET("State")
//                                                twID_pedSquadState[i] = ADD_TEXT_WIDGET("Squad State")
//                                                ADD_WIDGET_INT_READ_ONLY("Accuracy", serverBD.sEnemyPedHordeData[i].iAccuracy)
//                                                ADD_WIDGET_VECTOR_SLIDER("Current Respawn Coords", serverBD.sEnemyPedhordeData[i].vCurrentSpawnCoords, -10000.0, 10000.0, 0.1)
//                                                ADD_WIDGET_STRING(strRespawnTimer[i])
//                                                ADD_WIDGET_INT_READ_ONLY("Squad", serverBD.sEnemyPedhordeData[i].sSquadIdData.iSquad)
//                                                ADD_WIDGET_INT_READ_ONLY("Squad Slot", serverBD.sEnemyPedhordeData[i].sSquadIdData.iSquadSlot)
//                                            ENDREPEAT
//                                    STOP_WIDGET_GROUP()

									
									
									
									
									
									
//									START_WIDGET_GROUP("       PLAYLIST COLUMNS")
//	
//								        START_WIDGET_GROUP(" THUMB PERCENTAGE")
//								              ADD_WIDGET_FLOAT_SLIDER("TVOTEP_H ", TVOTEP_H  , 0, 2.0, 0.001)
//								              ADD_WIDGET_FLOAT_SLIDER("TVOTEP_W ", TVOTEP_W  , 0, 2.0, 0.001)
//								              ADD_WIDGET_FLOAT_SLIDER("TVOTEP_Y ", TVOTEP_Y  , 0, 2.0, 0.001)
//								        STOP_WIDGET_GROUP()
//										
//										START_WIDGET_GROUP(" SCROLL")
//											ADD_WIDGET_FLOAT_SLIDER("SCROLL_X", SCROLL_X , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("SCROLL_Y", SCROLL_Y , 0, 2.0, 0.001)
//											
//											ADD_WIDGET_FLOAT_SLIDER("COUNT_LONG__RECT_X", COUNT_LONG__RECT_X, 0, 1.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("COUNT_RECT_Y", COUNT_RECT_Y, 0, 1.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("COUNT_LONG__RECT_W", COUNT_LONG__RECT_W, 0, 1.0, 0.001)
//											
//											ADD_WIDGET_FLOAT_SLIDER("SCROLLARR_X", SCROLLARR_X , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("SCROLLARR_Y", SCROLLARR_Y , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("SCROLLARR_W", SCROLLARR_W , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("SCROLLARR_H", SCROLLARR_H , 0, 2.0, 0.001)
//											
//											ADD_WIDGET_INT_SLIDER("g_i_ActualLeaderboardPlayerSelected", g_i_ActualLeaderboardPlayerSelected, -100, 9999, 1)
//											ADD_WIDGET_INT_SLIDER("g_i_SpecLeaderboardPlayerSelected", g_i_SpecLeaderboardPlayerSelected, -100, 9999, 1)
//											ADD_WIDGET_INT_SLIDER("g_i_LeaderboardRowCounter", g_i_LeaderboardRowCounter, -100, 9999, 1)
//											ADD_WIDGET_INT_SLIDER("g_i_LeaderboardShift", g_i_LeaderboardShift, -100, 9999, 1)
//										STOP_WIDGET_GROUP()
//
//										ADD_WIDGET_FLOAT_SLIDER("COLUM_TITLE_X", COLUM_TITLE_X , 0, 2.0, 0.001)
//										ADD_WIDGET_FLOAT_SLIDER("COLUM_TITLE_Y", COLUM_TITLE_Y , 0, 2.0, 0.001)
//										
//										START_WIDGET_GROUP(" ICONS")
//											ADD_WIDGET_FLOAT_SLIDER("VOICE_H ", VOICE_H , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("VOICE_W ", VOICE_W , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("VOICE_X ", VOICE_X , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("VOICE_Y ", VOICE_Y , 0, 2.0, 0.001)
//										STOP_WIDGET_GROUP()
//										
//										START_WIDGET_GROUP(" RECT")
//											ADD_WIDGET_FLOAT_SLIDER("GRAD_X", GRAD_X, 0, 1.0, 0.001)	
//											ADD_WIDGET_FLOAT_SLIDER("GRAD_Y", GRAD_Y, 0, 1.0, 0.001)	
//											ADD_WIDGET_FLOAT_SLIDER("GRAD_W", GRAD_W, 0, 1.0, 0.001)	
//											ADD_WIDGET_FLOAT_SLIDER("GRAD_H", GRAD_H, 0, 1.0, 0.001)	
//										
//											ADD_WIDGET_FLOAT_SLIDER("COLUMN_HEADING_RECT_Y", COLUMN_HEADING_RECT_Y, 0, 1.0, 0.001)	
//											ADD_WIDGET_FLOAT_SLIDER("LBD_TEXT_X", LBD_TEXT_X, 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("LBD_TEXT_Y", LBD_TEXT_Y, 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("RED_X", RED_X, 0, 1.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("RED_W", RED_W, 0, 1.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("PLAYER_NAME_RECT_X", PLAYER_NAME_RECT_X, 0, 1.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("PLAYER_NAME_RECT_W", PLAYER_NAME_RECT_W, 0, 1.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("VAR_RECT_X", VAR_RECT_X, 0, 1.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("VAR_RECT_W", VAR_RECT_W, 0, 1.0, 0.001)
//										STOP_WIDGET_GROUP()
//											
//										START_WIDGET_GROUP(" wraps")	
//											START_WIDGET_GROUP("6 col")
//												ADD_WIDGET_FLOAT_SLIDER("f6wrap1EndX", f6wrap1EndX , -1, 2.0, 0.001)
//												ADD_WIDGET_FLOAT_SLIDER("f6wrap2EndX", f6wrap2EndX , -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f6wrap3EndX", f6wrap3EndX , -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f6wrap4EndX", f6wrap4EndX , -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f6wrap5EndX", f6wrap5EndX , -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f6wrap6EndX", f6wrap6EndX , -1, 2.0, 0.001)  
//											STOP_WIDGET_GROUP()
//
//											START_WIDGET_GROUP("5 col")
//												ADD_WIDGET_FLOAT_SLIDER("f5wrap1EndX", f5wrap1EndX, -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f5wrap2EndX", f5wrap2EndX, -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f5wrap3EndX", f5wrap3EndX, -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f5wrap4EndX", f5wrap4EndX, -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f5wrap5EndX", f5wrap5EndX, -1, 2.0, 0.001)  
//											STOP_WIDGET_GROUP()
//
//											START_WIDGET_GROUP("4 col")
//												ADD_WIDGET_FLOAT_SLIDER("f4wrap1EndX", f4wrap1EndX , -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f4wrap2EndX", f4wrap2EndX , -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f4wrap3EndX", f4wrap3EndX , -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f4wrap4EndX", f4wrap4EndX , -1, 2.0, 0.001)   
//											STOP_WIDGET_GROUP()
//
//											START_WIDGET_GROUP("3 col")
//												ADD_WIDGET_FLOAT_SLIDER("f3wrap1EndX", f3wrap1EndX , -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f3wrap2EndX", f3wrap2EndX , -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f3wrap3EndX", f3wrap3EndX , -1, 2.0, 0.001)   
//											STOP_WIDGET_GROUP()
//
//											START_WIDGET_GROUP("2 col")
//												ADD_WIDGET_FLOAT_SLIDER("f2wrap1EndX", f2wrap1EndX , -1, 2.0, 0.001)     
//												ADD_WIDGET_FLOAT_SLIDER("f2wrap2EndX", f2wrap2EndX , -1, 2.0, 0.001)   
//											STOP_WIDGET_GROUP()
//
//											START_WIDGET_GROUP("1 col")
//												ADD_WIDGET_FLOAT_SLIDER("f1wrap1EndX", f1wrap1EndX , -1, 2.0, 0.001) 
//											STOP_WIDGET_GROUP()
//										STOP_WIDGET_GROUP()
//										
//										START_WIDGET_GROUP("6 COLUMNS")
//											ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_1 		",  	SIX_COLUMN_1 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_2 		",  	SIX_COLUMN_2 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_3 		",  	SIX_COLUMN_3 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_4 		",  	SIX_COLUMN_4 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_5 		",  	SIX_COLUMN_5 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_6 		",  	SIX_COLUMN_6 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_2_PLAYLIST",  	SIX_COLUMN_2_PLAYLIST 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_3_PLAYLIST",  	SIX_COLUMN_3_PLAYLIST 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_4_PLAYLIST",  	SIX_COLUMN_4_PLAYLIST 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_5_PLAYLIST",  	SIX_COLUMN_5_PLAYLIST 		 , 0, 2.0, 0.001)
//										STOP_WIDGET_GROUP()
//										
//										START_WIDGET_GROUP("5 COLUMNS")
//											ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_1  	",  	FIVE_COLUMN_1 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_2  	",  	FIVE_COLUMN_2 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_3  	",  	FIVE_COLUMN_3 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_4  	",  	FIVE_COLUMN_4 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_5  	",  	FIVE_COLUMN_5 		 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_2_PLAYLIST",  	FIVE_COLUMN_2_PLAYLIST, 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_3_PLAYLIST",  	FIVE_COLUMN_3_PLAYLIST, 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_4_PLAYLIST",  	FIVE_COLUMN_4_PLAYLIST, 0, 2.0, 0.001)
//										STOP_WIDGET_GROUP()	
//										
//										START_WIDGET_GROUP("4 COLUMNS")
//											ADD_WIDGET_FLOAT_SLIDER("  FOUR_COLUMN_1 	",   	FOUR_COLUMN_1 	 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("  FOUR_COLUMN_2 	",   	FOUR_COLUMN_2 	 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("  FOUR_COLUMN_3 	",   	FOUR_COLUMN_3 	 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("  FOUR_COLUMN_4 	",   	FOUR_COLUMN_4 	 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("  FOUR_COLUMN_2_PLAYLIST", FOUR_COLUMN_2_PLAYLIST, 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("  FOUR_COLUMN_3_PLAYLIST", FOUR_COLUMN_3_PLAYLIST, 0, 2.0, 0.001)
//										STOP_WIDGET_GROUP()	
//										
//										START_WIDGET_GROUP("3 COLUMNS")
//											ADD_WIDGET_FLOAT_SLIDER("   THREE_COLUMN_1 	",  	 	THREE_COLUMN_1 	 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("   THREE_COLUMN_2 	",  	 	THREE_COLUMN_2 	 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("   THREE_COLUMN_3 	",  	 	THREE_COLUMN_3 	 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("   THREE_COLUMN_2_PLAYLIST",	THREE_COLUMN_2_PLAYLIST, 0, 2.0, 0.001)
//										STOP_WIDGET_GROUP()	
//										
//										START_WIDGET_GROUP("2 COLUMNS")
//											ADD_WIDGET_FLOAT_SLIDER("   TWO_COLUMN_1 	",   TWO_COLUMN_1 	 , 0, 2.0, 0.001)
//											ADD_WIDGET_FLOAT_SLIDER("   TWO_COLUMN_2 	",   TWO_COLUMN_2 	 , 0, 2.0, 0.001)
//										STOP_WIDGET_GROUP()	
//										
//										START_WIDGET_GROUP("1 COLUMN")
//											ADD_WIDGET_FLOAT_SLIDER("   ONE_COLUMN_1 	",   ONE_COLUMN_1 	 , 0, 2.0, 0.001)
//										STOP_WIDGET_GROUP()
//										
//										
//									STOP_WIDGET_GROUP()
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
									
                                    
                            STOP_WIDGET_GROUP()
                            
                            iCreateWidgetsStage++
                            
                    ENDIF
                    
            BREAK
            
            CASE 2
                    
                    UPDATE_WIDGETS()
                    
            BREAK
            
    ENDSWITCH
    
    
    
ENDPROC

#ENDIF

PROC PROCESS_PARTICPANT_ABANDONDED_SURVIVAL_TICKER(INT iParticipant, PARTICIPANT_INDEX participantId, BOOL bPartActive)
	
	IF NOT IS_BIT_SET(iPlayerAbandondedSurvivalTickerBit, iParticipant)
		IF IS_BIT_SET(playerBD[iParticipant].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
			participantId = INT_TO_PARTICIPANTINDEX(iParticipant)
			IF bPartActive
				PRINT_TICKER_WITH_PLAYER_NAME("LFT_SRVL", NETWORK_GET_PLAYER_INDEX(participantId))
				SET_BIT(iPlayerAbandondedSurvivalTickerBit, iParticipant)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(playerBD[iParticipant].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
			CLEAR_BIT(iPlayerAbandondedSurvivalTickerBit, iParticipant)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_KICK_LEECHERS_REMINDER_TICKER()
	
	INT i, iFightingPart
	PARTICIPANT_INDEX partId
	
	// Loop through fighting participants and see if they have hit any of the potential leecher triggers.
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
		IF serverBD.sFightingParticipantsList.iParticipant[i] != (-1)
			
			iFightingPart = serverBD.sFightingParticipantsList.iParticipant[i]
			
			IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, iFightingPart)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iFightingPart))
					
					partId = INT_TO_NATIVE(PARTICIPANT_INDEX, iFightingPart)
					
					IF GET_PARTICIPANT_IS_ANY_MIGHT_NOT_BE_PLAYING_FLAG_SET(iFightingPart)
						
						IF NOT IS_BIT_SET(iPrintedLeecherRemindeTickerForfightingParticipant, iFightingPart)
							PRINT_TICKER_WITH_PLAYER_NAME("LEECHRMDR", NETWORK_GET_PLAYER_INDEX(partId))
							SET_BIT(iPrintedLeecherRemindeTickerForfightingParticipant, iFightingPart)
							PRINTLN("[Survival] - printing ticker LEECHRMDR for participant ", iFightingPart)
							PRINTLN("[Survival] - setting bit iPrintedLeecherRemindeTickerForfightingParticipant for participant ", iFightingPart)
						ENDIF
						
					ELSE
						
						IF IS_BIT_SET(iPrintedLeecherRemindeTickerForfightingParticipant, iFightingPart)
							PRINTLN("[Survival] - clearing bit iPrintedLeecherRemindeTickerForfightingParticipant for participant ", iFightingPart)
							CLEAR_BIT(iPrintedLeecherRemindeTickerForfightingParticipant, iFightingPart)
						ENDIF
						
					ENDIF
					
				ENDIF
			ENDIF
			
		ENDIF
	ENDREPEAT
	
ENDPROC

// *********************************** \\
// *********************************** \\
//                                                                         \\
//                      PARTICIPANT LOOP                   \\
//                                                                         \\
// *********************************** \\
// *********************************** \\

PROC PRE_PROCESS_PARTICIPANT_LOOP()
    fFurthestTargetDist = 50.0
	fPartDis = 0
	bAllPlayersLostAllLivesFlag = TRUE
	iPlayerIsNetOkBitset = 0
	iParticpantIsDeadBitset = 0
ENDPROC

TEXT_LABEL_23 tl23_Names[NUM_NETWORK_PLAYERS]

PROC PROCESS_PARTICPANT_LOOP()

    INT i
	PLAYER_INDEX playerId
	PARTICIPANT_INDEX partId
	PED_INDEX pedID
	INT iPlayerId
	BOOL bPartActive
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		
		
		// Sort IDs and net player ok checks.
		partId = INT_TO_NATIVE(PARTICIPANT_INDEX, i)
		bPartActive = NETWORK_IS_PARTICIPANT_ACTIVE(partId)
		IF bPartActive
			playerId = NETWORK_GET_PLAYER_INDEX(partId)
			iPlayerId = NATIVE_TO_INT(playerId)
			IF IS_NET_PLAYER_OK(playerId, FALSE)
				tl23_Names[i] = GET_PLAYER_NAME(playerId)
				pedID = GET_PLAYER_PED(playerId)
				SET_BIT(iPlayerIsNetOkBitset, iPlayerId)
				IF IS_ENTITY_DEAD(pedID)
					SET_BIT(iParticpantIsDeadBitset, i)
				ENDIF
			ENDIF
			MAINTAIN_HAVE_ALL_PARTICIPANTS_LOST_ALL_LIVES_FLAG(i, iPlayerId, partId)
		ENDIF
		
		// Radar zoom. 
    	CALCULATE_FURTHEST_PLAYER_DISTANCE(i)
		
		// Ticker.
		PROCESS_PARTICPANT_ABANDONDED_SURVIVAL_TICKER(i, partId, bPartActive)
		
	ENDREPEAT
	
ENDPROC

PROC POST_PROCESS_PARTICIPANT_LOOP()
    
	UPDATE_SCTV_TICKER_SCORE_BASED_ON_WHO_IM_SPECTATING_HORDE_LEADERBOARD_STRUCT(HordeLeaderboard, tl23_Names)
	
ENDPROC

PROC MAINTAIN_PARTICPANT_LOOP()
    
    PRE_PROCESS_PARTICIPANT_LOOP()
    PROCESS_PARTICPANT_LOOP()
    POST_PROCESS_PARTICIPANT_LOOP()
    
ENDPROC



// *********************************** \\
// *********************************** \\
//                                                                         \\
//                              EVENT QUEUE                       \\
//                                                                         \\
// *********************************** \\
// *********************************** \\

#IF IS_DEBUG_BUILD
PROC PRINT_DAMAGED_EVENT_DATA(STRUCT_ENTITY_DAMAGE_EVENT &sEntityID)

    PRINTLN("[Survival_Damage_Events] - sEntityID.VictimIndex = ", NATIVE_TO_INT(sEntityID.VictimIndex))
    PRINTLN("[Survival_Damage_Events] - sEntityID.DamagerIndex = ", NATIVE_TO_INT(sEntityID.DamagerIndex))
    PRINTLN("[Survival_Damage_Events] - sEntityID.Damage = ", sEntityID.Damage)
    
    IF sEntityID.VictimDestroyed
    	PRINTLN("[Survival_Damage_Events] - sEntityID.VictimDestroyed = TRUE")
    ELSE
    	PRINTLN("[Survival_Damage_Events] - sEntityID.VictimDestroyed = TRUE")
    ENDIF
	
    PRINTLN("[Survival_Damage_Events] - sEntityID.WeaponUsed = ", sEntityID.WeaponUsed)
    PRINTLN("[Survival_Damage_Events] - sEntityID.VictimSpeed = ", sEntityID.VictimSpeed)
    PRINTLN("[Survival_Damage_Events] - sEntityID.DamagerSpeed = ", sEntityID.DamagerSpeed)
    
ENDPROC
#ENDIF

FUNC BOOL IS_PED_A_HORDE_PED(PED_INDEX ped, BOOL bIShouldCleanup, BOOL &bHeliPed)
    
    INT i, j
    PED_INDEX hordePed
    
    REPEAT MAX_NUM_ENEMY_SQUADS j
		REPEAT MAX_NUM_PEDS_IN_SQUAD i
	        IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.structAiSquad[j].sPed[i].netId)
	            IF NET_TO_PED(serverBD.structAiSquad[j].sPed[i].netId) != NULL
	                hordePed = NET_TO_PED(serverBD.structAiSquad[j].sPed[i].netId)
	                IF ped = hordePed
						IF bIShouldCleanup
							CLEANUP_NET_ID(serverBD.structAiSquad[j].sPed[i].netId)
						ENDIF
	                    RETURN TRUE     
	                ENDIF
	            ENDIF
	        ENDIF
		ENDREPEAT
    ENDREPEAT
    
	REPEAT MAX_NUM_LAND_VEHICLES j
		REPEAT NUM_LAND_VEH_PEDS i
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sSurvivalLandVehicle[j].sPed[i].netId)
				IF NET_TO_PED(serverBD.sSurvivalLandVehicle[j].sPed[i].netId) != NULL
	                hordePed = NET_TO_PED(serverBD.sSurvivalLandVehicle[j].sPed[i].netId)
	                IF ped = hordePed
						IF bIShouldCleanup
							CLEANUP_NET_ID(serverBD.sSurvivalLandVehicle[j].sPed[i].netId)
						ENDIF
	                    RETURN TRUE     
	                ENDIF
	            ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	REPEAT MAX_NUM_SURVIVAL_HELIS j
		REPEAT NUM_HELI_PEDS i
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sSurvivalHeli[j].sPed[i].netId)
				IF NET_TO_PED(serverBD.sSurvivalHeli[j].sPed[i].netId) != NULL
	                hordePed = NET_TO_PED(serverBD.sSurvivalHeli[j].sPed[i].netId)
	                IF ped = hordePed
						bHeliPed = TRUE
						IF bIShouldCleanup
							CLEANUP_NET_ID(serverBD.sSurvivalHeli[j].sPed[i].netId)
						ENDIF
	                    RETURN TRUE     
	                ENDIF
	            ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
    RETURN FALSE
    
ENDFUNC

FUNC BOOL IS_VEHICLE_A_HORDE_VEHICLE(VEHICLE_INDEX vehicle, BOOL &IsHeli)
	
	INT j
	
	REPEAT MAX_NUM_LAND_VEHICLES j
		PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - checking land vehicle ", j)
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sSurvivalLandVehicle[j].netId)
			PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = TRUE.")
			IF NET_TO_VEH(serverBD.sSurvivalLandVehicle[j].netId) != NULL
				PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - NET_TO_VEH != NULL.")
				PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - vehicle id = ", NATIVE_TO_INT(vehicle), ", NET_TO_VEH(serverBD.sSurvivalLandVehicle[j].netId) id = ", NATIVE_TO_INT(NET_TO_VEH(serverBD.sSurvivalLandVehicle[j].netId)))
				IF vehicle = NET_TO_VEH(serverBD.sSurvivalLandVehicle[j].netId)
					PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - vehicle passed = NET_TO_VEH.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUM_SURVIVAL_HELIS j
		PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - checking heli ", j)
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sSurvivalHeli[j].netId)	
			PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = TRUE.")
			IF NET_TO_VEH(serverBD.sSurvivalHeli[j].netId) != NULL
				PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - NET_TO_VEH != NULL.")
				IF vehicle = NET_TO_VEH(serverBD.sSurvivalHeli[j].netId)
					PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - vehicle passed = NET_TO_VEH.")
					IsHeli = TRUE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - vehicle passed is not a horde vehicle.")
    RETURN FALSE
    
ENDFUNC

FUNC BOOL GET_IS_ENTITY_DAMAGER_LOCAL_PLAYER(ENTITY_INDEX &entity, BOOL bDamagerIsPed)
    
    VEHICLE_INDEX veh
    
    IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
            RETURN FALSE
    ENDIF
    
    IF IS_ENTITY_DEAD(PLAYER_PED_ID())
            RETURN FALSE
    ENDIF
    
    IF bDamagerIsPed
            
            IF GET_PED_INDEX_FROM_ENTITY_INDEX(entity) != PLAYER_PED_ID()
                    
                    RETURN FALSE
                    
            ENDIF
            
    ELSE
            
            IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
                    RETURN FALSE
            ENDIF
            
            IF NOT DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
                    RETURN FALSE
            ENDIF
            
            veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
            
            IF NOT IS_VEHICLE_DRIVEABLE(veh)
                    RETURN FALSE
            ENDIF
            
            IF GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity) != veh
                    RETURN FALSE
            ENDIF
            
    ENDIF
    
    RETURN TRUE
    
ENDFUNC

FUNC PLAYER_INDEX GET_ENTITY_DAMAGER_PLAYER_INDEX(ENTITY_INDEX &entity, BOOL bDamagerIsPed)
    
    VEHICLE_INDEX veh
    PED_INDEX pedId
    
    IF bDamagerIsPed
            
            pedId = GET_PED_INDEX_FROM_ENTITY_INDEX(entity)
            
            IF NOT IS_PED_A_PLAYER(pedId)
                    RETURN INVALID_PLAYER_INDEX()
            ENDIF
            
    ELSE
            
            IF NOT IS_ENTITY_A_VEHICLE(entity)
                    RETURN INVALID_PLAYER_INDEX()
            ENDIF
            
            veh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity)
            
            IF IS_VEHICLE_SEAT_FREE(veh)
                    RETURN INVALID_PLAYER_INDEX()
            ENDIF
            
            pedId = GET_PED_IN_VEHICLE_SEAT(veh)
            
            IF NOT IS_PED_A_PLAYER(pedId)
                    RETURN INVALID_PLAYER_INDEX()
            ENDIF
            
    ENDIF
    
    RETURN NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
    
ENDFUNC

PROC CAP_HORDE_KILL_XP(INT &iXp)
	
	IF iXp > 1000
		iXp = 1000
		PRINTLN("[Survival] - CAP_HORDE_KILL_XP - iXp is > 1000, capping. iXp now equals ", iXp)
	ENDIF
	
ENDPROC

PROC GIVE_HORDE_KILL_XP(BOOL bVehicleKilled, VEHICLE_INDEX victimVehicle = NULL, PED_INDEX victimPed = NULL)
	   
//	INT iXpReward = GET_FIGHTING_PARTICIPANTS_AVERAGE_RANK()
	INT iFinalXpReward
	INT iXpReward = 10
	FLOAT fXp
	
	IF bVehicleKilled
		iXpReward = 25
	ENDIF
	
    // Increment kill stats.
	IF NOT bVehicleKilled
		
   		playerBD[PARTICIPANT_ID_TO_INT()].iMyKills++
    	playerBD[PARTICIPANT_ID_TO_INT()].iMyKillsThisWave++
    
		// Headshots.
		IF CHECK_HEADSHOT(victimPed)
			playerBD[PARTICIPANT_ID_TO_INT()].iMyHeadshots++
		ENDIF
	
	    // For kill streak.
	    structKillStreakData.iNumKillsInARow++
	
	ELSE
	
		playerBD[PARTICIPANT_ID_TO_INT()].iMyVehicleKills++
	
	ENDIF
	
    // Get xp to give.
    iXpReward = (iXpReward * GET_KILL_STREAK_XP_MULTIPLIER())
//        iXpReward = (iXpReward/2)
	
	IF iXpReward < 1
		iXpReward = 1
	ENDIF
	
    // Give XP for my kill.
	IF bVehicleKilled
	
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP before conversion to float = ", iXpReward)
		
		fXp = TO_FLOAT(iXpReward)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP after conversion to float = ", fXp)
		
		fXp = (fXp*g_sMPTunables.fxp_tunable_Survival_Vehicle_destroyed)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - fxp_tunable_Survival_Vehicle_destroyed = ", g_sMPTunables.fxp_tunable_Survival_Vehicle_destroyed, ", XP after tuneable adjustment = ", fXp)
		
		iFinalXpReward = ROUND(fXp)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP after conversion back to int = ", iFinalXpReward)
		
		CAP_HORDE_KILL_XP(iFinalXpReward)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP after cap = ", iFinalXpReward)
		
		iFinalXpReward = GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIMATION(eXPTYPE_STANDARD, victimVehicle, "XPT_KAIE",XPTYPE_ACTION, XPCATEGORY_ACTION_SURVIVAL_KILLS, iFinalXpReward, 1, -1, "", TRUE)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP final value = ", iFinalXpReward)
		
		#IF IS_DEBUG_BUILD
		IF (serverBD.sWaveData.iWaveCount-1) >= 0
		AND (serverBD.sWaveData.iWaveCount-1) < NUMBER_HORDE_WAVES
			iVehsDestroyedXp[(serverBD.sWaveData.iWaveCount-1)] += iFinalXpReward
		ENDIF
		#ENDIF
		
	ELSE
		
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP before conversion to float = ", iXpReward)
		
		fXp = TO_FLOAT(iXpReward)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP after conversion to float = ", fXp)
		
		fXp = (fXp*g_sMPTunables.fxp_tunable_Survival_enemy_Kill)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - fxp_tunable_Survival_enemy_Kill = ", g_sMPTunables.fxp_tunable_Survival_enemy_Kill, ", XP after tuneable adjustment = ", fXp)
		
		iFinalXpReward = ROUND(fXp)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP after conversion back to int = ", iFinalXpReward)
		
		CAP_HORDE_KILL_XP(iFinalXpReward)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP final value = ", iFinalXpReward)
		
    	iFinalXpReward = GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIMATION(eXPTYPE_STANDARD, victimPed, "XPT_KAIE",XPTYPE_ACTION, XPCATEGORY_ACTION_SURVIVAL_KILLS, iFinalXpReward, 1, -1, "", TRUE)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP final value = ", iFinalXpReward)
		
		#IF IS_DEBUG_BUILD
		IF (serverBD.sWaveData.iWaveCount-1) >= 0
		AND (serverBD.sWaveData.iWaveCount-1) < NUMBER_HORDE_WAVES
			iKillsXp[(serverBD.sWaveData.iWaveCount-1)] += iFinalXpReward
		ENDIF
		#ENDIF
		
	ENDIF
	
    playerBD[PARTICIPANT_ID_TO_INT()].iXpGained += iFinalXpReward
	
ENDPROC

PROC PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT(INT iCount)
    
	BOOL bHeliPed, bIsHeli
	
    PLAYER_INDEX playerId
    PED_INDEX victimPed, damagerPed
	VEHICLE_INDEX victimVeh
    BOOL bDamagerIsPed
    STRUCT_ENTITY_DAMAGE_EVENT sEntityID
   	
	bIHaveDamagedAnEnemy = FALSE
	
    // Grab the event data.
    GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
    
    // Print event data.
    #IF IS_DEBUG_BUILD
    PRINTLN("[Survival_Damage_Events] - got event data.")
    PRINT_DAMAGED_EVENT_DATA(sEntityID)
    #ENDIF
    
    // Process event.
    IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)

        PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity exists.")
        
        IF sEntityID.VictimDestroyed
    
            PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was killed.")
            
            IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
        
                PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was a ped.")
                
                victimPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
                
                IF NOT IS_PED_A_PLAYER(victimPed)
            
                    PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was an ai ped.")
                    
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						
						bDamagerIsPed = IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						
                        IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
                    
                            PRINTLN("[Survival_Damage_Events] - event ", iCount, " - damager was a ped.")
                            
                            damagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
                            
							 IF IS_PED_A_HORDE_PED(victimPed, (playerId = PLAYER_ID()), bHeliPed)
                                  
								PRINTLN("[Survival_Damage_Events] - event ", iCount, " - ped was a horde ped.")
								  
								IF NOT bHeliPed
									PRINTLN("[Survival_Damage_Events] - event ", iCount, " - ped was not a heli horde ped.")
									IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
										serverBD.iNumKillsThisWaveFromEvents++
										serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage]++
										PRINTLN("[Survival_Damage_Events] - killed a horde enemy, iNumKillsThisWaveFromEvents now equals ", serverBD.iNumKillsThisWaveFromEvents)
										PRINTLN("[Survival_Damage_Events] - killed a horde enemy, iNumKillsThisSubStageFromEvents now equals ", serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage])
									ENDIF
								ENDIF
								
	                            IF IS_PED_A_PLAYER(damagerPed)
									
									PRINTLN("[Survival_Damage_Events] - event ", iCount, " - damager was a player.")
									  	
	                                playerId = GET_ENTITY_DAMAGER_PLAYER_INDEX(sEntityID.DamagerIndex, bDamagerIsPed)
	                                
									IF (playerId = PLAYER_ID())
										
										PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was a horde ped.")
										
								        PRINTLN("[Survival_Damage_Events] - I killed a horde ped! Give my rewards!")
								       	
										bIHaveDamagedAnEnemy = TRUE
										
										GIVE_HORDE_KILL_XP(FALSE, NULL, victimPed)
										
								        // Increment horde kills stat.
								        INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_HORDKILLS, 1)
								        
										iRefreshDpad++ // refresh dpad
										
								        // Send a ticker for my kill.
//								        BROADCAST_FMMC_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_PED)
        								
                                  	ENDIF
                                
								ENDIF
								
                            ENDIF
                        
						ENDIF
					
                    ENDIF
                		
                ELSE
                        
                    playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(victimPed)
                    
                    PRINTLN("[Survival_Damage_Events] - event ", iCount, " - victim was a player. Id = ", NATIVE_TO_INT(playerId))
                    
                    IF playerId != PLAYER_ID()
                        PRINTLN("[Survival_Damage_Events] - event ", iCount, " - victim was a player who is not me.")
                        IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerId)
                            PRINTLN("[Survival_Damage_Events] - event ", iCount, " - victim was a participant. Id = ", NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerId)))
                            PRINTLN("[Survival_Damage_Events] - event ", iCount, " - displaying big message")
                            SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_SURVIVAL_TM_DIED, playerId)
                        ENDIF
                    ELSE
                        PRINTLN("[Survival_Damage_Events] - event ", iCount, " - victim was a player who is me, no big message because I know I'm dead.")
                    ENDIF
                    
                ENDIF
                
			ELSE
			
				IF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
					
					PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was a vehicle.")
					
					victimVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
					
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						
						bDamagerIsPed = IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						
                        IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
                    
                            PRINTLN("[Survival_Damage_Events] - event ", iCount, " - damager was a ped.")
                            
                            damagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
                            
							IF IS_VEHICLE_A_HORDE_VEHICLE(victimVeh, bIsHeli)
								
								IF bIsHeli
									PRINTLN("[Survival_Damage_Events] - event ", iCount, " - vehicle was a heli.")
									IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
										serverBD.iNumKillsThisWaveFromEvents++
										serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage]++
										PRINTLN("[Survival_Damage_Events] - killed a horde enemy, iNumKillsThisWaveFromEvents now equals ", serverBD.iNumKillsThisWaveFromEvents)
										PRINTLN("[Survival_Damage_Events] - killed a horde enemy, iNumKillsThisSubStageFromEvents now equals ", serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage])
									ENDIF
								ENDIF
								
	                            IF IS_PED_A_PLAYER(damagerPed)
	                                    
	                                PRINTLN("[Survival_Damage_Events] - event ", iCount, " - damager was a player.")
	                                	
		                           	playerId = GET_ENTITY_DAMAGER_PLAYER_INDEX(sEntityID.DamagerIndex, bDamagerIsPed)
	                            	
									IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(victimVeh))
										PRINT_TICKER_WITH_PLAYER_NAME("HRD_KILLDCHPPR", playerId)
										PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was a horde chopper, printing ticker for who killed it.")
									ENDIF
									
									IF playerId = PLAYER_ID()
										
										PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was a horde vehicle.")
									
										PRINTLN("[Survival_Damage_Events] - I killed a horde vehicle! Give my rewards!")
										
										bIHaveDamagedAnEnemy = TRUE
										
										GIVE_HORDE_KILL_XP(TRUE, victimVeh)
									
									ENDIF
									
								ENDIF
								
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
            ENDIF
		
		ELSE
			
//			PRINTLN("[Survival_Damage_Events] - event ", iCount, " - victim was not destoryed.")
			
			IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
	        	
				victimPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				
				IF NOT IS_PED_A_PLAYER(victimPed)
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							
						bDamagerIsPed = IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						
	                    IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
	                
	//                      PRINTLN("[Survival_Damage_Events] - event ", iCount, " - damager was a ped.")
	                        
	                        damagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
	                        
	                        IF IS_PED_A_PLAYER(damagerPed)
								
	//							PRINTLN("[Survival_Damage_Events] - event ", iCount, " - damager was a player.")
								  	
	                            playerId = GET_ENTITY_DAMAGER_PLAYER_INDEX(sEntityID.DamagerIndex, bDamagerIsPed)
	                            
								IF (playerId = PLAYER_ID())
								
									bIHaveDamagedAnEnemy = TRUE
									
								ENDIF
								
							ENDIF
							
						ENDIF
							
	            	ENDIF
	        	ENDIF
        	
			ENDIF
			
		ENDIF
		
    ENDIF
                                            
ENDPROC

PROC PROCESS_COLLECTED_PICKUP_EVENT(INT iCount)
    
    STRUCT_PICKUP_EVENT PickupData
    TEXT_LABEL_15 labelTemp
    
    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, PickupData, SIZE_OF(PickupData))
        IF IS_NET_PLAYER_OK(PickupData.PlayerIndex)
            IF PickupData.PlayerIndex = PLAYER_ID()
                IF NETWORK_IS_PLAYER_A_PARTICIPANT(PickupData.PlayerIndex)
                labelTemp = GET_CORRECT_TICKER_STRING(PickupData.PickupType)
                PRINT_WEAPON_TICKER(labelTemp) 
                ENDIF
            ENDIF
        ENDIF
    ENDIF
                            
ENDPROC

// PURPOSE : Process all the events received in the events queue
PROC PROCESS_EVENTS()

    INT iCount
    EVENT_NAMES ThisScriptEvent
	
    #IF IS_DEBUG_BUILD
        STRUCT_EVENT_COMMON_DETAILS Details
    #ENDIF
    
    //process the events
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

        ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
        
        #IF IS_DEBUG_BUILD
            IF ThisScriptEvent = EVENT_NETWORK_SCRIPT_EVENT
                GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
                IF Details.Type != SCRIPT_EVENT_OHD_TALKING
                AND Details.Type != SCRIPT_EVENT_OHD_TALKING_RESET
                    PRINTLN("[Survival] - [Events] - ThisScriptEvent = ", ENUM_TO_INT(ThisScriptEvent))
                ENDIF
            ENDIF
        #ENDIF
        
        SWITCH ThisScriptEvent
            
            CASE EVENT_NETWORK_PLAYER_COLLECTED_PICKUP
                PRINTLN("[Survival] - [Events] - recieved event EVENT_NETWORK_PLAYER_COLLECTED_PICKUP")
                PROCESS_COLLECTED_PICKUP_EVENT(iCount)
            BREAK
            
            CASE EVENT_NETWORK_DAMAGE_ENTITY
            	PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT(iCount)
            BREAK
           	
        ENDSWITCH
            
    ENDREPEAT
    
ENDPROC

//===Start Edit David G===
/// PURPOSE:
///   (SERVER ONLY: Maintains the leaderboard data)
PROC SERVER_MAINTAIN_HORDE_LEADERBOARD_FOR_PARTICIPANT(THE_LEADERBOARD_STRUCT& leaderBoard[], INT iParticipant)
                                                                                                            
    //#IF IS_DEBUG_BUILD    NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_MAINTAIN_FM_LEADERBOARD_FOR_PARTICIPANT called ") #ENDIF
    
    INT i
    THE_LEADERBOARD_STRUCT EmptyLeaderBoardStruct
    THE_LEADERBOARD_STRUCT StoredLeaderBoardStruct
    THE_LEADERBOARD_STRUCT TempLeaderBoardStruct

    // remove this participant from leader board before we re-add him.
    BOOL bParticipantRemoved = FALSE
    REPEAT NUM_NETWORK_PLAYERS i 
//        IF leaderBoard[i].bInitialise = FALSE
		IF NOT IS_BIT_SET(leaderBoard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
            leaderBoard[i].playerID                 = INVALID_PLAYER_INDEX()
            leaderBoard[i].iParticipant     = -1
//            leaderBoard[i].bInitialise = TRUE
			SET_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
        ENDIF

        IF NOT (bParticipantRemoved)
            IF (leaderBoard[i].iParticipant = iParticipant)
                leaderBoard[i] = EmptyLeaderBoardStruct
                bParticipantRemoved = TRUE
            ENDIF
        ELSE
            TempLeaderBoardStruct = leaderBoard[i]
            leaderBoard[i-1] =      TempLeaderBoardStruct
            leaderBoard[i] = EmptyLeaderBoardStruct
        ENDIF
    ENDREPEAT
	
    INT iCurrentRank = 1
    // add this participant to the leader board
    BOOL bParticipantInserted = FALSE
    REPEAT NUM_NETWORK_PLAYERS i 
        IF NOT (bParticipantInserted)
            IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
                PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
                IF IS_NET_PLAYER_OK(PlayerId, FALSE, FALSE)
                    IF NOT IS_PLAYER_SCTV(PlayerId)
						IF IS_BIT_SET(playerBD[iParticipant].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
	                        IF NOT IS_BIT_SET(playerBD[iParticipant].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
	                            BOOL bGoodToInsert = TRUE
	                            // if there is already a someone there?
	                            IF (leaderBoard[i].playerID <> playerID)
	                                                                                                
	                                // check that person is still active and compare their position
	                                IF NATIVE_TO_INT(leaderBoard[i].playerID) <> -1
	                                    IF IS_NET_PLAYER_OK(leaderBoard[i].playerID, FALSE, FALSE)
	                                    AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX,leaderBoard[i].iParticipant))                                                                     
	                                        
	                                        // Who has most cash?
	                                        IF (leaderBoard[i].iKills > playerBD[iParticipant].iMyKills)
												iCurrentRank++
	                                            bGoodToInsert = FALSE
	                                        ENDIF
	                                    ENDIF
	                                ENDIF
	                            ENDIF
	                    
	                            // are we still good to insert this person into the leader board?
	                            IF (bGoodToInsert)
	                                StoredLeaderBoardStruct = leaderBoard[i]

	                                leaderBoard[i].playerID = PlayerId
	                                leaderBoard[i].iParticipant = iParticipant                                                      
	                                
	                                leaderBoard[i].iKills = playerBD[iParticipant].iMyKillsThisWave
	                                leaderBoard[i].iDeaths = playerBD[iParticipant].iDeaths
//	                                leaderBoard[i].iAssists = 0
	                                leaderBoard[i].iHeadshots = 0
//	                                leaderBoard[i].iHighestStreak = 0
	                                leaderBoard[i].iJobCash = 0
	                                leaderBoard[i].iJobXP = playerBD[iParticipant].iXpGained
									
	                                leaderBoard[i].iBadgeRank = GlobalplayerBD_FM[NATIVE_TO_INT(PlayerId)].scoreData.iRank
	//                                                            leaderBoard[i].iBadgeTeam = StatDataPlayerBD[NATIVE_TO_INT(PlayerId)].WhatTeamAmI

	                                // NOT CONVINCED THIS RANK BIT IS WORKING CORRECTLY ***********************
	                                leaderBoard[i].iRank = iCurrentRank
	                        
	                                bParticipantInserted = TRUE
	                            ENDIF
	                        ENDIF
						ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ELSE
            // shift everyone else in the leaderboard down
            TempLeaderBoardStruct = leaderBoard[i]
            leaderBoard[i] = StoredLeaderBoardStruct
            StoredLeaderBoardStruct = TempLeaderBoardStruct
        ENDIF
    ENDREPEAT
ENDPROC

PROC PROCESS_LEADERBOARD()
	
    INT iSlotCounter
    INT i, iSpectatorNumber
    PLAYER_INDEX PlayerId
	STRING sSpectatorName
	INT iNumDeaths
	
    //g_i_ActualLeaderboardPlayerSelected = -1                        //Used to block selection on horde leaderboard
	BOOL bDraw[FMMC_MAX_TEAMS]
    
	//check if correct, add spectators
	INT iNumberOfLeaderboardPlayers = serverBD.sFightingParticipantsList.iNumFightingParticipants + serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants
	
    REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		DEFINE_FREEMODE_NAMES_ARRAY(tl63LeaderboardPlayerNames, i)
		SERVER_MAINTAIN_HORDE_LEADERBOARD_FOR_PARTICIPANT(HordeLeaderboard, i)
    ENDREPEAT
	DEFINE_LBD_NAMES_ARRAY(lbdVars, tl63LeaderboardPlayerNames, TRUE)
	
	TRACK_LBD_ROW_FOR_SCROLL(lbdVars, 0, iNumberOfLeaderboardPlayers, FALSE)
	
	PROCESS_LBD_SCROLL_LOGIC(lbdVars, timeScrollDelay, iLBPlayerSelection, 0, 0, iNumberOfLeaderboardPlayers, 0, FALSE)
	
	
	IF g_i_ActualLeaderboardPlayerSelected = -1
		g_i_ActualLeaderboardPlayerSelected = 0
	ENDIF
	
	IF HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED(LeaderboardPlacement)
    
	    // Draw the leaderboard
		RENDER_LEADERBOARD(lbdVars, LeaderboardPlacement, SUB_HORDE, FALSE, GET_LEADERBOARD_TITLE(SUB_HORDE, g_FMMC_STRUCT.tl63MissionName), GET_NUM_LBD_COLUMNS(SUB_HORDE), DEFAULT, DEFAULT, DEFAULT, FALSE)
		
		// Check for player card press
		
		IF g_i_ActualLeaderboardPlayerSelected > -1
		AND g_i_ActualLeaderboardPlayerSelected < 32
		AND IS_BIT_SET(iSpectatorBit, g_i_ActualLeaderboardPlayerSelected)
			HANDLE_SPECTATOR_PLAYER_CARDS(lbdVars, iSavedSpectatorRow)
		ELSE
			BOOL bJobSpectating = (GET_SPECTATOR_HUD_STAGE(specData.specHUDData) = SPEC_HUD_STAGE_LEADERBOARD)
			HANDLE_THE_PLAYER_CARDS(HordeLeaderboard, lbdVars, iSavedRow, bJobSpectating)
		ENDIF
		
		GRAB_SPECTATOR_PLAYER_SELECTED(HordeLeaderboard, iSavedRow)
	    
	    REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		
			// Draw spectators
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))				
				PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				
				/*// DEFINE_NAMES_ARRAY, grab names for lbd and gamer handles
				IF IS_STRING_NULL_OR_EMPTY(tl63LeaderboardPlayerNames[i])	
					tl63LeaderboardPlayerNames[i] = GET_PLAYER_NAME(PlayerId)
					STORE_GAMER_HANDLES(lbdVars, PlayerId, i)
                ENDIF*/
				
				POPULATE_HEADSHOTS_LBD(PlayerId)
				
				IF IS_NET_PLAYER_OK(PlayerId,FALSE)
					INT iSpectatorRow = GET_SPECTATOR_ROW(FALSE, serverBD.sFightingParticipantsList.iNumFightingParticipants, 0, iSpectatorNumber)
					IF IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
						sSpectatorName = GET_PLAYER_NAME(playerId)
						DRAW_SPECTATOR_ROW(lbdVars, LeaderboardPlacement, SUB_HORDE, playerId, sSpectatorName, GlobalplayerBD_FM[NATIVE_TO_INT(PlayerId)].scoreData.iRank, GET_SPECTATOR_ROW(FALSE, serverBd.sFightingParticipantsList.iNumFightingParticipants, 1, iSpectatorNumber), i)
						iSavedSpectatorRow[NATIVE_TO_INT(PlayerId)] = iSpectatorRow	
						SET_BIT(iSpectatorBit, iSpectatorRow)
						iSpectatorNumber ++
					ENDIF
				ENDIF
			ENDIF
			
	        IF NATIVE_TO_INT(HordeLeaderboard[i].playerID) <> -1
	            IF IS_NET_PLAYER_OK(HordeLeaderboard[i].playerID, FALSE)
					
					iNumDeaths = 0
					
					IF IS_ENTITY_DEAD(GET_PLAYER_PED(HordeLeaderboard[i].playerID))
						iNumDeaths = 1
					ENDIF
					
					iSavedRow[NATIVE_TO_INT(HordeLeaderboard[i].playerID)] = iSlotCounter
					
					SET_LBD_SELECTION_TO_PLAYER_POS(lbdVars, 
						HordeLeaderboard[i].playerID, 
						iLBPlayerSelection, 
						iSlotCounter, 
						iNumberOfLeaderboardPlayers)
					
					POPULATE_COLUMN_LBD(lbdVars,
										LeaderboardPlacement,
										SUB_HORDE,
										LBD_VOTE_NOT_USED,
										tl63LeaderboardPlayerNames,
										GET_NUM_LBD_COLUMNS(SUB_HORDE),
										GET_KILL_DEATH_RATIO(HordeLeaderboard[i].iKills, HordeLeaderboard[i].iDeaths),
										HordeLeaderboard[i].iKills,
										iNumDeaths,
										HordeLeaderboard[i].iJobXP,
										0,
										0,
										0,
										0,
										0,
										iSlotCounter,
										i+1,
	//									HordeLeaderboard[i].iRank,
										HordeLeaderboard[i].iParticipant,
										HordeLeaderboard[i].iBadgeRank, 
										HordeLeaderboard[i].playerID, 
										FALSE,
										bDraw[0],
										-1,
										-1)
					
	                iSlotCounter++
	            ENDIF
	        ENDIF
	    ENDREPEAT
	ENDIF
ENDPROC
//===End Edit David G===

PROC MAINTAIN_SPECTATOR_HORDE_CAM()
    
    // Turn on the spectator camera.
    IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
	
        IF IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
			SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
			ACTIVATE_SPECTATOR_CAM( specData, SPEC_MODE_PLAYERS_STAY_DEAD_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_QUIT_SCREEN|ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED )
        ENDIF
		
	ELSE
	
		 // Horde hud.
    	MAINTAIN_SPECTATOR_HORDE_HUD()
		
		//===Start Edit David G===
	    IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
	        PROCESS_LEADERBOARD()
		ELSE
			SET_ON_LBD_GLOBAL(FALSE)
	    ENDIF
    	//===End Edit David G===
		
    ENDIF
    
ENDPROC

PROC SET_PROP_NODELS_AS_NO_LONGER_NEEDED()
    
    INT i
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i
            IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
                    SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
            ENDIF
    ENDREPEAT
    
ENDPROC

PROC SET_VEHICLE_MODELS_AS_NO_LONGER_NEEDED()
    
    INT i
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
            IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
                    SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
            ENDIF
    ENDREPEAT
                                    
ENDPROC

PROC SET_OBJECT_MODELS_AS_NO_LONGER_NEEDED()
    
    INT i
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects i
            IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn != DUMMY_MODEL_FOR_SCRIPT
                    SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
            ENDIF
    ENDREPEAT
                                    
ENDPROC

PROC SET_PED_MODELS_AS_NO_LONGER_NEEDED()
    
    INT i
    
    REPEAT MAX_NUM_SQUAD_PEDS i
        IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
            SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn)
        ENDIF
    ENDREPEAT
                                    
ENDPROC

FUNC BOOL WARP_TO_START_POSITION(INT &iStageVar, BOOL bDoQuickWarp = FALSE)
    
    SWITCH iStageVar
        
        CASE 0
                
            IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS, FALSE, FALSE, FALSE, bDoQuickWarp)
                iStageVar = 1
            ENDIF
                
        BREAK
        
        CASE 1
                
            RETURN TRUE
                
        BREAK
        
    ENDSWITCH
    
    RETURN FALSE
    
ENDFUNC

FUNC BOOL DO_PRE_LEAVE_SURVIVAL()
	
	PRINTLN("[Survival] - DO_PRE_LEAVE_SURVIVAL being called.")
	
    IF bIamQuittingCoronaSpectator
		PRINTLN("[Survival] - bIamQuittingCoronaSpectator = TRUE, DO_PRE_LEAVE_SURVIVAL returning TRUE.")
        RETURN TRUE
    ENDIF
    
    IF CONTROL_UPDATING_CHALLENGES()
		PRINTLN("[Survival] - CONTROL_UPDATING_CHALLENGES = TRUE, DO_PRE_LEAVE_SURVIVAL returning TRUE.")
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

FUNC BOOL SHOULD_I_DISPLAY_END_LEADERBOARD()
    
	IF bModeEndedSpecCamTranisition
		IF NOT IS_SCREEN_FADED_IN()
			RETURN FALSE
		ENDIF
	ENDIF
	
    IF NOT bIamQuittingCoronaSpectator
        RETURN TRUE
    ELSE
        IF GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET
            RETURN TRUE
        ENDIF
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
			RETURN TRUE
		ENDIF
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

FUNC BOOL SHOULD_I_PROCESS_END_HORDE_STATS()
    
    IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM) 
        RETURN FALSE
    ENDIF
    
    IF IS_PLAYER_SCTV(PLAYER_ID()) // Don't want corona or SCTV spectators to track stats.
        RETURN FALSE
    ENDIF
    
    RETURN TRUE
    
ENDFUNC

FUNC INT GET_FREE_SLOT_IN_FIGHTING_PARTICIPANT_LIST()
    
    INT i
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        IF serverBD.sFightingParticipantsList.iParticipant[i] = (-1)
            RETURN i
        ENDIF
    ENDREPEAT
    
    RETURN (-1)
    
ENDFUNC

PROC ADD_PARTICIPANT_TO_FIGHTING_PARTICIPANT_LIST(INT iPart, INT iSlot, BOOL bFullfighter)
	
	// Add them to the list and update the list data.
	
    PRINTLN("[Survival] - [Participant List] - adding participant ", iPart, " to fighting participant list slot ", iSlot)
    
	IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, iPart)
		
		PRINTLN("[Survival] - [Participant List] - bit ", iPart, " of sFightingParticipantsList.iPotentialFighterBitset not set.")
		
	    serverBD.sFightingParticipantsList.iParticipant[iSlot] = iPart
	    SET_BIT(serverBD.sFightingParticipantsList.iAddedBitset, iPart)
		
		PRINTLN("[Survival] - [Participant List] - sFightingParticipantsList.iParticipant[iSlot] = ", iPart)
		PRINTLN("[Survival] - [Participant List] - setting bit ", iPart, " of sFightingParticipantsList.iAddedBitset.")
		
	ENDIF
	
	// If the participant is a straight to spectator player.
   	IF NOT bFullfighter
		PRINTLN("[Survival] - [Participant List] - bFullfighter = FALSE, setting bit ", iPart, " of sFightingParticipantsList.iPotentialFighterBitset.")
    	SET_BIT(serverBD.sFightingParticipantsList.iPotentialFighterBitset, iPart)
	ELSE
		PRINTLN("[Survival] - [Participant List] - bFullfighter = TRUE, clearing bit ", iPart, " of sFightingParticipantsList.iPotentialFighterBitset.")
		CLEAR_BIT(serverBD.sFightingParticipantsList.iPotentialFighterBitset, iPart)
	ENDIF
	
ENDPROC

PROC INITIALISE_HORDE_FIGHTING_PARTICIPANT_LIST_DATA()
    
    INT i
    INT iSlot
    
    REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
        IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
           	
			 IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
				IF NOT IS_PLAYER_SPECTATING(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
				
		            IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iAddedBitset, i)
		                
		                iSlot = GET_FREE_SLOT_IN_FIGHTING_PARTICIPANT_LIST()
		                
		                IF iSlot != (-1)
		            
		                    PRINTLN("[Survival] - [Participant List] - adding participant ", i, " to fighting participant list slot ", iSlot)
		                    
		                    ADD_PARTICIPANT_TO_FIGHTING_PARTICIPANT_LIST(	i, 
																			iSlot, 
																			TRUE)
		                    
		                    PRINTLN("[Survival] - [Participant List] - iNumFightingParticipants now = ", serverBD.sFightingParticipantsList.iNumFightingParticipants)
		                    
		                ENDIF
		                
		            ENDIF
					
				ENDIF
			ENDIF
                
        ENDIF
    ENDREPEAT
    
    serverBD.sFightingParticipantsList.bListBeenInitialised = TRUE
    
ENDPROC

FUNC BOOL IS_PARTICIPANT_IN_VALID_STAGE_FOR_ADD_TO_PART_LIST_CHECK(PARTICIPANT_INDEX partId, BOOL bCheckingPotentialFighter = FALSE)
	
	SWITCH GET_CLIENT_HORDE_STAGE(partId) 
		CASE eHORDESTAGE_JIP_IN 					RETURN FALSE
	    CASE eHORDESTAGE_SETUP_MISSION 				RETURN FALSE
	    CASE eHORDESTAGE_START_FADE_IN 				RETURN FALSE
	    CASE eHORDESTAGE_FIGHTING 					RETURN TRUE
	    CASE eHORDESTAGE_PASSED_WAVE 				RETURN TRUE
	    CASE eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE		RETURN TRUE
	    CASE eHORDESTAGE_SPECTATING 				
			IF NOT bCheckingPotentialFighter
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
	    CASE eHORDESTAGE_SCTV 						RETURN FALSE
	    CASE eHORDESTAGE_REWARDS_AND_FEEDACK 		RETURN FALSE
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_NUM_FIGHTING_PARTICIPANTS()
	
	INT i
	
	#IF IS_DEBUG_BUILD
	INT iBeforeLoopValue = serverBD.sFightingParticipantsList.iNumFightingParticipants
	INT iBeforeLoopValue2 = serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants
	#ENDIF
	
	serverBD.sFightingParticipantsList.iNumFightingParticipants = 0
	serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants = 0
	
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS  i
		IF serverBD.sFightingParticipantsList.iParticipant[i] >= 0
			IF IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
				serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants++
			ELSE
				serverBD.sFightingParticipantsList.iNumFightingParticipants++
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	IF iBeforeLoopValue != serverBD.sFightingParticipantsList.iNumFightingParticipants
		PRINTLN("[Survival] - iNumFightingParticipants changed to ", serverBD.sFightingParticipantsList.iNumFightingParticipants)
	ENDIF
	IF iBeforeLoopValue2 != serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants
		PRINTLN("[Survival] - iNumPotentialfightingParticipants changed to ", serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants)
	ENDIF
	#ENDIF
	
ENDPROC

PROC MAINTAIN_FIGHTING_PARTICIPANTS_LIST()
	
	INT iSlot, iListIndex, i
	
	// Initialise the list.
    IF NOT serverBD.sFightingParticipantsList.bListBeenInitialised
      	
		IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE 
        	INITIALISE_HORDE_FIGHTING_PARTICIPANT_LIST_DATA()
      	ENDIF
		
    ELSE
		
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
			
			// If particpant is active and not an sctv participant.
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			
				IF IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
				
					IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, ", participant active.") ENDIF
					
					IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
						
						IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, " not an sctv participant.") ENDIF
						
						IF NOT IS_PLAYER_SPECTATING(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
						AND IS_PARTICIPANT_IN_VALID_STAGE_FOR_ADD_TO_PART_LIST_CHECK(INT_TO_PARTICIPANTINDEX(i))
							
							// If we haven't added them to the list yet.
							IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iAddedBitset, i)
								
								IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, " not been added to list.") ENDIF
								
								// If we have room on the list, add participant to it.
								IF (serverBD.sFightingParticipantsList.iNumFightingParticipants + serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants) < MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS
									
									IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, ", room on list.") ENDIF
									
									// Get a free slot for them.
								    iSlot = GET_FREE_SLOT_IN_FIGHTING_PARTICIPANT_LIST()
								    
									// If we successfully got a free slot.
								    IF iSlot != (-1)
										ADD_PARTICIPANT_TO_FIGHTING_PARTICIPANT_LIST(	i, 
																						iSlot, 
																						TRUE )
										IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, ", added to list.") ENDIF
									ENDIF
									
								ENDIF
								
							// If participant is already on the list.
							ELSE
								
								IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, ", already on list.") ENDIF
								
								// If participant is only a potential fighter.
								IF IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, i)
									
									IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, ", is a potential fighter.") ENDIF
									
									// If the participant is no longer a straight to spectator participant.
									IF NOT IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
										
										IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, ", not a straight to spec cam participant.") ENDIF
										
										// Set them as not just a potential fighter (give them full fighter status next time they are found in the loop!).
										ADD_PARTICIPANT_TO_FIGHTING_PARTICIPANT_LIST(	i, 
																						iSlot, 
																						TRUE)
										
									ENDIF
									
								ENDIF
								
							ENDIF
						
						ELSE
							
							IF IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
							
								// If we haven't added them to the list yet.
								IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iAddedBitset, i)
									
									// If we have room on the list, add participant to it.
									IF (serverBD.sFightingParticipantsList.iNumFightingParticipants + serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants) < MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS
										
										// Get a free slot for them.
									    iSlot = GET_FREE_SLOT_IN_FIGHTING_PARTICIPANT_LIST()
										
										// If we successfully got a free slot.
									    IF iSlot != (-1)
											ADD_PARTICIPANT_TO_FIGHTING_PARTICIPANT_LIST(	i, 
																							iSlot, 
																							FALSE)
											IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, ", added to list.") ENDIF
										ENDIF
										
									ENDIF
									
								ENDIF
							
							ENDIF
							
						ENDIF
						
					ENDIF
				
				ENDIF
				
			// If the participant is no longer active.
			ELSE
				
				// If the participant was added to the list.
				IF IS_BIT_SET(serverBD.sFightingParticipantsList.iAddedBitset, i)
					
					// Loop through the list.
					REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS iListIndex
						
						// If we have found the participant in the list.
						IF serverBD.sFightingParticipantsList.iParticipant[iListIndex] = i
				        	
							// Remove them from the list.
							PRINTLN("[Survival] - [Participant List] - removing participant ", i, " from fighting participant list slot ", iListIndex)
				            
				            serverBD.sFightingParticipantsList.iParticipant[iListIndex] = (-1)
				            CLEAR_BIT(serverBD.sFightingParticipantsList.iAddedBitset, i)
				            CLEAR_BIT(serverBD.sFightingParticipantsList.iPotentialFighterBitset, i)
							
				            IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - iNumFightingParticipants now = ", serverBD.sFightingParticipantsList.iNumFightingParticipants) ENDIF
				        	
						ENDIF
						
					ENDREPEAT
				
				ENDIF
				
			ENDIF
			
		ENDREPEAT
//		
//		iFightingPartListCount++
//		
//		IF iFightingPartListCount >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
//			iFightingPartListCount = 0
//		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL HAVE_ALL_FIGHTING_PARTICIPANTS_SPAWNED_FADED_IN()
    
    INT i
    INT iPartId
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        
        IF serverBD.sFightingParticipantsList.iParticipant[i] != (-1)
			IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
	            iPartId = serverBD.sFightingParticipantsList.iParticipant[i]
	            IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartId))
	                IF NOT IS_BIT_SET(playerBD[iPartId].iBitset, BITSET_PLAYER_I_AM_ALIVE_AND_FADED_IN)
	                    RETURN FALSE
	                ENDIF
	            ENDIF
			ENDIF
        ENDIF
        
    ENDREPEAT
    
    RETURN TRUE
    
ENDFUNC

FUNC BOOL FIND_LAND_VEHICLE_SPAWN_COORDS(STRUCT_SURVIVAL_LAND_VEHICLE_DATA &sSurvivalLandVehicle, INT iLandVehicle)
	
	VECTOR vtemp
	FLOAT fTemp
	// g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[0].vPos
    // Get spawn point.
    IF IS_VECTOR_ZERO(sSurvivalLandVehicle.vSpawncoords)
		PRINTLN("[Survival] - [AI Spawning] - FIND_LAND_VEHICLE_SPAWN_COORDS - sSurvivalLandVehicle.vSpawncoords = ZERO.")
		
		VEHICLE_SPAWN_LOCATION_PARAMS Params
		Params.fMinDistFromCoords= 0.0 
		Params.bConsiderHighways= TRUE
		Params.fMaxDistance= 200.0
		Params.bConsiderOnlyActiveNodes= FALSE
		Params.bAvoidSpawningInExclusionZones= FALSE

        IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iLandVehicle].vPos, g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos, sSurvivalLandVehicle.eModel, FALSE,  vTemp, fTemp, Params)
//        IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iLandVehicle].vPos, 60, vTemp, fTemp, g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos, FALSE, -1, TRUE, 40, FALSE, FALSE, FALSE, FALSE, TRUE)
			sSurvivalLandVehicle.vSpawncoords = vTemp
			PRINTLN("[Survival] - [AI Spawning] - FIND_LAND_VEHICLE_SPAWN_COORDS - GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY got coords at ", sSurvivalLandVehicle.vSpawncoords)
			RETURN TRUE
		ELSE
			PRINTLN("[Survival] - [AI Spawning] - FIND_LAND_VEHICLE_SPAWN_COORDS - GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY returning FALSE.")
        ENDIF
	ELSE
		RETURN TRUE
    ENDIF
   	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SPAWN_SURVIVAL_LAND_VEHICLE(STRUCT_SURVIVAL_LAND_VEHICLE_DATA &sSurvivalLandVehicle)
    
    IF REQUEST_LOAD_MODEL(sSurvivalLandVehicle.eModel)
        IF REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].mn)
            
            IF NOT IS_NET_VEHICLE_DRIVEABLE(sSurvivalLandVehicle.netId)
        
                IF CREATE_NET_VEHICLE(sSurvivalLandVehicle.netId, sSurvivalLandVehicle.eModel, sSurvivalLandVehicle.vSpawnCoords, 0.0, TRUE)
                    
                    SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(sSurvivalLandVehicle.netId), VEHICLELOCK_LOCKED)
					
                    IF CREATE_NET_PED_IN_VEHICLE(sSurvivalLandVehicle.sPed[0].netId, sSurvivalLandVehicle.netId, PEDTYPE_CIVMALE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].mn, VS_DRIVER, TRUE)
                        
                        // Set driver properties.
                        SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(sSurvivalLandVehicle.sPed[0].netId), rgFM_AiHate)
                        SETUP_PED_FOR_WAVE(0, 0, NET_TO_PED(sSurvivalLandVehicle.sPed[0].netId), TRUE)
						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalLandVehicle.sPed[0].netId), CA_DISABLE_TARGET_CHANGES_DURING_VEHICLE_PURSUIT, TRUE)
                        SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalLandVehicle.sPed[0].netId), CA_DO_DRIVEBYS, TRUE)
						SET_PED_AS_ENEMY(NET_TO_PED(sSurvivalLandVehicle.sPed[0].netId), TRUE)
						
                        IF CREATE_NET_PED_IN_VEHICLE(sSurvivalLandVehicle.sPed[1].netId, sSurvivalLandVehicle.netId, PEDTYPE_CIVMALE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].mn, VS_FRONT_RIGHT, TRUE)
                            
                            SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(sSurvivalLandVehicle.sPed[1].netId), rgFM_AiHate)
                            SETUP_PED_FOR_WAVE(0, 0, NET_TO_PED(sSurvivalLandVehicle.sPed[1].netId), TRUE)
                            SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalLandVehicle.sPed[1].netId), CA_DO_DRIVEBYS, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalLandVehicle.sPed[1].netId), CA_DISABLE_TARGET_CHANGES_DURING_VEHICLE_PURSUIT, TRUE)
							SET_PED_AS_ENEMY(NET_TO_PED(sSurvivalLandVehicle.sPed[1].netId), TRUE)
							
							IF sSurvivalLandVehicle.iNumPeds >= 3
							
								IF CREATE_NET_PED_IN_VEHICLE(sSurvivalLandVehicle.sPed[2].netId, sSurvivalLandVehicle.netId, PEDTYPE_CIVMALE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].mn, VS_BACK_LEFT, TRUE)
                          			
									SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(sSurvivalLandVehicle.sPed[2].netId), rgFM_AiHate)
		                            SETUP_PED_FOR_WAVE(0, 0, NET_TO_PED(sSurvivalLandVehicle.sPed[2].netId), TRUE)
		                            SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalLandVehicle.sPed[2].netId), CA_DO_DRIVEBYS, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalLandVehicle.sPed[2].netId), CA_DISABLE_TARGET_CHANGES_DURING_VEHICLE_PURSUIT, TRUE)
									SET_PED_AS_ENEMY(NET_TO_PED(sSurvivalLandVehicle.sPed[2].netId), TRUE)
									
									IF sSurvivalLandVehicle.iNumPeds >= 4
									
										IF CREATE_NET_PED_IN_VEHICLE(sSurvivalLandVehicle.sPed[3].netId, sSurvivalLandVehicle.netId, PEDTYPE_CIVMALE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].mn, VS_BACK_RIGHT, TRUE)
	                          				
											SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(sSurvivalLandVehicle.sPed[3].netId), rgFM_AiHate)
				                            SETUP_PED_FOR_WAVE(0, 0, NET_TO_PED(sSurvivalLandVehicle.sPed[3].netId), TRUE)
				                            SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalLandVehicle.sPed[3].netId), CA_DO_DRIVEBYS, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalLandVehicle.sPed[3].netId), CA_DISABLE_TARGET_CHANGES_DURING_VEHICLE_PURSUIT, TRUE)
											SET_PED_AS_ENEMY(NET_TO_PED(sSurvivalLandVehicle.sPed[3].netId), TRUE)
											
											RETURN TRUE
										
										ENDIF
										
									ELSE
									
										RETURN TRUE
									
									ENDIF
								
								ENDIF
								
							ELSE
							
								RETURN TRUE
							
							ENDIF
							
                        ENDIF
                        
                    ENDIF
                    
                ENDIF
                
            ENDIF
    
        ENDIF
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

// *********************************** \\
// *********************************** \\
//                                     \\
//			AI HELICOPTERS             \\
//                                     \\
// *********************************** \\
// *********************************** \\

FUNC BOOL FIND_SURVIVAL_HELI_SPAWN_COORDS(STRUCT_SURVIVAL_HELI_DATA &sSurvivalHeli)
    
    VECTOR vtemp
    
    IF IS_VECTOR_ZERO(sSurvivalHeli.vSpawnCoords)
        
        vtemp = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(  g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos,
                                                        0.0,
                                                        <<(-1*200 * SIN(TO_FLOAT(sSurvivalHeli.iCircCount)*30)),(200 * COS(TO_FLOAT(sSurvivalHeli.iCircCount)*30)),0>>      )
        
        vtemp.z = GET_APPROX_HEIGHT_FOR_POINT(Vtemp.x, Vtemp.y)
        
        vtemp.z += 30.0
        
        IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vtemp, 20.0)
                
            sSurvivalHeli.vSpawnCoords = vtemp
                
        ELSE
        
            sSurvivalHeli.iCircCount++
            
            IF sSurvivalHeli.iCircCount >= 12
                sSurvivalHeli.iCircCount = 0
            ENDIF
            
        ENDIF
            
    ELSE
    
        RETURN TRUE
    
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

FUNC BOOL SPAWN_SURVIVAL_HELI(STRUCT_SURVIVAL_HELI_DATA &sSurvivalHeli)
    
	INT iHealth
	
	IF serverBD.sWaveData.iWaveCount > 4
		sSurvivalHeli.eModel = BUZZARD2
	ENDIF
	
    IF REQUEST_LOAD_MODEL(sSurvivalHeli.eModel)
        IF REQUEST_LOAD_MODEL(sSurvivalHeli.sPed[0].eModel)
            
            IF NOT IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
        
                IF CREATE_NET_VEHICLE(sSurvivalHeli.netId, sSurvivalHeli.eModel, sSurvivalHeli.vSpawnCoords, 0.0, TRUE)
                    
                    SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(sSurvivalHeli.netId), VEHICLELOCK_LOCKED)
                    SET_HELI_BLADES_FULL_SPEED(NET_TO_VEH(sSurvivalHeli.netId))
					SET_VEHICLE_ENGINE_ON(NET_TO_VEH(sSurvivalHeli.netId), TRUE, TRUE)
					ACTIVATE_PHYSICS(NET_TO_VEH(sSurvivalHeli.netId))
					iHealth = GET_ENTITY_HEALTH(NET_TO_VEH(sSurvivalHeli.netId))
					iHealth = (iHealth/3)
					SET_ENTITY_MAX_HEALTH(NET_TO_VEH(sSurvivalHeli.netId), iHealth)
					SET_ENTITY_HEALTH(NET_TO_VEH(sSurvivalHeli.netId), iHealth)
					
                    IF CREATE_NET_PED_IN_VEHICLE(sSurvivalHeli.sPed[0].netId, sSurvivalHeli.netId, PEDTYPE_CIVMALE, sSurvivalHeli.sPed[0].eModel, VS_DRIVER, TRUE)
                        
                        // Set driver properties.
                        SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(sSurvivalHeli.sPed[0].netId), rgFM_AiHate)
                        GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(sSurvivalHeli.sPed[0].netId), WEAPONTYPE_COMBATMG, 15000, TRUE)
                        SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalHeli.sPed[0].netId), CA_ALWAYS_FIGHT, TRUE)
                        SET_PED_COMBAT_MOVEMENT(NET_TO_PED(sSurvivalHeli.sPed[0].netId), CM_WILLADVANCE)
						SET_PED_CONFIG_FLAG(NET_TO_PED(sSurvivalHeli.sPed[0].netId), PCF_GetOutBurningVehicle, FALSE)
						SET_PED_CONFIG_FLAG(NET_TO_PED(sSurvivalHeli.sPed[0].netId), PCF_GetOutUndriveableVehicle, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalHeli.sPed[0].netId), CA_LEAVE_VEHICLES, FALSE)
						SET_PED_AS_ENEMY(NET_TO_PED(sSurvivalHeli.sPed[0].netId), TRUE)
						
                        IF CREATE_NET_PED_IN_VEHICLE(sSurvivalHeli.sPed[1].netId, sSurvivalHeli.netId, PEDTYPE_CIVMALE, sSurvivalHeli.sPed[1].eModel, VS_BACK_LEFT, TRUE)
                            
                            // Set driver properties.
                            SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(sSurvivalHeli.sPed[1].netId), rgFM_AiHate)
                            GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(sSurvivalHeli.sPed[1].netId), WEAPONTYPE_COMBATMG, 15000, TRUE)
                            SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalHeli.sPed[1].netId), CA_DO_DRIVEBYS, TRUE)
                            SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalHeli.sPed[1].netId), CA_ALWAYS_FIGHT, TRUE)
                            SET_PED_COMBAT_MOVEMENT(NET_TO_PED(sSurvivalHeli.sPed[1].netId), CM_WILLADVANCE)
                            SET_PED_FIRING_PATTERN(NET_TO_PED(sSurvivalHeli.sPed[1].netId),FIRING_PATTERN_BURST_FIRE_HELI)
							SET_PED_CONFIG_FLAG(NET_TO_PED(sSurvivalHeli.sPed[1].netId), PCF_GetOutBurningVehicle, FALSE)
							SET_PED_CONFIG_FLAG(NET_TO_PED(sSurvivalHeli.sPed[1].netId), PCF_GetOutUndriveableVehicle, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalHeli.sPed[1].netId), CA_LEAVE_VEHICLES, FALSE)
							SET_PED_AS_ENEMY(NET_TO_PED(sSurvivalHeli.sPed[1].netId), TRUE)
							SET_PED_ACCURACY(NET_TO_PED(sSurvivalHeli.sPed[1].netId), 9)
							
                            IF CREATE_NET_PED_IN_VEHICLE(sSurvivalHeli.sPed[2].netId, sSurvivalHeli.netId, PEDTYPE_CIVMALE, sSurvivalHeli.sPed[2].eModel, VS_BACK_RIGHT, TRUE)
                                
                                // Set driver properties.
                                SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(sSurvivalHeli.sPed[2].netId), rgFM_AiHate)
                                GIVE_DELAYED_WEAPON_TO_PED(NET_TO_PED(sSurvivalHeli.sPed[2].netId), WEAPONTYPE_COMBATMG, 15000, TRUE)
                                SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalHeli.sPed[2].netId), CA_DO_DRIVEBYS, TRUE)
                                SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalHeli.sPed[2].netId), CA_ALWAYS_FIGHT, TRUE)
                                SET_PED_COMBAT_MOVEMENT(NET_TO_PED(sSurvivalHeli.sPed[2].netId), CM_WILLADVANCE)
                                SET_PED_FIRING_PATTERN(NET_TO_PED(sSurvivalHeli.sPed[2].netId),FIRING_PATTERN_BURST_FIRE_HELI)
								SET_PED_CONFIG_FLAG(NET_TO_PED(sSurvivalHeli.sPed[2].netId), PCF_GetOutBurningVehicle, FALSE)
								SET_PED_CONFIG_FLAG(NET_TO_PED(sSurvivalHeli.sPed[2].netId), PCF_GetOutUndriveableVehicle, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(sSurvivalHeli.sPed[2].netId), CA_LEAVE_VEHICLES, FALSE)
								SET_PED_AS_ENEMY(NET_TO_PED(sSurvivalHeli.sPed[2].netId), TRUE)
								SET_PED_ACCURACY(NET_TO_PED(sSurvivalHeli.sPed[1].netId), 9)
								
                                serverBD.iNumAliveHelis++
//                              serverBD.sWaveData.iNumRequiredKills++
                                
                                RETURN TRUE
                                
                            ENDIF
                            
                        ENDIF
                        
                    ENDIF
                    
                ENDIF
                
            ENDIF
    
        ENDIF
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

FUNC INT GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS()
	
	RETURN (serverBD.iNumAliveSquadEnemies + serverBD.iNumAliveLandVehiclePeds)
	
ENDFUNC


PROC MAINTAIN_ACTIVATING_LAND_VEHICLES()
	
	SWITCH serverBD.sWaveData.iWaveCount
		
		CASE 3
			
			IF serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM
				IF GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS() <= 5
					IF serverBD.sSurvivalLandVehicle[0].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (SANDKING2).")
						serverBD.sSurvivalLandVehicle[0].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
					IF serverBD.sSurvivalLandVehicle[1].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (SANDKING2).")
						serverBD.sSurvivalLandVehicle[1].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 5
			
			IF serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM
				IF GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS() <= 5
					IF serverBD.sSurvivalLandVehicle[0].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (SANDKING2) ")
						serverBD.sSurvivalLandVehicle[0].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
				ENDIF
			ELIF serverBD.sWaveData.eStage = eWAVESTAGE_HARD
				IF GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS() <= 5
					IF serverBD.sSurvivalLandVehicle[1].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (CAVALCADE) ")
						serverBD.sSurvivalLandVehicle[1].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
					IF GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE() >= 3
						IF serverBD.sSurvivalLandVehicle[2].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
							PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (CAVALCADE) ")
							serverBD.sSurvivalLandVehicle[2].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 6
			
			IF serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM
				IF GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS() <= 5
					IF serverBD.sSurvivalLandVehicle[0].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (SANDKING2) ")
						serverBD.sSurvivalLandVehicle[0].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
					IF serverBD.sSurvivalLandVehicle[1].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (CAVALCADE) ")
						serverBD.sSurvivalLandVehicle[1].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
				ENDIF
			ELIF serverBD.sWaveData.eStage = eWAVESTAGE_HARD
				IF GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS() <= 5
					IF serverBD.sSurvivalLandVehicle[2].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (SPEEDO) ")
						serverBD.sSurvivalLandVehicle[2].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
					IF GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE() >= 3
						IF serverBD.sSurvivalLandVehicle[3].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
							PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (SPEEDO) ")
							serverBD.sSurvivalLandVehicle[3].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 8
			
			IF serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM
				IF GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS() <= 5
					IF serverBD.sSurvivalLandVehicle[0].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (SANDKING2) ")
						serverBD.sSurvivalLandVehicle[0].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
				ENDIF
			ELIF serverBD.sWaveData.eStage = eWAVESTAGE_HARD
				IF GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS() <= 5
					IF serverBD.sSurvivalLandVehicle[1].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (CAVALCADE) ")
						serverBD.sSurvivalLandVehicle[1].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
					IF serverBD.sSurvivalLandVehicle[2].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (CAVALCADE) ")
						serverBD.sSurvivalLandVehicle[2].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
					IF GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE() >= 4
						IF serverBD.sSurvivalLandVehicle[3].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
							PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (SPEEDO) ")
							serverBD.sSurvivalLandVehicle[3].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 9
			
			IF serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM
				IF GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS() <= 5
					IF serverBD.sSurvivalLandVehicle[0].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] -  MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (SANDKING2) ")
						serverBD.sSurvivalLandVehicle[0].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
				ENDIF
			ELIF serverBD.sWaveData.eStage = eWAVESTAGE_HARD
				IF GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS() <= 5
					IF serverBD.sSurvivalLandVehicle[1].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (CAVALCADE) ")
						serverBD.sSurvivalLandVehicle[1].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
					IF serverBD.sSurvivalLandVehicle[2].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (SPEEDO) ")
						serverBD.sSurvivalLandVehicle[2].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
					ENDIF
					IF GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE() >= 4
						IF serverBD.sSurvivalLandVehicle[3].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
							PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating land vehicle (SPEEDO) ")
							serverBD.sSurvivalLandVehicle[3].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC MAINTAIN_ACTIVATING_HELIS()
    
    SWITCH serverBD.sWaveData.iWaveCount
        
        CASE 4
            
			IF NOT serverBD.sSurvivalHeli[0].bActive
	            IF serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM
					PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_HELIS - activating heli 0.")
	               	serverBD.sSurvivalHeli[0].bActive = TRUE
	            ENDIF
            ENDIF
			
        BREAK
        
        CASE 7
                
            IF serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM
				IF NOT serverBD.sSurvivalHeli[0].bActive
					PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_HELIS - activating heli 0.")
                	serverBD.sSurvivalHeli[0].bActive = TRUE
				ENDIF
                IF serverBD.sFightingParticipantsList.iNumFightingParticipants >= 2
					IF NOT serverBD.sSurvivalHeli[1].bActive	
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_HELIS - activating heli 1.")
						serverBD.sSurvivalHeli[1].bActive = TRUE
					ENDIF
                ENDIF
            ENDIF
                   
        BREAK
        
        CASE 10
        	
            IF serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM
				IF NOT serverBD.sSurvivalHeli[0].bActive
					PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_HELIS - activating heli 0.")
               		serverBD.sSurvivalHeli[0].bActive = TRUE
				ENDIF
				IF NOT serverBD.sSurvivalHeli[1].bActive
					PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_HELIS - activating heli 1.")
                    serverBD.sSurvivalHeli[1].bActive = TRUE
				ENDIF
                IF serverBD.sFightingParticipantsList.iNumFightingParticipants >= 3
				AND (serverBD.iNumAliveHelis <= 1)
					IF NOT serverBD.sSurvivalHeli[2].bActive
						PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_HELIS - activating heli 2.")
                    	serverBD.sSurvivalHeli[2].bActive = TRUE
					ENDIF
                ENDIF
            ENDIF
                                
        BREAK
            
    ENDSWITCH
    
ENDPROC

INT iHeliAttackRangePartCount

FUNC BOOL IS_HELI_WITHIN_ATTACK_RANGE_OF_SURVIVAL(NETWORK_INDEX niHeli)
	
	PARTICIPANT_INDEX partId
	PLAYER_INDEX playerId
	PED_INDEX pedId
	VEHICLE_INDEX viHeli
	VECTOR vHeliCoords
	
	IF NOT IS_NET_VEHICLE_DRIVEABLE(niHeli)
		RETURN FALSE
	ENDIF
	
	viHeli = NET_TO_VEH(niHeli)
	vHeliCoords = GET_ENTITY_COORDS(viHeli)
	
	IF (GET_DISTANCE_BETWEEN_COORDS(vHeliCoords, g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos) <= 150.0)
		PRINTLN("[Survival] - [AI Spawning] - heli is <= 150.0m from survival.")
		RETURN TRUE
	ENDIF
	
	IF (serverBD.sFightingParticipantsList.iParticipant[iHeliAttackRangePartCount] != -1)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, serverBD.sFightingParticipantsList.iParticipant[iHeliAttackRangePartCount]))
			IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[iHeliAttackRangePartCount])
				
				partId = INT_TO_NATIVE(PARTICIPANT_INDEX, serverBD.sFightingParticipantsList.iParticipant[iHeliAttackRangePartCount])
				
				IF NOT IS_PLAYER_SCTV(playerId)
					IF NOT IS_PLAYER_SPECTATING(playerId)
						
						playerId = NETWORK_GET_PLAYER_INDEX(partId)
						pedId = GET_PLAYER_PED(playerId)
						
						IF DOES_ENTITY_EXIST(pedId)
							IF NOT IS_PED_INJURED(pedId)
								IF (GET_DISTANCE_BETWEEN_ENTITIES(viHeli, pedId) <= 200.0)
									PRINTLN("[Survival] - [AI Spawning] - heli is <= 200.0m from participant ", serverBD.sFightingParticipantsList.iParticipant[iHeliAttackRangePartCount])
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF
	
	iHeliAttackRangePartCount++
	
	IF (iHeliAttackRangePartCount >= MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS)
		iHeliAttackRangePartCount = 0
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_HELI_BRAIN(STRUCT_SURVIVAL_HELI_DATA &sSurvivalHeli, INT iHeli)
    
    BOOL bResetState = TRUE
    STRUCT_SURVIVAL_HELI_DATA sTemp
	eSURVIVAL_HELI_STATE eTempState
	
    SWITCH sSurvivalHeli.eState
        
        CASE eSURVIVALHELISTATE_NOT_EXIST
            
            IF sSurvivalHeli.bActive
                sSurvivalHeli.eState = eSURVIVALHELISTATE_SPAWNING
                PRINTLN("[Survival] - [AI Spawning] - sSurvivalHeli.bActive - setting heli state to eSURVIVALHELISTATE_SPAWNING")
            ENDIF
            
        BREAK
        
        CASE eSURVIVALHELISTATE_SPAWNING
          	
            IF (serverBd.iSearchingForChopper = (-1) AND (serverBd.iSearchingForPed = -1) AND (serverBd.iSearchingForLandVehicle = -1))
            OR (serverBd.iSearchingForChopper = iHeli AND (serverBd.iSearchingForPed = -1) AND (serverBd.iSearchingForLandVehicle = -1))
                IF FIND_SURVIVAL_HELI_SPAWN_COORDS(sSurvivalHeli)
                    IF SPAWN_SURVIVAL_HELI(sSurvivalHeli)
                        serverBd.iSearchingForChopper = (-1)
						serverBD.iNumCreatedEnemies++
                        sSurvivalHeli.eState = eSURVIVALHELISTATE_FLYING_TO_SURVIVAL
                        PRINTLN("[Survival] - [AI Spawning] - heli spawned - setting heli state to eSURVIVALHELISTATE_FLYING_TO_SURVIVAL")
                    ELSE
                        PRINTLN("[Survival] - [AI Spawning] - found suitable spawn coords, spawning heli.")
                    ENDIF
                ELSE
                    PRINTLN("[Survival] - [AI Spawning] - finding heli spawn coords.")
                ENDIF
            ENDIF
                
        BREAK
        
		CASE eSURVIVALHELISTATE_FLYING_TO_SURVIVAL
			
			IF NOT IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
				IF IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
				AND IS_NET_PED_INJURED(sSurvivalHeli.sPed[1].netId)
				AND IS_NET_PED_INJURED(sSurvivalHeli.sPed[2].netId)
	                serverBD.iNumAliveHelis--
					serverBD.iKillsThisWave++
					serverBD.iKills++
                	serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage]++
					RESET_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
					START_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
	                sSurvivalHeli.eState = eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
	                PRINTLN("[Survival] - [AI Spawning] - heli not driveable - setting heli state to eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP")
				ENDIF
            ELIF IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
				IF IS_NET_PED_INJURED(sSurvivalHeli.sPed[1].netId)
				AND IS_NET_PED_INJURED(sSurvivalHeli.sPed[2].netId)
	                serverBD.iNumAliveHelis--
					serverBD.iKillsThisWave++
					serverBD.iKills++
                	serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage]++
					RESET_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
					START_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
	                sSurvivalHeli.eState = eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
	                PRINTLN("[Survival] - [AI Spawning] - pilot - setting heli state to eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP")
				ENDIF
            ENDIF
			
			IF IS_HELI_WITHIN_ATTACK_RANGE_OF_SURVIVAL(sSurvivalHeli.netId)
				sSurvivalHeli.eState = eSURVIVALHELISTATE_FLYING_FIGHTING
            	PRINTLN("[Survival] - [AI Spawning] - heli spawned - setting heli state to eSURVIVALHELISTATE_FLYING_FIGHTING")
			ENDIF
			
		BREAK
		
        CASE eSURVIVALHELISTATE_FLYING_FIGHTING
              
            IF NOT IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
				IF IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
				AND IS_NET_PED_INJURED(sSurvivalHeli.sPed[1].netId)
				AND IS_NET_PED_INJURED(sSurvivalHeli.sPed[2].netId)
	                serverBD.iNumAliveHelis--
					serverBD.iKillsThisWave++
					serverBD.iKills++
                	serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage]++
					RESET_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
					START_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
	                sSurvivalHeli.eState = eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
	                PRINTLN("[Survival] - [AI Spawning] - heli not driveable - setting heli state to eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP")
				ENDIF
            ELIF IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
				IF IS_NET_PED_INJURED(sSurvivalHeli.sPed[1].netId)
				AND IS_NET_PED_INJURED(sSurvivalHeli.sPed[2].netId)
	                serverBD.iNumAliveHelis--
					serverBD.iKillsThisWave++
					serverBD.iKills++
                	serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage]++
					RESET_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
					START_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
	                sSurvivalHeli.eState = eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
	                PRINTLN("[Survival] - [AI Spawning] - pilot - setting heli state to eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP")
				ENDIF
            ENDIF
                
        BREAK
        
		CASE eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.netId)
				IF IS_ENTITY_DEAD(NET_TO_VEH(sSurvivalHeli.netId))
					sSurvivalHeli.eState = eSURVIVALHELISTATE_DEAD
					PRINTLN("[Survival] - [AI Spawning] - pilot - setting heli state to eSURVIVALHELISTATE_DEAD")
				ENDIF
			ENDIF
			
		BREAK
		
        CASE eSURVIVALHELISTATE_DEAD
            
            IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.sPed[0].netId)
                bResetState = FALSE
            ENDIF
            
            IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.sPed[1].netId)
                bResetState = FALSE
            ENDIF
            
            IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.sPed[2].netId)
                bResetState = FALSE
            ENDIF
            
            IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.netId)
                bResetState = FALSE
            ENDIF
            
            IF bResetState
                
				eTempState						= sSurvivalHeli.eState
                sSurvivalHeli                   = sTemp
                serverBd.iSearchingForChopper   = (-1)
                sSurvivalHeli.eState			= eTempState
				
                SWITCH iHeli
                    CASE 0  serverBD.sSurvivalHeli[0].iCircCount = SURVIVAL_HELI_0_START_CIRC_COUNT        BREAK
                    CASE 1  serverBD.sSurvivalHeli[1].iCircCount = SURVIVAL_HELI_1_START_CIRC_COUNT        BREAK
                    CASE 2  serverBD.sSurvivalHeli[2].iCircCount = SURVIVAL_HELI_2_START_CIRC_COUNT        BREAK
                ENDSWITCH
                
                IF serverBD.sWaveData.eStage = eWAVESTAGE_EASY
                    sSurvivalHeli.eState = eSURVIVALHELISTATE_NOT_EXIST
                ENDIF
                
            ENDIF
            
        BREAK
        
    ENDSWITCH
    
ENDPROC

PROC TRACK_NUM_ALIVE_LAND_VEHICLE_PEDS()
	
	INT i, j
	
	serverBD.iNumAliveLandVehiclePeds = 0
	
	REPEAT MAX_NUM_LAND_VEHICLES i
		REPEAT NUM_LAND_VEH_PEDS j
			IF NOT IS_NET_PED_INJURED(serverBD.sSurvivalLandVehicle[i].sPed[j].netId)
				serverBD.iNumAliveLandVehiclePeds++
			ENDIF
		ENDREPEAT
	ENDREPEAT 
	
ENDPROC

PROC PROCESS_LAND_VEHICILE_BRAIN(STRUCT_SURVIVAL_LAND_VEHICLE_DATA &sSurvivalLandVehicle, INT iVehicle)
	
	INT i
    BOOL bResetState = TRUE
    STRUCT_SURVIVAL_LAND_VEHICLE_DATA sTemp
	BOOL bPedInjured[NUM_LAND_VEH_PEDS]
	eSURVIVAL_LAND_VEHICLE_STATE eState
	
	REPEAT NUM_LAND_VEH_PEDS i
		IF IS_NET_PED_INJURED(sSurvivalLandVehicle.sPed[i].netId)
			bPedInjured[i] = TRUE
		ENDIF
	ENDREPEAT
	
    SWITCH sSurvivalLandVehicle.eState
    	
        CASE eSURVIVALLANDVEHICLESTATE_SPAWNING
        	
            IF (serverBd.iSearchingForLandVehicle = (-1) AND (serverBd.iSearchingForPed = -1) AND (serverBd.iSearchingForChopper = -1))
            OR (serverBd.iSearchingForLandVehicle = iVehicle AND (serverBd.iSearchingForPed = -1) AND (serverBd.iSearchingForChopper = -1))
				serverBd.iSearchingForLandVehicle = iVehicle
				PRINTLN("[Survival] - [AI Spawning] - PROCESS_LAND_VEHICILE_BRAIN - serverBd.iSearchingForLandVehicle = ", serverBd.iSearchingForLandVehicle)
                IF FIND_LAND_VEHICLE_SPAWN_COORDS(sSurvivalLandVehicle, iVehicle)
                    IF SPAWN_SURVIVAL_LAND_VEHICLE(sSurvivalLandVehicle)
						serverBD.iNumCreatedEnemies += sSurvivalLandVehicle.iNumPeds
                        serverBd.iSearchingForLandVehicle = (-1)
                        sSurvivalLandVehicle.eState = eSURVIVALLANDVEHICLESTATE_DRIVING
                        PRINTLN("[Survival] - [AI Spawning] - PROCESS_LAND_VEHICILE_BRAIN - land vehicle spawned - setting state to eSURVIVALLANDVEHICLESTATE_DRIVING")
                    ELSE
                        PRINTLN("[Survival] - [AI Spawning] - PROCESS_LAND_VEHICILE_BRAIN - found suitable spawn coords, spawning land vehicle.")
                    ENDIF
                ELSE
                    PRINTLN("[Survival] - [AI Spawning] - PROCESS_LAND_VEHICILE_BRAIN - finding bike spawn coords.")
                ENDIF
            ENDIF
                
        BREAK
        
        CASE eSURVIVALLANDVEHICLESTATE_DRIVING
            
			// If all the peds in the vehicle have been killed, the vehicle is now considered dead.
			IF bPedInjured[0]
			AND bPedInjured[1]
			AND bPedInjured[2]
			AND bPedInjured[3]
				serverBD.iKills += sSurvivalLandVehicle.iNumPeds
				serverBD.iKillsThisWave += sSurvivalLandVehicle.iNumPeds
                serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] += sSurvivalLandVehicle.iNumPeds
                sSurvivalLandVehicle.eState = eSURVIVALLANDVEHICLESTATE_DEAD
			ENDIF
			   
        BREAK
        
		CASE eSURVIVALLANDVEHICLESTATE_ARRIVED
			
			// Keep this here for if we need to do anything special for the arrived state.
			
		BREAK
		
        CASE eSURVIVALLANDVEHICLESTATE_DEAD
            
            IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalLandVehicle.sPed[0].netId)
                bResetState = FALSE
            ENDIF
            
            IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalLandVehicle.sPed[1].netId)
                bResetState = FALSE
            ENDIF
			
            IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalLandVehicle.sPed[2].netId)
                bResetState = FALSE
            ENDIF
            
            IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalLandVehicle.sPed[3].netId)
                bResetState = FALSE
            ENDIF
			
            IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalLandVehicle.netId)
                bResetState = FALSE
            ENDIF
            
            IF bResetState
                
				eState									= sSurvivalLandVehicle.eState
                sSurvivalLandVehicle                  	= sTemp
				serverBd.iSearchingForLandVehicle     	= (-1)
				sSurvivalLandVehicle.eState				= eState
				
                IF serverBD.sWaveData.eStage = eWAVESTAGE_EASY
                    sSurvivalLandVehicle.eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
                ENDIF
                
            ENDIF
            
        BREAK
        
    ENDSWITCH
    
	
ENDPROC

PROC MAINTAIN_SURVIVAL_HELIS_SERVER()
    
    INT i
    
    MAINTAIN_ACTIVATING_HELIS()
    
    REPEAT MAX_NUM_SURVIVAL_HELIS i
        PROCESS_HELI_BRAIN(serverBD.sSurvivalHeli[i], i)
    ENDREPEAT
    
ENDPROC

PROC MAINTAIN_SURVIVAL_LAND_VEHICLES_SERVER()
	
	INT i
	
	MAINTAIN_ACTIVATING_LAND_VEHICLES()
	
	REPEAT MAX_NUM_LAND_VEHICLES i
        PROCESS_LAND_VEHICILE_BRAIN(serverBD.sSurvivalLandVehicle[i], i)
    ENDREPEAT
	
ENDPROC

PROC ADD_HELI_BLIP(NETWORK_INDEX netId, INT iHeli)
	
	IF NOT DOES_BLIP_EXIST(biHeliBlip[iHeli])
        biHeliBlip[iHeli] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(netId))
		SET_BLIP_SPRITE(biHeliBlip[iHeli], RADAR_TRACE_ENEMY_HELI_SPIN)
    ENDIF
					
ENDPROC

PROC REMOVE_HELI_BLIP(INT iHeli)
    
    IF DOES_BLIP_EXIST(biHeliBlip[iHeli])
        REMOVE_BLIP(biHeliBlip[iHeli])
    ENDIF
    
ENDPROC

PROC HANDLE_HELI_SEARCHLIGHTS(NETWORK_INDEX netId)
	
	// Sort searchlights.
	IF DOES_TOD_NEED_HELI_SEARCHLIGHTS()
		IF NOT IS_VEHICLE_SEARCHLIGHT_ON(NET_TO_VEH(netId))
			SET_VEHICLE_SEARCHLIGHT(NET_TO_VEH(netId), TRUE)
		ENDIF
	ELSE
		IF IS_VEHICLE_SEARCHLIGHT_ON(NET_TO_VEH(netId))
			SET_VEHICLE_SEARCHLIGHT(NET_TO_VEH(netId), FALSE)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_HELI_BODY(STRUCT_SURVIVAL_HELI_DATA &sSurvivalHeli, INT iHeli)
    
    PED_INDEX pedId
	VEHICLE_INDEX viHeli
	VEHICLE_MISSION eVehicleMission
	VECTOR vHeliGoto
	
    SWITCH sSurvivalHeli.eState
            
            CASE eSURVIVALHELISTATE_NOT_EXIST
                    
                REMOVE_HELI_BLIP(iHeli)
                    
            BREAK
            
            CASE eSURVIVALHELISTATE_SPAWNING
                    
                REMOVE_HELI_BLIP(iHeli)
                    
            BREAK
            
			CASE eSURVIVALHELISTATE_FLYING_TO_SURVIVAL
				
				// If heli is still flying.
                IF IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
					
					// Get Ids.
					viHeli = NET_TO_VEH(sSurvivalHeli.netId)
					eVehicleMission = GET_ACTIVE_VEHICLE_MISSION_TYPE(viHeli)
					
					// Handle searchlights and collision.
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.netId)
						
						HANDLE_HELI_SEARCHLIGHTS(sSurvivalHeli.netId)
						
						// If the ped gets stuck waiting on collision, load it around them so they can get moving again.
						IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(NET_TO_VEH(sSurvivalHeli.netId))
							SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_VEH(sSurvivalHeli.netId), TRUE)
						ENDIF
						
						// Blip the heli.
						ADD_HELI_BLIP(sSurvivalHeli.netId, iHeli)
						
					ENDIF
					
					// Tell the heli to fly towards the survival.
					IF NOT IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
						
						// Get data.
						pedId = NET_TO_PED(sSurvivalHeli.sPed[0].netId)
						vHeliGoto = g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos + << 0.0, 0.0, 20.0 >>
						
						// Get flight height.
						IF fHeightMapHeight = -1.0
							fHeightMapHeight = GET_APPROX_HEIGHT_FOR_POINT(vHeliGoto.x, vHeliGoto.y)
						ENDIF
						
						// If not flying towards the survival, do it.
						IF (eVehicleMission != MISSION_GOTO)
							TASK_HELI_MISSION(pedId, viHeli, NULL, NULL,  vHeliGoto, MISSION_GOTO, 20.0, 40.0, -1.0, CEIL(fHeightMapHeight), 10)
							PRINTLN("[Survival] - [AI Spawning] - given heli ", iHeli, " task MISSION_GOTO with coords ", vHeliGoto, " and flight height ", fHeightMapHeight)
						ENDIF
						
					ENDIF
				
				ENDIF
				
			BREAK
			
            CASE eSURVIVALHELISTATE_FLYING_FIGHTING
                
				// If heli is stil flying.
                IF IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
                   	
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.netId)
						
						HANDLE_HELI_SEARCHLIGHTS(sSurvivalHeli.netId)
						
						// If the ped gets stuck waiting opn collision, load it around them so they can get moving again.
						IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(NET_TO_VEH(sSurvivalHeli.netId))
							SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_VEH(sSurvivalHeli.netId), TRUE)
						ENDIF
						
					ENDIF
					
					// Blip the heli.
                    ADD_HELI_BLIP(sSurvivalHeli.netId, iHeli)
                            
                    IF NOT IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.sPed[0].netId)
                            pedId = NET_TO_PED(sSurvivalHeli.sPed[0].netId)
                            IF NOT IS_PED_IN_COMBAT(pedId)	
								IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK
                              		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
                                	TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.0)
								ENDIF
                            ENDIF
							// If the ped gets stuck waiting opn collision, load it around them so they can get moving again.
							IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(pedId)
								SET_ENTITY_LOAD_COLLISION_FLAG(pedId, TRUE)
							ENDIF
                        ENDIF
                    ENDIF
                    
                    IF NOT IS_NET_PED_INJURED(sSurvivalHeli.sPed[1].netId)
                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.sPed[1].netId)
                            pedId = NET_TO_PED(sSurvivalHeli.sPed[1].netId)
                            IF NOT IS_PED_IN_COMBAT(pedId)
								IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK
                                	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
                                	TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.0)
								ENDIF
                            ENDIF
							// If the ped gets stuck waiting opn collision, load it around them so they can get moving again.
							IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(pedId)
								SET_ENTITY_LOAD_COLLISION_FLAG(pedId, TRUE)
							ENDIF
                        ENDIF
                    ENDIF
                    
                    IF NOT IS_NET_PED_INJURED(sSurvivalHeli.sPed[2].netId)
                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.sPed[2].netId)
                            pedId = NET_TO_PED(sSurvivalHeli.sPed[2].netId)
                            IF NOT IS_PED_IN_COMBAT(pedId)
								IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK
                                	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
                                	TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.0)
								ENDIF
                            ENDIF
							// If the ped gets stuck waiting opn collision, load it around them so they can get moving again.
							IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(pedId)
								SET_ENTITY_LOAD_COLLISION_FLAG(pedId, TRUE)
							ENDIF
                        ENDIF
                    ENDIF
                        
                ENDIF
                
				// If heli or pilot are dead, kill crew.
				IF NOT IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
				OR IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
				
					IF NOT IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.sPed[0].netId)
                            pedId = NET_TO_PED(sSurvivalHeli.sPed[0].netId)
                            SET_ENTITY_HEALTH(pedId, 0)
                        ENDIF
                    ENDIF
                    
                    IF NOT IS_NET_PED_INJURED(sSurvivalHeli.sPed[1].netId)
                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.sPed[1].netId)
                        	pedId = NET_TO_PED(sSurvivalHeli.sPed[1].netId)
                           	SET_ENTITY_HEALTH(pedId, 0)
                        ENDIF
                    ENDIF
                    
                    IF NOT IS_NET_PED_INJURED(sSurvivalHeli.sPed[2].netId)
                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.sPed[2].netId)
                            pedId = NET_TO_PED(sSurvivalHeli.sPed[2].netId)
                            SET_ENTITY_HEALTH(pedId, 0)
                        ENDIF
                    ENDIF
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.netId)
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.netId)
							IF NOT IS_ENTITY_DEAD(NET_TO_VEH(sSurvivalHeli.netId))
//								SET_VEHICLE_OUT_OF_CONTROL(NET_TO_VEH(sSurvivalHeli.netId))
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
            BREAK
            
			CASE eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.netId)
					IF IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
						IF HAS_NET_TIMER_STARTED(sSurvivalHeli.stBlownUpTimer)
							IF HAS_NET_TIMER_EXPIRED(sSurvivalHeli.stBlownUpTimer, 10000)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.netId)
									NETWORK_EXPLODE_HELI(NET_TO_VEH(sSurvivalHeli.netId), TRUE, FALSE, sSurvivalHeli.netId)
									PRINTLN("[Survival] - alive timer expired, calling EXPLODE_VEHICLE to make sure it's dead.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
            CASE eSURVIVALHELISTATE_DEAD
                
                REMOVE_HELI_BLIP(iHeli)
                    
            BREAK
            
    ENDSWITCH
    
ENDPROC

FUNC BOOL IS_PED_LEAVING_VEHICLE(VEHICLE_INDEX vehId, PED_INDEX ped)
	
	IF GET_VEHICLE_PED_IS_USING(ped) = vehId
		IF NOT IS_PED_SITTING_IN_VEHICLE(ped, vehId)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_LAND_VEHICLE_BODY(STRUCT_SURVIVAL_LAND_VEHICLE_DATA &sSurvivalLandVehicle, INT iPed)
    
    PED_INDEX pedId
    VEHICLE_INDEX vehId
	BOOL bVehDriveable, bLeavingVehicle, bNeedToLeaveVehicle
	
    SWITCH sSurvivalLandVehicle.eState
            
            CASE eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
                    
            BREAK
            
            CASE eSURVIVALLANDVEHICLESTATE_SPAWNING
                  
            BREAK
            
            CASE eSURVIVALLANDVEHICLESTATE_DRIVING
                
                IF NOT IS_NET_PED_INJURED(sSurvivalLandVehicle.sPed[iPed].netId)
					
					// Setup various flags about ped and vehicle before doing rest of body processing.
					pedId = NET_TO_PED(sSurvivalLandVehicle.sPed[iPed].netId)
					
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sSurvivalLandVehicle.netId)
						
						vehId = NET_TO_VEH(sSurvivalLandVehicle.netId)
						
						bVehDriveable = IS_VEHICLE_DRIVEABLE(vehId)
						
						IF IS_PED_LEAVING_VEHICLE(vehId, pedId)
							bLeavingVehicle = TRUE
						ENDIF
						
						IF bVehDriveable
							IF IS_PED_IN_VEHICLE(pedId, vehId)
								IF IS_ENTITY_ON_FIRE(vehId)
									bNeedToLeaveVehicle = TRUE
								ENDIF
							ENDIF
						ELSE
							bNeedToLeaveVehicle = TRUE
						ENDIF
						
					ENDIF
					
					// Process ped if I have control of him.
                    IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalLandVehicle.sPed[iPed].netId)
						
						// If on foot and with no weapon in hand, put a weapon in hand so they don't flee.
						IF GET_PEDS_CURRENT_WEAPON(pedId) = WEAPONTYPE_UNARMED
							SETUP_PED_FOR_WAVE(0,0,pedId,TRUE,TRUE)
							PRINTLN("[Survival] - land ped has become unarmed, giving weapon to ped. Land Ped = ", iPed)
						ENDIF
					
						// If not in the process of leaving the vehicle, task him to combat.
                        IF NOT IS_PED_IN_COMBAT(pedId)
							IF NOT bLeavingVehicle
								IF bNeedToLeaveVehicle
									IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
										TASK_LEAVE_ANY_VEHICLE(pedId, 0, ECF_DONT_CLOSE_DOOR | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_RESUME_IF_INTERRUPTED)
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK
		                            	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
		                            	TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.0)
									ENDIF
								ENDIF
							ENDIF
                        ENDIF
                    	
						// If the ped gets stuck waiting on collision, load it around them so they can get moving again.
						// Don't have to worry about if in a vehicle because they have special ai for navigating at long range.
						IF NOT IS_PED_IN_ANY_VEHICLE(pedId)
							IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(pedId)
								SET_ENTITY_LOAD_COLLISION_FLAG(pedId, TRUE)
							ENDIF
						ENDIF
						
					ENDIF
					
                ENDIF
				
				IF bVehDriveable
				
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalLandVehicle.netId)
						
						IF IS_VEHICLE_STUCK_TIMER_UP(vehId, VEH_STUCK_ON_ROOF, ROOF_TIME)
						OR IS_VEHICLE_STUCK_TIMER_UP(vehId, VEH_STUCK_ON_SIDE, SIDE_TIME)
						OR IS_VEHICLE_STUCK_TIMER_UP(vehId, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
						OR IS_VEHICLE_STUCK_TIMER_UP(vehId, VEH_STUCK_JAMMED, JAMMED_TIME)				   
					    	SET_VEHICLE_ENGINE_HEALTH(vehId, -1000)
							SET_VEHICLE_PETROL_TANK_HEALTH(vehId, -1000)
						ENDIF
						
					ENDIF
					
                ENDIF
                
            BREAK
            
			CASE eSURVIVALLANDVEHICLESTATE_ARRIVED
				
			BREAK
			
            CASE eSURVIVALLANDVEHICLESTATE_DEAD
                     
            BREAK
            
    ENDSWITCH
    
ENDPROC

FUNC INT GET_ACCURACY()

	INT iEndHits =  GET_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
	INT iEndShots = GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOTS)
	
	IF (iEndShots - iStartShots) > 1
		FLOAT frecentaccuracy = TO_FLOAT((iEndHits - iStartHits) / (iEndShots - iStartShots))
		RETURN ROUND(frecentaccuracy)
	ENDIF
	
	RETURN 0
	
ENDFUNC

FUNC INT CALCULATE_SURVIVAL_SCORE()
	
	INT iScore
	INt iTimeBonus
	
	// See B*1530484 for formula.
	
	// Kills bonus.
	iScore += (playerBD[PARTICIPANT_ID_TO_INT()].iMyKills*100)
	PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding kills bonus, score now equals: ", iScore)
	
	// Time bonus.
	iTimeBonus = ((GET_WAVE_SURVIVED_UNTIL()*2500) - ((GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stSurvivalTime)/1000)*GET_WAVE_SURVIVED_UNTIL()))
	IF iTimeBonus > 0
		iScore += iTimeBonus
		PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding time bonus, score now equals: ", iScore)
	ELSE
		PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - not adding time bonus, negative value: ", iTimeBonus)
	ENDIF
	
	// Headshots bonus.
	iScore += playerBD[PARTICIPANT_ID_TO_INT()].iMyHeadshots*200
	PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding headshots bonus, score now equals: ", iScore)
	
	// Accuracy bonus.
	iScore += GET_ACCURACY()*5000
	PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding accuracy bonus, score now equals: ", iScore)
	
	// Vehicle kills bonus.
	iScore += playerBD[PARTICIPANT_ID_TO_INT()].iMyVehicleKills*400
	PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding vehicle kills bonus, score now equals: ", iScore)
	
	// No deaths bonus.
	IF playerBD[PARTICIPANT_ID_TO_INT()].iDeaths = 0
		iScore += 5000
		PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding no deaths bonus, score now equals: ", iScore)
	ENDIF
	
	RETURN iScore
	
ENDFUNC

// === Start Conor McG Edit === \\
PROC WRITE_TO_SURVIVAL_SC_LB()

    // Don't write if spectating
    IF IS_PLAYER_SCTV(PLAYER_ID())

        PRINTLN("[Survival] - WRITE_TO_SURVIVAL_SC_LB bypassed because IS_PLAYER_SCTV")
        
        EXIT
    ENDIF
    
	PRINTLN("[Survival] - WRITE_TO_SURVIVAL_SC_LB ") 
    IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.tl31LoadedContentID,"")
		TEXT_LABEL_31 categoryNames[1] 
   		TEXT_LABEL_23 uniqueIdentifiers[1]
   		categoryNames[0] = "Mission" 
  		uniqueIdentifiers[0] = g_FMMC_STRUCT.tl31LoadedContentID
		
		// *SCORE_COLUMN( AGG_SUM ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
    	//WAVES_SURVIVED( AGG_SUM ) : COLUMN_ID_LB_NUM_WINS - LBCOLUMNTYPE_INT64
    	//KILLS( AGG_SUM ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
    	//DEATHS( AGG_SUM ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
    	//KILL_DEATH_RATIO( AGG_LAST ) : COLUMN_ID_LB_KILL_DEATH_RATIO - LBCOLUMNTYPE_DOUBLE
		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_SURVIVAL,uniqueIdentifiers,categoryNames,1,-1,TRUE)
           	
	        // Write score
	        WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(	ENUM_TO_INT(LEADERBOARD_FREEMODE_SURVIVAL),LB_INPUT_COL_SCORE,playerBD[PARTICIPANT_ID_TO_INT()].iSurvivalScore,0)
	        
			// Write waves
	       	WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(	ENUM_TO_INT(LEADERBOARD_FREEMODE_SURVIVAL),LB_INPUT_COL_NUM_WINS,serverBD.sWaveData.iWaveCount,0)
	                
	        // Write kills
	       	WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(	ENUM_TO_INT(LEADERBOARD_FREEMODE_SURVIVAL),LB_INPUT_COL_KILLS,playerBD[PARTICIPANT_ID_TO_INT()].iMyKills,0)
	                        
	        // Write deaths                 
	        WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(	ENUM_TO_INT(LEADERBOARD_FREEMODE_SURVIVAL),LB_INPUT_COL_DEATHS,playerBD[PARTICIPANT_ID_TO_INT()].iDeaths,0)
			
			//1061136 UN_COMMENT WHEN 1098154           
			LEADERBOARDS_WRITE_ADD_COLUMN_LONG(						LB_INPUT_COL_MATCH_ID,serverbd.iMatchHistoryId[1],serverbd.iMatchHistoryId[0])
			PRINTLN("[Survival] - LEADERBOARDS_WRITE_ADD_COLUMN_LONG writing to column 8 hashedMac: ",serverbd.iMatchHistoryId[1]," matchHistoryID: " , serverbd.iMatchHistoryId[0])
	       
	        PRINTLN("[Survival] - -<>- WRITE_TO_SURVIVAL_SC_LB: Score: ", playerBD[PARTICIPANT_ID_TO_INT()].iSurvivalScore) 
	        PRINTLN("[Survival] - -<>- WRITE_TO_SURVIVAL_SC_LB: Waves: ", serverBD.sWaveData.iWaveCount) 
	        PRINTLN("[Survival] - -<>- WRITE_TO_SURVIVAL_SC_LB: Kills: ", TO_FLOAT(playerBD[PARTICIPANT_ID_TO_INT()].iMyKills)) 
	        PRINTLN("[Survival] - -<>- WRITE_TO_SURVIVAL_SC_LB: Deaths: ", playerBD[PARTICIPANT_ID_TO_INT()].iDeaths) 
		ENDIF
      //  mpGlobals.g_bFlushLBWrites = TRUE
    ENDIF
    
ENDPROC 
// === End Conor McG Edit === \\

FUNC BOOL GET_AM_I_IN_FIGHTING_PARTICIPANTS_LIST_AS_POTENTIAL_FIGHTER()
    
    INT i
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        IF PARTICIPANT_ID_TO_INT() = serverBD.sFightingParticipantsList.iParticipant[i]
			IF IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
            	RETURN TRUE
			ENDIF
        ENDIF
    ENDREPEAT
    
    RETURN FALSE
    
ENDFUNC

FUNC BOOL AM_I_STRAIGHT_TO_SPECTATOR_PLAYER_THAT_SHOULD_JOIN_FIGHT()
	
	IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
		IF GET_AM_I_IN_FIGHTING_PARTICIPANTS_LIST_AS_POTENTIAL_FIGHTER()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL AM_I_ABANDONED_HORDE_ON_PLAYLIST_PLAYER_THAT_SHOULD_JOIN_FIGHT()
	
	IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC LEAVE_SPECTATOR_STAGE_AND_JOIN_FIGHT(INT iDebugCall, BOOL bKeepViewingTarget)

    PRINTLN("[Survival] - LEAVE_SPECTATOR_STAGE_AND_JOIN_FIGHT - being called - iDebugCall =", iDebugCall)
	
	INT iFlags = DSCF_NONE
	
    IF iDebugCall = iDebugCall
        iDebugCall = iDebugCall
    ENDIF
    
	IF bKeepViewingTarget
		//iFlags = DSCF_REMAIN_VIEWING_TARGET
		PRINTLN("[Survival] - LEAVE_SPECTATOR_STAGE_AND_JOIN_FIGHT - iFlags =", DSCF_REMAIN_VIEWING_TARGET)
	ENDIF
	
    IF IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
        DEACTIVATE_SPECTATOR_CAM(specData, iFlags)
    ENDIF
	
	CLEAR_I_JOIN_MISSION_AS_SPECTATOR()
	
	CLEAR_STRAIGHT_TO_SPECTATOR_PLAYER()
	
	RESET_RESPAWNING_DATA_CLIENT(0, AFTERLIFE_SET_SPECTATORCAM)
	SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(FALSE)
    bGotWaveProgressLabel = FALSE
	
	DISPLAY_RADAR(TRUE)
	
	HIDE_PLAYER(FALSE)
	bDoUnhide = TRUE
	PRINTLN("[Survival] - bDoUnhide = TRUE")
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		SET_PLAYER_TEAM(PLAYER_ID(), 0)
	ENDIF
	
	ANIMPOSTFX_STOP_ALL()
	PRINTLN("[Survival] - cleared post fx - ANIMPOSTFX_STOP_ALL()")
	
	NETWORK_OVERRIDE_TEAM_RESTRICTIONS(0, TRUE)
	
	CLEAR_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()
	PRINTLN("[Survival] - calling CLEAR_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP() to fix bug url:bugstar:2371909")
	
    SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
    
ENDPROC

FUNC BOOL DID_I_GET_MOST_SURVIVAL_KILLS()
	
	INT i
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF i != PARTICIPANT_ID_TO_INT()
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				IF playerBD[i].iMyKills >= playerBD[PARTICIPANT_ID_TO_INT()].iMyKills
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_EOM_RESTART_VOTE_LOGIC()
	
	INT iparticipant
	
	IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
	OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
		
		FMMC_EOM_SERVER_RESET_PLAYER_COUNTS(sPlayerCounts)
	
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
			FMMC_EOM_SERVER_PLAYER_PROCESSING(sPlayerCounts, iparticipant)
		ENDREPEAT
	
		FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING(sPlayerCounts, serverBD.sServerFMMC_EOM, FMMC_TYPE_SURVIVAL, 0)
		IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, serverBD.sServerFMMC_EOM.iVoteStatus)
			SERVER_JOB_VOTE_PROCESSING(g_sMC_serverBDEndJob)
		ENDIF
	ENDIF
ENDPROC

PROC DO_MATCH_END()
	
	// Save end playstats if I did start playstats.
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		IF NOT bDidMatchEnd
			IF bSavedMatchstartPlayStatsData
				DEAL_WITH_FM_MATCH_END(	FMMC_TYPE_SURVIVAL, 
										serverBD.iMatchHistoryId[0], 
										0, 
										GET_FMMC_END_OF_MISSION_STATUS_FOR_END_HORDE_REASON(), 
										GET_MY_LEADERBOARD_POSITION_FOR_PLAYSTATS(), 
										playerBD[PARTICIPANT_ID_TO_INT()].iMyKills, 
										playerBD[PARTICIPANT_ID_TO_INT()].iDeaths, 
										0, 
										0, 
										GET_PLAYER_TEAM(PLAYER_ID()),
										playerBD[PARTICIPANT_ID_TO_INT()].iMyHeadshots,
										DUMMY_MODEL_FOR_SCRIPT,
										GET_WAVE_SURVIVED_UNTIL() )
				bDidMatchEnd = TRUE
				PRINTLN("[Survival] - DO_MATCH_END() - called DEAL_WITH_FM_MATCH_END.")
			ENDIF
		ENDIF
		
	ELSE
		
		bDidMatchEnd = TRUE
		PRINTLN("[Survival] - DO_MATCH_END() - called DEAL_WITH_FM_MATCH_END.")
		
	ENDIF
	
ENDPROC



// *********************************** \\
// *********************************** \\
//                                     \\
//				CLIENT LOGIC           \\
//                                     \\
// *********************************** \\
// *********************************** \\


PROC PROCESS_eHORDESTAGE_JIP_IN_CLIENT()
 
	BOOL bTimeout
	
	IF NOT HAS_NET_TIMER_STARTED(stJipTimer)
		START_NET_TIMER(stJipTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(stJipTimer, 20000)
			bTimeout = TRUE
			NET_SCRIPT_ASSERT("Survival - JIP in stage timed out - are there any pickups or props missing?")
		ENDIF
	ENDIF
	
    // Create props.
    IF CREATE_FMMC_PROPS_MP(oiHordeProp, oihordePropChildren)
    OR bTimeout
        // Create guns, armour and health.
        IF CREATE_PICKUPS()
    	OR bTimeout
            // If the server has moved on.
            IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_JIP_IN
                SET_PROP_NODELS_AS_NO_LONGER_NEEDED()
                SET_CLIENT_HORDE_STAGE(eHORDESTAGE_SETUP_MISSION)
				IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
					HIDE_PLAYER(FALSE)
                	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
            ENDIF
    
        ENDIF
        
    ENDIF
   	
ENDPROC

PROC PROCESS_eHORDESTAGE_SETUP_MISSION_CLIENT()
    
    // Move on to next stage of server does.
    IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_SETUP_MISSION
       	SET_CLIENT_HORDE_STAGE(eHORDESTAGE_START_FADE_IN)
    ENDIF
    
ENDPROC

/// PURPOSE:
///    Helper function to get a clients game state
FUNC INT GET_HC_CLIENT_GAME_STATE(INT iPart)

	RETURN playerBD[iPart].iGameState
	
ENDFUNC

FUNC BOOL HAS_EVERYONE_REACHED_TEAMCUTSCENE()

	INT iparticipant
	PARTICIPANT_INDEX tempPart

	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			IF GET_HC_CLIENT_GAME_STATE(iparticipant) < SURVIVAL_GAME_FADE_IN
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE

ENDFUNC

PROC PROCESS_eHORDESTAGE_START_FADE_IN_CLIENT()
    
	//===Start Edit David G===
    IF IS_PLAYER_SCTV(PLAYER_ID())

		PRINTLN("[Survival] - I am an sctv player.")
		
        // Give control back to player.
        IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
            IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
                SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
            ENDIF
        ENDIF
		CLEANUP_ALL_CORONA_FX(TRUE) 
		SET_SKYFREEZE_CLEAR()
		IF ANIMPOSTFX_IS_RUNNING("MP_job_load")
			ANIMPOSTFX_STOP("MP_job_load")
		ENDIF
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
        SET_CLIENT_HORDE_STAGE(eHORDESTAGE_SCTV)
       	
    ELSE            
    //===End Edit David G===
	
	    // Do start fade in.
	    IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_COMPLETED_START_FADE_IN)
		
	        // Give control back to player.
	        IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	            IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	                SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	            ENDIF
	        ENDIF
			
	        // If I am not ready to fade in yet.
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_READY_FOR_START_FADE_IN)
				
				// Say I am ready for start fade in.
				IF DID_I_JOIN_MISSION_AS_SPECTATOR()
					bSetupInitialSpawnPos = TRUE
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_READY_FOR_START_FADE_IN)
				ELSE
					IF NOT bSetupInitialSpawnPos
						SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(FALSE)
						bSetupInitialSpawnPos = TRUE
					ELSE
				        IF WARP_TO_START_POSITION(iWarpToStartPosStage)
							PRINTLN("[Survival] - warped to start position, going to, ready to begin fade in.")
				            SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_READY_FOR_START_FADE_IN)
				        ENDIF
					ENDIF
				ENDIF
				
			// If I am ready to fade in.
			ELSE
			
				// Do fade in once everyone is ready for it.
		        IF NOT bStartFadeInTriggered
		            IF ARE_ALL_PARTICIPANTS_READY_TO_DO_START_FADE_IN()
						PRINTLN("[Survival] - all participants are ready to fade in.")
		                IF IS_BIT_SET(serverBd.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
							PRINTLN("[Survival] - server says new players should go straight to the spectator camera.")
		                    SETUP_STRAIGHT_TO_SPECTATOR_PLAYER()
		                    MAINTAIN_SPECTATOR_HORDE_CAM()
		                    SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_COMPLETED_START_FADE_IN)
		                ENDIF
		                bStartFadeInTriggered = TRUE
		            ENDIF
		        ELSE
			       	IF DID_I_JOIN_MISSION_AS_SPECTATOR()
					OR FADE_SCREEN_IN()
						PRINTLN("[Survival] - completed screen fade in.")
			          	SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_COMPLETED_START_FADE_IN)
			       	ENDIF
		        ENDIF
			
	        ENDIF
			
	    ELSE

			PRINTLN("[Survival] - I am not an sctv player.")
			
            // If server has moved on to a different stage, we should do that too.
            IF (IS_BIT_SET(serverBd.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM) AND serverBD.sWaveData.iWaveCount > 1) 
			OR DID_I_JOIN_MISSION_AS_SPECTATOR()
			
				PRINTLN("[Survival] - server says new players go straght to spectator camera*.")
				
                SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
                SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
				
                IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_START_FADE_IN
            		
					PRINTLN("[Survival] - server is no longer at horde stage eHORDESTAGE_START_FADE_IN.")
					
                    SETUP_STRAIGHT_TO_SPECTATOR_PLAYER()
                    
                    // Give control back to player.
                    IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
                        IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
                            SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
                        ENDIF
                    ENDIF
                    
					iRejoinFromSpecCamStage = 0
					PRINTLN("[Survival] - iRejoinFromSpecCamStage = 0, call 1.")
                    SET_CLIENT_HORDE_STAGE(eHORDESTAGE_SPECTATING)
                    
                ENDIF
                    
            ELSE
        		
				PRINTLN("[Survival] - server says new players go to fighting stage.")
				
                IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_START_FADE_IN
					PRINTLN("[Survival] - server is no longer at horde stage eHORDESTAGE_START_FADE_IN.")
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
                    SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
                    SET_CLIENT_HORDE_STAGE(eHORDESTAGE_FIGHTING)
                ENDIF
                    
            ENDIF
                
        ENDIF
        
    ENDIF
    
ENDPROC

FUNC FLOAT GET_WAVE_PERCENTAGE_COMPLETE()

	RETURN (TO_FLOAT(serverBD.iKillsThisWave)/TO_FLOAT(serverBD.sWaveData.iNumRequiredKills))*100.0
	
ENDFUNC

PROC RESET_FIGHTING_STAGE_MUSIC_EVENTS()
	
	INT i
	
	REPEAT NUM_MISSION_MUSIC_EVENTS i
		IF (INT_TO_ENUM(ENUM_MUSIC_EVENT, i) != eMUSICEVENT_WAVE_COMPLETE)
			structMusicEvent[i].bTriggered = FALSE
		ENDIF
	ENDREPEAT
	
	PRINTLN("[Survival] - [Music] - reset data for fighting stage music events..")
	
ENDPROC

PROC PROCESS_eHORDESTAGE_FIGHTING_CLIENT()
    
    INT i, j
    FLOAT fWavePercentageComplete
	STRING sMusicEvent
	
    TEXT_LABEL_15 tl15_obj = "HRD_OBJ"
    BOOL bPlayerAtShootout = IS_PLAYER_AT_HORDE_SHOOTOUT(PLAYER_ID())
    
    // J-skips.
    #IF IS_DEBUG_BUILD
    MAINTAIN_CLIENT_J_SKIPS()
    #ENDIF
    
    // Process ped actions.
    PROCESS_SQUAD_ENEMY_PED_BODIES()
    
	// Bugstar 2218090.
	SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(TRUE)
	
    // Kill streaks.
//    MAINTAIN_KILL_STREAKS() // Uncomment to add killstreaks in again.
    
    // Helicopters.
    REPEAT MAX_NUM_SURVIVAL_HELIS i
      	PROCESS_HELI_BODY(serverBD.sSurvivalHeli[i], i)
    ENDREPEAT
    
	// Bikes.
	REPEAT MAX_NUM_LAND_VEHICLES i
		REPEAT NUM_LAND_VEH_PEDS j
	     	UPDATE_ENEMY_NET_PED_BLIP(	serverBD.sSurvivalLandVehicle[i].sPed[j].netId, 
										structLandVehicleEnemyBlip[i][j],
										DEFAULT,
										FALSE, 
										FORCE_ENEMY_BLIPS_ON() )
			PROCESS_LAND_VEHICLE_BODY(serverBD.sSurvivalLandVehicle[i], j)
		ENDREPEAT
	ENDREPEAT
	
	// Make sure this is false if we are fighting so spec cam works on next death.
	bTurnedOffSpecCam = FALSE
	
	// Start playstats since I am a competitive player.
	IF NOT bSavedMatchstartPlayStatsData
		DEAL_WITH_FM_MATCH_START(FMMC_TYPE_SURVIVAL, serverBD.iMatchHistoryId[0], serverBD.iMatchHistoryId[1])
		bSavedMatchstartPlayStatsData = TRUE
		IF NOT HAS_NET_TIMER_STARTED(stSurvivalTime)
			START_NET_TIMER(stSurvivalTime)
			PRINTLN("[Survival] - started stSurvivalTime.")
		ENDIF
		PRINTLN("[Survival] - called DEAL_WITH_FM_MATCH_START(FMMC_TYPE_SURVIVAL)")
	ENDIF
	
	// Display help explaining lives if never played survival before.
	SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DO_SURV_LIVES_HELP) 
	
	// Tell player to get back to the shoot out if not near it.
	MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(bPlayerAtShootout)
	
	// B*1581527.
	IF NOT bResetReminderTimer
		RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_SURVIVAL) 
		bResetReminderTimer = TRUE
	ENDIF
	
    IF bPlayerAtShootout
    #IF IS_DEBUG_BUILD
	OR bBlockDistanceCheck
	#ENDIF
	   	
        // Process pickup blips.
        PROCESS_PICKUP_BLIPS()
        
		// Background audio track.
		IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
			IF (GET_SERVER_HORDE_STAGE() = eHORDESTAGE_FIGHTING)
				
				fWavePercentageComplete = GET_WAVE_PERCENTAGE_COMPLETE()
				
				IF IS_SCORE_CORONA_OPTION_ON()
					IF fWavePercentageComplete < 40.0
						IF NOT structMusicEvent[ENUM_TO_INT(eMUSICEVENT_START)].bTriggered
							sMusicEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_START, serverBD.sWaveData.iWaveCount)
							IF PREPARE_MUSIC_EVENT(sMusicEvent)
								TRIGGER_MUSIC_EVENT(sMusicEvent)
								structMusicEvent[ENUM_TO_INT(eMUSICEVENT_START)].bTriggered = TRUE
								PRINTLN("[Survival] - [Music] - triggered ", sMusicEvent)
							ENDIF
						ENDIF
					ELIF fWavePercentageComplete < 70.0
						IF NOT structMusicEvent[ENUM_TO_INT(eMUSICEVENT_WAVE_40P)].bTriggered
							sMusicEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_WAVE_40P, serverBD.sWaveData.iWaveCount)
							IF PREPARE_MUSIC_EVENT(sMusicEvent)
								TRIGGER_MUSIC_EVENT(sMusicEvent)
								structMusicEvent[ENUM_TO_INT(eMUSICEVENT_WAVE_40P)].bTriggered = TRUE
								PRINTLN("[Survival] - [Music] - triggered ", sMusicEvent)
							ENDIF
						ENDIF
					ELSE
						IF NOT structMusicEvent[ENUM_TO_INT(eMUSICEVENT_WAVE_70P)].bTriggered
							sMusicEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_WAVE_70P, serverBD.sWaveData.iWaveCount)
							IF PREPARE_MUSIC_EVENT(sMusicEvent)
								TRIGGER_MUSIC_EVENT(sMusicEvent)
								structMusicEvent[ENUM_TO_INT(eMUSICEVENT_WAVE_70P)].bTriggered = TRUE
								PRINTLN("[Survival] - [Music] - triggered ", sMusicEvent)
							ENDIF
						ENDIF
					ENDIF
					IF NOT bMutedNearbyRadios
						START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
						bMutedNearbyRadios = TRUE
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		
        // Tell player to kill!
        IF (serverBD.sWaveData.iNumRequiredKills - serverBD.iKillsThisWave) <= 1
		AND (serverBD.iNumAliveSquadEnemies + serverBD.iNumAliveHelis + serverBD.iNumAliveLandVehiclePeds) <= 1
         	tl15_obj = "HRD_OBJSING"
        ENDIF
        
        Print_Objective_Text(tl15_obj)
        
        // Display wave progress.
        MAINTAIN_WAVE_PROGRESS_BAR(HUDORDER_BOTTOM)
        
    ELSE
        
        // Remove blips.
//      REMOVE_HORDE_BLIPS(FALSE)
		
    ENDIF
    
    // If server has moved on to a different stage, we should do that too.
    IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
        IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_FIGHTING
            Clear_Any_Objective_Text_From_This_Script()
            bGotWaveProgressLabel = FALSE
			ENUM_MUSIC_EVENT eMusicEvent
			IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
				IF serverBD.sWaveData.iWaveCount < 10
					eMusicEvent = eMUSICEVENT_WAVE_COMPLETE
					sMusicEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(eMusicEvent, serverBD.sWaveData.iWaveCount)
				ELSE
					eMusicEvent = eMUSICEVENT_STOP
					sMusicEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(eMusicEvent, serverBD.sWaveData.iWaveCount)
				ENDIF
				IF IS_SCORE_CORONA_OPTION_ON()
					IF NOT structMusicEvent[ENUM_TO_INT(eMusicEvent)].bTriggered
	//					IF PREPARE_MUSIC_EVENT(sMusicEvent)
							CANCEL_MUSIC_EVENT(GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_START, serverBD.sWaveData.iWaveCount))
							CANCEL_MUSIC_EVENT(GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_WAVE_40P, serverBD.sWaveData.iWaveCount))
							CANCEL_MUSIC_EVENT(GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_WAVE_70P, serverBD.sWaveData.iWaveCount))
							TRIGGER_MUSIC_EVENT(sMusicEvent)
							structMusicEvent[ENUM_TO_INT(eMusicEvent)].bTriggered = TRUE
							PRINTLN("[Survival] - [Music] - triggered ", sMusicEvent)
							RESET_FIGHTING_STAGE_MUSIC_EVENTS()
							IF bMutedNearbyRadios
								STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
								bMutedNearbyRadios = FALSE
							ENDIF
	            			SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
	//					ENDIF
					ENDIF
				ELSE
					SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
				ENDIF
			ELSE
				IF IS_SCORE_CORONA_OPTION_ON()
					RESET_FIGHTING_STAGE_MUSIC_EVENTS()
					IF bMutedNearbyRadios
						STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
						bMutedNearbyRadios = FALSE
					ENDIF
				ENDIF
            	SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
			ENDIF
        ENDIF
    ENDIF
    
ENDPROC

PROC UPDATE_HORDE_WAVES_SURVIVED_AWARD()
	
	IF NOT bIWentFromSpectatingToPassedWaveStage
	    IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	        IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF serverBD.sWaveData.iWaveCount > GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE)
					SWITCH serverBD.sWaveData.iWaveCount // Wave count increments as soon as the wave is done, so need to check for one higher than the wave that was just completed.
						CASE 4 
							IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE) < 3
								SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE, 3) 
								PRINTLN("[Survival] - [Awards] - given award MP_AWARD_FMHORDWAVESSURVIVE, 3.")
							ENDIF
						BREAK
						CASE 6 
							IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE) < 5
								SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE, 5) 
								PRINTLN("[Survival] - [Awards] - given award MP_AWARD_FMHORDWAVESSURVIVE, 5.")
							ENDIF
						BREAK
						CASE 8 
							IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE) < 7
								SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE, 7) 
								PRINTLN("[Survival] - [Awards] - given award MP_AWARD_FMHORDWAVESSURVIVE, 7.")
							ENDIF
						BREAK
						CASE 10 
							IF GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES
								IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE) < 10
									SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE, 10) 
									PRINTLN("[Survival] - [Awards] - given award MP_AWARD_FMHORDWAVESSURVIVE, 10.")
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
        ENDIF
    ENDIF
	
ENDPROC

PROC MAINTAIN_TIME_SPENT_IN_PAUSE_MENU()
	
	IF NOT bBeenInPauseMenuTooLongOnWavePassed
		
		IF IS_PAUSE_MENU_ACTIVE()
			
			TIME_DATATYPE CurrentTime = GET_NETWORK_TIME()
			
			IF bGotFirstTimeForHoldValue
				iTimeInPauseMenu += GET_TIME_DIFFERENCE(CurrentTime, iTimeForHold)
			ENDIF
			
			iTimeForHold = GET_NETWORK_TIME()
			
			IF bGotFirstTimeForHoldValue
				IF iTimeInPauseMenu >= GET_TIME_FOR_BIG_MESSAGE(BIG_MESSAGE_WAVE_COMPLETE)
					bBeenInPauseMenuTooLongOnWavePassed = TRUE
				ENDIF
			ENDIF
			
			bGotFirstTimeForHoldValue = TRUE
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_eHORDESTAGE_PASSED_WAVE_CLIENT()
    
	INT i, j
	
	// B*1089716
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	
    // J-skips.
    #IF IS_DEBUG_BUILD
    MAINTAIN_CLIENT_J_SKIPS()
    #ENDIF
    
    // Process ped actions.
    PROCESS_SQUAD_ENEMY_PED_BODIES()
    
	// Bugstar 2218090.
	SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(FALSE)
	
    // Helicopters.
    REPEAT MAX_NUM_SURVIVAL_HELIS i
      	PROCESS_HELI_BODY(serverBD.sSurvivalHeli[i], i)
    ENDREPEAT
    
	// No enemy blips during passed wave section.
	REMOVE_ON_FOOT_PED_BLIPS()
	REMOVE_VEHICLE_PED_BLIPS()
	
	// Bikes.
	REPEAT MAX_NUM_LAND_VEHICLES i
		REPEAT NUM_LAND_VEH_PEDS j
			PROCESS_LAND_VEHICLE_BODY(serverBD.sSurvivalLandVehicle[i], j)
		ENDREPEAT
	ENDREPEAT
	
    // Process pickup blips.
    PROCESS_PICKUP_BLIPS()
    
	// No heli blips.
    REPEAT MAX_NUM_SURVIVAL_HELIS i
		REMOVE_HELI_BLIP(i)
    ENDREPEAT
	
    // Kill streaks.
//    MAINTAIN_KILL_STREAKS() // Uncomment to add killstreaks in again.
    
    // Remove current pickups, will recreate them next wave with new pickup types.
    REMOVE_PICKUPS()
            
    // Display end of wave text and move on once server does.
    IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
    
//        // Fix for bug 900517 - hide radar during wave complete screen.
//        HIDE_HUD_AND_RADAR_THIS_FRAME()
        
        IF serverBD.sWaveData.iWaveCount < NUMBER_HORDE_WAVES
			
			IF NOT bTurnedOffSpecCam
				LEAVE_SPECTATOR_STAGE_AND_JOIN_FIGHT(2, FALSE)
				RESET_RESPAWNING_DATA_CLIENT(1, AFTERLIFE_JUST_REJOIN)
				bTurnedOffSpecCam = TRUE
			ENDIF
		
			MAINTAIN_TIME_SPENT_IN_PAUSE_MENU()
			
			// Make sure respawners have no control. 
			IF g_bCelebrationScreenIsActive
				IF (sCelebrationData.eCurrentStage < CELEBRATION_STAGE_END_TRANSITION)
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE) 
					ENDIF
				ENDIF
			ENDIF
			
            IF DISPLAY_END_OF_WAVE_TEXT(FALSE, TRUE)
			OR bBeenInPauseMenuTooLongOnWavePassed
				#IF IS_DEBUG_BUILD
					IF bBeenInPauseMenuTooLongOnWavePassed
						PRINTLN("[Survival] - bBeenInPauseMenuTooLongOnWavePassed = TRUE.")
					ENDIF
				#ENDIF
				TIME_DATATYPE temp
				iTimeForHold = temp
				iTimeInPauseMenu = 0
				bGotFirstTimeForHoldValue = FALSE
				bBeenInPauseMenuTooLongOnWavePassed = FALSE
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WAVE_COMPLETE)
				
				//1628806 - If bBeenInPauseMenuTooLongOnWavePassed kicks in then we have to run the celebration cleanup		
				IF DOES_CAM_EXIST(camCelebrationScreen)
					DESTROY_CAM(camCelebrationScreen, TRUE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					ANIMPOSTFX_STOP_ALL()
					ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
				ENDIF
				
				CLEAR_ALL_BIG_MESSAGES()
				STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
				CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
				SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
				
				IF NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Survival] - check 0, IS_PLAYER_RESPAWNING = TRUE, skipping setting control TRUE, respawning should handle it.")
				#ENDIF
				ENDIF
				
                SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
				
            ENDIF
			
        // On last wave skip straight to eom stage and use hud there.
        ELSE
            SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
        ENDIF
            
    ELSE
    	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	
        IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_PASSED_WAVE
            playerBD[PARTICIPANT_ID_TO_INT()].iMyKillsThisWave = 0
            CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
            
			structMusicEvent[ENUM_TO_INT(eMUSICEVENT_WAVE_COMPLETE)].bTriggered = FALSE
			structMusicEvent[ENUM_TO_INT(eMUSICEVENT_STOP)].bTriggered = FALSE
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					RESET_RESPAWNING_DATA_CLIENT(8)
				ENDIF
			ENDIF
			
            CLEAR_HUD_PLAYER_ARRAY(sEndBoardData)
            bGivenEndOfWaveXp = FALSE
            iEndOfWaveHudStage = 0
			bTurnedOffSpecCam = FALSE
			
			IF (GET_SERVER_HORDE_STAGE() = eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE)
            	TRIGGER_MUSIC_EVENT(GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_SILENT, serverBD.sWaveData.iWaveCount)) // We want silence during the countdown. B* 1590089. 
			ENDIF
			
			IF NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[Survival] - check 1, IS_PLAYER_RESPAWNING = TRUE, skipping setting control TRUE, respawning should handle it.")
			#ENDIF
			ENDIF
			
			RESET_FIGHTING_STAGE_MUSIC_EVENTS()
			
			IF GET_END_HORDE_REASON() != eENDHORDEREASON_ALL_PLAYERS_DEAD
				bIWentFromSpectatingToPassedWaveStage = FALSE
				PRINTLN("[Survival] - GET_END_HORDE_REASON() != eENDHORDEREASON_ALL_PLAYERS_DEAD, setting bIWentFromSpectatingToPassedWaveStage = FALSE.")
			ENDIF
			
			UPDATE_HORDE_WAVES_SURVIVED_AWARD()
			
			SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
            
        ENDIF
            
    ENDIF
    
ENDPROC

PROC PROCESS_eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE_CLIENT()
	
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR) //1623915 - Prevent the rank bar from appearing.
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
		STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")	
		PRINTLN("[Survival] - MP_CELEB_SCREEN_SCENE is active, calling STOP_AUDIO_SCENE(MP_CELEB_SCREEN_SCENE). Call E.")
	ENDIF
					
    // J-skips.
    #IF IS_DEBUG_BUILD
    MAINTAIN_CLIENT_J_SKIPS()
    #ENDIF
    
	//gdisablerankupmessage = FALSE
	SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	
    // Process pickup blips.
    PROCESS_PICKUP_BLIPS()
    
	// Bugstar 2218090.
	SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(TRUE)
	
    // Kill streaks.
//    MAINTAIN_KILL_STREAKS() // Uncomment to add killstreaks in again.
    
	// Show countdown until next wave.
    MAINTAIN_NEXT_WAVE_HUD(HUDORDER_BOTTOM)
	
    // New pickups!
    CREATE_PICKUPS_FOR_NEW_WAVE(iCreatePickupsStage)
    
	ANIMPOSTFX_STOP("MP_Celeb_Win")
	ANIMPOSTFX_STOP("MP_Celeb_Lose")
	
    // Broadcast if I'm ready for the next wave countdown.
    IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_AM_ALIVE_AND_FADED_IN)
        IF IS_NET_PLAYER_OK(PLAYER_ID())
            IF IS_SCREEN_FADED_IN()
                //IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING)
				IF NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
                    SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_AM_ALIVE_AND_FADED_IN)
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
	IF bDoUnhide
		HIDE_PLAYER(FALSE)
	ENDIF
	
    // Move on once server does.
    IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
	
        #IF IS_DEBUG_BUILD
        playerBD[PARTICIPANT_ID_TO_INT()].bSkipWave = FALSE
		bDoingWarpToHorde = FALSE
		bJSkipped = FALSE
        #ENDIF
		
		IF bDoUnhide
			bDoUnhide = FALSE
			PRINTLN("[Survival] - bDoUnhide = FALSE")
		ENDIF

        iCountDownAudioStage = 0
        iCreatePickupsStage = 0
        CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_AM_ALIVE_AND_FADED_IN)
		
		structMusicEvent[ENUM_TO_INT(eMUSICEVENT_WAVE_COMPLETE)].bTriggered = FALSE
		structMusicEvent[ENUM_TO_INT(eMUSICEVENT_SILENT)].bTriggered = FALSE
		
        SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
		
    ENDIF
    
ENDPROC

PROC PROCESS_eHORDESTAGE_SPECTATING_CLIENT()
    
	INT i, j
    BOOL bGoToRewardsAndFeedback
	
	
	
	// ****************************************************************
	// 	Process fighting stage logic that could migrate to spectators.
	// ****************************************************************
	
    // Process ped actions.
    PROCESS_SQUAD_ENEMY_PED_BODIES()
    
    // Helicopters.
    REPEAT MAX_NUM_SURVIVAL_HELIS i
      	PROCESS_HELI_BODY(serverBD.sSurvivalHeli[i], i)
    ENDREPEAT
    
	// Bikes.
	REPEAT MAX_NUM_LAND_VEHICLES i
		REPEAT NUM_LAND_VEH_PEDS j
			UPDATE_ENEMY_NET_PED_BLIP(	serverBD.sSurvivalLandVehicle[i].sPed[j].netId, 
										structLandVehicleEnemyBlip[i][j], 
										DEFAULT,
										FALSE, 
										FORCE_ENEMY_BLIPS_ON() )
			PROCESS_LAND_VEHICLE_BODY(serverBD.sSurvivalLandVehicle[i], j)
		ENDREPEAT
	ENDREPEAT
	
    // Process pickup blips.
    PROCESS_PICKUP_BLIPS()
    
	
	
	// ****************************
	// 	Process eom spec cam exit.
	// ****************************
	
	// If we go to the passed wave stage from here, set a flag so we know.
	bIWentFromSpectatingToPassedWaveStage = TRUE
	
    // If the mission has not ended yet.
    IF (GET_SERVER_HORDE_STAGE() != eHORDESTAGE_REWARDS_AND_FEEDACK)
		
		//New flag for "do you want to quit" screen, DaveyG 22/11/2012 - check if player has used quit button.
        IF DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(specData.specCamData)
//            bIamQuittingCoronaSpectator = TRUE
			DO_MATCH_END()
            bGoToRewardsAndFeedback = TRUE
			PRINTLN("[Survival] - DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE = TRUE, setting bGoToRewardsAndFeedback = TRUE")
        ENDIF
		CLEAR_DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(specData.specCamData)
	
	// If the mission has ended.
    ELSE
		
       	bGoToRewardsAndFeedback = TRUE
		PRINTLN("[Survival] - (GET_SERVER_HORDE_STAGE() = eHORDESTAGE_REWARDS_AND_FEEDACK), setting bGoToRewardsAndFeedback = TRUE")
		
    ENDIF
    
	
    // If we should be ending the mission.
    IF bGoToRewardsAndFeedback
       	
		// Save if we were mid tranisition into the spectator camera as it will affect how we exit.
		IF IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
		AND NOT IS_THIS_SPECTATOR_CAM_RUNNING(specData.specCamData)
			bModeEndedSpecCamTranisition = TRUE
		ENDIF
		
		// Go to rewards an feedback stage.
        SET_CLIENT_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
    
	
	
	// ***************************************
	// 	Process running spec cam during mode.
	// ***************************************
	
    // If we don't have to end the mission. 
    ELSE
        
        // If I am in the spectator mode as a corona spectator.
        IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)     
        OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
			
			SWITCH iRejoinFromSpecCamStage
				
				CASE 0
					
					// Process spec cam and display horde hud.
                	MAINTAIN_SPECTATOR_HORDE_CAM()
				
					// If I am allowed to play, leave the spec mode and start playing!
            		IF AM_I_STRAIGHT_TO_SPECTATOR_PLAYER_THAT_SHOULD_JOIN_FIGHT()
					OR AM_I_ABANDONED_HORDE_ON_PLAYLIST_PLAYER_THAT_SHOULD_JOIN_FIGHT()
						
						iRejoinFromSpecCamStage = 1
						
						#IF IS_DEBUG_BUILD
							IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
								PRINTLN("[Survival] - BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS, iRejoinFromSpecCamStage = 1.")
							ENDIF
							IF AM_I_STRAIGHT_TO_SPECTATOR_PLAYER_THAT_SHOULD_JOIN_FIGHT()
								PRINTLN("[Survival] - AM_I_STRAIGHT_TO_SPECTATOR_PLAYER_THAT_SHOULD_JOIN_FIGHT() = TRUE, iRejoinFromSpecCamStage = 1.")
							ENDIF
						#ENDIF
						
					ENDIF
					
				BREAK
				
				CASE 1
					
					IF NOT bSetupSpecificSpawnCoords
						FORCE_CLEAR_SPECTATOR_HIDDEN(specData.specCamData, TRUE)
						SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA(sSurvivalBoundsData.vMin, sSurvivalBoundsData.vMax, sSurvivalBoundsData.fWidth, g_FMMC_STRUCT.sFMMCEndConditions[0].vStartPos, TRUE, FALSE, FALSE)
						bSetupSpecificSpawnCoords = TRUE
					ELSE
						IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA)
							bSetupSpecificSpawnCoords = FALSE
							CLEAR_SPECIFIC_SPAWN_LOCATION()
			                CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
			                CLEAR_STRAIGHT_TO_SPECTATOR_PLAYER()
							iRejoinFromSpecCamStage = 0
							PRINTLN("[Survival] - iRejoinFromSpecCamStage = 0, call 1.")
							Clear_Any_Objective_Text_From_This_Script()
			                LEAVE_SPECTATOR_STAGE_AND_JOIN_FIGHT(0, FALSE)
						ENDIF
					ENDIF
					
				BREAK
				
			ENDSWITCH
			
        // If I am a spectator because I died during a wave.
        ELSE
            
            // Process spec cam and display horde hud.
            MAINTAIN_SPECTATOR_HORDE_CAM()
            
            // If not in the fighting stage, turn off the spectator camera.
            IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_FIGHTING
                    
                // If I am a proper player and not a late spectator.
                IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
                    IF GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) != eHORDESTAGE_SCTV
						IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_PASSED_WAVE
							LEAVE_SPECTATOR_STAGE_AND_JOIN_FIGHT(1, (serverBD.sWaveData.iWaveCount >= NUMBER_HORDE_WAVES) )
						ENDIF
                    ENDIF   
                ENDIF
				
            ENDIF
                    
        ENDIF
            
    ENDIF
    
ENDPROC

//===Start Edit David G===
PROC PROCESS_eHORDESTAGE_SCTV_CLIENT()
   	
    // Process ped actions.
    PROCESS_SQUAD_ENEMY_PED_BODIES()
    
    // Process pickup blips.
    PROCESS_PICKUP_BLIPS()
    
    // HUD.
    MAINTAIN_SPECTATOR_HORDE_HUD()
    
	IF IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_FADE_IN_REQUESTED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_FADE_IN_REQUESTED)")					
		#ENDIF
		IF NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()")					
			#ENDIF
			IF NOT IS_SCREEN_FADED_IN()
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === NOT IS_SCREEN_FADED_IN()")					
				#ENDIF
				IF NOT IS_SCREEN_FADING_IN()
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === NOT IS_SCREEN_FADING_IN()")					
					#ENDIF
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_FADE_IN_REQUESTED)")					
				#ENDIF
				CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_FADE_IN_REQUESTED)
			ENDIF
		ENDIF
	ENDIF
	
	// So SCTV players don;t hold up the survival for competitiors.
	IF IS_SCREEN_FADED_IN()
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_COMPLETED_START_FADE_IN)
	ENDIF
	
    IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
        PROCESS_LEADERBOARD()
	ELSE
		SET_ON_LBD_GLOBAL(FALSE)
    ENDIF
    
    IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_QUIT_REQUESTED)
        DO_SCREEN_FADE_OUT(50)
		PRINTLN("[Survival] - GLOBAL_SPEC_BS_QUIT_REQUESTED bit is set, calling SCRIPT_CLEANUP.")
		// Sort leader board data for end of mode.
        SCRIPT_CLEANUP()
    ENDIF
    
    // Everyone go to rewards and feedback if end of mode.
    IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_REWARDS_AND_FEEDACK
        IF IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
            DEACTIVATE_SPECTATOR_CAM(specData)
        ENDIF
		SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(FALSE)
        SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
    ENDIF
            
ENDPROC
//===End Edit David G===

PROC CLEANUP_BEFORE_END()

	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_SURVIVAL_PASSED)
	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_SURVIVAL_FAILED)
	
	PRINTLN("[Survival] - CLEANUP_BEFORE_END - being called.")
	
    // Fade screen out and leave script.
    IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		PRINTLN("[Survival] - CLEANUP_BEFORE_END - PLAYER_PED_ID exists.")
   		IF DO_PRE_LEAVE_SURVIVAL()
			PRINTLN("[Survival] - CLEANUP_BEFORE_END - DO_PRE_LEAVE_SURVIVAL = TRUE.")
            IF SHOULD_GO_TO_WAITING_TO_LEAVE_STATE()
				PRINTLN("[Survival] - CLEANUP_BEFORE_END - SHOULD_GO_TO_WAITING_TO_LEAVE_STATE = TRUE.")
                SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_WAITING_TO_LEAVE)
            ELSE
				PRINTLN("[Survival] - CLEANUP_BEFORE_END - SHOULD_GO_TO_WAITING_TO_LEAVE_STATE = FALSE.")
                SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_END)
            ENDIF
        ENDIF
    ENDIF
	
ENDPROC

FUNC BOOL IS_PARTICIPANT_IN_FIGHTING_PARTICIPANTS_LIST(INT iParticipant)
	
	INT i
	
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        IF serverBD.sFightingParticipantsList.iParticipant[i] = iParticipant
			IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
				RETURN TRUE
			ENDIF
        ENDIF
    ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

PROC SAVE_SURVIVAL_TEAM_GAMER_HANDLES_TO_GLOBALS()
	
	INT i, iGamerHandleCount
	PLAYER_INDEX playerId
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
			IF IS_PARTICIPANT_IN_FIGHTING_PARTICIPANTS_LIST(i)
				playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
				IF IS_NET_PLAYER_OK(playerId, FALSE)
					IF NOT IS_PLAYER_SCTV(playerId)
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.eCreateSurvivalRewardVehsGamerHandles[iGamerHandleCount] = GET_GAMER_HANDLE_PLAYER(playerId)
						IF (playerId = PLAYER_ID())
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.iCreateSurvivalRewardVehsMyGamerHandleArrayPos = iGamerHandleCount
						ENDIF
						iGamerHandleCount++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

BOOL bOkToDoOutro
BOOL bDoneOutro

PROC DO_OUTROS()
	IF NOT bDoneOutro
		IF bOkToDoOutro
			ANIM_OUTRO_PREP(jobIntroData)
			REQUEST_ANIMATIONS_FOR_INTRO(jobIntroData)
//			IF GET_CLIENT_MISSION_STAGE(playerBDPassed) > CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
				IF HAS_STARTED_NEW_OUTRO(jobIntroData)
					PRINTLN("DO_OUTROS, YES")
					bDoneOutro = TRUE
				ENDIF
//			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT()
	
	BOOL bFailedMode
	
	// ************************************************************
	// Functions that need processed regardless of feedback stage.
	// ************************************************************
	
	// B*1089716. Stops player staying on scoped weapons during feedback hud.
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	
	IF bSetSctvTickerOn
		SET_SCTV_TICKER_SCORE_OFF()
		bSetSctvTickerOn = FALSE
	ENDIF
	
	IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
	OR 	IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
		
		// Bugstar 2218090.
		SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(FALSE)
	
		#IF IS_DEBUG_BUILD
		BOOL bStraightToSpecCam = IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
		BOOL bSctv = IS_PLAYER_SCTV(PLAYER_ID())
		BOOL bAbandonded = IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
		PRINTLN("[Survival] - SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS - been called.")
		PRINTLN("[Survival] - SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS - IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM) = ", bStraightToSpecCam)
		PRINTLN("[Survival] - SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS - IS_PLAYER_SCTV(PLAYER_ID()) = ", bSctv)
		PRINTLN("[Survival] - SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS - IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE) = ", bAbandonded)
		#ENDIF
		
		SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS(jobUnderTenSecondsTimer, 
																(IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM) OR IS_PLAYER_SCTV(PLAYER_ID())),
																IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE))
		
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM) 
		AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_SURVIVAL) 
		ENDIF
		
	ENDIF
			
	// ***************************
	// Feedback stage sub-stages.
	// ***************************
	
	SWITCH iFeedbackStage
		
		
		// -----------------------------------
		// Wait for wasted message to clear.
		// -----------------------------------
		
		CASE 0
			
			IF NOT bDealtWithWasted
				
				playerBD[PARTICIPANT_ID_TO_INT()].iSurvivalScore = CALCULATE_SURVIVAL_SCORE()
				PRINTLN("[Survival] - calculated my survival score to be ", playerBD[PARTICIPANT_ID_TO_INT()].iSurvivalScore)
				
				IF IS_ENTITY_DEAD(PLAYER_PED_ID())
				
					IF ALLOW_FOR_WASTED_MESSAGE()
						bDealtWithWasted = TRUE
						PRINTLN("[Survival] - player dead, allowed for wasted message.")
					ENDIF
					
					IF IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData) 
						IF IS_THIS_SPECTATOR_CAM_RUNNING(specData.specCamData)
							bDealtWithWasted = TRUE
							PRINTLN("[Survival] - player on spec cam, already allowed for wasted message.")
						ENDIF
					ELSE
						SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_AUTO_REJOIN)
					ENDIF
					
				ELSE
					
					SAVE_PLAYER_END_JOB_POSITION_FOR_POST_MISSION_CLEANUP()
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
					ENDIF
					
					bDealtWithWasted = TRUE
					PRINTLN("[Survival] - player not dead, don't need to allow for wasted message.")
					
				ENDIF
				
				NEW_LOAD_SCENE_STOP()
				PRINTLN("[Survival] - NEW_LOAD_SCENE_STOP().")
				
				CLEAR_FOCUS()
				PRINTLN("[Survival] - CLEAR_FOCUS().")
				
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVING_SURVIVAL)
				
				SAVE_SURVIVAL_TEAM_GAMER_HANDLES_TO_GLOBALS()
				
			ELSE
				
				CLEANUP_DPAD_LBD(eMiniLeaderBoardData.siMovie)
				
				IF SHOULD_I_PROCESS_END_HORDE_STATS()
					
					IF GET_WAVE_SURVIVED_UNTIL() > GET_MP_INT_CHARACTER_STAT(MP_STAT_HORDELVL)
						SET_MP_INT_CHARACTER_STAT(MP_STAT_HORDELVL, GET_WAVE_SURVIVED_UNTIL())
					ENDIF
					
					IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
						INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_HORDEWINS, 1)
						PRINTLN("[Survival] - [Awards] - beat all waves, incrementing stat MP_STAT_HORDEWINS by 1.")
					ENDIF
					
					// Sort unlocking of other survivals. Only want to do this if we've actually played, not if we were only a spectator or an sctv person.
					IF GET_DISTANCE_BETWEEN_COORDS(<<269.3056,2777.5464,42.8282>>, g_FMMC_STRUCT.vStartPos) <= 2.5
					OR GET_DISTANCE_BETWEEN_COORDS(<<2296.8977,3110.9358,46.4730>>, g_FMMC_STRUCT.vStartPos) <= 2.5
						SET_PLAYER_HAS_COMPLETED_PRIMARY_SURVIVAL()
						PRINTLN("[Survival] - played a primary survival, called SET_PLAYER_HAS_COMPLETED_PRIMARY_SURVIVAL().")
					ENDIF
					
				ENDIF
				
				iFeedbackStage++
				PRINTLN("[Survival] - going to iFeedbackStage ", iFeedbackStage)
				
			ENDIF
					
		BREAK
		
		
		// ------------------------------------------------------------
		// Cleanup and sorting of data before displaying end feedback.
		// ------------------------------------------------------------
		CASE 1
			
			// No more blips.
		    REMOVE_HORDE_BLIPS()
		    
		    // No more teammate died big messages.
		    CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_SURVIVAL_TM_DIED)
			
		    // Comment out on 22/02/2013 by daveyg experiment for PT 1139613
			//DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT()		
			
			// Sort leader board data for end of mode.
			DO_END_OF_MODE_LEADERBOARD_DATA_SORT()
			
			// No objective text during this bit.
	        IF NOT bClearedObjectiveTextForRewards
	            Clear_Any_Objective_Text()
	            SET_LOCAL_PLAYER_CAN_COLLECT_HORDE_PICKUPS(FALSE)
	            bClearedObjectiveTextForRewards = TRUE
	        ENDIF
			
			// Deal with award for most survival kills.
			IF NOT bProcessedMostSurvivalKillsAward
				UPDATE_HORDE_WAVES_SURVIVED_AWARD()
				IF serverBD.sFightingParticipantsList.iNumFightingParticipants >= 4
					IF DID_I_GET_MOST_SURVIVAL_KILLS()
						PRINTLN("[Survival] - [Awards] - I got the most kills, giving myself award MP_AWARD_FMMOSTKILLSSURVIVE")
						SET_MP_BOOL_CHARACTER_AWARD (MP_AWARD_FMMOSTKILLSSURVIVE, TRUE)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[Survival] - [Awards] - I did not get the most kills.")
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Survival] - [Awards] - serverBD.sFightingParticipantsList.iNumFightingParticipants < 3, cannot give award MP_AWARD_FMMOSTKILLSSURVIVE.")
				#ENDIF
				ENDIF
				bProcessedMostSurvivalKillsAward = TRUE
			ENDIF
			
			// Handle stats and awards.
	        IF SHOULD_I_PROCESS_END_HORDE_STATS()
	            IF NOT bGivenEndModeRewards
					REWARD_XP_FOR_PLAYING_OWN_CONTENT_AGAINST_OTHERS(NETWORK_GET_NUM_PARTICIPANTS())
	                bGivenEndModeRewards = TRUE
	            ENDIF
	        ENDIF
			
			// checks player did not abandon game BUG 1727516
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
				DO_CREW_CUT_ACHIEVEMENT() // 1415006
			ENDIF
			// Give XP for taking part.
			IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
	            IF NOT bGivenTakingPartXp
					INT iXpGiven
	               	iXpGiven = GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_TKPRTHRD", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_SURVIVAL_TAKEN_PART, TAKING_PART_XP, 1)
					playerBD[PARTICIPANT_ID_TO_INT()].iXpGained += iXpGiven
					ADD_TO_END_CELBRATION_SCORE(iXpGiven)
					#IF IS_DEBUG_BUILD
					iTakePartXp = iXpGiven
					#ENDIF
	                bGivenTakingPartXp = TRUE
	            ENDIF
			ENDIF
			
			CLEAR_CELEBRATION_SCREEN_MOVIE_IDS(sCelebrationData)
			
			iFeedbackStage++
			PRINTLN("[Survival] - going to iFeedbackStage ", iFeedbackStage)
			
		BREAK
		
		
		// --------------------------------
		// Request the leaderboard camera.
		// --------------------------------
		CASE 2
			BOOL bReadyToContinue
		
			DISABLE_SPECTATOR_HUD(specData.specHUDData)
			START_LBD_AUDIO_SCENE()
			
			IF IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
			//AND IS_THIS_SPECTATOR_CAM_RUNNING(specData.specCamData) //1639621 - If all players die at the same time then this check could fall through and never terminate spectator cam.
				IF IS_SCREEN_FADED_IN()
					FADE_SCREEN_IN()
				ENDIF
				SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
				IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
					DEACTIVATE_SPECTATOR_CAM(specData, DSCF_PAUSE_RENDERPHASE)
				ELSE
					//1623884 - If the player is on spectator cam due to death then freeze on exit for the celebration screen transition.
					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)  
						DEACTIVATE_SPECTATOR_CAM(specData, DSCF_REMAIN_VIEWING_TARGET | DSCF_BAIL_FOR_TRANSITION)
					ELSE
						DEACTIVATE_SPECTATOR_CAM(specData, DSCF_REMAIN_VIEWING_TARGET | DSCF_PAUSE_RENDERPHASE)
						
						IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
							PLAY_CELEB_WIN_POST_FX()
							PRINTLN("[Survival] - playing MP_Celeb_Win, call 5")
						ELSE
							PLAY_CELEB_LOSE_POST_FX()
						ENDIF
						
						PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
						
						bAlreadyTriggeredCelebrationPostFX = TRUE
					ENDIF
				ENDIF
				
				bReadyToContinue = TRUE
				PRINTLN("[Survival] - spec cam active while in rewards stage, calling deactivate with DSCF_REMAIN_VIEWING_TARGET flag.")
				PRINTLN("[Survival] - going to iFeedbackStage ", iFeedbackStage)

			ELSE
				BOOL bPlacedCameraSuccessfully
			
				IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
					IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camCelebrationScreen, bPlacedCameraSuccessfully)
//						IF HAS_STARTED_NEW_OUTRO(jobIntroData)
							bOkToDoOutro = TRUE
							bReadyToContinue = TRUE
//						ELSE
//							PRINTLN("[eENDHORDEREASON_BEAT_ALL_WAVES], HAS_STARTED_NEW_OUTRO (Speirs) ")
//						ENDIF
					ELSE
						PRINTLN("[eENDHORDEREASON_BEAT_ALL_WAVES], PLACE_CAMERA_FOR_CELEBRATION_SCREEN ")
					ENDIF
				ELSE
					bReadyToContinue = TRUE
				ENDIF
				
				IF bReadyToContinue				
					IF bPlacedCameraSuccessfully
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ELSE
						//1626331 - If the celebration cam didn't successfully create then turn the ped to face the game cam.
						IF IS_NET_PLAYER_OK(PLAYER_ID())
						AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							TASK_TURN_PED_TO_FACE_COORD(PLAYER_PED_ID(), GET_FINAL_RENDERED_CAM_COORD(), 2000)
						ENDIF
					ENDIF
					
					IF NOT bAlreadyTriggeredCelebrationPostFX
						IF IS_NET_PLAYER_OK(PLAYER_ID())
							ANIMPOSTFX_STOP_ALL()
						ENDIF
						
						IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
							PLAY_CELEB_WIN_POST_FX()	
							PRINTLN("[Survival] - playing MP_Celeb_Win, call 6")
						ELSE
							PLAY_CELEB_LOSE_POST_FX()
						ENDIF
					ENDIF
					
					PLAY_SOUND_FRONTEND(-1, "MP_WAVE_COMPLETE", "HUD_FRONTEND_DEFAULT_SOUNDSET")

					PRINTLN("[Survival] - going to iFeedbackStage ", iFeedbackStage)
				ENDIF
			ENDIF
			
			IF bReadyToContinue
				REQUEST_CELEBRATION_SCREEN(sCelebrationData)
				SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
				KILL_UI_FOR_CELEBRATION_SCREEN()
				HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
				IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
				AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				
				IF IS_PAUSE_MENU_ACTIVE()					
					SET_FRONTEND_ACTIVE(FALSE)
				ENDIF

				iFeedbackStage++
			ENDIF
		BREAK
		
		
		// --------------------
		// Do rank predicition.
		// --------------------
		
		CASE 3
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			// setup the social club leaderboard data.
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,
													GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType,
													g_FMMC_STRUCT.tl31LoadedContentID,
													g_FMMC_STRUCT.tl63MissionName)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31MissionName)
			
			IF scLB_rank_predict.bFinishedRead
			AND NOT scLB_rank_predict.bFinishedWrite
				WRITE_TO_SURVIVAL_SC_LB()
				scLB_rank_predict.bFinishedWrite = TRUE
			ENDIF
	        
			IF GET_RANK_PREDICTION_DETAILS(scLB_control)
				sclb_useRankPrediction = TRUE
				iFeedbackStage++
				PRINTLN("[Survival] - going to iFeedbackStage ", iFeedbackStage)
			ENDIF
			
		BREAK
		
		
		// -----------------
		// Display end hud.
		// -----------------
		
		CASE 4
			
			// setup the social club leaderboard data.
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,
													GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType,
													g_FMMC_STRUCT.tl31LoadedContentID,
													g_FMMC_STRUCT.tl63MissionName)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31MissionName)
																
			//LOAD_SOCIAL_CLUB_LEADERBOARD_DATA(scLB_control)
			
			// If not done feedback yet.
   			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK)
				
				PRINTLN("[Survival] - BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK = FALSE.")
				
				// No dying during end feedback.
		        IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					// Fix for bug 900517 - hide radar during wave complete screen.
			       	HIDE_HUD_AND_RADAR_THIS_FRAME()
						
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
						AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
							IF IS_SKYSWOOP_AT_GROUND() 
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					// Sort music.
					IF IS_SCORE_CORONA_OPTION_ON()
						IF NOT structMusicEvent[ENUM_TO_INT(eMUSICEVENT_STOP)].bTriggered
							IF PREPARE_MUSIC_EVENT(GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_STOP, serverBD.sWaveData.iWaveCount))
								TRIGGER_MUSIC_EVENT(GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_STOP, serverBD.sWaveData.iWaveCount))
								structMusicEvent[ENUM_TO_INT(eMUSICEVENT_STOP)].bTriggered = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		                SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
		            ENDIF
		            
					IF NOT IS_END_OF_ACTIVITY_HUD_ACTIVE()
						bAllAwardsBeenShown = TRUE
					ENDIF
					
					IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
						bFailedMode = TRUE
					ENDIF
					IF DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(specData.specCamData)
						bFailedMode = TRUE
					ENDIF
					
					DISPLAY_END_OF_WAVE_TEXT(TRUE, bFailedMode)
					
					IF g_bCelebrationScreenIsActive
					OR bNewJobScreens //There's a few frames between the celebration screen and the voting screen where the rank bar appears.
						HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
					ENDIF
					
		            // Deal with end of mission feedback
//		            IF SHOULD_I_DISPLAY_END_LEADERBOARD()
					IF NOT g_bCelebrationScreenIsActive
					
						PRINTLN("[Survival] - g_bCelebrationScreenIsActive = FALSE.")
						
						IF NOT bDoneMatchEnd
							DO_MATCH_END()
							bDoneMatchEnd = TRUE
						ENDIF
						
						IF NOT bSetMissionOverAndOnLbForSctv
							SET_MISSION_OVER_AND_ON_LB_FOR_SCTV()
							bSetMissionOverAndOnLbForSctv = TRUE
						ENDIF
						
						IF bAllAwardsBeenShown
						OR bNewJobScreens
							
							#IF IS_DEBUG_BUILD
							IF bAllAwardsBeenShown
								PRINTLN("[Survival] - bAllAwardsBeenShown = TRUE.")
							ENDIF
							IF bNewJobScreens
								PRINTLN("[Survival] - bNewJobScreens = TRUE.")
							ENDIF
							#ENDIF
							
							IF IS_NET_PLAYER_OK(PLAYER_ID())
								IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
									INVISIBLE_FOR_LBD()
								ENDIF
							ENDIF
						
							IF bNewJobScreens = TRUE
								CLEAR_ALL_BIG_MESSAGES()	
							ENDIF
							
							//If the player is on a playlist
							IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
								
								PRINTLN("[Survival] - IS_PLAYER_ON_A_PLAYLIST = TRUE.")
								
								IF iEndOfWaveHudStage >= 3
									PRINTLN("[Survival] - iEndOfWaveHudStage >= 3")
									IF CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS(sEndOfMission.iProgress, sEndOfMission, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
										PRINTLN("[Survival] - CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS = TRUE")
										CLEANUP_BEFORE_END()
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK)
									ELSE
										PRINTLN("[Survival] - CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS = FALSE")
									ENDIF
								ENDIF
								
							//Not on a playlist do this:
							ELSE
								
								PRINTLN("[Survival] - IS_PLAYER_ON_A_PLAYLIST = FALSE.")
								
								// Finish up
								IF bMoveToEnd
								OR DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(specData.specCamData)
									IF bIamQuittingCoronaSpectator
										DEACTIVATE_SPECTATOR_CAM(specData/*, DSCF_REMAIN_VIEWING_TARGET*/)
									ENDIF
									CLEANUP_BEFORE_END()
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK)
									
									#IF IS_DEBUG_BUILD
									PRINTLN("[Survival] - [CS_HORDE] bMoveToEnd")
									IF DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(specData.specCamData)
										PRINTLN("[Survival] - DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE = TRUE.")
									ENDIF
									#ENDIF
								ELSE
									IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
										sEndOfMission.bBeingKickedFromSurvival = TRUE
										PRINTLN("[Survival] - BITSET_PLAYER_ABANDONED_HORDE = TRUE.")
									ENDIF
									IF bNewJobScreens = FALSE
										PRINTLN("[Survival] - bNewJobScreens = FALSE.")
										IF MAINTAIN_END_OF_MISSION_SCREEN(lbdVars,sEndOfMission, serverBD.sServerFMMC_EOM,TRUE,FALSE,FALSE,FALSE,FALSE,TRUE, NULL, 0, "", FALSE, TRUE, SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, serverBD.sServerFMMC_EOM.iVoteStatus))
											PRINTLN("[Survival] - MAINTAIN_END_OF_MISSION_SCREEN = TRUE.")
											IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, serverBD.sServerFMMC_EOM.iVoteStatus)
												PRINTLN("[Survival] - SHOULD_RUN_NEW_VOTING_SYSTEM = TRUE.")
												IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
													SET_READY_FOR_NEXT_JOBS(nextJobStruct)
													// Set up end Job screens
													bNewJobScreens = TRUE
													PRINTLN("[Survival] - [CS_HORDE] bNewJobScreens = TRUE")
													PRINTLN("[Survival] - setting bNewJobScreens = TRUE.")
												ELSE
													// Just finish up
													bMoveToEnd = TRUE
													PRINTLN("[Survival] - [CS_HORDE] bMoveToEnd = TRUE")
													#IF IS_DEBUG_BUILD
													IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
														PRINTLN("[Survival] - bQuitJob = TRUE.")
													ENDIF
													#ENDIF
												ENDIF
											ELSE
												// Just finish up
												bMoveToEnd = TRUE
												PRINTLN("[Survival] - [CS_HORDE] bMoveToEnd = TRUE")
												PRINTLN("[Survival] - SHOULD_RUN_NEW_VOTING_SYSTEM = FALSE.")
											ENDIF
										ELSE
											PRINTLN("[Survival] - [CS_HORDE] MAINTAIN_END_OF_MISSION_SCREEN = FALSE")
											PRINTLN("[Survival] - MAINTAIN_END_OF_MISSION_SCREEN = FALSE.")
										ENDIF
									ELSE
										PRINTLN("[Survival] - bNewJobScreens = TRUE.")
										IF NOT sEndOfMission.bBeingKickedFromSurvival
											IF DO_END_JOB_VOTING_SCREEN(g_sMC_serverBDEndJob, nextJobStruct, g_sMC_serverBDEndJob.timerVotingEnds.Timer)
												PRINTLN("[Survival] - DO_END_JOB_VOTING_SCREEN = TRUE.")
												bMoveToEnd = TRUE
											ELSE
												PRINTLN("[Survival] - DO_END_JOB_VOTING_SCREEN = FALSE.")
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
											IF sEndOfMission.bBeingKickedFromSurvival
												PRINTLN("[Survival] - bBeingKickedFromSurvival = TRUE.")
											ENDIF
											#ENDIF
											bMoveToEnd = TRUE
										ENDIF
										PRINTLN("[Survival] - [CS_HORDE] DO_END_JOB_VOTING_SCREEN")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[Survival] - g_bCelebrationScreenIsActive = TRUE.")
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

//		
//		// Do friend comparison of scores and send messages to those I beat.
//		IF NOT bDidFriendLeaderboardComparison
////			IF DO_FRIEND_SCORE_COMPARISON(scLB_controlFriendComp, sUgcStatData, playerBD[PARTICIPANT_ID_TO_INT()].iSurvivalScore, bHaveFriendToSendTo, FMMC_TYPE_SURVIVAL, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName)
//				bDidFriendLeaderboardComparison = TRUE
////			ENDIF
//		ENDIF
//		
//		//Draw the leaderboard
//		IF bDidFriendLeaderboardComparison
//			
//		ENDIF
//		
	
ENDPROC

PROC DRAW_DPAD_LEADERBOARD(DPAD_VARS &sDpadVars)

    INT i, iRank, iPlayer
    INT iSlotCounter
	TEXT_LABEL_15 tlCrewTag
	STRING strHeadshotTxd
	PEDHEADSHOT_ID pedHeadshot
	PLAYER_INDEX playerId
	PARTICIPANT_INDEX partId
	//REFRESH_DPAD_LEADERBOARD_AFTER_TIME(sDpadVars)
	
	g_i_NumDpadLbdPlayers = 0
	
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        
		IF (eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID != INVALID_PLAYER_INDEX())
			
            IF IS_NET_PLAYER_OK(eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID, FALSE)
				
				partId = NETWORK_GET_PARTICIPANT_INDEX(eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID)
				
				IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(partId)].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
					
					playerId = eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID
				
					iPlayer = NATIVE_TO_INT(playerId)
					
					IF playerId = PLAYER_ID()
						sDpadVars.iPlayersRow = iSlotCounter
					ENDIF
					       
	//              PRINTLN("[Survival] - DRAW_DPAD_LEADERBOARD - miniLeaderBoardStruct[", i, "].iParticipant = ", eMiniLeaderBoardData.miniLeaderBoardStruct[i].iParticipant)N
	//              PRINTLN("[Survival] - DRAW_DPAD_LEADERBOARD - miniLeaderBoardStruct[", i, "].iPlayerScore = ", eMiniLeaderBoardData.miniLeaderBoardStruct[i].iPlayerScore)
	//              PRINTLN("[Survival] - DRAW_DPAD_LEADERBOARD - miniLeaderBoardStruct[", i, "].iParticipant = ", NATIVE_TO_INT(playerId))N
	//              PRINTLN("[Survival] - DRAW_DPAD_LEADERBOARD - miniLeaderBoardStruct[", i, "].iKills = ", eMiniLeaderBoardData.miniLeaderBoardStruct[i].iKills)

					sDpadVars.iScore = eMiniLeaderBoardData.miniLeaderBoardStruct[i].iKills
	//				sDpadVars.tlDisplayStr = GET_PLAYER_NAME(playerId)
	//				sDpadVars.tlDisplayStr = tl63LeaderboardPlayerNames[i]
					sDpadVars.tl63LeaderboardPlayerNames = tl63LeaderboardPlayerNames[i]
					
					IF IS_ENTITY_DEAD(GET_PLAYER_PED(playerId))
						SET_BIT(sDpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
					ELSE
						CLEAR_BIT(sDpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
					ENDIF
					
					// Rank
					iRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
					
					// Attempt to get player headshot
					pedHeadshot = Get_HeadshotID_For_Player(playerId)
					strHeadshotTxd = ""
					IF pedHeadshot <> NULL
						
						strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
						PRINTLN("[Survival] - [CS_DPAD] OK strHeadshotTxd = ", strHeadshotTxd)
	//				ELSE
	//					PRINTLN("[CS_DPAD] NULL strHeadshotTxd = ", strHeadshotTxd)
					ENDIF
					
	//				POPULATE_SCALEFORM_DPAD_ROWS(SUB_HORDE, sDpadVars, eMiniLeaderBoardData.siMovie, iSlotCounter, 0, GET_CREW_TAG_DPAD_LBD(playerId))
	
					PRINTLN("[Survival] - [CS_DPAD] Survival about to call POPULATE_NEW_FREEMODE_DPAD_ROWS with row = ", iSlotCounter, " player name = ", sDpadVars.tl63LeaderboardPlayerNames, " iPlayer = ", iPlayer)
	
					POPULATE_NEW_FREEMODE_DPAD_ROWS(SUB_HORDE, eMiniLeaderBoardData.siMovie, sDpadVars, iSlotCounter, tlCrewTag, strHeadshotTxd, iRank, eMiniLeaderBoardData.miniLeaderBoardStruct[i].iKills, FRIENDLY_DM_COLOUR(), DISPLAY_HEADSHOT, -1)
					
					// Store out mics row
					sDpadVars.sDpadMics[iPlayer].iRow =	iSlotCounter		
					
					g_i_NumDpadLbdPlayers ++															
	                iSlotCounter++
				
				ENDIF
				
            ENDIF
		ENDIF
    ENDREPEAT
ENDPROC

//eMINI_LEADER_BOARD_STATE eMiniLeaderBoardState
//LEADERBOARD_PLACEMENT_TOOLS sPlacement
//THE_LEADERBOARD_STRUCT miniLeaderBoardStruct[MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS]
//STRUCT_HORDE_BOARD_DATA sMiniLeaderBoardSortedData

//STRUCT STRUCT_HORDE_BOARD_DATA
//      BOOL bPopulatedHudPlayerArray
//      PLAYER_INDEX hudPlayers[4]
//      INT iScores[4]
//ENDSTRUCT

PROC COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA()
    
    INT i
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i

        eMiniLeaderBoardData.miniLeaderBoardStruct[i].iParticipant      = (-1)
        eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID          = INVALID_PLAYER_INDEX()
        eMiniLeaderBoardData.miniLeaderBoardStruct[i].iPlayerScore      = (-1)
        eMiniLeaderBoardData.miniLeaderBoardStruct[i].iKills            = (-1)
//              
//      PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA - reset data for fighting participant ", i)
//              
        IF (eMiniLeaderBoardData.sMiniLeaderBoardSortedData.hudPlayers[i] != INVALID_PLAYER_INDEX())
            
            eMiniLeaderBoardData.miniLeaderBoardStruct[i].iParticipant      = NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX( eMiniLeaderBoardData.sMiniLeaderBoardSortedData.hudPlayers[i] ) )
			tl63LeaderboardPlayerNames[i]									= GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX( eMiniLeaderBoardData.sMiniLeaderBoardSortedData.hudPlayers[i])))
            eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID          = eMiniLeaderBoardData.sMiniLeaderBoardSortedData.hudPlayers[i]
            eMiniLeaderBoardData.miniLeaderBoardStruct[i].iPlayerScore      = eMiniLeaderBoardData.sMiniLeaderBoardSortedData.iScores[i]
            eMiniLeaderBoardData.miniLeaderBoardStruct[i].iKills            = eMiniLeaderBoardData.sMiniLeaderBoardSortedData.iScores[i]
                      
	        PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA - miniLeaderBoardStruct[", i, "].iParticipant = ", eMiniLeaderBoardData.miniLeaderBoardStruct[i].iParticipant)
	        PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA -  = tl63LeaderboardPlayerNames[", i, "] = ", tl63LeaderboardPlayerNames[i])
			PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA - miniLeaderBoardStruct[", i, "].iPlayerScore = ", eMiniLeaderBoardData.miniLeaderBoardStruct[i].iPlayerScore)
	        PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA - miniLeaderBoardStruct[", i, "].playerID = ", NATIVE_TO_INT(eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID))
	        PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA - miniLeaderBoardStruct[", i, "].iKills = ", eMiniLeaderBoardData.miniLeaderBoardStruct[i].iKills)
	                      
//        ELSE
//      	
//          PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA - eMiniLeaderBoardData.sMiniLeaderBoardSortedData.hudPlayers[", i, "] = NULL or INVALID_PLAYER_INDEX()")
//              
        ENDIF
            
    ENDREPEAT
    
ENDPROC

PROC MAINTAIN_MINI_LEADER_BOARD()
    
	INT i,iIcon
	PLAYER_INDEX playerId
	
    // Temp release build compile fix - Dave R
    BOOL bDebugPrints 
    #IF IS_DEBUG_BUILD bDebugPrints = bDoMiniBoardDebugPrints #ENDIF
    
    IF GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) = eHORDESTAGE_FIGHTING
            
        SWITCH eMiniLeaderBoardData.eMiniLeaderBoardState
            
            // Do not display leader board.
            CASE eMINILEADERBOARDSTATE_OFF
                
				IF SHOULD_DPAD_LBD_DISPLAY(eMiniLeaderBoardData.siMovie, SUB_HORDE, eMiniLeaderBoardData.sDpadVars, serverBD.sFightingParticipantsList.iNumFightingParticipants )
					eMiniLeaderBoardData.eMiniLeaderBoardState = eMINILEADERBOARDSTATE_DO_FIRST_SORT
				ENDIF
				
            BREAK
            
            // Init leader board data.
            CASE eMINILEADERBOARDSTATE_DO_FIRST_SORT
                    
                // Sort players and copy data into leaderboard array.
				IF SHOULD_DPAD_LBD_DISPLAY(eMiniLeaderBoardData.siMovie, SUB_HORDE, eMiniLeaderBoardData.sDpadVars, serverBD.sFightingParticipantsList.iNumFightingParticipants)
	                
					IF NOT eMiniLeaderBoardData.sMiniLeaderBoardSortedData.bPopulatedHudPlayerArray
	                        
	                    // Sort data.
	                    GET_PLAYER_KILLS(eMiniLeaderBoardData.sMiniLeaderBoardSortedData, FALSE)
	                    POPULATE_HUD_PLAYER_ARRAY(eMiniLeaderBoardData.sMiniLeaderBoardSortedData, bDebugPrints)
	                    COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA()
	                    eMiniLeaderBoardData.sMiniLeaderBoardSortedData.bPopulatedHudPlayerArray = TRUE
						
	                // Move onto leader board on state.
	                ELSE
	                    PRINTLN("[Survival] - sMiniLeaderBoardSortedData.bPopulatedHudPlayerArray = TRUE, going to mini leader board state eMINILEADERBOARDSTATE_ON.")
	                    eMiniLeaderBoardData.eMiniLeaderBoardState = eMINILEADERBOARDSTATE_ON
	                ENDIF
            	
				ELSE
				
					PRINTLN("[Survival] - SHOULD_DPAD_LBD_DISPLAY = FALSE, going to mini leader board state eMINILEADERBOARDSTATE_OFF.")
	                eMiniLeaderBoardData.eMiniLeaderBoardState = eMINILEADERBOARDSTATE_OFF
					
				ENDIF
				
            BREAK
            
            // Draw leader board and check conditions for turning it off.
            CASE eMINILEADERBOARDSTATE_ON
                
				IF SHOULD_DPAD_LBD_DISPLAY(eMiniLeaderBoardData.siMovie, SUB_HORDE, eMiniLeaderBoardData.sDpadVars, serverBD.sFightingParticipantsList.iNumFightingParticipants)
					
					SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
					
	                // Sort data.
	                GET_PLAYER_KILLS(eMiniLeaderBoardData.sMiniLeaderBoardSortedData, FALSE)
	                POPULATE_HUD_PLAYER_ARRAY(eMiniLeaderBoardData.sMiniLeaderBoardSortedData, bDebugPrints)
	                COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA()
	                
	                // Draw leader board.
	                DRAW_DPAD_LEADERBOARD(eMiniLeaderBoardData.sDpadVars)
	                
					IF NOT IS_BIT_SET(eMiniLeaderBoardData.sDpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
						HIGHLIGHT_LOCAL_PLAYER(eMiniLeaderBoardData.sDpadVars, eMiniLeaderBoardData.siMovie)
						DISPLAY_VIEW(eMiniLeaderBoardData.siMovie)
						SET_BIT(eMiniLeaderBoardData.sDpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
					ENDIF
					
					HIGHLIGHT_LOCAL_PLAYER(eMiniLeaderBoardData.sDpadVars, eMiniLeaderBoardData.siMovie)
					
					// Draw the mics
					REPEAT NUM_NETWORK_PLAYERS i
						PlayerId = INT_TO_PLAYERINDEX(i)
						IF PlayerId <> INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(playerId, FALSE)
								IF eMiniLeaderBoardData.sDpadVars.sDpadMics[i].iRow <> -1
									iIcon = GET_DPAD_PLAYER_VOICE_ICON(eMiniLeaderBoardData.sDpadVars.sDpadMics[i].iVoiceChatState)
									SET_PLAYER_MIC(eMiniLeaderBoardData.siMovie, eMiniLeaderBoardData.sDpadVars.sDpadMics[i].iRow, iIcon, GlobalplayerBD_FM[i].scoreData.iRank)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF HAS_SCALEFORM_MOVIE_LOADED(eMiniLeaderBoardData.siMovie)
						DRAW_SCALEFORM_MOVIE(eMiniLeaderBoardData.siMovie, SDPAD_X, SDPAD_Y, SDPAD_W, SDPAD_H, 255, 255, 255, 255)
					ENDIF
					
					IF MAINTAIN_DPAD_VOICE_CHAT(eMiniLeaderBoardData.sDpadVars)
					
//						PRINTLN("[CS_DPAD]  MAINTAIN_DPAD_VOICE_CHAT, eMiniLeaderBoardData.sDpadVars.iDrawProgress = DPAD_INIT")
					
//						eMiniLeaderBoardData.sDpadVars.iDrawProgress = DPAD_INIT
					ENDIF
					
					RESET_SCRIPT_GFX_ALIGN()
				ELSE
					PRINTLN("[Survival] - SHOULD_DPAD_LBD_DISPLAY = FALSE, going to mini leader board state eMINILEADERBOARDSTATE_OFF.")
	                eMiniLeaderBoardData.eMiniLeaderBoardState = eMINILEADERBOARDSTATE_OFF
					RESET_DPAD_REFRESH()
					
					IF IS_BIT_SET(eMiniLeaderBoardData.sDpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
						CLEAR_BIT(eMiniLeaderBoardData.sDpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
					ENDIF
				ENDIF
            BREAK
            
        ENDSWITCH
            
    ELSE
        
        // If we're not in the fighting stage, reset.
		TEXT_LABEL_23 tl23_Temp
		REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
			tl63LeaderboardPlayerNames[i] = tl23_Temp
		ENDREPEAT
        eMiniLeaderBoardData.eMiniLeaderBoardState = eMINILEADERBOARDSTATE_OFF
        
    ENDIF
	
ENDPROC

/*PROC MAINTAIN_ENEMY_SPEC_CAM_REGISTRATION()	//removed for 1623032
	
	IF NOT IS_NET_PED_INJURED(serverBd.structAiSquad[iSpecCamRegSquad].sPed[iSpecCamRegPedCount].netId)
		IF NOT IS_BIT_SET(iRegisteredEnemyWithSpecCamBitset[iSpecCamRegSquad], iSpecCamRegPedCount)
			REGISTER_PED_AS_SPECTATOR_TARGET_GENERIC_ENEMY(specData.specHUDData, NET_TO_PED(serverBd.structAiSquad[iSpecCamRegSquad].sPed[iSpecCamRegPedCount].netId))
			SET_BIT(iRegisteredEnemyWithSpecCamBitset[iSpecCamRegSquad], iSpecCamRegPedCount)
		ENDIF
	ELSE
		IF IS_BIT_SET(iRegisteredEnemyWithSpecCamBitset[iSpecCamRegSquad], iSpecCamRegPedCount)
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBd.structAiSquad[iSpecCamRegSquad].sPed[iSpecCamRegPedCount].netId)
				UNREGISTER_PED_WITH_SPECTATOR_TARGET_LISTS(specData.specHUDData, NET_TO_PED(serverBd.structAiSquad[iSpecCamRegSquad].sPed[iSpecCamRegPedCount].netId))
			ENDIF
			CLEAR_BIT(iRegisteredEnemyWithSpecCamBitset[iSpecCamRegSquad], iSpecCamRegPedCount)
		ENDIF
	ENDIF
	
	iSpecCamRegPedCount++
	
	IF iSpecCamRegPedCount >= MAX_NUM_PEDS_IN_SQUAD
		iSpecCamRegSquad++
		iSpecCamRegPedCount = 0
	ENDIF
	
	IF iSpecCamRegSquad >= MAX_NUM_ENEMY_SQUADS
		iSpecCamRegSquad = 0
	ENDIF
	
ENDPROC*/

PROC MAINTAIN_CANNOT_USE_VEHICLES_HELP()
	
	VEHICLE_INDEX vehIndex
	BOOL bDisplayHelp
	
	// Tell players they cannot use vehicles.
	IF NOT bDisplayedNoVehsHelp
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				
					IF IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
						bDisplayHelp = TRUE
					ENDIF
					
					vehIndex = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
					
					IF DOES_ENTITY_EXIST(vehIndex)
						IF IS_VEHICLE_DRIVEABLE(vehIndex)
							IF GET_VEHICLE_DOOR_LOCK_STATUS(vehIndex) = VEHICLELOCK_LOCKED
								bDisplayHelp = TRUE
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bDisplayHelp
		PRINT_HELP("HRD_NOJACK")
		bDisplayedNoVehsHelp = TRUE
	ENDIF
	
ENDPROC

PROC PROCESS_SPECTATOR_FLAGS()
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND)
		IF serverBD.sWaveData.iWaveCount = 10
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND)")					
			#ENDIF
			SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLE_REALTIME_MULTIPLAYER()
	
	IF g_bBlock_PROCESS_DISABLE_REALTIME_MULTIPLAYER
		PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - GLOBAL BLOCK SET - EXITING")
		EXIT
	ENDIF
	
	IF g_sMPTunables.bDisableRealtimeMPCheck_JIP_HORDE // If set use the functionality tested before url:bugstar:7534395
		IF GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) >= eHORDESTAGE_SPECTATING
		AND GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) != eHORDESTAGE_SCTV
			NETWORK_DISABLE_REALTIME_MULTIPLAYER()
			PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - Calling NETWORK_DISABLE_REALTIME_MULTIPLAYER for Horde - bDisableRealtimeMPCheck_JIP_HORDE")
		ENDIF
	ELSE
		IF GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) = eHORDESTAGE_REWARDS_AND_FEEDACK
			NETWORK_DISABLE_REALTIME_MULTIPLAYER()
			PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - Calling NETWORK_DISABLE_REALTIME_MULTIPLAYER for Horde")
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_CLIENT()
    
	VECTOR vMin, vMax
	
	IF NOT bSetSctvTickerOn
		SET_SCTV_TICKER_SCORE_ON()
		bSetSctvTickerOn = TRUE
	ENDIf
	
    // Handle events wueue.
    PROCESS_EVENTS()
    
	// Player deaths stat.
	TRACK_LOCAL_PLAYER_DEATHS()
	
    // Processes if should respawn or go to spectator mode.
    PROCESS_RESPAWNING()
    
    // Mini leader board that is on dpad down.
    MAINTAIN_MINI_LEADER_BOARD()
   	
	// Enemy spec cam registration.
	//MAINTAIN_ENEMY_SPEC_CAM_REGISTRATION() //removed for 1623032
	
	// Tell players they cannot use vehicles during a survival.
	MAINTAIN_CANNOT_USE_VEHICLES_HELP()
	
	// Don't want ambient peds trying to take out the players.
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	
	// Challenge timer.
	MAINTAIN_CHALLENGE_TIMER()
	
	// Ticker to remind to kick leechers.
	// Don't want SCTV player to see ticker, they can't vote to kick.
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		MAINTAIN_MY_MIGHT_NOT_BE_PLAYING_STATE()
		PROCESS_KICK_LEECHERS_REMINDER_TICKER()
	ENDIF
	
	// Need paths for ai navigation.
	vMin = ( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[0].vLoc[0] - << 0.0, 0.0, 5.0 >> )
	vMax = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[1].vLoc[0]
	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vMin.x, vMin.y, vMax.x, vMax.y)
	
	// As soon as we have an elegant end of mode reason, save out the the player ugc data.
	IF NOT bSavedOutPlayerUgcData
		IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
		OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
			IF SAVE_OUT_UGC_PLAYER_DATA(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName, sSaveOutVars)
				bSavedOutPlayerUgcData = TRUE
				PRINTLN("[Survival] - saved out UGC player data.")
			ELSE
				PRINTLN("[Survival] - saving out UGC player data.")
			ENDIF
		ENDIF
	ENDIF
	
	REFRESH_DPAD_WHEN_DATA_CHANGES(iRefreshDpad, iStoredRefreshDpadValue)
	
	PROCESS_SPECTATOR_FLAGS()
	
	PROCESS_DISABLE_REALTIME_MULTIPLAYER()
	
	// Main switch.
    SWITCH GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID())

        CASE eHORDESTAGE_JIP_IN
            PROCESS_eHORDESTAGE_JIP_IN_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_SETUP_MISSION
            PROCESS_eHORDESTAGE_SETUP_MISSION_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_START_FADE_IN
            PROCESS_eHORDESTAGE_START_FADE_IN_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_FIGHTING
           	PROCESS_eHORDESTAGE_FIGHTING_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_PASSED_WAVE
            PROCESS_eHORDESTAGE_PASSED_WAVE_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
            PROCESS_eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_SPECTATING
            PROCESS_eHORDESTAGE_SPECTATING_CLIENT()
        BREAK
        
        //===Start Edit David G===
        CASE eHORDESTAGE_SCTV
            PROCESS_eHORDESTAGE_SCTV_CLIENT()
        BREAK
        //===End Edit David G===
        
        CASE eHORDESTAGE_REWARDS_AND_FEEDACK
            PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT()
        BREAK
        
    ENDSWITCH
    
ENDPROC

PROC MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG()
	
	BOOL bClear
	
	IF serverBD.sFightingParticipantsList.iNumFightingParticipants >= MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS
		IF NOT IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			SET_BIT(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - iNumFightingParticipants >= MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS - setting BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM.")
		ENDIF
	ENDIF
	
	IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_FIGHTING
	OR GET_SERVER_HORDE_STAGE() = eHORDESTAGE_PASSED_WAVE
	OR GET_SERVER_HORDE_STAGE() = eHORDESTAGE_SPECTATING
	OR GET_SERVER_HORDE_STAGE() = eHORDESTAGE_SCTV
		IF NOT IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			SET_BIT(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			#IF IS_DEBUG_BUILD
				IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_FIGHTING
					PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - server in stage eHORDESTAGE_FIGHTING")
				ENDIF
				IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_PASSED_WAVE
					PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - server in stage eHORDESTAGE_PASSED_WAVE")
				ENDIF
				IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_SPECTATING
					PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - server in stage eHORDESTAGE_SPECTATING")
				ENDIF
				IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_SCTV
					PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - server in stage eHORDESTAGE_SCTV")
				ENDIF
				PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - setting BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM.")
			#ENDIF
		ENDIF
	ENDIF
	
	IF bClear
		IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - bClear = TRUE - clearing BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM.")
		ENDIF
	ENDIF
	
ENDPROC



// *********************************** \\
// *********************************** \\
//                                     \\
// 				SERVER LOGIC           \\
//                                     \\
// *********************************** \\
// *********************************** \\

PROC PROCESS_eHORDESTAGE_JIP_IN_SERVER()
    SET_SERVER_HORDE_STAGE(eHORDESTAGE_SETUP_MISSION)
ENDPROC

PROC PROCESS_eHORDESTAGE_SETUP_MISSION_SERVER()
    
    IF LOAD_AND_CREATE_ALL_FMMC_ENTITIES_WK(serverBD.sFMMC_SBD, TRUE)
	
        SET_VEHICLE_MODELS_AS_NO_LONGER_NEEDED()
        SET_OBJECT_MODELS_AS_NO_LONGER_NEEDED()
        SET_PED_MODELS_AS_NO_LONGER_NEEDED()
        START_NET_TIMER(serverBD.ststartHordeTime)
        SET_SERVER_HORDE_STAGE(eHORDESTAGE_START_FADE_IN)
		
    ENDIF
    
ENDPROC

PROC PROCESS_eHORDESTAGE_START_FADE_IN_SERVER()
    
    // Once everyone has faded in, move onto the next wave stage.
    IF HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN()
		serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave = serverBD.sFightingParticipantsList.iNumFightingParticipants
		PRINTLN("[Survival] - call 1 - set serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave = serverBD.sFightingParticipantsList.iNumFightingParticipants, ", serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave)
    	SET_SERVER_HORDE_STAGE(eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE)
    ENDIF
    
ENDPROC


//INT iKillsThisWavebdTracking[MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS]
//INT iKillsThisWaveTotalBdTracking
//
//PROC TEMP()
//	
//	INT i, iPart
//	
//	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
//		
//		IF serverBD.sFightingParticipantsList.iParticipant[i] != (-1)
//			
//			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, serverBD.sFightingParticipantsList.iParticipant[i]))
//				
//				iPart = serverBD.sFightingParticipantsList.iParticipant[i]
//				
//				IF iKillsThisWavebdTracking[i] < playerBD[i].iMyKillsThisWave
//					
//					iKillsThisWavebdTracking[i] = playerBD[i].iMyKillsThisWave
//					
//				ENDIF
//				
//			ENDIF
//		
//		ENDIF
//		
//	ENDREPEAT
//	
//	iKillsThisWaveTotalBdTracking = iKillsThisWavebdTracking[0] + iKillsThisWavebdTracking[1] = iKillsThisWavebdTracking[2] = iKillsThisWavebdTracking[3]
//	
//ENDPROC

PROC PROCESS_eHORDESTAGE_FIGHTING_SERVER()
    
    // S and F skips for quick pass/fail.
    #IF IS_DEBUG_BUILD
    MAINTAIN_S_AND_F_SKIPS()
    #ENDIF
    
    // Process ped decisions.
    PROCESS_ENEMY_PED_BRAINS()
    
    // Helicopters.
    MAINTAIN_SURVIVAL_HELIS_SERVER()
    
	// Enemies arriving on bikes.
	MAINTAIN_SURVIVAL_LAND_VEHICLES_SERVER()
	
	// Update the wave stage. This used in the squad spawning to build up the enemy numbers as the wave progresses.
    MAINTAIN_WAVE_PROGRESSION()
	
	// In case vehicles get stuck outside of the survival.
	MAINTAIN_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_FAILSAFE()
	
	// Check for end of wave. If we have killed everything then we win.
    IF (serverBD.iNumAliveSquadEnemies <= 0)
		IF (serverBD.iNumAliveHelis <= 0)
			IF (serverBD.iNumAliveLandVehiclePeds <= 0)
				IF (serverBD.iKillsThisWave >= serverBD.sWaveData.iNumRequiredKills)
//				OR (serverBD.iKillsThisWave >= (serverBD.sWaveData.iNumRequiredKills-1))
//					#IF IS_DEBUG_BUILD
//						IF (serverBD.iKillsThisWave >= (serverBD.sWaveData.iNumRequiredKills-1))
//							PRINTLN("[Survival] - having to force end of wave, 1 ped out - iKillsThisWave = ", serverBD.iKillsThisWave, ", iNumRequiredKills = ", serverBD.sWaveData.iNumRequiredKills)
//						ENDIF
//					#ENDIF
	                SET_BIT(serverBd.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
	            ENDIF
			ENDIF
        ENDIF
    ENDIF
	
	// Fail safes for enemies getting stuck or not spawning. 
	
	// Vehicle peds.
	IF serverBd.bAllVehiclePedsStuckOutOfArea
		#IF IS_DEBUG_BUILD
			PRINTLN("[Survival] - bAllVehiclePedsStuckOutOfArea = TRUE, forcing end of wave.")
		#ENDIF
        SET_BIT(serverBd.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
	ENDIF
	
	// On foot peds.
	IF (serverBD.iNumAliveSquadEnemies <= 0)
		IF (serverBD.iNumAliveHelis <= 0)
			IF (serverBD.iNumAliveLandVehiclePeds <= 0)
				
				IF (serverBd.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] < serverBd.sWaveData.iNumRequiredSubstageKills[serverBD.sWaveData.eStage])
				OR (serverBD.iKillsThisWave < serverBD.sWaveData.iNumRequiredKills)
					
					IF NOT HAS_NET_TIMER_STARTED(serverBD.stNoSpawningEnemiesTimer)
						
						PRINTLN("[Survival] - starting no enemies spawn failsafe.")
						
						IF (serverBD.iNumKillsThisWaveFromEvents > 0) 
						AND (serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] > 0)
							
							PRINTLN("[Survival] - (serverBD.iNumKillsThisWaveFromEvents > 0) ")
							
							IF (serverBD.iNumKillsThisWaveFromEvents > serverBD.iKillsThisWave)
								PRINTLN("[Survival] - serverBD.iKillsThisWave = ", serverBD.iKillsThisWave, ", serverBD.iNumKillsThisWaveFromEvents = ", serverBD.iNumKillsThisWaveFromEvents)
								serverBD.iKillsThisWave = serverBD.iNumKillsThisWaveFromEvents
								PRINTLN("[Survival] - (serverBD.iNumKillsThisWaveFromEvents > serverBD.iKillsThisWave), setting (serverBD.iKillsThisWave = serverBD.iNumKillsThisWaveFromEvent)")
								PRINTLN("[Survival] - serverBD.iKillsThisWave = ", serverBD.iKillsThisWave, ", serverBD.iNumKillsThisWaveFromEvents = ", serverBD.iNumKillsThisWaveFromEvents)
							ENDIF
							
							IF serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] > serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage]
		//						PRINTLN("[Survival] - serverBD.iKillsThisWaveSubStage = ", serverBD.iKillsThisWaveSubStage, ", serverBD.iNumKillsThisSubStageFromEvents = ", serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage])
								serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] = serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage]
								PRINTLN("[Survival] - (serverBD.iNumKillsThisSubStageFromEvents > serverBD.iKillsThisWaveSubStage), setting (serverBD.iKillsThisWaveSubStage = serverBD.iNumKillsThisSubStageFromEvents)")
		//						PRINTLN("[Survival] - serverBD.iKillsThisWaveSubStage = ", serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage], ", serverBD.iNumKillsThisSubStageFromEvents = ", serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage])
							ENDIF
							
						ENDIF
						
						START_NET_TIMER(serverBD.stNoSpawningEnemiesTimer)
						
					ENDIF
				ELSE
					RESET_NET_TIMER(serverBD.stNoSpawningEnemiesTimer)
				ENDIF
			ELSE
				RESET_NET_TIMER(serverBD.stNoSpawningEnemiesTimer)
			ENDIF
		ELSE
			RESET_NET_TIMER(serverBD.stNoSpawningEnemiesTimer)
		ENDIF
	ELSE
		RESET_NET_TIMER(serverBD.stNoSpawningEnemiesTimer)
	ENDIF
	
	// On foot peds.
	IF (serverBD.iNumAliveSquadEnemies <= 0)
		IF (serverBD.iNumAliveHelis <= 0)
			IF (serverBD.iNumAliveLandVehiclePeds <= 0)
				
				IF HAS_NET_TIMER_STARTED(serverBD.stNoSpawningEnemiesTimer)
					IF HAS_NET_TIMER_EXPIRED(serverBD.stNoSpawningEnemiesTimer, 20000)
						
						PRINTLN("[Survival] - no enemies have spawned for 20seconds, setting sub stage kill count to the same as reuired kills.")
						
						serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] = serverBd.sWaveData.iNumRequiredSubstageKills[serverBD.sWaveData.eStage]
						serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] = serverBd.sWaveData.iNumRequiredSubstageKills[serverBD.sWaveData.eStage]
						
						IF serverBD.sWaveData.iWaveCount <= 3
							IF serverBd.sWaveData.eStage = eWAVESTAGE_MEDIUM
								serverBD.iNumKillsThisWaveFromEvents = serverBD.sWaveData.iNumRequiredKills
								serverBD.iKillsThisWave = serverBD.sWaveData.iNumRequiredKills
							ENDIF
						ELSE
							IF serverBd.sWaveData.eStage = eWAVESTAGE_HARD
								serverBD.iNumKillsThisWaveFromEvents = serverBD.sWaveData.iNumRequiredKills
								serverBD.iKillsThisWave = serverBD.sWaveData.iNumRequiredKills
							ENDIF
						ENDIF
					ENDIF
					IF HAS_NET_TIMER_EXPIRED(serverBD.stNoSpawningEnemiesTimer, 45000)
						#IF IS_DEBUG_BUILD
							PRINTLN("[Survival] - no enemies have spawned for 30 seconds when we are supposed to have more, forcing end of wave.")
						#ENDIF
			            SET_BIT(serverBd.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
    // Once the current wave is defeated, move onto passed wave stage.
    IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
        IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
        #IF IS_DEBUG_BUILD
        OR serverBD.bSkipWave
        #ENDIF
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(stEndOfWaveDelay, 1000) //1623906 - Request to reduce delay from 2 seconds to 1 second.
	            PRINTLN("[Survival] - BITSET_SERVER_CURRENT_WAVE_DEFEATED bit is set, going to eHORDESTAGE_PASSED_WAVE.")
				RESET_NET_TIMER(stEndOfWaveDelay)
				RESET_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_TIMER()
				RESET_NET_TIMER(serverBD.stNoSpawningEnemiesTimer)
				CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
	            SET_SERVER_HORDE_STAGE(eHORDESTAGE_PASSED_WAVE)
			ENDIF
        ENDIF
    ENDIF
    
    // If all players lost all lives, set end reason and go to end stage.
    IF bAllPlayersLostAllLivesFlag
		PRINTLN("[Survival] - bAllPlayersLostAllLivesFlag = TRUE.")
        SET_END_HORDE_REASON(eENDHORDEREASON_ALL_PLAYERS_DEAD)
		serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE
		RESET_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_TIMER()
		RESET_NET_TIMER(serverBD.stNoSpawningEnemiesTimer)
        SET_SERVER_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
   	ENDIF
	
ENDPROC

PROC PROCESS_eHORDESTAGE_PASSED_WAVE_SERVER()
    
    // S and F skips for quick pass/fail.
    #IF IS_DEBUG_BUILD
    MAINTAIN_S_AND_F_SKIPS()
    REMOVE_PREVIOUS_WAVE_ENTITIES_DEBUG()
    #ENDIF
    
	BOOL bCleanedEnemies = CLEANUP_ALL_ENEMIES()
	
    IF HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD()
		IF bCleanedEnemies
	        #IF IS_DEBUG_BUILD
	        IF NOT serverBD.bSkipWave
	        #ENDIF
	        serverBD.iKillsThisWave = 0
	        IF serverBD.sWaveData.iWaveCount < NUMBER_HORDE_WAVES
                INCREMENT_WAVE()
				CLEAR_BIT(serverBd.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
				serverBd.iNumKillsThisWaveFromEvents = 0
				serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_EASY] = 0
				serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_MEDIUM] = 0
				serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_HARD] = 0
				SET_SERVER_HORDE_STAGE(eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE)
            ELSE
                SET_END_HORDE_REASON(eENDHORDEREASON_BEAT_ALL_WAVES)
				serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE
                SET_SERVER_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
            ENDIF
	        #IF IS_DEBUG_BUILD
	        ENDIF
	        #ENDIF
		ENDIF
    ENDIF
    
ENDPROC

PROC PROCESS_eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE_SERVER()
    
    // S and F skips for quick pass/fail.
    #IF IS_DEBUG_BUILD
    MAINTAIN_S_AND_F_SKIPS()
    REMOVE_PREVIOUS_WAVE_ENTITIES_DEBUG()
    #ENDIF
    
    SWITCH serverBD.sDelayBeforeNextWaveStageData.eStage
        
        CASE eDELAYBEFORENEXTWAVESTAGE_ADD_NEW_FIGHTING_PARTICIPANTS
          	
            // If we have data, move onto wait for fade in stage.
            IF serverBD.sFightingParticipantsList.bListBeenInitialised
                PRINTLN("[Survival] - going to delay before next wave stage eDELAYBEFORENEXTWAVESTAGE_WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN.")
                serverBD.sDelayBeforeNextWaveStageData.eStage = eDELAYBEFORENEXTWAVESTAGE_WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN
            ENDIF
            
        BREAK
        
        CASE eDELAYBEFORENEXTWAVESTAGE_WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN
            
            // If doing first countdown at beginning of mode, skip through.
            IF serverBD.sWaveData.iWaveCount = 1
                
                PRINTLN("[Survival] - wave = 1, setting BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN.")
                SET_BIT(serverBD.iBitset, BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN)
                
            ELSE
                    
                // Wait for all fighting participants to have spawned and faded in.
                IF NOT IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN)
                    IF HAVE_ALL_FIGHTING_PARTICIPANTS_SPAWNED_FADED_IN()
                        PRINTLN("[Survival] - all fighting participants have respawned and faded in, setting BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN.")
                        SET_BIT(serverBD.iBitset, BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN)
                    ENDIF
                ENDIF
                    
            ENDIF
            
			// Go to the countdown if we're ready for it.
            IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN)
                PRINTLN("[Survival] - BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN is set, going to delay before next wave stage eDELAYBEFORENEXTWAVESTAGE_DO_COUNT_DOWN.")
                serverBD.sDelayBeforeNextWaveStageData.eStage = eDELAYBEFORENEXTWAVESTAGE_DO_COUNT_DOWN
            ENDIF
            
        BREAK
        
        CASE eDELAYBEFORENEXTWAVESTAGE_DO_COUNT_DOWN
            
			IF NOT HAS_NET_TIMER_STARTED(jobUnderTenSecondsTimer)
				START_NET_TIMER(jobUnderTenSecondsTimer)
				PRINTLN("[Survival] - jobUnderTenSecondsTimer has been started.")
			ENDIF
			
			// Wave countdown.
            IF NOT HAS_NET_TIMER_STARTED(serverBD.stNextWaveDelay)
			
                PRINTLN("[Survival] - starting countdown to next wave timer.")
                START_NET_TIMER(serverBD.stNextWaveDelay)
				
            ELSE
				
				// If we have finished the countdown.
                IF HAS_NET_TIMER_EXPIRED(serverBD.stNextWaveDelay, DELAY_BETWEEN_WAVES)
				
                    PRINTLN("[Survival] - countdown to next wave timer has expired.")
					
                    #IF IS_DEBUG_BUILD
                    IF NOT serverBD.bSkipWave
                    #ENDIF
					
					// Setup the next wave.
					serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave = serverBD.sFightingParticipantsList.iNumFightingParticipants
					PRINTLN("[Survival] - call 2 - set serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave = serverBD.sFightingParticipantsList.iNumFightingParticipants, ", serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave)
                    RESET_NET_TIMER(serverBD.stNextWaveDelay)
                    SETUP_NEXT_WAVE()
                    CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN)
                    serverBD.sDelayBeforeNextWaveStageData.eStage = eDELAYBEFORENEXTWAVESTAGE_ADD_NEW_FIGHTING_PARTICIPANTS
                    SET_SERVER_HORDE_STAGE(eHORDESTAGE_FIGHTING)
					
                    #IF IS_DEBUG_BUILD
                    ENDIF
                    #ENDIF
					
                ENDIF
            ENDIF
            
        BREAK
        
    ENDSWITCH
    
ENDPROC

PROC PROCESS_eHORDESTAGE_SPECTATING_SERVER()
    
    // Nothing.
    
ENDPROC

PROC PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_SERVER()
    
    IF SHOULD_GO_TO_WAITING_TO_LEAVE_STATE()
        SET_SERVER_GAME_STATE(SURVIVAL_GAME_STATE_WAITING_TO_LEAVE)
    ELSE
        SET_SERVER_GAME_STATE(SURVIVAL_GAME_STATE_END)
    ENDIF
    
ENDPROC

PROC PROCESS_SERVER()
    
    SWITCH GET_SERVER_HORDE_STAGE()

        CASE eHORDESTAGE_JIP_IN
            PROCESS_eHORDESTAGE_JIP_IN_SERVER()
        BREAK
        
        CASE eHORDESTAGE_SETUP_MISSION
            PROCESS_eHORDESTAGE_SETUP_MISSION_SERVER()
        BREAK
        
        CASE eHORDESTAGE_START_FADE_IN
            PROCESS_eHORDESTAGE_START_FADE_IN_SERVER()
        BREAK
        
        CASE eHORDESTAGE_FIGHTING
    		
            PROCESS_eHORDESTAGE_FIGHTING_SERVER()
            
            #IF IS_DEBUG_BUILD
            #IF SCRIPT_PROFILER_ACTIVE 
                ADD_SCRIPT_PROFILE_MARKER("PROCESS_eHORDESTAGE_FIGHTING_SERVER")
            #ENDIF
            #ENDIF
            
        BREAK
        
        CASE eHORDESTAGE_PASSED_WAVE
            PROCESS_eHORDESTAGE_PASSED_WAVE_SERVER()
        BREAK
        
        CASE eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
            PROCESS_eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE_SERVER()
        BREAK
        
        CASE eHORDESTAGE_SPECTATING
            PROCESS_eHORDESTAGE_SPECTATING_SERVER()
        BREAK
        
        CASE eHORDESTAGE_REWARDS_AND_FEEDACK
            PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_SERVER()
        BREAK
        
    ENDSWITCH
    
ENDPROC

// *********************************** \\
// *********************************** \\
//                                     \\
//				 MAIN LOOP             \\
//                                     \\
// *********************************** \\
// *********************************** \\

BOOL bDidJipSpecFade

FUNC BOOL IsSwoopDownAlmostFinished()
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS() 
		IF GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
			IF GET_PLAYER_SWITCH_STATE() > SWITCH_STATE_OUTRO_HOLD
				RETURN(TRUE)
			ENDIF
		ENDIF
	ELSE
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
	
ENDFUNC

/// PURPOSE:
///    Main loop.
/// PARAMS:
///    HordeMissionData - mission data.
SCRIPT(MP_MISSION_DATA HordeMissionData)
    
	// Densities.
	FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME()
	
    // Carry out all the initial game starting duties. 
    IF NETWORK_IS_GAME_IN_PROGRESS()
        IF NOT PROCESS_PRE_GAME(HordeMissionData)
			PRINTLN("[Survival] - Failed to receive an initial network broadcast. Calling SCRIPT_CLEANUP.")
			SCRIPT_CLEANUP()
		ENDIF
    ENDIF
	
//	Refresh_MP_Script_Tunables(TUNE_CONTEXT_FM_SURVIVAL)
	
    // Main while loop.
    WHILE TRUE
        
        // One wait to rule them all. This can be the ONLY wait from here on in...
        MP_LOOP_WAIT_ZERO() 
        
		// B*1554155.
		IF serverBD.iTimeOfDay > -1
			SET_TIME_OF_DAY(serverBD.iTimeOfDay)
		ENDIF
		
		// Densities.
		FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME()
		
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE
        SCRIPT_PROFILER_START_OF_FRAME()
        #ENDIF
        #ENDIF  
        
        // If we have a match end event, bail.
        IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[Survival] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE returning TRUE, calling SCRIPT_CLEANUP.")
			DO_MATCH_END()
            SCRIPT_CLEANUP()
        ENDIF
        IF SHOULD_PLAYER_LEAVE_MP_MISSION(TRUE) 
			PRINTLN("[Survival] - SHOULD_PLAYER_LEAVE_MP_MISSION returning TRUE, calling SCRIPT_CLEANUP.")
			IF SHOULD_I_PROCESS_END_HORDE_STATS()
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_HORDELVL) > GET_WAVE_SURVIVED_UNTIL()
					SET_MP_INT_CHARACTER_STAT(MP_STAT_HORDELVL, GET_WAVE_SURVIVED_UNTIL())
				ENDIF
			ENDIF
			DO_MATCH_END()
            SCRIPT_CLEANUP()
        ENDIF
        
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
            ADD_SCRIPT_PROFILE_MARKER("SCRIPT_CLEANUP")
        #ENDIF
        #ENDIF
		
		DO_OUTROS()
		#IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
            ADD_SCRIPT_PROFILE_MARKER("SCRIPT_CLEANUP")
        #ENDIF
        #ENDIF
		
		IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, serverBD.sServerFMMC_EOM.iVoteStatus)
			IF NOT READY_TO_MOVE_TO_NEXT_JOBS(nextJobStruct)
				DO_END_JOB_VOTING_SCREEN(g_sMC_serverBDEndJob, nextJobStruct, g_sMC_serverBDEndJob.timerVotingEnds.Timer)			
				#IF IS_DEBUG_BUILD
		        #IF SCRIPT_PROFILER_ACTIVE 
		            ADD_SCRIPT_PROFILE_MARKER("CLIENT_JOB_VOTE_GRAB_IMAGES")
		        #ENDIF
		        #ENDIF
			ENDIF
		ENDIF
        
        // Deal with the debug
        #IF IS_DEBUG_BUILD              
            MAINTAIN_WIDGETS()
        #ENDIF
        
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
        OPEN_SCRIPT_PROFILE_MARKER_GROUP("servers stuff")
        #ENDIF
        #ENDIF
		
		IF GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT()) = SURVIVAL_GAME_STATE_INIT
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		ENDIF
		
        IF NETWORK_IS_GAME_IN_PROGRESS()
    
            // -----------------------------------
            // Process client game logic    
            REINIT_NET_TIMER(tdNetTimer)
            
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())						//This MAINTAIN_SPECTATOR is just for player specating, not SCTV
				MAINTAIN_SPECTATOR(specData)
				#IF IS_DEBUG_BUILD
		        #IF SCRIPT_PROFILER_ACTIVE 
		            ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPECTATOR")
		        #ENDIF
		        #ENDIF
			ENDIF
			
			// Participant loops.
            MAINTAIN_PARTICPANT_LOOP()
					
	        #IF IS_DEBUG_BUILD
	        #IF SCRIPT_PROFILER_ACTIVE 
	            ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_STAGGERED_PARTICPANT_LOOP")
	        #ENDIF
	        #ENDIF
			
//			// Sort radar zoom level.
			CONTROL_RADAR_ZOOM()
			
			// For pre-loading the celebration where possible.
			IF GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET
				TRIGGER_CELEBRATION_PRE_LOAD(sCelebrationData, TRUE)
			ENDIF
			MAINTAIN_CELEBRATION_PRE_LOAD(sCelebrationData)
		
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				TRACK_NUM_ALIVE_LAND_VEHICLE_PEDS()
			ENDIF
			
//			// Maintain showing overheads through out job intro.
//			IF GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT()) < SURVIVAL_GAME_STATE_RUNNING
//				IF DOES_CAM_EXIST(jobIntroData.jobIntroCam)
//					IF IS_CAM_ACTIVE(jobIntroData.jobIntroCam)
//						SET_ON_JOB_INTRO(TRUE)
//					ELSE
//						SET_ON_JOB_INTRO(FALSE)
//					ENDIF
//				ELSE
//					SET_ON_JOB_INTRO(FALSE)
//				ENDIF
//			ELSE
//				SET_ON_JOB_INTRO(FALSE)
//			ENDIF
			
			// Main switch.
            SWITCH GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT())
                                        
                // Wait until the server gives the all go before moving on.
                CASE SURVIVAL_GAME_STATE_INIT
					
					// Request anims.
                    REQUEST_DM_ANIMS()
					
					// Don;t want hud.
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					
					// Add busy spinner while doing setup.
					IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
						IF NOT BUSYSPINNER_IS_ON() //*1600246
							BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD")
							END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
							PRINTLN("[Survival] - BUSY SPINNER ON")
						ENDIF
					ENDIF
					
					// Play anim post fx.
					IF IS_CORONA_READY_TO_START_WITH_JOB()
					AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
						IF NOT IS_CORONA_FREEZE_AND_START_FX_SET()
							SET_HIDOF_OVERRIDE(TRUE,FALSE,0,0,0,0)
							ANIMPOSTFX_PLAY("MP_job_load", 0, TRUE)
						ENDIF
					ENDIF
					
					// If the server hasmoved beyond init.
                    IF (GET_SERVER_GAME_STATE() > SURVIVAL_GAME_STATE_INIT)
						// If I am alive.
                        IF IS_NET_PLAYER_OK(PLAYER_ID())
							// Load Horde text.
						    IF LOAD_HORDE_TEXT_BLOCK()
						        // Create props.
						        IF CREATE_FMMC_PROPS_MP(oiHordeProp, oihordePropChildren)
						            // Create guns, armour and health.
						            IF CREATE_PICKUPS()
										// Say I am ready for start fade in.
										IF NOT bSetupInitialSpawnPos
											SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(TRUE)
											bSetupInitialSpawnPos = TRUE
										ELSE
											IF DID_I_JOIN_MISSION_AS_SPECTATOR()
											OR IS_PLAYER_SCTV(PLAYER_ID())
												IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL
												OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_TERMINATE_MAINTRANSITION
												OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_FM_SWOOP_DOWN
													bContinueToCut = TRUE
												ELSE
													#IF IS_DEBUG_BUILD
													CPRINTLN(DEBUG_SPECTATOR, 	"=== SURVIVAL === Not moving to spectate as GET_CURRENT_TRANSITION_STATE = ", 
																				GET_TRANSITION_STATE_STRING(GET_CURRENT_TRANSITION_STATE()))
													#ENDIF
												ENDIF
											ELSE
												IF WARP_TO_START_POSITION(iWarpToStartPosStage)
													bContinueToCut = TRUE
												ENDIF
											ENDIF
									        IF bContinueToCut
											
												// Get spectator mode runnin as early as possible.
												IF bDidJipSpecFade
													SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
													CLEAR_IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA()
													SET_MG_READY_FOR_TRANSITION_STATE_FM_SWOOP_DOWN()
													IF DID_I_JOIN_MISSION_AS_SPECTATOR()
													OR IS_PLAYER_SCTV(PLAYER_ID())
														PRINTLN("[Survival] - DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE")
														DO_SCREEN_FADE_OUT(500)
														IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
															PRINTLN("[Survival] - IS_THIS_SPECTATOR_CAM_ACTIVE = FALSE")
															IF IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
																PRINTLN("[Survival] - IS_SAFE_TO_TURN_ON_SPECTATOR_CAM = TRUE")
																CLEANUP_ALL_CORONA_FX(TRUE) 
								           						SET_SKYFREEZE_CLEAR()
																IF ANIMPOSTFX_IS_RUNNING("MP_job_load")
																	ANIMPOSTFX_STOP("MP_job_load")
																ENDIF
																SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
																ACTIVATE_SPECTATOR_CAM( specData, SPEC_MODE_PLAYERS_STAY_DEAD_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_QUIT_SCREEN|ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED )
																
																bDidJipSpecFade = TRUE
													        ENDIF
														ENDIF
													ENDIF
												ENDIF
					
												IF IS_CORONA_READY_TO_START_WITH_JOB()
												OR DID_I_JOIN_MISSION_AS_SPECTATOR()
												OR IS_PLAYER_SCTV(PLAYER_ID())
													IF (g_myStartingWeapon = WEAPONTYPE_INVALID)
														GIVE_PLAYER_STARTING_MISSION_WEAPONS(WEAPONINHAND_LASTWEAPON_BOTH)
														PRINTLN("[Survival] - giving player WEAPONINHAND_LASTWEAPON_BOTH.")
													ELSE
														SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), g_myStartingWeapon, TRUE)
														PRINTLN("[Survival] - giving player g_myStartingWeapon - ", GET_WEAPON_NAME(g_myStartingWeapon))
													ENDIF
													CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND)
													SET_CLIENT_GAME_STATE(SURVIVAL_GAME_FADE_IN)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
                        ENDIF
                    ENDIF
                    
                BREAK
				
				CASE SURVIVAL_GAME_FADE_IN
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					IF HAS_EVERYONE_REACHED_TEAMCUTSCENE()
						//SET_CLIENT_GAME_STATE(SURVIVAL_GAME_TEAMCUT_CUTSCENE)
						g_b_TransitionActive = FALSE
						SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_FAKE_PED_CLEANUP)
						IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
						AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
						ENDIF
						PLAY_SOUND_FRONTEND(-1,"Hit","RESPAWN_SOUNDSET")
						SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(FALSE)
						IF NOT IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
						AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
							PRINTLN("[Survival] - going to state SURVIVAL_GAME_INTRO_CUTSCENE.")
							SET_CLIENT_GAME_STATE(SURVIVAL_GAME_INTRO_CUTSCENE)
						ELSE
							#IF IS_DEBUG_BUILD
							IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
								PRINTLN("[Survival] - SHOULD_PARTICIPANT_GO_STRAIGHT_TO_SPECTATOR_CAMERA = TRUE.")
							ENDIF
							
							IF DID_I_JOIN_MISSION_AS_SPECTATOR()
								PRINTLN("[Survival] - DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE.")
							ENDIF
							#ENDIF
							IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
								IF IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
									SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
									ACTIVATE_SPECTATOR_CAM( specData, SPEC_MODE_PLAYERS_STAY_DEAD_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_QUIT_SCREEN|ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED )
						        ENDIF
							ENDIF
							
							SET_IDLE_KICK_TIME_OVERRIDDEN(180000)
							
							PRINTLN("[Survival] - going to state SURVIVAL_GAME_STATE_RUNNING.")
							SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_RUNNING)
						ENDIF
					ENDIF
				BREAK
                
				CASE SURVIVAL_GAME_INTRO_CUTSCENE

					HIDE_HUD_AND_RADAR_THIS_FRAME()
					HIDE_PLAYER(FALSE)
					
					//CLEANUP_ALL_CORONA_FX(FALSE)
					
					IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
					OR DID_I_JOIN_MISSION_AS_SPECTATOR()
						
						#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
							PRINTLN("[Survival] - SHOULD_PARTICIPANT_GO_STRAIGHT_TO_SPECTATOR_CAMERA = TRUE.")
						ENDIF
						
						IF DID_I_JOIN_MISSION_AS_SPECTATOR()
							PRINTLN("[Survival] - DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE.")
						ENDIF
						#ENDIF
						
						// Get spectator mode runnin as early as possible.
						IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
							IF IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
								SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
								ACTIVATE_SPECTATOR_CAM( specData, SPEC_MODE_PLAYERS_STAY_DEAD_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_QUIT_SCREEN|ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED )
					        ENDIF
						ENDIF
						
						SET_IDLE_KICK_TIME_OVERRIDDEN(180000)
						
						PRINTLN("[Survival] - going to state SURVIVAL_GAME_STATE_RUNNING.")
						SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_RUNNING)
						
					ELSE
						
						IF g_b_OnRaceIntro
							SET_SKYFREEZE_CLEAR(TRUE)
						ENDIF
						
	                   	IF PROCESS_DEATHMATCH_STARTING_CUT(jobIntroData)//PROCESS_JOB_OVERVIEW_CUT(jobIntroData)
							//CLEANUP_JOB_INTRO_CUTSCENE(jobIntroData) 
							//SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							//CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,FMMC_TYPE_SURVIVAL,TRUE) // Also called in DO_TUTORIAL_MISSION_CUTSCENE()
							PRINTLN("[Survival] - PROCESS_DEATHMATCH_STARTING_CUT = TRUE.")
							PRINTLN("[Survival] - going to state SURVIVAL_GAME_INTRO_CAMERA_BLEND.")
							SET_ON_JOB_INTRO(FALSE)
							SET_CLIENT_GAME_STATE(SURVIVAL_GAME_INTRO_CAMERA_BLEND)
						ELSE
							SET_ON_JOB_INTRO(TRUE)
						ENDIF
						
					ENDIF
					
                BREAK
				
				CASE SURVIVAL_GAME_INTRO_CAMERA_BLEND
				
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					HIDE_PLAYER(FALSE)
					
					IF PROCESS_DEATHMATCH_BLEND_OUT(jobIntroData)
						CLEANUP_ALL_CORONA_FX(FALSE)
						SET_SKYFREEZE_CLEAR()
						PLAY_SOUND_FRONTEND(-1,"Hit","RESPAWN_SOUNDSET")
						ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE)
						CLEANUP_JOB_INTRO_CUTSCENE(jobIntroData) 
						CLEANUP_NEW_ANIMS(jobIntroData)
						CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,FMMC_TYPE_SURVIVAL,TRUE) // Also called in DO_TUTORIAL_MISSION_CUTSCENE()
						CLEANUP_CELEBRATION_SCALEFORMS(jobIntroData)
						IF IS_PLAYLIST_SETTING_CHALLENGE_TIME()
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMMC_PL_STCL")
								PRINT_HELP("FMMC_PL_STCL",6000)
							ENDIF
						ENDIF
						SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
						SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_RUNNING)
					ENDIF
				
				BREAK
				
                // Main gameplay state.
                CASE SURVIVAL_GAME_STATE_RUNNING                
                    
                    #IF IS_DEBUG_BUILD
                    #IF SCRIPT_PROFILER_ACTIVE 
                        ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PARTICPANT_LOOP")
                    #ENDIF
                    #ENDIF
					
                    // Process client logic.
                    PROCESS_CLIENT()
					
                    #IF IS_DEBUG_BUILD
                    #IF SCRIPT_PROFILER_ACTIVE 
                        ADD_SCRIPT_PROFILE_MARKER("PROCESS_CLIENT")
                    #ENDIF
                    #ENDIF
                    
//                    // If the server says the game's over, then I should think that too
//                    IF GET_SERVER_GAME_STATE()= SURVIVAL_GAME_STATE_END
//                        SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_END)
//                        PRINTLN("[Survival] - Moving client to SURVIVAL_GAME_STATE_END because server in SURVIVAL_GAME_STATE_END)")
//                    ENDIF   
//					
                    #IF IS_DEBUG_BUILD
                    #IF SCRIPT_PROFILER_ACTIVE 
                        ADD_SCRIPT_PROFILE_MARKER("GET_SERVER_GAME_STATE()= SURVIVAL_GAME_STATE_END")
                    #ENDIF
                    #ENDIF
            
                BREAK   
                
                CASE SURVIVAL_GAME_STATE_WAITING_TO_LEAVE 
                        
//                    // Process full part loop.
//                    MAINTAIN_PARTICPANT_LOOP()
//                    
//                    SET_MISSION_FINISHED(serverBD.TerminationTimer)
//                    
//                    IF IS_MISSION_READY_TO_CLEANUP(serverBD.TerminationTimer)
                        SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_END)
//                    ENDIF
                        
                BREAK
                
                CASE SURVIVAL_GAME_STATE_LEAVE  
                        
                    SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_END)
                        
                FALLTHRU

                CASE SURVIVAL_GAME_STATE_END         
					PRINTLN("[Survival] - in SURVIVAL_GAME_STATE_END, calling SCRIPT_CLEANUP.")
                    SCRIPT_CLEANUP()
                BREAK
                
                DEFAULT 
                    #IF IS_DEBUG_BUILD
                        SCRIPT_ASSERT("DM: Problem in SWITCH GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT())") 
                    #ENDIF
                BREAK
				
            ENDSWITCH
			
        ENDIF
		
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
        CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
        #ENDIF
        #ENDIF
		
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
            ADD_SCRIPT_PROFILE_MARKER("process main game state")
        #ENDIF
        #ENDIF
        
        // -----------------------------------
        // Process server game logic    
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
        OPEN_SCRIPT_PROFILE_MARKER_GROUP("server logic")
        #ENDIF
        #ENDIF
		
        IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
    
            #IF IS_DEBUG_BUILD
                NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER stuff being called by machine that is not host! Host only command!")
            #ENDIF
			
			// Do new participants go straight to the spectator camera?
			MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG()
			
			// Maintain list of players taking part in script and their current roles on it.
			MAINTAIN_FIGHTING_PARTICIPANTS_LIST()
			
			// This sets up the value that is used in many loops. 
			MAINTAIN_NUM_FIGHTING_PARTICIPANTS()
			
			PROCESS_EOM_RESTART_VOTE_LOGIC()
            
            SWITCH GET_SERVER_GAME_STATE()
                
                CASE SURVIVAL_GAME_STATE_INIT                   
                    SET_SERVER_GAME_STATE(SURVIVAL_GAME_STATE_RUNNING)
                BREAK
                
                CASE SURVIVAL_GAME_STATE_RUNNING                
                        
                    // Process server logic.
                    PROCESS_SERVER()
					
                    #IF IS_DEBUG_BUILD
                    #IF SCRIPT_PROFILER_ACTIVE 
                        ADD_SCRIPT_PROFILE_MARKER("PROCESS_SERVER")
                    #ENDIF
                    #ENDIF
                    
                    // Look for server end conditions
                    IF HAVE_END_GAME_CONDITIONS_BEEN_MET()
                        SET_SERVER_GAME_STATE(SURVIVAL_GAME_STATE_END)
                    ENDIF  
					
                    #IF IS_DEBUG_BUILD
                    #IF SCRIPT_PROFILER_ACTIVE 
                        ADD_SCRIPT_PROFILE_MARKER("Look for server end conditions")
                    #ENDIF
                    #ENDIF
                    
                BREAK
                
                CASE SURVIVAL_GAME_STATE_WAITING_TO_LEAVE
            		
                    IF ARE_ALL_PARTICIPANTS_WAITING_TO_LEAVE()
                        serverBD.iServerGameState = SURVIVAL_GAME_STATE_END
                    ENDIF
                    
                BREAK
                
                CASE SURVIVAL_GAME_STATE_END 

                BREAK   
                
                DEFAULT 
                    #IF IS_DEBUG_BUILD
                        SCRIPT_ASSERT("MC: Problem in SWITCH GET_SERVER_GAME_STATE()") 
                    #ENDIF
                BREAK
                    
            ENDSWITCH
        ENDIF
		
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
        CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
        #ENDIF
        #ENDIF
		
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
           	ADD_SCRIPT_PROFILE_MARKER("server processing")
        #ENDIF
        #ENDIF
        
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE
        SCRIPT_PROFILER_END_OF_FRAME()
        #ENDIF
        #ENDIF  
        
    ENDWHILE
	
ENDSCRIPT
