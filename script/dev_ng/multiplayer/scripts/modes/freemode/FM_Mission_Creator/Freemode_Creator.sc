//////////////////////////////////////////////////////////////
// Name: Freemode_Creator.sc								//
// Description: Placement tool for Freemode Missions		//
// Written by:  Jack Thallon								//
// Date: 25/02/2020											//
//////////////////////////////////////////////////////////////

#IF IS_FINAL_BUILD
script
endscript
#ENDIF

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_camera.sch" 
USING "FMMC_header.sch"
USING "FMMC_keyboard.sch"
USING "FMMC_MP_In_Game_Menu.sch"
USING "FM_Tutorials.sch"
USING "PM_MissionCreator_Public.sch"
USING "rc_helper_functions.sch"
USING "profiler.sch"
USING "Freemode_Creator_Creation.sch"
USING "net_peds_script_management.sch"

FMMC_LOCAL_STRUCT 						sFMMCdata
FMMC_CAM_DATA 							sCamData
TEAM_SPAWN_CREATION_STRUCT				sTeamSpawnStruct[FMMC_MAX_TEAMS]
WEAPON_CREATION_STRUCT					sWepStruct
DYNOPROP_CREATION_STRUCT				sDynoPropStruct
INVISIBLE_OBJECT_STRUCT					sInvisibleObjects
HUD_COLOURS_STRUCT 						sHCS
CREATION_VARS_STRUCT 					sCurrentVarsStruct
structFMMC_MENU_ITEMS 					sFMMCmenu
START_END_BLIPS							sStartEndBlips
FMMC_COMMON_MENUS_VARS 					sFMMCendStage
FMMC_UP_DOWN_STRUCT						menuScrollController
PATROL_NODE_CREATION_STRUCT				sPatrols
GET_UGC_CONTENT_STRUCT 					sGetUGC_content

ENUM RESET_STATE 
	RESET_STATE_FADE,
	RESET_STATE_CLEAR,
	RESET_STATE_WEAPONS,
	RESET_STATE_UNLOAD_WEAPONS,
	RESET_STATE_VEHICLES,
	RESET_STATE_UNLOAD_VEHICLES,
	RESET_STATE_PROPS,
	RESET_STATE_PEDS,
	RESET_STATE_OBJECTS,
	RESET_STATE_MOVING_DOORS,
	RESET_STATE_UNLOAD_PROPS,
	RESET_STATE_ATTACH,
	RESET_STATE_SPAWNS,
	RESET_STATE_TEAM_SPAWNS,
	RESET_STATE_OTHER,
	RESET_STATE_FINISH
ENDENUM
RESET_STATE iResetState

INT iDoorSetupStage, iDoorSlowLoop
INT iLoadingOverrideTimer
INT iWarpState
INT iVehicleModelLoadTimers[FMMC_MAX_VEHICLES]
INT iPropModelTimers[FMMC_MAX_NUM_FMC_PROPS]

BOOL bInitialIntroCamSetup = TRUE
BOOL bInitialIntroCamWarp = FALSE
BOOL bIsMaleSPTC, bIsCreatorModelSetSPTC
BOOL bOnGroundBeforeTest
BOOL bContentReadyForUGC

//Photo and Lobby Cameras
PREVIEW_PHOTO_STRUCT PPS
INT iIntroCamPlacementStage
SCALEFORM_INDEX SF_Movie_Gallery_Shutter_Index
INT iCamPanState = PAN_CAM_SETUP
JOB_INTRO_CUT_DATA jobIntroData
VECTOR vInitialPreviewPos

FMC_MISSION_INTERIOR eCurrentInterior = eFMCINTERIOR_INVALID
INT iCurrentInteriorVar

BOOL bShowMenu = TRUE
BOOL bDelayAFrame = TRUE

//Trigger
INT iTriggerCreationStage = CREATION_STAGE_WAIT

//Blips
BLIP_INDEX bCameraTriggerBlip
BLIP_INDEX bCameraPanBlip
BLIP_INDEX bPhotoBlip

BOOL ButtonPressed
BOOL bCreatorLimitedCloudDown = FALSE
BOOL bSignedOut = FALSE

//Help
INT iHelpBitSet
INT iHelpBitSetOld
CONST_INT biPickupEntityButton 						0
CONST_INT biDeleteEntityButton 						4
CONST_INT biWarpToCameraButton 						6
CONST_INT biWarpToCoronaButton 						7

//Menus
INT iTopItem

PROC ENABLE_HEIST_ISLAND(BOOL bEnable)

	IF bEnable
		IF NOT IS_BIT_SET(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
		OR IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RefreshIPL)
			IF HEIST_ISLAND_LOADING__LOAD_ISLAND_IPLS()
			AND SET_MISSION_ISLAND_CUSTOMISATION()
				SET_BIT(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
				PRINTLN("PRE_FRAME_CREATOR_IPL_PROCESSING - ciIPLBS_IslandLoaded now set")
			ENDIF
			CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RefreshIPL)
		ENDIF
	ELSE
		IF IS_BIT_SET(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
			HEIST_ISLAND_LOADING__UNLOAD_ISLAND_IPLS()
			UNLOAD_MISSION_ISLAND_CUSTOMISATION()
			CLEAR_BIT(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
			PRINTLN("PRE_FRAME_CREATOR_IPL_PROCESSING - ciIPLBS_IslandLoaded cleared")
		ENDIF
	ENDIF

ENDPROC

PROC ENABLE_ISLAND_PARTY_PEDS(BOOL bEnable)

	IF bEnable
		IF NOT bPartyPedsLaunched
			MAINTAIN_PED_SCRIPT_LAUNCHING(PED_LOCATION_ISLAND, bPartyPedsLaunched, PED_SCRIPT_INSTANCE_ISLAND, FALSE)
			g_bTurnOnPeds = TRUE
			PRINTLN("ENABLE_ISLAND_PARTY_PEDS - Enabling")
		ENDIF
	ELSE
		IF bPartyPedsLaunched
		AND g_bTurnOnPeds
			g_bTurnOnPeds = FALSE
			bPartyPedsLaunched = FALSE
			PRINTLN("ENABLE_ISLAND_PARTY_PEDS - Disabling")
		ENDIF
	ENDIF

ENDPROC

PROC REMOVE_FMC_IPLS()

	INT iInterior
	REPEAT COUNT_OF(sMissionPlacementData.Interior.eInteriors) iInterior
		SETUP_FMC_INTERIOR_IPLS(sMissionPlacementData.Interior.eInteriors[iInterior].eType, sMissionPlacementData.Interior.eInteriors[iInterior].iVariation, FALSE)
	ENDREPEAT
	
ENDPROC

PROC PROCESS_PRE_GAME()

	SET_BIT(sFMMCdata.iBitSet, bCameraActive)
	SET_CREATOR_AUDIO(TRUE, FALSE)
	SETUP_FMMC_SCRIPT(sFMMCMenu, sHCS, sCurrentVarsStruct)
	INITIALISE_MODEL_ARRAYS(sPedStruct,	sWepStruct, sObjStruct, sFMMCmenu)
	FILL_BLIP_SPRITE_CREATOR_ARRAY(sFMMCmenu)
	
	sFMMCmenu.iScriptCreationType	= SCRIPT_CREATION_TYPE_MISSION
	INT i
	FOR i = 0 TO PROP_LIBRARY_MAX_ITEMS-1
		sFMMCmenu.iNumberOfPropsInLibrary[i] = -1
	ENDFOR
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	g_FMMC_STRUCT.tl63MissionName = ""
	g_FMMC_STRUCT.tl63MissionDecription[0] = ""
	g_FMMC_STRUCT.iMinNumParticipants = 1
	g_FMMC_STRUCT.iMaxNumberOfTeams = 1
	
	sFMMCmenu.iSelectedObjectLibrary = OBJECT_LIBRARY_CAPTURE+1
	
	g_iStartingWave = 1
	
	FMC_MENU_INITIALISATION(sFMMCmenu)
	
ENDPROC

PROC SCRIPT_CLEANUP(BOOL bSetWorldActive)

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Main_Menu")) = 0
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SF_Movie_Gallery_Shutter_Index)	
	
	IF DOES_CAM_EXIST(sCamData.cam)
		DESTROY_CAM(sCamData.cam)
	ENDIF
	
	IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
		REMOVE_BLIP(sStartEndBlips.biStart)
	ENDIF
	
	IF sStartEndBlips.ciStartType != NULL
		DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
		sStartEndBlips.ciStartType = NULL
	ENDIF
	
	IF DOES_BLIP_EXIST(bCameraPanBlip)
		REMOVE_BLIP(bCameraPanBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bPhotoBlip)
		REMOVE_BLIP(bPhotoBlip)
	ENDIF
	
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	g_bFMMC_TutorialSelectedFromMpSkyMenu = FALSE
	COMMON_FMMC_SCRIPT_CLEAN_UP(sPedStruct,	sVehStruct,	sWepStruct,	sObjStruct,sPropStruct, sDynoPropStruct, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, bSetWorldActive)
	iDoorSetupStage = 0
	
	CLEANUP_MENU_ASSETS()
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		IF SCRIPT_IS_CLOUD_AVAILABLE()
			PRINTLN("SCRIPT_IS_CLOUD_AVAILABLE()")
		    SET_FAKE_MULTIPLAYER_MODE(FALSE)
		ELSE
			PRINTLN("NOT SCRIPT_IS_CLOUD_AVAILABLE()")
			IF NOT IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
				NETWORK_SESSION_LEAVE_SINGLE_PLAYER() 
			ENDIF
			g_Private_IsMultiplayerCreatorRunning = FALSE
			g_Private_MultiplayerCreatorNeedsToEnd = TRUE
		ENDIF
	ENDIF
	
	SET_CREATOR_AUDIO(FALSE, FALSE)
	
	CLEANUP_ALL_INTERIORS(g_ArenaInterior)
	
	REMOVE_FMC_IPLS()
	
	ENABLE_HEIST_ISLAND(FALSE)
	ENABLE_ISLAND_PARTY_PEDS(FALSE)
	
	g_FMMC_STRUCT.iMissionType = 0
	
	RESET_SCRIPT_GFX_ALIGN()
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC REFRESH_FMC_MENUS()
	REFRESH_MENU(sFMMCMenu)

	DRAW_FMC_MENUS(sFMMCMenu)
	
ENDPROC

FUNC BOOL DEAL_WITH_SKIPPING_EMPTY_OPTIONS(INT iDirection)
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > sFMMCmenu.iCurrentMenuLength-1
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
	ENDIF
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iCurrentMenuLength - 1)
	ENDIF

	IF SHOULD_CURRENT_SELECTION_BE_SKIPPED(sFMMCmenu)
		SKIP_EMPTY_OPTION(sFMMCmenu, iDirection)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC SET_ENTITY_CREATION_STATUS(INT iNewState)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[CREATOR] - SET_ENTITY_CREATION_STATUS - Setting entity creation state from ", sCurrentVarsStruct.iEntityCreationStatus, " to ", iNewState)
	sCurrentVarsStruct.iEntityCreationStatus = iNewState
ENDPROC

PROC TRACK_TIME_IN_CREATOR_MODE()

	IF NETWORK_CLAN_SERVICE_IS_VALID()  
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_MISSION_CREATO, TIMERA())
		SETTIMERA(0)
	ENDIF
	
ENDPROC

PROC DEAL_WITH_AMBIENT_AND_HUD()
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		sCurrentVarsStruct.creationStats.iCreatingTimeMS += GET_GAME_TIMER() - sCurrentVarsStruct.creationStats.iCurrentTimeMS
		
		FMMC_MAINTAIN_TIME_AND_WEATHER(g_FMMC_STRUCT.iTimeOfDay)
		FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME()	
		IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)	
			DISABLE_ADDITIONAL_CONTROLS()
		ENDIF
		BOOL bHideRadar
		FMMC_HIDE_HUD_ELEMENTS(sFMMCdata, sFMMCMenu, bHideRadar)
		MAINTAIN_CAMERA_ROTATION_LIMITS(sCamData, sFMMCMenu.sActiveMenu, sCurrentVarsStruct, bInitialIntroCamSetup, bInitialIntroCamWarp)
		IF bHideRadar
			SET_BIGMAP_ACTIVE(FALSE, FALSE)
		ELSE
			SET_BIGMAP_ACTIVE(TRUE, FALSE)
		ENDIF
	ENDIF
	sCurrentVarsStruct.creationStats.iCurrentTimeMS = GET_GAME_TIMER()
	
	DEAL_WITH_PHOTO_PREVIEW(PPS, sFMMCendStage.sTakePhotoVars, sCurrentVarsStruct.iMenuState, sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE AND iIntroCamPlacementStage = 2 AND NOT IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsHidePreviewPhoto),sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
	DEAL_WITH_THE_PLACING_INTRO_CAM(SF_Movie_Gallery_Shutter_Index, sFMMCMenu.sActiveMenu, iIntroCamPlacementStage, bInitialIntroCamSetup, bShowMenu, sCurrentVarsStruct.iMenuState, sCurrentVarsStruct.iEntityCreationStatus)
	
	CREATOR_DOORS_SETUP(iDoorSlowLoop,iDoorSetupStage, FMMC_TYPE_MISSION)
	
	IF SCRIPT_IS_CLOUD_AVAILABLE()
		TRACK_TIME_IN_CREATOR_MODE()
	ENDIF
ENDPROC

PROC MAINTAIN_PLACED_MARKERS()
	MAINTAIN_PLACED_TRIGGER_LOCATION(g_FMMC_STRUCT.vStartPos, sStartEndBlips.biStart, sStartEndBlips.ciStartType, g_FMMC_STRUCT.vCameraPanPos, bCameraPanBlip, bPhotoBlip)
ENDPROC

PROC SET_ALL_MENU_ITEMS_ACTIVE()
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_CREATOR)
	IF g_FMMC_STRUCT.bMissionIsPublished = FALSE
	AND SCRIPT_IS_CLOUD_AVAILABLE()
		SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
	ENDIF
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_RADIO)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_OPTIONS)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_EXIT)
ENDPROC

// The gateway to all creation types being created
PROC HANDLE_ENTITY_CREATION()

	SWITCH sFMMCmenu.iEntityCreation
		CASE -1
			sCurrentVarsStruct.iHoverEntityType  = -1
			IF sFMMCMenu.sActiveMenu != eFmmc_TOP_MENU
			AND sFMMCMenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
				UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)
			ELSE
				UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
			ENDIF
		BREAK
		
		CASE CREATION_TYPE_TRIGGER
			IF DO_TRIGGER_CREATION(sFMMCmenu, sCurrentVarsStruct, sFMMCdata, sStartEndBlips.biStart, sHCS.hcStartCoronaColour,sStartEndBlips.ciStartType, sFMMCendStage, iTriggerCreationStage, 3)
				SET_ALL_MENU_ITEMS_ACTIVE()
			ENDIF
		BREAK
		
		CASE CREATION_TYPE_PAN_CAM
			BOOL bResetCamMenu
			MAINTAIN_PLACING_INTRO_CAMERAS(sFMMCMenu, bCameraTriggerBlip, bResetCamMenu, bInitialIntroCamSetup)
			UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
			IF bResetCamMenu
				REFRESH_FMC_MENUS()
				sCurrentVarsStruct.bResetUpHelp = TRUE
			ENDIF
		BREAK
		
		CASE CREATION_TYPE_WEAPONS
			DO_WEAPON_CREATION(sWepStruct, sObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, FMMC_MAX_WEAPONS, g_FMMC_STRUCT.iVehicleDeathmatch = 0)
		BREAK
		
		CASE CREATION_TYPE_PROPS
			DO_FMC_PROP_CREATION(sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, sCamData, FMMC_MAX_NUM_FMC_PROPS)
		BREAK
		
		CASE CREATION_TYPE_GOTO_LOC
			DO_FMC_LOCATION_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, MAX_NUM_GOTO_LOCATIONS)
		BREAK
		
		CASE CREATION_TYPE_PORTALS
			DO_FMC_PORTAL_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, MAX_NUM_MISSION_PORTALS)
		BREAK
		CASE CREATION_TYPE_PORTAL_WARP
			DO_FMC_PORTAL_WARP_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, MAX_NUM_PORTAL_WARP_COORDS)
		BREAK
		
		CASE CREATION_TYPE_SPAWN_LOCATION
			DO_FMC_CUSTOM_SPAWN_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata)
		BREAK
		
		CASE CREATION_TYPE_WORLD_PROPS
			DO_WORLD_PROP_SELECTION(sFMMCmenu, sCurrentVarsStruct)
		BREAK
		
		CASE CREATION_TYPE_DOOR
			DO_DOOR_SELECTION(sCurrentVarsStruct, sFMMCmenu, sFMMCdata)
		BREAK
		
		CASE CREATION_TYPE_PEDS
			DO_FMC_PED_CREATION(sEditedPedPlacementData, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata)
		BREAK
		
		CASE CREATION_TYPE_VEHICLES
			DO_FMC_VEHICLE_CREATION(sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, MAX_NUM_VEHICLES)
		BREAK
		
		CASE CREATION_TYPE_OBJECTS
			DO_MISSION_ENTITY_CREATION(sCurrentVarsStruct, sFMMCmenu, sFMMCdata)
		BREAK
		
		CASE CREATION_TYPE_PATROL
			DO_FMC_PATROL_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sPatrols, MAX_NUM_PATROL_NODES)
		BREAK
		
		CASE CREATION_TYPE_FMMC_ZONE
			DO_ZONE_CREATION(sCurrentVarsStruct, sFMMCmenu, sFMMCendStage)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfZones, GET_MAX_PLACED_ZONES(), "FMMCCMENU_18")
		BREAK
		
		CASE CREATION_TYPE_LEAVE_AREA
			DO_FMC_LEAVE_AREA_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, MAX_NUM_LEAVE_AREAS)
		BREAK
		
		CASE CREATION_TYPE_SEARCH_AREA
			DO_FMC_SEARCH_AREA_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sCamData, MAX_NUM_SEARCH_AREAS)
		BREAK
		
		CASE CREATION_TYPE_MOVING_DOOR
			DO_FMC_MOVING_DOOR_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, MAX_NUM_MOVING_DOORS)
		BREAK
		
		CASE CREATION_TYPE_DUMMY_BLIPS
			DO_DUMMY_BLIP_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage)
		BREAK
		
		CASE CREATION_TYPE_TARGETS
			
		BREAK
		CASE CREATION_TYPE_CHECKPOINTS_LIST
			DO_FMC_CHECKPOINTS_LIST_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, MAX_NUM_CHECKPOINTS )
		BREAK
		
		CASE CREATION_TYPE_SECURITY_CAMERA
			DO_FMC_CAMERA_VISION_CONE_CREATION(sFMMCmenu, sFMMCdata, sCamData)
		BREAK
		
		CASE CREATION_TYPE_SPAWN_POSITIONS
			#IF MAX_NUM_SPAWN_POSITIONS
			// url:bugstar:7523499 - Freemode Creator - Investigate better ways to handle when we want an entity to spawn randomly out of several locations
			// Link to setup of spawn position menu
			DO_FMC_SPAWN_POSITIONS_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sCamData, sMissionPlacementData.SpawnPosition.iCount)
			#ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC CONTROL_CAMERA_AND_CORONA()
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)		

		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)

		DEAL_WITH_HITTING_MAX_ENTITES(sCurrentVarsStruct, sFMMCmenu)
		
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
			MAINTAIN_FMMC_CAMERA(sFMMCdata, sCamData, sCurrentVarsStruct.iMenuState, DEFAULT, FALSE)
		ELSE
			IF IS_PLAYER_IN_A_MAP_ESCAPE(PLAYER_PED_ID())
				SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,FALSE,FALSE,TRUE,FALSE)
			ENDIF
		ENDIF
		
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()	
		AND sFMMCMenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFMMCMenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
			
			VECTOR vDummyRot = <<0,0,0>>
			IF DOES_CAM_EXIST(sCamData.cam)
				vDummyRot = GET_CAM_ROT(sCamData.cam)
			ENDIF
			
			MAINTAIN_SCREEN_CENTER_COORD(vDummyRot, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, 	sHCS.hcCurrentCoronaColour, sFMMCmenu.fCreationHeightIncrease, sFMMCmenu.fCreationHeightIncrease != 0.0, FALSE, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), FALSE, g_FMMC_STRUCT.iVehicleDeathmatch = 1)
			
		ENDIF
	ENDIF
ENDPROC

FUNC MENU_ICON_TYPE DOES_THIS_DESCRIPTION_NEED_A_WARNING(STRING description)
	IF NOT IS_STRING_NULL_OR_EMPTY(description)
		IF ARE_STRINGS_EQUAL(description, "FMMCNO_CLOUD")
		OR ARE_STRINGS_EQUAL(description, GET_DISCONNECT_HELP_MESSAGE())
		OR ARE_STRINGS_EQUAL(description, "FMMCNO_SCLUB")
			RETURN MENU_ICON_ALERT
		ENDIF
	ENDIF
	
	RETURN MENU_ICON_DUMMY
ENDFUNC

// Grabs the description if one has been set in the sFMMCmenu.sOptionDescription array
FUNC STRING GET_MENU_ITEM_DESCRIPTION()
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != -1
	AND NOT IS_STRING_NULL_OR_EMPTY(sFMMCmenu.sOptionDescription[GET_CREATOR_MENU_SELECTION(sFMMCmenu)])
		RETURN sFMMCmenu.sOptionDescription[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
	ENDIF
	RETURN sOptionDesc[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
ENDFUNC

// Most of this function is noise you can ignore but ultimately it draws the menus on screen and sets your menu selection
PROC DRAW_MENU_SELECTION()
	
	// Clear all the help bits before checking
	clear_BIT(iHelpBitSet, biWarpToCoronaButton) 
	clear_BIT(iHelpBitSet, biWarpToCameraButton) 	
	CLEAR_BIT(iHelpBitSet, biPickupEntityButton)
	CLEAR_BIT(iHelpBitSet, biDeleteEntityButton)	
	sCurrentVarsStruct.bSelectedAnEntity = FALSE
		
	IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_CHECKPOINT
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_PROPS
		sCurrentVarsStruct.bSelectedAnEntity = TRUE
	ENDIF
	IF sFMMCMenu.iSelectedEntity = -1
		IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
		AND (sCurrentVarsStruct.iHoverEntityType = sFMMCmenu.iEntityCreation)
			IF IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona)
				SET_BIT(iHelpBitSet, biPickupEntityButton)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
			SET_BIT(iHelpBitSet, biDeleteEntityButton)
		ENDIF
	ENDIF
	
	IF sFMMCMenu.iSelectedEntity = -1		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
			SET_BIT(iHelpBitSet, biWarpToCameraButton) 
		ELSE
			SET_BIT(iHelpBitSet, biWarpToCoronaButton)
		ENDIF
	ENDIF
	
	BOOL bRefresh
	IF iHelpBitSetOld != iHelpBitSet
		iHelpBitSetOld = iHelpBitSet
		PRINTLN("bRefresh = TRUE")
		bRefresh = TRUE
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF NOT IS_GAMEPLAY_CAM_RENDERING()
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
			OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
				PRINTLN("bRefresh = TRUE - FORCED BY HOLDING LSHIFT DOWN")
				bRefresh = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Set the current item and draw menu each frame
	IF sFMMCmenu.iCurrentMenuLength <= 10
		iTopItem = 0
	ENDIF
	SET_TOP_MENU_ITEM(iTopItem)
	SET_CURRENT_MENU_ITEM(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
		
	IF sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
	OR sFMMCMenu.sActiveMenu = eFmmc_PAN_CAM_BASE
		sCurrentVarsStruct.bDisplaySelected = FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_STRING_NULL_OR_EMPTY(sCurrentVarsStruct.sFailReason)
		PRINTLN("sFailReason = ", sCurrentVarsStruct.sFailReason)
	ENDIF
	#ENDIF
	IF sCurrentVarsStruct.bDisplayFailReason
		IF sCurrentVarsStruct.iFailDescriptionTimer > GET_GAME_TIMER() - 5000
			SET_CURRENT_MENU_ITEM_DESCRIPTION(sCurrentVarsStruct.sFailReason)
		ELSE
			sCurrentVarsStruct.bDisplayFailReason = FALSE
		ENDIF
	ENDIF
	
	IF sCurrentVarsStruct.bDisplayFailReason = FALSE
		IF sCurrentVarsStruct.bDisplaySelected
		AND sFMMCmenu.iEntityCreation != CREATION_TYPE_NONE
			PROCESS_SHOWING_HIGHLIGHTED_ENTITY(sCurrentVarsStruct)
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_MENU_ITEM_DESCRIPTION(), 0, DOES_THIS_DESCRIPTION_NEED_A_WARNING(GET_MENU_ITEM_DESCRIPTION())) // Pass in "" to clear
			IF NOT IS_STRING_NULL_OR_EMPTY(GET_MENU_ITEM_DESCRIPTION())
			AND (ARE_STRINGS_EQUAL(GET_MENU_ITEM_DESCRIPTION(), "DMC_H_14AT")
			OR ARE_STRINGS_EQUAL(GET_MENU_ITEM_DESCRIPTION(), "DMC_H_14T1"))
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints)
				IF ((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints > 1
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FMMC_SPP")
				ELSE
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FMMC_SPS")
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = efmmc_PROP_TEMPLATE_GRAB_PROPS
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FM_NXT_DM")
			ENDIF
		ENDIF
	ENDIF	
		
	INT iLines = 10
	
	SET_MAX_MENU_ROWS_TO_DISPLAY(iLines)
	
	DRAW_MENU()
	
	iTopItem = GET_TOP_MENU_ITEM()
	
	//If the help text need to be redrawn then do so.
	IF bRefresh
		sCurrentVarsStruct.bResetUpHelp = TRUE
	ENDIF
ENDPROC

// Changes the camera from the skycam to the ground
PROC HANDLE_SWITCH_TO_GROUND_CAMERA()
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		IF sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
		AND NOT (HAS_CURRENT_PROP_BEEN_FREE_ROTATED(sFMMCmenu) AND IS_PC_VERSION() AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL))
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_SWITCH_CAMERA_BUTTON())		
				sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

// This handles the "Cycle Entities" options which I don't think actually exist in any FMC menus
// If that's implemented most of the below will need rewritten but the base structure can remain the same
PROC DEAL_WITH_CAMERA_SWITCH_SELECTION(BOOL bIncrease, INT iMaximumValue)
	
	IF NOT HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		START_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)
	ELIF HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		REINIT_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)
	ENDIF
	
	IF iMaximumValue > 0
		IF sCycleCamStruct.bSwitchingCam = FALSE
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			
			IF bIncrease
				sFMMCmenu.iSwitchCam ++
				IF sFMMCmenu.iSwitchCam < 0
					sFMMCmenu.iSwitchCam = 1
				ENDIF
			ELSE
				sFMMCmenu.iSwitchCam --
				IF sFMMCmenu.iSwitchCam < 0
					sFMMCmenu.iSwitchCam = iMaximumValue - 1
				ENDIF
			ENDIF
			IF sFMMCmenu.iSwitchCam > iMaximumValue - 1
				sFMMCmenu.iSwitchCam = 0
			ENDIF
			
			PLAY_EDIT_MENU_ITEM_SOUND()
			
			IF sFMMCMenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
				sCycleCamStruct.vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				sCycleCamStruct.fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCmenu.iSwitchCam].fHead
			ELIF sFMMCMenu.sActiveMenu = eFmmc_PICKUP_BASE
				sCycleCamStruct.vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				sCycleCamStruct.fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sFMMCmenu.iSwitchCam].vRot.z
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCMenu.sActiveMenu = eFmmc_FMC_PROP_MENU
				sCycleCamStruct.vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedProp[sFMMCmenu.iSwitchCam].vPos
				sCycleCamStruct.fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedProp[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCMenu.sActiveMenu = eFmmc_DYNOPROP_BASE
				sCycleCamStruct.vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				sCycleCamStruct.fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCMenu.sActiveMenu = eFMMC_FMC_VEH_MENU
				sCycleCamStruct.vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sFMMCmenu.iSwitchCam].vPos
				sCycleCamStruct.fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCMenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
				INT iTempTeam, iTempSpawn
				IF sFMMCmenu.iSwitchCam < g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				AND sFMMCmenu.iSwitchCam >= 0
				AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] > 0
					iTempTeam = 0
					iTempSpawn = sFMMCmenu.iSwitchCam
				ELIF sFMMCmenu.iSwitchCam < g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				AND sFMMCmenu.iSwitchCam >= g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] > 0
					iTempTeam = 1
					iTempSpawn = sFMMCmenu.iSwitchCam - g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				ELIF sFMMCmenu.iSwitchCam < g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				AND sFMMCmenu.iSwitchCam >= g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
				AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] > 0
					iTempTeam = 2
					iTempSpawn = sFMMCmenu.iSwitchCam - (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0])
				ELSE
					iTempTeam = 3
					iTempSpawn = sFMMCmenu.iSwitchCam - (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0])
				ENDIF
				
				PRINTLN("iSwitchCam = ", sFMMCmenu.iSwitchCam, ", iTempTeam = ", iTempTeam, ", iTempSpawn = ", iTempSpawn)
				sCycleCamStruct.vSwitchVec = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTempTeam][iTempSpawn].vPos + <<0.0, 0.0, 8.1>>
				sCycleCamStruct.fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTempTeam][iTempSpawn].fHead
			ENDIF
			
			sCycleCamStruct.bSwitchingCam = TRUE 
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_THIS_OPTION_SELECTABLE()
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_OPTION_ROTATABLE()

	IF sFMMCmenu.sActiveMenu = eFmmc_FMC_PROP_MENU
	OR sFMMCMenu.iSelectedEntity != -1
	OR sFMMCmenu.sActiveMenu = eFmmc_FMC_CUSTOM_SPAWNS
	OR sFMMCmenu.sActiveMenu = eFmmc_FMC_CHECKPOINTS_LIST_MENU

		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_LIVERY_NAME()
	IF IS_ENTITY_ALIVE(sVehStruct.viCoronaVeh)	
		IF GET_VEHICLE_LIVERY_COUNT(sVehStruct.viCoronaVeh) > -1
		OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, MULE) AND NOT IS_BIT_SET(sFMMCmenu.iVehBitSet, ciFMMC_VEHICLE_EXTRA1))
		OR GET_NUM_VEHICLE_MODS(sVehStruct.viCoronaVeh, MOD_LIVERY) > 0
			IF IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, MULE)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, SANCHEZ)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, POLMAV)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, SHAMAL)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, RUMPO)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, STUNT)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, GBURRITO2)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, JET)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, WINDSOR)
			OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, AMBULANCE) AND IS_ROCKSTAR_DEV())
			OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, POLICE3) AND IS_ROCKSTAR_DEV())
			OR (GET_NUM_VEHICLE_MODS(sVehStruct.viCoronaVeh, MOD_LIVERY) > 0 AND IS_ROCKSTAR_DEV())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Add any functionality that you want to run when in specific menus in here
// eg. if you want to draw a marker in the world or something to show a position
PROC HANDLE_EVERY_FRAME_MENU_FUNCTIONALITY()

	SWITCH sFMMCMenu.sActiveMenu
		CASE eFmmc_FMC_TRIGGER_AREA_OPTIONS
		CASE eFmmc_FMC_TRIGGER_AREAS
			PROCESS_TRIGGER_AREA_OPTIONS_EVERY_FRAME(sFMMCMenu, sCurrentVarsStruct)
		BREAK
		CASE eFmmc_FMC_FOCUS_CAMS_UPDATE_MENU
			PROCESS_FOCUS_CAMS_OPTIONS_EVERY_FRAME(sFMMCMenu)
		BREAK
		CASE eFmmc_FMC_GO_TO_POINT 
			PROCESS_FMC_GOTO_MENU_OPTIONS_EVERY_FRAME(sFMMCmenu)
		BREAK
		CASE eFmmc_FMC_PORTALS
			PROCESS_FMC_PORTAL_MENU_OPTIONS_EVERY_FRAME(sFMMCmenu)
		BREAK
		CASE eFmmc_PORTALS_SUBMENU_WARP
			PROCESS_FMC_PORTAL_SUBMENU_WARP_OPTIONS_EVERY_FRAME(sFMMCmenu)
		BREAK
		CASE eFmmc_FMC_LEAVE_AREAS
			PROCESS_FMC_LEAVE_AREA_OPTIONS_EVERY_FRAME(sFMMCmenu)
		BREAK
		CASE eFmmc_FMC_SEARCH_AREA_OPTIONS
			PROCESS_FMC_SEARCH_AREA_OPTIONS_EVERY_FRAME(sFMMCmenu)
		BREAK
		CASE eFMMC_FMC_PED_MENU
			PROCESS_FMC_PED_OPTIONS_EVERY_FRAME(sFMMCmenu, sEditedPedPlacementData)
		BREAK
		CASE eFmmc_FMC_MOVING_DOOR_MENU
			PROCESS_FMC_MOVING_DOOR_MENU_OPTIONS_EVERY_FRAME(sFMMCmenu)
		BREAK
		CASE eFmmc_FMC_TARGET_LIST_MENU
		CASE eFmmc_FMC_TARGET_UPDATE_MENU
			PROCESS_FMC_TARGET_OPTIONS_EVERY_FRAME(sFMMCMenu)
			IF sFMMCMenu.sActiveMenu = eFmmc_FMC_TARGET_UPDATE_MENU
				DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.TakeOutTarget[sFMMCMenu.iSelectedFMCEntity].iCount, MAX_NUM_TARGETS, "FMC_T_FCTAR")
			ENDIF
		BREAK
		CASE eFmmc_FMC_SECURITY_CAMERA_OPTIONS_MENU
			PROCESS_FMC_SECURITY_CAMERAS_DATA_MENU_EVERY_FRAME(sFMMCmenu)
		BREAK
	ENDSWITCH
	
	PROCESS_FMC_COUNTS_EVERY_FRAME()
ENDPROC

// Where Left/Right selection menu editing takes place
PROC EDIT_MENU_OPTIONS(INT iChange)
	INT iType
	
	SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_TOP_MENU
			
		BREAK
		
		CASE eFmmc_MAIN_OPTIONS_BASE
			HANDLE_BASIC_CREATOR_BASE_OPTIONS_MENU(sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_DELETE_ENTITIES
			
		BREAK
		
		CASE eFmmc_TEAM_TEST
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
				
			ENDIF
		BREAK
		
		CASE eFMMC_FMC_PED_MENU
			EDIT_PED_MENU_OPTIONS(sFMMCmenu, sEditedPedPlacementData, iChange, sFMMCmenu.iPedModelTailIndex[sFMMCmenu.iPedLibrary])
		BREAK
		
		CASE eFmmc_FMC_ANIM_MENU
			EDIT_PED_ANIM_OPTIONS(sFMMCmenu, iChange)
		BREAK
		
		CASE eFMMC_FMC_VEH_MENU
			EDIT_VEH_MENU_OPTIONS(sFMMCmenu, iChange)
		BREAK
		
		CASE eFMMC_FMC_OBJ_MENU
			EDIT_OBJ_MENU_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_PATROL_UPDATE_MENU
			EDIT_FMC_PATROL_NODE_OPTIONS(sFMMCmenu, sFMMCendStage, sFMMCdata, sCamData, iChange)
		BREAK
		
		CASE eFmmc_FMC_AMBUSH_MENU
			EDIT_FMC_AMBUSH_OPTIONS(sFMMCMenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_AMBUSH_UPDATE_MENU
			EDIT_FMC_AMBUSH_UPDATE_OPTIONS(sFMMCMenu, sFMMCendStage, sFMMCdata, sCamData, iChange)
		BREAK
		
		CASE eFmmc_FMC_TRAILER_UPDATE_MENU
			EDIT_FMC_TRAILER_OPTIONS(sFMMCMenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_PED_GROUP_UPDATE_MENU
			EDIT_FMC_PED_GROUP_OPTIONS(sFMMCMenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_POPULATION_UPDATE_MENU
			EDIT_FMC_POPULATION_OPTIONS(sFMMCMenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_CATEGORY_TOGGLE
			FMC_EDIT_CATEGORY_MENU_OPTIONS(sFMMCMenu)
		BREAK
		
		CASE eFMMC_FMC_PROP_MENU
			EDIT_PROP_MENU_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_GO_TO_POINT
			EDIT_FMC_GOTO_MENU_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		CASE eFmmc_FMC_COMMON_BLIP_MENU
			EDIT_FMC_COMMON_BLIP_SUBMENU_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_PORTALS
			EDIT_FMC_PORTAL_MENU_OPTIONS(sFMMCmenu, sFMMCendStage, sFMMCdata, sCamData, iChange)
		BREAK
		CASE eFmmc_PORTALS_SUBMENU_WARP
			EDIT_FMC_PORTAL_SUBMENU_WARP_OPTIONS(sFMMCmenu, sFMMCendStage, sFMMCdata, sCamData, iChange)
		BREAK
		
		CASE eFmmc_FMC_CUSTOM_SPAWNS
			EDIT_FMC_CUSTOM_SPAWN_MENU_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_FOCUS_CAMS_UPDATE_MENU
			EDIT_FMC_FOCUS_CAM_OPTIONS(sFMMCmenu, sFMMCendStage, sFMMCdata, sCamData, iChange)
		BREAK
		
		CASE eFmmc_FMC_TARGET_UPDATE_MENU
			EDIT_FMC_TARGET_OPTIONS(sFMMCmenu, sFMMCendStage, sFMMCdata, sCamData, iChange)
		BREAK
		
		CASE eFmmc_FMC_TRIGGER_AREA_OPTIONS
			EDIT_TRIGGER_AREA_OPTIONS(sFMMCMenu, sCurrentVarsStruct, iChange)
		BREAK
		
		CASE eFmmc_FMC_SEARCH_AREA_OPTIONS
			EDIT_SEARCH_AREA_OPTIONS(sFMMCMenu, sCurrentVarsStruct, iChange)
		BREAK
		
		CASE eFmmc_DOORS_BASE
			EDIT_DOORS_BASE_LEGACY_MENU(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE efMMC_WORLD_PROPS
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps)
			ENDIF
		BREAK
		
		CASE eFmmc_ZONES_BASE
			EDIT_ZONE_BASE_MENU(sFMMCmenu, sCurrentVarsStruct, sFMMCendStage, iChange)
		BREAK
		
		CASE eFMMC_ZONE_TYPE_SPECIFIC_OPTIONS
			EDIT_ZONE_TYPE_SPECIFIC_OPTIONS_MENU(sFMMCMenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFMMC_DUMMY_BLIPS
			EDIT_DUMMY_BLIP_BASE_MENU(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_RESTRICTED_INTERIORS
			EDIT_RESTRICTED_INTERIOR_OPTIONS(sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmC_FMC_LEAVE_AREAS
			EDIT_FMC_LEAVE_AREA_MENU_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_OPTIONS
			EDIT_FMC_OPTIONS_MENU_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_MOVING_DOOR_MENU
			EDIT_FMC_MOVING_DOOR_MENU_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_SECURITY_CAMERA_OPTIONS_MENU
			EDIT_FMC_SECURITY_CAMERA_OPTIONS_MENU(sFMMCmenu, sFMMCendStage, sFMMCdata, sCamData, iChange)
		BREAK
		
		CASE eFmmc_FMC_TAGS_UPDATE_MENU
			EDIT_FMC_TAG_OPTIONS(sFMMCmenu, sFMMCendStage, sFMMCdata, sCamData, iChange)
		BREAK
		
		CASE eFmmc_FMC_MODE_TIMER_MENU
			EDIT_FMC_MODE_TIMER_MENU_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_WANTED_MENU
			EDIT_FMC_WANTED_MENU_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_FMC_CHECKPOINTS_LIST_MENU
			EDIT_FMC_CHECKPOINTS_LISTS_LIST_MENU_OPTIONS(sFMMCmenu, sFMMCendStage, sFMMCdata, sCamData, iChange)
		BREAK
		
		CASE eFmmc_FMC_VEHICLE_DOORS_MENU
			EDIT_FMC_VEHICLE_DOOR_MENU_OPTIONS(sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_FMC_SPAWN_POSITION_MENU
			#IF MAX_NUM_SPAWN_POSITIONS
			// url:bugstar:7523499 - Freemode Creator - Investigate better ways to handle when we want an entity to spawn randomly out of several locations
			// Spawn Position menu Edit
			EDIT_FMC_SPAWN_POSITION(sFMMCMenu, sFMMCendStage, iChange)
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	IF iType != -1
	
		sFMMCmenu.iCurrentEntitySelection[iType]+= iChange
		INT iMaximum
		IF iType = CREATION_TYPE_WEAPONS
			IF g_FMMC_STRUCT.iVehicleDeathmatch = 0
				IF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_PISTOLS
					iMaximum = FMMC_MAX_NUM_PISTOLS
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_PISTOLS
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SHOTGUNS
					iMaximum = FMMC_MAX_NUM_SHOTGUNS
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_SHOTGUNS
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_MGS
					iMaximum = FMMC_MAX_NUM_MGS
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_MGS
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_RIFLES
					iMaximum = FMMC_MAX_NUM_RIFLES
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_RIFLES
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SNIPERS
					iMaximum = FMMC_MAX_NUM_SNIPERS
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_SNIPERS
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_HEAVY
					iMaximum = FMMC_MAX_NUM_HEAVY
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_HEAVY-1
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_THROWN
					iMaximum = FMMC_MAX_NUM_THROWN
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_THROWN
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_MELEE
					iMaximum = FMMC_MAX_NUM_MELEE
					IF NOT IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_MELEE
					ENDIF
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SPECIAL
					iMaximum = FMMC_MAX_NUM_SPECIAL - 2
				ENDIF
			ELSE
				iMaximum = VEH_MAX_ARMOURY_TYPES
			ENDIF
		ELIF iType = CREATION_TYPE_WEAPON_AMMO
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sWepStruct.iNumberOfClips, iChange, FMMC_MAX_CLIPS)
			sFMMCmenu.iWepClips = sWepStruct.iNumberOfClips
		ELIF iType = CREATION_TYPE_WEAPON_REST
			EDIT_CREATOR_BOOL_MENU_ITEM(sFMMCendStage, sWepStruct.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-3])
			sFMMCmenu.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-3] = sWepStruct.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-3]
		ELIF iType = CREATION_TYPE_WEAPON_LIBRARY
			IF g_FMMC_STRUCT.iVehicleDeathmatch = 0
				iMaximum = FMMC_WEAPON_LIBRARY_MAX
			ELSE
				iMaximum = FMMC_VEH_WEAPON_LIBRARY_MAX
			ENDIF
		ENDIF
		
		INT iWrapMax = iMaximum - 1
		
		IF sFMMCmenu.iCurrentEntitySelection[iType] < 0 
			sFMMCmenu.iCurrentEntitySelection[iType] = iWrapMax
		ELIF sFMMCmenu.iCurrentEntitySelection[iType] > iWrapMax
			sFMMCmenu.iCurrentEntitySelection[iType] = 0
		ENDIF
		
		IF iType = CREATION_TYPE_WEAPONS
			CHANGE_WEAPON_TYPE(sFMMCmenu, sWepStruct, sFMMCmenu.iCurrentEntitySelection[iType], TRUE, iChange > 0, iMaximum)
		ELIF iType = CREATION_TYPE_WEAPON_LIBRARY
			CHANGE_WEAPON_LIBRARY(sFMMCmenu, sWepStruct, sFMMCmenu.iCurrentEntitySelection[iType], TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_OPTION_A_MENU_ACTION()	
	
	IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_OPTION_A_MENU_GOTO()	
	
	IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
PROC REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(INT iCurrentState)

	MODEL_NAMES oldModel
	
	IF iCurrentState != CREATION_TYPE_VEHICLES
		IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
			oldModel = GET_ENTITY_MODEL(sVehStruct.viCoronaVeh)
			DELETE_VEHICLE(sVehStruct.viCoronaVeh)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_OBJECTS
		IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sObjStruct.viCoronaObj)
			DELETE_OBJECT(sObjStruct.viCoronaObj)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState !=CREATION_TYPE_WEAPONS
		IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
			oldModel = GET_ENTITY_MODEL(sWepStruct.viCoronaWep)
			DELETE_OBJECT(sWepStruct.viCoronaWep)					
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_PROPS
		IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sPropStruct.viCoronaObj)
			DELETE_OBJECT(sPropStruct.viCoronaObj)			
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
		DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
	ENDIF
	IF iCurrentState != CREATION_TYPE_DYNOPROPS
		IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sDynoPropStruct.viCoronaObj)
			DELETE_OBJECT(sDynoPropStruct.viCoronaObj)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			
			IF GET_MODEL_NAME_OF_CHILD_PROP_FOR_DYNO(oldModel) != DUMMY_MODEL_FOR_SCRIPT
				IF DOES_ENTITY_EXIST(sDynoPropStruct.oiChildTemp)
					DELETE_OBJECT(sDynoPropStruct.oiChildTemp)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			ENDIF
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_PEDS
		IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
			oldModel = GET_ENTITY_MODEL(sPedStruct.piTempPed)
			DELETE_PED(sPedStruct.piTempPed)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF	
	ENDIF
	IF iCurrentState != CREATION_TYPE_MOVING_DOOR
		IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiCoronaObj)
			oldModel = GET_ENTITY_MODEL(sMovingDoorStruct.oiCoronaObj)
			DELETE_OBJECT(sMovingDoorStruct.oiCoronaObj)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
ENDPROC

PROC CLEAN_LAST_GO_TO_LOCATION()
	PRINTLN("CLEAN_LAST_GO_TO_LOCATION")
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] < FMMC_MAX_GANG_HIDE_LOCATIONS
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].vLoc[0]			= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].fRadius[0]			= 0.0
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].vLoc1				= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].vLoc2				= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].fWidth				=  1.0
		INT iTeam
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].iRule[iTeam] 		= FMMC_OBJECTIVE_LOGIC_NONE
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].iPriority[iTeam]	= FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].fHeading[iTeam]	= 0.0
		ENDFOR
	ENDIF
ENDPROC

// Resets a lot of functionality and settings if you cancel the placement of an entity.
// A lot of it is noise from the old creators to be honest
PROC CANCEL_ENTITY_CREATION(INT iTypePassed)

	// If you're cancelling the placement of a new entity and backing out of the creation menu
	IF sFMMCMenu.iSelectedEntity = -1
		INT i
		
		IF sCurrentVarsStruct.mnThingToPlaceForBudget != DUMMY_MODEL_FOR_SCRIPT
			REMOVE_MODEL_FROM_CREATOR_BUDGET(sCurrentVarsStruct.mnThingToPlaceForBudget)
			sCurrentVarsStruct.mnThingToPlaceForBudget = DUMMY_MODEL_FOR_SCRIPT
		ENDIF
		
		sWepStruct.iSwitchingINT 	= CREATION_STAGE_WAIT
		sVehStruct.iSwitchingINT	= CREATION_STAGE_WAIT
		sObjStruct.iSwitchingINT	= CREATION_STAGE_WAIT
		sPedStruct.iSwitchingINT	= CREATION_STAGE_WAIT
		SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)

		iTriggerCreationStage		= CREATION_STAGE_WAIT
		
		IF iTypePassed = CREATION_TYPE_VEHICLES
			UNLOAD_ALL_VEHICLE_MODELS()
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			UNLOAD_ALL_FMMC_WEAPON_ASSETS(TRUE, sWepStruct)
			UNLOAD_ALL_FMMC_WEAPON_ASSETS(FALSE, sWepStruct)
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1)
				IF DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
					CREATE_FMMC_BLIP(sPropStruct.biObject[i], GET_ENTITY_COORDS(sPropStruct.oiObject[i]), HUD_COLOUR_PURPLE, "FMMC_B_5", 1)
				ENDIF
			ENDFOR
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
		ELIF iTypePassed	=  CREATION_TYPE_TRIGGER
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				CREATE_FMMC_BLIP(sStartEndBlips.biStart, g_FMMC_STRUCT.vStartPos, HUD_COLOUR_WHITE, "FMMC_B_4", 1)	
				BLIP_SPRITE blip = GET_TRIGGER_RADAR_BLIP() 
				IF blip != RADAR_TRACE_INVALID
					SET_BLIP_SPRITE(sStartEndBlips.biStart, blip)
					SET_BLIP_NAME_FROM_TEXT_FILE(sStartEndBlips.biStart, "FMMC_B_4")	
				ENDIF
			ENDIF
		ENDIF	
		
		IF sFMMCMenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCMenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
		AND sFMMCMenu.sActiveMenu != eFmmc_PROP_ADVANCED_OPTIONS
		AND sFMMCMenu.sActiveMenu != eFmmc_SNAPPING_OPTIONS
		AND sFMMCMenu.sActiveMenu != eFMMC_OFFSET_POSITION
		AND sFMMCMenu.sActiveMenu != eFMMC_OFFSET_ROTATION
			PRINTLN("[LH][VANISH] 4")
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
			CLEAR_BIT(sFMMCmenu.iDynoPropBitset, ciFMMC_DYNOPROP_Position_Override)
		ENDIF
		
		IF sFMMCMenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCMenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
		AND sFMMCMenu.sActiveMenu != eFmmc_PROP_ADVANCED_OPTIONS
		AND sFMMCMenu.sActiveMenu != eFmmc_SNAPPING_OPTIONS
		AND sFMMCMenu.sActiveMenu != eFMMC_OFFSET_POSITION
		AND sFMMCMenu.sActiveMenu != eFMMC_OFFSET_ROTATION
			PRINTLN("[JJT] sFMMCmenu.vOverridePosition changed 8")
			SET_OVERRIDE_POSITION_COORDS(sFMMCmenu, <<0,0,0>>)
			SET_OVERRIDE_ROTATION_VECTOR(sFMMCmenu, <<0,0,0>>)
			PRINTLN("[PROP ROTATION](0) Setting override rotation to zero ")
		ENDIF
		
		SET_LONG_BIT(sFMMCmenu.iBitActive, PAN_CAM_PLACE)
		IF DOES_BLIP_EXIST(bCameraTriggerBlip)
			REMOVE_BLIP(bCameraTriggerBlip)
		ENDIF
		
		SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
		
		sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
		sHCS.hcCurrentCoronaColour = sHCS.hcDefaultCoronaColour	
		REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
		
	// If you're cancelling the creation of an entity you just picked up and need to put it back
	ELSE 
	
		PLAY_SOUND_FRONTEND(-1, "EDIT", GET_CREATOR_SPECIFIC_SOUND_SET())
		
		CLEAR_LONG_BIT(sEditedMisEntPlacementData.iBitset, ENUM_TO_INT(eMISSIONENTITYDATABITSET_OVERRIDE_POSITION))
		CLEAR_LONG_BIT(sEditedMisEntPlacementData.iBitset, ENUM_TO_INT(eMISSIONENTITYDATABITSET_OVERRIDE_ROTATION))
		
		CLEAR_LONG_BIT(sEditedPropPlacementData.iBitset, ENUM_TO_INT(ePROPDATABITSET_OVERRIDEN_POSITION))
		CLEAR_LONG_BIT(sEditedPropPlacementData.iBitset, ENUM_TO_INT(ePROPDATABITSET_OVERRIDEN_ROTATION))
		
		CLEAR_LONG_BIT(sEditedMovingDoorsPlacementData.iBitset, ENUM_TO_INT(eMOVINGDOORDATABITSET_OVERRIDE_POSITION))
		CLEAR_LONG_BIT(sEditedMovingDoorsPlacementData.iBitset, ENUM_TO_INT(eMOVINGDOORDATABITSET_OVERRIDE_ROTATION))
		
		CLEAR_BIT(sEditedGenericData.iBitset, ENUM_TO_INT(eGENERICDATABITSET_OVERRIDE_POSITION))
		CLEAR_BIT(sEditedGenericData.iBitset, ENUM_TO_INT(eGENERICDATABITSET_OVERRIDE_ROTATION))
		
		IF iTypePassed	=  CREATION_TYPE_VEHICLES		
			PRINTLN("IF iTypePassed	= CREATION_TYPE_VEHICLES")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_VEHICLE(sVehStruct, sFMMCMenu.iSelectedEntity)
			sVehStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_WEAPONS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_WEAPON(sFMMCmenu, sWepStruct, sFMMCMenu.iSelectedEntity, g_FMMC_STRUCT.iVehicleDeathmatch = 0, sCurrentVarsStruct)
			sWepStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_PROPS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_FMC_PROP(sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		ELIF iTypePassed	=  CREATION_TYPE_DYNOPROPS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_DYNOPROPS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_DYNOPROP(sPropStruct, sDynoPropStruct, sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		ELIF iTypePassed = CREATION_TYPE_PEDS
		
			PRINTLN("TypePassed	= CREATION_TYPE_PEDS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			IF DOES_ENTITY_EXIST(sPedStruct.piPed[sFMMCMenu.iSelectedEntity])	
				DELETE_PED(sPedStruct.piPed[sFMMCMenu.iSelectedEntity])
			ENDIF
			IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
				DELETE_PED(sPedStruct.piTempPed)
			ENDIF

			IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds >= FMMC_MAX_PEDS
				IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
					DELETE_PED(sPedStruct.piTempPed)
				ENDIF
			ENDIF
			
			sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
			
			REFRESH_FMC_MENUS()
			
			SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sEditedPed.mn)
		ENDIF	
		
	ENDIF

ENDPROC

PROC CLEAR_OPTION_DESCRIPTIONS()

	INT iDesc
	REPEAT COUNT_OF(sOptionDesc) iDesc
		sOptionDesc[iDesc] = ""
	ENDREPEAT
	
ENDPROC

// The main function that deals with things that happen when you press buttons in menus
// Called RETURN_CIRCLE_MENU_SELECTIONS in the old creators but with a more appropriate name here
FUNC enumFMMC_CIRCLE_MENU_ACTIONS DO_MENU_BUTTON_PRESS_ACTIONS()
	IF LOAD_MENU_ASSETS()
		DRAW_MENU_SELECTION()
	ENDIF
	
	BOOL bSubmenuActive = NOT IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack) 
	
	HANDLE_EVERY_FRAME_MENU_FUNCTIONALITY()
	
	IF IS_SCREEN_FADED_IN()
	
		// If you press Accept
		IF FFMC_IS_ACCEPT_JUST_PRESSED(sFMMCmenu) 
		OR (FMMC_IS_CIRCLE_MENU_A_CAMERA_MENU(sFMMCmenu.sActiveMenu) AND FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu))
			IF IS_THIS_OPTION_SELECTABLE()
				IF IS_THIS_OPTION_A_MENU_ACTION()
				OR IS_THIS_OPTION_A_MENU_GOTO() 
					IF (sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1)
					AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
						PLAY_SOUND_FRONTEND(-1, "SELECT", GET_SOUND_SET_FROM_CREATOR_TYPE())
						PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
					PRINTSTRING("Set up menu")PRINTNL()
					INITIALISE_FMC_PLACEMENTS(sFMMCmenu)
					//If it is then reset up the menu
					FMC_GO_TO_MENU(sFMMCmenu, sFMMCdata,sCamData)
					IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
						IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
							sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
						ENDIF
					ENDIF
					IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
						IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
							sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
						ENDIF
					ENDIF
					CLEAR_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)	
					
					RETURN eFmmc_Action_Null
				ENDIF
				
				IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
					IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
						IF bInitialIntroCamSetup
							RETURN eFmmc_Action_Null
						ELIF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT.vStartPos, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
							PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
							PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
							sCurrentVarsStruct.bDisplayFailReason = TRUE
							sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
							sCurrentVarsStruct.sFailReason = "FMMC_ER_023"
							RETURN eFmmc_Action_Null
						ENDIF
					ENDIF
					
					RETURN  sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
				ENDIF
				
				SWITCH sFMMCmenu.sActiveMenu
					CASE eFMMC_FMC_OVERRIDE_POSITION
						FMC_EDIT_OVERRIDDEN_POSITION_MENU(sFMMCMenu)
					BREAK
					CASE eFMMC_FMC_OVERRIDE_ROTATION
						FMC_EDIT_OVERRIDDEN_ROTATION_MENU(sFMMCMenu)
					BREAK
					CASE eFmmc_FMC_TRIGGER_AREA_OPTIONS
						EDIT_TRIGGER_AREA_OPTIONS(sFMMCmenu, sCurrentVarsStruct, 0)
					BREAK
					CASE eFmmc_FMC_POPULATION_UPDATE_MENU
						FMC_POPULATION_ACCEPT_ACTIONS(sFMMCMenu, sCurrentVarsStruct)
					BREAK
					CASE eFmmc_FMC_FOCUS_CAMS_UPDATE_MENU
						FMC_FOCUS_CAM_ACCEPT_ACTIONS(sFMMCMenu, sCurrentVarsStruct)
					BREAK
				ENDSWITCH
				
				REFRESH_FMC_MENUS()
				
			ELSE
				PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
			ENDIF
			
		ENDIF
		
		// If you press Cancel
		IF FMMC_IS_CANCEL_JUST_PRESSED(sFMMCmenu)
		OR SHOULD_FORCE_MENU_BACKOUT(sFMMCMenu)
		
			sCurrentVarsStruct.bDisplayFailReason = FALSE					
			IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
				IF IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
					CLEAR_BIT(sFMMCdata.iBitSet, bResetMenu)
				ENDIF
			ELSE
				sCurrentVarsStruct.bResetUpHelp = TRUE
				
				IF sFMMCMenu.sActiveMenu = eFMMC_FMC_VEH_MENU
					SET_VEHICLE_MENU_DEFAULTS()
				ENDIF
				
				INT iSaveEntityType = sFMMCmenu.iEntityCreation
				
				IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
						
					CANCEL_ENTITY_CREATION(sFMMCmenu.iEntityCreation)
				ENDIF
				
				IF bSubmenuActive
					SET_CREATION_TYPE(sFMMCMenu, iSaveEntityType)
				ENDIF
				
				IF (sFMMCMenu.iSelectedEntity != -1 AND NOT bSubmenuActive)
					sFMMCMenu.iSelectedEntity = -1
					
				ELIF sFMMCmenu.sMenuBack != eFmmc_Null_item
					PLAY_SOUND_FRONTEND(-1, "BACK", GET_SOUND_SET_FROM_CREATOR_TYPE())
					GO_BACK_TO_MENU(sFMMCmenu)
					CLEAR_OPTION_DESCRIPTIONS()
					sFMMCmenu.sSubTypeName[CREATION_TYPE_CYCLE] = ""
				ENDIF
				IF sFMMCMenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
				OR sFMMCMenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
					CHECK_IF_SNAPPING_SHOULD_BE_TURNED_OFF(sFMMCmenu)
				ENDIF
				
			ENDIF
			REFRESH_FMC_MENUS()
		ENDIF
		
		// If you press Square
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			INT iCurrentSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
			IF sFMMCMenu.sActiveMenu = eFmmc_FMC_SCENARIOS_MENU
				IF NOT IS_STRING_NULL_OR_EMPTY(sMissionPlacementData.Scenario[iCurrentSelection].sName)
					sMissionPlacementData.Scenario[iCurrentSelection].sName = ""
					INT iPed
					FOR iPed = 0 TO MAX_NUM_PEDS-1
						IF sMissionPlacementData.Ped.Peds[iPed].iScenarioIndex = iCurrentSelection
							sMissionPlacementData.Ped.Peds[iPed].iScenarioIndex = -1
						ENDIF
					ENDFOR
					IF sEditedPedPlacementData.iScenarioIndex = iCurrentSelection
						sEditedPedPlacementData.iScenarioIndex = -1
					ENDIF
					REFRESH_FMC_MENUS()
				ENDIF
			ELIF sFMMCMenu.sActiveMenu = eFmmc_FMC_ANIM_DICT_MENU
				IF NOT IS_STRING_NULL_OR_EMPTY(sMissionPlacementData.Animation.Dict[iCurrentSelection].sName)
					sMissionPlacementData.Animation.Dict[iCurrentSelection].sName = ""
					INT i
					FOR i = 0 TO MAX_NUM_ANIMATIONS-1
						IF sMissionPlacementData.Animation.Anim[i].iDictIndex = iCurrentSelection
							sMissionPlacementData.Animation.Anim[i].iDictIndex = -1
						ENDIF
					ENDFOR
					REFRESH_FMC_MENUS()
				ENDIF
			ELIF sFMMCMenu.sActiveMenu = eFmmc_FMC_ANIM_MENU
				IF IS_MENU_ITEM_SELECTED("FMC_ANMO_OP")
					iCurrentSelection /= 2
					IF NOT IS_STRING_NULL_OR_EMPTY(sMissionPlacementData.Animation.Anim[iCurrentSelection].sName)
						sMissionPlacementData.Animation.Anim[iCurrentSelection].sName = ""
						INT i
						FOR i = 0 TO MAX_NUM_PEDS-1
							IF sMissionPlacementData.Ped.Peds[i].iAnimIndex = iCurrentSelection
								sMissionPlacementData.Ped.Peds[i].iAnimIndex = -1
							ENDIF
						ENDFOR
						IF sEditedPedPlacementData.iAnimIndex = iCurrentSelection
							sEditedPedPlacementData.iAnimIndex = -1
						ENDIF
						REFRESH_FMC_MENUS()
					ENDIF
				ENDIF
			ENDIF
			
			SWITCH sFMMCMenu.sActiveMenu
				CASE eFmmc_FMC_TRIGGER_AREAS
					DELETE_THIS_FMC_TRIGGER_AREA(sFMMCmenu, sFMMCMenu.iSelectedFMCEntity)
				BREAK
				CASE eFmmc_FMC_TRIGGER_AREA_OPTIONS
					CLEAR_TRIGGER_AREA_OPTIONS(sFMMCmenu)
				BREAK
				CASE eFmmc_FMC_SEARCH_AREA_LIST_MENU
					PRINTLN("[SEARCH_AREA] DELETE DO_MENU_BUTTON_PRESS_ACTIONS ", sFMMCMenu.iSelectedFMCEntity)
					DELETE_FMC_PLACED_SEARCH_AREA(sFMMCMenu, sFMMCMenu.iSelectedFMCEntity)
				BREAK
				CASE eFmmc_FMC_SEARCH_AREA_OPTIONS
					CLEAR_SEARCH_AREA_OPTIONS(sFMMCmenu)
				BREAK
				CASE eFmmc_FMC_RESTRICTED_INTERIORS
					DELETE_RESTRICTED_INTERIOR_OPTION(sFMMCmenu)
				BREAK
				CASE eFmmc_FMC_PED_GROUPS_LIST_MENU
					DELETE_FMC_PED_GROUP(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
				BREAK
				CASE eFmmc_FMC_TRAILERS_LIST_MENU
					DELETE_FMC_TRAILER(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
				BREAK
				CASE eFmmc_FMC_PATROLS_LIST_MENU
					DELETE_FMC_PATROL(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
				BREAK
				CASE eFmmc_FMC_POPULATIONS_LIST_MENU
					DELETE_FMC_POPULATION(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
				BREAK
				CASE eFmmc_FMC_FOCUS_CAMS_UPDATE_MENU
					DELETE_FMC_FOCUS_CAM(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
				BREAK
				CASE eFmmc_FMC_TARGET_LIST_MENU
					DELETE_FMC_TARGET(sFMMCmenu, sFMMCMenu.iSelectedFMCEntity)
				BREAK
				CASE eFmmc_FMC_SECURITY_CAMERA_OPTIONS_MENU
					IF sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].iProp != -1
						DELETE_FMC_SECURITY_CAMERA(sFMMCMenu.iSelectedFMCEntity)
						GO_BACK_TO_MENU(sFMMCmenu)
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
					ENDIF
				BREAK
				CASE eFmmc_FMC_AMBUSH_MENU
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > ciFMC_AMB_RESPAWN_DELAY
						DELETE_FMC_AMBUSH(sFMMCMenu.iSelectedFMCEntity)
					ENDIF
				BREAK
			ENDSWITCH
			
			REFRESH_FMC_MENUS()
			
		ENDIF
		
		// If you press Triangle
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			IF sFMMCMenu.sActiveMenu = eFmmc_FMC_RESTRICTED_INTERIORS
				INT iCurrentSelection = GET_INTERIOR_SELECTED_IN_MENU(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
				IF sMissionPlacementData.Interior.eInteriors[iCurrentSelection].eType != eFMCINTERIOR_INVALID
					sCurrentVarsStruct.iMenuState = MENU_STATE_WARP_TO_INTERIOR
				ENDIF
			ENDIF
		ENDIF
		
		// If you press Down
		IF MENU_CONTROL_DOWN_CREATOR(menuScrollController)
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)
			
			WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS(1)
				PRINTLN("[TMS] Skipping options (down)...")
			ENDWHILE
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > sFMMCmenu.iCurrentMenuLength-1
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
			ENDIF
			IF sFMMCmenu.iCurrentMenuLength > 1
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("SCROLL SOUND EFFECT!!!")PRINTNL()
			ENDIF
				
			IF NOT sFMMCendStage.bHasValidROS
				sFMMCendStage.bHasValidROS = NETWORK_HAS_VALID_ROS_CREDENTIALS()
			ENDIF
			
			sCurrentVarsStruct.bResetUpHelp  = TRUE
			
			REFRESH_FMC_MENUS()
		ENDIF
		
		// If you press Up
		IF MENU_CONTROL_UP_CREATOR(menuScrollController)
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
			WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS(-1)
				PRINTLN("[TMS] Skipping options (up)...")
			ENDWHILE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iCurrentMenuLength - 1)
			ENDIF
			IF sFMMCmenu.iCurrentMenuLength > 1
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("SCROLL SOUND EFFECT!!!")PRINTNL()
			ENDIF
			
			sCurrentVarsStruct.bResetUpHelp  = TRUE
			
			REFRESH_FMC_MENUS()
		ENDIF
		
		// If you press Left or Right
		INT iChange = 0
		IF MENU_CONTROL_RIGHT_CREATOR(menuScrollController)
			iChange = 1
		ENDIF
		IF MENU_CONTROL_LEFT_CREATOR(menuScrollController)
			iChange = -1
		ENDIF
		IF iChange != 0
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_PICKUP_OVERRIDE_USE_OVERRIDE
				TOGGLE_MENU_BIT(sFMMCmenu.iWepPlacedBitset, ciFMMC_WEP_Position_Override)
				IF NOT IS_BIT_SET(sFMMCmenu.iWepPlacedBitset, ciFMMC_WEP_Position_Override)
					SET_OVERRIDE_POSITION_COORDS(sFMMCmenu, <<0,0,0>>)
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_PICKUP_OVERRIDE_ALIGNMENT
				IF sFMMCMenu.sActiveMenu = eFMMC_FMC_OVERRIDE_POSITION
					IF sFMMCmenu.iCurrentAlignment = FREE_POSITION_WORLD
						sFMMCmenu.iCurrentAlignment = FREE_POSITION_LOCAL
					ELSE
						sFMMCmenu.iCurrentAlignment = FREE_POSITION_WORLD
					ENDIF
				ELIF sFMMCMenu.sActiveMenu = eFMMC_FMC_OVERRIDE_ROTATION
					IF sFMMCmenu.iCurrentAlignment_Rotation = FREE_POSITION_WORLD
						sFMMCmenu.iCurrentAlignment_Rotation = FREE_POSITION_LOCAL
					ELSE
						sFMMCmenu.iCurrentAlignment_Rotation = FREE_POSITION_WORLD
					ENDIF
				ENDIF
				//FREE_POSITION_VIEW exists but was not desired.
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_PICKUP_OVERRIDE_RESET
				IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
					IF sFMMCMenu.sActiveMenu = eFMMC_FMC_OVERRIDE_POSITION
						SET_OVERRIDE_POSITION_COORDS(sFMMCmenu, <<0,0,0>>)
					ELIF sFMMCMenu.sActiveMenu = eFMMC_FMC_OVERRIDE_ROTATION
						SET_OVERRIDE_ROTATION_VECTOR(sFMMCmenu, <<0,0,0>>)
					ENDIF
				ENDIF
			ENDIF
			EDIT_MENU_OPTIONS(iChange)
			REFRESH_FMC_MENUS()
		ENDIF
		
		// If you press the touchpad/select button to change cameras
		HANDLE_SWITCH_TO_GROUND_CAMERA()
		
	ENDIF
	
	RETURN eFmmc_Action_Null
ENDFUNC

PROC ENABLE_FMC_INTERIOR_INDEX(FMC_MISSION_INTERIOR eInterior, INT iVariation, INTERIOR_INSTANCE_INDEX iInteriorIndex, BOOL bActive)
	IF bActive
		PRINTLN("ENABLE_FMC_INTERIOR_INDEX - Pinning ", GET_FMC_MISSION_INTERIOR_NAME(eInterior))
		PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
	ELSE
		PRINTLN("ENABLE_FMC_INTERIOR_INDEX - Unpinning ", GET_FMC_MISSION_INTERIOR_NAME(eInterior))
		UNPIN_INTERIOR(iInteriorIndex)
	ENDIF
	
	IF IS_INTERIOR_CAPPED(iInteriorIndex)
		CAP_INTERIOR(iInteriorIndex, (NOT bActive))
		PRINTLN("ENABLE_FMC_INTERIOR_INDEX - Set CAP_INTERIOR to ", GET_STRING_FROM_BOOL(NOT bActive)) 
	ENDIF
	IF IS_INTERIOR_DISABLED(iInteriorIndex)
		DISABLE_INTERIOR(iInteriorIndex, (NOT bActive))
		PRINTLN("ENABLE_FMC_INTERIOR_INDEX - Set DISABLE_INTERIOR to ", GET_STRING_FROM_BOOL(NOT bActive))
	ENDIF
	
	SETUP_FMC_INTERIOR_ENTITY_SETS(eInterior, iVariation, iInteriorIndex, bActive)
	
	IF bActive
		REFRESH_INTERIOR(iInteriorIndex)
	ENDIF
ENDPROC

PROC SET_ALL_ENTITY_FMC_INTERIORS()
	INT iLoop, iInterior
	REPEAT sMissionPlacementData.Ped.iCount iLoop
		iInterior = GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(sMissionPlacementData.Ped.Peds[iLoop].vCoords)
		IF iInterior != (-1)
		AND sMissionPlacementData.Ped.Peds[iLoop].iInterior != iInterior
			sMissionPlacementData.Ped.Peds[iLoop].iInterior = iInterior
			PRINTLN("SET_ALL_ENTITY_FMC_INTERIORS - Set ped ", iLoop, " interior to ", GET_FMC_MISSION_INTERIOR_NAME(sMissionPlacementData.Interior.eInteriors[iInterior].eType))
		ENDIF
	ENDREPEAT
	
	REPEAT sMissionPlacementData.Vehicle.iCount iLoop
		iInterior = GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(sMissionPlacementData.Vehicle.Vehicles[iLoop].vCoords)
		IF iInterior != (-1)
		AND sMissionPlacementData.Vehicle.Vehicles[iLoop].iInterior != iInterior
			sMissionPlacementData.Vehicle.Vehicles[iLoop].iInterior = iInterior
			PRINTLN("SET_ALL_ENTITY_FMC_INTERIORS - Set vehicle ", iLoop, " interior to ", GET_FMC_MISSION_INTERIOR_NAME(sMissionPlacementData.Interior.eInteriors[iInterior].eType))
		ENDIF
	ENDREPEAT
	
	REPEAT sMissionPlacementData.Prop.iCount iLoop
		iInterior = GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(sMissionPlacementData.Prop.Props[iLoop].vCoords)
		IF iInterior != (-1)
		AND sMissionPlacementData.Prop.Props[iLoop].iInterior != iInterior
			sMissionPlacementData.Prop.Props[iLoop].iInterior = iInterior
			PRINTLN("SET_ALL_ENTITY_FMC_INTERIORS - Set prop ", iLoop, " interior to ", GET_FMC_MISSION_INTERIOR_NAME(sMissionPlacementData.Interior.eInteriors[iInterior].eType))
		ENDIF
	ENDREPEAT
	
	REPEAT sMissionPlacementData.MissionEntity.iCount iLoop
		iInterior = GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(sMissionPlacementData.MissionEntity.MissionEntities[iLoop].vCoords)
		IF iInterior != (-1)
		AND sMissionPlacementData.MissionEntity.MissionEntities[iLoop].iInterior != iInterior
			sMissionPlacementData.MissionEntity.MissionEntities[iLoop].iInterior = iInterior
			PRINTLN("SET_ALL_ENTITY_FMC_INTERIORS - Set mission entity ", iLoop, " interior to ", GET_FMC_MISSION_INTERIOR_NAME(sMissionPlacementData.Interior.eInteriors[iInterior].eType))
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUM_MISSION_PORTALS iLoop
		iInterior = GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(sMissionPlacementData.Portal[iLoop].vCoord)
		IF iInterior != (-1)
		AND NOT IS_ARRAYED_BIT_ENUM_SET(sMissionPlacementData.Portal[iLoop].iBitset, ePORTALDATABITSET_INSIDE_INTERIOR)
			SET_ARRAYED_BIT_ENUM(sMissionPlacementData.Portal[iLoop].iBitset, ePORTALDATABITSET_INSIDE_INTERIOR)
			PRINTLN("SET_ALL_ENTITY_FMC_INTERIORS - Set portal ", iLoop, " to inside interior. Interior: ", GET_FMC_MISSION_INTERIOR_NAME(sMissionPlacementData.Interior.eInteriors[iInterior].eType))
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_ENTITY_SET_FOR_INTERIOR(FMC_MISSION_INTERIOR eInterior)
	INT iLoop
	REPEAT MAX_NUM_MISSION_INTERIORS iLoop
		IF sMissionPlacementData.Interior.eInteriors[iLoop].eType = eInterior
			RETURN sMissionPlacementData.Interior.eInteriors[iLoop].iVariation
		ENDIF
	ENDREPEAT
	RETURN 0
ENDFUNC								

FUNC BOOL HANDLE_FMC_INTERIOR_WARP()

	INT iInteriorSelected = GET_INTERIOR_SELECTED_IN_MENU(GET_CREATOR_MENU_SELECTION(sFMMCMenu))
	FMC_MISSION_INTERIOR eInterior = sMissionPlacementData.Interior.eInteriors[iInteriorSelected].eType
	INT iInteriorVariation = sMissionPlacementData.Interior.eInteriors[iInteriorSelected].iVariation
	
	INTERIOR_INSTANCE_INDEX iIdx = NULL
	VECTOR vInteriorCoord
	STRING sInteriorName
	
	IF eInterior != eFMCINTERIOR_INVALID
		IF GET_FMC_INTERIOR_RESTRICTED_INTERIOR_ENUM(eInterior) != RESTRICTED_INTERIOR_INVALID
			sInteriorName = GET_RESTRICTED_ACCESS_INTERIOR_NAME(GET_FMC_INTERIOR_RESTRICTED_INTERIOR_ENUM(eInterior))
			vInteriorCoord = GET_RESTRICTED_INTERIOR_COORDS(GET_FMC_INTERIOR_RESTRICTED_INTERIOR_ENUM(eInterior))
		ELSE
			sInteriorName = GET_FMC_INTERIOR_STRING_FOR_WARP(eInterior)
			vInteriorCoord = GET_FMC_INTERIOR_COORD_FOR_WARP(eInterior)
		ENDIF
		
		SETUP_FMC_INTERIOR_IPLS(eInterior, iInteriorVariation, TRUE)
		MAINTAIN_FMC_INTERIOR_SETTINGS(eInterior, TRUE)
		
		IF NOT IS_VECTOR_ZERO(vInteriorCoord)
		AND NOT IS_STRING_NULL_OR_EMPTY(sInteriorName)
			iIdx = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorCoord, sInteriorName)
			PRINTLN("HANDLE_FMC_INTERIOR_WARP - Grabbed interior index = ", NATIVE_TO_INT(iIdx))
		ENDIF
	ENDIF

	SWITCH iWarpState
	
		CASE INTERIOR_WARP_STATE_INTIALISE
			//iWarpStateTimeoutStart = 0
			PRINTLN("HANDLE_FMC_INTERIOR_WARP - IN INTERIOR_WARP_STATE_INTIALISE - vInteriorCoord = ",vInteriorCoord, " sInteriorName = ",sInteriorName)

			IF iIdx != NULL
				IF eCurrentInterior != eFMCINTERIOR_INVALID
					IF eCurrentInterior != eInterior
					OR iCurrentInteriorVar != iInteriorVariation
						INTERIOR_INSTANCE_INDEX currentInterior 
						currentInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_FMC_INTERIOR_COORD_FOR_WARP(eCurrentInterior), GET_FMC_INTERIOR_STRING_FOR_WARP(eCurrentInterior))
						IF IS_VALID_INTERIOR(currentInterior)	
							iInteriorVariation = GET_ENTITY_SET_FOR_INTERIOR(eCurrentInterior)
							ENABLE_FMC_INTERIOR_INDEX(eCurrentInterior, iCurrentInteriorVar, currentInterior, FALSE)
						ENDIF
						PRINTLN("HANDLE_FMC_INTERIOR_WARP - Disabling current interior (", GET_FMC_MISSION_INTERIOR_NAME(eCurrentInterior),", variation ", iInteriorVariation,") for new one (", GET_FMC_MISSION_INTERIOR_NAME(eInterior),")")
					ENDIF
				ENDIF
				
				IF IS_VALID_INTERIOR(iIdx)
					ENABLE_FMC_INTERIOR_INDEX(eInterior, sMissionPlacementData.Interior.eInteriors[iInteriorSelected].iVariation, iIdx, TRUE)
					eCurrentInterior = eInterior
					iCurrentInteriorVar = sMissionPlacementData.Interior.eInteriors[iInteriorSelected].iVariation
					PRINTLN("HANDLE_FMC_INTERIOR_WARP - Activating ", GET_FMC_MISSION_INTERIOR_NAME(eInterior), ", variation ", sMissionPlacementData.Interior.eInteriors[iInteriorSelected].iVariation, ". Updating eCurrentInterior.")
					SET_INTERIOR_WARP_STATE(INTERIOR_WARP_STATE_WAIT_FOR_READY, iWarpState)
				ELSE
					SET_INTERIOR_WARP_STATE(INTERIOR_WARP_STATE_FAILED, iWarpState)
					PRINTLN("HANDLE_FMC_INTERIOR_WARP - Interior is not null, but no valid either.")
				ENDIF
			ELSE
				SET_INTERIOR_WARP_STATE(INTERIOR_WARP_STATE_FAILED, iWarpState)
				PRINTLN("HANDLE_FMC_INTERIOR_WARP - Invalid interior found at warp coords.")
			ENDIF
		BREAK
		
		CASE INTERIOR_WARP_STATE_WAIT_FOR_READY
	
			IF NOT IS_SCREEN_FADING_OUT()
			AND NOT IS_SCREEN_FADED_OUT()
				PRINTLN("HANDLE_FMC_INTERIOR_WARP - INTERIOR_WARP_STATE_WAIT_FOR_READY - fading out")
				DO_SCREEN_FADE_OUT(500)
			ELIF IS_SCREEN_FADED_OUT()
				PRINTLN("HANDLE_FMC_INTERIOR_WARP - Valid interior found (", GET_FMC_MISSION_INTERIOR_NAME(eInterior), ") - Waiting on ready.")
				SET_INTERIOR_WARP_STATE(INTERIOR_WARP_STATE_SET_POSITION, iWarpState)
			ENDIF
		BREAK
		
		CASE INTERIOR_WARP_STATE_SET_POSITION
			IF NOT IS_SCREEN_FADING_OUT()
			AND NOT IS_SCREEN_FADED_OUT()
				PRINTLN("HANDLE_FMC_INTERIOR_WARP - INTERIOR_WARP_STATE_SET_POSITION - fading out")
				DO_SCREEN_FADE_OUT(500)
			ELIF IS_SCREEN_FADED_OUT()
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					PRINTLN("HANDLE_FMC_INTERIOR_WARP - Swapping camera to on foot")
					SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, TRUE, FALSE, TRUE, IS_THIS_A_RACE())
				ENDIF
				ENTITY_INDEX eiToWarp
				eiToWarp = PLAYER_PED_ID()
				IF IS_THIS_A_RACE()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						eiToWarp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					ENDIF
				ENDIF
				SET_ENTITY_COORDS(eiToWarp, vInteriorCoord)
				SET_ENTITY_HEADING(eiToWarp,	6.7417)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-23.8335)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-8.3001)
				FREEZE_ENTITY_POSITION(eiToWarp, FALSE)
				PRINTLN("HANDLE_FMC_INTERIOR_WARP - Setting player coords to ", vInteriorCoord)
				SET_INTERIOR_WARP_STATE(INTERIOR_WARP_STATE_RETURN_TO_DEFAULT, iWarpState)
			ENDIF
		BREAK
		
		CASE INTERIOR_WARP_STATE_FAILED
			IF NOT IS_SCREEN_FADING_OUT()
			AND NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(500)
				PRINTLN("HANDLE_FMC_INTERIOR_WARP - INTERIOR_WARP_STATE_FAILED - fading out ")
			ELIF IS_SCREEN_FADED_OUT()
				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS("FMMC_SEL_FAILD", "FMMC_INT_FAIL", FE_WARNING_OK)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					SET_INTERIOR_WARP_STATE(INTERIOR_WARP_STATE_RETURN_TO_DEFAULT, iWarpState)								
				ENDIF
			ENDIF
		BREAK
		
		CASE INTERIOR_WARP_STATE_RETURN_TO_DEFAULT
			SET_ALL_ENTITY_FMC_INTERIORS()
		
			DO_SCREEN_FADE_IN(500)
			PRINTLN("HANDLE_FMC_INTERIOR_WARP - INTERIOR_WARP_STATE_RETURN_TO_DEFAULT - finished, intitialising warp stage")
			SET_INTERIOR_WARP_STATE(INTERIOR_WARP_STATE_INTIALISE, iWarpState)
			sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	RETURN FALSE	
ENDFUNC

FUNC BOOL SHOULD_MAINTAIN_QUICK_SET_PED_SCENARIO()
	IF NOT IS_PC_VERSION()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_QUICK_SET_PED_SCENARIO_INPUT()
	INT iChange
	
	IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		iChange = -ciFMC_QUICK_SET_PED_SCENARIO_CHANGE_SMALL
	ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		iChange = ciFMC_QUICK_SET_PED_SCENARIO_CHANGE_SMALL
	ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
		iChange = -ciFMC_QUICK_SET_PED_SCENARIO_CHANGE_MEDIUM
	ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
		iChange = ciFMC_QUICK_SET_PED_SCENARIO_CHANGE_MEDIUM
	ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LS)
		iChange = -ciFMC_QUICK_SET_PED_SCENARIO_CHANGE_LARGE
	ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RS)
		iChange = ciFMC_QUICK_SET_PED_SCENARIO_CHANGE_LARGE
	ENDIF
	
	IF iChange != 0
		INT iScenario = ENUM_TO_INT(sQuickSetPedScenarioStruct.eScenario)
		
		IF iScenario = -1 AND iChange < 0
			iScenario = 0
		ENDIF
		
		iScenario = IWRAP_INDEX(iScenario + iChange, COUNT_OF(PED_SCENARIO))
		
		PRINTLN("[QUICK_SET_PED_SCENARIO] MAINTAIN_QUICK_SET_PED_SCENARIO_INPUT - iChange = ", iChange, ", iScenario = ", iScenario, ". Current scenario = ", GET_PED_SCENARIO_STRING(sQuickSetPedScenarioStruct.eScenario), ", new scenario = ", GET_PED_SCENARIO_STRING(INT_TO_ENUM(PED_SCENARIO, iScenario)))
		
		sQuickSetPedScenarioStruct.eScenario = INT_TO_ENUM(PED_SCENARIO, iScenario)
	ENDIF
ENDPROC

PROC MAINTAIN_QUICK_SET_PED_SCENARIO_UPDATE(TEXT_LABEL_63& tlScenario)
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMMCD_DEV_QSPS")
			CLEAR_HELP()
		ENDIF
	ELSE
		PRINT_HELP_FOREVER("FMMCD_DEV_QSPS")
	ENDIF
	
	IF sQuickSetPedScenarioStruct.eScenarioCached != sQuickSetPedScenarioStruct.eScenario
		PRINTLN("[QUICK_SET_PED_SCENARIO] MAINTAIN_QUICK_SET_PED_SCENARIO_UPDATE - ", GET_PED_SCENARIO_STRING(sQuickSetPedScenarioStruct.eScenarioCached), " != ", GET_PED_SCENARIO_STRING(sQuickSetPedScenarioStruct.eScenario), ", updating scenario.")
		
		sQuickSetPedScenarioStruct.eScenarioCached = sQuickSetPedScenarioStruct.eScenario
		
		sFMMCendStage.oskStatus = OSK_PENDING
		sFMMCendStage.iKeyBoardStatus = OSK_STAGE_SET_UP
		
		tlScenario = GET_PED_SCENARIO_STRING(sQuickSetPedScenarioStruct.eScenario)
	ENDIF
ENDPROC

PROC MAINTAIN_QUICK_SET_PED_SCENARIO(TEXT_LABEL_63& tlPassed)
	IF NOT SHOULD_MAINTAIN_QUICK_SET_PED_SCENARIO()
		EXIT
	ENDIF
	
	MAINTAIN_QUICK_SET_PED_SCENARIO_INPUT()
	MAINTAIN_QUICK_SET_PED_SCENARIO_UPDATE(tlPassed)
ENDPROC

PROC CLEANUP_QUICK_SET_PED_SCENARIO()
	PRINTLN("[QUICK_SET_PED_SCENARIO] CLEANUP_QUICK_SET_PED_SCENARIO")
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMMCD_DEV_QSPS")
		CLEAR_HELP()
	ENDIF
	
	sQuickSetPedScenarioStruct.eScenario = ePEDSCENARIO_INVALID
	sQuickSetPedScenarioStruct.eScenarioCached = ePEDSCENARIO_INVALID
ENDPROC

PROC CLEANUP_ENTER_SIMPLE_TL63()
	SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_FMC_SCENARIOS_MENU
			CLEANUP_QUICK_SET_PED_SCENARIO()
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_MENU_STATE_ENTER_SIMPLE_TL63(TEXT_LABEL_63& tlPassed)
	SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_FMC_SCENARIOS_MENU
			MAINTAIN_QUICK_SET_PED_SCENARIO(tlPassed)
		BREAK
	ENDSWITCH
ENDPROC

// Menu state machine that spends most of it's time in default but has conditions for if you're doing things like entering text or setting cameras
// Most of these are ignored in the FMC creator
PROC HANDLE_MENU_STATE()
	SWITCH sCurrentVarsStruct.iMenuState
		CASE MENU_STATE_DEFAULT
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			IF sFMMCMenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
				sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
				sCurrentVarsStruct.sCoronaShapeTest.bDoStartShapeTest = FALSE
				sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL	
			ENDIF
			
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				IF SHOULD_MENU_BE_DRAWN(sFMMCData, sFMMCMenu, sCurrentVarsStruct, menuScrollController, 1 + g_FMMC_STRUCT.iVehicleDeathmatch, sFMMCMenu.sActiveMenu = eFmmc_TOP_MENU, bShowMenu)
					IF MAINTAIN_FMMC_RADIO(sFMMCmenu, menuScrollController)
						sCurrentVarsStruct.bResetUpHelp = TRUE
						REFRESH_FMC_MENUS()
					ENDIF
					FMMC_DO_MENU_ACTIONS(DO_MENU_BUTTON_PRESS_ACTIONS(), sCurrentVarsStruct, sFMMCmenu, sFMMCData, sFMMCendStage, sHCS)
					HANDLE_ENTITY_CREATION()
					SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(2)
				ELSE
					sCurrentVarsStruct.bDiscVisible = FALSE
					
					sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
					sWepStruct.iSwitchingINT = CREATION_STAGE_WAIT
					sVehStruct.iSwitchingINT = CREATION_STAGE_WAIT

					SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
					
					IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
						DELETE_VEHICLE(sVehStruct.viCoronaVeh)
					ENDIF
					IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
						DELETE_OBJECT(sWepStruct.viCoronaWep)
					ENDIF
					IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
						DELETE_OBJECT(sPropStruct.viCoronaObj)
					ENDIF
					DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND sCurrentVarsStruct.iMenuState != MENU_STATE_SWITCH_CAM
				BOOL bReloadMenu
				IF NOT IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					DEAL_WITH_SKY_CAM_SWITCH(sFMMCdata,sCycleCamStruct.bSwitchingCam,sCycleCamStruct.vSwitchVec,sCamData, bReloadMenu)
				ELSE
					DEAL_WITH_PLAYER_WARP_TO_ITEM(sFMMCdata,sCycleCamStruct.bSwitchingCam,sCycleCamStruct.vSwitchVec,sCycleCamStruct.fSwitchHeading, bReloadMenu)
				ENDIF
				IF bReloadMenu
					REFRESH_FMC_MENUS()
				ENDIF
			ENDIF
			
			CHECK_FOR_MENU_SET_UP(sCurrentVarsStruct, sPedStruct, sVehStruct, sWepStruct, sObjStruct, sFMMCmenu)
			IF sCurrentVarsStruct.bResetUpHelp
			AND (sCurrentVarsStruct.bFirstShapeTestCheckDone OR sFMMCmenu.iEntityCreation = -1 OR sFMMCmenu.iEntityCreation = CREATION_TYPE_PAN_CAM)
				sCurrentVarsStruct.bResetUpHelp  = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_PLACE_CAM
			IF PPS.iStage != WAIT_PREVIEW_PHOTO
			AND PPS.iStage != CLEANUP_PREVIEW_PHOTO
			AND sFMMCendStage.sTakePhotoVars.iTakePhotoStage != ciFMMC_TAKE_PHOTO_STAGE_OPEN_SHUT
				PPS.iStage = CLEANUP_PREVIEW_PHOTO
				PPS.iPreviewPhotoDelayCleanup = 0
			ENDIF
			
			IF sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE							
				REMOVE_DECALS_FOR_PHOTOS(sFMMCendStage.sTakePhotoVars.iTakePhotoStage, sStartEndBlips, sPedStruct, sTeamSpawnStruct, sWepStruct, sObjStruct, sHCS.hcStartCoronaColour)
			ENDIF
			IF TAKE_PHOTO_FOR_FMMC(sFMMCendStage.sTakePhotoVars, SF_Movie_Gallery_Shutter_Index, sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
				IF sFMMCMenu.sActiveMenu = eFmmc_PAN_CAM_BASE
					g_FMMC_STRUCT.vCameraPanPos = GET_CAM_COORD(GET_RENDERING_CAM())
					g_FMMC_STRUCT.vCameraPanRot = GET_CAM_ROT(GET_RENDERING_CAM())
					VECTOR vRot
					vRot = GET_CAM_ROT(GET_RENDERING_CAM())
					g_FMMC_STRUCT.fCameraPanHead = vRot.z
					PRINTLN("PAN CAM POSITIONED Rot - ", g_FMMC_STRUCT.vCameraPanRot, " Vector - ", g_FMMC_STRUCT.vCameraPanPos)
				ENDIF
				sFMMCendStage.bMajorEditOnLoadedMission = TRUE
				REFRESH_FMC_MENUS()
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
			ENDIF
		BREAK
		
		CASE MENU_STATE_PAN_CAM
			IF DEAL_WITH_CREATOR_CAMERA_PAN_PREVIEW(iCamPanState, jobIntroData, vInitialPreviewPos)	
				IF !SCRIPT_IS_CLOUD_AVAILABLE()
					IF NOT IS_SCREEN_FADED_IN()
						DO_SCREEN_FADE_IN(500)
					ENDIF	
				ENDIF
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
			ENDIF
		BREAK
		
		CASE MENU_STATE_TITLE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, TRUE)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_FMC_MENUS()
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_DESCRIPTION
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_FMC_MENUS()
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_TAGS
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, TRUE)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_FMC_MENUS()
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ENTER_SIMPLE_TL63
			BOOL bCancel
			IF bDelayAFrame = FALSE
				TEXT_LABEL_63 tlTemp
				MAINTAIN_MENU_STATE_ENTER_SIMPLE_TL63(tlTemp)
				
				IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, tlTemp)
					SET_SIMPLE_TEXT_ENTRY(sFMMCmenu, tlTemp)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					CLEANUP_ENTER_SIMPLE_TL63()
					REFRESH_FMC_MENUS()
					bDelayAFrame = TRUE
					
					#IF IS_DEBUG_BUILD
						SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
					#ENDIF
				ELSE
					IF bCancel
						sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						CLEANUP_ENTER_SIMPLE_TL63()
						REFRESH_FMC_MENUS()
						bDelayAFrame = TRUE
					ENDIF
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_SWITCH_CAM
			IF NOT IS_SCREEN_FADED_OUT()
				PRINTSTRING("screen not faded out yet, calling fade out")PRINTNL()
				DO_SCREEN_FADE_OUT(500)								
				IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
					SET_ENTITY_ALPHA(sWepStruct.viCoronaWep, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
					SET_ENTITY_ALPHA(sPropStruct.viCoronaObj, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
					SET_ENTITY_ALPHA(sDynoPropStruct.viCoronaObj, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
					SET_ENTITY_ALPHA(sVehStruct.viCoronaVeh, 0, FALSE)
				ENDIF
			ELSE
				PRINTSTRING("Screen is faded out do swap cams")PRINTNL()
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,TRUE,FALSE,TRUE,FALSE)
				ELSE
					SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,FALSE,FALSE,TRUE,FALSE)
				ENDIF
				NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20)
				sCurrentVarsStruct.iMenuState = MENU_STATE_LOADING_AREA
				iLoadingOverrideTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
		
		CASE MENU_STATE_LOADING_AREA
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			OR IS_NEW_LOAD_SCENE_LOADED()	
			OR iLoadingOverrideTimer + 8000 < GET_GAME_TIMER()
			
				PRINTSTRING("LOADED, FADE IN")PRINTNL()
				IF NOT sCycleCamStruct.bSwitchingCam
					DO_SCREEN_FADE_IN(500)
				ENDIF
				NEW_LOAD_SCENE_STOP()
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				ELSE
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
					RESET_ENTITY_ALPHA(sWepStruct.viCoronaWep)
				ENDIF
				IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
					RESET_ENTITY_ALPHA(sPropStruct.viCoronaObj)
				ENDIF
				IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
					RESET_ENTITY_ALPHA(sDynoPropStruct.viCoronaObj)
				ENDIF
				IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
					RESET_ENTITY_ALPHA(sVehStruct.viCoronaVeh)
				ENDIF								
				
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT

			ELSE							
				PRINTSTRING("WAITING FOR SCEEN TO LOAD")PRINTNL()
			ENDIF
		BREAK
		
		CASE MENU_STATE_TEST_MISSION
			//Empty
		BREAK
		
		CASE MENU_STATE_CUSTOM_TEMPLATE_NAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_TEMPLATE_NAME(sFMMCendStage, g_FMMC_STRUCT.iNumberOfPropTemplates-1)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_FMC_MENUS()
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_WARP_TO_INTERIOR
			HANDLE_FMC_INTERIOR_WARP()
		BREAK
		
		CASE MENU_STATE_ADD_CUSTOM_PROP
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, FALSE, FALSE, TRUE)
					REFRESH_MENU(sFMMCMenu)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					bDelayAFrame = TRUE
					
					#IF IS_DEBUG_BUILD
						SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
					#ENDIF
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ADD_CUSTOM_PED
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_ADDING_CUSTOM_MODEL(sFMMCendStage, sFMMCmenu, TRUE)
					REFRESH_MENU(sFMMCMenu)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					bDelayAFrame = TRUE
					
					#IF IS_DEBUG_BUILD
						SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
					#ENDIF
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ADD_CUSTOM_VEHICLE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_ADDING_CUSTOM_MODEL(sFMMCendStage, sFMMCmenu, TRUE, TRUE)
					REFRESH_MENU(sFMMCMenu)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					bDelayAFrame = TRUE
					
					#IF IS_DEBUG_BUILD
						SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
					#ENDIF
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_GET_CONTENT_TO_LOAD_LOCALLY
			IF bDelayAFrame = FALSE
			
				#IF IS_DEBUG_BUILD 
				BOOL bAllowDebugPopulate 
				bAllowDebugPopulate = TRUE 
				IF bAllowDebugPopulate
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RS) //url:bugstar:6887768
				ENDIF
				#ENDIF
			
				IF DEAL_WITH_SETTING_LOAD_MISSION_TEXT(sFMMCendStage, sLocalLoadSaveStruct.bLocalLoadCancelled, sLocalLoadSaveStruct.tlContentToLoad #IF IS_DEBUG_BUILD , bAllowDebugPopulate #ENDIF)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sLocalLoadSaveStruct.tlContentToLoad)
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_LOCAL_LOADING
					ENDIF
					REMOVE_ALL_PLACED_FMC_ITEMS(FALSE)
					DELETE_FMC_STRUCTS()
					INITIALISE_MODEL_ARRAYS(sPedStruct,	sWepStruct, sObjStruct, sFMMCmenu)
					SET_CREATOR_MENU_SELECTION(sFMMCmenu, ciFMC_TOP_MENU_PLACEMENT)	
					SET_ACTIVE_MENU(sFMMCMenu, eFmmc_TOP_MENU)
					
					#IF IS_DEBUG_BUILD
						CLEANUP_DEBUG_POPULATE(sFMMCendStage)
						SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
					#ENDIF
				ENDIF
			ELSE
				RESET_LOCAL_LOAD_SAVE_DATA(sLocalLoadSaveStruct)
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC MENU_INITIALISATION()
	sFMMCmenu.sSubTypeName[CREATION_TYPE_WEAPONS] 					= GET_CREATOR_NAME_FOR_PICKUP_TYPE(GET_PICKUP_TYPE_FROM_WEAPON_TYPE(sWepStruct.wtGunType[0]))
	sFMMCmenu.sSubTypeName[CREATION_TYPE_PROPS]             		= GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType))
	sFMMCmenu.sSubTypeName[CREATION_TYPE_DYNOPROPS]         		= GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType))
	
	//CHECK_FOR_END_CONDITIONS_MET()
	
	SET_ALL_MENU_ITEMS_ACTIVE()
	
	REFRESH_FMC_MENUS()
		
ENDPROC

PROC SET_STARTING_MENUS_ACTIVE()
//	SET_BIT(sFMMCmenu.iBitActive, START_MENU_ITEM_DM)
//	SET_BIT(sFMMCmenu.iBitActive, MAP_MENU_ITEM_DM)
	
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_CREATOR)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_RADIO)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_OPTIONS)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_EXIT)
ENDPROC

PROC HANDLE_MOUSE_ENTITY_SELECTION_CANCEL_AND_SWITCH(eFMMC_MENU_ENUM itemMenu, INT iNewItemIndex, INT iMaxItems)
	// cancel out of stuff
	CANCEL_ENTITY_CREATION(sFMMCmenu.iEntityCreation) 		
	IF sFMMCMenu.iSelectedEntity != -1	
		sFMMCMenu.iSelectedEntity = -1
	ENDIF

	// force the menu to be the one that we want
	GO_TO_MENU(sFMMCMenu, sFMMCdata, itemMenu)
	REFRESH_FMC_MENUS()

	// instead of looping through until the switch cam increases we force it
	sFMMCmenu.iSwitchCam = iNewItemIndex - 1
	DEAL_WITH_CAMERA_SWITCH_SELECTION(TRUE, iMaxItems)
	SET_CURSOR_POSITION(0.5, 0.5)
	PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())			
ENDPROC

PROC DO_END_MENU()
	
	IF NOT IS_SCREEN_FADING_OUT()
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				DO_SCREEN_FADE_OUT(500)	
				SET_CREATOR_AUDIO(FALSE, TRUE)
			ENDIF
		ELSE

			DRAW_END_MENU()
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				sCurrentVarsStruct.bSwitching = FALSE
				sCurrentVarsStruct.stiScreenCentreLOS = NULL
				DO_SCREEN_FADE_IN(500)
				SET_CREATOR_AUDIO(TRUE, TRUE)
			ENDIF
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_FINISHING)
			ENDIF
				
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_RESET_STATE(RESET_STATE eNewState)
	PRINTLN("SET_RESET_STATE - Changing iResetState from ", iResetState, " to ", eNewState)
	iResetState = eNewState
ENDPROC

// State machine that fades out and recreates all entities after a load
FUNC BOOL RESET_THE_MISSION_UP_SAFELY()

	INT i

	SWITCH iResetState
		CASE RESET_STATE_FADE
			PRINTLN("RESET_STATE_FADE")
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = TRUE 
				
				SET_OVERRIDE_WEATHER("CLEAR")
				
			    IF SCRIPT_IS_CLOUD_AVAILABLE()
					PRINTLN("SCRIPT_IS_CLOUD_AVAILABLE()")
				    SET_FAKE_MULTIPLAYER_MODE(FALSE)
				ELSE
					PRINTLN("NOT SCRIPT_IS_CLOUD_AVAILABLE()")
					//NETWORK_SESSION_LEAVE_SINGLE_PLAYER() 
					g_Private_IsMultiplayerCreatorRunning = FALSE
					g_Private_MultiplayerCreatorNeedsToEnd = TRUE
				ENDIF
			ENDIF
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE 
				SET_RESET_STATE(RESET_STATE_CLEAR)
				RESET_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())				
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)	
				ENDIF
				
				IF bOnGroundBeforeTest = FALSE
					CLEAR_BIT(sFMMCdata.iBitSet, bCameraActive)
					PRINTLN("CLEAR_BIT(sFMMCdata.iBitSet, bCameraActive) GO BACK TO CAMERA")
				ELSE
					SET_BIT(sFMMCdata.iBitSet, bCameraActive)	
					PRINTLN("SET_BIT(sFMMCdata.iBitSet, bCameraActive) GO BACK TO GROUND")
				ENDIF
				SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, FALSE, FALSE, FALSE)
			ENDIF
			
		BREAK
		
		CASE RESET_STATE_CLEAR
			PRINTLN("RESET_STATE_CLEAR")
			IF (NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			AND NOT NETWORK_IS_GAME_IN_PROGRESS())
			OR NOT SCRIPT_IS_CLOUD_AVAILABLE()
				FOR i = 0 TO FMMC_MAX_VEHICLES-1
					IF DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[i])
						REMOVE_BLIP(sVehStruct.biVehicleBlip[i])
					ENDIF
					IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[i])
						SET_ENTITY_AS_MISSION_ENTITY(sVehStruct.veVehcile[i], TRUE, TRUE)
						DELETE_VEHICLE(sVehStruct.veVehcile[i])
					ENDIF
				ENDFOR
				FOR i = 0 TO FMMC_MAX_NUM_FMC_PROPS-1
					IF DOES_BLIP_EXIST(sPropStruct.biObject[i])
						REMOVE_BLIP(sPropStruct.biObject[i])
					ENDIF
					if DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
						DELETE_OBJECT(sPropStruct.oiObject[i])
					ENDIF
				ENDFOR
				FOR i = 0 TO FMMC_MAX_PEDS-1
					IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])
						REMOVE_BLIP(sPedStruct.biPedBlip[i])
					ENDIF
					if DOES_ENTITY_EXIST(sPedStruct.piPed[i])
						DELETE_PED(sPedStruct.piPed[i])
					ENDIF
				ENDFOR
				FOR i = 0 TO FMMC_MAX_NUM_OBJECTS-1
					IF DOES_BLIP_EXIST(sObjStruct.biObject[i])
						REMOVE_BLIP(sObjStruct.biObject[i])
					ENDIF
					if DOES_ENTITY_EXIST(sObjStruct.oiObject[i])
						DELETE_OBJECT(sObjStruct.oiObject[i])
					ENDIF
				ENDFOR
				FOR i = 0 TO FMMC_MAX_WEAPONS-1
					IF DOES_PICKUP_EXIST(sWepStruct.Pickups[i])
						REMOVE_PICKUP(sWepStruct.Pickups[i])
					ENDIF
					IF DOES_BLIP_EXIST(sWepStruct.biWeaponBlip[i])
						REMOVE_BLIP(sWepStruct.biWeaponBlip[i])
					ENDIF
					if DOES_ENTITY_EXIST(sWepStruct.oiWeapon[i])
						DELETE_OBJECT(sWepStruct.oiWeapon[i])
					ENDIF
					IF sWepStruct.iDecalNum[i] != -1
						IF IS_DECAL_ALIVE(sWepStruct.diDecal[sWepStruct.iDecalNum[i]])
							REMOVE_DECAL(sWepStruct.diDecal[sWepStruct.iDecalNum[i]])
						ENDIF
					ENDIF
				ENDFOR
				IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
					REMOVE_BLIP(sStartEndBlips.biStart)
				ENDIF							
				IF sStartEndBlips.ciStartType != NULL
					DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
					sStartEndBlips.ciStartType = NULL
				ENDIF
				
				FOR i = 0 TO ciMAX_CUSTOM_DEV_PROPS-1
					g_FMMC_STRUCT.sCustomProps[i].mnCustomPropName = DUMMY_MODEL_FOR_SCRIPT
				ENDFOR
				g_FMMC_STRUCT.iCurrentNumberOfDevProps = 0
				PRINTLN("RESET_THE_MISSION_UP_SAFELY - Clearing all custom props")
				
				REMOVE_ALL_PLACED_FMC_ITEMS(FALSE)
				
				SET_RESET_STATE(RESET_STATE_PROPS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_PROPS
			IF CREATE_ALL_FMC_PROPS(iPropModelTimers)
				SET_RESET_STATE(RESET_STATE_VEHICLES)
			ENDIF
		BREAK
		
		CASE RESET_STATE_VEHICLES
			PRINTLN("RESET_STATE_VEHICLES")
			IF CREATE_ALL_FMC_VEHICLES(sFMMCmenu, iVehicleModelLoadTimers)
				SET_RESET_STATE(RESET_STATE_PEDS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_PEDS
			IF CREATE_ALL_FMC_PEDS(sFMMCmenu)
				SET_RESET_STATE(RESET_STATE_OBJECTS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_OBJECTS
			IF CREATE_ALL_FMC_MISSION_ENTITIES()
				SET_RESET_STATE(RESET_STATE_MOVING_DOORS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_MOVING_DOORS
			IF CREATE_ALL_FMC_MOVING_DOORS()
				SET_RESET_STATE(RESET_STATE_OTHER)
			ENDIF
		BREAK
		
		CASE RESET_STATE_OTHER
			PRINTLN("RESET_STATE_OTHER")
			
			REQUEST_STREAMED_TEXTURE_DICT("MPMissMarkers256")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPMissMarkers256")
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
					SET_ALL_MENU_ITEMS_ACTIVE()
					
					CREATE_TRIGGER_BLIP_AND_CORONA_WITH_DECAL(sStartEndBlips.biStart, sHCS.hcStartCoronaColour, sStartEndBlips.ciStartType)
					
				ELSE
					SET_STARTING_MENUS_ACTIVE()
				ENDIF
				
				SET_RESET_STATE(RESET_STATE_FINISH)
			ENDIF
			
		BREAK
		
		CASE RESET_STATE_FINISH
			PRINTLN("RESET_STATE_FINISH")
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC WARP_CAMERA_TO_START_LOCATION(FMMC_CAM_DATA &sCamDataPassed, FLOAT fHeight = 40.0)
	IF NOT IS_VECTOR_ZERO(sMissionPlacementData.MissionEntity.MissionEntities[0].vCoords)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_CAM_COORD(sCamDataPassed.cam, sMissionPlacementData.MissionEntity.MissionEntities[0].vCoords + <<0.0,0.0, fHeight>>)
		ENDIF
	ELIF NOT IS_VECTOR_ZERO(sMissionPlacementData.Ped.Peds[0].vCoords)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_CAM_COORD(sCamDataPassed.cam, sMissionPlacementData.Ped.Peds[0].vCoords + <<0.0,0.0, fHeight>>)
		ENDIF
	ELIF NOT IS_VECTOR_ZERO(sMissionPlacementData.Prop.Props[0].vCoords)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_CAM_COORD(sCamDataPassed.cam, sMissionPlacementData.Prop.Props[0].vCoords + <<0.0,0.0, fHeight>>)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PREPARE_CONTENT_TO_BE_SAVED_OR_PUBLISHED()
	sCurrentVarsStruct.creationStats.bSuccessfulSave = FALSE
	
	PRINTLN("PRE PUBLISH/SAVE DONE. Time to go for it")
	
	RETURN TRUE
ENDFUNC

// Displays the little buttons in the bottom right to show what you can do on screen
// This should cover most existing menu/creator interaction but hasn't really been maintained for the FMC as the public wont use it
PROC BUTTON_HELP()

	REMOVE_MENU_HELP_KEYS()
	
	INT iBitSet
	
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		
		//Left & Right
		IF IS_THIS_OPTION_SELECTABLE()
		AND NOT IS_THIS_OPTION_A_MENU_GOTO()
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_CYCLE_OPTIONS)
		ENDIF
		
		//A
		IF IS_BIT_SET(iHelpBitSet, biPickupEntityButton)
		AND sFMMCMenu.iSelectedEntity = -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
		ELSE
			IF IS_THIS_OPTION_SELECTABLE()
				IF IS_THIS_OPTION_A_MENU_GOTO()
				OR IS_THIS_OPTION_A_MENU_ACTION()
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
				ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
					IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
					AND sCurrentVarsStruct.bAreaIsGoodForPlacement
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
					ENDIF
				ELIF NOT IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)
				OR sFMMCMenu.iSelectedEntity != -1
					IF sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU
					AND sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
					AND sCurrentVarsStruct.bAreaIsGoodForPlacement
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
					ENDIF
				ENDIF
				
				SWITCH sFMMCmenu.sActiveMenu
				    CASE eFmmc_FMC_FOCUS_CAMS_UPDATE_MENU
						IF CUSTOM_FOCUS_CAM(sFMMCmenu)
							SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
						ENDIF
					BREAK
					CASE eFmmc_FMC_PATROL_UPDATE_MENU
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
					BREAK
					CASE eFmmc_FMC_GO_TO_POINT
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != ciFMC_GOTO_BLIP
							SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
		//B
		IF sFMMCmenu.sMenuBack != eFmmc_Null_item
		OR sFMMCMenu.iBoundsEditPointIndex > -1
			IF sFMMCMenu.iSelectedEntity = -1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			ELSE
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_CANCEL)
			ENDIF
		ENDIF
		//X
		IF IS_BIT_SET(iHelpBitSet, biDeleteEntityButton)
		AND CAN_DELETE_FROM_CURRENT_MENU(sFMMCMenu, sCurrentVarsStruct)
		AND sFMMCMenu.iSelectedEntity = -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF

		IF (sFMMCmenu.sActiveMenu = eFmmc_FMC_AMBUSH_MENU)
		AND GET_NUMBER_OF_AMBUSH_VEHICLES() > 0
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= ciFMC_AMB_AMBUSH
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF (sFMMCmenu.sActiveMenu = eFmmc_FMC_TARGET_LIST_MENU)
		AND GET_NUM_TARGET_STAGES() > 0
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF (sFMMCmenu.sActiveMenu = eFmmc_FMC_SEARCH_AREA_LIST_MENU)
		AND GET_NUMBER_OF_FMC_SEARCH_AREAS() > 0
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) < GET_NUMBER_OF_FMC_SEARCH_AREAS()
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF GET_HIGHLIGHTED_WORLD_PROP_INDEX(sCurrentVarsStruct.vCoronaHitEntity, sCurrentVarsStruct.vCoronaPos) != -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
				
		IF sCurrentVarsStruct.bBlockDelete
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF sFMMCmenu.iPropLibrary = PROP_LIBRARY_CUSTOM_PROPS 
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_FMC_FOCUS_CAMS_UPDATE_MENU
			IF NOT IS_VECTOR_ZERO(sMissionPlacementData.FocusCam.Cam[sFMMCMenu.iSelectedFMCEntity].vCoords)
			OR NOT CUSTOM_FOCUS_CAM(sFMMCmenu) AND sMissionPlacementData.FocusCam.Cam[sFMMCMenu.iSelectedFMCEntity].fRange > 0.0
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
			ENDIF
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_FMC_SECURITY_CAMERA_OPTIONS_MENU
		AND sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].iProp != -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		//Y
		
		//Bumpers
		IF IS_THIS_OPTION_ROTATABLE()
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE)
		ENDIF
		//Triggers
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)
		ENDIF
		
		//Misc
		IF sFMMCmenu.iCurrentMenuLength > 1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
		ENDIF
		IF (sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE) AND (sFMMCMenu.sActiveMenu != eFmmc_RADIO_MENU)
		AND sFmmcMenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFmmcMenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
			IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				IF IS_BIT_SET(iHelpBitSet, biWarpToCameraButton) 
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
				ENDIF
			ELSE
				IF sFMMCMenu.iSelectedEntity = -1
					IF IS_BIT_SET(iHelpBitSet, biWarpToCoronaButton) 
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
		OR sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_HEIGHT)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_FOV)
			IF DOES_CAM_EXIST(sCamData.cam)
			AND GET_CAM_FOV(sCamData.cam) != 40
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_RESET_FOV)		
			ENDIF
		ENDIF
		
		IF NOT FMMC_IS_CIRCLE_MENU_A_CAMERA_MENU(sFMMCmenu.sActiveMenu) 
			IF NOT IS_GAMEPLAY_CAM_RENDERING()
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE_CAM_HOLD)		
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_MOVE_CAMERA)
			ENDIF
		ENDIF
		
		SET_UP_PROP_HELP_BUTTONS(sFMMCmenu, iBitSet, sCurrentVarsStruct)
			
		IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_FMC_CHECKPOINTS_LIST_MENU
			IF sFMMCMenu.sCheckpoint.bAirCheckpoint
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_HEIGHT)
			ENDIF
		ENDIF
		
		CREATE_FMMC_INSTRUCTIONAL_BUTTONS(sFMMCmenu, iBitSet)
	ELSE
		IF sFMMCmenu.sMenuBack != eFmmc_Null_item
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1 
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
			ENDIF
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
			CREATE_FMMC_INSTRUCTIONAL_BUTTONS(sFMMCmenu, iBitSet)
		ENDIF
	ENDIF
ENDPROC

// Warning menus for big creator state changes - hasn't really been maintained for the FMC
PROC DO_FMC_CONFIRMATION_MENU()
	
	IF NOT IS_SCREEN_FADING_OUT()
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(500)	
			ButtonPressed = true
		ELSE
			IF NOT ButtonPressed
				IF DISPLAY_DELETE_ALL_WARNING_SCREEN(sCurrentVarsStruct.iEntityCreationStatus)
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_MENU_CURSOR_CANCEL_PRESSED()
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP		
						sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						sCurrentVarsStruct.bSwitching = FALSE
						sCurrentVarsStruct.stiScreenCentreLOS = NULL
						DO_SCREEN_FADE_IN(500)	
					ENDIF
					
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						INT i
						SWITCH sCurrentVarsStruct.iEntityCreationStatus
							CASE STAGE_DELETE_ALL
							
								IF sFMMCmenu.iScriptCreationType = SCRIPT_CREATION_TYPE_MISSION
									FOR i = 0 TO COUNT_OF(sFMMCmenu.iBitActive) - 1
										sFMMCmenu.iBitActive[0] = 0
									ENDFOR
									SET_MISSION_STARTING_MENU_ITEMS_ACTIVE(sFMMCmenu.iBitActive)
								ENDIF
			
								REMOVE_ALL_PLACED_FMC_ITEMS()
																								
								INIT_CREATOR_BUDGET()
								SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
								SET_ACTIVE_MENU(sFMMCMenu, eFmmc_TOP_MENU)
							BREAK
							CASE STAGE_DELETE_PEDS
								FMC_REMOVE_ALL_PEDS()
							BREAK
							CASE STAGE_DELETE_PROPS
								FMC_REMOVE_ALL_PROPS()
							BREAK
							CASE STAGE_DELETE_VEHICLES
								FMC_REMOVE_ALL_VEHICLES()
							BREAK
							CASE STAGE_DELETE_OBJECTS
								FMC_REMOVE_ALL_MISSION_ENTITIES()
							BREAK
							CASE STAGE_DELETE_TRIGGER_AREAS
								FMC_REMOVE_ALL_TRIGGER_AREAS()
							BREAK
							CASE STAGE_DELETE_POPULATION_BLOCKER
								FMC_REMOVE_ALL_POPULATION_BLOCKERS()
							BREAK
						ENDSWITCH
						
						sCurrentVarsStruct.creationStats.bMadeAChange = TRUE
						
						PRINTLN("DO_FMC_CONFIRMATION_MENU - Stage delete done")
						RESET_RACE_TEST(sFMMCmenu)
						
						RESET_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
						
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
						sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						PRINTLN("Setting iEntityCreation = -1  loc1")
						SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
						
						sCurrentVarsStruct.stiScreenCentreLOS = NULL
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_SCREEN_FADE_IN(500)	
						sCurrentVarsStruct.bSwitching = FALSE
						REFRESH_MENU(sFMMCmenu)
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF ButtonPressed
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			ButtonPressed = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC PRE_FRAME_CREATOR_IPL_PROCESSING()

	IF IS_ARRAYED_BIT_ENUM_SET(sMissionPlacementData.iBitset, eDATABITSET_ENABLE_HEIST_ISLAND)
		ENABLE_HEIST_ISLAND(TRUE)
	ELSE
		ENABLE_HEIST_ISLAND(FALSE)
	ENDIF

	IF IS_ARRAYED_BIT_ENUM_SET(sMissionPlacementData.iBitset, eDATABITSET_ENABLE_ISLAND_PARTY_PEDS)
		ENABLE_ISLAND_PARTY_PEDS(TRUE)
	ELSE
		ENABLE_ISLAND_PARTY_PEDS(FALSE)
	ENDIF

ENDPROC

PROC PRE_FRAME_CREATOR_INTERIOR_PROCESSING()


ENDPROC

PROC PRE_FRAME_CREATOR_PROCESSING()

	PRE_FRAME_CREATOR_IPL_PROCESSING()
	PRE_FRAME_CREATOR_INTERIOR_PROCESSING()

ENDPROC

SCRIPT

	IF IS_PAUSE_MENU_REQUESTING_TO_EDIT_A_MISSION()
		PRINTLN("[JA@PAUSEMENU] Set up which file to load into creator", GET_PAUSE_MENU_MISSION_FILE_TO_LOAD())

		sFMMCendStage.iLoadDeleteStage  = ciLOAD_DELETE_STAGE_LOAD
		sFMMCendStage.iMenuReturn		= GET_PAUSE_MENU_MISSION_FILE_TO_LOAD()
	ENDIF
	CLEAN_UP_PAUSE_MENU_MISSION_CREATOR_DATA()
	
	PROCESS_PRE_GAME()	
	
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP))
		SCRIPT_CLEANUP(TRUE)
	ENDIF	
	
	// Used for the model memory budget. Must be initiated at the start.
	INIT_CREATOR_BUDGET()
	
	INT iMarkerCount = 0
	
//	#IF IS_DEBUG_BUILD
//		CREATE_WIDGETS()
//	#ENDIF	

	#IF IS_DEBUG_BUILD
		SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
	#ENDIF
	
	// Main loop.
	WHILE TRUE	
	
		PROCESS_SINGLEPLAYER_MODEL_ON_CREATOR(bIsCreatorModelSetSPTC, bIsMaleSPTC)
		FMMC_FAKE_LEFT_AND_RIGHT_INPUTS_FOR_MENUS(sFMMCmenu)
		
		sCurrentVarsStruct.bDiscVisible = FALSE
		sCurrentVarsStruct.bCanCreateADecalThisFrame = TRUE
		
		PRE_FRAME_CREATOR_PROCESSING()
		
		WAIT(0)
		
		BUTTON_HELP()
		
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		DISPLAY_ALL_CURRENT_ROCKSTAR_CREATED(sFMMCendStage.sRocStarCreatedVars, sGetUGC_content)
		DEAL_WITH_AMBIENT_AND_HUD()
		
		// Refresh the menu this frame if it's needed
		IF sFMMCMenu.bRefreshMenuASAP
			REFRESH_FMC_MENUS()
			sFMMCMenu.bRefreshMenuASAP = FALSE
			PRINTLN("[TMS][ARENACREATOR][ARENA] Resetting menu due to bRefreshMenuASAP")
		ENDIF
		
		IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
			IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_FINISHING
				SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_FINISHING)
			ENDIF
		ENDIF
		
		IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()
			IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_FINISHING
				SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_FINISHING)
			ENDIF
		ENDIF
		
		// The main Creator state machine
		SWITCH sCurrentVarsStruct.iEntityCreationStatus
		
			// The base stage which usually checks to load content from the cloud but that's not necessary in the FMC
			CASE STAGE_CHECK_TO_LOAD_CREATION
				SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_LOADING)
			BREAK
			
			// Recreates the mission after data has been loaded in
			CASE STAGE_DEAL_WITH_LOADING
				IF SET_SKYSWOOP_UP(TRUE)
					IF RESET_THE_MISSION_UP_SAFELY()
						SET_RESET_STATE(RESET_STATE_FADE)
						IF sFMMCmenu.iRadioState = FMMC_RADIO_MAX_OPTIONS_STATE
							SET_MOBILE_PHONE_RADIO_STATE(TRUE)
						ENDIF			
						CLEAR_HELP()
						CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
						CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)	
						sCurrentVarsStruct.creationStats.bEditingACreation = true
						IF g_FMMC_STRUCT.bMissionIsPublished
							sCurrentVarsStruct.creationStats.bMadeAChange = FALSE
							SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
						ENDIF
						//Clean up
						REFRESH_FMC_MENUS()
						SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_LOAD)
						sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
						ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
						iHelpBitSetOld = -1

						WARP_CAMERA_TO_START_LOCATION(sCamData, 110.0)	
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_VOTING_DISABLED) 
							CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
						ELSE
							SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
						ENDIF
						
						DO_SCREEN_FADE_IN(500)
						
						IF IS_LOADING_ICON_ACTIVE()
							SET_LOADING_ICON_INACTIVE()
						ENDIF
					ELSE
						PRINTLN("RESET_THE_MISSION_UP_SAFELY = FALSE")
					ENDIF
				ENDIF
			BREAK
			
			// Load the menu assets
			CASE STAGE_ENTITY_PLACEMENT_LOAD
				IF HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
					IF LOAD_MENU_ASSETS()
						MENU_INITIALISATION()
						SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
					ENDIF
				ENDIF
			BREAK
			
			// This is the main stage where most creator activity takes place - Menus/Placement etc..
			CASE STAGE_ENTITY_PLACEMENT_SETUP
				
				// Camera control
				IF g_TurnOnCreatorHud
					CONTROL_CAMERA_AND_CORONA()	
				ENDIF
				
				// Stops you using special abilities if you're controlling the player
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				OR IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
				ENDIF
				
				// These maintain functions are the every frame calls different creation types need
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive) 
				AND NOT IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
					sFMMCmenu.iCurrentMarkersDrawn = 0
					UPDATE_SWAP_CAM_STATUS(sCurrentVarsStruct, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround))
					RESET_MAINTAIN_PLACED_ENTITY_VARIABLES(iLocalBitSet, sCurrentVarsStruct)
					MAINTAIN_PLACED_MARKERS()
					MAINTAIN_PLACED_DUMMY_BLIPS(sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sHCS, iLocalBitSet, sFMMCendStage)
					MAINTAIN_WORLD_PROPS(sFMMCmenu, sCurrentVarsStruct, iLocalBitSet)
					MAINTAIN_PLACED_FMC_VEHICLES(sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sHCS, sFMMCendStage)
					MAINTAIN_PLACED_FMC_PEDS(sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sHCS, sFMMCendStage)
					MAINTAIN_PLACED_FMC_OBJECTS(sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sHCS, sFMMCendStage)
					MAINTAIN_PLACED_FMC_PROPS(sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sHCS, sFMMCendStage)
					MAINTAIN_PLACED_FMC_LOCATIONS(sCurrentVarsStruct, sFMMCmenu, sHCS, sFMMCdata, sFMMCendStage)
					MAINTAIN_PLACED_FMC_PATROLS(sCurrentVarsStruct, sFMMCmenu, sHCS, sFMMCdata, sFMMCendStage, sPatrols)
					MAINTAIN_PLACED_FMC_CUSTOM_SPAWN_LOCATIONS(sCurrentVarsStruct, sFMMCmenu, sHCS, sFMMCdata, iLocalBitSet, sFMMCendStage)
					MAINTAIN_PLACED_FMC_LEAVE_AREAS(sCurrentVarsStruct, sFMMCmenu, sHCS, sFMMCdata, sFMMCendStage)
					MAINTAIN_PLACED_FMC_MOVING_DOORS(sCurrentVarsStruct, sFMMCmenu, sHCS, sFMMCdata, sFMMCendStage)
					MAINTAIN_PLACED_FMC_SECURITY_CAMERAS()
					MAINTAIN_PLACED_FMC_PORTALS(sCurrentVarsStruct, sFMMCmenu, sHCS, sFMMCdata, sFMMCendStage)
					MAINTAIN_PLACED_FMC_PORTAL_WARPS(sCurrentVarsStruct, sFMMCmenu, sHCS, sFMMCdata, sFMMCendStage)
					MAINTAIN_PLACED_FMC_SEARCH_AREAS(sCurrentVarsStruct, sFMMCmenu, sHCS, sFMMCdata, sFMMCendStage)
					MAINTAIN_PLACED_FMC_ZONES(ciFMC_ZONE_TYPE_TRIGGER_AREA, sCurrentVarsStruct, sFMMCmenu)
					MAINTAIN_PLACED_FMC_ZONES(ciFMC_ZONE_TYPE_POPULATION_ZONE, sCurrentVarsStruct, sFMMCmenu)
					MAINTAIN_CORONA_ENTITIES(sPedStruct, sFMMCmenu, sHCS, sCurrentVarsStruct, sFMMCdata, iMarkerCount, iLocalBitSet)
					MAINTAIN_PLACED_FMC_CHECKPOINTS_LIST(sCurrentVarsStruct, sFMMCmenu, sHCS, sFMMCdata, sFMMCendStage)
				ENDIF
				
				// Operates the L3/R3 height setting functionality
				DEAL_WITH_SETTING_HEIGHT_ON_ENTITY(sFMMCmenu, sCamData, sCurrentVarsStruct, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), CAN_HEIGHT_BE_SET_FOR_ENTITY(sFMMCmenu, sFMMCmenu.iEntityCreation, FALSE))
				REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
				
				// The menu state machine
				HANDLE_MENU_STATE()
				
				// Cycle between placed entities 
				MAINTAIN_CYCLE_ENTITIES(sFMMCmenu, sFMMCdata, sCamData)
				
				// Deals with the placement disc and it's size/marker type
				// You shouldn't need to edit anything in here as its variables can be set in other places
				MAINTAIN_PLACEMENT_DISC(sCurrentVarsStruct, sFMMCmenu, iLocalBitSet, sFMMCdata.iBitSet, FALSE, sFMMCmenu.iEntityCreation = -1)
				
				// Some blip stuff you can ignore
				BLIP_INDEX biFurthestBlip
				IF sFMMCmenu.bZoomedOutRadar
					biFurthestBlip = GET_FURTHEST_BLIP_TO_PLAYER(sPedStruct.biPedBlip, sVehStruct.biVehicleBlip, sWepStruct.biWeaponBlip, sPropStruct.biObject, sDynoPropStruct.biObject, sStartEndBlips.biStart, bCameraPanBlip, sCurrentVarsStruct.biLocateBlip)
				ENDIF
				IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
					CONTROL_PLAYER_BLIP(TRUE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, TRUE, biFurthestBlip)
				ELSE
					IF sFMMCMenu.sActiveMenu != eFmmc_PAN_CAM_BASE
					AND sFMMCMenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
					AND sFMMCMenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
					AND sFMMCMenu.sActiveMenu != eFmmc_TOP_MENU
					AND sFMMCMenu.sActiveMenu != eFmmc_RADIO_MENU
						CONTROL_PLAYER_BLIP(FALSE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, sFMMCmenu.bZoomedOutRadar, biFurthestBlip)
					ELSE
						CONTROL_PLAYER_BLIP(TRUE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, sFMMCmenu.bZoomedOutRadar, biFurthestBlip)
					ENDIF												
				ENDIF
			BREAK
			
			// The delete all stuff which hasn't been set up here
			CASE STAGE_DELETE_ALL
			CASE STAGE_DELETE_PEDS
			CASE STAGE_DELETE_VEHICLES
			CASE STAGE_DELETE_PROPS
			CASE STAGE_DELETE_OBJECTS
			CASE STAGE_DELETE_TRIGGER_AREAS
			CASE STAGE_DELETE_POPULATION_BLOCKER
				DO_FMC_CONFIRMATION_MENU()
			BREAK
			
			// End screens state - most big confirmation menus have their own state so that nothing else will run while the user decides
			CASE STAGE_ENTITY_PLACEMENT_END_MENU
				DO_END_MENU()
			BREAK
			
			// No network screen
			CASE STAGE_CLOUD_FAILURE
				FMMC_DRAW_CLOUD_FAIL_WARNING(bCreatorLimitedCloudDown, bSignedOut, FALSE)
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					IF !SCRIPT_IS_CLOUD_AVAILABLE()
						sCurrentVarsStruct.iMenuState = MENU_STATE_PAN_CAM
					ELSE
						IF NOT IS_SCREEN_FADED_IN()
							DO_SCREEN_FADE_IN(500)
						ENDIF
					ENDIF
					SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
					REFRESH_FMC_MENUS()
				ENDIF
			BREAK
			
			// Local saving state
			CASE STAGE_DEAL_WITH_LOCAL_SAVING
				sMissionPlacementData.tlContentName = g_FMMC_STRUCT.tl63MissionName
				IF bContentReadyForUGC = FALSE
					IF PREPARE_CONTENT_TO_BE_SAVED_OR_PUBLISHED()
						IF SAVE_LOCAL_FREEMODE_DATA(sMissionPlacementData.tlContentName, sMissionPlacementData, sLocalLoadSaveStruct, TRUE)
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			// Local loading state
			CASE STAGE_DEAL_WITH_LOCAL_LOADING			
				IF NOT sLocalLoadSaveStruct.bLocalLoadCancelled
					IF NOT IS_STRING_NULL_OR_EMPTY(sLocalLoadSaveStruct.tlContentToLoad)
						IF LOAD_LOCAL_FREEMODE_CONTENT_DATA(sLocalLoadSaveStruct.tlContentToLoad, sMissionPlacementData, sLocalLoadSaveStruct)
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
								sCamData.camPos = g_FMMC_STRUCT.vStartPos + <<0,-0.5, 20>>
							ENDIF
							
							g_FMMC_STRUCT.tl63MissionName = sMissionPlacementData.tlContentName
							
							SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_LOADING)
						ELSE
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
						ENDIF
					ENDIF
				ELSE
					SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
				ENDIF
			BREAK
			
			// Cleanup State
			CASE STAGE_DEAL_WITH_FINISHING
				IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()
					TRIGGER_TRANSITION_MENU_ACTIVE(TRUE) 					
					PRINTLN("[JA@PAUSEMENU] Clean up creator due to IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP")
					DO_SCREEN_FADE_IN(200)
					SCRIPT_CLEANUP(TRUE)
				ELIF IS_PAUSE_MENU_REQUESTING_TRANSITION()
					TRIGGER_TRANSITION_MENU_ACTIVE(TRUE) 
					PRINTLN("[JA@PAUSEMENU] Clean up creator due to pause menu requesting transition")
					DO_SCREEN_FADE_IN(200)
					SCRIPT_CLEANUP(TRUE)
				ELIF DEAL_WITH_FINISHING_CREATOR(sFMMCendStage)
					DO_SCREEN_FADE_IN(200)
					PRINTLN("[END CREATOR] DONE")
				 	SCRIPT_CLEANUP(FALSE)
				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
			PRINTLN("[LM] - Creator Bit - Clearing bMenuJustChangedPlacementBlocker")
			CLEAR_BIT(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
		ENDIF
		
		IF g_Private_Gamemode_Current = GAMEMODE_FM
		AND NETWORK_IS_GAME_IN_PROGRESS()
			PRINTLN("[LH] A Network Game is in Progress! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main")) != 0
			PRINTLN("[LH] The player has escaped to singleplayer! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		IF g_bKillFMMC_SignedOut
			SCRIPT_CLEANUP(TRUE)
			PRINTLN("g_bKillFMMC_SignedOut = TRUE")
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
		MAINTAIN_ENTITY_DETAIL_EXPORT()
		MAINTAIN_ENTITY_DETAIL_EXPORT_ALTERNATE(sVehStruct)
		MAINTAIN_ENTITY_DETAIL_EXPORT_PLACEHOLDER(sVehStruct)
		
		//Force clean up
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_END_MENU
		ENDIF
		
		#ENDIF
		
	ENDWHILE
ENDSCRIPT

#ENDIF
