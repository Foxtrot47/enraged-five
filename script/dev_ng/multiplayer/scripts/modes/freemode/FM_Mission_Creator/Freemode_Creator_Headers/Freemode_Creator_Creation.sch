USING "Freemode_Creator_Menus.sch"
USING "FMMC_Ped_Variation_List.sch"

VECTOR VECTOR_ZERO = <<0.0,0.0,0.0>>

FUNC BOOL FMC_DO_IS_MODEL_FMMC_VALID_ASSERT_CHECK(MODEL_NAMES model)
	IF model = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC FMC_UPDATE_THE_CORONA_DISC_VALUES(CREATION_VARS_STRUCT &sCurrentVarsStruct, ENTITY_INDEX eiPassed, BOOL bBelowEntity = TRUE, BOOL bOverrideEntityCheck = FALSE, FLOAT fOffset = fDEFAULT_PLACEMENT_DISC_HEIGHT, BOOL bVisible = TRUE)

	IF bVisible
		sCurrentVarsStruct.bDiscVisible = TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(eiPassed) 
		VECTOR vec1
		VECTOR vec2
		IF bBelowEntity = FALSE
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(eiPassed), vec1, vec2)
			sCurrentVarsStruct.vCoronaDiscPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiPassed, <<0,0, vec2.z>>) - sCurrentVarsStruct.vCoronaPos
		ELSE	
			sCurrentVarsStruct.vCoronaDiscPos = <<0,0,fOffset>>
		ENDIF		
	ELSE
		IF bOverrideEntityCheck
			sCurrentVarsStruct.vCoronaDiscPos = <<0,0,fOffset>>
		ENDIF
	ENDIF	
	
ENDPROC

FUNC BOOL FMC_CAN_PLACE_BUTTON_BE_PRESSED(structFMMC_MENU_ITEMS &sFMMCmenu)

	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= MAX_MENU_ITEMS
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADING_IN()
	OR NOT IS_SCREEN_FADED_IN()
		PRINTLN("FMC_CAN_PLACE_BUTTON_BE_PRESSED returning FALSE due to screen fading")
		RETURN FALSE
	ENDIF
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_SWITCH_CAMERA_BUTTON())
		PRINTLN("FMC_CAN_PLACE_BUTTON_BE_PRESSED returning FALSE due to pressing switch button")
		RETURN FALSE
	ENDIF
	
	IF sFMMCmenu.bButtonDelay
		sFMMCmenu.bButtonDelay = FALSE
		PRINTLN("FMC_CAN_PLACE_BUTTON_BE_PRESSED || returning FALSE due to sFMMCmenu.bButtonDelay")
		RETURN FALSE
	ENDIF
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = -1
	OR sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
	OR sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
		PRINTLN("FMC_CAN_PLACE_BUTTON_BE_PRESSED || returning FALSE due to sFMMCmenu.sActiveMenu")
		RETURN FALSE
	ENDIF
			
	RETURN TRUE
ENDFUNC

PROC FMC_SET_AREA_GOOD_FOR_PLACEMENT(CREATION_VARS_STRUCT &sCurrentVarsStruct, BOOL bIsGoodForPlacement)
	IF sCurrentVarsStruct.bAreaIsGoodForPlacement != bIsGoodForPlacement
		DEBUG_PRINTCALLSTACK()
		PRINTLN("FMC_SET_AREA_GOOD_FOR_PLACEMENT - Setting bAreaIsGoodForPlacement to ", PICK_STRING(bIsGoodForPlacement,"TRUE","FALSE"))
		sCurrentVarsStruct.bAreaIsGoodForPlacement = bIsGoodForPlacement
	ENDIF
ENDPROC

PROC FMC_CREATE_PLACEMENT_PARTICLE_FX(BOOL bOnWater, VECTOR vParticlePos)
	REQUEST_NAMED_PTFX_ASSET("scr_mp_creator")
	IF HAS_NAMED_PTFX_ASSET_LOADED("scr_mp_creator")
		USE_PARTICLE_FX_ASSET("scr_mp_creator")
		IF bOnWater
			PRINTLN("SPLASH PARTICLE GO NOW!")
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mp_splash", vParticlePos, <<0,0,0>>)
		ELSE
			PRINTLN("DUST PARTICLE GO NOW!")
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mp_dust_cloud", vParticlePos, <<0,0,0>>)
		ENDIF
	ENDIF
ENDPROC

// Generic Entity Functions

PROC FMC_MOVE_ENTITY(ENTITY_INDEX eiPassed, VECTOR &vCoord, FLOAT &fHead, CREATION_VARS_STRUCT &sCurrentVarsStruct, FMMC_LOCAL_STRUCT &sFMMCdata, structFMMC_MENU_ITEMS &sFMMCmenu, FLOAT fZmovement = 0.0, BOOL bCanBeUnderwater = FALSE, BOOL bPickup = FALSE, BOOL bFreeRotate = FALSE, BOOL bRotationOverriden = FALSE)

	FLOAT fTemp
	VECTOR vTemp = <<0, 0, -1000.0>>
	
	IF  eiPassed != NULL
	
		IF sCurrentVarsStruct.bCoronaOnWater = FALSE
		OR bCanBeUnderwater
		OR (IS_CORONA_UNDERWATER() AND IS_UNDERWATER_PLACEMENT_ALLOWED(sFMMCmenu))
			vCoord = sCurrentVarsStruct.vCoronaPos
			
		ELSE
			
			IF GET_WATER_HEIGHT_NO_WAVES(sCurrentVarsStruct.vCoronaPos + <<0.0, 0.0, 2.0>> ,fTemp)
				//Set it's coordinates
				vTemp 	=  <<sCurrentVarsStruct.vCoronaPos.x, sCurrentVarsStruct.vCoronaPos.y, fTemp>> + <<0.0, 0.0, 0.2>>		
				vCoord = vTemp
			ELSE
				vCoord = sCurrentVarsStruct.vCoronaPos
			ENDIF
			
		ENDIF
		
		vCoord += <<0.0, 0.0, fZmovement>>
		
		IF vCoord.x < 16000
		AND vCoord.y < 16000
		
			IF IS_ENTITY_AN_OBJECT(eiPassed)
			
				IF bPickup
				AND NOT bFreeRotate
					SET_ENTITY_COORDS(eiPassed, sCurrentVarsStruct.vCoronaPos + <<0.0, 0.0, fZmovement>>)
					IF IS_BIT_SET(sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_POSITION_OFFSET)	
						SET_ENTITY_COORDS(eiPassed, sCurrentVarsStruct.vCoronaPos + sFMMCmenu.vOffsetPosition + <<0.0, 0.0, fZmovement>>)
					ENDIF
				ELSE
					SET_ENTITY_COORDS(eiPassed, vCoord)
					IF IS_BIT_SET(sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_POSITION_OFFSET)
						PRINTLN("sFMMCmenu.vOffsetPosition = <<", sFMMCmenu.vOffsetPosition.X, ", ", sFMMCmenu.vOffsetPosition.Y, ", ", sFMMCmenu.vOffsetPosition.Z, ">>")
						SET_ENTITY_COORDS(eiPassed, sCurrentVarsStruct.vCoronaPos + sFMMCmenu.vOffsetPosition + <<0.0, 0.0, fZmovement>>)
					ENDIF
				ENDIF
				
			ELIF IS_ENTITY_A_PED(eiPassed)
			
				SET_ENTITY_COORDS(eiPassed, vCoord)
				
			ELSE
			
				IF bPickup
					SET_ENTITY_COORDS(eiPassed, sCurrentVarsStruct.vCoronaPos + <<0.0, 0.0, fZmovement>>)	
				ELSE
					SET_ENTITY_COORDS(eiPassed, vCoord)	
				ENDIF
				
			ENDIF
			
			IF IS_ENTITY_A_VEHICLE(eiPassed)
			
				SET_VEHICLE_ENGINE_HEALTH(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiPassed), 1000.0)
				SET_ENTITY_HEALTH(eiPassed, 1000)
				
			ENDIF
			
			IF (NOT bFreeRotate AND NOT IS_ENTITY_ATTACHED(eiPassed) AND NOT bRotationOverriden AND NOT (IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround) AND IS_SPECIAL_VEHICLE_THRUSTER_RACE()))
			OR sFMMCmenu.bForceRotate
			
				SET_ENTITY_HEADING(eiPassed, fHead)
				
				IF sFMMCMenu.bRuleMenuActive	 = FALSE
					FLOAT fNewRot = ADD_TO_CURRENT_HEADING(sCurrentVarsStruct.fRotationVeloc, DEFAULT, DEFAULT, sFMMCmenu.bForceRotate)
					IF fNewRot != 0.0
						fHead 		= GET_ENTITY_HEADING(eiPassed) - fNewRot
						sCurrentVarsStruct.fCreationHeading 	= fHead
						SET_ENTITY_HEADING(eiPassed, fHead)
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ELSE
	
		IF GET_WATER_HEIGHT_NO_WAVES(sCurrentVarsStruct.vCoronaPos ,fTemp)
		AND NOT (IS_CORONA_UNDERWATER() AND IS_UNDERWATER_PLACEMENT_ALLOWED(sFMMCmenu))
			vTemp 	=  <<sCurrentVarsStruct.vCoronaPos.x, sCurrentVarsStruct.vCoronaPos.y, fTemp>> + <<0.0, 0.0, fZmovement>>
		ENDIF
		
		IF sCurrentVarsStruct.vCoronaPos.z > vTemp.z
			vCoord = sCurrentVarsStruct.vCoronaPos
		ELSE
			vCoord = vTemp
		ENDIF
		
		fHead -= ADD_TO_CURRENT_HEADING(sCurrentVarsStruct.fRotationVeloc)
		IF fHead > 360.0
			fHead = 0.0
		ELIF fHead < 0.0 
			fHead = 360.0
		ENDIF
		
		sCurrentVarsStruct.fCreationHeading = fHead
		
		PRINTLN("fHeading is " , fHead)
		
	ENDIF
ENDPROC

PROC CREATE_CREATOR_BLIP(BLIP_INDEX &blip, VECTOR coords, HUD_COLOURS colour, STRING name, FLOAT size, BLIP_SPRITE bsCustomSprite = RADAR_TRACE_INVALID)
	IF DOES_BLIP_EXIST(blip)
		REMOVE_BLIP(blip)
	ENDIF
	blip = ADD_BLIP_FOR_COORD(coords)
	IF bsCustomSprite != RADAR_TRACE_INVALID
		SET_BLIP_SPRITE(blip, bsCustomSprite)
	ENDIF
	SET_BLIP_COLOUR_FROM_HUD_COLOUR(blip, colour)
	SET_BLIP_SCALE(blip, size)
	SET_BLIP_AS_MINIMAL_ON_EDGE(blip, TRUE)
	SET_BLIP_PRIORITY(blip,BLIPPRIORITY_MED)
	SHOW_HEIGHT_ON_BLIP(blip, FALSE)
	IF NOT IS_STRING_NULL_OR_EMPTY(name)
		SET_BLIP_NAME_FROM_TEXT_FILE(blip, name)
	ENDIF
ENDPROC

PROC SET_UP_BLIP_FOR_CREATOR(MISSION_PLACEMENT_DATA_BLIP sBlipData, BLIP_INDEX biBlip, ENTITY_INDEX eiEntity, STRING sBlipName)
	
	CREATE_CREATOR_BLIP(biBlip, GET_ENTITY_COORDS(eiEntity), sBlipData.eHudColour, sBlipName, sBlipData.fScale, sBlipData.eSprite)

ENDPROC

PROC FMC_SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iReason)
	
	sCurrentVarsStruct.iFailPlacementReason[0] = 0
	sCurrentVarsStruct.iFailPlacementReason[1] = 0
	SET_BIT(sCurrentVarsStruct.iFailPlacementReason[GET_LONG_BITSET_INDEX(iReason)], GET_LONG_BITSET_BIT(iReason))
	PRINTLN("FMC_SET_THE_PLACEMENT_FAIL_AS_THIS_REASON - ", sCurrentVarsStruct.iFailPlacementReason[0], " & ", sCurrentVarsStruct.iFailPlacementReason[1], " iReason(",iReason,"): ", sCurrentVarsStruct.iFailPlacementReason[GET_LONG_BITSET_INDEX(iReason)])
	
	IF sCurrentVarsStruct.iPrevFailPlacementReason != sCurrentVarsStruct.iFailPlacementReason[GET_LONG_BITSET_INDEX(iReason)] 
		PRINTLN("FMC_SET_THE_PLACEMENT_FAIL_AS_THIS_REASON has been called and it's different to the last time it was called. The reason was: ", iReason, "! Callstack coming up")
		DEBUG_PRINTCALLSTACK()
		sCurrentVarsStruct.iPrevFailPlacementReason = sCurrentVarsStruct.iFailPlacementReason[GET_LONG_BITSET_INDEX(iReason)]
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    sCoronaShapeTest - 
///    fCoronaSize - 
///    EntityToPlace - 
///    eiIgnoreEntity - 
///    bIsPickup - 
/// RETURNS:
///    
	
FUNC BOOL FMC_IS_AREA_AROUND_CORONA_CLEAR(CORONA_SHAPE_TEST_DATA &sCoronaShapeTest, FLOAT fCoronaSize, BOOL &bReturnedAValue, ENTITY_INDEX EntityToPlace = NULL, ENTITY_INDEX eiIgnoreEntity = NULL, BOOL bIsPickup = FALSE, BOOL bIsWaterVehicle = FALSE, BOOL bIsTrigger = FALSE, BOOL bIsInAir = FALSE, BOOL bIsAnimated = FALSE, BOOL bAmphibiousVehicle = FALSE, BOOL bForceCoronaSizeTest = FALSE, FLOAT fDimensionsScalar = 1.0)

	VECTOR vTemp1Test // Move these to global and uncomment debug draw lines below to show where hit test is failing
	VECTOR vTemp2Test
	
	DEBUG_PRINTCALLSTACK()
	ENTITY_INDEX hitEntity
	IF sCoronaShapeTest.stiShapeTest  = NULL
		PRINTLN("[TMSSP][IAC][ST] CoronaShapeTest.stiShapeTest  = NULL")
		//Move the position up so it's off the ground
		VECTOR vDimensions
		VECTOR vRot
		IF EntityToPlace = NULL
		OR bForceCoronaSizeTest
			//Get the rotation to aligh it to the camera
			vRot = GET_FINAL_RENDERED_CAM_ROT()
			sCoronaShapeTest.vPos += <<0.0, 0.0, 1.4>>
			vRot.X = 0.0
			vRot.Y = 0.0
			vDimensions = <<fCoronaSize, fCoronaSize, 2.0>>
			PRINTLN("[TMSSP][IAC][ST] - using corona size")
		ELSE
			//FLOAT //z  //, x, y
			VECTOR vMin
			VECTOR vMax
			vRot = GET_ENTITY_ROTATION(EntityToPlace) 
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(EntityToPlace), vMin, vMax)
			IF GET_ENTITY_MODEL(EntityToPlace) = STUNT
				vRot.x -= 9.8
			ELIF GET_ENTITY_MODEL(EntityToPlace) = DUSTER
				vRot.x -= 11.0
			ELIF GET_ENTITY_MODEL(EntityToPlace) = MAVERICK
				vRot.x -= 3.0
			ELIF GET_ENTITY_MODEL(EntityToPlace) = MAMMATUS
				vRot.x -= 0.9
			ELIF GET_ENTITY_MODEL(EntityToPlace) = POLMAV
				vRot.x -= 2.2
			ENDIF
			
			IF DOES_VEHICLE_MODEL_NEED_PLACEMENT_ASSISTANCE(EntityToPlace)
				vRot.x = 0
			ENDIF
			
			IF IS_THIS_A_SURVIVAL()
			AND IS_ENTITY_A_VEHICLE(EntityToPlace)
				vDimensions = <<ciLARGE_VEHICLE_CHECK_POINT_SIZE/2, ciLARGE_VEHICLE_CHECK_POINT_SIZE, 2.0>>
				IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(EntityToPlace))
					vRot.y = 1.1
					IF vRot.z > 0
						vRot.y *= -1
					ENDIF
					vRot.x -= 0.4
				ENDIF
			ELSE
				vDimensions = vMax - vMin
			ENDIF
			
			IF IS_ENTITY_A_PED(EntityToPlace)
				sCoronaShapeTest.vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(EntityToPlace, <<0,0,0.2>>)
			ELSE
				sCoronaShapeTest.vPos = GET_ENTITY_COORDS(EntityToPlace)
				
				PRINTLN("[FMC_IS_AREA_AROUND_CORONA_CLEAR] 1 sCoronaShapeTest.vPos = <<", sCoronaShapeTest.vPos.X, ", ", sCoronaShapeTest.vPos.Y, ", ", sCoronaShapeTest.vPos.Z, ">>")
				
				IF (NOT bIsWaterVehicle OR bAmphibiousVehicle)
				AND NOT bIsInAir
					IF GET_GROUND_Z_FOR_3D_COORD(sCoronaShapeTest.vPos + <<0,0,1>>, sCoronaShapeTest.vPos.Z)
						sCoronaShapeTest.vPos.z += vDimensions.z/2
						sCoronaShapeTest.vPos.z += 0.1
						vDimensions.z -= 0.2
					ENDIF
					IF GET_ENTITY_MODEL(EntityToPlace) = HOWARD
					OR GET_ENTITY_MODEL(EntityToPlace) = BOMBUSHKA
						vDimensions.z -= 1.0
					ENDIF
					
					IF GET_ENTITY_MODEL(EntityToPlace) = VOLATOL
						vDimensions.z -= 1.5
					ENDIF
					PRINTLN("[FMC_IS_AREA_AROUND_CORONA_CLEAR] 2 sCoronaShapeTest.vPos = <<", sCoronaShapeTest.vPos.X, ", ", sCoronaShapeTest.vPos.Y, ", ", sCoronaShapeTest.vPos.Z, ">>")
				ELSE
					IF IS_ENTITY_A_VEHICLE(EntityToPlace)
						FLOAT zOffset = -0.2
						sCoronaShapeTest.vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(EntityToPlace, <<0,0,(vDimensions.z/2) + zOffset>>)	
						IF GET_ENTITY_MODEL(EntityToPlace) = MARQUIS
						OR GET_ENTITY_MODEL(EntityToPlace) = SUBMERSIBLE
						OR GET_ENTITY_MODEL(EntityToPlace) = SUBMERSIBLE2
							sCoronaShapeTest.vPos.z -= 3
						ENDIF
					ELSE
						sCoronaShapeTest.vPos += vDimensions.z/2
						IF GET_ENTITY_MODEL(EntityToPlace) = PROP_JETSKI_RAMP_01
						OR GET_ENTITY_MODEL(EntityToPlace) = INT_TO_ENUM(MODEL_NAMES, HASH("ar_prop_ar_jetski_ramp_01_dev"))
							sCoronaShapeTest.vPos.z -= 2
						ELIF GET_ENTITY_MODEL(EntityToPlace) = Prop_Dock_Bouy_1
						OR GET_ENTITY_MODEL(EntityToPlace) = PROP_DOCK_BOUY_2
							sCoronaShapeTest.vPos.z -= 4
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF	
		
		IF fDimensionsScalar != 1.0
			FLOAT fTempDimensionsZ = vDimensions.z
			vDimensions *= fDimensionsScalar
			sCoronaShapeTest.vPos.z += vDimensions.z - fTempDimensionsZ
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
			DRAW_DEBUG_BOX(sCoronaShapeTest.vPos - vDimensions * 0.5, sCoronaShapeTest.vPos + vDimensions * 0.5, 255, 255, 255, 100)
		ENDIF
		#ENDIF
		
		PRINTLN("[FMC_IS_AREA_AROUND_CORONA_CLEAR] 3 sCoronaShapeTest.vPos = ", sCoronaShapeTest.vPos, " / vDimensions: ", vDimensions, " / fDimensionsScalar: ", fDimensionsScalar)	
		PRINTLN("[TMSSP][IAC][ST] GET_SHAPE_TEST_BOX_IGNORE_TYPE")
		
		sCoronaShapeTest.stiShapeTest = GET_SHAPE_TEST_BOX_IGNORE_TYPE(sCoronaShapeTest.vPos, vDimensions, vRot, eiIgnoreEntity, bIsPickup, bIsTrigger, bIsAnimated)
	ELSE
	
		IF GET_SHAPE_TEST_RESULT(sCoronaShapeTest.stiShapeTest, sCoronaShapeTest.iResult, vTemp1Test, vTemp2Test, hitEntity) = SHAPETEST_STATUS_RESULTS_READY
			PRINTLN("[TMSSP][IAC][ST] Result")
			IF DOES_ENTITY_EXIST(hitEntity)
			AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hitEntity, FALSE)
				sCoronaShapeTest.bGridVehicleHit = (IS_ENTITY_A_VEHICLE(hitEntity)
													AND IS_THIS_A_RACE())
				sCoronaShapeTest.hitEntity = hitEntity
			ELSE
				sCoronaShapeTest.hitEntity = NULL
				sCoronaShapeTest.bGridVehicleHit = FALSE
			ENDIF
			RELEASE_SCRIPT_GUID_FROM_ENTITY(hitEntity)
			sCoronaShapeTest.stiShapeTest  = NULL
			sCoronaShapeTest.bDoStartShapeTest  = FALSE
			
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
				DRAW_DEBUG_LINE(sCoronaShapeTest.vPos, vTemp1Test)
				
				IF IS_ENTITY_AN_OBJECT(hitEntity)
					PRINTLN("[TMSSP][IAC][ST] Hit object is ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(hitEntity)))
				ELSE
					PRINTLN("[TMSSP][IAC][ST] Hit entity isn't an object")
				ENDIF
			ENDIF
			#ENDIF
		
			IF  sCoronaShapeTest.iResult = 0
				bReturnedAValue = TRUE
				vTemp1Test = <<0.0,0.0,0.0>>
				vTemp2Test = <<0.0,0.0,0.0>>
				PRINTLN("[TMSSP][IAC][ST] RETURNING TRUE 5970")
				RETURN TRUE
			ELSE
				sCoronaShapeTest.iResult = 0
				bReturnedAValue = TRUE
				
				PRINTLN("[TMSSP][IAC][ST] 5977")
				RETURN FALSE
			ENDIF
		ELSE
			PRINTLN("[TMSSP][IAC][ST] no shapetest result")
			
			RELEASE_SCRIPT_GUID_FROM_ENTITY(hitEntity)
			IF GET_SHAPE_TEST_RESULT(sCoronaShapeTest.stiShapeTest, sCoronaShapeTest.iResult, vTemp1Test, vTemp2Test, hitEntity) = SHAPETEST_STATUS_NONEXISTENT
				sCoronaShapeTest.stiShapeTest = NULL
			ENDIF
			RELEASE_SCRIPT_GUID_FROM_ENTITY(hitEntity)
		ENDIF
		
	ENDIF
	
	PRINTLN("[TMSSP][IAC][ST] 5991")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to make sure the entity can be placed. Curently trigger locations use a different system.
/// PARAMS:
///    sCurrentVarsStruct - 
///    fCoronaSize - 
///    bOnGround - 
///    EntityToPlace - 
///    bOnWater - If it can be placed on water
///    eiIgnoreEntity - Ignore this entity when doing the shape test
///    bIsPickup - If this is an object pickup work differently. Uses less shapetest checks but it does these checks afterwards in MOVE_LAST_PLACED_OBJECT
/// RETURNS:
///    
FUNC BOOL FMC_IS_AREA_CLEAR_FOR_PLACEMENT(CREATION_VARS_STRUCT &sCurrentVarsStruct, FLOAT fCoronaSize, ENTITY_INDEX EntityToPlace = NULL, BOOL bShouldBeOnWater = FALSE, ENTITY_INDEX eiIgnoreEntity = NULL, BOOL bIsPickup = FALSE, BOOL bIsTrigger = FALSE, BOOL bIsInAir = FALSE, BOOL bAttachedtoTruck = FALSE, BOOL bAmphibiousVehicle = FALSE, BOOL bForceCoronaSizeCheck = FALSE, FLOAT fSizeScalar = 1.0)
	
	IF IS_ROCKSTAR_DEV()
	AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_DELETE_BUTTON())
		RETURN TRUE
	ENDIF
	
	SET_FORCE_OBJECT_THIS_FRAME(sCurrentVarsStruct.vCoronaPos, 10)
	IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
		IF sCurrentVarsStruct.sCoronaShapeTest.bDoStartShapeTest = FALSE
			IF bShouldBeOnWater 
			AND NOT sCurrentVarsStruct.bCoronaOnWater
			AND NOT bAmphibiousVehicle
				FMC_SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bLandFail)
				FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
				sCurrentVarsStruct.bFirstShapeTestCheckDone = TRUE
			ELIF NOT bShouldBeOnWater
			OR sCurrentVarsStruct.bCoronaOnWater
			OR bAttachedtoTruck
			OR bAmphibiousVehicle
				sCurrentVarsStruct.sCoronaShapeTest.bDoStartShapeTest = TRUE
				sCurrentVarsStruct.sCoronaShapeTest.vPos = sCurrentVarsStruct.vCoronaPos
				sCurrentVarsStruct.sCoronaShapeTest.vRefNewShapeTestPos = sCurrentVarsStruct.vCoronaPos
				FMC_IS_AREA_AROUND_CORONA_CLEAR(sCurrentVarsStruct.sCoronaShapeTest, fCoronaSize, sCurrentVarsStruct.bFirstShapeTestCheckDone, EntityToPlace, eiIgnoreEntity, bIsPickup, bShouldBeOnWater, bIsTrigger, bIsInAir, DEFAULT, bAmphibiousVehicle,bForceCoronaSizeCheck, fSizeScalar)
			ELSE
				FMC_SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bWaterFail)
				FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
				sCurrentVarsStruct.bFirstShapeTestCheckDone = TRUE
				PRINTLN("[TMSSP][IAC] 6209")
			ENDIF
		ELSE			
			IF FMC_IS_AREA_AROUND_CORONA_CLEAR(sCurrentVarsStruct.sCoronaShapeTest, fCoronaSize, sCurrentVarsStruct.bFirstShapeTestCheckDone, EntityToPlace, eiIgnoreEntity, bIsPickup, bShouldBeOnWater, bIsTrigger, bIsInAir, DEFAULT, bAmphibiousVehicle, bForceCoronaSizeCheck, fSizeScalar)
			
				FLOAT fRange
				FLOAT fMaxSlope
				IF EntityToPlace = NULL
					fRange = fCoronaSize/2
					fMaxSlope = 25.0
				ELSE
					fMaxSlope = 20.0
				ENDIF
				
				IF sCurrentVarsStruct.bSelectedAnEntity = TRUE
					FMC_SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bSelectedFail)
					FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
					PRINTLN("[TMSSP][IAC] 6225")
				ELSE
					IF (NOT FMMC_IS_THIS_A_SLOPE(sCurrentVarsStruct.vCoronaPos, EntityToPlace, fMaxSlope, fRange) OR bShouldBeOnWater OR bIsInAir)
					
						sCurrentVarsStruct.sCoronaShapeTest.vRefLastShapeTestPos = sCurrentVarsStruct.sCoronaShapeTest.vRefNewShapeTestPos	
						PRINTLN("[TMSSP][IAC] RETURNING TRUE 6232")
						
						RETURN TRUE
					ELSE
						FMC_SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bSlopeFail)
						FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
						PRINTLN("[TMSSP][IAC] 6235")
					ENDIF
				ENDIF
			ELSE
				FMC_SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bShapeTestFail)
				FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
				PRINTLN("[TMSSP][IAC] 6241")
			ENDIF
		ENDIF
	ELSE
		IF sCurrentVarsStruct.bitsetOkToPlace = BS_CANT_PLACE__DIST_FAIL
			FMC_SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bDistFail)
		ELSE
			FMC_SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bMapFail)
		ENDIF
		
		PRINTLN("[TMSSP][IAC] 6250")
		FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
	ENDIF
	
	PRINTLN("[TMSSP][IAC] Not clear! b")
	RETURN FALSE
	
ENDFUNC

FUNC BOOL FMC_IS_VECTOR_IN_INTERIOR_INSTANCE(VECTOR vVectorToCheck, INTERIOR_INSTANCE_INDEX iiInteriorInstance)
	IF NOT IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vVectorToCheck))
		RETURN FALSE
	ELSE
		IF GET_INTERIOR_AT_COORDS(vVectorToCheck) != iiInteriorInstance
			RETURN FALSE
		ENDIF
	ENDIF	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL FMC_IS_THIS_FAIL_REASON_SET(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iFailReason)
	RETURN IS_BIT_SET(sCurrentVarsStruct.iFailPlacementReason[GET_LONG_BITSET_INDEX(iFailReason)], GET_LONG_BITSET_BIT(iFailReason))
ENDFUNC


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Common creation checks
// ##### Description: Functions used in multiple creation types
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_FMC_ENTITY_IN_THE_CORONA_UPDATE(structFMMC_MENU_ITEMS &sFMMCmenu, INT iType)
	
	RETURN iType = sFMMCmenu.iEntityCreation

ENDFUNC

FUNC BOOL IS_FMC_GO_TO_POINT_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT i, FLOAT fRadius = 5.75)
	IF  (sCurrentVarsStruct.vCoronaPos.x + fRadius) > (sMissionPlacementData.GoToPoint.Locations[i].vCoords.x)
	AND (sCurrentVarsStruct.vCoronaPos.y + fRadius) > (sMissionPlacementData.GoToPoint.Locations[i].vCoords.y)
	AND (sCurrentVarsStruct.vCoronaPos.x - fRadius) < (sMissionPlacementData.GoToPoint.Locations[i].vCoords.x)
	AND (sCurrentVarsStruct.vCoronaPos.y - fRadius) < (sMissionPlacementData.GoToPoint.Locations[i].vCoords.y)
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.GoToPoint.Locations[i].vCoords)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FMC_PORTAL_POINT_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT i, FLOAT fRadius = 5.75)
	IF  (sCurrentVarsStruct.vCoronaPos.x + fRadius) > (sMissionPlacementData.Portal[i].vCoord.x)
	AND (sCurrentVarsStruct.vCoronaPos.y + fRadius) > (sMissionPlacementData.Portal[i].vCoord.y)
	AND (sCurrentVarsStruct.vCoronaPos.x - fRadius) < (sMissionPlacementData.Portal[i].vCoord.x)
	AND (sCurrentVarsStruct.vCoronaPos.y - fRadius) < (sMissionPlacementData.Portal[i].vCoord.y)
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.Portal[i].vCoord)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FMC_PORTAL_WARP_POINT_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT i, FLOAT fRadius = 5.75, INT iPortal = -1)
	IF iPortal != -1
	AND (sCurrentVarsStruct.vCoronaPos.x + fRadius) > (sMissionPlacementData.Portal[iPortal].vWarpCoord[i].x)
	AND (sCurrentVarsStruct.vCoronaPos.y + fRadius) > (sMissionPlacementData.Portal[iPortal].vWarpCoord[i].y)
	AND (sCurrentVarsStruct.vCoronaPos.x - fRadius) < (sMissionPlacementData.Portal[iPortal].vWarpCoord[i].x)
	AND (sCurrentVarsStruct.vCoronaPos.y - fRadius) < (sMissionPlacementData.Portal[iPortal].vWarpCoord[i].y)
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.Portal[iPortal].vWarpCoord[i])
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FMC_PATROL_NODE_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iPatrol, INT iPatrolNode, FLOAT fRadius = 5.75)
	IF  (sCurrentVarsStruct.vCoronaPos.x + fRadius) > (sMissionPlacementData.Patrol.Patrols[iPatrol].Node[iPatrolNode].vCoord.x)
	AND (sCurrentVarsStruct.vCoronaPos.y + fRadius) > (sMissionPlacementData.Patrol.Patrols[iPatrol].Node[iPatrolNode].vCoord.y)
	AND (sCurrentVarsStruct.vCoronaPos.x - fRadius) < (sMissionPlacementData.Patrol.Patrols[iPatrol].Node[iPatrolNode].vCoord.x)
	AND (sCurrentVarsStruct.vCoronaPos.y - fRadius) < (sMissionPlacementData.Patrol.Patrols[iPatrol].Node[iPatrolNode].vCoord.y)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CUSTOM_SPAWN_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT i, FLOAT fRadius = 5.75)
	IF  (sCurrentVarsStruct.vCoronaPos.x + fRadius) > (sMissionPlacementData.CustomSpawns.Spawns[i].vCoords.x)
	AND (sCurrentVarsStruct.vCoronaPos.y + fRadius) > (sMissionPlacementData.CustomSpawns.Spawns[i].vCoords.y)
	AND (sCurrentVarsStruct.vCoronaPos.x - fRadius) < (sMissionPlacementData.CustomSpawns.Spawns[i].vCoords.x)
	AND (sCurrentVarsStruct.vCoronaPos.y - fRadius) < (sMissionPlacementData.CustomSpawns.Spawns[i].vCoords.y)
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.CustomSpawns.Spawns[i].vCoords)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_OTHER_CUSTOM_SPAWN_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, FLOAT fRadius = 1.0, INT iNumber = -1)
	INT i 
	FOR i = 0 TO (MAX_NUM_CUSTOM_SPAWNS -1)
		IF iNumber > -1
		AND i = iNumber
			PRINTLN("[WJK] - IS_ANY_OTHER_CUSTOM_SPAWN_IN_CORONA - FALSE")
		ELSE
			IF IS_CUSTOM_SPAWN_IN_CORONA(sCurrentVarsStruct, i, fRadius)
				PRINTLN("[WJK] - IS_ANY_OTHER_CUSTOM_SPAWN_IN_CORONA - TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FMC_LEAVE_AREA_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iLeaveArea, FLOAT fRadius = 5.75)
	IF  (sCurrentVarsStruct.vCoronaPos.x + fRadius) > sMissionPlacementData.LeaveArea[iLeaveArea].vCoords.x
	AND (sCurrentVarsStruct.vCoronaPos.y + fRadius) > sMissionPlacementData.LeaveArea[iLeaveArea].vCoords.y
	AND (sCurrentVarsStruct.vCoronaPos.x - fRadius) < sMissionPlacementData.LeaveArea[iLeaveArea].vCoords.x
	AND (sCurrentVarsStruct.vCoronaPos.y - fRadius) < sMissionPlacementData.LeaveArea[iLeaveArea].vCoords.y
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.LeaveArea[iLeaveArea].vCoords)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FMC_SEARCH_AREA_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT i, FLOAT fRadius = 5.75)
	IF  (sCurrentVarsStruct.vCoronaPos.x + fRadius) > sMissionPlacementData.SearchArea.Areas[i].vCoords.x
	AND (sCurrentVarsStruct.vCoronaPos.y + fRadius) > sMissionPlacementData.SearchArea.Areas[i].vCoords.y
	AND (sCurrentVarsStruct.vCoronaPos.x - fRadius) < sMissionPlacementData.SearchArea.Areas[i].vCoords.x
	AND (sCurrentVarsStruct.vCoronaPos.y - fRadius) < sMissionPlacementData.SearchArea.Areas[i].vCoords.y
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.SearchArea.Areas[i].vCoords)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FMC_CHECKPOINTS_LIST_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT i, FLOAT fRadius = 5.75)
	IF  (sCurrentVarsStruct.vCoronaPos.x + fRadius) > sMissionPlacementData.Checkpoint.Checkpoints[i].vCoords.x
	AND (sCurrentVarsStruct.vCoronaPos.y + fRadius) > sMissionPlacementData.Checkpoint.Checkpoints[i].vCoords.y
	AND (sCurrentVarsStruct.vCoronaPos.x - fRadius) < sMissionPlacementData.Checkpoint.Checkpoints[i].vCoords.x
	AND (sCurrentVarsStruct.vCoronaPos.y - fRadius) < sMissionPlacementData.Checkpoint.Checkpoints[i].vCoords.y
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.Checkpoint.Checkpoints[i].vCoords)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_OTHER_FMC_CHECKPOINT_LIST_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iCheckpoint = -1, FLOAT fRadius = 1.0)
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		PRINTLN("[CHECKPOINT] - IS_ANY_OTHER_FMC_CHECKPOINT_LIST_IN_CORONA - player pos = ", vPlayerPos)
	ENDIF
	
	INT iNumCheckpoints = GET_NUMBER_OF_FMC_CHECKPOINTS_LISTS()
	
	INT i
	REPEAT iNumCheckpoints i
		PRINTLN("[CHECKPOINT] - IS_ANY_OTHER_FMC_CHECKPOINT_LIST_IN_CORONA - iCheckpoint = ", i)
		
		IF iCheckpoint > -1
		AND i = iCheckpoint
			PRINTLN("[CHECKPOINT] - IS_ANY_OTHER_FMC_CHECKPOINT_LIST_IN_CORONA - iCheckpoint > -1 AND i = iCheckpoint")
		ELSE
			IF IS_FMC_CHECKPOINTS_LIST_IN_CORONA(sCurrentVarsStruct, i, fRadius)
				PRINTLN("[CHECKPOINT] - IS_ANY_OTHER_FMC_CHECKPOINT_LIST_IN_CORONA - TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MOVE_LAST_PLACED_CHECKPOINT(structFMMC_MENU_ITEMS &sFMMCmenu, OBJECT_INDEX eiPassed, VECTOR &vCoord, CREATION_VARS_STRUCT &sCurrentVarsStruct, FMMC_LOCAL_STRUCT &sFMMCdata, INT iCheckpoint, BOOL bOverrideRot = FALSE)
	FMC_MOVE_ENTITY(eiPassed, vCoord, sCurrentVarsStruct.fCreationHeading, sCurrentVarsStruct, sFMMCdata, sFMMCmenu,DEFAULT,DEFAULT,DEFAULT,DEFAULT,bOverrideRot)
	IF bOverrideRot
		IF IS_VECTOR_ZERO(sFMMCmenu.vOverrideRotation)
			SET_OVERRIDE_ROTATION_FROM_ENTITY(sFMMCMenu, eiPassed)
			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
		ENDIF
		
		SET_ENTITY_ROTATION(eiPassed, sFMMCmenu.vOverrideRotation)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION									// This conditions will stop placing the checkpoint while in the rotation menu
	AND sFMMCmenu.sActiveMenu != eFMMC_FMC_OVERRIDE_ROTATION									// and when on the override rotation option on the checkpoint menu (so it enters
	AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) != ciFMC_CHECKPOINTS_LIST_ROTATION				// in the rotation menu)
	AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) != ROT_FMC_OVERRIDE_USE_OVERRIDE					// 
		IF IS_CREATOR_PLACEMENT_BUTTON_PRESSED(sFMMCMenu)
			IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
				IF CAN_PLACE_BUTTON_BE_PRESSED(sFMMCmenu, sCurrentVarsStruct)
					IF NOT IS_PLACEMENT_IN_A_RESTRICTED_AREA(sCurrentVarsStruct.vCoronaPos)
						IF NOT IS_ANY_OTHER_FMC_CHECKPOINT_LIST_IN_CORONA(sCurrentVarsStruct, iCheckpoint, 10.0)
						OR IS_ROCKSTAR_DEV()
							IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
								sCurrentVarsStruct.bDisplayFailReason = FALSE
								PLAY_SOUND_FRONTEND(-1, "ADD_SPAWN_POINT", GET_CREATOR_SPECIFIC_SOUND_SET())
								PRINTLN("[CHECKPOINT] ADD_SPAWN_POINT SOUND EFFECT in soundset ", GET_CREATOR_SPECIFIC_SOUND_SET(), "!!!")
								
								RETURN TRUE
							ENDIF
						ELSE
							sCurrentVarsStruct.bDisplayFailReason = TRUE
							sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
							sCurrentVarsStruct.sFailReason = "FMMC_HELP0GT"
						ENDIF
					ELSE
						SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
						sCurrentVarsStruct.bFirstShapeTestCheckDone = TRUE
						sCurrentVarsStruct.sFailReason = "FMMC_ER_024"
						SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bRestrictedFail)
						
						RETURN FALSE
					ENDIF
					
					PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
					PRINTLN("[CHECKPOINT] ERROR SOUND EFFECT!!!")
				ENDIF		
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FMC_ENTITY_IN_CORONA(structFMMC_MENU_ITEMS &sFMMCmenu, ENTITY_INDEX eiPassed, CREATION_VARS_STRUCT &sCurrentVarsStruct, FLOAT fRadius = 1.0, INT iNumber = 0, INT iType = -1, INT iSubIndex = -1)
	IF eiPassed = NULL
		SWITCH iType
			CASE CREATION_TYPE_GOTO_LOC
				RETURN IS_FMC_GO_TO_POINT_IN_CORONA(sCurrentVarsStruct, iNumber, fRadius + 1.5)
			CASE CREATION_TYPE_PATROL
				RETURN IS_FMC_PATROL_NODE_IN_CORONA(sCurrentVarsStruct, iNumber, iSubIndex, fRadius + 0.5)
			CASE CREATION_TYPE_SPAWN_LOCATION
				RETURN IS_CUSTOM_SPAWN_IN_CORONA(sCurrentVarsStruct, iNumber, fRadius + 1.5)
			CASE CREATION_TYPE_LEAVE_AREA
				RETURN IS_FMC_LEAVE_AREA_IN_CORONA(sCurrentVarsStruct, iNumber, fRadius + 1.5)
			CASE CREATION_TYPE_PORTALS
				RETURN IS_FMC_PORTAL_POINT_IN_CORONA(sCurrentVarsStruct, iNumber, fRadius + 1.5)
			CASE CREATION_TYPE_PORTAL_WARP
				RETURN IS_FMC_PORTAL_WARP_POINT_IN_CORONA(sCurrentVarsStruct, iNumber, fRadius + 1.5,sFMMCMenu.iSelectedFMCEntity)
			CASE CREATION_TYPE_SEARCH_AREA
				RETURN IS_FMC_SEARCH_AREA_IN_CORONA(sCurrentVarsStruct, iNumber, fRadius + 1.5)
			CASE CREATION_TYPE_CHECKPOINTS_LIST
				RETURN IS_FMC_CHECKPOINTS_LIST_IN_CORONA(sCurrentVarsStruct, iNumber, fRadius + 1.5)
		ENDSWITCH
	ELIF DOES_ENTITY_EXIST(eiPassed)
		IF IS_ENTITY_AT_COORD(eiPassed, sCurrentVarsStruct.vCoronaPos, <<fRadius, fRadius, fRadius + (fRadius/2)>>, FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_FMC_CREATION_TYPE_BE_PICKED_UP(structFMMC_MENU_ITEMS 	&sFMMCmenu, INT iCreationType)

	UNUSED_PARAMETER(sFMMCmenu)

	SWITCH iCreationType
		CASE CREATION_TYPE_FMMC_ZONE
		CASE CREATION_TYPE_PLAYER_RULE
			RETURN FALSE
		BREAK
	ENDSWITCH
	
//	SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
//		CASE ciFMC_CUSTOM_SPAWN
//			RETURN FALSE
//	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_FMC_ENTITY_IN_THE_CORONA_NEW(	structFMMC_MENU_ITEMS 	&sFMMCmenu, 
										HUD_COLOURS_STRUCT 		&sHCS, 
										CREATION_VARS_STRUCT 	&sCurrentVarsStruct, 
										INT 					iType, 
										INT 					iNumberPassed, 
										ENTITY_INDEX 			eiPassed, 
										BOOL 					&bPlaneCorona, 
										HUD_COLOURS 			hcHudColourPassed,
										FMMC_LOCAL_STRUCT 		&sFMMCdata,
										FMMC_COMMON_MENUS_VARS 	&sFMMCendStage, 
										FLOAT 					fRadius = 1.0,
										BOOL					bCanEdit = TRUE,
										INT						iSubIndex = -1)
	
	INT creatorMenuSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
	IF creatorMenuSelection = -1
		creatorMenuSelection = 0
	ENDIF
	
	IF sFMMCmenu.iSelectedEntity != -1
		RETURN FALSE
	ENDIF
	
	//Exit out of here if you are currently overriding the position or rotation of a prop
	IF (sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_POSITION AND IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override))
	OR (sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_ROTATION AND IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override))
	OR (sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_POSITION AND IS_BIT_SET(sFMMCmenu.iDynoPropBitset, ciFMMC_DYNOPROP_Position_Override))
		RETURN FALSE
	ENDIF
	
	// PLAYERS SHOULD NOT BE ABLE TO EDIT OBJECTS WHEN IN 3RD PERSON CAM WITH NO MENU
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bCameraActive)
	AND NOT IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
		RETURN FALSE
	ENDIF
			
	IF sCurrentVarsStruct.iReSelectTimer < GET_GAME_TIMER()
	AND SHOULD_FMC_ENTITY_IN_THE_CORONA_UPDATE(sFMMCmenu, iType)
	AND (IS_FMC_ENTITY_IN_CORONA(sFMMCmenu, eiPassed, sCurrentVarsStruct, fRadius, iNumberPassed, iType, iSubIndex)
		OR (eiPassed = sCurrentVarsStruct.vCoronaHitEntity AND eiPassed != NULL))
	AND NOT (iType = CREATION_TYPE_VEHICLES AND sCurrentVarsStruct.iHoverEntityType = CREATION_TYPE_PEDS)
	AND NOT sFMMCmenu.bPlacementOverride
	AND NOT (iType = CREATION_TYPE_PROPS AND IS_PROP_UNSELECTABLE(eiPassed, sFMMCmenu))
		SET_BIT(iLocalBitSet, biEntityInCorona)
		
		SET_HOVER_ENTITY(sCurrentVarsStruct, iType, iNumberPassed)
		bPlaneCorona = FALSE
		
		IF NOT IS_AREA_CLEAR_PLACEMENT_OVERRIDE_ACTIVE(sFMMCmenu)
		AND NOT (IS_A_PROP_CHAIN_SNAP_ACTIVE(sFMMCmenu) AND sFMMCmenu.iEntityCreation = CREATION_TYPE_PROPS)
			PRINTLN("IS_FMC_ENTITY_IN_THE_CORONA_NEW - sFMMCmenu.iSelectedEntity: ", sFMMCmenu.iSelectedEntity)
			PRINTLN("IS_FMC_ENTITY_IN_THE_CORONA_NEW - sFMMCmenu.iEntityCreation: ", sFMMCmenu.iEntityCreation)
			PRINTLN("IS_FMC_ENTITY_IN_THE_CORONA_NEW - iType: ", iType)
			
			IF bCanEdit
			AND SHOULD_PICK_UP_WORK(sFMMCmenu.iSelectedEntity, sFMMCmenu.iEntityCreation, iType)
			AND CAN_FMC_CREATION_TYPE_BE_PICKED_UP(sFMMCmenu, iType)
			AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_DELETE_BUTTON())

				IF NOT IS_ENTITY_SUB_MENU_HIGHLIGHTED(sFMMCmenu)
				AND sFMMCmenu.sMenuAction[creatorMenuSelection] != efmmc_action_tick_box
					
					IF CAN_FMC_CREATION_TYPE_BE_PICKED_UP(sFMMCmenu, iType)
						SET_BIT(iLocalBitSet, biCanEditEntityInCorona)
					ENDIF

					IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sFMMCmenu.tdCycleSwitchCooldownTimer, 750)	
					AND NOT DOING_HILL_VEHICLE_CREATION(sFMMCmenu)
					AND NOT DOING_HILL_OBJECT_CREATION(sFMMCmenu)
						IF sFMMCmenu.iSwitchCam != iNumberPassed							
							sFMMCmenu.iSwitchCam = iNumberPassed
							REFRESH_MENU(sFMMCmenu)
						ENDIF
					ENDIF
			
					sHCS.hcCurrentCoronaColour = hcHudColourPassed							
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_ACCEPT_BUTTON(sFMMCdata)) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu)
						SET_BIT(iLocalBitSet, biPickupButtonPressed)
						sHCS.hcCurrentCoronaColour = sHCS.hcDefaultCoronaColour
						sFMMCendStage.bMajorEditOnLoadedMission = TRUE
						sCurrentVarsStruct.iReSelectTimer = GET_GAME_TIMER() + 200
						PRINTLN("IS_FMC_ENTITY_IN_THE_CORONA_NEW - Picking UP entity")
						SET_BIT(iLocalBitSet, biCurrentlySelectedAnEntity)
						PRINTLN("IS_FMC_ENTITY_IN_THE_CORONA_NEW - Entity type - ", iType, " - ", iNumberPassed, " - being reselected")
						RETURN TRUE
					ENDIF
					
				ENDIF
				
			ENDIF
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_DELETE_BUTTON())
			IF CAN_DELETE_BUTTON_BE_PRESSED(sFMMCmenu, sCurrentVarsStruct)
				sFMMCmenu.iSwitchCam--
				SET_BIT(iLocalBitSet, biDeleteButtonPressed)
				sHCS.hcCurrentCoronaColour = sHCS.hcDefaultCoronaColour
				sFMMCendStage.bMajorEditOnLoadedMission = TRUE
				sCurrentVarsStruct.iReSelectTimer = GET_GAME_TIMER() + 200
				PRINTLN("IS_FMC_ENTITY_IN_THE_CORONA_NEW - Deleting Entity!")
				PRINTLN("IS_FMC_ENTITY_IN_THE_CORONA_NEW - Entity type - ", iType, " - ", iNumberPassed, " - being deleted")
				RETURN TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ped Creation
// ##### Description: All functions for creating peds
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC FMC_UNLOAD_ALL_MENU_PED_MODELS(structFMMC_MENU_ITEMS &sFMMCmenu)
	INT i, i2
	FOR i = 0 TO MAX_NUM_IN_PED_LIBRARY-1
		FOR i2 = 0 TO MAX_NUM_PEDS_PER_LIBRARY-1
			IF sFMMCMenu.mnPedModels[i][i2] != DUMMY_MODEL_FOR_SCRIPT
				IF IS_MODEL_VALID(sFMMCMenu.mnPedModels[i][i2])
					SET_MODEL_AS_NO_LONGER_NEEDED(sFMMCMenu.mnPedModels[i][i2])
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR 
ENDPROC

// Common Ped create function that can be called from multiple places to create and set up a ped
FUNC BOOL CREATE_PED_FMC(PED_INDEX &piPassed, MISSION_PLACEMENT_DATA_PEDS &sEditedPed, BOOL bGiveWeapon = TRUE)
	IF sEditedPed.model != DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("CREATE_PED_FMC - 1")
		IF HAS_MODEL_LOADED(sEditedPed.model)
			PRINTLN("CREATE_PED_FMC - 2")
			piPassed = CREATE_PED(PEDTYPE_MISSION, sEditedPed.model, sEditedPed.vCoords, sEditedPed.fHeading, FALSE, FALSE)
			IF bGiveWeapon
				IF sEditedPed.weapon != WEAPONTYPE_INVALID
				AND sEditedPed.weapon != WEAPONTYPE_UNARMED
					GIVE_WEAPON_TO_PED(piPassed, sEditedPed.weapon, 1000, TRUE)
				ENDIF
			ENDIF
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPassed, TRUE)
			SET_ENTITY_PROOFS(piPassed, TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_ENTITY_HEADING(piPassed, sEditedPed.fHeading)
			PRINTLN("CREATE_PED_FMC - 3")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_ALL_FMC_PEDS(structFMMC_MENU_ITEMS &sFMMCmenu)

	INT i
	// Loop over all peds and create them
	FOR i = 0 TO (sMissionPlacementData.Ped.iCount - 1)
		IF NOT DOES_ENTITY_EXIST(sPedStruct.piPed[i])
			REQUEST_MODEL(sMissionPlacementData.Ped.Peds[i].model)
			IF HAS_MODEL_LOADED(sMissionPlacementData.Ped.Peds[i].model)
				CREATE_PED_FMC(sPedStruct.piPed[i], sMissionPlacementData.Ped.Peds[i])
				
				ADD_MODEL_TO_CREATOR_BUDGET(sMissionPlacementData.Ped.Peds[i].model)
				SET_WAIT_FOR_COLLISIONS_BEFORE_PROBE(sPedStruct.piPed[i], TRUE)
				FREEZE_ENTITY_POSITION(sPedStruct.piPed[i], TRUE)
				SET_ENTITY_COLLISION(sPedStruct.piPed[i], FALSE)
				
				// Add its custom prop to the list if it uses one
				IF IS_LONG_BIT_SET(sMissionPlacementData.Ped.Peds[i].iBitset, ENUM_TO_INT(ePEDDATABITSET_CUSTOM_PED_MODEL))
					INT iCustom
					BOOL bAdd = TRUE
					FOR iCustom = 0 TO ciMAX_CUSTOM_DEV_PEDS-1
						IF sMissionPlacementData.Ped.Peds[i].model = sFMMCMenu.sCustomPeds.mnCustomPed[iCustom]
							bAdd = FALSE
							BREAKLOOP
						ENDIF
					ENDFOR
					IF bAdd
						ADD_CUSTOM_PED(sFMMCmenu, sMissionPlacementData.Ped.Peds[i].model)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	// Make sure they all exist
	FOR i = 0 TO (sMissionPlacementData.Ped.iCount - 1)
		IF NOT DOES_ENTITY_EXIST(sPedStruct.piPed[i])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	// Unload their models
	REPEAT sMissionPlacementData.Ped.iCount i
		IF sMissionPlacementData.Ped.Peds[i].model != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(sMissionPlacementData.Ped.Peds[i].model)
		ENDIF
	ENDREPEAT	
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL FMC_MOVE_LAST_PLACED_PED(structFMMC_MENU_ITEMS &sFMMCmenu, PED_INDEX eiPassed, VECTOR &vCoord, FLOAT &fHead, CREATION_VARS_STRUCT &sCurrentVarsStruct, FMMC_LOCAL_STRUCT &sFMMCdata)

	// Update the entity to the position and rotation of the corona
	FMC_MOVE_ENTITY( eiPassed, vCoord, fHead, sCurrentVarsStruct, sFMMCdata, sFMMCmenu)
	
	IF IS_BIT_SET(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
		
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
			IF FMC_CAN_PLACE_BUTTON_BE_PRESSED(sFMMCmenu)
				
				// Allows you to bypass the placement restrictions if you press square.
				IF IS_ROCKSTAR_DEV()
				AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_DELETE_BUTTON())
					FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
					sCurrentVarsStruct.bFirstShapeTestCheckDone = TRUE
					sCurrentVarsStruct.sCoronaShapeTest.vPos = sCurrentVarsStruct.vCoronaPos
					sCurrentVarsStruct.sCoronaShapeTest.vRefLastShapeTestPos = sCurrentVarsStruct.vCoronaPos
				ELSE
					IF FMC_IS_AREA_CLEAR_FOR_PLACEMENT(sCurrentVarsStruct, sCurrentVarsStruct.fCheckPointSize, eiPassed, FALSE, NULL, DEFAULT, DEFAULT , DEFAULT, DEFAULT, DEFAULT, sCurrentVarsStruct.bForceCoronaSizeCheck)
						FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
					ENDIF
				ENDIF
				
				// Places the ped
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu)
					IF sCurrentVarsStruct.bAreaIsGoodForPlacement
					AND IS_BIT_SET(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
						PLAY_SOUND_FRONTEND(-1, "PLACE_ENEMY", GET_CREATOR_SPECIFIC_SOUND_SET())
						PRINTSTRING("PLACE_ENEMY SOUND EFFECT!!!")PRINTNL()	
						CLEAR_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
						sCurrentVarsStruct.bDisplayFailReason = FALSE
						REFRESH_MENU_ASAP(sFMMCmenu)
						RETURN TRUE
					ELSE
						PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
						PRINTSTRING("MOVE_LAST_PLACED_VEHICLE ERROR SOUND EFFECT 1!!!")PRINTNL()
						sCurrentVarsStruct.bDisplayFailReason = TRUE
						sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
						IF FMC_IS_THIS_FAIL_REASON_SET(sCurrentVarsStruct, bLocalReasonFail)
							sCurrentVarsStruct.sFailReason = "FMMC_ER_011"
						ELSE
							sCurrentVarsStruct.sFailReason = GET_GENERIC_PLACEMENT_FAIL_DESCRIPTION(sCurrentVarsStruct)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
			ENDIF
		ENDIF
	ELSE
		SET_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
	ENDIF
	
	// Sets the alpha to show if it is or isn't an acceptable place to place
	IF !sCurrentVarsStruct.bAreaIsGoodForPlacement
		SET_ENTITY_ALPHA(eiPassed, 150, FALSE)
	ELSE
		RESET_ENTITY_ALPHA(eiPassed)
	ENDIF
	
	SET_ENTITY_COORDS(eiPassed, GET_ENTITY_COORDS(eiPassed) + <<0,0,-0.60>>)
	
	SET_ENTITY_VISIBLE(sCurrentVarsStruct.obCoronaObject, TRUE)
	
	RETURN FALSE
	
ENDFUNC

FUNC MODEL_NAMES FMC_GET_PED_MODEL_FROM_MENU_SELECTION(structFMMC_MENU_ITEMS &sFMMCMenu)
	IF sFMMCMenu.iPedLibrary = PED_LIBRARY_CUSTOM
		IF sFMMCMenu.iPedType > 0
			RETURN sFMMCmenu.mnPedModels[sFMMCMenu.iPedLibrary][sFMMCMenu.iPedType-1]
		ELSE
			RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDIF
	ENDIF
	RETURN sFMMCmenu.mnPedModels[sFMMCMenu.iPedLibrary][sFMMCMenu.iPedType]
ENDFUNC

FUNC INT GET_PED_SELECTION_FROM_CUSTOM_MODEL(structFMMC_MENU_ITEMS &sFMMCmenu, MODEL_NAMES mnPassed)
	INT i
	FOR i = 0 TO sFMMCMenu.sCustomPeds.iCurrentNumberOfDevPeds - 1
		IF sFMMCMenu.sCustomPeds.mnCustomPed[i] = mnPassed
			RETURN i+1
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

PROC GET_PED_LIBRARY_AND_SELECTION_FROM_MODEL(structFMMC_MENU_ITEMS &sFMMCmenu)
	
	// Grab the model/libaray from the standard list
	sFMMCmenu.iPedLibrary = GET_PED_LIBRARY_FROM_MODEL(GET_ENTITY_MODEL(sPedStruct.piTempPed), sFMMCMenu)
	sFMMCmenu.iPedType = GET_PED_TYPE_FROM_MODEL(GET_ENTITY_MODEL(sPedStruct.piTempPed), sFMMCMenu, sFMMCmenu.iPedLibrary) 
	
	// If it's not in there update it to go into the custom library
	IF sFMMCmenu.iPedLibrary = -1
		sFMMCmenu.iPedType = GET_PED_SELECTION_FROM_CUSTOM_MODEL(sFMMCmenu, sEditedPedPlacementData.model)
		IF sFMMCmenu.iPedType > -1
			sFMMCmenu.iPedLibrary = PED_LIBRARY_CUSTOM
		ELIF sFMMCMenu.sCustomPeds.iCurrentNumberOfDevPeds < ciMAX_CUSTOM_DEV_PEDS
			ADD_CUSTOM_PED(sFMMCmenu, sEditedPedPlacementData.model)
			sFMMCmenu.iPedType = sFMMCMenu.sCustomPeds.iCurrentNumberOfDevPeds
			sFMMCmenu.iPedLibrary = PED_LIBRARY_CUSTOM
		ENDIF
	ENDIF
	IF sFMMCmenu.iPedLibrary = PED_LIBRARY_CUSTOM
		sFMMCmenu.iPedType += 1
	ENDIF
ENDPROC

FUNC INT GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(VECTOR vEntityCoord)
	INT iInterior
	REPEAT MAX_NUM_MISSION_INTERIORS iInterior
		IF sMissionPlacementData.Interior.eInteriors[iInterior].eType != eFMCINTERIOR_INVALID
		AND IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS_WITH_TYPE(vEntityCoord, GET_FMC_INTERIOR_STRING_FOR_CREATOR(sMissionPlacementData.Interior.eInteriors[iInterior].eType)))
			RETURN iInterior
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

PROC DO_FMC_PED_CREATION(MISSION_PLACEMENT_DATA_PEDS &sEditedPed,
						CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
						HUD_COLOURS_STRUCT 			&sHCSpassed,
						structFMMC_MENU_ITEMS 		&sFMMCmenu, 
						FMMC_LOCAL_STRUCT 			&sFMMCData, 
						INT 						iMaximum = MAX_NUM_PEDS)
	INT iPedNumber
	
	STRING sPedType = "FMMC_AB_01"
	MODEL_NAMES mnPedModel = FMC_GET_PED_MODEL_FROM_MENU_SELECTION(sFMMCMenu)
	
	PRINTLN("[DO_FMC_PED_CREATION]")
	
	// Determine which ped we're working with
	IF sFMMCMenu.iSelectedEntity = -1
		iPedNumber = sMissionPlacementData.Ped.iCount
	ELSE

		iPedNumber = sFMMCMenu.iSelectedEntity
		
		IF iPedNumber >= iMaximum 
			iPedNumber = iMaximum - 1
			sFMMCMenu.iSelectedEntity = iPedNumber
		ENDIF
		
		// If you've picked up a ped, remove the one that was placed from the world so that it can be recreated as the placement one
		IF DOES_ENTITY_EXIST(sPedStruct.piPed[iPedNumber])
			
			PRINTLN("[DO_FMC_PED_CREATION] - iPedNumber = ", iPedNumber)
			
			IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
				DELETE_PED(sPedStruct.piTempPed)
			ENDIF
			IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[iPedNumber])
				REMOVE_BLIP(sPedStruct.biPedBlip[iPedNumber])
			ENDIF
			
			sPedStruct.piTempPed 						= sPedStruct.piPed[iPedNumber]
			sPedStruct.piPed[iPedNumber] 				= NULL
			
			// Update the menu and edited ped data to that of the one you've just picked up
			sEditedPed = sMissionPlacementData.Ped.Peds[iPedNumber]
			
			// Obligatory: Calculate the Category first before attempting to retrieve the weapon type else you will be using the weapon group from the
			// selection menu instead of the one from the picked up ped.
			sPedStruct.iSelectedPedWeaponCategory = ENUM_TO_INT(GET_WEAPON_CATEGORY_FROM_WEAPON(sMissionPlacementData.Ped.Peds[iPedNumber].weapon))
			sFMMCMenu.sSubTypeName[CREATION_TYPE_ENEMY_WEAPON_CATEG] = GET_ACTOR_WEAPON_GROUP_NAME_FROM_INDEX(sPedStruct.iSelectedPedWeaponCategory)
			
			sPedStruct.iSelectedPedWeaponType = GET_WEAPON_INT_FOR_CREATOR_WEAPON_CATEGORY(sMissionPlacementData.Ped.Peds[iPedNumber].weapon, sPedStruct)
			sFMMCMenu.sSubTypeName[CREATION_TYPE_ENEMY_WEAPON] = GET_WEAPON_NAME(GET_AVAILABLE_WEAPONS_FROM_CATEGORY(sPedStruct.iSelectedPedWeaponType, sPedStruct))
			
			GET_PED_LIBRARY_AND_SELECTION_FROM_MODEL(sFMMCmenu)
			
			sCurrentVarsStruct.fCreationHeading			= sEditedPed.fHeading
						
			sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_PEDS] = sFMMCmenu.iPedType
			sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_PED_LIBRARY] = sFMMCmenu.iPedLibrary	
						
			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
			IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
				CLEAR_PED_TASKS_IMMEDIATELY(sPedStruct.piTempPed)
				SET_ENTITY_COLLISION(sPedStruct.piTempPed, FALSE)
			ENDIF
			
		ENDIF
		
		REQUEST_MODEL(sEditedPed.model)
	ENDIF
	
	// Error for if you try to place one when too many already exist
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PEDS)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		AND sFMMCMenu.iSelectedEntity = -1
			STRING sMax = "FMMC_ER_TP"
			DO_TOO_MANY_ENTITY_ERROR(sCurrentVarsStruct, sPedStruct.flashColour, sMax)
		ENDIF
	ENDIF
	
	// Ped counter UI
	IF sMissionPlacementData.Ped.iCount >= iMaximum
	AND iPedNumber >= iMaximum
		sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
		PRINTLN("DO_FMC_PED_CREATION - Moving to CREATION_STAGE_WAIT 1")
		CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PEDS )
		IF sPedStruct.flashColour = HUDFLASHING_NONE
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.Ped.iCount,	iMaximum, sPedType, -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sPedStruct.flashColour, 0)
		ELSE
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.Ped.iCount,	iMaximum, sPedType, -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sPedStruct.flashColour, 500)
		ENDIF
		PRINTLN("DO_FMC_PED_CREATION - exit")
		EXIT
	ELSE
		sPedStruct.flashColour = HUDFLASHING_NONE
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PEDS )
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.Ped.iCount,	iMaximum, sPedType, -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sPedStruct.flashColour, 0)
	ENDIF
	
	// Main creation state machine
	SWITCH sPedStruct.iSwitchingINT
	
		// Wait stage to stall the creation of more if there are too many already
		CASE CREATION_STAGE_WAIT
			IF sMissionPlacementData.Ped.iCount <= MAX_NUM_PEDS		
			AND iPedNumber < MAX_NUM_PEDS
				FMC_UNLOAD_ALL_MENU_PED_MODELS(sFMMCMenu)
				IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
					DELETE_PED(sPedStruct.piTempPed)
				ENDIF	
				sPedStruct.iSwitchingINT = CREATION_STAGE_SET_UP
				PRINTLN("DO_FMC_PED_CREATION - Moving to CREATION_STAGE_SET_UP 1")
			ENDIF
		BREAK
		
		// Request the model ready for creation
		CASE CREATION_STAGE_SET_UP
			IF NOT DOES_ENTITY_EXIST(sPedStruct.piTempPed)
							
				sEditedPed.vCoords = sCurrentVarsStruct.vCoronaPos
				sEditedPed.fHeading = sCurrentVarsStruct.fCreationHeading
				sEditedPed.Model = mnPedModel
				
				IF NOT IS_MODEL_VALID(mnPedModel)
					SCRIPT_ASSERT("Invalid ped model")
				ELSE
					sPedStruct.iSwitchingINT = CREATION_STAGE_MAKE
					PRINTLN("DO_FMC_PED_CREATION - Moving to CREATION_STAGE_MAKE 1")
					REQUEST_MODEL(mnPedModel)
				ENDIF
				
			ELSE
				sPedStruct.iSwitchingINT = CREATION_STAGE_PLACE
				PRINTLN("DO_FMC_PED_CREATION - Moving to CREATION_STAGE_PLACE 1")
			ENDIF
		BREAK
		
		// Create the new placement ped for the corona
		CASE CREATION_STAGE_MAKE
		
			IF NOT DOES_ENTITY_EXIST(sPedStruct.piTempPed)
				REQUEST_MODEL(mnPedModel)
				IF HAS_MODEL_LOADED(mnPedModel)
					IF CREATE_PED_FMC(sPedStruct.piTempPed, sEditedPed)
						FMMC_SET_PED_VARIATION(sPedStruct.piTempPed, mnPedModel, sFMMCmenu.iPedVariation)
						SET_ENTITY_HEADING(sPedStruct.piTempPed, sCurrentVarsStruct.fCreationHeading)
						SET_ENTITY_COLLISION(sPedStruct.piTempPed, FALSE)	
						SET_ENTITY_ALPHA(sPedStruct.piTempPed, 0, FALSE)
						sPedStruct.iSwitchingINT = CREATION_STAGE_PLACE
						PRINTLN("DO_FMC_PED_CREATION - Moving to CREATION_STAGE_PLACE 2")
						IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_BASE
							RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
						ENDIF
						sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
						sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL
						SET_MODEL_AS_NO_LONGER_NEEDED(mnPedModel)
					ENDIF
				ENDIF
			ELSE
				sPedStruct.iSwitchingINT = CREATION_STAGE_PLACE
				PRINTLN("DO_FMC_PED_CREATION - Moving to CREATION_STAGE_PLACE 3")
			ENDIF
			
		BREAK
		
		// Do the placement of the ped, updating it's position/rotation/model/etc based on user input
		CASE CREATION_STAGE_PLACE
		
			sHCSpassed.hcCurrentCoronaColour 	= sHCSpassed.hcColour[CREATION_TYPE_PEDS]	

			sCurrentVarsStruct.fCheckPointSize = ciPED_CHECK_POINT_SIZE
			
			sEditedPed.vCoords = sCurrentVarsStruct.vCoronaPos
			sEditedPed.fHeading = sCurrentVarsStruct.fCreationHeading
			sEditedPed.Model = mnPedModel
			
			#IF IS_DEBUG_BUILD
				IF NOT FMC_DO_IS_MODEL_FMMC_VALID_ASSERT_CHECK(sEditedPed.Model)
					SCRIPT_ASSERT("906308 - sEditedPed.Model = DUMMY_MODEL_FOR_SCRIPT")
					NET_PRINT("[WJK] - 906308 - sEditedPed.Model = DUMMY_MODEL_FOR_SCRIPT")NET_NL()
				ENDIF
			#ENDIF
			
			IF NOT IS_PED_INJURED(sPedStruct.piTempPed)
				// Update the ped's position/rotation and check the area around it is safe
				IF FMC_MOVE_LAST_PLACED_PED(sFMMCmenu,
											sPedStruct.piTempPed,
											sEditedPed.vCoords,
											sEditedPed.fHeading,
											sCurrentVarsStruct,
											sFMMCData)
					
					//Assign edited ped to data ped if the placement button has been pressed
					sMissionPlacementData.Ped.Peds[iPedNumber] = sEditedPed
					sMissionPlacementData.Ped.Peds[iPedNumber].model = FMC_GET_PED_MODEL_FROM_MENU_SELECTION(sFMMCMenu)
					PRINTLN("DO_FMC_PED_CREATION - Setting sMissionPlacementData.Ped.Peds[",iPedNumber,"] from sEditedPed")
					
					sMissionPlacementData.Ped.Peds[iPedNumber].iInterior = GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(sEditedPed.vCoords)
					PRINTLN("DO_FMC_PED_CREATION - Setting sMissionPlacementData.Ped.Peds[",iPedNumber,"].iInterior to ", sMissionPlacementData.Ped.Peds[iPedNumber].iInterior)

					IF sFmmcmenu.iPedLibrary = PED_LIBRARY_CUSTOM
						SET_LONG_BIT(sMissionPlacementData.Ped.Peds[iPedNumber].iBitset, ENUM_TO_INT(ePEDDATABITSET_CUSTOM_PED_MODEL))
					ELSE
						CLEAR_LONG_BIT(sMissionPlacementData.Ped.Peds[iPedNumber].iBitset, ENUM_TO_INT(ePEDDATABITSET_CUSTOM_PED_MODEL))
					ENDIF

					RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
					
					// Reset Ped-Settings
					MISSION_PLACEMENT_DATA_PEDS sBlankPedStruct
					sBlankPedStruct.eSeat = VS_DRIVER
					sBlankPedStruct.iGroup = sEditedPed.iGroup // url:bugstar:7467649 - Freemode Creator - Can we keep the Group option in the Actor menu set the same as the previous ped you placed rather than it reverting back to 0 every time you place a ped please.
					sBlankPedStruct.weapon = sEditedPed.weapon // Davor L - Because if not the Ped won't be placed with their selected weapon.
					sEditedPed = sBlankPedStruct
					
					sPedStruct.iSwitchingINT = CREATION_STAGE_DONE
					PRINTLN("DO_FMC_PED_CREATION - Moving to CREATION_STAGE_DONE 1")
				ENDIF	
			ENDIF
			
			IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
			
				// update the weapon if the user changes it
				IF sEditedPedPlacementData.weapon != sMissionPlacementData.Ped.Peds[iPedNumber].weapon
					REMOVE_ALL_PED_WEAPONS(sPedStruct.piTempPed)
					PRINTLN("REMOVE_ALL_PED_WEAPONS")
					sMissionPlacementData.Ped.Peds[iPedNumber].weapon = sEditedPedPlacementData.weapon
					GIVE_WEAPON_TO_PED(sPedStruct.piTempPed, sMissionPlacementData.Ped.Peds[iPedNumber].weapon, 1000, TRUE)
				ENDIF
				
				//go back through set up if the object model has changed
				IF mnPedModel != GET_ENTITY_MODEL(sPedStruct.piTempPed)
					IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[iPedNumber])
						REMOVE_BLIP(sPedStruct.biPedBlip[iPedNumber])
					ENDIF
					MODEL_NAMES oldModel
					oldModel = GET_ENTITY_MODEL(sPedStruct.piTempPed)
					DELETE_PED(sPedStruct.piTempPed)
					SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
					sPedStruct.iSwitchingINT = CREATION_STAGE_SET_UP
					PRINTLN("DO_FMC_PED_CREATION - Moving to CREATION_STAGE_SET_UP 2")
				ENDIF
			ENDIF
			
		BREAK
		
		// Update the ped you've just placed, increase the counter and reset for the next one
		CASE CREATION_STAGE_DONE
		
			IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
				sPedStruct.piPed[iPedNumber] 	= sPedStruct.piTempPed
				FMC_UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, sPedStruct.piTempPed, TRUE, FALSE, 0.0)
				sPedStruct.piTempPed 			= NULL

				IF NOT IS_PED_INJURED(sPedStruct.piPed[iPedNumber])
				
					IF NOT IS_PED_IN_ANY_VEHICLE(sPedStruct.piPed[iPedNumber])
						SET_ENTITY_COLLISION(sPedStruct.piPed[iPedNumber], TRUE)
					ENDIF
					RESET_ENTITY_ALPHA(sPedStruct.piPed[iPedNumber])
					
					CREATE_FMMC_BLIP(sPedStruct.biPedBlip[iPedNumber], sMissionPlacementData.Ped.Peds[iPedNumber].vCoords, HUD_COLOUR_RED, "FMMC_B_9", 1)
					FMC_CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)
					
					#IF IS_DEBUG_BUILD
						TEXT_LABEL tl15 
						tl15 = "Ped - "
						//ret
						tl15 += iPedNumber
						SET_PED_NAME_DEBUG(sPedStruct.piPed[iPedNumber], tl15)
					#ENDIF
					
					IF sFMMCMenu.iSelectedEntity = -1
						
						sMissionPlacementData.Ped.iCount++
						
						IF sMissionPlacementData.Ped.iCount > MAX_NUM_PEDS
							sMissionPlacementData.Ped.iCount = MAX_NUM_PEDS
						ENDIF
					ENDIF
					
				ENDIF
				
				MPGlobalsScoreHud.iGoalFadeFlashing_GenericScore[0] = 255
				REINIT_NET_TIMER(MPGlobalsScoreHud.iGoalMetFlashing_GenericScore[0], TRUE)	
				
				REFRESH_MENU(sFMMCMenu)
				sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
				PRINTLN("DO_FMC_PED_CREATION - Moving to CREATION_STAGE_WAIT 2")
			ENDIF
			
			sFMMCmenu.iPedPhotoRange = 0
			sFMMCmenu.iPedFollowRange = 0
			
			sFMMCMenu.iSelectedEntity = -1
			
		BREAK
		
	ENDSWITCH
	
	FMC_UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, sPedStruct.piTempPed)

ENDPROC

PROC UPDATE_TAGS(INT iEntity, eTAG_TYPE eType)

	UNUSED_PARAMETER(iEntity)
	UNUSED_PARAMETER(eType)

	#IF MAX_NUM_TAGS
	INT i
	REPEAT MAX_NUM_TAGS i
		IF sMissionPlacementData.Tags.Tag[i].eType = eType
			IF sMissionPlacementData.Tags.Tag[i].iID = iEntity
				sMissionPlacementData.Tags.Tag[i].eType = eTAGTYPE_PED
				sMissionPlacementData.Tags.Tag[i].iID = -1
				sMissionPlacementData.Tags.Tag[i].tlName = ""
				PRINTLN("UPDATE_TAGS - Removed tag [",i,"] due to ", GET_TAG_TYPE_DISPLAY_NAME(eType), " ",iEntity, " being deleted.")
			ELIF sMissionPlacementData.Tags.Tag[i].iID > iEntity
				sMissionPlacementData.Tags.Tag[i].iID--
				PRINTLN("UPDATE_TAGS - Moved tag [",i,"] to entity ",sMissionPlacementData.Tags.Tag[i].iID," due to ", GET_TAG_TYPE_DISPLAY_NAME(eType), " ",iEntity, " being deleted.")
			ENDIF
		ENDIF
	ENDREPEAT
	#ENDIF

ENDPROC

PROC UPDATE_TARGETS(ENTITY_TYPE eType, INT iEntity)
	
	MISSION_PLACEMENT_DATA_TAKE_OUT_TARGET emptyTargetStruct

	INT iSTAGE, iTarget, i
	
	INT iStoredStage = -1
	INT iStoredSlot = -1
	INT iStoredEntityId = -1
	
	//Find the target to delete
	REPEAT MAX_NUM_TAKE_OUT_TARGET_STAGES iSTAGE
		REPEAT MAX_NUM_TARGETS iTarget
			IF sMissionPlacementData.TakeOutTarget[iSTAGE].TakeOutTargets[iTarget].eType = eType
			AND sMissionPlacementData.TakeOutTarget[iSTAGE].TakeOutTargets[iTarget].iIndex = iEntity
				PRINTLN("[TARGETS] [UPDATE_TARGETS] [CLEAR] sMissionPlacementData.TakeOutTarget[",iSTAGE,"].TakeOutTargets[",iTarget,"].iIndex = ", sMissionPlacementData.TakeOutTarget[iSTAGE].TakeOutTargets[iTarget].iIndex)
				
				iStoredStage = iSTAGE
				iStoredSlot = iTarget
				iStoredEntityId = iEntity
				
				BREAKLOOP
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	iSTAGE = 0
	iTarget = 0
	
	IF iStoredStage != -1
	
//		REPEAT MAX_NUM_TAKE_OUT_TARGET_STAGES iSTAGE
//			REPEAT MAX_NUM_TARGETS iTarget
//				IF sMissionPlacementData.TakeOutTarget[iSTAGE].TakeOutTargets[iTarget].iIndex != -1
//					PRINTLN("7413496, BEFORE sMissionPlacementData.TakeOutTarget[",iSTAGE,"].TakeOutTargets[",iTarget,"].iIndex = ", sMissionPlacementData.TakeOutTarget[iSTAGE].TakeOutTargets[iTarget].iIndex)
//				ENDIF
//			ENDREPEAT
//		ENDREPEAT
		
		//Move all the entries down
		INT iNumTargets = sMissionPlacementData.TakeOutTarget[iStoredStage].iCount
		FOR i = iStoredSlot + 1 TO iNumTargets - 1
			IF sMissionPlacementData.TakeOutTarget[iStoredStage].TakeOutTargets[i - 1].iIndex != -1
			AND sMissionPlacementData.TakeOutTarget[iStoredStage].TakeOutTargets[i - 1].eType = eType
				sMissionPlacementData.TakeOutTarget[iStoredStage].TakeOutTargets[i - 1] = sMissionPlacementData.TakeOutTarget[iStoredStage].TakeOutTargets[i]
			ENDIF
		ENDFOR
		
		//Clear the last entry
		COPY_SCRIPT_STRUCT(sMissionPlacementData.TakeOutTarget[iStoredStage].TakeOutTargets[iNumTargets - 1], emptyTargetStruct, SIZE_OF(MISSION_PLACEMENT_DATA_TAKE_OUT_TARGET))
		
		//Decrement the indexes above the one deleted
		REPEAT MAX_NUM_TAKE_OUT_TARGET_STAGES iSTAGE	
			REPEAT MAX_NUM_TARGETS iTarget
				IF sMissionPlacementData.TakeOutTarget[iSTAGE].TakeOutTargets[iTarget].iIndex > iStoredEntityId
				AND sMissionPlacementData.TakeOutTarget[iSTAGE].TakeOutTargets[iTarget].eType = eType
					sMissionPlacementData.TakeOutTarget[iSTAGE].TakeOutTargets[iTarget].iIndex --
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
//		REPEAT MAX_NUM_TAKE_OUT_TARGET_STAGES iSTAGE
//			REPEAT MAX_NUM_TARGETS iTarget
//				IF sMissionPlacementData.TakeOutTarget[iSTAGE].TakeOutTargets[iTarget].iIndex != -1
//					PRINTLN("7413496, AFTER sMissionPlacementData.TakeOutTarget[",iSTAGE,"].TakeOutTargets[",iTarget,"].iIndex = ", sMissionPlacementData.TakeOutTarget[iSTAGE].TakeOutTargets[iTarget].iIndex)
//				ENDIF
//			ENDREPEAT
//		ENDREPEAT
		
		//Clear all the old bits
		REPEAT MAX_NUM_TAKE_OUT_TARGET_STAGES iSTAGE
			sTargetStruct.iPedTargetBitSet[iSTAGE] = 0
			sTargetStruct.iVehicleTargetBitSet[iSTAGE] = 0
			sTargetStruct.iPropTargetBitSet[iSTAGE] = 0
		ENDREPEAT
		
		//Reset the new bits
		INITIALISE_EXISTING_TARGETS()
		
		PRINTLN("[TARGETS] [UPDATE_TARGETS] [CLEAR] BEFORE sMissionPlacementData.TakeOutTarget[", iStoredStage,"].iCount = ", sMissionPlacementData.TakeOutTarget[iStoredStage].iCount)
		
		//Update the stored counts
		REPEAT MAX_NUM_TAKE_OUT_TARGET_STAGES iSTAGE
			IF sMissionPlacementData.TakeOutTarget[iSTAGE].iCount != GET_NUM_TARGET_ENTITIES(iSTAGE)
				sMissionPlacementData.TakeOutTarget[iSTAGE].iCount = GET_NUM_TARGET_ENTITIES(iSTAGE)
			ENDIF
		ENDREPEAT

		PRINTLN("[TARGETS] [UPDATE_TARGETS] [CLEAR] AFTER sMissionPlacementData.TakeOutTarget[", iStoredStage,"].iCount = ", sMissionPlacementData.TakeOutTarget[iStoredStage].iCount)
		iStoredStage = -1
		iStoredSlot = -1
		iStoredEntityId = -1
	ENDIF
	
ENDPROC

PROC UPDATE_PED_VEHICLES(INT iEntity)

	UNUSED_PARAMETER(iEntity)

	#IF MAX_NUM_PEDS
	INT i
	FOR i = 0 TO MAX_NUM_PEDS-1
		IF sMissionPlacementData.Ped.Peds[i].iVehicle = iEntity
			sMissionPlacementData.Ped.Peds[i].iVehicle = -1
			PRINTLN("UPDATE_PED_VEHICLES - Removed ped [",i,"] from vehicle ",iEntity, " due to it being deleted.")
		ELIF sMissionPlacementData.Ped.Peds[i].iVehicle > iEntity
			sMissionPlacementData.Ped.Peds[i].iVehicle--
			PRINTLN("UPDATE_PED_VEHICLES - Moved ped [",i,"] to vehicle ",sMissionPlacementData.Ped.Peds[i].iVehicle," due to vehicle ", iEntity, " being deleted.")
		ENDIF
	ENDFOR
	#ENDIF

ENDPROC

PROC UPDATE_AMBUSH_DATA(INT iEntity)
	INT iIndex
	REPEAT MAX_NUM_AMBUSH_VEHICLES iIndex
		IF iEntity = sMissionPlacementData.Ambush.Vehicle[iIndex].iIndex
			sMissionPlacementData.Ambush.Vehicle[iIndex].iIndex = -1
			sMissionPlacementData.Ambush.iCount--
			PRINTLN("UPDATE_AMBUSH_DATA - Removing vehicle #", iEntity, " from ambush #", iIndex, " data as it was deleted, and reducing ambush count to ", sMissionPlacementData.Ambush.iCount)
		ELIF iEntity < sMissionPlacementData.Ambush.Vehicle[iIndex].iIndex
			PRINTLN("UPDATE_AMBUSH_DATA - Moving vehicle #", sMissionPlacementData.Ambush.Vehicle[iIndex].iIndex, " from ambush #", iIndex, " down the array due to vehicle #", iEntity, " being deleted.")
			sMissionPlacementData.Ambush.Vehicle[iIndex].iIndex--
		ENDIF
	ENDREPEAT
ENDPROC

PROC DELETE_FMC_SECURITY_CAMERA(INT iCamera, BOOL bPlaySound = TRUE)
	IF bPlaySound
		// Play delete sound
		PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	ENDIF
	
	INT iNumCameras = GET_NUMBER_OF_FMC_SECURITY_CAMERAS()
	
	// Move all the entries down
	INT i
	FOR i = iCamera + 1 TO iNumCameras - 1
		sMissionPlacementData.SecurityCamera.Cameras[i - 1] = sMissionPlacementData.SecurityCamera.Cameras[i]
	ENDFOR
	
	// Clear the last entry
	MISSION_PLACEMENT_DATA_SECURITY_CAMERA_DATA sTempSecurityCameraStruct
	COPY_SCRIPT_STRUCT(sMissionPlacementData.SecurityCamera.Cameras[iNumCameras - 1], sTempSecurityCameraStruct, SIZE_OF(MISSION_PLACEMENT_DATA_SECURITY_CAMERA_DATA))
ENDPROC

PROC UPDATE_SECURITY_CAMERAS(INT iEntity)

	UNUSED_PARAMETER(iEntity)

	#IF MAX_NUM_SECURITY_CAMERAS
	INT i
	FOR i = 0 TO MAX_NUM_SECURITY_CAMERAS-1
		IF sMissionPlacementData.SecurityCamera.Cameras[i].iProp = iEntity
			DELETE_FMC_SECURITY_CAMERA(i, FALSE)
			PRINTLN("UPDATE_SECURITY_CAMERAS - Removed security camera [",i,"] due to prop ",iEntity, " being deleted.")
			i--
		ELIF sMissionPlacementData.SecurityCamera.Cameras[i].iProp > iEntity
			sMissionPlacementData.SecurityCamera.Cameras[i].iProp--
			PRINTLN("UPDATE_SECURITY_CAMERAS - Moved security camera [",i,"] to prop ",sMissionPlacementData.SecurityCamera.Cameras[i].iProp," due to prop",iEntity, " being deleted.")
		ENDIF
	ENDFOR
	#ENDIF

ENDPROC

PROC DELETE_FMC_PLACED_PED(INT iPedNumber)

	INT i
	MISSION_PLACEMENT_DATA_PEDS sTempPedStruct
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	// Remove the entity/blip
	IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[iPedNumber])
		REMOVE_BLIP(sPedStruct.biPedBlip[iPedNumber])
	ENDIF
	IF DOES_ENTITY_EXIST(sPedStruct.piPed[iPedNumber])
		DELETE_PED(sPedStruct.piPed[iPedNumber])
	ENDIF
	
	// Move all the entries down
	FOR i = iPedNumber+1 TO sMissionPlacementData.Ped.iCount-1
		sPedStruct.biPedBlip[i-1] = sPedStruct.biPedBlip[i]
		sPedStruct.piPed[i-1] = sPedStruct.piPed[i]
		sMissionPlacementData.Ped.Peds[i-1]= sMissionPlacementData.Ped.Peds[i]
	ENDFOR
	
	// Reduce the array
	sMissionPlacementData.Ped.iCount--
	
	// Clear the last entry
	sMissionPlacementData.Ped.Peds[sMissionPlacementData.Ped.iCount] = sTempPedStruct
	sPedStruct.biPedBlip[sMissionPlacementData.Ped.iCount] = NULL
	sPedStruct.piPed[sMissionPlacementData.Ped.iCount] = NULL
	
	// Update tags
	UPDATE_TAGS(iPedNumber, eTAGTYPE_PED)
	UPDATE_TARGETS(ET_PED, iPedNumber)
	
ENDPROC

PROC MAINTAIN_TARGET_BLIPS(structFMMC_MENU_ITEMS &sFMMCmenu, ENTITY_TYPE eType, INT i)

	SWITCH eType
		CASE ET_PED
			IF NOT DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])
				IF IS_ENTITY_A_TARGET(sFMMCmenu, ET_PED, sFMMCMenu.iSelectedFMCEntity, i)
					CREATE_FMMC_BLIP(sPedStruct.biPedBlip[i], sMissionPlacementData.Ped.Peds[i].vCoords, HUD_COLOUR_RED, "FMMC_B_9", 1, DEFAULT, DEFAULT, RADAR_TRACE_TEMP_4)
				ENDIF
			ELSE
				IF NOT IS_ENTITY_A_TARGET(sFMMCmenu, ET_PED, sFMMCMenu.iSelectedFMCEntity, i)
					REMOVE_BLIP(sPedStruct.biPedBlip[i])
				ENDIF
			ENDIF
		BREAK
		CASE ET_VEHICLE
			IF NOT DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[i])
				IF IS_ENTITY_A_TARGET(sFMMCmenu, ET_VEHICLE, sFMMCMenu.iSelectedFMCEntity, i)
					CREATE_FMMC_BLIP(sVehStruct.biVehicleBlip[i], GET_ENTITY_COORDS(sVehStruct.veVehcile[i]), HUD_COLOUR_BLUE, "FMMC_B_10", 1, DEFAULT, DEFAULT, RADAR_TRACE_TEMP_4)
				ENDIF
			ELSE
				IF NOT IS_ENTITY_A_TARGET(sFMMCmenu, ET_VEHICLE, sFMMCMenu.iSelectedFMCEntity, i)
					REMOVE_BLIP(sVehStruct.biVehicleBlip[i])
				ENDIF
			ENDIF
		BREAK
		CASE ET_OBJECT
			IF NOT DOES_BLIP_EXIST(sPropStruct.biObject[i])
				IF IS_ENTITY_A_TARGET(sFMMCmenu, ET_OBJECT, sFMMCMenu.iSelectedFMCEntity, i)
					CREATE_FMMC_BLIP(sPropStruct.biObject[i], GET_ENTITY_COORDS(sPropStruct.oiObject[i]), HUD_COLOUR_GREEN, "FMMC_B_12", 1, DEFAULT, DEFAULT, RADAR_TRACE_TEMP_4)
				ENDIF
			ELSE
				IF NOT IS_ENTITY_A_TARGET(sFMMCmenu, ET_OBJECT, sFMMCMenu.iSelectedFMCEntity, i)
					REMOVE_BLIP(sPropStruct.biObject[i])
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC TEXT_LABEL_63 GET_ON_SCREEN_ENTITY_INFO_PED_DATA(INT iRow, INT iPage, INT iEntityIndex, INT iMaxRows)
	TEXT_LABEL_63 tl63
	
	iRow = iRow + (iMaxRows*iPage)
	
	SWITCH iRow
		CASE 0
			tl63 = "Model: "
			tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Ped.Peds[iEntityIndex].model)
		BREAK
		
		CASE 1 
			tl63 = "iGroup: "
			tl63 += sMissionPlacementData.Ped.Peds[iEntityIndex].iGroup
		BREAK
		
		CASE 2
			IF sMissionPlacementData.Ped.Peds[iEntityIndex].iInterior != (-1)
				tl63 = "Interior: "
				tl63 += GET_FMC_MISSION_INTERIOR_NAME(sMissionPlacementData.Interior.eInteriors[sMissionPlacementData.Ped.Peds[iEntityIndex].iInterior].eType)
			ELSE
				tl63 = "Interior: None"
			ENDIF
		BREAK
		
		CASE 3
			tl63 = "Position: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Ped.Peds[iEntityIndex].vCoords.x, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Ped.Peds[iEntityIndex].vCoords.y, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Ped.Peds[iEntityIndex].vCoords.z, 1)
		BREAK
		
		CASE 4
			tl63 = "Heading: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Ped.Peds[iEntityIndex].fHeading, 1)
		BREAK
		
		CASE 5
			IF sMissionPlacementData.Ped.Peds[iEntityIndex].iScenarioIndex != (-1)
				tl63 = "Scenario: "
				tl63 += sMissionPlacementData.Scenario[sMissionPlacementData.Ped.Peds[iEntityIndex].iScenarioIndex].sName
			ELSE
				tl63 = "Scenario: None"
			ENDIF
		BREAK

		CASE 6
			IF sMissionPlacementData.Ped.Peds[iEntityIndex].weapon != WEAPONTYPE_INVALID
				tl63 = "Weapon: "
				tl63 += GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_WEAPON_NAME(sMissionPlacementData.Ped.Peds[iEntityIndex].weapon))
			ELSE
				tl63 = "Weapon: None"
			ENDIF
		BREAK
		
		CASE 7
			IF sMissionPlacementData.Ped.Peds[iEntityIndex].iVehicle != (-1)
				tl63 = "Vehicle: "
				tl63 += sMissionPlacementData.Ped.Peds[iEntityIndex].iVehicle
				tl63 += " - "
				tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Vehicle.Vehicles[iEntityIndex].model)
			ELSE
				tl63 = "Vehicle: None"
			ENDIF
		BREAK
		
		CASE 8
			IF sMissionPlacementData.Ped.Peds[iEntityIndex].iVehicle != (-1)
				tl63 = "    Seat: "
				tl63 += GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_TEXT_LABEL_FOR_VEHICLE_SEAT(sMissionPlacementData.Ped.Peds[iEntityIndex].eSeat))
			ELSE
				tl63 = "    Seat: N/A"
			ENDIF
		BREAK
		
		CASE 9
			SWITCH sMissionPlacementData.Ped.Peds[iEntityIndex].eMovement
				CASE CM_STATIONARY			tl63 = "Movement Type: CM_STATIONARY"			BREAK
				CASE CM_DEFENSIVE			tl63 = "Movement Type: CM_DEFENSIVE"			BREAK
				CASE CM_WILLADVANCE			tl63 = "Movement Type: CM_WILLADVANCE"			BREAK
				CASE CM_WILLRETREAT			tl63 = "Movement Type: CM_WILLRETREAT"			BREAK
			ENDSWITCH
		BREAK
		
		CASE 10
			tl63 = "Attached Prop: None"
			
			INT iAttached
			REPEAT MAX_NUM_ATTACH_PROPS iAttached
				IF sMissionPlacementData.Prop.AttachedProp[iAttached].eParentType = ET_PED
				AND sMissionPlacementData.Prop.AttachedProp[iAttached].iParentIndex = iEntityIndex
				AND sMissionPlacementData.Prop.AttachedProp[iAttached].iPropIndex != (-1)	
					tl63 = "Attached Prop: "
					tl63 += sMissionPlacementData.Prop.AttachedProp[iAttached].iPropIndex
					tl63 += " - "
					tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Prop.Props[sMissionPlacementData.Prop.AttachedProp[iAttached].iPropIndex].model)
					BREAKLOOP
				ENDIF
			ENDREPEAT
		BREAK
		
		CASE 11
			IF sMissionPlacementData.Ped.Peds[iEntityIndex].fDefensiveRadius >= cfFMC_MINIMUM_DEFENSIVE_SPHERE
				tl63 = "Defensive Sphere: "
				tl63 += FLOAT_TO_STRING(sMissionPlacementData.Ped.Peds[iEntityIndex].fDefensiveRadius)
			ELSE
				tl63 = "Defensive Sphere: N/A"
			ENDIF
		BREAK
		
		CASE 12
			tl63 = "Hearing Range: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Ped.Peds[iEntityIndex].fHearingRange)
		BREAK
		
		CASE 13
			tl63 = "Seeing Range: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Ped.Peds[iEntityIndex].fSeeingRange)
		BREAK
		
		CASE 14
			tl63 = "Seeing Angle: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Ped.Peds[iEntityIndex].fSeeingAngle)
		BREAK
		
		CASE 15
			IF sMissionPlacementData.Ped.Peds[iEntityIndex].iAmbientSpeechIndex != (-1)
				tl63 = "Ambient Speech: "
				tl63 += sMissionPlacementData.Ped.AmbientSpeech[sMissionPlacementData.Ped.Peds[iEntityIndex].iAmbientSpeechIndex].iVoiceHash
			ELSE
				tl63 = "Ambient Speeche: N/A"
			ENDIF
		BREAK
		
		CASE 16
			IF sMissionPlacementData.Ped.Peds[iEntityIndex].iAnimIndex != (-1)
				INT iDict
				iDict = sMissionPlacementData.Animation.Anim[sMissionPlacementData.Ped.Peds[iEntityIndex].iAnimIndex].iDictIndex
				IF iDict != (-1)
					tl63 = "Ambient Speech: "
					tl63 += sMissionPlacementData.Animation.Dict[iDict].sName
				ELSE
					tl63 = "Ambient Speech: N/A"
				ENDIF
			ELSE
				tl63 = "Ambient Speech: N/A"
			ENDIF
		BREAK
		
		CASE 17
			IF sMissionPlacementData.Ped.Peds[iEntityIndex].iMaxReactionDelay != (-1)
				tl63 = "Max Reaction Delay: "
				tl63 += sMissionPlacementData.Ped.Peds[iEntityIndex].iMaxReactionDelay
			ELSE
				tl63 = "Max Reaction Delay: N/A"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN tl63
ENDFUNC

FUNC TEXT_LABEL_63 GET_ON_SCREEN_ENTITY_INFO_VEHICLE_DATA(INT iRow, INT iPage, INT iEntityIndex, INT iMaxRows)
	TEXT_LABEL_63 tl63
	
	iRow = iRow + (iMaxRows*iPage)
	
	SWITCH iRow
		CASE 0
			tl63 = "Model: "
			tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Vehicle.Vehicles[iEntityIndex].model)
		BREAK
		
		CASE 1
			IF sMissionPlacementData.Vehicle.Vehicles[iEntityIndex].iInterior != (-1)
				tl63 = "Interior: "
				tl63 += GET_FMC_MISSION_INTERIOR_NAME(sMissionPlacementData.Interior.eInteriors[sMissionPlacementData.Vehicle.Vehicles[iEntityIndex].iInterior].eType)
			ELSE
				tl63 = "Interior: None"
			ENDIF
		BREAK
		
		CASE 2
			tl63 = "Position: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Vehicle.Vehicles[iEntityIndex].vCoords.x, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Vehicle.Vehicles[iEntityIndex].vCoords.y, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Vehicle.Vehicles[iEntityIndex].vCoords.z, 1)
		BREAK
		
		CASE 3
			tl63 = "Heading: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Vehicle.Vehicles[iEntityIndex].fHeading, 1)
		BREAK
		
		CASE 4
			tl63 = "Colour: "
			tl63 += sMissionPlacementData.Vehicle.Vehicles[iEntityIndex].iColour
		BREAK
		
		CASE 5
			INT iTrailer
			REPEAT MAX_NUM_TRAILERS iTrailer
				IF sMissionPlacementData.Vehicle.Trailer[iTrailer].iTrailerIndex != (-1)
				AND sMissionPlacementData.Vehicle.Trailer[iTrailer].iAttachIndex != (-1)
					IF sMissionPlacementData.Vehicle.Trailer[iTrailer].iTrailerIndex = iEntityIndex
						tl63 = "Attached To Vehicle: "
						tl63 += sMissionPlacementData.Vehicle.Trailer[iTrailer].iAttachIndex
						tl63 += " - "
						tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Vehicle.Vehicles[sMissionPlacementData.Vehicle.Trailer[iTrailer].iAttachIndex].model)
						BREAKLOOP
					ELIF sMissionPlacementData.Vehicle.Trailer[iTrailer].iAttachIndex = iEntityIndex
						tl63 = "Attached To Trailer: "
						tl63 += sMissionPlacementData.Vehicle.Trailer[iTrailer].iTrailerIndex
						tl63 += " - "
						tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Vehicle.Vehicles[sMissionPlacementData.Vehicle.Trailer[iTrailer].iTrailerIndex].model)
						BREAKLOOP
					ELSE
						tl63 = "Vehicle/Trailer Attachment: None"
					ENDIF
				ELSE
					tl63 = "Vehicle/Trailer Attachment: None"
				ENDIF
			ENDREPEAT
		BREAK
		
		CASE 6
			tl63 = "Attached Prop: None"
			
			INT iAttached
			REPEAT MAX_NUM_ATTACH_PROPS iAttached
				IF sMissionPlacementData.Prop.AttachedProp[iAttached].eParentType = ET_VEHICLE
				AND sMissionPlacementData.Prop.AttachedProp[iAttached].iParentIndex = iEntityIndex
				AND sMissionPlacementData.Prop.AttachedProp[iAttached].iPropIndex != (-1)	
					tl63 = "Attached Prop: "
					tl63 += sMissionPlacementData.Prop.AttachedProp[iAttached].iPropIndex
					tl63 += " - "
					tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Prop.Props[sMissionPlacementData.Prop.AttachedProp[iAttached].iPropIndex].model)
					BREAKLOOP
				ENDIF
			ENDREPEAT
		BREAK
		
		CASE 7
			IF sMissionPlacementData.Vehicle.iTrafficReductionVehicle != (-1)
			AND sMissionPlacementData.Vehicle.iTrafficReductionVehicle = iEntityIndex
				tl63 = "Traffic Reduction Around Vehicle: Active"
			ELSE
				tl63 = "Traffic Reduction Around Vehicle: Inactive"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN tl63
ENDFUNC

FUNC TEXT_LABEL_63 GET_ON_SCREEN_ENTITY_INFO_PORTAL_DATA(INT iRow, INT iPage, INT iEntityIndex, INT iMaxRows)
	TEXT_LABEL_63 tl63
	
	iRow = iRow + (iMaxRows*iPage)
	
	SWITCH iRow
		CASE 0
			IF sMissionPlacementData.Portal[iEntityIndex].iLinkedPortal != (-1)
				tl63 = "Warps To Portal: "
				tl63 += sMissionPlacementData.Portal[iEntityIndex].iLinkedPortal
			ELSE
				tl63 = "Warps To Portal: None"
			ENDIF
		BREAK
		
		CASE 1
			IF sMissionPlacementData.Portal[iEntityIndex].iCutscene != (-1)
				tl63 = "Cutscene: "
				tl63 += sMissionPlacementData.Portal[iEntityIndex].iCutscene
			ELSE
				tl63 = "Cutscene: None"
			ENDIF
		BREAK
		
		CASE 2
			tl63 = "Position: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Portal[iEntityIndex].vCoord.x, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Portal[iEntityIndex].vCoord.y, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Portal[iEntityIndex].vCoord.z, 1)
		BREAK
		
		CASE 3
			tl63 = "Heading: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Portal[iEntityIndex].fHeading, 1)
		BREAK
	ENDSWITCH
	
	RETURN tl63
ENDFUNC

FUNC TEXT_LABEL_63 GET_ON_SCREEN_ENTITY_INFO_PROP_DATA(INT iRow, INT iPage, INT iEntityIndex, INT iMaxRows)
	TEXT_LABEL_63 tl63
	
	iRow = iRow + (iMaxRows*iPage)
	
	SWITCH iRow
		CASE 0
			tl63 = "Model: "
			tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Prop.Props[iEntityIndex].model)
		BREAK
		
		CASE 1
			IF sMissionPlacementData.Prop.Props[iEntityIndex].iInterior != (-1)
				tl63 = "Interior: "
				tl63 += GET_FMC_MISSION_INTERIOR_NAME(sMissionPlacementData.Interior.eInteriors[sMissionPlacementData.Prop.Props[iEntityIndex].iInterior].eType)
			ELSE
				tl63 = "Interior: None"
			ENDIF
		BREAK

		CASE 2
			tl63 = "Position: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Prop.Props[iEntityIndex].vCoords.x, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Prop.Props[iEntityIndex].vCoords.y, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Prop.Props[iEntityIndex].vCoords.z, 1)
		BREAK
		
		CASE 3
			tl63 = "Heading: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Prop.Props[iEntityIndex].fHeading, 1)
		BREAK
		
		CASE 4
			tl63 = "Rotation: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Prop.Props[iEntityIndex].vRotation.x, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Prop.Props[iEntityIndex].vRotation.y, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.Prop.Props[iEntityIndex].vRotation.z, 1)
		BREAK
		
		CASE 5
			tl63 = "Attached Prop: None"
			
			INT iAttached
			REPEAT MAX_NUM_ATTACH_PROPS iAttached
				IF sMissionPlacementData.Prop.AttachedProp[iAttached].iParentIndex != (-1)
					IF sMissionPlacementData.Prop.AttachedProp[iAttached].iPropIndex = iEntityIndex	
						SWITCH sMissionPlacementData.Prop.AttachedProp[iAttached].eParentType
							CASE ET_OBJECT
								tl63 = "Attached to Prop: "
								tl63 += sMissionPlacementData.Prop.AttachedProp[iAttached].iParentIndex
								tl63 += " - "
								tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Prop.Props[sMissionPlacementData.Prop.AttachedProp[iAttached].iParentIndex].model)
							BREAK
							
							CASE ET_VEHICLE
								tl63 = "Attached to Vehicle: "
								tl63 += sMissionPlacementData.Prop.AttachedProp[iAttached].iParentIndex
								tl63 += " - "
								tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Vehicle.Vehicles[sMissionPlacementData.Prop.AttachedProp[iAttached].iParentIndex].model)
							BREAK
							
							CASE ET_PED
								tl63 = "Attached to Ped: "
								tl63 += sMissionPlacementData.Prop.AttachedProp[iAttached].iParentIndex
								tl63 += " - "
								tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Ped.Peds[sMissionPlacementData.Prop.AttachedProp[iAttached].iParentIndex].model)
							BREAK
						ENDSWITCH
					ELIF sMissionPlacementData.Prop.AttachedProp[iAttached].iParentIndex = iEntityIndex	
						tl63 = "Attached Prop: "
						tl63 += sMissionPlacementData.Prop.AttachedProp[iAttached].iPropIndex
						tl63 += " - "
						tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Prop.Props[sMissionPlacementData.Prop.AttachedProp[iAttached].iPropIndex].model)
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH
	
	RETURN tl63
ENDFUNC

FUNC TEXT_LABEL_63 GET_ON_SCREEN_ENTITY_INFO_MISSION_ENTITY_DATA(INT iRow, INT iPage, INT iEntityIndex, INT iMaxRows)
	TEXT_LABEL_63 tl63
	
	iRow = iRow + (iMaxRows*iPage)
	
	SWITCH iRow
		CASE 0
			tl63 = "Model: "
			tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].model)
		BREAK
		
		CASE 1
			IF sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].iInterior != (-1)
				tl63 = "Interior: "
				tl63 += GET_FMC_MISSION_INTERIOR_NAME(sMissionPlacementData.Interior.eInteriors[sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].iInterior].eType)
			ELSE
				tl63 = "Interior: None"
			ENDIF
		BREAK
		
		CASE 2
			IF sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].iCarrierVehicle != (-1)
				SWITCH sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].eCarrierType
					CASE ET_VEHICLE
						tl63 = "Carrier Vehicle: "
						tl63 += sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].iCarrierVehicle
						tl63 += " - "
						tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Vehicle.Vehicles[sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].iCarrierVehicle].model)
					BREAK
					
					CASE ET_PED
						tl63 = "Carrier Ped: "
						tl63 += sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].iCarrierVehicle
						tl63 += " - "
						tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Ped.Peds[sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].iCarrierVehicle].model)
					BREAK
					
					CASE ET_OBJECT
						tl63 = "Carrier Prop: "
						tl63 += sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].iCarrierVehicle
						tl63 += " - "
						tl63 += GET_MODEL_NAME_FOR_DEBUG(sMissionPlacementData.Prop.Props[sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].iCarrierVehicle].model)
					BREAK
				ENDSWITCH
			ELSE
				tl63 = "Carrier: None"
			ENDIF
		BREAK

		CASE 3
			tl63 = "Position: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].vCoords.x, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].vCoords.y, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].vCoords.z, 1)
		BREAK
		
		CASE 4
			tl63 = "Heading: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].fHeading, 1)
		BREAK
		
		CASE 5
			tl63 = "Rotation: "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].vRotation.x, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].vRotation.y, 1)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(sMissionPlacementData.MissionEntity.MissionEntities[iEntityIndex].vRotation.z, 1)
		BREAK
	ENDSWITCH
	
	RETURN tl63
ENDFUNC

PROC GET_ENTITY_INFO_WINDOW_X_AND_Y(FLOAT &fX, FLOAT &fY)
	fX = cf_COPY_PASTE_DEBUG_WINDOW_DEFAULT_POS_X + CUSTOM_MENU_W
	fY = cf_COPY_PASTE_DEBUG_WINDOW_DEFAULT_POS_Y
	
	#IF IS_DEBUG_BUILD
	IF g_fCreatorCopyPasteDebugWindowCustomPosX > 0.0
	AND g_fCreatorCopyPasteDebugWindowCustomPosX != cf_COPY_PASTE_DEBUG_WINDOW_DEFAULT_POS_X
		fX = g_fCreatorCopyPasteDebugWindowCustomPosX
	ENDIF
	IF g_fCreatorCopyPasteDebugWindowCustomPosY > 0.0
	AND g_fCreatorCopyPasteDebugWindowCustomPosY != cf_COPY_PASTE_DEBUG_WINDOW_DEFAULT_POS_Y
		fY = g_fCreatorCopyPasteDebugWindowCustomPosY
	ENDIF
	#ENDIF
ENDPROC

PROC GET_ENTITY_INFO_WINDOW_SIZEX_AND_SIZEY(FLOAT &fX, FLOAT &fY, FLOAT &fTextSize, FLOAT &fSpacing, INT iMaxRows = 0)
	fX = 0.2
	fY = 0.0175*(iMaxRows+1)
	
	#IF IS_DEBUG_BUILD
	IF g_fCreatorCopyPasteDebugWindowCustomSize <> 0.0
		fSpacing *= 1.0+(g_fCreatorCopyPasteDebugWindowCustomSize)
		fTextSize *= 1.0+g_fCreatorCopyPasteDebugWindowCustomSize
		fY = 0.0175*(1.0+g_fCreatorCopyPasteDebugWindowCustomSize)
		fY *= (iMaxRows+1)
		
		fX *= 1.0+g_fCreatorCopyPasteDebugWindowCustomSize
	ENDIF
	#ENDIF
ENDPROC

FUNC STRING GET_ENTITY_TAG_FOR_ENTITY_INFO(eTAG_TYPE eType, INT iEntityIndex)
	TEXT_LABEL_23 tl63_Title = " ("
	INT iTag
	REPEAT MAX_NUM_TAGS iTag
		IF sMissionPlacementData.Tags.Tag[iTag].eType = eType
		AND sMissionPlacementData.Tags.Tag[iTag].iID = iEntityIndex
			tl63_Title += sMissionPlacementData.Tags.Tag[iTag].tlName
			tl63_Title += ")"
			RETURN TEXT_LABEL_TO_STRING(tl63_Title)
		ENDIF
	ENDREPEAT
	RETURN ""
ENDFUNC

PROC DRAW_ON_SCREEN_ENTITY_INFO(structFMMC_MENU_ITEMS &sFMMCmenu, INT iEntityIndex, INT iEntityType)
	#IF IS_DEBUG_BUILD 
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_CreatorCopyPasteDisable")
		EXIT
	ENDIF
	#ENDIF
		
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
	SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
	
	INT i = 0 
	INT iMaxRows = ci_NUMBER_OF_ROWS_COPY_FROM_DEBUG
	INT iPagesMax = 4
	FLOAT fX, fY
	FLOAT fYSpacing = 0.0175
	FLOAT fTextSize = 0.245
	TEXT_LABEL_31 tl63_Title
	TEXT_LABEL_31 tl63_ExtraInfo[ci_NUMBER_OF_ROWS_COPY_FROM_DEBUG_ABSOLUTE_MAX]
	TEXT_LABEL_31 tl63_Page
	TEXT_LABEL_31 tl63_Tag
	TEXT_LABEL_31 tl63_Temp
	TEXT_LABEL_63 tl63_Controls_1
	TEXT_LABEL_63 tl63_Controls_2
	TEXT_LABEL_63 tl63_Controls_3
	
	#IF IS_DEBUG_BUILD
		IF g_iCreatorCopyPasteDebugWindowRowsOverride > 0
			iMaxRows = g_iCreatorCopyPasteDebugWindowRowsOverride
		ENDIF
	#ENDIF
	
	tl63_Tag = "Entity Info"
	IF sFMMCmenu.bCopyPastePreviewWindowOverEntity
		tl63_Tag += " Over Entity"
	ENDIF
	IF sFMMCmenu.bPressedCopyPastePreviewWindow	
		tl63_Tag += " List Mode"
	ENDIF
	tl63_Page = "Page: "
	tl63_Page += (sFMMCmenu.iCopyFromEntityDebugPage+1)
	tl63_Page += "/"
	
	IF iEntityType = CREATION_TYPE_GOTO_LOC
	
	ELIF iEntityType = CREATION_TYPE_PEDS
		tl63_Title = "Ped: "
		tl63_Title += iEntityIndex
		tl63_Title += GET_ENTITY_TAG_FOR_ENTITY_INFO(eTAGTYPE_PED, iEntityIndex)
		
		// Loop through all data to ascertain max page number.
		FOR i = 0 TO ci_NUMBER_OF_COPY_FROM_DEBUG_EXPOSED_VARIABLES-1
			tl63_Temp = GET_ON_SCREEN_ENTITY_INFO_PED_DATA(i, 0, iEntityIndex, ci_NUMBER_OF_COPY_FROM_DEBUG_EXPOSED_VARIABLES)
			IF IS_STRING_NULL_OR_EMPTY(tl63_Temp) 
				iPagesMax = FLOOR(TO_FLOAT(i)/TO_FLOAT(iMaxRows))
				BREAKLOOP
			ENDIF
		ENDFOR
		// Assign data to the current Page.
		FOR i = 0 TO iMaxRows-1
			tl63_ExtraInfo[i] = GET_ON_SCREEN_ENTITY_INFO_PED_DATA(i, sFMMCmenu.iCopyFromEntityDebugPage, iEntityIndex, iMaxRows)
		ENDFOR
	ELIF iEntityType = CREATION_TYPE_VEHICLES
		tl63_Title = "Vehicle: "
		tl63_Title += iEntityIndex
		tl63_Title += GET_ENTITY_TAG_FOR_ENTITY_INFO(eTAGTYPE_VEHICLE, iEntityIndex)
		
		// Loop through all data to ascertain max page number.
		FOR i = 0 TO ci_NUMBER_OF_COPY_FROM_DEBUG_EXPOSED_VARIABLES-1
			tl63_Temp = GET_ON_SCREEN_ENTITY_INFO_VEHICLE_DATA(i, 0, iEntityIndex, ci_NUMBER_OF_COPY_FROM_DEBUG_EXPOSED_VARIABLES)
			IF IS_STRING_NULL_OR_EMPTY(tl63_Temp) 
				iPagesMax = FLOOR(TO_FLOAT(i)/TO_FLOAT(iMaxRows))
				BREAKLOOP
			ENDIF
		ENDFOR
		// Assign data to the current Page.
		FOR i = 0 TO iMaxRows-1
			tl63_ExtraInfo[i] = GET_ON_SCREEN_ENTITY_INFO_VEHICLE_DATA(i, sFMMCmenu.iCopyFromEntityDebugPage, iEntityIndex, iMaxRows)
		ENDFOR
	ELIF iEntityType = CREATION_TYPE_OBJECTS
		tl63_Title = "Mission Entity: "
		tl63_Title += iEntityIndex
		tl63_Title += GET_ENTITY_TAG_FOR_ENTITY_INFO(eTAGTYPE_MISSION_ENTITY, iEntityIndex)
		
		// Loop through all data to ascertain max page number.
		FOR i = 0 TO ci_NUMBER_OF_COPY_FROM_DEBUG_EXPOSED_VARIABLES-1
			tl63_Temp = GET_ON_SCREEN_ENTITY_INFO_MISSION_ENTITY_DATA(i, 0, iEntityIndex, ci_NUMBER_OF_COPY_FROM_DEBUG_EXPOSED_VARIABLES)
			IF IS_STRING_NULL_OR_EMPTY(tl63_Temp) 
				iPagesMax = FLOOR(TO_FLOAT(i)/TO_FLOAT(iMaxRows))
				BREAKLOOP
			ENDIF
		ENDFOR
		// Assign data to the current Page.
		FOR i = 0 TO iMaxRows-1
			tl63_ExtraInfo[i] = GET_ON_SCREEN_ENTITY_INFO_MISSION_ENTITY_DATA(i, sFMMCmenu.iCopyFromEntityDebugPage, iEntityIndex, iMaxRows)
		ENDFOR
	ELIF iEntityType = CREATION_TYPE_PROPS
		tl63_Title = "Prop: "
		tl63_Title += iEntityIndex
		tl63_Title += GET_ENTITY_TAG_FOR_ENTITY_INFO(eTAGTYPE_PROP, iEntityIndex)
		
		// Loop through all data to ascertain max page number.
		FOR i = 0 TO ci_NUMBER_OF_COPY_FROM_DEBUG_EXPOSED_VARIABLES-1
			tl63_Temp = GET_ON_SCREEN_ENTITY_INFO_PROP_DATA(i, 0, iEntityIndex, ci_NUMBER_OF_COPY_FROM_DEBUG_EXPOSED_VARIABLES)
			IF IS_STRING_NULL_OR_EMPTY(tl63_Temp) 
				iPagesMax = FLOOR(TO_FLOAT(i)/TO_FLOAT(iMaxRows))
				BREAKLOOP
			ENDIF
		ENDFOR
		// Assign data to the current Page.
		FOR i = 0 TO iMaxRows-1
			tl63_ExtraInfo[i] = GET_ON_SCREEN_ENTITY_INFO_PROP_DATA(i, sFMMCmenu.iCopyFromEntityDebugPage, iEntityIndex, iMaxRows)
		ENDFOR
	
	ELIF iEntityType = CREATION_TYPE_WARP_PORTALS
		tl63_Title = "Portal: "
		tl63_Title += iEntityIndex
		
		// Loop through all data to ascertain max page number.
		FOR i = 0 TO ci_NUMBER_OF_COPY_FROM_DEBUG_EXPOSED_VARIABLES-1
			tl63_Temp = GET_ON_SCREEN_ENTITY_INFO_PORTAL_DATA(i, 0, iEntityIndex, ci_NUMBER_OF_COPY_FROM_DEBUG_EXPOSED_VARIABLES)
			IF IS_STRING_NULL_OR_EMPTY(tl63_Temp) 
				iPagesMax = FLOOR(TO_FLOAT(i)/TO_FLOAT(iMaxRows))
				BREAKLOOP
			ENDIF
		ENDFOR
		// Assign data to the current Page.
		FOR i = 0 TO iMaxRows-1
			tl63_ExtraInfo[i] = GET_ON_SCREEN_ENTITY_INFO_PORTAL_DATA(i, sFMMCmenu.iCopyFromEntityDebugPage, iEntityIndex, iMaxRows)
		ENDFOR
	ELIF iEntityType = CREATION_TYPE_CHECKPOINTS_LIST
		tl63_Title = "Checkpoint: "
		tl63_Title += iEntityIndex
	ENDIF
	
	BOOL bBlockRBInputs = TRUE
	#IF IS_DEBUG_BUILD 
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_CreatorAllowCopyPasteRotationButtonConflict")
		bBlockRBInputs = FALSE
	ENDIF
	#ENDIF
	
	IF bBlockRBInputs
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RB, TRUE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LB, TRUE)
	ENDIF
	
	BOOL bNextPage
	BOOL bPrevPage
	
	IF sFMMCmenu.bPressedCopyPastePreviewWindow
	OR sFMMCmenu.bCopyPastePreviewWindowOverEntity
		IF NOT sFMMCmenu.bCopyPastePreviewWindowOverEntity
			tl63_Controls_1 = "Left/Right Arrow Keys to change Page"
			tl63_Controls_2 = "Up/Down Arrow Keys to change Entity"
			tl63_Controls_3 = "Numpad 1 to teleport to Entity"
		ENDIF
					
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_RIGHT, KEYBOARD_MODIFIER_ALT, "NextvDbgWnd2")
			bNextPage = TRUE
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_LEFT, KEYBOARD_MODIFIER_ALT, "NextvDbgWnd2")
			bPrevPage = TRUE
		ENDIF
	ELSE
		tl63_Controls_1 = "LB and RB to change Page"
		tl63_Controls_2 = "Hold Fast Zoom + LB/RB to cycle entities"
		
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		AND NOT (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_RT_BUTTON())	AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_LT_BUTTON()))	
			bNextPage = TRUE
		ENDIF
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		AND NOT (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_RT_BUTTON())	AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_LT_BUTTON()))
			bPrevPage = TRUE
		ENDIF
	ENDIF
	
	IF bNextPage
		sFMMCmenu.iCopyFromEntityDebugPage++
		IF sFMMCmenu.iCopyFromEntityDebugPage > iPagesMax
			sFMMCmenu.iCopyFromEntityDebugPage = 0
		ENDIF
	ENDIF
	IF bPrevPage
		sFMMCmenu.iCopyFromEntityDebugPage--
		IF sFMMCmenu.iCopyFromEntityDebugPage < 0
			sFMMCmenu.iCopyFromEntityDebugPage = iPagesMax
		ENDIF
	ENDIF
	
	tl63_Page += (iPagesMax+1)
	
	FLOAT fRectSizeX, fRectSizeY
	
	GET_ENTITY_INFO_WINDOW_X_AND_Y(fX, fY)
	GET_ENTITY_INFO_WINDOW_SIZEX_AND_SIZEY(fRectSizeX, fRectSizeY, fTextSize, fYSpacing, iMaxRows)
		
	FLOAT fRectPosX = fX+(fRectSizeX/2)
	FLOAT fRectPosY = fY+(fRectSizeY/2)
	FLOAT fTextPosX = fRectPosX-(fRectSizeX/2)
	FLOAT fTextPosY = fRectPosY-(fRectSizeY/2)
	
	DRAW_SPRITE("CommonMenu", "Gradient_Bgd", fRectPosX, fRectPosY, fRectSizeX*1.01, fRectSizeY*1.01, 0.0, 0, 0, 0, 255)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63_Tag)
		SET_TEXT_SCALE(fTextSize+0.03, fTextSize+0.03)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_DROPSHADOW(6, 0, 0, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fTextPosX, fTextPosY-0.027, "STRING", tl63_Tag)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63_Page)
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(50, 255, 50, 255)
		SET_TEXT_DROPSHADOW(2, 0, 0, 0, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fTextPosX+fRectSizeX/1.4, fTextPosY, "STRING", tl63_Page)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63_Title)
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(0, 255, 50, 255)
		SET_TEXT_DROPSHADOW(2, 0, 0, 0, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fTextPosX, fTextPosY, "STRING", tl63_Title)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63_Controls_1)
		DRAW_RECT(fRectPosX, fTextPosY+fYSpacing*0.5+(fYSpacing*(iMaxRows+2)), fRectSizeX, (fTextSize*0.2)*0.33, 0, 0, 0, 175)
		SET_TEXT_SCALE(fTextSize*0.75, fTextSize*0.75)
		SET_TEXT_COLOUR(175, 225, 175, 255)
		SET_TEXT_DROPSHADOW(2, 0, 0, 0, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fTextPosX, fTextPosY+(fYSpacing*(iMaxRows+2)), "STRING", tl63_Controls_1)
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63_Controls_2)
		DRAW_RECT(fRectPosX, fTextPosY+fYSpacing*0.5+(fYSpacing*(iMaxRows+3)), fRectSizeX, (fTextSize*0.2)*0.33, 0, 0, 0, 175)
		SET_TEXT_SCALE(fTextSize*0.75, fTextSize*0.75)
		SET_TEXT_COLOUR(175, 225, 175, 255)
		SET_TEXT_DROPSHADOW(2, 0, 0, 0, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fTextPosX, fTextPosY+(fYSpacing*(iMaxRows+3)), "STRING", tl63_Controls_2)
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63_Controls_3)
		DRAW_RECT(fRectPosX, fTextPosY+fYSpacing*0.5+(fYSpacing*(iMaxRows+4)), fRectSizeX, (fTextSize*0.2)*0.33, 0, 0, 0, 175)
		SET_TEXT_SCALE(fTextSize*0.75, fTextSize*0.75)
		SET_TEXT_COLOUR(175, 225, 175, 255)
		SET_TEXT_DROPSHADOW(2, 0, 0, 0, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fTextPosX, fTextPosY+(fYSpacing*(iMaxRows+4)), "STRING", tl63_Controls_3)
	ENDIF
	
	FOR i = 0 TO iMaxRows-1
		IF NOT IS_STRING_NULL_OR_EMPTY(tl63_ExtraInfo[i])
			SET_TEXT_SCALE(fTextSize, fTextSize)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_DROPSHADOW(2, 0, 0, 0, 200)
			DISPLAY_TEXT_WITH_LITERAL_STRING(fTextPosX, fTextPosY+(fYSpacing*(i+1)), "STRING", tl63_ExtraInfo[i])
		ENDIF
	ENDFOR	
ENDPROC

PROC CLEAR_PED_VISION_CONE_BLIPS()
	INT i
	REPEAT MAX_NUM_PEDS i
		IF DOES_BLIP_EXIST(sPedStruct.biConeBlip[i])
			REMOVE_BLIP(sPedStruct.biConeBlip[i])
		ENDIF		
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_DRAW_VISION_CONES(structFMMC_MENU_ITEMS &sFMMCMenu)

	IF sFMMCmenu.sActiveMenu != eFMMC_FMC_PED_MENU
		RETURN FALSE
	ENDIF
	
	IF sFMMCMenu.iCopyFromEntity < 0
	OR sFMMCMenu.iCopyFromEntity > MAX_NUM_PEDS -1
		RETURN FALSE
	ENDIF
	
	IF NOT IS_LONG_BIT_SET(sMissionPlacementData.Ped.Peds[sFMMCMenu.iCopyFromEntity].iBitset, ENUM_TO_INT(ePEDDATABITSET_CHECK_VISION))
		RETURN FALSE
	ENDIF
	
	RETURN IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)

ENDFUNC

PROC FMC_REMOVE_PED_BLIPS()
	INT i
	FOR i = 0 TO (FMMC_MAX_PEDS - 1)
		IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])
			REMOVE_BLIP(sPedStruct.biPedBlip[i])
		ENDIF
	ENDFOR
ENDPROC

PROC FMC_REMOVE_CUSTOM_SPAWNS_BLIPS()
	INT i 
	FOR i = 0 TO (MAX_NUM_CUSTOM_SPAWNS -1)
		IF DOES_BLIP_EXIST(sCustomSpawns.biBlips[i])
			REMOVE_BLIP(sCustomSpawns.biBlips[i])
		ENDIF
	ENDFOR
ENDPROC

PROC FMC_REMOVE_PROP_BLIPS()
	
	INT i 
	
	FOR i = 0 TO (MAX_NUM_PROPS - 1)
		IF DOES_BLIP_EXIST(sPropStruct.biObject[i])
			REMOVE_BLIP(sPropStruct.biObject[i])
		ENDIF
	ENDFOR

ENDPROC

PROC FMC_REMOVE_VEHICLE_BLIPS()
	
	INT i 
	
	FOR i = 0 TO (MAX_NUM_VEHICLES - 1)
		IF DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[i])
			REMOVE_BLIP(sVehStruct.biVehicleBlip[i])
		ENDIF
	ENDFOR
	
ENDPROC

PROC FMC_HIDE_BLIPS_FOR_VISION_CONE_DISPLAY()
	FMC_REMOVE_PED_BLIPS()
	FMC_REMOVE_CUSTOM_SPAWNS_BLIPS()
	FMC_REMOVE_PROP_BLIPS()
	FMC_REMOVE_VEHICLE_BLIPS()
ENDPROC

PROC DRAW_PED_VISION_CONES(structFMMC_MENU_ITEMS &sFMMCMenu)
	
	IF NOT SHOULD_DRAW_VISION_CONES(sFMMCMenu)
	
		IF sCycleCamStruct.BlockBlipsState != eBLOCK_BLIPS_NONE	
			CLEAR_PED_VISION_CONE_BLIPS()
			sCycleCamStruct.BlockBlipsState = eBLOCK_BLIPS_NONE
		ENDIF
	
		EXIT
	ENDIF
	
	sCycleCamStruct.BlockBlipsState = eBLOCK_BLIPS_ALL

	INT iHudColour = ENUM_TO_INT(HUD_COLOUR_RED)
	FLOAT fAngle = sMissionPlacementData.Ped.Peds[sFMMCMenu.iCopyFromEntity].fSeeingAngle
	FLOAT fRadius = sMissionPlacementData.Ped.Peds[sFMMCMenu.iCopyFromEntity].fSeeingRange
//	FLOAT fHeading = sMissionPlacementData.Ped.Peds[sFMMCMenu.iCopyFromEntity].fHeading

	IF NOT DOES_BLIP_EXIST(sPedStruct.biConeBlip[sFMMCMenu.iCopyFromEntity])
		FMC_HIDE_BLIPS_FOR_VISION_CONE_DISPLAY()
//		CLEAR_FAKE_CONE_ARRAY()
		CLEAR_PED_VISION_CONE_BLIPS()
		SET_PED_SEEING_RANGE(sPedStruct.piPed[sFMMCMenu.iCopyFromEntity], fRadius)
		SET_PED_VISUAL_FIELD_CENTER_ANGLE(sPedStruct.piPed[sFMMCMenu.iCopyFromEntity], fAngle)
		sPedStruct.biConeBlip[sFMMCMenu.iCopyFromEntity] = CREATE_BLIP_FOR_ENTITY(sPedStruct.piPed[sFMMCMenu.iCopyFromEntity], TRUE) 
		PRINTLN("[DRAW_PED_VISION_CONES] CREATE_BLIP_FOR_ENTITY ", sFMMCMenu.iCopyFromEntity)
	ELSE
//		PRINTLN("[DRAW_PED_VISION_CONES], fAngle ", fAngle, " fRadius = " , fRadius, " sFMMCMenu.iCopyFromEntity = ", sFMMCMenu.iCopyFromEntity)
//		SETUP_FAKE_CONE_DATA(sPedStruct.biConeBlip[sFMMCMenu.iCopyFromEntity], -1.0, 1.0, fAngle, 1.0, fRadius, DEG_TO_RAD(fHeading + 180.0), TRUE, iHudColour)
		SET_BLIP_SHOW_CONE(sPedStruct.biConeBlip[sFMMCMenu.iCopyFromEntity], TRUE, iHudColour)
	ENDIF

ENDPROC

PROC MAINTAIN_PLACED_FMC_PEDS(CREATION_VARS_STRUCT &sCurrentVarsStruct,
							structFMMC_MENU_ITEMS &sFMMCmenu,
							FMMC_LOCAL_STRUCT &sFMMCdata,
							HUD_COLOURS_STRUCT &sHCS,
							FMMC_COMMON_MENUS_VARS &sFMMCendStage)
							
	//If these are then same value then don't draw the corona
	BOOL bDrawCorona = (sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION AND sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION)
	INT i, iR, iG, iB, iA
	FLOAT fTempx, fTempy
	BOOL bPlaneCorona = TRUE
	INT iMarkerCount
	BOOL bDelete = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_DELETE_BUTTON())
	
	// Loop over all peds to do every-frame logic
	FOR i = 0 TO sMissionPlacementData.Ped.iCount-1
		
		HANDLE_CUSTOM_PED_DELETE_INPUT(sFMMCmenu, bDelete, sMissionPlacementData.ped.peds[i].model)
	
		IF DOES_ENTITY_EXIST(sPedStruct.piPed[i])
			
			DRAW_ENTITY_LABEL(CREATION_TYPE_PEDS, i, VECTOR_ZERO)
			TEXT_LABEL_63 tl63 = "Ped: "
			tl63 += i
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(sPedStruct.piPed[i], tl63, 0.5)
			
			IF NOT IS_PED_INJURED(sPedStruct.piPed[i])
				
				IF IS_ENTITY_IN_CORONA(sPedStruct.piPed[i], sCurrentVarsStruct, 1.0, i, CREATION_TYPE_PEDS, -1, sFMMCmenu.iSelectedTeam)
					IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PEDS
						sFMMCMenu.iCopyFromEntity = i
						DRAW_ON_SCREEN_ENTITY_INFO(sFMMCmenu, sFMMCMenu.iCopyFromEntity, CREATION_TYPE_PEDS)
					ENDIF
				ENDIF
				
				// Check if the ped is being highlighted and needs updated
				IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_PEDS, i, sPedStruct.piPed[i], bPlaneCorona, HUD_COLOUR_RED, sFMMCdata, sFMMCendStage)
					
					// Pickup if Accept is pressed
					IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
						PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_PEDS, i)
						CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
					
					// Delete if delete is pressed
					ELIF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
						DELETE_FMC_PLACED_PED(i)
						sCurrentVarsStruct.iEntityRemoved = i
						CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
						REFRESH_MENU(sFMMCMenu)
						sFMMCMenu.iSelectedEntity = -1
						CLEAR_BIT(iLocalBitSet, biEntityInCorona)
					ENDIF
					bPlaneCorona = TRUE
					
				ELIF bDrawCorona
					IF bPlaneCorona = FALSE
						sCurrentVarsStruct.tl15SelectedName = GET_CREATOR_NAME_FOR_PED_MODEL(sMissionPlacementData.Ped.Peds[i].model)
						PRINT_SELECTED_ENTITY_NAME_AS_FLOATING_HELP(sCurrentVarsStruct, sCurrentVarsStruct.tl15SelectedName)
						bPlaneCorona = TRUE
					ENDIF
					IF iMarkerCount < ciMAX_MARKER_COUNT
					AND GET_SCREEN_COORD_FROM_WORLD_COORD(sMissionPlacementData.Ped.Peds[i].vCoords, fTempx, fTempy)
						GET_HUD_COLOUR(sHCS.hcColour[CREATION_TYPE_PEDS],iR, iG, iB, iA)
					ENDIF
				ENDIF
				
				//  Give it a blip if it has none
				IF sFMMCmenu.iEntityCreation = CREATION_TYPE_TARGETS
					MAINTAIN_TARGET_BLIPS(sFMMCmenu, ET_PED, i)
				ELSE
					IF NOT DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])
					AND sCycleCamStruct.BlockBlipsState = eBLOCK_BLIPS_NONE
						CREATE_FMMC_BLIP(sPedStruct.biPedBlip[i], sMissionPlacementData.Ped.Peds[i].vCoords, HUD_COLOUR_RED, "FMMC_B_9", 1)
					ENDIF
				ENDIF
				
			ENDIF
		ELSE
			// Recreate the ped if it doesn't exist (such as if you pick it up to edit it and then cancel that)
			IF sFMMCmenu.iEntityCreation != CREATION_TYPE_PEDS
			OR sFMMCMenu.iSelectedEntity != i
				CREATE_PED_FMC(sPedStruct.piPed[i], sMissionPlacementData.Ped.Peds[i])
			ENDIF
		ENDIF			
	ENDFOR
	
	IF sFMMCmenu.iPedLibrary = PED_LIBRARY_CUSTOM
	AND sFMMCMenu.iPedType > 0
	AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciPLACE_PED_TYPE
	AND bDelete
		PRINTLN("MAINTAIN_PLACED_FMC_PROPS - Calling REMOVE_CUSTOM_PED: ", (sFMMCMenu.iPedType - 1))
		REMOVE_CUSTOM_PED(sFMMCmenu, sFMMCMenu.iPedType - 1)
	ENDIF
	
ENDPROC



/////////////////////////////////////////////
///    VEHICLES
///    

// Generic function to create a vehicle based on its placement data
PROC CREATE_FMC_VEHICLE(VEHICLE_INDEX &viPassed, MISSION_PLACEMENT_DATA_VEHICLES sVehicle)
	IF sVehicle.model != DUMMY_MODEL_FOR_SCRIPT
	
		PRINTLN("CREATE_FMC_VEHICLE - Creating vehicle")
		viPassed = CREATE_VEHICLE(sVehicle.model, sVehicle.vCoords, sVehicle.fHeading, FALSE, FALSE, TRUE)
		
		SET_ENTITY_COLLISION(viPassed, TRUE)
		SET_CAN_CLIMB_ON_ENTITY(viPassed, FALSE)			
		SET_VEHICLE_ENGINE_ON(viPassed, FALSE, TRUE)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viPassed, FALSE)
		SET_VEHICLE_DIRT_LEVEL(viPassed, 0.0)
		SET_ENTITY_HEALTH(viPassed, 1000)
		SET_ENTITY_MAX_HEALTH(viPassed, 1000)
		SET_VEHICLE_ENGINE_HEALTH(viPassed,1000)
		SET_VEHICLE_PETROL_TANK_HEALTH(viPassed,1000)
		SET_VEHICLE_BODY_HEALTH(viPassed,1000)
		
		IF sVehicle.iColour != -1
			SET_VEHICLE_COLOURS(viPassed, sVehicle.iColour, sVehicle.iColour)
			SET_VEHICLE_EXTRA_COLOURS(viPassed, sVehicle.iColour, sVehicle.iColour)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(viPassed)
			SET_ENTITY_PROOFS(viPassed, TRUE, TRUE, TRUE, TRUE, TRUE)
		ENDIF
		
	ELSE
		PRINTLN("[TMS4657] CREATE_VEHICLE_FMMC - Vehicle is a dummy model")
	ENDIF
ENDPROC

FUNC BOOL CREATE_ALL_FMC_VEHICLES(structFMMC_MENU_ITEMS &sFMMCmenu, INT &iVehicleModelLoadTimers[])

	INT i
	BOOL bDone[FMMC_MAX_VEHICLES]
	
	// Loop over all vehicles and create them
	REPEAT sMissionPlacementData.Vehicle.iCount i
		IF NOT DOES_ENTITY_EXIST(sVehStruct.veVehcile[i])
		
			REQUEST_MODEL(sMissionPlacementData.Vehicle.Vehicles[i].model)
			IF iVehicleModelLoadTimers[i] = 0
				iVehicleModelLoadTimers[i] = GET_GAME_TIMER()
			ENDIF
			IF HAS_MODEL_LOADED(sMissionPlacementData.Vehicle.Vehicles[i].model)
				CREATE_FMC_VEHICLE(sVehStruct.veVehcile[i], sMissionPlacementData.Vehicle.Vehicles[i])
				ADD_MODEL_TO_CREATOR_BUDGET(sMissionPlacementData.Vehicle.Vehicles[i].model)
				FREEZE_ENTITY_POSITION(sVehStruct.veVehcile[i], TRUE)
				
				// Add its custom veh to the list if it uses one
				IF IS_LONG_BIT_SET(sMissionPlacementData.Vehicle.Vehicles[i].iBitset, ENUM_TO_INT(eVEHICLEDATABITSET_CUSTOM_VEHICLE_MODEL))
					INT iCustom
					BOOL bAdd = TRUE
					FOR iCustom = 0 TO ciMAX_CUSTOM_DEV_VEHS-1
						IF sMissionPlacementData.Vehicle.Vehicles[i].model = sFMMCMenu.sCustomVehs.mnCustomVeh[iCustom]
							bAdd = FALSE
							BREAKLOOP
						ENDIF
					ENDFOR
					IF bAdd
						ADD_CUSTOM_VEH(sFMMCmenu, sMissionPlacementData.Vehicle.Vehicles[i].model)
					ENDIF
				ENDIF
				
			ENDIF
				
		ELSE
			// Failsafe in case a vehicle can't be created
			IF GET_GAME_TIMER() - iVehicleModelLoadTimers[i] > 15000
				sMissionPlacementData.Vehicle.Vehicles[i].model = NINEF
				iVehicleModelLoadTimers[i] = GET_GAME_TIMER()
			ENDIF
			IF GET_GAME_TIMER() - iVehicleModelLoadTimers[i] > 30000
				PRINTLN("CREATE_ALL_CREATOR_VEHICLES CRITICAL FAILURE ON CREATING VEHICLE ",i)
			ENDIF
			bDone[i] = TRUE
		ENDIF
		
	ENDREPEAT
	
	// Make sure all vehicles exist before moving on
	REPEAT sMissionPlacementData.Vehicle.iCount i
		IF NOT bDone[i]
			PRINTLN("CREATE_ALL_CREATOR_VEHICLES Failed on vehicle ", i)
			RETURN FALSE
		ENDIF
	ENDREPEAT 
	
	// Unload their models
	REPEAT sMissionPlacementData.Vehicle.iCount i
		SET_MODEL_AS_NO_LONGER_NEEDED(sMissionPlacementData.Vehicle.Vehicles[i].model)
		iVehicleModelLoadTimers[i] = 0
	ENDREPEAT 
	
	RETURN TRUE
	
ENDFUNC

FUNC INT GET_VEH_SELECTION_FROM_CUSTOM_MODEL(structFMMC_MENU_ITEMS &sFMMCmenu, MODEL_NAMES mnPassed)
	INT i
	FOR i = 0 TO sFMMCMenu.sCustomVehs.iCurrentNumberOfDevVehs - 1
		IF sFMMCMenu.sCustomVehs.mnCustomVeh[i] = mnPassed
			RETURN i+1
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

PROC GET_VEHICLE_ENTITY_LIBRARY_AND_SELECTION_FROM_MODEL(structFMMC_MENU_ITEMS &sFMMCmenu, MODEL_NAMES mnVehicle)

	sFMMCmenu.iVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(mnVehicle, TRUE)
	sFMMCmenu.iVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(mnVehicle)
	
	PRINTLN("[CUST_VEH] GET_VEHICLE_ENTITY_LIBRARY_AND_SELECTION_FROM_MODEL, iVehicleLibrary = ", sFMMCmenu.iVehicleLibrary)
	PRINTLN("[CUST_VEH] GET_VEHICLE_ENTITY_LIBRARY_AND_SELECTION_FROM_MODEL, iVehicleType = ", sFMMCmenu.iVehicleType)
	
	// If it's not in there update it to go into the custom library
	IF sFMMCmenu.iVehicleLibrary = -1
		sFMMCmenu.iVehicleType = GET_VEH_SELECTION_FROM_CUSTOM_MODEL(sFMMCmenu, mnVehicle)
		IF sFMMCmenu.iVehicleType > -1
			sFMMCmenu.iVehicleLibrary = VEHICLE_LIBRARY_CUSTOM
		ELIF sFMMCMenu.sCustomVehs.iCurrentNumberOfDevVehs < ciMAX_CUSTOM_DEV_VEHS
			ADD_CUSTOM_VEH(sFMMCmenu, mnVehicle)
			sFMMCmenu.iVehicleType = sFMMCMenu.sCustomVehs.iCurrentNumberOfDevVehs
			sFMMCmenu.iVehicleLibrary = VEHICLE_LIBRARY_CUSTOM
		ENDIF
	ENDIF
ENDPROC

PROC DO_FMC_VEHICLE_CREATION(CREATION_VARS_STRUCT 		&sCurrentVarsStruct, 
							HUD_COLOURS_STRUCT 			&sHCS, 
							structFMMC_MENU_ITEMS 		&sFMMCmenu, 
							FMMC_LOCAL_STRUCT 			&sFMMCData,
							INT 						iMaximum = MAX_NUM_VEHICLES)
							
	INT iVehicleNumber
	MODEL_NAMES mnCreationModel = GET_VEHICLE_MODEL_FROM_MENU_SELECTION(sFMMCmenu)
	
//	PRINTLN("[CUST_VEH] DO_FMC_VEHICLE_CREATION, mnCreationModel = ", GET_MODEL_NAME_FOR_DEBUG(mnCreationModel))
//	PRINTLN("[CUST_VEH] DO_FMC_VEHICLE_CREATION, sFMMCMenu.iVehicleLibrary = ", sFMMCMenu.iVehicleLibrary)
//	PRINTLN("[CUST_VEH] DO_FMC_VEHICLE_CREATION, sFMMCMenu.iVehicleType = ", sFMMCMenu.iVehicleType)
//	PRINTLN("[CUST_VEH] DO_FMC_VEHICLE_CREATION, sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)
//	PRINTLN("[CUST_VEH] DO_FMC_VEHICLE_CREATION, sVehStruct.iSwitchingINT = ", sVehStruct.iSwitchingINT)
	
	// Get the number of the vehicle based on if it's new or picked up
	IF sFMMCMenu.iSelectedEntity = -1
	
		iVehicleNumber = sMissionPlacementData.Vehicle.iCount
	ELSE
		
		iVehicleNumber = sFMMCMenu.iSelectedEntity
		
		IF iVehicleNumber > iMaximum 
			iVehicleNumber = iMaximum - 1
			sFMMCMenu.iSelectedEntity = iVehicleNumber
		ENDIF
		
		// Remove the existing vehicle if it already exists and update the creation data to the selected vehicles data
		IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[iVehicleNumber])
			
			IF DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[iVehicleNumber])
				REMOVE_BLIP(sVehStruct.biVehicleBlip[iVehicleNumber])
			ENDIF				
			IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(sVehStruct.viCoronaVeh))	
				DELETE_VEHICLE(sVehStruct.viCoronaVeh)
			ENDIF
			IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[iVehicleNumber])
				DELETE_VEHICLE(sVehStruct.veVehcile[iVehicleNumber])
			ENDIF
			
			sEditedVehPlacementData = sMissionPlacementData.Vehicle.Vehicles[iVehicleNumber]
			
			mnCreationModel = sEditedVehPlacementData.model
			GET_VEHICLE_ENTITY_LIBRARY_AND_SELECTION_FROM_MODEL(sFMMCmenu, mnCreationModel)
			
			sVehStruct.iSelectedVehicleColour = GET_COLOUR_SELECTION_FROM_VEHICLE_COLOUR(sEditedVehPlacementData.iColour)
			sVehStruct.iSelectedVehicleColourOld = sVehStruct.iSelectedVehicleColour
			
			sCurrentVarsStruct.fCreationHeading = sEditedVehPlacementData.fHeading
			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
			
			IF IS_MODEL_VALID(sEditedVehPlacementData.model)
				REQUEST_MODEL(sEditedVehPlacementData.model)
			ENDIF
		ENDIF
		
	ENDIF
	
	// Too many Vehs
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		AND sFMMCMenu.iSelectedEntity = -1
			STRING sMax = "FMMC_ER_TV"
			DO_TOO_MANY_ENTITY_ERROR(sCurrentVarsStruct, sVehStruct.flashColour, sMax)
		ENDIF
	ENDIF
	TEXT_LABEL_15 tlVehType = "FMMC_AB_02"
	IF sMissionPlacementData.Vehicle.iCount >= iMaximum	
	AND iVehicleNumber >= iMaximum
		sVehStruct.iSwitchingINT = CREATION_STAGE_WAIT
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)		
		IF sVehStruct.flashColour = HUDFLASHING_NONE
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.Vehicle.iCount,	iMaximum, tlVehType, -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sVehStruct.flashColour, 0)
		ELSE
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.Vehicle.iCount,	iMaximum, tlVehType, -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sVehStruct.flashColour, 500)
		ENDIF
	ELSE
		sVehStruct.flashColour = HUDFLASHING_NONE
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.Vehicle.iCount,	iMaximum, tlVehType, -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sVehStruct.flashColour, 0)
	ENDIF
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
		sHCS.hcCurrentCoronaColour = sHCS.hcColourCantPlace
		EXIT
	ENDIF
	HANDLE_MODEL_MEMORY_METER()
	
	// The switch case
	SWITCH sVehStruct.iSwitchingINT
	
		// Remove any old vehicle before creating a new one. Make sure there aren't too many
		CASE CREATION_STAGE_WAIT
			IF sMissionPlacementData.Vehicle.iCount <= MAX_NUM_VEHICLES
			AND iVehicleNumber < MAX_NUM_VEHICLES
				sVehStruct.iSwitchingINT = CREATION_STAGE_SET_UP
				
				IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
					DELETE_VEHICLE(sVehStruct.viCoronaVeh)
				ENDIF
			ENDIF
		BREAK
		
		// Request model data
		CASE CREATION_STAGE_SET_UP
			
			IF NOT DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)				
				IF IS_MODEL_VALID(mnCreationModel)
					REQUEST_MODEL(mnCreationModel)
					sEditedVehPlacementData.vCoords = sCurrentVarsStruct.vCoronaPos
					sEditedVehPlacementData.fHeading = sCurrentVarsStruct.fCreationHeading
					sEditedVehPlacementData.model = mnCreationModel					
					sVehStruct.iSwitchingINT = CREATION_STAGE_MAKE
					PRINTLN("DO_FMC_VEHICLE_CREATION, CREATION_STAGE_SET_UP, mnCreationModel = ", GET_MODEL_NAME_FOR_DEBUG(mnCreationModel))
				ELSE
					PRINTLN("DO_FMC_VEHICLE_CREATION, CREATION_STAGE_SET_UP, mnCreationModel INVALID ")
				ENDIF
			ELSE
				sVehStruct.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
			
		BREAK
		
		// Create the new placement vehicle
		CASE CREATION_STAGE_MAKE
		
			IF NOT DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
				REQUEST_MODEL(mnCreationModel)
				IF HAS_MODEL_LOADED(mnCreationModel)
					CREATE_FMC_VEHICLE(sVehStruct.viCoronaVeh, sEditedVehPlacementData)
					
					PRINTLN("DO_FMC_VEHICLE_CREATION, CREATION_STAGE_MAKE, CREATE_FMC_VEHICLE ")
					
					SET_ENTITY_HEADING(sVehStruct.viCoronaVeh, sCurrentVarsStruct.fCreationHeading)
					
					sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
					sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL
					SET_ENTITY_COLLISION(sVehStruct.viCoronaVeh, FALSE)
					SET_ENTITY_ALPHA(sVehStruct.viCoronaVeh, 0, FALSE)
					SET_MODEL_AS_NO_LONGER_NEEDED(mnCreationModel)
					
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(sVehStruct.viCoronaVeh)
				sVehStruct.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
			
			REFRESH_MENU(sFMMCmenu)
		BREAK
		
		// Do the placement of the veh, updating it's position/rotation/model/etc based on user input
		CASE CREATION_STAGE_PLACE
		
			IF IS_ENTITY_ALIVE(sVehStruct.viCoronaVeh)
				SET_UP_MODEL_MEMORY_METER(GET_ENTITY_MODEL(sVehStruct.viCoronaVeh), sCurrentVarsStruct)
			ENDIF
			
			sCurrentVarsStruct.fCheckPointSize = ciVEHICLE_CHECK_POINT_SIZE
			
			sEditedVehPlacementData.vCoords = sCurrentVarsStruct.vCoronaPos
			sEditedVehPlacementData.fHeading = sCurrentVarsStruct.fCreationHeading
			sEditedVehPlacementData.model = mnCreationModel
			
			IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
			AND IS_VEHICLE_DRIVEABLE(sVehStruct.viCoronaVeh)
			
				BOOL bSelectedEntity
				bSelectedEntity = IS_BIT_SET(iLocalBitset, biCanEditEntityInCorona)
				VEHICLE_INDEX viAttachedTruck
				
				// Handle the override placement where you can input the exact position/rotation in the menus
				HANDLE_ENTITY_CREATION_OVERRIDES(sFMMCMenu, sFMMCData, sCurrentVarsStruct, sVehStruct.viCoronaVeh, sEditedGenericData.iBitset, ENUM_TO_INT(eGENERICDATABITSET_OVERRIDE_POSITION), ENUM_TO_INT(eGENERICDATABITSET_OVERRIDE_ROTATION), FALSE, FALSE, FALSE)
			
				// Has to pass this in as MOVE_LAST_PLACED_VEHICLE is a Mission Creator function that freemode is sharing.			
				TRAIN_CREATION_STRUCT sTrainBlank
			
				// Update the vehicle's position/rotation and check the area is safe to place in
				IF MOVE_LAST_PLACED_VEHICLE(sFMMCmenu, 
									sVehStruct.viCoronaVeh, 
									sVehStruct,
									sObjStruct,
									sTrainBlank,
									sEditedVehPlacementData.vCoords,
									sEditedVehPlacementData.fHeading,
									sCurrentVarsStruct,
									sFMMCData, <<0,0,0>>,
									bSelectedEntity,
									IS_BIT_SET(iLocalBitset, biDLCLocked),
									viAttachedTruck)
									
					IF sFMMCMenu.iVehicleLibrary = VEHICLE_LIBRARY_CUSTOM
						SET_LONG_BIT(sMissionPlacementData.Vehicle.Vehicles[iVehicleNumber].iBitset, ENUM_TO_INT(eVEHICLEDATABITSET_CUSTOM_VEHICLE_MODEL))
					ELSE
						CLEAR_LONG_BIT(sMissionPlacementData.Vehicle.Vehicles[iVehicleNumber].iBitset, ENUM_TO_INT(eVEHICLEDATABITSET_CUSTOM_VEHICLE_MODEL))
					ENDIF					
					
					// Automate interior data
					sEditedVehPlacementData.iInterior = GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(sEditedVehPlacementData.vCoords)
					PRINTLN("DO_FMC_VEHICLE_CREATION - Setting sMissionPlacementData.Vehicle.Vehicles[",iVehicleNumber,"].iInterior to ", sEditedVehPlacementData.iInterior)

					// Copy the edited data into the struct if it has been placed
					sMissionPlacementData.Vehicle.Vehicles[iVehicleNumber] = sEditedVehPlacementData
					
					SET_VEHICLE_MENU_DEFAULTS()
					REFRESH_MENU_ASAP(sFMMCMenu)
					
					sVehStruct.iSwitchingINT = CREATION_STAGE_DONE
					
				ENDIF
				
				IF sVehStruct.iSelectedVehicleColourOld != sVehStruct.iSelectedVehicleColour
					INT iColour
					
					IF sVehStruct.iSelectedVehicleColour = -1
						iColour = GET_RANDOM_INT_IN_RANGE(0, ciNUMBER_OF_VEHICLE_COLOURS)
					ELSE
						iColour = sVehStruct.iSelectedVehicleColour
					ENDIF
					
					iColour = GET_VEHICLE_COLOUR_FROM_SELECTION(iColour)
					SET_VEHICLE_COLOURS(sVehStruct.viCoronaVeh, iColour, iColour)
					SET_VEHICLE_EXTRA_COLOURS(sVehStruct.viCoronaVeh, iColour, iColour)
					
					sVehStruct.iSelectedVehicleColourOld = sVehStruct.iSelectedVehicleColour
				ENDIF
				
				INT iDoor
				SC_DOOR_LIST eDoor
				REPEAT ciFMC_MAX_VEHICLE_DOORS iDoor
					eDoor = INT_TO_ENUM(SC_DOOR_LIST, iDoor)
					IF GET_IS_DOOR_VALID(sVehStruct.viCoronaVeh, eDoor)
						SWITCH INT_TO_ENUM(FMC_VEHICLE_DOOR_STATE, sVehStruct.iDoorState[iDoor])
							CASE eVEHICLEDOORSTATE_DEFAULT
							CASE eVEHICLEDOORSTATE_CLOSED
								SET_VEHICLE_DOOR_CONTROL(sVehStruct.viCoronaVeh, eDoor, DT_DOOR_INTACT, 0.0)
								SET_VEHICLE_DOOR_LATCHED(sVehStruct.viCoronaVeh, eDoor, TRUE, TRUE)
							BREAK
							CASE eVEHICLEDOORSTATE_OPEN
								SET_VEHICLE_DOOR_LATCHED(sVehStruct.viCoronaVeh, eDoor, FALSE, FALSE)
								SET_VEHICLE_DOOR_CONTROL(sVehStruct.viCoronaVeh, eDoor, DT_DOOR_INTACT, 1.0)
							BREAK
							CASE eVEHICLEDOORSTATE_OPEN_SWING_FREE
								SET_VEHICLE_DOOR_LATCHED(sVehStruct.viCoronaVeh, eDoor, FALSE, FALSE)
								SET_VEHICLE_DOOR_CONTROL(sVehStruct.viCoronaVeh, eDoor, DT_DOOR_SWINGING_FREE, 0.3)
							BREAK
							CASE eVEHICLEDOORSTATE_MISSING
								SET_VEHICLE_DOOR_BROKEN(sVehStruct.viCoronaVeh, eDoor, TRUE)
							BREAK
						ENDSWITCH
					ENDIF
				ENDREPEAT
			ELSE
				// Recreate the vehicle if it is somehow dead
				IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
					DELETE_VEHICLE(sVehStruct.viCoronaVeh)
				ENDIF
				
				sVehStruct.iSwitchingINT = CREATION_STAGE_SET_UP
			ENDIF
			
			//Go back through set up if the object model has changed
			IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
				MODEL_NAMES mnToUse
				mnToUse = GET_VEHICLE_MODEL_FROM_MENU_SELECTION(sFMMCmenu)
				
				IF IS_MODEL_VALID(mnToUse)
				AND mnToUse != GET_ENTITY_MODEL(sVehStruct.viCoronaVeh)
					IF DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[iVehicleNumber])
						REMOVE_BLIP(sVehStruct.biVehicleBlip[iVehicleNumber])
					ENDIF
					MODEL_NAMES oldModel
					oldModel = GET_ENTITY_MODEL(sVehStruct.viCoronaVeh)
					DELETE_VEHICLE(sVehStruct.viCoronaVeh)
					SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
					sVehStruct.iSwitchingINT = CREATION_STAGE_SET_UP
				ENDIF
			ENDIF
			
		BREAK
		
		// Update the ped you've just placed, increase the counter and reset for the next one
		CASE CREATION_STAGE_DONE
			IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
				IF IS_VEHICLE_DRIVEABLE(sVehStruct.viCoronaVeh)
				
					REAPPLY_NEW_2020_ENTITY_CREATION_DEFAULTS(sFMMCMenu)
					START_PLACEMENT_COOLDOWN_TIMER(sCurrentVarsStruct)
					
					// Update the vehicle entity now that it's sitting in the world
					SET_ENTITY_COLLISION(sVehStruct.viCoronaVeh, TRUE)
					SET_CAN_CLIMB_ON_ENTITY(sVehStruct.viCoronaVeh, FALSE)
					SET_ENTITY_PROOFS(sVehStruct.viCoronaVeh , FALSE, FALSE, FALSE, FALSE, FALSE)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sVehStruct.viCoronaVeh, FALSE)
					IF VEHICLE_IS_A_WATER_BASED_VEHICLE(sVehStruct.viCoronaVeh)
					AND (IS_ENTITY_IN_WATER(sVehStruct.viCoronaVeh) OR GET_ENTITY_MODEL(sVehStruct.viCoronaVeh) = MARQUIS)
					AND GET_ENTITY_MODEL(sVehStruct.viCoronaVeh) != STROMBERG
						SET_VEHICLE_ON_GROUND_PROPERLY(sVehStruct.viCoronaVeh)
						VECTOR vBoatCoords
						vBoatCoords = GET_ENTITY_COORDS(sVehStruct.viCoronaVeh)
						FLOAT fFloorZ
						IF NOT GET_GROUND_Z_FOR_3D_COORD(vBoatCoords, fFloorZ)
						OR fFloorZ < (vBoatCoords.z - 3.0)
							SET_BOAT_ANCHOR(sVehStruct.viCoronaVeh, TRUE)
							PRINTLN("ANCHOR THE BOAT, PLEASE.")
						ELSE
							PRINTLN("WATER NOT DEEP ENOUGH TO ANCHOR")
						ENDIF
					ELSE
						IF NOT IS_ENTITY_IN_AIR(sVehStruct.viCoronaVeh)
							SET_VEHICLE_ON_GROUND_PROPERLY(sVehStruct.viCoronaVeh)
						ENDIF
					ENDIF
					
					// Give it a blip
					CREATE_FMMC_BLIP(sVehStruct.biVehicleBlip[iVehicleNumber], GET_ENTITY_COORDS(sVehStruct.viCoronaVeh), HUD_COLOUR_BLUE, "FMMC_B_10", 1)
					
					// Add it's model to the budget (not really important in this creator)
					ADD_MODEL_TO_CREATOR_BUDGET(GET_ENTITY_MODEL(sVehStruct.viCoronaVeh))
					
					// Do the placement particle effect
					CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, GET_ENTITY_COORDS(sVehStruct.viCoronaVeh))
					
					// Update the vehicle index so that it's in the array for the in world vehicles
					sVehStruct.veVehcile[iVehicleNumber] = sVehStruct.viCoronaVeh
					sVehStruct.viCoronaVeh= NULL
					
					CLEAR_BIT(sEditedGenericData.iBitset, ENUM_TO_INT(eGENERICDATABITSET_OVERRIDE_POSITION))
					CLEAR_BIT(sEditedGenericData.iBitset, ENUM_TO_INT(eGENERICDATABITSET_OVERRIDE_ROTATION))
					
					sFMMCmenu.iSwitchCam = iVehicleNumber
					RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
					
					REFRESH_MENU_ASAP(sFMMCMenu)
							
					RESET_ENTITY_ALPHA(sVehStruct.veVehcile[iVehicleNumber])
					
					// Increase the count if you've placed a new one
					IF sFMMCMenu.iSelectedEntity = -1
						
						sMissionPlacementData.Vehicle.iCount++
						
						IF sMissionPlacementData.Vehicle.iCount > MAX_NUM_VEHICLES
							sMissionPlacementData.Vehicle.iCount = MAX_NUM_VEHICLES
						ENDIF
					ENDIF
				ENDIF
				
				sFMMCMenu.iSelectedEntity = -1
				sVehStruct.iSwitchingINT = CREATION_STAGE_WAIT
			ENDIF
		BREAK
	ENDSWITCH
	
	FMC_UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, sVehStruct.viCoronaVeh, FALSE)
	
ENDPROC

PROC DELETE_FMC_PLACED_VEHICLE(INT iVehicleNumber)

	INT i
	MISSION_PLACEMENT_DATA_VEHICLES sTempVehStruct
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	INT iAmbush
	REPEAT MAX_NUM_AMBUSH_VEHICLES iAmbush
		IF sMissionPlacementData.Ambush.Vehicle[iAmbush].iIndex = iVehicleNumber
			DELETE_FMC_AMBUSH(iAmbush)
		ENDIF
	ENDREPEAT
	
	// Remove the entity/blip
	IF DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[iVehicleNumber])
		REMOVE_BLIP(sVehStruct.biVehicleBlip[iVehicleNumber])
	ENDIF
	IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[iVehicleNumber])
		DELETE_VEHICLE(sVehStruct.veVehcile[iVehicleNumber])
	ENDIF
	
	// Move all the entries down
	FOR i = iVehicleNumber+1 TO sMissionPlacementData.Vehicle.iCount-1
		sVehStruct.biVehicleBlip[i-1] = sVehStruct.biVehicleBlip[i]
		sVehStruct.veVehcile[i-1] = sVehStruct.veVehcile[i]
		sMissionPlacementData.Vehicle.Vehicles[i-1]= sMissionPlacementData.Vehicle.Vehicles[i]
	ENDFOR
	
	// Reduce the array
	sMissionPlacementData.Vehicle.iCount--
	
	// Clear the last entry
	sMissionPlacementData.Vehicle.Vehicles[sMissionPlacementData.Vehicle.iCount] = sTempVehStruct
	sVehStruct.biVehicleBlip[sMissionPlacementData.Vehicle.iCount] = NULL
	sVehStruct.veVehcile[sMissionPlacementData.Vehicle.iCount] = NULL
	
	// Update references
	UPDATE_TAGS(iVehicleNumber, eTAGTYPE_VEHICLE)
	UPDATE_PED_VEHICLES(iVehicleNumber)
	UPDATE_TARGETS(ET_VEHICLE, iVehicleNumber)
	UPDATE_AMBUSH_DATA(iVehicleNumber)
	
ENDPROC

//Deal with the placed vehicles
PROC MAINTAIN_PLACED_FMC_VEHICLES(CREATION_VARS_STRUCT &sCurrentVarsStruct, structFMMC_MENU_ITEMS &sFMMCmenu, FMMC_LOCAL_STRUCT &sFMMCdata, HUD_COLOURS_STRUCT &sHCS, FMMC_COMMON_MENUS_VARS &sFMMCendStage)
	
	//If these are then same value then don't draw the corona
	VECTOR vMin, vMax
	FLOAT fRadius
	MODEL_NAMES mnTemp
	INT i
	
	BOOL bPlaneCorona = TRUE
	BOOL bAbleToEdit = TRUE
	
	//When deleting props, checkpoints and respawns on the prop should also be deleted
	IF IS_BIT_SET(iLocalBitSet, biVehicleHeightTestNeeded)
		IF sCurrentVarsStruct.iVehiclePropDeleteionBitset > 0
			IF BATCH_MARK_VEHICLES_FOR_DELETION_THEN_DELETE(sVehStruct, sCurrentVarsStruct, sFMMCmenu, sFMMCendStage)
				CLEAR_BIT(iLocalBitSet, biVehicleHeightTestNeeded)
			ENDIF
		ELSE
			CLEAR_BIT(iLocalBitSet, biVehicleHeightTestNeeded)
		ENDIF
	ENDIF
	
	// Loop through all vehicles for every frame checks
	FOR i = 0 TO sMissionPlacementData.Vehicle.iCount-1
	
		IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[i])
			CLEAR_BIT(sCurrentVarsStruct.iVehFlaggedForReCreation, i)
			
			DRAW_ENTITY_LABEL(CREATION_TYPE_VEHICLES, i, VECTOR_ZERO)
			TEXT_LABEL_63 tl63 = "Vehicle: "
			tl63 += i
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(sVehStruct.veVehcile[i], tl63, 0.5)
			
			IF IS_VEHICLE_DRIVEABLE(sVehStruct.veVehcile[i])
			
				//Get the size of the vehicle to know if it's in the corona
				mnTemp = GET_ENTITY_MODEL(sVehStruct.veVehcile[i])
				GET_MODEL_DIMENSIONS(mnTemp, vMin, vMax)
				//get the min width
				IF vMax.x < vMax.y
					fRadius = vMax.x 
				ELSE
					fRadius = vMax.y 
				ENDIF
				//make it a bit bigger
				fRadius *= 1.2
				//cap it
				IF fRadius < 1.0
					fRadius = 1.0
				ELIF fRadius > 2.5
					fRadius = 2.5
				ENDIF
				
				// [LM][CopyAndPaste]
				#IF IS_DEBUG_BUILD
				IF IS_ENTITY_IN_CORONA(sVehStruct.veVehcile[i], sCurrentVarsStruct, 2.0, i, CREATION_TYPE_VEHICLES, -1, sFMMCmenu.iSelectedTeam)
					IF sFMMCmenu.iEntityCreation = CREATION_TYPE_VEHICLES
						sFMMCMenu.iCopyFromEntity = i
						DRAW_ON_SCREEN_ENTITY_INFO(sFMMCmenu, sFMMCMenu.iCopyFromEntity, CREATION_TYPE_VEHICLES)
					ENDIF
				ENDIF
				#ENDIF
				
				IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_VEHICLES, i, sVehStruct.veVehcile[i], bPlaneCorona, HUD_COLOUR_BLUEDARK, sFMMCdata, sFMMCendStage, fRadius, bAbleToEdit)
					
					// Pick it up
					IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
						PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_VEHICLES, i)
						REMOVE_MODEL_FROM_CREATOR_BUDGET(sMissionPlacementData.Vehicle.Vehicles[i].model)
						CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
					
					// Delete it
					ELIF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
						REMOVE_MODEL_FROM_CREATOR_BUDGET(sMissionPlacementData.Vehicle.Vehicles[i].model)
						sCurrentVarsStruct.iEntityRemoved = i
						DELETE_FMC_PLACED_VEHICLE(i)
						CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
						REFRESH_MENU(sFMMCMenu)
						sFMMCMenu.iSelectedEntity = -1
						CLEAR_BIT(iLocalBitSet, biEntityInCorona)
					ENDIF
					bPlaneCorona = TRUE
				ELSE
					IF bPlaneCorona = FALSE
						sCurrentVarsStruct.tl15SelectedName = GET_CREATOR_NAME_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(sVehStruct.veVehcile[i]))
						PRINT_SELECTED_ENTITY_NAME_AS_FLOATING_HELP(sCurrentVarsStruct, sCurrentVarsStruct.tl15SelectedName)
						bPlaneCorona = TRUE
					ENDIF
				ENDIF
				
				IF IS_ENTITY_ALIVE(sVehStruct.veVehcile[i])
					IF NOT (VEHICLE_IS_A_WATER_BASED_VEHICLE(sVehStruct.veVehcile[i])
					AND IS_ENTITY_IN_WATER(sVehStruct.veVehcile[i])
					AND GET_ENTITY_MODEL(sVehStruct.veVehcile[i]) != STROMBERG)
						FREEZE_ENTITY_POSITION(sVehStruct.veVehcile[i], TRUE)
					ENDIF
				ENDIF
				
			ENDIF
			
			// Create a blip if it needs one
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_TARGETS
				MAINTAIN_TARGET_BLIPS(sFMMCmenu, ET_VEHICLE, i)
			ELSE
				IF NOT DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[i])
				AND sCycleCamStruct.BlockBlipsState = eBLOCK_BLIPS_NONE
					CREATE_FMMC_BLIP(sVehStruct.biVehicleBlip[i], GET_ENTITY_COORDS(sVehStruct.veVehcile[i]), HUD_COLOUR_BLUE, "FMMC_B_10", 1)
				ENDIF
			ENDIF
			
			MOVE_ENTITY_TO_CURRENT_ARENA(sVehStruct.veVehcile[i])
		ELSE
			// Recreate it if it has been deleted
			IF sFMMCmenu.iEntityCreation != CREATION_TYPE_VEHICLES
			OR sFMMCMenu.iSelectedEntity != i
				CREATE_FMC_VEHICLE(sVehStruct.veVehcile[i], sMissionPlacementData.Vehicle.Vehicles[i])
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC







/////////////////////////////////////////////
///    MISSION ENTITIES
///    

// Generic create object function based on the placement data
PROC CREATE_FMC_MISSION_ENTITY(OBJECT_INDEX &oiObject, MISSION_PLACEMENT_DATA_MISSION_ENTITIES &MissionEntitiy)

	oiObject = CREATE_OBJECT(MissionEntitiy.model, MissionEntitiy.vCoords, FALSE, FALSE)
	SET_ENTITY_HEADING(oiObject, MissionEntitiy.fHeading)
	FREEZE_ENTITY_POSITION(oiObject, TRUE)
	SET_ENTITY_COLLISION(oiObject, FALSE)
	SET_ENTITY_LOD_DIST(oiObject, 1000)	
	
ENDPROC

FUNC BOOL CREATE_ALL_FMC_MISSION_ENTITIES()
	
	INT i
	
	// Loop over all mission entities and create them
	FOR i = 0 TO (sMissionPlacementData.MissionEntity.iCount - 1)	
		IF NOT DOES_ENTITY_EXIST(sObjStruct.oiObject[i])
			REQUEST_MODEL(sMissionPlacementData.MissionEntity.MissionEntities[i].model)
			IF HAS_MODEL_LOADED(sMissionPlacementData.MissionEntity.MissionEntities[i].model)
				sObjStruct.oiObject[i] = CREATE_OBJECT(sMissionPlacementData.MissionEntity.MissionEntities[i].model , sMissionPlacementData.MissionEntity.MissionEntities[i].vCoords,FALSE,FALSE )
				
				SET_ENTITY_COORDS_NO_OFFSET(sObjStruct.oiObject[i], sMissionPlacementData.MissionEntity.MissionEntities[i].vCoords)
				IF NOT IS_VECTOR_ZERO(sMissionPlacementData.MissionEntity.MissionEntities[i].vRotation)
					SET_ENTITY_ROTATION(sObjStruct.oiObject[i], sMissionPlacementData.MissionEntity.MissionEntities[i].vRotation)
				ELSE
					SET_ENTITY_HEADING(sObjStruct.oiObject[i], sMissionPlacementData.MissionEntity.MissionEntities[i].fHeading)
				ENDIF
				SET_ENTITY_PROOFS(sObjStruct.oiObject[i], TRUE, TRUE, TRUE, TRUE, TRUE)
				FREEZE_ENTITY_POSITION(sObjStruct.oiObject[i], TRUE)
				
				SET_WAIT_FOR_COLLISIONS_BEFORE_PROBE(sObjStruct.oiObject[i], TRUE)
				
				// Update the cusom props list with this entity's model if it's custom
				IF IS_LONG_BIT_SET(sMissionPlacementData.MissionEntity.MissionEntities[i].iBitset, ENUM_TO_INT(eMISSIONENTITYDATABITSET_CUSTOM_MODEL))
					INT iCustom
					BOOL bAdd = TRUE
					FOR iCustom = 0 TO ciMAX_CUSTOM_DEV_PROPS-1
						IF sMissionPlacementData.MissionEntity.MissionEntities[i].model = g_FMMC_STRUCT.sCustomProps[iCustom].mnCustomPropName
							bAdd = FALSE
							BREAKLOOP
						ENDIF
					ENDFOR
					IF bAdd
						ADD_CUSTOM_PROP(sMissionPlacementData.MissionEntity.MissionEntities[i].model)
					ENDIF
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	// Make sure they all exist
	FOR i = 0 TO (sMissionPlacementData.MissionEntity.iCount - 1)
		IF NOT DOES_ENTITY_EXIST(sObjStruct.oiObject[i])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	// Unload their models
	REPEAT sMissionPlacementData.MissionEntity.iCount 	 i
		IF sMissionPlacementData.MissionEntity.MissionEntities[i].model != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(sMissionPlacementData.MissionEntity.MissionEntities[i].model)
		ENDIF
	ENDREPEAT	
	
	RETURN TRUE
	
ENDFUNC

FUNC MODEL_NAMES GET_MISSION_ENTITY_MODEL(structFMMC_MENU_ITEMS &sFMMCmenu)
	
	INT iLibType = sFMMCMenu.iMissionEntModelLibrary
	INT iEntSelection = sFMMCMenu.iMissionEntType
	INT iEntLibrary = sFMMCMenu.iMissionEntLibrary
	
	// Grab the model based on whether it's a cusom prop or from the standard object library
	IF iLibType = MISSION_ENT_LIBRARY_STANDARD
		RETURN sObjStruct.mnObjectType[iEntLibrary][iEntSelection]
		
	ELIF iLibType = MISSION_ENT_LIBRARY_CUSTOM
		IF iEntSelection <= ciMAX_CUSTOM_DEV_PROPS
		AND iEntSelection > 0
			RETURN g_FMMC_STRUCT.sCustomProps[iEntSelection-1].mnCustomPropName
		ENDIF
		
	ENDIF
	
	// Return PROP_DRUG_PACKAGE as default
	RETURN sObjStruct.mnObjectType[OBJECT_LIBRARY_CAPTURE][DEFAULT_OBJECT_INDEX]
	
ENDFUNC

FUNC INT GET_MISSION_ENTITY_SELECTION_FROM_CUSTOM_MODEL(MODEL_NAMES mnPassed)
	INT i
	FOR i = 0 TO g_FMMC_STRUCT.iCurrentNumberOfDevProps - 1
		IF g_FMMC_STRUCT.sCustomProps[i].mnCustomPropName = mnPassed
			RETURN i+1
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

PROC GET_MISSION_ENTITY_LIBRARY_AND_SELECTION_FROM_MODEL(structFMMC_MENU_ITEMS &sFMMCmenu, MODEL_NAMES mnMissionEntity)
	
	// First try to find the model from the standard object prop list
	sFMMCMenu.iMissionEntType = GET_OBJ_SELECTION_FROM_MODEL(sObjStruct, mnMissionEntity, FALSE, TRUE)
	
	// If no valid model is found search through the custom props list
	IF sFMMCMenu.iMissionEntType = -1
		sFMMCMenu.iMissionEntType = GET_MISSION_ENTITY_SELECTION_FROM_CUSTOM_MODEL(mnMissionEntity)
		
		IF sFMMCMenu.iMissionEntType > -1
			// If it finds a valid one set the library to custom
			sFMMCMenu.iMissionEntModelLibrary = MISSION_ENT_LIBRARY_CUSTOM
			
		ELIF g_FMMC_STRUCT.iCurrentNumberOfDevProps < ciMAX_CUSTOM_DEV_PROPS
			// If it doesn't find a valid one then add the model to the cusom library
			ADD_CUSTOM_PROP(mnMissionEntity)
			sFMMCMenu.iMissionEntType = g_FMMC_STRUCT.iCurrentNumberOfDevProps
			sFMMCMenu.iMissionEntModelLibrary = MISSION_ENT_LIBRARY_CUSTOM
		ENDIF
	ELSE
		sFMMCMenu.iMissionEntModelLibrary = MISSION_ENT_LIBRARY_STANDARD
	ENDIF
ENDPROC

PROC DO_MISSION_ENTITY_CREATION(	CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
									structFMMC_MENU_ITEMS 		&sFMMCmenu, 
									FMMC_LOCAL_STRUCT 			&sFMMCData,
									INT 						iMaximum = MAX_NUM_MISSION_ENTITIES)

	INT iObjectNumber
	
	// Get the number of the mission entity based on if it's new or picked up
	IF sFMMCMenu.iSelectedEntity = -1
		iObjectNumber = sMissionPlacementData.MissionEntity.iCount
	ELSE
		
		iObjectNumber = sFMMCMenu.iSelectedEntity
		
		IF iObjectNumber >= iMaximum 
			iObjectNumber = iMaximum - 1
			sFMMCMenu.iSelectedEntity = iObjectNumber
		ENDIF
		
		// If you've picked up an existing one, remove it from the world so that it can be recreated as the placement one
		IF DOES_ENTITY_EXIST(sObjStruct.oiObject[iObjectNumber])
		
			IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
				DELETE_OBJECT(sObjStruct.viCoronaObj)
			ENDIF
			IF DOES_ENTITY_EXIST(sObjStruct.oiObject[iObjectNumber])
				DELETE_OBJECT(sObjStruct.oiObject[iObjectNumber])
			ENDIF
			IF DOES_BLIP_EXIST(sObjStruct.biObject[iObjectNumber])
				REMOVE_BLIP(sObjStruct.biObject[iObjectNumber])
			ENDIF
			
			// Update the edited data to that of the placed one
			sEditedMisEntPlacementData = sMissionPlacementData.MissionEntity.MissionEntities[iObjectNumber]
			
			sObjStruct.viCoronaObj 				= sObjStruct.oiObject[iObjectNumber]
			sObjStruct.oiObject[iObjectNumber]	= NULL
			
			sCurrentVarsStruct.fCreationHeading = sEditedMisEntPlacementData.fHeading
			SET_OVERRIDE_ROTATION_VECTOR(sFMMCmenu, sEditedMisEntPlacementData.vRotation)
			
			GET_MISSION_ENTITY_LIBRARY_AND_SELECTION_FROM_MODEL(sFMMCmenu, sMissionPlacementData.MissionEntity.MissionEntities[iObjectNumber].model)
			
			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
						
			IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
				SET_ENTITY_COLLISION(sObjStruct.viCoronaObj, FALSE)
			ENDIF
			
		ENDIF
	ENDIF
	
	// UI for counting the number placed so far and there being too many
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_OBJECTS)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		AND sFMMCMenu.iSelectedEntity = -1
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			sCurrentVarsStruct.bDisplayFailReason = TRUE
			sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			sCurrentVarsStruct.sFailReason = "FMMC_ER_TO"
			PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
		ENDIF
	ENDIF
	IF sMissionPlacementData.MissionEntity.iCount >= iMaximum	
	AND iObjectNumber >= iMaximum
		sObjStruct.iSwitchingINT = CREATION_STAGE_WAIT
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_OBJECTS)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.MissionEntity.iCount,  iMaximum,	"FMMC_AB_04", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_FLASHRED, 500)
		EXIT
	ELSE
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.MissionEntity.iCount,  iMaximum,	"FMMC_AB_04", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_OBJECTS)
	ENDIF
	
	// Main placement state machine
	SWITCH sObjStruct.iSwitchingINT
	
		// Wait stage to stall the creation of more if there are too many already
		CASE CREATION_STAGE_WAIT
			IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
				DELETE_OBJECT(sObjStruct.viCoronaObj)
			ENDIF
			IF iObjectNumber < iMaximum
				sObjStruct.iSwitchingINT = CREATION_STAGE_SET_UP	
			ENDIF
		BREAK
		
		// Request the model ready for creation
		CASE CREATION_STAGE_SET_UP
			IF NOT DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
			
				sObjStruct.iSwitchingINT = CREATION_STAGE_MAKE
				IF IS_MODEL_VALID(GET_MISSION_ENTITY_MODEL(sFMMCMenu))
					REQUEST_MODEL(GET_MISSION_ENTITY_MODEL(sFMMCMenu))
					PRINTLN("(1) Loading ", GET_MISSION_ENTITY_MODEL(sFMMCMenu))
				ENDIF
				
			ELSE
				sObjStruct.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		
		// Create the new placement mission entity for the corona
		CASE CREATION_STAGE_MAKE
		
			IF NOT DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
				REQUEST_MODEL(GET_MISSION_ENTITY_MODEL(sFMMCMenu))
				IF HAS_MODEL_LOADED(GET_MISSION_ENTITY_MODEL(sFMMCMenu))
					
					REFRESH_MENU_ASAP(sFMMCMenu)
					CREATE_FMC_MISSION_ENTITY(sObjStruct.viCoronaObj, sEditedMisEntPlacementData)
					sObjStruct.iSwitchingINT = CREATION_STAGE_PLACE
				ENDIF		
			ELSE
				sObjStruct.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
			
		BREAK
		
		// Do the placement of the mission entity, updating it's position/rotation/model/etc based on user input
		CASE CREATION_STAGE_PLACE
		
			sEditedMisEntPlacementData.vCoords = sCurrentVarsStruct.vCoronaPos
			sEditedMisEntPlacementData.fHeading = sCurrentVarsStruct.fCreationHeading
			sEditedMisEntPlacementData.model = GET_MISSION_ENTITY_MODEL(sFMMCMenu)
			
			// Handle the override placement where you can input the exact position/rotation in the menus
			HANDLE_ENTITY_CREATION_OVERRIDES(sFMMCMenu, sFMMCData, sCurrentVarsStruct, sObjStruct.viCoronaObj, sEditedMisEntPlacementData.iBitset[GET_LONG_BITSET_INDEX(iObjectNumber)], ENUM_TO_INT(eMISSIONENTITYDATABITSET_OVERRIDE_POSITION), ENUM_TO_INT(eMISSIONENTITYDATABITSET_OVERRIDE_ROTATION), FALSE, FALSE, FALSE)
			
			// Has to pass this in as MOVE_LAST_PLACED_OBJECT is a Mission Creator function that freemode is sharing.
			TRAIN_CREATION_STRUCT sTrainBlank
			
			// This uses the existing MOVE_LAST_PLACED from the other creators as the functionality is shared
			// Updates the position/rotation and checks that the area is safe and if the user has placed it
			IF MOVE_LAST_PLACED_OBJECT(sFMMCmenu,
										sObjStruct.viCoronaObj,
										sObjStruct,
										sVehStruct,
										iObjectNumber,
										sEditedMisEntPlacementData.vCoords,
										sEditedMisEntPlacementData.fHeading,
										sCurrentVarsStruct,
										sFMMCData,
										sTrainBlank,
										IS_LONG_BIT_SET(sEditedMisEntPlacementData.iBitset, ENUM_TO_INT(eMISSIONENTITYDATABITSET_OVERRIDE_POSITION)),
										IS_LONG_BIT_SET(sEditedMisEntPlacementData.iBitset, ENUM_TO_INT(eMISSIONENTITYDATABITSET_OVERRIDE_ROTATION)))
			
				// Update the placement data to the edited version
				sMissionPlacementData.MissionEntity.MissionEntities[iObjectNumber] = sEditedMisEntPlacementData
				
				IF IS_LONG_BIT_SET(sEditedMisEntPlacementData.iBitset, ENUM_TO_INT(eMISSIONENTITYDATABITSET_OVERRIDE_ROTATION))
					sMissionPlacementData.MissionEntity.MissionEntities[iObjectNumber].vRotation = GET_ENTITY_ROTATION(sObjStruct.viCoronaObj)
				ENDIF
				
				sMissionPlacementData.MissionEntity.MissionEntities[iObjectNumber].iInterior = GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(sEditedMisEntPlacementData.vCoords)
				PRINTLN("DO_MISSION_ENTITY_CREATION - Setting sMissionPlacementData.MissionEntity.MissionEntities[",iObjectNumber,"].iInterior to ", sMissionPlacementData.MissionEntity.MissionEntities[iObjectNumber].iInterior)
				
				sMissionPlacementData.MissionEntity.Blip = sEditedMisEntBlipData.Blip
				
				// Set if a custom prop has been used
				IF sFMMCMenu.iMissionEntModelLibrary = MISSION_ENT_LIBRARY_CUSTOM
					SET_LONG_BIT(sMissionPlacementData.MissionEntity.MissionEntities[iObjectNumber].iBitset, ENUM_TO_INT(eMISSIONENTITYDATABITSET_CUSTOM_MODEL))
				ELSE
					CLEAR_LONG_BIT(sMissionPlacementData.MissionEntity.MissionEntities[iObjectNumber].iBitset, ENUM_TO_INT(eMISSIONENTITYDATABITSET_CUSTOM_MODEL))
				ENDIF
				
				sObjStruct.iSwitchingINT = CREATION_STAGE_DONE
				
			ENDIF
			
			//go back through set up if the object model has changed
			IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
				IF GET_MISSION_ENTITY_MODEL(sFMMCMenu) != GET_ENTITY_MODEL(sObjStruct.viCoronaObj)
					IF DOES_BLIP_EXIST(sObjStruct.biObject[iObjectNumber])
						REMOVE_BLIP(sObjStruct.biObject[iObjectNumber])
					ENDIF
					MODEL_NAMES oldModel
					oldModel = GET_ENTITY_MODEL(sObjStruct.viCoronaObj)
					DELETE_OBJECT(sObjStruct.viCoronaObj)					
					SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
					sObjStruct.iSwitchingINT = CREATION_STAGE_SET_UP
				ENDIF
				IF IS_BIT_SET(iLocalBitset, biCanEditEntityInCorona)
					SET_ENTITY_ALPHA(sObjStruct.viCoronaObj, 0, FALSE)
				ELSE
					RESET_ENTITY_ALPHA(sObjStruct.viCoronaObj)
				ENDIF
			ELSE
				sObjStruct.iSwitchingINT = CREATION_STAGE_SET_UP
			ENDIF
		BREAK
		
		// Update the object you've just placed, increase the counter and reset for the next one
		CASE CREATION_STAGE_DONE
		
			IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
			
				sObjStruct.oiObject[iObjectNumber] = sObjStruct.viCoronaObj
				SET_ENTITY_COLLISION(sObjStruct.oiObject[iObjectNumber], TRUE)					
				CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)					
				CREATE_OBJECT_DECAL(sObjStruct, iObjectNumber, TRUE, sCurrentVarsStruct.bCanCreateADecalThisFrame)
				
				CREATE_FMMC_BLIP(sObjStruct.biObject[iObjectNumber], GET_ENTITY_COORDS(sObjStruct.oiObject[iObjectNumber]), HUD_COLOUR_GREEN, "FMMC_B_12", 1)
				
				CLEAR_LONG_BIT(sEditedMisEntPlacementData.iBitset, ENUM_TO_INT(eMISSIONENTITYDATABITSET_OVERRIDE_POSITION))
				CLEAR_LONG_BIT(sEditedMisEntPlacementData.iBitset, ENUM_TO_INT(eMISSIONENTITYDATABITSET_OVERRIDE_ROTATION))
				
				sObjStruct.viCoronaObj = NULL
				
				IF sFMMCMenu.iSelectedEntity = -1
					
					sMissionPlacementData.MissionEntity.iCount++
					
					IF sMissionPlacementData.MissionEntity.iCount > iMaximum
						sMissionPlacementData.MissionEntity.iCount = iMaximum
					ENDIF
				ENDIF
				
				sFMMCMenu.iSelectedEntity = -1
				sObjStruct.iSwitchingINT = CREATION_STAGE_WAIT
				
			ENDIF
		BREAK
		
	ENDSWITCH
	
	FMC_UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, sObjStruct.viCoronaObj, TRUE, DEFAULT, DEFAULT, !IS_LONG_BIT_SET(sEditedMisEntPlacementData.iBitset, ENUM_TO_INT(eMISSIONENTITYDATABITSET_OVERRIDE_POSITION)))
	
ENDPROC

PROC DELETE_FMC_PLACED_MISSION_ENTITY(INT iMissionEntityNumber)

	INT i
	MISSION_PLACEMENT_DATA_MISSION_ENTITIES sTempMisEntStruct
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	// Remove the entity/blip
	IF DOES_BLIP_EXIST(sObjStruct.biObject[iMissionEntityNumber])
		REMOVE_BLIP(sObjStruct.biObject[iMissionEntityNumber])
	ENDIF
	IF DOES_ENTITY_EXIST(sObjStruct.oiObject[iMissionEntityNumber])
		DELETE_OBJECT(sObjStruct.oiObject[iMissionEntityNumber])
	ENDIF
	
	// Move all the entries down
	FOR i = iMissionEntityNumber+1 TO sMissionPlacementData.MissionEntity.iCount-1
		sObjStruct.biObject[i-1] = sObjStruct.biObject[i]
		sObjStruct.oiObject[i-1] = sObjStruct.oiObject[i]
		sMissionPlacementData.MissionEntity.MissionEntities[i-1]= sMissionPlacementData.MissionEntity.MissionEntities[i]
	ENDFOR
	
	// Reduce the array
	sMissionPlacementData.MissionEntity.iCount--
	
	// Clear the last entry
	sMissionPlacementData.MissionEntity.MissionEntities[sMissionPlacementData.MissionEntity.iCount] = sTempMisEntStruct
	sObjStruct.biObject[sMissionPlacementData.MissionEntity.iCount] = NULL
	sObjStruct.oiObject[sMissionPlacementData.MissionEntity.iCount] = NULL
	
	// Update tags
	UPDATE_TAGS(iMissionEntityNumber, eTAGTYPE_MISSION_ENTITY)
	
ENDPROC

//Deal with the placed vehicles
PROC MAINTAIN_PLACED_FMC_OBJECTS(CREATION_VARS_STRUCT &sCurrentVarsStruct, structFMMC_MENU_ITEMS &sFMMCmenu, FMMC_LOCAL_STRUCT &sFMMCdata, HUD_COLOURS_STRUCT &sHCS, FMMC_COMMON_MENUS_VARS &sFMMCendStage)

	INT i
	BOOL bPlaneCorona = TRUE
	
	// Loop over the placed objects
	FOR i = 0 TO sMissionPlacementData.MissionEntity.iCount-1
		IF DOES_ENTITY_EXIST(sObjStruct.oiObject[i])
		
			// Determine the radius to check based on the model to see if it's in the corona
			FLOAT fRadius = 1.0
			IF DOES_OBJECT_NEED_LARGER_RADIUS(GET_ENTITY_MODEL(sObjStruct.oiObject[i]))
				fRadius = 2.0
			ENDIF
			
			IF IS_ENTITY_IN_CORONA(sObjStruct.oiObject[i], sCurrentVarsStruct, 1.0, i, CREATION_TYPE_OBJECTS, -1, sFMMCmenu.iSelectedTeam)
				IF sFMMCmenu.iEntityCreation = CREATION_TYPE_OBJECTS
					sFMMCMenu.iCopyFromEntity = i
					DRAW_ON_SCREEN_ENTITY_INFO(sFMMCmenu, sFMMCMenu.iCopyFromEntity, CREATION_TYPE_OBJECTS)
				ENDIF
			ENDIF
			
			// Delete it if the model doesn't match anymore
			IF GET_ENTITY_MODEL(sObjStruct.oiObject[i]) != sMissionPlacementData.MissionEntity.MissionEntities[i].model
				DELETE_OBJECT(sObjStruct.oiObject[i])
			ELSE
				
				// Check if it's in the corona
				DRAW_ENTITY_LABEL(CREATION_TYPE_OBJECTS, i, VECTOR_ZERO)
				TEXT_LABEL_63 tl63 = "Mission Entity: "
				tl63 += i
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(sObjStruct.oiObject[i], tl63, 0.5)
				
				IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_OBJECTS, i, sObjStruct.oiObject[i], bPlaneCorona, HUD_COLOUR_PURPLEDARK, sFMMCdata, sFMMCendStage, fRadius)
				
					// Pickup
					IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
						IF sObjStruct.iDecalNum[i] != -1
							REMOVE_DECAL(sObjStruct.diDecal[sObjStruct.iDecalNum[i]])
						ENDIF
						PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_OBJECTS, i)
						CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
						
					// Delete
					ELIF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
						PRINTLN("Deleting here 1")
						DELETE_FMC_PLACED_MISSION_ENTITY(i)
						CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
					ENDIF
					
				ENDIF
			ENDIF
			
			// Give it a blip if needed
			IF NOT DOES_BLIP_EXIST(sObjStruct.biObject[i])
				CREATE_FMMC_BLIP(sObjStruct.biObject[i], GET_ENTITY_COORDS(sObjStruct.oiObject[i]), HUD_COLOUR_GREEN, "FMMC_B_12", 1)
			ENDIF
		ELSE
			// Recreate it if needed
			IF sFMMCmenu.iEntityCreation != CREATION_TYPE_OBJECTS
			OR sFMMCMenu.iSelectedEntity != i
				CREATE_FMC_MISSION_ENTITY(sObjStruct.oiObject[i], sMissionPlacementData.MissionEntity.MissionEntities[i])
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Prop Creation
// ##### Description: Functions for creating and editing placed props
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Generic function to create the prop based on it's data
PROC SET_UP_FMC_PROP(OBJECT_INDEX oiPassed, INT iPropNumber)

	SET_ENTITY_COORDS(oiPassed, sMissionPlacementData.Prop.Props[iPropNumber].vCoords)
	
	IF NOT IS_VECTOR_ZERO(sMissionPlacementData.Prop.Props[iPropNumber].vRotation)
	OR IS_SNAPPABLE_STUNT_PROP(sMissionPlacementData.Prop.Props[iPropNumber].model)
		SET_ENTITY_ROTATION(oiPassed, sMissionPlacementData.Prop.Props[iPropNumber].vRotation)
	ELSE
		SET_ENTITY_HEADING(oiPassed, sMissionPlacementData.Prop.Props[iPropNumber].fHeading)
	ENDIF
	
	SET_ENTITY_VISIBLE(oiPassed, TRUE)
	SET_OBJECT_TARGETTABLE(oiPassed, FALSE)
	SET_ENTITY_INVINCIBLE(oiPassed, TRUE)
	SET_ENTITY_LOD_DIST(oiPassed, PROP_LOD_DIST) 
	SET_WAIT_FOR_COLLISIONS_BEFORE_PROBE(oiPassed, TRUE)
		
	IF NOT IS_PROP_WATER_TYPE(GET_ENTITY_MODEL(oiPassed)) //Water props need to behave correctly in water
	AND NOT IS_PROP_CORNER_SIGN_TYPE(GET_ENTITY_MODEL(oiPassed))
		IF SHOULD_OBJECT_BE_FROZEN(oiPassed)
			SET_ENTITY_PROOFS(oiPassed, TRUE,TRUE,TRUE,TRUE,TRUE)
			FREEZE_ENTITY_POSITION(oiPassed, TRUE)
			SET_OBJECT_ALLOW_LOW_LOD_BUOYANCY(oiPassed,TRUE)
		ENDIF
	ENDIF
 	
ENDPROC

FUNC BOOL CREATE_ALL_FMC_PROPS(INT &iPropModelTimers[])

	INT i
	
	// Loop over all placed props to recreate them
	FOR i = 0 TO (sMissionPlacementData.Prop.iCount - 1)
		IF NOT DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
			REQUEST_MODEL(sMissionPlacementData.Prop.Props[i].model)
			
			IF iPropModelTimers[i] = 0
				iPropModelTimers[i] = GET_GAME_TIMER()
			ENDIF
			
			IF HAS_MODEL_LOADED(sMissionPlacementData.Prop.Props[i].model)
				sPropStruct.oiObject[i] = CREATE_OBJECT(sMissionPlacementData.Prop.Props[i].model , sMissionPlacementData.Prop.Props[i].vCoords, FALSE, FALSE, TRUE)
				
				SET_UP_FMC_PROP(sPropStruct.oiObject[i], i)	
				
				ADD_MODEL_TO_CREATOR_BUDGET(sMissionPlacementData.Prop.Props[i].model)
				CREATE_FMMC_BLIP(sPropStruct.biObject[i], GET_ENTITY_COORDS(sPropStruct.oiObject[i]), HUD_COLOUR_PURPLE, "FMMC_B_5", 1)
				SET_ENTITY_LOD_DIST(sPropStruct.oiObject[i], 900000)
				iPropModelTimers[i] = GET_GAME_TIMER()
				
				// Add its custom prop to the list if it uses one
				IF IS_LONG_BIT_SET(sMissionPlacementData.Prop.Props[i].iBitset, ENUM_TO_INT(ePROPDATABITSET_CUSTOM_PROP_MODEL))
					INT iCustom
					BOOL bAdd = TRUE
					FOR iCustom = 0 TO ciMAX_CUSTOM_DEV_PROPS-1
						IF sMissionPlacementData.Prop.Props[i].model = g_FMMC_STRUCT.sCustomProps[iCustom].mnCustomPropName
							bAdd = FALSE
							BREAKLOOP
						ENDIF
					ENDFOR
					IF bAdd
						ADD_CUSTOM_PROP(sMissionPlacementData.Prop.Props[i].model)
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDFOR
	
	// Double check that they all exist
	FOR i = 0 TO (sMissionPlacementData.Prop.iCount - 1)
		IF NOT DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
			PRINTLN("Prop ", i, " Does not exist")
			
			// Update its model to a generic one as a failsafe if it's not being created
			IF GET_GAME_TIMER() - iPropModelTimers[i] > 15000
				sMissionPlacementData.Prop.Props[i].model = PROP_CONST_FENCE02B
				iPropModelTimers[i] = GET_GAME_TIMER()
				PRINTLN("CREATE_ALL_CREATOR_PROPS timed out creating prop ", i, " Switching to fence and restarting timer")
			ENDIF
			
			RETURN FALSE
		ENDIF
	ENDFOR
	
	// Unload the models
	REPEAT sMissionPlacementData.Prop.iCount i
		IF sMissionPlacementData.Prop.Props[i].model != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(sMissionPlacementData.Prop.Props[i].model)
		ENDIF
		iPropModelTimers[i] = 0
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_PROP_SELECTION_FROM_CUSTOM_MODEL(MODEL_NAMES mnPassed)
	INT i
	FOR i = 0 TO g_FMMC_STRUCT.iCurrentNumberOfDevProps - 1
		IF g_FMMC_STRUCT.sCustomProps[i].mnCustomPropName = mnPassed
			RETURN i+1
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

PROC GET_PROP_LIBRARY_AND_SELECTION_FROM_MODEL(structFMMC_MENU_ITEMS &sFMMCmenu)
	
	// Grab the model/libaray from the standard list
	sFMMCmenu.iPropType					= GET_PROP_LIBRARY_SELECTION_FROM_MODEL(sEditedPropPlacementData.model)
	sFMMCmenu.iPropLibrary				= GET_PROP_LIBRARY_FROM_MODEL(sEditedPropPlacementData.model)
	
	// If it's not in there update it to go into the custom library
	IF sFMMCmenu.iPropLibrary = -1
		sFMMCmenu.iPropType = GET_PROP_SELECTION_FROM_CUSTOM_MODEL(sEditedPropPlacementData.model)
		IF sFMMCmenu.iPropType > -1
			sFMMCmenu.iPropLibrary = PROP_LIBRARY_CUSTOM_PROPS
		ELIF g_FMMC_STRUCT.iCurrentNumberOfDevProps < ciMAX_CUSTOM_DEV_PROPS
			ADD_CUSTOM_PROP(sEditedPropPlacementData.model)
			sFMMCmenu.iPropType = g_FMMC_STRUCT.iCurrentNumberOfDevProps
			sFMMCmenu.iPropLibrary = PROP_LIBRARY_CUSTOM_PROPS
		ENDIF
	ENDIF
ENDPROC

// Main Prop Creation Func
// Props have loads of extra functionality and exceptions in the main creators and while this is cut down, it's messier than the other DO_X_CREATION functions
// If you're making a new type based on objects please base it on DO_MISSION_ENTITY_CREATION as it's much simpler
PROC DO_FMC_PROP_CREATION(	CREATION_VARS_STRUCT 		&sCurrentVarsStruct, 
							HUD_COLOURS_STRUCT 			&sHCS,
							structFMMC_MENU_ITEMS 		&sFMMCmenu, 
							FMMC_LOCAL_STRUCT 			&sFMMCData,
							FMMC_COMMON_MENUS_VARS 		&sFMMCendStage,
							FMMC_CAM_DATA				&sCamData,
							INT 						iMaximum = FMMC_MAX_NUM_FMC_PROPS)
	
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_PURPLE ,iR, iG, iB, iA)
	
	INT iProp
	FLOAT fMoveMent
	VECTOR vMin, vMax
	FLOAT fArea
	BOOL bBelowDisc = TRUE
	
	// Determine if the placement disk should be drawn above or below the prop based on its size
	MODEL_NAMES mnCurrent = GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType)
	REPLACE_CREATOR_PROP_WITH_NEW_VERSION(mnCurrent)
	IF mnCurrent != DUMMY_MODEL_FOR_SCRIPT
	AND NOT DOES_PROP_FIT_ON_WALLS(mnCurrent)
		GET_MODEL_DIMENSIONS(mnCurrent, vMin, vMax)
		fArea = (vMax.x - vMin.x) * (vMax.y - vMin.y)
		if fArea < 4.5
			bBelowDisc = FALSE
		ENDIF
	ENDIF
				
	HANDLE_MODEL_MEMORY_METER()
	
	// Determine the one we're working with based on if it's new or was picked up
	IF sFMMCMenu.iSelectedEntity = -1
		iProp = sMissionPlacementData.Prop.iCount
	ELSE
		iProp = sFMMCMenu.iSelectedEntity
		IF iProp >= iMaximum 
			iProp = iMaximum - 1
			sFMMCMenu.iSelectedEntity = iProp
		ENDIF
		
		// If you've picked up a prop, remove the one that was placed from the world so that it can be recreated as the placement one
		IF DOES_ENTITY_EXIST(sPropStruct.oiObject[iProp])
			
			IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(sPropStruct.viCoronaObj))
				DELETE_OBJECT(sPropStruct.viCoronaObj)
			ENDIF
			IF DOES_BLIP_EXIST(sPropStruct.biObject[iProp])
				REMOVE_BLIP(sPropStruct.biObject[iProp])
			ENDIF
			
			IF sMissionPlacementData.Prop.Props[iProp].model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_mp_spec_races_ammu_sign"))
				sMissionPlacementData.Prop.Props[iProp].model = INT_TO_ENUM(MODEL_NAMES, HASH("ar_prop_ar_ammu_sign"))
			ENDIF
			
			// Update the creation data to match that of the placed one
			sEditedPropPlacementData 			= sMissionPlacementData.Prop.Props[iProp]
			sPropStruct.viCoronaObj 			= sPropStruct.oiObject[iProp]
			sPropStruct.oiObject[iProp]			= NULL
			
			// Store out the prop struct so when you hit cancel, the picked up prop returns to its original state CANCEL_FROM_EDITING_FMC_PROP
			sCachedPropPlacementData = sMissionPlacementData.Prop.Props[iProp]
			PRINTLN("DO_PROP_CREATION, sCachedEditedPropPreEditSettings = ", sCachedPropPlacementData.vCoords)
			
			GET_PROP_LIBRARY_AND_SELECTION_FROM_MODEL(sFMMCmenu)
			
			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
			
			SET_ASSOCIATED_RULE_MENU_FROM_ENTITY(iProp, sFMMCmenu)
			sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_PROPS] = sFMMCmenu.iPropType
			sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_PROP_LIBRARY] = sFMMCmenu.iPropLibrary
			
			IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
				SET_ENTITY_COLLISION(sPropStruct.viCoronaObj, FALSE)
			ENDIF
			
			IF IS_MODEL_VALID(sEditedPropPlacementData.model)
				REQUEST_MODEL(sEditedPropPlacementData.model)
			ENDIF
			
			IF IS_MODEL_STUNT_PROP(sEditedPropPlacementData.model)
				SET_OBJECT_TINT_INDEX(sPropStruct.viCoronaObj, sFMMCmenu.iPropColour)
			ENDIF
			
			// Set the creation height to keep it in the air if it was placed there
			IF sEditedPropPlacementData.model != DUMMY_MODEL_FOR_SCRIPT
				GET_MODEL_DIMENSIONS(sEditedPropPlacementData.model, vMin, vMax)	
				FLOAT fPropHeightOffset = (vMax.z - vMin.z)/2
				FLOAT fHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(sPropStruct.viCoronaObj)
				
				IF fHeight - fPropHeightOffset > 0
					sFMMCmenu.fCreationHeightIncrease = (fHeight - fPropHeightOffset)
					PRINTLN("[CREATIONHEIGHT][4] sFMMCmenu.fCreationHeightIncrease = ", sFMMCmenu.fCreationHeightIncrease)
				ELSE
					PRINTLN("[CREATIONHEIGHT][5] Picking up prop which is on the floor")
					sFMMCmenu.fCreationHeightIncrease = 0.0
				ENDIF
			ENDIF
			
			sCurrentVarsStruct.fCreationHeading = sEditedPropPlacementData.fHeading
				
			REFRESH_MENU(sFMMCmenu)
			
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		ENDIF
	ENDIF
	
	// Entity counter/ too many placed UI
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PROPS)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_ACCEPT_BUTTON(sFMMCdata))
		AND sFMMCMenu.iSelectedEntity = -1	
			DO_TOO_MANY_ENTITY_ERROR(sCurrentVarsStruct, sPropStruct.flashColour, "FMMC_ER_TPO")
		ENDIF
	ENDIF
	IF sMissionPlacementData.Prop.iCount >= iMaximum 
	AND sFMMCMenu.iSelectedEntity = -1
		
		SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PROPS)
		IF sPropStruct.flashColour = HUDFLASHING_NONE
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.Prop.iCount, iMaximum, "FMMC_AB_13", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sPropStruct.flashColour, 0)
		ELSE
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.Prop.iCount, iMaximum, "FMMC_AB_13", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sPropStruct.flashColour, 500)
		ENDIF
		IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
			DELETE_OBJECT(sPropStruct.viCoronaObj)
		ENDIF
		sCurrentVarsStruct.eiCurProp = INT_TO_NATIVE(ENTITY_INDEX,-1)
		EXIT
	ELSE
		sPropStruct.flashColour = HUDFLASHING_NONE
		
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.Prop.iCount,  iMaximum,	"FMMC_AB_13", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, sPropStruct.flashColour, 0)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PROPS)
	ENDIF
	
	// Placement state machine
	SWITCH GET_CURRENT_GENERIC_CREATION_STAGE()
		
		// Wait stage to stall the creation of more if there are too many already
		CASE CREATION_STAGE_WAIT	
			IF sMissionPlacementData.Prop.iCount <= iMaximum
			AND iProp < iMaximum
				DELETE_CURRENT_CORONA_PROP_OBJECTS(sPropStruct, sFMMCmenu.iPropLibrary)
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_SET_UP)
			ENDIF
		BREAK
		
		// Request the model ready for creation
		CASE CREATION_STAGE_SET_UP
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
			IF NOT DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
							
				sEditedPropPlacementData.vCoords = sCurrentVarsStruct.vCoronaPos
				
				sPropStruct.iCurrentObjectToCreate = 0
				IF IS_VECTOR_IN_WORLD_BOUNDS(sEditedPropPlacementData.vCoords)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						sEditedPropPlacementData.fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
					ENDIF
					//Stick it on the ground
					FLOAT fGroundZ
					IF GET_GROUND_Z_FOR_3D_COORD(sEditedPropPlacementData.vCoords, fGroundZ)
						sEditedPropPlacementData.vCoords.z = fGroundZ
					ENDIF
					
					IF IS_MODEL_VALID(mnCurrent)
						REQUEST_MODEL(mnCurrent)
					ENDIF
					SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_MAKE)
				ENDIF
			ELSE
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_PLACE)
			ENDIF
		BREAK
		
		// Create the new placement prop for the corona
		// Most of this can be ignored and is carry over from the many many exceptions certain props need to work properly in the main creator
		CASE CREATION_STAGE_MAKE
		
			IF NOT DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
				IF IS_MODEL_VALID(mnCurrent)
					REQUEST_MODEL(mnCurrent)
					IF HAS_MODEL_LOADED(mnCurrent)
						PRINTLN("[RCC MISSION] [PLOWED] DO_PROP_CREATION - ",iProp," is being made.")
						
						VECTOR vNewPos, vNewRot
						vNewPos = sEditedPropPlacementData.vCoords + cfOBJECT_MOVEMENT_TO_STOP_754479
						vNewRot = sFMMCmenu.vLastPropRotation
						
						sEditedPropPlacementData.model = mnCurrent
						
						IF VDIST(vNewPos, <<0.0, 0.0, 0.0>>) < 1.0
							vNewPos = sCurrentVarsStruct.vCoronaPos
							PRINTLN("DO_PROP_CREATION - Almost created a prop at 0,0,0 - creating it at ", sCurrentVarsStruct.vCoronaPos, " instead")
						ENDIF
						
						sPropStruct.viCoronaObj = CREATE_OBJECT(mnCurrent, vNewPos, FALSE, FALSE)
						
						SET_ENTITY_HEADING(sPropStruct.viCoronaObj, sCurrentVarsStruct.fCreationHeading)
						sEditedPropPlacementData.fHeading = sCurrentVarsStruct.fCreationHeading
						
						IF NOT IS_VECTOR_ZERO(vNewRot)
							SET_ENTITY_ROTATION(sPropStruct.viCoronaObj, vNewRot)
						ENDIF
						
						IF SHOULD_OBJECT_BE_FROZEN(sPropStruct.viCoronaObj)
							FREEZE_ENTITY_POSITION(sPropStruct.viCoronaObj, TRUE)
						ENDIF
						SET_ENTITY_COLLISION(sPropStruct.viCoronaObj, FALSE)
						SET_ENTITY_ALPHA(sPropStruct.viCoronaObj, 0, FALSE)
						SET_ENTITY_LOD_DIST(sPropStruct.viCoronaObj, 900000)
						
						IF IS_MODEL_STUNT_PROP(sEditedPropPlacementData.model)
							SET_OBJECT_TINT_INDEX(sPropStruct.viCoronaObj, sFMMCmenu.iPropColour)
						ENDIF
						
						IF sEditedPropPlacementData.model = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_boxpile_01"))
						OR sEditedPropPlacementData.model = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_impexp_boxpile_02"))
						OR sEditedPropPlacementData.model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_01"))
						OR sEditedPropPlacementData.model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_02"))
						OR sEditedPropPlacementData.model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_03"))
						OR sEditedPropPlacementData.model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_track_wall"))
						OR sEditedPropPlacementData.model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_tube_wall"))
						OR sEditedPropPlacementData.model = INT_TO_ENUM(MODEL_NAMES, HASH("sr_mp_spec_races_ammu_sign"))
						OR sEditedPropPlacementData.model = INT_TO_ENUM(MODEL_NAMES, HASH("ar_Prop_AR_Ammu_Sign"))
						OR IS_INFLATABLE_STUNT_PROP(sEditedPropPlacementData.model)
						OR sEditedPropPlacementData.model = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Track_ExShort"))
						OR sEditedPropPlacementData.model = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Track_Short"))
							SET_ENTITY_PROOFS(sPropStruct.viCoronaObj, FALSE, FALSE, FALSE, FALSE, FALSE)
							SET_ENTITY_INVINCIBLE(sPropStruct.viCoronaObj,FALSE)
							SET_DISABLE_FRAG_DAMAGE(sPropStruct.viCoronaObj, FALSE)
							FREEZE_ENTITY_POSITION(sPropStruct.viCoronaObj, FALSE)
							PRINTLN("[RCC MISSION] [PLOWED] DO_PROP_CREATION - ",iProp," is set to be fraggable")
						ENDIF
						
						sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
						sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL
						SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_PLACE)
					ENDIF
				ENDIF
			ELSE
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_PLACE)
			ENDIF
		BREAK
		
		// Do the placement of the prop, updating it's position/rotation/model/etc based on user input
		CASE CREATION_STAGE_PLACE
			sHCS.hcCurrentCoronaColour = sHCS.hcColour[CREATION_TYPE_OBJECTS]
			sCurrentVarsStruct.fCheckPointSize = ciOBJECT_CHECK_POINT_SIZE
			fMoveMent = GET_PROP_ADDITIONAL_OFFSET(sEditedPropPlacementData.model)
			sEditedPropPlacementData.model = mnCurrent
			
			IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
				SET_UP_MODEL_MEMORY_METER(GET_ENTITY_MODEL(sPropStruct.viCoronaObj), sCurrentVarsStruct)
			ENDIF
			
			// Do the override position/rotation stuff to grab the coords from those in the menu
			HANDLE_ENTITY_CREATION_OVERRIDES(sFMMCMenu, sFMMCData, sCurrentVarsStruct, sPropStruct.viCoronaObj, sEditedPropPlacementData.iBitset[GET_LONG_BITSET_INDEX(iProp)], ENUM_TO_INT(ePROPDATABITSET_OVERRIDEN_POSITION), ENUM_TO_INT(ePROPDATABITSET_OVERRIDEN_ROTATION), FALSE, FALSE, FALSE)
			
			// Has to pass this in as MOVE_LAST_PLACED_PROP is a Mission Creator function that freemode is sharing.
			TRAIN_CREATION_STRUCT sTrainBlank
			
			// MOVE_LAST_PLACED function for the props from the original creators to update it's position/rotation and check if it's been placed
			// This func also has a mountain of extra functionality for things like snapping and special case prop behaviour
			IF MOVE_LAST_PLACED_PROP(sFMMCmenu, 
										sPropStruct.viCoronaObj, 
										sEditedPropPlacementData.vCoords,
										sEditedPropPlacementData.vRotation,
										sEditedPropPlacementData.fHeading, 
										sCurrentVarsStruct,
										sFMMCData,
										sPropStruct,
										sVehStruct,
										sCamData,
										sPedStruct,
										sTrainBlank,
										fMoveMent,
										TRUE,
										IS_PROP_WATER_TYPE(sEditedPropPlacementData.model),
										IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona),
										bBelowDisc,
										IS_LONG_BIT_SET(sEditedPropPlacementData.iBitset, ENUM_TO_INT(ePROPDATABITSET_OVERRIDEN_POSITION)),
										IS_LONG_BIT_SET(sEditedPropPlacementData.iBitset, ENUM_TO_INT(ePROPDATABITSET_OVERRIDEN_ROTATION)))
				
				// Copy the data from the edited stuct into the actual array
				sMissionPlacementData.Prop.Props[iProp] = sEditedPropPlacementData
				
				sMissionPlacementData.Prop.Props[iProp].iInterior = GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(sEditedPropPlacementData.vCoords)
				PRINTLN("DO_PROP_CREATION - Setting sMissionPlacementData.Prop.Props[",iProp,"].iInterior to ", sMissionPlacementData.Prop.Props[iProp].iInterior)
				
				// Move onto creation stage done as it's been placed
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_DONE)
				
				sPropStruct.iCachedLoopEnd = 0
			ENDIF
			
			//go back through set up if the object model has changed
			IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
				IF mnCurrent != GET_ENTITY_MODEL(sPropStruct.viCoronaObj)
					IF DOES_BLIP_EXIST(sPropStruct.biObject[iProp])
						REMOVE_BLIP(sPropStruct.biObject[iProp])
					ENDIF
					MODEL_NAMES oldModel
					oldModel = GET_ENTITY_MODEL(sPropStruct.viCoronaObj)
					DELETE_OBJECT(sPropStruct.viCoronaObj)			
					SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
					SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_SET_UP)
				ENDIF
			ENDIF
			
		BREAK
		
		// Update the prop you've just placed, increase the counter and reset for the next one
		CASE CREATION_STAGE_DONE
			
			START_PLACEMENT_COOLDOWN_TIMER(sCurrentVarsStruct)
			
			sPropStruct.fCachedHeight = sFMMCmenu.fCreationHeightIncrease
			
			IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
				IF NOT IS_BIT_SET(sFMMCData.iBitSet, bOnTheGround)
					SET_CAMERA_ABOVE_PLACED_PROP(sCamData, sPropStruct.viCoronaObj)
				ENDIF
				
				MISSION_PLACEMENT_DATA_PROPS sClearStruct
				sEditedPropPlacementData = sClearStruct
				
				IF sFMMCmenu.iPropLibrary = PROP_LIBRARY_CUSTOM_PROPS
					SET_LONG_BIT(sMissionPlacementData.Prop.Props[iProp].iBitset, ENUM_TO_INT(ePROPDATABITSET_CUSTOM_PROP_MODEL))
				ELSE
					CLEAR_LONG_BIT(sMissionPlacementData.Prop.Props[iProp].iBitset, ENUM_TO_INT(ePROPDATABITSET_CUSTOM_PROP_MODEL))
				ENDIF
				
				MODEL_NAMES mnProp 
				mnProp = GET_ENTITY_MODEL(sPropStruct.viCoronaObj)
				REPLACE_CREATOR_PROP_WITH_NEW_VERSION(mnProp)
				ADD_MODEL_TO_CREATOR_BUDGET(mnProp)
				sPropStruct.oiObject[iProp] = sPropStruct.viCoronaObj
				SET_ENTITY_COORDS(sPropStruct.oiObject[iProp], GET_ENTITY_COORDS(sPropStruct.oiObject[iProp])
										+ ADJUST_PROP_ORIGIN_POINT_FOR_CREATOR(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType), sPropStruct.oiObject[iProp]))
				sMissionPlacementData.Prop.Props[iProp].vCoords = GET_ENTITY_COORDS(sPropStruct.oiObject[iProp])
				sMissionPlacementData.Prop.Props[iProp].vRotation = GET_ENTITY_ROTATION(sPropStruct.oiObject[iProp])
				PRINTLN("DO_PROP_CREATION - Placed Prop ", iProp, "  with a rotation of ", sMissionPlacementData.Prop.Props[iProp].vRotation)
				sPropStruct.viCoronaObj = NULL
				CREATE_FMMC_BLIP(sPropStruct.biObject[iProp], GET_ENTITY_COORDS(sPropStruct.oiObject[iProp]), HUD_COLOUR_PURPLE, "FMMC_B_5", 1)
				SET_ENTITY_COLLISION(sPropStruct.oiObject[iProp], TRUE)
				RESET_ENTITY_ALPHA(sPropStruct.oiObject[iProp])
				IF sFMMCMenu.iSelectedEntity = -1
					sMissionPlacementData.Prop.iCount++
					IF sMissionPlacementData.Prop.iCount > iMaximum
						sMissionPlacementData.Prop.iCount = iMaximum
					ENDIF
				ENDIF
				sCurrentVarsStruct.fCreationHeading = sMissionPlacementData.Prop.Props[iProp].vRotation.z
			ENDIF
			
			sFMMCendStage.bMajorEditOnLoadedMission = TRUE
			sCurrentVarsStruct.creationStats.bMadeAChange = TRUE
			
			CLEAR_LONG_BIT(sEditedPropPlacementData.iBitset, ENUM_TO_INT(ePROPDATABITSET_OVERRIDEN_POSITION))
			CLEAR_LONG_BIT(sEditedPropPlacementData.iBitset, ENUM_TO_INT(ePROPDATABITSET_OVERRIDEN_ROTATION))
			
			CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, GET_ENTITY_COORDS(sPropStruct.oiObject[iProp]))
			
			IF bBelowDisc
				UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, sPropStruct.viCoronaObj, FALSE)
			ELSE
				UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, sPropStruct.viCoronaObj, TRUE, FALSE, 0.0)
			ENDIF
			
			sFMMCmenu.iPropBitset = 0
			
			SET_LONG_BIT(sPropStruct.iBS_Editted, iProp)
						
			sFMMCmenu.iSwitchCam = iProp
			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
			
			IF sFMMCMenu.iSelectedEntity != -1
				SETUP_DELETED_PROP_CHECKS(sCurrentVarsStruct, iLocalBitset, sFMMCMenu)
								
				sFMMCMenu.iSelectedEntity = -1
				
			ENDIF
			
			IF sFMMCMenu.sActiveMenu = eFmmc_FMC_PROP_MENU
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
				
				SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_PROPS)
			ELSE
				SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			ENDIF
			
			IF sFMMCMenu.sActiveMenu = eFMMC_GENERIC_OVERRIDE_POSITION
			OR sFMMCMenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION
				sFMMCmenu.iCurrentSelection = 0
			ENDIF
			
			REFRESH_MENU(sFMMCMenu)
			
		BREAK
		
	ENDSWITCH
		
	IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
	AND (DOES_PROP_FIT_ON_WALLS(GET_ENTITY_MODEL(sPropStruct.viCoronaObj)) OR IS_LONG_BIT_SET(sEditedPropPlacementData.iBitset, ENUM_TO_INT(ePROPDATABITSET_OVERRIDEN_POSITION)) OR HAS_CURRENT_PROP_BEEN_FREE_ROTATED(sFMMCmenu))
		UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, sPropStruct.viCoronaObj, TRUE, FALSE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
	ELSE
		IF bBelowDisc
			UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, sPropStruct.viCoronaObj, FALSE)
		ELSE
			UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, sPropStruct.viCoronaObj, TRUE)
		ENDIF
	ENDIF		
		
ENDPROC

PROC DELETE_FMC_PLACED_PROP(INT iProp)

	INT i
	MISSION_PLACEMENT_DATA_PROPS sTempPropStruct
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	// Remove the entity/blip
	IF DOES_BLIP_EXIST(sPropStruct.biObject[iProp])
		REMOVE_BLIP(sPropStruct.biObject[iProp])
	ENDIF
	IF DOES_ENTITY_EXIST(sPropStruct.oiObject[iProp])
		DELETE_OBJECT(sPropStruct.oiObject[iProp])
	ENDIF
	
	// Move all the entries down
	FOR i = iProp+1 TO sMissionPlacementData.Prop.iCount-1
		sPropStruct.biObject[i-1] = sPropStruct.biObject[i]
		sPropStruct.oiObject[i-1] = sPropStruct.oiObject[i]
		sMissionPlacementData.Prop.Props[i-1]= sMissionPlacementData.Prop.Props[i]
	ENDFOR
	
	// Reduce the array
	sMissionPlacementData.Prop.iCount--
	
	// Clear the last entry
	sMissionPlacementData.Prop.Props[sMissionPlacementData.Prop.iCount] = sTempPropStruct
	sPropStruct.biObject[sMissionPlacementData.Prop.iCount] = NULL
	sPropStruct.oiObject[sMissionPlacementData.Prop.iCount] = NULL
	
	// Update references
	UPDATE_TAGS(iProp, eTAGTYPE_PROP)
	UPDATE_SECURITY_CAMERAS(iProp)
	UPDATE_TARGETS(ET_OBJECT, iProp)
	
ENDPROC

FUNC BOOL FMC_CAN_DELETE_CUSTOM_PROP(structFMMC_MENU_ITEMS &sFMMCmenu, INT iProp)
	IF sFMMCMenu.iPropType > 0
	AND sFMMCMenu.iPropType < ciMAX_CUSTOM_DEV_PROPS
		IF g_FMMC_STRUCT.sCustomProps[sFMMCMenu.iPropType-1].mnCustomPropName = sMissionPlacementData.Prop.Props[iProp].model
			IF g_FMMC_STRUCT.sCustomProps[sFMMCMenu.iPropType-1].mnCustomPropName != DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("FMC_CAN_DELETE_CUSTOM_PROP - Model Selected sFMMCMenu.iPropType: ", sFMMCMenu.iPropType, " model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT.sCustomProps[sFMMCMenu.iPropType].mnCustomPropName),  " iProp ", iProp, " Model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn))
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC HANDLE_FMC_CUSTOM_PROP_DELETE_INPUT(structFMMC_MENU_ITEMS &sFMMCmenu, INT iProp, BOOL &bDelete)
	IF NOT FMC_CAN_DELETE_CUSTOM_PROP(sFMMCmenu, iProp)
		bDelete = FALSE
		PRINTLN("HANDLE_FMC_CUSTOM_PROP_DELETE_INPUT - bDelete = FALSE")
	ENDIF
ENDPROC

// Run through the every frame checks and logic for the placed props.
// Like DO_FMC_PROP_CREATION this has inherited a lot of the extra logic props in the main creators use
// so if you're making a new type based on objects it's better to look at MAINTAIN_PLACED_MISSION_ENTITIES
PROC MAINTAIN_PLACED_FMC_PROPS(CREATION_VARS_STRUCT &sCurrentVarsStruct, structFMMC_MENU_ITEMS &sFMMCmenu, FMMC_LOCAL_STRUCT &sFMMCdata, HUD_COLOURS_STRUCT &sHCS,
							FMMC_COMMON_MENUS_VARS 	&sFMMCendStage)
	
	BOOL bDelete = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_DELETE_BUTTON())
	BOOL bPlaneCorona = TRUE
	
	// If you delete all props this will remove the entities on top of them.
	IF IS_BIT_SET(iLocalBitSet, biDeleteAllProps)
		IF sMissionPlacementData.Prop.iCount = 0
			CLEAR_BIT(iLocalBitSet, biDeleteAllProps)
			sCurrentVarsStruct.creationStats.bMadeAChange = TRUE
			sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
			sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
			sCurrentVarsStruct.iSubOptionStatus = FMMC_SUB_DEFAULT
			SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
			sCurrentVarsStruct.stiScreenCentreLOS = NULL
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			DO_SCREEN_FADE_IN(500)	
			sCurrentVarsStruct.bSwitching = FALSE
			REFRESH_MENU(sFMMCmenu)
			PRINTLN("MAINTAIN_PLACED_FMC_PROPS - Delete all props FINISHED")
		ENDIF
		
		EXIT
		
	ENDIF
	
	IF sFMMCmenu.iEntityCreation != CREATION_TYPE_PROPS
		CLEAR_PROP_FREE_ROTATION(sFMMCmenu)
		IF DOES_ENTITY_EXIST(sPropStruct.viGroupParent)
			DELETE_OBJECT(sPropStruct.viGroupParent)
		ENDIF
	ENDIF
	
	INT iProp
	
	// Loop over all placed props
	FOR iProp = 0 TO (sMissionPlacementData.Prop.iCount - 1)
		
		HANDLE_FMC_CUSTOM_PROP_DELETE_INPUT(sFMMCmenu, iProp, bDelete)
		
		IF DOES_ENTITY_EXIST(sPropStruct.oiObject[iProp])
			
			DRAW_ENTITY_LABEL(CREATION_TYPE_PROPS, iProp, VECTOR_ZERO)
			TEXT_LABEL_63 tl63 = "Prop: "
			tl63 += iProp
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(sPropStruct.oiObject[iProp], tl63, 0.5)
			
			IF IS_ENTITY_IN_CORONA(sPropStruct.oiObject[iProp], sCurrentVarsStruct, 1.0, iProp, CREATION_TYPE_PROPS, -1, sFMMCmenu.iSelectedTeam)
				IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PROPS
					sFMMCMenu.iCopyFromEntity = iProp
					DRAW_ON_SCREEN_ENTITY_INFO(sFMMCmenu, sFMMCMenu.iCopyFromEntity, CREATION_TYPE_PROPS)
				ENDIF
			ENDIF
			
			// Check if it's in the corona
			IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_PROPS, iProp, sPropStruct.oiObject[iProp], bPlaneCorona, HUD_COLOUR_YELLOW, sFMMCdata, sFMMCendStage)
			
				// Pickup
				IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
					
					REMOVE_MODEL_FROM_CREATOR_BUDGET(sMissionPlacementData.Prop.Props[iProp].model)
					PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_PROPS, iProp)
					CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
					
				// Delete
				ELIF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
					
					IF sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_POSITION
						CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
						PRINTLN("MAINTAIN_PLACED_FMC_PROPS - PREVENTING DELETE WHILE IN OVERRIDE MENU")
					ELSE
						REMOVE_MODEL_FROM_CREATOR_BUDGET(sMissionPlacementData.Prop.Props[iProp].model)
						
						//Axis Aligned Bounding Box
						GET_AXIS_ALIGNED_BOX_VALUES_FOR_PROP(sPropStruct.oiObject[iProp], sFMMCmenu.vDeletedPropBoxMin, sFMMCMenu.vDeletedPropBoxMax)
						SETUP_DELETED_PROP_CHECKS(sCurrentVarsStruct, iLocalBitSet, sFMMCMenu)
						
						DELETE_FMC_PLACED_PROP(iProp)
						sCurrentVarsStruct.creationStats.bMadeAChange = TRUE
						sFMMCEndStage.bMajorEditOnLoadedMission = TRUE
						sCurrentVarsStruct.iEntityRemoved = iProp
						CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
						REFRESH_MENU(sFMMCMenu)
						sFMMCMenu.iSelectedEntity = -1
						CLEAR_BIT(iLocalBitSet, biEntityInCorona)
						PRINTLN("MAINTAIN_PLACED_FMC_PROPS - DELETING!")
					ENDIF
				ENDIF
				
				bPlaneCorona = TRUE
				
			ELSE
				
				sCurrentVarsStruct.bResetUpHelp = TRUE
				
				IF bPlaneCorona = FALSE
					sCurrentVarsStruct.tl15SelectedName = GET_CREATOR_NAME_FOR_PROP_MODEL(sMissionPlacementData.Prop.Props[iProp].model)
					IF NOT IS_STRING_NULL_OR_EMPTY(sCurrentVarsStruct.tl15SelectedName)
						PRINT_SELECTED_ENTITY_NAME_AS_FLOATING_HELP(sCurrentVarsStruct, sCurrentVarsStruct.tl15SelectedName)
					ENDIF
					bPlaneCorona = TRUE
				ENDIF
			ENDIF
			
			// Recreate the blip if needed
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_TARGETS
				MAINTAIN_TARGET_BLIPS(sFMMCmenu, ET_OBJECT, iProp)
			ELSE
				IF NOT DOES_BLIP_EXIST(sPropStruct.biObject[iProp])
				AND sCycleCamStruct.BlockBlipsState = eBLOCK_BLIPS_NONE
					CREATE_FMMC_BLIP(sPropStruct.biObject[iProp], GET_ENTITY_COORDS(sPropStruct.oiObject[iProp]), HUD_COLOUR_PURPLE, "FMMC_B_5", 1)
				ENDIF
			ENDIF
			
		ENDIF
	ENDFOR
	
	IF sFMMCmenu.iPropLibrary = PROP_LIBRARY_CUSTOM_PROPS
	AND sFMMCMenu.iPropType > 0
	AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciFMC_PROP_TYPE
	AND bDelete
		PRINTLN("MAINTAIN_PLACED_FMC_PROPS - Calling REMOVE_CUSTOM_PROP: ", (sFMMCMenu.iPropType - 1))
		REMOVE_CUSTOM_PROP(sFMMCmenu, sFMMCMenu.iPropType - 1)
	ENDIF
ENDPROC

/////////////////////////////////////////////
///    GOTO LOCATIONS
///    

// Placement function for Gotos
// This is more stripped down than the usual ones with entities involved
PROC DO_FMC_LOCATION_CREATION(	SELECTION_DETAILS_STRUCT 	&sSelDetails,
								CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
								structFMMC_MENU_ITEMS 		&sFMMCmenu,
								FMMC_LOCAL_STRUCT 			&sFMMCData,
								INT 						iMaximum = MAX_NUM_GOTO_LOCATIONS)

	INT iR, iG, iB, iA
	INT iGoToPoint
	FLOAT fRadius
	
	// Determine which goto we're working with
	IF sFMMCMenu.iSelectedEntity = -1
	
		iGoToPoint = GET_NUMBER_OF_FMC_GOTOS()
		
	ELSE
	
		iGoToPoint = sFMMCMenu.iSelectedEntity
		
		IF iGoToPoint >= iMaximum 
			iGoToPoint = iMaximum - 1
			sFMMCMenu.iSelectedEntity = iGoToPoint
		ENDIF
		
		// If you've picked up a goto, remove the blip of the one that was in the world so that it can be recreated as the placement one
		IF DOES_BLIP_EXIST(sLocStruct.biGoToBlips[iGoToPoint])
		
			REMOVE_BLIP(sLocStruct.biGoToBlips[iGoToPoint])
			
			// Update the edited goto data to that of the one you've just picked up
			sEditedGoToPlacementData = sMissionPlacementData.GoToPoint.Locations[iGoToPoint]
			
			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
			
		ENDIF
		
	ENDIF
	
	// Error for if you try to place one when too many already exist
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_GOTO_LOCATIONS)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCMenu)
		AND sFMMCMenu.iSelectedEntity = -1
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			sCurrentVarsStruct.bDisplayFailReason = TRUE
			sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			sCurrentVarsStruct.sFailReason = "FMMC_HELP9CL"
			PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
			PRINTSTRING("MOVE_LAST_PLACED_VEHICLE ERROR SOUND EFFECT 4!!!")PRINTNL()
		ENDIF
	ENDIF
	
	// Ped counter UI
	IF GET_NUMBER_OF_FMC_GOTOS() >= iMaximum
	AND iGoToPoint >= iMaximum
		sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
		sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_GOTO_LOCATIONS )
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_FMC_GOTOS(),  iMaximum,	"FMMC_AB_09", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_FLASHRED, 500)
		EXIT
	ELSE
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_FMC_GOTOS(),  iMaximum,	"FMMC_AB_09", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_GOTO_LOCATIONS )
	ENDIF
	
	// Main creation state machine
	SWITCH sSelDetails.iSwitchingINT
	
		// Wait stage to stall the creation of more if there are too many already
		CASE CREATION_STAGE_WAIT
			IF iGoToPoint < iMaximum
				// We skip straight to placement on gotos as they don't need to create anything
				sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		
		// Do the placement of the goto, updating it's position based on user input
		CASE CREATION_STAGE_PLACE
		
			FLOAT fHeading
			// Checks if the place button has been pressed and that it's a valid place to go
			IF MOVE_LAST_PLACED_GO_TO_POINT(	sFMMCmenu, 
												sEditedGoToPlacementData.vCoords,
												fHeading,
												sCurrentVarsStruct, sFMMCData, 
												iGoToPoint)
				
				PRINTLN("sSelDetails.iSwitchingINT = CREATION_STAGE_DONE")
				sSelDetails.iSwitchingINT = CREATION_STAGE_DONE
				
			ENDIF
			
			// Update the position
			sEditedGoToPlacementData.vCoords = sCurrentVarsStruct.vCoronaPos
			
			GET_HUD_COLOUR(HUD_COLOUR_YELLOW,iR, iG, iB, iA)
			
			// Draw the goto area
			fRadius = sEditedGoToPlacementData.fRadius
			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, sEditedGoToPlacementData.vCoords, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<fRadius, fRadius, fRadius>>, iR, iG, iB, 100)
			
		BREAK
		
		// Update the goto you've just placed, increase the counter and reset for the next one
		CASE CREATION_STAGE_DONE
		
			sMissionPlacementData.GoToPoint.Locations[iGoToPoint]	= sEditedGoToPlacementData
			PRINTLN("[LOCS] Creating goto ",iGoToPoint," with pos/rad of ", sEditedGoToPlacementData.vCoords," / ", sEditedGoToPlacementData.fRadius)
			
			START_PLACEMENT_COOLDOWN_TIMER(sCurrentVarsStruct)
			
			CREATE_FMMC_BLIP(sLocStruct.biGoToBlips[iGoToPoint], sMissionPlacementData.GoToPoint.Locations[iGoToPoint].vCoords, HUD_COLOUR_YELLOW, "FMMC_B_11", 1)
			
			SET_BIT(sLocStruct.iBS_Editted, iGoToPoint)
			
			CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)
			
			IF sFMMCMenu.iSelectedEntity = -1
				RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			ENDIF
			sFMMCMenu.iSelectedEntity = -1
			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
			
		BREAK
		
	ENDSWITCH
	
	UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)	
	
ENDPROC

PROC DELETE_FMC_PLACED_GOTO(INT iGoto)

	INT i
	MISSION_PLACEMENT_DATA_GOTO_LOCATION sTempGoToStruct
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	// Remove the entity/blip
	IF DOES_BLIP_EXIST(sLocStruct.biGoToBlips[iGoto])
		REMOVE_BLIP(sLocStruct.biGoToBlips[iGoto])
	ENDIF
	
	// Move all the entries down
	FOR i = iGoto+1 TO MAX_NUM_GOTO_LOCATIONS-1
		sLocStruct.biGoToBlips[i-1] = sLocStruct.biGoToBlips[i]
		sMissionPlacementData.GoToPoint.Locations[i-1]= sMissionPlacementData.GoToPoint.Locations[i]
	ENDFOR
	
	// Clear the last entry
	sMissionPlacementData.GoToPoint.Locations[MAX_NUM_GOTO_LOCATIONS-1] = sTempGoToStruct
	sLocStruct.biGoToBlips[MAX_NUM_GOTO_LOCATIONS-1] = NULL
	
ENDPROC

PROC MAINTAIN_PLACED_FMC_LOCATIONS(	CREATION_VARS_STRUCT &sCurrentVarsStruct,
									structFMMC_MENU_ITEMS &sFMMCmenu,
									HUD_COLOURS_STRUCT &sHCS,
									FMMC_LOCAL_STRUCT &sFMMCdata,
									FMMC_COMMON_MENUS_VARS &sFMMCendStage)

	INT i, iR, iG, iB, iA
	BOOL bPlaneCorona = TRUE
	BOOL bDrawCorona = (sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION AND sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION)
	
	// Loop over all gotos
	FOR i = 0 TO MAX_NUM_GOTO_LOCATIONS-1
	
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.GoToPoint.Locations[i].vCoords)
			IF bDrawCorona
				
				// Check if it's in the corona
				IF IS_FMC_ENTITY_IN_CORONA(sFMMCmenu, NULL, sCurrentVarsStruct)
					IF sFMMCmenu.iEntityCreation = CREATION_TYPE_GOTO_LOC
						sFMMCMenu.iCopyFromEntity = i
						DRAW_ON_SCREEN_ENTITY_INFO(sFMMCmenu, sFMMCMenu.iCopyFromEntity, CREATION_TYPE_GOTO_LOC)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.iEntityCreation != CREATION_TYPE_GOTO_LOC
				OR sFMMCMenu.iSelectedEntity != i
				
					// Check if it's in the corona
					IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_GOTO_LOC, i, NULL, bPlaneCorona, GET_GOTO_LOC_HUD_COLOUR(i), sFMMCdata, sFMMCendStage)
					
						//Pickup
						IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
							PRINTLN("MAINTAIN_PLACED_FMC_LOCATIONS, biPickupButtonPressed ", i)
							PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_GOTO_LOC, i)
							CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
						
						//Delete
						ELIF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
							PRINTLN("MAINTAIN_PLACED_FMC_LOCATIONS, biDeleteButtonPressed ", i)
							sCurrentVarsStruct.iEntityRemoved = i
							DELETE_FMC_PLACED_GOTO(i)
							CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
							RELOOP
						ENDIF
					ENDIF
					
					GET_HUD_COLOUR(HUD_COLOUR_YELLOWLIGHT,iR, iG, iB, iA)
					
					FLOAT fRad = sMissionPlacementData.GoToPoint.Locations[i].fRadius
					
					// Draw the selection zone in the centre of the goto if you're in the goto creation type
					IF sFMMCmenu.iEntityCreation = CREATION_TYPE_GOTO_LOC
						DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_CYLINDER, sMissionPlacementData.GoToPoint.Locations[i].vCoords, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<ciSTART_CHECK_POINT_SIZE, ciSTART_CHECK_POINT_SIZE, ciSTART_CHECK_POINT_SIZE>>, iR, iG, iB,iA)
					ENDIF
					
					// Draw the area that represents the goto
					DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, sMissionPlacementData.GoToPoint.Locations[i].vCoords, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<fRad, fRad, fRad>>, iR, iG, iB, 100, FALSE, FALSE, EULER_XYZ, FALSE, NULL_STRING(),NULL_STRING(),sFMMCmenu.iGoToLocIsAerial = 0)
					
					// Give it a blip
					IF NOT DOES_BLIP_EXIST(sLocStruct.biGoToBlips[i])
						CREATE_FMMC_BLIP(sLocStruct.biGoToBlips[i], sMissionPlacementData.GoToPoint.Locations[i].vCoords, HUD_COLOUR_YELLOW, "FMMC_B_11", 1)
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDFOR
ENDPROC

/////////////////////////////////////////////
///    PORTALS
///    

PROC DO_FMC_PORTAL_CREATION(	SELECTION_DETAILS_STRUCT 	&sSelDetails,
								CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
								structFMMC_MENU_ITEMS 		&sFMMCmenu,
								FMMC_LOCAL_STRUCT 			&sFMMCData,
								INT 						iMaximum = MAX_NUM_MISSION_PORTALS)

	INT iR, iG, iB, iA
	INT iPortal
	FLOAT fRadius
	
	// Determine which goto we're working with
	IF sFMMCMenu.iSelectedEntity = -1
	
		iPortal = GET_NUMBER_OF_FMC_PORTALS()
		
	ELSE
	
		iPortal = sFMMCMenu.iSelectedEntity
		
		IF iPortal >= iMaximum 
			iPortal = iMaximum - 1
			sFMMCMenu.iSelectedEntity = iPortal
		ENDIF
		
		// If you've picked up a goto, remove the blip of the one that was in the world so that it can be recreated as the placement one
		//IF DOES_BLIP_EXIST(sLocStruct.biPortalBlips[iPortal])
		
		//	REMOVE_BLIP(sLocStruct.biGoToBlips[iPortal])
			
			// Update the edited goto data to that of the one you've just picked up
			//sEditedPortalPlacementData = sMissionPlacementData.Portal[iPortal]
			
			//sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
			
		//ENDIF
		
	ENDIF
	
	// Error for if you try to place one when too many already exist
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_GOTO_LOCATIONS)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCMenu)
		AND sFMMCMenu.iSelectedEntity = -1
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			sCurrentVarsStruct.bDisplayFailReason = TRUE
			sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			sCurrentVarsStruct.sFailReason = "FMMC_HELP9CL"
			PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
			PRINTSTRING("MOVE_LAST_PLACED_VEHICLE ERROR SOUND EFFECT 4!!!")PRINTNL()
		ENDIF
	ENDIF
	
	// Ped counter UI
	IF GET_NUMBER_OF_FMC_PORTALS() >= iMaximum
	AND iPortal >= iMaximum
		sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
		sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_GOTO_LOCATIONS )
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_FMC_GOTOS(),  iMaximum,	"FMMC_AB_09", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_FLASHRED, 500)
		EXIT
	ELSE
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_FMC_GOTOS(),  iMaximum,	"FMMC_AB_09", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_GOTO_LOCATIONS )
	ENDIF
	
	// Main creation state machine
	SWITCH sSelDetails.iSwitchingINT
	
		// Wait stage to stall the creation of more if there are too many already
		CASE CREATION_STAGE_WAIT
			IF iPortal < iMaximum
				// We skip straight to placement on gotos as they don't need to create anything
				sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		
		// Do the placement of the goto, updating it's position based on user input
		CASE CREATION_STAGE_PLACE
		
			FLOAT fHeading
			// Checks if the place button has been pressed and that it's a valid place to go
			IF MOVE_LAST_PLACED_GO_TO_POINT(	sFMMCmenu, 
												sEditedPortalPlacementData.vCoord,
												fHeading,
												sCurrentVarsStruct, sFMMCData, 
												iPortal)
				
				PRINTLN("sSelDetails.iSwitchingINT = CREATION_STAGE_DONE")
				sSelDetails.iSwitchingINT = CREATION_STAGE_DONE
				
			ENDIF
			
			// Update the position
			sEditedPortalPlacementData.vCoord = sCurrentVarsStruct.vCoronaPos
			
			IF GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(sEditedPortalPlacementData.vCoord) != (-1)
				SET_ARRAYED_BIT(sEditedPortalPlacementData.iBitset, ENUM_TO_INT(ePORTALDATABITSET_INSIDE_INTERIOR))
				PRINTLN("DO_FMC_PORTAL_CREATION - Setting ePORTALDATABITSET_INSIDE_INTERIOR for portal ",iPortal)
			ENDIF
			
			GET_HUD_COLOUR(HUD_COLOUR_ORANGE,iR, iG, iB, iA)
			
			// Draw the goto area
			fRadius = 0.5
			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_CYLINDER, sEditedPortalPlacementData.vCoord, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<fRadius, fRadius, fRadius>>, iR, iG, iB, 100)
			
		BREAK
		
		// Update the goto you've just placed, increase the counter and reset for the next one
		CASE CREATION_STAGE_DONE
		
			sMissionPlacementData.Portal[iPortal]	= sEditedPortalPlacementData
			PRINTLN("[LOCS] Creating portal ",iPortal," with pos ", sEditedPortalPlacementData.vCoord)
			
			START_PLACEMENT_COOLDOWN_TIMER(sCurrentVarsStruct)
			
			//CREATE_FMMC_BLIP(sLocStruct.biGoToBlips[iGoToPoint], sMissionPlacementData.GoToPoint.Locations[iGoToPoint].vCoords, HUD_COLOUR_YELLOW, "FMMC_B_11", 1)
			
			SET_BIT(sLocStruct.iBS_Editted, iPortal)
			
			CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)
			
			IF sFMMCMenu.iSelectedEntity = -1
				RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			ENDIF
			
			// url:bugstar:7467489 - Freemode Creator - Can we add the ability to pick up and edit an already placed portal please.	
			sFMMCMenu.iSelectedEntity = -1 // Reactivated extisting implementation
			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
			
		BREAK
		
	ENDSWITCH
	
	UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)	
	
ENDPROC

PROC DELETE_FMC_PLACED_PORTAL(INT iGoto)

	INT i
	MISSION_PLACEMENT_DATA_PORTALS sTempPortalStruct
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
//	// Remove the entity/blip
//	IF DOES_BLIP_EXIST(sLocStruct.biGoToBlips[iGoto])
//		REMOVE_BLIP(sLocStruct.biGoToBlips[iGoto])
//	ENDIF
	
	// Move all the entries down
	FOR i = iGoto+1 TO MAX_NUM_MISSION_PORTALS-1
		//sLocStruct.biGoToBlips[i-1] = sLocStruct.biGoToBlips[i]
		sMissionPlacementData.Portal[i-1]= sMissionPlacementData.Portal[i]
	ENDFOR
	
	// Clear the last entry
	sMissionPlacementData.Portal[MAX_NUM_MISSION_PORTALS-1] = sTempPortalStruct
	//sLocStruct.biGoToBlips[MAX_NUM_MISSION_PORTALS-1] = NULL
	
ENDPROC

PROC MAINTAIN_PLACED_FMC_PORTALS(	CREATION_VARS_STRUCT &sCurrentVarsStruct,
									structFMMC_MENU_ITEMS &sFMMCmenu,
									HUD_COLOURS_STRUCT &sHCS,
									FMMC_LOCAL_STRUCT &sFMMCdata,
									FMMC_COMMON_MENUS_VARS &sFMMCendStage)

	INT i, iR, iG, iB, iA
	BOOL bPlaneCorona = TRUE
	BOOL bDrawCorona = (sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION AND sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION)
	
	// Loop over all gotos
	FOR i = 0 TO MAX_NUM_MISSION_PORTALS-1
	
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.Portal[i].vCoord)
			IF bDrawCorona
				IF sFMMCmenu.iEntityCreation != CREATION_TYPE_PORTALS
				OR sFMMCMenu.iSelectedEntity != i
				
					// Check if it's in the corona
					IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_PORTALS, i, NULL, bPlaneCorona, GET_GOTO_LOC_HUD_COLOUR(i), sFMMCdata, sFMMCendStage)
						IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PORTALS
							sFMMCMenu.iCopyFromEntity = i
							DRAW_ON_SCREEN_ENTITY_INFO(sFMMCmenu, sFMMCMenu.iCopyFromEntity, CREATION_TYPE_PORTALS)
						ENDIF
					
						// url:bugstar:7467489 - Freemode Creator - Can we add the ability to pick up and edit an already placed portal please.
						// Reactivated extisting implementation
						//Pickup 
						IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
							PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_PORTALS, i)
							CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
						ENDIF
						//Delete
						IF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
							sCurrentVarsStruct.iEntityRemoved = i
							DELETE_FMC_PLACED_PORTAL(i)
							CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
							RELOOP
						ENDIF
					ENDIF
					
					GET_HUD_COLOUR(HUD_COLOUR_ORANGE,iR, iG, iB, iA)

					// Draw the selection zone in the centre of the goto if you're in the goto creation type
					IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PORTALS
						IF sFMMCMenu.iSelectedFMCEntity != i
							GET_HUD_COLOUR(HUD_COLOUR_ORANGEDARK,iR, iG, iB, iA)
						ENDIF
						
						INT iPortal
						REPEAT MAX_NUM_MISSION_PORTALS iPortal
							IF sFMMCMenu.iSelectedFMCEntity = iPortal
							AND sMissionPlacementData.Portal[iPortal].iLinkedPortal = i
								GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
								BREAKLOOP
							ENDIF
						ENDREPEAT
					ENDIF
					
					DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_CYLINDER, sMissionPlacementData.Portal[i].vCoord, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<1.0, 1.0, 1.0>>, iR, iG, iB,iA)
					FLOAT fHeading = sMissionPlacementData.Portal[i].fHeading
					IF fHeading = 180
						fHeading = 179.9999
					ELIF fHeading = -180
						fHeading = -179.9999
					ENDIF
					DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_ARROW, sMissionPlacementData.Portal[i].vCoord+0.1, <<0.0,0.0,0.0>>, <<90.0,fHeading+180,0.0>>, <<1.0, 1.0, 1.0>>, 0, iG, iB, 100,FALSE,FALSE,EULER_XYZ)
					
					// Draw the area that represents the goto
					//DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_CYLINDER, sMissionPlacementData.Portal[i].vCoord, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<fRad, fRad, fRad>>, iR, iG, iB, 100, FALSE, FALSE, EULER_XYZ, FALSE, NULL_STRING(),NULL_STRING(),sFMMCmenu.iGoToLocIsAerial = 0)
					
					// Give it a blip
//					IF NOT DOES_BLIP_EXIST(sLocStruct.biGoToBlips[i])
//						CREATE_FMMC_BLIP(sLocStruct.biGoToBlips[i], sMissionPlacementData.GoToPoint.Locations[i].vCoords, HUD_COLOUR_YELLOW, "FMMC_B_11", 1)
//					ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDFOR
ENDPROC
/////////////////////////////////////////////
///    PORTAL WARPS
///    

PROC DO_FMC_PORTAL_WARP_CREATION(	SELECTION_DETAILS_STRUCT 	&sSelDetails,
									CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
									structFMMC_MENU_ITEMS 		&sFMMCmenu,
									FMMC_LOCAL_STRUCT 			&sFMMCData,
									INT 						iMaximum = MAX_NUM_PORTAL_WARP_COORDS)

	INT iR, iG, iB, iA
	INT iPortalWarp
	FLOAT fRadius
	
	// Determine which goto we're working with
	IF sFMMCMenu.iSelectedSubEntity = -1
	
		iPortalWarp = GET_NUMBER_OF_FMC_PORTAL_WARP(sFMMCmenu.iSelectedFMCEntity)
		
	ELSE
	
		iPortalWarp = sFMMCMenu.iSelectedSubEntity
		
		IF iPortalWarp >= iMaximum 
			iPortalWarp = iMaximum - 1
			sFMMCMenu.iSelectedSubEntity = iPortalWarp
		ENDIF
				
	ENDIF
	
	// Error for if you try to place one when too many already exist
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_GOTO_LOCATIONS)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCMenu)
		AND sFMMCMenu.iSelectedEntity = -1
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			sCurrentVarsStruct.bDisplayFailReason = TRUE
			sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			sCurrentVarsStruct.sFailReason = "FMMC_HELP9CL"
			PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
			PRINTSTRING("MOVE_LAST_PLACED_VEHICLE ERROR SOUND EFFECT 4!!!")PRINTNL()
		ENDIF
	ENDIF
	
	// Ped counter UI
	IF GET_NUMBER_OF_FMC_PORTAL_WARP(sFMMCmenu.iSelectedFMCEntity) >= iMaximum
	AND iPortalWarp >= iMaximum
		sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
		sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_GOTO_LOCATIONS )
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_FMC_GOTOS(),  iMaximum,	"FMMC_AB_09", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_FLASHRED, 500)
		EXIT
	ELSE
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_FMC_GOTOS(),  iMaximum,	"FMMC_AB_09", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_GOTO_LOCATIONS )
	ENDIF
	
	// Main creation state machine
	SWITCH sSelDetails.iSwitchingINT
	
		// Wait stage to stall the creation of more if there are too many already
		CASE CREATION_STAGE_WAIT
			IF iPortalWarp < iMaximum
				// We skip straight to placement on gotos as they don't need to create anything
				sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		
		// Do the placement of the goto, updating it's position based on user input
		CASE CREATION_STAGE_PLACE
		
			FLOAT fHeading
			// Checks if the place button has been pressed and that it's a valid place to go
			IF MOVE_LAST_PLACED_GO_TO_POINT(	sFMMCmenu, 
												sEditedPortalPlacementData.vCoord,
												fHeading,
												sCurrentVarsStruct, sFMMCData, 
												iPortalWarp)
				
				PRINTLN("sSelDetails.iSwitchingINT = CREATION_STAGE_DONE")
				sSelDetails.iSwitchingINT = CREATION_STAGE_DONE
				
			ENDIF
			
			// Update the position
			sEditedPortalPlacementData.vCoord = sCurrentVarsStruct.vCoronaPos
			
			IF GET_ENTITY_FMC_INTERIOR_ON_PLACEMENT(sEditedPortalPlacementData.vCoord) != (-1)
				SET_ARRAYED_BIT(sEditedPortalPlacementData.iBitset, ENUM_TO_INT(ePORTALDATABITSET_INSIDE_INTERIOR))
				PRINTLN("DO_FMC_PORTAL_CREATION - Setting ePORTALDATABITSET_INSIDE_INTERIOR for portal ",sFMMCmenu.iSelectedFMCEntity," warp ",iPortalWarp)
			ENDIF
			
			GET_HUD_COLOUR(HUD_COLOUR_ORANGE,iR, iG, iB, iA)
			
			// Draw the goto area
			fRadius = 0.5
			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_CYLINDER, sEditedPortalPlacementData.vCoord, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<fRadius, fRadius, fRadius>>, iR, iG, iB, 100)
			
		BREAK
		
		// Update the goto you've just placed, increase the counter and reset for the next one
		CASE CREATION_STAGE_DONE
		
			sMissionPlacementData.Portal[sFMMCmenu.iSelectedFMCEntity].vWarpCoord[iPortalWarp]	= sEditedPortalPlacementData.vCoord
			PRINTLN("[LOCS] Creating portal ",sFMMCmenu.iSelectedFMCEntity," warp ",iPortalWarp," with pos ", sEditedPortalPlacementData.vCoord)
			
			START_PLACEMENT_COOLDOWN_TIMER(sCurrentVarsStruct)
			
			//CREATE_FMMC_BLIP(sLocStruct.biGoToBlips[iGoToPoint], sMissionPlacementData.GoToPoint.Locations[iGoToPoint].vCoords, HUD_COLOUR_YELLOW, "FMMC_B_11", 1)
			
			SET_BIT(sLocStruct.iBS_Editted, iPortalWarp)
			
			CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)
			
			IF sFMMCMenu.iSelectedSubEntity = -1
				RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			ENDIF
			//sFMMCMenu.iSelectedEntity = -1
			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
			
		BREAK
		
	ENDSWITCH
	
	UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)	
	
ENDPROC

PROC DELETE_FMC_PLACED_PORTAL_WARP(INT iGoto, INT iPortal)

	INT i
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
//	// Remove the entity/blip
//	IF DOES_BLIP_EXIST(sLocStruct.biGoToBlips[iGoto])
//		REMOVE_BLIP(sLocStruct.biGoToBlips[iGoto])
//	ENDIF
	
	// Move all the entries down
	FOR i = iGoto+1 TO MAX_NUM_PORTAL_WARP_COORDS-1
		//sLocStruct.biGoToBlips[i-1] = sLocStruct.biGoToBlips[i]
		sMissionPlacementData.Portal[iPortal].vWarpCoord[i-1] = sMissionPlacementData.Portal[iPortal].vWarpCoord[i]
		sMissionPlacementData.Portal[iPortal].fWarpHeading[i-1] = sMissionPlacementData.Portal[iPortal].fWarpHeading[i]
	ENDFOR
	
	// Clear the last entry
	sMissionPlacementData.Portal[iPortal].vWarpCoord[MAX_NUM_PORTAL_WARP_COORDS-1] = <<0.0,0.0,0.0>>
	sMissionPlacementData.Portal[iPortal].fWarpHeading[MAX_NUM_PORTAL_WARP_COORDS-1] = 0.0
	//sLocStruct.biGoToBlips[MAX_NUM_MISSION_PORTALS-1] = NULL
	
ENDPROC

PROC MAINTAIN_PLACED_FMC_PORTAL_WARPS(	CREATION_VARS_STRUCT &sCurrentVarsStruct,
										structFMMC_MENU_ITEMS &sFMMCmenu,
										HUD_COLOURS_STRUCT &sHCS,
										FMMC_LOCAL_STRUCT &sFMMCdata,
										FMMC_COMMON_MENUS_VARS &sFMMCendStage)

	IF sFMMCmenu.iEntityCreation != CREATION_TYPE_PORTAL_WARP
		EXIT
	ENDIF

	INT i, iR, iG, iB, iA
	BOOL bPlaneCorona = TRUE
	BOOL bDrawCorona = (sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION AND sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION)
	
	// Loop over all gotos
	FOR i = 0 TO MAX_NUM_PORTAL_WARP_COORDS-1
	
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.Portal[sFMMCmenu.iSelectedFMCEntity].vWarpCoord[i])
			IF bDrawCorona
				IF sFMMCmenu.iEntityCreation != CREATION_TYPE_PORTAL_WARP
				OR sFMMCMenu.iSelectedEntity != i
				
					// Check if it's in the corona
					IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_PORTAL_WARP, i, NULL, bPlaneCorona, GET_GOTO_LOC_HUD_COLOUR(i), sFMMCdata, sFMMCendStage)
						IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PORTAL_WARP
							sFMMCMenu.iCopyFromEntity = i
							DRAW_ON_SCREEN_ENTITY_INFO(sFMMCmenu, sFMMCMenu.iCopyFromEntity, CREATION_TYPE_PORTAL_WARP)
						ENDIF
					
//						//Pickup
//						IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
//							PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_PORTAL_WARP, i)
//							CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
//						
//						//Delete
						IF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
							sCurrentVarsStruct.iEntityRemoved = i
							DELETE_FMC_PLACED_PORTAL_WARP(i,sFMMCmenu.iSelectedFMCEntity)
							CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
							RELOOP
						ENDIF
					ENDIF
					
					GET_HUD_COLOUR(HUD_COLOUR_ORANGE,iR, iG, iB, iA)

					// Draw the selection zone in the centre of the goto if you're in the goto creation type
					IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PORTAL_WARP
						IF sFMMCMenu.iSelectedSubFMCEntity != i
							GET_HUD_COLOUR(HUD_COLOUR_ORANGEDARK,iR, iG, iB, iA)
						ENDIF
						
//						INT iPortalWarp
//						REPEAT MAX_NUM_PORTAL_WARP_COORDS iPortalWarp
//							IF sFMMCMenu.iSelectedSubFMCEntity = iPortalWarp
//							AND sMissionPlacementData.Portal[iPortal].iLinkedPortal = i
//								GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
//								BREAKLOOP
//							ENDIF
//						ENDREPEAT
					ENDIF
					
					DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_CYLINDER, sMissionPlacementData.Portal[sFMMCmenu.iSelectedFMCEntity].vWarpCoord[i], <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<1.0, 1.0, 1.0>>, iR, iG, iB,iA)
					FLOAT fHeading = sMissionPlacementData.Portal[sFMMCmenu.iSelectedFMCEntity].fWarpHeading[i]
					IF fHeading = 180
						fHeading = 179.9999
					ELIF fHeading = -180
						fHeading = -179.9999
					ENDIF
					DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_ARROW, sMissionPlacementData.Portal[sFMMCmenu.iSelectedFMCEntity].vWarpCoord[i]+0.1, <<0.0,0.0,0.0>>, <<90.0,fHeading+180,0.0>>, <<1.0, 1.0, 1.0>>, 0, iG, iB, 100,FALSE,FALSE,EULER_XYZ)
										
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDFOR
ENDPROC

/////////////////////////////////////////////
///    FOCUS CAM
///    
///    

PROC DELETE_FMC_FOCUS_CAM(INT iCam)

	MISSION_PLACEMENT_DATA_FOCUS_CAM sTempFocus
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	// Move all the entries down
	INT i
	FOR i = iCam +1 TO MAX_NUM_FOCUS_CAMS -1
		sMissionPlacementData.FocusCam.Cam[i-1] = sMissionPlacementData.FocusCam.Cam[i]
	ENDFOR
	
	// Clear the last entry
	sMissionPlacementData.FocusCam.Cam[MAX_NUM_FOCUS_CAMS-1] = sTempFocus
ENDPROC

/////////////////////////////////////////////
///    CUSTOM SPAWN LOCATIONS
///    
///    
   
FUNC INT GET_NUMBER_CUSTOM_SPAWNS()
	INT i
	FOR i = 0 TO MAX_NUM_CUSTOM_SPAWNS-1
		IF IS_VECTOR_ZERO(sMissionPlacementData.CustomSpawns.Spawns[i].vCoords)
			RETURN i
		ENDIF
	ENDFOR
	RETURN MAX_NUM_CUSTOM_SPAWNS
ENDFUNC

PROC DELETE_CUSTOM_SPAWN_POINT_DECAL(INT i)	
	IF IS_DECAL_ALIVE(sCustomSpawns.diDecal[i])
		REMOVE_DECAL(sCustomSpawns.diDecal[i])
		PRINTLN("[CUST_SPAWN] [DECAL] DELETE_CUSTOM_SPAWN_POINT_DECAL, i = ", i)
	ENDIF
ENDPROC

PROC CREATE_CUSTOM_SPAWN_POINT_DECAL(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT i)

	IF sCurrentVarsStruct.bCanCreateADecalThisFrame

		INT r,g,b,a
		GET_HUD_COLOUR(HUD_COLOUR_BLUELIGHT, r,g,b,a)
		
		VECTOR vPlacement = sMissionPlacementData.CustomSpawns.Spawns[i].vCoords
		
		VECTOR vDirection, vSide
		GET_DECAL_VECTORS(vPlacement, sMissionPlacementData.CustomSpawns.Spawns[i].fHeading, vDirection, vSide)
		
		sCustomSpawns.vDecalSpawnVector[i] = vPlacement
		sCustomSpawns.diDecal[i] = ADD_DECAL(DECAL_RSID_HALO_PT, vPlacement, vDirection, vSide, 2,2, TO_FLOAT(r)/255,TO_FLOAT(g)/255,TO_FLOAT(b)/255, 1.0,-1, TRUE)
		
		sCurrentVarsStruct.bCanCreateADecalThisFrame = FALSE
		PRINTLN("[CUST_SPAWN] [DECAL] CREATE_CUSTOM_SPAWN_POINT_DECAL, i = ", i, " at ", vPlacement)
	ENDIF
ENDPROC

PROC INITIALISE_CUSTOM_SPAWNS()
	IF sMissionPlacementData.CustomSpawns.iCount != GET_NUMBER_CUSTOM_SPAWNS()
		sMissionPlacementData.CustomSpawns.iCount = GET_NUMBER_CUSTOM_SPAWNS()
	ENDIF
ENDPROC

FUNC BOOL MOVE_LAST_PLACED_CUSTOM_SPAWN(structFMMC_MENU_ITEMS &sFMMCmenu,VECTOR &vCoord, FLOAT &fHead, CREATION_VARS_STRUCT &sCurrentVarsStruct, FMMC_LOCAL_STRUCT &sFMMCdata, INT iNumber)
		
	MOVE_ENTITY(NULL, vCoord, fHead, sCurrentVarsStruct, sFMMCdata, sFMMCmenu)
	
	IF IS_CREATOR_PLACEMENT_BUTTON_PRESSED(sFMMCMenu)
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
			IF CAN_PLACE_BUTTON_BE_PRESSED(sFMMCmenu, sCurrentVarsStruct)
			
				// When inside buildings, Spawn points can be placed closer together
				FLOAT fDistance = 10.0			
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != 0
							fDistance = 5.0				
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PLACEMENT_IN_A_RESTRICTED_AREA(sCurrentVarsStruct.vCoronaPos)
					IF NOT IS_ANY_OTHER_CUSTOM_SPAWN_IN_CORONA(sCurrentVarsStruct, fDistance, iNumber)	//TODO: change
					OR IS_ROCKSTAR_DEV()
						IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
							sCurrentVarsStruct.bDisplayFailReason = FALSE
							PLAY_SOUND_FRONTEND(-1, "ADD_SPAWN_POINT", GET_CREATOR_SPECIFIC_SOUND_SET())
							PRINTLN("[CUST_SPAWN] ADD_SPAWN_POINT SOUND EFFECT in soundsset ",GET_CREATOR_SPECIFIC_SOUND_SET(), "!!!")PRINTNL()	
							RETURN  TRUE
						ENDIF
					ELSE
						sCurrentVarsStruct.bDisplayFailReason = TRUE
						sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
						sCurrentVarsStruct.sFailReason = "FMMC_HELP0GT"
						
					ENDIF
				ELSE
					SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
					sCurrentVarsStruct.bFirstShapeTestCheckDone = TRUE
					sCurrentVarsStruct.sFailReason = "FMMC_ER_024"
					SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bRestrictedFail)
					RETURN FALSE
				ENDIF
				PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("[CUST_SPAWN] ERROR SOUND EFFECT!!!")PRINTNL()
			ENDIF		
		ENDIF
	ENDIF
	RETURN FALSE
	
ENDFUNC

PROC DO_FMC_CUSTOM_SPAWN_CREATION(	SELECTION_DETAILS_STRUCT 	&sSelDetails,
								CREATION_VARS_STRUCT 			&sCurrentVarsStruct,
								structFMMC_MENU_ITEMS 			&sFMMCmenu,
								FMMC_LOCAL_STRUCT 				&sFMMCData)

	INT iR, iG, iB, iA
	INT iSpawnPoint
	
	IF sFMMCMenu.iSelectedEntity = -1
	
		iSpawnPoint = sMissionPlacementData.CustomSpawns.iCount
	ELSE
	
		iSpawnPoint = sFMMCMenu.iSelectedEntity
		
		IF iSpawnPoint >= MAX_NUM_CUSTOM_SPAWNS 
			iSpawnPoint = MAX_NUM_CUSTOM_SPAWNS -1
			sFMMCMenu.iSelectedEntity = iSpawnPoint
		ENDIF
		
		IF DOES_BLIP_EXIST(sCustomSpawns.biBlips[iSpawnPoint])
		
			REMOVE_BLIP(sCustomSpawns.biBlips[iSpawnPoint])
			
			sEditedCustomSpawnPlacementData = sMissionPlacementData.CustomSpawns.Spawns[iSpawnPoint]
			
			sSelDetails.iSwitchingINT= CREATION_STAGE_WAIT
		ENDIF
	ENDIF
	
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_CUSTOM_SPAWNS)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCMenu)
		AND sFMMCMenu.iSelectedEntity = -1
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			sCurrentVarsStruct.bDisplayFailReason = TRUE
			sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			sCurrentVarsStruct.sFailReason = "FMMC_HELPCST"
			PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
			PRINTSTRING("[CUST_SPAWN] MOVE_LAST_PLACED_VEHICLE ERROR SOUND EFFECT 4!!!")PRINTNL()
		ENDIF
	ENDIF
	
	IF sMissionPlacementData.CustomSpawns.iCount >= MAX_NUM_CUSTOM_SPAWNS
	AND iSpawnPoint >= MAX_NUM_CUSTOM_SPAWNS
		sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
		sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_CUSTOM_SPAWNS )
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.CustomSpawns.iCount,  MAX_NUM_CUSTOM_SPAWNS,	"FMMC_AB_CST", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_FLASHRED, 500)
		EXIT
	ELSE
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.CustomSpawns.iCount,  MAX_NUM_CUSTOM_SPAWNS,	"FMMC_AB_CST", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_CUSTOM_SPAWNS )
	ENDIF
	
	SWITCH sSelDetails.iSwitchingINT
	
		CASE CREATION_STAGE_WAIT
			IF iSpawnPoint < MAX_NUM_CUSTOM_SPAWNS
				FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE) // MAINTAIN_PLACEMENT_DISC handles colour
				sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		
		CASE CREATION_STAGE_PLACE
		
			IF MOVE_LAST_PLACED_CUSTOM_SPAWN(	sFMMCmenu, 											
												sEditedCustomSpawnPlacementData.vCoords,
												sEditedCustomSpawnPlacementData.fHeading,
												sCurrentVarsStruct, 
												sFMMCData, 
												iSpawnPoint)
				
				PRINTLN("[CUST_SPAWN] sSelDetails.iSwitchingINT = CREATION_STAGE_DONE")
				sSelDetails.iSwitchingINT = CREATION_STAGE_DONE
			ENDIF
			
			FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
			sCurrentVarsStruct.iCoronaMarkerType = ciCoronaSmallPointer
			
			sEditedCustomSpawnPlacementData.vCoords = sCurrentVarsStruct.vCoronaPos
			sEditedCustomSpawnPlacementData.fHeading = sCurrentVarsStruct.fCreationHeading
			
			GET_HUD_COLOUR(HUD_COLOUR_YELLOW,iR, iG, iB, iA)			
		BREAK
		
		CASE CREATION_STAGE_DONE
		
			sMissionPlacementData.CustomSpawns.Spawns[iSpawnPoint] = sEditedCustomSpawnPlacementData
			PRINTLN("[CUST_SPAWN] Creating custom spawn ", iSpawnPoint," with pos/rad of ", sEditedCustomSpawnPlacementData.vCoords," / ", sEditedCustomSpawnPlacementData.fHeading)
			
			START_PLACEMENT_COOLDOWN_TIMER(sCurrentVarsStruct)

			CREATE_FMMC_BLIP(sCustomSpawns.biBlips[iSpawnPoint], sMissionPlacementData.CustomSpawns.Spawns[iSpawnPoint].vCoords, HUD_COLOUR_BLUELIGHT, "FMMC_B_CST", 1)	

			CREATE_CUSTOM_SPAWN_POINT_DECAL(sCurrentVarsStruct, iSpawnPoint)
			PRINTLN("[CUST_SPAWN] [DECAL] DO_FMC_CUSTOM_SPAWN_CREATION, CREATE_CUSTOM_SPAWN_POINT_DECAL for i = ", iSpawnPoint)
			
			SET_BIT(sCustomSpawns.iBS_Editted, iSpawnPoint)
			
			FMC_CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)
			
			IF sFMMCMenu.iSelectedEntity = -1
				RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
				
				sMissionPlacementData.CustomSpawns.iCount++
				IF sMissionPlacementData.CustomSpawns.iCount > MAX_NUM_CUSTOM_SPAWNS
					sMissionPlacementData.CustomSpawns.iCount = MAX_NUM_CUSTOM_SPAWNS
				ENDIF
			ENDIF
			
			sFMMCMenu.iSelectedEntity = -1
			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		BREAK
		
	ENDSWITCH
	
	UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)	
ENDPROC

PROC DELETE_FMC_PLACED_CUSTOM_SPAWN(INT iSpawnPoint)

	INT i
	MISSION_PLACEMENT_DATA_CUSTOM_SPAWN sTempStruct
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	// Remove the entity/blip
	IF DOES_BLIP_EXIST(sCustomSpawns.biBlips[iSpawnPoint])
		REMOVE_BLIP(sCustomSpawns.biBlips[iSpawnPoint])
	ENDIF
	
	PRINTLN("[CUST_SPAWN] [DECAL] ATTEMPT TO DELETE 1 ", iSpawnPoint)
	DELETE_CUSTOM_SPAWN_POINT_DECAL(iSpawnPoint)
	
	// Move all the entries down
	FOR i = iSpawnPoint +1 TO sMissionPlacementData.CustomSpawns.iCount -1
		sCustomSpawns.diDecal[i-1] = sCustomSpawns.diDecal[i]
		sCustomSpawns.biBlips[i-1] = sCustomSpawns.biBlips[i]
		sMissionPlacementData.CustomSpawns.Spawns[i-1]= sMissionPlacementData.CustomSpawns.Spawns[i]
	ENDFOR
	
	sMissionPlacementData.CustomSpawns.iCount--
	IF sMissionPlacementData.CustomSpawns.iCount < 0
		sMissionPlacementData.CustomSpawns.iCount = 0
	ENDIF
	
	// Clear the last entry
	sMissionPlacementData.CustomSpawns.Spawns[sMissionPlacementData.CustomSpawns.iCount] = sTempStruct
	sCustomSpawns.biBlips[sMissionPlacementData.CustomSpawns.iCount] = NULL
	sCustomSpawns.diDecal[sMissionPlacementData.CustomSpawns.iCount] = NULL
	
	PRINTLN("[CUST_SPAWN] [DECAL] DELETE_FMC_PLACED_CUSTOM_SPAWN, iSpawnPoint = ", iSpawnPoint)
ENDPROC

PROC MAINTAIN_PLACED_FMC_CUSTOM_SPAWN_LOCATIONS(CREATION_VARS_STRUCT &sCurrentVarsStruct,
													structFMMC_MENU_ITEMS &sFMMCmenu,
													HUD_COLOURS_STRUCT &sHCS,
													FMMC_LOCAL_STRUCT &sFMMCdata,
													INT &iLocalCustomSpawnBitSet,
													FMMC_COMMON_MENUS_VARS &sFMMCendStage)
													
													
	IF sMissionPlacementData.CustomSpawns.iCount <= 0
		EXIT
	ENDIF

	INT i, iR, iG, iB, iA
	BOOL bPlaneCorona = TRUE
	BOOL bDrawCorona = (sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION)
	
	//Spawn points
	FOR i = 0 TO MAX_NUM_CUSTOM_SPAWNS -1
	
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.CustomSpawns.Spawns[i].vCoords)
		
			#IF IS_DEBUG_BUILD
			DRAW_ENTITY_LABEL(CREATION_TYPE_SPAWN_LOCATION, i, sMissionPlacementData.CustomSpawns.Spawns[i].vCoords)
			TEXT_LABEL_63 tl63 = "Cst: "
			tl63 += i
			DRAW_DEBUG_TEXT_ABOVE_COORDS(sMissionPlacementData.CustomSpawns.Spawns[i].vCoords, tl63, 0.5)
			#ENDIF
		
			IF bDrawCorona
				IF sFMMCmenu.iEntityCreation != CREATION_TYPE_SPAWN_LOCATION
				OR sFMMCMenu.iSelectedEntity != i
				
					IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_SPAWN_LOCATION, i, NULL, bPlaneCorona, GET_GOTO_LOC_HUD_COLOUR(i), sFMMCdata, sFMMCendStage)
						IF IS_BIT_SET(iLocalCustomSpawnBitSet, biPickupButtonPressed)
							PRINTLN("[CUST_SPAWN] [DECAL] ATTEMPT TO DELETE 2 ", i)
							DELETE_CUSTOM_SPAWN_POINT_DECAL(i)
							PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_SPAWN_LOCATION, i)
							CLEAR_BIT(iLocalCustomSpawnBitSet, biPickupButtonPressed)
						ELIF IS_BIT_SET(iLocalCustomSpawnBitSet, biDeleteButtonPressed)
							sCurrentVarsStruct.iEntityRemoved = i
							DELETE_FMC_PLACED_CUSTOM_SPAWN(i)
							CLEAR_BIT(iLocalCustomSpawnBitSet, biDeleteButtonPressed)
							RELOOP
						ENDIF
					ENDIF
					
					GET_HUD_COLOUR(HUD_COLOUR_YELLOWLIGHT,iR, iG, iB, iA)
					DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_CYLINDER, sMissionPlacementData.CustomSpawns.Spawns[i].vCoords, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<1.95, 1.95, 0.6>>, iR, iG, iB, 180)
					
					IF NOT DOES_BLIP_EXIST(sCustomSpawns.biBlips[i])
					AND sCycleCamStruct.BlockBlipsState = eBLOCK_BLIPS_NONE
						CREATE_FMMC_BLIP(sCustomSpawns.biBlips[i], sMissionPlacementData.CustomSpawns.Spawns[i].vCoords, HUD_COLOUR_BLUELIGHT, "FMMC_B_CST", 1)
					ENDIF
					IF NOT IS_BIT_SET(sCustomSpawns.iDecalBitset, i)
						IF NOT IS_DECAL_ALIVE(sCustomSpawns.diDecal[i])
							CREATE_CUSTOM_SPAWN_POINT_DECAL(sCurrentVarsStruct, i)
						ELSE
							SET_BIT(sCustomSpawns.iDecalBitset, i)
							PRINTLN("[CUST_SPAWN] [DECAL] MAINTAIN_PLACED_FMC_CUSTOM_SPAWN_LOCATIONS, CREATE_CUSTOM_SPAWN_POINT_DECAL, SET_BIT for i = ", i)
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
		ELSE
			DELETE_CUSTOM_SPAWN_POINT_DECAL(i)
		ENDIF
	ENDFOR
	
	IF sFMMCMenu.sActiveMenu = eFmmc_FMC_CUSTOM_SPAWNS
		IF sFMMCmenu.iSelectedEntity != -1
		AND sFMMCmenu.iSelectedEntity < MAX_NUM_CUSTOM_SPAWNS
		AND NOT IS_VECTOR_ZERO(sMissionPlacementData.CustomSpawns.Spawns[sFMMCmenu.iSelectedEntity].vCoords)
			GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_CYLINDER, sMissionPlacementData.CustomSpawns.Spawns[sFMMCmenu.iSelectedEntity].vCoords, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<1.95, 1.95, 0.6>>, iR, iG, iB, 255)
		ENDIF
	ENDIF

ENDPROC

///////////////////////////////////////////////
/////    PATROLS LOCATIONS
/////    

PROC DELETE_PATROL_NODE_DECAL(PATROL_NODE_CREATION_STRUCT &sPatrol, INT i)	
	IF IS_DECAL_ALIVE(sPatrol.diDecal[i])
		REMOVE_DECAL(sPatrol.diDecal[i])
		PRINTLN("[PATROL] [DECAL] DELETE_PATROL_NODE_POINT_DECAL, i = ", i)
	ENDIF
ENDPROC

PROC CREATE_PATROL_NODE_DECAL(structFMMC_MENU_ITEMS &sFMMCmenu, CREATION_VARS_STRUCT &sCurrentVarsStruct, PATROL_NODE_CREATION_STRUCT &sPatrol, INT i)
	IF sCurrentVarsStruct.bCanCreateADecalThisFrame

		INT r,g,b,a
		GET_HUD_COLOUR(HUD_COLOUR_REDLIGHT, r,g,b,a)
		
		VECTOR vPlacement = sMissionPlacementData.Patrol.Patrols[sFMMCMenu.iSelectedPatrol].Node[i].vCoord		
		VECTOR vLookAt = sMissionPlacementData.Patrol.Patrols[sFMMCMenu.iSelectedPatrol].Node[i].vLookAt
		FLOAT fHeading 
		IF NOT IS_VECTOR_ZERO(vLookAt)
			fHeading = GET_HEADING_BETWEEN_VECTORS_2D(vPlacement, vLookAt)
		ENDIF
		
		VECTOR vDirection, vSide
		GET_DECAL_VECTORS(vPlacement, fHeading, vDirection, vSide)
		
		sPatrol.vDecalSpawnVector[i] = vPlacement
		sPatrol.diDecal[i] = ADD_DECAL(DECAL_RSID_HALO_PT, vPlacement, vDirection, vSide, 2,2, TO_FLOAT(r)/255,TO_FLOAT(g)/255,TO_FLOAT(b)/255, 1.0,-1, TRUE)
		sCurrentVarsStruct.bCanCreateADecalThisFrame = FALSE
		PRINTLN("[PATROL] [DECAL] CREATE_PATROL_NODE_POINT_DECAL, i = ", i, " at ", vPlacement, " fHeading = ", fHeading, " vLookAt = ", vLookAt)
	ENDIF
ENDPROC

FUNC VECTOR GET_NEXT_PATROL_NODE_POSITION(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iPatrol, INT iNode, INT iSelected = -1)
	IF iNode = iSelected
		RETURN sCurrentVarsStruct.vCoronaPos
	ELIF iNode+1 < MAX_NUM_PATROL_NODES
		RETURN sMissionPlacementData.Patrol.Patrols[iPatrol].Node[iNode+1].vCoord
	ENDIF
	RETURN <<0,0,0>>
ENDFUNC

PROC DRAW_PATROL_PATH(structFMMC_MENU_ITEMS &sFMMCmenu, CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iPatrol, HUD_COLOURS eColour, INT iSelected = -1)

	INT i, iR, iG, iB, iA
	
	GET_HUD_COLOUR(eColour,iR, iG, iB, iA)
	VECTOR vThisNode, vNextNode
	VECTOR vGroundOffset = <<0,0,1>>
	
	FOR i = 0 TO MAX_NUM_PATROL_NODES-1
	
		IF i = iSelected
			GET_HUD_COLOUR(HUD_COLOUR_WHITE,iR, iG, iB, iA)
			vThisNode = sCurrentVarsStruct.vCoronaPos
		ELSE
			vThisNode = sMissionPlacementData.Patrol.Patrols[iPatrol].Node[i].vCoord
		ENDIF
		vNextNode = GET_NEXT_PATROL_NODE_POSITION(sCurrentVarsStruct, iPatrol, i, iSelected)
		
		DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, vThisNode+vGroundOffset, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<0.5, 0.5, 0.5>>, iR, iG, iB, 200, FALSE, FALSE, EULER_XYZ, FALSE, NULL_STRING(),NULL_STRING(),sFMMCmenu.iGoToLocIsAerial = 0)
		
		GET_HUD_COLOUR(eColour,iR, iG, iB, iA)
		
		IF i = 0
		AND sMissionPlacementData.Patrol.Patrols[iPatrol].iPed > -1
		AND NOT IS_VECTOR_ZERO(sMissionPlacementData.Ped.Peds[sMissionPlacementData.Patrol.Patrols[iPatrol].iPed].vCoords)
			DRAW_LINE(sMissionPlacementData.Ped.Peds[sMissionPlacementData.Patrol.Patrols[iPatrol].iPed].vCoords+vGroundOffset, vThisNode+vGroundOffset, iR, iG, iB)
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(vNextNode)
			DRAW_LINE(vThisNode+vGroundOffset, vNextNode+vGroundOffset, iR, iG, iB)
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL IS_SETTING_PATROL_NODE_LOOK_DIRECTION(structFMMC_MENU_ITEMS &sFMMCmenu, INT iMenuSelection)
	
	//Check that a node has been placed
	IF GET_NUMBER_OF_FMC_PATROL_NODES(sFMMCMenu.iSelectedPatrol) <= 0
		RETURN FALSE
	ENDIF
	
	//Check that the user is on the correct menu option
	IF iMenuSelection <= 0
		RETURN FALSE
	ENDIF
	IF ( iMenuSelection % ciFMC_PATROL_LOOK_AT ) != 0
		RETURN FALSE
	ENDIF
	
	//Check a patrol ped exists
	INT iPatrolPed = sMissionPlacementData.Patrol.Patrols[sFMMCMenu.iSelectedPatrol].iPed
	IF iPatrolPed < 0
		RETURN FALSE
	ENDIF
	
	IF IS_VECTOR_ZERO(sMissionPlacementData.Ped.Peds[iPatrolPed].vCoords)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

PROC DO_FMC_PATROL_CREATION(	SELECTION_DETAILS_STRUCT 	&sSelDetails,
								CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
								structFMMC_MENU_ITEMS 		&sFMMCmenu,
								FMMC_LOCAL_STRUCT 			&sFMMCData,
								PATROL_NODE_CREATION_STRUCT &sPatrol,
								INT 						iMaxNodes = MAX_NUM_PATROL_NODES)

	INT iPatrolNode
	INT iPatrolPed
	INT iMenuSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
	INT iNodeSelected = ( iMenuSelection/ciFMC_PATROL_LOOK_AT ) -1 //Grab the node menu option selected so we can populate the correct look at coord
	
	// Select the patrol node we'll be working with
	IF sFMMCMenu.iSelectedEntity = -1
	
		iPatrolNode = GET_NUMBER_OF_FMC_PATROL_NODES(sFMMCMenu.iSelectedPatrol)
	ELSE
	
		iPatrolNode = sFMMCMenu.iSelectedEntity
		
		IF iPatrolNode >= iMaxNodes 
			iPatrolNode = iMaxNodes - 1
			sFMMCMenu.iSelectedEntity = iPatrolNode
		ENDIF
		
	ENDIF
	
	// Error message if there are too many nodes placed already
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PATROL_NODES)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCMenu)
		AND sFMMCMenu.iSelectedEntity = -1
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			sCurrentVarsStruct.bDisplayFailReason = TRUE
			sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			sCurrentVarsStruct.sFailReason = "FMMC_HELP9CL"
			PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
			PRINTSTRING("MOVE_LAST_PLACED_VEHICLE ERROR SOUND EFFECT 4!!!")PRINTNL()
		ENDIF
	ENDIF
	
	// Counter UI in the bottom right
	IF GET_NUMBER_OF_FMC_GOTOS() >= iMaxNodes
	AND iPatrolNode >= iMaxNodes
		sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
		sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PATROL_NODES)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_FMC_PATROL_NODES(sFMMCMenu.iSelectedPatrol), iMaxNodes, "FMC_N_PTRLN", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_FLASHRED, 500)
		EXIT
	ELSE
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_FMC_PATROL_NODES(sFMMCMenu.iSelectedPatrol), iMaxNodes, "FMC_N_PTRLN", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_PATROL_NODES)
	ENDIF
	
	// The main placement state machine
	SWITCH sSelDetails.iSwitchingINT
	
		// Wait is mostly useless for paths as there's no entity
		CASE CREATION_STAGE_WAIT
			IF iPatrolNode < iMaxNodes
				sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		
		// Where the actual placement takes place
		CASE CREATION_STAGE_PLACE
		
			IF IS_CREATOR_PLACEMENT_BUTTON_PRESSED(sFMMCMenu)
			AND CAN_PLACE_BUTTON_BE_PRESSED(sFMMCmenu, sCurrentVarsStruct)
			AND NOT IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
				
				iPatrolPed = sMissionPlacementData.Patrol.Patrols[sFMMCMenu.iSelectedPatrol].iPed
				
				PRINTLN("[PATROL] DO_FMC_PATROL_CREATION, iPatrolPed      = ", iPatrolPed)
				PRINTLN("[PATROL] DO_FMC_PATROL_CREATION, iMenuSelection  = ", iMenuSelection)
				PRINTLN("[PATROL] DO_FMC_PATROL_CREATION, iSelectedPatrol = ", sFMMCMenu.iSelectedPatrol)
				PRINTLN("[PATROL] DO_FMC_PATROL_CREATION, iPatrolNode     = ", iPatrolNode)
				
				IF IS_SETTING_PATROL_NODE_LOOK_DIRECTION(sFMMCMenu, iMenuSelection)
					sMissionPlacementData.Patrol.Patrols[sFMMCMenu.iSelectedPatrol].Node[iNodeSelected].vLookAt = sCurrentVarsStruct.vCoronaPos
					DELETE_PATROL_NODE_DECAL(sPatrol, iNodeSelected)
					sPatrol.iDecalNode = iNodeSelected
					PRINTLN("[PATROL] DO_FMC_PATROL_CREATION, vLookAt = ", sMissionPlacementData.Patrol.Patrols[sFMMCMenu.iSelectedPatrol].Node[iNodeSelected].vLookAt, " iNodeSelected = ", iNodeSelected)
				ELSE
					// Set node coord
					sMissionPlacementData.Patrol.Patrols[sFMMCMenu.iSelectedPatrol].Node[iPatrolNode].vCoord = sCurrentVarsStruct.vCoronaPos
					sPatrol.iDecalNode = iPatrolNode
					PRINTLN("[PATROL] DO_FMC_PATROL_CREATION, vCoord = ", sMissionPlacementData.Patrol.Patrols[sFMMCMenu.iSelectedPatrol].Node[iPatrolNode].vCoord)
				ENDIF
				
				sSelDetails.iSwitchingINT = CREATION_STAGE_DONE
			ENDIF
			
		BREAK
		
		// Setting any extra data and returning to the base state to create another node
		CASE CREATION_STAGE_DONE
			REFRESH_MENU_ASAP(sFMMCMenu)
			START_PLACEMENT_COOLDOWN_TIMER(sCurrentVarsStruct)
			CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)
			IF sFMMCMenu.iSelectedEntity = -1
				RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			ENDIF
			sFMMCMenu.iSelectedEntity = -1
			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
			CREATE_PATROL_NODE_DECAL(sFMMCmenu, sCurrentVarsStruct, sPatrol, sPatrol.iDecalNode)			
		BREAK
		
	ENDSWITCH
	
	// Draws the creation path
	DRAW_PATROL_PATH(sFMMCmenu, sCurrentVarsStruct, sFMMCMenu.iSelectedPatrol, HUD_COLOUR_RED, iPatrolNode)
	
	UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)	
	
ENDPROC

PROC DELETE_FMC_PLACED_PATROL_NODE(PATROL_NODE_CREATION_STRUCT &sPatrol, INT iPatrol, INT iNode)

	INT i
	MISSION_PLACEMENT_DATA_PATROL_NODE sTempPatrolNodeStruct
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	DELETE_PATROL_NODE_DECAL(sPatrol, iNode)	
	
	// Move all the entries down
	FOR i = iNode+1 TO MAX_NUM_PATROL_NODES-1
		sMissionPlacementData.Patrol.Patrols[iPatrol].Node[i-1]= sMissionPlacementData.Patrol.Patrols[iPatrol].Node[i]
	ENDFOR
	
	// Clear the last entry
	sMissionPlacementData.Patrol.Patrols[iPatrol].Node[MAX_NUM_PATROL_NODES-1] = sTempPatrolNodeStruct
	
	PRINTLN("[PATROL] [DECAL] DELETE_FMC_PLACED_PATROL_NODE, iPatrol = ", iPatrol, " iNode = ", iNode)
ENDPROC

PROC DELETE_FMC_PLACED_PATROL(PATROL_NODE_CREATION_STRUCT &sPatrol, INT iPatrol)

	INT i
	MISSION_PLACEMENT_DATA_PATROL sTempPatrolStruct
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	// Move all the entries down
	FOR i = iPatrol+1 TO MAX_NUM_PATROLS-1
		sMissionPlacementData.Patrol.Patrols[i-1]= sMissionPlacementData.Patrol.Patrols[i]
		DELETE_PATROL_NODE_DECAL(sPatrol, iPatrol)
	ENDFOR
	
	// Clear the last entry
	sMissionPlacementData.Patrol.Patrols[MAX_NUM_PATROLS-1] = sTempPatrolStruct
	
	PRINTLN("[PATROL] [DECAL] DELETE_FMC_PLACED_PATROL, iPatrol = ", iPatrol)
ENDPROC

PROC MAINTAIN_PLACED_FMC_PATROLS(	CREATION_VARS_STRUCT &sCurrentVarsStruct,
									structFMMC_MENU_ITEMS &sFMMCmenu,
									HUD_COLOURS_STRUCT &sHCS,
									FMMC_LOCAL_STRUCT &sFMMCdata,
									FMMC_COMMON_MENUS_VARS &sFMMCendStage,
									PATROL_NODE_CREATION_STRUCT &sPatrol)

	INT iPatrol, iPatrolNode
	BOOL bPlaneCorona = TRUE
	
	//Loop over each patrol
	FOR iPatrol = 0 TO GET_NUMBER_OF_FMC_PATROLS()-1
	
		// If it's the current patrol detect if any of it's nodes are in the corona
		IF sFMMCMenu.iSelectedPatrol = iPatrol
		AND sFMMCmenu.iEntityCreation = CREATION_TYPE_PATROL
			FOR iPatrolNode = 0 TO GET_NUMBER_OF_FMC_PATROL_NODES(sFMMCMenu.iSelectedPatrol)-1
			
				IF NOT IS_VECTOR_ZERO(sMissionPlacementData.Patrol.Patrols[iPatrol].Node[iPatrolNode].vCoord)
					IF sFMMCMenu.iSelectedEntity != iPatrolNode
					
						// Detect if the current node is in the corona
						IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_PATROL, iPatrol, NULL, bPlaneCorona, HUD_COLOUR_RED, sFMMCdata, sFMMCendStage, DEFAULT, DEFAULT, iPatrolNode)
						
							// Pick it up
							IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
								DELETE_PATROL_NODE_DECAL(sPatrol, iPatrolNode)
								PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_PATROL, iPatrolNode)
								CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
							
							// Delete it
							ELIF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
								sCurrentVarsStruct.iEntityRemoved = iPatrolNode
								DELETE_FMC_PLACED_PATROL_NODE(sPatrol, iPatrol, iPatrolNode)
								CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
								RELOOP
							ENDIF
						ENDIF
					ENDIF
				ELSE	
					DELETE_PATROL_NODE_DECAL(sPatrol, iPatrolNode)
				ENDIF
			ENDFOR
		ELSE
			// Draw the patrols
			DRAW_PATROL_PATH(sFMMCmenu, sCurrentVarsStruct, iPatrol, HUD_COLOUR_GREEN)
			
		ENDIF
	ENDFOR
ENDPROC

//Zone Placement

PROC DELETE_THIS_FMC_TRIGGER_AREA(structFMMC_MENU_ITEMS &sFMMCmenu, INT iIndex)
	
	IF sMissionPlacementData.TriggerArea.iCount > 0
		INT i
		
		MISSION_PLACEMENT_DATA_TRIGGER_AREA emptyArea
		
		sMissionPlacementData.TriggerArea.Areas[iIndex] = emptyArea
		
		FOR i = iIndex + 1 TO (sMissionPlacementData.TriggerArea.iCount - 1)
			sMissionPlacementData.TriggerArea.Areas[i-1] = sMissionPlacementData.TriggerArea.Areas[i]
			sMissionPlacementData.TriggerArea.Areas[i]	= emptyArea	
		ENDFOR
		
		sMissionPlacementData.TriggerArea.Areas[sMissionPlacementData.TriggerArea.iCount] = emptyArea
		
		sMissionPlacementData.TriggerArea.iCount--
		
		sFMMCMenu.bRefreshMenuASAP = TRUE
	ENDIF
ENDPROC

/////////////////////////////////////////////
///    TUNER CHECKPOINTS
/////////////////////////////////////////////

PROC DRAW_FMC_CHECKPOINTS_LIST_MARKER(INT& iMarkersDrawn, MISSION_PLACEMENT_DATA_CHECKPOINTS_DATA eCheckpointData)
	INT iR,iG,iB,iA
	FLOAT fRadius = eCheckpointData.fCollectRadius
	MARKER_TYPE mtMarkerType = GET_MARKER_TYPE_FROM_FMC_CHECKPOINTS_LIST_MARKER_TYPE(eCheckpointData.iMarkerType)
	SWITCH mtMarkerType
		CASE MARKER_CYLINDER
			GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
			DRAW_MARKER_FOR_CREATOR(iMarkersDrawn, mtMarkerType, eCheckpointData.vCoords, <<0.0,0.0,0.0>>, eCheckpointData.vRotation, <<fRadius, fRadius, 5.0>>, iR, iG, iB, 100)
			fRadius = eCheckpointData.fVisualRadius
			GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS,eCheckpointData.iVisualColour),iR, iG, iB, iA)
			DRAW_MARKER_FOR_CREATOR(iMarkersDrawn, mtMarkerType, eCheckpointData.vCoords, <<0.0,0.0,0.0>>, eCheckpointData.vRotation, <<fRadius, fRadius, 5.0>>, iR, iG, iB, 100)
		BREAK
		CASE MARKER_RING
			GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
			DRAW_MARKER_FOR_CREATOR(iMarkersDrawn, mtMarkerType, eCheckpointData.vCoords, <<0.0,0.0,0.0>>, eCheckpointData.vRotation, <<fRadius, fRadius, fRadius>>, iR, iG, iB, 100)
			fRadius = eCheckpointData.fVisualRadius
			GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS,eCheckpointData.iVisualColour),iR, iG, iB, iA)
			DRAW_MARKER_FOR_CREATOR(iMarkersDrawn, mtMarkerType, eCheckpointData.vCoords, <<0.0,0.0,0.0>>, eCheckpointData.vRotation, <<fRadius, fRadius, fRadius>>, iR, iG, iB, 100)
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_FMC_CHECKPOINTS_LIST_CREATION(	SELECTION_DETAILS_STRUCT 	&sSelDetails,
										CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
										structFMMC_MENU_ITEMS 		&sFMMCmenu,
										FMMC_LOCAL_STRUCT 			&sFMMCData,
										INT 						iMaximum = MAX_NUM_CHECKPOINTS )

	PRINTLN("DO_FMC_CHECKPOINTS_LIST_CREATION ")
	INT iCheckpoint
	MODEL_NAMES mnCurrent = PROP_LD_CRATE_01 // doesn't matter much the model, the object will go invisible, the object is for the override rotation module to work
	
	// Determine which checkpoint we're working with
	PRINTLN("DO_FMC_CHECKPOINTS_LIST_CREATION - sFMMCMenu.iSelectedEntity ",sFMMCMenu.iSelectedEntity)
	IF sFMMCMenu.iSelectedEntity = -1
	
		iCheckpoint = GET_NUMBER_OF_FMC_CHECKPOINTS_LISTS()
		PRINTLN("DO_FMC_CHECKPOINTS_LIST_CREATION - iCheckpoint ",iCheckpoint)
		
	ELSE
	
		iCheckpoint = sFMMCMenu.iSelectedEntity
		
		IF iCheckpoint >= iMaximum 
			iCheckpoint = iMaximum - 1
			sFMMCMenu.iSelectedEntity = iCheckpoint
		ENDIF

		// If you've picked up a goto, remove the blip of the one that was in the world so that it can be recreated as the placement one
		IF DOES_BLIP_EXIST(sCheckpointListCreationStruct.biBlips[iCheckpoint])
		
			REMOVE_BLIP(sCheckpointListCreationStruct.biBlips[iCheckpoint])
			
			// Update the edited goto data to that of the one you've just picked up
			sEditedCheckpointPlacementData = sMissionPlacementData.Checkpoint.Checkpoints[iCheckpoint]
			sFMMCmenu.sCheckpoint.fCollectRadius = sEditedCheckpointPlacementData.fCollectRadius
			sFMMCmenu.sCheckpoint.fVisualRadius = sEditedCheckpointPlacementData.fVisualRadius
			sFMMCmenu.sCheckpoint.bAirCheckpoint = sEditedCheckpointPlacementData.bAirCheckpoint
			sFMMCmenu.sCheckpoint.iMarkerType = ENUM_TO_INT(sEditedCheckpointPlacementData.iMarkerType)
			sFMMCmenu.sCheckpoint.iVisualColour = ENUM_TO_INT(sEditedCheckpointPlacementData.iVisualColour)

			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
			
		ENDIF
	ENDIF
	
	// Error for if you try to place one when too many already exist
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_CHECKPOINTS_LIST)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCMenu)
		AND sFMMCMenu.iSelectedEntity = -1
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			sCurrentVarsStruct.bDisplayFailReason = TRUE
			sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			sCurrentVarsStruct.sFailReason = "FMMC_HELP3CL"
			PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
			PRINTSTRING("MOVE_LAST_PLACED_VEHICLE ERROR SOUND EFFECT 4!!!")PRINTNL()
		ENDIF
	ENDIF
	
	IF GET_NUMBER_OF_FMC_CHECKPOINTS_LISTS() >= iMaximum
	AND iCheckpoint >= iMaximum
		sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
		sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_CHECKPOINTS_LIST )
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_FMC_CHECKPOINTS_LISTS(),  iMaximum,	"FMMC_AB_06", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_FLASHRED, 500)
		EXIT
	ELSE
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(GET_NUMBER_OF_FMC_CHECKPOINTS_LISTS(),  iMaximum,	"FMMC_AB_06", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_CHECKPOINTS_LIST )
	ENDIF
	
	// Main creation state machine
	SWITCH sSelDetails.iSwitchingINT
	
		// Wait stage to stall the creation of more if there are too many already
		CASE CREATION_STAGE_WAIT
			IF DOES_ENTITY_EXIST(sCheckpointListCreationStruct.oiCoronaObj)
				DELETE_OBJECT(sCheckpointListCreationStruct.oiCoronaObj)
			ENDIF
			
			IF iCheckpoint < iMaximum
				sSelDetails.iSwitchingINT = CREATION_STAGE_SET_UP
			ENDIF
		BREAK
		// Request the model ready for creation
		CASE CREATION_STAGE_SET_UP
			IF NOT DOES_ENTITY_EXIST(sMovingDoorStruct.oiCoronaObj)
				sSelDetails.iSwitchingINT = CREATION_STAGE_MAKE
			ELSE
				sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		
		// Create the new placement checkpoint for the corona
		CASE CREATION_STAGE_MAKE
			IF NOT DOES_ENTITY_EXIST(sCheckpointListCreationStruct.oiCoronaObj)
				IF IS_MODEL_VALID(mnCurrent)
					REQUEST_MODEL(mnCurrent)
					PRINTLN("[CHECKPOINT] Loading ", mnCurrent)
					
					IF HAS_MODEL_LOADED(mnCurrent)
						REFRESH_MENU_ASAP(sFMMCMenu)
						
						sCheckpointListCreationStruct.oiCoronaObj = CREATE_OBJECT(mnCurrent, sCurrentVarsStruct.vCoronaPos, FALSE, FALSE)
						SET_ENTITY_HEADING(sCheckpointListCreationStruct.oiCoronaObj, sCurrentVarsStruct.fCreationHeading)
						FREEZE_ENTITY_POSITION(sCheckpointListCreationStruct.oiCoronaObj, TRUE)
						SET_ENTITY_COLLISION(sCheckpointListCreationStruct.oiCoronaObj, FALSE)
						SET_ENTITY_LOD_DIST(sCheckpointListCreationStruct.oiCoronaObj, 1000)
						SET_ENTITY_VISIBLE(sCheckpointListCreationStruct.oiCoronaObj,FALSE)
						
						sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
					ENDIF
				ENDIF
			ELSE
				sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		// Do the placement of the goto, updating it's position based on user input
		CASE CREATION_STAGE_PLACE
					
			// Handle the override placement where you can input the exact position/rotation in the menus
			HANDLE_ENTITY_CREATION_OVERRIDES(sFMMCMenu, sFMMCData, sCurrentVarsStruct, sCheckpointListCreationStruct.oiCoronaObj, sEditedCheckpointPlacementData.iBitset[GET_LONG_BITSET_INDEX(iCheckpoint)], ENUM_TO_INT(eCHECKPOINTLISTDATABITSET_OVERRIDE_POSITION), ENUM_TO_INT(eCHECKPOINTLISTDATABITSET_OVERRIDE_ROTATION), FALSE, FALSE, FALSE)
			
			// Checks if the place button has been pressed and that it's a valid place to go
			IF MOVE_LAST_PLACED_CHECKPOINT(	sFMMCmenu,
											sCheckpointListCreationStruct.oiCoronaObj,
											sEditedCheckpointPlacementData.vCoords,
											sCurrentVarsStruct,
											sFMMCData,
											iCheckpoint,
											IS_LONG_BIT_SET(sEditedCheckpointPlacementData.iBitset,ENUM_TO_INT(eCHECKPOINTLISTDATABITSET_OVERRIDE_ROTATION)))

				PRINTLN("sSelDetails.iSwitchingINT = CREATION_STAGE_DONE")
				sSelDetails.iSwitchingINT = CREATION_STAGE_DONE
				
			ENDIF
			
			// Update the position
			sEditedCheckpointPlacementData.vCoords = sCurrentVarsStruct.vCoronaPos
			sEditedCheckpointPlacementData.vRotation = GET_ENTITY_ROTATION(sCheckpointListCreationStruct.oiCoronaObj)
			DRAW_FMC_CHECKPOINTS_LIST_MARKER(sFMMCmenu.iCurrentMarkersDrawn,sEditedCheckpointPlacementData)
		BREAK
		
		// Update the goto you've just placed, increase the counter and reset for the next one
		CASE CREATION_STAGE_DONE
		
			sEditedCheckpointPlacementData.fCollectRadius = sFMMCmenu.sCheckpoint.fCollectRadius
			sEditedCheckpointPlacementData.fVisualRadius = sFMMCmenu.sCheckpoint.fVisualRadius
			sEditedCheckpointPlacementData.bAirCheckpoint = sFMMCmenu.sCheckpoint.bAirCheckpoint
			sEditedCheckpointPlacementData.bShowChevron = sFMMCmenu.sCheckpoint.bShowChevron
			sEditedCheckpointPlacementData.iMarkerType = INT_TO_ENUM(FMC_CHECKPOINTS_LIST_MARKER_TYPE, sFMMCmenu.sCheckpoint.iMarkerType)
			sEditedCheckpointPlacementData.iVisualColour = INT_TO_ENUM(HUD_COLOURS,sFMMCmenu.sCheckpoint.iVisualColour)
//			sEditedCheckpointPlacementData.iChevronColour = sFMMCmenu.iCheckpointChevronColour
//			sEditedCheckpointPlacementData.iChevronType = sFMMCmenu.iCheckpointChevronType
			sMissionPlacementData.Checkpoint.Checkpoints[iCheckpoint]	= sEditedCheckpointPlacementData
			PRINTLN("[LOCS] Creating checkpoint ",iCheckpoint," with pos ", sEditedCheckpointPlacementData.vCoords," collect radius ", sEditedCheckpointPlacementData.fCollectRadius," visual radius ", sEditedCheckpointPlacementData.fVisualRadius)
			
			START_PLACEMENT_COOLDOWN_TIMER(sCurrentVarsStruct)
			
			CREATE_FMMC_BLIP(sCheckpointListCreationStruct.biBlips[iCheckpoint], sMissionPlacementData.Checkpoint.Checkpoints[iCheckpoint].vCoords, HUD_COLOUR_YELLOW, "FMC_B_CHECK", 1)
			
			SET_BIT(sLocStruct.iBS_Editted, iCheckpoint)
			
			CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)
			
			IF sFMMCMenu.iSelectedEntity = -1
				RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			ENDIF
			sFMMCMenu.iSelectedEntity = -1
			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT

			CLEAR_LONG_BIT(sEditedCheckpointPlacementData.iBitset, ENUM_TO_INT(eCHECKPOINTLISTDATABITSET_OVERRIDE_ROTATION))
			
		BREAK
		
	ENDSWITCH
	
	UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)	
	
ENDPROC

PROC DELETE_FMC_PLACED_CHECKPOINTS_LIST(INT iCheckpoint)

	INT i
	MISSION_PLACEMENT_DATA_CHECKPOINTS_DATA sTempCheckpointStruct
	
	// Play delete sound
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	// Remove the entity/blip
	IF DOES_BLIP_EXIST(sCheckpointListCreationStruct.biBlips[iCheckpoint])
		REMOVE_BLIP(sCheckpointListCreationStruct.biBlips[iCheckpoint])
	ENDIF
	
	// Move all the entries down
	FOR i = iCheckpoint+1 TO MAX_NUM_CHECKPOINTS -1
		//sLocStruct.biGoToBlips[i-1] = sLocStruct.biGoToBlips[i]
		sMissionPlacementData.Checkpoint.Checkpoints[i-1]= sMissionPlacementData.Checkpoint.Checkpoints[i]
	ENDFOR
	
	// Clear the last entry
	sMissionPlacementData.Checkpoint.Checkpoints[MAX_NUM_CHECKPOINTS -1] = sTempCheckpointStruct
	sCheckpointListCreationStruct.biBlips[MAX_NUM_CHECKPOINTS-1] = NULL
	
ENDPROC

PROC DRAW_THICK_LINE(VECTOR VecCoorsFirst, VECTOR VecCoorsSecond, FLOAT fWidth, int red = 0, int green = 0, int blue = 255, int alpha_param = 255)
	VecCoorsFirst.z += 1.0
	VecCoorsSecond.z += 1.0
	VECTOR v1a,v1b,v2a,v2b
	VECTOR vDir,vRight,vUp
	vUp = <<0,0,1>>
	vDir = NORMALISE_VECTOR(VecCoorsSecond - VecCoorsFirst)
	vRight = CROSS_PRODUCT(vDir,vUp)
	v1a = VecCoorsFirst + vRight * fWidth * 0.5
	v1b = VecCoorsFirst - vRight * fWidth * 0.5
	v2a = VecCoorsSecond + vRight * fWidth * 0.5
	v2b = VecCoorsSecond - vRight * fWidth * 0.5
	DRAW_DEBUG_POLY(v1b,v1a,v2b,red,green,blue,alpha_param)
	DRAW_DEBUG_POLY(v2a,v2b,v1a,red,green,blue,alpha_param)
ENDPROC

FUNC BOOL IS_CHECKPOINTS_LIST_LINE_IN_CORONA(structFMMC_MENU_ITEMS &sFMMCmenu, CREATION_VARS_STRUCT &sCurrentVarsStruct, FLOAT fRadius = 1.0, INT iNumber = 0)
	
	VECTOR Corona
	VECTOR Vector1
	VECTOR Vector2
	VECTOR VReturnVec
	FLOAT fDistanceFromLine, fDist
	
	Corona = sCurrentVarsStruct.vCoronaPos
	Vector1 = sMissionPlacementData.Checkpoint.Checkpoints[iNumber].vCoords
	Vector2 = sMissionPlacementData.Checkpoint.Checkpoints[iNumber-1].vCoords
	
	VReturnVec = GET_CLOSEST_POINT_ON_LINE(Corona,Vector1,Vector2)
	
	// if we can select any of the coronas, we cannot select the line
	IF IS_FMC_ENTITY_IN_CORONA(sFMMCmenu, NULL, sCurrentVarsStruct, fRadius, iNumber, CREATION_TYPE_CHECKPOINTS_LIST)
	OR IS_FMC_ENTITY_IN_CORONA(sFMMCmenu, NULL, sCurrentVarsStruct, fRadius, iNumber-1, CREATION_TYPE_CHECKPOINTS_LIST)
		RETURN FALSE
	ENDIF
		
	fDistanceFromLine = 2.5
	fDist = GET_DISTANCE_BETWEEN_COORDS(VReturnVec, Corona)
	IF fDist < fDistanceFromLine
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CAN_CHECKPOINTS_LIST_BE_INSERT_ON_LINE(structFMMC_MENU_ITEMS &sFMMCmenu, CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iNumber = 0)
	IF sFMMCmenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION
	OR sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_ROTATION
	OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciFMC_CHECKPOINTS_LIST_ROTATION
	OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_FMC_OVERRIDE_USE_OVERRIDE
		RETURN FALSE
	ENDIF
	
	// Lines are between the current and previous item on the array, there's nothing before the item 0
	IF iNumber <= 0
		RETURN FALSE
	ENDIF
	// We can't insert a new item, if we reached the max
	IF GET_NUMBER_OF_FMC_CHECKPOINTS_LISTS() >= MAX_NUM_CHECKPOINTS
		RETURN FALSE
	ENDIF
	// We can't insert a new item, if we on top of another checkpoint (we will edit it instead)
	IF NOT IS_CHECKPOINTS_LIST_LINE_IN_CORONA(sFMMCmenu,sCurrentVarsStruct,5.0,iNumber)
		RETURN FALSE
	ENDIF
	// We can't insert a new item, if we are editing a checkpoint
	IF sFMMCMenu.iSelectedEntity != -1
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC INSERT_FMC_CHECKPOINT_LIST(INT iInsertIndex)
	INT iCheckpoint
	MISSION_PLACEMENT_DATA_CHECKPOINTS_DATA sPrevious, sTemp
	// We loop through all items + 1 (we checked earlier if the array allowed us to insert a new item)
	REPEAT GET_NUMBER_OF_FMC_CHECKPOINTS_LISTS()+1 iCheckpoint
		IF iCheckpoint < iInsertIndex
			// We do nothing as the items on this position are not changing their index
		ELIF iCheckpoint = iInsertIndex
			// We copy the item that was supposed to go on this position on sPrevious, and write what we have on the corona
			sPrevious = sMissionPlacementData.Checkpoint.Checkpoints[iCheckpoint]
			sMissionPlacementData.Checkpoint.Checkpoints[iCheckpoint] = sEditedCheckpointPlacementData
		ELSE
			// We copy the item that was supposed to go on this position on sTemp, and write what we have on sPrevious as it has to go before the current one
			sTemp = sMissionPlacementData.Checkpoint.Checkpoints[iCheckpoint]
			sMissionPlacementData.Checkpoint.Checkpoints[iCheckpoint] = sPrevious
			// Then we copy it to sPrevious, so the next loop writes it on the next position
			sPrevious = sTemp
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_PLACED_FMC_CHECKPOINTS_LIST(	CREATION_VARS_STRUCT &sCurrentVarsStruct,
											structFMMC_MENU_ITEMS &sFMMCmenu,
											HUD_COLOURS_STRUCT &sHCS,
											FMMC_LOCAL_STRUCT &sFMMCdata,
											FMMC_COMMON_MENUS_VARS &sFMMCendStage)
	INT i, iR, iG, iB, iA
	FLOAT fGroundZ
	BOOL bPlaneCorona = TRUE
	BOOL bDrawCorona = (sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION AND sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION)
	BOOL bCoronaOnLine
	VECTOR vCurrent,vPrevious
	// Loop over all gotos
	FOR i = 0 TO MAX_NUM_CHECKPOINTS -1
	
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.Checkpoint.Checkpoints[i].vCoords)
			IF bDrawCorona
				IF IS_FMC_ENTITY_IN_CORONA(sFMMCmenu, NULL, sCurrentVarsStruct, 5.0, i, CREATION_TYPE_CHECKPOINTS_LIST)
					IF sFMMCmenu.iEntityCreation = CREATION_TYPE_CHECKPOINTS_LIST
						sFMMCMenu.iCopyFromEntity = i
						IF sFMMCMenu.iSelectedEntity != i
							DRAW_ON_SCREEN_ENTITY_INFO(sFMMCmenu, sFMMCMenu.iCopyFromEntity, CREATION_TYPE_CHECKPOINTS_LIST)
						ENDIF
					ENDIF
				ENDIF
			
				IF sFMMCmenu.iEntityCreation != CREATION_TYPE_CHECKPOINTS_LIST
				OR sFMMCMenu.iSelectedEntity != i
				
					IF NOT (sFMMCmenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION
						OR sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_ROTATION
						OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciFMC_CHECKPOINTS_LIST_ROTATION
						OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_FMC_OVERRIDE_USE_OVERRIDE)
						// Check if corona is on line between checkpoints
						IF CAN_CHECKPOINTS_LIST_BE_INSERT_ON_LINE(sFMMCmenu,sCurrentVarsStruct,i)
							bCoronaOnLine = TRUE
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_ACCEPT_BUTTON(sFMMCdata)) OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu)
								sHCS.hcCurrentCoronaColour = sHCS.hcDefaultCoronaColour
								sFMMCendStage.bMajorEditOnLoadedMission = TRUE
								sCurrentVarsStruct.iReSelectTimer = GET_GAME_TIMER() + 200
								SET_BIT(iLocalBitSet, biCurrentlySelectedAnEntity)
								// We insert a new checkpoint
								INSERT_FMC_CHECKPOINT_LIST(i)
								// And edit it immediately, so it won't be placed straight away, it will split the line between the checkpoints and allow to place it anywere.
								PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_CHECKPOINTS_LIST, i)
							ENDIF
						ELSE
							bCoronaOnLine = FALSE
						ENDIF
						
						// Check if it's in the corona
						IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_CHECKPOINTS_LIST, i, NULL, bPlaneCorona, GET_GOTO_LOC_HUD_COLOUR(i), sFMMCdata, sFMMCendStage,DEFAULT,NOT bCoronaOnLine)
							IF sFMMCmenu.iEntityCreation = CREATION_TYPE_CHECKPOINTS_LIST
								sFMMCMenu.iCopyFromEntity = i
								IF sFMMCMenu.iSelectedEntity != i
									DRAW_ON_SCREEN_ENTITY_INFO(sFMMCmenu, sFMMCMenu.iCopyFromEntity, CREATION_TYPE_CHECKPOINTS_LIST)
								ENDIF
							ENDIF
						
							//Pickup
							IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
								PRINTLN("MAINTAIN_PLACED_FMC_CHECKPOINTS_LIST, biPickupButtonPressed ", i)
								PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_CHECKPOINTS_LIST, i)
								// calculate the height from the ground the checkpoint is
								GET_GROUND_Z_FOR_3D_COORD(sCurrentVarsStruct.vCoronaPos+<<0,0,5>>, fGroundZ, FALSE)
								sFMMCmenu.fCreationHeightIncrease = sMissionPlacementData.Checkpoint.Checkpoints[i].vCoords.z - fGroundZ
								// copy the data from the picked checkpoint to the data used to move the checkpoint when placing it
								sFMMCmenu.sCheckpoint.bAirCheckpoint = sMissionPlacementData.Checkpoint.Checkpoints[i].bAirCheckpoint
								sFMMCmenu.sCheckpoint.bShowChevron = sMissionPlacementData.Checkpoint.Checkpoints[i].bShowChevron
								sFMMCmenu.sCheckpoint.fCollectRadius = sMissionPlacementData.Checkpoint.Checkpoints[i].fCollectRadius
								sFMMCmenu.sCheckpoint.fVisualRadius = sMissionPlacementData.Checkpoint.Checkpoints[i].fVisualRadius
								sFMMCmenu.sCheckpoint.iMarkerType = ENUM_TO_INT(sMissionPlacementData.Checkpoint.Checkpoints[i].iMarkerType)
								sFMMCmenu.sCheckpoint.iVisualColour = ENUM_TO_INT(sMissionPlacementData.Checkpoint.Checkpoints[i].iVisualColour)
								sFMMCmenu.vOverrideRotation = sEditedCheckpointPlacementData.vRotation
								sCurrentVarsStruct.fCreationHeading = sEditedCheckpointPlacementData.vRotation.z
								sEditedCheckpointPlacementData = sMissionPlacementData.Checkpoint.Checkpoints[i]
								CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
							// Delete
							ELIF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
								PRINTLN("MAINTAIN_PLACED_FMC_CHECKPOINTS_LIST, biDeleteButtonPressed ", i)
								sCurrentVarsStruct.iEntityRemoved = i
								DELETE_FMC_PLACED_CHECKPOINTS_LIST(i)
								CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
								RELOOP
							ENDIF
						ENDIF
					ENDIF
					
					// Give it a blip
					IF NOT DOES_BLIP_EXIST(sCheckpointListCreationStruct.biBlips[i])
						CREATE_FMMC_BLIP(sCheckpointListCreationStruct.biBlips[i], sMissionPlacementData.Checkpoint.Checkpoints[i].vCoords, HUD_COLOUR_YELLOW, "FMC_B_CHECK", 1)
					ENDIF
					
					DRAW_FMC_CHECKPOINTS_LIST_MARKER(sFMMCmenu.iCurrentMarkersDrawn,sMissionPlacementData.Checkpoint.Checkpoints[i])
				ENDIF
				
				IF i > 0
					IF bCoronaOnLine
						GET_HUD_COLOUR(HUD_COLOUR_GOLD,iR,iG,iB,iA)
					ELSE
						GET_HUD_COLOUR(HUD_COLOUR_BLUELIGHT,iR,iG,iB,iA)
					ENDIF
					
					vPrevious = sMissionPlacementData.Checkpoint.Checkpoints[i-1].vCoords
					IF sFMMCMenu.iSelectedEntity = i-1
						vPrevious = sEditedCheckpointPlacementData.vCoords
					ENDIF
					vCurrent = sMissionPlacementData.Checkpoint.Checkpoints[i].vCoords
					IF sFMMCMenu.iSelectedEntity = i
						vCurrent = sEditedCheckpointPlacementData.vCoords
					ENDIF
										
					DRAW_THICK_LINE(vPrevious,vCurrent,0.5,iR, iG, iB, iA)
				ENDIF					
			
			ENDIF
			
		ENDIF
	ENDFOR
ENDPROC


FUNC BOOL IS_POINT_IN_FMC_ZONE(INT iShape, VECTOR vPoint, VECTOR vMin, VECTOR vMax, FLOAT fSize = 0.0)
	
	SWITCH iShape
		CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
		
			DRAW_DEBUG_LINE(vPoint, vMax)
			IF 	vPoint.x > vMin.x AND vPoint.x < vMax.x
			AND vPoint.y > vMin.y AND vPoint.y < vMax.y
			AND	vPoint.z > vMin.z AND vPoint.z < vMax.z
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciFMMC_ZONE_SHAPE__SPHERE
			IF VDIST2(vPoint, vMin) < POW(fSize,2)
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
			FLOAT fDistTo0, fDistTo1, fDist0To1
			fDistTo0 = VDIST(vPoint, vMin)
			fDistTo1 = VDIST(vPoint, vMax)
			fDist0To1 = VDIST(vMin, vMax)
			IF fDistTo0 < fSize * 1.415
			OR fDistTo1 < fSize * 1.415
			OR fDistTo0 < fDist0To1 * 1.415
			OR fDistTo1 < fDist0To1 * 1.415
				IF IS_POINT_IN_ANGLED_AREA(vPoint, vMin, vMax, fSize)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CURRENT_MENU_A_FMC_ZONE_MENU(structFMMC_MENU_ITEMS &sFMMCmenu)

	SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_FMC_TRIGGER_AREA_OPTIONS
		CASE eFmmc_FMC_POPULATIONS_LIST_MENU
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_FMC_ZONE_ROUGH_SIZE(INT iZoneShape, VECTOR vMin, VECTOR vMax, FLOAT fZoneSize = 0.0)
	
	FLOAT fSize = 0.0
	
	IF DOES_ZONE_SHAPE_USE_TWO_POSITIONS(iZoneShape)
		fSize += VDIST(vMin, vMax)
	ENDIF
	
	IF DOES_ZONE_SHAPE_USE_WIDTH(iZoneShape)
		fSize += fZoneSize
	ENDIF
	
	IF DOES_ZONE_SHAPE_USE_RADIUS(iZoneShape)
		fSize += fZoneSize
	ENDIF
	
	
	RETURN fSize
ENDFUNC

PROC DRAW_FMC_ZONE(VECTOR vCustomPosToDraw0, VECTOR vCustomPosToDraw1, INT iShape, FLOAT fSize = 0.0, FLOAT fAlpha = 30.0, HUD_COLOURS eCustomColour = HUD_COLOUR_PURE_WHITE)
	
	VECTOR vZonePositionsToUse[ciFMMC_ZONE_MAX_POSITIONS]
	
	IF NOT IS_VECTOR_ZERO(vCustomPosToDraw0)
		vZonePositionsToUse[0] = vCustomPosToDraw0
	ENDIF
	IF NOT IS_VECTOR_ZERO(vCustomPosToDraw1)
		vZonePositionsToUse[1] = vCustomPosToDraw1
	ENDIF

	FLOAT fZoneRadiusToUse = fSize
	
	INT iR, iG, iB
	IF eCustomColour != HUD_COLOUR_PURE_WHITE
		INT iDummyAlpha = -1
		GET_HUD_COLOUR(eCustomColour, iR, iG, iB, iDummyAlpha)
	ELSE
		GET_ZONE_COLOURS(iShape, iR, iG, iB)
	ENDIF
	
	SWITCH iShape
		CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
			DRAW_BOX(vZonePositionsToUse[0], vZonePositionsToUse[1], iR, iG, iB, ROUND(fAlpha))
		BREAK
		
		CASE ciFMMC_ZONE_SHAPE__SPHERE
			DRAW_MARKER(MARKER_SPHERE, vZonePositionsToUse[0], EMPTY_VEC(), EMPTY_VEC(), <<fZoneRadiusToUse, fZoneRadiusToUse, fZoneRadiusToUse>>, iR, iG, iB, ROUND(fAlpha))
		BREAK
		
		CASE ciFMMC_ZONE_SHAPE__CYLINDER
			DRAW_CYLINDER(vZonePositionsToUse[0], vZonePositionsToUse[1], fZoneRadiusToUse, iR, iG, iB, ROUND(fAlpha))
		BREAK
		
		CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
			fAlpha *= 0.35 // Angled areas are naturally higher alpha than the other shapes
			DRAW_ANGLED_AREA_FROM_FACES(vZonePositionsToUse[0], vZonePositionsToUse[1], fZoneRadiusToUse, iR, iG, iB, ROUND(fAlpha))
		BREAK
	ENDSWITCH
	
ENDPROC

PROC MAINTAIN_PLACED_FMC_ZONES(INT iZoneType, CREATION_VARS_STRUCT &sCurrentVarsStruct, structFMMC_MENU_ITEMS &sFMMCmenu)

	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		EXIT
	ENDIF
	
	BOOL bCanModifyZones = IS_CURRENT_MENU_A_FMC_ZONE_MENU(sFMMCmenu)
	
	FLOAT fSmallestZoneSoFar = 99999.9
	
	INT iCount
	IF iZoneType = ciFMC_ZONE_TYPE_TRIGGER_AREA
		iCount = sMissionPlacementData.TriggerArea.iCount
	ELIF iZoneType = ciFMC_ZONE_TYPE_POPULATION_ZONE
		iCount = GET_NUMBER_OF_FMC_POPULATION_BLOCKERS()
	ELSE
		EXIT
	ENDIF
	
	IF iCount = 0
		EXIT
	ENDIF
	
	VECTOR vMin, vMax
	FLOAT fSize
	INT iShape
	
	INT iZone
	FOR iZone = 0 TO iCount - 1
		
		IF iZoneType = ciFMC_ZONE_TYPE_TRIGGER_AREA
			vMin = sMissionPlacementData.TriggerArea.Areas[iZone].vMin
			vMax = sMissionPlacementData.TriggerArea.Areas[iZone].vMax
			fSize = sMissionPlacementData.TriggerArea.Areas[iZone].fWidth
			iShape = ciFMMC_ZONE_SHAPE__ANGLED_AREA
		ENDIF
		
		IF bCanModifyZones
			IF IS_POINT_IN_FMC_ZONE(iShape, sCurrentVarsStruct.vCoronaPos, vMin, vMax, fSize)
			AND GET_FMC_ZONE_ROUGH_SIZE(iShape, vMin, vMax, fSize) < fSmallestZoneSoFar
				fSmallestZoneSoFar = GET_FMC_ZONE_ROUGH_SIZE(iShape, vMin, vMax, fSize)
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iZone = 0 TO iCount - 1
		
		IF iZoneType = ciFMC_ZONE_TYPE_TRIGGER_AREA
			vMin = sMissionPlacementData.TriggerArea.Areas[iZone].vMin
			vMax = sMissionPlacementData.TriggerArea.Areas[iZone].vMax
			fSize = sMissionPlacementData.TriggerArea.Areas[iZone].fWidth
			iShape = ciFMMC_ZONE_SHAPE__ANGLED_AREA
		ELIF iZoneType = ciFMC_ZONE_TYPE_POPULATION_ZONE
			vMin = sMissionPlacementData.Population.Blockers[iZone].vMin
			vMax = sMissionPlacementData.Population.Blockers[iZone].vMax
			iShape = ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
		ENDIF
	
		BOOL bFlashThisZone = FALSE
		
		FLOAT fAlpha = 35.0
		
		IF IS_CURRENT_MENU_A_FMC_ZONE_MENU(sFMMCmenu)
		AND sFMMCmenu.iCurrentSelection = iZone
			bFlashThisZone = TRUE
		ENDIF
		
		IF bFlashThisZone
			fAlpha += 55
			fAlpha += 80 * GET_ANIMATED_SINE_VALUE(20)
		ENDIF
		
		DRAW_FMC_ZONE(vMin, vMax, iShape, fSize, fAlpha)
	ENDFOR
	
ENDPROC

PROC PROCESS_FMC_DRAWING_CURRENT_ZONE(FMC_ZONE_DATA &sZone, CREATION_VARS_STRUCT &sCurrentVarsStruct, structFMMC_MENU_ITEMS &sFMMCmenu)
	
	FLOAT fMinimumSideSize = 0.5
	FLOAT fMinimumSideSize_Z = 1.0
	
	IF NOT IS_ROCKSTAR_DEV()
		fMinimumSideSize = 1.0
		fMinimumSideSize_Z = 15.0
	ENDIF
	
	IF sFMMCmenu.iHighlightedEntity > -1
	AND NOT CURRENTLY_EDITING_A_PLACED_ZONE(sFMMCMenu)
		EXIT
	ENDIF
	
	VECTOR vTempZonePositions[ciFMMC_ZONE_MAX_POSITIONS]

	vTempZonePositions[0] = sZone.vMin
	vTempZonePositions[1] = sZone.vMax
				
	SWITCH sZone.iShape
		
		CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX			
		CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
		
			IF IS_VECTOR_ZERO(sZone.vMin) // We place position 0 first
				
				vTempZonePositions[0] = sCurrentVarsStruct.vCoronaPos
				
			ELIF IS_VECTOR_ZERO(sZone.vMax)
				
				vTempZonePositions[1] = sCurrentVarsStruct.vCoronaPos
				
				IF ABSF(sFMMCMenu.sMenuZone.vPos[0].x - vTempZonePositions[1].x) < fMinimumSideSize
					vTempZonePositions[1].x = sZone.vMin.x + fMinimumSideSize
				ENDIF
				IF ABSF(sFMMCMenu.sMenuZone.vPos[0].y - vTempZonePositions[1].y) < fMinimumSideSize
					vTempZonePositions[1].y = sZone.vMin.y + fMinimumSideSize
				ENDIF
				IF ABSF(sFMMCMenu.sMenuZone.vPos[0].z - vTempZonePositions[1].z) < fMinimumSideSize_Z
					vTempZonePositions[1].z = sZone.vMin.z + fMinimumSideSize_Z
				ENDIF

			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF !IS_VECTOR_ZERO(vTempZonePositions[0])
		DRAW_FMC_ZONE(vTempZonePositions[0], vTempZonePositions[1], sZone.iShape, sZone.fSize, 125 + (90 * GET_ANIMATED_SINE_VALUE(15)))
	ENDIF
ENDPROC

PROC PROCESS_TRIGGER_AREA_OPTIONS_EVERY_FRAME(structFMMC_MENU_ITEMS &sFMMCMenu, CREATION_VARS_STRUCT &sCurrentVarsStruct)
	IF sFMMCMenu.sActiveMenu = eFmmc_FMC_TRIGGER_AREA_OPTIONS
		FMC_ZONE_DATA sZoneData
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.TriggerArea.Areas[sFMMCMenu.iSelectedFMCEntity].vMin)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciFMC_TRIGGER_AREA_POS_1
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciFMC_TRIGGER_AREA_POS_2
			OR NOT IS_VECTOR_ZERO(sMissionPlacementData.TriggerArea.Areas[sFMMCMenu.iSelectedFMCEntity].vMax)
				sZoneData.vMin = sMissionPlacementData.TriggerArea.Areas[sFMMCMenu.iSelectedFMCEntity].vMin
				sZoneData.vMax = sMissionPlacementData.TriggerArea.Areas[sFMMCMenu.iSelectedFMCEntity].vMax
				sZoneData.fSize = sMissionPlacementData.TriggerArea.Areas[sFMMCMenu.iSelectedFMCEntity].fWidth
				sZoneData.iShape = ciFMMC_ZONE_SHAPE__ANGLED_AREA
				
				PROCESS_FMC_DRAWING_CURRENT_ZONE(sZoneData, sCurrentVarsStruct, sFMMCmenu)
			ENDIF
		ENDIF	
	ENDIF
	
	SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
		CASE ciFMC_TRIGGER_AREA_WIDTH
			EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sMissionPlacementData.TriggerArea.Areas[sFMMCMenu.iSelectedFMCEntity].fWidth, 0.75, 200)
		BREAK
		CASE ciFMC_TRIGGER_AREA_HEIGHT_1
			EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sMissionPlacementData.TriggerArea.Areas[sFMMCMenu.iSelectedFMCEntity].vMin.z, -10000.0, 10000.0)
		BREAK
		CASE ciFMC_TRIGGER_AREA_HEIGHT_2
			EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sMissionPlacementData.TriggerArea.Areas[sFMMCMenu.iSelectedFMCEntity].vMax.z, -10000.0, 10000.0)
		BREAK
	ENDSWITCH
	
	DRAW_GENERIC_BIG_DOUBLE_NUMBER(sMissionPlacementData.TriggerArea.iCount, MAX_NUM_TRIGGER_AREAS, "FMC_TA_T")
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_DRAW_PED_SEEING_ANGLE(MISSION_PLACEMENT_DATA_PEDS& sPedsPlacementData)
	VECTOR vCoords 
	
	IF IS_ENTITY_ALIVE(sPedStruct.piTempPed)
	AND WAS_PED_SKELETON_UPDATED(sPedStruct.piTempPed)
		vCoords = GET_PED_BONE_COORDS(sPedStruct.piTempPed, BONETAG_HEAD, <<0.0, 0.0, 0.0>>)
	ELSE
		vCoords = sPedsPlacementData.vCoords + <<0.0, 0.0, 2.0>>
	ENDIF
	
	FLOAT fAngle = sPedsPlacementData.fSeeingAngle * 2.0 //fSeeingAngle is passed to SET_PED_VISUAL_FIELD_CENTER_ANGLE, which uses it as a min/max horizontal angle, so it will need to be doubled for DEBUG_DRAW_CIRCLE_SECTOR.
	
	DEBUG_DRAW_CIRCLE_SECTOR(vCoords, sPedsPlacementData.fHeading, sPedsPlacementData.fSeeingRange, fAngle, 255, 255, 0)
ENDPROC

#ENDIF

PROC PROCESS_FMC_PED_OPTIONS_EVERY_FRAME(structFMMC_MENU_ITEMS &sFMMCMenu, MISSION_PLACEMENT_DATA_PEDS &sPedsPlacementData)

	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_GREYLIGHT, iR, iG, iB, iA)
	
	SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
		CASE ciPLACE_PED_DEFENSIVE
			EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sPedsPlacementData.fDefensiveRadius, -1.0, 100.0)
			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, sPedsPlacementData.vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<sPedsPlacementData.fDefensiveRadius, sPedsPlacementData.fDefensiveRadius, sPedsPlacementData.fDefensiveRadius>>, iR, iG, iB, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		BREAK
		CASE ciPLACE_PED_SEEING
			EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sPedsPlacementData.fSeeingRange, 0.0, 1000.0)
			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, sPedsPlacementData.vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<sPedsPlacementData.fSeeingRange, sPedsPlacementData.fSeeingRange, sPedsPlacementData.fSeeingRange>>, iR, iG, iB, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		BREAK
		CASE ciPLACE_PED_SEEING_A
			EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sPedsPlacementData.fSeeingAngle, 0.0, 1000.0)
			
			#IF IS_DEBUG_BUILD
			DEBUG_DRAW_PED_SEEING_ANGLE(sPedsPlacementData)
			#ENDIF
		BREAK
		CASE ciPLACE_PED_HEARING
			EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sPedsPlacementData.fHearingRange, 0.0, 1000.0)
			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, sPedsPlacementData.vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<sPedsPlacementData.fHearingRange, sPedsPlacementData.fHearingRange, sPedsPlacementData.fHearingRange>>, iR, iG, iB, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		BREAK	
	ENDSWITCH
	
	DRAW_PED_VISION_CONES(sFMMCMenu)
		
ENDPROC

FUNC VECTOR GET_FOCUS_CAM_INITIAL_COORDS(INT iFocusCam)
	SWITCH sMissionPlacementData.FocusCam.Cam[iFocusCam].eFocusType
		CASE ET_NONE
			RETURN sMissionPlacementData.FocusCam.Cam[iFocusCam].vCoords
		
		CASE ET_OBJECT
			IF sMissionPlacementData.FocusCam.Cam[iFocusCam].iEntityIndex > -1
			AND sMissionPlacementData.FocusCam.Cam[iFocusCam].iEntityIndex < sMissionPlacementData.Prop.iCount
				RETURN sMissionPlacementData.Prop.Props[sMissionPlacementData.FocusCam.Cam[iFocusCam].iEntityIndex].vCoords
			ENDIF
		BREAK
		
		CASE ET_PED
			IF sMissionPlacementData.FocusCam.Cam[iFocusCam].iEntityIndex > -1
			AND sMissionPlacementData.FocusCam.Cam[iFocusCam].iEntityIndex < sMissionPlacementData.Ped.iCount
				RETURN sMissionPlacementData.Ped.Peds[sMissionPlacementData.FocusCam.Cam[iFocusCam].iEntityIndex].vCoords
			ENDIF
		BREAK
		
		CASE ET_VEHICLE
			IF sMissionPlacementData.FocusCam.Cam[iFocusCam].iEntityIndex > -1
			AND sMissionPlacementData.FocusCam.Cam[iFocusCam].iEntityIndex < sMissionPlacementData.Vehicle.iCount
				RETURN sMissionPlacementData.Vehicle.Vehicles[sMissionPlacementData.FocusCam.Cam[iFocusCam].iEntityIndex].vCoords
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC PROCESS_FOCUS_CAMS_OPTIONS_EVERY_FRAME(structFMMC_MENU_ITEMS &sFMMCMenu)
	IF sFMMCMenu.sActiveMenu = eFmmc_FMC_FOCUS_CAMS_UPDATE_MENU
		VECTOR vPos = GET_FOCUS_CAM_INITIAL_COORDS(sFMMCMenu.iSelectedFMCEntity)
		IF NOT IS_VECTOR_ZERO(vPos)
			DRAW_FMC_ZONE(vPos, EMPTY_VEC(), ciFMMC_ZONE_SHAPE__SPHERE, sMissionPlacementData.FocusCam.Cam[sFMMCMenu.iSelectedFMCEntity].fRange, 67, HUD_COLOUR_YELLOWDARK)
		ENDIF	
	ENDIF
	
	SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
		CASE ciFMC_FCS_RANGE
			IF CUSTOM_FOCUS_CAM(sFMMCMenu)
			OR MAX_ENTITY_ID(sMissionPlacementData.FocusCam.Cam[sFMMCMenu.iSelectedFMCEntity].eFocusType) > 0
				EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sMissionPlacementData.FocusCam.Cam[sFMMCMenu.iSelectedFMCEntity].fRange, 0.5, 2000)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_FMC_TARGET_OPTIONS_EVERY_FRAME(structFMMC_MENU_ITEMS &sFMMCMenu)
	
	IF sMissionPlacementData.TakeOutTarget[sFMMCMenu.iSelectedFMCEntity].iCount != GET_NUM_TARGET_ENTITIES(sFMMCMenu.iSelectedFMCEntity)
		sMissionPlacementData.TakeOutTarget[sFMMCMenu.iSelectedFMCEntity].iCount = GET_NUM_TARGET_ENTITIES(sFMMCMenu.iSelectedFMCEntity)
		PRINTLN("[TARGETS] [UPDATE_TARGETS] PROCESS_FMC_TARGET_OPTIONS_EVERY_FRAME, iCount = ", sMissionPlacementData.TakeOutTarget[sFMMCMenu.iSelectedFMCEntity].iCount)	
	ENDIF
	
ENDPROC

PROC PROCESS_FMC_COUNTS_EVERY_FRAME()

	IF sMissionPlacementData.GoToPoint.iCount != GET_NUMBER_OF_FMC_GOTOS()
		sMissionPlacementData.GoToPoint.iCount = GET_NUMBER_OF_FMC_GOTOS()
		PRINTLN("[PROCESS_FMC_COUNTS_EVERY_FRAME] sMissionPlacementData.GoToPoint.iCount = ", sMissionPlacementData.GoToPoint.iCount)
	ENDIF
	
	IF sMissionPlacementData.Population.iCount != GET_NUMBER_OF_FMC_POPULATION_BLOCKERS()
		sMissionPlacementData.Population.iCount = GET_NUMBER_OF_FMC_POPULATION_BLOCKERS()
		PRINTLN("[PROCESS_FMC_COUNTS_EVERY_FRAME] sMissionPlacementData.Population.iCount = ", sMissionPlacementData.Population.iCount)
	ENDIF
	
	IF sMissionPlacementData.SearchArea.iNumAreas != GET_NUMBER_OF_FMC_SEARCH_AREAS()
		sMissionPlacementData.SearchArea.iNumAreas = GET_NUMBER_OF_FMC_SEARCH_AREAS()
		PRINTLN("[PROCESS_FMC_COUNTS_EVERY_FRAME] sMissionPlacementData.SearchArea.iNumAreas = ", sMissionPlacementData.SearchArea.iNumAreas)
	ENDIF
	
	IF sMissionPlacementData.Tags.iCount != GET_NUM_TAGS() 
		sMissionPlacementData.Tags.iCount = GET_NUM_TAGS()
		PRINTLN("[PROCESS_FMC_COUNTS_EVERY_FRAME] sMissionPlacementData.Tags.Tag.iCount = ", sMissionPlacementData.Tags.iCount)
	ENDIF
	
	IF sMissionPlacementData.Checkpoint.iCount != GET_NUMBER_OF_FMC_CHECKPOINTS_LISTS() 
		sMissionPlacementData.Checkpoint.iCount = GET_NUMBER_OF_FMC_CHECKPOINTS_LISTS()
		PRINTLN("[PROCESS_FMC_COUNTS_EVERY_FRAME] sMissionPlacementData.Checkpoint.iCount = ", sMissionPlacementData.Checkpoint.iCount)
	ENDIF
	
ENDPROC

PROC PROCESS_FMC_LEAVE_AREA_OPTIONS_EVERY_FRAME(structFMMC_MENU_ITEMS &sFMMCMenu)
	
	SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
		CASE ciFMC_LEAVE_AREA_RADIUS
			EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sEditedLeaveAreaPlacementData.fRadius, 0.5, 1500)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_FMC_SEARCH_AREA_OPTIONS_EVERY_FRAME(structFMMC_MENU_ITEMS &sFMMCMenu)

	//Trigger radius needs to be smaller than the enter zone
	FLOAT fMaxTrigger = sEditedSearchAreaData.fEnterRadius - 5.00
	FLOAT fMinEnter = sEditedSearchAreaData.fTriggerRadius + 5.00
	
	SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
		CASE ciFMC_SEARCH_AREA_ENTER_RADIUS
			EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sEditedSearchAreaData.fEnterRadius, fMinEnter, 500)
		BREAK
		CASE ciFMC_SEARCH_AREA_TRIGGER_RADIUS
			EDIT_FLOAT_VALUE_WITH_TIME_BASED_ACCELERATION(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sEditedSearchAreaData.fTriggerRadius, 2.0, fMaxTrigger)
		BREAK
		CASE ciFMC_SEARCH_AREA_CENTER_OPTION
			// url:bugstar:7062472 - Freemode Creator - Search Area - Add option to set centre position
			EDIT_BOOL_VALUE(sFMMCMenu.bRefreshMenuASAP, sFMMCMenu, sEditedSearchAreaData.bCenterCoordsActive)
		BREAK
	ENDSWITCH	
ENDPROC

FUNC BOOL IS_ANY_OTHER_FMC_SEARCH_AREA_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iSearchArea = -1, FLOAT fRadius = 1.0)
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		PRINTLN("[SEARCH_AREA] - IS_ANY_OTHER_FMC_SEARCH_AREA_IN_CORONA - player pos = ", vPlayerPos)
	ENDIF
	
	INT iNumSearchAreas = GET_NUMBER_OF_FMC_SEARCH_AREAS()
	
	INT i
	REPEAT iNumSearchAreas i
		PRINTLN("[SEARCH_AREA] - IS_ANY_OTHER_FMC_SEARCH_AREA_IN_CORONA - iSearchArea = ", i)
		
		IF iSearchArea > -1
		AND i = iSearchArea
			PRINTLN("[SEARCH_AREA] - IS_ANY_OTHER_FMC_SEARCH_AREA_IN_CORONA - iSearchArea > -1 AND i = iSearchArea")
		ELSE
			IF IS_FMC_SEARCH_AREA_IN_CORONA(sCurrentVarsStruct, i, fRadius)
				PRINTLN("[SEARCH_AREA] - IS_ANY_OTHER_FMC_SEARCH_AREA_IN_CORONA - TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MOVE_LAST_PLACED_SEARCH_AREA(structFMMC_MENU_ITEMS &sFMMCmenu, VECTOR &vCoord, FLOAT &fHead, CREATION_VARS_STRUCT &sCurrentVarsStruct, FMMC_LOCAL_STRUCT &sFMMCdata, INT iSearchArea)

	FMC_MOVE_ENTITY(NULL, vCoord, fHead, sCurrentVarsStruct, sFMMCdata, sFMMCmenu)
	
	IF IS_CREATOR_PLACEMENT_BUTTON_PRESSED(sFMMCMenu)
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
			IF CAN_PLACE_BUTTON_BE_PRESSED(sFMMCmenu, sCurrentVarsStruct)
				IF NOT IS_PLACEMENT_IN_A_RESTRICTED_AREA(sCurrentVarsStruct.vCoronaPos)
					IF NOT IS_ANY_OTHER_FMC_SEARCH_AREA_IN_CORONA(sCurrentVarsStruct, iSearchArea, 10.0)
					OR IS_ROCKSTAR_DEV()
						IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
							sCurrentVarsStruct.bDisplayFailReason = FALSE
							PLAY_SOUND_FRONTEND(-1, "ADD_SPAWN_POINT", GET_CREATOR_SPECIFIC_SOUND_SET())
							PRINTLN("[SEARCH_AREA] ADD_SPAWN_POINT SOUND EFFECT in soundset ", GET_CREATOR_SPECIFIC_SOUND_SET(), "!!!")
							
							RETURN TRUE
						ENDIF
					ELSE
						sCurrentVarsStruct.bDisplayFailReason = TRUE
						sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
						sCurrentVarsStruct.sFailReason = "FMMC_HELP0GT"
					ENDIF
				ELSE
					SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
					sCurrentVarsStruct.bFirstShapeTestCheckDone = TRUE
					sCurrentVarsStruct.sFailReason = "FMMC_ER_024"
					SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bRestrictedFail)
					
					RETURN FALSE
				ENDIF
				
				PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTLN("[SEARCH_AREA] ERROR SOUND EFFECT!!!")
			ENDIF		
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Placement function for search areas
BOOL bSearchAreaCenterMode = FALSE
PROC DO_FMC_SEARCH_AREA_CREATION(SELECTION_DETAILS_STRUCT 	&sSelDetails,
								CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
								structFMMC_MENU_ITEMS 		&sFMMCmenu,
								FMMC_LOCAL_STRUCT 			&sFMMCData,
								FMMC_CAM_DATA 				&sCamData,
								INT							iMaximumSearchAreas = MAX_NUM_SEARCH_AREAS)
	INT iR, iG, iB, iA
	INT iSearchArea
	INT iNumSearchAreas = GET_NUMBER_OF_FMC_SEARCH_AREAS()
	
	// Determine which search area we're working with
	IF sFMMCMenu.iSelectedFMCEntity = -1
		iSearchArea = iNumSearchAreas
	ELSE
		sFMMCMenu.iSelectedFMCEntity = CLAMP_INT(sFMMCMenu.iSelectedFMCEntity, 0, iMaximumSearchAreas - 1)
		iSearchArea = sFMMCMenu.iSelectedFMCEntity
		PRINTLN("JT DEBUG - iSearchArea: ", iSearchArea)
		// If you've picked up a search area, remove the blip of the one that was in the world so that it can be recreated as the placement one
		IF DOES_BLIP_EXIST(sSearchAreaStruct.biBlips[iSearchArea])
			PRINTLN("JT DEBUG - blip")
			REMOVE_BLIP(sSearchAreaStruct.biBlips[iSearchArea])
			
			// Update the edited search area data to that of the one you've just picked up
			sEditedSearchAreaData = sMissionPlacementData.SearchArea.Areas[iSearchArea] 
			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		ELSE
			PRINTLN("JT DEBUG - No blip")
		ENDIF
	ENDIF
	
	// Error for if you try to place one when too many already exist
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_SEARCH_AREAS)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		AND sFMMCMenu.iSelectedFMCEntity = -1
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			sCurrentVarsStruct.bDisplayFailReason = TRUE
			sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			sCurrentVarsStruct.sFailReason = "FMC_SA_MAX"
			PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
			PRINTSTRING("[SEARCH_AREA] ERROR SOUND EFFECT!!!")PRINTNL()
		ENDIF
	ENDIF
	
	// search area counter UI
	IF iNumSearchAreas >= iMaximumSearchAreas
	AND iSearchArea >= iMaximumSearchAreas
		sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_SEARCH_AREAS)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumSearchAreas, iMaximumSearchAreas, "FMC_SA_T", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_FLASHRED, 500)
		EXIT
	ELSE
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumSearchAreas, iMaximumSearchAreas, "FMC_SA_T", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_SEARCH_AREAS)
	ENDIF
	
	// Main creation state machine
	SWITCH sSelDetails.iSwitchingINT
		// Wait stage to stall the creation of more if there are too many already
		CASE CREATION_STAGE_WAIT
			IF iSearchArea < iMaximumSearchAreas
				// We skip straight to placement on search areas as they don't need to create anything
				FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
				sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		
		CASE CREATION_STAGE_PLACE
			FLOAT fHeading
			
			// url:bugstar:7062472 - Freemode Creator - Search Area - Add option to set centre position
			// Switch between edit modes
			IF sEditedSearchAreaData.bCenterCoordsActive AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= ciFMC_SEARCH_AREA_CENTER_OPTION
				
				// If not set, give the custom center the position of the corona
				IF IS_VECTOR_ZERO(sEditedSearchAreaData.vCenterCoords)
					sEditedSearchAreaData.vCenterCoords = sCurrentVarsStruct.vCoronaPos
				ENDIF
				
				IF NOT bSearchAreaCenterMode
					// Camera Warp
					CYCLE_ENTITIES(sFMMCData, sCamData, TRUE, sEditedSearchAreaData.vCenterCoords + <<0,0,sEditedSearchAreaData.fEnterRadius*3>>) // enough height to see the radius
					
					IF sCycleCamStruct.bSwitchingCam
						EXIT
					ELSE
						// Warp Complete
						sCurrentVarsStruct.vCoronaPos = sEditedSearchAreaData.vCenterCoords
						bSearchAreaCenterMode = TRUE
					ENDIF
				ENDIF				
			ELSE
				IF bSearchAreaCenterMode
					// Camera Warp
					CYCLE_ENTITIES(sFMMCdata, sCamData, TRUE, sEditedSearchAreaData.vCoords + <<0,0,sEditedSearchAreaData.fTriggerRadius*3>>) // enough height to see the radius
					
					IF sCycleCamStruct.bSwitchingCam
						EXIT
					ELSE
						// Warp Complete
						sCurrentVarsStruct.vCoronaPos = sEditedSearchAreaData.vCoords
						bSearchAreaCenterMode = FALSE
					ENDIF
				ENDIF
				
				// Checks if the place button has been pressed and that it's a valid place to go
				IF MOVE_LAST_PLACED_SEARCH_AREA(sFMMCmenu, 
												sEditedSearchAreaData.vCoords,
												fHeading,
												sCurrentVarsStruct,
												sFMMCData, 
												iSearchArea)
					
					PRINTLN("sSelDetails.iSwitchingINT = CREATION_STAGE_DONE")
					sSelDetails.iSwitchingINT = CREATION_STAGE_DONE
				ENDIF
			
			ENDIF
			
			FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
				
			IF sEditedSearchAreaData.bCenterCoordsActive AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= ciFMC_SEARCH_AREA_CENTER_OPTION
				// url:bugstar:7062472 - Freemode Creator - Search Area - Add option to set centre position
				// New Function
				sEditedSearchAreaData.vCenterCoords = sCurrentVarsStruct.vCoronaPos
				sMissionPlacementData.SearchArea.Areas[iSearchArea]	= sEditedSearchAreaData
				REFRESH_MENU_ASAP(sFMMCmenu)
			ELSE
				// Update the position - Default Behaviour
				sEditedSearchAreaData.vCoords = sCurrentVarsStruct.vCoronaPos
				REFRESH_MENU_ASAP(sFMMCmenu)
			ENDIF
			
			// --- Search Area ---
			// url:bugstar:7062472 - Freemode Creator - Search Area - Add option to set centre position
			// Set draw center of search area
			VECTOR vCenterCoord
			IF sEditedSearchAreaData.bCenterCoordsActive AND NOT IS_VECTOR_ZERO(sEditedSearchAreaData.vCenterCoords)
				vCenterCoord = sEditedSearchAreaData.vCenterCoords
			ELSE
				vCenterCoord = sEditedSearchAreaData.vCoords
			ENDIF
			
			// Draw the search enter area
			GET_HUD_COLOUR(HUD_COLOUR_GREY, iR, iG, iB, iA)
			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, vCenterCoord, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<sEditedSearchAreaData.fEnterRadius, sEditedSearchAreaData.fEnterRadius, sEditedSearchAreaData.fEnterRadius>>, iR, iG, iB, 100)
			
			// --- Trigger Area ---
			// Draw the search trigger area
			GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, sEditedSearchAreaData.vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<sEditedSearchAreaData.fTriggerRadius, sEditedSearchAreaData.fTriggerRadius, sEditedSearchAreaData.fTriggerRadius>>, iR, iG, iB, 100)
		BREAK
		
		// Update the search area you've just placed, increase the counter and reset for the next one
		CASE CREATION_STAGE_DONE
			
			sMissionPlacementData.SearchArea.Areas[iSearchArea]	= sEditedSearchAreaData
			PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_SEARCH_AREA, iSearchArea)
			
			PRINTLN("[SEARCH_AREA] Creating search area ", iSearchArea, " with pos/rad of ", sEditedSearchAreaData.vCoords," / ", sEditedSearchAreaData.fEnterRadius)
			
			START_PLACEMENT_COOLDOWN_TIMER(sCurrentVarsStruct)
			
			CREATE_FMMC_BLIP(sSearchAreaStruct.biBlips[iSearchArea], sMissionPlacementData.SearchArea.Areas[iSearchArea].vCoords, HUD_COLOUR_GREY, "FMC_B_LVAREA", 1)
			
			SET_BIT(sSearchAreaStruct.iBS_Editted, iSearchArea)
			
			FMC_CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)
			
			IF sFMMCMenu.iSelectedFMCEntity = -1
				RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			ENDIF
			
			sFMMCMenu.iSelectedFMCEntity = -1
			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		BREAK
	ENDSWITCH
	
	UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)	
ENDPROC

PROC DELETE_FMC_PLACED_SEARCH_AREA(structFMMC_MENU_ITEMS &sFMMCmenu, INT iSearchArea)

	INT iNumSearchAreas = GET_NUMBER_OF_FMC_SEARCH_AREAS()
	
	IF iNumSearchAreas <= 0
	OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= iNumSearchAreas
		EXIT
	ENDIF

	PRINTLN("[SEARCH_AREA] DELETE DELETE_FMC_PLACED_SEARCH_AREA ", iSearchArea)

	// Play delete sound.
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	// Remove the blip.
	IF DOES_BLIP_EXIST(sSearchAreaStruct.biBlips[iSearchArea])
		REMOVE_BLIP(sSearchAreaStruct.biBlips[iSearchArea])
	ENDIF
	
	// Move all the entries down.
	INT i
	FOR i = iSearchArea + 1 TO iNumSearchAreas - 1
		sSearchAreaStruct.biBlips[i - 1] = sSearchAreaStruct.biBlips[i]
		sMissionPlacementData.SearchArea.Areas[i - 1] = sMissionPlacementData.SearchArea.Areas[i]
	ENDFOR
	
	// Clear the last entry.
	MISSION_PLACEMENT_DATA_SEARCH_AREA_DATA sTempSearchAreaDataStruct
	COPY_SCRIPT_STRUCT(sMissionPlacementData.SearchArea.Areas[iNumSearchAreas - 1], sTempSearchAreaDataStruct, SIZE_OF(sTempSearchAreaDataStruct))
	sSearchAreaStruct.biBlips[iNumSearchAreas - 1] = NULL
	
	sFMMCMenu.bRefreshMenuASAP = TRUE
ENDPROC

PROC MAINTAIN_PLACED_FMC_SEARCH_AREAS(	CREATION_VARS_STRUCT &sCurrentVarsStruct,
										structFMMC_MENU_ITEMS &sFMMCmenu,
										HUD_COLOURS_STRUCT &sHCS,
										FMMC_LOCAL_STRUCT &sFMMCdata,
										FMMC_COMMON_MENUS_VARS &sFMMCendStage)
	BOOL bPlaneCorona
	INT iR, iG, iB, iA
	
	INT i
	REPEAT MAX_NUM_SEARCH_AREAS i
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.SearchArea.Areas[i].vCoords)
			IF sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION
				IF sFMMCmenu.iEntityCreation != CREATION_TYPE_SEARCH_AREA
				OR sFMMCMenu.iSelectedFMCEntity != i
					// Check if it's in the corona
					IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_SEARCH_AREA, i, NULL, bPlaneCorona, HUD_COLOUR_GREY, sFMMCdata, sFMMCendStage)
						//Pickup
						IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
							PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_SEARCH_AREA, i)
							CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
						//Delete
						ELIF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
							sCurrentVarsStruct.iEntityRemoved = i
							PRINTLN("[SEARCH_AREA] DELETE MAINTAIN_PLACED_FMC_SEARCH_AREAS ", i)							
							DELETE_FMC_PLACED_SEARCH_AREA(sFMMCmenu, i)
							CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
							
							RELOOP
						ENDIF
					ENDIF
					
					GET_HUD_COLOUR(HUD_COLOUR_GREYLIGHT, iR, iG, iB, iA)
					
					// Draw the selection zone in the centre of the search area if you're in the search area creation type
					IF sFMMCmenu.iEntityCreation = CREATION_TYPE_SEARCH_AREA
						
						// Draw trigger area
						GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
						DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_CYLINDER, sMissionPlacementData.SearchArea.Areas[i].vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<ciSTART_CHECK_POINT_SIZE, ciSTART_CHECK_POINT_SIZE, ciSTART_CHECK_POINT_SIZE>>, iR, iG, iB, iA)
						DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, sMissionPlacementData.SearchArea.Areas[i].vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<sMissionPlacementData.SearchArea.Areas[i].fTriggerRadius, sMissionPlacementData.SearchArea.Areas[i].fTriggerRadius, sMissionPlacementData.SearchArea.Areas[i].fTriggerRadius>>, iR, iG, iB, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE) // Add circle representation for TriggerArea
						
						// Draw the search area
						VECTOR vCenterCoord
						IF sEditedSearchAreaData.bCenterCoordsActive AND NOT IS_VECTOR_ZERO(sEditedSearchAreaData.vCenterCoords)
							vCenterCoord = sEditedSearchAreaData.vCenterCoords // Use center overwrite coordinate
						ELSE
							vCenterCoord = sEditedSearchAreaData.vCoords
						ENDIF
 						
						GET_HUD_COLOUR(HUD_COLOUR_GREY, iR, iG, iB, iA)
						DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, vCenterCoord, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<sMissionPlacementData.SearchArea.Areas[i].fEnterRadius, sMissionPlacementData.SearchArea.Areas[i].fEnterRadius, sMissionPlacementData.SearchArea.Areas[i].fEnterRadius>>, iR, iG, iB, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
					ENDIF
					
					// Give it a blip
					IF NOT DOES_BLIP_EXIST(sSearchAreaStruct.biBlips[i])
						CREATE_FMMC_BLIP(sSearchAreaStruct.biBlips[i], sMissionPlacementData.SearchArea.Areas[i].vCoords, HUD_COLOUR_GREY, "FMC_SA_B", 1)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_NUMBER_OF_FMC_LEAVE_AREAS()
	INT i
	REPEAT MAX_NUM_LEAVE_AREAS i
		IF IS_VECTOR_ZERO(sMissionPlacementData.LeaveArea[i].vCoords)
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN MAX_NUM_LEAVE_AREAS
ENDFUNC

FUNC BOOL IS_ANY_OTHER_FMC_LEAVE_AREA_IN_CORONA(CREATION_VARS_STRUCT &sCurrentVarsStruct, INT iLeaveArea = -1, FLOAT fRadius = 1.0)
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		PRINTLN("[LEAVE_AREA] - IS_ANY_OTHER_FMC_LEAVE_AREA_IN_CORONA - player pos = ", vPlayerPos)
	ENDIF
	
	INT iNumLeaveAreas = GET_NUMBER_OF_FMC_LEAVE_AREAS()
	
	INT i
	REPEAT iNumLeaveAreas i
		PRINTLN("[LEAVE_AREA] - IS_ANY_OTHER_FMC_LEAVE_AREA_IN_CORONA - iLeaveArea = ", i)
		
		IF iLeaveArea > -1
		AND i = iLeaveArea
			PRINTLN("[LEAVE_AREA] - IS_ANY_OTHER_FMC_LEAVE_AREA_IN_CORONA - iLeaveArea > -1 AND i = iLeaveArea")
		ELSE
			IF IS_FMC_LEAVE_AREA_IN_CORONA(sCurrentVarsStruct, i, fRadius)
				PRINTLN("[LEAVE_AREA] - IS_ANY_OTHER_FMC_LEAVE_AREA_IN_CORONA - TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MOVE_LAST_PLACED_LEAVE_AREA(structFMMC_MENU_ITEMS &sFMMCmenu, VECTOR &vCoord, FLOAT &fHead, CREATION_VARS_STRUCT &sCurrentVarsStruct, FMMC_LOCAL_STRUCT &sFMMCdata, INT iLeaveArea)
	FMC_MOVE_ENTITY(NULL, vCoord, fHead, sCurrentVarsStruct, sFMMCdata, sFMMCmenu)
	
	IF IS_CREATOR_PLACEMENT_BUTTON_PRESSED(sFMMCMenu)
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
			IF CAN_PLACE_BUTTON_BE_PRESSED(sFMMCmenu, sCurrentVarsStruct)
				IF NOT IS_PLACEMENT_IN_A_RESTRICTED_AREA(sCurrentVarsStruct.vCoronaPos)
					IF NOT IS_ANY_OTHER_FMC_LEAVE_AREA_IN_CORONA(sCurrentVarsStruct, iLeaveArea, 10.0)
					OR IS_ROCKSTAR_DEV()
						IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
							sCurrentVarsStruct.bDisplayFailReason = FALSE
							PLAY_SOUND_FRONTEND(-1, "ADD_SPAWN_POINT", GET_CREATOR_SPECIFIC_SOUND_SET())
							PRINTLN("[LEAVE_AREA] ADD_SPAWN_POINT SOUND EFFECT in soundset ", GET_CREATOR_SPECIFIC_SOUND_SET(), "!!!")
							
							RETURN TRUE
						ENDIF
					ELSE
						sCurrentVarsStruct.bDisplayFailReason = TRUE
						sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
						sCurrentVarsStruct.sFailReason = "FMMC_HELP0GT"
					ENDIF
				ELSE
					SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
					sCurrentVarsStruct.bFirstShapeTestCheckDone = TRUE
					sCurrentVarsStruct.sFailReason = "FMMC_ER_024"
					SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bRestrictedFail)
					
					RETURN FALSE
				ENDIF
				
				PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTLN("[LEAVE_AREA] ERROR SOUND EFFECT!!!")
			ENDIF		
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Placement function for leave areas
PROC DO_FMC_LEAVE_AREA_CREATION(SELECTION_DETAILS_STRUCT 	&sSelDetails,
								CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
								structFMMC_MENU_ITEMS 		&sFMMCmenu,
								FMMC_LOCAL_STRUCT 			&sFMMCData,
								INT							iMaximumLeaveAreas = MAX_NUM_LEAVE_AREAS)
	INT iR, iG, iB, iA
	INT iLeaveArea
	INT iNumLeaveAreas = GET_NUMBER_OF_FMC_LEAVE_AREAS()
	
	// Determine which leave area we're working with
	IF sFMMCMenu.iSelectedEntity = -1
		iLeaveArea = iNumLeaveAreas
	ELSE
		sFMMCMenu.iSelectedEntity = CLAMP_INT(sFMMCMenu.iSelectedEntity, 0, iMaximumLeaveAreas - 1)
		iLeaveArea = sFMMCMenu.iSelectedEntity
		
		// If you've picked up a leave area, remove the blip of the one that was in the world so that it can be recreated as the placement one
		IF DOES_BLIP_EXIST(sLeaveAreaStruct.biBlips[iLeaveArea])
			REMOVE_BLIP(sLeaveAreaStruct.biBlips[iLeaveArea])
			
			// Update the edited leave area data to that of the one you've just picked up
			sEditedLeaveAreaPlacementData = sMissionPlacementData.LeaveArea[iLeaveArea]
			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		ENDIF
	ENDIF
	
	// Error for if you try to place one when too many already exist
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_LEAVE_AREAS)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		AND sFMMCMenu.iSelectedEntity = -1
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			sCurrentVarsStruct.bDisplayFailReason = TRUE
			sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			sCurrentVarsStruct.sFailReason = "FMC_HELPLVAREAM"
			PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
			PRINTSTRING("[LEAVE_AREA] ERROR SOUND EFFECT!!!")PRINTNL()
		ENDIF
	ENDIF
	
	// Leave area counter UI
	IF iNumLeaveAreas >= iMaximumLeaveAreas
	AND iLeaveArea >= iMaximumLeaveAreas
		sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_LEAVE_AREAS)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumLeaveAreas, iMaximumLeaveAreas, "FMC_AB_LVAREA", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_FLASHRED, 500)
		EXIT
	ELSE
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumLeaveAreas, iMaximumLeaveAreas, "FMC_AB_LVAREA", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_LEAVE_AREAS)
	ENDIF
	
	// Main creation state machine
	SWITCH sSelDetails.iSwitchingINT
		// Wait stage to stall the creation of more if there are too many already
		CASE CREATION_STAGE_WAIT
			IF iLeaveArea < iMaximumLeaveAreas
				// We skip straight to placement on leave areas as they don't need to create anything
				FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
				sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		
		CASE CREATION_STAGE_PLACE
			FLOAT fHeading
			
			// Checks if the place button has been pressed and that it's a valid place to go
			IF MOVE_LAST_PLACED_LEAVE_AREA(	sFMMCmenu, 
											sEditedLeaveAreaPlacementData.vCoords,
											fHeading,
											sCurrentVarsStruct,
											sFMMCData, 
											iLeaveArea)
				
				PRINTLN("sSelDetails.iSwitchingINT = CREATION_STAGE_DONE")
				sSelDetails.iSwitchingINT = CREATION_STAGE_DONE
			ENDIF
			
			FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
			
			// Update the position
			sEditedLeaveAreaPlacementData.vCoords = sCurrentVarsStruct.vCoronaPos
			
			GET_HUD_COLOUR(HUD_COLOUR_GREY, iR, iG, iB, iA)
			
			// Draw the leave area
			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, sEditedLeaveAreaPlacementData.vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<sEditedLeaveAreaPlacementData.fRadius, sEditedLeaveAreaPlacementData.fRadius, sEditedLeaveAreaPlacementData.fRadius>>, iR, iG, iB, 100)
		BREAK
		
		// Update the leave area you've just placed, increase the counter and reset for the next one
		CASE CREATION_STAGE_DONE
			sMissionPlacementData.LeaveArea[iLeaveArea]	= sEditedLeaveAreaPlacementData
			PRINTLN("[LEAVE_AREA] Creating leave area ", iLeaveArea, " with pos/rad of ", sEditedLeaveAreaPlacementData.vCoords," / ", sEditedLeaveAreaPlacementData.fRadius)
			
			START_PLACEMENT_COOLDOWN_TIMER(sCurrentVarsStruct)
			
			CREATE_FMMC_BLIP(sLeaveAreaStruct.biBlips[iLeaveArea], sMissionPlacementData.LeaveArea[iLeaveArea].vCoords, HUD_COLOUR_GREY, "FMC_B_LVAREA", 1)
			
			SET_BIT(sLeaveAreaStruct.iBS_Editted, iLeaveArea)
			
			FMC_CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)
			
			IF sFMMCMenu.iSelectedEntity = -1
				RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			ENDIF
			
			sFMMCMenu.iSelectedEntity = -1
			sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		BREAK
	ENDSWITCH
	
	UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)	
ENDPROC

PROC DELETE_FMC_PLACED_LEAVE_AREA(INT iLeaveArea)
	// Play delete sound.
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	// Remove the blip.
	IF DOES_BLIP_EXIST(sLeaveAreaStruct.biBlips[iLeaveArea])
		REMOVE_BLIP(sLeaveAreaStruct.biBlips[iLeaveArea])
	ENDIF
	
	// Move all the entries down.
	INT iNumLeaveAreas = GET_NUMBER_OF_FMC_LEAVE_AREAS()
	INT i
	FOR i = iLeaveArea + 1 TO iNumLeaveAreas - 1
		sLeaveAreaStruct.biBlips[i - 1] = sLeaveAreaStruct.biBlips[i]
		sMissionPlacementData.LeaveArea[i - 1] = sMissionPlacementData.LeaveArea[i]
	ENDFOR
	
	// Clear the last entry.
	MISSION_PLACEMENT_DATA_LEAVE_AREA sTempLeaveAreaStruct
	COPY_SCRIPT_STRUCT(sMissionPlacementData.LeaveArea[iNumLeaveAreas - 1], sTempLeaveAreaStruct, SIZE_OF(MISSION_PLACEMENT_DATA_LEAVE_AREA))
	sLeaveAreaStruct.biBlips[iNumLeaveAreas - 1] = NULL
ENDPROC

PROC MAINTAIN_PLACED_FMC_LEAVE_AREAS(	CREATION_VARS_STRUCT &sCurrentVarsStruct,
										structFMMC_MENU_ITEMS &sFMMCmenu,
										HUD_COLOURS_STRUCT &sHCS,
										FMMC_LOCAL_STRUCT &sFMMCdata,
										FMMC_COMMON_MENUS_VARS &sFMMCendStage)
	BOOL bPlaneCorona
	INT iR, iG, iB, iA
	
	INT i
	REPEAT MAX_NUM_LEAVE_AREAS i
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.LeaveArea[i].vCoords)
			IF sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION
				IF sFMMCmenu.iEntityCreation != CREATION_TYPE_LEAVE_AREA
				OR sFMMCMenu.iSelectedEntity != i
					// Check if it's in the corona
					IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_LEAVE_AREA, i, NULL, bPlaneCorona, HUD_COLOUR_GREY, sFMMCdata, sFMMCendStage)
						//Pickup
						IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
							PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_LEAVE_AREA, i)
							CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
						//Delete
						ELIF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
							sCurrentVarsStruct.iEntityRemoved = i
							DELETE_FMC_PLACED_LEAVE_AREA(i)
							CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
							
							RELOOP
						ENDIF
					ENDIF
					
					GET_HUD_COLOUR(HUD_COLOUR_GREYLIGHT, iR, iG, iB, iA)
					
					// Draw the selection zone in the centre of the leave area if you're in the leave area creation type
					IF sFMMCmenu.iEntityCreation = CREATION_TYPE_LEAVE_AREA
						DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_CYLINDER, sMissionPlacementData.LeaveArea[i].vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<ciSTART_CHECK_POINT_SIZE, ciSTART_CHECK_POINT_SIZE, ciSTART_CHECK_POINT_SIZE>>, iR, iG, iB, iA)
						
						// Draw the leave area
						DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, sMissionPlacementData.LeaveArea[i].vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<sMissionPlacementData.LeaveArea[i].fRadius, sMissionPlacementData.LeaveArea[i].fRadius, sMissionPlacementData.LeaveArea[i].fRadius>>, iR, iG, iB, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
					ENDIF
					
					// Give it a blip
					IF NOT DOES_BLIP_EXIST(sLeaveAreaStruct.biBlips[i])
						CREATE_FMMC_BLIP(sLeaveAreaStruct.biBlips[i], sMissionPlacementData.LeaveArea[i].vCoords, HUD_COLOUR_GREY, "FMC_B_LVAREA", 1)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_NUMBER_OF_FMC_MOVING_DOORS()
	INT i
	REPEAT MAX_NUM_MOVING_DOORS i
		IF sMissionPlacementData.MovingDoor.MovingDoors[i].model = DUMMY_MODEL_FOR_SCRIPT
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN MAX_NUM_MOVING_DOORS
ENDFUNC

PROC GET_MOVING_DOOR_LIBRARY_AND_SELECTION_FROM_MODEL(structFMMC_MENU_ITEMS &sFMMCmenu)
	// Grab the library/model from the standard list
	sFMMCmenu.iPropLibrary = GET_PROP_LIBRARY_FROM_MODEL(sEditedMovingDoorsPlacementData.model)
	sFMMCmenu.iPropType = GET_PROP_LIBRARY_SELECTION_FROM_MODEL(sEditedMovingDoorsPlacementData.model)
	
	// If it's not in there, update it to go into the custom library
	IF sFMMCmenu.iPropLibrary = -1
		sFMMCmenu.iPropType = GET_PROP_SELECTION_FROM_CUSTOM_MODEL(sEditedMovingDoorsPlacementData.model)
		
		IF sFMMCmenu.iPropType > -1
			sFMMCmenu.iPropLibrary = PROP_LIBRARY_CUSTOM_PROPS
		ELIF g_FMMC_STRUCT.iCurrentNumberOfDevProps < ciMAX_CUSTOM_DEV_PROPS
			ADD_CUSTOM_PROP(sEditedMovingDoorsPlacementData.model)
			
			sFMMCmenu.iPropType = g_FMMC_STRUCT.iCurrentNumberOfDevProps
			sFMMCmenu.iPropLibrary = PROP_LIBRARY_CUSTOM_PROPS
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL MOVE_LAST_PLACED_MOVING_DOOR(structFMMC_MENU_ITEMS &sFMMCmenu, OBJECT_INDEX eiPassed, VECTOR &vCoord, FLOAT &fHead, CREATION_VARS_STRUCT &sCurrentVarsStruct, FMMC_LOCAL_STRUCT &sFMMCdata, BOOL bOverridePos = FALSE, BOOL bOverrideRot = FALSE)
	IF NOT DOES_ENTITY_EXIST(eiPassed)
		RETURN FALSE
	ENDIF
	
	VECTOR vMin, vMax
	
	IF IS_ENTITY_AN_OBJECT(eiPassed)
	AND NOT IS_ENTITY_DEAD(eiPassed)
		MODEL_NAMES mnTemp = GET_ENTITY_MODEL(eiPassed)
		REQUEST_MODEL(mnTemp)
		
		IF HAS_MODEL_LOADED(mnTemp)
			GET_MODEL_DIMENSIONS(mnTemp, vMin, vMax)
		ENDIF
	ENDIF
	
	FMC_MOVE_ENTITY(eiPassed, vCoord, fHead, sCurrentVarsStruct, sFMMCdata, sFMMCmenu, g_FMMC_STRUCT.fZOffset - vMin.z, TRUE, DEFAULT, DEFAULT, bOverrideRot)		
	
	IF NOT bOverridePos
	AND NOT sCurrentVarsStruct.bCoronaOnWater
		PLACE_OBJECT_ON_GROUND_PROPERLY(eiPassed)
		vCoord = GET_ENTITY_COORDS(eiPassed)
	ENDIF
	
	IF bOverridePos
		IF IS_VECTOR_ZERO(sFMMCmenu.vOverridePosition)
			SET_OVERRIDE_POSITION_COORDS(sFMMCmenu, vCoord)
			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
		ENDIF
		
		SET_ENTITY_COORDS(eiPassed, sFMMCmenu.vOverridePosition)
		vCoord = sFMMCmenu.vOverridePosition
		
		IF sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_POSITION
			DRAW_OVERRIDE_ARROW_MARKERS(sFMMCMenu, eiPassed, sFMMCMenu.iCurrentAlignment, FALSE, FALSE, FALSE)
		ENDIF
	ENDIF
	
	IF bOverrideRot
		IF IS_VECTOR_ZERO(sFMMCmenu.vOverrideRotation)
			SET_OVERRIDE_ROTATION_FROM_ENTITY(sFMMCMenu, eiPassed)
			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
		ENDIF
		
		SET_ENTITY_ROTATION(eiPassed, sFMMCmenu.vOverrideRotation)
		fHead = sFMMCmenu.vOverrideRotation.z
	ENDIF
	
	IF IS_BIT_SET(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
			IF CAN_PLACE_BUTTON_BE_PRESSED(sFMMCmenu, sCurrentVarsStruct)
				IF sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_POSITION
				OR sFMMCmenu.sActiveMenu = eFMMC_FMC_OVERRIDE_ROTATION
				OR FMC_IS_AREA_CLEAR_FOR_PLACEMENT(sCurrentVarsStruct, sCurrentVarsStruct.fCheckPointSize, eiPassed, FALSE, NULL, TRUE)
					IF NOT sCurrentVarsStruct.bAreaIsGoodForPlacement
						FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
					ENDIF
				ENDIF
				
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCMenu)
					IF sCurrentVarsStruct.bAreaIsGoodForPlacement AND IS_BIT_SET(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
					OR IS_ROCKSTAR_DEV()
						sCurrentVarsStruct.bDisplayFailReason = FALSE
						CLEAR_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
						
						PLAY_SOUND_FRONTEND(-1, "PLACE_VEHICLE", GET_CREATOR_SPECIFIC_SOUND_SET())
						PRINTLN("[MOVING_DOORS] MOVE_LAST_PLACED_MOVING_DOOR SOUND EFFECT!!!")
						
						RETURN TRUE
					ELSE
						sCurrentVarsStruct.bDisplayFailReason = TRUE
						sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
						sCurrentVarsStruct.sFailReason = GET_GENERIC_PLACEMENT_FAIL_DESCRIPTION(sCurrentVarsStruct)
						
						PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
						PRINTLN("[MOVING_DOORS] MOVE_LAST_PLACED_MOVING_DOOR ERROR SOUND EFFECT!!!")
					ENDIF
				ENDIF
			ELSE
				IF sCurrentVarsStruct.bAreaIsGoodForPlacement
					FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[MOVING_DOORS] Creator Bit - Blocking Placement. We had just entered a sub-menu.")		
		ENDIF
	ELSE
		SET_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
	ENDIF
	
	//Handle previewing the offset position and rotation when either of those menu options is currently selected.
	IF sFMMCmenu.sActiveMenu = eFmmc_FMC_MOVING_DOOR_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciFMC_MOVING_DOORS_END_POS_Z_OFFSET
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciFMC_MOVING_DOORS_END_ROT_Z_OFFSET
			SET_ENTITY_COORDS(eiPassed, <<vCoord.x, vCoord.y, vCoord.z + sMovingDoorStruct.fEndCoordOffsetZ>>)
			SET_ENTITY_HEADING(eiPassed, fHead + sMovingDoorStruct.fEndRotationOffsetZ)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_FMC_CAMERA_VISION_CONE_CREATION(structFMMC_MENU_ITEMS &sFMMCmenu, FMMC_LOCAL_STRUCT &sFMMCData, FMMC_CAM_DATA &sCamData)
	IF sFMMCmenu.sActiveMenu = eFmmc_FMC_SECURITY_CAMERA_OPTIONS_MENU
		
		IF NOT IS_BIT_SET(sSecurityCameraStruct.iGeneralBitset, biHasWarpedToEntity)
			IF sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].iProp != -1
				CYCLE_ENTITIES(sFMMCdata, sCamData, TRUE, CYCLE_ENTITY_POS(ET_OBJECT, sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].iProp))
				SET_BIT(sSecurityCameraStruct.iGeneralBitset, biHasWarpedToEntity)
			ENDIF
		ENDIF
			
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciFMC_SECURITY_CAMERAS_PROP_DATA_VISION_RANGE
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciFMC_SECURITY_CAMERAS_PROP_DATA_VISION_WIDTH
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciFMC_SECURITY_CAMERAS_PROP_DATA_VISION_SIZE
			
			// Draw vision cone here.
			
			VECTOR vCamPos = CYCLE_ENTITY_POS(ET_OBJECT, sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].iProp)
			//FLOAT fHeading = sMissionPlacementData.Prop.Props[sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].iProp].fHeading
			FLOAT fRadius = sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].fVisionRange
			FLOAT fAngle = sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].fVisionSize
			FLOAT fAngleForCone = sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].fVisionWidth
			
			
			OBJECT_INDEX oiCamIndex = sPropStruct.oiObject[sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].iProp]
			
			VECTOR vCameraHeadingVector
			
			//The inverse of GET_ENTITY_FORWARD_VECTOR is returned by default as the forward vector for some props points in the opposite direction to the lens (e.g. XM_PROP_X17_SERVER_FARM_CCTV_01).
			vCameraHeadingVector = -GET_ENTITY_FORWARD_VECTOR(oiCamIndex)
			
			MODEL_NAMES camModel = sMissionPlacementData.Prop.Props[sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].iProp].model
			
			VECTOR vRotOffset
			SWITCH camModel
				CASE PROP_CCTV_CAM_01A	vRotOffset = <<0.0, 0.0, 26.0>>		BREAK
				CASE PROP_CCTV_CAM_01B	vRotOffset = <<0.0, 0.0, -30.0>>	BREAK
				CASE PROP_CCTV_POLE_04	vRotOffset = <<0.0, 0.0, -62.5>>	BREAK
				DEFAULT					vRotOffset = <<0.0, 0.0, 0.0>>		BREAK
			ENDSWITCH
			
			//The inverse forward vector for some props may still not be aligned with the lens, so offset its rotation depending on the model being used.
			RotateVec(vCameraHeadingVector, vRotOffset)
			
			vCameraHeadingVector = <<vCameraHeadingVector.x, vCameraHeadingVector.y, 0.0>> //Ignore the Z axis when calculating headings for the blips and cones.
		
			FLOAT fCameraHeading = 0.0
		
			IF NOT IS_VECTOR_ZERO(vCameraHeadingVector)
				fCameraHeading = ATAN2(vCameraHeadingVector.x, -vCameraHeadingVector.y)
			ENDIF
			
			// Debug Line forward
			VECTOR vCameraHeadingDirection = <<vCameraHeadingVector.x, vCameraHeadingVector.y, 0.0>>
			vCameraHeadingDirection = NORMALISE_VECTOR(vCameraHeadingVector)
			DRAW_DEBUG_LINE(vCamPos, vCamPos + vCameraHeadingDirection, 0, 255, 0)
			// End Debug Line forward
			
			VECTOR vEndVector = vCamPos + vCameraHeadingDirection * fRadius
			vEndVector.z = vEndVector.z - 5.0
			
			#IF IS_DEBUG_BUILD
			
			DRAW_DEBUG_POLY_ANGLED_AREA(vCamPos, vEndVector, fAngleForCone, 255, 0, 0, 255, FALSE, FALSE)
			
			// Drawing the vision cone
			INT iProp = sMissionPlacementData.SecurityCamera.Cameras[sFMMCMenu.iSelectedFMCEntity].iProp
			
			//Clear fake cone array once before doing anything with fake cones.
			IF NOT IS_BIT_SET(sSecurityCameraStruct.iGeneralBitset, biEntitySetComplete)	
				CLEAR_FAKE_CONE_ARRAY()
				//PRINTLN("[DAVOR-CREATOR] Clearing up fake cone array")
				SET_BIT(sSecurityCameraStruct.iGeneralBitset, biEntitySetComplete)
			ENDIF
			
			// The below is necesarry to create the fake cones - checking for NOT DOES_BLIP_EXIST does not work.
			IF NOT IS_BIT_SET(sSecurityCameraStruct.iBlipBitset, iProp)
				sPropStruct.biObject[iProp] = CREATE_BLIP_FOR_ENTITY(oiCamIndex)
				//PRINTLN("[DAVOR-CREATOR] Creating blip.")
				SET_BIT(sSecurityCameraStruct.iBlipBitset, iProp)
			ENDIF
			
			
			BLIP_INDEX biCamIndex = sPropStruct.biObject[iProp]
			
			IF DOES_BLIP_EXIST(biCamIndex)
				SETUP_FAKE_CONE_DATA(biCamIndex, -1.0, 1.0, fAngle, 1.0, fRadius, DEG_TO_RAD(fCameraHeading + 180.0), TRUE)
				SET_BLIP_SHOW_CONE(biCamIndex, TRUE)
				//PRINTLN("[DAVOR-CREATOR] Setting up fake cone + showing it to true.")
			ENDIF
			// END Drawing the vision cone
			#ENDIF
			
			
		ENDIF
	ELSE	
		IF IS_BIT_SET(sSecurityCameraStruct.iGeneralBitset, biHasWarpedToEntity)
			CLEAR_BIT(sSecurityCameraStruct.iGeneralBitset, biHasWarpedToEntity)
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_MOVING_DOOR_PATH(structFMMC_MENU_ITEMS &sFMMCmenu, VECTOR v1, VECTOR v2, HUD_COLOURS eColour = HUD_COLOUR_RED)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(eColour, iR, iG, iB, iA)
	
	DRAW_LINE(v1, v2, iR, iG, iB, 250)
	DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, v1, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.1, 0.1, 0.1>>, iR, iG, iB, 200)
	DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, v2, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.1, 0.1, 0.1>>, iR, iG, iB, 200)
ENDPROC

// Placement function for moving doors
PROC DO_FMC_MOVING_DOOR_CREATION(SELECTION_DETAILS_STRUCT 	&sSelDetails,
								CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
								structFMMC_MENU_ITEMS 		&sFMMCmenu,
								FMMC_LOCAL_STRUCT 			&sFMMCData,
								INT							iMaximumMovingDoors = MAX_NUM_MOVING_DOORS)
	// Determine if the placement disk should be drawn above or below the moving door based on its size
	MODEL_NAMES mnCurrent = GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType)
	REPLACE_CREATOR_PROP_WITH_NEW_VERSION(mnCurrent)
	
	VECTOR vMin, vMax
	FLOAT fArea
	BOOL bBelowDisc = TRUE
	
	IF mnCurrent != DUMMY_MODEL_FOR_SCRIPT
	AND NOT DOES_PROP_FIT_ON_WALLS(mnCurrent)
		GET_MODEL_DIMENSIONS(mnCurrent, vMin, vMax)
		fArea = (vMax.x - vMin.x) * (vMax.y - vMin.y)
		
		IF fArea < 4.5
			bBelowDisc = FALSE
		ENDIF
	ENDIF
	
	INT iMovingDoor
	INT iNumMovingDoors = GET_NUMBER_OF_FMC_MOVING_DOORS()
	
	// Determine which moving door we're working with
	IF sFMMCmenu.iSelectedEntity = -1
		iMovingDoor = iNumMovingDoors
	ELSE
		sFMMCMenu.iSelectedEntity = CLAMP_INT(sFMMCMenu.iSelectedEntity, 0, iMaximumMovingDoors - 1)
		iMovingDoor = sFMMCMenu.iSelectedEntity
		
		// If you've picked up a moving door, remove it from the world so that it can be recreated as the placement one
		IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiMovingDoors[iMovingDoor])
			IF DOES_BLIP_EXIST(sMovingDoorStruct.biBlips[iMovingDoor])
				REMOVE_BLIP(sMovingDoorStruct.biBlips[iMovingDoor])
			ENDIF
			
			IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiMovingDoors[iMovingDoor])
				DELETE_OBJECT(sMovingDoorStruct.oiMovingDoors[iMovingDoor])
			ENDIF
			
			IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiCoronaObj)
				DELETE_OBJECT(sMovingDoorStruct.oiCoronaObj)
			ENDIF
			
			// Update the edited moving door data to that of the placed one
			sEditedMovingDoorsPlacementData = sMissionPlacementData.MovingDoor.MovingDoors[iMovingDoor]
			
			IF IS_VECTOR_ZERO(sEditedMovingDoorsPlacementData.vEndCoord)
				sEditedMovingDoorsPlacementData.vEndCoord = sEditedMovingDoorsPlacementData.vCoord
			ENDIF
			
			IF IS_VECTOR_ZERO(sEditedMovingDoorsPlacementData.vEndRotation)
				sEditedMovingDoorsPlacementData.vEndRotation = sEditedMovingDoorsPlacementData.vRotation
			ENDIF
			
			sMovingDoorStruct.oiCoronaObj = sMovingDoorStruct.oiMovingDoors[iMovingDoor]
			sMovingDoorStruct.oiMovingDoors[iMovingDoor] = NULL
			sMovingDoorStruct.fEndCoordOffsetZ = sEditedMovingDoorsPlacementData.vEndCoord.z - sEditedMovingDoorsPlacementData.vCoord.z
			sMovingDoorStruct.fEndRotationOffsetZ = sEditedMovingDoorsPlacementData.vEndRotation.z - sEditedMovingDoorsPlacementData.vRotation.z
			
			sCurrentVarsStruct.fCreationHeading = sEditedMovingDoorsPlacementData.vRotation.z
			
			GET_MOVING_DOOR_LIBRARY_AND_SELECTION_FROM_MODEL(sFMMCmenu)
			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
			
			IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiCoronaObj)
				SET_ENTITY_COLLISION(sMovingDoorStruct.oiCoronaObj, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	// UI for counting the number placed so far and there being too many
	IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_MOVING_DOORS)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		AND sFMMCMenu.iSelectedEntity = -1
			RESET_GENERIC_DOUBLE_NUMBER_FLASHING_COLOUR(0)
			sCurrentVarsStruct.bDisplayFailReason = TRUE
			sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
			sCurrentVarsStruct.sFailReason = "FMC_HELPMVDRSM"
			PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
			PRINTLN("[MOVING_DOORS] ERROR SOUND EFFECT!!!")
		ENDIF
	ENDIF
	
	IF iNumMovingDoors >= iMaximumMovingDoors	
	AND iMovingDoor >= iMaximumMovingDoors
		sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
		SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_MOVING_DOORS)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumMovingDoors, iMaximumMovingDoors, "FMC_AB_MVDRS", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_FLASHRED, 500)
		EXIT
	ELSE
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(iNumMovingDoors, iMaximumMovingDoors, "FMC_AB_MVDRS", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0)
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_MOVING_DOORS)
	ENDIF
	
	// Main placement state machine
	SWITCH sSelDetails.iSwitchingINT
		// Wait stage to stall the creation of more if there are too many already
		CASE CREATION_STAGE_WAIT
			IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiCoronaObj)
				DELETE_OBJECT(sMovingDoorStruct.oiCoronaObj)
			ENDIF
			
			IF iMovingDoor < iMaximumMovingDoors
				sSelDetails.iSwitchingINT = CREATION_STAGE_SET_UP	
			ENDIF
		BREAK
		
		// Request the model ready for creation
		CASE CREATION_STAGE_SET_UP
			IF NOT DOES_ENTITY_EXIST(sMovingDoorStruct.oiCoronaObj)
				sSelDetails.iSwitchingINT = CREATION_STAGE_MAKE
			ELSE
				sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		
		// Create the new placement moving door for the corona
		CASE CREATION_STAGE_MAKE
			IF NOT DOES_ENTITY_EXIST(sMovingDoorStruct.oiCoronaObj)
				IF IS_MODEL_VALID(mnCurrent)
					REQUEST_MODEL(mnCurrent)
					PRINTLN("[MOVING_DOORS] Loading ", mnCurrent)
					
					IF HAS_MODEL_LOADED(mnCurrent)
						REFRESH_MENU_ASAP(sFMMCMenu)
						
						sMovingDoorStruct.oiCoronaObj = CREATE_OBJECT(mnCurrent, sCurrentVarsStruct.vCoronaPos, FALSE, FALSE)
						SET_ENTITY_HEADING(sMovingDoorStruct.oiCoronaObj, sCurrentVarsStruct.fCreationHeading)
						FREEZE_ENTITY_POSITION(sMovingDoorStruct.oiCoronaObj, TRUE)
						SET_ENTITY_COLLISION(sMovingDoorStruct.oiCoronaObj, FALSE)
						SET_ENTITY_LOD_DIST(sMovingDoorStruct.oiCoronaObj, 1000)	
						
						sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
					ENDIF
				ENDIF
			ELSE
				sSelDetails.iSwitchingINT = CREATION_STAGE_PLACE
			ENDIF
		BREAK
		
		// Do the placement of the moving door, updating it's position/rotation/model/etc based on user input
		CASE CREATION_STAGE_PLACE
			sCurrentVarsStruct.iCoronaMarkerType = ciCoronaSmallRotate
			
			sEditedMovingDoorsPlacementData.model = mnCurrent
			sEditedMovingDoorsPlacementData.vCoord = sCurrentVarsStruct.vCoronaPos
			sEditedMovingDoorsPlacementData.vRotation.z = sCurrentVarsStruct.fCreationHeading
			
			// Handle the override placement where you can input the exact position/rotation in the menus
			HANDLE_ENTITY_CREATION_OVERRIDES(sFMMCMenu, sFMMCData, sCurrentVarsStruct, sMovingDoorStruct.oiCoronaObj, sEditedMovingDoorsPlacementData.iBitset[GET_LONG_BITSET_INDEX(iMovingDoor)], ENUM_TO_INT(eMOVINGDOORDATABITSET_OVERRIDE_POSITION), ENUM_TO_INT(eMOVINGDOORDATABITSET_OVERRIDE_ROTATION), FALSE, FALSE, FALSE)
			
			IF MOVE_LAST_PLACED_MOVING_DOOR(sFMMCmenu,
											sMovingDoorStruct.oiCoronaObj,
											sEditedMovingDoorsPlacementData.vCoord,
											sEditedMovingDoorsPlacementData.vRotation.z,
											sCurrentVarsStruct,
											sFMMCData,
											IS_LONG_BIT_SET(sEditedMovingDoorsPlacementData.iBitset, ENUM_TO_INT(eMOVINGDOORDATABITSET_OVERRIDE_POSITION)),
											IS_LONG_BIT_SET(sEditedMovingDoorsPlacementData.iBitset, ENUM_TO_INT(eMOVINGDOORDATABITSET_OVERRIDE_ROTATION)))
				// Set if a custom prop has been used
				IF sFMMCMenu.iMissionEntModelLibrary = MISSION_ENT_LIBRARY_CUSTOM
					SET_LONG_BIT(sMissionPlacementData.MovingDoor.MovingDoors[iMovingDoor].iBitset, ENUM_TO_INT(eMOVINGDOORDATABITSET_CUSTOM_MODEL))
				ELSE
					CLEAR_LONG_BIT(sMissionPlacementData.MovingDoor.MovingDoors[iMovingDoor].iBitset, ENUM_TO_INT(eMOVINGDOORDATABITSET_CUSTOM_MODEL))
				ENDIF
				
				sSelDetails.iSwitchingINT = CREATION_STAGE_DONE
			ENDIF
			
			sEditedMovingDoorsPlacementData.vEndCoord = <<sEditedMovingDoorsPlacementData.vCoord.x, sEditedMovingDoorsPlacementData.vCoord.y, sEditedMovingDoorsPlacementData.vCoord.z + sMovingDoorStruct.fEndCoordOffsetZ>>
			sEditedMovingDoorsPlacementData.vEndRotation = <<sEditedMovingDoorsPlacementData.vRotation.x, sEditedMovingDoorsPlacementData.vRotation.y, sEditedMovingDoorsPlacementData.vRotation.z + sMovingDoorStruct.fEndRotationOffsetZ>>
			
			IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiCoronaObj)
				DRAW_MOVING_DOOR_PATH(sFMMCmenu, sEditedMovingDoorsPlacementData.vCoord, sEditedMovingDoorsPlacementData.vEndCoord)
				
				IF IS_BIT_SET(iLocalBitset, biCanEditEntityInCorona)
					SET_ENTITY_ALPHA(sMovingDoorStruct.oiCoronaObj, 0, FALSE)
				ELSE
					RESET_ENTITY_ALPHA(sMovingDoorStruct.oiCoronaObj)
				ENDIF
				
				// Go back through set up if the object model has changed
				IF mnCurrent != GET_ENTITY_MODEL(sMovingDoorStruct.oiCoronaObj)
					IF DOES_BLIP_EXIST(sMovingDoorStruct.biBlips[iMovingDoor])
						REMOVE_BLIP(sMovingDoorStruct.biBlips[iMovingDoor])
					ENDIF
					
					MODEL_NAMES oldModel
					oldModel = GET_ENTITY_MODEL(sMovingDoorStruct.oiCoronaObj)
					
					DELETE_OBJECT(sMovingDoorStruct.oiCoronaObj)
					SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
					
					sSelDetails.iSwitchingINT = CREATION_STAGE_SET_UP
				ENDIF
			ELSE
				sSelDetails.iSwitchingINT = CREATION_STAGE_SET_UP
			ENDIF
		BREAK
		
		// Update the object you've just placed and reset for the next one
		CASE CREATION_STAGE_DONE
			IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiCoronaObj)
				sMovingDoorStruct.oiMovingDoors[iMovingDoor] = sMovingDoorStruct.oiCoronaObj
				SET_ENTITY_COLLISION(sMovingDoorStruct.oiMovingDoors[iMovingDoor], TRUE)	
				FMC_CREATE_PLACEMENT_PARTICLE_FX(sCurrentVarsStruct.bCoronaOnWater, sCurrentVarsStruct.vCoronaPos)
				CREATE_FMMC_BLIP(sMovingDoorStruct.biBlips[iMovingDoor], GET_ENTITY_COORDS(sMovingDoorStruct.oiMovingDoors[iMovingDoor]), HUD_COLOUR_PURPLELIGHT, "FMC_B_MVDRS", 1)
				
				sMovingDoorStruct.oiCoronaObj = NULL
				sFMMCMenu.iSelectedEntity = -1
				
				// Update the placement data to the edited version
				sMissionPlacementData.MovingDoor.MovingDoors[iMovingDoor] = sEditedMovingDoorsPlacementData
				
				CLEAR_LONG_BIT(sEditedMovingDoorsPlacementData.iBitset, ENUM_TO_INT(eMOVINGDOORDATABITSET_OVERRIDE_POSITION))
				CLEAR_LONG_BIT(sEditedMovingDoorsPlacementData.iBitset, ENUM_TO_INT(eMOVINGDOORDATABITSET_OVERRIDE_ROTATION))
				
				sSelDetails.iSwitchingINT = CREATION_STAGE_WAIT
			ENDIF
		BREAK
	ENDSWITCH
	
	FMC_UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, sMovingDoorStruct.oiCoronaObj, NOT bBelowDisc, DEFAULT, DEFAULT, FALSE)
ENDPROC

PROC DELETE_FMC_PLACED_MOVING_DOOR(INT iMovingDoor)
	INT iNumMovingDoors = GET_NUMBER_OF_FMC_MOVING_DOORS()
	
	// Play delete sound.
	PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
	
	// Remove the blip.
	IF DOES_BLIP_EXIST(sMovingDoorStruct.biBlips[iMovingDoor])
		REMOVE_BLIP(sMovingDoorStruct.biBlips[iMovingDoor])
	ENDIF
	
	// Remove the object.
	IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiMovingDoors[iMovingDoor])
		DELETE_OBJECT(sMovingDoorStruct.oiMovingDoors[iMovingDoor])
	ENDIF
	
	// Move all the entries down.
	INT i
	FOR i = iMovingDoor + 1 TO iNumMovingDoors - 1
		sMovingDoorStruct.biBlips[i - 1] = sMovingDoorStruct.biBlips[i]
		sMovingDoorStruct.oiMovingDoors[i - 1] = sMovingDoorStruct.oiMovingDoors[i]
		sMissionPlacementData.MovingDoor.MovingDoors[i - 1] = sMissionPlacementData.MovingDoor.MovingDoors[i]
	ENDFOR
	
	// Clear the last entry.
	MISSION_PLACEMENT_DATA_MOVING_DOORS sTempMovingDoorsStruct
	COPY_SCRIPT_STRUCT(sMissionPlacementData.MovingDoor.MovingDoors[iNumMovingDoors - 1], sTempMovingDoorsStruct, SIZE_OF(MISSION_PLACEMENT_DATA_MOVING_DOORS))
	sMovingDoorStruct.biBlips[iNumMovingDoors - 1] = NULL
	sMovingDoorStruct.oiMovingDoors[iNumMovingDoors - 1] = NULL
ENDPROC

//Deal with the placed moving doors
PROC MAINTAIN_PLACED_FMC_MOVING_DOORS(CREATION_VARS_STRUCT &sCurrentVarsStruct, structFMMC_MENU_ITEMS &sFMMCmenu, HUD_COLOURS_STRUCT &sHCS, FMMC_LOCAL_STRUCT &sFMMCdata, FMMC_COMMON_MENUS_VARS &sFMMCendStage)
	BOOL bPlaneCorona = TRUE
	
	// Loop over the placed moving doors
	INT i
	REPEAT MAX_NUM_MOVING_DOORS i
		IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiMovingDoors[i])
			// Determine the radius to check based on the model to see if it's in the corona.
			FLOAT fRadius = 1.0
			
			IF DOES_OBJECT_NEED_LARGER_RADIUS(GET_ENTITY_MODEL(sMovingDoorStruct.oiMovingDoors[i]))
				fRadius = 2.0
			ENDIF
			
			// Delete it if the model doesn't match anymore
			IF GET_ENTITY_MODEL(sMovingDoorStruct.oiMovingDoors[i]) != sMissionPlacementData.MovingDoor.MovingDoors[i].model
				DELETE_OBJECT(sMovingDoorStruct.oiMovingDoors[i])
			ELSE
				// Check if it's in the corona
				IF IS_FMC_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, CREATION_TYPE_MOVING_DOOR, i, sMovingDoorStruct.oiMovingDoors[i], bPlaneCorona, HUD_COLOUR_PURPLE, sFMMCdata, sFMMCendStage, fRadius)
					// Pickup
					IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
						PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_MOVING_DOOR, i)
						CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
					// Delete
					ELIF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
						sCurrentVarsStruct.iEntityRemoved = i
						DELETE_FMC_PLACED_MOVING_DOOR(i)
						CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
						
						RELOOP
					ENDIF
				ENDIF
			ENDIF
			
			// Give it a blip
			IF NOT DOES_BLIP_EXIST(sMovingDoorStruct.biBlips[i])
				CREATE_FMMC_BLIP(sMovingDoorStruct.biBlips[i], GET_ENTITY_COORDS(sMovingDoorStruct.oiMovingDoors[i]), HUD_COLOUR_PURPLELIGHT, "FMC_B_MVDRS", 1)
			ENDIF
			
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_MOVING_DOOR
				DRAW_MOVING_DOOR_PATH(sFMMCmenu, sMissionPlacementData.MovingDoor.MovingDoors[i].vCoord, sMissionPlacementData.MovingDoor.MovingDoors[i].vEndCoord)
			ENDIF
		ELSE
			// Recreate it if needed
			IF sMissionPlacementData.MovingDoor.MovingDoors[i].model != DUMMY_MODEL_FOR_SCRIPT
				IF sFMMCmenu.iEntityCreation != CREATION_TYPE_MOVING_DOOR
				OR sFMMCMenu.iSelectedEntity != i
					sMovingDoorStruct.oiMovingDoors[i] = CREATE_OBJECT(sMissionPlacementData.MovingDoor.MovingDoors[i].model, sMissionPlacementData.MovingDoor.MovingDoors[i].vCoord, FALSE, FALSE)
					SET_ENTITY_HEADING(sMovingDoorStruct.oiMovingDoors[i], sMissionPlacementData.MovingDoor.MovingDoors[i].vRotation.z)
					FREEZE_ENTITY_POSITION(sMovingDoorStruct.oiMovingDoors[i], TRUE)
					SET_ENTITY_COLLISION(sMovingDoorStruct.oiMovingDoors[i], FALSE)
					SET_ENTITY_LOD_DIST(sMovingDoorStruct.oiMovingDoors[i], 1000)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL CREATE_ALL_FMC_MOVING_DOORS()
	INT iNumMovingDoors = GET_NUMBER_OF_FMC_MOVING_DOORS()
	
	// Loop over all moving doors and create them
	INT i
	REPEAT iNumMovingDoors i
		IF NOT DOES_ENTITY_EXIST(sMovingDoorStruct.oiMovingDoors[i])
			REQUEST_MODEL(sMissionPlacementData.MovingDoor.MovingDoors[i].model)
			
			IF HAS_MODEL_LOADED(sMissionPlacementData.MovingDoor.MovingDoors[i].model)
				sMovingDoorStruct.oiMovingDoors[i] = CREATE_OBJECT(sMissionPlacementData.MovingDoor.MovingDoors[i].model, sMissionPlacementData.MovingDoor.MovingDoors[i].vCoord, FALSE, FALSE)
				
				SET_ENTITY_COORDS_NO_OFFSET(sMovingDoorStruct.oiMovingDoors[i], sMissionPlacementData.MovingDoor.MovingDoors[i].vCoord)
				SET_ENTITY_ROTATION(sMovingDoorStruct.oiMovingDoors[i], sMissionPlacementData.MovingDoor.MovingDoors[i].vRotation)
				SET_ENTITY_PROOFS(sMovingDoorStruct.oiMovingDoors[i], TRUE, TRUE, TRUE, TRUE, TRUE)
				FREEZE_ENTITY_POSITION(sMovingDoorStruct.oiMovingDoors[i], TRUE)
				SET_WAIT_FOR_COLLISIONS_BEFORE_PROBE(sMovingDoorStruct.oiMovingDoors[i], TRUE)
				
				IF IS_LONG_BIT_SET(sMissionPlacementData.MovingDoor.MovingDoors[i].iBitset, ENUM_TO_INT(eMOVINGDOORDATABITSET_CUSTOM_MODEL))
					BOOL bAdd = TRUE
					INT iCustom
					REPEAT ciMAX_CUSTOM_DEV_PROPS iCustom
						IF sMissionPlacementData.MovingDoor.MovingDoors[i].model = g_FMMC_STRUCT.sCustomProps[iCustom].mnCustomPropName
							bAdd = FALSE
							BREAKLOOP
						ENDIF
					ENDREPEAT
					
					IF bAdd
						ADD_CUSTOM_PROP(sMissionPlacementData.MovingDoor.MovingDoors[i].model)
					ENDIF
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Make sure they all exist
	REPEAT iNumMovingDoors i
		IF sMissionPlacementData.MovingDoor.MovingDoors[i].model != DUMMY_MODEL_FOR_SCRIPT
		AND NOT DOES_ENTITY_EXIST(sMovingDoorStruct.oiMovingDoors[i])
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	// Unload their models
	REPEAT iNumMovingDoors i
		IF sMissionPlacementData.MovingDoor.MovingDoors[i].model != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(sMissionPlacementData.MovingDoor.MovingDoors[i].model)
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_PLACED_FMC_SECURITY_CAMERAS()
	INT iNumCameras = GET_NUMBER_OF_FMC_SECURITY_CAMERAS()
	
	INT i
	FOR i = iNumCameras - 1 TO 0 STEP -1
		IF sMissionPlacementData.SecurityCamera.Cameras[i].iProp != -1
			IF sMissionPlacementData.SecurityCamera.Cameras[i].iProp > sMissionPlacementData.Prop.iCount - 1
				DELETE_FMC_SECURITY_CAMERA(i, FALSE)
				iNumCameras--
			ENDIF
		ENDIF
	ENDFOR
	
	IF sMissionPlacementData.SecurityCamera.iNumCameras != iNumCameras
		sMissionPlacementData.SecurityCamera.iNumCameras = iNumCameras
	ENDIF
ENDPROC

#IF MAX_NUM_SPAWN_POSITIONS
////////////////////////
///    Spawn Position Placement
////////////////////////
INT iLastSpawnPosition = -1
BOOL bSpawnPointMoveMode = FALSE
// url:bugstar:7523499 - Freemode Creator - Investigate better ways to handle when we want an entity to spawn randomly out of several locations
// Placement function for spawn points
PROC DO_FMC_SPAWN_POSITIONS_CREATION(SELECTION_DETAILS_STRUCT 	&sSelDetails,
								CREATION_VARS_STRUCT 		&sCurrentVarsStruct,
								structFMMC_MENU_ITEMS 		&sFMMCmenu,
								FMMC_LOCAL_STRUCT 			&sFMMCData,
								FMMC_CAM_DATA 				&sCamData,
								INT							iMaximumSpawnPoints = MAX_NUM_SPAWN_POSITIONS)
	
	UNUSED_PARAMETER(sSelDetails)
				
	// --- Update for selected entry ---
	INT iR, iG, iB, iA
	
	INT iSpawnPosition =  GET_CREATOR_MENU_SELECTION(sFMMCmenu) / 5 // divided to 5 entries per position
	
	// search area counter UI
	DRAW_GENERIC_BIG_DOUBLE_NUMBER(iMaximumSpawnPoints, MAX_NUM_SPAWN_POSITIONS, "FMC_SP_T", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0)
	
	// Update corona disc
	UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)

	IF iSpawnPosition >= (iMaximumSpawnPoints)
		EXIT
	ENDIF
	
	IF IS_VECTOR_ZERO(sMissionPlacementData.SpawnPosition.Positions[iSpawnPosition].vCoords)
		bSpawnPointMoveMode = TRUE
		sMissionPlacementData.SpawnPosition.Positions[iSpawnPosition].vCoords = sCurrentVarsStruct.vCoronaPos
		sMissionPlacementData.SpawnPosition.Positions[iSpawnPosition].vRotation = <<0, 0, sCurrentVarsStruct.fCoronaDiscHeading>>
	ENDIF
	
	// check if camera and selection need to move
	IF iSpawnPosition != iLastSpawnPosition
		bSpawnPointMoveMode = TRUE
		iLastSpawnPosition = iSpawnPosition
	ENDIF
	
	// move camera to next spot
	IF bSpawnPointMoveMode = TRUE
		// Camera Warp
		CYCLE_ENTITIES(sFMMCData, sCamData, TRUE, sMissionPlacementData.SpawnPosition.Positions[iSpawnPosition].vCoords + <<0,0,10>>)
		
		IF sCycleCamStruct.bSwitchingCam
			EXIT
		ELSE
			// Warp Complete
			sCurrentVarsStruct.vCoronaPos = sMissionPlacementData.SpawnPosition.Positions[iSpawnPosition].vCoords
			sCurrentVarsStruct.fCoronaDiscHeading = sMissionPlacementData.SpawnPosition.Positions[iSpawnPosition].vRotation.z
			bSpawnPointMoveMode = FALSE
		ENDIF
	ENDIF
	
	FMC_SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
	
	// Editing existing entry
	sMissionPlacementData.SpawnPosition.Positions[iSpawnPosition].vCoords = sCurrentVarsStruct.vCoronaPos
	sCurrentVarsStruct.fCoronaDiscHeading = sMissionPlacementData.SpawnPosition.Positions[iSpawnPosition].vRotation.z
	REFRESH_MENU_ASAP(sFMMCmenu)
			
	// --- Draw locations for all spawn Postions ---
	INT i
	REPEAT iMaximumSpawnPoints i
		IF NOT IS_VECTOR_ZERO(sMissionPlacementData.SpawnPosition.Positions[i].vCoords)
			// Draw Creator Markers
			IF i = iSpawnPosition
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
			ELSE
				GET_HUD_COLOUR(HUD_COLOUR_GREY, iR, iG, iB, iA)
			ENDIF
			
			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_SPHERE, sMissionPlacementData.SpawnPosition.Positions[i].vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<2, 2, 2>>, iR, iG, iB, 100)	
//			DRAW_MARKER_FOR_CREATOR(sFMMCmenu.iCurrentMarkersDrawn, MARKER_ARROW, sMissionPlacementData.SpawnPosition.Positions[i].vCoords + <<0,0,0.2>>, <<0,0,0>>, <<90,0,0>> + sMissionPlacementData.SpawnPosition.Positions[i].vRotation, <<3, 3, 3>>, iR, iG, iB, 200)	
		ENDIF
	ENDREPEAT
ENDPROC

#ENDIF
// ----------------------------------------------

PROC FMC_REMOVE_ALL_PEDS(BOOL bClearMissionPlacementData = TRUE)
	
	INT i 
	//Peds
	IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
		DELETE_PED(sPedStruct.piTempPed)
	ENDIF
	FOR i = 0 TO (MAX_NUM_PEDS - 1)
		IF DOES_ENTITY_EXIST(sPedStruct.piPed[i])
			DELETE_PED(sPedStruct.piPed[i])
		ENDIF
		IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])
			REMOVE_BLIP(sPedStruct.biPedBlip[i])
		ENDIF
	ENDFOR
	
	IF bClearMissionPlacementData		
		MISSION_PLACEMENT_DATA_PED sEmpty
		COPY_SCRIPT_STRUCT(sMissionPlacementData.Ped, sEmpty, SIZE_OF(MISSION_PLACEMENT_DATA_PED))
	ENDIF
		
ENDPROC

PROC FMC_REMOVE_ALL_VEHICLES(BOOL bClearMissionPlacementData = TRUE)
	
	INT i 
	
	//Vehicles
	IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
		DELETE_VEHICLE(sVehStruct.viCoronaVeh)
	ENDIF
	FOR i = 0 TO (MAX_NUM_VEHICLES - 1)
		IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[i])
			REMOVE_MODEL_FROM_CREATOR_BUDGET(GET_ENTITY_MODEL(sVehStruct.veVehcile[i]))
			DELETE_VEHICLE(sVehStruct.veVehcile[i])
		ENDIF
		IF DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[i])
			REMOVE_BLIP(sVehStruct.biVehicleBlip[i])
		ENDIF
	ENDFOR
	
	IF bClearMissionPlacementData
		MISSION_PLACEMENT_DATA_VEHICLE sEmpty
		COPY_SCRIPT_STRUCT(sMissionPlacementData.Vehicle, sEmpty, SIZE_OF(MISSION_PLACEMENT_DATA_VEHICLE))
	ENDIF
	
ENDPROC

PROC FMC_REMOVE_ALL_MISSION_ENTITIES(BOOL bClearMissionPlacementData = TRUE)
	
	INT i 
	
	IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
		DELETE_OBJECT(sObjStruct.viCoronaObj)
	ENDIF
	FOR i = 0 TO (MAX_NUM_MISSION_ENTITIES - 1)
		IF DOES_ENTITY_EXIST(sObjStruct.oiObject[i])
			DELETE_OBJECT(sObjStruct.oiObject[i])
		ENDIF		
		IF DOES_BLIP_EXIST(sObjStruct.biObject[i])
			REMOVE_BLIP(sObjStruct.biObject[i])
		ENDIF
		IF sObjStruct.iDecalNum[i] != -1
			IF IS_DECAL_ALIVE(sObjStruct.diDecal[sObjStruct.iDecalNum[i]])
				REMOVE_DECAL(sObjStruct.diDecal[sObjStruct.iDecalNum[i]])
			ENDIF
		ENDIF
	ENDFOR
	
	IF bClearMissionPlacementData
		MISSION_PLACEMENT_DATA_MISSION_ENTITY sEmpty
		COPY_SCRIPT_STRUCT(sMissionPlacementData.MissionEntity, sEmpty, SIZE_OF(MISSION_PLACEMENT_DATA_MISSION_ENTITY))
	ENDIF
ENDPROC

PROC FMC_REMOVE_ALL_PROPS(BOOL bClearMissionPlacementData = TRUE)
	
	INT i 
	
	//Prop
	FOR i = 0 TO (MAX_NUM_PROPS - 1)
		IF DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
			REMOVE_MODEL_FROM_CREATOR_BUDGET(GET_ENTITY_MODEL(sPropStruct.oiObject[i]))
			DELETE_OBJECT(sPropStruct.oiObject[i])
		ENDIF
		IF DOES_BLIP_EXIST(sPropStruct.biObject[i])
			REMOVE_BLIP(sPropStruct.biObject[i])
		ENDIF
	ENDFOR
	
	IF bClearMissionPlacementData
		MISSION_PLACEMENT_DATA_PROP sEmpty
		COPY_SCRIPT_STRUCT(sMissionPlacementData.Prop, sEmpty, SIZE_OF(MISSION_PLACEMENT_DATA_PROP))
	ENDIF
ENDPROC

PROC FMC_REMOVE_ALL_TARGETS(BOOL bClearMissionPlacementData = TRUE)	
	IF bClearMissionPlacementData
		MISSION_PLACEMENT_DATA_TAKE_OUT_TARGETS sEmpty
		INT i
		REPEAT MAX_NUM_TAKE_OUT_TARGET_STAGES i
			COPY_SCRIPT_STRUCT(sMissionPlacementData.TakeOutTarget[i], sEmpty, SIZE_OF(MISSION_PLACEMENT_DATA_TAKE_OUT_TARGETS))
		ENDREPEAT
	ENDIF
ENDPROC

PROC FMC_REMOVE_ALL_CUSTOM_SPAWNS(BOOL bClearMissionPlacementData = TRUE)
	INT i 
	FOR i = 0 TO (MAX_NUM_CUSTOM_SPAWNS -1)
		IF DOES_BLIP_EXIST(sCustomSpawns.biBlips[i])
			REMOVE_BLIP(sCustomSpawns.biBlips[i])
		ENDIF
		IF IS_DECAL_ALIVE(sCustomSpawns.diDecal[i])
			REMOVE_DECAL(sCustomSpawns.diDecal[i])
		ENDIF
	ENDFOR
	
	IF bClearMissionPlacementData
		MISSION_PLACEMENT_DATA_CUSTOM_SPAWNS sEmpty
		COPY_SCRIPT_STRUCT(sMissionPlacementData.CustomSpawns, sEmpty, SIZE_OF(MISSION_PLACEMENT_DATA_CUSTOM_SPAWNS))
	ENDIF
ENDPROC

PROC FMC_REMOVE_ALL_LOCATIONS(BOOL bClearMissionPlacementData = TRUE)

	INT i 
	
	//GotoLocation
	FOR i = 0 TO (MAX_NUM_GOTO_LOCATIONS - 1)
		IF DOES_BLIP_EXIST(sLocStruct.biGoToBlips[i])
			REMOVE_BLIP(sLocStruct.biGoToBlips[i])
		ENDIF
	ENDFOR
	
	IF bClearMissionPlacementData
		MISSION_PLACEMENT_DATA_GOTO sEmpty
		COPY_SCRIPT_STRUCT(sMissionPlacementData.GoToPoint, sEmpty, SIZE_OF(MISSION_PLACEMENT_DATA_GOTO))
	ENDIF
ENDPROC

PROC FMC_REMOVE_ALL_TRIGGER_AREAS(BOOL bClearMissionPlacementData = TRUE)
	IF bClearMissionPlacementData
		MISSION_PLACEMENT_DATA_TRIGGER_AREAS sEmpty
		COPY_SCRIPT_STRUCT(sMissionPlacementData.TriggerArea, sEmpty, SIZE_OF(MISSION_PLACEMENT_DATA_TRIGGER_AREAS))
	ENDIF
ENDPROC

PROC FMC_REMOVE_ALL_POPULATION_BLOCKERS(BOOL bClearMissionPlacementData = TRUE)	
	IF bClearMissionPlacementData
		MISSION_PLACEMENT_DATA_POPULATION sEmpty
		COPY_SCRIPT_STRUCT(sMissionPlacementData.Population, sEmpty, SIZE_OF(MISSION_PLACEMENT_DATA_POPULATION))
	ENDIF
ENDPROC

PROC FMC_REMOVE_ALL_LEAVE_AREAS(BOOL bClearMissionPlacementData = TRUE)
	INT i
	REPEAT MAX_NUM_LEAVE_AREAS i
		IF DOES_BLIP_EXIST(sLeaveAreaStruct.biBlips[i])
			REMOVE_BLIP(sLeaveAreaStruct.biBlips[i])
		ENDIF
	ENDREPEAT
	
	IF bClearMissionPlacementData
		MISSION_PLACEMENT_DATA_LEAVE_AREA sTempLeaveareaStruct
		COPY_SCRIPT_STRUCT(sMissionPlacementData.LeaveArea, sTempLeaveareaStruct, SIZE_OF(MISSION_PLACEMENT_DATA_LEAVE_AREA))
	ENDIF
ENDPROC

PROC FMC_REMOVE_ALL_MOVING_DOORS(BOOL bClearMissionPlacementData = TRUE)
	IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiCoronaObj)
		DELETE_OBJECT(sMovingDoorStruct.oiCoronaObj)
	ENDIF
	
	INT i
	REPEAT MAX_NUM_MOVING_DOORS i
		IF DOES_BLIP_EXIST(sMovingDoorStruct.biBlips[i])
			REMOVE_BLIP(sMovingDoorStruct.biBlips[i])
		ENDIF
		
		IF DOES_ENTITY_EXIST(sMovingDoorStruct.oiMovingDoors[i])
			DELETE_OBJECT(sMovingDoorStruct.oiMovingDoors[i])
		ENDIF
	ENDREPEAT
	
	IF bClearMissionPlacementData
		MISSION_PLACEMENT_DATA_MOVING_DOOR sTempMovingDoorStruct
		COPY_SCRIPT_STRUCT(sMissionPlacementData.MovingDoor, sTempMovingDoorStruct, SIZE_OF(MISSION_PLACEMENT_DATA_MOVING_DOOR))
	ENDIF
ENDPROC

PROC FMC_REMOVE_ALL_CHECKPOINT_LIST(BOOL bClearMissionPlacementData = TRUE)
	IF DOES_ENTITY_EXIST(sCheckpointListCreationStruct.oiCoronaObj)
		DELETE_OBJECT(sCheckpointListCreationStruct.oiCoronaObj)
	ENDIF
	
	INT i
	REPEAT MAX_NUM_CHECKPOINTS i
		IF DOES_BLIP_EXIST(sCheckpointListCreationStruct.biBlips[i])
			REMOVE_BLIP(sCheckpointListCreationStruct.biBlips[i])
		ENDIF		
	ENDREPEAT
	
	IF bClearMissionPlacementData
		MISSION_PLACEMENT_DATA_CHECKPOINT sTempCheckpointStruct
		COPY_SCRIPT_STRUCT(sMissionPlacementData.Checkpoint, sTempCheckpointStruct, SIZE_OF(MISSION_PLACEMENT_DATA_CHECKPOINT))
	ENDIF
ENDPROC

PROC REMOVE_ALL_PLACED_FMC_ITEMS(BOOL bClearMissionPlacementData = TRUE)
	PRINTLN("REMOVE_ALL_PLACED_FMC_ITEMS")
	FMC_REMOVE_ALL_PEDS(bClearMissionPlacementData)
	FMC_REMOVE_ALL_VEHICLES(bClearMissionPlacementData)
	FMC_REMOVE_ALL_MISSION_ENTITIES(bClearMissionPlacementData)
	FMC_REMOVE_ALL_PROPS(bClearMissionPlacementData)
	FMC_REMOVE_ALL_LOCATIONS(bClearMissionPlacementData)
	FMC_REMOVE_ALL_TRIGGER_AREAS(bClearMissionPlacementData)
	FMC_REMOVE_ALL_POPULATION_BLOCKERS(bClearMissionPlacementData)
	FMC_REMOVE_ALL_LEAVE_AREAS(bClearMissionPlacementData)
	FMC_REMOVE_ALL_MOVING_DOORS(bClearMissionPlacementData)
	FMC_REMOVE_ALL_TARGETS(bClearMissionPlacementData)
	FMC_REMOVE_ALL_CUSTOM_SPAWNS(bClearMissionPlacementData)
	FMC_REMOVE_ALL_CHECKPOINT_LIST(bClearMissionPlacementData)
	IF bClearMissionPlacementData
		SET_BIT(iLocalBitSet, biDeleteAllProps)
	ENDIF
ENDPROC

PROC DELETE_FMC_STRUCTS()
	
	PED_CREATION_STRUCT 		sTempPedStruct
	COPY_SCRIPT_STRUCT(sPedStruct, sTempPedStruct, SIZE_OF(PED_CREATION_STRUCT))
	VEHICLE_CREATION_STRUCT		sTempVehStruct
	COPY_SCRIPT_STRUCT(sVehStruct, sTempVehStruct, SIZE_OF(VEHICLE_CREATION_STRUCT))
	OBJECT_CREATION_STRUCT		sTempObjStruct
	COPY_SCRIPT_STRUCT(sObjStruct, sTempObjStruct, SIZE_OF(OBJECT_CREATION_STRUCT))
	PROP_CREATION_STRUCT		sTempPropStruct
	COPY_SCRIPT_STRUCT(sPropStruct, sTempPropStruct, SIZE_OF(PROP_CREATION_STRUCT))
	LOCATION_CREATION_STRUCT	sTempLocStruct
	COPY_SCRIPT_STRUCT(sLocStruct, sTempLocStruct, SIZE_OF(LOCATION_CREATION_STRUCT))
	LEAVE_AREA_CREATION_STRUCT	sTempLeaveAreaStruct
	COPY_SCRIPT_STRUCT(sLeaveAreaStruct, sTempLeaveAreaStruct, SIZE_OF(LEAVE_AREA_CREATION_STRUCT))
	MOVING_DOOR_CREATION_STRUCT	sTempMovingDoorStruct
	COPY_SCRIPT_STRUCT(sMovingDoorStruct, sTempMovingDoorStruct, SIZE_OF(MOVING_DOOR_CREATION_STRUCT))
	CYCLE_CAM_STRUCT			sTempCycleCamStruct
	COPY_SCRIPT_STRUCT(sCycleCamStruct, sTempCycleCamStruct, SIZE_OF(CYCLE_CAM_STRUCT))
	TARGET_STRUCT 				sTempTargetStruct
	COPY_SCRIPT_STRUCT(sTargetStruct, sTempTargetStruct, SIZE_OF(TARGET_STRUCT))
	CUSTOM_SPAWN_CREATION_STRUCT sTempCustomSpawnStruct
	COPY_SCRIPT_STRUCT(sCustomSpawns, sTempCustomSpawnStruct, SIZE_OF(CUSTOM_SPAWN_CREATION_STRUCT))
	
	PRINTLN("DELETE_FMC_STRUCTS ")
ENDPROC

PROC INITIALISE_FMC_PLACEMENTS(structFMMC_MENU_ITEMS &sFMMCmenu)
	SWITCH sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] 
		CASE eFmmc_FMC_TARGET_LIST_MENU
			INITIALISE_EXISTING_TARGETS()
		BREAK
		CASE eFmmc_FMC_CUSTOM_SPAWNS
			INITIALISE_CUSTOM_SPAWNS()
		BREAK
		CASE eFmmc_FMC_SEARCH_AREA_LIST_MENU
			INTIALISE_SEARCH_AREA_DATA()
		BREAK
		CASE eFMMC_FMC_VEH_MENU
			SET_VEHICLE_MENU_DEFAULTS()
		BREAK
	ENDSWITCH
ENDPROC

PROC CANCEL_FROM_EDITING_FMC_PROP(INT i)

	IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
		IF DOES_ENTITY_EXIST(sPropStruct.oiObject[i])	
			DELETE_OBJECT(sPropStruct.oiObject[i])
		ENDIF
		DELETE_OBJECT(sPropStruct.viCoronaObj)
		
		sMissionPlacementData.Prop.Props[i] = sCachedPropPlacementData

		sPropStruct.oiObject[i] = CREATE_OBJECT(sMissionPlacementData.Prop.Props[i].model, sMissionPlacementData.Prop.Props[i].vCoords, FALSE, FALSE, TRUE)

		SET_UP_FMC_PROP(sPropStruct.oiObject[i], i)	
		
		CREATE_FMMC_BLIP(sPropStruct.biObject[i], GET_ENTITY_COORDS(sPropStruct.oiObject[i]), HUD_COLOUR_PURPLE, "FMMC_B_5", 1)
		
		ADD_MODEL_TO_CREATOR_BUDGET(sMissionPlacementData.Prop.Props[i].model)
		
		PRINTLN("7520737 CANCEL_FROM_EDITING_FMC_PROP ", i)
	ENDIF
	DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
ENDPROC

