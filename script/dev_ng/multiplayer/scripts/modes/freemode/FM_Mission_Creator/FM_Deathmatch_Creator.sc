USING "rage_builtins.sch"
USING "globals.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_camera.sch" 
USING "FMMC_header.sch"
USING "FMMC_keyboard.sch"
USING "FMMC_MP_In_Game_Menu.sch"
USING "FM_Tutorials.sch"
USING "PM_MissionCreator_Public.sch"
USING "rc_helper_functions.sch"
USING "FMMC_Bot_Controller.sch"
USING "net_snowball.sch"
USING "FMMC_PowerUps.sch"
USING "rgeneral_include.sch"

#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF

SNOWBALL_DATA SnowballData

//Summary Window
STRUCT SUMMARY_WINDOW_DATA
	INT iCurrentModSet = -1
	DM_MODIFIER_SET__SETUP sSetupStruct
	BOOL bTeamScore
	BOOL bPercentageComparison
	BOOL bUpdate
ENDSTRUCT

SUMMARY_WINDOW_DATA sSummaryData
SCALEFORM_INDEX sfSummaryMenu
STRUCT_DL_PHOTO_VARS_LITE sDownloadPhotoVars

// Bit Fields
INT iLocalBitSet
INT iHelpBitSetOld 									= - 1
INT iHelpBitSet
INT iVehicleModelLoadTimers[FMMC_MAX_VEHICLES]
INT iPropModelTimers[FMMC_MAX_NUM_PROPS]

INT iSeamineBS = 0

//IPL/Interiors
VECTOR vIPLSelected
INT iIPLLoadBitSet
CONST_INT ciIPLBS_IslandLoaded	0

// iHelpBitSet Values
CONST_INT biPickupEntityButton 						0
CONST_INT biRandomizing 							3
CONST_INT biDeleteEntityButton 						4
CONST_INT biWarpToCameraButton 						6
CONST_INT biWarpToCoronaButton 						7

// iRandomizer stuff
INT iRand
SCALEFORM_INDEX SF_RandomizerMovie
BLIP_INDEX bRandomizerBlip
INT iSpawnNumber
INT iSpawnTypeCheck
INT iNumOfRandomPlacedItems
INT iNumOfRandomPlacedSpawns
INT iNumOfRandomPlacedWeapons
INT iRandomizeResultsTimer = 0
INT iShardTimer = 0
BOOL bPlayShardOutro = FALSE
VECTOR vRandomArray[260]
INT iTotalRandomSpawnsFound
VECTOR vRandomCentre
// Trigger Stuff
INT iTriggerCreationStage = CREATION_STAGE_WAIT

// Photo and Lobby Cameras
INT iIntroCamPlacementStage
INT iCamPanState = PAN_CAM_SETUP
PREVIEW_PHOTO_STRUCT PPS
SCALEFORM_INDEX SF_Movie_Gallery_Shutter_Index
BLIP_INDEX bCameraTriggerBlip
BLIP_INDEX bCameraPanBlip
BLIP_INDEX bPhotoBlip
JOB_INTRO_CUT_DATA jobIntroData
VECTOR vInitialPreviewPos
BOOL bInitialIntroCamSetup = TRUE
BOOL bInitialIntroCamWarp = FALSE

BLIMP_SIGN sBlimpSign
FMMC_ARENA_TRAPS_HOST_INFO sTrapInfo_Host
FMMC_ARENA_TRAPS_LOCAL_INFO sTrapInfo_Local

//INT iHelptextBitset
//SCRIPT_TIMER stHelptextTimer

INT iVehicleSpawnedBitset
INT iVehicleRespawnBitset
INT iVehicleCreatedBitset
INT iVehicleRespawnGhostedBitset
SHAPETEST_INDEX stiVehSafe
SHAPETEST_STATUS stiVehSafeStatus
INT iVehToShapeTest = 0
SCRIPT_TIMER td_VehCleanupTimer[FMMC_MAX_VEHICLES]
	
// Test Stuff
INT iPlayerTeamNum
BOOL bOnGroundBeforeTest = FALSE
int iNumOnTeam1
int iNumOnTeam2
INT iPlayerRespawnTimer
BOOL bPlayerIsRespawning = FALSE
BOOL bPlayerIsInvincible = FALSE
BOOL bTestStatBeenIncreased = FALSE
INT iQuickRespawnBarExtraFill = 0
TEST_PEDS testPeds[FMMC_MAX_SPAWNPOINTS]
MODEL_NAMES DM_TestPedModel[4]
INT iTestKills = 0
INT iTestScore = 0
INT iMyPreviousScore = 0
INT iIndividualScore = 0
INT TEST_TIMER
INT iGlobalTestKills = 0
INT iGlobalTestScore = 0
PED_INDEX piLastKilled
FLOAT fManualRespawnFill = 0.0
INT iWorldPropIterator
LEGACY_RUNTIME_WORLD_PROP_DATA sRuntimeWorldPropData
SCRIPT_TIMER tdPickupSpawnTimer
INT iTestLocalBitSet
INT iStaggeredPickup

STRUCT TEST_PED_PLAYER_MODIFIERS
	PLAYER_MODIFIER_DATA sModifierData
	INT iCurrentModSet = -1
	INT iPendingModSet
ENDSTRUCT
TEST_PED_PLAYER_MODIFIERS sTestPedModifiers[FMMC_MAX_TEAMS]
INT iPedNeedsModifierReapplied[2]
MODEL_NAMES mnTeamVehicle[FMMC_MAX_TEAMS]

//Test Lives
INT iTestMaxLives
INT iTestExtraLives
INT iTestMaxTeamLives[FMMC_MAX_TEAMS]
INT iTestTeamExtraLives[FMMC_MAX_TEAMS]
INT iTestDeaths
INT iTestTeamDeaths[FMMC_MAX_TEAMS]
BOOL bOutofLives
BOOL bLivingEnemies
BOOL bValidLivesCheck

TARGET_FLOATING_SCORE sTargetFloatingScores[TARGET_MAX_FS]

CONST_INT ciTest_MinimumScore 10
CONST_INT ciTestLocalBS_DelayedPickupsSpawned	0

// Tutorial Stuff
INT iTutorialPrevWepNum = 0
structDMCreatorTutorial tutDMCreator
STRING sTutorialDescription

structNGCreatorTutorial tutNGCreator
CONST_INT ciKOTHTutorial_VehsToPlace		5
CONST_INT ciKOTHTutorial_WeaponsToPlace		5

// Camera Switching Stuff
VECTOR vSwitchVec = <<0,0,0>>
FLOAT fSwitchHeading = 0
BOOL bSwitchingCam = FALSE

// Confirmation/Warning Menus
BOOL ButtonPressed
BOOL bCreatorLimitedCloudDown = FALSE
BOOL bSignedOut = FALSE

BOOL bContentReadyForUGC = FALSE
int iTopItem
BOOL bShowMenu = TRUE
BOOL bShowMenuHighlight = FALSE
BOOL bDelayAFrame = TRUE
INT iLoadingOverrideTimer
BOOL bLastUseMouseKeyboard = FALSE

// Door Stuff
INT iDoorSetupStage
INT iDoorSlowLoop

// King of the Hill //
INT iKOTH_HillErrorBS
FLOAT fKOTH_RadiusIncrementVelocity = 0.0

// // // // // // // //

INT iSpawnVehCheckIndex = 0
INT iSpawnVehTeamIndex = 0

CONST_INT ciSPVS_Spawning		0
CONST_INT ciSPVS_Checking		1
CONST_INT ciSPVS_Done			2
INT iSpawnVehCheckState = ciSPVS_Spawning

INT iMaxSpawnsPerTeam[FMMC_MAX_TEAMS]

BOOL bHasSpawned = FALSE

INT iTestMenuTimestamp = 0

INTERIOR_INSTANCE_INDEX interiorArena_VIPLounge
INT iWarpState, iWarpStateTimeoutStart
VECTOR vArenaCamCoords

//BLIP_INDEX				biBlipBounds

//CREATION STRUCTS
REL_GROUP_HASH 							lrgFM_Team[FMMC_MAX_SPAWNPOINTS]
FMMC_LOCAL_STRUCT 						sFMMCdata
FMMC_CAM_DATA 							sCamData
FMMC_COMMON_MENUS_VARS 					sFMMCendStage
PED_CREATION_STRUCT 					sPedStruct
TEAM_SPAWN_CREATION_STRUCT				sTeamSpawnStruct[FMMC_MAX_TEAMS]
VEHICLE_CREATION_STRUCT					sVehStruct
WEAPON_CREATION_STRUCT					sWepStruct
OBJECT_CREATION_STRUCT					sObjStruct
LOCATION_CREATION_STRUCT				sLocStruct // [William.kennedy@rockstarnorth.com] - mission creator needs this to be an argument in shared command FMMC_DO_MENU_ACTIONS. Does nothing in this script, added so it compiles. 
RAND_CREATION_STRUCT					sRandStruct	
HUD_COLOURS_STRUCT 						sHCS
structFMMC_MENU_ITEMS 					sFMMCmenu
CREATION_VARS_STRUCT 					sCurrentVarsStruct
PROP_CREATION_STRUCT					sPropStruct
DYNOPROP_CREATION_STRUCT				sDynoPropStruct
INVISIBLE_OBJECT_STRUCT					sInvisibleObjects
START_END_BLIPS							sStartEndBlips
GET_UGC_CONTENT_STRUCT 					sGetUGC_content
FMMC_UP_DOWN_STRUCT						menuScrollController
TRAIN_CREATION_STRUCT					sTrainCreationStruct

FMMC_POWERUP_STRUCT						sPowerUps
FMMC_VEHICLE_MACHINE_GUN				sVehMG
FMMC_VEHICLE_MACHINE_GUN_PBD 			sVehMG_PBD

PLAYER_MODIFIER_DATA					sPlayerModifierData
INT iCurrentPlayerModSet = -1
INT iPendingPlayerModSet

SCRIPT_TIMER tdBlockBoundsOnRespawnTimer
CONST_INT ciBlockBoundsOnRespawnTimerLength 5000

ENUM DEAD_PLAYER_STATE 
	DPS_WAIT_TILL_FADE,
	DPS_FADE,
	DPS_FADE_UP,
	DPS_INVINCIBLE
ENDENUM
DEAD_PLAYER_STATE iDeadPlayerState

ENUM RESET_STATE 
	RESET_STATE_FADE,
	RESET_STATE_CLEAR,
	RESET_STATE_WEAPONS,
	RESET_STATE_UNLOAD_WEAPONS,
	RESET_STATE_VEHICLES,
	RESET_STATE_UNLOAD_VEHICLES,
	RESET_STATE_PROPS,
	RESET_STATE_DYNOPROPS,
	RESET_STATE_OBJECTS,
	RESET_STATE_UNLOAD_PROPS,
	RESET_STATE_ATTACH,
	RESET_STATE_SPAWNS,
	RESET_STATE_TEAM_SPAWNS,
	RESET_STATE_OTHER,
	RESET_STATE_FINISH
ENDENUM
RESET_STATE iResetState

ENUM SET_UP_TEST_STATE 
	SET_UP_TEST_STATE_FADE,
	SET_UP_TEST_STATE_CLEAR,
	SET_UP_TEST_STATE_NETWORK_SETUP,
	SET_UP_TEST_PLAYER_MODIFIERS,
	SET_UP_TEST_STATE_CREATE_PLAYER,
	SET_UP_TEST_STATE_LOAD_AUDIO,
	SET_UP_TEST_STATE_PLACE_PLAYER,
	SET_UP_TEST_STATE_PLACE_WEPS,
	SET_UP_TEST_STATE_PLACE_VEHS,
	SET_UP_TEST_STATE_PLACE_PROPS,
	SET_UP_TEST_STATE_PLACE_DYNOPROPS,
	SET_UP_TEST_STATE_PLACE_OBJS,
	SET_UP_TEST_STATE_PLACE_PEDS,
	SET_UP_TEST_STATE_FINISH
ENDENUM
SET_UP_TEST_STATE iSetUpTestState

BOOL bIsMaleSPTC
BOOL bIsCreatorModelSetSPTC

INT iTestAlertStartFrame = -1

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////              DEBUG FUNCTIONS	                //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD
USING "fmmc_mp_setup_mission.sch"
USING "select_mission_stage.sch"

COVER_CREATION_STRUCT					sCoverStruct

BOOL bSave
BOOL bLoad
BOOL bClean
BOOL bDoJumps
BOOL bLoadRockstar
BOOL bLoadRockstarHeader
BOOL bDrawZmenu
BOOL bDrawSmenu
BOOL bLoadDirectory
BOOL bLoadXML
INT iXmlToLoad, iXmlToSave
FLOAT fMultiplyer = 10.0
FLOAT max
FLOAT min
FLOAT fDebugHeight = 0.0
INT iModelArray = 0
VECTOR vVehicleRotationDebug
BOOL bGetUGC_byIDWithString
TEXT_WIDGET_ID twID

BOOL bForcedPublicCreator

WIDGET_GROUP_ID wgGroup
PROC CREATE_WIDGETS( )

	IF wgGroup= wgGroup
	ENDIF
	wgGroup= START_WIDGET_GROUP( " FMMC Deathmatch Creator") 
		
		ADD_WIDGET_BOOL(" Add RockStar Blips", sFMMCendStage.sRocStarCreatedVars.bDisplayRockstarUGC)
		ADD_WIDGET_BOOL(" Allow Mouse Input", sFMMCmenu.bAllowMouseInput)
		ADD_WIDGET_BOOL(" Allow Mouse Entity Selection", sFMMCmenu.bAllowMouseSelection)
		
		ADD_WIDGET_INT_SLIDER("Cloud Load Version", g_FMMC_STRUCT.iVersion, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("Cloud Load Folder", g_FMMC_STRUCT.iFolder, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("Switching Int", g_CreatorsSelDetails.iSwitchingINT, 0, 1000, 0)
		
		ADD_WIDGET_INT_READ_ONLY("iTeam", sFMMCMenu.iSelectedTeam)
		
		ADD_WIDGET_INT_READ_ONLY("iPropLibrary", sFMMCmenu.iPropLibrary)
		ADD_WIDGET_INT_READ_ONLY("iPropType", sFMMCmenu.iPropType)
		ADD_WIDGET_VECTOR_SLIDER("Current prop rotation", sFMMCmenu.vLastPropRotation, -360.0, 360.0, 0.0)
		
		START_WIDGET_GROUP( "XML")
			ADD_WIDGET_BOOL("Save Xml",sCurrentVarsStruct.bSaveXml)
			ADD_WIDGET_BOOL("Load XML",bLoadXML)
			ADD_WIDGET_INT_SLIDER("Xml To Save", iXmlToSave, 0, 31, 1)
			ADD_WIDGET_INT_SLIDER("Xml To Load", iXmlToLoad, 0, 31, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("STATS AND STUFF")
			ADD_WIDGET_BOOL("Made a change", sCurrentVarsStruct.creationStats.bMadeAChange)
			ADD_WIDGET_INT_SLIDER("Times tested", sCurrentVarsStruct.creationStats.iTimesTestedLoc, 0, 50, 1)
		STOP_WIDGET_GROUP()
		
//		START_WIDGET_GROUP("FlatbedAreaCheck")
//			ADD_WIDGET_VECTOR_SLIDER("flatbed area check offset 1", vFlatBedForwardOffset, 	-5.0, 5.0, 0.1)
//			ADD_WIDGET_VECTOR_SLIDER("flatbed area check offset 2", vFlatBedBackwardOffset, -5.0, 5.0, 0.1)
//			ADD_WIDGET_FLOAT_SLIDER("flatbed are check width", fFlatBedOffsetWidth, 0.0, 5.0, 0.1)
//		STOP_WIDGET_GROUP()
		
		INT i 
		TEXT_LABEL_15 tl15
		ADD_WIDGET_INT_SLIDER("Current Save Slot", sFMMCendStage.iMenuReturn, 0, 31, 1)

		ADD_WIDGET_BOOL("g_bMissionCreatorActive", g_bMissionCreatorActive)
		ADD_WIDGET_FLOAT_SLIDER("RADAR ZOOM OFFSET", sCurrentVarsStruct.fCurrentRadarZoom,-1000, 1000, 0.01)
		ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentSelection", sFMMCmenu.iCurrentSelection,-1, 99, 1)
		
		ADD_WIDGET_BOOL("bDoJumps", bDoJumps)
		ADD_WIDGET_FLOAT_SLIDER("fMultiplyer", fMultiplyer, 0, 400, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("max", max, -400, 400, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("min", min, -400, 400, 0.01)
		
		ADD_WIDGET_FLOAT_SLIDER("fDebugHeight", fDebugHeight, -1.0, 0.0, 0.01)
	   	ADD_WIDGET_BOOL("Have Full permissions", g_bHaveFull_NetworkPrivileges)

		ADD_WIDGET_BOOL("bLoadRockstarHeader", bLoadRockstarHeader)
		ADD_WIDGET_BOOL("bLoadDirectory", bLoadDirectory)
		ADD_WIDGET_BOOL("bClean", bClean)
		ADD_WIDGET_BOOL("bSave", bSave)
		ADD_WIDGET_BOOL("bLoad", bLoad)
		ADD_WIDGET_BOOL("bLoadRockstar", bLoadRockstar)
		ADD_WIDGET_BOOL("bDontDrawSmenu", bDrawSmenu)
		ADD_WIDGET_BOOL("bDontDrawZmenu", bDrawZmenu)
		ADD_WIDGET_BOOL("bDrawMenu", bDrawMenu)
		
		ADD_WIDGET_STRING("Switch Statement States")
		ADD_WIDGET_INT_SLIDER("MAIN STATE", sCurrentVarsStruct.iEntityCreationStatus, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("Current Entity Type", sFMMCmenu.iEntityCreation,-1, 99, 1)
		ADD_WIDGET_INT_SLIDER("sFMMCmenu.iEntityCreation",sFMMCmenu.iEntityCreation,-1, 99, 1)
		
		SET_UP_FMMC_GLOBAL_WIDGETS()
		
		START_WIDGET_GROUP("Disc Marker")  
			ADD_WIDGET_VECTOR_SLIDER("Scale", sCurrentVarsStruct.vCoronaDiscScale, -360, 360, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( " UGC_GET_GET_BY_CONTENT_ID") 
			twID = ADD_TEXT_WIDGET("")
			ADD_WIDGET_BOOL("Get it", bGetUGC_byIDWithString )			
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP( "Everything else")  
			ADD_WIDGET_INT_SLIDER("iNumParticipants", g_FMMC_STRUCT.iNumParticipants, -1, 16, 1)
			ADD_WIDGET_FLOAT_SLIDER("sCurrentVarsStruct.fCreationHeading", sCurrentVarsStruct.fCreationHeading, 0, 360, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("sCurrentVarsStruct.fCheckPointSize", sCurrentVarsStruct.fCheckPointSize, 0, 30, 0.1)
			ADD_WIDGET_INT_READ_ONLY("sFMMCMenu.iSelectedEntity", sFMMCMenu.iSelectedEntity)
			ADD_WIDGET_INT_READ_ONLY("iLocalBitSet", iLocalBitSet)
			ADD_WIDGET_FLOAT_READ_ONLY("vVehicleRotationDebug.x", vVehicleRotationDebug.x)
			
			ADD_WIDGET_INT_SLIDER("sPedStruct.iCurrentRelGroupSelection", sPedStruct.iCurrentRelGroupSelection[0], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sPedStruct.iCurrentRelGroupSelection[0]", sPedStruct.iCurrentRelGroupSelection[0], 0, 99, 1)			
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_PEDS]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_PEDS], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPONS]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPONS], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLES]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLES], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("iEntityCreationStatus", sCurrentVarsStruct.iEntityCreationStatus, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("g_FMMC_STRUCT.iNumberOfCheckPoints", g_FMMC_STRUCT.iNumberOfCheckPoints, 0, 99, 1)
			ADD_WIDGET_FLOAT_SLIDER("g_FMMC_STRUCT.fGridLength", g_FMMC_STRUCT.fGridLength, 0, 9999, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_FMMC_STRUCT.fGridWidth", g_FMMC_STRUCT.fGridWidth, 0, 9999, 0.01)
			ADD_WIDGET_INT_READ_ONLY("iNumberOfTeamSpawnPoints[0]", g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0])
			ADD_WIDGET_INT_READ_ONLY("iNumberOfTeamSpawnPoints[1]", g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1]) 
			ADD_WIDGET_INT_SLIDER("Vehicle Deathmatch", g_FMMC_STRUCT.iVehicleDeathmatch, 0, 1, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "g_FMMC_STRUCT Spawn coordinates") 
			START_WIDGET_GROUP("vSpawnpoint") 
				FOR i = 0 TO (FMMC_MAX_SPAWNPOINTS - 1)
					tl15 = "vSP - "
					tl15 += i
					ADD_WIDGET_VECTOR_SLIDER(tl15, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, -9999.0, 9999.0,0.100 )
				ENDFOR
			STOP_WIDGET_GROUP()			
			START_WIDGET_GROUP("Team 1 Coords") 
				ADD_WIDGET_INT_READ_ONLY("iNumberOfTeamSpawnPoints[0] ", g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0])
				ADD_WIDGET_INT_READ_ONLY("iSwitchingINT[0] ", sTeamSpawnStruct[0].iSwitchingINT)
				FOR i = 0 TO (FMMC_MAX_TEAMSPAWNPOINTS - 1)
					tl15 = "vCoord "
					tl15 += i
					ADD_WIDGET_VECTOR_SLIDER(tl15, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][i].vPos, -9999.0, 9999.0,0.100 )
				ENDFOR
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Team 2 Coords") 
				ADD_WIDGET_INT_READ_ONLY("iNumberOfTeamSpawnPoints[1] ", g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1])
				ADD_WIDGET_INT_READ_ONLY("iSwitchingINT[1] ", sTeamSpawnStruct[1].iSwitchingINT)
				FOR i = 0 TO (FMMC_MAX_TEAMSPAWNPOINTS - 1)
					tl15 = "Coord "
					tl15 += i
					ADD_WIDGET_VECTOR_SLIDER(tl15, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[1][i].vPos, -9999.0, 9999.0,0.100 )
				ENDFOR
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Team 3 Coords") 
				ADD_WIDGET_INT_READ_ONLY("iNumberOfTeamSpawnPoints[2] ", g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2])
				ADD_WIDGET_INT_READ_ONLY("iSwitchingINT[2] ", sTeamSpawnStruct[2].iSwitchingINT)
				FOR i = 0 TO (FMMC_MAX_TEAMSPAWNPOINTS - 1)
					tl15 = "Coord "
					tl15 += i
					ADD_WIDGET_VECTOR_SLIDER(tl15, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[2][i].vPos, -9999.0, 9999.0,0.100 )
				ENDFOR
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Team 4 Coords") 
				ADD_WIDGET_INT_READ_ONLY("iNumberOfTeamSpawnPoints[3] ", g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[3])
				ADD_WIDGET_INT_READ_ONLY("iSwitchingINT[3] ", sTeamSpawnStruct[3].iSwitchingINT)
				FOR i = 0 TO (FMMC_MAX_TEAMSPAWNPOINTS - 1)
					tl15 = "Coord "
					tl15 += i
					ADD_WIDGET_VECTOR_SLIDER(tl15, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[3][i].vPos, -9999.0, 9999.0,0.100 )
				ENDFOR
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "g_FMMC_STRUCT Spawn coordinates") 
			START_WIDGET_GROUP( "vSpawnpoint") 
				FOR i = 0 TO (FMMC_MAX_SPAWNPOINTS - 1)
					tl15 = "vSP - "
					tl15 += i
					ADD_WIDGET_VECTOR_SLIDER(tl15, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, -9999.0, 9999.0,0.100 )
				ENDFOR
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("closest invisible objects array") 
			START_WIDGET_GROUP("vPositions of objects") 
				FOR i = 0 TO IMAX_INVISIBLE_OBJECTS - 1
					tl15 = "vPos - "
					tl15 += i
					ADD_WIDGET_VECTOR_SLIDER(tl15, sInvisibleObjects.vClosestPos[i], -9999.0, 9999.0,0.100 )
				ENDFOR
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "g_FMMC_STRUCT Check point coordinates") 
			START_WIDGET_GROUP( "vCheckPoint") 
				FOR i = 0 TO (GET_FMMC_MAX_NUM_CHECKPOINTS() - 1)
					tl15 = "vCheckPoint"
					tl15 += i
					ADD_WIDGET_VECTOR_SLIDER(tl15, g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint, -9999.0, 9999.0,0.100 )
				ENDFOR
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
		
	STOP_WIDGET_GROUP()
	
	CREATE_DM_TUTORIAL_WIDGET(wgGroup, tutDMCreator)
ENDPROC		

PROC UPDATE_WIDGETS()
	UPDATE_DM_TUTORIAL_WIDGET(tutDMCreator)
ENDPROC

//PURPOSE: DEBUG - print int and string into the TTY
PROC DEBUG_PRINT_INT(STRING s, INT iInt)
	PRINTNL()
	PRINTSTRING(" Mission Creator - ")
	PRINTSTRING(s)
	PRINTINT(iInt)
	PRINTNL()
ENDPROC	

#ENDIF

PROC UPDATE_AVAILABLE_TEAMS()
	INT i		
	
	FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] >= iMaxSpawnsPerTeam[i]
		AND i < g_FMMC_STRUCT.iMaxNumberOfTeams
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.iBS_AvailableDMTeams, i)
				PRINTLN("Setting ", i, " because g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] is ", g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i], " and iMaxSpawnsPerTeam[i] is ", iMaxSpawnsPerTeam[i])
				SET_BIT(g_FMMC_STRUCT_ENTITIES.iBS_AvailableDMTeams, i)
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.iBS_AvailableDMTeams, i)
				PRINTLN("CLEARING ", i, " because g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] is ", g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i], " and iMaxSpawnsPerTeam[i] is ", iMaxSpawnsPerTeam[i])
				CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.iBS_AvailableDMTeams, i)
			ENDIF
		ENDIF
		
	ENDFOR			
ENDPROC

PROC CHECK_NEEDS_RETESTED()

	INT iTeam
	//Check to see if we need to clear the tested bit sets
	REPEAT FMMC_MAX_TEAMS iTeam
		IF IS_PUBLIC_ARENA_CREATOR()
		AND NOT IS_KING_OF_THE_HILL()
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.biTeamTestComplete, iTeam)
				SET_BIT(g_FMMC_STRUCT.biTeamTestComplete, iTeam)
				PRINTLN("CHECK_NEEDS_RETESTED - Clearing team ", iTeam, " as tested")
			ENDIF
			sFMMCendStage.bMissionNeedsRetested[iTeam] = FALSE
			RELOOP
		ENDIF
		IF sFMMCendStage.bMissionNeedsRetested[iTeam]
			IF IS_BIT_SET(g_FMMC_STRUCT.biTeamTestComplete, iTeam)
				CLEAR_BIT(g_FMMC_STRUCT.biTeamTestComplete, iTeam)
				PRINTLN("CHECK_NEEDS_RETESTED - Clearing team ", iTeam, " as tested")
			ENDIF
			sFMMCendStage.bMissionNeedsRetested[iTeam] = FALSE
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_THIS_A_TEAM_DM()
	RETURN g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
ENDFUNC

// PURPOSE:
///    Make a string point at a text label.
/// RETURNS:
///    A STRING which points at whatever was passed in.
FUNC STRING TEXT_LABEL_INTO_STRING(STRING sTextLabel)
	RETURN sTextLabel
ENDFUNC

PROC PROCESS_DEATHMATCH_BOUNDS_ERRORS()
	INT iSpawn
	BOOL bSpawnError = FALSE
	
	INT iTeamSpawnPoints = g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] + g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[3]
	iTeamSpawnPoints = CLAMP_INT(iTeamSpawnPoints, 0, FMMC_MAX_TEAMSPAWNPOINTS)
	
	INT iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
	
	FOR iSpawn = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints - 1
		IF iSpawn < FMMC_MAX_SPAWNPOINTS
			IF NOT IS_POINT_IN_DEATHMATCH_BOUNDS(iBoundsIndex, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iSpawn].vPos, <<0,0,0>>, <<0,0,0>>)
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iSpawn].vPos)
				PRINTLN("[KOTH] Placed Spawn point ", iSpawn, " is out of bounds!")
				bSpawnError = TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iSpawn = 0 TO iTeamSpawnPoints - 1
		INT iTeam
		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
			IF NOT IS_POINT_IN_DEATHMATCH_BOUNDS(iBoundsIndex, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawn].vPos, <<0,0,0>>, <<0,0,0>>)
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawn].vPos)
				PRINTLN("[KOTH] Team Spawn point ", iSpawn, " is out of bounds!")
				bSpawnError = TRUE
			ENDIF
		ENDFOR
	ENDFOR
	
	IF NOT bSpawnError
		CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS)
	ELSE
		SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS)
	ENDIF
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////        	  	Change Menu Bits		       	//////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC BOOL ARE_TEAM_SPAWN_POINTS_REQUIRED()
	IF g_FMMC_STRUCT.iTeamDeathmatch != FMMC_DEATHMATCH_TEAM
	AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LockedDeathmatchType)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    This handles opening and closing the save/publish options, as well as setting sFMMCmenu.iBitMenuItemAlert which will be used to show warning icons in the menu.
PROC CHECK_FOR_END_CONDITIONS_MET()

	CLEAR_BIT_ALERT(sFMMCmenu)
	
	IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionName)
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
	ENDIF
	
	IF IS_MISSION_DESCIPTION_EMPTY()
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_DESC)
	ENDIF
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
	ENDIF	

	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraPanPos)
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPhoto, FMMC_PHOTO_TAKEN)
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iPhoto, FMMC_PHOTO_TAKEN)
	AND g_FMMC_STRUCT.iPhotoPath != UGC_PATH_PHOTO_NG
	AND !bNextGenPhotoTaken 
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_LAST_GEN_PHOTO)
	ENDIF
	
	IF NOT SCRIPT_IS_CLOUD_AVAILABLE()
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
			IF bCreatorLimitedCloudDown = FALSE
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_CLOUD_FAILURE
			ENDIF
		ENDIF
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_NAME)
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_DESCRIPTION)
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_TAGS)	
	ELSE
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
			IF bCreatorLimitedCloudDown = TRUE
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_CLOUD_FAILURE
			ENDIF
		ENDIF
		CLEAR_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_NAME)
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_DESCRIPTION)
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_TAGS)
	ENDIF
	
	IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_TEAM_OPTIONS)
	ELSE
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_TEAM_OPTIONS)
	ENDIF
	
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_RESPAWN_VEHICLE)

	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_AVAILABLE_VEHICLES_ARENA)
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_DEFAULT_VEHICLE)
	
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_ARENA)
	SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_DM_DEV_MENU)
	
	INT iRemainingSpawns1 = ((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints
	IF iRemainingSpawns1 > 0
	AND NOT FMMC_IS_ROCKSTAR_DEV()
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
	ENDIF
	
	INT iSpawn = 0
	INT iTeam = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		sPedStruct.iNumberOfTeamSpawnsNeeded[iTeam] = 0
		sPedStruct.iNumberOfTeamSpawns[iTeam] = 0
		
		FOR iSpawn = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints-1				
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iSpawn].iTeamRestrictedBS, iTeam)
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iSpawn].vPos)
			AND iTeam < g_FMMC_STRUCT.iMaxNumberOfTeams
				sPedStruct.iNumberOfTeamSpawns[iTeam]++
				
			ENDIF
		ENDFOR
	ENDFOR
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		// Reset
		CLEAR_BIT(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_FEW_TEAM_1_RESPAWN_POINTS+iTeam)		
		
		// Set
		sPedStruct.iNumberOfTeamSpawnsNeeded[iTeam] = (((g_FMMC_STRUCT.iNumParticipants + 2) * 2)/(g_FMMC_STRUCT.iMaxNumberOfTeams))
		IF sPedStruct.iNumberOfTeamSpawnsNeeded[iTeam] > g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam]
			sPedStruct.iNumberOfTeamSpawnsNeeded[iTeam] = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam]
		ENDIF
		PRINTLN("[TMSDM] sPedStruct.iNumberOfTeamSpawnsNeeded[", iTeam, "] = ", sPedStruct.iNumberOfTeamSpawnsNeeded[iTeam])
		sPedStruct.iNumberOfTeamSpawnsNeeded[iTeam] -= sPedStruct.iNumberOfTeamSpawns[iTeam]
		PRINTLN("[TMSDM] sPedStruct.iNumberOfTeamSpawns[", iTeam, "] = ", sPedStruct.iNumberOfTeamSpawns[iTeam])
		
		IF iTeam < g_FMMC_STRUCT.iMaxNumberOfTeams
			IF sPedStruct.iNumberOfTeamSpawnsNeeded[iTeam] > 0
				SET_BIT(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_FEW_TEAM_1_RESPAWN_POINTS + iTeam)
				SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
				PRINTLN("[TMSDM] Setting ciMENU_ALERT_2_TOO_FEW_TEAM_1_RESPAWN_POINTS for team ", iTeam)
			ELSE
				PRINTLN("[TMSDM] Clearing spawns needed menu alert for team ", iTeam)
			ENDIF
			
			//Check to see if it's untested. 
			IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.biTeamTestComplete, TEAM_1_TEST_COMPLETE+iTeam)
				AND (NOT IS_PUBLIC_ARENA_CREATOR() OR IS_KING_OF_THE_HILL())
					SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_UNTESTED)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	iMaxSpawnsPerTeam[0] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/2.0)
	iMaxSpawnsPerTeam[1] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/2.0)
	iMaxSpawnsPerTeam[2] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/2.0)
	iMaxSpawnsPerTeam[3] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/2.0)
			
	ADJUST_NUMBER_OF_PLAYERS_ON_TEAM_BASED_ON_RATIO(iMaxSpawnsPerTeam)	
	
	UPDATE_AVAILABLE_TEAMS()
	
	IF g_FMMC_STRUCT.iVehicleDeathmatch = 0
		INT i
		IF ARE_TEAM_SPAWN_POINTS_REQUIRED()
			FOR i = 0 TO g_FMMC_STRUCT.iMaxNumberOfTeams - 1
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.iBS_AvailableDMTeams, i)
				
					PRINTLN("Team doesn't have enough spawn points! Team is: ", i)
					SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)
				ENDIF
			ENDFOR
		ENDIF
										
		CLEAR_BIT(sFMMCMenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_MANY_START_POINTS)
		FOR iTeam = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)			
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeam] > iMaxSpawnsPerTeam[iTeam]
				SET_BIT(sFMMCMenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_MANY_START_POINTS) //If ANY team is over the limit, set the bit
				PRINTLN("Team has too many start points! Team: ", iTeam)
			ENDIF
		ENDFOR
		
		IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)
			IF IS_SKYSWOOP_IN_SKY()
				iMaxSpawnsPerTeam[0] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/2.0)
				iMaxSpawnsPerTeam[1] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/2.0)
				iMaxSpawnsPerTeam[2] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/3.0)
				iMaxSpawnsPerTeam[3] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/4.0)	
				
				ADJUST_NUMBER_OF_PLAYERS_ON_TEAM_BASED_ON_RATIO(iMaxSpawnsPerTeam)
			ENDIF
		ENDIF
	ENDIF
	
	// Normal Spawns
	INT iVSpwnBS = 0
	BOOL bVSpwnAllGood = TRUE
	FOR iVSpwnBS = 0 TO (FMMC_MAX_SPAWNPOINTS / 30) - 1
		IF sFMMCdata.iVehSpawnInvalidBS[iVSpwnBS] > 0
			bVSpwnAllGood = FALSE
		ENDIF
	ENDFOR
	IF bVSpwnAllGood
		IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_OBSCURED)
			CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_SPAWN_OBSCURED)
			REFRESH_MENU(sFMMCMenu)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_OBSCURED)
			SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_SPAWN_OBSCURED)
			REFRESH_MENU(sFMMCMenu)
		ENDIF
	ENDIF
	
	// Veh spawns
	bVSpwnAllGood = TRUE
	INT iVspwnTeam = 0
	FOR iVspwnTeam = 0 TO FMMC_MAX_TEAMS - 1
		FOR iVSpwnBS = 0 TO (FMMC_MAX_TEAMSPAWNPOINTS / 30) - 1
			IF sFMMCdata.iVehTeamSpawnInvalidBS[iVspwnTeam][iVSpwnBS] > 0
				bVSpwnAllGood = FALSE
			ENDIF
		ENDFOR
	ENDFOR
	IF bVSpwnAllGood
		IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_OBSCURED)
			CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_START_POINT_OBSCURED)
			REFRESH_MENU(sFMMCMenu)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_OBSCURED)
			SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_START_POINT_OBSCURED)
			REFRESH_MENU(sFMMCMenu)
		ENDIF
	ENDIF
	
	IF IS_KING_OF_THE_HILL()
		IF g_FMMC_STRUCT.sKotHData.iNumberOfHills = 0
			SET_BIT(sFMMCMenu.iBitMenuItemAlert2, ciMENU_ALERT_2_NO_HILLS_PLACED)
		ELSE
			CLEAR_BIT(sFMMCMenu.iBitMenuItemAlert2, ciMENU_ALERT_2_NO_HILLS_PLACED)
		ENDIF
		
		PROCESS_DEATHMATCH_BOUNDS_ERRORS()
		
		IF iKOTH_HillErrorBS > 0
			SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_KOTH_HILL_OOB)
		ELSE
			CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_KOTH_HILL_OOB)
		ENDIF
		
		IF sFMMCmenu.iKOTH_NonHillObjErrorBS > 0
			SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_NON_HILL_OBJ_IN_KOTH)
		ELSE
			CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_NON_HILL_OBJ_IN_KOTH)
		ENDIF
		
	ENDIF
	
	INT iMaxParticipants = g_FMMC_STRUCT.iNumParticipants+2
	INT iTeamsUsingMaxNumbers
	INT iDifference 		
	FOR iTeam = 0 TO 1 // g_FMMC_STRUCT.iMaxNumberOfTeams-1 only works with 2 teams at the moment.
		iDifference += g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam]
		IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam] > 0
			iTeamsUsingMaxNumbers++
		ENDIF
	ENDFOR
	
	IF iTeamsUsingMaxNumbers > 1
		iDifference -= iMaxParticipants
		iDifference = ABSI(iDifference)
		IF iDifference >= 1			
			SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_TEAM_BALANCING_NOT_BALANCED)
			PRINTLN("sCurrentVarsStruct.creationStats.bMadeAChange = TRUE iDifference: ", iDifference)
		ELSE
			CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_TEAM_BALANCING_NOT_BALANCED)
		ENDIF
	ELSE
		CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_TEAM_BALANCING_NOT_BALANCED)
	ENDIF
	
	IF HAS_DM_CREATOR_TUTORIAL_COMPLETED(tutDMCreator)
		IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)	
		OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)	
		OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_MANY_START_POINTS)
		OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_NO_HILLS_PLACED)
		OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_KOTH_HILL_OOB)
		OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS)
		OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_NON_HILL_OBJ_IN_KOTH)
		OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_OBSCURED)
		OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_START_POINT_OBSCURED)
		OR sFMMCMenu.iDMModsetSpawnPointBitset != 0
		OR NOT SCRIPT_IS_CLOUD_AVAILABLE()	
			CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
			
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)	 
				PRINTLN("Can't test due to ciMENU_ALERT_SPAWNS")
			ENDIF
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)	 
				PRINTLN("Can't test due to ciMENU_ALERT_TEAM_SPAWNS")
			ENDIF
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_MANY_START_POINTS)	 
				PRINTLN("Can't test due to ciMENU_ALERT_2_TOO_MANY_START_POINTS")
			ENDIF
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_NO_HILLS_PLACED)	 
				PRINTLN("Can't test due to ciMENU_ALERT_2_NO_HILLS_PLACED")
			ENDIF
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_KOTH_HILL_OOB)	 
				PRINTLN("Can't test due to ciMENU_ALERT_2_KOTH_HILL_OOB")
			ENDIF
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_NON_HILL_OBJ_IN_KOTH)	 
				PRINTLN("Can't test due to ciMENU_ALERT_2_NON_HILL_OBJ_IN_KOTH")
			ENDIF
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS)	 
				PRINTLN("Can't test due to ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS")
			ENDIF
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_OBSCURED)	 
				PRINTLN("Can't test due to ciMENU_ALERT_2_SPAWN_OBSCURED")
			ENDIF
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_START_POINT_OBSCURED)	 
				PRINTLN("Can't test due to ciMENU_ALERT_2_START_POINT_OBSCURED")
			ENDIF
			IF sFMMCMenu.iDMModsetSpawnPointBitset != 0
				PRINTLN("Can't test due to iDMModsetSpawnPointBitset")
			ENDIF
		ELSE
			SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
		ENDIF
		
		IF sCurrentVarsStruct.bTestModeFailed
			PRINTLN("[DM] Network has bailed! Disabling test")
			CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
		ENDIF
	ELSE		
		IF tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_WEAPONS
		OR tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_PLACE_ALL_WEAPONS
		OR tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_ONFOOT_PLACEMENT		
		OR tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_PLACE_IN_STORE
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TUTORIAL_WEAPONS)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET_ALERT(sFMMCmenu)
		CLEAR_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
	ELSE
		SET_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
	ENDIF
	
	IF NETWORK_PLAYER_IS_CHEATER()
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_CHEATER)
	ENDIF
	
	IF SCRIPT_IS_CLOUD_AVAILABLE()
		IF NOT CAN_I_PUBLISH_WITH_CURRENT_PRIVILEGES()
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
			PRINTLN("[ALERTBITS] ciMENU_ALERT_BLOCKED set")
		ENDIF
	ENDIF
	
	IF (sCurrentVarsStruct.creationStats.iTimesTestedLoc = 0 AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciCanBePublished))
	AND g_FMMC_STRUCT.bMissionIsPublished = FALSE
	AND (NOT IS_PUBLIC_ARENA_CREATOR() OR IS_KING_OF_THE_HILL())
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_UNTESTED)
	ENDIF
	IF sCurrentVarsStruct.creationStats.bMadeAChange = TRUE 
	AND (NOT IS_PUBLIC_ARENA_CREATOR() OR IS_KING_OF_THE_HILL())
		PRINTLN("CHECK_FOR_END_CONDITIONS_MET bMadeAChange = true")
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_UNTESTED)
	ENDIF
		
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF FMMC_IS_ROCKSTAR_DEV()
			CLEAR_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
			CLEAR_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_UNTESTED)
			SET_BIT(g_FMMC_STRUCT.biTeamTestComplete, TEAM_1_TEST_COMPLETE+iTeam)
		ENDIF
	ENDFOR
	
	IF (HAS_DM_CREATOR_TUTORIAL_COMPLETED(tutDMCreator) OR IS_KING_OF_THE_HILL())
	AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
		IF g_FMMC_STRUCT.bMissionIsPublished = FALSE
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
			OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
			//OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
			//OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_MANY_START_POINTS)
			//OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_NO_HILLS_PLACED)
				CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
			ELSE
				SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
			ENDIF
		ELSE
			CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
		ENDIF
		
//		IF g_FMMC_STRUCT.bMissionIsPublished = TRUE 
//		AND sFMMCendStage.bMajorEditOnLoadedMission = FALSE
//			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CHANGE)
//		ENDIF
			
		IF NOT IS_BIT_SET_ALERT(sFMMCmenu)
			SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciCanBePublished)
			SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
		ELSE
			CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciCanBePublished)
			CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
		ENDIF
	ELSE
		CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
		CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Turns Off all teh menu bits, except for the specified ones.
/// PARAMS:
///    activeMenuOption1 - Item to turn on
///    activeMenuOption2 - Item to turn on 
///    activeMenuOption3 - Item to turn on 
PROC TURN_OFF_MENU_OPTIONS_DURING_TUTORIAL(INT activeMenuOption1, INT activeMenuOption2 = -1, INT activeMenuOption3 = -1)

	SET_ALL_DM_MENU_ITEMS_OFF(sFMMCmenu)	
	SET_LONG_BIT(sFMMCMenu.iBitActive, activeMenuOption1)
	IF activeMenuOption2 != -1
		SET_LONG_BIT(sFMMCMenu.iBitActive, activeMenuOption2)
	ENDIF
	IF activeMenuOption3 != -1
		SET_LONG_BIT(sFMMCMenu.iBitActive, activeMenuOption3)
	ENDIF

ENDPROC

/// PURPOSE:
///    Turns on all the menu bits
PROC SET_ALL_MENU_ITEMS_ACTIVE()
	SET_LONG_BIT(sFMMCMenu.iBitActive, START_MENU_ITEM_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, SPAWN_MENU_ITEM_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, COV_MENU_ITEM_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, TEAM_SPAWN_MENU_ITEM_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, DON_KOTH_HILLS_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, DON_KOTH_BOUNDS_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, PANCAM_MENU_ITEM_DM)	
	SET_LONG_BIT(sFMMCMenu.iBitActive, WEP_MENU_ITEM_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, ZNE_MENU_ITEM_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, WORLDPROPS_MENU_ITEM_DM)
	if g_FMMC_STRUCT.iVehicleDeathmatch = 0
		SET_LONG_BIT(sFMMCMenu.iBitActive, VEH_MENU_ITEM_DM)
	ENDIF
	SET_LONG_BIT(sFMMCMenu.iBitActive, PROP_MENU_ITEM_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, ARENAPROP_MENU_ITEM_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, DON_MENU_RAND_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, MAP_MENU_ITEM_DM)
	SET_LONG_BIT(sFMMCMenu.iBitActive, RES_MENU_ITEM_DM)
	
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_CREATOR)
	IF g_FMMC_STRUCT.bMissionIsPublished = FALSE
	AND SCRIPT_IS_CLOUD_AVAILABLE()
		SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
	ENDIF
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_RADIO)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_OPTIONS)
	SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_EXIT)
		
ENDPROC

//Set up the menu
PROC MENU_INITIALISATION()
	sFMMCmenu.sSubTypeName[CREATION_TYPE_WEAPONS] 					= GET_CREATOR_NAME_FOR_PICKUP_TYPE(GET_PICKUP_TYPE_FROM_WEAPON_TYPE(sWepStruct.wtGunType[0]))
	sFMMCmenu.sSubTypeName[CREATION_TYPE_VEHICLE_COLOUR] 			= GET_NAME_VEHICLE_COLOUR(ciNUMBER_OF_VEHICLE_COLOURS)
	sFMMCmenu.sSubTypeName[CREATION_TYPE_PROPS]             		= GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType))
	sFMMCmenu.sSubTypeName[CREATION_TYPE_DYNOPROPS]         		= GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType))
	sFMMCMenu.sSubTypeName[CREATION_TYPE_OBJECTS] 					= GET_NAME_CAPTURE_OBJECT_MODEL(sObjStruct, 0, OBJECT_LIBRARY_CAPTURE)
	sFMMCmenu.sSubTypeName[CREATION_TYPE_ENEMY_WEAPON] 				= GET_ACTOR_WEAPON_NAME_FROM_SELECTION(0)
	sFMMCmenu.sSubTypeName[CREATION_TYPE_RAND_SPAWN] 				= GET_NAME_RAND_OPTION(2)
	sFMMCmenu.sSubTypeName[CREATION_TYPE_RAND_VEHICLES]  			= GET_NAME_RAND_OPTION(2)
	sFMMCmenu.sSubTypeName[CREATION_TYPE_RAND_WEAPONS]  			= GET_NAME_RAND_OPTION(2)
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS]	= 2
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_SPAWN] 	= 2
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_VEHICLES] 	= 2
	
	INT i
	FOR i = 0 TO OPTION_DM_MAX - 1
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, i)
	ENDFOR
	
	if NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		SET_ALL_MENU_ITEMS_ACTIVE()
	ELSE
		SET_DM_STARTING_MENU_ITEMS_ACTIVE(sFMMCmenu)
	ENDIF
	
	CHECK_FOR_END_CONDITIONS_MET()
	
	REFRESH_MENU(sFMMCMenu)
		
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////              Script Clean UP	                //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC CLEANUP_IPLS_AND_INTERIORS()
	ARENA_CLEANUP(TRUE, DEFAULT, TRUE, TRUE)
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		HEIST_ISLAND_LOADING__UNLOAD_ISLAND_IPLS()
		UNLOAD_MISSION_ISLAND_CUSTOMISATION()
	ENDIF
ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP(BOOL bSetWorldActive)

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Main_Menu")) = 0
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SF_Movie_Gallery_Shutter_Index)	

	IF DOES_CAM_EXIST(sCamData.cam)
		DESTROY_CAM(sCamData.cam)
	ENDIF
	
	IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
		REMOVE_BLIP(sStartEndBlips.biStart)
	ENDIF
	
	IF sStartEndBlips.ciStartType != NULL
		DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
		sStartEndBlips.ciStartType = NULL
	ENDIF
	
	IF DOES_BLIP_EXIST(bCameraPanBlip)
		REMOVE_BLIP(bCameraPanBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bPhotoBlip)
		REMOVE_BLIP(bPhotoBlip)
	ENDIF
	
	
	INT i, i2
	
	FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
		FOR i2 = 0 TO FMMC_MAX_TEAMSPAWNPOINTS - 1
			IF DOES_BLIP_EXIST(sTeamSpawnStruct[i].biPedBlip[i2])
				REMOVE_BLIP(sTeamSpawnStruct[i].biPedBlip[i2])
			ENDIF			
		ENDFOR
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	FOR i = 0 TO FMMC_MAX_NUM_COVER - 1
		IF DOES_BLIP_EXIST(sCoverStruct.biCover[i])
			REMOVE_BLIP(sCoverStruct.biCover[i])
		ENDIF
	ENDFOR
	#ENDIF
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	g_bFMMC_TutorialSelectedFromMpSkyMenu = FALSE
	COMMON_FMMC_SCRIPT_CLEAN_UP(sPedStruct,	sVehStruct,	sWepStruct,	sObjStruct,sPropStruct, sDynoPropStruct, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, bSetWorldActive)
	iDoorSetupStage = 0
		
	CLEANUP_MENU_ASSETS()
	DO_BLIMP_SIGNS(sBlimpSign, TRUE)
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		IF SCRIPT_IS_CLOUD_AVAILABLE()
			PRINTLN("SCRIPT_IS_CLOUD_AVAILABLE()")
		    SET_FAKE_MULTIPLAYER_MODE(FALSE)
		ELSE
			PRINTLN("NOT SCRIPT_IS_CLOUD_AVAILABLE()")
			IF NOT IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
				NETWORK_SESSION_LEAVE_SINGLE_PLAYER() 
			ENDIF
			g_Private_IsMultiplayerCreatorRunning = FALSE
			g_Private_MultiplayerCreatorNeedsToEnd = TRUE
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	#ENDIF
	
	REPEAT FMMC_MAX_SPAWNPOINTS i
		REMOVE_RELATIONSHIP_GROUP(lrgFM_Team[i])
	ENDREPEAT
	
	SET_CREATOR_AUDIO(FALSE, FALSE)
	
	CLEANUP_ALL_INTERIORS(g_ArenaInterior)
	
	IF IS_KING_OF_THE_HILL()
		CLEANUP_KING_OF_THE_HILL(sFMMCMenu.sKOTH_LocalInfo)
	ENDIF
	
	PROCESS_VEHICLE_FOR_RESPAWN_VEH(sFMMCdata.viPlacementSpawnVeh, DUMMY_MODEL_FOR_SCRIPT, <<0,0,0>>, 0.0)
	PROCESS_VEHICLE_FOR_RESPAWN_VEH(sFMMCdata.viPostPlacementSpawnVehCheck, DUMMY_MODEL_FOR_SCRIPT, <<0,0,0>>, 0.0)
	
	//Reset mission type
	g_FMMC_STRUCT.iMissionType = 0
	
	RESET_SCRIPT_GFX_ALIGN()
	
	SET_IN_ARENA_MODE(FALSE)
	SET_VEHICLE_COMBAT_MODE(FALSE)
	
	CLEANUP_IPLS_AND_INTERIORS()
	
	SET_PLAYER_TEAM(PLAYER_ID(), -1)
	
	PRINTLN("TERMINATE THE DM CREATOR. WORLD ACTIVE = ", bSetWorldActive)
	
	g_bFMMC_EnteredLegacyDeathmatchCreator = FALSE
	
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

/// PURPOSE:
///    Cleanup the the tutorial stuff
PROC CLEANUP_TUTORIAL()

	CLEAR_HELP()
	
	SET_DM_CREATOR_TUTORIAL_COMPLETE(tutDMCreator)
	RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)

	SET_ALL_MENU_ITEMS_ACTIVE()
	
	sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
	sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT		
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	sCurrentVarsStruct.bSwitching = FALSE
	sCurrentVarsStruct.stiScreenCentreLOS = NULL
	g_FMMC_STRUCT.iNumberOfCheckPoints = 0
	MENU_INITIALISATION()
	sCurrentVarsStruct.bResetUpHelp = TRUE
	
	sCurrentVarsStruct.bDisableEditingDuringTutorial = FALSE
	
	SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
	
	bShowMenu = TRUE
	CLEAR_HELP()
	
	RUN_RADAR_MAP()
ENDPROC

//// PURPOSE:
 ///    Removes any of the existing corona placement entities
PROC REMOVE_TEMP_CREATION_ENTITIES()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints  < FMMC_MAX_SPAWNPOINTS
		g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints].vPos 	= <<0.0,0.0,0.0>>
	ENDIF
	IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
		DELETE_PED(sPedStruct.piTempPed)
	ENDIF	
	IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
		DELETE_VEHICLE(sVehStruct.viCoronaVeh)
	ENDIF
	IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
		DELETE_OBJECT(sWepStruct.viCoronaWep)
	ENDIF
	IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
		DELETE_OBJECT(sObjStruct.viCoronaObj)
	ENDIF
	IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
		DELETE_OBJECT(sPropStruct.viCoronaObj)
	ENDIF
	DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
ENDPROC

FUNC BOOL SHOULD_USE_VEHICLES(INT iTeam = -1, INT iModSet = -1)

	IF iTeam > -1
		IF IS_NON_ARENA_KING_OF_THE_HILL()
			IF NOT KOTH_SHOULD_I_SPAWN_IN_VEHICLE(iTeam)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.iVehicleDeathmatch > 0
	OR IS_ARENA_CREATOR()
	OR KOTH_USES_VEHICLES()
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DM_SpawnInCoronaVehicle)
		RETURN TRUE
	ENDIF
	
	IF iModSet != -1
	AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSet].iModifierSetBS, ciDM_ModifierBS_SpawnInVehicle)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PUT_PLAYER_IN_VEH()
	REQUEST_MODEL(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel))
	
	IF NOT HAS_MODEL_LOADED(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel))
		PRINTLN("[TMS][ARENA] PUT_PLAYER_IN_VEH - Loading vehicle model: ", GET_MODEL_NAME_FOR_DEBUG(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel)))
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(testPeds[0].viVehicleDM)
		
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		
		IF DOES_ENTITY_EXIST(testPeds[0].viVehicleDM)
			DELETE_VEHICLE(testPeds[0].viVehicleDM)
			PRINTLN("[DMTEST] Deleting testPeds[0].viVehicleDM")
		ENDIF
		
		testPeds[0].viVehicleDM = CREATE_VEHICLE(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel), GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()))
		PRINTLN("[DMTEST] Creating testPeds[", 0, "].viVehicleDM for player")
		
		if NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), testPeds[0].viVehicleDM)
		ENDIF
		SET_VEHICLE_DOORS_LOCKED(testPeds[0].viVehicleDM, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
		SET_ENTITY_INVINCIBLE(testPeds[0].viVehicleDM, TRUE)

		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel))
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes any of the existing corona placement entities, if we arent in that creation type.
/// PARAMS:
///    iCurrentState - The Creation Type. eg CREATION_TYPE_VEHICLES
PROC REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(INT iCurrentState)

	MODEL_NAMES oldModel
	
	IF iCurrentState != CREATION_TYPE_VEHICLES
		IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
			oldModel = GET_ENTITY_MODEL(sVehStruct.viCoronaVeh)
			DELETE_VEHICLE(sVehStruct.viCoronaVeh)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState !=CREATION_TYPE_WEAPONS
		IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
			oldModel = GET_ENTITY_MODEL(sWepStruct.viCoronaWep)
			DELETE_OBJECT(sWepStruct.viCoronaWep)					
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_PROPS
		IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sPropStruct.viCoronaObj)
			DELETE_OBJECT(sPropStruct.viCoronaObj)			
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
		DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
	ENDIF
	IF iCurrentState != CREATION_TYPE_OBJECTS
		IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sObjStruct.viCoronaObj)
			DELETE_OBJECT(sObjStruct.viCoronaObj)					
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_DYNOPROPS
		IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sDynoPropStruct.viCoronaObj)
			DELETE_OBJECT(sDynoPropStruct.viCoronaObj)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			
			// [ChildDyno]
			IF GET_MODEL_NAME_OF_CHILD_PROP_FOR_DYNO(oldModel) != DUMMY_MODEL_FOR_SCRIPT
				IF DOES_ENTITY_EXIST(sDynoPropStruct.oiChildTemp)
					DELETE_OBJECT(sDynoPropStruct.oiChildTemp)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DISPLAY_LIVERY_NAME()
	IF IS_ENTITY_ALIVE(sVehStruct.viCoronaVeh)	
		IF GET_VEHICLE_LIVERY_COUNT(sVehStruct.viCoronaVeh) > -1
		OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, MULE) AND NOT IS_BIT_SET(sFMMCmenu.iVehBitSet, ciFMMC_VEHICLE_EXTRA1))
			IF IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, MULE)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, SANCHEZ)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, POLMAV)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, SHAMAL)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, RUMPO)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, STUNT)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, JET)
			//OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, WINDSOR)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////        	    Stat Tracking       	        //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
// PURPOSE: Tracking time in creator mode
PROC TRACK_TIME_IN_CREATOR_MODE()

	IF NETWORK_CLAN_SERVICE_IS_VALID()  
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_MISSION_CREATO, TIMERA())
		SETTIMERA(0)
	ENDIF
	
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////              CAMERA STUFF	                //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///  Deals with increasing / decreasing the selection number. 
///  Number is used to set a vector vSwitchVec - This is used inside DEAL_WITH_SKY_CAM_SWITCH
///  Also sets BOOL bSwitchingCam = TRUE - This starts the switch camera action inside DEAL_WITH_SKY_CAM_SWITCH
PROC DEAL_WITH_CAMERA_SWITCH_SELECTION(BOOL bIncrease, INT iMaximumValue, INT iTeam = -1)
	
	IF NOT FMMC_IS_ROCKSTAR_DEV()
		IF SHOULD_CYCLE_ITEMS_BE_BLOCKED(sFMMCMenu)
			EXIT
		ENDIF
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		START_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)
	ELIF HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		REINIT_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)
	ENDIF
	
	IF iMaximumValue > 0
		IF bSwitchingCam = FALSE
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			
			IF bIncrease
				sFMMCmenu.iSwitchCam ++
				IF sFMMCmenu.iSwitchCam < 0
					sFMMCmenu.iSwitchCam = 1
				ENDIF
			ELSE
				sFMMCmenu.iSwitchCam --
				IF sFMMCmenu.iSwitchCam < 0
					sFMMCmenu.iSwitchCam = iMaximumValue - 1
				ENDIF
			ENDIF
			IF sFMMCmenu.iSwitchCam > iMaximumValue - 1
				sFMMCmenu.iSwitchCam = 0
			ENDIF
			
			PLAY_EDIT_MENU_ITEM_SOUND()
			
			IF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCmenu.iSwitchCam].fHead
			ELIF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sFMMCmenu.iSwitchCam].vRot.z
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedProp[sFMMCmenu.iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedProp[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sFMMCmenu.iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
				IF iTeam > -1
					vSwitchVec = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
					fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][sFMMCmenu.iSwitchCam].fHead
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS
				vSwitchVec = g_FMMC_STRUCT.sKotHData.sHillData[sFMMCmenu.iSwitchCam].vHill_Center + <<0.0, 0.0, 8.1>>
				fSwitchHeading = 0.0
				
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_CAPTURE_OBJECT_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedObject[sFMMCmenu.iSwitchCam].fHead
				
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ENDIF
			
			bSwitchingCam = TRUE 
			
		ENDIF
		
		IF CONTENT_IS_USING_ARENA()
			vSwitchVec.z = 188.0
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL PROCESS_SPAWN_VEH_SHAPETEST()

	INT iBSArrayIndex = GET_LONG_BITSET_INDEX(iSpawnVehCheckIndex)
	INT iBSArrayBit = GET_LONG_BITSET_BIT(iSpawnVehCheckIndex)
	
	FLOAT fGroundHeightThreshold = 0.65
	
	IF sFMMCdata.stVehShapetest = NULL
		sFMMCdata.stVehShapetest = START_SHAPE_TEST_BOUNDING_BOX(sFMMCdata.viPostPlacementSpawnVehCheck, DEFAULT, DEFAULT)
		
	ELIF sFMMCdata.stVehShapetest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
		
	ELSE
		VECTOR vPos, vNormal
		ENTITY_INDEX eiHitEntity
		INT iHitSomething
		
		SHAPETEST_STATUS eStatus = GET_SHAPE_TEST_RESULT(sFMMCdata.stVehShapetest, iHitSomething, vPos, vNormal, eiHitEntity)
		
		#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
			IF iHitSomething > 0
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(sFMMCdata.viPostPlacementSpawnVehCheck), vPos, 255, 0, 0)
				DRAW_DEBUG_SPHERE(vPos, 0.75, 255, 0, 0, 100)
				
				ASSERTLN("PROCESS_SPAWN_VEH_SHAPETEST || Hit ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(eiHitEntity)))
			ELSE
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(sFMMCdata.viPostPlacementSpawnVehCheck), 1.5, 0, 255, 0, 100)
			ENDIF
		ENDIF
		#ENDIF
		
		SWITCH eStatus
			CASE SHAPETEST_STATUS_NONEXISTENT
				sFMMCdata.stVehShapetest = NULL
			BREAK
			CASE SHAPETEST_STATUS_RESULTS_NOTREADY
				
			BREAK
			CASE SHAPETEST_STATUS_RESULTS_READY
			
				IF iHitSomething > 0
				
					FLOAT fGroundZ
					VECTOR vGroundPos
					vGroundPos = GET_ENTITY_COORDS(sFMMCdata.viPostPlacementSpawnVehCheck)
					
					IF GET_GROUND_Z_FOR_3D_COORD(vGroundPos, fGroundZ)
					
						vGroundPos.z = fGroundZ
						FLOAT fHeightOfCollision
						fHeightOfCollision = (vPos.z - fGroundZ)
						
						IF fHeightOfCollision <= fGroundHeightThreshold
							PRINTLN("PROCESS_SPAWN_VEH_SHAPETEST || Collision point not high enough off the ground")
							iHitSomething = 0
						ENDIF
					ENDIF					
				ENDIF
				
				IF iSpawnVehTeamIndex = -1
					IF iHitSomething > 0
						IF NOT IS_BIT_SET(sFMMCdata.iVehSpawnInvalidBS[iBSArrayIndex], iBSArrayBit)
							SET_BIT(sFMMCdata.iVehSpawnInvalidBS[iBSArrayIndex], iBSArrayBit)
							PRINTLN("PROCESS_SPAWN_VEH_SHAPETEST || Setting ", iBSArrayBit, " in sFMMCdata.iVehSpawnInvalidBS[", iBSArrayIndex, "]")
						ENDIF
					ELSE
						IF IS_BIT_SET(sFMMCdata.iVehSpawnInvalidBS[iBSArrayIndex], iBSArrayBit)
							CLEAR_BIT(sFMMCdata.iVehSpawnInvalidBS[iBSArrayIndex], iBSArrayBit)
							PRINTLN("PROCESS_SPAWN_VEH_SHAPETEST || Clearing ", iBSArrayBit, " in sFMMCdata.iVehSpawnInvalidBS[", iBSArrayIndex, "]")
						ENDIF
					ENDIF
				ELSE
					IF iHitSomething > 0
						IF NOT IS_BIT_SET(sFMMCdata.iVehTeamSpawnInvalidBS[iSpawnVehTeamIndex][iBSArrayIndex], iBSArrayBit)
							SET_BIT(sFMMCdata.iVehTeamSpawnInvalidBS[iSpawnVehTeamIndex][iBSArrayIndex], iBSArrayBit)
							PRINTLN("PROCESS_SPAWN_VEH_SHAPETEST || Setting ", iBSArrayBit, " in sFMMCdata.iVehTeamSpawnInvalidBS[", iSpawnVehTeamIndex, "][", iBSArrayIndex, "]")
						ENDIF
					ELSE
						IF IS_BIT_SET(sFMMCdata.iVehTeamSpawnInvalidBS[iSpawnVehTeamIndex][iBSArrayIndex], iBSArrayBit)
							CLEAR_BIT(sFMMCdata.iVehTeamSpawnInvalidBS[iSpawnVehTeamIndex][iBSArrayIndex], iBSArrayBit)
							PRINTLN("PROCESS_SPAWN_VEH_SHAPETEST || Clearing ", iBSArrayBit, " in sFMMCdata.iVehTeamSpawnInvalidBS[", iSpawnVehTeamIndex, "][", iBSArrayIndex, "]")
						ENDIF
					ENDIF
				ENDIF
				
				RETURN TRUE
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_SPAWN_POINT_VEHICLE_DISPLAY()
	
	//INT i
	VECTOR vSize = <<0,0,0>>
	MODEL_NAMES mnModel = GET_BIGGEST_RESPAWN_VEHICLE(vSize)
	
	IF sFMMCmenu.iEntityCreation = CREATION_TYPE_TEAM_SPAWN_LOCATION
	AND sFMMCMenu.iSelectedTeam > -1
		mnModel = g_FMMC_STRUCT.mnVehicleModel[sFMMCMenu.iSelectedTeam]
		
		IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_STANDARD
			mnModel = g_FMMC_STRUCT.mnVehicleModel[0]
		ENDIF
	ENDIF
	
	IF sFMMCmenu.iEntityCreation = CREATION_TYPE_SPAWN_LOCATION
	OR sFMMCmenu.iEntityCreation = CREATION_TYPE_TEAM_SPAWN_LOCATION
		PROCESS_VEHICLE_FOR_RESPAWN_VEH(sFMMCdata.viPlacementSpawnVeh, mnModel, sCurrentVarsStruct.vCoronaPos, sCurrentVarsStruct.fCreationHeading)
	ELSE
		PROCESS_VEHICLE_FOR_RESPAWN_VEH(sFMMCdata.viPlacementSpawnVeh, DUMMY_MODEL_FOR_SCRIPT, <<0,0,0>>, 0.0)
	ENDIF
	
	//								//
	// SPAWN POINT COLLISION CHECKS //
	//								//
	
	IF sFMMCmenu.iEntityCreation != CREATION_TYPE_SPAWN_LOCATION
	AND sFMMCmenu.iEntityCreation != CREATION_TYPE_TEAM_SPAWN_LOCATION
		VECTOR vPos
		FLOAT fHeading
		
		IF iSpawnVehTeamIndex = -1
			
			IF iSpawnVehCheckIndex >= g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints
				iSpawnVehCheckIndex = 0
				iSpawnVehTeamIndex = 0
				EXIT
			ENDIF
				
			vPos = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iSpawnVehCheckIndex].vPos
			fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iSpawnVehCheckIndex].fHead
		ELSE
			IF iSpawnVehCheckIndex >= g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iSpawnVehTeamIndex]
				iSpawnVehCheckIndex = 0
				iSpawnVehTeamIndex++
				
				IF iSpawnVehTeamIndex >= FMMC_MAX_TEAMS
					iSpawnVehTeamIndex = -1
					EXIT
				ENDIF
			ENDIF
			
			vPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iSpawnVehTeamIndex][iSpawnVehCheckIndex].vPos
			fHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iSpawnVehTeamIndex][iSpawnVehCheckIndex].fHead
		ENDIF
		
		SWITCH iSpawnVehCheckState
			CASE ciSPVS_Spawning
				IF PROCESS_VEHICLE_FOR_RESPAWN_VEH(sFMMCdata.viPostPlacementSpawnVehCheck, mnModel, vPos, fHeading, DEFAULT, FALSE)

					IF HAS_COLLISION_LOADED_AROUND_ENTITY(sFMMCdata.viPostPlacementSpawnVehCheck)
						iSpawnVehCheckState = ciSPVS_Checking
						PROCESS_SPAWN_POINT_VEHICLE_DISPLAY()
					ELSE
						SET_ENTITY_LOAD_COLLISION_FLAG(sFMMCdata.viPostPlacementSpawnVehCheck, TRUE)
					ENDIF
				ELSE
					IF mnModel = DUMMY_MODEL_FOR_SCRIPT
						sFMMCdata.iVehSpawnInvalidBS[GET_LONG_BITSET_INDEX(iSpawnVehCheckIndex)] = 0
						
						IF iSpawnVehTeamIndex > -1
							sFMMCdata.iVehTeamSpawnInvalidBS[iSpawnVehTeamIndex][GET_LONG_BITSET_INDEX(iSpawnVehCheckIndex)] = 0
						ENDIF
						
						iSpawnVehCheckIndex++
					ENDIF
				ENDIF
			BREAK
			CASE ciSPVS_Checking
			
				INT iTemp
				FOR iTemp = 0 TO IMAX_INVISIBLE_OBJECTS - 1
					IF IS_ENTITY_ALIVE(sInvisibleObjects.oiInvObj[iTemp])
						SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sInvisibleObjects.oiInvObj[iTemp], FALSE)
					ENDIF
				ENDFOR
					
				IF IS_ENTITY_ALIVE(sFMMCdata.viPostPlacementSpawnVehCheck)
					IF PROCESS_SPAWN_VEH_SHAPETEST()
						iSpawnVehCheckState = ciSPVS_Done
						PROCESS_SPAWN_POINT_VEHICLE_DISPLAY()
					ENDIF
				ELSE
					iSpawnVehCheckState = ciSPVS_Done
				ENDIF
				
				FOR iTemp = 0 TO IMAX_INVISIBLE_OBJECTS - 1
					IF IS_ENTITY_ALIVE(sInvisibleObjects.oiInvObj[iTemp])
						SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sInvisibleObjects.oiInvObj[iTemp], TRUE)
					ENDIF
				ENDFOR
			BREAK
			CASE ciSPVS_Done
				IF IS_ENTITY_ALIVE(sFMMCdata.viPostPlacementSpawnVehCheck)
					SET_ENTITY_LOAD_COLLISION_FLAG(sFMMCdata.viPostPlacementSpawnVehCheck, FALSE)
					DELETE_VEHICLE(sFMMCdata.viPostPlacementSpawnVehCheck)
				ENDIF
				
				iSpawnVehCheckState = ciSPVS_Spawning
				iSpawnVehCheckIndex++
				PROCESS_SPAWN_POINT_VEHICLE_DISPLAY()
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

// KING OF THE HILL //
PROC INIT_KING_OF_THE_HILL_CREATOR()
	UNUSED_PARAMETER(sFMMCmenu)
	
	IF g_bFMMC_EnterKotHCreator
		//Brand new King of the Hill map
		PRINTLN("[KOTH] INIT_KING_OF_THE_HILL_CREATOR")
		
		g_bFMMC_EnterKotHCreator = FALSE
		SET_BIT(g_FMMC_STRUCT.sKotHData.iKotHBS, ciKotHBS_KingOfTheHillModeEnabled)
		
		g_FMMC_STRUCT.sKotHData.iNumberOfHills = 0
		g_FMMC_STRUCT.iTargetScore = KOTH_TARGET_SCORE_MINIMUM
	ELSE
		
	ENDIF
	
	FORCE_RENDER_IN_GAME_UI(TRUE)
	
	SET_FORCED_KOTH_SETTINGS(TRUE)
	
	IF g_FMMC_STRUCT.iTargetScore < KOTH_TARGET_SCORE_MINIMUM
		g_FMMC_STRUCT.iTargetScore = KOTH_TARGET_SCORE_MINIMUM
	ENDIF
ENDPROC

PROC DELETE_HILL(INT iHill)

	IF g_FMMC_STRUCT.sKotHData.iNumberOfHills = 0
		EXIT
	ENDIF
	
	INT i
	
	FOR i = iHill TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 2
		g_FMMC_STRUCT.sKotHData.sHillData[i] = g_FMMC_STRUCT.sKotHData.sHillData[i + 1]
		REMOVE_BLIP_FOR_HILL(sFMMCmenu.sKOTH_LocalInfo.sHills[i])
	ENDFOR
	
	REMOVE_BLIP_FOR_HILL(sFMMCmenu.sKOTH_LocalInfo.sHills[g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1])
	
	INT iBoundsIndex = 0
	REFRESH_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex])
	
	KotH_HILL_DATA_STRUCT sTemp
	g_FMMC_STRUCT.sKotHData.sHillData[g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1] = sTemp
	g_FMMC_STRUCT.sKotHData.iNumberOfHills--
	
	sCurrentVarsStruct.creationStats.bMadeAChange = TRUE
	SET_RETEST_BOOLS(sFMMCendStage)
	
	//Remove the blip of Hill 0 so we can refresh it and give it the correct sprite
	IF g_FMMC_STRUCT.sKotHData.iNumberOfHills = 1
		IF DOES_BLIP_EXIST(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biCenterBlip)
			REMOVE_BLIP(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biCenterBlip)
		ENDIF
		
		IF DOES_BLIP_EXIST(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biAreaBlip)
			REMOVE_BLIP(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biAreaBlip)
		ENDIF
	ENDIF
	
	REFRESH_MENU(sFMMCmenu)
	
	PRINTLN("[KOTH] Deleting Hill ", iHill)
	DEBUG_PRINTCALLSTACK()
	
ENDPROC

FUNC BOOL SET_ENTITY_FOR_KOTH_HILL(KotH_HILL_DATA_STRUCT& sHill, KOTH_HILL_RUNTIME_LOCAL_STRUCT& sHill_Local)
	
	IF sHill.iHillEntity < 0
	OR sHill.iHillType = ciKOTH_HILL_TYPE__CIRCLE
	OR sHill.iHillType = ciKOTH_HILL_TYPE__RECT
		sHill_Local.eiMyEntity = NULL
		RETURN TRUE
	ENDIF
	
	SWITCH sHill.iHillType
		CASE ciKOTH_HILL_TYPE__VEHICLE
			sHill_Local.eiMyEntity = sVehStruct.veVehcile[sHill.iHillEntity]
		BREAK
		CASE ciKOTH_HILL_TYPE__OBJECT
			sHill_Local.eiMyEntity = sObjStruct.oiObject[sHill.iHillEntity]
		BREAK
	ENDSWITCH
	
	RETURN DOES_ENTITY_EXIST(sHill_Local.eiMyEntity)
ENDFUNC

FUNC BOOL PROCESS_HILL_ENTITY_DELETION(INT i)

	INT iIndexDeleted = -1
	
	SWITCH g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType
		CASE ciKOTH_HILL_TYPE__VEHICLE
			iIndexDeleted = sVehStruct.iKOTH_VehDeleted
			
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles < 1
				DELETE_HILL(i)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciKOTH_HILL_TYPE__OBJECT
			iIndexDeleted = sObjStruct.iKOTH_ObjDeleted
			
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfObjects < 1
				DELETE_HILL(i)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF iIndexDeleted > -1
		IF iIndexDeleted = g_FMMC_STRUCT.sKotHData.sHillData[i].iHillEntity
			PRINTLN("[KOTH] Removing Hill ", i, " because its hold entity is ", g_FMMC_STRUCT.sKotHData.sHillData[i].iHillEntity)
			DELETE_HILL(i)
			RETURN TRUE
		ENDIF
		
		IF iIndexDeleted <= g_FMMC_STRUCT.sKotHData.sHillData[i].iHillEntity
		AND iIndexDeleted > -1
			g_FMMC_STRUCT.sKotHData.sHillData[i].iHillEntity--
			PRINTLN("[KOTH] Moving Hill ", i, " down to ", g_FMMC_STRUCT.sKotHData.sHillData[i].iHillEntity)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_HIGHLIGHTED_HILL()
	IF sFMMCMenu.iCurrentHighlightedKOTHHill = -1
	OR sFMMCMenu.iSelectedEntity > -1
	OR IS_HELP_MESSAGE_BEING_DISPLAYED() // For the "press ~a~ to activate menu" helptext in on-foot mode
		CLEAR_BIT(iLocalBitSet, biEntityInCorona)
		sCurrentVarsStruct.iHoverEntityType = -1
		EXIT
	ENDIF
	
	IF sFMMCMenu.fKOTH_SelectionCooldown > 0.0
		sFMMCMenu.fKOTH_SelectionCooldown -= GET_FRAME_TIME()
		EXIT
	ENDIF
	
	SWITCH sFMMCMenu.sTempHillStruct.iHillType
		CASE ciKOTH_HILL_TYPE__CIRCLE
			sCurrentVarsStruct.sSelectedName = "KOTH_AT_CIR"
			SET_BIT(iLocalBitSet, biEntityInCorona)
		BREAK
		
		CASE ciKOTH_HILL_TYPE__RECT
			sCurrentVarsStruct.sSelectedName = "KOTH_AT_REC"
			SET_BIT(iLocalBitSet, biEntityInCorona)
		BREAK
		
		CASE ciKOTH_HILL_TYPE__VEHICLE
			sCurrentVarsStruct.sSelectedName = "KOTH_AT_VEH"
		BREAK
		
		CASE ciKOTH_HILL_TYPE__OBJECT
			sCurrentVarsStruct.sSelectedName = "KOTH_AT_OBJ"
		BREAK
	ENDSWITCH
	PRINT_SELECTED_ENTITY_NAME_AS_FLOATING_HELP(sCurrentVarsStruct, sCurrentVarsStruct.sSelectedName)
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	AND IS_HILL_TYPE_A_SHAPE(sFMMCMenu.sTempHillStruct.iHillType)
		PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_HILL, sFMMCMenu.iCurrentHighlightedKOTHHill)
		sFMMCMenu.sTempHillStruct = g_FMMC_STRUCT.sKotHData.sHillData[sFMMCMenu.iCurrentHighlightedKOTHHill]
		PRINTLN("[KOTH] Picking up Hill ", sFMMCMenu.iSelectedEntity)
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_DELETE_BUTTON())
	AND IS_HILL_TYPE_A_SHAPE(g_FMMC_STRUCT.sKotHData.sHillData[sFMMCMenu.iCurrentHighlightedKOTHHill].iHillType)
		PLAY_SOUND_FRONTEND(-1, "DELETE", GET_CREATOR_SPECIFIC_SOUND_SET())
		
		INT iEntity = g_FMMC_STRUCT.sKotHData.sHillData[sFMMCMenu.iCurrentHighlightedKOTHHill].iHillEntity
		IF iEntity > -1
			IF g_FMMC_STRUCT.sKotHData.sHillData[sFMMCMenu.iCurrentHighlightedKOTHHill].iHillType = ciKOTH_HILL_TYPE__VEHICLE
				REMOVE_MODEL_FROM_CREATOR_BUDGET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].mn)
				DELETE_LAST_VEHICLE_PLACED(sVehStruct, iEntity, CREATION_TYPE_VEHICLES)
			ELIF g_FMMC_STRUCT.sKotHData.sHillData[sFMMCMenu.iCurrentHighlightedKOTHHill].iHillType = ciKOTH_HILL_TYPE__OBJECT
				REMOVE_MODEL_FROM_CREATOR_BUDGET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].mn)
				DELETE_CREATOR_OBJECT(sObjStruct, sInvisibleObjects, sFMMCmenu, sCurrentVarsStruct, sFMMCendStage, iEntity)
			ENDIF
		ENDIF
		
		DELETE_HILL(sFMMCMenu.iCurrentHighlightedKOTHHill)
	ENDIF
ENDPROC

PROC MAINTAIN_PLACED_KING_OF_THE_HILL_HILLS()
	INT i
	
	sFMMCMenu.iCurrentHighlightedKOTHHill = -1
	sFMMCMenu.iCurrentEditedKOTHHill = -1
	
	BOOL bError[ciKotH_MAX_HILLS]
	BOOL bHideHill[ciKotH_MAX_HILLS]
	
	INT iHillJustDeleted = -1
	
	FOR i = 0 TO ciKotH_MAX_HILLS - 1
		
		IF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__CIRCLE
		AND DOING_HILL_CIRCLE_CREATION(sFMMCmenu)
		AND sFMMCmenu.iEntityCreation = CREATION_TYPE_HILL
		AND sFMMCMenu.iSelectedEntity = i
			bHideHill[i] = TRUE
			sFMMCMenu.iCurrentEditedKOTHHill = i
			PRINTLN("[KOTH] MAINTAIN_PLACED_KING_OF_THE_HILL_HILLS - Relooping Circle Hill ", i, " because it is being edited")
			RELOOP
		ELIF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__RECT
		AND DOING_HILL_RECT_CREATION(sFMMCmenu)
		AND sFMMCmenu.iEntityCreation = CREATION_TYPE_HILL
		AND sFMMCMenu.iSelectedEntity = i
			bHideHill[i] = TRUE
			sFMMCMenu.iCurrentEditedKOTHHill = i
			PRINTLN("[KOTH] MAINTAIN_PLACED_KING_OF_THE_HILL_HILLS - Relooping Rect Hill ", i, " because it is being edited")
			RELOOP
		ELIF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__VEHICLE
		AND DOING_HILL_VEHICLE_CREATION(sFMMCmenu)
		AND sFMMCmenu.iEntityCreation = CREATION_TYPE_VEHICLES
		AND sFMMCMenu.iSelectedEntity = g_FMMC_STRUCT.sKotHData.sHillData[i].iHillEntity
			bHideHill[i] = TRUE
			sFMMCMenu.iCurrentEditedKOTHHill = i
			PRINTLN("[KOTH] MAINTAIN_PLACED_KING_OF_THE_HILL_HILLS - Relooping Vehicle Hill ", i, " because its vehicle is being edited")
			RELOOP
		ELIF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__OBJECT
		AND DOING_HILL_OBJECT_CREATION(sFMMCmenu)
		AND sFMMCmenu.iEntityCreation = CREATION_TYPE_OBJECTS
		AND sFMMCMenu.iSelectedEntity = g_FMMC_STRUCT.sKotHData.sHillData[i].iHillEntity
			bHideHill[i] = TRUE
			sFMMCMenu.iCurrentEditedKOTHHill = i
			PRINTLN("[KOTH] MAINTAIN_PLACED_KING_OF_THE_HILL_HILLS - Relooping Object Hill ", i, " because its object is being edited")
			RELOOP
		ENDIF
		
		BOOL bLockedSelection = FALSE
		INT iBoundsIndex = 0
		IF NOT IS_POINT_IN_DEATHMATCH_BOUNDS(iBoundsIndex, g_FMMC_STRUCT.sKotHData.sHillData[i].vHill_Center, <<0,0,0>>, <<0,0,0>>)
			bError[i] = TRUE
			SET_BIT(iKOTH_HillErrorBS, i)
		ELSE
			CLEAR_BIT(iKOTH_HillErrorBS, i)
		ENDIF
		
		IF sFMMCmenu.sTempHillStruct.iHillEntity > -1
			//PRINTLN("[KOTH] bLockedSelection is TRUE due to sFMMCmenu.sTempHillStruct.iHillEntity being ", sFMMCmenu.sTempHillStruct.iHillEntity)
			//bLockedSelection = TRUE
		ENDIF
		
		IF sFMMCMenu.iSelectedEntity > -1
			PRINTLN("[KOTH] bLockedSelection is TRUE due to sFMMCMenu.iSelectedEntity being ", sFMMCMenu.iSelectedEntity)
			bLockedSelection = TRUE
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS
			IF IS_POINT_INSIDE_KOTH_HILL(sCurrentVarsStruct.vCoronaPos, i, sFMMCmenu.sKOTH_LocalInfo)
			AND NOT bLockedSelection
			
				IF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = sFMMCmenu.sTempHillStruct.iHillType
					IF IS_HILL_TYPE_A_SHAPE(g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType)
						sFMMCMenu.iCurrentHighlightedKOTHHill = i
					ELIF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__VEHICLE
						IF sCurrentVarsStruct.vCoronaHitEntity = sFMMCmenu.sKOTH_LocalInfo.sHills[i].eiMyEntity
							sFMMCMenu.iCurrentHighlightedKOTHHill = i
						ENDIF
					ELIF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__OBJECT
						IF IS_ENTITY_IN_CORONA(sFMMCmenu.sKOTH_LocalInfo.sHills[i].eiMyEntity, sCurrentVarsStruct, DEFAULT, i, CREATION_TYPE_OBJECTS)
							sFMMCMenu.iCurrentHighlightedKOTHHill = i
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_HILL_TYPE_A_SHAPE(g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType)
					bLockedSelection = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_HILL_TYPE_A_SHAPE(g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType)
			// If our entity doesn't exist, it's likely to be because editing it, so we grab whatever is in the corona and use that for now until our proper entity exists
			IF NOT DOES_ENTITY_EXIST(sFMMCmenu.sKOTH_LocalInfo.sHills[i].eiMyEntity)
				IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
					sFMMCmenu.sKOTH_LocalInfo.sHills[i].eiMyEntity = sVehStruct.viCoronaVeh
				ELIF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
					sFMMCmenu.sKOTH_LocalInfo.sHills[i].eiMyEntity = sObjStruct.viCoronaObj
				ELSE
					// If we still can't find anything, just hide this Hill
					bHideHill[i] = TRUE
					PRINTLN("MAINTAIN_PLACED_KING_OF_THE_HILL_HILLS - Hiding Hill ", i, " because the entity doesn't exist")
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(sFMMCmenu.sKOTH_LocalInfo.sHills[i].eiMyEntity)
				SET_KOTH_AREA_RADIUS_BASED_ON_ENTITY_SIZE(g_FMMC_STRUCT.sKotHData.sHillData[i], sFMMCmenu.sKOTH_LocalInfo.sHills[i])
			ENDIF
		ENDIF
		
		IF PROCESS_HILL_ENTITY_DELETION(i)
			iHillJustDeleted = i
			i--
			PRINTLN("[KOTH] Setting iHillJustDeleted to ", iHillJustDeleted)
		ENDIF
	ENDFOR
	
	IF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS
		IF iHillJustDeleted = -1
			PROCESS_HIGHLIGHTED_HILL()
		ENDIF
	ELSE
		
	ENDIF
	
	//Clear entity deletion info
	sVehStruct.iKOTH_VehDeleted = -1
	sObjStruct.iKOTH_ObjDeleted = -1
	
	// DRAWING
	IF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT.sKotHData.iNumberOfHills,	ciKotH_MAX_HILLS,	"FMMC_AB_KHA")
	ENDIF
	
	FOR i = 0 TO ciKotH_MAX_HILLS - 1
		IF NOT bHideHill[i]
			DRAW_KOTH_HILL(i, sFMMCmenu.sKOTH_HostInfo, sFMMCmenu.sKOTH_PlayerInfo, sFMMCmenu.sKOTH_LocalInfo, g_FMMC_STRUCT.sKotHData.sHillData[i], sFMMCmenu.sKOTH_HostInfo.sHills[i], sFMMCmenu.sKOTH_LocalInfo.sHills[i], FALSE, sFMMCMenu.iCurrentHighlightedKOTHHill = i, bError[i])
		ENDIF

		SET_ENTITY_FOR_KOTH_HILL(g_FMMC_STRUCT.sKotHData.sHillData[i], sFMMCmenu.sKOTH_LocalInfo.sHills[i])
	ENDFOR
ENDPROC

PROC DO_KOTH_HILL_CREATION()
	
	BOOL bFoundEntity = FALSE
	BOOL bFoundAvailableEntity = FALSE
	
	IF DOING_HILL_VEHICLE_CREATION(sFMMCmenu)
		SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_VEHICLES)
		VEHICLE_INDEX viCurrentVeh
		
		IF DOES_ENTITY_EXIST(sFMMCmenu.sTempHillRuntimeLocalStruct.eiMyEntity)
		AND IS_ENTITY_A_VEHICLE(sFMMCmenu.sTempHillRuntimeLocalStruct.eiMyEntity)
			viCurrentVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sFMMCmenu.sTempHillRuntimeLocalStruct.eiMyEntity)
		ENDIF
		
		IF (viCurrentVeh != sVehStruct.viCoronaVeh OR sFMMCmenu.sTempHillStruct.iHillEntity > g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles)
		AND (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles < FMMC_MAX_VEHICLES OR sFMMCMenu.iSelectedEntity > -1)
			sFMMCmenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__VEHICLE
			sFMMCmenu.sTempHillRuntimeLocalStruct.eiMyEntity = sVehStruct.viCoronaVeh
			
			IF sFMMCMenu.iSelectedEntity = -1
				SET_KOTH_AREA_ENTITY_INDEX(sFMMCmenu.sTempHillStruct, sFMMCmenu.sTempHillRuntimeLocalStruct, g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles)
			ELSE
				SET_KOTH_AREA_ENTITY_INDEX(sFMMCmenu.sTempHillStruct, sFMMCmenu.sTempHillRuntimeLocalStruct, sFMMCMenu.iSelectedEntity)
			ENDIF
		ENDIF
		
		bFoundEntity = TRUE
		bFoundAvailableEntity = TRUE
		
		IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_OBJECTS)
			CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_OBJECTS)
			PRINTLN("[KOTH] Clearing ciTOO_MANY_OBJECTS because we're doing vehicles")
		ENDIF
		
		//EXIT
	
	ELIF DOING_HILL_OBJECT_CREATION(sFMMCmenu)
		SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_OBJECTS)
		OBJECT_INDEX oiCurrentObj
		
		IF DOES_ENTITY_EXIST(sFMMCmenu.sTempHillRuntimeLocalStruct.eiMyEntity)
		AND IS_ENTITY_AN_OBJECT(sFMMCmenu.sTempHillRuntimeLocalStruct.eiMyEntity)
			oiCurrentObj = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sFMMCmenu.sTempHillRuntimeLocalStruct.eiMyEntity)
		ENDIF
		
		IF (oiCurrentObj != sObjStruct.viCoronaObj OR sFMMCmenu.sTempHillStruct.iHillEntity > g_FMMC_STRUCT_ENTITIES.iNumberOfObjects)
		AND (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects < ciKotH_MAX_HILLS OR sFMMCMenu.iSelectedEntity > -1)
			sFMMCmenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__OBJECT
			sFMMCmenu.sTempHillRuntimeLocalStruct.eiMyEntity = sObjStruct.viCoronaObj
			
			IF sFMMCMenu.iSelectedEntity = -1
				SET_KOTH_AREA_ENTITY_INDEX(sFMMCmenu.sTempHillStruct, sFMMCmenu.sTempHillRuntimeLocalStruct, g_FMMC_STRUCT_ENTITIES.iNumberOfObjects)
			ELSE
				SET_KOTH_AREA_ENTITY_INDEX(sFMMCmenu.sTempHillStruct, sFMMCmenu.sTempHillRuntimeLocalStruct, sFMMCMenu.iSelectedEntity)
			ENDIF
		ENDIF
		
		bFoundEntity = TRUE
		bFoundAvailableEntity = TRUE
		
		IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
			CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
			PRINTLN("[KOTH] Clearing ciTOO_MANY_VEHICLES because we're doing objects")
		ENDIF
		
		//EXIT
	ELSE
		SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_HILL)
		sFMMCmenu.sTempHillStruct.vHill_Center = sCurrentVarsStruct.vCoronaPos
		
		sFMMCmenu.sTempHillStruct.fHill_Heading -= ADD_TO_CURRENT_HEADING(sCurrentVarsStruct.fRotationVeloc, DEFAULT, DEFAULT)
		
		IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
			CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_VEHICLES)
			PRINTLN("[KOTH] Clearing ciTOO_MANY_VEHICLES because we're doing circles")
		ENDIF
		
		IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_OBJECTS)
			CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_OBJECTS)
			PRINTLN("[KOTH] Clearing ciTOO_MANY_OBJECTS because we're doing circles")
		ENDIF
	ENDIF
	
	IF (bFoundAvailableEntity AND sFMMCmenu.sTempHillStruct.iHillEntity > -1)
	OR sFMMCMenu.iCurrentHighlightedKOTHHill = -1
		IF NOT bFoundEntity
			IF NOT IS_HILL_TYPE_A_SHAPE(sFMMCmenu.sTempHillStruct.iHillType)
				RESET_KOTH_HILL_DATA(sFMMCmenu.sTempHillStruct, FALSE)
				sFMMCmenu.sTempHillRuntimeLocalStruct.eiMyEntity = NULL
				REFRESH_MENU(sFMMCmenu)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tlDebugText3D = "iHillEntity: "
		tlDebugText3D += sFMMCmenu.sTempHillStruct.iHillEntity
		DRAW_DEBUG_TEXT(tlDebugText3D, sFMMCmenu.sTempHillStruct.vHill_Center)
		#ENDIF
		
		IF g_FMMC_STRUCT.sKotHData.iNumberOfHills < ciKotH_MAX_HILLS
		OR sFMMCMenu.iSelectedEntity > -1
			sFMMCmenu.sTempHillStruct.vHill_Center = GET_KOTH_HILL_RUNTIME_POSITION(sFMMCmenu.sTempHillStruct, sFMMCmenu.sTempHillRuntimeLocalStruct)
			
			IF IS_ENTITY_ALIVE(sCurrentVarsStruct.piUnderwaterPed)
				SET_ENTITY_LOAD_COLLISION_FLAG(sCurrentVarsStruct.piUnderwaterPed, TRUE)
				
				IF IS_HILL_TYPE_A_SHAPE(sFMMCmenu.sTempHillStruct.iHillType)
				AND HAS_COLLISION_LOADED_AROUND_ENTITY(sCurrentVarsStruct.piUnderwaterPed)
					PUT_KOTH_HILL_ON_GROUND_PROPERLY(sFMMCmenu.sTempHillStruct.vHill_Center, sFMMCmenu.sTempHillStruct.fHill_Radius, sFMMCmenu.sTempHillStruct.vHill_Rotation, sFMMCmenu.sTempHillStruct.vHill_UpVector)
				ENDIF
			ENDIF
			
			IF DOING_HILL_SHAPE_CREATION(sFMMCmenu)
				IF CAN_PLACE_KOTH_HILL(sFMMCmenu.sTempHillStruct, sFMMCmenu, sCurrentVarsStruct)
				AND sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_KotH_PLACE_HILL
					sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
					sCurrentVarsStruct.bAreaIsGoodForPlacement = TRUE
				ELSE
					sCurrentVarsStruct.bitsetOkToPlace = BS_CANT_PLACE__MAP_FAIL
					sCurrentVarsStruct.bAreaIsGoodForPlacement = FALSE
				ENDIF
			ENDIF
			
			IF sFMMCMenu.iCurrentHighlightedKOTHHill = -1
				DRAW_KOTH_HILL(-1, sFMMCmenu.sKOTH_HostInfo, sFMMCmenu.sKOTH_PlayerInfo, sFMMCmenu.sKOTH_LocalInfo, sFMMCmenu.sTempHillStruct, sFMMCmenu.sTempHillRuntimeStruct, sFMMCmenu.sTempHillRuntimeLocalStruct, FALSE, FALSE, NOT CAN_PLACE_KOTH_HILL(sFMMCmenu.sTempHillStruct, sFMMCmenu, sCurrentVarsStruct))
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_HILL_TYPE_A_SHAPE(sFMMCMenu.sTempHillStruct.iHillType)
		
		IF DOES_ENTITY_EXIST(sFMMCmenu.sTempHillRuntimeLocalStruct.eiMyEntity)
		OR sFMMCmenu.sTempHillStruct.iHillEntity > -1
			
			RESET_KOTH_HILL_DATA(sFMMCmenu.sTempHillStruct, FALSE)
			REFRESH_MENU(sFMMCmenu)
			PRINTLN("[KOTH] Resetting temp Hill struct because this Hill has an entity when it's meant to just be a circle!")
		ENDIF
		
		IF sFMMCmenu.fCreationHeightIncrease >= 1.5
			IF NOT IS_BIT_SET(sFMMCMenu.sTempHillStruct.iHillBS, cfKOTHHILLBS_InAir)
				SET_BIT(sFMMCMenu.sTempHillStruct.iHillBS, cfKOTHHILLBS_InAir)
				PRINTLN("[KOTH] Setting cfKOTHHILLBS_InAir")
			ENDIF
		ELSE
			IF IS_BIT_SET(sFMMCMenu.sTempHillStruct.iHillBS, cfKOTHHILLBS_InAir)
				CLEAR_BIT(sFMMCMenu.sTempHillStruct.iHillBS, cfKOTHHILLBS_InAir)
				PRINTLN("[KOTH] Clearing cfKOTHHILLBS_InAir")
			ENDIF
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__CIRCLE
		IF sFMMCmenu.sTempHillStruct.fHill_Radius > cfKOTH_HILL_MAX_RADIUS
		OR sFMMCmenu.sTempHillStruct.fHill_Radius < cfKOTH_HILL_MIN_RADIUS
			KotH_HILL_DATA_STRUCT sTemp
			sFMMCmenu.sTempHillStruct = sTemp
			REFRESH_MENU(sFMMCmenu)
			PRINTLN("[KOTH] Resetting temp Hill struct because the radius is wrong!")
		ENDIF
	ENDIF
ENDPROC

PROC DO_KOTH_AUTO_BOUNDS()
	
	INT iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
	IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape != ciDMBOUNDSSHAPE_SPHERE
		EXIT
	ENDIF
	
	INT iArea
	VECTOR vCentre = <<0,0,0>>
	FLOAT fDistance = 0.0
	FLOAT fRadiusBefore = g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius
	
	FOR iArea = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
		vCentre += g_FMMC_STRUCT.sKotHData.sHillData[iArea].vHill_Center
		
		IF iArea > 0 
			fDistance += VDIST(g_FMMC_STRUCT.sKotHData.sHillData[iArea - 1].vHill_Center, g_FMMC_STRUCT.sKotHData.sHillData[iArea].vHill_Center)
		ENDIF
	ENDFOR
	
	g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsCentre.x = vCentre.x / g_FMMC_STRUCT.sKotHData.iNumberOfHills
	g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsCentre.y = vCentre.y / g_FMMC_STRUCT.sKotHData.iNumberOfHills
	g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsCentre.z = vCentre.z / g_FMMC_STRUCT.sKotHData.iNumberOfHills
	
	fDistance = fDistance / g_FMMC_STRUCT.sKotHData.iNumberOfHills
	FLOAT fBoundsBuffer = TO_FLOAT(ciDMBOUNDS_BUFFER_RADIUS)
	
	g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius = (fDistance + fBoundsBuffer)
	
	FLOAT fScaleUp = 10.0
	
	IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius >= 1000.0
		g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius = g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius * 0.01
		fScaleUp = 100.0
	ELSE
		g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius = g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius * 0.1
		fScaleUp = 10.0
	ENDIF
	
	g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius = ROUND(g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius) * fScaleUp
	g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius = CLAMP(g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius, ciDMBOUNDS_MIN_RADIUS, ciDMBOUNDS_MAX_RADIUS)
	
	IF fRadiusBefore != g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius
		REMOVE_BLIP(sFMMCmenu.sKOTH_LocalInfo.sBounds.biBoundsBlip)
		REFRESH_MENU(sFMMCmenu)
		PRINTLN("[KOTH] DO_KOTH_AUTO_BOUNDS - fRadiusBefore: ", fRadiusBefore, " / fBoundsRadius: ", g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius, " || fScaleUp: ", fScaleUp)
		PRINTLN("[KOTH] DO_KOTH_AUTO_BOUNDS - Updating bounds blip and refreshing menu!")
	ENDIF
ENDPROC

PROC DO_KOTH_BOUNDS_CREATION()
	
	INT iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
	
	IF sFMMCmenu.sActiveMenu = eFmmc_DM_MODIFIER_BOUNDS
		IF sFMMCMenu.iDMModifierIndex > -1
			IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].iBoundsIndex > -1
				iBoundsIndex = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].iBoundsIndex
			ENDIF
			
			IF iBoundsIndex != sFMMCMenu.iCachedDMModBoundsIndex
				IF sFMMCMenu.iCachedDMModBoundsIndex > -1
					REFRESH_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData[sFMMCMenu.iCachedDMModBoundsIndex])
				ENDIF
				sFMMCMenu.iCachedDMModBoundsIndex = iBoundsIndex
			ENDIF
		ENDIF
	ELSE
		//Clear when we're not in the menu anymore
		IF sFMMCMenu.iCachedDMModBoundsIndex > -1
			REFRESH_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData[sFMMCMenu.iCachedDMModBoundsIndex])
			sFMMCMenu.iCachedDMModBoundsIndex = -1
		ENDIF
	ENDIF
	
	// Auto
	IF IS_KING_OF_THE_HILL_CREATOR()
		IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsSetting = ciDMBOUNDSSETTING_AUTO
			DO_KOTH_AUTO_BOUNDS()
		ENDIF
	ELSE
		g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsSetting = ciDMBOUNDSSETTING_CUSTOM
	ENDIF
	
	sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex].vCachedCoronaPos = sCurrentVarsStruct.vCoronaPos
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	
	BOOL bDrawMarker = (sFMMCmenu.sActiveMenu = eFmmc_KotH_BOUNDS OR sFMMCmenu.sActiveMenu = eFmmc_DM_MODIFIER_BOUNDS OR sFMMCmenu.sActiveMenu = efmmc_DM_SHRINKING_BOUNDS)

	sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex].fCurrentRadius = g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius 
	sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex].vCurrentBoundsPoints[0] = g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[0]
	sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex].vCurrentBoundsPoints[1] = g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[1]

	//Draw the size of the shrunken bounds
	IF sFMMCmenu.sActiveMenu = eFmmc_DM_SHRINKING_BOUNDS	
		sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex].fCurrentRadius = g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius * (g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iShrinkToPercent * 0.01)
		
		FLOAT fLerpPercent = (1.0 - (g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iShrinkToPercent * 0.01))
		sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex].vCurrentBoundsPoints[0] = LERP_VECTOR(g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[0], g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsCentre, fLerpPercent)
		sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex].vCurrentBoundsPoints[1] = LERP_VECTOR(g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsPoints[1], g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsCentre, fLerpPercent)
	ENDIF
	
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		DRAW_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData, iBoundsIndex, sFMMCmenu.iPreviousBoundsIndex, piPlayerPed, TRUE, bDrawMarker)
	ELSE
		CLEANUP_CREATOR_DEATHMATCH_BOUNDS_BLIPS(sFMMCmenu.sDeathmatchBoundsData)
	ENDIF
	
	//DRAW_DEBUG_SPHERE(g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].vBoundsCentre, 2.0)
	//DRAW_DEBUG_SPHERE(sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex].vCurrentBoundsPoints[0], 2.0)
	//DRAW_DEBUG_SPHERE(sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex].vCurrentBoundsPoints[1], 2.0)

ENDPROC

PROC PROCESS_KING_OF_THE_HILL_CREATOR()
	
	MAINTAIN_PLACED_KING_OF_THE_HILL_HILLS()
	
	IF NOT CONTENT_IS_USING_ARENA()
		DO_KOTH_BOUNDS_CREATION()
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS
		DO_KOTH_HILL_CREATION()
	ELSE
		IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
		AND sFMMCMenu.sTempHillStruct.iHillType != ciKOTH_HILL_TYPE__CIRCLE
			sFMMCMenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__CIRCLE
			PRINTLN("[KOTH] Setting iHillType = to ciKOTH_HILL_TYPE__CIRCLE")
		ENDIF
		
		IF IS_ENTITY_ALIVE(sCurrentVarsStruct.piUnderwaterPed)
			SET_ENTITY_LOAD_COLLISION_FLAG(sCurrentVarsStruct.piUnderwaterPed, FALSE)
		ENDIF
		
		IF DOES_BLIP_EXIST(sFMMCmenu.sTempHillRuntimeLocalStruct.biCenterBlip)
			REMOVE_BLIP(sFMMCmenu.sTempHillRuntimeLocalStruct.biCenterBlip)
		ENDIF
		IF DOES_BLIP_EXIST(sFMMCmenu.sTempHillRuntimeLocalStruct.biAreaBlip)
			REMOVE_BLIP(sFMMCmenu.sTempHillRuntimeLocalStruct.biAreaBlip)
		ENDIF
	ENDIF
	
	IF sFMMCmenu.iShowingKOTHSpawnClearMsgStartFrame > 0
		SET_WARNING_MESSAGE("KOTH_SP_CLEAR", FE_WARNING_YESNO)
		
		IF sFMMCmenu.iShowingKOTHSpawnClearMsgStartFrame < (GET_FRAME_COUNT() - 10) //Can't accept/decline until 10 frames after starting the alert
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			
				FMMC_REMOVE_ALL_SPAWNS(sPedStruct)
				FMMC_REMOVE_ALL_TEAM_START_POINTS(sTeamSpawnStruct, TRUE)
				
				CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCMenuBS_PlacedSpawnsWithCurrentKotHConfig)
				
				sFMMCmenu.iShowingKOTHSpawnClearMsgStartFrame = -1
				PRINTLN("[TMS][KOTH] Accepted clear spawns alert screen!")
				
				g_FMMC_STRUCT.mnVehicleModel[sFMMCMenu.iSelectedTeam] = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iRespawnVehicleLibrary, sFMMCMenu.iRespawnVehicleType)
				REFRESH_MENU(sFMMCMenu)
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				sFMMCmenu.iShowingKOTHSpawnClearMsgStartFrame = -1
				GO_BACK_TO_MENU(sFMMCmenu)
				PRINTLN("[TMS][KOTH] Declined clear spawns alert screen!")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////        	    DRAW Menu		       	        //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

//// PURPOSE:
 ///    Finds out if the selected menu item will lead to another item eg Vehicle Menu
 /// RETURNS:
 ///    The menu item is a doorway to another menu when accept is pressed
FUNC BOOL IS_THIS_OPTION_A_MENU_GOTO()	
	
	IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Finds out if the selected menu item will do an action, eg Capture a photo
/// RETURNS:
///    True if the menu item will perform an action when accept is pressed
FUNC BOOL IS_THIS_OPTION_A_MENU_ACTION()	
	
	IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the menu item has its Bit set and therefore can be scrolled left/right or selected.
/// RETURNS:
///    True if you can use the item.
FUNC BOOL IS_THIS_OPTION_SELECTABLE()

//	IF sFMMCendStage.bHasValidROS = FALSE
//		RETURN FALSE
//	EL
	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
		IF NOT IS_LONG_BIT_SET(sFMMCMenu.iBitActive, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
			RETURN FALSE
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
		IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
			RETURN FALSE
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF NOT IS_BIT_SET(sFMMCmenu.iOptionsMenuBitSet, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
			RETURN FALSE
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DELETE_ENTITIES
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints = 0
			RETURN FALSE
		ELIF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] = 0
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] = 0
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] = 0
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[3] = 0
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2
			RETURN FALSE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
			RETURN FALSE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfProps = 0
			RETURN FALSE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 5
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps = 0
			RETURN FALSE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 6
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles = 0
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Decides if the menu item can be toggled by pressing left/right
/// RETURNS:
///    True if the menu item can scroll through various options
FUNC BOOL IS_THIS_OPTION_TOGGLEABLE()	
	
	IF g_sMenuData.bItemToggleable[1] = FALSE
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD 
	IF FMMC_IS_ROCKSTAR_DEV() 
		IF sFMMCmenu.sActiveMenu = eFmmc_COVER_POINT_BASE
			RETURN TRUE
		ENDIF
	ENDIF 
	#ENDIF
	
	IF NOT g_sMenuData.bIsSelectable[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
		RETURN FALSE
	ENDIF
		
	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF IS_THIS_OPTION_A_MENU_ACTION()
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEST_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_MODIFIER_SETUP
		IF IS_INT_IN_RANGE(GET_CREATOR_MENU_SELECTION(sFMMCmenu), DM_CGS_SETUP_T1, DM_CGS_SETUP_T4+1)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_OPTION_VALID_FOR_SELECT_HELP_BUTTON()
	IF sFMMCmenu.sActiveMenu = eFmmc_DM_MODIFIER_SETUP
		IF IS_INT_IN_RANGE(GET_CREATOR_MENU_SELECTION(sFMMCmenu), DM_CGS_SETUP_T1, DM_CGS_SETUP_T4+1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Recreates the instructional Buttons in the bottom right.
PROC RESET_UP_BUTTON_HELP()
	PRINTLN("Setting up the menu Help")
	DEBUG_PRINTCALLSTACK()
	REMOVE_MENU_HELP_KEYS()
	
	INT iBitSet, iBitSet2
	
	IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
	
	ENDIF
	
	IF NOT IS_BIT_SET(iHelpBitSet,biRandomizing)	
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
			IF IS_BIT_SET(iHelpBitSet, biPickupEntityButton)
			AND sFMMCMenu.iSelectedEntity = -1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
			ELSE
				IF IS_THIS_OPTION_SELECTABLE()
					AND IS_THIS_OPTION_TOGGLEABLE()
					AND NOT IS_THIS_OPTION_A_MENU_GOTO()
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_CYCLE_OPTIONS)
				ENDIF
					
				IF sFMMCmenu.sActiveMenu = eFmmc_RADIO_MENU
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_CYCLE_OPTIONS)
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
				ENDIF

				IF IS_THIS_OPTION_SELECTABLE()
					IF IS_THIS_OPTION_A_MENU_GOTO()
					OR IS_THIS_OPTION_A_MENU_ACTION()
					OR IS_OPTION_VALID_FOR_SELECT_HELP_BUTTON()
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_DM_RANDOMIZER 
					IF sCurrentVarsStruct.bLOSHitGround = TRUE
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
					ENDIF	
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DELETE_ENTITIES				
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
				ELIF NOT IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)
				OR sFMMCMenu.iSelectedEntity != -1
					if sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU
					AND sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
					AND sCurrentVarsStruct.bAreaIsGoodForPlacement
					AND CAN_PLACE_BUTTON_BE_PRESSED(sFMMCmenu, sCurrentVarsStruct)
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
					ENDIF
				ENDIF
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS
			AND NOT IS_BIT_SET(iBitSet, FMMC_HELP_BUTTON_PLACE)
				IF IS_THIS_OPTION_SELECTABLE()
				AND IS_THIS_OPTION_A_MENU_GOTO()
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
				ENDIF
				
				IF IS_THIS_OPTION_SELECTABLE()
				AND IS_THIS_OPTION_A_MENU_ACTION()
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
				ENDIF
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_CAPTURE_TEAM_VEHICLE_MAIN_MENU
				IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) < g_FMMC_STRUCT.iMaxNumberOfTeams			
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
					
					IF g_FMMC_STRUCT.mnVehicleModel[GET_CREATOR_MENU_SELECTION(sFMMCMenu)] != DUMMY_MODEL_FOR_SCRIPT
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
					ENDIF
				ENDIF
			ENDIF
			
			IF sFMMCmenu.sMenuBack != eFmmc_Null_item
			OR IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				IF sFMMCMenu.iSelectedEntity = -1
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
				ELSE
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_CANCEL)
				ENDIF
			ENDIF
				
			IF IS_BIT_SET(iHelpBitSet, biDeleteEntityButton)
			AND CAN_DELETE_FROM_CURRENT_MENU(sFMMCMenu, sCurrentVarsStruct)
			AND sFMMCMenu.iSelectedEntity = -1
			AND sFMMCmenu.sActiveMenu != eFmmc_KotH_HILLS
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)		
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS 
				IF sFMMCMenu.iCurrentHighlightedKOTHHill > -1
				AND sFMMCMenu.iSelectedEntity = -1
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
				ENDIF
			ENDIF
			
			
			
			IF IS_THIS_OPTION_SELECTABLE()
			AND IS_THIS_OPTION_TOGGLEABLE()
			AND NOT IS_THIS_OPTION_A_MENU_GOTO()
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_CYCLE_OPTIONS)
			ENDIF
		
			IF (sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT 
			OR sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
			OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
			OR sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
			OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
			OR sFMMCmenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
			OR sFMMCmenu.sActiveMenu = eFMMC_OFFSET_POSITION
			OR sFMMCmenu.sActiveMenu = eFMMC_OFFSET_ROTATION
			OR sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			OR sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE
			OR sFMMCmenu.sActiveMenu = eFmmc_SELECT_START_END)
			AND sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
				
			ELIF IS_THIS_OPTION_SELECTABLE()
			AND IS_THIS_OPTION_TOGGLEABLE()
			AND NOT IS_THIS_OPTION_A_MENU_GOTO()
				IF sFMMCmenu.sActiveMenu = eFMMC_GENERIC_OVERRIDE_POSITION
				OR sFMMCmenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION
				OR sFMMCmenu.sActiveMenu = eFMMC_OFFSET_POSITION
				OR sFMMCmenu.sActiveMenu = eFMMC_OFFSET_ROTATION
				OR sFMMCmenu.iCurrentAxis >= 0
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_ADJUST)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBitset, biDLCLocked)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_STORE)
			ENDIF
			
			IF sFMMCmenu.iCurrentMenuLength > 1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
			ENDIF
			
			IF (sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE) AND (sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU)
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					IF IS_BIT_SET(iHelpBitSet, biWarpToCameraButton) 
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
					ENDIF
				ELSE
					IF sFMMCMenu.iSelectedEntity = -1
						if IS_BIT_SET(iHelpBitSet, biWarpToCoronaButton) 
							SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)
			ENDIF
			
			IF NOT IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)
			OR sFMMCMenu.iSelectedEntity != -1
				IF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
				OR sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT_MODIFIER_RESTRICTED
				OR sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
				OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
				OR sFMMCmenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
				OR sFMMCmenu.sActiveMenu = eFMMC_OFFSET_POSITION
				OR sFMMCmenu.sActiveMenu = eFMMC_OFFSET_ROTATION
				OR sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE)
				ENDIF
				
				IF DOING_HILL_RECT_CREATION(sFMMCmenu)
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE)
				ENDIF
			ENDIF
			
			IF CAN_HEIGHT_BE_SET_FOR_ENTITY(sFMMCmenu, sFMMCmenu.iEntityCreation, FALSE)
			AND NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_HEIGHT)
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_TEAM_TEST
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
			ENDIF
			
			IF CAN_PROP_ROTATION_BE_RESET(sFMMCmenu, sCurrentVarsStruct)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_CLEAR_ROTATION)
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
			OR sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
				iBitSet = 0
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_HEIGHT)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
				
				IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
					IF NOT DOES_BLIP_EXIST(bCameraTriggerBlip)
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
							SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
							SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
						ENDIF	
					ENDIF
				ELSE
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
					ENDIF					
					IF DOES_CAM_EXIST(sCamData.cam)
					AND GET_CAM_FOV(sCamData.cam) != 40
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_RESET_FOV)		
					ENDIF
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_FOV)			
				ENDIF
			ENDIF
			
			IF NOT FMMC_IS_CIRCLE_MENU_A_CAMERA_MENU(sFMMCmenu.sActiveMenu) 
				IF NOT IS_GAMEPLAY_CAM_RENDERING()
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE_CAM_HOLD)		
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_MOVE_CAMERA)
				ENDIF
			ENDIF
			
			IF sCurrentVarsStruct.iMenuState = MENU_STATE_PAN_CAM
				iBitSet = 0
				CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
				CLEAR_BIT(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_ALLOW_ALL_HUD)
			ENDIF
			
			IF IS_CURRENT_MENU_A_DM_CUSTOM_SETTINGS_MENU(sFMMCmenu)
			AND sFMMCmenu.sActiveMenu != eFmmc_DM_CUSTOM_GAME_OPTIONS
				SET_BIT(iBitSet2, FMMC_HELP2_DM_BUMPER_MOD_SWAP)
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_DM_CUSTOM_GAME_OPTIONS
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) != g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings
				SET_BIT(iBitSet2, FMMC_HELP2_DM_DELETE_CUSTOM_GAME_SETTING)
			ENDIF
			
			SET_UP_PROP_HELP_BUTTONS(sFMMCmenu, iBitSet, sCurrentVarsStruct)
			
			IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			ENDIF
			
			CREATE_FMMC_INSTRUCTIONAL_BUTTONS(sFMMCmenu, iBitSet, DEFAULT, iBitSet2)
			
		ELSE
			IF sFMMCmenu.sMenuBack != eFmmc_Null_item
				IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1 AND NOT CONTENT_IS_USING_ARENA())
				OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0 AND CONTENT_IS_USING_ARENA())
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
				ENDIF
				
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_ADJUST)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
				
				CREATE_FMMC_INSTRUCTIONAL_BUTTONS(sFMMCmenu, iBitSet)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Finds out which piece of menu help is needed for the current menu and selected item.
/// RETURNS:
///    The description string is returned
FUNC STRING GET_MENU_ITEM_DESCRIPTION()
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != -1
	AND NOT IS_STRING_NULL_OR_EMPTY(sFMMCmenu.sOptionDescription[GET_CREATOR_MENU_SELECTION(sFMMCmenu)])
		RETURN sFMMCmenu.sOptionDescription[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
	ENDIF
	
	MODEL_NAMES mnProp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = START_MENU_ITEM_DM
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				IF IS_KING_OF_THE_HILL()
					RETURN "KH_H_13A"	
				ELSE
					RETURN "DMC_H_13A"	
				ENDIF
			ELSE
				IF CONTENT_IS_USING_ARENA()
					RETURN "RMC_H_00ARNA"
				ELSE
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_13"	
					ELSE
						RETURN "DMC_H_13"	
					ENDIF
				ENDIF
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PANCAM_MENU_ITEM_DM
			if IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELSE
				IF CONTENT_IS_USING_ARENA()
					RETURN "RMC_H_28ARNA"
				ELSE
					RETURN "DMC_H_33"
				ENDIF
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SPAWN_MENU_ITEM_DM
			if IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELSE
				RETURN "DMC_H_14"
			ENDIF	
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SPAWN_MENU_ITEM_DM
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "FMMC_ER_006"
			ELIF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_MANY_START_POINTS)
				RETURN "DMC_H_TMSP"
			ELSE
				IF IS_KING_OF_THE_HILL()
					RETURN "KH_H_14T2"
				ELSE
					RETURN "DMC_H_14T2"
				ENDIF
			ENDIF	
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = WEP_MENU_ITEM_DM
			if IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				RETURN "FMMC_ER_006"
			ELSE
				IF CONTENT_IS_USING_ARENA()
					RETURN "MC_H_WEPAR"
				ELSE
					RETURN "MC_H_WEP"
				ENDIF
			ENDIF							
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEH_MENU_ITEM_DM
			IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				RETURN "FMMC_ER_006"
			ELSE
				IF NOT CONTENT_IS_USING_ARENA()
					RETURN "DMC_H_15"
				ELSE
					RETURN "DMC_H_15ARNA"
				ENDIF
			ENDIF							
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ITEM_DM
			if IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				RETURN "FMMC_ER_006"
			ELSE
				IF IS_KING_OF_THE_HILL()
					RETURN "KH_H_16"
				ELSE
					RETURN "DMC_H_16"
				ENDIF
			ENDIF		
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ARENAPROP_MENU_ITEM_DM
			RETURN "MC_H_LOC_ARPRP"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DON_MENU_RAND_DM
			if IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				RETURN "FMMC_ER_006"
			ELSE
				IF CONTENT_IS_USING_ARENA()
					RETURN "DMC_H_21A"
				ELSE
					RETURN "DMC_H_21"
				ENDIF
			ENDIF							
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MAP_MENU_ITEM_DM
			IF CONTENT_IS_USING_ARENA()
				RETURN "MC_H_MAPA"
			ELSE
				RETURN "MC_H_MAP"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = COV_MENU_ITEM_DM
			RETURN "FMMC_CP_D"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RES_MENU_ITEM_DM
			IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				RETURN "FMMC_ER_006"
			ELSE	
				RETURN "DMC_H_18"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DON_KOTH_HILLS_DM
			IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				RETURN "FMMC_ER_006"
			ELIF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_NON_HILL_OBJ_IN_KOTH)
				RETURN "KTHH_HILLS_OB"
			ELIF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_KOTH_HILL_OOB)
				RETURN "KTHH_HILLS_PA"
			ELSE
				IF g_FMMC_STRUCT.sKotHData.iNumberOfHills > 0
					RETURN "KTHH_HILLS"
				ELSE
					RETURN "KTHH_HILLS_NO"
				ENDIF
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DON_KOTH_BOUNDS_DM
			IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				RETURN "FMMC_ER_006"
			ELSE
				IF IS_KING_OF_THE_HILL_CREATOR()
					IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS)
						RETURN "KTHH_BNDS_SOOB"
					ELSE
						RETURN "KTHH_BNDS"
					ENDIF
				ELSE
					IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS)
						RETURN "DM_H_BNDS_SOOB"
					ELSE
						RETURN "DM_H_BNDS"
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu =eFmmc_CAPTURE_OBJECT_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CAPTURE_OBJECT_PLACE
			RETURN "KTHH_OBJ_TYP"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CAPTURE_OBJECT_DISABLE_SPRINTING
			RETURN "KH_H_OBJ_NS"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CAPTURE_OBJECT_CYCLE
			RETURN "KTHH_OBJ_CYC"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_KotH_BOUNDS
		
		IF sFMMCmenu.iCurrentSelection = KOTH_BOUNDS_INDEX
			RETURN "KTH_H_B_IND"
		ELIF sFMMCmenu.iCurrentSelection = KOTH_BOUNDS_SHAPE
			RETURN "KTHH_B_SH"
		ENDIF
		
		INT iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
		
		IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape = ciDMBOUNDSSHAPE_SPHERE
		
			BOOL bCustomBounds = (g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsSetting = ciDMBOUNDSSETTING_CUSTOM)
			
			IF sFMMCmenu.iCurrentSelection != KOTH_BOUNDS_TYPE
			AND NOT bCustomBounds
				RETURN "KTHH_B_BLK"
			ENDIF
			
			IF sFMMCmenu.iCurrentSelection = KOTH_BOUNDS_TYPE
				RETURN "KTHH_B_TY"
			ELIF sFMMCmenu.iCurrentSelection = KOTH_BOUNDS_RELOCATE
				RETURN "KTHH_B_RL"
			ELIF sFMMCmenu.iCurrentSelection = KOTH_BOUNDS_RADIUS
				RETURN "KTHH_B_RA"
			ELIF sFMMCmenu.iCurrentSelection = KOTH_BOUNDS_3D_CHECK
				RETURN "KTHH_B_3DS"
			ENDIF
			
		ELIF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape = ciDMBOUNDSSHAPE_ANGLED_AREA
			IF sFMMCmenu.iCurrentSelection = KOTH_BOUNDS_POSITION_ONE
				RETURN "KTHH_B_PS0"
			ELIF sFMMCmenu.iCurrentSelection = KOTH_BOUNDS_POSITION_TWO
				RETURN "KTHH_B_PS1"
			ELIF sFMMCmenu.iCurrentSelection = KOTH_BOUNDS_WIDTH
				RETURN "KTHH_B_WDT"
			ELIF sFMMCmenu.iCurrentSelection = KOTH_BOUNDS_HEIGHT
				RETURN "KTHH_B_HET"
			ELIF sFMMCmenu.iCurrentSelection = KOTH_BOUNDS_3D_CHECK
				RETURN "KTHH_B_3D"
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = efmmc_PROP_TEMPLATE_BASE
		RETURN "MC_H_PTMP_BASE"
	ELIF sFMMCmenu.sActiveMenu = efmmc_PROP_TEMPLATE_GRAB_PROPS
		IF sFMMCmenu.iCurrentSelection = 0
			RETURN "MC_H_PTMP_NEW"
		ELIF sFMMCmenu.iCurrentSelection = 1
			RETURN "MC_H_PTMP_SAVE"
		ENDIF			
	ELIF sFMMCmenu.sActiveMenu = eFmmc_COVER_POINT_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciCOVER_COVER_TYPE
			RETURN "FMMC_CP_TYPED"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciCOVER_COVER_HEIGHT
			RETURN "FMMC_CP_HIGHD"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_SELECT_START_END			
		IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
			IF IS_KING_OF_THE_HILL()
				RETURN "KH_H_13A"
			ELSE
				RETURN "DMC_H_13A"
			ENDIF
		ELSE
			IF IS_KING_OF_THE_HILL()
				RETURN "KH_H_13"
			ELSE
				RETURN "DMC_H_13"
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE			
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			IF DOES_BLIP_EXIST(bCameraTriggerBlip)
				RETURN "DMC_H_33B"
			ELSE
				RETURN "DMC_H_33"	
			ENDIF			
		ELSE
			RETURN "DMC_H_35"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS
		IF sFMMCmenu.iCurrentSelection = FIXTURE_REMOVAL__SELECT_FIXTURE
			RETURN "FMMC_H_LMPR"
		ELIF sFMMCmenu.iCurrentSelection = FIXTURE_REMOVAL__PREVIEW
			RETURN "MC_H_WP_PRE"
		ELIF sFMMCmenu.iCurrentSelection = FIXTURE_REMOVAL__CYCLE
			RETURN "FMMC_H_LMPR_C"
		ENDIF
	
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
		
		BOOL bEnoughSpawnPointsPlaced = ((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints <= 0
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SPAWNPOINTS_ADD
			IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_STANDARD
			
				IF bEnoughSpawnPointsPlaced
				AND (IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_FEW_TEAM_1_RESPAWN_POINTS + 0)
				OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_FEW_TEAM_1_RESPAWN_POINTS + 1)
				OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_FEW_TEAM_1_RESPAWN_POINTS + 2)
				OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_FEW_TEAM_1_RESPAWN_POINTS + 3))
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_14TA"
					ELSE
						RETURN "DMC_H_14TA"
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_14AT"
					ELSE
						RETURN "DMC_H_14AT"
					ENDIF
				ELSE
					RETURN "DMC_H_14"
				ENDIF
			ELSE
				IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
					IF NOT bEnoughSpawnPointsPlaced
						IF IS_KING_OF_THE_HILL()
							RETURN "KH_H_14T1"
						ELSE
							RETURN "DMC_H_14T1"
						ENDIF
					ELSE
						IF IS_KING_OF_THE_HILL()
							RETURN "KH_H_14TA"
						ELSE
							RETURN "DMC_H_14TA"
						ENDIF
					ENDIF
				ELSE
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_14T0"
					ELSE
						RETURN "DMC_H_14T0"
					ENDIF
				ENDIF
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SPAWNPOINTS_CYCLE
			if IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				IF IS_KING_OF_THE_HILL()
					RETURN "DMC_H_14C"
				ELSE
					RETURN "KH_H_14C"
				ENDIF
			ELSE
				RETURN "DMC_H_14B"
			ENDIF
		ELSE
			RETURN "FMMC_DM_RS"
		ENDIF
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			IF IS_KING_OF_THE_HILL()
				RETURN "KH_H_14T3"
			ELSE
				RETURN "DMC_H_14T3"
			ENDIF
		ELSE
			if IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				RETURN "DMC_H_14C"
			ELSE
				RETURN "DMC_H_14B"
			ENDIF
		ENDIF
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE
		IF IS_BIT_SET(iLocalBitSet, biDLCLocked)
			IF IS_PS3_VERSION()				// This will need updating when new platforms are added
				RETURN "FMMC_ER_033s"
			ELSE//IF IS_XBOX360_VERSION()
				RETURN "FMMC_ER_033m"
			ENDIF
		ENDIF		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_WEAPON_MODS
		RETURN "MC_H_VEH_VWM"
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_RESTRICTION_MENU
	
		IF IS_MENU_ITEM_SELECTED("FMMC_VEH_RSTF")
			RETURN "MC_H_VEH_RSTF"
		ENDIF
		
		RETURN "MC_H_VEH_RST0"
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
		IF IS_BIT_SET(iLocalBitSet, biDLCLocked)
			RETURN "FMMC_ER_031"
		ENDIF
		
		SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
			CASE VEHICLE_BASIC_LIBRARY
				RETURN "CTF_H_VEH01"
			CASE VEHICLE_BASIC_TYPE
				RETURN "mc_H_VEH0"
			CASE VEHICLE_BASIC_COLOUR
				RETURN "MC_H_VEH1"
			CASE VEHICLE_BASIC_LIVERY
				RETURN "MC_H_VEH1A"
			CASE VEHICLE_BASIC_HEALTH
				RETURN "MC_H_VEH4A"
			CASE VEHICLE_BASIC_BULLETPROOF_TYRES
				RETURN "MC_H_VEH_EBPT"
			CASE VEHICLE_BASIC_WEAPONS
				RETURN "MC_H_VEH_VWM"
			CASE VEHICLE_BASIC_TEAM_RESTRICTIONS
				IF NOT DOING_HILL_VEHICLE_CREATION(sFMMCmenu)
					RETURN "MC_H_VEH_RST"
				ELSE
					RETURN "MC_H_VEH_RSTH"
				ENDIF
			BREAK
			CASE VEHICLE_BASIC_CAN_RESPAWN
				IF DOING_HILL_VEHICLE_CREATION(sFMMCmenu)
					RETURN "MC_H_VHH_RISPW"	
				ELSE
					RETURN "MC_H_VEH_RISPW"	
				ENDIF
			BREAK	
			CASE VEHICLE_BASIC_CYCLE
				RETURN "MC_H_VEH4"
		ENDSWITCH
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
		IF IS_SWITCH_PROP_OPTION_SELECTED(sFMMCmenu)
			RETURN "DMC_H_16_3"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_LIBRARY
			RETURN "DMC_H_16_2a"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_CREATE
			RETURN "DMC_H_16_1"
		ENDIF
		IF IS_STUNT_LIBRARY(sFMMCMenu.iPropLibrary)
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_COLOUR
				RETURN "MC_H_PRP_CLR"
			ElIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
				RETURN GET_PROP_ROTATION_STRING(sFMMCmenu, mnProp)
			ElIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
				RETURN "MC_H_PRP_STK"
			ElIF sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Set_Blimp_Message
				RETURN "MC_H_RSD_BPT"
			ENDIF
			IF IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SNAPPING
					RETURN "MC_H_PRP_SNP"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ADVANCED_OPTIONS
					RETURN "MC_H_PRP_ADV"
				ENDIF
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_STUNT_SPECIAL
				IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_FLARE_01"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_01"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_03"))
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_PLAYERS
						RETURN "MC_H_PRP_PTF"
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_LAPS
						RETURN "MC_H_PRP_LTF"
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ADVANCED_OPTIONS
						RETURN "MC_H_PRP_ADV"
					ENDIF
				ELSE
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ADVANCED_OPTIONS
						RETURN "MC_H_PRP_ADV"
					ENDIF
				ENDIF	
			ELSE
				IF IS_MENU_ITEM_SELECTED("FMMC_PRP_ADV")
					RETURN "MC_H_PRP_ADV"
				ELIF IS_MENU_ITEM_SELECTED("FMMC_PRP_SNP")
					RETURN "MC_H_PRP_SNP"
				ENDIF
			ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePad
				RETURN "MC_H_WP_PSSR"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePadDelay
				RETURN "MC_H_WP_PPDL"
			ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iCurrentMenuLength - 1 
				RETURN "DM_PRP_CYCL"
			ENDIF
				
		ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
				RETURN GET_PROP_ROTATION_STRING(sFMMCmenu, mnProp)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
				RETURN "MC_H_PRP_STK"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ADVANCED_OPTIONS
				RETURN "MC_H_PTMP_BASE"
			ElIF sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Set_Blimp_Message
				RETURN "MC_H_RSD_BPT"
			ENDIF
		ELSE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
				RETURN GET_PROP_ROTATION_STRING(sFMMCmenu, mnProp)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
				RETURN "MC_H_PRP_STK"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ADVANCED_OPTIONS
			AND ARE_STUNT_PROPS_AND_FEATURES_ALLOWED()
				RETURN "MC_H_PRP_ADV"
			ElIF sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Set_Blimp_Message
				RETURN "MC_H_RSD_BPT"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePad
				RETURN "MC_H_WP_PSSR"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePadDelay
				RETURN "MC_H_WP_PPDL"
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0	//Position offset
			IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
				RETURN "MC_H_PRP_PSO"
			ELSE
				RETURN "MC_H_PRP_ONAP"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1	//Rotation offset
			IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
				RETURN "MC_H_PRP_RSO"
			ELSE
				RETURN "MC_H_PRP_ONAR"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2	//Snapping OR Lock Delete
			IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Null_item
				RETURN "MC_H_PRP_LKDL"
			ELSE
				RETURN "MC_H_PRP_SNPO"
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3  //Lock Delete
			RETURN "MC_H_PRP_LKDL"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_PROXIMITY
			RETURN "MC_H_PRP_DPX"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_TRIGGERED
			RETURN "MC_H_PRP_DTR"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_CHAIN
			RETURN "MC_H_PRP_DCH"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_OVERRIDE
			RETURN "MC_H_PRP_ORS"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_ANGLE
			RETURN "MC_H_PRP_SNPANG"
		ENDIF
	ELIF (sFMMCmenu.sActiveMenu = eFMMC_GENERIC_OVERRIDE_POSITION)
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_USE_OVERRIDE
			RETURN "MC_H_ORPU"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_ALIGNMENT
			RETURN "MC_H_ORPL"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_X_VALUE
			RETURN "MC_H_ORPX"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_Y_VALUE
			RETURN "MC_H_ORPY"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_Z_VALUE
			RETURN "MC_H_ORPZ"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_RESET
			RETURN "MC_H_ORPR"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_WORLD_COORD
			RETURN "MC_H_WDPS"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_CLAMP_X
			RETURN "MC_H_OVR_CLMPX"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_CLAMP_Y
			RETURN "MC_H_OVR_CLMPY"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_CLAMP_Z
			RETURN "MC_H_OVR_CLMPZ"
		ENDIF
	ELIF (sFMMCmenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION)
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_USE_OVERRIDE
			RETURN "MC_H_ORRU"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_ALIGNMENT
			RETURN "MC_H_ORRL"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_X_VALUE
			RETURN "MC_H_ORRX"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_Y_VALUE
			RETURN "MC_H_ORRY"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_Z_VALUE
			RETURN "MC_H_ORRZ"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_RESET
			RETURN "MC_H_ORRR"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFMMC_OFFSET_POSITION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_USE_OFFSET
			RETURN "MC_H_PRP_UPO"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_FREE_CAMERA
			RETURN "MC_H_PRP_PTF"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_LOCK_POSITION
			RETURN "MC_H_PRP_UFC"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFMMC_OFFSET_ROTATION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_USE_OFFSET
			RETURN "MC_H_PRP_URO"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_FREE_CAMERA
			RETURN "MC_H_PRP_UFC"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_LOCK_POSITION
			RETURN "MC_H_PRP_LRO"
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_CREATE
			RETURN "DMC_H_16_1"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_LIBRARY
			RETURN "DMC_H_16_2b"
		ELIF IS_MENU_ITEM_SELECTED("FMMC_OVR_POS")
			RETURN "MC_OVR_POS_H"
		ELIF IS_MENU_ITEM_SELECTED("FMMC_OVR_ROT")
			RETURN "MC_OVR_ROT_H"	
		ELIF IS_MENU_ITEM_SELECTED("FMMC_TPST")
			RETURN "MC_H_TPST"
		ELIF IS_MENU_ITEM_SELECTED("FMMC_PRP_ROT")
			RETURN GET_PROP_ROTATION_STRING(sFMMCmenu, mnProp)
		ELIF IS_SWITCH_PROP_OPTION_SELECTED(sFMMCmenu)
			RETURN "DMC_H_16_3"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2 AND ARE_STRINGS_EQUAL(GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType)),"FMMC_DPR_AMOCRT")
			RETURN "FMMC_AC_H_A"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_COLOUR
			RETURN "MC_H_PRP_CLR"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iCurrentMenuLength - 1 
			RETURN "DM_DPRP_CYCL"
		ENDIF
		
		ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_RANDOMIZER
			RETURN "DMC_H_21"		
		
		ELIF sFMMCmenu.sActiveMenu = eFmmc_CAPTURE_TEAM_VEHICLE_MAIN_MENU
		OR sFMMCmenu.sActiveMenu = eFmmc_TEAM_RESPAWN_VEHICLE_MENU 
			IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_STANDARD
				RETURN "KOTH_H_RV_N"
			ELSE
				RETURN "KOTH_H_RV_T"
			ENDIF
			
		ELIF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS
			
			SWITCH sFMMCMenu.sTempHillStruct.iHillType
				CASE ciKOTH_HILL_TYPE__CIRCLE
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__TYPE
						RETURN "KTHH_AM_TOP"
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RADIUS
						RETURN "KTHH_AM_RAD"
					ENDIF
				BREAK
				CASE ciKOTH_HILL_TYPE__RECT
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__TYPE
						RETURN "KTHH_AM_TOR"
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_WIDTH
						RETURN "KH_H_AT_RECWD"
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_HEIGHT
						RETURN "KH_H_AT_RECHE"
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_LENGTH
						RETURN "KH_H_AT_RECLN"
					ENDIF
				BREAK
				CASE ciKOTH_HILL_TYPE__VEHICLE
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__TYPE
						RETURN "KTHH_AM_VEH"
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__ENTITY_OPTIONS
						RETURN "KH_H_AT_VEHST"
					ENDIF
				BREAK
				CASE ciKOTH_HILL_TYPE__OBJECT
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__TYPE
						RETURN "KTHH_AM_OBJ"
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__ENTITY_OPTIONS
						RETURN "KH_H_AT_OBJST"
					ENDIF
				BREAK
			ENDSWITCH
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__CAPTURE_TIME
				RETURN "KH_H_AT_CPTM"
			ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__POINTS_PER_SECOND
				RETURN "KH_H_AT_PPT"
			ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__MINIMUM_PLAYERS
				RETURN "KH_H_AT_MNP"
			ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__CYCLE
				RETURN "KTHH_AM_CYC"
			ENDIF
			
		ELIF sFMMCmenu.sActiveMenu = eFmmc_TEST_BASE
			IF IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
				AND NOT CONTENT_IS_USING_ARENA()
					RETURN "DMC_H_43"
				ELSE
					IF CONTENT_IS_USING_ARENA()
					AND NOT IS_KING_OF_THE_HILL()
						RETURN "FMMCC_EXTEST"
					ENDIF
					
					IF bTestStatBeenIncreased
						RETURN "DMC_H_44P"
					ELSE
						IF IS_KING_OF_THE_HILL()
							RETURN "KH_H_44F"
						ELSE
							RETURN "DMC_H_44F"
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = eFmmc_DELETE_ENTITIES
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0		// All
				RETURN "MC_H_RSTA"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1	// Spawn Pointa
				RETURN "DMCD_H_SP"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2	// Team Start Points
				RETURN "CTF_H_RSTT"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3	// Weapons
				RETURN "CTF_H_RSTW"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4	// Props
				RETURN "CTF_H_RSTP"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 5	// Dynamic Props
				RETURN "CTF_H_RSTD"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 6	// Vehicles
				RETURN "DMCD_H_VEH"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 7	// Hills
				RETURN "KHD_H_DHL"
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = eFmmc_RADIO_MENU
			IF IS_KING_OF_THE_HILL()
				RETURN "KH_H_20"
			ELSE
				RETURN "DMC_H_20"
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
				RETURN "DMC_H_40"
			ELSE
				RETURN "FMMC_H_PP"
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_PTB_OPTIONS
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_ENABLE_TAGGING
				RETURN "DM_H_PTB_MD"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_DISPLAY_BOMB_CARRIER
				RETURN "DM_H_IND_BOMB"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_NITRO_BOMB_CARRIER
				RETURN "DMH_BMB_BST"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_TAGGED_EXPLOSION_TIMER
				RETURN "DM_H_TAG_EXP"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_TAG_RETURN_COOLDOWN
				RETURN "DM_H_TAG_COOL"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_VFX_CROSS_THE_LINE
				RETURN "DM_H_VFX_CTL"
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
			IF NOT IS_LEGACY_DEATHMATCH_CREATOR()
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= DEATHMATCH_DETAILS_TYPE
				RETURN ""
			ENDIF
			IF g_FMMC_STRUCT.iVehicleDeathmatch = 0
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_NAME
					IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
						IF bSignedOut
							RETURN GET_DISCONNECT_HELP_MESSAGE()
						ELSE
							RETURN "FMMCNO_CLOUD"
						ENDIF
					ELIF sFMMCendStage.bHasValidROS = FALSE
						RETURN "FMMCNO_SCLUB"
					ELSE
						IF IS_KING_OF_THE_HILL()
							RETURN "KH_H_36"
						ELSE
							RETURN "DMC_H_36"
						ENDIF
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_DESCRIPTION
					IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
						IF bSignedOut
							RETURN GET_DISCONNECT_HELP_MESSAGE()
						ELSE
							RETURN "FMMCNO_CLOUD"
						ENDIF
					ELIF sFMMCendStage.bHasValidROS = FALSE
						RETURN "FMMCNO_SCLUB"
					ELSE
						IF IS_KING_OF_THE_HILL()
							RETURN "KH_H_37"
						ELSE
							RETURN "DMC_H_37"
						ENDIF
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PHOTO
					RETURN "DMC_H_40"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_TAGS
					IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
						IF bSignedOut
							RETURN GET_DISCONNECT_HELP_MESSAGE()
						ELSE
							RETURN "FMMCNO_CLOUD"
						ENDIF
					ELIF sFMMCendStage.bHasValidROS = FALSE
						RETURN "FMMCNO_SCLUB"
					ELSE
						RETURN "DMC_H_42"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_MAX_PLAYERS
					IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_MANY_START_POINTS)
						IF IS_KING_OF_THE_HILL()
							RETURN "KH_H_00E"
						ELSE
							RETURN "DMC_H_00E"
						ENDIF
					ELSE
						IF IS_KING_OF_THE_HILL()
							RETURN "KH_H_00"
						ELSE
							RETURN "DMC_H_00"
						ENDIF
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_MIN_PLAYERS
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_00A"
					ELSE
						RETURN "DMC_H_00A"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_DURATION
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_01"
					ELSE
						RETURN "DMC_H_01"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_TARGET
					IF IS_KING_OF_THE_HILL()
						IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_STANDARD
							RETURN "KH_H_02A"
						ELSE
							RETURN "KH_H_02T"
						ENDIF
					ELSE
						RETURN "DMC_H_02A"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_STARTWEP
					IF CONTENT_IS_USING_ARENA()
						RETURN "FMKH_AR_LKW"
					ENDIF
					RETURN "DMC_H_22"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_LOCKWEP
				
					IF CONTENT_IS_USING_ARENA()
						RETURN "FMKH_AR_LKW"
					ENDIF
			
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked)
						IF IS_KING_OF_THE_HILL()
							RETURN "FMKH_H1_LKW"
						ELSE
							RETURN "FMMC_H1_LKW"
						ENDIF
					ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked) 
						IF IS_KING_OF_THE_HILL()
							RETURN "FMKH_H2_LKW"
						ELSE
							RETURN "FMMC_H2_LKW"
						ENDIF
					ELSE
						IF IS_KING_OF_THE_HILL()
							RETURN "FMKH_H0_LKW"
						ELSE
							RETURN "FMMC_H0_LKW"
						ENDIF
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_TIME_OF_DAY
					IF NOT CONTENT_IS_USING_ARENA()
						IF IS_KING_OF_THE_HILL()
							RETURN "KTH_H_09"
						ELSE
							RETURN "DMC_H_09"
						ENDIF
					ELSE
						RETURN "DMC_H_09A"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_FORCE_CAMERA
					RETURN "FMMC_FRCAM_H"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_RESPAWN_VEHICLE
					RETURN "KOTH_H_MD6"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_TEAM_OPTIONS
					RETURN "DM_H_TD"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_AVAILABLE_VEHICLES_ARENA
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_AV_A"
					ELSE
						RETURN "DM_H_AV_A"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_DEFAULT_VEHICLE
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_DV_A"
					ELSE
						RETURN "DM_H_DV_A"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_REMOVE_HELMETS
					IF IS_KING_OF_THE_HILL()
						RETURN "MC_H_RKH_HEL"
					ELSE
						RETURN "MC_H_RMD_HEL"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_WEATHER
					IF NOT IS_ARENA_CREATOR()
						IF IS_KING_OF_THE_HILL()
							RETURN "KH_H_WETH"
						ELSE
							RETURN "DMC_H_WETH"
						ENDIF
					ELSE
						RETURN "DMC_H_09A"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_ARENA
					RETURN "MC_H_LOC_ARENA"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_TRAFFIC_DENSITY
					IF CONTENT_IS_USING_ARENA()
						RETURN "AR_H_10"
					ENDIF
					
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_10"
					ELSE
						RETURN "DMC_H_10"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_TYPE
					IF IS_KING_OF_THE_HILL()
						RETURN "KTH_H_45"
					ELSE
						RETURN "DMC_H_45"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_TEAMS
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_46"
					ELSE
						RETURN "DMC_H_46"
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_RADIO
					RETURN "FMMC_HELP_RAD"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_BLIMP_TEXT
					RETURN "MC_H_RSD_BPT"
				ENDIF
			ELSE
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_NAME
					RETURN "DMC_H_36"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_DESCRIPTION
					RETURN "DMC_H_37"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_PHOTO
					RETURN "DMC_H_40"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_MAX_PLAYERS
					RETURN "DMC_H_00"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_MIN_PLAYERS
					RETURN "DMC_H_00A"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_VEHICLE
					RETURN "DMC_H_39"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_DURATION
					RETURN "DMC_H_01"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_RANK
					RETURN "DMC_H_32"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_TARGET
					RETURN "DMC_H_02A"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_FORCE_CAMERA
					RETURN "FMMC_FRCAM_H"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_TYPE
					RETURN "DMC_H_45"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_CONTACT_CHAR
					RETURN "FMMC_MH_D14"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_TIME_OF_DAY
					RETURN "DMC_H_09"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_TRAFFIC_DENSITY
					RETURN "DMC_H_10"
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_VDM_RADIO
					RETURN "FMMC_HELP_RAD"
				ENDIF
			ENDIF
			
		ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_DETAILS
		
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_TEAMNAME
				RETURN "CTF_H_TMNM"
			ElIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_TEAMWANTED
				IF NOT CONTENT_IS_USING_ARENA()
					RETURN "CTF_H_TMWTD"
				ELSE
					RETURN "CTF_H_TMWTDAR"
				ENDIF
			ElIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_TEAMFORCEDWEAPON
				IF CONTENT_IS_USING_ARENA()
					RETURN "MC_H_TFW_A"
				ELSE
					RETURN "CTF_H_TMFWP"
				ENDIF
			ENDIF
			
		ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_NAME_OPTION
			RETURN "CTF_H_TMNAM"
		ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_WANTED_OPTION
			RETURN "CTF_H_TMWTD"
		ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_START_WEAPON_OPTION
			RETURN "CTF_H_TMFWP"	
		ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_DEV_OPTIONS
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_HIDE_SCORES
				RETURN "MC_H_DODM_HS"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_SPAWN_IN_CORONA_VEH
				RETURN "MC_H_DODM_SCV"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_ALLOW_PERSONAL_VEHICLES
				RETURN "FMMC_H_UPV"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_ENABLE_DM_MANUAL_RESPAWN
				RETURN "MC_H_EMRSP"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DISABLE_DM_OUT_OF_VEH_RESPAWN
				RETURN "MC_H_DFROOV"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_FORCE_RESP_ON_BROKEN_VEH
				RETURN "MC_H_EMRSPV"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_FIX_SUICIES_FOR_FLOW_PLAY
				RETURN "MC_H_FPFIXC"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_ENABLE_VEH_HEALTHBAR_OVERHEAD
				RETURN "MC_H_EVOHED"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_ENABLE_VEH_HEALTHBAR_OVERHEAD_LOCAL_PLAYER
				RETURN "MC_H_EVOHEDL"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_LTS_STYLE_LDB_ORDERING
				RETURN "MC_H_LMSOR"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_EXTRA_VEHICLE_COLLISION_DMG
				RETURN "MC_H_EECDFV"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_EXPLODE_AT_ZERO_HEALTH
				RETURN "MC_H_DDR_EAZ"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_REMOVE_TIME_LIMIT
				RETURN "MC_H_REMTL"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_SCORE_TO_KILLS_ON_LDB
				RETURN "DM_H_SDSAK"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_DISABLE_DEFAULT_HELP_TEXT
				RETURN "DM_H_DSDFHPT"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_BULLETPROOF_TYRES
				RETURN "DM_H_BP_TYRES"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_MONSTER_INCREASED_CRUSH_DAMAGE
				RETURN "FMMC_H_MICD"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_SHOW_FIRSTPERSON_VEHICLE_HEALTHBAR
				RETURN "FMMC_H_FPVHB"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_DISABLE_SNACKS_AND_ARMOUR
				RETURN "FM_H_DSA"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_ENABLE_VEHICLE_AMMO
				RETURN "FMMC_H_EAVA"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_VEHICLE_WEAPON_FIRE_DRAIN_TIME
				RETURN "FMMC_H_VWFDT"				
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_321GO_COUNTDOWN
				RETURN "MC_H_DM321G"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_ENABLE_KILL_WHEN_OFF_VEH
				RETURN "FMMC_REOP_15D"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_MAX_TEAM_DIFFERENCE
				RETURN "MC_H_CMTD"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_PLAYERS_REMAINING
				RETURN "MC_H_ARN_RMEN"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_GIVE_PLAYER_PARACHUTE
				RETURN "MC_H_ARN_GVPA"
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_DEV_CORONA
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_TARGET_SCORE
				RETURN "MC_H_CORO_HTS"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_ONLINE_IDS
				RETURN "MC_H_CORO_HOI"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_OPPONENT_HEALTH_BAR
				RETURN "MC_H_CORO_HOHB"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_TRAFFIC
				RETURN "MC_H_CORO_HTRA"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_LOCK_WEAPONS
				RETURN "MC_H_CORO_HLCK"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_FORCED_WEAPONS
				RETURN "MC_H_CORO_HFRC"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_HIDE_TIMELIMIT
				RETURN "MC_H_CORO_HTML"
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_HIDE_WEATHER
				RETURN "MC_H_CORO_HW"
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = eFmmc_CoronaModOptions
			RETURN GET_DISABLE_CORONA_MOD_DESCRIPTIONS(sFMMCmenu)
		ELIF sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_TOP
		OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_LVL1
		OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_LVL2
		OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_LVL3
		OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_LVL4
		OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_MISC
		OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_EXTRAS
		OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_SETTINGS
			RETURN GET_ARENA_HELPTEXT(sFMMCmenu)
		ELIF sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_SETTINGS
			RETURN EDIT_ARENA_OPTIONS_MENU_HELPTEXT(sFMMCmenu)
		ELIF sFMMCmenu.sActiveMenu = eFmmc_ROAMING_SPECTATOR_OPTIONS
			RETURN GET_ROAMING_SPECTATOR_DESCRIPTIONS(sFMMCmenu)
		ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_AMBIENT_OPTIONS		
			RETURN GET_DEV_AMBIENT_DESCRIPTIONS(sFMMCmenu)
		ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_OPTIONS
				IF IS_KING_OF_THE_HILL()
					RETURN "KH_H_23"
				ELSE
					RETURN "DMC_H_23"
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_CREATOR
				IF CONTENT_IS_USING_ARENA()
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_24A"
					ELSE
						RETURN "DMC_H_24A"
					ENDIF
				ELSE
					IF IS_KING_OF_THE_HILL()
						RETURN "KH_H_24"
					ELSE
						RETURN "DMC_H_24"
					ENDIF
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_RADIO
				IF IS_KING_OF_THE_HILL()
					RETURN "KH_H_20"
				ELSE
					RETURN "DMC_H_20"
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_EXIT
				IF IS_KING_OF_THE_HILL()
					RETURN "KH_H_25"
				ELSE
					RETURN "DMC_H_25"
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_TEST
				IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
					IF bSignedOut
						RETURN GET_DISCONNECT_HELP_MESSAGE()
					ELSE
						RETURN "FMMCNO_CLOUD"
					ENDIF
				ELSE
					IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
						IF IS_KING_OF_THE_HILL()
							RETURN "KH_H_30"
						ELSE
							RETURN "DMC_H_30"
						ENDIF
					ELIF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
						
						IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_MANY_START_POINTS)
							RETURN "FMMC_ER_005Z"
						ENDIF
						
						IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_NO_HILLS_PLACED)
							RETURN "FMMC_ER_NHA"
						ENDIF
						
						IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_KOTH_HILL_OOB)
							RETURN "FMMC_ER_HOB"
						ENDIF
						
						IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS)
							RETURN "FMMC_ER_SOB"
						ENDIF
						
						IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_NON_HILL_OBJ_IN_KOTH)
							RETURN "FMMC_ER_NHO"
						ENDIF
						
						IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_OBSCURED)
						OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_START_POINT_OBSCURED)
							RETURN "FMMC_ER_OBS"
						ENDIF
												
						IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)
							RETURN "FMMC_ER_005B"
						ELIF g_bFMMC_TutorialSelectedFromMpSkyMenu
							IF IS_KING_OF_THE_HILL()
								RETURN "KH_H_17"
							ELSE
								RETURN "DMC_H_38DC"
							ENDIF
						ELIF sCurrentVarsStruct.bTestModeFailed
							RETURN "CRT_NT_NWB"
						ELIF sFMMCMenu.iDMModsetSpawnPointBitset != 0
							RETURN "FMMC_ER_005C"
						ELSE
							RETURN "FMMC_ER_005"
						ENDIF
					ELSE
						IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_UNTESTED)
							IF g_FMMC_STRUCT.bMissionIsPublished = TRUE
								IF IS_KING_OF_THE_HILL()
									RETURN "KH_H_41P"
								ELIF NOT IS_LEGACY_DEATHMATCH_CREATOR()
									RETURN "DMC_H_41N"
								ELSE
									RETURN "DMC_H_41P"
								ENDIF
							ELSE
								IF IS_KING_OF_THE_HILL()
									RETURN "KH_H_41"
								ELIF NOT IS_LEGACY_DEATHMATCH_CREATOR()
									RETURN "DMC_H_41NP"
								ELSE
									RETURN "DMC_H_41"
								ENDIF
							ENDIF
						ELSE
							IF IS_KING_OF_THE_HILL()
								RETURN "KH_H_17"
							ELSE
								IF CONTENT_IS_USING_ARENA()
									RETURN "DMC_H_17A"
								ELSE
									RETURN "DMC_H_17"
								ENDIF
							ENDIF
						ENDIF
					ENDIF		
				ENDIF		
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_SAVE
				IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
					IF bSignedOut
						RETURN GET_DISCONNECT_HELP_MESSAGE()
					ELSE
						RETURN "FMMCNO_CLOUD"
					ENDIF
				ELSE
					IF g_FMMC_STRUCT.bMissionIsPublished
						IF IS_KING_OF_THE_HILL()
							RETURN "KH_H_29C"
						ELSE
							RETURN "DMC_H_29C"
						ENDIF
					ELSE
						IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
							IF IS_KING_OF_THE_HILL()
								RETURN "KH_H_38B"
							ELSE
								RETURN "DMC_H_38B"
							ENDIF
						ELSE
							IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TOO_MANY_START_POINTS)
								RETURN "FMMC_ER_005Z"
							ENDIF
							IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
								IF IS_KING_OF_THE_HILL()
									RETURN "KH_H_38M"
								ELSE
									RETURN "DMC_H_38M"
								ENDIF
							ELSE
							
								IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
									RETURN "CNSV_HT"
								ENDIF
							
								IF IS_KING_OF_THE_HILL()
									RETURN "KH_H_28"
								ELSE
									RETURN "DMC_H_28"
								ENDIF
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_PUBLISH
				IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
					IF bSignedOut
						RETURN GET_DISCONNECT_HELP_MESSAGE()
					ELSE
						RETURN "FMMCNO_CLOUD"
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
						IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_CHEATER)
							IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
								IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CHANGE)
									IF IS_KING_OF_THE_HILL()
										RETURN "FMMC_N_1KF"
									ELSE
										RETURN "FMMC_N_1DF"
									ENDIF
								ELSE
									IF IS_KING_OF_THE_HILL()
										RETURN "KH_H_38C"
									ELSE
										RETURN "DMC_H_38C"
									ENDIF
								ENDIF
							ELSE
								IF g_FMMC_STRUCT.bMissionIsPublished
									IF IS_KING_OF_THE_HILL()
										RETURN "KH_H_29B"
									ELSE
										RETURN "DMC_H_29B"
									ENDIF
								ELSE						
									IF IS_KING_OF_THE_HILL()
										RETURN "KH_H_29"
									ELSE
										RETURN "DMC_H_29"
									ENDIF
								ENDIF
							ENDIF
						ELSE
							RETURN "DMC_H_38A"
						ENDIF
					ELSE
						STRING sRet
						
						IF g_FMMC_STRUCT.bMissionIsPublished
							IF IS_KING_OF_THE_HILL()
								sRet = "ALERT_KH_UPD"
							ELSE
								sRet = "ALERT_DM_UPD"
							ENDIF
						ELSE
							IF IS_KING_OF_THE_HILL()
								sRet = "ALERT_KH_PUB"
							ELSE
								sRet = "ALERT_DM_PUB"
							ENDIF
						ENDIF
						RETURN sRet
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Checks the string shown in the description and decides wether it should show a warning icon beside it
/// PARAMS:
///    description - The menu description
/// RETURNS:
///    MENU_ICON_ALERT if it requires and alert , otherwise it is MENU_ICON_DUMMY
FUNC MENU_ICON_TYPE DOES_THIS_DESCRIPTION_NEED_A_WARNING(STRING description)
	IF ARE_STRINGS_EQUAL(description, "DMC_H_13A")
	OR ARE_STRINGS_EQUAL(description, "KH_H_13A")
	OR ARE_STRINGS_EQUAL(description, "DMC_H_30")
	OR ARE_STRINGS_EQUAL(description, "KH_H_30")
	OR ARE_STRINGS_EQUAL(description, "DMC_H_38M")
	OR ARE_STRINGS_EQUAL(description, "KH_H_38M")
	OR ARE_STRINGS_EQUAL(description, "DMC_H_38B")		
	OR ARE_STRINGS_EQUAL(description, "KH_H_38B")	
	OR ARE_STRINGS_EQUAL(description, "FMMCNO_CLOUD")
	OR ARE_STRINGS_EQUAL(description, GET_DISCONNECT_HELP_MESSAGE())
	OR ARE_STRINGS_EQUAL(description, "FMMCNO_SCLUB")
	OR ARE_STRINGS_EQUAL(description, "DMC_H_38C")
	OR ARE_STRINGS_EQUAL(description, "KH_H_38C")
	OR ARE_STRINGS_EQUAL(description, "FMMC_ER_006")
	OR ARE_STRINGS_EQUAL(description, "FMMC_ER_005")
	OR ARE_STRINGS_EQUAL(description, "FMMC_ER_005B")
	OR ARE_STRINGS_EQUAL(description, "FMMC_ER_005C")
	OR ARE_STRINGS_EQUAL(description, "FMMC_N_1DF")
	OR ARE_STRINGS_EQUAL(description, "DMC_H_14AT")
	OR ARE_STRINGS_EQUAL(description, "KH_H_14AT")
	OR ARE_STRINGS_EQUAL(description, "DMC_H_14") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
	OR ARE_STRINGS_EQUAL(description, "DMC_H_14T1")
	OR ARE_STRINGS_EQUAL(description, "KH_H_14T1")
	OR ARE_STRINGS_EQUAL(description, "DMC_H_14T0") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
	OR ARE_STRINGS_EQUAL(description, "DMC_H_14T2") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)
	OR ARE_STRINGS_EQUAL(description, "DMC_H_14T3") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)
	OR ARE_STRINGS_EQUAL(description, "KH_H_14T1")
	OR ARE_STRINGS_EQUAL(description, "KH_H_14T0") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
	OR ARE_STRINGS_EQUAL(description, "KH_H_14T2") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)
	OR ARE_STRINGS_EQUAL(description, "KH_H_14T3") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)
	OR ARE_STRINGS_EQUAL(description, "KH_H_41")
	OR ARE_STRINGS_EQUAL(description, "DMC_H_24B")	
	OR ARE_STRINGS_EQUAL(description, "KH_H_24B")	
	OR ARE_STRINGS_EQUAL(description, "DMC_H_33") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
	OR ARE_STRINGS_EQUAL(description, "KH_H_33") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
	OR (ARE_STRINGS_EQUAL(description, "DMC_H_36") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE))
	OR (ARE_STRINGS_EQUAL(description, "KH_H_36") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE))
	OR (ARE_STRINGS_EQUAL(description, "DMC_H_37") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_DESC))
	OR (ARE_STRINGS_EQUAL(description, "KH_H_37") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_DESC))
	OR ARE_STRINGS_EQUAL(description, "DMC_H_33B")		
	OR ARE_STRINGS_EQUAL(description, "KH_H_33B")		
	OR (ARE_STRINGS_EQUAL(description, "DMC_H_40") AND (IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO) OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_LAST_GEN_PHOTO)))
	OR (ARE_STRINGS_EQUAL(description, "KH_H_40") AND (IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO) OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_LAST_GEN_PHOTO)))
	OR ARE_STRINGS_EQUAL(description, "DMC_H_44F")
	OR ARE_STRINGS_EQUAL(description, "KH_H_44F")
	OR ARE_STRINGS_EQUAL(description, "ALERT_DM_UPD") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
	OR ARE_STRINGS_EQUAL(description, "ALERT_DM_PUB") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
	OR ARE_STRINGS_EQUAL(description, "ALERT_KH_UPD") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
	OR ARE_STRINGS_EQUAL(description, "ALERT_KH_PUB") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
	OR ARE_STRINGS_EQUAL(description, "KTHH_HILLS_NO")
	OR ARE_STRINGS_EQUAL(description, "KTHH_BNDS") AND (IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_KOTH_HILL_OOB) OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS))
	OR ARE_STRINGS_EQUAL(description, "DM_H_BNDS") AND (IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_KOTH_HILL_OOB) OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS))
	OR (ARE_STRINGS_EQUAL(description, "FMMC_ER_NHO") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_NON_HILL_OBJ_IN_KOTH))
	OR ARE_STRINGS_EQUAL(description, "KTHH_HILLS_OB")
	OR ARE_STRINGS_EQUAL(description, "KTHH_HILLS_PA")
	OR ARE_STRINGS_EQUAL(description, "KTHH_BNDS_SOOB")
	OR ARE_STRINGS_EQUAL(description, "DM_H_BNDS_SOOB")
	OR ARE_STRINGS_EQUAL(description, "FMMC_ER_NHA")
	OR ARE_STRINGS_EQUAL(description, "FMMC_ER_HOB")
	OR ARE_STRINGS_EQUAL(description, "FMMC_ER_SOB")
	OR (ARE_STRINGS_EQUAL(description, "MC_H_TEA_BALXD") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert2, ciMENU_ALERT_2_TEAM_BALANCING_NOT_BALANCED))
	OR (ARE_STRINGS_EQUAL(description, "MC_H_SPMR_MS") AND IS_BIT_SET(sFMMCMenu.iDMModsetSpawnPointBitset, GET_CREATOR_MENU_SELECTION(sFMMCmenu)))
	OR (ARE_STRINGS_EQUAL(description, "MC_H_SPMR") AND sFMMCMenu.iDMModsetSpawnPointBitset != 0)
	OR (ARE_STRINGS_EQUAL(description, "DMC_H_14") AND sFMMCMenu.iDMModsetSpawnPointBitset != 0)
		RETURN MENU_ICON_ALERT
	ENDIF
	
	RETURN MENU_ICON_DUMMY
ENDFUNC

/// PURPOSE:
///    This PROC will draw the menu.
PROC DRAW_MENU_SELECTION()
	
	// Clear all the help bits before checking
	clear_BIT(iHelpBitSet, biWarpToCoronaButton) 
	clear_BIT(iHelpBitSet, biWarpToCameraButton) 	
	CLEAR_BIT(iHelpBitSet, biPickupEntityButton)
	CLEAR_BIT(iHelpBitSet, biDeleteEntityButton)	
	sCurrentVarsStruct.bSelectedAnEntity = FALSE
		
	if sFMMCmenu.sActiveMenu != eFmmc_DM_RANDOMIZER
		IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
		AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_CHECKPOINT
		AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_PROPS
			sCurrentVarsStruct.bSelectedAnEntity = TRUE
		ENDIF
		if sFMMCMenu.iSelectedEntity = -1
			IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
			AND (sCurrentVarsStruct.iHoverEntityType = sFMMCmenu.iEntityCreation)
			OR (sFMMCmenu.iEntityCreation = CREATION_TYPE_HILL AND sFMMCmenu.iCurrentHighlightedKOTHHill > -1)
				IF IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona)
				OR (sFMMCmenu.iEntityCreation = CREATION_TYPE_HILL AND sFMMCmenu.iCurrentHighlightedKOTHHill > -1)
					SET_BIT(iHelpBitSet, biPickupEntityButton)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
				SET_BIT(iHelpBitSet, biDeleteEntityButton)
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_DM_CREATOR_TUTORIAL_COMPLETED(tutDMCreator)
	OR tutDMCreator.tutorialStage >= DM_CREATOR_TUTORIAL_STAGE_SWITCH_CAM
	OR IS_KING_OF_THE_HILL()
		if sFMMCMenu.iSelectedEntity = -1		
			IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				SET_BIT(iHelpBitSet, biWarpToCameraButton) 
			ELSE
				SET_BIT(iHelpBitSet, biWarpToCoronaButton)
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bRefresh
	IF iHelpBitSetOld != iHelpBitSet
		iHelpBitSetOld = iHelpBitSet
		PRINTLN("bRefresh = TRUE")
		bRefresh = TRUE
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF NOT IS_GAMEPLAY_CAM_RENDERING()
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
			OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
				PRINTLN("bRefresh = TRUE - FORCED BY HOLDING LSHIFT DOWN")
				bRefresh = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Set the current item and draw menu each frame
	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		SET_TOP_MENU_ITEM(iTopItem)
	ENDIF
	SET_CURRENT_MENU_ITEM(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
		
	IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
		sCurrentVarsStruct.bDisplaySelected = FALSE
	ENDIF
	
	PRINTLN("sFailReason = ", sCurrentVarsStruct.sFailReason)
	IF sCurrentVarsStruct.bDisplayFailReason
		IF sCurrentVarsStruct.iFailDescriptionTimer > GET_GAME_TIMER() - 5000
			SET_CURRENT_MENU_ITEM_DESCRIPTION(sCurrentVarsStruct.sFailReason)
		ELSE
			sCurrentVarsStruct.bDisplayFailReason = FALSE
		ENDIF
	ENDIF
	
	IF sCurrentVarsStruct.bDisplayFailReason = FALSE
		IF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
		AND IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRPW", 0, MENU_ICON_ALERT)
		ELIF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRPW", 0, MENU_ICON_ALERT)
		ELIF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRW", 0, MENU_ICON_ALERT)
			
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
		AND CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRDPW", 0, MENU_ICON_ALERT)
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRDPW", 0, MENU_ICON_ALERT)
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRDRW", 0, MENU_ICON_ALERT)
			
		ELIF sCurrentVarsStruct.bDisplaySelected
		AND sFMMCmenu.iEntityCreation != CREATION_TYPE_NONE
			PROCESS_SHOWING_HIGHLIGHTED_ENTITY(sCurrentVarsStruct)
		ELSE
			TEXT_LABEL_23 tlDescription = GET_DESCRIPTION_AS_A_TEXT_LABEL(sFMMCmenu)
			IF NOT IS_STRING_NULL_OR_EMPTY(tlDescription)
				SET_CURRENT_MENU_ITEM_DESCRIPTION_AS_TEXT_LABEL(tlDescription, 0, MENU_ICON_DUMMY)
			ELSE
				SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_MENU_ITEM_DESCRIPTION(), 0, DOES_THIS_DESCRIPTION_NEED_A_WARNING(GET_MENU_ITEM_DESCRIPTION())) // Pass in "" to clear
			ENDIF
			IF ARE_STRINGS_EQUAL(GET_MENU_ITEM_DESCRIPTION(), "DMC_H_14AT")
			OR ARE_STRINGS_EQUAL(GET_MENU_ITEM_DESCRIPTION(), "DMC_H_14T1")
			OR ARE_STRINGS_EQUAL(GET_MENU_ITEM_DESCRIPTION(), "KH_H_14AT")
			OR ARE_STRINGS_EQUAL(GET_MENU_ITEM_DESCRIPTION(), "KH_H_14T1")			
				IF ((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints > 0
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints)
					IF ((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints > 1
						ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FMMC_SPP")
					ELSE
						ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FMMC_SPS")
					ENDIF
				ELSE
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(0)
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FMMC_SPP")
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = efmmc_PROP_TEMPLATE_GRAB_PROPS
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FM_NXT_DM")
			ENDIF
		ENDIF
	ENDIF	
		
	INT iLines = 10
	
	SET_MAX_MENU_ROWS_TO_DISPLAY(iLines)
	
	DRAW_MENU()
	
	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		iTopItem = GET_TOP_MENU_ITEM()
	ENDIF
	//If the help text need to be redrawn then do so.
	IF bRefresh
		sCurrentVarsStruct.bResetUpHelp = TRUE
	ENDIF
ENDPROC

//// PURPOSE:
 ///    The fade and Warning Screen when the player is trying to quit. Checks for a button release before continuing.
PROC DO_END_MENU()
	
	IF NOT IS_SCREEN_FADING_OUT()
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				DO_SCREEN_FADE_OUT(500)	
				SET_CREATOR_AUDIO(FALSE, TRUE)
			ENDIF
		ELSE
			IF NOT HAS_DM_CREATOR_TUTORIAL_COMPLETED(tutDMCreator)	
				DRAW_END_MENU(TRUE)
				
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					sCurrentVarsStruct.bSwitching = FALSE
					sCurrentVarsStruct.stiScreenCentreLOS = NULL
					DO_SCREEN_FADE_IN(500)
					SET_CREATOR_AUDIO(TRUE, TRUE)
				ENDIF
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)	
					sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_FINISHING
				ENDIF
				
			ELSE

				DRAW_END_MENU()
				
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					sCurrentVarsStruct.bSwitching = FALSE
					sCurrentVarsStruct.stiScreenCentreLOS = NULL
					DO_SCREEN_FADE_IN(500)
					SET_CREATOR_AUDIO(TRUE, TRUE)
				ENDIF
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_FINISHING
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////        	    Mantain Menu Actions       	    //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    This should be called once the player presses cancel. If you arent editing it resets the creation types, stages
///    and rescales the blips. If you are editing, it will cancel editing instead.
/// PARAMS:
///    iTypePassed - Creation Type eg CREATION_TYPE_VEHICLES
PROC CANCEL_ENTITY_CREATION(INT iTypePassed)
	
	PRINTLN("CANCEL_ENTITY_CREATION - sFMMCMenu.iSelectedEntity is ", sFMMCMenu.iSelectedEntity, " || iTypePassed = ", iTypePassed)
	
	IF sFMMCMenu.iSelectedEntity = -1
		INT i,j
		
		IF sCurrentVarsStruct.mnThingToPlaceForBudget != DUMMY_MODEL_FOR_SCRIPT
			REMOVE_MODEL_FROM_CREATOR_BUDGET(sCurrentVarsStruct.mnThingToPlaceForBudget)
			sCurrentVarsStruct.mnThingToPlaceForBudget = DUMMY_MODEL_FOR_SCRIPT
		ENDIF
		
		sPedStruct.iSwitchingINT	= CREATION_STAGE_WAIT
		sWepStruct.iSwitchingINT 	= CREATION_STAGE_WAIT
		sVehStruct.iSwitchingINT	= CREATION_STAGE_WAIT
		sObjStruct.iSwitchingINT	= CREATION_STAGE_WAIT
		SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		
		#IF IS_DEBUG_BUILD
		sCoverStruct.iSwitchingINT						= CREATION_STAGE_WAIT
		#ENDIF
		iTriggerCreationStage							= CREATION_STAGE_WAIT
		sCurrentVarsStruct.iZoneCreationStage 										= CREATION_STAGE_WAIT
		FOR i = 0 TO FMMC_MAX_TEAMS - 1
			sTeamSpawnStruct[i].iSwitchingINT = CREATION_STAGE_WAIT
		ENDFOR
		
		IF iTypePassed = CREATION_TYPE_VEHICLES
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
				CREATE_FMMC_BLIP(sVehStruct.biVehicleBlip[i], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, HUD_COLOUR_BLUEDARK, "FMMC_B_10", 1)
			ENDFOR
			UNLOAD_ALL_VEHICLE_MODELS()
		ELIF iTypePassed	= CREATION_TYPE_SPAWN_LOCATION	
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints - 1)
				CREATE_FMMC_BLIP(sPedStruct.biPedBlip[i], g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, HUD_COLOUR_BLUELIGHT, "FMMC_B_3", 1)
			ENDFOR
		ELIF iTypePassed	= CREATION_TYPE_TEAM_SPAWN_LOCATION	
			FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
				FOR j = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] - 1)
					CREATE_FMMC_BLIP(sTeamSpawnStruct[i].biPedBlip[j], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][j].vPos, HUD_COLOUR_WHITE, getNameForTeamSpawnBlip(i), 1)
					SET_BLIP_COLOUR(sTeamSpawnStruct[i].biPedBlip[j] , getColourForSpawnBlip(i))
				ENDFOR
			ENDFOR
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			FLOAT fSize
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1)
				IF IS_SPECIAL_WEAPON_BLIP(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
					fSize = 1
				ELSE
					fSize = 1
				ENDIF
				CREATE_FMMC_BLIP(sWepStruct.biWeaponBlip[i], GET_ENTITY_COORDS(sWepStruct.oiWeapon[i]), HUD_COLOUR_GREEN, "", fSize)
				SET_BLIP_SPRITE(sWepStruct.biWeaponBlip[i] , GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt,sWepStruct.iSubType))
			ENDFOR
			UNLOAD_ALL_FMMC_WEAPON_ASSETS(TRUE, sWepStruct)
			UNLOAD_ALL_FMMC_WEAPON_ASSETS(FALSE, sWepStruct)
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1)
				IF DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
					CREATE_FMMC_BLIP(sPropStruct.biObject[i], GET_ENTITY_COORDS(sPropStruct.oiObject[i]), HUD_COLOUR_PURPLE, "FMMC_B_5", 1)
				ENDIF
			ENDFOR
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
		ELIF iTypePassed	=  CREATION_TYPE_DYNOPROPS
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1)
				CREATE_FMMC_BLIP(sDynoPropStruct.biObject[i], GET_ENTITY_COORDS(sDynoPropStruct.oiObject[i]), HUD_COLOUR_NET_PLAYER2, "FMMC_B_13", 1)
			ENDFOR
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
		ELIF iTypePassed	=  CREATION_TYPE_OBJECTS
			UNLOAD_ALL_OBJ_MODELS(sObjStruct)
		ELIF iTypePassed	=  CREATION_TYPE_TRIGGER
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				CREATE_FMMC_BLIP(sStartEndBlips.biStart, g_FMMC_STRUCT.vStartPos, HUD_COLOUR_WHITE, "FMMC_B_4", 1)	
				BLIP_SPRITE blip = GET_TRIGGER_RADAR_BLIP() 
				IF blip != RADAR_TRACE_INVALID
					SET_BLIP_SPRITE(sStartEndBlips.biStart, blip)
					SET_BLIP_NAME_FROM_TEXT_FILE(sStartEndBlips.biStart, "FMMC_B_4")	
				ENDIF
			ENDIF
		ENDIF	
		
		IF sFMMCmenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCmenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
		AND sFMMCmenu.sActiveMenu != eFmmc_PROP_ADVANCED_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFmmc_SNAPPING_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_POSITION
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_ROTATION
		AND sFMMCmenu.sActiveMenu != eFmmc_SOUND_TRIGGER_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFmmc_PROP_SIGN_TRIGGER_OPTIONS
			PRINTLN("[LH][VANISH] 4")
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
			CLEAR_BIT(sFMMCmenu.iDynoPropBitset, ciFMMC_DYNOPROP_Position_Override)
			sFMMCmenu.iCurrentAxis = -1
		ENDIF
		
		IF sFMMCmenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCmenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
		AND sFMMCmenu.sActiveMenu != eFmmc_PROP_ADVANCED_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFmmc_SNAPPING_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_POSITION
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_ROTATION
		AND sFMMCmenu.sActiveMenu != eFmmc_SOUND_TRIGGER_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFmmc_PROP_SIGN_TRIGGER_OPTIONS
			PRINTLN("[JJT] sFMMCmenu.vOverridePosition changed 8")
			SET_OVERRIDE_POSITION_COORDS(sFMMCmenu, <<0,0,0>>)
			SET_OVERRIDE_ROTATION_VECTOR(sFMMCmenu, <<0,0,0>>)
			PRINTLN("[PROP ROTATION](0) Setting override rotation to zero ")
		ENDIF
		
		SET_LONG_BIT(sFMMCMenu.iBitActive,PAN_CAM_PLACE)
		IF DOES_BLIP_EXIST(bCameraTriggerBlip)
			REMOVE_BLIP(bCameraTriggerBlip)
		ENDIF
		
		SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
		
		sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
		sHCS.hcCurrentCoronaColour = sHCS.hcDefaultCoronaColour	
		REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
	ELSE 
	
		PLAY_SOUND_FRONTEND(-1, "EDIT", GET_CREATOR_SPECIFIC_SOUND_SET())
			
		IF iTypePassed	= CREATION_TYPE_SPAWN_LOCATION
			PRINTLN("TypePassed	= CREATION_TYPE_SPAWN_LOCATION")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity] = g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint
			
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity].iTeam  = ", g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity].iTeam )
				
			CREATE_FMMC_BLIP(sPedStruct.biPedBlip[sFMMCMenu.iSelectedEntity], g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity].vPos, HUD_COLOUR_BLUELIGHT, "FMMC_B_3", 1)

			sPedStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	= CREATION_TYPE_TEAM_SPAWN_LOCATION
			PRINTLN("TypePassed	= CREATION_TYPE_TEAM_SPAWN_LOCATION")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam][sFMMCMenu.iSelectedEntity] = g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint
			
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity].iTeam  = ", g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam)
				
			CREATE_FMMC_BLIP(sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].biPedBlip[sFMMCMenu.iSelectedEntity], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam][sFMMCMenu.iSelectedEntity].vPos, HUD_COLOUR_WHITE, getNameForTeamSpawnBlip(g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam), 1)
			SET_BLIP_COLOUR(sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].biPedBlip[sFMMCMenu.iSelectedEntity] , getColourForSpawnBlip(g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam))
			
			sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].iSwitchingINT = CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_VEHICLES		
			PRINTLN("IF iTypePassed	= CREATION_TYPE_VEHICLES")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_VEHICLE(sVehStruct, sFMMCMenu.iSelectedEntity)
			sVehStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_WEAPONS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_WEAPON(sFMMCmenu, sWepStruct, sFMMCMenu.iSelectedEntity, g_FMMC_STRUCT.iVehicleDeathmatch = 0, sCurrentVarsStruct)
			sWepStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_PROPS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_PROP(sPropStruct, sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			
		ELIF iTypePassed	=  CREATION_TYPE_DYNOPROPS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_DYNOPROPS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_DYNOPROP(sPropStruct, sDynoPropStruct, sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			
		ELIF iTypePassed	=  CREATION_TYPE_OBJECTS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_OBJECTS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_OBJECT(sObjStruct, sFMMCMenu.iSelectedEntity, sCurrentVarsStruct)
			sObjStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ENDIF	
		
	ENDIF

ENDPROC

FUNC INT GET_DM_HELPTEXT_SLOT()
	IF sFMMCmenu.iCurrentSelection > 0
		RETURN FLOOR(TO_FLOAT(sFMMCmenu.iCurrentSelection) / TO_FLOAT(DM_HELPTEXT_OPTIONS_PER_SLOT))
	ENDIF
	
	RETURN 0
ENDFUNC

PROC EDIT_DM_HELPTEXT_MENU(INT iChange)

	INT iOptionType = sFMMCmenu.iCurrentSelection - GET_DM_HELPTEXT_SLOT() * DM_HELPTEXT_OPTIONS_PER_SLOT
	
	IF iOptionType = DM_HELPTEXT_OPTION_TEXTLABEL
		//EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iPlayerLives, iChange, 21, 0)
	ELIF iOptionType = DM_HELPTEXT_OPTION_DELAY
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_Struct.iDMHelptextDelay[GET_DM_HELPTEXT_SLOT()], iChange, 100, -1)
	ENDIF
ENDPROC

FUNC BOOL IS_ENTITY_VALID_FOR_KOTH_HILL()
	INT i
	FOR i = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
		IF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = sFMMCmenu.sTempHillStruct.iHillType
		AND g_FMMC_STRUCT.sKotHData.sHillData[i].iHillEntity = sFMMCmenu.sTempHillStruct.iHillEntity
			PRINTLN("[KOTH] IS_ENTITY_VALID_FOR_KOTH_HILL - Not valid due to other area ", i, " already using this entity")
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN SET_ENTITY_FOR_KOTH_HILL(sFMMCmenu.sTempHillStruct, sFMMCmenu.sTempHillRuntimeLocalStruct)
ENDFUNC

PROC SET_KOTH_HILL_OPTIONS_MENU(INT iChangeValue)

	// TYPE
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__TYPE
	AND sFMMCmenu.iSelectedEntity = -1
	
		IF sVehStruct.iSwitchingINT = CREATION_STAGE_SET_UP
		OR sVehStruct.iSwitchingINT = CREATION_STAGE_MAKE
		OR sVehStruct.iSwitchingINT = CREATION_STAGE_DONE
			PRINTLN("[KOTH] SET_KOTH_HILL_OPTIONS - Exiting because sVehStruct.iSwitchingINT is ", sVehStruct.iSwitchingINT)
			EXIT
		ENDIF
		IF sObjStruct.iSwitchingINT = CREATION_STAGE_SET_UP
		OR sObjStruct.iSwitchingINT = CREATION_STAGE_MAKE
		OR sObjStruct.iSwitchingINT = CREATION_STAGE_DONE
			PRINTLN("[KOTH] SET_KOTH_HILL_OPTIONS - Exiting because sObjStruct.iSwitchingINT is ", sObjStruct.iSwitchingINT)
			EXIT
		ENDIF
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCMenu)
			PRINTLN("[KOTH] SET_KOTH_HILL_OPTIONS - Exiting because player is pressing place button")
			EXIT
		ENDIF
		
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.sTempHillStruct.iHillType, iChangeValue, ciKOTH_HILL_TYPE__MAX, ciKOTH_HILL_TYPE__CIRCLE)
		
		WHILE NOT IS_HILL_TYPE_AVAILABLE(sFMMCmenu.sTempHillStruct.iHillType)
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.sTempHillStruct.iHillType, iChangeValue, ciKOTH_HILL_TYPE__MAX, ciKOTH_HILL_TYPE__CIRCLE)
		ENDWHILE
	ENDIF
	
	// ENTITY / RADIUS	
	IF sFMMCmenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__CIRCLE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RADIUS
		AND CAN_EDIT_KOTH_HILL(sFMMCMenu)
		
			// DONE ELSEWHERE - SEARCH FOR "EDITING CIRCLE HILL RADIUS"
		
			//EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.sTempHillStruct.fHill_Radius, TO_FLOAT(iChangeValue) * cfKOTH_HILL_RADIUS_INCREMENT, 100.0, 3.0, DEFAULT, TRUE)
		ENDIF
	ELSE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__ENTITY_OPTIONS
			// Nothing, allow cursor to do this
		ENDIF
	ENDIF
	
	// CAPTURE TIME
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__CAPTURE_TIME
	AND CAN_EDIT_KOTH_HILL(sFMMCMenu)
		EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.sTempHillStruct.fHill_CaptureTime, TO_FLOAT(iChangeValue) * cfKOTH_HILL_CAPTIME_INCREMENT, cfKOTH_HILL_MAX_CAPTIME, cfKOTH_HILL_MIN_CAPTIME)
	ENDIF
	
	// POINTS PER SECOND
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__POINTS_PER_SECOND
	AND CAN_EDIT_KOTH_HILL(sFMMCMenu)
		EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.sTempHillStruct.fHill_PointsPerTick, TO_FLOAT(iChangeValue) * cfKOTH_HILL_POINTSPERTICK_INCREMENT, cfKOTH_HILL_MAX_POINTSPERTICK, cfKOTH_HILL_MIN_POINTSPERTICK)
	ENDIF
	
	// MINIMUM PLAYERS
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__MINIMUM_PLAYERS
	AND CAN_EDIT_KOTH_HILL(sFMMCMenu)
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.sTempHillStruct.iHill_MinimumPlayers, iChangeValue, NUM_NETWORK_PLAYERS, 2)
	ENDIF
	
	// CYCLE
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__CYCLE
		DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT.sKotHData.iNumberOfHills)
	ENDIF
	
ENDPROC

PROC SET_KOTH_BOUNDS_OPTIONS_MENU(INT iChangeValue)
	
	INT iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
	
	IF NOT IS_KING_OF_THE_HILL_CREATOR()
	AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_INDEX
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex, iChangeValue, FMMC_MAX_NUM_DM_BOUNDS, 0, DEFAULT, TRUE, TRUE)
		REFRESH_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex])
	ENDIF
	
	iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_SHAPE
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape, iChangeValue, ciDMBOUNDSSHAPE_MAX, ciDMBOUNDSSHAPE_SPHERE, DEFAULT, TRUE, TRUE)

		IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape = ciDMBOUNDSSHAPE_SPHERE
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius = 80.0
			
		ELIF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape = ciDMBOUNDSSHAPE_ANGLED_AREA
			g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius = 80.0
			
		ENDIF
		
		REFRESH_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex])
	
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_OOB_TIME
		ADD_CHANGE_VALUE_MODIFIER(iChangeValue, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iOOBTimer, 0, 5000.0)
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iOOBTimer, iChangeValue, ciDM_OUT_OF_BOUNDS_TIME_MAX+1, 0)
	ENDIF
	
	
	IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape = ciDMBOUNDSSHAPE_SPHERE
	
		BOOL bCustomBounds = (g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsSetting = ciDMBOUNDSSETTING_CUSTOM OR NOT IS_KING_OF_THE_HILL_CREATOR())
		
		// TYPE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_TYPE
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsSetting, iChangeValue, ciDMBOUNDSSETTING_MAX, ciDMBOUNDSSETTING_AUTO, DEFAULT, TRUE, TRUE)
			
			IF IS_KING_OF_THE_HILL_CREATOR()
				IF iChangeValue != 0
					IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsSetting = ciDMBOUNDSSETTING_CUSTOM
						IF NOT IS_VECTOR_ZERO(sFMMCmenu.sBackupKOTHBoundsInfo[iBoundsIndex].vBoundsCentre)
							g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex] = sFMMCmenu.sBackupKOTHBoundsInfo[iBoundsIndex]
							g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsSetting = ciDMBOUNDSSETTING_CUSTOM
							PRINTLN("[KOTH] SET_KOTH_BOUNDS_OPTIONS - Setting bounds data back to backup")
						ENDIF
					ENDIF
					
					REMOVE_BLIP(sFMMCmenu.sKOTH_LocalInfo.sBounds.biBoundsBlip)
					PRINTLN("[KOTH] SET_KOTH_BOUNDS_OPTIONS - Refreshing bounds blip")
				ENDIF
			ELSE
				g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex] = sFMMCmenu.sBackupKOTHBoundsInfo[iBoundsIndex]
				g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsSetting = ciDMBOUNDSSETTING_CUSTOM
			ENDIF
		ENDIF
		
		// RELOCATE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_RELOCATE
		AND bCustomBounds
			// Nothing, allow action to do this
		ENDIF

		// RADIUS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_RADIUS
		AND bCustomBounds
			FLOAT fChangeAmount = 10.0
			
			IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius + (fChangeAmount * iChangeValue) > 1000.0
				fChangeAmount = 100.0
			ENDIF
			
			IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius + (5 * iChangeValue) <= 200.0
				fChangeAmount = 5.0
			ENDIF
			
			FLOAT fMaxValue = ciDMBOUNDS_MAX_RADIUS
			IF IS_KING_OF_THE_HILL_CREATOR()
				fMaxValue = ciKOTHBOUNDS_MAX_RADIUS
			ENDIF
			
			EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius, TO_FLOAT(iChangeValue) * fChangeAmount, fMaxValue, ciDMBOUNDS_MIN_RADIUS, DEFAULT, TRUE)
			REFRESH_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex])
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_3D_CHECK
		AND bCustomBounds
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sKotHData.iKotHBS, ciKotHBS_3DBoundsCheck)
			REFRESH_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex])
		ENDIF
	
	ELIF g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iBoundsShape = ciDMBOUNDSSHAPE_ANGLED_AREA
		
		FLOAT fChangeAmount = 5.0
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_POSITION_ONE
			// Nothing, allow action to do this
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_POSITION_TWO
			// Nothing, allow action to do this
			
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_WIDTH
			EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsRadius, TO_FLOAT(iChangeValue) * fChangeAmount, cfDMBOUNDS_MAX_WIDTH + fChangeAmount, cfDMBOUNDS_MIN_WIDTH, DEFAULT, TRUE)
			REFRESH_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex])
			
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_HEIGHT
			EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].fBoundsHeight, TO_FLOAT(iChangeValue) * fChangeAmount, cfDMBOUNDS_MAX_HEIGHT + fChangeAmount, cfDMBOUNDS_MIN_HEIGHT, DEFAULT, TRUE)
			REFRESH_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex])
			
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_BOUNDS_3D_CHECK
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sKotHData.iKotHBS, ciKotHBS_3DBoundsCheck)
			REFRESH_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData[iBoundsIndex])
			
		ENDIF
		
	ENDIF
ENDPROC

PROC EDIT_DEV_OPTIONS_MENU(INT iChange)
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_RANK
		IF iChange > 0
			IF g_FMMC_STRUCT.iRank = 9900
				iChange *= 99
			ELSE
				IF g_FMMC_STRUCT.iRank < 100
				
				ELIF g_FMMC_STRUCT.iRank < 1000
					iChange *= 10
				ELIF g_FMMC_STRUCT.iRank < 9999
					iChange *= 100	
				ELSE
					iChange *= 90000
				ENDIF
			ENDIF
		ELSE
			IF g_FMMC_STRUCT.iRank = 9999
				iChange *= 99
			ELSE
				IF g_FMMC_STRUCT.iRank <= 100
				
				ELIF g_FMMC_STRUCT.iRank <= 1000
					iChange *= 10
				ELIF g_FMMC_STRUCT.iRank <= 9999
					iChange *= 100
				ELSE
					iChange *= 90000
				ENDIF
			ENDIF
		ENDIF
		g_FMMC_STRUCT.iRank	+= iChange		
		CAP_FMMC_MENU_ITEM(OPTION_MISSION_MAX_RANK, g_FMMC_STRUCT.iRank, 1)	
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_VEH_RESPAWN
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciRESPAWN_VEHICLES)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_TEAM_RESPAWN
		IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_TEAM_RESPAWN) 
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_TEAM_RESPAWN) 
			ELSE
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_TEAM_RESPAWN)  
			ENDIF
		ELSE
			CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_TEAM_RESPAWN) 
		ENDIF
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_BOUNDS
		FLOAT fVal = TO_FLOAT(iChange)/2
							
		IF g_FMMC_STRUCT.fDeathmatchBounds + fVal < ciMIN_DM_RADIUS
			IF iChange < 0
				g_FMMC_STRUCT.fDeathmatchBounds = 0
			ELSE
				g_FMMC_STRUCT.fDeathmatchBounds	 = ciMIN_DM_RADIUS
			ENDIF
		ELSE
			g_FMMC_STRUCT.fDeathmatchBounds += fVal
		
			g_FMMC_STRUCT.fDeathmatchBounds	 = CLAMP(g_FMMC_STRUCT.fDeathmatchBounds, ciMIN_DM_RADIUS, 100)
		ENDIF
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CONTACT_CHAR
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iContactChar, iChange, OPTION_MISSION_MAX_CONTACT_CHARS)
		g_FMMC_STRUCT.iContactCharEnum = Get_Hash_Of_enumCharacterList_Contact(CONVERT_MENU_OPTION_TO_CHARACTER_ENUM(g_FMMC_STRUCT.iContactChar))
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_VOTING
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendstage, g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_PSEUDORACE_START_CORONA
		TOGGLE_MENU_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciMISSION_IS_PSEUDORACE)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_HIDE_SCORES
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Scores_For_DM)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_SPAWN_IN_CORONA_VEH
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DM_SpawnInCoronaVehicle)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_ALLOW_PERSONAL_VEHICLES
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciUSE_PERSONAL_VEHICLES_FOR_TEAM_VEHICLE)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_ENABLE_DM_MANUAL_RESPAWN
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Manual_Respawn_For_Any_DM)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DISABLE_DM_OUT_OF_VEH_RESPAWN
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_DM_Disable_Respawn_When_Out_Of_Veh)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_FORCE_RESP_ON_BROKEN_VEH
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Force_Manual_Respawn_For_Undriveable_Veh_DM)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_FIX_SUICIES_FOR_FLOW_PLAY
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Treat_Suicide_As_Kill)		
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_ENABLE_VEH_HEALTHBAR_OVERHEAD
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_OVERHEAD_HEALTH)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_ENABLE_VEH_HEALTHBAR_OVERHEAD_LOCAL_PLAYER
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_ShowVehicleHealthOverheadForLocalPlayer)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_LTS_STYLE_LDB_ORDERING
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_EXTRA_VEHICLE_COLLISION_DMG
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_EnableExtraCollisionDamageForVehicles)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_EXPLODE_AT_ZERO_HEALTH
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_ExplodeAtZeroBodyHealth)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_REMOVE_TIME_LIMIT
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_RemoveTimeLimitForDM)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_AUDIO_SCORE
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iNewAudioScore, iChange, ci_DLC_PACK_AUDIO_TRACK_MAX, -1)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_EXPLOSION_RESISTANT_VEHICLES
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_PlayerVehiclesExplosionResistant)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_SCORE_TO_KILLS_ON_LDB
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_DM_SCORE_TO_KILLS_ON_LDB)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_MODE_TYPE
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iAdversaryModeType, iChange, ciNEWVS_MAX_ADVERSARY_MODES, 0)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_DISABLE_DEFAULT_HELP_TEXT
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_DM_Disable_Default_HelpText)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_BULLETPROOF_TYRES
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_DM_Enable_Bulletproof_Tyres)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_MONSTER_INCREASED_CRUSH_DAMAGE
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Monster3IncreasedCrushDamage)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_SHOW_FIRSTPERSON_VEHICLE_HEALTHBAR
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_ShowVehicleHealthinFP)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_DISABLE_SNACKS_AND_ARMOUR
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_DisableSnacksAndArmour)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_LOCAL_PLAYER_ARROW_TEAM_COLOUR
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Local_Player_Arrow_Team_Colour)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_PROOFS_INVALIDATED_ON_FOOT
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_ProofsInvalidatedOnFoot)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_TEAM_NAME_FROM_COLOUR
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciTEAM_COLOUR_NAMES)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_CLEANUP_EMPTY_VEHICLES
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciCLEAR_UP_VEHICLE_ON_GAME_LEAVE)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_CLEAN_UP_LEAVERS_ON_MISSION
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_CLEAN_UP_LEAVERS_FOR_MISSION)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_ENABLE_VEHICLE_AMMO
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_EnableArenaVehicleAmmo)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_DEATHMATCH_DM_VEHICLE_WEAPON_FIRE_DRAIN_TIME
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_EnableArenaVehicleAmmo)
			FLOAT fChange = TO_FLOAT(iChange)/10.0
			EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, g_FMMC_STRUCT.fVehicleWeaponFireDrainTime, fChange, 5.0, 1.0)
		ENDIF	
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_321GO_COUNTDOWN
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DM_321GO_Countdown)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_ENABLE_KILL_WHEN_OFF_VEH
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_KillPlayerOutOfVehicle)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_MAX_TEAM_DIFFERENCE
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iMaxTeamDifference, iChange, 100)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_PLAYERS_REMAINING
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_ArenaPlayersRemaining)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_GIVE_PLAYER_PARACHUTE
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_GiveParachute)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DISABLE_VEHICLE_EXIT
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisableGTARaceVehicleExit)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_DISABLE_NAVMESH_SPAWNING
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DisableNavMeshSpawnFallback)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_DISABLE_DM_OBJECTIVES
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DisableDMObjectives)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_CUSTOM_TDM_COLOURS
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_UseCustomTDMPlayerBlipColours)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_DISABLE_POWER_PLAYS
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DisableDMPowerPlays)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_DISABLE_PI_KILL_YOURSELF
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisablePIMenuKillYourself)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_DM_DISABLE_CASH_DROP
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisableCashDrop)
	ENDIF
ENDPROC

PROC EDIT_PTB_OPTIONS_MENU(INT iChange)
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_ENABLE_TAGGING
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_DM_TAGGED_EXPLOSION_TIMER)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_DISPLAY_BOMB_CARRIER
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisplayBombCarrier)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_NITRO_BOMB_CARRIER
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_GiveBombCarrierNitro)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_DISABLE_BOMB_SOUNDS
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisableBombSounds)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_BOMB_CARRIER_NITRO_LEVEL
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iPassTheBomb_CarrierNitroLevel, iChange, 4)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_NON_BOMB_CARRIER_SPEED
		EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, g_FMMC_STRUCT.fPassTheBomb_DragForNonCarriers, TO_FLOAT(iChange) * 0.05, 10, 1.0)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_TAG_RETURN_COOLDOWN
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iTagReturnCooldown, iChange, ciDM_MAX_TYPE_DM_TAG_RETURN_COOLDOWN)	
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_PTBOPTIONS_VFX_CROSS_THE_LINE
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_DM_VFX_CROSS_THE_LINE)
	ENDIF
ENDPROC

PROC SET_MISSION_CORONA_DEV_OPTIONS()
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_TARGET_SCORE
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Target_Score)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_ONLINE_IDS
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Online_ID)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_OPPONENT_HEALTH_BAR
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Opponent_Health_Bar)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_TRAFFIC
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Traffic)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_LOCK_WEAPONS
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Lock_Weapons)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_FORCED_WEAPONS
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Forced_Weapons)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_HIDE_TIMELIMIT
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_HideCoronaTimeLimit)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_HIDE_WEATHER
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Hide_Corona_Option_Weather)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DEATHMATCH_CORONA_ARMOUR
		EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciREMOVE_ARMOUR_PURCHASE_OPTION)
	ENDIF
ENDPROC

PROC SET_DM_SHRINKING_BOUNDS_OPTIONS_MENU(INT iChangeValue)
	
	INT iBoundsIndex = g_FMMC_STRUCT.iDefaultDeathmatchBoundsIndex
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DM_SHRINKING_BOUNDS_SHRINK_TO
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iShrinkToPercent, iChangeValue, 101, 0, DEFAULT, TRUE, TRUE)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DM_SHRINKING_BOUNDS_SHRINK_TIME
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iShrinkTime, iChangeValue, 3601, 0, DEFAULT, TRUE, TRUE)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DM_SHRINKING_BOUNDS_SHRINK_DELAY_CONDITION
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iShrinkDelayCondition, iChangeValue, 2, 0, DEFAULT, TRUE, TRUE)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DM_SHRINKING_BOUNDS_TIME_SHRINK_DELAY
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sDeathmatchBoundsInfo[iBoundsIndex].iShrinkDelay, iChangeValue, 3601, 0, DEFAULT, TRUE, TRUE)
	ENDIF
	
ENDPROC

PROC EDIT_MENU_OPTIONS(INT iChangeValue)

	INT iType = -1
	INT iMaximum
	
	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		sFMMCendStage.bMajorEditOnLoadedMission = TRUE
		EDIT_MISSION_DETAILS_MENU(sFMMCmenu, sFMMCendStage, iChangeValue > 0, GET_CREATOR_MENU_SELECTION(sFMMCmenu), sFMMCmenu.iOptionsMenuBitSet)	
	
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TAG_TIMERS
		INT iTimerSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
		g_FMMC_STRUCT.iTagExplosionTimer[iTimerSelection] += iChangeValue
		CAP_FMMC_MENU_ITEM(DM_TAG_EXPLOSION_TIMER_MAX,	g_FMMC_STRUCT.iTagExplosionTimer[iTimerSelection], DM_TAG_EXPLOSION_TIMER_OFF)
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_DEV_OPTIONS
		IF FMMC_IS_ROCKSTAR_DEV()
			EDIT_DEV_OPTIONS_MENU(iChangeValue)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_IPL_OPTIONS
		IF FMMC_IS_ROCKSTAR_DEV()
			EDIT_DEATHMATCH_IPL_MENU(sFMMCMenu, sFMMCendStage, iChangeValue)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = efmmc_IPL_ISLAND
		EDIT_ISLAND_IPL_OPTIONS_MENU(sFMMCmenu, sFMMCendStage)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_LIVES
		EDIT_DM_LIVES_OPTIONS(sFMMCMenu, sFMMCendStage, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_DETAILS
		EDIT_DM_DETAILS_MENU(sFMMCMenu, sFMMCendStage, iChangeValue)		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_BALANCING
		EDIT_DM_TEAM_BALANCING_MENU(sFMMCMenu, sFMMCendStage, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_POWERUPS
		EDIT_POWERUPS_OPTIONS(sFMMCMenu, sFMMCendStage, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_POWERUPS_RANDOM
		EDIT_POWERUP_RANDOM_OPTIONS(sFMMCMenu, sFMMCendStage, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_POWERUPS_BOOST_SHUNT
		EDIT_POWERUP_BOOST_SHUNT_OPTIONS(sFMMCMenu, sFMMCendStage, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_VEH_MG
		EDIT_FMMC_VEHICLE_MACHINE_GUN_OPTIONS(sFMMCMenu, sFMMCendStage, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_DEV_PICKUP_OPTIONS
		EDIT_DM_PICKUP_OPTIONS(sFMMCMenu, sFMMCendStage, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_WEAPON_MODIFIERS
		EDIT_DM_WEAPON_DAMAGE_MODIFIER_MENU(sFMMCMenu, sFMMCendStage, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_PTB_OPTIONS
		EDIT_PTB_OPTIONS_MENU(iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS
		EDIT_WORLD_PROPS_BASE_MENU(sFMMCendStage, sFMMCMenu, iChangeValue)	
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_Helptext
		IF FMMC_IS_ROCKSTAR_DEV()
			EDIT_DM_HELPTEXT_MENU(iChangeValue)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_DEV_CORONA
		IF FMMC_IS_ROCKSTAR_DEV()
			SET_MISSION_CORONA_DEV_OPTIONS()
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_CoronaModOptions
		EDIT_DISABLE_CORONA_MOD_OPTIONS_MENU(sFMMCmenu)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEST_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
		AND NOT CONTENT_IS_USING_ARENA()
			EDIT_CREATOR_BOOL_MENU_ITEM(sFMMCendStage, sFMMCmenu.bPlayerInvincibleTest)
			VEHICLE_INDEX viPlayerVeh
			
			IF sFMMCmenu.bPlayerInvincibleTest = FALSE
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
				SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)	
			ELSE
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
				SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				viPlayerVeh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
				IF sFMMCmenu.bPlayerInvincibleTest = FALSE
					SET_ENTITY_INVINCIBLE(viPlayerVeh, FALSE)
					SET_ENTITY_PROOFS(viPlayerVeh, FALSE, FALSE, FALSE, FALSE, FALSE)	
				ELSE
					SET_ENTITY_INVINCIBLE(viPlayerVeh, TRUE)
					SET_ENTITY_PROOFS(viPlayerVeh, TRUE, TRUE, TRUE, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_TEST
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iTestMyTeam, iChangeValue, g_FMMC_STRUCT.iMaxNumberOfTeams)
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iTestNumberOfTeams, iChangeValue, g_FMMC_STRUCT.iMaxNumberOfTeams+1, 1)
		IF g_FMMC_STRUCT.iTestNumberOfTeams <= g_FMMC_STRUCT.iTestMyTeam
			g_FMMC_STRUCT.iTestNumberOfTeams = g_FMMC_STRUCT.iTestMyTeam+1
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_COVER_POINT_BASE
		IF FMMC_IS_ROCKSTAR_DEV()
			SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
				CASE ciCOVER_COVER_TYPE	
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCoverType, iChangeValue, 4)
				BREAK
				CASE ciCOVER_COVER_HEIGHT
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCoverType, iChangeValue*2, 3, 0)
				BREAK
			ENDSWITCH
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
		MODEL_NAMES vehModel = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
		SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
			CASE VEHICLE_BASIC_LIBRARY
				EDIT_CREATOR_VEHICLE_LIBRARY_MENU_ITEM(sFMMCMenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChangeValue)
				RESET_CUR_VEH_MODS(sFMMCMenu)
			BREAK
			CASE VEHICLE_BASIC_TYPE
				PRINTLN("sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_COLOUR] = ", sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_COLOUR])
				EDIT_CREATOR_VEHICLE_TYPE_MENU_ITEM(sFMMCMenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChangeValue)
				CHANGE_VEHICLE_COLOUR(sFMMCMenu, sVehStruct, sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_COLOUR], FALSE)
				RESET_CUR_VEH_MODS(sFMMCMenu)
			BREAK
			CASE VEHICLE_BASIC_COLOUR
				IF NOT ARE_STRINGS_EQUAL(sFMMCMenu.sSubTypeName[CREATION_TYPE_VEHICLE_COLOUR], "FMMC_COL_DEF")	
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_COLOUR], iChangeValue, ciNUMBER_OF_VEHICLE_COLOURS_PUBLIC, -1)
					CHANGE_VEHICLE_COLOUR(sFMMCMenu, sVehStruct, sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_COLOUR])
				ENDIF
			BREAK
			CASE VEHICLE_BASIC_LIVERY
				IF IS_LIVERY_AVAILABLE(vehModel)
					IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
						CHANGE_VEHICLE_LIVERY(sFMMCMenu, vehModel, sVehStruct.viCoronaVeh,iChangeValue)
					ENDIF
				ENDIF
			BREAK
			CASE VEHICLE_BASIC_HEALTH
				IF(iChangeValue > 0 AND sFMMCmenu.iVehicleHealth >= 100)
				OR 	(iChangeValue < 0 AND sFMMCmenu.iVehicleHealth > 100)
					iChangeValue *= 25
				ELSE
					iChangeValue *= 5
				ENDIF
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iVehicleHealth, iChangeValue, 251, 25)
			BREAK
			CASE VEHICLE_BASIC_BULLETPROOF_TYRES
				IF CAN_CURRENT_VEHICLE_HAVE_BULLETPROOF_TYRES(sFMMCMenu)
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iVehBitsetSeven,  ciFMMC_VEHICLE7_BULLETPROOF_TYRES)
				ENDIF
			BREAK
			CASE VEHICLE_BASIC_CAN_RESPAWN
				IF NOT DOING_HILL_VEHICLE_CREATION(sFMMCmenu)
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iVehBitsetEight,  ciFMMC_VEHICLE8_DM_VEHICLES_CAN_RESPAWN)
				ENDIF
			BREAK
			CASE VEHICLE_BASIC_CYCLE
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles)
			BREAK
		ENDSWITCH
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_RESPAWN_VEHICLE_MENU
	
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iRespawnVehicleLibrary, sFMMCMenu.iRespawnVehicleType))
	
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = 0
			EDIT_CREATOR_VEHICLE_LIBRARY_MENU_ITEM(sFMMCMenu, sFMMCmenu.iRespawnVehicleLibrary, sFMMCMenu.iRespawnVehicleType, iChangeValue, TRUE)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = 1
			EDIT_CREATOR_VEHICLE_TYPE_MENU_ITEM(sFMMCMenu, sFMMCmenu.iRespawnVehicleLibrary, sFMMCMenu.iRespawnVehicleType, iChangeValue, TRUE)
		ENDIF
		
	ELIF sFMMCmenu.sActiveMenu = eFMMC_VEHICLE_WEAPON_MODS
		EDIT_VEHICLE_WEAPON_MOD_MENU(sFMMCendStage, sFMMCmenu, iChangeValue)
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_RESTRICTION_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < g_FMMC_STRUCT.iMaxNumberOfTeams
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iVehBitsetTwo,  GET_CREATOR_MENU_SELECTION(sFMMCmenu)+2)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_MAX_TEAMS
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iVehBitsetEight,  ciFMMC_VEHICLE8_RESTRICT_FOR_ALL)
		ENDIF
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
		MODEL_NAMES mnTemp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_LIBRARY
			CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
			CHANGE_PROP_LIBRARY(sFMMCmenu, iChangeValue)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_CREATE
			CHANGE_PROP_TYPE(sFMMCmenu, iChangeValue)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SET_INVISIBLE
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropBitSet, ciFMMC_PROP_SetInvisible)
		ENDIF
		IF NOT IS_STUNT_LIBRARY(sFMMCMenu.iPropLibrary)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
				IF (NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround) OR FMMC_IS_ROCKSTAR_DEV())
				AND IS_PROP_SUITABLE_FOR_ROTATION(mnTemp,sFMMCMenu.bPropSnapped)
					CHANGE_FREE_ROTATION_AXIS(sFMMCmenu, iChangeValue, sPropStruct.viCoronaObj)
					PRINTLN("[LH] CHANGE_FREE_ROTATION_AXIS")
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_STACK)
					REFRESH_MENU(sFMMCmenu)
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SNAPPING
			AND (FMMC_IS_ROCKSTAR_DEV() AND sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_ACTIVE)
				ENSURE_A_SNAP_MODE_IS_ACTIVE(sFMMCmenu)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForSnappingOptions
			AND (sFMMCMenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
				IF FMMC_IS_ROCKSTAR_DEV()
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_ACTIVE)
					ENSURE_A_SNAP_MODE_IS_ACTIVE(sFMMCmenu)
				ENDIF				
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForSnappingEntity
			AND (sFMMCMenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
				IF FMMC_IS_ROCKSTAR_DEV()
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropSnapParentEntity, iChangeValue, GET_NUMBER_OF_PROPS_IN_SAVED_TEMPLATE(sFMMCmenu.iProptype), -1)
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePad
				EDIT_PRESSUREPAD_TRAP_SELECTION_MENU_ITEM(sFMMCendStage, sFMMCMenu, iChangeValue)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePadDelay
			AND sFMMCMenu.iObjectPressurePadIndex >= 0
				EDIT_TRAP_PRESSUREPAD_DELAY_MENU_ITEM(sFMMCMenu, sFMMCendStage, iChangeValue)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropDisplayIndexes_MenuPos
				IF FMMC_IS_ROCKSTAR_DEV()
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iCreatorInfoBitset, ciCIB_DRAW_PROP_NUMBERS)
				ENDIF
				
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
			AND NOT ARE_STUNT_PROPS_AND_FEATURES_ALLOWED()
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
			AND ARE_STUNT_PROPS_AND_FEATURES_ALLOWED()
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
			ENDIF
		ELSE
			MODEL_NAMES mnProp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_COLOUR AND NOT g_sMPTunables.bDisable_Prop_Colour
				IF sFMMCMenu.iPropLibrary = PROP_LIBRARY_STUNT_NEON_TUBE
					CHANGE_PROP_TYPE(sFMMCmenu, iChangeValue*PROP_LIBRARY_STUNT_NEON_TUBE_TYPES)
					REFRESH_MENU(sFMMCmenu)
				ELSE
					INT i
					
					REPEAT MAX_COLOUR_PALETTE i
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColour, iChangeValue, MAX_COLOUR_PALETTE)
						
						IF IS_PROP_COLOUR_VALID(mnProp, sFMMCmenu.iPropColour)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
				IF (NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround) OR FMMC_IS_ROCKSTAR_DEV())
				AND IS_PROP_SUITABLE_FOR_ROTATION(mnTemp,sFMMCMenu.bPropSnapped)
					CHANGE_FREE_ROTATION_AXIS(sFMMCmenu, iChangeValue, sPropStruct.viCoronaObj)
					PRINTLN("[LH] CHANGE_FREE_ROTATION_AXIS")
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_STACK)
					REFRESH_MENU(sFMMCmenu)
				ENDIF			
			ELIF IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)
			OR IS_SNAPPABLE_STUNT_PROP(mnProp)
			OR (FMMC_IS_ROCKSTAR_DEV() AND sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SNAPPING
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_ACTIVE)
					ENSURE_A_SNAP_MODE_IS_ACTIVE(sFMMCmenu)
				ENDIF
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_STUNT_SPECIAL
				IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_FLARE_01"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_01"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_03"))
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_PLAYERS AND NOT g_sMPTunables.bDisable_Fireworks_players_to_trigger_for
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet,  ciFMMC_PROP_TriggerForAllRacers)
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_LAPS AND NOT g_sMPTunables.bDisable_Fireworks_Laps_to_Trigger_on
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet,  ciFMMC_PROP_TriggerOnEachLap)
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_RADIUS
						EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.fPTFXTriggerRadius, TO_FLOAT(iChangeValue*1),100.0)
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_SUB_TYPE
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropEffectSubType, iChangeValue, ciSTUNT_FIREWORK_VFX_MAX, -1)
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_COLOUR
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColouring, iChangeValue, ciHUD_COLOUR_MAX -1)
					ENDIF
					
				ELIF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_tannoy"))
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iAlarmTypePos
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iAlarmSound, iChangeValue, 12)
						DEAL_WITH_CHANGING_STUNT_ALARM_PREVIEW(sFMMCmenu, TRUE)
					ENDIF	
				ENDIF
			ENDIF
			IF IS_SPEED_BOOST_PROP(mnProp)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iSpeedBoostPropMenuPosition
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSpeedBoostAmount, iChangeValue, 6, 1, TRUE)
				ENDIF		
			ENDIF
			IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown"))
			OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T1"))
			OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T2")) 
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iSpeedBoostPropMenuPosition
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSpeedBoostAmount, iChangeValue, 4, 1, TRUE)
				ENDIF		
			ENDIF			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
			ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePad
				EDIT_PRESSUREPAD_TRAP_SELECTION_MENU_ITEM(sFMMCendStage, sFMMCMenu, iChangeValue)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePadDelay
			AND sFMMCMenu.iObjectPressurePadIndex >= 0
				EDIT_TRAP_PRESSUREPAD_DELAY_MENU_ITEM(sFMMCMenu, sFMMCendStage, iChangeValue)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropTurretTower_MenuPos
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet2,  ciFMMC_PROP2_UseAsTurret)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropTurretTower_Heading
				EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCMenu.fTurretHeading, TO_FLOAT(iChangeValue)*5, 365.0, 0.0)
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
		IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3 AND (IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary) OR IS_CURRENT_PROP_LIBRARY_AN_ARENA_LIBRARY(sFMMCMenu)))   //Lock Delete full menu
		OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2 AND NOT (IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary) OR IS_CURRENT_PROP_LIBRARY_AN_ARENA_LIBRARY(sFMMCMenu))) //Lock Delete smaller menu
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropBitSet, ciFMMC_PROP_Lock_Delete)
		ENDIF
		
	ELIF sFMMCmenu.sActiveMenu = eFMMC_GENERIC_OVERRIDE_POSITION
		SWITCH sFMMCmenu.iEntityCreation
			CASE CREATION_TYPE_PROPS
				EDIT_GENERIC_OVERRIDE_POSITION_MENU(sFMMCmenu, sFMMCendStage, CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu), sFMMCMenu.iPropBitset2, ciFMMC_PROP2_Clamp_X_Override_Value, ciFMMC_PROP2_Clamp_Y_Override_Value, ciFMMC_PROP2_Clamp_Z_Override_Value)
			BREAK
			DEFAULT
				INT iDummyBS
				EDIT_GENERIC_OVERRIDE_POSITION_MENU(sFMMCmenu, sFMMCendStage, CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu), iDummyBS)
			BREAK
		ENDSWITCH
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION
		EDIT_GENERIC_OVERRIDE_ROTATION_MENU(sFMMCmenu, sFMMCendStage, CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu))
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_PROXIMITY
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_PROXIMITY)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_TRIGGERED
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_TRIGGERED)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_CHAIN
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_CHAIN)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_OVERRIDE
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_OVERRIDES)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_ANGLE
			//Sets the angle to 0 first if it's currently -1
			IF sFMMCmenu.iCustomSnapRotationAngle = -1
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCustomSnapRotationAngle, 1, 0, 0, FALSE)
			ENDIF
			
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCustomSnapRotationAngle, iChangeValue * ciFMMC_SnapRotationAngleIncrement, 270, 0, FALSE)
			
			//Sets the angle to -1 if it's been set to 0
			IF sFMMCmenu.iCustomSnapRotationAngle = 0
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCustomSnapRotationAngle, -1, -1, -1, FALSE)
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFMMC_OFFSET_POSITION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_USE_OFFSET
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_POSITION_OFFSET)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_FREE_CAMERA
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_FREE_CAMERA)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_LOCK_POSITION
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_LOCK_POSITION)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFMMC_OFFSET_ROTATION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_USE_OFFSET
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_ROTATION_OFFSET)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_FREE_CAMERA
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_USE_FREE_CAMERA)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_LOCK_POSITION
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_LOCK_ROTATION)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
		MODEL_NAMES mnProp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_CATEGORY
			CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
			CHANGE_PROP_LIBRARY(sFMMCmenu, iChangeValue)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_TYPE
			CHANGE_PROP_TYPE(sFMMCmenu, iChangeValue)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_ROTATION_TYPE
			IF (NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround) OR FMMC_IS_ROCKSTAR_DEV())
			AND IS_PROP_SUITABLE_FOR_ROTATION(mnProp,sFMMCMenu.bPropSnapped)
				CHANGE_FREE_ROTATION_AXIS(sFMMCmenu, iChangeValue, sDynoPropStruct.viCoronaObj)
				PRINTLN("[LH] CHANGE_FREE_ROTATION_AXIS (Dynoprop)")
			ENDIF
			
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_AMMO AND GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType) = PROP_CONTAINER_LD_PU
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCratesAmmo, iChangeValue, 3)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_COLOUR AND DOES_PROP_HAVE_EXTRA_COLOURS(mnProp)
			INT i
			
			REPEAT MAX_COLOUR_PALETTE i
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColour, iChangeValue, MAX_COLOUR_PALETTE)
				
				IF IS_PROP_COLOUR_VALID(mnProp, sFMMCmenu.iPropColour)
					BREAKLOOP
				ENDIF
			ENDREPEAT
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = (sFMMCMenu.iCurrentMenuLength - 1)
			DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps)
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iTrapProximityOptionIndex
			EDIT_TRAP_PROXIMITY_SETTING_MENU_ITEM(sFMMCmenu, sFMMCendStage, iChangeValue)
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iTrapRespawnTimeIndex
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCMenu.iTrapRespawnTime, iChangeValue, 30, -1)
		ENDIF
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE
		FLOAT fChange
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_LIBRARY
			iType = CREATION_TYPE_WEAPON_LIBRARY
			sFMMCmenu.iCurrentEntitySelection[iType] = sFMMCmenu.iWepLibrary
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_TYPE
			iType = CREATION_TYPE_WEAPONS
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = PICKUP_MENU_VEHICLE_WEAPON_PICKUP_TYPE
			IF ARE_STRINGS_EQUAL(sFMMCMenu.sSubTypeName[CREATION_TYPE_WEAPONS], "WEP_VEH_PICKUP")
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCMenu.iVehicleWeaponPickupType, iChangeValue, ciVEH_WEP_MAX, -1)
				sWepStruct.iVehicleWeaponPickupType = sFMMCMenu.iVehicleWeaponPickupType
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_CLIP_OR_BULLET
			IF sFMMCMenu.iWepLibrary = FMMC_WEAPON_LIBRARY_THROWN
				EXIT
			ENDIF
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iWepPlacedBitset, ciFMMC_WEP_DM_UseBulletsForAmmo)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_AMMO
			iType = CREATION_TYPE_WEAPON_AMMO
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_AMMO_PICKUP_ALL_WEAPONS
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iWepPlacedBitset, ciFMMC_WEP_DM_CustomAmmoAllWeapons)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_CUSTOM_PICKUP_TYPE	
			IF ARE_STRINGS_EQUAL(sFMMCMenu.sSubTypeName[CREATION_TYPE_WEAPONS], "WEP_CUST_PICKUP")
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT_ENTITIES.sEditedWeapon.iCustomPickupType, iChangeValue, ciCUSTOM_PICKUP_TYPE__MAX, ciCUSTOM_PICKUP_TYPE__NONE)
			ELSE
				g_FMMC_STRUCT_ENTITIES.sEditedWeapon.iCustomPickupType = ciCUSTOM_PICKUP_TYPE__NONE
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_BLIP_RANGE
			ADD_CHANGE_VALUE_MODIFIER(iChangeValue, ROUND(sFMMCMenu.sEntityBlipStruct.fBlipRange), 10, 5.0)
			ADD_CHANGE_VALUE_MODIFIER(iChangeValue, ROUND(sFMMCMenu.sEntityBlipStruct.fBlipRange), 100, 25.0)
			ADD_CHANGE_VALUE_MODIFIER(iChangeValue, ROUND(sFMMCMenu.sEntityBlipStruct.fBlipRange), 500, 50.0)
			fChange = TO_FLOAT(iChangeValue)
			EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCMenu.sEntityBlipStruct.fBlipRange, fChange, 1000)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_BLIP_HEIGHT_RANGE
			ADD_CHANGE_VALUE_MODIFIER(iChangeValue, ROUND(sFMMCMenu.sEntityBlipStruct.fBlipHeightDifference), 10, 5.0)
			ADD_CHANGE_VALUE_MODIFIER(iChangeValue, ROUND(sFMMCMenu.sEntityBlipStruct.fBlipHeightDifference), 100, 25.0)
			ADD_CHANGE_VALUE_MODIFIER(iChangeValue, ROUND(sFMMCMenu.sEntityBlipStruct.fBlipHeightDifference), 500, 50.0)
			fChange = TO_FLOAT(iChangeValue)
			EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCMenu.sEntityBlipStruct.fBlipHeightDifference, fChange, 1000)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_VEHICLE_PU
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendstage, sFMMCmenu.iWepPlacedBitset, ciFMMC_WEP_Can_Pickup_in_vehicle)
		ELIF NOT FMMC_IS_ROCKSTAR_DEV() //once ammo & restrictions work, make this available to public
			DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
		
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_AMMO_TYPE
			IF IS_WEAPON_TYPE_MK2(sFMMCMenu.wtCurrentWeapon)
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iWepAmmoType, iChangeValue, WEAPON_MENU_AMMO_TYPE_MAX, 0)
			ELSE
				sFMMCmenu.iWepAmmoType = WEAPON_MENU_AMMO_TYPE_DEFAULT
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_DAMAGE_MULTIPLIER
			EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.fWepDamageMultiplier, TO_FLOAT(iChangeValue) / 10, 10.0, 0.0) 
			
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_PLAY_RESPAWN_SOUND
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendstage, sFMMCmenu.iWepPlacedBitset, ciFMMC_WEP_PlayRespawnSound)
			
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_RESTRICTIONS_TEAM1
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_RESTRICTIONS_TEAM2
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_RESTRICTIONS_TEAM3
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_RESTRICTIONS_TEAM4
			iType = CREATION_TYPE_WEAPON_REST
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_DM_SPAWN_ON_TIMER
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iWepPlacedBitset, ciFMMC_WEP_DM_SpawnOnTimer)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_DM_TIMER_LENGTH
			IF IS_BIT_SET(sFMMCMenu.iWepPlacedBitset, ciFMMC_WEP_DM_SpawnOnTimer)
				ADD_CHANGE_VALUE_MODIFIER(iChangeValue, g_FMMC_STRUCT.iSpawnPickupsAtStartTimer, 20, 5.0)
				ADD_CHANGE_VALUE_MODIFIER(iChangeValue, g_FMMC_STRUCT.iSpawnPickupsAtStartTimer, 60, 10.0)
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iSpawnPickupsAtStartTimer, iChangeValue, 301)
			ENDIF
		ELSE
			DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS
		SET_KOTH_HILL_OPTIONS_MENU(iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_KotH_BOUNDS
		SET_KOTH_BOUNDS_OPTIONS_MENU(iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = efmmc_DM_SHRINKING_BOUNDS
		SET_DM_SHRINKING_BOUNDS_OPTIONS_MENU(iChangeValue)
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_RANDOMIZER
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_SPAWN], iChangeValue, 4)
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				IF sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS] = 0
				AND sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_SPAWN] = 0
					sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_SPAWN] = 1
				ENDIF
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
				IF sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS] = 0
				AND sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_SPAWN] = 0
					sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_SPAWN] = 3
				ENDIF
			ENDIF
			CHANGE_SPAWN_RANDOMIZER_LEVEL(sFMMCmenu, sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_SPAWN])
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS], iChangeValue, 4)
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				IF sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_SPAWN] = 0
				AND sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS] = 0
					sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS] = 1
				ENDIF
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
				IF sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_SPAWN] = 0
				AND sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS] = 0
					sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS] = 3
				ENDIF
			ENDIF
			CHANGE_WEAPON_RANDOMIZER_LEVEL(sFMMCmenu, sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS])
		ENDIF
		
		IF sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_SPAWN] = 0
		AND sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS] = 0
			sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS] = 1
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
		EDIT_PHOTO_CAM_BASE_MENU(sFMMCmenu, sFMMCendStage)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_ZONES_BASE
		EDIT_ZONE_BASE_MENU(sFMMCMenu, sCurrentVarsStruct, sFMMCendStage, iChangeValue)
	
	ELIF sFMMCmenu.sActiveMenu = eFMMC_ZONE_TYPE_SPECIFIC_OPTIONS
		EDIT_ZONE_TYPE_SPECIFIC_OPTIONS_MENU(sFMMCMenu, sFMMCendStage, iChangeValue)
		
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SPAWNPOINTS_CYCLE
			DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints)
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= SPAWNPOINTS_TEAM1
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) <= SPAWNPOINTS_TEAM4
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iDMSpawnTeamRestrictedBS, GET_CREATOR_MENU_SELECTION(sFMMCmenu) - SPAWNPOINTS_TEAM1)
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT_MODIFIER_RESTRICTED
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= 0
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) < ciDM_MAX_MODIFIER_SETS
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iDMSpawnModifierRestrictedBS, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_WANTED_OPTION
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iTeamWanted[GET_CREATOR_MENU_SELECTION(sFMMCmenu)], iChangeValue, 7, 1, TRUE, TRUE)	
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_NAME_OPTION
		EDIT_CREATOR_TEAM_NAME_MENU_ITEM(sFMMCmenu, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_START_WEAPON_OPTION
		CHANGE_TEAM_FORCED_WEAPON(sFMMCmenu, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = eFMMC_AVAILABLE_VEHICLE_LIBRARIES
		INT i
			
		BOOL bMissionVehicleOptions = FALSE
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CORONA_VEHICLE_LIST_TEAM
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSelectedTeam, iChangeValue, g_FMMC_STRUCT.iMaxNumberOfTeams)
		ENDIF
		
		FOR i = 0 TO VEHICLE_LIBRARY_MAX - 1
			IF g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][i][0] > 0
			OR g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][i][1] > 0
				bMissionVehicleOptions = TRUE
			ENDIF
		ENDFOR
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CORONA_VEHICLE_LIST_USE_VEHICLE_LIST
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sFMMCEndConditions[sFMMCMenu.iSelectedTeam].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CORONA_VEHICLE_LIST_DEFAULT_VEHICLE_TYPE
			IF bMissionVehicleOptions
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam], iChangeValue, VEHICLE_LIBRARY_MAX)
				
				FOR i = 0 TO VEHICLE_LIBRARY_MAX - 1
					IF g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][0] = 0
					AND g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][1] = 0
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam], iChangeValue, VEHICLE_LIBRARY_MAX)
					ELSE
						BREAKLOOP
					ENDIF
				ENDFOR
			ENDIF
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CORONA_VEHICLE_LIST_DEFAULT_VEHICLE
			IF bMissionVehicleOptions
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicle[sFMMCmenu.iSelectedTeam], iChangeValue, GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]))
				
				FOR i = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]) - 1
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][GET_LONG_BITSET_INDEX(g_FMMC_STRUCT.iDefaultCommonVehicle[sFMMCmenu.iSelectedTeam])], GET_LONG_BITSET_BIT(g_FMMC_STRUCT.iDefaultCommonVehicle[sFMMCmenu.iSelectedTeam]))
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicle[sFMMCmenu.iSelectedTeam], iChangeValue, GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]))
					ELSE
						BREAKLOOP
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFMMC_DEFAULT_VEHICLE_LIBRARIES
		INT iSelectedLibrary = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
		IF iSelectedLibrary >= 0 AND iSelectedLibrary < VEHICLE_LIBRARY_MAX
			
			INT iLoop
			FOR iLoop = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(iSelectedLibrary) - 1
				IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][iSelectedLibrary][GET_LONG_BITSET_INDEX(iLoop)], GET_LONG_BITSET_BIT(iLoop))
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicleForType[iSelectedLibrary], iChangeValue, GET_NUMBER_OF_VEHICLES_IN_LIBRARY(iSelectedLibrary))
					INT i
					FOR i = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(iSelectedLibrary) - 1
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][iSelectedLibrary][GET_LONG_BITSET_INDEX(g_FMMC_STRUCT.iDefaultCommonVehicleForType[iSelectedLibrary])], GET_LONG_BITSET_BIT(g_FMMC_STRUCT.iDefaultCommonVehicleForType[iSelectedLibrary]))
							EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicleForType[iSelectedLibrary], iChangeValue, GET_NUMBER_OF_VEHICLES_IN_LIBRARY(iSelectedLibrary))
						ELSE
							BREAKLOOP
						ENDIF
					ENDFOR
					BREAKLOOP
				ENDIF
			ENDFOR
			
		ENDIF
		
		//Set a bit for the corona if any of these options have been used
		INT iLoop
		INT iEmptyDefaults
		FOR iLoop = 0 TO VEHICLE_LIBRARY_MAX - 1
			IF g_FMMC_STRUCT.iDefaultCommonVehicleForType[iLoop] = 0
				iEmptyDefaults++
			ENDIF
		ENDFOR
		IF iEmptyDefaults < VEHICLE_LIBRARY_MAX
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_DEFAULT_VEHICLES_PER_TYPE)
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_DEFAULT_VEHICLES_PER_TYPE)
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_DEFAULT_VEHICLES_PER_TYPE)
				CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_DEFAULT_VEHICLES_PER_TYPE)
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			IF sFMMCMenu.iSelectedEntity != -1
				CANCEL_ENTITY_CREATION(CREATION_TYPE_TEAM_SPAWN_LOCATION)
				sFMMCMenu.iSelectedEntity 		= -1
			ENDIF
			INT i,j
			FOR i = 0 TO FMMC_MAX_TEAMS - 1
				sTeamSpawnStruct[i].iSwitchingINT = CREATION_STAGE_WAIT
				FOR j = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] - 1)
					CREATE_FMMC_BLIP(sTeamSpawnStruct[i].biPedBlip[j], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][j].vPos, HUD_COLOUR_WHITE, getNameForTeamSpawnBlip(i), 1)
					SET_BLIP_COLOUR(sTeamSpawnStruct[i].biPedBlip[j] , getColourForSpawnBlip(i))
				ENDFOR
			ENDFOR
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSelectedTeam, iChangeValue, g_FMMC_STRUCT.iMaxNumberOfTeams)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sFMMCmenu.iSelectedTeam] > 0
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChangeValue > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sFMMCmenu.iSelectedTeam], sFMMCmenu.iSelectedTeam)
			ENDIF
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_TOP
		EDIT_ARENA_TOP_MENU(sFMMCmenu, sFMMCendStage, iChangeValue)
		
	ELIF sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_MISC
		EDIT_ARENA_MISC_MENU(sFMMCMenu, sFMMCendStage)
	ELIF sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_SETTINGS
		EDIT_ARENA_OPTIONS_MENU(sFMMCMenu, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_RANDOM_LIGHTING
		EDIT_ARENA_RANDOM_LIGHTING_MENU(sFMMCMenu, iChangeValue)
	ELIF sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_EXTRAS	
		EDIT_ARENA_EXTRAS_MENU(sFMMCMenu)
	ELIF sFMMCmenu.sActiveMenu = eFMMC_Inventory_Single
		EDIT_SINGLE_INVENTORY_OPTIONS_MENU(sFMMCmenu, sFMMCendStage, iChangeValue, g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCmenu.iDMModifierIndex].sModifierInventory)
	ELIF sFMMCmenu.sActiveMenu = eFMMC_Inventory_Weapons
		EDIT_INVENTORY_WEAPON_OPTIONS_MENU(sFMMCMenu, sFMMCendStage, iChangeValue, g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCmenu.iDMModifierIndex].sModifierInventory, sWepStruct)
	ELIF sFMMCmenu.sActiveMenu = eFMMC_Inventory_Component_List
		EDIT_INVENTORY_WEAPON_COMPONENTS_OPTIONS_MENU(sFMMCMenu, g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCmenu.iDMModifierIndex].sModifierInventory)
	ELIF sFMMCmenu.sActiveMenu = eFMMC_Inventory_Parachutes
		EDIT_INVENTORY_PARACHUTE_OPTIONS_MENU(sFMMCMenu, sFMMCendStage, iChangeValue, g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCmenu.iDMModifierIndex].sModifierInventory)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_COMPONENTS
		EDIT_PICKUP_COMPONENT_MENU(sFMMCmenu)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_SHARED_AMBIENT_OPTIONS
		EDIT_SHARED_AMBIENT_OPTIONS_MENU(sFMMCMenu, sFMMCendStage, iChangeValue)
	ELSE 
		PROCESS_EDIT_DEATHMATCH_MENUS(sFMMCMenu, sFMMCendStage, iChangeValue)
	ENDIF
	
	IF iType != -1
	
		sFMMCmenu.iCurrentEntitySelection[iType]+= iChangeValue
		
		IF iType = CREATION_TYPE_WEAPONS
			IF g_FMMC_STRUCT.iVehicleDeathmatch = 0
				IF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_PISTOLS
					iMaximum = FMMC_MAX_NUM_PISTOLS
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SHOTGUNS
					iMaximum = FMMC_MAX_NUM_SHOTGUNS
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_MGS
					iMaximum = FMMC_MAX_NUM_MGS
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_RIFLES
					iMaximum = FMMC_MAX_NUM_RIFLES
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SNIPERS
					iMaximum = FMMC_MAX_NUM_SNIPERS
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_HEAVY
					iMaximum = FMMC_MAX_NUM_HEAVY
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_THROWN
					iMaximum = FMMC_MAX_NUM_THROWN
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_MELEE
					iMaximum = FMMC_MAX_NUM_MELEE
				ELIF sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SPECIAL
					iMaximum = FMMC_MAX_NUM_SPECIAL
					IF NOT FMMC_IS_ROCKSTAR_DEV()
						iMaximum = FMMC_MAX_NUM_SPECIAL-1
					ENDIF
				ENDIF
			ELSE
				iMaximum = VEH_MAX_ARMOURY_TYPES
			ENDIF
		ELIF iType = CREATION_TYPE_WEAPON_AMMO
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sWepStruct.iNumberOfClips, iChangeValue, FMMC_MAX_CLIPS)
			sFMMCmenu.iWepClips = sWepStruct.iNumberOfClips
		ELIF iType = CREATION_TYPE_WEAPON_VEHICLE_PICKUP_TYPE
			
			
		ELIF iType = CREATION_TYPE_WEAPON_REST
			EDIT_CREATOR_BOOL_MENU_ITEM(sFMMCendStage, sWepStruct.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu) - PICKUP_MENU_RESTRICTIONS_TEAM1])
			sFMMCmenu.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu) - PICKUP_MENU_RESTRICTIONS_TEAM1] = sWepStruct.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu) - PICKUP_MENU_RESTRICTIONS_TEAM1]
		ELIF iType = CREATION_TYPE_WEAPON_LIBRARY
			IF g_FMMC_STRUCT.iVehicleDeathmatch = 0
				iMaximum = FMMC_WEAPON_LIBRARY_MAX
			ELSE
				iMaximum = FMMC_VEH_WEAPON_LIBRARY_MAX
			ENDIF
		ELIF iType = CREATION_TYPE_VEHICLE_COLOUR
			iMaximum = (ciRC_MAX_TYPE_LOCAL_PLAYER_COLOR +1)
		ELIF iType = CREATION_TYPE_TEAM_SPAWN_LOCATION
			iMaximum = g_FMMC_STRUCT.iMaxNumberOfTeams
		ELIF iType = CREATION_TYPE_PROPS
			IF sFMMCmenu.iPropLibrary = PROP_LIBRARY_BARRIERS
				iMaximum = MAX_NUM_IN_BARRIERS
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_BENCHES
				iMaximum = MAX_NUM_IN_BENCHES
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_BINS
				iMaximum = MAX_NUM_IN_BINS
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_BOUYS
				iMaximum = MAX_NUM_IN_BOUYS
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_CABINS
				iMaximum = MAX_NUM_IN_CABINS
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_CONTAINERS
				iMaximum = MAX_NUM_IN_CONTAINERS
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_CRATES
				iMaximum = MAX_NUM_IN_CRATES
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_CONSTRUCT
				iMaximum = MAX_NUM_IN_CONSTRUCT
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_MACHINERY
				iMaximum = MAX_NUM_IN_MACHINERY
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_RAMPS
				iMaximum = MAX_NUM_IN_RAMPS
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_ROADSIDE
				iMaximum = MAX_NUM_IN_ROADSIDE
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_TRAILERS
				iMaximum = MAX_NUM_IN_TRAILERS
			ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_WRECKAGE
				iMaximum = MAX_NUM_IN_WRECKAGE
			ENDIF
		ELIF iType = CREATION_TYPE_DYNOPROPS
			iMaximum = MAX_NUM_IN_DYNOPROP_PALLET
		ELIF iType = CREATION_TYPE_PROP_LIBRARY
			IF sFMMCMenu.iSelectedEntity = -1
				iMaximum = (PROP_LIBRARY_MAX_ITEMS - 1)
			ELSE
				iMaximum = (PROP_LIBRARY_MAX_ITEMS - 2)
			ENDIF
		ENDIF
		
		INT iWrapMax = iMaximum - 1
		
		IF sFMMCmenu.iCurrentEntitySelection[iType] < 0 
			sFMMCmenu.iCurrentEntitySelection[iType] = iWrapMax
		ELIF sFMMCmenu.iCurrentEntitySelection[iType] > iWrapMax
			sFMMCmenu.iCurrentEntitySelection[iType] = 0
		ENDIF
		
		IF iType = CREATION_TYPE_WEAPONS
			CHANGE_WEAPON_TYPE(sFMMCmenu, sWepStruct, sFMMCmenu.iCurrentEntitySelection[iType], TRUE, iChangeValue > 0, iMaximum)
		ELIF iType = CREATION_TYPE_WEAPON_LIBRARY
			CHANGE_WEAPON_LIBRARY(sFMMCmenu, sWepStruct, sFMMCmenu.iCurrentEntitySelection[iType], TRUE)
		ENDIF
	ENDIF
	
ENDPROC


// -----------------------------------		TIME  
FUNC INT GET_TEST_DURATION(INT iTimer)

	SWITCH iTimer
		CASE 0	RETURN 	(1000*60*5)-1000	BREAK
		CASE 1	RETURN	(1000*60*10)-1000	BREAK
		CASE 2	RETURN	(1000*60*15)-1000	BREAK
		CASE 3	RETURN	(1000*60*20)-1000	BREAK
		CASE 4	RETURN	(1000*60*30)-1000	BREAK
		CASE 5	RETURN	(1000*60*45)-1000	BREAK
		CASE 6	RETURN	(1000*60*60)-1000	BREAK
	ENDSWITCH
	
	RETURN (1000*60*10)-1000

ENDFUNC

FUNC INT GET_TEST_HUD_TIME(INT iTimer)
	RETURN GET_TEST_DURATION(iTimer) + 1000 - (GET_GAME_TIMER() - TEST_TIMER)
ENDFUNC

PROC CHECK_TIME_FOR_VALID_TEST()
	IF NOT bTestStatBeenIncreased
	AND NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
	AND NOT IS_KING_OF_THE_HILL()
		
		/*INT tempTimer
		tempTimer = (GET_GAME_TIMER() - TEST_TIMER)
		IF tempTimer < 120000
			DRAW_GENERIC_TIMER((120000 - tempTimer), "FMMC_DM_TIME",0, TIMER_STYLE_DONTUSEMILLISECONDS, 0, PODIUMPOS_NONE, HUDORDER_BOTTOM)
		ENDIF*/
		
		IF iGlobalTestKills < ciTest_MinimumScore
		AND NOT CONTENT_IS_USING_ARENA()
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(iGlobalTestKills, ciTest_MinimumScore, "FMMC_DM_KILL", 0, HUD_COLOUR_WHITE, HUDORDER_BOTTOM)
		ENDIF
		
		IF iGlobalTestKills > ciTest_MinimumScore
		OR NOT IS_LEGACY_DEATHMATCH_CREATOR()
			bTestStatBeenIncreased = TRUE
			sCurrentVarsStruct.creationStats.iTimesTestedLoc++
			sCurrentVarsStruct.creationStats.bMadeAChange = FALSE
			IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
				SET_BIT(g_FMMC_STRUCT.biTeamTestComplete, g_FMMC_STRUCT.iTestMyTeam)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
 
PROC HANDLE_MOUSE_ENTITY_SELECTION_CANCEL_AND_SWITCH(eFMMC_MENU_ENUM itemMenu, INT iNewItemIndex, INT iMaxItems)
	// cancel out of stuff
	CANCEL_ENTITY_CREATION(sFMMCmenu.iEntityCreation)
	IF sFMMCMenu.iSelectedEntity != -1	
		sFMMCMenu.iSelectedEntity = -1
	ENDIF

	// force the menu to be the one that we want
	GO_TO_MENU(sFMMCMenu, sFMMCdata, itemMenu)
	REFRESH_MENU(sFMMCMenu)

	// instead of looping through until the switch cam increases we force it
	sFMMCmenu.iSwitchCam = iNewItemIndex - 1
	DEAL_WITH_CAMERA_SWITCH_SELECTION(TRUE, iMaxItems)
	SET_CURSOR_POSITION(0.5, 0.5)
	PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())			
ENDPROC

FUNC BOOL DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM(INT iDirection)
	//INT iSavedSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > sFMMCmenu.iCurrentMenuLength-1
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
	ENDIF
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iCurrentMenuLength - 1)
	ENDIF

	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ARENAPROP_MENU_ITEM_DM
		AND NOT CONTENT_IS_USING_ARENA()
		AND NOT FMMC_IS_ROCKSTAR_DEV()
			PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping ARENAPROP_MENU_ITEM_DM")
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
			RETURN TRUE
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DON_KOTH_HILLS_DM
		AND NOT IS_KING_OF_THE_HILL()
			PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping DON_KOTH_HILLS_DM")
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
			RETURN TRUE
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DON_KOTH_BOUNDS_DM
		AND (NOT IS_THIS_A_DEATHMATCH() OR CONTENT_IS_USING_ARENA())
			PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping DON_KOTH_BOUNDS_DM")
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
			RETURN TRUE
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DON_MENU_RAND_DM
		AND IS_KING_OF_THE_HILL()
			PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping DON_MENU_RAND_DM")
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_ARENA
		AND NOT CONTENT_IS_USING_ARENA()
			PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping OPTION_DM_ARENA")
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
			RETURN TRUE
		ENDIF

		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_DEFAULT_VEHICLE
		AND (NOT CONTENT_IS_USING_ARENA() OR FMMC_IS_ROCKSTAR_DEV())
			PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping OPTION_DM_DEFAULT_VEHICLE")
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
			RETURN TRUE
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_RESPAWN_VEHICLE
		AND NOT IS_NON_ARENA_KING_OF_THE_HILL()
			PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping OPTION_DM_RESPAWN_VEHICLE")
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
			RETURN TRUE
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_DM_AVAILABLE_VEHICLES_ARENA
		AND (NOT CONTENT_IS_USING_ARENA() OR FMMC_IS_ROCKSTAR_DEV())
			PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping OPTION_DM_AVAILABLE_VEHICLES_ARENA")
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RADIUS
		AND sFMMCMenu.sTempHillStruct.iHillType != ciKOTH_HILL_TYPE__CIRCLE
			PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping KOTH_HILL__RADIUS")
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
			RETURN TRUE
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_WIDTH
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_LENGTH
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_HEIGHT
			IF sFMMCMenu.sTempHillStruct.iHillType != ciKOTH_HILL_TYPE__RECT
				PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping KOTH_HILL__RECT_WIDTH or KOTH_HILL__RECT_HEIGHT")
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF NOT FMMC_IS_ROCKSTAR_DEV()
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__MINIMUM_PLAYERS
				PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping dev only option")
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
				RETURN TRUE
			ENDIF
		ENDIF

		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__ENTITY_OPTIONS
			IF sFMMCMenu.sTempHillStruct.iHillType != ciKOTH_HILL_TYPE__VEHICLE
			AND sFMMCMenu.sTempHillStruct.iHillType != ciKOTH_HILL_TYPE__OBJECT
				PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM - Skipping KOTH_HILL__ENTITY_OPTIONS")
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_CURRENT_SELECTION_BE_SKIPPED(sFMMCmenu)
		SKIP_EMPTY_OPTION(sFMMCmenu, iDirection)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HANDLE_MOUSE_ENTITY_SELECTION(BOOL bTesting = FALSE)

	BOOL bMouseOverCorona = FALSE
	
	IF NOT FMMC_DO_HANDLE_MOUSE_SELECTION_CHECK(sFMMCmenu, sCurrentVarsStruct, bMouseOverCorona, bTesting)
		EXIT
	ENDIF
	
	FP_FMMC_MOUSE_ENTITY_SELECT_HANDLER funcPtr = &HANDLE_MOUSE_ENTITY_SELECTION_CANCEL_AND_SWITCH
	
	// check objects
	IF FMMC_HANDLE_MOUSE_DYNOPROP_SELECTION(sFMMCmenu, sDynoPropStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_PROP_SELECTION(sFMMCmenu, sPropStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	 
	IF FMMC_HANDLE_MOUSE_SPAWN_POINT_SELECTION(sFMMCmenu, sPedStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_0_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[0], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_1_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[1], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_2_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[2], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_3_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[3], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_VEHICLE_SELECTION(sFMMCmenu, sVehStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_WEAPON_SELECTION(sFMMCmenu, sWepStruct, sCurrentVarsStruct, sInvisibleObjects, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF (bMouseOverCorona = FALSE) AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
		FMMC_SET_CURSOR_MAP_ACCEPT_BIT(sFMMCmenu, FALSE)
	ENDIF
ENDPROC

FUNC BOOL CAN_SWITCH_CAMERA()
	IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
		PRINTLN("[CAMSWAP] CAN_SWITCH_CAMERA - Invalid menu || Menu is: ", sFMMCmenu.sActiveMenu)
		RETURN FALSE
	ENDIF
	
//	IF NOT SHOULD_MENU_BE_DRAWN(sFMMCData, sFMMCMenu, sCurrentVarsStruct, menuScrollController,2, TRUE, bShowMenu)
//		PRINTLN("[CAMSWAP] CAN_SWITCH_CAMERA - SHOULD_MENU_BE_DRAWN")
//		RETURN FALSE
//	ENDIF

	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCMenu)
		PRINTLN("[CAMSWAP] CAN_SWITCH_CAMERA - Player is pressing place button")
		RETURN FALSE
	ENDIF
	
	IF (HAS_CURRENT_PROP_BEEN_FREE_ROTATED(sFMMCmenu) AND IS_PC_VERSION() AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL))
		PRINTLN("[CAMSWAP] CAN_SWITCH_CAMERA - HAS_CURRENT_PROP_BEEN_FREE_ROTATED(sFMMCmenu) AND IS_PC_VERSION() AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)")
		RETURN FALSE
	ENDIF
	
	IF sFMMCMenu.bRefreshArena
		PRINTLN("[CAMSWAP] CAN_SWITCH_CAMERA - sFMMCMenu.bRefreshArena")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		PRINTLN("[CAMSWAP] CAN_SWITCH_CAMERA - biTestMissionActive")
		RETURN FALSE
	ENDIF
	
	IF bSwitchingCam
		PRINTLN("[CAMSWAP] CAN_SWITCH_CAMERA - bSwitchingCam")
		RETURN FALSE
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_NEXT_CAMERA)
	RETURN TRUE
ENDFUNC

PROC GET_MOD_SET_SPAWN_VEHICLE_LIBRARY_AND_TYPE(INT iModSet)
	IF (iModSet != -1 AND iModSet < ciDM_MAX_MODIFIER_SETS)
	AND g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSet].mnRespawnVehicle != DUMMY_MODEL_FOR_SCRIPT
		sFMMCMenu.iRespawnVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSet].mnRespawnVehicle)
		sFMMCMenu.iRespawnVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iModSet].mnRespawnVehicle)
	ELSE
		sFMMCMenu.iRespawnVehicleLibrary = 0
		sFMMCMenu.iRespawnVehicleType = 0
	ENDIF
ENDPROC

PROC UPDATE_DATA_ON_MODIFIER_BUMPER_SWITCH()
	GET_MOD_SET_SPAWN_VEHICLE_LIBRARY_AND_TYPE(sFMMCMenu.iDMModifierIndex)
ENDPROC

PROC PROCESS_ACCEPT_PRESS_MENU_OPTIONS()
	INT iTeamToUse = sFMMCmenu.iSelectedTeam
	INT i
	SWITCH sFMMCmenu.sActiveMenu
		
		CASE eFmmc_DM_CUSTOM_GAME_OPTIONS
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings
			AND NOT sFMMCmenu.bTutorialRunning
				g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings++
			ENDIF
		BREAK
		CASE eFmmc_DM_MODIFIER_SETUP
			IF IS_INT_IN_RANGE(GET_CREATOR_MENU_SELECTION(sFMMCmenu), DM_CGS_SETUP_T1, DM_CGS_SETUP_T4+1)
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].sModifierSetup.iTeamBS, GET_CREATOR_MENU_SELECTION(sFMMCMenu)-DM_CGS_SETUP_T1)
				REFRESH_MENU(sFMMCmenu)
			ENDIF
		BREAK
		CASE eFmmc_DM_CUSTOM_GAME_MANAGEMENT
			PROCESS_CUSTOM_GAME_SETTING_MANAGEMENT_MENU_ACCEPT(sFMMCmenu, sCurrentVarsStruct)
		BREAK
		CASE eFmmc_DM_MODIFIER_SET
			IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_DM_MODIFIER_SPAWN_VEHICLE
				GET_MOD_SET_SPAWN_VEHICLE_LIBRARY_AND_TYPE(sFMMCMenu.iDMModifierIndex)
			ENDIF
		BREAK
		CASE eFMMC_AVAILABLE_VEHICLE_LIBRARIES
			IF sFMMCmenu.iCurrentSelection < CORONA_VEHICLE_LIST_MAX
				EXIT
			ENDIF
			
			INT iSelectedLibrary
			iTeamToUse = sFMMCmenu.iSelectedTeam
			iSelectedLibrary = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - CORONA_VEHICLE_LIST_MAX
			IF iSelectedLibrary >= 0
				IF g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][iSelectedLibrary][0] != 0
				OR g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][iSelectedLibrary][1] != 0
					g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][iSelectedLibrary][0] = 0
					g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][iSelectedLibrary][1] = 0
				ELSE
					FOR i = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(iSelectedLibrary)-1
						SET_BIT(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][iSelectedLibrary][GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
					ENDFOR
				ENDIF
				FOR i = 0 TO VEHICLE_LIBRARY_MAX - 1
					IF g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeamToUse]][0] = 0
					AND	g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeamToUse]][1] = 0
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeamToUse], 1, VEHICLE_LIBRARY_MAX)
					ELSE
						BREAKLOOP
					ENDIF
				ENDFOR
				FOR i = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeamToUse]) - 1
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeamToUse]][GET_LONG_BITSET_INDEX(g_FMMC_STRUCT.iDefaultCommonVehicle[iTeamToUse])], GET_LONG_BITSET_BIT(g_FMMC_STRUCT.iDefaultCommonVehicle[iTeamToUse]))
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicle[iTeamToUse], 1, GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeamToUse]))
					ELSE
						BREAKLOOP
					ENDIF
				ENDFOR
			ENDIF
			REFRESH_MENU(sFMMCmenu)
			
		BREAK
		
		CASE eFMMC_AVAILABLE_VEHICLES
			iTeamToUse = sFMMCmenu.iSelectedTeam
			EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][sFMMCmenu.iVehicleLibrary][GET_LONG_BITSET_INDEX(GET_CREATOR_MENU_SELECTION(sFMMCmenu))], GET_LONG_BITSET_BIT(GET_CREATOR_MENU_SELECTION(sFMMCmenu)))
			
			FOR i = 0 TO VEHICLE_LIBRARY_MAX - 1
				IF g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][0] = 0
				AND g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][1] = 0
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeamToUse], 1, VEHICLE_LIBRARY_MAX)
				ELSE
					BREAKLOOP
				ENDIF
			ENDFOR
			FOR i = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeamToUse]) - 1
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[iTeamToUse][g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeamToUse]][GET_LONG_BITSET_INDEX(g_FMMC_STRUCT.iDefaultCommonVehicle[iTeamToUse])], GET_LONG_BITSET_BIT(g_FMMC_STRUCT.iDefaultCommonVehicle[iTeamToUse]))
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicle[iTeamToUse], 1, GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeamToUse]))
				ELSE
					BREAKLOOP
				ENDIF
			ENDFOR
			REFRESH_MENU(sFMMCMenu)
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    This handles most of the button presses, and works out what to do. It also calls DRAW_MENU_SELECTION() to draw the menu.
///    When Accept is pressed it checks wether the item is an action item or menu go to item. If it is a Go To, it updates the active menu and return null as the action.
/// RETURNS:
///   enumFMMC_CIRCLE_MENU_ACTIONS value if the menu is to perform an action when Accept is pressed. Otherwise it returns NULL
FUNC enumFMMC_CIRCLE_MENU_ACTIONS RETURN_CIRCLE_MENU_SELECTIONS()
	
	// Draws the menu here
	IF LOAD_MENU_ASSETS()
		DRAW_MENU_SELECTION()
	ENDIF
	
	BOOL bSubmenuActive = NOT IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack) 
	
	IF NOT IS_BIT_SET(iHelpBitSet,biRandomizing)
		IF IS_SCREEN_FADED_IN()
		
			IF sFMMCmenu.sActiveMenu = eFmmc_DM_CUSTOM_GAME_OPTIONS
				sFMMCMenu.iDMModifierIndex = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
			ELIF sFMMCmenu.sActiveMenu = eFMMC_Inventory_Weapon_List
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < FMMC_MAX_INVENTORY_WEAPONS
					sFMMCMenu.iCurrentInventoryWeapon = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
					WEAPON_TYPE wtWep = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].sModifierInventory.sWeaponStruct[sFMMCMenu.iCurrentInventoryWeapon].wtWeapon
					IF wtWep != WEAPONTYPE_INVALID
						SET_WEAPON_LIBRARY_AND_TYPE_FROM_WEAPON_TYPE(sFMMCmenu, sWepStruct, wtWep)
					ELSE
						sFMMCmenu.iWepLibrary = -1
						sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPON_LIBRARY] = -1
					ENDIF
					PRINTLN("[INVENTORY] sFMMCMenu.iCurrentInventoryWeapon set to: ", sFMMCMenu.iCurrentInventoryWeapon)
				ENDIF
			ENDIF
		
			IF FFMC_IS_ACCEPT_JUST_PRESSED(sFMMCmenu) 
			OR (FMMC_IS_CIRCLE_MENU_A_CAMERA_MENU(sFMMCmenu.sActiveMenu) AND FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu))
				IF IS_THIS_OPTION_SELECTABLE()
					IF IS_THIS_OPTION_A_MENU_ACTION()
					OR IS_THIS_OPTION_A_MENU_GOTO() 
						IF (sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1)
						AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
						AND sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_KotH_PLACE_HILL
							PLAY_SOUND_FRONTEND(-1, "SELECT", GET_SOUND_SET_FROM_CREATOR_TYPE())
							PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
						ENDIF
					ENDIF
					
				ELSE
					IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_PUBLISH
						OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_SAVE
							CHECK_FOR_BLOCKED_PRIVILEGES(sFMMCmenu)
						ENDIF
					ENDIF
					
					PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
					PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
				ENDIF
				
				SWITCH sFMMCmenu.sActiveMenu
					CASE eFmmc_DM_RANDOMIZER
						IF IS_THIS_OPTION_SELECTABLE()
						AND sCurrentVarsStruct.bLOSHitGround = FALSE
							PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
							PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
							sCurrentVarsStruct.bDisplayFailReason = TRUE
							sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
							SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bDistFail)
							sCurrentVarsStruct.sFailReason = GET_GENERIC_PLACEMENT_FAIL_DESCRIPTION(sCurrentVarsStruct)
							RETURN eFmmc_Action_Null
						ENDIF
					BREAK
					
					CASE efMMC_WORLD_PROPS
						IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = WORLD_PROPS_SELECTED_MODEL_ENTER_MANUALLY						
						AND IS_BIT_SET(sFMMCmenu.sCurrentWorldProp.iBitset, ciFMMC_WorldPropManuallyPlaced)
							sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63							
							RETURN eFmmc_Action_Null
						ENDIF
					BREAK
				ENDSWITCH
				
				PROCESS_ACCEPT_PRESS_MENU_OPTIONS()
				
				IF sFMMCMenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
					GO_TO_MENU(sFMMCMenu, sFMMCdata, sFMMCMenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)])
					CLEAR_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)	
					WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM(1)
						PRINTLN("[DM] Skipping options on menu entry...")
					ENDWHILE
					
					SWITCH sFMMCmenu.sActiveMenu
						CASE eFmmc_PAN_CAM_BASE
						CASE eFmmc_PHOTO_CAM_BASE
							IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
								sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
							ENDIF
						BREAK
					ENDSWITCH
					
					RETURN eFmmc_Action_Null
				ENDIF
				
				IF IS_THIS_OPTION_SELECTABLE()	
					//If the menu is not to change then return the action returned
					IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
					
						IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
						AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
							IF bInitialIntroCamSetup
								RETURN eFmmc_Action_Null
							ELIF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT.vStartPos, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
								PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
								PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
								sCurrentVarsStruct.bDisplayFailReason = TRUE
								sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
								sCurrentVarsStruct.sFailReason = "FMMC_ER_023"
								RETURN eFmmc_Action_Null
							ENDIF
						ENDIF
						
						RETURN sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
					ENDIF
				ENDIF
			ENDIF
			
			IF FMMC_IS_CANCEL_JUST_PRESSED(sFMMCmenu)
			OR SHOULD_FORCE_MENU_BACKOUT(sFMMCMenu)
			
				sCurrentVarsStruct.bDisplayFailReason = FALSE					
				IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
					IF IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
						CLEAR_BIT(sFMMCdata.iBitSet, bResetMenu)
					ENDIF
				ELSE
					sCurrentVarsStruct.bResetUpHelp = TRUE
					sCurrentVarsStruct.bDisplayFailReason = FALSE
					
					INT iSaveEntityType = sFMMCmenu.iEntityCreation
					
					IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
						IF sFMMCMenu.iSelectedEntity > -1
							IF sFMMCmenu.iEntityCreation = CREATION_TYPE_VEHICLES
							AND sVehStruct.iSwitchingINT	!= CREATION_STAGE_PLACE
								PRINTLN("CANCEL_ENTITY_CREATION - Vehicle is not ready to be cancelled out of!")
								RETURN eFmmc_Action_Null
							ENDIF
							
							IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PROPS
							AND GET_CURRENT_GENERIC_CREATION_STAGE() != CREATION_STAGE_PLACE
								PRINTLN("CANCEL_ENTITY_CREATION - Prop is not ready to be cancelled out of!")
								RETURN eFmmc_Action_Null
							ENDIF
							
							IF sFMMCmenu.iEntityCreation = CREATION_TYPE_DYNOPROPS
							AND GET_CURRENT_GENERIC_CREATION_STAGE() != CREATION_STAGE_PLACE
								PRINTLN("CANCEL_ENTITY_CREATION - Dynoprop is not ready to be cancelled out of!")
								RETURN eFmmc_Action_Null
							ENDIF
						ENDIF
						
						CANCEL_ENTITY_CREATION(sFMMCmenu.iEntityCreation)
					ENDIF
					
					IF bSubmenuActive
						SET_CREATION_TYPE(sFMMCMenu, iSaveEntityType)
					ENDIF
					
					IF (sFMMCMenu.iSelectedEntity != -1 AND NOT bSubmenuActive)
					OR (sFMMCmenu.sActiveMenu = eFmmc_DOORS_BASE AND sFMMCMenu.sCurrentDoor.mnDoorModel != DUMMY_MODEL_FOR_SCRIPT)
					OR (sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS AND sFMMCMenu.sCurrentWorldProp.mn != DUMMY_MODEL_FOR_SCRIPT)
						sFMMCMenu.iSelectedEntity = -1
					ELSE
						IF sFMMCmenu.sMenuBack != eFmmc_Null_item
							IF g_FMMC_STRUCT.iAvailableCommonVehicles[0][VEHICLE_LIBRARY_CONTENDER][0] = 0
							AND CONTENT_IS_USING_ARENA()
								sCurrentVarsStruct.bDisplayFailReason = TRUE
								sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()	
								sCurrentVarsStruct.sFailReason = "FMMC_ER_036A"
								RESET_UP_BUTTON_HELP()
							ELSE
								PLAY_SOUND_FRONTEND(-1, "BACK", GET_SOUND_SET_FROM_CREATOR_TYPE())
								GO_BACK_TO_MENU(sFMMCmenu)
								sFMMCmenu.sSubTypeName[CREATION_TYPE_CYCLE] = ""
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF sFMMCmenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
				OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
					CHECK_IF_SNAPPING_SHOULD_BE_TURNED_OFF(sFMMCmenu)
				ENDIF
				IF sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
				OR sFMMCmenu.iEntityCreation != CREATION_TYPE_PROPS
					sFMMCmenu.bChainSnapBroken = FALSE
					sPropStruct.iRotTimerStart = -1
					sFMMCmenu.bForceRotate = FALSE
				ENDIF
				REFRESH_MENU(SFMMCMenu)
			ENDIF
			
			IF MENU_CONTROL_DOWN_CREATOR(menuScrollController)
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)
				WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM(1)
					PRINTLN("[DM] Skipping options...")
				ENDWHILE
				IF g_FMMC_STRUCT.iVehicleDeathmatch = 1
					IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
					AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEH_MENU_ITEM_DM
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DON_MENU_RAND_DM)
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)
						IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEH_MENU_ITEM_DM
						OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DON_MENU_RAND_DM)
							SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)
						ENDIF	
					ENDIF
				ENDIF
				IF g_FMMC_STRUCT.bMissionIsPublished
					IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_SAVE
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)
					ENDIF
				ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > sFMMCmenu.iCurrentMenuLength-1
					SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
				ENDIF
				
				IF sFMMCmenu.iCurrentMenuLength > 1
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
					PRINTSTRING("SCROLL SOUND EFFECT!!!")PRINTNL()
				ENDIF
				
				REFRESH_MENU(sFMMCmenu)
				
				IF NOT sFMMCendStage.bHasValidROS
					sFMMCendStage.bHasValidROS = NETWORK_HAS_VALID_ROS_CREDENTIALS()
				ENDIF
				
				sCurrentVarsStruct.bResetUpHelp  = TRUE
			ENDIF
			
			IF MENU_CONTROL_UP_CREATOR(menuScrollController)
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
				
				WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS_DM(-1)
					PRINTLN("[DM] Skipping options...")
				ENDWHILE
				
				IF g_FMMC_STRUCT.iVehicleDeathmatch = 1
					IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
					AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEH_MENU_ITEM_DM
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = WEP_MENU_ITEM_DM
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DON_MENU_RAND_DM)
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
						IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEH_MENU_ITEM_DM
						OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = WEP_MENU_ITEM_DM
						OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DON_MENU_RAND_DM)
							SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)										
						ENDIF					
					ENDIF		
				ENDIF
				IF g_FMMC_STRUCT.bMissionIsPublished
					IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_SAVE
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
					ENDIF
				ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
					SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iCurrentMenuLength - 1)
				ENDIF
				
				IF sFMMCmenu.iCurrentMenuLength > 1
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
					PRINTSTRING("SCROLL SOUND EFFECT!!!")PRINTNL()
				ENDIF

				REFRESH_MENU(sFMMCmenu)
				
				IF NOT sFMMCendStage.bHasValidROS
					sFMMCendStage.bHasValidROS = NETWORK_HAS_VALID_ROS_CREDENTIALS()
				ENDIF
				
				sCurrentVarsStruct.bResetUpHelp  = TRUE
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_DM_RANDOMIZER
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0 
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT) AND sFMMCmenu.sRandStruct.fRadius < 100
					sFMMCmenu.sRandStruct.fRadius += 1.0
					REMOVE_BLIP(bRandomizerBlip)
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
				ENDIF
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT) AND sFMMCmenu.sRandStruct.fRadius > 25
					sFMMCmenu.sRandStruct.fRadius -= 1.0
					REMOVE_BLIP(bRandomizerBlip)
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
				ENDIF
				sRandStruct.fRadius = sFMMCmenu.sRandStruct.fRadius
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE)
				SWITCH sFMMCmenu.sActiveMenu
					CASE eFmmc_CAPTURE_TEAM_VEHICLE_MAIN_MENU
						g_FMMC_STRUCT.mnVehicleModel[sFMMCMenu.iSelectedTeam] = DUMMY_MODEL_FOR_SCRIPT
					BREAK
					CASE efmmc_IPL_ISLAND
						IF IS_BIT_SET(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
								
							vIPLSelected = GET_ISLAND_IPL_WARP_LOCATION(sFMMCmenu)
							
							IF NOT IS_VECTOR_ZERO(vIPLSelected)
								IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vIPLSelected) > 100
									IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
										SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, TRUE, FALSE, TRUE)
									ENDIF
								
									SET_ENTITY_COORDS(PLAYER_PED_ID(), vIPLSelected)
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE eFmmc_DM_CUSTOM_GAME_OPTIONS
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings
							sFMMCmenu.iDMModifierCopyTarget = sFMMCmenu.iDMModifierIndex + 1
							IF sFMMCmenu.iDMModifierCopyTarget >= g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings
								sFMMCmenu.iDMModifierCopyTarget = 0
							ENDIF
							GO_TO_MENU(sFMMCmenu, sFMMCdata, eFmmc_DM_CUSTOM_GAME_MANAGEMENT)
						ENDIF
					BREAK
				ENDSWITCH
				
				REFRESH_MENU(sFMMCmenu)
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS
				IF sFMMCMenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__CIRCLE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RADIUS
					// EDITING CIRCLE HILL RADIUS
					
					FLOAT fRadiusIncrement = 0.0
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						fRadiusIncrement = 1.0
					ENDIF
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						fRadiusIncrement = -1.0
					ENDIF
					
					IF fRadiusIncrement != 0.0
						fKOTH_RadiusIncrementVelocity += fKOTH_RadiusIncrementVelocity * GET_FRAME_TIME() * 2.0
						fKOTH_RadiusIncrementVelocity = CLAMP(fKOTH_RadiusIncrementVelocity, 1.0, 50.0)
						
						EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.sTempHillStruct.fHill_Radius, cfKOTH_HILL_RADIUS_INCREMENT * GET_FRAME_TIME() * fRadiusIncrement * fKOTH_RadiusIncrementVelocity, cfKOTH_HILL_MAX_RADIUS, cfKOTH_HILL_MIN_RADIUS, TRUE, TRUE)
						REFRESH_MENU(sFMMCmenu)
					ELSE
						fKOTH_RadiusIncrementVelocity = 1.0
					ENDIF
				ELIF sFMMCMenu.sTempHillStruct.iHillType = ciKOTH_HILL_TYPE__RECT
					
					FLOAT fIncrement = 0.0
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						fIncrement = 1.0
					ENDIF
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						fIncrement = -1.0
					ENDIF
					
					IF fIncrement != 0.0
						fKOTH_RadiusIncrementVelocity += fKOTH_RadiusIncrementVelocity * GET_FRAME_TIME() * 2.0
						fKOTH_RadiusIncrementVelocity = CLAMP(fKOTH_RadiusIncrementVelocity, 1.0, 50.0)
						
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_WIDTH
							EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.sTempHillStruct.fHill_Width, cfKOTH_HILL_RADIUS_INCREMENT * GET_FRAME_TIME() * fIncrement * fKOTH_RadiusIncrementVelocity, cfKOTH_HILL_MAX_WIDTH, cfKOTH_HILL_MIN_WIDTH, TRUE, TRUE)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_HEIGHT
							EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.sTempHillStruct.fHill_Height, cfKOTH_HILL_RADIUS_INCREMENT * GET_FRAME_TIME() * fIncrement * fKOTH_RadiusIncrementVelocity, cfKOTH_HILL_MAX_HEIGHT, cfKOTH_HILL_MIN_HEIGHT, TRUE, TRUE)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = KOTH_HILL__RECT_LENGTH
							EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.sTempHillStruct.fHill_Length, cfKOTH_HILL_RADIUS_INCREMENT * GET_FRAME_TIME() * fIncrement * fKOTH_RadiusIncrementVelocity, cfKOTH_HILL_MAX_LENGTH, cfKOTH_HILL_MIN_LENGTH, TRUE, TRUE)
						ENDIF
						
						REFRESH_MENU(sFMMCmenu)
					ELSE
						fKOTH_RadiusIncrementVelocity = 1.0
					ENDIF
				ENDIF
			ENDIF
			
			INT iChange = 0
			IF MENU_CONTROL_RIGHT_CREATOR(menuScrollController)
				iChange = 1
			ENDIF
			
			IF MENU_CONTROL_LEFT_CREATOR(menuScrollController)
				iChange = -1
			ENDIF
			
			IF sFMMCmenu.sActiveMenu =  eFmmc_DEV_PROOFS_OPTIONS
				EDIT_DEV_PLAYER_PROOFS_OPTIONS_MENU(sFMMCMenu, sFMMCendStage, sCurrentVarsStruct, iChange)
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_ROAMING_SPECTATOR_OPTIONS
				EDIT_ROAMING_SPECTATOR_OPTIONS_MENU(sFMMCmenu, sFMMCendStage, sCurrentVarsStruct, iChange)
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_DEV_AMBIENT_OPTIONS
				EDIT_DEV_AMBIENT_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_CAPTURE_OBJECT_BASE
			AND iChange != 0
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CAPTURE_OBJECT_PLACE
					sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS] += iChange
					
					IF sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS] >= 13
						sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS] = 0
					ELIF sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS] < 0
						sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS] = 12
					ENDIF
				
					CHANGE_OBJECT_TYPE(sFMMCMenu, sObjStruct, sFMMCMenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS])
				ENDIF
				
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CAPTURE_OBJECT_DISABLE_SPRINTING
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iObjMenuBitset, cibsOBJ_KOTH_NoSprintWhenHeld, TRUE)
				ENDIF
				
				// CYCLE
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CAPTURE_OBJECT_CYCLE
					DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfObjects)
				ENDIF
			ENDIF
		
			IF iChange != 0
				EDIT_MENU_OPTIONS(iChange)
				REFRESH_MENU(sFMMCmenu)
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)	
				IF IS_BIT_SET(iLocalBitSet, biDLCLocked)
					IF sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
					OR sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE
						OPEN_COMMERCE_STORE("","")
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SWITCH_CAMERA()
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_SWITCH_CAMERA_BUTTON())		
					IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
					AND sCurrentVarsStruct.bCanSwapCams
						sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
						PRINTLN("[CAMSWAP] Setting menu state to MENU_STATE_SWITCH_CAM")
					ELSE
						PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
						PRINTSTRING("CANT SWITCH ERROR SOUND EFFECT!!!")PRINTNL()
						PRINTLN("[CAMSWAP] Can't switch")
						sCurrentVarsStruct.bDisplayFailReason = TRUE
						sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
						IF sCurrentVarsStruct.bCanSwapCams
							sCurrentVarsStruct.sFailReason = "FMMC_ER_026"
						ELSE
							sCurrentVarsStruct.sFailReason = "FMMC_ER_CAMSWP"
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			AND FMMC_IS_ROCKSTAR_DEV()
				IF sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_TOP
					sCurrentVarsStruct.iMenuState = MENU_STATE_WARP_TO_INTERIOR
				ENDIF
			
				IF sFMMCmenu.sActiveMenu = eFMMC_AVAILABLE_VEHICLE_LIBRARIES
					IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) - CORONA_VEHICLE_LIST_MAX) >= 0
						sFMMCmenu.iVehicleLibrary = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - (CORONA_VEHICLE_LIST_MAX)
					ENDIF
					//If it is then reset up the menu
					GO_TO_MENU(sFMMCmenu, sFMMCdata, eFMMC_AVAILABLE_VEHICLES)
					
				ENDIF
				
				REFRESH_MENU(sFMMCmenu)
			ENDIF
			
		ENDIF
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		IF IS_CURRENT_MENU_A_DM_CUSTOM_SETTINGS_MENU(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu != eFmmc_DM_CUSTOM_GAME_OPTIONS
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
				sFMMCmenu.iDMModifierIndex--
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
				sFMMCmenu.iDMModifierIndex++
			ENDIF
			CAP_FMMC_MENU_ITEM(g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings, sFMMCmenu.iDMModifierIndex)
			sFMMCMenu.iReturnSelections[sFMMCMenu.iCurrentDepth-1] = sFMMCmenu.iDMModifierIndex
			UPDATE_DATA_ON_MODIFIER_BUMPER_SWITCH()
			REFRESH_MENU(sFMMCmenu)
		ENDIF
	ENDIF
	
	RETURN eFmmc_Action_Null
ENDFUNC

FUNC MODEL_NAMES GET_VEHICLE_FOR_TEST_PED_TO_SPAWN_IN(INT iTeam = 0)
	
	IF NOT IS_THIS_LEGACY_DM_CONTENT()
	AND IS_CURRENT_MODIFIER_SET_VALID(sTestPedModifiers[iTeam].iCurrentModSet)
		RETURN g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sTestPedModifiers[iTeam].iCurrentModSet].mnRespawnVehicle
	ENDIF
	
	RETURN GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, iTeam)
ENDFUNC

FUNC BOOL DOES_TEAM_HAVE_MAX_MEMBERS(INT iTeamMemberCount, INT iTeamToCheck)
	
	IF iTeamToCheck = iPlayerTeamNum
	AND g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeamToCheck] = 1 // Player is the only person on their team
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeamToCheck] > 0
	AND iTeamMemberCount+1 >= g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeamToCheck]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_ADD_PED_TO_THIS_TEAM(INT &iTeamMemberCount[], INT iTeamToPlace)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.iBS_AvailableDMTeams, iTeamToPlace)
	OR DOES_TEAM_HAVE_MAX_MEMBERS(iTeamMemberCount[iTeamToPlace], iTeamToPlace)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CREATE_THE_TEST_PEDS()

	IF IS_ARENA_CREATOR()
		RETURN TRUE
	ENDIF
	
	PRINTLN("CREATE_THE_TEST_PEDS || g_FMMC_STRUCT.iNumParticipants = ", g_FMMC_STRUCT.iNumParticipants)
	
	INT i
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints != 0
	
		iNumOnTeam1 = 1
		iNumOnTeam2 = 0
		
		BOOL bCreatePed = TRUE
		INT iNum
		
		IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
		
			INT iTeamMemberCount[FMMC_MAX_TEAMS]
			INITIALISE_INT_ARRAY(iTeamMemberCount, -1)
			iTeamMemberCount[iPlayerTeamNum] = 0
			
			INT iTeamToPlace = 0
			FOR i = 1 TO g_FMMC_STRUCT.iNumParticipants+1
			
				PRINTLN("CREATE_THE_TEST_PEDS - iTeamToPlace: ", iTeamToPlace, " || i = ", i, " || g_FMMC_STRUCT.iNumParticipants = ", g_FMMC_STRUCT.iNumParticipants, " g_FMMC_STRUCT.iMaxNumPlayersPerTeam[",iTeamToPlace,"] = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeamToPlace])
				
				// If team 0 is full, move onto team 1...
				IF NOT SHOULD_ADD_PED_TO_THIS_TEAM(iTeamMemberCount, iTeamToPlace) // Team 0
					iTeamToPlace++
				ENDIF
				
				// If team 1 is full, move onto team 2...
				IF NOT SHOULD_ADD_PED_TO_THIS_TEAM(iTeamMemberCount, iTeamToPlace) // Team 1
					iTeamToPlace++
				ENDIF
				
				// If team 2 is full, move onto team 3...
				IF NOT SHOULD_ADD_PED_TO_THIS_TEAM(iTeamMemberCount, iTeamToPlace) // Team 2
					iTeamToPlace++
				ENDIF
				
				// If team 3 is full, exit the loop...
				IF NOT SHOULD_ADD_PED_TO_THIS_TEAM(iTeamMemberCount, iTeamToPlace) // Team 3
					BREAKLOOP
				ENDIF
				
				iTeamMemberCount[iTeamToPlace]++
				PRINTLN("CREATE_THE_TEST_PEDS - Spawning ped on team: ", iTeamToPlace, " Number of members on team: ", iTeamMemberCount[iTeamToPlace])
				
				INT iTeamCount = iTeamMemberCount[iTeamToPlace]
				
				IF iTeamToPlace = iPlayerTeamNum
				AND iTeamCount = iRand
				
					iTeamCount++
					IF iTeamCount >= g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeamToPlace]
						iTeamCount = 0
					ENDIF
					
				ENDIF
				
				VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamToPlace][iTeamCount].vPos
				FLOAT fHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamToPlace][iTeamCount].fHead
				
				IF DOES_ENTITY_EXIST(testPeds[i].viVehicleDM)
					IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(testPeds[i].viVehicleDM))
						PRINTLN("CREATE_THE_TEST_PEDS - Setting point ", i, " as not clear because someone is in testPeds[", i, "].viVehicleDM")
						vPos = <<0, 0, 0>>
					ENDIF
				ENDIF
				
				IF NOT IS_VECTOR_ZERO(vPos)
				
					IF NOT DOES_ENTITY_EXIST(testPeds[i].pedID)
					
						testPeds[i].iTeam = iTeamToPlace

						IF NOT IS_CURRENT_MODIFIER_SET_VALID(sTestPedModifiers[iTeamToPlace].iCurrentModSet)
							sTestPedModifiers[testPeds[i].iTeam].iCurrentModSet = GET_RELEVANT_MODIFIER_SET_INDEX(sTestPedModifiers[testPeds[i].iTeam].sModifierData, testPeds[i].iTeam)
							PRINTLN("CREATE_THE_TEST_PEDS - TDM - sTestPedModifiers[",testPeds[i].iTeam,"].iCurrentModSet: ", sTestPedModifiers[testPeds[i].iTeam].iCurrentModSet)
						ENDIF
												
						IF NOT SHOULD_USE_VEHICLES(iTeamToPlace, sTestPedModifiers[iTeamToPlace].iCurrentModSet)
							testPeds[i].pedID = CREATE_PED(PEDTYPE_MISSION, DM_TestPedModel[iTeamToPlace], vPos, fHeading, FALSE, FALSE)
							
							IF IS_ENTITY_ALIVE(testPeds[i].pedID)
								SET_PED_FLEE_ATTRIBUTES(testPeds[i].pedID, FA_NEVER_FLEE, TRUE)	
								SET_PED_COMBAT_ATTRIBUTES(testPeds[i].pedID, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(testPeds[i].pedID, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
								SET_PED_DROPS_WEAPONS_WHEN_DEAD(testPeds[i].pedID, FALSE) 
								testPeds[i].bNotNeeded = FALSE
							ENDIF
						ELSE
							MODEL_NAMES mnVeh = GET_VEHICLE_FOR_TEST_PED_TO_SPAWN_IN(iTeamToPlace)
							REQUEST_MODEL(mnVeh)
							IF HAS_MODEL_LOADED(mnVeh)
								
								IF DOES_ENTITY_EXIST(testPeds[i].viVehicleDM)
									DELETE_VEHICLE(testPeds[i].viVehicleDM)
									PRINTLN("[DMTEST] Deleting testPeds[", i, "].viVehicleDM")
								ENDIF
								
								PRINTLN("[DMTEST] Creating testPeds[", i, "].viVehicleDM for enemy (team ", iTeamToPlace, ")")
								testPeds[i].viVehicleDM = CREATE_VEHICLE(mnVeh, vPos, fHeading, FALSE, FALSE)
								testPeds[i].pedID = CREATE_PED_INSIDE_VEHICLE(testPeds[i].viVehicleDM, PEDTYPE_MISSION, DM_TestPedModel[iTeamToPlace], VS_DRIVER, FALSE, FALSE)
								
								INIT_VDM_TEST_MODE_ENEMY(testPeds[i].pedID, testPeds[i].viVehicleDM)
										
								INT iVehicleColour
								iVehicleColour = GET_DM_VEHICLE_COLOR_FROM_SELECTION(testPeds[i].iTeam)								
								SET_VEHICLE_COLOURS(testPeds[i].viVehicleDM, iVehicleColour, iVehicleColour)
								
								mnTeamVehicle[testPeds[iNum].iTeam] = mnVeh
								testPeds[i].bNotNeeded = FALSE
							ELSE
								PRINTLN("CREATE_THE_TEST_PEDS - Exiting for ped ", i, " on team ", iTeamToPlace)
								RETURN FALSE
							ENDIF
						ENDIF
						
						IF testPeds[i].iTeam = iPlayerTeamNum
							SET_THE_COMMON_TEST_PED_VARIABLES(testPeds[i].pedID, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()), testPeds[i].iTeam)
						ELSE
							SET_THE_COMMON_TEST_PED_VARIABLES(testPeds[i].pedID, lrgFM_Team[testPeds[i].iTeam], testPeds[i].iTeam)
						ENDIF
						
						testPeds[i].iRespawnTimer = 0
						
						IF DOES_ENTITY_EXIST(testPeds[i].pedID)
						AND IS_CURRENT_MODIFIER_SET_VALID(sTestPedModifiers[testPeds[i].iTeam].iCurrentModSet)
							APPLY_MOD_SET_TO_TEST_MODE_BOT(testPeds[i].pedID, sTestPedModifiers[testPeds[i].iTeam].iCurrentModSet)
						ENDIF
						
						PRINTLN("CREATE_THE_TEST_PEDS - Ped ", i, " successfully created on team ", iTeamToPlace)
					ENDIF
				ELSE
					PRINTLN("CREATE_THE_TEST_PEDS - VECTOR IS ZERO WHICH SHOULD NEVER HAPPEN")
				ENDIF
			ENDFOR
			
		ELSE
		
			FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints-1
			
				IF i < FMMC_MAX_SPAWNPOINTS+1
				
					IF i != iRand
							
						IF iNumOnTeam1 > g_FMMC_STRUCT.iNumParticipants+1
							bCreatePed = FALSE
						ENDIF
												
						IF bCreatePed = TRUE
						
							iNum = iNumOnTeam1 + iNumOnTeam2
							
							IF NOT DOES_ENTITY_EXIST(testPeds[iNum].pedID)
							AND NOT DOES_ENTITY_EXIST(testPeds[iNum].viVehicleDM)
								
								testPeds[iNum].iTeam = 0
								IF NOT IS_CURRENT_MODIFIER_SET_VALID(sTestPedModifiers[testPeds[iNum].iTeam].iCurrentModSet)
									sTestPedModifiers[testPeds[iNum].iTeam].iCurrentModSet = GET_RELEVANT_MODIFIER_SET_INDEX(sTestPedModifiers[testPeds[iNum].iTeam].sModifierData, testPeds[iNum].iTeam)
									PRINTLN("CREATE_THE_TEST_PEDS - FFA - sTestPedModifiers[",testPeds[iNum].iTeam,"].iCurrentModSet: ", sTestPedModifiers[testPeds[iNum].iTeam].iCurrentModSet)
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].iModifierRestrictedBS, sTestPedModifiers[testPeds[iNum].iTeam].iCurrentModSet)
									PRINTLN("CREATE_THE_TEST_PEDS - Spawnpoint ", i, " is blocked on this modset")
									RELOOP
								ENDIF
								
								IF NOT SHOULD_USE_VEHICLES(DEFAULT, sTestPedModifiers[testPeds[iNum].iTeam].iCurrentModSet)
								
									testPeds[iNum].pedID = CREATE_PED(PEDTYPE_MISSION, DM_TestPedModel[0], g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].fHead, FALSE, FALSE)
										
									IF IS_ENTITY_ALIVE(testPeds[iNum].pedID)
										SET_PED_FLEE_ATTRIBUTES(testPeds[iNum].pedID, FA_NEVER_FLEE, TRUE)		
										SET_PED_COMBAT_ATTRIBUTES(testPeds[iNum].pedID, CA_AGGRESSIVE, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(testPeds[iNum].pedID, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(testPeds[iNum].pedID, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
										SET_PED_DROPS_WEAPONS_WHEN_DEAD(testPeds[iNum].pedID, FALSE)
										testPeds[i].bNotNeeded = FALSE
									ENDIF
								ELSE
									IF DOES_ENTITY_EXIST(testPeds[iNum].viVehicleDM)
										DELETE_VEHICLE(testPeds[iNum].viVehicleDM)
										PRINTLN("[DMTEST] Deleting testPeds[", iNum, "].viVehicleDM")
									ENDIF
									
									MODEL_NAMES mnVeh = GET_VEHICLE_FOR_TEST_PED_TO_SPAWN_IN()
									REQUEST_MODEL(mnVeh)
									IF NOT HAS_MODEL_LOADED(mnVeh)
										PRINTLN("CREATE_THE_TEST_PEDS - Exiting for ped ", i, " vehicle model not loaded yet")
										RETURN FALSE
									ENDIF
									
									PRINTLN("[DMTEST] Creating testPeds[", iRand, "].viVehicleDM for enemy")
									testPeds[iNum].viVehicleDM = CREATE_VEHICLE(mnVeh, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].fHead, FALSE, FALSE)
									testPeds[iNum].pedID = CREATE_PED_INSIDE_VEHICLE(testPeds[iNum].viVehicleDM, PEDTYPE_MISSION, DM_TestPedModel[0], VS_DRIVER, FALSE, FALSE)
									
									INIT_VDM_TEST_MODE_ENEMY(testPeds[iNum].pedID, testPeds[iNum].viVehicleDM)
									
									INT iVehicleColour
									iVehicleColour = GET_DM_VEHICLE_COLOR_FROM_SELECTION(iNum)
									SET_VEHICLE_COLOURS(testPeds[iNum].viVehicleDM, iVehicleColour, iVehicleColour)
									
									mnTeamVehicle[testPeds[iNum].iTeam] = mnVeh
									testPeds[i].bNotNeeded = FALSE
								ENDIF
								SET_THE_COMMON_TEST_PED_VARIABLES(testPeds[iNum].pedID, lrgFM_Team[i],testPeds[iNum].iTeam)
								
								testPeds[iNum].iRespawnTimer = 0
								iNumOnTeam1++
								
								IF DOES_ENTITY_EXIST(testPeds[iNum].pedID)
								AND IS_CURRENT_MODIFIER_SET_VALID(sTestPedModifiers[testPeds[iNum].iTeam].iCurrentModSet)
									APPLY_MOD_SET_TO_TEST_MODE_BOT(testPeds[iNum].pedID, sTestPedModifiers[testPeds[iNum].iTeam].iCurrentModSet)
								ENDIF
							ENDIF
						ELSE
							PRINTLN("CREATE_THE_TEST_PEDS - bCreatePed = FALSE")
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			NET_PRINT("*BAH - MADE PEDS FOR TEST")NET_NL()
		ENDIF
	ELSE
		PRINTLN("CREATE_THE_TEST_PEDS - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints = 0")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_THE_PLAYER_FOR_THE_TEST()

	MODEL_NAMES mnPlayerVeh = GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, g_FMMC_STRUCT.iTestMyTeam)
	
	IF iCurrentPlayerModSet != -1
	AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].iModifierSetBS, ciDM_ModifierBS_SpawnInVehicle)
		mnPlayerVeh = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].mnRespawnVehicle
	ENDIF
	
	IF SHOULD_USE_VEHICLES(g_FMMC_STRUCT.iTestMyTeam, iCurrentPlayerModSet)
	AND IS_MODEL_VALID(mnPlayerVeh)
		REQUEST_MODEL(mnPlayerVeh)
		
		IF NOT HAS_MODEL_LOADED(mnPlayerVeh)
			PRINTLN("[ARENA] CREATE_THE_PLAYER_FOR_THE_TEST - Waiting to load veh model ", GET_MODEL_NAME_FOR_DEBUG(mnPlayerVeh))
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		VECTOR vPos
		FLOAT fHeading
		
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints != 0						
			IF IS_THIS_A_TEAM_DM() 
				iPlayerTeamNum = g_FMMC_STRUCT.iTestMyTeam
				iRand = GET_RANDOM_INT_IN_RANGE(0, g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iPlayerTeamNum])
				
				vPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iPlayerTeamNum][iRand].vPos
				fHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iPlayerTeamNum][iRand].fHead
			ELSE
				iRand = GET_RANDOM_INT_IN_RANGE(0, g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints)
										
				INT i
				FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRand].iModifierRestrictedBS, iCurrentPlayerModSet)
						BREAKLOOP
					ENDIF
					
					iRand++
					IF iRand >= g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints
						iRand = 0
					ENDIF
					
				ENDFOR
				
				vPos = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRand].vPos
				fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRand].fHead
				
			ENDIF
		ELSE 
			iRand = 0
		ENDIF	
		
		IF vPos.x = 0.0
		AND vPos.y = 0.0
			PRINTSTRING("PLAYER PLACED AT CORONA")PRINTNL()
			SET_ENTITY_COORDS(PLAYER_PED_ID(), sCurrentVarsStruct.vCoronaPos)
		ELSE
			PRINTSTRING("PLAYER PLACED AT A SPAWN POINT")PRINTNL()		
			IF NOT SHOULD_USE_VEHICLES(g_FMMC_STRUCT.iTestMyTeam, iCurrentPlayerModSet)	
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
			ELSE
				IF DOES_ENTITY_EXIST(testPeds[iRand].viVehicleDM)
					DELETE_VEHICLE(testPeds[iRand].viVehicleDM)
					PRINTLN("[DMTEST] Deleting testPeds[", iRand, "].viVehicleDM")
				ENDIF
				
				PRINTLN("[DMTEST] Creating testPeds[", iRand, "].viVehicleDM for player (1)")
				testPeds[iRand].viVehicleDM = CREATE_VEHICLE(mnPlayerVeh, vPos, fHeading, FALSE, FALSE)
								
				SET_VEHICLE_ON_GROUND_PROPERLY(testPeds[iRand].viVehicleDM)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(mnPlayerVeh)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				
				IF NOT IS_VEHICLE_SEAT_FREE(testPeds[iRand].viVehicleDM)
					PED_INDEX piPedInTheWay = GET_PED_IN_VEHICLE_SEAT(testPeds[iRand].viVehicleDM)
					
					IF DOES_ENTITY_EXIST(piPedInTheWay)
						PRINTLN("[TMS][DMTEST] piPedInTheWay = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(piPedInTheWay)))
						
						IF NOT IS_PED_A_PLAYER(piPedInTheWay)
							DELETE_PED(piPedInTheWay)
							PRINTLN("[TMS][DMTEST] Deleting piPedInTheWay")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[TMS][DMTEST] Seat is free!")
				ENDIF
				
				IF IS_VEHICLE_SEAT_FREE(testPeds[iRand].viVehicleDM, VS_DRIVER)
					TASK_ENTER_VEHICLE(PLAYER_PED_ID(), testPeds[iRand].viVehicleDM, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
					
					IF NOT IS_NON_ARENA_KING_OF_THE_HILL()
					AND (IS_CURRENT_MODIFIER_SET_VALID(iCurrentPlayerModSet)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].iModifierSetBS, ciDM_ModifierBS_SpawnInVehicle))
						SET_VEHICLE_DOORS_LOCKED(testPeds[iRand].viVehicleDM, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
					ENDIF
					
					IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(testPeds[iRand].viVehicleDM))
						SET_VEHICLE_ENGINE_ON(testPeds[iRand].viVehicleDM, TRUE, TRUE)
						SET_VEHICLE_FORWARD_SPEED(testPeds[iRand].viVehicleDM, 50.0)
					ENDIF
				ELSE
					PRINTLN("[TMS][DMTEST] Seat ultimately was not free!")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciREMOVE_PLAYER_HELMETS)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, FALSE)
			REMOVE_PLAYER_HELMET(GET_PLAYER_INDEX(), TRUE)
			PRINTLN("Calling REMOVE_PLAYER_HELMET due to ciREMOVE_PLAYER_HELMETS")
		ELSE
			PRINTLN("Calling PCF_DontTakeOffHelmet due to not ciREMOVE_PLAYER_HELMETS")
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, TRUE)
			GIVE_PLAYER_STORED_HELMET()
		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		
		IF DOES_ENTITY_EXIST(testPeds[iRand].viVehicleDM)
			SET_DM_VEHICLE_COLOUR(testPeds[iRand].viVehicleDM)
		ENDIF
				
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(), TRUE)
		IF sFMMCmenu.bPlayerInvincibleTest = FALSE
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)	
		ELSE
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
		ENDIF
		
		SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), TRUE)
		SET_PED_DIES_IN_SINKING_VEHICLE(PLAYER_PED_ID(), TRUE)
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked)	
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMInitialWepLocked)							
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 5)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SMG, 200)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 300)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 40)
		ENDIF
		
		INT iAmmoToGive = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet)) * 3)
		INT iForcedWeapon = g_FMMC_STRUCT_ENTITIES.iWeaponPallet
		
		IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
			IF g_FMMC_STRUCT.iTeamForcedWeapon[g_FMMC_STRUCT.iTestMyTeam] != -1
				iForcedWeapon = g_FMMC_STRUCT.iTeamForcedWeapon[g_FMMC_STRUCT.iTestMyTeam]
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_CREATOR_INDEX(iForcedWeapon), iAmmoToGive, TRUE) 
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_WEAPONTYPE_FROM_CREATOR_INDEX(iForcedWeapon), TRUE)
			ELSE
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(iForcedWeapon), iAmmoToGive, TRUE)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(iForcedWeapon), TRUE)
			ENDIF
		ELSE
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(iForcedWeapon), iAmmoToGive, TRUE)
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(iForcedWeapon), TRUE)
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC INIT_KOTH_TEST_MODE()
	KING_OF_THE_HILL_RUNTIME_HOST_STRUCT sTemp
	sFMMCmenu.sKOTH_HostInfo = sTemp
	
	sFMMCmenu.sKOTH_PlayerInfo.iCachedKOTHTeam = g_FMMC_STRUCT.iTestMyTeam
	sFMMCmenu.sKOTH_LocalInfo.tlCachedTeamNames[sFMMCmenu.sKOTH_PlayerInfo.iCachedKOTHTeam] = "KTHH_TST"
	
	IF IS_THIS_A_TEAM_DM()
		SET_BIT(sFMMCmenu.sKOTH_HostInfo.iKingOfTheHillBS, ciKotH_Runtime__TeamMode)
	ELSE
		CLEAR_BIT(sFMMCmenu.sKOTH_HostInfo.iKingOfTheHillBS, ciKotH_Runtime__TeamMode)
	ENDIF
	
	INT iArea = 0
	
	FOR iArea = 0 TO ciKotH_MAX_HILLS - 1
		sFMMCmenu.sKOTH_LocalInfo.sHills[iArea].fKingOfTheHillScoreBuildup = 0.0
		sFMMCmenu.sKOTH_LocalInfo.sHills[iArea].fKingOfTheHillScoreBuildup = 0.0
		sFMMCmenu.sKOTH_LocalInfo.sHills[iArea].fKingOfTheHillScoreBuildup = 0.0
		sFMMCmenu.sKOTH_LocalInfo.sHills[iArea].fKingOfTheHillScoreBuildup = 0.0
	ENDFOR
	
	IF DOES_BLIP_EXIST(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biCenterBlip)
		REMOVE_BLIP(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biCenterBlip)
	ENDIF
	IF DOES_BLIP_EXIST(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biAreaBlip)
		REMOVE_BLIP(sFMMCmenu.sKOTH_LocalInfo.sHills[0].biAreaBlip)
	ENDIF
	
	sFMMCmenu.sKOTH_PlayerInfo.fKOTHScore = 0.0
	
	PRINTLN("[KOTH] Setting up KOTH test mode")
ENDPROC

PROC DISPLAY_END_OF_TEST_MENU()
	
	STRING sDescLabel = "FMMC_ENDDM2"
	
	IF IS_KING_OF_THE_HILL()
		sDescLabel = "FMMC_ENDKH2"
		SET_BIT(tutNGCreator.iTutorialBS, ciCREATOR_TUTORIAL__CONTENT_TESTED)
	ENDIF
	
	SET_WARNING_MESSAGE_WITH_HEADER("FMMC_ENDDM", sDescLabel, FE_WARNING_OK,"",FALSE,-1,"","",TRUE )
	
	IF iTestAlertStartFrame < 0
		iTestAlertStartFrame = GET_FRAME_COUNT()
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	AND iTestAlertStartFrame < GET_FRAME_COUNT()
		sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_RECREATING_MISSION
		PRINTLN("[TMSDMC] DISPLAY_END_OF_TEST_MENU - Heading to STAGE_DEAL_WITH_RECREATING_MISSION")
	ENDIF
ENDPROC

FUNC BOOL KOTH_TEST_COMPLETE()

	IF NOT IS_KING_OF_THE_HILL()
		RETURN FALSE
	ENDIF
	
	IF bPlayerIsRespawning
		PRINTLN("[KOTH] KOTH_TEST_COMPLETE - bPlayerIsRespawning")
		RETURN FALSE	
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		PRINTLN("[KOTH] KOTH_TEST_COMPLETE - Ending test early due to Numpad5!")
		sFMMCmenu.sKOTH_PlayerInfo.fKOTHScore = ciKotH_TEST_TARGET_SCORE + 1
	ENDIF
	#ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
	OR IS_SCREEN_FADING_OUT()
	OR IS_SCREEN_FADING_IN()
		//PRINTLN("[KOTH] KOTH_TEST_COMPLETE - Screen fading")
		//RETURN FALSE	
	ENDIF

	IF sFMMCmenu.sKOTH_PlayerInfo.fKOTHScore >= ciKotH_TEST_TARGET_SCORE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_SHRINKING_BOUNDS_CLEANUP()
	
	DM_SHRINKING_BOUNDS_SERVER_DATA sEmptyServer
	DM_SHRINKING_BOUNDS_CLIENT_DATA sEmptyClient
	DM_SHRINKING_BOUNDS_RUNTIME_LOCAL_STRUCT sEmptyLocal
	DEATHMATCH_BOUNDS_RUNTIME_LOCAL_STRUCT sEmptyLocalOther
	
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_DM_BOUNDS - 1	
		COPY_SCRIPT_STRUCT(sFMMCmenu.sBoundsServerData[i], sEmptyServer, SIZE_OF(DM_SHRINKING_BOUNDS_SERVER_DATA))
		COPY_SCRIPT_STRUCT(sFMMCmenu.sBoundsClientData[i], sEmptyClient, SIZE_OF(DM_SHRINKING_BOUNDS_CLIENT_DATA))
		COPY_SCRIPT_STRUCT(sFMMCmenu.sShrinkingBoundsLocalRuntimeData[i], sEmptyLocal, SIZE_OF(DM_SHRINKING_BOUNDS_RUNTIME_LOCAL_STRUCT))
		COPY_SCRIPT_STRUCT(sFMMCmenu.sDeathmatchBoundsData[i], sEmptyLocalOther, SIZE_OF(DEATHMATCH_BOUNDS_RUNTIME_LOCAL_STRUCT))
	ENDFOR
	
ENDPROC

PROC PROCESS_TEST_PED_MOD_SET_CLEANUP()
	
	TEST_PED_PLAYER_MODIFIERS sEmpty
	INT i
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		COPY_SCRIPT_STRUCT(sTestPedModifiers[i], sEmpty, SIZE_OF(TEST_PED_PLAYER_MODIFIERS))
	ENDFOR
	
	CLEAR_LONG_BITSET(iPedNeedsModifierReapplied)
	
ENDPROC

PROC END_TEST()
	SET_FRONTEND_ACTIVE(FALSE)
	SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
	DO_SCREEN_FADE_OUT(1000)
	REMOVE_SCENARIO_BLOCKING_AREAS()
	SET_IN_ARENA_MODE(FALSE)
	SET_VEHICLE_COMBAT_MODE(FALSE)
	TRIGGER_MUSIC_EVENT("MP_DM_STOP_TRACK")	
	LEGACY_CLEANUP_WORLD_PROPS(sRuntimeWorldPropData)
	CLEAR_ALL_BIG_MESSAGES()
	
	IF IS_KING_OF_THE_HILL()
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_VINEWOOD/KOTH")
		PRINTLN("[TMS] UNLOAD_SOUNDS - Releasing DLC_VINEWOOD/KOTH")
	ENDIF
	
	POWERUP_SYSTEM_CLEANUP(sPowerUps)
	
	PROCESS_FMMC_VEHICLE_MACHINE_GUN_CLEANUP(sVehMG)
	
	RESET_WEAPON_DAMAGE_MODIFIERS()
	PROCESS_DM_WEAPON_DAMAGE_MODIFIERS_CLEANUP()
	
	iTestLocalBitSet = 0
	RESET_NET_TIMER(tdPickupSpawnTimer)
	
	SET_PLAYER_TEAM(PLAYER_ID(), -1)
	
	PROCESS_PLAYER_MODIFIER_CLEANUP(sPlayerModifierData)
	PROCESS_TEST_PED_MOD_SET_CLEANUP()
	iCurrentPlayerModSet = -1
	
	PROCESS_SHRINKING_BOUNDS_CLEANUP()
	
	BOOL bMinimumScoreReached
	IF IS_LEGACY_DEATHMATCH_CREATOR()
		bMinimumScoreReached = iGlobalTestKills >= ciTest_MinimumScore
	ELSE
		bMinimumScoreReached = iGlobalTestScore >= ciTest_MinimumScore
	ENDIF
	
	RESET_NET_TIMER(tdBlockBoundsOnRespawnTimer)
	
	IF SCRIPT_IS_CLOUD_AVAILABLE()
	
		IF (bMinimumScoreReached AND NOT IS_KING_OF_THE_HILL())
		OR KOTH_TEST_COMPLETE()
			PRINTLN("[TMSDMC] Calling DISPLAY_END_OF_TEST_MENU")
			DISPLAY_END_OF_TEST_MENU()
		ELSE
			PRINTLN("[TMSDMC] Going straight to STAGE_DEAL_WITH_RECREATING_MISSION")
			sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_RECREATING_MISSION
		ENDIF
	ELSE
		sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_RECREATING_MISSION
	ENDIF
ENDPROC

PROC INIT_TEST_MODE_LIVES()
	
	iTestExtraLives = 0
	iTestDeaths = 0
	bOutofLives = FALSE
	bLivingEnemies = FALSE
	bValidLivesCheck = FALSE
	
	IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
		INT iTeamLoop
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
			iTestMaxTeamLives[iTeamLoop] = g_FMMC_STRUCT.sDMCustomSettings.iTeamLives[iTeamLoop]
			PRINTLN("INIT_TEST_MODE_LIVES - iTestMaxTeamLives[",iTeamLoop,"]: ", iTestMaxTeamLives[iTeamLoop])
			iTestTeamDeaths[iTeamLoop] = 0
			iTestTeamExtraLives[iTeamLoop] = 0
		ENDFOR
	ELSE
		iTestMaxLives = g_FMMC_STRUCT.sDMCustomSettings.iFFALives
		PRINTLN("INIT_TEST_MODE_LIVES - iTestMaxLives: ", iTestMaxLives)
	ENDIF
	
	INT iPeds
	FOR iPeds = 1 TO g_FMMC_STRUCT.iNumParticipants + 1
		testPeds[iPeds].bOutOfLives = FALSE
		testPeds[iPeds].iDeaths = 0
	ENDFOR
	
ENDPROC

FUNC BOOL SET_UP_TEST_MISSION()
	
	INT i
	BOOL bAudioReady = TRUE
		
		SWITCH iSetUpTestState
			CASE SET_UP_TEST_STATE_FADE
				PRINTSTRING("SET_UP_TEST_STATE_FADE")PRINTNL()
				if not IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				ENDIF
				
				NETWORK_SKIP_RADIO_RESET_NEXT_OPEN()
				NETWORK_SKIP_RADIO_RESET_NEXT_CLOSE()
					
				IF NOT IS_MOBILE_PHONE_RADIO_ACTIVE()
					TRIGGER_MUSIC_EVENT("MP_DM_START_ALL")
				ENDIF
				
				iSetUpTestState = SET_UP_TEST_STATE_CLEAR
			BREAK
			CASE SET_UP_TEST_STATE_CLEAR
				PRINTSTRING("SET_UP_TEST_STATE_CLEAR")PRINTNL()
				if IS_SCREEN_FADED_OUT()
					
					SET_CREATOR_AUDIO(FALSE, FALSE)
					//SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(FALSE)
					HANDLE_DM_WANTED_LEVEL(sFMMCMenu.iSelectedTeam, 0)
					SET_STORE_ENABLED(FALSE) 
					
					SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
					REFRESH_MENU(sFMMCMenu)
					
					iSeamineBS = 0
					sCurrentVarsStruct.iMenuState = MENU_STATE_TEST_MISSION
					
					IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						bOnGroundBeforeTest = TRUE
					ELSE
						bOnGroundBeforeTest = FALSE
					ENDIF
					
					ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
					SET_BIT(sFMMCdata.iBitSet, bCameraActive)		
					SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, FALSE, FALSE, FALSE)
					
					RESET_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
					
					iGlobalTestKills = 0
					iGlobalTestScore = 0
					iTestKills = 0
					iTestScore = 0
					iIndividualScore = 0
					
					INIT_TEST_MODE_LIVES()
					
					SET_ACTIVE_MENU(sFMMCMenu, eFmmc_TEST_BASE)
					SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
					INIT_CREATOR_MENU(sFMMCmenu)
					REFRESH_MENU(sFMMCMenu)
					
					IF sStartEndBlips.ciStartType != NULL
						DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
						sStartEndBlips.ciStartType = NULL
					ENDIF
					
					IF DOES_BLIP_EXIST(bPhotoBlip)
						REMOVE_BLIP(bPhotoBlip)
					ENDIF
					IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
						REMOVE_BLIP(sStartEndBlips.biStart)
					ENDIF
					IF DOES_BLIP_EXIST(bCameraPanBlip)
						REMOVE_BLIP(bCameraPanBlip)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					FOR i = 0 TO FMMC_MAX_NUM_COVER - 1
						IF DOES_BLIP_EXIST(sCoverStruct.biCover[i])
							REMOVE_BLIP(sCoverStruct.biCover[i])
						ENDIF
					ENDFOR
					#ENDIF
					
					IF DOES_ENTITY_EXIST(sCurrentVarsStruct.viMapEscapeVeh)
						DELETE_VEHICLE(sCurrentVarsStruct.viMapEscapeVeh)
					ENDIF
					FOR i = 0 TO 7
						IF DOES_ENTITY_EXIST(sPropStruct.viWaterCornerVehs[i])
							DELETE_VEHICLE(sPropStruct.viWaterCornerVehs[i])
						ENDIF
					ENDFOR
					
					IF DOES_ENTITY_EXIST(testPeds[0].viVehicleDM)
						DELETE_VEHICLE(testPeds[0].viVehicleDM)
					ENDIF
					
					IF DOES_MISSION_HAVE_ANY_PLACED_TURRETS()
						ARENA_CONTESTANT_TURRET_STACK_CLEAR()
					ENDIF
					
					FMMC_REMOVE_ALL_SPAWNS(sPedStruct, FALSE)
					FMMC_REMOVE_ALL_TEAM_START_POINTS(sTeamSpawnStruct, FALSE)
					FMMC_REMOVE_ALL_WEAPONS(sWepStruct, FALSE)
					FMMC_REMOVE_ALL_VEHICLES(sVehStruct, FALSE)
					FMMC_REMOVE_ALL_PROPS(sPropStruct, FALSE)
					FMMC_REMOVE_ALL_DYNOPROPS(sDynoPropStruct, FALSE, sObjStruct)
					FMMC_REMOVE_ALL_OBJECTS(sObjStruct, FALSE)
					
					HANDLE_TEAM_START_WEAPONS(g_FMMC_STRUCT.iTestMyTeam)					
					
					IF SCRIPT_IS_CLOUD_AVAILABLE()
						SET_FAKE_MULTIPLAYER_MODE(TRUE)
					ELSE
						g_Private_IsMultiplayerCreatorRunning = TRUE
						g_Private_MultiplayerCreatorNeedsToEnd = FALSE
					ENDIF
					
					IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(<<-7000.0, -7000.0, -1000.0>>, <<7000.0, 7000.0, 1000.0>>)
						PRINTLN("ADD_SCENARIO_BLOCKING_AREA, test deathmatch ")
						ADD_SCENARIO_BLOCKING_AREA(<<-7000.0, -7000.0, -1000.0>>, <<7000.0, 7000.0, 1000.0>>)
					ELSE
						PRINTLN("ADD_SCENARIO_BLOCKING_AREA - failed - already exists , test deathmatch")
					ENDIF
					
					// Make sure Hill vehicles can respawn.
					INT iArea
					FOR iArea = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
						IF g_FMMC_STRUCT.sKotHData.sHillData[iArea].iHillType = ciKOTH_HILL_TYPE__VEHICLE
							IF g_FMMC_STRUCT.sKotHData.sHillData[iArea].iHillEntity > -1
								SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT.sKotHData.sHillData[iArea].iHillEntity].iVehBitsetEight, ciFMMC_VEHICLE8_DM_VEHICLES_CAN_RESPAWN)
							ENDIF
						ENDIF
					ENDFOR											
					iVehicleSpawnedBitset = 0
					iVehicleRespawnBitset = 0
					iVehicleCreatedBitset = 0					
					iVehicleRespawnGhostedBitset = 0
					
					IF CONTENT_IS_USING_ARENA()
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Disable_Arena_Mode)
						SET_IN_ARENA_MODE(TRUE)
						PREVENT_LOCAL_PLAYER_FALLING_OUT_WHEN_DEAD()
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_EnableVehicleShuntCombat)
							SET_VEHICLE_COMBAT_MODE(TRUE)
							PRINTLN("[ARENA][SHUNTING] Enabling vehicle combat")
						ENDIF
						PRINTLN("[ARENA][SHUNTING] Turning on 'Arena Mode'")
					ENDIF
					
					IF NOT CONTENT_IS_USING_ARENA()
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DM_SpawnInCoronaVehicle)
						g_FMMC_STRUCT.iDefaultCommonVehicleType[g_FMMC_STRUCT.iTestMyTeam] = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.mnVehicleModel[g_FMMC_STRUCT.iTestMyTeam])
						g_FMMC_STRUCT.iDefaultCommonVehicle[g_FMMC_STRUCT.iTestMyTeam] = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(g_FMMC_STRUCT.mnVehicleModel[g_FMMC_STRUCT.iTestMyTeam])
					ENDIF
					
					REQUEST_APPLICABILITY_UPDATE(sPlayerModifierData)
					
					sFMMCmenu.iPreviousBoundsIndex = -1
					
					RESET_PLACED_PROP_ARRAY()
					
					iSetUpTestState = SET_UP_TEST_STATE_NETWORK_SETUP
				ENDIF
			BREAK
			CASE SET_UP_TEST_STATE_NETWORK_SETUP
				PRINTSTRING("SET_UP_TEST_STATE_NETWORK_SETUP")PRINTNL()
				IF IS_FAKE_MULTIPLAYER_MODE_SET()
					PRINTSTRING("IS_FAKE_MULTIPLAYER_MODE_SET = TRUE")PRINTNL()
					IF NETWORK_IS_GAME_IN_PROGRESS()
					OR NOT SCRIPT_IS_CLOUD_AVAILABLE()
						INT h,m,s
						NETWORK_GET_GLOBAL_MULTIPLAYER_CLOCK(h,m,s)
						NETWORK_OVERRIDE_CLOCK_TIME(h,m,s)
						iSetUpTestState = SET_UP_TEST_PLAYER_MODIFIERS
					ELSE
						PRINTLN("WAITING FOR NETWORK GAME")
						
						IF sCurrentVarsStruct.bNetworkBailed
							sCurrentVarsStruct.bTestModeFailed = TRUE
							iSetUpTestState = SET_UP_TEST_STATE_FINISH
						ENDIF
					ENDIF
				ELSE
					PRINTSTRING("IS_FAKE_MULTIPLAYER_MODE_SET = FALSE")PRINTNL()
					IF SCRIPT_IS_CLOUD_AVAILABLE()
						SET_FAKE_MULTIPLAYER_MODE(TRUE)
						
						g_Private_IsMultiplayerCreatorRunning = TRUE
						g_Private_MultiplayerCreatorNeedsToEnd = FALSE
					ELSE
						PRINTSTRING("SCRIPT_IS_CLOUD_AVAILABLE = FALSE")PRINTNL()
					ENDIF
				ENDIF
			BREAK
			CASE SET_UP_TEST_PLAYER_MODIFIERS
				
				SET_PLAYER_TEAM(PLAYER_ID(), g_FMMC_STRUCT.iTestMyTeam)
				
				PROCESS_KEEP_PLAYER_MODIFIER_SCORES_TO_CHECK_UP_TO_DATE(sPlayerModifierData, iIndividualScore, iTestScore)
				
				IF NOT IS_THIS_LEGACY_DM_CONTENT()
				AND NOT IS_KING_OF_THE_HILL()
					PROCESS_DEATHMATCH_INIT_SCORE_AND_TIME(sPlayerModifierData, GET_TARGET_SCORE_VALUE(g_FMMC_STRUCT.iTargetScore), GET_TEST_DURATION(g_FMMC_STRUCT.iRoundTime))
				ENDIF
				
				PROCESS_APPLICABILITY_INIT(sPlayerModifierData, iCurrentPlayerModSet, iPendingPlayerModSet)
				
				iSetUpTestState = SET_UP_TEST_STATE_CREATE_PLAYER
			BREAK
			CASE SET_UP_TEST_STATE_CREATE_PLAYER
				IF CREATE_THE_PLAYER_FOR_THE_TEST()
					NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50)	
					SET_WEATHER_FOR_FMMC_MISSION(g_FMMC_STRUCT.iWeather)
					iSetUpTestState = SET_UP_TEST_STATE_LOAD_AUDIO
				ENDIF
			BREAK
			CASE SET_UP_TEST_STATE_LOAD_AUDIO
				
				IF IS_KING_OF_THE_HILL()
					IF REQUEST_SCRIPT_AUDIO_BANK("DLC_VINEWOOD/KOTH")
						PRINTLN("DLC_VINEWOOD/KOTH has loaded!")
					ELSE
						bAudioReady = FALSE
						PRINTLN("DLC_VINEWOOD/KOTH is still loading!")
					ENDIF
				ENDIF
				
				IF bAudioReady
					PRINTLN("DM AUDIO ALL LOADED!")
					iSetUpTestState = SET_UP_TEST_STATE_PLACE_WEPS
				ENDIF
			BREAK
			
			CASE SET_UP_TEST_STATE_PLACE_WEPS
				PRINTSTRING("SET_UP_TEST_STATE_PLACE_WEPS")PRINTNL()
				IF CREATE_TEST_PICKUPS(sWepStruct, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked))
					iSetUpTestState = SET_UP_TEST_STATE_PLACE_PROPS
				ENDIF
			BREAK
			CASE SET_UP_TEST_STATE_PLACE_PROPS
				PRINTSTRING("SET_UP_TEST_STATE_PLACE_PROPS")PRINTNL()
				IF CREATE_ALL_CREATOR_PROPS(sPropStruct, iPropModelTimers, FALSE)
					iSetUpTestState = SET_UP_TEST_STATE_PLACE_DYNOPROPS
				ENDIF
			BREAK
			CASE SET_UP_TEST_STATE_PLACE_DYNOPROPS
				PRINTSTRING("SET_UP_TEST_STATE_PLACE_DYNOPROPS")PRINTNL()
				IF CREATE_ALL_CREATOR_DYNOPROPS(sDynoPropStruct, TRUE)
					iSetUpTestState = SET_UP_TEST_STATE_PLACE_OBJS
				ENDIF
			BREAK
			CASE SET_UP_TEST_STATE_PLACE_OBJS
				PRINTSTRING("SET_UP_TEST_STATE_PLACE_OBJS")PRINTNL()
				IF CREATE_ALL_CREATOR_OBJECTS(sObjStruct, sCurrentVarsStruct, TRUE, FALSE, FALSE, PICKUP_PORTABLE_CRATE_FIXED_INCAR_WITH_PASSENGERS)
					iSetUpTestState = SET_UP_TEST_STATE_PLACE_VEHS
					
					
					//SET_OBJECT_GLOW_IN_SAME_TEAM(sObjStruct.oiObject[iObj])
					//SET_TEAM_PICKUP_OBJECT(sObjStruct.oiObject[iObj], -1, TRUE)
					//SET_TEAM_PICKUP_OBJECT(sObjStruct.oiObject[iObj], -1, TRUE)
					//SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn, TRUE)
					//SET_OBJECT_TARGETTABLE(sObjStruct.oiObject[iObj], TRUE)
					//PREVENT_COLLECTION_OF_PORTABLE_PICKUP(sObjStruct.oiObject[iObj], FALSE, TRUE)
					
					ALLOW_ALL_PLAYERS_TO_COLLECT_PICKUPS_OF_TYPE(PICKUP_PORTABLE_PACKAGE)
					SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
					SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(DUMMY_MODEL_FOR_SCRIPT, -1)
				ENDIF
			BREAK
			CASE SET_UP_TEST_STATE_PLACE_VEHS
				PRINTSTRING("SET_UP_TEST_STATE_PLACE_VEHS")PRINTNL()
				IF CREATE_ALL_CREATOR_VEHICLES(sVehStruct, iVehicleModelLoadTimers, TRUE)
					// Vehicles
					INT iVeh
					FOR iVeh = 0 TO FMMC_MAX_VEHICLES-1
						IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[iVeh])
							SET_BIT(iVehicleSpawnedBitset, iVeh)
							PRINTLN("[LM][DM_ENTITY_RESPAWN][SERVER_CREATE_DM_ENTITIES] - Successfully (inital) SPAWNED iVeh: ", iVeh)
						ENDIF
					ENDFOR
					iSetUpTestState = SET_UP_TEST_STATE_PLACE_PEDS
				ENDIF
			BREAK
			CASE SET_UP_TEST_STATE_PLACE_PEDS	
				PRINTSTRING("SET_UP_TEST_STATE_PLACE_PEDS")PRINTNL()				
				REQUEST_MODEL(DM_TestPedModel[0])
				REQUEST_MODEL(DM_TestPedModel[1])
				REQUEST_MODEL(DM_TestPedModel[2])
				REQUEST_MODEL(DM_TestPedModel[3])
				
				IF KOTH_SHOULD_I_SPAWN_IN_VEHICLE(0)
					REQUEST_MODEL(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, 0))
				ENDIF
				IF KOTH_SHOULD_I_SPAWN_IN_VEHICLE(1)
					REQUEST_MODEL(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, 1))
				ENDIF
				IF KOTH_SHOULD_I_SPAWN_IN_VEHICLE(2)
					REQUEST_MODEL(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, 2))
				ENDIF
				IF KOTH_SHOULD_I_SPAWN_IN_VEHICLE(3)
					REQUEST_MODEL(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, 3))
				ENDIF
				
				IF HAS_MODEL_LOADED(DM_TestPedModel[0])
				AND HAS_MODEL_LOADED(DM_TestPedModel[1])
				AND HAS_MODEL_LOADED(DM_TestPedModel[2])
				AND HAS_MODEL_LOADED(DM_TestPedModel[3])
					IF (NOT SHOULD_USE_VEHICLES(DEFAULT, iCurrentPlayerModSet))
					OR ((NOT KOTH_SHOULD_I_SPAWN_IN_VEHICLE(0) OR HAS_MODEL_LOADED(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, 0)))
					AND (NOT KOTH_SHOULD_I_SPAWN_IN_VEHICLE(1) OR HAS_MODEL_LOADED(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, 1)))
					AND (NOT KOTH_SHOULD_I_SPAWN_IN_VEHICLE(2) OR HAS_MODEL_LOADED(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, 2)))
					AND (NOT KOTH_SHOULD_I_SPAWN_IN_VEHICLE(3) OR HAS_MODEL_LOADED(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, 3))))
						IF CREATE_THE_TEST_PEDS()
							SET_MODEL_AS_NO_LONGER_NEEDED(DM_TestPedModel[0])
							SET_MODEL_AS_NO_LONGER_NEEDED(DM_TestPedModel[1])
							SET_MODEL_AS_NO_LONGER_NEEDED(DM_TestPedModel[2])
							SET_MODEL_AS_NO_LONGER_NEEDED(DM_TestPedModel[3])
							iSetUpTestState = SET_UP_TEST_STATE_FINISH
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE SET_UP_TEST_STATE_FINISH
				PRINTSTRING("SET_UP_TEST_STATE_FINISH")PRINTNL()
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					IF IS_NEW_LOAD_SCENE_LOADED()
						IF IS_KING_OF_THE_HILL()
							INIT_KOTH_TEST_MODE()
						ENDIF
						
						INIT_POWERUP_SYSTEM(sPowerUps)
						PROCESS_DM_WEAPON_DAMAGE_MODIFIERS()
						
						
												
						TEST_TIMER = GET_GAME_TIMER() - DEFAULT_FADE_TIME
						SET_MODIFIER_SET_TIMER_REQUIREMENT_TIME_STAMP(sPlayerModifierData, -DEFAULT_FADE_TIME)
						
						DO_SCREEN_FADE_IN(500)
						NEW_LOAD_SCENE_STOP()
						iSetUpTestState = SET_UP_TEST_STATE_FADE
						
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLadderClimbing, FALSE)
						
						IF sCurrentVarsStruct.bNetworkBailed
							PRINTLN("[DM] Bailing from test because a network game is not in progress")
							END_TEST()
						ENDIF
							
						RETURN TRUE
					ELSE
						PRINTSTRING("NOT LOADED")PRINTNL()
					ENDIF
				ELSE
					PRINTSTRING("NO SCENE ACTIVE")PRINTNL()
					NEW_LOAD_SCENE_STOP()
					NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50)	
				ENDIF
			BREAK
		ENDSWITCH
		
		RETURN FALSE
	
ENDFUNC 

PROC WARP_CAMERA_TO_START_LOCATION(FMMC_CAM_DATA &sCamDataPassed, FLOAT fHeight = 40.0)

	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			PRINTLN("WARP_CAMERA_TO_START_LOCATION - Going to trigger location")
			SET_CAM_COORD(sCamDataPassed.cam, g_FMMC_STRUCT.vStartPos+<<0.0,0.0, fHeight>>)
		ENDIF
	ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[0].vPos)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			PRINTLN("WARP_CAMERA_TO_START_LOCATION - Going to spawn point 0")
			SET_CAM_COORD(sCamDataPassed.cam, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[0].vPos+<<0.0,0.0, fHeight>>)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL RESET_THE_MISSION_UP_SAFELY()

	INT i, i2

	SWITCH iResetState
		CASE RESET_STATE_FADE
			PRINTLN("RESET_STATE_FADE")
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = TRUE 
				
				SET_OVERRIDE_WEATHER("CLEAR")
				
			    IF SCRIPT_IS_CLOUD_AVAILABLE()
					PRINTLN("SCRIPT_IS_CLOUD_AVAILABLE()")
				    SET_FAKE_MULTIPLAYER_MODE(FALSE)
				ELSE
					PRINTLN("NOT SCRIPT_IS_CLOUD_AVAILABLE()")
					//NETWORK_SESSION_LEAVE_SINGLE_PLAYER() 
					g_Private_IsMultiplayerCreatorRunning = FALSE
					g_Private_MultiplayerCreatorNeedsToEnd = TRUE
				ENDIF
			ENDIF
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE 
				iResetState = RESET_STATE_CLEAR
				RESET_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())				
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)	
				ENDIF
				
				IF bOnGroundBeforeTest = FALSE
					CLEAR_BIT(sFMMCdata.iBitSet, bCameraActive)
					PRINTLN("CLEAR_BIT(sFMMCdata.iBitSet, bCameraActive) GO BACK TO CAMERA")
				ELSE
					SET_BIT(sFMMCdata.iBitSet, bCameraActive)	
					PRINTLN("SET_BIT(sFMMCdata.iBitSet, bCameraActive) GO BACK TO GROUND")
				ENDIF
				SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, FALSE, FALSE, FALSE)
			ENDIF
			
		BREAK
		
		CASE RESET_STATE_CLEAR
			PRINTLN("RESET_STATE_CLEAR")
			IF (NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			AND NOT NETWORK_IS_GAME_IN_PROGRESS())
			OR NOT SCRIPT_IS_CLOUD_AVAILABLE()
				FOR i = 0 TO (FMMC_MAX_SPAWNPOINTS - 1)
					IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])
						REMOVE_BLIP(sPedStruct.biPedBlip[i])
					ENDIF
					IF DOES_ENTITY_EXIST(sPedStruct.piPed[i])
						DELETE_PED(sPedStruct.piPed[i])
					ENDIF
					IF DOES_ENTITY_EXIST(testPeds[i].pedID)
						DELETE_PED(testPeds[i].pedID)
					ENDIF
					IF DOES_BLIP_EXIST(testPeds[i].blipID)
						REMOVE_BLIP(testPeds[i].blipID)
					ENDIF
					
					IF DOES_ENTITY_EXIST(testPeds[i].viVehicleDM)
						SET_ENTITY_AS_MISSION_ENTITY(testPeds[i].viVehicleDM, TRUE, TRUE)
						DELETE_VEHICLE(testPeds[i].viVehicleDM)
					ENDIF
					
					IF IS_DECAL_ALIVE(sPedStruct.diDecal[i])
						REMOVE_DECAL(sPedStruct.diDecal[i])
					ENDIF
					testPeds[i].iTeam = 0
				ENDFOR
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
					IF DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[i])
						REMOVE_BLIP(sVehStruct.biVehicleBlip[i])
					ENDIF
					IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[i])
						SET_ENTITY_AS_MISSION_ENTITY(sVehStruct.veVehcile[i], TRUE, TRUE)
						DELETE_VEHICLE(sVehStruct.veVehcile[i])
					ENDIF
				ENDFOR				
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1)
					IF DOES_BLIP_EXIST(sPropStruct.biObject[i])
						REMOVE_BLIP(sPropStruct.biObject[i])
					ENDIF
					if DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
						DELETE_OBJECT(sPropStruct.oiObject[i])
					ENDIF
				ENDFOR						
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1)
					IF DOES_BLIP_EXIST(sDynoPropStruct.biObject[i])
						REMOVE_BLIP(sDynoPropStruct.biObject[i])
					ENDIF
					if DOES_ENTITY_EXIST(sDynoPropStruct.oiObject[i])
						DELETE_OBJECT(sDynoPropStruct.oiObject[i])
					ENDIF
				ENDFOR	
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
					IF DOES_BLIP_EXIST(sObjStruct.biObject[i])
						REMOVE_BLIP(sObjStruct.biObject[i])
					ENDIF
					IF DOES_ENTITY_EXIST(sObjStruct.oiObject[i])
						DELETE_OBJECT(sObjStruct.oiObject[i])
					ENDIF
					IF sObjStruct.iDecalNum[i] != -1
						IF IS_DECAL_ALIVE(sObjStruct.diDecal[sObjStruct.iDecalNum[i]])
							REMOVE_DECAL(sObjStruct.diDecal[sObjStruct.iDecalNum[i]])
						ENDIF
					ENDIF
				ENDFOR
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1)
					IF DOES_PICKUP_EXIST(sWepStruct.Pickups[i])
						REMOVE_PICKUP(sWepStruct.Pickups[i])
					ENDIF
					IF DOES_BLIP_EXIST(sWepStruct.biWeaponBlip[i])
						REMOVE_BLIP(sWepStruct.biWeaponBlip[i])
					ENDIF
					if DOES_ENTITY_EXIST(sWepStruct.oiWeapon[i])
						DELETE_OBJECT(sWepStruct.oiWeapon[i])
					ENDIF
					IF sWepStruct.iDecalNum[i] != -1
						IF IS_DECAL_ALIVE(sWepStruct.diDecal[sWepStruct.iDecalNum[i]])
							REMOVE_DECAL(sWepStruct.diDecal[sWepStruct.iDecalNum[i]])
						ENDIF
					ENDIF
				ENDFOR
				IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
					REMOVE_BLIP(sStartEndBlips.biStart)
				ENDIF							
				IF sStartEndBlips.ciStartType != NULL
					DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
					sStartEndBlips.ciStartType = NULL
				ENDIF
				
				RESET_PLACED_PROP_ARRAY()
				
				iResetState = RESET_STATE_WEAPONS
			ENDIF
		BREAK
		
		CASE RESET_STATE_WEAPONS
			PRINTLN("RESET_STATE_WEAPONS")
			IF CREATE_ALL_CURRENT_PICKUPS(sWepStruct, g_FMMC_STRUCT.iVehicleDeathmatch = 0, sCurrentVarsStruct)
				iResetState = RESET_STATE_PROPS
			ENDIF
		BREAK
		
		CASE RESET_STATE_PROPS
			IF CREATE_ALL_CREATOR_PROPS(sPropStruct, iPropModelTimers)
				iResetState = RESET_STATE_DYNOPROPS
			ENDIF
		BREAK
		
		CASE RESET_STATE_DYNOPROPS
			IF CREATE_ALL_CREATOR_DYNOPROPS(sDynoPropStruct)
				iResetState = RESET_STATE_OBJECTS
			ENDIF
		BREAK
		
		CASE RESET_STATE_OBJECTS
			PRINTLN("RESET_STATE_OBJECTS")
			IF CREATE_ALL_CREATOR_OBJECTS(sObjStruct, sCurrentVarsStruct, FALSE)
				iResetState = RESET_STATE_VEHICLES
			ENDIF			
		BREAK
		
		CASE RESET_STATE_VEHICLES
			PRINTLN("RESET_STATE_VEHICLES")
			IF CREATE_ALL_CREATOR_VEHICLES(sVehStruct, iVehicleModelLoadTimers)
				iResetState = RESET_STATE_ATTACH
			ENDIF			
		BREAK
		
		CASE RESET_STATE_ATTACH
			PRINTLN("RESET_STATE_ATTACH")
			FOR i = 0 TO FMMC_MAX_VEHICLES-1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent >= 0
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType = CREATION_TYPE_VEHICLES
						FMMC_ATTACH_VEHICLE_TO_VEHICLE(sVehStruct.veVehcile[i], sVehStruct.veVehcile[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType = CREATION_TYPE_OBJECTS
						FMMC_ATTACH_VEHICLE_TO_OBJECT(sVehStruct.veVehcile[i], sObjStruct.oiObject[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
					ENDIF
				ENDIF
			ENDFOR
			iResetState = RESET_STATE_SPAWNS
		BREAK
		
		CASE RESET_STATE_SPAWNS
			PRINTLN("RESET_STATE_SPAWNS")			
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints - 1)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos)		
					IF NOT DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])
						CREATE_FMMC_BLIP(sPedStruct.biPedBlip[i], g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, HUD_COLOUR_BLUELIGHT, "FMMC_B_3", 1)
						CREATE_SPAWN_POINT_DECAL(sPedStruct, i, FALSE, sCurrentVarsStruct.bCanCreateADecalThisFrame)
												
						PRINTLN("CREATED A SPAWN BLIP AND DECAL ", i)	
						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDFOR	
			
			iResetState = RESET_STATE_TEAM_SPAWNS
			
		BREAK
		
		CASE RESET_STATE_TEAM_SPAWNS
			PRINTLN("RESET_STATE_TEAM_SPAWNS")		
			FOR i = 0 TO (FMMC_MAX_TEAMS - 1)		
				FOR i2 = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] - 1)
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].vPos)		
						IF NOT DOES_BLIP_EXIST(sTeamSpawnStruct[i].biPedBlip[i2])
							CREATE_FMMC_BLIP(sTeamSpawnStruct[i].biPedBlip[i2], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].vPos, HUD_COLOUR_WHITE, getNameForTeamSpawnBlip(i), 1)
							SET_BLIP_COLOUR(sTeamSpawnStruct[i].biPedBlip[i2], getColourForSpawnBlip(i))	
							CREATE_TEAM_SPAWN_POINT_DECAL(sTeamSpawnStruct[i], i2, FALSE, sCurrentVarsStruct.bCanCreateADecalThisFrame, i)
							
							PRINTLN("CREATED A TEAM SPAWN BLIP AND DECAL ", i)	
							
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
			ENDFOR
			
			iResetState = RESET_STATE_OTHER
			
		BREAK
		
		CASE RESET_STATE_OTHER
			PRINTLN("RESET_STATE_OTHER")
			REQUEST_STREAMED_TEXTURE_DICT("MPMissMarkers256")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPMissMarkers256")
				HAS_PATCHED_START_LOGO_STATE_SWAPPED(TRUE, 3)
				
					PRINTLN("!! CHECK FOR THE NUM OF VALID TEAMS !!")
					PRINTLN("iMaxNumberOfTeams = ", g_FMMC_STRUCT.iMaxNumberOfTeams)
					
					FOR i = 0 TO 3
						IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] >= iMaxSpawnsPerTeam[i]
						AND i < g_FMMC_STRUCT.iMaxNumberOfTeams
							PRINTLN("Team ", i, " is valid - Num = ", g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i], " Max on Team = ", iMaxSpawnsPerTeam[i])
							SET_BIT(g_FMMC_STRUCT_ENTITIES.iBS_AvailableDMTeams, i)
						ELSE
							PRINTLN("Team ", i, " is NOT valid - Num = ", g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i], " Max on Team = ", iMaxSpawnsPerTeam[i])
							CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.iBS_AvailableDMTeams, i)
						ENDIF
					ENDFOR	
				
				g_FMMC_STRUCT.iContactChar = CONVERT_CHARACTER_INT_TO_MENU_OPTION(g_FMMC_STRUCT.iContactCharEnum)
				
				if NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
					SET_ALL_MENU_ITEMS_ACTIVE()
					
					CREATE_TRIGGER_BLIP_AND_CORONA_WITH_DECAL(sStartEndBlips.biStart, sHCS.hcStartCoronaColour, sStartEndBlips.ciStartType)
					
				ELSE
					SET_DM_STARTING_MENU_ITEMS_ACTIVE(sFMMCmenu)
				ENDIF
					
				iResetState = RESET_STATE_FINISH
			
			ENDIF
		BREAK
		CASE RESET_STATE_FINISH
			PRINTLN("RESET_STATE_FINISH")
//			IF DOES_ENTITY_EXIST(testPeds[i].viVehicleDM)
//				SET_ENTITY_AS_MISSION_ENTITY(testPeds[i].viVehicleDM, TRUE, TRUE)
//				DELETE_VEHICLE(testPeds[i].viVehicleDM)
//			ENDIF
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//Check to see if the test state should switch
PROC SWITCH_TEST_STATE()
	IF IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
			IF SET_UP_TEST_MISSION()
				
				PRINTSTRING("test set up!!!")PRINTNL()
				iHelpBitSetOld = -1
				
				SET_ALL_VEHICLE_GENERATORS_ACTIVE()
				SET_VEHICLE_POPULATION_BUDGET(3)
				SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)
				DISABLE_VEHICLE_DISTANTLIGHTS(FALSE)
				
				SET_BIGMAP_ACTIVE(FALSE, FALSE)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
				SET_BIT(sFMMCdata.iBitSet, biTestMissionActive)
				bTestStatBeenIncreased = FALSE
			
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				iTestAlertStartFrame = -1
				
				LEGACY_INITIALISE_WORLD_PROPS()
				
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				END_TEST()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////         		RANDOMIZER!!!           		//////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////	
  
/// PURPOSE:
///    Makes sure the position passed in is valid
/// PARAMS:
///    NewLocation - Check this vector against all other used vectors
/// RETURNS:
///    TRUE - If position passed in is far enough away from other points made
///    FALSE - Otherwise
FUNC BOOL RAND_IS_POSITION_VALID(VECTOR NewLocation)
	
	INT i
	VECTOR vPosSpawn
	FLOAT fRandomValue
	
	IF IS_PLACEMENT_IN_A_RESTRICTED_AREA(NewLocation)
		return FALSE
	ENDIF
	
	SWITCH iSpawnTypeCheck
		CASE 0 // Weapons
		
			IF FMMC_IS_THIS_A_SLOPE(NewLocation, NULL)
				return FALSE
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
				FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints - 1
					IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])			
						fRandomValue = GET_RANDOM_FLOAT_IN_RANGE(3, 8)
						vPosSpawn = GET_BLIP_COORDS(sPedStruct.biPedBlip[i])
						IF  (NewLocation.x + fRandomValue) > vPosSpawn.x
						AND (NewLocation.y + fRandomValue) > vPosSpawn.y
						AND (NewLocation.z + fRandomValue/2) > vPosSpawn.z
						AND (NewLocation.x - fRandomValue) < vPosSpawn.x
						AND (NewLocation.y - fRandomValue) < vPosSpawn.y
						AND (NewLocation.z - fRandomValue/2) < vPosSpawn.z
							IF NOT IS_VECTOR_ZERO(vPosSpawn)
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				RETURN TRUE
			ELSE
				FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons	- 1			
					fRandomValue = GET_RANDOM_FLOAT_IN_RANGE(0, 5)
					IF VDIST(NewLocation,g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos) < 2 + fRandomValue
						RETURN FALSE
					ENDIF
				ENDFOR
				FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints - 1	
					IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])		
						fRandomValue = GET_RANDOM_FLOAT_IN_RANGE(3, 8)
						vPosSpawn = GET_BLIP_COORDS(sPedStruct.biPedBlip[i])
						IF  (NewLocation.x + fRandomValue) > vPosSpawn.x
						AND (NewLocation.y + fRandomValue) > vPosSpawn.y
						AND (NewLocation.z + fRandomValue) > vPosSpawn.z
						AND (NewLocation.x - fRandomValue) < vPosSpawn.x
						AND (NewLocation.y - fRandomValue) < vPosSpawn.y
						AND (NewLocation.z - fRandomValue) < vPosSpawn.z
							IF NOT IS_VECTOR_ZERO(vPosSpawn)
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				RETURN TRUE
								
			ENDIF
			
			
		BREAK
		
		CASE 1 // Spawn points
			FLOAT fCoronaArea
			
			if g_FMMC_STRUCT.iVehicleDeathmatch = 1
				fCoronaArea = 2.0
			ELSE
				fCoronaArea = 0.5
			ENDIF
			
			IF FMMC_IS_THIS_A_SLOPE(NewLocation, NULL, 12, fCoronaArea)
				return FALSE
			ENDIF
		
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints = 0
			
				FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1		
					fRandomValue = GET_RANDOM_FLOAT_IN_RANGE(3, 8)
					vPosSpawn = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos
					IF  (NewLocation.x + fRandomValue) >  (vPosSpawn.x)
					AND (NewLocation.y + fRandomValue) >  (vPosSpawn.y)
					AND (NewLocation.z + fRandomValue) >  (vPosSpawn.z)
					AND (NewLocation.x - fRandomValue) <  (vPosSpawn.x)
					AND (NewLocation.y - fRandomValue) <  (vPosSpawn.y)
					AND (NewLocation.z - fRandomValue) <  (vPosSpawn.z)
						IF NOT IS_VECTOR_ZERO(vPosSpawn)
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
				
				RETURN TRUE
			ELSE
				FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints - 1
				
					fRandomValue = GET_RANDOM_FLOAT_IN_RANGE(0, 5)
					vPosSpawn = GET_BLIP_COORDS(sPedStruct.biPedBlip[i])
					IF  (NewLocation.x + 10.0 + fRandomValue) >  (vPosSpawn.x)
					AND (NewLocation.y + 10.0 + fRandomValue) >  (vPosSpawn.y)
					AND (NewLocation.z + 2.5 + (fRandomValue/2)) >  (vPosSpawn.z)
					AND (NewLocation.x - 10.0 - fRandomValue) <  (vPosSpawn.x)
					AND (NewLocation.y - 10.0 - fRandomValue) <  (vPosSpawn.y)
					AND (NewLocation.z - 2.5 - (fRandomValue/2)) <  (vPosSpawn.z)
						IF NOT IS_VECTOR_ZERO(vPosSpawn)
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
				
				FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1
					fRandomValue = GET_RANDOM_FLOAT_IN_RANGE(3, 8)
					vPosSpawn = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos
					IF  (NewLocation.x + fRandomValue) >  (vPosSpawn.x)
					AND (NewLocation.y + fRandomValue) >  (vPosSpawn.y)
					AND (NewLocation.z + fRandomValue) >  (vPosSpawn.z)
					AND (NewLocation.x - fRandomValue) <  (vPosSpawn.x)
					AND (NewLocation.y - fRandomValue) <  (vPosSpawn.y)
					AND (NewLocation.z - fRandomValue) <  (vPosSpawn.z)
						IF NOT IS_VECTOR_ZERO(vPosSpawn)
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
				
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_INT("SPAWN POINT IS NOT VALID!!! ", 1)
				#ENDIF
				
				RETURN TRUE
				
			ENDIF
		
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC REMOVE_ENTRY_IN_RANDOM_ARRAY(INT iStart)

	INT i
	
	FOR i = iStart TO iTotalRandomSpawnsFound
		
		IF i != iStart		
			vRandomArray[i - 1] = vRandomArray[i]
		ENDIF
		
	ENDFOR
	
	iTotalRandomSpawnsFound--
	
ENDPROC

FUNC BOOL VALID_SPACE_FOR_MORE_SPAWNS()
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints < FMMC_MAX_SPAWNPOINTS
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles making the spawn points during random placememnt
/// RETURNS:
///    TRUE - If all spawn points have been placed
///    FALSE - If a spawn location is not valid 
FUNC BOOL DO_CREATE_SPAWN()

	INT i	
	VECTOR vSpawnLocation
	
	PRINTLN("[LH][RANDOMIZE] sRandStruct.iPedLevel = ", sRandStruct.iPedLevel)
	PRINTLN("[LH][RANDOMIZE] sFMMCmenu.sRandStruct.iPedLevel = ", sFMMCmenu.sRandStruct.iPedLevel)
	
	IF sRandStruct.iPedLevel > 0
		IF iNumOfRandomPlacedItems < sRandStruct.iPedLevel * FLOOR(sRandStruct.fRadius/5)
			IF iTotalRandomSpawnsFound >= 0
				IF VALID_SPACE_FOR_MORE_SPAWNS()
					iSpawnNumber = GET_RANDOM_INT_IN_RANGE(0, iTotalRandomSpawnsFound)
					vSpawnLocation = vRandomArray[iSpawnNumber]
					
					PRINTLN(iSpawnNumber, " = ", vSpawnLocation)
					
					IF RAND_IS_POSITION_VALID(vSpawnLocation)
						IF NOT IS_VECTOR_ZERO(vSpawnLocation)
							FOR i = 0 TO FMMC_MAX_SPAWNPOINTS - 1
								IF NOT DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])
								
									g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos =  vSpawnLocation
									g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].fHead = GET_RANDOM_FLOAT_IN_RANGE(0, 360)
									
									CREATE_FMMC_BLIP(sPedStruct.biPedBlip[i], g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, HUD_COLOUR_BLUELIGHT, "FMMC_B_3", 1)
									
									CREATE_SPAWN_POINT_DECAL(sPedStruct, i, TRUE, sCurrentVarsStruct.bCanCreateADecalThisFrame)
									
									g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints++
									iNumOfRandomPlacedItems++
									
									REMOVE_ENTRY_IN_RANDOM_ARRAY(iSpawnNumber)						
									RETURN FALSE
								ENDIF
							ENDFOR
						ELSE
						
							REMOVE_ENTRY_IN_RANDOM_ARRAY(iSpawnNumber)						
							RETURN FALSE
						ENDIF
					ELSE
					
						REMOVE_ENTRY_IN_RANDOM_ARRAY(iSpawnNumber)			
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN TRUE
	
	
ENDFUNC 

/// PURPOSE:
///   Handles making the weapons during random placement.
/// RETURNS:
///   TRUE - If all weapons have been made and placed successfully. 
///   FALSE - If for any reason a weapon cannot be placed.
FUNC BOOL DO_CREATE_WEAPONS()

	INT i	
	VECTOR vSpawnLocation
	INT iRandWep
	FLOAT fGroundZ
	BOOL bNotVDM
	
	if g_FMMC_STRUCT.iVehicleDeathmatch = 0
		bNotVDM = TRUE					
	ELSE
		bNotVDM = FALSE
	ENDIF
	
	IF GET_USED_CREATOR_BUDGET() >= 1
		RETURN TRUE
	ENDIF

	IF sRandStruct.iWeaponLevel > 0
		IF iNumOfRandomPlacedItems < (sRandStruct.iWeaponLevel * FLOOR(sRandStruct.fRadius/10))
			IF iTotalRandomSpawnsFound >= 0
				iSpawnNumber = GET_RANDOM_INT_IN_RANGE(0, iTotalRandomSpawnsFound)
				vSpawnLocation = vRandomArray[iSpawnNumber]
				
				IF GET_GROUND_Z_FOR_3D_COORD(vSpawnLocation + <<0,0,1>>, fGroundZ)
					IF vSpawnLocation.z < fGroundZ
						PRINTLN("DO_CREATE_WEAPONS - Spawn location ", iSpawnNumber, " is under the ground!")
						vRandomArray[iSpawnNumber] = <<0,0,0>>
						vSpawnLocation = <<0,0,0>>
					ENDIF
				ENDIF
				
				PRINTLN(iSpawnNumber, " = ", vSpawnLocation)
				
				IF RAND_IS_POSITION_VALID(vSpawnLocation)
					IF NOT IS_VECTOR_ZERO(vSpawnLocation)
						FOR i = 0 TO FMMC_MAX_WEAPONS - 1
						
							IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons >= FMMC_MAX_WEAPONS
								PRINTLN("DO_CREATE_WEAPONS - Breaking loop because we have too many weapons")
								RETURN TRUE
							ENDIF
							
							IF NOT DOES_ENTITY_EXIST(sWepStruct.oiWeapon[i])
							
								IF bNotVDM
									iRandWep = GET_RANDOM_INT_IN_RANGE(0, FOOT_MAX_ARMOURY_TYPES - 1)	
								ELSE
									iRandWep = GET_RANDOM_INT_IN_RANGE(0, 8)
								ENDIF
								
								iRandWep = GET_NON_DLC_WEAPON_FOR_RANDOM_PLACEMENT(iRandWep, sWepStruct)
								
								MODEL_NAMES mnPickUpModel = GET_PICKUP_MODEL(iRandWep,sWepStruct, bNotVDM, i)
								g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt = GET_PICKUP_TYPE_FROM_WEAPON_TYPE(sWepStruct.wtGunType[iRandWep], bNotVDM)	
							
								IF DOES_BLIP_EXIST(sWepStruct.biWeaponBlip[i])
									REMOVE_BLIP(sWepStruct.biWeaponBlip[i])
								ENDIF
								
								g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos =  vSpawnLocation								
								IF GET_GROUND_Z_FOR_3D_COORD(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos, fGroundZ)
									g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos.z = fGroundZ+0.5
								ELSE
									g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos.z = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos.z+0.5
								ENDIF
								
								REQUEST_MODEL(mnPickUpModel)
								IF NOT HAS_MODEL_LOADED(mnPickUpModel)
									PRINTLN("DO_CREATE_WEAPONS - Returning FALSE || Waiting for ", GET_MODEL_NAME_FOR_DEBUG(mnPickUpModel), " to load")
									RETURN FALSE
								ENDIF
								
								CREATE_WEAPON_FMMC(sWepStruct.oiWeapon[i] , i, mnPickUpModel, sCurrentVarsStruct)
								CREATE_WEAPON_DECAL(sWepStruct, i, TRUE, sCurrentVarsStruct.bCanCreateADecalThisFrame)
								
								ADD_MODEL_TO_CREATOR_BUDGET(mnPickUpModel)
								
								SET_ENTITY_ROTATION(sWepStruct.oiWeapon[i], g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vRot)
								FREEZE_ENTITY_POSITION(sWepStruct.oiWeapon[i], TRUE)
								SET_ENTITY_COLLISION(sWepStruct.oiWeapon[i], FALSE)
								g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons++
								
								iNumOfRandomPlacedItems++
								FLOAT fBlipSize
								IF IS_SPECIAL_WEAPON_BLIP(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
									fBlipSize = 1
								ELSE
									fBlipSize = 1
								ENDIF
								CREATE_FMMC_BLIP(sWepStruct.biWeaponBlip[i], GET_ENTITY_COORDS(sWepStruct.oiWeapon[i]), HUD_COLOUR_GREEN, "", fBlipSize)
								SET_BLIP_SPRITE(sWepStruct.biWeaponBlip[i] , GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt))
								
								IF NOT IS_PICKUP_TYPE_INVALID_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
									SET_WEAPON_BLIP_NAME_FROM_PICKUP_TYPE(sWepStruct.biWeaponBlip[i], g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
								ENDIF
								
								REMOVE_ENTRY_IN_RANDOM_ARRAY(iSpawnNumber)	
								
								IF GET_USED_CREATOR_BUDGET() < 1								
									RETURN FALSE
								ELSE
									RETURN TRUE
								ENDIF
							ENDIF
						ENDFOR
					ELSE 
						REMOVE_ENTRY_IN_RANDOM_ARRAY(iSpawnNumber)						
						RETURN FALSE
					ENDIF
				ELSE
					REMOVE_ENTRY_IN_RANDOM_ARRAY(iSpawnNumber)			
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN TRUE
	
ENDFUNC

PROC CONVERT_SEARCH_RESULTS_INTO_AN_ARRAY(INT iNumberOfSpawnPoints)
	INT i
	FLOAT fOut_X, fOut_Y, fOut_Z
	
	FOR i = 0 to iNumberOfSpawnPoints
		SPAWNPOINTS_GET_SEARCH_RESULT(i, fOut_X, fOut_Y, fOut_Z)
		vRandomArray[i].x = fOut_X
		vRandomArray[i].y = fOut_Y
		vRandomArray[i].z = fOut_Z	
	ENDFOR
	
ENDPROC

PROC DISPLAY_DEATHMATCH_BOUNDS()
	/*IF NOT DOES_BLIP_EXIST(biBlipBounds)
		biBlipBounds = ADD_BLIP_FOR_RADIUS(sCurrentVarsStruct.vCoronaPos, g_FMMC_STRUCT.fDeathmatchBounds)
		SET_BLIP_COLOUR(biBlipBounds, BLIP_COLOUR_RED)
	ENDIF
	
	IF DOES_BLIP_EXIST(biBlipBounds)
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
			SET_BLIP_COORDS(biBlipBounds, sCurrentVarsStruct.vCoronaPos)
		ELSE
			SET_BLIP_COORDS(biBlipBounds, g_FMMC_STRUCT.vStartPos)
		ENDIF
	ENDIF*/
	
	FLOAT fRadius = g_FMMC_STRUCT.fDeathmatchBounds
		
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.vStartPos, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<fRadius, fRadius, fRadius>>, 255, 255, 255, 50, FALSE, FALSE, EULER_XYZ, FALSE, NULL_STRING(),NULL_STRING(),TRUE)
		ELSE
			DRAW_MARKER(MARKER_SPHERE, sCurrentVarsStruct.vCoronaPos, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<fRadius, fRadius, fRadius>>, 255, 255, 255, 50, FALSE, FALSE, EULER_XYZ, FALSE, NULL_STRING(),NULL_STRING(),TRUE)
		ENDIF
	ELSE
		
	ENDIF
ENDPROC

FUNC BOOL IS_DEATHMATCH_SET_BY_BOUNDS()
	RETURN g_FMMC_STRUCT.fDeathmatchBounds > 0
ENDFUNC

PROC DO_RANDOMIZE_DM()
	
	PRINTLN("DO_RANDOMIZE_DM - sCurrentVarsStruct.iRandomCreationStatus: ", sCurrentVarsStruct.iRandomCreationStatus, " | ", sFMMCmenu.sRandStruct.iPedLevel, " / ", sFMMCmenu.sRandStruct.iWeaponLevel)
	
	SWITCH sCurrentVarsStruct.iRandomCreationStatus
		CASE DM_RAND_INIT
			
			#IF IS_DEBUG_BUILD
			NET_PRINT("*BAH - RANDOMIZER INIT")NET_NL()
			#ENDIF
			iSpawnNumber = 0
			iSpawnTypeCheck = 1
			iNumOfRandomPlacedItems = 0
			
			#IF IS_DEBUG_BUILD
			NET_PRINT("*BAH - INIT weapon creation")NET_NL()
			#ENDIF
			
			IF HAS_SCALEFORM_MOVIE_LOADED(SF_RandomizerMovie)
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SF_RandomizerMovie)
			ENDIF
			
			sRandStruct = sFMMCmenu.sRandStruct
			
			IF (sRandStruct.iPedLevel = 0 OR sRandStruct.bPeds)
			AND (sRandStruct.iWeaponLevel = 0 OR sRandStruct.bWeps)
				
				sCurrentVarsStruct.iRandomCreationStatus = DM_RAND_INIT
	
				
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
				SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
				
				SET_UP_DEATHMATCH_RANDOMIZER_MENU(sFMMCmenu)	
				CLEAR_BIT(iHelpBitSet,biRandomizing)
				sCurrentVarsStruct.bResetUpHelp = TRUE
				
				PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
				
			ELSE
				IF NOT SPAWNPOINTS_IS_SEARCH_ACTIVE()
					SET_BIT(iHelpBitSet,biRandomizing)
					
					PLAY_SOUND_FRONTEND(-1, "SELECT", GET_SOUND_SET_FROM_CREATOR_TYPE())
					PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
					vRandomCentre = sCurrentVarsStruct.vCoronaPos
					SPAWNPOINTS_START_SEARCH(vRandomCentre,sRandStruct.fRadius,20.0,SPAWNPOINTS_FLAG_ALLOW_ROAD_POLYS | SPAWNPOINTS_FLAG_ALLOW_ISOLATED_POLYS | SPAWNPOINTS_FLAG_MAY_SPAWN_IN_EXTERIOR, 8)
				ELSE
					IF SPAWNPOINTS_IS_SEARCH_COMPLETE()
						sFMMCendStage.bMajorEditOnLoadedMission = TRUE
						iTotalRandomSpawnsFound = SPAWNPOINTS_GET_NUM_SEARCH_RESULTS() - 1
						CONVERT_SEARCH_RESULTS_INTO_AN_ARRAY(iTotalRandomSpawnsFound)
						#IF IS_DEBUG_BUILD
						DEBUG_PRINT_INT("NUMBER OF SPAWNS = ", iTotalRandomSpawnsFound)
						#ENDIF
						sCurrentVarsStruct.iRandomCreationStatus = DM_RAND_SETUP_SPAWNS
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
	 	CASE DM_RAND_SETUP_SPAWNS
			IF IS_SCREEN_FADED_OUT()
				IF DO_CREATE_SPAWN()
					iNumOfRandomPlacedSpawns = iNumOfRandomPlacedItems
					iNumOfRandomPlacedItems = 0
					iSpawnNumber = 0
					iSpawnTypeCheck = 0
					
					sCurrentVarsStruct.iRandomCreationStatus = DM_RAND_INIT_SPAWNS
					SPAWNPOINTS_CANCEL_SEARCH()
				ENDIF
			ELSE
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(500)
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_RAND_INIT_SPAWNS
			IF NOT SPAWNPOINTS_IS_SEARCH_ACTIVE()
				SPAWNPOINTS_START_SEARCH(vRandomCentre,sRandStruct.fRadius,20.0,SPAWNPOINTS_FLAG_ALLOW_ROAD_POLYS | SPAWNPOINTS_FLAG_ALLOW_ISOLATED_POLYS | SPAWNPOINTS_FLAG_MAY_SPAWN_IN_EXTERIOR, 5 + sRandStruct.fRadius/7)
			ELSE
				IF SPAWNPOINTS_IS_SEARCH_COMPLETE()
					iTotalRandomSpawnsFound = SPAWNPOINTS_GET_NUM_SEARCH_RESULTS() - 1
					CONVERT_SEARCH_RESULTS_INTO_AN_ARRAY(iTotalRandomSpawnsFound)
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_INT("NUMBER OF SPAWNS = ", iTotalRandomSpawnsFound)
					#ENDIF
					sCurrentVarsStruct.iRandomCreationStatus = DM_RAND_SETUP_WEPS
				ENDIF
			ENDIF				
		BREAK
		
		CASE DM_RAND_SETUP_WEPS
		IF DO_CREATE_WEAPONS()		
				iNumOfRandomPlacedWeapons = iNumOfRandomPlacedItems	
				iNumOfRandomPlacedItems = 0				
				iSpawnTypeCheck = 2				
				sCurrentVarsStruct.iRandomCreationStatus = DM_RAND_DONE
			ENDIF
		BREAK
		
		CASE DM_RAND_DONE
		
			iRandomizeResultsTimer = GET_GAME_TIMER() + 7000
			SF_RandomizerMovie = REQUEST_SCALEFORM_MOVIE("MIDSIZED_MESSAGE")
			IF HAS_SCALEFORM_MOVIE_LOADED (SF_RandomizerMovie)				
				BEGIN_SCALEFORM_MOVIE_METHOD(SF_RandomizerMovie, "SHOW_COND_SHARD_MESSAGE")//"SHOW_SHARD_MIDSIZED_MESSAGE")
					IF iNumOfRandomPlacedSpawns + iNumOfRandomPlacedWeapons > 0
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_RANDDONE")
					ELSE
						BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_RANDFAIL")
					ENDIF
					END_TEXT_COMMAND_SCALEFORM_STRING()
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_RANDRES")
						ADD_TEXT_COMPONENT_INTEGER(iNumOfRandomPlacedSpawns)
						ADD_TEXT_COMPONENT_INTEGER(iNumOfRandomPlacedWeapons)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()

	
				#IF IS_DEBUG_BUILD
				NET_PRINT("*BAH - RANDOMIZER DONE")NET_NL()
				#ENDIF
				SPAWNPOINTS_CANCEL_SEARCH()
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
				
				SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
				sCurrentVarsStruct.creationStats.bMadeAChange = TRUE
				PRINTLN("CHECK_FOR_END_CONDITIONS_MET bMadeAChange = true")
				
				SET_UP_DEATHMATCH_RANDOMIZER_MENU(sFMMCmenu)
				DO_SCREEN_FADE_IN(500)
				
				sCurrentVarsStruct.iRandomCreationStatus = DM_RAND_INIT
				
				bPlayShardOutro = FALSE
				iShardTimer = GET_GAME_TIMER() + 14000

				CLEAR_BIT(iHelpBitSet,biRandomizing)
			ELSE	
				PRINTLN("RANDOMIZER - Loading scaleform MIDSIZED_MESSAGE")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC DEAL_WITH_RANDOMIZER_HUD()

	if sFMMCmenu.sActiveMenu = eFmmc_DM_RANDOMIZER
	
		INT iR,iG,iB,iA
		iR = 0
		iG = 0
		iB = 0
		iA = 0
		GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, iR,iG,iB,iA)
		IF sCurrentVarsStruct.bLOSHitGround = TRUE
			DRAW_MARKER(MARKER_SPHERE, sCurrentVarsStruct.vCoronaPos, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<sRandStruct.fRadius, sRandStruct.fRadius, sRandStruct.fRadius>>, iR,iG,iB,125, FALSE, FALSE, EULER_XYZ, FALSE, NULL_STRING(),NULL_STRING(),TRUE)
		ELSE
			sCurrentVarsStruct.bResetUpHelp = TRUE
		ENDIF
		IF NOT DOES_BLIP_EXIST(bRandomizerBlip)
			bRandomizerBlip = ADD_BLIP_FOR_RADIUS(sCurrentVarsStruct.vCoronaPos, sRandStruct.fRadius)
			SET_BLIP_ALPHA(bRandomizerBlip, 125)
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(bRandomizerBlip, HUD_COLOUR_PURE_WHITE)
			SHOW_HEIGHT_ON_BLIP(bRandomizerBlip, FALSE)
			SET_BLIP_PRIORITY(bRandomizerBlip, BLIPPRIORITY_HIGHEST)
		ELSE
			SET_BLIP_COORDS(bRandomizerBlip, sCurrentVarsStruct.vCoronaPos)
		ENDIF
		
		IF sCurrentVarsStruct.bLOSHitGround = TRUE
			SET_BLIP_ALPHA(bRandomizerBlip, 125)
		ELSE
			SET_BLIP_ALPHA(bRandomizerBlip, 0)
		ENDIF
		
		IF DOES_BLIP_EXIST(sCurrentVarsStruct.biLocateBlip)
			REMOVE_BLIP(sCurrentVarsStruct.biLocateBlip)
		ENDIF	
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons,	FMMC_MAX_WEAPONS,	"FMMC_AB_03")
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints, FMMC_MAX_SPAWNPOINTS, "FMMC_AB_05")
	ELSE
		IF DOES_BLIP_EXIST(bRandomizerBlip)
			REMOVE_BLIP(bRandomizerBlip)
		ENDIF
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED (SF_RandomizerMovie)
		PRINTLN("LOADED")
		IF sFMMCmenu.sActiveMenu = eFmmc_DM_RANDOMIZER
			PRINTLN("Randomizer menu")
			IF iRandomizeResultsTimer > GET_GAME_TIMER()
				PRINTLN("iRandomizeResultsTimer = ", iRandomizeResultsTimer, " GET_GAME_TIMER() = ", GET_GAME_TIMER())
				IF IS_SCREEN_FADED_IN()													
					IF NOT (IS_PAUSE_MENU_ACTIVE())
						DRAW_SCALEFORM_MOVIE (SF_RandomizerMovie, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 255)
					ENDIF
				ENDIF
			ELIF iShardTimer > GET_GAME_TIMER() 
				IF bPlayShardOutro
					IF IS_SCREEN_FADED_IN()													
						IF NOT (IS_PAUSE_MENU_ACTIVE())
							DRAW_SCALEFORM_MOVIE (SF_RandomizerMovie, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 255)
						ENDIF
					ENDIF
				ELSE
					bPlayShardOutro = TRUE
					BEGIN_SCALEFORM_MOVIE_METHOD(SF_RandomizerMovie, "SHARD_ANIM_OUT")
					IF iNumOfRandomPlacedSpawns + iNumOfRandomPlacedWeapons > 0
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE)) 
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_RED))
					ENDIF
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.33)
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ELSE
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED (SF_RandomizerMovie)
				bPlayShardOutro = FALSE
			ENDIF
		ELSE
			PRINTLN("set as no longer needed 2")
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED (SF_RandomizerMovie)
			bPlayShardOutro = FALSE
		ENDIF
	ENDIF
									
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////         		TEST DEATHMATCH          		//////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////	

FUNC BOOL PROCESS_DM_TEST_MANUAL_RESPAWN()

	IF bPlayerIsRespawning
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OnlyExitVehicleOnButtonRelease, TRUE)
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) != testPeds[iRand].viVehicleDM
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	FLOAT fTargetFill = 0.0
	FLOAT fManualRespawnSpeed = 30.0
	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
		fTargetFill = 100.0
	ENDIF
	
	IF fTargetFill > fManualRespawnFill
		fManualRespawnFill += fManualRespawnSpeed * GET_FRAME_TIME()
	ELSE
		fManualRespawnFill -= fManualRespawnSpeed * GET_FRAME_TIME()
	ENDIF
	
	fManualRespawnFill = CLAMP(fManualRespawnFill, 0.0, 100.0)
	
	IF fManualRespawnFill >= 100.0
		RETURN TRUE
	ELIF fManualRespawnFill > 0.0
		DRAW_GENERIC_METER(ROUND(fManualRespawnFill), 100, "FMMC_AB_12")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DM_TEST_RESPAWN_PLAYER(BOOL VDM = FALSE, BOOL bManualRespawn = FALSE)
	
	MAINTAIN_BIG_MESSAGE()

	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	
		IF bPlayerIsRespawning = FALSE			
			IF IS_ENTITY_DEAD(PLAYER_PED_ID())
			OR bManualRespawn
				bPlayerIsRespawning = TRUE
				iPlayerRespawnTimer = GET_GAME_TIMER()
				iQuickRespawnBarExtraFill = 0
				
				g_b_onNewJobVoteScreen = FALSE				
				SET_BIT(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_ALLOW_WASTED)
				
				SET_APPLICABILITY_APPLY_CONDITION_RESPAWNING(sPlayerModifierData)
				iTestDeaths++
				PRINTLN("[Test Lives] Player has died! Total deaths: ", iTestDeaths)
				
				IF NOT bManualRespawn
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WASTED)
				ENDIF
				iDeadPlayerState = DPS_WAIT_TILL_FADE
				PRINTLN("[DEAD PLAYER] PLAYER IS DEAD!!")
			ELIF VDM
				IF DOES_ENTITY_EXIST(testPeds[iRand].viVehicleDM)			
					IF (IS_ENTITY_DEAD(testPeds[iRand].viVehicleDM) AND NOT IS_NON_ARENA_KING_OF_THE_HILL())
					OR bManualRespawn
						bPlayerIsRespawning = TRUE
						iPlayerRespawnTimer = GET_GAME_TIMER()
						iDeadPlayerState = DPS_WAIT_TILL_FADE
						
						IF NOT bManualRespawn
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WASTED)
						ELSE
							iDeadPlayerState = DPS_FADE
							fManualRespawnFill = 0.0
							DO_SCREEN_FADE_OUT(1000)
						ENDIF
						
						PRINTLN("testPeds[iRand].viVehicleDM IS DEAD, TIMER = ", testPeds[iRand].iRespawnTimer - GET_GAME_TIMER())
					ELSE
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), testPeds[iRand].viVehicleDM)
						AND NOT IS_NON_ARENA_KING_OF_THE_HILL()
						AND (IS_CURRENT_MODIFIER_SET_VALID(iCurrentPlayerModSet)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].iModifierSetBS, ciDM_ModifierBS_SpawnInVehicle))
							TASK_ENTER_VEHICLE(PLAYER_PED_ID(), testPeds[iRand].viVehicleDM, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
						ENDIF
						
						IF NOT IS_NON_ARENA_KING_OF_THE_HILL()
							IF IS_VEHICLE_STUCK_TIMER_UP(testPeds[iRand].viVehicleDM, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
							OR IS_VEHICLE_STUCK_TIMER_UP(testPeds[iRand].viVehicleDM, VEH_STUCK_JAMMED, JAMMED_TIME)
							OR IS_VEHICLE_STUCK_TIMER_UP(testPeds[iRand].viVehicleDM, VEH_STUCK_ON_ROOF, ROOF_TIME)
							OR IS_VEHICLE_STUCK_TIMER_UP(testPeds[iRand].viVehicleDM, VEH_STUCK_ON_SIDE, SIDE_TIME)
								bPlayerIsRespawning = TRUE
								iPlayerRespawnTimer = GET_GAME_TIMER()
								iDeadPlayerState = DPS_WAIT_TILL_FADE
								
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WASTED)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		INT iMyTeam = g_FMMC_STRUCT.iTestMyTeam
		INT iCurrAmmo = 0
		INT iClipCapacity = 0
		WEAPON_TYPE wtRespawnWep = GET_SPAWN_WEAPON(g_FMMC_STRUCT.iTestMyTeam)
					
		INT iWaitTime = 5000
		INT iRespawnTime = iPlayerRespawnTimer + iWaitTime
		INT iTimeElapsed = (GET_GAME_TIMER() - iPlayerRespawnTimer) + iQuickRespawnBarExtraFill
		
		IF bPlayerIsRespawning
			SWITCH iDeadPlayerState
				CASE DPS_WAIT_TILL_FADE
					PRINTLN("[DEAD PLAYER] DPS_WAIT_TILL_FADE!!")
					
					SET_BIT(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_ALLOW_ALL_HUD)
					SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						caRespawnInput = INPUT_CURSOR_ACCEPT
					ELSE
						caRespawnInput = INPUT_FRONTEND_ACCEPT
					ENDIF
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HUD_INPUT97", MPGlobals.g_KillStrip.instructionalButtons)					
					RENDER_RESPAWN_BUTTONS()					
					
					DRAW_GENERIC_METER(iTimeElapsed + 500, iWaitTime, "FMMC_AB_12")
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						iQuickRespawnBarExtraFill += iWaitTime / 20
						PRINTLN("[TMS] Increasing iQuickRespawnBarExtraFill to ", iQuickRespawnBarExtraFill)
					ENDIF
					
					IF GET_GAME_TIMER() > iRespawnTime
					OR iTimeElapsed >= iWaitTime
					OR fManualRespawnFill >= 100.0
						iDeadPlayerState = DPS_FADE
						DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					ENDIF
				BREAK
				CASE DPS_FADE
					PRINTLN("[DEAD PLAYER] DPS_FADE!!")
				
					IF IS_SCREEN_FADED_OUT()	
						
						MODEL_NAMES mnVehSpawn
						mnVehSpawn = GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, iMyTeam)
						IF iCurrentPlayerModSet != -1
						AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].iModifierSetBS, ciDM_ModifierBS_SpawnInVehicle)
							mnVehSpawn = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].mnRespawnVehicle
						ENDIF
						IF VDM
							REQUEST_MODEL(mnVehSpawn)
						ENDIF
						IF NOT VDM
						OR HAS_MODEL_LOADED(mnVehSpawn)
							INT iRandomSpot
							iRandomSpot = GET_SAFE_PLAYER_SPAWN_POSITION(testPeds, g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM, iMyTeam, iCurrentPlayerModSet)
							
							IF VDM
								NET_SPAWN_PLAYER(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRandomSpot].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRandomSpot].fHead, SPAWN_REASON_MANUAL, SPAWN_LOCATION_AT_SPECIFIC_COORDS, FALSE, FALSE)
								
								IF DOES_ENTITY_EXIST(testPeds[iRand].viVehicleDM)
									DELETE_VEHICLE(testPeds[iRand].viVehicleDM)
									PRINTLN("[DMTEST] Deleting testPeds[", iRand, "].viVehicleDM")
								ENDIF
								
								PRINTLN("[DMTEST] Creating testPeds[", iRand, "].viVehicleDM for player (respawn)")
								testPeds[iRand].viVehicleDM = CREATE_VEHICLE(mnVehSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRandomSpot].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRandomSpot].fHead, FALSE, FALSE)
								SET_VEHICLE_ON_GROUND_PROPERLY(testPeds[iRand].viVehicleDM)
								TASK_ENTER_VEHICLE(PLAYER_PED_ID(), testPeds[iRand].viVehicleDM, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
								
								IF NOT IS_NON_ARENA_KING_OF_THE_HILL()
								AND (IS_CURRENT_MODIFIER_SET_VALID(iCurrentPlayerModSet)
								AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].iModifierSetBS, ciDM_ModifierBS_SpawnInVehicle))
									SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_HARD)
									SET_VEHICLE_DOORS_LOCKED(testPeds[iRand].viVehicleDM, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
								ENDIF
								
								IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(testPeds[iRand].viVehicleDM))
									SET_VEHICLE_ENGINE_ON(testPeds[iRand].viVehicleDM, TRUE, TRUE)
									SET_VEHICLE_FORWARD_SPEED(testPeds[iRand].viVehicleDM, 50.0)
								ENDIF
								
								testPeds[iRand].iRespawnTimer = 0
							ELSE
								IF NOT SHOULD_USE_TEAM_SPAWN(bHasSpawned)
									NET_SPAWN_PLAYER(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRandomSpot].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iRandomSpot].fHead, SPAWN_REASON_MANUAL, SPAWN_LOCATION_AT_SPECIFIC_COORDS, FALSE, FALSE)
								ELSE
									NET_SPAWN_PLAYER(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iMyTeam][iRandomSpot].vPos, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iMyTeam][iRandomSpot].fHead, SPAWN_REASON_MANUAL, SPAWN_LOCATION_AT_SPECIFIC_COORDS, FALSE, FALSE)
								ENDIF
							ENDIF

							SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
							SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
							
							iDeadPlayerState = DPS_FADE_UP
						ENDIF
					ELSE
						PRINTLN("WAITING FOR SCREEN FADE IN")
					ENDIF
				BREAK
				CASE DPS_FADE_UP
					PRINTLN("[DEAD PLAYER] DPS_FADE_UP!!")
					
					IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
						IF GET_GAME_TIMER() > iPlayerRespawnTimer + 8000 + DEFAULT_FADE_TIME
						AND HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())		
							IF VDM
								IF DOES_ENTITY_EXIST(testPeds[iRand].viVehicleDM)
									IF IS_VEHICLE_DRIVEABLE(testPeds[iRand].viVehicleDM)
										FREEZE_ENTITY_POSITION(testPeds[iRand].viVehicleDM, FALSE)
									ENDIF
								ENDIF
							ELSE
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)	
							ENDIF
		
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = 0	
							wtRespawnWep = GET_SPAWN_WEAPON(g_FMMC_STRUCT.iTestMyTeam)
							
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtRespawnWep, TRUE)
							iCurrAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtRespawnWep)
							iClipCapacity = GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(),wtRespawnWep)
							IF iCurrAmmo < iClipCapacity //make sure the player has at least one clip 
								SET_PED_AMMO_BY_TYPE( PLAYER_PED_ID(), GET_PED_AMMO_TYPE_FROM_WEAPON(PLAYER_PED_ID(),wtRespawnWep), iClipCapacity)
							ENDIF
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(-3, 0.2)
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							iDeadPlayerState = DPS_INVINCIBLE
							RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(MPGlobals.g_KillStrip.instructionalButtons)
							CLEAR_ALL_BIG_MESSAGES()
							
							fManualRespawnFill = 0.0
							PRINTLN("iDeadPlayerState = DPS_INVINCIBLE")
						ELSE
							PRINTLN("TIMER OR COLLISION FAIL")
						ENDIF
					ELSE
						PRINTLN("PLAYER IS DEAD OR DYING")
						iDeadPlayerState = DPS_FADE
						DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					ENDIF
				BREAK
				CASE DPS_INVINCIBLE
				
					PRINTLN("[DEAD PLAYER] DPS_INVINCIBLE!!")
					
					IF IS_SCREEN_FADED_IN()		
						IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
							PRINTLN("[DEAD PLAYER] FADED IN!!")							
							iDeadPlayerState = DPS_WAIT_TILL_FADE
							bPlayerIsRespawning = FALSE
							bPlayerIsInvincible = TRUE
							iPlayerRespawnTimer = GET_GAME_TIMER()
							ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
						ELSE
							PRINTLN("[DEAD PLAYER] STILL DEAD!!")
							iDeadPlayerState = DPS_FADE
							DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
						ENDIF
					ELSE
						PRINTLN("WAITING FOR SCREEN FADE IN")
					ENDIF
				BREAK
			ENDSWITCH
			
		ELIF bPlayerIsInvincible
			IF GET_GAME_TIMER() > iPlayerRespawnTimer + 5000
				IF sFMMCmenu.bPlayerInvincibleTest = FALSE
					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
					SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)	
				ELSE
					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
					SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
				ENDIF
				SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), TRUE)	
				bPlayerIsInvincible = FALSE
			ELSE
				PRINTLN("[DEAD PLAYER] INVINCIBILITY DELAY!!")
			ENDIF
		ENDIF
	ENDIF 
ENDPROC

FUNC INT GET_TEST_PED_INDEX(PED_INDEX testPed)

	INT iTestPedIndex = -1
	
	INT i
	FOR i = 1 TO g_FMMC_STRUCT.iNumParticipants+1
		IF testPeds[i].pedID = testPed
			iTestPedIndex = i
			BREAKLOOP
		ENDIF
	ENDFOR
	
	RETURN iTestPedIndex
	
ENDFUNC

FUNC INT GET_TEST_PED_TEAM(PED_INDEX testPed)

	INT iTestPedIndex = GET_TEST_PED_INDEX(testPed)
	
	INT iTeam = -1
	IF iTestPedIndex >= 0 
	AND iTestPedIndex <= FMMC_MAX_SPAWNPOINTS
		iTeam = testPeds[iTestPedIndex].iTeam
	ENDIF
	
	RETURN iTeam

ENDFUNC

FUNC INT GET_TEST_KILL_SCORE(PED_INDEX victimPed, BOOL bDrawFloatingScore = TRUE)

	INT iTotalScore
	
	INT iVictimPedTeam = GET_TEST_PED_TEAM(victimPed)
	WEAPON_TYPE wtWeapon = GET_PED_CAUSE_OF_DEATH(victimPed)
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentPlayerModSet)
		PRINTLN("[SCR_MODS] GET_TEST_KILL_SCORE - Invalid Modset: ", iCurrentPlayerModSet)
		RETURN iTotalScore
	ENDIF
	
	// Kill Score
	iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].sModifierScoring.iKill
	PRINTLN("[SCR_MODS] GET_TEST_KILL_SCORE - Base Kill. Total Score: ", iTotalScore)
	
	// Melee Bonus
	IF WAS_PED_DAMAGED_BY_MELEE(victimPed)
		iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].sModifierScoring.iMelee
		PRINTLN("[SCR_MODS] GET_TEST_KILL_SCORE - Melee Kill. Total Score: ", iTotalScore)
	ELSE
		
		// Headshot Bonus
		IF CHECK_HEADSHOT(victimPed)
			iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].sModifierScoring.iHeadshot 
			PRINTLN("[SCR_MODS] GET_TEST_KILL_SCORE - Headshot Kill. Total Score: ", iTotalScore)
		ENDIF
		
		// Vehicle Bonus
		IF IS_ENTITY_DEAD(victimPed)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(victimPed)
			OR wtWeapon = WEAPONTYPE_RUNOVERBYVEHICLE
				iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].sModifierScoring.iVehicle
				PRINTLN("[SCR_MODS] GET_TEST_KILL_SCORE - Vehicle Kill. Total Score: ", iTotalScore)
			ENDIF
		ENDIF
		
		// Specific Weapon Group Bonus
		iTotalScore += GET_PLAYER_MODIFIERS_SCORING_WEAPON_GROUP_SCORE(iCurrentPlayerModSet, wtWeapon)
		PRINTLN("[SCR_MODS] GET_TEST_KILL_SCORE - Specific Weapon Group Kill. Weapon: ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(wtWeapon), " Total Score: ", iTotalScore)
		
	ENDIF
	
	// Specific Team Bonus
	IF iVictimPedTeam >= 0 AND iVictimPedTeam <= g_FMMC_STRUCT.iMaxNumberOfTeams
		iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].sModifierScoring.iSpecificTeam[iVictimPedTeam]
		PRINTLN("[SCR_MODS] GET_TEST_KILL_SCORE - Specific Team Kill. Team: ", iVictimPedTeam, " Total Score: ", iTotalScore)
	ENDIF
	
	IF bDrawFloatingScore
		PROCESS_SCORING_MODIFIERS_ADD_FLOATING_SCORE(sTargetFloatingScores, iTotalScore)
	ENDIF
	
	RETURN iTotalScore

ENDFUNC

FUNC INT GET_TEST_FRIENDLY_KILL_SCORE(BOOL bDrawFloatingScore = TRUE)
	
	INT iTotalScore
		
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentPlayerModSet)
		PRINTLN("[SCR_MODS] GET_TEST_FRIENDLY_KILL_SCORE - Invalid modset")
		RETURN iTotalScore
	ENDIF
	
	// Friendly kill score
	iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].sModifierScoring.iFriendlyKill
	PRINTLN("[SCR_MODS] GET_TEST_FRIENDLY_KILL_SCORE - Friendly Kill. Total Score: ", iTotalScore)
	
	IF bDrawFloatingScore
		PROCESS_SCORING_MODIFIERS_ADD_FLOATING_SCORE(sTargetFloatingScores, iTotalScore)
	ENDIF
	
	RETURN iTotalScore
	
ENDFUNC

FUNC INT GET_TEST_SUICIDE_SCORE(BOOL bDrawFloatingScore = TRUE)
	
	INT iTotalScore
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentPlayerModSet)
		PRINTLN("[SCR_MODS] GET_TEST_SUICIDE_SCORE - Invalid Modset: ", iCurrentPlayerModSet)
		RETURN iTotalScore
	ENDIF
	
	// Suicide score
	iTotalScore = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].sModifierScoring.iSuicide
	PRINTLN("[SCR_MODS] GET_TEST_SUICIDE_SCORE - Suicide. Total Score: ", iTotalScore)
	
	IF bDrawFloatingScore
		PROCESS_SCORING_MODIFIERS_ADD_FLOATING_SCORE(sTargetFloatingScores, iTotalScore)
	ENDIF
	
	RETURN iTotalScore
	
ENDFUNC

FUNC INT GET_TEST_OUT_OF_BOUNDS_SCORE(BOOL bDrawFloatingScore = TRUE)
	
	INT iTotalScore
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentPlayerModSet)
		PRINTLN("[SCR_MODS] GET_TEST_OUT_OF_BOUNDS_SCORE - Invalid modset: ", iCurrentPlayerModSet)
		RETURN iTotalScore
	ENDIF
	
	// Out of bounds score
	iTotalScore = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentPlayerModSet].sModifierScoring.iOutOfBounds
	PRINTLN("[SCR_MODS] GET_TEST_OUT_OF_BOUNDS_SCORE - Out Of Bounds. Total Score: ", iTotalScore)
	
	IF bDrawFloatingScore
		PROCESS_SCORING_MODIFIERS_ADD_FLOATING_SCORE(sTargetFloatingScores, iTotalScore)
	ENDIF
	
	RETURN iTotalScore

ENDFUNC

PROC PROCESS_CUSTOM_DM_GIVE_LIVES_TEST_MODE(INT iCurrentModSet)
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModset)
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].iModifierSetBS, ciDM_ModifierBS_GiveLives)
		EXIT
	ENDIF
	
	INT iTeam = g_FMMC_STRUCT.iTestMyTeam
	INT iAdditionalLives = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierOnKill.iExtraLives

	IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
	AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_SharedTeamLives)
		iTestTeamExtraLives[iTeam] += iAdditionalLives
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_CUSTOM_DM_GIVE_LIVES_TEST_MODE - Team recevied extra lives! Additional lives: ", iAdditionalLives, " | Total Extra Lives: ", iTestTeamExtraLives[iTeam])
	ELSE		
		iTestExtraLives += iAdditionalLives
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_CUSTOM_DM_GIVE_LIVES_TEST_MODE - Player recevied extra lives! Additional lives: ", iAdditionalLives, " | Total Extra Lives: ", iTestExtraLives)
	ENDIF
	
	SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_ON_KILL_EFFECT)
	
//	url:bugstar:7776049
	UNUSED_PARAMETER(sTargetFloatingScores)	
//	TARGET_ADD_FLOATING_SCORE(iAdditionalLives, GET_CENTER_OF_SCREEN(), HUD_COLOUR_GREENLIGHT, sTargetFloatingScores, "DM_IND_LIV", DEFAULT, DEFAULT, TARGET_SCORE_SCALE_SMALL, TARGET_SCORE_SPACING_LARGE, TARGET_SCORE_LIFETIME_LONG, TRUE)
	
ENDPROC

PROC PROCESS_TEST_ON_PLAYER_KILL(PED_INDEX victimPed)
		
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentPlayerModSet)
		EXIT
	ENDIF
	
	PROCESS_CUSTOM_DM_GIVE_LIVES_TEST_MODE(iCurrentPlayerModSet)
	PROCESS_CUSTOM_DM_GIVE_ARMOUR(iCurrentPlayerModSet, sTargetFloatingScores)
	PROCESS_CUSTOM_DM_GIVE_AMMO(sPlayerModifierData, iCurrentPlayerModSet, sTargetFloatingScores)
	
	WEAPON_TYPE wtWeapon = GET_PED_CAUSE_OF_DEATH(victimPed)
	
	IF GET_WEAPON_GROUP(wtWeapon) != -1
	
		FLOAT fDamage = GET_WEAPON_DAMAGE(wtWeapon, WEAPONCOMPONENT_INVALID)
		PROCESS_CUSTOM_DM_GIVE_HEALTH(fDamage, iCurrentPlayerModSet, sTargetFloatingScores)
	
	ENDIF
	
ENDPROC

PROC CHECK_FOR_PLAYER_KILL()

    EVENT_NAMES eEventType
	STRUCT_ENTITY_ID sEntityID
	INT iCount = 0
	PED_INDEX victimPed
	ENTITY_INDEX killer
	PED_INDEX killerPed
	VEHICLE_INDEX killerVeh
	ENTITY_TYPE etEntity
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iCount
	
		eEventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iCount)
		
        SWITCH (eEventType)
            CASE EVENT_ENTITY_DAMAGED
			
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iCount, sEntityID, SIZE_OF(STRUCT_ENTITY_ID))
					
					IF DOES_ENTITY_EXIST(sEntityID.EntityId)
					
						IF IS_ENTITY_A_PED(sEntityID.EntityId)
							victimPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.EntityId)
							
							IF IS_ENTITY_DEAD(victimPed)
							AND IS_ENTITY_A_MISSION_ENTITY(victimPed)
								killer = GET_PED_SOURCE_OF_DEATH(victimPed)
								
								IF NOT IS_PED_A_PLAYER(victimPed)
									etEntity = GET_ENTITY_TYPE(killer)
									
									// Killed by Ped
									IF etEntity = ET_PED
										killerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(killer)
										
										IF piLastKilled = victimPed
											piLastKilled = PLAYER_PED_ID()
										ELSE
										
											IF killerPed = PLAYER_PED_ID()
											
												PRINTLN("CREATOR KILL - Player has killed someone")
												IF GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) = GET_PED_RELATIONSHIP_GROUP_HASH(victimPed)
													iIndividualScore += GET_TEST_FRIENDLY_KILL_SCORE()
													iTestScore += GET_TEST_FRIENDLY_KILL_SCORE()
												ELSE
													PROCESS_TEST_ON_PLAYER_KILL(victimPed)
													iIndividualScore += GET_TEST_KILL_SCORE(victimPed)
													iTestScore += GET_TEST_KILL_SCORE(victimPed, FALSE)
													piLastKilled = victimPed
													iTestKills++
												ENDIF
												
											ELSE
											
												IF GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) = GET_PED_RELATIONSHIP_GROUP_HASH(killerPed)
													PRINTLN("CREATOR KILL - AI on your team has killed someone")
													iTestScore += GET_TEST_KILL_SCORE(victimPed, FALSE)
													iTestKills++
												ENDIF
												
											ENDIF
											
											iGlobalTestScore += GET_TEST_KILL_SCORE(victimPed, FALSE)
											iGlobalTestKills++
											
										ENDIF
									
									// Killed by Vehicle
									ELIF etEntity = ET_VEHICLE
										killerVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(killer)
										
										IF SHOULD_USE_VEHICLES(DEFAULT, iCurrentPlayerModSet)
										OR IS_VEHICLE_DRIVEABLE(killerVeh)
										
											killerPed = GET_PED_IN_VEHICLE_SEAT(killerVeh)
											IF killerPed = PLAYER_PED_ID()
											
												PRINTLN("CREATOR KILL - Player vehicle has killed someone")
												IF GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) = GET_PED_RELATIONSHIP_GROUP_HASH(victimPed)
													iIndividualScore += GET_TEST_FRIENDLY_KILL_SCORE(FALSE)
													iTestScore += GET_TEST_FRIENDLY_KILL_SCORE()
												ELSE
													PROCESS_TEST_ON_PLAYER_KILL(victimPed)
													iIndividualScore += GET_TEST_KILL_SCORE(victimPed)
													iTestScore += GET_TEST_KILL_SCORE(victimPed, FALSE)
													piLastKilled = victimPed
													iTestKills++
												ENDIF
												
											ELSE
											
												IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
												AND DOES_ENTITY_EXIST(killerPed)
													IF GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) = GET_PED_RELATIONSHIP_GROUP_HASH(killerPed)
														PRINTLN("CREATOR KILL - AI (vehicle) on your team has killed someone")
														iTestScore += GET_TEST_KILL_SCORE(victimPed, FALSE)
														iTestKills++
													ENDIF
												ENDIF
												
											ENDIF
											
											iGlobalTestScore += GET_TEST_KILL_SCORE(victimPed, FALSE)
											iGlobalTestKills++
											
										ENDIF
									ELSE
										
										// We can't get a reason for ped death
										// Let's check to see if either the player, or an entity controlled by player dealt fatal blow. 
										IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
											IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(victimPed, PLAYER_PED_ID())
												IF piLastKilled = victimPed
													piLastKilled = PLAYER_PED_ID()
												ELSE
													PRINTLN("CREATOR KILL - Player has killed someone")
													PROCESS_TEST_ON_PLAYER_KILL(victimPed)
													iTestScore += GET_TEST_KILL_SCORE(victimPed)
													piLastKilled = victimPed
													iTestKills++
												ENDIF
											ENDIF
										ENDIF
										
										iGlobalTestScore += GET_TEST_KILL_SCORE(victimPed, FALSE)
										iGlobalTestKills++
										
									ENDIF
								ELSE
								
									// Player died
									iGlobalTestKills++
									
									// Get killer ped
									etEntity = GET_ENTITY_TYPE(killer)
									IF etEntity = ET_PED
										killerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(killer)
										
									ELIF etEntity = ET_VEHICLE
										killerVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(killer)
										
										IF SHOULD_USE_VEHICLES(DEFAULT, iCurrentPlayerModSet)
										OR IS_VEHICLE_DRIVEABLE(killerVeh)
											killerPed = GET_PED_IN_VEHICLE_SEAT(killerVeh)
										ENDIF
									ENDIF
									
									// If player commit suicide
									IF killerPed = PLAYER_PED_ID()
										PRINTLN("CREATOR KILL - Player commit suicide")
										iGlobalTestScore += GET_TEST_SUICIDE_SCORE()
									 	iTestScore += GET_TEST_SUICIDE_SCORE(FALSE)
									ELSE
										PRINTLN("CREATOR KILL - Player was killed")
										iGlobalTestScore += GET_TEST_KILL_SCORE(victimPed, FALSE)
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

PROC PROCESS_DM_PLAYER_OUT_OF_BOUNDS(INT iEventID)

	SCRIPT_EVENT_DATA_DM_PLAYER_OUT_OF_BOUNDS OutOfBoundsData

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, OutOfBoundsData, SIZE_OF(OutOfBoundsData))
		PRINTLN("PROCESS_DM_CREATOR_EVENTS - Player Out of Bounds")
		iGlobalTestScore += GET_TEST_OUT_OF_BOUNDS_SCORE()
		iTestScore += GET_TEST_OUT_OF_BOUNDS_SCORE()
	ENDIF

ENDPROC

PROC PROCESS_DM_CREATOR_EVENTS()

	INT iEventID
	EVENT_NAMES ThisScriptEvent	
	STRUCT_PICKUP_EVENT PickupData
	STRUCT_EVENT_COMMON_DETAILS EventData
	TEXT_LABEL_15 labelTemp

	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
	
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
					
		SWITCH ThisScriptEvent
			// Weapon pickup tickers
			CASE EVENT_NETWORK_PLAYER_COLLECTED_PICKUP
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, PickupData, SIZE_OF(PickupData))
					
					BOOL bLocalPlayer
					
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
						bLocalPlayer = (PickupData.PlayerIndex = PLAYER_ID())
					ENDIF
					
					INT iPickup, iPickupCollected
					FOR iPickup = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1
						
						IF iPickup >= FMMC_MAX_WEAPONS
							BREAKLOOP
						ENDIF
						
						IF PickupData.PickupIndex != sWepStruct.Pickups[iPickup]
							RELOOP
						ENDIF
						iPickupCollected = iPickup
						PRINTLN("PROCESS_DM_CREATOR_EVENTS - Pickup ", iPickup)
						
						IF PickupData.PickupType = PICKUP_CUSTOM_SCRIPT
							PROCESS_CUSTOM_PICKUP_EVENT(iPickup)
						ELSE
							PROCESS_POWERUP_PICKUP_EVENT(sPowerUps, iPickup, bLocalPlayer)
						ENDIF
						
					ENDFOR
					
					IF bLocalPlayer
						INT iSubType
						iSubType = -1
						IF PickupData.PickupType = PICKUP_CUSTOM_SCRIPT
							iSubType = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickupCollected].iCustomPickupType
						ENDIF
						labelTemp = GET_CORRECT_TICKER_STRING(PickupData.PickupType, iSubType)
						PRINT_TICKER(labelTemp)
					ENDIF
					
				ENDIF
			BREAK
			
			CASE EVENT_NETWORK_SCRIPT_EVENT
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))							
				SWITCH EventData.Type
					CASE SCRIPT_EVENT_DM_PLAYER_OUT_OF_BOUNDS
						PROCESS_DM_PLAYER_OUT_OF_BOUNDS(iEventID)
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDREPEAT
	
ENDPROC


PROC PROCESS_DYNOPROPS_DMTEST()
	INT iDynoprop
	
	PROCESS_PUTTING_OUT_FIREPIT_FIRES(sTrapInfo_Local)
	PROCESS_PRELOOP_ARENA_TRAPS(sTrapInfo_Local)
	
	FOR iDynoprop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps -1
		IF DOES_ENTITY_EXIST(sDynoPropStruct.oiObject[iDynoprop])
			PROCESS_UGC_SEA_MINES(sDynoPropStruct.oiObject[iDynoprop], iDynoprop, TRUE, iSeamineBS)
			
			PROCESS_ARENA_TRAPS(sDynoPropStruct.oiObject[iDynoprop], iDynoprop, TRUE, sTrapInfo_Host, sTrapInfo_Local)
			
			PROCESS_REMOVING_TARGETTABLE_FROM_DYNO_PROP(sDynoPropStruct.oiObject[iDynoprop])
		ELSE
			CLEANUP_TRAP(iDynoprop, sTrapInfo_Host, sTrapInfo_Local)
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_PROPS_DMTEST()
	INT iProp
	
	FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps -1
		IF DOES_ENTITY_EXIST(sPropStruct.oiObject[iProp])
			IF IS_THIS_MODEL_A_TRAP_PRESSURE_PAD(GET_ENTITY_MODEL(sPropStruct.oiObject[iProp]))
				PROCESS_ARENA_TRAP_PRESSUREPAD(sPropStruct.oiObject[iProp], iProp, sTrapInfo_Host, sTrapInfo_Local)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL IS_SHAPETESTED_VEHICLE_AREA_SAFE(INT iVeh)
	
	INT iHitSomething
	ENTITY_INDEX hitEntity
	VECTOR vNormal
	VECTOR vSpawnPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos
	MODEL_NAMES vehModel = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn
	VECTOR vStart, vEnd, vMax, vMin
	FLOAT fWidth
	GET_MODEL_DIMENSIONS(vehModel, vMin, vMax)
	IF IS_THIS_MODEL_A_HELI(vehModel)
		fWidth = vMax.x*0.55
		vMin.x = 0	
		vMax.x = 0
		vMin.z = vMax.z*0.8
		vMax.z = vMax.z*0.8
		vMin.y = vMin.y*0.5
		vMax.y = vMax.y*0.5
	ELSE
		fWidth = vMax.x*0.8
		vMin.x = 0	
		vMax.x = 0
		vMin.z = vMax.z*0.8
		vMax.z = vMax.z*0.8
		vMin.y = vMin.y*0.9
		vMax.y = vMax.y*0.9
	ENDIF	
	vStart = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, vMin)
	vEnd = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, vMax)
	
	IF iVehToShapeTest != iVeh
		PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_SHAPETESTED_VEHICLE_AREA_SAFE] iVehToShapeTest != iVeh not shapetesting yet, wait out turn (iVeh: ", iVeh, " iVehToShapeTest: ", iVehToShapeTest, ")")
		RETURN FALSE
	ENDIF

	/*
	vMin.y -= fWidth // START_SHAPE_TEST_CAPSULE is extending the end/start points........................
	vMax.y += fWidth	
	VECTOR vdbgStart = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, <<vMin.x+fWidth, vMin.y, vMin.z>>)
	VECTOR vdbgEnd = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, <<vMax.x+fWidth, vMax.y, vMax.z>>)
	DRAW_DEBUG_LINE(vstart, vend)
	DRAW_DEBUG_LINE(vdbgStart, vdbgEnd)
	*/
	
	stiVehSafeStatus = GET_SHAPE_TEST_RESULT(stiVehSafe, iHitSomething, vSpawnPos, vNormal, hitEntity)
	IF stiVehSafeStatus = SHAPETEST_STATUS_RESULTS_READY
		PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_SHAPETESTED_VEHICLE_AREA_SAFE] -stStatus: SHAPETEST_STATUS_RESULTS_READY")
		
		iVehToShapeTest++
		
		IF NOT DOES_ENTITY_EXIST(hitEntity)		
			PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_SHAPETESTED_VEHICLE_AREA_SAFE] -stStatus: Entity Not Hit. Returning True - Next vehicle to test is iVeh: ", iVehToShapeTest)
			RETURN TRUE
		ELSE
			PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_SHAPETESTED_VEHICLE_AREA_SAFE] -stStatus: Entity Hit. Returning False Next vehicle to test is iVeh: ", iVehToShapeTest)
			RETURN FALSE
		ENDIF
	ELIF stiVehSafeStatus =  SHAPETEST_STATUS_NONEXISTENT
		PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_SHAPETESTED_VEHICLE_AREA_SAFE] - Shape Test vStart: ", vStart, "  vEnd: ", vEnd, " fWidth: ", fWidth)
		VEHICLE_INDEX viVeh = sVehStruct.veVehcile[iVeh]
		stiVehSafe = START_SHAPE_TEST_CAPSULE(vStart, vEnd, fWidth, SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED, viVeh, SCRIPT_SHAPETEST_OPTION_DEFAULT)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_RESPAWN_DM_VEH_NOW_SAFE(INT iVeh)
	
	IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[iVeh])
		VEHICLE_INDEX viVeh = sVehStruct.veVehcile[iVeh]
		
		IF NOT IS_ENTITY_ALIVE(viVeh)
			viVeh = NULL
		ENDIF
				
		IF NOT IS_SHAPETESTED_VEHICLE_AREA_SAFE(iVeh)
			PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_RESPAWN_DM_VEH_NOW_SAFE] - iVeh: ", iVeh, " the area is occupied by something. Not Safe.")
			RETURN FALSE	
		ENDIF
	ENDIF
	
	PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_RESPAWN_DM_VEH_NOW_SAFE] - iVeh: ", iVeh, " The area is now safe, returning true.")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL RESPAWN_DM_VEH(INT iVeh)

	MODEL_NAMES vehModel = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn
	
	VECTOR vSpawnPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos
	VECTOR vStart, vEnd, vMax, vMin
	GET_MODEL_DIMENSIONS(vehModel, vMin, vMax)
	vStart = vSpawnPos+vMin
	vEnd = vSpawnPos+vMax
	
	IF IS_AREA_OCCUPIED(vStart, vEnd, FALSE, TRUE, TRUE, FALSE, FALSE)
		SET_BIT(iVehicleRespawnGhostedBitset, iVeh)	
	ENDIF
	
	IF NOT IS_BIT_SET(iVehicleCreatedBitset, iVeh)
		CREATE_VEHICLE_FMMC(sVehStruct.veVehcile[iVeh], iVeh, TRUE)
		SET_BIT(iVehicleCreatedBitset, iVeh)
	ENDIF
	
	VEHICLE_INDEX viVeh = sVehStruct.veVehcile[iVeh]
	
	IF DOES_ENTITY_EXIST(viVeh)
		IF IS_VEHICLE_MODEL(viVeh, MULE)
			FMMC_SET_THIS_VEHICLE_EXTRAS(viVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery)
		ELSE
			FMMC_SET_THIS_VEHICLE_EXTRAS(viVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet)
		ENDIF
		
		FMMC_SET_THIS_VEHICLE_COLOURS(viVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColour,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColourCombination, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour2, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour3, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fEnvEff)
				
		SET_ENTITY_VISIBLE(viVeh, TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
		
		IF DOES_VEHICLE_HAVE_ANY_WEAPON_MODS(vehModel)
			SET_VEHICLE_WEAPON_MODS(viVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModTurret, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModArmour, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, iVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModAirCounter, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModExhaust, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModBombBay, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModSpoiler)
		ENDIF
		
		SET_UP_FMMC_VEH(viVeh, iVeh)
		
		PRINTLN("[LM][DM_ENTITY_RESPAWN][RESPAWN_DM_VEH] - Successfully respawned iVeh: ", iVeh)
		CLEAR_BIT(iVehicleCreatedBitset, iVeh)
		
			
		IF IS_BIT_SET(iVehicleRespawnGhostedBitset, iVeh)
			SET_ENTITY_COLLISION(viVeh, FALSE)
			SET_ENTITY_ALPHA(viVeh, 125, FALSE)
			FREEZE_ENTITY_POSITION(viVeh, TRUE)
		ENDIF
		
		RETURN TRUE		
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_DM_VEH_TO_RESPAWN(INT iVeh, INT iReason)
	IF NOT IS_BIT_SET(iVehicleRespawnBitset, iVeh)
		PRINTLN("[LM][DM_ENTITY_RESPAWN][SET_DM_VEH_TO_RESPAWN] - Flagging iVeh: ", iVeh, " To respawn due to iReason: ", iReason)
		UNUSED_PARAMETER(iReason)
		SET_BIT(iVehicleRespawnBitset, iVeh)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DM_VEH_RESPAWN(INT iVeh)
	// Flagged to respawn vehicle
	IF IS_BIT_SET(iVehicleRespawnBitset, iVeh)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iVehicleSpawnedBitset, iVeh)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_DM_VEHICLES_CAN_RESPAWN)
		// Check if we should set Respawn Flag	
		VEHICLE_INDEX viVeh = sVehStruct.veVehcile[iVeh]
		IF DOES_ENTITY_EXIST(viVeh)
			
			// Destroyed/Stuck/Undriveable
			IF NOT IS_VEHICLE_DRIVEABLE(viVeh)
			OR IS_ENTITY_DEAD(viVeh)
			OR (IS_ENTITY_SUBMERGED_IN_WATER(viVeh, FALSE, WATER_SUBMERGED_LEVEL) AND IS_THIS_MODEL_A_BIKE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn))
				SET_DM_VEH_TO_RESPAWN(iVeh, 0)
				RESET_NET_TIMER(td_VehCleanupTimer[iVeh])
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CLEANED_UP_DM_VEH(INT iVeh)
	
	VEHICLE_INDEX viVeh = sVehStruct.veVehcile[iVeh]
	
	IF DOES_ENTITY_EXIST(viVeh)
		IF IS_VEHICLE_EMPTY(viVeh, DEFAULT, DEFAULT, DEFAULT, TRUE)
			IF HAS_NET_TIMER_STARTED(td_VehCleanupTimer[iVeh])
				IF HAS_NET_TIMER_EXPIRED(td_VehCleanupTimer[iVeh], 5000)					
					IF HAS_NET_TIMER_EXPIRED(td_VehCleanupTimer[iVeh], 10000)
						PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - Deleting netVehicleToCleanUp")	
						SET_ENTITY_COLLISION(viVeh, FALSE)
						
						IF IS_ENTITY_ON_FIRE(viVeh)
							STOP_ENTITY_FIRE(viVeh)
						ENDIF
						
						STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(viVeh), 5.0)
						RESET_NET_TIMER(td_VehCleanupTimer[iVeh])
						
						DELETE_VEHICLE(viVeh)
					ELSE
						PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - Waiting for Timer.")
					ENDIF
				ELSE
					PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - Waiting for Timer, or we are respawning.")
				ENDIF
			ELSE
				PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - Starting Timer.")
				START_NET_TIMER(td_VehCleanupTimer[iVeh])
			ENDIF
		ELSE
			PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - NETWORK_FADE_OUT_ENTITY Vehicle is not empty...")
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC DRAW_LOCAL_RESPAWN_VEHICLE_BOUNDS(INT iVeh)
	VECTOR vSpawnPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos
	MODEL_NAMES vehModel = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn
	VECTOR vStart, vEnd, vMax, vMin
	FLOAT fWidth
	GET_MODEL_DIMENSIONS(vehModel, vMin, vMax)
	IF IS_THIS_MODEL_A_HELI(vehModel)
		fWidth = vMax.x*0.55
		vMin.x = 0	
		vMax.x = 0
		vMin.z = vMax.z*0.8
		vMax.z = vMax.z*0.8
		vMin.y = vMin.y*0.5
		vMax.y = vMax.y*0.5
	ELSE
		fWidth = vMax.x*0.8
		vMin.x = 0	
		vMax.x = 0
		vMin.z = vMax.z*0.8
		vMax.z = vMax.z*0.8
		vMin.y = vMin.y*0.9
		vMax.y = vMax.y*0.9
	ENDIF
	vMin.y -= fWidth // START_SHAPE_TEST_CAPSULE is extending the end/start points........................
	vMax.y += fWidth
	vStart = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, vMin)
	vEnd = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, vMax)
			
	vSpawnPos.x = (vStart.x + vEnd.x)
	vSpawnPos.y = (vStart.y + vEnd.y)
	vSpawnPos.x /= 2
	vSpawnPos.y /= 2	
	
	FLOAT length = GET_DISTANCE_BETWEEN_COORDS(vEnd, vStart, FALSE)
	FLOAT angle = GET_HEADING_FROM_VECTOR_2D(vEnd.x - vStart.x, vEnd.y - vStart.y)
	DRAW_MARKER(MARKER_BOXES, (<<vSpawnPos.x, vSpawnPos.y, vSpawnPos.z-0.2>>), (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, angle>>), (<<ABSF(fWidth*2), ABSF(length), 0.5>>), 255, 50, 50, 150)
	
ENDPROC

// ################ IF YOU'RE UPDATING THIS, UPDATE THE CREATOR VERSION OF THESE FUNCTION TOO ############### ---------------------------------
PROC PROCESS_DM_RESPAWN_ENTITITES(KING_OF_THE_HILL_RUNTIME_HOST_STRUCT& sKOTH_HostInfo)
		
	KOTH_HILL_RUNTIME_HOST_STRUCT sTemp
	
	// Vehicles
	INT iVeh
	FOR iVeh = 0 TO FMMC_MAX_VEHICLES-1
		IF SHOULD_DM_VEH_RESPAWN(iVeh)
			IF CLEANED_UP_DM_VEH(iVeh)
				IF RESPAWN_DM_VEH(iVeh)
					CLEAR_BIT(iVehicleRespawnBitset, iVeh)
					
					INT iArea
					FOR iArea = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
						IF g_FMMC_STRUCT.sKotHData.sHillData[iArea].iHillEntity = iVeh
							IF g_FMMC_STRUCT.sKotHData.sHillData[iArea].iHillType = ciKOTH_HILL_TYPE__VEHICLE
								sKOTH_HostInfo.sHills[iArea] = sTemp
								sFMMCmenu.sKOTH_LocalInfo.sHills[iArea].eiMyEntity = sVehStruct.veVehcile[g_FMMC_STRUCT.sKotHData.sHillData[iArea].iHillEntity]
							ENDIF
						ENDIF
					ENDFOR		
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iVehicleRespawnGhostedBitset, iVeh)
			DRAW_LOCAL_RESPAWN_VEHICLE_BOUNDS(iVeh)
			IF IS_RESPAWN_DM_VEH_NOW_SAFE(iVeh)
				VEHICLE_INDEX viVeh = sVehStruct.veVehcile[iVeh]
				IF IS_VEHICLE_DRIVEABLE(viVeh)
					SET_ENTITY_COLLISION(viVeh, TRUE)
					SET_ENTITY_ALPHA(viVeh, 255, FALSE)
					FREEZE_ENTITY_POSITION(viVeh, FALSE)
					CLEAR_BIT(iVehicleRespawnGhostedBitset, iVeh)
				ENDIF
			ENDIF
		ENDIF		
					
		IF iVeh = iVehToShapeTest
		AND (NOT IS_BIT_SET(iVehicleRespawnGhostedBitset, iVeh))
			iVehToShapeTest++
			PRINTLN("[LM][DM_ENTITY_RESPAWN][PROCESS_DM_RESPAWN_ENTITITES] Shape Test Veh was dirty. Moved onto iVeh: ", iVehToShapeTest)
		ENDIF
		IF iVehToShapeTest >= 32
			iVehToShapeTest = 0
		ENDIF
	ENDFOR
	
	// Other?
ENDPROC

PROC PROCESS_KING_OF_THE_HILL_TEST_MODE()
	
	IF IS_THIS_A_TEAM_DM()
		sFMMCmenu.sKOTH_PlayerInfo.iCachedKOTHTeam = g_FMMC_STRUCT.iTestMyTeam
	ELSE
		sFMMCmenu.sKOTH_PlayerInfo.iCachedKOTHTeam = 0
	ENDIF
	
	sFMMCmenu.sKOTH_LocalInfo.iNumberOfKotHPlayers = NUM_NETWORK_PLAYERS - 3
	
	CLEAR_KING_OF_THE_HILL_PLAYERS_IN_ALL_HILLS(sFMMCmenu.sKOTH_HostInfo)
		
	INT iArea
	FOR iArea = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
		IF IS_CAPTURING_KOTH_HILL(sFMMCmenu.sKOTH_PlayerInfo, iArea)
		AND IS_SCREEN_FADED_IN()
			sFMMCmenu.sKOTH_HostInfo.sHills[iArea].iPlayersInHillPerTeam[sFMMCmenu.sKOTH_PlayerInfo.iCachedKOTHTeam]++
			sFMMCmenu.sKOTH_HostInfo.sHills[iArea].iPlayersInHill++
			
			sFMMCmenu.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPoints = 1
			SET_BIT(sFMMCmenu.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPointsBS, sFMMCmenu.sKOTH_PlayerInfo.iCachedKOTHTeam)
		ELSE
			IF IS_BIT_SET(sFMMCmenu.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPointsBS, sFMMCmenu.sKOTH_PlayerInfo.iCachedKOTHTeam)
				sFMMCmenu.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPoints = 0
				CLEAR_BIT(sFMMCmenu.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPointsBS, sFMMCmenu.sKOTH_PlayerInfo.iCachedKOTHTeam)
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT IS_BIT_SET(sFMMCmenu.sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_AssignedEntities)
		FOR iArea = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
		
			SWITCH g_FMMC_STRUCT.sKotHData.sHillData[iArea].iHillType
				CASE ciKOTH_HILL_TYPE__VEHICLE
					sFMMCmenu.sKOTH_LocalInfo.sHills[iArea].eiMyEntity = sVehStruct.veVehcile[g_FMMC_STRUCT.sKotHData.sHillData[iArea].iHillEntity]
				BREAK
				CASE ciKOTH_HILL_TYPE__OBJECT
					sFMMCmenu.sKOTH_LocalInfo.sHills[iArea].eiMyEntity = sObjStruct.oiObject[g_FMMC_STRUCT.sKotHData.sHillData[iArea].iHillEntity]
				BREAK
			ENDSWITCH
			
		ENDFOR
		
		SET_BIT(sFMMCmenu.sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_AssignedEntities)
	ENDIF
		
	PROCESS_KING_OF_THE_HILL_PLAYER(sFMMCmenu.sKOTH_HostInfo, sFMMCmenu.sKOTH_PlayerInfo, sFMMCmenu.sKOTH_LocalInfo, DEFAULT ,bPlayerIsRespawning)
	
	DRAW_KOTH_TEAM_BAR(sFMMCmenu.sKOTH_HostInfo, sFMMCmenu.sKOTH_PlayerInfo, sFMMCmenu.sKOTH_PlayerInfo.iCachedKOTHTeam, sFMMCmenu.sKOTH_LocalInfo.tlCachedTeamNames[sFMMCmenu.sKOTH_PlayerInfo.iCachedKOTHTeam], FLOOR(sFMMCmenu.sKOTH_PlayerInfo.fKOTHScore), ciKotH_TEST_TARGET_SCORE, HUDORDER_FIFTHBOTTOM, FALSE)
	
	FOR iArea = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
		INT iDifferentTeamsInArea = 0
		
		INT iTeam = 0 
		
		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
			HOST_PROCESS_KOTH_HILL_FOR_TEAM(sFMMCmenu.sKOTH_HostInfo, sFMMCmenu.sKOTH_LocalInfo, iArea, iTeam, iDifferentTeamsInArea)
		ENDFOR
		
		HOST_PROCESS_KOTH_AREA(sFMMCmenu.sKOTH_HostInfo, sFMMCmenu.sKOTH_PlayerInfo, sFMMCmenu.sKOTH_LocalInfo, iArea, iDifferentTeamsInArea)
	ENDFOR
ENDPROC

FUNC BOOL SHOULD_SHOW_MENU_IN_TEST()
	IF bPlayerIsRespawning
		PRINTLN("SHOULD_SHOW_MENU_IN_TEST - bPlayerIsRespawning")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		PRINTLN("SHOULD_SHOW_MENU_IN_TEST - IS_PED_INJURED")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
		WEAPON_TYPE wtPlayerWeapon 

		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtPlayerWeapon)
		AND IS_WEAPON_VALID(wtPlayerWeapon)
			IF is_player_zooming_with_sniper_rifle()
				PRINTLN("SHOULD_SHOW_MENU_IN_TEST - is_player_zooming_with_sniper_rifle")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		PRINTLN("SHOULD_SHOW_MENU_IN_TEST - IS_PAUSE_MENU_ACTIVE")
		RETURN FALSE
	ENDIF
				
	RETURN TRUE
ENDFUNC

PROC PROCESS_DELAYED_PICKUP_SPAWNING_TEST_MODE()
	
	IF IS_BIT_SET(iTestLocalBitSet, ciTestLocalBS_DelayedPickupsSpawned)
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.iSpawnPickupsAtStartTimer = 0
		EXIT
	ENDIF
		
	IF NOT HAS_NET_TIMER_STARTED(tdPickupSpawnTimer)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			REINIT_NET_TIMER(tdPickupSpawnTimer)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_EXPIRED(tdPickupSpawnTimer, g_FMMC_STRUCT.iSpawnPickupsAtStartTimer*1000)
			IF CREATE_TEST_PICKUPS(sWepStruct, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked), FALSE)
				
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "DM_PWR_SPWN", "", DEFAULT, 5000)
				RESET_NET_TIMER(tdPickupSpawnTimer)
				SET_BIT(iTestLocalBitSet, ciTestLocalBS_DelayedPickupsSpawned)
				PRINTLN("PROCESS_DELAYED_PICKUP_SPAWNING_TEST_MODE - Delayed pickups have been spawned!!")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_STAGGERED_PICKUPS()
	
	PROCESS_PICKUP_WEAPON_ATTACHMENTS(GET_PICKUP_OBJECT(sWepStruct.Pickups[iStaggeredPickup]), iStaggeredPickup, DOES_PICKUP_EXIST(sWepStruct.Pickups[iStaggeredPickup]))
	iStaggeredPickup++
	
	IF iStaggeredPickup >= g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
		iStaggeredPickup = 0
	ENDIF
	
ENDPROC

PROC PROCESS_APPLICABILITY_TRIGGERS_TEST()

	IF iTestScore != iMyPreviousScore
		PRINTLN("[Player Modifiers] - PROCESS_APPLICABILITY_TRIGGERS_TEST - Score updated, requested refresh.")
		REQUEST_APPLICABILITY_UPDATE(sPlayerModifierData)
		iMyPreviousScore = iTestScore
	ENDIF
		
	IF sPlayerModifierData.iNextTimeToRefresh != -1
	AND HAS_MODIFIER_SET_TIMER_REQUIREMENT_BEEN_MET(sPlayerModifierData.iTimerConditionTimeStamp, sPlayerModifierData.iNextTimeToRefresh)
		PRINTLN("[Player Modifiers] - PROCESS_APPLICABILITY_TRIGGERS_TEST - Timer updated, requested refresh.")
		REQUEST_APPLICABILITY_UPDATE(sPlayerModifierData)
		sPlayerModifierData.iNextTimeToRefresh = -1
	ENDIF
	
ENDPROC

FUNC INT GET_MAX_NUMBER_OF_LIVES_FOR_PED(PED_INDEX piPed, INT iTeam)
	
	IF iTeam != -1
	AND iTeam < FMMC_MAX_TEAMS
	AND g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
		IF iTestMaxTeamLives[iTeam] = 0 //Infinite lives
			RETURN 0
		ELIF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_SharedTeamLives)
			RETURN iTestMaxTeamLives[iTeam] + iTestTeamExtraLives[iTeam]
		ELSE
			RETURN iTestMaxTeamLives[iTeam] + PICK_INT(piPed = PLAYER_PED_ID(), iTestExtraLives, 0)
		ENDIF
	ENDIF
	
	RETURN iTestMaxLives + PICK_INT(piPed = PLAYER_PED_ID(), iTestExtraLives, 0)
	
ENDFUNC

FUNC INT GET_NUMBER_OF_DEATHS_FOR_PED(INT iTeam, INT iPed = -1)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_SharedTeamLives)
		IF iTeam != -1
		AND g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
			RETURN iTestTeamDeaths[iTeam]
		ENDIF
	ENDIF
	
	IF iPed != -1
		RETURN testPeds[iPed].iDeaths
	ENDIF
	
	RETURN iTestDeaths
ENDFUNC

FUNC BOOL HAS_PED_RUN_OUT_OF_LIVES(INT iTeam, INT iPed = -1)	
	
	IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
		IF iTeam != -1
		AND iTeam < FMMC_MAX_TEAMS
		AND iTestMaxTeamLives[iTeam] = 0
			RETURN FALSE
		ENDIF
	ELSE
		IF iTestMaxLives = 0
			RETURN FALSE
		ENDIF
	ENDIF
	
	
	
	PED_INDEX piPedToCheck = PLAYER_PED_ID()
	
	IF iPed != -1
		piPedToCheck = testPeds[iPed].pedID
		PRINTLN("HAS_PED_RUN_OUT_OF_LIVES ped ", iPed, " Deaths ", GET_NUMBER_OF_DEATHS_FOR_PED(testPeds[iPed].iTeam, iPed), ">= Lives", GET_MAX_NUMBER_OF_LIVES_FOR_PED(piPedToCheck, testPeds[iPed].iTeam))
		RETURN GET_NUMBER_OF_DEATHS_FOR_PED(testPeds[iPed].iTeam, iPed) >= GET_MAX_NUMBER_OF_LIVES_FOR_PED(piPedToCheck, testPeds[iPed].iTeam)
	ENDIF
	
	RETURN GET_NUMBER_OF_DEATHS_FOR_PED(g_FMMC_STRUCT.iTestMyTeam) >= GET_MAX_NUMBER_OF_LIVES_FOR_PED(piPedToCheck, g_FMMC_STRUCT.iTestMyTeam)
ENDFUNC

PROC PRE_FRAME_LIVES_PROCESSING()
	INT iTeamLoop
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
		iTestTeamDeaths[iTeamLoop] = 0
	ENDFOR
	bLivingEnemies = FALSE
ENDPROC

PROC CHECK_IF_ANY_LIVING_ENEMIES_REMAIN(INT iPed)
	
	IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
		IF testPeds[iPed].iTeam = g_FMMC_STRUCT.iTestMyTeam
			EXIT
		ENDIF
	ENDIF
		
	IF !testPeds[iPed].bOutOfLives
	AND !bLivingEnemies
	AND DOES_ENTITY_EXIST(testPeds[iPed].pedID)
		bLivingEnemies = TRUE
		IF !bValidLivesCheck
			bValidLivesCheck = TRUE
		ENDIF
		PRINTLN("[Test Lives] CHECK_IF_ANY_LIVING_ENEMIES_REMAIN - Living enemy found")
	ENDIF
	
ENDPROC

PROC PROCESS_TEST_MODE_LIVES()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LivesEndType)
		EXIT
	ENDIF
	
	PRE_FRAME_LIVES_PROCESSING()
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints != 0
		INT iPedLoop
		FOR iPedLoop = 1 TO g_FMMC_STRUCT.iNumParticipants + 1
			
			IF !testPeds[iPedLoop].bOutOfLives
			AND HAS_PED_RUN_OUT_OF_LIVES(testPeds[iPedLoop].iTeam, iPedLoop)
				testPeds[iPedLoop].bOutOfLives = TRUE
				PRINTLN("[Test Lives] PROCESS_TEST_MODE_LIVES - Ped ", iPedLoop, " is now out of lives!!")
			ENDIF
			
			IF testPeds[iPedLoop].iTeam != -1
				iTestTeamDeaths[testPeds[iPedLoop].iTeam] += testPeds[iPedLoop].iDeaths
			ENDIF
			
			CHECK_IF_ANY_LIVING_ENEMIES_REMAIN(iPedLoop)
			
		ENDFOR
	ENDIF
	
	//Local Player
	IF HAS_PED_RUN_OUT_OF_LIVES(g_FMMC_STRUCT.iTestMyTeam)
		bOutofLives = TRUE
	ENDIF
	iTestTeamDeaths[g_FMMC_STRUCT.iTestMyTeam] += iTestDeaths
	
ENDPROC

PROC PROCESS_TEST_MODE_END_CONDITIONS()
	
	BOOL bTestCompleted

	// Target number
	IF (g_FMMC_STRUCT.iTargetScore < DM_TARGET_KILLS_MAX AND g_FMMC_STRUCT.iTargetScore != 7
	OR IS_KING_OF_THE_HILL())
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LivesEndType)
	
		INT LocalScore
		
		IF NOT IS_KING_OF_THE_HILL()
			LocalScore = GET_TARGET_SCORE_VALUE(g_FMMC_STRUCT.iTargetScore)
		ELSE
			LocalScore = ciKotH_TEST_TARGET_SCORE
		ENDIF
		
		IF LocalScore > -1
			
			IF IS_LEGACY_DEATHMATCH_CREATOR()
				bTestCompleted = iTestKills >= LocalScore
				IF bTestCompleted
					PRINTLN("PROCESS_TEST_MODE_END_CONDITIONS - Legacy score target reached!")
				ENDIF
			ELSE
				bTestCompleted = iTestScore >= LocalScore
				IF bTestCompleted
					PRINTLN("PROCESS_TEST_MODE_END_CONDITIONS - Score target reached!")
				ENDIF
			ENDIF
			
			IF IS_KING_OF_THE_HILL()
				IF KOTH_TEST_COMPLETE()
					PRINTLN("[KOTH] PROCESS_TEST_MODE_END_CONDITIONS - Test complete!")
					bTestCompleted = TRUE
				ENDIF
			ENDIF
			
			IF NOT bTestCompleted
				IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
				AND NOT CONTENT_IS_USING_ARENA()
				AND NOT IS_KING_OF_THE_HILL()
					STRING NewTarget = "HUD_TARG"
					DRAW_GENERIC_SCORE(LocalScore, NewTarget, 0, HUD_COLOUR_WHITE, HUDORDER_THIRDBOTTOM, FALSE, "")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LivesEndType)
		IF bOutofLives
			bTestCompleted = TRUE
			PRINTLN("[Test Lives] PROCESS_TEST_MODE_END_CONDITIONS - Local player is out of lives")
		ENDIF
		
		IF !bLivingEnemies
		AND bValidLivesCheck
			bTestCompleted = TRUE
			PRINTLN("[Test Lives] PROCESS_TEST_MODE_END_CONDITIONS - No living enemies remain")
		ENDIF
	ENDIF
	
	// Timer
	IF g_FMMC_STRUCT.iRoundTime > -1
	AND TEST_TIMER >= 0
		IF GET_TEST_HUD_TIME(g_FMMC_STRUCT.iRoundTime) >= 0
			IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
				DRAW_GENERIC_TIMER(GET_TEST_HUD_TIME(g_FMMC_STRUCT.iRoundTime), "TIMER_TIME",0, TIMER_STYLE_DONTUSEMILLISECONDS, 0, PODIUMPOS_NONE, HUDORDER_SECONDBOTTOM)
			ENDIF
		ELSE
			bTestCompleted = TRUE
		ENDIF
	ENDIF
	
	IF bTestCompleted
		SET_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
		IF iGlobalTestKills > ciTest_MinimumScore
		OR (IS_KING_OF_THE_HILL() AND KOTH_TEST_COMPLETE())
		OR NOT IS_LEGACY_DEATHMATCH_CREATOR()
			IF bTestStatBeenIncreased = FALSE
				sCurrentVarsStruct.creationStats.iTimesTestedLoc++
				sCurrentVarsStruct.creationStats.bMadeAChange = FALSE
				IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
					SET_BIT(g_FMMC_STRUCT.biTeamTestComplete, g_FMMC_STRUCT.iTestMyTeam)
				ENDIF
			ENDIF
		ELSE
			PRINTLN("PROCESS_TEST_MODE_END_CONDITIONS - Test completed but not valid!")
		ENDIF
		CHECK_FOR_END_CONDITIONS_MET()
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PROCESS_DM_BOUNDS()

	IF bPlayerIsRespawning
		REINIT_NET_TIMER(tdBlockBoundsOnRespawnTimer)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdBlockBoundsOnRespawnTimer)
		IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdBlockBoundsOnRespawnTimer, ciBlockBoundsOnRespawnTimerLength)
			PRINTLN("SHOULD_PROCESS_DM_BOUNDS - tdBlockBoundsOnRespawnTimer ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdBlockBoundsOnRespawnTimer)," / ", ciBlockBoundsOnRespawnTimerLength)
			RETURN FALSE
		ELSE
			RESET_NET_TIMER(tdBlockBoundsOnRespawnTimer)
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_BOT_MODIFIER_SET()
	
	IF IS_THIS_LEGACY_DM_CONTENT()
		EXIT
	ENDIF
	
	INT itestPed
	FOR itestPed = 0 TO (FMMC_MAX_SPAWNPOINTS)-1
		IF testPeds[itestPed].iRespawnTimer != 0
			SET_LONG_BIT(iPedNeedsModifierReapplied, itestPed)
		ELSE
			IF IS_LONG_BIT_SET(iPedNeedsModifierReapplied, itestPed)
				IF DOES_ENTITY_EXIST(testPeds[itestPed].pedID)
				AND IS_CURRENT_MODIFIER_SET_VALID(sTestPedModifiers[testPeds[itestPed].iTeam].iCurrentModSet)
					APPLY_MOD_SET_TO_TEST_MODE_BOT(testPeds[itestPed].pedID, sTestPedModifiers[testPeds[itestPed].iTeam].iCurrentModSet)
					PRINTLN("PROCESS_BOT_MODIFIER_SET - Reapplying mod set to ped ", itestPed)
				ENDIF
				CLEAR_LONG_BIT(iPedNeedsModifierReapplied, itestPed)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC DEAL_WITH_TEST_BEING_ACTIVE()

	FLOAT fVehicleDense
	fVehicleDense = 1.0 - TO_FLOAT(g_FMMC_STRUCT.iTraffic)
	FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME(0,0,0, fVehicleDense, fVehicleDense, fVehicleDense, fVehicleDense)
	
	PROCESS_KEEP_PLAYER_MODIFIER_SCORES_TO_CHECK_UP_TO_DATE(sPlayerModifierData, iIndividualScore, iTestScore)
	
	PROCESS_APPLICABILITY_TRIGGERS_TEST()
	PROCESS_APPLICABILITY_EVERY_FRAME(sPlayerModifierData, iCurrentPlayerModSet, iPendingPlayerModSet)
	
	PROCESS_DISABLED_CONTROL_ACTIONS(sPlayerModifierData)

	MAINTAIN_SNOWBALLS(SnowballData)
	
	PROCESS_TEST_MODE_LIVES()
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
		SWITCH g_FMMC_STRUCT.iTimeOfDay
			CASE TIME_OFF
			BREAK
			CASE TIME_MORNING
				NETWORK_OVERRIDE_CLOCK_TIME(6, 0, 0)			
			BREAK
			CASE TIME_NOON
				NETWORK_OVERRIDE_CLOCK_TIME(12, 0, 0)	
			BREAK
			CASE TIME_NIGHT
				NETWORK_OVERRIDE_CLOCK_TIME(21, 0, 0)
			BREAK
		ENDSWITCH
	ELSE
		NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
	ENDIF
	
	IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
	AND NOT CONTENT_IS_USING_ARENA()
	AND NOT IS_KING_OF_THE_HILL()
		DRAW_GENERIC_BIG_NUMBER(iTestKills, "LBD_KIL", 0, FRIENDLY_DM_COLOUR(), HUDORDER_SIXTHBOTTOM, FALSE, "", FRIENDLY_DM_COLOUR())
		
		IF NOT IS_LEGACY_DEATHMATCH_CREATOR()
			DRAW_GENERIC_BIG_NUMBER(iTestScore, "LBD_SCO", 0, FRIENDLY_DM_COLOUR(), HUDORDER_FIFTHBOTTOM, FALSE, "", FRIENDLY_DM_COLOUR())
		ENDIF
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
			PRINTLN("[TMS][ARENA] Killing player for being out of their vehicle!")
		ENDIF
	ENDIF
	
	//PROCESS_DM_HELPTEXT(iHelptextBitset, stHelptextTimer)
	
	PROCESS_DM_RESPAWN_ENTITITES(sFMMCmenu.sKOTH_HostInfo)
	
	IF IS_KING_OF_THE_HILL()
		PROCESS_KING_OF_THE_HILL_TEST_MODE()
	ENDIF
	
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	INT iBoundsIndex = GET_CURRENT_DM_BOUNDS_INDEX(sPlayerModifierData)
	
	sFMMCmenu.sShrinkingBoundsConditionData.iDeathmatchDuration = GET_TEST_DURATION(g_FMMC_STRUCT.iRoundTime)
	PROCESS_DEATHMATCH_SHRINKING_BOUNDS_CLIENT(sFMMCmenu.sBoundsServerData, sFMMCmenu.sBoundsClientData, sFMMCmenu.sShrinkingBoundsLocalRuntimeData, 
												sFMMCmenu.sDeathmatchBoundsData, sFMMCmenu.sShrinkingBoundsConditionData, IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive))
	
	IF SHOULD_PROCESS_DM_BOUNDS()
		BOOL bDrawMarker = (sFMMCmenu.sShrinkingBoundsLocalRuntimeData[iBoundsIndex].eShrinkingBoundsState >= DM_SHRINKING_BOUNDS_STATE_SHRINK)
		
		PRINTLN("[DMBOUNDS] Bounds index: ", iBoundsIndex)
		PRINTLN("[DMBOUNDS] Shrinking bounds state: ", ENUM_TO_INT(sFMMCmenu.sShrinkingBoundsLocalRuntimeData[iBoundsIndex].eShrinkingBoundsState))
		
		PROCESS_DEATHMATCH_BOUNDS(sFMMCmenu.sDeathmatchBoundsData, iBoundsIndex, sFMMCMenu.iPreviousBoundsIndex, piPlayerPed, bDrawMarker)
	ENDIF
	
	PROCESS_FMMC_POWERUPS(sPowerUps)
	PROCESS_POWERUP_HUD(sPowerUps)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
		UPDATE_VEHICLE_BOOSTING()
		UPDATE_VEHICLE_ROCKETS()
	ENDIF
	
	PROCESS_DELAYED_PICKUP_SPAWNING_TEST_MODE()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisableGTARaceVehicleExit)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	
	PROCESS_DEATHMATCH_LIVES_REMAINING(GET_NUMBER_OF_DEATHS_FOR_PED(g_FMMC_STRUCT.iTestMyTeam), GET_MAX_NUMBER_OF_LIVES_FOR_PED(PLAYER_PED_ID(), g_FMMC_STRUCT.iTestMyTeam))
	
	PROCESS_FMMC_VEHICLE_MACHINE_GUN_FIRING(sVehMG, sVehMG_PBD)
	PROCESS_FMMC_VEHICLE_MACHINE_GUN_HUD(sVehMG)
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps > 0
	AND g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps <= GET_FMMC_MAX_WORLD_PROPS()
		LEGACY_PROCESS_WORLD_PROPS(iWorldPropIterator, sRuntimeWorldPropData)
		iWorldPropIterator++
		IF iWorldPropIterator >= g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps
			iWorldPropIterator = 0
		ENDIF
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_TEST_BASE
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
		
		//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	ENDIF
	
	PROCESS_TEST_MODE_END_CONDITIONS()

	CHECK_TIME_FOR_VALID_TEST()	
	CHECK_FOR_PLAYER_KILL()
	
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL) // DO NOT LET THEM SWITCH
	
	BLIP_PICKUPS_CREATOR(sWepStruct)
	
	PROCESS_STAGGERED_PICKUPS()
	
	BLIP_ENEMIES_IN_DM_TEST(testPeds)
	PROCESS_DM_CREATOR_EVENTS()
	
	IF NOT SHOULD_USE_VEHICLES(g_FMMC_STRUCT.iTestMyTeam, iCurrentPlayerModSet)
		DM_TEST_RESPAWN_PLAYER()
		bHasSpawned = TRUE
	ELSE
		DM_TEST_RESPAWN_PLAYER(TRUE, PROCESS_DM_TEST_MANUAL_RESPAWN())
	ENDIF
		
	IF NOT CONTENT_IS_USING_ARENA()
	AND g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints != 0
		INT iPed
		FOR iPed = 1 TO g_FMMC_STRUCT.iNumParticipants+1
			IF NOT SHOULD_USE_VEHICLES(DEFAULT, sTestPedModifiers[testPeds[iPed].iTeam].iCurrentModSet)
				RESPAWN_ENEMIES_IN_DM_TEST(iPed, DM_TestPedModel, lrgFM_Team, testPeds, iPlayerTeamNum, SHOULD_USE_TEAM_SPAWN(bHasSpawned), sTestPedModifiers[testPeds[iPed].iTeam].iCurrentModSet)
			ELSE
				RESPAWN_ENEMIES_IN_VDM_TEST(iPed, DM_TestPedModel, lrgFM_Team, testPeds, iRand, GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel), mnTeamVehicle)
			ENDIF
		ENDFOR
		PROCESS_BOT_MODIFIER_SET()
	ENDIF
	
	IF NOT SCRIPT_IS_CLOUD_AVAILABLE()
		SET_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
		CHECK_FOR_END_CONDITIONS_MET()
	ENDIF
	
	IF SHOULD_SHOW_MENU_IN_TEST()
		IF SHOULD_MENU_BE_DRAWN(sFMMCData, sFMMCMenu, sCurrentVarsStruct, menuScrollController, 2, TRUE, bShowMenu)
		AND NOT FMMC_PROCESS_ENTITY_LIST_MENU(sFMMCMenu, sFMMCData, sCurrentVarsStruct)
		
			SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(2)
			FMMC_DO_MENU_ACTIONS(	RETURN_CIRCLE_MENU_SELECTIONS(),
									sCurrentVarsStruct, sFMMCmenu, 
									sFMMCData, sFMMCendStage, sHCS)
									
			iTestMenuTimestamp = GET_FRAME_COUNT()
			bShowMenuHighlight = TRUE
		ELSE
			PRINTLN("[DMTEST] Hiding menu because of SHOULD_MENU_BE_DRAWN")
		ENDIF
		
		IF iTestMenuTimestamp >= GET_FRAME_COUNT() - 100
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		ENDIF
		
		IF sCurrentVarsStruct.bResetUpHelp
			sCurrentVarsStruct.bResetUpHelp  = FALSE
		ENDIF
	ENDIF

	
	IF DOES_ENTITY_EXIST(sCurrentVarsStruct.viMapEscapeVeh)
		DELETE_VEHICLE(sCurrentVarsStruct.viMapEscapeVeh)
	ENDIF
	
	PROCESS_DYNOPROPS_DMTEST()
	PROCESS_PROPS_DMTEST()
	
	IF g_FMMC_STRUCT.iTestMyTeam > -1
	AND IS_TEAM_DEATHMATCH()
		IF g_FMMC_STRUCT.iTeamWanted[g_FMMC_STRUCT.iTestMyTeam] > 1
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < g_FMMC_STRUCT.iTeamWanted[g_FMMC_STRUCT.iTestMyTeam] - 1
			SET_MAX_WANTED_LEVEL(g_FMMC_STRUCT.iTeamWanted[g_FMMC_STRUCT.iTestMyTeam] - 1)
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), g_FMMC_STRUCT.iTeamWanted[g_FMMC_STRUCT.iTestMyTeam] - 1)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			
			PRINTLN("[TMSDMC] Setting player's wanted level to ", g_FMMC_STRUCT.iTeamWanted[g_FMMC_STRUCT.iTestMyTeam] - 1)
		ENDIF
	ENDIF
	
	TARGET_DRAW_FLOATING_SCORES(sTargetFloatingScores)
	
ENDPROC 

PROC SET_DLC_AND_CHECK_FOR_LEGACY()
	IF g_FMMC_STRUCT.iDLCRelease = -1
	AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionName)
	AND NOT IS_KING_OF_THE_HILL_CREATOR()
	AND NOT IS_ARENA_CREATOR()
	AND IS_DEATHMATCH_CREATOR_UPDATE_ALLOWED()
		SETUP_FRESH_DEATHMATCH_UPDATE_SETTING_DEFAULTS()
		PRINTLN("SET_DLC_AND_CHECK_FOR_LEGACY - Setting DLC Release: ", g_FMMC_STRUCT.iDLCRelease)
	ENDIF
	IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_DLC_1_2022)
		PRINTLN("SET_DLC_AND_CHECK_FOR_LEGACY - g_FMMC_STRUCT.iDLCRelease: ", g_FMMC_STRUCT.iDLCRelease)
		g_bFMMC_EnteredLegacyDeathmatchCreator = TRUE
	ENDIF
ENDPROC

PROC PROCESS_PRE_GAME()
	SET_BIT(sFMMCdata.iBitSet, bCameraActive)
	
	SET_CREATOR_AUDIO(TRUE, FALSE)
	
	SETUP_FMMC_SCRIPT(sFMMCMenu, sHCS, sCurrentVarsStruct)
	INITIALISE_MODEL_ARRAYS(sPedStruct,	sWepStruct, sObjStruct, sFMMCmenu, g_FMMC_STRUCT.iVehicleDeathmatch = 0)
		
	sFMMCmenu.iScriptCreationType	= SCRIPT_CREATION_TYPE_DEATHMATCH
	
	FMMC_SET_INTIAL_MISSION_OPTION_VALUES()
	if g_bFMMC_TutorialSelectedFromMpSkyMenu = TRUE
		tutDMCreator.bDisabled = FALSE		
	ENDIF
	
	// Setup rel groups
	INT i
	REPEAT FMMC_MAX_SPAWNPOINTS i
		TEXT_LABEL_63 tlRelGroup
		tlRelGroup = "rgFM_DEATHMATCH"
		tlRelGroup += i
		ADD_RELATIONSHIP_GROUP(tlRelGroup, lrgFM_Team[i])
	ENDREPEAT	
	
	i = 0
	REPEAT FMMC_MAX_SPAWNPOINTS i
		SET_TEAM_RELATIONSHIPS(ACQUAINTANCE_TYPE_PED_HATE, lrgFM_Team, i)
	ENDREPEAT
	
	FOR i = 0 TO PROP_LIBRARY_MAX_ITEMS-1
		sFMMCmenu.iNumberOfPropsInLibrary[i] = -1
	ENDFOR
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	g_FMMC_STRUCT.iTeamWanted[0] = 1
	g_FMMC_STRUCT.iTeamWanted[1] = 1
	g_FMMC_STRUCT.iTeamWanted[2] = 1
	g_FMMC_STRUCT.iTeamWanted[3] = 1
	
	g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] = 0
	g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] = 0
	g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] = 0
	g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[3] = 0
	g_FMMC_STRUCT.tl63MissionName = ""
	g_FMMC_STRUCT.tl63MissionDecription[0] = ""
	g_FMMC_STRUCT.iMinNumParticipants = 2
	g_FMMC_STRUCT.iMaxNumberOfTeams = 2
	
	g_FMMC_STRUCT.iVehicleModel = 9
	
	DM_TestPedModel[0] = G_M_Y_Lost_02
	DM_TestPedModel[1] = G_M_Y_MexGoon_02
	DM_TestPedModel[2] = G_M_Y_BallaOrig_01
	DM_TestPedModel[3] = G_M_Y_Korean_01
	
	i = 0
	REPEAT FMMC_MAX_SPAWNPOINTS i
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()), lrgFM_Team[i])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, lrgFM_Team[i], GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
	ENDREPEAT
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)	
	
	g_FMMC_STRUCT.iMusic = GET_RANDOM_INT_IN_RANGE(0, ciRC_MAX_TYPE_RACE_RADIO)
	STRING sTempRadio
	sTempRadio = GET_RADIO_STATION_NAME(g_FMMC_STRUCT.iMusic)
	g_FMMC_STRUCT.iMusic = GET_HASH_KEY(sTempRadio)
	
	iMaxSpawnsPerTeam[0] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/2.0)
	iMaxSpawnsPerTeam[1] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/2.0)
	iMaxSpawnsPerTeam[2] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/3.0)
	iMaxSpawnsPerTeam[3] = ROUND(TO_FLOAT(g_FMMC_STRUCT.iNumParticipants + 2)/4.0)
	
	ADJUST_NUMBER_OF_PLAYERS_ON_TEAM_BASED_ON_RATIO(iMaxSpawnsPerTeam)
	
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_SPAWN] = 2
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_RAND_WEAPONS] = 2
	sFMMCmenu.sRandStruct.iPedLevel = 2
	sFMMCmenu.sRandStruct.iWeaponLevel = 2

	IF CONTENT_IS_USING_ARENA()
		INIT_UGC_ARENA_PREGAME(sTrapInfo_Local)
	ENDIF
	
	IF !g_bFMMC_LoadFromMpSkyMenu
		SET_DLC_AND_CHECK_FOR_LEGACY()
	ENDIF
	
	RESET_PLACED_PROP_ARRAY()
	
ENDPROC

PROC CONTROL_CAMERA_AND_CORONA()
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)		
		IF HAS_DM_CREATOR_TUTORIAL_COMPLETED(tutDMCreator)
		OR IS_KING_OF_THE_HILL()
			DEAL_WITH_HITTING_MAX_ENTITES(sCurrentVarsStruct, sFMMCmenu)
		ELSE
			CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)
			IF IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciTOO_MANY_WEAPONS)
			AND tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_SWITCH_CAM
				SET_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)
			ELSE
				DEAL_WITH_HITTING_MAX_ENTITES(sCurrentVarsStruct, sFMMCmenu)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)	
			MAINTAIN_FMMC_CAMERA(sFMMCdata, sCamData, sCurrentVarsStruct.iMenuState, DEFAULT, NOT (HAS_DM_CREATOR_TUTORIAL_COMPLETED(tutDMCreator) OR IS_KING_OF_THE_HILL()))
		ELSE
			IF IS_PLAYER_IN_A_MAP_ESCAPE(PLAYER_PED_ID())
				SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,FALSE,FALSE,TRUE,FALSE)
			ENDIF
		ENDIF
		
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()	
		AND sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
			
			VECTOR vDummyRot = <<0,0,0>>
			IF DOES_CAM_EXIST(sCamData.cam)
				vDummyRot = GET_CAM_ROT(sCamData.cam)
			ENDIF
			
			INT iOkToPlace = sCurrentVarsStruct.bitsetOkToPlace
			
			MAINTAIN_SCREEN_CENTER_COORD(vDummyRot, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, 	sHCS.hcCurrentCoronaColour, sFMMCmenu.fCreationHeightIncrease, sFMMCmenu.fCreationHeightIncrease != 0.0, FALSE, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), FALSE, g_FMMC_STRUCT.iVehicleDeathmatch = 1)
			
			IF iOkToPlace != sCurrentVarsStruct.bitsetOkToPlace
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL THE_HELP_MESSAGE_HAS_FINISHED()
	IF (GET_GAME_TIMER() - tutDMCreator.iTutTimer) > 10000
	OR NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	#IF IS_DEBUG_BUILD	
		or IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_DM_CREATOR_TUTORIAL()
	
	bShowMenu = FALSE
	PRINTLN("[TMSTEMP] bShowMenu = FALSE (2)")
	
	sTutorialDescription = ""
	ALLOW_MISSION_CREATOR_WARP(FALSE)
	
	SWITCH tutDMCreator.tutorialStage
		CASE DM_CREATOR_TUTORIAL_STAGE_INIT	
			if not IS_SCREEN_FADING_OUT()	
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					
					sCurrentVarsStruct.bDisableEditingDuringTutorial = FALSE
					
					SET_ALL_DM_MENU_ITEMS_OFF(sFMMCmenu)
       				CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_CREATOR)	
					CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)	
					CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
					CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_RADIO)
					
					REFRESH_MENU(sFMMCMenu)
					
					tutDMCreator.iTutTimer = GET_GAME_TIMER()
					tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_INTRO 
				ENDIF
			ENDIF		
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_INTRO
			IF (GET_GAME_TIMER() - tutDMCreator.iTutTimer) > 2000
				IF HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
					PRINT_HELP("FMMC_DM_T0", 10000)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()	
						tutDMCreator.iTutTimer = GET_GAME_TIMER()
						tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_INTRO_HELP2
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_INTRO_HELP
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				PRINT_HELP("FMMC_DM_T16", 10000) 
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()	
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_INTRO_HELP2
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_INTRO_HELP2
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				PRINT_HELP("DM_T_DET", 10000) 
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()	
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_DETAILS_HELP
				//tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_INST //warp help
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_INST
		
			IF THE_HELP_MESSAGE_HAS_FINISHED()		
				PRINT_HELP("FMMC_DM_TMW", 10000)
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_DETAILS_HELP
			ENDIF
			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_DETAILS_HELP
			IF THE_HELP_MESSAGE_HAS_FINISHED()				
				CLEAR_HELP()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_SELECT_DETAILS				
			ENDIF			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_SELECT_DETAILS
			
			bShowMenu = TRUE
			
			sTutorialDescription = "FMMC_DM_T17"
			
			IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE	
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EDIT_DETAILS
			ENDIF
			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_EDIT_DETAILS	
		
			IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				sTutorialDescription = "FMMC_DM_T17"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
				IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO)	
					sTutorialDescription = "RC_T_DTP"
				ELSE
					sTutorialDescription = "DM_T_RDT"
				ENDIF
			ELSE
				sTutorialDescription = "FMMC_DM_T18"
			ENDIF

			IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
			AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_DESC)
			AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO)		
			AND sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
			AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
			AND sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
				PRINT_HELP("FMMC_DM_T52", 10000) //The options menu is where you can fine tune your deathmatch by changing weapon loadouts, time limits or voice chat.
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()	
					SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_CREATOR)			
					tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_HELP
					tutDMCreator.iTutTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				bShowMenu = TRUE
			ENDIF
			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_HELP
			IF THE_HELP_MESSAGE_HAS_FINISHED()			
				
				IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
					PRINT_HELP("DM_T_TRGH", 10000)
					sTutorialDescription = "FMMC_DM_T50"
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					tutDMCreator.iTutTimer = GET_GAME_TIMER()
					tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_WARP_EXP
					ENDIF
				ELSE
					sTutorialDescription = "DM_T_DETL"
					bShowMenu = TRUE
				ENDIF
			ENDIF
		BREAK		
		
		CASE DM_CREATOR_TUTORIAL_STAGE_WARP_EXP
		
			IF THE_HELP_MESSAGE_HAS_FINISHED()		
				CLEAR_HELP()
				TURN_OFF_MENU_OPTIONS_DURING_TUTORIAL(START_MENU_ITEM_DM)
				iHelpBitSetOld = -1
				sCurrentVarsStruct.bResetUpHelp  = TRUE
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_SELECT_TRIGGER
			ENDIF
			
		BREAK
		
		
		
		CASE DM_CREATOR_TUTORIAL_STAGE_SELECT_TRIGGER
		
			
			IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				sTutorialDescription = "FMMC_DM_T11"				
			ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				sTutorialDescription = "FMMC_DM_T50"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
				sTutorialDescription = "FMMC_DM_T38"
			ELSE
				sTutorialDescription = "FMMC_DM_T12"
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_SELECT_START_END
				PRINT_HELP("DM_T_TRGH1", 10000) //The trigger location is the most important part of a deathmatch. T
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()	
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_MORE_TRIGGER_HELP			
				tutDMCreator.iTutTimer = GET_GAME_TIMER()	
				ENDIF
			ELSE
				bShowMenu = TRUE 
			ENDIF
			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_MORE_TRIGGER_HELP
		
			IF THE_HELP_MESSAGE_HAS_FINISHED()		
				CLEAR_HELP()
				TURN_OFF_MENU_OPTIONS_DURING_TUTORIAL(START_MENU_ITEM_DM)
				iHelpBitSetOld = -1
				sCurrentVarsStruct.bResetUpHelp  = TRUE
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_MOVE
			ENDIF
			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_PLACE_TRIGGER_MOVE
		
			bShowMenu = TRUE 
			
			IF sFMMCmenu.sActiveMenu = eFmmc_SELECT_START_END			
				sTutorialDescription = "FMMC_DM_T2"	
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				sTutorialDescription = "FMMC_DM_T11"				
			ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				sTutorialDescription = "FMMC_DM_T50"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
				sTutorialDescription = "FMMC_DM_T38"
			ELSE
				sTutorialDescription = "FMMC_DM_T12"
			ENDIF
			
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)	
			AND iTriggerCreationStage < CREATION_STAGE_DONE	
				PRINT_HELP("DM_T_LOBH", 10000) //The trigger location is the most important part of a deathmatch. T
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()	
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_LOBBY_CAM_HELP			
				tutDMCreator.iTutTimer = GET_GAME_TIMER()	
				ENDIF
			ENDIF
			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_LOBBY_CAM_HELP
		
			IF THE_HELP_MESSAGE_HAS_FINISHED()			
				CLEAR_HELP()
				SET_LONG_BIT(sFMMCMenu.iBitActive, PANCAM_MENU_ITEM_DM)
				iHelpBitSetOld = -1
				sCurrentVarsStruct.bResetUpHelp  = TRUE
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_LOBBY_CAM_PLACE
			ENDIF
			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_LOBBY_CAM_PLACE
		
			bShowMenu = TRUE 
			
			IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE			
				sTutorialDescription = "DM_T_LOB1"	
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				sTutorialDescription = "DM_T_LOB0"				
			ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				sTutorialDescription = "FMMC_DM_T50"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
				sTutorialDescription = "FMMC_DM_T38"
			ELSE
				sTutorialDescription = "FMMC_DM_T12"
			ENDIF
			
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraPanPos)	
			
				sTutorialDescription = "FMMC_DM_T12"
				SET_LONG_BIT(sFMMCMenu.iBitActive, SPAWN_MENU_ITEM_DM)
				
				IF sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE	
					PRINT_HELP("FMMC_DM_T4", 10000) //The trigger location is the most important part of a deathmatch. T
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()	
					tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_1ST_SPAWN_INST			
					tutDMCreator.iTutTimer = GET_GAME_TIMER()	
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_1ST_SPAWN_INST
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				CLEAR_HELP()
				sCurrentVarsStruct.bResetUpHelp  = TRUE
				tutDMCreator.iTutTimer = GET_GAME_TIMER()	
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_SELECT_SPAWNS		
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_SELECT_SPAWNS
						
			IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				sTutorialDescription = "FMMC_DM_T50"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
				sTutorialDescription = "FMMC_DM_T38"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				sTutorialDescription = "FMMC_DM_T20"
			ELSE
				sTutorialDescription = "FMMC_DM_T12"
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
				PRINT_HELP("DM_T_SPWH", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()	
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_1ST_SPAWN_INST2	
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				bShowMenu = TRUE
			ENDIF
				
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_1ST_SPAWN_INST2
			
			DRAW_GENERIC_BIG_NUMBER(((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints,"FMMC_ER_B4", -1, HUD_COLOUR_WHITE, HUDORDER_TOP, FALSE, NULL_STRING(), HUD_COLOUR_WHITE, FALSE, HUDFLASHING_NONE, 0)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints, FMMC_MAX_SPAWNPOINTS, "FMMC_AB_05", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0, IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS))		
			SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(2)
			
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					PRINT_HELP("DM_T_SPWD_PC", 10000)
				ELSE
					PRINT_HELP("DM_T_SPWD", 10000)
				ENDIF
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_1ST_SPAWN_INST3
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_1ST_SPAWN_INST3
		
			DRAW_GENERIC_BIG_NUMBER(((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints,"FMMC_ER_B4", -1, HUD_COLOUR_WHITE, HUDORDER_TOP, FALSE, NULL_STRING(), HUD_COLOUR_WHITE, FALSE, HUDFLASHING_NONE, 0)
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints, FMMC_MAX_SPAWNPOINTS, "FMMC_AB_05", -1, HUD_COLOUR_WHITE, HUDORDER_DONTCARE, FALSE, HUDFLASHING_NONE, 0, IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS))		
			SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(2)
			
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				CLEAR_HELP()
				sCurrentVarsStruct.bResetUpHelp  = TRUE
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_PLACE_ALL_SPAWNS
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_PLACE_ALL_SPAWNS
			bShowMenu = TRUE			
			
			IF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT			
				sTutorialDescription = "FMMC_DM_T5"					
			ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				sTutorialDescription = "FMMC_DM_T50"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
				sTutorialDescription = "FMMC_DM_T38"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				sTutorialDescription = "FMMC_DM_T20"
			ELSE
				sTutorialDescription = "FMMC_DM_T12"
			ENDIF
			
			if g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints >= (g_FMMC_STRUCT.iNumParticipants + 2) * 2
				PRINT_HELP("DM_T_TSH1", 10000) //The first stage is to place a launch location for the deathmatch. This is where players will meet to launch your deathmatch.
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()	
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_TEAM_SPAWNS1
				SET_LONG_BIT(sFMMCMenu.iBitActive, TEAM_SPAWN_MENU_ITEM_DM)						
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
				
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_TEAM_SPAWNS1
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				PRINT_HELP("DM_T_TSH2", 10000) //The trigger location is the most important part of a deathmatch. T
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()	
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_TEAM_SPAWNS2			
				tutDMCreator.iTutTimer = GET_GAME_TIMER()	
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_TEAM_SPAWNS2
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				CLEAR_HELP()
				sCurrentVarsStruct.bResetUpHelp  = TRUE
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_PLACE_TEAM_SPAWNS
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_PLACE_TEAM_SPAWNS
		
			bShowMenu = TRUE			
			
			IF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT			
				sTutorialDescription = "DM_T_TSH4"					
			ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				sTutorialDescription = "FMMC_DM_T50"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
				sTutorialDescription = "FMMC_DM_T38"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				sTutorialDescription = "DM_T_TSH3"
			ELSE
				sTutorialDescription = "FMMC_DM_T12"
			ENDIF
			
			IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)
				PRINT_HELP("FMMC_DM_T10", 10000) //The first stage is to place a launch location for the deathmatch. This is where players will meet to launch your deathmatch.
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()	
					tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_WEAPONS
					SET_LONG_BIT(sFMMCMenu.iBitActive, WEP_MENU_ITEM_DM)						
					tutDMCreator.iTutTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_WEAPONS
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				CLEAR_HELP()
				REFRESH_MENU(sFMMCMenu)
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_PLACE_ALL_WEAPONS
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_PLACE_ALL_WEAPONS
			bShowMenu = TRUE	
			
			IF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE	
				DRAW_GENERIC_BIG_NUMBER(6 - g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons,"DM_T_WEPC", -1, HUD_COLOUR_WHITE, HUDORDER_TOP, FALSE, NULL_STRING(), HUD_COLOUR_WHITE, FALSE, HUDFLASHING_NONE, 0) //sWepStruct.flashColour
				sTutorialDescription = "FMMC_DM_T14"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				sTutorialDescription = "FMMC_DM_T13"					
			ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				sTutorialDescription = "FMMC_DM_T50"
			ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
				sTutorialDescription = "FMMC_DM_T38"
			ELSE
				sTutorialDescription = "FMMC_DM_T12"
			ENDIF
			
			

			if g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons > 5
				PRINT_HELP("FMMC_DM_T29", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_CAM1
				ENDIF
			ENDIF
			
		BREAK
			
		CASE DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_CAM1
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					PRINT_HELP("FMMC_DM_T30_PC", 10000)
				ELSE
					PRINT_HELP("FMMC_DM_T30", 10000)
				ENDIF
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_CAM2
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_CAM2
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				CLEAR_HELP()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_SWITCH_CAM
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_SWITCH_CAM
			
			sTutorialDescription = "FMMC_DM_T31"
			
			if iTutorialPrevWepNum != 0
				iTutorialPrevWepNum = g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
			ENDIF
			
			IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				
				IF iTutorialPrevWepNum = g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
					tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_PLACE_IN_STORE
				ELSE
					PRINT_HELP("FMMC_DM_T54", 10000)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_SWITCH_CAM2	
					tutDMCreator.iTutTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ELSE
				bShowMenu = TRUE
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_SWITCH_CAM2		
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons >= FMMC_MAX_WEAPONS
					
					PRINT_HELP("FMMC_DM_T22", 10000)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					tutDMCreator.iTutTimer = GET_GAME_TIMER()
					tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_DM_ELEMENTS
					SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
					ENDIF
				ELSE
					
					PRINT_HELP("FMMC_DM_T55", 10000)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_ONFOOT_PLACEMENT
						tutDMCreator.iTutTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_ONFOOT_PLACEMENT		
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				REFRESH_MENU(sFMMCMenu)
				tutDMCreator.iTutTimer = GET_GAME_TIMER()				
				iTutorialPrevWepNum = g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_PLACE_IN_STORE
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_PLACE_IN_STORE
			bShowMenu = TRUE			
			
			IF IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
				IF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE			
					sTutorialDescription = "FMMC_DM_T34"
				ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
					sTutorialDescription = "FMMC_DM_T13"					
				ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
					sTutorialDescription = "FMMC_DM_T50"
				ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
					sTutorialDescription = "FMMC_DM_T38"
				ELSE
					sTutorialDescription = "FMMC_DM_T12"
				ENDIF
			ELSE
				sTutorialDescription = "FMMC_T_OM"
			ENDIF
			
			IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				CLEAR_HELP()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				iTutorialPrevWepNum = g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_SWITCH_CAM
			ENDIF
			
			IF iTutorialPrevWepNum > g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
				iTutorialPrevWepNum = g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
			ENDIF
			
			IF iTutorialPrevWepNum < g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
			OR g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons >= FMMC_MAX_WEAPONS
				PRINT_HELP("FMMC_DM_T22", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					tutDMCreator.iTutTimer = GET_GAME_TIMER()
					
					tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_DM_ELEMENTS
					SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
				ENDIF
			ELSE
				if IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
					IF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE
						DRAW_GENERIC_BIG_NUMBER(1,"DM_T_WEPC", -1, HUD_COLOUR_WHITE, HUDORDER_TOP, FALSE, NULL_STRING(), HUD_COLOUR_WHITE, FALSE, HUDFLASHING_NONE, 0)
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_DM_ELEMENTS
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				PRINT_HELP("FMMC_DM_T27", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					tutDMCreator.iTutTimer = GET_GAME_TIMER()
					tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_RANDOMIZER
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_RANDOMIZER
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()				
				
				IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)	
				AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)	
				AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)	
					PRINT_HELP("FMMC_DM_T28", 10000)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					tutDMCreator.iTutTimer = GET_GAME_TIMER()	
					SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
					tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_TEST
					ENDIF
				ELSE
					PRINT_HELP("FMMC_DM_T53", 10000)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					tutDMCreator.iTutTimer = GET_GAME_TIMER()	
					tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_FINAL_WARN_SPAWNS
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_FINAL_WARN_SPAWNS
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				CLEAR_HELP()
				
				bShowMenu = TRUE
				SET_BIT(sFMMCdata.iBitSet, bResetMenu)
				CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
				sCurrentVarsStruct.bSwitching = FALSE
				sCurrentVarsStruct.stiScreenCentreLOS = NULL
				
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_FINAL_CHECK_SPAWNS
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_FINAL_CHECK_SPAWNS	
			IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)	
			AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)	
			AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)	
			AND sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
				PRINT_HELP("FMMC_DM_T28", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_TEST
				IF SCRIPT_IS_CLOUD_AVAILABLE()
					SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)	
				ENDIF
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				bShowMenu = TRUE
				
				IF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
				AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)
					sTutorialDescription = "FMMC_DM_T5"		
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
				AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)
					sTutorialDescription = "DM_T_TSH4"	
				ELIF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
				AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
					sTutorialDescription = "DM_T_LOB1"				
				ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
					sTutorialDescription = "FMMC_DM_T50"
				ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
					sTutorialDescription = "FMMC_DM_T38"
				ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
					sTutorialDescription = "DM_T_MISH"
				ELSE
					sTutorialDescription = "FMMC_DM_T12"
				ENDIF					
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_TEST
			IF SCRIPT_IS_CLOUD_AVAILABLE()
				IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
					SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)	
				ENDIF
			ELSE
				IF IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
					CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)		
				ENDIF
			ENDIF
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				CLEAR_HELP()	
				bShowMenu = TRUE
				SET_BIT(sFMMCdata.iBitSet, bResetMenu)
				sCurrentVarsStruct.bSwitching = FALSE
				sCurrentVarsStruct.stiScreenCentreLOS = NULL
				
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_TEST_DM
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_TEST_DM	
			IF SCRIPT_IS_CLOUD_AVAILABLE()
				IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
					SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)	
				ENDIF
			ELSE
				IF IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
					CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)		
				ENDIF
			ENDIF
		
			IF IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
			OR NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU				
					sTutorialDescription = "FMMC_DM_T23"
				ELSE
					sTutorialDescription = "FMMC_DM_T38"
				ENDIF
			ELIF NOT IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
				sTutorialDescription = "FMMC_T_OM"
			ENDIF
			
			IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_SPAWNS)	
			AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)	
			AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)		
				IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
					SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
					sFMMCmenu.bPlayerInvincibleTest = TRUE
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
					ENDIF
					PRINT_HELP("FMMC_DM_T36", 10000)
					REFRESH_MENU(sFMMCMenu)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					tutDMCreator.iTutTimer = GET_GAME_TIMER()
					tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_TEST_DM_HELP1
					ENDIF
				ELSE				
					bShowMenu = TRUE			
				ENDIF
			ELSE			
				
				PRINT_HELP("FMMC_DM_T53", 10000)
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_FINAL_WARN_SPAWNS
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_TEST_DM_HELP1
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				PRINT_HELP("FMMC_DM_T41", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_TEST_DM_HELP2
				ENDIF
			ENDIF			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_TEST_DM_HELP2
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				PRINT_HELP("DM_T_PUBT", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_TEST_DM_HELP3
				ENDIF
			ENDIF			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_TEST_DM_HELP3
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_WAITING_TO_FINISH_TEST
				sTutorialDescription = "FMMC_DM_T40"
			ENDIF			
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_WAITING_TO_FINISH_TEST
			
			bShowMenu = TRUE
			
			sTutorialDescription = "DM_T_COMT"			
			
			IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
				IF sCurrentVarsStruct.creationStats.iTimesTestedLoc = 0
					tutDMCreator.iTutTimer = GET_GAME_TIMER()
					CLEAR_HELP()
					PRINTLN("GO TO - FAILED_TEST")
					tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_FAILED_TEST
				ELSE
					tutDMCreator.iTutTimer = GET_GAME_TIMER()
					CLEAR_HELP()	
					tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_GOODBYE1
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_FAILED_TEST
			IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_LOADING
				PRINTLN("DM_CREATOR_TUTORIAL_STAGE_FAILED_TEST")
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				DO_SCREEN_FADE_IN(500)
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_FAILED_TEST_HELP
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_FAILED_TEST_HELP
			IF IS_SCREEN_FADED_IN()
				IF IS_KING_OF_THE_HILL()
					PRINT_HELP("KH_T_FAIL", 10000)
				ELSE
					PRINT_HELP("DM_T_FAIL", 10000)
				ENDIF
				
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_EXPLAIN_TEST
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_GOODBYE1
			IF (GET_GAME_TIMER() - tutDMCreator.iTutTimer) > 1000
			AND IS_SCREEN_FADED_IN()
				PRINT_HELP("FMMC_DM_T24", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_GOODBYE2
				ENDIF
			ELSE
				IF IS_SCREEN_FADED_OUT() OR NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(500)
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_GOODBYE2
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				PRINT_HELP("FMMC_DM_T25", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_GOODBYE3
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_GOODBYE3
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				PRINT_HELP("FMMC_DM_T26", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_GOODBYE4
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_GOODBYE4
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				PRINT_HELP("DM_T_SV1", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_GOODBYE5
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_GOODBYE5
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				PRINT_HELP("DM_T_SV2", 10000)
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_GOODBYE6
				ENDIF
			ENDIF
		BREAK
		
		CASE DM_CREATOR_TUTORIAL_STAGE_GOODBYE6
			IF THE_HELP_MESSAGE_HAS_FINISHED()
				CLEAR_HELP()
				tutDMCreator.iTutTimer = GET_GAME_TIMER()
				tutDMCreator.tutorialStage= DM_CREATOR_TUTORIAL_STAGE_DONE
				CLEANUP_TUTORIAL()
			ENDIF
		BREAK
		
	ENDSWITCH	
	
	if bShowMenu = FALSE
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_CREATOR_SWITCH_CAMERA_BUTTON())
	ELSE	
		if tutDMCreator.tutorialStage < DM_CREATOR_TUTORIAL_STAGE_SWITCH_CAM
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, GET_CREATOR_SWITCH_CAMERA_BUTTON())
		ENDIF
		IF IS_SCREEN_FADED_IN()
		AND NOT IS_STRING_NULL_OR_EMPTY(sTutorialDescription)	
			SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0, -0.0775)		
			PRINT_NOW(sTutorialDescription, 1, 1)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		//Force clean up
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			CLEANUP_TUTORIAL()
			tutDMCreator.tutorialStage = DM_CREATOR_TUTORIAL_STAGE_DONE
		ENDIF
	#ENDIF	

ENDPROC

FUNC BOOL HANDLE_KOTH_TUTORIAL()
	
	//Pre state machine
	tutNGCreator.bShowLeftHandMenu = TRUE
	
	CLEAR_BIT(sFMMCMenu.iOptionsMenuBitSet, OPTION_DM_TEAMS)
	CLEAR_BIT(sFMMCMenu.iOptionsMenuBitSet, OPTION_DM_MIN_PLAYERS)
	CLEAR_BIT(sFMMCMenu.iOptionsMenuBitSet, OPTION_DM_MAX_PLAYERS)
	
	INT iCircleHills = 0
	INT iRectHills = 0
	INT iVehicleHills = 0
	INT iObjectHills = 0
	INT i
	FOR i = 0 TO ciKotH_MAX_HILLS - 1
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sKotHData.sHillData[i].vHill_Center)
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__CIRCLE
			iCircleHills++
		ELIF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__RECT
			iRectHills++
		ELIF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__VEHICLE
			iVehicleHills++
		ELIF g_FMMC_STRUCT.sKotHData.sHillData[i].iHillType = ciKOTH_HILL_TYPE__OBJECT
			iObjectHills++
		ENDIF
	ENDFOR
	
	// State machine
	SWITCH tutNGCreator.eKOTHTutorialStage
		CASE KOTH_CREATOR_TUTORIAL_STAGE_INIT
			CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_INTRO
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_INTRO_0", 8500)
				BREAK
				CASE 1
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_INTRO_1", 8500)
				BREAK
				CASE 2
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_TRIGGER
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_TRIGGER_0", 8500)
				BREAK
				CASE 1
					IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_TRIGGER_0") // Go to Placement
					ENDIF
				BREAK
				CASE 2
					IF sFMMCmenu.sActiveMenu = eFmmc_SELECT_START_END
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_TRIGGER_1") // Go to Trigger
					ENDIF
				BREAK
				CASE 3
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_TRIG_1PC", 11500)
					ELSE
						CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_TRIGGER_1", 9500)
					ENDIF
				BREAK
				CASE 4
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_TRIGGER_2") // Place a trigger
					ENDIF
				BREAK
				CASE 5
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_SPAWN_POINTS
		
			SET_LONG_BIT(sFMMCMenu.iBitActive, SPAWN_MENU_ITEM_DM)
			
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_SPAWN_0", 9500)
				BREAK
				CASE 1
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_SPAWN_1", 7500)
				BREAK
				CASE 2
					CTUT__RETURN_TO_PLACEMENT_STEP(sFMMCmenu, tutNGCreator)
				BREAK
				CASE 3
					IF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_SPAWN_0") // Go to Spawn points	
					ENDIF
				BREAK
				CASE 4
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_SPAWN_PL", 9500)
				BREAK
				CASE 5
					IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints >= 16
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_SPAWN_1") // Place spawn points	
					ENDIF
				BREAK
				CASE 6
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_TEAM_START_POINTS
		
			//CLEAR_LONG_BIT(sFMMCMenu.iBitActive, SPAWN_MENU_ITEM_DM)
			SET_LONG_BIT(sFMMCMenu.iBitActive, TEAM_SPAWN_MENU_ITEM_DM)
					
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_TEAMST_0", 10500)
				BREAK
				CASE 1
					CTUT__RETURN_TO_PLACEMENT_STEP(sFMMCmenu, tutNGCreator)
				BREAK
				CASE 2
					IF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT 
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_TEAMST_0") // Go to start points
					ENDIF
				BREAK
				CASE 3
					IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] >= 4
					AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] >= 4
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_TEAMST_1") // Place start points for both teams
					ENDIF
				BREAK
				CASE 4
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_VEHICLES
		
			SET_LONG_BIT(sFMMCMenu.iBitActive, VEH_MENU_ITEM_DM)
					
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_VEH_0", 7500)
				BREAK
				CASE 1
					CTUT__RETURN_TO_PLACEMENT_STEP(sFMMCmenu, tutNGCreator)
				BREAK
				CASE 2
					IF sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_VEH_0") // Get in Vehicles
					ENDIF
				BREAK
				CASE 3
					IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles >= 5
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						DRAW_GENERIC_BIG_NUMBER(ciKOTHTutorial_VehsToPlace - g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles, "KH_T_VEHC", -1, HUD_COLOUR_WHITE, HUDORDER_TOP, FALSE, NULL_STRING(), HUD_COLOUR_WHITE, FALSE, HUDFLASHING_NONE, 0)
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_VEH_1") // Place a vehicle
					ENDIF
				BREAK
				CASE 4
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_HILLS
		
			SET_LONG_BIT(sFMMCMenu.iBitActive, DON_KOTH_HILLS_DM)
					
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_HILLS_0", 7500)
				BREAK
				CASE 1
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_HILLS_1", 7800)
				BREAK
				CASE 2
					CTUT__RETURN_TO_PLACEMENT_STEP(sFMMCmenu, tutNGCreator)
				BREAK
				CASE 3
					IF sFMMCmenu.sActiveMenu = eFmmc_KotH_HILLS
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_HILLS_1") // Get in Hills
					ENDIF
				BREAK
				CASE 4
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_HILLS_2", 10500)
				BREAK
				CASE 5
					IF iCircleHills > 0
					AND iRectHills > 0
					AND iObjectHills > 0
					AND iVehicleHills > 0
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_HILLS_2") // Place all types of Hill
					ENDIF
				BREAK
				CASE 6
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_WEAPONS
		
			//CLEAR_LONG_BIT(sFMMCMenu.iBitActive, DON_KOTH_HILLS_DM)
			SET_LONG_BIT(sFMMCMenu.iBitActive, WEP_MENU_ITEM_DM)
					
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_WEP_0", 7500)
				BREAK
				CASE 1
					CTUT__RETURN_TO_PLACEMENT_STEP(sFMMCmenu, tutNGCreator)
				BREAK
				CASE 2
					IF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_WEP_0") // Get in Weapons
					ENDIF
				BREAK
				CASE 3
					IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons >= ciKOTHTutorial_WeaponsToPlace
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						DRAW_GENERIC_BIG_NUMBER(ciKOTHTutorial_WeaponsToPlace - g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons, "DM_T_WEPC", -1, HUD_COLOUR_WHITE, HUDORDER_TOP, FALSE, NULL_STRING(), HUD_COLOUR_WHITE, FALSE, HUDFLASHING_NONE, 0)
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_WEP_1") // Place weapons
					ENDIF
				BREAK
				CASE 4
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_LOBBY_CAMERA
		
			//CLEAR_LONG_BIT(sFMMCMenu.iBitActive, WEP_MENU_ITEM_DM)
			SET_LONG_BIT(sFMMCMenu.iBitActive, PANCAM_MENU_ITEM_DM)
					
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_LBBY_0", 9500)
				BREAK
				CASE 1
					CTUT__RETURN_TO_PLACEMENT_STEP(sFMMCmenu, tutNGCreator)
				BREAK
				CASE 2
					IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_LBBY_0") // Go to lobby camera
					ENDIF
				BREAK
				CASE 3
					IF DOES_BLIP_EXIST(bCameraPanBlip)
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_LBBY_1") // Take photo
					ENDIF
				BREAK
				CASE 4
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_TITLE
			
			SWITCH tutNGCreator.iSubstage
				CASE 0
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionName)
						CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
						RETURN FALSE
					ENDIF
			
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_TITL_0", 3500)
				BREAK
				CASE 1
					CTUT__RETURN_TO_PLACEMENT_STEP(sFMMCmenu, tutNGCreator)
				BREAK
				CASE 2
					IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_TITL_0") // Back out
					ENDIF
				BREAK
				CASE 3
					IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_TITL_1") // Go to details
					ENDIF
				BREAK
				CASE 4
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionName)
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_TITL_2") // Write a title
					ENDIF
				BREAK
				CASE 5
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_DESCRIPTION
			SWITCH tutNGCreator.iSubstage
				CASE 0
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionDecription[0])
						CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
						RETURN FALSE
					ENDIF
					
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_DESC_0", 100)
				BREAK
				CASE 1
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionDecription[0])
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_DESC_0") // Write a desc
					ENDIF
				BREAK
				CASE 2
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_PHOTO
			SWITCH tutNGCreator.iSubstage
				CASE 0
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vDetailsPhotoPos)
						CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
						RETURN FALSE
					ENDIF
					
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_PHOT_0", 8500)
				BREAK
				CASE 1
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vDetailsPhotoPos)
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_PHOT_0") // Take a photo
					ENDIF
				BREAK
				CASE 2
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_SAVE
		
			SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
			
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_SAVE_0", 9500)
				BREAK
				CASE 1
					IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_SAVE_0") // Back out
					ENDIF
				BREAK
				CASE 2
					IF IS_BIT_SET(tutNGCreator.iTutorialBS, ciCREATOR_TUTORIAL__CONTENT_SAVED)
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_SAVE_1") // Click "save"
					ENDIF
				BREAK
				CASE 3
					IF IS_SCREEN_FADED_IN()
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 4
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_TEST
			
			SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
			
			SWITCH tutNGCreator.iSubstage
				CASE 0
					IF CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_TEST_0", 6500)
						REFRESH_MENU(sFMMCmenu)
					ENDIF
				BREAK
				CASE 1
					IF IS_FAKE_MULTIPLAYER_MODE_SET()
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_TEST_0") // Click "test"
					ENDIF
				BREAK
				CASE 2
					IF IS_BIT_SET(tutNGCreator.iTutorialBS, ciCREATOR_TUTORIAL__CONTENT_TESTED)
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						IF IS_FAKE_MULTIPLAYER_MODE_SET()
							CTUT__SHOW_OBJECTIVE_TEXT("KHOBJ_TEST_1") // Complete the test
						ELSE
							tutNGCreator.iSubstage = 1
							PRINTLN("[KOTH] Go back to entering test")
						ENDIF
					ENDIF
				BREAK
				CASE 3
					IF IS_SCREEN_FADED_IN()
						CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE KOTH_CREATOR_TUTORIAL_STAGE_OUTRO
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_OUTRO_0", 7500)
				BREAK
				CASE 1
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_OUTRO_1", 9500)
				BREAK
				CASE 2
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "KHTUT_OUTRO_2", 7500)
				BREAK
				CASE 3
					CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
					CLEAR_BIT(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_ALLOW_ALL_HUD)
					
					SET_BIT(sFMMCMenu.iOptionsMenuBitSet, OPTION_DM_TEAMS)
					SET_BIT(sFMMCMenu.iOptionsMenuBitSet, OPTION_DM_MIN_PLAYERS)
					SET_BIT(sFMMCMenu.iOptionsMenuBitSet, OPTION_DM_MAX_PLAYERS)
	
					bShowMenu = TRUE
					RETURN TRUE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	//Post state machine
	CTUT_COMMON_POST_STATE_MACHINE_PROCESSING(tutNGCreator)
	
	IF IS_BIT_SET(tutNGCreator.iTutorialBS, ciCREATOR_TUTORIAL__CONTENT_TESTED)
		tutNGCreator.bShowLeftHandMenu = FALSE
	ENDIF
	
	bShowMenu = tutNGCreator.bShowLeftHandMenu
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 tlDebugText
	tlDebugText = "eKOTHTutorialStage: "
	tlDebugText += GET_KOTH_CREATOR_TUTORIAL_STAGE_AS_STRING(tutNGCreator.eKOTHTutorialStage)
	tlDebugText += " | iSubstage: "
	tlDebugText += tutNGCreator.iSubstage
	DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.05, 0.65, 0.5>>)
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		bShowMenu = TRUE
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_NEW_DEATHMATCH_CREATOR_TUTORIAL()
	
	tutNGCreator.bShowLeftHandMenu = TRUE
	
	sTutorialDescription = ""
	ALLOW_MISSION_CREATOR_WARP(FALSE)
	
	SWITCH tutNGCreator.eDMTutStage
		CASE NDM_CREATOR_TUTORIAL_STAGE_INIT
			
			IF NOT IS_SCREEN_FADING_OUT()	
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					
					sCurrentVarsStruct.bDisableEditingDuringTutorial = FALSE
					
					SET_ALL_DM_MENU_ITEMS_OFF(sFMMCmenu)
       				CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_CREATOR)	
					CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)	
					CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
					CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_RADIO)
					CLEAR_BIT(sFMMCMenu.iOptionsMenuBitSet, DEATHMATCH_DETAILS_CUSTOM_SETTINGS)
					
					
					REFRESH_MENU(sFMMCMenu)
					
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				ENDIF
			ENDIF	
			
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_INTRO
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "FMMC_DM_T0", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
			
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_REQUIRED_DETAILS
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "FMMC_DM_T16", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_T_DET", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 2
					sTutorialDescription = "FMMC_DM_T17"
					IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 3
					IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						sTutorialDescription = "FMMC_DM_T17"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
						IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO)	
							sTutorialDescription = "RC_T_DTP"
						ELSE
							sTutorialDescription = "DM_T_RDT"
						ENDIF
					ELSE
						sTutorialDescription = "FMMC_DM_T18"
					ENDIF
					
					IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
					AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_DESC)
					AND NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO)		
					AND sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
					AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
					AND sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						bShowMenu = TRUE
					ENDIF
					
				BREAK
				CASE 4
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "FMMC_DM_T52", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 5
					SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_CREATOR)
					SET_LONG_BIT(sFMMCMenu.iBitActive, START_MENU_ITEM_DM)
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_TRIGGER
			SWITCH tutNGCreator.iSubstage
				CASE 0
					sTutorialDescription = "DM_T_DETL"
					IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 1
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_T_TRGH", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 2
					sTutorialDescription = "FMMC_DM_T50"
					IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF 
				BREAK
				CASE 3
					IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
						sTutorialDescription = "FMMC_DM_T11"				
					ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						sTutorialDescription = "FMMC_DM_T50"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
						sTutorialDescription = "FMMC_DM_T38"
					ELSE
						sTutorialDescription = "FMMC_DM_T12"
					ENDIF
					
					IF sFMMCmenu.sActiveMenu = eFmmc_SELECT_START_END
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 4
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_T_TRGH1", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 5
					bShowMenu = TRUE 
			
					IF sFMMCmenu.sActiveMenu = eFmmc_SELECT_START_END			
						sTutorialDescription = "FMMC_DM_T2"	
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
						sTutorialDescription = "FMMC_DM_T11"				
					ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						sTutorialDescription = "FMMC_DM_T50"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
						sTutorialDescription = "FMMC_DM_T38"
					ELSE
						sTutorialDescription = "FMMC_DM_T12"
					ENDIF

					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)	
					AND iTriggerCreationStage < CREATION_STAGE_DONE
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
					
				BREAK
				CASE 6
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_T_LOBH", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				
				CASE 7
					SET_LONG_BIT(sFMMCMenu.iBitActive, PANCAM_MENU_ITEM_DM)
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_LOBBY_CAMERA
			SWITCH tutNGCreator.iSubstage
				CASE 0
					IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE			
						sTutorialDescription = "DM_T_LOB1"	
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
						sTutorialDescription = "DM_T_LOB0"				
					ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						sTutorialDescription = "FMMC_DM_T50"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
						sTutorialDescription = "FMMC_DM_T38"
					ELSE
						sTutorialDescription = "FMMC_DM_T12"
					ENDIF
					
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraPanPos)
						SET_LONG_BIT(sFMMCMenu.iBitActive, SPAWN_MENU_ITEM_DM)
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 1
					sTutorialDescription = "FMMC_DM_T12"
					
					IF sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE	
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 2
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_SPAWN_POINTS
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "FMMC_DM_T4", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						sTutorialDescription = "FMMC_DM_T50"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
						sTutorialDescription = "FMMC_DM_T38"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
						sTutorialDescription = "FMMC_DM_T20"
					ELSE
						sTutorialDescription = "FMMC_DM_T12"
					ENDIF
					
					IF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
					
				BREAK
				CASE 2
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_T_SPWH", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 3
					SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(2)
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_T_SPWD_PC", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
					ELSE
						CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_T_SPWD", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
					ENDIF
					
				BREAK
				CASE 4
					SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(2)
					IF sFMMCmenu.sActiveMenu = eFmmc_DM_SPAWN_POINT			
						sTutorialDescription = "FMMC_DM_T5"					
					ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						sTutorialDescription = "FMMC_DM_T50"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
						sTutorialDescription = "FMMC_DM_T38"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
						sTutorialDescription = "FMMC_DM_T20"
					ELSE
						sTutorialDescription = "FMMC_DM_T12"
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints >= (g_FMMC_STRUCT.iNumParticipants + 2) * 2
						SET_LONG_BIT(sFMMCMenu.iBitActive, TEAM_SPAWN_MENU_ITEM_DM)
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
					
				BREAK
				CASE 5
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_TEAM_START
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_TSP", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_TSP2", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 2
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_TSP3", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 3
					IF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT			
						sTutorialDescription = "DM_T_TSH4"					
					ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						sTutorialDescription = "FMMC_DM_T50"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
						sTutorialDescription = "FMMC_DM_T38"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
						sTutorialDescription = "DM_T_TSH3"
					ELSE
						sTutorialDescription = "FMMC_DM_T12"
					ENDIF
					
					IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TEAM_SPAWNS)
						SET_LONG_BIT(sFMMCMenu.iBitActive, WEP_MENU_ITEM_DM)
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
					
				BREAK
				CASE 4
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_PICKUPS
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "FMMC_DM_T10", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					IF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE	
						DRAW_GENERIC_BIG_NUMBER(6 - g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons,"DM_T_WEPC", -1, HUD_COLOUR_WHITE, HUDORDER_TOP, FALSE, NULL_STRING(), HUD_COLOUR_WHITE, FALSE, HUDFLASHING_NONE, 0) //sWepStruct.flashColour
						sTutorialDescription = "FMMC_DM_T14"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
						sTutorialDescription = "FMMC_DM_T13"					
					ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
						sTutorialDescription = "FMMC_DM_T50"
					ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
						sTutorialDescription = "FMMC_DM_T38"
					ELSE
						sTutorialDescription = "FMMC_DM_T12"
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons > 5
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
					
				BREAK
				CASE 2
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_REQUIREMENTS_SUMMARY
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "FMMC_DM_T22", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_TEST_1
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "FMMC_DM_T28", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					IF IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
					OR NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU				
							sTutorialDescription = "FMMC_DM_T23"
						ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_TEST
							sTutorialDescription = "DM_TUT_STTT"
						ELSE
							sTutorialDescription = "FMMC_DM_T38"
						ENDIF
					ELIF NOT IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
						sTutorialDescription = "FMMC_T_OM"
					ENDIF
					
					IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
						sFMMCmenu.bPlayerInvincibleTest = TRUE
						IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
							SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
						ENDIF
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 2
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "FMMC_DM_T36", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 3
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "FMMC_DM_T41", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 4
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_TSTR", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 5
					sTutorialDescription = "DM_T_COMT"
					
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
						IF sCurrentVarsStruct.creationStats.iTimesTestedLoc = 0
							tutNGCreator.iSubstage = 1
						ELSE
							CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
						ENDIF
					ENDIF
				BREAK
				CASE 6
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_BASICS_SUMMARY
			SWITCH tutNGCreator.iSubstage
				CASE 0
					IF IS_SCREEN_FADED_IN()
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						bShowMenu = FALSE
						FMMC_SAFE_FADE_SCREEN_IN_FROM_BLACK(1000)
					ENDIF
				BREAK
				CASE 1
					bShowMenu = FALSE
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "FMMC_DM_T24", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 2
					SET_BIT(sFMMCMenu.iOptionsMenuBitSet, DEATHMATCH_DETAILS_CUSTOM_SETTINGS)
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_CUSTOM_GAME_SETTINGS
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS1")
				BREAK
				CASE 1
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS2")
				BREAK
				CASE 2
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS3")
				BREAK
				CASE 3
					
					IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE				
						sTutorialDescription = "DM_TUT_SCGS"
					ELSE
						sTutorialDescription = "DM_TUT_NDD"
					ENDIF
					
					//Go to the menu
					IF sFMMCmenu.sActiveMenu = eFmmc_DM_CUSTOM_GAME_OPTIONS
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 4
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS4", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 5
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS5", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 6
					sTutorialDescription = "DM_TUT_SCGSD"
					IF sFMMCmenu.sActiveMenu = eFmmc_DM_MODIFIER_SET
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 7
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS6", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 8
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_SCORING
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS_S1", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					sTutorialDescription = "DM_TUT_SCGSS"
					IF sFMMCmenu.sActiveMenu = eFmmc_DM_MODIFIER_SCORING
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 2
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS_S2", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 3
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS_S3", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 4
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS_S4", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 5
					IF sFMMCmenu.sActiveMenu = eFmmc_DM_MODIFIER_SET
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 6
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_INVENTORY
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS_I1", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					sTutorialDescription = "DM_TUT_SCGSI"
					IF sFMMCmenu.sActiveMenu = eFMMC_Inventory_Single
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 2
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS_I2", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 3
					
					IF sFMMCmenu.sActiveMenu != eFMMC_Inventory_Single
					AND sFMMCmenu.sActiveMenu != eFMMC_Inventory_Weapon_List
					AND sFMMCmenu.sActiveMenu != eFMMC_Inventory_Weapons
					AND sFMMCmenu.sActiveMenu != eFMMC_Inventory_Component_List
					AND sFMMCmenu.sActiveMenu != eFMMC_Inventory_Parachutes
						sTutorialDescription = "DM_TUT_SCGSIR"
					ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].sModifierInventory.iInventoryBitset, ciFMMC_INVENTORY_BS_DM_MODSET_USING_INVENTORY)
						sTutorialDescription = "DM_TUT_SCGSIB"
					ELSE
						sTutorialDescription = "DM_TUT_SCGSIS"
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].sModifierInventory.iInventoryBitset, ciFMMC_INVENTORY_BS_DM_MODSET_USING_INVENTORY)
					AND (g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].sModifierInventory.sWeaponStruct[0].wtWeapon != WEAPONTYPE_INVALID
						OR IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].sModifierInventory.iRandomWeaponBitSet, 0))
					AND NOT HAS_NET_TIMER_STARTED(tutNGCreator.stWaitTimer)
						REINIT_NET_TIMER(tutNGCreator.stWaitTimer)
					ENDIF
					
					IF HAS_NET_TIMER_STARTED(tutNGCreator.stWaitTimer)
					AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tutNGCreator.stWaitTimer, 10000)
						RESET_NET_TIMER(tutNGCreator.stWaitTimer)
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 4
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS_I3", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 5
					IF sFMMCmenu.sActiveMenu = eFmmc_DM_MODIFIER_SET
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 6
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_TEST_2
			SWITCH tutNGCreator.iSubstage
				CASE 0
					//Many other features but time to test
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_TST1", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					IF IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
					OR NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU				
							sTutorialDescription = "FMMC_DM_T23"
						ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_TEST
							sTutorialDescription = "DM_TUT_STTT"
						ELSE
							sTutorialDescription = "FMMC_DM_T38"
						ENDIF
					ELIF NOT IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
						sTutorialDescription = "FMMC_T_OM"
					ENDIF
					
					IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
						sFMMCmenu.bPlayerInvincibleTest = TRUE
						IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
							SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
						ENDIF
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 2
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ENDIF
				BREAK
				CASE 3
					IF IS_SCREEN_FADED_IN()
						CTUT__INCREMENT_SUBSTAGE(tutNGCreator)
					ELSE
						FMMC_SAFE_FADE_SCREEN_IN_FROM_BLACK(1000)
						bShowMenu = FALSE
					ENDIF
				BREAK
				CASE 4
					bShowMenu = FALSE
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_MOD_SET_SUM
			SWITCH tutNGCreator.iSubstage
				CASE 0
					bShowMenu = FALSE
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_CGS_SM", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
				BREAK
			ENDSWITCH
		BREAK
		CASE NDM_CREATOR_TUTORIAL_STAGE_OUTRO
			SWITCH tutNGCreator.iSubstage
				CASE 0
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_PUB1", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 1
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_TUT_PUB2", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 2
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "FMMC_DM_T26", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 3
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_T_SV1", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 4
					CTUT__SHOW_TIMED_HELPTEXT(tutNGCreator, "DM_T_SV2", ciDEATHMATCH_CREATOR_TUTORIAL__HELP_TEXT_REDUCTION)
				BREAK
				CASE 5
					bShowMenu = TRUE
					CLEANUP_TUTORIAL()
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	CTUT_COMMON_POST_STATE_MACHINE_PROCESSING(tutNGCreator)
	
	IF IS_BIT_SET(tutNGCreator.iTutorialBS, ciCREATOR_TUTORIAL__CONTENT_TESTED)
		tutNGCreator.bShowLeftHandMenu = FALSE
	ENDIF
	
	bShowMenu = tutNGCreator.bShowLeftHandMenu
	
	IF IS_SCREEN_FADED_IN()
	AND NOT IS_STRING_NULL_OR_EMPTY(sTutorialDescription)	
		SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0, -0.0775)		
		PRINT_NOW(sTutorialDescription, 1, 1)
	ENDIF
	
	VECTOR vPlayerPos
	IF HAS_PLAYER_PED_MAP_WARPED_WITH_FRONT_END(sFMMCdata, vPlayerPos)
		IF IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_IN(500)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 tlDebugText
	tlDebugText = "eDMTutStage: "
	tlDebugText += GET_DM_CREATOR_TUTORIAL_STAGE_AS_STRING(tutNGCreator.eDMTutStage)
	DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.05, 0.65, 0.5>>)
	tlDebugText = "iSubstage: "
	tlDebugText += tutNGCreator.iSubstage
	DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.05, 0.67, 0.5>>)
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		CTUT__INCREMENT_TUTORIAL_STAGE(tutNGCreator)
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		bShowMenu = TRUE
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PREPARE_DM_TO_BE_SAVED_OR_PUBLISHED()

	IF g_FMMC_STRUCT.iVehicleModel = -1	
		g_FMMC_STRUCT.iVehicleModel = 0	
	ENDIF		
	IF g_FMMC_STRUCT.iRank = 0
		g_FMMC_STRUCT.iRank = 1
	ENDIF
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciSavedWithNewWeaponData)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED) 
		CLEAR_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_VOTING_DISABLED) 
	ELSE
		SET_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_VOTING_DISABLED) 
	ENDIF
	CLEAR_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
	IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
		g_FMMC_STRUCT.iMissionSubType = FMMC_DEATHMATCH_TEAM
		g_FMMC_STRUCT.iVehicleModel = 0	
	ELIF g_FMMC_STRUCT.iVehicleDeathmatch = 1
		g_FMMC_STRUCT.iMissionSubType = FMMC_DEATHMATCH_VEHICLE
	ELSE
		g_FMMC_STRUCT.iMissionSubType = FMMC_DEATHMATCH_STANDARD
		g_FMMC_STRUCT.iVehicleModel = 0	
	ENDIF
	
	IF IS_KING_OF_THE_HILL()
		IF g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
			g_FMMC_STRUCT.iMissionSubType = FMMC_KING_OF_THE_HILL_TEAM
			PRINTLN("[KOTH] Saving as FMMC_KING_OF_THE_HILL_TEAM")
		ELSE
			g_FMMC_STRUCT.iMissionSubType = FMMC_KING_OF_THE_HILL
			PRINTLN("[KOTH] Saving as FMMC_KING_OF_THE_HILL")
		ENDIF
	ENDIF
	
	sCurrentVarsStruct.creationStats.bSuccessfulSave = FALSE
	
	IF CONTENT_IS_USING_ARENA()
		INIT_PUBLIC_ARENA_CREATOR(sFMMCmenu)
	ENDIF
	
	SET_BIT(tutNGCreator.iTutorialBS, ciCREATOR_TUTORIAL__CONTENT_SAVED)
	
	PRINTLN("PRE PUBLISH/SAVE DONE. Time to go for it")
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_DM_CREATOR_IPLS_PRE_FRAME()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		IF NOT IS_BIT_SET(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
		OR IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RefreshIPL)
			IF HEIST_ISLAND_LOADING__LOAD_ISLAND_IPLS()
			AND SET_MISSION_ISLAND_CUSTOMISATION()
				SET_BIT(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
				PRINTLN("PROCESS_DM_CREATOR_IPLS_PRE_FRAME - ciIPLBS_IslandLoaded now set")
				CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RefreshIPL)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
			HEIST_ISLAND_LOADING__UNLOAD_ISLAND_IPLS()
			UNLOAD_MISSION_ISLAND_CUSTOMISATION()
			CLEAR_BIT(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
			PRINTLN("PROCESS_DM_CREATOR_IPLS_PRE_FRAME - ciIPLBS_IslandLoaded cleared")
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_DATA_SLOT_SUMMARY_WINDOW(SCALEFORM_INDEX &sfIndex, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING stStatTitle, STRING stStatText, INT iInt1, INT iInt2 = -1)
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfIndex, "SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId)// menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stStatTitle) // stat name
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(stStatText)	
									
			ADD_TEXT_COMPONENT_INTEGER(iInt1)
			
			IF iInt2 != -1
				ADD_TEXT_COMPONENT_INTEGER(iInt2)
			ENDIF
						
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DISPLAY_WAVE_SUMMARY()

	IF IS_LEGACY_DEATHMATCH_CREATOR()
	OR NOT IS_CURRENT_MENU_A_DM_CUSTOM_SETTINGS_MENU(sFMMCmenu)
		RETURN FALSE
	ENDIF
	
	IF sFMMCmenu.iDMModifierIndex = -1
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL MAINTAIN_SUMMARY_PHOTO()
	
	IF IS_STRING_NULL_OR_EMPTY(g_sFMMC_LoadedMission)
	OR IS_VECTOR_ZERO(g_FMMC_STRUCT.vDetailsPhotoPos)
		PRINTLN("[SUMMARY] MAINTAIN_SUMMARY_PHOTO - Skipping for now as there is no photo available || g_FMMC_STRUCT.vDetailsPhotoPos: ", g_FMMC_STRUCT.vDetailsPhotoPos)
		RETURN TRUE
	ENDIF
	
	IF NOT sDownloadPhotoVars.bSucess
		IF DOWNLOAD_PHOTO_FOR_FMMC_LITE(sDownloadPhotoVars, g_sFMMC_LoadedMission, 0, DEFAULT, 2)
			PRINTLN("[SUMMARY] MAINTAIN_SUMMARY_PHOTO - DOWNLOAD_PHOTO_FOR_FMMC_LITE returned TRUE")
			sDownloadPhotoVars.bSucess = TRUE
		ELSE
			PRINTLN("[SUMMARY] MAINTAIN_SUMMARY_PHOTO - DOWNLOAD_PHOTO_FOR_FMMC_LITE returned FALSE")
		ENDIF
	ENDIF
	
	RETURN sDownloadPhotoVars.bSucess
ENDFUNC

FUNC TEXT_LABEL_23 GET_ACTIVE_TEAMS_FOR_SUMMARY_WINDOW()
	
	TEXT_LABEL_23 tl23
	
	IF sFMMCmenu.iDMModifierIndex = -1
		RETURN tl23
	ENDIF
	
	INT iTeamLoop, iTeamCount
	FOR iTeamLoop = 0 TO g_FMMC_STRUCT.iMaxNumberOfTeams
		IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCmenu.iDMModifierIndex].sModifierSetup.iTeamBS, iTeamLoop)
			IF iTeamCount != 0
				tl23 += "/"
			ENDIF
			tl23 += iTeamLoop+1
			iTeamCount++
		ENDIF
	ENDFOR
	
	RETURN tl23
ENDFUNC

PROC SET_DATA_SLOT_SUMMARY_WINDOW_WITH_COMPONENT(SCALEFORM_INDEX &sfIndex, INT iMenuIndex, INT iMenuId, INT iUniqueId, STRING stStatTitle, STRING stStatText, INT iInt1, INT iInt2 = -1)
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfIndex, "SET_DATA_SLOT")
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId)// menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(stStatTitle) // stat name
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(stStatText)	
									
			ADD_TEXT_COMPONENT_INTEGER(iInt1)
			
			IF iInt2 != -1
				ADD_TEXT_COMPONENT_INTEGER(iInt2)
			ENDIF
						
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC PROCESS_SUMMARY_WINDOW()
		
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_SOCIAL_CLUB_ACTIVE()
		CLEAR_BIT(sFMMCmenu.iMiscBitSet, bsShowSummaryWindow)
		EXIT
	ENDIF
	
	PRINTLN("[SUMMARY] PROCESS_SUMMARY_WINDOW - Calling")
	TEXT_LABEL_63 tlMissionName, tlDescription
	
	TEXT_LABEL_31 tlTxd = ""
	TEXT_LABEL_31 tlTxn = ""
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vDetailsPhotoPos)
		tlTxd = g_sFMMC_LoadedMission
		tlTxn = g_sFMMC_LoadedMission
	ENDIF
	
	TEXT_LABEL_23 tl23
	INT iMenuID = ENUM_TO_INT(MENU_UNIQUE_ID_MISSION_CREATOR)
	INT iMenuPos
	INT iLoadInt = 1
	tlMissionName = g_FMMC_STRUCT.tl63MissionName
	
	IF sSummaryData.iCurrentModSet != sFMMCmenu.iDMModifierIndex
	OR sSummaryData.sSetupStruct.eWhoType != g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sSummaryData.iCurrentModSet].sModifierSetup.eWhoType
	OR sSummaryData.sSetupStruct.iTeamBS != g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sSummaryData.iCurrentModSet].sModifierSetup.iTeamBS
	OR sSummaryData.sSetupStruct.eApplyType != g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sSummaryData.iCurrentModSet].sModifierSetup.eApplyType
	OR sSummaryData.sSetupStruct.eConditionType != g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sSummaryData.iCurrentModSet].sModifierSetup.eConditionType
	OR sSummaryData.sSetupStruct.ePositionType != g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sSummaryData.iCurrentModSet].sModifierSetup.ePositionType
	OR sSummaryData.sSetupStruct.eScoreCondition != g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sSummaryData.iCurrentModSet].sModifierSetup.eScoreCondition
	OR sSummaryData.sSetupStruct.iScoreConditionToCheck != g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sSummaryData.iCurrentModSet].sModifierSetup.iScoreConditionToCheck
	OR sSummaryData.sSetupStruct.iScorePercentage != g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sSummaryData.iCurrentModSet].sModifierSetup.iScorePercentage
	OR sSummaryData.sSetupStruct.iTimerConditionLength != g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sSummaryData.iCurrentModSet].sModifierSetup.iTimerConditionLength
	OR sSummaryData.sSetupStruct.iTimerPercentage != g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sSummaryData.iCurrentModSet].sModifierSetup.iTimerPercentage
	OR sSummaryData.bTeamScore != IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].iModifierSetBS, ciDM_ModifierBS_CheckTeamScore)
	OR sSummaryData.bPercentageComparison != IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].iModifierSetBS, ciDM_ModifierBS_UsePercentageComparison)
		sSummaryData.iCurrentModSet = sFMMCmenu.iDMModifierIndex
		sSummaryData.bUpdate = TRUE
	ENDIF
	
	IF sSummaryData.bUpdate
	AND HAS_SCALEFORM_MOVIE_LOADED(sfSummaryMenu)
		SCALEFORM_SET_DATA_SLOT_EMPTY(sfSummaryMenu)
		
		sSummaryData.sSetupStruct = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sSummaryData.iCurrentModSet].sModifierSetup
		sSummaryData.bTeamScore = IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].iModifierSetBS, ciDM_ModifierBS_CheckTeamScore)
		sSummaryData.bPercentageComparison = IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[sFMMCMenu.iDMModifierIndex].iModifierSetBS, ciDM_ModifierBS_UsePercentageComparison)
		
		//Title
		SET_CREATOR_DETAILS_TITLE(sfSummaryMenu, tlDescription, tlMissionName, 0, tlTxd, tlTxn, TRUE, iLoadInt)
		
		//Current Mod Set
		IF sSummaryData.iCurrentModSet = 0
			FMMC_SET_DATA_SLOT_FOR_STAT(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, FALSE, "DM_MDMSS", "DM_DEFAULT")
		ELSE
			SET_DATA_SLOT_SUMMARY_WINDOW(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, "DM_MDMSS", "NUMBER", sSummaryData.iCurrentModSet)
		ENDIF
		iMenuPos++
		
		//Who Type
		tl23 = "DM_WHO_"
		tl23 += ENUM_TO_INT(sSummaryData.sSetupStruct.eWhoType)
		FMMC_SET_DATA_SLOT_FOR_STAT(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, FALSE, "DM_WHO", tl23)
		iMenuPos++
		
		//Teams
		IF sSummaryData.sSetupStruct.eWhoType = DM_WHO__TEAMS
			tl23 = GET_ACTIVE_TEAMS_FOR_SUMMARY_WINDOW()
			FMMC_SET_DATA_SLOT_FOR_STAT(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, FALSE, "FM_TEAMS", tl23, DEFAULT, TRUE)
			iMenuPos++
		ENDIF
		
		tl23 = "DM_POS_"
		tl23 += ENUM_TO_INT(sSummaryData.sSetupStruct.ePositionType)
		FMMC_SET_DATA_SLOT_FOR_STAT(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, FALSE, "DM_POS", tl23)
		iMenuPos++
		
		//Condition Type
		tl23 = "DM_CND_"
		tl23 += ENUM_TO_INT(sSummaryData.sSetupStruct.eConditionType)
		FMMC_SET_DATA_SLOT_FOR_STAT(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, FALSE, "DM_CND", tl23)
		iMenuPos++
		
		tl23 = PICK_STRING(sSummaryData.bPercentageComparison, "DM_CMPT_1", "DM_CMPT_0")
		FMMC_SET_DATA_SLOT_FOR_STAT(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, FALSE, "DM_CMPT", tl23)
		iMenuPos++
		
		IF sSummaryData.sSetupStruct.eConditionType = DM_CONDITION__SCORE
			
			TEXT_LABEL_23 tlScore = PICK_STRING(sSummaryData.bTeamScore, "DM_SC_ST_1", "DM_SC_ST_0")
			tl23 = "DM_SC_SM_"
			tl23 += ENUM_TO_INT(sSummaryData.sSetupStruct.eScoreCondition)
			
			INT iScoreToDisplay = sSummaryData.sSetupStruct.iScoreConditionToCheck
			IF sSummaryData.bPercentageComparison
				iScoreToDisplay = sSummaryData.sSetupStruct.iScorePercentage
				tl23 += "_P"
			ENDIF
			
			SET_DATA_SLOT_SUMMARY_WINDOW_WITH_COMPONENT(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, tlScore, tl23, iScoreToDisplay)
			iMenuPos++
			
		ELIF sSummaryData.sSetupStruct.eConditionType = DM_CONDITION__TIMER
			INT iModMills = sSummaryData.sSetupStruct.iTimerConditionLength % 1000
		    INT iSecs = sSummaryData.sSetupStruct.iTimerConditionLength / 1000
			
			IF NOT sSummaryData.bPercentageComparison
			
			    FLOAT fSecs
			    IF iSecs < 60
			        fSecs = iSecs + (TO_FLOAT(iModMills)/1000)
					SET_DATA_SLOT_SUMMARY_WINDOW_WITH_COMPONENT(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, "DM_CND_2", "FMMC_SEL_SEC", ROUND(fSecs))
					iMenuPos++
			    ELSE
		          INT iMins = iSecs / 60
		          iSecs = iSecs - (iMins * 60)
		          fSecs = iSecs + (TO_FLOAT(iModMills)/1000)
				  SET_DATA_SLOT_SUMMARY_WINDOW_WITH_COMPONENT(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, "DM_CND_2", "FMMC_SEL_SAM", iMins, ROUND(fSecs))
				  iMenuPos++
			    ENDIF
				
			ELSE
			
				SET_DATA_SLOT_SUMMARY_WINDOW_WITH_COMPONENT(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, "DM_CND_2", "FMMC_PERCENT", sSummaryData.sSetupStruct.iTimerPercentage)
				iMenuPos++
				
			ENDIF
		ENDIF
		
		//Apply Type
		tl23 = "DM_WHN_"
		tl23 += ENUM_TO_INT(sSummaryData.sSetupStruct.eApplyType)
		FMMC_SET_DATA_SLOT_FOR_STAT(sfSummaryMenu, iMenuPos, iMenuID, iMenuPos, FALSE, "DM_WHN", tl23)
		iMenuPos++
		
		IF BEGIN_SCALEFORM_MOVIE_METHOD(sfSummaryMenu, "DISPLAY_VIEW")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			END_SCALEFORM_MOVIE_METHOD()
			PRINTLN("PROCESS_SUMMARY_WINDOW - DISPLAY_VIEW")
		ENDIF
		
		SET_BIT(sFMMCmenu.iMiscBitSet, bsShowSummaryWindow)
		
		sSummaryData.bUpdate = FALSE
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED (sfSummaryMenu)
		PRINTLN("[SUMMARY] PROCESS_SUMMARY_WINDOW - Loaded")
		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_TOP)
		SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
		DRAW_SCALEFORM_MOVIE (sfSummaryMenu, 0.165, 0.350, 0.225, 0.59722, 255, 255, 255, 255)
		RESET_SCRIPT_GFX_ALIGN()
	ELSE
		sfSummaryMenu = REQUEST_SCALEFORM_MOVIE("MP_MISSION_DETAILS_CARD")
		PRINTLN("[SUMMARY] PROCESS_SUMMARY_WINDOW - Not loaded yet")
	ENDIF
ENDPROC

PROC PROCESS_OPTION_MARKERS()
	
	SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_PICKUP_BASE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_BLIP_RANGE
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_BLIP_HEIGHT_RANGE
				FLOAT fCylinderHeight
				fCylinderHeight = PICK_FLOAT(sFMMCMenu.sEntityBlipStruct.fBlipHeightDifference> 0.0, sFMMCMenu.sEntityBlipStruct.fBlipHeightDifference, 1.0)
				DRAW_MARKER(MARKER_CYLINDER, sCurrentVarsStruct.vCoronaPos, <<0,0,0>>, <<0,0,0>>, <<sFMMCMenu.sEntityBlipStruct.fBlipRange, sFMMCMenu.sEntityBlipStruct.fBlipRange, fCylinderHeight>>, 100, 250, 100)
				DRAW_MARKER(MARKER_CYLINDER, sCurrentVarsStruct.vCoronaPos, <<0,0,0>>, <<0,0,0>>, <<sFMMCMenu.sEntityBlipStruct.fBlipRange, sFMMCMenu.sEntityBlipStruct.fBlipRange, -fCylinderHeight>>, 100, 250, 100)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_AUTOMATED_BLOCK_ARMOUR_PURCHASE()
	INT iLoop
	
	IF IS_THIS_LEGACY_DM_CONTENT()
		EXIT
	ENDIF
	
	IF IS_ROCKSTAR_DEV()
		EXIT
	ENDIF
	
	FOR iLoop = 0 TO g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings-1
		IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierInventory.eArmour != eFMMC_ARMOUR_RETAIN
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciREMOVE_ARMOUR_PURCHASE_OPTION)
				PRINTLN("PROCESS_AUTOMATED_BLOCK_ARMOUR_PURCHASE - Setting ciREMOVE_ARMOUR_PURCHASE_OPTION")
			ENDIF
			#ENDIF
			SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciREMOVE_ARMOUR_PURCHASE_OPTION)
			EXIT
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciREMOVE_ARMOUR_PURCHASE_OPTION)
		PRINTLN("PROCESS_AUTOMATED_BLOCK_ARMOUR_PURCHASE - Clearing ciREMOVE_ARMOUR_PURCHASE_OPTION")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciREMOVE_ARMOUR_PURCHASE_OPTION)
ENDPROC

PROC PROCESS_AUTOMATED_SHOW_FORCED_WEAPON()
	
	IF IS_THIS_LEGACY_DM_CONTENT()
		EXIT
	ENDIF
	
	INT iLoop
	
	FOR iLoop = 0 TO g_FMMC_STRUCT.sDMCustomSettings.iNumberOfCustomSettings-1
		IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iLoop].sModifierInventory.iInventoryBitset, ciFMMC_INVENTORY_BS_DM_MODSET_USING_INVENTORY)
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Forced_Weapons)
				PRINTLN("PROCESS_AUTOMATED_SHOW_FORCED_WEAPON - Setting ciREMOVE_ARMOUR_PURCHASE_OPTION")
			ENDIF
			#ENDIF
			SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Forced_Weapons)
			EXIT
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Forced_Weapons)
		PRINTLN("PROCESS_AUTOMATED_SHOW_FORCED_WEAPON - Clearing ciREMOVE_ARMOUR_PURCHASE_OPTION")
	ENDIF
	#ENDIF
	CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Hide_Corona_Option_Forced_Weapons)
ENDPROC

PROC PROCESS_AUTOMATED_OPTIONS()
	
	PROCESS_AUTOMATED_BLOCK_ARMOUR_PURCHASE()
	
	PROCESS_AUTOMATED_SHOW_FORCED_WEAPON()
	
ENDPROC

SCRIPT

	// JA: Adjust params needed to load in requested DM
	IF IS_PAUSE_MENU_REQUESTING_TO_EDIT_A_MISSION()
		PRINTLN("[JA@PAUSEMENU] Set up which file to load into creator", GET_PAUSE_MENU_MISSION_FILE_TO_LOAD())

		sFMMCendStage.iLoadDeleteStage  = ciLOAD_DELETE_STAGE_LOAD
		sFMMCendStage.iMenuReturn		= GET_PAUSE_MENU_MISSION_FILE_TO_LOAD()
	ENDIF
	CLEAN_UP_PAUSE_MENU_MISSION_CREATOR_DATA()

	PROCESS_PRE_GAME()	
	
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP))
		SCRIPT_CLEANUP(TRUE)
	ENDIF	
	
	IF IS_ARENA_CREATOR()
		INIT_PUBLIC_ARENA_CREATOR(sFMMCmenu)
		PRINTLN("[ARENA] SET_UP_INTERIOR(INTERIOR_V_ARENA)")
		SET_UP_INTERIOR(INTERIOR_V_ARENA)
	ENDIF
	
	IF IS_KING_OF_THE_HILL()
		INIT_KING_OF_THE_HILL_CREATOR()
	ENDIF
	
	// Used for the model memory budget. Must be initiated at the start.
	INIT_CREATOR_BUDGET()
	
	// Set up the timers to track the time spent creating a mission
	sCurrentVarsStruct.creationStats.iStartTimeMS = GET_GAME_TIMER()
	sCurrentVarsStruct.creationStats.iCurrentTimeMS = GET_GAME_TIMER()
	sFMMCmenu.bAllowMouseInput = TRUE
	sFMMCmenu.bAllowMouseSelection = TRUE
	
	// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
		SET_UP_FMMC_SKIP_NAMES(sFMMCendStage.SkipMenu)
	#ENDIF
	 
	FMMCSpawnPointStruct sTempSpawnPointArray[FMMC_MAX_SPAWNPOINTS]
	
	#IF IS_DEBUG_BUILD 
	INITIALIZE_ADDITIONAL_DEBUG_AND_WIDGETS()
	#ENDIF
	
	// Main loop.
	WHILE TRUE	
		#IF IS_DEBUG_BUILD iPrintNum = 0 #ENDIF
		PROCESS_SINGLEPLAYER_MODEL_ON_CREATOR(bIsCreatorModelSetSPTC,bIsMaleSPTC)
		// THIS HAS TO BE CALLED BEFORE THE WAIT(0)
		FMMC_FAKE_LEFT_AND_RIGHT_INPUTS_FOR_MENUS(sFMMCmenu)
		
		PROCESS_DM_CREATOR_IPLS_PRE_FRAME()
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		WAIT(0)
				
		IF SHOULD_DISPLAY_WAVE_SUMMARY()
			IF MAINTAIN_SUMMARY_PHOTO()
				PROCESS_SUMMARY_WINDOW()
			ENDIF
		ELSE
			IF IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsShowSummaryWindow)
			
				RESET_STRUCT_DL_PHOTO_VARS_LITE(sDownLoadPhotoVars)
				
				CLEAR_BIT(sFMMCmenu.iMiscBitSet, bsShowSummaryWindow)
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfSummaryMenu)
			ENDIF
		ENDIF		
		
		
		//Prevent recording whilst in the creator
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
		PROCESS_CREATOR_EVENTS(sCurrentVarsStruct, sFMMCmenu)
		
		PROCESS_ON_DEMAND_MENU_REFRESHING(sFMMCMenu)
	
		RESET_UP_BUTTON_HELP()
		//Check to see if we need to clear the tested bit sets
		CHECK_NEEDS_RETESTED()
		FMMC_HANDLE_KEYBOARD_AND_MOUSE(sFMMCmenu, NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive) OR (bShowMenu), bShowMenuHighlight AND bShowMenu)
		
		// Switch help from mouse and keyboard to joypad if we change controls
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) != bLastUseMouseKeyboard
			bLastUseMouseKeyboard = IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		ENDIF
		
		KEEP_CAMERA_WITHIN_WORLD_BOUNDS(sCamData)
				
		FMMC_UPDATE_MENU_EXCLUSIVITY_IN_TEST_MODE(sFMMCdata, (IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive) AND bShowMenuHighlight AND bShowMenu))
		bShowMenuHighlight = FALSE
		
		DO_BLIMP_SIGNS(sBlimpSign, sCurrentVarsStruct.iMenuState = STAGE_DEAL_WITH_FINISHING)
		
		PROCESS_PUBLIC_ARENA_CREATOR(sFMMCmenu, sCamData, sCurrentVarsStruct, g_ArenaInterior)
		
		HANDLE_ARENA_INTERIOR_ENTITY_CHANGE(sFMMCMenu, sCamData, vArenaCamCoords, g_ArenaInterior, sPropStruct)
		PROCESS_CREATOR_ARENA(sFMMCdata, sFMMCmenu, g_ArenaInterior, interiorArena_VIPLounge, sCamData, sFMMCMenu.bRefreshArena, IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive))
		
		PROCESS_OPTION_MARKERS()
		
		IF CONTENT_IS_USING_ARENA()
			IF sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_SCREEN_FADED_OUT()
						PUT_PLAYER_IN_VEH()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_SCREEN_FADED_IN()
		AND NOT IS_SCREEN_FADING_IN()
		AND NOT IS_SCREEN_FADING_OUT()
			
		ELSE
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO, FALSE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO, FALSE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO_TRACK, FALSE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO_TRACK, FALSE)
		
		IF sFMMCData.bArenaCamStuck
			REFRESH_ARENA(sFMMCMenu, FALSE, TRUE)		
		ENDIF
		
		IF sFMMCData.bClearPlacedStuffForMajorArenaChange
			FMMC_REMOVE_ALL_SPAWNS(sPedStruct)
			REMOVE_TEAM_START_POINTS(sTeamSpawnStruct[0], 0)
			REMOVE_TEAM_START_POINTS(sTeamSpawnStruct[1], 1)
			REMOVE_TEAM_START_POINTS(sTeamSpawnStruct[2], 2)
			REMOVE_TEAM_START_POINTS(sTeamSpawnStruct[3], 3)
			
			FMMC_REMOVE_ALL_PROPS(sPropStruct, TRUE)
			FMMC_REMOVE_ALL_DYNOPROPS(sDynoPropStruct, TRUE, sObjStruct)
			
			IF IS_KING_OF_THE_HILL()
				FMMC_REMOVE_ALL_KOTH_HILLS()
			ENDIF
			
			CLEAR_BIT(g_FMMC_STRUCT.iPhoto, FMMC_PHOTO_TAKEN)
			g_FMMC_STRUCT.vDetailsPhotoPos = <<0, 0, 0>>
			
			PRINTLN("[TMS][ARENACREATOR][ARENA] Clearing certain placed things due to bClearPlacedStuffForMajorArenaChange")
			sFMMCData.bClearPlacedStuffForMajorArenaChange = FALSE
		ENDIF
		
		IF IS_KING_OF_THE_HILL()
			IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
				PROCESS_KING_OF_THE_HILL_CREATOR()
				PROCESS_SPAWN_POINT_VEHICLE_DISPLAY()
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
				INT iTMS = 0
				FOR iTMS = 0 TO FMMC_MAX_SPAWNPOINTS - 1
					FLOAT fHeight = 0.2
					fHeight += TO_FLOAT(iTMS) * 0.03
					
					IF DOES_ENTITY_EXIST(testPeds[iTMS].viVehicleDM)
						TEXT_LABEL_63 tlEntityDebug = "testPeds["
						tlEntityDebug += iTMS
						tlEntityDebug += "].viVehicleDM"
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(testPeds[iTMS].viVehicleDM, tlEntityDebug, 0.1, 255, 255, 255)
					ELSE
						TEXT_LABEL_63 tlDebugText = "testPeds["
						tlDebugText += iTMS
						tlDebugText += "].viVehicleDM NOPE!!"
						DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.5, fHeight, 0.5>>, 255, 255, 255, 100)
					ENDIF
				ENDFOR
			ENDIF
			#ENDIF
		ENDIF
		
		//Deathmatch bounds
		IF NOT CONTENT_IS_USING_ARENA()
			DO_KOTH_BOUNDS_CREATION()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
			#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_START_OF_FRAME()
			#ENDIF
			
			
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63DMHelptext)
			
				INT iHTSlot = GET_DM_HELPTEXT_SLOT()
				
				IF sFMMCMenu.iDMHT_TeamIndexes[GET_CREATOR_MENU_SELECTION(sFMMCMenu)] = -1
					g_FMMC_STRUCT.tl63DMHelptextSaved[iHTSlot] = g_FMMC_STRUCT.tl63DMHelptext
					PRINTLN("[DM HELPTEXT] Setting g_FMMC_STRUCT.tl63DMHelptextSaved[", iHTSlot, "] to ", g_FMMC_STRUCT.tl63DMHelptextSaved[iHTSlot])
				ELSE
					g_FMMC_STRUCT.tl63DMHelptextTeams[iHTSlot][sFMMCMenu.iDMHT_TeamIndexes[GET_CREATOR_MENU_SELECTION(sFMMCMenu)]] = g_FMMC_STRUCT.tl63DMHelptext
					PRINTLN("[DM HELPTEXT] Setting g_FMMC_STRUCT.tl63DMHelptextTeams[", iHTSlot, "][", sFMMCMenu.iDMHT_TeamIndexes[GET_CREATOR_MENU_SELECTION(sFMMCMenu)], " to ", g_FMMC_STRUCT.tl63DMHelptextTeams[iHTSlot][sFMMCMenu.iDMHT_TeamIndexes[GET_CREATOR_MENU_SELECTION(sFMMCMenu)]])
				ENDIF
				
				g_FMMC_STRUCT.tl63DMHelptext = ""
				REFRESH_MENU(sFMMCMenu)
			ENDIF
			
			PROCESS_FORCED_PUBLIC_CREATOR_FOR_DEV(sFMMCmenu, bForcedPublicCreator)
			
		#ENDIF
		
		
		sCurrentVarsStruct.bDiscVisible = FALSE
		sCurrentVarsStruct.bCanCreateADecalThisFrame = TRUE
		
		BOOL bHideRadar = FALSE 
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
			sCurrentVarsStruct.creationStats.iCreatingTimeMS += GET_GAME_TIMER() - sCurrentVarsStruct.creationStats.iCurrentTimeMS
			
			FMMC_MAINTAIN_TIME_AND_WEATHER(g_FMMC_STRUCT.iTimeOfDay)
			FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME()	
			IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)	
				DISABLE_ADDITIONAL_CONTROLS()
			ENDIF
			FMMC_HIDE_HUD_ELEMENTS(sFMMCdata, sFMMCMenu, bHideRadar)
			MAINTAIN_CAMERA_ROTATION_LIMITS(sCamData, sFMMCmenu.sActiveMenu, sCurrentVarsStruct, bInitialIntroCamSetup, bInitialIntroCamWarp)
		
			DEAL_WITH_PHOTO_PREVIEW(PPS, sFMMCendStage.sTakePhotoVars, sCurrentVarsStruct.iMenuState, sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE AND iIntroCamPlacementStage = 2 AND NOT IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsHidePreviewPhoto),sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
			DEAL_WITH_THE_PLACING_INTRO_CAM(SF_Movie_Gallery_Shutter_Index, sFMMCmenu.sActiveMenu, iIntroCamPlacementStage, bInitialIntroCamSetup, bShowMenu, sCurrentVarsStruct.iMenuState, sCurrentVarsStruct.iEntityCreationStatus)
		
		ENDIF
		sCurrentVarsStruct.creationStats.iCurrentTimeMS = GET_GAME_TIMER()
		
		//Add all the rockstar blips to the map
		DISPLAY_ALL_CURRENT_ROCKSTAR_CREATED(sFMMCendStage.sRocStarCreatedVars, sGetUGC_content)
						
		// Tracking time in creator mode
		IF SCRIPT_IS_CLOUD_AVAILABLE()
			TRACK_TIME_IN_CREATOR_MODE()
		ENDIF

		// James A: Pause menu is launching a new mission / mode
		IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
			IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_FINISHING
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_FINISHING
			ENDIF
		ENDIF
		
		// Brenda: MainTransition is launching a new mission / mode
		IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()
			IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_FINISHING
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_FINISHING
			ENDIF
		ENDIF
				
		//setup doors
		CREATOR_DOORS_SETUP(iDoorSlowLoop,iDoorSetupStage,FMMC_TYPE_DEATHMATCH)
		
		PROCESS_AUTOMATED_OPTIONS()
		
		// -----------------------------------
		// Process server game logic		
		INT i, iSpawnPointCount
		SWITCH sCurrentVarsStruct.iEntityCreationStatus
			CASE STAGE_CHECK_TO_LOAD_CREATION
				
				// g_bFMMC_LoadFromMpSkyMenu is set from creator.sc depending on wether we want to load a saved creation.
				IF g_bFMMC_LoadFromMpSkyMenu = TRUE
					IF LOAD_A_MISSION_INTO_THE_CREATOR(sGetUGC_content, sFMMCendStage, g_sFMMC_LoadedMission)
						sFMMCmenu.iForcedWeapon = GET_CREATOR_WEAPON_INDEX_FROM_WEAPON_TYPE(GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet))
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_LOADING	
						IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
							sCamData.camPos = g_FMMC_STRUCT.vStartPos + <<0,-0.5, 20>>
						ENDIF
						
						//Hack to assign the correct number of Spawn Points when loading a DM (1831263)
						iSpawnPointCount = 0
						FOR i = 0 TO FMMC_MAX_SPAWNPOINTS - 1
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos)
								sTempSpawnPointArray[iSpawnPointCount] = g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i]
								iSpawnPointCount++
							ENDIF						
						ENDFOR
						g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints = iSpawnPointCount
						FOR i = 0 TO FMMC_MAX_SPAWNPOINTS - 1
							g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i] = sTempSpawnPointArray[i]
						ENDFOR
						
						IF IS_KING_OF_THE_HILL()
							INIT_KING_OF_THE_HILL_CREATOR()
						ENDIF
						
						#IF IS_DEBUG_BUILD
						IF g_bFMMC_EnteredLoadFromPack
							g_FMMC_STRUCT.bMissionIsPublished = FALSE
							PRINTLN("PROCESS_POST_LOAD_DATA_SETUP - Clearing g_FMMC_STRUCT.bMissionIsPublished as we have accessed content through 'Load from Pack' menu")
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_LOAD
				ENDIF
				
			BREAK
			
			CASE STAGE_ENTITY_PLACEMENT_LOAD
				IF HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
					IF LOAD_MENU_ASSETS()
						sRandStruct.iWeaponLevel = 2
						sRandStruct.iPedLevel = 2
						sRandStruct.iVehicleLevel = 2
						sRandStruct.fRadius = 50
						sFMMCmenu.sRandStruct.fRadius = sRandStruct.fRadius
						
						g_FMMC_STRUCT.iMissionTypeBitSet = 0	
						
						MENU_INITIALISATION()
						PRINTLN("LOADED g_FMMC_STRUCT.iMissionType = ", g_FMMC_STRUCT.iMissionType)
						
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_CG_TO_NG_WARNING
					ENDIF
				ENDIF
			BREAK
			CASE STAGE_CG_TO_NG_WARNING
				IF g_FMMC_STRUCT.bIsUGCjobNG
				OR IS_STRING_EMPTY(g_FMMC_STRUCT.tl63MissionName)			
				OR DRAW_CG_TO_NG_WARNING()
					sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
				ENDIF
			BREAK
			CASE STAGE_ENTITY_PLACEMENT_SETUP
				//Procs that need to run all the time
				
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
					IF bHideRadar
						SET_BIGMAP_ACTIVE(FALSE, FALSE)
					ELSE
						SET_BIGMAP_ACTIVE(TRUE, FALSE)
					ENDIF
				ENDIF
				
				//Deal with the camera.
				IF g_TurnOnCreatorHud
					CONTROL_CAMERA_AND_CORONA()	
					HANDLE_MOUSE_ENTITY_SELECTION(IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive))
				ENDIF
				
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				OR IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY) //TURN OFF SPECIAL ABILITY
				ENDIF
				
				IF NOT HAS_DM_CREATOR_TUTORIAL_COMPLETED(tutDMCreator)
					sFMMCMenu.bTutorialRunning = TRUE
					IF IS_KING_OF_THE_HILL()
						IF HANDLE_KOTH_TUTORIAL()
							tutDMCreator.bDisabled = TRUE
						ENDIF
					ELIF NOT IS_THIS_LEGACY_DM_CONTENT()
						IF PROCESS_NEW_DEATHMATCH_CREATOR_TUTORIAL()
							tutDMCreator.bDisabled = TRUE
						ENDIF
					ELSE
						
						DO_DM_CREATOR_TUTORIAL()	
						IF bShowMenu = FALSE
							IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
								HIDE_HUD_AND_RADAR_THIS_FRAME()
							ENDIF
							sCurrentVarsStruct.bDisableEditingDuringTutorial = TRUE
						ELSE
							sCurrentVarsStruct.bDisableEditingDuringTutorial = FALSE
						ENDIF
					ENDIF
				ELSE
					sFMMCMenu.bTutorialRunning = FALSE
				ENDIF
				
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive) AND NOT IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
					#IF IS_DEBUG_BUILD												
					// [LM][CopyAndPaste] - Needs to be above the MAINTAIN placed entity kind of functions.
					PROCESS_COPY_PASTE_FRONTEND_LOGIC(sCurrentVarsStruct)
					sFMMCMenu.iCopyFromEntity = -1
					sFMMCMenu.iCopyFromEntityTeam = -1
					#ENDIF
					
					IF IS_DEATHMATCH_SET_BY_BOUNDS()
						DISPLAY_DEATHMATCH_BOUNDS()
					ENDIF
					UPDATE_SWAP_CAM_STATUS(sCurrentVarsStruct, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround))
					
					RESET_MAINTAIN_PLACED_ENTITY_VARIABLES(iLocalBitSet, sCurrentVarsStruct)						
					MAINTAIN_PLACED_TRIGGER_LOCATION(g_FMMC_STRUCT.vStartPos, sStartEndBlips.biStart, sStartEndBlips.ciStartType, g_FMMC_STRUCT.vCameraPanPos, bCameraPanBlip, bPhotoBlip)
					
					INT j
					FOR j = 0 TO FMMC_MAX_TEAMS - 1
						MAINTAIN_PLACED_TEAM_SPAWNPOINTS(sTeamSpawnStruct[j], j, sInvisibleObjects, sHCS, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, iLocalBitSet, sFMMCendStage, sVehStruct)
					ENDFOR
					
					#if IS_DEBUG_BUILD
						IF FMMC_IS_ROCKSTAR_DEV()
							MAINTAIN_PLACED_COVER(sCoverStruct, sCurrentVarsStruct, iLocalBitSet, sFMMCmenu)
						ENDIF
					#ENDIF
					
					//Main tain the enemies
					MAINTAIN_PLACED_ENTITIES(sFMMCmenu, sHCS, sCurrentVarsStruct, sPedStruct, sVehStruct, sWepStruct, sObjStruct, sLocStruct, sTrainCreationStruct, sPropStruct, sDynoPropStruct, sInvisibleObjects, sFMMCdata, g_CreatorsSelDetails, sFMMCendStage, sCamData, iLocalBitSet)
					IF g_sMenuData.bHelpCreated = FALSE
					AND sCurrentVarsStruct.iMenuState != MENU_STATE_TITLE
					ENDIF
					
					MAINTAIN_WORLD_PROPS(sFMMCmenu, sCurrentVarsStruct, iLocalBitSet)
				ENDIF
				
				//Check the test state
				SWITCH_TEST_STATE()
				
				DEAL_WITH_SETTING_HEIGHT_ON_ENTITY(sFMMCmenu, sCamData, sCurrentVarsStruct, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), CAN_HEIGHT_BE_SET_FOR_ENTITY(sFMMCmenu, sFMMCmenu.iEntityCreation, FALSE))
				
				REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
		
				SWITCH sCurrentVarsStruct.iMenuState
					CASE MENU_STATE_DEFAULT	
#IF IS_DEBUG_BUILD
						IF sFMMCmenu.sActiveMenu != eFmmc_TOP_MENU
						AND HAS_CREATOR_SEARCH_BEEN_TRIGGERED()
							sCurrentVarsStruct.iMenuState = MENU_STATE_SEARCH
						ELSE // Else rest of function
#ENDIF							
					
						SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
																		
						if sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
							SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
							sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
							sCurrentVarsStruct.sCoronaShapeTest.bDoStartShapeTest = FALSE
							sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL	
						ENDIF
						
						IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
							
							CHECK_FOR_END_CONDITIONS_MET()
							
							IF SHOULD_MENU_BE_DRAWN(sFMMCData, sFMMCMenu, sCurrentVarsStruct, menuScrollController, 1 + g_FMMC_STRUCT.iVehicleDeathmatch, sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU, bShowMenu)									
							AND NOT FMMC_PROCESS_ENTITY_LIST_MENU(sFMMCMenu, sFMMCData, sCurrentVarsStruct)
								
								bShowMenuHighlight = TRUE
								IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints >= FMMC_MAX_SPAWNPOINTS
								AND g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_STANDARD
									sRandStruct.bPeds = TRUE
								ELIF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints >= FMMC_MAX_TEAMSPAWNPOINTS * 4
								AND g_FMMC_STRUCT.iTeamDeathmatch = FMMC_DEATHMATCH_TEAM
									sRandStruct.bPeds = TRUE
								ELSE
									sRandStruct.bPeds = FALSE
								ENDIF
								IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons >= FMMC_MAX_WEAPONS
									sRandStruct.bWeps = TRUE
								ELSE
									sRandStruct.bWeps = FALSE
								ENDIF
								IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles >= FMMC_MAX_VEHICLES
									sRandStruct.bVehs = TRUE
								ELSE
									sRandStruct.bVehs = FALSE 
								ENDIF
								
								IF MAINTAIN_FMMC_RADIO(sFMMCmenu, menuScrollController)
								ENDIF
								
								FMMC_DO_MENU_ACTIONS(	RETURN_CIRCLE_MENU_SELECTIONS(),
														sCurrentVarsStruct, sFMMCmenu, 
														sFMMCData, sFMMCendStage, sHCS)
														
								DEAL_WITH_RANDOMIZER_HUD()
								
								IF CHECK_FOR_DLC_LOCKED_VEHICLE(sFMMCMenu)
								OR CHECK_FOR_DLC_LOCKED_WEAPONS(sWepStruct, sFMMCmenu.sActiveMenu)
									IF NOT IS_BIT_SET(iLocalBitset, biDLCLocked)
										SET_BIT(iLocalBitset, biDLCLocked)
										PRINTLN("BIT SET!")
										sCurrentVarsStruct.bResetUpHelp = TRUE
									ENDIF
								ELSE
									IF IS_BIT_SET(iLocalBitset, biDLCLocked)
										PRINTLN("BIT CLEARED!")
										CLEAR_BIT(iLocalBitset, biDLCLocked)
										sCurrentVarsStruct.bResetUpHelp = TRUE
									ENDIF
								ENDIF
								
								SWITCH sFMMCmenu.iEntityCreation
									CASE CREATION_TYPE_NONE
										sCurrentVarsStruct.iHoverEntityType  = -1
										if sFMMCmenu.sActiveMenu != eFmmc_TOP_MENU
										AND sFMMCmenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
											UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)
										ELSE
											UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
										ENDIF
									BREAK
									CASE CREATION_TYPE_TRIGGER 									
										IF DO_TRIGGER_CREATION(sFMMCmenu, sCurrentVarsStruct, sFMMCdata, sStartEndBlips.biStart, sHCS.hcStartCoronaColour,sStartEndBlips.ciStartType, sFMMCendStage, iTriggerCreationStage, 3)
											IF HAS_DM_CREATOR_TUTORIAL_COMPLETED(tutDMCreator)
												SET_ALL_MENU_ITEMS_ACTIVE()
											ENDIF
										ENDIF
									BREAK
									CASE CREATION_TYPE_PAN_CAM 												
										BOOL bResetCamMenu
										bResetCamMenu = FALSE
										MAINTAIN_PLACING_INTRO_CAMERAS(sFMMCMenu, bCameraTriggerBlip, bResetCamMenu, bInitialIntroCamSetup)
										UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
										IF bResetCamMenu
											REFRESH_MENU(sFMMCMenu)
											sCurrentVarsStruct.bResetUpHelp = TRUE
										ENDIF
									BREAK	
									CASE CREATION_TYPE_SPAWN_LOCATION 									
										DO_SPAWN_POINT_CREATION(sPedStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, FMMC_MAX_SPAWNPOINTS, g_FMMC_STRUCT.iVehicleDeathmatch = 1, IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona))
									BREAK	
									CASE CREATION_TYPE_TEAM_SPAWN_LOCATION 	
										DO_TEAM_SPAWN_POINT_CREATION(sTeamSpawnStruct[sFMMCMenu.iSelectedTeam], sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage,sVehStruct, iMaxSpawnsPerTeam, IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona))//, sTeamSpawnStruct)
									BREAK
									CASE CREATION_TYPE_OBJECTS
										DO_CAPTURE_OBJECT_CREATION(sObjStruct, sCurrentVarsStruct, sHCS, sFMMCMenu, sFMMCdata, sFMMCendStage, IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona), ciKotH_MAX_HILLS)												
									BREAK
									CASE CREATION_TYPE_VEHICLES
										#IF IS_DEBUG_BUILD
										IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
										AND IS_VEHICLE_DRIVEABLE(sVehStruct.viCoronaVeh)
											vVehicleRotationDebug = GET_ENTITY_ROTATION(sVehStruct.viCoronaVeh)
										ENDIF
										#ENDIF
										DO_VEHICLE_CREATION(sVehStruct, sPedStruct, sObjStruct, sTrainCreationStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, FMMC_MAX_VEHICLES, iLocalBitSet)
									BREAK
									CASE CREATION_TYPE_WEAPONS 
										DO_WEAPON_CREATION(sWepStruct, sObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, FMMC_MAX_WEAPONS, g_FMMC_STRUCT.iVehicleDeathmatch = 0, iLocalBitSet)
									BREAK
									CASE CREATION_TYPE_FMMC_ZONE
										DO_ZONE_CREATION(sCurrentVarsStruct, sFMMCmenu, sFMMCendStage)
										DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfZones,  GET_MAX_PLACED_ZONES(),	"FMMCCMENU_18", -1, HUD_COLOUR_GREEN)
									BREAK
									CASE CREATION_TYPE_PROPS 
										DO_PROP_CREATION(sPropStruct, sVehStruct, sObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, sCamData, sPedStruct, sTrainCreationSTruct, GET_FMMC_MAX_NUM_PROPS(), iLocalBitSet)
									BREAK
									CASE CREATION_TYPE_DYNOPROPS 
										DO_DYNOPROP_CREATION(sDynoPropStruct, sPropStruct, sVehStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, sCamData, sPedStruct, sTrainCreationStruct, FMMC_MAX_NUM_DYNOPROPS, iLocalBitSet)
									BREAK
									CASE CREATION_TYPE_RAND_RADIUS
										DO_RANDOMIZE_DM()
									BREAK
									CASE CREATION_TYPE_WORLD_PROPS
										DO_WORLD_PROP_SELECTION(sFMMCmenu, sCurrentVarsStruct)
									BREAK
									#IF IS_DEBUG_BUILD
									CASE CREATION_TYPE_COVER
										IF FMMC_IS_ROCKSTAR_DEV()
											DO_COVER_POINT_CREATION(sCoverStruct, sCurrentVarsStruct, sFMMCmenu, sFMMCendStage)	
										ENDIF
									BREAK
									#ENDIF
								ENDSWITCH
								
								SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(2)
														
							ELSE

								sCurrentVarsStruct.bDiscVisible = FALSE
								
								IF DOES_BLIP_EXIST(bRandomizerBlip)
									REMOVE_BLIP(bRandomizerBlip)
								ENDIF
																	
								//Reset stuff. 
								sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
								sWepStruct.iSwitchingINT = CREATION_STAGE_WAIT
								sVehStruct.iSwitchingINT = CREATION_STAGE_WAIT	
								SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)									
								
								IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
									DELETE_VEHICLE(sVehStruct.viCoronaVeh)
								ENDIF
								IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
									DELETE_OBJECT(sWepStruct.viCoronaWep)
								ENDIF
								IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
									DELETE_OBJECT(sPropStruct.viCoronaObj)
								ENDIF
								IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
									DELETE_OBJECT(sDynoPropStruct.viCoronaObj)
								ENDIF
								
								FOR i = 0 TO 7
									IF DOES_ENTITY_EXIST(sPropStruct.viWaterCornerVehs[i])
										DELETE_VEHICLE(sPropStruct.viWaterCornerVehs[i])
									ENDIF
								ENDFOR
								
								DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
								
							ENDIF
						ENDIF	
						
						IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							IF sCurrentVarsStruct.iMenuState != MENU_STATE_SWITCH_CAM
								BOOL bReloadMenu
								bReloadMenu = FALSE
								IF NOT IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
									DEAL_WITH_SKY_CAM_SWITCH(sFMMCdata,bSwitchingCam,vSwitchVec,sCamData, bReloadMenu)
								ELSE
									DEAL_WITH_PLAYER_WARP_TO_ITEM(sFMMCdata,bSwitchingCam,vSwitchVec,fSwitchHeading, bReloadMenu)
								ENDIF
								IF bReloadMenu
									REFRESH_MENU(sFMMCMenu)
								ENDIF
							ENDIF
						ENDIF
						
						CHECK_FOR_MENU_SET_UP(sCurrentVarsStruct, sPedStruct, sVehStruct, sWepStruct, sObjStruct, sFMMCmenu)
						IF sCurrentVarsStruct.bResetUpHelp
						AND (sCurrentVarsStruct.bFirstShapeTestCheckDone OR sFMMCmenu.iEntityCreation = -1 OR sFMMCmenu.iEntityCreation = CREATION_TYPE_PAN_CAM)
							sCurrentVarsStruct.bResetUpHelp  = FALSE
						ENDIF
						
#IF IS_DEBUG_BUILD
						ENDIF // Else search function
#ENDIF
					BREAK
					
					CASE MENU_STATE_PLACE_CAM
						IF PPS.iStage != WAIT_PREVIEW_PHOTO
						AND PPS.iStage != CLEANUP_PREVIEW_PHOTO
						AND sFMMCendStage.sTakePhotoVars.iTakePhotoStage != ciFMMC_TAKE_PHOTO_STAGE_OPEN_SHUT
							PPS.iStage = CLEANUP_PREVIEW_PHOTO
							PPS.iPreviewPhotoDelayCleanup = 0
						ENDIF
						
						IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE								
							REMOVE_DECALS_FOR_PHOTOS(sFMMCendStage.sTakePhotoVars.iTakePhotoStage, sStartEndBlips, sPedStruct, sTeamSpawnStruct, sWepStruct, sObjStruct, sHCS.hcStartCoronaColour)
						ENDIF
						IF TAKE_PHOTO_FOR_FMMC(sFMMCendStage.sTakePhotoVars, SF_Movie_Gallery_Shutter_Index, sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
							IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
								g_FMMC_STRUCT.vCameraPanPos = GET_CAM_COORD(GET_RENDERING_CAM())
								g_FMMC_STRUCT.vCameraPanRot = GET_CAM_ROT(GET_RENDERING_CAM())
								VECTOR vRot
								vRot = GET_CAM_ROT(GET_RENDERING_CAM())
								g_FMMC_STRUCT.fCameraPanHead = vRot.z
								PRINTLN("PAN CAM POSITIONED Rot - ", g_FMMC_STRUCT.vCameraPanRot, " Vector - ", g_FMMC_STRUCT.vCameraPanPos)
							ENDIF
							sFMMCendStage.bMajorEditOnLoadedMission = TRUE
							REFRESH_MENU(sFMMCMenu)
							sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						ENDIF
					BREAK
					
					CASE MENU_STATE_PAN_CAM
						IF DEAL_WITH_CREATOR_CAMERA_PAN_PREVIEW(iCamPanState, jobIntroData, vInitialPreviewPos)	
							// Shonky fix for 2102449
							IF !SCRIPT_IS_CLOUD_AVAILABLE()
								IF NOT IS_SCREEN_FADED_IN()
									DO_SCREEN_FADE_IN(500)
								ENDIF	
							ENDIF
							sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						ENDIF
					BREAK
					
					CASE MENU_STATE_TITLE
						IF bDelayAFrame = FALSE
							IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, TRUE)	
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								REFRESH_MENU(sFMMCMenu)
								bDelayAFrame = TRUE
							ELSE							
								bShowMenuHighlight = FALSE
							ENDIF
						ELSE
							bDelayAFrame = FALSE
						ENDIF
					BREAK
					
					CASE MENU_STATE_DESCRIPTION
						IF bDelayAFrame = FALSE
							IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE)	
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								REFRESH_MENU(sFMMCMenu)
								bDelayAFrame = TRUE
							ELSE							
								bShowMenuHighlight = FALSE
							ENDIF
						ELSE
							bDelayAFrame = FALSE
						ENDIF
					BREAK
					
					CASE MENU_STATE_SET_SPEC_OBJ_TEXT
						IF bDelayAFrame = FALSE	
							IF sFMMCMenu.iSelectedTeam > -1
								IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, g_FMMC_STRUCT.tl63_RoamingSpectatorObjectiveTextOverride[sFMMCmenu.iSelectedTeam])										
									sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
									REFRESH_MENU(sFMMCmenu)
									bDelayAFrame = TRUE
								ENDIF
							ENDIF
						ELSE
							bDelayAFrame = FALSE
						ENDIF
					BREAK
					
					CASE MENU_STATE_BLIMP_MESSAGE
						IF bDelayAFrame = FALSE
							IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, FALSE, TRUE)
								REFRESH_MENU(sFMMCMenu)
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								bDelayAFrame = TRUE
								CLEANUP_BLIMP(sBlimpSign)
							ELSE
								bShowMenuHighlight = FALSE
							ENDIF
						ELSE
							bDelayAFrame = FALSE
						ENDIF
					BREAK
									
					CASE MENU_STATE_SET_DM_HELPTEXT
						IF bDelayAFrame = FALSE
							IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, FALSE, FALSE, FALSE, TRUE, GET_DM_HELPTEXT_SLOT()) // [MJL] Get position in menu, then div by 2 to get tl63DMHelptextSaved[index]
								REFRESH_MENU(sFMMCMenu)
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								bDelayAFrame = TRUE
							ELSE
								bShowMenuHighlight = FALSE
							ENDIF
						ELSE
							bDelayAFrame = FALSE
						ENDIF
					BREAK
#IF IS_DEBUG_BUILD
					CASE MENU_STATE_SEARCH
						IF bDelayAFrame = FALSE
							IF DEAL_WITH_MENU_SEARCH(sFMMCendStage, sFMMCmenu)	
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								REFRESH_MENU(sFMMCmenu)
								bDelayAFrame = TRUE
							ENDIF
						ELSE
							bDelayAFrame = FALSE
						ENDIF
					BREAK
#ENDIF						
					CASE MENU_STATE_TAGS
						IF bDelayAFrame = FALSE
							IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, TRUE)	
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								REFRESH_MENU(sFMMCMenu)
								bDelayAFrame = TRUE
							ELSE							
								bShowMenuHighlight = FALSE
							ENDIF
						ELSE
							bDelayAFrame = FALSE
						ENDIF
					BREAK
					
					CASE MENU_STATE_SWITCH_CAM
						
						IF NOT IS_SCREEN_FADED_OUT()
							PRINTSTRING("screen not faded out yet, calling fade out")PRINTNL()
							DO_SCREEN_FADE_OUT(500)								
							IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
								SET_ENTITY_ALPHA(sWepStruct.viCoronaWep, 0, FALSE)
							ENDIF
							IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
								SET_ENTITY_ALPHA(sPropStruct.viCoronaObj, 0, FALSE)
							ENDIF
							IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
								SET_ENTITY_ALPHA(sDynoPropStruct.viCoronaObj, 0, FALSE)
							ENDIF
							IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
								SET_ENTITY_ALPHA(sVehStruct.viCoronaVeh, 0, FALSE)
							ENDIF
						ELSE
							PRINTSTRING("Screen is faded out do swap cams")PRINTNL()
							IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
								SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,TRUE,FALSE,TRUE,FALSE)
							ELSE
								SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,FALSE,FALSE,TRUE,FALSE)
							ENDIF
							NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20)
							sCurrentVarsStruct.iMenuState = MENU_STATE_LOADING_AREA
							iLoadingOverrideTimer = GET_GAME_TIMER()
							REFRESH_MENU(sFMMCmenu)
						ENDIF
					BREAK
					
					CASE MENU_STATE_LOADING_AREA						
						
						IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
						OR IS_NEW_LOAD_SCENE_LOADED()	
						OR iLoadingOverrideTimer + 8000 < GET_GAME_TIMER()
							IF NOT SHOULD_USE_VEHICLES(DEFAULT, iCurrentPlayerModSet)
							OR IS_NON_ARENA_KING_OF_THE_HILL()
							
								PRINTSTRING("LOADED, FADE IN")PRINTNL()
								IF NOT bSwitchingCam
									DO_SCREEN_FADE_IN(500)
								ENDIF
								NEW_LOAD_SCENE_STOP()
								IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
									SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
									SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
								ELSE
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
								ENDIF
								
								IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
									RESET_ENTITY_ALPHA(sWepStruct.viCoronaWep)
								ENDIF
								IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
									RESET_ENTITY_ALPHA(sPropStruct.viCoronaObj)
								ENDIF
								IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
									RESET_ENTITY_ALPHA(sDynoPropStruct.viCoronaObj)
								ENDIF
								IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
									RESET_ENTITY_ALPHA(sVehStruct.viCoronaVeh)
								ENDIF								
								
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
							ELSE 
								REQUEST_MODEL(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, 0))
								
								IF HAS_MODEL_LOADED(GET_VDM_VEHICLE(g_FMMC_STRUCT.iVehicleModel, 0))
									PRINTSTRING("LOADED, FADE IN")PRINTNL()
									DO_SCREEN_FADE_IN(500)
									NEW_LOAD_SCENE_STOP()
									RESET_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
									
									IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
										PUT_PLAYER_IN_VEH()
									ELSE
										IF DOES_ENTITY_EXIST(testPeds[0].viVehicleDM)
											DELETE_VEHICLE(testPeds[0].viVehicleDM)
										ENDIF
									ENDIF
									
									sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								ENDIF
							ENDIF
						ELSE							
							PRINTSTRING("WAITING FOR SCEEN TO LOAD")PRINTNL()
						ENDIF
					BREAK
											
					CASE MENU_STATE_TEST_MISSION
						IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
							DEAL_WITH_TEST_BEING_ACTIVE()
						ENDIF
					BREAK
					
					CASE MENU_STATE_CUSTOM_TEMPLATE_NAME
						IF bDelayAFrame = FALSE
							IF DEAL_WITH_SETTING_CUSTOM_TEMPLATE_NAME(sFMMCendStage, g_FMMC_STRUCT.iNumberOfPropTemplates-1)
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								REFRESH_MENU(sFMMCmenu)
								bDelayAFrame = TRUE
							ENDIF
						ELSE
							bDelayAFrame = FALSE
						ENDIF
					BREAK
					
					CASE MENU_STATE_WARP_TO_INTERIOR
						HANDLE_PLAYER_INTERIOR_WARP(iWarpState, iWarpStateTimeoutStart, sFMMCmenu, sFMMCdata, sCamData, sCurrentVarsStruct)
					BREAK
					
					CASE MENU_STATE_CONFIRMATION_SCREEN
						HANDLE_CONFIRMATION_SCREEN(sFMMCmenu, sCurrentVarsStruct, sFMMCendStage, sPedStruct, sTeamSpawnStruct[sFMMCMenu.iSelectedTeam], iLocalBitSet)
					BREAK
					
					#IF IS_DEBUG_BUILD
					CASE MENU_STATE_ADD_CUSTOM_PROP
						IF bDelayAFrame = FALSE
							IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, FALSE, FALSE, TRUE)
								REFRESH_MENU(sFMMCMenu)
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								bDelayAFrame = TRUE
								SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
							ENDIF
						ELSE
							bDelayAFrame = FALSE
						ENDIF
					BREAK
					
					CASE MENU_STATE_ENTER_SIMPLE_TL63
						IF bDelayAFrame = FALSE
						
							TEXT_LABEL_63 tl63Return
							INT iCap
							iCap = -1
																			
							IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = WORLD_PROPS_SELECTED_MODEL_ENTER_MANUALLY
							AND sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS
								IF sFMMCmenu.iSelectedEntity != -1
									tl63Return = FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sWorldProps[sFMMCmenu.iSelectedEntity].mn)
								ELSE
									tl63Return = FMMC_GET_MODEL_NAME_FOR_DEBUG(sFMMCmenu.sCurrentWorldProp.mn)
								ENDIF
								iCap = 63
							ENDIF
								
							IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, tl63Return, iCap)
																
								IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = WORLD_PROPS_SELECTED_MODEL_ENTER_MANUALLY
								AND sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS
									IF sFMMCmenu.iSelectedEntity != -1
										g_FMMC_STRUCT_ENTITIES.sWorldProps[sFMMCmenu.iSelectedEntity].mn = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(tl63return))
									ELSE
										sFMMCmenu.sCurrentWorldProp.mn = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(tl63return))
									ENDIF						
								ENDIF
								
								SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
								
								tl63return = ""
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								REFRESH_MENU(sFMMCmenu)
								bDelayAFrame = TRUE
							ENDIF
						ELSE
							bDelayAFrame = FALSE
						ENDIF
					BREAK
					#ENDIF
				ENDSWITCH						
				
				BOOL bHideMarker
				IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_COVER_POINT_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
					bHideMarker = TRUE
				ELSE
					bHideMarker = FALSE
				ENDIF
				
				MAINTAIN_PLACED_ZONES(sCurrentVarsStruct, iLocalBitSet, sFMMCmenu)
				
				IF sFMMCmenu.sActiveMenu = eFmmc_RADIO_MENU
					sCurrentVarsStruct.bDiscVisible = FALSE
				ENDIF
				MAINTAIN_PLACEMENT_DISC(sCurrentVarsStruct, sFMMCmenu, iLocalBitSet, sFMMCdata.iBitSet, bHideMarker, sFMMCmenu.iEntityCreation = -1)					
				
				BLIP_INDEX biFurthestBlip
				IF sFMMCmenu.bZoomedOutRadar
					biFurthestBlip = GET_FURTHEST_BLIP_TO_PLAYER(sPedStruct.biPedBlip, sVehStruct.biVehicleBlip, sWepStruct.biWeaponBlip, sPropStruct.biObject, sDynoPropStruct.biObject, sStartEndBlips.biStart, bCameraPanBlip, sCurrentVarsStruct.biLocateBlip)
				ENDIF
				IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
					CONTROL_PLAYER_BLIP(TRUE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, TRUE, biFurthestBlip)
				ELSE
					IF sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
					AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
					AND sFMMCmenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
					AND sFMMCmenu.sActiveMenu != eFmmc_TOP_MENU
					AND sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU
						CONTROL_PLAYER_BLIP(FALSE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, sFMMCmenu.bZoomedOutRadar, biFurthestBlip)
					ELSE
						CONTROL_PLAYER_BLIP(TRUE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, sFMMCmenu.bZoomedOutRadar, biFurthestBlip)
					ENDIF												
				ENDIF

			BREAK
			
			//Deal with using the Delete All menu
			CASE STAGE_DELETE_ALL
			CASE STAGE_DELETE_PEDS
			CASE STAGE_DELETE_HILL_AREAS
			CASE STAGE_DELETE_VEHICLES
			CASE STAGE_DELETE_WEAPONS
			CASE STAGE_DELETE_PROPS
			CASE STAGE_DELETE_DYNOPROPS
			CASE STAGE_DELETE_TEAM_START
				DO_CONFIRMATION_MENU(sPedStruct, sTeamSpawnStruct, sVehStruct, sWepStruct, sObjStruct, sPropStruct, sDynoPropStruct, sLocStruct, sCurrentVarsStruct, sFMMCmenu, sInvisibleObjects, sFMMCEndStage, ButtonPressed, iLocalBitSet)
			BREAK
			
			
			//Deal with the end menu
			CASE STAGE_ENTITY_PLACEMENT_END_MENU
				DO_END_MENU()
			BREAK	
			
			// Cloud is down. Show a warning and wait for the player to accept.
			CASE STAGE_CLOUD_FAILURE				
			
				IF sCurrentVarsStruct.bNetworkBailed
					SET_WARNING_MESSAGE_WITH_HEADER("FMMC_CLDEAD", "FMMC_CLDEAD1", FE_WARNING_OK)
				ELSE
					FMMC_DRAW_CLOUD_FAIL_WARNING(bCreatorLimitedCloudDown, bSignedOut, NOT HAS_DM_CREATOR_TUTORIAL_COMPLETED(tutDMCreator))
				ENDIF
				
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					// Shonky fix for 2102449
					IF !SCRIPT_IS_CLOUD_AVAILABLE()
						sCurrentVarsStruct.iMenuState = MENU_STATE_PAN_CAM
					ELSE
						IF NOT IS_SCREEN_FADED_IN()
							DO_SCREEN_FADE_IN(500)
						ENDIF
					ENDIF
					sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
					REFRESH_MENU(sFMMCMenu)
				ENDIF
			BREAK	
			
			// Re creating a DM after testing
			CASE STAGE_DEAL_WITH_RECREATING_MISSION
				IF NOT IS_SCREEN_FADING_OUT()	
					IF RESET_THE_MISSION_UP_SAFELY()
						iResetState = RESET_STATE_FADE		
						SET_STORE_ENABLED(TRUE)
						SET_CREATOR_AUDIO(TRUE, FALSE)						
						IF sFMMCmenu.iRadioState = FMMC_RADIO_MAX_OPTIONS_STATE
							SET_MOBILE_PHONE_RADIO_STATE(TRUE)
						ENDIF	
						
						CLEAR_HELP()
						CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
						CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
						
						bPlayerIsRespawning = FALSE
						
						ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
						ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
						
						SET_ACTIVE_MENU(sFMMCMenu, eFmmc_TOP_MENU)
						
						KILL_WORLD_FOR_THE_CREATORS()
						
						IF IS_KING_OF_THE_HILL()
							RESET_ALL_KING_OF_THE_HILL_ZONE_HOST_INFO(sFMMCmenu.sKOTH_HostInfo)
							CLEANUP_KING_OF_THE_HILL(sFMMCmenu.sKOTH_LocalInfo, FALSE)
						ENDIF
						
						FOR i = 0 TO FMMC_MAX_NUM_DM_BOUNDS - 1
							CLEANUP_DEATHMATCH_BOUNDS_DATA(sFMMCmenu.sDeathmatchBoundsData[i])
						ENDFOR
						RESET_RUNTIME_BOUNDS_DATA(sFMMCmenu.sDeathmatchBoundsData)
						
						iHelpBitSetOld = -1
						
						IF sCurrentVarsStruct.bTestModeFailed
							sCurrentVarsStruct.iEntityCreationStatus = STAGE_CLOUD_FAILURE
						ELSE
							sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
						ENDIF
						
						sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						
						sFMMCdata.vPlayerPos = <<0,0,10>>
								
						CHECK_FOR_END_CONDITIONS_MET()
						//Clean up
						REFRESH_MENU(sFMMCMenu)
												
						if IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
							DO_SCREEN_FADE_IN(500)
						ELSE
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						ENDIF	
					ENDIF
				ENDIF
			BREAK
			
			//Deal With LOADING a previous mission
			CASE STAGE_DEAL_WITH_LOADING	
			
				PRINTLN("STAGE_DEAL_WITH_LOADING")
				
				IF SET_SKYSWOOP_UP(TRUE)
				OR IS_ARENA_CREATOR()
				
					IF RESET_THE_MISSION_UP_SAFELY()
						iResetState = RESET_STATE_FADE
						IF sFMMCmenu.iRadioState = FMMC_RADIO_MAX_OPTIONS_STATE
							SET_MOBILE_PHONE_RADIO_STATE(TRUE)
						ENDIF			
						CLEAR_HELP()
						CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
						CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)	
						
						sCurrentVarsStruct.creationStats.bEditingACreation = TRUE
						#IF IS_DEBUG_BUILD
						IF g_bFMMC_EnteredLoadFromPack
							sCurrentVarsStruct.creationStats.bEditingACreation = FALSE
							IF NOT IS_BIT_SET(g_iLoadFromPackExtraBS, ciLoadFromPackExtraBS_LegacyDM)
								SETUP_FRESH_DEATHMATCH_UPDATE_SETTING_DEFAULTS()
							ENDIF
							PRINTLN("STAGE_DEAL_WITH_LOADING - Clearing sCurrentVarsStruct.creationStats.bEditingACreation as we have accessed content through 'Load from Pack' menu")
						ENDIF
						#ENDIF
						
						IF g_FMMC_STRUCT.bMissionIsPublished
							sCurrentVarsStruct.creationStats.bMadeAChange = FALSE
							SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
						ENDIF
						//Clean up
						REFRESH_MENU(sFMMCMenu)
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_LOAD	
						sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
						ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
						iHelpBitSetOld = -1

						WARP_CAMERA_TO_START_LOCATION(sCamData, 110.0)	
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_VOTING_DISABLED) 
							CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
						ELSE
							SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
						ENDIF
						
						IF (HAS_DM_CREATOR_TUTORIAL_COMPLETED(tutDMCreator) OR IS_KING_OF_THE_HILL())
						AND NOT IS_ARENA_CREATOR()
							DO_SCREEN_FADE_IN(500)
						ENDIF		
						
						IF IS_LOADING_ICON_ACTIVE()
							SET_LOADING_ICON_INACTIVE()
						ENDIF
						
						SET_DLC_AND_CHECK_FOR_LEGACY()
					ELSE
						PRINTLN("RESET_THE_MISSION_UP_SAFELY = FALSE")
					ENDIF	
				ENDIF
				
			BREAK
			
			//The player wants to save their Creation
			CASE STAGE_DEAL_WITH_SAVING		
			
				IF bContentReadyForUGC = FALSE
					IF PREPARE_DM_TO_BE_SAVED_OR_PUBLISHED()
						bContentReadyForUGC = TRUE
					ENDIF					
				ELSE
					IF SAVE_THIS_CREATION(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats)//SAVE_MISSION_TO_UGC_SERVER(sGetUGC_content, sFMMCendStage, FALSE, FALSE)
						sFMMCendStage.iEndStage = 0
						sFMMCendStage.dataStruct.iLoadStage = 0
						sFMMCendStage.iPublishConformationStage = 0
						sFMMCendStage.iPublishStage = 0
						sFMMCendStage.iButtonBitSet = 0
						#IF IS_DEBUG_BUILD
						PRINT_FMMC_DEBUGDATA(FALSE)	
						#ENDIF
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
						sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DO_SCREEN_FADE_IN(200)
						sCurrentVarsStruct.bSwitching = FALSE
						sCurrentVarsStruct.stiScreenCentreLOS = NULL
						bContentReadyForUGC = FALSE
						IF NOT IS_STRING_NULL_OR_EMPTY(sFMMCendStage.tl23ReturnedContexID)
							sFMMCendStage.tl23LoadedContexID = sFMMCendStage.tl23ReturnedContexID						
							// Removed for: 2162073/2162036 CREATOR_WRITE_TO_LEADERBOARD(sCurrentVarsStruct.creationStats, sFMMCendStage.tl23ReturnedContexID)
						ENDIF
					ENDIF
					IF sFMMCendStage.oskStatus = OSK_CANCELLED
						sFMMCendStage.iEndStage = 0
						sFMMCendStage.dataStruct.iLoadStage = 0
						sFMMCendStage.iPublishConformationStage = 0
						sFMMCendStage.iPublishStage = 0
						sFMMCendStage.iButtonBitSet = 0
						sFMMCendStage.oskStatus = OSK_PENDING
						sFMMCendStage.iKeyBoardStatus = 0
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
						sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						sCurrentVarsStruct.bSwitching = FALSE
						sCurrentVarsStruct.stiScreenCentreLOS = NULL
						bContentReadyForUGC = FALSE
						DO_SCREEN_FADE_IN(200)
						#IF IS_DEBUG_BUILD
						PRINTLN("oskStatus = OSK_CANCELLED")
						#ENDIF					
					ENDIF
				ENDIF
			BREAK
			
			//The player wants to publish their Creation
			CASE STAGE_ENTITY_PLACEMENT_PUBLISH
				IF bContentReadyForUGC = FALSE
					IF PREPARE_DM_TO_BE_SAVED_OR_PUBLISHED()
						bContentReadyForUGC = TRUE
					ENDIF
				ELSE	
					IF PUBLISH_THIS_CREATION(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats)
						REQUEST_SYSTEM_ACTIVITY_TYPE_PUBLISHED_DM(g_FMMC_STRUCT.tl63MissionName)
						sFMMCendStage.iEndStage = 0
						sFMMCendStage.dataStruct.iLoadStage = 0
						sFMMCendStage.iPublishConformationStage = 0
						sFMMCendStage.iPublishStage = 0
						sFMMCendStage.iButtonBitSet = 0
						#IF IS_DEBUG_BUILD
						PRINT_FMMC_DEBUGDATA(FALSE)	
						#ENDIF
			 			sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
						sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
						
						IF sCurrentVarsStruct.creationStats.bSuccessfulSave = TRUE
							PRINTLN("sFMMCendStage.bSaveCancelled = FALSE, setting sFMMCendStage.bMajorEditOnLoadedMission = FALSE")
							sFMMCendStage.bMajorEditOnLoadedMission = FALSE
							IF NOT IS_STRING_NULL_OR_EMPTY(sFMMCendStage.tl23ReturnedContexID)
								sFMMCendStage.tl23LoadedContexID = sFMMCendStage.tl23ReturnedContexID						
								// Removed for: 2162073/2162036 CREATOR_WRITE_TO_LEADERBOARD(sCurrentVarsStruct.creationStats, sFMMCendStage.tl23ReturnedContexID)
							ENDIF
						ENDIF
						
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						sCurrentVarsStruct.bSwitching = FALSE
						sCurrentVarsStruct.stiScreenCentreLOS = NULL
						bContentReadyForUGC = FALSE
						CHECK_FOR_END_CONDITIONS_MET()
						CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
						
						PRINTLN("PUBLISH_THIS_CREATION = TRUE")
						REFRESH_MENU(sFMMCMenu)
						DO_SCREEN_FADE_IN(200)			
					ENDIF
					IF sFMMCendStage.oskStatus = OSK_CANCELLED
						sFMMCendStage.iEndStage = 0
						sFMMCendStage.dataStruct.iLoadStage = 0
						sFMMCendStage.iPublishConformationStage = 0
						sFMMCendStage.iPublishStage = 0
						sFMMCendStage.iButtonBitSet = 0
						sFMMCendStage.oskStatus = OSK_PENDING
						sFMMCendStage.iKeyBoardStatus = 0
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_SETUP
						sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						sCurrentVarsStruct.bSwitching = FALSE
						sCurrentVarsStruct.stiScreenCentreLOS = NULL
						bContentReadyForUGC = FALSE
						DO_SCREEN_FADE_IN(200)
						#IF IS_DEBUG_BUILD
						PRINTLN("oskStatus = OSK_CANCELLED")
						#ENDIF					
					ENDIF
				ENDIF
			BREAK
			
			//Deal With ENDING THE the FMMC Script
			CASE STAGE_DEAL_WITH_FINISHING
				IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()
					TRIGGER_TRANSITION_MENU_ACTIVE(TRUE) 					
					PRINTLN("[JA@PAUSEMENU] Clean up dm creator due to IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP")
					DO_SCREEN_FADE_IN(200)
					SCRIPT_CLEANUP(TRUE)
				ELIF IS_PAUSE_MENU_REQUESTING_TRANSITION()
					TRIGGER_TRANSITION_MENU_ACTIVE(TRUE) 
					PRINTLN("[JA@PAUSEMENU] Clean up dm creator due to pause menu requesting transition")
					DO_SCREEN_FADE_IN(200)
					SCRIPT_CLEANUP(TRUE)
				ELIF DEAL_WITH_FINISHING_CREATOR(sFMMCendStage)//, FALSE)
					IF NOT CONTENT_IS_USING_ARENA()
						DO_SCREEN_FADE_IN(200)
					ENDIF
					
					PRINTLN("[END CREATOR] DONE")
				 	SCRIPT_CLEANUP(FALSE)
				ENDIF
			BREAK		
		ENDSWITCH
		
		IF NOT IS_SCREEN_FADED_IN()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_NEXT_CAMERA)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("process main logic")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		
			IF bGetUGC_byIDWithString
				STRING stWidgetContents 
				stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
				IF IS_STRING_NULL(stWidgetContents)
					bGetUGC_byIDWithString = FALSE
				ENDIF
				IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, stWidgetContents)			
					bGetUGC_byIDWithString = FALSE
					sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_LOADING	
					CORRECT_MAX_FOR_MENUS_FROM_UGC(g_FMMC_STRUCT.iMissionType, g_FMMC_STRUCT.iNumParticipants, g_FMMC_STRUCT.iRaceType)
				ENDIF
			ENDIF
			
			//Update the widgets
			UPDATE_WIDGETS()
			
			if g_FMMC_STRUCT.iVehicleDeathmatch != iModelArray
				INITIALISE_MODEL_ARRAYS(sPedStruct,	sWepStruct, sObjStruct, sFMMCmenu, g_FMMC_STRUCT.iVehicleDeathmatch = 0)
				iModelArray = g_FMMC_STRUCT.iVehicleDeathmatch
			ENDIF
			
			//Force clean up
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
					sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_END_MENU
				ENDIF
			ENDIF
			
			IF sCurrentVarsStruct.bSaveXml
				SAVE_FM_MISSION_CREATOR_DATA(iXmlToSave)
				sCurrentVarsStruct.bSaveXml = FALSE
			ENDIF
			
			IF bLoadXML
				IF LOAD_XML_DATA(iXmlToLoad)
					WARP_CAMERA_TO_START_LOCATION(sCamData, 110.0)
					bLoadXML = FALSE
				ENDIF
			ENDIF
			
		#ENDIF
		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
			PRINTLN("[LM] - Creator Bit - Clearing bMenuJustChangedPlacementBlocker")
			CLEAR_BIT(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("debug stuff")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		IF g_bDisableCreatorForceTest
			sCurrentVarsStruct.creationStats.bMadeAChange = FALSE
			sCurrentVarsStruct.creationStats.iTimesTestedLoc = 1
			INT iTeam
			REPEAT FMMC_MAX_TEAMS iTeam
				SET_BIT(g_FMMC_STRUCT.biTeamTestComplete, iTeam)
			ENDREPEAT			
		ENDIF
		#ENDIF	
		
		IF g_Private_Gamemode_Current = GAMEMODE_FM
		AND NETWORK_IS_GAME_IN_PROGRESS()
			PRINTLN("[LH] A Network Game is in Progress! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		//Quit if the player is in singleplayer
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(hash("main")) != 0
			PRINTLN("[LH] The player has escaped to singleplayer! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		//If we've been signed out the wait for Brenda to dealwith cleanup
		IF g_bKillFMMC_SignedOut
			SCRIPT_CLEANUP(TRUE)
			PRINTLN("g_bKillFMMC_SignedOut = TRUE")
		ENDIF
		
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(hash("freemode")) != 0
			PRINTLN("The player has escaped to freemode! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
			NETWORK_BAIL_FROM_CREATOR()
		ENDIF
		
	ENDWHILE
ENDSCRIPT

