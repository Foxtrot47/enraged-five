//╔═════════════════════════════════════════════════════════════════════════════╗
//║		FM_Impromptu_DM_Controler.sc											║
//║		Christopher Speirs/ Robert Wright										║
//╚═════════════════════════════════════════════════════════════════════════════╝	
USING "globals.sch"
USING "shared_hud_displays.sch"
USING "net_spawn.sch"
USING "net_deathmatch.sch"
USING "freemode_header.sch"
USING "net_script_tunables.sch"		// KGM 25/7/12 - added so that Refresh_MP_Script_Tunables() can be called to gather Deathmatch specific values
USING "net_wait_zero.sch"
#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				VARIABLES														║
//╚═════════════════════════════════════════════════════════════════════════════╝
SHARED_DM_VARIABLES dmVars 
INT iLoopParticipant
FMMC_SERVER_DATA_STRUCT serverBD
ServerBroadcastData serverBDdeathmatch
ServerBroadcastData_Leaderboard serverBD_Leaderboard
PlayerBroadcastData playerBD[MAX_NUM_DM_PLAYERS]

LEADERBOARD_PLACEMENT_TOOLS Placement 

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				PROCS															║
//╚═════════════════════════════════════════════════════════════════════════════╝

PROC SERVER_SYNC_GLOBALS()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.iDuration 			 	= GlobalServerBD_DM.iDuration 	
		serverBD.iTarget			 	= GlobalServerBD_DM.iTarget		
		serverBD.iSpawnTime			 	= GlobalServerBD_DM.iSpawnTime	
//		serverBD.iAim				 	= GlobalServerBD_DM.iAim			
		serverBD.iBlips		 		 	= GlobalServerBD_DM.iBlips		
		serverBD.iTags			 	 	= GlobalServerBD_DM.iTags			
		serverBD.iVoice		 		 	= GlobalServerBD_DM.iVoice		
		serverBD.iWeaponRespawnTime  	= GlobalServerBD_DM.iWeaponRespawnTime
		serverBD.iVehicleRespawnTime 	= GlobalServerBD_DM.iVehicleRespawnTime 
		serverBD.bIsTeam				= GlobalServerBD_DM.bIsTeamDM
	ENDIF
ENDPROC

//Resset the players PlayerBroadcastData
PROC RESET_PlayerBroadcastData()
	PlayerBroadcastData playerBBReset
	playerBD[PARTICIPANT_ID_TO_INT()] = playerBBReset
	PRINTLN("[RESET] RESET_PlayerBroadcastData ")
ENDPROC

//Reset all the ServerBroadcastData
PROC RESET_ServerBroadcastData()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBroadcastData serverBDReset 
		serverBDdeathmatch = serverBDReset
		PRINTLN("[RESET] RESET_ServerBroadcastData ")
	ENDIF
ENDPROC

/// PURPOSE:
///    Runs on all clients at startup.  Used to initialize values and set/register variables and such
/// RETURNS:
///    FALSE if the scritp fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA &fmmcMissionData)

	
	g_FMMC_STRUCT.iMissionType = FMMC_TYPE_IMPROMPTU_DM
	g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
	g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles = 2
	CLEAR_ABANDON_MP_MISSION_FLAG()
	FMMC_PROCESS_PRE_GAME_COMMON(fmmcMissionData, serverBD.iPlayerMissionToLoad, serverBD.iMissionVariation, DEFAULT, FALSE)
	
	//Cache Host and Particiant ID for use in this function
	PROCESS_CACHING_NETWORK_HOST()
	PROCESS_CACHING_LOCAL_PARTICIPANT_STATE(TRUE)

	GlobalplayerBD_FM[iLocalPart].iCurrentMissionType = FMMC_TYPE_IMPROMPTU_DM
	
	IF bIsLocalPlayerHost
		serverBDdeathmatch.iMissionVariation = serverBD.iMissionVariation
	ENDIF
	
	#IF IS_DEBUG_BUILD
	g_sDM_SB_CoronaOptions.bOnePlayerDeathmatch = GlobalServerBD_DM.bOnePlayerDeathmatch
	#ENDIF
	//IN IMPROMPTU DM THERE ARE NO OPTIONS, SET SOME DEFAULTS!
	GlobalServerBD_DM.iNumberOfTeams 		= 0
	GlobalServerBD_DM.iLocation 			= 0
	GlobalServerBD_DM.iDuration 			= DM_DURATION_5
	GlobalServerBD_DM.iTarget				= DM_TARGET_KILLS_IMPROMPTU
	GlobalServerBD_DM.iWeapons 				= 0
	GlobalServerBD_DM.iPeds					= 0
	GlobalServerBD_DM.iRadio	 			= 0
	GlobalServerBD_DM. iVoice				= 0
	GlobalServerBD_DM.iPlayerHealth			= 0
	GlobalServerBD_DM.iWeaponBlips			= 0
	GlobalServerBD_DM.iSpawnTime			= DM_SPAWN_TIME_MED	
	GlobalServerBD_DM.iWeaponRespawnTime	= 2	
	GlobalServerBD_DM.iVehicleRespawnTime 	= VEH_RESPAWN_OFF	
	GlobalServerBD_DM.iBlips				= DM_BLIPS_ALWAYS
	GlobalServerBD_DM.iWeaponBlips	 		= PICKUP_BLIPS_ON
	GlobalServerBD_DM.iTags					= TAGS_WHEN_TARGETED	
	GlobalServerBD_DM.iPeds					= PEDS_OFF			
	GlobalServerBD_DM.iPolice				= POLICE_OFF
	GlobalServerBD_DM.iHealthBar			= DM_HEALTH_BARS_ON	
	GlobalServerBD_DM.iTimeOfDay			= TIME_OFF
	GlobalServerBD_DM.iWeather				= ciFMMC_WEATHER_OPTION_CURRENT
	IF IS_BIT_SET(fmmcMissionData.iBitSet, ciIMPROMPTU_DOG_FIGHT)
		PRINTLN("IS_BIT_SET(fmmcMissionData.iBitSet, ciIMPROMPTU_DOG_FIGHT)")
		GlobalServerBD_DM.iTypeOfDeathmatch	= FMMC_DM_TYPE_VEHICLE	
		g_mnMyRaceModel = g_mnImpromptuVehicle
	ELSE
		GlobalServerBD_DM.iTypeOfDeathmatch	= 0	
	ENDIF
	PRINTLN("GlobalServerBD_DM.iTypeOfDeathmatch= ", GlobalServerBD_DM.iTypeOfDeathmatch )
	GlobalServerBD_DM.iVehicleDeathmatch  	= 0
	GlobalServerBD_DM.bIsTeamDM 			= FALSE		
	
	RESET_PlayerBroadcastData()
	RESET_ServerBroadcastData()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_DM, SIZE_OF(GlobalServerBD_DM))
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBDdeathmatch, SIZE_OF(serverBDdeathmatch))
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD_Leaderboard, SIZE_OF(serverBD_Leaderboard))
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(g_MissionControllerserverBD_LB, SIZE_OF(g_MissionControllerserverBD_LB))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	#IF IS_DEBUG_BUILD
	INT iSize = SIZE_OF(serverBDdeathmatch)
	PRINTLN("Size of serverBDdeathmatch: ", (iSize * 8))
	iSize = SIZE_OF(serverBD_Leaderboard)
	PRINTLN("Size of serverBD_Leaderboard: ", (iSize * 8))
	#ENDIF
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	INT i
	
	IF bIsLocalPlayerHost
		serverBDdeathmatch.vImpromptuCoord = GET_PLAYER_COORDS(LocalPlayer)
	ENDIF
	// Setup rel groups
	REPEAT NUM_NETWORK_PLAYERS i
		TEXT_LABEL_63 tlRelGroup
		tlRelGroup = "dmVars.rgFM_DEATHMATCH"
		tlRelGroup += i
		ADD_RELATIONSHIP_GROUP(tlRelGroup, dmVars.rgFM_DEATHMATCH[i])
	ENDREPEAT
	REPEAT NUM_NETWORK_PLAYERS i
		SET_TEAM_RELATIONSHIPS(dmVars, ACQUAINTANCE_TYPE_PED_HATE, dmVars.rgFM_DEATHMATCH[i])
	ENDREPEAT
	
	// For Neil's spawning function
	IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
//		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(LocalPlayerPed, dmVars.rgFM_DEATHMATCH[iLocalPart])	//Put all players on default team to start with
	ELSE
		SCRIPT_ASSERT("Player is dead while trying to start deathmatch")
	ENDIF
	
	SET_FM_MISSION_LAUNCHED_SUCESS(fmmcMissionData.mdID.idCreator, fmmcMissionData.mdID.idVariation, fmmcMissionData.iInstanceId)
	
	SERVER_SYNC_GLOBALS()
	
	PRINTLN("PROCESS_PRE_GAME_FM_Impromptu_DM_Controler.sc ")
	
	SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_INI)
	
	dmVars.sEndOfMission.bOnlyDoQuit = TRUE
	
	GB_CLEANUP_TARGETTING()
	
	RETURN TRUE
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				MAIN LOOP														║
//╚═════════════════════════════════════════════════════════════════════════════╝

SCRIPT(MP_MISSION_DATA fmmcMissionData)
	
	PRINTLN("------------------------------------------------------------------------------")
	PRINTLN("--                                                                          --")
	PRINTLN("--              Impromptu_DM_Controler_LAUNCHED!!                           --")
	PRINTLN("--                                                                          --")
	PRINTLN("------------------------------------------------------------------------------")
	
	// Carry out all the initial game starting duties. 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(fmmcMissionData)
			PRINTLN("Impromptu_DM_controller: Failed to receive intial network broadcast. Cleaning up.")
			SCRIPT_CLEANUP(serverBDdeathmatch, serverBD, playerBD,  dmVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED, serverBD_Leaderboard)
		ENDIF
	ENDIF
	
	// KGM 25/7/12: Load Tunable Script Variable values from the cloud for Freemode Deathmatches
//	Refresh_MP_Script_Tunables(TUNE_CONTEXT_FM_DM)

	#IF IS_DEBUG_BUILD
//		dmVars.iInstance = fmmcMissionData.iInstanceId
		CREATE_DM_WIDGETS(serverBDdeathmatch, playerBD, dmVars, serverBD_Leaderboard)
	#ENDIF
	
	WHILE TRUE
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF			
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			SCRIPT_CLEANUP(serverBDdeathmatch, serverBD, playerBD,  dmVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED, serverBD_Leaderboard)
		ENDIF
		INT iSlot = NATIVE_TO_INT(PLAYER_ID())
		IF iSlot <> -1
			IF IS_BIT_SET(GlobalplayerBD_FM[iSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob) = FALSE // we want player to go through dm results if phone quitting
				IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
					PRINTLN("SHOULD_PLAYER_LEAVE_MP_MISSION")
					SCRIPT_CLEANUP(serverBDdeathmatch, serverBD, playerBD,  dmVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED, serverBD_Leaderboard)
				ENDIF
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SCRIPT_CLEANUP checks")
		#ENDIF
		#ENDIF
		
		PROCESS_PRE_FRAME_LOCAL_PLAYER_CACHING()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_PRE_FRAME_LOCAL_PLAYER_CACHING")
		#ENDIF
		#ENDIF
		
		PROCESS_DEATHMATCH_EVENTS(serverBDdeathmatch, serverBD_Leaderboard, serverBD, playerBD, dmVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_DEATHMATCH_EVENTS")
		#ENDIF
		#ENDIF			
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
//			//This now has to happen every frame otherwise it will not properly ensure the correct traffic state on the server or client
//			FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME(GET_DM_PED_DENSITY(), 0.0, 0.0, GET_TRAFFIC_DENSITY_FOR_DM(serverBDdeathmatch), GET_TRAFFIC_DENSITY_FOR_DM(serverBDdeathmatch), GET_PARKED_TRAFFIC_DENSITY_FOR_DM(serverBDdeathmatch), GET_TRAFFIC_DENSITY_FOR_DM(serverBDdeathmatch), IS_TRAFFIC_DISABLED_FOR_DM(serverBDdeathmatch) )
	
//╔═════════════════════════════════════════════════════════════════════════════╗
//║			CLIENT																║
//╚═════════════════════════════════════════════════════════════════════════════╝	
			SWITCH GET_CLIENT_GAME_STATE(playerBD, PARTICIPANT_ID_TO_INT())
							
				// Wait until the server gives the all go before moving on.
				CASE GAME_STATE_INI
					IF GET_SERVER_GAME_STATE(serverBDdeathmatch) > GAME_STATE_INI	
						IF serverBDdeathmatch.iDeathmatchType <> -1
							IF LOAD_DM_TEXT_BLOCK()
								IF NOT IS_VECTOR_ZERO(serverBDdeathmatch.vImpromptuCoord)
									IF IS_NET_PLAYER_OK(PLAYER_ID())	
//										CLEANUP_WAYPOINTS_AND_GPS()									
										SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_RUNNING)
									ELSE
										PRINTLN("Stuck IS_NET_PLAYER_OK   ")
									ENDIF
								ELSE
									PRINTLN("serverBDdeathmatch.vImpromptuCoord = 0.0, 0.0, 0.0")
								ENDIF
							ELSE
								PRINTLN("Stuck LOAD_DM_TEXT_BLOCK ")
							ENDIF
						ELSE
							PRINTLN("serverBDPassed.iDeathmatchType = -1, add PT to Bobby ")
						ENDIF
					ELSE
						PRINTLN("Stuck SERVER_GAME_STATE =      ", GET_SERVER_GAME_STATE(serverBDdeathmatch), "     ")
					ENDIF
				BREAK
				
				// Main gameplay state.
				CASE GAME_STATE_RUNNING		
					
					PROCESS_CLIENT_DEATHMATCH(serverBD, Placement, serverBDdeathmatch, playerBD, dmVars, serverBD_Leaderboard)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("PROCESS_CLIENT_DEATHMATCH")
					#ENDIF
					#ENDIF
					
					// Loop through everyone
					MAINTAIN_DEATHMATCH_PLAYER_LOOP(serverBDdeathmatch, playerBD, dmVars)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DEATHMATCH_PLAYER_LOOP")
					#ENDIF
					#ENDIF
					
					// -----------------------------------	DEBUGGERY ----------------------------------------------------------------------
					#IF IS_DEBUG_BUILD
						HOLD_NUM_7_TO_PRINT_CLIENT_STAGE(playerBD)
					#ENDIF
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("UPDATE_DEBUG_SKIPS")
					#ENDIF
					#ENDIF
					
					// ---------------------------------------------------------------------------------------------------------------------
				BREAK	
				
				CASE GAME_STATE_LEAVE			
					SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_END)
				FALLTHRU

				CASE GAME_STATE_END		
					SCRIPT_CLEANUP(serverBDdeathmatch, serverBD, playerBD,  dmVars, Placement, ciFMMC_END_OF_MISSION_STATUS_PASSED, serverBD_Leaderboard)
				BREAK
					
				DEFAULT 
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("DM: Problem in SWITCH GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT())") 
					#ENDIF
				BREAK
			ENDSWITCH
		ENDIF
//╔═════════════════════════════════════════════════════════════════════════════╗
//║		SERVER																	║
//╚═════════════════════════════════════════════════════════════════════════════╝	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			OPEN_SCRIPT_PROFILE_MARKER_GROUP("impromptu server") 
			#ENDIF
			#ENDIF
		
			#IF IS_DEBUG_BUILD
				NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER stuff being called by machine that is not host! Host only command!")
			#ENDIF
			
			SERVER_SETS_BIT_WHEN_TEAMS_COUNTED(serverBDdeathmatch)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_BIT_WHEN_TEAMS_COUNTED")
			#ENDIF
			#ENDIF

			iLoopParticipant += 1			
			IF (iLoopParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS())
				iLoopParticipant = 0 	
			ENDIF
			
			SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT(serverBD_Leaderboard.leaderboard, serverBDdeathmatch, playerBD, iLoopParticipant #IF IS_DEBUG_BUILD , dmVars #ENDIF )										
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT")
			#ENDIF
			#ENDIF
			
			SERVER_STORES_FINISH_TIME(serverBDdeathmatch)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("SERVER_STORES_FINISH_TIME")
			#ENDIF
			#ENDIF
										
			SWITCH GET_SERVER_GAME_STATE(serverBDdeathmatch)
				
				CASE GAME_STATE_INI							
					IF PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBDdeathmatch.iHashedMac, serverBDdeathmatch.iMatchHistoryId)
						INIT_SERVER_DATA(serverBDdeathmatch, serverBD)
						SET_SERVER_GAME_STATE(serverBDdeathmatch, GAME_STATE_RUNNING)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("Impromptu_DM_controller: PLAYSTATS_CREATE_MATCH_HISTORY_ID_2 = FALSE")
						#ENDIF
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING		
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
					OPEN_SCRIPT_PROFILE_MARKER_GROUP("impromptu - GAME_STATE_RUNNING") 
					#ENDIF
					#ENDIF
					
					SERVER_GRABS_IMPROMPTU_VECTOR(serverBDdeathmatch)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_GRABS_IMPROMPTU_VECTOR")
					#ENDIF
					#ENDIF
					
					SERVER_GRABS_AVERAGE_RANK_OF_DEATHMATCH_PLAYERS(serverBDdeathmatch)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_GRABS_AVERAGE_RANK_OF_DEATHMATCH_PLAYERS")
					#ENDIF
					#ENDIF
					
					// TIME
					// -----------------------------------------------------------------------------------------------//
					START_DM_TIMER(serverBDdeathmatch)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("START_DM_TIMER")
					#ENDIF
					#ENDIF
					
					#IF IS_DEBUG_BUILD
					SERVER_DEBUG_TIME_DOWN(serverBDdeathmatch)	
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_DEBUG_TIME_DOWN")
					#ENDIF
					#ENDIF
					#ENDIF
					
//					//  Track winning team
//					// -----------------------------------------------------------------------------------------------//
//					SERVER_SORT_WINNING_TEAMS(serverBDdeathmatch, dmVars)
//					#IF IS_DEBUG_BUILD
//					#IF SCRIPT_PROFILER_ACTIVE 
//						ADD_SCRIPT_PROFILE_MARKER("SERVER_SORT_WINNING_TEAMS")
//					#ENDIF
//					#ENDIF
					
					SERVER_ASSIGN_WINNING_TEAMS(serverBDdeathmatch)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_ASSIGN_WINNING_TEAMS")
					#ENDIF
					#ENDIF
					
					// Don't move this it caused issues with restarting
					SERVER_PROCESSING(serverBDdeathmatch, playerBD, dmVars #IF IS_DEBUG_BUILD , TRUE #ENDIF, serverBD_Leaderboard)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_PROCESSING")
					#ENDIF
					#ENDIF
					
					// Server hands out spawn tickets
					SERVER_SPAWN_TICKETS(serverBDdeathmatch, playerBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_SPAWN_TICKETS")
					#ENDIF
					#ENDIF
					
					SERVER_SETS_ONE_PLAYER_LEFT_BIT(serverBDdeathmatch #IF IS_DEBUG_BUILD , dmVars #ENDIF)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_ONE_PLAYER_LEFT_BIT")
					#ENDIF
					#ENDIF
				
//					// Look for server end conditions
//					IF HAVE_DM_END_CONDITIONS_BEEN_MET(serverBDdeathmatch, dmVars)
//						
//						SET_SERVER_GAME_STATE(serverBDdeathmatch, GAME_STATE_END)
//					ENDIF	
//					#IF IS_DEBUG_BUILD
//					#IF SCRIPT_PROFILER_ACTIVE 
//						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_NUM_DM_PLAYERS")
//					#ENDIF
//					#ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
					CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
					#ENDIF
					#ENDIF
				BREAK
				
				// Shouldn't ever get in here
				CASE GAME_STATE_END		

				BREAK	
				
				DEFAULT 
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("DM: Problem in SWITCH GET_SERVER_GAME_STATE()") 
					#ENDIF
				BREAK
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("processing server game state")
			#ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
			#ENDIF
			#ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("Server processing")
		#ENDIF
		#ENDIF
	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DEBUG")
		#ENDIF
		#ENDIF
	ENDWHILE
ENDSCRIPT
