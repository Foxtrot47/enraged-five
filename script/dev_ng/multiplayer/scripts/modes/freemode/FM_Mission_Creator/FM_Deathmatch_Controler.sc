//╔═════════════════════════════════════════════════════════════════════════════╗
//║		FM_Deathmatch_Controler													║
//║		Christopher Speirs	June 2011											║
//╚═════════════════════════════════════════════════════════════════════════════╝	

USING "globals.sch"
USING "shared_hud_displays.sch"
USING "net_spawn.sch"
USING "net_deathmatch.sch"
USING "freemode_header.sch"
USING "net_script_tunables.sch"		// KGM 25/7/12 - added so that Refresh_MP_Script_Tunables() can be called to gather Deathmatch specific values
#IF IS_DEBUG_BUILD
USING "profiler.sch"
USING "FMMC_ContentOverviewDebug.sch"
#ENDIF
USING "net_wait_zero.sch"

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				VARIABLES														║
//╚═════════════════════════════════════════════════════════════════════════════╝
SHARED_DM_VARIABLES dmVars 
INT iLoopParticipant
FMMC_SERVER_DATA_STRUCT serverBD
ServerBroadcastData serverBDdeathmatch
ServerBroadcastData_Leaderboard serverBD_Leaderboard
PlayerBroadcastData playerBD[MAX_NUM_DM_PLAYERS]
LEADERBOARD_PLACEMENT_TOOLS Placement

BLIMP_SIGN sBlimpSign

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				PROCS															║
//╚═════════════════════════════════════════════════════════════════════════════╝

PROC SERVER_SYNC_GLOBALS()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PRINTLN("[7719312] - SERVER_SYNC_GLOBALS")
		serverBD.iDuration 			 	= GlobalServerBD_DM.iDuration
		serverBD.iTarget			 	= GlobalServerBD_DM.iTarget		
		serverBD.iSpawnTime			 	= GlobalServerBD_DM.iSpawnTime			
		serverBD.iBlips		 		 	= GlobalServerBD_DM.iBlips		
		serverBD.iTags			 	 	= GlobalServerBD_DM.iTags			
		serverBD.iVoice		 		 	= GlobalServerBD_DM.iVoice		
		serverBD.iWeaponRespawnTime  	= GlobalServerBD_DM.iWeaponRespawnTime
		serverBD.iVehicleRespawnTime 	= GlobalServerBD_DM.iVehicleRespawnTime 
		serverBD.bIsTeam				= GlobalServerBD_DM.bIsTeamDM
	ENDIF
ENDPROC

PROC SERVER_INITIALISES_ALL_GLOBAL_DATA()
	DEBUG_PRINTCALLSTACK()
	PRINTLN("SERVER_INITIALISES_ALL_GLOBAL_DATA - Resetting GlobalServerBD_DM")
	PRINTLN("[7719312] - SERVER_INITIALISES_ALL_GLOBAL_DATA")
	GlobalServerBD_DM.iTypeOfDeathmatch		= -1
	GlobalServerBD_DM.iStartWeapon			= -1
	GlobalServerBD_DM.iNumberOfTeams 		= -1
	GlobalServerBD_DM.iBalanceTeams 		= -1
	GlobalServerBD_DM.iLocation 			= -1
	GlobalServerBD_DM.iDuration 			= -1
	GlobalServerBD_DM.iTarget				= -1		
	GlobalServerBD_DM.iWeapons 				= -1
	GlobalServerBD_DM.iTraffic 				= -1
	GlobalServerBD_DM.iPolice 				= -1
	GlobalServerBD_DM.iPeds					= -1
	GlobalServerBD_DM.iBlips				= -1
	GlobalServerBD_DM.iTags					= -1	
	GlobalServerBD_DM.iRadio	 			= -1
	GlobalServerBD_DM.iVoice				= -1
	GlobalServerBD_DM.iSpawnTime			= -1
	GlobalServerBD_DM.iTimeOfDay 			= -1
	GlobalServerBD_DM.iWeather 				= -1
	GlobalServerBD_DM.iWeaponRespawnTime	= -1
	GlobalServerBD_DM.iVehicleRespawnTime	= -1
	GlobalServerBD_DM.iVehicleDeathmatch	= -1
	GlobalServerBD_DM.iPlayerHealth			= -1
	GlobalServerBD_DM.iHealthBar			= -1
	GlobalServerBD_DM.iWeaponBlips			= -1
	
	GlobalServerBD_DM.bIsTeamDM 				= FALSE
	PRINTLN("[7719312] - SERVER_INITIALISES_ALL_GLOBAL_DATA GlobalServerBD_DM.bIsTeamDM set to FALSE")
	GlobalServerBD_DM.bResetDeathmatchOptions 	= FALSE
	#IF IS_DEBUG_BUILD
	GlobalServerBD_DM.bOnePlayerDeathmatch 		= FALSE
	#ENDIF
ENDPROC

PROC RESET_GlobalServerBD_DM()
	DEBUG_PRINTCALLSTACK()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		// THIS MAY BE TEMPORARY (Speirs 05/12/12)
		SERVER_INITIALISES_ALL_GLOBAL_DATA()
		PRINTLN("[7719312] - RESET_GlobalServerBD_DM HOST")
		PRINTLN("SERVER_INITIALISES_ALL_GLOBAL_DATA")
		PRINTLN("HOST RESET_GlobalServerBD_DM()")
	ELSE
		GlobalServerBroadcastDataDM GlobalServerBD_DM_RESET
		GlobalServerBD_DM = GlobalServerBD_DM_RESET
		PRINTLN("CLIENT RESET_GlobalServerBD_DM()")
		PRINTLN("[7719312] - RESET_GlobalServerBD_DM CLIENT")
	ENDIF
ENDPROC

//Resset the players PlayerBroadcastData
PROC RESET_PlayerBroadcastData()
	PlayerBroadcastData playerBBReset
	playerBD[PARTICIPANT_ID_TO_INT()] = playerBBReset
	PRINTLN("[RESET] RESET_PlayerBroadcastData ")
ENDPROC

//Reset all the ServerBroadcastData
PROC RESET_ServerBroadcastData()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBroadcastData serverBDReset 
		serverBDdeathmatch = serverBDReset
		PRINTLN("[RESET] RESET_ServerBroadcastData ")
	ENDIF
ENDPROC

//Do necessary pre game start ini.
//Returns FALSE if the script fails to receive its initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA &fmmcMissionData)
	
	serverBDdeathmatch.sServerFMMC_EOM.bAllowedToVote = FALSE
	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)			
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	
	SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(PLAYER_ID(), TRUE)
	
	SET_AMBIENT_ZONE_STATE("AZ_COUNTRY_MAJESTIC_QUARRY_PIT", FALSE, TRUE)
	
	FMMC_PROCESS_PRE_GAME_COMMON(fmmcMissionData, serverBD.iPlayerMissionToLoad, serverBD.iMissionVariation)
	
	//Cache Host and Particiant ID for use in this function
	PROCESS_CACHING_NETWORK_HOST()
	PROCESS_CACHING_LOCAL_PARTICIPANT_STATE(TRUE)

	INT iTeam = GlobalplayerBD_FM[iLocalPart].sClientCoronaData.iTeamChosen
	IF NOT IS_TEAM_DEATHMATCH()
		iTeam = 0
	ENDIF
	
	GlobalplayerBD_FM[iLocalPart].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
	
	IF bIsLocalPlayerHost
		serverBDdeathmatch.iMissionVariation = serverBD.iMissionVariation
	ENDIF
	
	#IF IS_DEBUG_BUILD
	g_sDM_SB_CoronaOptions.bOnePlayerDeathmatch = GlobalServerBD_DM.bOnePlayerDeathmatch
	dmVars.debugDmVars.bDontEndOnePlayer = GET_COMMANDLINE_PARAM_EXISTS("sc_AllowOnePlayerDeathmatch")
	#ENDIF
	
	// #1 Everyone resets menu globals 
	RESET_GlobalServerBD_DM()
	RESET_PlayerBroadcastData()
	RESET_ServerBroadcastData()
	
	PRINTLN("KEITHTEST A_AFTER_CLEAR")
//	DEBUG_PRINT_SPEW()
	
	// #2 Clients must register up front so that the initial data is consistent with the server
	IF NOT bIsLocalPlayerHost
		PRINTLN("KEITHTEST CLIENT_SETTING_DATA")
//		PRINT_CORONA_OPTIONS()
		PRINTLN("[7719312] - GlobalServerBD_DM = g_sDM_SB_CoronaOptions CLIENT")
		PRINTLN("[7719312] - Pre copy g_sDM_SB_CoronaOptions.bIsTeamDM: ", g_sDM_SB_CoronaOptions.bIsTeamDM)
		PRINTLN("[7719312] - Pre copy GlobalServerBD_DM.bIsTeamDM: ", GlobalServerBD_DM.bIsTeamDM)
		GlobalServerBD_DM = g_sDM_SB_CoronaOptions
		PRINTLN("[7719312] - Post copy g_sDM_SB_CoronaOptions.bIsTeamDM: ", g_sDM_SB_CoronaOptions.bIsTeamDM)
		PRINTLN("[7719312] - Post copy GlobalServerBD_DM.bIsTeamDM: ", GlobalServerBD_DM.bIsTeamDM)
		PRINTLN("KEITHTEST CLIENT_AFTER_COPY")
//		DEBUG_PRINT_SPEW()
	ENDIF
	
	RESET_SERVER_NEXT_JOB_VARS()
	
	//and it's not a rounds dm
	IF NOT IS_THIS_IS_A_ROUNDS_MISSION_INITIALISATION()
	AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds = 0
		RESET_ROUND_MISSION_DATA()
		RESET_ROUNDS_MISSION_CELEBRATION_GLOBALS()
		g_MissionControllerserverBD_LB.iHighestRankInSession = 0	
	ENDIF

	IF IS_THIS_A_ROUNDS_MISSION()
	AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed = 0
		RESET_ROUNDS_MISSION_CELEBRATION_GLOBALS()
		PRINTLN("PROCESS_PRE_GAME - RESET_ROUNDS_MISSION_CELEBRATION_GLOBALS called. g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed)
	ENDIF
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_DM, SIZE_OF(GlobalServerBD_DM), "GlobalServerBD_DM")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD), "serverBD")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBDdeathmatch, SIZE_OF(serverBDdeathmatch), "serverBDdeathmatch")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD_Leaderboard, SIZE_OF(serverBD_Leaderboard), "serverBD_Leaderboard")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(g_sMC_serverBDEndJob, SIZE_OF(g_sMC_serverBDEndJob), "g_sMC_serverBDEndJob")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(g_MissionControllerserverBD_LB, SIZE_OF(g_MissionControllerserverBD_LB), "g_MissionControllerserverBD_LB")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD), "playerBD")
	PRINTLN("[7719312] - Register BD")
	#IF IS_DEBUG_BUILD
	INT iSize = SIZE_OF(serverBDdeathmatch)
	PRINTLN("Size of serverBDdeathmatch: ", (iSize * 8))
	iSize = SIZE_OF(serverBD_Leaderboard)
	PRINTLN("Size of serverBD_Leaderboard: ", (iSize * 8))
	
	PRINTLN("PROCESS_PRE_GAME - GlobalServerBD_DM.bIsTeamDM: ", PICK_STRING(GlobalServerBD_DM.bIsTeamDM, "TRUE", "FALSE"))
	
	#ENDIF
			
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF SHOULD_JIP_SHOW_STAY_AS_SPECTATOR_SCREEN_BETWEEN_ROUNDS()
			SET_JIP_SHOW_STAY_AS_SPECTATOR_SCREEN_BETWEEN_ROUNDS(FALSE)
			PRINTLN("[LM][SPEC_SPEC][JIP] - PROCESS_PRE_GAME Hiding Menu. It's too late now.")
		ENDIF
		IF SHOULD_JIP_STAY_AS_SPECTATOR_BETWEEN_ROUNDS()
			PRINTLN("[LM][SPEC_SPEC][JIP] - PROCESS_PRE_GAME SHOULD_JIP_STAY_AS_SPECTATOR_BETWEEN_ROUNDS = TRUE")
		ELSE
			PRINTLN("[LM][SPEC_SPEC][JIP] - PROCESS_PRE_GAME SHOULD_JIP_STAY_AS_SPECTATOR_BETWEEN_ROUNDS = FALSE")
		ENDIF		
		IF GlobalplayerBD_FM[iLocalPart].sClientCoronaData.iTeamChosen = -1
		AND SHOULD_LOCAL_PLAYER_STAY_IN_ARENA_BOX_BETWEEN_ROUNDS()
		AND SHOULD_JIP_STAY_AS_SPECTATOR_BETWEEN_ROUNDS()
			PRINTLN("[LM][SPEC_SPEC][JIP] - PROCESS_PRE_GAME - GlobalplayerBD_FM[iLocalPart].sClientCoronaData.iTeamChosen = -1")
			SET_I_JOIN_MISSION_AS_SPECTATOR()
			SET_BIT(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_BALANCE_MY_TEAM)
			PUSH_ME_TO_MOST_APPROPREATE_TEAM_ONROUNDS_JIP()
		ENDIF
		SET_JIP_STAYS_AS_SPECTATOR_BETWEEN_ROUNDS(FALSE) // reset
	ENDIF
	
	IF SHOULD_SPECTATOR_BE_ROAMING_AT_START()
		PRINTLN("[DMC][SPEC_SPEC][PROCESS_PRE_GAME] SHOULD_SPECTATOR_BE_ROAMING_AT_START - Pausing Renderphase to prevent Arena Interior Pop in.")
		TOGGLE_PAUSED_RENDERPHASES(FALSE)
	ENDIF	
	
	PRINTLN("KEITHTEST B_AFTER_REGISTER")
//	DEBUG_PRINT_SPEW()

	// #3 Host needs to store the data after registration to ensure it gets broadcast to late joiners
	IF bIsLocalPlayerHost
		
		PRINTLN("KEITHTEST HOST_SETTING_DATA")
//		PRINT_CORONA_OPTIONS()
		PRINTLN("[7719312] - GlobalServerBD_DM = g_sDM_SB_CoronaOptions HOST")
		PRINTLN("[7719312] - Pre copy g_sDM_SB_CoronaOptions.bIsTeamDM: ", g_sDM_SB_CoronaOptions.bIsTeamDM)
		PRINTLN("[7719312] - Pre copy GlobalServerBD_DM.bIsTeamDM: ", GlobalServerBD_DM.bIsTeamDM)
		GlobalServerBD_DM = g_sDM_SB_CoronaOptions
		PRINTLN("[7719312] - Post copy g_sDM_SB_CoronaOptions.bIsTeamDM: ", g_sDM_SB_CoronaOptions.bIsTeamDM)
		PRINTLN("[7719312] - Post copy GlobalServerBD_DM.bIsTeamDM: ", GlobalServerBD_DM.bIsTeamDM)
		PRINTLN("KEITHTEST HOST_AFTER_COPY")
//		DEBUG_PRINT_SPEW()
		
	ENDIF
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	PRINTLN("KEITHTEST C_AFTER_FIRSTBROADCAST")
//	DEBUG_PRINT_SPEW()

//	MAKE_RIVAL_TEAMS_HATE_EACH_OTHER()
	
	// For Neil's spawning function
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		//SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), dmVars.rgFM_DEATHMATCH[PARTICIPANT_ID_TO_INT()])	//Put all players on default team to start with
	#IF IS_DEBUG_BUILD
	ELSE
		SCRIPT_ASSERT("Player is dead while trying to start deathmatch")
		PRINTLN("Player is dead while trying to start deathmatch ")
	#ENDIF
	ENDIF
	
	SET_FM_MISSION_LAUNCHED_SUCESS(fmmcMissionData.mdID.idCreator, fmmcMissionData.mdID.idVariation, fmmcMissionData.iInstanceId)
	
	SERVER_SYNC_GLOBALS()
	
	//changed by Brenda 05/03/2012
	//Clean up all the over head stuff if it is team DM. 
	IF IS_TEAM_DEATHMATCH()
		REFRESH_ALL_OVERHEAD_DISPLAY()
	ENDIF
	
	IF IS_KING_OF_THE_HILL()
		INIT_KING_OF_THE_HILL(playerBD, serverBDdeathmatch, serverBDdeathmatch.sKOTH_HostInfo, playerBD[PARTICIPANT_ID_TO_INT()].sKOTH_PlayerInfo, dmVars.sKOTH_LocalInfo)
	ENDIF
	
	LEGACY_INITIALISE_WORLD_PROPS()
	
	// 825058
	IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(<<-7000.0, -7000.0, -1000.0>>, <<7000.0, 7000.0, 1000.0>>)
		PRINTLN("ADD_SCENARIO_BLOCKING_AREA, deathmatch ")
		ADD_SCENARIO_BLOCKING_AREA(<<-7000.0, -7000.0, -1000.0>>, <<7000.0, 7000.0, 1000.0>>)
	ENDIF
	
	SET_AMBIENT_PEDS_DROP_MONEY(FALSE)
	
	PRINTLN("PROCESS_PRE_GAME FM_dm_controller ")
	INT iRank = GET_HIGHEST_TRANSITION_PLAYER_RANK() 
	IF bIsLocalPlayerHost
		IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds != 0
		AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
			PRINTLN("[TS] [MSROUND] - g_MissionControllerserverBD_LB.iHighestRankInSession = ", iRank)
			g_MissionControllerserverBD_LB.iHighestRankInSession = iRank
		ENDIF
		g_MissionControllerserverBD_LB.bDM = TRUE
	ENDIF
	SET_CHECK_RANK_CREATE_PICKUP_GLOBALS(iRank)
	
	
	SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_INI)
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		ACTIVATE_KILL_TICKERS(TRUE)
		PRINTLN("DM - PRE_GAME - SCTV - ACTIVATE_KILL_TICKERS - SET TO TRUE")
	ENDIF
	
	g_b_WastedShard_Eliminated_Not_Suicide = FALSE
	
	CLEAR_WAS_A_SPECIAL_SPECTATOR_IN_THIS_ROUND()
	
	ENABLE_COUNTERMEASURE_FOR_MISSION_VEHICLES(TRUE)
	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_LeaveEngineOnWhenExitingVehicles, FALSE)
	
	// 5433586
	IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
	AND GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_UNARMED 
		PRINTLN("[ARENA] Calling SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED) to force the HUD to not show the weapon UI")
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
	
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
		NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
		
		PRINTLN("[ARENA] Calling SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE) and NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE) because this Deathmatch is in the Arena")
			
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Disable_Arena_Mode)
			SET_IN_ARENA_MODE(TRUE)
			PREVENT_LOCAL_PLAYER_FALLING_OUT_WHEN_DEAD()
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_EnableVehicleShuntCombat)
				SET_VEHICLE_COMBAT_MODE(TRUE)
				PRINTLN("[ARENA][SHUNTING] Enabling vehicle combat")
			ENDIF
			
			PRINTLN("[ARENA][SHUNTING] Turning on 'Arena Mode'")
		ENDIF
		
		IF DOES_MISSION_HAVE_ANY_PLACED_TURRETS()
			ARENA_CONTESTANT_TURRET_STACK_CLEAR()
		ENDIF
		
		INIT_UGC_ARENA_PREGAME(dmVars.sTrapInfo_Local)
		CLEANUP_ARENA_ANNOUNCER_VARIABLES(TRUE)
		
		IF serverBDdeathmatch.iArena_Lighting > -1
		AND DID_I_JOIN_MISSION_AS_SPECTATOR()
			g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = serverBDdeathmatch.iArena_Lighting
			PRINTLN("[ARENA] Updating lighting to match serverBDdeathmatch.iArena_Lighting which is ", serverBDdeathmatch.iArena_Lighting)
		ENDIF
	ENDIF
	
	IF KOTH_SHOULD_I_SPAWN_IN_VEHICLE(iTeam)
		
		g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeam] = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.mnVehicleModel[iTeam])
		g_FMMC_STRUCT.iDefaultCommonVehicle[iTeam] = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(g_FMMC_STRUCT.mnVehicleModel[iTeam])
//		g_mnCoronaMissionVehicle = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeam], g_FMMC_STRUCT.iDefaultCommonVehicle[iTeam])
//		g_mnMyRaceModel = g_mnCoronaMissionVehicle
//		playerBD[PARTICIPANT_ID_TO_INT()].mnCoronaVehicle = g_mnMyRaceModel
//		PRINTLN("[KOTH][SPAWN VEH] g_mnCoronaMissionVehicle: ", GET_MODEL_NAME_FOR_DEBUG(g_mnCoronaMissionVehicle))
//		ASSERTLN("[KOTH][SPAWN VEH] g_mnCoronaMissionVehicle: ", GET_MODEL_NAME_FOR_DEBUG(g_mnCoronaMissionVehicle))
	ENDIF
	
	INIT_POWERUP_SYSTEM(dmVars.sPowerUps)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisablePIMenuKillYourself)
	OR NOT SHOULD_EXTRA_GAMEPLAY_FEATURES_RUN_IN_THIS_DM()
		DISABLE_KILL_YOURSELF_OPTION()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_DisableSnacks)
		BLOCK_SNACKS(TRUE)
	ENDIF
		
	RETURN TRUE
ENDFUNC

FUNC BOOL LOAD_IPLS_AND_INTERIORS(SHARED_DM_VARIABLES &dmVarsPassed)
		
	IF NOT LOAD_UGC_ARENA(g_ArenaInterior, dmVarsPassed.iArenaInterior_VIPLoungeIndex)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		IF FMMC_LOAD_ISLAND_IPLS()
			BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
		ELSE
			PRINTLN("LOAD_IPLS_AND_INTERIORS - Waiting on island")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Mission Overview Debug (6 Debug)
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_WEAPON_DAMAGE_MODIFIER_TEXT_FOR_DEBUG(INT iCurrentModSet, INT iWeaponGroup)
	
	IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iWeaponGroupDamageModifier[iWeaponGroup] = ciDM_WEAPON_DAMAGE_MODIFIER_ONE_HIT
		RETURN "One Hit Kill"
	ENDIF
	
	RETURN FLOAT_TO_STRING(TO_FLOAT(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].iWeaponGroupDamageModifier[iWeaponGroup])/100)
	
ENDFUNC
	
PROC PROCESS_DM_MODIFIER_STATE_DEBUG_WINDOW()
	
	IF NOT SET_UP_INITIAL_CONTENT_OVERVIEW_DEBUG_WINDOW_DIMENSIONS()
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tl63_Name
	
	sContentOverviewDebug.iDebugWindowPageMax[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 1
		
	IF NOT IS_BIT_SET(sContentOverviewDebug.iContentOverviewBS, ciContentOverviewBS_HideLegend)					
		// Draw Legend Wrapper 
		//DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE, 0, 1)	
		tl63_Name = "Rounds: "
		tl63_Name += g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds
		DRAW_LEGEND_DEBUG_WINDOW_TEXT(tl63_Name, sContentOverviewDebug.fBaseTextSize+0.01, FALSE)
	ENDIF
	
	INT iCurrentModSet = playerBD[PARTICIPANT_ID_TO_INT()].iCurrentModSet
	
	IF iCurrentModSet = -1
		EXIT
	ENDIF
	
	// Main Draw Loop -------------------------
	sContentOverviewDebug.iEntityStateNumberOfActive[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)] = 0
	INT i = 0
	FOR i = 0 TO 20
		FLOAT fTextSize = sContentOverviewDebug.fBaseTextSize	
		FLOAT fEntityListColumnSpacingBase = (sContentOverviewDebug.fRectSizeX / GET_NUMBER_OF_COLUMNS_FOR_ITEMS(sContentOverviewDebug.eDebugWindow))
		FLOAT fEntityListColumnSpacing = (fEntityListColumnSpacingBase * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow)))
		FLOAT fEntityListStartPosX = sContentOverviewDebug.fTextPosX
		FLOAT fEntityListStartPosY = sContentOverviewDebug.fTextPosY + 0.02
		
		FLOAT fEntityNamePosX = fEntityListStartPosX + fEntityListColumnSpacing
		FLOAT fEntityNamePosY = fEntityListStartPosY + (sContentOverviewDebug.fYSpacing * i) -  (sContentOverviewDebug.fYSpacing * (GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow) * (i / GET_NUMBER_ITEMS_FOR_COLUMNS(sContentOverviewDebug.eDebugWindow))))
		
		INT iCurrentRow
		iCurrentRow = 0
		
		sContentOverviewDebug.fBaseTextSize = 0.225
		
		SWITCH sContentOverviewDebug.iDebugWindowPage[ENUM_TO_INT(sContentOverviewDebug.eDebugWindow)]
			CASE 0
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Current Modifier Set: "
					tl63_Name += iCurrentModSet
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Pending Modifier Set: "
					tl63_Name += playerBD[PARTICIPANT_ID_TO_INT()].iPendingModSet
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Pending Modifier Set Blocker BS: "
					tl63_Name += dmVars.sPlayerModifierData.iPendingApplyBlockerBS
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					sContentOverviewDebug.fBaseTextSize = 0.3
					tl63_Name = "Weapon Damage Modifiers -------------"
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Current Weapon Modifier Damage: "
					WEAPON_TYPE wtWeapon
					wtWeapon = WEAPONTYPE_INVALID
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeapon)
						tl63_Name += FLOAT_TO_STRING(GET_WEAPON_DAMAGE_MODIFIER(wtWeapon))
					ENDIF
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Pistol Damage Modifier: "
					tl63_Name += GET_WEAPON_DAMAGE_MODIFIER_TEXT_FOR_DEBUG(iCurrentModSet, ciDM_WEAPON_GROUP_MODIFIER_PISTOL)
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "SMG Damage Modifier: "
					tl63_Name += GET_WEAPON_DAMAGE_MODIFIER_TEXT_FOR_DEBUG(iCurrentModSet, ciDM_WEAPON_GROUP_MODIFIER_SMG)
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Assault Rifle Damage Modifier: "
					tl63_Name += GET_WEAPON_DAMAGE_MODIFIER_TEXT_FOR_DEBUG(iCurrentModSet, ciDM_WEAPON_GROUP_MODIFIER_AR)
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Machine Gun Damage Modifier: "
					tl63_Name += GET_WEAPON_DAMAGE_MODIFIER_TEXT_FOR_DEBUG(iCurrentModSet, ciDM_WEAPON_GROUP_MODIFIER_MG)
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Sniper Damage Modifier: "
					tl63_Name += GET_WEAPON_DAMAGE_MODIFIER_TEXT_FOR_DEBUG(iCurrentModSet, ciDM_WEAPON_GROUP_MODIFIER_SNIPER)
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Melee Damage Modifier: "
					tl63_Name += GET_WEAPON_DAMAGE_MODIFIER_TEXT_FOR_DEBUG(iCurrentModSet, ciDM_WEAPON_GROUP_MODIFIER_MELEE)
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Shotgun Damage Modifier: "
					tl63_Name += GET_WEAPON_DAMAGE_MODIFIER_TEXT_FOR_DEBUG(iCurrentModSet, ciDM_WEAPON_GROUP_MODIFIER_SHOTGUN)
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Heavy Damage Modifier: "
					tl63_Name += GET_WEAPON_DAMAGE_MODIFIER_TEXT_FOR_DEBUG(iCurrentModSet, ciDM_WEAPON_GROUP_MODIFIER_HEAVY)
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Throwable Damage Modifier: "
					tl63_Name += GET_WEAPON_DAMAGE_MODIFIER_TEXT_FOR_DEBUG(iCurrentModSet, ciDM_WEAPON_GROUP_MODIFIER_THROWABLE)
				ENDIF
				
				//Column 2
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					sContentOverviewDebug.fBaseTextSize = 0.3
					tl63_Name = "Timer Condition -------------"
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Next Timer Refresh: "
					tl63_Name += dmVars.sPlayerModifierData.iNextTimeToRefresh
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Timer Condition Timestamp: "
					tl63_Name += dmVars.sPlayerModifierData.iTimerConditionTimeStamp
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Elapsed Time: "
					tl63_Name += (NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())) - dmVars.sPlayerModifierData.iTimerConditionTimeStamp
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					sContentOverviewDebug.fBaseTextSize = 0.3
					tl63_Name = "Contextual Help -------------"
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Required Help Text: "
					tl63_Name += iContextHelpTextRequiredBS
				ENDIF
				
				IF SHOULD_STATE_DEBUG_WINDOW_ROW_BE_DRAWN_YET(iCurrentRow, i)
					tl63_Name = "Shown Help: "
					tl63_Name += iContextHelpTextShownBS
				ENDIF
				
			BREAK			
		ENDSWITCH
			
		DRAW_ENTITY_DEBUG_WINDOW_TEXT(tl63_Name, fEntityNamePosX, fEntityNamePosY, fTextSize)	
	ENDFOR
ENDPROC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_MAIN_FOR_SPECIFIC_CONTENT()
	  	
	SET_DEFAULT_DEBUG_WINDOW_FOR_SCRIPT(CONTENT_OVERVIEW_DEBUG_WINDOW_DM_MODIFIERS)
	
	SWITCH sContentOverviewDebug.eDebugWindow
		CASE CONTENT_OVERVIEW_DEBUG_WINDOW_DM_MODIFIERS
			PROCESS_DM_MODIFIER_STATE_DEBUG_WINDOW()
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT()
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Debug Processing Function
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_DM_DEBUG()
	
	PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW(&PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_MAIN_FOR_SPECIFIC_CONTENT, &PROCESS_CONTENT_OVERVIEW_DEBUG_WINDOW_CLICK_EVENT)
	PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN(sContentOverviewDebug.eDebugWindow = CONTENT_OVERVIEW_DEBUG_WINDOW_NONE)
	
ENDPROC

#ENDIF

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				MAIN LOOP														║
//╚═════════════════════════════════════════════════════════════════════════════╝

SCRIPT(MP_MISSION_DATA fmmcMissionData)
	
	PRINTLN("------------------------------------------------------------------------------")
	PRINTLN("--                                                                          --")
	PRINTLN("--                   DM_CONTROLLER LAUNCHED!!!11                            --")
	PRINTLN("--                                                                          --")
	PRINTLN("------------------------------------------------------------------------------")
	
	// Carry out all the initial game starting duties. 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(fmmcMissionData)
			PRINTLN("FAILED TO RECEIVE INITIAL NETWORK BROADCAST - Cleaning up.")
			SCRIPT_CLEANUP(serverBDdeathmatch, serverBD, playerBD,  dmVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED, serverBD_Leaderboard)
		ENDIF
	ENDIF
	
	// KGM 25/7/12: Load Tunable Script Variable values from the cloud for Freemode Deathmatches
//	Refresh_MP_Script_Tunables(TUNE_CONTEXT_FM_DM)

//	// Store loadout
//	PRINTLN(" GET_PED_WEAPONS_MP, DM_CONTROLLER")
//	GET_PED_WEAPONS_MP(PLAYER_PED_ID(), dmVarsNoReset.structWeaponInfo)

	#IF IS_DEBUG_BUILD
	CREATE_DM_WIDGETS(serverBDdeathmatch, playerBD, dmVars, serverBD_Leaderboard)
	#ENDIF
	
	WHILE TRUE
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF			
				
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_teamRestrictSpwnDMdbg")
			INT iss
			FOR iss = 0 TO FMMC_MAX_SPAWNPOINTS - 1
				TEXT_LABEL_63 tlDebugText3D = "Bitset: "
				tlDebugText3D += g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iss].iTeamRestrictedBS
				DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[iss].vPos)
			ENDFOR
		ENDIF
		
		//Home key requests to be the host of the mission controller script
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
			IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
				NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
				PRINTLN("[DM] Requesting to be the host of this script!!")
			ENDIF
		ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF
		
		PROCESS_PRE_FRAME_LOCAL_PLAYER_CACHING()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_PRE_FRAME_LOCAL_PLAYER_CACHING")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		PROCESS_PAUSED_TIMERS(serverBDdeathmatch)
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_PAUSED_TIMERS")
		#ENDIF
		#ENDIF
		
		MAINTAIN_FROZEN_VEHICLE(serverBDdeathmatch, dmVars, playerBD)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FROZEN_VEHICLE")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		PROCESS_DM_DEBUG()
		#ENDIF
		
		IF CONTENT_IS_USING_ARENA()
		OR IS_THIS_VEHICLE_DEATHMATCH(serverBDdeathmatch)
		OR DOES_PLAYER_HAVE_ACTIVE_SPAWN_VEHICLE_MODIFIER(playerBD[PARTICIPANT_ID_TO_INT()].iCurrentModSet, dmVars.sPlayerModifierData)
			CHECK_VEHICLE_STUCK_TIMERS(serverBDdeathmatch)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("CHECK_VEHICLE_STUCK_TIMERS")
			#ENDIF
			#ENDIF
		ENDIF
		
		CONTROL_RADAR_ZOOM(serverBDdeathmatch, dmVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_RADAR_ZOOM")
		#ENDIF
		#ENDIF
		
		HIDE_AREA_NAMES_BOTTOM_RIGHT(serverBDdeathmatch)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("HIDE_AREA_NAMES_BOTTOM_RIGHT")
		#ENDIF
		#ENDIF
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[NET_DEATHMATCH] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
			
			// url:bugstar:5452493
			IF CONTENT_IS_USING_ARENA()
			AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
				HANDLE_END_OF_ARENA_EVENT_PLAYER_CAREER_REWARDS(0, IS_THIS_TEAM_DEATHMATCH(serverBDdeathmatch), TRUE, DEFAULT, DEFAULT, DEFAULT, serverBDdeathmatch.iNumberOfTeams)
			ENDIF
			
			SCRIPT_CLEANUP(serverBDdeathmatch, serverBD, playerBD,  dmVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED, serverBD_Leaderboard)
		ENDIF
		
		IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BITSET_ROAMING_SPECTATOR)
				PRINTLN("[DM][SPEC_SPEC] Setting PBBOOL4_ROAMING_SPECTATOR")
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BITSET_ROAMING_SPECTATOR)
			ENDIF
		ELSE
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BITSET_ROAMING_SPECTATOR)
				PRINTLN("[DM][SPEC_SPEC] Clearing PBBOOL4_ROAMING_SPECTATOR")
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iClientBitSet, CLIENT_BITSET_ROAMING_SPECTATOR)
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE")
		#ENDIF
		#ENDIF
		
		INT iSlot = NATIVE_TO_INT(PLAYER_ID())
		IF iSlot <> -1
			IF IS_BIT_SET(GlobalplayerBD_FM[iSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob) = FALSE
			OR IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			OR TRANSITION_SESSIONS_QUIT_CURRENT_PLAYLIST()
				IF SHOULD_PLAYER_LEAVE_MP_MISSION(TRUE)
					PRINTLN("SHOULD_PLAYER_LEAVE_MP_MISSION")
					SCRIPT_CLEANUP(serverBDdeathmatch, serverBD, playerBD,  dmVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED, serverBD_Leaderboard)
				ENDIF
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SCRIPT_CLEANUP checks")
		#ENDIF
		#ENDIF
		
		PROCESS_DEATHMATCH_EVENTS(serverBDdeathmatch, serverBD_Leaderboard, serverBD, playerBD, dmVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_DEATHMATCH_EVENTS")
		#ENDIF
		#ENDIF			
		
		SAVE_THUMB_VOTE(playerBD, dmVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SAVE_THUMB_VOTE")
		#ENDIF
		#ENDIF
		
		MAINTAIN_RACE_EVENT_NETWORK_CONNECTION_TIMEOUTS(dmVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_RACE_EVENT_NETWORK_CONNECTION_TIMEOUTS")
		#ENDIF
		#ENDIF
			
		IF NETWORK_IS_GAME_IN_PROGRESS()

			//This now has to happen every frame otherwise it will not properly ensure the correct traffic state on the server or client
			FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME(GET_DM_PED_DENSITY(), 0.0, 0.0, GET_TRAFFIC_DENSITY_FOR_DM(serverBDdeathmatch), GET_TRAFFIC_DENSITY_FOR_DM(serverBDdeathmatch), GET_PARKED_TRAFFIC_DENSITY_FOR_DM(serverBDdeathmatch), GET_TRAFFIC_DENSITY_FOR_DM(serverBDdeathmatch),IS_TRAFFIC_DISABLED_FOR_DM(serverBDdeathmatch))
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME")
			#ENDIF
			#ENDIF
			g_bMissionClientGameStateRunning = FALSE
//╔═════════════════════════════════════════════════════════════════════════════╗
//║			CLIENT																║
//╚═════════════════════════════════════════════════════════════════════════════╝	
			SWITCH GET_CLIENT_GAME_STATE(playerBD, PARTICIPANT_ID_TO_INT())
							
				// Wait until the server gives the all go before moving on.
				CASE GAME_STATE_INI
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					IF LOAD_DM_TEXT_BLOCK()
						IF GET_SERVER_GAME_STATE(serverBDdeathmatch) > GAME_STATE_INI	
							IF serverBDdeathmatch.iDeathmatchType <> -1
								IF IS_NET_PLAYER_OK(PLAYER_ID())
									IF LOAD_IPLS_AND_INTERIORS(dmVars)
									AND LOAD_DM_SOUNDS()
										BOOL bAllLoaded
										bAllLoaded = TRUE
										IF CONTENT_IS_USING_ARENA()
											IF NOT LOAD_AND_INITIALIZE_ARENA_ANNOUNCER()
												PRINTLN("[RACE][LM-JT][ArenaAnnouncer][GAME_STATE_INI] - bAllLoaded = FALSE")
												bAllLoaded = FALSE
											ENDIF
										ENDIF										
										IF bAllLoaded
											SET_ALL_CLIENTS_RECEIVE_INVALID_WEAPON_DAMAGE_EVENTS()
											CLIENT_CLEAR_WHOLE_MAP_AREA()
											SET_INVINCIBLE_START_OF_JOB(TRUE)
		//									Clear_All_Generated_MP_Headshots(FALSE) // Stop players having wrong headshot in deathmatch leaderboard.
											SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_RUNNING)											
										ENDIF
									ELSE
										PRINTLN("DM - Loading interiors still...")
									ENDIF
									
								ELSE
									PRINTLN("DM - TRANSITION TIME - F-(", GET_FRAME_COUNT(), ") S(", GET_CLOUD_TIME_AS_INT(), ")  IS_NET_PLAYER_OK")
									PRINTLN("Stuck IS_NET_PLAYER_OK   ")
								ENDIF
							ELSE
								PRINTLN("serverBDPassed.iDeathmatchType = -1, add PT to Bobby ")
							ENDIF
						ELSE
							PRINTLN("DM - TRANSITION TIME - F-(", GET_FRAME_COUNT(), ") S(", GET_CLOUD_TIME_AS_INT(), ") GET_SERVER_GAME_STATE(serverBDdeathmatch) > GAME_STATE_INI	")
							PRINTLN("Stuck SERVER_GAME_STATE =      ", GET_SERVER_GAME_STATE(serverBDdeathmatch), "     ")
							PRINTLN("HOST_IS_CALLED ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())) )  
						ENDIF
					ELSE
						PRINTLN("DM - TRANSITION TIME - F-(", GET_FRAME_COUNT(), ") S(", GET_CLOUD_TIME_AS_INT(), ") LOAD_DM_TEXT_BLOCK")
						PRINTLN("Stuck LOAD_DM_TEXT_BLOCK ")
					ENDIF
					
					// Loop through everyone
					MAINTAIN_DEATHMATCH_PLAYER_LOOP(serverBDdeathmatch, playerBD, dmVars)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DEATHMATCH_PLAYER_LOOP")
					#ENDIF
					#ENDIF
				BREAK
				
				// Main gameplay state.
				CASE GAME_STATE_RUNNING	
					g_bMissionClientGameStateRunning = TRUE
					PROCESS_CLIENT_DEATHMATCH(serverBD, Placement, serverBDdeathmatch, playerBD, dmVars, serverBD_Leaderboard)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("PROCESS_CLIENT_DEATHMATCH")
					#ENDIF
					#ENDIF
						
					INT iPartLoop
					REPEAT g_FMMC_STRUCT.iNumberOfTeams iPartLoop
						PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND g_FMMC_STRUCT.iNumPlayersPerTeam[", iPartLoop, "]: ", g_FMMC_STRUCT.iNumPlayersPerTeam[iPartLoop])
					ENDREPEAT
					
					// Loop through everyone
					MAINTAIN_DEATHMATCH_PLAYER_LOOP(serverBDdeathmatch, playerBD, dmVars)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DEATHMATCH_PLAYER_LOOP")
					#ENDIF
					#ENDIF
					
					DO_BLIMP_SIGNS(sBlimpSign, FALSE)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("DO_BLIMP_SIGNS")
					#ENDIF
					#ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciCLEAR_UP_VEHICLE_ON_GAME_LEAVE)
						DELETE_ANY_EMPTY_NEARBY_VEHICLES_SHARED(FALSE, TRUE)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("DELETE_ANY_EMPTY_NEARBY_VEHICLES_SHARED")
						#ENDIF
						#ENDIF
					ENDIF
					
					// -----------------------------------	DEBUGGERY ----------------------------------------------------------------------
					#IF IS_DEBUG_BUILD
						HOLD_NUM_7_TO_PRINT_CLIENT_STAGE(playerBD)
					#ENDIF
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("UPDATE_DEBUG_SKIPS")
					#ENDIF
					#ENDIF
					// ---------------------------------------------------------------------------------------------------------------------
				BREAK	
				
				CASE GAME_STATE_LEAVE			
					SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_END)
					DO_BLIMP_SIGNS(sBlimpSign, TRUE)
				FALLTHRU

				CASE GAME_STATE_END		
					SCRIPT_CLEANUP(serverBDdeathmatch, serverBD, playerBD,  dmVars, Placement, ciFMMC_END_OF_MISSION_STATUS_PASSED, serverBD_Leaderboard)
					DO_BLIMP_SIGNS(sBlimpSign, TRUE)
				BREAK
				
				DEFAULT 
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("DM: Problem in SWITCH GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT())") 
					#ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
//╔═════════════════════════════════════════════════════════════════════════════╗
//║		SERVER																	║
//╚═════════════════════════════════════════════════════════════════════════════╝	
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
				BOOL bCoronaReady = TRUE
				IF IS_PLAYER_IN_CORONA()
					bCoronaReady = IS_CORONA_READY_TO_START_WITH_JOB()
				ENDIF
			
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
				OPEN_SCRIPT_PROFILE_MARKER_GROUP("server stuuff") 
				#ENDIF
				#ENDIF
			
				#IF IS_DEBUG_BUILD
					NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER stuff being called by machine that is not host! Host only command!")
				#ENDIF
				
				SERVER_SETS_BIT_WHEN_TEAMS_COUNTED(serverBDdeathmatch)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_BIT_WHEN_TEAMS_COUNTED")
				#ENDIF
				#ENDIF
				
				iLoopParticipant += 1			
				IF (iLoopParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS())
					iLoopParticipant = 0 	
				ENDIF
				
				IF bCoronaReady
					SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT(serverBD_Leaderboard.leaderboard, serverBDdeathmatch, playerBD, iLoopParticipant #IF IS_DEBUG_BUILD , dmVars #ENDIF )										
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT")
					#ENDIF
					#ENDIF
					
					SERVER_CHOOSE_ARENA_ANIMATIONS(serverBDdeathmatch.sCelebServer, GET_CELEB_TYPE(serverBDdeathmatch), dmVars.sCelebrationData.iNumPlayerFoundForWinnerScene)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_CHOOSE_ARENA_ANIMATIONS")
					#ENDIF
					#ENDIF
				ENDIF
					
				SERVER_STORES_FINISH_TIME(serverBDdeathmatch)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("SERVER_STORES_FINISH_TIME")
				#ENDIF
				#ENDIF
											
				SWITCH GET_SERVER_GAME_STATE(serverBDdeathmatch)
					
					CASE GAME_STATE_INI								
						IF HAVE_ALL_CORONA_PLAYERS_JOINED(dmVars.timeCoronaFailSafe)
							IF SHOULD_LONE_SPECTATOR_CLEAN_UP(dmVars.timeCoronaFailSafe)
								PRINTLN("[CS_DM] [HAVE_ALL_CORONA_PLAYERS_JOINED], SHOULD_LONE_SPECTATOR_CLEAN_UP, SCRIPT_CLEANUP ")
								SCRIPT_CLEANUP(serverBDdeathmatch, serverBD, playerBD,  dmVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED, serverBD_Leaderboard)
							ELSE
								CLIENT_CLEAR_WHOLE_MAP_AREA()
								IF INIT_SERVER_DATA(serverBDdeathmatch, serverBD)
									SERVER_SET_PED_AND_TRAFFIC_DENSITIES(serverBDdeathmatch.iPopulationHandle, GET_DM_PED_DENSITY(), GET_TRAFFIC_DENSITY_FOR_DM(serverBDdeathmatch))
									PRINTLN("SET_SERVER_GAME_STATE(serverBDdeathmatch, GAME_STATE_RUNNING)")
									
									SET_SERVER_GAME_STATE(serverBDdeathmatch, GAME_STATE_RUNNING)
								ELSE
									PRINTLN("[DM] Waiting on INIT_SERVER_DATA")
								ENDIF
							ENDIF
						ELSE
							PRINTLN("Failing on HAVE_ALL_CORONA_PLAYERS_JOINED")
						ENDIF
					BREAK
					
					CASE GAME_STATE_RUNNING		
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
						OPEN_SCRIPT_PROFILE_MARKER_GROUP("GAME_STATE_RUNNING") 
						#ENDIF
						#ENDIF
						
						// Make vehicles etc.
						IF bCoronaReady
							SERVER_CREATE_DM_ENTITIES(serverBDdeathmatch, serverBD, dmVars)
							#IF IS_DEBUG_BUILD
							#IF SCRIPT_PROFILER_ACTIVE 
								ADD_SCRIPT_PROFILE_MARKER("SERVER_CREATE_DM_ENTITIES")
							#ENDIF
							#ENDIF
							
							SERVER_GRABS_AVERAGE_RANK_OF_DEATHMATCH_PLAYERS(serverBDdeathmatch)
							#IF IS_DEBUG_BUILD
							#IF SCRIPT_PROFILER_ACTIVE 
								ADD_SCRIPT_PROFILE_MARKER("SERVER_GRABS_AVERAGE_RANK_OF_DEATHMATCH_PLAYERS")
							#ENDIF
							#ENDIF
						ENDIF
						
						PROCESS_DM_RESPAWN_ENTITITES(serverBD, serverBDdeathmatch, serverBDdeathmatch.sKOTH_HostInfo, dmVars)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("PROCESS_RESPAWN_ENTITITES")
						#ENDIF
						#ENDIF
						
						SERVER_SETS_BRUCIE_BOX_COORDS(serverBDdeathmatch, dmVars)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_BRUCIE_BOX_COORDS")
						#ENDIF
						#ENDIF
						
						// TIME
						// -----------------------------------------------------------------------------------------------//
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DM_321GO_Countdown)
							START_DM_TIMER(serverBDdeathmatch)	
						ENDIF
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("START_DM_TIMER")
						#ENDIF
						#ENDIF
						
						#IF IS_DEBUG_BUILD
						SERVER_DEBUG_TIME_DOWN(serverBDdeathmatch)	
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_DEBUG_TIME_DOWN")
						#ENDIF
						#ENDIF
						#ENDIF
						
						//  Track winning team
						// -----------------------------------------------------------------------------------------------//
						SERVER_SORT_WINNING_TEAMS(serverBDdeathmatch, playerBD #IF IS_DEBUG_BUILD , dmVars #ENDIF )
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_SORT_WINNING_TEAMS")
						#ENDIF
						#ENDIF
						
						SERVER_ASSIGN_WINNING_TEAMS(serverBDdeathmatch)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_ASSIGN_WINNING_TEAMS")
						#ENDIF
						#ENDIF
						
						SERVER_SETS_EVERYONE_BETTING_BIT(serverBDdeathmatch, playerBD)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_EVERYONE_BETTING_BIT")
						#ENDIF
						#ENDIF
						
						// Server hands out spawn tickets
						SERVER_SPAWN_TICKETS(serverBDdeathmatch, playerBD)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_SPAWN_TICKETS")
						#ENDIF
						#ENDIF
						
						serverBDdeathmatch.bAllPlayersHaveAFinishTime = DO_ALL_ACTIVE_PARTICIPANTS_HAVE_A_FINISH_TIME(playerBD)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("DO_ALL_ACTIVE_PARTICIPANTS_HAVE_A_FINISH_TIME")
						#ENDIF
						#ENDIF
						
						IF bCoronaReady
							SERVER_PROCESSING(serverBDdeathmatch, playerBD, dmVars #IF IS_DEBUG_BUILD , TRUE #ENDIF, serverBD_Leaderboard)
							#IF IS_DEBUG_BUILD
							#IF SCRIPT_PROFILER_ACTIVE 
								ADD_SCRIPT_PROFILE_MARKER("SERVER_PROCESSING")
							#ENDIF
							#ENDIF
							
							PROCESS_SERVER_STORE_CACHE_SPECIFIC_PLAYER_POSITIONS(serverBDdeathmatch, serverBD_Leaderboard)
							#IF IS_DEBUG_BUILD
							#IF SCRIPT_PROFILER_ACTIVE 
								ADD_SCRIPT_PROFILE_MARKER("PROCESS_SERVER_STORE_CACHE_SPECIFIC_PLAYER_POSITIONS")
							#ENDIF
							#ENDIF
							
							// Manage power plays
							SERVER_POWER_PLAYS(serverBDdeathmatch, playerBD, serverBD_Leaderboard)
							#IF IS_DEBUG_BUILD
							#IF SCRIPT_PROFILER_ACTIVE 
								ADD_SCRIPT_PROFILE_MARKER("SERVER_POWER_PLAYS")
							#ENDIF
							#ENDIF
							
							
						ENDIF
						
						SERVER_DOES_RESTART_STUFF(serverBDdeathmatch, playerBD, dmVars)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_DOES_RESTART_STUFF")
						#ENDIF
						#ENDIF
						
						SERVER_SETS_ONE_PLAYER_LEFT_BIT(serverBDdeathmatch #IF IS_DEBUG_BUILD , dmVars #ENDIF)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_ONE_PLAYER_LEFT_BIT")
						#ENDIF
						#ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_DM_TAGGED_EXPLOSION_TIMER)
						AND HAS_DM_STARTED(serverBDdeathmatch)
						AND NOT HAS_DEATHMATCH_FINISHED(serverBDdeathmatch)
							PROCESS_PASSTHEBOMB_HOST(serverBDdeathmatch, dmVars, playerBD)
						ENDIF
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("PROCESS_PASSTHEBOMB_HOST")
						#ENDIF
						#ENDIF
						
						IF IS_KING_OF_THE_HILL()
						AND HAS_DM_STARTED(serverBDdeathmatch)
						AND NOT HAS_DEATHMATCH_FINISHED(serverBDdeathmatch)
							PROCESS_KING_OF_THE_HILL_HOST(serverBD, serverBDdeathmatch, serverBDdeathmatch.sKOTH_HostInfo, dmVars.sKOTH_LocalInfo, playerBD, NOT IS_TEAM_DEATHMATCH())
						ENDIF
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("PROCESS_KING_OF_THE_HILL_HOST")
						#ENDIF
						#ENDIF
						
						IF CONTENT_IS_USING_ARENA()
							SERVER_STORE_CELEB_WINNER_VEHICLE(serverBDdeathmatch, playerBD)
						ENDIF
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_STORE_CELEB_WINNER_VEHICLE")
						#ENDIF
						#ENDIF
												
						 //#IF FEATURE_ARENA_WARS
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
						CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
						#ENDIF
						#ENDIF
						
					BREAK
					
					// Shouldn't ever get in here
					CASE GAME_STATE_END		
					

					BREAK	
					
					DEFAULT 
						#IF IS_DEBUG_BUILD
							SCRIPT_ASSERT("DM: Problem in SWITCH GET_SERVER_GAME_STATE()") 
						#ENDIF
					BREAK
				ENDSWITCH
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("processing server game state")
				#ENDIF
				#ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
				CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
				#ENDIF
				#ENDIF
				
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			iPrintNum = 0
		#ENDIF
		
		PROCESS_INSTANCED_CONTENT_FADE_SCREEN_OUT_FOR_PERIOD_OF_TIME()
		
		IF CONTENT_IS_USING_ARENA()
			PROCESS_TRAP_CAM_TRAP_BLIPS(dmVars.biTrapCamBlip)
			PROCESS_SPECIAL_SPECTATOR_MODES_FOR_INSTANCED_CONTENT()
			PROCESS_TIME_REMAINING_CACHE(serverBDdeathmatch)
		ENDIF
		
		IF CONTENT_IS_USING_ARENA()

			IF IS_SKYSWOOP_AT_GROUND()
				PROCESS_ARENA_ANNOUNCER_EVERY_FRAME()
			ENDIF
			
			PROCESS_UGC_ARENA(dmVars.sArenaSoundsInfo, GET_CLIENT_GAME_STATE(playerBD, PARTICIPANT_ID_TO_INT()) <= GAME_STATE_RUNNING, serverBDdeathmatch.iArena_Lighting)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("Server processing")
		#ENDIF
		#ENDIF		
		
	ENDWHILE
ENDSCRIPT
