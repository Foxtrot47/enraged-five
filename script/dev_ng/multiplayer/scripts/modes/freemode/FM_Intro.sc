//////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FM_INTRO.sc																	//
// Description: Intro to Freemode for new players. 										 	//
// Written by:  David Watson																//
// Date: 08/10/2012																			//
//////////////////////////////////////////////////////////////////////////////////////////////


USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_fire.sch"
USING "building_control_public.sch"
USING "cutscene_public.sch"
USING "commands_streaming.sch"

// Network Headers
//USING "net_include.sch"
USING "net_events.sch"
USING "MP_SkyCam.sch"
USING "net_spawn.sch"


USING "net_mission.sch"
USING "net_scoring_common.sch"

USING "net_ambience.sch"
USING "net_rank_ui.sch"
USING "net_hud_activating.sch"
USING "selector_public.sch"
USING "help_at_location.sch"
USING "net_objective_text.sch"
USING "net_cutscene.sch"

USING "carmod_shop_private.sch"

USING "freemode_header.sch"
USING "net_mission_locate_message.sch"

//USING "net_freemode_cut.sch"
USING "net_fm_intro_cut.sch"
USING "NET_TAXI.sch"

USING "cheat_handler.sch"

USING "net_wait_zero.sch"

USING "net_cash_transactions.sch"

//USING "traffic_default_values.sch" // this must be included before traffic.sch 

/*
// total should not exceed 225
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					62
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					1	
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				5	

// total should not exceed 12
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK 		8   
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		4					

// this should be fine
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		6

USING "traffic.sch"

*/
#IF IS_DEBUG_BUILD
	USING "net_debug_log.sch"
#ENDIF


CONST_INT NUM_MISSION_PLAYERS	2

MP_MISSION thisMission = eFM_INTRO
SHOP_NAME_ENUM shopAmmu = GUN_SHOP_06_LS //GUN_SHOP_01_DT

FM_CUT_STRUCT FmIntroCutscene

#IF IS_DEBUG_BUILD
//CF_PASSIVE_CUT_STRUCT cfPassiveCut
#ENDIF

ENUM CUTSCENE_PROG
	CUTSCENE_INIT,
	CUTSCENE_LOAD,
	CUTSCENE_FIRST_CAM,
	CUTSCENE_FIRST_HELP,
	CUTSCENE_WAIT_FOR_HELP,
	CUTSCENE_STAGE_SKIP,
	CUSTCENE_FINISH,
	CUTSCENE_DONE
ENDENUM

SCRIPT_TIMER timeIntro
//SCRIPT_TIMER timeTrigTut
SCRIPT_TIMER timePhoncallTimeout
SCRIPT_TIMER timeAllowTaxiSkip
SCRIPT_TIMER timeTaxiSetOff
SCRIPT_TIMER timeForceSkipTaxiJourney
//SCRIPT_TIMER timeLeaveTaxi
SCRIPT_TIMER timeTaxiSpeech
structPedsForConversation sSpeech

// Game States
CONST_INT GAME_STATE_INI 		0
CONST_INT GAME_STATE_INI_SPAWN	1
CONST_INT GAME_STATE_RUNNING	2
CONST_INT GAME_STATE_LEAVE		3
CONST_INT GAME_STATE_FAILED		4
CONST_INT GAME_STATE_TERMINATE_DELAY 5
CONST_INT GAME_STATE_END		6

// Mission States
CONST_INT FM_INTRO_RUN_CUTSCENE				-2
CONST_INT FM_INTRO_FADE_IN					-1
CONST_INT FM_INTRO_STATE_GET_TO_AMMU		0
CONST_INT FM_INTRO_STATE_GET_TO_SHOOT		1
CONST_INT FM_INTRO_STATE_TRIG_TUT_CUT		2
CONST_INT FM_INTRO_STAGE_LEAVE_AMMU			3

CONST_INT FM_INTRO_STATE_FINISHED			99

CONST_INT RACE_WARP_LEAVE_GUNSHOP			0
CONST_INT RACE_WARP_SEND_TEXT				1
CONST_INT RACE_WARP_MESSAGE_END				2
CONST_INT RACE_WARP_PHONE_AWAY				3
CONST_INT RACE_WARP_START_WARP				4
CONST_INT RACE_WARP_TO_START				5
CONST_INT RACE_WARP_FINISHED				6

CONST_INT biS_DebugSkipped					0
CONST_INT biS_PlayerGotWeapon				1
CONST_INT biS_PlayerDoneCut					2
CONST_INT biS_CreateTaxi					3
CONST_INT biS_PlayerInTaxi					4
CONST_INT biS_SkipTaxi						5
CONST_INT biS_WarpedTaxi					6
CONST_INT biS_TaxiArrived					7
CONST_INT biS_PlayerOutTaxi					8

CONST_INT TAXI_STAGE_CREATE						0
CONST_INT TAXI_STAGE_DRIVE_TO_DEST				1
CONST_INT TAXI_STAGE_PLAYER_INTO_TAXI			2
CONST_INT TAXI_STAGE_WARP_TAXI					3
CONST_INT TAXI_STAGE_PULL_OVER					4
CONST_INT TAXI_STAGE_ARRIVED					5
CONST_INT TAXI_STAGE_WANDER						6

// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData

	INT iServerGameState
	INT iServerBitSet	
	INT iTaxiStage
	NETWORK_INDEX niTaxi
	NETWORK_INDEX niDriver
	SCRIPT_TIMER timeTerminate
	
ENDSTRUCT
ServerBroadcastData serverBD
structPedsForConversation sTaxiSpeech

CONST_INT	biP_BoughtPistol		0
CONST_INT	biP_LeftAmmu			1
CONST_INT 	biP_DoneCutscene		2
CONST_INT 	biP_ForceTaxiSkip		3
CONST_INT	biP_WarpTaxi			4
CONST_INT 	biP_TaxiAtDest			5
CONST_INT	biP_PlayerOutTaxi		6

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it. 
STRUCT PlayerBroadcastData

	INT iGameState
	INT iMissionState
	INT iFmIntroState
	INT iPlayerBitSet
	INT iIntroCutBitset
	//WELCOME_ENUM MissionStage =  ENTITY_CREATION
	#IF IS_DEBUG_BUILD
		BOOL bPressS = FALSE
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_MISSION_PLAYERS]

INT iBoolsBitSet
INT iBoolsBitSet2

CONST_INT biL_PlayerToldToGetToAmmu			0
CONST_INT biL_DoneAmmuHelp					1
CONST_INT biL_DoneFirstCall					2
CONST_INT biL_DoneTaxiHelp					3
CONST_INT biL_DoneGetToShootText			4
CONST_INT biL_DoneShootHelpText				5
CONST_INT biL_showFakeCorona				6
CONST_INT biL_InitFakeCoronaMessage			7
CONST_INT biL_RunningCutscene				8
CONST_INT biL_CutWasSkipped					9
CONST_INT biL_RemovedWeapons				10
CONST_INT biL_InitialCallFinished			12
CONST_INT biL_ToldToBuyPistol				13
CONST_INT biL_ToldToLoseWAnted				14
CONST_INT biL_RemovedGpsForTaxi				15
CONST_INT biL_DoneTaxiHelp2					16
CONST_INT biL_DoneSocialHelp				17
CONST_INT biL_DoPhoneCall					18
CONST_INT bil_DoneRankHelp					19
CONST_INT biL_AddedCOntact					20
CONST_INT biL_DoneTextMsg					21
CONST_INT biL_EnabledTaxiHelp				22
CONST_INT biL_SetTaxiForceSkip				23
CONST_INT biL_ShowRankBar					24
CONST_INT biL_GivenAmmuObjOnce				25
CONST_INT biL_LoadAllNodes					26
CONST_INT biL_FadedOutForWarp				27
CONST_INT biL_DoneSetOffSpeech				28
CONST_INT biL_TaxiPullOver					29
CONST_INT biL_DoneArrivedSpeech				30
CONST_INT biL_SavedStat						31

CONST_INT biL2_PlayerCOntrolTurnedOff		0
CONST_INT biL2_DoHelpText					1
CONST_INT biL2_DoneHelpText					2
CONST_INT biL2_StartedUber					3
CONST_INT biL2_UberPausedAtStart			4




SEQUENCE_INDEX seqOutCar
BLIP_INDEX blipShootRange

//SEQUENCE_INDEX seqTaxi

//CUTSCENE_PROG cutsceneProg
//CAMERA_INDEX cutsceneCam

INT iHelpTextProg

INT iLeaveAmmuCLientProg
//MODEL_NAMES mPlayerPed
//NETWORK_INDEX niCutPlayer
BOOL bDoCoronaTut = TRUE
BOOL bDoRaceWarp = TRUE
BOOL bDoIntroCut = TRUE


BOOL bDoNewIntroCut = TRUE
//INT storeLocateMessageID = NO_LOCATE_MESSAGE_ID

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	BOOL bHostEndMissionNow
	BOOL bShowRankBar
	BOOL bUseMobilePhone
	BOOL bWdSkipTaxi
	BOOL bWdDoPassiveCut
	BOOL bWdCreateCF
	BOOL bWdRunUber
	BOOL bWdWarpUber
	INT iWdRunUberProg
	VEHICLE_INDEX vehUberMain
	
	CONST_INT biD_GiveGunForDebug			0
	INT iDebugBitSet
	
	PROC NET_DW_PRINT(STRING sText)
		NET_PRINT_TIME() NET_PRINT(GET_MP_MISSION_NAME(thisMission)) NET_PRINT(" [DSW] ")NET_PRINT_STRINGS(" ", sText) NET_NL()
	ENDPROC 

	PROC NET_DW_PRINT_STRING_INT(STRING sText1, INT i)
		NET_PRINT_TIME() NET_PRINT_STRINGS(GET_MP_MISSION_NAME(thisMission), " ") NET_PRINT(" [DSW] ") NET_PRINT_STRING_INT(sText1, i) NET_NL()
	ENDPROC

	PROC NET_DW_PRINT_STRING_FLOAT(STRING sText1, FLOAT f)
		NET_PRINT_TIME() NET_PRINT_STRINGS(GET_MP_MISSION_NAME(thisMission), " ") NET_PRINT(" [DSW] ") NET_PRINT_STRING_FLOAT(sText1, f) NET_NL()
	ENDPROC

	PROC NET_DW_PRINT_STRING_VECTOR(STRING sText1, VECTOR v)
		NET_PRINT_TIME() NET_PRINT_STRINGS(GET_MP_MISSION_NAME(thisMission), " ") NET_PRINT(" [DSW] ") NET_PRINT_STRING_VECTOR(sText1, v) NET_NL()
	ENDPROC
	
	PROC NET_DW_PRINT_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player)
		NET_PRINT_TIME() NET_PRINT(GET_MP_MISSION_NAME(thisMission)) NET_PRINT(" [DSW] ") NET_PRINT_STRINGS(" ", sText) NET_PRINT_STRINGS(" ", GET_PLAYER_NAME(player)) NET_NL()
	ENDPROC
	
	PROC NET_DW_PRINT_STRINGS(STRING sText1, STRING sText2)
		NET_PRINT_TIME() NET_PRINT("[ImportExport] [DSW] ") NET_PRINT_STRINGS(sText1, sText2) NET_NL()
	ENDPROC
	
	/*
	PROC INIT_UBER_DATA()
		TrafficCarPos[0] = <<-2399.3047, -244.5505, 14.9263>>
		TrafficCarQuatX[0] = 0.0072
		TrafficCarQuatY[0] = 0.0106
		TrafficCarQuatZ[0] = 0.5016
		TrafficCarQuatW[0] = 0.8650
		TrafficCarRecording[0] = 1
		TrafficCarStartime[0] = 5694.0000
		TrafficCarModel[0] = RancherXL

		TrafficCarPos[1] = <<-2365.5552, -265.5454, 13.9025>>
		TrafficCarQuatX[1] = 0.0043
		TrafficCarQuatY[1] = 0.0088
		TrafficCarQuatZ[1] = 0.4663
		TrafficCarQuatW[1] = 0.8846
		TrafficCarRecording[1] = 2
		TrafficCarStartime[1] = 6947.0000
		TrafficCarModel[1] = washington

		TrafficCarPos[2] = <<-2301.8604, -309.1367, 13.2139>>
		TrafficCarQuatX[2] = 0.0008
		TrafficCarQuatY[2] = 0.0032
		TrafficCarQuatZ[2] = 0.5156
		TrafficCarQuatW[2] = 0.8568
		TrafficCarRecording[2] = 3
		TrafficCarStartime[2] = 9058.0000
		TrafficCarModel[2] = washington

		TrafficCarPos[3] = <<-2225.7073, -353.5604, 13.6592>>
		TrafficCarQuatX[3] = 0.0011
		TrafficCarQuatY[3] = -0.0007
		TrafficCarQuatZ[3] = 0.7426
		TrafficCarQuatW[3] = -0.6697
		TrafficCarRecording[3] = 4
		TrafficCarStartime[3] = 11104.0000
		TrafficCarModel[3] = Mule2

		TrafficCarPos[4] = <<-2199.3643, -355.8710, 12.4825>>
		TrafficCarQuatX[4] = -0.0081
		TrafficCarQuatY[4] = -0.0136
		TrafficCarQuatZ[4] = 0.7572
		TrafficCarQuatW[4] = -0.6530
		TrafficCarRecording[4] = 5
		TrafficCarStartime[4] = 11764.0000
		TrafficCarModel[4] = buccaneer

		TrafficCarPos[5] = <<-2170.4912, -340.1079, 12.8046>>
		TrafficCarQuatX[5] = 0.0043
		TrafficCarQuatY[5] = -0.0002
		TrafficCarQuatZ[5] = 0.9983
		TrafficCarQuatW[5] = -0.0582
		TrafficCarRecording[5] = 6
		TrafficCarStartime[5] = 12556.0000
		TrafficCarModel[5] = exemplar

		TrafficCarPos[6] = <<-2147.4604, -349.2338, 12.5741>>
		TrafficCarQuatX[6] = 0.0002
		TrafficCarQuatY[6] = -0.0002
		TrafficCarQuatZ[6] = 0.6007
		TrafficCarQuatW[6] = 0.7995
		TrafficCarRecording[6] = 7
		TrafficCarStartime[6] = 13150.0000
		TrafficCarModel[6] = sentinel2

		TrafficCarPos[7] = <<-2144.8462, -345.8707, 12.7220>>
		TrafficCarQuatX[7] = -0.0007
		TrafficCarQuatY[7] = -0.0006
		TrafficCarQuatZ[7] = 0.5809
		TrafficCarQuatW[7] = 0.8139
		TrafficCarRecording[7] = 8
		TrafficCarStartime[7] = 13216.0000
		TrafficCarModel[7] = washington

		TrafficCarPos[8] = <<-2140.8115, -351.0432, 12.7979>>
		TrafficCarQuatX[8] = 0.0010
		TrafficCarQuatY[8] = -0.0010
		TrafficCarQuatZ[8] = 0.6129
		TrafficCarQuatW[8] = 0.7902
		TrafficCarRecording[8] = 9
		TrafficCarStartime[8] = 13282.0000
		TrafficCarModel[8] = schafter2

		TrafficCarPos[9] = <<-2106.4185, -381.1376, 12.1007>>
		TrafficCarQuatX[9] = 0.0007
		TrafficCarQuatY[9] = -0.0083
		TrafficCarQuatZ[9] = 0.8326
		TrafficCarQuatW[9] = -0.5538
		TrafficCarRecording[9] = 10
		TrafficCarStartime[9] = 14404.0000
		TrafficCarModel[9] = sentinel

		TrafficCarPos[10] = <<-2027.5253, -409.5529, 10.6808>>
		TrafficCarQuatX[10] = -0.0049
		TrafficCarQuatY[10] = -0.0053
		TrafficCarQuatZ[10] = 0.4195
		TrafficCarQuatW[10] = 0.9077
		TrafficCarRecording[10] = 11
		TrafficCarStartime[10] = 16912.0000
		TrafficCarModel[10] = washington

		TrafficCarPos[11] = <<-2026.7312, -416.3292, 10.8506>>
		TrafficCarQuatX[11] = -0.0086
		TrafficCarQuatY[11] = 0.0010
		TrafficCarQuatZ[11] = 0.4111
		TrafficCarQuatW[11] = 0.9115
		TrafficCarRecording[11] = 12
		TrafficCarStartime[11] = 16978.0000
		TrafficCarModel[11] = schafter2

		TrafficCarPos[12] = <<-2021.3420, -421.7732, 11.0063>>
		TrafficCarQuatX[12] = -0.0088
		TrafficCarQuatY[12] = 0.0009
		TrafficCarQuatZ[12] = 0.4131
		TrafficCarQuatW[12] = 0.9107
		TrafficCarRecording[12] = 13
		TrafficCarStartime[12] = 17176.0000
		TrafficCarModel[12] = exemplar

		TrafficCarPos[13] = <<-2016.1177, -419.1502, 11.1113>>
		TrafficCarQuatX[13] = -0.0043
		TrafficCarQuatY[13] = -0.0047
		TrafficCarQuatZ[13] = 0.4202
		TrafficCarQuatW[13] = 0.9074
		TrafficCarRecording[13] = 14
		TrafficCarStartime[13] = 17242.0000
		TrafficCarModel[13] = felon2

		TrafficCarPos[14] = <<-1992.8494, -445.3260, 11.3102>>
		TrafficCarQuatX[14] = -0.0031
		TrafficCarQuatY[14] = 0.0038
		TrafficCarQuatZ[14] = 0.4331
		TrafficCarQuatW[14] = 0.9013
		TrafficCarRecording[14] = 15
		TrafficCarStartime[14] = 18100.0000
		TrafficCarModel[14] = voltic

		TrafficCarPos[15] = <<-1995.5291, -470.2938, 11.2030>>
		TrafficCarQuatX[15] = -0.0001
		TrafficCarQuatY[15] = 0.0010
		TrafficCarQuatZ[15] = 0.9038
		TrafficCarQuatW[15] = -0.4280
		TrafficCarRecording[15] = 16
		TrafficCarStartime[15] = 18364.0000
		TrafficCarModel[15] = GRANGER

		TrafficCarPos[16] = <<-1975.0636, -453.2077, 11.2214>>
		TrafficCarQuatX[16] = 0.0012
		TrafficCarQuatY[16] = -0.0045
		TrafficCarQuatZ[16] = 0.4250
		TrafficCarQuatW[16] = 0.9052
		TrafficCarRecording[16] = 17
		TrafficCarStartime[16] = 18562.0000
		TrafficCarModel[16] = sentinel

		TrafficCarPos[17] = <<-1968.6289, -464.8877, 12.0969>>
		TrafficCarQuatX[17] = -0.0010
		TrafficCarQuatY[17] = 0.0017
		TrafficCarQuatZ[17] = 0.4288
		TrafficCarQuatW[17] = 0.9034
		TrafficCarRecording[17] = 18
		TrafficCarStartime[17] = 18826.0000
		TrafficCarModel[17] = Mule2

		TrafficCarPos[18] = <<-1971.3881, -474.6383, 11.4146>>
		TrafficCarQuatX[18] = -0.0074
		TrafficCarQuatY[18] = -0.0027
		TrafficCarQuatZ[18] = 0.9026
		TrafficCarQuatW[18] = -0.4305
		TrafficCarRecording[18] = 19
		TrafficCarStartime[18] = 18892.0000
		TrafficCarModel[18] = GRANGER

		TrafficCarPos[19] = <<-1978.3639, -482.9777, 11.2413>>
		TrafficCarQuatX[19] = 0.0024
		TrafficCarQuatY[19] = 0.0035
		TrafficCarQuatZ[19] = 0.9023
		TrafficCarQuatW[19] = -0.4311
		TrafficCarRecording[19] = 20
		TrafficCarStartime[19] = 18892.0000
		TrafficCarModel[19] = washington

		TrafficCarPos[20] = <<-1954.9985, -502.5823, 11.4308>>
		TrafficCarQuatX[20] = 0.0040
		TrafficCarQuatY[20] = 0.0020
		TrafficCarQuatZ[20] = 0.9043
		TrafficCarQuatW[20] = -0.4269
		TrafficCarRecording[20] = 21
		TrafficCarStartime[20] = 19618.0000
		TrafficCarModel[20] = schafter2

		TrafficCarPos[21] = <<-1945.9519, -502.5286, 11.3376>>
		TrafficCarQuatX[21] = -0.0041
		TrafficCarQuatY[21] = -0.0019
		TrafficCarQuatZ[21] = 0.9013
		TrafficCarQuatW[21] = -0.4332
		TrafficCarRecording[21] = 22
		TrafficCarStartime[21] = 19750.0000
		TrafficCarModel[21] = washington

		TrafficCarPos[22] = <<-1934.6993, -504.6757, 11.5060>>
		TrafficCarQuatX[22] = -0.0042
		TrafficCarQuatY[22] = -0.0020
		TrafficCarQuatZ[22] = 0.9042
		TrafficCarQuatW[22] = -0.4271
		TrafficCarRecording[22] = 23
		TrafficCarStartime[22] = 20014.0000
		TrafficCarModel[22] = GRANGER


		TrafficCarPos[23] = <<-1932.9640, -519.9604, 11.3872>>
		TrafficCarQuatX[23] = 0.0047
		TrafficCarQuatY[23] = 0.0003
		TrafficCarQuatZ[23] = 0.9033
		TrafficCarQuatW[23] = -0.4290
		TrafficCarRecording[23] = 24
		TrafficCarStartime[23] = 20278.0000
		TrafficCarModel[23] = washington

	

		TrafficCarPos[24] = <<-1896.9862, -549.8754, 11.4011>>
		TrafficCarQuatX[24] = 0.0005
		TrafficCarQuatY[24] = -0.0000
		TrafficCarQuatZ[24] = 0.9043
		TrafficCarQuatW[24] = -0.4269
		TrafficCarRecording[24] = 25
		TrafficCarStartime[24] = 21400.0000
		TrafficCarModel[24] = felon2

		

		TrafficCarPos[25] = <<-1869.6921, -572.5540, 11.2179>>
		TrafficCarQuatX[25] = 0.0003
		TrafficCarQuatY[25] = -0.0008
		TrafficCarQuatZ[25] = 0.9052
		TrafficCarQuatW[25] = -0.4251
		TrafficCarRecording[25] = 26
		TrafficCarStartime[25] = 22324.0000
		TrafficCarModel[25] = exemplar

		TrafficCarPos[26] = <<-1867.2515, -548.5517, 11.3445>>
		TrafficCarQuatX[26] = 0.0014
		TrafficCarQuatY[26] = 0.0006
		TrafficCarQuatZ[26] = 0.4265
		TrafficCarQuatW[26] = 0.9045
		TrafficCarRecording[26] = 27
		TrafficCarStartime[26] = 22324.0000
		TrafficCarModel[26] = felon2

		TrafficCarPos[27] = <<-1835.3103, -567.3809, 11.0081>>
		TrafficCarQuatX[27] = 0.0022
		TrafficCarQuatY[27] = -0.0038
		TrafficCarQuatZ[27] = 0.4262
		TrafficCarQuatW[27] = 0.9046
		TrafficCarRecording[27] = 28
		TrafficCarStartime[27] = 22984.0000
		TrafficCarModel[27] = washington

		TrafficCarPos[28] = <<-1835.1680, -575.2065, 11.0741>>
		TrafficCarQuatX[28] = 0.0003
		TrafficCarQuatY[28] = 0.0041
		TrafficCarQuatZ[28] = 0.4271
		TrafficCarQuatW[28] = 0.9042
		TrafficCarRecording[28] = 29
		TrafficCarStartime[28] = 23116.0000
		TrafficCarModel[28] = schafter2

		TrafficCarPos[29] = <<-1837.0200, -584.9578, 10.8339>>
		TrafficCarQuatX[29] = -0.0032
		TrafficCarQuatY[29] = -0.0036
		TrafficCarQuatZ[29] = 0.9026
		TrafficCarQuatW[29] = -0.4304
		TrafficCarRecording[29] = 30
		TrafficCarStartime[29] = 23182.0000
		TrafficCarModel[29] = sentinel2

		
		TrafficCarPos[30] = <<-1784.4249, -615.2433, 10.5069>>
		TrafficCarQuatX[30] = 0.0041
		TrafficCarQuatY[30] = 0.0016
		TrafficCarQuatZ[30] = 0.4284
		TrafficCarQuatW[30] = 0.9036
		TrafficCarRecording[30] = 31
		TrafficCarStartime[30] = 24634.0000
		TrafficCarModel[30] = washington

		TrafficCarPos[31] = <<-1777.0490, -641.4568, 10.0975>>
		TrafficCarQuatX[31] = -0.0003
		TrafficCarQuatY[31] = -0.0059
		TrafficCarQuatZ[31] = 0.9040
		TrafficCarQuatW[31] = -0.4275
		TrafficCarRecording[31] = 32
		TrafficCarStartime[31] = 25096.0000
		TrafficCarModel[31] = sentinel

		TrafficCarPos[32] = <<-1769.1995, -640.9190, 10.2921>>
		TrafficCarQuatX[32] = -0.0012
		TrafficCarQuatY[32] = -0.0063
		TrafficCarQuatZ[32] = 0.9040
		TrafficCarQuatW[32] = -0.4275
		TrafficCarRecording[32] = 33
		TrafficCarStartime[32] = 25228.0000
		TrafficCarModel[32] = exemplar

		TrafficCarPos[33] = <<-1777.9781, -655.4622, 10.0526>>
		TrafficCarQuatX[33] = 0.0015
		TrafficCarQuatY[33] = -0.0065
		TrafficCarQuatZ[33] = 0.9231
		TrafficCarQuatW[33] = -0.3845
		TrafficCarRecording[33] = 34
		TrafficCarStartime[33] = 25294.0000
		TrafficCarModel[33] = voltic

		TrafficCarPos[34] = <<-1743.7854, -667.8901, 10.0689>>
		TrafficCarQuatX[34] = -0.0098
		TrafficCarQuatY[34] = -0.0023
		TrafficCarQuatZ[34] = 0.8995
		TrafficCarQuatW[34] = -0.4368
		TrafficCarRecording[34] = 35
		TrafficCarStartime[34] = 26086.0000
		TrafficCarModel[34] = GRANGER

		TrafficCarPos[35] = <<-1709.2864, -673.8101, 10.4791>>
		TrafficCarQuatX[35] = -0.0131
		TrafficCarQuatY[35] = 0.0019
		TrafficCarQuatZ[35] = 0.4438
		TrafficCarQuatW[35] = 0.8960
		TrafficCarRecording[35] = 36
		TrafficCarStartime[35] = 26878.0000
		TrafficCarModel[35] = sentinel2

		TrafficCarPos[36] = <<-1679.0759, -714.5037, 11.1251>>
		TrafficCarQuatX[36] = 0.0038
		TrafficCarQuatY[36] = 0.0020
		TrafficCarQuatZ[36] = 0.8756
		TrafficCarQuatW[36] = -0.4829
		TrafficCarRecording[36] = 37
		TrafficCarStartime[36] = 28000.0000
		TrafficCarModel[36] = picador

		TrafficCarPos[37] = <<-1650.7747, -712.2825, 10.9251>>
		TrafficCarQuatX[37] = -0.0018
		TrafficCarQuatY[37] = 0.0028
		TrafficCarQuatZ[37] = 0.5120
		TrafficCarQuatW[37] = 0.8590
		TrafficCarRecording[37] = 38
		TrafficCarStartime[37] = 28528.0000
		TrafficCarModel[37] = picador

		TrafficCarPos[38] = <<-1621.6965, -746.8388, 11.0997>>
		TrafficCarQuatX[38] = 0.0032
		TrafficCarQuatY[38] = 0.0027
		TrafficCarQuatZ[38] = 0.8390
		TrafficCarQuatW[38] = -0.5442
		TrafficCarRecording[38] = 39
		TrafficCarStartime[38] = 29584.0000
		TrafficCarModel[38] = felon2

		TrafficCarPos[39] = <<-1528.3920, -767.0764, 18.6432>>
		TrafficCarQuatX[39] = 0.0080
		TrafficCarQuatY[39] = -0.0059
		TrafficCarQuatZ[39] = 0.3974
		TrafficCarQuatW[39] = 0.9176
		TrafficCarRecording[39] = 40
		TrafficCarStartime[39] = 32026.0000
		TrafficCarModel[39] = picador

	ENDPROC
	*/
	TEXT_WIDGET_ID textWdMusicCue
	INT iWdCurrentCutsceneTimer
	BOOL bWdPlayCue
	BOOL bWdSHowMissionInt
	BOOL bWdShowPlaneRecTime
	BOOL bWdTestLoadScene
	BOOL bWdMalePed
	FLOAT fWdPlaneRecTime
	INT iWdLoadSceneTest
	//WIDGET_GROUP_ID wdFmIntro
	PROC CREATE_WIDGETS()	
		/*wdFmIntro = */START_WIDGET_GROUP(GET_MP_MISSION_NAME(thisMission))
			ADD_WIDGET_INT_READ_ONLY("Script Progress", iWdScriptProgress)
			ADD_WIDGET_INT_READ_ONLY("Current Cutscene Timer", iWdCurrentCutsceneTimer)
			ADD_WIDGET_INT_READ_ONLY("iCurrentMissionType", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType)
			ADD_WIDGET_INT_READ_ONLY("iLeaveAmmuCLientProg", iLeaveAmmuCLientProg)
			ADD_WIDGET_BOOL("Show rank bar", bShowRankBar)
			ADD_WIDGET_BOOL("Give use phone task", bUseMobilePhone)
			ADD_WIDGET_BOOL("Skip taxi", bWdSkipTaxi)
			ADD_WIDGET_BOOL("Show mission int", bWdSHowMissionInt)
			ADD_WIDGET_BOOL("Test Load scene", bWdTestLoadScene)
			ADD_WIDGET_BOOL("Male ped?",bWdMalePed )
			textWdMusicCue = ADD_TEXT_WIDGET("Cue")
			ADD_WIDGET_BOOL("Play cue", bWdPlayCue)
			START_WIDGET_GROUP("Intro Cut")
				ADD_WIDGET_BOOL("Show plane rec time", bWdShowPlaneRecTime)
				ADD_WIDGET_FLOAT_READ_ONLY("Plane rec time", fWdPlaneRecTime)
			STOP_WIDGET_GROUP() 
			START_WIDGET_GROUP("Cutscene test")
				ADD_WIDGET_BOOL("Do passive cut", bWdDoPassiveCut)
				ADD_WIDGET_BOOL("CReate CF", bWdCreateCF)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Uber test")
				ADD_WIDGET_BOOL("Warp to Uber", bWdWarpUber)
				ADD_WIDGET_BOOL("Run Uber", bWdRunUber)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		SET_CONTENTS_OF_TEXT_WIDGET(textWdMusicCue, "FM_INTRO_START")
		//SET_UBER_PARENT_WIDGET_GROUP(wdFmIntro) 
	ENDPROC
	
	PROC UPDATE_WIDGETS()
	
		iWdCurrentCutsceneTimer = GET_GAME_TIMER() - FmIntroCutscene.iCutStartTime
		
		IF bShowRankBar	
			PRINT_HELP("FM_IHELP_INT2", 4000)
			//SET_XP_BAR_ACTIVE(TRUE)
			SET_XP_BAR_ACTIVE_IN_CUTSCENE(TRUE)
			bShowRankBar = FALSE
		ENDIF
		
		IF bUseMobilePhone
			bUseMobilePhone = FALSE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				TASK_USE_MOBILE_PHONE(PLAYER_PED_ID(), TRUE)
				NET_DW_PRINT("Given TASK_USE_MOBILE_PHONE via widget")
			ENDIF
		ENDIF
		
		IF bWdSkipTaxi
			bWdSkipTaxi = FALSE
			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ForceTaxiSkip)
		ENDIF
		
		IF bWdPlayCue
			TRIGGER_MUSIC_EVENT(GET_CONTENTS_OF_TEXT_WIDGET(textWdMusicCue))
			bWdPlayCue = FALSE
		ENDIF
		
		IF bWdMalePed
			IF NOT IS_PLAYER_FEMALE()
				PRINTLN("Player ped is male")
			ELSE	
				PRINTLN("Player ped is female")
			ENDIF
		ENDIF
		
		IF bWdShowPlaneRecTime
		//	FmIntroCutscene.bShowPlaneRecTime = TRUE
			IF DOES_ENTITY_EXIST(FmIntroCutscene.vehHeli)
				IF IS_VEHICLE_DRIVEABLE(FmIntroCutscene.vehHeli)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(FmIntroCutscene.vehHeli)
						PRINT_INTRO_STRING("Displaying plane rec time...")
						fWdPlaneRecTime = GET_TIME_POSITION_IN_RECORDING(FmIntroCutscene.vehHeli)
						
					
					ENDIF
				
				ENDIF
			
			ENDIF
		ENDIF
		
		IF bWdDoPassiveCut
		//	IF DO_CF_PASSIVE_CUT(cfPassiveCut)
				bWdDoPassiveCut = FALSE
		//	ENDIF
		ENDIF
		
		IF bWdCreateCF
		//	VECTOR vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, 5.0, 0.0>>)
		//	PED_INDEX pedTemp
		//	IF CREATE_PASSIVE_PED(pedTemp, vTemp, 0.0)
				bWdCreateCF = FALSE
		//	ENDIF
		ENDIF
		INTERIOR_INSTANCE_INDEX interiorGarage
		IF bWdTestLoadScene
			SWITCH iWdLoadSceneTest
				CASE 0
					interiorGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<234.0331, -1005.2032, -98.4626>>,"hei_dlc_garage_high_new")
					IF interiorGarage <> NULL
						PIN_INTERIOR_IN_MEMORY(interiorGarage)
					ELSE
						#IF IS_DEBUG_BUILD PRINT_INTRO_STRING("Garage interior is NULL! ") #ENDIF
					ENDIF
					NEW_LOAD_SCENE_START(<<228.8164, -1006.7802, -97.5672>>,  CONVERT_ROTATION_TO_DIRECTION_VECTOR( <<-14.5887, -0.0000, -13.0554>>), 100.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE) 
								
					PRINTLN("Started load scene")
					iWdLoadSceneTest++
				BREAK
				
				CASE 1
					IF IS_NEW_LOAD_SCENE_LOADED()
						PRINTLN("Scene loaded")
						NEW_LOAD_SCENE_STOP()
						bWdTestLoadScene = FALSE
						iWdLoadSceneTest= 0
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
		IF bWdWarpUber
			bWdWarpUber = FALSE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-2408.3252, -230.8033, 15.3468>>)
			ENDIF
		ENDIF
		
		IF bWdRunUber
			SWITCH iWdRunUberProg
				CASE 0
					REQUEST_MODEL(CHEETAH)
					REQUEST_VEHICLE_RECORDING(100, "FM_Intro_uber")
					IF HAS_MODEL_LOADED(CHEETAH)
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(100, "FM_Intro_uber")
						
						vehUberMain = CREATE_VEHICLE(CHEETAH, <<-2408.3252, -230.8033, 15.3468>>, 13.3357, FALSE, FALSE)
						
				//		INITIALISE_UBER_PLAYBACK("FM_Intro_uber", 100)
				//		INIT_UBER_DATA()
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							TASK_ENTER_VEHICLE( PLAYER_PED_ID(), vehUberMain, 1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED )
						ENDIF
						iWdRunUberProg++
					ENDIF
				BREAK
				
				CASE 1
					IF IS_VEHICLE_DRIVEABLE(vehUberMain)
						 START_PLAYBACK_RECORDED_VEHICLE(vehUberMain, 100, "FM_Intro_uber")
						 iWdRunUberProg++
					ENDIF
				BREAK
				
				CASE 2
					IF IS_VEHICLE_DRIVEABLE(vehUberMain)	
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehUberMain)
			

							SET_PLAYBACK_SPEED(vehUberMain, 1.0)

						//	UPDATE_UBER_PLAYBACK(vehUberMain, 1.0)

						ELSE
							// finish	
						//	CLEANUP_UBER_PLAYBACK()
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDPROC
	
	PROC DO_DEBUG_KEYS()
		FLOAT fDist
		INTERIOR_INSTANCE_INDEX interior
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
		
			IF NOT MPGlobalsAmbience.bRunningFmIntroCut
			OR NOT FmIntroCutscene.bPTFXOn //check ptfx flag as that's only on when running new character
				
				SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LeftAmmu)
				SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biS_PlayerDoneCut)
				playerBD[PARTICIPANT_ID_TO_INT()].bPressS = TRUE
				g_bPlayerSkippedIntro = TRUE
			ENDIF
			
			
			
		/*	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_S, KEYBOARD_MODIFIER_SHIFT, "Force Pass All Tutorials")
				IF NETWORK_IS_IN_TUTORIAL_SESSION()
					NETWORK_END_TUTORIAL_SESSION()
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Removing from tutorial session as shift-s skipped") #ENDIF
				ENDIF
				IF NETWORK_IS_CLOCK_TIME_OVERRIDDEN()
					SET_TIME_OF_DAY(TIME_OFF, TRUE)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing clock override as shift-s skipped") #ENDIF
				ENDIF
				SET_LOCAL_PLAYER_IS_IN_TUTORIAL_SESSION_FOR_INTRO(FALSE)
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_TRIGTUT_DONE, TRUE)
				UNLOCK_ALL_FM_ACTIVITIES()
			ENDIF */
			
			
		ELSE	
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_SHIFT, "Skip tutorials")
			
				SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LeftAmmu)
				SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biS_PlayerDoneCut)
				playerBD[PARTICIPANT_ID_TO_INT()].bPressS = TRUE
				
				NET_DW_PRINT("SHift-J was used, setting Activity tutorial done")
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_TRIGTUT_DONE, TRUE)
				
				UNLOCK_ALL_FM_ACTIVITIES()
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				NET_DW_PRINT("Player ok for j-skip...")
				
				Kill_Any_MP_Comms_From_This_Script()
				SWITCH playerbd[PARTICIPANT_ID_TO_INT()].iFmIntroState
					CASE FM_INTRO_STATE_GET_TO_AMMU
						SET_BIT(iBoolsBitSet, biL_DoneSocialHelp)
						SET_BIT(iBoolsBitSet, bil_DoneRankHelp)
						NET_DW_PRINT("j-skip FM_INTRO_STATE_GET_TO_AMMU...")
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), << 18.4771, -1125.8584, 27.8313 >>) > 45.0
					//		J_SKIP_MP(<< 18.4771, -1125.8584, 27.8313 >>, <<2.0, 2.0, 2.0>>)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ForceTaxiSkip)
							SET_BIT(iBoolsBitSet, biL_DoPhoneCall)
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoPhoneCall)
								CLEAR_HELP()
								SET_BIT(iBoolsBitSet, biL_DoPhoneCall)
								NET_DW_PRINT("j-skip setting biL_DoPhoneCall...")
							ENDIF
						ENDIF
					BREAK
					
					CASE FM_INTRO_STATE_GET_TO_SHOOT
						fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<12.8983, -1099.2043, 28.7970>>)
						interior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
						IF fDist > 45.0
							IF interior = NULL OR fDist > 100
								J_SKIP_MP(<< 18.4771, -1125.8584, 27.8313 >>, <<2.0, 2.0, 2.0>>)
							ELSE	
								SET_ENTITY_COORDS(PLAYER_PED_ID(), <<8.8578, -1097.6902, 28.7970>>)
								SET_ENTITY_HEADING(PLAYER_PED_ID(), 253.1402)
							ENDIF
						ELSE
							IF fDist > 5.0
								IF interior <> NULL 
									SET_ENTITY_COORDS(PLAYER_PED_ID(), <<8.8578, -1097.6902, 28.7970>>)
									SET_ENTITY_HEADING(PLAYER_PED_ID(), 253.1402)
								ELSE	
									SET_ENTITY_COORDS(PLAYER_PED_ID(), <<8.8578, -1097.6902, 28.7970>>)
									SET_ENTITY_HEADING(PLAYER_PED_ID(), 253.1402)
								ENDIF
							ELSE	
								IF interior <> NULL 
									SET_ENTITY_COORDS(PLAYER_PED_ID(), <<12.8983, -1099.2043, 28.7970>>)
									SET_ENTITY_HEADING(PLAYER_PED_ID(), 249.5692)
								ENDIF
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDPROC
	
	
#ENDIF

/*PROC INIT_TAXI_REC_UBER_DATA()
	
	
TrafficCarPos[0] = <<-835.7921, -2590.7693, 14.5351>>
TrafficCarQuatX[0] = 0.0093
TrafficCarQuatY[0] = 0.0174
TrafficCarQuatZ[0] = -0.2608
TrafficCarQuatW[0] = 0.9652
TrafficCarRecording[0] = 1
TrafficCarStartime[0] = 12443.0000
TrafficCarModel[0] = coach


TrafficCarPos[1] = <<-733.2234, -2402.8303, 14.0708>>
TrafficCarQuatX[1] = -0.0038
TrafficCarQuatY[1] = -0.0165
TrafficCarQuatZ[1] = -0.3139
TrafficCarQuatW[1] = 0.9493
TrafficCarRecording[1] = 2
TrafficCarStartime[1] = 23729.0000
TrafficCarModel[1] = cogcabrio



TrafficCarPos[2] = <<-737.8010, -2378.1970, 14.7642>>
TrafficCarQuatX[2] = -0.0124
TrafficCarQuatY[2] = -0.0076
TrafficCarQuatZ[2] = 0.9215
TrafficCarQuatW[2] = -0.3881
TrafficCarRecording[2] = 3
TrafficCarStartime[2] = 24719.0000
TrafficCarModel[2] = Baller

TrafficCarPos[3] = <<-727.1304, -2379.2849, 14.5529>>
TrafficCarQuatX[3] = -0.0070
TrafficCarQuatY[3] = 0.0199
TrafficCarQuatZ[3] = 0.4479
TrafficCarQuatW[3] = 0.8938
TrafficCarRecording[3] = 4
TrafficCarStartime[3] = 24917.0000
TrafficCarModel[3] = baller2

TrafficCarPos[4] = <<-692.8860, -2325.1372, 13.0607>>
TrafficCarQuatX[4] = 0.0076
TrafficCarQuatY[4] = -0.0005
TrafficCarQuatZ[4] = 1.0000
TrafficCarQuatW[4] = 0.0000
TrafficCarRecording[4] = 5
TrafficCarStartime[4] = 28019.0000
TrafficCarModel[4] = Airbus

TrafficCarPos[5] = <<-667.2964, -2327.0879, 10.2966>>
TrafficCarQuatX[5] = 0.0046
TrafficCarQuatY[5] = 0.0621
TrafficCarQuatZ[5] = 0.9339
TrafficCarQuatW[5] = 0.3520
TrafficCarRecording[5] = 6
TrafficCarStartime[5] = 28613.0000
TrafficCarModel[5] = Airbus

TrafficCarPos[6] = <<-698.1522, -2299.6895, 12.6488>>
TrafficCarQuatX[6] = -0.0001
TrafficCarQuatY[6] = -0.0019
TrafficCarQuatZ[6] = 0.9994
TrafficCarQuatW[6] = 0.0336
TrafficCarRecording[6] = 7
TrafficCarStartime[6] = 32837.0000
TrafficCarModel[6] = taxi

TrafficCarPos[7] = <<-820.8416, -2282.2034, 9.1033>>
TrafficCarQuatX[7] = -0.0387
TrafficCarQuatY[7] = -0.0111
TrafficCarQuatZ[7] = 0.7165
TrafficCarQuatW[7] = 0.6964
TrafficCarRecording[7] = 8
TrafficCarStartime[7] = 34817.0000
TrafficCarModel[7] = Airbus

TrafficCarPos[8] = <<-913.1492, -2177.8582, 8.3971>>
TrafficCarQuatX[8] = 0.0045
TrafficCarQuatY[8] = 0.0019
TrafficCarQuatZ[8] = 0.6869
TrafficCarQuatW[8] = 0.7268
TrafficCarRecording[8] = 9
TrafficCarStartime[8] = 47291.0000
TrafficCarModel[8] = taxi

TrafficCarPos[9] = <<-962.7810, -2153.9744, 8.7769>>
TrafficCarQuatX[9] = -0.0237
TrafficCarQuatY[9] = -0.0008
TrafficCarQuatZ[9] = 0.8616
TrafficCarQuatW[9] = -0.5071
TrafficCarRecording[9] = 10
TrafficCarStartime[9] = 49931.0000
TrafficCarModel[9] = baller2

TrafficCarPos[10] = <<-998.1550, -2118.3391, 12.1904>>
TrafficCarQuatX[10] = 0.0044
TrafficCarQuatY[10] = -0.0372
TrafficCarQuatZ[10] = 0.9239
TrafficCarQuatW[10] = -0.3809
TrafficCarRecording[10] = 11
TrafficCarStartime[10] = 53033.0000
TrafficCarModel[10] = coach

TrafficCarPos[11] = <<-1009.9365, -2106.8423, 12.5743>>
TrafficCarQuatX[11] = 0.0204
TrafficCarQuatY[11] = -0.0270
TrafficCarQuatZ[11] = 0.9358
TrafficCarQuatW[11] = -0.3509
TrafficCarRecording[11] = 12
TrafficCarStartime[11] = 53891.0000
TrafficCarModel[11] = baller2

TrafficCarPos[12] = <<-1027.9304, -2089.3674, 13.5135>>
TrafficCarQuatX[12] = -0.0082
TrafficCarQuatY[12] = 0.0009
TrafficCarQuatZ[12] = 0.9294
TrafficCarQuatW[12] = -0.3691
TrafficCarRecording[12] = 13
TrafficCarStartime[12] = 55871.0000
TrafficCarModel[12] = baller2

TrafficCarPos[13] = <<-1037.4514, -2070.4861, 13.3255>>
TrafficCarQuatX[13] = -0.0135
TrafficCarQuatY[13] = 0.0118
TrafficCarQuatZ[13] = 0.3744
TrafficCarQuatW[13] = 0.9271
TrafficCarRecording[13] = 14
TrafficCarStartime[13] = 56729.0000
TrafficCarModel[13] = Baller

TrafficCarPos[14] = <<-1048.9854, -2066.5906, 13.1832>>
TrafficCarQuatX[14] = -0.0090
TrafficCarQuatY[14] = 0.0043
TrafficCarQuatZ[14] = 0.9179
TrafficCarQuatW[14] = -0.3967
TrafficCarRecording[14] = 15
TrafficCarStartime[14] = 57521.0000
TrafficCarModel[14] = baller2

TrafficCarPos[15] = <<-1072.8124, -2042.8628, 12.9976>>
TrafficCarQuatX[15] = -0.0014
TrafficCarQuatY[15] = -0.0042
TrafficCarQuatZ[15] = 0.9378
TrafficCarQuatW[15] = -0.3471
TrafficCarRecording[15] = 16
TrafficCarStartime[15] = 63263.0000
TrafficCarModel[15] = baller2

TrafficCarPos[16] = <<-1091.6719, -2013.8805, 13.9786>>
TrafficCarQuatX[16] = -0.0130
TrafficCarQuatY[16] = -0.0031
TrafficCarQuatZ[16] = 0.9857
TrafficCarQuatW[16] = -0.1681
TrafficCarRecording[16] = 17
TrafficCarStartime[16] = 66563.0000
TrafficCarModel[16] = coach

TrafficCarPos[17] = <<-1097.7356, -1985.0912, 13.0441>>
TrafficCarQuatX[17] = -0.0097
TrafficCarQuatY[17] = 0.0008
TrafficCarQuatZ[17] = 0.9986
TrafficCarQuatW[17] = 0.0511
TrafficCarRecording[17] = 18
TrafficCarStartime[17] = 68741.0000
TrafficCarModel[17] = baller2


TrafficCarPos[18] = <<-947.8526, -1807.5502, 19.3172>>
TrafficCarQuatX[18] = -0.0132
TrafficCarQuatY[18] = 0.0055
TrafficCarQuatZ[18] = 0.9225
TrafficCarQuatW[18] = 0.3858
TrafficCarRecording[18] = 19
TrafficCarStartime[18] = 82667.0000
TrafficCarModel[18] = banshee

TrafficCarPos[19] = <<-912.1384, -1825.2117, 35.4339>>
TrafficCarQuatX[19] = 0.0175
TrafficCarQuatY[19] = -0.0319
TrafficCarQuatZ[19] = 0.9759
TrafficCarQuatW[19] = 0.2153
TrafficCarRecording[19] = 20
TrafficCarStartime[19] = 85175.0000
TrafficCarModel[19] = TOWTRUCK

TrafficCarPos[20] = <<-913.9094, -1774.5818, 19.3145>>
TrafficCarQuatX[20] = -0.0059
TrafficCarQuatY[20] = 0.0108
TrafficCarQuatZ[20] = 0.9395
TrafficCarQuatW[20] = 0.3424
TrafficCarRecording[20] = 21
TrafficCarStartime[20] = 86957.0000
TrafficCarModel[20] = surge

TrafficCarPos[21] = <<-897.2902, -1797.9849, 36.4068>>
TrafficCarQuatX[21] = 0.0158
TrafficCarQuatY[21] = -0.0239
TrafficCarQuatZ[21] = 0.9489
TrafficCarQuatW[21] = 0.3143
TrafficCarRecording[21] = 22
TrafficCarStartime[21] = 87089.0000
TrafficCarModel[21] = Seminole

TrafficCarPos[22] = <<-911.0331, -1770.7452, 19.5059>>
TrafficCarQuatX[22] = -0.0110
TrafficCarQuatY[22] = 0.0159
TrafficCarQuatZ[22] = 0.9169
TrafficCarQuatW[22] = 0.3986
TrafficCarRecording[22] = 23
TrafficCarStartime[22] = 87419.0000
TrafficCarModel[22] = Baller

TrafficCarPos[23] = <<-881.2488, -1778.5300, 37.1705>>
TrafficCarQuatX[23] = 0.0185
TrafficCarQuatY[23] = -0.0173
TrafficCarQuatZ[23] = 0.9206
TrafficCarQuatW[23] = 0.3896
TrafficCarRecording[23] = 24
TrafficCarStartime[23] = 89069.0000
TrafficCarModel[23] = asterope

TrafficCarPos[24] = <<-880.0293, -1777.2653, 37.5994>>
TrafficCarQuatX[24] = 0.0210
TrafficCarQuatY[24] = -0.0180
TrafficCarQuatZ[24] = 0.9109
TrafficCarQuatW[24] = 0.4116
TrafficCarRecording[24] = 25
TrafficCarStartime[24] = 89993.0000
TrafficCarModel[24] = baller2

TrafficCarPos[25] = <<-848.1504, -1747.6141, 37.5282>>
TrafficCarQuatX[25] = 0.0075
TrafficCarQuatY[25] = -0.0076
TrafficCarQuatZ[25] = 0.8517
TrafficCarQuatW[25] = 0.5239
TrafficCarRecording[25] = 26
TrafficCarStartime[25] = 95141.0000
TrafficCarModel[25] = Seminole

TrafficCarPos[26] = <<-820.2446, -1722.0101, 33.0672>>
TrafficCarQuatX[26] = 0.0254
TrafficCarQuatY[26] = 0.0548
TrafficCarQuatZ[26] = 0.7939
TrafficCarQuatW[26] = 0.6050
TrafficCarRecording[26] = 27
TrafficCarStartime[26] = 98639.0000
TrafficCarModel[26] = Baller

TrafficCarPos[27] = <<-836.5851, -1692.0010, 17.9624>>
TrafficCarQuatX[27] = -0.0064
TrafficCarQuatY[27] = 0.0201
TrafficCarQuatZ[27] = 0.9273
TrafficCarQuatW[27] = 0.3738
TrafficCarRecording[27] = 28
TrafficCarStartime[27] = 98705.0000
TrafficCarModel[27] = Seminole

TrafficCarPos[28] = <<-804.8829, -1711.9642, 30.8013>>
TrafficCarQuatX[28] = 0.0105
TrafficCarQuatY[28] = 0.0523
TrafficCarQuatZ[28] = 0.7849
TrafficCarQuatW[28] = 0.6173
TrafficCarRecording[28] = 29
TrafficCarStartime[28] = 99827.0000
TrafficCarModel[28] = TOWTRUCK

TrafficCarPos[29] = <<-825.0161, -1679.2863, 17.4250>>
TrafficCarQuatX[29] = -0.0055
TrafficCarQuatY[29] = 0.0239
TrafficCarQuatZ[29] = 0.9122
TrafficCarQuatW[29] = 0.4089
TrafficCarRecording[29] = 30
TrafficCarStartime[29] = 100157.0000
TrafficCarModel[29] = banshee

TrafficCarPos[30] = <<-808.9496, -1662.3126, 16.4978>>
TrafficCarQuatX[30] = -0.0037
TrafficCarQuatY[30] = 0.0199
TrafficCarQuatZ[30] = 0.9675
TrafficCarQuatW[30] = 0.2522
TrafficCarRecording[30] = 31
TrafficCarStartime[30] = 102005.0000
TrafficCarModel[30] = surge

TrafficCarPos[31] = <<-799.2814, -1643.7328, 15.4086>>
TrafficCarQuatX[31] = -0.0056
TrafficCarQuatY[31] = 0.0224
TrafficCarQuatZ[31] = 0.9594
TrafficCarQuatW[31] = 0.2812
TrafficCarRecording[31] = 32
TrafficCarStartime[31] = 103391.0000
TrafficCarModel[31] = Seminole

TrafficCarPos[32] = <<-767.4692, -1606.2012, 13.8968>>
TrafficCarQuatX[32] = -0.0110
TrafficCarQuatY[32] = 0.0096
TrafficCarQuatZ[32] = 0.9253
TrafficCarQuatW[32] = 0.3789
TrafficCarRecording[32] = 33
TrafficCarStartime[32] = 106625.0000
TrafficCarModel[32] = sentinel2

TrafficCarPos[33] = <<-693.2957, -1563.1563, 16.6450>>
TrafficCarQuatX[33] = -0.0211
TrafficCarQuatY[33] = 0.0648
TrafficCarQuatZ[33] = 0.9280
TrafficCarQuatW[33] = 0.3663
TrafficCarRecording[33] = 34
TrafficCarStartime[33] = 112565.0000
TrafficCarModel[33] = banshee

TrafficCarPos[34] = <<-670.5125, -1523.4238, 12.2399>>
TrafficCarQuatX[34] = -0.0177
TrafficCarQuatY[34] = 0.0422
TrafficCarQuatZ[34] = 0.9889
TrafficCarQuatW[34] = 0.1414
TrafficCarRecording[34] = 35
TrafficCarStartime[34] = 115271.0000
TrafficCarModel[34] = asterope

TrafficCarPos[35] = <<-650.6110, -1491.2662, 10.1585>>
TrafficCarQuatX[35] = -0.0087
TrafficCarQuatY[35] = -0.0038
TrafficCarQuatZ[35] = -0.0636
TrafficCarQuatW[35] = 0.9979
TrafficCarRecording[35] = 36
TrafficCarStartime[35] = 117779.0000
TrafficCarModel[35] = sentinel2

TrafficCarPos[36] = <<-645.0757, -1488.0890, 10.2357>>
TrafficCarQuatX[36] = -0.0065
TrafficCarQuatY[36] = 0.0178
TrafficCarQuatZ[36] = -0.0949
TrafficCarQuatW[36] = 0.9953
TrafficCarRecording[36] = 37
TrafficCarStartime[36] = 118175.0000
TrafficCarModel[36] = banshee

TrafficCarPos[37] = <<-660.0182, -1452.5342, 10.1582>>
TrafficCarQuatX[37] = -0.0012
TrafficCarQuatY[37] = -0.0044
TrafficCarQuatZ[37] = 0.9798
TrafficCarQuatW[37] = 0.2001
TrafficCarRecording[37] = 38
TrafficCarStartime[37] = 119957.0000
TrafficCarModel[37] = asterope

TrafficCarPos[38] = <<-649.6765, -1449.9265, 10.5533>>
TrafficCarQuatX[38] = -0.0004
TrafficCarQuatY[38] = 0.0015
TrafficCarQuatZ[38] = -0.1818
TrafficCarQuatW[38] = 0.9833
TrafficCarRecording[38] = 39
TrafficCarStartime[38] = 119957.0000
TrafficCarModel[38] = baller2

TrafficCarPos[39] = <<-654.0535, -1446.8331, 10.1374>>
TrafficCarQuatX[39] = 0.0000
TrafficCarQuatY[39] = -0.0000
TrafficCarQuatZ[39] = 0.9995
TrafficCarQuatW[39] = 0.0309
TrafficCarRecording[39] = 40
TrafficCarStartime[39] = 120089.0000
TrafficCarModel[39] = Seminole

TrafficCarPos[40] = <<-646.4331, -1427.5206, 10.3097>>
TrafficCarQuatX[40] = 0.0001
TrafficCarQuatY[40] = 0.0068
TrafficCarQuatZ[40] = -0.0298
TrafficCarQuatW[40] = 0.9995
TrafficCarRecording[40] = 41
TrafficCarStartime[40] = 122133.0000
TrafficCarModel[40] = surge

TrafficCarPos[41] = <<-645.3032, -1338.1102, 10.1753>>
TrafficCarQuatX[41] = 0.0009
TrafficCarQuatY[41] = -0.0000
TrafficCarQuatZ[41] = 0.9953
TrafficCarQuatW[41] = 0.0970
TrafficCarRecording[41] = 42
TrafficCarStartime[41] = 133286.0000
TrafficCarModel[41] = asterope

TrafficCarPos[42] = <<-629.2446, -1293.5460, 10.1638>>
TrafficCarQuatX[42] = -0.0017
TrafficCarQuatY[42] = -0.0031
TrafficCarQuatZ[42] = -0.4725
TrafficCarQuatW[42] = 0.8813
TrafficCarRecording[42] = 43
TrafficCarStartime[42] = 140018.0000
TrafficCarModel[42] = asterope

TrafficCarPos[43] = <<-635.2798, -1319.7693, 10.1207>>
TrafficCarQuatX[43] = 0.0007
TrafficCarQuatY[43] = 0.0043
TrafficCarQuatZ[43] = -0.1881
TrafficCarQuatW[43] = 0.9821
TrafficCarRecording[43] = 44
TrafficCarStartime[43] = 140018.0000
TrafficCarModel[43] = sabregt

TrafficCarPos[44] = <<-622.0163, -1278.3843, 10.6586>>
TrafficCarQuatX[44] = 0.0022
TrafficCarQuatY[44] = -0.0096
TrafficCarQuatZ[44] = 0.9760
TrafficCarQuatW[44] = 0.2174
TrafficCarRecording[44] = 45
TrafficCarStartime[44] = 142196.0000
TrafficCarModel[44] = baller2



TrafficCarPos[45] = <<-586.8435, -1234.8634, 13.8440>>
TrafficCarQuatX[45] = -0.0052
TrafficCarQuatY[45] = -0.0588
TrafficCarQuatZ[45] = 0.9294
TrafficCarQuatW[45] = 0.3642
TrafficCarRecording[45] = 46
TrafficCarStartime[45] = 150248.0000
TrafficCarModel[45] = surge

TrafficCarPos[46] = <<-570.8846, -1216.5063, 16.3687>>
TrafficCarQuatX[46] = -0.0098
TrafficCarQuatY[46] = -0.0444
TrafficCarQuatZ[46] = 0.9555
TrafficCarQuatW[46] = 0.2915
TrafficCarRecording[46] = 47
TrafficCarStartime[46] = 150380.0000
TrafficCarModel[46] = asterope

TrafficCarPos[47] = <<-548.4938, -1195.0651, 17.6531>>
TrafficCarQuatX[47] = 0.0244
TrafficCarQuatY[47] = 0.0372
TrafficCarQuatZ[47] = -0.2021
TrafficCarQuatW[47] = 0.9784
TrafficCarRecording[47] = 48
TrafficCarStartime[47] = 152426.0000
TrafficCarModel[47] = sabregt



TrafficCarPos[48] = <<-557.4473, -1213.0536, 16.9583>>
TrafficCarQuatX[48] = 0.0448
TrafficCarQuatY[48] = 0.0576
TrafficCarQuatZ[48] = -0.2692
TrafficCarQuatW[48] = 0.9603
TrafficCarRecording[48] = 49
TrafficCarStartime[48] = 153878.0000
TrafficCarModel[48] = ruiner

TrafficCarPos[49] = <<-531.2596, -1175.4528, 18.7747>>
TrafficCarQuatX[49] = 0.0094
TrafficCarQuatY[49] = -0.0246
TrafficCarQuatZ[49] = -0.5584
TrafficCarQuatW[49] = 0.8292
TrafficCarRecording[49] = 50
TrafficCarStartime[49] = 154076.0000
TrafficCarModel[49] = prairie


TrafficCarPos[50] = <<-516.2030, -1138.2195, 19.6888>>
TrafficCarQuatX[50] = 0.0324
TrafficCarQuatY[50] = -0.0168
TrafficCarQuatZ[50] = 0.2514
TrafficCarQuatW[50] = 0.9672
TrafficCarRecording[50] = 51
TrafficCarStartime[50] = 155528.0000
TrafficCarModel[50] = asterope

TrafficCarPos[51] = <<-526.4636, -1112.0481, 21.3226>>
TrafficCarQuatX[51] = 0.0186
TrafficCarQuatY[51] = 0.0208
TrafficCarQuatZ[51] = 0.1158
TrafficCarQuatW[51] = 0.9929
TrafficCarRecording[51] = 52
TrafficCarStartime[51] = 156782.0000
TrafficCarModel[51] = peyote

TrafficCarPos[52] = <<-533.3487, -1106.4366, 22.2180>>
TrafficCarQuatX[52] = 0.0146
TrafficCarQuatY[52] = -0.0119
TrafficCarQuatZ[52] = -0.0035
TrafficCarQuatW[52] = 0.9998
TrafficCarRecording[52] = 53
TrafficCarStartime[52] = 162453.0000
TrafficCarModel[52] = baller2

TrafficCarPos[53] = <<-528.8509, -1036.6139, 22.4072>>
TrafficCarQuatX[53] = 0.0049
TrafficCarQuatY[53] = 0.0037
TrafficCarQuatZ[53] = 0.0027
TrafficCarQuatW[53] = 1.0000
TrafficCarRecording[53] = 54
TrafficCarStartime[53] = 163641.0000
TrafficCarModel[53] = surge

TrafficCarPos[54] = <<-539.1306, -985.1822, 22.7948>>
TrafficCarQuatX[54] = 0.0039
TrafficCarQuatY[54] = 0.0031
TrafficCarQuatZ[54] = 0.0193
TrafficCarQuatW[54] = 0.9998
TrafficCarRecording[54] = 55
TrafficCarStartime[54] = 167469.0000
TrafficCarModel[54] = Seminole

TrafficCarPos[55] = <<-539.5681, -1047.6665, 22.1188>>
TrafficCarQuatX[55] = -0.0036
TrafficCarQuatY[55] = -0.0048
TrafficCarQuatZ[55] = 0.9990
TrafficCarQuatW[55] = 0.0442
TrafficCarRecording[55] = 56
TrafficCarStartime[55] = 167535.0000
TrafficCarModel[55] = sabregt

TrafficCarPos[56] = <<-533.1873, -947.7057, 23.1491>>
TrafficCarQuatX[56] = 0.0039
TrafficCarQuatY[56] = -0.0060
TrafficCarQuatZ[56] = 0.9716
TrafficCarQuatW[56] = 0.2363
TrafficCarRecording[56] = 57
TrafficCarStartime[56] = 172419.0000
TrafficCarModel[56] = pcj

TrafficCarPos[57] = <<-531.2491, -943.5247, 23.0656>>
TrafficCarQuatX[57] = 0.0077
TrafficCarQuatY[57] = -0.0062
TrafficCarQuatZ[57] = 0.9716
TrafficCarQuatW[57] = 0.2364
TrafficCarRecording[57] = 58
TrafficCarStartime[57] = 172815.0000
TrafficCarModel[57] = zion

TrafficCarPos[58] = <<-513.6036, -893.8575, 26.8742>>
TrafficCarQuatX[58] = -0.0177
TrafficCarQuatY[58] = -0.0454
TrafficCarQuatZ[58] = 0.9911
TrafficCarQuatW[58] = 0.1242
TrafficCarRecording[58] = 59
TrafficCarStartime[58] = 176841.0000
TrafficCarModel[58] = asterope



TrafficCarPos[59] = <<-635.2964, -899.3182, 24.2381>>
TrafficCarQuatX[59] = 0.0147
TrafficCarQuatY[59] = 0.0027
TrafficCarQuatZ[59] = -0.0025
TrafficCarQuatW[59] = 0.9999
TrafficCarRecording[59] = 60
TrafficCarStartime[59] = 199413.0000
TrafficCarModel[59] = prairie

TrafficCarPos[60] = <<-645.8241, -887.8140, 24.5099>>
TrafficCarQuatX[60] = -0.0213
TrafficCarQuatY[60] = 0.0009
TrafficCarQuatZ[60] = 0.9998
TrafficCarQuatW[60] = 0.0015
TrafficCarRecording[60] = 61
TrafficCarStartime[60] = 199545.0000
TrafficCarModel[60] = ruiner


ENDPROC*/

 
/// PURPOSE:
///    Helper function to get a clients game/mission state
/// PARAMS:
///    iPlayer - The player's game state you want.
/// RETURNS:
///    The game state.
FUNC INT GET_CLIENT_MISSION_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iGameState
ENDFUNC

FUNC INT GET_CLIENT_FMINTRO_STATE(INT iPlayer)
	RETURN playerBD[iPlayer].iFmIntroState
ENDFUNC

/// PURPOSE:
///    Helper function to get the servers game/mission state
/// RETURNS:
///    The server game state.
FUNC INT GET_SERVER_MISSION_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC

//Returns true when all the required NetIDs exist
FUNC BOOL DO_REQUIRED_NET_IDS_EXIST()

	
	RETURN TRUE
ENDFUNC

PROC CREATE_SEQUENCES()
	OPEN_SEQUENCE_TASK(seqOutCar)
		TASK_LEAVE_ANY_VEHICLE(NULL)
		TASK_TURN_PED_TO_FACE_COORD(NULL, <<17.0693, -1115.9354, 28.7968>>)
	CLOSE_SEQUENCE_TASK(seqOutCar)
	
ENDPROC

FUNC BOOL HAS_PLAYER_PURCHASED_ITEM_FROM_ANY_GUN_SHOP()
	RETURN (HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(GUN_SHOP_01_DT) 
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(GUN_SHOP_02_SS )
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(GUN_SHOP_03_HW )
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(GUN_SHOP_04_ELS)
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(GUN_SHOP_05_PB )
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(GUN_SHOP_06_LS )
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(GUN_SHOP_07_MW )
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(GUN_SHOP_08_CS )
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(GUN_SHOP_09_GOH)
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(GUN_SHOP_10_VWH)
	OR HAS_PLAYER_PURCHASED_ITEM_FROM_SHOP(GUN_SHOP_11_ID1))
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ANY_GUN_SHOP()
	RETURN (IS_PLAYER_IN_SHOP(GUN_SHOP_01_DT) 
	OR IS_PLAYER_IN_SHOP(GUN_SHOP_02_SS )
	OR IS_PLAYER_IN_SHOP(GUN_SHOP_03_HW )
	OR IS_PLAYER_IN_SHOP(GUN_SHOP_04_ELS)
	OR IS_PLAYER_IN_SHOP(GUN_SHOP_05_PB )
	OR IS_PLAYER_IN_SHOP(GUN_SHOP_06_LS )
	OR IS_PLAYER_IN_SHOP(GUN_SHOP_07_MW )
	OR IS_PLAYER_IN_SHOP(GUN_SHOP_08_CS )
	OR IS_PLAYER_IN_SHOP(GUN_SHOP_09_GOH)
	OR IS_PLAYER_IN_SHOP(GUN_SHOP_10_VWH)
	OR IS_PLAYER_IN_SHOP(GUN_SHOP_11_ID1))
ENDFUNC

FUNC BOOL IS_PLAYER_BROWSING_IN_ANY_GUN_SHOP()
	RETURN (IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_01_DT) 
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_02_SS )
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_03_HW )
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_04_ELS)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_05_PB )
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_06_LS )
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_07_MW )
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_08_CS )
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_09_GOH)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_10_VWH)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_11_ID1))
ENDFUNC

PROC BLOCK_DIALOGUE_FOR_ALL_GUNSHOPS(BOOL bBlock = TRUE)
	#IF IS_DEBUG_BUILD
		IF bBlock
			NET_DW_PRINT("[BLOCK_DIALOGUE_FOR_ALL_GUNSHOPS] Setting SET_SHOP_DIALOGUE_IS_BLOCKED for gunshops")
		ELSE	
			NET_DW_PRINT("[BLOCK_DIALOGUE_FOR_ALL_GUNSHOPS] Clearing SET_SHOP_DIALOGUE_IS_BLOCKED for gunshops")
		ENDIF
	#ENDIF
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_01_DT, bBlock)
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_02_SS , bBlock)
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_03_HW , bBlock)
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_04_ELS, bBlock)
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_05_PB , bBlock)
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_06_LS , bBlock)
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_07_MW , bBlock)
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_08_CS , bBlock)
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_09_GOH, bBlock)
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_10_VWH, bBlock)
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_11_ID1, bBlock)
	
ENDPROC

PROC MAINTAIN_PLAYER_CASH(INT iMinAmount = 400)
	INT iCurrentCash
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		iCurrentCash = GET_PLAYER_CASH(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		//	NET_DW_PRINT_STRING_INT("Current cash... ", iCurrentCash)
		#ENDIF
		IF iCurrentCash < iMinAmount	
			#IF IS_DEBUG_BUILD
				NET_DW_PRINT_STRING_INT("[MAINTAIN_PLAYER_CASH] Giving cash... ", (iMinAmount - iCurrentCash))
			#ENDIF
			IF USE_SERVER_TRANSACTIONS()
				INT iTransactionID
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_HOLDUPS, iMinAmount - iCurrentCash, iTransactionID)
			ELSE
				NETWORK_EARN_FROM_HOLDUPS(iMinAmount - iCurrentCash)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/*
PROC DO_FAKE_ACTIVITY_CORONA()
	IF IS_BIT_SET(iBoolsBitSet, biL_showFakeCorona)
		VECTOR VtempCoord = GET_FMMC_MINI_GAME_START_LOCATION(FMMC_TYPE_MG_SHOOTING_RANGE)
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_InitFakeCoronaMessage)
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), VtempCoord) <= 22.0
				IF Request_Locate_Message(storeLocateMessageID)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Got storeLocateMessageID for fake corona") #ENDIF
					Store_Locate_Message_Details(storeLocateMessageID, eFM_SHOOTING_RANGE, NO_MISSION_VARIATION)
					SET_BIT(iBoolsBitSet, biL_InitFakeCoronaMessage)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), VtempCoord) <= 25.0
					Display_Locate_Message(storeLocateMessageID,VtempCoord + <<0.0, 0.0, 1.0>>)
				ELSE
					CLEAR_BIT(iBoolsBitSet, biL_InitFakeCoronaMessage)
				ENDIF
			ENDIF
			
		ENDIF
		
		INT Red, Green, Blue, iAlpha
		GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, Red, Green, Blue, iAlpha)
		DRAW_MARKER(MARKER_CYLINDER, VtempCoord - <<0.0, 0.0, 1.2>>, <<0,0,0>>, <<0,0,0>>, <<2.6, 2.6, 2.0>>,  Red, Green, Blue, iAlpha)  
	ENDIF
ENDPROC
*/


PROC DO_SOCIAL_CLUB_HELP()
	GAMER_HANDLE gamerPlayer =  GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	BOOL bJoinedSocial = NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
	BOOL bJoinedCrew = IS_PLAYER_IN_ACTIVE_CLAN(gamerPlayer)
	
	#IF IS_DEBUG_BUILD
		IF bJoinedSocial
			NET_DW_PRINT("[DO_SOCIAL_CLUB_HELP] Player has social club account")
		ELSE
			NET_DW_PRINT("[DO_SOCIAL_CLUB_HELP] Player doesn't have social club account")
		ENDIF
		
		IF bJoinedCrew
			NET_DW_PRINT("[DO_SOCIAL_CLUB_HELP] Player is in a crew")
		ELSE
			NET_DW_PRINT("[DO_SOCIAL_CLUB_HELP] Player is NOT in a crew")
		ENDIF
	#ENDIF


	IF NOT bJoinedSocial
	AND NOT bJoinedCrew
		//--Player not in a crew and hasn't joined social club
		
		// Log onto www.rockstargames.com to join the Social Club and also to join a Crew. You can also check your progress and see how you compare to other players.
		PRINT_HELP_NO_SOUND("HLP_TAXI3", 9000) 
	ELIF bJoinedSocial
	AND NOT bJoinedCrew
		//-- Player joined social club, but not in a crew
		
		// Log onto www.rockstargames.com to join a Crew. You can also check your progress and see how you compare to other players.
		PRINT_HELP_NO_SOUND("HLP_SOC2", 9000) 
	ELIF bJoinedSocial
	AND bJoinedCrew
		//-- Player joined social club and in a crew
		
		// Log onto www.rockstargames.com to check your progress and see how you compare to other players.
		PRINT_HELP_NO_SOUND("HLP_SOC1", 9000) 
	ELSE	
		#IF IS_DEBUG_BUILD
			NET_DW_PRINT("[DO_SOCIAL_CLUB_HELP] Didn't get social club help!")
		#ENDIF
	ENDIF
	
	
	// Log onto www.rockstargames.com to join the Social Club and also to join a Crew. You can also check your progress and see how you compare to other players.
//	PRINT_HELP("HLP_TAXI3", 9000) 
ENDPROC

FUNC BOOL IS_SOCIAL_CLUB_HELP_BEING_DISPLAYED()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLP_TAXI3")
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLP_SOC1")
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLP_SOC2")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

SCRIPT_TIMER timePause
PROC DO_INTRO_HELP(FM_CUT_STRUCT &FMCutData)
	BOOL bPauseMenu = IS_PAUSE_MENU_ACTIVE()
	
	SWITCH iHelpTextProg
		CASE 0
		//	IF IS_BIT_SET(iBoolsBitSet2, biL2_DoHelpText)
			IF IS_BIT_SET(FMCutData.iBitset, biDisplayFMIntroHelp)
				IF NOT HAS_NET_TIMER_STARTED(timePause)
					START_NET_TIMER(timePause)
				ENDIF
				IF NOT bPauseMenu
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//						CLEANUP_AIRPORT_SCENARIO_PEDS(FmIntroCutscene)
						PRINT_HELP_NO_SOUND("HLP_XP1", DEFAULT_HELP_TEXT_TIME-1000) // As you play you will gain experience points (XP). Winning Races, performing Jobs and taking out enemies will increase your XP.
						RESET_NET_TIMER(timePause)
						START_NET_TIMER(timePause)
						iHelpTextProg++
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iHelpTextProg = ", iHelpTextProg) #ENDIF
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(timePause, 5000)
						iHelpTextProg++
						RESET_NET_TIMER(timePause)
						START_NET_TIMER(timePause)
//						CLEANUP_AIRPORT_SCENARIO_PEDS(FmIntroCutscene)
						#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Pause menu active iHelpTextProg = ", iHelpTextProg) #ENDIF
					ENDIF
				ENDIF
			ENDIF	
		BREAK
		
		CASE 1
			IF NOT bPauseMenu
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP_NO_SOUND("HLP_XP2", DEFAULT_HELP_TEXT_TIME-1000) // You can gain Rank by taking part in any currently available Job. 
					RESET_NET_TIMER(timePause)
					START_NET_TIMER(timePause)
					iHelpTextProg++
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iHelpTextProg = ", iHelpTextProg) #ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timePause, 5000)
					iHelpTextProg++
					RESET_NET_TIMER(timePause)
					START_NET_TIMER(timePause)
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Pause menu active iHelpTextProg = ", iHelpTextProg) #ENDIF
				ENDIF
			ENDIF
			
			
		BREAK
		
		CASE 2
			IF NOT bPauseMenu
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP_NO_SOUND("HLP_RNK1", DEFAULT_HELP_TEXT_TIME-1000) // Your Rank is shown at the top of the screen. Press ~INPUT_HUD_SPECIAL~ to display.
					
					SET_DPADDOWN_ACTIVE(TRUE)
					MPGlobalsAmbience.bFMIntroCutDemandingDpadDownActive = TRUE
					DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
					
					//SET_XP_BAR_ACTIVE(TRUE)
					//uncomment this to bring it back, causes trouble SET_XP_BAR_ACTIVE_IN_CUTSCENE(TRUE)
					RESET_NET_TIMER(timePause)
					START_NET_TIMER(timePause)
					iHelpTextProg++
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iHelpTextProg = ", iHelpTextProg) #ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timePause, 5000)
					iHelpTextProg++
					RESET_NET_TIMER(timePause)
					START_NET_TIMER(timePause)
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Pause menu active iHelpTextProg = ", iHelpTextProg) #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT bPauseMenu
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					//SET_XP_BAR_ACTIVE(FALSE)
					SET_XP_BAR_ACTIVE_IN_CUTSCENE(FALSE)
					IF IS_XBOX360_VERSION()
					OR IS_XBOX_PLATFORM()
						PRINT_HELP_NO_SOUND("HLP_CASHXBX", DEFAULT_HELP_TEXT_TIME-1000) //Cash is the key to success in GTA Online. Buy apartments, vehicles and entertain yourself and others with various activities. You can earn Cash or purchase from the Xbox Games Store.
					ELIF IS_PS3_VERSION()
					OR IS_PLAYSTATION_PLATFORM()
						PRINT_HELP_NO_SOUND("HLP_CASHPSN", DEFAULT_HELP_TEXT_TIME-1000) //Cash is the key to success in GTA Online. Buy apartments, vehicles and entertain yourself and others with various activities. You can earn Cash or purchase from the PlayStation®Store.
					ELSE
						PRINT_HELP_NO_SOUND("HLP_CASHGEN", DEFAULT_HELP_TEXT_TIME-1000) //Cash is the key to success in GTA Online. Buy apartments, vehicles and entertain yourself and others with various activities. You can earn Cash or purchase from the Store in the Pause Menu.
					ENDIF
					RESET_NET_TIMER(timePause)
					START_NET_TIMER(timePause)
					iHelpTextProg = 99
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iHelpTextProg = ", iHelpTextProg) #ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(timePause, 5000)
					iHelpTextProg = 99
					RESET_NET_TIMER(timePause)
					START_NET_TIMER(timePause)
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Pause menu active iHelpTextProg = ", iHelpTextProg) #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		/*CASE 4
			IF NOT bPauseMenu
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					
					DO_SOCIAL_CLUB_HELP()
					RESET_NET_TIMER(timeIntro)
					START_NET_TIMER(timeIntro)
					SET_BIT(iBoolsBitSet, bil_DoneRankHelp)
					SET_BIT(iBoolsBitSet, biL_DoneSocialHelp)
					RESET_NET_TIMER(timePause)
					START_NET_TIMER(timePause)
					iHelpTextProg = 5
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iHelpTextProg = ", iHelpTextProg) #ENDIF
				ENDIF
			ELSE	
				IF HAS_NET_TIMER_EXPIRED(timePause, 5000)
					iHelpTextProg = 5
					SET_BIT(iBoolsBitSet, bil_DoneRankHelp)
					SET_BIT(iBoolsBitSet, biL_DoneSocialHelp)
					RESET_NET_TIMER(timePause)
					START_NET_TIMER(timePause)
					RESET_NET_TIMER(timeIntro)
					START_NET_TIMER(timeIntro)
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Pause menu active iHelpTextProg = ", iHelpTextProg) #ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT bPauseMenu
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					
					PRINT_HELP_NO_SOUND("HLP_FEED") // Keep an eye on the Feed above the Radar for important Online messages.
					
					RESET_NET_TIMER(timePause)
					START_NET_TIMER(timePause)
					iHelpTextProg = 99
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("iHelpTextProg = ", iHelpTextProg) #ENDIF
				ENDIF
			ELSE	
				IF HAS_NET_TIMER_EXPIRED(timePause, 5000)
					iHelpTextProg = 99
					RESET_NET_TIMER(timePause)
					START_NET_TIMER(timePause)
					#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Pause menu active iHelpTextProg = ", iHelpTextProg) #ENDIF
				ENDIF
			ENDIF
		BREAK*/
		
		CASE 99
			IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_DoneHelpText)
				SET_BIT(iBoolsBitSet2, biL2_DoneHelpText)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC RUN_INTRO_CUTSCENE_CLIENT()
	IF RUN_FREEMODE_INTRO_CUTSCENE(FmIntroCutscene, playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset)
		playerBD[PARTICIPANT_ID_TO_INT()].iFmIntroState = FM_INTRO_FADE_IN
		#IF IS_DEBUG_BUILD NET_DW_PRINT("iFmIntroState FM_INTRO_RUN_CUTSCENE -> FM_INTRO_FADE_IN") #ENDIF
	ENDIF
	
	DO_INTRO_HELP(FmIntroCutscene)
ENDPROC
PROC SET_AMMU_AVAILABLE_FOR_INTRO()
	SET_COUNTRYSIDE_AMMU_AVAILABLE(FALSE)
	//SET_CITY_AMMU_AVAILABLE(FALSE)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_01_DT, FALSE)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_03_HW, FALSE)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_04_ELS, FALSE)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_07_MW, FALSE)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_06_LS, TRUE)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_11_ID1, TRUE)
	#IF IS_DEBUG_BUILD NET_DW_PRINT("SET_AMMU_AVAILABLE_FOR_INTRO called") #ENDIF
ENDPROC
PROC FADE_IN_CLIENT()
	//SET_AMMU_AVAILABLE_FOR_INTRO()
	DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
	IF NOT bDoIntroCut
	OR bDoNewIntroCut
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	//		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
	IF NOT bDoNewIntroCut
		playerBD[PARTICIPANT_ID_TO_INT()].iFmIntroState = FM_INTRO_STATE_GET_TO_AMMU
		#IF IS_DEBUG_BUILD NET_DW_PRINT("iFmIntroState FM_INTRO_FADE_IN -> FM_INTRO_STATE_GET_TO_AMMU") #ENDIF
	ELSE
		playerBD[PARTICIPANT_ID_TO_INT()].iFmIntroState = FM_INTRO_STAGE_LEAVE_AMMU
		#IF IS_DEBUG_BUILD NET_DW_PRINT("iFmIntroState FM_INTRO_FADE_IN -> FM_INTRO_STAGE_LEAVE_AMMU") #ENDIF
	ENDIF
	
ENDPROC


PROC GET_TO_AMMU_CLIENT()
	
	IF IS_LOCAL_PLAYER_RUNNING_FM_INTRO_CUT()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			SET_LOCAL_PLAYER_RUNNING_FM_INTRO_CUT(FALSE)
		ENDIF
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(timePhoncallTimeout)
		START_NET_TIMER(timePhoncallTimeout)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("Started timePhoncallTimeout") #ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTextMsg)
	//	IF Request_MP_Comms_Txtmsg(CHAR_LAMAR, "CELL_INTRO", DEFAULT, DEFAULT)
			SET_BIT(iBoolsBitSet, biL_DoneTextMsg)
	//		ADD_CONTACT_TO_PHONEBOOK (CHAR_LAMAR, MULTIPLAYER_BOOK, FALSE)
	//	ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneTaxiHelp)
		IF NOT HAS_NET_TIMER_STARTED(timeIntro)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Started timeIntro") #ENDIF
			START_NET_TIMER(timeIntro)
		ELSE	
			IF IS_BIT_SET(iBoolsBitSet, biL_DoneTextMsg)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF HAS_NET_TIMER_EXPIRED(timeIntro, 3000)
							//CLEAR_BIT(MPGlobalsAmbience.iTaxiBitSet, biG_OkToDisplayTaxiMeter)
							//#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleared biG_OkToDisplayTaxiMeter") #ENDIF
							SET_BIT(iBoolsBitSet, biL_DoneTaxiHelp)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	/*	IF IS_BIT_SET(MPGlobalsAmbience.iTaxiBitSet, biG_TaxiBlipExists)
			IF NOT IS_PAUSE_MENU_ACTIVE()
				PRINT_HELP("HLP_TAXI2", 5000) // Jump in a Cab ~BLIP_TAXI~~s~.They will take you where you want to go in Los Santos. Press and Hold ~INPUT_ENTER~ to enter a Cab as a passenger.
				SET_BIT(iBoolsBitSet, biL_DoneTaxiHelp)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_DoneTaxiHelp as help displayed") #ENDIF
			ENDIF
		ENDIF */
		
		IF HAS_NET_TIMER_EXPIRED(timeIntro, 40000)
			SET_BIT(iBoolsBitSet, biL_DoneTaxiHelp)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_DoneTaxiHelp as timer expired") #ENDIF
		ENDIF
	ELSE	 
		IF NOT IS_BIT_SET(iBoolsBitSet,biL_DoneTaxiHelp2 ) //biL_DoneAmmuHelp
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT IS_PAUSE_MENU_ACTIVE()
				//	PRINT_HELP("HLP_TAXI", 5000) // You can also call for a cab on your mobile phone. 
					SET_BIT(iBoolsBitSet,biL_DoneTaxiHelp2 )
					RESET_NET_TIMER(timeIntro)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_DoneTaxiHelp2") #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI) //IS_BIT_SET(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerUsingTaxi)
		IF NOT HAS_NET_TIMER_STARTED(timeTaxiSetOff)
			//IF HAS_PLAYER_CHOSEN_TAXI_DESTINATION() 
				START_NET_TIMER(timeTaxiSetOff)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Starting timeTaxiSetOff as taxi set off") #ENDIF
		//	ENDIF
		ELSE
			IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_DoHelpText)
				IF HAS_NET_TIMER_EXPIRED(timeTaxiSetOff, 10000)
					SET_BIT(iBoolsBitSet2, biL2_DoHelpText)
					RESET_NET_TIMER(timeTaxiSetOff)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Starting biL2_DoHelpText") #ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iBoolsBitSet, bil_DoneRankHelp)
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoPhoneCall)
						IF NOT IS_SOCIAL_CLUB_HELP_BEING_DISPLAYED() 
						OR HAS_NET_TIMER_EXPIRED(timeIntro, 5000)
							//SET_XP_BAR_ACTIVE(FALSE)
							SET_XP_BAR_ACTIVE_IN_CUTSCENE(FALSE)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_DoPhoneCall as done help") #ENDIF
							SET_BIT(iBoolsBitSet, biL_DoPhoneCall)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID()) // 961426
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableCameraAnimations, FALSE)
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, FALSE)
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			/*
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneSocialHelp)
				IF HAS_NET_TIMER_EXPIRED(timeTaxiSetOff, 10000)
					IF NOT IS_PAUSE_MENU_ACTIVE()
						DO_SOCIAL_CLUB_HELP()
						SET_BIT(iBoolsBitSet, biL_DoneSocialHelp)
						RESET_NET_TIMER(timeTaxiSetOff)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("Starting biL_DoneSocialHelp") #ENDIF
					ENDIF
					
					
				ENDIF
			ELSE	
				IF NOT IS_BIT_SET(iBoolsBitSet, bil_DoneRankHelp)
					IF NOT IS_SOCIAL_CLUB_HELP_BEING_DISPLAYED() //IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLP_TAXI3") 
						IF NOT IS_PAUSE_MENU_ACTIVE()
						//	PRINT_HELP("FM_IHELP_INT2", 4000) // You can gain Rank by taking part in any currently available Activity. 
							DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
							SET_BIT(iBoolsBitSet, biL_ShowRankBar)
							RESET_NET_TIMER(timeIntro)
							START_NET_TIMER(timeIntro)
						
							//SET_XP_BAR_ACTIVE(TRUE)
							SET_XP_BAR_ACTIVE_IN_CUTSCENE(TRUE)
						
							SET_BIT(iBoolsBitSet, bil_DoneRankHelp)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Done rank help") #ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoPhoneCall)
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_INT2")
						OR HAS_NET_TIMER_EXPIRED(timeIntro, 5000)
							//SET_XP_BAR_ACTIVE(FALSE)
							SET_XP_BAR_ACTIVE_IN_CUTSCENE(FALSE)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_DoPhoneCall as done rank help") #ENDIF
							SET_BIT(iBoolsBitSet, biL_DoPhoneCall)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID()) // 961426
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableCameraAnimations, FALSE)
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, FALSE)
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			*/
		ENDIF
	ENDIF
	
	
	
	//-- First phonecall
	IF IS_BIT_SET(iBoolsBitSet, biL_DoPhoneCall)
		
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneFirstCall)
			IF IS_SCREEN_FADED_IN()
				
				INT phonecallModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR
				SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_NO_HANGUP_FOR_PHONECALL)
				SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER)
				SET_BIT(phonecallModifiers, MP_COMMS_MODIFIER_NO_HANGUP_ON_PAUSEMENU)
				IF Request_MP_Comms_Message(sSpeech, CHAR_LAMAR, "FM_1AU", "FM_ICALL", phonecallModifiers) 
					DISABLE_CELLPHONE_DIALOGUE_LINE_SKIP (TRUE)
					SET_BIT(iBoolsBitSet, biL_DoneFirstCall)
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_AddedCOntact)
						ADD_CONTACT_TO_PHONEBOOK (CHAR_LAMAR, MULTIPLAYER_BOOK)
						SET_BIT(iBoolsBitSet, biL_AddedCOntact)
					ENDIF
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_USE_MOBILE_PHONE(PLAYER_PED_ID(), TRUE)
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("Given TASK_USE_MOBILE_PHONE") 
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							NET_DW_PRINT("Can't Give TASK_USE_MOBILE_PHONE as player ped injured!") 
						#ENDIF
					ENDIF
					
					
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT("Started timeForceSkipTaxiJourney ") 
					#ENDIF
				ENDIF
			ENDIF
		ELSE	
			IF NOT IS_PHONE_ONSCREEN()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT Is_MP_Comms_Still_Playing()
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_InitialCallFinished)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID()) // 961426
								TASK_USE_MOBILE_PHONE(PLAYER_PED_ID(), FALSE)
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableCameraAnimations, TRUE)
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, TRUE)
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, TRUE)
							ENDIF
							SET_BIT(iBoolsBitSet, biL_InitialCallFinished)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Initial call finished") #ENDIF
							#IF IS_DEBUG_BUILD NET_DW_PRINT("timeAllowTaxiSkip") #ENDIF
							START_NET_TIMER(timeAllowTaxiSkip)
							START_NET_TIMER(timeForceSkipTaxiJourney)
						ENDIF
					ENDIF
				ENDIF
			ENDIF 
		ENDIF
		
		IF IS_BIT_SET(iBoolsBitSet, biL_InitialCallFinished) 
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneSocialHelp)
				IF NOT IS_PAUSE_MENU_ACTIVE()
					//-- Incase we didn't get it first time.
					DO_SOCIAL_CLUB_HELP()
					SET_BIT(iBoolsBitSet, biL_DoneSocialHelp)
					RESET_NET_TIMER(timeIntro)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_DoneSocialHelp, but not in taxi") #ENDIF		
				ENDIF
			ELSE
				IF NOT IS_SOCIAL_CLUB_HELP_BEING_DISPLAYED() //IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLP_TAXI3") // Social club help
					IF NOT IS_BIT_SET(iBoolsBitSet, biL_SetTaxiForceSkip)
						IF HAS_NET_TIMER_EXPIRED(timeForceSkipTaxiJourney, 4500) // 65000
							SET_BIT(iBoolsBitSet, biL_SetTaxiForceSkip)
						//	SET_BIT(MPGlobalsAmbience.iTaxiBitSet, biG_SkipJourneyForIntro)
							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ForceTaxiSkip)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Set biL_SetTaxiForceSkip to force taxi skip") #ENDIF	
						ENDIF
					ENDIF
					IF HAS_NET_TIMER_EXPIRED(timeAllowTaxiSkip, 15000)
						//-- Allow taxi skip
					/*	IF IS_TAXI_SKIP_DISABLED() 
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Enabled taxi skip") #ENDIF		
							ENABLE_TAXI_SKIP() 
						ENDIF
						IF IS_TAXI_TUTORIAL_FLAG_SET() 
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Clear Taxi Tutorial Flag") #ENDIF		
							CLEAR_TAXI_TUTORIAL_FLAG()
						ENDIF */
						
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_EnabledTaxiHelp)
							//-- Needed to enable help text in the taxi
							//SET_BIT(MPGlobalsAmbience.iTaxiBitSet, biG_DisplayTaxiHelpForTut)
							SET_BIT(iBoolsBitSet, biL_EnabledTaxiHelp)
						//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Set biL_EnabledTaxiHelp") #ENDIF
						ENDIF 
					ENDIF
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF IS_BIT_SET(iBoolsBitSet, biL_ToldToLoseWAnted)
							CLEAR_BIT(iBoolsBitSet, biL_ToldToLoseWAnted)
						ENDIF
					//	IF NOT IS_PLAYER_IN_ANY_GUN_SHOP()
						IF NOT IS_PLAYER_IN_SHOP(shopAmmu)
							IF IS_BIT_SET(iBoolsBitSet, biL_ToldToBuyPistol)
								CLEAR_BIT(iBoolsBitSet, biL_ToldToBuyPistol)
							ENDIF
							IF IS_BIT_SET(iBoolsBitSet, biL_GivenAmmuObjOnce)
							OR (NOT IS_BIT_SET(iBoolsBitSet, biL_GivenAmmuObjOnce) AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())) //--Wait until player out taxi
								IF NOT IS_BIT_SET(iBoolsBitSet, biL_PlayerToldToGetToAmmu)
									IF IS_BIT_SET(iBoolsBitSet, biL_RemovedGpsForTaxi)
										CLEAR_BIT(iBoolsBitSet, biL_RemovedGpsForTaxi)
									ENDIF
									IF DOES_BLIP_EXIST(g_sShopSettings.blipID[shopAmmu])
										Print_Objective_Text("GET_AMMU")
										SET_BIT(iBoolsBitSet, biL_PlayerToldToGetToAmmu)
										SET_BIT(iBoolsBitSet, biL_GivenAmmuObjOnce)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Done get to Ammu objective text ") #ENDIF
										IF DOES_BLIP_EXIST(g_sShopSettings.blipID[shopAmmu])
											IF NOT IS_BIT_SET(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerUsingTaxi)
												SET_BLIP_ROUTE(g_sShopSettings.blipID[shopAmmu], TRUE)
												#IF IS_DEBUG_BUILD NET_DW_PRINT("Gun shop blip gps added") #ENDIF
											ENDIF
											SET_SHOP_BLIP_LONG_RANGE(shopAmmu  , TRUE) 
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Gun shop blip set long range") #ENDIF
										ELSE	
											#IF IS_DEBUG_BUILD NET_DW_PRINT("Blip g_sShopSettings.blipID[shopAmmu] doesn't exist!") #ENDIF
										ENDIF 
									ELSE	
										#IF IS_DEBUG_BUILD NET_DW_PRINT("Blip g_sShopSettings.blipID[shopAmmu] doesn't exist!") #ENDIF
									ENDIF
								ELSE	
									IF IS_BIT_SET(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerUsingTaxi)
										IF DOES_BLIP_EXIST(g_sShopSettings.blipID[shopAmmu])
											IF NOT IS_BIT_SET(iBoolsBitSet, biL_RemovedGpsForTaxi)
												//-- Don't have GPS route if player is in taxi
												SET_BLIP_ROUTE(g_sShopSettings.blipID[shopAmmu], FALSE)
												SET_BIT(iBoolsBitSet, biL_RemovedGpsForTaxi)
												#IF IS_DEBUG_BUILD NET_DW_PRINT("Removing GPS as player in Taxi") #ENDIF
											ENDIF
										ENDIF
									ELSE
										IF DOES_BLIP_EXIST(g_sShopSettings.blipID[shopAmmu])
											IF IS_BIT_SET(iBoolsBitSet, biL_RemovedGpsForTaxi)
												SET_BLIP_ROUTE(g_sShopSettings.blipID[shopAmmu], TRUE)
												#IF IS_DEBUG_BUILD NET_DW_PRINT("Adding GPS as player no longer in Taxi") #ENDIF
												CLEAR_BIT(iBoolsBitSet, biL_RemovedGpsForTaxi)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE	
							IF IS_BIT_SET(iBoolsBitSet, biL_PlayerToldToGetToAmmu)
								CLEAR_BIT(iBoolsBitSet, biL_PlayerToldToGetToAmmu)
							ENDIF
							IF NOT IS_BIT_SET(iBoolsBitSet, biL_ToldToBuyPistol)
								Print_Objective_Text("GET_GUN")
								SET_LOCAL_PLAYER_READY_TO_PRELOAD_LAMAR_ASSETS(TRUE)
								SET_BIT(iBoolsBitSet, biL_ToldToBuyPistol)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Doing buy pistol objective text") #ENDIF
							ENDIF
						ENDIF
					
					ELSE
						//-- PLayer has a wanted level
						IF NOT IS_BIT_SET(iBoolsBitSet, biL_ToldToLoseWAnted)
							CLEAR_HELP()
							IF IS_BIT_SET(iBoolsBitSet, biL_PlayerToldToGetToAmmu)
								CLEAR_BIT(iBoolsBitSet, biL_PlayerToldToGetToAmmu)
							ENDIF
							IF IS_BIT_SET(iBoolsBitSet, biL_ToldToBuyPistol)
								CLEAR_BIT(iBoolsBitSet, biL_ToldToBuyPistol)
							ENDIF
							IF DOES_BLIP_EXIST(g_sShopSettings.blipID[shopAmmu])
								SET_BLIP_ROUTE(g_sShopSettings.blipID[shopAmmu], FALSE)
								SET_BLIP_FLASHES(g_sShopSettings.blipID[shopAmmu], FALSE)
								SET_SHOP_BLIP_LONG_RANGE(shopAmmu  , FALSE) 
								#IF IS_DEBUG_BUILD NET_DW_PRINT("Cleared gunshop blip as player has wanted") #ENDIF
							ENDIF 
							
							Print_Objective_Text("LOSE_COP") //-- Lose the cops
							SET_BIT(iBoolsBitSet, biL_ToldToLoseWAnted)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		//-- Timeout for phonecall
		IF HAS_NET_TIMER_EXPIRED(timePhoncallTimeout, 300000) // 
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Setting biL_DoPhoneCall as max time expired") #ENDIF
			SET_BIT(iBoolsBitSet, biL_DoPhoneCall)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_BoughtPistol)
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
		OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL)
		OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL)
		OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG)
			IF HAS_PLAYER_PURCHASED_ITEM_FROM_ANY_GUN_SHOP()
			#IF IS_DEBUG_BUILD OR IS_BIT_SET(iDebugBitSet, biD_GiveGunForDebug) #ENDIF
				BLOCK_DIALOGUE_FOR_ALL_GUNSHOPS()
				SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_BoughtPistol)
			//	IF IS_PLAYER_IN_ANY_GUN_SHOP()
				IF IS_PLAYER_IN_SHOP(shopAmmu)
				//	Clear_Any_Objective_Text_From_This_Script()
				//	Print_Objective_Text("LEV_AMMU") // Leave AmmuNation.
				ENDIF
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("I have a pistol") 
					
				#ENDIF				
				
			ELSE
			ENDIF
		ELSE	
			MAINTAIN_PLAYER_CASH()
		ENDIF
	ELSE
		
		
		IF NOT IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LeftAmmu)
			IF NOT IS_PLAYER_BROWSING_IN_ANY_GUN_SHOP()
			#IF IS_DEBUG_BUILD OR IS_BIT_SET(iDebugBitSet, biD_GiveGunForDebug) #ENDIF
				SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LeftAmmu)
				Clear_Any_Objective_Text_From_This_Script()
				PRINT_HELP("FM_IHELP_AMMU")
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_SavedStat)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Saving intro complete stat MP_STAT_FM_INTRO_MISS_DONE as I have a gun") #ENDIF
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_MISS_DONE, TRUE)
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_CUT_DONE, TRUE)
//					SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_INTRO_MISS_DONE)
//					SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_INTRO_CUT_DONE)
					
				//	REQUEST_SAVE()
					SET_BIT(iBoolsBitSet, biL_SavedStat)
				ENDIF
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("I've stopped browsing Ammu") 
					IF NOT bDoCoronaTut
						NET_DW_PRINT("Moving state FM_INTRO_STATE_GET_TO_AMMU > FM_INTRO_STATE_FINISHED")
					ELSE	
						IF bDoRaceWarp
							NET_DW_PRINT("Moving state FM_INTRO_STATE_GET_TO_AMMU > FM_INTRO_STAGE_LEAVE_AMMU")
						ELSE	
							NET_DW_PRINT("Moving state FM_INTRO_STATE_GET_TO_AMMU > FM_INTRO_STATE_GET_TO_SHOOT")
						ENDIF
					ENDIF
					
				#ENDIF 
				
				RESET_NET_TIMER(timeIntro)
				IF bDoCoronaTut
					IF bDoRaceWarp
						playerBD[PARTICIPANT_ID_TO_INT()].iFmIntroState = FM_INTRO_STAGE_LEAVE_AMMU
					ELSE
						playerBD[PARTICIPANT_ID_TO_INT()].iFmIntroState = FM_INTRO_STATE_GET_TO_SHOOT
					ENDIF
				ELSE
					playerBD[PARTICIPANT_ID_TO_INT()].iFmIntroState = FM_INTRO_STATE_FINISHED
				ENDIF
			ENDIF
		
		ENDIF			
	ENDIF
	
//	DO_INTRO_HELP()
ENDPROC


PROC LEAVE_AMMU_CLIENT()
//	INT optionalParams 
//	STRING sFakeInvite = "CELL_FINV"
	SWITCH iLeaveAmmuCLientProg
		CASE RACE_WARP_LEAVE_GUNSHOP
		//	IF NOT IS_PLAYER_IN_ANY_GUN_SHOP()
			IF NOT IS_PLAYER_IN_SHOP(shopAmmu)
			
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[LEAVE_AMMU_CLIENT] Ending fm intro...") #ENDIF
				SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
				playerBD[PARTICIPANT_ID_TO_INT()].iFmIntroState = FM_INTRO_STATE_FINISHED
			//	iLeaveAmmuCLientProg = RACE_WARP_SEND_TEXT
			ENDIF
		BREAK
		
		
	ENDSWITCH
ENDPROC

PROC PROCESS_MISSION_STAGES_CLIENT()
	SWITCH GET_CLIENT_FMINTRO_STATE(PARTICIPANT_ID_TO_INT())
	
		CASE FM_INTRO_RUN_CUTSCENE
			RUN_INTRO_CUTSCENE_CLIENT()
		BREAK
		CASE FM_INTRO_FADE_IN
			FADE_IN_CLIENT()	
		BREAK
		
		CASE FM_INTRO_STATE_GET_TO_AMMU
			GET_TO_AMMU_CLIENT()
		BREAK
		
		CASE FM_INTRO_STATE_GET_TO_SHOOT
		//	GET_TO_SHOOTING_RANGE_CLIENT()
		BREAK
		
		CASE FM_INTRO_STATE_TRIG_TUT_CUT
		//	TRIGGER_TUT_CUT_CLIENT()
		BREAK
		
		CASE FM_INTRO_STAGE_LEAVE_AMMU
			LEAVE_AMMU_CLIENT()
		BREAK
	ENDSWITCH
	
	//DO_FM_INTRO_TIME_SYNC(FmIntroCutscene)
ENDPROC

PROC SERVER_REPEAT()
	INT i
	//PLAYER_INDEX playerTemp
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		//	playerTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			#IF IS_DEBUG_BUILD
				IF NOT IS_BIT_SET(serverbd.iServerGameState, biS_DebugSkipped)
					IF playerbd[i].bPressS
						SET_BIT(serverbd.iServerGameState, biS_DebugSkipped)
						NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Server thinks this player pressed S ", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
					ENDIF
				ENDIF
			#ENDIF
			
			IF bDoCoronaTut
				IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerDoneCut)
					IF IS_BIT_SET(playerbd[i].iPlayerBitSet, biP_DoneCutscene)
						SET_BIT(serverbd.iServerBitSet, biS_PlayerDoneCut)
						#IF IS_DEBUG_BUILD 
							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Server thinks this player has done trigger tur cutscene... ", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerGotWeapon)
					IF IS_BIT_SET(playerbd[i].iPlayerBitSet, biP_LeftAmmu)
						SET_BIT(serverbd.iServerBitSet, biS_PlayerGotWeapon)
						#IF IS_DEBUG_BUILD 
							NET_DW_PRINT_STRING_WITH_PLAYER_NAME("Server thinks this player has purchased a weapon and left Ammu... ", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC PROCESS_GAME_SERVER()

	
ENDPROC

//Player Control Off with Cam Control On
PROC MAKE_ONLY_PLAYER_CAMERA_WORK()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)

	IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_PlayerCOntrolTurnedOff)
		SET_BIT(iBoolsBitSet2, biL2_PlayerCOntrolTurnedOff)
		SET_LOCAL_PLAYER_IN_TAXI_FOR_INTRO(TRUE)
		#IF IS_DEBUG_BUILD NET_DW_PRINT("Set biL2_PlayerCOntrolTurnedOff") #ENDIF
	ENDIF
ENDPROC

PROC RESET_PLAYER_ONLY_CAMERA_CONTROLS()
	#IF IS_DEBUG_BUILD 
		DEBUG_PRINTCALLSTACK()
		NET_DW_PRINT("RESET_PLAYER_ONLY_CAMERA_CONTROLS called...") 
	#ENDIF
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
ENDPROC


PROC PROCESS_TAXI_BRAIN()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_CreateTaxi)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, CREATE_TAXI_FOR_INTRO)
				SET_BIT(serverbd.iServerBitSet, biS_CreateTaxi)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_CreateTaxi")
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerInTaxi)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
				SET_BIT(serverbd.iServerBitSet, biS_PlayerInTaxi)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_PlayerInTaxi")
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_SkipTaxi)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ForceTaxiSkip)
				SET_BIT(serverbd.iServerBitSet, biS_SkipTaxi)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_SkipTaxi")
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_WarpedTaxi)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WarpTaxi)
				SET_BIT(serverbd.iServerBitSet, biS_WarpedTaxi)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_WarpedTaxi")
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_TaxiArrived)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_TaxiAtDest)
				SET_BIT(serverbd.iServerBitSet, biS_TaxiArrived)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_TaxiArrived")
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerOutTaxi)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PlayerOutTaxi)
				SET_BIT(serverbd.iServerBitSet, biS_PlayerOutTaxi)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_PlayerOutTaxi")
				#ENDIF
			ENDIF
		ENDIF
		
		SWITCH serverBD.iTaxiStage
			CASE TAXI_STAGE_CREATE
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_CreateTaxi)
					IF CREATE_NON_AM_TAXI(serverBD.niTaxi, serverBD.niDriver, <<-1031.7882, -2731.8159, 19.0546>>, 240.4822, sTaxiSpeech)
						SET_VEHICLE_ENGINE_ON(NET_TO_VEH(serverBD.niTaxi), TRUE, FALSE)
						serverBD.iTaxiStage = TAXI_STAGE_PLAYER_INTO_TAXI
						#IF IS_DEBUG_BUILD 
							NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_PLAYER_INTO_TAXI")
						#ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_PLAYER_INTO_TAXI
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerInTaxi) 
					serverBD.iTaxiStage = TAXI_STAGE_DRIVE_TO_DEST
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_DRIVE_TO_DEST")
					#ENDIF
					
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_DRIVE_TO_DEST
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_SkipTaxi)
					serverBD.iTaxiStage = TAXI_STAGE_WARP_TAXI
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_WARP_TAXI")
					#ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_WARP_TAXI
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_WarpedTaxi)
					serverBD.iTaxiStage = TAXI_STAGE_PULL_OVER
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_PULL_OVER")
					#ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_PULL_OVER
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_TaxiArrived)
					serverBD.iTaxiStage = TAXI_STAGE_ARRIVED
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_ARRIVED")
					#ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_ARRIVED
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerOutTaxi)
					serverBD.iTaxiStage = TAXI_STAGE_WANDER
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_WANDER")
					#ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC PROCESS_TAXI_BODY()
	
		
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTaxi)
		SWITCH serverBD.iTaxiStage
			CASE TAXI_STAGE_PLAYER_INTO_TAXI
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, WALK_PLAYER_INTO_TAXI)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niTaxi)
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK
									
									TASK_ENTER_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niTaxi), DEFAULT_TIME_BEFORE_WARP, VS_BACK_RIGHT, PEDMOVEBLENDRATIO_WALK)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Giving player TASK_ENTER_VEHICLE ") #ENDIF
									
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
									IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niTaxi))
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
										SET_LOCAL_PLAYER_IN_TAXI_FOR_INTRO(TRUE)
									//	SET_TAXI_LIGHTS(NET_TO_VEH(serverBD.niTaxi), FALSE)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting PLAYER_SAT_IN_TAXI") #ENDIF
									ELSE	
										CLEAR_AREA_OF_VEHICLES(<<-1031.7882, -2731.8159, 19.0546>>, 50.0)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_DRIVE_TO_DEST
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niTaxi)
					IF NOT IS_NET_PED_INJURED(serverBD.niDriver)
						IF  IS_BIT_SET(iBoolsBitSet, biL_LoadAllNodes)
							
							IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
								
							//	TASK_VEHICLE_MISSION_COORS_TARGET(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niTaxi), <<17.6804, -1114.2880, 28.7970>>, MISSION_GOTO, 15.0, DrivingMode_StopForCars|DF_AdjustCruiseSpeedBasedOnRoadSpeed, 5, 5, FALSE)
								TASK_VEHICLE_MISSION_COORS_TARGET(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niTaxi), <<-663.8057, -952.3882, 20.2985>>, MISSION_GOTO, 15.0, DrivingMode_StopForCars|DF_AdjustCruiseSpeedBasedOnRoadSpeed, 5, 5, FALSE)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Giving TASK_VEHICLE_MISSION_COORS_TARGET") #ENDIF
								
							ENDIF
							
							/*
							IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
								OPEN_SEQUENCE_TASK(seqTaxi)
									TASK_VEHICLE_MISSION_COORS_TARGET(NULL, NET_TO_VEH(serverBD.niTaxi),<<-783.6407, -2324.5884, 13.8523>> , MISSION_GOTO, 15.0, DrivingMode_StopForCars|DF_AdjustCruiseSpeedBasedOnRoadSpeed, 5, 5, FALSE)
									TASK_VEHICLE_MISSION_COORS_TARGET(NULL, NET_TO_VEH(serverBD.niTaxi), <<-663.8057, -952.3882, 20.2985>>, MISSION_GOTO, 15.0, DrivingMode_StopForCars|DF_AdjustCruiseSpeedBasedOnRoadSpeed, 5, 5, FALSE)
								CLOSE_SEQUENCE_TASK(seqTaxi)
								TASK_PERFORM_SEQUENCE(NET_TO_PED(serverBD.niDriver), seqTaxi)
								CLEAR_SEQUENCE_TASK(seqTaxi)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Giving TASK_VEHICLE_MISSION_COORS_TARGET") #ENDIF
								
							ENDIF
							*/
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_WARP_TAXI
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_FadedOutForWarp)
					IF NOT IS_SCREEN_FADED_OUT()
					AND NOT IS_SCREEN_FADING_OUT()
						IF NOT IS_TRANSITION_ACTIVE()
							IF NOT IS_PAUSE_MENU_ACTIVE()
								DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Fading screen out for warp...") #ENDIF
							ENDIF
						ENDIF
					ELSE	
						IF IS_SCREEN_FADED_OUT()
							SET_BIT(iBoolsBitSet, biL_FadedOutForWarp)
						ENDIF
					ENDIF
				ELSE	
					IF NOT IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet,biP_WarpTaxi) //  ( IS_BIT_SET(iBoolsBitSet, biL_DoneWarp)
						//-- Do warp
					//	IF NET_WARP_TO_COORD(<<32.2232, -1127.5334, 28.3027>>, 91.0413, TRUE, FALSE)
					//		CLEAR_AREA_OF_VEHICLES(<<32.2232, -1127.5334, 28.3027>>, 20.0)
						IF NET_WARP_TO_COORD(<<-649.4202, -954.0247, 20.5269>>, 90.7653, TRUE, FALSE)
							CLEAR_AREA_OF_VEHICLES(<<-649.4202, -954.0247, 20.5269>>, 20.0)
							DISABLE_CINEMATIC_VEHICLE_IDLE_MODE_THIS_UPDATE()
							INVALIDATE_CINEMATIC_VEHICLE_IDLE_MODE()

							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet,biP_WarpTaxi)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Done taxi warp...") #ENDIF
						ENDIF
					ELSE	
						
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_PULL_OVER
				IF NOT IS_NET_PED_INJURED(serverBD.niDriver)
					IF NOT IS_BIT_SET(iBoolsBitSet,biL_TaxiPullOver)
					
						IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niTaxi) 
			//				TASK_VEHICLE_PARK(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niTaxi), <<18.9591, -1125.3546, 27.8204>>, 91.3972, PARK_TYPE_PARALLEL, 60, TRUE)
							TASK_VEHICLE_PARK(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niTaxi), <<-664.8411, -952.2484, 20.2830>>, 92.0219, PARK_TYPE_PARALLEL, 60, TRUE)
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							SET_BIT(iBoolsBitSet,biL_TaxiPullOver)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Given taxi TASK_VEHICLE_PARK...") #ENDIF
						ENDIF
					
					ELSE	
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_TaxiAtDest)		
							IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_VEHICLE_PARK) = FINISHED_TASK
								
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_TaxiAtDest)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting biP_TaxiAtDest") #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_ARRIVED
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> WAITING_TO_START_TASK
							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Giving player TASK_LEAVE_ANY_VEHICLE") #ENDIF
						ENDIF
						
					/*	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
							//TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqOutCar)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Giving player TASK_LEAVE_ANY_VEHICLE") #ENDIF
						ENDIF */
					ELSE
					/*	IF NOT HAS_NET_TIMER_STARTED(timeLeaveTaxi)
							START_NET_TIMER(timeLeaveTaxi)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("Started timeLeaveTaxi") #ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
							OR HAS_NET_TIMER_EXPIRED(timeLeaveTaxi, 10000) */
								IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PlayerOutTaxi)
									SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PlayerOutTaxi)
									SET_BIT(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerLeftTaxiInIntro)
									CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
								//	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									RESET_PLAYER_ONLY_CAMERA_CONTROLS()
									SET_LOCAL_PLAYER_IN_TAXI_FOR_INTRO(FALSE)
									PRINT_HELP("HLP_TAXI") // You can call for a taxi on your cell phone
									IF USE_SERVER_TRANSACTIONS()
										INT iTransactionID
										TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_TAXI, 200, iTransactionID)
									ELSE
										GIVE_LOCAL_PLAYER_CASH(-200)
										//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_TAXIS, 200)
										NETWORK_SPENT_TAXI(200)
									ENDIF
									

									#IF IS_DEBUG_BUILD 
										NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting biP_PlayerOutTaxi") 
										NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting biG_PlayerLeftTaxiInIntro") 
									#ENDIF
								ENDIF
					//		ENDIF
					//	ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_WANDER
				IF NOT IS_NET_PED_INJURED(serverBD.niDriver)
					IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niTaxi)
						IF GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(NET_TO_PED(serverBD.niDriver), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> WAITING_TO_START_TASK
							SET_PED_KEEP_TASK(NET_TO_PED(serverBD.niDriver), TRUE)
							TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niTaxi), 12, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
							CLEANUP_NET_ID(serverBD.niTaxi)
							CLEANUP_NET_ID(serverBD.niDriver)
						ENDIF 
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
		
		//--Load nodes
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, WALK_PLAYER_INTO_TAXI)
			REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(-1614.1733, -3243.3167, 28.4915, -440.9012)
			IF NOT IS_BIT_SET(iBoolsBitSet, biL_LoadAllNodes)
				SET_BIT(iBoolsBitSet, biL_LoadAllNodes)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Set biL_LoadAllNodes")  #ENDIF
			ENDIF
//			#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] REQUEST_PATH_NODES_IN_AREA_THIS_FRAME...")  #ENDIF
//			IF NOT IS_BIT_SET(iBoolsBitSet, biL_LoadAllNodes)
//				IF LOAD_ALL_PATH_NODES(TRUE)
//					SET_BIT(iBoolsBitSet, biL_LoadAllNodes)
//					#IF IS_DEBUG_BUILD 
//						NET_DW_PRINT("[PROCESS_TAXI_BODY] Loaded all nodes") 
//						SCRIPT_ASSERT("Use REQUEST_PATH_NODES_IN_AREA_THIS_FRAME")
//					#ENDIF
//				ENDIF
//			ENDIF
		ENDIF
		
		//-- Set off speech
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneSetOffSpeech)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
				IF NOT HAS_NET_TIMER_STARTED(timeTaxiSpeech)
					Enable_MP_Comms()
					START_NET_TIMER(timeTaxiSpeech)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Started timeTaxiSpeech ") #ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(timeTaxiSpeech, 5000)
						IF CREATE_CONVERSATION(sTaxiSpeech, "MPTXIAU", "MPTXI_T1", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
							SET_BIT(iBoolsBitSet, biL_DoneSetOffSpeech)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting biL_DoneSetOffSpeech") #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Arrive pseech
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneArrivedSpeech)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_TaxiAtDest)
				IF CREATE_CONVERSATION(sTaxiSpeech, "MPTXIAU", "MPTXI_AR", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
					SET_BIT(iBoolsBitSet, biL_DoneArrivedSpeech)
					#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting biL_DoneArrivedSpeech") #ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//-- Disable idle warning
		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PlayerOutTaxi)
			SET_IDLE_KICK_DISABLED_THIS_FRAME()
		ENDIF
		
		IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
					//MAKE_ONLY_PLAYER_CAMERA_WORK()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

VEHICLE_INDEX vehTaxi
PED_INDEX pedTaxi

PROC REQUEST_TAXI_MODELS_FOR_INTRO()
	REQUEST_MODEL(MPGlobalsAmbience.TaxiCarModel)
	REQUEST_MODEL(MPGlobalsAmbience.TaxiDriverModel)
ENDPROC

FUNC BOOL CREATE_FM_INTRO_TAXI(VECTOR vCreate, FLOAT fHead)
	IF NOT DOES_ENTITY_EXIST(vehTaxi)
		IF HAS_MODEL_LOADED(MPGlobalsAmbience.TaxiCarModel)
			vehTaxi = CREATE_VEHICLE(MPGlobalsAmbience.TaxiCarModel, vCreate, fHead, FALSE, FALSE)
			SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehTaxi, TRUE)
		ENDIF
	ELSE	
		IF NOT DOES_ENTITY_EXIST(pedTaxi)
			IF HAS_MODEL_LOADED(MPGlobalsAmbience.TaxiDriverModel)
				IF IS_VEHICLE_DRIVEABLE(vehTaxi)
					pedTaxi = CREATE_PED_INSIDE_VEHICLE(vehTaxi, PEDTYPE_SPECIAL, MPGlobalsAmbience.TaxiDriverModel, DEFAULT, FALSE, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTaxi, TRUE)
				
					SET_MODEL_AS_NO_LONGER_NEEDED(MPGlobalsAmbience.TaxiCarModel)
					SET_MODEL_AS_NO_LONGER_NEEDED(MPGlobalsAmbience.TaxiDriverModel)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedTaxi, FALSE)
					
					SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedTaxi, TRUE)
									
					ADD_PED_FOR_DIALOGUE(sTaxiSpeech, 8, pedTaxi, "FM_TAXI")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehTaxi)
	OR NOT DOES_ENTITY_EXIST(pedTaxi)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_TAXI_BRAIN_V2()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_CreateTaxi)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, CREATE_TAXI_FOR_INTRO)
				SET_BIT(serverbd.iServerBitSet, biS_CreateTaxi)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_CreateTaxi")
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerInTaxi)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
				SET_BIT(serverbd.iServerBitSet, biS_PlayerInTaxi)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_PlayerInTaxi")
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_SkipTaxi)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_ForceTaxiSkip)
				SET_BIT(serverbd.iServerBitSet, biS_SkipTaxi)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_SkipTaxi")
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_WarpedTaxi)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WarpTaxi)
				SET_BIT(serverbd.iServerBitSet, biS_WarpedTaxi)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_WarpedTaxi")
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_TaxiArrived)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_TaxiAtDest)
				SET_BIT(serverbd.iServerBitSet, biS_TaxiArrived)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_TaxiArrived")
				#ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerOutTaxi)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PlayerOutTaxi)
				SET_BIT(serverbd.iServerBitSet, biS_PlayerOutTaxi)
				#IF IS_DEBUG_BUILD 
					NET_DW_PRINT("[PROCESS_TAXI_BRAIN] Setting biS_PlayerOutTaxi")
				#ENDIF
			ENDIF
		ENDIF
		
		SWITCH serverBD.iTaxiStage
			CASE TAXI_STAGE_CREATE
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_CreateTaxi)
					REQUEST_VEHICLE_RECORDING(100, "FM_TAxi")
					REQUEST_TAXI_MODELS_FOR_INTRO()
					//REQUEST_MODEL(DEFAULT_PED_MODEL())
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(100, "FM_TAxi")
					//AND HAS_MODEL_LOADED(DEFAULT_PED_MODEL())
						IF CREATE_FM_INTRO_TAXI(<<-1031.7882, -2731.8159, 19.0546>>, 240.4822)
						
							SET_VEHICLE_ENGINE_ON(vehTaxi, TRUE, FALSE)
							//INITIALISE_UBER_PLAYBACK("FM_TAxi", 100)
							//SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
							//SET_UBER_PLAYBACK_TO_CLEANUP_DEFAULT_PED_MODEL(FALSE)
							//INIT_TAXI_REC_UBER_DATA()
							serverBD.iTaxiStage = TAXI_STAGE_PLAYER_INTO_TAXI
							#IF IS_DEBUG_BUILD 
								NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_PLAYER_INTO_TAXI")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_PLAYER_INTO_TAXI
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerInTaxi) 
					serverBD.iTaxiStage = TAXI_STAGE_DRIVE_TO_DEST
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_DRIVE_TO_DEST")
					#ENDIF
					
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_DRIVE_TO_DEST
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_SkipTaxi)
					serverBD.iTaxiStage = TAXI_STAGE_WARP_TAXI
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_WARP_TAXI")
					#ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_WARP_TAXI
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_WarpedTaxi)
					serverBD.iTaxiStage = TAXI_STAGE_PULL_OVER
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_PULL_OVER")
					#ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_PULL_OVER
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_TaxiArrived)
					serverBD.iTaxiStage = TAXI_STAGE_ARRIVED
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_ARRIVED")
					#ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_ARRIVED
				IF IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerOutTaxi)
					serverBD.iTaxiStage = TAXI_STAGE_WANDER
					#IF IS_DEBUG_BUILD 
						NET_DW_PRINT("[PROCESS_TAXI_BRAIN] serverBD.iTaxiStage = TAXI_STAGE_WANDER")
					#ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC PROCESS_TAXI_BODY_V2()
	/*
		Put traffic recording range up reasonably high when recording
	
	*/
		
	IF DOES_ENTITY_EXIST(vehTaxi)
		
		SWITCH serverBD.iTaxiStage
			CASE TAXI_STAGE_PLAYER_INTO_TAXI
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, WALK_PLAYER_INTO_TAXI)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(vehTaxi)
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK
									
									TASK_ENTER_VEHICLE(PLAYER_PED_ID(), vehTaxi, DEFAULT_TIME_BEFORE_WARP, VS_BACK_RIGHT, PEDMOVEBLENDRATIO_WALK)
									#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Giving player TASK_ENTER_VEHICLE ") #ENDIF
									
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
									IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehTaxi)
										SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
									//	SET_TAXI_LIGHTS(NET_TO_VEH(serverBD.niTaxi), FALSE)
										#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting PLAYER_SAT_IN_TAXI") #ENDIF
									ELSE	
										CLEAR_AREA_OF_VEHICLES(<<-1031.7882, -2731.8159, 19.0546>>, 50.0)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_DRIVE_TO_DEST
				IF IS_VEHICLE_DRIVEABLE(vehTaxi)
					IF NOT IS_PED_INJURED(pedTaxi)
						IF NOT IS_BIT_SET(iBoolsBitSet, biL2_StartedUber)
							
					 		
							SET_BIT(iBoolsBitSet, biL2_StartedUber)
						ELSE
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTaxi)
		
								SET_PLAYBACK_SPEED(vehTaxi, 1.0)
								//UPDATE_UBER_PLAYBACK(vehTaxi, 1.0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_WARP_TAXI
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_FadedOutForWarp)
					IF NOT IS_SCREEN_FADED_OUT()
					AND NOT IS_SCREEN_FADING_OUT()
						IF NOT IS_TRANSITION_ACTIVE()
							IF NOT IS_PAUSE_MENU_ACTIVE()
								DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Fading screen out for warp...") #ENDIF
							ENDIF
						ENDIF
					ELSE	
						IF IS_SCREEN_FADED_OUT()
							SET_BIT(iBoolsBitSet, biL_FadedOutForWarp)
							IF IS_VEHICLE_DRIVEABLE(vehTaxi)
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTaxi)
									STOP_PLAYBACK_RECORDED_VEHICLE(vehTaxi)
									//CLEANUP_UBER_PLAYBACK()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE	
					IF NOT IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet,biP_WarpTaxi) //  ( IS_BIT_SET(iBoolsBitSet, biL_DoneWarp)
						//-- Do warp
					//	IF NET_WARP_TO_COORD(<<32.2232, -1127.5334, 28.3027>>, 91.0413, TRUE, FALSE)
					//		CLEAR_AREA_OF_VEHICLES(<<32.2232, -1127.5334, 28.3027>>, 20.0)
						IF NET_WARP_TO_COORD(<<-649.4202, -954.0247, 20.5269>>, 90.7653, TRUE, FALSE)
							CLEAR_AREA_OF_VEHICLES(<<-649.4202, -954.0247, 20.5269>>, 20.0)
							DISABLE_CINEMATIC_VEHICLE_IDLE_MODE_THIS_UPDATE()
							INVALIDATE_CINEMATIC_VEHICLE_IDLE_MODE()

							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							SET_BIT(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet,biP_WarpTaxi)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Done taxi warp...") #ENDIF
						ENDIF
					ELSE	
						
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_PULL_OVER
				IF NOT IS_PED_INJURED(pedTaxi)
					IF NOT IS_BIT_SET(iBoolsBitSet,biL_TaxiPullOver)
					
						IF IS_VEHICLE_DRIVEABLE(vehTaxi)
			//				TASK_VEHICLE_PARK(NET_TO_PED(serverBD.niDriver), NET_TO_VEH(serverBD.niTaxi), <<18.9591, -1125.3546, 27.8204>>, 91.3972, PARK_TYPE_PARALLEL, 60, TRUE)
							TASK_VEHICLE_PARK(pedTaxi, vehTaxi, <<-664.8411, -952.2484, 20.2830>>, 92.0219, PARK_TYPE_PARALLEL, 60, TRUE)
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							SET_BIT(iBoolsBitSet,biL_TaxiPullOver)
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Given taxi TASK_VEHICLE_PARK...") #ENDIF
						ENDIF
					
					ELSE	
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_TaxiAtDest)		
							IF GET_SCRIPT_TASK_STATUS(pedTaxi, SCRIPT_TASK_VEHICLE_PARK) = FINISHED_TASK
								
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_TaxiAtDest)
								#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting biP_TaxiAtDest") #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_ARRIVED
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> WAITING_TO_START_TASK
							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Giving player TASK_LEAVE_ANY_VEHICLE") #ENDIF
						ENDIF
						
					
					ELSE
					
							IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PlayerOutTaxi)
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_PlayerOutTaxi)
								SET_BIT(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerLeftTaxiInIntro)
								CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
							//	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								RESET_PLAYER_ONLY_CAMERA_CONTROLS()
								PRINT_HELP("HLP_TAXI") // You can call for a taxi on your cell phone
								IF USE_SERVER_TRANSACTIONS()
									TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_TAXI, 200)
								ELSE
									GIVE_LOCAL_PLAYER_CASH(-200)
									//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_TAXIS, 200)
									NETWORK_SPENT_TAXI(200)
								ENDIF

								//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_TAXIS, 200)
								#IF IS_DEBUG_BUILD 
									NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting biP_PlayerOutTaxi") 
									NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting biG_PlayerLeftTaxiInIntro") 
								#ENDIF
							ENDIF

					ENDIF
				ENDIF
			BREAK
			
			CASE TAXI_STAGE_WANDER
				IF NOT IS_PED_INJURED(pedTaxi)
					IF IS_VEHICLE_DRIVEABLE(vehTaxi)
						IF GET_SCRIPT_TASK_STATUS(pedTaxi, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(pedTaxi, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> WAITING_TO_START_TASK
							SET_PED_KEEP_TASK(pedTaxi, TRUE)
							TASK_VEHICLE_DRIVE_WANDER(pedTaxi, vehTaxi, 12, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
							ENTITY_INDEX ent 
							ent = pedTaxi
							SET_ENTITY_AS_NO_LONGER_NEEDED(ent)
							ent = vehTaxi
							SET_ENTITY_AS_NO_LONGER_NEEDED(ent)
							//SET_MODEL_AS_NO_LONGER_NEEDED(DEFAULT_PED_MODEL())
						ENDIF 
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF NOT IS_BIT_SET(iBoolsBitSet2, biL2_UberPausedAtStart)
			IF IS_VEHICLE_DRIVEABLE(vehTaxi)
				START_PLAYBACK_RECORDED_VEHICLE(vehTaxi, 100, "FM_TAxi")
				SET_PLAYBACK_SPEED(vehTaxi, 0.0)
				//UPDATE_UBER_PLAYBACK(vehTaxi, 0.0)
				SET_BIT(iBoolsBitSet2, biL2_UberPausedAtStart)
				#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Set biL2_UberPausedAtStart") #ENDIF
			ENDIF
		ENDIF
/*		
		//-- Set off speech
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneSetOffSpeech)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
				IF NOT IS_PED_INJURED(pedTaxi)
					Enable_MP_Comms()
					IF CREATE_CONVERSATION(sTaxiSpeech, "MPTXIAU", "MPTXI_T1", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
						SET_BIT(iBoolsBitSet, biL_DoneSetOffSpeech)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting biL_DoneSetOffSpeech") #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF 
		
		//-- Arrive pseech
		IF NOT IS_BIT_SET(iBoolsBitSet, biL_DoneArrivedSpeech)
			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_TaxiAtDest)
				IF NOT IS_PED_INJURED(pedTaxi)
					IF CREATE_CONVERSATION(sTaxiSpeech, "MPTXIAU", "MPTXI_AR", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
						SET_BIT(iBoolsBitSet, biL_DoneArrivedSpeech)
						#IF IS_DEBUG_BUILD NET_DW_PRINT("[PROCESS_TAXI_BODY] Setting biL_DoneArrivedSpeech") #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		*/
		
	//	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iIntroCutBitset, PLAYER_SAT_IN_TAXI)
	//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	//			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	//				MAKE_ONLY_PLAYER_CAMERA_WORK()
	//			ENDIF
	//		ENDIF
	//	ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_MISSION_STAGES_SERVER()
	
	PROCESS_TAXI_BRAIN()
ENDPROC

FUNC BOOL HAVE_MISSION_END_CONDITIONS_BEEN_MET()
	#IF IS_DEBUG_BUILD
		IF bHostEndMissionNow
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True because bHostEndMissionNow") #ENDIF
			RETURN TRUE
		ENDIF
		
		IF g_ShouldShiftingTutorialsBeSkipped 
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True because g_ShouldShiftingTutorialsBeSkipped") #ENDIF
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(serverbd.iServerGameState, biS_DebugSkipped)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True because biS_DebugSkipped") #ENDIF
			RETURN TRUE
		ENDIF
	#ENDIF	
	
	IF NOT bDoCoronaTut
		IF IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerGotWeapon)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True because biS_PlayerGotWeapon") #ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(serverbd.iServerBitSet, biS_PlayerDoneCut)
			#IF IS_DEBUG_BUILD NET_DW_PRINT("[HAVE_MISSION_END_CONDITIONS_BEEN_MET] True because biS_PlayerDoneCut") #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC




PROC SCRIPT_CLEANUP(BOOL bEmergencyTermination = FALSE)
	BOOL bPassed
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF bDoCoronaTut
			IF IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DoneCutscene)
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT(" Set MP_STAT_FM_INTRO_MISS_DONE")
					
				#ENDIF
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_SavedStat)
//					SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_INTRO_MISS_DONE)
//					SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_INTRO_CUT_DONE)
				
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_MISS_DONE, TRUE)
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_CUT_DONE, TRUE)
//					REQUEST_SAVE() //This is done later in RESET_FMMC_MISSION_VARIABLES
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT(" Cleanup Saving MP_STAT_FM_INTRO_MISS_DONE as hasn't been saved already")
						
					#ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT(" Calling SET_FM_UNLOCKS_BIT_SET...")
				#ENDIF
				
				SET_FM_UNLOCKS_BIT_SET() // Deal with any ranking up that took place while player on fm intro (892680)
				
				bPassed = TRUE
				
			ELSE	
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT(" Cleanup running, but biP_DoneCutscene not set. ")
					
				#ENDIF
				
			ENDIF
		ELSE
			IF IS_BIT_SET(playerbd[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_LeftAmmu)
				
			//	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_MISS_DONE, TRUE)
			//	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_CUT_DONE, TRUE)
			//	REQUEST_SAVE()
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_SavedStat)
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_MISS_DONE, TRUE)
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_CUT_DONE, TRUE)
					
//					SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_INTRO_MISS_DONE)
//					SET_FREEMODE_PROLOGUE_PROFILE_SETTINGS(PSFP_FM_INTRO_CUT_DONE)
					
				//	REQUEST_SAVE()
					#IF IS_DEBUG_BUILD
						NET_DW_PRINT(" Cleanup Saving MP_STAT_FM_INTRO_MISS_DONE as hasn't been saved already")
						
					#ENDIF
				ENDIF
				bPassed = TRUE
			//	SET_FM_UNLOCKS_BIT_SET() // Deal with any ranking up that took place while player on fm intro (892680)
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT(" Set MP_STAT_FM_INTRO_MISS_DONE")
				#ENDIF
		//		SET_LOCAL_PLAYER_DISPLAYING_PRE_TUTORIAL_HELP(TRUE)
			ELSE	
				#IF IS_DEBUG_BUILD
					NET_DW_PRINT(" Cleanup running, but biP_LeftAmmu not set. ")
					
				#ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
//	SET_MODEL_AS_NO_LONGER_NEEDED(mPlayerPed)
//	CLEANUP_TRIG_TUT_CUTSCENE()
//	CLEANUP_AIRPORT_SCENARIO_PEDS(FmIntroCutscene)
	CLEANUP_FREEMODE_INTRO_CUTSCENE(FmIntroCutscene, FALSE, bEmergencyTermination)	
	
	//#IF IS_DEBUG_BUILD
//	IF NOT HAS_PLAYER_STARTED_FINAL_INTRO_CUT_SHOT(PLAYER_ID())
		IF NETWORK_IS_GAME_IN_PROGRESS()
	//		IF playerBD[PARTICIPANT_ID_TO_INT()].bPressS
			IF MPGlobals.bStartedMPCutscene
				CLEANUP_MP_CUTSCENE(FALSE)
			ENDIF
	//		ENDIF
		ENDIF
//	ENDIF
	//#ENDIF
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SET_LOCAL_PLAYER_IN_TAXI_FOR_INTRO(FALSE)
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(FmIntroCutscene.scenBlockCut)
	
	//-- Outside airport 
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA( <<-1042.918213,-2723.140137,17.341135>>, <<-1087.763916,-2667.605225,25.795933>>, 25.562500)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA( <<-1005.742432,-2734.264648,14.919123>>, <<-1082.958374,-2675.188965,26.016474>>, 39.375000)
	
	CLEAR_BIT(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerLeftTaxiInIntro)
	
	DISABLE_CELLPHONE_DIALOGUE_LINE_SKIP (FALSE)
	
	BLOCK_DIALOGUE_FOR_ALL_GUNSHOPS(FALSE)
	ENABLE_TAXI_SKIP()
	//CLEAR_TAXI_TUTORIAL_FLAG()
	
//	LOAD_ALL_PATH_NODES(FALSE)
//	RELEASE_PATH_NODES()
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT("Clearing SET_SHOP_IGNORES_KICKING_OFF_CHECKS")#ENDIF
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(shopAmmu, FALSE)
	 
//	IF NETWORK_IS_CLOCK_TIME_OVERRIDDEN()
//		SET_TIME_OF_DAY(TIME_OFF, TRUE)
//		#IF IS_DEBUG_BUILD NET_DW_PRINT("Clock time stil overridden!") #ENDIF
//	ENDIF
	
	RESET_NET_TIMER(MPglobals.tdTimefade)
	MPglobals.g_iRealHours = 0
	MPglobals.g_iOverrideHours = 0
	MPglobals.g_iTutTimeDiff = 0
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) // 961426
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableCameraAnimations, TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, TRUE)
	ENDIF
//	REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(CHAR_MP_FM_CONTACT, MULTIPLAYER_BOOK)

	
	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[SCRIPT_CLEANUP] GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType is ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType) #ENDIF
	IF bPassed 
		#IF IS_DEBUG_BUILD NET_DW_PRINT("[SCRIPT_CLEANUP] Calling RESET_FMMC_MISSION_VARIABLES, not clearing clock override") #ENDIF
		RESET_FMMC_MISSION_VARIABLES(FALSE, FALSE, ciFMMC_END_OF_MISSION_STATUS_PASSED, FALSE)
		/*
		IF DID_LOCAL_PLAYER_CHOOSE_TO_SKIP_TUTORIALS()
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Player chose to skip tutorials") #ENDIF
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_TRIGTUT_DONE, TRUE)
			REQUEST_SAVE()
			IF NETWORK_IS_IN_TUTORIAL_SESSION()
			AND NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
				NETWORK_END_TUTORIAL_SESSION()
				#IF IS_DEBUG_BUILD NET_DW_PRINT("Removing from tutorial session as I chose not to do tutorials") #ENDIF
			ENDIF
			SET_FM_UNLOCKS_BIT_SET() 
		ELSE
		ENDIF */
	ELSE	
		CLEAR_HELP()
		RESET_FMMC_MISSION_VARIABLES(FALSE, FALSE, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
	ENDIF
	
	//CLEAR_WEATHER_TYPE_PERSIST()
	//CLEAR_OVERRIDE_WEATHER()
	
	CLEAR_BIT(MPGlobalsAmbience.iTaxiBitSet, biG_SendTaxiToPlayer)
	HIDE_ALL_SHOP_BLIPS(FALSE)	
	HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CARMOD, FALSE)
	HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CLOTHES, FALSE)
	HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_HAIRDO, FALSE)
	HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_TATTOO, FALSE)
	HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_GUN, FALSE)
	SET_SHOP_BLIP_LONG_RANGE(shopAmmu  , FALSE)
	
//	SET_COUNTRYSIDE_AMMU_AVAILABLE(TRUE)
//	SET_CITY_AMMU_AVAILABLE(TRUE)
	RESET_PLAYER_ONLY_CAMERA_CONTROLS()
	
	
	Clear_Any_Objective_Text_From_This_Script()
	
	IF DOES_BLIP_EXIST(g_sShopSettings.blipID[shopAmmu])
		SET_BLIP_ROUTE(g_sShopSettings.blipID[shopAmmu], FALSE)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipShootRange)
		REMOVE_BLIP(blipShootRange)
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SET_LOCAL_PLAYER_IS_RUNNING_INTRO_MISSION(FALSE)
		SET_LOCAL_PLAYER_RUNNING_FM_INTRO_CUT(FALSE)
	ENDIF
//	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("Done cleanup")
	#ENDIF
	
	
	TERMINATE_THIS_MULTIPLAYER_THREAD(serverbd.timeTerminate)

ENDPROC

PROC SET_ALL_AMMU_BLIPS_LONG_RANGE(BOOL bSetAllLongRange = TRUE)
	SET_SHOP_BLIP_LONG_RANGE(GUN_SHOP_01_DT  , bSetAllLongRange)     // Weapons - Downtown
	SET_SHOP_BLIP_LONG_RANGE(GUN_SHOP_02_SS  , bSetAllLongRange)     // Weapons - Sandy Shores
	SET_SHOP_BLIP_LONG_RANGE(GUN_SHOP_03_HW  , bSetAllLongRange)     // Weapons - Vinewood
	SET_SHOP_BLIP_LONG_RANGE(GUN_SHOP_04_ELS , bSetAllLongRange)// Weapons - East Los Santos
	SET_SHOP_BLIP_LONG_RANGE(GUN_SHOP_05_PB  , bSetAllLongRange)     // Weapons - Paleto Bay
	SET_SHOP_BLIP_LONG_RANGE(GUN_SHOP_06_LS  , bSetAllLongRange)     // Weapons - Little Seoul
	SET_SHOP_BLIP_LONG_RANGE(GUN_SHOP_07_MW  , bSetAllLongRange)     // Weapons - Morningwood
	SET_SHOP_BLIP_LONG_RANGE(GUN_SHOP_08_CS  , bSetAllLongRange)     // Weapons - Countryside
	SET_SHOP_BLIP_LONG_RANGE(GUN_SHOP_09_GOH , bSetAllLongRange)	// Weapons - Great Ocean Highway
	SET_SHOP_BLIP_LONG_RANGE(GUN_SHOP_10_VWH , bSetAllLongRange)		// Weapons - Vinewood Hills
	SET_SHOP_BLIP_LONG_RANGE(GUN_SHOP_11_ID1 , bSetAllLongRange)		// Weapons - Cypress Flats
						
ENDPROC


        
        



         



/// PURPOSE:
///    Do necessary pre game start ini.
///
PROC PROCESS_PRE_GAME(MP_MISSION_DATA missionScriptArgs)
	#IF IS_DEBUG_BUILD
		NET_DW_PRINT("     ---------->    Start Process_pre_game() <----------     ")
		IF DID_LOCAL_PLAYER_CHOOSE_TO_SKIP_TUTORIALS()
			NET_DW_PRINT("Player chose to SKIP tutorials")
		ELSE
			NET_DW_PRINT("Player chose to PLAY tutorials")
		ENDIF
		
		IF lw_bUnlockAllShopItems
			lw_bUnlockAllShopItems = FALSE
			NET_DW_PRINT("lw_bUnlockAllShopItems was set, clearing")
			
		ENDIF
	#ENDIF
	
		
	//This marks the script as a net script, and handles any instancing setup. 
	//SETUP_MP_MISSION_FOR_NETWORK(GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mission),  missionScriptArgs)
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(2, FALSE, missionScriptArgs.iInstanceId )

	//Reserve Assets
//	RESERVE_NETWORK_MISSION_PEDS(1)
//	RESERVE_NETWORK_MISSION_VEHICLES(2)

	//This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GUN_INTRO
	
	
//	DISABLE_TAXI_SKIP() 
//	SET_TAXI_TUTORIAL_FLAG()
	#IF IS_DEBUG_BUILD
		//NET_DW_PRINT("     ---------->    SET_TAXI_TUTORIAL_FLAG from process pre game <----------     ")
	#ENDIF
	
	IF bDoIntroCut
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		ENDIF
		IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
			IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
				//-- CHeck MAINTAIN_ADD_TO_TUTORIAL_SESSION in intro cut
				INT instance  
				instance = NATIVE_TO_INT(PLAYER_ID()) + NUM_NETWORK_PLAYERS
				NETWORK_ALLOW_GANG_TO_JOIN_TUTORIAL_SESSION(1, instance)
				SET_LOCAL_PLAYER_IS_IN_TUTORIAL_SESSION_FOR_INTRO(TRUE) 
				#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Putting player on tut session... ", instance) #ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD NET_DW_PRINT("Player already in toturoial session!") #ENDIF
		ENDIF
		
	ENDIF
	
	RESERVE_NETWORK_MISSION_PEDS(1)
	RESERVE_NETWORK_MISSION_VEHICLES(1)
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		#IF IS_DEBUG_BUILD NET_LOG("FAILED TO RECEIVE INITIAL NETWORK BROADCAST") #ENDIF
		SCRIPT_CLEANUP()
	ENDIF

	//This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT("Calling SET_SHOP_IGNORES_KICKING_OFF_CHECKS for ammu") #ENDIF
	SET_SHOP_IGNORES_KICKING_OFF_CHECKS(shopAmmu, TRUE)
	
	SET_LOCAL_PLAYER_IS_RUNNING_INTRO_MISSION(TRUE)
	
	#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("[PROCESS_PRE_GAME] GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType set to ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType) #ENDIF
	
	//NETWORK_START_SOLO_TUTORIAL_SESSION()
	IF NOT bDoIntroCut
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
	MPGlobalsAmbience.iFmNmhBitSet = 0
	//-- Outside airport
	SET_ROADS_IN_ANGLED_AREA(<<-1005.742432,-2734.264648,14.919123>>, <<-1082.958374,-2675.188965,26.016474>>, 39.375000, FALSE, FALSE, FALSE)
	
	
	
	#IF IS_DEBUG_BUILD
	NET_DW_PRINT("     ---------->    Done Process_pre_game() <----------     ")
	#ENDIF
	
	
	CREATE_SEQUENCES()
	
	//set that all the invites are blocked
	SET_TUTORIAL_INVITES_ARE_BLOCKED()

//	REQUEST_MODEL(LANDSTALKER)
//	REQUEST_MODEL(TAILGATER)
//	SET_BIT(MPGlobalsAmbience.iTaxiBitSet, biG_SendTaxiToPlayer)
	playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_INI
ENDPROC

SCRIPT(MP_MISSION_DATA missionScriptArgs)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		#IF IS_DEBUG_BUILD	
			NET_DW_PRINT("launching ")
		#ENDIF
	
		// Carry out all the initial game starting duties.
		PROCESS_PRE_GAME(missionScriptArgs)	
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			CREATE_WIDGETS()
		#ENDIF
	ENDIF
		
	// Main loop.
	WHILE TRUE

		MP_LOOP_WAIT_ZERO() 
		
	//	DO_FAKE_ACTIVITY_CORONA()
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD NET_LOG("SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")	#ENDIF
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		//GET_NETWORK_TIMER(iNetTimer)
				
		// Deal with the debug.
		#IF IS_DEBUG_BUILD		
			UPDATE_WIDGETS()
		#ENDIF		
	
		// If we need to leave the mission, make sure we just straight to the end stage and terminate the script.
		IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			#IF IS_DEBUG_BUILD
				NET_DW_PRINT("Leave mission = TRUE \n")
			#ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
			#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_LEAVE")		#ENDIF
		ENDIF
		
		IF GET_CLIENT_FMINTRO_STATE(PARTICIPANT_ID_TO_INT()) > FM_INTRO_FADE_IN
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		ENDIF
		
		// -----------------------------------
		// Process your game logic.....
		SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())
						
			// Wait untill the server gives the all go before moving on.
			CASE GAME_STATE_INI
				IF NOT IS_BIT_SET(iBoolsBitSet, biL_RemovedWeapons)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						//REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())		//nned to remove this remove all weapon call. Guns are now given in the creator - KW 3/9/13
						SET_BIT(iBoolsBitSet, biL_RemovedWeapons)
					//	#IF IS_DEBUG_BUILD NET_DW_PRINT("Removed all my weapons...") #ENDIF
					ENDIF
				ENDIF
				REQUEST_ADDITIONAL_TEXT("FMINT", MISSION_TEXT_SLOT)
				
				IF DO_REQUIRED_NET_IDS_EXIST()
				AND HAS_THIS_ADDITIONAL_TEXT_LOADED("FMINT", MISSION_TEXT_SLOT)
					IF GET_SERVER_MISSION_STATE() > GAME_STATE_INI		
						
						ADD_PED_FOR_DIALOGUE(sSpeech, 1, NULL, "Lamar")
						HIDE_ALL_SHOP_BLIPS(FALSE)	
						HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CARMOD, TRUE)
						HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_CLOTHES, TRUE)
						HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_HAIRDO, TRUE)
						HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_TATTOO, TRUE)
						HIDE_SHOP_BLIP_BY_TYPE(SHOP_TYPE_GUN, FALSE)
						SET_SHOP_BLIP_LONG_RANGE(shopAmmu  , TRUE)     // Weapons - Downtown
						
						//Removing, this is done in the character creator now. - Brenda
//						INT iCurrentCash
//						INT iRandomAdd
//						INT iAfterTunable
//						iCurrentCash = GET_PLAYER_CASH(PLAYER_ID())
//						#IF IS_DEBUG_BUILD
//							NET_DW_PRINT_STRING_INT("Current cash... ", iCurrentCash)
//						#ENDIF
////						
//						
//						IF iCurrentCash <= 2500
//							IF iCurrentCash > 0
//								GIVE_LOCAL_PLAYER_CASH(-iCurrentCash)
//								#IF IS_DEBUG_BUILD
//									NET_DW_PRINT_STRING_INT("New current cash after reduction... ", GET_PLAYER_CASH(PLAYER_ID()))
//								#ENDIF
//								//SET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_MADE_FROM_MISSIONS, 0) // bug 1137939
//							ENDIF
//							iRandomAdd = GET_RANDOM_INT_IN_RANGE(2500, 2700) // 979342
//							#IF IS_DEBUG_BUILD 
//								NET_DW_PRINT_STRING_INT("random Wallet cash... ", (iRandomAdd)) 
//								NET_DW_PRINT_STRING_FLOAT("Wallet cash tunable... ", g_sMPTunables.ftutorial_wallet_cash)
//								NET_DW_PRINT_STRING_FLOAT("bank cash tunable... ", g_sMPTunables.ftutorial_bank_cash)
//							#ENDIF
//							
//							iAfterTunable = FLOOR(TO_FLOAT(iRandomAdd) * g_sMPTunables.ftutorial_wallet_cash)
//							#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Wallet cash After tunable applied ", iAfterTunable) #ENDIF
//							INT iBankCash
//							INT iBankAfterTunable
//							
//							iBankCash = 5000
//							iBankAfterTunable = FLOOR(TO_FLOAT(iBankCash) *g_sMPTunables.ftutorial_bank_cash) //MULTIPLY_CASH_BY_TUNABLE(iBankCash)
//							
//							#IF IS_DEBUG_BUILD NET_DW_PRINT_STRING_INT("Bank cash After tunable applied ", iBankAfterTunable) #ENDIF
//							GIVE_LOCAL_PLAYER_CASH(iAfterTunable)
//							// See MAINTAIN_PLAYER_CASH as well
//							NETWORK_INITIALIZE_CASH(iAfterTunable, iBankAfterTunable)
//							iCurrentCash = GET_PLAYER_CASH(PLAYER_ID())
//							#IF IS_DEBUG_BUILD
//								NET_DW_PRINT_STRING_INT("New total cash cash... ", iCurrentCash)
//							#ENDIF
//						ENDIF

						playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_RUNNING
						
						IF bDoIntroCut 
							playerBD[PARTICIPANT_ID_TO_INT()].iFmIntroState = FM_INTRO_RUN_CUTSCENE
						ELSE
							playerBD[PARTICIPANT_ID_TO_INT()].iFmIntroState = FM_INTRO_FADE_IN
						ENDIF
						
						#IF IS_DEBUG_BUILD 
							NET_DW_PRINT("Client game state GAME_STATE_INI -> GAME_STATE_RUNNING")
							NET_LOG("iGameState = GAME_STATE_RUNNING")	
						#ENDIF
					ENDIF
				ELSE	
					#IF IS_DEBUG_BUILD NET_DW_PRINT("Still waiting for text to load") #ENDIF
				ENDIF
				
				// Look for the server to say the mission has failed.
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_FAILED
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_FAILED
					#IF IS_DEBUG_BUILD 
						NET_LOG("iGameState = GAME_STATE_FAILED")
						NET_DW_PRINT("Client game state GAME_STATE_INI -> GAME_STATE_FAILED")
					#ENDIF
				ENDIF
				
				// Look for the server to say the mission has ended.
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
				AND playerBD[PARTICIPANT_ID_TO_INT()].iGameState < GAME_STATE_FAILED
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
					#IF IS_DEBUG_BUILD 
						NET_LOG("iGameState = GAME_STATE_END - A")	
						NET_DW_PRINT("Client game state GAME_STATE_INI -> GAME_STATE_END")
					#ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING
				#IF IS_DEBUG_BUILD
					DO_DEBUG_KEYS()
				#ENDIF
				
				// Cool stuff goes here
				PROCESS_TAXI_BODY()
				PROCESS_MISSION_STAGES_CLIENT()
				
				
				// Look for the server to say the mission has failed
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_FAILED
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_FAILED
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_FAILED")		#ENDIF	
				ENDIF
				
				// Look for the server to say the mission has ended
				IF GET_SERVER_MISSION_STATE() = GAME_STATE_END
				AND playerBD[PARTICIPANT_ID_TO_INT()].iGameState < GAME_STATE_FAILED
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState =GAME_STATE_TERMINATE_DELAY // GAME_STATE_END
					#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY - B")			#ENDIF
				ENDIF
			
			BREAK
			
			//The game stage the player is placed in if they fail the mission
			CASE GAME_STATE_FAILED
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_TERMINATE_DELAY //GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_TERMINATE_DELAY - C")			#ENDIF
			BREAK
			
			CASE GAME_STATE_TERMINATE_DELAY
					

				SET_MISSION_FINISHED(serverbd.timeTerminate) //the server 
				IF IS_MISSION_READY_TO_CLEANUP(serverbd.timeTerminate)
					playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_LEAVE
				ENDIF
				
				
			BREAK
			// The game stage the local player is placed when we want him to leave a mission. 
			CASE GAME_STATE_LEAVE
				LEAVE_MY_MP_MISSION(thisMission)
				playerBD[PARTICIPANT_ID_TO_INT()].iGameState = GAME_STATE_END
				#IF IS_DEBUG_BUILD NET_LOG("iGameState = GAME_STATE_END - D")		#ENDIF	
			FALLTHRU
			
			//Cleans up then terminates the mission
			CASE GAME_STATE_END
				SCRIPT_CLEANUP()
			BREAK

		ENDSWITCH
		
		// -----------------------------------
		// Process server game logic		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			SWITCH GET_SERVER_MISSION_STATE()
				
				// For CnC group missions, we may want to wait for all players to have launched the script.
				CASE GAME_STATE_INI							

					serverBD.iServerGameState = GAME_STATE_RUNNING	
					#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_RUNNING")		#ENDIF
						
				BREAK
				
				// Look for game end conditions.
				CASE GAME_STATE_RUNNING		
					
					// Handle any game logic needed.
					SERVER_REPEAT()
					PROCESS_GAME_SERVER()
					PROCESS_MISSION_STAGES_SERVER()
				//	FAIL_CHECKS()
					
					IF HAVE_MISSION_END_CONDITIONS_BEEN_MET()
						serverBD.iServerGameState = GAME_STATE_END
						#IF IS_DEBUG_BUILD NET_LOG_Host("iServerGameState = GAME_STATE_END")		#ENDIF
					ENDIF
					
				BREAK
				
				CASE GAME_STATE_END
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bWdSHowMissionInt
				NET_DW_PRINT_STRING_INT("GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType.... ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType)
			ENDIF
		#ENDIF
	ENDWHILE
ENDSCRIPT
