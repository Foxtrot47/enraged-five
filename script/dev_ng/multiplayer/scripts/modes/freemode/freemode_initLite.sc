USING "globals.sch"
USING "mp_globals.sch"
USING "MP_Globals_FM.sch"
USING "net_stat_generator.sch"
USING "net_script_tunables.sch"
USING "net_main_client.sch"
USING "net_spawning_init.sch"
USING "net_interactions_synced.sch"
USING "net_pv_rate_limiter.sch"
#IF IS_DEBUG_BUILD
USING "net_debug.sch"
#ENDIF
USING "net_private_yacht.sch"

#IF FEATURE_FREEMODE_CLASSIC


SCRIPT 
	
	NET_PRINT("********** freemode_init.sch started **********") NET_NL()

	#IF FEATURE_FREEMODE_CLASSIC
	DISABLE_SCTV_PLAYER_SLOTS(TRUE)
	#ENDIF

	INIT_STATS()
			
	INIT_MP_SPAWN_POINTS()
	
	INIT_SYNCED_INTERACTIONS()
	
	FLATTEN_GLOBALS_START_FREEMODE()
	
	// Fade the clouds back in 2193949
	SET_CLOUD_STATE(ENUMCLOUDSTATE_FADING_IN)
	
	Initialise_Main_Generic_Client()

	// set all tutorials to complete so all contacts are unlocked etc.
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset = HIGHEST_INT
	
	g_FinishedFreemodeInit = TRUE

	
	INT iLastUsedPV = CURRENT_SAVED_VEHICLE_SLOT()
	IF (iLastUsedPV >= 0)
	AND NOT IS_THIS_MODEL_A_SPECIAL_VEHICLE(g_MpSavedVehicles[iLastUsedPV].vehicleSetupMP.VehicleSetup.eModel)
	AND NOT IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[iLastUsedPV].vehicleSetupMP.VehicleSetup.eModel)
		MPGlobalsAmbience.iCachedPVSlotForPIMenu = iLastUsedPV
		PRINTLN("[personal_vehicle] freemode init - setting MPGlobalsAmbience.iCachedPVSlotForPIMenu to ", MPGlobalsAmbience.iCachedPVSlotForPIMenu)
	ELSE
		PRINTLN("[personal_vehicle] freemode init - iLastUsedPV was special vehicle, not setting MPGlobalsAmbience.iCachedPVSlotForPIMenu ", MPGlobalsAmbience.iCachedPVSlotForPIMenu)	
	ENDIF

	DELETE_ALL_NON_NETWORKED_VEHILCES()

	NET_PRINT("********** freemode_init.sch finished **********") NET_NL()

ENDSCRIPT

#ENDIF
