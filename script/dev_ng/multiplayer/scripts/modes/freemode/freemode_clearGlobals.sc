USING "globals.sch"
USING "mp_globals.sch"
USING "MP_Globals_FM.sch"
USING "net_main_client.sch"
#IF IS_DEBUG_BUILD
USING "net_debug.sch"
#ENDIF


SCRIPT 
	
	NET_PRINT("********** freemode_clearGlobals started **********") NET_NL()
	CLEAR_ALL_MP_GLOBALS()
	g_bFinishedFreemodeClearGlobals = TRUE
	NET_PRINT("********** freemode_clearGlobals finished **********") NET_NL()

ENDSCRIPT