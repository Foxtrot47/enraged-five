//////////////////////////////
///    VEHICLE SPAWNING    ///
//////////////////////////////

USING "net_mission.sch"
USING "net_wait_zero.sch"
USING "net_saved_vehicles.sch"

///////////////////////
///    CONSTANTS    ///
///////////////////////

CONST_INT NUMBER_OF_VEHICLES 20

///////////////////////////
///    STRUCTS/ENUMS    ///
///////////////////////////

STRUCT ServerBroadcastData
	NETWORK_INDEX niVehicleIds[NUMBER_OF_VEHICLES]
ENDSTRUCT
ServerBroadcastData serverBD

///////////////////////
///    VARIABLES    ///
///////////////////////

INT m_iNumberOfVehiclesSpawned = 0

BOOL m_bIsPlayerInPosition = FALSE
BOOL m_bEverythingIsReady = FALSE
BOOL m_bEntitySetsActive = FALSE
BOOL m_bInteriorPinned = FALSE
BOOL m_bInteriorSetup = FALSE
BOOL m_bKillScript = FALSE

FLOAT m_fStartHeading = 277.7314

INTERIOR_INSTANCE_INDEX m_intImpExpInterior01

VECTOR m_vStartCoords = <<-196.0450, -580.1300, 135.0004>>

#IF IS_DEBUG_BUILD
BOOL m_bAttemptToDeleteVehicles = FALSE
BOOL m_bAttemptToSpawnVehicles = TRUE
BOOL m_bAreVehiclesSpawned = TRUE
BOOL m_bIsVehicleSpawned = FALSE
BOOL m_bIsVehicleReady = FALSE
#ENDIF

///////////////////////
///    PROC/FUNC    ///
///////////////////////

PROC DELETE_SAVED_VEHICLE(INT iVehicleIndex)
	DELETE_NET_ID(serverBD.niVehicleIds[iVehicleIndex])
	RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() - 1)
ENDPROC

FUNC FLOAT GET_VEHICLE_HEADING(INT iVehicleIndex)
	SWITCH iVehicleIndex
		// 1st Floor
		CASE 0 		RETURN	76.01000	BREAK
		CASE 1 		RETURN	103.9400	BREAK
		CASE 2 		RETURN	139.4000	BREAK
		CASE 3 		RETURN	166.0200	BREAK
		CASE 4 		RETURN	-164.520	BREAK
		CASE 5 		RETURN	-164.520	BREAK
		// 2nd Floor
		CASE 6 		RETURN	76.01000	BREAK
		CASE 7 		RETURN	103.9400	BREAK
		CASE 8 		RETURN	139.4000	BREAK
		CASE 9 		RETURN	166.0200	BREAK
		CASE 10 	RETURN	-164.520	BREAK
		CASE 11 	RETURN	-164.520	BREAK
		CASE 12 	RETURN	-164.520	BREAK
		// 3rd Floor
		CASE 13 	RETURN	76.01000	BREAK
		CASE 14 	RETURN	103.9400	BREAK
		CASE 15 	RETURN	139.4000	BREAK
		CASE 16 	RETURN	166.0200	BREAK
		CASE 17 	RETURN	-164.520	BREAK
		CASE 18 	RETURN	-164.520	BREAK
		CASE 19 	RETURN	-164.520	BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_VEHICLE_COORDS(INT iVehicleIndex)
	SWITCH iVehicleIndex
		// 1st Floor
		CASE 0 		RETURN	<<-172.853, -584.205, 136.016>>		BREAK
		CASE 1 		RETURN	<<-172.807, -578.614, 136.016>>		BREAK
		CASE 2 		RETURN	<<-175.573, -574.298, 136.016>>		BREAK
		CASE 3 		RETURN	<<-179.736, -572.181, 136.016>>		BREAK
		CASE 4 		RETURN	<<-184.537, -572.228, 136.016>>		BREAK
		CASE 5 		RETURN	<<-189.673, -573.662, 136.016>>		BREAK
		// 2nd Floor
		CASE 6 		RETURN	<<-172.853, -584.205, 141.359>>		BREAK
		CASE 7		RETURN	<<-172.807, -578.614, 141.359>>		BREAK
		CASE 8 		RETURN	<<-175.573, -574.298, 141.359>>		BREAK
		CASE 9 		RETURN 	<<-179.736, -572.181, 141.359>>		BREAK
		CASE 10 	RETURN	<<-184.537, -572.228, 141.359>>		BREAK
		CASE 11 	RETURN	<<-189.673, -573.662, 141.359>>		BREAK
		CASE 12 	RETURN	<<-194.142, -574.912, 141.359>>	 	BREAK
		// 3rd Floor
		CASE 13 	RETURN	<<-172.853, -584.205, 146.704>>		BREAK
		CASE 14 	RETURN	<<-172.807, -578.614, 146.704>>	 	BREAK
		CASE 15 	RETURN	<<-175.573, -574.298, 146.704>>	 	BREAK
		CASE 16 	RETURN	<<-179.736, -572.181, 146.704>>	 	BREAK
		CASE 17 	RETURN	<<-184.537, -572.228, 146.704>>	 	BREAK
		CASE 18 	RETURN	<<-189.673, -573.662, 146.704>>	 	BREAK
		CASE 19 	RETURN	<<-194.142, -574.912, 146.704>>	 	BREAK
	ENDSWITCH
	
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC BOOL SPAWN_SAVED_VEHICLE()
	MODEL_NAMES modVehicleModel = SANDKING
	VEHICLE_INDEX viTempVehicle
	
	IF NOT IS_NET_VEHICLE_DRIVEABLE(serverBD.niVehicleIds[m_iNumberOfVehiclesSpawned])
		IF IS_MODEL_IN_CDIMAGE(modVehicleModel)
			IF REQUEST_LOAD_MODEL(modVehicleModel)
				IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES() + 1, false, true)
					RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() + 1)
					
					IF CAN_REGISTER_MISSION_ENTITIES(0, 1, 0, 0)
						viTempVehicle = CREATE_VEHICLE(modVehicleModel, GET_VEHICLE_COORDS(m_iNumberOfVehiclesSpawned), GET_VEHICLE_HEADING(m_iNumberOfVehiclesSpawned))
						serverbD.niVehicleIds[m_iNumberOfVehiclesSpawned] = VEH_TO_NET(viTempVehicle)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(modVehicleModel)
						
						FREEZE_ENTITY_POSITION(viTempVehicle, TRUE)
						
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(viTempVehicle, TRUE)
						SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(viTempVehicle, FALSE)
						SET_VEHICLE_FULLBEAM(viTempVehicle, FALSE)
						SET_VEHICLE_LIGHTS(viTempVehicle, FORCE_VEHICLE_LIGHTS_OFF)
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viTempVehicle, TRUE)
						
						IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
							SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(viTempVehicle, PLAYER_ID(), FALSE)
						ENDIF
						
						SET_VEHICLE_DIRT_LEVEL(viTempVehicle, 0.0)
			            SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(viTempVehicle, TRUE)
						SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(viTempVehicle, TRUE)
						SET_ENTITY_CAN_BE_DAMAGED(viTempVehicle, FALSE)
						SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(viTempVehicle, SC_WHEEL_CAR_FRONT_LEFT, 0.0)
						SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(viTempVehicle, SC_WHEEL_CAR_FRONT_RIGHT, 0.0)
						SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(viTempVehicle, SC_WHEEL_CAR_MID_LEFT, 0.0)
						SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(viTempVehicle, SC_WHEEL_CAR_MID_RIGHT, 0.0)
						SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(viTempVehicle, SC_WHEEL_CAR_REAR_LEFT, 0.0)
						SET_HYDRAULIC_SUSPENSION_RAISE_FACTOR(viTempVehicle, SC_WHEEL_CAR_REAR_RIGHT, 0.0)
						SET_CAN_USE_HYDRAULICS(viTempVehicle, FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(modVehicleModel)
						
						IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
							IF NOT DECOR_EXIST_ON(viTempVehicle, "Player_Vehicle")
								DECOR_SET_INT(viTempVehicle, "Player_Vehicle", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
							ENDIF
						ENDIF
						
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(serverbD.niVehicleIds[m_iNumberOfVehiclesSpawned], PLAYER_ID(), FALSE)
						
						FORCE_ROOM_FOR_ENTITY(viTempVehicle, m_intImpExpInterior01, GET_HASH_KEY("mainRoom001"))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC SETUP_VEHICLE_SPAWNING_DEBUG_WIDGETS()
	START_WIDGET_GROUP("Spawn Vehicles")
		ADD_WIDGET_BOOL("Spawn vehicles", m_bAttemptToSpawnVehicles)
		ADD_WIDGET_BOOL("Delete vehicles", m_bAttemptToDeleteVehicles)
		ADD_WIDGET_BOOL("Kill script", m_bKillScript)
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_VEHICLE_SPAWNING_DEBUG_WIDGETS()
	IF m_bAttemptToSpawnVehicles
		m_bAttemptToSpawnVehicles = FALSE
		m_bAreVehiclesSpawned = FALSE
		m_bIsVehicleReady = TRUE
		m_iNumberOfVehiclesSpawned = 0
	ENDIF
	
	IF NOT m_bAreVehiclesSpawned
		IF m_iNumberOfVehiclesSpawned = NUMBER_OF_VEHICLES
			m_bAreVehiclesSpawned = TRUE
		ELSE
			IF m_bIsVehicleReady
				m_bIsVehicleSpawned = SPAWN_SAVED_VEHICLE()
				
				IF m_bIsVehicleSpawned
					m_bIsVehicleReady = FALSE
				ENDIF
			ELSE
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVehicleIds[m_iNumberOfVehiclesSpawned])
				AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.niVehicleIds[m_iNumberOfVehiclesSpawned]))
					IF HAVE_VEHICLE_MODS_STREAMED_IN(NET_TO_VEH(serverBD.niVehicleIds[m_iNumberOfVehiclesSpawned]))
						m_bIsVehicleReady = TRUE
						m_iNumberOfVehiclesSpawned++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF m_bAttemptToDeleteVehicles
		IF m_iNumberOfVehiclesSpawned != 0
			DELETE_SAVED_VEHICLE(m_iNumberOfVehiclesSpawned - 1)
			m_iNumberOfVehiclesSpawned--
		ELSE
			m_bAttemptToDeleteVehicles = FALSE
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL ACTIVATE_ENTITY_SETS()
	IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(m_intImpExpInterior01, "garage_decor_01")
		ACTIVATE_INTERIOR_ENTITY_SET(m_intImpExpInterior01, "garage_decor_01")
	ELSE
		REFRESH_INTERIOR(m_intImpExpInterior01)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_INTERIOR()
	m_intImpExpInterior01 = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
	
	IF IS_VALID_INTERIOR(m_intImpExpInterior01)
		IF NOT m_bInteriorPinned
			PIN_INTERIOR_IN_MEMORY(m_intImpExpInterior01)
			m_bInteriorPinned = TRUE
		ELIF IS_INTERIOR_READY(m_intImpExpInterior01)
			UNPIN_INTERIOR(m_intImpExpInterior01)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/////////////////////////////////
///    CLEANUP/PRE_PROCESS    ///
/////////////////////////////////

PROC SCRIPT_CLEANUP()
	INT iVehicleIndex
	
	REPEAT m_iNumberOfVehiclesSpawned iVehicleIndex
		DELETE_SAVED_VEHICLE(iVehicleIndex)
	ENDREPEAT
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

PROC PROCESS_PRE_GAME()
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
	
	HANDLE_NET_SCRIPT_INITIALISATION()
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	
	IF NOT Wait_For_First_Network_Broadcast()
		SCRIPT_CLEANUP()
	ENDIF
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	IF NOT IS_IPL_ACTIVE("imp_dt1_02_cargarage_a")
		REQUEST_IPL("imp_dt1_02_cargarage_a")
	ENDIF
ENDPROC

////////////////////
///    SCRIPT    ///
////////////////////

SCRIPT
	DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
	
	#IF IS_DEBUG_BUILD
		SETUP_VEHICLE_SPAWNING_DEBUG_WIDGETS()
	#ENDIF
	
	PROCESS_PRE_GAME()
	
	WHILE TRUE
		DISABLE_OCCLUSION_THIS_FRAME()
		
		MP_LOOP_WAIT_ZERO()
		
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		OR m_bKillScript
			SCRIPT_CLEANUP()
		ENDIF
		
		IF NOT m_bEverythingIsReady
			IF NOT m_bIsPlayerInPosition
			AND IS_SCREEN_FADED_OUT()
				SET_ENTITY_COORDS(PLAYER_PED_ID(), m_vStartCoords)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), m_fStartHeading)
				NEW_LOAD_SCENE_START_SPHERE(m_vStartCoords, 2500.0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				m_bIsPlayerInPosition = TRUE
			ELSE
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					IF NOT m_bInteriorSetup
						m_bInteriorSetup = SETUP_INTERIOR()
					ELSE
						IF NOT m_bEntitySetsActive
							m_bEntitySetsActive = ACTIVATE_ENTITY_SETS()
						ELSE
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
								
							m_bEverythingIsReady = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			MAINTAIN_VEHICLE_SPAWNING_DEBUG_WIDGETS()
			#ENDIF
		ENDIF
	ENDWHILE
ENDSCRIPT