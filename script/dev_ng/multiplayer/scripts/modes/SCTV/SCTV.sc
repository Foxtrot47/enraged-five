//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        SCTV.sc																						//
// Description: Controls the SCTV player																	//
// Written by:  David Gentles																				//
// Date: 20/01/2013																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "building_control_public.sch"
USING "menu_public.sch"
USING "context_control_public.sch"

// Network Headers
USING "net_events.sch"
USING "net_blips.sch"
USING "net_scoring_common.sch"
USING "net_mission.sch"
USING "shared_hud_displays.sch"
USING "net_entity_icons.sch"
USING "fm_relationships.sch"
USING "net_taxi.sch"
USING "net_spawn.sch"

USING "freemode_header.sch"

USING "rage_builtins.sch"
USING "net_team_info.sch"
USING "net_spectator_cam.sch"
USING "net_cutscene.sch"
USING "FMMC_header.sch"
USING "net_spectator_variables.sch"
USING "net_SCTV_common.sch"
USING "net_realty_new.sch"
USING "net_heists_common.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
USING "profiler.sch"
#ENDIF

USING "net_wait_zero.sch"

CONST_INT DIST_DRAW_FAKE_UGC_BOOKMARK_CORONAS_SQR		10000//100sqr

CONST_INT SCTV_BAIL_TIME								30000//30sec
CONST_INT SCTV_EXTENDED_BAIL_TIME						60000//1min

FMMC_LOCAL_STRUCT sFMMCdata
FMMC_CAM_DATA sCamData

SPECTATOR_DATA_STRUCT specData
STRUCT_TRANSITION_SESSION_AS_A_SPECTATOR sJoinTransitionSpecVars

INT iFreeCamFadeSwitchStage = -1
TIME_DATATYPE timeFreeCamFadeSafety
TIME_DATATYPE timeNet
TIME_DATATYPE timerFollowToSession
INT iFollowRetryAttempts = 0

SCALEFORM_INDEX sfGTAOTV

SCRIPT_TIMER timeBail

SCRIPT_TIMER SpectatingYouTickerTimer
CONST_INT SPECTATING_YOU_TICKER_DELAY	10000

SCRIPT_TIMER EndSCTVDelay

BOOL bIgnoreBulletCollisionSet

#IF IS_DEBUG_BUILD
	
/// PURPOSE:
///    Creates widgets for SCTV
	PROC Create_Spectator_Widget()
		START_WIDGET_GROUP("Spectator Player")
			
		STOP_WIDGET_GROUP()
	ENDPROC
	
#ENDIF

/// PURPOSE:
///    Initialise SCTV script
PROC PROCESS_PRE_GAME()

	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	DONT_RENDER_IN_GAME_UI(FALSE)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === DONT_RENDER_IN_GAME_UI(FALSE)")
	#ENDIF
	
	SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0, -0.0375)
	
	MPSpecGlobals.SCTVData.bBailCalledThisFrame = FALSE
	MPSpecGlobals.SCTVData.bBailTimeIsUp = FALSE
	
	CLEAR_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()
	
	CLEAN_ALL_HEIST_PRE_PLANNING()
	CLEAN_ALL_HEIST_STRAND_PLANNING()
	CLEAN_HEIST_FINALE_ALL(FALSE, FALSE, FALSE)
	CLEAN_HEIST_SHARED_DATA()
	
	IF g_bUpdateSpectatorPositionSCTVOverride = TRUE
		g_bUpdateSpectatorPosition = TRUE
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === PROCESS_PRE_GAME - g_bUpdateSpectatorPositionSCTVOverride - SET g_bUpdateSpectatorPosition = TRUE")
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === PROCESS_PRE_GAME")
	#ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up the SCTV script and terminates
PROC CLEANUP_SCTV()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === CLEANUP_SCTV")
	#ENDIF
	// Spectator cam
    IF NOT IS_THIS_SPECTATOR_CAM_OFF(specData.specCamData)
		FORCE_CLEANUP_SPECTATOR_CAM(specData)
    ENDIF
	CLEAR_FOCUS()
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === CLEANUP_SCTV - CLEAR_FOCUS")
	
	ENABLE_ENTITY_BULLET_COLLISION(PLAYER_PED_ID())
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === CLEANUP_SCTV - Bullet collision turned back on leaving spectator. See url:bugstar:2989819.")
		
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfGTAOTV)
	CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_IS_SPECTATING)
	ENABLE_WEAPON_SWAP()
	ENABLE_INTERACTION_MENU()
	Enable_MP_Comms()
	FORCE_CLEANUP_SPECTATOR_CAM(specData)	
	RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
	CLEAN_UP_FMMC_CAMERA(FALSE, FALSE)	
	IF NOT g_bOnHorde
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === CLEANUP_SCTV - SET_MAX_WANTED_LEVEL(5)")
		SET_MAX_WANTED_LEVEL(5)
	ENDIF
	SET_RADAR_ZOOM(0)
	UNLOCK_MINIMAP_ANGLE()
	UNLOCK_MINIMAP_POSITION()
	SET_IN_SPECTATOR_MODE(FALSE)
	NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
	SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), TRUE)
	g_iOverheadNamesState = -1					//Turn off player names
	MPSpecGlobals.SCTVData.iBitSet = 0
	MPSpecGlobals.SCTVData.currentMode = SCTV_MODE_INIT
	MPSpecGlobals.SCTVData.switchToMode = SCTV_MODE_INIT
	MPSpecGlobals.SCTVData.coronaState = SCTV_CORONA_STATE_NULL
	MPSpecGlobals.SCTVData.stage = SCTV_STAGE_INIT
	PRINTLN(" CLEANUP_SCTV - MPSpecGlobals.SCTVData.stage = SCTV_STAGE_INIT")
	
	SET_TEAM_COLOUR_OVERRIDE_FLAG(FALSE)
	
	CLEAR_SCTV_TICKER_FEED_STRUCT()
	CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN)
	CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === CLEANUP_SCTV - SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN - CLEARED")
	
	IF IS_PLAYER_A_ROCKSTAR_DEV()
		STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE") 
		STOP_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
		PRINTLN("=== SCTV === CLEANUP_SCTV - GTATV - ENABLE RADIO - MP_POSITIONED_RADIO_MUTE_SCENE + MP_JOB_CHANGE_RADIO_MUTE")
	ENDIF
	
	IF g_bUpdateSpectatorPositionSCTVOverride = TRUE
		g_bUpdateSpectatorPosition = FALSE
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === CLEANUP_SCTV - g_bUpdateSpectatorPositionSCTVOverride - SET g_bUpdateSpectatorPosition = FALSE")
	ENDIF
	
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

/// PURPOSE:
///    Terminates SCTV when appropriate
PROC HANDLE_SCTV_TERMINATION()
	
	#IF IS_DEBUG_BUILD
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === HANDLE_SCTV_TERMINATION - Terminate - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE")
	ENDIF
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === HANDLE_SCTV_TERMINATION - Terminate - NOT NETWORK_IS_GAME_IN_PROGRESS")
	ENDIF
	#ENDIF

	BOOL bTerminateNow = TRUE
	IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		bTerminateNow = FALSE
		IF IS_PLAYER_SWITCH_IN_PROGRESS() 
			IF GET_PLAYER_SWITCH_STATE() > SWITCH_STATE_PREP_FOR_CUT
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === HANDLE_SCTV_TERMINATION - bTerminateNow = TRUE")
				#ENDIF
				bTerminateNow = TRUE
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === HANDLE_SCTV_TERMINATION - Terminate delayed, GET_PLAYER_SWITCH_STATE < SWITCH_STATE_PREP_FOR_CUT")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === HANDLE_SCTV_TERMINATION - Terminate delayed, IS_PLAYER_SWITCH_IN_PROGRESS = FALSE")
			#ENDIF
		ENDIF
	ENDIF
	
	IF DID_QUIT_GAME_WITH_SPECTATOR_ACTIVE()
		bTerminateNow = TRUE
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === HANDLE_SCTV_TERMINATION - Terminate Enabled, DID_QUIT_GAME_WITH_SPECTATOR_ACTIVE = TRUE")
	ENDIF
	
	IF bTerminateNow
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === HANDLE_SCTV_TERMINATION - CLEANUP_SCTV")
		CLEANUP_SCTV()
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets a bail time for the sctv player depending on the current mission and playlist
/// RETURNS:
///    A time to wait before bailing SCTV
FUNC INT GET_SCTV_BAIL_TIME()
	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID(), FALSE)
			RETURN SCTV_EXTENDED_BAIL_TIME
		ENDIF
	ENDIF
	
	RETURN SCTV_BAIL_TIME
ENDFUNC

/// PURPOSE:
///    Gets the current SCTV stage
/// RETURNS:
///    The current SCTV as eSCTVStageList
FUNC eSCTVStageList GET_SCTV_STAGE()
	RETURN MPSpecGlobals.SCTVData.stage
ENDFUNC

/// PURPOSE:
///    Sets the current SCTV stage
/// PARAMS:
///    eStage - desired SCTV stage
PROC SET_SCTV_STAGE(eSCTVStageList eStage)
	IF GET_SCTV_STAGE() <> eStage
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SET_SCTV_STAGE(", GET_SCTV_STAGE_NAME(eStage), ")")
		#ENDIF
		MPSpecGlobals.SCTVData.stage = eStage
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the interp camera index in FMMC_CAM_DATA equal to the current camera index in FMMC_CAM_DATA
/// PARAMS:
///    sCamDataPassed - struct of data holding the camera index
PROC FORCE_INTERP_FREE_CAM(FMMC_CAM_DATA &sCamDataPassed)
	SET_CAM_ACTIVE(sCamDataPassed.cam, FALSE)
	IF NOT DOES_CAM_EXIST(sCamDataPassed.inTerp)
		sCamDataPassed.inTerp = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
	ENDIF
	SET_CAM_ACTIVE(sCamDataPassed.inTerp, TRUE)
	
	TAKE_CONTROL_OF_TRANSITION()

	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	SET_CAM_PARAMS(sCamDataPassed.inTerp, GET_CAM_COORD(sCamDataPassed.cam), 		GET_CAM_ROT(sCamDataPassed.cam),	GET_CAM_FOV(sCamDataPassed.cam))
	SET_CAM_PARAMS(sCamDataPassed.inTerp, GET_CAM_COORD(sCamDataPassed.cam), 		GET_CAM_ROT(sCamDataPassed.cam),	GET_CAM_FOV(sCamDataPassed.cam), 1000)
ENDPROC

/// PURPOSE:
///    Sets the creator camera FMMC_CAM_DATA to the current desired camPos and CamRot
/// PARAMS:
///    sCamDataPassed - 
///    fHeight - 
PROC CREATE_INIT_CAMERA_FOR_TUTORIAL(FMMC_CAM_DATA &sCamDataPassed, FLOAT fHeight = 40.0)
	IF NOT DOES_CAM_EXIST(sCamDataPassed.cam)
		sCamDataPassed.cam 	= CREATE_CAM("DEFAULT_SCRIPTED_FLY_CAMERA", FALSE)
	ENDIF
	
	SET_FLY_CAM_MAX_HEIGHT(sCamDataPassed.cam, 820)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//SET_ENTITY_COORDS(PLAYER_PED_ID(),  << 388.7828, -1742.3505, 28.3196 >>)
		VECTOR VTemp				
		IF NOT IS_VECTOR_ZERO(sCamDataPassed.camPos)
			SET_CAM_COORD(sCamDataPassed.cam, sCamDataPassed.camPos)
			SET_CAM_ROT(sCamDataPassed.cam, sCamDataPassed.camRot)
		ELSE
			IF GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(PLAYER_PED_ID()), VTemp)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), VTemp+<<0.0,0.0,1.0>>, DEFAULT, TRUE, DEFAULT, FALSE)
			ENDIF
			SET_CAM_COORD(sCamDataPassed.cam, GET_ENTITY_COORDS(PLAYER_PED_ID())+<<0.0,0.0, fHeight>>)
		ENDIF
		
		FORCE_INTERP_FREE_CAM(sCamDataPassed)
		
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
		SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Forces the creator cam to a specific position and rotation
/// PARAMS:
///    sFMMCdataPassed - 
///    sCamDataPassed - 
///    thisCoords - desired position
///    thisRotation - desired rotation
PROC SET_FREE_CAM_COORDS_AND_ROTATION(FMMC_LOCAL_STRUCT sFMMCdataPassed, FMMC_CAM_DATA &sCamDataPassed, VECTOR thisCoords, VECTOR thisRotation)
	sCamDataPassed.camPos = thisCoords
	sCamDataPassed.camRot = thisRotation
	CREATE_INIT_CAMERA_FOR_TUTORIAL(sCamDataPassed)
	MAINTAIN_PLAYER_COORDS(sFMMCdataPassed,  sCamDataPassed.cam)
ENDPROC

//PURPOSE: Returns TRUE if the external fade flag has been set and the post celeb scene has started
FUNC BOOL EXTERNAL_FADE_FLAG_READY()
	IF IS_DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB_SET()
		IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_EXTERNAL_FADE_FLAG_READY)
			IF IS_PLAYER_RUNNING_ANY_TYPE_OF_POST_MISSION_SCENE(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_SELECTED_PED()))
				SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_EXTERNAL_FADE_FLAG_READY)
				CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === MANAGE_EXTERNAL_FADES - SPEC_HUD_BS_EXTERNAL_FADE_FLAG_READY - SET")
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls fading the player out based on remote script flags
PROC MANAGE_EXTERNAL_FADES()
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
	AND GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM
	AND IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
	AND DOES_ENTITY_EXIST(GET_SPECTATOR_SELECTED_PED())
	AND IS_PED_A_PLAYER(GET_SPECTATOR_SELECTED_PED())
		PLAYER_INDEX playerID
		playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_SELECTED_PED())
		IF IS_SCREEN_FADED_IN()
			IF g_bCelebrationScreenIsActive = FALSE
				IF IS_PLAYER_RUNNING_ANY_TYPE_OF_POST_MISSION_SCENE(playerID)
					DO_SCREEN_FADE_OUT(0)
					SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN)
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === MANAGE_EXTERNAL_FADES - DO FADE OUT - SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN - SET")
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN)
			OR EXTERNAL_FADE_FLAG_READY()
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_PLAYER_RUNNING_ANY_TYPE_OF_POST_MISSION_SCENE(playerID)
						DO_SCREEN_FADE_IN(1000)
						CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN)
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === MANAGE_EXTERNAL_FADES - DO FADE IN - SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN - CLEARED")
						CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_EXTERNAL_FADE_FLAG_READY)
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === MANAGE_EXTERNAL_FADES - SPEC_HUD_BS_EXTERNAL_FADE_FLAG_READY - CLEARED")
						DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB(FALSE)
					ELSE
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === MANAGE_EXTERNAL_FADES - DON'T FADE IN YET - IS_PLAYER_RUNNING_A_POST_CELEBRATION_SCENE = TRUE")
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD IF GET_FRAME_COUNT() % 120 = 0 PRINTLN("=== SCTV === MANAGE_EXTERNAL_FADES - FADED/FADING OUT BUT OUR FLAGS ARE NOT SET") ENDIF #ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					IF IS_PLAYER_IN_CORONA()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === MANAGE_EXTERNAL_FADES - DO_SCREEN_FADE_IN - IS_PLAYER_IN_CORONA = TRUE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	//NOT VALID
	ELSE
		IF IS_SCREEN_FADED_OUT()
			IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN)
			OR IS_DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB_SET()
				DO_SCREEN_FADE_IN(1000)
				CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN)
				CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === MANAGE_EXTERNAL_FADES - DO FADE IN - SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN - CLEARED - BACKUP (NOT ABLE TO DO NORMAL CHECKS)")
				DO_SCTV_EXTERNAL_FADE_IN_AFTER_POST_CELEB(FALSE)
			ELSE
				#IF IS_DEBUG_BUILD IF GET_FRAME_COUNT() % 120 = 0 PRINTLN("=== SCTV === MANAGE_EXTERNAL_FADES - FADE OUT BUT OUR FLAGS ARE NOT SET - CHECK B") ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Puts the SCTV player, in follow cam mode, underneath the ground at -100z at the camera's x and y coords
PROC MANAGE_HIDE_SCTV_PLAYER()

	VECTOR vPlayerHide
	
	BOOL bCoordsSet = FALSE
	
	CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_SPECTATING_DIFFERENT_CAMERA)
	
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	AND NOT IS_SKYSWOOP_MOVING()
		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			IF GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM
				IF IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
					
					IF NOT bCoordsSet
						IF IS_PLAYER_SCTV(PLAYER_ID())
							IF GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM
								IF DOES_ENTITY_EXIST(GET_SPECTATOR_SELECTED_PED())
									IF IS_PED_A_PLAYER(GET_SPECTATOR_SELECTED_PED())
										PLAYER_INDEX playerID
										playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_SELECTED_PED())
										IF NETWORK_IS_PLAYER_ACTIVE(playerID)
											IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerID, MPAM_TYPE_CINEMA)
											
												IF (g_bUpdateSpectatorPosition)
													vPlayerHide = GET_ENTITY_COORDS(GET_SPECTATOR_SELECTED_PED(), FALSE) //+ <<0.0, 0.0, -50.0>>
												ENDIF
												SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_SPECTATING_DIFFERENT_CAMERA)
												bCoordsSet = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
		
					IF NOT bCoordsSet
						IF (g_bUpdateSpectatorPosition)
							IF NOT HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
								vPlayerHide = GET_FINAL_RENDERED_CAM_COORD()
							ELSE
								vPlayerHide = GET_SPECTATOR_OVERRIDE_COORDS()
							ENDIF
						ENDIF
						//vPlayerHide.z = -100.0
						bCoordsSet = TRUE
					ENDIF
					
					IF IS_SCREEN_FADED_OUT()
						bCoordsSet = FALSE
					ENDIF
					
					IF bCoordsSet
						IF (g_bUpdateSpectatorPosition)
							IF VDIST2(vPlayerHide, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 250.0
								SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerHide, FALSE, TRUE, DEFAULT, FALSE)
								CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MANAGE_HIDE_SCTV_PLAYER - Setting player position to <<", vPlayerHide.x, ", ", vPlayerHide.y, ", ", vPlayerHide.z, ">>")
							ENDIF
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
								IF NOT bIgnoreBulletCollisionSet
									SET_ENTITY_COMPLETELY_DISABLE_COLLISION(PLAYER_PED_ID(), FALSE)
									CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MANAGE_HIDE_SCTV_PLAYER - Bullet collision turned off playing Gunsmith. See url:bugstar:2989819.")
									bIgnoreBulletCollisionSet = TRUE
								ENDIF
							ELSE
								SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
							ENDIF
							
							SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the player in the required state to use SCTV (hidden, invincible etc)
PROC SET_SCTV_PLAYER_PED_STATE()
	IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
		DETACH_ENTITY(PLAYER_PED_ID())
	ENDIF
	IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
	ENDIF
	SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
	SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
	SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
	INVALIDATE_IDLE_CAM()
ENDPROC

/// PURPOSE:
///    Starts loading at the current position of the creator cam
PROC START_FREE_CAM_LOAD_CONTROL()
	IF NOT IS_TRANSITION_SESSION_RELAUNCHING_FREEMODE_AS_SCTV()
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		NEW_LOAD_SCENE_START_SPHERE(sCamData.camPos, 50.0)
		
		iFreeCamFadeSwitchStage = 0
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === START_FREE_CAM_LOAD_CONTROL")
		#ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Returns true once a load scene started by START_FREE_CAM_LOAD_CONTROL has finished
/// PARAMS:
///    bDoFades - fade in once complete
/// RETURNS:
///    true once complete
FUNC BOOL HAS_FREE_CAM_LOAD_CONTROL_FINISHED(BOOL bDoFades = TRUE)

	IF IS_TRANSITION_SESSION_RELAUNCHING_FREEMODE_AS_SCTV()
		RETURN TRUE
	ELSE
		SWITCH iFreeCamFadeSwitchStage
			CASE -1
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === HAS_FREE_CAM_LOAD_CONTROL_FINISHED CALLED BEFORE BEING SET UP CORRECTLY RETURN TRUE")
				#ENDIF
				RETURN TRUE
			BREAK
			CASE 0
				timeFreeCamFadeSafety = timeNet
				iFreeCamFadeSwitchStage++
			BREAK
			CASE 1
				IF IS_NEW_LOAD_SCENE_ACTIVE()
				AND (NOT IS_NEW_LOAD_SCENE_LOADED())
				AND GET_TIME_DIFFERENCE(timeNet, timeFreeCamFadeSafety) < 10000		//If loading something AND time limit not up
					
				ELSE
					iFreeCamFadeSwitchStage++
				ENDIF
			BREAK
			CASE 2
				IF IS_SCREEN_FADED_IN()
				OR NOT bDoFades
				OR IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
					NEW_LOAD_SCENE_STOP()
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === HAS_FREE_CAM_LOAD_CONTROL_FINISHED RETURN TRUE")
					#ENDIF
					iFreeCamFadeSwitchStage = -1
					RETURN TRUE
				ELSE
					IF bDoFades
						IF NOT IS_SCREEN_FADING_IN()
						AND NOT IS_SCREEN_FADED_IN()
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === Fade in 1")
							#ENDIF
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Listen for our transition session flag informing us we are moving sessions with target (should not bail)
PROC MAINTAIN_SCTV_FOLLOW_TARGET_TO_SESSION()

	IF IS_TRANSITION_SESSION_ACTIONING_FOLLOW_INVITE()
		BOOL bIgnoreInvite = FALSE
		
		IF GET_SCTV_MODE() = SCTV_MODE_FREE_CAM
			bIgnoreInvite = TRUE
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_FOLLOW_TARGET_TO_SESSION - bIgnoreInvite = TRUE - IN FREECAM")
		ENDIF
		
		//Don't follow if Player is GTAOTV Spectator in Tournament
		IF IS_PLAYER_A_ROCKSTAR_DEV()
		AND IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
			bIgnoreInvite = TRUE
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_FOLLOW_TARGET_TO_SESSION - bIgnoreInvite = TRUE - DEV IN TOURNAMENT")
		ENDIF
		
		IF bIgnoreInvite = FALSE
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_FOLLOW_TARGET_TO_SESSION - We are moving to new session with target. Change modes and set timer")
			
			// Get the spinner displaying 'Loading'
			BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("MP_SPINLOADING")
			END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
			
			SET_SKYFREEZE_FROZEN()
			
			timerFollowToSession = GET_NETWORK_TIME()
			SWITCH_SCTV_MODE(SCTV_MODE_FOLLOW_TO_SESSION)
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_FOLLOW_TARGET_TO_SESSION - We are in free cam so should ignore this: NETWORK_CLEAR_FOLLOW_INVITE()")
			
			CLEAR_TRANSITION_SESSION_ACTIONING_FOLLOW_INVITE()
			NETWORK_CLEAR_FOLLOW_INVITE()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Every-frame function that manages the camera mode currently being used by SCTV
PROC PERFORM_CURRENT_SCTV_MODE()
	VECTOR vCamPos
	
	PLAYER_INDEX playerCurrentFocus 
	
	USING_MISSION_CREATOR(FALSE) // set this by default, it gets set on when in free cam and in the pause menu.
	
	MANAGE_HIDE_SCTV_PLAYER()	//hide the player
	
	SWITCH GET_SCTV_MODE()
					
		CASE SCTV_MODE_INIT		//initialise
		
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === PERFORM_CURRENT_SCTV_MODE = SCTV_MODE_INIT ")
					ENDIF
				ENDIF
			#ENDIF
		
			IF NETWORK_IS_PLAYER_CONNECTED(PLAYER_ID())
				POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(specData)
				SET_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_IS_SPECTATING)
				RESET_NET_TIMER(SpectatingYouTickerTimer)
				
				IF IS_VALID_SPECTATE_TARGET_IN_SESSION()			//If someone is in game, follow them - or if spectate player
				OR NOT IS_PLAYER_A_ROCKSTAR_DEV()
					SWITCH_SCTV_MODE(SCTV_MODE_FOLLOW_CAM)
				ELSE
					SWITCH_SCTV_MODE(SCTV_MODE_FREE_CAM)			//otherwise freecam
				ENDIF
			ENDIF
		BREAK
		
		CASE SCTV_MODE_FOLLOW_CAM		//standard spectator cam
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === PERFORM_CURRENT_SCTV_MODE = SCTV_MODE_FOLLOW_CAM ")
					ENDIF
				ENDIF
			#ENDIF
			
			IF IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_QUIT_TO_JOIN_NORMAL_SESSION)
				IF IS_SKYSWOOP_MOVING()
					//IF HAS_NET_TIMER_EXPIRED(EndSCTVDelay, 3000)
						//DO_SCREEN_FADE_OUT(0)//snap to black!
						SET_ENTERED_GAME_AS_SCTV(FALSE)
						SET_PLAYER_TEAM(PLAYER_ID(), TEAM_FREEMODE)
						MPSpecGlobals.SCTVData.stage = SCTV_STAGE_CLEANUP
						CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SCTV_BIT_JOINED_TRANSITION DONE")
						CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_QUIT_TO_JOIN_NORMAL_SESSION)
						RESET_NET_TIMER(EndSCTVDelay)
						EXIT
					//ENDIF
				ENDIF
			ENDIF
			
			IF specData.specCamData.eState = eSPECCAMSTATE_WATCHING
				SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_READY_FOR_MODE_SWITCH)
			ELSE
				CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_READY_FOR_MODE_SWITCH)
			ENDIF
			
			MAINTAIN_SCTV_FOLLOW_TARGET_TO_SESSION()
			
			IF NETWORK_GET_TOTAL_NUM_PLAYERS() < 1	//if we're all alone, bail out
				IF SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS()
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === NETWORK_GET_TOTAL_NUM_PLAYERS() = ", NETWORK_GET_TOTAL_NUM_PLAYERS())
					#ENDIF
					BAIL_FROM_SCTV_FOLLOW()
					MPSpecGlobals.iBailReason = SCTV_BAIL_REASON_NO_PLAYERS
				ENDIF
			ENDIF
						
			SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
			IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)	//make sure that the spectator cam is active
				SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
				ACTIVATE_SPECTATOR_CAM(specData, SPEC_MODE_SCTV, NULL, ASCF_QUIT_SCREEN)
			ELSE
				//Send a Ticker to the Target after a short time
				IF NOT IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_SPECTATING_YOU_TICKER_DONE)
					IF HAS_NET_TIMER_EXPIRED(SpectatingYouTickerTimer, SPECTATING_YOU_TICKER_DELAY)
						IF NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget) > -1
							IF NETWORK_IS_PLAYER_ACTIVE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
								TickerEventData.TickerEvent = TICKER_EVENT_SPECTATING_YOU
								BROADCAST_TICKER_EVENT(TickerEventData, SPECIFIC_PLAYER(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget))
							ENDIF
						ENDIF
						SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_SPECTATING_YOU_TICKER_DONE)
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === BROADCAST SPECTATING YOU TICKER() = ")
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
				IF IS_PED_A_PLAYER(GET_SPECTATOR_CURRENT_FOCUS_PED())
					playerCurrentFocus = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())
					IF NETWORK_IS_PLAYER_ACTIVE(playerCurrentFocus)
						IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget <> playerCurrentFocus
							IF CAN_SET_SCTV_FOLLOW_TARGET()
								
								IF NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget) > -1
									IF NETWORK_IS_PLAYER_ACTIVE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)	
										TickerEventData.TickerEvent = TICKER_EVENT_STOPPED_SPECTATING_YOU
										BROADCAST_TICKER_EVENT(TickerEventData, SPECIFIC_PLAYER(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget))
									ENDIF
								ENDIF
								
								SET_SCTV_FOLLOW_TARGET(playerCurrentFocus)	//set current SCTV focus to current focus, so we'll follow the focus if they change session
								
								RESET_NET_TIMER(SpectatingYouTickerTimer)
								CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_SPECTATING_YOU_TICKER_DONE)
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			MANAGE_EXTERNAL_FADES()
			
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				IF IS_SCREEN_FADED_IN()
				AND NOT IS_SPECTATOR_RUNNING_CUTSCENE()
				AND IS_SKYSWOOP_AT_GROUND()
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_SET_INVISIBLE|NSPC_NO_COLLISION|NSPC_FREEZE_POSITION|NSPC_PREVENT_COLLISION_CHANGES|NSPC_DONT_KILL_SPECTATE)
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === PUT PLAYER CONTROL BACK ON - FOLLOW")
				ENDIF
			ENDIF
		BREAK
		
		CASE SCTV_MODE_FREE_CAM		//modified creator cam
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === PERFORM_CURRENT_SCTV_MODE = SCTV_MODE_FREE_CAM ")
					ENDIF
				ENDIF
			#ENDIF
		
		
			IF NOT IS_PLAYER_A_ROCKSTAR_DEV()	//if we are not rockstar dev, bail to follow cam
			OR (NETWORK_IS_ACTIVITY_SESSION()
				AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_IN_CORONA 
				AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget = INVALID_PLAYER_INDEX())
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === NON ROCKSTAR DEV ENDED UP IN FREE CAM SWITCH_SCTV_MODE(SCTV_MODE_FOLLOW_CAM)")
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget = ", NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget))
				#ENDIF
				SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_READY_FOR_MODE_SWITCH)
				SWITCH_SCTV_MODE(SCTV_MODE_FOLLOW_CAM)
			ELSE
				// Hiding HUD / Radar from dev stream for 1801068
				HIDE_HUD_AND_RADAR_THIS_FRAME()				
			
				IF IS_BIT_SET(sFMMCdata.iBitSet, biCameraNotTerping)
				AND IS_BIT_SET(sFMMCdata.iBitSet, bCameraActive)
					SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_READY_FOR_MODE_SWITCH)
				ELSE
					CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_READY_FOR_MODE_SWITCH)
				ENDIF
				
				IF IS_PAUSE_MENU_ACTIVE()
					USING_MISSION_CREATOR(TRUE) // this command allow you to warp in the front end map (using x)
					NET_NL()NET_PRINT("SCTV.sc SCTV_MODE_FREE_CAM - calling USING_MISSION_CREATOR(TRUE)  ")						
				ENDIF
				
				MAINTAIN_SCTV_FOLLOW_TARGET_TO_SESSION()
				
				IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
					MPSpecGlobals.SCTVData.iFreeCamTransitionCatchCounter = 0
				ELSE
					SWITCH MPSpecGlobals.SCTVData.iFreeCamTransitionCatchCounter				//Catch the camera coming back down from MP menu
						CASE 0																	//(after DPADDOWN)
							IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
								IF IS_SKYSWOOP_MOVING()
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === Transition menu activated, preparing to handle transition back to SCTV freecam")
									#ENDIF
									MPSpecGlobals.SCTVData.iFreeCamTransitionCatchCounter++
								ENDIF
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_SKYSWOOP_MOVING()
								MPSpecGlobals.SCTVData.iFreeCamTransitionCatchCounter++
							ENDIF
						BREAK
						CASE 2
							IF IS_SKYSWOOP_MOVING()
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === Doing transition back to SCTV freecam")
								#ENDIF
								FORCE_INTERP_FREE_CAM(sCamData)
								MPSpecGlobals.SCTVData.iFreeCamTransitionCatchCounter = 0
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
				
				POPULATE_SPECTATOR_CAM_TARGET_LISTS(specData)		//make sure list is populated
				MAINTAIN_FMMC_CAMERA(sFMMCdata, sCamData, -1)
				CHANGE_SCRIPT_FLY_CAM_ROTATION(sCamData.cam, 0.015)
				
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					vCamPos = GET_CAM_COORD(sCamData.cam)
					IF vCamPos.x <> 0.0
					OR vCamPos.y <> 0.0
					OR vCamPos.z <> 0.0
						SET_FOCUS_POS_AND_VEL(vCamPos, <<0,0,0>>)
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === SCTV_MODE_FREE_CAM - SET_FOCUS_POS_AND_VEL ", vCamPos)
					ENDIF
				ENDIF
			
			ENDIF
			
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				IF IS_SCREEN_FADED_IN()
				AND IS_SKYSWOOP_AT_GROUND()
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_SET_INVISIBLE|NSPC_NO_COLLISION|NSPC_FREEZE_POSITION|NSPC_PREVENT_COLLISION_CHANGES|NSPC_DONT_KILL_SPECTATE)
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === PUT PLAYER CONTROL BACK ON - FREE CAM")
				ENDIF
			ENDIF
		BREAK
		
		CASE SCTV_MODE_FOLLOW_TO_SESSION			
			THEFEED_HIDE_THIS_FRAME()
			
			
			INT iTimeout
			iTimeout =  g_sMPTunables.iSpectatorChangeSessionTime
			IF HAS_TRANSITION_SESSION_CALLED_ACTION_FOLLOW_INVITE()	
				iTimeout +=  g_sMPTunables.iSpectatorChangeSessionTime
				//Give more time in debug. 1820606
				#IF IS_DEBUG_BUILD
				iTimeout +=  g_sMPTunables.iSpectatorChangeSessionTime
				#ENDIF
			ENDIF
			
			SET_SKYSWOOP_UP(TRUE, DEFAULT, FALSE, DEFAULT, TRUE)
			
			IF ABSI(GET_TIME_DIFFERENCE(timerFollowToSession, GET_NETWORK_TIME())) > iTimeout
			//and we've not accepted the follow invite
			AND NOT IS_TRANSITION_SESSION_ACCEPTING_FOLLOW_INVITE()	
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MODE: SCTV_MODE_FOLLOW_TO_SESSION timed out. NETWORK_BAIL, Frame: ", GET_FRAME_COUNT(), " T(", GET_CLOUD_TIME_AS_INT(), ")")
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("=== SCTV === MODE: SCTV_MODE_FOLLOW_TO_SESSION timed out. NETWORK_BAIL, Frame: ", GET_FRAME_COUNT(), " T(", GET_CLOUD_TIME_AS_INT(), ")")
				#ENDIF
				BAIL_FROM_SCTV_FOLLOW(TRUE)
				MPSpecGlobals.iBailReason = SCTV_BAIL_REASON_FOLLOW_TO_SESSION_TIMEOUT
			ENDIF
		BREAK
				
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Sets the stage in the process of switching SCTV between different camera modes
/// PARAMS:
///    modeSwitchStage - stage to change to
PROC SET_SCTV_MODE_SWITCH_STAGE(eSCTVModeSwitchStageList modeSwitchStage)
	IF MPSpecGlobals.SCTVData.modeSwitchStage <> modeSwitchStage
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SET_SCTV_MODE_SWITCH_STAGE(", GET_SCTV_MODE_SWITCH_STAGE_NAME(modeSwitchStage), ")")
		#ENDIF
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("=== SCTV === SET_SCTV_MODE_SWITCH_STAGE(", GET_SCTV_MODE_SWITCH_STAGE_NAME(modeSwitchStage), ")")
		#ENDIF
		MPSpecGlobals.SCTVData.modeSwitchStage = modeSwitchStage
	ENDIF
ENDPROC

/// PURPOSE:
///    Transitions between the different SCTV camera modes, and then performs that mode once transitioned
PROC PERFORM_SCTV_MODE()
	BOOL bProgress = FALSE
	VECTOR vCamPos
	
	SWITCH MPSpecGlobals.SCTVData.modeSwitchStage
		
		CASE SCTV_MODE_SWITCH_STAGE_START	//begin transition
			bProgress = TRUE
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === PERFORM_SCTV_MODE = SCTV_MODE_SWITCH_STAGE_START ")
					ENDIF
				ENDIF
			#ENDIF
			
			IF bProgress
				SET_SCTV_MODE_SWITCH_STAGE(SCTV_MODE_SWITCH_STAGE_FADE_OUT)
			ENDIF
		BREAK
		
		CASE SCTV_MODE_SWITCH_STAGE_FADE_OUT	//fade out if necessary
			bProgress = FALSE
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === PERFORM_SCTV_MODE = SCTV_MODE_SWITCH_STAGE_FADE_OUT ")
					ENDIF
				ENDIF
			#ENDIF
			#IF USE_FINAL_PRINTS
				IF GET_FRAME_COUNT() % 10 = 0
					PRINTLN_FINAL("=== SCTVSTAGES === PERFORM_SCTV_MODE = SCTV_MODE_SWITCH_STAGE_FADE_OUT ")
				ENDIF
			#ENDIF
			
			IF GET_SCTV_MODE() = SCTV_MODE_INIT	//to stop fading during skycam from one event to another in SCTV
			AND MPSpecGlobals.SCTVData.switchToMode = SCTV_MODE_FOLLOW_CAM
			AND IS_PLAYER_SWITCH_IN_PROGRESS()
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === Don't fade out to new mode since going from init to follow in skycam")
				#ENDIF
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("=== SCTV === Don't fade out to new mode since going from init to follow in skycam")
				#ENDIF
				bProgress = TRUE
			ELSE
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
					IF NOT IS_SCREEN_FADING_OUT()
						IF GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM			//If leaving or going to follow cam
						OR MPSpecGlobals.SCTVData.switchToMode = SCTV_MODE_FOLLOW_CAM
							DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT()
						ENDIF
						IF NOT SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
						AND GET_SCTV_SWITCHING_TO_MODE() != SCTV_MODE_FOLLOW_TO_SESSION
							IF g_bCelebrationScreenIsActive = FALSE
								CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === Fade out 1")
								DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
							ELSE
								bProgress = TRUE
								CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === Don't fade out to new cam mode since g_bCelebrationScreenIsActive = TRUE")
							ENDIF
						ELSE
							bProgress = TRUE
							CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === Don't fade out to new cam mode since SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA")
						ENDIF
					ENDIF
				ELSE
					bProgress = TRUE
				ENDIF
			ENDIF
			
			IF bProgress
				SET_SCTV_MODE_SWITCH_STAGE(SCTV_MODE_SWITCH_STAGE_CLEANUP_OLD)
			ENDIF
		BREAK
		
		CASE SCTV_MODE_SWITCH_STAGE_CLEANUP_OLD	//cleanup the previous camera mode
			bProgress = FALSE
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === Doing Cleanup for ", GET_SCTV_MODE_NAME(GET_SCTV_MODE()))
			#ENDIF
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("=== SCTV === Doing Cleanup for ", GET_SCTV_MODE_NAME(GET_SCTV_MODE()))
			#ENDIF			
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === PERFORM_SCTV_MODE = SCTV_MODE_SWITCH_STAGE_CLEANUP_OLD ")
					ENDIF
				ENDIF
			#ENDIF
			
			#IF USE_FINAL_PRINTS
				IF GET_FRAME_COUNT() % 10 = 0
					PRINTLN_FINAL("== SCTVSTAGES === PERFORM_SCTV_MODE = SCTV_MODE_SWITCH_STAGE_CLEANUP_OLD ")
				ENDIF
			#ENDIF
			
			SWITCH GET_SCTV_MODE()
							
				CASE SCTV_MODE_INIT
					bProgress = TRUE
				BREAK
				
				CASE SCTV_MODE_FOLLOW_CAM
					ENABLE_WEAPON_SWAP()
					Enable_MP_Comms()
					FORCE_CLEANUP_SPECTATOR_CAM(specData)
					SET_SCTV_PLAYER_PED_STATE()
					iFollowRetryAttempts = 50	//load up follow start with 50 attempts
					bProgress = TRUE
				BREAK
				
				CASE SCTV_MODE_FREE_CAM
					ENABLE_WEAPON_SWAP()
					Enable_MP_Comms()
					CLEAN_UP_FMMC_CAMERA(DEFAULT, FALSE)
					
					CLEAR_BIT(sFMMCdata.iBitSet, biCameraNotTerping)
					
					g_iOverheadNamesState = -1					//Turn off player names
					
					IF DOES_CAM_EXIST(sCamData.cam)
						DESTROY_CAM(sCamData.cam)
					ENDIF
					IF DOES_CAM_EXIST(sCamData.inTerp)
						DESTROY_CAM(sCamData.inTerp)
					ENDIF
					
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					SET_RADAR_ZOOM(0)
					
					SET_SCTV_PLAYER_PED_STATE()
					
					SET_IN_SPECTATOR_MODE(FALSE)
					
					CLEAR_FOCUS()
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === SCTV_MODE_FREE_CAM - CLEAR_FOCUS() ")
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("=== SCTVSTAGES === SCTV_MODE_FREE_CAM - CLEAR_FOCUS() ")
					#ENDIF
					bProgress = TRUE
				BREAK
				
				CASE SCTV_MODE_FOLLOW_TO_SESSION
					BUSYSPINNER_OFF()
					bProgress = TRUE
				BREAK
				
			ENDSWITCH
			
			IF bProgress
				SET_SCTV_MODE_SWITCH_STAGE(SCTV_MODE_SWITCH_STAGE_SETUP_NEW)
			ENDIF
		BREAK
		
		CASE SCTV_MODE_SWITCH_STAGE_SETUP_NEW		//setup the desired camera mode
			bProgress = FALSE
			
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === PERFORM_SCTV_MODE = SCTV_MODE_SWITCH_STAGE_SETUP_NEW ")
					ENDIF
				ENDIF
			#ENDIF
			
			#IF USE_FINAL_PRINTS
				IF GET_FRAME_COUNT() % 10 = 0
					PRINTLN_FINAL("=== SCTVSTAGES === PERFORM_SCTV_MODE = SCTV_MODE_SWITCH_STAGE_SETUP_NEW ")
				ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === Doing Setup for ", GET_SCTV_MODE_NAME(MPSpecGlobals.SCTVData.switchToMode))
			#ENDIF
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("=== SCTV === Doing Setup for ", GET_SCTV_MODE_NAME(MPSpecGlobals.SCTVData.switchToMode))
			#ENDIF
			
			SWITCH MPSpecGlobals.SCTVData.switchToMode
							
				CASE SCTV_MODE_INIT
					bProgress = TRUE
				BREAK
				
				CASE SCTV_MODE_FOLLOW_CAM
					/*IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_POPULATE_FOR_TOURNAMENT)
						POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(specData)
						CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_POPULATE_FOR_TOURNAMENT)
					ENDIF*/
	
					bProgress = FALSE
					
					POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(specData)
				
					// try and return to the target you were last spectating
					IF SPEC_IS_PED_ACTIVE(specData.pedLastCurrentFocus)
					AND IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(specData, specData.pedLastCurrentFocus)
						
						SET_SPECTATOR_DESIRED_FOCUS_PED(specData, specData.pedLastCurrentFocus)
						SET_SPECTATOR_CURRENT_FOCUS_PED(specData, specData.pedLastCurrentFocus)
						
						INT iList, iEntry
						IF FIND_PED_IN_SPECTATOR_HUD_TARGET_LISTS(specData.specHUDData, iList, iEntry, specData.pedLastCurrentFocus)
							SET_SPECTATOR_HUD_CURRENT_FOCUS_TARGET_FROM_LIST(specData, iList, iEntry)
						ENDIF
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SETUP SCTV_MODE_FOLLOW_CAM - using last player")
						#ENDIF
						bProgress = TRUE
						
					ELSE
					
						INT iList, iEntry
						IF SHOULD_FIND_ANOTHER_SPECTATOR_TARGET(specData, TRUE)
							IF GET_SPECTATOR_HUD_CURRENTLY_VALID_LISTS_ENTRY(specData, iList, iEntry)
								SET_SPECTATOR_HUD_CURRENT_FOCUS_TARGET_FROM_LIST(specData, iList, iEntry)
								SET_SPECTATOR_DESIRED_FOCUS_PED(specData, GET_SPECTATOR_HUD_TARGET_LIST_ENTRY_PED_ID(specData.specHUDData, iList, iEntry))
								SET_SPECTATOR_CURRENT_FOCUS_PED(specData, GET_SPECTATOR_HUD_TARGET_LIST_ENTRY_PED_ID(specData.specHUDData, iList, iEntry))
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SETUP SCTV_MODE_FOLLOW_CAM - found new target to follow")
								#ENDIF
								bProgress = TRUE
							ELSE
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SETUP SCTV_MODE_FOLLOW_CAM - no target appropriate!")
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SETUP SCTV_MODE_FOLLOW_CAM - no players, not allowed to find spectator target!")
							#ENDIF
						ENDIF
						
					ENDIF
					
					IF NOT bProgress
						iFollowRetryAttempts-- //take away one follow retry
						IF iFollowRetryAttempts < 0//out of retries
							bProgress = TRUE
						ENDIF
					ENDIF
					
					
					IF bProgress					//if we were successful
						specData.eStartMode = SPEC_MODE_SCTV
						SETUP_FORCE_SPECTATOR_CAM(specData.specCamData, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV(), FALSE)
						Disable_MP_Comms()
						SET_SCTV_PLAYER_PED_STATE()
					ENDIF
				BREAK
				
				CASE SCTV_MODE_FREE_CAM
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					SET_BIT(sFMMCdata.iBitSet, bCameraActive)
					Disable_MP_Comms()
					SET_RADAR_ZOOM(1350)
					SET_IN_SPECTATOR_MODE(TRUE, PLAYER_PED_ID())
					NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
					g_iOverheadNamesState = TAGS_ON					//Turn on player names
					
					VECTOR vPos, vRot
					vPos = <<0.0, 0.0, 0.0>>
					vRot = <<-90.0, 0.0, 0.0>>
					
					IF SPEC_IS_PED_ACTIVE(specData.specHUDData.specHUDCurrentFocusTarget.pedIndex)
						PED_INDEX pedCamPosition
						pedCamPosition = specData.specHUDData.specHUDCurrentFocusTarget.pedIndex
						
						IF DOES_ENTITY_EXIST(pedCamPosition)
							vPos = GET_ENTITY_COORDS(pedCamPosition) + <<0.0, 0.0, 99.0>>
						ENDIF
					ENDIF
					
					IF IS_VECTOR_ZERO(vPos)
						vPos = <<239.1166, -442.7514, 374.2336>>
						vRot = <<-35.0000, 0.0002, 130.0562>>
					ENDIF
					
					// Set our bit to allow you to quit from free cam (does not use normal spectator cam activation)
					SET_BIT(specData.specCamData.iActivateBitset, ASCF_INDEX_QUIT_SCREEN)
					
					SET_FREE_CAM_COORDS_AND_ROTATION(sFMMCdata, sCamData, vPos, vRot)
					
					START_FREE_CAM_LOAD_CONTROL()
					
					vCamPos = GET_CAM_COORD(sCamData.cam)
					IF vCamPos.x <> 0.0
					OR vCamPos.y <> 0.0
					OR vCamPos.z <> 0.0
						SET_FOCUS_POS_AND_VEL(vCamPos, <<0,0,0>>)
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === SCTV_MODE_FREE_CAM - B - SET_FOCUS_POS_AND_VEL ", vCamPos)
					ENDIF
					
					SET_SCTV_PLAYER_PED_STATE()
					
					bProgress = TRUE
				BREAK
				
				CASE SCTV_MODE_FOLLOW_TO_SESSION
					bProgress = TRUE
				BREAK
				
			ENDSWITCH
			
			IF bProgress
				specData.specCamData.bFirstTransitionComplete = TRUE
				SET_SCTV_MODE_SWITCH_STAGE(SCTV_MODE_SWITCH_STAGE_FINISH)
			ENDIF
		BREAK
		
		CASE SCTV_MODE_SWITCH_STAGE_FINISH		//cleanup the transition process
		
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === PERFORM_SCTV_MODE = SCTV_MODE_SWITCH_STAGE_FINISH ")
					ENDIF
				ENDIF
			#ENDIF
			
			bProgress = FALSE
			
			SWITCH MPSpecGlobals.SCTVData.switchToMode
							
				CASE SCTV_MODE_INIT
					bProgress = TRUE
				BREAK
				
				CASE SCTV_MODE_FOLLOW_CAM
					bProgress = TRUE
				BREAK
				
				CASE SCTV_MODE_FREE_CAM
					HIDE_HUD_AND_RADAR_THIS_FRAME()
				
					IF HAS_FREE_CAM_LOAD_CONTROL_FINISHED()
						bProgress = TRUE
					ENDIF
				BREAK
				
				CASE SCTV_MODE_FOLLOW_TO_SESSION
					bProgress = TRUE
				BREAK
				
			ENDSWITCH
			
			IF bProgress
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === Spectator switched mode from ", GET_SCTV_MODE_NAME(GET_SCTV_MODE())," to ", GET_SCTV_MODE_NAME(MPSpecGlobals.SCTVData.switchToMode)) 
				#ENDIF
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("=== SCTV === Spectator switched mode from ", GET_SCTV_MODE_NAME(GET_SCTV_MODE()), " to ", GET_SCTV_MODE_NAME(MPSpecGlobals.SCTVData.switchToMode))
				#ENDIF
				
				MPSpecGlobals.SCTVData.currentMode = MPSpecGlobals.SCTVData.switchToMode
				SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
				SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
				ENABLE_SPECTATOR_FADES()
			
				SET_SCTV_MODE_SWITCH_STAGE(SCTV_MODE_SWITCH_STAGE_COMPLETE)
			ENDIF
		BREAK
		
		CASE SCTV_MODE_SWITCH_STAGE_COMPLETE		//perform current mode
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === PERFORM_SCTV_MODE = SCTV_MODE_SWITCH_STAGE_COMPLETE ")
					ENDIF
				ENDIF
			#ENDIF
			
			PERFORM_CURRENT_SCTV_MODE()
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Checks the current spectated target to see if they have started a transition, and follows them into the same transition
PROC MANAGE_JOINING_INSTANCES()

	//Join a mission if spectator target is in one
	IF GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM
		IF SPEC_IS_PED_ACTIVE(GET_SPECTATOR_CURRENT_FOCUS_PED())
		AND IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(specData, GET_SPECTATOR_CURRENT_FOCUS_PED(), FALSE)
			
			IF IS_PED_A_PLAYER(GET_SPECTATOR_CURRENT_FOCUS_PED())
				IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
					
					// Check if we need to even process the joining of a transition session. If we are moving to another session as non-sctv skip
					IF GET_CONFIRM_INVITE_INTO_GAME_STATE() AND NOT IS_INVITED_TO_SCTV()
						CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MANAGE_JOINING_INSTANCES - Skip joining instances as transitioning to new session not as SCTV")
						EXIT
					ENDIF
					
					PLAYER_INDEX focusPlayer  = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())		
					
					IF NETWORK_HAS_PLAYER_STARTED_TRANSITION(focusPlayer)
						IF IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_JOINED_TRANSITION)
							//If we got a started failed event
							IF TRANSITION_SESSIONS_STARTED_EVENT_FAILED()
							AND NOT GOT_SESSION_RESPONSE_DENY_NOT_JOINABLE()
								CLEAR_TRANSITION_SESSIONS_STARTED_EVENT_FAILED()
								//1707391 - Please clean up spectator if there are no slots in the transition session
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === TRANSITION_SESSIONS_STARTED_EVENT_FAILED, NETWORK_BAIL()")
								CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_SCTV_NO_SLOTS_IN_SESSION)")
								#ENDIF
								SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_SCTV_NO_SLOTS_IN_SESSION)
								RESET_JOIN_TRANSITION_SESSION_AS_A_SPECTATOR_VARS(sJoinTransitionSpecVars)
								NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_SCTV_TS_STARTED_EVENT_FAILED))
							ENDIF
						ELSE				
							IF JOIN_TRANSITION_SESSION_AS_A_SPECTATOR(sJoinTransitionSpecVars, focusPlayer)
								SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_JOINED_TRANSITION)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_JOINED_TRANSITION)")
								#ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_JOINED_TRANSITION)
							CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_JOINED_TRANSITION)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_JOINED_TRANSITION)")
							#ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Performs SCTV initialisation
PROC DO_SCTV_STAGE_INITIALISE()
	IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL
	OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_TERMINATE_MAINTRANSITION
	OR IS_PLAYER_IN_LAUNCHED_TRANSITION_SESSION_CORONAS()
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === DO_SCTV_STAGE_INITIALISE()")
		IF IS_PLAYER_IN_LAUNCHED_TRANSITION_SESSION_CORONAS()
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === DO_SCTV_STAGE_INITIALISE() - IS_PLAYER_IN_LAUNCHED_TRANSITION_SESSION_CORONAS")
		ENDIF
		#ENDIF
		
		IF GET_STAT_CHARACTER_TEAM() = TEAM_SCTV
			SET_PLAYER_TEAM(PLAYER_ID(), TEAM_SCTV)
			//TEST NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			
			SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
		ENDIF
		
		CLEAR_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_SCTV_NO_OTHER_PLAYERS)
		CLEAR_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_SCTV_NO_SLOTS_IN_SESSION)
		
		DISABLE_INTERACTION_MENU()
		
		IF IS_PLAYER_A_ROCKSTAR_DEV()
			START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
			START_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === DO_SCTV_STAGE_INITIALISE - GTATV - DISABLE RADIO - MP_POSITIONED_RADIO_MUTE_SCENE + MP_JOB_CHANGE_RADIO_MUTE")
		ENDIF
		
		MPSpecGlobals.SCTVData.coronaState = SCTV_CORONA_STATE_NULL
		
		SET_SCTV_STAGE(SCTV_STAGE_RUNNING)
	
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === DO_SCTV_STAGE_INITIALISE - WAITING FOR GET_CURRENT_TRANSITION_STATE to equal TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL or TRANSITION_STATE_TERMINATE_MAINTRANSITION or IS_PLAYER_IN_LAUNCHED_TRANSITION_SESSION_CORONAS")
	#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Watches if the current spectated player is no longer in a corona, and if so make sure local player cleans up corona
PROC MAINTAIN_SCTV_CORONA_TARGET()
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget, FALSE, FALSE)
		IF NOT IS_THIS_PLAYER_IN_CORONA(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_CORONA_TARGET - Target no longer in corona reset, coronaState = SCTV_CORONA_STATE_NULL, T(", GET_CLOUD_TIME_AS_INT(), ")")
			
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV ===  === CAM === MAINTAIN_SCTV_CORONA_TARGET - CLEANUP_SPECIAL_SPECTATOR_CAMERA - X1")
			CLEANUP_SPECIAL_SPECTATOR_CAMERA(specData)					
			SET_SCTV_CORONA_STATE(SCTV_CORONA_STATE_CLEANUP)
		
		#IF IS_DEBUG_BUILD
		ELSE
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_PrintCoronaSpectatorTarget") 
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_CORONA_TARGET - playerSCTVTarget = ", GET_PLAYER_NAME(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget))
			ENDIF
		#ENDIF
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_CORONA_TARGET - Target no longer valid, coronaState = SCTV_CORONA_STATE_NULL, T(", GET_CLOUD_TIME_AS_INT(), ")")
		
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV ===  === CAM === MAINTAIN_SCTV_CORONA_TARGET - CLEANUP_SPECIAL_SPECTATOR_CAMERA - X2")
		CLEANUP_SPECIAL_SPECTATOR_CAMERA(specData)					
		SET_SCTV_CORONA_STATE(SCTV_CORONA_STATE_CLEANUP)
	ENDIF
ENDPROC

/// PURPOSE:
///    Performs processing required for SCTV players spectating a player who is in a corona
PROC MAINTAIN_SCTV_CORONA()

	IF IS_TRANSITION_SESSION_LAUNCHING()
	AND NOT IS_SKYSWOOP_AT_GROUND()
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_CORONA - IS_TRANSITION_SESSION_LAUNCHING, T(", GET_CLOUD_TIME_AS_INT(), ")")
		EXIT
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF GET_SCTV_CORONA_STATE() > SCTV_CORONA_STATE_NULL
	OR IS_PLAYER_IN_CORONA()
		NETWORK_DISABLE_REALTIME_MULTIPLAYER()
		NETWORK_DISABLE_REALTIME_MULTIPLAYER_SPECTATOR()
	ENDIF
	#ENDIF
	
	SWITCH GET_SCTV_CORONA_STATE()
	
		// Wait for the player to be spectating a player in the corona
		CASE SCTV_CORONA_STATE_NULL
			//If we are in follow cam
			IF GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM
			OR SHOULD_TRANSITION_SESSIONS_PUSH_SCTV_TO_CORONA()
			
				//And we have a spectator and we are not alreay in a corona!
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget != INVALID_PLAYER_INDEX()
				AND (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState <= FMMC_LAUNCHER_STATE_IN_CORONA
					OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_RUN_PLAY_LIST)
				AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState >= FMMC_LAUNCHER_STATE_RUNNING	//ADDED FOR BUG 2199345
				AND NOT IS_PLAYLIST_LAUNCHING_CONTROLLER_SCRIPT()
					IF IS_NET_PLAYER_OK(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget, FALSE, FALSE)
						IF GET_PLAYER_CORONA_STATUS(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget) >= CORONA_STATUS_SETUP_VARS
						AND NOT IS_THIS_PLAYER_LEAVING_THE_CORONA(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
						AND GET_PLAYER_CORONA_STATUS(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget) != CORONA_STATUS_WAIT_FOR_ACTIVE
						AND GET_PLAYER_CORONA_STATUS(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget) != CORONA_STATUS_LAUNCH
						AND NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
							IF NOT NETWORK_IS_ACTIVITY_SESSION()
							//OR IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
								PRINTLN("BWW... SET_FM_LAUNCHER_PLAYER_GAME_STATE - FMMC_LAUNCHER_STATE_IN_CORONA - FMMC_LAUNCHER_STATE_IN_CORONA")		
								GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_IN_CORONA
								CLEAR_TRANSITION_SESSIONS_PUSH_SCTV_TO_CORONA()
								CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_CORONA - Target now in corona, coronaState = SCTV_CORONA_STATE_IN_CORONA, T(", GET_CLOUD_TIME_AS_INT(), ")")
								SET_SCTV_CORONA_STATE(SCTV_CORONA_STATE_IN_CORONA)
							ELSE
								IF IS_THIS_PLAYER_ACTIVE_IN_CORONA(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
								//AND NOT IS_THIS_A_ROUNDS_MISSION()
									PRINTLN("BWW... SET_FM_LAUNCHER_PLAYER_GAME_STATE - FMMC_LAUNCHER_STATE_IN_CORONA - FMMC_LAUNCHER_STATE_IN_CORONA")		
									GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_IN_CORONA
									
									CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_CORONA - Target now in launched corona but still active, coronaState = SCTV_CORONA_STATE_IN_CORONA, T(", GET_CLOUD_TIME_AS_INT(), ")")
									SET_SCTV_CORONA_STATE(SCTV_CORONA_STATE_IN_CORONA)
								ELSE
									CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_CORONA - Target now in launched corona, coronaState = SCTV_CORONA_STATE_WAIT_FOR_MISSION, T(", GET_CLOUD_TIME_AS_INT(), ")")
									SET_SCTV_CORONA_STATE(SCTV_CORONA_STATE_WAIT_FOR_MISSION)
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		// Wait until the spectated player has launched the transition session
		CASE SCTV_CORONA_STATE_IN_CORONA
		
			// If we are in a playlist, wait for the player to progress to in corona state
			IF IS_THIS_TRANSITION_SESSION_A_PLAYLIST()
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget != INVALID_PLAYER_INDEX()
					IF IS_THIS_PLAYER_ACTIVE_IN_CORONA_PAST_TRANSITION_SESSION(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
						CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_CORONA - Target now in launched corona, coronaState = SCTV_CORONA_STATE_WAIT_FOR_PLAYLIST, T(", GET_CLOUD_TIME_AS_INT(), ")")
						
						//PRINTLN("BWW... SET_FM_LAUNCHER_PLAYER_GAME_STATE - FMMC_LAUNCHER_STATE_IN_CORONA - FMMC_LAUNCHER_STATE_RUN_PLAY_LIST")		
						//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_RUN_PLAY_LIST
						
						SET_SCTV_CORONA_STATE(SCTV_CORONA_STATE_WAIT_FOR_PLAYLIST)
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_CORONA - SCTV_CORONA_STATE_IN_CORONA = Target player is not valid, T(", GET_CLOUD_TIME_AS_INT(), ")")
				#ENDIF
					
				ENDIF
			ELSE
				IF NETWORK_IS_ACTIVITY_SESSION()
					CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_CORONA - Target now in launched corona, coronaState = SCTV_CORONA_STATE_WAIT_FOR_MISSION, T(", GET_CLOUD_TIME_AS_INT(), ")")
					SET_SCTV_CORONA_STATE(SCTV_CORONA_STATE_WAIT_FOR_MISSION)
				ENDIF
			ENDIF
			
			MAINTAIN_SCTV_CORONA_TARGET()
			
			//IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				// CORONA_TODO: DO CLEANUP / EXIT BEHAVIOUR HERE
			//ENDIF
		
		BREAK
		
		// Listen for us quitting, otherwise we will eventually be active on the mission
		CASE SCTV_CORONA_STATE_WAIT_FOR_MISSION
			
			/*MAY BE NEEDED TO MAKE PLAYERS SEE CORONA AFTER BEING SENT TO THIS STATE DUE TO HEIST PLANNING
			IF IS_THIS_PLAYER_ACTIVE_IN_CORONA(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
			AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)].sClientCoronaData.iBitSet, ciCORONA_BS_PLAYER_IS_ON_LAUNCHED_TRANSITION_JOB_CORONA)
				PRINTLN("BWW... SCTV_CORONA_STATE_WAIT_FOR_MISSION - SET_FM_LAUNCHER_PLAYER_GAME_STATE - FMMC_LAUNCHER_STATE_IN_CORONA - FMMC_LAUNCHER_STATE_IN_CORONA")		
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_IN_CORONA
				
				CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === MAINTAIN_SCTV_CORONA - SCTV_CORONA_STATE_WAIT_FOR_MISSION - Target now in launched corona but still active, coronaState = SCTV_CORONA_STATE_IN_CORONA, T(", GET_CLOUD_TIME_AS_INT(), ")")
				SET_SCTV_CORONA_STATE(SCTV_CORONA_STATE_IN_CORONA)
			ENDIF*/
			
			// Listen for quit
			MAINTAIN_SCTV_CORONA_TARGET()
			
		BREAK
		
		// Wait for the playlist to be processing.
		CASE SCTV_CORONA_STATE_WAIT_FOR_PLAYLIST
		
			// Listen for quit
			MAINTAIN_SCTV_CORONA_TARGET()
		
		BREAK
		
		CASE SCTV_CORONA_STATE_CLEANUP
			
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState != FMMC_LAUNCHER_STATE_IN_CORONA
				SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
				SET_SCTV_CORONA_STATE(SCTV_CORONA_STATE_NULL)
			ENDIF
		BREAK
	
	ENDSWITCH

	// Monitor if we have the corona effects active for entering a Job
	IF GET_SPEC_CORONA_SCREEN_EFFECTS_ACTIVE()
	
		//...yes, check if the screen is no longer frozen
		IF GET_SKYFREEZE_STAGE() != SKYFREEZE_FROZEN
			//...clean up our global so the hud is drawn again.
			SET_SPEC_CORONA_SCREEN_EFFECTS_ACTIVE(FALSE)
		ENDIF
	ENDIF
ENDPROC
		
/// PURPOSE:
///    Draws a small white corona, used to represent other player's UGC content
/// PARAMS:
///    paramCoords - position of the corona
// KGM SCTV 13/2/14: Removing nearest 'UGC coronas' routines - need general 'all coronas' routines for SCTV players to refer to - this will be built into Missions At Coords system
//PROC DRAW_FAKE_TARGET_UGC_BOOKMARK_CORONA(VECTOR paramCoords)
//	
//	// Get the corona details
//	// ...colour
//	INT         theRed            = 0
//	INT         theGreen    = 0
//	INT         theBlue           = 0
//	INT         theAlpha    = 0
//	GET_HUD_COLOUR(FM_MY_UGC_CORONA_COLOUR, theRed, theGreen, theBlue, theAlpha)
//	
//	// ...coords (should parameter should be on the ground, so drop it a few centimetres below ground)
//	paramCoords.z -= MATC_CORONA_BELOW_GROUND_m
//	
//	// ...scale
//	FLOAT       triggerRadius     = ciIN_LOCATE_DISTANCE
//	FLOAT       displayDiameter   = triggerRadius * 2 * MATC_CORONA_DRAW_SIZE_MODIFIER
//	FLOAT       coronaHeight      = MATC_CORONA_INACTIVE_HEIGHT_m
//	VECTOR      theScale          = << displayDiameter, displayDiameter, coronaHeight >>
//	
//	// ...unchanged values
//	VECTOR      ignoreDirection   = << 0.0, 0.0, 0.0 >>
//	VECTOR      ignoreRotation    = << 0.0, 0.0, 0.0 >>
//	
//	// Draw the Corona
//	DRAW_MARKER(MARKER_CYLINDER, paramCoords, ignoreDirection, ignoreRotation, theScale, theRed, theGreen, theBlue, theAlpha)
//	
//ENDPROC

/// PURPOSE:
///    Loops through the spectated player's three nearest local UGC content coronas and draws a fake version
// KGM SCTV 13/2/14: Removing nearest 'UGC coronas' routines - need general 'all coronas' routines for SCTV players to refer to - this will be built into Missions At Coords system
//PROC MAINTAIN_DRAWING_TARGETS_NEAREST_CORONAS()
//	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget != INVALID_PLAYER_INDEX()
//		IF IS_NET_PLAYER_OK(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget, FALSE, FALSE)
//			INT iTargetPlayerID = NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget)
//			IF iTargetPlayerID > -1
//				INT i
//				REPEAT NUMBER_OF_LOCAL_CORONA_VECTORS i
//					IF GlobalplayerBD[iTargetPlayerID].vNearestLocalCorona[i].z > 0.0
//						IF VDIST2(GlobalplayerBD[iTargetPlayerID].vTruePlayerCoords, GlobalplayerBD[iTargetPlayerID].vNearestLocalCorona[i]) < DIST_DRAW_FAKE_UGC_BOOKMARK_CORONAS_SQR
//							DRAW_FAKE_TARGET_UGC_BOOKMARK_CORONA(GlobalplayerBD[iTargetPlayerID].vNearestLocalCorona[i])
//						ENDIF
//					ENDIF
//				ENDREPEAT
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

/// PURPOSE:
///    Performs the SCTV mode transitions and processing, as well as every-frame calls to disable non-required HUD and control features
PROC DO_SCTV_STAGE_RUNNING()
	
	IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		EXIT
	ENDIF
	
	//move in to a corona if I needs be.
	MAINTAIN_SCTV_CORONA()
	
	
	IF IS_EXTERNAL_SCTV_QUIT_HAS_BEEN_CANCELLED()
		SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_CLEAR_DEACTIVATED_DUE_TO_SKYSWOOP_UP_FLAG)
		SET_EXTERNAL_SCTV_QUIT_HAS_BEEN_CANCELLED(FALSE)
	ENDIF
	
	
	IF GET_SCTV_MODE() <> SCTV_MODE_FOLLOW_CAM					//If not on follow targets, dont swap until it's cleaned up
		IF specData.specCamData.eState <> eSPECCAMSTATE_OFF
			CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_READY_FOR_MODE_SWITCH)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_CLEAR_DEACTIVATED_DUE_TO_SKYSWOOP_UP_FLAG)
		IF IS_BIT_SET(specData.specCamData.iBitSet, SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP) sctv flag")
			#ENDIF
			CLEAR_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP)
		ENDIF
		
		IF DID_QUIT_GAME_WITH_SPECTATOR_ACTIVE()
			SET_QUIT_GAME_WITH_SPECTATOR_ACTIVE(FALSE)
		ENDIF
		
		CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_CLEAR_DEACTIVATED_DUE_TO_SKYSWOOP_UP_FLAG)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_CLEAR_DEACTIVATED_DUE_TO_SKYSWOOP_UP_FLAG)")
		#ENDIF
	ENDIF
	
	//Block of disabling stuff
	IF NOT IS_PAUSE_MENU_ACTIVE()
		DISABLE_SPECTATOR_CONTROL_ACTIONS_GENERAL(specData)
	ENDIF
	REMOVE_MULTIPLAYER_HUD_CASH()
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_RANKBAR)
	IF NOT Is_MP_Comms_Disabled()
		Disable_MP_Comms()
	ENDIF
	HUD_FORCE_WEAPON_WHEEL(FALSE)
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())						//Never show
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)				//EVER.
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			ENDIF
		ENDIF
		SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(),TRUE)
		INVALIDATE_IDLE_CAM()
	ENDIF
	HIDE_HELP_TEXT_THIS_FRAME()
	IF IS_ANY_FLOATING_HELP_BEING_DISPLAYED()
		CLEAR_ALL_FLOATING_HELP()
	ENDIF
	SET_MAX_WANTED_LEVEL(0)
	
	IF GET_SCTV_MODE() <> SCTV_MODE_FREE_CAM
		MPSpecGlobals.SCTVData.iFreeCamTransitionCatchCounter = 0
		NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
	ENDIF
	
	MANAGE_JOINING_INSTANCES()
			
	MAINTAIN_SPECTATOR(specData)
		
// KGM SCTV 13/2/14: Removing nearest 'UGC coronas' routines - need general 'all coronas' routines for SCTV players to refer to - this will be built into Missions At Coords system
//	MAINTAIN_DRAWING_TARGETS_NEAREST_CORONAS()
	
	PERFORM_SCTV_MODE()
	
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                      Main Loop                      //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SMP: Starting mission")
		#ENDIF
		
		// Carry out all the initial game starting duties. 
		PROCESS_PRE_GAME()
		
		// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
		#IF IS_DEBUG_BUILD
			Create_Spectator_Widget()
		#ENDIF
	ENDIF
	
	// Main loop
	WHILE TRUE

		MP_LOOP_WAIT_ZERO() 
		
		// If we have a match end event, bail
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			HANDLE_SCTV_TERMINATION()
		ELIF NOT NETWORK_IS_GAME_IN_PROGRESS()
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === NOT NETWORK_IS_GAME_IN_PROGRESS() = TRUE")
			HANDLE_SCTV_TERMINATION()
		ELSE
			
			IF HAS_NETWORK_TIME_STARTED()
				timeNet = GET_NETWORK_TIME()
			ENDIF
			
			MPSpecGlobals.SCTVData.bBailCalledThisFrame = FALSE
			
			// Deal with the debug
			//#IF IS_DEBUG_BUILD
				//UPDATE_WIDGETS()
			//#ENDIF
			
			SWITCH GET_SCTV_STAGE()
			
				CASE SCTV_STAGE_INIT
					DO_SCTV_STAGE_INITIALISE()
				BREAK
				
				CASE SCTV_STAGE_RUNNING
					DO_SCTV_STAGE_RUNNING()
				BREAK
				
				CASE SCTV_STAGE_CLEANUP
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("=== SCTV === SCTV_STAGE_CLEANUP - CLEANUP_SCTV")
					#ENDIF
					CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === SCTV_STAGE_CLEANUP - CLEANUP_SCTV")
					CLEANUP_SCTV()
				BREAK
				
			ENDSWITCH
			
			IF MPSpecGlobals.SCTVData.bBailCalledThisFrame
			AND NOT IS_TRANSITION_SESSION_ACTIONING_FOLLOW_INVITE()
				IF HAS_NET_TIMER_EXPIRED(timeBail, GET_SCTV_BAIL_TIME())
					MPSpecGlobals.SCTVData.bBailTimeIsUp = TRUE
				ELSE
					MPSpecGlobals.SCTVData.bBailTimeIsUp = FALSE
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === will bail in ", GET_SCTV_BAIL_TIME() - (ABSI(GET_TIME_DIFFERENCE(timeNet, timeBail.Timer))))
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("=== SCTV === will bail in ", GET_SCTV_BAIL_TIME() - (ABSI(GET_TIME_DIFFERENCE(timeNet, timeBail.Timer))))
					#ENDIF
				ENDIF
			ELSE
				MPSpecGlobals.SCTVData.bBailTimeIsUp = FALSE
				IF HAS_NET_TIMER_STARTED(timeBail)
					RESET_NET_TIMER(timeBail)
					CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === RESET_NET_TIMER - timeBail ")
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDWHILE
	
ENDSCRIPT
