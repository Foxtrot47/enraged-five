
// ________________________________________
//
//	Game: GTAV - Testbed.sc
//  Script: Testbed.sc	
//  Author: Robert Wright 
//	Description: Testbed mode
// ___________________________________________   

#if IS_DEBUG_BUILD

USING "globals.sch"
USING "net_reset_MP_Globals.sch"
USING "freemode_header.sch"
USING "net_bot_utils.sch"
USING "net_bots.sch"
USING "net_stat_generator.sch"
USING "net_main_client.sch"
USING "net_main_server.sch"
//USING "freemode_stunt_jumps.sch"
USING "net_vote_at_blip.sch"
//USING "net_spectator_hud.sch"
USING "shop_private.sch"
USING "net_freemode_cut.sch"
#IF IS_DEBUG_BUILD
USING "net_debug.sch"
#ENDIF

// -----------------------------------	VARIABLES ---------------------------------------------------------------------

REL_GROUP_HASH rgFM_Default
REL_GROUP_HASH lrgFM_Team[NUM_NETWORK_PLAYERS]

BOOL bSetUpPlayer
//INT iAskForHelpTimer

//BOOL iIsNetPlayerOK

HEIST_ISLAND_TRAVEL_STRUCT sHeistIslandTravel
FM_RANDOM_EVENT_LOCAL_SCRIPT_EVENT sFMREScriptEventdata

// Widgets & Debug.
#IF IS_DEBUG_BUILD
	MODEL_NAMES RandomModel
	//For prints
	TEXT_LABEL_31 tl31MissionName = " TestBed"
//	BOOL bHasPlayerSwapped
	BOOL bSetUpUsjs 
//	WIDGET_GROUP_ID missionTab
	BOOL bHostEndGameNow
//	BOOL bHostDoNotEndGame
//	BOOL bHostAllLeave
	BOOL bSwapSetUp
	//sequence_index script variable
	SEQUENCE_INDEX SequenceIndex

#ENDIF

// -----------------------------------	MAIN PROCS ----------------------------------------------------------------------
//Set Teams to Hate each other
PROC SET_TEAM_RELATIONSHIPS(RELATIONSHIP_TYPE relType, REL_GROUP_HASH relGroup)
	INT i

	REPEAT NUM_NETWORK_PLAYERS i
		IF relGroup <> lrgFM_Team[i]
			SET_RELATIONSHIP_BETWEEN_GROUPS(relType, relGroup, lrgFM_Team[i]) 
		ENDIF
	ENDREPEAT
ENDPROC

// -----------------------------------	DEBUGGERY ----------------------------------------------------------------------
#IF IS_DEBUG_BUILD

	PROC UPDATE_DEBUG			
	
	ENDPROC
	/// PURPOSE:
	///    Temporary hacky way of dealing with launching mission from debug menu. When the proper mission launching system is in place this will be removed.
	PROC HANDLE_LAUNCHING_MISSIONS()
//		STRING scriptName
//		INT stackSize = MINI_STACK_SIZE
//		SWITCH g_iDebugSelectedMission
//			CASE 0
//				// To keep it from spamming when there is no valid mission
//				EXIT
//			BREAK
//			CASE 1
//				scriptName = "FM_YOGA"
//			BREAK
//			CASE 2
//				scriptName = "GOLF_MP_LAUNCHER"
//				stackSize = DEFAULT_STACK_SIZE
//			BREAK
//			CASE 3
//				scriptName = "RANGE_MODERN_MP"
//				stackSize = SCRIPT_XML_STACK_SIZE
//			BREAK
//			CASE 4
//				scriptName = "AM_Armwrestling"
//				stackSize = SCRIPT_XML_STACK_SIZE
//			BREAK 
//			CASE 5
//				scriptName = "FM_Mission_Controller"
//				stackSize = MULTIPLAYER_MISSION_STACK_SIZE
//			BREAK
//			CASE 6
//				scriptName = "TENNIS_NETWORK_MP"
//				stackSize = DEFAULT_STACK_SIZE
//			BREAK 
//			CASE 7
//				scriptName = "MPR_race_editor"
//				stackSize = SCRIPT_XML_STACK_SIZE
//			BREAK
//			CASE 8
//				scriptName = "MP_Races"
//				stackSize = DEBUG_SCRIPT_STACK_SIZE
//			BREAK
//			CASE 10
//				scriptName = "POOL_MP_LAUNCHER"
//				stackSize = SCRIPT_XML_STACK_SIZE
//			BREAK
//			CASE 11
//				scriptName = "AM_Darts"
//				stackSize = SCRIPT_XML_STACK_SIZE
//			BREAK
//			DEFAULT
//				NET_PRINT("Tried to start a mission that doesn't exist")PRINTNL()
//			EXIT 
//			BREAK
//		ENDSWITCH
//		g_iDebugSelectedMission = 0
//		
//		EXIT
//		
//		IF GET_NUMBER_OF_THREADS_RUNNING_THIS_SCRIPT(scriptName) <= 0
//			IF ARE_STRINGS_EQUAL(scriptName, "FM_Mission_Controller")
//				MP_MISSION_DATA fmmcMissionData
//				fmmcMissionData.iInstanceId = -1
//				IF NET_LOAD_AND_LAUNCH_SCRIPT_FM(scriptName,stackSize, fmmcMissionData)
//					g_iDebugSelectedMission = 0
//				ENDIF
//			ELSE
//				IF NET_LOAD_AND_LAUNCH_SCRIPT(scriptName,stackSize)
//					g_iDebugSelectedMission = 0
//				ENDIF
//			ENDIF
//		ELSE
//			//this script is already running reseting the g_iDebugSelectedMission variable
//			g_iDebugSelectedMission = 0
//		ENDIF
	ENDPROC


	PROC UPDATE_WIDGETS()
		//Update Player Damage Modifier
		IF lw_bSetPlayerWeaponDamageModifier = TRUE
			BROADCAST_UPDATE_PLAYER_WEAPON_DAMAGE_MODIFIER(lw_fPlayerWeaponDamageModifier, ALL_PLAYERS())
			lw_bSetPlayerWeaponDamageModifier = FALSE
			NET_PRINT(" - LES WIDGETS - BROADCAST_UPDATE_PLAYER_WEAPON_DAMAGE_MODIFIER - lw_bSetPlayerWeaponDamageModifier = ") NET_PRINT_FLOAT(lw_fPlayerWeaponDamageModifier) NET_NL()
		ENDIF
		
		//Update Ai Damage Modifier
		IF lw_bSetAiDamageModifier = TRUE
			BROADCAST_UPDATE_AI_DAMAGE_MODIFIER(lw_fAiDamageModifier, ALL_PLAYERS())
			lw_bSetAiDamageModifier = FALSE
			NET_PRINT(" - LES WIDGETS - BROADCAST_UPDATE_AI_DAMAGE_MODIFIER - lw_fAiDamageModifier = ") NET_PRINT_FLOAT(lw_fAiDamageModifier) NET_NL()
		ENDIF
		
		IF g_bProcessBots
			//PROCESS_BOTS()
		ENDIF
		
		
		IF GlobalServerBD_FM.bSwapTeams = FALSE
			IF bSwapSetUp = FALSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_Default)
					PRINTSTRING("Set player team: rgFM_Default")PRINTNL()
					bSwapSetUp = TRUE
				ENDIF
			ENDIF
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_T, KEYBOARD_MODIFIER_NONE, "FM Swap Teams")
					 GlobalServerBD_FM.bSwapTeams = FALSE
				ENDIF
			ENDIF
		ELSE
			IF bSwapSetUp = TRUE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), lrgFM_Team[PARTICIPANT_ID_TO_INT()])
					PRINTSTRING("Set player team: lrgFM_Team[PARTICIPANT_ID_TO_INT()]")PRINTNL()
					bSwapSetUp = FALSE
				ENDIF
			ENDIF
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_T, KEYBOARD_MODIFIER_NONE, "FM Swap Teams")
					 GlobalServerBD_FM.bSwapTeams = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		
	ENDPROC

	//On all machines populate the sequence index with 
	//  tasks (this done on all machines so if the ped migrates 
	//  to another machine while performing the sequence it continues to work properly):
	BOOL bOpenTaskSequence = FALSE
	PROC FILL_SEQUENCE_TASK()
		OPEN_SEQUENCE_TASK(SequenceIndex)
			VECTOR playerCoord = GET_PLAYER_COORDS(PLAYER_ID())
	        TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -43.19, -57.01, 1.0>>, PEDMOVE_SPRINT, 999999, 10.0)
	        TASK_PED_SLIDE_TO_COORD(NULL, << -43.19, -57.01, 1.0>>, 0.0)
	        TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, playerCoord, PEDMOVE_SPRINT, 999999, 10.0)
	        TASK_PED_SLIDE_TO_COORD(NULL, playerCoord, 0.0)
		CLOSE_SEQUENCE_TASK(SequenceIndex)
		bOpenTaskSequence = FALSE
	ENDPROC

	// On the machine in control of the ped you want to do the 
	//   sequence, give the ped a task perform sequence and pass 
	//   in the sequence index from above.
	BOOL bStartTaskSequence = FALSE
	PROC DEBUG_TASK_SEQUENCES()
		// Start sequence if we have control of the focus ped.
		PED_INDEX pedIndex =  NETWORK_GET_FOCUS_PED_LOCAL_ID()
		IF pedIndex <> INT_TO_NATIVE(PED_INDEX, -1)
		AND pedIndex <> NULL
			IF NETWORK_HAS_CONTROL_OF_ENTITY(pedIndex)
				TASK_PERFORM_SEQUENCE(pedIndex, SequenceIndex)
			ENDIF
		ENDIF
		bStartTaskSequence = FALSE
	ENDPROC

	//Add widgets for setting relationship groups
	BOOL bSetToOwnTeam
	PROC ADD_RELATIONSHIP_GROUP_WIDGETS()
		START_WIDGET_GROUP("Player Teams")  				
			ADD_WIDGET_BOOL("On Own Team", bSetToOwnTeam)
		STOP_WIDGET_GROUP()		
	ENDPROC

	//Add widgets for temporary testing purposes
	BOOL bTestRappelTask
	BOOL bCreateTestPed
	BOOL bCreateTestVehicle
	BOOL bPlayAnimOnTestPed
	BOOL bTestPedFleeFromPlayer
	BOOL bTestPedFleeFromPoint
	BOOL bTestPedFollowNavMesh
	BOOL bTestPedGoToCoord
	BOOL bTestPedStandStill
	BOOL bTestPedThrowProjectile
	BOOL bTestPedCombatHatedPeds
	BOOL bTestPedGoToCoordAiming
	BOOL bTestPedPerformSequence
	BOOL bTestPedEnterVehicle
	BOOL bTestPedLeaveVehicle
	BOOL bTestPedDriveVehicleToCoord
	BOOL bTestPedDriveVehicleWander
	BOOL bTestPedDriveVehicleMission
	BOOL bClearTestPedTasks
	BOOL bClearTestPedTasksImmediately
	BOOL bDisplayScriptTaskStatus
	BOOL bGivePedWeapon
	BOOL bGivePedGrenade
	PROC ADD_TASK_DEBUG_WIDGETS()
		START_WIDGET_GROUP("Task Debug Widgets")
			ADD_WIDGET_BOOL("Test Rappel task", bTestRappelTask)
			ADD_WIDGET_BOOL("Create Test Ped", bCreateTestPed)
			ADD_WIDGET_BOOL("Create Test Vehicle", bCreateTestVehicle)
			START_WIDGET_GROUP("Basic Tasks")
				ADD_WIDGET_BOOL("Play Anim On Test Ped", bPlayAnimOnTestPed)
				ADD_WIDGET_BOOL("Make Test Ped Flee From Player", bTestPedFleeFromPlayer)
				ADD_WIDGET_BOOL("Make Test Ped Flee From Point", bTestPedFleeFromPoint)
				ADD_WIDGET_BOOL("Make Test Ped Follow Nav Mesh", bTestPedFollowNavMesh)
				ADD_WIDGET_BOOL("Make Test Ped Go To Coord", bTestPedGoToCoord)
				ADD_WIDGET_BOOL("Make Test Ped Stand Still", bTestPedStandStill)
				ADD_WIDGET_BOOL("Make Test Ped Throw Projectile", bTestPedThrowProjectile)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Combat Tasks")
				ADD_WIDGET_BOOL("Make Test Ped Combat Hated Peds", bTestPedCombatHatedPeds)
				ADD_WIDGET_BOOL("Make Test Ped Go To Coord Aiming", bTestPedGoToCoordAiming)
				ADD_WIDGET_BOOL("Make Test Ped Perform Sequence", bTestPedPerformSequence)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Vehicle Tasks")
				ADD_WIDGET_BOOL("Make Test Ped Enter Vehicle", bTestPedEnterVehicle)
				ADD_WIDGET_BOOL("Make Test Ped Leave Vehicle", bTestPedLeaveVehicle)
				ADD_WIDGET_BOOL("Make Test Drive Vehicle To Coord", bTestPedDriveVehicleToCoord)
				ADD_WIDGET_BOOL("Make Test Ped Drive Vehicle Wander", bTestPedDriveVehicleWander)
				ADD_WIDGET_BOOL("Make Test Ped Follow Vehicle Mission", bTestPedDriveVehicleMission)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Inventory")
				ADD_WIDGET_BOOL("Give Test Ped Weapon", bGivePedWeapon)
				ADD_WIDGET_BOOL("Give Test Ped Grenade", bGivePedGrenade)
			STOP_WIDGET_GROUP()
			ADD_WIDGET_BOOL("Clear test ped tasks", bClearTestPedTasks)
			ADD_WIDGET_BOOL("Clear test ped tasks immediately", bClearTestPedTasksImmediately)
			ADD_WIDGET_BOOL("Display Script Task Status", bDisplayScriptTaskStatus)
		STOP_WIDGET_GROUP()	
	ENDPROC

	BOOL bTeamDoToggle
//	//Process team relationships
	PROC PROCESS_RELATIONSHIP_GROUPS()
		IF bSetToOwnTeam=TRUE
			IF bTeamDoToggle = FALSE
				IF GET_PLAYER_TEAM(PLAYER_ID()) <> 1
				AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//					NET_PRINT("PARTICIPANT_ID_TO_INT() ")NET_PRINT_INT(PARTICIPANT_ID_TO_INT())
					SET_PLAYER_TEAM(PLAYER_ID(), 1)
					SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), lrgFM_Team[1])
					NET_PRINT("     ----->  PLAYER SET TO OWN TEAM  <-----  \n")
					bTeamDoToggle = TRUE
				ENDIF
			ENDIF
		ELIF bTeamDoToggle
			IF GET_PLAYER_TEAM(PLAYER_ID()) <> -1
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				bTeamDoToggle = FALSE
				SET_PLAYER_TEAM(PLAYER_ID(), -1)
				SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_Default)
				NET_PRINT("     ----->  PLAYER SET TO DEFAULT TEAM  <-----  \n")
			ENDIF
		ENDIF
	ENDPROC

	//Process temporary test code
	NETWORK_INDEX TestPed
	NETWORK_INDEX TestVehicle
	ROPE_INDEX TestRopeID
	PROC PROCESS_TASK_DEBUG()

		IF bTestRappelTask
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<29.23, 50, 11>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 286)
				TestRopeID = ADD_ROPE(<<30, 50.75, 10.7>>, <<0,90,0>>, 20.0, PHYSICS_ROPE_DEFAULT)
				TASK_RAPPEL_DOWN_WALL(PLAYER_PED_ID(), <<29.23, 50, 12>>, <<30, 50.75, 10.7>>, 2.5, TestRopeID)
				bTestRappelTask = FALSE
			ENDIF
		ENDIF

		IF DOES_ROPE_EXIST(TestRopeID)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SCRIPTTASKSTATUS iStatus = GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_RAPPEL_DOWN_WALL)
				IF iStatus != WAITING_TO_START_TASK AND iStatus != PERFORMING_TASK
					DELETE_ROPE(TestRopeID)
				ENDIF
			ENDIF
		ENDIF

		IF bCreateTestPed
			IF REQUEST_LOAD_MODEL(A_M_Y_METHHEAD_01)
				IF CREATE_NET_PED(TestPed, PEDTYPE_CIVMALE, A_M_Y_METHHEAD_01, <<14,-12,1>>, 180, FALSE)

					bCreateTestPed = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bCreateTestVehicle
			IF REQUEST_LOAD_MODEL(TAILGATER)
				IF CREATE_NET_VEHICLE(TestVehicle, TAILGATER, <<6,-22,1>>, 0, FALSE)
					bCreateTestVehicle = FALSE
				ENDIF
			ENDIF
		ENDIF
//		
//		IF bPlayAnimOnTestPed
//			IF NETWORK_DOES_NETWORK_ID_EXIST(TestPed)
//				REQUEST_ANIM_DICT("mp_arresting")
//				WHILE NOT HAS_ANIM_DICT_LOADED("mp_arresting")
//				   WAIT(0)
//				ENDWHILE
//				IF NOT IS_ENTITY_DEAD(NET_TO_PED(TestPed))
//					TASK_PLAY_ANIM(NET_TO_PED(TestPed), "mp_arresting", "guard_handsup_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
//					bPlayAnimOnTestPed = FALSE
//				ENDIF
//			ENDIF
//		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(TestPed)
			IF NOT IS_ENTITY_DEAD(NET_TO_PED(TestPed))
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_PED(TestPed))
				
					WEAPON_TYPE weaponType 
					
					IF bGivePedWeapon
						weaponType = WEAPONTYPE_PISTOL
					ELSE 
						IF bGivePedGrenade
							weaponType = WEAPONTYPE_GRENADE
						ENDIF
					ENDIF
					
					IF bGivePedWeapon OR bGivePedGrenade
						IF NOT HAS_PED_GOT_WEAPON(NET_TO_PED(TestPed), weaponType)
							GIVE_WEAPON_TO_PED(NET_TO_PED(TestPed), weaponType, -1, FALSE)
						ENDIF
						SET_CURRENT_PED_WEAPON(NET_TO_PED(TestPed), weaponType, TRUE)
					ENDIF
					
				ENDIF
			
				IF bTestPedFleeFromPlayer
					TASK_SMART_FLEE_PED(NET_TO_PED(TestPed), PLAYER_PED_ID(), 100.0, 5000)
					bTestPedFleeFromPlayer = FALSE
				ENDIF
				
				IF bTestPedFleeFromPoint
					TASK_SMART_FLEE_COORD(NET_TO_PED(TestPed), <<14,-12,1>>, 100.0, 5000)
					bTestPedFleeFromPoint = FALSE
				ENDIF
				
				IF bTestPedFollowNavMesh
					TASK_FOLLOW_NAV_MESH_TO_COORD(NET_TO_PED(TestPed), <<14,-12,1>>, PEDMOVE_WALK)
					bTestPedFollowNavMesh = FALSE
				ENDIF
				IF bTestPedGoToCoord
					TASK_GO_STRAIGHT_TO_COORD(NET_TO_PED(TestPed), <<14,-12,1>>, PEDMOVE_WALK)
					bTestPedGoToCoord = FALSE
				ENDIF
				IF bTestPedStandStill
					TASK_STAND_STILL(NET_TO_PED(TestPed), 5000)
					bTestPedStandStill = FALSE
				ENDIF
				IF bTestPedThrowProjectile
					TASK_THROW_PROJECTILE(NET_TO_PED(TestPed), <<13,-20,1>>)
					bTestPedThrowProjectile = FALSE
				ENDIF
				
				IF bTestPedCombatHatedPeds
					SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(TestPed), RELGROUPHASH_HATES_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NET_TO_PED(TestPed), 25.0)
					bTestPedCombatHatedPeds = FALSE
				ENDIF
				IF bTestPedGoToCoordAiming
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NET_TO_PED(TestPed), <<14,-12,1>>, <<14,-12,1>>, PEDMOVE_WALK, FALSE)
					bTestPedGoToCoordAiming = FALSE
				ENDIF
				IF bTestPedPerformSequence
					TASK_PERFORM_SEQUENCE(NET_TO_PED(TestPed), SequenceIndex)
					bTestPedPerformSequence = FALSE
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(TestVehicle)
					IF NOT IS_ENTITY_DEAD(NET_TO_VEH(TestVehicle))
						IF bTestPedEnterVehicle
							TASK_ENTER_VEHICLE(NET_TO_PED(TestPed), NET_TO_VEH(TestVehicle))
							bTestPedEnterVehicle = FALSE
						ENDIF
						IF bTestPedLeaveVehicle
							TASK_LEAVE_ANY_VEHICLE(NET_TO_PED(TestPed))
							bTestPedLeaveVehicle = FALSE
						ENDIF
						IF bTestPedDriveVehicleToCoord
							TASK_VEHICLE_DRIVE_TO_COORD(NET_TO_PED(TestPed),
														NET_TO_VEH(TestVehicle),
														<<6,-22,1>>,
														12.0,
														DRIVINGSTYLE_NORMAL,
														GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(NET_TO_PED(TestPed))),
														DRIVINGMODE_AVOIDCARS,
														1,
														1)
							bTestPedDriveVehicleToCoord = FALSE
						ENDIF
						IF bTestPedDriveVehicleWander
							TASK_VEHICLE_DRIVE_WANDER(NET_TO_PED(TestPed),
													  NET_TO_VEH(TestVehicle),
													  12.0,
													  DRIVINGMODE_AVOIDCARS)
							bTestPedDriveVehicleWander = FALSE
						ENDIF
						IF bTestPedDriveVehicleMission
							TASK_VEHICLE_MISSION_COORS_TARGET(NET_TO_PED(TestPed),
															  NET_TO_VEH(TestVehicle),
															  <<6,-22,1>>,
															  MISSION_GOTO,
															  10.0,
															  DRIVINGMODE_AVOIDCARS,
															  -1,
															  -1)
							bTestPedDriveVehicleMission = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF bClearTestPedTasks
			IF NETWORK_DOES_NETWORK_ID_EXIST(TestPed)
				IF NOT IS_ENTITY_DEAD(NET_TO_PED(TestPed))
					CLEAR_PED_TASKS(NET_TO_PED(TestPed))
					SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(TestPed), RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_Default)
					bClearTestPedTasks = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bClearTestPedTasksImmediately
			IF NETWORK_DOES_NETWORK_ID_EXIST(TestPed)
				IF NOT IS_ENTITY_DEAD(NET_TO_PED(TestPed))
					CLEAR_PED_TASKS_IMMEDIATELY(NET_TO_PED(TestPed))
					SET_PED_RELATIONSHIP_GROUP_HASH(NET_TO_PED(TestPed), RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_Default)
					bClearTestPedTasksImmediately = FALSE
				ENDIF
			ENDIF
		ENDIF

		IF bDisplayScriptTaskStatus
			IF NETWORK_DOES_NETWORK_ID_EXIST(TestPed)
				IF NOT IS_ENTITY_DEAD(NET_TO_PED(TestPed))
					SCRIPTTASKSTATUS iStatus = GET_SCRIPT_TASK_STATUS(NET_TO_PED(TestPed), SCRIPT_TASK_PLAY_ANIM)
					IF iStatus = WAITING_TO_START_TASK
						NET_PRINT("TASK WAITING TO START") NET_NL()
					ELSE 
						IF iStatus = PERFORMING_TASK
							NET_PRINT("TASK RUNNING") NET_NL()
						ELSE
							NET_PRINT("TASK NOT RUNNING") NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDPROC

//	PROC CREATE_TEST_SCRIPT_WIDGETS()
//					
//		INT iPlayer
//		TEXT_LABEL_63 tl63
//			
//		IF NOT DOES_WIDGET_GROUP_EXIST(missionTab)
//			
//			missionTab = START_WIDGET_GROUP(tl31MissionName)    
//			ADD_WIDGET_BOOL( "bHasPlayerSwapped", bHasPlayerSwapped)
//			ADD_WIDGET_BOOL( "bSetUpUsjs", bSetUpUsjs)
//			
//			START_WIDGET_GROUP( "Les Widgets")  
//				ADD_WIDGET_FLOAT_SLIDER("Player Weapon Damage Modifier",  lw_fPlayerWeaponDamageModifier, 0.0, 100.0, 0.1)
//				ADD_WIDGET_BOOL( "Set Player Weapon Damage Modifier", lw_bSetPlayerWeaponDamageModifier)
//			STOP_WIDGET_GROUP()
//		
//			
//			// Gameplay debug, sever only.
//			START_WIDGET_GROUP("Server Only Gameplay") 
//				ADD_WIDGET_BOOL("All: Swap Teams", GlobalServerBD_FM.bSwapTeams)
//				ADD_WIDGET_BOOL("All: End Match", bHostEndGameNow)
//				ADD_WIDGET_BOOL("All: End Session", bHostAllLeave)
//				ADD_WIDGET_BOOL("bHostDoNotEndGame", bHostDoNotEndGame)			
//			STOP_WIDGET_GROUP()	
//			
//			// Data about the server
//			START_WIDGET_GROUP("Server BD") 
//					ADD_WIDGET_INT_SLIDER("S. Game state", GlobalServerBD.iServerGameState,-1, HIGHEST_INT,1)
//			STOP_WIDGET_GROUP()	
//						
//			// Data about the clients. * = You.
//			START_WIDGET_GROUP("Client BD")  				
//				REPEAT NUM_NETWORK_PLAYERS iPlayer
//					tl63 = "Player "
//					tl63 += iPlayer
//					IF iPlayer = PARTICIPANT_ID_TO_INT()
//						tl63 += "*"
//					ENDIF
//					START_WIDGET_GROUP(tl63)
//						ADD_WIDGET_INT_SLIDER("Game state", GlobalplayerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
//												
//						START_WIDGET_GROUP("Weapon swapping")
//							ADD_WIDGET_INT_READ_ONLY("State", GlobalplayerBD[iPlayer].weaponSwappingData.iState)
//							ADD_WIDGET_INT_READ_ONLY("Part interacting with", GlobalplayerBD[iPlayer].weaponSwappingData.iPlayerInteractingWith)
//							ADD_WIDGET_INT_READ_ONLY("Ammo", GlobalplayerBD[iPlayer].weaponSwappingData.iAmmo)
//						STOP_WIDGET_GROUP()
//					STOP_WIDGET_GROUP()
//				ENDREPEAT
//				
//			STOP_WIDGET_GROUP()
//							
//			START_WIDGET_GROUP("Debug Task Sequence")
//				ADD_WIDGET_BOOL("Open", bOpenTaskSequence)
//				ADD_WIDGET_BOOL("Start", bStartTaskSequence)
//			STOP_WIDGET_GROUP()
//			
//			ADD_RELATIONSHIP_GROUP_WIDGETS()
//			
//			CREATE_BOT_WIDGET(TRUE)
//			
//			ADD_TASK_DEBUG_WIDGETS()
//			
//			Create_Main_Generic_Client_Widgets()
//			Create_Main_Generic_Server_Widgets()
//			
//			STOP_WIDGET_GROUP()
//			
//			SET_CURRENT_WIDGET_GROUP(missionTab)
//				ADD_SPAWNING_WIDGETS()
//			CLEAR_CURRENT_WIDGET_GROUP(missionTab)
//		ENDIF
//	ENDPROC		
#ENDIF

// -----------------------------------	STAGE FUNCTIONS
PROC SET_CLIENT_GAME_STATE(INT iStage)
	SWITCH iStage 
		CASE MAIN_GAME_STATE_INI 
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_INI  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_WAIT	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_WAIT  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_INITIAL_CAMERA_WORK 	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_INITIAL_CAMERA_WORK  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_FIRST_SPAWN 			
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_FIRST_SPAWN  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_RUNNING	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_RUNNING  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_END	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_END  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_ALL_LEAVE	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_ALL_LEAVE  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_SWAP_TEAM	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_SWAP_TEAM  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_CHOOSE_ALTERNATIVE_TEAM 
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_CHOOSE_ALTERNATIVE_TEAM  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_RUN_LOBBY					
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_RUN_LOBBY  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_HOLD_FOR_TRANSITION					
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_HOLD_FOR_TRANSITION  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_RUN_TRANSITION					
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> MAIN_GAME_STATE_RUN_TRANSITION  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("SET_CLIENT_GAME_STATE Deathmath.sc invalid option")
			#ENDIF
		BREAK
	ENDSWITCH
			
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iGameState = iStage
ENDPROC

PROC SET_SERVER_GAME_STATE(INT iStage)

	#IF IS_DEBUG_BUILD
		NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SET_SERVER_GAME_STATE Called on client by mistake")
	#ENDIF

	SWITCH iStage
		CASE MAIN_GAME_STATE_INI 
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_INI  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_WAIT	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_WAIT  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_INITIAL_CAMERA_WORK 	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_INITIAL_CAMERA_WORK  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_FIRST_SPAWN 			
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_FIRST_SPAWN  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_RUNNING	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_RUNNING  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_END	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_END  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_ALL_LEAVE	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_ALL_LEAVE  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_SWAP_TEAM	
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_SWAP_TEAM  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_CHOOSE_ALTERNATIVE_TEAM 
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_CHOOSE_ALTERNATIVE_TEAM  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		CASE MAIN_GAME_STATE_RUN_LOBBY					
			#IF IS_DEBUG_BUILD
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> MAIN_GAME_STATE_RUN_LOBBY  ", tl31MissionName) NET_NL()
			#ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("SET_SERVER_GAME_STATE Deathmath.sc invalid option")
			#ENDIF
		BREAK
	ENDSWITCH
			
	GlobalServerBD.iServerGameState = iStage
ENDPROC

//PROC RESET_FREEMODE_MISSION_PLAY_REQUEST_ARRAY()
//	INT i
//	FOR i = 0 TO (MAX_PENDING_PLAY_REQUESTS -1)
//		g_sFM_play_request_detail[i].iMissionType			= -1
//		g_sFM_play_request_detail[i].iCreatorID				= -1
//		g_sFM_play_request_detail[i].iVariation				= -1
//		g_sFM_play_request_detail[i].vLocation				= <<0.0, 0.0, 0.0>>
//		g_sFM_play_request_detail[i].piFromPlayer			= INT_TO_NATIVE(PLAYER_INDEX, -1)
//		g_sFM_play_request_detail[i].iBitSet				= 0
//	ENDFOR
//	g_iNumberStoredMessageRequests = 0
//ENDPROC


FUNC BOOL PROCESS_PRE_GAME()
	INT i	
	
	NET_NL()NET_PRINT("PROCESS PREGAME - FREEMODE ")
	// For this game, we can have 32 players. Other scripts may only have 8,12,16,24 ect players.
	//RESET_FREEMODE_MISSION_PLAY_REQUEST_ARRAY()
	
	// This makes sure the net script is active, waits untull it is.
	HANDLE_NET_SCRIPT_INITIALISATION(FALSE, ENUM_TO_INT(GAMEMODE_FM))	

	RESERVE_NETWORK_MISSION_PEDS(1)
	RESERVE_NETWORK_MISSION_VEHICLES(1)
	
	// register the common broadcast data 
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD, SIZE_OF(GlobalServerBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD, SIZE_OF(GlobalplayerBD))

	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_FM, SIZE_OF(GlobalServerBD_FM))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_FM, SIZE_OF(GlobalplayerBD_FM))
	
	// For the Missions Shared arrays
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_MissionsShared, SIZE_OF(GlobalServerBD_MissionsShared))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()	//NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)
	
	//Clean up every thing before the initilisation
	RESET_GlobalplayerBD_FM_2()
	//Register the Vote Boradcast data
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(GlobalplayerBD_FM_2, SIZE_OF(GlobalplayerBD_FM_2))
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	SET_INSTANCE_PRIORITY_MODE(INSTANCE_MODE_MULTIPLAYER)
	
	//Clear the time
	SET_TIME_OF_DAY(TIME_OFF, TRUE)
	INIT_STATS()
		
	SET_MAX_WANTED_LEVEL(6)
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	SET_CREATE_RANDOM_COPS(TRUE)
	SET_DITCH_POLICE_MODELS(FALSE)
	
	// switch on emergency services in freemode
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, TRUE)

	// In FM, make sure each player has their own colour, and not the common 
	// team colour.
	USE_PLAYER_COLOUR_INSTEAD_OF_TEAM_COLOUR(TRUE)
	
	// Setup rel groups
	ADD_RELATIONSHIP_GROUP("rgFM_Default", rgFM_Default)

	REPEAT NUM_NETWORK_PLAYERS i
		TEXT_LABEL_63 tlRelGroup
		tlRelGroup = "lrgFM_Team"
		tlRelGroup += i
		ADD_RELATIONSHIP_GROUP(tlRelGroup, lrgFM_Team[i])
	ENDREPEAT
	
	SET_TEAM_RELATIONSHIPS(ACQUAINTANCE_TYPE_PED_HATE, rgFM_Default)
	REPEAT NUM_NETWORK_PLAYERS i
		SET_TEAM_RELATIONSHIPS(ACQUAINTANCE_TYPE_PED_HATE, lrgFM_Team[i])
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, lrgFM_Team[i], rgFM_Default) 
	ENDREPEAT
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_Default) //lrgFM_Team[PARTICIPANT_ID_TO_INT()])	//Put all players on default team to start with rgFM_Default#
		//SET_CAN_ATTACK_FRIENDLY(PLAYER_PED_ID(), TRUE, TRUE) 
		PRINTLN("SET_CAN_ATTACK_FRIENDLY")
		//SET_PED_CAN_BE_TARGETTED_BY_TEAM(PLAYER_PED_ID(), -1, TRUE)
	ENDIF
	
	UPDATE_ACTIVE_PLAYERS_IN_MP_LAUNCH_SCRIPT()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		//NETWORK_SET_WEATHER("SMOG", 0, TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT_STRING_INT("NATIVE_TO_INT(PLAYER_ID()) = ", NATIVE_TO_INT(PLAYER_ID())) NET_NL()
		IF NOT CREATE_NETWORK_BOTS_RELATIONSHIP_GROUPS()
			SCRIPT_ASSERT("NOT CREATE_NETWORK_BOTS_RELATIONSHIP_GROUPS")
		ENDIF
	#ENDIF
	
	// Reset kill and death counters when player joins (CS)
	INITIALISE_KILL_AND_DEATH_COUNTERS()
	
	// Speirs added 07/07/11 to fix 153666
    // Set frontend player blip name
    BLIP_INDEX playerBlip = GET_MAIN_PLAYER_BLIP_ID()
    IF DOES_BLIP_EXIST(playerBlip)
    	SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_PLAYER")
    ENDIF
	
	//Set it so people can attack me.
	SET_EVERYONE_CAN_KILL_LOCAL_PLAYER(TRUE)
	PRINTLN("Calling SET_EVERYONE_CAN_KILL_LOCAL_PLAYER")
	
	SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_INI)
	INIT_STAT_TIMERS()
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_CLEAN_UP_WHEN_NOT_ON_A_MISSION()
	IF g_bFreemodeCleanUpAfterMission = TRUE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_Default) //lrgFM_Team[PARTICIPANT_ID_TO_INT()])	//Put all players on default team to start with rgFM_Default#
			//SET_CAN_ATTACK_FRIENDLY(PLAYER_PED_ID(), TRUE, TRUE) 
			PRINTLN("SET_CAN_ATTACK_FRIENDLY Called")
			//SET_PED_CAN_BE_TARGETTED_BY_TEAM(PLAYER_PED_ID(), -1, TRUE)
			
			PRINTLN("MAINTAIN_CLEAN_UP_WHEN_NOT_ON_A_MISSION() Called")
			g_bFreemodeCleanUpAfterMission = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FREEMODE_DPAD_DOWN()
	IF MAINTAIN_DPADDOWN_CHECK()
		IF GET_DPADDOWN_ACTIVATION_STATE() != DPADDOWN_NONE 
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(MPGlobalsHud.iOnMissionOverlayTimer, DPAD_DOWN_DISPLAY_TIME)
				SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_NONE)
				DPADDOWN_ONE_TIME_ACTIVE(FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC





////PURPOSE: Does a full 32 player repeat 
//PROC MAINTAIN_32_PLYR_REPEAT()
//	INT iParticipant
//	PLAYER_INDEX PlayerId
//	INT iCount
//	
//	#IF IS_DEBUG_BUILD
//	ADD_SCRIPT_PROFILE_PRE_LOOP_MARKER("loop")
//	#ENDIF	
//		
//	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iCount		
//	
//		#IF IS_DEBUG_BUILD
//			ADD_SCRIPT_PROFILE_MARKER("START OF REPEAT", 0)
//		#ENDIF	
//	
//		// if we are simulating max number of players then set the player id here
//		iParticipant = iCount
//		PlayerID = INVALID_PLAYER_INDEX()
//		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
//			PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
//
//			MPGlobals.vWarpRequest[iCount] = <<0.0, 0.0, 0.0>> // reset these each frame
//			
//			IF IS_NET_PLAYER_OK(PlayerId, FALSE, FALSE)
//				
//				#IF IS_DEBUG_BUILD
//				ADD_SCRIPT_PROFILE_MARKER("IS_NET_PLAYER_OK", 1)
//				#ENDIF
//
//	//			INT iPlayersGlobalInt = NATIVE_TO_INT(PlayerId)
//	//			INT iLocalPlayersIdtoGlobalInt = NETWORK_PLAYER_ID_TO_INT()
//				
//				// --------------------------------------------------------------------------------//
//				// Blips
//				// --------------------------------------------------------------------------------//
//
//				// Do blips for players.
//				HANDLE_FM_PLAYER_BLIPS(iParticipant)
//				#IF IS_DEBUG_BUILD
//				ADD_SCRIPT_PROFILE_MARKER("HANDLE_PLAYER_BLIPS", 7)
//				#ENDIF				
//
//				// -----------------------------------------------------------------------------------------------//
//				// These procs and funcs were previously repeats that we had to move in here. 
//				// They set a temp flag that in turn sets a global flag for a specific command to check against.
//				// -----------------------------------------------------------------------------------------------//
//				IF PLAYER_ID() <> PlayerId 
//								
//					// Check if I can offer this player my weapon
//					CHECK_PLAYER_WEAPON_OFFERING(iParticipant, PlayerId)			
//					#IF IS_DEBUG_BUILD
//					ADD_SCRIPT_PROFILE_MARKER("weapon swap", 17)
//					#ENDIF	
//				ENDIF
//				
//				//DISPLAY_WEAPON_SWAP_HUD(iParticipant)
//			ENDIF
//		ENDIF
//		
//	ENDREPEAT
//	
//	#IF IS_DEBUG_BUILD
//	ADD_SCRIPT_PROFILE_POST_LOOP_MARKER()
//	#ENDIF
//			
//ENDPROC

PROC SETUP_FM_USJS()
	DISABLE_STUNT_JUMP_SET(ciSINGLE_PLAYER_SJS_GROUP)
	PRINTLN("MPUSJ - DISABLE_STUNT_JUMP_SET(ciSINGLE_PLAYER_SJS_GROUP)")
	ENABLE_STUNT_JUMP_SET(ciFREEMODE_PLAYER_SJS_GROUP)
	PRINTLN("MPUSJ - ENABLE_STUNT_JUMP_SET(ciFREEMODE_PLAYER_SJS_GROUP)")
ENDPROC

PROC ACTIVATE_SHOPS_IN_FM()
	SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_L_01_SC, TRUE)
	SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_M_01_SM, TRUE)
	SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_H_01_BH, TRUE)
	SET_SHOP_IS_AVAILABLE(HAIRDO_SHOP_01_BH, 	TRUE)
	SET_SHOP_IS_AVAILABLE(HAIRDO_SHOP_02_SC, 	TRUE)
	SET_SHOP_IS_AVAILABLE(TATTOO_PARLOUR_01_HW, TRUE)
	SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_01_AP, 	TRUE)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_01_DT, 	TRUE)
	SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_L_01_SC,	FALSE) 
	SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_M_01_SM,	FALSE) 
	SET_SHOP_LOCATES_ARE_BLOCKED(CLOTHES_SHOP_H_01_BH,	FALSE) 
	SET_SHOP_LOCATES_ARE_BLOCKED(HAIRDO_SHOP_01_BH, 	FALSE) 
	SET_SHOP_LOCATES_ARE_BLOCKED(HAIRDO_SHOP_02_SC,		FALSE) 
	SET_SHOP_LOCATES_ARE_BLOCKED(TATTOO_PARLOUR_01_HW,	FALSE) 
	SET_SHOP_LOCATES_ARE_BLOCKED(CARMOD_SHOP_01_AP,		FALSE) 
	SET_SHOP_LOCATES_ARE_BLOCKED(GUN_SHOP_01_DT,		FALSE) 
ENDPROC


PROC SCRIPT_CLEANUP()	
	
	// Cleanup Widgets
	#IF IS_DEBUG_BUILD
		// Keith 14/12/11: Terminate The Generic Client and Server Routines			
		Destroy_Main_Generic_Client_Widgets()
		Destroy_Main_Generic_Server_Widgets()

		g_MainMissionScriptID = INT_TO_NATIVE(THREADID, -1)
		g_bMainMissionScriptActive = FALSE
		CLEANUP_BOTS()
		
		NET_PRINT("...KGM MP: Cleaning Up MPTESTBED") NET_NL()
	#ENDIF
	
	Cleanup_Main_Generic_Client()
	
	// Clean up police scanner radio. Allow ambient reports again.
	SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", FALSE)
	
	CLEANUP_FM_BLIPS()
	
	CLEAR_HELP()
	CLEAR_PRINTS()


//	TERMINATE_THIS_MULTIPLAYER_THREAD()
	Cleanup_Main_Generic_Client() // call this just before the terminate (as it wipes the globals)
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC INT GET_NUM_FREEMODE_PLAYERS()
	INT iParticipant
	INT iPlayerCount
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))

			iPlayerCount++
		ENDIF
	ENDREPEAT
	
	RETURN iPlayerCount
ENDFUNC

// Repeat through all clients
PROC MAINTAIN_FREEMODE_PLAYER_LOOP()

	INT iParticipant
	PLAYER_INDEX PlayerId
	INT iCount	
	
	BOOL bIsDpaddownActive = (GET_DPADDOWN_ACTIVATION_STATE() != DPADDOWN_NONE)
		
		
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iCount	
	
		
		iParticipant = iCount
		
		// Gamertags above heads probably temp solution (Speirs 357426 17/02/2012)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			
			
			IF IS_NET_PLAYER_OK(PlayerId)

				IF PlayerId != PLAYER_ID()
					//IF GET_PLAYER_TEAM(PlayerId) >= 0
						IF GET_PLAYER_TEAM(PlayerId) != GET_PLAYER_TEAM(PLAYER_ID())
							NETWORK_SET_ANTAGONISTIC_TO_PLAYER(TRUE, PlayerId)
						ENDIF
					//ENDIF
				ENDIF
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					RUN_OVERALL_FREEMODE_LOGIC_CHECKS(PlayerId, iParticipant, bIsDpaddownActive )
					PROCESS_PLAYER_NAME_DISPLAY_FM_DM_RC(PlayerId, iParticipant)
				ELSE
					HIDE_ALL_ABOVE_HEAD_DISPLAY_FOR_PLAYER_NOW(PlayerId, iParticipant)
				ENDIF
			ENDIF
		ELSE
			CLEANUP_ABOVE_HEAD_DISPLAY(iParticipant,PlayerId )
		ENDIF
	ENDREPEAT
	
	NETWORK_SET_ANTAGONISTIC_TO_PLAYER(TRUE, INVALID_PLAYER_INDEX())
	
ENDPROC


/// PURPOSE:
///    Routine to work through when trying to initially spawn the player
/// RETURNS:
///    True is spawn has happened
FUNC BOOL SPAWN_PLAYER_AT_TESTBED()
	VECTOR vSpawnPosition = <<0.0, 0.0, 0.0>>
	FLOAT fSpawnHeading = 0.0
	
	NET_PRINT("SPAWN_PLAYER_AT_TESTBED() called...") NET_NL()
	
	IF GET_SERVER_SCRIPT_GAME_STATE() > MAIN_GAME_STATE_INI			
		// Get a spawn position, then try to spawn the player		
		
		REQUEST_MODEL(MP_M_FREEMODE_01)
		IF HAS_MODEL_LOADED(MP_M_FREEMODE_01)
			IF HAS_GOT_SPAWN_LOCATION(vSpawnPosition, fSpawnHeading, SPAWN_LOCATION_NET_TEST_BED, FALSE)
					// make sure there are no blips left from pevious lives
				CLEANUP_ALL_PLAYER_BLIPS()
				SET_NET_PLAYER_MODEL(MP_M_FREEMODE_01)	
				REQUEST_MODEL(MP_M_FREEMODE_01)
				NET_SPAWN_PLAYER(vSpawnPosition , fSpawnHeading, SPAWN_REASON_MANUAL, SPAWN_LOCATION_AT_SPECIFIC_COORDS, FALSE, TRUE)	
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

				IF NOT (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN())
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF

				RETURN(TRUE)
			ENDIF	
		ENDIF
	ENDIF		


	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
USING "player_ped_public.sch"
FUNC BOOL HAS_PLAYER_PED_MODEL_SWAPPED()
	MODEL_NAMES mnTemp
	WHILE NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
		PRINTLN("HAS_PLAYER_PED_MODEL_SWAPPED- IS_NET_PLAYER_OK")
		WAIT(0)
	ENDWHILE 
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		mnTemp = GET_ENTITY_MODEL(PLAYER_PED_ID())
	ENDIF
	
	IF mnTemp = GET_PLAYER_PED_MODEL(CHAR_TREVOR)
		RETURN FALSE
	ENDIF
	IF mnTemp = GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
		RETURN FALSE
	ENDIF	
	IF mnTemp = GET_PLAYER_PED_MODEL(CHAR_FRANKLIN)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC 

FUNC BOOL CORRECT_PLAYER_PED()//VECTOR &vSpawnCoords, FLOAT &fSpawnHeading)
	IF RandomModel= DUMMY_MODEL_FOR_SCRIPT
		IF (GET_RANDOM_INT_IN_RANGE(0,2) = 0)
			RandomModel = GET_RANDOM_PED_MODEL(TRUE)
		ELSE
			RandomModel = GET_RANDOM_PED_MODEL(FALSE)
		ENDIF
	ENDIF
	PRINTSTRING("CORRECT_PLAYER_PED called")
	IF LOAD_AND_SET_PLAYER_MODEL(RandomModel)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			//SET_CAN_ATTACK_FRIENDLY(PLAYER_PED_ID(), TRUE, TRUE) 
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 32)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 128)	
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC 


#ENDIF

#ENDIF

// -----------------------------------	MAIN LOOP ----------------------------------------------------------------------
SCRIPT
	
#IF IS_DEBUG_BUILD
	#IF IS_DEBUG_BUILD
		NET_PRINT("\n\n >>>> MPTestBed: Script Launched..... \n\n  ")
	#ENDIF
	
	PRINTLN("LOAD_ALL_UGC_CONTENT_FOR_GTA_V_ONLINE - REQUEST_SCRIPT(\"UGC_Global_Registration\")")
	REQUEST_SCRIPT("UGC_Global_Registration")
	REQUEST_SCRIPT("UGC_Global_Registration_2")
	WHILE NOT HAS_SCRIPT_LOADED("UGC_Global_Registration")
	AND NOT HAS_SCRIPT_LOADED("UGC_Global_Registration_2")
		WAIT(0)
		REQUEST_SCRIPT("UGC_Global_Registration")
		REQUEST_SCRIPT("UGC_Global_Registration_2")
	ENDWHILE
	START_NEW_SCRIPT("UGC_Global_Registration", MICRO_STACK_SIZE)
	START_NEW_SCRIPT("UGC_Global_Registration_2", MICRO_STACK_SIZE)
	
	PRINTLN("Give UGC_Global_Registration.sc one frame to run before requesting and launching mpTestBed.")
	
	WAIT(0)
	
	CLEAR_ALL_MP_GLOBALS() // should be called first.

	SET_CURRENT_GAMEMODE(GAMEMODE_MPTESTBED)


//	VECTOR vSpawnCoords
//	FLOAT fSpawnHeading
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)

	
	SHUTDOWN_LOADING_SCREEN()
		
	Initialise_Main_Generic_Server()
	
	// Carry out all the initial game starting duties. 
	IF NOT PROCESS_PRE_GAME()
		NET_PRINT("\n\n >>>> MPTestbed: Failed to receive initial broadcast data. Cleaning up..... \n\n  ")
		SCRIPT_CLEANUP()
	ENDIF
	
	//KM: 
	Initialise_Main_Generic_Client()
	
	// initialising stat timers
	INIT_STAT_TIMERS()
	
	//Turn off/On StuntJumps
	SETUP_FM_USJS()
	
	ACTIVATE_SHOPS_IN_FM()
	
	// Turn off ambient cop dispatch (turned back on in script cleanup)
	SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE)

	#IF IS_DEBUG_BUILD
		g_MainMissionScriptID = GET_ID_OF_THIS_THREAD()
		SET_CURRENT_GAMEMODE(GAMEMODE_FM)
		NET_PRINT("\n\n >>>> Testbed: Started..... \n\n  ")
	#ENDIF
	
	
	// Setup widgets						 
	#IF IS_DEBUG_BUILD
		g_bMainMissionScriptActive = TRUE
//		CREATE_TEST_SCRIPT_WIDGETS()
		//IF NOT HAS_PLAYER_PED_MODEL_SWAPPED()
//		IF NETWORK_IS_IN_CODE_DEVELOPMENT_MODE()
//			bHasPlayerSwapped = FALSE
			PRINTSTRING(" HAS_PLAYER_PED_MODEL_SWAPPED bHasPlayerSwapped = FALSE")
//		ELSE			
//			PRINTSTRING(" HAS_PLAYER_PED_MODEL_SWAPPED bHasPlayerSwapped = TRUE")
//			bHasPlayerSwapped = TRUE
//		ENDIF
	#ENDIF
	

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	// Main loop.
	WHILE TRUE

		// One wait to rule them all. This can be the ONLY wait from here on in.....
		WAIT(0) 
		
		// Speirs added 08/06/12 (428425)
		IF NOT IS_LOCAL_PLAYER_ON_ANY_FM_MISSION()
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//SET_CAN_ATTACK_FRIENDLY(PLAYER_PED_ID(), TRUE) 
			ENDIF
		ENDIF

		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			SCRIPT_CLEANUP()
		ENDIF
		
		g_iMPTimer = GET_NETWORK_TIME()
		
		Maintain_Main_Generic_Client_Start_Of_Frame_Every_Frame(sHeistIslandTravel, sFMREScriptEventdata)
		
		//Updates list of active player (has to be done after events as that updates list when someone joins or leaves)
		SET_PLAYER_COUNTS()	
		
		
		// Deal with the debug.
		#IF IS_DEBUG_BUILD		
			PROCESS_RELATIONSHIP_GROUPS()
			PROCESS_TASK_DEBUG()
		
			UPDATE_WIDGETS()

			IF bOpenTaskSequence
				FILL_SEQUENCE_TASK()
				bOpenTaskSequence = FALSE
			ENDIF
			
			IF bStartTaskSequence
				DEBUG_TASK_SEQUENCES()
				bStartTaskSequence = FALSE
			ENDIF
		#ENDIF			

// -----------------------------------	CLIENT
		SWITCH GET_CLIENT_SCRIPT_GAME_STATE(PLAYER_ID())

			// Wait until the server gives the all go before moving on.
			CASE MAIN_GAME_STATE_INI
				NET_NL()NET_PRINT("MAIN_GAME_STATE_INI")
				IF GET_SERVER_SCRIPT_GAME_STATE() > MAIN_GAME_STATE_INI	
					/* BC: 08/05/2012 - Changed as I do all of this in the transition. 
					#IF IS_DEBUG_BUILD
					IF g_UseNewTransitionSystem = FALSE
					#ENDIF
					IF LOAD_AND_SET_PLAYER_MODEL(RandomModel)
						IF HAS_GOT_SPAWN_LOCATION(vSpawnCoords, fSpawnHeading, SPAWN_LOCATION_AUTOMATIC, FALSE)
							//REQUEST_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID()))
							//IF HAS_MODEL_LOADED(GET_ENTITY_MODEL(PLAYER_PED_ID()))
//							NET_SPAWN_PLAYER(vSpawnCoords, fSpawnHeading)
							ENABLE_STUNT_JUMP_SET(ciFREEMODE_PLAYER_SJS_GROUP)	
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_CAN_ATTACK_FRIENDLY(PLAYER_PED_ID(), TRUE, TRUE) 
								GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 32)
								GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 128)	
							ENDIF
								
							SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_INITIAL_CAMERA_WORK)

							//g_TransitionData.iTransitionStatsState = TRANSITION_STATS_STATE_SHUTDOWN
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
					*/
					
					
					IF SPAWN_PLAYER_AT_TESTBED()
					
						SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
						SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUN_TRANSITION)
						
					ENDIF
					
				ENDIF				
			BREAK
			
			CASE MAIN_GAME_STATE_RUN_TRANSITION
				NET_NL()NET_PRINT("MAIN_GAME_STATE_RUN_TRANSITION")
				TRIGGER_TRANSITION_MENU_ACTIVE(FALSE)  //Needs to be here so when DM_Deathmatch stops running, this can loop round and not instantly bring up the hud again.													
				SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_HOLD_FOR_TRANSITION)	
			BREAK
			
			CASE MAIN_GAME_STATE_HOLD_FOR_TRANSITION
				NET_NL()NET_PRINT("MAIN_GAME_STATE_HOLD_FOR_TRANSITION")
//				#IF IS_DEBUG_BUILD
//					IF NOT bHasPlayerSwapped	
//						SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)
//					ENDIF
//					IF NETWORK_IS_IN_CODE_DEVELOPMENT_MODE()

					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)		
						SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
						
					ENDIF		

					SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)
//					ENDIF
//				#ENDIF
			BREAK
			
			CASE MAIN_GAME_STATE_INITIAL_CAMERA_WORK
				//HIDE_HUD_AND_RADAR_THIS_FRAME()
				IF bSetUpPlayer = FALSE
					//IF IS_GANG_INTERIOR_LOADED(GET_PLAYER_TEAM(PLAYER_ID()))
					//	IF SPAWN_PLAYER_AT_GANG_HOUSE()
							bSetUpPlayer = TRUE
							// In freemode, it's everyone against everyone
							SET_PLAYER_TEAM(PLAYER_ID(), -1)
							PRINTSTRING(" Testbed - bSetUpPlayer - DONE")
					//	ENDIF
				//	ENDIF
				ELSE
				//IF NET_WARP_TO_COORD(vSpawnCoords, fSpawnHeading, FALSE, FALSE)
					IF SET_SKYSWOOP_DOWN()
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
							SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
						ENDIF
						SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_RUNNING)
						PRINTSTRING(" TESTBED - MAIN_GAME_STATE_RUNNING - TESTBED INTRO CUTSCENE ALREADY DONE")
					ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE MAIN_GAME_STATE_RUNNING		
				
				/*IF NOT HAS_FREEMODE_INTRO_BEEN_DONE()
					IF RUN_FREEMODE_INTRO_CUTSCENE()
						PRINTSTRING(" FREEMODE - MAIN_GAME_STATE_RUNNING - RUN_FREEMODE_INTRO_CUTSCENE  - DONE - B")
					ENDIF
				ENDIF*/
								
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)			
					ENDIF		
				
				//If we are in game develoipment the force the player ped
//				#IF IS_DEBUG_BUILD
//				IF NOT bHasPlayerSwapped
//					IF CORRECT_PLAYER_PED()
//						bHasPlayerSwapped = TRUE
//					ENDIF
//				ENDIF
//				//Run the debug health bars
//				RUN_ALL_OVERHEAD_HEALTHBARS()
//				#ENDIF
				//DEAL_WITH_TRIGGER_TICKER()
				
				#IF IS_DEBUG_BUILD
				Maintain_Main_Generic_Client_TESTBED()
				#ENDIF
				
				UPDATE_PLAYER_RESPAWN_STATE()
		
		
				MAINTAIN_FREEMODE_PLAYER_LOOP()
				
				MAINTAIN_CLEAN_UP_WHEN_NOT_ON_A_MISSION()
				
				MAINTAIN_FREEMODE_DPAD_DOWN()
				
				RUN_RADAR_MAP()
				
				
				
				
				// Process swapping weapons with other players
				/*MAINTAIN_PLAYER_WEAPON_SWAPPING(iIsNetPlayerOK)
				#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYER_WEAPON_SWAPPING")
					#ENDIF
					//TEST FOR LAUNCHING A MISSION FROM DEBUG MENU NOT FINAL
					HANDLE_LAUNCHING_MISSIONS()
				#ENDIF*/ 	 					

				// Look for the server say the game has ended.
				IF GET_SERVER_SCRIPT_GAME_STATE() = MAIN_GAME_STATE_END
					SET_CLIENT_GAME_STATE(MAIN_GAME_STATE_END)			
				ENDIF	
			BREAK	

			CASE MAIN_GAME_STATE_END				
				IF NETWORK_IS_IN_SESSION()
				AND NETWORK_CAN_SESSION_END()
					NETWORK_SESSION_END()	
				ENDIF
			BREAK
			
			DEFAULT
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("Testbed - invalid game state!!")
				#ENDIF
			BREAK
		ENDSWITCH
		
// -----------------------------------	SERVER		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()

			
			SWITCH GET_SERVER_SCRIPT_GAME_STATE()
				
				// For now just move straight into playing.
				CASE MAIN_GAME_STATE_INI							
					SET_SERVER_GAME_STATE(MAIN_GAME_STATE_RUNNING)				
				BREAK
				
				// Look for game end conditions.
				CASE MAIN_GAME_STATE_RUNNING		
					//SET_INITIAL_DOOR_STATES()
					Maintain_Main_Generic_Server()		
					
					
					#IF IS_DEBUG_BUILD
						IF bHostEndGameNow		
							GlobalServerBD.iServerGameState = MAIN_GAME_STATE_END
						ENDIF
					#ENDIF	
				BREAK 
				
				CASE MAIN_GAME_STATE_END			
									
					// We'd want to upload scores here, show results, do whatever.
					
				BREAK	
				
				DEFAULT
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("Testbed.sc - SERVER invalid game state!!")
					#ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	
		#IF IS_DEBUG_BUILD
			KEYPRESS_DEBUG()
			UPDATE_WIDGETS()
			UPDATE_DEBUG()
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
			
			IF bSetUpUsjs 
				//Turn off/On StuntJumps
				SETUP_FM_USJS()
				bSetUpUsjs  = FALSE
			ENDIF
		#ENDIF
		
		
	ENDWHILE
#ENDIF	
// End of Mission
ENDSCRIPT




