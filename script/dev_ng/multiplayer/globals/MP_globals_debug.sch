USING "rage_builtins.sch"

// Debug Globals Only
#IF IS_DEBUG_BUILD


USING "commands_debug.sch"

USING "mp_globals_comms_definitions.sch"				// For access to the array maximum
USING "mp_globals_ambience_definitions.sch"				// For access to the Cop Backup Requests Received array maximum
USING "mp_globals_activity_selector_definitions.sch"	// For access to MAX_HEIST_PLANNING_MISSIONS
USING "shared_global_definitions.sch"
USING "net_mission_enums.sch"							// For access to the MP_MISSION data
USING "MP_Mission_Data_Struct.sch"						// For access to DEFAULT_MISSION_INSTANCE
USING "Stats_Enums.sch"

// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_debug.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains MP debug-only globals and structs.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************

BOOL bShowPlaylistLbdDebug

BOOL g_bFakeRound
BOOL g_bFakeEndOfMatch

#IF IS_DEBUG_BUILD
BOOL g_bFakePrep
BOOL g_bFakeFinale
#ENDIF

ENUM DEBUG_DISPLAY_STATE
	DEBUG_DISP_OFF = 0,
	DEBUG_DISP_CHK,
	DEBUG_DISP_LBD,
	DEBUG_DISP_TRGT,
	DEBUG_DISP_CLEANUP,
	DEBUG_DISP_REPLAY
ENDENUM

DEBUG_DISPLAY_STATE g_debugDisplayState = DEBUG_DISP_OFF

// -----------------------------------------------------------------------------------------------------------
//     Arrest Debug globals
// -----------------------------------------------------------------------------------------------------------

BOOL g_bForceArrestSpecCamState



// -----------------------------------------------------------------------------------------------------------
//     NJVS globals
// -----------------------------------------------------------------------------------------------------------
BOOL g_bNeverTimeOut
BOOL g_bDontCountVotes
BOOL g_bBlankImages
BOOL g_bDebugRefresh

// ===========================================================================================================
//      MP Debug Log Globals
// ===========================================================================================================

// TRUE if the net log is actually rendering
// NOTE: this will happen if switched on OR if the activation key is continually pressed
BOOL g_MPDebugLogRendering = FALSE
 

// social Feed
BOOL g_b5MinWindowOpen



// -----------------------------------------------------------------------------------------------------------

CONST_INT	MAX_MP_DEBUG_LOG_LINES			32


// -----------------------------------------------------------------------------------------------------------

// The Line Display struct
STRUCT g_structMPDebugLogLineData
	TIME_DATATYPE	timestamp			
	TIME_DATATYPE	fadeStartTimestamp	
	BOOL 			fadeStartTimestampInitialised
	INT				team				= 0
	TEXT_LABEL_15	player
	MP_MISSION		mission				= eNULL_MISSION
	INT				instanceID			= DEFAULT_MISSION_INSTANCE
	TEXT_LABEL_63	extraText
ENDSTRUCT

STRUCT g_StructMPdebugContactMissions
	TEXT_LABEL_31 	CMname
ENDSTRUCT

BOOL g_DoAssociatedWantedLevelPrints

// -----------------------------------------------------------------------------------------------------------

// The Debug Log array - newest data will be placed at array position 0
g_structMPDebugLogLineData 		g_sMPDebugLogLines[MAX_MP_DEBUG_LOG_LINES]

g_StructMPdebugContactMissions	g_sMPDebugCMs[MAX_CONTACT_MISSIONS]

INT g_currentdebugLogLines = 0



// ===========================================================================================================
//      MP Mission Control and Mission Triggering System Debug Globals
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//     Debug Screen States
// -----------------------------------------------------------------------------------------------------------

BOOL g_MPF9ScreenIsRendering = FALSE




// -----------------------------------------------------------------------------------------------------------
//     Flags for toggling debug features on/off
// -----------------------------------------------------------------------------------------------------------

// DEBUG CONSTS FOR RANKED MISSION TRIGGERING
CONST_INT	NOT_USING_RANKED_TRIGGERING			-1



// -----------------------------------------------------------------------------------------------------------
//     Mission Control Debug Globals
// -----------------------------------------------------------------------------------------------------------

// Used to store the requested mission variation read from the XML to be passed to the new mission controller - possibly TEMP
INT		g_MissionControlVariationFromDebugMenu		= -1		// This is used for short-term storage only

// Used to allow missions to launch even if the team configuration is wrong - this is the local toggle flag that sends a broadcast to the server to set the server flag
BOOL	g_missionControlAllowLaunchWithOneTeam		= FALSE

// Used to allow missions to launch even if the minimum players is wrong - this is the local toggle flag that sends a broadcast to the server to set the server flag
BOOL	g_missionControlAllowLaunchWithOnePlayer	= FALSE

// Used to allow dynamic allocation of players to existing missions
BOOL	g_missionControlAllowDynamicAllocation		= FALSE




// -----------------------------------------------------------------------------------------------------------
//     Mission Control Host Resyncing Debug Globals
// -----------------------------------------------------------------------------------------------------------

STRUCT g_structF9ScreenMCHosts
	TEXT_LABEL_63		hostName			// The host name (as a string so it is retained if the host crashes out)
	TIME_DATATYPE		startTime			// The network time that the host swapped to be this player
	BOOL 				startTimeInitialised
	TEXT_LABEL_23		startTimeAsText		// The network time as a text string
	TEXT_LABEL_23		timeAsHostAsText	// The length of time the host was the host
ENDSTRUCT

// The number of F9 screen Host Details to record
CONST_INT	MAX_MC_F9_SCREEN_HOSTS		3
CONST_INT	KNOWN_HOST_ARRAY_POS		0
	
// The array of Host data
g_structF9ScreenMCHosts	g_sF9MCHosts[MAX_MC_F9_SCREEN_HOSTS]


// -----------------------------------------------------------------------------------------------------------

// Used to request logs for a mission controller resync with triggering data where corrective action was needed due to Host Migration data loss
BOOL	g_debugRequestTriggerResyncLogs = FALSE

BOOL	g_b_Ignore_PlayerCount_JobCash  = FALSE
BOOL 	g_b_ForceTutorial

BOOL 	g_bDisableCreatorForceTest


// -----------------------------------------------------------------------------------------------------------
//     Tutorial Session output
// -----------------------------------------------------------------------------------------------------------

BOOL	g_debugTutorialSessionActive	= FALSE
INT		g_debugTutorialSessionInstance	= -1


// ===========================================================================================================
//      MP Grouped Mission System Debug Globals
// ===========================================================================================================

// The current Host of the Mission Control script
INT		g_groupedMissionsHostPlayerAsInt		= -1		// The Grouped Missions Script Host - for debug console log output purposes only




// ===========================================================================================================
//      MP Communications System Debug Globals
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      MP End of Mission Message Debug Globals
// -----------------------------------------------------------------------------------------------------------

WIDGET_GROUP_ID	g_mainEOMMessageWidgetGroup			= NULL
WIDGET_GROUP_ID g_optionalEOMMessageWidgetGroup		= NULL
BOOL			g_WIDGET_EOM_Switch_Widgets_On		= FALSE
BOOL			g_holdEOMMessageWidgetsState		= FALSE
BOOL			g_WIDGET_EOM_Message_Received		= FALSE
BOOL			g_WIDGET_EOM_Message_Active			= FALSE
TEXT_LABEL_63	g_WIDGET_EOM_ScriptName				= ""
TEXT_WIDGET_ID	g_WIDGETID_EOM_ScriptName			= NULL
TEXT_WIDGET_ID	g_WIDGETID_EOM_CharacterID			= NULL
TEXT_WIDGET_ID	g_WIDGETID_EOM_SpeakerID			= NULL
TEXT_WIDGET_ID	g_WIDGETID_EOM_GroupID				= NULL
TEXT_WIDGET_ID	g_WIDGETID_EOM_ConvRootID			= NULL




// ----------------------------------------------------------------------------------------------------------
//      MP Stored Communication Debug Globals
//		KGM 2/9/11: There is now an array of Stored Communication Types so debug variables are needed for
//					all array elements
// -----------------------------------------------------------------------------------------------------------

WIDGET_GROUP_ID	g_mainStoredCommsWidgetGroup			= NULL
WIDGET_GROUP_ID g_optionalStoredCommsWidgetGroup		= NULL
BOOL			g_holdStoredCommsWidgetsState			= FALSE
BOOL			g_WIDGET_StoredComms_Switch_Widgets_On	= FALSE
BOOL			g_WIDGET_StoredComms_Received[MAX_STORED_COMMS_TYPES]
BOOL			g_WIDGET_StoredComms_Active[MAX_STORED_COMMS_TYPES]
BOOL			g_WIDGET_StoredComms_Delay_Expired[MAX_STORED_COMMS_TYPES]
TEXT_LABEL_63	g_WIDGET_StoredComms_ScriptName[MAX_STORED_COMMS_TYPES]
TEXT_LABEL_31	g_WIDGET_StoredComms_CharSheetID[MAX_STORED_COMMS_TYPES]
TEXT_WIDGET_ID	g_WIDGETID_StoredComms_ScriptName[MAX_STORED_COMMS_TYPES]
TEXT_WIDGET_ID	g_WIDGETID_StoredComms_CharacterID[MAX_STORED_COMMS_TYPES]
TEXT_WIDGET_ID	g_WIDGETID_StoredComms_SpeakerID[MAX_STORED_COMMS_TYPES]
TEXT_WIDGET_ID	g_WIDGETID_StoredComms_CharSheetID[MAX_STORED_COMMS_TYPES]
TEXT_WIDGET_ID	g_WIDGETID_StoredComms_GroupID[MAX_STORED_COMMS_TYPES]
TEXT_WIDGET_ID	g_WIDGETID_StoredComms_ConvRootID[MAX_STORED_COMMS_TYPES]




// -----------------------------------------------------------------------------------------------------------
//      MP Communications Requests Debug Globals
// -----------------------------------------------------------------------------------------------------------

WIDGET_GROUP_ID	g_mainMPCommsWidgetGroup				= NULL
WIDGET_GROUP_ID g_optionalMPCommsWidgetGroup			= NULL
BOOL			g_holdMPCommsWidgetsState				= FALSE
BOOL			g_WIDGET_MP_Comms_Switch_Widgets_On		= FALSE
BOOL			g_WIDGET_MP_Comms_Active				= FALSE
TEXT_LABEL_63	g_WIDGET_MP_Comms_ScriptName			= ""
TEXT_LABEL_63	g_WIDGET_MP_Comms_CharSheetID			= ""
TEXT_LABEL_63	g_WIDGET_MP_Comms_PlayerID				= ""
TEXT_LABEL_63	g_WIDGET_MP_Comms_GroupID				= ""
TEXT_LABEL_63	g_WIDGET_MP_Comms_ConvRootID			= ""
TEXT_LABEL_63	g_WIDGET_MP_Comms_CommsTypeID			= ""
TEXT_LABEL_63	g_WIDGET_MP_Comms_DeviceID				= ""
TEXT_LABEL_63	g_WIDGET_MP_Comms_ReplyID				= ""
TEXT_LABEL_63	g_WIDGET_MP_Comms_Component_TL			= ""
INT				g_WIDGET_MP_Comms_Component_INT			= 0
TEXT_WIDGET_ID	g_WIDGETID_MP_Comms_ScriptName			= NULL
TEXT_WIDGET_ID	g_WIDGETID_MP_Comms_CharSheetID			= NULL
TEXT_WIDGET_ID	g_WIDGETID_MP_Comms_PlayerID			= NULL
TEXT_WIDGET_ID	g_WIDGETID_MP_Comms_GroupID				= NULL
TEXT_WIDGET_ID	g_WIDGETID_MP_Comms_ConvRootID			= NULL
TEXT_WIDGET_ID	g_WIDGETID_MP_Comms_CommsTypeID			= NULL
TEXT_WIDGET_ID	g_WIDGETID_MP_Comms_DeviceID			= NULL
TEXT_WIDGET_ID	g_WIDGETID_MP_Comms_ReplyID				= NULL
// Comms Reply widgets - should be able to use the same strings but different widgets
WIDGET_GROUP_ID	g_optionalMPCommsReplyWidgetGroup		= NULL
TEXT_WIDGET_ID	g_WIDGETID_MP_Comms_Reply_ScriptName	= NULL
TEXT_WIDGET_ID	g_WIDGETID_MP_Comms_Reply_ReplyID		= NULL
// String Component widgets
TEXT_WIDGET_ID	g_WIDGETID_MP_Comms_Component_TL		= NULL


// -----------------------------------------------------------------------------------------------------------

// A global to prevent console log spamming by a repeating 'block comms' condition
BOOL			g_mpCommsBlockedByDisabledComms			= FALSE
BOOL			g_mpCommsBlockedByActiveComms			= FALSE
BOOL			g_mpCommsBlockedByPhoneOnscreen			= FALSE
BOOL			g_mpCommsBlockedByRecentInvite			= FALSE
// Added for TU
BOOL			g_mpCommsBlockedBySkyswoop				= FALSE
BOOL			g_mpCommsBlockedByPostHeistCelebration	= FALSE




// ===========================================================================================================
//      Ways To Get Additional Bonus XP: Debug Globals
// ===========================================================================================================

// Buddies in Car XP Bonus
INT		g_WIDGET_NumberOfBuddiesInCar			= 0
INT		g_WIDGET_BuddiesInCarPointsLastUpdate	= 0

// Sirens on MIssion XP Bonus
BOOL	g_WIDGET_SirensCopIsOnMission			= FALSE
INT		g_WIDGET_SirensPointsLastUpdate			= 0





// ===========================================================================================================
//      Objective Text: Debug Globals
// ===========================================================================================================

WIDGET_GROUP_ID	g_mainObjectiveTextWidgetGroup			= NULL
BOOL			g_WIDGET_ObjectiveTextReceived			= FALSE
BOOL			g_WIDGET_ObjectiveTextOnDisplay			= FALSE
BOOL			g_WIDGET_ObjectiveTextHiddenOnPurpose	= FALSE
BOOL			g_WIDGET_ObjectiveTextBlocked_Comms		= FALSE
BOOL			g_WIDGET_ObjectiveTextBlocked_MPHUD		= FALSE
BOOL			g_WIDGET_ObjectiveTextBlocked_Paused	= FALSE
BOOL			g_WIDGET_ObjectiveTextIsCommsActive		= FALSE
BOOL			g_WIDGET_ObjectiveTextIsGodTextOn		= FALSE
BOOL			g_WIDGET_ObjectiveTextIsMPHUDOn			= FALSE
BOOL			g_WIDGET_ObjectiveTextIsPaused			= FALSE



// ===========================================================================================================
//      MP Contact Missions Debug Globals
// ===========================================================================================================

INT		g_WIDGET_cmRank 				= 0
BOOL	g_WIDGET_setCMRank				= FALSE
BOOL	g_WIDGET_nextCMRank				= FALSE
BOOL	g_WIDGET_forceCMPlayerTimeout	= FALSE

INT		g_DEBUG_SkippedToRank			= 0


// ===========================================================================================================
//      Cop Scanning F9 Debug globals
// ===========================================================================================================

INT g_plateScanning_iTimeTillScanning

// ===========================================================================================================
//     territory takeover blocking
// ===========================================================================================================
BOOL g_WIDGET_bblockupload =TRUE
INT g_WIDGET_iexceptionterritory = -1	

// ===========================================================================================================
//      Shifting into the game
// ===========================================================================================================
BOOL g_HaveIShiftedIntoTheGame
INT g_iKickEveryoneOutSessionPlayerNumber
//BOOL g_ShouldShiftingTutorialsBeSkipped //Moving this to Common globals
// ===========================================================================================================
//      debug spawn map globals
// ===========================================================================================================

FLOAT spriteid[50]

STRUCT MAP_SPRITE_PLACEMENT
	
	FLOAT x, y
	FLOAT w, h
	INT r,g,b,a
	FLOAT fRotation
	
ENDSTRUCT

//debug spawn map stuff
MAP_SPRITE_PLACEMENT mpdebugmap[50]

BOOL WidgetsAreOn
WIDGET_GROUP_ID OverheadWidgets

FLOAT g_iMapPositionX
FLOAT g_iMapPositionY
FLOAT g_MapScale
FLOAT g_BlipScale
BOOL g_LeaveMapOn

BOOL g_ForceCantJoinSession

//Weapon and Award widget bools

BOOL WeaponsUnlocked[50]
BOOL WeaponsEquipped[50]
BOOL WeaponsFree[50]
//BOOL WidgetAwardsUnlocked[TOTAL_NUMBER_OF_AWARDS]
BOOL WidgetAwardsEarned[TOTAL_NUMBER_OF_AWARDS]
BOOL WidgetTurnOnApparel
BOOL WidgetRankApparelUnlocked[100]
BOOL WidgetRankApparelEquipped[100]



BOOL g_bPlayerDebugChangedRank
BOOL g_bPlayerDebuggedXP
BOOL g_bGivePlayerWeaponsForRankup
BOOL g_RankBarHandleDebugChanges[2]
BOOL g_bDoUnlocksForDebugRankUp



SCRIPT_TIMER g_DebugDpadDownGlobalPrint
//BOOL g_DebugDpadDownSet

SCRIPT_TIMER g_DebugDrawUnlocksPrintTimer
INT g_DebugTimerAmount = 0
INT g_FlashingTimer1, g_FlashingTimer2
SCRIPT_TIMER ActivatingDpaddown
SCRIPT_TIMER DPadDownCheckTimer
SCRIPT_TIMER bHudLightCheckTimer
SCRIPT_TIMER AwardDebugTimer
SCRIPT_TIMER UnlocksDebugTimer
SCRIPT_TIMER RankDebugTimer
SCRIPT_TIMER OverheadDebugTimer
SCRIPT_TIMER StatLoadingDebugTimer
SCRIPT_TIMER TodoBoxDebugTimer
SCRIPT_TIMER AWARDUNLOCKFINISHEDDebugTimer
SCRIPT_TIMER DisableHudCheckDebugTimer
SCRIPT_TIMER HeadshotsDebugTimer
SCRIPT_TIMER SkySwoopCamDebugTimer
SCRIPT_TIMER TimerHudDebugTimer
SCRIPT_TIMER TimerHudPhoneDebugTimer



BOOL g_b_OverrideObjective

BOOL g_bLeaderboardCamExtraDebugPrints

BOOL g_UpdatePlayersStartingHealth
INT g_StartingHealthChange 

BOOL g_TurnOnOverheadDebug
BOOL bInChatWindow
BOOL g_TurnOnOverheadDebugFullOn
BOOL g_TurnOnOverheadDebugHealthBar
BOOL g_TurnOnRallyDriverOverheadDebug
BOOL g_TurnOnSameVehCheckDebug
BOOL g_bDoArrowCheckDebug
BOOL g_bDoOverheadsPassivePrints
BOOL g_bDoSetLogicAndEventActivePrints
BOOL g_bBlockLocalPlayerOverhead[2]
BOOL g_bTogglePlayerOverhead[2][NUM_NETWORK_PLAYERS]

INT g_iNumberOfplayersIncludingMeWidget
INT g_iNumberOfplayersIncludingMeDisplay
INT g_iNumberOfplayersIncludingMeRawDisplay

BOOL g_WarnPeopleAboutSpectatorCam

BOOL g_bDoJSkip
VECTOR g_vJSkipPos
BOOL g_bJSkipKeepVehicle
BOOL g_bJSkipLeavePed

BOOL G_bTurnOnInGameWidgets
BOOL G_bTurnOnAllHUDBlockers
INT  G_iPlayerRoleWidgets
INT  G_iHeistRoleWidgets

BOOL g_bLaunchImportExportHpv
BOOL g_bLaunchAmbMiss
BOOL g_bKickingPlayer_LastFrame[NUM_NETWORK_PLAYERS][NUM_NETWORK_PLAYERS]
BOOL g_BKickingRunWidgetCHeck
BOOL g_BKickingRunWIDGETPRINTS
BOOL g_Widget_KickPlayer[NUM_NETWORK_PLAYERS]

BOOL g_Widget_KickLocalPlayer

BOOL g_DebugDoAwardPrints

BOOL g_bPlayerSkippedIntro
BOOL g_bTutorialCompleteCheck

BOOL g_bDebugLaunchCarModTut
BOOL g_bDebugLaunchLesterCut
BOOL g_bDebugLaunchHoldupTut
BOOL g_bDebugLaunchTrevorCut

BOOL g_bLaunchSCTVChopper			// Speirs, allows me to create SCTV chopper
BOOL g_IgnoreTeamGTARaceCoronaRules // Speirs, allows me to enter team GTA race with only 2 players same team
BOOL g_b4357710
BOOL g_NJVS_Spam					// Spam for 2001613
BOOL g_DebugDrawDpadLbd				// Debug draw the dpad leaderboard
BOOL g_b_PadColours 				// Allow pad colours widget in races
BOOL g_bFakeAirDrop

BOOL g_bDrawDebugActivityCreatorStuff
BOOL g_bDrawDebugActivityPedCreatorStuff

BOOL g_b_MaskDebug
INT g_i_MaskAnimToPlay = -1

BOOL g_DM_Intro_Hold				// Stop intro from ending
BOOL g_DM_Intro_Name_Anim
BOOL g_DM_Reset						// Reset intro to start
BOOL g_b_MakePeds
BOOL g_SkipCelebAndLbd
INT g_i_AnimToPlay = -1
INT g_i_AnimChar = -1
INT g_i_AnimTime = 3000
FLOAT fPlayAnimFrom = 0.0

INT iArenaCelebNumPlayers = -1

// -----------------------------------------------------------------------------------------------------------
//     Luke Cam globals 2136207
// -----------------------------------------------------------------------------------------------------------
FLOAT g_f_FOV 
VECTOR g_v_Cam1   
VECTOR g_v_Cam2
VECTOR g_v_CamRot1   
VECTOR g_v_CamRot2
VECTOR g_v_Cam1Point
VECTOR g_v_Cam2Point
//VECTOR g_v_TeamCentre
VECTOR g_v_CenterPoint
INT g_i_Duration
INT g_i_OldCamNumber = -1
INT g_i_OldHCamNumber = -1
BOOL g_b_ResetAllcams
BOOL g_b_OutPut
BOOL g_b_SaveCam1
BOOL g_b_SaveCam2
//BOOL g_b_SaveCamPt1
//BOOL g_b_SaveCamPt2
BOOL g_b_SaveOldCam
BOOL g_b_SaveVsCam
//BOOL g_b_SaveCentre
BOOL g_b_SaveOldHCam
BOOL g_b_FailCams

// ===========================================================================================================
//     Placeholder Cutscene
// ===========================================================================================================
BOOL g_bDrawPlaceHolderCutRect
BOOL g_bDrawPlaceHolderCutRectSmall

BOOL g_bChatOverrideIsActive
INT g_iLastChatOverrideTicker = -1

// ===========================================================================================================
//    Bots
// ===========================================================================================================

STRUCT GLOBAL_BOTS_DATA

	BOOL bBotsActive
	BOOL g_bBotsToJoinHeist

	INT iNumberOfCreatedBots
	
	BOOL bFillSubScriptsWithBots
	BOOL bRemoveBotsFromSubScripts
	BOOL bRemoveSingleBot
	BOOL bRemoveAllBots
	//BOOL bAddBotForTeam[MAX_NUM_TEAMS]
	BOOL bShowBotsDebug
	BOOL bFillSessionWithBots
	BOOL bAddSingleBot
	
	INT iBS_BotToldToJoinScript
	INT iBS_BotToldToLeaveScript
	INT iBS_BotToldToGetBrain
	INT iBS_BotToldToCopyPlayersBD
	//INT iBS_BotToldToJoinTeam[MAX_NUM_TEAMS]
	//INT iBS_BotInTeam[MAX_NUM_TEAMS]
	INT iLastCreatedBot = -1
	INT iLastBotToJoinScript = -1
	//INT iStaggeredTeam
	
	VECTOR vPerformanceTestCoords 
	FLOAT fPerformanceTestHeading
	BOOL bDoPerformanceTest
	TIME_DATATYPE PerformanceTimer
	
	
ENDSTRUCT
	
GLOBAL_BOTS_DATA g_BotsData

BOOL g_bDebugDisplayRewardBanner
BOOL g_bActivateRewardBanner[38]
INT g_iActivateRewardsTeam

// James A: Pause Menu Widgets
BOOL g_bDebugPMIncludeSPCategory

// Dave W Freemode debug unlocks
BOOL g_bDebugFMNoPhoneCalls


BOOL g_bCleanupInGameHud

BOOL g_bAlreadySkippedIntoFreemode

BOOL g_bIsRockstarDev

BOOL g_b_SpamRankOutput // Speirs added for 1407518

BOOL g_b_StreenameWidgetUse


// ===========================================================================================================
//      Player Headshots: Debug Globals
// ===========================================================================================================

// The most recent state of the headshot beign generated - used to cut down on the console log output spam
PEDHEADSHOTSTATE	g_debugHeadshotBeingGeneratedState	= PHS_INVALID
PEDHEADSHOTSTATE	g_debugTransparentHeadshotState	= PHS_INVALID



// ===========================================================================================================
//    Reward Wigets
// ===========================================================================================================
BOOL bTurnOnUnlockWidgets
BOOL g_bDeleteAllCharsQuick

BOOL g_bDebugAwardWeeklyInterestNow

BOOL g_bActivateWeaponUnlockWidgets[NUM_WEAPON_BITSETS]
BOOL g_bActivateWeaponAddonUnlockWidgets[NUM_WEAPON_BITSETS][10]

BOOL G_bShiftWithLowRank

BOOL g_bActivateKitUnlockWidgets[COUNT_OF(PLAYERKIT)]
BOOL g_bActivateAbilityUnlockWidgets[COUNT_OF(PLAYERABILITIES)]
BOOL g_bActivateCarModUnlockWidgets[COUNT_OF(CARMOD_UNLOCK_ITEMS)]
BOOL g_bActivateClothesUnlockWidgets[COUNT_OF(CLOTHES_UNLOCK_ITEMS)]
BOOL g_bActivateVEHICLESUnlockWidgets[COUNT_OF(PLAYERVEHICLE_UNLOCK)]
BOOL g_bActivateHEISTUnlockWidgets[COUNT_OF(HEIST_UNLOCK)]
BOOL g_bActivateHEALTHUnlockWidgets[COUNT_OF(PLAYERHEALTH)]
BOOL g_bActivateTATTOOUnlockWidgets[COUNT_OF(TATTOO_NAME_ENUM) ]

INT g_bAwardTeamAwardUnlock

BOOL g_bAward_UnlockAllBronze
INT g_iAward_UnlockAllBronzeStaggeredIndex
BOOL g_bAward_UnlockAllBronzePlayer
INT g_iAward_UnlockAllBronzePlayerStaggeredIndex

BOOL g_bAward_UnlockAllSilver
INT g_iAward_UnlockAllSilverStaggeredIndex
BOOL g_bAward_UnlockAllSilverPlayer
INT g_iAward_UnlockAllSilverPlayerStaggeredIndex

BOOL g_bAward_UnlockAllGold
INT g_iAward_UnlockAllGoldStaggeredIndex
BOOL g_bAward_UnlockAllGoldPlayer
INT g_iAward_UnlockAllGoldPlayerStaggeredIndex

BOOL g_bAward_UnlockAllPlatinum
INT g_iAward_UnlockAllPlatinumStaggeredIndex
BOOL g_bAward_UnlockAllPlatinumPlayer
INT g_iAward_UnlockAllPlatinumPlayerStaggeredIndex

BOOL g_bAward_UnlockAllBool
BOOL g_bAward_UnlockAllBoolPlayer

BOOL g_bActivateINTAWARDBRONZEUnlockWidgets[COUNT_OF(MP_INT_AWARD) ]
BOOL g_bActivateINTAWARDSILVERUnlockWidgets[COUNT_OF(MP_INT_AWARD)]
BOOL g_bActivateINTAWARDGOLDUnlockWidgets[COUNT_OF(MP_INT_AWARD)]
BOOL g_bActivateINTAWARDPLATINUMUnlockWidgets[COUNT_OF(MP_INT_AWARD)]

BOOL g_bActivateFLOATAWARDBRONZEUnlockWidgets[COUNT_OF(MP_FLOAT_AWARD)]
BOOL g_bActivateFLOATAWARDSILVERUnlockWidgets[COUNT_OF(MP_FLOAT_AWARD)]
BOOL g_bActivateFLOATAWARDGOLDUnlockWidgets[COUNT_OF(MP_FLOAT_AWARD)]
BOOL g_bActivateFLOATAWARDPLATINUMUnlockWidgets[COUNT_OF(MP_FLOAT_AWARD)]

BOOL g_bActivateBOOLAWARDBRONZEUnlockWidgets[COUNT_OF(MP_BOOL_AWARD)]
BOOL g_bActivateBOOLAWARDSILVERUnlockWidgets[COUNT_OF(MP_BOOL_AWARD)]
BOOL g_bActivateBOOLAWARDGOLDUnlockWidgets[COUNT_OF(MP_BOOL_AWARD)]
BOOL g_bActivateBOOLAWARDPLATINUMUnlockWidgets[COUNT_OF(MP_BOOL_AWARD)]

BOOL g_bActivatePLYINTAWARDBRONZEUnlockWidgets[MAX_NUM_MPPLY_INT_AWARD]
BOOL g_bActivatePLYINTAWARDSILVERUnlockWidgets[MAX_NUM_MPPLY_INT_AWARD]
BOOL g_bActivatePLYINTAWARDGOLDUnlockWidgets[MAX_NUM_MPPLY_INT_AWARD]
BOOL g_bActivatePLYINTAWARDPLATINUMUnlockWidgets[MAX_NUM_MPPLY_INT_AWARD]


BOOL g_bActivatePLYFLOATAWARDBRONZEUnlockWidgets[MAX_NUM_MPPLY_FLOAT_AWARD]
BOOL g_bActivatePLYFLOATAWARDSILVERUnlockWidgets[MAX_NUM_MPPLY_FLOAT_AWARD]
BOOL g_bActivatePLYFLOATAWARDGOLDUnlockWidgets[MAX_NUM_MPPLY_FLOAT_AWARD]
BOOL g_bActivatePLYFLOATAWARDPLATINUMUnlockWidgets[MAX_NUM_MPPLY_FLOAT_AWARD]

BOOL g_DisplayTimersDisplaying
BOOL g_bHUDOrderPrints

BOOL g_Debug4HourWantedCheck


//DEBUG MENU EVENTS
CONST_INT DEBUG_MENU_EVENT_CRATE_DROP0	0
CONST_INT DEBUG_MENU_EVENT_IMP_EXP		1
CONST_INT DEBUG_MENU_EVENT_IMP_HPV		2
CONST_INT DEBUG_MENU_AMB_MISSION		3





// ===========================================================================================================
//      General Globals copied from an obsolete file but look like they are still needed
// ===========================================================================================================

BOOL bXpAnimAboveHead
BOOL bXpAnimXYZ
VECTOR vXpAnimPt

BOOL bForceBigMessage
INT iDebugBigMessage
INT iDebugEndPos
BOOL g_bOverrideBMTime
INT g_iOverrideBMTime

INT g_iDebugSelectedMission			// Mission selected in widgets.
INT ib_DebugMenuLaunchingBitSet

BOOL g_SecVanLaunchingDebugPrints

BOOL g_bForcePlayerPlayedAnHour

// Debug Menu Mission Trigger Bitset
//CONST_INT DBM_START_SECVAN				6
//CONST_INT DBM_WARP_TO_SECVAN			7
CONST_INT DBM_START_JOYRIDER			22
CONST_INT DBM_WARP_TO_JOYRIDER			23

BOOL g_DB_AllowGarageOutput
SCRIPT_TIMER GarageOutputStopSpamTimer

BOOL bTestFlashingTags
BOOL bForceThroughCleanup


BOOL g_b_IgnoreSoloChecksForCheating

BOOL g_bPrintScriptEventDetails

INT g_iFailReason_SavedVehicleAvailableForCreation

BOOL b_bPrintFailReason_SavedVehicleAvailableForCreation

BOOL g_b_IsSavedVehicleSafeToProcess

BOOL g_db_bPrintServerPropertyRequestDetails
BOOL g_db_bPrintPropertyRequestDetailsAll

CONST_INT DEBUG_MENU_TUTORIAL_LAUNCH_CAR_MOD	0
CONST_INT DEBUG_MENU_TUTORIAL_LAUNCH_LESTER		1
CONST_INT DEBUG_MENU_TUTORIAL_LAUNCH_TREVOR		2
CONST_INT DEBUG_MENU_TUTORIAL_LAUNCH_SIMEON		3
CONST_INT DEBUG_MENU_TUTORIAL_LAUNCH_CNC		4
CONST_INT DEBUG_MENU_TUTORIAL_CNC_KILLGV		5
CONST_INT DEBUG_MENU_TUTORIAL_CNC_DDEAL_TUT		6
CONST_INT DEBUG_MENU_TUTORIAL_CNC_DDEAL			7
CONST_INT DEBUG_MENU_TUTORIAL_CNC_TERR			8
CONST_INT DEBUG_MENU_TUTORIAL_CNC_SNITCH_TUT	9
CONST_INT DEBUG_MENU_CNC_TERRITORY_TAKEOVER		10
CONST_INT DEBUG_MENU_CNC_OCEAN_DROP				11
CONST_INT DEBUG_MENU_HEIST_LAUNCH_INTRO			12
CONST_INT DEBUG_MENU_TUTORIAL_CNC_CP_WELC_TUT	13
CONST_INT DEBUG_MENU_TUTORIAL_CNC_CP_STOLEN_TUT	14

INT g_iDebugMenuTutorialLaunch


// ===========================================================================================================
//      Les Widget Globals copied from an obsolete file but may still be useful
//		(NOTE: Widget itself may now be removed so may need found and placed elsewhere)
// ===========================================================================================================

FLOAT lw_fNOTICABLE_RADIUS = 100
BOOL lw_bUnlockAllShopItems
BOOL lw_bUnlockAllTattoos
BOOL lw_bUnlockAllClothes
BOOL lw_bUnlockAllHaircuts
BOOL lw_bUnlockAllWeapons
BOOL lw_bUnlockAllAwards
INT lw_icooldowntimer   = 5000
INT lw_ibikeselect = -1
BOOL lw_bUnlockAllCarmods

BOOL g_b_HangTheTransition

FLOAT lw_fAiDamageModifier					= FREEMODE_AI_DAMAGE	//0.35
BOOL lw_bSetAiDamageModifier

FLOAT lw_fPlayerWeaponDamageModifier		= FREEMODE_PLAYER_WEAPON_DAMAGE		//0.35	//0.5	//1.0
BOOL lw_bSetPlayerWeaponDamageModifier

FLOAT lw_fPlayerVehicleDamageModifier		= FREEMODE_PLAYER_VEHICLE_DAMAGE	//0.5
BOOL lw_bSetPlayerVehicleDamageModifier

FLOAT lw_fSetMissPedHealth = 1
FLOAT lw_fSetMissPedAccuracy = 1
INT lw_iEnemyRespawns = 1
BOOL lw_bSetMissionDiffModifier

BOOL g_bRunChangeCharacterScreen

BOOL g_db_bsetAlwaysTriggerNPCBounty
BOOL g_db_OutputBountyDebug

BOOL g_db_bAlwaysUsePresence

BOOL g_bLaunchPIMDebug

TWEAK_INT MIN_PLAYER_OVERRIDE 0

STRUCT DEBUG_XP_ANIMATION
	BOOL bTriggerAnim
	BOOL bUsedWidgetValues
	BOOL bDisplayDebug
	
	BOOL bGrabPlayerCoords
	INT iValue
	INT iAnimType
	VECTOR vTestLoc
	
	TEXT_WIDGET_ID tTestName
	
	FLOAT fXTextLocRO
	FLOAT fYTextLocRO
	FLOAT fTextHeightRO
	FLOAT fTextWidthRO
	
	FLOAT fXIconLocRO
	FLOAT fYIconLocRO
	FLOAT fIconHeightRO
	FLOAT fIconWidthRO
	FLOAT fIconSizeMultiplier
	
	FLOAT fTextWidthOffsetMultiplier
	FLOAT fTextIconOffsetMultiplier
	FLOAT fTextIconEmptySpaceOffsetMultiplier
	
	FLOAT fTextHeightOffsetMultiplier
	FLOAT fTextIconHeightOffsetMultiplier
	
	FLOAT fScale
	
	FLOAT WrapEndX
	
	INT iJustification
ENDSTRUCT
DEBUG_XP_ANIMATION debugXPAnim

STRUCT DEBUG_LIMITED_SHOP_ITEMS
	BOOL bTriggerPurchase
	INT iPurchaseItemID
	INT iPurchaseAmount
	
	BOOL bReadLeaderboardValues
	INT iReadStage
	INT iTempLoadStage
	BOOL bTempReadResult
ENDSTRUCT
DEBUG_LIMITED_SHOP_ITEMS debugLimitedShopItems

STRUCT DEBUG_ELO_LEADERBOARD
	BOOL bReadData
	BOOL bWriteData
	INT iWriteNewEloValue
	INT iReturnedELO
	
	INT iReadStage
	INT iTempLoadStage
	BOOL bTempReadResult
ENDSTRUCT
DEBUG_ELO_LEADERBOARD debugELOLeaderboard


// // // from scenetool_debug.sch // //

CONST_INT SCENETOOL_PANWIDGET_MAX		5
CONST_INT SCENETOOL_CUTWIDGET_MAX		5
CONST_INT SCENETOOL_MARKERWIDGET_MAX	5
CONST_INT SCENETOOL_PLACERWIDGET_MAX	5
CONST_INT SCENETOOL_ANGAREAWIDGET_MAX	2
CONST_INT SCENETOOL_POINTWIDGET_MAX		5

STRUCT structSceneTool_MouseDrag
	BOOL bIsDragging
	FLOAT x
	FLOAT y
	FLOAT rot
	FLOAT width
	FLOAT height
ENDSTRUCT

STRUCT structSceneTool_PanWidget
	BOOL					bGrabStart
	BOOL					bGrabEnd
	BOOL					bShowStart
	BOOL					bShowEnd
ENDSTRUCT

STRUCT structSceneTool_CutWidget
	BOOL					bGrabCam
	BOOL					bShowCam
ENDSTRUCT

STRUCT structSceneTool_MarkerWidget
	BOOL					bGrabPos
ENDSTRUCT

STRUCT structSceneTool_PlacerWidget
	BOOL					bGrabPos
ENDSTRUCT

STRUCT structSceneTool_AngAreaWidget
	BOOL					bGrabPos0
	BOOL					bGrabPos1
ENDSTRUCT

STRUCT structSceneTool_PointWidget
	BOOL					bGrabPos
ENDSTRUCT

STRUCT structSceneTool
	WIDGET_GROUP_ID					hWidget
	BOOL							bRunScene
	BOOL							bSaveNow
	BOOL							bSaveOnExit
	BOOL							bForceDebugDraw

	INT								hCamSelection
	INT								hPosSelection
	structSceneTool_MouseDrag		mMouseDrag
	
	structSceneTool_PanWidget		mPanWidgets[SCENETOOL_PANWIDGET_MAX]
	structSceneTool_CutWidget		mCutWidgets[SCENETOOL_CUTWIDGET_MAX]
	structSceneTool_MarkerWidget	mMarkerWidgets[SCENETOOL_MARKERWIDGET_MAX]
	structSceneTool_PlacerWidget	mPlacerWidgets[SCENETOOL_PLACERWIDGET_MAX]
	structSceneTool_AngAreaWidget	mAngAreaWidgets[SCENETOOL_ANGAREAWIDGET_MAX]
	structSceneTool_PointWidget		mPointWidgets[SCENETOOL_POINTWIDGET_MAX]
ENDSTRUCT
structSceneTool g_sAmMpPropertyExtSceneTool
// // // // // // // // // // // // //


// KGM 29/10/14: Changing use of NUMPAD-7 variable to be more consistent with new progression STAT. Making g_nextNumpad7TriggeredHeist obsolete and renaming to avoid errors.
// Allows NUMPAD-7 to activate all the Heists in rotation
INT  	g_DEBUG_Numpad7HeistFlowOrder		= UNKNOWN_HEIST_FLOW_ORDER
BOOL	g_DEBUG_UseNumpad7Heist				= FALSE

// Allows Numpad7 launch the Heist strand specified by the Numpad7 variable above
BOOL g_DEBUG_launchNextHeistStrand = FALSE

// Allows Numpad7 launch the Heist strand specified by the Numpad7 variable above
BOOL g_DEBUG_quickLaunchPlanning = FALSE

// Allows Numpad8 to kill the current heist sequence
BOOL g_DEBUG_cancelCurrentHeistStrand = FALSE

// Allows Dave's 'M' key Lester pre-heist cutscene reset to completely reset the Heists back to the first Heist again
BOOL g_DEBUG_forceHeistsFullRestart = FALSE

// Contains F9 screen text for current Heist Cutscene status
TEXT_LABEL_31 g_DEBUG_F9_Heist_Cutscene_Stage

// Contains a flag used on the F9 screen to indicate a Prep Mission Headers download is being maintained and another to indicate Alastair's routines are being accessed (HEIST_PRE_START_MONITOR_TRIGGER)
BOOL g_DEBUG_F9_Prep_Download_Active = FALSE

// Contains some Heist Rewards details for F9 screen output
INT g_DEBUG_F9_Num_Heist_Rewards			= 0

INT g_DEBUG_Heist_Flow_Order_BS	= 0
INT g_DEBUG_Heist_Same_Team_BS	= 0
INT g_DEBUG_Heist_No_Deaths_BS	= 0
INT g_DEBUG_Heist_Member_BS     = 0
INT g_DEBUG_Heist_First_Person_BS = 0

BOOL g_DEBUG_Heist_First_Time_Fleeca
BOOL g_DEBUG_Heist_First_Time_Prison
BOOL g_DEBUG_Heist_First_Time_Humane
BOOL g_DEBUG_Heist_First_Time_Series
BOOL g_DEBUG_Heist_First_Time_Pacific

BOOL g_DEBUG_Heist_Enable_Flow_Challenge_Widgets

BOOL g_DEBUG_bUpdateGangopsFlowAwardWidgets = FALSE
INT g_DEBUG_GangopsAllInOrderBS	= 0
INT g_DEBUG_GangopsLoyaltyBS	= 0
INT g_DEBUG_GangopsLoyalty2BS	= 0
INT g_DEBUG_GangopsLoyalty3BS	= 0
INT g_DEBUG_GangopsCriminalMastermindBS	= 0
INT g_DEBUG_GangopsCriminalMastermind2BS	= 0
INT g_DEBUG_GangopsCriminalMastermind3BS	= 0
INT g_DEBUG_GangopsSupportBS	= 0

BOOL g_DEBUG_MP_Daily_Objective_Complete[3]

BOOL	g_DEBUG_widgetUseSpecifiedOrder		= FALSE
INT		g_DEBUG_WidgetRandomPrepToPlay[MAX_HEIST_PLANNING_MISSIONS]
BOOL	g_DEBUG_widgetClearAllReplayTimers	= FALSE
BOOL	g_DEBUG_widgetEachFrameOutputInvitorGH = FALSE
TEXT_LABEL_7 g_DEBUG_F9BoardReplayBits = ""

STRUCT DEBUG_VEH_DUP_TEST_STRUCT
	INT iVehicleTimestamp[2]
	INT iVehicleSlot[2]
	BOOL bTriggerTest
	BOOL bDeleteVehicles
	BOOL bEnableOverride
ENDSTRUCT
DEBUG_VEH_DUP_TEST_STRUCT g_dbVehDupProt

BOOL g_bDisableFactoryMaterialUsage = FALSE

#IF IS_DEBUG_BUILD
BOOL g_bStartRCVehicleScript
BOOL g_bCleanupRCVehicleScript
BOOL g_bStartDroneScript
BOOL g_bCleanupDroneScript
BOOL g_bStartVehWeaponScript
BOOL g_bCleanupehWeaponScript
INT g_iDroneType = -1
BOOL g_bForceRenderDroneCam	= FALSE
BOOL g_bForceStopRenderDroneCam	= FALSE
BOOL g_bDisplayDroneShapeTest	= FALSE
VECTOR g_vDroneShapeTestOffset
FLOAT g_fDroneShapeTestRadius

// nightclub editor
VECTOR g_vNightClubPedPos[MAX_NUMBER_OF_NIGHTCLUB_PED_DATA] 		// 0 in changed array
FLOAT g_fNightClubPedHeading[MAX_NUMBER_OF_NIGHTCLUB_PED_DATA]		// 1 
INT g_iNightClubActivitySlot[MAX_NUMBER_OF_NIGHTCLUB_PED_DATA]		// 2
INT g_iNightClubPedType[MAX_NUMBER_OF_NIGHTCLUB_PED_DATA]			// 3
INT g_iNightClubOffsetPedId[MAX_NUMBER_OF_NIGHTCLUB_PED_DATA]		// 4
BOOL g_bNightClubPedDataChanged[MAX_NUMBER_OF_NIGHTCLUB_PED_DATA][5]
BOOL g_bNightClubActivitySlotHasRandomness[MAX_NUMBER_OF_NIGHTCLUB_PED_DATA]		// 3
INT g_iHallwayPedLayoutWidget
INT g_iToiletPedsLayoutWidget
INT g_iMainAreaLayoutWidget
INT g_iMainAreaLayoutVariationWidget
BOOL g_bWidgetTurnOnExtraBouncers
BOOL g_bWidgetTurnOnExtraBartenders
BOOL g_bWidgetTurnOnDJEntourage
BOOL g_bWidgetTurnOnPodiumDancers
INT g_iWidgetPodiumPedsStyle
INT g_iWidgetPodiumPedsState
BOOL g_bNightClubPedEditorActive
BOOL g_bOutputPedVariations
INT g_iWidgetTonyPosition 
INT g_iWidgetLazlowPosition 
INT g_iNightClubSetupStateWidget
INT g_iActiveDJWidget
INT g_iNightClubDrunkSpawnLocation = -1

// casino editor
VECTOR g_vCasinoPedPos[MAX_NUMBER_OF_CASINO_PEDS] 					// 0 in changed array
FLOAT g_fCasinoPedHeading[MAX_NUMBER_OF_CASINO_PEDS]				// 1 
INT g_iCasinoActivitySlot[MAX_NUMBER_OF_CASINO_PEDS]				// 2
INT g_iCasinoPedType[MAX_NUMBER_OF_CASINO_PEDS]						// 3
BOOL g_bCasinoPedEditorActive
BOOL g_bCasinoPedWidgetRepositionPeds = TRUE
INT g_iCasinoDrunkSpawnLocation = -1
INT g_iArcadeDrunkSpawnLocation = -1
#IF FEATURE_CASINO_NIGHTCLUB
INT g_iCasinoNightclubDrunkSpawnLocation = -1
#ENDIF
#IF FEATURE_HEIST_ISLAND
INT g_iBeachPartyDrunkSpawnLocation = -1
#ENDIF
#IF FEATURE_FIXER
#IF FEATURE_MUSIC_STUDIO
INT g_iMusicStudioDrunkSpawnLocation = -1
#ENDIF
#ENDIF
BOOL g_bKillCasinoPedScript = FALSE
TEXT_WIDGET_ID tdCasinoPedAnimName[MAX_NUMBER_OF_CASINO_PEDS]
BOOL g_bKillCasinoPedLogic = FALSE

CONST_INT DEBUG_MAX_NUMBER_OF_CASINO_PEDS	(MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA + (MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)) + MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA + MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE // 228

VECTOR g_vDebugPedEditorLayoutOnePedCoords[DEBUG_MAX_NUMBER_OF_CASINO_PEDS]
VECTOR g_vDebugPedEditorLayoutTwoPedCoords[DEBUG_MAX_NUMBER_OF_CASINO_PEDS]
VECTOR g_vDebugPedEditorLayoutThreePedCoords[DEBUG_MAX_NUMBER_OF_CASINO_PEDS]
FLOAT g_fDebugPedEditorLayoutOnePedHeading[DEBUG_MAX_NUMBER_OF_CASINO_PEDS]
FLOAT g_fDebugPedEditorLayoutTwoPedHeading[DEBUG_MAX_NUMBER_OF_CASINO_PEDS]
FLOAT g_fDebugPedEditorLayoutThreePedHeading[DEBUG_MAX_NUMBER_OF_CASINO_PEDS]

STRUCT DEBUG_CASINO_PED_EDITOR_STRUCT
	INT iActivity
	INT iPedType
	INT iAssignedArea
ENDSTRUCT

DEBUG_CASINO_PED_EDITOR_STRUCT eDebugPedEditorLayoutOne[DEBUG_MAX_NUMBER_OF_CASINO_PEDS]
DEBUG_CASINO_PED_EDITOR_STRUCT eDebugPedEditorLayoutTwo[DEBUG_MAX_NUMBER_OF_CASINO_PEDS]
DEBUG_CASINO_PED_EDITOR_STRUCT eDebugPedEditorLayoutThree[DEBUG_MAX_NUMBER_OF_CASINO_PEDS]

BOOL g_bDebugRIGSettings = FALSE
BOOL g_bDisableGridLights = FALSE
BOOL g_bDoDJSignatureMove = FALSE
INT g_iDJSignatureMoveState = 0
INT g_iDJSigMove_Scene = -1
PED_INDEX g_ped_DJ_1
PED_INDEX g_ped_DJ_2
INT g_iDJ_active_id1
INT g_iDJ_active_id2
OBJECT_INDEX g_objectDJSigMove_1
OBJECT_INDEX g_objectDJSigMove_2
OBJECT_INDEX g_objectDJSigMove_3
OBJECT_INDEX g_objectDJSigMove_4

BOOL g_db_bTestArenaCareerLeaderboardWrite
BOOL g_db_bTestArenaCareerLeaderboardRead

BOOL g_db_bTestArenaModeLeaderboardWrite
BOOL g_db_bTestArenaModeLeaderboardRead

// Casino Special peds editor
BOOL	g_bCasinoSpecialPedsKeep
BOOL	g_bCasinoSpecialPedsDisable
INT 	g_iCasinoSpecialPeds[12]					
INT 	g_iCasinoSpecialPedOverrideConversation[12]					
BOOL 	g_bCasinoSpecialPedResetOverrideConversation[12]
BOOL 	g_bCasinoSpecialPedAlive[12]
BOOL 	g_bCasinoSpecialPedUpdateScenario[12]
BOOL 	g_bCasinoSpecialPedScenarios[36]
TEXT_LABEL_63 	g_tlCasinoSpecialPedScenario[12]
TEXT_WIDGET_ID	g_twCasinoSpecialPedAssignedArea[12]
TEXT_WIDGET_ID	g_twCasinoSpecialPedActiveScenario[12]
//

#ENDIF

BOOL g_bWidgetTurnOnStaffPeds
BOOL g_bWidgetTurnOnCustomerPeds
BOOL g_bNightClubUseWidgetValues
INT g_iNightClubClientele_WidgetValue
INT g_iNightClubClienteleVariation_WidgetValue
VECTOR g_vChampagnePosOffset
VECTOR g_vChampagneRotOffset

INT g_iDebugSetNightclubPedSpeech = -1

CONST_INT MAX_NUMBER_OF_VISIBLE_BLOCKS 		10
CONST_FLOAT MIN_VISIBLE_BLOCKS_AREA 		0.0
CONST_FLOAT MAX_VISIBLE_BLOCKS_AREA 		1.0

STRUCT PED_VISUAL_DISPLAY_BLOCK_STRUCT
	FLOAT fCentreX
	FLOAT fCentreY
	FLOAT fWidth 
	FLOAT fHeight
	INT iR
	INT iG
	INT iB = 255
	TEXT_LABEL_31 tlBlockHeading
ENDSTRUCT

//CONST_INT MAX_NUMBER_OF_NIGHTCLUB_PEDS	125

STRUCT PED_VISUAL_DISPLAY_STRUCT
	PED_VISUAL_DISPLAY_BLOCK_STRUCT VisualBlockArray[MAX_NUMBER_OF_VISIBLE_BLOCKS]
	PED_VISUAL_DISPLAY_BLOCK_STRUCT VisualPropBlockArray[MAX_NUMBER_OF_NIGHTCLUB_PEDS]
	PED_VISUAL_DISPLAY_BLOCK_STRUCT VisualPedBlockArray[MAX_NUMBER_OF_NIGHTCLUB_PEDS]
	INT iBlockCount
ENDSTRUCT

PED_VISUAL_DISPLAY_STRUCT g_VisualDisplayStruct
BOOL g_bOutputNightClubPedSettings
BOOL g_bOutputNightClubPedChangesOnly = TRUE
BOOL g_bRecreateNightClubPeds
BOOL gdb_bUseWidgetValuesForNCPedPositions

BOOL g_bOutputCasinoPedClothingSettings
BOOL g_bOutputCasinoPedSettings
BOOL g_bOutputCasinoPedChangesOnly = TRUE
BOOL g_bRecreateCasinoPeds
BOOL g_bWarpPlayerToHighlightedPed
BOOL gdb_bUseWidgetValuesForCasinoPedPositions
BOOL g_bCasinoDisplayActivePedTotal = FALSE
BOOL g_bActivityTests = FALSE
INT g_iAnimationSequence = -1
//BOOL g_bCasinoPedActivityChosen = FALSE
//INT g_iCasinoPedActivity = -1
BOOL g_bValidateHighlightedPedAnims = FALSE
BOOL g_bSetHighlightedPedAnimValidation = FALSE
INT g_iValidateHighlightedPedAnimID = 0
INT g_iValidateHighlightedPedStartingIndex = 0
INT g_iCasinoActivePedTotal = 0

INT g_iCasinoRouletteActivity = 0
INT g_iCasinoRouletteBettingClip = 0
INT g_iCasinoRoulettePedGender = 0
BOOL g_bCasinoRouletteCycleAnims = FALSE
BOOL g_bCasinoRouletteSetInitClip = FALSE
BOOL g_bCasinoRouletteInitAnimPlaying = FALSE
BOOL g_bCasinoRouletteProcessingAnim = FALSE

CONST_INT g_ciMAX_CASINO_AREAS 5
INT g_iCasinoPedLayoutType = 1
INT g_iCasinoPedLayoutArea = 0
INT g_iCasinoPedLayouts[g_ciMAX_CASINO_AREAS]
BOOL g_bSetCasinoPedLayout
BOOL g_bOutputSlotMachineChairCoords
BOOL g_bOutputSlotMachineChairHeading
BOOL g_bOutputInsideTrackChairCoords
BOOL g_bOutputInsideTrackChairHeading
BOOL g_bOutputBlackjackChairCoords
BOOL g_bOutputBlackjackChairHeading
BOOL g_bOutputThreeCardPokerChairCoords
BOOL g_bOutputThreeCardPokerChairHeading
BOOL g_bOutputRouletteChairCoords
BOOL g_bOutputRouletteChairHeading
BOOL g_bCreateCasinoChip
BOOL g_bDeleteCasinoChip
VECTOR g_vCasinoChipOffset
OBJECT_INDEX g_objCasinoChipDebug

INT g_iRoulettePreset = 0
BOOL g_bSetRoulettePreset = FALSE

INT g_iBlackjackPreset = 0
BOOL g_bSetBlackjackPreset = FALSE

BOOL g_bBroadcast_casino_peds_speech_data = FALSE
INT g_iBroadcast_casino_peds_speech_enum = 0
INT g_iBroadcast_casino_peds_speech_ped = 0

BOOL g_bCasinoInsideTrackRaceUpComing[3]
BOOL g_bCasinoInsideTrackRaceInProgress[3]
BOOL g_bCasinoInsideTrackRaceFinished[3]

INT g_iProductionPausedBS
CONST_INT PAUSE_ALL_BUSINESS_HUB_PROD_SLOTS 31


BOOL g_bDebugPlayerHasAccessToPremiumArenaEvent = FALSE

INT g_iDebugArenaWheelSpinMinigameRewardID = -1

BOOL g_iDebugCasinoInsideTrackDisableGameManagement = FALSE
BOOL g_iDebugCasinoInsideTrackDisablePlayerManagement = FALSE
BOOL g_iDebugCasinoInsideTrackMinimizeConsoleDisplay = FALSE

BOOL g_bBlockArcadeCabinetDrawing = FALSE
BOOL g_bMinimizeArcadeCabinetDrawing = FALSE
TWEAK_FLOAT g_fMinimizeArcadeCabinetCentreX			0.2
TWEAK_FLOAT g_fMinimizeArcadeCabinetCentreY			0.8
TWEAK_FLOAT g_fMinimizeArcadeCabinetCentreScale		0.33

#ENDIF		// IS_DEBUG_BUILD

