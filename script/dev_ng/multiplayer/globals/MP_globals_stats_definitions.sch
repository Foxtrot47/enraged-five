

//CONST_INT AWARDS_TOTAL			27		// How many awards are there
CONST_INT AWARDS_LEVEL_CAP		5		// What level can an award get to?

CONST_INT AWARDS_STATE_INIT 	0
CONST_INT AWARDS_STATE_PROCESS 	1
CONST_INT AWARDS_STATE_PAUSE 	2

// KGM 27/8/11: Converting all uses of #defines to #IF.
// NOTE: The CONST must now always be declared, but with a value of 0 to remove the code. Set to 1 to add the code.
CONST_INT AWARDS_ON_RIGHT		0	// Not Active
CONST_INT AWARDS_ON_LEFT		1	// Active
CONST_INT AWARDS_ON_TOP			0	// Not Active

#IF AWARDS_ON_RIGHT
CONST_FLOAT AWARD_X_OFFSET		0.28
#ENDIF
#IF AWARDS_ON_LEFT
CONST_FLOAT AWARD_X_OFFSET		-0.28
#ENDIF
#IF AWARDS_ON_TOP
CONST_FLOAT AWARD_X_OFFSET		0.0
CONST_FLOAT AWARD_Y_OFFSET		-0.2
#ENDIF

CONST_INT AWARD_FLASH_TIME		1500
CONST_INT SLIDE_ON_OFF_TIME		1600
CONST_INT STAY_ON_SCREEN_TIME	3000

CONST_FLOAT JUMP_AWARD_DISTANCE		40.0		// How far does a player have to go to get an award?
CONST_INT JUMP_AWARD_TIME			5		// How long do they have to be in the air to get an award (in secs)?

CONST_INT STUNTMAN_STATE_INIT 		0
CONST_INT STUNTMAN_STATE_PROCESS 	1


CONST_INT NUM_TRACKED_VEHICLES 		5

//ENUM MP_STAT
//	MP_STAT_ROCKETS_FIRED = 0,		
//	MP_STAT_MISSIONS_PASSED,		
//	MP_STAT_ENEMY_GANG,			
//	MP_STAT_ENEMY_COP,		
//	MP_STAT_FIRST_ON_SCENE,
//	MP_STAT_FIRST_LOOT_RECOVERY,
//	MP_STAT_COP_CAR_REQUEST,
//	MP_STAT_ARRESTED_CRIM,
//	MP_STAT_ESCAPED_ARREST,
//	MP_STAT_SNITCH_INFO,			// 10
//	MP_STAT_DRUGS_SOLD,
//	MP_STAT_CARS_DELIVERED,
//	MP_STAT_BRIBE_TAKEN,
//	MP_STAT_STORE_HELD_UP,
//	MP_STAT_PIMPED_HOOKER,
//	MP_STAT_MUGGED_CIVILIAN,
//	MP_STAT_FENCED_ITEM,
//	MP_STAT_SECURITY_VAN_ROBBED,
//	MP_STAT_ENEMIES_JACKED,
//	MP_STAT_SIX_STAR_EVASION,
//	MP_STAT_SIX_STAR_REACHED,
//	MP_STAT_JUMP_DISTANCE,
//	MP_STAT_JUMP_TIME,
//	MP_STAT_GANGWARS_IN,
//	MP_STAT_GANGWARS_WON,
//	MP_STAT_RACES_WON,
//	MP_STAT_ASS_KILLED,
//
//	MP_STAT_MAX_STATS
//ENDENUM


ENUM AWARDPOSITIONS
	AWARDPOSITIONS_GREYED_OUT = 0,
	AWARDPOSITIONS_WHITE,
	AWARDPOSITIONS_BRONZE,
	AWARDPOSITIONS_SILVER,
	AWARDPOSITIONS_GOLD,
	AWARDPOSITIONS_PLATINUM,
	AWARDPOSITIONS_DISPLAY_LAST_AWARD,
	AWARDPOSITIONS_DISPLAY_NEXT_AWARD,
	AWARDPOSITIONS_DISPLAY_NEXT_AWARD_LAST_PICTURE
	
ENDENUM



CONST_INT MAX_NUM_STAT_TIMERS 20
CONST_INT MAX_NUM_AWARD_TIMERS 20
CONST_INT MAX_NUM_AWARD_CHARINT_TIMERS 10
CONST_INT MAX_NUM_AWARD_CHARFLOAT_TIMERS 10
CONST_INT MAX_NUM_AWARD_PLYFLOAT_TIMERS 5
CONST_INT MAX_NUM_MEDALS_OF_EACH_TYPE 10




STRUCT MPGlobalsStatsStruct_OLD

	BOOL HasSlotErrorHitOnce
		
	INT TotalNumberOfMissions 		= 100
	INT TotalNumberOfMissionsPassed = 0
	
	BOOL  bIsMPCharRespondingToIncident[NUM_NETWORK_PLAYERS]
	INT iTotal_response_time_to_incidents
	INT i_time_of_current_wanted_level_session
	int total_accumulated_wanted_level_stars
	INT current_time_hrs_without_wanted_level

//	float missions_fraction_of_100pc = 65.0
	
	VEHICLE_INDEX veh_last_car_track_stat[NUM_TRACKED_VEHICLES]
	BOOL has_respond_time_been_passed_ARRESTWITHIN2MINS[NUM_NETWORK_PLAYERS]
	BOOL bIs_player_on_heist_mission = FALSE
	
	BOOL award_been_in_all_cop_vehicles_POLICE3 = FALSE
	BOOL award_been_in_all_cop_vehicles_POLICEt = FALSE
	BOOL award_been_in_all_cop_vehicles_POLICEb = FALSE
	BOOL award_been_in_all_cop_vehicles_POLMAV = FALSE
	
	
	BOOL bHaveAwardsBeenSetup = FALSE
	
	TIME_DATATYPE  LastStatTimer[MAX_NUM_STAT_TIMERS]
	TIME_DATATYPE  LastAwardTimer[MAX_NUM_AWARD_TIMERS]
	TIME_DATATYPE  LastAward_CharFloat_Timer[MAX_NUM_AWARD_CHARFLOAT_TIMERS]
	TIME_DATATYPE  LastAward_CharInt_Timer[MAX_NUM_AWARD_CHARINT_TIMERS]
	TIME_DATATYPE  LastAward_PlyFloat_Timer[MAX_NUM_AWARD_PLYFLOAT_TIMERS]
	
	MP_BOOL_AWARD bMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MP_INT_AWARD iMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MP_FLOAT_AWARD fMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	
	MPPLY_BOOL_AWARD bPlyMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MPPLY_INT_AWARD iPlyMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MPPLY_FLOAT_AWARD fPlyMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]


	MPGEN_BOOL_AWARD bGenMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MPGEN_INT_AWARD iGenMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MPGEN_FLOAT_AWARD fGenMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]

	
	INT local_multiple_kill = 0

	INT total_kills_recorded1 = 0
	INT high_multiple_kills_timer1 = 0
	

	INT veh_damage = 0
	
	
	BOOL bDisableIdleKick
	BOOL bhas_crew_unlocks_been_initialised = FALSE
	
	INT temp_integer_for_maintain_stat_SMG
	INT temp_integer_for_maintain_stat_ROCKETLAUNCHER
	INT temp_integer_for_maintain_stat_PASSENGERTIME
	INT temp_integer_for_maintain_stat_STICKYBOMB
	INT temp_integer_for_maintain_stat_MELEE
	INT temp_integer_for_maintain_stat_CR_MISSION_SCORE
	INT temp_integer_for_maintain_stat_HEADSHOTS
	INT temp_integer_for_maintain_stat_50_VEHICLES_BLOWNUP
	INT temp_integer_for_maintain_stat_VEHICLES_JACKEDR
	INT temp_integer_for_maintain_stat_MISSIONS_CREATED
	INT temp_integer_for_maintain_stat_FM_CR_MISSION_SCORE
	INT temp_integer_for_maintain_stat_PURCHASE_PISTOL
	INT temp_integer_for_maintain_stat_PURCHASE_SMG
	INT temp_integer_for_maintain_stat_PURCHASE_SNIPER
	INT temp_integer_for_maintain_stat_PURCHASE_SHOTGUN
	INT temp_integer_for_maintain_stat_LAPDANCES
	
ENDSTRUCT


STRUCT MPGlobalsStatsStruct

	BOOL HasSlotErrorHitOnce
		
	INT TotalNumberOfMissions 		= 100
	INT TotalNumberOfMissionsPassed = 0
	
	BOOL  bIsMPCharRespondingToIncident[NUM_NETWORK_PLAYERS]
	INT iTotal_response_time_to_incidents
	INT i_time_of_current_wanted_level_session
	int total_accumulated_wanted_level_stars
	INT current_time_hrs_without_wanted_level

//	float missions_fraction_of_100pc = 65.0
	
	VEHICLE_INDEX veh_last_car_track_stat[NUM_TRACKED_VEHICLES]
	BOOL has_respond_time_been_passed_ARRESTWITHIN2MINS[NUM_NETWORK_PLAYERS]
	BOOL bIs_player_on_heist_mission = FALSE
	
	BOOL award_been_in_all_cop_vehicles_POLICE3 = FALSE
	BOOL award_been_in_all_cop_vehicles_POLICEt = FALSE
	BOOL award_been_in_all_cop_vehicles_POLICEb = FALSE
	BOOL award_been_in_all_cop_vehicles_POLMAV = FALSE
	
	
	BOOL bHaveAwardsBeenSetup = FALSE
	
	TIME_DATATYPE  LastStatTimer[MAX_NUM_STAT_TIMERS]
	TIME_DATATYPE  LastAwardTimer[MAX_NUM_AWARD_TIMERS]
	TIME_DATATYPE  LastAward_CharFloat_Timer[MAX_NUM_AWARD_CHARFLOAT_TIMERS]
	TIME_DATATYPE  LastAward_CharInt_Timer[MAX_NUM_AWARD_CHARINT_TIMERS]
	TIME_DATATYPE  LastAward_PlyFloat_Timer[MAX_NUM_AWARD_PLYFLOAT_TIMERS]
	
	MP_BOOL_AWARD bMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MP_INT_AWARD iMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MP_FLOAT_AWARD fMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	
	MPPLY_BOOL_AWARD bPlyMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MPPLY_INT_AWARD iPlyMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MPPLY_FLOAT_AWARD fPlyMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]


	MPGEN_BOOL_AWARD bGenMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MPGEN_INT_AWARD iGenMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]
	MPGEN_FLOAT_AWARD fGenMedals[MAX_NUM_MEDALS_OF_EACH_TYPE]

	
	INT local_multiple_kill = 0

	INT total_kills_recorded1 = 0
	INT high_multiple_kills_timer1 = 0
	

	INT veh_damage = 0
	
	
	BOOL bDisableIdleKick
	BOOL bDisableIdleKickCutsceneCheck
	BOOL bhas_crew_unlocks_been_initialised = FALSE
	
	INT temp_integer_for_maintain_stat_SMG
	INT temp_integer_for_maintain_stat_ROCKETLAUNCHER
	INT temp_integer_for_maintain_stat_PASSENGERTIME
	INT temp_integer_for_maintain_stat_STICKYBOMB
	INT temp_integer_for_maintain_stat_MELEE
	INT temp_integer_for_maintain_stat_CR_MISSION_SCORE
	INT temp_integer_for_maintain_stat_HEADSHOTS
	INT temp_integer_for_maintain_stat_50_VEHICLES_BLOWNUP
	INT temp_integer_for_maintain_stat_VEHICLES_JACKEDR
	INT temp_integer_for_maintain_stat_MISSIONS_CREATED
	INT temp_integer_for_maintain_stat_FM_CR_MISSION_SCORE
	INT temp_integer_for_maintain_stat_PURCHASE_PISTOL
	INT temp_integer_for_maintain_stat_PURCHASE_SMG
	INT temp_integer_for_maintain_stat_PURCHASE_SNIPER
	INT temp_integer_for_maintain_stat_PURCHASE_SHOTGUN
	INT temp_integer_for_maintain_stat_LAPDANCES
	INT number_of_unique_stunt_jumps_performed
	BOOL b_able_to_check_number_of_unique_stunt_jumps_performed
	BOOL bwantedlevellostphonecall = FALSE
	
	BOOL bDISPLAY_LOW_ON_AMMO_MESSAGE
	
	BOOL bResetSctvStatTimer
	BOOL bResetJobSpecStatTimer
	BOOL bResetJobSpecStatStarted[4]
ENDSTRUCT




CONST_INT MAX_NUM_MP_INT_STATS_OLD		1288
CONST_INT MAX_NUM_MP_FLOAT_STATS_OLD	109
CONST_INT MAX_NUM_MP_BOOL_STATS_OLD		138
CONST_INT MAX_NUM_MP_STRING_STATS_OLD	13
CONST_INT MAX_NUM_MP_DATE_STATS_OLD		10
CONST_INT MAX_NUM_MP_VECTOR_STATS_OLD	18

CONST_INT MAX_NUM_MP_INT_AWARD_OLD		65
CONST_INT MAX_NUM_MP_BOOL_AWARD_OLD		25

