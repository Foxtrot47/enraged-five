
USING "MP_globals_COMMON_CONSTS.sch"

ENUM eOVERHEAD_OVERRIDE_STATE
	eOVERHEADOVERRIDESTATE_INACTIVE = 0,
	eOVERHEADOVERRIDESTATE_FORCE_OFF,
	eOVERHEADOVERRIDESTATE_FORCE_NAME_ON
ENDENUM
STRUCT DISPLAYOVERHEADSTRUCT_TITLEUPDATE
	BOOL bLeaderboardCamActiveCopy[NUM_NETWORK_PLAYERS]
	BOOL bAtArrowNotTagDistance[NUM_NETWORK_PLAYERS]
	BOOL bOnEndRaceScreen[NUM_NETWORK_PLAYERS]
	BOOL bPauseMenuActive[NUM_NETWORK_PLAYERS]
	BOOL bNotInSameInterior[NUM_NETWORK_PLAYERS]
	BOOL bOnImpromptuDm
	PED_INDEX piSpectatorTarget
	BOOL bOnCelebrationScreen
	BOOL bOnLeaderboard
	BOOL bSuppressHudThisFrameCopy
	eOVERHEAD_OVERRIDE_STATE eOverheadOverrideState[2]
	BOOL bOnJobIntro[2]
	BOOL bLaunchedHeist
	BOOL bBlockGlobalOverheadsForJobIntro
	INT iOnHeistBitset
	INT iPlayerTeam[NUM_NETWORK_PLAYERS]
	BOOL bTeamColourOverride
	INT iTeamColourOverride_OverheadsUpdatedBS
	INT iSavedOhColour[NUM_NETWORK_PLAYERS]
	INT iHideHealthBarsBitset[2]
ENDSTRUCT


STRUCT MPGlobalsHudStruct_TITLEUPDATE

	SCRIPT_TIMER stGamertagsTimer
	DPADDOWN_ACTIVATION bGamertagsDpadDownState = DPADDOWN_NONE

	//Overheads
	DISPLAYOVERHEADSTRUCT_TITLEUPDATE DisplayInfo_TitleUpdate
	
	BOOL CAN_DISPLAY_WEAPON_HELP
	
	INT iOhDensitySetting[NUM_NETWORK_PLAYERS]
	BOOL bCoronaToJobTransitionActive

	INT unlock_ticker_timer
	INT unlock_ticker_main_timer
	BOOL display_unlock_tickers
	INT display_unlock_stage
	
	INT iFirstTimeFlashingStage
	SCRIPT_TIMER stFlashTimer
	BOOL bOn
	INT iIdleCountdownBeepsCount
	
	INT iConstrainedCountdownBeepsCount
	
	BOOL bPlayerSwitchActive
	
	//------------------------------------------------
	//New content text
	//------------------------------------------------
	INT iContentCheckTimerMP
	INT iContentCheckStageMP
	BOOL bDisplayContentFeedMessageMP
	
	INT iCheaterTimeLeftTimerSP
	
	MPPLY_BOOL_AWARD_INDEX WhichBoolPLYAward
	INT unlock_delay_heists_ticker_main_timer
	
	INT iAwardStaggerStage
ENDSTRUCT

STRUCT MPGlobalsHudRewardsPLAYERStruct_TU

	INT BoolPlyAwardPlatinumMessageBitset[10]

ENDSTRUCT


BOOL GenericCheckpoint_Cross0[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericCheckpoint_Cross1[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericCheckpoint_Cross2[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericCheckpoint_Cross3[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericCheckpoint_Cross4[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericCheckpoint_Cross5[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericCheckpoint_Cross6[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericCheckpoint_Cross7[MAX_NUMBER_HUD_ELEMENT]

BOOL GenericElimination_Cross0[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericElimination_Cross1[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericElimination_Cross2[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericElimination_Cross3[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericElimination_Cross4[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericElimination_Cross5[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericElimination_Cross6[MAX_NUMBER_HUD_ELEMENT]
BOOL GenericElimination_Cross7[MAX_NUMBER_HUD_ELEMENT]

STRUCT MPGlobalsHudPositionStruct
	//TopLeft Mission display box
	INFOBOXDETAILSSTRUCT TopLeftDisplayBox
	
	FLOAT fMissionCorona_CoronaWidth
	
	GLOBALTICKERDETAIL GlobalXPTickerArray[MAX_NUM_GLOBALTICKER_QUEUE]

	INT AmmoSavingIndex
	
	
	BOOL ForceXPBarOn
	INT WeaponMessageBitset[4]
	INT WeaponAddonMessageBitset[15]
	INT AbilityMessageBitset[3]
	INT KitMessageBitset[11]
	INT CarModMessageBitset[4]
	INT HealthMessageBitset[3]
	INT TATTOOMessageBitset[20]
	INT VEHICLEMessageBitset[2]
	INT CLOTHESMessageBitset[8]
	INT HEISTMessageBitset[2]
	INT iOldXPValue
	INT CREWUNLOCKMessageBitset[2]
	
	BOOL ForceBarOnInCutscene
	
	INT XPBarAlphaLevel
	
	INT iOldScaleformXP
ENDSTRUCT

BOOL gbflatten_awardstruct = FALSE




