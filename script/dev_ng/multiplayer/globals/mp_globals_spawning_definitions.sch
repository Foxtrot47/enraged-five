USING "model_enums.sch"
USING "types.sch"
USING "commands_streaming.sch"
USING "commands_weapon.sch"
USING "commands_camera.sch"
USING "shared_global_definitions.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_spawning_definitions.sch
//      DESCRIPTION     :   Contains constant, enum, and struct definitions for all MP spawning routines.
//							NB. Should not contain instantiated variables.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


CONST_INT SPAWN_AREA_SHAPE_CIRCLE 	0
CONST_INT SPAWN_AREA_SHAPE_BOX		1
CONST_INT SPAWN_AREA_SHAPE_ANGLED	2


CONST_INT MAX_NUMBER_OF_MISSION_SPAWN_AREAS 4
CONST_INT MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS 10 // should be same as FMMC_MAX_NUM_ZONES


CONST_INT MAX_NUM_OF_STORED_SPAWNSEARCH_RESULTS 3

CONST_INT MAX_NUM_OF_STORED_LAST_SPAWN_RESULTS	4


CONST_INT MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS	100
CONST_INT MIN_NUMBER_OF_CUSTOM_SPAWN_POINTS	8
CONST_FLOAT CUSTOM_SPAWN_POINT_MIN_DIST	0.25

CONST_FLOAT FALLBACK_SPAWN_POINT_MIN_DIST	0.2

CONST_INT MAX_NUMBER_OF_CUSTOM_VEHICLE_NODES 	101


CONST_INT MAX_NUMBER_OF_FALLBACK_SPAWN_POINTS	50

CONST_INT QUERY_REPLY_YES 1
CONST_INT QUERY_REPLY_NO  0	

CONST_INT SPECIFIC_SPAWN_QUERY_STATE_IDLE 0
CONST_INT SPECIFIC_SPAWN_QUERY_STATE_WAITING 1
CONST_INT SPECIFIC_SPAWN_QUERY_STATE_FINISHED 2

CONST_INT MAX_NUMBER_OF_GLOBAL_EXCLUSION_AREAS_OLD 4



CONST_INT TOTAL_NUMBER_OF_RESPAWN_HOSPITALS 5

CONST_INT TOTAL_NUMBER_OF_RESPAWN_POLICE_STATIONS 7

CONST_INT TOTAL_NUMBER_OF_RESPAWN_HOTELS 4


CONST_INT TOTAL_PLAYER_SPAWN_POINTS_AT_CREW_LOCATIONS 8
CONST_INT TOTAL_VEHICLE_SPAWN_POINTS_AT_CREW_LOCATIONS 8

TWEAK_INT MAX_NUM_OF_SERVER_WARP_REQUESTS 7
TWEAK_INT MAX_NUM_OF_SERVER_WARP_REQUESTS_VEHICLES 20

CONST_INT MAX_NUM_OF_SERVER_WARP_TIMEOUTS 3
TWEAK_INT MAX_NUM_INCREASING_ATTEMPTS 3

TWEAK_INT MAX_SERVER_WARP_RESPONSE_TIME 3000

CONST_FLOAT SPAWNING_SHAPE_TEST_PROBE_DIST 3.0

CONST_INT NUMBER_OF_RESULTS_STORED_FOR_GETTING_SAFE_COORDS_DEBUG 10

CONST_INT NUM_SERVER_STORED_VEH_REQUESTS 2

//TWEAK_INT INVINCIBLE_FLASH_TIME 5000
//TWEAK_INT INVINCIBLE_FLASH_TIME_VEHICLE_DEATHMATCH 10000

CONST_INT VEHICLE_RESPOT_TIME	3000

CONST_INT DEFAULT_SPAWN_PROTECTION_TIME	2000
CONST_INT MAXIMUM_SPAWN_PROTECTION_TIME 10000

TWEAK_FLOAT SPAWN_CAM_OFFSET_X	0.15 
TWEAK_FLOAT SPAWN_CAM_OFFSET_Y	-1.5
TWEAK_FLOAT SPAWN_CAM_OFFSET_Z	0.8

TWEAK_FLOAT SPAWN_CAM_POINT_OFFSET_X	0.15
TWEAK_FLOAT SPAWN_CAM_POINT_OFFSET_Y	0.0
TWEAK_FLOAT SPAWN_CAM_POINT_OFFSET_Z	0.6

TWEAK_FLOAT SPAWN_CAM_FOV	50.0

TWEAK_FLOAT SPAWN_SHAPETEST_WIDTH 0.2
TWEAK_FLOAT SPAWN_SHAPETEST_MAX_ROAD_DIST 50.0

CONST_FLOAT PLAYABLE_ZONE_MIN_X -8000.0
CONST_FLOAT PLAYABLE_ZONE_MIN_Y -8000.0
CONST_FLOAT PLAYABLE_ZONE_MAX_X 8000.0
CONST_FLOAT PLAYABLE_ZONE_MAX_Y 8000.0

CONST_FLOAT CUSTOM_SPAWN_PED_CLEARANCE_RADIUS 0.7
CONST_FLOAT CUSTOM_SPAWN_VEHICLE_CLEARANCE_RADIUS 6.0
CONST_FLOAT CUSTOM_SPAWN_WORLD_OBJECT_CLEARANCE_RADIUS 1.0
CONST_FLOAT CUSTOM_SPAWN_SCRIPT_OBJECT_CLEARANCE_RADIUS 7.0
CONST_FLOAT CUSTOM_SPAWN_MIN_DIST_WARPER 	5.0
CONST_FLOAT CUSTOM_SPAWN_MIN_DIST_ENEMY 	0.0
CONST_FLOAT CUSTOM_SPAWN_MIN_DIST_TEAMMATE 	1.1
CONST_FLOAT CUSTOM_SPAWN_MAX_DIST_FROM_DEATH 40.0

CONST_INT	CB_VALID_SLOT_CONFIRM			666

CONST_INT NUM_STORED_SERVER_REFUSAL_COORDS 30

CONST_INT MAX_NUM_GROUP_WARP_AREAS 3

TWEAK_INT LANDING_GEAR_FRAME_DELAY 1

CONST_INT MAX_NUM_SPAWN_VEHICLE_ENTITY_AREAS 3

//Spawning Enums
ENUM MP_PLAYER_SPAWN_DECORATION
	SPDEC_NONE = 0,							// As the day they were first born.
	SPDEC_DUFFELBAG,						// Holdall bag used in heists.
	SPDEC_BALLISTIC_ARMOUR,					// Armour used in country heist.
	SPDEC_PARLEY_SNIPER						// Sniper role in Parley mission.
ENDENUM


ENUM PLAYER_SPAWN_LOCATION
	SPAWN_LOCATION_AUTOMATIC = 0,			// system will decide
	SPAWN_LOCATION_NEAR_DEATH,					
	SPAWN_LOCATION_NEAR_TEAM_MATES,			
	SPAWN_LOCATION_MISSION_AREA,			// script defined area
	SPAWN_LOCATION_NEAR_OTHER_PLAYERS, 
	SPAWN_LOCATION_NEAR_CURRENT_POSITION,	
	SPAWN_LOCATION_AT_CURRENT_POSITION,
	SPAWN_LOCATION_NET_TEST_BED,	
	SPAWN_LOCATION_CUSTOM_SPAWN_POINTS,
	SPAWN_LOCATION_OUTSIDE_SIMEON_GARAGE,
	SPAWN_LOCATION_NEAR_SPECIFIC_COORDS,
	SPAWN_LOCATION_AT_SPECIFIC_COORDS,
	SPAWN_LOCATION_AT_AIRPORT_ARRIVALS,
	SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE,
	SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA,
	SPAWN_LOCATION_NEAREST_RESPAWN_POINT, 
	SPAWN_LOCATION_AT_SPECIFIC_COORDS_RACE_CORONA, 
	SPAWN_LOCATION_INSIDE_GARAGE,
	SPAWN_LOCATION_INSIDE_PROPERTY,
	SPAWN_LOCATION_INSIDE_PROPERTY_OR_GARAGE,
	SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU,
	SPAWN_LOCATION_NEAR_CURRENT_POSITION_SPREAD_OUT,
	SPAWN_LOCATION_NEAREST_RESPAWN_POINT_TO_SPECIFIC_COORDS,
	SPAWN_LOCATION_NEAREST_HOSPITAL,
	SPAWN_LOCATION_NEAREST_POLICE_STATION,
	SPAWN_LOCATION_NEAREST_HOTEL_TO_SPECIFIC_COORDS,
	SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION,
	SPAWN_LOCATION_PRIVATE_YACHT,
	SPAWN_LOCATION_PRIVATE_YACHT_APARTMENT,
	SPAWN_LOCATION_PRIVATE_FRIEND_YACHT,
	SPAWN_LOCATION_PRIVATE_YACHT_NEAR_SHORE,
	SPAWN_LOCATION_NEAR_GANG_BOSS,
	SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG,
	SPAWN_LOCATION_GANG_DM,
	SPAWN_LOCATION_GANG_BOSS_PRIVATE_YACHT,
	SPAWN_LOCATION_OFFICE,
	SPAWN_LOCATION_CLUBHOUSE,
	SPAWN_LOCATION_NEAR_CURRENT_POSITION_AS_POSSIBLE,
	SPAWN_LOCATION_NEAR_CURRENT_PERCEIVED_POSITION,
	SPAWN_LOCATION_IE_WAREHOUSE,
	SPAWN_LOCATION_BUNKER,
	SPAWN_LOCATION_HANGAR,
	SPAWN_LOCATION_DEFUNCT_BASE,
	SPAWN_LOCATION_NIGHTCLUB,
	SPAWN_LOCATION_ARENA_GARAGE,
	SPAWN_LOCATION_CASINO,
	SPAWN_LOCATION_CASINO_APARTMENT,
	SPAWN_LOCATION_CASINO_OUTSIDE,
	SPAWN_LOCATION_ARCADE,
	#IF FEATURE_COPS_N_CROOKS
	SPAWN_LOCATION_AFTER_DLC_INTRO_BINK,
	#ENDIF
	SPAWN_LOCATION_CASINO_NIGHTCLUB,
	SPAWN_LOCATION_SUBMARINE,
	SPAWN_LOCATION_HEIST_ISLAND_NEAR_DEATH,
	SPAWN_LOCATION_HEIST_ISLAND_BEACH_PARTY,
	SPAWN_LOCATION_LAND_NEAR_SUBMARINE,
	SPAWN_LOCATION_CAR_MEET,
	SPAWN_LOCATION_AUTO_SHOP,
	SPAWN_LOCATION_FIXER_HQ,
	SPAWN_LOCATION_SITTING_SMOKING,
	SPAWN_LOCATION_DRUNK_WAKE_UP_MUSIC_STUDIO,
	#IF FEATURE_MUSIC_STUDIO
	SPAWN_LOCATION_MUSIC_STUDIO,
	#ENDIF
	#IF FEATURE_GEN9_EXCLUSIVE	
	SPAWN_LOCATION_TUTORIAL_BUSINESS,
	#ENDIF
	#IF FEATURE_DLC_2_2022
	SPAWN_LOCATION_SIMEON_SHOWROOM,
	SPAWN_LOCATION_LUXURY_SHOWROOM,
	#ENDIF
	TOTAL_SPAWN_LOCATIONS
ENDENUM


ENUM PLAYER_SPAWN_ACTIVITY
	SPAWN_ACTIVITY_NOTHING, // 0
	
	// outside only
	SPAWN_ACTIVITY_DRIVING_PERSONAL_VEHICLE, // 1	
	SPAWN_ACTIVITY_WALKING, // 2
	
	// inside players property only
	SPAWN_ACTIVITY_WATCHING_TV, // 3
	SPAWN_ACTIVITY_SHOWER,  // 4
	SPAWN_ACTIVITY_BED,  // 5
	
	// for biker clubhouse
	SPAWN_ACTIVITY_TOILET_SPEW, // 6
	
	SPAWN_ACTIVITY_DRUNK_AWAKEN, // 7 
	
	#IF FEATURE_FIXER
	SPAWN_ACTIVITY_SITTING_SMOKING,  // 8
	SPAWN_ACTIVITY_WAKE_UP_ON_FLOOR_SMOKING,  // 9
	#ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	SPAWN_ACTIVITY_HEIST_DEEPLINK,
	#ENDIF
	
	TOTAL_SPAWN_ACTIVITIES
ENDENUM


ENUM AFTERLIFE
	AFTERLIFE_BRINGUPHUD = 0,
	AFTERLIFE_SET_SPECTATORCAM,
	AFTERLIFE_JUST_REJOIN,
	AFTERLIFE_BRINGUPHUD_AFTER_TIME,
	AFTERLIFE_SET_SPECTATORCAM_AFTER_TIME,
	AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP,
	AFTERLIFE_JUST_REJOIN_AFTER_TIME,
	AFTERLIFE_JUST_REJOIN_INTO_LAST_VEHICLE,
	AFTERLIFE_JUST_AUTO_REJOIN, 
	AFTERLIFE_MANUAL_RESPAWN
	#IF FEATURE_FREEMODE_ARCADE
	, AFTERLIFE_FAST_TRAVEL_RESPAWN
	#ENDIF
	, AFTERLIFE_SPAWN_IN_AS_BIGFOOT
ENDENUM


ENUM MANUAL_RESPAWN_STATE
	MRS_NULL,
	MRS_EXIT_SPECTATOR_AFTERLIFE, 				// set by Rowan in mission when player comes out of spectator mode.
	MRS_RESPAWN_PLAYER_HIDDEN,  				// set by script when want to place player
	MRS_READY_FOR_PLACEMENT,  					// set by Neil when respawn without swoop is complete
	MRS_FINISH_RESPAWN							// set by William after you have placed the player and completed a swoop down
												// then Neil will set it back to state NULL when the respawn has finished
ENDENUM

ENUM GROUP_WARP_LOCATION
	GROUP_WARP_LOCATION_NULL,
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_1,
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_2,
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_3, 
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_4,
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_5,
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_6,
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_7,
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_8,
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_9,
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_10,
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_11,
	GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_12,	
	GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_1,
	GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_2,
	GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_3,
	GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_4,
	GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_6,
	GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_7,
	GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_8,
	GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_9,
	GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_10,
	
	#IF FEATURE_CASINO_HEIST
	GROUP_WARP_LOCATION_OUTSIDE_ARCADE_PALETO_BAY,
	GROUP_WARP_LOCATION_OUTSIDE_ARCADE_GRAPESEED,
	GROUP_WARP_LOCATION_OUTSIDE_ARCADE_DAVIS,
	GROUP_WARP_LOCATION_OUTSIDE_ARCADE_WEST_VINEWOOD,
	GROUP_WARP_LOCATION_OUTSIDE_ARCADE_ROCKFORD_HILLS,
	GROUP_WARP_LOCATION_OUTSIDE_ARCADE_LA_MESA,
	#ENDIF
	
	GROUP_WARP_LOCATION_END
ENDENUM

// bitset for group warp
CONST_INT GWBS_WARP_INTO_VEHICLE		0

CONST_INT GWBS_WARP_ON_FOOT				1





STRUCT PROBLEM_AREA
	VECTOR vCoords1
	VECTOR vCoords2
	FLOAT fFloat
	#IF IS_DEBUG_BUILD
		BOOL bShow
		BOOL bSetAroundPlayer
	#ENDIF	
ENDSTRUCT


STRUCT SPAWN_AREA_OLD
	VECTOR vCoords1
	VECTOR vCoords2
	FLOAT fFloat
	FLOAT fHeading
	BOOL bIsActive
	INT iShape
	BOOL bConsiderCentrePointAsValid
	#IF IS_DEBUG_BUILD
		BOOL bShow
		BOOL bSetAroundPlayer
	#ENDIF
ENDSTRUCT


STRUCT SPAWN_AREA
	VECTOR vCoords1
	VECTOR vCoords2
	FLOAT fFloat
	FLOAT fHeading
	FLOAT fIncreaseDist
	BOOL bIsActive
	INT iShape
	BOOL bConsiderCentrePointAsValid
	#IF IS_DEBUG_BUILD
		BOOL bShow
		BOOL bSetAroundPlayer
	#ENDIF
ENDSTRUCT

STRUCT CUSTOM_VEHICLE_NODE
	VECTOR vPos
	FLOAT fHeading
ENDSTRUCT

STRUCT FALLBACK_SPAWN_POINT
	VECTOR vPos
	FLOAT fHeading
ENDSTRUCT

STRUCT OFF_MISSION_SPAWN_EXCLUSION_AREA_OLD
	SPAWN_AREA_OLD ExclusionArea
	MP_MISSION Mission
	INT iMissionInstance
ENDSTRUCT

STRUCT OFF_MISSION_SPAWN_EXCLUSION_AREA
	SPAWN_AREA ExclusionArea
	MP_MISSION Mission
	INT iMissionInstance
ENDSTRUCT


STRUCT GLOBAL_SPAWN_EXCLUSION_AREA_OLD
	SPAWN_AREA_OLD ExclusionArea
	BOOL bUseSpecificRespawnArea
	VECTOR vSpecificRespawnArea
ENDSTRUCT

STRUCT GLOBAL_SPAWN_EXCLUSION_AREA
	SPAWN_AREA ExclusionArea
	BOOL bUseSpecificRespawnArea
	VECTOR vSpecificRespawnArea
	BOOL bAlwaysConsider
ENDSTRUCT


STRUCT NEXT_SPAWN_LOCATION_INFO
	PLAYER_SPAWN_LOCATION Location = SPAWN_LOCATION_AUTOMATIC
	THREADID Thread_ID
	BOOL bIsPersistant = FALSE
	BOOL bUseRaceCoronaSpacing = FALSE
	BOOL bIsWaterCheckpoint = FALSE
	BOOL bIsAerialCheckpoint = FALSE
ENDSTRUCT


STRUCT MP_SPAWN_POINT
	VECTOR Pos
	FLOAT Heading
ENDSTRUCT


STRUCT MP_SPAWN_RADIUS
	VECTOR Pos
	FLOAT Radius
ENDSTRUCT


STRUCT MP_VEHICLE_SPAWN
	VECTOR Pos
	FLOAT Heading
	MODEL_NAMES Model = DUMMY_MODEL_FOR_SCRIPT
	VEHICLE_GENERATOR_INDEX VehGen
ENDSTRUCT 

//
//STRUCT MISSION_SPAWN_DETAILS_OLD
//	// centre point of the spawn area
//	SPAWN_AREA_OLD SpawnArea[MAX_NUMBER_OF_MISSION_SPAWN_AREAS]
//	
//	// exclusion area
//	SPAWN_AREA_OLD ExclusionArea[MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS]
//
//	// facing coords
//	VECTOR vFacing
//	BOOL bFacePoint
//	BOOL bFaceAwayFromPoint	
//	
//	// spawn in interiors?
//	BOOL bConsiderInteriors
//	
//	INTERIOR_INSTANCE_INDEX DeathInterior
//	INT iDeathInteriorGroup
//	
//	
//	BOOL bDoNearARoadChecks = TRUE
//	
//	// for races, next checpoint info
//	FLOAT fSpecificSpawnLocationOverride = -1.0
//	VECTOR vNextRaceCheckpoint
//
//	BOOL bWillNotRespawnCopsInVehicles
//
//	// mission we are on
//	MP_MISSION Mission
//	
//	BOOL bSpawnInVehicle
//	MODEL_NAMES SpawnModel
//	BOOL bSpawnInLastVehicleIfDriveable
//	VEHICLE_SETUP_STRUCT SpawnVehicleSetup
//	BOOL bSpawnVehicleSetupSaved
//	VEHICLE_INDEX SpawnedVehicle
//	BOOL bDontUseVehicleNodes
//	
//	VECTOR vLastSpawn[MAX_NUM_OF_STORED_LAST_SPAWN_RESULTS]
//	TIME_DATATYPE LastSpawnTime[MAX_NUM_OF_STORED_LAST_SPAWN_RESULTS]	
//	
//
//	PLAYER_SPAWN_LOCATION DefaultSpawnLocation = SPAWN_LOCATION_AUTOMATIC
//	THREADID DefaultSpawnLocationThreadID 
//	
//	VECTOR vSecondaryRaceRespawn[FMMC_MAX_NUM_RACERS]
//	
//	#IF IS_DEBUG_BUILD
//		BOOL bToggleSpawnView
//		BOOL bSetFacePointOnPlayer
//		FLOAT SpawnWindowX = 0.318
//		FLOAT SpawnWindowY = 0.848
//		FLOAT SpawnWindowW = 0.190
//		FLOAT SpawnWindowH = 0.168
//		FLOAT SpawnWindowScale = 0.225
//		FLOAT SpawnWindowColumnX = -0.09
//		FLOAT SpawnWindowRowY = -0.078
//		FLOAT SpawnWindowSpacerX = 0.093
//		FLOAT SpawnWindowSpacerY = 0.015
//	#ENDIF
//ENDSTRUCT

STRUCT MISSION_SPAWN_GENERAL_BEHAVIOURS
	
	PLAYER_SPAWN_LOCATION DefaultSpawnLocation = SPAWN_LOCATION_AUTOMATIC
	THREADID DefaultSpawnLocationThreadID 	

ENDSTRUCT

STRUCT MISSION_SPAWN_PERSONAL_VEHICLE_DATA

	BOOL bDisablePersonalVehicleCreation
	BOOL bDisablePersonalVehicleCreationForMissionStartup
	BOOL bDisablePersonalVehicleCreationForMissionStartup_timerInitialised
	TIME_DATATYPE TimePersonalVehicleCreationForMissionStartup
	BOOL bMissionWaitingForPVCreation
	BOOL bMissionOKToProceedAfterPVCreation
	INT iTimerOKToProceedAfterPVCreation
	INT iPVPropertySpawn
	BOOL bPVIsQuickRestart
	BOOL bHidePersonalVehicles
	
	VECTOR vPersonalVehicleNoSpawnZone_Min
	VECTOR vPersonalVehicleNoSpawnZone_Max
	FLOAT fPersonalVehicleNoSpawnFloat
	INT iPersonalVehicleNoSpawnShape
	
	BOOL bPersonalVehicleNoSpawnZoneEnabled
	
ENDSTRUCT

STRUCT MISSION_SPAWN_EXCLUSION_AREAS

	// exclusion area
	SPAWN_AREA ExclusionArea[MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS]

	//INT iNumberSpawnExclusionAreas

ENDSTRUCT

STRUCT MISSION_SPAWN_DETAILS
	// centre point of the spawn area
	SPAWN_AREA SpawnArea[MAX_NUMBER_OF_MISSION_SPAWN_AREAS]

	// facing coords
	VECTOR vFacing
	BOOL bFacePoint
	BOOL bFaceAwayFromPoint	
	
	BOOL bUseNearCurrentCoordsForSpawnArea
	
	// spawn in interiors?
	BOOL bConsiderInteriors
	
	INTERIOR_INSTANCE_INDEX DeathInterior
	INT iDeathInteriorGroup
	
	
	BOOL bDoNearARoadChecks = TRUE
	
	// for races, next checpoint info
	FLOAT fSpecificSpawnLocationOverride = -1.0
	VECTOR vNextRaceCheckpoint

	BOOL bWillNotRespawnCopsInVehicles

	// mission we are on
	MP_MISSION Mission
	
	BOOL bSpawnInVehicle
	BOOL bUsePersonalVehicleToSpawn
	MODEL_NAMES SpawnModel
	BOOL bSpawnInLastVehicleIfDriveable
	VEHICLE_SETUP_STRUCT_MP SpawnVehicleSetupMP
	BOOL bSpawnVehicleSetupSaved
	VEHICLE_INDEX SpawnedVehicle
	VEHICLE_INDEX LastSpawnedVehicle
	BOOL bDontUseVehicleNodes
	BOOL bSpawningCreatedNewCar
	BOOL bSetVehicleStrong
	BOOL bSetTyresBulletProof
	BOOL bSetAsNonSaveable
	BOOL bSetAsNonModable
	BOOL bSetFireAfterGangBossMissionEnds
	BOOL bCanFaceOncomingTraffic
	INT iColour = -1
	
	VECTOR vLastSpawn[MAX_NUM_OF_STORED_LAST_SPAWN_RESULTS]
	TIME_DATATYPE LastSpawnTime[MAX_NUM_OF_STORED_LAST_SPAWN_RESULTS]	
	
	
	VECTOR vSecondaryRaceRespawn[FMMC_MAX_NUM_RACERS]		
	
	VECTOR vRaceRespawnPos
	BOOL bJustRespawnedInRace
	BOOL bUseRaceRespotAfterRespawns
	
	BOOL bAbortSpawnInVehicle
	
	VEHICLE_KNOCKOFF VehicleKnockOffState = KNOCKOFFVEHICLE_DEFAULT

	INT iDiedInMissionSpawnAreaCount
	

	FLOAT fMinDistFromEnemyNearDeath = -1.0
	FLOAT fMinDistFromDeathNearDeath = 25.0	
	
	BOOL bUseOffRoadChecking = FALSE
	BOOL bUseNavMeshFallback = TRUE
	
	FLOAT fCarNodeLowerZLimit = 0.0
	
	// visibility checks
	BOOL bDoVisibilityChecks = TRUE
	
	VECTOR vAvoidCoords
	FLOAT fAvoidRadius
	
	BOOL bGlobalExclusionIsIgnored=FALSE
	
	TIME_DATATYPE TimerRaceRespot
	
	BOOL bDisablePropertyCustomNodes = FALSE
	
	BOOL bDoForcedRespawn = FALSE
	TIME_DATATYPE timerForcedRespawn	//Timer to be started following the completion of the forced respawn
	BOOL bForcedRespawnTimerInitialised = FALSE
	
	#IF IS_DEBUG_BUILD
		BOOL bToggleSpawnView
		BOOL bSetFacePointOnPlayer
		FLOAT SpawnWindowX = 0.318
		FLOAT SpawnWindowY = 0.848
		FLOAT SpawnWindowW = 0.190
		FLOAT SpawnWindowH = 0.168
		FLOAT SpawnWindowScale = 0.225
		FLOAT SpawnWindowColumnX = -0.09
		FLOAT SpawnWindowRowY = -0.078
		FLOAT SpawnWindowSpacerX = 0.093
		FLOAT SpawnWindowSpacerY = 0.015
	#ENDIF
ENDSTRUCT


STRUCT CUSTOM_SPAWN_POINT
	VECTOR vPos
	FLOAT fHeading
	FLOAT fWeighting = 1.0
ENDSTRUCT


STRUCT SPAWN_SEARCH_SCORE_DATA
	INT iFallbackScore
	FLOAT fScore = 0.0
	FLOAT fDistToMissionArea = 9999.9
	FLOAT fDistScoreFromPlayers = 0.0
	VECTOR vFallbackCoords
	FLOAT fFallbackHeading
	FLOAT fMinDistToHostile
	FLOAT fNearestEnemy
ENDSTRUCT


CONST_INT NUM_OF_STORED_SPAWN_RESULTS 5
CONST_INT NUM_OF_STORED_NEAREST_CORONAS	8
CONST_INT NUM_OF_STORED_NEAREST_GANG_ATTACK 4
CONST_INT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS 128

STRUCT SPAWN_SEARCH_CORONA
	VECTOR vCoords
	FLOAT fRadius
ENDSTRUCT


STRUCT SPAWN_SEARCH_GANG_ATTACK
	VECTOR vMin
	VECTOR vMax
	FLOAT fWidth
	VECTOR vBlip
ENDSTRUCT
	

STRUCT SPAWN_SEARCH_VARIABLES_DATA
	// spawn search variables
	INT iNumOfFallbackSpawnPointResults
	INT iNumOfResults
	BOOL bGotFallback
	INT iLastResultProcessed = -1
	BOOL bHasConsideredRaw
	BOOL bIsFallbackSearch
ENDSTRUCT
	

STRUCT SPAWN_SEARCH_ANGLED_AREAS_OLD
	VECTOR vMin
	VECTOR vMax
	FLOAT fWidth
ENDSTRUCT

STRUCT SPAWN_SEARCH_VARIABLES_OLD
	// spawn search variables
	INT iNumOfResults
	//INT iHighestFallbackScore
	BOOL bGotFallback
	//VECTOR vFallbackCoords
	//FLOAT fFallbackHeading	
	//FLOAT fDistToMissionArea = 9999.9	
	INT iLastResultProcessed
	//FLOAT fDistScoreFromPlayers = 0.0
	//FLOAT fScore = 0.0
	BOOL bHasConsideredRaw
	BOOL bIsFallbackSearch
	
	// new struct for storing top 3 result
	SPAWN_SEARCH_SCORE_DATA TopResults[NUM_OF_STORED_SPAWN_RESULTS]
	
	//g_structMissionsAtCoordsMP NearestCoronas[NUM_OF_STORED_NEAREST_CORONAS]
	
	//g_structMatCAngledAreaMP NearestGangAttack[NUM_OF_STORED_NEAREST_GANG_ATTACK]
	
	SPAWN_SEARCH_CORONA NearestCoronas[NUM_OF_STORED_NEAREST_CORONAS]
	SPAWN_SEARCH_ANGLED_AREAS_OLD NearestGangAttack[NUM_OF_STORED_NEAREST_GANG_ATTACK]
	
ENDSTRUCT
	
STRUCT SPAWN_SEARCH_VARIABLES
	SPAWN_SEARCH_VARIABLES_DATA Data
	
	// new struct for storing top 3 result
	SPAWN_SEARCH_SCORE_DATA TopResults[NUM_OF_STORED_SPAWN_RESULTS]
	SPAWN_SEARCH_CORONA NearestCoronas[NUM_OF_STORED_NEAREST_CORONAS]
	SPAWN_SEARCH_GANG_ATTACK NearestGangAttack[NUM_OF_STORED_NEAREST_GANG_ATTACK]	
	
	//SPAWN_SEARCH_SCORE_DATA UnSortedTopResults[NUM_OF_STORED_UNSORTED_SPAWN_RESULTS]
ENDSTRUCT


STRUCT SPAWN_RESULTS_OLD
	VECTOR vCoords[NUM_OF_STORED_SPAWN_RESULTS]
	FLOAT fHeading[NUM_OF_STORED_SPAWN_RESULTS]
	//INT iScore[NUM_OF_STORED_SPAWN_RESULTS]
ENDSTRUCT


STRUCT SPAWN_RESULTS_LIGHT_OLD
	VECTOR vCoords[NUM_OF_STORED_SPAWN_RESULTS]
	//INT iScore[NUM_OF_STORED_SPAWN_RESULTS]
ENDSTRUCT

STRUCT SPAWN_RESULTS
	VECTOR vCoords[NUM_OF_STORED_SPAWN_RESULTS]
	FLOAT fHeading[NUM_OF_STORED_SPAWN_RESULTS]
	INT iScore[NUM_OF_STORED_SPAWN_RESULTS]
	FLOAT fNearestEnemy[NUM_OF_STORED_SPAWN_RESULTS]
ENDSTRUCT


//STRUCT SPAWN_RESULTS_LIGHT
//	VECTOR vCoords[NUM_OF_STORED_SPAWN_RESULTS]
//	INT iScore[NUM_OF_STORED_SPAWN_RESULTS]
//	FLOAT fMinDistToHostile[NUM_OF_STORED_SPAWN_RESULTS]
//ENDSTRUCT


STRUCT RESET_ON_SPAWN
	BOOL bHasSetVehicleWeapon
	BOOL bHasSetRadio
	BOOL bHasSetStealthMode
ENDSTRUCT


//STRUCT SPAWN_INFO_OLD
//	#IF IS_DEBUG_BUILD
//
//		BOOL bLeavePedBehind
//		BOOL bTestWarp
//		BOOL bDoQuickWarp
//		BOOL bWarpIntoCar
//		BOOL bUseLastCarIfDriveable
//		INT iSpawnVehicleModel
//		
//		//BOOL bKeepOriginalVehicle
//		
//		// vehicle spawning
//		BOOL bUpdate_SET_PLAYER_RESPAWN_IN_VEHICLE
//		BOOL bRespawnInVehicle
//		
//		BOOL bGetSpecificCoordsFromPlayerPos
//	
//		BOOL bSetPlayerAttributes
//
//		INT iSpawnLocation 
//		INT iNextSpawnLocation 
//	
//		VECTOR vTestCoordInput
//		VECTOR vTestCoordOutput
//		BOOL bGetSafeCoord
//		//BOOL bDoFinalDistanceCheckTest
//		//BOOL bUseNonPavementCheckTest
//		
//		BOOL bDebugLinesActive
//	
//		BOOL bShowCustomSpawnPoints
//		
//		BOOL  bGenerateRandomCustomSpawnPoints
//		FLOAT fGenerateRandomCustomSpawnPointsDist = 10.0
//		
//		BOOL bShowViewingCone
//		
//		THREADID CustomSpawnThreadID
//		
//		
//		BOOL DecisionPrint_Done
//		INT iEuler = 3
//		INT iRecRoomPosition = -1
//		
//		//BOOL bUseFOVFlag
//		
//		
//		// for testing GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY
//		BOOL bTestSafeCoordsInArea
//		
//		INT iSafeFriendlyTeam
//		VECTOR vSafeFacing
//		VECTOR vSafeCoordsOut
//		
//		FLOAT fSafeHeadingOut
//		FLOAT fSafeMinDistToPlayer
//		
//		BOOL bSafeConsiderInteriors
//		BOOL bSafePreferCloserToRoads
//		BOOL bSafeCloseToOriginAsPossible
//		BOOL bSafeConsiderOriginAsPoint
//		BOOL bSafeVehicleNodesOnly
//		BOOL bSafeBoatNodesOnly
//		BOOL bConsiderRoadPolys
//		BOOL bSafeEdgesOnly
//		
//		INT iSafeCoordsShape
//		VECTOR vSafeCoordsPoint1
//		VECTOR vSafeCoordsPoint2
//		FLOAT fSafeCoordsFloat
//		
//		INT iAlphaValue = 64
//		FLOAT fFaceOffset = 0.1
//		
//		TIME_DATATYPE iWarpStartTime
//		
////		BOOL bShowAllSafeAir
////		BOOL bOutputAllSafeAir
////		INT iWarpToSafeAir = -1
////		BOOL bGrabAirGroundZ
////		INT iGrabAirGroundZ
//		
//		BOOL bShowAllSafeSea
//		BOOL bOutputAllSafeSea
//		INT iWarpToSafeSea = -1
//		
//		BOOL bRecordSeaPoints
//		INT iRecordedSeaPoint
//		VECTOR vRecordedSeaPoint
//		INT iRecordingSeaDist = 800
//		
//		BOOL bShowAdvancedSpew
//		
//		BOOL bBlurForSpawn
//		BOOL bClearBlurForSpawn
//		
//		BOOL bClearAllRespawnPickups	
//		BOOL bRecreateAllRespawnPickups	
//	
//		BOOL bShowAllHospital
//		BOOL bShowAllPoliceStation
//		BOOL bShowAllHotels
//		
//		SPAWN_AREA LastGetSafeCoordsForCreatingEntity
//		MP_SPAWN_POINT LastCoordsReturnedForGetSafeCoords[NUMBER_OF_RESULTS_STORED_FOR_GETTING_SAFE_COORDS_DEBUG]
//		
//		VECTOR vSpawnVehicleCoords
//		FLOAT fSpawnVehicleHeading
//		BOOL bTestSpawnVehicle
//		VECTOR vSpawnVehicleStartCoords
//		BOOL bUsePlayerPositionForSpawnVehicle
//		BOOL bRenderSpawnVehicleResult
//		BOOL bSpawnVehicleUseRoadOffset
//		FLOAT fSpawnVehicleMinDistFromCoords
//		BOOL bSpawnVehicleConsiderHighways
//		BOOL bSpawnVehicleInExactCoordsIfPossible
//		FLOAT fSpawnVehicleMaxRange
//		
//	#ENDIF
//	
//	MISSION_SPAWN_DETAILS_OLD MissionSpawnDetails
//	PLAYER_SPAWN_LOCATION SpawnLocation
//	
//	NEXT_SPAWN_LOCATION_INFO NextSpawn
//	
//	WEAPON_TYPE VehicleWeapon
//	
//	// Candidate spawn point and heading
//	VECTOR vCandSpawnPoint
//	FLOAT fCandSpawnHeading
//	BOOL bSpawnLocHadFinalCheck
//	
//	BOOL bSpawnPersonalVehicle
//	BOOL bRespawnSearchStarted
//	
//	VECTOR vMyDeadCoords
//	
//	// stuff for getting location
//	VECTOR vRawCoords
//	FLOAT fRawHeading
//	FLOAT fSearchRadius
//	
//	BOOL bUseAngleAreas
//	VECTOR vAngledCoords1
//	VECTOR vAngledCoords2
//	FLOAT fAngledWidth
//	BOOL bConsiderRawAsValidPoint
//	
//	INT iGetLocationState
//	INT iGetLocationAttempts
//	THREADID GetLocationThreadID
//	//STREAMVOL_ID StreamVolID
//	TIME_DATATYPE iGetLocationTime
//	
//	VECTOR vExclusionCoord
//	FLOAT fNearExclusionRadius
//	
//	VECTOR vSpawnCoords
//	FLOAT fSpawnHeading
//
//	//INT iRespawnState // moved to global player bd
//	TIME_DATATYPE iLoadSceneTimer
//	TIME_DATATYPE InvincibleStartTime
//	INT iExtendedSpawnTimer
//	MP_PLAYER_SPAWN_DECORATION eDecoration
//	FLOAT fCamHeading = 0.0
//	BOOL bDontBringUpHudWhenDead
//	INT iWarpState
//	INT iWarpServerRequestCount
//	TIME_DATATYPE iWarpRequestTime
//	TIME_DATATYPE iWarpRequestSendTime
//	TIME_DATATYPE iHasGotSpawnLocationUpdateTime
//	THREADID WarpRequestThreadID
//	//VECTOR vRequestedWarpCoords
//	//FLOAT fRequestedWarpHeading
//	
//	SPAWN_RESULTS_OLD RequestedSpawnResults
//	
//	BOOL bServerRepliedToWarpRequest
//	BOOL bServerAgreedToWarpRequest
//	INT iServerSpawnResultsSlot
//	
//	BOOL bEnableUseOfSpawnNodes
//	BOOL bForceRespawn
//	BOOL bForceThroughJoining
//	BOOL bFallbackUsed
//	BOOL bFlashingInvincible
//	INT iFallbackAttempts
//	BOOL bDoLoadScene
//	
//	// spawn vehicle stuff
//	BOOL bServerRepliedToSpawnVehicleRequest
//	BOOL bServerAgreedToSpawnVehicleRequest	
//	THREADID SpawnVehicleRequestThreadID
//	INT iSpawnVehicleState
//	TIME_DATATYPE iSpawnVehicleRequestTime
//	TIME_DATATYPE iSpawnVehicleRequestSendTime
//	TIME_DATATYPE iHasGotVehicleSpawnLocationUpdateTime
//	INT iSpawnVehicleServerRequestCount
//	VECTOR vRequestedSpawnVehicleCoords
//	FLOAT fRequestedSpawnVehicleHeading
//	
//	// bools used to refine points
//	BOOL bMoveOutOfOtherGangZone 
//	BOOL bIgnoreExclusionCheck 
//	BOOL bNearAsPossibleToPassedInCoords
//	BOOL bDoNearARoadChecks
//	BOOL bConsiderInteriors
//	VECTOr vFacing
//	
//	
//	//THREADID WarpToLocationThread
//	//INT iWarpToLocationTime
//	PLAYER_SPAWN_LOCATION WarpToSpawnLocation
//	//INT iWarpToLocationState // moved to global player bd
//	TIME_DATATYPE iWarpToLocationTime
//	TIME_DATATYPE iWarpIntoVehicleTime
//	VECTOR vWarpToLocationCoords
//	FLOAT fWarpToLocationHeading
//	//BOOL bHasWarpedIntoVehicle
//	
//	// visibility check
//	BOOL bDoVisibilityChecksOnTeammates	= TRUE
//	
//	
//	// custom spawn points
//	BOOL bUseCustomSpawnPoints
//	INT iNumberOfCustomSpawnPoints
//	CUSTOM_SPAWN_POINT CustomSpawnPoints[MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS]
//	INT iLastCustomSpawnPoint = -1
//	INT iLastUsedCustomSpawnPoint = -1
//	BOOL bIgnorePreviousSpawnPoint = TRUE
//	#IF IS_DEBUG_BUILD
// 	TIME_DATATYPE LastAddCustomSpawnTime
//	TEXT_LABEL_63 ScriptUsingCustomSpawnPoints
//	#ENDIF
//	
//	// respawn search variables
//	BOOL bRespawnSearch_HasStarted
//	INT iRespawnSearch_State
//	VECTOR vRespawnSearch_CurrentSearch
//	FLOAT fRespawnSearch_CurrentRadius
//	TIME_DATATYPE iRespawnSearch_LastUpdateTime
//	TIME_DATATYPE iRespawnSearch_Time	
//	BOOL bNavMeshRequested
//	THREADID RespawnSearchThreadID
//	THREADID NavMeshRequestedThreadID
//	TIME_DATATYPE iNavMeshLoadTime
//	
//	// for querying the start spawn
//	TIME_DATATYPE iQuerySpecificSpawnTime
//	INT iQuerySpecificSpawnState
//	BOOL bHasQueriedSpecificPosition
//	INT iQuestSpecificSpawnReply
//	
//	
//	VECTOR vRespawnSearch_CurrentSearch_AngledArea_Point1
//	VECTOR vRespawnSearch_CurrentSearch_AngledArea_Point2
//	FLOAT fRespawnSearch_CurrentRadius_AngledArea_Width
//	BOOL bRespawnSearch_IsAngled
//	
//	BOOL bBothPartnersAreDead
//	INT iStoredBSForPartners
//	
//	BOOL bTellHUDNotToMovePlayer
//	
//	SPAWN_SEARCH_VARIABLES_OLD SearchTempData
//	
//	// JA added for bug 847162
//	TIME_DATATYPE killStripTimer
//	BOOL bPlayerWantsToRespawn
//	
//	BOOL bSafeForSpectatorCam
//	
//	VECTOR vSpawnSearchResults[MAX_NUM_OF_STORED_SPAWNSEARCH_RESULTS]
//
//
//	BOOL bSpawnTimerInitialised
//	TIME_DATATYPE SpawnTimer
//	TIME_DATATYPE GameStateTimer
//	BOOL bHasSpectatorCamBeenActive
//	AFTERLIFE AfterlifeValueOnDeath	
//
//	BOOL bIsBlurredOut
//	
//	BOOL bIsSwoopedUp
//	
//	BOOL bInitialisedCollectedRespawnPickupData
//	BOOL bRespawnPickupsCreated
//	INT iBS_RespawnPickupHelp
//	INT i_RespawnPickupStagger
//	INT iCreationPickupsProcessed
//	INT iRespawnPickupsCreated
//
//	
//	INT iSpawnNearbySavedState = 0
//	VECTOR vSavedVehicleCoords
//	FLOAT fSavedVehicleHeading
//	
//	
//	
//	VEHICLE_INDEX MyLastVehicle
//	INT iBS_InCarWith
//	
//	VEHICLE_INDEX VehicleToDelete
//	
//	//VECTOR vStartSwoop
//	//VECTOR vApproxEndSwoop
//	//SWITCH_TYPE eSwitchTypeStart
//	//SWITCH_TYPE eSwitchTypeEnd
//	
//	VECTOR vBeforeRespawnCoords
//	
//	BOOL bDoShapeTest
//	MANUAL_RESPAWN_STATE eManualRespawnState
//	
//	BOOL bDisableFadeInAndTurnOnControls
//	
//	BOOL bSpawningInProperty
//	BOOL bSpawningInGarage
//	
//	BOOL bPassedOutDrunk
//	BOOL bPassedOutDrunkWithWantedLevel
//	
//	INT iRadioStation = -1
//	
//	RESET_ON_SPAWN ResetOnSpawn
//	
//	BOOL bSpawnedInHospital = FALSE
//	
//	
//ENDSTRUCT

CONST_INT MAX_VEHICLE_NODES_CHECKED	40

STRUCT NEAREST_CAR_NODE_RESULTS
	VECTOR vCoords[MAX_VEHICLE_NODES_CHECKED]
	FLOAT fHeading[MAX_VEHICLE_NODES_CHECKED]
	INT iNumResults
	INT iNumResultsInsideArea
	INT iLastVehicleNode
	VECTOR vLastVehicleNodePos
ENDSTRUCT

CONST_INT MAX_NUM_AVOID_RADIUS 2

STRUCT SPAWN_SEARCH_PARAMS
	VECTOR vFacingCoords 
	BOOL bConsiderInteriors = FALSE
	BOOL bPreferPointsCloserToRoads=FALSE
	FLOAT fMinDistFromPlayer=65.0
	BOOL bCloseToOriginAsPossible=FALSE
	BOOL bConsiderOriginAsValidPoint = FALSE
	BOOL bSearchVehicleNodesOnly=FALSE
	BOOL bUseOnlyBoatNodes=FALSE
	BOOL bEdgesOnly = FALSE
	FLOAT fHeadingForConsideredOrigin=0.0
	FLOAT fMaxZBelowRaw=0.0 
	VECTOR vAvoidCoords[MAX_NUM_AVOID_RADIUS]
	FLOAT fAvoidRadius[MAX_NUM_AVOID_RADIUS]
	VECTOR vAvoidAngledAreaPos1
	VECTOR vAvoidAngledAreaPos2
	FLOAT fAvoidAngledAreaWidth
	BOOL bIsForFlyingVehicle=FALSE
	BOOL bUseOffRoadChecking=FALSE
	FLOAT fUpperZLimitForNodes=-1.0
	BOOL bAvoidOtherPeds=FALSE
	BOOL bAvoidOtherVehicles=TRUE
ENDSTRUCT

STRUCT PRIVATE_SPAWN_SEARCH_PARAMS

	VECTOR vSearchCoord
	FLOAT fRawHeading
	FLOAT fSearchRadius
	BOOL bIsForLocalPlayerSpawning
	BOOL bDoTeamMateVisCheck
	//BOOL bIsAngledArea
	INT iAreaShape
	VECTOR vAngledAreaPoint1
	VECTOR vAngledAreaPoint2
	FLOAT fAngledAreaWidth
	BOOL bForAVehicle
	BOOL bDoVisibleChecks = TRUE
	BOOL bIgnoreTeammatesForMinDistCheck
	VECTOR vOffsetOrigin
	BOOL bUseOffsetOrigin
	BOOL bAllowFallbackToUseInteriors = TRUE
	BOOL bAllowFallbackToUseInactiveNodes = TRUE
	BOOL bAllowFallbackToUseNavMesh = TRUE
	BOOL bFoundACarNode = FALSE
	BOOL bVehiclesCanConsiderInactiveNodes = FALSE
	FLOAT fPitch=0.0
	INT iFallbackSpawnPointsDefaultResultFlag = -1
	BOOL bAllowFace
	BOOL bCanFaceOncomingTraffic = FALSE
	
ENDSTRUCT

STRUCT NearestCarNodeSearch
	VECTOR vFavourFacing 

	FLOAT fMinPlayerDist=0.0
	FLOAT fMinDistFromCoords=0.0	
	FLOAT fMaxDistance=0.0
	FLOAT fUpperZLimit=4.0
	FLOAT fLowerZLimit=9999.0
	
	BOOL bDoSafeForSpawnCheck=TRUE
	BOOL bGetSafeOffset=FALSE	
	BOOL bConsiderOnlyActiveNodes=TRUE
	BOOL bWaterNodesOnly=FALSE 	
	BOOL bCheckVehicleSpawning=FALSE
	BOOL bConsiderHighways=TRUE 
	BOOL bConsiderTunnels=FALSE 	
	BOOL bCheckSpawnExclusionZones=TRUE
	BOOL bAllowFallbackToInactiveNodes=TRUE
	
	BOOL bStartOfMissionPVSpawn=FALSE
	
	BOOL bCheckInsideArea
	VECTOR vAreaCoords1
	VECTOR vAreaCoords2
	FLOAT fAreaFloat
	INT iAreaShape
	
	BOOL bCouldNotFindWaterNode
	
	BOOL bAllowReducingVisibility
	BOOL bReturnRandomGoodNode
	BOOL bReturnNearestGoodNode
	
	BOOL bCheckOwnVisibility = TRUE
	BOOL bUsingOffsetOrigin = FALSE
	
	INT iFallbackLevel=0 
	
	MODEL_NAMES CarModel=TAILGATER // fairly generic sized vehicle
	
	VECTOR vStartCoords
	
	VECTOR vAvoidCoords[MAX_NUM_AVOID_RADIUS]
	FLOAT fAvoidRadius[MAX_NUM_AVOID_RADIUS]
	
	BOOL bCheckPVNoSpawnZone
	FLOAT fVisibleDistance = 120.0
	
	BOOL bDoneMoveFallback
	BOOL bIsForFlyingVehicle
	
	BOOL bCheckForLowestEnemiesNearPoint = FALSE
	INT iLowestNumEnemiesNearPoint = 999
	FLOAT fCheckForLowestnemiesMaxDist = 9999.9 

	BOOL bDoVisibilityChecks = TRUE
	BOOL bCheckThisPlayerVehicle = TRUE
	BOOL bEnforceMinDistCheckForCustomNodes = TRUE
	
	BOOL bIgnoreLocalPlayerChecks = FALSE
	
	BOOL bUseCustomNodesOnly = FALSE
	
	BOOL bCanFaceOncomingTraffic = FALSE

ENDSTRUCT

STRUCT CUSTOM_SPAWN_POINT_INFO
	// custom spawn points
	INT iNumberOfCustomSpawnPoints
	CUSTOM_SPAWN_POINT CustomSpawnPoints[MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS]
	INT iLastCustomSpawnPoint = -1
	INT iLastUsedCustomSpawnPoint = -1
	BOOL bIgnorePreviousSpawnPoint = TRUE
	BOOL bIgnoreObstructionChecks = FALSE
	BOOL bIgnoreDistanceChecks = FALSE
	BOOL bNeverFallBackToNavMesh = FALSE
	BOOL bAvoidHostileNPCs = TRUE
	BOOL bAvoidResultClumping = FALSE
	FLOAT fCustomSpawnWorldObjectClearance = CUSTOM_SPAWN_WORLD_OBJECT_CLEARANCE_RADIUS
	FLOAT fCustomSpawnScriptObjectClearance = CUSTOM_SPAWN_SCRIPT_OBJECT_CLEARANCE_RADIUS
	FLOAT fCustomSpawnPedClearance = CUSTOM_SPAWN_PED_CLEARANCE_RADIUS
	FLOAT fCustomSpawnVehicleClearance = CUSTOM_SPAWN_VEHICLE_CLEARANCE_RADIUS
	FLOAT fCustomSpawnMinDistWarper = CUSTOM_SPAWN_MIN_DIST_WARPER
	FLOAT fCustomSpawnMinDistEnemy = CUSTOM_SPAWN_MIN_DIST_ENEMY
	FLOAT fCustomSpawnMinDistTeammate = CUSTOM_SPAWN_MIN_DIST_TEAMMATE
	FLOAT fCustomMaxDistFromDeath = CUSTOM_SPAWN_MAX_DIST_FROM_DEATH
	THREADID CustomSpawnThreadID
	#IF IS_DEBUG_BUILD
 		TIME_DATATYPE LastAddCustomSpawnTime
		TEXT_LABEL_63 ScriptUsingCustomSpawnPoints	
	#ENDIF
ENDSTRUCT

STRUCT VEH_SPAWN_REQUEST_INFO
	VECTOR vPos
	FLOAT fHeading
	MODEL_NAMES ModelName
	VEHICLE_INDEX VehicleID
ENDSTRUCT

STRUCT SPAWN_INFO
	#IF IS_DEBUG_BUILD

		BOOL bLeavePedBehind
		BOOL bTestWarp
		BOOL bDoQuickWarp
		BOOL bWarpIntoCar
		BOOL bUseLastCarIfDriveable
		BOOL bDontUseVehicleNodes
		BOOL bUsePersonalVehicleIfPossible
		BOOL bSetVehicleStrong = FALSE
		INT iColour = -1
		BOOL bSetTyresBulletProof = FALSE
		BOOL bSetAsNonSaveable=FALSE
		BOOL bSetAsNonModable = FALSE
		BOOL bSetFireAfterGangBossMissionEnds = FALSE	
		
		
		INT iSpawnVehicleModel
		
		BOOL bDontClearMissionSpawnPersonalVehicleData
		
		//BOOL bKeepOriginalVehicle
		
		// vehicle spawning
		BOOL bUpdate_SET_PLAYER_RESPAWN_IN_VEHICLE
		BOOL bRespawnInVehicle
		
		BOOL bGetSpecificCoordsFromPlayerPos
	

		INT iSpawnLocation 
		INT iNextSpawnLocation 
	
		VECTOR vTestCoordInput
		VECTOR vTestCoordOutput
		BOOL bGetSafeCoord
		//BOOL bDoFinalDistanceCheckTest
		//BOOL bUseNonPavementCheckTest
		
		BOOL bDebugLinesActive
		BOOL bDrawVectorMap
	
		BOOL bShowCustomSpawnPoints
		BOOL bShowCustomSpawnPointLabels = TRUE
		BOOL bClearCustomSpawnPoints
		INT iCustomSpawnPointsToGenerate = 16
		BOOL bCustomSpawnPointsIgnoreDistanceCheck = TRUE
		BOOL bCustomSpawnPointsOutputCoords
		BOOL bCustomSpawnPointOutputProperty
		BOOL bCustomSpawnPointOutputPostDeliveryScene
		BOOL bCustomSpawnPointOutputSimpleInterior
		BOOL bCustomSpawnPointOutputSimpleInteriorINT
		
		BOOL  bGenerateRandomCustomSpawnPoints_Circle
		BOOL  bGenerateRandomCustomSpawnPoints_Cluster
		BOOL  bGenerateRandomCustomSpawnPoints_Grid
		FLOAT fGenerateRandomCustomSpawnPointsDist = 1.0
		INT iGenerateRandomCustomSpawnPointsGridWidth = 5
		INT iGenerateRandomCustomSpawnPointsGridAlignment = 0
		FLOAT fGenerateRandomCustomSpawnPointsGridHeading = 0.0
		FLOAT fGenerateRandomCustomSpawnPointsGridHeading_Previous = 0.0
		BOOL bGenerateSingleCustomSpawnPoint
		INT iSelectedSpawnPointIndex = 0
		BOOL bAutoincrementGenerateSinglePointIndex = TRUE
		BOOL bAllowPointsMousePicking = TRUE
		BOOL bCurrentlyDraggingSpawnPoint
		BOOL bSwapCustomSpawnPoints
		INT iSpawnPointToSwapWith
		BOOL bDeleteSelectedSpawnPoint
		
		BOOL bShowViewingCone
		
		
		BOOL DecisionPrint_Done
		INT iEuler = 3
		INT iRecRoomPosition = -1
		
		//BOOL bUseFOVFlag
		
		
		// for testing GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY
		BOOL bTestSafeCoordsInArea
		BOOL bCreateMissionPedWithSafeCoords
		NETWORK_INDEX NetIdDebug
		
		SPAWN_SEARCH_PARAMS SafeSpawnSearchParams
		
		VECTOR vSafeCoordsOut
		FLOAT fSafeHeadingOut
		INT iSafeCoordsShape
		VECTOR vSafeCoordsPoint1
		VECTOR vSafeCoordsPoint2
		FLOAT fSafeCoordsFloat
		
		INT iAlphaValue = 64
		
		TIME_DATATYPE iWarpStartTime
		
//		BOOL bShowAllSafeAir
//		BOOL bOutputAllSafeAir
//		INT iWarpToSafeAir = -1
//		BOOL bGrabAirGroundZ
//		INT iGrabAirGroundZ
		
		BOOL bShowAllSafeSea
		BOOL bOutputAllSafeSea
		INT iWarpToSafeSea = -1
		
		BOOL bRecordSeaPoints
		INT iRecordedSeaPoint
		VECTOR vRecordedSeaPoint
		INT iRecordingSeaDist = 800
		
		BOOL bShowAdvancedSpew
		
		BOOL bBlurForSpawn
		BOOL bClearBlurForSpawn
		
		BOOL bClearAllRespawnPickups	
		BOOL bRecreateAllRespawnPickups	
	
		BOOL bShowAllHospital
		BOOL bShowAllPoliceStation
		BOOL bShowAllHotels
		//BOOL bShowAllInteriorBleeds
		BOOL bShowAllForbiddenAreas
		BOOL bShowAllProblemNodeAreas
		BOOL bShowAllProblemMinAirplaneHeight
		BOOL bShowAllSpawningSectors
		
		SPAWN_AREA LastGetSafeCoordsForCreatingEntity
		MP_SPAWN_POINT LastCoordsReturnedForGetSafeCoords[NUMBER_OF_RESULTS_STORED_FOR_GETTING_SAFE_COORDS_DEBUG]
		
		VECTOR vSpawnVehicleCoords
		FLOAT fSpawnVehicleHeading
		BOOL bTestSpawnVehicle
		VECTOR vSpawnVehicleStartCoords
		VECTOR vSpawnVehicleFacingCoords
		BOOL bUsePlayerPositionForSpawnVehicle
		BOOL bRenderSpawnVehicleResult
		BOOL bSpawnVehicleUseRoadOffset
		FLOAT fSpawnVehicleMinDistFromCoords
		BOOL bSpawnVehicleConsiderHighways
		BOOL bSpawnVehicleInExactCoordsIfPossible
		FLOAT fSpawnVehicleMaxRange
		BOOL bTestOnlyActiveNodes
		BOOL bTestAvoidExclusion
		BOOL bTestBoatNodesOnly
		
		BOOL bTestMoveSpecificCoords
		BOOL bMoveOutOfGlobalExclusion
		BOOL bMoveOutOfMissionExclusion
		BOOL bMoveOutOfGangAttack
		BOOL bMoveOutOfMissionCorona
		BOOL bRenderSpecificCoords
		BOOL bRenderAllRaceCheckpoints
		BOOL bDontUseSecondaryRespawn
		BOOL bRenderRespotSphere
		INT iRenderAllRaceCheckpointsDrawDistance = 500
		
		BOOL bIgnoreRaceSecondaryRespawns
		BOOL bTriggerManualRespawn
		
		BOOL bTestInteriorCoord
		VECTOR vTestInteriorCoord
		
		BOOL bDrawCodeAngleAreas
		BOOL bTestFlashInvincible
		
		BOOL bSpawnCamIgnoreShapeTest
		
		BOOL bShowPropertySpawnInfo
		INT iHighlightedProperty

		BOOL bGiveHelmet
		BOOL bTestShapeTest
		BOOL bDontResetShapeTestFlag
		
		VECTOR vShapeTestStartForward
		VECTOR vShapeTestStartForwardRight
		VECTOR vShapeTestStartRight
		VECTOR vShapeTestStartForwardLeft
		VECTOR vShapeTestStartLeft
		VECTOR vShapeTestStartRoad
		
		VECTOR vShapeTestEndForward
		VECTOR vShapeTestEndForwardRight
		VECTOR vShapeTestEndRight
		VECTOR vShapeTestEndForwardLeft
		VECTOR vShapeTestEndLeft
		VECTOR vShapeTestEndRoad		
		
		BOOL bShapeTestFailedForward
		BOOL bShapeTestFailedForwardRight
		BOOL bShapeTestFailedRight
		BOOL bShapeTestFailedForwardLeft
		BOOL bShapeTestFailedLeft
		BOOL bShapeTestFailedRoad
		
		VECTOR vShapeTestFailCoords_Forward
		VECTOR vShapeTestFailCoords_ForwardRight
		VECTOR vShapeTestFailCoords_Right
		VECTOR vShapeTestFailCoords_ForwardLeft
		VECTOR vShapeTestFailCoords_Left
		VECTOR vShapeTestFailCoords_Road
		
		BOOL bRenderShapeTest
		
		BOOL bOverrideValidPVModelCheck
		
		
		BOOL bCallSetSpawnLocation
		INT iSpawnLocationToSet
		
		BOOL bCallSpawnRandomVehicleNearPlayer
		BOOL bCallSpawnSavedVehicleNearPlayer
	 
		BOOL bClearPVNetID
		
		BOOL bCallCarNodeSearch
		VECTOR vCarNodeSearchInput
		VECTOR vCarNodeSearchOutput
		FLOAT fCarNodeSearchInputHeading
		FLOAT fCarNodeSearchOutputHeading
		NearestCarNodeSearch CarNodeSearchParams
		
		BOOL bSetCurrentCarAsRespawnVehicle
		
		BOOL bClearVehSpawnTimer
		
		BOOL bShowFallbackSpawnPoints
		BOOL bAddFallbackSpawnPoint
		VECTOR vFallbackSpawnPointPos
		FLOAT fFallbackSpawnPointHeading
		BOOL bClearFallbackSpawnPoints		
		
		BOOL bCall_IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA
		VECTOR vCoords1_TestSAEIAA
		VECTOR vCoords2_TestSAEIAA
		FLOAT fFloat_TestSAEIAA
		
		BOOL bTest_NET_WARP_TO_COORD
		BOOL b_NET_WARP_TO_COORD_keepvehicle=FALSE
		BOOL b_NET_WARP_TO_COORD_LeavePedBehind=TRUE
		BOOL b_NET_WARP_TO_COORD_DontSetCameraBehindPlayer=FALSE 
		BOOL b_NET_WARP_TO_COORD_KillLeftBehindPed=FALSE 
		BOOL b_NET_WARP_TO_COORD_KeepPortableObjects=TRUE
		BOOL b_NET_WARP_TO_COORD_DoQuickWarp=FALSE 
		BOOL b_NET_WARP_TO_COORD_SnapToGround=TRUE 
		BOOL b_NET_WARP_TO_COORD_IgnoreAlreadyThereCheck=FALSE
		
		
		BOOL bTest_GET_CLOSEST_VEHICLE = FALSE
		VECTOR vGET_CLOSEST_VEHICLE_coords
		INT iGET_CLOSEST_VEHICLE_modelHash = 0
		FLOAT fGET_CLOSEST_VEHICLE_dist = 30.0
		INT iGET_CLOSEST_VEHICLE_searchflags = 127
		VECTOR vGET_CLOSEST_VEHICLE_return
		
		BOOL bCall_WarpPlayerIntoCar
		INT iPlayers_car
		INT iCar_seat
		BOOL bWarpPlayerIntoCar_Force
		BOOL bWarpPlayerIntoCar_LastCar
		
		BOOL bForceNodeSearchFail= FALSE 
		
		BOOL bCallSetCustomPlayerModel_Franklin
		BOOL bCallSetCustomPlayerModel_Lamar
		BOOL bCallClearCustomPlayerModel
		BOOL bCallFinalizeHeadBlend
		BOOL bCallNetSpawnPlayer
		INT iSelectedSpawnLocation = -2
		TEXT_WIDGET_ID SpawnTextWidgetID
		
		
	#ENDIF
	
	FLOAT fFaceOffset = 0.1
	FLOAT fDebugSphereScale = 0.05
	
	INT iIsPointOKForNetEntityCreationPassMarks
	INT iIsPointOkForSpawningPassMarks
	
	BOOL bHasDoneInitialSpawn

	// spawn camera details
	BOOL bUpdateSpawnCam
	BOOL bUpdateSpawnCamRequestSent
	CAMERA_INDEX SpawnCam
	VECTOR vSpawnCamOffset 
	VECTOR vSpawnCamOffset2 
	FLOAT fSpawnCamFOV
	INT iSpawnCamState	
		
	INT iFrameCountForLandingGear
	VECTOR vVehPosForLandingGear
	FLOAT fVehHeadingForLandingGear
	VEHICLE_INDEX LandingGearVehicleID
	
	INT iNextRespawnFlashTime = 0
	INT iNextRespawnVehicleRespotTime = 0
	
	MISSION_SPAWN_GENERAL_BEHAVIOURS MissionSpawnGeneralBehaviours
	MISSION_SPAWN_PERSONAL_VEHICLE_DATA MissionSpawnPersonalVehicleData
	MISSION_SPAWN_DETAILS MissionSpawnDetails	
	MISSION_SPAWN_EXCLUSION_AREAS MissionSpawnExclusionAreas
	PLAYER_SPAWN_LOCATION SpawnLocation
	
	NEXT_SPAWN_LOCATION_INFO NextSpawn
	NEXT_SPAWN_LOCATION_INFO StoredNextSpawn
	
	WEAPON_TYPE VehicleWeapon
	MODEL_NAMES VehicleWeaponModel
	BOOL UseNonHomingMissiles = FALSE
	
	// Candidate spawn point and heading
	VECTOR vCandSpawnPoint
	FLOAT fCandSpawnHeading
	BOOL bSpawnLocHadFinalCheck
	
	BOOL bSpawnPersonalVehicle
	BOOL bRespawnSearchStarted
	
	VECTOR vMyDeadCoords
	BOOL bIgnoreGlobalExclusionCheck
	BOOL bIgnoreGlobalExclusionCheckForVehicleSpawning
	INT iDontSpawnPersonalVehicleThisFrame	
	
	BOOL bSpawningOnOwnInGang
	
	BOOL bIsBossSpawningInAircraft
	
	SPAWN_SEARCH_PARAMS PublicParams 			// used exclusively in GetSpawnLocationCoords()
	PRIVATE_SPAWN_SEARCH_PARAMS PrivateParams 	// used exclusively in GetSpawnLocationCoords()	
	
	// stuff for getting location
	//VECTOR vRawCoords
	//FLOAT fRawHeading
	//FLOAT fSearchRadius
	
	//BOOL bUseAngleAreas
	//VECTOR vAngledCoords1
	//VECTOR vAngledCoords2
	//FLOAT fAngledWidth
	//BOOL bConsiderRawAsValidPoint
	BOOL bUseIncreasingDist
	
	INT iGetLocationState
	INT iPreviousGetLocationState
	INT iGetLocationAttempts
	INT iGetLocationCarNodeSearchAttempts
	THREADID GetLocationThreadID
	//STREAMVOL_ID StreamVolID
	TIME_DATATYPE iGetLocationTime
	
	VECTOR vExclusionCoord
	FLOAT fNearExclusionRadius
	
	//FLOAT fMinDistFromPlayer
	//BOOL bIgnoreTeamMatesForDistChecks
	
	VECTOR vSpawnCoords
	FLOAT fSpawnHeading

	//INT iRespawnState // moved to global player bd
	TIME_DATATYPE iLoadSceneTimer
	//TIME_DATATYPE InvincibleStartTime
	INT iExtendedSpawnTimer
	MP_PLAYER_SPAWN_DECORATION eDecoration
	FLOAT fCamHeading = 0.0
	BOOL bDontBringUpHudWhenDead
	INT iWarpState
	INT iWarpServerRequestCount
	INT iWarpServerTimeoutCount
	TIME_DATATYPE iWarpRequestTime
	TIME_DATATYPE iWarpRequestSendTime
	TIME_DATATYPE iHasGotSpawnLocationUpdateTime	
	THREADID WarpRequestThreadID
	
	//VECTOR vRequestedWarpCoords
	//FLOAT fRequestedWarpHeading
	
	// for warping into vehcile
	INT iWarpIntoVehicleSeat
	INT iWarpIntoVehicleState
	INT iWarpIntoVehicleServerRequestCount
	INT iWarpIntoVehicleServerTimeoutCount
	THREADID WarpRequestIntoVehicleThreadID
	TIME_DATATYPE iHasGotSpecificSeatUpdateTime	
	TIME_DATATYPE iWarpIntoVehicleRequestTime
	TIME_DATATYPE iWarpIntoVehicleRequestSendTime
	TIME_DATATYPE iUnfreezeTimer
	BOOL bUnfreezingEarly
	
	SPAWN_RESULTS RequestedSpawnResults
	
	BOOL bServerRepliedToWarpRequest
	BOOL bServerAgreedToWarpRequest
	INT iServerSpawnResultsSlot
	INT iRequestToWarpID
	INT iResponseToWarpID
	
	BOOL bServerRepliedToWarpIntoVehicleRequest
	BOOL bServerAgreedToWarpIntoVehicleRequest
	
	BOOL bEnableUseOfSpawnNodes
	BOOL bForceRespawn
	BOOL bForceThroughJoining
	BOOL bFallbackUsed
	//BOOL bFlashingInvincible
	INT iFallbackAttempts
	BOOL bDoLoadScene
	BOOL bIsDoingLoadScene
	
	// spawn vehicle stuff
	BOOL bServerRepliedToSpawnVehicleRequest
	BOOL bServerAgreedToSpawnVehicleRequest	
	INT iRequestToVehicleSpawnID
	INT iResponseToVehicleSpawnID	
	
	THREADID SpawnVehicleRequestThreadID
	INT iSpawnVehicleState
	TIME_DATATYPE iSpawnVehicleRequestTime
	TIME_DATATYPE iSpawnVehicleRequestSendTime
	TIME_DATATYPE iHasGotVehicleSpawnLocationUpdateTime
	INT iHAS_GOT_SPAWN_LOCATION_waiting_frame
	
	TIME_DATATYPE iSpawnVehicleStartTime
	INT iSpawnVehicleServerRequestCount
	VECTOR vRequestedSpawnVehicleCoords
	FLOAT fRequestedSpawnVehicleHeading
	INT iSpawnVehicleEntityArea[MAX_NUM_SPAWN_VEHICLE_ENTITY_AREAS] 
	THREADID SpawnVehicleEntityThreadID[MAX_NUM_SPAWN_VEHICLE_ENTITY_AREAS]
	VECTOR vHasGotVehicleSpawnLocationCalledWithCoords
	MODEL_NAMES HasGotVehicleSpawnLocationCalledWithModelName
	INT iActiveSpawnVehicleEntityArea
	TIME_DATATYPE iSpawnVehicleEntityAreaTime

	
	// bools used to refine points
	BOOL bIgnoreExclusionCheck 
	//BOOL bNearAsPossibleToPassedInCoords
	//BOOL bDoNearARoadChecks
	//BOOL bConsiderInteriors
	//VECTOr vFacing
	
	
	//THREADID WarpToLocationThread
	//INT iWarpToLocationTime
	PLAYER_SPAWN_LOCATION WarpToSpawnLocation
	//INT iWarpToLocationState // moved to global player bd
	TIME_DATATYPE iWarpToLocationTime
	TIME_DATATYPE iWarpIntoVehicleTime
	TIME_DATATYPE iWarpDelayTime
	VECTOR vWarpToLocationCoords
	FLOAT fWarpToLocationHeading
	//BOOL bHasWarpedIntoVehicle
	

	INT iSetPlayerOnGroundAttempts = 0
	INT iSetMyVehicleOnGroundAttempts = 0
	BOOL bCanSetDelayTime = TRUE

	
	// visibility check
	BOOL bDoVisibilityChecksOnTeammates	= TRUE
	//BOOL bDoVisibleChecks = TRUE
	
	// custom spawn points
	CUSTOM_SPAWN_POINT_INFO CustomSpawnPointInfo
	CUSTOM_SPAWN_POINT_INFO StoredCustomSpawnPointInfo
	BOOL bHasStoredCustomSpawnPointInfo
	BOOL bUseCustomSpawnPoints

	// custom vehicle nodes
	INT iNumberOfCustomVehicleNodes
	CUSTOM_VEHICLE_NODE CustomVehicleNodes[MAX_NUMBER_OF_CUSTOM_VEHICLE_NODES]
	INT iCustomVehicleNodesOrder[MAX_NUMBER_OF_CUSTOM_VEHICLE_NODES]
	#IF IS_DEBUG_BUILD
		INT iCustomVehicleNodesClearedOnFrame
		THREADID CustomVehicleNodeThreadID 
		TIME_DATATYPE CustomVehicleNodeTimeAdded
	#ENDIF
	
	// fallback spawn points
	INT iNumberOfFallbackSpawnPoints
	FALLBACK_SPAWN_POINT FallbackSpawnPoints[MAX_NUMBER_OF_FALLBACK_SPAWN_POINTS]
	
	// respawn search variables
	BOOL bRespawnSearch_HasStarted
	INT iRespawnSearch_State
	VECTOR vRespawnSearch_CurrentSearch
	FLOAT fRespawnSearch_CurrentRadius
	TIME_DATATYPE iRespawnSearch_LastUpdateTime
	TIME_DATATYPE iRespawnSearch_Time	
	BOOL bNavMeshRequested
	THREADID RespawnSearchThreadID
	THREADID NavMeshRequestedThreadID
	TIME_DATATYPE iNavMeshLoadTime
	
	// for querying the start spawn
	TIME_DATATYPE iQuerySpecificSpawnTime
	INT iQuerySpecificSpawnState
	BOOL bHasQueriedSpecificPosition
	INT iQuestSpecificSpawnReply
	
	
	VECTOR vRespawnSearch_CurrentSearch_AngledArea_Point1
	VECTOR vRespawnSearch_CurrentSearch_AngledArea_Point2
	FLOAT fRespawnSearch_CurrentRadius_AngledArea_Width
	//BOOL bRespawnSearch_IsAngled
	INT iRespawnSearch_Shape
	
	BOOL bBothPartnersAreDead
	INT iStoredBSForPartners
	
	BOOL bTellHUDNotToMovePlayer
	
	SPAWN_SEARCH_VARIABLES SearchTempData
	
	// JA added for bug 847162
	TIME_DATATYPE killStripTimer
	BOOL bPlayerWantsToRespawn
	
	BOOL bSafeForSpectatorCam
	//SCRIPT_TIMER SpectatorTimeout
	
	VECTOR vSpawnSearchResults[MAX_NUM_OF_STORED_SPAWNSEARCH_RESULTS]


	BOOL bSpawnTimerInitialised
	TIME_DATATYPE SpawnTimer
	TIME_DATATYPE GameStateTimer
	TIME_DATATYPE RespawnStateTimer
	BOOL bHasSpectatorCamBeenActive
	AFTERLIFE AfterlifeValueOnDeath	

	BOOL bIsBlurredOut
	
	BOOL bIsSwoopedUp
	
	BOOL bFadingOutForWarp
	TIME_DATATYPE FadingOutTimer
	
	BOOL bInitialisedCollectedRespawnPickupData
	BOOL bRespawnPickupsCreated
	INT iBS_RespawnPickupHelp
	INT i_RespawnPickupStagger
	INT iCreationPickupsProcessed
	INT iRespawnPickupsCreated

	
	INT iSpawnNearbySavedState = 0	
	VECTOR vSavedVehicleCoords
	FLOAT fSavedVehicleHeading
	
	INT iSpawnNearbyRandomState = 0
	MODEL_NAMES RandomVehicleModel = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vRandomVehicleCoords
	FLOAT fRandomVehicleHeading	
	NETWORK_INDEX RandomVehicleNetID
	
	WEAPON_TYPE MyLastWeapon
	VEHICLE_INDEX MyLastVehicle
	INT iBS_InCarWith
	
	VEHICLE_INDEX VehicleToDelete
	NETWORK_INDEX NetVehicleToDelete
	BOOL bPlayerWasInVehicleToDelete
	
	VEHICLE_INDEX LastUsedVehicleID
	
	//VECTOR vStartSwoop
	//VECTOR vApproxEndSwoop
	//SWITCH_TYPE eSwitchTypeStart
	//SWITCH_TYPE eSwitchTypeEnd
	
	VECTOR vBeforeRespawnCoords
	
	BOOL bDoShapeTest
	BOOL bIgnoreFaceRoad
	MANUAL_RESPAWN_STATE eManualRespawnState
	
	BOOL bDisableFadeInAndTurnOnControls
	
	BOOL bSpawningInProperty
	BOOL bSpawningInGarage
	BOOL bSpawningInYacht
	//BOOL bHasTransitionSpawnedPlayer
	BOOL bSpawningInSimpleInterior
	
	BOOL bPassedOutDrunk
	BOOL bPassedOutDrunkWithWantedLevel
	
	INT iDrunkRespawnYachtState =0
	
	INT iDrunkRespawnState = 0
	INT iPropertyToSpawnIntoToilet = -1
	BOOL bDrunkRespawnSafe = TRUE
	PLAYER_SPAWN_LOCATION eDrunkSpawnLocation
	
	INT iRadioStation = -1
	BOOL bRadioStationOverScore = FALSE
	BOOL bdisableradiosettinginspawn = FALSE
	
	INT iCurrentRadioStation = -1
	
	RESET_ON_SPAWN ResetOnSpawn
	BOOL bRememberVehicleWeaponOnSpawn
	
	BOOL bSpawnedInHospital = FALSE
	
	BOOL bDontAskPermission
	
	BOOL bImpromptuBeingSetup
	
	PED_COMP_NAME_ENUM StoredHelmet = DUMMY_PED_COMP
	PED_COMP_NAME_ENUM StoredMask = DUMMY_PED_COMP
	
	INT iBS_FlashingDisabled
	TIME_DATATYPE timerFlashingDisabled[NUM_NETWORK_PLAYERS]
	
	INT iAttempt
	INT iAttemptWarpIntoVehicle
	
	VECTOR vServerRefusalCoords[NUM_STORED_SERVER_REFUSAL_COORDS]
	
	// For bigfoot - hunt the beast mode
	SCRIPT_TIMER timerBigfootShockingEvent
	INT iBigfootShockingEventId
	
	VECTOR vCargobobSpawnCoord[4]
	VECTOR vCargobobBackupCoord
	FLOAT fCargobobSpawnHeading[4]
	BOOL bCargobobUseBehaviour
	INT iClosestCBPlayerIndex
	FLOAT fClosestCBPlayerDist
	
	
	INT iYachtToWarpTo = -1
	INT iYachtToWarpFrom = -1
	INT iYachtZoneToWarpTo
	BOOL bHasAccessToYachtWarp
	BOOL bInEmptyYachtSpace
	PLAYER_INDEX WarpOwnerID
	TIME_DATATYPE YachtWarpTimer
	VEHICLE_INDEX YachtWarpVehicleID
	BOOL bYachtVehicleSpawnBlocked[NUMBER_OF_PRIVATE_YACHT_VEHICLES]
	VECTOR vOffsetCoordsOfYachtWarpVehicle
	FLOAT fOffsetHeadingOfYachtWarpVehicle
	FLOAT fNearestYachtDist = -1.0
	
	
	VEHICLE_INDEX SpecificVehicleID
	INT SpecificVehicleSeat = VS_INVALID
	
	BOOL bHasAttemptedFallbackUsingCustomPoints

	INT iGroupWarpState = -1
	INT iGroupWarpStatePVCreation = 0
	TIME_DATATYPE tGroupWarpTimer 
	INT iGroupWarpPlayerToProcess = 0
	VECTOR vGroupWarpCoords
	FLOAT fGroupWarpHeading
	BOOL bUseExactGroupWarpCoords
	INT iGroupWarp_Bitset
	#IF IS_DEBUG_BUILD
	BOOL bFailedToGetGroupWarpArea
	INT iTest_GroupWarpLocation
	BOOL bStartGroupWarpWithAllPlayersInSession
	
	BOOL bRenderGroupWarpPositions
	
	BOOL bStartDefunctBaseGroupWarp
	
	BOOL bGetCustomVehicleNodesForPlayerPosition
	BOOL bClearCustomVehicleNodesForPlayerPosition
	BOOL bRenderCustomVehicleNodesForPlayerPosition
	
	BOOL bCreatePlacementVehicles
	BOOL bPlacementVehiclesCreated
	BOOL bOutputPlacementVehicles
	BOOL bOutputPlacementFormation
	BOOL bLockTargetVehicle
	INT iPlacementVehicle_GroupWarpLocation = 0
	INT iPlacementVehicle_GroupWarpArea = 0
	
	INT iLoadPlacementFormation 
	
	VEHICLE_INDEX PlacementVehicleID[8]
	VECTOR vPlacementOffset[8]
	FLOAT fPlacementHeadingOffset[8]
	
	#ENDIF
	
	BOOL bUseUnsortedSearchResultsSystem = TRUE
	
	BOOL bRetractVehicleWheelsInstantly
	BOOL bExtendVehicleWheelsInstantly
	
	BOOL bUseAlternateVehicleForm
	BOOL bDeluxoFlightBoost
	BOOL bUseVerticalFlightMode
	
	BOOL bLateTurnOffVTOL
	INT iLateTurnOffVTOLFrame
	
	BOOL bDeployParachute
	
	FLOAT fPitch
	
	BOOL bUseVehStealthMode = FALSE
	
	VECTOR vLastVector_WILL_POINT_USE_CUSTOM_VEHICLE_NODES
	BOOL bLastResult_WILL_POINT_USE_CUSTOM_VEHICLE_NODES

	VECTOR vLastVector_IS_POINT_IN_AREA_WITH_CUSTOM_VEHICLE_NODES
	INT iLastResult_IS_POINT_IN_AREA_WITH_CUSTOM_VEHICLE_NODES
	
	VECTOR vLastVector_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP
	BOOL bLastResult_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP
	
	FLOAT fDistFromLastCall_1 = 0.1
	FLOAT fDistFromLastCall_2 = 10.0
	
	BOOL bShouldFadeInBeDelayed
	TIME_DATATYPE iFadeInDelayTimer
	INT iFadeInDelayTimeToExpire = 350
	
	BOOL bLaunchMission
	BOOL bMissionSetup
	BOOL bMissionFailedToLaunch
	BOOL bSkipClearingSpawn
	
	VEH_SPAWN_REQUEST_INFO VehSpawnRequestClearList[NUM_SERVER_STORED_VEH_REQUESTS]
	
	BOOL bCheckNetNPCs = TRUE
	
	#IF FEATURE_FREEMODE_ARCADE
	BOOL bRunFastTravel = FALSE
	BOOL bForceFastTravel = FALSE
	#ENDIF
	
	INT iRespawnInteractionType = -1 
	INT iRespawnSelectedAnim = -1
	BOOL bRespawnInteractionFullBody = FALSE
	FLOAT fRespawnInteractionStartPhase = 0.0
	BOOL bRespawnInteractionForceWeaponVisible = FALSE
	BOOL bRespawnInteractionUsesMatchStart = FALSE
	THREADID RespawnInteractionThreadID
	BOOL bRespawnInteractionPersists = FALSE
	
	#IF FEATURE_GEN9_EXCLUSIVE
	BOOL bHasCalledMpTutorialBikerBusinessEntitySet = FALSE
	#ENDIF
	
ENDSTRUCT



//STRUCT TRANSITION_SPAWN_DATA_OLD
//	
//	INT iSpawnActivity
//	INT iSpawnActivityState
//	BOOL bSpawnActivityReady
//	INT iSpawnInSavedVehicleState = 0
//	BOOL bSpawnNearbySavedVehicleUseRoadOffset=TRUE
//	BOOL bSpawnNearbySavedVehicle 
//	BOOL bWarpPlayerIntoSavedVehicle
//	BOOL bSwitchSavedVehicleEngineOn
//	BOOL bSpawnNearbySavedVehicleConsiderHighways=FALSE
//	BOOL bSpawnVehicleInExactCoordsIfPossible
//	FLOAT fSpawnVehicleMaxRange = 150.0
//	BOOL bSpawnInProperty_WatchingTV
//	
//	INT iSpawnInteractionType = -1
//	INT iSpawnInteractionAnim = -1
//	INT iSpawnInteractionState = 0
//	BOOL bSpawnInteractionReady
//	
//	INT iLastSpawnActivity = -1
//	BOOL bLastSpawnActivityCleanedUp
//	
//	INT iLastSpawnInteractionType = -1
//	INT iLastSpawnInteractionAnim = -1
//	BOOL bLastSpawnInteractionCleanedUp 
//	
//	INT iEntryWantedLevel
//	
//	BOOL bGamePlayCamSet
//	BOOL bGamePlayCamCleanedUp
//	FLOAT fGamePlayCamRelativePitch
//	FLOAT fGamePlayCamRelativeHeading
//	
//	BOOL bStartedTimer
//	TIME_DATATYPE iStartTime
//	
//	BOOL bIgnoreSpawnActivity
//	
//	#IF IS_DEBUG_BUILD
//		INT iJoiningSpawnActivityOverride =  -1
//		INT iJoiningSpawnInteractionTypeOverride =  -1
//		INT iJoiningSpawnInteractionAnimOverride =  -1
//	#ENDIF
//	
//ENDSTRUCT



STRUCT GLOBAL_PLAYER_GROUP_WARP_BROADCAST_DATA
	BOOL bReadyToStart
	BOOL bAbandon
	INT iUniqueID = -1
	GROUP_WARP_LOCATION eGroupWarpLocation
	INT iPlayerNamesHash[NUM_NETWORK_PLAYERS]
ENDSTRUCT

STRUCT GLOBAL_SERVER_GROUP_WARP_BROADCAST_DATA
	INT iUniqueID[NUM_NETWORK_PLAYERS]
	GROUP_WARP_LOCATION eGroupWarpLocation[NUM_NETWORK_PLAYERS]
	INT iArea[NUM_NETWORK_PLAYERS]
ENDSTRUCT



STRUCT TRANSITION_SPAWN_DATA
	INT iSpawnActivity
	INT iSpawnActivityState
	INT iSpawnInSavedVehicleState = 0
	TIME_DATATYPE SpawnActivityTime
	
	// bools
	BOOL bSpawnNearbySavedVehicleUseRoadOffset=TRUE
	BOOL bSpawnNearbyRandomVehicleUseRoadOffset=TRUE
	BOOL bSpawnNearbySavedVehicle = FALSE
	BOOL bSpawnNearbyRandomVehicle = FALSE
	BOOL bWarpPlayerIntoSavedVehicle = FALSE
	BOOL bFreezeSavedVehicle = FALSE
	BOOL bMakeSureVehicleCanBeDamaged = FALSE
	BOOL bSwitchSavedVehicleEngineOn = FALSE
	BOOL bSpawnNearbySavedVehicleConsiderHighways=FALSE
	BOOL bSpawnVehicleInExactCoordsIfPossible
	BOOL bSpawnInProperty_StartActivity
	BOOL bSpawnInteractionReady = FALSE
	BOOL bSpawnActivityReady = FALSE
	BOOL bSplineCamStarted = FALSE
	BOOL bIgnoreSpawnActivity = TRUE
	BOOL bSpawnActivtyObjectScriptReady = FALSE
	BOOL bSpawnActivityScriptHasLaunched = FALSE
	BOOL bFirstPersonCam = FALSE
	
	FLOAT fSpawnVehicleMaxRange = 150.0

	
	INT iSpawnInteractionType = -1
	INT iSpawnInteractionAnim = -1
	INT iSpawnInteractionState = 0
	
	INT iSpawnActivityLocationOverride = -1
	BOOL bNightClubSpawn = FALSE
	BOOL bCasinoAptSpawn = FALSE
	BOOL bArcadeSpawn = FALSE
	BOOL bSubmarineSpawn = FALSE
	BOOL bCarMeetSpawn = FALSE
	BOOL bAutoShopSpawn = FALSE
	#IF FEATURE_FIXER
	BOOL bFixerHQSpawn = FALSE
	#IF FEATURE_MUSIC_STUDIO
	BOOL bMusicStudioSpawn = FALSE
	#ENDIF
	#ENDIF
	INT iSyncScenePosition = 0
	
	INT iEntryWantedLevel

	BOOL bStartedTimer
	TIME_DATATYPE iStartTime
	
	// for spawning a minisub when waking up in sea
	BOOL bSpawnSpecificVehicle = FALSE
	VECTOR vSpecificVehicleCoords 
	FLOAT fSpecificVehicleHeading 
	MODEL_NAMES SpecificVehicleModel = DUMMY_MODEL_FOR_SCRIPT
	BOOL bSkipDrunkenWakeupAnim
	
	
	MODEL_NAMES StoredModelName = DUMMY_MODEL_FOR_SCRIPT
	scrPedHeadBlendData StoredHeadBlendData
	MODEL_NAMES CustomModelName = DUMMY_MODEL_FOR_SCRIPT
	THREADID CustomModelNameThreadID
	INT iTriggerNetSpawnFrame = 0	
	

ENDSTRUCT

STRUCT INTERIOR_SPAWN_ACTIVITY_DATA
	INT State = -1
	SIMPLE_INTERIOR_TYPE eSimpleInteriorType
	TEXT_LABEL_63 SpawnActivityDictionary
	TEXT_LABEL_63 SpawnActivityAnimation
	SCRIPT_TIMER SpawnActivityTimer
	INT SpawnActivityTimeToExpire = 10000
	INT SpawnActivitySceneID = -1
	VECTOR SpawnActivityScenePos
	VECTOR SpawnActivitySceneRot
ENDSTRUCT
	
TWEAK_FLOAT fSACam1OffsetX -1.5
TWEAK_FLOAT fSACam1OffsetY 0.0
TWEAK_FLOAT fSACam1OffsetZ 1.0

TWEAK_FLOAT fSACam1PointOffsetX 0.0
TWEAK_FLOAT fSACam1PointOffsetY 1.0
TWEAK_FLOAT fSACam1PointOffsetZ 0.5

TWEAK_FLOAT fSACam2OffsetX -0.1
TWEAK_FLOAT fSACam2OffsetY -1.0
TWEAK_FLOAT fSACam2OffsetZ 0.0

TWEAK_FLOAT fSACam2PointOffsetX 0.0
TWEAK_FLOAT fSACam2PointOffsetY 2.0
TWEAK_FLOAT fSACam2PointOffsetZ -0.1

TWEAK_FLOAT fSACamFOV 45.0
TWEAK_FLOAT fSACamShake 0.0
TWEAK_INT iSACamDuration 6000


CONST_INT NUMBER_OF_SPAWNING_SECTORS 9

//CONST_INT TOTAL_NUMBER_OF_INTERIOR_BLEEDS 2
//PROBLEM_AREA ProblemArea_InteriorBleed[TOTAL_NUMBER_OF_INTERIOR_BLEEDS]

//CONST_INT TOTAL_NUMBER_OF_FORBIDDEN_AREAS 34
CONST_INT MAX_NUMBER_OF_FORBIDDEN_AREAS_IN_SECTOR 22

//CONST_INT NUMBER_OF_PROBLEM_NODE_AREAS 26
CONST_INT MAX_NUMBER_OF_PROBLEM_NODE_AREAS_IN_SECTOR 11

CONST_INT NUMBER_OF_PROBLEM_NODESEARCH_AREAS 2
CONST_INT NUMBER_OF_MIN_AIRPLANE_HEIGHT_AREAS 4
CONST_INT NUMBER_OF_CUSTOM_VEHICLE_NODE_AREAS 32

//CONST_INT NUM_SAFE_SPAWN_IN_AIR 108
CONST_INT NUM_SAFE_SPAWN_IN_SEA 74

CONST_INT TOTAL_FIXED_HELI_LANDING_COORDS 299


// eof
