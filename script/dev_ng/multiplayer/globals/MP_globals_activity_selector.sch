USING "rage_builtins.sch"
USING "types.sch"										// For TIME_DATATYPE

USING "mp_globals_activity_selector_definitions.sch"
USING "MP_Globals_COMMON_CONSTS.sch"					// For NUM_NETWORK_PLAYERS


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_activity_selector.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains global variable instances for all MP Activity Selector routines.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Activity Selector Consts
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//      Contact Mission Consts
// -----------------------------------------------------------------------------------------------------------

// Will be setup with an unrealistic vector to represent any contact mission for registration and identification purposes
// Currently the mission's own coords will be used instead of this when setting up the mission
VECTOR				MP_CONTACT_MISSION_GENERIC_COORDS


// -----------------------------------------------------------------------------------------------------------
//      Heist Consts
// -----------------------------------------------------------------------------------------------------------

// Will be setup with an unrealistic vector to represent any safehouse for registration and identification purposes
// The player's own safehouse coords will be used instead of this when setting up the mission
VECTOR				MP_HEIST_GENERIC_SAFEHOUSE_COORDS


// ===========================================================================================================
//      Server Globals
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//      Group Scheduling - Server
// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Group Control variables
// NOTE: These will change frequently, so keep them local on all machines but only the current host will access them
//			(it won't matter if they go slightly out of date after a host migration, or if the scheduled group starts at the beginning again)
g_structASGroupControlServerMP		g_sASGroupControlServerMP


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing any Contact Mission Server Overall Control variables that should remain on local machine
// NOTE: These will change frequently, so keep them local on all machines but only the current host will access them
//			(it won't matter if they go slightly out of date after a host migration, or if the scheduled group starts at the beginning again)
g_structASCMControlServerMP		g_sASCMControlServerMP


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing any Heist Server Overall Control variables that should remain on local machine
// NOTE: These will change frequently, so keep them local on all machines but only the current host will access them
//			(it won't matter if they go slightly out of date after a host migration, or if the scheduled group starts at the beginning again)
g_structASHeistControlServerMP		g_sASHeistControlServerMP


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing any Contact Mission Server Overall Control variables that should remain on local machine
// NOTE: These will change frequently, so keep them local on all machines but only the current host will access them
//			(it won't matter if they go slightly out of date after a host migration, or if the scheduled group starts at the beginning again)
g_structASGAControlServerMP		g_sASGAControlServerMP





// ===========================================================================================================
//      Client Globals
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//      Group Scheduling - Client
// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Group Control variables
// NOTE: These will change frequently, so keep them local on all machines but only the current host will access them
//			(it won't matter if they go slightly out of date after a host migration, or if the scheduled group starts at the beginning again)
g_structASGroupControlClientMP		g_sASGroupControlClientMP


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing the positions of contact missions on the local cloud header data arrays
//g_structLocalContactMissions_Old		g_sLocalMPCMs_Old[MAX_CONTACT_MISSIONS]
INT									g_numLocalMPCMs			= 0


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Contact Mission Rank stats
g_structCMRankMissions_Old			g_CMsPerRank
TIME_DATATYPE						g_cmRankUpdateTimeout														// Timeout time before the Rank Update check is made

// -----------------------------------------------------------------------------------------------------------

// STRUCT containing any Heist Client Overall Control variables that should remain on local machine
// NOTE: These may change frequently, so keep them local on all machines rather than as PlayerBD
g_structASHeistControlClientMP		g_sASHeistControlClientMP



// -----------------------------------------------------------------------------------------------------------
//      Gang Attacks - Client
// -----------------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Client Control variables for the player's current Gang Attack - Local Variables
//g_structCurrentGAClient_Old			g_sCurrentClientGA_Old


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Client copy of the Server History List - for locking and unlocking of Gang Attacks
//g_structGAHistoryClient_Old			g_sHistoryClientGA_Old




// ===========================================================================================================
//      Transition Session Support Globals
// ===========================================================================================================

// KGM HEIST TRANSITION VARIABLES USED WHEN THE HEIST IS MOVING PAST THE INVITE STAGE - ENSURES THE SAME HEIST GETS SETUP WHEN THE PLAYERS MOVE TO THE NEW TRANSITION SESSION
// STRUCT containing the information needed to ensure the correct heist gets setup again in the new transition session
// KGM 8/4/13: Intended as TEMP to get transition support in quickly - it will need to be much more robust, probably passing Shared Mission and Pending Activity
//					so it can deal with the controlling player walking out of the corona, etc
// The controlling player stores this data when the SharedRegID is retrieved. After the transition it gets copied into the playerBD variables for the Host to pick up.
//g_structASFMHeistTransitionMP_Old g_sLocalTransitionSafeHeistData_Old


// -----------------------------------------------------------------------------------------------------------

// Contact Mission Transition Session variables - used to immediately setup the same Contact Mission again when the InCorona sequence moves to the new Transition Session
//g_structASFMCMTransitionMP	g_sLocalTransitionSafeCMData


// -----------------------------------------------------------------------------------------------------------

// KGM HEIST TRANSITION VARIABLES USED FOR CROSS-SESSION INVITES SO THAT THE PLAYERS JOINING FROM A DIFFERENT SESSION KNOW WHICH SAFEHOUSE THE HEIST IS IN
// NOTE: This is to solve the problem of the joining player placing the corona at the Mission's own coords - but the heist could be getting triggered from any safehouse
//g_structASFMHeistCrossSessionTransitionMP_Old g_sLocalHeistCrossSessionData_Old
