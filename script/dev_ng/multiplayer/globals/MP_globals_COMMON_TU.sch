USING "rage_builtins.sch"

USING "MP_Globals_Common_Definitions.sch"
USING "MP_Globals_Common_Consts.sch"
USING "clock_globals.sch"
USING "Commands_Netshopping.sch"
USING "net_script_timers.sch"

//Kill cam
KILL_CAM_STRUCT g_KillCamStruct


//Time in Session
TIME_DATATYPE gTimeSessionStarted

CONST_INT MAXQUICKFIXFEED	6

BOOL g_Private_QuitMPCoreScriptCrash
BOOL g_bCleanupMultiplayerMissionInteriors
BOOL g_b_SkipCoronaSaveCheck

CONST_INT MAX_NUM_SYSTEM_ACTIVITY_FEED 20

// for spawning into the game at specific coords
STRUCT SPECIFIC_SPAWN_LOCATION
    VECTOR vCoords
    FLOAT fMinRadius
    FLOAT fRadius
    FLOAT fHeading
    THREADID Thread_ID
    BOOL bDoVisibleChecks = TRUE
    BOOL bDoNearARoadChecks = TRUE
    BOOL bConsiderInteriors
    BOOL bUseAngledArea
    VECTOR vAngledCoords1
    VECTOR vAngledCoords2
    FLOAT fWidth
    VECTOR vFacing
    FLOAT fMinDistFromOtherPlayers = 65.0
	BOOL bNearCentrePoint
	BOOL bAllowFallbackToUseInteriors
	BOOL bIgnoreGlobalExclusionZones = FALSE
	BOOL bVehiclesCanConsiderInactiveNodes = FALSE
	FLOAT fPitch
	FLOAT fUpperZLimitForNodes = -1.0
ENDSTRUCT
SPECIFIC_SPAWN_LOCATION g_SpecificSpawnLocation


STRUCT MISSIONFILE_OUTPUT
	TEXT_LABEL_63 JobName
	TEXT_LABEL_23 ContentID
	TEXT_LABEL_63 TypeOfJob
	INT TotalPlayers
	INT Time
	INT PlayerRank
	INT Checkpoints
	BOOL Dnf
	INT CheckpointsPassed
	INT FinalPosition
	BOOL TeamGame
	INT KillTarget
	INT TeamPlace
	INT LastWaveCompleted
	INT Difficulty
	BOOL PassFail
	INT MissionRank
	BOOL TripSkipped
	BOOL HeistStage
	BOOL HeistLeader
	INT Medal
	BOOL TimesAwarded
	INT HighestMedal
	MODEL_NAMES VehicleType
	BOOL ImportExport	
	INT ActualCashAwarded
	INT ActualRPAwarded
	INT AverageRank
	INT SumEntryFees
	INT JobFinished_Posix
	INT PointsParachute
	BOOL IsCrewMember
	BOOL bIsFreeAim
	BOOL bDestroyLastPlace
ENDSTRUCT

MISSIONFILE_OUTPUT g_missionFileEMPTY
MISSIONFILE_OUTPUT g_missionFile
MISSIONFILE_OUTPUT g_missionFile_ExternalTracker


ENUM SYSTEM_ACTIVITY_TYPE
	SAT_NONE = 0,
//	SAT_FINISHED_RACE,
//	SAT_FINISHED_DM,
//	SAT_FINISHED_CAPTURE,
	
	SAT_ENTERED_GTAO,
	
	SAT_PUBLISHED_RACE,
	SAT_PUBLISHED_DM,
	SAT_PUBLISHED_CAPTURE,
	SAT_VERIFIED_RACE,
	SAT_VERIFIED_DM,
	SAT_VERIFIED_CAPTURE,
	SAT_PLAYED_FRIEND_PUBLISHED_RACE,
	SAT_PLAYED_FRIEND_PUBLISHED_DM,
	SAT_PLAYED_FRIEND_PUBLISHED_CAPTURE,
	
	SAT_JOINED_CREW,
	SAT_CREATED_CREW,
	
	SAT_PLAYING_W_FRIENDS,
	
	SAT_PURCHASED_TATTOO,
	SAT_PURCHASED_FIRST_PROPERTY,
	SAT_PURCHASED_SECOND_PROPERTY,
	SAT_PURCHASED_THIRD_PROPERTY,
	SAT_MOVED_HOUSE,
	SAT_PURCHASED_WEAPON,
	SAT_PURCHASED_VEHICLE_MOD,
	SAT_PURCHASED_VEHICLE,
	
	SAT_BOUGHT_CLOTHES,
	SAT_BOUGHT_HAIRCUT,
	
	SAT_COMPLETED_JOB,
	SAT_PLAYED_HEIST,
	
	SAT_PLAYED_GOLF,
	SAT_PLAYED_ARM_WRESTLING,
	SAT_PLAYED_SHOOTING_RANGE,
	SAT_PLAYED_DARTS,
	SAT_PLAYED_TENNIS,
	SAT_PLAYED_PILOT_SCHOOL,
	
	SAT_PLAYED_PLAYLIST,
	SAT_PLAYED_TOURNAMENT,
	SAT_PLAYED_TOURNAMENT_QUAL,
	SAT_WON_TOURNAMENT,
	SAT_PLAYED_EVENT_PLAYLIST,
	
	SAT_PLAYED_CHALLENGE_PLAYLIST,
	SAT_SETUP_CHALLENGE_PLAYLIST,
	SAT_WON_CHALLENGE_PLAYLIST,
	
	SAT_DONE_GANG_ATTACK,
	SAT_DONE_BOUNTY,
	SAT_DONE_IMPEXPORT,
	SAT_DONE_DELIVER,
	SAT_DONE_SECURITY_VAN,
	
	SAT_COLLECTED_CRATE_DROP,
	SAT_COLLECTED_SPECIAL_CRATE_DROP,
	SAT_ROBBED_HOLD_UP_STORE,
	SAT_DONE_STUNT_JUMP,
	
	SAT_DONE_ONE_ON_ONE_DM,
	SAT_DONE_IMPROMPTU_RACE,

	SAT_UPLOADED_PHOTO,
	SAT_MADE_MONEY_BETTING,
	
	SAT_MET_LESTER,
	SAT_MET_TREVOR,
	SAT_MET_MARTIN,
	
	SAT_DONE_HEIST,
	
	
	//SINGLEPLAYER
	
	SAT_COMPLETED_PROLOGUE,
	SAT_COMPLETED_LESTER1,
	SAT_COMPLETED_FRANKLIN2,
	SAT_COMPLETED_ALL_RANDOM,
	SAT_COMPLETED_SP_MISSION,
	
	SAT_DRIVEN_ALL_VEHICLES,
	SAT_COLLECTED_SPACESHIP,
	SAT_ALL_LETTER_SCRAPS,
	SAT_COLLECTED_SONAR,
	
	SAT_DRIVEN_500_MILES,
	SAT_FLOWN_500_MILES,
	SAT_RAN_50_MILES,
	SAT_BUSTED_10_TIMES,
	SAT_WASTED_10_TIMES,
	
	SAT_FIRED_HALFMIL_BULLETS,
	SAT_EVADED_5STAR_WANTED,
	
	SAT_PURCHASED_CAR_ONLINE,
	SAT_PURCHASED_RHINO_ONLINE,
	SAT_PURCHASED_BUZZARD_ONLINE,
	
	SAT_STOCKMARKET_LOSS,
	SAT_STOCKMARKET_INVESTED,
	
	SAT_SP_STUNT_JUMPS,	
	SAT_SP_UNDER_BRIDGE,
	
	SAT_FOUND_HIGHEST_POINT,
	SAT_DRUNK,
	SAT_PURCHASED_ALL_PROPERTIES,
	SAT_EXPLORATION,
	
	SAT_FOUND_LOWEST_POINT,
	SAT_HIGH_VEHICLE_SPEND,
	SAT_HIGH_WEAPON_SPEND,
	SAT_HIGH_CLOTHES_SPEND,
	SAT_HOLE_IN_ONE,
	
	SAT_DRIVEN_1000_KILOMETERS,
	SAT_FLOWN_1000_KILOMETERS,
	SAT_RAN_100_KILOMETERS,
	
	SAT_ANIMAL_PHOTOGRAPHY,
	
	SAT_MAX_ENTRIES

ENDENUM


ENUM ACTIVITY_FEED_ANIMAL
	AFA_NONE = 0,
	AFA_BOAR,
	AFA_BORDER_COLLIE,
	AFA_CAT,
	AFA_CHICKEN_HAWK,
	AFA_CORMORANT,
	AFA_COW,
	AFA_COYOTE,
	AFA_CROW,
	AFA_DEER,
	AFA_HEN,
	AFA_HUSKY,
	AFA_MOUNTAIN_LION,
	AFA_PIG,
	AFA_RABBIT,
	AFA_POODLE,
	AFA_PUG,
	AFA_RETRIEVER,
	AFA_ROTTWEILER,
	AFA_SEAGULL,
	AFA_WEST_HIGHLAND_TERRIER
ENDENUM


SCRIPT_TIMER g_st_SysActivityFeed_CooldownTimer[COUNT_OF(SYSTEM_ACTIVITY_TYPE)]
BOOL g_b_SysActivityFeed_HasThisBeenTriggered[COUNT_OF(SYSTEM_ACTIVITY_TYPE)]

CONST_INT NUM_SYSTEM_ACTIVITY_ARGS 4
STRUCT SYSTEM_ACTIVITY_DETAILS

	SYSTEM_ACTIVITY_TYPE Activity_TYPE
	TEXT_LABEL_63 Activity_NAME
	INT Activity_INT
	TEXT_LABEL_63 Arguments[NUM_SYSTEM_ACTIVITY_ARGS]
	

ENDSTRUCT

SYSTEM_ACTIVITY_DETAILS g_st_Private_System_Activity_Details[MAX_NUM_SYSTEM_ACTIVITY_FEED]


ENUM MIGRATION_PLATFORM_STAGES
	MIGRATION_PLATFORM_NONE = 0,
	MIGRATION_PLATFORM_START_CHECK,
	MIGRATION_PLATFORM_CHECK_START_STATUS,
	MIGRATION_PLATFORM_RUN_CHECK,
	MIGRATION_PLATFORM_IS_AVAILABLE,
	MIGRATION_PLATFORM_NOT_AVAILABLE
ENDENUM

ENUM MIGRATION_TRANSFER_STAGES
	MIGRATION_TRANSFER_NONE = 0,
	MIGRATION_TRANSFER_GET_STATUS,
	MIGRATION_TRANSFER_FAILED_RETRY,
	MIGRATION_TRANSFER_FAILED_NO_MORE_RETRY,
	MIGRATION_TRANSFER_ALREADY_DONE,
	MIGRATION_TRANSFER_START,
	MIGRATION_TRANSFER_SAVE_AVAILABLE,
	MIGRATION_TRANSFER_SAVE_NOT_AVAILABLE,
	MIGRATION_TRANSFER_FINISHED

ENDENUM

ENUM DELETE_REASONS
	DELETE_REASON_DEFAULT = HASH("DELETE_REASON_DEFAULT"),
	DELETE_REASON_CHARACTER_MENU_USER_ACTION = HASH("DELETE_REASON_CHARACTER_MENU_USER_ACTION"),
	DELETE_REASON_SLOT_FAILED_USER_ACTION = HASH("DELETE_REASON_SLOT_FAILED_USER_ACTION"),
	DELETE_REASON_JUNK_DATA_AUTO_ACTION = HASH("DELETE_REASON_JUNK_DATA_AUTO_ACTION"),
	DELETE_REASON_MAIN_FILE_FAILED_USER_ACTION = HASH("DELETE_REASON_MAIN_FILE_FAILED_USER_ACTION"),
	DELETE_REASON_CTRL_H_USER_DEBUG_ACTION = HASH("DELETE_REASON_CTRL_H_USER_DEBUG_ACTION")
	
ENDENUM

ENUM SAVE_MIGRATION_MAIN
	MIGRATION_MAIN_NONE = 0,
	
	MIGRATION_CHECK_TO_BEGIN,
	
	MIGRATION_CAN_START_XBOX360,
	MIGRATION_GET_STATUS_XBOX360,
	MIGRATION_FAILED_RETRY_XBOX360,
	MIGRATION_FAILED_NO_MORE_RETRY_XBOX360,
	MIGRATION_ALREADY_DONE_XBOX360,
	MIGRATION_NOT_AVAILABLE_XBOX360,
	MIGRATION_AVAILABLE_XBOX360,
	MIGRATION_IS_PLATFORM_AVAILABLE_XBOX360,
	
	MIGRATION_CAN_START_PS3,
	MIGRATION_GET_STATUS_PS3,
	MIGRATION_FAILED_RETRY_PS3,
	MIGRATION_FAILED_NO_MORE_RETRY_PS3,
	MIGRATION_ALREADY_DONE_PS3,
	MIGRATION_NOT_AVAILABLE_PS3,
	MIGRATION_AVAILABLE_PS3,
	MIGRATION_IS_PLATFORM_AVAILABLE_PS3,
	
	MIGRATION_CAN_START_XBONE,
	MIGRATION_GET_STATUS_XBONE,
	MIGRATION_FAILED_RETRY_XBONE,
	MIGRATION_FAILED_NO_MORE_RETRY_XBONE,
	MIGRATION_ALREADY_DONE_XBONE,
	MIGRATION_NOT_AVAILABLE_XBONE,
	MIGRATION_AVAILABLE_XBONE,
	MIGRATION_IS_PLATFORM_AVAILABLE_XBONE,
	
	MIGRATION_CAN_START_PS4,
	MIGRATION_GET_STATUS_PS4,
	MIGRATION_FAILED_RETRY_PS4,
	MIGRATION_FAILED_NO_MORE_RETRY_PS4,
	MIGRATION_ALREADY_DONE_PS4,
	MIGRATION_NOT_AVAILABLE_PS4,
	MIGRATION_AVAILABLE_PS4,
	MIGRATION_IS_PLATFORM_AVAILABLE_PS4,
	
	MIGRATION_PLAYER_PRE_CHOOSE_SCREEN,
	MIGRATION_PLAYER_PRE_CHOOSE_CONFIRM_CANCEL,
	MIGRATION_PLAYER_PRE_CHOOSE_ALREADY_DONE,
	
	MIGRATION_PLAYER_CHOOSE_WHICH_PLATFORM,
	
	MIGRATION_PLAYER_CHOOSE_CONFIRM_CANCEL,
	MIGRATION_PLAYER_CHOOSE_CONFIRM_ACCEPT,
	
	MIGRATION_START_XBOX360,
	MIGRATION_START_PS3,
	MIGRATION_START_XBONE,
	MIGRATION_START_PS4,
	
	
	MIGRATION_RUNNING,
	MIGRATION_SAVE_AVAILABLE,
	MIGRATION_SAVE_NOT_AVAILABLE,
	
	MIGRATION_PAUSEMENU_OPTION_FAILED,
	MIGRATION_PAUSEMENU_OPTION_CLOUD_FAILED,
	
	MIGRATION_FINISHED_DONE,
	MIGRATION_FINISHED_FAILED,
	MIGRATION_FINISHED_CANCELLED

ENDENUM


//Player blip data
PLAYER_BLIP_DATA g_PlayerBlipsData

//#IF FEATURE_GANG_BOSS
//GANG_BOSS_DATA g_GangBossData
//#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  GLOBAL SERVER AND CLIENT BROADCAST DATA              /////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

GlobalServerBroadcastData                   GlobalServerBD
GlobalServerBroadcastDataB                  GlobalServerBD_BlockB
GlobalServerBroadcastDataC					GlobalServerBD_BlockC
GlobalServerBroadcastDataMissionRequest     GlobalServerBD_MissionRequest
GlobalServerBroadcastDataMissionList        GlobalServerBD_MissionList
GlobalServerBroadcastDataExclusionArea      GlobalServerBD_ExclusionAreas
GlobalServerBroadcastDataActivitySelector   GlobalServerBD_ActivitySelector
GlobalServerBroadcastDataBetting            GlobalServerBD_Betting
GlobalServerBroadcastDataHoldUp             GlobalServerBD_HoldUp

GlobalPlayerBroadcastData                   GlobalplayerBD[NUM_NETWORK_PLAYERS]


BOOL g_Private_RunEmergencySkyCamUp
BOOL g_Private_WasInviteAcceptedInGTAO

BOOL g_bUseStackCopy = TRUE

// ******************************************************************************************************************************************************
// ******************************************************************************************************************************************************
// ******************************************************************************************************************************************************

// MPGlobal struct - declare any globals in here which will need reset when coming into CNC or when changing team. 

// ******************************************************************************************************************************************************
// ******************************************************************************************************************************************************
// ******************************************************************************************************************************************************

MPGlobalsStruct MPGlobals

//Copy of MPGlobals for resetting data temporarily - DO NOT DO ANYTHING WITH THIS STRUCT
MPGlobalsStruct MPGlobalsEmptyCopy 



FMMCLAUNCHER_SCRIPT_THREAD_BG_DATA g_sFmmcLauncherThreadBgData
FREEMODE_SCRIPT_THREAD_BG_DATA g_sFreemodeThreadBgData
PIMENU_SCRIPT_THREAD_BG_DATA g_sPiMenuThreadBgData

// ===========================================================================================================
//      Player corona positioning
// ===========================================================================================================

CHARACTER_CONTROLLER_GLOBALS g_sCharacterControllerData

FMMC_SELECTED_ITEMS g_sTransitionSessionOptions
FMMC_SELECTED_ITEMS g_sCoronaOptionsBackUp
CORONA_CUSTOM_VEHICLES_DATA g_sCoronaCustomVehiclesData

// Ammo and armor cash transactions are tracked for PC to listen for server response
ENUM AMMO_ARMOR_CASH_TRANSACTION_ENUM
	AMMO_ARMOR_CASH_TRANSACTION_FULL_AMMO = 0,
	AMMO_ARMOR_CASH_TRANSACTION_FULL_CLIP_AMMO,
	AMMO_ARMOR_CASH_TRANSACTION_SINGLE_CLIP_AMMO,
	AMMO_ARMOR_CASH_TRANSACTION_ARMOR
ENDENUM

STRUCT AMMO_ARMOR_CASH_TRANSACTION_STRUCT
	INT iAmmoArmorCashTransactionActive = 0
	WEAPON_TYPE wtWeapon
ENDSTRUCT
AMMO_ARMOR_CASH_TRANSACTION_STRUCT g_sAmmoArmorCashTransaction

CORONA_CLONE_PEDS g_CoronaClonePeds
CORONA_INVITED_PLAYER_DATA g_CoronaInvitedPlayers
CORONA_PLAYERS_END_OF_JOB g_CoronaEndOfJobPlayers


FMMC_TRANSITION_SESSION_DATA g_sTransitionSessionData

#IF IS_DEBUG_BUILD
FLOAT g_fCapWantedMultiplier = 1.0
#ENDIF
BOOL g_bCLEAR_ROOT_CONTENT_SAFE = TRUE

TRANSITION_SESSION_NON_RESET_VARS g_TransitionSessionNonResetVars
BOOL g_bForceEntityAlphaOnJoin = TRUE
BOOL g_bResetAlphaOnSpawn
//BOOL g_bAllowKickFromRespotVehicle = TRUE

STRUCT_SEND_DATA_JSON_TO_CLOUD g_sSendDataJson

BOOL g_Private_AreInviteSpinnerLoading

BOOL g_b_IsRockstarIDValidLastFrame
BOOL g_b_IsRockstarIDValidLastFrame_Init

BOOL g_DisableSCTVPlayerSlots = FALSE
INT g_iNumSCTVSecurity = 2

INT g_iNumPlayersOnMyHeistTeam = -1

SCRIPT_TIMER g_stCloudDownBailTimer
TRANSITION_MEMBERS g_transitionMembers

BOOL g_StreamingIsPaused

ENUM SESSION_FULL_QUESTION
	SESSION_FULL_QUESTION_JOIN_ANOTHER_SESSION = 0,
	SESSION_FULL_QUESTION_SPECTATE_PLAYER,
	SESSION_FULL_QUESTION_JOIN_QUEUE,
	SESSION_FULL_QUESTION_CANCELLED
ENDENUM

INT g_bitset_SessionFullQuestion_Available
INT g_bitset_SessionFullQuestion_Answer


#IF IS_DEBUG_BUILD
	BOOL g_RunDebugScAdminCorrectionWidget
	INT g_iDebugRPGiftSet
	INT g_iDebugRPGiftDelta
	INT g_iDebugCashGiftSet
#ENDIF

BOOL g_bSystem_Service_event_Shown_Upgrade


CURRENT_UGC_STATUS_STRUCT g_sCURRENT_UGC_STATUS

STRUCT_STAT_DATE aTimeforrefreshingtunables
INT itimerforcheckingtunablerefresh = 0

FM_HIDDEN_PACKAGE_CREATION_STRUCT FM_hidden_package[number_of_FM_hidden_package_pickups]
INT iHiddenPackagesProcessedLastFrame


// these are the maximum number of this type of pickup that can be created, must match with list above.
CONST_INT FM_WEAPON_PICKUPS_MAX_OTHERS 		10
CONST_INT FM_WEAPON_PICKUPS_MAX_MELEE 		38
CONST_INT FM_WEAPON_PICKUPS_MAX_PROJECTILE 	62
CONST_INT FM_WEAPON_PICKUPS_MAX_GUN 		90

CONST_INT FM_MAX_NUMBER_OF_WEAPON_PICKUPS 62

FM_WEAPON_PICKUP_SPAWN_LOCATION g_PickupSpawnLocations_Other[FM_WEAPON_PICKUPS_MAX_OTHERS]
FM_WEAPON_PICKUP_SPAWN_LOCATION g_PickupSpawnLocations_Melee[FM_WEAPON_PICKUPS_MAX_MELEE]
FM_WEAPON_PICKUP_SPAWN_LOCATION g_PickupSpawnLocations_Projectile[FM_WEAPON_PICKUPS_MAX_PROJECTILE]
FM_WEAPON_PICKUP_SPAWN_LOCATION g_PickupSpawnLocations_Gun[FM_WEAPON_PICKUPS_MAX_GUN]

CONST_INT FM_WEAPON_PICKUP_STATE_NULL 			0
CONST_INT FM_WEAPON_PICKUP_STATE_CLAIM_PENDING 	1

INT g_iNumberOfWeaponPickupsToBeCreated_Other
INT g_iNumberOfWeaponPickupsToBeCreated_Melee
INT g_iNumberOfWeaponPickupsToBeCreated_Projectile
INT g_iNumberOfWeaponPickupsToBeCreated_Gun
INT g_iTotalNumberOfWeaponPickupsToBeCreated

PICKUP_INDEX g_FMWeaponPickup[FM_MAX_NUMBER_OF_WEAPON_PICKUPS]
INT g_iFMWeaponPickupSpawnLocation[FM_MAX_NUMBER_OF_WEAPON_PICKUPS]
INT g_iFMWeaponPickupState[FM_MAX_NUMBER_OF_WEAPON_PICKUPS]
BLIP_INDEX g_FMWeaponPickupBlipID[FM_MAX_NUMBER_OF_WEAPON_PICKUPS]


BOOL g_bFMWeaponPickupsDisabled

TWEAK_FLOAT FM_WEAPON_PICKUP_Z_OFFSET 0.3

#IF IS_DEBUG_BUILD

	BOOL g_bSkipCashTransaction
	BOOL g_bFMWeaponPickup_ResetEverythingNow
	BOOL g_bFMWeaponPickup_DeleteAllClientPickupsNow
	BOOL g_bRenderAllFMWeaponPickupLocations_Other
	BOOL g_bRenderAllFMWeaponPickupLocations_Melee
	BOOL g_bRenderAllFMWeaponPickupLocations_Projectile
	BOOL g_bRenderAllFMWeaponPickupLocations_Gun
	BOOL g_bRenderAllFMWeaponPickupLocations_Active
	
	BOOL g_bFMWeaponPickupLocations_BlipsCreated_Other
	BOOL g_bFMWeaponPickupLocations_BlipsCreated_Melee
	BOOL g_bFMWeaponPickupLocations_BlipsCreated_Projectile
	BOOL g_bFMWeaponPickupLocations_BlipsCreated_Gun
	BOOL g_bFMWeaponPickupLocations_BlipsCreated_Active
	
	BOOL g_bDebugLinesActive
	
	BLIP_INDEX g_DebugWeaponBlip_Other[FM_WEAPON_PICKUPS_MAX_OTHERS]
	BLIP_INDEX g_DebugWeaponBlip_Melee[FM_WEAPON_PICKUPS_MAX_MELEE]
	BLIP_INDEX g_DebugWeaponBlip_Projectile[FM_WEAPON_PICKUPS_MAX_PROJECTILE]
	BLIP_INDEX g_DebugWeaponBlip_Gun[FM_WEAPON_PICKUPS_MAX_GUN]
	BLIP_INDEX g_DebugWeaponBlip_Active[FM_MAX_NUMBER_OF_WEAPON_PICKUPS]
	
#ENDIF


CONST_INT MAX_NUM_YACHT_WEAPON_PICKUPS 5
BOOL g_bLastYachtWeaponPickedUp[MAX_NUM_YACHT_WEAPON_PICKUPS]
INT g_iLastYachtWeaponCreateTime[MAX_NUM_YACHT_WEAPON_PICKUPS]
PICKUP_INDEX g_pickupYachtWeapon[MAX_NUM_YACHT_WEAPON_PICKUPS]


CONST_INT MAX_NUM_BUNKER_WEAPON_PICKUPS 4
BOOL g_bLastBunkerWeaponPickedUp[MAX_NUM_BUNKER_WEAPON_PICKUPS]
INT g_iLastBunkerWeaponCreateTime[MAX_NUM_BUNKER_WEAPON_PICKUPS]
PICKUP_INDEX g_pickupBunkerWeapon[MAX_NUM_BUNKER_WEAPON_PICKUPS]

CONST_INT MAX_NUM_HANGAR_WEAPON_PICKUPS 3
BOOL g_bLastHangarWeaponPickedUp[MAX_NUM_HANGAR_WEAPON_PICKUPS]
INT g_iLastHangarWeaponCreateTime[MAX_NUM_HANGAR_WEAPON_PICKUPS]
PICKUP_INDEX g_pickupHangarWeapon[MAX_NUM_HANGAR_WEAPON_PICKUPS]

CONST_INT MAX_NUM_DEFUNCT_BASE_WEAPON_PICKUPS 6
BOOL g_bLastDefunctBaseWeaponPickedUp[MAX_NUM_DEFUNCT_BASE_WEAPON_PICKUPS]
INT g_iLastDefunctBaseWeaponCreateTime[MAX_NUM_DEFUNCT_BASE_WEAPON_PICKUPS]
PICKUP_INDEX g_pickupDefunctBaseWeapon[MAX_NUM_DEFUNCT_BASE_WEAPON_PICKUPS]

BOOL bResetDefunctBaseClutterESTimer = FALSE

CONST_INT MAX_NUM_NIGHTCLUB_WEAPON_PICKUPS 1
BOOL g_bLastNightclubWeaponPickedUp[MAX_NUM_NIGHTCLUB_WEAPON_PICKUPS]
INT g_iLastNightclubWeaponCreateTime[MAX_NUM_NIGHTCLUB_WEAPON_PICKUPS]
PICKUP_INDEX g_pickupNightclubWeapon[MAX_NUM_NIGHTCLUB_WEAPON_PICKUPS]

CONST_INT MAX_NUM_BUSINESS_HUB_WEAPON_PICKUPS 5
BOOL g_bLastBusinessHubWeaponPickedUp[MAX_NUM_BUSINESS_HUB_WEAPON_PICKUPS]
INT g_iLastBusinessHubWeaponCreateTime[MAX_NUM_BUSINESS_HUB_WEAPON_PICKUPS]
PICKUP_INDEX g_pickupBusinessHubWeapon[MAX_NUM_BUSINESS_HUB_WEAPON_PICKUPS]

CONST_INT MAX_NUM_SUBMARINE_WEAPON_PICKUPS 8
BOOL g_bLastSubmarineWeaponPickedUp[MAX_NUM_SUBMARINE_WEAPON_PICKUPS]
INT g_iLastSubmarineWeaponCreateTime[MAX_NUM_SUBMARINE_WEAPON_PICKUPS]
PICKUP_INDEX g_pickupSubmarineWeapon[MAX_NUM_SUBMARINE_WEAPON_PICKUPS]
BOOL g_bSetSubmarineWeaponPickupsOwned = TRUE

#IF FEATURE_TUNER
CONST_INT MAX_NUM_INTERIOR_WEAPON_PICKUPS						10
CONST_INT MAX_NUM_INTERIOR_WEAPON_PICKUPS_COOLDOWN_TIME_SECS	600 // 10 mins
BOOL bSetInteriorWeaponPickupsOwned = TRUE

STRUCT INTERIOR_WEAPON_PICKUP_PLACEMENT_DATA
	VECTOR vPosition
	VECTOR vRotation
	PICKUP_TYPE ePickupType			= PICKUP_TYPE_INVALID
	WEAPON_TYPE eWeaponType			= WEAPONTYPE_INVALID
	MODEL_NAMES eCustomPickupModel 	= DUMMY_MODEL_FOR_SCRIPT
	INT iAmmoCount					= 0
	INT iCatalogueKey				= -1
ENDSTRUCT

//STRUCT INTERIOR_WEAPON_PICKUP_DATA
//	PICKUP_INDEX PickupWeapon
//	INT iPickupFlags
//	INT iLastWeaponCreateTime
//	BOOL bLastWeaponPickedUp
//	INTERIOR_WEAPON_PICKUP_PLACEMENT_DATA PlacementData
//ENDSTRUCT
////INTERIOR_WEAPON_PICKUP_DATA g_AutoShopWeaponPickupData[MAX_NUM_INTERIOR_WEAPON_PICKUPS]
#ENDIF // FEATURE_TUNER


MULTIPLAYER_SETTING_TITLE g_Private_CrewTitleSettings_LastFrame

CONST_INT MAX_NUM_SAVE_QUEUE  10

SAVE_TYPE g_bPrivate_SaveRequested[MAX_NUM_SAVE_QUEUE]
SCRIPT_SAVE_REQUEST_REASON g_ePrivate_SaveReason[MAX_NUM_SAVE_QUEUE]

SCRIPT_TIMER g_ClearCharacters_Headshots

INT g_iExternalSkycamUpStages
BOOL g_bHideSwoopUpUntilPanDown
BOOL g_bHideSwoopUpForInteriorsEnabled 				= TRUE
BOOL g_bHideSwoopUpForInteriors_ArenaOnly			= TRUE	//Instanced missions
BOOL g_bHideSwoopUpForInteriors_ArenaGarage 		= TRUE
BOOL g_bDoSkySwoopUpImmediateFadeOutIfNecessary 	= TRUE
BOOL g_bHideSwoopUpForCasinoInstInterior			= TRUE	//Instance missions
BOOL g_bHideSwoopUpForAllCasinoInteriors	 		= TRUE	//Freemode simple interiors
BOOL g_bHideSwoopUpForAllDisplacedInteriors			= TRUE	
BOOL g_bHideSwoopUpStarted
INT g_iSwoopUpTimer = -1
BOOL g_bDoHideSwoopUpForInteriorsCleanupInNetSpawn = TRUE

BOOL g_bHideSwoopDownStarted
INT g_iSwoopDownTimer

BOOL g_bDisableFadeInAfterRespawnWhenMissionStrandInitialising	= TRUE

#IF IS_DEBUG_BUILD
BOOL g_b_SaveHeistDebug
ScriptCatalogItem sCatalogData
#ENDIF

#IF FEATURE_GEN9_EXCLUSIVE
BOOL g_bForceFadeOutForMPIntro = TRUE
#ENDIF

BOOL g_b_HaveStatsInitialised

BOOL g_b_AutojoinCameraInSky

BOOL g_b_HasSkippedTutorialDueToNoMoreTutorials = FALSE

BOOL g_b_DidBailWhenEnteringOnlineFromBoot

BOOL g_bDisableCapChecker = FALSE


BOOL g_bBroadcastAppEnabled = FALSE //Was TRUE - requested to be commented out by Les TODO #1620959
BOOL g_bBroadcastAppOn
BOOL g_bBroadcastAppSuspended = FALSE

BOOL g_bTurnOnCrewTShirtAtEntry = FALSE

INT g_Private_Players_Current_crew_ID

BOOL bMinigamePlayerQuitFlag

BOOL g_b_Private_DeleteHeadshot[MAX_NUM_CHARACTER_SLOTS]
BOOL g_b_Private_DeleteHeadshotWithSlot = FALSE


BOOL g_b_BikeAppStatChange
BOOL g_b_BikeAppStatChangeMajor

BOOL g_b_CasinoPrioritySave
BOOL g_b_CasinoSave
BOOL g_b_DisableCasinoSaveManagement = FALSE

BOOL g_B_ShowingStreetName_LastFrame
BOOL g_B_ShowingAreaName_LastFrame
BOOL g_B_ShowingVehicleName_LastFrame
BOOL g_Private_SP_SelectorSkycamUp
BOOL g_Private_SP_PauseMenuSkyCamUp
BOOL g_Private_MP_PauseMenuSkyCamUp
BOOL g_Private_MP_SwapCharactersSkyCamUp
BOOL g_Private_SP_PauseMenuSkyCamUpDuringMissions

INT g_i_private_BirdsAndBeesAlphaStages
INT g_i_Private_BirdsAndBeesAlpha
INT g_i_Private_BirdsAndBeesColourDiff = 130
INT g_i_Private_BirdsAndBeesRectAlpha
SCRIPT_TIMER g_st_Private_BirdsAndBeesTimer
INT g_i_Private_BirdsAndBeesAlphaRate = 30

TEXT_LABEL_31 g_s_PlayersRadioStation

BOOL g_b_IsPlayerPedBigfoot
INT g_i_BigfootTransformStages
SCRIPT_TIMER g_st_BigfootFadeoutTimer
INT iMaxHealthBeforebigfoot


INT g_b_Private_NumberofAttemptsToChangeAimType 

INT g_B_Private_accepted_Invite_AimType 

BOOL g_bPlayEndOfJobRadioFrontEnd
INT g_iPlayEndOfJobRadioFrontEndState

BOOL g_bStartedPreLoadingSaveMigrationStatusCheck

BOOL g_Private_HasCloudChokedDuringTransition

VECTOR g_vPlayerVectorOnLeavingCorona
VECTOR g_vSafeLocationOutsideApartment
FLOAT g_fSafeLocationOutsideApartmentHeading

BOOL g_b_Private_Emergency_SkyCamup_Invite_while_spectating

// Cellphone Title Update Globals.
BOOL g_CellphoneHasMPAccess = FALSE  //When the phone is launched it will check for online availability in order to decide whether to display Joblist or Checklist.

BOOL g_b_bBlockNewGameButBlockedSkyCam = FALSE

// Building controller TU globals.
BOOL g_bForceBuildingControllerToClearDoorsInInit

BOOL g_b_Private_IsTutorialCutsceneGoingToRun
BOOL g_Private_IsScriptCloudDownStoppingSocialMatchmaking

BOOL g_b_is_ScriptBailProcessing
BOOL g_b_do_GoodBehaviourCashRewardTicker
INT g_b_do_GoodBehaviourCashRewardAmount
BOOL g_b_QuitActivityThroughSelector
BOOL g_b_QuitActivityThroughPauseMenu

BOOL g_QF_bForceAwardFlowOrder
BOOL g_QF_bForceAwardSameTeam
BOOL g_QF_bForceAwardUltimate
BOOL g_QF_bForceAwardMember
BOOL g_QF_bForceAwardFirstPerson

//INT g_B_LastGenSavesChecksStages

BOOL g_b_PauseMenuMigrateSaves

BOOL g_bUsedRPBoost = FALSE

BOOL g_b_RunningPostMigrationCheck

BOOL g_b_IsOnPolicyScreen

BOOL g_b_ChangePlayerNameToTeamName
BOOL g_b_UseBottomRightTitleColour

BOOL g_b_DoPropertySkyCam = FALSE

BOOL g_b_DoReturnTo_SP_RELOAD = FALSE

BOOL g_b_NeedToJoinSessionFromInvite

INT g_i_ServerRequestDelayTime

#IF IS_DEBUG_BUILD
BOOL g_b_DoPropertySkyCamWidget 
BOOL g_b_HoldUpDeleteThirdStage

BOOL g_bTurnOnWarningShots
SCRIPT_TIMER g_st_WarningShotstimer_Debug
INT g_b_WarningShots_SphereIndex
#ENDIF

VECTOR g_b_PropertyExteriorCamDownPos

VEHICLE_INDEX g_vi_LastVehicleUsedForInvite

INT g_b_EnteredMaintransitionSelectorBitset
BOOL g_b_InTransitionFromCharacterSelectEventListeners

BOOL g_bUseNetIndexForConceal = FALSE

INT g_iPlayerUsingArenaBoxSeat = -1
BOOL g_bPlayerUsingArenaSpectatorTable = FALSE

INT g_iPlayerUsingOfficeSeat = -1
BOOL g_bCancelOfficeSnacksMenu = FALSE
BOOL g_bEnableCustomWeaponLoadout = FALSE
BOOL g_bDontCrossRunning= FALSE
BOOL g_bOwnerPreviewingInterior = FALSE

BOOL g_bPlayerUsingBedActivity = FALSE
FLOAT g_fSubmarineTorpedoZLimit = 1.5

STRUCT OFFICE_HELI_DOCK_GLOBAL_DATA
	BOOL bDoingCutscene // Set if player is bcurrently doing heli dock cutscene
	BOOL bEnteredPropertyViaLanding // Set in am_mp_property_int if player entered office via landing
	BOOL bDoRecreateHeli // After we leave the office it is determined if we should try to recreate heli
	BOOL bRecreatePegasusAfterLeavingOffice
	BOOL bRecreateBossLimoAfterLeavingOffice
	BOOL bRecreateAmbientHeliAfterLeavingOffice
	NETWORK_INDEX netRecreatedAmbientHeli
	VECTOR vRecreateSpawnPoint
	BOOL bOwnerWantsToQuickWarpToOffice // Set when we recieve warp to office event from PA helicopter pickup
	INT iPropertyToQucikWarp = -1
	BOOL bBossWantsToTakeoff // Set when we recieve take off event
	BOOL bOwnerWantsToLand // Set when we recieve landing event
	
	BOOL bEnteringOfficeUsingQuickTravel
	SCRIPT_TIMER timerLastBroadcastSentAt
	
	//INT iBossRequestedPegasusHeliType = -1
	SITE_BUYABLE_VEHICLE ePegasusBuyableVehicle = UNSET_BUYABLE_VEHICLE
	SITE_BUYABLE_VEHICLE_COLOURS ePegasusBuyableVehicleColour = BCV_NO_COLOUR
	
	BOOL bCreateHeliForTakeoff // Heli for takeoff is created by am_vehicle_spawn on owners side, this is used to let that script know we want a heli
	BOOL bHeliForTakeoffCreated
	INT iInvolvedPlayersFlags
	
	INT iPropertyEnteredViaLanding = -1
	PLAYER_INDEX playerOfficeOwner
	PLAYER_INDEX playerDesignatedDriver
	
	BOOL bCalledFromHeliTaxi
	BOOL bHeliTaxiShouldCleanUpThePilot
	
	VEHICLE_SETUP_STRUCT_MP heliSetup
	SCRIPT_TIMER timerCooldownBeforeCanLand // This is started after transitioning from office to helicopter, cooldown before we can start heli-> office transition
	
	BOOL bTakeoffWarpDone // Set when the event form whoever is doing the warp comes through
	
	BOOL bShowLandingPrompt
	
	structPedsForConversation PAPilotAnnouncer // this really shouldnt be here...
	
	INT iOfficeBuildingSearchStagger
	
	BOOL bTryToUnlockHeliWhenGotControl
	
	BOOL bTakeoffWasAborted // set when takeoff was aborted so the interior script doesnt finish and instead resets to using preoprty
	
	BOOL bDisableLanding = FALSE
	
	BOOL bUsedSVMHeliStart // this is saved if starting SVM we used heli exit
	BOOL bFirstSVMHeliCutsceneDone
	GAMER_HANDLE ghDesignatedSVMPilot
	BOOL bCleanupSVMHeli
	VEHICLE_INDEX vehSVMHeli
	SCRIPT_TIMER timerWarpToSVMHeliOnQuickRestart
	INT iSVMHeliCreationStage
	BOOL bGetInSVMHeli
	SCRIPT_TIMER timerResendGetInSVM
	BOOL bMissionControllerInProcessIntro
	
	BOOL bLaunchingFromSIOnMission = FALSE
	
	#IF IS_DEBUG_BUILD
	BOOL d_bForceDebugSVMSeatsNoDriver
	BOOL d_bEveryoneUsesAnyPassenger
	#ENDIF
ENDSTRUCT
OFFICE_HELI_DOCK_GLOBAL_DATA g_OfficeHeliDockData

CONST_INT CI_HELI_DOCK_MAX_INVOLVED_PLAYERS 10
CONST_INT CI_HELI_DOCK_MAX_PED_ACTORS		10
CONST_INT CI_MAX_NUM_OFFICES				4

/// PURPOSE: Heli docking (landing/takeoff) states
ENUM OFFICE_HELI_DOCK_STATE
	HELI_DOCK_STATE_INITIALISE,
	HELI_DOCK_STATE_INACTIVE,
	HELI_DOCK_STATE_IDLE,
	HELI_DOCK_STATE_START_LANDING_CUTSCENE,
	HELI_DOCK_STATE_LOADING_LANDING_CUTSCENE,
	HELI_DOCK_STATE_PLAYING_LANDING_CUTSCENE,
	HELI_DOCK_STATE_CANCELING_LANDING,
	HELI_DOCK_STATE_WAIT_FOR_ENTRANCE,
	
	HELI_DOCK_STATE_START_TAKEOFF_CUTSCENE,
	HELI_DOCK_STATE_WAIT_FOR_HELI_CREATION,
	HELI_DOCK_STATE_LOADING_TAKEOFF_CUTSCENE,
	HELI_DOCK_STATE_WAITING_FOR_PEDS_TO_SETTLE_IN_TAKEOFF_CUTSCENE,
	HELI_DOCK_STATE_PLAYING_TAKEOFF_CUTSCENE,
	HELI_DOCK_STATE_FINISHING_TAKEOFF_CUTSCENE,
	HELI_DOCK_OWNER_IS_CREATING_HELI
ENDENUM

/// PURPOSE: ALl the data and variables needed for running heli dock prompt and animation and takeoff
STRUCT PROPERTY_HELI_DOCK_DATA
	OFFICE_HELI_DOCK_STATE eState = HELI_DOCK_STATE_IDLE
	INT iBS
	VECTOR vDockPosition
	FLOAT fDockHeading
	INT iBuildingID = -1
	INT iPropertyToEnter = -1
	INT iEntranceToUse = -1
	PLAYER_INDEX pDriverPlayerIndex
	PLAYER_INDEX pOwnerPlayerIndex // Only owner can initiate heli dock from the office
	PED_INDEX pedIndexOwner
	VECTOR vLandingSpot // where we'll task the heli to land and where it will be created on takeoff
	
	SCRIPT_TIMER timerLoadscene // after what time loadscene times out
	
	MODEL_NAMES eHeliModel
	CAMERA_INDEX camLanding
	VEHICLE_INDEX vehLandingClone
	VEHICLE_INDEX vehPlayerHeli
	PED_INDEX pedActors[CI_HELI_DOCK_MAX_PED_ACTORS]
	INT iPedActorsCount
	VEHICLE_SEAT vsPedActorSeats[CI_HELI_DOCK_MAX_PED_ACTORS]
	//NETWORK_INDEX netVehPlayerHeli
	INT iServerPlayerHeliCreationStage
	VEHICLE_SEAT vsOriginalSeat // Where local player was initially sitting/ should be sitting in case of cancelling 
	
	PLAYER_INDEX pInvolvedPlayers[CI_HELI_DOCK_MAX_INVOLVED_PLAYERS] // List of players involved in heli dock, driver is guaranteed to be at index 0
	INT iInvolvedPlayersCount
	INT iInvolvedPlayersOwnersBitset // bits are turned on if player owns office we are hovering about
	
	VECTOR vPlayerCoords
	FLOAT fPlayerHeading
	SCRIPT_TIMER cutsceneTimer
	TIME_DATATYPE timeDataCutsceneStart
	
	VECTOR vDockDirection
	VECTOR vDockCameraPosition
	
	SCRIPT_TIMER timerWaitForInvite // If players timeout waiting for the invite from the owner then we can cancel landing
	
	BOOL bInviteSent // this is used in exterior script to mark that broadcast invite has been sent by the owner. Should probably move this somewhere else (TODO:)
	
	#IF IS_DEBUG_BUILD
		// These are used to monitor state of global player bd
		INT d_iGlobalPlayerBD_ReadyForCutscene
		INT d_iGlobalPlayerBD_CutsceneDone
		INT d_iGlobalPlayerBD_WarpDone
		INT d_iGlobalPlayerBD_DoingCutscene
		INT d_iGlovalPlayerBD_ReadyForQuickTravel
		
		BOOL d_bDelayedLandingStart
		SCRIPT_TIMER d_timerDelayedStart
		
		BOOL d_bDelayBeforeAcceptingBroadcast
		SCRIPT_TIMER d_timerBeforeAcceptingBroadcast
		
		BOOL d_bDontSendBroadcastInvite
		BOOL d_bDontLockHeliDoor
		BOOL d_bToggleDrawSpawnPoints
		BOOL d_bDrawSpawnPoints = FALSE
		BOOL d_bForceRecreateHeli
		
		FLOAT d_fApproachAngle
		FLOAT d_fOldApproachAngle
		
		BOOL d_bFailLanding
		BOOL d_bFailLandingAfterHeliCleanup
		
		BOOL d_bSimulateOwnerDeadOnHeliCreation
	#ENDIF
	
	SCRIPT_TIMER forceRemovalTimer // this is for when player who is responsible for helicopter removal doesnt do it and times out and someone else has to do it
	SCRIPT_TIMER cancelControlTimer // timer for taking over control entity and canceling landing if nobody else has done it till now
	SCRIPT_TIMER taskEnterVehicleTimer // After we task ped with that task once we want to wait a bit before taking them again if unsuccessful
	
	SCRIPT_TIMER timerCargobobAttachmentCooldown
	
	VECTOR vCancelWarpCoord
	//structPedsForConversation PAPilotAnnouncer // TODO: taht struct is global now, move it somewhere elseeee
ENDSTRUCT

PROPERTY_HELI_DOCK_DATA g_ExteriorHeliDockData // Moved from am_mp_property_ext due to stack overflow

STRUCT HELI_TAXI_QUICK_TRAVEL_GLOBAL_DATA
	BOOL bDoingQuickTravelWarp
	INT iQuickWarpState 
	TIME_DATATYPE tQuickWarpTime
ENDSTRUCT
HELI_TAXI_QUICK_TRAVEL_GLOBAL_DATA g_HeliTaxiQuickTravelData


ENUM HEIST_PROPERTY_CAM_STAGE
	HEIST_PROPERTY_START,
	HEIST_PROPERTY_RUN_CAMERA_DOWN,
	HEIST_PROPERTY_SETUP_STATIC_CAM,
	HEIST_PROPERTY_CLEANUP_CAMERA_DOWN,
	HEIST_PROPERTY_HOLD_STAGE,
	HEIST_PROPERTY_LOAD_INTERIOR,
	HEIST_PROPERTY_WAIT_FOR_LOAD,
	HEIST_PROPERTY_CLEANUP,
	HEIST_PROPERTY_FINISHED
ENDENUM

HEIST_PROPERTY_CAM_STAGE HeistPropertyCamStages
BOOL MoveOnToCleanupHeistPropertyCamera
BOOL bHaveStartedSkycamCleanup
SCRIPT_TIMER HeistCameraLoadSceneTimeout
INTERIOR_INSTANCE_INDEX HeistApartment
INTERIOR_INSTANCE_INDEX HeistGarage
CAMERA_INDEX HeistPropertyCamera
VECTOR HeistPropertyCameraCoords
BOOL bUseHeistTransitionInviteMic

INT g_HeistPropertyCamPosID = -1


SCRIPT_TIMER st_ScriptTimers_BIG_MESSAGE_CashDisplay

INT g_TRANSITION_SPAWN_PLAYER_FM_stages_Finaloutput

BOOL g_b_EnableFocusDisconnectHeliCam = TRUE

BOOL g_b_StartingInApartmentForQuickRestart
BOOL g_b_ReadyToWalkOutApartmentForQuickRestart
BOOL g_b_AllPlayersReadyForApartmentQuickRestart
BOOL g_b_HasCheckedApartmentInfo

BOOL g_b_TriggerLaunchInteriorQuickRestart

STRUCT ScratchApartmentData
	INT iPackedInt  = 0
	GAMER_HANDLE ownerHandle
	BOOL bPlaceInApartment = FALSE
	VECTOR vApartmentCoords
	INT iApartmentLocation = -1
	INT iInvitedPropertyNum = -1
	INT iInvitedPropertyInstance = -1
	INT iInvitedPropertyEntrance = -1
ENDSTRUCT
ScratchApartmentData sScratchData


BOOL g_bHasEnteredFreshGameFromSP
BOOL g_B_DidEveryoneQuitAndLeaveYouOnActivity

BOOL g_b_QuitGameOnSpectator

BOOL g_bForceMissionControllerMaxPlayerCount = FALSE
BOOL g_bForceLegacyMissionControllerMaxPlayerCount = FALSE
BOOL g_bForceLegacyMissionControllerMaxZoneCount = FALSE
BOOL g_bForceLegacyDoorHashChecks = FALSE
BOOL g_bReenableHUDCheckJohnThinksIsBad = FALSE

BOOL g_b_Private_IsCharacterNewForUnlocks
BOOL g_b_Private_CharacterGenderHasChanged
INT g_b_Private_NewCharacterEyeValue

BOOL g_b_Private_Kick_player_to_switch_to_offline_mode

BOOL g_b_Private_Kick_player_to_switch_to_offline_Until_reboot

// Carmod shop TU globals
BOOL g_bAllowAllPremiumVehiclesToBeModified = FALSE

STRUCT_CORONA_VEHICLE_WORLD_CHECK_DATA g_structCoronaVehicleWorldCheck

INT g_iPropertyOptionsDisplayResetValue = -1

// Menu TU globals.
TEXT_LABEL_63 g_TUMenuGlobals_tlPlayerNameItem[MAX_STORED_MENU_PLAYER_NAMES]

// Distance for the Map ZOom in the Corona
FLOAT			g_fRaceRadarDistance

INT 			g_iSaveTypeForStore

BOOL g_bMissionReadyForAM_VEHICLE_SPAWN = FALSE
BOOL g_bMissionReadyForIslandWater = FALSE
BOOL g_bMissionLoadingIsland = FALSE

BOOL g_I_MAX_XP_CAP_DISPLAY

BOOL g_b_ProcessedStartingCheatTracker

BOOL g_bFinishedInitialSpawn

BOOL g_b_IdleKickHidefeed
BOOL g_b_IdleKickIsFeedPausedChecked

BOOL g_b_DoSlotReloadBackInSP[MAX_NUM_CHARACTER_SLOTS+1]

BOOL g_b_SkycamBootLoading

#IF FEATURE_SHORTEN_SKY_HANG
TIME_DATATYPE g_HangingInSkyTimer
INT g_iHangingInSkyState
BOOL g_bLastSkySwoopDownWasTruncated
BOOL g_bSkySwoopDownDidFadeOut
BOOL g_bPMCIsSwitchingToPlayer
BOOL g_bDoneHangingAssert
INT g_HangingInSkyFrameCounter
INT g_HangingInSkyFrameCounter_Start
#ENDIF


BOOL g_b_InvitesWhileOnReplay

PLAYER_INDEX EventHudPlayerIndexLastFrame[5]
TEXT_LABEL_63 EventHudPlayerNameLastFrame[5]

BOOL g_b_maintransitionNeedsHeistCamToRun

SCRIPT_TIMER g_b_TimeForSaveFailCheck
INT g_i_NumberOfSaveFails
BOOL g_b_HasFailedtoSaveInTime
BOOL g_b_WasSavingDown

TRANSITION_FM_SESSION_MENU_CHOICE g_Private_Players_FM_SESSION_Menu_Choice_PERSIST = FM_SESSION_MENU_CHOICE_EMPTY

BOOL g_b_TurnOnSRLStreaming

BOOL g_Private_HasScriptTimedOutJoining
PED_INDEX PlayerSwitchLeftBehindPed_PROPERTY


INT g_iVehicleSpawnRequestPlayer
INT g_iVehicleSpawnRequestRequest
INT g_iVehicleSpawnRequestVSID

BOOL g_b_IsFreemodeRunningActive
SCRIPT_TIMER g_st_IsFreemodeRunningTimer
BOOL g_B_TurnOnIsFreemodeRunningChecks = TRUE
INT g_i_IsFreemodeRunningTimeoutTime = 15000

INT g_bSaveFAiledMessageForce_Stages
SCRIPT_TIMER g_st_SaveFailedMessageForce
BOOL g_b_SaveFAiledHidefeed
BOOL g_bEnableStatsReloadOnFMEntry = FALSE
BOOL g_bDoneFullStatsReload = FALSE

BOOL g_bStreetTimerBusySpinner_LastFrame

BOOL g_b_ReapplyStickySaveFailedFeed
SCRIPT_TIMER g_st_SaveFailedMessageDELAYForce

BOOL g_b_WasBailedtoReboot

BOOL g_b_SaveClothesSafetyCheckDisable
BOOL g_b_SaveClothesMaxSafetyCheckDisable

INT g_bCASHGIFTForce_Stages
SCRIPT_TIMER g_st_CASHGIFTForce
BOOL g_b_CASHGIFTHidefeed

//INT g_bBirthdayCASHGIFTForce_Stages
//SCRIPT_TIMER g_st_BirthdayCASHGIFTForce
//BOOL g_b_BirthdayCASHGIFTHidefeed
//INT g_i_DoCashBirthdayGiftMessageAmount
//TEXT_LABEL_63 g_tl_CashBirthdayGiftAmount 



BOOL g_b_MainTransition_TransitionSessionJoining

INT g_bRPGIFTForce_Stages
SCRIPT_TIMER g_st_RPGIFTForce
BOOL g_b_RPGIFTHidefeed

SCRIPT_TIMER g_st_SETRPGIFTADMINForce
BOOL g_b_SETRPGIFTADMINHidefeed

BOOL g_b_ShownTransferSuccessfulScreen

INT g_iVehicleSpawnCompletePlayer
INT g_iVehicleSpawnCompleteRequest
INT g_iVehicleSpawnCompleteVSID

INT g_iExternalWarningScreen_Compat_stages
BOOL g_bAlreadyAskedForCompatInvite

INT g_playlist_ArmourGiven
INT g_playlist_reward_item[50]
BOOL g_b_SelectedToJoinGameAsSCTV

BOOL g_b_ForceRunPreMigrationCheckAfterFail

INT g_i_DoCashGiftACTUALAmount
INT g_i_DoCashGiftMessageAmount
TEXT_LABEL_63 g_tl_CashGiftAmount 
BOOL g_b_CashGiftFixedCrazyNumbers
TEXT_LABEL_63 g_tl_CashGiftServerString
BOOL g_b_skipCashGiftMessage
BOOL g_b_DoneScFeedCashGiftMessage
BOOL g_b_HasItemBeenGivenOnPc
INT g_i_SCAdminCashGiftScreenType


#IF IS_DEBUG_BUILD
BOOL g_b_SetupFakePCGiftsWidget
BOOL g_b_IsFakePCGiftsWidgetActive
INT g_i_CashGiftWidgetAmount
INT g_i_DoCashGiftACTUALAmountWidget
BOOL g_b_HasItemBeenGivenOnPcWidget
INT g_i_SCAdminCashGiftScreenTypeWidget
#ENDIF


BOOL g_b_LeftPlayersflyingHelisIntoSunset = TRUE

QUICKFIX_ELEMENT g_iWhichQuickFixElement[MAXQUICKFIXFEED]
TEXT_LABEL_31 g_tl_QuickFixElement[MAXQUICKFIXFEED]
TEXT_LABEL_31 g_tl_QuickFixTitle[MAXQUICKFIXFEED]
BOOL g_b_QuickFixRankBoost[MAXQUICKFIXFEED]

BOOL g_b_MPMapIsLoaded
BOOL g_b_MPMapIsLoaded_PlusCCS

INT g_b_DoRPGiftMessageAmount
INT g_b_DoSetRPGiftAdminMessageAmount[MAX_NUM_CHARACTER_SLOTS]

BOOL g_b_HasSystemServiceTriggered

BOOL g_bStoreTransitionForCash
BOOL g_bStoreTransitionForCashRenderPaused

// Post new character to FACEBOOK
INT g_iNumberOfCharacterFacebookAttempts
TIME_DATATYPE g_timeForNextCharacterFacebookPost

// Limit respawn of pickups to prevent SP exploit - B*1640522
TIMEOFDAY g_todNextBriefcaseSpawn = INVALID_TIMEOFDAY

BOOL g_bHasPegasusVehicleInAirport = FALSE

BOOL g_bHasCheckedPropertyStat
INT g_iWarpingOutOfPropertyWithID

BOOL g_bDisplayYachtInfo = TRUE

BOOL g_bCheckedUtilities

BOOL g_bSaving_Has_Force_Cloud_MP_Stats_Download_Been_Called = TRUE

BOOL g_bMissionOver

INT g_iPostMissionCutsceneOverride[FMMC_MAX_TEAMS]

BOOL g_bLeavingVehInteriorForMCEnd

BOOL g_bPlayerLeftCheckScore

CONST_INT g_ciPlayerLeftRemakeTrapNetPTFX	0
INT g_iBSPlayerLeftDoSomething

CONST_INT ciMMBITSET_OVERTIME_PLAYER_LEFT_CLEAR_AREA_REQUIRED 	0
CONST_INT ciMissionMiscBitset1_OverridingYachtCamShake		 	1
INT g_iMissionMiscBitset1

INT g_iMissionPlayerLeftBS

BOOL g_bHeistCheaterIncremented
BOOL g_bGangopsCheaterIncremented
BOOL g_bCasinoHeistCheaterIncremented
BOOL g_bIslandHeistCheaterIncremented

BOOL g_bPlayerBrowingGunshop = FALSE
BOOL g_bPlayerBrowsingWeaponWorkshop = FALSE
BOOL g_bSafeToRequestHeliPilotForDefunctBaseConcierge = FALSE

BOOL g_bIsRunningBackMission
BOOL g_bTriggerCrossTheLineAudio 

enumCharacterList g_ePreviousMissionVehGenPed
VEHICLE_SETUP_STRUCT g_sPreviousMissionVehGenData

INT g_b_iNumberOfDecimalPlacesForScore = 1

BOOL g_B_Is_Interior_stripper_Active_this_frame

INT g_i_ServerFailedMigrationReason
BOOL g_bFailedMigrationGeneralError

BOOL g_b_HaveLoadsFailedWhileInGame
BOOL g_b_HeadblendNeedsToEditCharacterAgain

CONST_INT iCONST_CHARACTER_APPEARANCE_MINIMUM_RANK	6

//BOOL g_b_PersonalMenuWantstoEditCharacter

SCRIPT_TIMER g_st_SkyCamLoadSceneTimeout

INT g_i_lastSlotLoaded = -999

BOOL g_EnteredCreatorNoSaveMode

INT g_iPrivate_ProcessingWhichSysActivityFeed
BOOL g_bPrivate_IsSystemActivityFeedHappening
SCRIPT_TIMER g_st_Private_SystemActivityFeedTimer

BOOL g_RunObjectiveTextSpam 
BOOL g_HasObjectiveTextActive_LastFrame 
INT g_HasObjectiveTextBusySpinnerActive_LastFrame 

BOOL g_b_RunFriendsInOnlineFeed
INT g_i_NumberOfFriendsInSessionFeed

BOOL g_b_isSelectorAbouttoBootMaintransition 

BOOL g_b_JunkCheckIsTrueButHavingIssues

BOOL g_b_WasIBailedFromSCTVDueToRockstar

BOOL g_b_isMainTransitionProcessingAnSCTVTransitionInvite

BOOL g_b_isMainTransitionBailingTheTransitionSession

BOOL g_b_NextRPAdditionShowRankBar = FALSE

BOOL g_b_HoldOffDrawingTimersThisFrame

BOOL g_Private_HasKickedConstrained

BOOL g_Private_IsMaintransitionPropertyExteriorCamRunning

BOOL g_b_Private_FMeventsHidingCodeUIThisFrame

BOOL g_b_ForceGetLastCoordsForReEntryUpdate

BOOL g_B_Disable_HELI_CAM_Shooting_This_Frame

MIGRATION_PLATFORM_STAGES g_Private_Checked_Stats_Transfer_For_Platform
MIGRATION_TRANSFER_STAGES g_Private_Checked_Stats_Transfer_For_Transfer
SAVE_MIGRATION_MAIN g_Private_Checked_Stats_Transfer_For_Transfer_Both


STRUCT_AVAILABLE_SAVE_DATA g_struct_Save_transfer_data
STRUCT_AVAILABLE_SAVE_DATA g_struct_Save_transfer_data_PS3
STRUCT_AVAILABLE_SAVE_DATA g_struct_Save_transfer_data_XBOX360
STRUCT_AVAILABLE_SAVE_DATA g_struct_Save_transfer_data_PS4
STRUCT_AVAILABLE_SAVE_DATA g_struct_Save_transfer_data_XBONE

INT g_Transition_spawn_PlayerSpawningStages_readonly_final

BOOL g_b_SaveTransfer_GotXbox360
BOOL g_b_SaveTransfer_GotPs3
BOOL g_b_SaveTransfer_AlreadyDone_Xbox360
BOOL g_b_SaveTransfer_AlreadyDone_PS3
BOOL g_b_SaveTransfer_GotXbone
BOOL g_b_SaveTransfer_GotPs4
BOOL g_b_SaveTransfer_AlreadyDone_Xbone
BOOL g_b_SaveTransfer_AlreadyDone_PS4
BOOL g_b_SaveTransfer_ClearStatsLoadedAfterFm
SCRIPT_TIMER g_st_Private_LastGenCharacterRetry
INT g_i_Private_LastGenCharacterAttempt

INT g_i_BadsportstartedEvent

BOOL g_b_HasTransitionInviteChangedAimtype

BOOL g_b_BGHoldUpPreHudStage
BOOL g_b_BGHoldUpPreJoiningStage
BOOL g_b_BGHoldUpPreScriptStage
BOOL g_b_BGHoldUpCreatePlayerStage
BOOL g_b_BGHoldUpMPSwoopDown

BOOL g_b_RunChangeCharactersAppearance
INT g_i_ChangeCharactersAppearanceBitset = 0

#IF IS_DEBUG_BUILD

	BOOL g_b_DisplayPrintsForRadarMap
	
	BOOL g_bAlwaysDisplayImpoundHelp = FALSE
	FLOAT g_fDebug_BadSportPersonalVehicle_VelocityRange = 0.0
	STRUCT_STAT_DATE g_Debug_DateToShaveOffBecomingBadSport
	BOOL g_Debug_ApplyDateToShaveOffBecomingBadSport
	
	
	BOOL g_Debug_FakeTimeout_PrehudStats
	BOOL g_Debug_FakeTimeout_JoiningSession
	BOOL g_Debug_FakeTimeout_WaitingToJoin
	BOOL g_Debug_FakeTimeout_SettingupPlayer
	BOOL g_Debug_FakeTimeout_PreJoiningSession


	BOOL aSavesFailed_LastFrame
	
	SCRIPT_TIMER st_IdleKickThisFramePrint
	
	BOOL g_B_IdleKickPrints
	
	SCRIPT_TIMER SaveFailedFeedDebugTimer
	
	INT g_iCreateGuyAtThisRank = -1
	
	BOOL g_b_displayROSCredentialsDroppedPrint
	
	BOOL g_b_MakeCompatPackCheckFail_Maintransition
	
	BOOL g_b_WidgetMakeAllInviteSCTVInvites
	
	BOOL g_b_ShiftF_FemaleCharacter
	
	BOOL g_b_Debug_HoldSkyCamBeforeDecent
	BOOL g_B_constrainedKickPrints
	
	BOOL g_b_RunSaveTransferScreen
	
	BOOL g_b_DebugRunCharacterCreator
	BOOL g_b_DebugCharacterTestRunning
	BOOL g_b_DebugResetCharacterCreator
	
	BOOL g_b_DoIntroDLCBink
	BOOL g_b_ClearIntroDLCBinkPackedStat
	
#ENDIF

BOOL g_b_DontSpawnAtCasinoAfterBink

#IF FEATURE_COPS_N_CROOKS
BOOL g_b_DontSpawnAtIntroDLCLocAfterBink = TRUE
#ENDIF

BOOL g_b_MaintransitionNeedsHeistToResync
BOOL g_b_MaintransitionNeedsCasinoHeistToResync
BOOL g_b_MaintransitionNeedsIslandHeistToResync

BOOL g_b_BailCasinoHeistAntiCheatIfOffline = TRUE
BOOL g_b_BailIslandHeistAntiCheatIfOffline = TRUE

BOOL g_b_Check_Chat_Restrictions_For_Message
BOOL g_b_Check_UGC_Restrictions_For_Message

BOOL g_b_DisplayCheaterPoolTimeFeedMessage

INT g_i_NumberOfPersonalVehicleReminders 

BOOL g_b_AskedToSkipTutorial

TIME_DATATYPE LoadingAndLobbyTrackerLastFrame

INT g_i_IdleKickTimerOverride = -1
INT g_i_CurrentIdleKickTime

INT LoadingAndLobbySPTimerTrackerLastFrame

BOOL g_b_HoldupSkycamBetweenRounds
BOOL g_b_HoldupCutsceneBetweenRounds

BOOL g_bAllPlayersSCTVProcess
BOOL g_bAllPlayersSCTV

BOOL g_b_KickedForMaintenance

BOOL g_b_KickedForFMRunning

#IF IS_DEBUG_BUILD
BOOL g_b_Debug_MakeFMArrayOverrun
#ENDIF

INT g_i_cashForEndEventShard

INT g_i_previousAimStateBeforeChange = -1
BOOL g_b_previousAimStateChanged

TEXT_LABEL_63 g_st_LastHostName

PLAYER_INDEX g_piPlayerToPayMyHospitalBill
TEXT_LABEL_63 g_tlKillerName
BOOL g_bKillerInPassiveMode

BOOL g_b_IsATMrenderingForTimers
BOOL g_b_skyCamPausedForOutroLastFrame
INT g_i_SetSkyswoopDownLastCalledTime
#IF IS_DEBUG_BUILD
BOOL g_b_SetSkyswoopDownCompleted
#ENDIF
INT g_i_SetSkyswoopUpLastCalledTime
BOOL g_b_Enable_RUN_EXTERNAL_EMERGENCY_SKYCAM_DOWN = TRUE

BOOL g_b_MainTransitionKillFreemodeScripts

BOOL g_b_HasBeenInGTAOThisBoot = FALSE

BOOL g_b_IsPlayerPrivateSCTV = FALSE

BOOL g_b_JoiningFullSessionAsSpectator = FALSE

CREATORMENU g_CreatorMenu_ChoiceStruct

BOOL g_b_TellPlayerTheyAreAlone

BOOL g_bRunHeistIntroShard

INT g_LastTransitionTrackingSubState
SCRIPT_TIMER g_st_LastTransitionTrackingState
SCRIPT_TIMER g_st_LastTransitionTrackingStateOVERALL

INT g_i_SaveTransferFailsNoRetries
BOOL g_b_JoiningBootInvite

BOOL g_b_SwitchSelectorLiveTileInviteShowScreenProcess = FALSE

STRUCT_NETWORK_APP_LAUNCHED g_AppLaunchStructDetails

BOOL g_b_external_sctv_Quit_Has_been_Cancelled

BOOL g_b_TriedToJipSpectatePlayerButFailed

TEXT_LABEL_23 tlSettingSpawnLocation

BOOL g_b_doEndofTransitionSave
BOOL g_b_doEndofTransitionSaveCloudOnly

BOOL g_b_ShowRadarNames
BOOL g_b_DisableRadarMap

BOOL g_b_TellEventsSaveTranferHappened

INT g_i_PlaneDpadIsActive
SCRIPT_TIMER g_st_PlaneDpadNameTimer


VECTOR g_AirDefExplosionCoords
VECTOR g_AirDefGunCoords
INT g_AirDefSphereIndex
INT g_AirDefYachtID

BOOL g_AirDefDia_Gentle
BOOL g_AirDefDia_Aggro
BOOL g_AirDefDia_Warning
INT g_AirDefDia_ActiveForYachtId 

BOOL g_b_RefreshBikerWall

BOOL g_b_EnteringBunkerFromMaintransition

BOOL g_b_IsCurrentContentMadeByAFriend
TEXT_LABEL_63 g_tl_CurrentContentFriendName

#IF FEATURE_GEN9_STANDALONE
BOOL g_b_SkipSignInLoadStatsTimerCheck = TRUE
#ENDIF
#IF NOT FEATURE_GEN9_STANDALONE
BOOL g_b_SkipSignInLoadStatsTimerCheck = FALSE
#ENDIF
SCRIPT_TIMER SignInLoadStatsTimer //To wait the 3 seconds when signing in before rejecting an invite. If the invite prompted the signin. BUG 1812687

BOOL g_b_JoinedQueueReturnToGTAO

BOOL g_b_TankVehicleTextMsgNeedsRefreshed

SCRIPT_TIMER g_st_MPAwardsCoolDownTimer_SaveTransfers
SCRIPT_TIMER g_st_MPAwardsDoneTimer_SaveTransfers

SCRIPT_TIMER g_st_ROSInvalidSelectorTimer

FLOAT g_fFurthestTargetDist = 50.0
FLOAT g_fOldFurthestTargetDist = 50.0
FLOAT g_fPartDis
FLOAT g_fHeightMapHeight = -1.0
BOOL g_b_Do_RadarZoom 
BOOL g_b_CodeZoom

BOOL g_b_PrivateContrainedEvent

BOOL g_b_Private_FreemodeHasMovedItselfIntoRunningState

INT g_ConstrainedFeedID
INT g_constrainedCheckMessageStages
BOOL g_b_constrainedKickIsFeedPausedChecked
BOOL g_b_constrainedKickHidefeed
BOOL g_Private_KickedForConstrained
BOOL g_b_Private_KickedForFailedCashTransaction
INT g_i_Private_KickedForFailedCashTransactionTransactionType

BOOL g_b_Private_DidLaunchLivetileWhileTransitioning

BOOL g_b_HasPlayerAcceptedWelcomeScreen
BOOL g_b_HasPlayerCancelledWelcomeScreen

BOOL g_b_HasPlayerSeenPsPlusPromoScreen
BOOL g_b_HasPlayerAcceptedPsPlusPromoScreen

#IF FEATURE_FREEMODE_ARCADE
CONST_INT LANDING_PAGE_BS_ACCEPTED		0
CONST_INT LANDING_PAGE_BS_SKIPPED		1

INT g_i_LandingPageBS
#ENDIF

ENUM FM_VERSION
	FM_VERSION_ONLINE = 0,
	#IF FEATURE_FREEMODE_ARCADE
	FM_VERSION_ARCADE,
	#ENDIF
	FM_TOTAL_VERSIONS // must go last
ENDENUM
FM_VERSION g_FMVersion

STRUCT SAVE_OVERWRITE_DATA
	INT Stages
	INT CharacterSlot
	INT iTransactionID
	BOOL Successful
ENDSTRUCT

//STRUCT NETWORK_SHOP_CATALOGUE_DATA
//	BOOL HasLoaded
//	INT LoadingStages
//	NET_GAMESERVER_TRANSACTION_STATUS LoadingStatus
//	BOOL HasFailed
//ENDSTRUCT
//
//NETWORK_SHOP_CATALOGUE_DATA g_Private_LoadingNetworkShopCatalogue

MP_SAVED_VEHICLE_STRUCT				QuickFixSavedVehicles[MAX_MP_SAVED_VEHICLES]

STRUCT NETWORK_SHOP_INVENTORY_DATA
	INT Slot
	BOOL HasLoaded
	INT LoadingStages
	NET_GAMESERVER_TRANSACTION_STATUS LoadingStatus
	BOOL HasFailed
	INT ErrorCode
ENDSTRUCT

NETWORK_SHOP_INVENTORY_DATA g_Private_LoadingNetworkShopInventory


STRUCT NETWORK_INIT_SHOP_DATA
	INT LoadingStages
	NET_GAMESERVER_TRANSACTION_STATUS LoadingStatus
	INT ErrorCode
ENDSTRUCT
NETWORK_INIT_SHOP_DATA g_Private_InitNetworkShopInventory

BOOL g_Private_Called_NETWORK_SHOP_INIT_SESSION = FALSE
INT g_Private_Called_NETWORK_SHOP_START_SESSION_WITH_SLOT = -1

ENUM SHOP_LOAD_RESULT
	SHOP_LOAD_NONE = 0,
	SHOP_LOAD_STARTING,
	SHOP_LOAD_RUNNING,
	SHOP_LOAD_LOADED,
	SHOP_LOAD_FAILED,
	SHOP_LOAD_CANCELLED
ENDENUM

INT QuickFixVehicleSplitStages

INTERIOR_INSTANCE_INDEX g_Interior_ModShop[6]
INTERIOR_INSTANCE_INDEX g_Interior_TattooShop[6]
INTERIOR_INSTANCE_INDEX g_Interior_StripClub
INTERIOR_INSTANCE_INDEX g_Interior_Barbers[7]
INTERIOR_INSTANCE_INDEX g_Interior_ClothesShop[15]
INTERIOR_INSTANCE_INDEX g_Interior_Ammunation[11]
BOOL g_bHasInitialisedInteriorInstances

	// Big Feed Player Screens
	CONST_INT ciBIG_FEED_DEFAULT				0	// This is the default news display.  Script shouldn't do anything with this
	CONST_INT ciBIG_FEED_ALL_JOBS_PLAYED		1
	CONST_INT ciBIG_FEED_CONTACT_JOBS_PLAYED	2
	CONST_INT ciBIG_FEED_UNPLAYED_JOB			3
	
	#IF IS_DEBUG_BUILD
		BOOL g_bDisplayBigFeedJobCounts
		INT g_iDisplayBigFeedScriptScreens
		BOOL g_bDebugSkipBigFeedScreen
	#ENDIF

// Data about the HUD colours that have been overwriten by script for part of the game UI
MP_HUD_COLOURS_DATA g_MPHudColourData[TOTAL_MP_HUD_COLOURS_SLOTS]

//for 1676619 to reduce stack for shower
// Variables
STRUCT GLOBAL_SHOWER_VARIABLES
	BOOL 			bSetUpAnimDict		= FALSE
	BOOL			bRestoredClothes	= FALSE
	BOOL 			bWashedBloodOff		= FALSE
	BOOL			bTakenClothesOff	= FALSE
	BOOL			bStartedPtfx		= FALSE
	BOOL			bShowerAudioLoaded 	= FALSE
	BOOL			bStoppedPtFX		= FALSE
	BOOL 			bShowering 			= FALSE
	BOOL			bDoorLocked			= FALSE
	BOOL			bInTransition		= FALSE
	BOOL			bInteriorForceCleanup = FALSE
	BOOL			bInteriorForceCleanupNoFade = FALSE
	BOOL			bInteriorForceCleanupToGar = FALSE
	BOOL			bScreenFadedByForcedClosedShower = FALSE
	BOOL			bShowerScriptJustRunCleanup = FALSE

	// Camera timings
	//FLOAT			fEnterShowerPhase
	FLOAT 			fLookShowerPhase
	FLOAT 			fAboveShowerPhase
	//FLOAT 			fTurnOffWidePhase
	FLOAT 			fExitFramePhase
	FLOAT 			fExitShowerPhase
	FLOAT 			fBathroomWidth

	// PTFX timings
	FLOAT 			fNakedPhase, fNonNakedPhase 
	FLOAT			fPtfxOnPhase, fPtfxOffPhase
	//FLOAT 			fWaterSoundOffPhase

	INT 			iScene //, iLocalScene

	INT 			iWaterSoundID		= -1
	INT 			iCurProperty
	INT 			iSingingDuration 	= 0

	//MODEL_NAMES		mDoorModel
	//NETWORK_INDEX	mDoor

	VECTOR 			vTriggerPos
	FLOAT			fTriggerHead	
	VECTOR 			vTriggerSize

	VECTOR	 		vScenePos				
	VECTOR 			vSceneHead			
			
	VECTOR 			vEnterWideCamPos					
	VECTOR 			vEnterWideCamHead				
	FLOAT 			fEnterWideCamFov	= 64.9935	

	VECTOR 			vLookShowerCamPos					
	VECTOR 			vLookShowerCamHead				
	FLOAT 			fLookShowerCamFov	= 40.0

	VECTOR 			vAboveShowerCamPos					
	VECTOR 			vAboveShowerCamHead				
	FLOAT 			fAboveShowerCamFov	= 32.0

	VECTOR 			vTurnOffWideCamPos					
	VECTOR 			vTurnOffWideCamHead				
	FLOAT 			fTurnOffWideCamFov	= 48.0

	VECTOR 			vExitFrameCamPos					
	VECTOR 			vExitFrameCamHead				
	FLOAT 			fExitFrameCamFov	= 48.0

	VECTOR 			vExitCamPos				
	VECTOR 			vExitCamHead		
	FLOAT 			fExitCamFov			= 50.0

	VECTOR 			vShowerHeadPos		
	VECTOR 			vShowerHeadHead		

	VECTOR 			vSteamPos
	VECTOR 			vSteamHead	

	//VECTOR		vPlayerPos

	//VECTOR		vPlayerPos
	VECTOR			vAreaAPos
	VECTOR 			vAreaBPos

	//VECTOR			vDoorHead
	VECTOR 			vExitShowerPos
	FLOAT 			fExitShowerHeading

	BOOL 			bLastCam = FALSE
	BOOL			bPauseSkyCamForSpawn = FALSE
ENDSTRUCT
GLOBAL_SHOWER_VARIABLES g_showerGlobals


STRUCT STRUCT_CRATE_DROP_F9
	//Server
	INT iServerGameState					//serverBD.iServerGameState
	INT eServerCrateDropStage				//serverBD.eCrateDropStage
	INT iParticpantGotCrate					//serverBD.CrateDropData.CrateData[iCrate].iParticpantGotCrate
	BOOL biCD_Crate_ReadyForDrop			//IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_ReadyForDrop)
	BOOL biCD_Crate_DropDone				//IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_DropDone)
	BOOL biCD_Crate_NoPlayerAtDropLocation	//IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_NoPlayerAtDropLocation)
	BOOL biCD_Crate_LandDone				//IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_LandDone)
	BOOL biCD_Crate_AiSetup					//IS_BIT_SET(serverBD.CrateDropData.CrateData[iCrate].iBitSet, biCD_Crate_AiSetup)
	BOOL biS_SpecialCrateDrop				//IS_BIT_SET(serverBD.iServerBitSet, biS_SpecialCrateDrop)

	//Client
	INT eClientCrateDropStage				//playerBD[PARTICIPANT_ID_TO_INT()].eCrateDropStage
	
	INT iBitSet
ENDSTRUCT
STRUCT_CRATE_DROP_F9 g_sCrateDropF9Data

STRUCT STRUCT_SEC_VAN_F9
	
	INT iLaunchingState
	INt ieLaunchingWaitState
	INT iPlayerCount
	BOOL bRequestedLaunch
	INT iCheckNodesStage
	BOOL bEndedForGarage
	BOOL bSessionBeenRunningOneInGameDay
	BOOL bClockCheckAllowSpawn
	BOOL bInProperty
	BOOL bInGarage
	
ENDSTRUCT
STRUCT_SEC_VAN_F9 g_sSecVanF9Data

CONST_INT biCDF9_CRATE_LOADED				0
CONST_INT biCDF9_CRATE_DOESNT_EXIST			1
CONST_INT biCDF9_CRATE_CAN_REGISTER			2
CONST_INT biCDF9_CHUTE_LOADED				3
CONST_INT biCDF9_CHUTE_CRATE_DOES_EXIST		4	
CONST_INT biCDF9_CHUTE_DOESNT_EXIST			5	
CONST_INT biCDF9_CHUTE_CAN_REGISTER			6
CONST_INT biCDF9_CHUTE_CREATED				7
CONST_INT biCDF9_CRATE_AND_CHUTE_CREATED	8
CONST_INT biCDF9_DEST_SKIP_FOR_SPECIAL		9
CONST_INT biCDF9_DEST_HAS_LOADED			10
CONST_INT biCDF9_DEST_DOESNT_EXIST			11
CONST_INT biCDF9_DEST_CRATE_EXISTS			12
CONST_INT biCDF9_DEST_CAN_REGISTER			13
CONST_INT biCDF9_DEST_CONTROL_CRATE			14
CONST_INT biCDF9_DEST_CREATED				15
CONST_INT biCDF9_DEST_DONE					16
CONST_INT biCDF9_DEST_CANT_REGISTER			17


VEHICLE_SETUP_STRUCT_MP EmptyVehicleStructMP


#IF IS_DEBUG_BUILD
	BOOL g_bGetVehicleSetupMPCalled
	BOOL g_bSetVehicleSetupMPCalled
#ENDIF

AML_ServerBroadcastData g_AML_serverBD

AM_CONTACTS_PlayerBroadcastData g_AMC_playerBD[NUM_NETWORK_PLAYERS]

BOOL g_bRUN_VALIDATE_PLAYERS_CLOTHING_CHECK = FALSE
INT FM_RUN_VALIDATE_PLAYERS_CLOTHING_CHECK_COUNTER = 0

TIME_DATATYPE g_tdCanDrawUnlocksScaleform_Timer
BOOL g_bCanDrawUnlocksScaleform_TimerSet = TRUE

// moved these out of the functions ARE_PLAYERS_ON_SAME_ACTIVE_CLAN, IS_PLAYER_ALREADY_IN_THIS_CLAN and IS_PLAYER_IN_PLAYERS_ACTIVE_CLAN to prevent stack overflows
// 16/04/2016: these are now also used in SHOULD_WAREHOUSE_BE_BLIPPED_THIS_FRAME
NETWORK_CLAN_DESC g_TempClanDescription
NETWORK_CLAN_DESC g_ClanDescription
NETWORK_CLAN_DESC g_otherClanDescription
GAMER_HANDLE g_GamerHandle 
GAMER_HANDLE g_otherGamerHandle  

CONST_INT MAX_FM_HEIST_ACTIVITY_XP 16000
BOOL g_bRUN_SPECIAL_REWARD_UNLOCKS = FALSE

INT g_money_dropped_amount_pending = 0

INT g_iLeaveApartmentReason

STRUCT g_structService_Spend_Cash_Drop_Transaction
	INT iIndex
	INT iCashToDrop
	INT iPlacementFlags
	VECTOR vDropCoords
ENDSTRUCT
g_structService_Spend_Cash_Drop_Transaction g_sCashDropTransaction

CONST_INT biDROP_CASH_TRANSACTION_REQUESTED 0
CONST_INT biDROP_CASH_DECOR 1
INT g_iDropCashBit
INT g_iDropCashDecor

INT g_iYachtBridgeEntryStage



CONST_INT HEIST_FLEECA_JOB_CASH_REWARD_MAX_CAP				 300000
CONST_INT HEIST_PRISON_BREAK_CASH_REWARD_MAX_CAP			 1000000
CONST_INT HEIST_HUMANE_LABS_RAID_CASH_REWARD_MAX_CAP		 1500000
CONST_INT HEIST_SERIES_A_FUNDING_CASH_REWARD_MAX_CAP		 1500000
CONST_INT HEIST_PACIFIC_STANDARD_JOB_CASH_REWARD_MAX_CAP	 2500000

CONST_INT GANGOPS_THE_IAA_JOB_CASH_REWARD_MAX_CAP 			1625000
CONST_INT GANGOPS_THE_SUBMARINE_JOB_CASH_REWARD_MAX_CAP 	2375000
CONST_INT GANGOPS_THE_MISSILE_SILO_JOB_CASH_REWARD_MAX_CAP 	3000000

#IF IS_DEBUG_BUILD
#IF FEATURE_WANTED_SYSTEM
// JA: 18/8/15 - Wanted system prototype global due to stack sizes being close to limits throughout.
// All wrapped with DEBUG and FEATURE directives and should never get released
WANTED_SYSTEM_DATA g_wantedSystemData
#ENDIF
#ENDIF


INT g_iDisablePIMAccessoryChangeForReasons = 0

BOOL g_bRunInvisPlayerFailsafe = TRUE

BOOL g_bDisablePosixVehicleCheck = FALSE // Used to disable POSIX time check which determines if player gained early access to vehicle they shouldn't have (more in IS_VEHICLE_AVAILABLE_PAST_CURRENT_TIME from vehicle_public.sch)

BOOL g_give_player_a_rebreather = FALSE
INT g_salvage_rebreather_has_been_collected_number = -1
BOOL g_salvage_rebreather_has_been_collected = FALSE
BOOL g_salvage_refresh_rebreather_blips = false 
BOOL g_salvage_player_has_collected_rebreather = false 
BOOL  gRESET_PLAYER_BD_PICKUP_FLAGS = false
BOOL  gsalvagegiveplayerapoint = false

BOOL g_weapon_unload_crate_being_carried = FALSE

STRUCT VEHICLE_EXPORT_REWARDS_DATA
	// Steal data
	
	// Sell data
	IE_DROPOFF eDropoffID 
	MODEL_NAMES vehicleModel
	BOOL bOriginalSeller
	PLAYER_INDEX pOriginalOwner
	PLAYER_INDEX pDoingDelivery
	IE_VEHICLE_ENUM eIEVehID
	
	// Shared data
	INT iCurrentVehicleValue
	INT iExtraCash
	INT iExtraRP
	FLOAT fCashMultiplier = 1.0
	FLOAT fRPMultiplier = 1.0
ENDSTRUCT

STRUCT QUEUED_VEHICLE_EXPORT_REWARD
	INT iCashEarned = -1
	INT iRPEarned = -1
ENDSTRUCT

CONST_INT IE_DELIVERY_BITSET_ARRAY_SIZE		2

CONST_INT BS_IE_DELIVERY_GLOBAL_DATA_DONT_USE_PARALLEL_WARP		31

STRUCT IE_DELIVERY_EVENT_THRESHOLD_TRACKER
	INT iCounter
	SCRIPT_TIMER stTimer
ENDSTRUCT

STRUCT IE_DELIVERY_EVENT_CAPS
	IE_DELIVERY_EVENT_THRESHOLD_TRACKER eTracker1

	IE_DELIVERY_EVENT_THRESHOLD_TRACKER eTracker2

	IE_DELIVERY_EVENT_THRESHOLD_TRACKER eTracker3

	IE_DELIVERY_EVENT_THRESHOLD_TRACKER eTracker4

	SCRIPT_TIMER stDeliveryEventBlocker
ENDSTRUCT

STRUCT IE_DELIVERY_GLOBAL_DATA
	INT iBS
	INT iDropoffModelHideSetUpBS[IE_DELIVERY_BITSET_ARRAY_SIZE]
	
	IE_DROPOFF eClosestDropoffID = IE_DROPOFF_INVALID
	IE_DROPOFF eClosestActiveDropoffID = IE_DROPOFF_INVALID		
	VEHICLE_INDEX vehCurrentlyDelivering
	BOOL bDeliveryViaCargobob
	BOOL bDeliveryViaTowtruck
	
	//INT iEnableDropoffBS[IE_DELIVERY_BITSET_ARRAY_SIZE]
	//INT iEnableDropoffBlipBS[IE_DELIVERY_BITSET_ARRAY_SIZE]
	
	BOOL bHaltVehicleAndLaunchCutscene
	BOOL bLaunchingScript
	VECTOR vLaunchPoint
	IE_DROPOFF eDropoffIDForLaunch
	BOOL bStartCutscene
	BOOL bCutsceneTriggeredByProxy // When we're around the dropoff and someone who's also delivering to that dropoff starts the cutscene
	
	BOOL bTransactionPending
	
	BOOL bDontDrawCoronas
	
	// These are used to give the player a bonus for delivering a collection of vehicles
	IE_VEHICLE_SET_ID eVehSetGangIsSelling = IE_VEH_SET_INVALID
	IE_VEHICLE_ENUM eVehSetVhiclesDelivered[ciMaxIEVehiclesInCollection]	//Array of vehicles from a collection that have been deliverd
	INT iNumVehLocalPlayerDelivered = 0
	
	INT iRewardDataQueueLength
	QUEUED_VEHICLE_EXPORT_REWARD rewardDataQueue[4]
	
	// These are used to deduct cost of repair of vehicles that someone delivered to the warehouse
	INT iRepairCostForBoss
	PLAYER_INDEX pWhoDidDeliveryForRepair
	SCRIPT_TIMER timerRepairCostText
	SCRIPT_TIMER timerRepairTicker
	MODEL_NAMES eModelVehRepaired = DUMMY_MODEL_FOR_SCRIPT
	
	FLOAT fDistanceForCratesCreation = 420.0
	
	IE_DELIVERY_EVENT_CAPS eIEScrapCaps
	IE_DELIVERY_EVENT_CAPS eIEWarehouseCaps
	IE_DELIVERY_EVENT_CAPS eIEBuyerCaps
	IE_VEHICLE_ENUM eIEVehiclesDelivered[4]//MAX_NUM_VEHICLE_EXPORT_ENTITIES
	
	#IF IS_DEBUG_BUILD
	BOOL d_bAllowAnyVehicle // no decorator checks, any vehicle can be delivered to any dropoff
	BOOL d_bForceCleanup // delivery cutscene script will cleanup the vehicle, no matter what
	BOOL d_bDrawDebug
	BOOL d_bForceCargobobVariation
	BOOL d_bForceTowtruckVariation
	#ENDIF
ENDSTRUCT
IE_DELIVERY_GLOBAL_DATA g_IEDeliveryData

BOOL g_bRandomDisplaySlots = FALSE

STRUCT IE_TRAFFIC_MANAGEMENT_STRUCT
	INT iBS
	INT iTrafficZoneIndex
	INT iReducedDropoffZone
	
	INT iTrafficZoneIndexMOC = -1
	
	FLOAT fDistance = 250.0
	
	SCRIPT_TIMER blockedDropoffTimer
	
	TIME_DATATYPE tPrevTimeStamp[46] // Stores a time stamp for each drop off. 
	TIME_DATATYPE tCurrentTimeStamp
ENDSTRUCT

IE_TRAFFIC_MANAGEMENT_STRUCT g_IETrafficData

CONST_INT IE_BLOCKED_DROPOFF_TIME_TO_CHECK	10000
CONST_INT IE_BLOCKED_DROPOFF_TIME_LIMIT		30000

CONST_INT IE_BS_TRAFFIC_REDUCTION_ACTIVE		0
CONST_INT IE_BS_RESET_DROPOFF_AREA_TIMER		1
CONST_INT IE_BS_MOC_TRAFFIC_REDUCTION_ACTIVE	2

CONST_INT ciJBInteriorSwitchCleanup		0
CONST_INT ciJBInteriorGeneralCleanup	1
CONST_INT ciJBJustRunCleanup			2
CONST_INT ciJBInLocate					3
CONST_INT ciJBTriggeredContextInt		4
CONST_INT ciJBBlockPlayerUsingJB		5

STRUCT JUKEBOX_GLOBAL_DATA
	INT iBitSet
ENDSTRUCT
JUKEBOX_GLOBAL_DATA g_JukeboxData

CONTRABAND_TRANSACTION_STATE g_ProductionTimestamp_state
CONTRABAND_TRANSACTION_STATE g_FactorySetup_state
CONTRABAND_TRANSACTION_STATE g_StopProduction_state
CONTRABAND_TRANSACTION_STATE g_FailedDefend_state
CONTRABAND_TRANSACTION_STATE g_PaidResupply_Materials_state	//Transaction for adding the supplies
CONTRABAND_TRANSACTION_STATE g_RemoveResearch_state
CONTRABAND_TRANSACTION_STATE g_BusinessHubTransactionState

STRUCT DELAYED_ADD_BUNKER_RESEARCH_DATA
	CONTRABAND_TRANSACTION_STATE eTransactionState = CONTRABAND_TRANSACTION_STATE_DEFAULT
	INT iResearchtoAdd = 0
	INT iNewResearchtoTotal = 0
	BOOL bUnlockItem
ENDSTRUCT
DELAYED_ADD_BUNKER_RESEARCH_DATA g_sBunkerAddResearch

BUSINESS_TYPE g_BusinessHubTransactionSlot = BUSINESS_TYPE_INVALID
NIGHTCLUB_TRANSACTION_STAGE g_eNightclubTransactionStage
INT g_iCachedPlayerCashForNightclubSafe = 0
NIGHTCLUB_ID g_eCachedPlayerNightclubForNightclubSafe = NIGHTCLUB_ID_INVALID
BOOL g_bDISABLE_ResetCashNightclubSafe = FALSE

// Arcade management globals
INT g_iCachedPlayerCashForArcadeSafe = 0
BOOL g_bArcadeTransactionPending = FALSE
ARCADE_PROPERTY_ID g_eCachedPlayerArcadeForArcadeSafe = ARCADE_PROPERTY_ID_INVALID
INT g_iArcadeSafeHelpTextCount = 0
INT g_iArcadeSafeFullHelpTextCount = 0
BOOL g_bDISABLE_ResetCashArcadeSafe = FALSE

// Fixer agency cash globals
INT g_iCachedPlayerCashForFixerHQSafe = 0
BOOL g_bFixerHQTransactionPending = FALSE
FIXER_HQ_ID g_eCachedPlayerFixerHQForHQSafe = FIXER_HQ_ID_INVALID
BOOL g_bDisable_ResetCashFixerHQSafe = FALSE
INT g_iFixerHQSafeHelpTextCount = 0
INT g_iFixerHQSafeFullHelpTextCount = 0

BOOL g_bSetCurrentLanguageComplexForDancingHelp = FALSE

BOOL g_bBusinessHubFullOfAGoodsType = FALSE

INT g_ProductionTimestamp_slot		= -1
INT g_iStopProduction_Slot			= -1
INT g_iFailedDefendTransactionSlot 	= -1
INT g_iFactorySetup_slot			= -1
INT g_iPaidResupply_Slot			= 0

INT g_iPaidResupplyMaterialsToAdd[ciMAX_OWNED_FACTORIES]	//Used for telemetry data

BOOL g_bHideMechanicPedForCLBHScene = FALSE
BOOL g_bDoingClbhCutscene			= FALSE

SCRIPT_TIMER g_stProductHalfwayTimer[ciMAX_OWNED_FACTORIES]
SCRIPT_TIMER g_stProductionTimer[ciMAX_OWNED_FACTORIES]
INT g_iRemainingTimeForProductReinit[ciMAX_OWNED_FACTORIES]
INT g_iRemTimeForFacResearchReinit
SCRIPT_TIMER g_stResearchTimer
INT g_iInteriorApartmentActivity = -1

BOOL g_bKillTheBed = FALSE

BOOL g_bPlayerViewingTurretHud = FALSE
BOOL g_bTurretFadeOut = FALSE

INT g_iKillAllActivityCreatorScripts[52]

BOOL g_bBypass_MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE = FALSE
BOOL g_bBypass_MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE = FALSE
BOOL g_bBypass_MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE = FALSE
BOOL g_bBypass_MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYERS = FALSE
BOOL g_bBypass_MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE = FALSE
#IF FEATURE_DLC_2_2022
BOOL g_bBypass_MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE = FALSE
#ENDIF
#IF FEATURE_DLC_2_2022
BOOL g_bBypass_MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE = FALSE
#ENDIF

INT g_iIETransactionAddVehicleID 	= -1	//The vehicle to run a transaction to add
INT g_iIETransactionRemoveVehicleID = -1	//The vehicle to run a transaction to add
INT g_iIETransactionAlterVehicleSlot 	= -1	//The slot which the above vehicle should be added to
CONTRABAND_TRANSACTION_STATE g_AddIEVehTransactionState
SCRIPT_TIMER sIEMissionFadeSafeTimer
#IF IS_DEBUG_BUILD
INT g_iIEDebugTransactionVehicleID[ciMAX_IE_OWNED_VEHICLES]
INT g_iIEDebugTransactionSlotID[ciMAX_IE_OWNED_VEHICLES]
CONTRABAND_TRANSACTION_STATE g_DebugIEVehTransactionState

BOOL g_bForceStartCarModScriptForOffice = FALSE
BOOL g_bDebugLaunchappImportExportScript = FALSE

#ENDIF


INT g_iFMMCCurrentStaticCam = -1
CAMERA_INDEX g_birdsEyeCam
BOOL g_LastPlayerAliveTinyRacers = FALSE

BOOL g_bMCForceThroughRestartState = FALSE

//Incredbile flow play hack for bug 3574647 - please remove this once code fix the issue!
INT g_bsOppressorFreezeHack


	//prevent modshop from cleaning up the cutscene, CLEANUP_PLAYER_FOR_MP_SHOP_CUTSCENE overrides cleanup param
	BOOL g_bPreventShopCutsceneCleanup = FALSE

MODEL_NAMES g_mnOfLastVehicleWeWasIn

BOOL g_bReBlockSeatSwapping = FALSE

INT g_iMissionObjectHasSpawnedBitset = 0
BOOL g_bMissionPlacedOrbitalCannon = FALSE
INT g_iOrbitalCannonObjectInUse = -1

INT g_iStrikeTeamBlockSelectingPlayerBS = 0

MODEL_NAMES dummyWeaponsedVehicleCapModel = DUMMY_MODEL_FOR_SCRIPT

SCRIPT_TIMER g_stHubProductionTimer[ciMAX_BUSINESS_STAFF_MEMBERS]
SCRIPT_TIMER g_stHubDefendLanuchTimer
SCRIPT_TIMER g_stPhoneMissionLanuchTimer
SCRIPT_TIMER g_stHubStatSaveTimer
SCRIPT_TIMER g_stHubBuyerSelectionTimer
INT g_iHubProductionTimeRemaining[BUSINESS_TYPE_COUNT]
INT g_iHubDefendLaunchTimeRemaining = -1
INT g_iHubPhoneMissionLaunchTimeRemaining = -1

STRUCT BUSINESS_HUB_PRODUCTION_PAUSE_DATA
	BOOL bSavedTime = TRUE
	INT iProductionSaveTimeMS[BUSINESS_TYPE_COUNT]
ENDSTRUCT

BUSINESS_HUB_PRODUCTION_PAUSE_DATA g_sBusinessHubPauseData

#IF IS_DEBUG_BUILD
INT g_iDebugNightclubSafeCashValue = -1
#ENDIF

SCRIPT_TIMER g_timerGhostOrgDelay
SCRIPT_TIMER g_timerBribeAuthDelay

BOOL g_bAllowKickFromMOCVehEntryAsPassenger = TRUE

BOOL g_bHasLaunchedMissionArenaBox = FALSE
BOOL g_bMissionPlacedArenaBox = FALSE
BOOL g_bPlayerIsInArenaBox = FALSE
BOOL g_bSpawnPlayerInArenaBoxSeat = FALSE
BOOL g_bArenaTabletSeatsFull = FALSE
BOOL g_bPlayerFullySeated = FALSE
BOOL g_bArenaBoxActivitiesLoaded = FALSE

CONST_INT GLOBAL_ARENA_SPECTATOR_BOX_BS_LAUNCH_DRONE		0
CONST_INT GLOBAL_ARENA_SPECTATOR_BOX_BS_LAUNCH_LIVE_STREAM	1
CONST_INT GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_DRONE			2
CONST_INT GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_LIVE_STREAM	3
CONST_INT GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_DRONE			4
CONST_INT GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_LIVE_STREAM	5
CONST_INT GLOBAL_ARENA_SPECTATOR_BOX_BS_STANDING_UP			6
CONST_INT GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY		7
CONST_INT GLOBAL_ARENA_SPECTATOR_BOX_BS_LOADED_CONTROLS		8

INT g_iArenaSpectatorBS = 0

#IF IS_DEBUG_BUILD
BOOL g_bDebugPlacedArenaBox = FALSE
BOOL g_bWarpPlayerToArenaBox = FALSE
#ENDIF

BOOL g_bStayInArenaBox = FALSE		// Used for joining in specifically to arena box, to prevent auto joining next round
BOOL g_bEnableRPBarInArenaBox = FALSE
BOOL g_bRefundWheelSpinMinigame = FALSE
BOOL g_bTriggerOnScreenWheelSpinMinigame = FALSE
BOOL g_bTriggerOnScreenWheelSpinMinigameHelpText = TRUE
BOOL g_bArenaBigScreenRendering = FALSE
BOOL g_bArenaOfficeTVRendering = FALSE

INT g_iRefundWheelSpinMinigameMultipler = 0

#IF FEATURE_FIXER
// globals used to control Franklin's cars spawning outside his house.
BOOL g_bFranklinCarsActive = TRUE
INT g_iFranklinCarsServerChangeSetupInterval = 600 // How often the server changes the layout of the vehicles. (in seconds) 10 mins 
INT g_iFranklinCarsServerUpdateInterval = 30000 // How often the server runs its checks. (in milliseconds) 30 seconds
FLOAT g_fFranklinCarsActivationRadius = 150.0 // How close the local player needs be to create the vehicles. (in metres)
FLOAT g_fFranklinCarsActivationRadiusSquared = (g_fFranklinCarsActivationRadius * g_fFranklinCarsActivationRadius) // same as above but squared for less expensive calculations.
FLOAT g_fFranklinCarsDeActivationDistSquared = 40000.0 // Distance the local player has to travel from location for the vehicles to get deleted. (200m squared)
FLOAT g_fFranklinCarsIsClearRadiusCheck = 5.0 // radius around centre point to check it's ok to spawn vehicles.
BOOL g_IsSafeToRunFranklinCars_Override = FALSE // This is for potential use in a bgscript.
BOOL g_IsSafeToRunFranklinCars_Override_Value = FALSE
#ENDIF

BOOL g_bReducedClearAreaForInteriorsActive = TRUE


#IF IS_DEBUG_BUILD
INT g_iArenaWarpBS = 0
INT g_iArenaWarpState = 0
INTERIOR_INSTANCE_INDEX g_iArenaInteriorIndex
PLAYER_INDEX g_pArenaBoxHost
BOOL g_bSetWheelReward = FALSE
INT g_iWheelRewardID = -1
INT g_iOnScreenWheelRewardID = -1
BOOL g_bDebugActivateOnScreenWheelSpinMinigame = FALSE

STRUCT STRUCT_LOAD_DEBUG_ARENA
	BOOL bRunDebugArena
	BOOL bCleanupDebugArena
	INT iState
	INTERIOR_INSTANCE_INDEX iInteriorID
	INT iVariation
	INT iCurrentVariation
	INT iLighting
	INT iCurrentLighting
ENDSTRUCT
STRUCT_LOAD_DEBUG_ARENA g_db_DebugLoadArena
#ENDIF

// Turret_manager data:
// * None of these variables should be set directly : see turret_manager_core.sch for details.
TURRET_MANAGER_LOCK_STATE g_eTurretManagerState	
TURRET_LOCK_SEARCH_TYPE g_eTurretLockSearchType		// Turret search type for current turret lock request.
TURRET_LOCK_REQUEST_ID g_iTurretScriptLaunchUid 	// Unique Id last turret script was launched with through turret launcher system.
INT g_iTurretLockBs									// Additional info sent to the server about the request.
BOOL g_bNewTurretLockRequest						// TRUE when a new lock request has been created.
TURRET_OCCUPANCY_DATA g_turretLockRequest			// Data from last turret lock request to server.
TURRET_OCCUPANCY_DATA g_turretLockResponse 			// Data from last turret lock response from server.
TURRET_LOCK_REQUEST_ID g_iTurretLockRequestUid		// Unique Id of last turret lock request.
TURRET_LOCK_REQUEST_ID g_iTurretLockResponseUid 	// Unique Id of last turret lock response.

// Turret_cam data: 
// * None of these variables should be set directly : see turret_cam_script.sch for details.
BOOL g_bKillTurretScript 							// Set this (TRUE) to force turret_cam_script script to shutdown.
TURRET_CAM_CONFIG_ID g_eTurretCamConfigId			// Used to verify that the most recent config is the one we're using when launching.
TURRET_CAM_CONFIG g_turretCamConfig					// Turret cam config
INT g_iBsTurretCamConfigSettings					// Used to identify which parts of the config are set (unset = use default values). ci_TURRET_CAM_CONFIG_BS_.
INT g_iBsTurretCamConfigUpdate						// Used to identify which parts of the config need updating. ci_TURRET_CAM_CONFIG_BS_.
INT g_iBsTurretCam									// Used for various global communication. ci_TURRET_CAM_BS_.

// Arena specator turrets
// * None of these variables should be set directly : see arena_specator_turret_public.sch for details.
ARENA_CONTESTANT_TURRET_STACK g_arenaContestantTurretStack // Stack of arena drive-in contestant turrets.

SCRIPT_TIMER g_sPlayerActionCooldown
WHEEL_SPIN_MINIGAME_DATA sOnScreenWheelSpinMinigameData  // Moved here due to freemode stack overflow B*5453955

TURRET_CAM_DAMAGE_ACCUMULATOR g_turretDamageTaken 	// Keep track of damage taken while in a turret

STRUCT ARENA_SPECTATOR_TURRET_LOCAL_DATA
	// Track missile cooldown locally so we can verify server has been updated
	TIME_DATATYPE tMissileCdComplete 	// Cooldown complete time
	INT iMissileCdTurretId = -1			// Turret id for cooldown time		
	SCRIPT_TIMER resendTimer			// Cooldown complete time
ENDSTRUCT

ARENA_SPECTATOR_TURRET_LOCAL_DATA g_arenaSpectatorTurret 

// The current Arena Wars TV channel2 playlist job (Office TV)
INT g_iArenaWarTvPlaylistJobIndex = -1
ENUM ARENA_WARS_TV_STATE
	AWTVS_NONE,
	AWTVS_INIT,
	AWTVS_CREATE,
	AWTVS_INTRO,
	AWTVS_LOOP_FADE_IN,
	AWTVS_LOOP
ENDENUM
// Globals for the Arena Wars TV (lobby)
ARENA_WARS_TV_STATE g_eAwTvLobbyState	
INT g_iAwTvLobbyJobIndex = -1			
INT g_iAwTvLobbyLastDrawFrame = -1
INT g_iAwTvLobbyMapHash = 0
INT g_iAwTvLobbyActionHash = 0
INT g_iAwTvLobbyLoopCurrentHash = 0
FLOAT g_fAwTvLobbyLoopFadeDuration = 250.0
FLOAT g_fAwTvLobbyLoopCurrentFadeDuration = 0.0
TIME_DATATYPE g_tAwTvLobbyFadeInStart

BOOL g_bPrintedArenaTurretTutHelp = FALSE // Have printed the 3x tutorial help for the Arena Contestant Turrets?
BOOL g_bBlockBuildingControllerArenaPackIPLs = FALSE
 //FEATURE_ARENA_WARS
 
BOOL h_bis_action_figure_collectables_on
INT i_action_figure_collectables_currently_close_to = -1
INT i_action_figure_collectables_timer = 0


BOOL h_bis_playing_card_collectables_on
INT i_playing_card_collectables_currently_close_to = -1
INT i_playing_card_collectables_timer = 0

BOOL h_bis_peyote_collectables_on
INT i_peyote_collectables_currently_close_to = -1
INT i_peyote_collectables_stagger_creation_timer = 0

BOOL h_bis_signal_jammer_collectables_on
INT i_signal_jammer_collectables_currently_close_to = -1
INT i_signal_jammer_collectables_timer = 0

BOOL g_Bis_player_a_peyote_animal = FALSE

STRUCT AnimalAnimationDataPeyote
    TEXT_LABEL_63 strDict
    TEXT_LABEL_63 strAnim
    TEXT_LABEL_63 strCamAnim
    TEXT_LABEL_63 strIdleDict
    TEXT_LABEL_63 strIdleAnim
    TEXT_LABEL_63 strBarkDict
    TEXT_LABEL_15 strBarkAnim
ENDSTRUCT


ENUM AnimalPickupTypePeyote
	APT_INVALID_PEYOTE = -1,
	APT_LOWLAND_PEYOTE = 0,
	APT_HIGHLAND_PEYOTE,
	APT_UNDERWATER_PEYOTE,
	APT_SECRET_PEYOTE,
	
	APT_COUNT_PEYOTE
ENDENUM

AnimalPickupTypePeyote eTriggeredTypePeyote



BOOL b_is_player_an_animal = FALSE

ENUM CollectableType
	COLLECTABLE_NONE = -1,
	COLLECTABLE_MOVIE_PROPS 
	#IF FEATURE_HEIST_ISLAND
	
	,	COLLECTABLE_HIDDEN_CACHES,
		COLLECTABLE_TREASURE_CHESTS,
		COLLECTABLE_RADIO_STATIONS
	#ENDIF
	
	,COLLECTABLE_USB_PIRATE_RADIO,
	COLLECTABLE_SHIPWRECKED
	#IF FEATURE_FIXER
	,
	COLLECTABLE_BURIED_STASH,
	COLLECTABLE_METAL_DETECTOR	
	#ENDIF 
	
	#IF FEATURE_DLC_1_2022
	,
	COLLECTABLE_TRICK_OR_TREAT,
	COLLECTABLE_LD_ORGANICS,
	COLLECTABLE_SKYDIVES,
	COLLECTABLE_SIGHTSEEING
	#ENDIF 
	
	#IF FEATURE_COPS_N_CROOKS
	 ,	COLLECTABLE_POLICE_BADGES,
		COLLECTABLE_GUN_PARTS,
		COLLECTABLE_TAGGING
	#ENDIF
	#IF FEATURE_DLC_2_2022
		, COLLECTABLE_SNOWMEN,
		COLLECTABLE_DEAD_DROP
	#ENDIF
ENDENUM


STRUCT COLLECTABLES_MISSION_DATA			
// STRUCT COLLECTABLES_DATA
	PICKUP_INDEX g_Pickup_Action_figure_Collectable[100]
  	PICKUP_INDEX g_Pickup_PLaying_card_Collectable[54]
	OBJECT_INDEX g_Pickup_Signal_Jammer_Collectable[50]
	OBJECT_INDEX g_Object_Peyote_Collectable
	
	INT initialise_collectables = 0
	INT inumberofactionfigurescollected = 0
	INT inumberofplayingcardscollected = 0
	INT inumberofsignaljammerscollected = 0
	//BOOL bprintfirstcardhelp1 = FALSE
	INT btimer1 = -1
	INT icollectablesbitset
	INT icollectablesbitset2
	INT btimer2 = -1
	INT btimer3 = -1
	VECTOR vrumble_player_position

	
	BLIP_INDEX blip_comic_shop
	SCRIPT_TIMER rumbletimer
	INT rumbletimer2	= -1
	BOOL bturnintoanimal
	INT  ianimalSelectorDEBUG = -1
	INT  ianimalSelector = -1
	BOOL bturnbacktohuman
	
	TEXT_LABEL_63 strBank
	TEXT_LABEL_31 strSound
	TEXT_LABEL_15 strBarkBank
	TEXT_LABEL_63 strBarkSound
	INT iPickupSoundID = -1
	INT iBarkSoundID = -1
	INT barkTimer = -1
	INT i_peyote_collectables_timerb = 0
	INT i_peyote_collectables_timeout = 0
	BOOL bplayerhaspressedContext = FALSE
	PED_INDEX temp_clone_ped
	INT iactionfigurendcutscenestage = 0
	
	INT cutscenetimer1


	INT timer_peyote[76]
	
	INT myTimer
	INT cameraTimer
	INT iLoadSceneTimer = -1
	INT iFadeInTimer = -1
	BOOL bWearingMask
	
	OBJECT_INDEX oChunky
	INT iTriggeredIndex
	BOOL bWearingScubaGear
	CAMERA_INDEX camSyncScene
	//INT iCullSpherePeyoteCollect = -1
	INT iCullSphereAnimalSpawn = -1
	INT iTriggeredWantedLevel
	//INT iBarkSoundID = -1

	FLOAT fDrunkCameraShake

	BOOL drawRect = FALSE
	BOOL whiteBoxfadeIn = TRUE
	INT drawTimer = -1
	INT whiteAlpha = 0
	INT iSyncSceneID = 0
	INT iStateBitset
	CAM_VIEW_MODE eTriggeredCameraView
	MODEL_NAMES playermodel
	BOOL btriggersmokeeffect = FALSE
	VECTOR vtriggersmokeeffecttriggerpoint
	BOOL bhasscreenfadedforcheckingtoswitchbacktohuman = FALSE
	BOOL bhasspawnedinhospital = FALSE
	INT isyncscene
	INT icasinochipamount = -1
	BOOL bgivecasinochipscollectables = FALSE
	AnimalAnimationDataPeyote sAnimAnimal
	INT icardboothelptimer
	VECTOR vsafeplaceawayfromcomicstore
	
	
	PICKUP_INDEX g_Object_Gun_Part_Collectable
	INT inumberofGunPartscollected = 0
	
	PICKUP_INDEX g_Object_Movie_prop_Collectable
	INT inumberofMovie_propscollected = 0
	INT inumberofMovie_propsdelivered = 0
	
	PICKUP_INDEX g_Object_Police_badge_Collectable
	INT inumberofpolicebadgescollected = 0
	
//	OBJECT_INDEX g_Object_Tagging_Collectable
//	INT inumberofTaggingcollected = 0
	
	INT ihelp_text_timer
	BOOL bCanDeliverMovieProp
	
	BLIP_INDEX blip_movie_prop_drop_off
	
	OBJECT_INDEX g_Object_USB_PIRATE_RADIO_Collectable
	INT inumber_of_USB_PIRATE_RADIO_collected = 0
	
	PICKUP_INDEX g_Object_Underwater_Packages_Collectable
	INT inumber_of_Underwater_Packages_collected = 0
	
	OBJECT_INDEX g_Object_Treasure_Chests_Collectable
	INT inumber_of_Treasure_Chests_collected = 0
	
	OBJECT_INDEX g_Object_Treasure_Chests_gold_coin
	OBJECT_INDEX g_Object_Treasure_Chests_gold_pile
	OBJECT_INDEX g_Object_Treasure_Chests_Collectable_opened
	
	
	OBJECT_INDEX g_Object_Radio_Stations_Collectable
	INT inumber_of_Radio_Stations_collected = 0
	INT iPickupSoundID2 = -1
	BLIP_INDEX blip_sonar[10]
	BOOL bsonar_alpha_on[10]
	INT iSonarBlipCount = 0
	INT isonartimer
	
	INT ipirateradiotextmessagetimer = 0
	GRASS_CULL_SPHERE_HANDLE grasscull
	BLIP_INDEX blip_radio_station_mirrorpark
//	INT iposterstage = 0
//	INT ipostertimer = 0
//	OBJECT_INDEX posterspraycan
//	BOOL g_bPlayTaggingAnimation
//	BOOL g_bPlayCollectingCanAnimation

	INT iRadioDelay
	FLOAT fRadioSignal
	SCRIPT_TIMER stRadioInterfTimer 
	SCRIPT_TIMER stRadioTuneTimer
	
	OBJECT_INDEX g_Object_Shipwrecked_Collectable
	INT inumber_of_Shipwrecked_collected = 0
	OBJECT_INDEX g_Object_shipwrecked_second_prop
	
	OBJECT_INDEX g_Object_ShipWrecked_gold_coin
	OBJECT_INDEX g_Object_ShipWrecked_gold_pile
	OBJECT_INDEX g_Object_ShipWrecked_Collectable_opened
	
	
	OBJECT_INDEX g_Object_Buried_Stash_Collectable
	INT inumber_of_Buried_Stash_collected = 0
	BLIP_INDEX blip_buried_stash_search_blip[2]
	BLIP_INDEX blip_buried_stash_search_blip2[2]
	BLIP_INDEX blip_buried_stash_actual_blip[2]
	INT iburiedstassh_sound= -1
	
	CollectableType Cutscene_CollectableType

	SCALEFORM_INDEX si_DetectorScaleform
	INT metaldetectortimer
	SCRIPT_TIMER stHalloween22_TOT_DelayTimer
	SCRIPT_TIMER stHalloween22_TOT_DrunkTimer
	SCRIPT_TIMER stHalloween22_TOT_AnimalTimer
	SCRIPT_TIMER stHalloween22_TOT_ExplosionTimer
	BOOL bHalloween22_TOT_Animal
	INT iHalloween22_TOT_last_trick_variation = -1
	INT iHalloween22_TOT_last_treat_variation = -1
	INT iHalloween22_TOT_pending_sfx_type = -1
	INT iHalloween22_TOT_pending_trick = -1
	BOOL iHalloween22_TOT_pending_drunk_walk = FALSE
	SCRIPT_TIMER stHalloween22_TOT_pending_trick_timer
	CAMERA_INDEX stHalloween22_TOT_drunk_camera_index
	BOOL iHalloween22_TOT_drunk_gameplay_camera_on
	
	INT icachetrick_or_treat_location_for_animal_transform = -1	
	INT rt_ID
	INT ihelp_text_timer_veh_unlock
	BOOL bNetworkactivty_initalise_check = FALSE
	
	BLIP_INDEX blip_ld_organics
	OBJECT_INDEX g_Object_Trick_or_Treat_Collectable
	INT inumber_of_Trick_or_Treat_collected = 0
	
	OBJECT_INDEX g_Object_LD_Organics_Collectable
	INT inumber_of_LD_Organics_collected = 0
	INT stLDOrganicsStonedTimer
	
	OBJECT_INDEX g_Object_Skydives_Collectable
	INT inumber_of_Skydives_collected = 0
	OBJECT_INDEX g_Object_joint
	
	BLIP_INDEX blip_snowmen
	OBJECT_INDEX g_Object_snowmen_Collectable
	INT inumber_of_snowmen_collected = 0
	
	//OBJECT_INDEX g_Object_dead_drop_Collectable
	PICKUP_INDEX g_Object_dead_drop_Collectable
	INT inumber_of_dead_drop_collected = 0

ENDSTRUCT



ENUM MOVIE_PROP_COLLECTABLES
	MOVIEPROP_WIFA_AWARD = 0,
	MOVIEPROP_MELTDOWN_FILM_REEL,
	MOVIEPROP_ALIEN_HEAD,
	MOVIEPROP_MELTDOWN_CLAPPERBOARD,
	MOVIEPROP_NATIVE_AMERICAN_HEADDRESS,
	MOVIEPROP_MUMMY_HEAD,
	MOVIEPROP_MONSTER_MASK,
	MOVIEPROP_TIGER_RUG,
	MOVIEPROP_SARCOPHAGUS,
	MOVIEPROP_DRINKS_GLOBE,
	
	MOVIEPROP_MAX
ENDENUM
	
#IF FEATURE_TUNER
ENUM USB_MUSIC_MIX_COLLECTABLES
	USB_MUSIC_MIX_NONE = -1,
	USB_MUSIC_MIX_0,			//TUNER_AP_MIX3_PARTA = "Blue EP"
	USB_MUSIC_MIX_1,			//TUNER_AP_MIX3_PARTB = "Green EP
	USB_MUSIC_MIX_2,			//TUNER_AP_MIX3_PARTC = "Violet EP"
	USB_MUSIC_MIX_3,			//TUNER_AP_MIX3_PARTD = "Black EP"
	USB_MUSIC_MIX_4_MOODY_MAN, // moody		//TUNER_AP_MIX1 = "MM Mix"
	USB_MUSIC_MIX_0_1_2_3,		// TUNER_AP_MIX3 = "Seth"
	USB_MUSIC_MIX_OFF, 			//Turning fav track in PI menu to OFF
	USB_MUSIC_MIX_7_FIXER_DRE,
	USB_MUSIC_MIX_7_FIXER_DRE_SPECIAL_FINALE_TRACK,
	USB_MUSIC_MIX_8_FIXER_LOCATION_MIX,
	USB_MUSIC_MIX_9_FIXER_LOCATION_MIX_2,
	USB_MUSIC_MIX_MAX
ENDENUM

	ENUM USB_COLLECTABLES_LOCATIONS
		USB_COLLECTABLE_LOCATIONS_FIXER_FINALE_DRE = 0,
		USB_COLLECTABLE_LOCATIONS_1,
		USB_COLLECTABLE_LOCATIONS_2,
		USB_COLLECTABLE_LOCATIONS_3,
		USB_COLLECTABLE_LOCATIONS_4,
		USB_COLLECTABLE_LOCATIONS_5,
		USB_COLLECTABLE_LOCATIONS_6,
		USB_COLLECTABLE_LOCATIONS_7,
		USB_COLLECTABLE_LOCATIONS_8,
		USB_COLLECTABLE_LOCATIONS_9,
		USB_COLLECTABLE_LOCATIONS_10,
		USB_COLLECTABLE_LOCATIONS_11,
		USB_COLLECTABLE_LOCATIONS_12,
		USB_COLLECTABLE_LOCATIONS_13,
		USB_COLLECTABLE_LOCATIONS_14,
		USB_COLLECTABLE_LOCATIONS_15,
		USB_COLLECTABLE_LOCATIONS_16,
		USB_COLLECTABLE_LOCATIONS_17,
		USB_COLLECTABLE_LOCATIONS_MOODY_MAN,
		USB_COLLECTABLE_LOCATIONS_ISLAND_COMPOUND_FINALE,
		USB_COLLECTABLE_LOCATION_MAX
	ENDENUM
	

#ENDIF


COLLECTABLES_MISSION_DATA collectables_missiondata_main

CONST_INT iMax_Number_Of_Signal_jammers 50

#IF FEATURE_SUMMER_2020
BOOL bis_movie_props_collectables_on
INT i_movie_props_collectables_currently_close_to = -1
INT i_movie_props_collectables_timer = 0
CONST_INT iMax_Number_Of_movie_props  10

#IF IS_DEBUG_BUILD
	BOOL g_bSetAllMoviePropsCollected
	BOOL g_bToggleMoviePropsCollectables
	BOOL g_bBlipAllMoviePropsCollectables
	BOOL g_bclearcollectablesMovieProps
	BOOL g_bwarp_to_next_MovieProps_collectable
	INT g_iselectedMoviePropscollectable
	BOOL g_bSet29MoviePropsCollected
	BLIP_INDEX g_debug_blip_MovieProps[iMax_Number_Of_movie_props]
#ENDIF
#ENDIF

#IF FEATURE_HEIST_ISLAND
	BOOL bis_Underwater_Packages_collectables_on
	INT i_Underwater_Packages_collectables_currently_close_to = -1
	INT i_Underwater_Packages_collectables_timer = 0
	CONST_INT iMax_Number_Of_Hidden_Caches   100
	//CONST_INT iMax_Number_Of_Active_Hidden_Caches   10

	#IF IS_DEBUG_BUILD
		BOOL g_bSet_All_Underwater_Packages_Collected
		BOOL g_bToggle_Underwater_Packages_Collectables
		BOOL g_bBlip_All_Underwater_Packages_Collectables
		BOOL g_bclearcollectables_Underwater_Packages
		BOOL g_bwarp_to_next_Underwater_Packages_collectable
		INT g_iselected_Underwater_Packages_collectable
		BOOL g_bSet_9_Underwater_Packages_Collected
		BLIP_INDEX g_debug_blip_Underwater_Packages[10]
		INT g_iactive_underwater_packages[10]
		INT g_daily_collectable_day
	#ENDIF
	
	BOOL bis_Treasure_Chests_collectables_on

	INT i_Treasure_Chests_collectables_currently_close_to = -1
	INT i_Treasure_Chests_collectables_timer = 0
	CONST_INT iMax_Number_Of_Treasure_Chests 20  // replaced with g_sMPTunables.iNumber_of_USB_Pirate_radio
	CONST_INT iMax_Number_Of_Active_Treasure_Chests  2
	#IF IS_DEBUG_BUILD
		BOOL g_bSet_All_Treasure_Chests_Collected
		BOOL g_bToggle_Treasure_Chests_Collectables
		BOOL g_bBlip_All_Treasure_Chests_Collectables
		BOOL g_bclearcollectables_Treasure_Chests
		BOOL g_bwarp_to_next_Treasure_Chests_collectable
		INT g_iselected_Treasure_Chests_collectable
		INT g_iactive_treasure_chests[2]
		BOOL gb_Update_daily_collectables
		BOOL gb_Reset_daily_collectables
		BOOL g_bSet_9_Treasure_Chests_Collected
		BLIP_INDEX g_debug_blip_Treasure_Chests[iMax_Number_Of_Treasure_Chests]
	#ENDIF
	
	BOOL bis_Radio_Stations_collectables_on
	INT i_Radio_Stations_collectables_currently_close_to = -1
	INT i_Radio_Stations_collectables_timer = 0
	CONST_INT iMax_Number_Of_Radio_Stations 10  // replaced with g_sMPTunables.iNumber_of_USB_Pirate_radio

	#IF IS_DEBUG_BUILD
		BOOL g_bSet_All_Radio_Stations_Collected
		BOOL g_bToggle_Radio_Stations_Collectables
		BOOL g_bBlip_All_Radio_Stations_Collectables
		BOOL g_bclearcollectables_Radio_Stations
		BOOL g_bwarp_to_next_Radio_Stations_collectable
		INT g_iselected_Radio_Stations_collectable
		BOOL g_bSet_9_Radio_Stations_Collected
		BLIP_INDEX g_debug_blip_Radio_Stations[iMax_Number_Of_Radio_Stations]
	#ENDIF

#ENDIF


#IF FEATURE_TUNER
	#IF IS_DEBUG_BUILD
		INT g_iMP_AWARD_SPRINTRACER  		      
		INT g_iMP_AWARD_STREETRACER			  
		INT g_iMP_AWARD_PURSUITRACER			  
		INT g_iMP_AWARD_AUTO_SHOP			
		INT g_iMP_AWARD_GROUNDWORK			
		INT g_iMP_AWARD_ROBBERY_CONTRACT  	
		INT g_iMP_AWARD_FACES_OF_DEATH  
		INT g_iMP_AWARD_TEST_CAR      		
		INT g_iMP_AWARD_CAR_EXPORT
		BOOL g_bUpdateTunerAwardsDebug
		BOOL g_brestartTunerBoardStats
	#ENDIF 	
		
		
	BOOL bis_USB_Pirate_Radio_collectables_on
	INT i_USB_Pirate_Radio_collectables_currently_close_to = -1
	INT i_USB_Pirate_Radio_collectables_timer = 0
	
	//CONST_INT iMax_Number_Of_USB_Pirate_Radio   10  // g_sMPTunables.iNumber_of_USB_Pirate_radio

//	ENUM USB_COLLECTABLES
//		USB_COLLECTABLE_0 = 0,
//		USB_COLLECTABLE_1,
//		USB_COLLECTABLE_2,
//		USB_COLLECTABLE_3,
//		USB_COLLECTABLE_4,
//		USB_COLLECTABLE_5,
//		USB_COLLECTABLE_6,
//		USB_COLLECTABLE_7,
//		ISLAND_COMPOUND_FINALE_USB_COLLECTABLE,
//		
//		USB_COLLECTABLE_MAX
//	ENDENUM
	

	#IF IS_DEBUG_BUILD
		BOOL g_bSet_All_USB_Pirate_Radio_Collected
		BOOL g_bToggle_USB_Pirate_Radio_Collectables
		BOOL g_bBlip_All_USB_Pirate_Radio_Collectables
		BOOL g_bclearcollectables_USB_Pirate_Radio
		BOOL g_bwarp_to_next_USB_Pirate_Radio_collectable
		INT g_iselected_USB_Pirate_Radio_collectable
		BOOL g_bSet_9_USB_Pirate_Radio_Collected
		BLIP_INDEX g_debug_blip_USB_Pirate_Radio[25]
		BOOL g_iusb_collectable_0_location[18]
		BOOL g_iusb_collectable_1_location[18]
		BOOL g_iusb_collectable_2_location[18]
		BOOL g_iusb_collectable_3_location[18]
		BOOL g_iusb_collectable_4_location[18]
		BOOL g_iusb_collectable_FIXER_LOCATION_MIX_location[18]
		BOOL g_iusb_collectable_FIXER_LOCATION_MIX2_location[18]
		BOOL g_iusb_collectable_has_initialised = FALSE
		
		BOOL gUSB_FINALE_Location_bitset[6]	
		//BOOL gUSB_SENTINAL_Location_bitset[6]
		BOOL gUSB_MOODY_Location_bitset	[6]
		BOOL gUSBdretrack 
		BOOL gUSBdreignorechecks = FALSE
	#ENDIF
	
	
	
	// Shipped Wrecked
	BOOL bis_Shipwrecked_collectables_on

	INT i_Shipwrecked_collectables_currently_close_to = -1
	INT i_Shipwrecked_collectables_timer = 0
	CONST_INT iMax_Number_Of_Shipwrecked_LOCATIONS 30  // replaced with g_sMPTunables.iNumber_of_USB_Pirate_radio
	CONST_INT iMax_Number_Of_Active_Shipwrecked  0
	CONST_INT iMax_Number_Of_Shipwrecked 17
	#IF IS_DEBUG_BUILD
		BOOL g_bSet_All_Shipwrecked_Collected
		BOOL g_bToggle_Shipwrecked_Collectables
		BOOL g_bBlip_All_Shipwrecked_Collectables
		BOOL g_bclearcollectables_Shipwrecked
		BOOL g_bwarp_to_next_Shipwrecked_collectable
		INT g_iselected_Shipwrecked_collectable
		INT g_iactive_Shipwrecked

		BOOL g_bSet_9_Shipwrecked_Collected
		BLIP_INDEX g_debug_blip_Shipwrecked[iMax_Number_Of_Shipwrecked]
	#ENDIF
	
	#IF FEATURE_FIXER
	// Buried Stash
	BOOL bis_Buried_stash_collectables_on

	INT i_Buried_stash_collectables_currently_close_to = -1
	INT i_Buried_stash_collectables_timer = 0
	CONST_INT iMax_Number_Of_Buried_stash 10  // replaced with g_sMPTunables.iNumber_of_USB_Pirate_radio
	CONST_INT iMax_Number_Of_Active_Buried_stash  2
	
	#IF IS_DEBUG_BUILD
		BOOL g_bSet_All_Buried_stash_Collected
		BOOL g_bToggle_Buried_stash_Collectables
		BOOL g_bBlip_All_Buried_stash_Collectables
		BOOL g_bclearcollectables_Buried_stash
		BOOL g_bwarp_to_next_Buried_stash_collectable
		INT g_iselected_Buried_stash_collectable
		INT g_iactive_Buried_stash[10]
		BOOL g_bToggle_Buried_stash_ignore_scoping_mission_check
		BOOL g_bSet_9_Buried_stash_Collected
		FLOAT gBuried_stash_ui_x 		=  0.21
		FLOAT gBuried_stash_ui_y 		=  0.39
		FLOAT gBuried_stash_ui_width 	=  0.36
		FLOAT gBuried_stash_ui_height 	=  0.65
		
		INT gBuried_stash_BarsR0 = 255
		INT gBuried_stash_BarsG1 = 10
		INT gBuried_stash_BarsB2 = 10
		
		INT gBuried_stash_LightsR0 = 255
		INT gBuried_stash_LightsG1 = 10
		INT gBuried_stash_LightsB2 = 10
		
		INT gBuried_stash_WrongwayR0 = 255
		INT gBuried_stash_WrongwayG1 = 10
		INT gBuried_stash_WrongwayB2 = 10
		BOOL bgBuried_stash_EDIT_UI  = FALSE
		FLOAT fgBuried_stash_Reading
		FLOAT fgBuried_stash_range   = -1
		
		BLIP_INDEX g_debug_blip_Buried_stash[iMax_Number_Of_Buried_stash]
		
		BOOL bunlockallfixerweaponskins = false
	#ENDIF
	
	#ENDIF 
	
			
	BOOL bis_Trick_Or_Treat_collectables_on
	INT i_Trick_Or_Treat_collectables_currently_close_to = -1
	INT i_Trick_Or_Treat_collectables_timer = 0

	
	BOOL bis_LD_Organics_collectables_on
	INT i_LD_Organics_collectables_currently_close_to = -1
	INT i_LD_Organics_collectables_timer = 0
	CONST_INT iMax_Number_Of_LD_Organics 100  // replaced with g_sMPTunables.iNumber_of_LD_Organics 
	
	BOOL bis_Skydives_collectables_on
	INT i_Skydives_collectables_currently_close_to = -1
	INT i_Skydives_collectables_timer = 0
	CONST_INT iMax_Number_Of_Skydives 10  // replaced with g_sMPTunables.iNumber_of_Skydive
	CONST_INT iMax_Number_Of_Active_Skydives 10
	
		
	BOOL bis_SNOWMEN_collectables_on
	INT i_SNOWMEN_collectables_currently_close_to = -1
	INT i_SNOWMEN_collectables_timer = 0
	CONST_INT iMax_Number_Of_SNOWMEN 15  
	CONST_INT iMax_Number_Of_Active_SNOWMEN 15
	
		
	BOOL bis_Dead_drop_collectables_on
	INT i_Dead_drop_collectables_currently_close_to = -1
	INT i_Dead_drop_collectables_timer = 0
	CONST_INT iMax_Number_Of_Dead_drop 1  
	CONST_INT iMax_Number_Of_Active_Dead_drop 1
	
	#IF IS_DEBUG_BUILD
		// 10 to collect, 200 possible locations
		
		BOOL g_bSet_All_Trick_Or_Treat_Collected
		BOOL g_bToggle_Trick_Or_Treat_Collectables
		BOOL g_bBlip_All_Trick_Or_Treat_Collectables
		BOOL g_bclearcollectables_Trick_Or_Treat
		BOOL g_bwarp_to_next_Trick_Or_Treat_collectable
		INT g_iselected_Trick_Or_Treat_collectable
		BOOL g_bSet_9_Trick_Or_Treat_Collected
		BLIP_INDEX g_debug_blip_Trick_Or_Treat[200]

	
		BOOL g_iTrick_Or_Treat_collectable_has_initialised = FALSE
		
		//100 LD organics to collect
		BOOL g_bSet_All_LD_Organics_Collected
		BOOL g_bToggle_LD_Organics_Collectables
		BOOL g_bBlip_All_LD_Organics_Collectables
		BOOL g_bclearcollectables_LD_Organics
		BOOL g_bwarp_to_next_LD_Organics_collectable
		INT g_iselected_LD_Organics_collectable
		BOOL g_bSet_9_LD_Organics_Collected
		BLIP_INDEX g_debug_blip_LD_Organics[100]


		
		BOOL g_iLD_Organics_collectable_has_initialised = FALSE
		
		
		BOOL g_bSet_All_Skydives_Collected
		BOOL g_bSet_All_Skydives_Gold
		BOOL g_bToggle_Skydives_Collectables
		BOOL g_bBlip_All_Skydives_Collectables
		BOOL g_bclearcollectables_Skydives
		BOOL g_bwarp_to_next_Skydives_collectable
		INT g_iselected_Skydives_collectable
		BOOL g_bSet_9_Skydives_Collected
		BOOL g_bSet_9_Skydives_Gold
		BLIP_INDEX g_debug_blip_Skydives[10]
		BOOL g_iSkydives_collectable_0[25]
		BOOL g_iSkydives_collectable_1[25]
		BOOL g_iSkydives_collectable_2[25]
		BOOL g_iSkydives_collectable_3[25]
		BOOL g_iSkydives_collectable_4[25]
		BOOL g_iSkydives_collectable_5[25]
		BOOL g_iSkydives_collectable_6[25]
		BOOL g_iSkydives_collectable_7[25]
		BOOL g_iSkydives_collectable_8[25]
		BOOL g_iSkydives_collectable_9[25]
		INT g_iactive_Skydives[10]
		
		BOOL g_iSkydives_collectable_has_initialised = FALSE
		INT g_iactive_Dead_Drop_area[15]
		INT g_iactive_Dead_Drop_location[5]
		BOOL g_bSet_All_Snowmen_Collected
		BOOL g_bToggle_Snowmen_Collectables
		BOOL g_bBlip_All_Snowmen_Collectables
		BOOL g_bclearcollectables_Snowmen
		BOOL g_bwarp_to_next_Snowmen_collectable
		INT g_iselected_Snowmen_collectable
		BOOL g_bSet_9_Snowmen_Collected
		BLIP_INDEX g_debug_blip_Snowmen[25]
		BOOL g_iSnowmen_collectable_has_initialised = FALSE
		
		BOOL g_bSet_All_Dead_Drop_Collected
		BOOL g_bToggle_Dead_Drop_Collectables
		BOOL g_bBlip_All_Dead_Drop_Collectables
		BOOL g_bclearcollectables_Dead_Drop
		BOOL g_bwarp_to_next_Dead_Drop_collectable
		BOOL g_bSet_9_Dead_Drop_Collected
		BLIP_INDEX g_debug_blip_Dead_Drop[1]
		BOOL g_iDead_Drop_collectable_has_initialised = FALSE
	#ENDIF
	
#ENDIF

CONST_INT ciMAX_REVOLVER_TREASURE_HUNT_PROGRESSION	4
CONST_INT ciMAX_STONE_HATCHET_BOUNTIES_PROGRESSION	6

#IF FEATURE_COPS_N_CROOKS
BOOL bis_police_badge_collectables_on
INT i_police_badge_collectables_currently_close_to = -1
INT i_police_badge_collectables_timer = 0
CONST_INT iMax_Number_Of_police_badges   30

#IF IS_DEBUG_BUILD
	BOOL g_bSetAllpolicebadgeCollected
	BOOL g_bTogglepolicebadgeCollectables
	BOOL g_bBlipAllpolicebadgeCollectables
	BOOL g_bclearcollectablespolicebadge
	BOOL g_bwarp_to_next_policebadge_collectable
	INT g_iselectedpolicebadgecollectable
	BOOL g_bSet29policebadgeCollected
	BLIP_INDEX g_debug_blip_policebadge[iMax_Number_Of_police_badges]
#ENDIF

BOOL bis_gun_parts_collectables_on
INT i_gun_parts_collectables_currently_close_to = -1
INT i_gun_parts_collectables_timer = 0
CONST_INT iMax_Number_Of_Gun_Parts   10


#IF IS_DEBUG_BUILD
	BOOL g_bSetAllGunPartsCollected
	BOOL g_bToggleGunPartsCollectables
	BOOL g_bBlipAllGunPartsCollectables
	BOOL g_bclearcollectablesGunParts
	BOOL g_bwarp_to_next_GunParts_collectable 
	INT g_iselectedGunPartscollectable = 1
	BOOL g_bSet29GunPartsCollected
	BLIP_INDEX g_debug_blip_GunParts[iMax_Number_Of_gun_parts]
#ENDIF

//BOOL bis_tagging_collectables_on
//INT i_tagging_collectables_currently_close_to = -1
//INT i_tagging_collectables_timer = 0
//CONST_INT iMax_Number_Of_Tagging 30

#IF IS_DEBUG_BUILD
//	BOOL g_bSetAllTaggingCollected
//	BOOL g_bToggleTaggingCollectables
//	BOOL g_bBlipAllTaggingCollectables
//	BOOL g_bclearcollectablesTagging
//	BOOL g_bwarp_to_next_Tagging_collectable
//	INT g_iselectedTaggingcollectable
//	BOOL g_bSet29TaggingCollected
//	BLIP_INDEX g_debug_blip_Tagging[iMax_Number_Of_Tagging]
//	BOOL g_bHasSprayCAnBeenCollectedTagging
//
//
//	OBJECT_INDEX g_otestposterspraycan
	
#ENDIF



STRUCT CNC_CROOKS_XP_ACTIVITIES
	INT iHoldUps
	INT iHackAtm
	INT iStolenGoodsVehicle
	INT iMugging
	INT iPackageDrop
	BOOL bChopperEscape
ENDSTRUCT

STRUCT CNC_COP_XP_ACTIVITIES
	INT iINCAPACITATE
	INT iArrest
	INT iCopKillerArrest
	INT iCopKillerKill
	BOOL bPointsReached
ENDSTRUCT

STRUCT CNC_XP_RANKUP_STRUCT
	BOOL bRankUp
	INT iOldRank
	INT iNewRank
ENDSTRUCT

STRUCT CNC_REWARD_EARNS_STRUCT
	#IF IS_DEBUG_BUILD
	INT iDebugXPToAward
	#ENDIF
	BOOL bAwardedXP
	BOOL bAwardedTokens
	BOOL bTriggerSave
	BOOL bXPBroadcastSet
	INT iBoostXPValue = 1
	CNC_CROOKS_XP_ACTIVITIES crooks
	CNC_COP_XP_ACTIVITIES cops
	CNC_XP_RANKUP_STRUCT returnData
	GENERIC_TRANSACTION_STATE transaction
	INT iXPEarnedThisGameActivities
	INT iXPEarnedThisGameChallenges
	INT iXPEarnedThisGameMatch
	INT iXPEarnedThisGameTotal
	
	INT iCashEarnedThisGameChallenges
	INT iCashEarnedThisGameBonus
	INT iCashEarnedThisGameMatch
	INT iCashEarnedThisGameTotal
ENDSTRUCT
CNC_REWARD_EARNS_STRUCT  cncReward_Earns

ENUM CNC_MATCH_RESULT
	eMATCHRESULT_NONE,
	eMATCHRESULT_WIN,
	eMATCHRESULT_DRAW,
	eMATCHRESULT_LOSE
ENDENUM

#ENDIF // FEATURE_COPS_N_CROOKS

#IF FEATURE_CASINO
// COLLECTABLES
CONST_INT iMax_Number_Of_Card_Collectables 				54
CONST_INT iMax_Number_Of_Action_Figures_Collectables 		100


INT g_iPedReservedSlotMachineBS = 0
INT g_iPedReservedSlotMachineBS2 = 0
INT g_iPedReservedInsideTrackSeatBS = 0
INT g_iPedReservedBlackjackTable = -1
INT g_iPedReservedThreeCardPokerTable = -1
INT g_iPedReservedRouletteTable = -1
INT g_iSpectatingSlotMachinePedID = -1
INT g_iManagerPedVariation = -1
INT g_iSpectatingSlotMachinePedType = 0
INT g_iInsideTrackPedBettingBS = 0
INT g_iSpecialPedBethBlackjackActivity = -1
INT g_iSpecialPedEileenSlotMachineActivity = -1
INT g_iSpecialPedCarolBlackjackActivity = -1
INT g_iSpecialPedLaurenBlackjackActivity = -1
INT g_iSpecialPedTaylorBlackjackActivity = -1
INT g_iSpecialPedRouletteSeatThreeActivity = -1
INT g_iSpecialPedRouletteSeatFourActivity = -1

CASINO_DEALER_PED_TYPES g_eBlackjackDealerPeds[4]
CASINO_DEALER_PED_TYPES g_eThreeCardPokerDealerPeds[4]
CASINO_DEALER_PED_TYPES g_eRouletteDealerPeds[6]

CONST_INT ciCASINO_PATROL_SECURITY_GUARD_ID	5
CASINO_SECURITY_GUARD_PED_TYPES g_eSecurityGuardPeds[6]
BOOL g_bSecurityGuardPedDataSet = FALSE

CONST_INT ciCASINO_GUARDS 7	
PED_INDEX g_CasinoSecurityGuardPedId[ciCASINO_GUARDS]

INT g_iPokerPedActivitiesID[4]
INT g_iRoulettePedActivitiesID[4]
INT g_iBlackjackPedActivitiesID[4]
INT g_iInsideTrackPedActivitiesID[3]
INT g_iTableGameSpectatorActivities[2]
INT g_iSlotMachinePedActivitiesID[11]
INT g_iSlotMachineSpectatorActivity = 0

BOOL g_bCasinoIntroCutsceneRequired = FALSE
BOOL g_bRefreshCasinoPedsAfterIntroCutscene = FALSE

INT g_iAnimDictsRequestedPerFrame = 0
INT g_iCasinoPedAnimsDictLimitPerFrame = 100

CONST_INT ciCASINO_MAITRED_CHAIRS					2
OBJECT_INDEX g_objMaitredChair[ciCASINO_MAITRED_CHAIRS]

CONST_INT ciINSIDE_TRACK_PEDS_RACE_IS_UPCOMING		0
CONST_INT ciINSIDE_TRACK_PEDS_RACE_IN_PROGRESS		1
CONST_INT ciINSIDE_TRACK_PEDS_RACE_COMPLETE		2
CONST_INT ciINSIDE_TRACK_PEDS_HAS_BET_ON_RACE		3
INT g_iInsideTrackPedsBS[3]

INSIDE_TRACK_RACE_DATA g_rdTriggerRaceFinishData
INT g_iInsideTrackMainEventSeed = 0
INSIDE_TRACK_RACE_DATA g_rdInsideTrackHostMainEvent
INT g_iInsideTrackTelemetryPlayersAtTable = 0
INT g_iInsideTrackTelemetryTableID = -1
INT g_iInsideTrackTelemetryDurationGameTime = 0
INT g_iInsideTrackTelemetrySingleEventDurationGameTime = 0
INT g_iInsideTrackTelemetryMainEventDurationGameTime = 0

INT g_iCasinoCarParkRequestCounter
INT g_iCasinoCarParkFirstEmpty
GENERIC_TRANSACTION_STATE g_eInsideTrackFMTransaction = TRANSACTION_STATE_DEFAULT

TEXT_LABEL_63 g_tlCasinoApartmentTCM = ""
TEXT_LABEL_63 g_tlArcadeTCM = ""

NETWORK_INDEX g_niArcadeBartender

#IF FEATURE_HEIST_ISLAND
INT g_iCasinoNightclubLightRigOverride = -1
#ENDIF

#IF IS_DEBUG_BUILD
BOOL g_bBypassAllDecorRestrictions

BOOL g_bOverrideCasinoNightClubLightRigTime = FALSE
INT g_iCasinoNightClubLightRigSecond = 0
INT g_iCasinoNightClubLightRigMinute = 0
INT g_iCasinoNightClubLightRigHour = 0
INT g_iCasinoNightClubLightRigDay = 3
INT g_iCasinoNightClubLightRigMonth = 11
INT g_iCasinoNightClubLightRigYear = 2020
#ENDIF

#ENDIF

#IF FEATURE_TUNER
INT g_iCarMeetCarParkRequestCounter
INT g_iCarMeetCarParkFirstEmpty

INT g_iCarMeetCarPlayerStagger = 0
#ENDIF

#IF FEATURE_FIXER
STRUCT INTERIOR_WEAPON_PICKUP_DATA
	PICKUP_INDEX PickupWeapon
	INT iPickupFlags
	INT iLastWeaponCreateTime
	BOOL bLastWeaponPickedUp
	INTERIOR_WEAPON_PICKUP_PLACEMENT_DATA PlacementData
ENDSTRUCT
INTERIOR_WEAPON_PICKUP_DATA g_FixerHQWeaponPickupData[MAX_NUM_INTERIOR_WEAPON_PICKUPS]


ENUM PAYPHONE_FLOW_STATE
	ePAYPHONEFLOW_STATE_WAIT,
	ePAYPHONEFLOW_STATE_AVAILABLE,
	ePAYPHONEFLOW_STATE_ACTIVE,
	ePAYPHONEFLOW_STATE_LAUNCHING,
	ePAYPHONEFLOW_STATE_ON_MISSION
ENDENUM

ENUM ePAYPHONEFLOW_BITSET
	ePAYPHONEFLOWBITSET_REQUESTED = 0, 														// 0
	ePAYPHONEFLOWBITSET_FLASHED, 															// 1
	ePAYPHONEFLOWBITSET_SHOWN_HELP, 														// 2
	ePAYPHONEFLOWBITSET_ACTIVE_ONCE,														// 3
	ePAYPHONEFLOWBITSET_FADED_OUT,															// 4
	ePAYPHONEFLOWBITSET_FIRST_TIME,															// 5
	ePAYPHONEFLOWBITSET_INITIALISED,														// 6
	ePAYPHONEFLOWBITSET_USE_REQUEST_DELAY,													// 7
	
	ePAYPHONEFLOWBITSET_END
ENDENUM
CONST_INT PAYPHONEFLOW_BITSET_ARRAY_SIZE	(COUNT_OF(ePAYPHONEFLOW_BITSET) + 30) / 32

#IF IS_DEBUG_BUILD
STRUCT PAYPHONE_FLOW_GLOBAL_DEBUG_STRUCT
	
	BOOL bTriggerRequest
	BOOL bSkipTimer
	BOOL bDrawTriggers
	
ENDSTRUCT
#ENDIF

STRUCT PAYPHONE_FLOW_GLOBAL_STRUCT

	PAYPHONE_FLOW_STATE eState
	
	INT iBitset[PAYPHONEFLOW_BITSET_ARRAY_SIZE]
	
	SCRIPT_TIMER stTimer
	INT iPayphone = -1
	INT iWaitTime = -1
	INT iSoundID = -1
	BLIP_INDEX blip
	
	#IF IS_DEBUG_BUILD
	PAYPHONE_FLOW_GLOBAL_DEBUG_STRUCT sDebug
	#ENDIF
	
ENDSTRUCT

PAYPHONE_FLOW_GLOBAL_STRUCT g_PayphoneFlow
#ENDIF

#IF FEATURE_GEN9_EXCLUSIVE
STRUCT BLOCKED_PLAYER_TICKER_STRUCT
	INT iBlockedPlayerBitSet
ENDSTRUCT
BLOCKED_PLAYER_TICKER_STRUCT g_sBlockedPlayers
#ENDIF

CONST_INT ciNUMBER_OF_INVALID_START_LOCATIONS 3
STRUCT BG_SCRIPT_INVALID_START_LOCATION_DATA
	BOOL bRun
	VECTOR VecCoors1[ciNUMBER_OF_INVALID_START_LOCATIONS]
	VECTOR VecCoors2[ciNUMBER_OF_INVALID_START_LOCATIONS]
	FLOAT DistanceP1toP4[ciNUMBER_OF_INVALID_START_LOCATIONS] 
ENDSTRUCT
BG_SCRIPT_INVALID_START_LOCATION_DATA g_sBgInvalidStartLocations
