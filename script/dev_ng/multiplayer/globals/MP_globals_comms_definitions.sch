USING "rage_builtins.sch"
USING "types.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_comms.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains constant, enum, and struct definitions for all MP communication systems.
//							NB. Should not contain instantiated variables.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


// ===========================================================================================================
//      General Communication globals
// ===========================================================================================================

// String lengths to ensure they fit into text labels
CONST_INT	MAX_LENGTH_MP_COMMS_CHARACTER_ID_STRING		23
CONST_INT	MAX_LENGTH_MP_COMMS_SPEAKER_ID_STRING		1
CONST_INT	MAX_LENGTH_MP_COMMS_GROUP_ID_STRING			7
CONST_INT	MAX_LENGTH_MP_COMMS_ROOT_ID_STRING			15
CONST_INT	MAX_LENGTH_MP_COMMS_COMPONENT_TEXTLABEL		31


// -----------------------------------------------------------------------------------------------------------

// MP Communication modifier bitflags - to describe optional behaviours
CONST_INT	ALL_MP_COMMS_MODIFIERS_CLEAR				0
CONST_INT	MP_COMMS_MODIFIER_COMMS_FORCE_ONSCREEN		0		// Force the cellphone on-screen
CONST_INT	MP_COMMS_MODIFIER_TXTMSG_ALLOW_BARTER		1		// Allow the middle 'barter' option on a text message
CONST_INT	MP_COMMS_MODIFIER_COMMS_LOCKED				2		// Player can't manually delete these comms (txtmsgs or emails)
CONST_INT	MP_COMMS_MODIFIER_NO_HANGUP_FOR_PHONECALL	3		// Disables the hangup button on a phonecall
CONST_INT	MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER	4		// Forces the player to answer the call
CONST_INT	MP_COMMS_MODIFIER_NO_HANGUP_ON_PAUSEMENU	5		// Don't hangup the phone when teh pause menu is active
CONST_INT	MP_COMMS_MODIFIER_IGNORE_RECENT_INVITE		6		// Ignores the Comms delay that occurs when there has been a recent invite
CONST_INT	MP_COMMS_MODIFIER_PRIORITY_PHONECALL		7		// Uses a higher priority than the default - intended for flow phonecalls


// -----------------------------------------------------------------------------------------------------------

// Used as a default value for a Player Index stored as an INT
CONST_INT	MP_COMMS_ILLEGAL_PLAYER_INDEX				-1


// -----------------------------------------------------------------------------------------------------------

// Used as a default value for timing out a reply required after sending a txtmsg
CONST_INT	MP_COMMS_DEFAULT_TXTMSG_REPLY_TIMEOUT_msec	120000


// -----------------------------------------------------------------------------------------------------------

// Used as a default value for headshot generation timeout - to ensure headshot generation doesn't delay a text message for too long
CONST_INT	MP_COMMS_HEADSHOT_GENERATION_TIMEOUT_msec	7000


// -----------------------------------------------------------------------------------------------------------

// Used as a default INT value when there is no Int substring Component
CONST_INT	NO_INT_SUBSTRING_COMPONENT_VALUE			-99




// ===========================================================================================================
//      End of Mission Message globals
// ===========================================================================================================

// End Of Mission Stages - bitflags
CONST_INT	CLEAR_ALL_MP_EOM_MESSAGE_BITFLAGS	0	// Used to set the bitflag int back to 0
CONST_INT	MP_EOM_MESSAGE_RECEIVED				0	// An EOM message has been received
CONST_INT	MP_EOM_MESSAGE_ACTIVE				1	// An EOM message is actively being displayed


// -----------------------------------------------------------------------------------------------------------

STRUCT g_structEndOfMissionMP
	INT				bitflagsEOM			// Any EOM flags
	TEXT_LABEL_23	characterEOM		// The Character ID doing the talking
	TEXT_LABEL_7	speakerEOM			// The Speaker ID (0-8, A-Z)
	TEXT_LABEL_7	groupEOM			// The Subtitle Group ID for the Conversation
	TEXT_LABEL_15	rootEOM				// The Root ID for the Conversation
ENDSTRUCT





// ===========================================================================================================
//      Stored Message globals
//		(Some Comms are fired from events and so can't repeatedly call the Comms functions.
//			This will store a Comms and every frame try to trigger it - like the EOM Message)
// ===========================================================================================================

// The types of Communications that need their own set of Stored Communications variables
ENUM g_eMPStoredCommsTypes
	SCT_GENERAL_COMMS,					// Used for all General Communications that can't be maintained by the calling script
	// Leave this at the bottom
	MAX_STORED_COMMS_TYPES
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// Stored Comms Stages - bitflags
CONST_INT	CLEAR_ALL_MP_STORED_COMMS_BITFLAGS	0	// Used to set the bitflag int back to 0
CONST_INT	MP_STORED_COMMS_RECEIVED			0	// A Stored Comms has been received
CONST_INT	MP_STORED_COMMS_ACTIVE				1	// A Stored Comms is actively being displayed


// -----------------------------------------------------------------------------------------------------------

STRUCT g_structStoredCommsMP
	INT				scBitflags			// Any Stored Comms flags
	TEXT_LABEL_23	scCharacter			// The Character ID doing the talking
	TEXT_LABEL_7	scSpeaker			// The Speaker ID (0-8, A-Z)
	INT				scCharSheetAsInt	// The CharSheet ID as INT
	TEXT_LABEL_7	scGroup				// The Subtitle Group ID for the Conversation
	TEXT_LABEL_15	scRoot				// The Root ID for the Conversation
	TIME_DATATYPE	scInitialTimeout	// The initial delay timeout time when this communication can attempt to play
	BOOL 			scInitialTimeoutInitialised
	TEXT_LABEL_23	scPostHelp			// Any single help to be played after a communication has finished
ENDSTRUCT





// ===========================================================================================================
//      MP Communication Txtmsg globals
// ===========================================================================================================

// MP Txtmsg types - used as priorities - the higher the value the higher the priority
CONST_INT	NO_MP_TXTMSG_TYPE					0	// DEFAULT: lowest priority
CONST_INT	MP_TXTMSG_TYPE_FROM_CHARSHEETID		1	// Standard Messages from CharSheetID: medium priority
CONST_INT	MP_TXTMSG_TYPE_FROM_PLAYER			2	// Standard Messages from Player: high priority


// -----------------------------------------------------------------------------------------------------------

// MP Txtmsg Options - bitflags
CONST_INT	CLEAR_ALL_MP_TXTMSG_BITFLAGS		0	// Used to set the bitflag int back to 0
CONST_INT	MP_TXTMSG_RECEIVED					0	// The Txtmsg has been received in this array position
CONST_INT	MP_TXTMSG_ACTIVE					1	// A Txtmsg in this array position is actively being displayed
CONST_INT	MP_TXTMSG_SENT_BY_PLAYERINDEX		2	// The txtmsg was sent by a PLAYER_INDEX
CONST_INT	MP_TXTMSG_SENT_BY_CHARSHEETID		3	// The txtmsg was sent by a CharSheetID
CONST_INT	MP_TXTMSG_COMPONENTS_IN_USE			4	// If components are being used for inserting into the main text message string
CONST_INT	MP_TXTMSG_COMPONENT_IS_PLAYER		5	// The string component is a Player Name


// -----------------------------------------------------------------------------------------------------------

// The maximum number of text messages that can be stored at any one time
// KGM 15/11/12: Boosted to 8 because txtmsgs about Bounties use the txtmsg comms system.
CONST_INT	MAX_MP_TXTMSGS						8


// -----------------------------------------------------------------------------------------------------------

// Indicates an invalid sender
CONST_INT	INVALID_MP_TXTMSG_SENDER			-1


// -----------------------------------------------------------------------------------------------------------

// MP Txtmsg details
STRUCT g_structTxtmsgMP
	INT					tmBitflags			// Any Txtmsg flags
	INT					tmType				// The text Message type
	INT					tmSenderAsInt		// The PlayerIndex or CharSheetID sending the txtmsg
	TEXT_LABEL_15		tmTextMessage		// The Text Message text label
	TEXT_LABEL_31		tmComponentString	// An Optional String Component for inserting into the text message
	INT					tmComponentInt		// An Optional Int Component for inserting into the text message
ENDSTRUCT




// ===========================================================================================================
//      MP Communication Candidate globals
// ===========================================================================================================

// MP Communication Candidate types - used as priorities - the higher the value the higher the priority
CONST_INT	NO_MP_COMMS_TYPE					0	// DEFAULT: lowest priority
CONST_INT	MP_COMMS_TYPE_STANDARD_MESSAGE		1	// Standard Messages: medium priority
CONST_INT	MP_COMMS_TYPE_TXTMSG				2	// Standard Txtmsg: medium priority
CONST_INT	MP_COMMS_TYPE_EMAIL					3	// Standard EMail: medium priority
CONST_INT	MP_COMMS_TYPE_OFFER_MISSION			4	// Offer Mission Messages: high priority
CONST_INT	MP_COMMS_TYPE_REPLY_TXTMSG			5	// Reply Txtmsg Messages: high priority
CONST_INT	MP_COMMS_TYPE_REPLY_EMAIL			6	// Reply EMail: high priority
CONST_INT	MP_COMMS_TYPE_FORCE_ONTO_MISSION	7	// Force Onto Mission Messages: high priority
CONST_INT	MP_COMMS_TYPE_EOM_MESSAGE			8	// End of Mission Messages: highest priority


// -----------------------------------------------------------------------------------------------------------

// MP Communication Candidate states - describes the current state of the current favoured communication
ENUM g_eMPCommsStates
	NO_CURRENT_MP_COMMS_REQUEST,					// There is no current favoured communication request
	MP_COMMS_RECEIVED_THIS_FRAME,					// The current favoured communication was requested on this frame
	MP_COMMS_RECEIVED_PREVIOUS_FRAME,				// The current favoured communication was requested on the previous frame
	MP_COMMS_ACTIVE									// The current favoured communication is now actively being played
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Communication Candidate replies - describes the current state of the player's reply
ENUM g_eMPCommsReply
	NO_MP_COMMS_REPLY_REQUIRED,						// The current favoured communication does not require a reply
	MP_COMMS_REPLY_REQUIRED,						// The current favoured communication requires a reply
	MP_COMMS_REPLY_YES,								// The current favoured communication has received a YES reply
	MP_COMMS_REPLY_NO,								// The current favoured communication has received a NO reply
	MP_COMMS_REPLY_SPECIAL							// The current favoured communication has received a SPECIAL reply (probably 'BARTER' txtmsg reply)
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Communication Devices - specifies the type of device to use
ENUM g_eMPCommsDevice
	MP_COMMS_DEVICE_DEFAULT,						// Use the default device for this type of message
	MP_COMMS_DEVICE_CELLPHONE,						// Use the Cellphone to deliver the message
	MP_COMMS_DEVICE_RADIO,							// Use the Radio to deliver the message
	MP_COMMS_DEVICE_TXTMSG,							// Use the Cellphone to deliver the Txtmsg
	MP_COMMS_DEVICE_EMAIL,							// Use the Cellphone to deliver the Email
	
	// Leave this at the bottom
	MP_COMMS_DEVICE_UNKNOWN
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Communication Candidate details - contains the current favoured communication, or the active communication if it is being played
STRUCT g_structCommsCandidate
	g_eMPCommsStates		ccState				// The state of this communication
	INT						ccHashScriptName	// The hash key for the script name requesting this communication
	INT						ccHashGroupRoot		// The hash key for the Group ID/Conversation Root for this communication
	INT						ccHashBodyText		// The hash key for the body text components for this communication
	TEXT_LABEL_15			ccRootID			// The RootID (also the subtitleID) - needed to check if a txtmsg has completed
	INT						ccType				// The communication type - also used as the priority
	g_eMPCommsDevice		ccDevice			// The Communication Device requested for the message - changes to be the used device when played
	g_eMPCommsReply			ccReply				// The reply requirements and state for this communication - copies YES/NO into Reply struct when received
	TIME_DATATYPE			ccTimeout			// The safety timeout for killing this Communication that requires a Reply
	BOOL					ccTimeoutInitialised
	TIME_DATATYPE			ccHeadshotTimeout	// A Timeout used to ensure the headshot generation doesn't delay a text message for too long
	INT						ccModifiers			// Any flags to modify the default behaviour
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// MP Communication Reply details - contains the current reply status of the active or most recently finished communication
// This ensures the reply is available to be collected after the communication has finished
//		(in fact, collection afterwards, rather than during the communication, will be enforced)
STRUCT g_structCommsReply
	INT					crHashScriptName		// The hash key for the script name that the reply is intended for
	g_eMPCommsReply		crReply					// The reply state for active, or most recently finished, communication
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// MP Communication Completion details - contains whether the most recently finished communication ended successfully or not
// This ensures the reply is available to be collected after the communication has finished
//		(in fact, collection afterwards, rather than during the communication, will be enforced)
STRUCT g_structCommsEndStatus
	BOOL				cesRequiresAbortedCheck	// TRUE if this communication requires an aborted check when completed, FALSE if not
	BOOL				cesEndedSuccessfully	// TRUE if the communication ended successfully, FALSE if it was interrupted
ENDSTRUCT





// ===========================================================================================================
//      The global communications variables struct
// ===========================================================================================================

// Contains all the MP Communication global variables
STRUCT g_structMPGlobalsComms
	g_structEndOfMissionMP		EOM										// End of Mission Communications globals
	g_structStoredCommsMP		storedComms[MAX_STORED_COMMS_TYPES]		// Array of Stored Communications globals - The Stored Comms routines can handle multiple types of delayed comms
	g_structCommsCandidate		priorityComms							// The details of the current highest priority communcation request
	g_structCommsReply			commsHoldReply							// Stores the reply details for the active, or most recently finished, communication
	g_structCommsEndStatus		commsEndStatus							// Stores any status information for when teh communication ends (does not include player replies)
	g_structTxtmsgMP			mpTxtmsgs[MAX_MP_TXTMSGS]				// Array of Txtmsg details - multiple txtmsgs can be stored
ENDSTRUCT





// ===========================================================================================================
//      The global Temporary Comms Block variables struct
// ===========================================================================================================

// The timeout time for the temporary Comms block
CONST_INT				MP_COMMS_TEMPORARY_BLOCK_TIMEOUT_msec			3000

// The timeout time for the temporary Incoming Calls block
CONST_INT				MP_COMMS_INCOMING_CALLS_BLOCK_TIMEOUT_msec		1000


// -----------------------------------------------------------------------------------------------------------

// Contains all the control variables to introduce a temporary cellphone block
STRUCT g_structMPCommsTempBlock
	// NOTE: This calls DISABLE_CELLPHONE_THIS_FRAME_ONLY() every frame for a period of time
	BOOL			blockActive			= FALSE							// TRUE if the Temporary Comms Block is active, otherwise FALSE
	TIME_DATATYPE	blockTimeout										// Timeout time when the temporary block will end
	// This blocks calls to 'request mp comms message' and other cellphone call triggers
	BOOL			blockIncomingCalls	= FALSE							// TRUE if there is a temporary block for incoming calls only, otherwise FALSE
	TIME_DATATYPE	blockIncomingCallsTimeout							// Timeout time when the temporary incoming calls block will end
ENDSTRUCT





// ===========================================================================================================
//      The global Temporary Voice-Chat block during in-game contact phonecalls
// ===========================================================================================================

// The call status
ENUM g_eMPCommsVoiceChatBlock
	MP_COMMS_VC_BLOCK_RINGING,											// A call from a contact is ringing
	MP_COMMS_VC_BLOCK_TALKING,											// A call from a contact is ongoing
	// Leave this at the bottom
	MP_COMMS_VC_BLOCK_NONE												// There is no current call from a contact
ENDENUM

