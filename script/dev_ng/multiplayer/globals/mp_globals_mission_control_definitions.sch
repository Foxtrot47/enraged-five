USING "rage_builtins.sch"

USING "MP_Mission_Data_Struct.sch"		// For the MP_MISSION_DATA struct


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_mission_control.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains constant, enum, and struct definitions for all MP mission control routines.
//							NB. Should not contain instantiated variables.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Mission Control globals
// ===========================================================================================================

// An invalid radius
CONST_FLOAT	INVALID_RADIUS						-1.0

// Number of Mission Request Slots required
CONST_INT	MAX_NUM_MISSIONS_ALLOWED			16

// No Tutorial Session active
CONST_INT	TUTORIAL_SESSION_NOT_ACTIVE			-1


// -----------------------------------------------------------------------------------------------------------

// Used to clear the various control bits and to check for this
CONST_INT	ALL_MISSION_CONTROL_BITS_CLEAR		0


// -----------------------------------------------------------------------------------------------------------

// MP Mission Request Source ID - describes where the mission request came from
ENUM g_eMPMissionSource
	MP_MISSION_SOURCE_GROUPED,				// Grouped MIssion Conditions have been met
	MP_MISSION_SOURCE_JOBLIST,				// Player requested the mission from Joblist
	MP_MISSION_SOURCE_INVITE,				// Player is trying to accept an invite to an existing mission request
	MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN,	// Player at map blip but treat as an invite rather than a walk-in, but allow new instance of mission (so JIPing a corona or starting a playlist doesn't get blocked)
	MP_MISSION_SOURCE_MISSION_FLOW_AUTO,	// Mission Flow (DO_MISSION_NOW) requested the mission
	MP_MISSION_SOURCE_MISSION_FLOW_BLIP,	// Mission Flow (DO_MISSION_AT_BLIP) requested the mission
	MP_MISSION_SOURCE_DEBUG_GROUPED,		// Grouped Missions forced to launch by debug
	MP_MISSION_SOURCE_DEBUG_MENU,			// Mission launched from In-Game Debug Menu
	MP_MISSION_SOURCE_AMBIENT,				// Ambient Mission request
	MP_MISSION_SOURCE_HEIST,				// Heist Mission request
	
	// Leave this at the bottom
	UNKNOWN_MISSION_SOURCE					// None of the above sources
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Mission Request State - describes the current status of a mission request
ENUM g_eMPMissionStatus
	MP_MISSION_STATE_ACTIVE,				// At least one player has joined the mission
	MP_MISSION_STATE_ACTIVE_SECONDARY,		// The mission is running and the Secondary Teams have just been asked to join it
	MP_MISSION_STATE_OFFERED,				// A mission has been offered to players but no player has yet accepted
	MP_MISSION_STATE_RECEIVED,				// A new request has been received but is unprocessed
	MP_MISSION_STATE_RESERVED,				// Reserves a mission but doesn't start it - intended for use by missions started by multiple people on a contact point
	
	// Leave this at the bottom
	NO_MISSION_REQUEST_RECEIVED				// This set of Mission Request Data is unused
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Mission Reservation Descriptions - describes how a player has been reserved for a mission
// NOTE: This is used only for temporary comparisons and isn't stored - player bitfields are used to store this
ENUM g_eMPMissionReservationDescs
	MP_RESERVED_BY_PLAYER,					// A player is reserved by himself for a mission (ie: player standing in a blip)
	MP_RESERVED_BY_LEADER,					// A player is reserved by a leader for a mission (ie: Heist Leader is planning a Heist)
	MP_RESERVED_BY_GAME,					// A player is reserved by the game for a mission (ie: Players allocated to the mission to fill it up)
	
	// Leave this at the bottom
	NO_MP_RESERVATION_DESCRIPTION			// There is no specific reservation description
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Mission Types - copied from MP_Globals_Common.sch
// KGM 29/10/11: These need tidied up
ENUM MP_MISSION_TYPE
	eTYPE_SINGLE_MISSION,		// Launched via the phone/widgets, for one (or more later) persons.	
	eTYPE_GROUP_MISSION,		// Multiple people on same tean at a locate.
	eTYPE_LARGE_COP_MISSION,	// Large cop investigation style missions.
	eTYPE_HEIST,				// Crim heist.		
	eTYPE_PRE_HEIST,			// a heist planning script
	eTYPE_PRE_GROUP,			// a pre-group mission script
	eTYPE_AMBIENCE,				// an ambient location script (e.g. air hockey)
	eTYPE_RANDOM_EVENT,			// A random event.
	eTYPE_COP_INCIDENT,			// A Cop Incident Oddjob.
	eTYPE_TWO_TEAMS,			// A mission that requires players from at least two teams to be in the gameworld.
	eTYPE_TUTORIAL,				// A mission teaches players how to do something
	
	// Leave this at the bottom
	UNKNOWN_MISSION_TYPE		// None of the above mission types
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Mission Request Options Bits - allows a specific mission request to set some flags
CONST_INT		MPMR_OPTION_BITFLAG_PREMISSION_CUTSCENE			0					// TRUE if mission plays a pre-mission cutscene
CONST_INT		MPMR_OPTION_BITFLAG_FIRST_CROOK_GANG_JOINING	1					// TRUE if the first Crook Gang has now been sent join requests (used to play alternative pre-mission comms if needed)
CONST_INT		MPMR_OPTION_BITFLAG_PLAYERS_CAN_JOIN_MISSION	2					// TRUE if players can still join the mission - used for missions that are not team-based
#IF IS_DEBUG_BUILD
CONST_INT		MPMR_OPTION_DEBUG_BITFLAG_ONE_PLAYER_LAUNCH		3					// TRUE if mission can debug launch without obeying minimum player requirements, FALSE to obey minimum player requirements
#ENDIF
// KGM 29/8/13: Adding any new option flags for TitleUpdate from here
CONST_INT		MPMR_OPTION_BITFLAG_CLOSED_FOR_WALK_INS			4					// TRUE if the mission is closed to Walk-Ins (but still open for invitees), FALSE if it's open to all


// -----------------------------------------------------------------------------------------------------------

STRUCT g_structMissionRequestMP_OLD
	g_eMPMissionStatus		mrStatusID						= NO_MISSION_REQUEST_RECEIVED		// The current status of this Mission Request
	g_eMPMissionSource		mrSourceID						= UNKNOWN_MISSION_SOURCE			// The source of this Mission Request
	MP_MISSION_TYPE			mrMissionTypeID					= UNKNOWN_MISSION_TYPE				// The type of Mission being Requested
	MP_MISSION_DATA_OLD		mrMissionData														// The Mission Details
	PLAYER_INDEX			mrRequester															// The PlayerIndex of the requesting player
	BOOL					mrRequesterIsHost				= FALSE								// TRUE if the requester was a script host, FALSE if an individual player
	INT						mrNumPlayersInOtherSessions		= 0									// The number of players on this mission but in another transition session
	INT						mrTutorialSessionID				= TUTORIAL_SESSION_NOT_ACTIVE		// The Specific Tutorial Session ID players need to be pre-allocated in to join this mission (if needed)
	INT						mrBitsRequestedTeams			= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Teams that Requested to play the mission
	INT						mrBitsReservedTeams				= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Teams Reserved to play this mission
	INT						mrBitsJoinableTeams				= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Teams currently allowed to Join this mission (also temp used to store all free teams for mission as initial mission request is being processed - this saves having to re-calculate the free teams again when the reserved teams are being picked)
	INT						mrBitsActivePlayers				= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Players On Mission
	INT						mrBitsConfirmedPlayers			= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Players that have asked for confirmation that it is still ok to join the mission (only players offered the mission need to confirm just in case th emission has become full before they accepted)
	INT						mrBitsJoiningPlayers			= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Players invited or being forced onto mission, but not yet on mission
	INT						mrBitsReservedPlayersByPlayer	= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Subset of All Reserved Players - Reserved By Player Choice (ie: player at blip, player accepted Heist Invite, etc) - HIGHEST PRIORITY (replaces old 'Required' bitfield)
	INT						mrBitsReservedPlayersByLeader	= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Subset of All Reserved Players - Reserved By Leader Choice (ie: Leader is Planning a Heist) - MEDIUM PRIORITY
	INT						mrBitsReservedPlayersByGame		= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Subset of All Reserved Players - Reserved By Game (ie: As opposition for a mission) - LOW PRIORITY
	INT						mrBitsReservedPlayersAll		= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: All players reserved to join the mission (Player Choise, Team Choice, Game Choice
	INT						mrBitsExcludedPlayers			= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Players excluded to join the mission (ie: they've already been on it and left)
	INT						mrBitsRejectedPlayers			= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Players that have been offered the mission but have actively rejected it (to prevent auto-allocation)
	INT						mrBitsOptions					= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Contains any options associated with a specific mission request
ENDSTRUCT


// MP Mission Request Data - contains all control data associated with one Mission Request
STRUCT g_structMissionRequestMP
	g_eMPMissionStatus		mrStatusID						= NO_MISSION_REQUEST_RECEIVED		// The current status of this Mission Request
	g_eMPMissionSource		mrSourceID						= UNKNOWN_MISSION_SOURCE			// The source of this Mission Request
	MP_MISSION_TYPE			mrMissionTypeID					= UNKNOWN_MISSION_TYPE				// The type of Mission being Requested
	MP_MISSION_DATA			mrMissionData														// The Mission Details
	PLAYER_INDEX			mrRequester															// The PlayerIndex of the requesting player
	BOOL					mrRequesterIsHost				= FALSE								// TRUE if the requester was a script host, FALSE if an individual player
	INT						mrNumPlayersInOtherSessions		= 0									// The number of players on this mission but in another transition session
	INT						mrTutorialSessionID				= TUTORIAL_SESSION_NOT_ACTIVE		// The Specific Tutorial Session ID players need to be pre-allocated in to join this mission (if needed)
	INT						mrBitsRequestedTeams			= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Teams that Requested to play the mission
	INT						mrBitsReservedTeams				= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Teams Reserved to play this mission
	INT						mrBitsJoinableTeams				= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Teams currently allowed to Join this mission (also temp used to store all free teams for mission as initial mission request is being processed - this saves having to re-calculate the free teams again when the reserved teams are being picked)
	INT						mrBitsActivePlayers				= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Players On Mission
	INT						mrBitsConfirmedPlayers			= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Players that have asked for confirmation that it is still ok to join the mission (only players offered the mission need to confirm just in case th emission has become full before they accepted)
	INT						mrBitsJoiningPlayers			= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Players invited or being forced onto mission, but not yet on mission
	INT						mrBitsReservedPlayersByPlayer	= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Subset of All Reserved Players - Reserved By Player Choice (ie: player at blip, player accepted Heist Invite, etc) - HIGHEST PRIORITY (replaces old 'Required' bitfield)
	INT						mrBitsReservedPlayersByLeader	= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Subset of All Reserved Players - Reserved By Leader Choice (ie: Leader is Planning a Heist) - MEDIUM PRIORITY
	INT						mrBitsReservedPlayersByGame		= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Subset of All Reserved Players - Reserved By Game (ie: As opposition for a mission) - LOW PRIORITY
	INT						mrBitsReservedPlayersAll		= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: All players reserved to join the mission (Player Choise, Team Choice, Game Choice
	INT						mrBitsExcludedPlayers			= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Players excluded to join the mission (ie: they've already been on it and left)
	INT						mrBitsRejectedPlayers			= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Players that have been offered the mission but have actively rejected it (to prevent auto-allocation)
	INT						mrBitsOptions					= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Contains any options associated with a specific mission request
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// DYNAMIC ALLOCATION CONSTS
CONST_INT		DYNAMIC_ALLOCATION_DELAY_msec				15000


// -----------------------------------------------------------------------------------------------------------

// MP Mission Control Data - contains all control data associated with Mission Control
STRUCT g_structMissionControlMP
	INT						mcBitsPreviousPlayers			= ALL_MISSION_CONTROL_BITS_CLEAR	// BITFIELD: Holds the active network players from the previous frame - to detect when players quit MP
	PLAYER_INDEX			mcHost																// The current Host PlayerIndex
	TIME_DATATYPE			mcDynamicAllocTimeout												// The next time that a dynamic allocation check will take place
	BOOL					mcRestartDynamicAllocationTimer	= TRUE								// TRUE if the timer should be restarted whether expired or not (ensures a dynamic allocation doesn't occur immediately following a period where there were no missions)
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// CONSTS and Globals associated with the player's status within the Mission Controller
CONST_INT		MPMCP_NO_MISSION_SLOT		-1

ENUM g_eMPMissionPlayerStatus
	MP_PLAYER_STATE_ACTIVE,					// Player is actively on a mission
	MP_PLAYER_STATE_JOINING,				// Player is in a joining state for the mission (incorporates the confirmation)
	MP_PLAYER_STATE_RESERVED,				// Player has been reserved for the mission (incorporates the 'required' state)
	
	// Leave this at the bottom
	MP_PLAYER_STATE_NONE					// Player is not associated with any mission
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Mission Control Data - contains the current player information
STRUCT g_structMissionPlayersMP
	g_eMPMissionPlayerStatus	mcpState	= MP_PLAYER_STATE_NONE				// The players current status on his associated mission
	INT							mcpSlot		= MPMCP_NO_MISSION_SLOT				// The mission request slot containing the mission this player is connected with
ENDSTRUCT




// ===========================================================================================================
//      Mission Control Client-Based Globals - not broadcast
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//     Mission Control Client Globals for maintaining server broadcasts to the client for one frame
// -----------------------------------------------------------------------------------------------------------

// ENUM for Received Broadcast Type
ENUM g_eMCBroadcastType
	MCCBT_REQUEST_FAILED,		// Mission Request Failed to Launch
	MCCBT_MISSION_FINISHED,		// Mission Launched and is now finished
	MCCBT_MISSION_FULL,			// Mission Launched but this player didn't get on it
	MCCBT_CHANGE_UNIQUE_ID,		// The Unique ID has changed (because this player has joined an existing mission)
	MCCBT_PLAYER_ON_MISSION,	// The player is now actively on the mission
	MCCBT_CONFIRMED_RESERVED,	// The player has been successfully reserved for this mission
	
	// Leave this at the bottom
	MCCBT_NONE
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing any data for the stored broadcasts
STRUCT g_structMissionControlBroadcasts
	g_eMCBroadcastType	MCB_Type		= MCCBT_NONE		// The broadcast type
	INT					uniqueID		= NO_UNIQUE_ID		// The unique ID this relates to
	INT					generalInt1		= 0					// A general INT (eg: newUniqueID for 'change' type, numPlayers for 'confirmed' type)
	INT					generalInt2		= 0					// A general INT (player bits for 'confirmed' type)
	BOOL				frameRetain		= FALSE				// On frame broadcast is received, set this TRUE, on next frame set it FALSE, on next frame remove the broadcast - ensures all broadcasts remain for a full frame
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// The Broadcast array
// NOTE: Entries are always added to the end of the array and shuffled up the array to overwrite older entries
CONST_INT		MAX_MISSION_CONTROL_BROADCAST_ARRAY		32


// -----------------------------------------------------------------------------------------------------------
//     Mission Control Client Globals for resyncing after a Host migration
// -----------------------------------------------------------------------------------------------------------

// THE RESYNC TIMEOUT DELAY - this needs to be long enough to let the data settle taking network lag into account
CONST_INT	MCRESYNC_TIMER_DELAY_msec					12000

// THE QUICK RE-CHECK RESYNC TIMEOUT DELAY - used if the resync needs a bit more time
CONST_INT	MCRESYNC_TIMER_QUICK_RECHECK_DELAY_msec		3000


// -----------------------------------------------------------------------------------------------------------

// THE RESYNC ENTRY ON ARRAY TIMEOUT - how long an entry remains valid on the resync array
// NOTE: originally 20000, but there was one issue when the host was sitting on an assert, so I'm boosting this now that entries can be removed by FAIL, etc. messages
CONST_INT	MCRESYNC_ENTRY_ON_ARRAY_TIMEOUT_msec		70000


// -----------------------------------------------------------------------------------------------------------

// ACTIVITY being performed by client when the resync check is performed
ENUM g_eMCCResyncActivity
	MCCRA_PREMISSION,						// Client is in Pre-Mission stage (eg: pre-mission phonecall or pre-mission cutscene)
	MCCRA_AWAITING_SERVER_OK,				// Client is waiting for an 'ok to join' confirmation from server
	MCCRA_LAUNCHING_MISSION,				// Client is launching the mission and waiting for it to be active
	MCCRA_MISSION_ACTIVE,					// Client is running a mission
	
	// Leave this at the bottom
	MCCRA_NO_ACTIVITY						// Client wasn't performing an activity when the re-sync was performed
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// STATE the server thinks the player is in when the resync check is performed
ENUM g_eMCCResyncHostState
	MCCRHS_JOINING,							// Host thinks the player is joining a mission
	MCCRHS_CONFIRMED,						// Host thinks the player is confirmed for a mission
	MCCRHS_ACTIVE,							// Host thinks the player is active on a mission
	
	// Leave this at the bottom
	MCCRHS_NO_ACTIVE_STATE					// Host thinks the player isn't on a mission (or may be reserved for a mission)
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// STRUCT allowing the client to resync with the host after a Host migration (to recover from lost event broadcasts)
STRUCT g_structMissionControlClientResyncWithHost
	PLAYER_INDEX			mccrKnownHost				// The current host as known by this client
	TIME_DATATYPE			mccrTriggeringTimeout		// Timeout for when the next stage of the resync with the mission triggerer needs to be performed
	BOOL 					mccrTriggeringTimeoutInitialised
	g_eMCCResyncActivity	mccrActivityOnClient		// The current client activity - helps detect if mission triggerer is waiting for a server broadcast
	INT						mccrUniqueIDOnClient		// The current client uniqueID - helps detect if mission triggerer is waiting for a server broadcast
	g_eMCCResyncHostState	mccrStateOnServer			// The current activity the Host thinks the player is performing
	INT						mccrUniqueIDOnServer		// The current uniqueID the Host thinks the player is performing
	TIME_DATATYPE			mccrExternalTimeout			// Timeout for when the next stage of the resync checking should occur for broadcasts from external systems
	BOOL					mccrExternalTimeoutInitialised
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// COMMUNICATIONS from external system to server that may cause problems if they get lost during Host Migration
ENUM g_eMCCResyncCommsTypeID
	MCCRCH_REQUEST_SPECIFIC_MISSION,		// External request for a new Specific Mission
	MCCRCH_REQUEST_MISSION_OF_TYPE,			// External request for a new Mission of Type
	MCCRCH_REQUEST_JOIN_MISSION,			// External request to join an existing mission
	MCCRCH_RESERVE_MISSION_BY_PLAYER,		// External request to reserve a mission by a player
	MCCRCH_RESERVE_MISSION_BY_LEADER,		// External request to reserve a mission by a Leader
	MCCRCH_CANCEL_RESERVATION_BY_PLAYER,	// External request to cancel a player's reservation on a mission
	MCCRCH_CANCEL_RESERVED_BY_LEADER,		// External request to canel a mission reserved by a Leader
	MCCRCH_START_RESERVED_MISSION,			// External request to start an already reserved mission
	MCCRCH_CHANGE_RESERVED_MISSION,			// External request to change the details of a reserved mission
	MCCRCH_MISSION_NOT_JOINABLE,			// External request to stop other players joining a mission
	MCCRCH_MISSION_JOINABLE_FOR_PLAYERS,	// External request to allow players to join the mission again
	MCCRCH_READY_FOR_SECONDARY_TEAMS,		// External request for secondary teams to join the mission
	
	// Leave this at the bottom
	MCCRCH_NO_COMMS							// Host thinks the player isn't on a mission (or may be reserved for a mission)
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// A struct containing details of a broadcast from an external system to the mission controller
STRUCT g_structMissionControlResyncBroadcasts
	g_eMCCResyncCommsTypeID		mcrbCommsTypeID			= MCCRCH_NO_COMMS		// The TypeID of communication
	INT							mcrbUniqueID			= NO_UNIQUE_ID			// The uniqueID of the mission request
	MP_MISSION					mcrbMissionID			= eNULL_MISSION			// The Mission ID if the event broadcast requires it to change
	INT							mcrbVariation			= NO_MISSION_VARIATION	// The Mission Variation of the event broadcast requires it to change
	TIME_DATATYPE				mcrbTimeout										// The timeout time for when we can assume this broadcast has been accepted and dealt with
	BOOL 						mcrbInUseByActiveResync	= FALSE					// TRUE if this entry is part of an ongoing resync check
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// The maximum number of broadcasts that should be recorded at any one time
// NOTE: quite high to cope with the Grouped Mission server remote possibility of requesting a load of missions at once
CONST_INT	MAX_MC_RESYNC_BROADCASTS		12
