USING "commands_misc.sch"
USING "commands_camera.sch"

// ===========================================================================================================
//      spectator cam globals and consts
// ===========================================================================================================

CONST_FLOAT SPECTATOR_HIDE_POSITION_X	8000.0
CONST_FLOAT SPECTATOR_HIDE_POSITION_Y	8000.0
CONST_FLOAT SPECTATOR_HIDE_POSITION_Z 	1500.0


CONST_FLOAT GLOBAL_MP_TV_LAUNCH_DISTANCE						2.0

CONST_INT MAX_NUM_SPEC_TEAMS 15	//MAX_NUM_RACE_TEAMS

CONST_INT GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD						0
CONST_INT GLOBAL_SPEC_BS_QUIT_REQUESTED								1
CONST_INT GLOBAL_SPEC_BS_NEED_TO_PROCESS_LIST_INSTANTLY				2
CONST_INT GLOBAL_SPEC_BS_ACTIVE										3
CONST_INT GLOBAL_SPEC_BS_RUNNING									4
CONST_INT GLOBAL_SPEC_BS_HIDE_LOCAL_PED_IF_ALIVE					5
CONST_INT GLOBAL_SPEC_BS_WANT_TO_LEAVE								6
CONST_INT GLOBAL_SPEC_BS_WATCHING_MP_TV								7
CONST_INT GLOBAL_SPEC_BS_FADES_DISABLED								8
CONST_INT GLOBAL_SPEC_BS_FADE_DISABLE_LOCKED						9
CONST_INT GLOBAL_SPEC_BS_SCRIPT_CAMS_OFF_REQUESTED					10
CONST_INT GLOBAL_SPEC_BS_HIDDEN										11
CONST_INT GLOBAL_SPEC_BS_NEWS_FILTER								12
CONST_INT GLOBAL_SPEC_BS_WAS_LAST_DEACTIVATED_BY_QUIT_SCREEN		13
CONST_INT GLOBAL_SPEC_BS_OVERHEAD_REQUESTED							14
CONST_INT GLOBAL_SPEC_BS_USING_MP_RADIO								15
CONST_INT GLOBAL_SPEC_BS_USING_MP_TV_SEAT							16
CONST_INT GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT						17
CONST_INT GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR						18
CONST_INT GLOBAL_SPEC_BS_FREEMODE_SPECTATOR_CHAT_SETUP				19
CONST_INT GLOBAL_SPEC_BS_TEAM_VS_JOB_ENTIRE_TEAM_DEAD				20
CONST_INT GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT						21
CONST_INT GLOBAL_SPEC_BS_REDO_NEWS_HUD								22
CONST_INT GLOBAL_SPEC_BS_CINEMATIC_SET								23
CONST_INT GLOBAL_SPEC_BS_BAILED_FOR_TRANSITION						24
CONST_INT GLOBAL_SPEC_BS_BLOCK_FIND_NEW_FOCUS						25
CONST_INT GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND						26
CONST_INT GLOBAL_SPEC_BS_DOING_LEGITIMATE_SKYSWOOP_UP				27
CONST_INT GLOBAL_SPEC_BS_SCTV_IN_MOCAP								28
CONST_INT GLOBAL_SPEC_BS_SCTV_TOGGLE_HUD							29
CONST_INT GLOBAL_SPEC_BS_POPULATE_FOR_TOURNAMENT					30
CONST_INT GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT_DELAYED				31

//Used for Bail and Non-Bail flags
CONST_INT GLOBAL_SPEC_BAIL_BS_SCTV_NO_OTHER_PLAYERS					0
CONST_INT GLOBAL_SPEC_BAIL_BS_SCTV_NO_SLOTS_IN_SESSION				1
CONST_INT GLOBAL_SPEC_BAIL_BS_STOP_MPTV_SPECTATE					2
CONST_INT GLOBAL_SPEC_BAIL_BS_RADIO_ON								3
CONST_INT GLOBAL_SPEC_BAIL_BS_RUNNING_CUTSCENE						4
CONST_INT GLOBAL_SPEC_BAIL_BS_RESET_STATE							5

CONST_INT SPEC_PROPERTY_CLEAR_TRANSITION_TIME						3000

// SCTV heli
BOOL bSCTVHeliActive, bShowTextSCTV
BOOL g_bLbReady = TRUE

BOOL g_bUpdateSpectatorPosition = FALSE
BOOL g_bUpdateSpectatorPositionSCTVOverride = TRUE
BOOL g_bKeepHelpTextWhenCleaningUpSpecHUD = FALSE


// =================
// 	Social Club Television (SCTV)
// =================

/// PURPOSE: List of IDs of SCTV modes
ENUM eSCTVModeList
	SCTV_MODE_INIT = 0,		//initialising
	SCTV_MODE_FOLLOW_CAM,	//standard spectator cam used in races, deathmatch and survival
	SCTV_MODE_FREE_CAM,		//modified version the creator cam, allows the player to move the camera around and see whatever they want
	SCTV_MODE_FOLLOW_TO_SESSION	//player is following their target into a new session
ENDENUM

/// PURPOSE: List of IDs of SCTV stages
ENUM eSCTVStageList
	SCTV_STAGE_INIT = 0,	//initialising
	SCTV_STAGE_RUNNING,		//sctv running
	SCTV_STAGE_CLEANUP		//cleanup, sctv about to terminate
ENDENUM

/// PURPOSE: List of IDs of SCTV stage switching, the process of switching between SCTV camera modes
ENUM eSCTVModeSwitchStageList
	SCTV_MODE_SWITCH_STAGE_START,		//preparing to change camera mode
	SCTV_MODE_SWITCH_STAGE_FADE_OUT,	//fade to black
	SCTV_MODE_SWITCH_STAGE_CLEANUP_OLD,	//cleanup old camera mode
	SCTV_MODE_SWITCH_STAGE_SETUP_NEW,	//create the next camera mode
	SCTV_MODE_SWITCH_STAGE_FINISH,		//wait for next camera to be created
	SCTV_MODE_SWITCH_STAGE_COMPLETE		//camera mode switch completed
ENDENUM

/// PURPOSE: State of the local SCTV player's progress through a corona
ENUM SCTV_CORONA_STATE
	SCTV_CORONA_STATE_NULL,					//sctv player and spectated player are not in corona
	SCTV_CORONA_STATE_IN_CORONA,			//sctv player has now followed spectated player into corona
	SCTV_CORONA_STATE_WAIT_FOR_MISSION,		//sctv player is in a corona waiting for a mission to start
	SCTV_CORONA_STATE_WAIT_FOR_PLAYLIST,	//sctv player is in a corona waiting for a playlist to start
	SCTV_CORONA_STATE_CLEANUP				//sctv player is leaving a corona
ENDENUM

/// PURPOSE: Stage of cleaning up the sctv player's fake skyswoop (done to mimic the spectated player's skyswoop, as they will be warping)
ENUM eSPEC_SKYSWOOP_CLEANUP_STAGE
	eSPECSKYSWOOPCLEANUPSTAGE_OFF = 0,			//sctv player not doing any skyswoop
	eSPECSKYSWOOPCLEANUPSTAGE_FADE_OUT,			//sctv player is fading out to cleanup the skyswoop
	eSPECSKYSWOOPCLEANUPSTAGE_WAIT_FOR_TIMER,	//sctv player is looking at a fade out until skyswoop cleaned up
	eSPECSKYSWOOPCLEANUPSTAGE_FADE_IN			//sctv player is fading in as the skyswoop is cleaned up
ENDENUM

CONST_INT SCTV_BIT_LAUNCHED														0
CONST_INT SCTV_BIT_FADE_IN_REQUESTED											1
CONST_INT SCTV_BIT_CLEANUP_MODE_REQUESTED										2
CONST_INT SCTV_BIT_INIT_MODE_REQUESTED											3
CONST_INT SCTV_BIT_READY_FOR_MODE_SWITCH										4
CONST_INT SCTV_BIT_SPECTATING_DIFFERENT_CAMERA									5
CONST_INT SCTV_BIT_UPDATE_HUD													6
CONST_INT SCTV_BIT_JOINED_TRANSITION											7
CONST_INT SCTV_BIT_ALREADY_CLEARED_SCTV_TARGET_THIS_JOB							8
CONST_INT SCTV_BIT_CLEAR_DEACTIVATED_DUE_TO_SKYSWOOP_UP_FLAG					9
CONST_INT SCTV_BIT_SPECTATING_YOU_TICKER_DONE									10
CONST_INT SCTV_BIT_SPECTATING_TEMP_TARGET_AS_FOLLOW_TARGET_UNSUITABLE			11
CONST_INT SCTV_BIT_QUIT_TO_JOIN_NORMAL_SESSION									12
CONST_INT SCTV_BIT_GLOBAL_REFRESH_BUTTONS										13
CONST_INT SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS									14
CONST_INT SCTV_BIT_REFRESH_FILTER												17
CONST_INT SCTV_BIT_TURF_WARS_CAMER_ACTIVE										18

//Used with iStoredCamBitSet
CONST_INT SCTV_SC_BIT_SET_CINEMATIC_CAM_MODE									0
CONST_INT SCTV_SC_BIT_SET_STORED_CAM_MODE										1
CONST_INT SCTV_SC_BIT_STORED_CAM_MODES											2

CONST_INT	SCTV_BAIL_REASON_GENERIC						0
CONST_INT	SCTV_BAIL_REASON_NO_PLAYERS						1
CONST_INT	SCTV_BAIL_REASON_FOLLOW_TO_SESSION_TIMEOUT		2
CONST_INT	SCTV_BAIL_REASON_NO_SUITABLE_TARGET				3


CONST_INT REDO_LIST_DELAY	2500

/// PURPOSE: data struct for holding SCTV information, required for SCTV.sc
STRUCT SCTV_DATA_STRUCT
	eSCTVModeList currentMode = SCTV_MODE_INIT
	eSCTVModeList switchToMode = SCTV_MODE_INIT
	eSCTVStageList stage = SCTV_STAGE_INIT
	eSCTVModeSwitchStageList modeSwitchStage = SCTV_MODE_SWITCH_STAGE_COMPLETE 
	SCTV_CORONA_STATE coronaState
	INT iBitSet = 0
	INT iFreeCamTransitionCatchCounter = 0
	BOOL bBailCalledThisFrame = FALSE
	BOOL bBailTimeIsUp = FALSE
	BOOL bCoronaScreenEffectsActive
	SCRIPT_TIMER timeBail
ENDSTRUCT

/// PURPOSE: struct of globals for spectator, allows other script to send information to all instances of spectator cam
STRUCT MPSpectatorGlobalStruct
	INT iBitSet = 0
	INT iBailBitset = 0
	PED_INDEX pedCurrentFocus
	PED_INDEX pedDesiredFocus
	INT iPreferredSpectatorPlayerID = -1
	
	SCRIPT_TIMER RedoListDelayTimer
	SCRIPT_TIMER SetCamModeDelay
	
	INT iCurrentJobPlayerPositions[NUM_NETWORK_PLAYERS]
	INT iCurrentJobTeamPositions[MAX_NUM_SPEC_TEAMS]
	
	CAM_VIEW_MODE StoredContextCam[NUM_CAM_VIEW_MODE_CONTEXTS]
	INT iStoredCamBitSet
	
	eSPEC_SKYSWOOP_CLEANUP_STAGE skyswoopCleanupStage = eSPECSKYSWOOPCLEANUPSTAGE_OFF
	
	SCTV_DATA_STRUCT SCTVData
	
	INT iBailReason = SCTV_BAIL_REASON_GENERIC
	
	BOOL bFriendlyOtherTeamPlayerAvailableToSpectate = FALSE
	VECTOR vOverrideCoords
ENDSTRUCT
MPSpectatorGlobalStruct MPSpecGlobals


