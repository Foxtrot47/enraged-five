//////////////////////////////////////////////////
///    
///    Constants for Casino Inside Track Minigame
///    

//USING "mp_globals_script_timers.sch"
//USING "mp_globals_new_features_tu.sch"
//USING "mp_globals_common_definitions.sch"
//
//USING "cellphone_globals.sch"
//USING "net_casino_peds_vars.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CONSTANTS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

TWEAK_FLOAT fTWEAK_INSIDE_TRACK_SCALEFORM_CENTREX	0.500	//0.125	//0.100
TWEAK_FLOAT fTWEAK_INSIDE_TRACK_SCALEFORM_CENTREY	0.500	//0.260	//0.250
TWEAK_FLOAT fTWEAK_INSIDE_TRACK_SCALEFORM_WIDTH		0.999	//0.235	//0.450
TWEAK_FLOAT fTWEAK_INSIDE_TRACK_SCALEFORM_HEIGHT	0.999	//0.450	//0.500

TWEAK_INT iTWEAK_INSIDE_TRACK_SCALEFORM_ALPHA		255

#IF IS_DEBUG_BUILD
TWEAK_FLOAT fTWEAK_INSIDE_TRACK_CONSOLE_CENTREX		0.5
TWEAK_FLOAT fTWEAK_INSIDE_TRACK_CONSOLE_CENTREY		0.5
TWEAK_FLOAT fTWEAK_INSIDE_TRACK_CONSOLE_SCALE		0.5
#ENDIF

CONST_INT iCONST_INSIDE_TRACK_COOLDOWN				10000
CONST_INT iCONST_INSIDE_TRACK_FREE_EXPIRY_TIMER		11000

CONST_INT iCONST_INSIDE_TRACK_LANE_COUNT			6
CONST_INT ciINSIDE_TRACK_MINIGAME_MAX_LOCATES		16

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ ENUMS ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

ENUM INSIDE_TRACK_GAME_STATE
	INSIDE_TRACK_GAME_STATE_INVALID = -1,
	// Playing Minigame States
	INSIDE_TRACK_GAME_STATE_SETUP_NEW_GAME = 0,
	INSIDE_TRACK_GAME_STATE_RACE_COUNTDOWN,
	INSIDE_TRACK_GAME_STATE_RANDOMISE_REWARD,
	INSIDE_TRACK_GAME_STATE_START_THE_RACE,
	INSIDE_TRACK_GAME_STATE_RACE_IN_PROGRESS,
	INSIDE_TRACK_GAME_STATE_REWARD,
	INSIDE_TRACK_GAME_STATE_POST_RACE,
	INSIDE_TRACK_GAME_STATE_RACE_RESULTS,
	INSIDE_TRACK_GAME_STATE_CLEANUP,
	
	INSIDE_TRACK_GAME_STATE_COUNT
ENDENUM

ENUM INSIDE_TRACK_PLAYER_STATE
	INSIDE_TRACK_PLAYER_STATE_INVALID = -1,
	// Playing Minigame States
	INSIDE_TRACK_PLAYER_STATE_LOCATE_CHECK = 0,
	INSIDE_TRACK_PLAYER_STATE_DETERMINE_PLAYER,
	INSIDE_TRACK_PLAYER_STATE_SHOW_DISCLOSURE,		//_DISCLOSURE
	INSIDE_TRACK_PLAYER_STATE_TASK_GO_TO_COORDS,
	INSIDE_TRACK_PLAYER_STATE_INTRO_ANIMATION,
	INSIDE_TRACK_PLAYER_STATE_SELECT_TO_PLAY,
	INSIDE_TRACK_PLAYER_STATE_SELECT_GAME_MODE,
	INSIDE_TRACK_PLAYER_STATE_SELECT_BET,
	INSIDE_TRACK_PLAYER_STATE_PAYMENT_FEE,
	INSIDE_TRACK_PLAYER_STATE_WATCH_THE_RACE,
	INSIDE_TRACK_PLAYER_STATE_RESULTS,
	INSIDE_TRACK_PLAYER_STATE_QUIT_PLAY,
	INSIDE_TRACK_PLAYER_STATE_EXIT_ANIMATION,
	INSIDE_TRACK_PLAYER_STATE_EXITING_ANIMATION,
	INSIDE_TRACK_PLAYER_STATE_CLEANUP,
	
	INSIDE_TRACK_PLAYER_STATE_COUNT
ENDENUM

ENUM INSIDE_TRACK_HORSE_TYPE
	 INSIDE_TRACK_HORSE_TYPE_INVALID = 0
	,INSIDE_TRACK_HORSE_ATetheredEnd = 1
	,INSIDE_TRACK_HORSE_BadEgg
	,INSIDE_TRACK_HORSE_BananaHammock
	,INSIDE_TRACK_HORSE_BetterThanNothing
	,INSIDE_TRACK_HORSE_BlackRockRooster
	,INSIDE_TRACK_HORSE_BleetMeBaby
	,INSIDE_TRACK_HORSE_BlueDream
	,INSIDE_TRACK_HORSE_BorrowedSorrow
	,INSIDE_TRACK_HORSE_BouncyBlessed
	,INSIDE_TRACK_HORSE_CancelledCheck
	,INSIDE_TRACK_HORSE_CantBeWronger
	,INSIDE_TRACK_HORSE_ClapbackCharlie
	,INSIDE_TRACK_HORSE_ConstantBrag
	,INSIDE_TRACK_HORSE_CountryStuck
	,INSIDE_TRACK_HORSE_CrackersandPlease
	,INSIDE_TRACK_HORSE_CreepyDentist
	,INSIDE_TRACK_HORSE_CrockJanley
	,INSIDE_TRACK_HORSE_DancinPole
	,INSIDE_TRACK_HORSE_DancinShoes
	,INSIDE_TRACK_HORSE_DarlingRicki
	,INSIDE_TRACK_HORSE_DeadFam
	,INSIDE_TRACK_HORSE_DeadHeatHattie
	,INSIDE_TRACK_HORSE_DexieRunner
	,INSIDE_TRACK_HORSE_DivorcedDoctor
	,INSIDE_TRACK_HORSE_DoozyFloozy
	,INSIDE_TRACK_HORSE_DowntownRenown
	,INSIDE_TRACK_HORSE_DrDeezReins
	,INSIDE_TRACK_HORSE_DreamShatterer
	,INSIDE_TRACK_HORSE_DroneWarning
	,INSIDE_TRACK_HORSE_DrunkenBrandee
	,INSIDE_TRACK_HORSE_DurbanPoison
	,INSIDE_TRACK_HORSE_FeedTheTrolls
	,INSIDE_TRACK_HORSE_FireHazards
	,INSIDE_TRACK_HORSE_FlippedWig
	,INSIDE_TRACK_HORSE_FriendlyFire
	,INSIDE_TRACK_HORSE_GettingHaughty
	,INSIDE_TRACK_HORSE_GhostDank
	,INSIDE_TRACK_HORSE_GlassorTina
//	,INSIDE_TRACK_HORSE_GoingforGlue
	,INSIDE_TRACK_HORSE_LosSantosSavior
	,INSIDE_TRACK_HORSE_HardTimeDone
	,INSIDE_TRACK_HORSE_HellforWeather
	,INSIDE_TRACK_HORSE_HennigansSteed
	,INSIDE_TRACK_HORSE_HippieCrack
	,INSIDE_TRACK_HORSE_HotBothered
	,INSIDE_TRACK_HORSE_InvadeGrenade
	,INSIDE_TRACK_HORSE_ItsaTrap
	,INSIDE_TRACK_HORSE_KraffRunning
	,INSIDE_TRACK_HORSE_LeadisOut
	,INSIDE_TRACK_HORSE_LitAsTruck
	,INSIDE_TRACK_HORSE_LonelyStepbrother
	,INSIDE_TRACK_HORSE_LoversSpeed
	,INSIDE_TRACK_HORSE_MeaslesSmeezles
	,INSIDE_TRACK_HORSE_MicroAggression
	,INSIDE_TRACK_HORSE_MinimumWager
	,INSIDE_TRACK_HORSE_MissMaryJohn
	,INSIDE_TRACK_HORSE_MissTriggered
	,INSIDE_TRACK_HORSE_MisterRedacted
	,INSIDE_TRACK_HORSE_MisterScissors
	,INSIDE_TRACK_HORSE_MoneytoBurn
	,INSIDE_TRACK_HORSE_MoonRocks
	,INSIDE_TRACK_HORSE_MrWorthwhile
	,INSIDE_TRACK_HORSE_MudDragon
	,INSIDE_TRACK_HORSE_NighttimeMare
	,INSIDE_TRACK_HORSE_NorthernLights
	,INSIDE_TRACK_HORSE_NunsOrders
	,INSIDE_TRACK_HORSE_OlSkag
	,INSIDE_TRACK_HORSE_OldIllWill
	,INSIDE_TRACK_HORSE_OmensandIce
	,INSIDE_TRACK_HORSE_Pedestrian
	,INSIDE_TRACK_HORSE_PrettyasaPistol
	,INSIDE_TRACK_HORSE_QuestionableDignity
	,INSIDE_TRACK_HORSE_ReachAroundTown
	,INSIDE_TRACK_HORSE_Robocall
	,INSIDE_TRACK_HORSE_SaltNSauce
	,INSIDE_TRACK_HORSE_SaltyandWoke
	,INSIDE_TRACK_HORSE_ScrawnyNag
	,INSIDE_TRACK_HORSE_SirScrambled
	,INSIDE_TRACK_HORSE_Sizzurp
	,INSIDE_TRACK_HORSE_SnatchedYourMama
	,INSIDE_TRACK_HORSE_SocialMediaWarrior
	,INSIDE_TRACK_HORSE_SquaretoGo
	,INSIDE_TRACK_HORSE_StudyBuddy
	,INSIDE_TRACK_HORSE_StupidMoney
	,INSIDE_TRACK_HORSE_SumptinSaucy
	,INSIDE_TRACK_HORSE_SweetReleaf
	,INSIDE_TRACK_HORSE_TaxthePoor
	,INSIDE_TRACK_HORSE_TeaAcheSea
	,INSIDE_TRACK_HORSE_Tenpenny
	,INSIDE_TRACK_HORSE_ThereSheBlows
	,INSIDE_TRACK_HORSE_ThrowingShady
	,INSIDE_TRACK_HORSE_ThunderSkunk
	,INSIDE_TRACK_HORSE_TotalBelter
	,INSIDE_TRACK_HORSE_TurntMood
	,INSIDE_TRACK_HORSE_UptownRider
	,INSIDE_TRACK_HORSE_WageofConsent
	,INSIDE_TRACK_HORSE_WeeScunner
	,INSIDE_TRACK_HORSE_WorthaKingdom
	,INSIDE_TRACK_HORSE_YayYoLetsGo
	,INSIDE_TRACK_HORSE_YellowSunshine
	
	,INSIDE_TRACK_HORSE_TYPE_COUNT
ENDENUM

ENUM INSIDE_TRACK_DRAW_STATE
	 INSIDE_TRACK_DRAW_STATE_LINK_RT = 0
	,INSIDE_TRACK_DRAW_STATE_INIT_WALL
	,INSIDE_TRACK_DRAW_STATE_UPDATE
	,INSIDE_TRACK_DRAW_STATE_DRAW
	,INSIDE_TRACK_DRAW_STATE_CLEANUP
ENDENUM

ENUM INSIDE_TRACK_SCALEFORM_WALL_ID
	 INSIDE_TRACK_SCALEFORM_WALL_INVALID				= -1	// 
	,INSIDE_TRACK_SCALEFORM_WALL_COUNTDOWN	 			= 0		// Countdown Screen (shows the time remaining before the race stars and an overview of all horses in the race).
	,INSIDE_TRACK_SCALEFORM_WALL_HORSE_DETAILS			= 1		// Horse Details Screen (shows details about a specific horse and who has bet on it).
	,INSIDE_TRACK_SCALEFORM_WALL_RACE					= 2		// Race Screen (where the actual race animation is shown).
	,INSIDE_TRACK_SCALEFORM_WALL_POST_RACE				= 3		// Post Race Screen (shows the winning horse).
	,INSIDE_TRACK_SCALEFORM_WALL_RESULTS				= 4		// Results Screen (shows the top three horses and winnings / losses for all players).
	,INSIDE_TRACK_SCALEFORM_WALL_ERROR					= 5		// Error Screen (shows a generic error message).
ENDENUM

ENUM INSIDE_TRACK_SCALEFORM_CONSOLE_ID
	 INSIDE_TRACK_SCALEFORM_CONSOLE_INVALID				= -1	// 
	,INSIDE_TRACK_SCALEFORM_CONSOLE_MAIN		 		= 0		// Main Screen (offers the player a choice between the main race and a single race. Also shows a countdown until the next main race).
	,INSIDE_TRACK_SCALEFORM_CONSOLE_SINGLE_SELECTION	= 1		// Single Race Selection Screen (allows the player to pick a horse to bet on)
	,INSIDE_TRACK_SCALEFORM_CONSOLE_MAIN_SELECTION		= 2		// Main Race Selection Screen (allows the player to pick a horse to bet on)
	,INSIDE_TRACK_SCALEFORM_CONSOLE_SINGLE_BETTING		= 3		// Single Betting Screen (allows the player to bet on a horse)
	,INSIDE_TRACK_SCALEFORM_CONSOLE_MAIN_BETTING		= 4		// Main Betting Screen (allows the player to bet on a horse)
	,INSIDE_TRACK_SCALEFORM_CONSOLE_RACE				= 5		// Race Screen (where the actual race animation is shown).
	,INSIDE_TRACK_SCALEFORM_CONSOLE_PHOTO_FINISH		= 6		// Photo Finish (shows the winning horse crossing the line)
	,INSIDE_TRACK_SCALEFORM_CONSOLE_RESULTS				= 7		// Results Screen (shows the top three horses and the player's wins or losses).
	,INSIDE_TRACK_SCALEFORM_CONSOLE_ERROR				= 8		// Error Screen (shows a generic error message).
	,INSIDE_TRACK_SCALEFORM_CONSOLE_RULES				= 9		// Rules Screen
	
	,INSIDE_TRACK_SCALEFORM_CONSOLE_COUNT
ENDENUM

ENUM INSIDE_TRACK_MONITOR_TEXTURE_ID
	 INSIDE_TRACK_MONITOR_BETTING_INVALID				= -1
	,INSIDE_TRACK_MONITOR_BETTING						= 0
	,INSIDE_TRACK_MONITOR_BETTING_GENERIC_ORANGE
	,INSIDE_TRACK_MONITOR_BETTING_GENERIC_ORANGE_unavailable
	,INSIDE_TRACK_MONITOR_BETTING_GENERIC_PURPLE
	,INSIDE_TRACK_MONITOR_BETTING_GENERIC_PURPLE_unavailable
	,INSIDE_TRACK_MONITOR_BETTING_SINGLE
	,INSIDE_TRACK_MONITOR_BETTING_MAIN
ENDENUM

ENUM INSIDE_TRACK_SCALEFORM_CONSOLE_BUTTONS
	 eINVALID_BUTTON									= -1
	,eMAIN_EVENT_BUTTON									= 0
	,eSINGLE_EVENT_BUTTON								= 1
	,eHORSE_1_BUTTON									= 2
	,eHORSE_2_BUTTON									= 3
	,eHORSE_3_BUTTON									= 4
	,eHORSE_4_BUTTON									= 5
	,eHORSE_5_BUTTON									= 6
	,eHORSE_6_BUTTON									= 7
	,eINCREASE_BET_BUTTON								= 8
	,eDECREASE_BET_BUTTON								= 9
	,ePLACE_BET_BUTTON									= 10
	,eCHANGE_BET_BUTTON									= 11
	,eCANCEL_BUTTON										= 12
	,ePLAY_AGAIN_BUTTON									= 13
	,eVIEW_MAIN_BET_BUTTON								= 14
	,eSHOW_RULES_BUTTON									= 15
ENDENUM

ENUM INSIDE_TRACK_ANNOUNCER
	 INSIDE_TRACK_ANNOUNCER_FIRST_PLACE_NAME = 0
	,INSIDE_TRACK_ANNOUNCER_CLOSE_FINISH_NAME
	,INSIDE_TRACK_ANNOUNCER_LEAD_NAME
	,INSIDE_TRACK_ANNOUNCER_TITLE_SCREEN		// You welcome the audience to the track.
	,INSIDE_TRACK_ANNOUNCER_SELECTION			// You ask the customer which bet they'd like to play.
	,INSIDE_TRACK_ANNOUNCER_PLACE_BETS			// You prompt the customer to choose their horse and place their bets.
	,INSIDE_TRACK_ANNOUNCER_ADVERTISEMENT		// You advertise the horse racing track as an announcement.
	,INSIDE_TRACK_ANNOUNCER_COUNTDOWN			// You warn your customers about the countdown to the main event.
	,INSIDE_TRACK_ANNOUNCER_5MIN				// You give your customers a five minute heads-up.
	,INSIDE_TRACK_ANNOUNCER_4MIN				// You give your customers a four minute heads-up.
	,INSIDE_TRACK_ANNOUNCER_3MIN				// You give your customers a three minute heads-up.
	,INSIDE_TRACK_ANNOUNCER_2MIN				// You give your customers a two minute heads-up.
	,INSIDE_TRACK_ANNOUNCER_1MIN				// You give your customers a one minute heads-up.
//	,INSIDE_TRACK_ANNOUNCER_NO_MORE_BETS		// You are no longer accepting bets and announce just that to the track.
	,INSIDE_TRACK_ANNOUNCER_EVENT_STARTING		// You announce to the track that the race is starting.
	,INSIDE_TRACK_ANNOUNCER_RACE_START			// The race has started and you announce that the horses have taken off.
	,INSIDE_TRACK_ANNOUNCER_HALFWAY				// You announce that the race is halfway through.
	,INSIDE_TRACK_ANNOUNCER_FINISH_NEAR			// You announce that the race is about to finish.
	,INSIDE_TRACK_ANNOUNCER_GENERAL_EXCITEMENT	// You're excited about the race and amp up the crowd.
	,INSIDE_TRACK_ANNOUNCER_CLOSE				// The horses are close together for first place and you announce it to the audience.
	,INSIDE_TRACK_ANNOUNCER_CHANGES				// The lead horse has been changing and you announce just that to the audience.
	,INSIDE_TRACK_ANNOUNCER_LEADER
	,INSIDE_TRACK_ANNOUNCER_LEADER_AHEAD
	,INSIDE_TRACK_ANNOUNCER_NEW_LEADER
	,INSIDE_TRACK_ANNOUNCER_FIRST_PLACE
	,INSIDE_TRACK_ANNOUNCER_SECOND_PLACE
	,INSIDE_TRACK_ANNOUNCER_THIRD_PLACE
	,INSIDE_TRACK_ANNOUNCER_FOURTH_PLACE
	,INSIDE_TRACK_ANNOUNCER_FIFTH_PLACE
	,INSIDE_TRACK_ANNOUNCER_SIXTH_PLACE
	,INSIDE_TRACK_ANNOUNCER_LAST_PLACE
	,INSIDE_TRACK_ANNOUNCER_FINISH_FIRST_PLACE
	,INSIDE_TRACK_ANNOUNCER_FINISH_SECOND_PLACE
	,INSIDE_TRACK_ANNOUNCER_FINISH_THIRD_PLACE
	,INSIDE_TRACK_ANNOUNCER_CLOSE_FINISH
	
	,INSIDE_TRACK_ANNOUNCER_COUNT
ENDENUM

ENUM INSIDE_TRACK_CAM_VIEW
	 INSIDE_TRACK_CAM_VIEW_SCREEN = 0
	,INSIDE_TRACK_CAM_VIEW_FIRST_PERSON
	,INSIDE_TRACK_CAM_VIEW_THIRD_PERSON
	
	,INSIDE_TRACK_CAM_VIEW_COUNT
ENDENUM

ENUM INSIDE_TRACK_TIMER_TYPE
	 INSIDE_TRACK_TIMER_DeterminePlayer = 0
	,INSIDE_TRACK_TIMER_PlayCooldown
	,INSIDE_TRACK_TIMER_FailedPlayAttempt
	,INSIDE_TRACK_TIMER_TriggerRaceSafety
	,INSIDE_TRACK_TIMER_IntroGoToCoordsSafety
	,INSIDE_TRACK_TIMER_WallConversation
	,INSIDE_TRACK_TIMER_ConsoleConversation
	,INSIDE_TRACK_TIMER_CancelPressed
	
	,INSIDE_TRACK_TIMER_COUNT
ENDENUM

ENUM INSIDE_TRACK_RACE_TIMER_TYPE
	 INSIDE_TRACK_RACE_TIMER_INVALID = -1
	
	,INSIDE_TRACK_RACE_TIMER_RaceInsideTrack = 0
	,INSIDE_TRACK_RACE_TIMER_RaceUpdatePositions
	,INSIDE_TRACK_RACE_TIMER_RaceToStart
	,INSIDE_TRACK_RACE_TIMER_ResultsScreen
	,INSIDE_TRACK_RACE_TIMER_RaceCountdown
	
	,INSIDE_TRACK_RACE_TIMER_COUNT
ENDENUM

ENUM eINSIDE_TRACK_END_REASON_ENUM
	 eEnd_Reason_null = 0
	,eEnd_Reason_Quit
	,eEnd_Reason_WonTooMuch
	,eEnd_Reason_LostTooMuch
	,eEnd_Reason_TimeSpentPlaying
	,eEnd_Reason_LowChips
	,eEnd_Reason_MembershipChange
	,eEnd_Reason_PlayerInCorona
	,eEnd_Reason_OnLeaderboard
	,eEnd_Reason_ResultsScreenDisplaying
	,eEnd_Reason_SwitchInProgress
	,eEnd_Reason_IsSignedOnline
	,eEnd_Reason_RemovedForMission
ENDENUM

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ STRUCTS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

STRUCT INSIDE_TRACK_RACE_DATA
	BOOL bRaceIsSet = FALSE
	
	INT iBS
	INSIDE_TRACK_GAME_STATE eGameState = INSIDE_TRACK_GAME_STATE_SETUP_NEW_GAME
	INSIDE_TRACK_HORSE_TYPE eHorsesRacing[iCONST_INSIDE_TRACK_LANE_COUNT]
	INT iHorseTimes[iCONST_INSIDE_TRACK_LANE_COUNT]
	INT iWinningSegment = -1
	INT iSeed = 0, iDuration = 0
	INT iPositionList = -1
	INT iMyHorseTemp = 0
	
	structTimer stTimer[INSIDE_TRACK_RACE_TIMER_COUNT]
ENDSTRUCT

STRUCT INSIDE_TRACK_BET_DATA
	TEXT_LABEL_63 tl63Gamertag
	INT iSelection			= -1		// INT iHorse = 0
	INT iStake				= 0			// INT iBet = -1
ENDSTRUCT

STRUCT INSIDE_TRACK_TELEMETRY_DATA
	INT iMatchTypeBS = 0
//	INT iTableID = -1
	eINSIDE_TRACK_END_REASON_ENUM eEndReason = eEnd_Reason_null
	INT iChipsStart = 0
//	INT iDurationGameTime = 0
	INT iBetTotalAmount = 0
	INT iWinCount = 0
	INT iLoseCount = 0
	INT iSingleEventCount = 0
	INT iMainEventCount = 0
ENDSTRUCT

ENUM GENERIC_TRANSACTION_STATE
	TRANSACTION_STATE_DEFAULT = 0,
	TRANSACTION_STATE_PENDING,
	TRANSACTION_STATE_SUCCESS,
	TRANSACTION_STATE_FAILED
ENDENUM

STRUCT INSIDE_TRACK_MINIGAME_DATA
	INT iBS[3]
	INT iExitTableContext = -1  // NEW_CONTEXT_INTENTION
	INT iSceneID = -1
	INT iLocateID = -1
	INT iMembershipID = 0
	
	INT iLeftX
	INT iLeftY
	INT iRightX
	INT iRightY
	
	GENERIC_TRANSACTION_STATE eTransactionState
	
	INSIDE_TRACK_TELEMETRY_DATA sTelemetryData
	
	INSIDE_TRACK_BET_DATA sMainBetData, sSingleBetData
	
//	BOOL bFMEventParticipant = FALSE
	BOOL bForceCleanupMinigame = FALSE
	BOOL bCleanedupMinigame = FALSE
	
	TEXT_LABEL_15 tl15CountdownTime
	
	TEXT_LABEL_63 tl63AnimDict
	TEXT_LABEL_31 tl31AnimClip[6]
	#IF IS_DEBUG_BUILD
	INT iAnimClipGameTimer[6]
	#ENDIF
	
	INSIDE_TRACK_RACE_DATA rdSingleEvent
	INT iBSTriggerConsoleDialogue[2]
	INT iBSTriggerWallDialogue[2]
	INT iResultsDialogue
	
	SCRIPT_TIMER stTimer[INSIDE_TRACK_TIMER_COUNT]
	
	INSIDE_TRACK_PLAYER_STATE ePlayerState = INSIDE_TRACK_PLAYER_STATE_LOCATE_CHECK
	
	INSIDE_TRACK_DRAW_STATE eWallDrawState = INSIDE_TRACK_DRAW_STATE_CLEANUP
	INSIDE_TRACK_SCALEFORM_WALL_ID eWallScreenID = INSIDE_TRACK_SCALEFORM_WALL_INVALID
	
	INSIDE_TRACK_DRAW_STATE eConsoleDrawState = INSIDE_TRACK_DRAW_STATE_CLEANUP
	INSIDE_TRACK_SCALEFORM_CONSOLE_ID eConsoleScreenID = INSIDE_TRACK_SCALEFORM_CONSOLE_INVALID
	
	INT iMonitorCount = 0
	INSIDE_TRACK_DRAW_STATE eMonitorsDrawState[ciINSIDE_TRACK_MINIGAME_MAX_LOCATES]
	INSIDE_TRACK_MONITOR_TEXTURE_ID eMonitorsTextureID[ciINSIDE_TRACK_MINIGAME_MAX_LOCATES]
	
	CAMERA_INDEX ciScreenCamera
	INSIDE_TRACK_CAM_VIEW eCurrentCameraView = INSIDE_TRACK_CAM_VIEW_SCREEN
	CAM_VIEW_MODE eInitialView = CAM_VIEW_MODE_THIRD_PERSON
	INSIDE_TRACK_CAM_VIEW eLastGameplayCameraView = INSIDE_TRACK_CAM_VIEW_THIRD_PERSON
	
	INT iWallAmbSoundID = -1, iWallRaceSoundID = -1
	INT iConsoleRaceSoundID = -1
	
	SCALEFORM_INDEX sfButton
	
	INT iBSWallDialogue[2], iBSConsoleDialogue[2]
	TEXT_LABEL_63 tl63WallContext, tl63ConsoleContext
	PED_INDEX piAnnouncer
	
	SCRIPT_TIMER holdButtonDelayTimer
	
	#IF IS_DEBUG_BUILD
	INT iDebugWallTimeToExpire = 0
	INT iDebugConsoleTimeToExpire = 0
	INT iDebugLocateIDPrint = -1999
	#ENDIF
	
	INT iWallPlayersBetCount = 0
	INT iWallRenderTargetID = -1, iMonitorsRenderTargetID[ciINSIDE_TRACK_MINIGAME_MAX_LOCATES]
	SCALEFORM_INDEX sfInsideTrackWall, sfInsideTrackConsole
	SCALEFORM_RETURN_INDEX currentSelectionReturnIndex
	SCALEFORM_RETURN_INDEX wallRaceCompleteReturnIndex, consoleRaceCompleteReturnIndex
	SCALEFORM_RETURN_INDEX wallRacePositionsReturnIndex, consoleRacePositionsReturnIndex
ENDSTRUCT
