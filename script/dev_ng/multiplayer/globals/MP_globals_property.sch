USING "rage_builtins.sch"
USING "types.sch"
USING "commands_clock.sch"

INT currentDayShowering
MONTH_OF_YEAR currentMonthShowering
INT currentYearShowering

INT totalTimeShowering = 0
BOOL bEarnedEnoughRPSinging

BOOL g_bMadeCarInvunerableOnEntry

STRUCT BG_SCRIPT_QUERY_PROPERTY_STATES
	INT iINT_localStage
	//INT iINT_processingID //set at different stages of script to highlight potential area of issue
ENDSTRUCT
BG_SCRIPT_QUERY_PROPERTY_STATES bg_ScriptPropertyGlobalStates
