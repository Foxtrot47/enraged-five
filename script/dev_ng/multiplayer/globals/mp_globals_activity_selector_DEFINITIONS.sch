USING "rage_builtins.sch"
USING "types.sch"										// For TIME_DATATYPE

USING "commands_network.sch"							// For Gamer_Handle

USING "MP_Globals_COMMON_CONSTS.sch"					// For NUM_NETWORK_PLAYERS
USING "mp_globals_missions_at_coords_definitions.sch"	// For Missions At Coords Globals
USING "mp_globals_missions_shared_definitions.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_activity_selector.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains constant, enum, and struct definitions for all MP Activity Selector routines.
//							NB. Should not contain instantiated variables. 
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************



// KGM 3/8/14: Moved from mp_globals_common_definitions.sch so it can be accessed below and avoid cyclic headers
CONST_INT MAX_HEIST_PLANNING_MISSIONS		7


// KGM 29/10/14: The Heist Flow Order - used for Heist Progression
CONST_INT	UNKNOWN_HEIST_FLOW_ORDER				-1
CONST_INT	HEIST_FLOW_ORDER_FLEECA_TUTORIAL		0
CONST_INT	HEIST_FLOW_ORDER_PRISON_BREAK			1
CONST_INT	HEIST_FLOW_ORDER_HUMANE_LABS			2
CONST_INT	HEIST_FLOW_ORDER_NARCOTICS				3
CONST_INT	HEIST_FLOW_ORDER_PACIFIC_STANDARD		4
CONST_INT	MAX_HEIST_FLOW_ORDER					5





// ===========================================================================================================
//      Activity Selector Consts
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//      General Consts
// -----------------------------------------------------------------------------------------------------------

// Group Scheduling Regular Maintenance Delay
CONST_INT			ACTIVITY_SELECTOR_GROUP_SCHEDULING_DELAY_msec		1000

// Bitflags all clear
CONST_INT			ALL_ACTIVITY_SELECTOR_BITFLAGS_CLEAR				0

// ENUM to represent an FM Heist-specific subset of the CharacterList
// NOTE: Used only for loop maintenance, most uses of the contact will still use the enumCharacterList and there are conversion functions to swap between the two
ENUM g_eFMHeistContactIDs
	FM_HEIST_GERALD,
	FM_HEIST_LAMAR,
	FM_HEIST_LESTER,
	FM_HEIST_MARTIN,
	FM_HEIST_RON,
	FM_HEIST_SIMEON,
	FM_HEIST_TREVOR,
	FM_HEIST_AGENT14,
	FM_HEIST_CEOASSIST,
	FM_HEIST_HAO,
	FM_HEIST_OSCAR,
	FM_HEIST_LAZLOW,
	FM_HEIST_MERRYWEATHER,
	FM_HEIST_BRUCIE,
	FM_HEIST_MICHAEL,
	FM_HEIST_FRANKLIN,
	FM_HEIST_ENG_DAVE,
	FM_HEIST_TONY,
	FM_HEIST_JIMMY,
	FM_HEIST_TRACEY,
	FM_HEIST_WADE,
	FM_HEIST_DAVE,
	FM_HEIST_RICKIE,
	FM_HEIST_CHEF,
	FM_HEIST_HUNTER,
	FM_HEIST_CRIS,
	FM_HEIST_MARNIE,
	FM_HEIST_MAUDE,
	FM_HEIST_ASHLEY,
	FM_HEIST_OMEGA,
	FM_HEIST_BBPAIGE,
	FM_HEIST_RAY_LAVOY,
	FM_HEIST_CASINO_MANAGER,
	FM_HEIST_YACHT_CAPTAIN,
	FM_HEIST_MIGUEL_MADRAZO,
	FM_HEIST_PAVEL,
	FM_HEIST_MOODYMANN,
	FM_HEIST_SESSANTA,
	FM_HEIST_KDJ, 
	FM_HEIST_FRANKLIN_AND_IMANI, 
	FM_HEIST_ULP,
	FM_HEIST_DAX,
	
	// Leave these at the bottom in this order
	MAX_FM_HEIST_CONTACTS,
	NO_FM_HEIST_CONTACT
ENDENUM


// -----------------------------------------------------------------------------------------------------------
//      Local Activity Selector Group Update Consts
//		NOTE: There are a number of dynamic server-based groups and these get updated on a schedule
//				I also need to update local groups - like the Contact Missions, so I'll tag these on
//				at the end of the server-based group updates
// -----------------------------------------------------------------------------------------------------------

CONST_INT	LOCAL_GROUP_CONTACT_MISSIONS_UPDATE_SCHEDULER		MAX_PENDING_ACTIVITIES_GROUPS
CONST_INT	MAX_LOCAL_GROUP_UPDATE_SCHEDULER					(LOCAL_GROUP_CONTACT_MISSIONS_UPDATE_SCHEDULER + 1)


// -----------------------------------------------------------------------------------------------------------
//      Contact Mission Consts
// -----------------------------------------------------------------------------------------------------------

// Initial per-Contact Delay before a Contact calls anybody about a Contact Mission
CONST_INT			MP_CONTACT_MISSION_INITIAL_CONTACT_DELAY_msec_OLD		(60000 * 5)		// Now Tunable: g_sMPTunables.ContactMissionServerDelay_secs

// General per-Contact Delay before a Contact offers another Contact Mission to any player
CONST_INT			MP_CONTACT_MISSION_CONTACT_DELAY_msec_OLD				(60000 * 10)	// Now Tunable: g_sMPTunables.ContactMissionRegularDelay_secs

// Initial per-Player Delay before the player will be offered a Contact Mission
CONST_INT			MP_CONTACT_MISSION_INITIAL_PLAYER_DELAY_msec_OLD		(60000 * 3)		// Now Tunable: g_sMPTunables.ContactMissionInitialDelay_secs

// General per-Player Delay before a Player will be offered another Contact Mission
CONST_INT			MP_CONTACT_MISSION_PLAYER_DELAY_msec_OLD				(60000 * 5)		// NOT USED

// Short per-Player Delay before a Player will be offered another Contact Mission
CONST_INT			MP_CONTACT_MISSION_SHORT_PLAYER_DELAY_msec			60000

// The Maximum number of local Contact Missions
CONST_INT			MAX_CONTACT_MISSIONS								500		// KGM 15/9/15 [BUG 2034824]: A general increase in the number of missions at once on NG

// Illegal local Contact Mission entry
CONST_INT			ILLEGAL_CONTACT_MISSION_SLOT						-1

// The Maximum number of played Contact Mission ContentID Hashes stored as stats
// NOTE: If thre are more CMs than this then the CM Minimum Rank will move on without the player playing all content at that CM Rank
CONST_INT			MAX_CM_CONTENTIDS_PLAYED							50

// Contact Mission Rank Check scheduling
CONST_INT			CONTACT_MISSION_RANK_CHECK_STANDARD_DELAY_msec		(60000 * 5)
CONST_INT			CONTACT_MISSION_RANK_CHECK_QUICK_RETEST_msec		30000

// The maximum number of Contact Missions from a Contact that a player can have active Invites for at the same time
CONST_INT			MAX_CONTACT_MISSION_INVITES_AT_ONCE					3
CONST_INT			MIN_CM_RANK_ALLOWING_1_CM_MAX						1
CONST_INT			MIN_CM_RANK_ALLOWING_2_CM_MAX						6
CONST_INT			MIN_CM_RANK_ALLOWING_3_CM_MAX						11

// A value to store in the 'frame' variable to indicate the mission is still in progress
CONST_INT			MISSION_IN_PROGRESS_FRAME							-1

// The timeout frame when CMs from the most recent contact will be allowed again (assume 30fps)
CONST_INT			MOST_RECENT_CONTACT_TIMEOUT_frame					(30 * 60 * 5)	// approx 5 mins

// ---------------------------------------------------------------------
//      Adversary Invite Weighting
// 		These now trigger / override contact mission invites
// ---------------------------------------------------------------------

CONST_INT SELECTOR_NEXT_INVITE_IS_ADVERSARY				0
CONST_INT SELECTOR_NEXT_INVITE_IS_CONTACT_MISSION		1

// Update this as the enum below increases
CONST_INT ADVERSARY_PROMO_ARRAY_SIZE 	2

ENUM g_eContactMissionAdversaryTypes
	CM_ADVERSARY_TYPE_NULL = -1,
	CM_ADVERSARY_TYPE_COME_OUT_TO_PLAY,			// 0
	CM_ADVERSARY_TYPE_SIEGE_MENTALITY,			// 1
	CM_ADVERSARY_TYPE_HASTA_LA_VISTA,			// 2
	CM_ADVERSARY_TYPE_HUNTING_PACK,				// 3
	CM_ADVERSARY_TYPE_CROSS_THE_LINE,			// 4
	CM_ADVERSARY_TYPE_SHEPHERD,					// 5
	CM_ADVERSARY_TYPE_RELAY,					// 6
	CM_ADVERSARY_TYPE_SPEED_RACE,				// 7
	CM_ADVERSARY_TYPE_EBC,						// 8
	CM_ADVERSARY_TYPE_HUNT_DARK,				// 9
	CM_ADVERSARY_TYPE_RUNNING_BACK,				// 10
	CM_ADVERSARY_TYPE_EXTRACTION,				// 11
	CM_ADVERSARY_TYPE_BEAST_V_SLASHER,			// 12
	CM_ADVERSARY_TYPE_DROP_ZONE,				// 13
	CM_ADVERSARY_TYPE_BE_MY_VALENTINE,			// 14
	CM_ADVERSARY_TYPE_DAVID_AND_GOLIATH,		// 15
	CM_ADVERSARY_TYPE_SUMO,						// 16
	CM_ADVERSARY_TYPE_RUGBY,					// 17
	CM_ADVERSARY_TYPE_IN_AND_OUT,				// 18
	CM_ADVERSARY_TYPE_POWER_PLAY,				// 19
	CM_ADVERSARY_TYPE_TRADE_PLACES,				// 20
	CM_ADVERSARY_TYPE_ENTOURAGE,				// 21
	CM_ADVERSARY_TYPE_SUMO_ZONE,				// 22
	CM_ADVERSARY_TYPE_DEADLINE,					// 23
	CM_ADVERSARY_TYPE_LOSTVDAMNED,				// 24
	CM_ADVERSARY_TYPE_SLIPSTREAM,				// 25
	CM_ADVERSARY_TYPE_KILL_QUOTA,				// 26
	CM_ADVERSARY_TYPE_JUGGERNAUT,				// 27
	CM_ADVERSARY_TYPE_TURF_WAR,					// 28
	CM_ADVERSARY_TYPE_POINTLESS,				// 29
	CM_ADVERSARY_TYPE_VEHICLE_VENDETTA,			// 30
	CM_ADVERSARY_TYPE_TINY_RACERS,				// 31
	
	CM_ADVERSARY_TYPE_LANDGRAB,					// 32 (0)
	CM_ADVERSARY_TYPE_RESURRECTION,				// 33 (1)
	CM_ADVERSARY_TYPE_OVERTIME,					// 34 (2)
	CM_ADVERSARY_TYPE_DAWN_RAID,				// 35 (3)
	CM_ADVERSARY_TYPE_POWER_MAD,				// 36 (4)
	CM_ADVERSARY_TYPE_OVERTIME_RUMBLE,			// 37 (5)
	CM_ADVERSARY_TYPE_VEHICLE_WARFARE,			// 38 (6)
	CM_ADVERSARY_TYPE_BOMBUSHKA_RUN,			// 39 (7)
	CM_ADVERSARY_TYPE_STOCKPILE,				// 40 (8)
	CM_ADVERSARY_TYPE_CONDEMNED,				// 41 (9)
	CM_ADVERSARY_TYPE_AIR_SHOOTOUT,				// 42 (10)
	CM_ADVERSARY_TYPE_HARD_TARGET,				// 43 (11)
	CM_ADVERSARY_TYPE_SLASHERS,					// 44 (12)
	CM_ADVERSARY_TYPE_HOSTILE_TAKEOVER,			// 45 (13)
	CM_ADVERSARY_TYPE_AIR_QUOTA,				// 46 (14)
	CM_ADVERSARY_TYPE_VENETIAN_JOB,				// 47 (15)
	CM_ADVERSARY_TYPE_TRAP_DOOR,				// 48 (16)
	CM_ADVERSARY_TYPE_SUMO_REMIX,				// 49 (17)
	CM_ADVERSARY_TYPE_HUNTING_PACK_REMIX,		// 50 (18)
	CM_ADVERSARY_TYPE_TRADING_PLACES_REMIX,		// 51 (19)
	CM_ADVERSARY_TYPE_RUNNING_BACK_REMIX,		// 52 (20)
	CM_ADVERSARY_TYPE_FRANKLIN_LAMAR_HOLDOUT,	// 53 (21)

	CM_ADVERSARY_TYPE_MAX
ENDENUM

// -----------------------------------------------------------------------------------------------------------

// ENUM for choosing Contact Missions to play
ENUM g_eContactMissionSelection
	CM_RANK_USE_PREVIOUSLY_PLAYED_BELOW_CURRENT_RANK,					// Previously Played missions below Current Player Rank
	CM_RANK_USE_NO_PLAYER_RECORD_BELOW_CM_RANK,							// New Missions (with no 'player record'), below the CM Min Rank
	// Leave this at the bottom	
	CM_RANK_USE_UNPLAYED_BETWEEN_CM_MIN_AND_CURRENT_RANKS				// Unplayed between the CM Min Rank and the Current Player Rank
ENDENUM



// -----------------------------------------------------------------------------------------------------------
//      Heist Consts
//		(Potentially OBSOLETE variables - used with teh server storage method of Heist triggering)
// -----------------------------------------------------------------------------------------------------------

// No Safehouse
CONST_INT			NO_OWNED_SAFEHOUSE									0	// see property consts setup in net_reality_details.sch, they start at 1 upwards - Conor interprets 0 as 'my safehouse', I'll treat it as 'none'

// TEMP: No Safehouse Interior - probably only needed while we're sharing the same interior for multiple safehouses
CONST_INT			NO_SAFEHOUSE_INTERIOR_FOR_HEISTS					-1

// Various Delay lengths for Heist Group processing
CONST_INT			MP_HEIST_SCHEDULING_DELAY_msec						60000
CONST_INT			MP_HEIST_CONTACT_DELAY_msec							1000

// Delay after buying a new property before the player can be given a Heist
CONST_INT			MP_HEIST_NEW_PROPERTY_DELAY_msec					30000



// -----------------------------------------------------------------------------------------------------------
//      Heist Consts
//		(The Local Storage CONSTS - used with the new way of doing Heist triggering)
// -----------------------------------------------------------------------------------------------------------

// The Maximum number of local Heist Strands (ie: max number of Heist Finale missions coming in from the Cloud - used for array sizes, etc)
CONST_INT			MAX_HEIST_STRANDS									25

// Illegal local Heist Strand entry
CONST_INT			ILLEGAL_HEIST_STRAND_SLOT							-1

// Delay between the Lester 'intro-to-heist' cutscene and the tutorial phonecall
CONST_INT			HEIST_CONTROL_INTRO_TO_TUTORIAL_DELAY_msec			(60000 * 2)
// Delay between the end of the Tutorial Heist and the Next Heist
CONST_INT			HEIST_CONTROL_TUTORIAL_TO_NEXT_HEIST_DELAY_msec		(60000 * 30)
// Standard Delay between each Heist
CONST_INT			HEIST_CONTROL_BETWEEN_HEISTS_DELAY_msec				(60000 * 60)
// Standard Delay between all heists if they've been already played
// KGM 19/2/15 [BUG 2243977]: We no longer want these to trigger, so we're setting the 'already played' delay to 48hours which effectively blocks these but gives us tunable flexibility to get them back
CONST_INT			HEIST_CONTROL_ALREADY_PLAYED_DELAY_msec				(60000 * 60 * 48)
// Short delay in case quick retry is required after an error
CONST_INT			HEIST_CONTROL_QUICK_RETRY_DELAY_msec				(60000 * 2)
// Repeat Phonecall Timeout if the Heist phonecall is not picked up
CONST_INT			HEIST_CONTROL_REPEAT_PHONECALL_DELAY_msec			35000
// Longer Delay repeat Phonecall Timeout if the Heist phonecall is not picked up (used if the Heist has previously been completed as Leader)
CONST_INT			HEIST_CONTROL_REPEAT_PHONECALL_LONGER_DELAY_msec	(60000 * 30)

// KGM 28/1/15 [BUG 2211902]: During JIP, keep the new heist strand timeout time above a minimum threshold to avoid an unfortunately timed mission load wiping the JIP data
// 		NOTE: If timer remaining falls below HEIST_JIP_MINIMUM_TIME_REMAINING_THRESHOLD_msec then boost it by HEIST_JIP_TIMEOUT_BOOST_msec
CONST_INT			HEIST_JIP_MINIMUM_TIME_REMAINING_THRESHOLD_msec		30000
CONST_INT			HEIST_JIP_TIMEOUT_BOOST_msec						10000

// KGM 2/2/15 [BUG 2220501]: During 'next heist phonecall delay', if the time gets below this threshold then set a STAT so that, on a reboot, the phonecall occurs quickly
CONST_INT			HEIST_SET_QUICK_PHONECALL_STAT_THRESHOLD_msec		(60000 * 10)
CONST_INT			HEIST_NEXT_HEIST_QUICK_PHONECALL_TIMEOUT_msec		60000

// Empty Cross-Session Transmission data
CONST_INT			EMPTY_HEIST_CROSS_SESSION_DATA						-1

// Value added to the cross-session Data to indicate the corona is for an Intro Cutscene or a Mid-Strand Cutscene
CONST_INT			HEIST_CUTSCENE_CORONA_CS_DATA_VALUE					10000
CONST_INT			HEIST_MID_STRAND_CUTSCENE_CORONA_CS_DATA_VALUE		20000
CONST_INT			HEIST_TUTORIAL_CUTSCENE_CORONA_CS_DATA_VALUE		40000
CONST_INT			HEIST_SIMPLE_INTERIOR								100000
CONST_INT			HEIST_SIMPLE_INTERIOR_STYLE							1000

// The size of the array of other Heist Coronas
CONST_INT			NUM_OTHER_HEIST_CORONAS								15		// NUM_NETWORK_PLAYERS - SPACES FOR SCTV - 1 (FOR LOCAL PLAYER BECAUSE NOT NEEDED)

// An illegal Heist RegID
CONST_INT			ILLEGAL_HEIST_CORONA_REG_ID							-1

// The cooldown period to delay new Heist Strands from being given having recently finished a Heist-related mission
CONST_INT			NEW_HEIST_STRAND_COOLDOWN_DELAY_msec				600000	// 10 mins

// Heist Strand replay delay details - to prevent phoning to replay a completed heist
CONST_INT			MAX_HEISTS_WITH_REPLAY_DELAYS						3
CONST_INT			HEIST_REPLAY_DELAY_TIMEOUT_msec						(60000 * 60)

// Heist Tutorial Help Text 'Go To Apartment' re-display delay
CONST_INT			HEIST_AVAILABLE_TUTORIAL_HELP_TEXT_DELAY_msec		(60000 * 60)
CONST_INT			HEIST_HELP_TEXT_QUICK_DELAY_msec					60000

// The length of time to display the 'not enough cash' help message (and other help messages) in the planning room
CONST_INT			HEIST_SEMI_PERMANENT_HELP_TIME_msec					(60000 * 3)

// The number of attempts allowed at downloading the Prep Mission header data in failure
CONST_INT			MAX_HEIST_PREP_MISSION_DOWNLOAD_ATTEMPTS			3

// Heist 'Another Heist Soon' Help Text delays
CONST_INT			ANOTHER_HEIST_SOON_INITIAL_DELAY_msec				1000
CONST_INT			ANOTHER_HEIST_SOON_QUICK_DELAY_msec					20000

// Heist Completion Phonecall delays
CONST_INT			POST_HEIST_PHONECALL_INITIAL_DELAY_msec				(60000 * 2)
CONST_INT			POST_HEIST_PHONECALL_QUICK_DELAY_msec				60000

// Heist 'Replay Reminder' Help Text delays
CONST_INT			REPLAY_REMINDER_INITIAL_DELAY_msec					(60000 * 5)
CONST_INT			REPLAY_REMINDER_ALL_FINALES_DELAY_msec				60000
CONST_INT			REPLAY_REMINDER_QUICK_DELAY_msec					20000

// Heist 'Versus Pause Menu' Help Text delays
CONST_INT			VERSUS_PAUSE_MENU_DELAY_msec						30000

// KGM 7/2/15 [BUG 2228603]: Heist 'Replayable Heists' data gathering delay
CONST_INT			REPLAYABLE_HEIST_DATA_GATHERING_TIMEOUT_msec		60000
CONST_INT			REPLAYABLE_HEIST_QUICK_DATA_GATHERING_TIMEOUT_msec	1000




// -----------------------------------------------------------------------------------------------------------
//      Gang Attack Consts
// -----------------------------------------------------------------------------------------------------------

// The Maximum Gang Attacks available on the map at any one time
CONST_INT			MAX_ACTIVE_GANG_ATTACKS					MAX_MISSIONS_SHARED_CLOUD_DATA

// The number of recent Gang Attacks played
CONST_INT			MAX_GANG_ATTACK_RECENT_HISTORY			MAX_MISSIONS_SHARED_CLOUD_DATA

// The delay before an active Gang Attack is added to the map again so that a player can rejoin it
CONST_INT			GANG_ATTACK_REACTIVATION_DELAY_msec		25000

// The minimum time before a Gang Attack can be replayed
CONST_INT			GANG_ATTACK_PLAY_AGAIN_DELAY_msec		(60000 * 30)

// The delay between Gang Attack server History List checks
CONST_INT			GANG_ATTACK_HISTORY_CHECK_msec			60000

// Indicates that none of the slots containing Gang Attacks on the map are active
CONST_INT			NO_ACTIVE_GANG_ATTACK_SLOT				-1





// ===========================================================================================================
//      Server Globals
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//      Group Scheduling - Server
// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Group Control variables
// NOTE: These will change frequently, so keep them local on all machines but only the current host will access them
//			(it won't matter if they go slightly out of date after a host migration, or if the scheduled group starts at the beginning again)
STRUCT g_structASGroupControlServerMP
	INT				asgcsNextScheduledGroupArrayPos	= 0					// The next scheduled group to receive some scheduled maintenance (remember: Groups are generated on-the-fly based on vector)
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------
//      Contact Missions - Server
// -----------------------------------------------------------------------------------------------------------

// ENUM containing Server Control Stages for one Contact Mission
ENUM g_eContactMissionStagesServer
	CONTACT_MISSION_SERVER_STAGE_ALLOW_INVITE,							// Allow the client to send an invite from the contact for the mission
	CONTACT_MISSION_SERVER_STAGE_TEMP_ACTIVE_MISSION,					// TEMP: To be broken down - Client has sent the invite to themselves to start the mission

	// Leave this at the bottom
	CONTACT_MISSION_SERVER_STAGE_NO_MISSION								// There is currently no Mission being controlled for this Contact
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Control variables for one Contact Mission on the Server
// NOTE: An array of these will need included as part of the serverBD data
STRUCT g_structContactMissionServer
	g_eContactMissionStagesServer		cmsStage		= CONTACT_MISSION_SERVER_STAGE_NO_MISSION		// Current Server Stage for a mission from this contact
	TIME_DATATYPE						cmsTimeout														// Timeout before another mission is allowed to be offered from this contact
	PLAYER_INDEX						cmsPlayer														// The player currently being allowed to deal with a Contact Mission from this Contact
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing any Contact Mission Server Overall Control variables that should remain on local machine
// NOTE: These will change frequently, so keep them local on all machines but only the current host will access them
//			(it won't matter if they go slightly out of date after a host migration, or if the scheduled group starts at the beginning again)
STRUCT g_structASCMControlServerMP
	INT				ascmcsNextScheduledCMContact			= 0					// The g_eFMHeistContactIDs (as INT) within the Contact Mission group that will next schedule an update
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------
//      Heist Missions - Server
// -----------------------------------------------------------------------------------------------------------

// ENUM containing Server Control Stages for one Heist Mission
ENUM g_eHeistStagesServer
	HEIST_SERVER_STAGE_AWAITING_PLAYER_IN_CORONA,				// Waiting for the controlling player to be in the corona
	HEIST_SERVER_STAGE_AWAITING_HEIST_STARTED,					// Waiting for the Heist to be started
	HEIST_SERVER_STAGE_HEIST_IN_PROGRESS,						// Heist is in progress so waiting for it to be all cleaned up

	// Leave this at the bottom
	HEIST_SERVER_STAGE_NO_HEIST_SELECTED						// There is currently no Heist mission selected
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Control variables for one Heist mission on the Server
// NOTE: An array of these will need included as part of the serverBD data
STRUCT g_structHeistMissionServer
	g_eHeistStagesServer	hmsStage				= HEIST_SERVER_STAGE_NO_HEIST_SELECTED		// Current Heist Control Stage
	INT						hmsPendingActivitySlot	= ILLEGAL_PENDING_ACTIVITIES_SLOT			// The Pending Activity slot containing the selected heist mission
	PLAYER_INDEX			hmsPlayer															// The player curently chosen to be the controlling player for this Heist
	INT						hmsSharedRegID			= ILLEGAL_SHARED_REG_ID						// The SharedRegID for this Heist mission (gets setup when controlling player enters corona)
	INT						hmsUniqueID				= NO_UNIQUE_ID								// The UniqueID for the Mission Request
	TIME_DATATYPE			hmsContactsDelay													// Delay between missions from the same contact
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing any Heist Server Overall Control variables that should remain on local machine
// NOTE: These will change frequently, so keep them local on all machines but only the current host will access them
//			(it won't matter if they go slightly out of date after a host migration, or if the scheduled group starts at the beginning again)
STRUCT g_structASHeistControlServerMP
	INT				ashcsNextScheduledHeistContact		= 0					// The g_eFMHeistContactIDs (as INT) within the Heist group that will next schedule an update
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------
//      Gang Attacks - Server
// -----------------------------------------------------------------------------------------------------------

STRUCT g_structActiveGangAttackServer_OLD
	INT							agasSharedMissionSlot		= NO_ACTIVE_GANG_ATTACK_SLOT			// The slot containing this Gang Attack on the Shared Missions array
	TEXT_LABEL_31				agasCloudFilename													// The cloud filename of the active Gang Attack
ENDSTRUCT


// STRUCT containing Server Control variables for one Active Gang Attack
STRUCT g_structActiveGangAttackServer
	INT							agasSharedMissionSlot		= NO_ACTIVE_GANG_ATTACK_SLOT			// The slot containing this Gang Attack on the Shared Missions array
	TEXT_LABEL_23				agasCloudFilename													// The cloud filename of the active Gang Attack
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

STRUCT g_structGangAttackHistoryServer_OLD
	TEXT_LABEL_31				gahsCloudFilename													// The cloud filename of a recently played Gang Attack
	TIME_DATATYPE				gahsTimeout															// The time that this Gang Attack can be removed from the history list and re-used if needed
ENDSTRUCT



// STRUCT containing Server Control variables for recently played Gang Attack
STRUCT g_structGangAttackHistoryServer
	TEXT_LABEL_23				gahsCloudFilename													// The cloud filename of a recently played Gang Attack
	TIME_DATATYPE				gahsTimeout															// The time that this Gang Attack can be removed from the history list and re-used if needed
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

STRUCT g_structGangAttackServer_OLD
	g_structActiveGangAttackServer_OLD	gasActive[MAX_ACTIVE_GANG_ATTACKS]								// An array containing details of currently active Gang Attacks - NOT CONTIGUOUS
	INT									gasNumActive 	= 0												// The number of active Gang Attacks - to cut down on processing, but the array CAN'T be continguous because it is broadcast data
	g_structGangAttackHistoryServer_OLD	gasHistory[MAX_GANG_ATTACK_RECENT_HISTORY]						// An array of most recent gang attacks - to prevent quick re-activation - NOT CONTIGUOUS
ENDSTRUCT


// STRUCT containing all Gang Attack control variables to be stored on serverBD
STRUCT g_structGangAttackServer
	g_structActiveGangAttackServer		gasActive[MAX_ACTIVE_GANG_ATTACKS]								// An array containing details of currently active Gang Attacks - NOT CONTIGUOUS
	INT									gasNumActive 	= 0												// The number of active Gang Attacks - to cut down on processing, but the array CAN'T be continguous because it is broadcast data
	g_structGangAttackHistoryServer		gasHistory[MAX_GANG_ATTACK_RECENT_HISTORY]						// An array of most recent gang attacks - to prevent quick re-activation - NOT CONTIGUOUS
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing any Contact Mission Server Overall Control variables that should remain on local machine
// NOTE: These will change frequently, so keep them local on all machines but only the current host will access them
//			(it won't matter if they go slightly out of date after a host migration, or if the scheduled group starts at the beginning again)
STRUCT g_structASGAControlServerMP
	INT				asgacsNextScheduledSharedCheck			= 0					// The next slot within the Shared Mission array to be checked if it contains a Gang Attack
	TIME_DATATYPE	asgacsNextHistoryCheckTimeout								// The timeout that will allow another Gang Attack history list re-activation check
ENDSTRUCT




// ===========================================================================================================
//      Client Globals
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//      Group Scheduling - Client
// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Group Control variables
// NOTE: These will change frequently, so keep them local on all machines but only the current host will access them
//			(it won't matter if they go slightly out of date after a host migration, or if the scheduled group starts at the beginning again)
STRUCT g_structASGroupControlClientMP
	INT				asgccNextScheduledGroupArrayPos	= 0					// The next scheduled group to receive some scheduled maintenance (remember: Groups are generated on-the-fly based on vector)
ENDSTRUCT



// -----------------------------------------------------------------------------------------------------------
//      Contact Missions - Client
// -----------------------------------------------------------------------------------------------------------

// ENUM containing Client Control Stages for one Contact Mission
ENUM g_eContactMissionStagesClient
	CONTACT_MISSION_CLIENT_STAGE_REQUEST_TO_SEND_INVITE,				// Client is requesting permission from the server to send an invite for a Contact Mission
	CONTACT_MISSION_CLIENT_STAGE_SEND_INVITE,							// Client has been given permission to send the invite
	CONTACT_MISSION_CLIENT_STAGE_INVITE_SENT,							// Client has sent the invite, so prepare for another CM Invite if allowed
	CONTACT_MISSION_CLIENT_STAGE_SEND_INVITE_ADVERSARY,					// Cleint has been given permission to send a adversary mission invite

	// Leave this at the bottom
	CONTACT_MISSION_CLIENT_STAGE_NO_MISSION								// There is currently no active Contact Mission for this player
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// OBSOLETE: This was the pre-titleUpdate struct - needs retained to preserve the global signature
//STRUCT g_structLocalContactMissions_Old
//	g_eFMHeistContactIDs			lcmContact				= NO_FM_HEIST_CONTACT								// The Contact
//	INT								lcmRank					= 0													// The Mission Rank
//	INT								lcmCreatorID			= ILLEGAL_CREATOR_ID								// The Creator ID
//	INT								lcmVariation			= NO_MISSION_VARIATION								// The Array Position of this mission on the global cloud array
//ENDSTRUCT

// STRUCT containing the positions of contact missions on the local cloud header data arrays
STRUCT g_structLocalContactMissions
	g_eFMHeistContactIDs			lcmContact				= NO_FM_HEIST_CONTACT								// The Contact
	INT								lcmRank					= 0													// The Mission Rank
	INT								lcmContentIdHash		= 0													// The mission name contentID Hash
	INT								lcmRootContentIdHash	= 0													// The mission's rootContentID Hash
	INT								lcmCreatorID			= ILLEGAL_CREATOR_ID								// The Creator ID
	INT								lcmVariation			= NO_MISSION_VARIATION								// The Array Position of this mission on the global cloud array
	INT								lcmOnHours				= MATC_ALL_DAY										// The OnHours for the Contact Mission
	BOOL							lcmPlayed				= FALSE												// TRUE if the mission has been played, otherwise FALSE
	BOOL							lcmOnePlay				= FALSE												// TRUE if the mission shouldn't be offered again once played, otherwise FALSE
	BOOL							lcmForcedAtRank			= FALSE												// TRUE if the mission should be forced at the specified rank, otherwise FALSE
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Contact Mission Rank stats
STRUCT g_structCMRankMissions_Old
	INT								cmrmRank				= 0													// The hidden Contact Mission Rank - this will never be higher than the real player rank
	TEXT_LABEL_23					cmrmRankContentIdsComplete[10]												// An array of Content IDs completed at the current CM Rank
ENDSTRUCT


// STRUCT containing Contact Mission Rank stats
// NOTE: The Hash ContentIDs complete is now the Hash RootContentIDs complete (31/1/14)
STRUCT g_structCMRankMissions
	INT								cmrmMinRank				= 0													// The Contact Mission Minimum Rank - this will never be higher than the real player rank
	INT								cmrmHashContentIdsComplete[MAX_CM_CONTENTIDS_PLAYED]						// An array of completed contact mission ContentIDs as Hash values
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT Counts the number of Contact Missions played up to and including the current contact mission rank per contact
STRUCT g_structCMsPlayedPerContact
	INT								cmppcPlayed[MAX_FM_HEIST_CONTACTS]											// The number of contact missions played per contact
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

STRUCT g_structContactMissionClient_OLD
	g_eContactMissionStagesClient	cmcStage				= CONTACT_MISSION_CLIENT_STAGE_NO_MISSION			// Current Contact Mission control stage
	g_eFMHeistContactIDs			cmcContact				= NO_FM_HEIST_CONTACT								// The current Heist contact whose mission has been chosen for this player
	MP_MISSION_ID_DATA_OLD			cmcMissionIdData															// The MissionID Data for the chosen mission
	TIME_DATATYPE					cmcTimeout																	// Timeout before this player can be offered another Contact Mission
ENDSTRUCT


// STRUCT containing the current Contact Mission control variables for the player - a player can only be handling one Contact Mission at a time
// An instance of this struct needs setup as playerBD
STRUCT g_structContactMissionClient
	g_eContactMissionStagesClient	cmcStage				= CONTACT_MISSION_CLIENT_STAGE_NO_MISSION			// Current Contact Mission control stage
	g_eFMHeistContactIDs			cmcContact				= NO_FM_HEIST_CONTACT								// The current Heist contact whose mission has been chosen for this player
	MP_MISSION_ID_DATA				cmcMissionIdData															// The MissionID Data for the chosen mission
	BOOL							cmcPreviouslyCompleted	= FALSE												// TRUE if this mission is from the pool of completed missions, FALSE if is is a new mission
	TIME_DATATYPE					cmcTimeout																	// Timeout before this player can be offered another Contact Mission
	INT								cmcActiveContentIdHashes[MAX_CONTACT_MISSION_INVITES_AT_ONCE]				// ContentID hash values of the CM Invites currently active for a player
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing local storage of currently active contact mission invites
// NOTE: These are transition-safe so that the existing contact missions can be setup again after a transition
STRUCT g_structLocalClientActiveCMs
	g_eFMHeistContactIDs			lcacmContact				= NO_FM_HEIST_CONTACT							// The current heist contact that has given this Contact Mission
	MP_MISSION_ID_DATA				lcacmMissionIdData															// The current mission details for this Contact Mission
	BOOL							lcacmPreviouslyCompleted	= FALSE											// TRUE if this mission is from the pool of completed missions, FALSE if it is a new mission
	BOOL							lcacmForcedAtRank			= FALSE											// TRUE if this mission is 'Forced At Rank', otherwise FALSE
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing control variables for checking if the player is on a Contact Mission
STRUCT g_structClientPlayingCM
	BOOL							cpcmOnCM					= FALSE											// TRUE if the player is on any mission, otherwise FALSE
	BOOL							cpcmFirstPlay				= FALSE											// TRUE for first play of the mission, FALSE for subsequent plays (and problems) - so only half cash is rewarded
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing control variables for delaying missions from the contact of the most recently played contact
STRUCT g_structClientRecentContactCM
	g_eFMHeistContactIDs			crcmHeistContact			= NO_FM_HEIST_CONTACT							// The Heist Contact
	INT								crcmFrameTimeout			= 0												// The frame that this contact can offer missions again
ENDSTRUCT

// STRUCT containing local played history of Contact Mission
// NOTE: This is transition-safe so that the history is retained after transition
STRUCT g_structClientHistoryCM
	g_structClientRecentContactCM	chcmRecentContact															// Details of the contact of the most recetly played contact mission
ENDSTRUCT



// -----------------------------------------------------------------------------------------------------------
//      Heist Missions - Client
//		(THE OLD, POTENTIALLY OBSOLETE, GLOBALS)
// -----------------------------------------------------------------------------------------------------------

// ENUM containing Client Control Stages for one Heist Mission
ENUM g_eHeistStagesClient
	HEIST_CLIENT_STAGE_NO_HEIST,							// There is currently no active Heist
	HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY,		// An initial delay before allowing Heists to trigger
	HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE,		// Download the Finale mission to gather some Planning details
	HEIST_CLIENT_STAGE_WAIT_UNTIL_COMMS_ALLOWED,			// Waiting for the ambient message routines to give the ok to tell the player about the next Heist
	HEIST_CLIENT_STAGE_REQUEST_COMMS,						// The ambient message routines have given the OK to tell the player about this Heist
	HEIST_CLIENT_STAGE_WAIT_FOR_COMMS_TO_END,				// The Comms is in progress, so wait for it to end
	HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT,			// Wait for the player to press the DPAD-RIGHT key when in the location
	HEIST_CLIENT_STAGE_DISPLAY_HEIST_COST,					// Display the cost of the heist and wait for the player to accept
	HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA,			// Register the Finale Heist as a Cutscene Corona in the apartment
	HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE,		// Wait until the Heist strand has been activated
	HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_COMPLETE,		// Wait until the Heist strand has been completed
	HEIST_CLIENT_STAGE_HEIST_VARIABLES_RESET,				// Reset the Heist variables ready for the next Heist

	// Leave this at the bottom
	HEIST_CLIENT_STAGE_HEIST_ERROR							// A holding variable
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing general Control variables for Heist missions as a group controlled by the Client
STRUCT g_structHeistGroupClient
	g_eHeistStagesClient	hgcStage				= HEIST_CLIENT_STAGE_NO_HEIST					// Current Heist Group control stage
	g_eFMHeistContactIDs	hgcActiveContact		= NO_FM_HEIST_CONTACT							// The current contact whose mission has been chosen to be played by the player
	INT						hgcMatcRegID			= ILLEGAL_AT_COORDS_ID							// MissionsAtCoords registrationID for the heist
	INT						hgcSharedRegID			= ILLEGAL_SHARED_REG_ID							// Shared Missions RegistrationID for the heist
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Control variables for one Heist mission controlled by the Client
// NOTE: An array of these will need included as part of the clientBD data
STRUCT g_structHeistMissionClient
	g_eHeistStagesClient	hmcStage				= HEIST_CLIENT_STAGE_NO_HEIST					// Current Heist control stage
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing General Heist Control Variables for the Client
// NOTE: This struct will be included as player broadcast data
STRUCT g_structHeistGeneralClient
	INT						hgcSafehouseID			= NO_OWNED_SAFEHOUSE							// Player's Currently Owned Safehouse
	TIME_DATATYPE			hgcNewPropertyDelay														// Delays this player from becoming Heist controller in the new property for a short time
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing any Heist Client Overall Control variables that should remain on local machine
// NOTE: These may change frequently, so keep them local on all machines rather than as PlayerBD
STRUCT g_structASHeistControlClientMP
	INT				ashccNextScheduledHeistContact		= 0					// The g_eFMHeistContactIDs (as INT) within the Heist group that will next schedule an update
ENDSTRUCT



// -----------------------------------------------------------------------------------------------------------
//      Heist Strands - Client
//		(THE NEW LOCAL DATA STORAGE GLOBALS)
// -----------------------------------------------------------------------------------------------------------

// STRUCT containing the positions of Heist Strands on the local cloud header data arrays
// NOTE: Using the Heist Finale mission as the control mission for a Heist Strand
STRUCT g_structLocalHeistStrands
	g_eFMHeistContactIDs			lhsContact				= NO_FM_HEIST_CONTACT								// The Contact
	INT								lhsRank					= 0													// The Heist Strand Rank
	MP_MISSION_ID_DATA				lhsMissionIdData															// The missionID Data for the Heist Finale of the Heist Strand
	INT								lhsHashFinaleRCID		= 0													// The Heist Finale rootContentID hash
	BOOL							lhsCompletedAsLeader	= FALSE												// TRUE if Heist strand completed as Leader, otherwise FALSE
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing variables for when a Heist corona should be in their own apartment
STRUCT g_structMyHeistCorona
	BOOL							mhcAvailable			= FALSE												// TRUE if there should be a corona available to the player to be displayed in in apartment, otherwise FALSE
	TEXT_LABEL_23					mhcContentID																// The contentID for the current Heist job (Finale or Planning)
	BOOL							mhcIsFinale				= FALSE												// TRUE if the corona is for a Heist Finale, FALSE for Heist Planning
	BOOL							mhcIsIntroCutscene		= FALSE												// TRUE if the corona is for the pre-planning Intro cutscene, otherwise FALSE
	BOOL							mhcIsMidStrandCutscene	= FALSE												// TRUE if the corona is for the Mid-Strand cutscene, otherwise FALSE
	INT								mhcMatcID				= ILLEGAL_AT_COORDS_ID								// The Matc Registered ID for an active corona
	BOOL							mhcInCorona				= FALSE												// TRUE if the player is in the Heist corona, otherwise FALSE
	BOOL							mhcAlreadyTransitioned	= FALSE												// TRUE if already in an Activity Session, otherwise FALSE (24/7/14: this can now occur after the Intro Cutscene - the first mission from the board is selected while still in the activity session)
	BOOL							mhcIsTutorialHeist		= FALSE
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing local Heist Control variables
// NOTE: These only get cleared when joining from SP
STRUCT g_structLocalHeistControl
	g_eHeistStagesClient			lhcStage				= HEIST_CLIENT_STAGE_NO_HEIST						// Current Heist Strand control stage
	INT								lhcTimeout																	// Used as a Timeout timer (ie: for Heist Initial Delay) [KGM 14/11/14 BUG 2131861: Converted to an INT]
	INT								lhcHashHeistRCID		= 0													// This will store the Finale Heist RCID Hash (retained until overwritten) - initially use 0 to kick things off
	g_structMyHeistCorona			lhcMyCorona																	// Handles the player's own corona, making it appear when player is in their own apartment
	INT								lhcContextCutscene		= -1												// The current context ID - (NEW_CONTEXT_INTENTION = -1 but I can't include header here)
	INT								lhcRequiredInitialDelay	= HEIST_CONTROL_BETWEEN_HEISTS_DELAY_msec			// The Required Initial Delay to be setup - allows the delay to differ based on circumstances
	BOOL							lhcPrepsDownloadedIn500	= FALSE												// TRUE if the Prep Missions have ben downloaded in array locations 500+, FALSE if they require to be downloaded
	INT								lhcDownloadAttempts		= 0													// The number of attempts at downloading the Prep Mission header data
	INT								lhcTutorialHelpTimeout	= 0													// A timeout that controls re-displaying the Tutorial Heist 'Go To Apartment' Help text
	BOOL							lhcClearTasksOnAlert	= FALSE												// TRUE if the Player needs to Clear Tasks behind the 'Cost' alert screen, FALSE if not
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing variables for one Heist Corona in the session belonging to another player standing in their corona
STRUCT g_structOtherHeistCoronas
	INT								ohcRegID				= ILLEGAL_HEIST_CORONA_REG_ID						// The RegID for this Heist Corona
	MP_MISSION_ID_DATA				ohcMissionIdData															// The MissionIdData for another player's corona
	VECTOR							ohcCoords																	// The Coords of another player's corona
	PLAYER_INDEX					ohcPlayerID																	// The PlayerID of the other player
	BOOL							ohcForIntroCutscene		= FALSE												// TRUE if this corona is for another player's Intro cutscene, otherwise FALSE
	BOOL							ohcForMidStrandCutscene	= FALSE												// TRUE if this corona is for another player's Mid-Strand cutscene, otherwise FALSE
	BOOL							ohcForTutorialCutscene	= FALSE												// TRUE if this corona is for another player's Tutorial cutscene, otherwise FALSE
	BOOL							ohcMarkedForDelete		= FALSE												// TRUE if this corona has been marked for delete, otherwise FALSE
	INT								ohcUniqueIDFromInvite	= NO_UNIQUE_ID										// The Mission Controller UniqueID when player accepts an invite to another player's heist-related mission
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing the control variables for all other player's active Heist coronas in the session
// NOTE:	The local player's corona is handled in the local data struct above
STRUCT g_structOtherHeistCoronasControl
	INT								ohccOnDisplayHeistRegID					= ILLEGAL_HEIST_CORONA_REG_ID		// The Heist RegID of the current 'on display' corona
	INT								ohccOnDisplayMatcRegID					= ILLEGAL_AT_COORDS_ID				// The Matc RegID of the current 'on display' corona
	g_structOtherHeistCoronas		ohccOtherCoronas[NUM_OTHER_HEIST_CORONAS]									// An array of other player's active heist coronas in the session [CONTIGUOUS]
	INT								ohccNumOtherCoronas						= 0									// The number of other player's active heist coronas in the session
	INT								ohccNextHeistCoronaRegID				= 0									// Tracks the next Heist Corona RegID to be given to a registered corona
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing details for transmission cross-session for CS Invites and JIPs
STRUCT g_structLocalHeistCrossSession
	INT						lhcsDataToBeTransmittedToOthers		= EMPTY_HEIST_CROSS_SESSION_DATA				// Packed INT containing data from Heist Leader to Cross-session joiners (ApartmentID, isCutsceneCorona)
	INT						lhcsHeistApartmentReceivedCS		= NO_OWNED_SAFEHOUSE							// Leader's safehouseID (as received cross-session)
	BOOL					lhcsIsHeistCutsceneCoronaCS			= FALSE											// TRUE if the Leader is in a generic (ie: Intro or MidStrand) Heist Cutscene corona (as received cross-session), otherwise FALSE
	BOOL					lhcsIsHeistMidStrandCutCoronaCS		= FALSE											// TRUE if the Leader is in a Heist Mid-Strand Cutscene corona (as received cross-session), otherwise FALSE
	BOOL					lhcsIsHeistTutorialCutCoronaCS		= FALSE											// TRUE if the Leader is in a Heist Tutorial Cutscene corona (as received cross-session), otherwise FALSE
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// A Struct used to temporarily store the Heist Planning RCID and Board Level data extracted from the Finale Mission
STRUCT g_structHeistPlanningData
	TEXT_LABEL_23	rootContentID			// A planning mission's rootContentID
	INT				boardLevel				// The level on the board that this mission will appear on (with 0 being the initially available missions)
ENDSTRUCT

// A struct used to temporarily hold all pre-heist Planning data downloaded from the Finale Mission
STRUCT g_structPreHeistDownloadData
	INT							phddFinaleHashRCID		= 0								// The Hash value of the Finale RCID from which teh data was downloaded - used for data verification purposes
	g_structHeistPlanningData	phddPrepMissions[MAX_HEIST_PLANNING_MISSIONS]			// An array of Planning Mission details ready to be stored as stats (RCID and Board Level)
	INT							phddReward				= 0								// The overall reward value for completing the Heist - used to calculate the upfront Leader cost
	INT							phddUpfrontLeaderCost	= 0								// The Upfront Heist Cost charged to the Leader, calculated from the Reward Band
	BOOL						phddIsTutorial			= FALSE 						// A flag to indicate that this Finale is for the Tutorial Heist - used to auto-navigate through the tutorial (ie: skipping out pre-planningboard, going directly from intro cutscene to first mission)
	INT							phddSessionIdMacAddr	= 0								// Heist Session ID (to uniquely identify this Heist Strand play): INT 1 is the Mac Address of the Leader
	INT							phddSessionIdPosixTime	= 0								// Heist Session ID (to uniquely identify this Heist Strand play): INT 2 is the Posix Time the Heist data downloaded
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// A Struct to record the most recently completed Heist Strands and their timeout time
// NOTE: Only a reboot should clear this data
STRUCT g_structHeistReplayDelays
	INT							hrdFinaleHashRCID		= 0								// The Hash value of the Finale RCID of the completed Heist Strand
	INT							hrdDelayTimeout			= 0								// The timeout time
ENDSTRUCT



// -----------------------------------------------------------------------------------------------------------
//      Heist 'another heist soon' help text
// -----------------------------------------------------------------------------------------------------------

// A Struct to decide if and when the 'another heist soon' help text should get displayed
// NOTE: Only a reboot should clear this data
STRUCT g_structHeistHelpText
	BOOL						hhtAnotherHeistSoonRequired		= FALSE					// TRUE if the 'Another Heist Soon' help text is required
	INT							hhtAnotherHeistSoonTimeout		= 0						// Contains the timeout time when the 'another heist soon' help text will get displayed
	BOOL						hhtHeistReplayReminderRequired	= FALSE					// TRUE if the 'Replay Reminder' help text is required
	INT							hhtHeistReplayReminderTimeout	= 0						// Contains the timeout time when the 'Replay Reminder' help text will get displayed
	BOOL						hhtHeistVersusPauseHelpOnVersus	= FALSE					// TRUE if the player is currently on a Versus Mission (only TRUE if the Versus Pause Help is required after it ends)
	BOOL						hhtHeistVersusPauseHelpRequired	= FALSE					// TRUE if the 'Versus Pause Menu Triggering' help text is required
	INT							hhtHeistVersusPauseHelpTimeout	= 0						// Contains the timeout time when the 'Versus Pause Menu Triggering' help text will get displayed
ENDSTRUCT



// -----------------------------------------------------------------------------------------------------------
//      Heist Completion Phonecall
// -----------------------------------------------------------------------------------------------------------

// ENUM containing Heist Completion Phonecall Stages
ENUM g_ePostHeistPhonecallStages
	POST_HEIST_PHONECALL_STAGE_NONE,							// There is no active post heist phonecall sequence
	POST_HEIST_PHONECALL_STAGE_DELAY,							// Performing the initial delay
	POST_HEIST_PHONECALL_STAGE_WAIT_FOR_COMMS,					// Wait for the ambient system to allow comms
	POST_HEIST_PHONECALL_STAGE_REQUEST_COMMS,					// Request the phonecall
	POST_HEIST_PHONECALL_STAGE_WAIT_FOR_COMMS_END,				// Wait for the phonecall to end
	POST_HEIST_PHONECALL_STAGE_CLEANUP,							// Cleanup after Comms

	// Leave this at the bottom
	POST_HEIST_PHONECALL_STAGE_ERROR							// A holding variable
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// A Struct to decide if and when a Heist Completion phonecall should get played
STRUCT g_structHeistCompletionPhonecall
	BOOL						hcpRequired			= FALSE								// TRUE if a Heist Completion Phonecall is required
	g_ePostHeistPhonecallStages	hcpStage			= POST_HEIST_PHONECALL_STAGE_NONE	// The current post heist phonecall stage
	INT							hcpHashRCID			= 0									// The Hash RootContentID of the strand for the required phonecall
	INT							hcpTimeout			= 0									// Contains the timeout time when the heist completion phonecall will get played
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// KGM 7/2/15 [BUG 2228603]: A Struct to maintain when a 'replayable heists' check should next get carried out
// NOTE: The actual data is stored as player broadcast data: GlobalplayerBD_FM_HeistPlanning[playerID].bitsetReplayableHeists
STRUCT g_structReplayableHeists
	INT							rhTimeout			= 0									// Contains the timeout time when the data will next be gathered
	BOOL						rhInApartment		= FALSE								// Tracks a change in state to allow a quick data refresh on entering apartment
ENDSTRUCT



// -----------------------------------------------------------------------------------------------------------
//      Heist Reward Unlocks - Client
// -----------------------------------------------------------------------------------------------------------

// BITFIELDS containing Heist Unlockable Rewards status flags
CONST_INT	HEIST_UNLOCKS_ALL_CLEAR									0				// No Heist Unlockable Flags have been set
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_1_AVAILABLE					0				// Indicates the Tutorial Reward Versus Missions Invite is unlocked
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_1_SMS_SENT					1				// Indicates that the Tutorial Reward pre-Invite SMS has successfully completed
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_1_PLAYED						2				// Indicates 1st Tutorial Reward Versus Mission has been played (so first invite no longer needed)
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_2_AVAILABLE					3				// Indicates the Prison Reward Versus Missions Invite is unlocked
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_2_PLAYED						4				// Indicates 1st Prison Reward Versus Mission has been played (so 1st invite no longer needed)
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_3_AVAILABLE					5				// Indicates the Humane Labs Reward Versus Missions Invite is unlocked
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_3_PLAYED						6				// Indicates 1st Humane Labs Reward Versus Mission has been played (so 1st invite no longer needed)
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_4_AVAILABLE					7				// Indicates the Narcotics Reward Versus Missions Invite is unlocked
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_4_PLAYED						8				// Indicates a Narcotics Reward Versus Mission has been played (so invite no longer needed)
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_5_AVAILABLE					9				// Indicates the Pacific Standard Reward Versus Missions Invite is unlocked
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_5_PLAYED						10				// Indicates a Pacific Standard Reward Versus Mission has been played (so invite no longer needed)
CONST_INT	HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_1					11				// The Help text telling the player to go to the apartment for the Tutorial Heist has been sent once
CONST_INT	HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_2					12				// The Help text telling the player to go to the apartment for the Tutorial Heist has been sent twice
CONST_INT	HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_3					13				// The Help text telling the player to go to the apartment for the Tutorial Heist has been sent three times
CONST_INT	HEIST_UNLOCK_BITSET_PHONECALL_AFTER_FLEECA_TUTORIAL		14				// The phonecall after the Tutorial Heist Completion is required
CONST_INT	HEIST_UNLOCK_BITSET_PHONECALL_AFTER_PRISON_BREAK		15				// The phonecall after the Prison Heist Completion is required
CONST_INT	HEIST_UNLOCK_BITSET_PHONECALL_AFTER_HUMANE_LABS			16				// The phonecall after the Humane Labs Heist Completion is required
CONST_INT	HEIST_UNLOCK_BITSET_PHONECALL_AFTER_NARCOTICS			17				// The phonecall after the Narcotics Heist Completion is required
CONST_INT	HEIST_UNLOCK_BITSET_PHONECALL_AFTER_PACIFIC_STANDARD	18				// The phonecall after the Pacific Standard Heist Completion is required
CONST_INT	HEIST_UNLOCK_BITFLAG_REPLAY_HEIST_HELP_TEXT_REQUIRED	19				// Help text to remind the player Heists can be replayed is required (see additional bitflags below: 26, 27)
CONST_INT	HEIST_UNLOCK_BITFLAG_ALLOW_DISPLAY_VERSUS_PAUSE_HELP	20				// Help text to advise the player that Versus missions can be launched through the Pause Menu
// NOTE: To preserve existing stat flags, I'm adding these at the end
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_1_PLAYED_2nd					21				// Indicates 2nd Tutorial Reward Versus Mission has been played (so second invite no longer needed)
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_2_PLAYED_2nd					22				// Indicates 2nd Prison Reward Versus Mission has been played (so 2nd invite no longer needed)
CONST_INT	HEIST_UNLOCK_BITFLAG_GROUP_3_PLAYED_2nd					23				// Indicates 2nd Humand Labs Reward Versus Mission has been played (so 2nd invite no longer needed)
CONST_INT	HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL			24				// Gets set when the 'next heist delay' timer is close to expiring. On a reboot, allows a quick triggering phonecall.
CONST_INT	HEIST_UNLOCK_BITFLAG_CUTSCENE_SKIP_HELP_DISPLAYED		25				// Gets set when the Leader sees the 'cutscene is skippable' help text for the first time.
// Used to decide which of the three replay help texts to use (Leader after Prison, Leader after Ornate, Leader or Crew after all awards given)
CONST_INT	HEIST_UNLOCK_LEADER_REPLAY_HELP_BOARD_AVAILABLE			26				// TRUE if the replay reminder help to use is for the Leader completing all Strands
CONST_INT	HEIST_UNLOCK_REPLAY_HELP_ALL_FINALES_COMPLETE			27				// TRUE if the replay helkp to use is due to all finale awards being received


// -----------------------------------------------------------------------------------------------------------

// ENUM to identify the Versus Mission GroupID
ENUM g_eHeistRewardGroupID
	HEIST_REWARD_VERSUS_AFTER_TUTORIAL,												// After Tutorial Heist: Unlock Siege Mentality (Rapunzel)
	HEIST_REWARD_VERSUS_AFTER_PRISON,												// After Prison Heist: Unlock Hasta La Vista (Terminator)
	HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS,											// After Humane Labs: Unlock Come Out To Play (Fox and Hounds)
	HEIST_REWARD_VERSUS_AFTER_NARCOTICS,											// After Narcotics Heist: Unlock Siege Mentality again
	HEIST_REWARD_VERSUS_AFTER_ORNATE_BANK,											// After Ornate Bank Heist: Unlock Hasta La Vista again
	
	// Leave this at the bottom
	NO_HEIST_REWARD_GROUP
ENDENUM



// -----------------------------------------------------------------------------------------------------------

// ENUM to the Heist Reward Control Stage
ENUM g_eHeistRewardStage
	HEIST_REWARD_STAGE_NO_REWARD,							// There is currently no active Reward being tracked (ie: not trying to send an invite to an unlocked Versus mission)
	HEIST_REWARD_STAGE_INITIAL_DELAY,						// A delay before trying to invite the player onto the Heist Reward Versus Mission
	HEIST_REWARD_STAGE_SEND_SMS,							// Send the pre-Invite Text Message
	HEIST_REWARD_STAGE_ACTIVE_SMS,							// Wait for the SMS to be completed
	HEIST_REWARD_STAGE_DELAY_AFTER_SMS,						// Wait for the post-SMS delay to expire
	HEIST_REWARD_STAGE_SEND_INVITE,							// Send the Invite to one of the Versus missions
	HEIST_REWARD_STAGE_INVITE_SENT,							// A distribution phase that takes into account whether a second invite should be sent or not
	HEIST_REWARD_STAGE_2nd_INVITE_DELAY,					// A delay before trying to send a second invite to a Heist Reward Versus mission
	HEIST_REWARD_STAGE_SEND_2nd_INVITE,						// Send the Invite to another of the Versus missions
	HEIST_REWARD_STAGE_ALL_INVITES_SENT,					// A holding stage that waits for all of the Versus missions to be played

	// Leave this at the bottom
	HEIST_REWARD_STAGE_ERROR								// A holding variable
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// Delay when no reward processing is required to prevent constant processing
CONST_INT	HEIST_REWARD_SNOOZE_DELAY_msec					30000
// Delay when no reward processing is required but there is an active invite
CONST_INT	HEIST_REWARD_ACTIVE_INVITE_SNOOZE_DELAY_msec	1000
// Delay after a new Heist Reward unlocks before the Invite is sent
CONST_INT	HEIST_REWARD_NEW_UNLOCK_DELAY_msec				(60000 * 2)
// Retry delay if there is a problem getting a Heist Reward unlock
CONST_INT	HEIST_REWARD_RETRY_DELAY_msec					60000
// A delay after sending the SMS before truing to send the Heist Reward Invite
CONST_INT	HEIST_REWARD_POST_SMS_PRE_INVITE_DELAY_msec		30000
// A basic delay if the normal timing rules don't apply for sending the 2nd Heist Versus Mission Invite
CONST_INT	HEIST_REWARD_2nd_INVITE_DEFAULT_DELAY_msec		(60000 * 5)


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing local Heist Unlockable Rewards variables
STRUCT g_structLocalHeistUnlockables
	INT						lhuBitsetStatCopyHeistUnlocks	= HEIST_UNLOCKS_ALL_CLEAR				// A copy of the Heist Unlocks Bitset Stat
	INT						lhuKnownHeistUnlocksBitset		= HEIST_UNLOCKS_ALL_CLEAR				// This will be used to see if lhuBitsetStatCopyHeistUnlocks has changed to re-save the STAT
	g_eHeistRewardStage		lhuStage						= HEIST_REWARD_STAGE_NO_REWARD			// The current Heist Reward Stage
	INT						lhuDelayTimeout					= 0										// A general timeout variable used by different stages
	INT						lhuInitialDelayToUse			= HEIST_REWARD_NEW_UNLOCK_DELAY_msec	// An initial delay if a new Heist Reward needs to be maintained
	BOOL					lhuOnVersusMission				= FALSE									// TRUE if the player is on ANY versus mission, otherwise FALSE
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// STRUCT containing the details of the active invite - so it can be re-added if necessary
// NOTE:	This struct shouldn't get auto-cleared so that the invite can be re-created if necessary when the player enters a new session, even from SP
STRUCT g_structActiveHeistReward
	g_eHeistRewardGroupID	ahrInviteGroupID				= NO_HEIST_REWARD_GROUP					// The reward groupID the player has been invited
	INT						ahrInviteMissionRCID			= 0										// The RCID of the 1st mission the player has been invited to
	INT						ahrInviteMissionRCID_2nd		= 0										// The RCID of the 2nd mission the player has been invited to
	INT						ahrInviteMissionRCID_played		= 0										// The RCID of a played mission - used to prevent dups if the first mission is played before the second invite is sent
ENDSTRUCT




// -----------------------------------------------------------------------------------------------------------
//      Gang Attacks - Client
// -----------------------------------------------------------------------------------------------------------

// Gang Attack Client Stage
ENUM g_eGangAttackClientStage
	GA_CLIENT_STAGE_WAIT_FOR_ON_MISSION,															// Wait for the Player to be actively on a Gang Attack
	GA_CLIENT_STAGE_ON_MISSION,																		// Player is actively on a Gang Attack
	GA_CLIENT_STAGE_REACTIVATE_MISSION																// Player has left the Gang Attack, so delay before re-activating
ENDENUM


// -----------------------------------------------------------------------------------------------------------

//STRUCT g_structCurrentGAClient_Old
//	g_eGangAttackClientStage	gacgacStage				= GA_CLIENT_STAGE_WAIT_FOR_ON_MISSION		// The current stage for the player's Gang Attack
//	TEXT_LABEL_31				gacgacCloudFilename													// The Current Gang Attack Cloud Filename
//	TIME_DATATYPE				gacgacReactivateTimeout												// The Timeout Time when the mission can re-activate (set once the player leaves it)
//ENDSTRUCT

// STRUCT containing Client Control variables for the player's current Gang Attack - Local Variables
STRUCT g_structCurrentGAClient
	g_eGangAttackClientStage	gacgacStage				= GA_CLIENT_STAGE_WAIT_FOR_ON_MISSION		// The current stage for the player's Gang Attack
	TEXT_LABEL_23				gacgacCloudFilename													// The Current Gang Attack Cloud Filename
	TIME_DATATYPE				gacgacReactivateTimeout												// The Timeout Time when the mission can re-activate (set once the player leaves it)
	VECTOR						gacgacAngledAreaMin													// The minimum coords for the trigger area
	VECTOR						gacgacAngledAreaMax													// The maximum coords for the trigger area
	FLOAT						gacgacAngledAreaWidth	= 0.0										// The width of the trigger area
	BOOL						gacgacStillInArea		= FALSE										// TRUE if the Gang Attack ends but the player is still in the trigger area, FALSE once the player has gone
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

//STRUCT g_structGAHistoryClient_Old
//	TEXT_LABEL_31				gahcCloudFilename[MAX_GANG_ATTACK_RECENT_HISTORY]					// An array of recently played Gang Attack filenames - should match Server list
//	INT							gahcNextScheduledHistorySlot										// The next history slot to be compared against the server slot for differences
//ENDSTRUCT


// STRUCT containing Client copy of the Server History List - for locking and unlocking of Gang Attacks
STRUCT g_structGAHistoryClient
	TEXT_LABEL_23				gahcCloudFilename[MAX_GANG_ATTACK_RECENT_HISTORY]					// An array of recently played Gang Attack filenames - should match Server list
	INT							gahcNextScheduledHistorySlot										// The next history slot to be compared against the server slot for differences
ENDSTRUCT




// ===========================================================================================================
//      Freemode Heist External Handshaking Globals
// ===========================================================================================================

// STRUCT containing the Freemode Heist data needed for the InCorona routines to communicate with the Activity Selector
STRUCT g_structASFMHeistMP
	INT					fmhPendingActivitiesArrayPos		= ILLEGAL_PENDING_ACTIVITIES_SLOT		// The array position on the Pending Activities array - allows Activity Selector to identify the chosen mission
	MP_MISSION_ID_DATA	fmhPendingActivitiesMissionIdData											// The mission data in the Pending Activities slot
ENDSTRUCT




// ===========================================================================================================
//      Transition Session Support Globals
// ===========================================================================================================

//STRUCT g_structASFMHeistTransitionMP_Old
//	BOOL					asfmTEMP_KeepClear				= FALSE									// When in the new transition session, don't re-store the contact details as we move towards mission launch
//	g_eFMHeistContactIDs	asfmhtContactID					= NO_FM_HEIST_CONTACT					// The Heist's contactID
//	TEXT_LABEL_31			asfmhtCloudFilename														// The Heist's cloud filename - used to find the mission again in the new session
//ENDSTRUCT


// KGM HEIST TRANSITION VARIABLES USED WHEN THE HEIST IS MOVING PAST THE INVITE STAGE - ENSURES THE SAME HEIST GETS SETUP WHEN THE PLAYERS MOVE TO THE NEW TRANSITION SESSION
// STRUCT containing the information needed to ensure the correct heist gets setup again in the new transition session
// KGM 8/4/13: Intended as TEMP to get transition support in quickly - it will need to be much more robust, probably passing Shared Mission and Pending Activity
//					so it can deal with the controlling player walking out of the corona, etc
// The controlling player stores this data when the SharedRegID is retrieved. After the transition it gets copied into the playerBD variables for the Host to pick up.
STRUCT g_structASFMHeistTransitionMP
	BOOL					asfmTEMP_KeepClear				= FALSE									// When in the new transition session, don't re-store the contact details as we move towards mission launch
	g_eFMHeistContactIDs	asfmhtContactID					= NO_FM_HEIST_CONTACT					// The Heist's contactID
	TEXT_LABEL_23			asfmhtCloudFilename														// The Heist's cloud filename - used to find the mission again in the new session
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Contact Mission Transition Session variables - used to immediately setup the same Contact Mission again when the InCorona sequence moves to the new Transition Session
STRUCT g_structASFMCMTransitionMP
	BOOL					asfmcmtTransitionInProgress		= FALSE									// TRUE if an InCorona transition is in progress when launching a Contact Mission
	TEXT_LABEL_23			asfmcmtCloudFilename													// The Contact Mission's Cloud Filename - used to find the mission again in the new session
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// KGM 22/5/14: I'm going to setup a new similar struct for use by the new Heist routines, but it'll do a similar job
// KGM HEIST TRANSITION VARIABLES USED FOR CROSS-SESSION INVITES SO THAT THE PLAYERS JOINING FROM A DIFFERENT SESSION KNOW WHICH SAFEHOUSE THE HEIST IS IN
// NOTE: This is to solve the problem of the joining player placing the corona at the Mission's own coords - but the heist could be getting triggered from any safehouse
//STRUCT g_structASFMHeistCrossSessionTransitionMP_Old
//	INT						asfmhcstMyPropertyID_Old			= NO_OWNED_SAFEHOUSE				// OBSOLETE - this used to contain the apartment that was due to be transmitted to other players
//	INT						asfmhcstPropertyIdForCorona_Old		= NO_OWNED_SAFEHOUSE				// This will be set to the safehouseID containing the heist corona when this player accepts a cross-session invite or quickmatch to a heist
//ENDSTRUCT




// ===========================================================================================================
//      Server and Player BD structs
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      ServerBD structs, so this one struct can be included in the Global BD data
// -----------------------------------------------------------------------------------------------------------

CONST_INT MAX_FM_HEIST_CONTACTS_OLD		7

STRUCT g_structActivitySelectorServerBD_OLD
	BOOL							assbdCheckForTransitionHeist		= TRUE			// KGM HEIST TRANSITION: TRUE until first contact timer runs out - while TRUE, check playerBD to see if any player was controller of a Heist being setup in this new session
	g_structHeistMissionServer		assbdHeists[MAX_FM_HEIST_CONTACTS_OLD]					// An array of Heist Mission Control structs - one struct per Heist Contact
	g_structContactMissionServer	assbdContactMissions[MAX_FM_HEIST_CONTACTS_OLD]			// An array of Contact Mission Control structs - one struct per Contact
	g_structGangAttackServer_OLD	assbdGangAttacks									// The Gang Attack Control structs
ENDSTRUCT


// NOTE: This is stored as server broadcast data: GlobalServerBD_ActivitySelector.activitySelector
STRUCT g_structActivitySelectorServerBD
	BOOL							assbdCheckForTransitionHeist		= TRUE			// KGM HEIST TRANSITION: TRUE until first contact timer runs out - while TRUE, check playerBD to see if any player was controller of a Heist being setup in this new session
	g_structHeistMissionServer		assbdHeists[MAX_FM_HEIST_CONTACTS]					// An array of Heist Mission Control structs - one struct per Heist Contact
	g_structContactMissionServer	assbdContactMissions[MAX_FM_HEIST_CONTACTS]			// An array of Contact Mission Control structs - one struct per Contact
	g_structGangAttackServer		assbdGangAttacks									// The Gang Attack Control structs
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------
//      PlayerBD structs, so this one struct can be included in the Global BD data
// -----------------------------------------------------------------------------------------------------------

//STRUCT g_structActivitySelectorPlayerBD_OLD
//	g_structHeistGroupClient			aspbHeistGroup										// Variables used to control the Heists as a group rather than as individual contact missions (because only one corona is needed to cover all heists)
//	g_structHeistMissionClient			aspbdHeists[MAX_FM_HEIST_CONTACTS_OLD]					// An array of Heist Mission Control stucts - currently one struct per Heist Contact (but this may be overkill on the client)
//	g_structHeistGeneralClient			aspbHeistGeneral									// Any general Heist data that needs to be player broadcast data
//	g_structASFMHeistTransitionMP_OLD	aspbAfterTransition									// KGM HEIST TRANSITION: Stores identifying Heist data so that the Heist server can retrieve it and setup the correct Heist after moving to the new transition session
//	g_structContactMissionClient_OLD	aspbContactMission									// The current state of any Contact Mission being handled by this player
//ENDSTRUCT

// NOTE: This is stored as player broadcast data: GlobalplayerBD[PlayerID].playerActivitySelector
STRUCT g_structActivitySelectorPlayerBD
	g_structHeistGroupClient		aspbHeistGroup										// Variables used to control the Heists as a group rather than as individual contact missions (because only one corona is needed to cover all heists)
//	g_structHeistMissionClient		aspbdHeists[MAX_FM_HEIST_CONTACTS]					// An array of Heist Mission Control stucts - currently one struct per Heist Contact (but this may be overkill on the client)
	g_structHeistGeneralClient		aspbHeistGeneral									// Any general Heist data that needs to be player broadcast data
	g_structASFMHeistTransitionMP	aspbAfterTransition									// KGM HEIST TRANSITION: Stores identifying Heist data so that the Heist server can retrieve it and setup the correct Heist after moving to the new transition session
	g_structContactMissionClient	aspbContactMission									// The current state of any Contact Mission being handled by this player
ENDSTRUCT
