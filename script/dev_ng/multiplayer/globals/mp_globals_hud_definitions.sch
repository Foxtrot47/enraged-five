USING "commands_graphics.sch"
USING "commands_stats.sch"
USING "commands_pad.sch"
USING "mp_icons.sch"
USING "mp_globals_new_features_TU.sch"
USING "mp_globals_common_definitions.sch"
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_hud_definitions.sch
//      DESCRIPTION     :   Contains constant, enum, and struct definitions for all MP hud routines.
//							NB. Should not contain instantiated variables.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


CONST_INT	DONT_SHOW_TEAM_ON_SCOREBOARD	9999


// KGM OBSOLETE - NEEDS MORE WORK TO REMOVE
CONST_INT CI_DONT_DISPLAY_MISSION_OVERLAY	1
CONST_INT CI_DONT_DISPLAY_RANK_AND_XP_BAR	2


CONST_INT BRONZE_AWARD_XP 100
CONST_INT SILVER_AWARD_XP 200
CONST_INT GOLD_AWARD_XP 400
CONST_INT PLATINUM_AWARD_XP 800

CONST_INT DPAD_DOWN_DISPLAY_TIME			10000
CONST_INT DPAD_DOWN_GAMERTAGS_DISPLAY_TIME	5000

CONST_INT MAX_NUM_INFOBOX_ELEMENTS 16
 
 
ENUM inventory_sprite_name
	MP_SpecItem_BoatPickup,
	MP_SpecItem_Cash,
	MP_SpecItem_Package,
	MP_SpecItem_CuffKeys,
	MP_SpecItem_Data,
	MP_SpecItem_Ped,
	MP_SpecItem_KeyCard,
	MP_SpecItem_RandomObject,
	MP_SpecItem_Remote,
	MP_SpecItem_Safe,
	MP_SpecItem_Weapons,
	MP_SpecItem_control_panel,
	MP_SpecItem_Vehicle,
	MP_SpecItem_Vehicle_Heli,
	MP_SpecItem_Vehicle_Plane,
	MP_SpecItem_Vehicle_Boat
ENDENUM
	
	
ENUM mission_gang_box_sprite_types
	MP_GETAWAYDRIVER,
	MP_COMPUTERHACKER,
	MP_LEADER,
	MP_SNIPER,
	MP_PARACHUTE,
	MP_NO_INVENTORY
ENDENUM
	
	
ENUM inventory_ped_name_following_player
	MP_INVENTORY_CHAR_NAME_NONE,
	MP_INVENTORY_CHAR_NAME_HOOKER_GENERIC,
	MP_INVENTORY_CHAR_NAME_EDGAR,
	MP_INVENTORY_CHAR_NAME_RUBEN,
	MP_INVENTORY_CHAR_NAME_RAY,
	MP_INVENTORY_CHAR_NAME_ROBERTO,
	MP_INVENTORY_CHAR_NAME_HOSTAGE,
	MP_INVENTORY_CHAR_NAME_RAFAEL,
	MP_INVENTORY_CHAR_NAME_BOSS,
	MP_INVENTORY_CHAR_NAME_VIP,
	MP_INVENTORY_CHAR_NAME_ERNIE,
	MP_INVENTORY_CHAR_NAME_INFORMANT,
	MP_INVENTORY_CHAR_NAME_JULIET,
	MP_INVENTORY_CHAR_NAME_WITNESS
ENDENUM
	

ENUM MP_inventory_vehicle_type
	MP_inventory_vehicle_none,
	MP_inventory_vehicle_car, 
	MP_inventory_vehicle_heli,
	MP_inventory_vehicle_plane
ENDENUM
	
	
//Hud States
CONST_INT RANKREWARD_STATE_INI 		0
CONST_INT RANKREWARD_STATE_PROCESS 	1
CONST_INT RANKREWARD_STATE_CLEAN_UP	3


//Bonus Help Text Displayed
CONST_INT BONUS_BUDDY_IN_CAR_X2_XP 0


//Number of duplicate hud elements - x4 timers or x8 scores
CONST_INT MAX_NUMBER_HUD_ELEMENT 10

CONST_FLOAT RANKUP_SCALE_START_MULT	2.0	
CONST_FLOAT REWARD_SCALE_START_MULT	1.5
CONST_FLOAT RANKUP_SCALE_STEP -0.15
CONST_FLOAT REWARD_SCALE_STEP -1.5
CONST_INT RANKUP_ALPHA_STEP 20
CONST_INT RANKUP_ALPHA_OUT_STEP	40
CONST_INT REWARD_ALPHA_STEP	15
CONST_INT REWARD_ALPHA_OUT_STEP	35

CONST_INT MAX_NUM_REWARD_QUEUE 10
CONST_INT MAX_NUM_GLOBALTICKER_QUEUE 10

CONST_FLOAT GENERIC_BOX_X_POSITION 0.150
CONST_FLOAT GENERIC_SLOT_WIDTH 0.200
CONST_FLOAT GENERIC_SLOT_HEIGHT 0.035


ENUM MISSIONCREATORHUD
	MISSIONCREATORHUD_NONE = 0,
	MISSIONCREATORHUD_SAVEANDENTER, 
	MISSIONCREATORHUD_SAVEANDQUIT,
	MISSIONCREATORHUD_QUIT
ENDENUM


// Mission over screen
ENUM MISSION_OVER_STATE
	MOS_OVER = 0,
	MOS_OVER_PASSED,
	MOS_PASSED, 
	MOS_FAILED,
	MOS_CRIME_STOPPED
ENDENUM


ENUM MISSION_OVER_OBJECTS
	MOO_CARS,
	MOO_PACKAGES,
	MOO_POINTS
ENDENUM


ENUM HUDTITLEDISPLAY
	HUDTITLEDISPLAY_TEXT,
	HUDTITLEDISPLAY_PLAYERNAME,
	HUDTITLEDISPLAY_TEAMNAME
ENDENUM


ENUM HUDFLASHING
	HUDFLASHING_NONE = 0,
	HUDFLASHING_FLASHWHITE,
	HUDFLASHING_FLASHRED,
	HUDFLASHING_FLASHGREEN
ENDENUM


ENUM ACTIVITY_POWERUP
	ACTIVITY_POWERUP_NONE = 0,
	ACTIVITY_POWERUP_SPIKES,
	ACTIVITY_POWERUP_ROCKETS,
	ACTIVITY_POWERUP_HOMING_ROCKETS,
	ACTIVITY_POWERUP_BOOSTS,
	ACTIVITY_POWERUP_XP,
	ACTIVITY_POWERUP_TICK,
	ACTIVITY_POWERUP_CROSS,
	ACTIVITY_POWERUP_BEAST,
	ACTIVITY_POWERUP_BULLET,
	ACTIVITY_POWERUP_RANDOM,
	ACTIVITY_POWERUP_SLOW_TIME,
	ACTIVITY_POWERUP_SWAP,
	ACTIVITY_POWERUP_TESTOSTERONE,
	ACTIVITY_POWERUP_THERMAL,
	ACTIVITY_POWERUP_WEED,
	ACTIVITY_POWERUP_HIDDEN,
	ACTIVITY_POWERUP_PED_HEADSHOT,
	ACTIVITY_POWERUP_PED_HEADSHOT_DEAD,
	ACTIVITY_POWERUP_PED_HEADSHOT_ALIVE,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE,
	ACTIVITY_POWERUP_PED_HEADSHOT_FADED,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ZERO_TINT,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_ONE_TINT,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_TWO_TINT,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_THREE_TINT,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FOUR_TINT,
	ACTIVITY_POWERUP_PED_HEADSHOT_SCORE_FIVE_TINT
ENDENUM


ENUM HUD_COUNTER_STYLE
	HUD_COUNTER_STYLE_DEFAULT = 0,
	HUD_COUNTER_STYLE_INPUT_ARROWS
ENDENUM


ENUM HUDORDER
	HUDORDER_NOTDISPLAYING = 0,
	
	//Any non slots information goes in here
	HUDORDER_FREEROAM,
	HUDORDER_DONTCARE,
	
	//strictly slot information.
	HUDORDER_BOTTOM,
	HUDORDER_SECONDBOTTOM,
	HUDORDER_THIRDBOTTOM,
	HUDORDER_FOURTHBOTTOM,
	HUDORDER_FIFTHBOTTOM,
	HUDORDER_SIXTHBOTTOM,
	HUDORDER_SEVENTHBOTTOM,
	HUDORDER_EIGHTHBOTTOM,
	HUDORDER_NINETHBOTTOM,
	HUDORDER_TENTHBOTTOM,
	HUDORDER_TOP
ENDENUM

ENUM HUD_PRIORITY
	HUD_PRIORITY_FIRST = 0,
	HUD_PRIORITY_SECOND,
	HUD_PRIORITY_MAX
ENDENUM

ENUM MD_FLAGS
	MD_FLAGS_NONE = 0,
	MD_FLAGS_IS_CASH = 1,
	MD_FLAGS_SIGNCOLOURED = 2,
	MD_FLAGS_ISCHECKBOX = 4,
	MD_FLAGS_ISMETER = 8,
	MD_FLAGS_ISELIMINATION = 16,
	MD_FLAGS_ISDOUBLEDASH = 32,
	MD_FLAGS_ISINT = 64,
	MD_FLAGS_ISFLOAT = 128,
	MD_FLAGS_ISBOOL = 256,
	MD_FLAGS_ISSTRING = 512,
	MD_FLAGS_ISPERCENT_INT = 1024,
//	MD_FLAGS_SHOWMEDALGOLD = 2048,
//	MD_FLAGS_SHOWMEDALSILVER = 4096,
	MD_FLAGS_IS_INT_TIMER = 8192,
//	MD_FLAGS_IS_PLAYER_IN_REASON = 16384,
	MD_FLAGS_IS_METER_DISTANCE_FLOAT = 32768,
	MD_FLAGS_IS_METER_DISTANCE_INT = 65536,
	MD_FLAGS_IS_MILE_DISTANCE_FLOAT = 131072,
	MD_FLAGS_IS_MILE_DISTANCE_INT = 262144,
	MD_FLAGS_ISPERCENT_FLOAT = 524288,
	MD_FLAGS_ISVALUE_SECONDARY = 1048576,
	MD_FLAGS_MAX_BIT = 2097151 //(2097152-1)
ENDENUM


ENUM UNLOCKTYPEENUM
	UNLOCKTYPEENUM_NONE = 0,
	UNLOCKTYPEENUM_AWARD,
	UNLOCKTYPEENUM_AWARD_PLATINUM,
	UNLOCKTYPEENUM_AWARD_PLATINUM_BOOL,
	UNLOCKTYPEENUM_AWARD_GOLD,
	UNLOCKTYPEENUM_AWARD_SILVER,
	UNLOCKTYPEENUM_AWARD_BRONZE,
	UNLOCKTYPEENUM_MEDAL,
	UNLOCKTYPEENUM_MEDAL_PLATINUM,
	UNLOCKTYPEENUM_MEDAL_GOLD,
	UNLOCKTYPEENUM_MEDAL_SILVER,
	UNLOCKTYPEENUM_MEDAL_BRONZE,
	UNLOCKTYPEENUM_TATTOO,
	UNLOCKTYPEENUM_VEHICLES,
	UNLOCKTYPEENUM_PATCH,
	UNLOCKTYPEENUM_WEAPON,
	UNLOCKTYPEENUM_WEAPON_ADDON,
	UNLOCKTYPEENUM_HEIST,
	UNLOCKTYPEENUM_KIT,
	UNLOCKTYPEENUM_ABILITY,
	UNLOCKTYPEENUM_CARMOD,
	UNLOCKTYPEENUM_HEALTH,
	UNLOCKTYPEENUM_CLOTHES,
	UNLOCKTYPEENUM_BETWON,
	UNLOCKTYPEENUM_BETLOST,
	UNLOCKTYPEENUM_FMHAIR,
	UNLOCKTYPEENUM_ABILITY_FEED,
	UNLOCKTYPEENUM_HEALTH_FEED,
	UNLOCKTYPEENUM_CREWUNLOCK,
	UNLOCKTYPEENUM_UNLOCKS
ENDENUM


ENUM GLOBALTICKERENUM
	GLOBALTICKERENUM_NONE = 0,
	GLOBALTICKERENUM_SOCIALCLUB
ENDENUM


CONST_INT NUMBER_OF_PROGRESSHUD_ELEMENTS 15
ENUM PROGRESSHUD_ELEMENTS
	PROGRESSHUD_INVALID = -1,
	PROGRESSHUD_METER = 0,
	PROGRESSHUD_CHECKPOINT,
	PROGRESSHUD_ELIMINATION,
	PROGRESSHUD_SINGLE_NUMBER,
	PROGRESSHUD_DOUBLE_NUMBER,
	PROGRESSHUD_DOUBLE_NUMBER_PLACE,
	PROGRESSHUD_SCORE,
	PROGRESSHUD_TIMER,
	PROGRESSHUD_WINDMETER,
	PROGRESSHUD_BIG_RACE_POSITION,
	PROGRESSHUD_SPRITE_METER,
	PROGRESSHUD_FOUR_ICON_BAR,
	PROGRESSHUD_FIVE_ICON_SCORE_BAR,
	PROGRESSHUD_SIX_ICON_BAR,
	PROGRESSHUD_DOUBLE_TEXT
ENDENUM

ENUM PERCENTAGE_METER_LINE
	PERCENTAGE_METER_LINE_NONE,
	PERCENTAGE_METER_LINE_10,
	PERCENTAGE_METER_LINE_20,
	PERCENTAGE_METER_LINE_30,
	PERCENTAGE_METER_LINE_40,
	PERCENTAGE_METER_LINE_50,
	PERCENTAGE_METER_LINE_60,
	PERCENTAGE_METER_LINE_70,
	PERCENTAGE_METER_LINE_80,
	PERCENTAGE_METER_LINE_90,
	PERCENTAGE_METER_LINE_ALL_20
ENDENUM


CONST_INT ELIMINATION_BITSET_ISACTIVE1 1
CONST_INT ELIMINATION_BITSET_ISACTIVE2 2
CONST_INT ELIMINATION_BITSET_ISACTIVE3 3
CONST_INT ELIMINATION_BITSET_ISACTIVE4 4
CONST_INT ELIMINATION_BITSET_ISACTIVE5 5
CONST_INT ELIMINATION_BITSET_ISACTIVE6 6
CONST_INT ELIMINATION_BITSET_ISACTIVE7 7
CONST_INT ELIMINATION_BITSET_ISACTIVE8 8


STRUCT INFOBOXDETAIL_INFOSTRUCT
	TEXT_LABEL_63 StatTitle
	
	INT IntStatValue
	TEXT_LABEL_63 StringStatValue
	BOOL BoolStatValue 
	FLOAT FloatStatValue 
	INT IntCurrentValue
	INT IntMaxValue
	HUD_COLOURS CheckpointColour
	HUD_COLOURS SecondaryCheckpointColour
	TEXT_COMPONENT_TIME_FORMAT TimerFormat
	INT EliminationActiveBitset
	INT Flags
ENDSTRUCT


ENUM TOPLEFTTYPES
	TOPLEFTTYPES_NONE = 0,
	TOPLEFTTYPES_MISSIONEND,
//	TOPLEFTTYPES_MISSIONSTART,
	TOPLEFTTYPES_RACEEND
ENDENUM
	
	
STRUCT INFOBOXDETAILSSTRUCT
	TOPLEFTTYPES TopLeftType
//	BOOL isTopLeftActive
	TEXT_LABEL_63 MainTitle
	TEXT_LABEL_63 SubTitle
	TEXT_LABEL_63 BottomText
	TEXT_LABEL_63 SubBottomText
	TEXT_LABEL_63 PlyBottomText
	PODIUMPOS MedalPosition 
	FLOAT fOverallFloatStat
	INT iOverallIntStat
	FLOAT FarEdgeWrap = 0.250
	FLOAT SecondaryWrap = 0.230
	INT NumberOfStats
	INT BronzeTargetTime
	INT SilverTargetTime
	INT GoldTargetTime
	TEXT_LABEL_63 BronzeTargetString
	TEXT_LABEL_63 SilverTargetString
	TEXT_LABEL_63 GoldTargetString
	
	BOOL bDetailsFull = FALSE
	
	INFOBOXDETAIL_INFOSTRUCT InformationStat[MAX_NUM_INFOBOX_ELEMENTS] //~144 
ENDSTRUCT


STRUCT MISSIONOVERSTRUCT
//	MP_MISSION eMission = eNULL_MISSION
//	MISSION_OVER_STATE eState
	BOOL bDisplaying = FALSE
//	INT iTimer = 0
//	INT iAlpha = 0
//	BOOL bShowFailReason
	
	// Score table
//	BOOL bScoretable				// Show it?
//	INT iTeams[MAX_NUM_TEAMS]
//	INT iScores[MAX_NUM_TEAMS]
//	MISSION_OVER_OBJECTS eObjects	
ENDSTRUCT


STRUCT HUDELEMENT_WINDMETER_STRUCT
	//------------------------------------------------
	//Globals for the Wind Meter hud
	//------------------------------------------------/
	FLOAT fGenericMeter_Heading[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_63 sGenericMeter_Title[MAX_NUMBER_HUD_ELEMENT]
	
	INT iGenericMeter_WindSpeed[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericMeter_RedComponent[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericMeter_BlueComponent[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericMeter_GreenComponent[MAX_NUMBER_HUD_ELEMENT]
	
	HUDORDER eGenericMeter_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT


STRUCT HUDELEMENT_BIG_RACE_POSITION_STRUCT
	//------------------------------------------------
	//Globals for the Big Race Position hud
	//------------------------------------------------/
	INT eGenericBigRacePos_iRacePosition[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS eGenericBigRacePos_eRacePositionHUDColour[MAX_NUMBER_HUD_ELEMENT]
	
	HUDORDER eGenericBigRacePos_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT


STRUCT HUDELEMENT_FOUR_ICON_BAR_STRUCT
	//------------------------------------------------
	//Globals for the Four Icon Bar hud
	//------------------------------------------------/
	HUD_COLOURS eGenericFourIconBar_TitleColour[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericFourIconBar_pPlayerOne[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericFourIconBar_pPlayerTwo[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericFourIconBar_pPlayerThree[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericFourIconBar_pPlayerFour[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericFourIconBar_aPowerupOne[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericFourIconBar_aPowerupTwo[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericFourIconBar_aPowerupThree[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericFourIconBar_aPowerupFour[MAX_NUMBER_HUD_ELEMENT]
	HUDORDER eGenericFourIconBar_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericFourIconBar_bFlashIconOne[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericFourIconBar_bFlashIconTwo[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericFourIconBar_bFlashIconThree[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericFourIconBar_bFlashIconFour[MAX_NUMBER_HUD_ELEMENT]
	INT eGenericFourIconBar_iFlashTime[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT

STRUCT HUDELEMENT_FIVE_ICON_SCORE_BAR_STRUCT
	//------------------------------------------------
	//Globals for the Five Icon Score Bar hud
	//------------------------------------------------/
	INT eGenericFiveIconScoreBar_Number[MAX_NUMBER_HUD_ELEMENT]
	INT eGenericFiveIconScoreBar_MaXNumber[MAX_NUMBER_HUD_ELEMENT]
	FLOAT eGenericFiveIconScoreBar_FloatValue[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_23 eGenericFiveIconScoreBar_NumberString[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericFiveIconScoreBar_isFloat[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericFiveIconScoreBar_DrawInfinity[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS eGenericFiveIconScoreBar_TitleColour[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericFiveIconScoreBar_pPlayerOne[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericFiveIconScoreBar_pPlayerTwo[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericFiveIconScoreBar_pPlayerThree[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericFiveIconScoreBar_pPlayerFour[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericFiveIconScoreBar_pPlayerFive[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericFiveIconScoreBar_aPowerupOne[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericFiveIconScoreBar_aPowerupTwo[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericFiveIconScoreBar_aPowerupThree[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericFiveIconScoreBar_aPowerupFour[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericFiveIconScoreBar_aPowerupFive[MAX_NUMBER_HUD_ELEMENT]
	HUDORDER eGenericFiveIconScoreBar_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericFiveIconScoreBar_pPlayerToHighlight[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericFiveIconScoreBar_bEnablePlayerHighlight[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS eGenericFiveIconScoreBar_ePowerupOneColour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS eGenericFiveIconScoreBar_ePowerupTwoColour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS eGenericFiveIconScoreBar_ePowerupThreeColour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS eGenericFiveIconScoreBar_ePowerupFourColour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS eGenericFiveIconScoreBar_ePowerupFiveColour[MAX_NUMBER_HUD_ELEMENT]
	INT eGenericFiveIconScoreBar_iInstanceToHighlight[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericFiveIconScoreBar_bPulseHighlight[MAX_NUMBER_HUD_ELEMENT]
	INT eGenericFiveIconScoreBar_iPulseTime[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericFiveIconScoreBar_pAvatarToFlash[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericFiveIconScoreBar_bFlashAvatar[MAX_NUMBER_HUD_ELEMENT]
	INT eGenericFiveIconScoreBar_iAvatarFlashTime[MAX_NUMBER_HUD_ELEMENT]
	INT eGenericFiveIconScoreBar_iAvatarSlotToFlash[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT

STRUCT HUDELEMENT_SIX_ICON_BAR_STRUCT
	//------------------------------------------------
	//Globals for the Six Icon Bar hud
	//------------------------------------------------/
	HUD_COLOURS eGenericSixIconBar_TitleColour[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericSixIconBar_pPlayerOne[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericSixIconBar_pPlayerTwo[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericSixIconBar_pPlayerThree[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericSixIconBar_pPlayerFour[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericSixIconBar_pPlayerFive[MAX_NUMBER_HUD_ELEMENT]
	PLAYER_INDEX eGenericSixIconBar_pPlayerSix[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericSixIconBar_aPowerupOne[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericSixIconBar_aPowerupTwo[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericSixIconBar_aPowerupThree[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericSixIconBar_aPowerupFour[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericSixIconBar_aPowerupFive[MAX_NUMBER_HUD_ELEMENT]
	ACTIVITY_POWERUP eGenericSixIconBar_aPowerupSix[MAX_NUMBER_HUD_ELEMENT]
	HUDORDER eGenericSixIconBar_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericSixIconBar_bFlashIconOne[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericSixIconBar_bFlashIconTwo[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericSixIconBar_bFlashIconThree[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericSixIconBar_bFlashIconFour[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericSixIconBar_bFlashIconFive[MAX_NUMBER_HUD_ELEMENT]
	BOOL eGenericSixIconBar_bFlashIconSix[MAX_NUMBER_HUD_ELEMENT]
	INT eGenericSixIconBar_iFlashTime[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT

STRUCT HUDELEMENT_METER_STUCT
	//------------------------------------------------
	//Globals for the Generic Meter hud
	//------------------------------------------------/
	//IF YOU ADD ANY HERE, PUT IT INTO RESET - OPTIMISATION REASONS> 
	INT iGenericMeter_Number[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericMeter_MaxNumber[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_63 sGenericMeter_Title[MAX_NUMBER_HUD_ELEMENT]
	
	INT iGenericMeter_FlashTimer[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericMeter_Colour[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericMeter_TitleNumber[MAX_NUMBER_HUD_ELEMENT]
	
	HUDORDER GenericMeter_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	
	VECTOR GenericMeter_FreeRoamPos[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericMeter_bIsPlayer[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericMeter_bOnlyZeroIsEmpty[MAX_NUMBER_HUD_ELEMENT]
		
	SCRIPT_TIMER iGenericMeter_ExtendedStartTimer[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericMeter_ExtendedTimer[MAX_NUMBER_HUD_ELEMENT]

	HUDFLASHING GenericMeter_ColourFlashType[MAX_NUMBER_HUD_ELEMENT]
	INT GenericMeter_ColourFlash[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericMeter_bBigMeter[MAX_NUMBER_HUD_ELEMENT]
	INT GenericMeter_iDrawRedDangerZonePercent[MAX_NUMBER_HUD_ELEMENT] // If non-zero, adds a red bar that fills the percent specified (0 - 100)
	BOOL GenericMeter_bIsLiteralString[MAX_NUMBER_HUD_ELEMENT]
	PERCENTAGE_METER_LINE GenericMeter_PercentageLine[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericMeter_FleckColour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericMeter_TextColour[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericMeter_bDrawLineUnderName[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericMeter_LineUnderNameColour[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericMeter_MakeBarUrgent[MAX_NUMBER_HUD_ELEMENT]
	INT GenericMeter_iUrgentPercentage[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS PulseToColour[MAX_NUMBER_HUD_ELEMENT]
	INT iPulseTime[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL bUseScoreTitle[MAX_NUMBER_HUD_ELEMENT]
	FLOAT GenericMeter_fNumber[MAX_NUMBER_HUD_ELEMENT]
	FLOAT GenericMeter_fMaxNumber[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericMeter_bUseSecondaryBar[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericMeter_eSecondaryBarColour[MAX_NUMBER_HUD_ELEMENT]
	FLOAT GenericMeter_fSecondaryBarPercentage[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericMeter_bTransparentSecBarIntersectingMainBar[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS GenericMeter_eSecBarPulseToColour[MAX_NUMBER_HUD_ELEMENT]
	INT GenericMeter_iSecBarPulseTime[MAX_NUMBER_HUD_ELEMENT]
	
	FLOAT GenericMeter_fSecBarStartPercentage[MAX_NUMBER_HUD_ELEMENT]
	INT GenericMeter_iGFXDrawOrder[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericMeter_bCapMaxPercentage[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT



STRUCT HUDELEMENT_SPRITE_METER_STUCT

	TEXT_LABEL_23 sGenericMeter_DictionaryName
	//------------------------------------------------
	//Globals for the Generic Sprite Meter hud
	//------------------------------------------------/
	//IF YOU ADD ANY HERE, PUT IT INTO RESET - OPTIMISATION REASONS> 
	INT iGenericMeter_Number[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericMeter_MaxNumber[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_63 sGenericMeter_Title[MAX_NUMBER_HUD_ELEMENT]
	
	INT iGenericMeter_FlashTimer[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericMeter_Colour[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericMeter_TitleNumber[MAX_NUMBER_HUD_ELEMENT]
	
	HUDORDER GenericMeter_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericMeter_bIsPlayer[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericMeter_bOnlyZeroIsEmpty[MAX_NUMBER_HUD_ELEMENT]
		
	SCRIPT_TIMER iGenericMeter_ExtendedStartTimer[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericMeter_ExtendedTimer[MAX_NUMBER_HUD_ELEMENT]

	HUDFLASHING GenericMeter_ColourFlashType[MAX_NUMBER_HUD_ELEMENT]
	INT GenericMeter_ColourFlash[MAX_NUMBER_HUD_ELEMENT]
	
	TEXT_LABEL_63 GenericMeter_SpriteName[MAX_NUMBER_HUD_ELEMENT]
	

	BOOL GenericMeter_bIsLiteralString[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericMeter_FleckColour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericMeter_TextColour[MAX_NUMBER_HUD_ELEMENT]


ENDSTRUCT



STRUCT HUDELEMENT_CHECKPOINT_STUCT
	//------------------------------------------------
	//Globals for the Generic Checkpoint hud
	//------------------------------------------------/
	//IF YOU ADD ANY HERE, PUT IT INTO RESET - OPTIMISATION REASONS> 
	INT iGenericCheckpoint_Number[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericCheckpoint_MaxNumber[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_63 sGenericCheckpoint_Title[MAX_NUMBER_HUD_ELEMENT]
	
	INT iGenericCheckpoint_FlashTimer[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericCheckpoint_Colour[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericCheckpoint_TitleNumber[MAX_NUMBER_HUD_ELEMENT]
	
	HUDORDER GenericCheckpoint_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	
	VECTOR GenericCheckpoint_FreeRoamPos[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericCheckpoint_bIsPlayer[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericCheckpoint_iInBuiltMultiplyer[MAX_NUMBER_HUD_ELEMENT]
		
	SCRIPT_TIMER iGenericCheckpoint_ExtendedStartTimer[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericCheckpoint_ExtendedTimer[MAX_NUMBER_HUD_ELEMENT]
	
	HUDFLASHING GenericCheckpoint_ColourFlashType[MAX_NUMBER_HUD_ELEMENT]
	INT GenericCheckpoint_ColourFlash[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericCheckpoint_FleckColour[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT


STRUCT HUDELEMENT_ELIMINATION_STUCT
	//------------------------------------------------
	//Globals for the Generic Elimination hud
	//------------------------------------------------/
	//IF YOU ADD ANY HERE, PUT IT INTO RESET - OPTIMISATION REASONS> 
	INT iGenericElimination_MaxNumber[MAX_NUMBER_HUD_ELEMENT]
	BOOL bGenericElimination_IsActive1[MAX_NUMBER_HUD_ELEMENT]
	BOOL bGenericElimination_IsActive2[MAX_NUMBER_HUD_ELEMENT]
	BOOL bGenericElimination_IsActive3[MAX_NUMBER_HUD_ELEMENT]
	BOOL bGenericElimination_IsActive4[MAX_NUMBER_HUD_ELEMENT]
	BOOL bGenericElimination_IsActive5[MAX_NUMBER_HUD_ELEMENT]
	BOOL bGenericElimination_IsActive6[MAX_NUMBER_HUD_ELEMENT]
	BOOL bGenericElimination_IsActive7[MAX_NUMBER_HUD_ELEMENT]
	BOOL bGenericElimination_IsActive8[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_63 sGenericElimination_Title[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericElimination_VisibleBoxes[MAX_NUMBER_HUD_ELEMENT]
	
	INT iGenericElimination_FlashTimer[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericElimination_ColourFirst[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericElimination_ColourSecond[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericElimination_TitleNumber[MAX_NUMBER_HUD_ELEMENT]
	
	HUDORDER GenericElimination_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	
	VECTOR GenericElimination_FreeRoamPos[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericElimination_bIsPlayer[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS GenericElimination_Box1Colour[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box2Colour[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box3Colour[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box4Colour[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box5Colour[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box6Colour[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box7Colour[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box8Colour[MAX_NUMBER_HUD_ELEMENT] 
	
	HUD_COLOURS GenericElimination_Box1Colour_InActive[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box2Colour_InActive[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box3Colour_InActive[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box4Colour_InActive[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box5Colour_InActive[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box6Colour_InActive[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box7Colour_InActive[MAX_NUMBER_HUD_ELEMENT] 
	HUD_COLOURS GenericElimination_Box8Colour_InActive[MAX_NUMBER_HUD_ELEMENT] 
	
	HUD_COLOURS GenericElimination_TitleColour[MAX_NUMBER_HUD_ELEMENT] 
		
	SCRIPT_TIMER iGenericElimination_ExtendedStartTimer[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericElimination_ExtendedTimer[MAX_NUMBER_HUD_ELEMENT]
	
	HUDFLASHING GenericElimination_ColourFlashType[MAX_NUMBER_HUD_ELEMENT]
	INT GenericElimination_ColourFlash[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericElimination_bUseNonPlayerFont[MAX_NUMBER_HUD_ELEMENT] 
	
	HUD_COLOURS GenericElimination_FleckColour[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS GenericElimination_Cross0Colour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericElimination_Cross1Colour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericElimination_Cross2Colour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericElimination_Cross3Colour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericElimination_Cross4Colour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericElimination_Cross5Colour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericElimination_Cross6Colour[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericElimination_Cross7Colour[MAX_NUMBER_HUD_ELEMENT]
	
ENDSTRUCT


STRUCT UNLOCKDETAILS
	UNLOCKTYPEENUM Unlock_Type
	TEXT_LABEL_63 Unlock_Texture_Dictionary
	TEXT_LABEL_63 Unlock_Texture_Name
	TEXT_LABEL_63 Unlock_Title
	TEXT_LABEL_63 Unlock_Description
	TEXT_LABEL_63 Unlock_Addon_Texture_Dictionary
	TEXT_LABEL_63 Unlock_Addon_Texture_Name
	INT Number
	TEXT_LABEL_23 Unlock_Player_Name
	INT  Unlock_DisplayTicker
	BOOL Unlock_isatshirt
ENDSTRUCT


STRUCT GLOBALTICKERDETAIL
	GLOBALTICKERENUM Ticker_Type
	TEXT_LABEL_63 Ticker_Title
ENDSTRUCT


ENUM TIMER_STYLE
	TIMER_STYLE_DONTUSEMILLISECONDS = 0,
	TIMER_STYLE_USEMILLISECONDS,
	TIMER_STYLE_CLOCKAM,
	TIMER_STYLE_CLOCKPM,
	TIMER_STYLE_STUNTPLANE,
	TIMER_STYLE_ONLY_SECONDS
ENDENUM


STRUCT HUDELEMENT_TIMER_STUCT
	//------------------------------------------------
	//Globals for the Timer hud
	//------------------------------------------------/
	//IF YOU ADD ANY HERE, PUT IT INTO RESET - OPTIMISATION REASONS> 
	INT iGenericTimer_Timer[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_63 sGenericTimer_TimerTitle[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericTimer_ExtraTime[MAX_NUMBER_HUD_ELEMENT]
	
	INT iGenericTimer_FlashTimer[MAX_NUMBER_HUD_ELEMENT]
	PODIUMPOS iGenericTimer_MedalDisplay[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericTimer_Colour[MAX_NUMBER_HUD_ELEMENT]
	TIMER_STYLE bGenericTimer_TimerStyle[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericTimer_TitleNumber[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericTimer_TitleColour[MAX_NUMBER_HUD_ELEMENT]
	
	HUDORDER GenericTimer_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericTimer_bIsPlayer[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericTimer_bIsLiteral[MAX_NUMBER_HUD_ELEMENT]
	
	SCRIPT_TIMER iGenericTimer_ExtendedStartTimer[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericTimer_ExtendedTimer[MAX_NUMBER_HUD_ELEMENT]
	
	HUDFLASHING GenericTimer_ColourFlashType[MAX_NUMBER_HUD_ELEMENT]
	INT GenericTimer_ColourFlash[MAX_NUMBER_HUD_ELEMENT]

	SCRIPT_TIMER iGenericTimer_ExtraTimeTimer[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericTimer_bDisplayAsDashes[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS GenericTimer_FleckColour[MAX_NUMBER_HUD_ELEMENT]
	
	ACTIVITY_POWERUP GenericTimer_Powerup[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericTimer_bHideUnusedZeros[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT


STRUCT HUDELEMENT_SINGLE_NUMBER_STUCT
	//------------------------------------------------
	//Globals for the Number hud
	//------------------------------------------------/
	//IF YOU ADD ANY HERE, PUT IT INTO RESET - OPTIMISATION REASONS> 
	INT iGenericNumber_Number[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_63 sGenericNumber_NumberTitle[MAX_NUMBER_HUD_ELEMENT]
	
	INT iGenericNumber_FlashTimer[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericNumber_Colour[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericNumber_TitleNumber[MAX_NUMBER_HUD_ELEMENT]
	
	HUDORDER GenericNumber_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericNumber_bIsPlayer[MAX_NUMBER_HUD_ELEMENT]

	SCRIPT_TIMER iGenericNumber_ExtendedStartTimer[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericNumber_ExtendedTimer[MAX_NUMBER_HUD_ELEMENT]

	TEXT_LABEL_63 sGenericNumber_NumberString[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS sGenericNumber_TitleColour[MAX_NUMBER_HUD_ELEMENT]
	
	HUDFLASHING GenericNumber_ColourFlashType[MAX_NUMBER_HUD_ELEMENT]
	INT GenericNumber_ColourFlash[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL bGenericNumber_DrawInfinity[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS GenericNumber_FleckColour[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericNumber_EnablePulsing[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericNumber_PulseColour[MAX_NUMBER_HUD_ELEMENT]
	INT GenericNumber_PulseTime[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT


STRUCT HUDELEMENT_SCORE_STUCT
	//------------------------------------------------
	//Globals for the Score hud
	//------------------------------------------------/
	//IF YOU ADD ANY HERE, PUT IT INTO RESET - OPTIMISATION REASONS> 
	INT iGenericScore_Number[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_63 sGenericScore_Title[MAX_NUMBER_HUD_ELEMENT]
	
	INT iGenericScore_FlashTimer[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericScore_Colour[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericScore_TitleNumber[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS GenericScore_TitleColour[MAX_NUMBER_HUD_ELEMENT]

	BOOL GenericScore_DisplayWarning[MAX_NUMBER_HUD_ELEMENT]
	INT GenericScore_MaxNumber[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericScore_DrawInfinity[MAX_NUMBER_HUD_ELEMENT]
	
	ACTIVITY_POWERUP GenericScore_Powerup[MAX_NUMBER_HUD_ELEMENT]
	
	HUDORDER GenericScore_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericScore_bIsPlayer[MAX_NUMBER_HUD_ELEMENT]
	
	TEXT_LABEL_63 sGenericScore_NumberString[MAX_NUMBER_HUD_ELEMENT]
	BOOL bGenericScore_IsFloat[MAX_NUMBER_HUD_ELEMENT]
	FLOAT bGenericScore_FloatValue[MAX_NUMBER_HUD_ELEMENT]
	
	SCRIPT_TIMER iGenericScore_ExtendedStartTimer[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericScore_ExtendedTimer[MAX_NUMBER_HUD_ELEMENT]

	HUDFLASHING GenericScore_ColourFlashType[MAX_NUMBER_HUD_ELEMENT]
	INT GenericScore_ColourFlash[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COUNTER_STYLE GenericScore_Style[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericScore_bIsLiteralTitle[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS GenericScore_FleckColour[MAX_NUMBER_HUD_ELEMENT]
	
	INT GenericScore_iAlpha[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericScore_bDisplayBlankScore[MAX_NUMBER_HUD_ELEMENT]
	
	PLAYER_INDEX GenericScore_pPlayerID[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericScore_bFlashTitle[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericScore_bDrawLineUnderName[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericScore_LineUnderNameColour[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT


STRUCT HUDELEMENT_DOUBLE_NUMBER_STUCT
	//------------------------------------------------
	//Globals for the Double Number hud
	//------------------------------------------------/
	//IF YOU ADD ANY HERE, PUT IT INTO RESET - OPTIMISATION REASONS> 
	INT iGenericDoubleNumber_Number[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_63 sGenericDoubleNumber_Title[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericDoubleNumber_NumberTwo[MAX_NUMBER_HUD_ELEMENT]
	
	INT iGenericDoubleNumber_FlashTimer[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericDoubleNumber_COLOUR[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericDoubleNumber_TitleNumber[MAX_NUMBER_HUD_ELEMENT]
	
	HUDORDER GenericDoubleNumber_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericDoubleNumber_bIsPlayer[MAX_NUMBER_HUD_ELEMENT]
	
	SCRIPT_TIMER iGenericDoubleNumber_ExtendedStartTimer[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericDoubleNumber_ExtendedTimer[MAX_NUMBER_HUD_ELEMENT]

	HUDFLASHING GenericDoubleNumber_ColourFlashType[MAX_NUMBER_HUD_ELEMENT]
	INT GenericDoubleNumber_ColourFlash[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericDoubleNumber_bDisplayWarning[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericDoubleNumber_bUseNonPlayerFont[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS GenericDoubleNumber_TitleCOLOUR[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS GenericDoubleNumber_FleckColour[MAX_NUMBER_HUD_ELEMENT]
	INT GenericDoubleNumber_iAlpha[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericDoubleNumber_bFlashTitle[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT


STRUCT HUDELEMENT_DOUBLE_NUMBER_PLACE_STUCT
	//------------------------------------------------
	//Globals for the Double Number with Placement hud
	//------------------------------------------------/
	//IF YOU ADD ANY HERE, PUT IT INTO RESET - OPTIMISATION REASONS> 
	INT iGenericDoubleNumberPlace_Number[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_63 sGenericDoubleNumberPlace_Title[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericDoubleNumberPlace_NumberTwo[MAX_NUMBER_HUD_ELEMENT]
	
	INT iGenericDoubleNumberPlace_FlashTimer[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericDoubleNumberPlace_COLOUR[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericDoubleNumberPlace_TitleNumber[MAX_NUMBER_HUD_ELEMENT]
	
	HUDORDER GenericDoubleNumberPlace_HUDOrder[MAX_NUMBER_HUD_ELEMENT]	
	BOOL GenericDoubleNumberPlace_bIsPlayer[MAX_NUMBER_HUD_ELEMENT]
	
	SCRIPT_TIMER iGenericDoubleNumberPlace_ExtendedStartTimer[MAX_NUMBER_HUD_ELEMENT]
	INT iGenericDoubleNumberPlace_ExtendedTimer[MAX_NUMBER_HUD_ELEMENT]

	HUDFLASHING GenericDoubleNumberPlace_ColourFlashType[MAX_NUMBER_HUD_ELEMENT]
	INT GenericDoubleNumberPlace_ColourFlash[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS GenericDoubleNumberPlace_TitleCOLOUR[MAX_NUMBER_HUD_ELEMENT]
	
	HUD_COLOURS GenericDoubleNumberPlace_FleckColour[MAX_NUMBER_HUD_ELEMENT]
	
	BOOL GenericDoubleNumberPlace_bCustomFont[MAX_NUMBER_HUD_ELEMENT]
	TEXT_FONTS GenericDoubleNumberPlace_eCustomFont[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT

STRUCT HUDELEMENT_DOUBLE_TEXT_STUCT
	TEXT_LABEL_63 sGenericDoubleText_TitleLeft[MAX_NUMBER_HUD_ELEMENT]
	TEXT_LABEL_63 sGenericDoubleText_TitleRight[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericDoubleText_bTitleLeftLiteral[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericDoubleText_bTitleRightLiteral[MAX_NUMBER_HUD_ELEMENT]
	HUDORDER GenericDoubleText_HUDOrder[MAX_NUMBER_HUD_ELEMENT]
	HUD_COLOURS GenericDoubleText_TitleCOLOUR[MAX_NUMBER_HUD_ELEMENT]
	BOOL GenericDoubleText_bCustomFont[MAX_NUMBER_HUD_ELEMENT]
	TEXT_FONTS GenericDoubleText_eCustomFont[MAX_NUMBER_HUD_ELEMENT]
ENDSTRUCT

ENUM DPADDOWN_ACTIVATION
	DPADDOWN_NONE = 0,
	DPADDOWN_FIRST,
	DPADDOWN_SECOND
ENDENUM


STRUCT DISPLAYOVERHEADSTRUCT
	INT iOverheadDisplayBitset[OH_BITSET_NUM][NUM_NETWORK_PLAYERS]
	INT iOverheadDisplayBitset_LastFrame[OH_BITSET_NUM][NUM_NETWORK_PLAYERS]

	INT iOverheadLogicBitset[OH_BITSET_LOGIC_NUM][NUM_NETWORK_PLAYERS]
	INT iOverheadLogicBitset_LastFrame[OH_BITSET_LOGIC_NUM][NUM_NETWORK_PLAYERS]

	INT iOverheadEVENTBitset[OH_BITSET_EVENT_NUM][NUM_NETWORK_PLAYERS]
	INT iOverheadEVENTBitset_LastFrame[OH_BITSET_EVENT_NUM][NUM_NETWORK_PLAYERS]
	INT iOverheadPlayerRoleCheck[NUM_NETWORK_PLAYERS]
	INT iOverheadHeistRoleCheck[NUM_NETWORK_PLAYERS]
	
	INT iOverheadRemoteTaggedPlayerBroadcasted_Bitset

	
	INT iOverhead_iPlayerWantedLevel_LastFrame[NUM_NETWORK_PLAYERS]
	INT iOverhead_iPlayerFakeWantedLevel_LastFrame[NUM_NETWORK_PLAYERS]
	INT iOverhead_iPlayerInventory_LastFrame[NUM_NETWORK_PLAYERS]
	INT iOverhead_iTeamNum_LastFrame[NUM_NETWORK_PLAYERS]
	
	BOOL iOverhead_OnDM_LastFrame[NUM_NETWORK_PLAYERS]
	BOOL iOverhead_OnRace_LastFrame[NUM_NETWORK_PLAYERS]
	BOOL iOverhead_OnMission_LastFrame[NUM_NETWORK_PLAYERS]
	
	BOOL iOverhead_IsBlipped_LastFrame[NUM_NETWORK_PLAYERS]
	INT iOverhead_inventory_LastFrame[NUM_NETWORK_PLAYERS]
	
	BOOL bOverhead_AllOverheadActive_LastFrame[NUM_NETWORK_PLAYERS]
	BOOL bOverhead_ThisPlayerOverheadActive_LastFrame[NUM_NETWORK_PLAYERS]
	
	DPADDOWN_ACTIVATION eDpadDownStateCopy[NUM_NETWORK_PLAYERS]
	
	BOOL bNeedsFmCleanupCopy[NUM_NETWORK_PLAYERS]
	GB_GANG_ROLE iOverhead_iPlayerGANG_ROLE_LastFrame[NUM_NETWORK_PLAYERS]
	INT iOverhead_iPlayerLargepackages_LastFrame[NUM_NETWORK_PLAYERS]
	INT iOverhead_iPatricipantIDPackage_transmittor_LastFrame[NUM_NETWORK_PLAYERS]
// Graeme moved these three to DISPLAYOVERHEADSTRUCT_TitleUpdate	
//	BOOL bLeaderboardCamActiveCopy[NUM_NETWORK_PLAYERS]
//	BOOL bAtArrowNotTagDistance[NUM_NETWORK_PLAYERS]
//	BOOL bOnEndRaceScreen[NUM_NETWORK_PLAYERS]
ENDSTRUCT


STRUCT TRANSITION_HUDCOGS
	INT iSelectedCharacter = 0
	INT iSelection = 1
	INT iSelectedChild = 0
	INT iSelectionY = 0 //Used for Gallery
	BOOL bTurnOnInfo
	INT iSelectedPage
	
	INT iSelectedCategory
	INT iSelectedWhichMenu

	//Used for hints
	SCRIPT_TIMER iHUDScreenTimer
	INT DisplayWhichSubScreen = 1
	INT DisplayWhichOldSubScreen = 1
ENDSTRUCT


CONST_INT MAX_SCALEFORM_GALLERY_WEAPON_ITEMS 9
CONST_INT MAX_SCALEFORM_GALLERY_WEAPON_ADDONS 9
STRUCT SCALEFORM_GALLERY_WEAPON
	BOOL bInitialisedGalleryVisuals
	
	BOOL bHasValue[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	
	TEXT_LABEL_15 TitleLabel[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS] 
	TEXT_LABEL_15 DescriptionLabel[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS] 
	TEXT_LABEL_31 TextureName[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS] 
	TEXT_LABEL_31 TextureDict[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS] 
	TEXT_LABEL_31 TextureSillouetteName[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS] 
	
	INT KillsValue[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	INT DeathsValue[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	INT RatingValue[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	FLOAT KillsVsDeathsValue[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	INT PowerValue[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	INT SpeedPercent[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	INT CapacityPercent[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	INT AccuracyVSPercent[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	
	BOOL isLocked[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	BOOL HasEverHeld[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	
	TEXT_LABEL_31 AddonTextureName[MAX_SCALEFORM_GALLERY_WEAPON_ADDONS][MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	
	SCALEFORM_INPUT_EVENT ScaleformInputEventNumber
	INT iHighlightBlock
	BOOL InitialiseScaleformGalleryInputs
	BOOL InitialiseScaleformGalleryHighlights
	BOOL bRefreshScaleformGalleryVisuals
	BOOL bRefreshScaleformGalleryInputs
	BOOL bRefreshScaleformGalleryHighlights 
ENDSTRUCT


CONST_INT MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS 9
CONST_INT MAX_SCALEFORMXML_GALLERY_WEAPON_ADDONS 36
STRUCT SCALEFORMXML_GALLERY_WEAPON
	BOOL bInitialisedGalleryVisuals
	
	BOOL bHasValue[MAX_SCALEFORM_GALLERY_WEAPON_ITEMS]
	
	TEXT_LABEL_15 TitleLabel[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS] 
	TEXT_LABEL_15 DescriptionLabel[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS] 
//	TEXT_LABEL_31 TextureName[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS] 
//	TEXT_LABEL_31 TextureDict[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS] 
//	TEXT_LABEL_31 TextureSillouetteName[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS] 
	WEAPON_TYPE aWeapon[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	
	INT KillsValue[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	INT DeathsValue[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	FLOAT MyAccuracy[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	INT Headshots[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]

	FLOAT KillsVsDeathsValue[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	INT PowerValue[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	INT SpeedPercent[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	INT CapacityPercent[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	INT RangePercent[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	INT AccuracyVSPercent[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	
	
	//-- Weapom components 
	INT compDamage[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	INT compSpeed[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	INT compCapacity[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	INT compAccuracy[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	INT compRange[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	
	BOOL isLocked[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	BOOL HasEverHeld[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	
	WEAPON_TYPE aScaleformWeapon[MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	
//	TEXT_LABEL_31 AddonTextureName[MAX_SCALEFORMXML_GALLERY_WEAPON_ADDONS][MAX_SCALEFORMXML_GALLERY_WEAPON_ITEMS]
	
	SCALEFORM_INPUT_EVENT ScaleformInputEventNumber
	INT SELECTEDCHAR
	INT iHighlightBlock
	BOOL InitialiseScaleformGalleryInputs
	BOOL InitialiseScaleformGalleryHighlights
	BOOL InitialiseScaleformGalleryDescription
	BOOL bRefreshScaleformGalleryVisuals
	BOOL bRefreshScaleformGalleryInputs
	BOOL bRefreshScaleformGalleryHighlights 
	BOOL bRefreshScaleformGalleryDescription
	
	INT ButtonReleasedBitset
ENDSTRUCT


CONST_INT MAX_SCALEFORMXML_TEXT_SELECTION 7
STRUCT SCALEFORMXML_TEXT_SELECTION
	BOOL bInitialisedVisuals
	
	BOOL bHasValue[MAX_SCALEFORMXML_TEXT_SELECTION]
	TEXT_LABEL_15 TitleLabel[MAX_SCALEFORMXML_TEXT_SELECTION] 
	BOOL isLocked[MAX_SCALEFORMXML_TEXT_SELECTION]
	BOOL IsNoCloud[MAX_SCALEFORMXML_TEXT_SELECTION]
	BOOL bHasExtraNumber[MAX_SCALEFORMXML_TEXT_SELECTION]
	INT iExtraNumber[MAX_SCALEFORMXML_TEXT_SELECTION]
	
	BOOL IsNoCloudLastFrame
	
	INT iHighlightBlock
	BOOL InitialiseScaleformXMLHighlights
	BOOL bRefreshScaleformXMLVisuals
	BOOL bRefreshScaleformXMLHighlights 
ENDSTRUCT


CONST_INT MAX_SCALEFORMXML_GALLERY_ITEMS 12
STRUCT SCALEFORMXML_GALLERY
	BOOL bInitialisedGalleryVisuals
	
	BOOL bHasValue[MAX_SCALEFORMXML_GALLERY_ITEMS]
	TEXT_LABEL_31 TitleLabel[MAX_SCALEFORMXML_GALLERY_ITEMS] 
	TEXT_LABEL_31 DescriptionLabel[MAX_SCALEFORMXML_GALLERY_ITEMS]
	INT DescriptionNumber[MAX_SCALEFORMXML_GALLERY_ITEMS]
	INT CurrentNumber[MAX_SCALEFORMXML_GALLERY_ITEMS]
	TEXT_LABEL_31 TextureName[MAX_SCALEFORMXML_GALLERY_ITEMS] 
	TEXT_LABEL_31 TextureDict[MAX_SCALEFORMXML_GALLERY_ITEMS] 
	HUD_COLOURS TextureLevel[MAX_SCALEFORMXML_GALLERY_ITEMS]
	INT BarPercent[MAX_SCALEFORMXML_GALLERY_ITEMS]
	
	SCALEFORM_INPUT_EVENT ScaleformXMLInputEventNumber
	INT iHighlightBlock
	INT iDescriptionBlock
	BOOL InitialiseScaleformXMLGalleryInputs
	BOOL InitialiseScaleformXMLGalleryHighlights
	BOOL InitialiseScaleformXMLGalleryDescription
	BOOL bRefreshScaleformXMLGalleryVisuals
	BOOL bRefreshScaleformXMLGalleryInputs
	BOOL bRefreshScaleformXMLGalleryHighlights 
	BOOL bRefreshScaleformXMLGalleryDescription
	
	INT ButtonReleasedBitset
	SCRIPT_TIMER NavigationUpTimer
	SCRIPT_TIMER NavigationDownTimer
	
	BOOL bRefreshGallery
	BOOL bisSpinnerHappening
	INT ifixerawardsviewed			= 0
	INT ifixerawardsviewedcompleted	= 0
	MP_BOOL_AWARD	bcharacteraward[MAX_SCALEFORMXML_GALLERY_ITEMS]
	MP_INT_AWARD	icharacteraward[MAX_SCALEFORMXML_GALLERY_ITEMS]
ENDSTRUCT


CONST_INT MAX_SCALEFORMXML_PLAYERLIST_ITEMS 18
STRUCT SCALEFORMXML_PLAYERLIST
	BOOL bInitialisedVisuals
	
	BOOL bHasValue[MAX_SCALEFORMXML_PLAYERLIST_ITEMS]
	
	TEXT_LABEL_63 GamerTag[MAX_SCALEFORMXML_PLAYERLIST_ITEMS]
	INT iTeam[MAX_SCALEFORMXML_PLAYERLIST_ITEMS]
	INT iCheckbox[MAX_SCALEFORMXML_PLAYERLIST_ITEMS]
	INT iRank[MAX_SCALEFORMXML_PLAYERLIST_ITEMS]
	MP_ICONS_ENUMS RankSymbol[MAX_SCALEFORMXML_PLAYERLIST_ITEMS]
	MP_ICONS_ENUMS SkillIcon1[MAX_SCALEFORMXML_PLAYERLIST_ITEMS]
	MP_ICONS_ENUMS SkillIcon2[MAX_SCALEFORMXML_PLAYERLIST_ITEMS]
	
	BOOL bHideEmptySlots
	BOOL bIsString
	BOOL bIsColourMonochrome
//	PAD_BUTTON_NUMBER InputNumber
	INT iHighlightBlock
//	BOOL InitialiseScaleformPLAYERLISTInputs
	BOOL InitialiseScaleformXMLPLAYERLISTHighlights
	BOOL bRefreshScaleformXMLPLAYERLISTVisuals
//	BOOL bRefreshScaleformPLAYERLISTInputs
	BOOL bRefreshScaleformXMLPLAYERLISTHighlights 
ENDSTRUCT


CONST_INT MAX_NUMBER_CONTROL_BITSETS 10
CONST_INT NUMBER_OF_DIFFERENT_HUD_ELEMENTS 15

CONST_INT BS_WEAPON_WHEEL_CONSUMABLES_SNACKS_STATS_CHECKED   	0
CONST_INT BS_WEAPON_WHEEL_CONSUMABLES_ARMOR_STATS_CHECKED   	1



STRUCT MPGlobalsHudStruct
	//------------------------------------------------
	//net_radio globals
	//------------------------------------------------
	
	// on mission details: KGM Obsolete? Don't think so - I think these are all Brenda's variables
	SCALEFORM_INDEX OnMissionOverlay
	SCRIPT_TIMER iOnMissionOverlayTimer 
	SCRIPT_TIMER timerBeenVisibleForTime[NUM_NETWORK_PLAYERS]
//	INT iOnDPADMAPTimer = -1
	DPADDOWN_ACTIVATION bStatOverheadOverlayOn = DPADDOWN_NONE
//	BOOL bDPadDownMapOn = FALSE
//	INT iOnMissionPlayersOnMission

// Graeme moved these two to MPGlobalsHudStruct_TITLEUPDATE
//	SCRIPT_TIMER stGamertagsTimer
//	DPADDOWN_ACTIVATION bGamertagsDpadDownState = DPADDOWN_NONE
	
	BOOL DisplayCheatMessage[15]
	BOOL DisplayBad_SportMessage[15]
	
	
	//------------------------------------------------
	//Planning board
	//------------------------------------------------
	BOOL bIsViewingPlanningBoard	
	
	//------------------------------------------------
	//MP Mission Creator
	//------------------------------------------------
	INT iMissionCreatorHud


	//------------------------------------------------
	//Globals for the Rank and Rewards
	//------------------------------------------------
//	INT RankAndRewardState = 0
//	INT iRRStartTimer = 0
	INT iXPAlpha
//	INT iXPAlphaCounter
//	INT iLastRank
	BOOL bRankingInProgress
//	FLOAT fGradualXP
	
	// Scaleform XP bar
	SCALEFORM_INDEX XPBar
//	INT iLastXPUpdate
//	INT iXPBarStage = 0


	MISSIONOVERSTRUCT MissionOver
	
	INT iTotalNumberOfMedals
	INT iTotalNumberOfMedalsGiven
	INT iStaggeredNumberOfMedalsGiven

	//Debug Widgets
//	FLOAT fCurrentXPDebug
//	FLOAT fIncreasedXPDebug
	FLOAT fHowMuchXpRank
	FLOAT fHowMuchNextXpRank
	
	BOOL bIsaRankupRunning
	
	BOOL bIsaRankupRunningBarAndBanner
	
	#IF IS_DEBUG_BUILD
	BOOL bIsaRankupRunningBarAndBannerLastFrame
	#ENDIF
	
//	INT iBonusesHelpDisplayed = -1

//	INT iTimerBarTime = -1
//	INT iFlashingTimer = -1
//	INT iFlashingMiniTimer = -1

	
	//------------------------------------------------
	//Globals for rank_rewards
	//------------------------------------------------

//	FLOAT fRankUpScaleMult
//	FLOAT fPromotedScaleMult
//	FLOAT fWeaponScaleMult
//	FLOAT fKitScaleMult
//	FLOAT fAbilityScaleMult
//	INT iRankUpAlpha
//	INT iPromotedAlpha
//	INT iWeaponAlpha
//	INT iKitAlpha
//	INT iAbilityAlpha
//	BOOL bWeaponGiven

	//------------------------------------------------
	//MISC HUD Elements
	//------------------------------------------------
	BOOL bObjectiveDistanceActive
	VECTOR vObjectiveDistance
	FLOAT fObjectiveOverrideDistance
	TEXT_LABEL_63 sObjectiveOverrideText
//	BOOL bObjectiveDistanceInit
	FLOAT fMissionIntroBoxYPos
	FLOAT fMissionIntroHeight
//	BOOL bDisplayOverheadStats = TRUE

	BOOL bDontDisplayOverheadStatsForPlayer[NUM_NETWORK_PLAYERS]
	BOOL bDontDisplayOverheadStatsForPlayerThisFrame[NUM_NETWORK_PLAYERS]
	BOOL bDisplayingOverheadStatsForPlayerNow[NUM_NETWORK_PLAYERS]
	WEAPON_TYPE players_weapon_wheel_selection
//	BOOL bDisplayHealthBarThisFrame
	
	//------------------------------------------------
	//Transition HUD Elements
	//------------------------------------------------
//	BOOL bDeathmatchTellingHudToCleanup
//	BOOL bRacesTellingHudToCleanup
//	BOOL bHaltMainPersistantForMPHud
	BOOL bIsDMLobbyOnScreen
	BOOL bIsRacesLobbyOnScreen
	
//	BOOL DoesMPHUDThinkPlayerIsDead

	BOOL bShouldMoveForHelp = FALSE
	
		

	
//	INT iPreviousHudScreen
	
	BOOL bDisplayingCNCLobby
	
	//Overheads
	DISPLAYOVERHEADSTRUCT DisplayInfo
	
	BOOL bDisplayedFirstWeaponUnlock
	BOOL bDisplayedFirstTattooUnlock
	BOOL bDisplayedFirstPatchUnlock
	BOOL bDisplayedFirstTattooUnlockQueue
	INT firsttattoo = 0
	INT FIRST_TATTOO_TIMER
	
// Graeme moved the following to MPGlobalsHudStruct_TITLEUPDATE
//	BOOL CAN_DISPLAY_WEAPON_HELP

//	BOOL bIsDpadDownMapOn
	
	INT HUDDISABLED
	INT HUDDISABLEDTHISFRAME
	INT HUDDISPLAYINGNOW
	
//	BOOL bTellHudNotToSwoopDown

	INT iHudButtonPressed[MAX_NUMBER_CONTROL_BITSETS]
	INT iDelayInt

	FLOAT fMissionBriefTotalHeight
	
	INT TunableCheck_PreviousXP
	
	INT ChangeCharacterIngameScreenStages
	
	BOOL bShowRallyIcons[2][NUM_NETWORK_PLAYERS]
	BOOL bInInterMenuInpromptuRaceSubMenu
	BOOL bInRaceRespawnDelay[2]

// Graeme moved these two to MPGlobalsHudStruct_TITLEUPDATE
//	INT iOhDensitySetting = -1
//	BOOL bCoronaToJobTransitionActive
	
	MP_FLOAT_AWARD WhichFloatAward
	MP_INT_AWARD WhichIntAward
	MP_BOOL_AWARD WhichBoolAward
	MPPLY_FLOAT_AWARD_INDEX WhichPlyFloatAward
	MPPLY_INT_AWARD_INDEX WhichPlyIntAward
	INT timer_for_dropping_guns = 0
	WEAPON_TYPE aPreviousWeapon 
	BOOL b_drop_a_clip_activated = FALSE
	//BOOL b_eat_a_snack_activated = FALSE
	SCRIPT_TIMER timerSnackEatingSafety
	//BOOL b_eat_a_snack_interaction_playing = FALSE
	
	INT i_snack_interaction_selected = -1
	INT i_armor_selected = -1
	BOOL bDisplayingSnackArmorSpecificHelpText = FALSE
	INT iWeaponWheelConsumablesBS = 0
	BOOL b_drop_a_entire_weapon_activated = FALSE
	SCRIPT_TIMER CheatMessageTimer
	SCRIPT_TIMER BadSportMessageTimer
	SCRIPT_TIMER IdleMessageTimer
	INT iHowLongIdling
	BOOL bIdleCountdownBeeps[12]
	
	SCRIPT_TIMER ConstrainedMessageTimer
	INT iHowLongConstrained
	BOOL bConstrainedCountdownBeeps[12]
	
	BOOL g_bHasAnythingChangedTimerHud
	HUDORDER OrderTracker[NUMBER_OF_DIFFERENT_HUD_ELEMENTS][MAX_NUMBER_HUD_ELEMENT]
	HUDORDER FixedOrderTracker[NUMBER_OF_DIFFERENT_HUD_ELEMENTS][MAX_NUMBER_HUD_ELEMENT]
	
	INT iCheatStateBitset_LastFrame[NUM_NETWORK_PLAYERS]
	
	BOOL b_HasLoadedWastedAudioBank
	BOOL b_StartedLoadWastedAudioBank
	BOOL b_HideCrossTheLineUi
	
	BOOL b_ShowSubtitlesDuringKillstrip
	BOOL b_ShowSubtitlesDuringSkyswoop
	
	SCRIPT_TIMER timerPassiveOhBitBackupTimer
	
// Graeme moved these nine to MPGlobalsHudStruct_TITLEUPDATE
// 	INT unlock_ticker_timer
// 	INT unlock_ticker_main_timer
// 	BOOL display_unlock_tickers
// 	INT display_unlock_stage
//	INT iFirstTimeFlashingStage
//	SCRIPT_TIMER stFlashTimer
//	BOOL bOn
//	INT iIdleCountdownBeepsCount
//	BOOL b_HasLoadedHUD321GoAudioBank
ENDSTRUCT


STRUCT MPGlobalsScoreHudStruct
	//------------------------------------------------
	//Globals for the UI stuff
	//------------------------------------------------
	
	INT iHowManyDisplays
	BOOL isSomethingDisplaying
	
//	BOOL bInit_GenericElimination[MAX_NUMBER_HUD_ELEMENT]
//	BOOL bInit_GenericDoubleNumber[MAX_NUMBER_HUD_ELEMENT] 
//	BOOL bInit_GenericScore[MAX_NUMBER_HUD_ELEMENT] 
//	BOOL bInit_GenericDoubleNumberPlace[MAX_NUMBER_HUD_ELEMENT] 
//	BOOL bInit_GenericNumber[MAX_NUMBER_HUD_ELEMENT] 
//	BOOL bInit_GenericTimer[MAX_NUMBER_HUD_ELEMENT]
//	BOOL bInit_GenericCheckpoint[MAX_NUMBER_HUD_ELEMENT]
//	BOOL bInit_GenericMeter[MAX_NUMBER_HUD_ELEMENT]

	SCRIPT_TIMER iFlashing_GenericDoubleNumberPlace_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericDoubleNumberPlace_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericDoubleNumber_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericDoubleNumber_MiniHud[MAX_NUMBER_HUD_ELEMENT] 
	SCRIPT_TIMER iFlashing_GenericScore_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericScore_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericNumber_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericNumber_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericTimer_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericTimer_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericElimination_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericElimination_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericCheckpoint_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericCheckpoint_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericMeter_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericMeter_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericFourIconBar_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericFourIconBar_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericFiveIconScoreBar_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericFiveIconScoreBar_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericFiveIconScoreBar_Avatar_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericFiveIconScoreBar_Avatar_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericSixIconBar_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iFlashing_GenericSixIconBar_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	
	SCRIPT_TIMER iColourFlashing_GenericDoubleNumberPlace_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericDoubleNumberPlace_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericDoubleNumber_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericDoubleNumber_MiniHud[MAX_NUMBER_HUD_ELEMENT] 
	SCRIPT_TIMER iColourFlashing_GenericScore_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericScore_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericNumber_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericNumber_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericTimer_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericTimer_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericElimination_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericElimination_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericCheckpoint_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericCheckpoint_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericMeter_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourFlashing_GenericMeter_MiniHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourPulsing_GenericMeter_Hud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourPulsing_GenericMeter_SecBarHud[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iColourPulsing_GenericBigNumber_Hud[MAX_NUMBER_HUD_ELEMENT]
	
	SCRIPT_TIMER iGoalMetFlashing_GenericMeter[MAX_NUMBER_HUD_ELEMENT]
	INT iGoalFadeFlashing_GenericMeter[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iGoalMetFlashing_GenericDoubleNumberPlace[MAX_NUMBER_HUD_ELEMENT]
	INT iGoalFadeFlashing_GenericDoubleNumberPlace[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iGoalMetFlashing_GenericDoubleNumber[MAX_NUMBER_HUD_ELEMENT]
	INT iGoalFadeFlashing_GenericDoubleNumber[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iGoalMetFlashing_GenericNumber[MAX_NUMBER_HUD_ELEMENT]
	INT iGoalFadeFlashing_GenericNumber[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iGoalMetFlashing_GenericTimer[MAX_NUMBER_HUD_ELEMENT]
	INT iGoalFadeFlashing_GenericTimer[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iGoalMetFlashing_GenericElimination[MAX_NUMBER_HUD_ELEMENT]
	INT iGoalFadeFlashing_GenericElimination[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iGoalMetFlashing_GenericCheckpoint[MAX_NUMBER_HUD_ELEMENT]
	INT iGoalFadeFlashing_GenericCheckpoint[MAX_NUMBER_HUD_ELEMENT]
	SCRIPT_TIMER iGoalMetFlashing_GenericScore[MAX_NUMBER_HUD_ELEMENT]
	INT iGoalFadeFlashing_GenericScore[MAX_NUMBER_HUD_ELEMENT]


	//------------------------------------------------
	//Globals for the Generic Collective Hud
	//------------------------------------------------/

	FLOAT BottomStartY
	FLOAT HudStartY
	
	INT bNumberOfInstructionalButtonsRowsUnderHud
	BOOL bGolfScoreUnderHud
	BOOL bCoronaUnderHud
	BOOL bPhoneUnderHud
	BOOL bPhoneUnderHudNoRise
	BOOL bTopRightHud
	BOOL bSwitchWheel
	BOOL bSwitchWheelAndStats
	BOOL bSniperScopeOn
	BOOL bTitleFarLeftJustified
	BOOL bTitleMiddleJustified
	BOOL bTitleExtraLeftJustified
	BOOL bTitleInsaneLeftJustified
	BOOL bTitleInsanePlusLeftJustified
	
	HUDELEMENT_METER_STUCT ElementHud_METER
	HUDELEMENT_CHECKPOINT_STUCT ElementHud_CHECKPOINT
	HUDELEMENT_ELIMINATION_STUCT ElementHud_ELIMINATION
	HUDELEMENT_SINGLE_NUMBER_STUCT ElementHud_SINGLENUMBER
	HUDELEMENT_DOUBLE_NUMBER_STUCT ElementHud_DOUBLENUMBER
	HUDELEMENT_DOUBLE_NUMBER_PLACE_STUCT ElementHud_DOUBLENUMPLACE
	HUDELEMENT_SCORE_STUCT ElementHud_SCORE
	HUDELEMENT_TIMER_STUCT ElementHud_TIMER
	HUDELEMENT_WINDMETER_STRUCT ElementHud_WIND
	HUDELEMENT_BIG_RACE_POSITION_STRUCT ElementHud_BIGRACEPOSITION
	HUDELEMENT_SPRITE_METER_STUCT ElementHud_SPRITEMETER
	HUDELEMENT_FOUR_ICON_BAR_STRUCT ElementHud_FOURICONBAR
	HUDELEMENT_FIVE_ICON_SCORE_BAR_STRUCT ElementHud_FIVEICONSCOREBAR
	HUDELEMENT_SIX_ICON_BAR_STRUCT ElementHud_SIXICONBAR
	HUDELEMENT_DOUBLE_TEXT_STUCT ElementHud_DOUBLETEXT
	
	INT ProgressHud_ActivationBitset[NUMBER_OF_PROGRESSHUD_ELEMENTS]
	INT ProgressHud_LastFrameBitset[NUMBER_OF_PROGRESSHUD_ELEMENTS]
	
	INT ProgressHud_InitBitset[NUMBER_OF_PROGRESSHUD_ELEMENTS]
	
	INT ProgressHud_ExtendDisplayBitset[NUMBER_OF_PROGRESSHUD_ELEMENTS]

	BOOL ProgressHud_ForceReset
	
	FLOAT TopOfTimers
ENDSTRUCT


//STRUCT MPGlobalsHudPositionStruct_old 
//	//TopLeft Mission display box
//	INFOBOXDETAILSSTRUCT TopLeftDisplayBox
//	
//	FLOAT fMissionCorona_CoronaWidth
//	
//	GLOBALTICKERDETAIL GlobalXPTickerArray[MAX_NUM_GLOBALTICKER_QUEUE]
//
//	INT AmmoSavingIndex
//	
//	
//	BOOL ForceXPBarOn
//	INT WeaponMessageBitset[2]
//	INT WeaponAddonMessageBitset[10]
//	INT AbilityMessageBitset[3]
//	INT KitMessageBitset[10]
//	INT CarModMessageBitset[4]
//	INT HealthMessageBitset[3]
//	INT TATTOOMessageBitset[4]
//	INT VEHICLEMessageBitset[2]
//	INT CLOTHESMessageBitset[8]
//	INT HEISTMessageBitset[2]
//	INT iOldXPValue
//	INT CREWUNLOCKMessageBitset[2]
//	
//	BOOL ForceBarOnInCutscene
//	
//	INT XPBarAlphaLevel
//	
//	INT iOldScaleformXP
//ENDSTRUCT


STRUCT MPGlobalsHudRewardsStruct
	UNLOCKDETAILS RewardArray[MAX_NUM_REWARD_QUEUE] 
	INT IntCharAwardPlatinumMessageBitset[15]
	INT FloatCharAwardPlatinumMessageBitset[15]
	INT BoolCharAwardPlatinumMessageBitset[15]
	

	
	INT IntCharAwardBronzeMessageBitset[15]
	INT FloatCharAwardBronzeMessageBitset[15]
	INT BoolCharAwardBronzeMessageBitset[15]

	
	INT IntCharAwardSilverMessageBitset[15]
	INT FloatCharAwardSilverMessageBitset[15]
	INT BoolCharAwardSilverMessageBitset[15]

	
	INT IntCharAwardGoldMessageBitset[15]
	INT FloatCharAwardGoldMessageBitset[15]
	INT BoolCharAwardGoldMessageBitset[15]
ENDSTRUCT


STRUCT MPGlobalsHudRewardsPLAYERStruct
	UNLOCKDETAILS RewardArray[MAX_NUM_REWARD_QUEUE] 

	INT IntPlyAwardPlatinumMessageBitset[10]
	INT FloatPlyAwardPlatinumMessageBitset[10]
	

	INT IntPlyAwardBronzeMessageBitset[10]
	INT FloatPlyAwardBronzeMessageBitset[10]
	

	INT IntPlyAwardSilverMessageBitset[10]
	INT FloatPlyAwardSilverMessageBitset[10]
	

	INT IntPlyAwardGoldMessageBitset[10]
	INT FloatPlyAwardGoldMessageBitset[10]
ENDSTRUCT


//STRUCT MPGlobalsHudRewardsGENERALStruct
//	UNLOCKDETAILS RewardArray[MAX_NUM_REWARD_QUEUE] 
//
//	INT IntGENAwardPlatinumMessageBitset[10]
//	INT FloatGENAwardPlatinumMessageBitset[10]
//	
//
//	INT IntGENAwardBronzeMessageBitset[10]
//	INT FloatGENAwardBronzeMessageBitset[10]
//	
//
//	INT IntGENAwardSilverMessageBitset[10]
//	INT FloatGENAwardSilverMessageBitset[10]
//	
//
//	INT IntGENAwardGoldMessageBitset[10]
//	INT FloatGENAwardGoldMessageBitset[10]
//
//ENDSTRUCT
//
//MPGlobalsHudRewardsGENERALStruct MPGlobalsHudRewardsGENERAL


STRUCT NET_FRIEND_DATA
	INT iTableSlot
	TEXT_LABEL_23 Name
	BOOL isOnline
	BOOL isGTA
	GAMER_HANDLE ghFriend
	SCRIPT_TIMER iTimer
	BOOL isInYourSession
ENDSTRUCT


TWEAK_INT UNIVERSAL_HUD_ALPHA 190 
TWEAK_INT UNIVERSAL_PLAYER_NAME_ALPHA 255 //210 //Upping to 255 for todo 484014


CONST_INT eMP_TAG_BITSETS	2


//Ints used as bit sets for controlling the current status of the players over heads.
STRUCT OVER_HEADS_GLOBAL_STRUCT
	TEXT_LABEL_7	ClanTag[NUM_NETWORK_PLAYERS]
	INT 			iOverHeadSetUp
	INT 			iWantedLevel[NUM_NETWORK_PLAYERS]
	INT 			iOverHeadItemDisplayed[eMP_TAG_BITSETS][NUM_NETWORK_PLAYERS]
	INT 			iOverHeadPlayerNumber[NUM_NETWORK_PLAYERS]
	INT				iPlayerRole[NUM_NETWORK_PLAYERS]
	BOOL 			bPlayerIsAlive[NUM_NETWORK_PLAYERS]
	BOOL			bmakelargepackageflash
	SCRIPT_TIMER	tFlashtimer
	//float 			fval
//	float 			fval2
//	float 			fval3
ENDSTRUCT




// =============================================================================================================
//      Mission Locate Message Globals
//		18/9/12: Added by Keith to control the three Scaleform movies that can display Mission Names in a corona
// =============================================================================================================

// Valid Locate Message IDs - these aren't really needed or used internally, but to prevent having to store an INT for every mission within the MissionsAtCoords
//		system, I'm setting one of three bitflags representing the (currently three) available movies. These Locate Message IDs are supplied
//		simply to cut down on the amount of knowledge the MissionsAtCoords system needs to have about the Locate Message system
CONST_INT		FIRST_LOCATE_MESSAGE_ID					0
CONST_INT		SECOND_LOCATE_MESSAGE_ID				1
CONST_INT		THIRD_LOCATE_MESSAGE_ID					2
// The maximum number of scaleform Locate Message movies available
CONST_INT		MAX_LOCATE_MESSAGE_MOVIES				3	// this should be the last entry above + 1

// An ID To use when there is no Locate Message (used for return values, etc)
CONST_INT		NO_LOCATE_MESSAGE_ID					-1


// -----------------------------------------------------------------------------------------------------------

// Locate Message Stages
ENUM g_eLocateMessageStage
	LMS_AVAILABLE_FOR_USE,					// Locate Message movie is available to use
	LMS_PREVIOUSLY_REQUESTED,				// Not requested this frame, but has previously been requested - when the movie loads it needs to be freed - the request hasn't been maintained so this movie is no longer needed
	LMS_REQUESTED_THIS_FRAME,				// Set when a request has been made this frame to load this Locate Message movie
	LMS_LOADED,								// Locate Message movie has loaded
	LMS_PREVIOUSLY_IN_USE,					// Locate Message movie wasn't displayed this frame - it will be automatically cleaned up next frame
	LMS_IN_USE_THIS_FRAME,					// Locate Message was displayed this frame
	
	// Leave this at the bottom
	LMS_NO_MOVIE_DO_NOT_USE					// A Locate Message movie is not available for this array position, so do not use this array position
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// Locate Message Types
ENUM g_eLocateMessageType
	LMT_BRIEF_DETAILS,						// This Locate Message type only requires brief details (ie: used for CnC locate messages)
	LMT_FULL_DETAILS,						// This Locate Message type requires full details (ie: used for Freemode locate messages)
	
	// Leave this at the bottom
	LMT_NO_DETAILS							// This Locate Message type requires no details - this should indicate an error
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// Struct containing Details for each available Locate Message scaleform movie
STRUCT g_structLocateMessageMP
	g_eLocateMessageStage	lmStage		= LMS_AVAILABLE_FOR_USE	// The current stage of this Locate Message movie
	g_eLocateMessageType	lmType		= LMT_NO_DETAILS		// The Type of Locate Message (indicates how much information can be displayed)
	SCALEFORM_INDEX			lmMovieID							// The Scaleform Index associated with this movie request
	TEXT_LABEL_31			lmMovieName							// The scleform movie name associated with this Locate Message (it will vary depending on teamID)
ENDSTRUCT


STRUCT g_structXPDescription
	TEXT_LABEL_15 tlXPDesc
	INT iXPAmount
	INT iParamInt
	BOOL bDisplayText
ENDSTRUCT


// ----- EOF 
