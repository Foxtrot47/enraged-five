//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	MP_Globals_Saved_Bounty.sch									//
//		AUTHOR			:	Conor McGuire												//
//		DESCRIPTION		:	Structs and enums used to store bounty information 			//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
USING "commands_network.sch"

STRUCT MP_SAVED_BOUNTY_STRUCT_OLD
	GAMER_HANDLE gh_BountyPlacer //player who placed bounty on you
	INT iTimePassedSoFar //total time so far
	INT iBountyValue //bounty value
ENDSTRUCT

CONST_INT MAX_BOUNTIES_PER_DAY 16

STRUCT MP_SAVED_BOUNTY_STRUCT
	GAMER_HANDLE gh_BountyPlacer //player who placed bounty on you
	INT iTimePassedSoFar //total time so far
	INT iBountyValue //bounty value
	GAMER_HANDLE gh_BountySet[MAX_BOUNTIES_PER_DAY] //player can only set one bounty on a given player a day. To total of 16 a day
	INT iPosXTimeSet[MAX_BOUNTIES_PER_DAY] //time before bounty slot gets cleared
ENDSTRUCT
//EOF
