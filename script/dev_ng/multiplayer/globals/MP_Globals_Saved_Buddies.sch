////////////////////////////////////////////////////////////////////////////////////////////
////																						//
////		SCRIPT NAME		:	MP_Globals_Saved_Buddies.sch								//
////		AUTHOR			:	Ryan Baker													//
////		DESCRIPTION		:	Structs and enums used to store the MP save budies	 		//
////																						//
////////////////////////////////////////////////////////////////////////////////////////////
//USING "commands_network.sch"
//
//CONST_INT MAX_MP_SAVED_BUDDIES 16
//
//STRUCT MP_SAVED_BUDDIES_STRUCT
//	GAMER_INFO	BuddyGamerInfo[MAX_MP_SAVED_BUDDIES]
//	INT iNumBuddies
//ENDSTRUCT
////EOF
