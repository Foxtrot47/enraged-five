USING "rage_builtins.sch"
USING "types.sch"
USING "mp_globals_spawning_definitions.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_spawning.sch
//      DESCRIPTION     :   Contains global variable instances for all MP spawning routines.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


#IF IS_DEBUG_BUILD
	INT iJoiningSpawnActivityOverride =  -1
	INT iJoiningSpawnInteractionTypeOverride =  -1
	INT iJoiningSpawnInteractionAnimOverride =  -1
	BOOL bTestSpawnActivity
	INT iTestSpawnActivityState	
	
	BOOL bForceJoinAtCoords
	VECTOR vForceJoinCoords
	FLOAT fForceJoinHeading
	
	BOOL bGrabForceJoinCoords
	
	BOOL bOverrideSpawnSearchResults
	VECTOR bOverrideSpawnSearchPos
	FLOAT fOverrideSpawnSearchHeading
	
	#IF FEATURE_FIXER
	INT g_iPostMissionSpawnScenario = -1
	INT g_iSittingSmokingLocationOverride = -1
	#ENDIF
	
#ENDIF
	
	
//SPAWN_INFO_OLD g_SpawnData_Old

//TRANSITION_SPAWN_DATA_OLD g_TransitionSpawnData_Old

//MISSION_SPAWN_DETAILS_OLD BlankMissionSpawnDetails_Old

MP_SPAWN_POINT SpawnPoints_SimonGarage[8]

MP_SPAWN_POINT SpawnPoints_AirportArrivals[26]

MP_SPAWN_POINT SpawnPoints_SafeCoastalPoints[8]

//SPAWN_AREA_OLD SpawnAreas_Hospitals_Old[TOTAL_NUMBER_OF_RESPAWN_HOSPITALS]

//SPAWN_AREA_OLD SpawnAreas_PoliceStation_Old[TOTAL_NUMBER_OF_RESPAWN_POLICE_STATIONS]
//
//SPAWN_AREA_OLD SpawnAreas_Hotels_Old[TOTAL_NUMBER_OF_RESPAWN_HOTELS]

//// global exclusion areas
//GLOBAL_SPAWN_EXCLUSION_AREA_OLD GlobalExclusionArea_Old[MAX_NUMBER_OF_GLOBAL_EXCLUSION_AREAS_OLD]

//VECTOR SafeSpawn_Air[NUM_SAFE_SPAWN_IN_AIR]
VECTOR SafeSpawn_Sea[NUM_SAFE_SPAWN_IN_SEA]

INT SUB_WARP_WAIT_TIME = 4000
FLOAT SUB_OUT_TO_SEA_DIST = 100.0

#IF FEATURE_HEIST_ISLAND
BOOL g_bHasSpawnedFirstSub = FALSE
INT g_iFirstSubSpawnLocation = -1

VECTOR g_vHeistIslandAngledArea1 = <<5511.550, -6014.775, -200.0 >>
VECTOR g_vHeistIslandAngledArea2 = <<4139.800, -4199.300, 200.0>>
FLOAT g_fHeistIslandAngledAreaWidth = 1308.550

VECTOR g_vHeistIslandSkyCamLocation = <<6000.0, 8000.0, 0.0>>

FLOAT g_fSubmarineFreezeOnCleanupDepth = -160.0
	
BOOL g_bNewSpawningBoundingBoxChecksEnabled = TRUE

BOOL g_bDoRespawnSearchForCoord_NewChecksEnabled = TRUE

BOOL g_bRUN_STATE_WARPING_TO_SPAWN_POINT_CheckClearCustomVehicleNodes_Enabled = TRUE

#ENDIF

#IF IS_DEBUG_BUILD
	//BLIP_INDEX SafeAirBlip[NUM_SAFE_SPAWN_IN_AIR]
	BLIP_INDEX SafeSeaBlip[NUM_SAFE_SPAWN_IN_SEA]
#ENDIF


//EOF
