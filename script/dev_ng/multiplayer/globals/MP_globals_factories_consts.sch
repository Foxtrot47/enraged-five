////////////////////////////////////////////
///    
///    Constants for Biker factories
///    

USING "mp_globals_new_features_tu.sch"


ENUM ILLICIT_GOOD_TRANSACTION_STATE
	ILLICIT_GOOD_TRANSACTION_STATE_DEFAULT = 0,
	ILLICIT_GOOD_TRANSACTION_STATE_PENDING,
	ILLICIT_GOOD_TRANSACTION_STATE_SUCCESS,
	ILLICIT_GOOD_TRANSACTION_STATE_FAILED
ENDENUM

CONST_INT ciMAX_OWNED_FACTORIES				6
CONST_INT BUNKER_SAVE_SLOT					5

CONST_INT MAX_OWNED_BIKER_FACTORIES			5 //Biker factories excludes the bunker
CONST_INT ciMAX_FACTORY_ENTITY_SETS			61
CONST_INT ciWEED_PACKETS_IN_WEED_BALE		5
CONST_INT ciMAX_FACTORY_PRODUCT_UNITS		101
CONST_INT ciBIKER_PRODUCT_FADE_TIME			500
CONST_INT ciMAX_FACTORY_PALLETS				8
CONST_INT ciMAX_WORKERS_PER_FACTORY			3
CONST_INT ciMAX_UPGRADES_PER_FACTORY		3
CONST_INT ciBIKER_UPGRADE_DELAY				2000
CONST_INT ciMAX_FACTORY_PRODUCTION_PROPS	26
CONST_INT ciMAX_FACTORY_UI_BARS				5
CONST_INT ciMAX_FACTORY_SOUND_IDS			13
CONST_INT ciMAX_FACTORY_METH_TRAY_SLOTS		26

CONST_INT ciBUSINESS_PROPERTY_CHARGE_INTERVAL	2000*60*24 // 1 game day

CONST_INT ciMAX_BUNKER_PRODUCT_MODELS		7
CONST_INT ciMAX_BUNKER_MATERIAL_MODELS		3
CONST_INT ciMAX_ARMORY_WEAPONS				6
CONST_INT ciMAX_BUNKER_PROPERTIES			12
CONST_INT ciMAX_FACTORY_MATERIAL_UNITS		6
CONST_INT ciMAX_BUNKER_IDLE_CALL_TIME		600000
CONST_INT ciMAX_BUNKER_INITIAL_PRODUCTS		35

CONST_INT ciDEFUNCT_BASE_COUNT				10

//Biker Factory ID's
ENUM FACTORY_ID
	FACTORY_ID_INVALID = 0,
	
	FACTORY_ID_METH_1,			//Paleto Bay meth lab
	FACTORY_ID_WEED_1,			//Mount Chiliad cannabis farm
	FACTORY_ID_CRACK_1,			//Paleto Bay Cocaine Lockup
	FACTORY_ID_FAKE_CASH_1,		//Paleto Bay Counterfit Cash Factory
	FACTORY_ID_FAKE_ID_1,		//Paleto Bay Forgery Office 
	
	FACTORY_ID_METH_2,			//El Burro Heights meth lab
	FACTORY_ID_WEED_2,			//Downtown vinewood cannabis farm
	FACTORY_ID_CRACK_2,			//Morningwood Cocaine Lockup
	FACTORY_ID_FAKE_CASH_2,		//Vespucci Canals Counterfit Cash Factory
	FACTORY_ID_FAKE_ID_2,		//Textile city Forgery Office
	
	FACTORY_ID_METH_3,			//Senora Desert meth lab
	FACTORY_ID_WEED_3,			//San Chianski cannabis farm 
	FACTORY_ID_CRACK_3,			//Zancudo River Cocaine Lockup
	FACTORY_ID_FAKE_CASH_3,		//Senora Desert Counterfit Cash Factory
	FACTORY_ID_FAKE_ID_3,		//Grapeseed Forgery Office
	
	FACTORY_ID_METH_4,			//Terminal meth lab 
	FACTORY_ID_WEED_4,			//Elysian Island cannabis farm 
	FACTORY_ID_CRACK_4,			//Elysian Island Cocaine Lockup 
	FACTORY_ID_FAKE_CASH_4,		//Cypress Flats Counterfit Cash Factory
	FACTORY_ID_FAKE_ID_4,		//Elysian Island Forgery Office
	
	FACTORY_ID_BUNKER_1,
	FACTORY_ID_BUNKER_2,
	FACTORY_ID_BUNKER_3,
	FACTORY_ID_BUNKER_4,
	FACTORY_ID_BUNKER_5,
	FACTORY_ID_BUNKER_6,
	FACTORY_ID_BUNKER_7,
//	FACTORY_ID_BUNKER_8,
	FACTORY_ID_BUNKER_9,
	FACTORY_ID_BUNKER_10,
	FACTORY_ID_BUNKER_11,
	FACTORY_ID_BUNKER_12,
	
	FACTORY_ID_MAX
ENDENUM

ENUM FACTORY_UPGRADE_ID
	UPGRADE_ID_INVALID	= -1,
	UPGRADE_ID_EQUIPMENT = 0,
	UPGRADE_ID_STAFF,
	UPGRADE_ID_SECURITY,
	
	UPGRADE_ID_DECOR_0,
	UPGRADE_ID_DECOR_1,
	UPGRADE_ID_DECOR_2,
	UPGRADE_ID_PERSONAL_QUARTERS,
	UPGRADE_ID_FIRING_RANGE_0,
	UPGRADE_ID_FIRING_RANGE_1,
	UPGRADE_ID_GUN_LOCKER,
	UPGRADE_ID_TRANSPORTATION_0,
	UPGRADE_ID_TRANSPORTATION_1,
	
	UPGRADE_ID_MAX
ENDENUM

ENUM FACTORY_PRODUCTION_MODE
	PRODUCTION_MODE_GOODS = 0,	//Factory is producing goods, weed, guns etc.
	PRODUCTION_MODE_RESEARCH,	//Factory is doind research
	PRODUCTION_MODE_MIXED		//Factory is spliting resources 50/50
ENDENUM

ENUM BUNKER_RESEARCH_ITEMS
	RESEARCH_ITEM_INVALID = -1,
	RESEARCH_ITEM_SAM_BATTERY = 0,
	RESEARCH_ITEM_BALLISTIC_EQUIPMENT,
	RESEARCH_ITEM_QUAD_HALFTRACK_FLAK_CANNONS,
	RESEARCH_ITEM_TAMPA3_MINIGUN,
	RESEARCH_ITEM_TAMPA3_REAR_MORTAR,
	RESEARCH_ITEM_TAMPA3_FRONT_MISSILE_LAUNCHER,
	RESEARCH_ITEM_TAMPA3_HEAVY_CHASSIS_ARMOR,
	RESEARCH_ITEM_DUNE3_GRENADE_LAUNCHER,
	RESEARCH_ITEM_DUNE3_MINIGUN,
	RESEARCH_ITEM_INSURGENT3_CAL_MINIGUN,
	RESEARCH_ITEM_INSURGENT3_LVL3_VEH_ARMROR,
	RESEARCH_ITEM_TECHNICAL3_MINIGUN,
	RESEARCH_ITEM_TECHNICAL3_RAM_BAR_MOD,
	RESEARCH_ITEM_TECHNICAL3_BRUTE_BAR_MOD,
	RESEARCH_ITEM_TECHNICAL3_HEAVY_CHASSIS_ARMOR,
	RESEARCH_ITEM_OPPRESSOR_ROCKETS,
	RESEARCH_ITEM_LIVERY_SET_FRACTAL,
	RESEARCH_ITEM_LIVERY_SET_DIGITAL,
	RESEARCH_ITEM_LIVERY_SET_GEOMETRIC,
	RESEARCH_ITEM_LIVERY_NATURE_RESERVE,
	RESEARCH_ITEM_LIVERY_NAVAL_BATTLE,
	RESEARCH_ITEM_AA_DUEL_FLAK,
	RESEARCH_ITEM_AA_MISSILE_BARRAGE,
	RESEARCH_ITEM_TRAILER_LARGE_TURRET,
	RESEARCH_ITEM_INCENDIARY_ROUNDS,
	RESEARCH_ITEM_HOLLOWPOINT_ROUNDS,
	RESEARCH_ITEM_ARMORPIERCING_ROUNDS,
	RESEARCH_ITEM_FMJ_ROUNDS,
	RESEARCH_ITEM_EXPLOSIVE_ROUNDS,
	RESEARCH_ITEM_PISTOL_MK2_RAIL,
	RESEARCH_ITEM_PISTOL_MK2_COMP,
	RESEARCH_ITEM_SMG_MK2_SIGHT,
	RESEARCH_ITEM_SMG_MK2_BARREL,
	RESEARCH_ITEM_HEAVYSNIPER_MK2_NV,
	RESEARCH_ITEM_HEAVYSNIPER_MK2_THERMAL,
	RESEARCH_ITEM_HEAVYSNIPER_MK2_BARREL,
	RESEARCH_ITEM_COMBATMG_MK2_SIGHT,
	RESEARCH_ITEM_COMBATMG_MK2_BARREL,
	RESEARCH_ITEM_ASSAULTRIFLE_MK2_SIGHT,
	RESEARCH_ITEM_ASSAULTRIFLE_MK2_BARREL,
	RESEARCH_ITEM_CARBINERIFLE_MK2_SIGHT,
	RESEARCH_ITEM_CARBINERIFLE_MK2_BARREL,
	RESEARCH_ITEM_PROXIMITY_MINES,
	RESEARCH_ITEM_WLIVERY_TIGER_STRIPE,
	RESEARCH_ITEM_WLIVERY_SKULL,
	RESEARCH_ITEM_WLIVERY_SESSANTA_NOVE,
	RESEARCH_ITEM_WLIVERY_PERSEUS,
	RESEARCH_ITEM_WLIVERY_LEOPARD_PRINT,
	RESEARCH_ITEM_WLIVERY_ZEBRA_STRIPE,
	RESEARCH_ITEM_WLIVERY_GEOMETRY,
	RESEARCH_ITEM_WLIVERY_KABOOM,
	RESEARCH_ITEM_MAX
ENDENUM

STRUCT STRUCT_BUNKER_LID_COLLISION_PROP_DATA
	INT iPropActive
	ENTITY_INDEX objectBunkerLidCollision[ciMAX_BUNKER_PROPERTIES]
ENDSTRUCT


STRUCT DEFUNCT_BASE_IPL_DATA
	INT iCollsionGeomState
	OBJECT_INDEX objCollisionGeom[ciDEFUNCT_BASE_COUNT]
ENDSTRUCT

