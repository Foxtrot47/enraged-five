USING "rage_builtins.sch"

USING "MP_Globals_Common_Definitions.sch"
USING "MP_Globals_Common_Consts.sch"
USING "mp_mission_data_struct.sch"
USING "MP_Globals_Mission_Control_Definitions.sch"
USING "MP_Globals_Activity_Selector_Definitions.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      DESCRIPTION     :   Contains global variable instances of core multiplayer globals.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


FLOAT fXPScaleX = 0.202
FLOAT fXPScaleY = 0.276

CONST_INT MECHANIC_CHARGE_INTERVAL 	2000*60*24 //1 game day
CONST_INT UTILITY_CHARGE_INTERVAL	2000*60*24 //1 game day 
CONST_INT BUSINESS_CHARGE_INTERVAL	2000*60*24 //1 game day 

#IF IS_DEBUG_BUILD
	BOOL bDisableXPAnimation
	BOOL bTestRequestPendingClearFailure
	BOOL bTestRequestCompleteFailure
	BOOL bTurnOffEnsureCleanup
	BOOL g_bOutOwnershipChargeDetails
	BOOL bForceInvitedToApartment
	INT iDisplayXPTime
	INT g_iMechanicFeeTime = MECHANIC_CHARGE_INTERVAL
	INT g_iBusinessFeeTime = BUSINESS_CHARGE_INTERVAL
	INT g_iApartmentFeeTime = UTILITY_CHARGE_INTERVAL
	BOOL g_bChargeOwnershipFeesNow
	BOOL g_db_bOutputDisplaySlotData
	BOOL g_db_bDrawDisplaySlotData
	BOOL g_db_bTestUnlockAirportDoors
	BOOL g_db_bTestResetUnlockAirportDoors
	BOOL g_db_bTestFailedVehTranInGarage
	BOOL g_db_bTestDLCIntroBink
#ENDIF


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  STRUCTS      					     /////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Vehicle boosting
VEHICLE_BOOST_INFO g_VehicleBoostInfo

//Vehicle spikes
VEHICLE_SPIKE_INFO g_VehicleSpikeInfo

//Vehicle rockets
VEHICLE_ROCKET_INFO g_VehicleRocketInfo

//Vehicle machine gun (only used for ruiner races)
VEHICLE_GUN_INFO g_VehicleGunInfo

//FMSP race data.
FMSP_RACE_DATA g_FMSP_Race_Data


//--------------------------------------------------------------------------
// Betting Data (James Adwick)
//--------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
BOOL bCoronaCamPoint
BOOL g_bDummyTeamCoronaPosition
BOOL g_bCleanUpTeamCoronaPosition
#ENDIF

#IF IS_DEBUG_BUILD
	BOOL bDebugLaunchCrateDrop
	BOOL bDebugForceRagePowerUp
	BOOL bDebugSetCrateDropSpecial
	BOOL bDebugCrateDropIgnoreNumPlayers
	BOOL bDebugLaunchStealPackageTrigger
	INT iCrateDropDay
	INT iCrateDropHour
	INT iCrateDropMin
	BOOL bPlayerOnFmAmbient
	BOOL bPlayerInTutorialSession
	BOOL bGangAttackDebugKill
//	BOOL bGangAttackDebugControl
	BOOL bGangAttackDebugPackage
#ENDIF


#IF IS_DEBUG_BUILD
	BOOL bDebugDisplayWeapons_pickups_packages
	//INT DebugDisplayWeapons_pickups_packagesbitset
#ENDIF


//--------------------------------------------------------------------------
// Kill Strip Data
//--------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
BOOL bDebugKillStrip
BOOL bHoldOnKillStrip 
BOOL bDisplayKillStreak
INT iKillStreakScoreOne = 1
INT iKillStreakScoreTwo = 1
INT iHUDColourOne
INT iHUDColourTwo
#ENDIF




// ******************************************************************************************************************************************************
// ******************************************************************************************************************************************************
// ******************************************************************************************************************************************************

//  persistent globals - these dont get reset when swapping team or leaving / entering cnc.  The are mostly transitional variables.

// ******************************************************************************************************************************************************
// ******************************************************************************************************************************************************
// ******************************************************************************************************************************************************
//BOOL g_bPlayerMustChooseAnotherTeam
BOOL g_bBringUpMPHud
BOOL g_bBringUpMainCreator

BOOL g_bTransitionBlockMusicStop
BOOL g_bTransitionNeedToPositionSPGuy
BOOL g_bTransitionTellCreatorToCleanup
BOOL g_Private_BailHappened
BOOL g_Private_BailSessionHappened = FALSE
BOOL g_bAllowPendingTransitions = TRUE
BOOL g_Private_QuitMPThroughMenu
BOOL g_bHaveCodeStatsLoaded[MAX_NUM_CHARACTER_SLOTS+1] 
INT g_bForceLoadStages[MAX_NUM_CHARACTER_SLOTS+1] 

BOOL g_bHasUGCContentLoaded_SP = FALSE
BOOL g_b_Private_Tshirt_is_new_character[NUM_NETWORK_PLAYERS]
WEAPON_TYPE gweapon_type_CurrentlyHeldWeapon 
INT g_iCurrentlyHeldWeapon_Ammo

MULTIPLAYER_SETTINGS g_MultiplayerSettings


PAUSE_MENU_MISSION_CREATOR_STRUCT g_PauseMenuMissionCreatorData

BOOL g_bMissionCreatorPauseMenuActive
BOOL g_bAwardsPauseMenuActive
BOOL g_bWeaponsPauseMenuActive


// for spawning into the game at specific coords
//VECTOR vSpecificSpawnLocationCoords_Old
//FLOAT fSpecificSpawnLocationMinRadius_Old
//FLOAT fSpecificSpawnLocationRadius_Old
//FLOAT fSpecificSpawnLocationHeading_Old
//THREADID SpecificSpawnLocationThreadID_Old
//BOOL bSpecificSpawnLocationDoVisibleChecks_Old
//BOOL bSpecificSpawnLocationDoNearARoadChecks_Old
//BOOL bSpecificSpawnLocationConsiderInteriors_Old
//BOOL bSpecificSpawnLocationUseAngledArea_Old
//VECTOR vSpecificSpawnLocationAngledCoords1_Old
//VECTOR vSpecificSpawnLocationAngledCoords2_Old
//FLOAT fSpecificSpawnLocationWidth_Old



#IF IS_DEBUG_BUILD
QUICKLAUNCH g_qlWhichTeam = QUICKLAUNCH_EMPTY

BOOL g_ShouldShiftingTutorialsBeSkipped
BOOL g_bRunMissionCreatorInDebugMode
#ENDIF

BOOL g_bDeathmatchFinished			// Speirs added for Neil (487055)
BOOL g_bBlockRankuphudendofactivity
BOOL g_bBasejumpRaceFinished			// Wong added 
//BOOL g_bDeathmatchRespawnTimeUp		// Speirs added (557768)
BOOL g_bDeathmatchPlayerDead		// Speirs added (970774)

//BOOL g_bOkToExitDeathmatch 			// Not used
//BOOL g_bOktoExitRaces				// Not used
BOOL g_bOkToExitMissionCreator
BOOL g_bKillFMMCmenu
BOOL g_bKillFMMC_SignedOut
BOOL g_bQuitDeathmatchScript
BOOL g_QuitRacesScript
BOOL g_NeedANewLobby
BOOL g_OkToKillTransition 			// Speirs added 09/07/12 for Brenda, ok to kill lobby in dm/race when screen faded in

BOOL g_SkipFmTutorials	// For occasions when temporarily joining FM from SP, but don't want to do tutorials / flow

BOOL g_IsInLoadingScreen
BOOL g_ForcePageRefresh

BOOL g_bCompKillTickers
BOOL g_bNoDeathTickers
BOOL g_bBlockAIKillXP

INT g_iMPGameModeCurrentlySittingIn
BOOL bHasCreatedEmptyGameMode
TIME_DATATYPE g_iMPTimer

STRUCT_NETWORK_SIGN_IN_STATE_CHANGED g_SignInStructDetails

BOOL g_IsSomethingElseExittingMP
TIMEOFDAY g_sMultiplayerTransitionTOD
INT g_iGametimeMPSPTransition = -1

BOOL g_bInitShopSessionForFailSlot = FALSE

TRANSITION_DATA g_TransitionData
MP_CHAR_DATA g_MPCharData
INT g_iLeaveMultiplayerState = LEAVE_MP_STATE_NULL
LEAVEMPHUD g_TransitionCameraState  = LEAVEMPHUD_NONE
NETWORK_SCRIPT_STATE g_scriptState

BOOL g_bForceBailWhenNoValidSpectatorTargetForJIP

// the players array needs to persist when player swaps team
//INT AllPlayersArray[NUM_NETWORK_PLAYERS]
//INT AllLobbyPlayersData[NUM_NETWORK_PLAYERS]

//Tell the hud to quit
//BOOL g_bTellMPHudToQuit
//BOOL g_bTellMPHudToQuitForInterpCutscene

//car bomb timer
SCRIPT_TIMER tdbombtimer
	
STRUCT_BAIL_EVENT g_BailEvent
STRUCT_JOIN_SESSION_RESPONSE g_JoinResponseEvent

END_REASON g_EndSessionReason

BOOL g_bInHudExitState
BOOL g_bForceNetBailToCancelFMRejoin = FALSE	//For use in the BG script to block an automatic return to freemode after a bail event

BOOL g_bStaggeredConsidersEveryone = FALSE

INT g_iStaggeredParticipant
INT g_iStaggeredParticipant_LastFrame
PARTICIPANT_INDEX g_StaggeredParticipant = INT_TO_NATIVE(PARTICIPANT_INDEX, -1)
PLAYER_INDEX g_StaggeredPlayer = INT_TO_NATIVE(PLAYER_INDEX, -1)

INT g_iServerStaggeredParticipant
INT g_iServerStaggeredParticipant_LastFrame
PARTICIPANT_INDEX g_ServerStaggeredParticipant = INT_TO_NATIVE(PARTICIPANT_INDEX, -1)
PLAYER_INDEX g_ServerStaggeredPlayer = INT_TO_NATIVE(PLAYER_INDEX, -1)
BOOL g_ServerStaggeredPlayerActive


INT g_iContinualServerStaggeredParticipant
INT g_iContinualServerStaggeredParticipant_LastFrame
PARTICIPANT_INDEX g_ContinualServerStaggeredParticipant = INT_TO_NATIVE(PARTICIPANT_INDEX, -1)
PLAYER_INDEX g_ContinualServerStaggeredPlayer = INT_TO_NATIVE(PLAYER_INDEX, -1)
BOOL g_ContinualServerStaggeredPlayerActive


INT NearPlayers[NUM_NETWORK_PLAYERS]
VECTOR g_vFreemodeCutStartPos = <<754.22, 1226.83, 356.51>> //Slightly updated to where Klaas said he was at the start of hte cutscene. BC <<747.2693, 1260.5319, 359.2997>> // Player position at start of freemode intro cut
VECTOR g_vFreemodeRaceTutStartPos = <<-199.1940, 305.1888, 95.9458>>// <<-180.9892, 314.8488, 96.7999>> // For spawning at start of race tutorial
VECTOR g_vFreemodeDMTutStartPos = <<355.7, 277.5,102.2490>>// <<-701.8183, -966.9388, 18.2538>> // // For spawning at start of DM tutorial
INT iStaggeredWarpRequestPlayer

BOOL g_bSomeoneRunningBackupHeliScript

#IF IS_DEBUG_BUILD	
F9_SCRIPT_INFO F9ScriptInfo[TOTAL_NUMBER_OF_F9_SCRIPTS]
INT iF9ScriptToUpdate
CONST_INT ciDEBUG_GANGOPS_PLANNINGBOARD_RESTART -2
CONST_INT ciDEBUG_GANGOPS_PLANNINGBOARD_OFF -1
CONST_INT ciDEBUG_GANGOPS_PLANNINGBOARD_PREP 0
CONST_INT ciDEBUG_GANGOPS_PLANNINGBOARD_FINALE 1
CONST_INT ciDEBUG_GANGOPS_PLANNINGBOARD_IDLE 2
CONST_INT ciDEBUG_GANGOPS_PLANNINGBOARD_TUTORIAL	3
INT iGangOpsPlanningBoardActive = ciDEBUG_GANGOPS_PLANNINGBOARD_OFF
#ENDIF

//--------------------------------------------------------------------------
// Custom Ped Variation Camera
//--------------------------------------------------------------------------
#IF IS_DEBUG_BUILD
BOOL bDeactivatePedVariationDebugCamera = FALSE
VECTOR vPedVariationStartPos
VECTOR vPedVariationCameraOffset
#ENDIF

//--------------------------------------------------------------------------
// Relationship Groups - these never change, effectively they are constants
//--------------------------------------------------------------------------
//Players
//REL_GROUP_HASH rgCnC_TeamCop
//REL_GROUP_HASH rgCnC_TeamCrimGang1
//REL_GROUP_HASH rgCnC_TeamCrimGang2
//REL_GROUP_HASH rgCnC_TeamCrimGang3
//REL_GROUP_HASH rgCnC_TeamCrimGang4
//REL_GROUP_HASH rgCnC_TeamCopHated
//REL_GROUP_HASH rgCnC_Idle

//AI
//REL_GROUP_HASH rgCnC_HateAllPlayers
//REL_GROUP_HASH rgCnC_LikeAllPlayers
//REL_GROUP_HASH rgCnC_IgnoreAllPlayers
//REL_GROUP_HASH rgCnC_HateCops
//REL_GROUP_HASH rgCnC_LikeCops
//REL_GROUP_HASH rgCnC_HateCrims
//REL_GROUP_HASH rgCnC_HateGang1
//REL_GROUP_HASH rgCnC_HateGang2
//REL_GROUP_HASH rgCnC_HateGang3
//REL_GROUP_HASH rgCnC_HateGang4
//REL_GROUP_HASH rgCnC_LikeCrims
//REL_GROUP_HASH rgCnC_LikeGang1
//REL_GROUP_HASH rgCnC_LikeGang2
//REL_GROUP_HASH rgCnC_LikeGang3
//REL_GROUP_HASH rgCnC_LikeGang4
//REL_GROUP_HASH rgCnC_LikeCops_HateCrims
//REL_GROUP_HASH rgCnC_LikeCrims_HateCops
//REL_GROUP_HASH rgCnC_LikeGang1_HateOthers
//REL_GROUP_HASH rgCnC_LikeGang2_HateOthers
//REL_GROUP_HASH rgCnC_LikeGang3_HateOthers
//REL_GROUP_HASH rgCnC_LikeGang4_HateOthers
//REL_GROUP_HASH rgCnC_LikedByAll
//REL_GROUP_HASH rgCnC_LikeCops_HateGang1
//REL_GROUP_HASH rgCnC_LikeCops_HateGang2
//REL_GROUP_HASH rgCnC_LikeCops_HateGang3
//REL_GROUP_HASH rgCnC_LikeCops_HateGang4
//REL_GROUP_HASH rgCnC_CuffedVagos
//REL_GROUP_HASH rgCnC_CuffedLost
//REL_GROUP_HASH rgCnC_CustodyVagos
//REL_GROUP_HASH rgCnC_CustodyLost
//REL_GROUP_HASH rgCnC_Snitch
//REL_GROUP_HASH rgCnC_EmptyNPC
//REL_GROUP_HASH rgCnC_CopNPC
//REL_GROUP_HASH rgCnC_TeamCopDirty
//REL_GROUP_HASH rgCnC_CuffedCop
//REL_GROUP_HASH rgCnC_CustodyCop
//REL_GROUP_HASH rgCnC_LikeAllAndIgnoredByPlayers
//REL_GROUP_HASH rgCnC_CuffedNPC
//REL_GROUP_HASH rgCnC_CustodyNPC
//
//


//------------------------------------------------
//Globals for gang locations
//------------------------------------------------

// new gang building blips
BLIP_INDEX g_blip_GangBuildings[NUMBER_OF_POLICE_STATIONS + NUMBER_OF_GANG1_SAFEHOUSES + NUMBER_OF_GANG2_SAFEHOUSES + NUMBER_OF_GANG3_SAFEHOUSES + NUMBER_OF_GANG4_SAFEHOUSES]
//BLIP_INDEX g_blip_GangBuildingOuterRadius[NUMBER_OF_POLICE_STATIONS + NUMBER_OF_GANG1_SAFEHOUSES + NUMBER_OF_GANG2_SAFEHOUSES + NUMBER_OF_GANG3_SAFEHOUSES + NUMBER_OF_GANG4_SAFEHOUSES]

//Player blip data
//PLAYER_BLIP_DATA_OLD g_PlayerBlipsData_Old

//BLIP_INDEX DrugDealingBlip
//BLIP_INDEX DrugTableBlip
//BOOL bTurnOffDrugBlips
//BLIP_INDEX DrugSellingBlips[MAX_NUMBER_OF_SELLING_PLACES]


CONST_INT ciNUM_CACHED_DAILY_OBJECTIVES 3
STRUCT DAILY_OBJECTIVE_CACHE
	BOOL bRunCheck = FALSE
	MP_DAILY_OBJECTIVES eObjective[ciNUM_CACHED_DAILY_OBJECTIVES]
ENDSTRUCT
DAILY_OBJECTIVE_CACHE g_DailyObjectiveCache


// ===========================================================================================================
//      Objective Text Control globals - Keith 28/7/11
//		Allows a mission to specify a piece of Objective Text which continues to display when there is no
//			other God Text or Conversation Text on display until switched off.
// ===========================================================================================================

g_structObjectiveTextControlMP	g_sObjectiveTextControlMP
g_structObjectiveTextMP	g_sObjectiveTextMP



// ===========================================================================================================
//      Mission Triggering Overview globals
//		(Game Information used to aid Mission Triggering decisions)
// ===========================================================================================================

g_structMissionOverviewLocalMP	g_sMissionOverviewLocalMP

// ===========================================================================================================
//     	Dropoff Cutscene globals
//		(Game Information used to aid Dropoff Cutscene decisions)
// ===========================================================================================================

// Values: 0 = No preference, 1 = Male, 2 = Female
ENUM CUSTOM_DELIVERY_SCENE_PED_GENDER
	eCUSTOMDELIVERYSCENE_PEDGENDER_NO_PREFERENCE = 0,
	eCUSTOMDELIVERYSCENE_PEDGENDER_MALE = 1,
	eCUSTOMDELIVERYSCENE_PEDGENDER_FEMALE = 2
ENDENUM
CUSTOM_DELIVERY_SCENE_PED_GENDER g_CustomDeliverySceneGenderPed = eCUSTOMDELIVERYSCENE_PEDGENDER_NO_PREFERENCE


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  GLOBAL SERVER AND CLIENT BROADCAST DATA              /////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//GlobalServerBroadcastData_OLD GlobalServerBD_Old
//GlobalPlayerBroadcastData_OLD GlobalplayerBD_Old[NUM_NETWORK_PLAYERS]
//LobbyServerBroadcastData LobbyServerBD
//StatDataPlayerBroadcastData StatDataPlayerBD[NUM_NETWORK_PLAYERS]
//StatDataServerBroadcastData StatDataServerBD

CHAR_PED_CREATION g_EmptyCharPedCreation
CHAR_PED_CREATION g_CharPedCreation[MAX_NUM_CHARACTER_SLOTS]




// ***************************************************************************************************************************
// ***************************************************************************************************************************
// ***************************************************************************************************************************
//  define debug globals - these dont currently get reset
// ***************************************************************************************************************************
// ***************************************************************************************************************************
// ***************************************************************************************************************************

#IF IS_DEBUG_BUILD
	BOOL bOutputCurrentLocation
	BOOL bEditPositions
	BOOL bOldEditPositions
	
	BOOL bDebugDrawLocations


	// Network Bots Globals
	INT             netbotCheckForSeatCounter
	REL_GROUP_HASH  netbotGroup

	
	BOOL g_bGameSpyOnline = TRUE
	BOOL g_bProcessBots = TRUE
	BOOL g_bMainMissionScriptActive = FALSE

	
	FLOAT g_fPlayerListTextScale = 0.224
	
	THREADID g_MainMissionScriptID = INT_TO_NATIVE(THREADID, -1)
	
	
	// Betting debug globals
	INT g_iDebugBettingUniqueID = 1
	BOOL g_bDebugEnableBetting
	BOOL g_bDebugControlBetting
	BOOL g_bDebugDisableTimerForBetting
	BOOL g_bDebugSendAvailForBetting
	BOOL g_bDebugSendLeaveBetting
	BOOL g_bDebugSendMissionLaunched
	BOOL g_bDebugSendMissionFinished
	BOOL g_bDebugShowBettingDebugScreen
	BOOL g_bDebugBettingMenu
	BOOL g_bDebugBettingShowUnderlay
	
	BOOL g_bDebugToggleCoronaPositions
	INT iDebugCoronaPosition
	BOOL bFakeCoronaFull
	BOOL bFakeFriendsOfFriends
	BOOL bDisplayQuickStartInvite
	BOOL bAllowCoronaDMScreens
	BOOL bTweakCoronaCam
	BOOL bDebugClonesForCoronaScene
	BOOL bDebugUseNewCoronaCamera
	INT iDebugNumberClonesForScene
	
	BOOL bDebugCoronaMap
	BOOL bDebugHeistLastJob
	
	BOOL g_bDebugTotalPlayersInCorona
	INT g_iDebugTotalPlayersInCorona
	
	BOOL g_bCoronaForceReposition
	
	BOOL g_bDebugTriggerCashTransaction

	INT g_iAfterLifeWidget
#ENDIF

BOOL g_bUseStaggeredLoops = TRUE
BOOL g_bHidePlayerArrowThisFrame = FALSE
BOOL g_Private_Update_PlayerList
INT g_Private_NumbersExpectedLobby

INT g_Lobby_PreviousRank[NUM_NETWORK_PLAYERS]
INT g_Lobby_PreviousTeam[NUM_NETWORK_PLAYERS]

BOOL g_Private_Update_PlayerList_Friend 
INT g_Private_NumbersFriendsLastFrame
INT g_Private_NumbersFriendsInvitedLastFrame
INT g_Private_NumbersOnlineFriendsLastFrame

INT g_Private_CHARACTER_TEAM[5]
INT g_Private_ACTIVE_CHARACTER

BOOL g_Private_Am_Kicked

BOOL g_TellTransitionToNotStartFreemode

BOOL g_Is_Invite_Transition_Session_Invite

BOOL g_Private_CrewChanged
BOOL g_Private_CloudDown
BOOL g_Private_CloudBackup
BOOL g_Private_HasKickedIdling
BOOL g_Private_HasJoinFailedWithDeadFreemode
BOOL g_Private_HasKickedCheater
BOOL g_Private_HasROSCredentialsChanged
BOOL g_Private_CloudDownWhenJoining
BOOL g_Private_LoadSlotFailedJoining
BOOL g_Private_HasScriptTimedOut
BOOL g_Private_EventTransitionEndLaunch
BOOL g_Private_EnteredCharCreationFromAlterationPrompt
BOOL g_Private_DoCameraQuickExitToSP

INT g_Private_BailWarningScreenBitset[2]
INT g_Private_BailWarningScreenEventParam
INT g_Private_BailWarningScreenButtonBitset

int g_Private_ASyncXboxGoldChecks
#IF FEATURE_GEN9_EXCLUSIVE
BOOL g_bAllowSafeToQuitBailScreenBypass = TRUE
#ENDIF

// ===========================================================================================================
//      Temp storage for choosing a mission location to aid Rowan's spawn map routines
//		NOTE: An instance of the struct is declared in the global struct below
// ===========================================================================================================

INT iBS_PM_ColumnHidden

// ===========================================================================================================
//      Player corona positioning
// ===========================================================================================================

//FMMC_SELECTED_ITEMS_OLD g_sTransitionSessionOptions_Old
//FMMC_SELECTED_ITEMS_OLD g_sCoronaOptionsBackUp_Old

//CORONA_CLONE_PEDS_OLD g_CoronaClonePeds_Old
//CORONA_INVITED_PLAYER_DATA_OLD g_CoronaInvitedPlayers_Old


//TRANSITION_SESSION_NON_RESET_VARS_OLD g_TransitionSessionNonResetVars_Old

CORONA_JOB_LAUNCH_DATA g_CoronaJobLaunchData

INT g_iMenuSelection[HUD_MENU_ITEMS]

BOOL g_bOnCoopMission
BOOL g_bVSMission
BOOL g_bMissionEnding
BOOL g_bMissionClientGameStateRunning
BOOL g_bMission321Done
BOOL g_bIntroJobShardPlayed
BOOL g_bForceCloseWarningScreen
BOOL g_ChangeWastedShardText
BOOL g_ForceRedWastedKillerShard
BOOL g_bCelebrationPreLoadTriggered
BOOL g_bAbortPropertyMenus
BOOL g_bDoMCDispatchCleanup
BOOL g_bDoMCTeamCleanup
BOOL g_bAirlockTextShown
INT g_iAirlockHudDrawnFrame
BOOL g_bStartedInAirlockTransition = FALSE
BOOL g_bStartedInFadeTransition = FALSE
BOOL g_bRunGroupYachtExit = FALSE
BOOL g_bRunningGroupYachtExit = FALSE
BOOL g_bMissionWasInVehWhenAlive
BOOL g_bMissionDoNotHangUpFromDialogueHandler = FALSE
BOOL g_bSkipReturnFalseInPMCFreemodeDead // Added to turn off a fix that was quite significant late in a pack. 23/07/2020. Could be safe to remove at some stage.
INT g_iMissionLobbyStartingInventoryIndex[FMMC_MAX_TEAMS]

#IF IS_DEBUG_BUILD
BOOL g_bAllowAllMissionsWithTwoPlayers = FALSE
BOOL g_bSoloStrand = FALSE
#ENDIF

#IF IS_DEBUG_BUILD
INT g_iMissionForcedSpawnGroup
INT g_iMissionForcedSpawnSubGroup[ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS]

INT g_iMissionForcedCheckpointSpawn = -1

CONST_INT ciCONTENT_OVERVIEW_SHARED_WARP_CACHE_MAX 2
VECTOR vContentOverviewSharedWarpCache[ciCONTENT_OVERVIEW_SHARED_WARP_CACHE_MAX]
FLOAT fContentOverviewSharedWarpCache[ciCONTENT_OVERVIEW_SHARED_WARP_CACHE_MAX]

#ENDIF

INT g_iBS1_Mission
CONST_INT ciBS1_Mission_PurchasedVehicleRepair							0
CONST_INT ciBS1_Mission_PurchasedVehicleRespray							1
CONST_INT ciBS1_Mission_PurchasedVehiclePlateChange						2
CONST_INT ciBS1_Mission_HoldUpRespawn									3
CONST_INT ciBS1_Mission_WaitingForSpecEnd								4
CONST_INT ciBS1_Mission_HideVehicleRepairOption							5
CONST_INT ciBS1_Mission_HideVehicleResprayOption						6
CONST_INT ciBS1_Mission_HideVehiclePlateChangeOption					7
CONST_INT ciBS1_Mission_PreventSimpleInteriorTerminateOnDeathAndIPLCleanup			8
CONST_INT ciBS1_Mission_EnablePersonalVehicles							9
CONST_INT ciBS1_Mission_DisablePersonalVehicles							10
CONST_INT ciBS1_Mission_AllowPVVehiclesInBunker							11

HUD_STATE g_Private_HudState = HUD_STATE_NO_DISPLAY

TRANSITION_STATE g_Private_TransitionState = TRANSITION_STATE_EMPTY
TEXT_LABEL_63 g_Private_TransitionString

TRANSITION_MENU_CHOICE g_Private_Players_Menu_Choice = MENU_CHOICE_EMPTY
TRANSITION_DM_MENU_CHOICE g_Private_Players_DM_Menu_Choice = DM_MENU_CHOICE_EMPTY
TRANSITION_FM_MENU_CHOICE g_Private_Players_FM_Menu_Choice = FM_MENU_CHOICE_EMPTY
TRANSITION_RACES_MENU_CHOICE g_Private_Players_RACES_Menu_Choice = RACES_MENU_CHOICE_EMPTY
TRANSITION_CREATOR_MENU_CHOICE g_Private_Players_CREATOR_Menu_Choice = CREATOR_MENU_CHOICE_EMPTY
TRANSITION_CREATOR_RACE_MENU_CHOICE g_Private_Players_RACE_CREATOR_Menu_Choice = CREATOR_RACE_MENU_CHOICE_EMPTY
TRANSITION_CREATOR_DM_MENU_CHOICE g_Private_Players_DM_CREATOR_Menu_Choice = CREATOR_DM_MENU_CHOICE_EMPTY
TRANSITION_FM_SESSION_MENU_CHOICE g_Private_Players_FM_SESSION_Menu_Choice = FM_SESSION_MENU_CHOICE_EMPTY
TRANSITION_FM_SESSION_MENU_CHOICE g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY = FM_SESSION_MENU_CHOICE_EMPTY
TRANSITION_CREATOR_MISSION_MENU_CHOICE g_Private_Players_MISSION_CREATOR_Menu_Choice = CREATOR_MISSION_MENU_CHOICE_EMPTY
TRANSITION_BAILING_OPTIONS g_Private_Bailing_Options = BAILING_OPTIONS_EMPTY
TRANSITION_CREATOR_CUSTOM_MENU_CHOICE g_Private_Players_CUSTOM_CREATOR_Menu_Choice = CREATOR_CUSTOM_MENU_CHOICE_EMPTY

#IF FEATURE_FREEMODE_ARCADE
#IF FEATURE_COPS_N_CROOKS

ENUM ARCADE_MODE_ENTRY_POINT
	ARCADE_ENTRY_POINT_PAUSE_MENU = 0,
	ARCADE_ENTRY_POINT_PHONE = 1,
	ARCADE_ENTRY_POINT_INVITE = 2,
	ARCADE_ENTRY_POINT_QUICK_JOB = 3
ENDENUM

ARCADE_MODE_ENTRY_POINT g_Private_ARCADE_MODE_ENTRY_POINT = ARCADE_ENTRY_POINT_PAUSE_MENU
#ENDIF
#ENDIF

// ===========================================================================================================
//		SAVEMIGRATION  
// ===========================================================================================================

CONST_INT SAVEMIGRATION_STATUS_STAGE_START 					0
CONST_INT SAVEMIGRATION_STATUS_STAGE_REQUEST_STATUS 		1
CONST_INT SAVEMIGRATION_STATUS_STAGE_WAIT_FOR_STATUS 		2
CONST_INT SAVEMIGRATION_STATUS_STAGE_STATUS_OK 				3
CONST_INT SAVEMIGRATION_STATUS_STAGE_STATUS_INPROGRESS		4
CONST_INT SAVEMIGRATION_STATUS_STAGE_STATUS_ERROR			5
CONST_INT SAVEMIGRATION_STATUS_STAGE_STATUS_PROFILE_ERROR	6

// ===========================================================================================================

INT g_Private_CharacterSelected //Used for Scaleform
INT g_Private_CharacterJoining	//Used for transition to tell if hte player has been swapped.
BOOL g_Private_LobbyDisableBack	//Disables the back button in the lobby screens
BOOL g_Private_LobbyDisableJoin	//Disables the accept button in the lobby screens
//THREADID g_Private_LobbyScriptID
BOOL g_Private_DidTransitionStartInMP
BOOL g_Private_HasConfirmInvite
BOOL g_Private_HasConfirmSameSessionDifferentTeamInvite
BOOL g_Private_HasPresenceConfirmInvite
BOOL g_Private_HasConfirmPartyInvite
BOOL g_Private_HasConfirmVIAPartyInvite
BOOL g_Private_LaunchedCNCFromArgs
BOOL g_Private_TransitionFirstSpawn
INT g_Private_InviteAimState
BOOL g_Private_IsMultiplayerCreatorRunning
BOOL g_Private_MultiplayerCreatorNeedsToEnd
BOOL g_Private_MultiplayerCreatorAllowIntro
BOOL g_Private_MultiplayerCreatorAllowDeathAndRespawning
BOOL g_b_HasQuitGame
BOOL g_b_HasEnteredGameAsSpectator[MAX_NUM_CHARACTER_SLOTS]

BOOL g_B_PlaySPReentryPOSTFX_DEPRECATED		//	Graeme - Added back in to keep the GLOBALS_BLOCK_MP block in sync with the released version of GTA5

BOOL g_B_DisplayOnlineSplash
BOOL g_B_HideOnlineJoiningFeed
BOOL g_B_IsOnlineJoiningFeedDisplaying
BOOL g_B_StopAllTransitionFeeds

//BOOL g_b_HasEntered_MPOfflineUGCMode
BOOL g_b_HasEntered_MPOfflineSAVEMode
BOOL g_b_HasEntered_ReducedPermsMessageDone
BOOL g_b_HasEntered_ReducedContent
BOOL g_b_HasEntered_ReducedContent_Permissions


BOOL g_b_HasCloudReturnedInGameMessageDisplayed 
BOOL g_b_HasCloudDownInGameMessageDisplayed

BOOL g_b_TellCoronaToCleanup
BOOL g_b_IsTransitionSplashScreenActive

BOOL g_b_IsInvitedToSCTV

BOOL g_b_Private_QuickJoin
//BOOL g_b_HasBailHappenedDuringTransition
BOOL g_b_IsBailStillValid 
BOOL g_b_BlackListSession
BOOL g_b_IsInTransition
BOOL g_b_HasJoinedSessionHappened
BOOL g_Private_EndSessionHappenedSomehow
BOOL g_Private_ReturningFromActivitySession

INT g_iSnackHealthAmount = 1			//Was 20, but now this gets added to your Inventory so we just want a value of 1
FLOAT g_fSnackHealthProbability = 0.2 	//20%

BOOL g_Private_HasCancelledJoin

BOOL g_AreBirdsDisplaying


INT g_NumberNeededForKicking = NUM_NEEDED_FOR_KICKING
INT g_iNumberKickingVotes

INT g_iPrivate_CharSummery_SelectedCharacter

INT g_PresenceInviteUniqueId

BOOL g_Private_Primary_Clan_Changed
BOOL g_Private_KickedFromClan
BOOL g_Private_KickedForIdling


SCRIPT_TIMER g_SavingIconPositionTimer
SCRIPT_TIMER g_SubtitlesPositionTimer
SCRIPT_TIMER g_StreetNamePositionTimer


//CURRENT_UGC_STATUS_STRUCT_OLD g_sCURRENT_UGC_STATUS_Old
    	
//FM_HIDDEN_PACKAGE_CREATION_STRUCT_OLD FM_hidden_package_Old

INT FM_AMMO_TRACKING_COUNTER
INT FM_PICKUP_COUNTER = 0
INT FM_RUN_STAGGERED_CLOTHES_UNLOCKS_CHECK_COUNTER = 0
INT FM_RUN_STAGGERED_WEAPON_UNLOCKS_CHECK_COUNTER = 0
INT FM_RUN_STAGGERED_RUN_FM_INITIAL_CARMOD_UNLOCK_CHECK_COUNTER =0
BOOL g_bcheckstartingcrewxp
INT g_iFMoldcrewxp 


#IF IS_DEBUG_BUILD
BOOL g_iFakeSLotFAil
BOOL g_UseForceAllHeadingValuesToAlign = FALSE
#ENDIF


BOOL g_Private_IgnoreSlotFailedLoad[MAX_NUM_CHARACTER_SLOTS+1]
//BOOL g_Private_SlotFailedLoad[MAX_NUM_CHARACTER_SLOTS+1]


NETWORK_CLAN_DESC g_Private_PlayerCrewData[NUM_NETWORK_PLAYERS]
SCRIPT_TIMER g_Private_ScriptBailTimer 

BOOL g_AutomuteChecksDone = FALSE

BOOL g_Private_IsCrewColourSet
INT g_Private_LocalPlayerCrew_Colour_Red
INT g_Private_LocalPlayerCrew_Colour_Green
INT g_Private_LocalPlayerCrew_Colour_Blue
TEXT_LABEL_63 g_Private_LocalPlayerCrew_RankTitle[101]
BOOL g_Private_TriggerCrewMetaDataChange
BOOL g_TurnOnCreatorHud = TRUE

BOOL g_Private_SwitchedSP_DuringActivity
BOOL g_Private_MP_Picked_from_selector


BOOL g_Private_RestartMain_Creator_Menu

PED_INDEX PlayerSwitchLeftBehindPed
PED_INDEX FakePedForFadeOutInteriorSwitch

//BOOL g_bPrivate_SaveRequested_Old[15]
BOOL g_bPrivate_IsSaveHappening
INT g_bPrivate_ProcessingWhichSave = -1

BOOL g_bTurnOnSpriteText

#IF IS_DEBUG_BUILD
BOOL g_ShowSavingPrints
BOOL g_ShowSavingMigrationPrints
#ENDIF

INT G_DELETE_STAGES_QUICK
INT G_CLEAR_ALL_CHARACTER_QUICK_INDEX

BOOL g_DidGameCreateNewCharacter[MAX_NUM_CHARACTER_SLOTS]

BOOL g_Private_CheckIgnoredSaves
BOOL g_Private_CheckIgnoredSavesMain

INT gPRivate_LowerDistance = 30
INT gPRivate_upperDistance = 40

INT g_iExternalWarningScreenStages

BOOL g_bSkipAllHUDResets
BOOL g_bSkipFaceIconReset

BOOL g_b_LoadingInProgress
BOOL g_b_IsLoadingStatsIconRunning
SCRIPT_TIMER g_st_SigningOutSelectorTimer

BOOL g_IsNewsFeedDisplaying
SCRIPT_TIMER g_aSplashScreenTimer
SCALEFORM_INDEX g_SplashScreenMovie
SCALEFORM_INDEX g_SplashScreenMovieBIG

BOOL g_bSplashScreenBigMethodCalledJoining
BOOL g_bSplashScreenBigMethodCalled 

#IF IS_DEBUG_BUILD
BOOL g_bIgnoreCombatPackCheck
VECTOR gPRivate_PositionOverhead 
#ENDIF

BOOL g_bDoesPlayerHavePrivileges
BOOL g_bShouldIgnoreCheaterRating

BOOL g_IsMainCreatorCameraRunning 

INT g_CheaterStages
INT g_CheaterMessageStages
FLOAT g_CheaterRatingLastFrame

BOOL g_HasLastSPSwitchCheck_Initialised
BOOL g_WasCheaterLastSPSwitchCheck
BOOL g_WasBadSportLastSPSwitchCheck

// FACEBOOK HEADSHOT UPDATES

BOOL g_bPostCharacterFacebookUpdate
INT g_iCharacterFacebookUpdateState

INT g_BadSportStages
INT g_BadSportMessageStages
FLOAT g_BadSportRatingLastFrame

INT g_IdleCheckMessageStages
INT g_IdleFeedID

BOOL g_ResetIdleTimerWhileViewingCredits = TRUE

BOOL g_HasCancelledJobOnPhone

BOOL g_QuitJobOnPhoneRace_DEPRECATED	//	Graeme - Added back in to keep the GLOBALS_BLOCK_MP block in sync with the released version of GTA5

INT g_HasLoadingIconAboveButton_LastFrame 
BOOL g_HasLoadingIconActive_LastFrame 
BOOL g_RunLoadingIconSpam

INT g_HasSubtitlesAboveButton_LastFrame 
BOOL g_HasSubtitlesActive_LastFrame 
BOOL g_RunSubtitlesSpam


BOOL g_HasStreetNamesAboveButton_LastFrame 
BOOL g_HasStreetNamesActive_LastFrame 
FLOAT g_fStreetTimerHeight_LastFrame 
BOOL g_RunStreetNamesSpam 

INT g_HasLoadingIconSubtitlesAboveButton 
INT g_PlayerCRCCheckExternal

#IF IS_DEBUG_BUILD
FLOAT iXPosLoadingIconAboveButton
FLOAT iYPosLoadingIconAboveButton

FLOAT iXPosSubtitlesAboveButton
FLOAT iYPosSubtitlesAboveButton

FLOAT iXPosStreetNameAboveButton
FLOAT iYPosStreetNameAboveButton
BOOL g_b_TurnOnStreetnamePrints
BOOL g_B_TurnOffStreetNames
#ENDIF

BOOL g_b_TurnOnStreetnameMOVEMENT		//	Graeme - moved out of IS_DEBUG_BUILD to keep the GLOBALS_BLOCK_MP block in sync with the released version of GTA5
BOOL g_b_CheaterCheck_LastFrame 

INT g_Bad_Sport_GRIEFING_LastCheck
INT g_Bad_Sport_OFFENSIVE_LANGUAGE_LastCheck
INT g_Bad_Sport_FRIENDLY_LastCheck
INT g_Bad_Sport_HELPFUL_LastCheck
INT g_Bad_Sport_OFFENSIVE_TAGPLATE_LastCheck
INT g_Bad_Sport_OFFENSIVE_UGC_LastCheck
INT g_Bad_Sport_VC_ANNOYING_LastCheck

BOOL g_b_RunFriendsInSessionFeed
INT g_iNumberOfPlayersOnThisActivity = 0

BOOL g_b_Has_Entered_GTAO_This_Boot 

INT number_kills_this_session_for_revenge_tracking // will have to be reset to 0 at start of every session

TIME_DATATYPE g_CrewDecalRequestTime

BOOL g_bBikerInSellHeli

// ===========================================================================================================
//		DEATHMATCH FUNCTIONS 
// ===========================================================================================================

INT DEATH_REVENGE_BITSET
TIME_DATATYPE revengetimerTimer[NUM_NETWORK_PLAYERS]
INT iKiller_of_player[NUM_NETWORK_PLAYERS]

VEHICLE_INDEX g_vehIDMP_Prop_Retained

INT iMP_Prop_RetainBS

BOOL addedMPInsuranceContact

INT g_I_MAX_XP_CAP_COUNTER
// Used for recording players rank before a mission
INT g_rank_before_activity
BOOL g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = FALSE
BOOL g_RUN_FM_INITIAL_CARMOD_UNLOCK_FM_CHECK = FALSE
BOOL g_RUN_STAGGERED_CLOTHING_UNLOCKS_CHECK = FALSE

BOOL bTransitionSpawningInProperty

INT g_iBGScriptVersion


//property bools for BG script
BOOL g_bAllowPropertyInviteClearOnInviteBlocked = TRUE
BOOL g_bAllowSVMDisplaySlotCorrection = TRUE


// to save on stack size
COMBINED_MODEL_DATA g_SafeCombinedModel_Data
VECTOR g_SafeCombinedModel_vMin[2], g_SafeCombinedModel_vMax[2]
FLOAT g_SafeCombinedModel_fWidth[2], g_SafeCombinedModel_fLength[2], g_SafeCombinedModel_fHeight[2]
FLOAT g_SafeCombinedModel_fMaxWidth, g_SafeCombinedModel_fMaxHeight
VECTOR g_SafeCombinedModel_vCombinedMin, g_SafeCombinedModel_vCombinedMax
	
VEHICLE_INDEX g_PoolVehicles[MAX_NUM_VEHICLES_IN_LOCAL_POOL]
VEHICLE_INDEX g_PoolTrucks[MAX_NUM_VEHICLES_IN_LOCAL_POOL]
BOOL g_bBlockPassiveModeOptionForMOC
BOOL g_bBlockPassiveModeOptionForAvenger
BOOL g_bBlockPassiveModeOptionForHackerTruck
VEHICLE_INDEX g_PoolAvengers[MAX_NUM_VEHICLES_IN_LOCAL_POOL]

#IF FEATURE_DLC_2_2022
VEHICLE_INDEX g_PoolAcidLabs[MAX_NUM_VEHICLES_IN_LOCAL_POOL]
#ENDIF

BOOL g_bMissionSeatSwapping = FALSE
BOOL g_bMissionHideRespawnBar = FALSE
BOOL g_bMissionInstantRespawn = FALSE
BOOL g_bMissionHideRespawnBarForRestart = FALSE
BOOL g_bMissionHasPlacedAvengerVehicle = FALSE

#IF IS_DEBUG_BUILD
BOOL g_bScriptPhoneBlockerActive 		= FALSE
BOOL g_bScriptPhoneBlockerDefaultSet 	= FALSE

BOOL g_bFMMCBlockRockstarDevCheckForCreator = FALSE
#ENDIF

//EOF
