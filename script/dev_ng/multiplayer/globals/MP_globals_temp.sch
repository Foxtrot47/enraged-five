// KEITH: A copy of MP_Globals.sch prior to splitting the MP Globals off into their own block
// The files from here will be moved across to MP_Globals.sch which is now registered in its own block
// This is still registered in the STANDARD block


//USING "global_block_defines.sch"
//
//// Multiplayer globals
//USING "MP_globals_COMMON.sch"
//
//// USING "MP_globals_Teams.sch"			Moved to globals since it is accessed in startup.sc
////USING "MP_globals_CNC_Flow.sch"
//USING "MP_globals_FM.sch"
//USING "MP_globals_ambience.sch"
//USING "MP_globals_comms.sch"
//USING "MP_globals_mission_control.sch"
//USING "MP_globals_mission_info.sch"
//USING "MP_globals_mission_trigger.sch"
//USING "MP_globals_missions_at_coords.sch"
//USING "MP_globals_activity_selector.sch"
//USING "MP_globals_missions_shared.sch"
//USING "MP_globals_hud.sch"
//USING "MP_globals_stats.sch"
//USING "MP_globals_tickers.sch"
//USING "MP_globals_events.sch"
//USING "MP_globals_DM.sch"
//USING "MP_globals_races.sch"
//USING "MP_globals_Spawning.sch"
//// USING "MP_globals_interior_instances.sch"	Moved to globals since it is accessed in startup.sc
////	USING "MP_globals_Tunables.sch"		Moved to its own block of globals called GLOBALS_BLOCK_TUNABLES
//USING "MP_globals_spectator.sch"
//USING "MP_globals_player_headshots.sch"
//USING "MP_globals_ambient_manager.sch"
//// USING "MP_Globals_saved.sch"			Moved to globals since it is accessed in startup.sc
//USING "MP_globals_drugdealing.sch"
//USING "MP_globals_ScriptSaves.sch"
//USING "MP_globals_interactions.sch"
//
//// Multiplayer debug-only globals
//#IF IS_DEBUG_BUILD
//	USING "MP_globals_debug.sch"
//#ENDIF





