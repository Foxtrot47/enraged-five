
//------------------------------------------------
// Global ticker data.
//------------------------------------------------

USING "hud_colours.sch"

CONST_INT MAX_TICKERS 6
CONST_INT MAX_COP_TICKERS 6
CONST_INT MAX_TICKER_TEXT_SLOT 5

CONST_FLOAT NULL_FLOAT -9999.9


CONST_INT TICKER_NULL		0
CONST_INT TICKER_ACTIVE		1
CONST_INT TICKER_FADE		2



//ENUM TICKER_TYPE
//	
//	// Null ticker.
//	eTICKER_NULL,
//	
//	// A custom ticker.
//	eTICKER_CUSTOM,
//	eTICKER_CUSTOM_FROM_EVENT, // for tickers from events, so F10 know not to print again.
//	
//	eTICKER_AWARD_PROGRESS,
//	eTICKER_AWARD_COMPLETE,
//	
//	#IF IS_DEBUG_BUILD
//	eTICKER_DEBUG,
//	eTICKER_CHEAT,
//	#ENDIF	
//	
//	eTICKER_XP
//
//	
//ENDENUM

//STRUCT TICKER_TEXT_INFO_2	
//	//INT iColour				// rgba packed into a single int
//	HUD_COLOURS tlColour
//	TEXT_LABEL_63 tlText	// Text to draw.
//ENDSTRUCT

//ENUM TICKER_DISPLAY_TYPE
//	
//	TICKER_DISPLAY_TEXT,
//	TICKER_DISPLAY_TEXT_WITH_LITERAL_STRING,
//	TICKER_DISPLAY_TEXT_WITH_TWO_LITERAL_STRINGS,
//	TICKER_DISPLAY_TEXT_WITH_STRING,
//	TICKER_DISPLAY_TEXT_WITH_TWO_STRINGS
//	
//ENDENUM


//STRUCT TICKER_MESSAGE
//	
//	// Type of ticker
//	TICKER_TYPE eTickerType
//	
//	// Ticker state. Active or Not Active just now.
//	INT iTickerState
//	
//	// Time the ticker was created.
//	INT iTimeCreated
//	
//	// Alpha to draw the entire ticker.
//	INT iTickerAlpha 
//	
//	// Offset for tickers moving up the queue
//	FLOAT fYOffset
//	
//	//FLOAT fScale = 1.0
//	
//	//INT iDrawBitfield			// Should my components be drawn? Bitfield
//	//INT iIsTextLabelBitfield	// Is the text of components a TL or literal string? Bitfield
//	
//	PLAYER_INDEX PlayerID
//	
//	// 0 = Left, 1 = mid, 2 = right.
////	TICKER_TEXT_INFO_2 textData[MAX_TICKER_TEXT_SLOT]
//	
//	//BOOL bIsNewTickerMessage
//	
//	//BOOL bOldTickerSystem 
//	BOOL bHasPrinted
//
////	INT iNumber1
////	INT iNumber2
//	
//	TEXT_LABEL_63 DictionaryName
//	TEXT_LABEL_63 TextureName	
//	
//	// new ticker system
//	NEW_TICKER_STRUCT Data
//	
////	TEXT_LABEL_63 mainTextLabel												
////	TICKER_COMPONENT_TYPE Component[MAX_TICKER_COMPONENTS]
////	HUD_COLOURS ComponentColour[MAX_TICKER_COMPONENTS]
////	TEXT_LABEL_63 ComponentTextLabel[MAX_TICKER_TEXT_LABEL_COMPONENTS]
////	TEXT_LABEL_63 ComponentString[MAX_TICKER_STRING_COMPONENTS]
////	FLOAT ComponentFloat[MAX_TICKER_FLOAT_COMPONENTS]
////	INT ComponentFloatDecimalPlaces[MAX_TICKER_FLOAT_COMPONENTS]
////	INT ComponentInt[MAX_TICKER_INT_COMPONENTS]
//
//	
//ENDSTRUCT

//TICKER_MESSAGE tickerMessages[MAX_TICKERS]
//TICKER_MESSAGE tickerCopMessages[MAX_COP_TICKERS]
//TICKER_MESSAGE tickerAwardCompleteMessages[MAX_TICKERS]
//TICKER_MESSAGE tickerAwardProgressMessages[MAX_TICKERS]

// new ticker system
//TICKER_MESSAGE currentTickerMessage
//TICKER_MESSAGE NullTickerMessage // moved this to a global from the function CLEAR_TICKER_MESSAGE as it was breaking stack sizes. 

//BOOL g_bTickerOpen
//TICKER_TYPE g_TickerType



// Ticker display values.
// TODO: Make const when in release.
// Note: When you change the ticker_justifyType, you'll need to update the order in which
// you draw the ticker components, see NOTE A and NOTE B in net_ticker_ui.
//
///*CONST_*/FLOAT ticker_width_hd 		=0.250		// width
///*CONST_*/FLOAT ticker_height_hd 		= 0.5 //0.4		// height
//
///*CONST_*/FLOAT ticker_pad_hd 			=0.002		// Spaceing/padding between ticker components. 
//
///*CONST_*/FLOAT ticker_base_x_hd		=0.056		// Starting/base X
/////*CONST_*/FLOAT ticker_base_y_hd		=0.719		// Starting/base Y
///*CONST_*/FLOAT ticker_base_y_hd		=0.659		// Starting/base Y
//
///*CONST_*/FLOAT ticker_queuescale_hd 	=0.9		// scale used if you are not the most recent ticker item
//
///*CONST_*/INT 	ticker_justifyType =	0	// Left hand justification	(pick one from net_render_text.sch)
//	
///*CONST_*/FLOAT fTickerGap 				= 0.03 		// Set gap between tickers.
//
///*CONST_*/FLOAT fTickerPhoneHeight 				=0.178 		// Set gap between tickers.

///*CONST_*/FLOAT Award_ticker_base_x_hd		=0.5		// Starting/base X
///*CONST_*/FLOAT Award_ticker_base_y_hd		=0.18		// Starting/base Y
//
///*CONST_*/FLOAT AwardComplete_SpriteX_offset		=0.0		// Starting/base X
///*CONST_*/FLOAT AwardComplete_SpriteY_offset		=0.060		// Starting/base X
//
///*CONST_*/FLOAT AwardProgress_base_x_hd		=0.072		// Starting/base X
///*CONST_*/FLOAT AwardProgress_base_y_hd		=0.730		// Starting/base Y
///*CONST_*/FLOAT AwardProgress_SpriteX_offset		=-0.013		// Starting/base X
///*CONST_*/FLOAT AwardProgress_SpriteY_offset		=0.029		// Starting/base X

//FLOAT AwardProgress_sprite_w = 0.045
//FLOAT AwardProgress_sprite_h = 0.055
//
//FLOAT AwardComplete_sprite_w = 0.065
//FLOAT AwardComplete_sprite_h = 0.075
//
//INT AwardProgress_DisplayAtOnce = 1
//INT AwardComplete_DisplayAtOnce = 1
//INT GameTicker_DisplayAtOnce = 4
//
//FLOAT ticker_cash_height_hd = 0.299
//FLOAT ticker_cash_base_x_hd = 0.943
//FLOAT ticker_cash_base_y_hd = 0.085
//FLOAT ticker_cash_wanted_offset_Y = 0.041
//
//FLOAT ticker_cop_height_hd = 0.4
//FLOAT ticker_cop_base_x_hd = 0.327
//FLOAT ticker_cop_base_y_hd = 0.561
//FLOAT ticker_cop_wanted_offset_Y = 0.025

// Debug and widgets	
#IF IS_DEBUG_BUILD
	BOOL bPrintTestTicker
	BOOL bPrintTestCashTicker
	BOOL bPrintTest1PlayerTicker
	BOOL bPrintTest2PlayerTicker
	BOOL bPrintTest3PlayerTicker
	BOOL bPrintTest4PlayerTicker
	//BOOL bPrintTestWithTeamName
	BOOL bPrintTestWithCrewName
	BOOL bTickerTestDontUseCodeNames
	
	BOOL bUseOldCrewTicker	
#ENDIF




// Tickers fade between TICKER_FADE_OUT_START_TIME and TICKER_MAX_DISPLAY_TIME
// The time each ticker is displayer for.
/*CONST_*/INT TICKER_MAX_DISPLAY_TIME		=7500
INT TICKET_MAX_FADE_OUT_TIME = 250
// The time in which the ticker will start fading. Must be < TICKER_MAX_DISPLAY_TIME
/*CONST_*/INT TICKER_FADE_OUT_START_TIME	=7250
/*CONST_*/INT TICKER_FADE_IN_TIME = 250
FLOAT TICKER_Y_MOVE_SPEED = 0.15
FLOAT TICKER_Y_OFFSET_START = 0//0.25//1.0
INT TICKER_QUEUE_MAX_ALPHA = 220

//------------------------------------------------
