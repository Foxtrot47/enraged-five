USING "rage_builtins.sch"

USING "mp_globals_mission_control_definitions.sch"
USING "MP_Mission_Data_Struct.sch"					// For the MP_MISSION_DATA struct




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_mission_control.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains global variable instances for all MP mission control routines.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


// -----------------------------------------------------------------------------------------------------------
//     Mission Control Client Globals for maintaining server broadcasts to the client for one frame
// -----------------------------------------------------------------------------------------------------------

// The Broadcast array
// NOTE: Entries are always added to the end of the array and shuffled up the array to overwrite older entries
g_structMissionControlBroadcasts g_sMCBroadcasts[MAX_MISSION_CONTROL_BROADCAST_ARRAY]
INT	g_numMCBroadcasts = 0


// -----------------------------------------------------------------------------------------------------------
//     Mission Control Client Globals for resyncing after a Host migration
// -----------------------------------------------------------------------------------------------------------

// STRUCT allowing the client to resync with the host after a Host migration (to recover from lost event broadcasts)
g_structMissionControlClientResyncWithHost	g_sMCResync


// -----------------------------------------------------------------------------------------------------------

// A struct containing details of a broadcast from an external system to the mission controller
// The array of resync broadcasts to be stored
g_structMissionControlResyncBroadcasts	g_sMCResyncComms[MAX_MC_RESYNC_BROADCASTS]
INT g_numMCResyncBroadcasts = 0
