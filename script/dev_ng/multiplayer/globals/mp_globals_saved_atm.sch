//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	MP_Globals_Saved_atm.sch									//
//		AUTHOR			:	Ak															//
//		DESCRIPTION		:	Structs and enums used to store bounty information 			//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

CONST_INT MP_TOTAL_ATM_LOG_ENTRIES 16

ENUM MP_ATM_LOG_ACTION_TYPE
	MALA_WITHDRAW,
	MALA_DEPOSIT,
	MALA_CASH_RECIEVED,
	MALA_CASH_SENT,
	MALA_CASH_BOUGHT,
	MALA_PURCHASE,//item bought
	MALA_REFUND//refund on purchase
ENDENUM

/*
ENUM MP_ACCOUNT_ACTION_SOURCE //Transaction sources that appear in the log in MP
	MAAS_UNSET = 0,
	MAAS_CARSITE = 333,
	MAAS_AUTOSITE,
	MAAS_PLANESITE,
	MAAS_BOATSITE,
	MAAS_PROPERTY
ENDENUM
*/

STRUCT MP_SAVED_ATM_STRUCT
	
	//log data
	INT iLogCaret
	INT LogValues[MP_TOTAL_ATM_LOG_ENTRIES]
	TEXT_LABEL_23 LogSourceNames[MP_TOTAL_ATM_LOG_ENTRIES]
	MP_ATM_LOG_ACTION_TYPE LogActionType[MP_TOTAL_ATM_LOG_ENTRIES]
	INT LogData[MP_TOTAL_ATM_LOG_ENTRIES]
	
	BOOL bAnyMpVehicleBought // 1640962
ENDSTRUCT
//EOF
