USING "types.sch"
USING "mp_globals_script_timers.sch"
USING "mp_globals_nightclub_consts.sch"

// -----------------------------------------------------------------------------------------------------------
// NightClub State Enums
// -----------------------------------------------------------------------------------------------------------

ENUM ACT_NIGHTCLUB_ACT_STATE
	NC_STAGE_INIT = 0,
	NC_STAGE_ACTIVITY_CREATE_PED_PROPS, 
	NC_STAGE_INIT_PED_CULLING,
	NC_STAGE_REMOVE_CULLED_PEDS,
	NC_STAGE_POPULATE_CULLED_PED_ARRAY,
	NC_STAGE_CREATE_PEDS,
	NC_STAGE_ASSIGN_PEDS_TO_ACTIVITY,
	NC_STAGE_UPDATE_PED_BEHAVIOUR,
	NC_STAGE_CLEANUP_PROPS_PEDS_AND_TERMINATE,
	NC_STAGE_RECREATE
ENDENUM

ENUM NIGHTCLUB_AUDIO_TAGS
	AUDIO_TAG_NULL = 0,
	AUDIO_TAG_LOW,
	AUDIO_TAG_MEDIUM,
	AUDIO_TAG_HIGH,
	AUDIO_TAG_HIGH_HANDS
ENDENUM

ENUM NIGHTCLUB_PED_ANIM_DICT
	NCAD_FACE_DJ,
	NCAD_SINGLE_PROPS,
	NCAD_AMBIENT_PEDS,
	NCAD_GROUPS
ENDENUM

ENUM NIGHTCLUB_PODIUM_DANCER_PED_STATE
	PODIUM_PEDS_STATE_NULL,
	PODIUM_PEDS_STATE_FEMALE,
	PODIUM_PEDS_STATE_MALE,
	PODIUM_PEDS_STATE_MIX
ENDENUM
ENUM NIGHTCLUB_PODIUM_DANCER_PED_STYLE
	PODIUM_PEDS_STYLE_NULL,
	PODIUM_PEDS_STYLE_1,
	PODIUM_PEDS_STYLE_2,
	PODIUM_PEDS_STYLE_3
ENDENUM

CONST_INT NSS_COMPLETED_NOTHING 0
CONST_INT NSS_COMPLETED_STAFF_ONLY 1
CONST_INT NSS_COMPLETED_EQUIP_ONLY 2
CONST_INT NSS_COMPLETED_STAFF_AND_EQUIPMENT 3
CONST_INT NSS_FULLY_SETUP 4

ACT_NIGHTCLUB_ACT_STATE g_ncLastState
ACT_NIGHTCLUB_ACT_STATE g_ncState
BOOL g_bNightClubPedsReadyToView
BOOL g_bPerventNightClubPedsCacheData = FALSE
BOOL g_bDisableNightClubPedsPedReadyBailTimer = FALSE
INT g_iNightClubPedsPedReadyBailTimer = 72000
NIGHTCLUB_AUDIO_TAGS g_eNightClubAudioIntensityLevel
NIGHTCLUB_AUDIO_TAGS g_eNightClubAudioIntensityLevelNext
INT g_iNightClubAudioCurrentTime = -1
INT g_iNightClubAudioNextTagTime = -1
NIGHTCLUB_PODIUM_DANCER_PED_STATE g_eNightClubPodiumDancersState
NIGHTCLUB_PODIUM_DANCER_PED_STYLE g_eNightClubPodiumDancersStyle
BOOL g_bTurnOnNightClubPeds
BOOL g_bTurnOnNCExtraBouncers
BOOL g_bTurnOnNCExtraBartenders
BOOL g_bTurnOnNCDJ
BOOL g_bTurnOnNCDJEntourage = TRUE
BOOL g_bTurnOnNCPodiumDancers
BOOL g_bTurnOnNCExtraDJ
BOOL g_bTurnOnNCStaffPeds
BOOL g_bTurnOnNCCustomerPeds
INT g_iNightClubClientele
NIGHTCLUB_DJ_ID g_eActiveDJ = DJ_NULL
NIGHTCLUB_DJ_ID g_eCurrentPlayingDJ = DJ_NULL
INT g_iNightclubCurrentDJSpeechCheck = -1
INT g_iNightClubSetupState = NSS_COMPLETED_NOTHING
INT g_iNightClubPedsHallwayLayout_Last
INT g_iNightClubPedsHallwayLayout
INT g_iNightClubPedsToiletLayout_Last
INT g_iNightClubPedsToiletLayout
INT g_iNightClubTonyPosition
INT g_iNightClubLazlowPosition
INT g_iNightClubPedsMainAreaLayout
INT g_iNightClubPedsMainAreaLayout_Variation_Last
INT g_iNightClubPedsMainAreaLayout_Variation
INT g_iNightClubClienteleVariation
INT g_iNightClubClienteleVariation_Last
INT g_iNightClubStationID
INT g_iNightClubPedScriptInstance
FLOAT g_fPercentagePopularity
INT g_iDJSequenceID

#IF IS_DEBUG_BUILD
INT g_incLastState_get
INT g_incState_get
INT g_iNightClubAudioIntensityLevel_get
INT g_iNightClubAudioIntensityLevelNext_get
INT g_iNightClubPodiumDancersState_get
INT g_iNightClubPodiumDancersStyle_get
INT g_iCurrentPlayingDJ_get
INT g_iCurrentClubPedCount_get

INT g_incState_set = -1
INT g_iNightClubAudioIntensityLevel_set = -1
INT g_iNightClubPodiumDancersState_set = -1
INT g_iNightClubPodiumDancersStyle_set = -1
INT g_iCurrentPlayingDJ_set = -1
INT g_iCurrentPlayingDJ_widget = -1
BOOL g_bDebugNightClubKick
INT g_iDebugNightClubMixStartMinute = -1
#ENDIF

CONST_INT NIGHT_CLUB_MAX_KICK_STRIKES 	 10
INT g_iNightClubKickStrikes

INT g_iBsNightClubKickOwner
SCRIPT_TIMER g_stNightClubKickTime

INT g_iBSNightclubSpeech
INT g_iBSNightclubCelebSpeech
INT g_iBSNightclubCashierSpeech
#IF FEATURE_CASINO_NIGHTCLUB
INT g_iBSCasNightclubCashierSpeech
SCRIPT_TIMER g_stCasinoNightClubKickTime
#ENDIF

SCRIPT_TIMER g_stNightclubSpeechTime
SCRIPT_TIMER g_stNightclubSpeechGreetTime

INT g_iToiletAttendantTipCount = 0

