USING "rage_builtins.sch"
USING "types.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_ambient_manager.sch
//      CREATED         :   David Gentles
//      DESCRIPTION     :   Contains globals and structs for all MP Ambient Manager functionality.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************

/// PURPOSE: List of ambient scripts
ENUM MPAM_TYPE_LIST
	MPAM_TYPE_CINEMA = 0,					//0
	MPAM_TYPE_GANGHIDEOUT,					//1
	MPAM_TYPE_RACETOPOINT,					//2
	MPAM_TYPE_HOLDUP,						//3		//never set
	MPAM_TYPE_CRATEDROP,					//4
	MPAM_TYPE_SECURITYVAN,					//5
	MPAM_TYPE_IMPORTEXPORT,					//6
	MPAM_TYPE_STRIPCLUB,					//7
	MPAM_TYPE_HOLD_UP_TUT,					//8
	MPAM_TYPE_JOYRIDER,						//9
	MPAM_TYPE_CAR_MOD_TUT,					//10
	MPAM_TYPE_GAMEDIR_MISSION,				//11
	MPAM_TYPE_LESTER_CUTSCENE,				//12
	MPAM_TYPE_TREVOR_CUTSCENE,				//13
	MPAM_TYPE_LESTER_WAREHOUSE_CUTSCENE,	//14
	MPAM_TYPE_PLANE_TAKEDOWN,				//15
	MPAM_TYPE_DISTRACT_COPS,				//16
	MPAM_TYPE_DESTROY_VEH					//17
	, MPAM_TYPE_MOVING_TARGET,					//18
	MPAM_TYPE_KILL_LIST,				//19
	MPAM_TYPE_AIR_DROP,					//20
	MPAM_TYPE_TIME_TRIAL,					//21
	MPAM_TYPE_CP_COLLECTION,				//22
	MPAM_TYPE_CHALLENGES,					//23
	MPAM_TYPE_PENNED_IN,					//24
	MPAM_TYPE_RC_TIME_TRIAL,				//25
	MPAM_TYPE_HOLD_THE_WHEEL,				//26
	MPAM_TYPE_HOT_PROPERTY,					//27
	MPAM_TYPE_DEAD_DROP,					//28
	MPAM_TYPE_KING_OF_THE_CASTLE,			//29
	MPAM_TYPE_CRIMINAL_DAMAGE					//30
	, MPAM_TYPE_LOWRIDER_INTRO_CUTSCENE		//31
ENDENUM
CONST_INT MPAM_TYPE_LIST_SIZE 			32
//If we reach 32 let David G know - except I'm leaving, so you'll need to expand this list to accept multiple bitsets

/// PURPOSE: Data struct used to store information regarding how many ambient entities are left to be reserved
STRUCT MP_AMBIENT_MANAGER_DATA
	INT iBitset

	INT iHighestPlayerPedReserve = 0
	INT iHighestPlayerVehicleReserve = 0
	INT iHighestPlayerObjectReserve = 0
	
	INT iProcessHighestPlayerPedReserve = 0
	INT iProcessHighestPlayerVehicleReserve = 0
	INT iProcessHighestPlayerObjectReserve = 0
	
	INT iPlayerStaggerCounter = 0


	#IF IS_DEBUG_BUILD
	INT iDebugReservedPeds, iDebugCreatedPeds
	INT iDebugReservedVehicles, iDebugCreatedVehicles
	INT iDebugReservedObjects, iDebugCreatedObjects
	INT iDebugReservedPickups, iDebugCreatedPickups
	BOOL bDebugCurrentlyInType[MPAM_TYPE_LIST_SIZE]
	#ENDIF

ENDSTRUCT
MP_AMBIENT_MANAGER_DATA g_MPAMData


// ===========================================================================================================
//      General MP Ambient Manager globals
// ===========================================================================================================
INT g_MaxNumReservePeds
INT g_MaxNumReserveVehicles
INT g_MaxNumReserveObjects
INT g_MaxNumReservePickups
