USING "rage_builtins.sch"

USING "MP_Globals_Common_Consts.sch"					// For NUM_NETWORK_PLAYERS
USING "mp_mission_data_struct.sch"						// For ILLEGAL_CREATOR_ID
USING "MP_Globals_Missions_At_Coords_Definitions.sch"	// For ILLEGAL_AT_COORDS_ID
USING "charsheet_global_definitions.sch"

// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_missions_shared.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains constant, enum, and struct definitions for all MP Missions Shared routines.
//							NB. Should not contain instantiated variables.
//
//		NOTES			:	RETRIEVING CLOUD HEADER DATA - KGM 13/3/13
//							The Cloud Header data is going to have to be retrieved by splitting the data over multiple events.
//
//							CLOUD DATA BEEN USED - KGM 13/3/13
//							I can see potential problems with an array element becoming 'not in use' and then immediately becoming
//							'in use' again with different data. To prevent these edge cases I've added a 'been used' flag. When looking
//							for an empty slot it will favour those not in use and not been used. Only when all have 'been used' will it
//							clear out those flags from all slots that are no longer 'in use' and start to re-use them.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      CONSTS and VARIABLES
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      General Missions Shared Consts
// -----------------------------------------------------------------------------------------------------------

// Clear Bitfield value
CONST_INT		ALL_MISSIONS_SHARED_BITS_CLEAR			0


// -----------------------------------------------------------------------------------------------------------
//      General Pending Activities Consts
// -----------------------------------------------------------------------------------------------------------

// Represents an illegal array position on th Pending Activities array
CONST_INT		ILLEGAL_PENDING_ACTIVITIES_SLOT				-1

// Represents a new search starting at the start of the array (equal to -1 because the search always starts at the passed in arraypos + 1)
CONST_INT		NEW_PENDING_ACTIVITIES_SEARCH				-1

// Maximum controlled missions in shared array
CONST_INT		MAX_PENDING_ACTIVITIES						16

// Default Rank values, used as default when no rank is specified - max is large enough to ensure all missions are included
CONST_INT		MIN_PENDING_ACTIVITIES_RANK					0
CONST_INT		MAX_PENDING_ACTIVITIES_RANK					9999

// Max time allocated to a player sending Pendign Activity data to the server
CONST_INT		PENDING_ACTIVITIES_DATA_SEND_TIMEOUT_msec	10000


// -----------------------------------------------------------------------------------------------------------
//      General Pending Activities Groups Consts
// -----------------------------------------------------------------------------------------------------------

// Maximum groupings of pending activities
CONST_INT		MAX_PENDING_ACTIVITIES_GROUPS				16

// Illegal pending activities Group
CONST_INT		ILLEGAL_PENDING_ACTIVITIES_GROUP			-1

// Overlap radius that classes two locations as part of the same group
CONST_FLOAT		PENDING_ACTIVITIES_GROUP_RADIUS_m			10.0


// -----------------------------------------------------------------------------------------------------------
//      General Missions Shared Cloud Data Consts
// -----------------------------------------------------------------------------------------------------------

// Maximum cloud-loaded header details for missions actively being shared
CONST_INT		MAX_MISSIONS_SHARED_CLOUD_DATA						NUM_NETWORK_PLAYERS

// The Stored 'atCoords' for Heists will be this base value + the heist corona regID returned, so always -ve
CONST_INT		MISSIONS_SHARED_AT_COORDS_BASE_ID_FOR_HEISTS		-1000000


// -----------------------------------------------------------------------------------------------------------
//      Generated Cloud Header Data Consts
// -----------------------------------------------------------------------------------------------------------

// A Fake Variation used if the cloud header data was generated from full mission info and the actual header data is not available (ie: cross-session joining another player's UGC)
CONST_INT		GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION		9999




// ===========================================================================================================
//      Missions Shared data
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      On-The-Fly Group Data
// -----------------------------------------------------------------------------------------------------------

// STRUCT containing basic group information for missions
STRUCT g_structPendingActivityGroups
	VECTOR						msgGroupLocation				// A vector representing the focal location for the group - this is used to gather other nearby missions into the group
	INT							msgNumInGroup		= 0			// The number of missions in this group
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------
//      Activity Data
// -----------------------------------------------------------------------------------------------------------

//// STRUCT containing the shared details for an activity
//STRUCT g_structPendingActivities_OLD
//	BOOL						paInUse				= FALSE								// TRUE if this slot is in use, FALSE if not in use
//	MP_MISSION_ID_DATA_OLD		paMissionIdData											// The Mission Identification Data
//	VECTOR						paLocation												// The Activity Location
//	enumCharacterList			paContact			= NO_CHARACTER						// The Contact
//	INT							paRank				= 0									// The Activity Rank
//	INT							paGroupSlot			= ILLEGAL_PENDING_ACTIVITIES_SLOT	// The Slot containing the Group Details for this mission
//	INT							paPlayersReg		= ALL_PLAYER_BITS_CLEAR				// A bitfield of players that registered this activity
//ENDSTRUCT

STRUCT g_structPendingActivities
	BOOL						paInUse				= FALSE								// TRUE if this slot is in use, FALSE if not in use
	MP_MISSION_ID_DATA			paMissionIdData											// The Mission Identification Data
	VECTOR						paLocation												// The Activity Location
	enumCharacterList			paContact			= NO_CHARACTER						// The Contact
	INT							paRank				= 0									// The Activity Rank
	INT							paGroupSlot			= ILLEGAL_PENDING_ACTIVITIES_SLOT	// The Slot containing the Group Details for this mission
	INT							paPlayersReg		= ALL_PLAYER_BITS_CLEAR				// A bitfield of players that registered this activity
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Any server general control data for maintaining pending activities
STRUCT g_structPAServerControlData
	INT							paccNextInSchedule		= 0								// The next activity to be maintained by the scheduled update
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Local Client Pending Activities data
STRUCT g_structLocalClientPendingActivities
	BOOL						lcpaStartSendingData	= FALSE							// TRUE when the server has told this player that they can send their Pendign Activities data
	PLAYER_INDEX				lcpaHost												// The Host Player that requested the data
ENDSTRUCT




// ===========================================================================================================
//      Missions Shared Server Cloud Data
//		This is select fields from FMMC_ROCKSTAR_CREATED_STRUCT all of which can be individually filled
//			through functions contained in FM_Mission_Info.sch, together with some control info
// ===========================================================================================================

// The data that is a subset of the FMMC_ROCKSTAR_CREATED_STRUCT data
// NOTE: This is going to be replaced when Bobby re-organises the FMMC_ROCKSTAR_CREATED_STRUCT into an array of structs (currently it's a struct with arrays)
//STRUCT g_structMSCloudHeaderData_OLD
//	TEXT_LABEL_23 				tlName														// The filename
//	TEXT_LABEL_23				tlOwner														// The Owner
//	TEXT_LABEL_31				tlMissionName												// The Mission Name
//	TEXT_LABEL_63				tlMissionDec												// The Mission Description
//	VECTOR						vStartPos													// The Start Location
//	VECTOR						vCamPos														// The Camera Coords
//	FLOAT						fCamHead				= 0.0								// The Camera Heading
//	INT							iType					= -1								// FMMC Mission Type ID
//	INT							iSubType				= -1								// Mission Subtype
//	INT							iMinPlayers				= -1								// Minimum Players
//	INT							iRank					= -1								// Minimum Rank to Play Mission
//	INT							iMaxPlayers				= -1								// Maximum Players
//	INT							iRating					= -1								// Community Rating for the mission
//	INT							iLesRating				= -1								// Les' Rating - probably temp
//	INT							iContactCharEnum		= 0									// Mission Contact Hash as loaded from the cloud that can be converted to an enumCharacterList
//	INT							iLaunchTimesBit			= MATC_ALL_DAY						// Bitfield where the first 24 bits represent the 24 hours of a day
//	INT							iBitSet					= ALL_MISSIONS_SHARED_BITS_CLEAR	// Bitset loaded from the cloud
//ENDSTRUCT


STRUCT g_structMSCloudHeaderData
	TEXT_LABEL_23 				tlName														// The filename
	TEXT_LABEL_23				tlOwner														// The Owner
	TEXT_LABEL_63				tlMissionName												// The Mission Name
	TEXT_LABEL_63				tlMissionDec												// The Mission Description
	VECTOR						vStartPos													// The Start Location
	VECTOR						vCamPos														// The Camera Coords
	VECTOR						vCamHead													// The Camera Heading
	INT							iType					= -1								// FMMC Mission Type ID
	INT							iMissionDecHash			= 0									// Hash value for retrieving the 500-char Mission Description
	INT							iSubType				= -1								// Mission Subtype
	INT							iMinPlayers				= -1								// Minimum Players
	INT							iRank					= -1								// Minimum Rank to Play Mission
	INT							iMaxPlayers				= -1								// Maximum Players
	INT							iRating					= -1								// Community Rating for the mission
	INT							iLesRating				= -1								// Les' Rating - probably temp
	INT							iContactCharEnum		= 0									// Mission Contact Hash as loaded from the cloud that can be converted to an enumCharacterList
	INT							iLaunchTimesBit			= MATC_ALL_DAY						// Bitfield where the first 24 bits represent the 24 hours of a day
	INT							iBitSet					= ALL_MISSIONS_SHARED_BITS_CLEAR	// Bitset loaded from the cloud
	INT							iPhotoPath				= 2									// Path for the photo
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Cloud Data Control Bitflags
CONST_INT		MSCLOUD_BITFLAG_GOT_DESCRIPTION_DATA	0	// TRUE if the mission description data block has been received
CONST_INT		MSCLOUD_BITFLAG_GOT_NAMES_DATA			1	// TRUE if the mission names data block has been received (mission name, user name)
CONST_INT		MSCLOUD_BITFLAG_GOT_DETAILS_DATA		2	// TRUE if the general mission details have been received (min/max players, rank, etc)
CONST_INT		MSCLOUD_BITFLAG_ALL_DATA_RECEIVED		3	// TRUE when all of BASIC, DESCRIPTION, NAMES, DETAILS data blocks have been received
CONST_INT		MSCLOUD_BITFLAG_RESTRICT_PLAYERS		4	// TRUE if the mission becomes not joinable or started and the players allowed on the mission become restricted


// -----------------------------------------------------------------------------------------------------------

// All the data for one shared cloud-loaded mission, including the control data for that mission
//STRUCT g_structMissionSharedCloudData_OLD
//	BOOL						mscInUse				= FALSE								// TRUE if this cloud data block is in use, FALSE if available
//	BOOL						mscBeenUsed				= FALSE								// TRUE if this block has been used or in use, FALSE if not used
//	INT							mscUniqueID				= NO_UNIQUE_ID						// The mission request uniqueID - identifies one specific mission being setup/launched
//	INT							mscPlayersReg			= ALL_PLAYER_BITS_CLEAR				// A bitfield of all players that required this cloud header details to be stored
//	INT							mscBitflags				= ALL_MISSIONS_SHARED_BITS_CLEAR	// A bitfield of control flags
//	MP_MISSION					mscMissionID			= eNULL_MISSION						// The missionID for this cloud data
//	INT							mscVariation			= NO_MISSION_VARIATION				// The mission Variation for this cloud data
//	INT							mscCreatorID			= ILLEGAL_CREATOR_ID				// The creatorID for this cloud data
//	g_structMSCloudHeaderData_OLD	mscHeaderData												// The header data copied from the global cloud arrays
//ENDSTRUCT

// KGM 17/8/14: An ENUM to describe the use of the cutscene data
// This is to prevent adding multiple BOOLs to registered server data
ENUM g_eForCutsceneTypes
	FOR_CUT_NONE,					// This is not for a cutscene
	FOR_CUT_HEIST_INTRO,			// Cutscene is for a Heist Intro Cutscene
	FOR_CUT_HEIST_MID_STRAND,		// Cutscene is for a Heist Mid-Strand Cutscene
	FOR_CUT_HEIST_TUTORIAL_INTRO,	// Cutscene is for a Heist Tutorial Intro Cutscene
	
	// Leave this at the bottom
	FOR_CUT_ERROR
ENDENUM

STRUCT g_structMissionSharedCloudData
	BOOL						mscInUse				= FALSE								// TRUE if this cloud data block is in use, FALSE if available
	BOOL						mscBeenUsed				= FALSE								// TRUE if this block has been used or in use, FALSE if not used
	INT							mscUniqueID				= NO_UNIQUE_ID						// The mission request uniqueID - identifies one specific mission being setup/launched
	INT							mscPlayersReg			= ALL_PLAYER_BITS_CLEAR				// A bitfield of all players that required this cloud header details to be stored
	INT							mscBitFirstPlayerReg	= ALL_PLAYER_BITS_CLEAR				// The bitflag of the first player registered needs to be stored for heist-related missions
	INT							mscBitflags				= ALL_MISSIONS_SHARED_BITS_CLEAR	// A bitfield of control flags
	MP_MISSION					mscMissionID			= eNULL_MISSION						// The missionID for this cloud data
	INT							mscVariation			= NO_MISSION_VARIATION				// The mission Variation for this cloud data
	INT							mscCreatorID			= ILLEGAL_CREATOR_ID				// The creatorID for this cloud data
	g_eForCutsceneTypes			mscForCutsceneID		= FOR_CUT_NONE						// Describes if this data is for normal mission triggering, or being used to trigger a Heist Intro or Mid-Strand cutscene
	g_structMSCloudHeaderData	mscHeaderData												// The header data copied from the global cloud arrays
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Any server general control data for amintaining the shared cloud-loaded missions
STRUCT g_structMSCloudServerControlData
	INT							msccNextInSchedule		= 0									// The next mission to be maintained by the scheduled update
ENDSTRUCT




// ===========================================================================================================
//      Contains all ServerBD structs, so this one struct can be included in the Global BD data
// ===========================================================================================================

// NOTE: This is stored as global server broadcast data: GlobalServerBD_MissionsShared
//STRUCT g_structMissionsSharedServerBD_OLD
//	INT								playerBitsReadyToSend								// A bitfield of players ready to send their Pending Activities initialisation data
//	TIME_DATATYPE					currentPlayerReadyToSendTimeout						// Don't need to know which player is sending data, but on timeout move on to next player - this should be for safety - the player will send an event when all events sent to server
//	g_structPendingActivities_OLD		activities[MAX_PENDING_ACTIVITIES]					// Activity details for Pending Activities (the ID Data)
//	g_structPendingActivityGroups		groups[MAX_PENDING_ACTIVITIES_GROUPS]				// Group info for Pending Activities
//	g_structMissionSharedCloudData_OLD	cloudDetails[MAX_MISSIONS_SHARED_CLOUD_DATA]		// A copy of the cloud header details for currently available cloud-loaded missions (ie: usually missions being activated)
//ENDSTRUCT
	
STRUCT g_structMissionsSharedServerBD
	INT								playerBitsReadyToSend								// A bitfield of players ready to send their Pending Activities initialisation data
	TIME_DATATYPE					currentPlayerReadyToSendTimeout						// Don't need to know which player is sending data, but on timeout move on to next player - this should be for safety - the player will send an event when all events sent to server
	g_structPendingActivities		activities[MAX_PENDING_ACTIVITIES]					// Activity details for Pending Activities (the ID Data)
	g_structPendingActivityGroups		groups[MAX_PENDING_ACTIVITIES_GROUPS]				// Group info for Pending Activities
	g_structMissionSharedCloudData	cloudDetails[MAX_MISSIONS_SHARED_CLOUD_DATA]		// A copy of the cloud header details for currently available cloud-loaded missions (ie: usually missions being activated)
ENDSTRUCT



// ===========================================================================================================
//      Missions Shared Client Cloud Data
// ===========================================================================================================

// Cloud Data Client Control Bitflags
CONST_INT		MSCCLIENT_BITFLAG_MISSION_PROCESSED			0	// TRUE if a shared cloud-loaded mission has been checked and dealt with by this player, FALSE if it is new and still needs dealt with
CONST_INT		MSCCLIENT_BITFLAG_ADDED_SHARED_MISSION		1	// TRUE if a shared cloud-loaded mission had to be locally registered using the shared data, FALSE if already registered using local data 
CONST_INT		MSCCLIENT_BITFLAG_MISSION_REMOVED			2	// TRUE if a shared cloud-loaded mission was removed because the player wasn't on it when it started, FALSE if player was on it or the mission wasn't added in the first place 
CONST_INT		MSCCLIENT_BITFLAG_RESTRICTED_CONTENT		3	// TRUE if this shared cloud-loaded mission contains content restricted for this player, FALSE if it is not restricted


// -----------------------------------------------------------------------------------------------------------

// A struct containing all client Missions Shared Cloud-Loaded data
STRUCT g_structMSCloudClientData
	INT 						msccdBitfield 					= ALL_MISSIONS_SHARED_BITS_CLEAR		// A bitfield to control the shared missions on this client
	INT							msccdMatcRegID					= ILLEGAL_AT_COORDS_ID					// Stores the Missions At Coords registration ID so that the local mission can be deleted when no longer available
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// A struct containing all client Missions Shared Cloud-Loaded Control data
STRUCT g_structMSCloudClientControlData
	INT 						mscccdNextInMissionSchedule 	= 0										// A scheduling variable for cloud-loaded shared mission scheduling
	g_structMSCloudClientData	mscccdMissions[MAX_MISSIONS_SHARED_CLOUD_DATA]							// A struct containing an entry for each shared mission entry
ENDSTRUCT




// ===========================================================================================================
//      A TRANSITION-SAFE COPY OF THE SHARED MISSION DATA THE PLAYER IS MOST RECENTLY REGISTERED ON
//		(NOTE: This was added to solve the problem where the player plays another player's UGC
//			and wants to play again  when it is over but the UGC owner doesn't want to play again.
//			Storing the data ensures the mission can be setup again without the UGC owner.)
// ===========================================================================================================

//STRUCT g_structCopyRegSharedMissionData_OLD
//	MP_MISSION					crsmdMissionID			= eNULL_MISSION						// The missionID for this copy data
//	INT							crsmdVariation			= NO_MISSION_VARIATION				// The mission Variation for this copy data
//	INT							crsmdCreatorID			= ILLEGAL_CREATOR_ID				// The creatorID for this copy data
//	g_structMSCloudHeaderData_OLD	crsmdHeaderData												// The copied header data
//ENDSTRUCT

// A copy of the player's most recent shared mission data that the player was registered on.
// This data can be used under specific circumstances to setup the mission again after a transition.
STRUCT g_structCopyRegSharedMissionData
	MP_MISSION					crsmdMissionID			= eNULL_MISSION						// The missionID for this copy data
	INT							crsmdVariation			= NO_MISSION_VARIATION				// The mission Variation for this copy data
	INT							crsmdCreatorID			= ILLEGAL_CREATOR_ID				// The creatorID for this copy data
	g_structMSCloudHeaderData	crsmdHeaderData												// The copied header data
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Transition-safe description of how the player is entering the game, used to decide if the above Copy of the Shared Mission Data should be used or not
ENUM g_eUseCopyMissionReason
	COPY_MISSION_USE_IS_PLAY_AGAIN,							// The player has chosen to play the same mission again, so the copy data will be needed if the mission was another player's UGC
	
	// Leave this at the bottom
	COPY_MISSION_NOT_REQUIRED								// The copy mission data is not required to be used
ENDENUM




// ===========================================================================================================
//      A TRANSITION-SAFE STORE OF A RANDOM MISSION TO BE PRIORITISED AFTER TRANSITION
//		(NOTE: This was added because choosing 'Random' after completing a mission started to use a
//			transition but the mission wasn't being prioritised or unlocked after the transition)
// ===========================================================================================================

// Stores details of a random mission to be prioritised after transition
// NOTE: This was added to store the details of a randomly selected next mission chosen after a mission had completed
// NOTE: Clear_Transition_Random_Mission() in net_transition_sessions_private.sch allows these to be cleared
// KGM 7/9/13: Also now using these for Play Again because both use the Restart_Random Transition
STRUCT g_structRandomMission
	BOOL					tmIsRandomMission = FALSE				// TRUE for a Random Mission, but ow FALSE means a Play Again mission
	TEXT_LABEL_23			tmRandomMissionFilename					// The cloud filename of the random or Play Again mission to be prioritised after transition
ENDSTRUCT




// ===========================================================================================================
//      HEADER DATA GENERATED FROM THE FULL MISSION DATA AFTER CROSS-SESSION TRANSITION
//		(NOTE: This is used if a cross-session player accepts an invite into another player's UGC
//		because the UGC header data will not be available - however the UGC full-mission data is
//		available and therefore the header data can be generated from it)
// ===========================================================================================================

//STRUCT g_structGeneratedCloudHeaderData_OLD
//	BOOL						gchdInUse						= FALSE									// TRUE if the generated header data is currently being used, FALSE if not
//	g_structMSCloudHeaderData_OLD	gchdHeaderData															// The generated header data
//ENDSTRUCT


// A struct containing the header data generated from the full-mission data
STRUCT g_structGeneratedCloudHeaderData
	BOOL						gchdInUse						= FALSE									// TRUE if the generated header data is currently being used, FALSE if not
	g_structMSCloudHeaderData	gchdHeaderData															// The generated header data
ENDSTRUCT




// ===========================================================================================================
//      HEADER DATA STORED IF THIS IS THE PART ONE OF A STRAND MISSION
//		This is stored in case the part two mission Quick Restarts at Part One and the Part One corona
//		needs setup again. The intention is that this will get filled by Mission Triggerer after launch.
//		On a Quick Restart, the data will be used to setup the corona again.
// ===========================================================================================================

// A struct containing the header data for the Part One mission
STRUCT g_structStrandMissionP1HeaderData
	BOOL						smp1hdInUse						= FALSE									// TRUE if the Stored Part One header data is currently being used, FALSE if not
	g_structMSCloudHeaderData	smp1hdHeaderData														// The Stored Part One header data
ENDSTRUCT
