
USING "mp_globals_missions_at_coords_definitions.sch"




//	Move this to a separate title_update_globals_definitions.sch
STRUCT g_structMatCAngledAreaMP_TitleUpdate
	VECTOR				matcaaBlipPos													// The position of the blip within the Angled Area. Moved from g_structMatCAngledAreaMP
ENDSTRUCT




// ===========================================================================================================
//      long-Range Blips for Missions At Coords globals
// ===========================================================================================================

// The max number of each type of blip that can appear long-range on the radar
CONST_INT	MAX_NUM_LONG_RANGE_BLIPS_PER_TYPE		1

// A float to indicate an invalid distance
CONST_FLOAT	INVALID_BLIP_DISTANCE					99990.0

// An enum description for the types of blips that will always appear long-range on the radar
ENUM g_eLongRangeBlipTypes
	LRBT_MISSION,
	LRBT_DEATHMATCH,
	LRBT_RACE,
	LRBT_SURVIVAL,
	LRBT_BASEJUMP,
	
	// Leave this at the bottom
	MAX_LONG_RANGE_BLIP_TYPES,
	NO_LONG_RANGE_BLIP_TYPE
ENDENUM

// Size of the array to hold potential initial display blips
CONST_INT	INITIAL_LONG_RANGE_BLIPS_ARRAY_SIZE		15	//(MAX_LONG_RANGE_BLIP_TYPES * MAX_NUM_LONG_RANGE_BLIPS_PER_TYPE)

// A struct containing hte initialisation data only, so the initial display blips can be identified but switch on deferred
STRUCT g_structInitialBlips
	INT								regID					= ILLEGAL_AT_COORDS_ID					// The regID for a potential initial blip for display
	BLIP_INDEX						blipIndex				= NULL									// The blip index of the blip potentially going to be marked as long-range initially
ENDSTRUCT

// A struct containing the details for one blip
STRUCT g_structOneLongRangeBlip
	INT								regID					= ILLEGAL_AT_COORDS_ID					// The REG_ID for the mission
	FLOAT							theDistance				= INVALID_BLIP_DISTANCE					// The blip's distance from the player
	BLIP_INDEX						blipIndex				= NULL									// The blip index of the blip, so it can be instantly returned to short-range if overwritten
ENDSTRUCT

// A struct containing all the blip details for one type
STRUCT g_structLongRangeBlipsOfType
	FLOAT							farthestBlipOfType		= INVALID_BLIP_DISTANCE					// The farthest of this type of long-range blip
	g_structOneLongRangeBlip		blipsOfType[MAX_NUM_LONG_RANGE_BLIPS_PER_TYPE]					// An array of the closest of the long-range blips per type
ENDSTRUCT

// A struct containing all the long-range blips
STRUCT g_structAllLongRangeBlips
	g_structLongRangeBlipsOfType	allBlips[MAX_LONG_RANGE_BLIP_TYPES]								// All long-range blips
	BOOL							allowUpdates			= FALSE									// FALSE until ready to do the initial run-through of all missions is performed to get an initial status
	BOOL							gatherDataOnly			= FALSE									// TRUE during the initial run through to gather the data but not display it
	g_structInitialBlips			potentialInitialBlips[INITIAL_LONG_RANGE_BLIPS_ARRAY_SIZE]		// An array containing the potential initial long-range blips for display
ENDSTRUCT




// ===========================================================================================================
//      Quick Update All Missions globals
// ===========================================================================================================

// BITFLAGS of reasons for doing a Quick Update of All Missions - because some require additional processing at the end
CONST_INT	QUICK_UPDATE_REASON_BITFLAG_LONG_RANGE_BLIPS		0							// Gathering the initial data for displaying the long-range blips
CONST_INT	QUICK_UPDATE_REASON_BITFLAG_FLASHING_BLIPS			1							// Gathering the data to allow newly unlocked blips to flash
CONST_INT	QUICK_UPDATE_REASON_BITFLAG_ALL_MISSIONS			2							// A general all-missions quick update (actually added for blips again, when warping out of a garage)

// The number of missions to process in one frame
CONST_INT	QUICK_UPDATE_NUM_MISSIONS_PER_FRAME					50

// A struct containing the controls for a Quick Update of All Missions, staggered over multiple frames
STRUCT g_structQuickUpdateAllMissions
	BOOL					isActive				= FALSE									// TRUE if a Quick Update is active
	INT						nextSlot				= 0										// The next slot to process (used as a bookmark during the staggered processing)
	INT						bitsReasons				= ALL_MISSIONS_AT_COORDS_BITS_CLEAR		// A bitfield of reasons for the Quick Update - because some work may need done when finished
ENDSTRUCT



// ===========================================================================================================
//      Missions At Coords Additional TU Initial Actions globals
// ===========================================================================================================

STRUCT g_structMatcInitialBlipsMP_TU
	BOOL							matcibCompleted			= FALSE		// TRUE when the check for any initial long range blips has been performed, FALSE if still to be performed
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Controls TU additional initial actions
STRUCT g_structMatcInitialActionsMP_TU
	g_structMatcInitialBlipsMP_TU		matciaLongRangeBlips				// Controls the initial long-range blips action (the updated TU usage version)
ENDSTRUCT




// ===========================================================================================================
//      Missions At Coords Visuals for Walk-Ins globals
// ===========================================================================================================

// A struct containing variables that control the visuals when a player walks into a corona (ie: corona flash, retaining locate message for a few frames, etc)
STRUCT g_structFocusMissionVisuals
	BOOL							visualsActive			= FALSE									// TRUE if the player has just walked into a corona and some visuals need controlled
	BOOL							coronaFlashActive		= FALSE									// TRUE if the corona flash visuals are required and still ongoing
	BOOL							coronaNoFlashActive		= FALSE									// TRUE if the corona is required to remain but without doing the flash
	INT								coronaFlashStartTime	= 0										// The time the corona flash animation started
	VECTOR							coronaPosition													// The corona position
	VECTOR							coronaScale														// The corona scale
	BOOL							retainedCoronaActive	= FALSE									// TRUE if the corona visuals are being retained as focus mission activated
	INT								retainedLocateMessage	= 0										// Clear the bitfield of locate message control flags
	HUD_COLOURS						coronaNoFlashColour		= HUD_COLOUR_WHITE						// The HUD_COLOUR if the corona needs to remain but without the flash animation
ENDSTRUCT



// ===========================================================================================================
//      Missions At Coords Exclusive ContentID globals
//		(Used by Heists to reserve a contentID for exclusive triggering even if corona not yet registered)
// ===========================================================================================================

// The length of time the contntID should remain exclusive
CONST_INT	MATC_EXCLUSIVE_CONTENTID_TIME_msec				60000

// A struct containing the exclusive corona control variables
STRUCT g_structMatcExclusiveCorona
	INT								matcecContentIdHash		= 0										// The contentID of the corona that should become temporarily exclusive
	INT								matcexTimeout			= 0										// A safety timeout for when this exclusivity should end
ENDSTRUCT



// ===========================================================================================================
//      Missions At Coords 'Hide Job Blip' PI Menu COmmunication globals
//		(Used when the PI Menu detects a jobtype blip/corona display status change)
// ===========================================================================================================

BOOL g_matcImmediateUpdateForPIMenuHideBlip = FALSE








