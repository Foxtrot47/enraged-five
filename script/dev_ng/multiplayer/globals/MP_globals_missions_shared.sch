USING "rage_builtins.sch"

USING "MP_globals_missions_shared_definitions.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_missions_shared.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains global variable instances for all MP Missions Shared routines.
//
//		NOTES			:	RETRIEVING CLOUD HEADER DATA - KGM 13/3/13
//							The Cloud Header data is going to have to be retrieved by splitting the data over multiple events.
//
//							CLOUD DATA BEEN USED - KGM 13/3/13
//							I can see potential problems with an array element becoming 'not in use' and then immediately becoming
//							'in use' again with different data. To prevent these edge cases I've added a 'been used' flag. When looking
//							for an empty slot it will favour those not in use and not been used. Only when all have 'been used' will it
//							clear out those flags from all slots that are no longer 'in use' and start to re-use them.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


// ===========================================================================================================
//      Missions Shared data
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Activity Data
// -----------------------------------------------------------------------------------------------------------

// Any server general control data for maintaining pending activities
g_structPAServerControlData	g_sPACloudControlServer										// Control variables to oversee the shared cloud-loaded missions maintenance


// -----------------------------------------------------------------------------------------------------------

g_structLocalClientPendingActivities	g_sPALocalClient





// ===========================================================================================================
//      Missions Shared Server Cloud Data
//		This is select fields from FMMC_ROCKSTAR_CREATED_STRUCT all of which can be individually filled
//			through functions contained in FM_Mission_Info.sch, together with some control info
// ===========================================================================================================

// Any server general control data for amintaining the shared cloud-loaded missions
g_structMSCloudServerControlData	g_sMSCloudControlServer									// Control variables to oversee the shared cloud-loaded missions maintenance




// ===========================================================================================================
//      Contains all ServerBD structs, so this one struct can be included in the Global BD data
// ===========================================================================================================

// NOTE: This is stored as global server broadcast data: GlobalServerBD_MissionsShared
// KGM 25/2/13: Missions Shared Data - needs to be broadcast in case the host migrates
//g_structMissionsSharedServerBD_OLD			GlobalServerBD_MissionsShared_OLD
//
//


// ===========================================================================================================
//      Missions Shared Client Cloud Data
// ===========================================================================================================

// A struct containing all client Missions Shared Cloud-Loaded Control data
g_structMSCloudClientControlData	g_MSCloudClientControl




// ===========================================================================================================
//      A TRANSITION-SAFE COPY OF THE SHARED MISSION DATA THE PLAYER IS MOST RECENTLY REGISTERED ON
//		(NOTE: This was added to solve the problem where the player plays another player's UGC
//			and wants to play again  when it is over but the UGC owner doesn't want to play again.
//			Storing the data ensures the mission can be setup again without the UGC owner.)
// ===========================================================================================================

// A copy of the player's most recent shared mission data that the player was registered on.
// This data can be used under specific circumstances to setup the mission again after a transition.
//g_structCopyRegSharedMissionData_OLD	g_sCopyRegSharedMissionData_OLD
//

// -----------------------------------------------------------------------------------------------------------

// Transition-safe description of how the player is entering the game, used to decide if the above Copy of the Shared Mission Data should be used or not
g_eUseCopyMissionReason g_eUseOfCopyRegSharedMissionData = COPY_MISSION_NOT_REQUIRED		// The use of the copy shared mission data




// ===========================================================================================================
//      A TRANSITION-SAFE STORE OF A RANDOM MISSION TO BE PRIORITISED AFTER TRANSITION
//		(NOTE: This was added because choosing 'Random' after completing a mission started to use a
//			transition but the mission wasn't being prioritised or unlocked after the transition)
// ===========================================================================================================

// Stores details of a random mission to be prioritised after transition
// NOTE: This was added to store the details of a randomly selected next mission chosen after a mission had completed
// NOTE: Clear_Transition_Random_Mission() in net_transition_sessions_private.sch allows these to be cleared
g_structRandomMission	g_sRandomMission




// ===========================================================================================================
//      HEADER DATA GENERATED FROM THE FULL MISSION DATA AFTER CROSS-SESSION TRANSITION
//		(NOTE: This is used if a cross-session player accepts an invite into another player's UGC
//		because the UGC header data will not be available - however the UGC full-mission data is
//		available and therefore the header data can be generated from it)
// ===========================================================================================================

// A struct containing the header data generated from the full-mission data
//g_structGeneratedCloudHeaderData_OLD	g_sGeneratedCloudHeaderData_OLD
