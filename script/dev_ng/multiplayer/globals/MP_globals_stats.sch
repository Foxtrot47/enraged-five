
USING "types.sch"
USING "commands_stats.sch"

USING "MP_globals_stats_definitions.sch"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// VARIABLES FOR AWARDS/ /////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD
//INT iDebugExtraPoints[MAX_NUM_MPPLY_INT_AWARD]
//FLOAT fStatCurrent
//FLOAT fStatMin
//FLOAT fStatMax
//FLOAT fStatPercent
//BOOL bClearAllStats
//BOOL bForceDisplayOnIncrement
//FLOAT fStuntmanDebugVehicleRoll
#ENDIF


BOOL G_B_HAS_PLAYER_ACCEPTED_GAME_INVITE = FALSE

//MPGlobalsStatsStruct_OLD MPGlobalsStats_Old

////New award Stuct - Brenda - Hopefully delete the above in due time
//STRUCT MPGlobalsStatTrackingStruct
//
//	INT AwardDenominator[TOTAL_NUMBER_OF_AWARDS]
//
//ENDSTRUCT
//MPGlobalsStatTrackingStruct MPGlobalsStatTracking


//STATSENUM MPIntStatNamesArray_Old[MAX_NUM_MP_INT_STATS_OLD][MAX_NUM_CHARACTER_SLOTS]
//STATSENUM MPFloatStatNamesArray_Old[MAX_NUM_MP_FLOAT_STATS_OLD][MAX_NUM_CHARACTER_SLOTS]
//STATSENUM MPBoolStatNamesArray_Old[MAX_NUM_MP_BOOL_STATS_OLD][MAX_NUM_CHARACTER_SLOTS]
STATSENUM MPLabelStatNamesArray[MAX_NUM_MP_LABEL_STATS][MAX_NUM_CHARACTER_SLOTS]
//STATSENUM MPStringStatNamesArray_Old[MAX_NUM_MP_STRING_STATS_OLD][MAX_NUM_CHARACTER_SLOTS]
//STATSENUM MPDateStatNamesArray_Old[MAX_NUM_MP_DATE_STATS_OLD][MAX_NUM_CHARACTER_SLOTS]
//STATSENUM MPVectorStatNamesArray_Old[MAX_NUM_MP_VECTOR_STATS_OLD][MAX_NUM_CHARACTER_SLOTS]
STATSENUM MPUserIdStatNamesArray[MAX_NUM_MP_USERID_STATS][MAX_NUM_CHARACTER_SLOTS]

//MPPLY_INT_STATS MPPlyIntStatNamesArray[MAX_NUM_MPPLY_INT_STATS]
//MPPLY_FLOAT_STATS MPPlyFloatStatNamesArray[MAX_NUM_MPPLY_FLOAT_STATS]
//MPPLY_BOOL_STATS MPPlyBoolStatNamesArray[MAX_NUM_MPPLY_BOOL_STATS]
//MPPLY_STRING_STATS MPPlyStringStatNamesArray[MAX_NUM_MPPLY_STRING_STATS]
//MPPLY_LABEL_STATS MPPlyLabelStatNamesArray[MAX_NUM_MPPLY_LABEL_STATS]
//MPPLY_DATE_STATS MPPlyDateStatNamesArray[MAX_NUM_MPPLY_DATE_STATS]
//MPPLY_DATE_AWARD MPPlyDateAwardNamesArray[MAX_NUM_MPPLY_DATE_STATS]
//MPPLY_VECTOR_STATS MPPlyVectorStatNamesArray[MAX_NUM_MPPLY_VECTOR_STATS]
//
//MPGEN_INT_STATS MPGenIntStatNamesArray[MAX_NUM_MPGEN_INT_STATS]
//MPGEN_FLOAT_STATS MPGenFloatStatNamesArray[MAX_NUM_MPGEN_FLOAT_STATS]
//MPGEN_BOOL_STATS MPGenBoolStatNamesArray[MAX_NUM_MPGEN_BOOL_STATS]
//MPGEN_STRING_STATS MPGenStringStatNamesArray[MAX_NUM_MPGEN_STRING_STATS]
//MPGEN_LABEL_STATS MPGenLabelStatNamesArray[MAX_NUM_MPGEN_LABEL_STATS]
//MPGEN_DATE_STATS MPGenDateStatNamesArray[MAX_NUM_MPGEN_DATE_STATS]
//MPGEN_VECTOR_STATS MPGenVectorStatNamesArray[MAX_NUM_MPGEN_VECTOR_STATS]


//STATSENUM MPAwdIntStatNamesArray_Old[MAX_NUM_MP_INT_AWARD_OLD][MAX_NUM_CHARACTER_SLOTS]
STATSENUM MPAwdFloatStatNamesArray[MAX_NUM_MP_FLOAT_AWARD][MAX_NUM_CHARACTER_SLOTS]
//STATSENUM MPAwdBoolStatNamesArray_Old[MAX_NUM_MP_BOOL_AWARD_OLD][MAX_NUM_CHARACTER_SLOTS]
STATSENUM MPAwdUserIdStatNamesArray[MAX_NUM_MP_USERID_AWARD][MAX_NUM_CHARACTER_SLOTS]

MPPLY_INT_AWARD MPAwdPlyIntStatNamesArray[MAX_NUM_MPPLY_INT_AWARD]
MPPLY_FLOAT_AWARD MPAwdPlyFloatStatNamesArray[MAX_NUM_MPPLY_FLOAT_AWARD]
MPPLY_BOOL_AWARD MPAwdPlyBoolStatNamesArray[MAX_NUM_MPPLY_BOOL_AWARD]
MPPLY_DATE_AWARD MPAwdPlyDateStatNamesArray[MAX_NUM_MPPLY_DATE_AWARD]
//
//MPGEN_INT_AWARD MPAwdGenIntStatNamesArray[MAX_NUM_MPGEN_INT_AWARD]
//MPGEN_FLOAT_AWARD MPAwdGenFloatStatNamesArray[MAX_NUM_MPGEN_FLOAT_AWARD]
//MPGEN_BOOL_AWARD MPAwdGenBoolStatNamesArray[MAX_NUM_MPGEN_BOOL_AWARD]
//MPGEN_DATE_AWARD MPAwdGenDateStatNamesArray[MAX_NUM_MPGEN_DATE_AWARD]

//MP_BOOL_AWARD MPBAwdBeenGiven_Old[MAX_NUM_MP_BOOL_AWARD_OLD]


// ***********************************************************************************************************************************************
// ***********************************************************************************************************************************************
//								STATS THAT GET READ OFTEN GO THROUGH THE EVENT SYSTEM - speak to Neil F.
// ***********************************************************************************************************************************************
// ***********************************************************************************************************************************************
BOOL g_bHeavilyAccessedStatsInitialised

// MPPLY STATS
INT g_MPPLY_CREW_0_ID
INT g_MPPLY_CREW_1_ID
INT g_MPPLY_CREW_2_ID
INT g_MPPLY_CREW_3_ID
INT g_MPPLY_CREW_4_ID

INT g_MPPLY_CREW_LOCAL_XP_0
INT g_MPPLY_CREW_LOCAL_XP_1
INT g_MPPLY_CREW_LOCAL_XP_2
INT g_MPPLY_CREW_LOCAL_XP_3
INT g_MPPLY_CREW_LOCAL_XP_4

INT g_MPPLY_BECAME_CHEATER_NUM
INT g_MPPLY_FRIENDLY
INT g_MPPLY_OFFENSIVE_LANGUAGE
INT g_MPPLY_GRIEFING
INT g_MPPLY_HELPFUL
INT g_MPPLY_OFFENSIVE_TAGPLATE
INT g_MPPLY_OFFENSIVE_UGC

BOOL gcompletedallplatnumawards
//INT g_MPPLY_LAST_MP_CHAR

// MP AWD
////INT g_MPPLY_AWD_FM_CR_MISSION_SCORE

// MP STATS
INT g_MP_STAT_GOLD_INTCHAR_0[5] 
INT g_MP_STAT_GOLD_INTCHAR_1[5]
INT g_MP_STAT_GOLD_INTCHAR_2[5]
INT g_MP_STAT_GOLD_INTCHAR_3[5]
INT g_MP_STAT_GOLD_INTCHAR_4[5]
INT g_MP_STAT_SILVER_INTCHAR_0[5] 
INT g_MP_STAT_SILVER_INTCHAR_1[5]
INT g_MP_STAT_SILVER_INTCHAR_2[5]
INT g_MP_STAT_SILVER_INTCHAR_3[5]
INT g_MP_STAT_SILVER_INTCHAR_4[5]
INT g_MP_STAT_BRONZE_INTCHAR_0[5] 
INT g_MP_STAT_BRONZE_INTCHAR_1[5]
INT g_MP_STAT_BRONZE_INTCHAR_2[5]
INT g_MP_STAT_BRONZE_INTCHAR_3[5]
INT g_MP_STAT_BRONZE_INTCHAR_4[5]
INT g_MP_STAT_PLATINUM_INTCHAR_0[5]
INT g_MP_STAT_PLATINUM_INTCHAR_1[5]
INT g_MP_STAT_PLATINUM_INTCHAR_2[5]
INT g_MP_STAT_PLATINUM_INTCHAR_3[5]
INT g_MP_STAT_PLATINUM_INTCHAR_4[5]

INT g_MP_STAT_FM_CUT_DONE[5]
INT g_MP_STAT_FM_CUT_DONE_2[5]
INT g_MP_STAT_CHAR_XP_FM[5]
INT g_MP_STAT_PROPERTY_HOUSE[5]

INT g_MP_STAT_PLATINUM_BOOLCHAR_0[5]
INT g_MP_STAT_PLATINUM_BOOLCHAR_1[5]
INT g_MP_STAT_PLATINUM_BOOLCHAR_2[5]
INT g_MP_STAT_PLATINUM_BOOLCHAR_3[5]
INT g_MP_STAT_PLATINUM_BOOLCHAR_4[5]
INT g_MP_STAT_NO_CLOTHES_UNLOCK[5]

// FOR FAST TRAVEL TELEMETRY
// NOTE: do NOT change the order of this, always add new entries to the end.
ENUM FAST_TRAVEL_TYPE
	FT_TYPE_NULL = 0,
	FT_TYPE_SUBMARINE,
	FT_TYPE_COMPANY_CAR,
	FT_TYPE_CASINO_LIMO_FREEMODE,
	FT_TYPE_YACHT,
	FT_TYPE_CASINO_LIMO_WARP,
	// add the rest
	
	FT_TOTAL // leave at end
ENDENUM


