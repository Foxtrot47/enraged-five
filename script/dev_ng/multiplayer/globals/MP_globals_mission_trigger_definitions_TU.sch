USING "rage_builtins.sch"

USING "MP_GLOBALS_COMMON_CONSTS.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_mission_trigger_Definitions_TU.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains constant, enum, and struct definitions for all MP mission triggering control routines in Title Update.
//							NB. Should not contain instantiated variables.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      JobList globals
// ===========================================================================================================

// The time that a Basic Invite will remain 'closed' for if requested closed by script
CONST_INT	BASIC_INVITE_CLOSED_DELAY_msec				10000

// A timer to indicate a cross-session invite has just been accepted
CONST_INT	CROSS_SESSION_INVITE_ACCEPTED_INITIAL_DELAY_msec		5000
CONST_INT	CROSS_SESSION_INVITE_ACCEPTED_EXTEND_DELAY_msec			1000




// -----------------------------------------------------------------------------------------------------------
//      Current Mission Description - retains it for the duration of the mission
// -----------------------------------------------------------------------------------------------------------

// The current mission description holding struct
STRUCT g_structCurrentMissionDesc
	BOOL									isInUse			= FALSE					// TRUE if the description is in use or is loading, FALSE if not
	BOOL									isLoading		= FALSE					// TRUE if still loading
	INT										descHash		= 0						// The description Hash
	CACHED_MISSION_DESCRIPTION_LOAD_VARS	descLoadVars							// The variables used to load and cleanup the description
ENDSTRUCT




// -----------------------------------------------------------------------------------------------------------
//      Restricted Content Help Message Timeout Value
// -----------------------------------------------------------------------------------------------------------

// The period of time before help message appears again
CONST_INT	RESTRICTED_INVITE_HELP_MESSAGE_TIMEOUT_msec		(60000 * 30)			// every 30 mins




// -----------------------------------------------------------------------------------------------------------
//      Current Mission Type and Subtype
//		(Used when setting up 'part 2' missions because the header details may be different on the cloud
//			to prevent these getting loaded in-game unless required - ie: they may be set as Heist Planning)
// -----------------------------------------------------------------------------------------------------------

// The current mission Type and Subtype Holding struct
STRUCT g_structDetailsForStrandMissions
	INT				dfsmFmmcType		= 	-1					// The fmmcType of the current or most recently triggered mission
	INT				dfsmSubtype			=	-1					// The subtype of the current or most recently triggered mission
ENDSTRUCT




