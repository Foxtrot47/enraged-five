USING "MP_globals_races_definitions.sch"

// -----------------------------------	SERVER BROADCAST DATA ----------------------------------------------------------------------


//The model of car to use for a race
MODEL_NAMES		g_mnMyRaceModel
INT				g_iMyRaceModelChoice
INT				g_iMyCurrentClass
BOOL			g_bCustomCoronaVeh

INT				g_iMyOpenWheelVehicleSelection = -1
INT				g_iMyOpenWheelColourSelection = -1
INT				g_iMyOpenWheelLiverySelection = -1

INT				g_iMyRaceColorSelection
MOD_COLOR_TYPE	g_iMyRaceColorType
INT				g_iMyRaceLiverySelection
INT				g_iMyRaceParachuteSelection
INT				g_iMyRaceLiverySelectionHotring = -2
INT				g_iMyRaceTyresSelection

INT				g_iMyArenaWarsArmourSelection
INT				g_iMyArenaWarsScoopRambarSelection
INT				g_iMyArenaWarsMinesSelection

INT				g_ArenaWarsCurrentLivery
INT				g_ArenaWarsCurrentArmour
INT				g_ArenaWarsCurrentScoopRambar
INT				g_ArenaWarsCurrentMines

INT				g_iArenaWarModLiveryForTelem = -2			// -2 = unchanged, -1 = none, rest are mod index
INT				g_iArenaWarModArmourForTelem = -2			// -2 = unchanged, -1 = none, rest are mod index
INT				g_iArenaWarModWeaponForTelem = -2			// -2 = unchanged, -1 = none, rest are mod index
INT				g_iArenaWarModMinesForTelem = -2			// -2 = unchanged, -1 = none, rest are mod index

INT 			g_iMyRaceColor
INT 			g_iMyRace2ndColor 
INT 			g_iMyPreferedPosition
PLAYER_INDEX	g_piMyPreferedPartner
INT				g_iMyPreferedPartner

INT				g_iMyChosenRadioStation

INT				g_iMyRaceOutfitSelection
INT				g_iMyRaceOutfitEnum

INT				g_iSpecialVehicleModel	=	-1

BOOL			g_bArenaWarsUseForcedStockImperator
BOOL			g_bArenaWarsUseForcedRaceOutfit

// Transition session Rally partner
GAMER_HANDLE					g_ghPreferredPartnerHandle

#IF IS_DEBUG_BUILD
TWEAK_INT iPursuitDispatchDurationOverrideDebug 0
#ENDIF


//GlobalServerBroadcastDataRaces_OLD GlobalServerBD_Races_Old
//GlobalServerBroadcastDataRaces_OLD g_sRC_SB_CoronaOptions_Old

// -----------------------------------	CLIENT BROADCAST DATA ----------------------------------------------------------------------
// 					Everyone can read this data, only the local player can update it. 

GlobalPlayerBroadcastDataRaces GlobalplayerBD_Races[NUM_NETWORK_PLAYERS]



