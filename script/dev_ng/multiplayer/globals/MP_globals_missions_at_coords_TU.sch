USING "rage_builtins.sch"
USING "mp_globals_missions_at_coords_definitions.sch"
USING "mp_globals_missions_at_coords_definitions_TU.sch"

USING "commands_hud.sch"
USING "HUD_COLOURS.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_missions_at_coords_TU.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains global variable instances for all MP missions at coords routines (included in TitleUpdate Global Block).
//		NOTES			:	See MP_globals_missions_at_coords.sch for the majority of the variable declarations.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// NEW BLIP AND CORONA COLOUR SCHEMES
// Bookmarked Missions
CONST_INT	FM_BOOKMARKED_BLIP_COLOUR			BLIP_COLOUR_YELLOW
HUD_COLOURS	FM_BOOKMARKED_CORONA_COLOUR			= HUD_COLOUR_YELLOW
HUD_COLOURS	FM_BOOKMARKED_TEXT_COLOUR			= HUD_COLOUR_YELLOW




// ===========================================================================================================
//      Missions At Coords Radius Blip globals
//		Controls the one Area-Triggered Radius Blip to be displayed - TITLE-UPDATE
// ===========================================================================================================

// A general radius blip used for an area-triggered activity close to the player
g_structMatcRadiusBlip	g_sMatcRadiusBlip

// A retained Gang Attack blip that keeps the blip active until the Gang Attack starts
g_structMatcRadiusBlip	g_sMatcRetainedRadiusBlip
BOOL					g_removeRetainedGABlip




// Mission at coords TU globals.
INT g_iMatccCounterOnDuringAmbTut		= 0	 // Counts the number of missions that are supposed to be active during an ambient tutorial


g_structMatCAngledAreaMP_TitleUpdate g_sMatCAngledAreasMP_TitleUpdate[MAX_MATC_ANGLED_AREAS]

// STRUCT containing details of all blips to flash - Graeme moved this from MP_globals_missions_at_coords.sch
g_structMatcAllFlashingBlipsMP		g_sMatcFlashingBlipsMP

// JA: 19/7/13: Pre Corona Host screen state
//	Graeme moved from mp_globals_missions_at_coords.sch
INT g_iPreCoronaHostScreenState = CORONA_PRE_HOST_SCREEN_INIT


// There is one instance of this - the player can only be in one corona at any one time - it'll get used when needed
// Graeme moved the proper version of this struct in to the title update global header
g_structMatCInCoronaMP	g_sAtCoordsInCoronaMP


g_structMissionsAtCoordsMP	g_sAtCoordsMP[MAX_NUM_MP_MISSIONS_AT_COORDS]



// KGM 27/8/13: Additional Focus Variables
g_structMatCFocusMissionMP_TU	g_sAtCoordsFocusMP_TU




// ===========================================================================================================
//      Missions At Coords Stages globals
//		These are the stages a mission will go through usually based on distance from player
// ===========================================================================================================

// MP Missions At Coords Stages Data
g_structMatCStagesMP	g_sAtCoordsStageInfo[MAX_MISSIONS_AT_COORDS_STAGES]




// ===========================================================================================================
//      Missions At Coords Blip Display globals
//		Used to display a number of blips always long-range on the radar
// ===========================================================================================================

// The current closest missions that are displaying long-range blips
g_structAllLongRangeBlips	g_sLongRangeBlips




// ===========================================================================================================
//      Missions At Coords Free Play Again globals
// ===========================================================================================================

// KGM 28/8/13: TRUE if the player is allowed to Play Again, or choose a Random mission for free after completing a mission if they don;t have enough cash
BOOL g_allowPoorPlayerToPlayAgainForFree = FALSE




// ===========================================================================================================
//      Missions At Coords Corona Visuals globals
// ===========================================================================================================

// A struct containing the current state of any Focus Mission corona visuals
g_structFocusMissionVisuals		g_sCoronaFocusVisuals




// ===========================================================================================================
//      Missions At Coords Initial Actions globals
//		Additional TU globals usd in conjunction with original struct
// ===========================================================================================================

// Controls additional TU initial actions
g_structMatcInitialActionsMP_TU		g_sMatcInitialActions_TU




// ===========================================================================================================
//      Missions At Coords Quick Update All Missions globals
//		Processes all missions over a short period of time for data gathering
// ===========================================================================================================

// The Quick Update All Mission control struct
g_structQuickUpdateAllMissions	g_sMatcQuickUpdate




// ===========================================================================================================
//      Missions At Coords Quick Update All Missions globals
//		Processes all missions over a short period of time for data gathering
// ===========================================================================================================

// Additional Variables required for the control struct
g_structMatCControlsMP_TU		g_sAtCoordsControlsMP_AddTU




// ===========================================================================================================
//      Scenario Blocking Struct
//		Stores the details of scenario blocking restrictions in place
//		(NOTE: The array is not stored contiguously, so just find the first empty slot to add new data)
// ===========================================================================================================

g_structMatCScenarioBlockingMP	g_sMatCScenariosMP[MAX_MATC_SCENARIO_BLOCKING]




// ===========================================================================================================
//      Paid To Lose Wanted Level globals
//		(This is so the cash and Wanted Level can be given back if the player walks out of the corona)
// ===========================================================================================================

// The previous wanted level if the player chose to pay to clear it when accepting the invite
// This is so it can be given back when refunded
INT		g_matcClearedWantedLevel		= 0
INT		g_matcCostOfClearedWantedLevel	= 0



// ===========================================================================================================
//      Missions At Coords Exclusive ContentID globals
//		(Used by Heists to reserve a contentID for exclusive triggering even if corona not yet registered)
// ===========================================================================================================

g_structMatcExclusiveCorona	g_sMatcExclusiveContentID



// ===========================================================================================================
//      Missions At Coords Performance globals
//		(Used to indicate that reduced processing should be performed)
// ===========================================================================================================

BOOL	g_matcUseReducedProcessing		= FALSE



// ===========================================================================================================
//      Missions At Coords Heist Mission Full Help Message globals
//		(Display help message to excess players when too many are in Leader's Planning Room when the mission is setup)
// ===========================================================================================================

CONST_INT		HEIST_FULL_HELP_MESSAGE_SAFETY_FRAMES		60

BOOL	g_matcHeistFullHelpNeeded		= FALSE
INT		g_matcHeistFullHelpSafetyFrame	= 0



// ===========================================================================================================
//      Missions At Coords External Blocking globals
//		(Used by external systems to functionally block all coronas and hide all coronas and blips)
// ===========================================================================================================

// Gets set immediately the blocking function gets called to hide coronas and block them
BOOL	g_matcBlockAndHideAllMissions	= FALSE
// Required to avoid cyclic headers - if this and the above flag are different then the blips need updated - this will be on the next frame
BOOL	g_matcAllMissionBlipsHidden		= FALSE

// KGM 23/6/15 [BUG 2396345]: When all mission blocked when on an FM Event, allow one mission to trigger.
CONST_INT		MATC_UNBLOCK_ONE_MISSION_ACTIVE_TIME_msec		60000

INT		g_matcUnblockOneMissionHash		= 0
INT		g_matcUnblockOneMissionTimeout	= 0

// KGM 17/6/15 [BUG 2316703]: A vector set by an external mission (ie: FM Events) to disable a corona or a GA in range
CONST_FLOAT		MATC_LOCAL_DISABLED_AREA_RANGE_m		100.0

VECTOR	g_matcLocalDisabledArea
BOOL	g_matcHasLocalDisabledArea		= FALSE
FLOAT	g_matcLocalDisabledAreaRange 	= MATC_LOCAL_DISABLED_AREA_RANGE_m



// ===========================================================================================================
//      KGM 14/7/15 [BUG: 2418849]: Missions At Coords DPADRIGHT Help Message timeout controls
//		(Ensures the dpadright help message auto-cleansup if the player is no longer in the corona)
// ===========================================================================================================

CONST_INT		DPADRIGHT_HELP_REMAIN_ACTIVE_FRAMES		2

INT		g_matcDpadRightHelpFrameTimeout	= 0

BOOL g_BG_Block_MatcLobby

