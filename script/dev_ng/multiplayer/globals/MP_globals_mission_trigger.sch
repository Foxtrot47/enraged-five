USING "rage_builtins.sch"

USING "MP_globals_mission_trigger_definitions.sch"
USING "mp_globals_missions_at_coords_definitions.sch"
USING "mp_mission_data_struct.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_mission_trigger.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains global variable instances for all MP mission triggering control routines.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// =============================================================================================================================================================
//      Mission Triggering Global Overview Consts
// =============================================================================================================================================================

// MP Mission Triggering Struct - deals with a mission offer to the player
// KEITH NOTE: This may change - there is a lot of crossover with the EOM struct so may be able to make a generic struct that both can use
//g_structMissionTriggeringMP_OLD g_sTriggerMP_Old


// -----------------------------------------------------------------------------------------------------------

// KGM 1/7/13: Hide Hud and Radar - Post Launch Controls
// During the handover from the transition to the mission there area few frames when the Hud and Radar re-appear. These controls will cover that period.
g_structPostLaunchMP    g_sTriggerPostLaunchMP




// ===========================================================================================================
//      JobList globals
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Joblist Entries
// -----------------------------------------------------------------------------------------------------------

// Contains the JobList details required to display the job on the Joblist app
//g_structMissionJobListMP_OLD    g_sJobListMP_Old[MAX_MP_JOBLIST_ENTRIES]
INT                         g_numMPJobListEntries   = 0


// -----------------------------------------------------------------------------------------------------------

// Joblist Update struct - used locally only
//g_structJoblistUpdate_OLD   g_sJoblistUpdate_Old




// -----------------------------------------------------------------------------------------------------------
//      Joblist Invites
// -----------------------------------------------------------------------------------------------------------

// Contains the details required to display invitations on the Joblist 
//g_structJobListInvitesMP_OLD    g_sJLInvitesMP_Old[MAX_MP_JOBLIST_ENTRIES]
INT                         g_numMPJobListInvitations           = 0
BOOL                        g_joblistInviteReceived             = FALSE


// -----------------------------------------------------------------------------------------------------------

// An invitation scheduling variable so that one invitation is checked every frame for validity
INT                         g_nextJoblistInvitationCheckSlot    = 0


// -----------------------------------------------------------------------------------------------------------

// The 'recent' invite timeout time - updated when an invitation is received to block MP Comms requests
TIME_DATATYPE               g_inviteRecentlyReceivedTimeout


// -----------------------------------------------------------------------------------------------------------

// A struct containing details of an Invite in the process of being accepted
//g_structInviteAcceptanceMP_OLD  g_sInviteAcceptanceMP_Old


// -----------------------------------------------------------------------------------------------------------

// A struct containing any Invite information
g_structInvitePlayer invitePlayerAptData


// -----------------------------------------------------------------------------------------------------------

// A struct containing Contact Invite acceptance information
// NOTES: This was setup for the Contact Mission routines to check for invite acceptance
g_structContactInvite g_sCMInviteAccept


// -----------------------------------------------------------------------------------------------------------
//      Joblist Cross-Session Invites
// -----------------------------------------------------------------------------------------------------------

// Contains the details required to display cross-session invitations on the Joblist 
//g_structJobListCSInvitesMP_OLD  g_sJLCSInvitesMP_OLD[MAX_MP_JOBLIST_ENTRIES]
INT                         g_numMPCSInvites                    = 0


// -----------------------------------------------------------------------------------------------------------

// An invitation scheduling variable so that one cross-session invite is checked every frame for validity
INT                         g_nextJoblistCSInviteCheckSlot      = 0


// -----------------------------------------------------------------------------------------------------------
//      Joblist Basic Invites that set a Return Value flag when accepted
// -----------------------------------------------------------------------------------------------------------

// Contains the details required to display invites that set a BOOL only when accepted
// EG: One on One Deathmatches, One on One Races, Tutorial Invites
//g_structJobListRVInvitesMP_OLD  g_sJLRVInvitesMP_Old[MAX_MP_JOBLIST_ENTRIES]
INT                         g_numMPRVInvites                    = 0


// -----------------------------------------------------------------------------------------------------------

// An scheduling variable so that one BAsic Invite is checked every frame for validity
INT                         g_nextJoblistRVInviteCheckSlot      = 0


// -----------------------------------------------------------------------------------------------------------
//      Fake Joblist Invite For Tutorial
// -----------------------------------------------------------------------------------------------------------

g_structFakeJoblistInviteMP g_fakeJoblistInvite


// -----------------------------------------------------------------------------------------------------------
//      Joblist Warp
// -----------------------------------------------------------------------------------------------------------

// Contains the details of a warp that needs to be maintained every frame after an invitation has been accepted on the joblist
// NOTE: I've taken this out of the app itself just in case the app gets killed mid-warp to ensure the warp continues.
//g_structJobListWarpMP_OLD   g_sJoblistWarpMP_Old


// -----------------------------------------------------------------------------------------------------------
//      Joblist Help Text globals
// -----------------------------------------------------------------------------------------------------------

// Used to tell the help text system that the trashcan is on-screen so it can display 'Press square to cancel activity'
BOOL g_cancelJobButtonOnScreen      = FALSE




// ===========================================================================================================
//      Mission Details Overlay globals
// ===========================================================================================================

// Contains any Mission Details Overlay Control Variables
//g_structMissionDetailsOverlayMP_OLD g_sMDOverlayMP_Old




// ===========================================================================================================
//      Cloud Mission Loader globals
//      (Allows communication with the standalone script that keeps  alocal copy of the big datafile structs)
// ===========================================================================================================

//g_structCloudMissionLoader_OLD  g_sCloudMissionLoaderMP_Old



