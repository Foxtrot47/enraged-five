
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   MP_globals_saved.sch
//      AUTHOR          :   Conor McGuire
//      DESCRIPTION     :   This script is essentially a giant struct containing other
//                          structs defined by other globals files. All variables contained
//                          within this struct will be saved.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "MP_Globals_Saved_Vehicles.sch"
//USING "MP_Globals_Saved_Buddies.sch"
USING "MP_Globals_Saved_CarApp.sch"
USING "MP_Globals_Saved_General.sch"
USING "MP_Globals_Saved_Big_Ass_Vehicles.sch"
USING "MP_Globals_Saved_Property.sch"
USING "MP_Globals_Saved_Bounty.sch"
USING "MP_Globals_Saved_Atm.sch"
USING "MP_Globals_ScriptSaves.sch"

CONST_INT SAVE_GAME_CHARACTER_SLOTS 1

CONST_INT SAVE_GAME_ALL_CHARACTER_SINGLE_SAVE 0

// All structs containing variables that should be saved should be contained within this struct
STRUCT g_structSavedMPGlobals

    // All saved data relating to MP saved vehicles
   // MP_SAVED_VEHICLE_STRUCT				MpSavedVehicles[MAX_MP_SAVED_VEHICLES_SG]
	
	// All saved data relating to MP saved vehicles that do not fit in the garage
    MP_SAVED_BIG_ASS_VEHICLES_STRUCT	MpSavedBigAssVehicles
	
	// All saved data relating to MP property
    MP_SAVED_PROPERTY_STRUCT			MpSavedProperty
  
  	// All saved data relating to MP saved buddies
	//MP_SAVED_BUDDIES_STRUCT				MpSavedBuddies
	
	// All saved data relating to MP CarApp data
	MP_SAVED_CARAPP_STRUCT				MpSavedCarApp

	// All saved data relating to MP General data
	MP_SAVED_GENERAL_STRUCT				MpSavedGeneral
	
	// All saved data relating to MP Bounties
	MP_SAVED_BOUNTY_STRUCT				MpSavedBounty
	
	//All saved data relating to the MP ATM log
	MP_SAVED_ATM_STRUCT					MpSavedATM
	
	//Save data setup to take a load off the profile stats. 
	MP_SAVE_DATA_STRUCT					mpSaveData
	
ENDSTRUCT

STRUCT g_structSavedMPGlobalsNew
	g_structSavedMPGlobals g_savedMPGlobals[SAVE_GAME_CHARACTER_SLOTS]
ENDSTRUCT

// *** Place no more variables after this ENDSTRUCT - they will not get saved

// This is the struct containing all MP saved variables.
// startup.sc sets this up as the PersistentGlobals struct.
//g_structSavedMPGlobalsNew g_savedMPGlobalsNew

//g_structSavedMPGlobals g_savedMPGlobals
