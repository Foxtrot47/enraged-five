USING "rage_builtins.sch"

USING "commands_streaming.sch"              // For Skyswoop Type
USING "mp_mission_data_struct.sch"
USING "mp_globals_common_consts.sch"
USING "mp_globals_hud_definitions.sch"
USING "mp_globals_missions_at_coords_definitions.sch"
USING "charsheet_global_definitions.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_mission_trigger.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains constant, enum, and struct definitions for all MP mission triggering control routines.
//							NB. Should not contain instantiated variables.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// =============================================================================================================================================================
//      Mission Triggering Global Overview Consts
// =============================================================================================================================================================

// Used to set the bitflag int back to 0
CONST_INT   CLEAR_ALL_MP_MT_OVERVIEW_BITFLAGS       0

// Used to indicate there is no Joblist Array entry
CONST_INT   NO_JOBLIST_ARRAY_ENTRY                  -1



// ===========================================================================================================
//      Mission Triggering globals
//      (Deals with the mission currently being requested)
// ===========================================================================================================

// The length of time that a mission offer will appear on the cellphone
CONST_INT   MISSION_OFFER_TIMEOUT_msec          60000

// The maximum length of time the Hud and Radar will be hidden post launch
CONST_INT   HIDE_HUD_SAFETY_TIMEOUT_msec        15000


// -----------------------------------------------------------------------------------------------------------

// MP Mission Triggering states - describes the current state of the mission being triggered
ENUM g_eMPTriggerStates
    NO_CURRENT_MP_TRIGGER_REQUEST,                  // There is no current mission triggering request
    MP_TRIGGER_START_PREMISSION_COMMS,              // Try to start the pre-launch Comms to the player
    MP_TRIGGER_WAITING_FOR_PREMISSION_COMMS_TO_END, // Waiting for the pre-Mission Comms to end
    MP_TRIGGER_LAUNCH_CUTSCENE,                     // Launch the cutscene mission
    MP_TRIGGER_LAUNCHING_CUTSCENE,                  // Waiting for the cutscene mission to launch
    MP_TRIGGER_CUTSCENE_ACTIVE,                     // Player is being shown the cutscene
    MP_TRIGGER_CHECK_STILL_AVAILABLE,               // Check with server that mission is still available
    MP_TRIGGER_AWAITING_CONFIRMATION,               // Waiting from confirmation from server that mission is still available
    MP_TRIGGER_LAUNCH_MISSION,                      // Launch the mission
    MP_TRIGGER_LAUNCHING_MISSION,                   // Waiting for the mission to launch
    MP_TRIGGER_MISSION_ACTIVE                       // Player is on mission
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Mission Triggering bitflags - any temporary flags that change the way the launch of this mission is dealt with
CONST_INT   CLEAR_ALL_MP_MP_TRIGGER_BITFLAGS            0   // Used to set the bitflag int back to 0
CONST_INT   MP_TRIGGER_BITFLAG_FORCE_ONTO_MISSION       1   // TRUE if the player is being forced onto the mission or activity (no choice), otherwise FALSE
CONST_INT   MP_TRIGGER_BITFLAG_MISSION_FINISHED         2   // TRUE if Mission Control has broadcast that the mission is finished, otherwise FALSE
CONST_INT   MP_TRIGGER_BITFLAG_MISSION_FAILED           3   // TRUE if Mission Control has broadcast that the mission failed to launch, otherwise FALSE
CONST_INT   MP_TRIGGER_BITFLAG_MISSION_FULL             4   // TRUE if Mission Control has broadcast that the Mission is Full for this player's team, otherwise FALSE
CONST_INT   MP_TRIGGER_BITFLAG_RECEIVED_OK_TO_JOIN      5   // TRUE if Mission Control has broadcast a confirmation that the mission is still ok to join for this player
CONST_INT   MP_TRIGGER_BITFLAG_DISPLAY_MISSION_DETAILS  6   // TRUE if Mission Details Box should be displayed, otherwise FALSE
CONST_INT   MP_TRIGGER_BITFLAG_PREMISSION_CUTSCENE      7   // TRUE if the mission should show a pre-mission cutscene
CONST_INT   MP_TRIGGER_BITFLAG_FORCE_QUIT_PREMISSION    8   // TRUE if the mission should be forced to quit during pre-mission phase (used to re-sync after host migration if problem detected)
CONST_INT   MP_TRIGGER_BITFIELD_USE_TXTMSG              9   // TRUE if the intro communication is a text message, FALSE if not


// -----------------------------------------------------------------------------------------------------------
//
//STRUCT g_structMissionTriggeringMP_OLD
//	g_eMPTriggerStates	mtState				// The current state of the mission triggering request
//	INT					mtBitflags			// Any Mission Triggering bitflags
//	TIME_DATATYPE		mtOfferTimeout		// The timeout time for when a mission offer on the cellphone will elapse
//	BOOL				mtOfferTimeoutInitialised
//	MP_MISSION_DATA_OLD	mtMissionData		// The Mission details
//	MISSION_OVER_STATE	mtMissionOverState	// The State of the mission for this player when completed: pass/fail/etc
//	
//	// There is crossover here with the EOM and the Stored Message structs so should be able to use a global struct for this data
//	TEXT_LABEL_23		mtCharacter			// The Character ID doing the talking
//	TEXT_LABEL_7		mtSpeaker			// The Speaker ID (0-8, A-Z)
//	INT					mtCharSheetAsInt	// The CharSheet ID as INT
//	TEXT_LABEL_7		mtGroup				// The Subtitle Group ID for the Conversation
//	TEXT_LABEL_15		mtRoot				// The Root ID for the Conversation
//ENDSTRUCT


// MP Mission Triggering Struct - deals with a mission offer to the player
// KEITH NOTE: This may change - there is a lot of crossover with the EOM struct so may be able to make a generic struct that both can use
STRUCT g_structMissionTriggeringMP
    g_eMPTriggerStates  mtState             // The current state of the mission triggering request
    INT                 mtBitflags          // Any Mission Triggering bitflags
    TIME_DATATYPE       mtOfferTimeout      // The timeout time for when a mission offer on the cellphone will elapse
    BOOL                mtOfferTimeoutInitialised
    MP_MISSION_DATA     mtMissionData       // The Mission details
    MISSION_OVER_STATE  mtMissionOverState  // The State of the mission for this player when completed: pass/fail/etc
    
    // There is crossover here with the EOM and the Stored Message structs so should be able to use a global struct for this data
    TEXT_LABEL_23       mtCharacter         // The Character ID doing the talking
    TEXT_LABEL_7        mtSpeaker           // The Speaker ID (0-8, A-Z)
    INT                 mtCharSheetAsInt    // The CharSheet ID as INT
    TEXT_LABEL_7        mtGroup             // The Subtitle Group ID for the Conversation
    TEXT_LABEL_15       mtRoot              // The Root ID for the Conversation
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// KGM 1/7/13: Hide Hud and Radar - Post Launch Controls
// During the handover from the transition to the mission there area few frames when the Hud and Radar re-appear. These controls will cover that period.
STRUCT g_structPostLaunchMP
    BOOL                plHideHudActive     = FALSE         // TRUE if the Hud and Radar should continue to be hidden until instructed to let them display
    TIME_DATATYPE       plHideHudTimeout                    // Safety Timeout to ensure the Hud and Radar don;t remain hidden beyond a period of time
ENDSTRUCT





// ===========================================================================================================
//      JobList globals
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Joblist Entries
// -----------------------------------------------------------------------------------------------------------

// Maximum number of entries (including invitations) that can appear on the joblist
// NOTE: missions in progress get priority followed by invitations
CONST_INT   MAX_MP_JOBLIST_ENTRIES      12


// -----------------------------------------------------------------------------------------------------------

// Max string lengths so they fit into text labels
CONST_INT   MAX_JOBLIST_MISSION_NAME_STRING_LENGTH      	63		// KGM 9/10/13: Increased from 31 since Mission Names are now a TL63
CONST_INT   MAX_JOBLIST_MISSION_DESC_STRING_LENGTH      	63


// -----------------------------------------------------------------------------------------------------------

// Indicates a feed entry is not required (can't be -1 because his is a valid feedID to mean 'no feedID)
CONST_INT	INVITE_FEED_ENTRY_NOT_REQUIRED					-99


// -----------------------------------------------------------------------------------------------------------

// The max length of a mission description we want to display
CONST_INT	INVITE_MAX_DESCRIPTION_LENGTH					500

// The max length of a mission description segment
CONST_INT	INVITE_DESCRIPTION_SEGMENT_LENGTH				63

// A timeout used after an invite is accepted to switch off certain controls to prevent control clashes
CONST_INT	INVITE_ACCEPT_CONTROLS_BLOCK_TIMEOUT_msec		1500

// Safety Timeout to ensure the game doesn't wait forever for establishing shot conditions to be met
CONST_INT	INVITE_ESTABLISHING_SHOT_SAFETY_TIMEOUT_msec	60000



// -----------------------------------------------------------------------------------------------------------

// MP JobList states - describes the current state of the joblist entry
ENUM g_eMPJobListStates
    NO_MP_JOBLIST_ENTRY,                // There is joblist entry in this array position
    MP_JOBLIST_CURRENT_MISSION,         // This array position contains the player's current mission
    MP_JOBLIST_JOINABLE_MISSION,        // This array position contains missions available for the player to join
    MP_JOBLIST_JOINABLE_PLAYLIST,       // This array position contains a playlist available for the player to join
    MP_JOBLIST_INVITATION,              // This array position contains an Invitation to warp to a mission start corona
    MP_JOBLIST_CS_INVITE,               // This array position contains a Cross-Session Invite
    MP_JOBLIST_RV_INVITE                // This array position contains a Basic Invite that set a Return Value flag to be polled
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Joblist bitfields - not sure if I need this, added so that I can mark a mission containing a player's bounty target
CONST_INT   JOBLIST_BITFLAG_MISSION_NOT_JOINABLE        0
CONST_INT   JOBLIST_BITFLAG_CONTAINS_BOUNTY_TARGET      1
CONST_INT   JOBLIST_BITFLAG_ON_PLAYLIST_LOBBY           2
CONST_INT   JOBLIST_BITFLAG_INVITE_CLOSED               3
CONST_INT   JOBLIST_BITFLAG_INVITOR_IS_PLAYER           4
CONST_INT	JOBLIST_BITFLAG_ALLOW_DELETE				5

// -----------------------------------------------------------------------------------------------------------

// The default Feed ID value
CONST_INT	NO_JOBLIST_INVITE_FEED_ID                   -1


// -----------------------------------------------------------------------------------------------------------

// A value to represent an asynchronous retrieval of the player's DisplayName is not required
CONST_INT	NO_DISPLAY_NAME_RETRIEVAL                   -1

// A safety timeout value to prevent holding up the invite too long if the Display Name is taking a long time to be retrieved
CONST_INT	DISPLAY_NAME_RETRIEVAL_SAFETY_TIMEOUT_msec	15000


// -----------------------------------------------------------------------------------------------------------

// Safety Timeout to stop hiding HUD and RADAR
CONST_INT       HIDE_HUD_SAFETY_FRAME_TIMEOUT			30


// -----------------------------------------------------------------------------------------------------------

// The number of frames to collect Contact Mission Invites re-added after a transition for display with one Reminder Feed Message
CONST_INT       RE_ADDED_INVITE_BATCH_FRAME_TIMEOUT		100


// -----------------------------------------------------------------------------------------------------------

// Delay before an Invite to a job from a friend should get repeated
CONST_INT       REPEAT_INVITE_DELAY_msec				50000


// -----------------------------------------------------------------------------------------------------------

// KGM 7/11/14: Delay after a skyswoop/teleport/screen fade out before an Invite can be displayed.
CONST_INT       POST_PLAYER_WARP_INVITE_DISPLAY_DELAY_msec	1000


// -----------------------------------------------------------------------------------------------------------

//STRUCT g_structMissionJobListMP_OLD
//	g_eMPJobListStates	jlState				// The type of mission details in this array position
//	INT					jlUniqueID			// Unique Mission Request ID
//	INT					jlMissionAsInt		// MP_MISSION as an Int
//	INT					jlInstance			// Instance ID for the mission - used when joining using the old Mission Triggering system
//	INT					jlBitfield			// Bitfield for anything special I need to mark on the Joblist (possibly TEMP)
//	VECTOR				jlCoords			// The coords to warp the player to if required (probably only needed by Invitations)
//	TEXT_LABEL_63		jlInvitorName		// The Invitor Name (literal string - for cross-session Invites)
//	TEXT_LABEL_31		jlMissionName		// The Mission Name (literal string)
//	TEXT_LABEL_63		jlDescription		// The Mission Description (literal string)
//	INT					jlCachedDescId		// MAY BE TEMP: The Cached Description ID - used to retrieve the 255-char description if there is one cached, or ILLEGAL_ARRAY_POSITION if not
//ENDSTRUCT



// Contains the JobList details required to display the job on the Joblist app
STRUCT g_structMissionJobListMP
    g_eMPJobListStates  jlState             // The type of mission details in this array position
    INT                 jlUniqueID          // Unique Mission Request ID
    INT                 jlMissionAsInt      // MP_MISSION as an Int
	INT					jlSubtype			// Mission Subtype
    INT                 jlInstance          // Instance ID for the mission - used when joining using the old Mission Triggering system
    INT                 jlBitfield          // Bitfield for anything special I need to mark on the Joblist (possibly TEMP)
    VECTOR              jlCoords            // The coords to warp the player to if required (probably only needed by Invitations)
    TEXT_LABEL_63       jlInvitorName       // The Invitor Name (literal string - for cross-session Invites)
    TEXT_LABEL_63       jlMissionName       // The Mission Name (literal string) [9/10/13: Increased to TL63 from TL31]
    TEXT_LABEL_63       jlDescription       // The Mission Description (literal string)
    INT                 jlDescHash      	// The Hash for the Cached Description - used to retrieve the 500-char description if there is one cached, or 0 if not
	TEXT_LABEL_23		jlContentID			// The contentID
	INT					jlRootContentIdHash	// The Root contentID Hashed
	INT					jlAdversaryModeType	// The type of AdversaryMode
	INT					jlMinPlayers		// The minimum players for the activity (where required)
	INT					jlMaxPlayers		// The maximum players for the activity (where required)
	INT					jlWagerH2H			// The wager for a Head-To-Head Playlist Challenge (-1 if not a H2H)
	INT					jlHeistPackedInt	// The PackedInt value as required to setup apartment for same-session invites into Heist Coronas
	INT					jlOrderIndex		// MMM 1645958 Added to give order to invites
	BOOL				jlIsPushBikeOnly	// MMM Added to differentiate between push bike and motorcycle missions
	BOOL				jlQuickMatch
	INT 				jlPropertyType	// Type of property
	BOOL 				jlForceToTop
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

//STRUCT g_structJoblistUpdate_OLD
//	BOOL			isUpdated			// TRUE if the list has been updated, otherwise FALSE
//	BOOL			joblistActiveUpdate	// ignored if joblist not active - TRUE if joblist active and updates allowed, otherwise FALSE
//	BOOL			missionsOnScreen	// TRUE if the list of missions is onscreen (Brenda's ToDo list), otherwise FALSE
//	TIME_DATATYPE	timeout				// The next joblist timeout time
//ENDSTRUCT


// Joblist Update struct - used locally only
STRUCT g_structJoblistUpdate
    BOOL            isUpdated               = FALSE                     // TRUE if the list has been updated, otherwise FALSE
    BOOL            joblistActiveUpdate     = FALSE                     // ignored if joblist not active - TRUE if joblist active and updates allowed, otherwise FALSE
    BOOL            missionsOnScreen        = FALSE                     // TRUE if the list of missions is onscreen (Brenda's ToDo list), otherwise FALSE
    INT             timeout                                             // The next joblist timeout time
                                                                        // Changed by Steve T at Keith's request to fix NT 1599405 
    BOOL            wantedFeedDisplayed     = FALSE                     // TRUE if the alternative Invite Feed message has been displayed if the player has a wanted level, FALSE if not
    INT             wantedFeedID            = NO_JOBLIST_INVITE_FEED_ID // The feed ID of the 'wanted level' feed message
	
	INT				reAddedFeedFrameTimeout	= 0							// The timeout frame for batching up Feed message for re-adding invites, or 0 if none
	
	INT				postPlayerWarpDelay		= 0							// The timeout frame when new feed messges can be displayed after a skyswoop/teleport/screen fade out
ENDSTRUCT
    

// -----------------------------------------------------------------------------------------------------------
//      Joblist Invites
// -----------------------------------------------------------------------------------------------------------

// The maximum time an Invitation will be delayed while waiting for a headshot to be generated
CONST_INT       MAX_INVITE_DELAY_FOR_HEADSHOT_msec          5000

// The Allow Again timeout - a duplicate invite from the same player to the same mission will be ignored within this time to avoid autofill spamming
CONST_INT       ALLOW_DUPLICATE_INVITE_DELAY_msec           30000


// -----------------------------------------------------------------------------------------------------------

// OBSOLETE: But struct needs to remain for pre-titleupdate global signature
//STRUCT g_structJobListInvitesMP_OLD
//	BOOL				jliActive				// TRUE if the feed message for this Invite has been sent and therefore the Invite can appear on the Joblist, otherwise FALSE (probably because the headshot is still being generated)
//	BOOL				jliAllowDisplay			// TRUE if the invite can still be displayed, FALSE if it cannot be displayed (this was added to hide invites when only 10 seconds remainined on the corona countdown)
//	BOOL				jliIsDuplicate			// TRUE if this entry is an invite to a mission with duplicate details but from a different player
//	BOOL				jliInviteRead			// TRUE if the Invite has been read (ie: has been listed on the Joblist App), FALSE if not read (ie: hasn't been listed on the Joblist App)
//	VECTOR				jliCoords				// Coords to warp the player to
//	PLAYER_INDEX		jliPlayer				// Player that sent the invitation
//	enumCharacterList	jliNPC					// In-Game Character that sent invitation
//	BOOL				jliInvitorIsPlayer		// TRUE if the invitor is a player, FALSE if the invitor is an in-game character
//	INT					jliCreatorID			// The CreatorID of the mission
//	INT					jliType					// The Type of Mission
//	INT					jliCachedDescID			// MAY BE TEMP: This is the array position within the Cached Content Descriptions so that we can access the 255-character descriptions
//	TEXT_LABEL_31		jliMissionName			// The Mission Name (literal string)
//	TEXT_LABEL_63		jliDescription			// The Mission Description (literal string)
//	TIME_DATATYPE		jliHeadshotTimeout		// Only allow a short period of time for the headshot to be generated before activating the Invite anyway - don't want to hold up the game too long for a headshot
//	INT					jliFeedID				// The Feed ID - so that it can be deleted if the invite is no longer valid
//	TIME_DATATYPE		jliAllowAgainTimeout	// An invite from the same player for the same mission will not be allowed again within a certain time - this is to avoid autofill spamming which seems to send the invite again evey few seconds
//ENDSTRUCT


// Contains the details required to display invitations on the Joblist 
STRUCT g_structJobListInvitesMP
    BOOL                					jliActive               // TRUE if the feed message for this Invite has been sent and therefore the Invite can appear on the Joblist, otherwise FALSE (probably because the headshot is still being generated)
    BOOL 									jliForceToTop
	BOOL                					jliAllowDisplay         // TRUE if the invite can still be displayed, FALSE if it cannot be displayed (this was added to hide invites when only 10 seconds remainined on the corona countdown)
    BOOL                					jliIsDuplicate          // TRUE if this entry is an invite to a mission with duplicate details but from a different player
    BOOL                					jliInviteRead           // TRUE if the Invite has been read (ie: has been listed on the Joblist App), FALSE if not read (ie: hasn't been listed on the Joblist App)
	BOOL									jliReAdding				// TRUE if the Invite is being re-added after transition, FALSE if it is new (really only applies to Contact Missions)
    VECTOR              					jliCoords               // Coords to warp the player to
    PLAYER_INDEX        					jliPlayer               // Player that sent the invitation
    enumCharacterList   					jliNPC                  // In-Game Character that sent invitation
    BOOL                					jliInvitorIsPlayer      // TRUE if the invitor is a player, FALSE if the invitor is an in-game character
    INT                 					jliCreatorID            // The CreatorID of the mission
    INT                 					jliType                 // The Type of Mission
	INT										jliSubtype				// The Mission Subtype
    INT                 					jliDescHash      		// The Hash for the Cached Description - used to retrieve the 500-char description if there is one cached, or 0 if not
	CACHED_MISSION_DESCRIPTION_LOAD_VARS	jliDescLoadVars			// The variables used to oversee the downloading of the 500-char Description
	BOOL									jliRequireDesc			// TRUE if the 500-char description is required to be downloaded
	BOOL									jliUseHeistCutsceneDesc	// TRUE if the Heist Umbrella Description should be used
    TEXT_LABEL_63       					jliMissionName          // The Mission Name (literal string) [9/10/13: Increased to TL63 from TL31]
    TEXT_LABEL_63       					jliDescription          // The Mission Description (literal string)
    TIME_DATATYPE       					jliHeadshotTimeout      // Only allow a short period of time for the headshot to be generated before activating the Invite anyway - don't want to hold up the game too long for a headshot
    INT                 					jliFeedID               // The Feed ID - so that it can be deleted if the invite is no longer valid
    TIME_DATATYPE       					jliAllowAgainTimeout    // An invite from the same player for the same mission will not be allowed again within a certain time - this is to avoid autofill spamming which seems to send the invite again evey few seconds
	TEXT_LABEL_23							jliContentID
	INT										jliRootContentIdHash
	INT										jliAdversaryModeType
	INT										jliOnHours				// The hours when this invite will be available (a bitset where the first 24 bits each represent an hour)
    BOOL                					jliToDownload			// TRUE if the mission header details need to be downloaded
    BOOL                					jliDownloading			// TRUE if the mission header details are being downloaded
    BOOL                					jliDownloaded			// TRUE if the mission header details have been downloaded
	INT										jliMinPlayers			// The minimum number of players required for this activity
	INT										jliMaxPlayers			// The maximum number of players required for this activity
	INT										jliWagerH2H				// The Head To Head Wager amoung
	BOOL									jliIsUnplayed			// TRUE if this mission has NOT been played (so that these can be highlighted), FALSE if played, or data not locally available, etc
	INT										jliHeistPackedInt		// Used to setup the correct apartment when accepting an invite into a Heist Corona (same-session only, CS gets this infor from transition variables)
	INT										jliOrderIndex			// MMM 1645958 Stores the position of the invite in relation to the others. Added by MMM 16/06/2014 - BUG  1645958
	BOOL									jliIsPushBikeOnly		// MMM Used for differentiating between motorbike and bicycle races
	BOOL									jliQuickMatch			// Do a quickmatch
	BOOL									jliRepeatInvite			// TRUE if this invite should be repeated later
	INT										jliRepeatTimeout		// The timeout time when this Invite should be repeated
	INT										jliPermissionCheckID	// Asynchronous check, result received through EVENT_NETWORK_PERMISSION_CHECK_RESULT: ensures a player that has blocked another player won;t still receive invites from the blocked player
	BOOL									jliDoingPermissionCheck	// TRUE if the asynchronous permissions check is active, FALSE if it has completed
	BOOL									jliPermissionCheckFail	// TRUE if the asynchronous permissions check has failed, FALSE if it hasn't failed
	BOOL									jliDoGamerToGamerCheck	// XBOXONE Only, an asynchronous refined content restrictions check is available with a result generated by code automatically when a new player enters a session, but this result can take a few frames to be ready so this flag ensure the delayed check gets done - BUG 2052076
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// The period of time where a new invite will still be classed as 'recent' (this is to block MP Comms)
CONST_INT                   RECENT_INVITE_TIME_msec             30000
CONST_INT                   INVITE_ACCEPT_SAFETY_TIMEOUT_msec   30000


// -----------------------------------------------------------------------------------------------------------

//STRUCT g_structInviteAcceptanceMP_OLD
//	BOOL			iaInviteAccepted		= FALSE					// TRUE if the invite acceptance process has started, otherwise FALSE
//	INT				iaMissionUniqueID		= NO_UNIQUE_ID			// The uniqueID passed when the player is attempting to be reserved for the mission - cleared when a reposnse has been received
//	BOOL			iaReservationFailed		= FALSE					// TRUE if the reservation request for the mission has been denied
//	VECTOR			iaInviteCoords									// The coords the player will be warped to on reservation confirmation
//	TIME_DATATYPE	iaSafetyTimeout									// A safety timeout waiting for a response after accepting an invitation
//ENDSTRUCT

// A struct containing details of an Invite in the process of being accepted
STRUCT g_structInviteAcceptanceMP
    BOOL            iaInviteAccepted        = FALSE                         // TRUE if the invite acceptance process has started, otherwise FALSE
    INT             iaMissionUniqueID       = NO_UNIQUE_ID                  // The uniqueID passed when the player is attempting to be reserved for the mission - cleared when a reposnse has been received
    TEXT_LABEL_63   iaMissionName                                           // The MissionName of the activity the player has accepted an invite to
    INT             iaFMType                = -1                            // The FM Mission Type of the activity the player has accepted an invite to
    INT             iaSubtype               = -1                            // The Subtype of the activity the player has accepted an invite to
    BOOL            iaReservationFailed     = FALSE                         // TRUE if the reservation request for the mission has been denied
    VECTOR          iaInviteCoords                                          // The coords the player will be warped to on reservation confirmation
    INT             iaFeedID                = NO_JOBLIST_INVITE_FEED_ID     // The FeedID of the invite being accepted, so it can be deleted along with the rest of the HUD
    //TIME_DATATYPE iaSafetyTimeout         //Removed by Steve T at Keith's request to fix NT 1599405 
                                            // A safety timeout waiting for a response after accepting an invitation
    INT             iaSafetyTimeout         // Added by Steve T at Keith's request to fix NT 1599405 
    TEXT_LABEL_23	iaContentID		
	INT 			iaInviteeAppartment
	PLAYER_INDEX	iaInvitingPlayer										// Used to decide if the apartment needs terminated during invite acceptance - if already in the inviting player's apartmetn then it shouldn't terminate
	INT 			irootContentId
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// A struct containing any Invite information
STRUCT g_structInvitePlayer
    INT             ipInviteApartment       = -1                    // The propertyID that the most recently accepted invite originated from
    GAMER_HANDLE    ipInvitingPlayer                                // The gamer handle of the player that sent the invite that has just been accepted
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// A struct containing Contact Invite acceptance information
// NOTES: This was setup for the Contact Mission routines to check for invite acceptance
STRUCT g_structContactInvite
    enumCharacterList       ciInvitingContact   = NO_CHARACTER      // The last contact character to send the player an invite
    TEXT_LABEL_31           ciCloudFilename                         // The cloud filename for the contact mission the player has been invited to
    BOOL                    ciReservedForJob    = FALSE             // TRUE if the player got reserved for the Contact Mission, otherwise FALSE
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------
//      Joblist Cross-Session Invites
// -----------------------------------------------------------------------------------------------------------

// Contains the details required to display cross-session invitations on the Joblist 
//STRUCT g_structJobListCSInvitesMP_OLD
//    BOOL                jcsiActive          // TRUE if the feed message for this Invite has been sent and therefore the Invite can appear on the Joblist, otherwise FALSE (probably because the headshot is still being generated)
//    BOOL                jcsiAllowDisplay    // TRUE if the invite can still be displayed, FALSE if it cannot be displayed
//    BOOL                jcsiToDownload      // TRUE if the mission header details need to be downloaded
//    BOOL                jcsiDownloading     // TRUE if the mission header details are being downloaded
//    BOOL                jcsiDownloaded      // TRUE if the mission header details have been downloaded
//    TEXT_LABEL_23       jcsiContentID       // The contentID if required for downloading mission header details
//    TEXT_LABEL_63       jcsiInvitorName     // Player Name that sent the invitation
//    INT                 jcsiCreatorID       // The CreatorID of the job
//    INT                 jcsiType            // The Type of job
//    INT                 jcsiCachedDescID    // MAY BE TEMP: This is the array position within the Cached Content Descriptions so that we can access the 255-character descriptions
//    TEXT_LABEL_31       jcsiMissionName     // The Mission Name (literal string)
//    TEXT_LABEL_63       jcsiDescription     // The Mission Description (literal string)
//    INT                 jcsiPlaylistCurrent // The current mission number in the playlist
//    INT                 jcsiPlaylistTotal   // The total missions in the playlist
//    INT                 jcsiInviteID        // The InviteID
//    INT                 jcsiFeedID          // The Feed ID - so that it can be deleted if the invite is no longer valid
//ENDSTRUCT


// Contains the details required to display cross-session invitations on the Joblist - TITLE UPDATE VERSION
// The playlist current variable will be used to store the Head-To-Head Playlist Wager if greater than a specific CONST value (the wager will be 'value minus CONST') - Quick Fix
// KGM 20/8/14: The InviterGamerTag is received from the Cross-session invite - we need to store this so that, when the invite is received, we can test to see if another invite from the same
//				player has also been received. XboxOne optionally allows a player's 'display name' to be displayed instead of the GamerTag. This will be a separate field because we need to fill
//				it using an asynchronous check so isn't instantly available. We'll make the decision which name to display when we pass the data to the joblist.
STRUCT g_structJobListCSInvitesMP
    BOOL                					jcsiActive          	// TRUE if the feed message for this Invite has been sent and therefore the Invite can appear on the Joblist, otherwise FALSE (probably because the headshot is still being generated)
    BOOL                					jcsiAllowDisplay    	// TRUE if the invite can still be displayed, FALSE if it cannot be displayed
    BOOL                					jcsiToDownload      	// TRUE if the mission header details need to be downloaded
    BOOL                					jcsiDownloading     	// TRUE if the mission header details are being downloaded
    BOOL                					jcsiDownloaded      	// TRUE if the mission header details have been downloaded
    BOOL                					jcsiInviteRead       	// TRUE if the Invite has been read (ie: has been listed on the Joblist App), FALSE if not read (ie: hasn't been listed on the Joblist App)
	TEXT_LABEL_23       					jcsiContentID       	// The contentID if required for downloading mission header details
	INT										jlRootContentIdHash
	INT										jlAdversaryModeType
	BOOL									jlQuickMatch
    TEXT_LABEL_63       					jcsiInvitorGamerTag		// The InviterName (GamerTag) received as part of the Cross-Session Invite
    TEXT_LABEL_63       					jcsiInvitorDisplayName	// The DisplayName (XboxOne only), used if the players have agreed to use their real names rather than their GamerTags
	INT										jcsiDisplayNameID		// An ID to reference the asynchronous retrieval of the player's displayName (XboxOne) - used instead of gamerTag (0+ is valid)
	INT										jcsiDisplayNameTimeout	// A timeout to prevent holding up the invite for too long if the display name request is taking longer than usual
    INT                 					jcsiCreatorID       	// The CreatorID of the job
    INT                 					jcsiType            	// The Type of job
	INT										jcsiSubtype				// The subtype of the Job
    INT                 					jcsiDescHash      		// The Hash for the Cached Description - used to retrieve the 500-char description if there is one cached, or 0 if not
	CACHED_MISSION_DESCRIPTION_LOAD_VARS	jcsiDescLoadVars		// The variables used to oversee the downloading of the 500-char Description
	BOOL									jcsiRequireDesc			// TRUE if the 500-char description is required to be downloaded
    TEXT_LABEL_63       					jcsiMissionName     	// The Mission Name (literal string)
    TEXT_LABEL_63       					jcsiDescription     	// The Mission Description (literal string)
    INT                 					jcsiPlaylistCurrent 	// The current mission number in the playlist (ALSO H2H WAGER VALUE - see Note above)
    INT                 					jcsiPlaylistTotal   	// The total missions in the playlist
    INT                 					jcsiInviteID        	// The InviteID
    INT                 					jcsiFeedID          	// The Feed ID - so that it can be deleted if the invite is no longer valid
	BOOL									jcsiIsUnplayed			// TRUE if this mission has NOT been played (so that these can be highlighted), FALSE if played, or data not locally available, etc
	INT										jcsiOrderIndex			// MMM 1645958 Gives the invite an order - Added by MMM - 16/06/2014 - BUG 1645958
	BOOL									jcsiIsPushBikeOnly		// MMM Used for differentiating between motorbike and bicycle races
	BOOL									jcsiRepeatInvite	// TRUE if this invite should be repeated later
	INT										jcsiRepeatTimeout	// The timeout time when this Invite should be repeated
	BOOL									jcsiInvitorIsFriend		// KGM 16/7/15 [BUG 2422920]: When the invite arrives, store the result of a friend check based on the GamerHandle
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------
//      Joblist Basic Invites that set a Return Value flag when accepted
// -----------------------------------------------------------------------------------------------------------

// Contains the details required to display invites that set a BOOL only when accepted
// EG: One on One Deathmatches, One on One Races, Tutorial Invites
//STRUCT g_structJobListRVInvitesMP_OLD
//    BOOL                jrviActive          // TRUE if the feed message for this Invite has been sent and therefore the Invite can appear on the Joblist, otherwise FALSE
//    BOOL                jrviAccepted        // TRUE if the invite has been accepted - the sender should poll for this
//    BOOL                jrviAllowDisplay    // TRUE if the invite can still be displayed, FALSE if it cannot be displayed
//    PLAYER_INDEX        jrviPlayer          // Player Name that sent the invitation
//    enumCharacterList   jrviNPC             // In-Game Character that sent invitation
//    BOOL                jrviInvitorIsPlayer // TRUE if the invitor is a player, FALSE if the invitor is an in-game character
//    INT                 jrviType            // The Type of job
//    TEXT_LABEL_31       jrviMissionName     // The Mission Name (literal string)
//    INT                 jrviFeedID          // The Feed ID - so that it can be deleted if the invite is no longer valid
//    TEXT_LABEL_31       jrviOptionalTL      // An Optional Text Label to supply additional details for the Invites screen
//    INT                 jrviOptionalInt1    // An optional INT to supply additional details for the Invite screen
//    INT                 jrviOptionalInt2    // An optional INT to supply additional details for the Invite screen
//ENDSTRUCT



// Contains the details required to display invites that set a BOOL only when accepted - TITLE UPDATE VERSION
// EG: One on One Deathmatches, One on One Races, Tutorial Invites
STRUCT g_structJobListRVInvitesMP
    BOOL                	jrviActive          		// TRUE if the feed message for this Invite has been sent and therefore the Invite can appear on the Joblist, otherwise FALSE
    BOOL					jliForceToTop
	BOOL                	jrviAccepted        		// TRUE if the invite has been accepted - the sender should poll for this
    BOOL                	jrviAllowDisplay    		// TRUE if the invite can still be displayed, FALSE if it cannot be displayed
    BOOL                	jrviInviteRead      		// TRUE if the Invite has been read (ie: has been listed on the Joblist App), FALSE if not read (ie: hasn't been listed on the Joblist App)
	PLAYER_INDEX        	jrviPlayer          		// Player Name that sent the invitation
    enumCharacterList   	jrviNPC             		// In-Game Character that sent invitation
    BOOL                	jrviInvitorIsPlayer 		// TRUE if the invitor is a player, FALSE if the invitor is an in-game character
    INT                 	jrviType            		// The Type of job
    INT                 	jrviSubType         		// Used for FMMC_TYPE_HEIST_PHONE_INVITE to specify if the Heist Quick Invite is for a PLANNING or FINALE (using the Mission Subtype IDs)
    TEXT_LABEL_63       	jrviMissionName     		// The Mission Name (literal string)
    INT                 	jrviFeedID          		// The Feed ID - so that it can be deleted if the invite is no longer valid
    TEXT_LABEL_31       	jrviOptionalTL      		// An Optional Text Label to supply additional details for the Invites screen
    INT                 	jrviOptionalInt1    		// An optional INT to supply additional details for the Invite screen
    INT                 	jrviOptionalInt2    		// An optional INT to supply additional details for the Invite screen
	BOOL					jrviRequestClosed			// TRUE if the Basic Invite has been requested to be closed, otherwise FALSE
	TIME_DATATYPE			jrviClosedTimeout			// The basic invite can now be classed as 'closed' until timeout instead of getting deleted
	BOOL					jrviAllowDelete				// TRUE if the invite can be deleted, otherwise FALSE
	INT						jrviOrderIndex				// Basic Invites should also appear in time-based order
	INT						jrviPermissionCheckID		// Asynchronous check, result received through EVENT_NETWORK_PERMISSION_CHECK_RESULT: ensures a player that has blocked another player won;t still receive invites from the blocked player
	BOOL					jrviDoingPermissionCheck	// TRUE if the asynchronous permissions check is active, FALSE if it has completed
	BOOL					jrviPermissionCheckFail		// TRUE if the asynchronous permissions check has failed, FALSE if it hasn't failed
	INT						jrviPropertyType			// Type of property invited to
	INT						jrviRootId					// The Root ID?
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------
//      Fake Joblist Invite For Tutorial
// -----------------------------------------------------------------------------------------------------------

STRUCT g_structFakeJoblistInviteMP
    BOOL                fjiReceived     = FALSE     // TRUE if the fake joblist invite has been received
    BOOL                fjiAccepted     = FALSE     // TRUE if the fake joblist invite has been accepted from the joblist by the player
    enumCharacterList   fjiSender                   // The fake joblist invite sender as an in-game character
    INT                 fjiType         = -1        // The fake joblist invite FM Mission Type
    TEXT_LABEL_31       fjiMissionName              // The fake joblist invite Mission Name (literal string)
    INT                 fjiFeedID       = -1        // The fake joblist invite Feed ID
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------
//      Joblist Warp
// -----------------------------------------------------------------------------------------------------------
//
//STRUCT g_structJobListWarpMP_OLD
//	BOOL			jlwIsActive		= FALSE				// TRUE if a joblist warp is being maintained
//	BOOL			jlwSkyswoop		= FALSE				// TRUE to use skyswoop, FALSE to teleport
//	BOOL			jlwFadedOut		= FALSE				// TRUE if the screen has been faded out
//	BOOL			jlwPlayerMoved	= FALSE				// TRUE when the player has been moved to the new coordinates as part of the skyswoop
//	BOOL			jlwSwoopUpDone	= FALSE				// TRUE if skyswoop up is completed
//	SWITCH_TYPE		jlwSkyswoopType	= SWITCH_TYPE_AUTO	// The type of skyswoop to be performed: Long or Medium (or auto if a problem)
//	VECTOR			jlwCoords							// The Coords for the centrepoint for the warp
//	VECTOR			jlwOffset							// The player offset from the centrepoint for the warp
//ENDSTRUCT


// Contains the details of a warp that needs to be maintained every frame after an invitation has been accepted on the joblist
// NOTE: I've taken this out of the app itself just in case the app gets killed mid-warp to ensure the warp continues.
STRUCT g_structJobListWarpMP
    BOOL            jlwIsActive     		= FALSE             // TRUE if a joblist warp is being maintained
    BOOL            jlwSkyswoop     		= FALSE             // TRUE to use skyswoop, FALSE to teleport
    BOOL            jlwFadedOut     		= FALSE             // TRUE if the screen has been faded out
    BOOL            jlwPlayerMoved  		= FALSE             // TRUE when the player has been moved to the new coordinates as part of the skyswoop
    BOOL            jlwSwoopUpDone  		= FALSE             // TRUE if skyswoop up is completed
    SWITCH_TYPE     jlwSkyswoopType 		= SWITCH_TYPE_AUTO  // The type of skyswoop to be performed: Long or Medium (or auto if a problem)
    VECTOR          jlwCoords                           		// The Coords for the centrepoint for the warp
    VECTOR          jlwOffset                           		// The player offset from the centrepoint for the warp
	TEXT_LABEL_23	jlwContentID
	INT				jlwSafetyStopFrame		= 0					// A safety frame timeout so that the radar and feed don;t get hidden forever under some bizarre circumstance
	BOOL			jlwImmediateLaunch		= FALSE				// TRUE if this is an immediate launch mission
	BOOL			jlwForceKillProperty	= FALSE				// TRUE if the property should be force killed after player moves during skyswoop - this is used if accepting a same-session invite into a Heist while in a property
	BOOL			jlwDoEstablishingShot	= FALSE				// TRUE if the establishing shot into the apartment is required
	INT				jlwEstShotSafetyTimeout	= 0					// A safety timeout used to prevent endlessly waiting for the establishing shot conditiions to get setup
	PLAYER_INDEX	jlwEstShotInvitingPlayer					// The Inviting Player for the corona that requires an establishing shot
ENDSTRUCT




// ===========================================================================================================
//      Mission Details Overlay globals
// ===========================================================================================================

CONST_INT   MISSION_DETAILS_OVERLAY_DISPLAY_FOREVER_msec        24 * 60 * 60 * 1000     // Needs to be a large number to represent 'forever' - using one realtime day
CONST_INT   MISSION_DETAILS_OVERLAY_DISPLAY_ON_JOIN_msec        30000                   // Extra display time for when player joins mission - 30 seconds
CONST_INT   MISSION_DETAILS_OVERLAY_DISPLAY_ON_FULL_msec        5000                    // Extra display time for when the mission becomes FULL for player's team - 5 seconds


// -----------------------------------------------------------------------------------------------------------

//STRUCT g_structMissionDetailsOverlayMP_OLD
//	BOOL				mdoOnDisplay		// TRUE if the Mission Details Overlay is on display, otherwise FALSE
//	TIME_DATATYPE		mdoDisplayTimeout	// Expiry timeout for the Overlay Display
//	INT					mdoMaxFromTeam		// The Maximum number of players from the player's team allowed on mission - calculated once then stored for reference
//	MP_MISSION_DATA_OLD	mdoMissionData		// The Mission details
//	//INT 				mdoDisplayMissionNameTimeout	//Timer to display the mission name when you start the mission
//ENDSTRUCT

// Contains any Mission Details Overlay Control Variables
STRUCT g_structMissionDetailsOverlayMP
    BOOL                mdoOnDisplay        // TRUE if the Mission Details Overlay is on display, otherwise FALSE
    TIME_DATATYPE       mdoDisplayTimeout   // Expiry timeout for the Overlay Display
    INT                 mdoMaxFromTeam      // The Maximum number of players from the player's team allowed on mission - calculated once then stored for reference
    MP_MISSION_DATA     mdoMissionData      // The Mission details
    //INT               mdoDisplayMissionNameTimeout    //Timer to display the mission name when you start the mission
ENDSTRUCT





// ===========================================================================================================
//      Cloud Mission Loader globals
//      (Allows communication with the standalone script that keeps  alocal copy of the big datafile structs)
// ===========================================================================================================

// The maximum number of automatic attempts to download the data before reporting failure
CONST_INT       MAX_CLOUD_MISSION_DOWNLOAD_ATTEMPTS     3

// The number of frames that the most recent load will report success/failure before auto-clearing the variables
// This is to prevent spamming when the function that requested the load has to quit without waiting for an answer
CONST_INT       CLOUD_DOWNLOAD_MAX_REPORTING_FRAMES     10


// -----------------------------------------------------------------------------------------------------------

//STRUCT g_structCloudMissionLoader_OLD
//	BOOL						cmlLoadRequired			= FALSE					// TRUE if a data load is required, FALSE if not (set by calling script, read by standalone script)
//	BOOL						cmlHeaderRequired		= FALSE					// TRUE if a header load is required, FALSE if not (set by calling script, read by standalone script)
//	MP_MISSION_ID_DATA_OLD		cmlMissionIdData								// The missionID data
//	BOOL						cmlDataLoadFinished		= FALSE					// TRUE if the data load has finished, FALSE if ongoing (set by standalone script, read by calling script)
//	BOOL						cmlDataLoadSuccessful	= FALSE					// TRUE if the data load ended successfully, FALSE if the data failed to load
//	INT							cmlNumLoadAttempts		= 0						// The number of attempts taken to load the data - there will be some automatic retrys before reporting failure
//	INT							cmlReportingFrames		= -1					// The number of frames that the success/failure will be reported to the calling routine before gicing up and auto-clearing the variables
//	INT							cmlReturnType			= -1					// The Type returned from loading the header data
//	TEXT_LABEL_31				cmlReturnMissionName							// The Mission Name returned from loading the header data
//	TEXT_LABEL_63				cmlReturnDescription							// The Description returned from loading the header data
//ENDSTRUCT



STRUCT g_structCloudMissionLoader
    BOOL                        cmlLoadRequired         		= FALSE         // TRUE if a data load is required, FALSE if not (set by calling script, read by standalone script)
    BOOL                        cmlHeaderRequired       		= FALSE         // TRUE if a header load is required, FALSE if not (set by calling script, read by standalone script)
    BOOL                        cmlHeistPlanningHeadersRequired	= FALSE         // TRUE if a Heist Planning Header load is required, FALSE if not (set by calling script, read by standalone script)
	BOOL						cmlCancelLoad					= FALSE			// TRUE if an ongoing data load should be cancelled (set by calling script, read by standalone script)
    MP_MISSION_ID_DATA          cmlMissionIdData                                // The missionID data
	BOOL						cmlUseOfflineID					= FALSE			// TRUE if the specific Offlien COntentID should be used instead of the contentID, otherwise FALSE
	TEXT_LABEL_31				cmlOfflineID									// The Offline ID to be used for the download, or EMPTY
    BOOL                        cmlDataLoadFinished     		= FALSE         // TRUE if the data load has finished, FALSE if ongoing (set by standalone script, read by calling script)
    BOOL                        cmlDataLoadSuccessful   		= FALSE         // TRUE if the data load ended successfully, FALSE if the data failed to load
    BOOL                        cmlClearDataPreload     		= FALSE         // TRUE if the previous load data should be cleared, FALSE if not (generally mission triggerer shouldn't so that it doesn't duplicate a corona download)
    INT                         cmlNumLoadAttempts      		= 0             // The number of attempts taken to load the data - there will be some automatic retrys before reporting failure
    INT                         cmlReportingFrames      		= -1            // The number of frames that the success/failure will be reported to the calling routine before giving up and auto-clearing the variables
    INT                         cmlReturnType           		= -1            // The Type returned from loading the header data
	INT							cmlReturnSubtype				= -1			// The subtype returned from loading the header data
    TEXT_LABEL_63               cmlReturnMissionName                            // The Mission Name returned from loading the header data
    TEXT_LABEL_63               cmlReturnDescription                            // The Description returned from loading the header data
ENDSTRUCT

// STUCT used for preventing and tracking spam invites
STRUCT m_sSpamJobList
	PLAYER_INDEX 				jlInvitorIndex		= NULL
	INT							jliContentIDHash	= 0
    INT                         Timer  = 0
	INT							jlcsInviteSessionID = -1
ENDSTRUCT
