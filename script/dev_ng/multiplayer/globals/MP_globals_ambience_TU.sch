USING "types.sch"
USING "MP_globals_ambience_definitions.sch"


MATCHED_PLAYERS			matchedPlayers


MPGlobalsAmbienceStruct MPGlobalsAmbience

VOICE_SESSION_SCRIPT_STRUCT 	voiceSession

FAILSAFE_CLEAR_PURCHASED_FLAGS clearPurchaseFlags

BOOL g_bHasTransitionSpawnedPlayer

TIME_DATATYPE g_tdTyreSmokeLoopTimer

//Fairground Stuff
FLOAT g_fWheelRotation
FLOAT g_fCoasterDist
FLOAT g_fCoasterSpeed

MPGlobalsFWStruct MPGlobalsFW

SCRIPT_TIMER g_stPlayerLocallyVisibleDuringFade
SCRIPT_TIMER g_stForceRadarOffTimer
//VEHICLE_INDEX g_VehForcedAlpha
INT g_iPlayersForcedToBeLocallyVisibleDuringFadeBS // We used to be able to force only local player visible, this extends this mechanism across all players
SCRIPT_TIMER g_stPlayersForcedToBeLocallyVisibleDuringFadeTimers[NUM_NETWORK_PLAYERS]

MISSION_CONTROL_NET_DOOR_OVERRIDES g_MissionNetDoorOverride

MP_PROPERTY_MENU tempPropertyExtMenu

MPGlobalsPrivateYachtStruct MPGlobalsPrivateYacht

MAGNATE_GANG_BOSS_OUTFIT_STRUCT g_sMagnateOutfitStruct

BOOL g_bFinishedExitingOfSMPLinteriorOnHeist
BOOL g_bAppliedMissionOutfit
INT g_iDefunctExitPosition
#IF FEATURE_CASINO_HEIST
INT g_iArcadeExitPosition
#ENDIF
BOOl g_bKickOutHangarEntryNoVeh = TRUE
SCRIPT_TIMER disableRadaronPropertyExitDelay
BOOL g_bBypassAliveCheckForHeistExitFromFacility

#IF IS_DEBUG_BUILD
BOOL g_bBypassPersonalVehicleTextDelay
STRUCT DEBUG_EXTERIOR_VEHICLE_SPAWN
	BOOL bCreateBikes
	BOOL bCreateQuads
	INT iInitCoordsBS
	INT iProperty =1
	INT iVehToEdit
	VEHICLE_INDEX VehID[8]
	VECTOR iVehPos
	VECTOR iVehRot
	BOOL bPrintDebugInfo
	BOOL bCreatedVehicles
	INT iVehicleLastFrame
	BOOL bDeleteVehicle[2]
ENDSTRUCT

BOOL g_bTestHeistExitFromBase = FALSE
BOOL g_bTestHesitQuickRestartExitFromBase = FALSE
BOOL g_bTestHeistFinaleExitFromBase = FALSE

#ENDIF

CONST_INT iCONST_DJ_TRLR_PED_COUNT 15
CONST_INT iCONST_DJ_TRLR_VEH_COUNT 15	
CONST_INT iCONST_DJ_TRLR_OBJ_COUNT 15

STRUCT DJ_TRLR_DATA
	INT 			iPlay_DJ_TRLR = -1
	BOOL 			bPlay_DJ_TRLR_P8 = FALSE
	BOOL 			bDrawDebug
	PED_INDEX		piPed			[iCONST_DJ_TRLR_PED_COUNT]
	VEHICLE_INDEX	piVeh			[iCONST_DJ_TRLR_VEH_COUNT]
	OBJECT_INDEX	piObj			[iCONST_DJ_TRLR_OBJ_COUNT]
ENDSTRUCT
	
DJ_TRLR_DATA g_sPlay_DJ_TRLR

#IF IS_DEBUG_BUILD
// The following globals are used to control Arena Contestant Turret specific settings  
FLOAT g_debug_turretMgWeight = 1.5
FLOAT g_debug_turretHmWeight = 1.3
FLOAT g_debug_turretPmWeight = 1.2
FLOAT g_debug_turretMgHmPmWeight = 1.0
BOOL g_debug_turretCooldownReset = FALSE
#ENDIF //IS_DEBUG_BUILD

//eof
