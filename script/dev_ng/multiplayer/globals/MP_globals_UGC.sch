USING "global_block_defines.sch"
USING "mp_globals_common_consts.sch"
USING "MP_globals_COMMON_definitions.sch"
USING "commands_network.sch"

GLOBALS GLOBALS_BLOCK_UGC

//Playlist length
CONST_INT FMMC_MAX_PLAY_LIST_LENGTH 16
CONST_INT FMMC_MAX_ROCKSTAR_PLAY_LIST_LENGTH 32

//Struct containing the data 
CONST_INT MAX_NUMBER_FMMC_SAVES 31
CONST_INT MAX_CLOUD_MISSION_TYPES 13

// Keith 25/8/13: A total for UGC+Bookmarks for array navigation
CONST_INT MAX_NUMBER_UGC_AND_BOOKMARKED			(MAX_NUMBER_FMMC_SAVES + MAX_NUMBER_FMMC_SAVES)

//For Rockstat Created
CONST_INT FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED 			2018
CONST_INT FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD	2000
CONST_INT FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED			100

//Max numbers for the mini games
CONST_INT ciFM_MINI_GAME_MAX_GOLF				1
CONST_INT ciFM_MINI_GAME_MAX_SHOOTING_RANGE		2
CONST_INT ciFM_MINI_GAME_MAX_TENNIS				7
CONST_INT ciFM_MINI_GAME_MAX_DARTS				1
CONST_INT ciFM_MINI_GAME_MAX_ARM_WRESTLING		10

//numbers for head 2 head
CONST_INT MAX_HEAD_TO_HEAD_CREWS	2
CONST_INT MAX_CHALLENGE_CREWS		1

//	31+31+250+250+1+1+10+2+10= 586
CONST_INT MAX_FM_AT_ONCE_CORONAS	(MAX_NUMBER_FMMC_SAVES + MAX_NUMBER_FMMC_SAVES + FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED + FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED + ciFM_MINI_GAME_MAX_GOLF + ciFM_MINI_GAME_MAX_SHOOTING_RANGE + ciFM_MINI_GAME_MAX_TENNIS + ciFM_MINI_GAME_MAX_DARTS + ciFM_MINI_GAME_MAX_ARM_WRESTLING	)	

STRUCT QUALIFYING_TOURNAMENT_PLAYLIST_FEED_STRUCT
	BOOL bFailedToSetTime
	BOOL bSetTime
	INT iTotalTime
ENDSTRUCT
QUALIFYING_TOURNAMENT_PLAYLIST_FEED_STRUCT g_sQualifyTourFeed

STRUCT CLOUD_LOADED_DEFAULT_OPTIONS
	INT 			iMaxNumberOfTeams 			//number of teams 				
	INT 			iRootContentIdHash			//Root content ID hash
	INT 			iCoronaFlowPlaylist = -1	
	INT 			iHash 						//The hash
	INT 			iTotalVotes					//The total votes
	INT 			iVersion			= -1
	INT 			iLanguage			= -1
	BOOL 			bBookMarked
	INT 			iMinGangRank
	INT 			iMinGangHate
	INT 			iHestRootContentIdArrayPos
	INT				iCnCBitset
	INT 			iStrand				= -1
//	FLOAT 			fRDis
//	VECTOR			vScenePos
ENDSTRUCT

//For the end of heist data
JobBInfo g_sJobHeistInfo 
STRUCT_INSTANCED_HEIST_ENDED g_sJobGangopsInfo
TIME_DATATYPE g_tsHeistPlanningBoardTime

STRUCT HEIST_TELEMETRY_DATA_STRUCT 
	INT iHeistHashedMac
	INT iHeistPosixTime
	INT iRootContentIdHash
ENDSTRUCT
HEIST_TELEMETRY_DATA_STRUCT g_HeistTelemetryData
CONST_INT FMMC_NUM_LABELS_FOR_DESCRIPTION_UGC 8
STRUCT FMMC_PLANNING_MISSION_HEADER_DESCRIPTIONS
	TEXT_LABEL_63 							tl63MissionDecription[FMMC_NUM_LABELS_FOR_DESCRIPTION_UGC]
	CACHED_MISSION_DESCRIPTION_LOAD_VARS    sMissionDescLoadVars
ENDSTRUCT

CONST_INT FMMC_MAX_AT_ONCE_PREP_MISSIONS 35
STRUCT FMMC_PLANNING_MISSION_HEADER_DATA
	INT 											iTotalNumMissions = FMMC_MAX_AT_ONCE_PREP_MISSIONS
	CLOUD_LOADED_MISSION_HEADER_DETAILS				sMissionHeaderVars[FMMC_MAX_AT_ONCE_PREP_MISSIONS]
	CLOUD_LOADED_DEFAULT_OPTIONS					sDefaultCoronaOptions[FMMC_MAX_AT_ONCE_PREP_MISSIONS]
	FMMC_PLANNING_MISSION_HEADER_DESCRIPTIONS		sDesc[FMMC_MAX_AT_ONCE_PREP_MISSIONS]
ENDSTRUCT
FMMC_PLANNING_MISSION_HEADER_DATA g_FMMC_PLANNING_MISSION


//The strand IDs
CONST_INT ciFLOW_STRAND_LOW_RIDER 0

CONST_INT ciLOW_FLOW_MISSION_DRIVE_BY_SHOOTOUT 		0
CONST_INT ciLOW_FLOW_MISSION_TRANSPORT_LOWRIDERS 	1
CONST_INT ciLOW_FLOW_MISSION_LOWRIDERS_RESCUE 		2
CONST_INT ciLOW_FLOW_MISSION_FUNERAL_SHOOTOUT 		3
CONST_INT ciLOW_FLOW_MISSION_BLOW_UP_LOWRIDERS		4
CONST_INT ciLOW_FLOW_MISSION_PHOTO_CARS 			5
CONST_INT ciLOW_FLOW_MISSION_LOWRIDER_ASSASSINATION	6
CONST_INT ciLOW_FLOW_MISSION_LOWRIDER_FINALE 		7
CONST_INT ciLOW_FLOW_MISSION_FLOW_OVER_CALL			8

CONST_INT ciLOW_FLOW_MISSION_VCM_RESCUE				9
CONST_INT ciLOW_FLOW_MISSION_VCM_BRAWL				10
CONST_INT ciLOW_FLOW_MISSION_VCM_MEETING				11
CONST_INT ciLOW_FLOW_MISSION_VCM_OIL_DEMOLITION				12
CONST_INT ciLOW_FLOW_MISSION_VCM_DEFEND				13
CONST_INT ciLOW_FLOW_MISSION_VCM_FINALE				14

#IF FEATURE_CASINO_HEIST
CONST_INT ciLOW_FLOW_MISSION_CASINO_HEIST_INT1		15
CONST_INT ciLOW_FLOW_MISSION_CASINO_HEIST_INT2		16
#ENDIF

#IF FEATURE_HEIST_ISLAND
CONST_INT ciLOW_FLOW_MISSION_HEIST_ISLAND_HS4F_INT	17
#ENDIF

//Data for pointing at the strand
STRUCT FLOW_MISSION_DETAILS
	INT iRockstarArrayMissionPos[ciMAX_ROCKSTAR_FLOW_MISSIONS]	//The Pointer to the rockstar Content
	INT iNumberOfMissions										//The number of missions in the strand
	INT iMissionsSetUpBitSet
ENDSTRUCT


STRUCT FMMC_ROCKSTAR_CREATED_STRUCT
	INT 									iVersionLoaded
	INT 									iHashOfLoaded
	INT 									iTotalNumMissions = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED
	INT 									iTimeOfLoad
	CLOUD_LOADED_MISSION_HEADER_DETAILS		sMissionHeaderVars[FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED]
	CLOUD_LOADED_DEFAULT_OPTIONS			sDefaultCoronaOptions[FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED]
	FLOW_MISSION_DETAILS					sFlowMissionDetails[ciMAX_ROCKSTAR_FLOW_STRANDS]
	BOOL									bDataLoaded
	TEXT_LABEL_23 							tlHesitRootContentId[NUM_HEIST_STRANDS]
	INT										iRootContentIdNum
	INT										iTypeCount[MAX_CLOUD_MISSION_TYPES]
	INT										iTypePlayedCount[MAX_CLOUD_MISSION_TYPES]	
ENDSTRUCT
FMMC_ROCKSTAR_CREATED_STRUCT g_FMMC_ROCKSTAR_CREATED		//MAX_FM_AT_ONCE_CORONAS

STRUCT FMMC_ROCKSTAR_VERIFIED_STRUCT
	INT 									iVersionLoaded
	INT 									iHashOfLoaded
	INT 									iTimeOfLoad
	TEXT_LABEL_23 							tlOwnerPretty[FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED]
	CLOUD_LOADED_MISSION_HEADER_DETAILS		sMissionHeaderVars[FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED]
	CLOUD_LOADED_DEFAULT_OPTIONS			sDefaultCoronaOptions[FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED]
	BOOL									bDataLoaded
	BOOL									bScNickName[FMMC_MAX_AT_ONCE_ROCKSTAR_VERIFIED]
	INT										iTypeCount[MAX_CLOUD_MISSION_TYPES]
	INT 									iMissionToLaunch
ENDSTRUCT
FMMC_ROCKSTAR_VERIFIED_STRUCT g_FMMC_ROCKSTAR_VERIFIED				//MAX_FM_AT_ONCE_CORONAS
FMMC_ROCKSTAR_VERIFIED_STRUCT g_FMMC_ROCKSTAR_VERIFIED_PAUSE_MENU	//MAX_FM_AT_ONCE_CORONAS


STRUCT FMMC_GLOBAL_HEADER_STRUCT 
	INT 								iNumberOfMissions
	INT 								iTotalNumberOfMissions
	INT 								iMissionToLaunch
	INT 								iNumberOfPublished
	INT 								iNumberOfBookMarks
	CLOUD_LOADED_MISSION_HEADER_DETAILS sMyMissionHeaderVars[MAX_NUMBER_FMMC_SAVES+MAX_NUMBER_FMMC_SAVES]
	INT									iCheckPermission[MAX_NUMBER_FMMC_SAVES+MAX_NUMBER_FMMC_SAVES]
	INT 								iMyTotalLikes
	INT 								iMyHighestLikes 
	BOOL 								bHavePublishedMission		= FALSE
	BOOL 								bHavePublishedDM			= FALSE
	BOOL 								bHavePublishedRace			= FALSE
ENDSTRUCT
FMMC_GLOBAL_HEADER_STRUCT g_FMMC_HEADER_STRUCT	//MAX_FM_AT_ONCE_CORONAS
FMMC_GLOBAL_HEADER_STRUCT g_FMMC_HEADER_STRUCT_PAUSE_MENU_MY_MISSIONS
FMMC_GLOBAL_HEADER_STRUCT g_FMMC_HEADER_STRUCT_PAUSE_MENU_BOOKMARKS

STRUCT FMMC_RECENT_MISSIONS_HEADER_STRUCT 
	INT 								iNumberOfMissions
	INT 								iRootContentIdHash[MAX_NUMBER_FMMC_SAVES]
	CLOUD_LOADED_MISSION_HEADER_DETAILS sRecentMissionHeaderVars[MAX_NUMBER_FMMC_SAVES]
	BOOL								bBookMarked[MAX_NUMBER_FMMC_SAVES]
	INT 								iMyTotalLikes
	INT 								iMyHighestLikes 
	BOOL 								bHavePublishedMission		= FALSE
	BOOL 								bHavePublishedDM			= FALSE
	BOOL 								bHavePublishedRace			= FALSE
ENDSTRUCT
FMMC_RECENT_MISSIONS_HEADER_STRUCT g_sRECENT_MISSION_HISTORY


CONST_INT ciMAX_AT_ONCE_GANG_ATTACKS	85
STRUCT GANG_ATTACK_LOCATE_VARS
	VECTOR vMin[ciMAX_AT_ONCE_GANG_ATTACKS]
	VECTOR vMax[ciMAX_AT_ONCE_GANG_ATTACKS]
	FLOAT fRadius[ciMAX_AT_ONCE_GANG_ATTACKS]
	INT iContentIdHash[ciMAX_AT_ONCE_GANG_ATTACKS]
	INT iNumberLoaded
ENDSTRUCT
GANG_ATTACK_LOCATE_VARS g_FMMC_ROCKSTAR_GANG_ATTACKS


CONST_INT HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD	0
STRUCT STRUCT_DONE_NUM_MISSION_TYPE_AWARD_DATA
	INT iTargetNumber
	INT iCurrentNumber
	INT TrackBitSet[(FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED/32)]
ENDSTRUCT
STRUCT_DONE_NUM_MISSION_TYPE_AWARD_DATA g_sDoneNumMissionTypeAwardData[1]

STRUCT FMMC_MISSION_DETAILS_STRUCT
	TEXT_LABEL_23 	tlName
	TEXT_LABEL_23 	tlOwner
	TEXT_LABEL_63 	tlMissionName
	TEXT_LABEL_63 	tlMissionDec
	INT				iMissionDecHash
	VECTOR			vStartPos
	VECTOR			vCamPos
	VECTOR			vCamHead
	INT 			iType
	INT 			iSubType
	INT 			iMinPlayers
	INT 			iPhotoPath
	INT 			iRank
	INT 			iMaxNumberOfTeams 			//number of teams 				
	INT 			iMaxPlayers
	INT 			iRating
	INT 			iBestScore
	INT 			iBestTime
	INT 			iBitSet
	INT 			iBitSetTwo
	INT 			iAdversaryModeType
	BOOL 			bInUse
	UGC_CATEGORY 	nCategory
ENDSTRUCT

ENDGLOBALS	//	GLOBALS_BLOCK_UGC

//////////
GLOBALS GLOBALS_BLOCK_UGC_2

STRUCT CURRENT_PLAY_LIST_DETAILS
	INT 							iVersionLoaded
	INT 							iCashBet			= ciMIN_CHALLENGE_CASH_BET
	INT 							iCashBetCache
	INT 							iPlaylistStartPosCache
	INT								iInitialCashWager	= ciMIN_CHALLENGE_CASH_BET
	INT 							iCashWinnings
	INT 							iTotatlScore
	INT 							iTotatlTime
	INT 							iMinNumber			= 0
	INT 							iMaxNumber			= NUM_NETWORK_PLAYERS
	INT								iPlayListBitSet
	BOOL 							bChallengeCompatable	= TRUE
	BOOL 							bHead2HeadCompatable	= TRUE
	BOOL 							bChallengeIsStillOpen	= TRUE
	TEXT_LABEL_63					tl31PlaylistName
	TEXT_LABEL_31					tl31ChallengeName
	TEXT_LABEL_23					tl23ChallengeContentID
	TEXT_LABEL_23					tl31szContentID
	TEXT_LABEL_63					tl63ClanName
	INT								iClanID
	INT								iLength
	INT								iPlaylistStartPos
	INT 							iCurrentPlayListPosition
	INT 							iPlaylistProgress = 1
	INT								iH2HCrewID1				= -1					//The two crew participating in H2H
	INT								iH2HCrewID2				= -1
	UGC_CATEGORY 					nCategory
	FMMC_MISSION_DETAILS_STRUCT		sLoadedMissionDetails[FMMC_MAX_PLAY_LIST_LENGTH]
	UGC_CONTENT_ID_QUERY 			tl31ContentIDs
ENDSTRUCT
CURRENT_PLAY_LIST_DETAILS g_sCurrentPlayListDetails
CURRENT_PLAY_LIST_DETAILS g_sMenuPlayListDetails
//CURRENT_PLAY_LIST_DETAILS g_sOnTheGoPlayListDetails

STRUCT PLAY_LIST_MISSION_NAMES
	//TEXT_LABEL_31					tl31MissionName[FMMC_MAX_PLAY_LIST_LENGTH]
	INT								iType[FMMC_MAX_PLAY_LIST_LENGTH]
ENDSTRUCT

STRUCT PLAY_LIST_HEADER_DETAILS
	TEXT_LABEL_63					tl31PlaylistName[FMMC_MAX_PLAY_LIST_LENGTH]
	TEXT_LABEL_23					tl31szContentID[FMMC_MAX_PLAY_LIST_LENGTH]
	TEXT_LABEL_63					tl63ClanName[FMMC_MAX_PLAY_LIST_LENGTH]
	PLAY_LIST_MISSION_NAMES			sMissionNames[FMMC_MAX_PLAY_LIST_LENGTH]
	INT								iCashBet[FMMC_MAX_PLAY_LIST_LENGTH]
	INT								iTotatlScore[FMMC_MAX_PLAY_LIST_LENGTH]
	INT								iTotatlTime[FMMC_MAX_PLAY_LIST_LENGTH]
	INT 							iMyClanID
	INT 							iTotalPlaylists
	INT 							iHead2HeadCompatableBitSet
	INT 							iChallengeCompatableBitSet
ENDSTRUCT
PLAY_LIST_HEADER_DETAILS g_sLoadedPlaylistDetails
PLAY_LIST_HEADER_DETAILS g_sLoadedBookMarkedPlaylistDetails
PLAY_LIST_HEADER_DETAILS g_sLoadedChallengeDetails

STRUCT PLAY_LIST_HEADER_DETAILS_ROCKSTAR
	TEXT_LABEL_63					tl31PlaylistName[FMMC_MAX_ROCKSTAR_PLAY_LIST_LENGTH]
	TEXT_LABEL_23					tl31szContentID[FMMC_MAX_ROCKSTAR_PLAY_LIST_LENGTH]
	TEXT_LABEL_63					tl63ClanName[FMMC_MAX_ROCKSTAR_PLAY_LIST_LENGTH]
	PLAY_LIST_MISSION_NAMES			sMissionNames[FMMC_MAX_ROCKSTAR_PLAY_LIST_LENGTH]
	INT								iCashBet[FMMC_MAX_ROCKSTAR_PLAY_LIST_LENGTH]
	INT								iTotatlScore[FMMC_MAX_ROCKSTAR_PLAY_LIST_LENGTH]
	INT								iTotatlTime[FMMC_MAX_ROCKSTAR_PLAY_LIST_LENGTH]
	INT 							iMyClanID
	INT 							iTotalPlaylists
	INT 							iHead2HeadCompatableBitSet
	INT 							iChallengeCompatableBitSet
ENDSTRUCT
PLAY_LIST_HEADER_DETAILS_ROCKSTAR g_sLoadedRockstarPlaylistDetails

CONST_INT ciMAX_AT_ONCE_CHALLANGES 10
STRUCT INDIVIDUAL_CHALANGE_STRUCT_DATA
	INT 			iExpireTime
	INT 			iAwardType
	INT 			iAwardAmount
	INT 			iStartTime
	INT 			iEndTime
	INT 			iChallangeType
	TEXT_LABEL_63 	tl63GrpHandel
	TEXT_LABEL_23	tl23File
	TEXT_LABEL_23	tl23ChalangeCreator
	TEXT_LABEL_23	tl23ChalangeDocName
	TEXT_LABEL_23	tl23Owner
	TEXT_LABEL_23	tl23MissionName
	INT 			iMissionType
	VECTOR			vPos
ENDSTRUCT
INDIVIDUAL_CHALANGE_STRUCT_DATA g_sChallangeData[ciMAX_AT_ONCE_CHALLANGES]
INT g_iCurrentActiveChallange
INT g_iNumberOfActiveChallanges
CONST_INT UGC_PATH_PHOTO_NG 2

//Mission launch details for FMMC Launcher
STRUCT MISSION_TO_LAUNCH_DETAILS
	INT					iIntroStatus
	INT					iPhotoPath	= -1
	INT					iSpeedZoneIndex
	INT					iHeistStatus
	INT					iLobbyStatus
	INT 				iRaceSubType
	INT					iInviteScreenStatus
	INT					iInCoronaStatus
	INT					iInCoronaStatusCache
	INT					iCoronaWalkInCameraStatus
	INT					iBettingStatus
	INT					iLoadStatus
	INT 				iNPCplayers
	INT					iTranisitionSessionSetUpStage
	INT 				iMissionType
	INT 				iMinPlayers
	INT 				iHashOfMission
	INT 				iRank
	INT					iRootContentIDHash
	INT					iDownloadFailCount
	#IF IS_DEBUG_BUILD
	INT 				iLesRating
	#ENDIF
	SCENARIO_BLOCKING_INDEX sbiBlockingArea
	SCRIPT_TIMER		sWalkInTimer
	SCRIPT_TIMER		sNjvsTimer	
	SCRIPT_TIMER		sHostSwapTimer	
	SCRIPT_TIMER		sRoundsKillScriptTimer
	SCRIPT_TIMER		sHeistInviteKillScriptTimer
	INT 				iRating
	INT 				iMaxParticipants
	INT 				iMissionToStart
	INT 				iMissionVariation
	INT 				iMissionSubType
	INT					iHeistSubType				// KGM 24/6/13: [becoming obsolete] // KGM 1/3/13: This is a 'copy' of the missionSubType field used by FMMC_MISSIONS only to signify a Heist (the iMissionSubType field gets modified by the leaderboards)
	BOOL				bIsHeistMission				// KGM 24/6/13: TRUE if the mission being launched is a Heist Mission
	BOOL				bIsHeistPlanning			// KGM 23/4/14: TRUE if the mission being launched is a Heist Planning mission
	BOOL				bIsForHeistPrepCutscene		// KGM 15/5/14: TRUE if this is a Heist Finale corona being used only to trigger the Heist Pre-Planning Cutscene (the intro cutscene)
	BOOL				bIsForMidStrandCutscene		// JA  5/8/14: TRUE if this is a Heist Finale corona being used as a mid strand cutscene rather than the intro
	BOOL				bIsForTutorialCutscene		// AMEC 11/9/14: TRUE is this heist corona is for a tutorial intro and mission.
	BOOL				bIsContactMission			// KGM 4/6/13: TRUE if the mission being launched is a Contact Mission
	INT					contactAsInt				// KGM 17/6/13: Add the mission Contact as an INT
	INT					iCoronaMissionSubType // JA: 18/07/13: Add this to use as subtype for corona menu setup
	INT 				iCreatorID 
	INT 				iLeaderBoard
	INT 				iLaps
	INT 				iMissionDecHash
	INT 				iRandChanceToHost				= 0	
	BOOL 				bViewingGlobalLeaderboard
	//BOOL 				bMissionDecLoaded
	BOOL 				bTutorialSetUp
	BOOL 				bLeaderBoardAvailable
	BOOL 				bCoDriverBoard
	BOOL 				bPreviousCoDriverState
	BOOL 				bTransitionSessionSetUp
	BOOL				bOverrideChat
	BOOL 				bGotRandHostChance
	BOOL 				bSetPedVehicleBudget
	BOOL				bPlayedAlready
	BOOL				bSetUpCoronaCamOnCallCleanUp
	INT 				iShootingRangeLBCurrentType //CDM 2013-3-23 This is used to determine what version of shooting range leaderboard to use
	INT 				iShootingRangeLBStoredType	//as above
	INT 				iShootingRangeLBWeaponType //CDM 2013-9-18 used to differenciate between weapons for shooting range
	INT  				iShootingRangeLBStoredWeaponType // as above
	BOOL 				bGetSCLBSetupData //used to grab leaderboard setup when required
	BOOL 				bRefreshButtons
	BOOL 				SetUpCompleate
	BOOL 				bForcedOut
	BOOL 				bCleanUptransitionSession
	BOOL 				bTransitionSessionStarted
	BOOL 				bTransitionSessionCleanedUp
	BOOL 				BlockJoinForLateJip
	BOOL				bTeamsSetUpForRounds
	BOOL 				bCleanUpAsKicked
	BOOL 				bPhotoLoaded
	BOOL 				bSetUpSpinner
	BOOL 				bSetJipBroadCast
	BOOL				bUseHostJoinScreen		// KGM 10/7/13: TRUE if the Host/Join screen should be displayed, otherwise FALSE
	VECTOR			 	vStartPoint
	VECTOR			 	vCamPos
	VECTOR			 	vCamHead
	BOOL 				bDoWalkOut	= TRUE
	INT 				iStartingBitSet
	INT 				theVoteMissionToUseValue
	INT 				theVoteVariationToUseValue
	TEXT_LABEL_23	 	stUserName
	TEXT_LABEL_23		stFileName
	TEXT_LABEL_63		stMissionName
	TEXT_LABEL_63		stMissionDec
	CLOUD_LOADED_DEFAULT_OPTIONS sDefaultCoronaOptions
	BOOL  				bDefaultOptionsLoaded
	//CAMERA_INDEX 		ciCam
	SCALEFORM_INDEX 	siIntorScaleForm
	SCRIPT_TIMER		sCoronaIntroTimer
	SCRIPT_TIMER		sCoronaCheckPlayerTimer
	SCRIPT_TIMER		sQuickmatchDelayTimer
	INT					iQuickMatchAttempts
	INT 				iQuickMatchMaxFailCount
	UGC_CATEGORY		eUgcCategory			= UGC_CATEGORY_NONE					//The category of the UGC mission
	INT					iTimerWalkInCamera		//Ross W: walk in camera timer.
	BOOL 				bTryToJoinPlayer
	BOOL 				bAllReadyForHeistCut
	PLAYER_INDEX		piPlayerToTryTojoin
	TIME_DATATYPE		controlWalkOutTimer
	INT 				iHeistWarningStage
	INT 				iHeistWarningBitSet
	//Walk in phone object
	OBJECT_INDEX oiWalkInPhone
	
	BOOL bConfirmWarningScreen
	INT iWarningScreenBS
	
ENDSTRUCT
MISSION_TO_LAUNCH_DETAILS g_sLaunchMissionDetailsTransition


CONST_INT ciMAX_RECENT_LOCAL_CONTENT_ID_HISTORY 5
STRUCT RECENT_LOCAL_CONTENT_ID_HISTORY
	TEXT_LABEL_23 g_tl23RecentMissionHistory[ciMAX_RECENT_LOCAL_CONTENT_ID_HISTORY]
	INT iStorageCount
ENDSTRUCT
RECENT_LOCAL_CONTENT_ID_HISTORY g_sRecentMissionHistory

//for adding to the tutorial race type
CONST_INT ciTUTORIAL_RACE_TYPE_CONST 100

//Unique match ID for the sc
TEXT_LABEL_31 g_tl31uniqueMatchId

STRUCT QUIT_MODE_TELEMETRY_DATA
	TIME_DATATYPE duration
	BOOL voteDiff
ENDSTRUCT
QUIT_MODE_TELEMETRY_DATA g_NjvsAndQuitModeTelemetryData

//Vars needed for getting the challenge difficulty
STRUCT CHALLENGE_DIFFICULTY_STRUCT
	INT iTotatlUniquePlays
	INT iMyRankPrediction
ENDSTRUCT
CHALLENGE_DIFFICULTY_STRUCT g_sCHALLENGE_DIFFICULTY

CONST_INT NUM_JOBS_TO_GRAB					18
CONST_INT JOB_PANEL_TOTAL					21
CONST_INT NUM_OF_TEAM_JOBS_TO_GRAB			4	
CONST_INT ciNUMBER_OF_LTS_TO_GRAB 			3
CONST_INT ciNUMBER_OF_NEW_VS_TO_GRAB 		6

CONST_INT ciNUMBER_OF_SAME_TO_GRAB 			6
CONST_INT ciNUMBER_OF_VERIFIED_TO_GRAB 		3

CONST_INT ciNUMBER_OF_VERIFIED_TYPE_RACE 	0
CONST_INT ciNUMBER_OF_VERIFIED_TYPE_DM	 	1
CONST_INT ciNUMBER_OF_VERIFIED_TYPES 		2

STRUCT MISSION_TYPE_COUNT
	INT iType[MAX_CLOUD_MISSION_TYPES]
	INT iTypeVerifiedType[ciNUMBER_OF_VERIFIED_TYPES]
	INT iAttempts[ciMAX_RECENT_LOCAL_CONTENT_ID_HISTORY]
	BOOL bUseRandom = TRUE
	BOOL bUseRandomKing
	INT iLastMissionArrayPos
	INT iNumLtsFound
	INT iMissionThatIsLTS[ciNUMBER_OF_LTS_TO_GRAB]
	INT iNumNewVsFound
	INT iMissionThatIsNewVs[ciNUMBER_OF_NEW_VS_TO_GRAB]
	INT iNewVsHash[ciNUMBER_OF_NEW_VS_TO_GRAB]
	INT iNumSameAsLastToFind
	BOOL bTypeCountSetUp
ENDSTRUCT

STRUCT TOTAL_COUNT_VARS
	INT iNumberOfTeamMissionsFound = NUM_OF_TEAM_JOBS_TO_GRAB
	INT iNumberOfSameAsLastFound = ciNUMBER_OF_SAME_TO_GRAB
	INT iNumberOfNewVsFound
	INT iNumberLTSCompatable
	INT iTotalNumberCompatable
ENDSTRUCT

CONST_INT ciEOJV_TYPE_ROCKSTAR_CREATED	0
CONST_INT ciEOJV_TYPE_ROCKSTAR_VERIFIED	1
CONST_INT ciEOJV_TYPE_MAX				2
INT g_iCurrentPosixforNJVS
STRUCT SERVER_NEXT_JOB_VARS
	SCRIPT_TIMER timerVotingEnds
	SCRIPT_TIMER timerServerRefresh
	INT iNumberOnJob
	INT iRefreshValue
	INT iHighestPlayerRank
	INT iAverageRank
	INT iJobVoteWinner = -1
	INT iSecondryJobVoteWinner = -1
	INT iRandomProgress
	INT iStoredRandom[NUM_JOBS_TO_GRAB]
//	INT iArrayPos[NUM_JOBS_TO_GRAB]
	INT iJobVotes[JOB_PANEL_TOTAL]
	INT iVoteTotal
	INT iMajorityVoteTotal //1816729
	INT iFound
	MISSION_TYPE_COUNT sTypeCounts
	INT iTeamMissionsFound
	INT iServerLogicProgress
	INT iServerJobBitSet
	INT iFoundJobOfTypeBitset
	INT iAttempts
	INT iNumRefreshed
	INT iNumberCompatableStage
	INT iNumberCompatable = -1	
	TOTAL_COUNT_VARS sCounts[ciEOJV_TYPE_MAX]
	
	
	INT iMatchTypeUnlockBitSet
	INT iCheckPlayersHaveJobsBitSet
	
	INT iVerifiedBitSet
	
	INT iHostMigrateBitSet
	PLAYER_INDEX playerCurrentJobHost
	
	INT iCustomVehicleNameBS = 0
	
	VECTOR vRestartPos 
	TEXT_LABEL_23 tlFileName[NUM_JOBS_TO_GRAB]
	#IF IS_DEBUG_BUILD
	BOOL bForceVoteForFreemode
	#ENDIF	
ENDSTRUCT


#IF IS_DEBUG_BUILD
INT itempiconforpausemenu
#ENDIF
SERVER_NEXT_JOB_VARS g_sMC_serverBDEndJob

STRUCT JOB_POINTS_PLAYER
	INT iJobPointsThisSession = 0
	INT iJobPointsStart		  = 0
ENDSTRUCT
JOB_POINTS_PLAYER g_sJobPoints

STRUCT PASSIVE_MODE_STORED
	BOOL bEnabled
ENDSTRUCT
PASSIVE_MODE_STORED g_PassiveModeStored

#IF IS_DEBUG_BUILD
INT g_iCS_JOB_ForceMaxPlayers
INT g_iCS_JOB_ForceRank
INT g_iForceTeam = -1
INT g_iEndOfJobScreenJobToFail = -1
INT g_iEndOfJobScreenJobToFail2 = -1
BOOL g_bBlockParachuting, g_bIvePlayedAllContactMissions, g_bBlockSurvival, g_bFakeContentMissMatch, g_bForceCloudEndOfJobFail, g_bForceJobFailSafe, g_bForceJobFailSafeKing, g_iEndOfJobScreenJobToFailAll, g_bEndOfJobScreenDoRefresh, g_bPrintMissionNames, g_bLaunchedViaZmenu
BOOL g_bDebugLaunchedMission
#ENDIF


CONST_INT ciMISSION_ENTERY_TYPE_INVALID									-1
CONST_INT ciMISSION_ENTERY_TYPE_HOST_CORONA								0
CONST_INT ciMISSION_ENTERY_TYPE_JOIN_CORONA								1
CONST_INT ciMISSION_ENTERY_TYPE_NPC_INVITE								2
CONST_INT ciMISSION_ENTERY_TYPE_PLAYLIST								3
CONST_INT ciMISSION_ENTERY_TYPE_HOST_PAUSE_MENU							4
CONST_INT ciMISSION_ENTERY_TYPE_JOIN_PAUSE_MENU_MP						5
CONST_INT ciMISSION_ENTERY_TYPE_JOIN_PAUSE_MENU_SP						6
CONST_INT ciMISSION_ENTERY_TYPE_MY_MISSION_PAUSE_MENU					7
CONST_INT ciMISSION_ENTERY_TYPE_BOOKMARK_PAUSE_MENU						8
CONST_INT ciMISSION_ENTERY_TYPE_END_OF_JOB_SCREEN						9
CONST_INT ciMISSION_ENTERY_TYPE_END_OF_JOB_SCREEN_RESTART				10
CONST_INT ciMISSION_ENTERY_TYPE_PHONE									11
CONST_INT ciMISSION_ENTERY_TYPE_PHONE_RANDOM							12
CONST_INT ciMISSION_ENTERY_TYPE_JIP										13
CONST_INT ciMISSION_ENTERY_TYPE_JIP_PLAYLIST							14
CONST_INT ciMISSION_ENTERY_TYPE_INVITE_FROM_SP							15
CONST_INT ciMISSION_ENTERY_TYPE_INVITE_FROM_MP							16
CONST_INT ciMISSION_ENTERY_TYPE_WALK_IN									17
CONST_INT ciMISSION_ENTERY_TYPE_QUICK_RESTART							18
CONST_INT ciMISSION_ENTERY_TYPE_SPECTATE								19
CONST_INT ciMISSION_ENTERY_TYPE_EVENT_BOOT								20
CONST_INT ciMISSION_ENTERY_TYPE_ON_CALL									21
CONST_INT ciMISSION_ENTERY_TYPE_STRAND_MISSION							22
CONST_INT ciMISSION_ENTERY_TYPE_ROUNDS_MISSION							23
CONST_INT ciMISSION_ENTERY_TYPE_RANDOM_BOOT								24
CONST_INT ciMISSION_ENTERY_TYPE_FEATURED_PLAYLIST						25
CONST_INT ciMISSION_ENTERY_TYPE_NJVS_QM_JOIN							26
CONST_INT ciMISSION_ENTERY_TYPE_NJVS_QM_HOST							27
CONST_INT ciMISSION_ENTERY_TYPE_HEIST_QUICK_INVITE						28
CONST_INT ciMISSION_ENTERY_TYPE_HEIST_PLANNING_BOARD					29
CONST_INT ciMISSION_ENTERY_TYPE_FM_MISSION_FLOW							30
CONST_INT ciMISSION_ENTERY_TYPE_IN_PROGRESS_MATCHMAKING					31
CONST_INT ciMISSION_ENTERY_TYPE_V2_CORONA								32
CONST_INT ciMISSION_ENTERY_TYPE_V2_CORONA_ON_CALL						33
CONST_INT ciMISSION_ENTERY_TYPE_V2_PROFESSIONAL_CORONA					34
CONST_INT ciMISSION_ENTERY_TYPE_V2_PROFESSIONAL_CORONA_ON_CALL			35
CONST_INT ciMISSION_ENTERY_TYPE_V2_ADVERSARY_CORONA						36
CONST_INT ciMISSION_ENTERY_TYPE_V2_ADVERSARY_CORONA_ON_CALL				37
CONST_INT ciMISSION_ENTERY_TYPE_CLUB_HOUSE								38
CONST_INT ciMISSION_ENTERY_TYPE_ADVERSARY_SERIES_CORONA					39
CONST_INT ciMISSION_ENTERY_TYPE_ADVERSARY_SERIES_CORONA_ON_CALL			40
CONST_INT ciMISSION_ENTERY_TYPE_ADVERSARY_SERIES_CORONA_1				41
CONST_INT ciMISSION_ENTERY_TYPE_ADVERSARY_SERIES_CORONA_ON_CALL_1		42
CONST_INT ciMISSION_ENTERY_TYPE_ADVERSARY_SERIES_CORONA_2				43
CONST_INT ciMISSION_ENTERY_TYPE_ADVERSARY_SERIES_CORONA_ON_CALL_2		44
CONST_INT ciMISSION_ENTERY_TYPE_SPECIAL_VEHICLE_SERIES_CORONA			45
CONST_INT ciMISSION_ENTERY_TYPE_SPECIAL_VEHICLE_SERIES_CORONA_ON_CALL	46
CONST_INT ciMISSION_ENTERY_TYPE_BUNKER_SERIES_CORONA					47
CONST_INT ciMISSION_ENTERY_TYPE_BUNKER_SERIES_CORONA_ON_CALL			48
CONST_INT ciMISSION_ENTERY_TYPE_TRANSFORM_SERIES_CORONA					49
CONST_INT ciMISSION_ENTERY_TYPE_TRANSFORM_SERIES_CORONA_ON_CALL			50
CONST_INT ciMISSION_ENTERY_TYPE_TARGET_ASSAULT_SERIES_CORONA			51
CONST_INT ciMISSION_ENTERY_TYPE_TARGET_ASSAULT_SERIES_CORONA_ON_CALL	52
CONST_INT ciMISSION_ENTERY_TYPE_HOTRING_SERIES_CORONA					53
CONST_INT ciMISSION_ENTERY_TYPE_HOTRING_SERIES_CORONA_ON_CALL			54
CONST_INT ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA						55
CONST_INT ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA_ON_CALL				56
CONST_INT ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA_WALL				57
CONST_INT ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA_WALL_ON_CALL		58
CONST_INT ciMISSION_ENTERY_TYPE_RACE_SERIES_CORONA						59
CONST_INT ciMISSION_ENTERY_TYPE_RACE_SERIES_CORONA_ON_CALL				60
CONST_INT ciMISSION_ENTERY_TYPE_SURVIVAL_SERIES_CORONA					61
CONST_INT ciMISSION_ENTERY_TYPE_SURVIVAL_SERIES_CORONA_ON_CALL			62
CONST_INT ciMISSION_ENTERY_TYPE_OPEN_WHEEL_SERIES_CORONA				63
CONST_INT ciMISSION_ENTERY_TYPE_OPEN_WHEEL_SERIES_CORONA_ON_CALL		64
CONST_INT ciMISSION_ENTERY_TYPE_CASINO_HEIST_BOARD						65
CONST_INT ciMISSION_ENTERY_TYPE_CASINO_HEIST_ON_CALL					66
CONST_INT ciMISSION_ENTERY_TYPE_HEIST_ISLAND_TABLE						67
CONST_INT ciMISSION_ENTERY_TYPE_HEIST_ISLAND_ON_CALL					68
CONST_INT ciMISSION_ENTERY_TYPE_TUNER_JOB_BOARD							69
CONST_INT ciMISSION_ENTERY_TYPE_STREET_RACE_SERIES_CORONA				70
CONST_INT ciMISSION_ENTERY_TYPE_STREET_RACE_SERIES_CORONA_ON_CALL		71
CONST_INT ciMISSION_ENTERY_TYPE_PURSUIT_SERIES_CORONA					72
CONST_INT ciMISSION_ENTERY_TYPE_PURSUIT_SERIES_CORONA_ON_CALL			73
#IF FEATURE_GEN9_EXCLUSIVE
CONST_INT ciMISSION_ENTERY_TYPE_HSW_RACE_SERIES_CORONA					74
CONST_INT ciMISSION_ENTERY_TYPE_HSW_RACE_SERIES_CORONA_ON_CALL			75
CONST_INT ciMISSION_ENTERY_TYPE_LANDING_PAGE							76
#ENDIF
CONST_INT ciMISSION_ENTERY_TYPE_FIXER_APP								77
CONST_INT ciMISSION_ENTERY_TYPE_FIXER_PAYPHONE_REQUESTED				78
CONST_INT ciMISSION_ENTERY_TYPE_FIXER_PAYPHONE_AMBIENT					79
CONST_INT ciMISSION_ENTERY_TYPE_FIXER_SECURITY_CONTRACT_REQUESTED		80
CONST_INT ciMISSION_ENTERY_TYPE_FIXER_WORLD_TRIGGER						81
CONST_INT ciMISSION_ENTERY_TYPE_FIXER_SHORT_TRIP_MAIN_ENTRANCE			82
CONST_INT ciMISSION_ENTERY_TYPE_FIXER_SHORT_TRIP_SMOKING_ROOM			83
#IF FEATURE_GEN9_EXCLUSIVE
CONST_INT ciMISSION_ENTERY_TYPE_GAME_INTENT								84
#ENDIF
CONST_INT ciMISSION_ENTERY_TYPE_COMMUNITY_SERIES_CORONA					85
CONST_INT ciMISSION_ENTERY_TYPE_COMMUNITY_SERIES_CORONA_ON_CALL			86
CONST_INT ciMISSION_ENTERY_TYPE_CAYO_PERICO_SERIES_CORONA				87
CONST_INT ciMISSION_ENTERY_TYPE_CAYO_PERICO_SERIES_CORONA_ON_CALL		88
CONST_INT ciMISSION_ENTERY_TYPE_NIGHTCLUB_APP							89
CONST_INT ciMISSION_ENTERY_TYPE_NIGHTCLUB_PHONE_MENU					90
CONST_INT ciMISSION_ENTERY_TYPE_NIGHTCLUB_INTERACTION					91
CONST_INT ciMISSION_ENTERY_TYPE_TERRORBYTE_APP							92
CONST_INT ciMISSION_ENTERY_TYPE_NIGHTCLUB_TIMER							93
CONST_INT ciMISSION_ENTERY_TYPE_NIGHTCLUB_ACCEPT_PHONE_CALL				94
 
INT g_iMissionEnteryType = ciMISSION_ENTERY_TYPE_INVALID

BOOL gdisablerankupmessage
BOOL grankbarchangedlevelactivatebigmessage
INT irankbarchangedlevelactivatebigmessagetimer

STRUCT_TO_FREEMODE_GAMERS	g_sGamersFollow

// NJVS
//Vars needed for downloading all the images
STRUCT STRUCT_IMAGES_FOR_NEXT_JOB_SCREEN_VARS
	STRUCT_DL_PHOTO_VARS_LITE sDownLoadPhotoVars[NUM_JOBS_TO_GRAB]
	INT iDownloadedBitSet
	INT iSucessBitSet
	INT iSucessBitSetOld
	INT iImageBoolsBitSet
	INT iPlayerWhoVoted = -1
	INT iJobVotedFor
	BOOL bShowVote
ENDSTRUCT

STRUCT_IMAGES_FOR_NEXT_JOB_SCREEN_VARS g_sNextJobImages

CACHED_MISSION_DESCRIPTION_LOAD_VARS g_sCMDLvar[NUM_JOBS_TO_GRAB]

//scripted cutscene variables
ENUM eFMMC_SCRIPTED_CUTSCENE_PROGRESS
	SCRIPTEDCUTPROG_INIT = 0,
	SCRIPTEDCUTPROG_WAIT_PLAYER_SELECTION,
	SCRIPTEDCUTPROG_FADEOUT,
	SCRIPTEDCUTPROG_FADEOUT_AND_WARP,
	SCRIPTEDCUTPROG_WARP_PLAYERS,
	SCRIPTEDCUTPROG_WARPINTRO,
	SCRIPTEDCUTPROG_PROCESS_SHOTS,
	SCRIPTEDCUTPROG_WARPEND_SHORT,
	SCRIPTEDCUTPROG_WARPEND_LONG,
	SCRIPTEDCUTPROG_ENDING_PROCEDURES,
	SCRIPTEDCUTPROG_CLEANUP
ENDENUM

// Management of the Mocap Cutscene, before it's running.
ENUM eFMMC_MOCAP_PROGRESS
	MOCAPPROG_INIT = 0,
	MOCAPPROG_WAIT_PLAYER_SELECTION,
	MOCAPPROG_FADEOUT,
	MOCAPPROG_INTRO,
	MOCAPPROG_APARTMENT,
	MOCAPPROG_PROCESS_SHOTS,
	MOCAPPROG_WARPEND,
	MOCAPPROG_ENDING_PROCEDURES,
	MOCAPPROG_CLEANUP
ENDENUM

// Management of the Mocap Cutscene, before it's running.
ENUM eFMMC_MOCAP_RUNNING_PROGRESS
	MOCAPRUNNING_STAGE_0 = 0,
	MOCAPRUNNING_STAGE_1,
	MOCAPRUNNING_STAGE_2,
	MOCAPRUNNING_STAGE_3,
	MOCAPRUNNING_STAGE_4,
	MOCAPRUNNING_STAGE_5,
	MOCAPRUNNING_STAGE_6
ENDENUM


//Variables needed for the mission controller so we can debug via a BG script
CONST_INT ciLocalBGScriptBS_BailCurrentInteractable									0
CONST_INT ciLocalBGScriptBS_CompleteCurrentInteractable								1
CONST_INT ciLocalBGScriptBS_BlockInteractionWithCurrentNearestInteractable			2
CONST_INT ciLocalBGScriptBS_ForceAllowInteractionWithCurrentNearestInteractable		3
CONST_INT ciLocalBGScriptBS_DoNotDisablePlayerControl								4
CONST_INT ciLocalBGScriptBS_SkipIntimidationOpening									5
CONST_INT ciLocalBGScriptBS_ForceAllowNextIntimidationHit							6
CONST_INT ciLocalBGScriptBS_ResetFranklinIntimidationAnims							7
CONST_INT ciLocalBGScriptBS_ResetGolferIntimidationAnims							8
CONST_INT ciLocalBGScriptBS_IntimidationSkipWaitInPlace								9
CONST_INT ciLocalBGScriptBS_IntimidationExtraSafeStartChecks						10
CONST_INT ciLocalBGScriptBS_IntimidationTriggerApologyASAP							11
CONST_INT ciLocalBGScriptBS_IntimidationPassMinigameASAP							12
CONST_INT ciLocalBGScriptBS_SkipDoorInteriorChecks									13
CONST_INT ciLocalBGScriptBS_ScriptedAnimConv_CancelCurrentConversation				14
CONST_INT ciLocalBGScriptBS_BlockNewRoomKeyBackup									15
CONST_INT ciLocalBGScriptBS_BlockWarpPortalClearBitsFix								16

STRUCT MC_LocalVariables_VARS_STRUCT
	eFMMC_SCRIPTED_CUTSCENE_PROGRESS eScriptedCutsceneProgress
	eFMMC_MOCAP_RUNNING_PROGRESS eMocapRunningCutsceneProgress
	INT iScriptedCutsceneProgress
	INT iMocapCutsceneProgress
	INT iScriptedCutsceneTeam
	INT iScriptedCutscenePlaying
	INT iLocalBoolCheck
	INT iLocalBoolCheck2
	INT iLocalBoolCheck3
	INT iLocalBoolCheck4
	INT iLocalBoolCheck5
	INT iLocalBoolCheck6
	INT iLocalBoolCheck7
	INT iLocalBoolCheck8
	INT iLocalBoolCheck9
	INT iLocalBoolCheck10
	INT iLocalBoolCheck11
	INT iLocalBoolCheck12
	INT iLocalBoolCheck13
	INT iLocalBoolCheck14
	INT iLocalBoolCheck15
	INT iLocalBoolCheck16
	INT iLocalBoolCheck17
	INT iLocalBoolCheck18
	INT iLocalBoolCheck19
	INT iLocalBoolCheck20
	INT iLocalBoolCheck21
	INT iLocalBoolCheck22
	INT iLocalBoolCheck23
	INT iLocalBoolCheck24
	INT iLocalBoolCheck25
	INT iLocalBoolCheck26
	INT iLocalBoolCheck27
	INT iLocalBoolCheck28
	INT iLocalBoolCheck29
	INT iLocalBoolCheck30
	INT iLocalBoolCheck31
	INT iLocalBoolCheck32
	INT iLocalBoolCheck33
	INT iLocalBoolCheck34
	INT iLocalBoolCheck35
	INT iLocalBoolCheck36
	INT iLocalBoolResetCheck1
	INT iLocalAssignStagger
	
	INT iLocalInteractWithState
	INT iLocalBGScriptBS
	
	// Interactables
	INT iResetThisInteractableAsap = -1
	
	INT iLastDeliveredContinuityObjectThisRule = -1
	
	INT iStartPointNodeSearchOverride = 0
ENDSTRUCT 
STRUCT MC_PlayerBD_VARS_STRUCT
	INT iClientLogicStage 
	INT iGameState
	INT iCurrentLoc
	INT iInAnyLoc 
	INT iLeaveLoc
	INT iPedNear
	INT iVehNear
	INT iObjNear
	INT iObjHacking
	INT iPedCarryCount
	INT iVehCarryCount
	INT iObjCarryCount
	INT iNumPlayerDeaths
	INT iPlayerScore
	INT iClientBitSet
	INT iClientBitSet2
	INT iClientBitSet3
	INT iClientBitSet4
	INT iTinyRacersState
	INT iRestartState
	INT iPrerequisiteBS[ciPREREQ_Bitsets]
	INT iAbilityPrerequisite_AirStrike
	INT iAbilityPrerequisite_HeliBackup
	INT iAbilityPrerequisite_HeavyLoadout
	INT iAbilityPrerequisite_ReconDrone
	INT iAbilityPrerequisite_SupportSniper
	
	//For Copying Back from BG script
	INT iClientBitSet_ForceClear
	INT iClientBitSet2_ForceClear
	INT iClientBitSet3_ForceClear
	INT iClientBitSet4_ForceClear
	
	INT iClientBitSet_ForceSet
	INT iClientBitSet2_ForceSet
	INT iClientBitSet3_ForceSet
	INT iClientBitSet4_ForceSet
ENDSTRUCT
STRUCT MC_serverBD_VARS_STRUCT
	INT iServerGameState
	INT iServerBitSet
	INT iServerBitSet1
	INT iServerBitSet2
	INT iServerBitSet3
	INT iServerBitSet4
	INT iServerBitSet5
	INT iServerBitSet6
	INT iServerBitSet7
	INT iServerBitSet8
	INT iOptionsMenuBitSet
	INT iNumberOfTeams
	INT iNumActiveTeams
	INT iEndCutscene
	INT iCurrentHighestPriority[FMMC_MAX_TEAMS]
	INT iMaxObjectives[FMMC_MAX_TEAMS]
	INT iNumPlayerRuleHighestPriority[FMMC_MAX_TEAMS]
	INT iNumLocHighestPriority[FMMC_MAX_TEAMS]
	INT iNumPedHighestPriority[FMMC_MAX_TEAMS]
	INT iNumVehHighestPriority[FMMC_MAX_TEAMS]
	INT iNumObjHighestPriority[FMMC_MAX_TEAMS]
	INT iNumPedHighestPriorityHeld[FMMC_MAX_TEAMS]
	INT iNumVehHighestPriorityHeld[FMMC_MAX_TEAMS]
	INT iNumObjHighestPriorityHeld[FMMC_MAX_TEAMS]
	INT iPlayerRuleMissionLogic[FMMC_MAX_TEAMS]
	INT iLocMissionLogic[FMMC_MAX_TEAMS]
	INT iPedMissionLogic[FMMC_MAX_TEAMS]
	INT iVehMissionLogic[FMMC_MAX_TEAMS]
	INT iObjMissionLogic[FMMC_MAX_TEAMS]
	INT iObjMissionSubLogic[FMMC_MAX_TEAMS]
	INT iNumberOfPlayingPlayers[FMMC_MAX_TEAMS]
	INT iTeamScore[FMMC_MAX_TEAMS]
	INT iScoreOnThisRule[FMMC_MAX_TEAMS]
	INT iTeamDeaths[FMMC_MAX_TEAMS]
	INT iCutsceneID[FMMC_MAX_TEAMS]	
	OBJECT_INDEX oiObjects[32]//FMMC_MAX_NUM_OBJECTS
	PED_INDEX piPeds[80]//FMMC_MAX_PEDS 
	VEHICLE_INDEX viVehicles[32]//FMMC_MAX_VEHICLES
	INT iNumObjects
	INT iNumPeds
	INT iNumVehicles
	INT iServerEventKey
	
	//For Copying Back from BG script
	INT iServerBitSet_ForceClear
	INT iServerBitSet1_ForceClear
	INT iServerBitSet2_ForceClear
	INT iServerBitSet3_ForceClear
	INT iServerBitSet4_ForceClear
	INT iServerBitSet5_ForceClear
	INT iServerBitSet6_ForceClear
	INT iServerBitSet7_ForceClear
	INT iServerBitSet8_ForceClear
	
	INT iServerBitSet_ForceSet
	INT iServerBitSet1_ForceSet
	INT iServerBitSet2_ForceSet
	INT iServerBitSet3_ForceSet
	INT iServerBitSet4_ForceSet
	INT iServerBitSet5_ForceSet
	INT iServerBitSet6_ForceSet
	INT iServerBitSet7_ForceSet
	INT iServerBitSet8_ForceSet
	
ENDSTRUCT

//Variables needed for the mission controller so we can debug via a BG script
MC_LocalVariables_VARS_STRUCT gMC_LocalVariables_VARS
MC_PlayerBD_VARS_STRUCT gMC_PlayerBD_VARS
MC_serverBD_VARS_STRUCT gBG_MC_serverBD_VARS

//dont delete this, its used as part of a background script handshake
VECTOR g_vMCBGPINCode = <<10.61, 0.110, 210.2>>
VECTOR g_vMCBGPINEntry = <<0,0,0>>

INT g_iTRIP3_MCS1_PedTarget = 32
INT g_iTRIP3_MCS1_Timer = 5000
BOOL g_bTRIP3_MCS1_ForceSniper = TRUE

INT g_iFMMC_SkipPedScenario = -1

BOOL g_bFMMC_ForceCutsceneFlag = TRUE

BOOL g_bDMC_AllowLittleTeamBigTeam = FALSE

BOOL g_bSCL_RadioNativeSwapBack = FALSE

BOOL g_bFMMCBlockCustomAudioScript = FALSE

BOOL g_bBlock_PROCESS_DISABLE_REALTIME_MULTIPLAYER = FALSE


CONST_INT ci_GANGOPS_CARMOD_0_COL_1			0
CONST_INT ci_GANGOPS_CARMOD_0_COL_2			1
CONST_INT ci_GANGOPS_CARMOD_0_COL_EXTRA_1	2
CONST_INT ci_GANGOPS_CARMOD_0_COL_EXTRA_2	3
CONST_INT ci_GANGOPS_CARMOD_1_COL_1			4
CONST_INT ci_GANGOPS_CARMOD_1_COL_2			5
CONST_INT ci_GANGOPS_CARMOD_1_COL_EXTRA_1	6
CONST_INT ci_GANGOPS_CARMOD_1_COL_EXTRA_2	7
CONST_INT ci_GANGOPS_CARMOD_2_COL_1			8
CONST_INT ci_GANGOPS_CARMOD_2_COL_2			9
CONST_INT ci_GANGOPS_CARMOD_2_COL_EXTRA_1	10
CONST_INT ci_GANGOPS_CARMOD_2_COL_EXTRA_2	11
CONST_INT ci_GANGOPS_CARMOD_3_COL_1			12
CONST_INT ci_GANGOPS_CARMOD_3_COL_2			13
CONST_INT ci_GANGOPS_CARMOD_3_COL_EXTRA_1	14
CONST_INT ci_GANGOPS_CARMOD_3_COL_EXTRA_2	15
CONST_INT ci_GANGOPS_CARMOD_MAX				16

STRUCT GANG_OPS_CAR_MOD_COLOURS
	INT iColour[ci_GANGOPS_CARMOD_MAX]
ENDSTRUCT
GANG_OPS_CAR_MOD_COLOURS g_sGangOpsCarColous

ENDGLOBALS	//	GLOBALS_BLOCK_UGC_2










