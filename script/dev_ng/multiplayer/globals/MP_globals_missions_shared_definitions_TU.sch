USING "rage_builtins.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_missions_shared_Definitions_TU.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains constant, enum, and struct definitions for all MP Missions Shared TitleUpdate routines.
//							NB. Should not contain instantiated variables.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************



// ===========================================================================================================
//      General Missions Shared Cloud Data Consts
// ===========================================================================================================

// Maximum cloud-loaded header details for missions actively being shared
CONST_INT		MAX_MISSIONS_SHARED_CLOUD_DATA_TU			32		//NUM_NETWORK_PLAYERS - curently problems accessing NUM_NETWORK_PLAYERS from TitleUpdate globals block, so hardcoding
	



// ===========================================================================================================
//      Missions Shared Client Cloud Data
// ===========================================================================================================

// A client struct containing the local language version of the Mission Name
// NOTE: This is to ensure the local player gets the mission name in the local language rather than the distributed potentially foreign language
// KGM 11/3/14: Also now includes the local version of the Mission Description Hash (or 0 if not locally available)
//				- the descHash is a local 'pointer' into a batch of data in code so the shared version will never be correct
STRUCT g_structLocalLanguageDisplayText
	TEXT_LABEL_63				msclldtMissionName[MAX_MISSIONS_SHARED_CLOUD_DATA_TU]					// An client array of Mission Names in the local language to replace the Shared Server versions
	INT							msclldtDescHash[MAX_MISSIONS_SHARED_CLOUD_DATA_TU]						// A client array of local Description Hashes to replace teh Shared Server version
ENDSTRUCT



