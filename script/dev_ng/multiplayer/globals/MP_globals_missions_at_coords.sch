USING "rage_builtins.sch"
USING "mp_globals_missions_at_coords_definitions.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_missions_at_coords.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains global variable instances for all MP missions at coords routines.
//		NOTES			:	For efficiency there will be a multi-tiered update system based on the coords distance from the player.
//
//							There will be one Focus Mission that is treated as closest to the player - this mission will be the
//							one with an active vote and registered with the Mission Controller. It may also be the current active
//							mission. Until the player moves away from this mission and tidies up the active elements, no other
//							mission can take its place even if the player moves closer to another mission.
//
//							The remainder of the missions will be placed in the array in a very rough 'distance from player' order.
//							This will be based on distance bands. When a mission gets its maintenance update the distance from the
//							player will be calculated and, if the distance crosses a threshold, the mission will be moved up or down
//							the array to sit with other missions that are within the same distance band from the player.
//							EG: Array Position 0 will contain the Focus Mission, Array Positions 1 - (for eg) 5 could contain all the
//							missions currently within (say) 20m of the player and have the potential to become the focus mission.
//							Array Positions 6 - 13 could contain all missions within (say) 50m of the player, and array positions
//							14 and above could all be missions greater than 50m away. Within each band in the array there is no
//							sorting based on distance, only when a mission crosses into a different band will it's position change
//							within the array.
//
//							The nearest missions will be updated every frame or using a very tight update schedule - these missions
//							have the potential to become the Focus Mission. The remaining missions will be updated periodically to
//							minimise processing.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      TEMP DEVELOPMENT GLOBAL - USED AS I'M ADDING EACH PART TO THE GAME
//		As I've locally integrated each item I'll unwrap it and check it in for test then start wrapping
//			the next phase of development with this global instead.
// ===========================================================================================================

// THIS SHOULD BE CHECKED-IN AS FALSE, BUT I USE sc_cncmatc command line parameter to switch it locally to TRUE
BOOL	g_DEBUG_integratingCnCMissionsAtCoords		= FALSE

// Switching on the FM Mission Controller by default. Use sc_nofmcontroller to switch it off.
//BOOL	g_DEBUG_integratingFMMatC_OLD				= TRUE




// ===========================================================================================================
//      General Missions At Coords globals
// ===========================================================================================================

// These are the default distances the mission coords need to be from a player to trigger a range change
// NOTE: Initial assumption is that all missions will use the same ranges

TEXT_LABEL_15	MATC_CORONA_ACTIVE_TXD_NAME						// Will hold the corona Active TXD name
TEXT_LABEL_15	MATC_CORONA_ACTIVE_TEXTURE_NAME					// Will hold the corona Active Texture name

// Corona Projections
TEXT_LABEL_23	MATC_CORONA_PROJECTION_TXD_NAME					// Will hold the corona ground projections TXD name

// Mission Blips Currently on display - for Dave to send help text
BOOL		g_isAnyRSVerifiedRaceBlipOnDisplay		= FALSE
BOOL		g_isAnyRSVerifiedDMBlipOnDisplay		= FALSE




// ===========================================================================================================
//      Missions At Coords Initial Actions globals
//		Controls any actions that are required as a one-off when entering a new game
// ===========================================================================================================

// Controls all initial actions
// KGM 5/9/13: Also look at the additional TU struct which is used in conjunction with this
g_structMatcInitialActionsMP		g_sMatcInitialActions




// ===========================================================================================================
//      Missions At Coords Control globals
//		This is for any general control variables that don't belong to a specific mission, or band, or stage
// ===========================================================================================================

// A set of Missions At Coords Control Variables
g_structMatCControlsMP	g_sAtCoordsControlsMP




// ===========================================================================================================
//      Speed Zone Struct
//		Stores the details of speed zone restrictions in place
//		(NOTE: The array is not stored contiguously, so just find the first empty slot to add new data)
// ===========================================================================================================

g_structMatCSpeedZonesMP	g_sMatCSpeedZonesMP[MAX_MATC_SPEED_ZONES]




// ===========================================================================================================
//      Angled Area Struct
//		Stores the details of an Angled Area for mission triggering purposes
// ===========================================================================================================

g_structMatCAngledAreaMP	g_sMatCAngledAreasMP[MAX_MATC_ANGLED_AREAS]
INT							g_numMatcAngledAreas	= 0




// ===========================================================================================================
//      Flashing Blips Struct
//		Stores the details of blips that should flash in unison as activities unlock
// ===========================================================================================================

// STRUCT containing details of all blips to flash - Graeme moved this to title update global block
// g_structMatcAllFlashingBlipsMP		g_sMatcFlashingBlipsMP




// ===========================================================================================================
//      Focus Mission globals
//		Additional Variables required to control the one mission that is the current Focus Mission
// ===========================================================================================================

// MP Missions At Coords Focus Mission data - contains all additional control variables required when the mission is the Focus Mission
g_structMatCFocusMissionMP	g_sAtCoordsFocusMP


// -----------------------------------------------------------------------------------------------------------

// MP Missions At Coords Focus Mission InCorona Actions data - contains data used to commincate with an external script that is handling actions when the player is in a Corona
// EG: For Freemode, this is the menu display, the leaderboard display, the sending of Invites, etc. Initially also the voting, although this may change.
// Initial Intended Sequence (although I can foresee some flaws, so this will need tweaked):
//		MatC sets DoProcessing, sets Stage to INITIALISE, sets RegID
//		External Initialises and sets Stage to WORKING
//		External sets StartMission when player votes
//		MatC starts mission and sets DoProcessing to FALSE
//		External waits for STARTED feedback to be broadcast using RegID

// There is one instance of this - the player can only be in one corona at any one time - it'll get used when needed
// Graeme moved the proper version of this struct in to the title update global header
//g_structMatCInCoronaMP_OLD	g_sAtCoordsInCoronaMP_OLD




// ===========================================================================================================
//      Missions At Coords Banding globals
//		These are pointers into the Missions At Coords array to optimise the updating based on distance
//		One mission at each Band will be scheduled to update every frame, distant missions will update less often
// ===========================================================================================================

// MP Missions At Coords Bandings Data
g_structMatCBandingsMP	g_sAtCoordsBandsMP[MAX_MISSIONS_AT_COORDS_BANDINGS]




// ===========================================================================================================
//      Missions At Coords Stages globals
//		These are the stages a mission will go through usually based on distance from player
// ===========================================================================================================

// MP Missions At Coords Stages Data
//g_structMatCStagesMP	g_sAtCoordsStageInfo_Old[OLD_MAX_MISSIONS_AT_COORDS_STAGES]




// ===========================================================================================================
//      Missions At Coords missions array globals - contiguous
//		These are the details for all the missions being handled by the Misions At Coords array
// ===========================================================================================================

// MP Missions At Coords Data - contains all control data associated with one mission at a coord
//g_structMissionsAtCoordsMP_OLD	g_sAtCoordsMP_OLD[MAX_NUM_MP_MISSIONS_AT_COORDS]
INT							g_numAtCoordsMP		= 0


// JA: 19/7/13: Pre Corona Host screen state
//	Graeme moved to Title Update
//	INT g_iPreCoronaHostScreenState = CORONA_PRE_HOST_SCREEN_INIT




// ===========================================================================================================
//      Feedback Globals
//		Allows the Missions At Coords system to feedback to the script that registered the mission
// ===========================================================================================================

// The MissionsAtCoords Feedback struct
g_structMatCFeedbackMP	g_sAtCoordsFeedbackMP[MAX_NUM_MP_MATC_FEEDBACK_SLOTS]
INT						g_numAtCoordsFeedbackMP		= 0
