//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name: MP_globals_freemode_delivery_dropoffs.sch															//
// Description: Enum for all the freemode delivery dropoffs													//
// Written by:  Tymon																						//
// Date:  		18/01/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "types.sch"
USING "mp_globals_new_features_tu.sch"

CONST_FLOAT _FM_DELIVERY_DROPOFF_LAUNCH_DISTANCE 200.0

ENUM FREEMODE_DELIVERY_DROPOFFS
	FREEMODE_DELIVERY_DROPOFF_INVALID = 0,
	
	GUNRUNNING_DROPOFF_BUNKER_1, // 0
	GUNRUNNING_DROPOFF_BUNKER_2,
	GUNRUNNING_DROPOFF_BUNKER_3,
	GUNRUNNING_DROPOFF_BUNKER_4,
	GUNRUNNING_DROPOFF_BUNKER_5,
	GUNRUNNING_DROPOFF_BUNKER_6,
	GUNRUNNING_DROPOFF_BUNKER_7,
//	GUNRUNNING_DROPOFF_BUNKER_8,
	GUNRUNNING_DROPOFF_BUNKER_9,
	GUNRUNNING_DROPOFF_BUNKER_10, 
	GUNRUNNING_DROPOFF_BUNKER_11, 
	GUNRUNNING_DROPOFF_BUNKER_12, // 10
	
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_1, // 11
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_2,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_3,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_4,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_5,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_6,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_7,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_8,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_9,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_10,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_11,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_12,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_13,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_14,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_15,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_16,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_17,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_18,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_19,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_20,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_21,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_22,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_23,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_24,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_25,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_26,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_27,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_28,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_29,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_COUNTRY_30, // 40
	
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_1, // 41
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_2,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_3,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_4,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_5,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_6,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_7,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_8,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_9,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_10,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_11,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_12,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_13,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_14,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_15,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_16,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_17,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_18,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_19,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_20,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_21,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_22,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_23,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_24,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_25,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_26,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_27,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_28,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_29,
	GUNRUNNING_DROPOFF_MOVE_WEAPONS_CITY_30, // 70
	
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_1, // 71
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_2,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_3,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_4,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_5,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_6,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_7,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_8,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_9,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_COUNTRY_10,//80
	
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_1, // 81
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_2,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_3,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_4,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_5,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_6,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_7,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_8,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_9,
	GUNRUNNING_DROPOFF_FOLLOW_LEADER_CITY_10, //90
	
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_1, // 91
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_2,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_3,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_4,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_5,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_6,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_7,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_8,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_9,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_10,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_11, // 101
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_12,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_13,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_14,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_15,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_16,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_17,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_18,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_19,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_20,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_21, // 111
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_22,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_23,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_24,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_25,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_26,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_27,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_28,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_29,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_30,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_31, //121
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_32,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_33,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_34,
	GUNRUNNING_DROPOFF_AMBUSHED_COUNTRY_35, //125
	
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_1, // 126
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_2,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_3,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_4,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_5,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_6,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_7,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_8,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_9,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_10,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_11, // 136
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_12,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_13,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_14,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_15,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_16,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_17,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_18,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_19,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_20,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_21, // 146
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_22,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_23,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_24,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_25,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_26,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_27,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_28,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_29,
	GUNRUNNING_DROPOFF_AMBUSHED_CITY_30, //155
	
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_1, // 156
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_2,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_3,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_4,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_5,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_6,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_7,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_8,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_9,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_10,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_11, // 166
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_12,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_13,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_14,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_15,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_16,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_17,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_18,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_19,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_20,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_21, // 176
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_22,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_23,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_24,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_25,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_26,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_27,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_28,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_29,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_30, 
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_31, // 186
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_32,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_33,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_34,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_35,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_36,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_37,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_38,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_39,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_40,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_41, // 196
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_42,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_43,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_44,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_45,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_46,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_47,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_48,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_49,
	GUNRUNNING_DROPOFF_HILL_CLIMB_COUNTRY_50, //205
	
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_1, // 206
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_2,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_3,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_4,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_5,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_6,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_7,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_8,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_9,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_10,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_11, // 216
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_12,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_13,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_14,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_15,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_16,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_17,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_18,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_19,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_20,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_21, // 226
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_22,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_23,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_24,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_25,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_26,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_27,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_28,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_29,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_30, 
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_31, // 236
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_32,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_33,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_34,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_35,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_36,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_37,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_38,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_39,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_40,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_41, // 246
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_42,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_43,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_44,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_45,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_46,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_47,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_48,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_49,
	GUNRUNNING_DROPOFF_HILL_CLIMB_CITY_50, //255
	
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_1, // 256
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_2,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_3,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_4,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_5,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_6,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_7,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_8,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_9,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_10,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_11, // 266
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_12,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_13,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_14,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_15,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_16,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_17,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_18,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_19,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_20,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_21, // 276
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_22,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_23,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_24,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_25,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_26,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_27,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_28,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_29,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_30, 
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_31, // 286
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_32,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_33,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_34,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_35,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_36,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_37,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_38,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_39,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_40,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_41, // 296
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_42,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_43,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_44,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_45,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_46,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_47,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_48,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_49,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_COUNTRY_50, //305
	
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_1, // 306
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_2,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_3,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_4,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_5,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_6,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_7,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_8,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_9,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_10,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_11, // 316
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_12,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_13,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_14,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_15,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_16,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_17,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_18,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_19,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_20,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_21, // 326
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_22,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_23,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_24,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_25,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_26,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_27,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_28,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_29,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_30, 
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_31, // 336
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_32,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_33,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_34,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_35,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_36,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_37,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_38,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_39,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_40,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_41, // 346
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_42,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_43,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_44,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_45,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_46,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_47,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_48,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_49,
	GUNRUNNING_DROPOFF_ROUGH_TERRAIN_CITY_50, //355
	
	GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_1, // 356
	GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_2,
	GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_3,
	GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_4,
	GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_5,
	GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_6,
	GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_7,
	GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_8,
	GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_9,
	GUNRUNNING_DROPOFF_PHANTOM_COUNTRY_10, //365
	
	GUNRUNNING_DROPOFF_PHANTOM_CITY_1, // 366
	GUNRUNNING_DROPOFF_PHANTOM_CITY_2,
	GUNRUNNING_DROPOFF_PHANTOM_CITY_3,
	GUNRUNNING_DROPOFF_PHANTOM_CITY_4,
	GUNRUNNING_DROPOFF_PHANTOM_CITY_5,
	GUNRUNNING_DROPOFF_PHANTOM_CITY_6,
	GUNRUNNING_DROPOFF_PHANTOM_CITY_7,
	GUNRUNNING_DROPOFF_PHANTOM_CITY_8,
	GUNRUNNING_DROPOFF_PHANTOM_CITY_9,
	GUNRUNNING_DROPOFF_PHANTOM_CITY_10, //375
	
	SMUGGLER_DROPOFF_LSIA_HANGAR_1,			// LSIA Hangar 1		
	SMUGGLER_DROPOFF_LSIA_HANGAR_A17,		// LSIA Hangar A17
	SMUGGLER_DROPOFF_ZANCUDO_HANGAR_A2,		// Fort Zancudo Hangar A2
	SMUGGLER_DROPOFF_ZANCUDO_HANGAR_3497,	// Fort Zancudo Hangar 3497
	SMUGGLER_DROPOFF_ZANCUDO_HANGAR_3499,	// Fort Zancudo Hangar 3499
	
	SMUGGLER_DROPOFF_HEAVY_LIFTING_1, //381
	SMUGGLER_DROPOFF_HEAVY_LIFTING_2,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_3,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_4,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_5,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_6,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_7,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_8,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_9,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_10,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_11,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_12,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_13,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_14,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_15,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_16,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_17,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_18,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_19,
	SMUGGLER_DROPOFF_HEAVY_LIFTING_20, //401
	
	SMUGGLER_DROPOFF_CONTESTED_1, //402
	SMUGGLER_DROPOFF_CONTESTED_2,
	SMUGGLER_DROPOFF_CONTESTED_3,
	SMUGGLER_DROPOFF_CONTESTED_4,
	SMUGGLER_DROPOFF_CONTESTED_5,
	SMUGGLER_DROPOFF_CONTESTED_6,
	SMUGGLER_DROPOFF_CONTESTED_7,
	SMUGGLER_DROPOFF_CONTESTED_8,
	SMUGGLER_DROPOFF_CONTESTED_9,
	SMUGGLER_DROPOFF_CONTESTED_10,
	SMUGGLER_DROPOFF_CONTESTED_11,
	SMUGGLER_DROPOFF_CONTESTED_12,
	SMUGGLER_DROPOFF_CONTESTED_13,
	SMUGGLER_DROPOFF_CONTESTED_14,
	SMUGGLER_DROPOFF_CONTESTED_15,
	SMUGGLER_DROPOFF_CONTESTED_16,
	SMUGGLER_DROPOFF_CONTESTED_17,
	SMUGGLER_DROPOFF_CONTESTED_18,
	SMUGGLER_DROPOFF_CONTESTED_19,
	SMUGGLER_DROPOFF_CONTESTED_20,
	SMUGGLER_DROPOFF_CONTESTED_21,
	SMUGGLER_DROPOFF_CONTESTED_22,
	SMUGGLER_DROPOFF_CONTESTED_23,
	SMUGGLER_DROPOFF_CONTESTED_24,
	SMUGGLER_DROPOFF_CONTESTED_25,
	SMUGGLER_DROPOFF_CONTESTED_26,
	SMUGGLER_DROPOFF_CONTESTED_27,
	SMUGGLER_DROPOFF_CONTESTED_28,
	SMUGGLER_DROPOFF_CONTESTED_29,
	SMUGGLER_DROPOFF_CONTESTED_30, //431
	
	SMUGGLER_DROPOFF_AGILE_DELIVERY_1, //432
	SMUGGLER_DROPOFF_AGILE_DELIVERY_2,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_3,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_4,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_5,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_6,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_7,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_8,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_9,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_10, // 441
	
	SMUGGLER_DROPOFF_AGILE_DELIVERY_11,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_12,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_13,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_14,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_15,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_16,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_17,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_18,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_19,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_20, //451
	
	SMUGGLER_DROPOFF_AGILE_DELIVERY_21,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_22,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_23,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_24,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_25,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_26,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_27,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_28,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_29,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_30, // 461 
	
	SMUGGLER_DROPOFF_AGILE_DELIVERY_31,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_32,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_33,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_34,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_35,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_36,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_37,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_38,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_39,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_40, //471
	
	SMUGGLER_DROPOFF_AGILE_DELIVERY_41,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_42,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_43,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_44,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_45,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_46,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_47,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_48,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_49,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_50, //481
	
	SMUGGLER_DROPOFF_AGILE_DELIVERY_51,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_52,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_53,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_54,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_55,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_56,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_57,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_58,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_59,
	SMUGGLER_DROPOFF_AGILE_DELIVERY_60, //491
	
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_1, //492
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_2,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_3,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_4,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_5,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_6,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_7,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_8,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_9,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_10,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_11,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_12,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_13,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_14,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_15,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_16,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_17,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_18,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_19,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_20,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_21,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_22,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_23,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_24,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_25,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_26,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_27,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_28,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_29,
	SMUGGLER_DROPOFF_PRECISION_DELIVERY_30, //521
	
	SMUGGLER_DROPOFF_FLYING_FORTRESS_1, //522
	SMUGGLER_DROPOFF_FLYING_FORTRESS_2,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_3,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_4,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_5,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_6,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_7,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_8,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_9,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_10, // 531
	
	SMUGGLER_DROPOFF_FLYING_FORTRESS_11, //532
	SMUGGLER_DROPOFF_FLYING_FORTRESS_12,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_13,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_14,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_15,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_16,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_17,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_18,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_19,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_20, // 541
	
	SMUGGLER_DROPOFF_FLYING_FORTRESS_21, //542
	SMUGGLER_DROPOFF_FLYING_FORTRESS_22,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_23,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_24,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_25,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_26,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_27,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_28,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_29,
	SMUGGLER_DROPOFF_FLYING_FORTRESS_30, // 551
	
	SMUGGLER_DROPOFF_FLY_LOW_1, //552
	SMUGGLER_DROPOFF_FLY_LOW_2,
	SMUGGLER_DROPOFF_FLY_LOW_3,
	SMUGGLER_DROPOFF_FLY_LOW_4,
	SMUGGLER_DROPOFF_FLY_LOW_5,
	SMUGGLER_DROPOFF_FLY_LOW_6,
	SMUGGLER_DROPOFF_FLY_LOW_7,
	SMUGGLER_DROPOFF_FLY_LOW_8,
	SMUGGLER_DROPOFF_FLY_LOW_9,
	SMUGGLER_DROPOFF_FLY_LOW_10,
	SMUGGLER_DROPOFF_FLY_LOW_11,
	SMUGGLER_DROPOFF_FLY_LOW_12,
	SMUGGLER_DROPOFF_FLY_LOW_13,
	SMUGGLER_DROPOFF_FLY_LOW_14,
	SMUGGLER_DROPOFF_FLY_LOW_15,
	SMUGGLER_DROPOFF_FLY_LOW_16,
	SMUGGLER_DROPOFF_FLY_LOW_17,
	SMUGGLER_DROPOFF_FLY_LOW_18,
	SMUGGLER_DROPOFF_FLY_LOW_19,
	SMUGGLER_DROPOFF_FLY_LOW_20,
	SMUGGLER_DROPOFF_FLY_LOW_21,
	SMUGGLER_DROPOFF_FLY_LOW_22,
	SMUGGLER_DROPOFF_FLY_LOW_23,
	SMUGGLER_DROPOFF_FLY_LOW_24,
	SMUGGLER_DROPOFF_FLY_LOW_25,
	SMUGGLER_DROPOFF_FLY_LOW_26,
	SMUGGLER_DROPOFF_FLY_LOW_27,
	SMUGGLER_DROPOFF_FLY_LOW_28,
	SMUGGLER_DROPOFF_FLY_LOW_29,
	SMUGGLER_DROPOFF_FLY_LOW_30,
	SMUGGLER_DROPOFF_FLY_LOW_31,
	SMUGGLER_DROPOFF_FLY_LOW_32,
	SMUGGLER_DROPOFF_FLY_LOW_33, //584
	
	SMUGGLER_DROPOFF_AIR_DELIVERY_1, //585
	SMUGGLER_DROPOFF_AIR_DELIVERY_2,
	SMUGGLER_DROPOFF_AIR_DELIVERY_3,
	SMUGGLER_DROPOFF_AIR_DELIVERY_4,
	SMUGGLER_DROPOFF_AIR_DELIVERY_5,
	SMUGGLER_DROPOFF_AIR_DELIVERY_6,
	SMUGGLER_DROPOFF_AIR_DELIVERY_7,
	SMUGGLER_DROPOFF_AIR_DELIVERY_8,
	SMUGGLER_DROPOFF_AIR_DELIVERY_9,
	SMUGGLER_DROPOFF_AIR_DELIVERY_10,
	SMUGGLER_DROPOFF_AIR_DELIVERY_11,
	SMUGGLER_DROPOFF_AIR_DELIVERY_12,
	SMUGGLER_DROPOFF_AIR_DELIVERY_13,
	SMUGGLER_DROPOFF_AIR_DELIVERY_14,
	SMUGGLER_DROPOFF_AIR_DELIVERY_15,
	SMUGGLER_DROPOFF_AIR_DELIVERY_16,
	SMUGGLER_DROPOFF_AIR_DELIVERY_17,
	SMUGGLER_DROPOFF_AIR_DELIVERY_18,
	SMUGGLER_DROPOFF_AIR_DELIVERY_19,
	SMUGGLER_DROPOFF_AIR_DELIVERY_20,
	SMUGGLER_DROPOFF_AIR_DELIVERY_21,
	SMUGGLER_DROPOFF_AIR_DELIVERY_22,
	SMUGGLER_DROPOFF_AIR_DELIVERY_23,
	SMUGGLER_DROPOFF_AIR_DELIVERY_24,
	SMUGGLER_DROPOFF_AIR_DELIVERY_25,
	SMUGGLER_DROPOFF_AIR_DELIVERY_26,
	SMUGGLER_DROPOFF_AIR_DELIVERY_27,
	SMUGGLER_DROPOFF_AIR_DELIVERY_28,
	SMUGGLER_DROPOFF_AIR_DELIVERY_29,
	SMUGGLER_DROPOFF_AIR_DELIVERY_30, //614
	
	SMUGGLER_DROPOFF_AIR_POLICE_1, //615
	SMUGGLER_DROPOFF_AIR_POLICE_2,
	SMUGGLER_DROPOFF_AIR_POLICE_3,
	SMUGGLER_DROPOFF_AIR_POLICE_4,
	SMUGGLER_DROPOFF_AIR_POLICE_5,
	SMUGGLER_DROPOFF_AIR_POLICE_6,
	SMUGGLER_DROPOFF_AIR_POLICE_7,
	SMUGGLER_DROPOFF_AIR_POLICE_8,
	SMUGGLER_DROPOFF_AIR_POLICE_9,
	SMUGGLER_DROPOFF_AIR_POLICE_10,
	SMUGGLER_DROPOFF_AIR_POLICE_11,
	SMUGGLER_DROPOFF_AIR_POLICE_12,
	SMUGGLER_DROPOFF_AIR_POLICE_13,
	SMUGGLER_DROPOFF_AIR_POLICE_14,
	SMUGGLER_DROPOFF_AIR_POLICE_15,
	SMUGGLER_DROPOFF_AIR_POLICE_16,
	SMUGGLER_DROPOFF_AIR_POLICE_17,
	SMUGGLER_DROPOFF_AIR_POLICE_18,
	SMUGGLER_DROPOFF_AIR_POLICE_19,
	SMUGGLER_DROPOFF_AIR_POLICE_20,
	SMUGGLER_DROPOFF_AIR_POLICE_21,
	SMUGGLER_DROPOFF_AIR_POLICE_22,
	SMUGGLER_DROPOFF_AIR_POLICE_23,
	SMUGGLER_DROPOFF_AIR_POLICE_24,
	SMUGGLER_DROPOFF_AIR_POLICE_25,
	SMUGGLER_DROPOFF_AIR_POLICE_26,
	SMUGGLER_DROPOFF_AIR_POLICE_27,
	SMUGGLER_DROPOFF_AIR_POLICE_28,
	SMUGGLER_DROPOFF_AIR_POLICE_29,
	SMUGGLER_DROPOFF_AIR_POLICE_30, //644
	
	SMUGGLER_DROPOFF_UNDER_RADAR_1, //645
	SMUGGLER_DROPOFF_UNDER_RADAR_2,
	SMUGGLER_DROPOFF_UNDER_RADAR_3,
	SMUGGLER_DROPOFF_UNDER_RADAR_4,
	SMUGGLER_DROPOFF_UNDER_RADAR_5,
	SMUGGLER_DROPOFF_UNDER_RADAR_6,
	SMUGGLER_DROPOFF_UNDER_RADAR_7,
	SMUGGLER_DROPOFF_UNDER_RADAR_8,
	SMUGGLER_DROPOFF_UNDER_RADAR_9,
	SMUGGLER_DROPOFF_UNDER_RADAR_10,
	SMUGGLER_DROPOFF_UNDER_RADAR_11,
	SMUGGLER_DROPOFF_UNDER_RADAR_12,
	SMUGGLER_DROPOFF_UNDER_RADAR_13,
	SMUGGLER_DROPOFF_UNDER_RADAR_14,
	SMUGGLER_DROPOFF_UNDER_RADAR_15,
	SMUGGLER_DROPOFF_UNDER_RADAR_16,
	SMUGGLER_DROPOFF_UNDER_RADAR_17,
	SMUGGLER_DROPOFF_UNDER_RADAR_18,
	SMUGGLER_DROPOFF_UNDER_RADAR_19,
	SMUGGLER_DROPOFF_UNDER_RADAR_20,
	SMUGGLER_DROPOFF_UNDER_RADAR_21,
	SMUGGLER_DROPOFF_UNDER_RADAR_22,
	SMUGGLER_DROPOFF_UNDER_RADAR_23,
	SMUGGLER_DROPOFF_UNDER_RADAR_24,
	SMUGGLER_DROPOFF_UNDER_RADAR_25,
	SMUGGLER_DROPOFF_UNDER_RADAR_26,
	SMUGGLER_DROPOFF_UNDER_RADAR_27,
	SMUGGLER_DROPOFF_UNDER_RADAR_28,
	SMUGGLER_DROPOFF_UNDER_RADAR_29,
	SMUGGLER_DROPOFF_UNDER_RADAR_30, //674
	
	FM_GANGOPS_DROPOFF_1, //675
	FM_GANGOPS_DROPOFF_2,
	FM_GANGOPS_DROPOFF_3,
	FM_GANGOPS_DROPOFF_4,
	//FM_GANGOPS_DROPOFF_5,
	FM_GANGOPS_DROPOFF_6,
	FM_GANGOPS_DROPOFF_7,
	FM_GANGOPS_DROPOFF_8,
	FM_GANGOPS_DROPOFF_9,
	FM_GANGOPS_DROPOFF_10, //683	
	
	//Player owned properties
	BB_DROPOFF_NIGHTCLUB_LA_MESA,
	BB_DROPOFF_NIGHTCLUB_MISSION_ROW,
	BB_DROPOFF_NIGHTCLUB_STRAWBERRY_WAREHOUSE,
	BB_DROPOFF_NIGHTCLUB_WEST_VINEWOOD,
	BB_DROPOFF_NIGHTCLUB_CYPRESS_FLATS,
	BB_DROPOFF_NIGHTCLUB_LSIA_WAREHOUSE,
	BB_DROPOFF_NIGHTCLUB_ELYSIAN_ISLAND,					//690
	BB_DROPOFF_NIGHTCLUB_DOWNTOWN_VINEWOOD,
	BB_DROPOFF_NIGHTCLUB_DEL_PERRO_BUILDING,
	BB_DROPOFF_NIGHTCLUB_VESPUCCI_CANALS,
	
	//Secondary drop locations - Used when the local player does not own the hub
	BB_DROPOFF_SEC_1,
	BB_DROPOFF_SEC_2,
	BB_DROPOFF_SEC_3,
	BB_DROPOFF_SEC_4,
	BB_DROPOFF_SEC_5,
	BB_DROPOFF_SEC_6,
	BB_DROPOFF_SEC_7,										//700
	BB_DROPOFF_SEC_8,
	BB_DROPOFF_SEC_9,
	BB_DROPOFF_SEC_10,	
	
	DROPOFF_CASINO,
	DROPOFF_CASINO_2,
	DROPOFF_CASINO_BODY_DISPOSAL_1,
	DROPOFF_CASINO_BODY_DISPOSAL_2,
	DROPOFF_CASINO_BODY_DISPOSAL_3,
	DROPOFF_CASINO_BODY_DISPOSAL_4,
	DROPOFF_CASINO_CAR_PARK,								//710
	DROPOFF_CASINO_HELIPAD,
	DROPOFF_CASINO_TUNNEL,
	DROPOFF_CASINO_GARAGE_DOOR,
	
	//Arcades
	CSH_DROPOFF_ARCADE_PALETO_BAY,
	CSH_DROPOFF_ARCADE_GRAPESEED,
	CSH_DROPOFF_ARCADE_DAVIS,
	CSH_DROPOFF_ARCADE_WEST_VINEWOOD,
	CSH_DROPOFF_ARCADE_ROCKFORD_HILLS,
	CSH_DROPOFF_ARCADE_LA_MESA,
	
	//Arcades - Secondary location
	CSH_DROPOFF_ARCADE_PALETO_BAY_SECONDARY,				//720
	CSH_DROPOFF_ARCADE_GRAPESEED_SECONDARY,
	CSH_DROPOFF_ARCADE_DAVIS_SECONDARY,
	CSH_DROPOFF_ARCADE_WEST_VINEWOOD_SECONDARY,
	CSH_DROPOFF_ARCADE_ROCKFORD_HILLS_SECONDARY,
	CSH_DROPOFF_ARCADE_LA_MESA_SECONDARY,
	
	//Arcades - Tertiary location
	CSH_DROPOFF_ARCADE_PALETO_BAY_TERTIARY,
	CSH_DROPOFF_ARCADE_GRAPESEED_TERTIARY,
	CSH_DROPOFF_ARCADE_DAVIS_TERTIARY,
	CSH_DROPOFF_ARCADE_WEST_VINEWOOD_TERTIARY,
	CSH_DROPOFF_ARCADE_ROCKFORD_HILLS_TERTIARY,				//730
	CSH_DROPOFF_ARCADE_LA_MESA_TERTIARY,
	
	//Other casino heist misc locations
	CSH_DROPOFF_SUBSTATION,
	CSH_DROPOFF_SEWERS,
	CSH_DROPOFF_SANDY,
	CSH_DROPOFF_ROCKFORD_PLAZA,
	CSH_DROPOFF_ALTRUIST_CAMP,
	
	//Patrick McReary
	CSH_PATRICK_MCREARY_1,
	CSH_PATRICK_MCREARY_2,
	CSH_PATRICK_MCREARY_3,
	CSH_PATRICK_MCREARY_4,									//740
	CSH_PATRICK_MCREARY_5,
	
	//Bugstars pt2
	CSH_DROPOFF_BUGSTAR_1,
	CSH_DROPOFF_BUGSTAR_2,
	CSH_DROPOFF_BUGSTAR_3,
	CSH_DROPOFF_BUGSTAR_4,
	CSH_DROPOFF_BUGSTAR_5,
	
	FMC_DROPOFF_OMEGA,
	FMC_DROPOFF_PALETO_SHERRIFF,
	FMC_DROPOFF_VESPUCCI_POLICE,
	FMC_DROPOFF_SANDY_SHORES_SHERRIFF,						//750
	FMC_DROPOFF_MISSION_ROW_POLICE,
	FMC_DROPOFF_GREAT_CHAPARRL,
	FMC_DROPOFF_LA_MESA,
	FMC_DROPOFF_STRAWBERRY,
	
	FMC_DROPOFF_DJ_LSIA,
	FMC_DROPOFF_DJ_MUSIC_LOCKER,	
	FMC_DROPOFF_SUBMARINE,
	FMC_DROPOFF_ELYSIAN_ISLAND,
	FMC_DROPOFF_PALETO_COVE,
	FMC_SANDY_SHORES_AIRFIELD,
	FMC_DROPOFF_LSIA_HANGAR_1,
	FMC_DROPOFF_LSIA_HANGAR_2,
	FMC_DROPOFF_SMUGGLER_SCOPING,
	FMC_DROPOFF_LSIA_SEA,

	FMC_DROPOFF_PLYR_AUTO_SHOP_LA_MESA,
	FMC_DROPOFF_PLYR_AUTO_SHOP_STRAWBERRY,
	FMC_DROPOFF_PLYR_AUTO_SHOP_BURTON,
	FMC_DROPOFF_PLYR_AUTO_SHOP_RANCHO,
	FMC_DROPOFF_PLYR_AUTO_SHOP_MISSION_ROW,
	FMC_DROPOFF_DOCKS,
	FMC_DROPOFF_BOLINGBROKE_PENITENTIARY,
	FMC_DROPOFF_BOLINGBROKE_PENITENTIARY_2,
	
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H1,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H2,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H3,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H4,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H5,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H6,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H7,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_H8,
	
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L1,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L2,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L3,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L4,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L5,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L6,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L7,
	FMC_DROPOFF_AUTO_SHOP_CUSTOMER_L8,
	
	FMC_DROPOFF_UNION_DEPOSITORY_1,
	FMC_DROPOFF_UNION_DEPOSITORY_2,
	FMC_DROPOFF_UNION_DEPOSITORY_3,
	FMC_DROPOFF_WHOUSE_DEFENCES_ELYSIAN,
	FMC_DROPOFF_METH_TANKER_HARMONY,
	FMC_DROPOFF_INSIDE_MAN_MISS_ROW,
	
	FMC_DROPOFF_FIXER_OFFICE_HAWICK,
	FMC_DROPOFF_FIXER_OFFICE_HAWICK_GARAGE,
	FMC_DROPOFF_FIXER_OFFICE_ROCKFORD,
	FMC_DROPOFF_FIXER_OFFICE_ROCKFORD_GARAGE,
	FMC_DROPOFF_FIXER_OFFICE_LITTLE_SEOUL,
	FMC_DROPOFF_FIXER_OFFICE_LITTLE_SEOUL_GARAGE,
	FMC_DROPOFF_FIXER_OFFICE_VESPUCCI,
	FMC_DROPOFF_FIXER_OFFICE_VESPUCCI_GARAGE,
	FMC_DROPOFF_FRANKLINS_HOUSE,
	FMC_DROPOFF_MORNINGWOOD_LOCKUP,
	
	FMC_DROPOFF_AMMU_NATION_CYPRESS,
	FMC_DROPOFF_AMMU_NATION_PALETO_BAY,
	FMC_DROPOFF_AMMU_NATION_SANDY_SHORES,
	FMC_DROPOFF_AMMU_NATION_ROUTE_68,
	FMC_DROPOFF_AMMU_NATION_CHUMASH,
	FMC_DROPOFF_AMMU_NATION_TATAVIUM,
	FMC_DROPOFF_AMMU_NATION_HAWICK,
	FMC_DROPOFF_AMMU_NATION_MORNINGWOOD,
	FMC_DROPOFF_AMMU_NATION_STRAWBERRY,
	FMC_DROPOFF_AMMU_NATION_LA_MESA,
	
	FMC_DROPOFF_LS_MEDICAL_CENTER,
	FMC_DROPOFF_ECLIPSE_MEDICAL_TOWER,
	FMC_DROPOFF_ZONAH_MEDICAL_CENTER,
	FMC_DROPOFF_PILLBOX_MEDICAL_CENTER,
	FMC_DROPOFF_SANDY_SH_MEDICAL_CENTER,
	FMC_DROPOFF_ST_FIACRE_HOSPITAL,
	FMC_DROPOFF_PARSONS_REHAB_CENTER,
	FMC_DROPOFF_VINEWOOD_VIP_HOUSE,
	FMC_DROPOFF_BANHAM_CANYON_VIP_HOUSE,
	FMC_DROPOFF_MIRROR_PARK_VIP_HOUSE,
	FMC_DROPOFF_LAKE_VINEWOOD_VIP_HOUSE,
	FMC_DROPOFF_RICHMAN_VIP_HOUSE,
	
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_1,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_2,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_3,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_4,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_5,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_6,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_7,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_8,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_9,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_10,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_11,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_12,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_13,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_14,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_15,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_16,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_17,
	FMC_DROPOFF_BIKE_SHOP_CUSTOMER_18,
	
	FMC_DROPOFF_CARGO_WH_ELYSIAN_ISLAND_2,
	FMC_DROPOFF_CARGO_WH_LA_PUERTA,
	FMC_DROPOFF_CARGO_WH_LA_MESA,
	FMC_DROPOFF_CARGO_WH_RANCHO,
	FMC_DROPOFF_CARGO_WH_W_VINEWOOD,
	FMC_DROPOFF_CARGO_WH_LSIA,
	FMC_DROPOFF_CARGO_WH_DEL_PERRO,
	FMC_DROPOFF_CARGO_WH_LSIA_2,
	FMC_DROPOFF_CARGO_WH_ELYSIAN_ISLAND,
	FMC_DROPOFF_CARGO_WH_EL_BURRO_HEIGHTS,
	FMC_DROPOFF_CARGO_WH_ELYSIAN_ISLAND_3,
	FMC_DROPOFF_CARGO_WH_LA_MESA_2,
	FMC_DROPOFF_CARGO_WH_MAZE_BANK_ARENA,
	FMC_DROPOFF_CARGO_WH_STRAWBERRY,
	FMC_DROPOFF_CARGO_WH_DOWNROWN_VW,
	FMC_DROPOFF_CARGO_WH_LA_MESA_3,
	FMC_DROPOFF_CARGO_WH_LA_MESA_4,
	FMC_DROPOFF_CARGO_WH_CYPRESS_FLATS_2,
	FMC_DROPOFF_CARGO_WH_CYPRESS_FLATS_3,
	FMC_DROPOFF_CARGO_WH_VINEWOOD,
	FMC_DROPOFF_CARGO_WH_RANCHO_2,
	FMC_DROPOFF_CARGO_WH_BANNING,
	
	FMC_DROPOFF_CLUBHOUSE_1,
	FMC_DROPOFF_CLUBHOUSE_2,
	FMC_DROPOFF_CLUBHOUSE_3,
	FMC_DROPOFF_CLUBHOUSE_4,
	FMC_DROPOFF_CLUBHOUSE_5,
	FMC_DROPOFF_CLUBHOUSE_6,
	FMC_DROPOFF_CLUBHOUSE_7,
	FMC_DROPOFF_CLUBHOUSE_8,
	FMC_DROPOFF_CLUBHOUSE_9,
	FMC_DROPOFF_CLUBHOUSE_10,
	FMC_DROPOFF_CLUBHOUSE_11,
	FMC_DROPOFF_CLUBHOUSE_12,
	
	FMC_DROPOFF_UNDERWATER_CARGO_1,
	FMC_DROPOFF_UNDERWATER_CARGO_2,
	FMC_DROPOFF_UNDERWATER_CARGO_3,
	
	#IF FEATURE_DLC_2_2022
	FMC_DROPOFF_GANG_CONVOY_GARAGE_CYPRESS,
	FMC_DROPOFF_GANG_CONVOY_GARAGE_SAN_CHIANSKI,
	
	// PALETO BAY
	FMC_DROPOFF_FACTORY_ID_METH_1,
	FMC_DROPOFF_FACTORY_ID_WEED_1,
	FMC_DROPOFF_FACTORY_ID_CRACK_1,
	FMC_DROPOFF_FACTORY_ID_FAKE_CASH_1,
	FMC_DROPOFF_FACTORY_ID_FAKE_ID_1,
	
	// CITY
	FMC_DROPOFF_FACTORY_ID_METH_2,
	FMC_DROPOFF_FACTORY_ID_WEED_2,
	FMC_DROPOFF_FACTORY_ID_CRACK_2,
	FMC_DROPOFF_FACTORY_ID_FAKE_CASH_2,
	FMC_DROPOFF_FACTORY_ID_FAKE_ID_2,
	
	// COUNTRYSIDE
	FMC_DROPOFF_FACTORY_ID_METH_3,
	FMC_DROPOFF_FACTORY_ID_WEED_3,
	FMC_DROPOFF_FACTORY_ID_CRACK_3,
	FMC_DROPOFF_FACTORY_ID_FAKE_CASH_3,
	FMC_DROPOFF_FACTORY_ID_FAKE_ID_3,	
	
	// DOCKS
	FMC_DROPOFF_FACTORY_ID_METH_4,
	FMC_DROPOFF_FACTORY_ID_WEED_4,
	FMC_DROPOFF_FACTORY_ID_CRACK_4,
	FMC_DROPOFF_FACTORY_ID_FAKE_CASH_4,
	FMC_DROPOFF_FACTORY_ID_FAKE_ID_4,
	
	FMC_DROPOFF_ACID_LAB,
	FMC_DROPOFF_DAX_WAREHOUSE,
	FMC_DROPOFF_TREVOR_METH_LAB,
	
	FMC_DROPOFF_RV_EL_BURRO_HEIGHTS,
	FMC_DROPOFF_RV_WIND_FARM,
	FMC_DROPOFF_RV_SENORA_DESERT,
	
	FMC_DROPOFF_POLICE_STING_VAR1_1,
	FMC_DROPOFF_POLICE_STING_VAR1_2,
	FMC_DROPOFF_POLICE_STING_VAR1_3,
	FMC_DROPOFF_POLICE_STING_VAR1_4,
	FMC_DROPOFF_POLICE_STING_VAR2_1,
	FMC_DROPOFF_POLICE_STING_VAR2_2,
	FMC_DROPOFF_POLICE_STING_VAR2_3,
	FMC_DROPOFF_POLICE_STING_VAR2_4,
	FMC_DROPOFF_POLICE_STING_VAR3_1,
	FMC_DROPOFF_POLICE_STING_VAR3_2,
	FMC_DROPOFF_POLICE_STING_VAR3_3,
	FMC_DROPOFF_POLICE_STING_VAR3_4,
	FMC_DROPOFF_POLICE_STING_VAR4_1,
	FMC_DROPOFF_POLICE_STING_VAR4_2,
	FMC_DROPOFF_POLICE_STING_VAR4_3,
	FMC_DROPOFF_POLICE_STING_VAR4_4,
	FMC_DROPOFF_POLICE_STING_VAR5_1,
	FMC_DROPOFF_POLICE_STING_VAR5_2,
	FMC_DROPOFF_POLICE_STING_VAR5_3,
	FMC_DROPOFF_POLICE_STING_VAR5_4,
	#ENDIF
	
	//Do NOT add FMC dropoffs below this line
	FMC_DROPOFF_SOLOMONS_OFFICE,
	
	//Rember to update _FREEMODE_DELIVERY_GET_DROPOFF_CATEGORY and FREEMODE_DELIVERY_GET_DROPOFF_DEBUG_NAME when adding more dropoffs

	FREEMODE_DELIVERY_DROPOFF_COUNT
ENDENUM
