//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	MP_Globals_Saved_Big_Ass_Vehicles.sch						//
//		AUTHOR			:	David Gentles												//
//		DESCRIPTION		:	Structs and enums used to store the MP vehicles that are 	//
//							too large to fit in the garage								//
//////////////////////////////////////////////////////////////////////////////////////////
		
CONST_INT NUM_BIG_ASS_VEHICLE_BITSETS 5

STRUCT MP_SAVED_BIG_ASS_VEHICLES_STRUCT
	INT 								iBAV_timestamp
	INT 								iBigAssVehiclesBS[NUM_BIG_ASS_VEHICLE_BITSETS]
ENDSTRUCT
//EOF
