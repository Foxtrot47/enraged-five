
USING "mp_globals_activity_selector_DEFINITIONS.sch"




// -----------------------------------------------------------------------------------------------------------
//      Contact Missions - Client
// -----------------------------------------------------------------------------------------------------------

// STRUCT containing the positions of contact missions on the local cloud header data arrays
g_structLocalContactMissions		g_sLocalMPCMs[MAX_CONTACT_MISSIONS]



// STRUCT containing Contact Mission Rank stats
g_structCMRankMissions			g_CMsPlayed



// -----------------------------------------------------------------------------------------------------------

// STRUCT Counts the number of Contact Missions played up to and including the current contact mission rank per contact
g_structCMsPlayedPerContact			g_cmsPlayedPerContact




// -----------------------------------------------------------------------------------------------------------

// STRUCT containing local storage of currently active contact mission invites
// NOTE: These are transition-safe so that the existing contact missions can be setup again after a transition
g_structLocalClientActiveCMs		g_lcacmActiveCMs[MAX_CONTACT_MISSION_INVITES_AT_ONCE]						// CONTIGUOUS - so shuffle up to fill any gaps




// -----------------------------------------------------------------------------------------------------------

// STRUCT containing control variables for checking if the player is on a Contact Mission
g_structClientPlayingCM				g_cpcmPlaying
INT									g_cpcmHashMostRecentFirstPlayContentID	= 0
BOOL								g_cpcmMostRecentFirstPlayFullCashGiven	= FALSE



// STRUCT containing local played history of Contact Mission
// NOTE: This is transition-safe so that the history is retained after transition
g_structClientHistoryCM				g_cmLocalHistory




// -----------------------------------------------------------------------------------------------------------
//      Gang Attacks - Client
// -----------------------------------------------------------------------------------------------------------

// STRUCT containing Client Control variables for the player's current Gang Attack - Local Variables
g_structCurrentGAClient			g_sCurrentClientGA


// STRUCT containing Client copy of the Server History List - for locking and unlocking of Gang Attacks
g_structGAHistoryClient			g_sHistoryClientGA


// EXPLOIT BUG 2002718: Store ContentID Hash of Gang Attack being played when the Store was accessed to prevent it getting setup again on return
INT								g_gaContentIdHashActiveOnGotoStore	= 0




// -----------------------------------------------------------------------------------------------------------
//      Heist Finales - Client
//		(the new Local Storage variables)
// -----------------------------------------------------------------------------------------------------------

// STRUCT containing the positions of Heist Strands on the local cloud header data arrays
g_structLocalHeistStrands			g_sLocalMPHeists[MAX_HEIST_STRANDS]
INT									g_numLocalMPHeists						= 0

g_structLocalHeistControl			g_sLocalMPHeistControl

g_structLocalHeistCrossSession		g_sLocalMPHeistCSData

g_structOtherHeistCoronasControl	g_sOtherMPHeistCoronas

// Heist Cooldown - delays giving a player a new Heist Strand if player has recently played someone else's heist
INT									g_heistCooldownTimeout					= 0

// Contains Planning information extracted from the pre-heist download of the Heist Finale mission
g_structPreHeistDownloadData		g_sPreHeistDownloadData

// Stores recently completed Heist strands to prevent quick replay
// I'm going to leave this data as-is on character selection, this means the delay will persist even if the player swaps character
g_structHeistReplayDelays			g_sHeistReplayDelay[MAX_HEISTS_WITH_REPLAY_DELAYS]

// Handles Unlocks that follow Heist Strand completion
g_structLocalHeistUnlockables		g_sLocalMPHeistUnlocks

// Stores the details of the current unlocked mission the player has been invited to
g_structActiveHeistReward			g_sActiveMPHeistReward

// To fix problems when swapping characters, flag that Heists should fully reset when joining MP
BOOL								g_resetHeistsOnJoiningMP = TRUE

// To store the Flow Order of the Heist Being Replayed
// KGM 9/2/15 [BUG 2226481]: Also to indicate if the replay is from the replay board (TRUE) or from the phone (FALSE)
INT									g_replayHeistFlowOrder = UNKNOWN_HEIST_FLOW_ORDER
BOOL								g_replayHeistIsFromBoard = FALSE

// To maintain any Heist help text that may be needed
g_structHeistHelpText				g_sHeistHelpText

// To maintain a Heist Completion Phonecall
g_structHeistCompletionPhonecall	g_sPostHeistPhonecall

// Player phoned Lester to Cancel Active Heist Strand
// NOTE: If strand Stats stored, the function dealing with g_cancelHeistStrandByLester_KeithStuff will request g_cleanupHeistStrandStats_AlastairStuff = TRUE
BOOL								g_cancelHeistStrandByLester_KeithStuff	= FALSE
BOOL								g_cleanupHeistStrandStats_AlastairStuff	= FALSE

// To fix a board flash upon the phone call from Lester being requested.
BOOL								g_bIsHeistStageCommsActive = FALSE

// KGM 7/2/15 [BUG 2228603]: To control the gathering of replayable heist data when the player is in their own apartment
g_structReplayableHeists			g_sReplayableHeists

// KGM 18/2/15 [BUG 2239042]: Store the result of the 'has replay board been unlocked' check as an optimisation for Conor checking it every frame
BOOL								g_hasReplayBoardBeenUnlocked = FALSE
// KGM 21/2/15 [BUG 2246472]: Allow the update fucntion to run which updates the condition of the above flag
BOOL								g_updateFlag_HasBoardBeenUnlocked = FALSE


// ===========================================================================================================
//      Transition Session Support Globals
// ===========================================================================================================

// KGM HEIST TRANSITION VARIABLES USED WHEN THE HEIST IS MOVING PAST THE INVITE STAGE - ENSURES THE SAME HEIST GETS SETUP WHEN THE PLAYERS MOVE TO THE NEW TRANSITION SESSION
// STRUCT containing the information needed to ensure the correct heist gets setup again in the new transition session
// KGM 8/4/13: Intended as TEMP to get transition support in quickly - it will need to be much more robust, probably passing Shared Mission and Pending Activity
//					so it can deal with the controlling player walking out of the corona, etc
// The controlling player stores this data when the SharedRegID is retrieved. After the transition it gets copied into the playerBD variables for the Host to pick up.
g_structASFMHeistTransitionMP g_sLocalTransitionSafeHeistData





// ===========================================================================================================
//      Cross-session invites into an apartment
// ===========================================================================================================

// KGM 8/4/14: This 'may' be temporary. It gets set when the player has accepted a cross-session invite into an apartment so that Conor
//				knows to wait for more cross-session data to arrive before launching the apartment script (or something).
BOOL g_sPlayerAcceptedCrossSessionInviteToHeist = FALSE



