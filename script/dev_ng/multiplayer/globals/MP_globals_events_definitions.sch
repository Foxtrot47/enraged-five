

CONST_INT MAX_NUM_PRE_GROUP_SCRIPTS	10

CONST_INT ASK_JOIN_MISSION_INI		0
CONST_INT ASK_JOIN_MISSION_WAIT		1
CONST_INT ASK_JOIN_MISSION_CLEANUP 	2

//Off Mission Interaction states
CONST_INT OFF_MISSION_INTERACTION_INI		0
CONST_INT OFF_MISSION_INTERACTION_RUNNING	1
CONST_INT OFF_MISSION_INTERACTION_CLEANUP	2


//variable set when player dies valid until they are alive again
STRUCT PLAYER_DEAD_MESSAGE
	//this should be smaller but code side its 63.. put in bug to get it changed
	TEXT_LABEL_23 sKillerName
	TEXT_LABEL_23 sDeadPlayerName
	TIME_DATATYPE iPlayerDeadMessageTimer
	BOOL bPlayerDeadMessageTimerInitialised
ENDSTRUCT

STRUCT REQUESTING_PLAYER_STAT_DATA
	INT iRequestID
	INT iResponses
	INT iIntStat[NUM_NETWORK_PLAYERS]
	FLOAT fFloatStat[NUM_NETWORK_PLAYERS]
	BOOL bBoolStat[NUM_NETWORK_PLAYERS]
ENDSTRUCT

//STRUCT MPGlobalsEventsStruct_OLD
//
//	MP_MISSION_DATA_OLD PreMissionScriptData[MAX_NUM_PRE_GROUP_SCRIPTS]
//	MP_MISSION_DATA_OLD JoinThisScriptData
//	PLAYER_DEAD_MESSAGE thePlayerDeadMessage
//	
//	VECTOR OffMissionInteractionVehicleCoords
//	INT iOffMissionInteractionState
//	BLIP_INDEX OffMissionVehicleDestBlip	
//
//	INT iAskIfJoinMissionStage
//
//	REQUESTING_PLAYER_STAT_DATA requestingPlayerStatData
//	INT number_times_killed_player[NUM_NETWORK_PLAYERS] // for tracking the number of times a network participant kills player (Archenemy)
//	INT number_times_killed_by_player[NUM_NETWORK_PLAYERS] // for tracking the number of times a network participant is killed by player (Biggest Victim)
//	
//	INT iOverheadTagID
//	BOOL bOverheadTagSetup
//	BOOL bOverheadTagInitialise
//
//ENDSTRUCT


STRUCT MPGlobalsEventsStruct

	MP_MISSION_DATA PreMissionScriptData[MAX_NUM_PRE_GROUP_SCRIPTS]
	MP_MISSION_DATA JoinThisScriptData
	PLAYER_DEAD_MESSAGE thePlayerDeadMessage
	
	VECTOR OffMissionInteractionVehicleCoords
	INT iOffMissionInteractionState
	BLIP_INDEX OffMissionVehicleDestBlip	

	INT iAskIfJoinMissionStage

	REQUESTING_PLAYER_STAT_DATA requestingPlayerStatData
	INT number_times_killed_player[NUM_NETWORK_PLAYERS] // for tracking the number of times a network participant kills player (Archenemy)
	INT number_times_killed_by_player[NUM_NETWORK_PLAYERS] // for tracking the number of times a network participant is killed by player (Biggest Victim)
	
	INT iOverheadTagID
	BOOL bOverheadTagSetup
	BOOL bOverheadTagInitialise

ENDSTRUCT

