
USING "mp_globals_spawning_definitions.sch"
USING "mp_globals_spawning_definitions_TU.sch"


// Copied from MP_Globals_Spawning.sch

SPAWN_INFO g_SpawnData
SPAWN_SEARCH_SCORE_DATA g_Spawning_UnSortedTopResults[NUM_OF_STORED_UNSORTED_SPAWN_RESULTS]

TRANSITION_SPAWN_DATA g_TransitionSpawnData

MISSION_SPAWN_GENERAL_BEHAVIOURS BlankMissionSpawnGeneralBehaviours

MISSION_SPAWN_PERSONAL_VEHICLE_DATA BlankMissionSpawnPersonalVehicleData

MISSION_SPAWN_DETAILS BlankMissionSpawnDetails

MISSION_SPAWN_EXCLUSION_AREAS BlankMissionSpawnExclusionAreas

SPAWN_AREA SpawnAreas_Hospitals[TOTAL_NUMBER_OF_RESPAWN_HOSPITALS]

SPAWN_AREA SpawnAreas_PoliceStation[TOTAL_NUMBER_OF_RESPAWN_POLICE_STATIONS]

SPAWN_AREA SpawnAreas_Hotels[TOTAL_NUMBER_OF_RESPAWN_HOTELS]

// global exclusion areas
GLOBAL_SPAWN_EXCLUSION_AREA GlobalExclusionArea[MAX_NUMBER_OF_GLOBAL_EXCLUSION_AREAS]

PROBLEM_AREA ProblemArea_Forbidden[NUMBER_OF_SPAWNING_SECTORS][MAX_NUMBER_OF_FORBIDDEN_AREAS_IN_SECTOR]
INT g_iTotalForbiddenProblemAreasForSector[NUMBER_OF_SPAWNING_SECTORS]

BOOL g_EnableBlockingNodesClubhouse7524212 = TRUE

FLOAT g_fSpawningSectorYLine[3]
FLOAT g_fSpawningSectorXLine[4]

PROBLEM_AREA ProblemArea_Nodes[NUMBER_OF_SPAWNING_SECTORS][MAX_NUMBER_OF_PROBLEM_NODE_AREAS_IN_SECTOR]
INT g_iTotalNodesProblemAreasForSector[NUMBER_OF_SPAWNING_SECTORS]

PROBLEM_AREA ProblemArea_NodeSearch[NUMBER_OF_PROBLEM_NODESEARCH_AREAS]

PROBLEM_AREA ProblemArea_MinAirplaneHeight[NUMBER_OF_MIN_AIRPLANE_HEIGHT_AREAS]

PROBLEM_AREA ProblemArea_CustomVehicleNodes[NUMBER_OF_CUSTOM_VEHICLE_NODE_AREAS]

SPAWN_SEARCH_CORONA gSavedAtCoords
SPAWN_SEARCH_GANG_ATTACK gSavedAtAngledArea

NEAREST_CAR_NODE_RESULTS g_NearestCarNodeResults

INT g_Spawn_RespawnFromStoreLastProperty
BOOL g_bDisableTextAnimOnSpawn

#IF IS_DEBUG_BUILD
BOOL g_bEditSynchSceneOffsets
FLOAT g_vSynchScenePhase = 0.0
VECTOR g_vSynchSceneCoordOffset = <<0,0,0>>
VECTOR g_vSynchSceneRotOffset = <<0,0,0>>
#ENDIF

VECTOR g_vFixedHeliLandingPos[TOTAL_FIXED_HELI_LANDING_COORDS]
FLOAT g_fFixedHeliLandingHeading[TOTAL_FIXED_HELI_LANDING_COORDS]

INTERIOR_SPAWN_ACTIVITY_DATA g_InteriorSpawnActData
BOOL g_bRunUpdated_GET_WORLD_POINT_OF_PLAYER = TRUE

