// This is now registered in its own block


USING "global_block_defines.sch"
USING "title_update_globals.sch"
USING "MP_globals_script_timers.sch"

// Multiplayer globals
GLOBALS GLOBALS_BLOCK_MP

// Multiplayer globals
USING "MP_globals_mission_control.sch"
USING "MP_globals_missions_at_coords.sch"
USING "MP_globals_missions_shared.sch"  
USING "MP_globals_activity_selector.sch"  
USING "MP_globals_Spawning.sch" 	
USING "MP_globals_COMMON.sch"

// USING "MP_globals_Teams.sch"			Moved to globals since it is accessed in startup.sc
//USING "MP_globals_CNC_Flow.sch"
//USING "MP_globals_FM.sch"				Moved to GLOBALS_BLOCK_MP_FM
USING "MP_globals_ambience.sch"
USING "MP_globals_comms.sch"
USING "MP_globals_mission_info.sch"
USING "MP_globals_hud.sch"		
USING "MP_globals_mission_trigger.sch"
USING "MP_globals_stats.sch"
USING "MP_globals_tickers.sch"
USING "MP_globals_events.sch"
USING "MP_globals_DM.sch"
USING "MP_globals_races.sch"
// USING "MP_globals_interior_instances.sch"	Moved to globals since it is accessed in startup.sc
//	USING "MP_globals_Tunables.sch"		Moved to its own block of globals called GLOBALS_BLOCK_TUNABLES
USING "MP_globals_spectator_old.sch"
USING "MP_globals_player_headshots.sch"
USING "MP_globals_ambient_manager.sch"
// USING "MP_Globals_saved.sch"			Moved to globals since it is accessed in startup.sc
USING "MP_globals_ScriptSaves.sch"
USING "MP_globals_interactions.sch"
USING "MP_Event_Enums.sch"

//// Multiplayer debug-only globals
//#IF IS_DEBUG_BUILD
//	USING "MP_globals_debug.sch"		// moved to globals as some values are used for les widgets.
//#ENDIF

USING "mp_globals_property.sch"
#IF FEATURE_FREEMODE_ARCADE
USING "mp_globals_arcade.sch"
#ENDIF

ENDGLOBALS	//	GLOBALS_BLOCK_MP





