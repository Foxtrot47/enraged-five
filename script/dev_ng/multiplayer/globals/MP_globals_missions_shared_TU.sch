USING "rage_builtins.sch"

USING "MP_globals_missions_shared_definitions.sch"
USING "MP_globals_missions_shared_definitions_TU.sch"
USING "MP_globals_common_definitions.sch"



// ===========================================================================================================
//      Contains all ServerBD structs, so this one struct can be included in the Global BD data
// ===========================================================================================================

// NOTE: This is stored as global server broadcast data: GlobalServerBD_MissionsShared
// KGM 25/2/13: Missions Shared Data - needs to be broadcast in case the host migrates
g_structMissionsSharedServerBD			GlobalServerBD_MissionsShared





// ===========================================================================================================
//      HEADER DATA GENERATED FROM THE FULL MISSION DATA AFTER CROSS-SESSION TRANSITION
//		(NOTE: This is used if a cross-session player accepts an invite into another player's UGC
//		because the UGC header data will not be available - however the UGC full-mission data is
//		available and therefore the header data can be generated from it)
// ===========================================================================================================

// A struct containing the header data generated from the full-mission data
g_structGeneratedCloudHeaderData	g_sGeneratedCloudHeaderData




// ===========================================================================================================
//      A TRANSITION-SAFE COPY OF THE SHARED MISSION DATA THE PLAYER IS MOST RECENTLY REGISTERED ON
//		(NOTE: This was added to solve the problem where the player plays another player's UGC
//			and wants to play again  when it is over but the UGC owner doesn't want to play again.
//			Storing the data ensures the mission can be setup again without the UGC owner.)
// ===========================================================================================================

// A copy of the player's most recent shared mission data that the player was registered on.
// This data can be used under specific circumstances to setup the mission again after a transition.
g_structCopyRegSharedMissionData	g_sCopyRegSharedMissionData




// ===========================================================================================================
//      Missions Shared Client Cloud Data
// ===========================================================================================================

// A client struct containing the local language version of the Mission Name
g_structLocalLanguageDisplayText	g_MSCLocalLanguageText




// ===========================================================================================================
//      Temporary-Download Mission Header Data
// ===========================================================================================================

// Contains a copy of the most recent header data downloaded from the cloud (intended for temporary - preferable immediate - use only)
// (NOTE: A copy of net_cloud_mission_loader.sc::theReturnedHeaderDetails, if the download is successful)
CLOUD_LOADED_MISSION_HEADER_DETAILS	g_cloudLoadedMissionHeader




// ===========================================================================================================
//      HEADER DATA STORED IF THIS IS THE PART ONE OF A STRAND MISSION
//		This is stored in case the part two mission Quick Restarts at Part One and the Part One corona
//		needs setup again.
// ===========================================================================================================

// A client struct containing the header data for the Part One mission
g_structStrandMissionP1HeaderData	g_sStrandMissionPartOne

// A flag to indicate the stored details should be used to setup a corona
BOOL								g_sSetUpStrandMissionPartOne	= FALSE



