
USING "MP_Globals_Common_Definitions.sch"

// -----------------------------------	SERVER BROADCAST DATA ----------------------------------------------------------------------

CONST_INT ciRACE_SUB_TYPE_STANDARD 		0
CONST_INT ciRACE_SUB_TYPE_GTA			1
CONST_INT ciRACE_SUB_TYPE_RALLY			2
CONST_INT ciRACE_SUB_TYPE_NON_CONTACT	3
CONST_INT ciRACE_SUB_TYPE_MAX			4

CONST_INT ciRACE_SUB_TYPE_BASEJUMP	8
CONST_FLOAT RACE_CLEAR_VEHICLE_AREA	3000.00
CONST_INT MAX_NUM_RACE_TEAMS		15

CONST_INT TEAM_RACE_CHOSE_NONE		-2
CONST_INT TEAM_RACE_CHOSE_ANY       -1

CONST_INT ciCORONA_CATCHUP_CODE_ON			0
CONST_INT ciCORONA_CATCHUP_CODE_OFF			1

CONST_INT ciRACE_PARA_WEAPONS_OFF	0
CONST_INT ciRACE_PARA_WEAPONS_ON	1

// Constants for solo split time comparison
CONST_INT RACE_SPLIT_COMPARISON_WORLD		0
CONST_INT RACE_SPLIT_COMPARISON_FRIEND		1
CONST_INT RACE_SPLIT_COMPARISON_CREW		2
CONST_INT RACE_SPLIT_COMPARISON_PERSONAL	3



//STRUCT GlobalServerBroadcastDataRaces_OLD
//	
//	BOOL bServerStartsRaces
//	
//	// Server starts lobby timer
//	BOOL bStartTimer
//	
//	BOOL bReadiedUp
//	
//	// For calculating lobby time
//	INT iStartTime
//	INT iCurrentTime
//	INT iEndTime	
//	
//	// For figuring out who to kick
////	INT iServerKickBitset
//	INT iRaceType			=   0
//	INT iRaceClass			=	0
//	INT iRaceTrack			=	0
//	INT iLaps				=	0
//	INT iVehicleDamage		=	0
//	INT iTraffic			=	0
//	INT iPedestrians		=	0
//	INT iOnlineIDDisplay	=	0
//	INT iVoiceChat			=	0
//	INT iTimeOfDay			=	0
//	INT iWeather			=	0
////	INT iDescription		=	0
//	INT iKickplayer			=	0
//	INT iAutoAim			=	0
//	INT iWeapons			=	0
//	INT iTags				=	0
////	INT iNumRacers			= 	0
//	INT iStartingGrid		= 	0
//	INT iPolice 			= 	0
//	INT iRadio	 			= 	0
//	INT iSwapRoles			= 	0
//	INT iSplitComparison	=	0
//	INT iGTATeamRace		=	0
//	INT iBaseJumpShooting	=	0
//	
//	// Ok to reset global variable flag
//	BOOL bResetRaceOptions
//	BOOL bRcRace
//	INT iRaceMode
//ENDSTRUCT




STRUCT GlobalServerBroadcastDataRaces
	
//	BOOL bServerStartsRaces
	
	// Server starts lobby timer
	BOOL bStartTimer
	
	BOOL bReadiedUp
	
	// For calculating lobby time
	INT iStartTime
	INT iCurrentTime
	INT iEndTime	
	
	// For figuring out who to kick
//	INT iServerKickBitset
	INT iRaceType			=   0
	INT iRaceClass			=	0
	INT iRaceTrack			=	0
	INT iLaps				=	0
	INT iVehicleDamage		=	0
	INT iTraffic			=	0
	INT iPedestrians		=	0
	INT iOnlineIDDisplay	=	0
	INT iVoiceChat			=	0
	INT iTimeOfDay			=	0
	INT iWeather			=	0
//	INT iDescription		=	0
	INT iKickplayer			=	0
	INT iAutoAim			=	0
	INT iWeapons			=	0
	INT iTags				=	0
//	INT iNumRacers			= 	0
	INT iStartingGrid		= 	0
	INT iPolice 			= 	0
	INT iRadio	 			= 	0
	INT iSwapRoles			= 	0
	INT iSplitComparison	=	0
	INT	iSlipStream			=	0
	INT iGhostRace			=	0
	INT iGTATeamRace		=	0
	INT iBaseJumpShooting	=	0
	INT iCatchupCode		=	0
	INT iDestroyLoser		= 	0
	BOOL bAggregate
	
	// Ok to reset global variable flag
	BOOL bResetRaceOptions
	BOOL bRcRace
	INT iRaceMode
ENDSTRUCT


// -----------------------------------	CLIENT BROADCAST DATA ----------------------------------------------------------------------
// 					Everyone can read this data, only the local player can update it. 
STRUCT GlobalPlayerBroadcastDataRaces
	
	INT iRaceVehicle			=	0
	INT iRaceColor				=	0
	
//	INT iClientBitsetRaces	
	
	INT iPlayerID = 0
	INT iGBDSlot = 0
	TEXT_LABEL_63	txtPlayerName
	
	// Speirs added for Brenda 05/12/11 see 245499
//	BOOL bInRaces
	INT	  iTotalPoints
	INT	  iLargeTargetsHit
	INT	  iMediumTargetsHit
	INT	  iSmallTargetsHit
	INT	  iTinyTargetsHit
	
	INT iPlayerEliminatedBitSet
ENDSTRUCT



