/**********************************************************
/ Name: 		MP_globals_cash_transactions_definitions.sch
/ Author(s): 	James Adwick
/ Purpose: 		Definitions of cash transaction globals
***********************************************************/

USING "commands_netshopping.sch"
USING "commands_money.sch"

ENUM CASH_TRANSACTION_TYPE
	CASH_TRANSACTION_TYPE_SERVICE,
	CASH_TRANSACTION_TYPE_BASKET
ENDENUM

ENUM CASH_TRANSACTION_STATUS
	CASH_TRANSACTION_STATUS_NULL = 0,
	CASH_TRANSACTION_STATUS_PENDING,
	CASH_TRANSACTION_STATUS_SUCCESS,
	CASH_TRANSACTION_STATUS_FAIL
ENDENUM

CONST_INT CTPF_NONE 				0
CONST_INT CTPF_AUTO_RETRY 			1
CONST_INT CTPF_FAIL_ALERT 			2
CONST_INT CTPF_AUTO_PROCESS_REPLY 	4
CONST_INT CTPF_FILL_FAIL_GLOBALS	8
CONST_INT DEFAULT_CASH_TRANSACTION_FLAGS	CTPF_AUTO_RETRY|CTPF_FAIL_ALERT
CONST_INT DEFAULT_MAIN_TRANSITION_CASH_TRANSACTION_FLAGS 	CTPF_AUTO_RETRY|CTPF_FILL_FAIL_GLOBALS
CONST_INT NET_SHOP_MAX_TIMED_RETRIES	4

CONST_INT TRANSACTION_ALERT_SILENT		0
CONST_INT TRANSACTION_ALERT_FULLSCREEN	1
CONST_INT TRANSACTION_ALERT_FEED		2

STRUCT STORED_EXTRA_CASH_INFO
	GAMER_HANDLE gamerHandle
	GAMER_HANDLE gamerHandle2
	TEXT_LABEL_31 tl31ContenID
	TEXT_LABEL_23 tl23Challenge
	TEXT_LABEL_15 tl15Type
	TEXT_LABEL_31 tl15Reason 
ENDSTRUCT

STRUCT STORED_CASH_INFO
	INT iItemHash
	INT iLocation
	eStripClubExpenditureType eExpenseType
	MODEL_NAMES mModelName
	BOOL bHead2Head
	PURCHASE_TYPE purchaseType
	INT iGangUUIDA
	INT iGangUUIDB
	BOOL bNotInAGang
	INT iNumCrates
	INT iVehicle1	//Used for selling IE vehicles
	INT iVehicle2
	INT iVehicle3
	INT iExtraCashAmount
ENDSTRUCT

STRUCT CASH_TRANSACTION_ESSENTIAL_DETAILS
	INT iTransactionId = NET_SHOP_INVALID_ID
	INT iCost
	CASH_TRANSACTION_STATUS eTransactionStatus
	TRANSACTION_TYPES eTransactionType
	TRANSACTION_SERVICES eTransactionService
	CASH_TRANSACTION_TYPE eCashTransactionType
	SHOP_ITEM_CATEGORIES eTransactionCategory
	ITEM_ACTION_TYPES eActionType
	BOOL bTriggerFakeEvent
	INT iRetryAttempts
	CATALOG_ITEM_FLAGS eFlags
	INT iProcessFlags
	BOOL bTransactionStarted
	BOOL bTransactionReady
	INT iTransactionDelay
	INT iItemID
	INT iExtraItemID
	INT iEventCode
	
	//#IF IS_DEBUG_BUILD
		INT iEventFrameCount
	//#ENDIF
ENDSTRUCT

// Main struct for script to store info of ongoing transactions
STRUCT CASH_TRANSACTIONS_DETAILS	
	STORED_CASH_INFO cashInfo
	STORED_EXTRA_CASH_INFO eExtraData
	CASH_TRANSACTION_ESSENTIAL_DETAILS eEssentialData
ENDSTRUCT

// Transaction event data to catch the modders.
STRUCT CASH_TRANSACTION_EVENT_DETAILS
	INT iTransactionType
	INT iTransactionAction
	INT iTransactionServiceID
	INT iTransactionCost
	INT iEventCode
	INT iEventType
	INT iEventAction
	INT iEventServiceID
	INT iEventBank
	INT iEventWallet
	INT iFrameCount
	BOOL bValidate
ENDSTRUCT

// Cached version of transaction data. Used for the time re-try system.
STRUCT CASH_TRANSACTIONS_DETAILS_CACHE
	INT iTransactionId = NET_SHOP_INVALID_ID
	INT iCost
	
	TRANSACTION_SERVICES eTransactionService
	SHOP_ITEM_CATEGORIES eTransactionCategory
	
	TIME_DATATYPE retryTimeStamp
	INT iTimedRetryAttempts
	INT iTransactionTetherID = NET_SHOP_INVALID_ID
	
ENDSTRUCT

STRUCT CASH_TRANSACTION_EVENT
	CASH_TRANSACTION_ESSENTIAL_DETAILS eDetails
	STORED_CASH_INFO cashInfo
	INT iSlot
ENDSTRUCT
