USING "types.sch"
USING "mp_mission_data_struct.sch"
USING "clock_globals.sch"
USING "MP_globals_ambience_definitions.sch"
USING "mp_globals_common_definitions.sch"
USING "MP_globals_ambience_nightclub.sch"
USING "MP_globals_ambience_casino.sch"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////         				         MAIN GLOBAL AMBIENCE             				  /////////////////
/////////////////    Contains global variable instances for multiplayer ambient scripts.	  	  /////////////////  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Arresting
BOOL g_bIwasDeliveredWhileInCustodyNoLoseWantedAward // For Kev

STRUCT sDEVSPECTATESTRUCT
	BOOL b_DevSpectateLbd 
ENDSTRUCT

sDEVSPECTATESTRUCT g_s_DevSpectateStruct

// ===========================================================================================================
//     	Criminal Globals
// ===========================================================================================================

//#IF IS_DEBUG_BUILD
//	BOOL Crim_Warp_HUP_SHOP247_1
//	BOOL Crim_Warp_HUP_SHOP247_2
//	BOOL Crim_Warp_HUP_SHOP247_3
//	BOOL Crim_Warp_HUP_LIQUOR_1
//#ENDIF

// g_iSellBitSet
CONST_INT ciGB_BIKER_SELL_SERV_INIT_DONE 0

BOOL g_bGoToLoadingIfInReducedContent = FALSE

#IF FEATURE_HEIST_ISLAND
BOOL g_bBlockAllContactRequestsOnIsland = TRUE
#ENDIF

// Widgets & Debug.
#IF IS_DEBUG_BUILD
//	INT iCarTheftDebugCarBaseValue
//	BOOL bCarTheftWarpToDocks
//	BOOL bCarTheftDeliveryAreaOccupied
//	//BOOL bCarTheftIsCarInDeliveryArea
//	BOOL bCarTheftImDriving
//	BOOL bCarTheftMyCarExists
//	
//	// Cop dispatch debug
//	BOOL bDispatchAddDispatch
//	BOOL bTriggerSpecificDispatch
//	INT iDispatchDispatchNum = 1
//	BOOL bDispatchCleanUpArrays
//	BOOL bDispatchWarpToTestArea
	
	BOOL g_db_bOutputCallContactBlocks
	BOOL g_db_bTestPropertyEntranceFailure
	
	INT g_BombushkaTurretCamRotSpeedMultiplier = -1
	INT g_BombushkaTurretCamZoomSpeedMultiplier = -1
#ENDIF


// -----------------------------------------------------------------------------------------------------------
// Police scanner report subtitles
// -----------------------------------------------------------------------------------------------------------

FLOAT CopScanner_x = 0.50
FLOAT CopScanner_y = 0.85
FLOAT CopScanner_w = 0.30
FLOAT CopScanner_h = 0.45
FLOAT CopScanner_sWrap = 0.23
FLOAT CopScanner_eWrap = 0.77




BOOL g_bAllowRemoveYachtIPLsFromInactiveYachts = TRUE

// ===========================================================================================================
//      KGM 20/9/11: Crook Cabs Globals
// ===========================================================================================================

// Heli gun.
BOOL g_bTerminateHeliGunScript
INT g_iShouldLaunchTruckTurret = -1
BOOL g_bActiveInGunTurret
BOOL g_bCanPlayerUseTurretControls 
INT g_iPlayerIsInModTurretAngledArea 
BOOL g_bBustedWarpInProgress
INT g_iVehicleTurretSeat = -1
INT g_iInteriorTurretSeat = -1
BOOL g_bBombuskaExitCamTimerOn
BOOL g_bIAAGunCamerasEnabled
#IF FEATURE_HEIST_ISLAND
INT g_iShouldLaunchSubmarineSeat = -1
BOOL g_bKillWarpTransitionInvincibility = FALSE
#ENDIF

LAST_JOB_PLAYERS  		lastJobPlayers

// AMEC HEISTS
LAST_JOB_PLAYERS		lastHeistPlayers

LAST_JOB_PLAYERS		lastGangOpsHeistPlayers

NETWORK_PLATFORM_PARTY_DESC g_PartyDesc

MORS_MUTUAL_INSURANCE_STRUCT morsMutualData




// ===== TO BE MADE INTO STATS ======
//INT g_iStat_PersonalisedCarPlate0 	// temp until get proper stat
//INT g_iStat_PersonalisedCarPlate1	// temp until get proper stat
//INT g_iStat_PersonalisedCarPlate2	// temp until get proper stat
//INT g_iStat_PersonalisedCarPlate3	// temp until get proper stat
//INT g_iStat_PersonalisedCarPlate4	// temp until get proper stat
//
//INT g_iStat_SoldCarPlate0 	// temp until get proper stat
//INT g_iStat_SoldCarPlate1	// temp until get proper stat
//INT g_iStat_SoldCarPlate2	// temp until get proper stat
//INT g_iStat_SoldCarPlate3	// temp until get proper stat
//INT g_iStat_SoldCarPlate4	// temp until get proper stat
//
//INT g_iStat_KnownDupePlate0 // temp until get proper stat
//INT g_iStat_KnownDupePlate1	// temp until get proper stat
//INT g_iStat_KnownDupePlate2	// temp until get proper stat
//INT g_iStat_KnownDupePlate3	// temp until get proper stat
//INT g_iStat_KnownDupePlate4	// temp until get proper stat
//INT g_iStat_KnownDupePlate5 // temp until get proper stat
//INT g_iStat_KnownDupePlate6	// temp until get proper stat
//INT g_iStat_KnownDupePlate7	// temp until get proper stat
//INT g_iStat_KnownDupePlate8	// temp until get proper stat
//INT g_iStat_KnownDupePlate9	// temp until get proper stat
//
//INT g_iStat_KnownDupeSold0 // temp until get proper stat
//INT g_iStat_KnownDupeSold1	// temp until get proper stat
//INT g_iStat_KnownDupeSold2	// temp until get proper stat
//INT g_iStat_KnownDupeSold3	// temp until get proper stat
//INT g_iStat_KnownDupeSold4	// temp until get proper stat
//INT g_iStat_KnownDupeSold5 // temp until get proper stat
//INT g_iStat_KnownDupeSold6	// temp until get proper stat
//INT g_iStat_KnownDupeSold7	// temp until get proper stat
//INT g_iStat_KnownDupeSold8	// temp until get proper stat
//INT g_iStat_KnownDupeSold9	// temp until get proper stat


// ==== PERSONALISED PLATES ====
CONST_INT TOTAL_STORED_STATS_PLATES_PERSONALISED 5

// used to store stat values for use in functions, to avoid too many calls to stat natives.
INT g_iPersonalisedCarPlateStats[TOTAL_STORED_STATS_PLATES_PERSONALISED]

CONST_INT TOTAL_STORED_GLOBAL_PLATES_PERSONALISED 20
INT g_iPersonalisedCarPlateGlobals[TOTAL_STORED_GLOBAL_PLATES_PERSONALISED]
INT g_iStoredIFruitPlate = 0

// ==== SOLDS PLATES ====
CONST_INT TOTAL_STORED_STATS_PLATES_SOLD 5

// used to store stat values for use in functions, to avoid too many calls to stat natives.
INT g_iSoldCarPlateStats[TOTAL_STORED_STATS_PLATES_SOLD]

CONST_INT TOTAL_STORED_GLOBAL_PLATES_SOLD 20
INT g_iSoldCarPlateGlobals[TOTAL_STORED_GLOBAL_PLATES_PERSONALISED]


// ==== KNOWN DUPES ====
CONST_INT TOTAL_STORED_STATS_KNOWN_DUPES 10

// used to store stat values for use in functions, to avoid too many calls to stat natives.
INT g_iKnownDupePlateStats[TOTAL_STORED_STATS_KNOWN_DUPES]
INT g_iKnownDupeSoldStats[TOTAL_STORED_STATS_KNOWN_DUPES]

CONST_INT TOTAL_STORED_GLOBAL_PLATES_KNOWN_DUPES 20
INT g_iKnownDupePlateGlobals[TOTAL_STORED_GLOBAL_PLATES_KNOWN_DUPES]
INT g_iKnownDupeSoldGlobals[TOTAL_STORED_GLOBAL_PLATES_KNOWN_DUPES]

// === OTHER ===
CONST_INT NUMBER_OF_HIGH_MOD_VALUE_MODELS 15
MODEL_NAMES g_HighModValueModels[NUMBER_OF_HIGH_MOD_VALUE_MODELS]

VEHICLE_INDEX g_VehicleIDPlateDupeCheck
VEHICLE_INDEX g_SpawnedInVehicle

BOOL g_bDontCheckForArmoryPropertyDupe = FALSE
BOOL g_bOverride_ShouldIgnorePlateDupeCheck = FALSE

CONST_INT NUMBER_OF_VEHICLES_IN_METRIC_SENT 30
INT g_iVehicleIDMetricSent[NUMBER_OF_VEHICLES_IN_METRIC_SENT]
INT g_iLocationMetricSent[NUMBER_OF_VEHICLES_IN_METRIC_SENT]

BOOL g_bOptOutPlayerFromDupeDetector = FALSE

INT g_iRequiredSpacesInKnownDupeListForGarageScan = 4

INT g_iExploitLevelToBlockCustomPlates = 4

BOOL g_LockDupeVehicle_NonSaveOnly_1 = FALSE
BOOL g_LockDupeVehicle_NonSaveOnly_2 = FALSE
BOOL g_LockDupeVehicle_NonSaveOnly_3 = FALSE
BOOL g_LockDupeVehicle_NonSaveOnly_4 = TRUE
BOOL g_LockDupeVehicle_NonSaveOnly_5 = FALSE

// ==== TEMP - these need to be made into tunables ====

//// TUNABLES - FUNCTION ENABLERS
//BOOL  g_sMPTunables.bEnabled_IS_PERSONALISED_PLATE_OF_LOCAL_PLAYER = TRUE
//BOOL  g_sMPTunables.bEnabled_IS_PLATE_DUPE_OF_CAR_IN_GARAGE = TRUE
//BOOL  g_sMPTunables.bEnabled_ADD_PLATE_TO_RECENTLY_USED_PERSONALISED_PLATE_LIST = TRUE
//BOOL  g_sMPTunables.bEnabled_HAS_PLATE_BEEN_RECENTLY_USED_IN_PERSONALISED_PLATE_OPTION = TRUE
//BOOL  g_sMPTunables.bEnabled_ADD_PLATE_TO_RECENTLY_USED_SOLD_PLATE_LIST = TRUE
//BOOL  g_sMPTunables.bEnabled_HAS_PLATE_BEEN_RECENTLY_SOLD = TRUE
//BOOL  g_sMPTunables.bEnabled_DOES_VEHICLE_HAVE_REQUIRED_DECORATORS = TRUE
//BOOL  g_sMPTunables.bEnabled_ADD_PLATE_TO_KNOWN_DUPE_PLATE_LIST = TRUE
//BOOL  g_sMPTunables.bEnabled_IS_PLATE_A_KNOWN_DUPE = TRUE
//BOOL  g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE = TRUE
//BOOL  g_sMPTunables.bEnabled_IS_VEHICLE_A_GARAGE_DUPE = TRUE
//BOOL  g_sMPTunables.bEnabled_LOCK_ANY_AMBIENT_DUPES_OF_THIS_VEHICLE = TRUE
//BOOL  g_sMPTunables.bEnabled_ENSURE_PV_PLATE_MATCHES_SAVED_DATA = TRUE
//BOOL  g_sMPTunables.bEnabled_IS_VEHICLE_A_SUSPECTED_DUPE = TRUE
//
//// TUNABLES - CHECKS
//BOOL  g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_1 = TRUE
//BOOL  g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_2 = TRUE
//BOOL  g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_3 = TRUE
//BOOL  g_sMPTunables.bEnabled_RUN_CHECK_PLATE_DUPE_CHECK_ON_CURRENT_VEHICLE_Check_4 = TRUE
//BOOL  g_sMPTunables.bEnabled_CAN_WE_USE_THIS_VEHICLE_check_1 = TRUE
//BOOL  g_sMPTunables.bEnabled_CAN_WE_USE_THIS_VEHICLE_check_2 = TRUE
//BOOL  g_sMPTunables.bEnabled_CAN_WE_USE_THIS_VEHICLE_check_3 = TRUE
//BOOL  g_sMPTunables.bEnabled_DO_STAGE_PLATES_check_1 = TRUE
//BOOL  g_sMPTunables.bEnabled_GET_CARMOD_SELL_VALUE_apply_dupe_multiplier = FALSE


// TUNABLES - LIMITS
//INT g_sMPTunables.iTotalModValueThresholdLow = 2000
//INT g_sMPTunables.iTotalModValueThresholdHigh = 10000
//FLOAT  g_sMPTunables.fKnownDupeMultiplierLevel0 = 1.0
//FLOAT  g_sMPTunables.fKnownDupeMultiplierLevel1 = 0.5
//FLOAT  g_sMPTunables.fKnownDupeMultiplierLevel2 = 0.25
//FLOAT  g_sMPTunables.fKnownDupeMultiplierLevel3 = 0.125
//FLOAT  g_sMPTunables.fKnownDupeMultiplierLevel4 = 0.06
//FLOAT  g_sMPTunables.fKnownDupeMultiplierLevel5 = 0.02
//INT g_sMPTunables.iNumberOfGarageDupesAllowedToBeSold = 2
//INT g_sMPTunables.iForcedExploitLevelIncrease=0
//BOOL g_sMPTunables.bForceMaxCapHitToday = FALSE


// ==== DEBUG ====
#IF IS_DEBUG_BUILD
BOOL g_bCreateAmbientDupe
BOOL g_bApplyPlateToCurrentVehicle
BOOL g_bGetHashOfPlate
BOOL g_bOverideDupeCoord
INT g_iHashOfPlate
TEXT_WIDGET_ID g_AmbientDupePlateText
INT g_iAmbientDupeModelHash = 970598228 // SULTAN

BOOL g_bClearPlateDupeStatLists
BOOL g_bOutputVehicleTimestamps
BOOL g_bGiveVehicleRandomMods
BOOL g_bGiveVehicleRandomColour

BOOL g_bSavePlateDupeStatLists

BOOL g_bClearRecentlyChangedPlatesLists
BOOL g_bClearRecentlySoldLists
BOOL g_bClearKnownDupesLists
BOOL g_bDupeCurrentVehicle
BOOL g_bDupeCurrentVehicleAndPlate
BOOL g_bUnbrickCarPlayerIsUsing
BOOL g_bDupeCurrentGarageVehicle

FLOAT g_fDD_Scale = 0.428
FLOAT g_fDD_LineY = 0.03

VEHICLE_INDEX g_LockedDupe
INT g_LockedDupeReason

BOOL g_bDD_ShowDebugText = TRUE
BOOL g_bDD_OutputAllValues = FALSE

BOOL g_bHasProcessedIFruitOrder

#ENDIF


// for car mod value functions
ENUM MOD_PRICE_VARIATION_ENUM
	MPV_STANDARD 			= 0,
	MPV_SPORT 				= 1,
	MPV_SUV 				= 2,
	MPV_SPECIAL 			= 3,
	MPV_BIKE 				= 4,
	MPV_BTYPE3 				= 5,
	MPV_SULTANRS 			= 6,
	MPV_BANSHEE2 			= 7,
	MPV_LURCHER_LIVERY 		= 8,
	MPV_BTYPE2_LIVERY 		= 9,
	MPV_VIRGO2				= 10,
	MPV_SLAMVAN3			= 11,
	MPV_FACTION3_WHEELS 	= 12,
	MPV_OMNIS_LIVERY 		= 13,
	MPV_BF400_LIVERY 		= 14,
	MPV_TROPOS_LIVERY 		= 15,
	MPV_BRIOSO_LIVERY 		= 16,
	MPV_TROPHYTRUCK_LIVERY 	= 17,
	MPV_TROPHYTRUCK2_LIVERY = 18,
	MPV_CLIFFHANGER_LIVERY 	= 19,
	MPV_TAMPA2_LIVERY 		= 20,
	MPV_GARGOYLE_LIVERY 	= 21,
	MPV_LE7B_LIVERY 		= 22,
	MPV_TYRUS_LIVERY 		= 23,
	MPV_LYNX_LIVERY 		= 24,
	MPV_SHEAVA_LIVERY 		= 25,
	MPV_IE_RETRO 			= 26,
	MPV_IE_HIGH 			= 27,
	MPV_IE_BIKE	 			= 28,
	MPV_GR_HEAVY			= 29,
	MPV_GR_LIGHT			= 30,
	MPV_GR_BIKE				= 31,
	MPV_GR_TRAILERLARGE		= 32,
	MPV_SMUG_STANDARD 		= 33,
	MPV_SMUG_HEAVY 			= 34,
	MPV_GANG_OPS_STANDARD	= 35,
	MPV_BATTLE_STANDARD		= 36,
	MPV_BATTLE_HEAVY		= 37,
	MPV_ARENA_WARS			= 38,
	MPV_RC_BANDITO			= 39
ENDENUM

MOD_PRICE_VARIATION_ENUM g_eModPriceVariation = MPV_STANDARD
VEHICLE_INDEX g_ModVehicle
SHOP_NAME_ENUM g_eModShopName
INT g_iPersonalCarModVar


CONST_INT MOD_NAME_HANDLEBARS HASH("TOP_BAR") //Handle Bars
CONST_INT MOD_NAME_TRUCKBED HASH("TOP_BED") //Truck Bed
CONST_INT MOD_NAME_BODYWORK HASH("TOP_BODY") //Bodywork
CONST_INT MOD_NAME_BULLBARS HASH("TOP_BULL") //Bullbars
CONST_INT MOD_NAME_BULLBARS2 HASH("GB_BULLBAR") //Bullbars
CONST_INT MOD_NAME_ROLLCAGE HASH("TOP_CAGE") //Roll Cage
CONST_INT MOD_NAME_ENGINEBAY HASH("TOP_ENG") //Engine Bay
CONST_INT MOD_NAME_FAIRING HASH("TOP_FARI") //Fairing
CONST_INT MOD_NAME_FRONTFORKS HASH("TOP_FORK") //Front Forks
CONST_INT MOD_NAME_FRAME HASH("TOP_FRAME") //Frame
CONST_INT MOD_NAME_SADDLEBAGS HASH("TOP_BAGS") //Saddlebags
CONST_INT MOD_NAME_HEADLIGHTS HASH("TOP_HLT") //Headlights
CONST_INT MOD_NAME_MIRRORS HASH("TOP_MIR") //Mirrors
CONST_INT MOD_NAME_FRONTMUD HASH("TOP_MUDF") //Front Mudguard
CONST_INT MOD_NAME_REARMUD HASH("TOP_MUDR") //Rear Mudguard
CONST_INT MOD_NAME_PLATEHOLDER HASH("TOP_PLAT") //Plate Holder
CONST_INT MOD_NAME_PUSHBAR HASH("TOP_PUSH") //Pushbar
CONST_INT MOD_NAME_FRONTSEAT HASH("TOP_SEATF") //Front Seat
CONST_INT MOD_NAME_REARSEAT HASH("TOP_SEATR") //Rear Seat
CONST_INT MOD_NAME_SIDESTEP HASH("TOP_SSTEP") //Side Step
CONST_INT MOD_NAME_SPAREWHEEL HASH("TOP_SPARE") //Spare Wheel
CONST_INT MOD_NAME_FUELTANK HASH("TOP_TNK") //Fuel Tank
CONST_INT MOD_NAME_WHEELIEBAR HASH("TOP_WBAR") //Wheelie Bar
CONST_INT MOD_NAME_TAILGATE HASH("TOP_TGATE") //Tailgate
CONST_INT MOD_NAME_TRUNK HASH("TOP_TRUNK") //Trunk
CONST_INT MOD_NAME_DEFLECTORS HASH("TOP_DEF") //Light Deflectors
CONST_INT MOD_NAME_ORNAMENTS HASH("TOP_ORN") //ORNAMENTS
CONST_INT MOD_NAME_OILTANK HASH("TOP_OIL") //Oil tank
CONST_INT MOD_NAME_SISSY HASH("TOP_SISSY") //sissy bar
CONST_INT MOD_NAME_SHIFTER HASH("TOP_SHIFTER") //shifter
CONST_INT MOD_NAME_WINDSHEILD HASH("TOP_WINDSH") //wind sheild 
CONST_INT MOD_NAME_RACKS HASH("TOP_RACK") //racks
CONST_INT MOD_NAME_ANTENNA HASH("TOP_ANTENNA") // front antenna
CONST_INT MOD_NAME_ARCHCOVER HASH("TOP_ARCHCOVER") // arch cover
CONST_INT MOD_NAME_REARANTENNA HASH("TOP_ANTENNAR") // rear antenna
CONST_INT MOD_NAME_AIRFILTER HASH("TOP_AIR") // air filter
CONST_INT MOD_NAME_ENGINEBLOCK HASH("TOP_ENGINE") // engine block
CONST_INT MOD_NAME_MUDRGUARDS HASH("TOP_MUD") // Mudgruards
CONST_INT MOD_NAME_BACKRESTS HASH("TOP_BACKREST") // Backrests
CONST_INT MOD_NAME_HEADLIGHTCOV HASH("TOP_HEADCO") // headlight covers
CONST_INT MOD_NAME_FBUMPER HASH("TOP_BUMF") // front bumper
CONST_INT MOD_NAME_RBUMPER HASH("TOP_BUMR") // rear bumper
CONST_INT MOD_NAME_REAR_COVER HASH("TOP_COVER") //Rear Cover
CONST_INT MOD_NAME_REAR_DIFFUSER HASH("TOP_RDIFF") //Rear Diffuser
CONST_INT MOD_NAME_FRONT_DIFFUSER HASH("TOP_FDIFF") //Front Diffuser
CONST_INT MOD_NAME_ENGINE_COVER HASH("TOP_BOOT") //Engine Cover
CONST_INT MOD_NAME_HOOD_CATCH HASH("TOP_CATCH") //Hood Catches
CONST_INT MOD_NAME_SPLITTER HASH("TOP_SPLIT") //Splitter
CONST_INT MOD_NAME_HEADLIGHT_COV HASH("TOP_HL_CV") //Headlight covers
CONST_INT MOD_NAME_SUNSTRIPS HASH("TOP_SUNST") //Sunstrips
CONST_INT MOD_NAME_SEC_LIGHT HASH("TOP_SEC_LIGHT") //Secondary light surround
CONST_INT MOD_NAME_WEAPONS HASH("TOP_WEAPONS") //Weapons
CONST_INT MOD_NAME_ARMOR_PLATING HASH("TOP_ARM_PLAT") //Armor plating
CONST_INT MOD_NAME_REAR_PANEL HASH("TOP_RPNL") //Rear panel
CONST_INT MOD_NAME_DOOR_PLATE HASH("TOP_DPLT") //Door Plate
CONST_INT MOD_NAME_HEAD_LIGHT_PRO HASH("TOP_HDLP") // Headlight protector
CONST_INT MOD_NAME_REAR_LIGHT_PRO HASH("TOP_RLP") //Rear Light Protectors
CONST_INT MOD_NAME_WINDOW_PLATE HASH("TOP_WINP") //Window Plates
CONST_INT MOD_NAME_PROXIMITY_MINE HASH("TOP_MINE") //P Mines
CONST_INT MOD_NAME_SEAT HASH("TOP_SEAT") //P Mines
CONST_INT MOD_NAME_PRIMARY_WEAPON HASH("TOP_PWEAPON") //Primary weapon 
CONST_INT MOD_NAME_SUPPORT_WEAPON HASH("TOP_COUNTERM") //Support weapon
CONST_INT MOD_NAME_BOMB HASH("TOP_PBOMBS") //Bombs
CONST_INT MOD_NAME_THRUST HASH("TOP_THRUST") //Thrust
CONST_INT MOD_NAME_SECONDARY_WEAPON HASH("TOP_SWEAPON") //Secondary weapon
CONST_INT MOD_NAME_PROPELLER HASH("TOP_PROPELLER") //Propellers
CONST_INT MOD_NAME_BONNET HASH("TOP_BONNET") //Bonnet
CONST_INT MOD_NAME_BUMPER_PANEL HASH("TOP_BUM_PAN") //Bumper panel
CONST_INT MOD_NAME_SIDE_PANEL HASH("TOP_SIDE_PAN") //Side panel
CONST_INT MOD_NAME_DOORS HASH("TOP_DOORS") //Doors
CONST_INT MOD_NAME_TURRET_BRACKET HASH("TOP_TURRET_B") //Turret bracket
CONST_INT MOD_NAME_STRUT_BRACE HASH("TOP_BRACE") //Strut Brace
CONST_INT MOD_NAME_ENGINE_DETAIL HASH("TOP_ENGD") //Engine Detail
CONST_INT MOD_NAME_LOUVRES HASH("TOP_LOUV") //Louvres
CONST_INT MOD_NAME_VISORS HASH("TOP_VISORS")	// Visors 
CONST_INT MOD_NAME_FOGLIGHT_COVERS HASH("TOP_FOGLIGHTS")	// Foglights covers
CONST_INT MOD_NAME_VALVE_COV HASH("TOP_VALVE")	// Valve Covers 
CONST_INT MOD_NAME_ROOF_LIGHT HASH("TOP_ROOF_LIGHT") //Rs
CONST_INT MOD_NAME_SNORKEL HASH("TOP_SNORKEL") //Snorkel
CONST_INT MOD_NAME_DEFLEC 	HASH("TOP_DEFLEC") //Deflectors
CONST_INT MOD_NAME_JUMP		HASH("TOP_JUMP")	// Jump
CONST_INT MOD_NAME_SPIKE	HASH("TOP_SPIKE")	// Spike
CONST_INT MOD_NAME_SCOOP	HASH("TOP_SCOOPS")	// Scoop
CONST_INT MOD_NAME_BLADES	HASH("TOP_BLADES")	// Blades
CONST_INT MOD_NAME_DECORATIONS HASH("TOP_DECORATIONS") // Decorations
CONST_INT MOD_NAME_TOP_WIBBLE HASH("TOP_TWIBBLE") // Top wibble
CONST_INT MOD_NAME_REAR_WIBBLE HASH("TOP_RWIBBLE") // Rear wibble
CONST_INT MOD_NAME_BODY_PLATING HASH("TOP_BPLATING") //Body plating
CONST_INT MOD_NAME_WIND_ARMOR HASH("TOP_WARMOR") //Windscreen armor
CONST_INT MOD_NAME_UNDERPLATING HASH("TOP_UPLATING") //Undercarriage plating
CONST_INT MOD_NAME_BUMPER_EXTENSION HASH("TOP_BUMPEX") //Bumper Extension
CONST_INT MOD_NAME_WIND_DELFECTOR HASH("TOP_WIND_DF") //Wind deflector 
CONST_INT MOD_NAME_VORTEX_GEN HASH("CMOD_VORT_M") //Vortex Generators
CONST_INT MOD_NAME_ROOF_ACC HASH("CMOD_ROOF_ACC") //Roof Accessories 
CONST_INT MOD_NAME_HOOD_ACC HASH("TOP_HOOD_ACC") //hood Accessories 
CONST_INT MOD_NAME_TAIL_FIN HASH("TOP_TAILFIN") //Tail Fin
CONST_INT MOD_NAME_ROOF_PANEL HASH("TOP_ROOFPNL") //Roof panel
CONST_INT MOD_NAME_WINDOW_SPOILER HASH("TOP_RWING") //Window spoiler
CONST_INT MOD_NAME_CANARDS HASH("TOP_CANARDS") //Canards
CONST_INT MOD_NAME_HOOD_LIP HASH("TOP_HOODLIP") //Hood Lip
CONST_INT MOD_NAME_PANNIERS HASH("TOP_PANN") // Panniers
CONST_INT MOD_NAME_STORAGE_BOX HASH("TOP_STORAGEBOX") //Storage box
#IF FEATURE_COPS_N_CROOKS
CONST_INT MOD_NAME_SEARCH_LIGHT HASH("TOP_SEARCHLIGHT") //Search Light
CONST_INT MOD_NAME_CRASHBARS HASH("TOP_ROLLBAR") //Crash bars
#ENDIF
CONST_INT MOD_NAME_LADDER HASH("TOP_LADDER") //Ladder 
CONST_INT MOD_NAME_TRUNK_CATCHES HASH("TOP_TRUNKCATCH") // Trunk Catches
CONST_INT MOD_NAME_SUNSHADES HASH("TOP_SSHADES") //Sun Shades
CONST_INT MOD_NAME_SKID_PLATE HASH("TOP_SKID") // Skid plate
CONST_INT MOD_NAME_REAR_DOOR HASH("TOP_REAR_DOOR") // Trunk Door 
CONST_INT MOD_NAME_ROOF_SCOOP HASH("TOP_ROOFSC") // Roof Scoop
CONST_INT MOD_NAME_NOSE_PANEL HASH("TOP_NOSE") // Nose panel
CONST_INT MOD_NAME_NOSE_FIN HASH("TOP_NOSEFIN") // Nose Fin
CONST_INT MOD_NAME_ROOF_FIN HASH("TOP_ROOFFIN") // Roof Fin
CONST_INT MOD_NAME_FRONT_SHIELD HASH("TOP_SHIELD") // Front sheild
CONST_INT MOD_NAME_GRIP HASH("TOP_GRIP") // Grip
CONST_INT MOD_NAME_FOGLI HASH("TOP_FOGL")	// Foglight
CONST_INT MOD_NAME_VENTS HASH("TOP_VENT")	// Vents
CONST_INT MOD_NAME_INTERCOOLER HASH("TOP_INTERCOOL")	// Intercoolers
CONST_INT MOD_NAME_INTERIORTYPE HASH("TOP_INTERTYPE")	// Interior Type
CONST_INT MOD_NAME_VENT_BRACES HASH("TOP_VENTBRAC")	// Vent Brtaces
CONST_INT MOD_NAME_REAR_SPLITTER HASH("TOP_RSPLIT") // Rear Splitter
CONST_INT MOD_NAME_LIP_SPOILER HASH("TOP_LSPOIL") // Lip Spoiler
CONST_INT MOD_NAME_RAIL_COVERS HASH("TOP_RAILCOV") // Rail Covers
CONST_INT MOD_NAME_TAIL_LIGHTS HASH("TOP_TALIGHT") // Tail Lights
#IF FEATURE_GEN9_EXCLUSIVE
CONST_INT MOD_NAME_HSW_TURBO HASH("TOP_HSWTURBO") // HSW Turbo
CONST_INT MOD_NAME_HSW_UPGRADE HASH("TOP_HSWUPGRADE") // HSW Upgrade
#ENDIF
CONST_INT MOD_NAME_REMOTE_UNIT HASH("TOP_IMANI_TECH") // Imani's tech
CONST_INT MOD_NAME_BODY_TRIM HASH("TOP_BTRIM")	// Body Trim
CONST_INT MOD_NAME_ROOF_SPOILER HASH("TOP_RUFSPOILER")	// Roof Spoiler
CONST_INT MOD_NAME_SIDE_LOUVRES HASH("CMOD_SLOU_N") //Side Louvres
CONST_INT MOD_NAME_ROOF_RAILS HASH("TOP_ROOFRAIL") //Roof Rails


// ******************************************************************************************************************************************************
// ******************************************************************************************************************************************************
// ******************************************************************************************************************************************************

BOOL g_bSkipSwitchForBootLoad = TRUE

//MPGlobalsAmbienceStruct_OLD MPGlobalsAmbience_Old

//VOICE_SESSION_SCRIPT_STRUCT_OLD 	voiceSession_Old

//For the sweet stuntjumps	
STUNTJUMP_ID 	g_sjsIndex[ciMAX_STUNT_JUMPS]

MP_PROPERTY_EXTERIOR_HIDE mpPropExtTest

VEHICLE_STATS_FOR_CLASS vehicleStats

FRIEND_CREW_DATA globalFriendCrewData[FRIEND_CREW_DATA_MAX_CREWS]

#IF FEATURE_GEN9_EXCLUSIVE
BOOL g_bForceRespawnPV = FALSE
BOOL g_bTriggerRespawnPV = FALSE
BOOL g_bHasForcedPVRespawnFinished = FALSE

SCRIPT_TIMER g_stForcedPVRespawnDelay
#ENDIF

MP_SAVED_VEHICLE_STRUCT g_MpSavedVehicles[MAX_MP_SAVED_VEHICLES]
INT g_iPCVehInsuranceCounter

BOOL g_bCleanupLeftoverAirPV = TRUE

BOOL g_bOverrideWalkingStyle = FALSE

CHALLENGE_DPAD_STRUCT g_GBLeaderboardStruct
THE_LEADERBOARD_STRUCT g_DpadDownLeaderBoardData[NUM_NETWORK_PLAYERS]

ENUM VEHICLE_SPAWN_QUEUE_ENUM
	 eVeh_spawn_0_NONE
	,eVeh_spawn_1_PERSONAL
	,eVeh_spawn_2_PEGASUS
	,eVeh_spawn_3_TRUCK
	,eVeh_spawn_4_AIRCRAFT
	,eVeh_spawn_5_HACKERTRUCK
	,eVeh_spawn_6_SUBMARINE
	,eVeh_spawn_7_SUBMARINE_DINGHY
	#IF FEATURE_DLC_2_2022
	,eVeh_spawn_8_ACIDLAB
	,eVeh_spawn_9_SUPPORT_BIKE
	#ENDIF
ENDENUM
VEHICLE_SPAWN_QUEUE_ENUM g_eCachedVehicleSpawnQueue = eVeh_spawn_0_NONE

INT iContrabanTypRandomPos = -1
INT iContrabandTypesHistoryList[ciContrabandTypeHistoryListSize]

INT g_iSellBitSet
INT g_iFactoryPaidResupplyTimers[ciMAX_OWNED_FACTORIES]
INT g_iFactorySetupDelayTimers[ciMAX_OWNED_FACTORIES]
INT g_iFactoryDefendMissionTimers[ciMAX_OWNED_FACTORIES]
INT g_iFactoryDefendMissionSaveTime							//Timer to prevent us saving defend timer stats too often
INT g_iFactoryTimeStageCounter = 1
INT g_iFactoryFakeCashOpenDryer = 0
INT g_iMaterialsTotalForFactory[ciMAX_OWNED_FACTORIES]
INT g_iFactoryStaffArrivedTxtMsg
INT g_iFactoryStoppedProduction
INT g_iFactoryProductionLJTCalls
BOOL g_bFactorySetupComplete = FALSE
BOOL g_bWeedFactoryDoneRemoveProduct = FALSE
BOOL g_bMemorialWallUpdate
BOOL g_bBlockFactoryDefendMissionTimers = FALSE
SCRIPT_TIMER stBikerFactoryUpgradeCallTimer
BOOL g_bProductionShouldBeHalted[ciMAX_OWNED_FACTORIES]
SCRIPT_TIMER g_stBunkerPaidSupplyMessageTO				//Timeout before we stop trying to send the player a message about their supplies
#IF IS_DEBUG_BUILD
BOOL g_bFactoryWidgetEnabled
BOOL g_bFactoryForceActiveProduction
BOOL g_bFactoryHideUI
#ENDIF

INT g_iWarehouseVehicleSpawnOrder[ciMAX_IE_OWNED_VEHICLES]

BOOL g_bUsingShootingRange = FALSE
BOOL g_bShootingRangeUpgraded = FALSE
BOOL g_bFlashBunkerSuppliesBar = FALSE
VEHICLE_INDEX g_viGunRunTruck
VEHICLE_INDEX g_viGunRunTruckTailer
VEHICLE_INDEX g_viGunRunTruckTailer2
VEHICLE_INDEX g_viGunRunTruckTailerIamIn
VEHICLE_INDEX g_viGunRunTruckCabIamIn
VEHICLE_INDEX g_viGunRunTruckCabRemote
VEHICLE_INDEX g_viGunRunTruckTrailerRemote
VEHICLE_INDEX g_viGunRunTruckInBunker
VEHICLE_INDEX g_viGunRunTailerInBunker
VEHICLE_INDEX g_viGangOpsAvengerInDefunctBase
VEHICLE_INDEX g_viHackerTruckInBusinessHub
#IF FEATURE_DLC_2_2022
VEHICLE_INDEX g_viAcidLabInJuggaloWarehouse
VEHICLE_INDEX g_viSupportBikeInJuggaloWarehouse
#ENDIF
INT g_iFactoryProductionAgentFourteenTexts
SCRIPT_TIMER stBunkerFactoryCallTimer
INT	g_iTruckRadioStation
INT	g_iAvengerRadioStation

SCRIPT_TIMER stBunkerResCompleteDelay //10 second timer before unlocking a reward

VEHICLE_INDEX g_viAvengerIamIn
VEHICLE_INDEX g_viAvengerRemoteVehIndex
VEHICLE_INDEX g_viHackerTruckRemoteVehIndex
BOOL g_bStarterPackMetricSentCharSlotOne = FALSE
BOOL g_bStarterPackMetricSentCharSlotTwo = FALSE

VEHICLE_INDEX g_viHackerTruckIamIn
INT g_iNightclubSafeHelpText
INT g_iNightclubSafeTextMSG
BOOL g_bHackerTruckBlipBlue = FALSE
BOOL g_bHackerTruckLaptopBlipGreen = FALSE

#IF FEATURE_DLC_2_2022
BOOL g_bAcidLabBlipBlue = FALSE
VEHICLE_INDEX g_viAcidLabIamIn
VEHICLE_INDEX g_viAcidLabRemoteVehIndex
#ENDIF

#IF FEATURE_HEIST_ISLAND
VEHICLE_INDEX g_viSubmarineIamIn
#ENDIF

INT g_iprivate_club_request_PIM 
INT g_crowd_club_request_PIM

#IF FEATURE_CASINO_HEIST
INT g_iprivate_arcade_request_PIM 
INT g_iPassiveIncomePayoutBS// PASSIVE_INCOME_PAYOUT_BIT_SET_ENUM
#ENDIF

// TODO: Look at setting up a generic arcade game active flag that can disable player menu
BOOL g_bIsPlayerPlayingDegenatron = FALSE
BOOL g_bIsPlayerPlayingWizardsRuin = FALSE
BOOL g_bIsPlayerPlayingArcadeGame = FALSE

//Global for use within missions/events
#IF IS_DEBUG_BUILD
ENUM TRUCK_SCANNER_WIDGET_STAGE
	TSWS_INIT,
	TSWS_MAINTAIN
ENDENUM

STRUCT HACKER_TRUCK_SCANNER_DEBUG
	TRUCK_SCANNER_WIDGET_STAGE eWidgetStage
	INT iSetCamIndex
ENDSTRUCT
#ENDIF //IS_DEBUG_BUILD

ENUM HACKER_TRUCK_CAM_SOUNDS
	HTCS_ZOOM,
	HTCS_ROTATE,
	HTCS_BACKGROUND,
	
	HTCS_COUNT
ENDENUM

ENUM HACKER_TRUCK_CAM
	HTC_FRONT,
	HTC_BACK,
	
	HTC_COUNT
ENDENUM

ENUM HACKER_TRUCK_SCANNER_STATE
	HTSS_WAITING_FOR_TRUCK,
	HTSS_IDLE,
	HTSS_IN_CAM,
	HTSS_CLEANUP_CAM
ENDENUM

STRUCT HACKER_TRUCK_CAM_AUDIO
	INT iSounds[HTCS_COUNT]
ENDSTRUCT	

STRUCT SCANNER_AUDIO_STRUCT
	INT iSFXBackgroundLoop = -1
	INT iSFXScanLoop = -1
	INT iSFXScanResult = -1
ENDSTRUCT

CONST_INT MAX_SCANNER_VEHS 16


STRUCT VEHICLE_SCANNER_STRUCT
	VEHICLE_INDEX vehID[MAX_SCANNER_VEHS]
	INT iVehicleScannerBitSet[MAX_SCANNER_VEHS]
	INT iCurrentScanVeh = -1
	SCRIPT_TIMER ScanTimer
	SCRIPT_TIMER ScanningTextTimer
	SCRIPT_TIMER ResultsTimer
	SCRIPT_TIMER LosBreakTimer
	BOOL bShowingResults = FALSE
	//STRUCT_SHAPE_TEST_DATA sShapeTestData
	BOOL bStartedShapeTest = FALSE
	BOOL bLoadedTextureDict = FALSE
	BOOL bDrawScanText = TRUE
	
	SCANNER_AUDIO_STRUCT sAudio
ENDSTRUCT


STRUCT COMBO_SCANNER_STRUCT
	INT iBsPlayerHasBeenScanned			//Each bit represents whether the player of that index has been scanned already.					
	INT iBs
	

	
	PLAYER_INDEX piCurrentScanTarget	
	
	TIME_DATATYPE tScanTime				//ScanTimer
	TIME_DATATYPE tScanTextTime			//ScanningTextTimer
	TIME_DATATYPE tResultsTime			//ResultsTimer
	TIME_DATATYPE tLosBreakTime			//tLosBreakTime
	TIME_DATATYPE tNewTargetTime		//Time that we found a target closer to centre screen
	TIME_DATATYPE tOutOfRangeTime		//Time the current target got out of range
	
	CAMERA_INDEX camera 				//Camera we are using as scanner
	SCANNER_AUDIO_STRUCT 	sAudio		//Audio data for scanning
	SCALEFORM_INDEX			scaleform
	FLOAT 					fScanRange = -1.0
	
	VEHICLE_SCANNER_STRUCT  sVehicleScannerStruct
ENDSTRUCT

STRUCT HACKER_TRUCK_SCANNER_STRUCT
	// Scanner
	COMBO_SCANNER_STRUCT 	comboScanner
	
	// General
	VEHICLE_INDEX			viHackerScanVeh
	//INT						iContextIndex	= -1	
	HACKER_TRUCK_SCANNER_STATE eState
	INT iBs
	
	// Camera
	HACKER_TRUCK_CAM		eCamIndex
	CAMERA_INDEX			ciHackerScanCam
	HACKER_TRUCK_CAM_AUDIO	camAudio
	VECTOR					vCamRotation
	structPedsForConversation tutPedStruct
	
	INT iScannerSoundID 	= -1
	
	SCRIPT_TIMER 			stInputDelay
	SCRIPT_TIMER 			stRumbleTimer
	SCRIPT_TIMER			stDisableRecording
	// Widgets
	#IF IS_DEBUG_BUILD
	HACKER_TRUCK_SCANNER_DEBUG debug
	#ENDIF //IS_DEBUG_BUILD
ENDSTRUCT

HACKER_TRUCK_SCANNER_STRUCT g_hackerTruckScanner

SCRIPT_TIMER st_NightclubRadioDelay
INT g_iNightclubRadioDelayMS = 500
BOOL g_bNightClubRadioDelayNeeded
BOOL g_bNCIntroCleanupInProgress
BOOL g_bForceNightclubRadioToRestart
BOOL g_bHoldDJForAudioDelay

BOOL g_bPlayerExitedSeatNormally

BOOL g_bRestrictedInteriorCleanupFreemode = TRUE

BOOL g_bIgnoreGarageCheckMorsZero

#IF FEATURE_HEIST_ISLAND
BOOL g_bSpawnLSIAArrivalGangVehicle
#ENDIF
//EOF
