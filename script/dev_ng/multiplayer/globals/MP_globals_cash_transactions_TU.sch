/**********************************************************
/ Name: 		MP_globals_cash_transactions_TU.sch
/ Author(s): 	James Adwick
/ Purpose: 		Declaration of cash transaction globals
***********************************************************/

USING "MP_globals_cash_transactions_definitions.sch"

// Declaration of the global transaction data
CASH_TRANSACTIONS_DETAILS g_cashTransactionData[SHOPPING_TRANSACTION_MAX_NUMBER+1]	// TODO: Need to use const int that Miguel is adding for max transactions at once
CASH_TRANSACTIONS_DETAILS_CACHE g_cashTransactionDataCached[SHOPPING_TRANSACTION_MAX_NUMBER+1]
CASH_TRANSACTION_EVENT_DETAILS g_sUnknownCashTransactionEventData

BOOL g_bValidateCashTransactionEvent = TRUE

BOOL g_bPendingCashTransactionCommunicatingWithServer = FALSE

INT g_iTransactionFrameDelay_Basket = 1
INT g_iTransactionFrameDelay_Service = 1

INT g_LastActiveBuyContrabandMissionKey = 0
INT g_LastActiveFactoryBuyMissionKey 	= 0
INT g_LastActiveHangarBuyMissionKey 	= 0
INT g_iArenaSpectatorBoxEntryPricePaid	= 0

GENERIC_TRANSACTION_STATE g_eCasinoMembershipPurchase = TRANSACTION_STATE_DEFAULT

#IF FEATURE_FREEMODE_ARCADE
ENUM TRANSACTION_CURRENCY
	CURRENCY_DOLLARS
	#IF FEATURE_COPS_N_CROOKS
	,CURRENCY_CNC_TOKENS
	#ENDIF
ENDENUM
#ENDIF

#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 g_TempCatalogKey
	TEXT_LABEL_63 g_TransactionDebug_error
	INT g_TransactionDebug_id
	INT g_TransactionDebug_frame
	TEXT_LABEL_63 g_TransactionDebug_type
	TEXT_LABEL_63 g_TransactionDebug_action
	TEXT_LABEL_63 g_TransactionDebug_flags
	TEXT_LABEL_63 g_TransactionDebug_service
	TEXT_LABEL_63 g_TransactionDebug_category
	INT g_TransactionDebug_cost
	INT g_TransactionDebug_itemID
	INT g_TransactionDebug_extraItemID
	
	BOOL g_TransactionDebug_BlockEndService
	BOOL g_TransactionDebug_BlockBasketEnd
	
	BOOL bTriggerEarnTransaction
	INT iEarnTransactionID
	
	BOOL g_TransactionDebug_ForceSuccess
#ENDIF
