USING "rage_builtins.sch"
USING "types.sch"

USING "MP_Globals_Common_Definitions.sch"		// For NUM_NETWORK_PLAYERS




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_player_headshots.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains globals and structs for all Player Headshot generation and maintenance.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


// ===========================================================================================================
//      General Headshot globals
// ===========================================================================================================

// MP Headshot details
STRUCT g_structHeadshotDetailsMP
	BOOL			hsdActive		= FALSE			// TRUE if there is an active request for a Headshot (either requested or already generated), FALSE if not
	PLAYER_INDEX	hsdPlayerID						// Player Index of the player for which a headshot is being generated
	PEDHEADSHOT_ID	hsdHeadshotID					// The Headshot ID for the active request
	TIME_DATATYPE	hsdTimeout						// Ensures the request times out after a period of time to save it hanging around for ever
	BOOL			hsdKeepActive	= FALSE			// Set to TRUE when a request has been made to keep this headshot active, after its scheduled update this will become FALSE again

ENDSTRUCT


// ===========================================================================================================
//      Generated Headshots globals
// ===========================================================================================================

// The minimum time a headshot will stick around for once generated (it can stick around longer if required by a script)
CONST_INT	MP_HEADSHOT_INITIAL_RETAIN_TIME_msec		300000

// Additional Time a headshot will stick around for if required by script
CONST_INT	MP_HEADSHOT_ADDITIONAL_RETAIN_TIME_msec		30000

// The maximum number of requested or generated headshots
CONST_INT	MAX_MP_HEADSHOTS							NUM_NETWORK_PLAYERS

// Used if there is no available headshotID slot
CONST_INT	NO_HEADSHOT_SLOT							-1

// The default delay used if the regenerate headshot for this player should be delayed
CONST_INT	HEADSHOT_REGENERATION_REQUEST_DEFAULT_DELAY_msec	5000


// -----------------------------------------------------------------------------------------------------------

// An array of already generated headshots
g_structHeadshotDetailsMP	g_sGeneratedHeadshotsMP[MAX_MP_HEADSHOTS]
INT							g_numGeneratedHeadshotsMP	= 0


// -----------------------------------------------------------------------------------------------------------

// A scheduling variable for maintaining generated headshots
INT							g_nextHeadshotUpdateSlotMP	= 0




// ===========================================================================================================
//      Player Headshot Request globals
// ===========================================================================================================

// The minimum time a headshot will stick around for once generated (it can stick around longer if required by a script)
CONST_INT	MP_HEADSHOT_GENERATION_TIMEOUT_msec			60000


// -----------------------------------------------------------------------------------------------------------

// One 'generating' slot - used while a headshot is being generated
g_structHeadshotDetailsMP	g_sHeadshotBeingGeneratedMP


// -----------------------------------------------------------------------------------------------------------

// An array of headshot generation requests pending
PLAYER_INDEX				g_sPendingHeadshotsMP[MAX_MP_HEADSHOTS]
INT							g_sNumPendingHeadshotsMP	= 0



// ===========================================================================================================
//      Transparent Headshot Request globals
// ===========================================================================================================

// One 'generating' slot - used while a headshot is being generated
g_structHeadshotDetailsMP	g_sTransparentHeadshotMP


// -----------------------------------------------------------------------------------------------------------

// A headshot generation request
PLAYER_INDEX				g_sPendingTransparentHeadshotsMP


// ===========================================================================================================
//      Export Player Headshot to Social Club globals
// ===========================================================================================================
// The minimum time before we give up on exporting a headshot
CONST_INT	MP_EXPORT_PLAYER_HEADSHOT_TIMEOUT_msec						60000	//1min
TIME_DATATYPE	g_sExportPlayerHeadshotTimeout
TIME_DATATYPE	g_sExportPlayerHeadshotCountdown
BOOL			g_sExportPlayerHeadshotRequested						= FALSE
INT				g_sExportPlayerHeadshotStage							= -1
BOOL			g_bUseCachedTransparentLocalPlayerHeadshot[2]
BOOL			g_bReduceHeadshotUploadFrequency 						= TRUE
BOOL			g_bPlayerHeadshotNeedsUpdating 							= FALSE
BOOL			g_bNewPlayerHeadshotNeedsCreated 						= FALSE

#IF IS_DEBUG_BUILD
INT 			g_sDebugExportPlayerHeadshotTimeToExport				= 0
BOOL			g_sDebugExportPlayerStartExportPlayerHeadshotTimer 		= FALSE
BOOL			g_sDebugExportPlayerForceImmediateExportPlayerHeadshot	= FALSE
#ENDIF

// ===========================================================================================================
//      Pre-Generate and Keep All Headshots globals
// ===========================================================================================================

// A scheduling INT that loops through all Player Indexes (one per frame) and keeps that player's headshot active
// This ensures headshots get generated as a player enters the game and then remains active throughout

INT							g_scheduledPlayerHeadshotMaintenance	= 0

// Flagged by the selector HUD if it can't find a transparent headshot to display in the cloud.
// This will trigger off the timer to generate a new shot.
BOOL						g_bGenerateInitialTransparentHeadshot	= FALSE



// ===========================================================================================================
//      Delayed Headshot Re-Generation globals
// ===========================================================================================================

INT g_delayedHeadshotRegenerationTimeout = 0


// ===========================================================================================================
//      HeadShot needs refreshed due to clubhouse change
// ===========================================================================================================

INT g_PedShotHighRes_Bitset = 0
INT g_PedShotHighRes_NumberActive = 0

