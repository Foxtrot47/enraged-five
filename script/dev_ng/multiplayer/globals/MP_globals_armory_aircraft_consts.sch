////////////////////////////////////////////
///    
///    Constants for ArmoryAircraft
///

USING "mp_globals_new_features_tu.sch"


CONST_INT ARMORY_AIRCRAFT_ROOM_KEY		-25693127

ENUM ARMORY_AIRCRAFT_MOD_ENUM
	AAM_GUN_TURRET,
	AAM_WEAPON_WORKSHOP,
	AAM_VEHICLE_WORKSHOP,
	AAM_INTERIOR,
	
	AAM_MAX_ARMORY_AIRCRAFT_MODS
ENDENUM

