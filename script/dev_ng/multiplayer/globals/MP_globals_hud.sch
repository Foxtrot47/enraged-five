
USING "rage_builtins.sch"

USING "mp_globals_hud_definitions.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_hud.sch
//      DESCRIPTION     :   Contains global variable instances for all MP hud routines.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


//BOOL bDisplaygangmembersonmissionbox 
THREADID g_tiTransitionThread

INT flag_display_UPDATE_INVENTORY_BOX_CONTENT[7]

BOOL g_dPadDownButtonPress = FALSE
//BOOL g_dPadSecondPage = FALSE
CONST_INT MAX_DPAD_PLAYERS_ONE_PAGE 16

#IF IS_DEBUG_BUILD
	INT g_MAX_FM_ACTIVITY_XP_WIDGET = -1
#ENDIF

// stuff for the Inventroy purse
#IF IS_DEBUG_BUILD
	INT number_of_inventory_columns = 1
	INT Inventory_type_widget
	BOOL WIDGET_INVENTORY_ON_OFF
	BOOL RESET_WIDGET_INVENTORY
	eMP_TAG WIDGET_INVENTORY_ITEM
#ENDIF

VECTOR vCustomQuickGPSCoords

PROGRESSHUD_ELEMENTS g_ePriorityHudElement[2]

//eSCRIPT_HUD_COMPONENT OLD_scaleform_inventory[7] 
eSCRIPT_HUD_COMPONENT scaleform_inventory[7] 

	
eMP_TAG ARRAY_OF_INVENTORY_SPRITE_ENUMS[7]
BOOL spec_cam_inventory_achieved[7]
INT spec_cam_quantities[7]


inventory_ped_name_following_player ARRAY_OF_INVENTORY_CHARACTER_NAMES[7]
MP_inventory_vehicle_type ARRAY_OF_VEHICLE_TYPE[7]
	
	
BOOL spec_hud_display_drug_hud = FALSE
BOOL is_drug_hud_activated = FALSE
BOOL is_inventory_hud_active = FALSE
INT is_slot_on_display[7]


TRANSITION_HUDCOGS g_Transition_ScreenPlace


SCALEFORM_GALLERY_WEAPON g_EmptyGalleryWeaponStruct
SCALEFORM_GALLERY_WEAPON g_GalleryWeaponStruct


SCALEFORMXML_GALLERY_WEAPON g_EmptyGalleryXMLWeaponStruct
SCALEFORMXML_GALLERY_WEAPON g_GalleryXMLWeaponStruct


SCALEFORMXML_GALLERY g_GalleryXMLMedalStruct


SCALEFORMXML_PLAYERLIST g_EmptyPlayerListXMLStruct
SCALEFORMXML_PLAYERLIST g_PlayerListXMLStruct


#IF IS_DEBUG_BUILD
	FLOAT fRankUpOffset = 0.4
	FLOAT fRankUpMultiplier = 0.05
#ENDIF


ENUM eDPAD_TYPE
	eDPAD_TYPE_FM = 0,
	eDPAD_TYPE_CHALLENGE,
	eDPAD_TYPE_NOT_USED
ENDENUM

STRUCT DPAD_TYPE_DATA
	eDPAD_TYPE eDpadTypeToDraw
	eDPAD_TYPE eDpadTypeToDrawLastFrame
	BOOL bDrawingChallenge
ENDSTRUCT
DPAD_TYPE_DATA g_sDpadTypeToDraw

 // #IF FEATURE_NEW_AMBIENT_EVENTS


// KGM 26/8/11: Converted the remaining #defines to #IF, so TUNE_MISSION_OVER_SCREEN must exist but be set to 0 if the functionality is not required
// NOTE: To activate this debug functionality, change the value of TUNE_MISSION_OVER_SCREEN to be 1
CONST_INT TUNE_MISSION_OVER_SCREEN	0
#IF TUNE_MISSION_OVER_SCREEN
	FLOAT	fFailReasonMultPerLine = 0.035
	FLOAT	fFailReasonBackgroundBase = 0.275
	FLOAT	fFailReasonTopBaseBuffer = 0.01
	FLOAT	fFailReasonYPos = 0.287
	INT 	iFailString = 0
	FLOAT	fScoreTableBackgroundOffset = 0.178
	FLOAT	fScoreTableHeight = 0.405
	FLOAT	fScoreTableBasePos = 0.395
	FLOAT	fScoreTableFailOffsetBasePos = 0.024
	FLOAT 	fScoreTableLineOffset = 0.025
	FLOAT 	fScoreTableCol1X = 0.056
	FLOAT 	fScoreTableCol2X = 0.2
	FLOAT 	fScoreTableHeaderBreak = 0.005
#ENDIF


MPGlobalsHudStruct MPGlobalsHud
MPGlobalsScoreHudStruct MPGlobalsScoreHud
//MPGlobalsHudPositionStruct_old MPGlobalsHudPosition_old
MPGlobalsHudRewardsStruct MPGlobalsHudRewards
MPGlobalsHudRewardsPLAYERStruct MPGlobalsHudRewardsPLAYER


NET_FRIEND_DATA FriendSwappingData


#IF IS_DEBUG_BUILD
	INT g_iHeistRoleOverRide		= -1
	BOOL g_iCheatPlayPhoneCall
	FLOAT g_fBadSportLevelDisplayLesWidget
	INT g_iBadSportLevelIncreaseLesWidget
	BOOL g_iBadSportPlayPhoneCall

BOOL g_bTurnOnBadSportWidgets
#ENDIF


BOOL g_DressedPed_CrewTshirt[MAX_NUM_CHARACTER_SLOTS]


OVER_HEADS_GLOBAL_STRUCT g_sOverHead

CONST_INT LOOT_BAG_UI_MAX_ELEMENTS 5

STRUCT LOOT_BAG_ELEMENT
	STRING sLabel
	FLOAT fSpaceUsedPerecnt
	HUDORDER eHUDOrder = HUDORDER_DONTCARE
ENDSTRUCT

STRUCT LOOT_BAG_UI_DATA
	LOOT_BAG_ELEMENT element[LOOT_BAG_UI_MAX_ELEMENTS]
	INT iItems
	FLOAT fSpaceFilledPercent
	STRING sBagLabel
	HUDORDER eBagHUDOrder = HUDORDER_DONTCARE
ENDSTRUCT


// =============================================================================================================
//      Mission Locate Message Globals
//		18/9/12: Added by Keith to control the three Scaleform movies that can display Mission Names in a corona
// =============================================================================================================

// Struct containing Details for each available Locate Message scaleform movie
g_structLocateMessageMP	g_sLocateMessagesMP[MAX_LOCATE_MESSAGE_MOVIES]

g_structXPDescription g_sXPDescription


// ----- EOF 
