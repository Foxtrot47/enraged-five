


CONST_INT AD_DEFAULT						0

CONST_INT AD_LOW_DS 						0
CONST_INT AD_LOW_PS 						1

CONST_INT AD_HIGH_DS 						2
CONST_INT AD_HIGH_PS 						3
CONST_INT AD_HIGH_DS_REAR 					4
CONST_INT AD_HIGH_PS_REAR 					5

CONST_INT AD_BOHDI_DS 						6
CONST_INT AD_BOHDI_PS 						7
CONST_INT AD_BOHDI_DS_REAR 					8
CONST_INT AD_BOHDI_PS_REAR 					9

CONST_INT AD_FULL_BODY_M					10
CONST_INT AD_FULL_BODY_F					11

CONST_INT AD_SELFIE							12

CONST_INT MAX_NUM_INTERACTION_ANIM_DICTS 	13

CONST_INT FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE	 	-3
CONST_INT FORCED_BONEMASK_NO_FILTER					-2
CONST_INT FORCED_BONEMASK_NONE						-1
CONST_INT FORCED_BONEMASK_HEADONLY					0
CONST_INT FORCED_BONEMASK_HEAD_NECK_AND_ARMS		1
CONST_INT FORCED_BONEMASK_HEAD_NECK_AND_L_ARM		2
CONST_INT FORCED_BONEMASK_HEAD_NECK_AND_R_ARM		3

CONST_INT DOUBLE_TAP_INTERVAL 500

CONST_INT MAX_NUMBER_OF_SOUNDS 4


ENUM INTERACTION_ANIM_STAGE
	IAS_INTRO,
	IAS_BODY,
	IAS_OUTRO,
	IAS_FULL_BODY
ENDENUM


CONST_INT SIS_POST_NONE								-1

CONST_INT SIS_POST_SERIES_A_COKE 						0
CONST_INT SIS_POST_SERIES_A_TRASH						1
CONST_INT SIS_POST_SERIES_A_BIKERS						2
CONST_INT SIS_POST_SERIES_A_WEED						3
CONST_INT SIS_POST_SERIES_A_METH						4
CONST_INT SIS_POST_SERIES_A_FINALE						5
CONST_INT SIS_POST_FLEECA_SCORE_CAR						6
CONST_INT SIS_POST_FLEECA_SCORE_FINALE					7
CONST_INT SIS_POST_PRISON_BREAK_PLANE					8
CONST_INT SIS_POST_PRISON_STATION_CASCO					9
CONST_INT SIS_POST_PRISON_STATION_COPS					10
CONST_INT SIS_POST_PRISON_WET_WORK						11
CONST_INT SIS_POST_PRISON_BREAK_BUS						12
CONST_INT SIS_POST_HUMANE_INSURGENTS					13
CONST_INT SIS_POST_HUMANE_STEAL_EMP						14
CONST_INT SIS_POST_HUMANE_DELIVER_EMP					15
CONST_INT SIS_POST_HUMANE_STEAL_VALKYRIE				16
CONST_INT SIS_POST_PACIFIC_STANDARD_PHOTO_TRUCKS		17
CONST_INT SIS_POST_PACIFIC_STANDARD_HACK				18
CONST_INT SIS_POST_PACIFIC_STANDARD_WITSEC				19
CONST_INT SIS_POST_PACIFIC_STANDARD_CONVOY				20
CONST_INT SIS_POST_PACIFIC_STANDARD_BIKES				21
CONST_INT SIS_POST_APARTMENT							22 // unused
CONST_INT SIS_POST_STRIP_CLUB							23 // unused
CONST_INT SIS_POST_FLEECA_SCORE_FINALE_FALLBACK			24
CONST_INT SIS_POST_PACIFIC_STANDARD_FINALE				25
CONST_INT SIS_POST_PACIFIC_STANDARD_FINALE_FALLB		26
CONST_INT SIS_POST_PACIFIC_STANDARD_HACK_DECOY			27

CONST_INT SIS_POST_LOWRIDER_MISSION_1					28
CONST_INT SIS_POST_LOWRIDER_MISSION_2					29
CONST_INT SIS_POST_LOWRIDER_MISSION_3					30
CONST_INT SIS_POST_LOWRIDER_MISSION_4					31
CONST_INT SIS_POST_LOWRIDER_MISSION_5					32
CONST_INT SIS_POST_LOWRIDER_MISSION_6					33
CONST_INT SIS_POST_LOWRIDER_MISSION_7					34
CONST_INT SIS_POST_LOWRIDER_MISSION_8					35

CONST_INT SIS_POST_BENNY_GARAGE							36

CONST_INT SIS_SVM_BLAZER5								37
CONST_INT SIS_SVM_BOXVILLE5								38
CONST_INT SIS_SVM_DUNE4									39
CONST_INT SIS_SVM_PHANTOM2								40
CONST_INT SIS_SVM_RUINER2								41
CONST_INT SIS_SVM_TECHNICAL2							42
CONST_INT SIS_SVM_VOLTIC2								43
CONST_INT SIS_SVM_WASTELANDER							44

CONST_INT SIS_WVM_APC									45
CONST_INT SIS_WVM_DUNE3									46
CONST_INT SIS_WVM_HALFTRACK								47
CONST_INT SIS_WVM_OPPRESSOR								48
CONST_INT SIS_WVM_TAMPA3								49
CONST_INT SIS_WVM_TRAILER								50
CONST_INT SIS_WVM_BIGRIG								51

CONST_INT SIS_POST_IAA_MORGUE							52
CONST_INT SIS_POST_IAA_DELUXO							53
CONST_INT SIS_POST_IAA_SERVER_FARM						54
CONST_INT SIS_POST_IAA_JOB								55
CONST_INT SIS_POST_SUB_AVENGER							56
CONST_INT SIS_POST_SUB_FOUNDRY							57
CONST_INT SIS_POST_SUB_RCV								58
CONST_INT SIS_POST_SUB_STROMBERG						59
CONST_INT SIS_POST_SUB_JOB								60
CONST_INT SIS_POST_SILO_PREDATOR						61
CONST_INT SIS_POST_SILO_CHENOBOG						62
CONST_INT SIS_POST_SILO_BC_CUSTOM						63
CONST_INT SIS_POST_SILO_PANTHER							64
CONST_INT SIS_POST_SILO_SPY_PLANE						65
CONST_INT SIS_POST_SILO_JOB								66

CONST_INT SIS_POST_CASINO_MISSION_1						67
CONST_INT SIS_POST_CASINO_MISSION_2						68
CONST_INT SIS_POST_CASINO_MISSION_3						69
CONST_INT SIS_POST_CASINO_MISSION_4						70
CONST_INT SIS_POST_CASINO_MISSION_5						71
CONST_INT SIS_POST_CASINO_MISSION_6						72

CONST_INT SIS_POST_CASINO_HEIST							73
CONST_INT SIS_POST_CASINO_HEIST_2						74

CONST_INT SIS_POST_ISLAND_HEIST							75
//TUNER
CONST_INT SIS_POST_ROBBERY_UNION_DEPOSITORY_1			76
CONST_INT SIS_POST_ROBBERY_UNION_DEPOSITORY_2			77
CONST_INT SIS_POST_ROBBERY_MILITARY_CONVOY_1			78
CONST_INT SIS_POST_ROBBERY_MILITARY_CONVOY_2			79
CONST_INT SIS_POST_ROBBERY_FLEECA_1						80
CONST_INT SIS_POST_ROBBERY_FLEECA_2						81
CONST_INT SIS_POST_ROBBERY_FREIGHT_TRAIN_1				82
CONST_INT SIS_POST_ROBBERY_FREIGHT_TRAIN_2				83
CONST_INT SIS_POST_ROBBERY_BOLINGBROKE_1				84
CONST_INT SIS_POST_ROBBERY_BOLINGBROKE_2				85
CONST_INT SIS_POST_ROBBERY_IAA_RAID_1					86
CONST_INT SIS_POST_ROBBERY_IAA_RAID_2					87
CONST_INT SIS_POST_ROBBERY_METAAMPHETAMINE_1			88
CONST_INT SIS_POST_ROBBERY_METAAMPHETAMINE_2			89
CONST_INT SIS_POST_ROBBERY_BUNKER_1						90
CONST_INT SIS_POST_DATA_LEAK_INTRO_GOLF_MISSION			91
CONST_INT SIS_FIXER_AGENCY_PROPERTY_HAWICK				92
CONST_INT SIS_FIXER_AGENCY_PROPERTY_ROCKFORD			93
CONST_INT SIS_FIXER_AGENCY_PROPERTY_LITTLE_SEOUL		94
CONST_INT SIS_FIXER_AGENCY_PROPERTY_VESPUCCI			95
CONST_INT SIS_FIXER_FIRE_IN_BOOTH						96
CONST_INT SIS_FIXER_DONT_FUCK_WITH_DRE					97
#IF FEATURE_DLC_1_2022
CONST_INT SIS_SUM22_ULP_1_INTELLIGENCE					98
CONST_INT SIS_SUM22_ULP_2_COUNTERINTELLIGENCE			99
CONST_INT SIS_SUM22_ULP_3_EXTRACTION					100
CONST_INT SIS_SUM22_ULP_4_ASSET_SEIZURE					101
CONST_INT SIS_SUM22_ULP_5_OPERATION_PAPER_TRAIL			102
CONST_INT SIS_SUM22_ULP_6_CLEANUP						103
#ENDIF


#IF FEATURE_DLC_1_2022
CONST_INT TOTAL_SYNCED_INTERACTION_SCENES				104
#ENDIF
#IF NOT FEATURE_DLC_1_2022
CONST_INT TOTAL_SYNCED_INTERACTION_SCENES				98
#ENDIF

CONST_INT MAX_SYNCED_INTERACTION_COMBO_BOX_ENTRIES		79


CONST_INT NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS 3

CONST_INT CAM_PAN_CUSTOM 	0
CONST_INT CAM_PAN_RIGHT 	1
CONST_INT CAM_PAN_LEFT		2

CONST_FLOAT CAMERA_SHAKE_AMPLITUDE 0.1

STRUCT SYNCED_INTERACTION_SCENE
	VECTOR vActorCoords[4]
	FLOAT fActorHeading[4]
	
	VECTOR vPosition[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	FLOAT fHeading[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	INT iLayout[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	VECTOR vCamStartPos[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	VECTOR vCamStartRot[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	VECTOR vCamEndPos[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	VECTOR vCamEndRot[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	FLOAT fCamStartFOV[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	FLOAT fCamEndFOV[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	FLOAT fShakeAmplitude[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	//INT iCamDuration[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	INT iCamEndTime[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	INT iCamPanState[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS] // 0 = custom, 1 right, 2 left.
	BOOL bIsAFinale
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 strDebugName
	BOOL bOutputDetailsToFile[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	BOOL bGrabPosition[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	BOOL bGrabStartCam[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	BOOL bGrabEndCam[NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS]
	#ENDIF
ENDSTRUCT

STRUCT PLAYER_INTERACTION_BROADCAST_DATA
	BOOL bReadyToStart
	BOOL bAbandon
	INT iUniqueID = -1
	INT iSceneID = -1
	INT iPlayerNamesHash[4]
	INT iPlayerInSyncSceneHash[4]
	INT iBS_LocationIsBlockedByGangAttackOrMission
	INT iFullSyncSceneID = -1
	NETWORK_INDEX iSyncSceneObjectID[2]
	#IF IS_DEBUG_BUILD
		INT iForceLocation = -1
		INT iForceActor = -1
	#ENDIF
ENDSTRUCT

STRUCT SERVER_INTERACTION_BROADCAST_DATA
	INT iUniqueID[NUM_NETWORK_PLAYERS]
	INT iSceneID[NUM_NETWORK_PLAYERS]
	INT iLocation[NUM_NETWORK_PLAYERS]
	INT iActor[NUM_NETWORK_PLAYERS]
ENDSTRUCT


STRUCT INTERACTION_ANIM_DATA_OLD
	INT iType
	INT iAnim
	
	BOOL bIsDriverSide
	BOOL bIsLowVehicle
	BOOL bIsRearSeat
	BOOL bIsSideFacing
	BOOL bIsDriver
	BOOL bIsRearBohdi
	BOOL bCreateProp
	BOOL bDeleteProp
	BOOL bJustFinishedIntro		
	BOOL bObjectCreated		
	BOOL bHitMaxIterations
	
	INT iIteration 
	
	OBJECT_INDEX ObjectID
	NETWORK_INDEX NetworkObjectID
	PTFX_ID PTFXID_Loop	
	SEQUENCE_INDEX Seq
	
	PED_INDEX PedID	
ENDSTRUCT


STRUCT INTERACTION_INTERNAL_DATA_OLD

	INTERACTION_ANIM_DATA_OLD SelectedInteraction
	INTERACTION_ANIM_DATA_OLD PlayingInteraction
	INTERACTION_ANIM_DATA_OLD LastInteraction
	
	BOOL bHasUnloadedLastSelectedAnim
	BOOL bPlayingSelectedAnim
	BOOL bHasGrabbedCarInfo
	

ENDSTRUCT

STRUCT INTERACTION_SOUND_DATA

//	BOOL bPlayIntroSound = FALSE
//	TEXT_LABEL_63 strIntroSound
//	FLOAT fIntroSoundTime
//	
//	BOOL bPlayLoopSound = FALSE
//	TEXT_LABEL_63 strLoopSound
//	FLOAT fLoopSoundTime
//	
//	BOOL bPlayLoopSound2 = FALSE
//	FLOAT fLoopSoundTime2
//	
//	BOOL bPlayOutroSound = FALSE
//	TEXT_LABEL_63 strOutroSound
//	FLOAT fOutroSoundTime
//	
//	BOOL bHasPlayedIntroSound
//	BOOL bHasPlayedLoopSound
//	BOOL bHasPlayedLoopSound2
//	BOOL bHasPlayedOutroSound	
	
	BOOL bPlaySound[MAX_NUMBER_OF_SOUNDS]
	BOOL bHasPlayedSound[MAX_NUMBER_OF_SOUNDS]
	TEXT_LABEL_63 strSound[MAX_NUMBER_OF_SOUNDS]
	FLOAT fSoundTime[MAX_NUMBER_OF_SOUNDS]
	INTERACTION_ANIM_STAGE SoundAnimStage[MAX_NUMBER_OF_SOUNDS]
	FLOAT fLastPhase = 0.0
	
ENDSTRUCT

STRUCT CUSTOM_OBJECT_ANIM_DATA
	MODEL_NAMES ObjectModel = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vCoords
	VECTOR vRotation
	TEXT_LABEL_63 strAnimName	
ENDSTRUCT


STRUCT INTERACTION_DATA
	
	TEXT_LABEL_63 strAnimDict[MAX_NUM_INTERACTION_ANIM_DICTS]
	TEXT_LABEL_63 strAnimNameIntro
	TEXT_LABEL_63 strAnimName
	TEXT_LABEL_63 strAnimNameOutro
	TEXT_LABEL_63 strAnimNameFullBody
	
	TEXT_LABEL_63 strAnimNameIntro2
	TEXT_LABEL_63 strAnimName2
	TEXT_LABEL_63 strAnimNameOutro2
	
	TEXT_LABEL_63 strAnimNameIntro3
	TEXT_LABEL_63 strAnimName3
	TEXT_LABEL_63 strAnimNameOutro3	
	#IF FEATURE_DLC_1_2022
	TEXT_LABEL_63 strSpeechContext
	TEXT_LABEL_63 strSpeechVoiceName
	TEXT_LABEL_63 strSpeechParams
	
	BOOL bSpeechOnly
	#ENDIF
	BOOL bLeftHandedOnly
	BOOL bHasProp
	BOOL bBlockForceProp
	BOOL bDeletePropAtEnd
	BOOL bUsesPTFX
	BOOL bForceLeftHandAttach
	BOOL bForceRightHandAttach
	BOOL bIsLooped
	BOOL bCanSkipLoop
	BOOL bCanQuitLoopEarly
	BOOL bHasCooldown
	
	MODEL_NAMES PropModel = DUMMY_MODEL_FOR_SCRIPT
	
	INT iMinLoopIterations

	BOOL bPropHasAttachedPTFX
	TEXT_LABEL_63 strPropPTFX
	VECTOR vPropPTFXOffset
	VECTOR vPropPTFXRotation
	
	BOOL bHasLoopMouthPTFX
	TEXT_LABEL_63 strLoopPTFXMouth
	
	BOOL bHasLoopNosePTFX
	TEXT_LABEL_63 strLoopPTFXNose
	
	INT iLoopEventDrunk
	
	INT iForcedAnimFilter = FORCED_BONEMASK_NONE
	
	//BOOL bUseFinerSoundDetails = FALSE
	BOOL bUseSounds = FALSE
	
	//BOOL bRemoveClosedMaskHelmet
	
	FLOAT fLastPhase
	
	FLOAT fEnterAnimStartPhaseForInNightclub
ENDSTRUCT


STRUCT INTERACTION_ANIM_DATA
	INT iType = -1
	INT iAnim = -1
	
	BOOL bIsDriverSide
	BOOL bIsLowVehicle
	BOOL bIsRearSeat
	BOOL bIsSideFacing
	BOOL bIsDriver
	BOOL bIsBohdi
	BOOL bIsMale
	
	BOOL bCreateProp
	BOOL bDeleteProp
	BOOL bJustFinishedIntro		
	BOOL bObjectCreated		
	BOOL bHitMaxIterations
	BOOL bHasDoneMinIterations
	BOOL bHasDecrementedStat
	
	INTERACTION_SOUND_DATA SoundData
	
	BOOL bPlayingNoFilterWhenStanding
	INTERACTION_ANIM_STAGE AnimStage
	BOOL bSlowBlendBodyOutro
	BOOL bSelfieMode
	BOOL bFirstPerson
	
	INT iAnimFlags
	
	INT iIteration 
	
	FLOAT fLastBlendOutAnimPhase
	FLOAT fStartPhase
	BOOL bIsPaused
	BOOL bStarted
	
	OBJECT_INDEX ObjectID
	NETWORK_INDEX NetworkObjectID
	NETWORK_INDEX NetworkObjectIDPrevious
	OBJECT_INDEX ObjectIDPrevious
	PTFX_ID PTFXID_Loop	
	SEQUENCE_INDEX Seq
	VECTOR vObjectCoordsLastFrame
	
	PED_INDEX PedID	
	
	WEAPON_TYPE StoredWeapon = WEAPONTYPE_UNARMED
	
	//OBJECT_INDEX CustomObjectID
	
	INTERACTION_DATA Data
	BOOL bForceProp
	
	BOOL bSwappedMidItem = TRUE
	BOOL bSwappedLastItem = TRUE
ENDSTRUCT


STRUCT INTERACTION_INTERNAL_DATA

	INTERACTION_ANIM_DATA SelectedInteraction
	INTERACTION_ANIM_DATA PlayingInteraction
	INTERACTION_ANIM_DATA LastInteraction
	
	BOOL bHasUnloadedLastSelectedAnim
	BOOL bPlayingSelectedAnim
	BOOL bHasGrabbedCarInfo
	BOOL bPlayingFullBody
	BOOL bPlayingSelfie
	//BOOL bPlayingFullBodyMask

ENDSTRUCT




STRUCT INTERACTION_GLOBAL_DATA_OLD
	INT iInteractionType
	INT iInteractionAnim
	BOOL bPlayInteractionAnim
	BOOL bCleanupInteractionAnim
	BOOL bCleanupInteractionAnimImmediately
	BOOL bInteractionIsPlaying
	BOOL bHoldLoop
	INT iAlcoholShots
	INT iCurrentAlcoholShots
	INT iHealthChange
ENDSTRUCT


STRUCT MPGlobalsInteractionsStruct_OLD

	// Interaction anims	
	INTERACTION_GLOBAL_DATA_OLD  PlayerInteraction
	INTERACTION_GLOBAL_DATA_OLD  QuickplayInteraction
	INTERACTION_GLOBAL_DATA_OLD  QuickplayInteractionVeh
	INTERACTION_GLOBAL_DATA_OLD  PedInteraction[NUM_NETWORK_PLAYERS]
	
	PTFX_ID pfx_smoking[NUM_NETWORK_PLAYERS]
	INT iBS_SmokingPFXPlaying
	
	BOOL bAnyInteractionIsPlaying
	INT iMyCrewInteractionAnim
	BOOL bQuickplayButtonPressed
	BOOL bUpdateQuickplayAnim
	BOOL bUpdateQuickplayAnimVeh	

	#IF IS_DEBUG_BUILD
		BOOL bUseDebugFilterAndFlags
		INT iDebugPlayAnimFilter = 3
		INT iDebugPlayAnimFlags = 134283344
		BOOL bGiveCiggy
		BOOL bRemoveCiggys
		
		BOOL bGiveDrink1
		BOOL bRemoveDrink1
		
		BOOL bGiveDrink2
		BOOL bRemoveDrink2
		
		BOOL bGiveDrink3
		BOOL bRemoveDrink3
		
		BOOL bGiveFood1
		BOOL bRemoveFood1

		BOOL bGiveFood2
		BOOL bRemoveFood2
		
		BOOL bGiveFood3
		BOOL bRemoveFood3
		
	#ENDIF
	
ENDSTRUCT


STRUCT INTERACTION_GLOBAL_DATA
	INT iInteractionType = -1
	INT iInteractionAnim = -1
	
	INT iFlags
	
	BOOL bPlayInteractionAnim
	BOOL bPlayInteractionAnimFullBody
	BOOL bPlayInteractionSelfie
	BOOL bCleanupInteractionAnim
	BOOL bCleanupInteractionAnimImmediately
	BOOL bInteractionIsPlaying
	BOOL bHoldLoop
	BOOL bHasCleanedUp
	BOOL bStartedIntroTime
	BOOL bIsWearingHelmentInVehicle
	BOOL bWaitForPlayerControl
	BOOL bWaitForSync
	BOOL bWaitForFadingIn
	BOOL bForceWeaponVisible
	BOOL bIgnoreStatCheck
	BOOL bCreateLocalProps
	
	FLOAT fStartPhase
	
	INT iAlcoholShots
	INT iCurrentAlcoholShots
	INT iHealthChange
	
	TIME_DATATYPE ActiveTime
	TIME_DATATYPE IntroTime
	
ENDSTRUCT

ENUM POINTING_INTERACTION_STATE
	POINT_INTERACTION_NULL,
	POINT_INTERACTION_LOADING,
	POINT_INTERACTION_INTRO,
	POINT_INTERACTION_LOOP,
	POINT_INTERACTION_OUTRO
ENDENUM

INT g_iCurrentOnFootQuickplayType = -1
INT g_iCurrentOnFootQuickplayAnim = -1
INT g_iCurrentInVehQuickplayAnim = -1

STRUCT MPGlobalsInteractionsStruct

	// Interaction anims	
	INTERACTION_GLOBAL_DATA  PlayerInteraction
	INTERACTION_GLOBAL_DATA  QuickplayInteraction
	INTERACTION_GLOBAL_DATA  QuickplayInteractionVeh
	INTERACTION_GLOBAL_DATA  PedInteraction[NUM_NETWORK_PLAYERS]
	
	PTFX_ID pfx_smoking[NUM_NETWORK_PLAYERS]
	INT iBS_SmokingPFXPlaying
	
	BOOL bAnyInteractionIsPlaying
	BOOL bInteractionIsPlayingForQuickRestart
	BOOL bInteractionAssetsAreLoading
	INT iCurrentlyPlayingType = -1
	INT iCurrentlyPlayingAnim = -1
	
	INT iMyCrewInteractionAnim
	
	TIME_DATATYPE timeQuickplayPress
	BOOL bQuickplayButtonPressed
	BOOL bQuickplayDoubleTap
	//BOOL bUpdateQuickplayAnim
	//BOOL bUpdateQuickplayAnimVeh
	SCRIPT_CONTROL_HOLD_TIMER sQuickStartControlTimer
	
	//  pointing
	TIME_DATATYPE timePointingPress
	BOOL bPointingButtonPressed
	BOOL bPointingDoubleTap
	WEAPON_TYPE PointingStoredWeapon = WEAPONTYPE_UNARMED
	BOOL bPointingBlockingHelmetRemoval
	
	INT iLoadedType = -1
	INT iLoadedAnim = -1
	
	POINTING_INTERACTION_STATE PointingState = POINT_INTERACTION_NULL
	FLOAT fPreviousPointingHeading
	BOOL bDoPointingInterp
	BOOL bRequestedMoveStateTransition
	TIME_DATATYPE PointingTimer
	TIME_DATATYPE PointingStartTime
	
	TIME_DATATYPE timePlayerMoves
	TIME_DATATYPE timePlayerNonIdle
	
	eSpecialPickupState ePickupState
	
	BOOL bHasBeenSynced
	
	BOOL bClearCustomCarNodes
	TIME_DATATYPE timeClearCustomCarNodes
	
	INT iSyncedInteractionPlayerToProcess
	INT iSyncedInteractionState = -1
	TIME_DATATYPE SyncedInteractionStartTime
	BOOL bFirstPersonFlashTriggered
	VECTOR vSyncedInteractionCoords
	FLOAT fSyncedInteractionHeading
	CAMERA_INDEX SyncInteractionCam
	BOOL bUseClonePlayers = TRUE
	PED_INDEX ClonePedID[4]	
	//INT iFullSyncSceneID
	INT iSyncSceneHost
	BOOL bSyncSceneSuccessfullyStarted
	OBJECT_INDEX FullSyncSceneObjectID[2]
	SCENARIO_BLOCKING_INDEX ScenarioBlockingID
	BOOL bRestoreCelebrationAudioStage
	INT iPreCutsceneID = -1
	INT iPreCutsceneState = 0
	
	VEHICLE_INDEX preCutsceneVeh
	VECTOR preCutscenePlayerPosition
	VECTOR preCutscenePlayerRotation
	
	BOOL bDisabledThisFrame
	BOOL bQuickRestartAnim
	BOOL bThisIsAQuickRestartAnim
	BOOL bSkipInteractionAnimIntroDelay = FALSE
	BOOL bSimulate_QuickplayButtonPressed
	INT iPreLoadQuickplay = -1
	BOOL bDoPreLoadQuickplay
	
	#IF IS_DEBUG_BUILD
	
		INT iDebugStartType = -1
		INT iDebugStartAnimInCar = -1
		INT iDebugStartAnimCrew = -1
		INT iDebugStartAnimPlayer = -1
		INT iDebugStartAnimSpecial = -1
		BOOL bDebugCallCommmandInCar
		BOOL bDebugCallCommmandCrew
		BOOL bDebugCallCommmandPlayer
		BOOL bDebugCallCommmandSpecial
		BOOL bDebugCallCommand_SET_NEXT_RESPAWN_INTERACTION
		BOOL bDebugHoldLoop=FALSE 
		BOOL bDebugDoSnackCheck=TRUE
		BOOL bDebugFullBody=FALSE
		BOOL bDebugWaitUntilPlayerControl=FALSE
		BOOL bDebugWaitUntilSync=FALSE
		FLOAT fDebugStartPhase = 0.0
		BOOL bDebugSkipInteractionAnimIntroDelay
		BOOL bDebugWaitForFadingIn
		BOOL bDebugForceWeaponVisible
		BOOL bDebugIgnoreStatCheck
		
		BOOL bDebugCallCommandCleanupImmediately
		BOOL bDebugCallCommand_STOP_INTERACTION_ANIM
		
		BOOL bUseDebugFilterAndFlags
		INT iDebugPlayAnimFilter = 3
		INT iDebugPlayAnimFlags = 134283344
		BOOL bGiveCiggy
		BOOL bRemoveCiggys
		
		BOOL bGiveDrink1
		BOOL bRemoveDrink1
		
		BOOL bGiveDrink2
		BOOL bRemoveDrink2
		
		BOOL bGiveDrink3
		BOOL bRemoveDrink3
		
		BOOL bGiveFood1
		BOOL bRemoveFood1

		BOOL bGiveFood2
		BOOL bRemoveFood2
		
		BOOL bGiveFood3
		BOOL bRemoveFood3
		
		BOOL bSimulateEarlyExitEvent

		// synced interactions
		INT iSyncedInteractionOutput_SceneID
		BOOL bOutputSyncedInteractionData		
		BOOL bStartSyncedInteractionWithAllPlayersInSession
		BOOL bStartSyncedInteractionLocallyOnly
		BOOL bSyncInteractionSnapToFirstCam
		BOOL bSyncInteractionSnapToEndCam
		FLOAT fSyncStoredShakeAmplitute
		TIME_DATATYPE StoredSyncTime
		//INT iDebug_SyncedSceneUniqueID
		INT iDebug_SyncedSceneSceneID_actual
		INT iDebug_SyncedSceneSceneID_previous
		INT iDebug_SyncedSceneSceneID2_previous
		INT iDebug_SyncedSceneSceneID
		INT iDebug_SyncedSceneSceneID2
		INT iDebug_SyncedSceneLocationID = -1
		INT iDebug_SyncedSceneActor = -1
		BOOL bDebug_TurnOnCameraEditing
		BOOL bUpdateSyncedCamPans
		BOOL bDebug_SyncedSceneFailToGetLocation
		BOOL bDebug_FillSceneWithPeds
		PED_INDEX DebugPedID[3]
		INT iDebugPedActor[3]
		BOOL bTestBail1
		BOOL bTestBail2
		BOOL bTestBail3
		
		BOOL bSimulate_QuickplayButtonHeld
		BOOL bSimulate_PressedR1
		BOOL bSimulate_PressedL1
		
		BOOL bPinInterior
		INTERIOR_INSTANCE_INDEX debug_InteriorInstance
		
	#ENDIF
	
ENDSTRUCT


