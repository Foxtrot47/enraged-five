USING "model_enums.sch"
USING "rel_groups.sch"
USING "commands_hud.sch"
USING "charsheet_global_definitions.sch"

ENUM TEAM_TYPE
	TEAM_TYPE_COP,
	TEAM_TYPE_CRIMINAL,
	TEAM_TYPE_NON_ACTIVE
ENDENUM

STRUCT TEAM_DATA
	
	//BLIP_SPRITE blipHighlighted
	BLIP_SPRITE blipSafehouse
	
	BOOL bPlayerCanCollectDroppedMoney
	BOOL bHasGotGarages
	
	TEAM_TYPE TeamType
	
	enumCharacterList charBoss
	
	INT iVehicleColour
	INT iSpeakerID
	
	MODEL_NAMES ModelMale
	MODEL_NAMES ModelFemale
	MODEL_NAMES ModelVehicle
	MODEL_NAMES ModelLieutenent
	
	REL_GROUP_HASH relGroup
	
	TEXT_LABEL_15 str_Plural_AppendThe_StartOfSentence
	TEXT_LABEL_15 str_Plural_AppendThe_StartOfSentence_Capitalised
	TEXT_LABEL_15 str_Plural_AppendThe
	TEXT_LABEL_15 str_Plural_AppendThe_Capitalised
	TEXT_LABEL_15 str_Plural_HudColour
	TEXT_LABEL_15 str_Plural_HudColour_Capitalised
	TEXT_LABEL_15 str_Plural
	TEXT_LABEL_15 str_Plural_Capitalised
	TEXT_LABEL_15 str_Singular_HudColour
	TEXT_LABEL_15 str_Singular_HudColour_Capitalised
	TEXT_LABEL_15 str_Singular
	TEXT_LABEL_15 str_Singular_Capitalised	
	TEXT_LABEL_15 str_TeamSpeakerName
	TEXT_LABEL_31 str_OverheadImageSprite
	TEXT_LABEL_15 str_SafehouseBlipName
	TEXT_LABEL str_TimeCycleModifierName

	#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 str_TeamNameForDebug
	#ENDIF

ENDSTRUCT 
