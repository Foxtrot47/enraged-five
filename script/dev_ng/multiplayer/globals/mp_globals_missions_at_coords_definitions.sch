USING "rage_builtins.sch"

USING "mp_globals_mission_control_definitions.sch"		// For g_eMPMissionSource

USING "commands_hud.sch"					// For BLIP_SPRITE

USING "net_mission_enums.sch"				// For MP_MISSION
USING "mp_mission_data_struct.sch"			// For NO_MISSION_VARIATION
USING "mp_icons.sch"						// For eMP_TAG_SCRIPT for corona augmented icons


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_missions_at_coords.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains constant, enum, and struct definitions for all MP missions at coords routines.
//							NB. Should not contain instantiated variables. 
//		NOTES			:	For efficiency there will be a multi-tiered update system based on the coords distance from the player.
//
//							There will be one Focus Mission that is treated as closest to the player - this mission will be the
//							one with an active vote and registered with the Mission Controller. It may also be the current active
//							mission. Until the player moves away from this mission and tidies up the active elements, no other
//							mission can take its place even if the player moves closer to another mission.
//
//							The remainder of the missions will be placed in the array in a very rough 'distance from player' order.
//							This will be based on distance bands. When a mission gets its maintenance update the distance from the
//							player will be calculated and, if the distance crosses a threshold, the mission will be moved up or down
//							the array to sit with other missions that are within the same distance band from the player.
//							EG: Array Position 0 will contain the Focus Mission, Array Positions 1 - (for eg) 5 could contain all the
//							missions currently within (say) 20m of the player and have the potential to become the focus mission.
//							Array Positions 6 - 13 could contain all missions within (say) 50m of the player, and array positions
//							14 and above could all be missions greater than 50m away. Within each band in the array there is no
//							sorting based on distance, only when a mission crosses into a different band will it's position change
//							within the array.
//
//							The nearest missions will be updated every frame or using a very tight update schedule - these missions
//							have the potential to become the Focus Mission. The remaining missions will be updated periodically to
//							minimise processing.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      General Missions At Coords globals
// ===========================================================================================================

// These are the default distances the mission coords need to be from a player to trigger a range change
// NOTE: Initial assumption is that all missions will use the same ranges

CONST_FLOAT		MATC_RANGE_LEEWAY						  0.50		// Once in range of a boundary, the player will only be classed as out of range again when out of the boundary + this leeway
CONST_FLOAT		MATC_RANGE_MINIMUM_TRIGGER_RADIUS_m		  0.5		// A safety check - a mission probably shouldn't have a triggering radius smaller than this
CONST_FLOAT		MATC_RANGE_DEFAULT_FOCUS_RANGE_m		  2.6		// The distance within which the mission tries to become the one focal mission - for mission reservation, voting, starting, etc
CONST_FLOAT		MATC_RANGE_ADDITION_FOR_NAMED_RANGE_m	  6.0		// The distance beyond the triggering radius within which the mission name will be displayed inside the corona
CONST_FLOAT		MATC_RANGE_DEFAULT_CORONA_RANGE_m		130.00		// The distance within which the mission corona will be displayed
CONST_FLOAT		MATC_RANGE_CLOSE_TO_DISPLAY_RANGE_m		800.00		// A distance getting closer to the corona being displayed to allow faster reactions (KGM 15/9/15 [BUG 2034824]: A general increase in the number of missions at once on NG)


// -----------------------------------------------------------------------------------------------------------

// Invalid MissionAtCoords slot
CONST_INT	INVALID_MATC_SLOT						-1

// Illegal Missions At Coords Registration ID
CONST_INT	ILLEGAL_AT_COORDS_ID					-1

// MatC Speed Zone Controls
CONST_INT	ILLEGAL_MATC_SPEED_ZONE_ID				-1
CONST_INT	MAX_MATC_SPEED_ZONES					2
CONST_FLOAT	MATC_SPEED_ZONE_MAX_SPEED				0.0

// Matc Scenario Blocking Controls
CONST_INT	MAX_MATC_SCENARIO_BLOCKING				2

// Scheduling timeout when checking for old entries in the Feedback array
CONST_INT	MATC_FEEDBACK_CHECK_OLDEST_TIME_msec	1000

// A default distance (set to the VALUE, check if greater than CHECK - this is to allow for FLOAT rounding inaccuracies)
CONST_FLOAT	INVALID_MATC_RANGE_MAX_VALUE			99999.0
CONST_FLOAT	INVALID_MATC_RANGE_CHECK				90000.0

// Clear All Bits, and check for all bits being clear
CONST_INT	ALL_MISSIONS_AT_COORDS_BITS_CLEAR		0

// Blip Flash Time when first displayed
CONST_INT	MATC_BLIP_FLASH_TIME_msec				10000

// Number of MissionsAtCoords Slots required
CONST_INT	MAX_NUM_MP_MISSIONS_AT_COORDS			1200	// R*Created + R*Verified + MiniGames + UGC + Bookmarked + Shared (1100+200+24+32+32+32=1418) - contact missions (est. 200), rounded to 1200.

// InCorona Settle Timeout - this covers a brief delay between the external script indicating the mission has loaded and the player being warped to the edge of the corona
CONST_INT	INCORONA_ACTIONS_SETTLE_DELAY_msec		2000

// InCorona Settle Timeout - Waits for the player being picked up by the corona before listening for walk out, this needs to be large to cover JIPing from SP, etc
CONST_INT	INCORONA_ACTIONS_CORONA_PICKUP_SETTLE_DELAY_msec	120000

// Incorona Frame Timeout - as above but doesn't rely on hte network timer which can jump during a transition
CONST_INT	INCORONA_ACTIONS_CORONA_SETTLE_DELAY_frames			30

// Incorona Check if Registered As Shared Mission timeout - allows contual attempts at registration in case the host bombs out and doesn't process registration
CONST_INT	INCORONA_ACTIONS_CHECK_SHARED_DELAY_msec	2000

// Initial Actions Scheduled Timeout Delay
CONST_INT	INITIAL_ACTIONS_SCHEDULING_DELAY_msec	5000

// A cosmetic leeway in addition to the corona radius used for non-active radius functionality
// EG: The initial player spawn blockage, the speed zone radius, etc.
CONST_FLOAT	CORONA_GENERAL_COSMETIC_LEEWAY_m		2.0

// A height range that the player needs to be inside in order to trigger the corona
CONST_FLOAT	CORONA_HEIGHT_LEEWAY_ABOVE_m			3.0
CONST_FLOAT	CORONA_HEIGHT_LEEWAY_BELOW_m			-1.0

// Coronas
CONST_INT		MATC_CORONA_ACTIVE_ALPHA			255			// The Alpha Value for an active corona
CONST_INT		MATC_CORONA_INACTIVE_ALPHA			100			// The Alpha Value for an inactive corona
CONST_FLOAT		MATC_CORONA_INACTIVE_HEIGHT_m		1.0			// Inactive Corona display height
CONST_FLOAT		MATC_CORONA_ACTIVE_HEIGHT_m			5.0			// Active Corona display height for spinning texture
CONST_FLOAT		MATC_CORONA_BELOW_GROUND_m			0.2			// How much of the corona is below ground
CONST_FLOAT		MATC_CORONA_DRAW_SIZE_MODIFIER		1.0			// Corona Radius * 2 * DRAW_SIZE_MODIFER = corona diameter
CONST_FLOAT		MATC_CORONA_LIGHTS_ABOVE_GROUND_m	1.7			// The position of the light source at a corona location


// Corona Icons
CONST_FLOAT	CORONA_ICON_HEIGHT_m					1.7
CONST_FLOAT	CORONA_ICON_FAR_RANGE_START_m			MATC_RANGE_DEFAULT_CORONA_RANGE_m
CONST_FLOAT	CORONA_ICON_FADE_IN_RANGE_m				1.0
CONST_FLOAT	CORONA_ICON_FADE_OUT_RANGE_m			2.0
CONST_FLOAT	CORONA_ICON_FADE_OUT_CROSSOVER_RANGE_m	0.0			// KGM 22/3/13: No Crossover required

// Corona Projections
CONST_INT		MATC_CORONA_PROJECTION_ALPHA			64		// The alpha value for corona ground projections
CONST_INT		MATC_CORONA_MAX_PROJECTIONS				3		// The maximum number of projections that can be on show at any one time (this is dictated by unique ground projection Checkpoint Types there are)
CONST_FLOAT		MATC_CORONA_PROJECTION_SIZE_MODIFIER	1.04	// When corona and projection use the same values, there is a slight gap between the corona and the ground projection - this will increase the projection to try to close the gap


// A value to represent a mission that is available ALL_DAY - this is a bitfield, so the first 24 bits are set, the remainder are cleared
// NOTE: I could have used bitwise OR here, but that relies of the CONST having an initial value of 0 - which it should have - but just in case it doesn't using PLUS ensures it gets set to a specific value
CONST_INT		MATC_ALL_DAY						(BIT0 + BIT1 + BIT2 + BIT3 + BIT4 + BIT5 + BIT6 + BIT7 + BIT8 + BIT9 + BIT10 + BIT11 + BIT12 + BIT13 + BIT14 + BIT15 + BIT16 + BIT17 + BIT18 + BIT19 + BIT20 + BIT21 + BIT22 + BIT23)

// The maximum number of Angled Area triggered missions that can be stored
CONST_INT		MAX_MATC_ANGLED_AREAS				85

// Angled Area blip values
CONST_FLOAT		ANGLED_AREA_BLIP_RADIUS_m			80.0
CONST_INT		ANGLED_AREA_BLIP_ALPHA				120
	
// A safety timeout to quit trying to reserve the mission and try again
// A decent delay - we really don't want this kicking in unless there's a real problem
// Cancelling and retrying and then having the original reservation be accepted is likely to cause just as many issues
CONST_INT		MATC_RESERVATION_SAFETY_TIMEOUT_msec				50000

// Safety Timeout waiting to be told the Cloud Mission Data has loaded
// Needs to be sufficiently long to prevent timing out in error
CONST_INT		MATC_WAITING_FOR_CLOUD_DATA_SAFETY_TIMEOUT_msec		180000

// The maximum number of flashing blips of each activity type that has just unlocked
CONST_INT		MATC_MAX_FLASHING_UNLOCKED_BLIPS_OF_TYPE			3

// The maximum number of flashing blips during any unlock
CONST_INT		MATC_MAX_FLASHING_BLIPS_AT_ONCE						12

// Safety Timeout time to remove a mission from being Invite Accepted
CONST_INT		MATC_INVITE_ACCEPTED_SAFETY_TIMEOUT_msec			(60000 * 5)

// KGM 21/2/15 [BUG 2238981]: Short Timeout to block Walk-In Focus Missions if the Joblist just cleared Wanted Level
CONST_INT		MATC_PLAYER_CLEARED_WANTED_LEVEL_DELAY_msec				3000
// KGM 12/3/15 [BUG 2271928]: Use extended Timeout to block Walk-In Focus Missions if the Joblist just cleared Wanted Level for a cross-session invite acceptance
CONST_INT		MATC_EXTENDED_PLAYER_CLEARED_WANTED_LEVEL_DELAY_msec	10000

// KGM 16/7/15 [BUG 2423429]: THe Active Gang Attack default radius
CONST_FLOAT		ACTIVE_GANG_ATTACK_DEFAULT_RADIUS_m					150.0




// ===========================================================================================================
//      Missions At Coords Initial Actions globals
//		Controls any actions that are required as a one-off when entering a new game
// ===========================================================================================================

// Controls the initial requirement of showing some long-range blips if there is nothing nearby when the player starts the game
// New struct in TU definitions
STRUCT g_structMatcInitialBlipsMP_OLD
	BOOL							matcibCompleted			= FALSE		// TRUE when the check for any initial long range blips has been performed, FALSE if still to be performed
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Controls the initial requirement of preventing a mission corona to appear at the players spawn location if the player is still there
STRUCT g_structMatcInitialSpawnMP
	BOOL							matcisCompleted			= FALSE		// TRUE when the check for any initial spawn location has been performed, FALSE if still to be performed
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Controls the initial requirement of using the expensive Player Rank check function
STRUCT g_structMatcInitialRankMP
	BOOL							matcirUseCheapFunction	= FALSE		// TRUE when the quick rank check has real data and we can stop using the expensive rank check, FALSE if still need to use expensive rank check
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Controls all initial actions
// Secondary struct in TU definitions, used to extend initial actions as necessary
STRUCT g_structMatcInitialActionsMP
	BOOL							matciaAllComplete		= FALSE		// TRUE when all initial actions have been completed, FALSE if some still need to be checked
	TIME_DATATYPE					matciaScheduledTimeout				// A scheduled timeout for checking these initial actions
	g_structMatcInitialBlipsMP_OLD	matciaLongRangeBlips				// Controls the initial long-range blips action (OBSOLETE: use has changed for TU so new struct in TU defintions)
	g_structMatcInitialSpawnMP		matciaSpawnCoords					// Controls preventing the player from triggering a mission that happens to be at the players initial spawn location
	g_structMatcInitialRankMP		matciaPlayerRank					// Controls switching from teh initial use of the expensive player rank check to the cheaper player rank check
ENDSTRUCT




// ===========================================================================================================
//      Missions At Coords Control globals
//		This is for any general control variables that don't belong to a specific mission, or band, or stage
// ===========================================================================================================

// The player's current activity - used to store data to avoid calling the same functions multiple times in a frame
STRUCT g_structMatCPlayerActivityMP
	BOOL			matcadOnActivity		= FALSE					// TRUE if the player is on an activity this frame, otherwise FALSE
	BOOL			matcadOnMission			= FALSE					// TRUE if the player is on or triggering a mission, otherwise FALSE
	BOOL			matcadActivelyOnMission	= FALSE					// TRUE if the player is actively on a mission, otherwise FALSE (ie: still performing pre-mission activities (cutscene/phonecall, etc))
	BOOL			matcadOnAmbient			= FALSE					// TRUE if the player's activity is an ambient activity, otherwise FALSE
	INT				matcadMissionUniqueID	= NO_UNIQUE_ID			// The uniqueID of the mission if the player is on one
	MP_MISSION		matcadMissionID			= eNULL_MISSION			// The MissionID of the mission if the player is on one
	INT				matcadVariation			= NO_MISSION_VARIATION	// The Mission Variation of the mission if the player is on one
	// NOTE: ContentID is stored in the Additional TU control variables struct
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Ground Projection stage IDs - this is required because the decal needs to be unpatched a frame after the checkpoint is deleted to avoid a nasty artifact being rendered on switch off
ENUM g_eGroundTXDRenderStage
	GTXD_STAGE_BEING_RENDERED,										// This ground projection is being rendered
	GTXD_STAGE_UNPATCH_DECAL,										// The decal can be unpatched (this is in the frame after it is no longer being rendered)
	GTXD_STAGE_ALLOW_REUSE_NEXT_FRAME,								// Introduces a one-frame delay before re-use (because it looks like code is asynchronous, so just trying to be safer)
	
	// Leave this at the bottom
	GTXD_STAGE_NOT_IN_USE											// This ground projection is not in use
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// A set of Corona GroundTXD Control variables
STRUCT g_structMatcCoronaGroundTXD
	g_eGroundTXDRenderStage	stage			= GTXD_STAGE_NOT_IN_USE		// The rendering stage for the ground projection
	INT						regID			= ILLEGAL_AT_COORDS_ID		// The RegID of the mission using this projection Texture
	CHECKPOINT_INDEX		checkpointID								// The checkpoint Index for the checkpoint being displayed
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// A set of Missions At Coords Control Variables
STRUCT g_structMatCControlsMP
	INT								matccNextRegID					= 0								// The registration ID that will be given to the next mission to be registered - this is on a local machine so can just increment forever
	INT								matccPlayerRank					= 0								// Holds the player rank - initially an expensive function is used until a cheaper function kicks in so the rest of MatC should just use this
	BOOL							matccBlipsOnDisplay				= FALSE							// TRUE if the blips are on display, FALSE if they have been en-masse switched off (ie: player is on a mission)
	BOOL							matccDownloadInProgress			= FALSE							// TRUE if the cloud-loaded data is being downloaded, otherwise FALSE
	BOOL							matccInitialDataReady			= FALSE							// TRUE if the initial data refresh has been completed and data is ready, FALSE if there is still no iniitla data
	BOOL							matccRefreshInProgress			= FALSE							// TRUE if the cloud-loaded data is in the process of being refreshed (include while being downloaded), otherwise FALSE
	BOOL							matccPlayerMovingFast			= FALSE							// TRUE if the player is moving fast on foot (run/sprint) - (NOTE: Only maintained when close to blips)
	BOOL							matccPlayerInInterior			= FALSE							// TRUE if the player is in an interior - used for updating Interiors Only blips
	BOOL							matccPlayerHasWantedLevel		= FALSE							// TRUE if the player has a wanted level - used for switching blips on and off
	BOOL							matccCoronaRequiresActiveTXD	= FALSE							// TRUE if any corona on this frame requires the active corona TXD
	BOOL							matccCoronaActiveTXDRequested	= FALSE							// TRUE if the corona Active TXD is being requested
	BOOL							matccCoronaActiveTXDLoaded		= FALSE							// TRUE if the corona Active TXD is loaded (when TRUE, the Requested flag should become FALSE)
	BOOL							matccCoronaGroundTXDRequested	= FALSE							// TRUE if the corona Ground Projection TXD is being requested
	BOOL							matccCoronaGroundTXDLoaded		= FALSE							// TRUE if the corona Ground TXD is loaded (when TRUE, the Requested flag should become FALSE)
	BOOL							matccForceUpdateRequested		= FALSE							// TRUE if any mission has requested a forced update on the next frame
	g_structMatcCoronaGroundTXD		matccCoronaGroundTXDControl[MATC_CORONA_MAX_PROJECTIONS]		// The Control Details of missions showing a Ground Projection TXD
	TIME_DATATYPE					matccFeedbackTimeout											// The Feedback Timeout for scheduling the next check for deleting an old entry in the array
	g_structMatCPlayerActivityMP	matccActivity													// The Player's current activity details this frame
ENDSTRUCT

// Additional Title Update Missions At Coords Control Variables
STRUCT g_structMatCControlsMP_TU
	TEXT_LABEL_23					matccActivityContentID											// Additional variable intended as part of the g_structMatCPlayerActivityMP
	BOOL							matccWasInApartment				= FALSE							// TRUE if the player was in an apartment last frame, FALSE if not
	INT								matccInviteAcceptContentIdHash	= 0								// The contentID Hash value of the activity the player has accepted an invite to
	INT								matccInviteAcceptTimeout		= 0								// A safety timeout to free up the Invite Accepted ContentID in case teh player never makes it into the corona
	INT								matccPaidToClearWantedTimeout	= 0								// KGM 21/2/15 [BUG 2238981]: Timeout to prevent a walk-in Focus Mission shortly after paying JOblist to clear wanted level
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_23				matccDebugInviteAcceptContentID									// DEBUG ONLY: Putting this here to keep it together with the contentIdHash variable
	#ENDIF
ENDSTRUCT




// ===========================================================================================================
//      Speed Zone Struct
//		Stores the details of speed zone restrictions in place
//		(NOTE: The array is not stored contiguously, so just find the first empty slot to add new data)
// ===========================================================================================================

STRUCT g_structMatCSpeedZonesMP
	INT								matcszRegID			= ILLEGAL_AT_COORDS_ID			// The AtCoords ID of a mission with a speed zone restriction in place
	INT								matcszSpeedZoneID	= ILLEGAL_MATC_SPEED_ZONE_ID	// The SpeedZone ID
ENDSTRUCT




// ===========================================================================================================
//      Scenario Blocking Struct
//		Stores the details of scenario blocking restrictions in place
//		(NOTE: The array is not stored contiguously, so just find the first empty slot to add new data)
// ===========================================================================================================

STRUCT g_structMatCScenarioBlockingMP
	INT								matcsbRegID					= ILLEGAL_AT_COORDS_ID			// The AtCoords ID of a mission with a scenario blocking restriction in place
	SCENARIO_BLOCKING_INDEX			matcsbScenarioBlockingID									// The Scenario Blocking Index
ENDSTRUCT




// ===========================================================================================================
//      Angled Area Struct
//		Stores the details of an Angled Area for mission triggering purposes
// ===========================================================================================================

STRUCT g_structMatCAngledAreaMP
	INT					matcaaRegID		= ILLEGAL_AT_COORDS_ID							// The MatcRegID for the mission that uses an Angled Area
	VECTOR				matcaaMin														// The minimum coords for the Angled Area
	VECTOR				matcaaMax														// The maximum coords for the Angled Area
	FLOAT				matcaaWidth		= 0.0											// The width of the angled area
// Graeme - we're going to have to move this to a different globals block so that it doesn't affect single player scripts
//	VECTOR				matcaaBlipPos													// The position of the blip within the Angled Area
ENDSTRUCT




// ===========================================================================================================
//      Flashing Blips Struct
//		Stores the details of blips that should flash in unison as activities unlock
// ===========================================================================================================

// STRUCT containing hte details of one blip to flash
STRUCT g_structMatcOneFlashingBlipMP
	MP_MISSION			matcofbMissionID	= eNULL_MISSION								// Mission ID of this blip
	FLOAT				matcofbDistance		= 0.0										// Distance from player
	BLIP_INDEX			matcofcBlipIndex												// Blip Index of the blip
ENDSTRUCT

// STRUCT containing details of all blips to flash
STRUCT g_structMatcAllFlashingBlipsMP
	BOOL							matcafbCollecting	= FALSE										// TRUE if collecting details of flashing blips (gets set to TRUE when an update finds the first blip that should flash)
	INT								matcafbFrameExpiry	= 0											// The frame count when flashing blip information should stop being gathered and the blips should flash
	g_structMatcOneFlashingBlipMP	matcafbBlips[MATC_MAX_FLASHING_BLIPS_AT_ONCE]					// All the flashing blips that need to flash when collection is over
ENDSTRUCT




// ===========================================================================================================
//      Focus Mission globals
//		Additional Variables required to control the one mission that is the current Focus Mission
// ===========================================================================================================

// MP Missions At Coords Focus Mission data - contains all additional control variables required when the mission is the Focus Mission
STRUCT g_structMatCFocusMissionMP
	INT				focusUniqueID			= NO_UNIQUE_ID							// The Mission Controller uniqueID for the Mission Request
	INT				focusBitsPlayers		= ALL_MISSIONS_AT_COORDS_BITS_CLEAR		// A bitfield representing the other players already reserved for the mission
	BOOL			focusForceWalkOut		= FALSE									// TRUE if the player needs to be forced out of the corona while the cloud data load is still in progress (waits until load complete)
	TIME_DATATYPE	focusInCoronaTimeout											// A timeout to cover a brief delay between the External InCorona actions being setup and the player being warped to the corona edge
	BOOL			focusReserved			= FALSE						// Graeme added this back in for Title Update. Check with Keith
	BOOL			focusRegisteredAsShared	= FALSE									// TRUE once the player knows they have registered with the Shared Mission data, or it doesn't need to be Shared
	TIME_DATATYPE	focusCheckSharedTimeout											// A timeout to re-test to see if the Shared Mission registration needs sent again (in case the host bombed out)
	TIME_DATATYPE	focusReserveTimeout												// A safety timeout to cancel mission reservation and try it again
	TIME_DATATYPE	focusCloudDataTimeout											// A safety timeout waiting to be told the Cloud Data has loaded
ENDSTRUCT

// Additional Focus Mission TitleUpdate data
STRUCT g_structMatCFocusMissionMP_TU
	INT				focusFrameTimeout		= 0										// A frame timeout because during a transition the network timer can jump
ENDSTRUCT




// -----------------------------------------------------------------------------------------------------------

// A PROJECT-SPECIFIC ID used to specify which external routine should be dealing with this activity's InCorona Actions
// KGM NOTE 22/11/12: This is for future expansion. Initially the only external InCorona Actions will be handled by Freemode,
//						but in future other scripts could require external InCorona actions so this ID will ensure only the
//						correct script does processing.
ENUM g_eMatCInCoronaExternalID
	MATCICE_FMMC_LAUNCHER,					// FMMC_Launcher handles the InCorona Actions for this activity
	MATCICE_FM_TUTORIAL,					// FM Tutorial needs to control launching a mission without using all FMMC_Launcher InCorona stuff
	MATCICE_FM_CORONA_TUT,					// FM Corona Tutorial needs to show a cutscene before doing the usual corona sequence
	
	// Leave this at the bottom
	MATCICE_NONE
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// ENUM containing mission launch restriction reason
ENUM g_eMatCLaunchFailReasonID
	MATCLFR_RANK_TOO_LOW,					// The player's rank is too low to join the mission
	MATCLFR_WRONG_TIMEOFDAY,				// The Time Of Day is wrong for the mission
	MATCLFR_WANTED_LEVEL,					// The player has a wanted level
	MATCLFR_NOT_ENOUGH_CASH,				// The player can't afford to start the mission
	//MATCLFR_BOUNTY,							// The player has a bounty
	// Leave this at the bottom
	MATCLFR_NONE
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Missions At Coords Focus Mission InCorona Actions data - contains data used to commincate with an external script that is handling actions when the player is in a Corona
// EG: For Freemode, this is the menu display, the leaderboard display, the sending of Invites, etc. Initially also the voting, although this may change.
// Initial Intended Sequence (although I can foresee some flaws, so this will need tweaked):
//		MatC sets DoProcessing, sets Stage to INITIALISE, sets RegID
//		External Initialises and sets Stage to WORKING
//		External sets StartMission when player votes
//		MatC starts mission and sets DoProcessing to FALSE
//		External waits for STARTED feedback to be broadcast using RegID
STRUCT g_structMatCInCoronaMP
	g_eMatCInCoronaExternalID	matcicExternalID		= MATCICE_NONE			// The external ID that dictates which routine is performing the external processing for this activity
	BOOL						matcicDoProcessing		= FALSE					// MatC sets TRUE when player enters Corona, MatC sets FALSE when External StartsMission
	BOOL						matcicInitialise		= FALSE					// MatC sets to TRUE when External should setup InCorona Actions
	BOOL						matcicAllowReserve		= FALSE					// External sets to TRUE when a Host/Join decision has been made allowing Mission Reservation to take place
	BOOL						matcicPlayerReserved	= FALSE					// MatC sets to TRUE when External can continue with the rest of the corona sequence after mission reservation
	BOOL						matcicCloudDataLoaded	= FALSE					// External sets to TRUE when InCorona Actions Cloud Data has Loaded
	BOOL						matcicStartMission		= FALSE					// External sets to TRUE when player has voted to start activity
	BOOL						matcicExternalError		= FALSE					// KGM 15/1/15: External script has had an error, probably during cloud data download (previous workaround was to call 'cloud data loaded' to allow Matc to move on and then cleanup later - but this had issues)
	BOOL						matcicForceQuit			= FALSE					// MatC sets to TRUE if it wants the external routines to stop processing InCorona Actions
	BOOL						matcicOfferHostJoin		= FALSE					// MatC sets to TRUE if the Host/Join screen should be used
	BOOL						matcicForCutsceneOnly	= FALSE					// MatC sets to TRUE if this corona is being used only to trigger a mocap cutscene (ie: Heist Prep Cutscene)
	BOOL						matcicHeistMidStrandCut	= FALSE					// MatC sets to TRUE if this corona is being used only to trigger a Heist Mid-Strand cutscene (ForCutsceneOnly should also be set)
	BOOL						matcicHeistTutorialCut	= FALSE					// MatC sets to TRUE if this corona is being used to trigger a Heist Tutorial Cutscene putting the player immediately on the Prep Mission (ForCutsceneOnly should also be set)
	g_eMatCLaunchFailReasonID	matcicLaunchFailReason	= MATCLFR_NONE			// The reason why the mission launch failed (so that the external routine can retrieve the correct help text)
	VECTOR						matcicCoords									// The Mission Location
	INT							matcicRegID				= ILLEGAL_AT_COORDS_ID	// The RegistrationID, allows External to check for feedback
	MP_MISSION_ID_DATA			matcicMissionIdData								// Any Mission Identification data required for cloud-loaded missions
ENDSTRUCT


//STRUCT g_structMatCInCoronaMP_OLD
//	g_eMatCInCoronaExternalID	matcicExternalID		= MATCICE_NONE			// The external ID that dictates which routine is performing the external processing for this activity
//	BOOL						matcicDoProcessing		= FALSE					// MatC sets TRUE when player enters Corona, MatC sets FALSE when External StartsMission
//	BOOL						matcicInitialise		= FALSE					// MatC sets to TRUE when External should setup InCorona Actions
//	BOOL						matcicCloudDataLoaded	= FALSE					// External sets to TRUE when InCorona Actions Cloud Data has Loaded
//	BOOL						matcicStartMission		= FALSE					// External sets to TRUE when player has voted to start activity
//	BOOL						matcicForceQuit			= FALSE					// MatC sets to TRUE if it wants the external routines to stop processing InCorona Actions
//	g_eMatCLaunchFailReasonID	matcicLaunchFailReason	= MATCLFR_NONE			// The reason why the mission launch failed (so that the external routine can retrieve the correct help text)
//	VECTOR						matcicCoords									// The Mission Location
//	INT							matcicRegID				= ILLEGAL_AT_COORDS_ID	// The RegistrationID, allows External to check for feedback
//	MP_MISSION_ID_DATA_OLD		matcicMissionIdData								// Any Mission Identification data required for cloud-loaded missions
//ENDSTRUCT
//


// ===========================================================================================================
//      Missions At Coords Banding globals
//		These are pointers into the Missions At Coords array to optimise the updating based on distance
//		One mission at each Band will be scheduled to update every frame, distant missions will update less often
// ===========================================================================================================

// IDs for the different banding levels
ENUM g_eMatCBandings
	MATCB_FOCUS_MISSION,							// The focus mission, of which there is one (max)
	MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE,		// The nearby missions are inside mission name range and have a possibility of becoming the focus mission, so are updated frequently
	MATCB_MISSIONS_INSIDE_CORONA_RANGE,				// The mid-range missions are inside corona range but outside display name range, so need updated more frequently to check for boundaries
	MATCB_MISSIONS_OUTSIDE_CORONA_RANGE,			// Missions outside corona range, so are updated less frequently but allow the corona displays to be more reactive whenthe player gets nearer
	MATCB_MISSIONS_FAR_AWAY_RANGE,					// The distant missions, so are updated infrequently
	MATCB_NEW_MISSIONS,								// New missions which need initial processing - these are dumped at the end of the array ready the next maintainance check
	
	// Leave these at the end
	MAX_MISSIONS_AT_COORDS_BANDINGS,
	MATCB_MISSIONS_BEING_DELETED					// Used as the 'desired band' when a mission is in the process of being deleted from the Mission At Coords system
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Missions At Coords Bandings Data
STRUCT g_structMatCBandingsMP
	INT		matcbNumInBand	= 0						// The number of Mission At Coords entries in this banding level
	INT		matcbFirstSlot	= INVALID_MATC_SLOT		// The first slot containing a valid entry at this banding level
	INT		matcbLastSlot	= INVALID_MATC_SLOT		// The last slot containing a valid entry at this banding level
	INT		matcbUpdateSlot	= INVALID_MATC_SLOT		// For banding slots with scheduled updates, this records the most recent slot updated
ENDSTRUCT




// ===========================================================================================================
//      Missions At Coords Stages globals
//		These are the stages a mission will go through usually based on distance from player
// ===========================================================================================================

// IDs for the different stages
// NOTE: If a new Stage is added then the Initialise_MP_Missions_At_Coords() function will need range and Band information set up
ENUM g_eMatCStages_OLD
	OLD_MATCS_STAGE_NEW,								// This is a new mission that hasn't performed any stage checks yet
	OLD_MATCS_STAGE_DELETE,								// This mission has been marked for deletion
	OLD_MATCS_SLEEPING,									// This mission is sleeping and should not display a blip or corona (added for Gang Attacks that have been recently played)
	OLD_MATCS_OUTSIDE_ALL_RANGES,						// The player is outside any maintenance range for this mission
	OLD_MATCS_OUTSIDE_DISPLAY_RANGES,					// The player is outside all display ranges for the mission, but is still close to the corona
	OLD_MATCS_DISPLAY_CORONA_RANGE,						// The player is inside the corona display range for this mission
	OLD_MATCS_AREA_TRIGGERED_MISSION,					// The mission is area-triggered so needs to check for the player being in an angled area instead of at a corona
	OLD_MATCS_MISSION_LAUNCH_IMMEDIATELY,				// The mission launches immediately when unlocked without waiting for the player to be in the corona
	OLD_MATCS_REQUESTING_MISSION_NAME,					// The player is inside the range where the mission name will be displayed in the corona and should be requesting the Locate Message
	OLD_MATCS_DISPLAYING_MISSION_NAME,					// The player is now displaying the mission name as a Locate Message
	OLD_MATCS_FORCING_AS_FOCUS_MISSION,					// Only used when the player has been dragged onto this mission without using a corona but the mission can't yet become the Focus Mission
//	OLD_MATCS_WAITING_FOR_ALLOW_RESERVATION,			// The InCorona Sequence has been started and is waiting for the Host Join screen to make a decision to allow reservation
	OLD_MATCS_RESERVING_MISSION,						// The player is in the range for reserving the mission and has sent the reservation request and is waiting for confirmation
	OLD_MATCS_WAITING_FOR_CLOUD_DATA,					// The mission has requested its cloud-data (if it has any) and is waiting for it to be loaded
	OLD_MATCS_WAITING_TO_START,							// The mission has been reserved, cloud-data loaded, and is waiting to start (this may access the built-in voting functions, or external functions)
	OLD_MATCS_MISSION_STARTING,							// The player has voted and the request to start the mission has been sent and is waiting for confirmation
	OLD_MATCS_WAITING_FOR_ON_MISSION,					// Only used when the player has been dragged onto this mission without using a corona and the mission is triggering but not yet active
	OLD_MATCS_WAITING_FOR_QUIT_CORONA,					// KGM 24/6/13 [Slight Change of use]: Waits for the 'is player in corona' function to be FALSE then cleans up some standard corona controls, then moves on to waiting for player being outside corona triggering radius
	OLD_MATCS_WAITING_FOR_OUTSIDE_FOCUS_RANGE,			// After the external InCorona routines tidy up (which used to walk the player out but now doesn't), wait here until the player does move out before clearing the focus mission variables
	OLD_MATCS_DOWNLOAD_FOR_QUICK_LAUNCH,				// Used if the player enters the triggering area and a quick launch is required - downloads the cloud mission data
	OLD_MATCS_MISSION_QUICK_LAUNCHING,					// Used while the mission is waiting for Quick Launch confirmation from mission controller
	OLD_MATCS_ON_MISSION,								// The player is on mission and is waiting for the mission to be finished
	
	// Leave this at the end
	OLD_MAX_MISSIONS_AT_COORDS_STAGES
ENDENUM


ENUM g_eMatCStages
	MATCS_STAGE_NEW,								// This is a new mission that hasn't performed any stage checks yet
	MATCS_STAGE_DELETE,								// This mission has been marked for deletion
	MATCS_SLEEPING,									// This mission is sleeping and should not display a blip or corona (added for Gang Attacks that have been recently played)
	MATCS_OUTSIDE_ALL_RANGES,						// The player is outside any maintenance range for this mission
	MATCS_OUTSIDE_DISPLAY_RANGES,					// The player is outside all display ranges for the mission, but is still close to the corona
	MATCS_DISPLAY_CORONA_RANGE,						// The player is inside the corona display range for this mission
	MATCS_AREA_TRIGGERED_MISSION,					// The mission is area-triggered so needs to check for the player being in an angled area instead of at a corona
	MATCS_MISSION_LAUNCH_IMMEDIATELY,				// The mission launches immediately when unlocked without waiting for the player to be in the corona
	MATCS_REQUESTING_MISSION_NAME,					// The player is inside the range where the mission name will be displayed in the corona and should be requesting the Locate Message
	MATCS_DISPLAYING_MISSION_NAME,					// The player is now displaying the mission name as a Locate Message
	MATCS_FORCING_AS_FOCUS_MISSION,					// Only used when the player has been dragged onto this mission without using a corona but the mission can't yet become the Focus Mission
	MATCS_WAITING_FOR_ALLOW_RESERVATION,			// The InCorona Sequence has been started and is waiting for the Host Join screen to make a decision to allow reservation
	MATCS_RESERVING_MISSION,						// The player is in the range for reserving the mission and has sent the reservation request and is waiting for confirmation
	MATCS_WAITING_FOR_CLOUD_DATA,					// The mission has requested its cloud-data (if it has any) and is waiting for it to be loaded
	MATCS_WAITING_TO_START,							// The mission has been reserved, cloud-data loaded, and is waiting to start (this may access the built-in voting functions, or external functions)
	MATCS_MISSION_STARTING,							// The player has voted and the request to start the mission has been sent and is waiting for confirmation
	MATCS_WAITING_FOR_ON_MISSION,					// Only used when the player has been dragged onto this mission without using a corona and the mission is triggering but not yet active
	MATCS_WAITING_FOR_QUIT_CORONA,					// KGM 24/6/13 [Slight Change of use]: Waits for the 'is player in corona' function to be FALSE then cleans up some standard corona controls, then moves on to waiting for player being outside corona triggering radius
	MATCS_WAITING_FOR_OUTSIDE_FOCUS_RANGE,			// After the external InCorona routines tidy up (which used to walk the player out but now doesn't), wait here until the player does move out before clearing the focus mission variables
	MATCS_DOWNLOAD_FOR_QUICK_LAUNCH,				// Used if the player enters the triggering area and a quick launch is required - downloads the cloud mission data
	MATCS_MISSION_QUICK_LAUNCHING,					// Used while the mission is waiting for Quick Launch confirmation from mission controller
	MATCS_ON_MISSION,								// The player is on mission and is waiting for the mission to be finished
	
	// Leave this at the end
	MAX_MISSIONS_AT_COORDS_STAGES
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Missions At Coords Stages Data
STRUCT g_structMatCStagesMP
	g_eMatCBandings	matcbBand	= MAX_MISSIONS_AT_COORDS_BANDINGS	// The scheduling Band the mission should be moved into if the mission is at this stage
ENDSTRUCT




// ===========================================================================================================
//      Corona Struct
//		Stores Details for the corona display
// ===========================================================================================================

STRUCT g_structMatCCoronaMP
	HUD_COLOURS		theCoronaColour			= HUD_COLOUR_PURE_WHITE		// The Hud Colour to use for the corona
	HUD_COLOURS		theTextColour			= HUD_COLOUR_PURE_WHITE		// The Hud Colour to use for the InCorona Text
	FLOAT			triggerRadius			= 0.0						// The radius within which the player will be regarded as InCorona
	eMP_TAG_SCRIPT	theIcon					= MP_TAG_SCRIPT_NONE		// The icon to display at a greater disance than the InCorona mission text
	INT				theOnHours				= MATC_ALL_DAY				// Bitfield where the first 24 bits represent the hours of the day the mission is available
ENDSTRUCT




// ===========================================================================================================
//      Missions At Coords missions array globals - contiguous
//		These are the details for all the missions being handled by the Misions At Coords array
// ===========================================================================================================

// Mission Setup Options Bits
// General Rule: An option is something that gets setup externally and changes the way a corona is controlled
CONST_INT	MATC_BITFLAG_HAS_CUTSCENE					0				// TRUE if this mission has a cutscene
CONST_INT	MATC_BITFLAG_DELETE_ON_PLAY					1				// TRUE if this mission should be deleted after it has been played
CONST_INT	MATC_BITFLAG_DELETE_AFTER_FOCUS				2				// TRUE if this mission should be deleted after it has been the focus mission (including if it has just been played)
CONST_INT	MATC_BITFLAG_HANDLE_VOTING					3				// TRUE if this mission should use MatC's in-built voting functions, (FALSE if no vote required or vote handled externally)
CONST_INT	MATC_BITFLAG_INTERIOR_BLIP_ONLY				4				// TRUE if the blip for this mission should only appear when player nearby in an interior
CONST_INT	MATC_BITFLAG_MINIMAP_BLIP_ONLY				5				// TRUE if the blip should only appear on the minimap, not on the FrontEnd map
CONST_INT	MATC_BITFLAG_IGNORE_REFRESH					6				// TRUE if this mission should ignore the usual cloud-refresh rules (and therefore remain in play), FALSE to obey usual rules
CONST_INT	MATC_BITFLAG_SHARE_CLOUD_DATA				7				// TRUE if the cloud-loaded header details for this activity should be shared with all players, FALSE if not or if the data all comes from Mission Data
CONST_INT	MATC_BITFLAG_NO_BLIP_TO_BE_DISPLAYED		8				// TRUE if no blip should be displayed for this activity, FALSE if a blip should be displayed (as normal)
CONST_INT	MATC_BITFLAG_NO_CORONA_TO_BE_DISPLAYED		9				// TRUE if no corona should be displayed for this activity, FALSE if a corona should be displayed (as normal)
CONST_INT	MATC_BITFLAG_QUICK_LAUNCH					10				// TRUE if the mission should quick launch ignoring corona sequence (added for Gang Attacks), FALSE for reserve/incorona/start sequence (as normal)
CONST_INT	MATC_BITFLAG_AREA_TRIGGERED					11				// TRUE if this mission should be triggered when the player walks into an Area, FALSE if usual triggering rules apply
CONST_INT	MATC_BITFLAG_USE_EXTENDED_NAME_RANGE		12				// TRUE if the extended mission name (in corona) display range should be used, FALSE for standard range (added for MP_INTRO)
CONST_INT	MATC_BITFLAG_LAUNCH_IMMEDIATELY				13				// TRUE if the mission should launch immediately it has been unlocked (added for contact missions), FALSE for standard launch (waiting for player in corona, as normal)
CONST_INT	MATC_BITFLAG_INVITE_ONLY					14				// TRUE if the mission should only launch if an invite is accepted, FALSE to allow walk-ins as normal
CONST_INT	MATC_BITFLAG_AWAIT_ACTIVATION_AFTER_PLAY	15				// TRUE if the mission should await activation after play, FALSE if it can be re-triggered without re-activation (as normal)
CONST_INT	MATC_BITFLAG_REMAIN_OPEN_IF_ACTIVE			16				// TRUE if the mission should remain open if the mission is active in the session, FALSE to obey normal opening and closing times even if active (as normal)
CONST_INT	MATC_BITFLAG_ADDED_AS_SHARED_MISSION		17				// TRUE if this mission has only been added as a Shared Mission, FALSE if added (or re-added) by another game process
CONST_INT	MATC_BITFLAG_WAIT_FOR_SECONDARY_UNLOCK		18				// TRUE if this mission should wait for a secondary unlock of its type before becoming unlocked, FALSE if it unlocks with the primary unlock (as normal) (added for SURVIVAL two-stage unlock)
CONST_INT	MATC_BITFLAG_ALLOW_DURING_AMBIENT_TUTORIAL	19				// TRUE if the mission is allowed to be displayed and active during an ambient tutorial, otherwise FALSE
CONST_INT	MATC_BITFLAG_DO_NOT_TRIGGER					20				// TRUE if the mission should not be allowed to trigger, FALSE if it can trigger (as normal) - this was added for Tutorial stuff
CONST_INT	MATC_BITFLAG_SHOW_BLIP_LONG_RANGE			21				// TRUE if the blip should be temporarily displayed long-range, FALSE for short-range
CONST_INT	MATC_BITFLAG_FOR_CUTSCENE_ONLY				22				// TRUE if this corona is only setup to trigger a mocap cutscene (ie: Heist Pre-Planning mocap), FALSE if it's a normal corona
CONST_INT	MATC_BITFLAG_HEIST_MIDSTRAND_MOCAP			23				// TRUE if this corona is only setup to trigger a Heist Mid-Strand mocap cutscene (FOR_CUTSCENE_ONLY should also be set), FALSE if it's a normal corona or a Heist Intro Cutscene
CONST_INT	MATC_BITFLAG_HEIST_TUTORIAL_MOCAP			24				// TRUE if this corona will trigger a Heist Tutorial mocap cutscene followed immediately by the mission (FOR_CUTSCENE_ONLY should also be set), FALSE if it's a normal corona or a non-tutorial Heist Cutscene
CONST_INT	MATC_BITFLAG_HOST_OWN_INSTANCE				25				// TRUE if the player should launch their own instance of this corona, FALSE if ok to join other instances in the same session


// -----------------------------------------------------------------------------------------------------------

// Mission Internal States Bits
// General Rule: A state is something that usually gets set internally and records the current state of a changeable parameter
CONST_INT	MATC_BITFLAG_FLASH_BLIP						0				// TRUE if the blip should flash when next displayed (probably because the activity type has just unlocked)
CONST_INT	MATC_BITFLAG_USING_FIRST_LOCATE_MESSAGE		1				// TRUE if this mission is using the first locate message (there are 3 scaleform movies we can use)
CONST_INT	MATC_BITFLAG_USING_SECOND_LOCATE_MESSAGE	2				// TRUE if this mission is using the second locate message (there are 3 scaleform movies we can use)
CONST_INT	MATC_BITFLAG_USING_THIRD_LOCATE_MESSAGE		3				// TRUE if this mission is using the third locate message (there are 3 scaleform movies we can use)
CONST_INT	MATC_BITFLAG_MARKED_FOR_REFRESH				4				// TRUE if this mission has cloud-loaded data that may be getting refreshed
CONST_INT	MATC_BITFLAG_MARKED_FOR_DELETE				5				// TRUE if this mission has been marked for delete
CONST_INT	MATC_BITFLAG_ACTIVITY_LOCKED				6				// TRUE if the activity type of this mission is locked (and not available), FALSE if unlocked (and available)
CONST_INT	MATC_BITFLAG_ACTIVITY_TEMP_UNLOCKED			7				// TRUE if the activity has been temporarily unlocked (probably due to joblist invite), FALSE if not temporarily unlocked
CONST_INT	MATC_BITFLAG_TEMP_UNLOCKED_UNTIL_PLAYED		8				// TRUE if a temporarily unlocked activity should remain unlocked until played (works in addition to the above flag) - added for Race Tutorial
CONST_INT	MATC_BITFLAG_MISSION_BEING_PLAYED_AGAIN		9				// TRUE if the mission is being played again so should remain temporarily unlocked, FALSE under normal circumstances
CONST_INT	MATC_BITFLAG_IN_VEHICLE_HELP_DISPLAYED		10				// TRUE if the help text explaining the player can't trigger a mission while in a vehicle has been displayed - prevents help text spam
CONST_INT	MATC_BITFLAG_MISSION_HAS_BEEN_PLAYED		11				// TRUE if this player has played this mission during any session (shows a tick on the blip), FALSE if not played at all
CONST_INT	MATC_BITFLAG_VEHICLE_GENERATORS_BLOCKED		12				// TRUE if vehicle generators in the area of the corona have been blocked, FALSE if not blocked (ie: active)
CONST_INT	MATC_BITFLAG_CURRENT_PLAYLIST_ACTIVITY		13				// TRUE if this is the current playlist activity so the corona should be active on a playlist, FALSE if not the current playlist activity so standard triggering rules apply
CONST_INT	MATC_BITFLAG_INVITE_ACCEPTED				14				// TRUE if an invite to the mission has been accepted, FALSE if not
CONST_INT	MATC_BITFLAG_AWAITING_ACTIVATION			15				// TRUE if the mission is awaiting activation and should display no blip until something activates them (added for Gang Attacks), FALSE if active (as normal)
CONST_INT	MATC_BITFLAG_FORCE_UPDATE					16				// TRUE if the mission should do an immediate update on the next maintenance frame
CONST_INT	MATC_BITFLAG_RESTRICTED_WANTED				17				// TRUE if the mission is blocked due to wanted level
CONST_INT	MATC_BITFLAG_RESTRICTED_CASH				18				// TRUE if the mission is blocked due to not enough cash
CONST_INT	MATC_BITFLAG_RESTRICTED_RANK				19				// TRUE if the mission is blocked due to Rank too low
CONST_INT	MATC_BITFLAG_RESTRICTED_TOD					20				// TRUE if the mission is blocked due to wrong time of day : fall case restriction 
CONST_INT	MATC_BITFLAG_SHOW_RESERVED					21				// TRUE if the mission has a player waiting in a minigame 
CONST_INT	MATC_BITFLAG_HIDDEN_BY_OVERLAPPING_CORONA	22				// TRUE if this mission is hidden and untriggerable to walk-ins because of an overlapping mission, otherwise FALSE
CONST_INT	MATC_BITFLAG_WAS_FORCED_ONTO_MISSION		23				// TRUE if the player was dragged onto this mission by the mission controller without using a corona, FALSE if mission launched noramlly
CONST_INT	MATC_BITFLAG_HIDDEN_BY_PI_MENU				24				// TRUE if the player has hidden blips for this job type using the Personal Interactions menu, FALSE if not hidden (KGM 12/5/15: BUG 2313555 - PI Menu Hide Job Blips Option)
CONST_INT	MATC_BITFLAG_HIDDEN_BY_DISABLED_AREA		25				// TRUE if the mission is in a disabled area and should be hidden (ie: an FM Event vehicle area), FALSE if not hidden by disabled area
CONST_INT	MATC_BITFLAG_HIDDEN_GANG_ATTACK				26				// TRUE if this Gang Attack has been specifically hidden by rootContentID, FALSE if not [KGM 22/7/15 BUG 2382702]

// -----------------------------------------------------------------------------------------------------------

//STRUCT g_structMissionsAtCoordsMP_OLD
//	INT							matcRegID			= ILLEGAL_AT_COORDS_ID				// The registrationID, used for reference to this mission post-registration
//	g_eMatCStages				matcStage			= MATCS_STAGE_NEW					// The processing stage for this mission - this is usually based on the player's current distance from the mission coord
//	g_eMPMissionSource			matcSourceID		= UNKNOWN_MISSION_SOURCE			// The source of the mission - for passing on to the Mission Controller and also for some other uses (ie: mass delete)
//	VECTOR						matcCoords												// The focus location for the mission
//	g_structMatCCoronaMP		matcCorona												// The Corona Details for the mission
//	BLIP_INDEX					matcBlipIndex		= NULL								// The Blip Index
//	INT							matcOptionBitflags	= ALL_MISSIONS_AT_COORDS_BITS_CLEAR	// Any External Options flags
//	INT							matcStateBitflags	= ALL_MISSIONS_AT_COORDS_BITS_CLEAR	// Any Intrnal State Flags
//	g_eMatCInCoronaExternalID	matcExternalID		= MATCICE_NONE						// The External ID indicating which External script needs to handle any InCorona Actions (ie: For Freemode - Menus, Leaderboards, etc. are all handled externally)
//	MP_MISSION_ID_DATA_OLD		matcMissionIdData										// Mission Identification Data
//ENDSTRUCT


// MP Missions At Coords Data - contains all control data associated with one mission at a coord
STRUCT g_structMissionsAtCoordsMP
	INT							matcRegID			= ILLEGAL_AT_COORDS_ID				// The registrationID, used for reference to this mission post-registration
	g_eMatCStages				matcStage			= MATCS_STAGE_NEW					// The processing stage for this mission - this is usually based on the player's current distance from the mission coord
	g_eMPMissionSource			matcSourceID		= UNKNOWN_MISSION_SOURCE			// The source of the mission - for passing on to the Mission Controller and also for some other uses (ie: mass delete)
	VECTOR						matcCoords												// The focus location for the mission
	g_structMatCCoronaMP		matcCorona												// The Corona Details for the mission
	BLIP_INDEX					matcBlipIndex		= NULL								// The Blip Index
	INT							matcOptionBitflags	= ALL_MISSIONS_AT_COORDS_BITS_CLEAR	// Any External Options flags
	INT							matcStateBitflags	= ALL_MISSIONS_AT_COORDS_BITS_CLEAR	// Any Intrnal State Flags
	g_eMatCInCoronaExternalID	matcExternalID		= MATCICE_NONE						// The External ID indicating which External script needs to handle any InCorona Actions (ie: For Freemode - Menus, Leaderboards, etc. are all handled externally)
	MP_MISSION_ID_DATA			matcMissionIdData										// Mission Identification Data
	INT							matcIdCloudnameHash = 0									// Hash of the text file name for this cloudFilename 
ENDSTRUCT


// JA: 19/7/13: Pre Corona Host screen state
CONST_INT CORONA_PRE_HOST_SCREEN_INIT		0
CONST_INT CORONA_PRE_HOST_SCREEN_WAIT		1
CONST_INT CORONA_PRE_HOST_SCREEN_INPUT_SKIP	2
CONST_INT CORONA_PRE_HOST_SCREEN_MAINTAIN	3
CONST_INT CORONA_PRE_HOST_SCREEN_QUIT		4

CONST_INT CORONA_JOIN_EXISTING_INSTANCE			0
CONST_INT CORONA_HOST_NEW_INSTANCE				1
CONST_INT CORONA_JOIN_EXISTING_PLAYER_INSTANCE	2




// ===========================================================================================================
//      Global Blip Struct
//		Used to pass blip information to the Missions At Coords routines
// ===========================================================================================================

STRUCT g_structMatCBlipMP
	BOOL			isShortRange			= TRUE					// TRUE if the blip is short-range, FALSE for long-range
	BLIP_SPRITE		theBlipSprite			= RADAR_TRACE_INVALID	// The Blip Sprite ID to use
	TEXT_LABEL_23	theBlipName										// The blip name to use
	TEXT_LABEL_63	thePlayerNameForBlip							// An optional Player Name component for the Blip Name
	INT				theBlipColour			= BLIP_COLOUR_DEFAULT	// The Blip Colour
	BLIP_PRIORITY	theBlipPriority			= BLIPPRIORITY_MED		// The Blip's display priority
	BOOL			fromMissionCreator		= FALSE					// TRUE if the blip is a 'mission creator' blip, FALSE if not
	VECTOR			alternativeBlipCoords							// Generally ignored, but used when the blip should be displayed in a different location from the corona
ENDSTRUCT




// ===========================================================================================================
//      Feedback Globals
//		Allows the Missions At Coords system to feedback to the script that registered the mission
// ===========================================================================================================

// Number of MissionsAtCoords Feedback Slots required
CONST_INT	MAX_NUM_MP_MATC_FEEDBACK_SLOTS			8

// Length of time the feedback will remain active (used a value greater than the expected max host migration time of 10secs)
CONST_INT	MATC_FEEDBACK_ACTIVE_TIME_msec			20000


// -----------------------------------------------------------------------------------------------------------

// MissionAtCoords Feedback Type
ENUM g_eMatCFeedbackType
	MATCFBT_STARTED,						// Feedback when the mission has started and this player is on it
	MATCFBT_FINISHED,						// Feedback when the mission has been played and is finished
	
	// Leave this at the bottom
	MATCFBT_NONE							// No feedback
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// The MissionsAtCoords Feedback struct
STRUCT g_structMatCFeedbackMP
	g_eMatCFeedbackType		matcfbType		= MATCFBT_NONE				// The type of feedback
	INT						matcfbRegID		= ILLEGAL_AT_COORDS_ID		// The mission registration ID for this feedback
	TIME_DATATYPE			matcfbTimeout								// The time when this feedback times out
ENDSTRUCT




// ===========================================================================================================
//      Global Options Struct
//		Not retained. Used only to pass per-mission options to the Missions At Coords system.
//		Each mission stored will pass in an options struct which will be analyzed and dealt with immediately
//			rather than stored. Because of this I'm making the struct contain BOOLs rather than BITFIELDs
//			to make it easier to see what all the options are. Any that need stored will be converted to
//			a BITFLAG and stored on the missions bitfield.
// ===========================================================================================================

// A struct of BOOLs (to make the available options more readable) allowing per-mission options to be passed to the Missions At Coords system without requiring a big list of parameters.
STRUCT g_structMatCOptionsMP
	g_eMatCInCoronaExternalID	matcoInCoronaExternalID	= MATCICE_NONE		// An ID indicating which external script should handle InCorona Actions when the player is standing in the corona
	BOOL						matcoHandleVoting		= FALSE				// TRUE if MatC should handle the voting for this activity, FALSE if there is no voting or it is externally controlled
	BOOL						matcoDeleteOnPlay		= FALSE				// TRUE if this mission should be deleted on being played, FALSE if it should be retained
	BOOL						matcoDeleteAfterFocus	= FALSE				// TRUE if this mission should be deleted when the mission stops being the Focus mission, FALSE is it should be retained
	BOOL						matcoInactiveAfterPlay	= FALSE				// TRUE if this mission needs to await activation after play, FALSE if it remains active and can be triggered again immediately
	BOOL						matcoHasCutscene		= FALSE				// TRUE if this mission has a separate intro cutscene (this was for the CnC routines), FALSE if there is no intro cutscene or it is contained within the mission
	BOOL						matcoInteriorBlipOnly	= FALSE				// TRUE if blip is only displayed when the player is nearby and in an interior, FALSE means obey usual blip display rules
	BOOL						matcoMinimapBlipOnly	= FALSE				// TRUE if blip should only be displayed on the minimap (ie: for Random Event missions), FALSE if also on FrontEnd map
	BOOL						matcoHasBeenPlayed		= FALSE				// TRUE if blip should be marked as 'been played', FALSE if never played
	BOOL						matcoIgnoreRefresh		= FALSE				// TRUE if MatC should ignore a cloud-refresh for this mission, FALSE if it should obey usual refresh rules
	BOOL						matcoShareCloudData		= FALSE				// TRUE if the activity should share the cloud header data, FALSE if it doesn't need to share or if the data is all mission data
	BOOL						matcoDisplayNoBlip		= FALSE				// TRUE if the activity should not display a blip, FALSE if it should display a blip as normal
	BOOL						matcoDisplayNoCorona	= FALSE				// TRUE if the activity should not display a corona, FALSE if it should display a corona as normal
	BOOL						matcoQuickLaunch		= FALSE				// TRUE if the activity should quick launch with no corona sequence, FALSE for normal launch sequence
	BOOL						matcoUseAngledArea		= FALSE				// TRUE if the activity is Angled Area Triggered, FALSE for normal triggering at a corona
	BOOL						matcoRemainOpenIfActive	= FALSE				// TRUE if the activity should ignore opening/closing hours if it is active in teh session, FALSE if it obeys them as normal
	BOOL						matcoLaunchImmediately	= FALSE				// TRUE if the activity should launch immediately when unlocked, FALSE for normal launching when player reaches corona
	BOOL						matcoInviteOnly			= FALSE				// TRUE if the activity is only available to players invited to the mission, FALSE to also allow walk-ins
	BOOL						matcoSharedMission		= FALSE				// TRUE if this activity is being added as a Shared Mission, FALSE if being added by another setup routine
	BOOL						matcoSecondaryUnlock	= FALSE				// TRUE if the activity should wait for a secondary unlock of the type, FALSE if it can unlock with the initial unlock
	BOOL						matcoForCutsceneOnly	= FALSE				// TRUE if this corona is being used to piggyback Mocap Cutscene triggering (ie: Heist pre-planning cutscene AND heist mid-strand cutscene), FALSE if not
	BOOL						matcoHeistMidStrandCut	= FALSE				// TRUE if this corona is being used to piggyback Heist Mid-Strand Mocap Cutscene triggering, FALSE if not (or if it's being used for Heist Intro Cutscene triggering)
	BOOL						matcoHeistTutorialCuts	= FALSE				// TRUE if this corona is being used to piggyback Heist tutorial Mocap Cutscene triggering.
	BOOL						matcoHostOwnInstance	= FALSE				// TRUE if the player should host their own instance of the corona rather than joining another local instance, FALSE if not
	VECTOR						matcoAngledAreaMin							// A vector representing the minimum value for an Angled Area triggered mission
	VECTOR						matcoAngledAreaMax							// A vector representing the maximum value for an Angled Area triggered mission
	FLOAT						matcoAngledAreaWidth	= 0.0				// The Width of the Angled Area
ENDSTRUCT




// ===========================================================================================================
//      Unlocks Globals
//		Allows the feed message to display a breakdown of what was unlocked at a specific rank
// ===========================================================================================================

// Struct containing the values of the jobs unlocked
STRUCT g_structMatcUnlocks
	BOOL	rankJobsNotYetUnlocked		= FALSE								// TRUE if there are jobs at this rank where the job type hasn't been unlocked yet by Dave
	INT		numUnlockedMissions			= 0									// Unlocked Missions
	INT		numUnlockedLTS				= 0									// Unlocked LTS
	INT		numUnlockedCTF				= 0									// Unlocked CTF
	INT		numUnlockedRaces			= 0									// Unlocked Races
	INT		numUnlockedDeathmatches		= 0									// Unlocked Deathmatches
	INT		numUnlockedSurvivals		= 0									// Unlocked Survivals
	INT		numUnlockedGangAttacks		= 0									// Unlocked Gang Attacks
	INT		numUnlockedBaseJumps		= 0									// Unlocked BaseJumps
	BOOL	areUnlockChecksDone			= FALSE								// Allows me to check if the bools below have been pre-filled. They need to be to avoid cyclic headers.
	BOOL	areMissionsUnlocked			= FALSE								// Needs pre-filled (incl: LTS and CTF)
	BOOL	areRacesUnlocked			= FALSE								// Needs pre-filled
	BOOL	areDeathmatchesUnlocked		= FALSE								// Needs pre-filled
	BOOL	areSurvivalsUnlocked		= FALSE								// Needs pre-filled
	BOOL	areGangAttacksUnlocked		= FALSE								// Needs pre-filled
	BOOL	areBaseJumpsUnlocked		= FALSE								// Needs pre-filled
ENDSTRUCT




// ===========================================================================================================
//      Missions At Coords Radius Blip globals
//		Controls the one Area-Triggered Radius Blip to be displayed - TITLE-UPDATE
// ===========================================================================================================

// Struct containing the control variables for the (currently) one displayed area-triggered radius blip (ie: Gang Attacks)
STRUCT g_structMatcRadiusBlip
	BLIP_INDEX			mrbBlipIndex										// The blip index of the Radius Blip
	INT					mrbRegID			= ILLEGAL_AT_COORDS_ID			// The RegID of the mission currentl with control over the Area-Triggered mission blip
ENDSTRUCT



