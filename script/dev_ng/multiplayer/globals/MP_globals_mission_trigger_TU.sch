USING "rage_builtins.sch"

USING "MP_globals_mission_trigger_definitions.sch"
USING "MP_globals_mission_trigger_definitions_TU.sch"
USING "MP_globals_common_definitions.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_mission_trigger_TU.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains global variable instances for additoinal globals in the TitleUpdate block.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************



// =============================================================================================================================================================
//      Mission Triggering Global Overview Consts
// =============================================================================================================================================================

// MP Mission Triggering Struct - deals with a mission offer to the player
// KEITH NOTE: This may change - there is a lot of crossover with the EOM struct so may be able to make a generic struct that both can use
g_structMissionTriggeringMP g_sTriggerMP



// -----------------------------------------------------------------------------------------------------------
//      Joblist Entries
// -----------------------------------------------------------------------------------------------------------

// Contains the JobList details required to display the job on the Joblist app
g_structMissionJobListMP    g_sJobListMP[MAX_MP_JOBLIST_ENTRIES]



// Joblist Update struct - used locally only
g_structJoblistUpdate   g_sJoblistUpdate




// -----------------------------------------------------------------------------------------------------------
//      Joblist Invites
// -----------------------------------------------------------------------------------------------------------

// Contains the details required to display invitations on the Joblist 
g_structJobListInvitesMP    	g_sJLInvitesMP[MAX_MP_JOBLIST_ENTRIES]


// -----------------------------------------------------------------------------------------------------------
//      Joblist Cross-Session Invites
// -----------------------------------------------------------------------------------------------------------

// Contains the details required to display cross-session invitations on the Joblist 
g_structJobListCSInvitesMP  	g_sJLCSInvitesMP[MAX_MP_JOBLIST_ENTRIES]



// -----------------------------------------------------------------------------------------------------------
//      Joblist Basic Invites that set a Return Value flag when accepted
// -----------------------------------------------------------------------------------------------------------

// Contains the details required to display invites that set a BOOL only when accepted
// EG: One on One Deathmatches, One on One Races, Tutorial Invites
g_structJobListRVInvitesMP  g_sJLRVInvitesMP[MAX_MP_JOBLIST_ENTRIES]



// -----------------------------------------------------------------------------------------------------------
//      Joblist Warp
// -----------------------------------------------------------------------------------------------------------

// A struct containing details of an Invite in the process of being accepted
g_structInviteAcceptanceMP  	g_sInviteAcceptanceMP

// Contains the details of a warp that needs to be maintained every frame after an invitation has been accepted on the joblist
// NOTE: I've taken this out of the app itself just in case the app gets killed mid-warp to ensure the warp continues.
g_structJobListWarpMP   g_sJoblistWarpMP

// KGM 15/6/14: Contains the apartment details that the player is warping into when accepting a same-session invite to a Heist Corona
// This is being used because I need to mimic the data being passed as transition data used when a cross-session invite into a Heist Corona is accepted
// Without this for same-session invites, the game doesn't know which apartment to put the player into and when it gets it wrong the corona doesn't get setup
INVITED_TO_PROPERTY_STRUCT	g_sJoblistHeistApartmentDetails
BOOL						g_sJoblistHeistInviteShouldKillApartment = FALSE

// KGM 17/10/14: Introduce a flag to indicate a cross-session invite has just been accepted (NOTE: using a timer instead of a flag in case the CS Invite fails to transition)
INT							g_joblistCSInviteJustAcceptedTimeout = 0



// -----------------------------------------------------------------------------------------------------------
//      Joblist displays a Help Message on the Corona specifically if a Player Invite is received
// -----------------------------------------------------------------------------------------------------------

// Display the help message?
BOOL	g_displayCoronaInviteHelp	= FALSE



// ===========================================================================================================
//      Mission Details Overlay globals
// ===========================================================================================================

// Contains any Mission Details Overlay Control Variables
g_structMissionDetailsOverlayMP g_sMDOverlayMP




// ===========================================================================================================
//      Mission Details to carry forward to Part2 (Strand) Missions globals
// ===========================================================================================================

// Contains any Mission Details that may be required to setup Part2 of a mission (Strand Missions - initially used for Heists)
g_structDetailsForStrandMissions g_sMDStrandMissionsMP




// ===========================================================================================================
//      Globals to to ensure overlapped Missions played in Rounds always trigger the correct mission
// ===========================================================================================================

// The hash value of the mission being triggered as a Rounds mission
INT	g_roundsMissionHashCID = 0





// ===========================================================================================================
//      Cloud Mission Loader globals
//      (Allows communication with the standalone script that keeps  alocal copy of the big datafile structs)
// ===========================================================================================================

g_structCloudMissionLoader  g_sCloudMissionLoaderMP

// An array to hold the contentIDs for the Heist Planning missions for which we need to download the header details
UGC_CONTENT_ID_QUERY	g_heistPlanningCloudRCIDsMP
INT						g_numHeistPlanningRequired	= 0

// FLAG: Indicates a Prep Mission download is in progress
BOOL					g_prepMissionDownloadInProgress = FALSE




// ===========================================================================================================
//      Additional Detail globals
//      (Allows some additional access functionality)
// ===========================================================================================================

// Stores the contact for the current mission, or NO_CHARACTER if not on mission or no contact
enumCharacterList g_eCurrentMissionContact = NO_CHARACTER                       



// -----------------------------------------------------------------------------------------------------------
//      Current Mission Description Globals
// -----------------------------------------------------------------------------------------------------------

// Retain the Current Mission Description while mission ongoing
g_structCurrentMissionDesc		g_sCurrentMissionDesc



// -----------------------------------------------------------------------------------------------------------
//      Auto-Invite Spam Prevention Globals
// -----------------------------------------------------------------------------------------------------------

// Stores recently deleted invites to prevent the auto-inviter from spamming the player
m_sSpamJobList				g_spamJobList[MAX_MP_JOBLIST_ENTRIES]



// -----------------------------------------------------------------------------------------------------------
//      Joblist Restricted Invite Timeout
// -----------------------------------------------------------------------------------------------------------

// The timeout time before the 'invite restricted due to privileges' timeout (this shouldn't get cleared on initialisation)
INT			g_inviteRestrictedTimeout		= 0


// -----------------------------------------------------------------------------------------------------------
//      Joblist Invite Header Details Download Control Flags
// -----------------------------------------------------------------------------------------------------------

// Only one of these can be TRUE at any one time - controls which invite is currently in charge of the mission header details download routines
BOOL		g_invitationBeingDownloaded		= FALSE
BOOL		g_csInviteBeingDownloaded		= FALSE


// -----------------------------------------------------------------------------------------------------------
//      Joblist General Control Variables
// -----------------------------------------------------------------------------------------------------------

// MMM 1645958 - Added to put invites in correct order
INT g_inviteOrderCounter = 1

// MMM 1992061 - To stop player from ducking in car when accepting an invite
INT g_inviteControlTimeout = -1




