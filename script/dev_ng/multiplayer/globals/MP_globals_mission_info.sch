USING "rage_builtins.sch"
USING "net_mission_enums.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_mission_info.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains globals and structs for all MP mission info storage.
//		NOTES			:	There are four data arrays:
//								main array:				contains pointers into the variation array
//								variation array:		contains all the data for all variations of a mission
//								mission team array:		contains all team-specific data consistent for all variations of a mission
//								variation team array:	contains all team-specific data that can differ between variations of a mission
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      Mission Info Globals
// ===========================================================================================================

// Used to clear out a Mission Info bitfield array
CONST_INT		ALL_MISSION_INFO_BITS_CLEAR			0

// Used to indicate an INT field is undefined
CONST_INT		UNDEFINED_MISSION_INFO				-1

// Used when a piece of mission data applies to all variations of the mission
CONST_INT		ALL_MISSION_VARIATIONS				-999




// ===========================================================================================================
//      Mission Info Team data and array
//		NOTE: There are two team arrays (this is to cut down on memory usage)
//			Mission Team Array:		Team differences that are consistent for all variations of a mission
//									This array is referenced through the mission info array data
//			Variation Team Array:	Team differences that can vary between different variations of a mission
//									The array is referenced through the mission info variations array data
//		NOTE: Data stored in MissionID order (then Variation ID order if relevant) then in TeamID order
//		NOTE: Each struct contains team data for one team, so a mission is likely to use two structs
//				one for Cops and one for Crooks - should Team specific data for individual Crook Teams need
//				to be stored then a mission will contain three structs - one for Cops, one for Vagos and one
//				for Lost (but I don't think this will be needed - we should be able to workaround this).
//		NOTE: The mission info will store an ENUM to detail how the team data will be stored, mainly it
//				will be Cop+Crook (so a Cop struct and a Crook struct), but since this is intended to store
//				FM and DM data too this will be versatile enough to create only one struct for a mission
//				if there is no differences based on teams.
// ===========================================================================================================

// The max number of entries on the team array for team data that is consistent across all variations of a mission
CONST_INT		MAX_MI_TEAMS			200

// The max number of entries on the team array for team data that can differ for different variations of a mission
CONST_INT		MAX_MI_VAR_TEAMS		225


// -----------------------------------------------------------------------------------------------------------

// OPTIONAL MISSION TEAM DATA FLAGS - For Team data that is consistent across all variations of a mission
CONST_INT		MIT_FLAG_PREMISSION_TXTMSG			0		// TRUE if the mission uses a pre-mission text message for this team, otherwise FALSE
CONST_INT		MIT_FLAG_PREMISSION_PHONECALL		1		// TRUE if the mission uses a pre-mission phonecall for this team, otherwise FALSE
CONST_INT		MIT_FLAG_MISSION_DETAILS_BOX		2		// TRUE if the mission details box should be displayed for this team, otherwise FALSE
CONST_INT		MIT_FLAG_INVITE_ONTO_MISSION		3		// TRUE if the player from this team will be invited onto the mission, FALSE if the player will be forced onto the mission
CONST_INT		MIT_FLAG_SPECIFIC_PLAYERS_ONLY		4		// TRUE if only specific player from this team join the mission, FALSE if all players from the team join the mission


// -----------------------------------------------------------------------------------------------------------

// Struct containing team data that is consistent across all variations of a mission
// *** IMPORTANT: New fields need to be cleared in the reset function of net_mission_info_initialiser.sch ***
STRUCT g_structTeamInfoPerMission
	INT					bitsTeamMissionFlags					// A bitfield for all optional flags associated with team details consistent across all variations of a mission
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Struct containing team data that can differ across all variations of a mission
// *** IMPORTANT: New fields need to be cleared in the reset function of net_mission_info_initialiser.sch ***
STRUCT g_structTeamInfoPerVariation
	INT					bitsTeamVariationFlags					// A bitfield for all optional flags associated with team details that can differ across all variations of a mission
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Team Info consistent across all variations of a mission
g_structTeamInfoPerMission			g_sTeamInfoPerMission[MAX_MI_TEAMS]
INT									g_numTeamInfoPerMissionInUse = 0

// Team Info that can differ across all variations of a mission
g_structTeamInfoPerVariation		g_sTeamInfoPerVariation[MAX_MI_VAR_TEAMS]
INT									g_numTeamInfoPerVariationInUse = 0




// ===========================================================================================================
//      Mission Info Variations data and array
//		NOTE: This is actual mission variations, not just the same mission at a different location
//		NOTE: Data stored in MissionID order with all variations of the same mission adjacent to each other
// ===========================================================================================================

// MP Mission Location Selection Method - describes how a mission location is chosen for the mission (assuming a mission variation hasn't been pre-selected)
ENUM g_eMPLocMethod
	MP_LOC_RND_VAR,							// Randomly choose a mission variation from the list of available variations
	MP_LOC_FAIR_VAR,						// Based on the teams involved in the mission, choose a Fair Location from the list of available variations
	MP_LOC_FAIR_XYZ,						// Based on the teams involved in the mission, choose a Fair Vector
	MP_LOC_SPECIAL,							// The location selection method is special in some way
	
	// Leave this at the bottom
	NO_MP_LOC								// There is no Location Selection Method specified
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Mission Team Configurations - various permutations of team requirements for the mission
ENUM g_eMPTeamConfig
	MP_TEAMS_COPS,							// Cops Only
	MP_TEAMS_COPS_1GANG,					// Cops and One Crook Gang
	MP_TEAMS_COPS_1GANG_MIN,				// Cops and at least one Crook Gang
	MP_TEAMS_COPS_1OR2GANGS,				// Cops and up to two Crook Gangs
	MP_TEAMS_1GANG,							// One Crook Gang
	MP_TEAMS_2GANGS,						// Two Crook Gangs
	MP_TEAMS_1GANG_MIN,						// At least one Crook Gang
	MP_TEAMS_1TEAM,							// One Team, which can be Cops or a Crook Gang
	MP_TEAMS_2TEAMS_MIN,					// At least two teams, Cops can optionally be one of the teams
	
	// Leave this at the bottom
	NO_MP_TEAM_CONFIG						// There are no team configuration details
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Mission Launch Teams - describes initial Teams placed onto the mission
ENUM g_eMPLaunchTeams
	MP_LAUNCH_ALL_PLAYERS,					// All players join at launch (initially added for Freemode missions)
	MP_LAUNCH_ALL_TEAMS,					// All Teams join at launch
	MP_LAUNCH_ALL_COPS,						// All Cops join at launch
	MP_LAUNCH_ALL_GANGS,					// All Crook Gangs join at launch
	MP_LAUNCH_ONE_TEAM,						// One Team joins at launch (can be Cops or a Crook Gang)
	MP_LAUNCH_ONE_GANG,						// One Crook Gang joins at launch
	
	// Leave this at the bottom
	NO_MP_LAUNCH_TEAMS						// There are no Launch Teams specified
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// OPTIONAL MISSION VARIATION FLAGS
CONST_INT		MI_FLAG_SECONDARY_COMMS					0		// TRUE if the second Crook gang on the mission gets a different communication from the first Crook gang
CONST_INT		MI_FLAG_INCLUSION_RADIUS				1		// TRUE if this mission should only allocate players that are within a certain range of the mission location
CONST_INT		MI_FLAG_RESERVED_BY_PLAYER_JOIN_FIRST	2		// TRUE if players that choose to play this mission should join first
CONST_INT		MI_FLAG_DATA_ON_CLOUD					3		// TRUE if this mission variation gets its data from the cloud
CONST_INT		MI_FLAG_ALLOW_REJOIN_AFTER_LEAVING		4		// TRUE if the plaeyr can rejoin this mission variation after leaving it
CONST_INT		MI_FLAG_INTERIOR_BLIP_ONLY				5		// TRUE if blip only appears when player nearby in an interior, FALSE if the blip follows normal rules


// -----------------------------------------------------------------------------------------------------------

// The max number allowed of all variations of all missions
CONST_INT		MAX_MP_MISSION_VARIATIONS			130


// -----------------------------------------------------------------------------------------------------------

// Struct containing all the mission data for a variation
// *** IMPORTANT: New fields need to be cleared in the reset function of net_mission_info_initialiser.sch ***
STRUCT g_structMissionInfoForVariations
	INT					bitsOptionalFlags					// A bitfield for all optional flags associated with a mission variation
	FLOAT				optionalRadius						// A radius used by any optional parameters that require a radius restriction
	g_eMPLocMethod		locMethod							// The method for choosing a location
	g_eMPTeamConfig		teamConfig							// The configuration of teams allowed to launch this mission
	g_eMPLaunchTeams	launchTeams							// The teams that should join the mission at launch
	INT					arrayPosFirstVarTeam				// The array position within the Variation Team Data array for the first piece of team data
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

g_structMissionInfoForVariations	g_sMissionVariations[MAX_MP_MISSION_VARIATIONS]
INT									g_numVariationsInUse = 0




// ===========================================================================================================
//      Mission Info data and array - contains pointers into the Variations array
//		NOTE: Data stored in MissionID order
// ===========================================================================================================

// MP Team Storage Requirements IDs
// This is used to decide how many Team Data structs a mission needs to contain all the team-specific variations of a mission.
// It's also used to identify which storage element contains the data for a specific team.
// For Cops and Crooks this will usually by MIT_CP_CR which allocates 2 stucts: Cop data and Crook data (Crook Gangs tend to share the same team setup details so one struct of storage can be used for all Crook Gangs)
// To store Cop data, use ID MIT_COPS, to store crook data use MIT_CROOKS (if necessary, we'll also create MIT_VAGOS and MIT_LOST)
// There will be a conversion function within net_mission_info_private that converts a team ID to the correct storage location so that data for a specific team can be retrieved.
// This is a bit confusing because TEAM_COP will usually get data from the first storage struct for a mission, but VAGOS and LOST will both retrieve their data from the second storage struct because it will be the same
//		for both Crook Gangs. If a mission does require specifically different Vagos and Lost data then we'll need to allocate three team storage structs for the mission: Cops, Vagos, Lost.
//		It's done this way to avoid memory being wasted - we usually don't need a struct of data for each individual Crook gang - this memory waste would have become worse if we introduced a third or a fourth gang.
// For Freemode we'll use MIT_ALL where the same team data storage is used for all players.
ENUM g_eMITeamStorage
	MIT_ALL,								// All Players - one team storage struct is used for all players, or accesses the already allocated All Players storage - this will be used for Freemode missions/variations which isn't team-specific
	MIT_COP,								// Cop - one team storage struct used for Cops, or accesses the already allocated Cop storage for a mission
	MIT_CROOKS,								// Crooks - one team storage struct used for All Crooks, or accesses the already allocated Crook storage for a mission
	MIT_CP_CR,								// Cop + Crook - one team storage struct for Cops, one struct for All Crooks when individual Crook Gang data doesn't differ from each other - not used for access
	
	// Leave this at the bottom
	NO_MIT_DATA								// There is no team storage struct requirements
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// MP Mission Instance Control - describes how many instances of a mission are allowed to run at any one time
ENUM g_eMPInstances
	MP_INST_ONE,							// Only every allows one instance of the mission to run
	MP_INST_ANY,							// As many instances of the mission as required can run
	MP_INST_UNIQUE,							// Another instance can run as long as the same variation of the mission isn't running
	
	// Leave this at the bottom
	NO_MP_INST								// There is no specific instance requirements set up
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// Struct containing the main data for the mission - most of this data is shared by all variations of the mission
// *** IMPORTANT: New Fields need to be cleared in the reset function of net_mission_info_initialiser.sch ***
STRUCT g_structMissionInfo
	BOOL				playersAreInTeams		// TRUE if all players are in Teams (ie: CnC missions and activities), FALSE if all mission players are Individuals (ie: Freemode missions and activities)
	INT					numVariations			// The number of variations of the mission
	INT					arrayPosFirstVar		// The array position within the Variations array of the first variation of the mission
	g_eMPInstances		instances				// The number of instances of the mission that can run at the same time
	g_eMITeamStorage	teamStorage				// Identifies the number of storage structs required for team-specific data for this mission
	INT					arrayPosFirstTeam		// The array position within the Consistent Team Data array for the first piece of team data
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// Reserve space for the MP_MISSIONS_ARRAY_MAX number of missions instead of the eMAX_NUM_MP_MISSION which will vary in size as we add and remove missions
// The ARRAY_MAX ensures the amount of global space remains constant so we should be able to add the CnC missions back into this when we introduce the DLC
g_structMissionInfo g_sMissionInfo[MP_MISSIONS_ARRAY_MAX]




