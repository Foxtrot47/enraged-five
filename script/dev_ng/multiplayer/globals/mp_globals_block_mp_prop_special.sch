USING "global_block_defines.sch"
USING "commands_network.sch"
USING "mp_globals_common_consts.sch"
USING "mp_globals_new_features_TU.sch"
USING "MP_Property_Global_Common.sch"

GLOBALS GLOBALS_BLOCK_MP_SPEC_PROP

// ========== PRIVATE YACHT DETAILS ==============
MP_PROPERTY_STRUCT InteriorPropStruct
CONST_INT MAX_YACHT_ENTRANCES 		1
INT g_iCustomAptVariantPIM = 1
INT g_iCurrentPropertyVariation = 1
BOOL g_bUpdateCustomApt = FALSE
BOOL g_bCustomAptVariantPreviewMode  = FALSE
BOOL g_bSoundYachtHorn = FALSE
BOOL g_bForceSafeCashPile = FALSE

//Current active minigame inside a clubhouse, (-1 no active minigame) ref - MINIGAME_TYPES_ENUM
//INT 	g_iClubhouseMGID = -1
//Current num of player
INT		g_iClubhouseDartsPlayerCount = 0

BOOL g_bDoGarageElevator

MP_YACHT_PROPERTY_STRUCT mpYachts[NUMBER_OF_PRIVATE_YACHTS]

#IF IS_DEBUG_BUILD
	BOOL g_bActivateAllPrivateYachts
	BOOL g_bMakeDefaultYachtFullyPimped
	INT g_iPrioritiseThisYacht = -1
	BOOL g_BuyPrivateYacht
	BOOL g_BuyPrivateTruck
	YACHT_APPEARANCE g_BuyYachtAppearance
	BOOL g_RemovePrivateYacht
	BOOL g_RemovePrivateTruck
	BOOL g_SetYachtAsRespawnLocation
	BOOL g_DrawYachtBoundingBoxes
	BOOL g_DrawYachtTopToFlag
	BOOL g_GetPlayerCoordsAsYachtOffset
	BOOL g_bUseDebugOffsetsForYacht
	BOOL g_DrawYachtZones
	BLIP_INDEX g_YachtZoneBlipID[iMAX_PRIVATE_YACHT_SPAWN_ZONES]
	BOOL g_bCreatedYachtZoneBlips
	BOOL g_bOutputYachtDetails
	BOOL g_bWarpToMyYacht
	BOOL g_bDoWarpToReAssign
	INT g_iReassignZone
	INT g_iSpawnOnLandVehicle
	BOOL g_bSpawnOnLandSet
	BOOL g_bSpawnOnLandCall
	BOOL g_bDrawVehicleSpawnLocations
	BOOL g_bDrawExteriorYachtSpawnLocations
	BOOL g_bSpawnInStandardSupervolito
	
	YACHT_DATA g_MCYachtData

	INT g_iMCYachtState
	BOOL g_bMCYachtCreate
	BOOL g_bMCYachtDelete
	BOOL g_bMCYachtUpdate
	
	INT g_iTotalYachtObjectsCreated
	INT g_iMaxYachtObjectsCreated
	
#ENDIF 

GLOBAL_PRIVATE_YACHT_DETAILS g_PrivateYachtDetails[NUMBER_OF_PRIVATE_YACHTS]
GLOBAL_PRIVATE_YACHT_VEHICLE_SPAWN_DETAILS	gPrivateYachtSpawnVehicleDetails[NUMBER_OF_PRIVATE_YACHT_VEHICLES]
YACHT_DOCKING_LOCATIONS gPrivateYachtDockingOffsets[NUMBER_OF_PRIVATE_YACHT_VEHICLE_DOCKS]
YACHT_SPAWN_LOCATIONS g_PrivateYachtSpawnLocationOffest[NUMBER_OF_SPAWN_LOCATIONS_ON_YACHT_EXTERIOR]
YACHT_SPAWN_LOCATIONS g_PrivateYachtSpawnLocationApartmentOffest[NUMBER_OF_SPAWN_LOCATIONS_YACHT_APARTMENT]
VECTOR g_PrivateYachtZoneCoords[iMAX_PRIVATE_YACHT_SPAWN_ZONES]
VECTOR g_PrivateYachtBuoysCoords[NUMBER_OF_PRIVATE_YACHT_BUOY]

CONST_INT OFF_PA_SPEECH_BUMP						0
CONST_INT OFF_PA_SPEECH_ADDGENERAL					1
CONST_INT OFF_PA_SPEECH_ADDLOCKER					2
CONST_INT OFF_PA_SPEECH_ADDQUARTERS					3
CONST_INT OFF_PA_SPEECH_ADDSAFE						4
CONST_INT OFF_PA_SPEECH_CHATOTHERS					5
CONST_INT OFF_PA_SPEECH_CHATPHONE					6
CONST_INT OFF_PA_SPEECH_CHATVIP						7
CONST_INT OFF_PA_SPEECH_DECOR						8
CONST_INT OFF_PA_SPEECH_DOOR						9
CONST_INT OFF_PA_SPEECH_FAREWELL					10
CONST_INT OFF_PA_SPEECH_GREET						11
CONST_INT OFF_PA_SPEECH_STYLE						12
CONST_INT OFF_PA_SPEECH_AGREE						13	//?
CONST_INT OFF_PA_SPEECH_BYE							14
CONST_INT OFF_PA_SPEECH_DISAGREE					15	//?
CONST_INT OFF_PA_SPEECH_FRIGHTENED_HIGH				16
CONST_INT OFF_PA_SPEECH_FRIGHTENED_MED				17
CONST_INT OFF_PA_SPEECH_HI							18
CONST_INT OFF_PA_SPEECH_SHOCKED_HIGH				19
CONST_INT OFF_PA_SPEECH_SHOCKED_MED					20
CONST_INT OFF_PA_SPEECH_GARAGE						21
CONST_INT OFF_PA_SPEECH_FLOORS						22
CONST_INT OFF_PA_SPEECH_MODSHOP						23
CONST_INT OFF_PA_SPEECH_APPUPDATE					24
CONST_INT OFF_PA_SPEECH_ADMITTANCE					25
CONST_INT OFF_PA_SPEECH_LOITER1						26
CONST_INT OFF_PA_SPEECH_LOITER2						27
CONST_INT OFF_PA_SPEECH_TOOCLOSE					28
CONST_INT OFF_PA_SPEECH_WELCOME_MALE				29
CONST_INT OFF_PA_SPEECH_WELCOME_FEMALE				30
CONST_INT OFF_PA_SPEECH_WELCOME_POSSE				31

CONST_INT CASINO_PA_SPEECH_PLAYED_GREETING_LINE		0
CONST_INT CASINO_PA_SPEECH_PLAYED_ADMITTANCE_LINE	1
CONST_INT CASINO_PA_SPEECH_PLAYED_GOODBYE_LINE		2
CONST_INT CASINO_PA_SPEECH_PLAYED_LOITER1_LINE		3
CONST_INT CASINO_PA_SPEECH_PLAYED_LOITER2_LINE		4
CONST_INT CASINO_PA_SPEECH_RESET					5

INT g_iOfficePersonalAssistantSpeechBitset			= 0




INT g_iCurrentPVSlotInProperty = -1
INT g_iPreviousLastUsedSlot = -1

ENDGLOBALS
//EOF
