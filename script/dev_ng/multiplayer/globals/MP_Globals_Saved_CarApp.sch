//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	MP_Globals_Saved_CarApp.sch									//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Structs and enums used to store the MP car app data			//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

CONST_INT MAX_MP_CARAPP_SLOTS 212 //MAX_MP_SAVED_VEHICLES (for search/update purposes)
CONST_INT MAX_MP_VEHICLE_APP_CAN_HANDLE 10
									
STRUCT MP_SAVED_CARAPP_STRUCT
	SOCIAL_CAR_APP_ORDER_DATA sCarAppOrder
	
	
	BOOL bCarHiddenInApp[MAX_MP_VEHICLE_APP_CAN_HANDLE]
	
	BOOL bUpdateMods
	
	BOOL bMultiplayerDataWiped
	BOOL bDeleteCarData
	BOOL bCarAppPlateSet
	
	TEXT_LABEL_15 tlCarAppPlateText
	INT iCarAppPlateBack
	
	INT iOrderCount
	INT iOrderVehicle
	
	INT iProcessSlot[MAX_MP_VEHICLE_APP_CAN_HANDLE]
	INT iSlotPriority[MAX_MP_VEHICLE_APP_CAN_HANDLE]
	INT iLastSavedVehUsed
	INT iNewSavedVehToProcess
	BOOL bNewSetupInitialised
	
ENDSTRUCT
//EOF
