//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	MP_Globals_Saved_Property.sch								//
//		AUTHOR			:	David Gentles												//
//		DESCRIPTION		:	Structs and enums used to store information on the MP	 	//
//							property													//
//////////////////////////////////////////////////////////////////////////////////////////


STRUCT MP_SAVED_PROPERTY_STRUCT_OLD
	BOOL								bTVOn					= TRUE
	INT 								iTVChannelID			= 5
	BOOL								bRadioOn				= FALSE
	INT 								iRadioStationID			= 0
	BOOL								bGarageTVOn				= FALSE
	INT 								iGarageTVChannelID		= 5
	BOOL								bGarageRadioOn			= TRUE
	INT 								iGarageRadioStationID	= 0
ENDSTRUCT


CONST_INT NUMBER_OF_MP_TV_CHANNELS 9
CONST_INT DEFAULT_MP_TV_CHANNEL NUMBER_OF_MP_TV_CHANNELS-2

STRUCT MP_SAVED_PROPERTY_STRUCT
	BOOL								bTVOn					= TRUE
	INT 								iTVChannelID			= DEFAULT_MP_TV_CHANNEL
	BOOL								bPenthouseTVOn			= TRUE
	INT 								iPenthouseTVChannelID	= DEFAULT_MP_TV_CHANNEL
	BOOL								bMediaRoomTVOn			= TRUE
	INT 								iMediaRoomTVChannelID	= DEFAULT_MP_TV_CHANNEL
	BOOL								bRadioOn[2]
	INT 								iRadioStationID[2]
	BOOL								bGarageTVOn				= FALSE
	INT 								iGarageTVChannelID		= DEFAULT_MP_TV_CHANNEL
	BOOL								bGarageRadioOn			= TRUE
	INT 								iGarageRadioStationID	= 0
	INT									iNumOfTimesSmoked		= 0
	INT									iNumOfTimesDrank		= 0
	INT									iNumOfTimesStrippers	= 0
	
	INT									iNumOfTimesSmokedSecond		= 0
	INT									iNumOfTimesDrankSecond		= 0
	INT									iNumOfTimesStrippersSecond	= 0
	
	INT									iNumOfTimesSmoked3		= 0
	INT									iNumOfTimesDrank3		= 0
	INT									iNumOfTimesStrippers3	= 0
	
	INT									iNumOfTimesSmoked4		= 0
	INT									iNumOfTimesDrank4		= 0
	INT									iNumOfTimesStrippers4	= 0
	
	INT									iNumOfTimesSmoked5		= 0
	INT									iNumOfTimesDrank5		= 0
	INT									iNumOfTimesStrippers5	= 0
	
	INT									iNumOfTimesSmokedYacht		= 0
	INT									iNumOfTimesDrankYacht		= 0
	INT									iNumOfTimesStrippersYacht	= 0
	
	TIMEOFDAY							TODLastCleaned
	INT									iShowersTakenWithHeadset = 0

	INT iArrayNumTimesDrank[MAX_MESS_PROPERTIES]      //MAX_OWNED_PROPERTIES too annoying doing these individually going to move to an array going forward.
	INT iArrayNumTimesSmoked[MAX_MESS_PROPERTIES]
	INT iArrayNumTimesStripper[MAX_MESS_PROPERTIES]
	
	TEXT_LABEL_63 ArenaWarsNamedVehicles[30]
ENDSTRUCT

//EOF
