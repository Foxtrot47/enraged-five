USING "rage_builtins.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   MP_globals_mission_control_TU.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains global variable instances for additional globals in the TitleUpdate block.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************


// Flag indicates this was the host and is attempting to pass hosting duties to another player
// NOTE: Added because if the hot fails to pass hosting duties to another player then they become
//			host again, but for those frames where the attempted pass was made there was no host
//			and events can go missing. So if a player becomes host and this flag is set then
//			it means they've attempted to pass hosting and it failed. In all other circumstances
//			this flag can be cleared.
BOOL g_hostAttemptedHostMigration		= FALSE

// Records the ID of the last known forced host resync
// The host updates it's ID by one everytime it forces a resync so teh client needs to remember what the last known ID was to know if a resync is needed
// NOTE: I originally had a server bool that I set for a frame, but the request was getting missed.
INT g_localKnownMissionControlResyncID	= 0



