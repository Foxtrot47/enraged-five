USING "SceneTool_debug.sch"

PROC Private_SaveNET_REALTY_6_SCENE(STRUCT_NET_REALTY_6_SCENE& scene, INT iBuildingID)
	
	TEXT_LABEL_63 str
	str = Private_Get_Net_Realty_Building_Name(iBuildingID)
	str += "	//"
	str += Private_Get_Net_Realty_Building_Description(iBuildingID)
	
	SceneTool_OpenDebugFile("Private_Get_NET_REALTY_6_SCENE()",				str)

		SceneTool_ExportPan(	"NET_REALTY_6_SCENE_PAN_null",				scene.mPans[NET_REALTY_6_SCENE_PAN_null])

		SceneTool_ExportCut(	"NET_REALTY_6_SCENE_CUT_shot",				scene.mCuts[NET_REALTY_6_SCENE_CUT_shot])

		SceneTool_ExportPlacer(	"NET_REALTY_6_SCENE_PLACER_startCoords",	scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords])
		SceneTool_ExportPlacer(	"NET_REALTY_6_SCENE_PLACER_driveTo",		scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo])

		SceneTool_ExportBool(	"bEnablePans[0]",				scene.bEnablePans[0])
		
		SceneTool_ExportFloat(	"fExitDelay",					scene.fExitDelay)

	SceneTool_CloseDebugFile("Private_Get_NET_REALTY_6_SCENE()")
	
ENDPROC

PROC Private_DebugPlayNET_REALTY_6_SCENE(STRUCT_NET_REALTY_6_SCENE& scene, INT iPropertyEntered, INT iDoorHash)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			MODEL_NAMES model
			INT class
			GET_RANDOM_VEHICLE_MODEL_IN_MEMORY(TRUE, model, class)
			
			IF model = DUMMY_MODEL_FOR_SCRIPT
				SWITCH GET_RANDOM_INT_IN_RANGE(0,10)
					CASE 0	model = TAILGATER	BREAK
					CASE 1	model = BUFFALO		BREAK
					CASE 2	model = COMET2		BREAK
					CASE 3	model = ISSI2		BREAK
					CASE 4	model = SENTINEL	BREAK
					CASE 5	model = DOMINATOR	BREAK
					CASE 6	model = CHEETAH		BREAK
					CASE 7	model = STINGERGT	BREAK
					CASE 8	model = INFERNUS	BREAK
					CASE 9	model = JACKAL		BREAK
					
					DEFAULT	model = TAILGATER	BREAK
				ENDSWITCH
			ENDIF
			
			REQUEST_MODEL(model)
			WHILE NOT HAS_MODEL_LOADED(model)
				REQUEST_MODEL(model)
				WAIT(0)
			ENDWHILE
			
			VEHICLE_INDEX hVehicle = CREATE_VEHICLE(model, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			SET_MODEL_AS_NO_LONGER_NEEDED(model)
			
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), hVehicle, VS_DRIVER)
		ENDIF
		
		GARAGE_CAR_LEAVE_CUTSCENE_STAGE garageCarLeaveCutStage = GARAGE_Car_LEAVE_CUTSCENE_INIT
		INT iGarageCutsceneBitset, iGarageCutTimer
		CAMERA_INDEX camGarageCutscene
		
		IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash) = DOORSTATE_INVALID
			IF DOOR_SYSTEM_GET_DOOR_STATE(iDoorHash) != DOORSTATE_UNLOCKED
				IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
					DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash,DOORSTATE_UNLOCKED,TRUE,TRUE)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_DOOR(iDoorHash)
				ENDIF
			ENDIF
		ELSE
			IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iDoorHash) != DOORSTATE_UNLOCKED
				IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
					DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash,DOORSTATE_UNLOCKED,TRUE,TRUE)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_DOOR(iDoorHash)
				ENDIF
			ENDIF
		ENDIF
		VEHICLE_INDEX theVeh
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
		ENDIF
		INT iOwnersProgress = 1
		WHILE NOT DO_GARAGE_CAR_LEAVE_THIS_CUTSCENE(scene,
				garageCarLeaveCutStage,
				iGarageCutsceneBitset, iGarageCutTimer,
				camGarageCutscene, iPropertyEntered, PLAYER_ID(),iOwnersProgress, theVeh)
			WAIT(0)
		ENDWHILE
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
								//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF

ENDPROC

PROC Private_Edit_NET_REALTY_6_SCENE(structSceneTool_Launcher& launcher, INT iPropertyEntered, INT iBuildingID, INT iDoorHash, OBJECT_INDEX &doorBlock, OBJECT_INDEX &garageBlock)

	// Get scene data
	STRUCT_NET_REALTY_6_SCENE		scene
	//structSceneTool tool
	
	// bOutputMissingNetRealtyScenes
	INT iBuilding
	REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
		IF iBuilding = 0
			
		ELSE
			
			PRINTSTRING("property ")
			PRINTSTRING(Private_Get_Net_Realty_Building_Name(iBuilding))
			PRINTSTRING(" ")
			
			IF Private_Get_NET_REALTY_6_SCENE(iBuilding, scene, TRUE)
				PRINTSTRING("has scene six, ")
			ELSE
				PRINTSTRING("missing scene six, ")
			ENDIF
			
			PRINTNL()
		ENDIF
	ENDREPEAT
	PRINTNL()
	//	//	//	//	//	//
	
	Private_Get_NET_REALTY_6_SCENE(iBuildingID, scene)

	// Add widgets
	START_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool,Private_Get_Net_Realty_Building_Name(iBuildingID))

		START_WIDGET_GROUP("Camera shots")
//				START_WIDGET_GROUP(		"Establishing shot")
//					ADD_WIDGET_PAN(		"Establish - ",				g_sAmMpPropertyExtSceneTool,scene.mPans,			ENUM_TO_INT(NET_REALTY_6_SCENE_PAN_walkOut))
//				STOP_WIDGET_GROUP()
			START_WIDGET_GROUP(		"Establishing shot")
				ADD_WIDGET_CUT(		"Establish - ",				g_sAmMpPropertyExtSceneTool,scene.mCuts,			ENUM_TO_INT(NET_REALTY_6_SCENE_CUT_shot))
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
//			START_WIDGET_GROUP("Markers")
//				ADD_WIDGET_MARKER(		"MARKER_walkTo",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,		ENUM_TO_INT(NET_REALTY_6_SCENE_MARKER_walkTo))
//			STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Placers")
			ADD_WIDGET_PLACER(		"PLACER_startCoords",		g_sAmMpPropertyExtSceneTool,scene.mPlacers,		ENUM_TO_INT(NET_REALTY_6_SCENE_PLACER_startCoords))
			ADD_WIDGET_PLACER(		"PLACER_driveTo",		g_sAmMpPropertyExtSceneTool,scene.mPlacers,		ENUM_TO_INT(NET_REALTY_6_SCENE_PLACER_driveTo))
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("*sliders*")
			START_WIDGET_GROUP("Marker")
//					START_WIDGET_GROUP("MARKER_walkTo")
//						ADD_WIDGET_VECTOR_SLIDER(	"MARKER_walkTo", scene.mMarkers[NET_REALTY_6_SCENE_MARKER_walkTo].vPos, -6000, 6000, 0.01)
//					STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Placer")
				START_WIDGET_GROUP("PLACER_startCoords")
					ADD_WIDGET_VECTOR_SLIDER(	"PLACER_startCoords",	scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("PLACER_driveTo")
					ADD_WIDGET_VECTOR_SLIDER(	"PLACER_driveTo",	scene.mPlacers[NET_REALTY_6_SCENE_PLACER_driveTo].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
	STOP_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool)
	
	// Warp to player pos marker
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].fRot)
	ENDIF
	
	// Update widgets
	WHILE launcher.bEnableTool
	
		// Update system
		SWITCH SceneTool_UpdateTool(g_sAmMpPropertyExtSceneTool)
			
			CASE SCENETOOL_ACTION_RunScene
				Private_DebugPlayNET_REALTY_6_SCENE(scene, iPropertyEntered, iDoorHash)
			BREAK
			
			CASE SCENETOOL_ACTION_SaveScene
				Private_SaveNET_REALTY_6_SCENE(scene, iBuildingID)
			BREAK
			
			CASE SCENETOOL_ACTION_UpdateData
				SceneTool_UpdatePans(g_sAmMpPropertyExtSceneTool, scene.mPans)
				SceneTool_UpdateCuts(g_sAmMpPropertyExtSceneTool, scene.mCuts)
				SceneTool_UpdateMarkers(g_sAmMpPropertyExtSceneTool, scene.mMarkers)
				SceneTool_UpdatePlacers(g_sAmMpPropertyExtSceneTool, scene.mPlacers)
			BREAK
		
		ENDSWITCH
				
		// Draw markers
	//	SceneTool_DrawMarker2 (g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(NET_REALTY_6_SCENE_MARKER_walkTo),			200, 0, 200, scene.mPlacers[NET_REALTY_6_SCENE_PLACER_startCoords].vPos)
		
		// Draw placers
		SceneTool_DrawPlacer (g_sAmMpPropertyExtSceneTool, scene.mPlacers,	ENUM_TO_INT(NET_REALTY_6_SCENE_PLACER_startCoords),		255, 0, 75)
		SceneTool_DrawPlacer (g_sAmMpPropertyExtSceneTool, scene.mPlacers,	ENUM_TO_INT(NET_REALTY_6_SCENE_PLACER_driveTo),		255, 0, 75)
		KILL_BLOCKING_OBJECTS(doorBlock,garageBlock)
		WAIT(0)

	ENDWHILE
	
	// Cleanup system
	SWITCH SceneTool_DestroyTool(g_sAmMpPropertyExtSceneTool)
		CASE SCENETOOL_ACTION_SaveScene
			Private_SaveNET_REALTY_6_SCENE(scene, iBuildingID)
		BREAK
	ENDSWITCH

ENDPROC

