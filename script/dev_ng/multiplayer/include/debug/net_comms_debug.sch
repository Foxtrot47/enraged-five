// This header shouldn't be in the release project anyway, but wrap it up just in case
#IF IS_DEBUG_BUILD



USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Comms_Debug.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all MP Communications debug functionality.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      MP Convert Enum to String Routines
// ===========================================================================================================

// PURPOSE: Converts the MP Communication Type to a string
//
// INPUT PARAMS:        paramCommsType          The Comms Type ID
// RETURN VALUE:        STRING                  The ID as a string
FUNC STRING Convert_MP_Comms_Type_To_String(INT paramCommsType)

    // Can't use a switch because it is legal for different comms types to share the same value (used for prioritisation)
    IF (paramCommsType = NO_MP_COMMS_TYPE)
        RETURN ("UNDEFINED")
    ENDIF
    
    IF (paramCommsType = MP_COMMS_TYPE_STANDARD_MESSAGE)
        RETURN ("Standard Message")
    ENDIF
    
    IF (paramCommsType = MP_COMMS_TYPE_OFFER_MISSION)
        RETURN ("Offer Mission")
    ENDIF
    
    IF (paramCommsType = MP_COMMS_TYPE_FORCE_ONTO_MISSION)
        RETURN ("Force Onto Mission")
    ENDIF

    IF (paramCommsType = MP_COMMS_TYPE_EOM_MESSAGE)
        RETURN ("End Of Mission Message")
    ENDIF

    IF (paramCommsType = MP_COMMS_TYPE_TXTMSG)
        RETURN ("Text Message")
    ENDIF

    IF (paramCommsType = MP_COMMS_TYPE_REPLY_TXTMSG)
        RETURN ("Reply Text Message")
    ENDIF
        
    IF (paramCommsType = MP_COMMS_TYPE_EMAIL)
        RETURN ("Email")
    ENDIF
    
    RETURN ("ERROR: UNKNOWN MP COMMS TYPE - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Converts the MP Communication Device to a string
//
// INPUT PARAMS:        paramDevice             The Comms Device ID
// RETURN VALUE:        STRING                  The ID as a string
FUNC STRING Convert_MP_Comms_Device_To_String(g_eMPCommsDevice paramDevice)

    SWITCH (paramDevice)
        CASE MP_COMMS_DEVICE_DEFAULT
            RETURN ("Default Device For Message Type")
            
        CASE MP_COMMS_DEVICE_CELLPHONE
            RETURN ("Cellphone")
            
        CASE MP_COMMS_DEVICE_RADIO
            RETURN ("Radio")
            
        CASE MP_COMMS_DEVICE_TXTMSG
            RETURN ("Cellphone Txtmsg")
            
        CASE MP_COMMS_DEVICE_EMAIL
            RETURN ("Cellphone Email")
            
        CASE MP_COMMS_DEVICE_UNKNOWN
            RETURN ("UNDEFINED")
    ENDSWITCH
    
    // Unknown
    RETURN ("ERROR: UNKNOWN DEVICE TYPE - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Converts an MP Contact to a string
//
// INPUT PARAMS:        paramCharSheetID        The CharSheet ID
// RETURN VALUE:        STRING                  The ID as a string
FUNC STRING Convert_MP_CharSheetID_To_String(enumCharacterList paramCharSheetID)

    SWITCH (paramCharSheetID)
        CASE NO_CHARACTER
            RETURN ("No CharSheetID")
        
        CASE CHAR_MP_BIKER_BOSS
            RETURN ("MP Biker Boss")
            
        CASE CHAR_MP_FAM_BOSS
            RETURN ("MP Families Boss")
            
        CASE CHAR_MP_MEX_BOSS
            RETURN ("MP Mexican Boss")
            
        CASE CHAR_MP_PROF_BOSS
            RETURN ("MP Professionals Boss")
            
        /* Removed by Steve T #1492126
        CASE CHAR_MP_BIKER_LT
            RETURN ("MP Biker Lieutenant")
        */
		
		/* Removed by Steve T. CHAR_MP_MEX_LT is redundant and has been renamed as the female Personal Assistant, CHAR_MP_P_ASSISTF, for executive pack. See 2769151.
        CASE CHAR_MP_MEX_LT
            RETURN ("MP Mexican Lieutenant")
		*/
        
        CASE CHAR_LESTER
            RETURN ("Lester")
            
        /* Removed by Steve T #1492126
        CASE CHAR_MP_MEX_MECHANIC
            RETURN ("MP Mexican Mechanic")
        */
            
        CASE CHAR_MP_STRETCH
            RETURN ("MP Stretch (Families Car Dropoff)")

		/*   Removed by Steve T #2686174
        CASE CHAR_MP_BIKER_MECHANIC
            RETURN ("MP Biker Mechanic")
		*/
            
        CASE CHAR_MP_SNITCH
            RETURN ("MP Snitch")
            
        /* Removed by Steve T #1492126
        CASE CHAR_MP_BIKER_DRIVER
            RETURN ("MP Biker Driver")
        */
          
        /* Removed by Steve T #1492126  
        CASE CHAR_MP_COP_DRIVER
            RETURN ("MP Cop Driver")
        */
        
        /* Removed by Steve T #1492126    
        CASE CHAR_MP_MEX_DRIVER
            RETURN ("MP Mexican Driver")
        */

        //This CHAR_ENUM was originally CHAR_MP_FIB_CONTACT but have renamed for 2568241
        CASE CHAR_YACHT_CAPTAIN
            RETURN ("MP Yacht Captain")
            
        CASE CHAR_MP_ARMY_CONTACT
            RETURN ("MP Army Contact")
            

        /* Removed by Steve T #1492126
        CASE CHAR_MP_VIPTOUR_VIP
            RETURN ("MP VIP_Tour VIP")
        */
            
        //This CHAR_ENUM was originally CHAR_MP_ROBERTO but have renamed for 2519183
        CASE CHAR_BENNYS_OMW
            RETURN ("Bennys OMW")
            
        CASE CHAR_MP_RAY_LAVOY
            RETURN ("MP Ray Lavoy")
            
        CASE CHAR_MP_FM_CONTACT
            RETURN ("MP FM Contact")
            
        CASE CHAR_MP_BRUCIE
            RETURN ("MP Brucie")
            
        CASE CHAR_MP_MERRYWEATHER
            RETURN ("MP Merryweather")
            
        CASE CHAR_MP_GERALD
            RETURN ("MP Gerald")
        
        CASE CHAR_MP_STRIPCLUB_PR
            RETURN ("MP StripClub PR")
            
        CASE CHAR_MP_MECHANIC
            RETURN ("MP Mechanic")
			
        CASE CHAR_BIKER_CH2
            RETURN ("MP CHAR_BIKER_CH2")
			
        CASE CHAR_BIKER_CH1
            RETURN ("MP CHAR_BIKER_CH1")
            
        CASE CHAR_LS_CUSTOMS
            RETURN ("LS Customs (Car App)")
			
		CASE CHAR_MP_AGENT_14
			RETURN ("Agent 14")
			
		CASE CHAR_ULP
			RETURN ("MP CHAR_ULP")
		
		#IF FEATURE_DLC_2_2022
		CASE CHAR_DAX
			RETURN ("MP CHAR_DAX")
		#ENDIF
		
    ENDSWITCH
    
    // Unknown
    RETURN ("ERROR: UNKNOWN MP CHARSHEET FOR COMMS - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Converts the MP Communication Reply to a string
//
// INPUT PARAMS:        paramReply              The Comms Reply State ID
// RETURN VALUE:        STRING                  The ID as a string
FUNC STRING Convert_MP_Comms_Reply_To_String(g_eMPCommsReply paramReply)

    SWITCH (paramReply)
        CASE NO_MP_COMMS_REPLY_REQUIRED
            RETURN ("No Reply Required")
            
        CASE MP_COMMS_REPLY_REQUIRED
            RETURN ("Reply Required")
            
        CASE MP_COMMS_REPLY_YES
            RETURN ("Player Replied Yes")
            
        CASE MP_COMMS_REPLY_NO
            RETURN ("Player Replied No")
            
        CASE MP_COMMS_REPLY_SPECIAL
            RETURN ("Player Special Reply")
    ENDSWITCH
    
    // Unknown
    RETURN ("ERROR: UNKNOWN DEVICE TYPE - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Converts the MP Stored Communication Type to a string
//
// INPUT PARAMS:        paramStoredType         The Comms Stored Comms Type ID
// RETURN VALUE:        STRING                  The ID as a string
FUNC STRING Convert_Stored_Comms_Type_To_String(g_eMPStoredCommsTypes paramStoredType)
    
    SWITCH (paramStoredType)
        CASE SCT_GENERAL_COMMS
            RETURN ("General Stored Comms")
        
        // The 'Max' value - shouldn't get displayed
        CASE MAX_STORED_COMMS_TYPES
            RETURN ("ILLEGAL")
    ENDSWITCH
    
    // Unknown
    RETURN ("ERROR: UNKNOWN STORED COMMUNICATION TYPE - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Converts the MP Stored Communication Type (as an INT) to a string
//
// INPUT PARAMS:        paramStoredTypeAsInt    The Comms Stored Comms Type ID (as an INT)
// RETURN VALUE:        STRING                  The ID as a string
FUNC STRING Convert_Stored_Comms_Type_As_Int_To_String(INT paramStoredTypeAsInt)

    g_eMPStoredCommsTypes theStoredCommsType = INT_TO_ENUM(g_eMPStoredCommsTypes, paramStoredTypeAsInt)
    
    RETURN (Convert_Stored_Comms_Type_To_String(theStoredCommsType))
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Converts the MP Txtmsg Type to a string
//
// INPUT PARAMS:        paramType               The MP Txtmsg Type ID
// RETURN VALUE:        STRING                  The ID as a string
FUNC STRING Convert_MP_Txtmsg_Type_To_String(INT paramType)

    SWITCH (paramType)
        CASE MP_TXTMSG_TYPE_FROM_PLAYER
            RETURN ("From Player")
            
        CASE MP_TXTMSG_TYPE_FROM_CHARSHEETID
            RETURN ("From Story Character")
            
        CASE NO_MP_TXTMSG_TYPE
            RETURN ("UNDEFINED")
    ENDSWITCH
    
    // Unknown
    RETURN ("ERROR: UNKNOWN MP TXTMSG TYPE - ADD TO FUNCTION")

ENDFUNC





// ===========================================================================================================
//      MP Communications System Standard Debug Output Layout
// ===========================================================================================================

// PURPOSE: Displays the details passed in to the MP Communication functions
//
// INPUT PARAMS:        paramScriptname             The name of the script that requested the communication
//                      paramGroupID                The Subtitle Group ID field from dialogueStar
//                      paramRootID                 The conversation Root field from dialogueStar
//                      paramCommsType              An INT representing the type of Comms - also used to prioritise (CONST_INTs defined in MP_globals_Comms.sch)
//                      paramDevice                 The Device ID with which to play the message
//                      paramModifiers              Any flags that modify the default behaviour
//                      paramComponentTextLabel     The Substring Component: TextLabel (as String)
//                      paramComponentIsPlayerName  The textLabel substring component is a player name
//                      paramComponentInt           The Substring Component: Int
//                      paramComponentString1       A substring component: Literal String 1
//                      paramComponentString2       A substring component: Literal String 2
PROC Debug_Output_MP_Comms_Details(STRING paramScriptName, STRING paramGroupID, STRING paramRootID, INT paramCommsType, g_eMPCommsDevice paramDevice, INT paramModifiers, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName, INT paramComponentInt, STRING paramComponentString1, STRING paramComponentString2)

    NET_PRINT("      From Script: ") NET_PRINT(paramScriptName) NET_NL()
    NET_PRINT("      Subtitle Group ID: ") NET_PRINT(paramGroupID) NET_PRINT("   Root: ") NET_PRINT(paramRootID) NET_NL()
    
    NET_PRINT("         Substring Component String: ")
    IF (Is_String_Null_Or_Empty(paramComponentTextLabel))
        NET_PRINT("[NO STRING COMPONENT]") NET_NL()
    ELSE
        IF NOT (paramComponentIsPlayerName)
            NET_PRINT(paramComponentTextLabel) NET_PRINT(" [") NET_PRINT(GET_STRING_FROM_TEXT_FILE(paramComponentTextLabel)) NET_PRINT("]") NET_NL()
        ELSE
            NET_PRINT(paramComponentTextLabel) NET_PRINT(" [PLAYER NAME]") NET_NL()
        ENDIF
    ENDIF
    
    NET_PRINT("         Substring Component Int: ")
    IF (paramComponentInt = NO_INT_SUBSTRING_COMPONENT_VALUE)
        NET_PRINT("[NO INT COMPONENT]") NET_NL()
    ELSE
        NET_PRINT_INT(paramComponentInt) NET_NL()
    ENDIF
    
    NET_PRINT("         Additional Component String1: ")
    IF (Is_String_Null_Or_Empty(paramComponentString1))
        NET_PRINT("[NO STRING COMPONENT - 1]") NET_NL()
    ELSE
        NET_PRINT(paramComponentString1) NET_NL()
    ENDIF
    
    NET_PRINT("         Additional Component String2: ")
    IF (Is_String_Null_Or_Empty(paramComponentString2))
        NET_PRINT("[NO STRING COMPONENT - 2]") NET_NL()
    ELSE
        NET_PRINT(paramComponentString2) NET_NL()
    ENDIF
    
    NET_PRINT("      MP Communication Type: ") NET_PRINT(Convert_MP_Comms_Type_To_String(paramCommsType)) NET_NL()
    NET_PRINT("      MP Communication Device: ") NET_PRINT(Convert_MP_Comms_Device_To_String(paramDevice)) NET_NL()
    
    // Any Modifiers
    IF NOT (paramModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR)
        NET_PRINT("      MP Communication Modifiers:")
        IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_COMMS_FORCE_ONSCREEN))
            NET_PRINT("  [FORCE ONSCREEN]")
        ENDIF
        IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_TXTMSG_ALLOW_BARTER))
            NET_PRINT("  [ALLOW BARTER]")
        ENDIF
        IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_COMMS_LOCKED))
            NET_PRINT("  [LOCKED]")
        ENDIF
        IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_NO_HANGUP_FOR_PHONECALL))
            NET_PRINT("  [NO HANGUP]")
        ENDIF
        IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_IGNORE_RECENT_INVITE))
            NET_PRINT("  [NO RECENT INVITE DELAY]")
        ENDIF
        NET_NL()
    ENDIF
            
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Displays the stored details for the current communication
PROC Debug_Output_MP_Comms_Details_For_Existing_Request()

    STRING unknownStringComponent = "Unknown String - not stored"
    Debug_Output_MP_Comms_Details(g_WIDGET_MP_Comms_ScriptName, g_WIDGET_MP_Comms_GroupID, g_sCommsMP.priorityComms.ccRootID, g_sCommsMP.priorityComms.ccType, g_sCommsMP.priorityComms.ccDevice, g_sCommsMP.priorityComms.ccModifiers, g_WIDGET_MP_Comms_Component_TL, FALSE, g_WIDGET_MP_Comms_Component_INT, unknownStringComponent, unknownStringComponent)

ENDPROC




// ===========================================================================================================
//      MP Communications System Widgets
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      MP End of Mission Message Debug Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Updates the End of Mission Message text widgets
PROC Update_MP_End_Of_Mission_Text_Widgets()

    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_EOM_ScriptName))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_EOM_ScriptName,      g_WIDGET_EOM_ScriptName)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_EOM_CharacterID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_EOM_CharacterID,     g_sCommsMP.EOM.characterEOM)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_EOM_SpeakerID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_EOM_SpeakerID,       g_sCommsMP.EOM.speakerEOM)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_EOM_GroupID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_EOM_GroupID,         g_sCommsMP.EOM.groupEOM)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_EOM_ConvRootID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_EOM_ConvRootID,      g_sCommsMP.EOM.rootEOM)
    ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Create the End of Mission Message optional widgets
PROC Create_MP_End_Of_Mission_Optional_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Create_MP_End_Of_Mission_Optional_Widgets") NET_NL()

    IF (DOES_WIDGET_GROUP_EXIST(g_optionalEOMMessageWidgetGroup))
        NET_PRINT("           OPTIONAL WIDGET GROUP ALREADY EXISTS") NET_NL()
    
        EXIT
    ENDIF
    
    // Create the Widgets, place them inside the correct widget group
    IF (DOES_WIDGET_GROUP_EXIST(g_mainEOMMessageWidgetGroup))
        SET_CURRENT_WIDGET_GROUP(g_mainEOMMessageWidgetGroup)
    ENDIF

    // Create all the widgets
    g_optionalEOMMessageWidgetGroup = START_WIDGET_GROUP("Optional Widgets")
        ADD_WIDGET_BOOL("EOM Message Received?", g_WIDGET_EOM_Message_Received)
        ADD_WIDGET_BOOL("EOM Message Active?", g_WIDGET_EOM_Message_Active)
        
        START_WIDGET_GROUP("Message Details")
            g_WIDGETID_EOM_ScriptName       = ADD_TEXT_WIDGET("Requested By Script")
            g_WIDGETID_EOM_CharacterID      = ADD_TEXT_WIDGET("Character ID")
            g_WIDGETID_EOM_SpeakerID        = ADD_TEXT_WIDGET("Speaker ID")
            g_WIDGETID_EOM_GroupID          = ADD_TEXT_WIDGET("Subtitle Group ID")
            g_WIDGETID_EOM_ConvRootID       = ADD_TEXT_WIDGET("Conv. Root ID")
        STOP_WIDGET_GROUP()
    STOP_WIDGET_GROUP()
    
    // Unset the widget group again
    CLEAR_CURRENT_WIDGET_GROUP(g_mainEOMMessageWidgetGroup)
    
    // Update the text widgets with the latest data
    Update_MP_End_Of_Mission_Text_Widgets()
    
    // Update the state variable
    g_holdEOMMessageWidgetsState = TRUE
    
    NET_PRINT("           OPTIONAL WIDGET GROUP CREATED") NET_NL()
        
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Destroy the End of Mission Message optional widgets
PROC Destroy_MP_End_Of_Mission_Optional_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Destroy_MP_End_Of_Mission_Optional_Widgets") NET_NL()

    IF NOT (DOES_WIDGET_GROUP_EXIST(g_optionalEOMMessageWidgetGroup))
        NET_PRINT("           OPTIONAL WIDGET GROUP DOESN'T EXIST") NET_NL()
    
        EXIT
    ENDIF

    // Destroy all the widgets
    // ...delete the text widgets
    DELETE_TEXT_WIDGET(g_WIDGETID_EOM_ScriptName)
    DELETE_TEXT_WIDGET(g_WIDGETID_EOM_CharacterID)
    DELETE_TEXT_WIDGET(g_WIDGETID_EOM_SpeakerID)
    DELETE_TEXT_WIDGET(g_WIDGETID_EOM_GroupID)
    DELETE_TEXT_WIDGET(g_WIDGETID_EOM_ConvRootID)
    
    // ...delete the optional group
    DELETE_WIDGET_GROUP(g_optionalEOMMessageWidgetGroup)
    g_optionalEOMMessageWidgetGroup = NULL
    
    // Update the state variable
    g_holdEOMMessageWidgetsState = FALSE
    
    NET_PRINT("           OPTIONAL WIDGET GROUP DELETED") NET_NL()
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Sets up the MP End of Mission message widgets
PROC Create_MP_End_Of_Mission_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Create_MP_End_Of_Mission_Widgets") NET_NL()
    
    // Clear out the text widget datafields
    g_WIDGET_EOM_ScriptName     = ""

    g_mainEOMMessageWidgetGroup = START_WIDGET_GROUP("KGM: MP End Of Mission Messages")
        ADD_WIDGET_BOOL("Switch On EOM Message Widgets?", g_WIDGET_EOM_Switch_Widgets_On)
    STOP_WIDGET_GROUP()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Destroy the End of Mission Message widgets
PROC Destroy_MP_End_Of_Mission_Widgets()

    Destroy_MP_End_Of_Mission_Optional_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Destroy_MP_End_Of_Mission_Widgets") NET_NL()

    IF NOT (DOES_WIDGET_GROUP_EXIST(g_mainEOMMessageWidgetGroup))
        NET_PRINT("           MAIN WIDGET GROUP DOESN'T EXIST") NET_NL()
    
        EXIT
    ENDIF
    
    // ...delete the main group
    DELETE_WIDGET_GROUP(g_mainEOMMessageWidgetGroup)
    g_mainEOMMessageWidgetGroup = NULL
    
    NET_PRINT("           MAIN WIDGET GROUP DELETED") NET_NL()
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintains any MP End of Mission widgets that need regular updating or checking
PROC Maintain_MP_End_Of_Mission_Widgets()

    g_WIDGET_EOM_Message_Received   = (IS_BIT_SET(g_sCommsMP.EOM.bitflagsEOM, MP_EOM_MESSAGE_RECEIVED))
    g_WIDGET_EOM_Message_Active     = (IS_BIT_SET(g_sCommsMP.EOM.bitflagsEOM, MP_EOM_MESSAGE_ACTIVE))
    
    // Check if the optional widgets should be switched on or off
    IF (g_WIDGET_EOM_Switch_Widgets_On = g_holdEOMMessageWidgetsState)
        EXIT
    ENDIF
    
    // Switch them on?
    IF (g_WIDGET_EOM_Switch_Widgets_On)
        Create_MP_End_Of_Mission_Optional_Widgets()
        EXIT
    ENDIF
    
    // Switch them off
    Destroy_MP_End_Of_Mission_Optional_Widgets()

ENDPROC





// -----------------------------------------------------------------------------------------------------------
//      MP Stored Communication Debug Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Updates the Stored Communication text widgets for one Stored Communication Type
//
// INPUT PARAMS:    paramTypeAsInt              The array type for this stored communication
PROC Update_MP_Stored_Communication_Text_Widgets(INT paramTypeAsInt)

    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_StoredComms_ScriptName[paramTypeAsInt]))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_StoredComms_ScriptName[paramTypeAsInt],      g_WIDGET_StoredComms_ScriptName[paramTypeAsInt])
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_StoredComms_CharacterID[paramTypeAsInt]))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_StoredComms_CharacterID[paramTypeAsInt],     g_sCommsMP.storedComms[paramTypeAsInt].scCharacter)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_StoredComms_SpeakerID[paramTypeAsInt]))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_StoredComms_SpeakerID[paramTypeAsInt],       g_sCommsMP.storedComms[paramTypeAsInt].scSpeaker)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_StoredComms_CharSheetID[paramTypeAsInt]))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_StoredComms_CharSheetID[paramTypeAsInt],     g_WIDGET_StoredComms_CharSheetID[paramTypeAsInt])
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_StoredComms_GroupID[paramTypeAsInt]))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_StoredComms_GroupID[paramTypeAsInt],         g_sCommsMP.storedComms[paramTypeAsInt].scGroup)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_StoredComms_ConvRootID[paramTypeAsInt]))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_StoredComms_ConvRootID[paramTypeAsInt],      g_sCommsMP.storedComms[paramTypeAsInt].scRoot)
    ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Sets up the optional MP Stored Communication widgets for all Stored Comms Types
PROC Create_MP_Stored_Communication_Optional_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Create_MP_Stored_Communication_Optional_Widgets") NET_NL()

    IF (DOES_WIDGET_GROUP_EXIST(g_optionalStoredCommsWidgetGroup))
        NET_PRINT("           OPTIONAL WIDGET GROUP ALREADY EXISTS") NET_NL()
    
        EXIT
    ENDIF
    
    // Create the Widgets, place them inside the correct widget group
    IF (DOES_WIDGET_GROUP_EXIST(g_mainStoredCommsWidgetGroup))
        SET_CURRENT_WIDGET_GROUP(g_mainStoredCommsWidgetGroup)
    ENDIF

    // Create all the widgets
    g_optionalStoredCommsWidgetGroup = START_WIDGET_GROUP("Optional Widgets")
        INT tempLoop = 0
        REPEAT MAX_STORED_COMMS_TYPES tempLoop
            START_WIDGET_GROUP(Convert_Stored_Comms_Type_As_Int_To_String(tempLoop))
                ADD_WIDGET_BOOL("Stored Communication Received?",               g_WIDGET_StoredComms_Received[tempLoop])
                ADD_WIDGET_BOOL("Stored Communication Active?",                 g_WIDGET_StoredComms_Active[tempLoop])
                ADD_WIDGET_BOOL("Stored Communication Initial Delay Expired?",  g_WIDGET_StoredComms_Delay_Expired[tempLoop])
                
                START_WIDGET_GROUP("Stored Communication Details")
                    g_WIDGETID_StoredComms_ScriptName[tempLoop]     = ADD_TEXT_WIDGET("Requested By Script")
                    g_WIDGETID_StoredComms_CharacterID[tempLoop]    = ADD_TEXT_WIDGET("Character ID")
                    g_WIDGETID_StoredComms_SpeakerID[tempLoop]      = ADD_TEXT_WIDGET("Speaker ID")
                    g_WIDGETID_StoredComms_CharSheetID[tempLoop]    = ADD_TEXT_WIDGET("Caller Name On Cellphone")
                    g_WIDGETID_StoredComms_GroupID[tempLoop]        = ADD_TEXT_WIDGET("Subtitle Group ID")
                    g_WIDGETID_StoredComms_ConvRootID[tempLoop]     = ADD_TEXT_WIDGET("Conv. Root ID")
                STOP_WIDGET_GROUP()
            STOP_WIDGET_GROUP()
        ENDREPEAT
    STOP_WIDGET_GROUP()
    
    // Unset the widget group again
    CLEAR_CURRENT_WIDGET_GROUP(g_mainStoredCommsWidgetGroup)
    
    // Update the text widgets with the latest data
    REPEAT MAX_STORED_COMMS_TYPES tempLoop
        Update_MP_Stored_Communication_Text_Widgets(tempLoop)
    ENDREPEAT
    
    // Update the state variable
    g_holdStoredCommsWidgetsState = TRUE
    
    NET_PRINT("           OPTIONAL WIDGET GROUP CREATED") NET_NL()
        
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Destroy the Stored Communication optional widgets
PROC Destroy_MP_Stored_Communication_Optional_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Destroy_MP_Stored_Communication_Optional_Widgets") NET_NL()

    IF NOT (DOES_WIDGET_GROUP_EXIST(g_optionalStoredCommsWidgetGroup))
        NET_PRINT("           OPTIONAL WIDGET GROUP DOESN'T EXIST") NET_NL()
    
        EXIT
    ENDIF

    // Destroy all the widgets
    // ...delete the text widgets
    INT tempLoop = 0
    REPEAT MAX_STORED_COMMS_TYPES tempLoop
        DELETE_TEXT_WIDGET(g_WIDGETID_StoredComms_ScriptName[tempLoop])
        DELETE_TEXT_WIDGET(g_WIDGETID_StoredComms_CharacterID[tempLoop])
        DELETE_TEXT_WIDGET(g_WIDGETID_StoredComms_SpeakerID[tempLoop])
        DELETE_TEXT_WIDGET(g_WIDGETID_StoredComms_CharSheetID[tempLoop])
        DELETE_TEXT_WIDGET(g_WIDGETID_StoredComms_GroupID[tempLoop])
        DELETE_TEXT_WIDGET(g_WIDGETID_StoredComms_ConvRootID[tempLoop])
    ENDREPEAT
    
    // ...delete the optional group
    DELETE_WIDGET_GROUP(g_optionalStoredCommsWidgetGroup)
    g_optionalStoredCommsWidgetGroup = NULL
    
    // Update the state variable
    g_holdStoredCommsWidgetsState = FALSE
    
    NET_PRINT("           OPTIONAL WIDGET GROUP DELETED") NET_NL()
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Sets up the MP Stored Communication widgets
PROC Create_MP_Stored_Communication_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Create_MP_Stored_Communication_Widgets") NET_NL()
    
    // Clear out the text widget datafields
    INT tempLoop = 0
    REPEAT MAX_STORED_COMMS_TYPES tempLoop
        // Clear out the text widgets
        g_WIDGET_StoredComms_ScriptName[tempLoop]   = ""
        g_WIDGET_StoredComms_CharSheetID[tempLoop]  = ""
    ENDREPEAT

    g_mainStoredCommsWidgetGroup = START_WIDGET_GROUP("KGM: MP Stored Communications")
        ADD_WIDGET_BOOL("Switch On Stored Communication Widgets?", g_WIDGET_StoredComms_Switch_Widgets_On)
    STOP_WIDGET_GROUP()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Destroy the  MP Stored Communication widgets
PROC Destroy_MP_Stored_Communication_Widgets()

    Destroy_MP_Stored_Communication_Optional_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Destroy_MP_Stored_Communication_Widgets") NET_NL()

    IF NOT (DOES_WIDGET_GROUP_EXIST(g_mainStoredCommsWidgetGroup))
        NET_PRINT("           MAIN WIDGET GROUP DOESN'T EXIST") NET_NL()
    
        EXIT
    ENDIF
    
    // ...delete the main group
    DELETE_WIDGET_GROUP(g_mainStoredCommsWidgetGroup)
    g_mainStoredCommsWidgetGroup = NULL
    
    NET_PRINT("           MAIN WIDGET GROUP DELETED") NET_NL()
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintains any MP Stored Communication widgets that need regular updating or checking
//
// INPUT PARAMS:    paramTypeAsInt              The array type for this stored communication
PROC Maintain_MP_Stored_Communication_Widgets(INT paramTypeAsInt)

    g_WIDGET_StoredComms_Received[paramTypeAsInt]   = (IS_BIT_SET(g_sCommsMP.storedComms[paramTypeAsInt].scBitflags, MP_STORED_COMMS_RECEIVED))
    g_WIDGET_StoredComms_Active[paramTypeAsInt]     = (IS_BIT_SET(g_sCommsMP.storedComms[paramTypeAsInt].scBitflags, MP_STORED_COMMS_ACTIVE))
    
    g_WIDGET_StoredComms_Delay_Expired[paramTypeAsInt] = FALSE
    IF (g_WIDGET_StoredComms_Received[paramTypeAsInt])
        IF IS_TIME_MORE_THAN(GET_NETWORK_TIME() , g_sCommsMP.storedComms[paramTypeAsInt].scInitialTimeout)
            g_WIDGET_StoredComms_Delay_Expired[paramTypeAsInt] = TRUE
        ENDIF
    ENDIF
    
    // Check if the optional widgets should be switched on or off
    IF (g_WIDGET_StoredComms_Switch_Widgets_On = g_holdStoredCommsWidgetsState)
        EXIT
    ENDIF
    
    // Switch them on?
    IF (g_WIDGET_StoredComms_Switch_Widgets_On)
        Create_MP_Stored_Communication_Optional_Widgets()
        EXIT
    ENDIF
    
    // Switch them off
    Destroy_MP_Stored_Communication_Optional_Widgets()

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      MP Communications Reply (holding variables) Debug Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Updates the End of Mission Message text widgets
// NOTES:   Uses the data held in the MP Communication strings since the data is the same
PROC Update_MP_Communication_Reply_Text_Widgets()

    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_MP_Comms_Reply_ScriptName))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_MP_Comms_Reply_ScriptName,       g_WIDGET_MP_Comms_ScriptName)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_MP_Comms_Reply_ReplyID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_MP_Comms_Reply_ReplyID,          g_WIDGET_MP_Comms_ReplyID)
    ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Sets up the MP Communications Reply widgets
PROC Create_MP_Communications_Reply_Widgets()
        
    g_optionalMPCommsReplyWidgetGroup = START_WIDGET_GROUP("Reply Holding Variables")
        g_WIDGETID_MP_Comms_Reply_ScriptName        = ADD_TEXT_WIDGET("Requested By Script")
        g_WIDGETID_MP_Comms_Reply_ReplyID           = ADD_TEXT_WIDGET("Reply")
    STOP_WIDGET_GROUP()
    
    Update_MP_Communication_Reply_Text_Widgets()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Destroy the MP Communications Reply widgets
PROC Destroy_MP_Communications_Reply_Widgets()

    IF NOT (DOES_WIDGET_GROUP_EXIST(g_optionalMPCommsReplyWidgetGroup))
        EXIT
    ENDIF
    
    // Destroy all the widgets
    // ...delete the text widgets
    DELETE_TEXT_WIDGET(g_WIDGETID_MP_Comms_Reply_ScriptName)
    DELETE_TEXT_WIDGET(g_WIDGETID_MP_Comms_Reply_ReplyID)
    
    // ...delete the optional group
    DELETE_WIDGET_GROUP(g_optionalMPCommsReplyWidgetGroup)
    g_optionalMPCommsReplyWidgetGroup = NULL

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      MP Communications Requests Debug Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Updates the MP Communication text widgets
PROC Update_MP_Communications_Text_Widgets()

    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_MP_Comms_ScriptName))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_MP_Comms_ScriptName,         g_WIDGET_MP_Comms_ScriptName)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_MP_Comms_CharSheetID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_MP_Comms_CharSheetID,        g_WIDGET_MP_Comms_CharSheetID)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_MP_Comms_PlayerID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_MP_Comms_PlayerID,           g_WIDGET_MP_Comms_PlayerID)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_MP_Comms_GroupID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_MP_Comms_GroupID,            g_WIDGET_MP_Comms_GroupID)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_MP_Comms_ConvRootID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_MP_Comms_ConvRootID,         g_WIDGET_MP_Comms_ConvRootID)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_MP_Comms_CommsTypeID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_MP_Comms_CommsTypeID,        g_WIDGET_MP_Comms_CommsTypeID)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_MP_Comms_DeviceID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_MP_Comms_DeviceID,           g_WIDGET_MP_Comms_DeviceID)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_MP_Comms_ReplyID))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_MP_Comms_ReplyID,            g_WIDGET_MP_Comms_ReplyID)
    ENDIF
    
    IF (DOES_TEXT_WIDGET_EXIST(g_WIDGETID_MP_Comms_Component_TL))
        SET_CONTENTS_OF_TEXT_WIDGET(g_WIDGETID_MP_Comms_Component_TL,       GET_STRING_FROM_TEXT_FILE(g_WIDGET_MP_Comms_Component_TL))
    ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Sets up the MP Communications optional widgets
PROC Create_MP_Communications_Optional_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Create_MP_Communications_Optional_Widgets") NET_NL()

    IF (DOES_WIDGET_GROUP_EXIST(g_optionalMPCommsWidgetGroup))
        NET_PRINT("           OPTIONAL WIDGET GROUP ALREADY EXISTS") NET_NL()
    
        EXIT
    ENDIF
    
    // Create the Widgets, place them inside the correct widget group
    IF (DOES_WIDGET_GROUP_EXIST(g_mainMPCommsWidgetGroup))
        SET_CURRENT_WIDGET_GROUP(g_mainMPCommsWidgetGroup)
    ENDIF

    g_optionalMPCommsWidgetGroup = START_WIDGET_GROUP("Optional Widgets")
        ADD_WIDGET_BOOL("Request Active? (eg: now being played)", g_WIDGET_MP_Comms_Active)
        
        START_WIDGET_GROUP("Highest Priority Request")
            g_WIDGETID_MP_Comms_ScriptName      = ADD_TEXT_WIDGET("Requested By Script")
            g_WIDGETID_MP_Comms_CharSheetID     = ADD_TEXT_WIDGET("CharSheet ID")
            g_WIDGETID_MP_Comms_PlayerID        = ADD_TEXT_WIDGET("Player ID")
            g_WIDGETID_MP_Comms_GroupID         = ADD_TEXT_WIDGET("Subtitle Group ID")
            g_WIDGETID_MP_Comms_ConvRootID      = ADD_TEXT_WIDGET("Conv. Root ID")
            g_WIDGETID_MP_Comms_CommsTypeID     = ADD_TEXT_WIDGET("Comms Type ID")
            g_WIDGETID_MP_Comms_DeviceID        = ADD_TEXT_WIDGET("Device")
            g_WIDGETID_MP_Comms_ReplyID         = ADD_TEXT_WIDGET("Reply")
            g_WIDGETID_MP_Comms_Component_TL    = ADD_TEXT_WIDGET("Component: TextLabel")
            
            ADD_WIDGET_INT_READ_ONLY("Component: INT", g_WIDGET_MP_Comms_Component_INT)
                    
            //ADD_WIDGET_INT_READ_ONLY("Reply Timeout (-1 if no timeout)", g_sCommsMP.priorityComms.ccTimeout)
        STOP_WIDGET_GROUP()
        
        // Store the Reply holding data widgets
        Create_MP_Communications_Reply_Widgets()
    STOP_WIDGET_GROUP()
    
    // Unset the widget group again
    CLEAR_CURRENT_WIDGET_GROUP(g_mainMPCommsWidgetGroup)
    
    // Update the text widgets with the latest data
    Update_MP_Communications_Text_Widgets()
    
    // Update the state variable
    g_holdMPCommsWidgetsState = TRUE
    
    NET_PRINT("           OPTIONAL WIDGET GROUP CREATED") NET_NL()
        
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Destroy the MP Communications optional widgets
PROC Destroy_MP_Communications_Optional_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Destroy_MP_Communications_Optional_Widgets") NET_NL()

    IF NOT (DOES_WIDGET_GROUP_EXIST(g_optionalMPCommsWidgetGroup))
        NET_PRINT("           OPTIONAL WIDGET GROUP DOESN'T EXIST") NET_NL()
    
        EXIT
    ENDIF

    // Destroy all the widgets
    // ...destroy the reply widgets
    Destroy_MP_Communications_Reply_Widgets()
    
    // ...delete the text widgets
    DELETE_TEXT_WIDGET(g_WIDGETID_MP_Comms_ScriptName)
    DELETE_TEXT_WIDGET(g_WIDGETID_MP_Comms_CharSheetID)
    DELETE_TEXT_WIDGET(g_WIDGETID_MP_Comms_PlayerID)
    DELETE_TEXT_WIDGET(g_WIDGETID_MP_Comms_GroupID)
    DELETE_TEXT_WIDGET(g_WIDGETID_MP_Comms_ConvRootID)
    DELETE_TEXT_WIDGET(g_WIDGETID_MP_Comms_CommsTypeID)
    DELETE_TEXT_WIDGET(g_WIDGETID_MP_Comms_DeviceID)
    DELETE_TEXT_WIDGET(g_WIDGETID_MP_Comms_ReplyID)
    DELETE_TEXT_WIDGET(g_WIDGETID_MP_Comms_Component_TL)
    
    // ...delete the optional group
    DELETE_WIDGET_GROUP(g_optionalMPCommsWidgetGroup)
    g_optionalMPCommsWidgetGroup = NULL
    
    // Update the state variable
    g_holdMPCommsWidgetsState = FALSE
    
    NET_PRINT("           OPTIONAL WIDGET GROUP DELETED") NET_NL()
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Sets up the MP Communication widgets
PROC Create_MP_Communications_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Create_MP_Communications_Widgets") NET_NL()
    
    // Clear out the text widget datafields
    // ...store the text widgets that display text that is otherwise non-persistent
    g_WIDGET_MP_Comms_ScriptName            = ""
    g_WIDGET_MP_Comms_CharSheetID           = ""
    g_WIDGET_MP_Comms_PlayerID              = ""
    g_WIDGET_MP_Comms_GroupID               = ""
    g_WIDGET_MP_Comms_ConvRootID            = ""
    g_WIDGET_MP_Comms_CommsTypeID           = ""
    g_WIDGET_MP_Comms_DeviceID              = ""
    g_WIDGET_MP_Comms_ReplyID               = ""
    g_WIDGET_MP_Comms_Component_TL          = ""
    
    // ...initial value of other non-string widgets
    g_WIDGET_MP_Comms_Component_INT         = 0

    g_mainMPCommsWidgetGroup = START_WIDGET_GROUP("KGM: MP Comms")
        ADD_WIDGET_BOOL("Switch On MP Comms Widgets?", g_WIDGET_MP_Comms_Switch_Widgets_On)
    STOP_WIDGET_GROUP()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Destroy the  MP Communications widgets
PROC Destroy_MP_Communications_Widgets()

    Destroy_MP_Communications_Optional_Widgets()
    
    NET_PRINT("...KGM MP [MPComms]: Destroy_MP_Communications_Widgets") NET_NL()

    IF NOT (DOES_WIDGET_GROUP_EXIST(g_mainMPCommsWidgetGroup))
        NET_PRINT("           MAIN WIDGET GROUP DOESN'T EXIST") NET_NL()
    
        EXIT
    ENDIF
    
    // ...delete the main group
    DELETE_WIDGET_GROUP(g_mainMPCommsWidgetGroup)
    g_mainMPCommsWidgetGroup = NULL
    
    NET_PRINT("           MAIN WIDGET GROUP DELETED") NET_NL()
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintains any MP Communications widgets that need regular updating or checking
PROC Maintain_MP_Communications_Widgets()

    IF (g_sCommsMP.priorityComms.ccState = NO_CURRENT_MP_COMMS_REQUEST)
        g_WIDGET_MP_Comms_Active = FALSE
    ELSE
        g_WIDGET_MP_Comms_Active = TRUE
    ENDIF
    
    // Check if the optional widgets should be switched on or off
    IF (g_WIDGET_MP_Comms_Switch_Widgets_On = g_holdMPCommsWidgetsState)
        EXIT
    ENDIF
    
    // Switch them on?
    IF (g_WIDGET_MP_Comms_Switch_Widgets_On)
        Create_MP_Communications_Optional_Widgets()
        EXIT
    ENDIF
    
    // Switch them off
    Destroy_MP_Communications_Optional_Widgets()

ENDPROC






#ENDIF  // IS_DEBUG_BUILD

