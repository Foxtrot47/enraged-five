




#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "Net_prints.sch"

BOOL BDisplayHUDCheckpoint
	BOOL BDisplayHUDScreenTimer
	BOOL bDisplayHUDEliminationScreenTimer
	BOOL BDisplayHUDScreenTimerAtPosition
	BOOL BDisplayHUDCheckpointAtPosition
	BOOL BDisplayHUDEliminationAtPosition
	BOOL bDisplayHUDShootingRange
	BOOL bDisplayObjectiveDistance
	BOOL bDisplayArrestTimer
	BOOL bDisplayDeathmatchHud
	BOOL bDisplayCollectionHud
	BOOL bDisplayTwoPackagesHud
	BOOL bListenForCheats 
	BOOL bDisplayRampageHud
	BOOL bDisplayHUDNewRaceHud
	BOOL bDisplayFormationHud
	BOOL bDisplayHUDNewScoreHud
	BOOL bDisplayHudElimination
	BOOL bDisplayHudSmallsingleScore
	BOOL bDisplayHudBigSingleScore
	BOOL bDisplayHudBigDoubleScore
	BOOL bDisplayHudMovingCheckpoint
	BOOL bDisplayHUDOverheadRefresh
	
	BOOL bDisplayTodoBox

	BOOL BTurnOnDecals
	BOOL bDisplayClock
//	INT TopLeftNumberOfStats = 0
	BOOL TopLeftSwitchedOnDebug
	BOOL TopLeftSwitchedOnDebugMissionEnd
	INT HUDCOLOUR_COMBO[MAX_NUM_INFOBOX_ELEMENTS]
	INT MEDAL_COLOUR
	INT FlagTotal[MAX_NUM_INFOBOX_ELEMENTS]
//	INT TIMER_SETTING[MAX_NUM_INFOBOX_ELEMENTS]
	BOOL TIMERFORMATSETTINGS[11][MAX_NUM_INFOBOX_ELEMENTS]
	INT HudWidgetValue[5]
	FLOAT HudWidgetValueFloat[5]
//	VECTOR MissionTriggerDirection
	FLOAT MissionTriggerWidth = 3.5
	FLOAT MissionStartDoubleNumberWidth 
//	FLOAT MissionTriggerHeight
	VECTOR PlacementVector[5]
	VECTOR HeadingVector[5]
	VECTOR LightPos[5]
	FLOAT LightRange[5]
	FLOAT LightIntensity[5]
	BOOL IsHudCuffed
	BOOL isHudHasKeys
	BOOL isBeingUncuffed
	BOOL bGivePlayerRole
	BOOL bGivePlayerTick
	BOOL bDisplayDistanceDisplay
	INT iTimeArresting 
	INT MissionStartNumbers
	VECTOR MissionStartSINGLENumberPos
	VECTOR MissionStartDOUBLENumberPos1
	VECTOR MissionStartDOUBLENumberPos2
	FLOAT MissionStartSingleNumberWidth
	FLOAT MissionStartCoronaHeightScaler = 1
	FLOAT MissionStartZScaler = 1
	BOOL bTurnOnAllOverheads
	BOOL TurnOffMyOverheads 
	BOOL bDisplayXPBarStatic
	BOOL bDisplayRankupTitle
	INT iDisplayRankupRankTo
	INT iDisplayRankupWhichTeam
	INT iRewardComboBox
	BOOL bDisplayAward_widget
	BOOL bDisplayGlobalXp
	BOOL DisplayedGlobalTickerMessage
	BOOL bDisplayMissionBrief

	BOOL Widget_Afterlife_BringupHud
	BOOL Widget_Afterlife_BringupHud_AfterTime
	BOOL Widget_Afterlife_JustRespawn
	BOOL Widget_Afterlife_JustRespawn_AfterTime
	BOOL Widget_Afterlife_JustRespawn_AtRace 
	BOOL Widget_Afterlife_SetSpectatorCam
	BOOL Widget_Afterlife_SetSpectatorCam_AfterTime
	
	


PROC CREATE_TEXTURE_UI_WIDGET(BOOL& TOPLEFTFLAGS_COMBO[21][MAX_NUM_INFOBOX_ELEMENTS], INFOBOXDETAILSSTRUCT& aTopLeftScreenStruct)
	INT I
	TEXT_LABEL_63 WidgetTitle 
	
	START_WIDGET_GROUP("Afterlife Setting")
		ADD_WIDGET_BOOL("Afterlife_BringupHud", Widget_Afterlife_BringupHud)
		ADD_WIDGET_BOOL("Afterlife_BringupHud_AfterTime", Widget_Afterlife_BringupHud_AfterTime)
		ADD_WIDGET_BOOL("Afterlife_JustRespawn", Widget_Afterlife_JustRespawn)
		ADD_WIDGET_BOOL("Afterlife_JustRespawn_AfterTime", Widget_Afterlife_JustRespawn_AfterTime)
		ADD_WIDGET_BOOL("Afterlife_JustRespawn_AtRace", Widget_Afterlife_JustRespawn_AtRace)
		ADD_WIDGET_BOOL("Afterlife_SetSpectatorCam", Widget_Afterlife_SetSpectatorCam)
		ADD_WIDGET_BOOL("Afterlife_SetSpectatorCam_AfterTime", Widget_Afterlife_SetSpectatorCam_AfterTime)
	STOP_WIDGET_GROUP()
	
	START_WIDGET_GROUP("UI Displays")
		START_WIDGET_GROUP("UI Right Bottom Corner")
		//UI Debug bools
			ADD_WIDGET_BOOL("Display Checkpoint", BDisplayHUDCheckpoint)
			ADD_WIDGET_BOOL("Display Screen Timer", BDisplayHUDScreenTimer)
			ADD_WIDGET_BOOL("Display Elimination Timer", bDisplayHUDEliminationScreenTimer)
			ADD_WIDGET_BOOL("Display Timer at position ",BDisplayHUDScreenTimerAtPosition)
			ADD_WIDGET_BOOL("Display Checkbox at position ",BDisplayHUDCheckpointAtPosition)
			ADD_WIDGET_BOOL("Display Elimination at position ",BDisplayHUDEliminationAtPosition)
			ADD_WIDGET_BOOL("Display Shooting Range hud", bDisplayHUDShootingRange)
			ADD_WIDGET_BOOL("Display Arrest Timer Hud", bDisplayArrestTimer)
			ADD_WIDGET_BOOL("Display deathmatch Hud", bDisplayDeathmatchHud)
			ADD_WIDGET_BOOL("Display Collection Hud", bDisplayCollectionHud)
			ADD_WIDGET_BOOL("Display Formation Hud", bDisplayFormationHud)
			ADD_WIDGET_BOOL("Display Rampage Hud", bDisplayRampageHud)
			ADD_WIDGET_BOOL("Display New Race Hud", bDisplayHUDNewRaceHud)
			ADD_WIDGET_BOOL("Display New Score Hud", bDisplayHUDNewScoreHud)
			ADD_WIDGET_BOOL("Display New Elimination Hud", bDisplayHudElimination)
			ADD_WIDGET_BOOL("Display Small Single Score", bDisplayHudSmallsingleScore)
			ADD_WIDGET_BOOL("Display Big Single Score", bDisplayHudBigSingleScore)
			ADD_WIDGET_BOOL("Display Big Double Score", bDisplayHudBigDoubleScore)
	//		ADD_WIDGET_BOOL("Display Gang in mission box", bDisplaygangmembersonmissionbox)		
			ADD_WIDGET_BOOL("Display Am Clock", bDisplayClock)
			ADD_WIDGET_BOOL("Display Two Packages Hud", bDisplayTwoPackagesHud)
			ADD_WIDGET_BOOL("Display Distance Display", bDisplayDistanceDisplay)
			ADD_WIDGET_BOOL("Display Moving Checkpoint", bDisplayHudMovingCheckpoint)
			ADD_WIDGET_FLOAT_READ_ONLY("HudStartY", MPGlobalsScoreHud.HudStartY)
			ADD_WIDGET_FLOAT_READ_ONLY("BottomStartY", MPGlobalsScoreHud.BottomStartY)
			ADD_WIDGET_FLOAT_SLIDER("Hud Widget FLOAT 0", HudWidgetValueFloat[0], -20, 20, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Hud Widget FLOAT 1", HudWidgetValueFloat[1], -20, 20, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Hud Widget FLOAT 2", HudWidgetValueFloat[2], -20, 20, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Hud Widget FLOAT 3", HudWidgetValueFloat[3], -20, 20, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Hud Widget FLOAT 4", HudWidgetValueFloat[4], -20, 20, 0.001)
			ADD_WIDGET_INT_SLIDER("Hud Widget INT 0", HudWidgetValue[0], -1, 100, 1)
			ADD_WIDGET_INT_SLIDER("Hud Widget INT 1", HudWidgetValue[1], -1, 100, 1)
			ADD_WIDGET_INT_SLIDER("Hud Widget INT 2", HudWidgetValue[2], -1, 100, 1)
			ADD_WIDGET_INT_SLIDER("Hud Widget INT 3", HudWidgetValue[3], -1, 100000, 1)
			ADD_WIDGET_INT_SLIDER("Hud Widget INT 4", HudWidgetValue[4], -1, 100000, 1)
			
			ADD_WIDGET_INT_SLIDER("FlashingTimer1", G_FlashingTimer1, -1, 100000, 1)
			ADD_WIDGET_INT_SLIDER("FlashingTimer2", G_FlashingTimer2, -1, 100000, 1)
			
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("UI Top Left Corner")
			START_WIDGET_GROUP("Mission Brief")
				ADD_WIDGET_BOOL("bDisplayMissionBrief", bDisplayMissionBrief)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Top Left New")
				ADD_WIDGET_BOOL("Turn On New Mission Info DEBUG", TopLeftSwitchedOnDebug)
				ADD_WIDGET_BOOL("Turn On New Mission Info", TopLeftSwitchedOnDebugMissionEnd) 
//				ADD_WIDGET_BOOL("Turn On New Mission Info MAIN", aTopLeftScreenStruct.isTopLeftActive)
				ADD_WIDGET_INT_SLIDER("Number of Stats", aTopLeftScreenStruct.NumberOfStats, 0, 16, 1)
				ADD_WIDGET_FLOAT_SLIDER("MissionBox Height", MPGlobalsHud.fMissionIntroHeight,0.0, 1.0, 0.001  )
				ADD_WIDGET_FLOAT_SLIDER("MissionBox YStart", MPGlobalsHud.fMissionIntroBoxYPos,0.0, 1.0, 0.001  )
				ADD_WIDGET_STRING(aTopLeftScreenStruct.MainTitle)
				ADD_WIDGET_STRING(aTopLeftScreenStruct.SubTitle)
				FOR I = 0 TO MAX_NUM_INFOBOX_ELEMENTS-1
					WidgetTitle = ""
					WidgetTitle += "INFO STAT "
					WidgetTitle += I
					START_WIDGET_GROUP(WidgetTitle)
						ADD_WIDGET_STRING(aTopLeftScreenStruct.InformationStat[I].StatTitle)
						ADD_WIDGET_INT_SLIDER("Int Value", aTopLeftScreenStruct.InformationStat[I].IntStatValue, -10000, 10000, 1)
						ADD_WIDGET_FLOAT_SLIDER("Float Value", aTopLeftScreenStruct.InformationStat[I].FloatStatValue, -10000.0, 10000.0, 0.01)
						ADD_WIDGET_STRING(aTopLeftScreenStruct.InformationStat[I].StringStatValue)
						ADD_WIDGET_BOOL("Bool Value", aTopLeftScreenStruct.InformationStat[I].BoolStatValue)
						ADD_WIDGET_INT_SLIDER("Int Current Value", aTopLeftScreenStruct.InformationStat[I].IntCurrentValue, 0, 10000, 1)
						ADD_WIDGET_INT_SLIDER("Int Max Value", aTopLeftScreenStruct.InformationStat[I].IntMaxValue, 0, 10000, 1)
						ADD_WIDGET_FLOAT_SLIDER("fOverallFloatStat", aTopLeftScreenStruct.fOverallFloatStat, 0, 100, 1)
						ADD_WIDGET_INT_SLIDER("iOverallIntStat", aTopLeftScreenStruct.iOverallIntStat, 0, 5000, 1)
						ADD_WIDGET_FLOAT_SLIDER("FarEdgeWrap", aTopLeftScreenStruct.FarEdgeWrap, 0.0, 1.0, 0.001)
						ADD_WIDGET_FLOAT_SLIDER("SecondaryWrap", aTopLeftScreenStruct.SecondaryWrap, 0.0, 1.0, 0.001)
						ADD_WIDGET_INT_READ_ONLY("Flags Variable", aTopLeftScreenStruct.InformationStat[I].Flags)
						START_NEW_WIDGET_COMBO()
							ADD_TO_WIDGET_COMBO("NONE")
							ADD_TO_WIDGET_COMBO("BLUE")
							ADD_TO_WIDGET_COMBO("RED")
							ADD_TO_WIDGET_COMBO("GREEN")
							ADD_TO_WIDGET_COMBO("YELLOW")
						STOP_WIDGET_COMBO("HUD_COLOURS", HUDCOLOUR_COMBO[I])
						START_NEW_WIDGET_COMBO()
							ADD_TO_WIDGET_COMBO("NONE")
							ADD_TO_WIDGET_COMBO("GOLD")
							ADD_TO_WIDGET_COMBO("SILVER")
							ADD_TO_WIDGET_COMBO("BRONZE")
						STOP_WIDGET_COMBO("MEDAL COLOUR", MEDAL_COLOUR) 
						ADD_WIDGET_BOOL("Time Format Milliseconds", TIMERFORMATSETTINGS[0][I])
						ADD_WIDGET_BOOL("Time Format Seconds", TIMERFORMATSETTINGS[1][I])
						ADD_WIDGET_BOOL("Time Format Minutes", TIMERFORMATSETTINGS[2][I])
						ADD_WIDGET_BOOL("Time Format Hours", TIMERFORMATSETTINGS[3][I])
						ADD_WIDGET_BOOL("Time Format Days", TIMERFORMATSETTINGS[4][I])
						ADD_WIDGET_BOOL("show 12:20 not 00:12:20", TIMERFORMATSETTINGS[5][I])
						ADD_WIDGET_BOOL("show 1:14 not 01:14", TIMERFORMATSETTINGS[6][I])
						ADD_WIDGET_BOOL("show 3m24s not 3:24", TIMERFORMATSETTINGS[7][I])
						ADD_WIDGET_BOOL("show 3m24 not 3m24s", TIMERFORMATSETTINGS[8][I])
						ADD_WIDGET_BOOL("show 05:51 not 05:519", TIMERFORMATSETTINGS[9][I])
						ADD_WIDGET_BOOL("show 05:5 not 05:519", TIMERFORMATSETTINGS[10][I])
						ADD_WIDGET_INT_READ_ONLY("Overall Timer Format Flag", FlagTotal[I])
//						START_NEW_WIDGET_COMBO()
//							ADD_TO_WIDGET_COMBO("NONE")
//							ADD_TO_WIDGET_COMBO("TIME_FORMAT_MILLISECONDS")
//							ADD_TO_WIDGET_COMBO("TIME_FORMAT_SECONDS")
//							ADD_TO_WIDGET_COMBO("TIME_FORMAT_MINUTES")
//							ADD_TO_WIDGET_COMBO("TIME_FORMAT_HOURS")
//							ADD_TO_WIDGET_COMBO("TIME_FORMAT_DAYS")
//						STOP_WIDGET_COMBO("TIMER_SETTING", TIMER_SETTING[I])

						ADD_WIDGET_BOOL("MD_FLAGS_IS_CASH", TOPLEFTFLAGS_COMBO[0][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_SIGNCOLOURED", TOPLEFTFLAGS_COMBO[1][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_ISCHECKBOX", TOPLEFTFLAGS_COMBO[2][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_ISMETER", TOPLEFTFLAGS_COMBO[3][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_ISELIMINATION", TOPLEFTFLAGS_COMBO[4][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_ISDOUBLEDASH", TOPLEFTFLAGS_COMBO[5][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_ISINT", TOPLEFTFLAGS_COMBO[6][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_ISFLOAT", TOPLEFTFLAGS_COMBO[7][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_ISBOOL", TOPLEFTFLAGS_COMBO[8][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_ISSTRING", TOPLEFTFLAGS_COMBO[9][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_ISPERCENT_INT", TOPLEFTFLAGS_COMBO[10][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_ISPERCENT_FLOAT", TOPLEFTFLAGS_COMBO[19][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_IS_INT_TIMER", TOPLEFTFLAGS_COMBO[13][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_IS_METER_DISTANCE_FLOAT", TOPLEFTFLAGS_COMBO[15][I] )
						ADD_WIDGET_BOOL("MD_FLAGS_IS_METER_DISTANCE_INT", TOPLEFTFLAGS_COMBO[16][I] )
						
						ADD_WIDGET_BOOL("MD_FLAGS_ISVALUE_SECONDARY", TOPLEFTFLAGS_COMBO[20][I] )
						
					STOP_WIDGET_GROUP()
				ENDFOR
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("UI Bottom Left Corner")
			ADD_WIDGET_BOOL("Objective Distance", bDisplayObjectiveDistance)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("UI Top Centre")
			ADD_WIDGET_BOOL("bDisplayXPBarStatic", bDisplayXPBarStatic)
			ADD_WIDGET_BOOL("bDisplayRankupTitle", bDisplayRankupTitle)
			ADD_WIDGET_INT_SLIDER("iDisplayRankupRankTo", iDisplayRankupRankTo, 0, 100, 1)
			ADD_WIDGET_INT_SLIDER("iDisplayRankupWhichTeam",iDisplayRankupWhichTeam, 0, 3, 1 )
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("UI Bottom Centre")
			ADD_WIDGET_BOOL("bDisplayAward_widget", bDisplayAward_widget)
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("NONE")
				ADD_TO_WIDGET_COMBO("AWARD")
				ADD_TO_WIDGET_COMBO("MEDAL")
				ADD_TO_WIDGET_COMBO("TATTOO")
				ADD_TO_WIDGET_COMBO("WEAPON")
				ADD_TO_WIDGET_COMBO("HEIST")
			STOP_WIDGET_COMBO("REWARD TYPE", iRewardComboBox) 
			ADD_WIDGET_BOOL("bDisplayGlobalXp", bDisplayGlobalXp)
			ADD_WIDGET_BOOL("DisplayedGlobalTickerMessage", DisplayedGlobalTickerMessage)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("GENERAL UI")
			START_WIDGET_GROUP("MP HUD")
				ADD_WIDGET_VECTOR_SLIDER("PlacementVector[0]", PlacementVector[0], -4000, 4000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("HeadingVector[0]", HeadingVector[0], -4000, 4000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("LightVector[0]", LightPos[0], -4000, 4000, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Range Light[0]", LightRange[0], -10, 10, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Intensity Light[0]", LightIntensity[0], -10, 10, 0.001)
				
				ADD_WIDGET_VECTOR_SLIDER("PlacementVector[1]", PlacementVector[1], -4000, 4000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("HeadingVector[1]", HeadingVector[1], -4000, 4000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("LightVector[1]", LightPos[1], -4000, 4000, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Range Light[1]", LightRange[1], -10, 10, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Intensity Light[1]", LightIntensity[1], -10, 10, 0.001)
				
				ADD_WIDGET_VECTOR_SLIDER("PlacementVector[2]", PlacementVector[2], -4000, 4000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("HeadingVector[2]", HeadingVector[2], -4000, 4000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("LightVector[2]", LightPos[2], -4000, 4000, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Range Light[2]", LightRange[2], -10, 10, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Intensity Light[2]", LightIntensity[2], -10, 10, 0.001)
				
				ADD_WIDGET_VECTOR_SLIDER("PlacementVector[3]", PlacementVector[3], -4000, 4000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("HeadingVector[3]", HeadingVector[3], -4000, 4000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("LightVector[3]", LightPos[3], -4000, 4000, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Range Light[3]", LightRange[3], -10, 10, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Intensity Light[3]", LightIntensity[3], -10, 10, 0.001)
				
				ADD_WIDGET_VECTOR_SLIDER("PlacementVector[4]", PlacementVector[4], -4000, 4000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("HeadingVector[4]", HeadingVector[4], -4000, 4000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("LightVector[4]", LightPos[4], -4000, 4000, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Range Light[4]", LightRange[4], -10, 10, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Intensity Light[4]", LightIntensity[4], -10, 10, 0.001)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("In Game Random Stuff")
				ADD_WIDGET_BOOL("bDisplayHUDOverheadRefresh", bDisplayHUDOverheadRefresh)
				ADD_WIDGET_BOOL("bListenForCheats", bListenForCheats)
				ADD_WIDGET_BOOL("Turn On Mission entry corona", BTurnOnDecals)
				ADD_WIDGET_FLOAT_SLIDER("Width", MissionTriggerWidth, -10, 10, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Mission Single Number Width", MissionStartSingleNumberWidth, -20.0, 20.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Mission Double Number Width", MissionStartDoubleNumberWidth, -20.0, 20.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Mission Start Z Scaler", MissionStartZScaler, -10, 10, 0.01)
				ADD_WIDGET_INT_SLIDER("mission start numbers", MissionStartNumbers, 0, 16, 1)
				ADD_WIDGET_FLOAT_SLIDER("Mission corona height scaler", MissionStartCoronaHeightScaler, -10, 10, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Mission Start SINGLE Number Pos", MissionStartSINGLENumberPos, -30, 30, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("Mission Start DOUBLE Number Pos 1", MissionStartDOUBLENumberPos1, -30, 30, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("Mission Start DOUBLE Number Pos 2", MissionStartDOUBLENumberPos2, -30, 30, 0.001)
				
				ADD_WIDGET_BOOL("Turn on All overheads", bTurnOnAllOverheads)
				ADD_WIDGET_BOOL("Is Cuffed", IsHudCuffed)
				ADD_WIDGET_BOOL("Has Keys", isHudHasKeys)
				ADD_WIDGET_BOOL("Is Being Uncuffed", isBeingUncuffed)
				ADD_WIDGET_BOOL("Give a Player role", bGivePlayerRole)
				ADD_WIDGET_BOOL("Give a Player Tick", bGivePlayerTick)
				ADD_WIDGET_INT_SLIDER("Time Arresting", iTimeArresting, 0, 100, 1)
				ADD_WIDGET_BOOL("Turn Off all my overheads", TurnOffMyOverheads)
				ADD_WIDGET_BOOL("bDisplayTodoBox", bDisplayTodoBox)
			STOP_WIDGET_GROUP()
			
			

		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC


PROC RUN_TOPLEFT_WIDGET_UPDATE(BOOL& TOPLEFTFLAGS_COMBO[21][MAX_NUM_INFOBOX_ELEMENTS], INFOBOXDETAILSSTRUCT& aTopLeftScreenStruct)
INT I
INT ClearValue 
//TEXT_COMPONENT_TIME_FORMAT ClearTimeValue

INT TImerFormatValue 
INT TimerValue 

	
	IF TopLeftSwitchedOnDebug

	
		SWITCH MEDAL_COLOUR
			CASE 0 
				aTopLeftScreenStruct.MedalPosition = PODIUMPOS_NONE
			BREAK
			CASE 1
				aTopLeftScreenStruct.MedalPosition = PODIUMPOS_GOLD
			BREAK
			CASE 2
				aTopLeftScreenStruct.MedalPosition = PODIUMPOS_SILVER
			BREAK
			CASE 3
				aTopLeftScreenStruct.MedalPosition = PODIUMPOS_BRONZE
			BREAK
		ENDSWITCH

		INT MAX_TEXT_FORMAT_BIT = 2048-1

		FOR I = 0 TO MAX_NUM_INFOBOX_ELEMENTS-1

			TImerFormatValue = ENUM_TO_INT(aTopLeftScreenStruct.InformationStat[I].TimerFormat)

			IF TIMERFORMATSETTINGS[0][I] 
				TimerValue = ENUM_TO_INT(TIME_FORMAT_MILLISECONDS)
				TImerFormatValue = TImerFormatValue|TimerValue
			ELSE
				TimerValue = ENUM_TO_INT(TIME_FORMAT_MILLISECONDS)
				ClearValue = MAX_TEXT_FORMAT_BIT-TimerValue
				TImerFormatValue = TImerFormatValue&ClearValue
			ENDIF
			IF TIMERFORMATSETTINGS[1][I] 
				TimerValue = ENUM_TO_INT(TIME_FORMAT_SECONDS)
				TImerFormatValue = TImerFormatValue|TimerValue
			ELSE
				TimerValue = ENUM_TO_INT(TIME_FORMAT_SECONDS)
				ClearValue = MAX_TEXT_FORMAT_BIT-TimerValue
				TImerFormatValue = TImerFormatValue&ClearValue
			ENDIF
			IF TIMERFORMATSETTINGS[2][I] 
				TimerValue = ENUM_TO_INT(TIME_FORMAT_MINUTES)
				TImerFormatValue = TImerFormatValue|TimerValue
			ELSE
				TimerValue = ENUM_TO_INT(TIME_FORMAT_MINUTES)
				ClearValue = MAX_TEXT_FORMAT_BIT-TimerValue
				TImerFormatValue = TImerFormatValue&ClearValue
			ENDIF
			IF TIMERFORMATSETTINGS[3][I] 
				TimerValue = ENUM_TO_INT(TIME_FORMAT_HOURS)
				TImerFormatValue = TImerFormatValue|TimerValue
			ELSE
				TimerValue = ENUM_TO_INT(TIME_FORMAT_HOURS)
				ClearValue = MAX_TEXT_FORMAT_BIT-TimerValue
				TImerFormatValue = TImerFormatValue&ClearValue
			ENDIF
			IF TIMERFORMATSETTINGS[4][I] 
				TimerValue = ENUM_TO_INT(TIME_FORMAT_DAYS)
				TImerFormatValue = TImerFormatValue|TimerValue
			ELSE
				TimerValue = ENUM_TO_INT(TIME_FORMAT_DAYS)
				ClearValue = MAX_TEXT_FORMAT_BIT-TimerValue
				TImerFormatValue = TImerFormatValue&ClearValue
			ENDIF
			IF TIMERFORMATSETTINGS[5][I] 
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_HIDE_LEADING_UNITS_EQUAL_TO_ZERO)
				TImerFormatValue = TImerFormatValue|TimerValue
			ELSE
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_HIDE_LEADING_UNITS_EQUAL_TO_ZERO)
				ClearValue = MAX_TEXT_FORMAT_BIT-TimerValue
				TImerFormatValue = TImerFormatValue&ClearValue
			ENDIF
			IF TIMERFORMATSETTINGS[6][I] 
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS)
				TImerFormatValue = TImerFormatValue|TimerValue
			ELSE
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_HIDE_LEADING_ZEROS_ON_LEADING_UNITS)
				ClearValue = MAX_TEXT_FORMAT_BIT-TimerValue
				TImerFormatValue = TImerFormatValue&ClearValue
			ENDIF
			IF TIMERFORMATSETTINGS[7][I] 
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_SHOW_UNIT_DIVIDERS_AS_LETTERS)
				TImerFormatValue = TImerFormatValue|TimerValue
			ELSE
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_SHOW_UNIT_DIVIDERS_AS_LETTERS)
				ClearValue = MAX_TEXT_FORMAT_BIT-TimerValue
				TImerFormatValue = TImerFormatValue&ClearValue
			ENDIF
			IF TIMERFORMATSETTINGS[8][I] 
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_HIDE_UNIT_LETTER_FOR_SMALLEST_UNITS)
				TImerFormatValue = TImerFormatValue|TimerValue
			ELSE
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_HIDE_UNIT_LETTER_FOR_SMALLEST_UNITS)
				ClearValue = MAX_TEXT_FORMAT_BIT-TimerValue
				TImerFormatValue = TImerFormatValue&ClearValue
			ENDIF
			IF TIMERFORMATSETTINGS[9][I] 
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_HIDE_MILLISECONDS_UNITS_DIGIT)
				TImerFormatValue = TImerFormatValue|TimerValue
			ELSE
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_HIDE_MILLISECONDS_UNITS_DIGIT)
				ClearValue = MAX_TEXT_FORMAT_BIT-TimerValue
				TImerFormatValue = TImerFormatValue&ClearValue
			ENDIF
			IF TIMERFORMATSETTINGS[10][I] 
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_HIDE_MILLISECONDS_TENS_DIGIT)
				TImerFormatValue = TImerFormatValue|TimerValue
			ELSE
				TimerValue = ENUM_TO_INT(TEXT_FORMAT_HIDE_MILLISECONDS_TENS_DIGIT)
				ClearValue = MAX_TEXT_FORMAT_BIT-TimerValue
				TImerFormatValue = TImerFormatValue&ClearValue
			ENDIF

			aTopLeftScreenStruct.InformationStat[I].TimerFormat =  INT_TO_ENUM(TEXT_COMPONENT_TIME_FORMAT, TImerFormatValue)
			
			FlagTotal[I] = TImerFormatValue
		
			SWITCH HUDCOLOUR_COMBO[I]
				CASE 0
//					aTopLeftScreenStruct.InformationStat[I].CheckpointColour = HUD_COLOUR_WHITE
				BREAK
				CASE 1
					aTopLeftScreenStruct.InformationStat[I].CheckpointColour = HUD_COLOUR_BLUE
				BREAK
				CASE 2
					aTopLeftScreenStruct.InformationStat[I].CheckpointColour = HUD_COLOUR_RED
				BREAK
				CASE 3
					aTopLeftScreenStruct.InformationStat[I].CheckpointColour = HUD_COLOUR_GREEN
				BREAK
				CASE 4
					aTopLeftScreenStruct.InformationStat[I].CheckpointColour = HUD_COLOUR_YELLOW
				BREAK
			ENDSWITCH
			
			
			
			IF TOPLEFTFLAGS_COMBO[0][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_IS_CASH)) = ENUM_TO_INT(MD_FLAGS_IS_CASH))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_IS_CASH))
				TOPLEFTFLAGS_COMBO[0][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_IS_CASH)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[0][I] = FALSE
			ENDIF
			
			IF TOPLEFTFLAGS_COMBO[1][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_SIGNCOLOURED)) = ENUM_TO_INT(MD_FLAGS_SIGNCOLOURED))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_SIGNCOLOURED))
				TOPLEFTFLAGS_COMBO[1][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_SIGNCOLOURED)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[1][I] = FALSE
			ENDIF
			
			IF TOPLEFTFLAGS_COMBO[2][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISCHECKBOX)) = ENUM_TO_INT(MD_FLAGS_ISCHECKBOX))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISCHECKBOX))
				TOPLEFTFLAGS_COMBO[2][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISCHECKBOX)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[2][I] = FALSE
			ENDIF	
			
			IF TOPLEFTFLAGS_COMBO[3][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISMETER)) = ENUM_TO_INT(MD_FLAGS_ISMETER))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISMETER))
				TOPLEFTFLAGS_COMBO[3][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISMETER)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[3][I] = FALSE
			ENDIF
			IF TOPLEFTFLAGS_COMBO[4][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISELIMINATION)) = ENUM_TO_INT(MD_FLAGS_ISELIMINATION))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISELIMINATION))
				TOPLEFTFLAGS_COMBO[4][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISELIMINATION)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[4][I] = FALSE
			ENDIF
			IF TOPLEFTFLAGS_COMBO[5][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISDOUBLEDASH)) = ENUM_TO_INT(MD_FLAGS_ISDOUBLEDASH))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISDOUBLEDASH))
				TOPLEFTFLAGS_COMBO[5][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISDOUBLEDASH)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[5][I] = FALSE
			ENDIF
			IF TOPLEFTFLAGS_COMBO[6][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISINT)) = ENUM_TO_INT(MD_FLAGS_ISINT))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISINT))
				TOPLEFTFLAGS_COMBO[6][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISINT)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[6][I] = FALSE
			ENDIF
			
	//		SET_MD_FLAGS_ISFLOAT(aTopLeftScreenStruct.InformationStat[I].Flags,TOPLEFTFLAGS_COMBO[7][I] )
			IF TOPLEFTFLAGS_COMBO[7][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISFLOAT)) = ENUM_TO_INT(MD_FLAGS_ISFLOAT))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISFLOAT))
				TOPLEFTFLAGS_COMBO[7][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISFLOAT)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[7][I] = FALSE
			ENDIF
			IF TOPLEFTFLAGS_COMBO[8][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISBOOL)) = ENUM_TO_INT(MD_FLAGS_ISBOOL))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISBOOL))
				TOPLEFTFLAGS_COMBO[8][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISBOOL)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[8][I] = FALSE
			ENDIF
			IF TOPLEFTFLAGS_COMBO[9][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISSTRING)) = ENUM_TO_INT(MD_FLAGS_ISSTRING))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISSTRING))
				TOPLEFTFLAGS_COMBO[9][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISSTRING)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[9][I] = FALSE
			ENDIF
			IF TOPLEFTFLAGS_COMBO[10][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISPERCENT_INT)) = ENUM_TO_INT(MD_FLAGS_ISPERCENT_INT))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISPERCENT_INT))
				TOPLEFTFLAGS_COMBO[10][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISPERCENT_INT)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)		
				TOPLEFTFLAGS_COMBO[10][I] = FALSE
			ENDIF
//			IF TOPLEFTFLAGS_COMBO[11][I] = TRUE
//			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISELIMINATION)) = ENUM_TO_INT(MD_FLAGS_ISELIMINATION))
//				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISELIMINATION))
//			ELSE
//				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISELIMINATION)
//				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)		
//			ENDIF
	//		IF TOPLEFTFLAGS_COMBO[12][I] = TRUE
	//			aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_SHOWMEDALSILVER))
	//		ELSE
	//			ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_SHOWMEDALSILVER)
	//			aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)	
	//		ENDIF
			IF TOPLEFTFLAGS_COMBO[13][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_IS_INT_TIMER)) = ENUM_TO_INT(MD_FLAGS_IS_INT_TIMER))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_IS_INT_TIMER))
				TOPLEFTFLAGS_COMBO[13][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_IS_INT_TIMER)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[13][I] = FALSE
			ENDIF
	//		IF TOPLEFTFLAGS_COMBO[14][I] = TRUE
	//			aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_IS_PLAYER_IN_REASON))
	//		ELSE
	//			ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_IS_PLAYER_IN_REASON)
	//			aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
	//		ENDIF
			IF TOPLEFTFLAGS_COMBO[15][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_IS_METER_DISTANCE_FLOAT)) = ENUM_TO_INT(MD_FLAGS_IS_METER_DISTANCE_FLOAT))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_IS_METER_DISTANCE_FLOAT))
				TOPLEFTFLAGS_COMBO[15][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_IS_METER_DISTANCE_FLOAT)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[15][I] = FALSE
			ENDIF
			IF TOPLEFTFLAGS_COMBO[16][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_IS_METER_DISTANCE_INT)) = ENUM_TO_INT(MD_FLAGS_IS_METER_DISTANCE_INT))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_IS_METER_DISTANCE_INT))
				TOPLEFTFLAGS_COMBO[16][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_IS_METER_DISTANCE_INT)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[16][I] = FALSE
			ENDIF
	//		IF TOPLEFTFLAGS_COMBO[17][I] = TRUE
	//			aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_IS_MILE_DISTANCE_FLOAT))
	//		ELSE
	//			ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_IS_MILE_DISTANCE_FLOAT)
	//			aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
	//		ENDIF
	//		IF TOPLEFTFLAGS_COMBO[18][I] = TRUE
	//			aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_IS_MILE_DISTANCE_INT))
	//		ELSE
	//			ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_IS_MILE_DISTANCE_INT)
	//			aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
	//		ENDIF
			IF TOPLEFTFLAGS_COMBO[19][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISPERCENT_FLOAT)) = ENUM_TO_INT(MD_FLAGS_ISPERCENT_FLOAT))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISPERCENT_FLOAT))
				TOPLEFTFLAGS_COMBO[19][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISPERCENT_FLOAT)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[19][I] = FALSE
			ENDIF
			IF TOPLEFTFLAGS_COMBO[20][I] = TRUE
			OR ((aTopLeftScreenStruct.InformationStat[I].Flags&ENUM_TO_INT(MD_FLAGS_ISVALUE_SECONDARY)) = ENUM_TO_INT(MD_FLAGS_ISVALUE_SECONDARY))
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags|ENUM_TO_INT(MD_FLAGS_ISVALUE_SECONDARY))
				TOPLEFTFLAGS_COMBO[20][I] = TRUE
			ELSE
				ClearValue = ENUM_TO_INT(MD_FLAGS_MAX_BIT)-ENUM_TO_INT(MD_FLAGS_ISVALUE_SECONDARY)
				aTopLeftScreenStruct.InformationStat[I].Flags = (aTopLeftScreenStruct.InformationStat[I].Flags&ClearValue)
				TOPLEFTFLAGS_COMBO[20][I] = FALSE
			ENDIF
					
		ENDFOR
		
	ENDIF
	
ENDPROC


#ENDIF

