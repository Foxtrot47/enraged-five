// This header shouldn't be in the release project anyway, but wrap it up just in case
#IF IS_DEBUG_BUILD



USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "net_mission_info.sch"
USING "net_mission_control_debug.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Missions_At_Coords_Debug.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all MP Missions At Coords debug functionality.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Console Log Output Functions
// ===========================================================================================================

// PURPOSE:	Converts the MP Missions At Coords Banding ID to a string
//
// INPUT PARAMS:		paramBanding			The Missions At Coords Banding
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Missions_At_Coords_Banding_To_SameLength_String(g_eMatCBandings paramBanding)

	SWITCH (paramBanding)
		CASE MATCB_FOCUS_MISSION
			RETURN ("Focus    ")
			
		CASE MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE
			RETURN ("Nearby   ")
			
		CASE MATCB_MISSIONS_INSIDE_CORONA_RANGE
			RETURN ("MidRange ")
		
		CASE MATCB_MISSIONS_OUTSIDE_CORONA_RANGE
			RETURN ("LongRange")
		
		CASE MATCB_MISSIONS_FAR_AWAY_RANGE
			RETURN ("FarAway  ")
		
		CASE MATCB_NEW_MISSIONS
			RETURN ("New      ")
		
		CASE MAX_MISSIONS_AT_COORDS_BANDINGS
			RETURN ("MAX BANDINGS VALUE")
			
		CASE MATCB_MISSIONS_BEING_DELETED
			RETURN ("Delete   ")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: UNKNOWN MISSIONS AT COORDS BANDING ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Missions At Coords Stages ID to a string
//
// INPUT PARAMS:		paramStage				The Missions At Coords Stage
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Missions_At_Coords_Stage_To_String(g_eMatCStages paramStage)

	SWITCH (paramStage)
		CASE MATCS_STAGE_NEW
			RETURN ("Unprocessed Mission")
			
		CASE MATCS_STAGE_DELETE
			RETURN ("Mission To Be Deleted")
			
		CASE MATCS_SLEEPING
			RETURN ("Sleeping")
			
		CASE MATCS_OUTSIDE_ALL_RANGES
			RETURN ("Outside All Ranges")
			
		CASE MATCS_OUTSIDE_DISPLAY_RANGES
			RETURN ("Outside All Display Ranges")
			
		CASE MATCS_DISPLAY_CORONA_RANGE
			RETURN ("Display Corona Range")
			
		CASE MATCS_AREA_TRIGGERED_MISSION
			RETURN ("Area-Triggered Mission")
			
		CASE MATCS_MISSION_LAUNCH_IMMEDIATELY
			RETURN ("Mission Launches Immediately")
		
		CASE MATCS_REQUESTING_MISSION_NAME
			RETURN ("Requesting Mission Name")
		
		CASE MATCS_DISPLAYING_MISSION_NAME
			RETURN ("Displaying Mission Name")
		
		CASE MATCS_FORCING_AS_FOCUS_MISSION
			RETURN ("Forcing As Focus Mission")
			
		CASE MATCS_WAITING_FOR_ALLOW_RESERVATION
			RETURN ("Waiting For Allow Reservation")
		
		CASE MATCS_RESERVING_MISSION
			RETURN ("Reserving Mission")
			
		CASE MATCS_WAITING_FOR_CLOUD_DATA
			RETURN ("Waiting For Cloud Data")
		
		CASE MATCS_WAITING_TO_START
			RETURN ("Waiting To Start")
		
		CASE MATCS_MISSION_STARTING
			RETURN ("Mission Starting")
			
		CASE MATCS_WAITING_FOR_ON_MISSION
			RETURN ("Waiting For On Mission")
			
		CASE MATCS_WAITING_FOR_QUIT_CORONA
			RETURN ("Waiting For Quitting Corona")
			
		CASE MATCS_WAITING_FOR_OUTSIDE_FOCUS_RANGE
			RETURN ("Waiting For Outside Focus Range")
			
		CASE MATCS_DOWNLOAD_FOR_QUICK_LAUNCH
			RETURN ("Download For Quick Launch")
			
		CASE MATCS_MISSION_QUICK_LAUNCHING
			RETURN ("Mission Quick Launching")
		
		CASE MATCS_ON_MISSION
			RETURN ("On Mission")
			
		CASE MAX_MISSIONS_AT_COORDS_STAGES
			RETURN ("ILLEGAL STAGE")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: UNKNOWN MISSIONS AT COORDS STAGE ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Missions At Coords Feedback Type ID to a string
//
// INPUT PARAMS:		paramFeedbackType		The Missions At Coords Feedback Type
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Missions_At_Coords_Feedback_Type_To_String(g_eMatCFeedbackType paramFeedbackType)

	SWITCH (paramFeedbackType)
		CASE MATCFBT_STARTED
			RETURN ("STARTED")
			
		CASE MATCFBT_FINISHED
			RETURN ("FINISHED")
			
		CASE MATCFBT_NONE
			RETURN ("NONE")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: UNKNOWN MISSIONS AT COORDS FEEDBACK TYPE ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Missions At Coords External InCorona Stage ID to a string
//
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_MissionsAtCoords_InCorona_Stage_Flags_To_Strings()

	IF (g_sAtCoordsInCoronaMP.matcicInitialise)
		RETURN ("INITIALISE")
	ENDIF
			
	IF (g_sAtCoordsInCoronaMP.matcicAllowReserve)
		RETURN ("ALLOW RESERVATION")
	ENDIF
	
	IF (g_sAtCoordsInCoronaMP.matcicPlayerReserved)
		RETURN ("PLAYER RESERVED")
	ENDIF
			
	IF (g_sAtCoordsInCoronaMP.matcicCloudDataLoaded)
		RETURN ("CLOUD DATA LOADED")
	ENDIF
			
	IF (g_sAtCoordsInCoronaMP.matcicStartMission)
		RETURN ("START MISSION")
	ENDIF
	
	RETURN ("UNKNOWN STAGE")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Missions At Coords InCorona External ID to a string
//
// INPUT PARAMS:		paramExternalID			The Missions At Coords InCorona External ID
// RETURN VALUE:		STRING					The ID as a string
//
// NOTES:	THESE IDs ARE PROJECT-SPECIFIC
FUNC STRING Convert_Missions_At_Coords_InCorona_ExternalID_To_String(g_eMatCInCoronaExternalID paramExternalID)

	SWITCH (paramExternalID)
		CASE MATCICE_FMMC_LAUNCHER
			RETURN ("FMMC_Launcher")
			
		CASE MATCICE_FM_TUTORIAL
			RETURN ("FM Tutorial")
			
		CASE MATCICE_FM_CORONA_TUT
			RETURN ("FM Corona Tut")
			
		CASE MATCICE_NONE
			RETURN ("NONE - There are no External Actions")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: UNKNOWN MISSIONS AT COORDS IN-CORONA EXTERNAL ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the Details of one slot from the Missions At Coords array
//
// INPUT PARAMS:		paramSlot				The Array Pos to be output
PROC Debug_Output_Missions_At_Coords_Slot_In_One_Line(INT paramSlot)

	NET_PRINT("Slot ")
	NET_PRINT_INT(paramSlot)
	NET_PRINT(": ")
	
	IF (paramSlot >= g_numAtCoordsMP)
	OR (paramSlot < 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("ARRAY POSITION DOES NOT CONTAIN VALID DATA")
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NOT (Is_This_Cloud_Loaded_Data_In_Correct_ArrayPos(g_sAtCoordsMP[paramSlot].matcMissionIdData))
		#IF IS_DEBUG_BUILD
			NET_PRINT("NO DEBUG OUTPUT: DATA IS NOT IN SAME CLOUD-LOADED ARRAY POSITION (Avoid long-winded search for debug)")
		#ENDIF
		
		EXIT
	ENDIF
	
	NET_PRINT("[")
	NET_PRINT("REGID: ")		NET_PRINT_INT(g_sAtCoordsMP[paramSlot].matcRegID)
	IF (Does_MP_Mission_Variation_Get_Data_From_The_Cloud(g_sAtCoordsMP[paramSlot].matcMissionIdData.idMission, g_sAtCoordsMP[paramSlot].matcMissionIdData.idVariation))
		NET_PRINT("   (")			NET_PRINT(g_sAtCoordsMP[paramSlot].matcMissionIdData.idCloudFilename) NET_PRINT(")")
		TEXT_LABEL_63 theMissionName = Get_Mission_Name_For_FM_Cloud_Loaded_Activity(g_sAtCoordsMP[paramSlot].matcMissionIdData)
		NET_PRINT("   (")			NET_PRINT(theMissionName)
	ELSE
		NET_PRINT("   (")			NET_PRINT(GET_MP_MISSION_NAME(g_sAtCoordsMP[paramSlot].matcMissionIdData.idMission))
	ENDIF
	NET_PRINT(" - ")				NET_PRINT_INT(g_sAtCoordsMP[paramSlot].matcMissionIdData.idVariation) NET_PRINT(")")
	NET_PRINT("   ")			NET_PRINT_VECTOR(g_sAtCoordsMP[paramSlot].matcCoords)
	NET_PRINT("   (")			NET_PRINT(Convert_Mission_Source_To_String(g_sAtCoordsMP[paramSlot].matcSourceID)) NET_PRINT(")")
	NET_PRINT("  (Stage: ")		NET_PRINT(Convert_Missions_At_Coords_Stage_To_String(g_sAtCoordsMP[paramSlot].matcStage)) NET_PRINT(")")
	NET_PRINT("]")
	
	IF (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MARKED_FOR_DELETE))
		NET_PRINT(" - MARKED FOR DELETE")
	ELIF (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_AWAITING_ACTIVATION))
		NET_PRINT(" - AWAITING ACTIVATION")
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the Details of one Missions At Coords Band
//
// INPUT PARAMS:		paramBand				The Banding ID to be output
PROC Debug_Output_Missions_At_Coords_Banding_In_One_Line(g_eMatCBandings paramBand)
	
	NET_PRINT("[")
	NET_PRINT("Total: ") NET_PRINT_INT(g_sAtCoordsBandsMP[paramBand].matcbNumInBand)
	NET_PRINT("   First: ") NET_PRINT_INT(g_sAtCoordsBandsMP[paramBand].matcbFirstSlot)
	NET_PRINT("   Last: ") NET_PRINT_INT(g_sAtCoordsBandsMP[paramBand].matcbLastSlot)
	NET_PRINT("   (Update: ") NET_PRINT_INT(g_sAtCoordsBandsMP[paramBand].matcbUpdateSlot) NET_PRINT(")")
	NET_PRINT("]")

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the Details of all Missions At Coords Bands
//
// INPUT PARAMS:		paramAllDetails			[DEFAULT = FALSE] TRUE if the missions at coords array should be output too, otherwise FALSE for just the Bands array
PROC Debug_Output_Missions_At_Coords_Banding_Array(BOOL paramAllDetails = FALSE)

	NET_PRINT("      MISSIONS AT COORDS BANDING DETAILS (NOTE: Update Slot may be incorrect if Banding details have changed - this is sorted out during next update)") NET_NL()
	
	g_eMatCBandings	thisBanding
	INT				tempLoop	= 0
	INT				matcLoop	= 0
	INT				addedTotals	= 0
	
	REPEAT MAX_MISSIONS_AT_COORDS_BANDINGS tempLoop
		thisBanding	= INT_TO_ENUM(g_eMatCBandings, tempLoop)
		addedTotals	+= g_sAtCoordsBandsMP[tempLoop].matcbNumInBand
		
		NET_PRINT("         ")
		NET_PRINT(Convert_Missions_At_Coords_Banding_To_SameLength_String(thisBanding))
		NET_PRINT(" ")
		Debug_Output_Missions_At_Coords_Banding_In_One_Line(thisBanding)
		NET_NL()
		
		// Display the MissionsAtCoords array details too?
		IF (paramAllDetails)
			IF (g_sAtCoordsBandsMP[tempLoop].matcbNumInBand > 0)
				FOR matcLoop = g_sAtCoordsBandsMP[tempLoop].matcbFirstSlot TO g_sAtCoordsBandsMP[tempLoop].matcbLastSlot
					NET_PRINT("            ")
					Debug_Output_Missions_At_Coords_Slot_In_One_Line(matcLoop)
					NET_NL()
				ENDFOR
			ENDIF
		ENDIF
	ENDREPEAT
	
	NET_PRINT("                  TOTAL: ")
	NET_PRINT_INT(addedTotals)
	NET_PRINT("  g_numAtCoordsMP = ")
	NET_PRINT_INT(g_numAtCoordsMP)
	NET_PRINT(" (from ")
	NET_PRINT_INT(MAX_NUM_MP_MISSIONS_AT_COORDS)
	NET_PRINT(")")
	NET_NL()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the Details of all Missions At Coords Stages
PROC Debug_Output_Missions_At_Coords_Stages_Array()

	NET_PRINT("      MISSIONS AT COORDS STAGES INFO") NET_NL()
	
	g_eMatCStages	thisStage
	INT				tempLoop	= 0
	
	REPEAT MAX_MISSIONS_AT_COORDS_STAGES tempLoop
		thisStage	= INT_TO_ENUM(g_eMatCStages, tempLoop)
		
		NET_PRINT("         ")
		NET_PRINT(Convert_Missions_At_Coords_Stage_To_String(thisStage))
		NET_PRINT("m   Scheduling Band: ")
		NET_PRINT(Convert_Missions_At_Coords_Banding_To_SameLength_String(g_sAtCoordsStageInfo[tempLoop].matcbBand))
		NET_NL()
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the Details of all Missions At Coords Feedback array entries
PROC Debug_Output_Missions_At_Coords_Feedback_Array()

	NET_PRINT("      (Current NetTime: ")
	NET_PRINT(GET_TIME_AS_STRING(GET_NETWORK_TIME()))
	NET_PRINT(")  (Max Entries: ")
	NET_PRINT_INT(MAX_NUM_MP_MATC_FEEDBACK_SLOTS)
	NET_PRINT("):")
	NET_NL()
	
	IF (g_numAtCoordsFeedbackMP = 0)
		NET_PRINT("         <FEEDBACK ARRAY EMPTY>") NET_NL()
		EXIT
	ENDIF
	
	INT	tempLoop = 0
	REPEAT MAX_NUM_MP_MATC_FEEDBACK_SLOTS tempLoop
		IF (tempLoop < g_numAtCoordsFeedbackMP)
			NET_PRINT("         Slot: ")
			NET_PRINT_INT(tempLoop)
			NET_PRINT("  [Reg = ")
			NET_PRINT_INT(g_sAtCoordsFeedbackMP[tempLoop].matcfbRegID)
			NET_PRINT("]  [Feedback = ")
			NET_PRINT(Convert_Missions_At_Coords_Feedback_Type_To_String(g_sAtCoordsFeedbackMP[tempLoop].matcfbType))
			NET_PRINT("]  [Timeout = ")
			NET_PRINT(GET_TIME_AS_STRING(g_sAtCoordsFeedbackMP[tempLoop].matcfbTimeout))
			NET_PRINT("]")
			NET_NL()
		ENDIF
	ENDREPEAT
	
	IF (g_numAtCoordsFeedbackMP = MAX_NUM_MP_MATC_FEEDBACK_SLOTS)
		NET_PRINT("         <FEEDBACK ARRAY FULL>") NET_NL()
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output details of any passed-in optional parameters
//
// INPUT PARAMS:			paramOptions			The Options struct
PROC Debug_Output_MissionsAtCoords_Optional_Parameters(g_structMatCOptionsMP paramOptions)

	// MatC should handle the voting? MATC_BITFLAG_HANDLE_VOTING
	IF (paramOptions.matcoHandleVoting)
		NET_PRINT("         - OPTION: HANDLE VOTING") NET_NL()
	ENDIF

	// Delete this mission after it has been played? MATC_BITFLAG_DELETE_ON_PLAY
	IF (paramOptions.matcoDeleteOnPlay)
		NET_PRINT("         - OPTION: DELETE ON PLAY") NET_NL()
	ENDIF
	
	// Delete this mission after it has been the Focus Mission? MATC_BITFLAG_DELETE_AFTER_FOCUS
	IF (paramOptions.matcoDeleteAfterFocus)
		NET_PRINT("         - OPTION: DELETE AFTER FOCUS") NET_NL()
	ENDIF
	
	// Should this mission need to Await Activation again after being played? MATC_BITFLAG_AWAIT_ACTIVATION_AFTER_PLAY
	IF (paramOptions.matcoInactiveAfterPlay)
		NET_PRINT("         - OPTION: AWAIT ACTIVATION AFTER PLAY") NET_NL()
	ENDIF
	
	// Mission has a pre-mission cutscene (old-cnc style)? MATC_BITFLAG_HAS_CUTSCENE
	IF (paramOptions.matcoHasCutscene)
		NET_PRINT("         - OPTION: HAS PRE-MISSION CUTSCENE") NET_NL()
	ENDIF
	
	// Mission Blip should only appear when player nearby in an interior? MATC_BITFLAG_INTERIOR_BLIP_ONLY
	IF (paramOptions.matcoInteriorBlipOnly)
		NET_PRINT("         - OPTION: INTERIOR BLIP ONLY") NET_NL()
	ENDIF
	
	// Mission Blip should only appear on the Minimap? MATC_BITFLAG_MINIMAP_BLIP_ONLY
	IF (paramOptions.matcoMinimapBlipOnly)
		NET_PRINT("         - OPTION: MINIMAP BLIP ONLY") NET_NL()
	ENDIF

	// Mission has been played during previous session? MATC_BITFLAG_MISSION_HAS_BEEN_PLAYED
	IF (paramOptions.matcoHasBeenPlayed)
		NET_PRINT("         - OPTION: MARK AS PLAYED") NET_NL()
	ENDIF
	
	// Mission should ignore Cloud Refresh rules? MATC_BITFLAG_IGNORE_REFRESH
	IF (paramOptions.matcoIgnoreRefresh)
		NET_PRINT("         - OPTION: IGNORE CLOUD REFRESH RULES") NET_NL()
	ENDIF
	
	// Mission should ignore Cloud Refresh rules? MATC_BITFLAG_SHARE_CLOUD_DATA
	IF (paramOptions.matcoShareCloudData)
		NET_PRINT("         - OPTION: SHARE CLOUD-LOADED HEADER DATA") NET_NL()
	ENDIF
	
	// Mission should not display a blip? MATC_BITFLAG_NO_BLIP_TO_BE_DISPLAYED
	IF (paramOptions.matcoDisplayNoBlip)
		NET_PRINT("         - OPTION: MISSION DOES NOT DISPLAY A BLIP") NET_NL()
	ENDIF
	
	// Mission should not display a corona? MATC_BITFLAG_NO_CORONA_TO_BE_DISPLAYED
	IF (paramOptions.matcoDisplayNoCorona)
		NET_PRINT("         - OPTION: MISSION DOES NOT DISPLAY A CORONA") NET_NL()
	ENDIF
	
	// Mission should Quick Launch? MATC_BITFLAG_QUICK_LAUNCH
	IF (paramOptions.matcoQuickLaunch)
		NET_PRINT("         - OPTION: MISSION SHOULD QUICK LAUNCH") NET_NL()
	ENDIF
	
	// Mission should Remain Open If Active? MATC_BITFLAG_REMAIN_OPEN_IF_ACTIVE
	IF (paramOptions.matcoRemainOpenIfActive)
		NET_PRINT("         - OPTION: MISSION SHOULD REMAIN OPEN IF ACTIVE IN THE SESSION") NET_NL()
	ENDIF
	
	// Mission should Unlock only with a secondary unlock of it's type? MATC_BITFLAG_WAIT_FOR_SECONDARY_UNLOCK
	IF (paramOptions.matcoSecondaryUnlock)
		NET_PRINT("         - OPTION: MISSION UNLOCKS WITH SECONDARY UNLOCK, NOT INITIAL UNLOCK") NET_NL()
	ENDIF
	
	// Corona is being used only to trigger a Mocap Cutscene - ie: Heist pre-planning mocap? MATC_BITFLAG_FOR_CUTSCENE_ONLY
	IF (paramOptions.matcoForCutsceneOnly)
		NET_PRINT("         - OPTION: FOR CUTSCENE TRIGGERING ONLY") NET_NL()
	ENDIF
	
	// Corona is being used only to trigger specifically a Heist Mid-Strand Mocap Cutscene, not a Heist Intro Cutscene. MATC_BITFLAG_HEIST_MIDSTRAND_MOCAP
	// NOTE: The matcoForCutsceneOnly option should also be set if this is set - there is an assert elsewhere if not
	IF (paramOptions.matcoHeistMidStrandCut)
		NET_PRINT("         - OPTION: HEIST MID-STRAND CUTSCENE") NET_NL()
	ENDIF
	
	// Corona is being used to trigger specifically a Tutorial Heist Mocap Cutscene, which triggers the mission immediately afterwards. MATC_BITFLAG_HEIST_TUTORIAL_MOCAP
	// NOTE: The matcoForCutsceneOnly option should also be set if this is set - there is an assert elsewhere if not
	IF (paramOptions.matcoHeistTutorialCuts)
		NET_PRINT("         - OPTION: HEIST TUTORIAL CUTSCENE") NET_NL()
	ENDIF
	
	// Player is hosting their own instance of this corona? MATC_BITFLAG_HOST_OWN_INSTANCE.
	IF (paramOptions.matcoHostOwnInstance)
		NET_PRINT("         - OPTION: HOST OWN INSTANCE") NET_NL()
	ENDIF
	
	// Mission is Area Triggered? MATC_BITFLAG_AREA_TRIGGERED
	IF (paramOptions.matcoUseAngledArea)
		NET_PRINT("         - OPTION: MISSION IS ANGLED AREA TRIGGERED")
		NET_PRINT(" [min: ")
		NET_PRINT_VECTOR(paramOptions.matcoAngledAreaMin)
		NET_PRINT(" max: ")
		NET_PRINT_VECTOR(paramOptions.matcoAngledAreaMax)
		NET_PRINT(" width: ")
		NET_PRINT_FLOAT(paramOptions.matcoAngledAreaWidth)
		NET_PRINT("]")
		NET_NL()
	ENDIF
	
	// Mission should Launch Immediately when unlocked? MATC_BITFLAG_LAUNCH_IMMEDIATELY
	IF (paramOptions.matcoLaunchImmediately)
		NET_PRINT("         - OPTION: MISSION SHOULD LAUNCH IMMEDIATELY (WHEN UNLOCKED)") NET_NL()
	ENDIF
	
	// Mission is Invite Only? MATC_BITFLAG_INVITE_ONLY
	IF (paramOptions.matcoInviteOnly)
		NET_PRINT("         - OPTION: MISSION IS INVITE ONLY") NET_NL()
	ENDIF
	
	// Mission being setup as a Shared Mission? MATC_BITFLAG_ADDED_AS_SHARED_MISSION
	IF (paramOptions.matcoSharedMission)
		NET_PRINT("         - OPTION: MISSION IS SHARED MISSION") NET_NL()
	ENDIF

ENDPROC





// ===========================================================================================================
//      Consistency Checking Functions
// ===========================================================================================================

// PURPOSE:	Check the Banding Array data for inconsistencies
PROC Do_Missions_At_Coords_Banding_Array_Consistency_Checking()

	INT		tempLoop				= 0
	INT		expectedNextSlot		= 0
	INT		firstSlot				= 0
	INT		lastSlot				= 0
	INT		totalSlotsUsed			= 0
	INT		bitsUnclearedData		= ALL_MISSIONS_AT_COORDS_BITS_CLEAR
	INT		bitsUnSetData			= ALL_MISSIONS_AT_COORDS_BITS_CLEAR
	INT		bitsUnmatchedData		= ALL_MISSIONS_AT_COORDS_BITS_CLEAR
	INT		bitsInconsistentData	= ALL_MISSIONS_AT_COORDS_BITS_CLEAR
	BOOL	arrayConsistent			= TRUE
	
	REPEAT MAX_MISSIONS_AT_COORDS_BANDINGS tempLoop
		IF (g_sAtCoordsBandsMP[tempLoop].matcbNumInBand = 0)
			// ...there should be no details stored for this band
			IF (g_sAtCoordsBandsMP[tempLoop].matcbFirstSlot		!= INVALID_MATC_SLOT)
			OR (g_sAtCoordsBandsMP[tempLoop].matcbLastSlot		!= INVALID_MATC_SLOT)
			OR (g_sAtCoordsBandsMP[tempLoop].matcbUpdateSlot	!= INVALID_MATC_SLOT)
				SET_BIT(bitsUnclearedData, tempLoop)
				arrayConsistent = FALSE
			ENDIF
		ELSE
			// ...there should be full details stored for this band
			IF (g_sAtCoordsBandsMP[tempLoop].matcbFirstSlot		= INVALID_MATC_SLOT)
			OR (g_sAtCoordsBandsMP[tempLoop].matcbLastSlot		= INVALID_MATC_SLOT)
			OR (g_sAtCoordsBandsMP[tempLoop].matcbUpdateSlot	= INVALID_MATC_SLOT)
				SET_BIT(bitsUnSetData, tempLoop)
				arrayConsistent = FALSE
			ENDIF
			
			// ...the details within the band should match up (lastEntry - firstEntry + 1 should equal totalEntries in band)
			firstSlot		= g_sAtCoordsBandsMP[tempLoop].matcbFirstSlot
			lastSlot		= g_sAtCoordsBandsMP[tempLoop].matcbLastSlot
			totalSlotsUsed	= g_sAtCoordsBandsMP[tempLoop].matcbNumInBand
			
			IF (totalSlotsUsed != (lastSlot - firstSlot + 1))
				SET_BIT(bitsUnmatchedData, tempLoop)
				arrayConsistent = FALSE
			ENDIF
			
			// ...the details of this band should be consistent with what's expected from the previous bands
			IF (firstSlot != expectedNextSlot)
				SET_BIT(bitsInconsistentData, tempLoop)
				arrayConsistent = FALSE
			ENDIF
			
			expectedNextSlot = lastSlot + 1
		ENDIF
	ENDREPEAT
	
	// With all checks completed, the Expected Next Slot should equal the last used array position
	BOOL consistentTotalSlotsInUse = TRUE
	
	IF (expectedNextSlot != g_numAtCoordsMP)
		consistentTotalSlotsInUse	= FALSE
		arrayConsistent				= FALSE
	ENDIF

	// If there is any inconsistency, output the banding values but use a non-standard output format
	IF NOT (arrayConsistent)
		NET_NL()
		NET_PRINT("...KGM MP [At Coords]: MISSIONS AT COORDS BANDING DETAILS INCONSISTENCY DETECTED") NET_NL()
		
		g_eMatCBandings thisBanding
		INT				addedTotals	= 0
		
		REPEAT MAX_MISSIONS_AT_COORDS_BANDINGS tempLoop
			thisBanding = INT_TO_ENUM(g_eMatCBandings, tempLoop)
			addedTotals	+= g_sAtCoordsBandsMP[tempLoop].matcbNumInBand
			
			NET_PRINT("      ")
			NET_PRINT(Convert_Missions_At_Coords_Banding_To_SameLength_String(thisBanding))
			NET_PRINT(": ")
			Debug_Output_Missions_At_Coords_Banding_In_One_Line(thisBanding)
			
			IF (IS_BIT_SET(bitsUnclearedData, tempLoop))
				NET_PRINT(" [EMPTY BAND HAS SET DATA]")
			ENDIF
			
			IF (IS_BIT_SET(bitsUnSetData, tempLoop))
				NET_PRINT(" [ACTIVE BAND HAS UNSET DATA]")
			ENDIF
			
			IF (IS_BIT_SET(bitsUnmatchedData, tempLoop))
				NET_PRINT(" [TOTAL SLOTS DOESN'T MATCH FIRST/LAST SLOT DETAILS]")
			ENDIF
			
			IF (IS_BIT_SET(bitsInconsistentData, tempLoop))
				NET_PRINT(" [BAND DATA INCONSISTENT WITH EARLIER BANDS]")
			ENDIF
			
			NET_NL()
		ENDREPEAT
		
		// Print Total Slots In Use
		NET_PRINT("                TOTAL: ")
		NET_PRINT_INT(addedTotals)
		NET_PRINT("  g_numAtCoordsMP = ")
		NET_PRINT_INT(g_numAtCoordsMP)
		NET_PRINT(" (from ")
		NET_PRINT_INT(MAX_NUM_MP_MISSIONS_AT_COORDS)
		NET_PRINT(")")
		
		IF NOT (consistentTotalSlotsInUse)
			NET_PRINT(" [TOTAL SLOTS IN USE INCONSISTENT WITH EARLIER BANDS]")
		ENDIF
		
		NET_NL()
	ENDIF

ENDPROC





// ===========================================================================================================
//      Finding Missions Near Specified Coords Console Log Functions
// ===========================================================================================================

// PURPOSE:	Output all missions showing their distance from these coords and the closest mission to the coords - used when a search by coords failed
//
// INPUT PARAMS:			paramCoords			The coords being searched for
//							paramTolerance		The tolerance range from these coords
//							paramRSCreatedOnly	[DEFAULT = FALSE] TRUE if the output should only dealw ith R* Created Content only
PROC Debug_Output_Failed_To_Find_Mission_Near_Coords(VECTOR paramCoords, FLOAT paramTolerance, BOOL paramRSCreatedOnly = FALSE)

	NET_PRINT("          Passed Coords: ") NET_PRINT_VECTOR(paramCoords) NET_PRINT(" [tolerance = ") NET_PRINT_FLOAT(paramTolerance) NET_PRINT("m]") NET_NL()
	NET_PRINT("          Full List For Debug Checking")
	IF (paramRSCreatedOnly)
		NET_PRINT(" (Checking for Rockstar Created Content only)")
	ENDIF
	NET_PRINT(":") NET_NL()
	
	FLOAT	closestDistance = 99999.9
	FLOAT	thisDistance	= 0.0
	VECTOR	thisCoords		= << 0.0, 0.0, 0.0 >>
	INT		tempLoop		= 0
	BOOL	checkThisOne	= FALSE
	
	REPEAT g_numAtCoordsMP tempLoop
		NET_PRINT("            ") NET_PRINT_INT(tempLoop)
		IF (tempLoop < 10)
			NET_PRINT(" ")
		ENDIF
		IF (tempLoop < 100)
			NET_PRINT(" ")
		ENDIF
		NET_PRINT(") ")
		thisCoords = g_sAtCoordsMP[tempLoop].matcCoords
		NET_PRINT_VECTOR(thisCoords)
		thisDistance = GET_DISTANCE_BETWEEN_COORDS(thisCoords, paramCoords)
		NET_PRINT("   Distance: ")
		NET_PRINT_FLOAT(thisDistance)
		checkThisOne = FALSE
		IF (paramRSCreatedOnly)
			IF (g_sAtCoordsMP[tempLoop].matcMissionIdData.idCreator = FMMC_ROCKSTAR_CREATOR_ID)
				checkThisOne = TRUE
			ENDIF
		ELSE
			checkThisOne = TRUE
		ENDIF
		IF (checkThisOne)
			IF (thisDistance < closestDistance)
				closestDistance = thisDistance
				NET_PRINT("    [Closest So Far]")
			ENDIF
		ELSE
			NET_PRINT("         - IGNORING: Not Rockstar Created Content")
		ENDIF
		NET_NL()
	ENDREPEAT
		
ENDPROC





// ===========================================================================================================
//      Projected Ground Textures Functions
// ===========================================================================================================

// PURPOSE:	output the contents of the Ground Projections texture array
PROC Debug_Output_Ground_Projections()

	NET_PRINT("   GROUND PROJECTIONS ARRAY CONTENTS:") NET_NL()
	
	INT tempLoop = 0
	
	REPEAT MATC_CORONA_MAX_PROJECTIONS tempLoop
		NET_PRINT("      ")
		NET_PRINT_INT(tempLoop)
		NET_PRINT(")  ")
		SWITCH (g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].stage)
			CASE GTXD_STAGE_NOT_IN_USE					NET_PRINT("[not in use]            ")		BREAK
			CASE GTXD_STAGE_BEING_RENDERED				NET_PRINT("[IN USE]                ")		BREAK
			CASE GTXD_STAGE_UNPATCH_DECAL				NET_PRINT("[UNPATCH DECAL]         ")		BREAK
			CASE GTXD_STAGE_ALLOW_REUSE_NEXT_FRAME		NET_PRINT("[ALLOW REUSE NEXT FRAME]")		BREAK
			DEFAULT										NET_PRINT("[***UNKNOWN STAGE***]   ")		BREAK
		ENDSWITCH
		NET_PRINT("   RegID: ")
		NET_PRINT_INT(g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].regID)
		NET_PRINT("   CheckpointID: ")
		NET_PRINT_INT(NATIVE_TO_INT(g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].checkpointID))
		NET_NL()
	ENDREPEAT

ENDPROC





// ===========================================================================================================
//      Current PI Menu Hide/Show Blips Functions
// ===========================================================================================================

// PURPOSE:	output the current Show/Hide output for the PI Menu Hide/Show blips routines
PROC Debug_Output_PI_Menu_Hide_Blips_Status()

	PRINTLN(".KGM [At Coords]: Debug_Output_PI_Menu_Hide_Blips_Status (listed below if any)")
	
	// Jobs
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_FEATURED))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_FEATURED      : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_STUNT))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_STUNT      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_ADVERSARY))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_ADVERSARY      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_SPECIAL_RACE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_SPECIAL_RACE      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_BUNKER))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_BUNKER      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_TRANSFORM))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_TRANSFORM      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_ARENA_WAR))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_ARENA_WAR      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_SUPER_SPORT))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_SUPER_SPORT      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_RACE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_RACE      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_SURVIVAL))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_SURVIVAL      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_OPEN_WHEEL))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_OPEN_WHEEL      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_STREET_RACE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_STREET_RACE      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_PURSUIT))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_PURSUIT      : HIDE")
	ENDIF
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_SERIES_PREMIUM_RACE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_SERIES_PREMIUM_RACE      : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_LAND_RACE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_LAND_RACE      : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_BIKE_RACE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_BIKE_RACE      : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_AIR_RACE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_AIR_RACE       : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_SEA_RACE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_SEA_RACE       : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_PARACHUTE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_PARACHUTE      : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_DM))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_DM             : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_TEAM_DM))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_TEAM_DM        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_VEHICLE_DM))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_VEHICLE_DM     : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_MISSION))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_MISSION        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_CAPTURE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_CAPTURE        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_LTS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_LTS            : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_SURVIVAL))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_SURVIVAL       : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_KING_OF_THE_HILL))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_KING_OF_THE_HILL       : HIDE")
	ENDIF
		
	IF (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_OPEN_WHEEL))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_JOBS_JOB_OPEN_WHEEL       : HIDE")
	ENDIF
			
	// Ambient
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_ARM_WRESTLING))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_ARM_WRESTLING : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_CAR_WASH))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_CAR_WASH : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_DARTS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_DARTS         : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_FAIRGROUND))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_FAIRGROUND         : HIDE")
	ENDIF

	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_GOLF))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_GOLF          : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_MOVIES))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_MOVIES          : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_STRIP_CLUB))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_STRIP_CLUB          : HIDE")
	ENDIF

	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_TENNIS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_TENNIS        : HIDE")
	ENDIF

	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_FLIGHT_SCHOOL))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_FLIGHT_SCHOOL        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_TIME_TRIAL))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_TIME_TRIAL        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_RC_BANDITO_TIME_TRIAL))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_RC_BANDITO_TIME_TRIAL        : HIDE")
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_GEN9_HSW_TIME_TRIALS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_GEN9_HSW_TIME_TRIALS        : HIDE")
	ENDIF
	#ENDIF 
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_JUNK_ENERGY_SKYDIVES))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_JUNK_ENERGY_SKYDIVES        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_CHECKPOINTS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_CHECKPOINTS : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_CHALLENGE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_CHALLENGE : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_CRIMINAL_DAMAGE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_CRIMINAL_DAMAGE         : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_HOLD_THE_WHEEL))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_HOLD_THE_WHEEL         : HIDE")
	ENDIF

	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_HOT_PROPERTY))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_HOT_PROPERTY          : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_HUNT_THE_BEAST))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_HUNT_THE_BEAST          : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_KILL_LIST))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_KILL_LIST          : HIDE")
	ENDIF

	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_KING_OF_THE_CASTLE))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_KING_OF_THE_CASTLE        : HIDE")
	ENDIF

	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_PENNED_IN))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_PENNED_IN        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_BUSINESS_BATTLES))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_AMBIENT_EVENTS_BUSINESS_BATTLES        : HIDE")
	ENDIF
	
	// Misc
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_CEO_BUSINESS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_CEO_BUSINESS : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_MC_BUSINESS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_MC_BUSINESS : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_GUNRUNNING))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_GUNRUNNING         : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_AIR_FREIGHT))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_AIR_FREIGHT         : HIDE")
	ENDIF

	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_NIGHTCLUB))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_NIGHTCLUB          : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_ARCADE_BUSINESS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_ARCADE_BUSINESS          : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_AUTO_SHOP))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_RIVAL_BUSINESS_AUTO_SHOP          : HIDE")
	ENDIF

	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_PLAYER_NIGHTCLUBS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_PLAYER_NIGHTCLUBS        : HIDE")
	ENDIF

	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_PLAYER_ARCADES))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_PLAYER_ARCADES        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_PLAYER_AUTO_SHOPS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_PLAYER_AUTO_SHOPS        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_FREEMODE_WORK))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_FREEMODE_WORK        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_PROPERTY))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_PROPERTY        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_SUPER_YACHTS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_SUPER_YACHTS        : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_STORES))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_STORES_STORES : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_AMMUNATION))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_STORES_AMMUNATION : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_BARBER))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_STORES_BARBER         : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_CLOTHES))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_STORES_CLOTHES         : HIDE")
	ENDIF

	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_MOD_SHOP))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_STORES_MOD_SHOP          : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_TATTOO))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_STORES_TATTOO          : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_MASKS))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_STORES_MASKS         : HIDE")
	ENDIF
		
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_PREMIUM_DELUXE_MOTORSPORT))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_STORES_PREMIUM_DELUXE_MOTORSPORT         : HIDE")
	ENDIF
	
	IF (SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_LUXURY_AUTO))
		PRINTLN(".KGM [At Coords]: ...ciPI_HIDE_MENU_ITEM_MISC_STORES_LUXURY_AUTO         : HIDE")
	ENDIF


ENDPROC






#ENDIF	// IS_DEBUG_BUILD

