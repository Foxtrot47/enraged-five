USING "SceneTool_debug.sch"

PROC Private_SaveNET_REALTY_4_SCENE(STRUCT_NET_REALTY_4_SCENE& scene, INT iBuildingID)
	
	TEXT_LABEL_63 str
	str = Private_Get_Net_Realty_Building_Name(iBuildingID)
	str += "	//"
	str += Private_Get_Net_Realty_Building_Description(iBuildingID)
	
	SceneTool_OpenDebugFile("Private_Get_NET_REALTY_4_SCENE()",				str)

		SceneTool_ExportPan(	"NET_REALTY_4_SCENE_PAN_walkOut",			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut])

		SceneTool_ExportCut(	"NET_REALTY_4_SCENE_CUT_catchup",			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup])

		SceneTool_ExportMarker(	"NET_REALTY_4_SCENE_MARKER_walkTo",			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo])

		SceneTool_ExportPlacer(	"NET_REALTY_4_SCENE_PLACER_resetCoords",	scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords])

		SceneTool_ExportPlacer(	"NET_REALTY_4_SCENE_PLACER_occupiedSphere",	scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere])

		SceneTool_ExportBool(	"bEnablePans[0]",				scene.bEnablePans[0])
		
		SceneTool_ExportFloat(	"fExitDelay",					scene.fExitDelay)

	SceneTool_CloseDebugFile("Private_Get_NET_REALTY_4_SCENE()")
	
ENDPROC

PROC Private_DebugPlayNET_REALTY_4_SCENE(STRUCT_NET_REALTY_4_SCENE &scene, INT iCurrentProperty,
		INT &iWalkOutStage,
		CAMERA_INDEX &WalkOutCam0, CAMERA_INDEX &WalkOutCam1,
		SCRIPT_TIMER &iWalkOutTimer,
		SCRIPT_TIMER &iWalkOutCamInterpDelayTimer,
		SCRIPT_TIMER &iWalkOutCamEndTimer)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		WHILE NOT WALK_OUT_OF_THIS_PROPERTY(scene, iCurrentProperty, iWalkOutStage, WalkOutCam0, WalkOutCam1,
				iWalkOutTimer, iWalkOutCamInterpDelayTimer, iWalkOutCamEndTimer,FALSE)
			WAIT(0)
		ENDWHILE
		
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
								//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF

ENDPROC

PROC Private_Edit_NET_REALTY_4_SCENE(structSceneTool_Launcher& launcher, INT iBuildingID, INT iCurrentProperty,
		CAMERA_INDEX &WalkOutCam0, CAMERA_INDEX &WalkOutCam1,
		SCRIPT_TIMER &iWalkOutTimer, SCRIPT_TIMER &iWalkOutCamInterpDelayTimer, SCRIPT_TIMER &iWalkOutCamEndTimer,
		OBJECT_INDEX &doorBlock, OBJECT_INDEX &garageBlock)

	// Get scene data
	STRUCT_NET_REALTY_4_SCENE		scene
	//structSceneTool tool
	
	// bOutputMissingNetRealtyScenes
	INT iBuilding
	REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
		IF iBuilding = 0
			
		ELSE
			
			PRINTSTRING("property ")
			PRINTSTRING(Private_Get_Net_Realty_Building_Name(iBuilding))
			PRINTSTRING(" ")
			
			IF Private_Get_NET_REALTY_4_SCENE(iBuilding, scene, TRUE)
				PRINTSTRING("has scene four, ")
			ELSE
				PRINTSTRING("missing scene four, ")
			ENDIF
			
			PRINTNL()
		ENDIF
	ENDREPEAT
	PRINTNL()
	//	//	//	//	//	//
	
	Private_Get_NET_REALTY_4_SCENE(iBuildingID, scene)
	
	// Add widgets
	START_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool,Private_Get_Net_Realty_Building_Name(iBuildingID))

		START_WIDGET_GROUP("Camera shots")
			START_WIDGET_GROUP(		"Pan shot")
				ADD_WIDGET_PAN(		"Pan - ",					g_sAmMpPropertyExtSceneTool,scene.mPans,			ENUM_TO_INT(NET_REALTY_4_SCENE_PAN_walkOut))
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP(		"Cut shot")
				ADD_WIDGET_Cut(		"Cut - ",					g_sAmMpPropertyExtSceneTool,scene.mCuts,			ENUM_TO_INT(NET_REALTY_4_SCENE_CUT_catchup))
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Markers")
			ADD_WIDGET_MARKER(		"MARKER_walkTo",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,		ENUM_TO_INT(NET_REALTY_4_SCENE_MARKER_walkTo))
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Placers")
			ADD_WIDGET_PLACER(		"PLACER_resetCoords",		g_sAmMpPropertyExtSceneTool,scene.mPlacers,		ENUM_TO_INT(NET_REALTY_4_SCENE_PLACER_resetCoords))
			ADD_WIDGET_PLACER(		"PLACER_occupiedSphere",	g_sAmMpPropertyExtSceneTool,scene.mPlacers,		ENUM_TO_INT(NET_REALTY_4_SCENE_PLACER_occupiedSphere))
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("*sliders*")
			START_WIDGET_GROUP("Marker")
				START_WIDGET_GROUP("MARKER_walkTo")
					ADD_WIDGET_VECTOR_SLIDER(	"MARKER_walkTo", scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Placer")
				START_WIDGET_GROUP("PLACER_resetCoords")
					ADD_WIDGET_VECTOR_SLIDER(	"PLACER_resetCoords",	scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("PLACER_occupiedSphere")
					ADD_WIDGET_VECTOR_SLIDER(	"PLACER_occupiedSphere",	scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
	STOP_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool)
	
	// Warp to player pos marker
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot)
	ENDIF
	
	INT iWalkOutStage = WOS_INIT
	
	// Update widgets
	WHILE launcher.bEnableTool
	
		// Update system
		SWITCH SceneTool_UpdateTool(g_sAmMpPropertyExtSceneTool)
			
			CASE SCENETOOL_ACTION_RunScene
				Private_DebugPlayNET_REALTY_4_SCENE(scene, iCurrentProperty,
						iWalkOutStage, WalkOutCam0, WalkOutCam1,
						iWalkOutTimer, iWalkOutCamInterpDelayTimer, iWalkOutCamEndTimer)
			BREAK
			
			CASE SCENETOOL_ACTION_SaveScene
				Private_SaveNET_REALTY_4_SCENE(scene, iBuildingID)
			BREAK
			
			CASE SCENETOOL_ACTION_UpdateData
				SceneTool_UpdatePans(g_sAmMpPropertyExtSceneTool, scene.mPans)
				SceneTool_UpdateCuts(g_sAmMpPropertyExtSceneTool, scene.mCuts)
				SceneTool_UpdateMarkers(g_sAmMpPropertyExtSceneTool, scene.mMarkers)
				SceneTool_UpdatePlacers(g_sAmMpPropertyExtSceneTool, scene.mPlacers)
			BREAK
		
		ENDSWITCH
				
		// Draw markers
		SceneTool_DrawMarker2 (g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(NET_REALTY_4_SCENE_MARKER_walkTo),			200, 0, 200, scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos)
		
		// Draw placers
		SceneTool_DrawPlacer (g_sAmMpPropertyExtSceneTool, scene.mPlacers,	ENUM_TO_INT(NET_REALTY_4_SCENE_PLACER_resetCoords),		255, 0, 75)
		
		SceneTool_DrawPlacer (g_sAmMpPropertyExtSceneTool, scene.mPlacers,		ENUM_TO_INT(NET_REALTY_4_SCENE_PLACER_occupiedSphere),		75, 75, 200)
		DRAW_DEBUG_SPHERE(scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos, scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot,						75, 75, 200, 64)
		KILL_BLOCKING_OBJECTS(doorBlock,garageBlock)
		WAIT(0)

	ENDWHILE
	
	// Cleanup system
	SWITCH SceneTool_DestroyTool(g_sAmMpPropertyExtSceneTool)
		CASE SCENETOOL_ACTION_SaveScene
			Private_SaveNET_REALTY_4_SCENE(scene, iBuildingID)
		BREAK
	ENDSWITCH

ENDPROC

