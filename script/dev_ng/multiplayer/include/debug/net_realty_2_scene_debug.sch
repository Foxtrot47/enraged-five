USING "SceneTool_debug.sch"

PROC Private_Savenet_realty_2_scene(STRUCT_NET_REALTY_2_SCENE& scene, INT iBuildingID)
	
	TEXT_LABEL_63 str
	str = Private_Get_Net_Realty_Building_Name(iBuildingID)
	str += "	//"
	str += Private_Get_Net_Realty_Building_Description(iBuildingID)
	
	SceneTool_OpenDebugFile("Private_Get_net_realty_2_SCENE()",				str)

		SceneTool_ExportCut(	"NET_REALTY_2_SCENE_CUT_start",				scene.mCuts[NET_REALTY_2_SCENE_CUT_start])
		SceneTool_ExportPan(	"NET_REALTY_2_SCENE_PAN_descent",			scene.mPans[NET_REALTY_2_SCENE_PAN_descent])

		SceneTool_ExportMarker(	"NET_REALTY_2_SCENE_MARKER_gotoA",			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA])
		SceneTool_ExportMarker(	"NET_REALTY_2_SCENE_MARKER_findCoord",		scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord])
		SceneTool_ExportMarker(	"NET_REALTY_2_SCENE_MARKER_gotoB",			scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB])

		SceneTool_ExportPlacer(	"NET_REALTY_2_SCENE_PLACER_resetCoords",	scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords])
		SceneTool_ExportPlacer(	"NET_REALTY_2_SCENE_PLACER_gotoA",			scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA])
		
		SceneTool_ExportAngArea("NET_REALTY_2_SCENE_ANGAREA_area0",			scene.mAngAreas[NET_REALTY_2_SCENE_ANGAREA_area0])

		SceneTool_ExportBool(	"bEnablePans[0]",				scene.bEnablePans[0])
		
		SceneTool_ExportFloat(	"fExitDelay",					scene.fExitDelay)

	SceneTool_CloseDebugFile("Private_Get_net_realty_2_scene()")
	
ENDPROC

TYPEDEF FUNC BOOL SceneRealty2Func(BOOL bResetCoords, STRUCT_NET_REALTY_2_SCENE &scene)

PROC Private_DebugPlaynet_realty_2_scene(STRUCT_NET_REALTY_2_SCENE& scene, SceneRealty2Func customRealty2Cutscene)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		WHILE NOT CALL customRealty2Cutscene(TRUE, scene)
			WAIT(0)
		ENDWHILE
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
								//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF

ENDPROC

PROC Private_Edit_net_realty_2_scene(structSceneTool_Launcher& launcher, INT iBuildingID, SceneRealty2Func customRealty2Cutscene, OBJECT_INDEX &doorBlock, OBJECT_INDEX &garageBlock)

	// Get scene data
	STRUCT_NET_REALTY_2_SCENE		scene
	//structSceneTool tool
	
	// bOutputMissingNetRealtyScenes
	INT iBuilding
	REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
		IF iBuilding = 0
			
		ELSE
			
			PRINTSTRING("property ")
			PRINTSTRING(Private_Get_Net_Realty_Building_Name(iBuilding))
			PRINTSTRING(" ")
			
			IF Private_Get_NET_REALTY_2_SCENE(iBuilding, scene, TRUE)
				PRINTSTRING("has scene two, ")
			ELSE
				PRINTSTRING("missing scene two, ")
			ENDIF
			
			PRINTNL()
		ENDIF
	ENDREPEAT
	PRINTNL()
	//	//	//	//	//	//
	
	Private_Get_net_realty_2_scene(iBuildingID, scene)

	// Add widgets
	START_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool,Private_Get_Net_Realty_Building_Name(iBuildingID))

		START_WIDGET_GROUP("Camera shots")
			START_WIDGET_GROUP(		"Establishing shot")
				ADD_WIDGET_CUT(		"CUT_start",			g_sAmMpPropertyExtSceneTool,scene.mCuts,		ENUM_TO_INT(NET_REALTY_2_SCENE_CUT_start))
				ADD_WIDGET_PAN(		"PAN_descent",			g_sAmMpPropertyExtSceneTool,scene.mPans,		ENUM_TO_INT(NET_REALTY_2_SCENE_PAN_descent))
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Markers")
			ADD_WIDGET_MARKER(		"MARKER_gotoA",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,	ENUM_TO_INT(NET_REALTY_2_SCENE_MARKER_gotoA))
			ADD_WIDGET_MARKER(		"MARKER_findCoord",		g_sAmMpPropertyExtSceneTool,scene.mMarkers,	ENUM_TO_INT(NET_REALTY_2_SCENE_MARKER_findCoord))
			ADD_WIDGET_MARKER(		"MARKER_gotoB",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,	ENUM_TO_INT(NET_REALTY_2_SCENE_MARKER_gotoB))
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Placers")
			ADD_WIDGET_PLACER(		"PLACER_resetCoords",	g_sAmMpPropertyExtSceneTool,scene.mPlacers,	ENUM_TO_INT(NET_REALTY_2_SCENE_PLACER_resetCoords))
			ADD_WIDGET_PLACER(		"PLACER_gotoA",			g_sAmMpPropertyExtSceneTool,scene.mPlacers,	ENUM_TO_INT(NET_REALTY_2_SCENE_PLACER_gotoA))
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Angled Area")
			ADD_WIDGET_ANGAREA(		"ANGAREA_area0",		g_sAmMpPropertyExtSceneTool,scene.mAngAreas,	ENUM_TO_INT(NET_REALTY_2_SCENE_ANGAREA_area0))
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("*sliders*")
			START_WIDGET_GROUP("Marker")
				START_WIDGET_GROUP("MARKER_gotoA")
					ADD_WIDGET_VECTOR_SLIDER(	"MARKER_gotoA", scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("MARKER_findCoord")
					ADD_WIDGET_VECTOR_SLIDER(	"MARKER_findCoord", scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("MARKER_gotoB")
					ADD_WIDGET_VECTOR_SLIDER(	"MARKER_gotoB", scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoB].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Placer")
				START_WIDGET_GROUP("PLACER_resetCoords")
					ADD_WIDGET_VECTOR_SLIDER(	"PLACER_resetCoords",	scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("PLACER_gotoA")
					ADD_WIDGET_VECTOR_SLIDER(	"PLACER_gotoA",	scene.mPlacers[NET_REALTY_2_SCENE_PLACER_gotoA].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
	STOP_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool)
	
	// Warp to player pos marker
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].fRot)
	ENDIF
	
	// Update widgets
	WHILE launcher.bEnableTool
	
		// Update system
		SWITCH SceneTool_UpdateTool(g_sAmMpPropertyExtSceneTool)
			
			CASE SCENETOOL_ACTION_RunScene
				Private_DebugPlaynet_realty_2_scene(scene, customRealty2Cutscene)
			BREAK
			
			CASE SCENETOOL_ACTION_SaveScene
				Private_Savenet_realty_2_scene(scene, iBuildingID)
			BREAK
			
			CASE SCENETOOL_ACTION_UpdateData
				SceneTool_UpdatePans(g_sAmMpPropertyExtSceneTool, scene.mPans)
				SceneTool_UpdateCuts(g_sAmMpPropertyExtSceneTool, scene.mCuts)
				SceneTool_UpdateMarkers(g_sAmMpPropertyExtSceneTool, scene.mMarkers)
				SceneTool_UpdatePlacers(g_sAmMpPropertyExtSceneTool, scene.mPlacers)
				SceneTool_UpdateAngAreas(g_sAmMpPropertyExtSceneTool, scene.mAngAreas)
			BREAK
		
		ENDSWITCH
		
		// Draw markers
		SceneTool_DrawMarker2 (g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(NET_REALTY_2_SCENE_MARKER_gotoA),			200, 0, 200, scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos)
		SceneTool_DrawMarker2 (g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(NET_REALTY_2_SCENE_MARKER_gotoB),			200, 0, 200, scene.mMarkers[NET_REALTY_2_SCENE_MARKER_gotoA].vPos)
		
		SceneTool_DrawMarker (g_sAmMpPropertyExtSceneTool, scene.mMarkers,		ENUM_TO_INT(NET_REALTY_2_SCENE_MARKER_findCoord),		75, 75, 200)
		DRAW_DEBUG_SPHERE(scene.mMarkers[NET_REALTY_2_SCENE_MARKER_findCoord].vPos, 8.0,						75, 75, 200, 64)
		
		// Draw placers
		SceneTool_DrawPlacer (g_sAmMpPropertyExtSceneTool, scene.mPlacers,		ENUM_TO_INT(NET_REALTY_2_SCENE_PLACER_resetCoords),		255, 0, 75)
		SceneTool_DrawPlacer2(g_sAmMpPropertyExtSceneTool, scene.mPlacers,		ENUM_TO_INT(NET_REALTY_2_SCENE_PLACER_gotoA),			255, 0, 75, scene.mPlacers[NET_REALTY_2_SCENE_PLACER_resetCoords].vPos)
		
		// Draw Angled Areas
		SceneTool_DrawAngArea(g_sAmMpPropertyExtSceneTool, scene.mAngAreas,	ENUM_TO_INT(NET_REALTY_2_SCENE_ANGAREA_area0),			255, 0, 75)
		KILL_BLOCKING_OBJECTS(doorBlock,garageBlock)
		WAIT(0)

	ENDWHILE
	
	// Cleanup system
	SWITCH SceneTool_DestroyTool(g_sAmMpPropertyExtSceneTool)
		CASE SCENETOOL_ACTION_SaveScene
			Private_Savenet_realty_2_scene(scene, iBuildingID)
		BREAK
	ENDSWITCH
	
ENDPROC

