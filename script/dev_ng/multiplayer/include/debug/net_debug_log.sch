// This header shouldn't be in the release project anyway, but wrap it up just in case
#IF IS_DEBUG_BUILD



USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "net_include.sch"

USING "net_mission.sch"

// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Debug_Log.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Maintains all 'net_log' debug messages to record important script events from all players.
//		NOTES:			:	Uses SHIFT-F11 as the display toggle.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************



// The additional text maximum length
CONST_INT	MAX_ADDITIONAL_TEXT_LENGTH		63

// The maximum player name length
CONST_INT	MAX_PLAYER_NAME_LENGTH			15




// ===========================================================================================================
//      Display Values
// ===========================================================================================================

CONST_FLOAT	DebugLogTextScaleX			0.2830
CONST_FLOAT	DebugLogTextScaleY			0.2430

CONST_FLOAT	DebugLogStartY				0.1090
CONST_FLOAT DebugLogAddY				0.0230

CONST_FLOAT	DebugLogRectX				0.5980
CONST_FLOAT	DebugLogRectY				0.1030
CONST_FLOAT	DebugLogRectWidth			0.3750
CONST_FLOAT	DebugLogRectHeight			0.5920
CONST_INT	DebugLogRectAlpha			175

CONST_FLOAT	DebugLogMissionInstX		0.6040

CONST_FLOAT	DebugLogTimeX				0.6120
CONST_FLOAT	DebugLogPlayerX				0.6450
CONST_FLOAT	DebugLogTextX				0.7340

CONST_INT	DebugLogFadedOutAlpha		30
CONST_INT	DebugLogFadeTimeSecs		120

//Contact mission
CONST_FLOAT	DbgCM_RectX				0.006
CONST_FLOAT	DbgCM_RectY				0.013
CONST_FLOAT	DbgCM_RectWidth			0.987
CONST_FLOAT	DbgCM_RectHeight		0.735
CONST_INT	DbgCM_RectAlpha			200

CONST_FLOAT	DbgCM_TextScaleX		0.258
CONST_FLOAT	DbgCM_TextScaleY		0.268
CONST_INT	DbgCM_TextAlpha			70

CONST_FLOAT DbgCM_AddYSpace			0.020
CONST_FLOAT	DbgCM_addColSpaceX		0.338
CONST_FLOAT DbgPlayerListYSpace  	0.001
CONST_INT 	DbgCM_Rows  			32

CONST_FLOAT	DbgCM_TitleX			0.430
CONST_FLOAT	DbgCM_TitleY			0.020
CONST_FLOAT	DbgCM_infoStartY		0.051
CONST_FLOAT	DbgCM_infoStartX		0.026


// ===========================================================================================================
//      Widgets
// ===========================================================================================================

DEBUGONLY PROC Create_MP_Debug_Log_Widgets()

	START_WIDGET_GROUP("Debug Log Screen")
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogTextScaleX",			DebugLogTextScaleX,		0.0,	1.0,	0.005)
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogTextScaleY",			DebugLogTextScaleY,		0.0,	1.0,	0.005)
//		
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogStartY",				DebugLogStartY,			0.0,	1.0,	0.005)
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogAddY",				DebugLogAddY,			0.0,	1.0,	0.005)
//		
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogRectX",				DebugLogRectX,			0.0,	1.0,	0.005)
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogRectY",				DebugLogRectY,			0.0,	1.0,	0.005)
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogRectWidth",			DebugLogRectWidth,		0.0,	1.0,	0.005)
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogRectHeight",			DebugLogRectHeight,		0.0,	1.0,	0.005)
//		ADD_WIDGET_INT_SLIDER	("DebugLogRectAlpha",			DebugLogRectAlpha,		0,		255,	1)
//		
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogMissionInstX",		DebugLogMissionInstX,	0.0,	1.0,	0.005)
//
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogTimeX",				DebugLogTimeX,			0.0,	1.0,	0.005)
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogPlayerX",				DebugLogPlayerX,		0.0,	1.0,	0.005)
//		ADD_WIDGET_FLOAT_SLIDER	("DebugLogTextX",				DebugLogTextX,			0.0,	1.0,	0.005)
//
//		ADD_WIDGET_INT_SLIDER	("DebugLogFadedOutAlpha [min alpha to remove entry]",	DebugLogFadedOutAlpha,		0,		255,		1)
//		ADD_WIDGET_INT_SLIDER	("DebugLogFadeTimeSecs [only if not my mission]",		DebugLogFadeTimeSecs,		1,		3600,		10)
		
//		START_WIDGET_GROUP("Contact missions menu")
//			START_WIDGET_GROUP("Contact missions menu - rectangle")
//				ADD_WIDGET_FLOAT_SLIDER	("RectX",						DbgCM_RectX,			0.0,	1.0,	0.005)
//				ADD_WIDGET_FLOAT_SLIDER	("RectY",						DbgCM_RectY,			0.0,	1.0,	0.005)
//				ADD_WIDGET_FLOAT_SLIDER	("RectWidth",					DbgCM_RectWidth,		0.0,	1.0,	0.005)
//				ADD_WIDGET_FLOAT_SLIDER	("RectHeight",					DbgCM_RectHeight,		0.0,	1.0,	0.005)
//				ADD_WIDGET_INT_SLIDER	("RectAlpha",					DbgCM_RectAlpha,		0,		255,	1)
//			STOP_WIDGET_GROUP()			
//			START_WIDGET_GROUP("Contact missions menu - scale's & gap's")
//				//scale
//				ADD_WIDGET_FLOAT_SLIDER	("TextScaleX",					DbgCM_TextScaleX,		0.0,	1.0,	0.005)
//				ADD_WIDGET_FLOAT_SLIDER	("TextScaleY",					DbgCM_TextScaleY,		0.0,	1.0,	0.005)				
//				ADD_WIDGET_INT_SLIDER	("Text Alpha",					DbgCM_TextAlpha,		0,		255,	1)	
//				//gaps
//				ADD_WIDGET_FLOAT_SLIDER	("AddY space",					DbgCM_AddYSpace,		0.0,	1.0,	0.005)
//				ADD_WIDGET_FLOAT_SLIDER	("coloum X space",				DbgCM_addColSpaceX,		0.0,	1.0,	0.005)
//				ADD_WIDGET_FLOAT_SLIDER	("DbgPlayerListYSpace",			DbgPlayerListYSpace,	0.0,	1.0,	0.001)
//				ADD_WIDGET_INT_SLIDER	("Row amount",					DbgCM_Rows,				0,		200,	1)	
//			STOP_WIDGET_GROUP()
//			START_WIDGET_GROUP("Contact missions menu - Positions")		
//				ADD_WIDGET_FLOAT_SLIDER	("DbgTitleX",					DbgCM_TitleX,			0.0,	1.0,	0.005)
//				ADD_WIDGET_FLOAT_SLIDER	("DbgTitleY",					DbgCM_TitleY,			0.0,	1.0,	0.005)
//				ADD_WIDGET_FLOAT_SLIDER	("Mission info Y",				DbgCM_infoStartY,		0.0,	1.0,	0.005)
//				ADD_WIDGET_FLOAT_SLIDER	("Mission info X",				DbgCM_infoStartX,		0.0,	1.0,	0.005)		
//			STOP_WIDGET_GROUP()			
//		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()

ENDPROC




// ===========================================================================================================
//      Message Log Array Functions
// ===========================================================================================================

// PURPOSE:	Shuffle the whole data array down from (and including) the passed in position
//
// INPUT PARAMS:		paramArrayPos			The position where the new data will be added
PROC Create_Space_In_Net_Debug_Message_Log_At_This_Position(INT paramArrayPos)

	INT toPos = g_currentdebugLogLines
	INT fromPos = toPos - 1
	
	WHILE (fromPos >= paramArrayPos)
		IF (toPos < MAX_MP_DEBUG_LOG_LINES)
			g_sMPDebugLogLines[toPos] = g_sMPDebugLogLines[fromPos]
		ENDIF
		
		toPos--
		fromPos--
	ENDWHILE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Remove the entry from the array at the specified position, update array size counter, clear the last data entry
//
// INPUT PARAMS:		paramArrayPos			The array position that will be overwritten
PROC Remove_Net_Debug_Message_From_Log_At_Position(INT paramArrayPos)

	IF (g_currentdebugLogLines = 0)
		EXIT
	ENDIF

	INT toPos = paramArrayPos
	INT fromPos = toPos + 1
	
	WHILE (fromPos < g_currentdebugLogLines)
		g_sMPDebugLogLines[toPos] = g_sMPDebugLogLines[fromPos]
		
		toPos++
		fromPos++
	ENDWHILE
	
	// Clearout the entry at the end
	g_structMPDebugLogLineData emptyData
	g_sMPDebugLogLines[toPos] = emptyData
	
	// Reduce the number of entries in the array
	g_currentdebugLogLines--

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Insert this data into the correct position in the net log array based on the timestamp
//
// INPUT PARAMS:		paramLogData			The new data to be inserted
PROC Add_Net_Debug_Message_To_Log(g_structMPDebugLogLineData paramLogData)

	TIME_DATATYPE theTimestamp = paramLogData.timestamp
	
	// The newest logs are at array position 0, so insert this data in the correct place
	TIME_DATATYPE testTimestamp
	
	INT insertPosition = 0
	REPEAT g_currentdebugLogLines insertPosition
		testTimestamp = g_sMPDebugLogLines[insertPosition].timestamp
		IF IS_TIME_MORE_THAN(theTimestamp , testTimestamp)
			// ...create space
			Create_Space_In_Net_Debug_Message_Log_At_This_Position(insertPosition)
			
			// ...insert here
			g_sMPDebugLogLines[insertPosition] = paramLogData
			
			// ...update the data counter (but if it is already at max position then the insert dumped an old log entry)
			g_currentdebugLogLines++
			IF (g_currentdebugLogLines > MAX_MP_DEBUG_LOG_LINES)
				g_currentdebugLogLines = MAX_MP_DEBUG_LOG_LINES
			ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT
	
	// Everything in the log is newer than this new data, so add it at the end of the valid data unless the array is full
	IF (g_currentdebugLogLines >= MAX_MP_DEBUG_LOG_LINES)
		EXIT
	ENDIF
	
	// There is room, so insert here
	g_sMPDebugLogLines[g_currentdebugLogLines] = paramLogData
	
	// Update the data counter
	g_currentdebugLogLines++

ENDPROC




// ===========================================================================================================
//      Display Functions
// ===========================================================================================================

// PURPOSE:	Get the line alpha - an entry in the log will fade over time
//
// INPUT PARAMS:		paramArrayPos			Array Position within message log
//						paramMyMission			TRUE if this message is for My Active Mission, otherwise FALSE
//
// NOTES:	If the fadeout timer has already started then allow fadeout to continue - this allows systems messages about a particular mission to fade because the timer has already started
DEBUGONLY FUNC INT Get_MP_Debug_Log_Line_Alpha(INT paramArrayPos, BOOL paramMyMission)

	// Is this entry already fading out?
	IF NOT (g_sMPDebugLogLines[paramArrayPos].fadeStartTimestampInitialised)
		// ...no, so check if it should be fading out - it should be if this isn't the player's mission
		IF NOT (paramMyMission)
			// ...yes, start it fading out
			g_sMPDebugLogLines[paramArrayPos].fadeStartTimestamp = GET_NETWORK_TIME()
			g_sMPDebugLogLines[paramArrayPos].fadeStartTimestampInitialised = TRUE
		ENDIF
	ENDIF
	
	// Only calculate the alpha if fadeout has started
	IF NOT (g_sMPDebugLogLines[paramArrayPos].fadeStartTimestampInitialised)
		RETURN 255
	ENDIF
	
	// Get fade time
	TIME_DATATYPE fadeStartTime = g_sMPDebugLogLines[paramArrayPos].fadeStartTimestamp
	
	// The number of msecs the fade has been ongoing
	INT ongoingFadeMsecs = GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , fadeStartTime)
	
	// The total number of msecs the fade will last for
	INT totalFadeMsecs = DebugLogFadeTimeSecs * 1000
	
	// The alpha range
	INT theAlphaRange = 255 - DebugLogFadedOutAlpha
	
	// The fade value = 255 - ((ongoingFadeTime / totalFadeTime) * theAlphaRange)
	// EG: ongoingFadeTime = 60000, totalFadeTime = 240000, alphaRange = 120 (= 60000*120/240000 = 30 :- alpha = 255 - 30 = 225)
	INT fadeValue = (255 - (ongoingFadeMsecs * theAlphaRange / totalFadeMsecs))
	IF (fadeValue < 0)
		RETURN 0
	ENDIF
	
	IF (fadeValue > 255)
		RETURN 255
	ENDIF
	
	RETURN fadeValue

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Default text output
DEBUGONLY PROC MP_Debug_Log_Text_Defaults()

	SET_TEXT_SCALE(DebugLogTextScaleX, DebugLogTextScaleY)
	SET_TEXT_CENTRE(FALSE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the timestamp
DEBUGONLY PROC Display_MP_Debug_Log_Timestamp(INT paramArrayPos, FLOAT paramY, INT paramR, INT paramG, INT paramB, INT paramA)

	MP_Debug_Log_Text_Defaults()
	SET_TEXT_COLOUR(paramR, paramG, paramB, paramA)
	//DISPLAY_TEXT_WITH_NUMBER(DebugLogTimeX, paramY, "NUMBER", g_sMPDebugLogLines[paramArrayPos].timestamp)
	DISPLAY_TEXT(DebugLogTimeX, paramY, GET_TIME_AS_STRING(g_sMPDebugLogLines[paramArrayPos].timestamp)) 

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Player Name (pre-stored string in case the player leaves)
DEBUGONLY PROC Display_MP_Debug_Log_Player(INT paramArrayPos, FLOAT paramY, INT paramR, INT paramG, INT paramB, INT paramA)

	MP_Debug_Log_Text_Defaults()
	SET_TEXT_COLOUR(paramR, paramG, paramB, paramA)
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")		
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_sMPDebugLogLines[paramArrayPos].player)
	END_TEXT_COMMAND_DISPLAY_TEXT(DebugLogPlayerX, paramY)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Mission name and Instance
DEBUGONLY PROC Display_MP_Debug_Log_Mission_And_Instance(INT paramArrayPos, FLOAT paramY, INT paramR, INT paramG, INT paramB, INT paramA, BOOL paramMyMission)

	MP_Debug_Log_Text_Defaults()
	SET_TEXT_COLOUR(paramR, paramG, paramB, paramA)
	
	TEXT_LABEL_15 textToUse = ""
	IF (paramMyMission)
		textToUse = "LOG_MISSINSTMY"
	ELSE
		textToUse = "LOG_MISSINST"
	ENDIF
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(textToUse)		
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(GET_MP_MISSION_NAME(g_sMPDebugLogLines[paramArrayPos].mission))
		ADD_TEXT_COMPONENT_INTEGER(g_sMPDebugLogLines[paramArrayPos].instanceID)
	END_TEXT_COMMAND_DISPLAY_TEXT(DebugLogMissionInstX, paramY)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Text
DEBUGONLY PROC Display_MP_Debug_Log_Text(INT paramArrayPos, FLOAT paramY, INT paramR, INT paramG, INT paramB, INT paramA)

	MP_Debug_Log_Text_Defaults()
	SET_TEXT_COLOUR(paramR, paramG, paramB, paramA)
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")		
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_sMPDebugLogLines[paramArrayPos].extraText)
	END_TEXT_COMMAND_DISPLAY_TEXT(DebugLogTextX, paramY)

ENDPROC
DEBUGONLY PROC Display_MP_Contact_missions_Log()
	// Display background rectangle
	DRAW_RECT_FROM_CORNER(DbgCM_RectX, DbgCM_RectY, DbgCM_RectWidth, DbgCM_RectHeight, 0, 0, 0, DbgCM_RectAlpha)
	
	// Display the log data
	INT thisColumn = 0
	INT thisRow = 0
	int i = 0
	INT r, g, b, a
	
	FLOAT 					theY 				= DbgCM_infoStartY
	g_eFMHeistContactIDs 	thisHeistContactID 	
 	enumCharacterList 		thisCharacter 	
	
	SET_TEXT_SCALE(DbgCM_TextScaleX, DbgCM_TextScaleY)
	SET_TEXT_COLOUR(255, 255, 255, 255)		
	DISPLAY_TEXT_WITH_NUMBER(DbgCM_TitleX, DbgCM_TitleY, "F9_CM_RANK", g_CMsPlayed.cmrmMinRank)
							
	REPEAT MAX_CONTACT_MISSIONS i
		if g_sLocalMPCMs[i].lcmContentIdHash != 0
			SET_TEXT_SCALE(DbgCM_TextScaleX, DbgCM_TextScaleY)
			SET_TEXT_CENTRE(FALSE)
			SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
			SET_TEXT_EDGE(0, 0, 0, 0, 0)
			
			thisHeistContactID 	= g_sLocalMPCMs[i].lcmContact
			thisCharacter 		= Convert_FM_Heist_ContactID_To_CharacterListID(thisHeistContactID)
			//Set colour base on status			
			IF 	g_sLocalMPCMs[i].lcmPlayed 
				SET_TEXT_COLOUR(0,255,0, 255)	//green
			ELSE
				GET_HUD_COLOUR(HUD_COLOUR_WHITE, r,g,b, a)
				if g_sLocalMPCMs[i].lcmRank > GET_PLAYER_RANK(PLAYER_ID()) 
					a = DbgCM_TextAlpha	
				else
					a = 255
				endif
				SET_TEXT_COLOUR(r,g,b, a)	
			ENDIF
			int activeCM = 0
			REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE activeCM
				if GET_HASH_KEY(g_lcacmActiveCMs[activeCM].lcacmMissionIdData.idCloudFilename) = g_sLocalMPCMs[i].lcmContentIdHash
					GET_HUD_COLOUR(HUD_COLOUR_ORANGE, r,g,b, a)		
					SET_TEXT_COLOUR(r,g,b, 255)	
				endif
			ENDREPEAT
			
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("F11_CM_info")
				ADD_TEXT_COMPONENT_INTEGER(i) //~1~.)	
				ADD_TEXT_COMPONENT_INTEGER(g_sLocalMPCMs[i].lcmRootContentIdHash) //ID:~1~	
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_sMPDebugCMs[i].CMname) //Name:~a~		
				//contact: ~a~
				IF (thisCharacter != NO_CHARACTER)
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_sCharacterSheetAll[thisCharacter].label)
		   	 	ENDIF	
				//rank: ~1~
				ADD_TEXT_COMPONENT_INTEGER(g_sLocalMPCMs[i].lcmRank)
			END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(DbgCM_infoStartX + (thisColumn * DbgCM_addColSpaceX), theY + ((thisRow+2) * DbgPlayerListYSpace))		
			theY += DbgCM_AddYSpace
			thisRow++
			IF (thisRow = DbgCM_Rows)
				thisRow = 0
				theY 	= DbgCM_infoStartY
				thisColumn++
			ENDIF
		endif
	ENDREPEAT
	
ENDPROC
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the debug log on-screen
DEBUGONLY PROC Display_MP_Debug_Log()

	// Safe to render?
	IF (g_debugMenuControl.bDebugMenuOnScreen)
		EXIT
	ENDIF
	
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
		EXIT
	ENDIF
	
	IF NOT (IS_SAFE_TO_DRAW_ON_SCREEN())
		EXIT
	ENDIF
	//Also display the contacts mission log
	Display_MP_Contact_missions_Log()	
//	
//	// Display background rectangle
//	DRAW_RECT_FROM_CORNER(DebugLogRectX, DebugLogRectY, DebugLogRectWidth, DebugLogRectHeight, 0, 0, 0, DebugLogRectAlpha)
//	
//	// Display the log data
//	FLOAT theY = DebugLogStartY
//	FLOAT bottomY = DebugLogRectY + DebugLogRectHeight - DebugLogAddY
//	
//	INT tempLoop = 0
//	INT r, g, b, a
//
//	MP_MISSION holdMission	= eNULL_MISSION
//	INT holdInstance		= DEFAULT_MISSION_INSTANCE
//	
//	MP_MISSION thisMission	= eNULL_MISSION
//	INT thisInstance		= DEFAULT_MISSION_INSTANCE
//	
//	BOOL thisIsMyMission	= FALSE
//	MP_MISSION_DATA myActiveMission = GET_PLAYER_MP_MISSION_DATA(PLAYER_ID())
//	
//	BOOL fadedOut[MAX_MP_DEBUG_LOG_LINES]
//	BOOL anyFadedOutEntries = FALSE
//	
//	REPEAT g_currentdebugLogLines tempLoop
//		fadedOut[tempLoop] = FALSE
//	
//		// Is this the player's mission script?
//		thisIsMyMission = FALSE
//		IF (myActiveMission.mdID.idMission	= g_sMPDebugLogLines[tempLoop].mission)
//		AND (myActiveMission.iInstanceId	= g_sMPDebugLogLines[tempLoop].instanceID)
//			thisIsMyMission = TRUE
//		ENDIF
//	
//		// Get this team colour
//		IF (g_sMPDebugLogLines[tempLoop].team = TEAM_INVALID)
//			// ...invalid team - assume host
//			GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r, g, b, a)
////		ELIF (g_sMPDebugLogLines[tempLoop].team = MAX_NUM_TEAMS)
////			// ...illegal team value - assume player left the game
////			GET_HUD_COLOUR(HUD_COLOUR_GREYDARK, r, g, b, a)
//		ELSE
//			// ...proper team
//			GET_TEAM_RGB_COLOUR(g_sMPDebugLogLines[tempLoop].team, r, g, b)
//			
//		ENDIF
//		a = Get_MP_Debug_Log_Line_Alpha(tempLoop, thisIsMyMission)
//		
//		IF (a <= DebugLogFadedOutAlpha)
//			// ...this entry should no longer be displayed, so remove it from the array after this
//			fadedOut[tempLoop]	= TRUE
//			anyFadedOutEntries	= TRUE
//		ENDIF
//		
//		// Get this mission and instance
//		thisMission		= g_sMPDebugLogLines[tempLoop].mission
//		thisInstance	= g_sMPDebugLogLines[tempLoop].instanceID
//		
//		IF NOT (thisMission = holdMission)
//		OR NOT (thisInstance = holdInstance)
//			// Display Mission Name and Instance ID
//			Display_MP_Debug_Log_Mission_And_Instance(tempLoop, theY, 255, 255, 255, a, thisIsMyMission)
//			
//			holdMission		= thisMission
//			holdInstance	= thisInstance
//		
//			// Next screen line
//			theY += DebugLogAddY
//			
//			// Off the bottom?
//			IF (theY > bottomY)
//				EXIT
//			ENDIF
//		ENDIF
//		
//		// Display Timestamp
//		Display_MP_Debug_Log_Timestamp(tempLoop, theY, r, g, b, a)
//		
//		// Display Player Name
//		Display_MP_Debug_Log_Player(tempLoop, theY, r, g, b, a)
//		
//		// Display Text
//		Display_MP_Debug_Log_Text(tempLoop, theY, r, g, b, a)
//		
//		// Next screen line
//		theY += DebugLogAddY
//		
//		// Off the bottom?
//		IF (theY > bottomY)
//			EXIT
//		ENDIF
//	ENDREPEAT
//	
//	// Clean up the array if any messages are now faded out (start at the end and work backwards since any entries after the removed entry get shuffled up the array)
//	IF (anyFadedOutEntries)
//		tempLoop = g_currentdebugLogLines - 1
//		WHILE (tempLoop >= 0)
//			IF (fadedOut[tempLoop])
//				//...remove this entry
//				Remove_Net_Debug_Message_From_Log_At_Position(tempLoop)
//			ENDIF
//			
//			tempLoop--
//		ENDWHILE
//	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the debug log rendering
DEBUGONLY PROC Maintain_Display_MP_Debug_Log()

	// Has the keyboard activation key just been pressed?
	IF (IS_DEBUG_KEY_JUST_PRESSED(KEY_F11, KEYBOARD_MODIFIER_SHIFT, "Network Debug Log On/Off"))
		// If rendering is ON, then switch it OFF
		IF (g_MPDebugLogRendering)
			g_MPDebugLogRendering = FALSE
			
			EXIT
		ENDIF
		
		// Rendering must be OFF
		g_MPDebugLogRendering = TRUE
	ENDIF
	
	IF NOT (g_MPDebugLogRendering)
		EXIT
	ENDIF
	
	Display_MP_Debug_Log()

ENDPROC




// ===========================================================================================================
//      String Functions
// ===========================================================================================================

// PURPOSE:	Cut a string to an appropriate length
//
// INPUT PARAMS:		paramText			The additional text
// RETURN VALUE:		TEXT_LABEL_63		The cutdown string as a text label
DEBUGONLY FUNC TEXT_LABEL_63 Cut_Net_Debug_Log_Text_To_Length(STRING paramText)

	TEXT_LABEL_63 returnString = ""

	IF (IS_STRING_NULL_OR_EMPTY(paramText))
		RETURN returnString
	ENDIF
	
	INT stringLen = GET_LENGTH_OF_LITERAL_STRING(paramText)
	IF (stringLen <= MAX_ADDITIONAL_TEXT_LENGTH)
		returnString = paramText
		RETURN returnString
	ENDIF
	
	// String is too long, need to cut it
	TEXT_LABEL_7 theCutText = "[cut]"
	INT maxLength = MAX_ADDITIONAL_TEXT_LENGTH - GET_LENGTH_OF_LITERAL_STRING(theCutText)
	returnString = GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(paramText, maxLength)
	returnString += theCutText
	
	RETURN returnString

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Cut a playername to an appropriate length
//
// INPUT PARAMS:		paramPlayerID		A player Index
// RETURN VALUE:		TEXT_LABEL_15		The cutdown string as a text label of 15 characters
//
// NOTES:	Assume the playerIndex has been pre-checked for validity and for the player being OK
//			This is just being used to prevent using a TEXT_LABEL greater than 15 for a debug log player name (PS3 allows 15 chars, but Xbox allows 16 chars)
DEBUGONLY FUNC TEXT_LABEL_15 Cut_Net_Debug_Log_PlayerName_To_Length(PLAYER_INDEX paramPlayerID)

	TEXT_LABEL_15 returnString = ""

	STRING thePlayerName = GET_PLAYER_NAME(paramPlayerID)
	
	INT stringLen = GET_LENGTH_OF_LITERAL_STRING(thePlayerName)
	IF (stringLen <= MAX_PLAYER_NAME_LENGTH)
		returnString = thePlayerName
		RETURN returnString
	ENDIF
	
	// String is too long, need to cut it
	returnString = GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(thePlayerName, MAX_PLAYER_NAME_LENGTH)
	
	RETURN returnString

ENDFUNC




// ===========================================================================================================
//      Broadcast Event Functions
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Event: Net Debug Log Message
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventNetDebugLogMessage
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	TIME_DATATYPE				timestamp				// Timestamp for when the message was sent by the client
	MP_MISSION					missionID				// Mission sending player is sending a log message about
	INT							instanceID				// InstanceID of sending player's mission
	TEXT_LABEL_63				extraText				// Additional Text to be displayed
	BOOL						isHostMessage			// TRUE if this message is from the script Host, FALSE if from an individual player
	BOOL						isSystemMessage			// TRUE if this message is a systems message, FALSE if from a normal (ie: mission) script
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To broadcast a new Net Debug Log message
//
// INPUT PARAMS:		paramText				The additional text
//						paramIsHost				TRUE if this is a message from the script Host, FALSE if from a client
//						paramSystemMessage		[DEFAULT=FALSE] TRUE means it is a system message, FALSE means a normal message
//						paramMissionID			[DEFAULT=eNULL_MISSION] Allows a script other than the mission script to post a message about a mission (ie: mission trigger for joining messages)
//						paramInstanceID			[DEFAULT=DEFAULT_MISSION_INSTANCE] Allows a script other than the mission script to post a message about a mission
//
// NOTES:	A system message is like 'joined mission', etc. These will (probably) timeout faster than script messages
DEBUGONLY PROC Broadcast_Net_Debug_Log_Message(STRING paramText, BOOL paramIsHost, BOOL paramSystemMessage = FALSE, MP_MISSION paramMissionID = eNULL_MISSION, INT paramInstanceID = DEFAULT_MISSION_INSTANCE)


	// Keith 23/9/13: We no longer display the debug log screen, so stop sending the debug log messages
	EXIT


	IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
		#IF IS_DEBUG_BUILD
			PRINTSTRING("...KGM MP: Broadcast_Net_Debug_Log_Message(): IGNORING: Network Game is not in progress.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF

	// Gather the additional data
	// ...timestamp
	
	// ...mission details
	MP_MISSION		theMission		= paramMissionID
	INT				theInstanceID	= paramInstanceID
	
	MP_MISSION_DATA activeMission	= GET_PLAYER_MP_MISSION_DATA(PLAYER_ID())
	IF NOT (activeMission.mdID.idMission = eNULL_MISSION)
		// Is this message from the active mission script?
		IF (GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_KEY(GET_MP_MISSION_NAME(activeMission.mdID.idMission)))
			// ...yes
			theMission		= activeMission.mdID.idMission
			theInstanceID	= activeMission.iInstanceId
		ENDIF
	ENDIF
	
	// ...the text (within strict text length boundaries)
	TEXT_LABEL_63 theText = Cut_Net_Debug_Log_Text_To_Length(paramText)

	// Store the Broadcast data
	m_structEventNetDebugLogMessage Event
	
	Event.Details.Type				= SCRIPT_EVENT_NET_DEBUG_LOG_MESSAGE
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.timestamp					= GET_NETWORK_TIME()
	Event.missionID					= theMission
	Event.instanceID				= theInstanceID
	Event.extraText					= theText
	Event.isHostMessage				= paramIsHost
	Event.isSystemMessage			= paramSystemMessage
	
	// Broadcast the event to ALL_PLAYERS (to be acted upon by the mission control server)
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())

	NET_PRINT("...KGM MP: Broadcast_Net_Debug_Log_Message: ")
	IF (paramIsHost)
		NET_PRINT("[HOST] ")
	ENDIF
	NET_PRINT(paramText)
	NET_NL()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a Net Debug Log message
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
DEBUGONLY PROC Process_Event_Net_Debug_Log_Message(INT paramEventID)

	NET_PRINT("...KGM MP: Process_Event_Net_Debug_Log_Message") NET_NL()
	
	// Retrieve the data associated with the event
	m_structEventNetDebugLogMessage theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Net_Debug_Log_Message(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Store the data in an instance of the data struct
	g_structMPDebugLogLineData thisData
	
	// ...timestamp
	thisData.timestamp = theEventData.timestamp
	
	// ...team and player data (influenced by 'is host message')
	IF (theEventData.isHostMessage)
		// ...message is from script host
		thisData.team	= TEAM_INVALID
		thisData.player	= "(HOST)"
	ELSE
		// ...message is from a client
		IF (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
			// ...player is ok
			thisData.player = Cut_Net_Debug_Log_PlayerName_To_Length(theEventData.Details.FromPlayerIndex)
			thisData.team	= GET_PLAYER_TEAM(theEventData.Details.FromPlayerIndex)
//		ELSE
//			// ...player is not ok
//			thisData.player	= "[INVALID]"
//			thisData.team	= MAX_NUM_TEAMS
		ENDIF
	ENDIF
	
	// ...mission and instanceID
	thisData.mission	= theEventData.missionID
	thisData.instanceID	= theEventData.instanceID
	
	// ...additional text
	thisData.extraText	= theEventData.extraText
	
	// ...fadeStart Timestamp (a systems message will start fading out immediately, otherwise this will get updated by the display routine)
	IF (theEventData.isSystemMessage)
		// ...systems message
		thisData.fadeStartTimestamp = GET_NETWORK_TIME()
		thisData.fadeStartTimestampInitialised = TRUE
	ELSE
		// ...message from a normal script (ie: mission script)
		thisData.fadeStartTimestampInitialised = FALSE
	ENDIF
	
	// Add this net log message to the message array
	Add_Net_Debug_Message_To_Log(thisData)
	
	// Output this message to the console log
	NET_PRINT("[LES LOG]  ")
	NET_PRINT(GET_TIME_AS_STRING(thisData.timestamp))
	NET_PRINT(" [")
	NET_PRINT(thisData.player)
	NET_PRINT("] [")
	NET_PRINT(GET_MP_MISSION_NAME(thisData.mission))
	NET_PRINT(" (")
	NET_PRINT_INT(thisData.instanceID)
	NET_PRINT(")] ")
	NET_PRINT(thisData.extraText)
	NET_NL()	
		
ENDPROC




// ===========================================================================================================
//      Initialisation Functions
// ===========================================================================================================

// PURPOSE:	Initialise the local globals used by the debug log
DEBUGONLY PROC Initialise_MP_Debug_Log()

	NET_PRINT("...KGM MP: Initialise_MP_Debug_Log") NET_NL()
	
	// Rendering flag
	g_MPDebugLogRendering = FALSE
	
	// The line data
	g_structMPDebugLogLineData emptyLineData
	
	INT tempLoop = 0
	REPEAT MAX_MP_DEBUG_LOG_LINES tempLoop
		g_sMPDebugLogLines[tempLoop] = emptyLineData
	ENDREPEAT
	
	g_currentdebugLogLines = 0

ENDPROC




// ===========================================================================================================
//      Maintenance Functions
// ===========================================================================================================

// PURPOSE:	Maintain the debug log functionality
DEBUGONLY PROC Maintain_MP_Debug_Log()

	// Maintain the Rendering of the debug log
	Maintain_Display_MP_Debug_Log()

ENDPROC




// ===========================================================================================================
//      External Access Functions
// ===========================================================================================================

// PURPOSE: Allow a script to send a net debug log as a client
//
// INPUT PARAMS:		paramText			The string to send with the message
DEBUGONLY PROC Net_Log(STRING paramText)
	Broadcast_Net_Debug_Log_Message(paramText, FALSE)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Allow a script to send a net debug log as a host
//
// INPUT PARAMS:		paramText			The string to send with the message
DEBUGONLY PROC Net_Log_Host(STRING paramText)
	Broadcast_Net_Debug_Log_Message(paramText, TRUE)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Allow a systems script to send a net debug log for a different mission/instanceID
//
// INPUT PARAMS:		paramText			The string to send with the message
//						paramMissionID		[DEFAULT=eNULL_MISSION] A Mission ID to use to represent where the message came from
//						paramInstanceID		[DEFAULT=DEFAULT_MISSION_INSTANCE] An InstanceID to use to represent where the message came from
//
// NOTES:	Example usage - mission trigger to indicate player has joined/left a mission
DEBUGONLY PROC Net_Log_System(STRING paramText, MP_MISSION paramMissionID = eNULL_MISSION, INT paramInstanceID = DEFAULT_MISSION_INSTANCE)
	Broadcast_Net_Debug_Log_Message(paramText, FALSE, TRUE, paramMissionID, paramInstanceID)
ENDPROC




#ENDIF	// IS_DEBUG_BUILD

