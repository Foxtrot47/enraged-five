#IF IS_DEBUG_BUILD

USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

USING "net_realty_vehicle_garage.sch"

ENUM TEST_VEHICLE_ENUM  //70
	VEH_ADDER,				     
	VEH_ALPHA,						
	VEH_BANSHEE2,
	VEH_BESTIAGTS,
	VEH_BLADE,					  
	VEH_BLISTA,                   
	VEH_BTYPE3,
	VEH_BUCCANEER,				 
	VEH_BUFFALO,				 
	VEH_CARBONIZZARE,     		 
	VEH_CHEETAH,
	VEH_CHINO,					
	VEH_COQUETTE2,
	VEH_COQUETTE3,
	VEH_DOMINATOR,				
	VEH_ELEGY,					
	VEH_ENTITYXF,
	VEH_FELTZER2,
	VEH_FELTZER3,
	VEH_FMJ,	
	VEH_Fusilade,				
	VEH_FUTO,					
	VEH_GAUNTLET,				
	VEH_HOTKNIFE,				
	VEH_JESTER,
	VEH_MAMBA,
	VEH_MASSACRO,
	VEH_NIGHTSHADE,
	VEH_OMNIS,
	VEH_OSIRIS,
	VEH_PENUMBRA,				
	VEH_PFISTER811,
	VEH_PHOENIX,				
	VEH_PICADOR,				
	VEH_PROTOTIPO,
	VEH_RAPIDGT,				
	VEH_REAPER,
	VEH_SABREGT2,					
	VEH_SEVEN70,
	VEH_SHEAVA,
	VEH_SULTANRS,
	VEH_SURANO,					
	VEH_T20,
	VEH_TAMPA,
	VEH_TURISMOR,
	VEH_TROPOS,
	VEH_TYRUS,
	VEH_VERLIERER2,
	VEH_ZENTORNO,					
	VEH_ZTYPE,
	VEH_RUINER,
	VEH_SLAMVAN,
	VEH_STALION,
	VEH_VIRGO,
	VEH_VIGERO,
	VEH_VOODOO,					//56
	VEH_BULLET,
	VEH_INFERNUS,
	VEH_VACCA,
	VEH_VOLTIC,					//60
	VEH_JB700,
	VEH_MANANA,
	VEH_MONROE,
	VEH_NEBULA,
	VEH_PEYOTE,
	VEH_PIGALLE,
	VEH_STINGER,
	VEH_TORNADO,
	VEH_ASEA,
	VEH_ASTEROPE,				//70
	///
	VEH_EMPEROR,
	VEH_FUGITIVE,
	VEH_GLENDALE,
	VEH_INGOT,
	VEH_INTRUDER,
	VEH_PREMIER,
	VEH_PRIMO,
	VEH_REGINA,
	VEH_ROMERO,
	VEH_SCHAFTER2,				//80
	VEH_STANIER,
	VEH_STRATUM,
	VEH_MULE,
	VEH_MULE2,
	VEH_MULE3,
	VEH_SUPERD,
	VEH_SURGE,
	VEH_TAILGATER,
	VEH_WARRENER,
	VEH_WASHINGTON,				//90
	VEH_BALLER,
	VEH_CAVALCADE,
	VEH_DUBSTA,					
	VEH_DILETTANTE,
	VEH_ISSI2,
	VEH_PANTO,
	VEH_PRAIRIE,
	VEH_RHAPSODY,
	VEH_COGNOSCENTI,
	VEH_EXEMPLAR,
	VEH_F620,
	VEH_FELON,
	VEH_WINDSOR						//100
	
ENDENUM

//NEW POSITONS SPECIFIC FOR PLACEMENT IN CAR MEET
PROC GET_CAR_MEET_VEHICLE_POSITON_AND_HEADING(INT index, VECTOR &vPosition, FLOAT &fHeading)
	SWITCH index
	//FIRST CARS NEXT TO THE ENTRANCE ON THE LEFT
	CASE 0	 vPosition = << -2201.87, 1153.3, -23.2549 >>					 fHeading = 130.0			BREAK
	CASE 1	 vPosition = << -2201.87, 1157.8, -23.2549 >>					 fHeading = 130.0			BREAK
	CASE 2	 vPosition = << -2201.87, 1162.3, -23.2549 >>					 fHeading = 130.0			BREAK
	//1ST ROW (RIGHTMOST)
	CASE 3	 vPosition = << -2219.92, 1069.33, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 4	 vPosition = << -2219.92, 1073.83, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 5	 vPosition = << -2219.92, 1078.33, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 6	 vPosition = << -2219.92, 1082.83, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 7	 vPosition = << -2219.92, 1087.33, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 8	 vPosition = << -2219.92, 1091.83, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 9	 vPosition = << -2219.92, 1096.33, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 10	 vPosition = << -2219.92, 1100.83, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 11	 vPosition = << -2219.92, 1105.33, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 12	 vPosition = << -2219.92, 1109.83, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 13	 vPosition = << -2219.92, 1114.33, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 14	 vPosition = << -2219.92, 1118.83, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 15	 vPosition = << -2219.92, 1123.33, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 16	 vPosition = << -2219.92, 1127.83, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 17	 vPosition = << -2219.92, 1132.33, -23.2548 >>					 fHeading = 300.0			BREAK
	CASE 18	 vPosition = << -2219.92, 1136.83, -23.2548 >>					 fHeading = 300.0			BREAK
	//2ND ROW (CLOSE TO ENTRANCE)
	CASE 19	 vPosition = << -2208.12, 1117.83, -23.2549 >>					 fHeading = 60.0			BREAK
	CASE 20	 vPosition = << -2208.12, 1123.13, -23.2549 >>					 fHeading = 60.0			BREAK
	CASE 21	 vPosition = << -2208.12, 1128.43, -23.2549 >>					 fHeading = 60.0			BREAK
	//2ND ROW (far from entrance)
	CASE 22	 vPosition = << -2207.95, 1082.96, -23.2541 >>					 fHeading = 60.0			BREAK
	CASE 23	 vPosition = << -2207.95, 1087.46, -23.2541 >>					 fHeading = 60.0			BREAK
	//3RD ROW (close to entrance)
	CASE 24	 vPosition = << -2199.93, 1114.71, -23.2549 >>					 fHeading = 240.0			BREAK
	CASE 25	 vPosition = << -2199.93, 1119.71, -23.2549 >>					 fHeading = 240.0			BREAK
	CASE 26	 vPosition = << -2199.93, 1124.71, -23.2549 >>					 fHeading = 240.0			BREAK
	CASE 27	 vPosition = << -2199.93, 1129.71, -23.2549 >>					 fHeading = 240.0			BREAK
	//3RD ROW (far from entrance)
	CASE 28	 vPosition = << -2199.5, 1084.13, -23.254 >>					 fHeading = 240.0			BREAK
	CASE 29	 vPosition = << -2199.5, 1088.93, -23.254 >>					 fHeading = 240.0			BREAK
	//4TH ROW (close to entrance)
	CASE 30	 vPosition = << -2187.49, 1114.58, -23.2549 >>					 fHeading = 120.0			BREAK
	CASE 31	 vPosition = << -2187.49, 1119.58, -23.2549 >>					 fHeading = 120.0			BREAK
	CASE 32	 vPosition = << -2187.49, 1124.58, -23.2549 >>					 fHeading = 120.0			BREAK
	CASE 33	 vPosition = << -2187.49, 1129.58, -23.2549 >>					 fHeading = 120.0			BREAK
	CASE 34	 vPosition = << -2187.49, 1134.58, -23.2549 >>					 fHeading = 120.0			BREAK
	//4TH ROW (far from entrance)
	CASE 35	 vPosition = << -2187.39, 1079.23, -23.2549 >>					 fHeading = 120.0			BREAK
	CASE 36	 vPosition = << -2187.39, 1084.53, -23.2549 >>					 fHeading = 120.0			BREAK
	CASE 37	 vPosition = << -2187.39, 1089.83, -23.2549 >>					 fHeading = 120.0			BREAK
	//5TH ROW
	CASE 38	 vPosition = << -2178, 1076, -24.362 >>							 fHeading = 300.0			BREAK
	CASE 39	 vPosition = << -2178, 1081.1, -24.362 >>						 fHeading = 300.0			BREAK
	CASE 40	 vPosition = << -2178, 1086.2, -24.362 >>						 fHeading = 300.0			BREAK
	CASE 41	 vPosition = << -2178, 1091.3, -24.362 >>						 fHeading = 300.0			BREAK
	CASE 42	 vPosition = << -2178, 1096.4, -24.362 >>						 fHeading = 300.0			BREAK
	CASE 43	 vPosition = << -2178, 1101.5, -24.362 >>						 fHeading = 300.0			BREAK
	CASE 44	 vPosition = << -2178, 1106.6, -24.362 >>						 fHeading = 300.0			BREAK
	CASE 45	 vPosition = << -2178, 1111.7, -24.362 >>						 fHeading = 300.0			BREAK
	CASE 46	 vPosition = << -2178, 1116.8, -24.362 >>						 fHeading = 300.0			BREAK
	CASE 47	 vPosition = << -2178, 1121.9, -24.362 >>						 fHeading = 300.0			BREAK
	CASE 48	 vPosition = << -2178, 1127, -24.362 >>							 fHeading = 300.0			BREAK
	CASE 49	 vPosition = << -2178, 1132.1, -24.362 >>						 fHeading = 300.0			BREAK
	CASE 50	 vPosition = << -2178, 1137.2, -24.362 >>						 fHeading = 300.0			BREAK
	//6TH ROW
	CASE 51	 vPosition = << -2165.85, 1087.76, -24.3591 >>					 fHeading = 60.0			BREAK
	CASE 52	 vPosition = << -2165.85, 1092.86, -24.3591 >>					 fHeading = 60.0			BREAK
	CASE 53	 vPosition = << -2165.85, 1097.96, -24.3591 >>					 fHeading = 60.0			BREAK
	CASE 54	 vPosition = << -2165.82, 1112.95, -24.3666 >>					 fHeading = 60.0			BREAK
	CASE 55	 vPosition = << -2165.82, 1118.05, -24.3666 >>					 fHeading = 60.0			BREAK
	//ALONE CAR BETWEEN 6TH and 7TH
	CASE 56	 vPosition = << -2161.7, 1130.67, -24.367 >>					 fHeading = 240.0			BREAK
	//7TH ROW
	CASE 57	 vPosition = << -2157.76, 1084.76, -24.359 >>					 fHeading = 240.0			BREAK
	CASE 58	 vPosition = << -2157.76, 1089.56, -24.359 >>					 fHeading = 240.0			BREAK
	CASE 59	 vPosition = << -2157.76, 1094.36, -24.359 >>					 fHeading = 240.0			BREAK
	CASE 60	 vPosition = << -2157.76, 1099.16, -24.359 >>					 fHeading = 240.0			BREAK
	CASE 61	 vPosition = << -2157.62, 1114.46, -24.3666 >>					 fHeading = 240.0			BREAK
	CASE 62	 vPosition = << -2157.62, 1119.26, -24.3666 >>					 fHeading = 240.0			BREAK
	//8TH ROW (far side)
	CASE 63	 vPosition = << -2145.93, 1065.58, -24.3605 >>					 fHeading = 120.0			BREAK
	CASE 64	 vPosition = << -2145.93, 1070.08, -24.3605 >>					 fHeading = 120.0			BREAK
	CASE 65	 vPosition = << -2145.93, 1074.58, -24.3605 >>					 fHeading = 120.0			BREAK
	CASE 66	 vPosition = << -2145.93, 1079.08, -24.3605 >>					 fHeading = 120.0			BREAK
	CASE 67	 vPosition = << -2145.93, 1083.58, -24.3605 >>					 fHeading = 120.0			BREAK
	CASE 68	 vPosition = << -2145.93, 1088.08, -24.3605 >>					 fHeading = 120.0			BREAK
	CASE 69	 vPosition = << -2145.93, 1092.58, -24.3605 >>					 fHeading = 120.0			BREAK
	CASE 70	 vPosition = << -2145.93, 1097.08, -24.3605 >>					 fHeading = 120.0			BREAK
	//8TH ROW (close side)
	CASE 71	 vPosition =	<< -2145.63, 1128.8, -24.3676 >>				 fHeading = 120.0			BREAK
	CASE 72	 vPosition =	<< -2145.63, 1133.3, -24.3676 >>				 fHeading = 120.0			BREAK
	CASE 73	 vPosition =	<< -2145.63, 1137.8, -24.3676 >>				 fHeading = 120.0			BREAK
	CASE 74	 vPosition =	<< -2145.63, 1142.3, -24.3676 >>				 fHeading = 120.0			BREAK
	CASE 75	 vPosition =	<< -2145.63, 1146.8, -24.3676 >>				 fHeading = 120.0			BREAK
	CASE 76	 vPosition =	<< -2145.63, 1151.3, -24.3676 >>				 fHeading = 120.0			BREAK
	CASE 77	 vPosition =	<< -2145.63, 1155.8, -24.3676 >>				 fHeading = 120.0			BREAK
	//PERPENDICULAR TO THE REST OF THE CARS IN THE NOOK ON CIRCLES
	CASE 78	 vPosition = << -2166.21, 1166.76, -24.3679 >>					 fHeading = 30.0			BREAK
	CASE 79	 vPosition = << -2159.51, 1166.76, -24.3679 >>					 fHeading = 30.0			BREAK
	CASE 80	 vPosition = << -2152.81, 1166.76, -24.3679 >>					 fHeading = 30.0			BREAK
	CASE 81	 vPosition = << -2146.11, 1166.76, -24.3679 >>					 fHeading = 30.0			BREAK				
	//DELIVERY TRUCKS
	CASE 82	 vPosition = << -2193.68, 1100.75, -23.2501 >>					 fHeading = 180.0			BREAK
	CASE 83	 vPosition = << -2162.64, 1077.02, -24.3582 >>					 fHeading = 270.0			BREAK
	CASE 84	 vPosition = << -2172.11, 1156.74, -24.3587 >>					 fHeading = 180.0			BREAK
	ENDSWITCH
ENDPROC

PROC GET_TEST_VEHICLE_DATA(TEST_VEHICLE_ENUM eVehicle, VEHICLE_SETUP_STRUCT_MP &sData)

	sData.VehicleSetup.iLivery = -1	
	SWITCH eVehicle
		// SPEC 1
		CASE VEH_ADDER
			sData.VehicleSetup.eModel = ADDER 
			sData.VehicleSetup.tlPlateText = "ADDER"
			sData.VehicleSetup.iColour1 = 118
			sData.VehicleSetup.iColour2 = 118
			sData.VehicleSetup.iColourExtra1 = 3
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 18
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE VEH_ALPHA
			sData.VehicleSetup.eModel = alpha // ALPHA
			sData.VehicleSetup.tlPlateText = "ALPHA"
			sData.VehicleSetup.iColour1 = 118
			sData.VehicleSetup.iColour2 = 118
			sData.VehicleSetup.iColourExtra1 = 3
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 18
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE VEH_BANSHEE2
			sData.VehicleSetup.eModel = banshee2 // BANSHEE2
			sData.VehicleSetup.tlPlateText = "DR1FT3R"
			sData.VehicleSetup.iColour1 = 69
			sData.VehicleSetup.iColour2 = 96
			sData.VehicleSetup.iColourExtra1 = 107
			sData.VehicleSetup.iColourExtra2 = 154
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 180
			sData.VehicleSetup.iTyreG = 130
			sData.VehicleSetup.iTyreB = 97
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 18
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE VEH_BESTIAGTS
			sData.VehicleSetup.eModel = bestiagts // BESTIAGTS
			sData.VehicleSetup.tlPlateText = "BE4STY"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 40
			sData.VehicleSetup.iColour2 = 57
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
		BREAK
		
		CASE VEH_BLADE
			sData.VehicleSetup.eModel = BLADE // BLADE
			sData.VehicleSetup.tlPlateText = "BLADE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 40
			sData.VehicleSetup.iColour2 = 57
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
		BREAK
		
		CASE VEH_BLISTA
			sData.VehicleSetup.eModel = BLISTA // BLISTA
			sData.VehicleSetup.tlPlateText = "BLISTA"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 40
			sData.VehicleSetup.iColour2 = 57
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
		BREAK
		
		CASE VEH_BUCCANEER
			sData.VehicleSetup.eModel = BUCCANEER // BUCCANEER
			sData.VehicleSetup.tlPlateText = "BCCNR"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 40
			sData.VehicleSetup.iColour2 = 57
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
		BREAK
		
		CASE VEH_BUFFALO
			sData.VehicleSetup.eModel = BUFFALO // BUFFALO
			sData.VehicleSetup.tlPlateText = "BUFFALO"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 40
			sData.VehicleSetup.iColour2 = 57
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
		BREAK
		
		CASE VEH_BTYPE3
			sData.VehicleSetup.eModel = btype3 // ROOSEVELT2
			sData.VehicleSetup.tlPlateText = "L4WLE55"
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 146
			sData.VehicleSetup.iColour2 = 118
			sData.VehicleSetup.iColourExtra1 = 33
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 3
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 2
		BREAK
		CASE VEH_CARBONIZZARE
			sData.VehicleSetup.eModel = CARBONIZZARE // CARBONIZZARE
			sData.VehicleSetup.tlPlateText = "CARBON"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 91
			sData.VehicleSetup.iColour2 = 67
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 35
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreG = 174
			sData.VehicleSetup.iTyreB = 239
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 5
			sData.VehicleSetup.iNeonB = 190
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 6
		BREAK
		CASE VEH_CHEETAH
			sData.VehicleSetup.eModel = CHEETAH // CHEETAH
			sData.VehicleSetup.tlPlateText = "BUZZ3D"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 91
			sData.VehicleSetup.iColour2 = 67
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 35
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreG = 174
			sData.VehicleSetup.iTyreB = 239
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 5
			sData.VehicleSetup.iNeonB = 190
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 6
		BREAK
		
		CASE VEH_CHINO
			sData.VehicleSetup.eModel = CHINO // CHEETAH
			sData.VehicleSetup.tlPlateText = "CHINO"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 91
			sData.VehicleSetup.iColour2 = 67
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 35
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreG = 174
			sData.VehicleSetup.iTyreB = 239
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 5
			sData.VehicleSetup.iNeonB = 190
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 6
		BREAK

		CASE VEH_COQUETTE2
			sData.VehicleSetup.eModel = coquette2 // COQUETTE2
			sData.VehicleSetup.tlPlateText = "T0PL3SS"
			sData.VehicleSetup.iColour1 = 66
			sData.VehicleSetup.iColour2 = 2
			sData.VehicleSetup.iColourExtra1 = 136
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 203
			sData.VehicleSetup.iTyreG = 54
			sData.VehicleSetup.iTyreB = 148
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 43
		BREAK

		CASE VEH_COQUETTE3
			sData.VehicleSetup.eModel = coquette3 // COQUETTE3
			sData.VehicleSetup.tlPlateText = "V1NT4G3"
			sData.VehicleSetup.iColour1 = 5
			sData.VehicleSetup.iColour2 = 117
			sData.VehicleSetup.iColourExtra1 = 112
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 20
			sData.VehicleSetup.iTyreG = 20
			sData.VehicleSetup.iTyreB = 20
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
		BREAK
		
		CASE VEH_DOMINATOR
			sData.VehicleSetup.eModel = DOMINATOR // DOMINATOR
			sData.VehicleSetup.tlPlateText = "DMNTR"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 35
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 15
			sData.VehicleSetup.iNeonG = 3
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_ELEGY
			sData.VehicleSetup.eModel = ELEGY // ELEGY
			sData.VehicleSetup.tlPlateText = "ELEGY"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 35
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 15
			sData.VehicleSetup.iNeonG = 3
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_ENTITYXF
			sData.VehicleSetup.eModel = ENTITYXF // ENTITYXF
			sData.VehicleSetup.tlPlateText = "IML4TE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 35
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 15
			sData.VehicleSetup.iNeonG = 3
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_HORN] = 33
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE VEH_FELTZER2
			sData.VehicleSetup.eModel = Feltzer2 // FELTZER
			sData.VehicleSetup.tlPlateText = "P0W3RFUL"
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 83
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 140
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK

		CASE VEH_FELTZER3
			sData.VehicleSetup.eModel = feltzer3 // FELTZER3
			sData.VehicleSetup.tlPlateText = "M4J3ST1C"
			sData.VehicleSetup.iColour1 = 100
			sData.VehicleSetup.iColour2 = 149
			sData.VehicleSetup.iColourExtra1 = 105
			sData.VehicleSetup.iColourExtra2 = 109
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 6
		BREAK

		CASE VEH_FMJ
			sData.VehicleSetup.eModel = fmj // FMJ
			sData.VehicleSetup.tlPlateText = "C4TCHM3"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 92
			sData.VehicleSetup.iColour2 = 159
			sData.VehicleSetup.iColourExtra1 = 135
			sData.VehicleSetup.iColourExtra2 = 41
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 8
		BREAK
		
		CASE VEH_Fusilade
			sData.VehicleSetup.eModel = Fusilade // Fusilade
			sData.VehicleSetup.tlPlateText = "Fusila"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 92
			sData.VehicleSetup.iColour2 = 159
			sData.VehicleSetup.iColourExtra1 = 135
			sData.VehicleSetup.iColourExtra2 = 41
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 8
		BREAK
		CASE VEH_FUTO
			sData.VehicleSetup.eModel = FUTO // FUTO
			sData.VehicleSetup.tlPlateText = "FUTO"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 92
			sData.VehicleSetup.iColour2 = 159
			sData.VehicleSetup.iColourExtra1 = 135
			sData.VehicleSetup.iColourExtra2 = 41
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
		BREAK
		
		CASE VEH_GAUNTLET
			sData.VehicleSetup.eModel = GAUNTLET // GAUNTLET
			sData.VehicleSetup.tlPlateText = "GAUNTLET"
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 135
			sData.VehicleSetup.iColour2 = 54
			sData.VehicleSetup.iColourExtra1 = 67
			sData.VehicleSetup.iColourExtra2 = 140
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreG = 174
			sData.VehicleSetup.iTyreB = 239
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 140
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_HORN] = 6
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
		BREAK
		
		CASE VEH_HOTKNIFE
			sData.VehicleSetup.eModel = HOTKNIFE // HOTKNIFE
			sData.VehicleSetup.tlPlateText = "HOTHOT"
			sData.VehicleSetup.iColour1 = 74
			sData.VehicleSetup.iColour2 = 154
			sData.VehicleSetup.iColourExtra1 = 74
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 27
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_JESTER
			sData.VehicleSetup.eModel = Jester // JESTER
			sData.VehicleSetup.tlPlateText = "H0TP1NK"
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 135
			sData.VehicleSetup.iColour2 = 54
			sData.VehicleSetup.iColourExtra1 = 67
			sData.VehicleSetup.iColourExtra2 = 140
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreG = 174
			sData.VehicleSetup.iTyreB = 239
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 140
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_HORN] = 6
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
		BREAK

		CASE VEH_MAMBA
			sData.VehicleSetup.eModel = MAMBA // MAMBA
			sData.VehicleSetup.tlPlateText = "0LDBLU3"
			sData.VehicleSetup.iColour1 = 74
			sData.VehicleSetup.iColour2 = 154
			sData.VehicleSetup.iColourExtra1 = 74
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 27
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE VEH_MASSACRO
			sData.VehicleSetup.eModel = Massacro // MASSACRO
			sData.VehicleSetup.tlPlateText = "TR0P1CAL"
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 54
			sData.VehicleSetup.iColour2 = 6
			sData.VehicleSetup.iColourExtra1 = 92
			sData.VehicleSetup.iColourExtra2 = 91
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 252
			sData.VehicleSetup.iTyreG = 238
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 14
		BREAK

		CASE VEH_NIGHTSHADE
			sData.VehicleSetup.eModel = nightshade // NITESHAD
			sData.VehicleSetup.tlPlateText = "DE4DLY"
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 147
			sData.VehicleSetup.iColour2 = 119
			sData.VehicleSetup.iColourExtra1 = 34
			sData.VehicleSetup.iColourExtra2 = 35
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 226
			sData.VehicleSetup.iTyreG = 6
			sData.VehicleSetup.iTyreB = 6
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
		BREAK

		CASE VEH_OMNIS
			sData.VehicleSetup.eModel = omnis // OMNIS
			sData.VehicleSetup.tlPlateText = "0BEYM3"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 36
			sData.VehicleSetup.iColour2 = 92
			sData.VehicleSetup.iColourExtra1 = 89
			sData.VehicleSetup.iColourExtra2 = 55
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 3
			sData.VehicleSetup.iTyreR = 114
			sData.VehicleSetup.iTyreG = 204
			sData.VehicleSetup.iTyreB = 114
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_HORN] = 45
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 13
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE VEH_OSIRIS
			sData.VehicleSetup.eModel = osiris // OSIRIS
			sData.VehicleSetup.tlPlateText = "OH3LL0"
			sData.VehicleSetup.iColour1 = 147
			sData.VehicleSetup.iColour2 = 119
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 70
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 15
			sData.VehicleSetup.iNeonG = 3
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 13
		BREAK

		CASE VEH_PENUMBRA
			sData.VehicleSetup.eModel = PENUMBRA // PENUMBRA
			sData.VehicleSetup.tlPlateText = "PUMBA"
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 50
			sData.VehicleSetup.iColourExtra1 = 53
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK

		CASE VEH_PFISTER811
			sData.VehicleSetup.eModel = pfister811 // PFISTER811
			sData.VehicleSetup.tlPlateText = "M1DL1F3"
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 50
			sData.VehicleSetup.iColourExtra1 = 53
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK
		
		CASE VEH_PHOENIX
			sData.VehicleSetup.eModel = PHOENIX // PHOENIX
			sData.VehicleSetup.tlPlateText = "PHOENIX"
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 50
			sData.VehicleSetup.iColourExtra1 = 53
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK
		
		CASE VEH_PICADOR
			sData.VehicleSetup.eModel = PICADOR // PICADOR
			sData.VehicleSetup.tlPlateText = "PICADOR"
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 50
			sData.VehicleSetup.iColourExtra1 = 53
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK

		CASE VEH_PROTOTIPO
			sData.VehicleSetup.eModel = prototipo // PROTOTIPO
			sData.VehicleSetup.tlPlateText = "FUTUR3"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 146
			sData.VehicleSetup.iColour2 = 35
			sData.VehicleSetup.iColourExtra1 = 145
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 252
			sData.VehicleSetup.iTyreG = 238
			sData.VehicleSetup.iNeonR = 35
			sData.VehicleSetup.iNeonG = 1
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_HORN] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK
		
			CASE VEH_RAPIDGT
			sData.VehicleSetup.eModel = RAPIDGT // RAPIDGT
			sData.VehicleSetup.tlPlateText = "RAPIDGT"
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 50
			sData.VehicleSetup.iColourExtra1 = 53
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK
		
		CASE VEH_REAPER
			sData.VehicleSetup.eModel = reaper // REAPER
			sData.VehicleSetup.tlPlateText = "2FA5T4U"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 71
			sData.VehicleSetup.iColour2 = 2
			sData.VehicleSetup.iColourExtra1 = 107
			sData.VehicleSetup.iColourExtra2 = 145
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 132
			sData.VehicleSetup.iTyreG = 102
			sData.VehicleSetup.iTyreB = 226
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE VEH_SABREGT2
			sData.VehicleSetup.eModel = sabregt2 // SABREGT2
			sData.VehicleSetup.tlPlateText = "GUNZ0UT"
			sData.VehicleSetup.iColour1 = 141
			sData.VehicleSetup.iColour2 = 118
			sData.VehicleSetup.iColourExtra1 = 64
			sData.VehicleSetup.iColourExtra2 = 88
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1
		BREAK

		CASE VEH_SEVEN70
			sData.VehicleSetup.eModel = seven70 // SEVEN70
			sData.VehicleSetup.tlPlateText = "FRU1TY"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 84
			sData.VehicleSetup.iColour2 = 3
			sData.VehicleSetup.iColourExtra2 = 138
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 14
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE VEH_SHEAVA
			sData.VehicleSetup.eModel = sheava // SHEAVA
			sData.VehicleSetup.tlPlateText = "B1GB0Y"
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 29
			sData.VehicleSetup.iColour2 = 34
			sData.VehicleSetup.iColourExtra1 = 35
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 20
			sData.VehicleSetup.iTyreG = 20
			sData.VehicleSetup.iTyreB = 20
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE VEH_SULTANRS
			sData.VehicleSetup.eModel = sultanrs // SULTANRS
			sData.VehicleSetup.tlPlateText = "SN0WFLK3"
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 4
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 15
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 22
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE VEH_SURANO
			sData.VehicleSetup.eModel = SURANO // SURANO
			sData.VehicleSetup.tlPlateText = "SURANO"
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 106
			sData.VehicleSetup.iColour2 = 159
			sData.VehicleSetup.iColourExtra1 = 67
			sData.VehicleSetup.iColourExtra2 = 136
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 3
			sData.VehicleSetup.iTyreR = 180
			sData.VehicleSetup.iTyreG = 130
			sData.VehicleSetup.iTyreB = 97
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 50
			sData.VehicleSetup.iNeonB = 100
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
		BREAK

		CASE VEH_T20
			sData.VehicleSetup.eModel = t20 // T20
			sData.VehicleSetup.tlPlateText = "CAR4M3L"
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 106
			sData.VehicleSetup.iColour2 = 159
			sData.VehicleSetup.iColourExtra1 = 67
			sData.VehicleSetup.iColourExtra2 = 136
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 3
			sData.VehicleSetup.iTyreR = 180
			sData.VehicleSetup.iTyreG = 130
			sData.VehicleSetup.iTyreB = 97
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 50
			sData.VehicleSetup.iNeonB = 100
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
		BREAK

		CASE VEH_TAMPA
			sData.VehicleSetup.eModel = tampa // TAMPA
			sData.VehicleSetup.tlPlateText = "CH4RG3D"
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 12
			sData.VehicleSetup.iColour2 = 120
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 20
			sData.VehicleSetup.iTyreG = 20
			sData.VehicleSetup.iTyreB = 20
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_HORN] = 43
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 30
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE VEH_TURISMOR
			sData.VehicleSetup.eModel = TurismoR // TURISMOR
			sData.VehicleSetup.tlPlateText = "IN4H4ZE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 145
			sData.VehicleSetup.iColour2 = 119
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 140
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 12
		BREAK

		CASE VEH_TROPOS
			sData.VehicleSetup.eModel = tropos // TROPOS
			sData.VehicleSetup.tlPlateText = "1MS0RAD"
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 34
			sData.VehicleSetup.iColour2 = 149
			sData.VehicleSetup.iColourExtra2 = 153
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 140
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
		BREAK

		CASE VEH_TYRUS
			sData.VehicleSetup.eModel = tyrus // TYRUS
			sData.VehicleSetup.tlPlateText = "C1TRUS"
			sData.VehicleSetup.iColour1 = 36
			sData.VehicleSetup.iColour2 = 3
			sData.VehicleSetup.iColourExtra1 = 26
			sData.VehicleSetup.iColourExtra2 = 89
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 252
			sData.VehicleSetup.iTyreG = 238
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 10
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE VEH_VERLIERER2
			sData.VehicleSetup.eModel = verlierer2 // VERLIER
			sData.VehicleSetup.tlPlateText = "PR3C1OUS"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 136
			sData.VehicleSetup.iColour2 = 118
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 20
			sData.VehicleSetup.iTyreG = 20
			sData.VehicleSetup.iTyreB = 20
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
		BREAK

		CASE VEH_ZENTORNO
			sData.VehicleSetup.eModel = zentorno // ZENTORNO
			sData.VehicleSetup.tlPlateText = "W1NN1NG"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 119
			sData.VehicleSetup.iColour2 = 29
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 1
			sData.VehicleSetup.iNeonB = 1
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK

		CASE VEH_ZTYPE
			sData.VehicleSetup.eModel = ztype // ZTYPE
			sData.VehicleSetup.tlPlateText = "B1GMON3Y"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_RUINER
			sData.VehicleSetup.eModel = RUINER // RUINER
			sData.VehicleSetup.tlPlateText = "RUINER"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_SLAMVAN
			sData.VehicleSetup.eModel = SLAMVAN // SLAMVAN
			sData.VehicleSetup.tlPlateText = "SLAMVAN"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_STALION
			sData.VehicleSetup.eModel = STALION // STALION
			sData.VehicleSetup.tlPlateText = "STALION"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		
		CASE VEH_VIRGO
			sData.VehicleSetup.eModel = VIRGO // VIRGO
			sData.VehicleSetup.tlPlateText = "VIRGO"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_VIGERO
			sData.VehicleSetup.eModel = VIGERO // VIGERO
			sData.VehicleSetup.tlPlateText = "VIGERO"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_VOODOO		
			sData.VehicleSetup.eModel = VOODOO // VOODOO
			sData.VehicleSetup.tlPlateText = "VOODOO"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_BULLET
			sData.VehicleSetup.eModel = BULLET // BULLET
			sData.VehicleSetup.tlPlateText = "BULLET"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_INFERNUS
			sData.VehicleSetup.eModel = INFERNUS // INFERNUS
			sData.VehicleSetup.tlPlateText = "INFERNUS"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_VACCA
			sData.VehicleSetup.eModel = VACCA // VACCA
			sData.VehicleSetup.tlPlateText = "VACCA"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_VOLTIC	
			sData.VehicleSetup.eModel = VOLTIC // VOLTIC
			sData.VehicleSetup.tlPlateText = "VOLTIC"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_JB700
			sData.VehicleSetup.eModel = JB700 // JB700
			sData.VehicleSetup.tlPlateText = "JB700"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_MANANA
			sData.VehicleSetup.eModel = MANANA // MANANA
			sData.VehicleSetup.tlPlateText = "MANANA"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_MONROE
			sData.VehicleSetup.eModel = MONROE // MONROE
			sData.VehicleSetup.tlPlateText = "MONROE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_NEBULA
			sData.VehicleSetup.eModel = NEBULA // NEBULA
			sData.VehicleSetup.tlPlateText = "NEBULA"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_PEYOTE
			sData.VehicleSetup.eModel = PEYOTE // PEYOTE
			sData.VehicleSetup.tlPlateText = "PEYOTE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_PIGALLE
			sData.VehicleSetup.eModel = PIGALLE // PIGALLE
			sData.VehicleSetup.tlPlateText = "PIGALLE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_STINGER
			sData.VehicleSetup.eModel = STINGER // STINGER
			sData.VehicleSetup.tlPlateText = "STINGER"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_TORNADO
			sData.VehicleSetup.eModel = TORNADO // TORNADO
			sData.VehicleSetup.tlPlateText = "TORNADO"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_ASEA
			sData.VehicleSetup.eModel = ASEA // ASEA
			sData.VehicleSetup.tlPlateText = "ASEA"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_ASTEROPE	
			sData.VehicleSetup.eModel = ASTEROPE // ASTEROPE
			sData.VehicleSetup.tlPlateText = "ASTEROPE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_EMPEROR
			sData.VehicleSetup.eModel = EMPEROR // EMPEROR
			sData.VehicleSetup.tlPlateText = "EMPEROR"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_FUGITIVE
			sData.VehicleSetup.eModel = FUGITIVE // FUGITIVE
			sData.VehicleSetup.tlPlateText = "FUGITIVE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_GLENDALE
			sData.VehicleSetup.eModel = GLENDALE // GLENDALE
			sData.VehicleSetup.tlPlateText = "GLENDALE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_INGOT
			sData.VehicleSetup.eModel = INGOT // INGOT
			sData.VehicleSetup.tlPlateText = "INGOT"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_INTRUDER
			sData.VehicleSetup.eModel = INTRUDER // INTRUDER
			sData.VehicleSetup.tlPlateText = "INTRUDER"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_PREMIER
			sData.VehicleSetup.eModel = PREMIER // PREMIER
			sData.VehicleSetup.tlPlateText = "PREMIER"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_PRIMO
			sData.VehicleSetup.eModel = PRIMO // PRIMO
			sData.VehicleSetup.tlPlateText = "PRIMO"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_REGINA
			sData.VehicleSetup.eModel = REGINA // REGINA
			sData.VehicleSetup.tlPlateText = "REGINA"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_ROMERO
			sData.VehicleSetup.eModel = ROMERO // ROMERO
			sData.VehicleSetup.tlPlateText = "ROMERO"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_SCHAFTER2
			sData.VehicleSetup.eModel = SCHAFTER2 // SCHAFTER2
			sData.VehicleSetup.tlPlateText = "SCHAFTER2"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_STANIER
			sData.VehicleSetup.eModel = STANIER // STANIER
			sData.VehicleSetup.tlPlateText = "STANIER"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_STRATUM
			sData.VehicleSetup.eModel = STRATUM // STRATUM
			sData.VehicleSetup.tlPlateText = "STRATUM"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE VEH_MULE
			sData.VehicleSetup.eModel = MULE // STRATUM
			sData.VehicleSetup.tlPlateText = "DUMMY"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE VEH_MULE2
			sData.VehicleSetup.eModel = MULE2 // STRATUM
			sData.VehicleSetup.tlPlateText = "DUMMY2"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE VEH_MULE3
			sData.VehicleSetup.eModel = MULE3 // STRATUM
			sData.VehicleSetup.tlPlateText = "DUMMY3"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_SUPERD
			sData.VehicleSetup.eModel = SUPERD // SUPERD
			sData.VehicleSetup.tlPlateText = "SUPERD"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_SURGE
			sData.VehicleSetup.eModel = SURGE // SURGE
			sData.VehicleSetup.tlPlateText = "SURGE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_TAILGATER
			sData.VehicleSetup.eModel = TAILGATER // TAILGATER
			sData.VehicleSetup.tlPlateText = "TAILGATER"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_WARRENER
			sData.VehicleSetup.eModel = WARRENER // WARRENER
			sData.VehicleSetup.tlPlateText = "WARRENER"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_WASHINGTON
			sData.VehicleSetup.eModel = WASHINGTON // WASHINGTON
			sData.VehicleSetup.tlPlateText = "WASHINGTON"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_BALLER
			sData.VehicleSetup.eModel = BALLER // BALLER
			sData.VehicleSetup.tlPlateText = "BALLER"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_CAVALCADE
			sData.VehicleSetup.eModel = CAVALCADE // CAVALCADE
			sData.VehicleSetup.tlPlateText = "CAVALCADE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_DUBSTA
			sData.VehicleSetup.eModel = DUBSTA // DUBSTA
			sData.VehicleSetup.tlPlateText = "DUBSTA"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_DILETTANTE
			sData.VehicleSetup.eModel = DILETTANTE // DILETTANTE
			sData.VehicleSetup.tlPlateText = "DILETTANTE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_ISSI2
			sData.VehicleSetup.eModel = ISSI2 // ISSI2
			sData.VehicleSetup.tlPlateText = "ISSI2"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_PANTO
			sData.VehicleSetup.eModel = PANTO // PANTO
			sData.VehicleSetup.tlPlateText = "PANTO"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_PRAIRIE
			sData.VehicleSetup.eModel = PRAIRIE // PRAIRIE
			sData.VehicleSetup.tlPlateText = "PRAIRIE"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_RHAPSODY
			sData.VehicleSetup.eModel = RHAPSODY // RHAPSODY
			sData.VehicleSetup.tlPlateText = "RHAPSODY"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_COGNOSCENTI
			sData.VehicleSetup.eModel = COGNOSCENTI // COGNOSCENTI
			sData.VehicleSetup.tlPlateText = "COGNOSCENTI"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_EXEMPLAR
			sData.VehicleSetup.eModel = EXEMPLAR // EXEMPLAR
			sData.VehicleSetup.tlPlateText = "EXEMPLAR"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_F620
			sData.VehicleSetup.eModel = F620 // F620
			sData.VehicleSetup.tlPlateText = "F620"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_FELON
			sData.VehicleSetup.eModel = FELON // FELON
			sData.VehicleSetup.tlPlateText = "FELON"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE VEH_WINDSOR
			sData.VehicleSetup.eModel = WINDSOR // WINDSOR
			sData.VehicleSetup.tlPlateText = "WINDSOR"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
			
	ENDSWITCH	
ENDPROC

PROC SET_STARTING_POSITION_TO_PLAYER_POSITION()
	VehDebugScriptManagementData.vGridStartPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	PRINTLN("[NET_VEHICLE_STRESS_TEST_DEBUG] SETTNG STARTING POSITION TO PLAYER POSITION AT ", VehDebugScriptManagementData.vGridStartPosition)
ENDPROC

PROC SET_STARTING_POSITION_TO_AIRPORT()	
	VehDebugScriptManagementData.vGridStartPosition = <<-1314.2097, -3054.0520, 12.9444>>
ENDPROC
PROC SET_STARTING_POSITION_TO_AUTO_SHOP()	
	VehDebugScriptManagementData.vGridStartPosition = <<-1341.579, 138.817, -99.194>>
ENDPROC
PROC SET_STARTING_POSITION_TO_CAR_MEET()
	VehDebugScriptManagementData.vGridStartPosition = << -2095.1, 1146.43, -28.3637 >>
	VehDebugScriptManagementData.fVehicleHeading = 90
ENDPROC
PROC SET_CURRENT_POSITON_TO_CAR_MEET_DRIVE_AREA()
	VehDebugScriptManagementData.vGridStartPosition = << -2095.1, 1146.43, -28.3637 >>
	VehDebugScriptManagementData.vGridCurrentPosition =<< -2095.1, 1146.43, -28.3637 >>
	VehDebugScriptManagementData.fVehicleHeading = 90
ENDPROC
PROC WARP_PLAYER_TO_STRESS_TEST_STARTING_POSITION()
	NET_WARP_TO_COORD(VehDebugScriptManagementData.vGridStartPosition, 90.0)
ENDPROC
PROC SELECT_PRESET()
	SWITCH VehDebugScriptManagementData.ePreset
		CASE VEHICLE_ST_PRESET_AUTO_SHOP
			SET_STARTING_POSITION_TO_AUTO_SHOP()	
			VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates = FALSE
			VehDebugScriptManagementData.iNumberOfCarsToSpawn = 10
			VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn = 0
			VehDebugScriptManagementData.iEdgeLength = 5
			VehDebugScriptManagementData.iNumberOfDifferentModels = 10
			VehDebugScriptManagementData.fVehicleHeading = 90
			VehDebugScriptManagementData.iNumberOfModdedVehicles = 10
			VehDebugScriptManagementData.fXSpacingBetweenCars = 6.25
			VehDebugScriptManagementData.fYSpacingBetweenCars = 4.25
		BREAK
		CASE VEHICLE_ST_PRESET_FILL_CAR_MEET
			VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates = TRUE
			VehDebugScriptManagementData.eStage = VEHICLE_STRESS_TEST_STAGE_SPAWN_NET_VEHICLES
			VehDebugScriptManagementData.iNumberOfCarsToSpawn = 85
			VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn = 30
			VehDebugScriptManagementData.iEdgeLength = 20
			VehDebugScriptManagementData.iNumberOfDifferentModels = 100
			VehDebugScriptManagementData.fVehicleHeading = 90
			VehDebugScriptManagementData.iNumberOfModdedVehicles = 30
		BREAK
		CASE VEHICLE_ST_PRESET_100
			VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates = TRUE
			VehDebugScriptManagementData.eStage = VEHICLE_STRESS_TEST_STAGE_SPAWN_NET_VEHICLES
			VehDebugScriptManagementData.iNumberOfCarsToSpawn = 100
			VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn = 30
			VehDebugScriptManagementData.iEdgeLength = 20
			VehDebugScriptManagementData.iNumberOfDifferentModels = 100
			VehDebugScriptManagementData.fVehicleHeading = 90
			VehDebugScriptManagementData.iNumberOfModdedVehicles = 30
		BREAK
		CASE VEHICLE_ST_PRESET_75
			VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates = TRUE
			VehDebugScriptManagementData.eStage = VEHICLE_STRESS_TEST_STAGE_SPAWN_NET_VEHICLES
			VehDebugScriptManagementData.iNumberOfCarsToSpawn = 75
			VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn = 30
			VehDebugScriptManagementData.iEdgeLength = 20
			VehDebugScriptManagementData.iNumberOfDifferentModels = 100
			VehDebugScriptManagementData.fVehicleHeading = 90
			VehDebugScriptManagementData.iNumberOfModdedVehicles = 30
			
		BREAK
		CASE VEHICLE_ST_PRESET_50
			VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates = TRUE
			VehDebugScriptManagementData.eStage = VEHICLE_STRESS_TEST_STAGE_SPAWN_NET_VEHICLES
			VehDebugScriptManagementData.iNumberOfCarsToSpawn = 50
			VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn = 30
			VehDebugScriptManagementData.iEdgeLength = 20
			VehDebugScriptManagementData.iNumberOfDifferentModels = 100
			VehDebugScriptManagementData.fVehicleHeading = 90
			VehDebugScriptManagementData.iNumberOfModdedVehicles = 30
			
		BREAK
		CASE VEHICLE_ST_PRESET_25
			VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates = TRUE
			VehDebugScriptManagementData.eStage = VEHICLE_STRESS_TEST_STAGE_SPAWN_NET_VEHICLES
			VehDebugScriptManagementData.iNumberOfCarsToSpawn = 25
			VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn = 25
			VehDebugScriptManagementData.iEdgeLength = 20
			VehDebugScriptManagementData.iNumberOfDifferentModels = 100
			VehDebugScriptManagementData.fVehicleHeading = 90
			VehDebugScriptManagementData.iNumberOfModdedVehicles = 25
			
		BREAK
		CASE VEHICLE_ST_PRESET_12
			VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates = TRUE
			VehDebugScriptManagementData.eStage = VEHICLE_STRESS_TEST_STAGE_SPAWN_NET_VEHICLES
			VehDebugScriptManagementData.iNumberOfCarsToSpawn = 12
			VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn = 12
			VehDebugScriptManagementData.iEdgeLength = 20
			VehDebugScriptManagementData.iNumberOfDifferentModels = 100
			VehDebugScriptManagementData.fVehicleHeading = 90
			VehDebugScriptManagementData.iNumberOfModdedVehicles = 12
			
		BREAK
		DEFAULT
			PRINTLN("[NET_VEHICLE_STRESS_TEST_DEBUG.sch] Running test without a preset.")
		BREAK
	ENDSWITCH
	IF VehDebugScriptManagementData.iAleadyReservedNetworkVehicles < VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn
		PRINTLN("[NET_VEHICLE_STRESS_TEST_DEBUG] RESERVING NETWORK VEHICLES : ", VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn - VehDebugScriptManagementData.iAleadyReservedNetworkVehicles)
		RESERVE_NETWORK_MISSION_VEHICLES(VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn - VehDebugScriptManagementData.iAleadyReservedNetworkVehicles)
		VehDebugScriptManagementData.iAleadyReservedNetworkVehicles = VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn
		PRINTLN("[NET_VEHICLE_STRESS_TEST_DEBUG] CURRENTLY RESERVED NETWORK VEHICLES:", VehDebugScriptManagementData.iAleadyReservedNetworkVehicles)
	ENDIF
ENDPROC

PROC OUTPUT_VEH_DATA()

	INT i	
	STRING sPath = "X:/gta5/titleupdate/dev_ng/"
	TEXT_LABEL_63 tlFile = "vehicle_ST_Data.txt"
	
	OPEN_NAMED_DEBUG_FILE(sPath, tlFile)			
	REPEAT VehDebugScriptManagementData.iNumberOfCarsToSpawn i
		IF i < VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn 
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE ", sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" vPosition = ", sPath, tlFile)
			SAVE_VECTOR_TO_NAMED_DEBUG_FILE(GET_ENTITY_COORDS(NET_TO_ENT(VehDebugScriptManagementData.niVehicles[i])),  sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" fHeading = ", sPath, tlFile)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(GET_ENTITY_HEADING(NET_TO_ENT(VehDebugScriptManagementData.niVehicles[i])), sPath, tlFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE ", sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" vPosition = ", sPath, tlFile)
			SAVE_VECTOR_TO_NAMED_DEBUG_FILE(GET_ENTITY_COORDS(VehDebugScriptManagementData.viVehicles[i-VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn]),  sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" fHeading = ", sPath, tlFile)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(GET_ENTITY_HEADING(VehDebugScriptManagementData.viVehicles[i-VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn]), sPath, tlFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		ENDIF
	ENDREPEAT
	CLOSE_DEBUG_FILE()
	
ENDPROC

PROC CLEAR_OUTPUT_FILE()	
	STRING sPath = "X:/gta5/titleupdate/dev_ng/"
	TEXT_LABEL_63 tlFile = "vehicle_ST_Data.txt"
	
	CLEAR_NAMED_DEBUG_FILE(sPath, tlFile)		
ENDPROC

PROC CLEANUP_CARS()
	INT i = 0
	VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates = FALSE
	VehDebugScriptManagementData.ePreset = VEHICLE_ST_PRESET_INVALID
	REPEAT ciMAX_NUMBER_OF_VEHICLES i		
			IF DOES_ENTITY_EXIST(VehDebugScriptManagementData.viVehicles[i])				
				DELETE_VEHICLE(VehDebugScriptManagementData.viVehicles[i])
			ENDIF
	ENDREPEAT
	 i = 0
	REPEAT ciMAX_NUMBER_OF_NET_VEHICLES i		
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(VehDebugScriptManagementData.niVehicles[i])
				DELETE_NET_ID(VehDebugScriptManagementData.niVehicles[i])			
			ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL NET_SPAWN_CARS()
	
	INT i = 0
	VEHICLE_SETUP_STRUCT_MP sData	
	VehDebugScriptManagementData.vGridCurrentPosition = VehDebugScriptManagementData.vGridStartPosition  						
	REPEAT VehDebugScriptManagementData.iNumberOfCarsToSpawn i			
		GET_TEST_VEHICLE_DATA(INT_TO_ENUM(TEST_VEHICLE_ENUM, (i%VehDebugScriptManagementData.iNumberOfDifferentModels)), sData)
		//FIRST SPAWN NETWORKED VEHICLES
		IF i < VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn 
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(VehDebugScriptManagementData.niVehicles[i])
				IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)
					//FOR THIS ONE PRESET GET CAR MEET COORDS
					IF VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates
						IF NOT (i>85)
							GET_CAR_MEET_VEHICLE_POSITON_AND_HEADING(i, VehDebugScriptManagementData.vGridCurrentPosition, VehDebugScriptManagementData.fVehicleHeading)
						ELSE
							SET_CURRENT_POSITON_TO_CAR_MEET_DRIVE_AREA()
							VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates = FALSE
						ENDIF
					ENDIF
					CREATE_NET_VEHICLE(VehDebugScriptManagementData.niVehicles[i], sData.VehicleSetup.eModel, VehDebugScriptManagementData.vGridCurrentPosition, VehDebugScriptManagementData.fVehicleHeading)
					IF i<VehDebugScriptManagementData.iNumberOfModdedVehicles
						SET_VEHICLE_SETUP(NET_TO_VEH(VehDebugScriptManagementData.niVehicles[i]), sData.VehicleSetup)
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)
					PRINTLN("[NET_VEHICLE_STRESS_TEST_DEBUG] CREATING NETWORKED VEHICLE ID:",i," AT vPosition = ", VehDebugScriptManagementData.vGridCurrentPosition , "     fHeading = ", VehDebugScriptManagementData.fVehicleHeading)
					IF NOT VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates
						VehDebugScriptManagementData.vGridCurrentPosition.y += VehDebugScriptManagementData.fYSpacingBetweenCars				
						IF ((i+1)%VehDebugScriptManagementData.iEdgeLength) = 0 
							VehDebugScriptManagementData.vGridCurrentPosition.x += VehDebugScriptManagementData.fXSpacingBetweenCars
							VehDebugScriptManagementData.vGridCurrentPosition.y = VehDebugScriptManagementData.vGridStartPosition.y
						ENDIF
					ENDIF
					RETURN FALSE //TO MAKE SURE WE CREATE ONE MODEL PER FRAME
				ELSE
					RETURN FALSE // BAIL OUT IF MODEL IS NOT LOADED
				ENDIF
			ELSE //IF THE VEHICLE EXISTS ONLY ADJUST THE POSITION FOR THE NEXT VEHICLE SPAWN
				IF NOT VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates
					VehDebugScriptManagementData.vGridCurrentPosition.y += VehDebugScriptManagementData.fYSpacingBetweenCars			
					IF ((i+1)%VehDebugScriptManagementData.iEdgeLength) = 0 
						VehDebugScriptManagementData.vGridCurrentPosition.x += VehDebugScriptManagementData.fXSpacingBetweenCars
						VehDebugScriptManagementData.vGridCurrentPosition.y = VehDebugScriptManagementData.vGridStartPosition.y
					ENDIF
				ENDIF
			ENDIF
		ELSE //AFTER NETWORKED VEHS ARE SPAWNED , SPAWN LOCAL ONES
			IF NOT DOES_ENTITY_EXIST(VehDebugScriptManagementData.viVehicles[i-VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn])	
				IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)
					IF VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates
						IF NOT (i>85)
								GET_CAR_MEET_VEHICLE_POSITON_AND_HEADING(i, VehDebugScriptManagementData.vGridCurrentPosition, VehDebugScriptManagementData.fVehicleHeading)
							ELSE
								SET_CURRENT_POSITON_TO_CAR_MEET_DRIVE_AREA()
								VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates = FALSE
							ENDIF
						ENDIF
					VehDebugScriptManagementData.viVehicles[i-VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn] = CREATE_VEHICLE(sData.VehicleSetup.eModel, VehDebugScriptManagementData.vGridCurrentPosition, VehDebugScriptManagementData.fVehicleHeading, FALSE)
					IF i<VehDebugScriptManagementData.iNumberOfModdedVehicles
						SET_VEHICLE_SETUP(VehDebugScriptManagementData.viVehicles[i-VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn], sData.VehicleSetup)
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)
					PRINTLN("[NET_VEHICLE_STRESS_TEST_DEBUG] CREATING LOCAL VEHICLE ID:",i," (",i-VehDebugScriptManagementData.iNumberOfNetworkCarsToSpawn,") AT vPosition = ", VehDebugScriptManagementData.vGridCurrentPosition , "     fHeading = ", VehDebugScriptManagementData.fVehicleHeading)
					IF NOT VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates
						VehDebugScriptManagementData.vGridCurrentPosition.y += VehDebugScriptManagementData.fYSpacingBetweenCars
						IF ((i+1)%VehDebugScriptManagementData.iEdgeLength) = 0 
							VehDebugScriptManagementData.vGridCurrentPosition.x += VehDebugScriptManagementData.fXSpacingBetweenCars
							VehDebugScriptManagementData.vGridCurrentPosition.y = VehDebugScriptManagementData.vGridStartPosition.y
						ENDIF
					ENDIF
					RETURN FALSE //TO MAKE SURE WE CREATE ONE MODEL PER FRAME
				ELSE
					RETURN FALSE
				ENDIF
			ELSE  //IF THE VEHICLE EXISTS ONLY ADJUST THE POSITION FOR THE NEXT VEHICLE SPAWN
				IF NOT VehDebugScriptManagementData.bShouldVehiclesUseCarMeetSpecicCoortinates
					VehDebugScriptManagementData.vGridCurrentPosition.y += VehDebugScriptManagementData.fYSpacingBetweenCars			
					IF ((i+1)%VehDebugScriptManagementData.iEdgeLength) = 0 
						VehDebugScriptManagementData.vGridCurrentPosition.x += VehDebugScriptManagementData.fXSpacingBetweenCars
						VehDebugScriptManagementData.vGridCurrentPosition.y = VehDebugScriptManagementData.vGridStartPosition.y
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE //ALL CARS SPAWNED
	
ENDFUNC
FUNC BOOL SPAWN_AUTO_SHOP_VEHICLES()
	
	VEHICLE_SETUP_STRUCT_MP sData	
	
	//SPOTLIGHT VEHICLE
	VECTOR vPosition1 = <<-1351.3293, 155.5383, -98.1943>>
	FLOAT fHeading1 = 0
	//LIFT VEHICLE
	VECTOR vPosition2 = <<-1364.2360, 160.4908, -98.4943>>
	FLOAT fHeading2 = 270
//	//2nd LIFT VEHICLE
//	VECTOR vPosition3 = <<-1364.5400, 155.3665, -98.1943>>
//	FLOAT fHeading3 = 270
	PRINTLN("[NET_VEHICLE_STRESS_TEST_DEBUG] SPAWN_AUTO_SHOP_VEHICLES")
	IF NOT DOES_ENTITY_EXIST(VehDebugScriptManagementData.viVehicles[10])
		GET_TEST_VEHICLE_DATA( VEH_ALPHA, sData)		
		IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)
			VehDebugScriptManagementData.viVehicles[10] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vPosition1, fHeading1, FALSE)
			SET_VEHICLE_SETUP(VehDebugScriptManagementData.viVehicles[10], sData.VehicleSetup)
			SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)			
			RETURN FALSE //TO MAKE SURE WE CREATE ONE MODEL PER FRAME
		ENDIF
	ELIF NOT DOES_ENTITY_EXIST(VehDebugScriptManagementData.viVehicles[11])	
		GET_TEST_VEHICLE_DATA( VEH_ADDER, sData)	
		IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)
			VehDebugScriptManagementData.viVehicles[11] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vPosition2, fHeading2, FALSE)
			SET_VEHICLE_SETUP(VehDebugScriptManagementData.viVehicles[11], sData.VehicleSetup)
			SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)			
			RETURN FALSE //TO MAKE SURE WE CREATE ONE MODEL PER FRAME
		ENDIF
//	ELIF NOT DOES_ENTITY_EXIST(VehDebugScriptManagementData.viVehicles[12])	
//		IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)
//			VehDebugScriptManagementData.viVehicles[12] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vPosition3, fHeading3, FALSE)
//			SET_VEHICLE_SETUP(VehDebugScriptManagementData.viVehicles[12], sData.VehicleSetup)
//			SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)			
//			RETURN FALSE //TO MAKE SURE WE CREATE ONE MODEL PER FRAME
//		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL PERFORM_VEHICLE_STRESS_TEST()	
	
	SWITCH VehDebugScriptManagementData.eStage
		CASE VEHICLE_STRESS_TEST_STAGE_INIT // HAPPENS ONLY FIRST FRAME
			SELECT_PRESET()
			VehDebugScriptManagementData.eStage = VEHICLE_STRESS_TEST_STAGE_SPAWN_NET_VEHICLES
			RETURN FALSE
		BREAK
		CASE VEHICLE_STRESS_TEST_STAGE_SPAWN_NET_VEHICLES  // HAPPENS UNTIL ALL CARS ARE SPAWNED
			IF NET_SPAWN_CARS()		
				IF NOT VehDebugScriptManagementData.bShouldVehiclesSpawnInAutoShop
					RETURN TRUE
				ELSE
					VehDebugScriptManagementData.eStage = VEHICLE_STRESS_TEST_STAGE_SPAWN_AUTO_SHOP_VEHICLES
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		CASE VEHICLE_STRESS_TEST_STAGE_SPAWN_AUTO_SHOP_VEHICLES
			RETURN SPAWN_AUTO_SHOP_VEHICLES()
		BREAK
		CASE VEHICLE_STRESS_TEST_STAGE_CLEANUP
			 CLEANUP_CARS()			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
#ENDIF
