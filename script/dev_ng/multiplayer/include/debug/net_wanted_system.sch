// _____________________________________________________________________________________
//
//	Game: MP Wanted System Prototype
//  Script: net_wanted_system.sch
//  Author: James Adwick 
//	Description: Prototype system for A.Scotland
//				NB: If required we should split this into well defined headers.
//					All put in this one header to make it easy to exclude from project
// ______________________________________________________________________________________

using "globals.sch"
using "net_include.sch"

using "net_blips.sch"
using "net_big_message.sch"
using "screens_header.sch"

#IF IS_DEBUG_BUILD
#IF FEATURE_WANTED_SYSTEM

TWEAK_FLOAT WS_TWEAK_CRIME_RADIUS	40.0
TWEAK_FLOAT WS_TWEAK_CRIME_RANGE	200.0
TWEAK_INT	WS_TWEAK_CRIME_TIMEOUT	30

TWEAK_FLOAT WS_TWEAK_CRIME_LOST_RADIUS_START	10.0
TWEAK_FLOAT WS_TWEAK_CRIME_LOST_RADIUS_END		80.0
TWEAK_INT 	WS_TWEAK_CRIME_LOST_TIME			20

#IF IS_DEBUG_BUILD
STRUCT WANTED_SYSTEM_LAUNCH_EVENT_STRUCT
	STRUCT_EVENT_COMMON_DETAILS details
	BOOL bLaunch
	BOOL bKill
ENDSTRUCT

STRUCT WANTED_SYSTEM_UPDATE_ROLES
	STRUCT_EVENT_COMMON_DETAILS details
	INT iNewRoles
ENDSTRUCT

STRUCT WANTED_SYSTEM_SYNC_WIDGETS
	STRUCT_EVENT_COMMON_DETAILS details
	FLOAT fCrimeRadius
	FLOAT fCrimeRange
	INT	iCrimeTimeout
	FLOAT fCrimeLostRadiusStart
	FLOAT fCrimeLostRadiusEnd
	INT iCrimeLostTime
ENDSTRUCT
#ENDIF

STRUCT WANTED_SYSTEM_CRIME_STRUCT
	STRUCT_EVENT_COMMON_DETAILS details
	VECTOR vPosition
	TIME_DATATYPE timeOfCrime
ENDSTRUCT

STRUCT WANTED_SYSTEM_CRIME_LOST
	STRUCT_EVENT_COMMON_DETAILS details
	INT iCrime
	TIME_DATATYPE timeCrimeLost
ENDSTRUCT

STRUCT WANTED_SYSTEM_CRIME_TAGGED
	STRUCT_EVENT_COMMON_DETAILS details
	INT iTaggedCrime
ENDSTRUCT

#IF IS_DEBUG_BUILD
FUNC STRING WANTED_SYSTEM_GET_GLOBAL_STATE_DEBUG_STRING(WANTED_SYSTEM_GLOBAL_STATE eState)
	SWITCH eState
		CASE WS_GLOBAL_INITIALISATION	RETURN "WS_GLOBAL_INITIALISATION"
		CASE WS_GLOBAL_IDLE				RETURN "WS_GLOBAL_IDLE			"
		CASE WS_GLOBAL_SETUP			RETURN "WS_GLOBAL_SETUP			"
		CASE WS_GLOBAL_RUNNING			RETURN "WS_GLOBAL_RUNNING		"
		CASE WS_GLOBAL_CLEANUP			RETURN "WS_GLOBAL_CLEANUP		"
	ENDSWITCH

	RETURN "UNKNOWN"
ENDFUNC

FUNC STRING WANTED_SYSTEM_GET_CRIME_STATE_DEBUG_STRING(WANTED_SYSTEM_CRIME_STATE eState)
	SWITCH eState
		CASE WS_CRIME_COMMITTED			RETURN "WS_CRIME_COMMITTED		"
		CASE WS_CRIME_LOST				RETURN "WS_CRIME_LOST			"
		CASE WS_CRIME_SPOTTED_LOCAL		RETURN "WS_CRIME_SPOTTED_LOCAL	"
		CASE WS_CRIME_TAGGED			RETURN "WS_CRIME_TAGGED			"
	ENDSWITCH

	RETURN "UNKNOWN"
ENDFUNC

PROC WANTED_SYSTEM_SET_UP_PLAYER_ROLE_OPTIONS(PLAYER_INDEX playerID, WANTED_SYSTEM_DATA &sWantedSystemData)
	
	START_WIDGET_GROUP(GET_PLAYER_NAME(playerID))
		ADD_WIDGET_BOOL("Cop?", sWantedSystemData.bCops[NATIVE_TO_INT(playerID)])
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC WANTED_SYSTEM_POPULATE_PLAYER_WIDGETS(WANTED_SYSTEM_DATA &sWantedSystemData)
	
	sWantedSystemData.widgetGroupSub = START_WIDGET_GROUP("Override Cops / Criminals")
	INT i
	PLAYER_INDEX playerID
	REPEAT NUM_NETWORK_PLAYERS i
		playerID = INT_TO_PLAYERINDEX(i)
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			WANTED_SYSTEM_SET_UP_PLAYER_ROLE_OPTIONS(playerID, sWantedSystemData)
		ENDIF	
	ENDREPEAT
	
	ADD_WIDGET_BOOL("Apply Roles", sWantedSystemData.bApplyRoles)
	
	STOP_WIDGET_GROUP()
		
ENDPROC

PROC WANTED_SYSTEM_REFRESH_PLAYER_WIDGETS(WANTED_SYSTEM_DATA &sWantedSystemData)
	IF DOES_WIDGET_GROUP_EXIST(sWantedSystemData.widgetGroupSub)
		DELETE_WIDGET_GROUP(sWantedSystemData.widgetGroupSub)
	ENDIF
	
	SET_CURRENT_WIDGET_GROUP(sWantedSystemData.widgetGroup)
		WANTED_SYSTEM_POPULATE_PLAYER_WIDGETS(sWantedSystemData)
	CLEAR_CURRENT_WIDGET_GROUP(sWantedSystemData.widgetGroup)
ENDPROC

PROC WANTED_SYSTEM_CREATE_WIDGETS(WANTED_SYSTEM_DATA &sWantedSystemData)
	
	#IF IS_DEBUG_BUILD
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_EnableWantedSystem")
		EXIT
	ENDIF
	#ENDIF
	
	WIDGET_GROUP_ID topLevel = GET_ID_OF_TOP_LEVEL_WIDGET_GROUP()
	SET_CURRENT_WIDGET_GROUP(topLevel)
	sWantedSystemData.widgetGroup = START_WIDGET_GROUP("1. Online Wanted System [A.Scotland]")
		ADD_WIDGET_BOOL("Activate Wanted System:", sWantedSystemData.bBeginSystem)
		ADD_WIDGET_BOOL("DeActivate Wanted System:", sWantedSystemData.bEndSystem)
		ADD_WIDGET_BOOL("Trigger a Crime:", sWantedSystemData.bTriggerCrime)
		ADD_WIDGET_BOOL("Allow AI Wanted Level:", sWantedSystemData.bDisableWantedLevels)
		ADD_WIDGET_BOOL("Sync Widget settings with all players", sWantedSystemData.bSyncWidgets)
		
		WANTED_SYSTEM_POPULATE_PLAYER_WIDGETS(sWantedSystemData)
		
		START_WIDGET_GROUP("Crime Committed")
			ADD_WIDGET_FLOAT_SLIDER("Radius of Crime Blip", WS_TWEAK_CRIME_RADIUS, 0.0, 500.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Range Cops will see Crime", WS_TWEAK_CRIME_RANGE, 0.0, 2500.0, 0.5)
			ADD_WIDGET_INT_SLIDER("Time Crime Committed radius will appear (s)", WS_TWEAK_CRIME_TIMEOUT, 0, 300, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Crime Being Lost")
			ADD_WIDGET_FLOAT_SLIDER("Radius of Loss Blip START", WS_TWEAK_CRIME_LOST_RADIUS_START, 0.0, 500.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Radius of Loss Blip END", WS_TWEAK_CRIME_LOST_RADIUS_END, 0.0, 2500.0, 0.5)
			ADD_WIDGET_INT_SLIDER("Time until radius is lost (s)", WS_TWEAK_CRIME_LOST_TIME, 0, 300, 1)
		STOP_WIDGET_GROUP()

	STOP_WIDGET_GROUP()
	CLEAR_CURRENT_WIDGET_GROUP(topLevel)
ENDPROC

PROC WANTED_SYSTEM_BROADCAST_LAUNCH_EVENT(BOOL bLaunch, BOOL bKill)

	WANTED_SYSTEM_LAUNCH_EVENT_STRUCT Event
	Event.Details.Type = SCRIPT_EVENT_DEBUG_LAUNCH_WANTED_SYSTEM
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.bLaunch = bLaunch
	Event.bKill = bKill
	
	INT iBS = ALL_PLAYERS(TRUE, FALSE)
	IF NOT (iBS = 0)
		PRINTLN("[WS] WANTED_SYSTEM_BROADCAST_LAUNCH_EVENT")
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iBS)
	ELSE
		PRINTLN("WANTED_SYSTEM_BROADCAST_LAUNCH_EVENT - playerflags = 0 so not broadcasting")
	ENDIF
ENDPROC

PROC WANTED_SYSTEM_PROCESS_LAUNCH_EVENT(INT iEventID)

	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF

	WANTED_SYSTEM_LAUNCH_EVENT_STRUCT Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		
		IF Event.bLaunch
			GlobalServerBD.sServerWantedSystem.bWSActive = TRUE
		ELIF Event.bKill
			GlobalServerBD.sServerWantedSystem.bWSActive = FALSE
		ENDIF		
	ENDIF
	
ENDPROC


PROC WANTED_SYSTEM_BROADCAST_ROLE_UPDATE(INT iBitSet)
	
	WANTED_SYSTEM_UPDATE_ROLES Event
	Event.Details.Type = SCRIPT_EVENT_DEBUG_UPDATE_ROLES
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.iNewRoles = iBitSet
	
	INT iBS = ALL_PLAYERS(TRUE, FALSE)
	IF NOT (iBS = 0)
		PRINTLN("[WS] WANTED_SYSTEM_BROADCAST_ROLE_UPDATE")
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iBS)
	ELSE
		PRINTLN("WANTED_SYSTEM_BROADCAST_ROLE_UPDATE - playerflags = 0 so not broadcasting")
	ENDIF
	
ENDPROC


PROC WANTED_SYSTEM_BROADCAST_SYNC_WIDGETS()

	WANTED_SYSTEM_SYNC_WIDGETS Event
	Event.Details.Type = SCRIPT_EVENT_DEBUG_SYNC_WIDGETS
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.fCrimeRadius = WS_TWEAK_CRIME_RADIUS
	Event.fCrimeRange = WS_TWEAK_CRIME_RANGE
	Event.iCrimeTimeout = WS_TWEAK_CRIME_TIMEOUT
	Event.fCrimeLostRadiusStart = WS_TWEAK_CRIME_LOST_RADIUS_START
	Event.fCrimeLostRadiusEnd = WS_TWEAK_CRIME_LOST_RADIUS_END
	Event.iCrimeLostTime = WS_TWEAK_CRIME_LOST_TIME
		
	INT iBS = ALL_PLAYERS(TRUE, FALSE)
	IF NOT (iBS = 0)
		PRINTLN("[WS] WANTED_SYSTEM_BROADCAST_ROLE_UPDATE")
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iBS)
	ELSE
		PRINTLN("WANTED_SYSTEM_BROADCAST_ROLE_UPDATE - playerflags = 0 so not broadcasting")
	ENDIF

ENDPROC

PROC WANTED_SYSTEM_PROCESS_SYNC_WIDGETS(INT iEventID)

	WANTED_SYSTEM_SYNC_WIDGETS Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		WS_TWEAK_CRIME_RADIUS = Event.fCrimeRadius
		WS_TWEAK_CRIME_RANGE = Event.fCrimeRange
		WS_TWEAK_CRIME_TIMEOUT = Event.iCrimeTimeout
		WS_TWEAK_CRIME_LOST_RADIUS_START = Event.fCrimeLostRadiusStart
		WS_TWEAK_CRIME_LOST_RADIUS_END = Event.fCrimeLostRadiusEnd
		WS_TWEAK_CRIME_LOST_TIME = Event.iCrimeLostTime
	ENDIF

ENDPROC

#ENDIF

// ****************** STATE SET / GET FUNCTIONS **************


FUNC WANTED_SYSTEM_GLOBAL_STATE WANTED_SYSTEM_GET_CLIENT_STATE()
	RETURN GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sClientWantedSystem.eClientWSState
ENDFUNC

PROC WANTED_SYSTEM_SET_CLIENT_STATE(WANTED_SYSTEM_GLOBAL_STATE eNewState)
	PRINTLN("[WS] WANTED_SYSTEM_SET_CLIENT_STATE - OldState = ", WANTED_SYSTEM_GET_GLOBAL_STATE_DEBUG_STRING(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sClientWantedSystem.eClientWSState), ", NewState = ", WANTED_SYSTEM_GET_GLOBAL_STATE_DEBUG_STRING(eNewState))
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sClientWantedSystem.eClientWSState = eNewState
ENDPROC

FUNC WANTED_SYSTEM_GLOBAL_STATE WANTED_SYSTEM_GET_SERVER_STATE()
	RETURN GlobalServerBD.sServerWantedSystem.eServerWSState
ENDFUNC

FUNC BOOL WANTED_SYSTEM_IS_CRIME_GLOBALLY_VISIBLE(INT iCrime)
	RETURN IS_BIT_SET(GlobalServerBD.sServerWantedSystem.iBSCrimesVisible, iCrime)
ENDFUNC

PROC WANTED_SYSTEM_SET_SERVER_STATE(WANTED_SYSTEM_GLOBAL_STATE eNewState)

	// Assert if we are not the host
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SCRIPT_ASSERT("[WS] WANTED_SYSTEM_SET_SERVER_STATE - none host is calling set state for server")
		EXIT
	ENDIF

	PRINTLN("[WS][SERVER] WANTED_SYSTEM_SET_SERVER_STATE - OldState = ", WANTED_SYSTEM_GET_GLOBAL_STATE_DEBUG_STRING(GlobalServerBD.sServerWantedSystem.eServerWSState), ", NewState = ", WANTED_SYSTEM_GET_GLOBAL_STATE_DEBUG_STRING(eNewState))
	GlobalServerBD.sServerWantedSystem.eServerWSState = eNewState
ENDPROC

PROC WANTED_SYSTEM_SET_CLIENT_ROLE(WANTED_SYSTEM_ROLE eRole)
	PRINTLN("[WS] WANTED_SYSTEM_SET_CLIENT_ROLE - Role: ", PICK_STRING(eRole = WS_COP, "COP", "CROOK"))

	GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sClientWantedSystem.eClientWSRole = eRole
ENDPROC

FUNC WANTED_SYSTEM_ROLE WANTED_SYSTEM_GET_CLIENT_ROLE(PLAYER_INDEX playerID)
	RETURN GlobalPlayerBD[NATIVE_TO_INT(playerID)].sClientWantedSystem.eClientWSRole
ENDFUNC

FUNC BOOL WANTED_SYSTEM_AM_I_A_COP()
	RETURN WANTED_SYSTEM_GET_CLIENT_ROLE(PLAYER_ID()) = WS_COP
ENDFUNC

FUNC BOOL WANTED_SYSTEM_IS_PLAYER_A_COP(PLAYER_INDEX playerID)
	
	IF playerID = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF

	RETURN (WANTED_SYSTEM_GET_CLIENT_ROLE(playerID) = WS_COP)
ENDFUNC

FUNC BOOL WANTED_SYSTEM_AM_I_A_CROOK()
	RETURN WANTED_SYSTEM_GET_CLIENT_ROLE(PLAYER_ID()) = WS_CROOK
ENDFUNC

PROC WANTED_SYSTEM_SET_CRIME_STATE(WANTED_SYSTEM_DATA &sWantedSystemData, INT iCrime, WANTED_SYSTEM_CRIME_STATE eState)
	
	PRINTLN("[WS] ** WANTED_SYSTEM_SET_CRIME_STATE - Updating crime: [ID: ", iCrime, "] from: ", WANTED_SYSTEM_GET_CRIME_STATE_DEBUG_STRING(sWantedSystemData.crimes[iCrime].eCrimeState), ", to: ", WANTED_SYSTEM_GET_CRIME_STATE_DEBUG_STRING(eState))
	
	sWantedSystemData.crimes[iCrime].eCrimeState = eState
ENDPROC

// ************************ VISUALS ***************************

/// PURPOSE:
///    Returns TRUE if a COP is in the vicinity of the crime.
FUNC BOOL WANTED_SYSTEM_IS_CRIME_VISIBLE(INT iIndex, WANTED_SYSTEM_DATA &sWantedSystemData, BOOL bRunBlipCheck = FALSE)

	// If you are not a cop then you can not see this crime
	IF NOT WANTED_SYSTEM_AM_I_A_COP()
		RETURN FALSE
	ENDIF
	
	IF iIndex = -1
		RETURN FALSE
	ENDIF
	
	// If we should check the blip then don't update if we already have the search radius
	IF bRunBlipCheck
		IF DOES_BLIP_EXIST(sWantedSystemData.crimes[iIndex].bCrime)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// If we are outside the radius then we would not see this crime blip
	VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
	IF VDIST2(vPlayerPos, sWantedSystemData.crimes[iIndex].vPosition) > (WS_TWEAK_CRIME_RANGE*WS_TWEAK_CRIME_RANGE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE	
ENDFUNC

/// PURPOSE:
///    Turn a player blip on / off
PROC WANTED_SYSTEM_TOGGLE_PLAYER_BLIP(WANTED_SYSTEM_DATA &sWantedSystemData, INT iIndex, BOOL bVisible)

	IF DOES_BLIP_EXIST(sWantedSystemData.biPlayers[iIndex])
		IF bVisible
			SET_BLIP_ALPHA(sWantedSystemData.biPlayers[iIndex], 255)
		ELSE
			SET_BLIP_ALPHA(sWantedSystemData.biPlayers[iIndex], 0)
		ENDIF		
	ENDIF

ENDPROC

PROC WANTED_SYSTEM_REMOVE_CRIME_BLIP(BLIP_INDEX &aBlip)
	IF DOES_BLIP_EXIST(aBlip)
		REMOVE_BLIP(aBlip)
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds a blip for the crime that is detected or lost (search radius)
PROC WANTED_SYSTEM_ADD_BLIP_FOR_CRIME(INT iIndex, WANTED_SYSTEM_DATA &sWantedSystemData)
	PRINTLN("[WS]")
	PRINTLN("[WS] WANTED_SYSTEM_ADD_BLIP_FOR_CRIME - Adding blip for crime: [ID:", iIndex, "]")

	WANTED_SYSTEM_REMOVE_CRIME_BLIP(sWantedSystemData.crimes[iIndex].bCrime)

	IF sWantedSystemData.crimes[iIndex].eCrimeState = WS_CRIME_LOST
		sWantedSystemData.crimes[iIndex].bCrime = ADD_BLIP_FOR_RADIUS(sWantedSystemData.crimes[iIndex].vPosition, WS_TWEAK_CRIME_LOST_RADIUS_START)
	ELSE
		sWantedSystemData.crimes[iIndex].bCrime = ADD_BLIP_FOR_RADIUS(sWantedSystemData.crimes[iIndex].vPosition, WS_TWEAK_CRIME_RADIUS)
	ENDIF
	
	SET_BLIP_COLOUR(sWantedSystemData.crimes[iIndex].bCrime, BLIP_COLOUR_RED)
	SET_BLIP_ALPHA(sWantedSystemData.crimes[iIndex].bCrime, 180)
	SET_BLIP_PRIORITY(sWantedSystemData.crimes[iIndex].bCrime, BLIPPRIORITY_LOW)
	
	IF sWantedSystemData.crimes[iIndex].eCrimeState = WS_CRIME_LOST
		SET_BLIP_FLASHES(sWantedSystemData.crimes[iIndex].bCrime, TRUE)
	ELSE
		SET_BLIP_FLASHES(sWantedSystemData.crimes[iIndex].bCrime, FALSE)
		PRINT_TICKER_WITH_PLAYER_NAME_STRING("STRING", "Search the area for the criminal")
	ENDIF
ENDPROC

/// PURPOSE:
///    Update our cop blips, displaying the view cone etc.
PROC WANTED_SYSTEM_UPDATE_COP_BLIPS(WANTED_SYSTEM_DATA &sWantedSystemData, BOOL bVisible)
	
	PRINTLN("[WS] WANTED_SYSTEM_UPDATE_COP_BLIPS - Called")
	
	IF NOT WANTED_SYSTEM_AM_I_A_CROOK()
		EXIT
	ENDIF
	
	INT i
	PLAYER_INDEX playerID
	
	REPEAT NUM_NETWORK_PLAYERS i
		
		playerID = INT_TO_PLAYERINDEX(i)
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			PRINTLN("[WS] WANTED_SYSTEM_UPDATE_COP_BLIPS - ", GET_PLAYER_NAME(playerID), " COP: ", WANTED_SYSTEM_IS_PLAYER_A_COP(playerID))
			IF WANTED_SYSTEM_IS_PLAYER_A_COP(playerID)
				WANTED_SYSTEM_TOGGLE_PLAYER_BLIP(sWantedSystemData, i, bVisible)
				SET_BLIP_SHOW_CONE(sWantedSystemData.biPlayers[i], bVisible)
				PRINTLN("[WS] WANTED_SYSTEM_UPDATE_COP_BLIPS - Showing cop blip for player: ", i)
			ENDIF
		ENDIF
	
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Cleans up a no longer valid crime.
PROC WANTED_SYSTEM_CLEANUP_CRIME(INT iIndex, WANTED_SYSTEM_DATA &sWantedSystemData)

	// Trying to clean up an invalid crime
	IF iIndex = -1
		EXIT
	ENDIF

	WANTED_SYSTEM_REMOVE_CRIME_BLIP(sWantedSystemData.crimes[iIndex].bCrime)
	
	IF WANTED_SYSTEM_AM_I_A_COP()
		WANTED_SYSTEM_TOGGLE_PLAYER_BLIP(sWantedSystemData, iIndex, FALSE)
	ELSE
		IF NATIVE_TO_INT(PLAYER_ID()) = iIndex
			WANTED_SYSTEM_UPDATE_COP_BLIPS(sWantedSystemData, FALSE)
			SET_FAKE_WANTED_LEVEL(0)
		ENDIF
	ENDIF
	
	// Reset all of the crime variables
	CLEAR_BIT(sWantedSystemData.iCrimeAvailableForTag, iIndex)
	WANTED_SYSTEM_SET_CRIME_STATE(sWantedSystemData, iIndex, WS_CRIME_COMMITTED)
	sWantedSystemData.crimes[iIndex].vPosition = <<0, 0, 0>>
	
	PRINTLN("[WS]")
	PRINTLN("[WS] WANTED_SYSTEM_CLEANUP_CRIME - Cleaned up crime. iIndex = ", iIndex)
ENDPROC

/// PURPOSE:
///    Mark the crime as tagged locally (Updates UI, State and players blips)
PROC WANTED_SYSTEM_MARK_CRIME_AS_TAGGED(INT iIndex, WANTED_SYSTEM_DATA &sWantedSystemData)
	
	IF iIndex != -1
		
		PRINTLN("[WS]")
		PRINTLN("[WS] WANTED_SYSTEM_MARK_CRIME_AS_TAGGED - Crime tagged: [ID:", iIndex, "]")
		// Clear our flag, as no need to have help text
		CLEAR_BIT(sWantedSystemData.iCrimeAvailableForTag, iIndex)
		WANTED_SYSTEM_REMOVE_CRIME_BLIP(sWantedSystemData.crimes[iIndex].bCrime)
		WANTED_SYSTEM_SET_CRIME_STATE(sWantedSystemData, iIndex, WS_CRIME_TAGGED)
		
		// Make sure the cop can now see the criminal
		IF WANTED_SYSTEM_AM_I_A_COP()
			WANTED_SYSTEM_TOGGLE_PLAYER_BLIP(sWantedSystemData, iIndex, TRUE)
		ENDIF
	ENDIF	
ENDPROC


// *********************** EVENTS **********************

/// PURPOSE:
///    On receiving a crime committed event it will be stored agains the player who has performed it
PROC WANTED_SYSTEM_PROCESS_CRIME_COMMMITTED(INT iEventID, WANTED_SYSTEM_DATA &sWantedSystemData)

	WANTED_SYSTEM_CRIME_STRUCT Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))

		IF Event.details.FromPlayerIndex != INVALID_PLAYER_INDEX()
			
			PRINTLN("[WS]------------------------------------------------------------------------------")
			PRINTLN("[WS] WANTED_SYSTEM_PROCESS_CRIME_COMMMITTED - Crime committed [", GET_PLAYER_NAME(Event.details.FromPlayerIndex) ,"]")
			PRINTLN("[WS] 										 - Position		: ", Event.vPosition)
			PRINTLN("[WS] 										 - TimeOfCrime	: ", NATIVE_TO_INT(Event.timeOfCrime))
			
			// Store our details
			sWantedSystemData.crimes[NATIVE_TO_INT(Event.details.FromPlayerIndex)].vPosition = Event.vPosition
			sWantedSystemData.crimes[NATIVE_TO_INT(Event.details.FromPlayerIndex)].timeOfCrime = Event.timeOfCrime
			
			// Check to see if we should see a blip for this crime (ie. Are we a cop, close to crime?)
			IF WANTED_SYSTEM_IS_CRIME_VISIBLE(NATIVE_TO_INT(Event.details.FromPlayerIndex), sWantedSystemData)
			
				//...yes, draw the blip to our map so we can start searching
				WANTED_SYSTEM_ADD_BLIP_FOR_CRIME(NATIVE_TO_INT(Event.details.FromPlayerIndex), sWantedSystemData)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[WS] 								- Not visible to local player")
				
				IF NOT WANTED_SYSTEM_AM_I_A_COP()
					PRINTLN("[WS] 								- WANTED_SYSTEM_AM_I_A_COP() = FALSE")
				ELSE 
					PRINTLN("[WS] 								- DIST is beyond range")
				ENDIF
			#ENDIF
				
			ENDIF
			
			PRINTLN("[WS]------------------------------------------------------------------------------")
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Local player broadcasts to all players that they have committed a crime
PROC WANTED_SYSTEM_BROADCAST_PLAYER_COMMITTED_CRIME()
	
	// For now set a fake wanted level
	SET_FAKE_WANTED_LEVEL(3)
	
	WANTED_SYSTEM_CRIME_STRUCT Event
	Event.details.Type = SCRIPT_EVENT_WANTED_SYSTEM_CRIME
	Event.details.FromPlayerIndex = PLAYER_ID()
	
	// Include details of where this was performed and when (for expiry)
	Event.vPosition = GET_PLAYER_COORDS(PLAYER_ID())
	Event.timeOfCrime = GET_NETWORK_TIME()
		
	INT iBS = ALL_PLAYERS(TRUE, FALSE)
	IF NOT (iBS = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iBS)
	ELSE
		NET_PRINT("WANTED_SYSTEM_BROADCAST_PLAYER_COMMITTED_CRIME - playerflags = 0 so not broadcasting") NET_NL()
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Process the incoming event to move the crime to the 'Tagged' state and update blips
PROC WANTED_SYSTEM_PROCESS_CRIME_HAS_BEEN_TAGGED(INT iEventID, WANTED_SYSTEM_DATA &sWantedSystemData)

	WANTED_SYSTEM_CRIME_TAGGED Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		
		IF Event.iTaggedCrime != -1
		
			PRINTLN("[WS]------------------------------------------------------------------------------")
			PRINTLN("[WS] WANTED_SYSTEM_PROCESS_CRIME_HAS_BEEN_TAGGED - T(", GET_CLOUD_TIME_AS_INT(), ")")
			PRINTLN("[WS] 	 			- Event.iTaggedCrime = ", Event.iTaggedCrime)
			WANTED_SYSTEM_MARK_CRIME_AS_TAGGED(Event.iTaggedCrime, sWantedSystemData)
			
			// If this is my crime (local criminal), view cop blips
			IF WANTED_SYSTEM_AM_I_A_CROOK()
				IF NATIVE_TO_INT(PLAYER_ID()) = Event.iTaggedCrime
					WANTED_SYSTEM_UPDATE_COP_BLIPS(sWantedSystemData, TRUE)
				ENDIF
			ENDIF
			PRINTLN("[WS]------------------------------------------------------------------------------")
		ENDIF		
	ENDIF
ENDPROC

/// PURPOSE:
///    Alert all cops that a crime has been tagged
PROC WANTED_SYSTEM_BROADCAST_CRIME_HAS_BEEN_TAGGED(INT iCrime)

	WANTED_SYSTEM_CRIME_TAGGED Event
	Event.details.Type = SCRIPT_EVENT_WANTED_SYSTEM_TAGGED_CRIME
	Event.details.FromPlayerIndex = PLAYER_ID()
	Event.iTaggedCrime = iCrime
		
	INT iBS = ALL_PLAYERS(TRUE, FALSE)
	IF NOT (iBS = 0)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iBS)
	ELSE
		NET_PRINT("WANTED_SYSTEM_BROADCAST_CRIME_HAS_BEEN_TAGGED - playerflags = 0 so not broadcasting") NET_NL()
	ENDIF

ENDPROC

/// PURPOSE:
///    Sets up the correct blip for the player based on their role and the local players role
PROC WANTED_SYSTEM_ADD_BLIP_FOR_PLAYER(PLAYER_INDEX playerID, WANTED_SYSTEM_DATA &sWantedSystemData, BOOL bCop)
	INT i = NATIVE_TO_INT(playerID)
	IF playerID != PLAYER_ID()
		sWantedSystemData.biPlayers[i] = ADD_BLIP_FOR_ENTITY(GET_PLAYER_PED(playerID))
	ENDIF
	
	IF bCop
	
		IF playerID != PLAYER_ID()
			IF WANTED_SYSTEM_AM_I_A_COP()
				SET_BLIP_COLOUR(sWantedSystemData.biPlayers[i], BLIP_COLOUR_BLUELIGHT)
			ELSE
				SET_BLIP_SPRITE(sWantedSystemData.biPlayers[i], INT_TO_ENUM(BLIP_SPRITE, 41))
				SET_BLIP_SCALE(sWantedSystemData.biPlayers[i], 0.7)
				SET_BLIP_ALPHA(sWantedSystemData.biPlayers[i], 0)
			ENDIF
		ENDIF
	
		#IF IS_DEBUG_BUILD
		sWantedSystemData.bCops[i] = TRUE
		#ENDIF
	ELSE
		IF playerID != PLAYER_ID()
			IF WANTED_SYSTEM_AM_I_A_CROOK()
				SET_BLIP_COLOUR(sWantedSystemData.biPlayers[i], BLIP_COLOUR_RED)
			ELSE
				SET_BLIP_COLOUR(sWantedSystemData.biPlayers[i], BLIP_COLOUR_RED)
				SET_BLIP_SPRITE(sWantedSystemData.biPlayers[i], INT_TO_ENUM(BLIP_SPRITE, 252))
				SET_BLIP_SCALE(sWantedSystemData.biPlayers[i], 1.3)
				SET_BLIP_ALPHA(sWantedSystemData.biPlayers[i], 0)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ******************* CLIENT PROCESSING *****************

/// PURPOSE:
///    Update our role based on the Server data. Update every player's sprite
PROC WANTED_SYSTEM_SETUP_ROLE(WANTED_SYSTEM_DATA &sWantedSystemData, INT iRoles)

	WANTED_SYSTEM_ROLE myRole
	// From server data, mark our role and display a Shard to indicate this to the player
	IF IS_BIT_SET(iRoles, NATIVE_TO_INT(PLAYER_ID()))
		myRole = WS_COP
		SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GENERIC_MIDSIZED, "STRING", "You are a Cop", HUD_COLOUR_BLUE, DEFAULT, "PM_INF_QMFT", HUD_COLOUR_BLUE)
		
		MP_OUTFITS_APPLY_DATA 	sApplyData
		sApplyData.pedID 		= PLAYER_PED_ID()
		sApplyData.eOutfit 		= OUTFIT_HEIST_POLICE_0
		sApplyData.eApplyStage 	= AOS_SET 		// Force stage to setting stage
		sApplyData.iPlayerIndex = -1			// This will only ever by the local player
		SET_PED_MP_OUTFIT(sApplyData, FALSE)		
	ELSE
		myRole = WS_CROOK
		SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GENERIC_MIDSIZED, "STRING", "You are a Criminal", HUD_COLOUR_RED, DEFAULT, "PM_INF_QMFT", HUD_COLOUR_RED)
		SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
	ENDIF

	WANTED_SYSTEM_SET_CLIENT_ROLE(myRole)

	HIDE_ALL_PLAYER_BLIPS(TRUE)
	
	INT i
	PLAYER_INDEX playerID
	REPEAT NUM_NETWORK_PLAYERS i
		playerID = INT_TO_PLAYERINDEX(i)
		
		IF DOES_BLIP_EXIST(sWantedSystemData.biPlayers[i])
			REMOVE_BLIP(sWantedSystemData.biPlayers[i])
		ENDIF
				
		IF IS_NET_PLAYER_OK(playerID, FALSE)
		
			WANTED_SYSTEM_ADD_BLIP_FOR_PLAYER(playerID, sWantedSystemData, IS_BIT_SET(iRoles, i))
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Receive event to override the roles of the wanted system
PROC WANTED_SYSTEM_PROCESS_UPDATE_ROLES(INT iEventID, WANTED_SYSTEM_DATA &sWantedSystemData)
	
	WANTED_SYSTEM_UPDATE_ROLES Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		
		PRINTLN("[WS] WANTED_SYSTEM_PROCESS_UPDATE_ROLES - Roles updates with bitset: ", Event.iNewRoles, " From: ", GET_PLAYER_NAME(Event.details.FromPlayerIndex))
		WANTED_SYSTEM_SETUP_ROLE(sWantedSystemData, Event.iNewRoles)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			GlobalServerBD.sServerWantedSystem.iServerWSRoleBitset = Event.iNewRoles
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Additional checks locally for committing a crime
PROC WANTED_SYSTEM_MONITOR_CRIMES()
	
	// Do not process this if I am not a crook
	IF NOT WANTED_SYSTEM_AM_I_A_CROOK()
		EXIT
	ENDIF
	
	BOOL bCrimeCommitted = FALSE
	// Listen for jacking (net_ambience.sch)
	IF IS_PED_JACKING(PLAYER_PED_ID())
		bCrimeCommitted = TRUE
	ENDIF
	
	IF bCrimeCommitted
		WANTED_SYSTEM_BROADCAST_PLAYER_COMMITTED_CRIME()
	ENDIF
ENDPROC

/// PURPOSE:
///    Basic LOS check for Cop to Crook. If spotted return TRUE
FUNC BOOL WANTED_SYSTEM_HAS_CROOK_BEEN_SPOTTED(WANTED_SYSTEM_DATA &sWantedSystemData)
		
	PED_INDEX playerPed
	IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(sWantedSystemData.iStaggeredCrime), FALSE)
		playerPed = GET_PLAYER_PED(INT_TO_PLAYERINDEX(sWantedSystemData.iStaggeredCrime))
		IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(PLAYER_PED_ID(), playerPed)
		AND WANTED_SYSTEM_IS_CRIME_VISIBLE(sWantedSystemData.iStaggeredCrime, sWantedSystemData, FALSE)
		
			// If the timeout has been triggered but now we see the criminal, reset our timer
			IF sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].lossOfSight != null
				sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].lossOfSight = NULL
			ENDIF
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL WANTED_SYSTEM_IS_TAGGING_HELP_SHOWN()
	BEGIN_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STRING")
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("Use ~INPUT_FRONTEND_LEFT~ to tag this Criminal and alert all other Cops")
	RETURN END_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(HELP_TEXT_SLOT_STANDARD)
ENDFUNC


/// PURPOSE:
///    Monitor if any crimes are in the tagged / lost phase. If so monitor the player input or animate the search radius
PROC WANTED_SYSTEM_MAINTAIN_CRIME_EVERY_FRAME(WANTED_SYSTEM_DATA &sWantedSystemData)
	
	IF NOT WANTED_SYSTEM_AM_I_A_COP()
		EXIT
	ENDIF
	
	// Tagging available if a bit has been set
	IF sWantedSystemData.iCrimeAvailableForTag != 0
		
		IF NOT WANTED_SYSTEM_IS_TAGGING_HELP_SHOWN()
			PRINT_HELP_WITH_PLAYER_NAME("STRING", "Use ~INPUT_FRONTEND_LEFT~ to tag this Criminal and alert all other Cops", HUD_COLOUR_WHITE)
		ENDIF
		
		// If the player chooses to tag the criminal, all other cops should see their blip
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		
			INT i
			REPEAT NUM_NETWORK_PLAYERS i
				
				// This crime has been spotted locally so we should be able to tag
				IF sWantedSystemData.crimes[i].eCrimeState = WS_CRIME_SPOTTED_LOCAL
					PRINTLN("[WS] WANTED_SYSTEM_MAINTAIN_TAGGING_OF_CRIMES - Crime tagged: [ID:", i, "]. Crook: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)))
					
					// All cops need to know about this.
					WANTED_SYSTEM_BROADCAST_CRIME_HAS_BEEN_TAGGED(i)
					WANTED_SYSTEM_MARK_CRIME_AS_TAGGED(i, sWantedSystemData)
									
					PRINT_TICKER_WITH_PLAYER_NAME_STRING("STRING", "All Cops have been alerted.")
				ENDIF
			ENDREPEAT
			
		ENDIF
	ELSE
		IF WANTED_SYSTEM_IS_TAGGING_HELP_SHOWN()
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	INT i
	INT iTimeDelta
	FLOAT fPercentage
	// Loop over those that are 'LOST' and enlarge the area of the blip while the cops can search
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF sWantedSystemData.crimes[i].eCrimeState = WS_CRIME_LOST
			
			IF DOES_BLIP_EXIST(sWantedSystemData.crimes[i].bCrime)				
				iTimeDelta = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sWantedSystemData.crimes[i].timeOfCrime))
				
				IF iTimeDelta <= (WS_TWEAK_CRIME_LOST_TIME*1000)
				
					fPercentage = (WS_TWEAK_CRIME_LOST_RADIUS_END - WS_TWEAK_CRIME_LOST_RADIUS_START) * (TO_FLOAT(iTimeDelta) / TO_FLOAT(WS_TWEAK_CRIME_LOST_TIME*1000))
				
					//PRINTLN("[WS] WS_CRIME_LOST.... iTimeDetla = ", iTimeDelta, ", fPercentage = ", fPercentage)
				
					SET_BLIP_SCALE(sWantedSystemData.crimes[i].bCrime, fPercentage + WS_TWEAK_CRIME_LOST_RADIUS_START)
				ENDIF
			ENDIF
		
		ENDIF		
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Moves the crime to a LOST state. Updates location and time so we see a search area enlarge
PROC WANTED_SYSTEM_MOVE_CRIME_TO_LOST_STATE(INT iIndex, TIME_DATATYPE time, WANTED_SYSTEM_DATA &sWantedSystemData)
	
	// Mark the time (used to slowly make the blip larger)
	sWantedSystemData.crimes[iIndex].timeOfCrime = time
	WANTED_SYSTEM_SET_CRIME_STATE(sWantedSystemData, iIndex, WS_CRIME_LOST)
	sWantedSystemData.crimes[iIndex].vPosition = GET_PLAYER_COORDS(INT_TO_PLAYERINDEX(iIndex))
	
	IF WANTED_SYSTEM_AM_I_A_COP()
	AND WANTED_SYSTEM_IS_CRIME_VISIBLE(iIndex, sWantedSystemData, FALSE)
		WANTED_SYSTEM_ADD_BLIP_FOR_CRIME(iIndex, sWantedSystemData)
		WANTED_SYSTEM_TOGGLE_PLAYER_BLIP(sWantedSystemData, iIndex, FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    While the cop is actively involved in crimes, update if they can see the crime or not
PROC WANTED_SYSTEM_MAINTAIN_SPOTTED_CRIMES(WANTED_SYSTEM_DATA &sWantedSystemData)

	IF sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].eCrimeState = WS_CRIME_TAGGED
	OR sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].eCrimeState = WS_CRIME_LOST

		IF WANTED_SYSTEM_AM_I_A_COP()
		
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sClientWantedSystem.iBSCrimesVisible, sWantedSystemData.iStaggeredCrime)
				IF NOT WANTED_SYSTEM_HAS_CROOK_BEEN_SPOTTED(sWantedSystemData)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sClientWantedSystem.iBSCrimesVisible, sWantedSystemData.iStaggeredCrime)
					PRINTLN("[WS] WANTED_SYSTEM_MAINTAIN_SPOTTED_CRIMES - Crime [ID:", sWantedSystemData.iStaggeredCrime, "] SPOTTED")
				ENDIF
			ELSE
				IF WANTED_SYSTEM_HAS_CROOK_BEEN_SPOTTED(sWantedSystemData)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sClientWantedSystem.iBSCrimesVisible, sWantedSystemData.iStaggeredCrime)
					PRINTLN("[WS] WANTED_SYSTEM_MAINTAIN_SPOTTED_CRIMES - Crime [ID:", sWantedSystemData.iStaggeredCrime, "] LOST")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL WANTED_SYSTEM_HAS_LOSS_OF_SIGHT_TIMED_OUT(WANTED_SYSTEM_DATA &sWantedSystemData)
	
	IF sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].lossOfSight != NULL
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].lossOfSight)) >= 1500
			RETURN TRUE
		ENDIF	
	ELSE
		sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].lossOfSight = GET_NETWORK_TIME()
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Monitor the crimes based on their current state.
PROC WANTED_SYSTEM_MAINTAIN_CRIMES(WANTED_SYSTEM_DATA &sWantedSystemData)
	
	WANTED_SYSTEM_MAINTAIN_CRIME_EVERY_FRAME(sWantedSystemData)
	
	WANTED_SYSTEM_MAINTAIN_SPOTTED_CRIMES(sWantedSystemData)
	
	IF NOT IS_VECTOR_ZERO(sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].vPosition)
		
		SWITCH sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].eCrimeState
		
			// Handles initial state of a crime (cops are searching for the crook)
			CASE WS_CRIME_COMMITTED
				
				IF WANTED_SYSTEM_AM_I_A_COP()
				
					// Make sure it is visible if not already
					IF WANTED_SYSTEM_IS_CRIME_VISIBLE(sWantedSystemData.iStaggeredCrime, sWantedSystemData, TRUE)
						
						WANTED_SYSTEM_ADD_BLIP_FOR_CRIME(sWantedSystemData.iStaggeredCrime, sWantedSystemData)
					ENDIF
					
					// If the crime has timed out of no longer valid, clean up
					IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].timeOfCrime)) >= (WS_TWEAK_CRIME_TIMEOUT*1000)
					OR NOT IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(sWantedSystemData.iStaggeredCrime))
						WANTED_SYSTEM_CLEANUP_CRIME(sWantedSystemData.iStaggeredCrime, sWantedSystemData)
					ENDIF
					
					// Check to see if we have spotted the crook, option to tag
					IF WANTED_SYSTEM_HAS_CROOK_BEEN_SPOTTED(sWantedSystemData)
					
						PRINTLN("[WS] WANTED_SYSTEM_MAINTAIN_CRIMES - Crook has been spotted. Allow tagging: [ID:", sWantedSystemData.iStaggeredCrime, "]")
					
						SET_BIT(sWantedSystemData.iCrimeAvailableForTag, sWantedSystemData.iStaggeredCrime)
						WANTED_SYSTEM_REMOVE_CRIME_BLIP(sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].bCrime)
						WANTED_SYSTEM_TOGGLE_PLAYER_BLIP(sWantedSystemData, sWantedSystemData.iStaggeredCrime, TRUE)
						
						sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].lossOfSight = NULl
						WANTED_SYSTEM_SET_CRIME_STATE(sWantedSystemData, sWantedSystemData.iStaggeredCrime, WS_CRIME_SPOTTED_LOCAL)
					ENDIF
				ELSE
					// If the crime has timed out of no longer valid, clean up
					IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].timeOfCrime)) >= (WS_TWEAK_CRIME_TIMEOUT*1000)
						WANTED_SYSTEM_CLEANUP_CRIME(sWantedSystemData.iStaggeredCrime, sWantedSystemData)
					ENDIF
				ENDIF
			BREAK
			
			// Handles the state once the crook has been spotted locally on the cop machine
			CASE WS_CRIME_SPOTTED_LOCAL
			
				IF WANTED_SYSTEM_AM_I_A_COP()
					// Check to see if we have spotted the crook, option to tag
					IF NOT WANTED_SYSTEM_HAS_CROOK_BEEN_SPOTTED(sWantedSystemData)
					AND WANTED_SYSTEM_HAS_LOSS_OF_SIGHT_TIMED_OUT(sWantedSystemData)
						
						PRINTLN("[WS] WANTED_SYSTEM_MAINTAIN_CRIMES - WS_CRIME_SPOTTED_LOCAL Crook has been lost. [ID:", sWantedSystemData.iStaggeredCrime, "]")
					
						// If they had been spotted but never tagged and they escaped them you lost them?
						PRINT_TICKER_WITH_PLAYER_NAME_STRING("STRING", "Criminal has been lost")
						WANTED_SYSTEM_CLEANUP_CRIME(sWantedSystemData.iStaggeredCrime, sWantedSystemData)
					ENDIF
				ENDIF
			BREAK
			
			// Handles once a crime has been tagged for all other cops to see (should see crook blip until they are lost)
			CASE WS_CRIME_TAGGED
				// Server now notifies clients that a crime has been lost from tagged so nothing to do here
			BREAK
			
			// Handles the crime being lost, search radius will enlarge until they have full been lost
			CASE WS_CRIME_LOST
				IF WANTED_SYSTEM_AM_I_A_COP()
				AND (WANTED_SYSTEM_HAS_CROOK_BEEN_SPOTTED(sWantedSystemData) OR WANTED_SYSTEM_IS_CRIME_GLOBALLY_VISIBLE(sWantedSystemData.iStaggeredCrime))
					
					PRINTLN("[WS] WANTED_SYSTEM_MAINTAIN_CRIMES - WS_CRIME_LOST Crook has been spotted again. [ID:", sWantedSystemData.iStaggeredCrime, "]")
					
					// Alert all other players we have this criminal tagged again
					WANTED_SYSTEM_BROADCAST_CRIME_HAS_BEEN_TAGGED(sWantedSystemData.iStaggeredCrime)
					WANTED_SYSTEM_MARK_CRIME_AS_TAGGED(sWantedSystemData.iStaggeredCrime, sWantedSystemData)
					
					// Update our blips on the minimap
					WANTED_SYSTEM_TOGGLE_PLAYER_BLIP(sWantedSystemData, sWantedSystemData.iStaggeredCrime, TRUE)
					WANTED_SYSTEM_REMOVE_CRIME_BLIP(sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].bCrime)
				ELSE
				
					// If the time expires and still haven't spotted the player again, cleanup
					IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sWantedSystemData.crimes[sWantedSystemData.iStaggeredCrime].timeOfCrime)) >= (WS_TWEAK_CRIME_LOST_TIME*1000)
						PRINTLN("[WS] WANTED_SYSTEM_MAINTAIN_CRIMES - WS_CRIME_LOST Crime has timed out. [ID:", sWantedSystemData.iStaggeredCrime, "]")
						WANTED_SYSTEM_CLEANUP_CRIME(sWantedSystemData.iStaggeredCrime, sWantedSystemData)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	// Increment and loop as we finish processing a full loop
	sWantedSystemData.iStaggeredCrime++
	IF sWantedSystemData.iStaggeredCrime >= NUM_NETWORK_PLAYERS
		sWantedSystemData.iStaggeredCrime = 0
	ENDIF
	
ENDPROC

PROC WANTED_SYSTEM_MAINTAIN_PLAYER_BLIPS(WANTED_SYSTEM_DATA &sWantedSystemData)
	IF sWantedSystemData.iBlipToManage != 0
		
		INT i
		PLAYER_INDEX playerID
		
		REPEAT NUM_NETWORK_PLAYERS i
			playerID = INT_TO_PLAYERINDEX(i)
			
			IF IS_BIT_SET(sWantedSystemData.iBlipToManage, i)
				IF IS_NET_PLAYER_OK(playerID, TRUE)
					PRINTLN("[WS] WANTED_SYSTEM_MAINTAIN_PLAYER_BLIPS - Re-adding a blip for [", GET_PLAYER_NAME(playerID) ,"]")
					WANTED_SYSTEM_ADD_BLIP_FOR_PLAYER(playerID, sWantedSystemData, WANTED_SYSTEM_IS_PLAYER_A_COP(playerID))
					CLEAR_BIT(sWantedSystemData.iBlipToManage, i)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

// ******************* SERVER PROCESSING *****************

PROC WANTED_SYSTEM_PROCESS_CRIME_LOST(INT iEventID, WANTED_SYSTEM_DATA &sWantedSystemData)
	WANTED_SYSTEM_CRIME_LOST Event	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		
		PRINTLN("[WS]------------------------------------------------------------------------------")
		PRINTLN("[WS] WANTED_SYSTEM_PROCESS_CRIME_LOST - Crime lost [", GET_PLAYER_NAME(Event.details.FromPlayerIndex) ,"]")
		PRINTLN("[WS] 										 - iCrime		: ", Event.iCrime)
		PRINTLN("[WS] 										 - TimeLost	: ", NATIVE_TO_INT(Event.timeCrimeLost))
		
		IF Event.iCrime != -1
			IF sWantedSystemData.crimes[Event.iCrime].eCrimeState = WS_CRIME_TAGGED
				
				WANTED_SYSTEM_MOVE_CRIME_TO_LOST_STATE(Event.iCrime, Event.timeCrimeLost, sWantedSystemData)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC WANTED_SYSTEM_BROADCAST_CRIME_LOST(INT iCrime)

	WANTED_SYSTEM_CRIME_LOST Event
	Event.details.Type = SCRIPT_EVENT_WANTED_SYSTEM_CRIME_LOST
	Event.details.FromPlayerIndex = PLAYER_ID()
	
	// Include details of where this was performed and when (for expiry)
	Event.iCrime = iCrime
	Event.timeCrimeLost = GET_NETWORK_TIME()
		
	INT iBS = ALL_PLAYERS(TRUE, FALSE)
	IF NOT (iBS = 0)
		PRINTLN("[WS][SERVER] WANTED_SYSTEM_BROADCAST_CRIME_LOST - Firing event for crime: ", iCrime)
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iBS)
	ELSE
		PRINTLN("[WS][SERVER] WANTED_SYSTEM_BROADCAST_CRIME_LOST - playerflags = 0 so not broadcasting")
	ENDIF

ENDPROC

/// PURPOSE:
///    Server needs to monitor if Cops can see the criminal during an active crime 
PROC WANTED_SYSTEM_SERVER_MAINTAIN_CRIME(WANTED_SYSTEM_DATA &sWantedSystemData)

	INT i
	BOOL bCrimeVisible
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_BIT_SET(GlobalPlayerBD[i].sClientWantedSystem.iBSCrimesVisible, sWantedSystemData.iServerStaggeredCrime)
			bCrimeVisible = TRUE
			i = NUM_NETWORK_PLAYERS
		ENDIF
	ENDREPEAT
	
	IF bCrimeVisible
		IF NOT IS_BIT_SET(GlobalServerBD.sServerWantedSystem.iBSCrimesVisible, sWantedSystemData.iServerStaggeredCrime)
			SET_BIT(GlobalServerBD.sServerWantedSystem.iBSCrimesVisible, sWantedSystemData.iServerStaggeredCrime)
			PRINTLN("[WS][SERVER] WANTED_SYSTEM_SERVER_MAINTAIN_CRIME - Marking visible crime: [ID: ", sWantedSystemData.iServerStaggeredCrime, "]")
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalServerBD.sServerWantedSystem.iBSCrimesVisible, sWantedSystemData.iServerStaggeredCrime)
			CLEAR_BIT(GlobalServerBD.sServerWantedSystem.iBSCrimesVisible, sWantedSystemData.iServerStaggeredCrime)
			
			WANTED_SYSTEM_BROADCAST_CRIME_LOST(sWantedSystemData.iServerStaggeredCrime)
			
			PRINTLN("[WS][SERVER] WANTED_SYSTEM_SERVER_MAINTAIN_CRIME - Marking crime NOT visible: [ID: ", sWantedSystemData.iServerStaggeredCrime, "]")
		ENDIF
	ENDIF
	
	sWantedSystemData.iServerStaggeredCrime++
	IF sWantedSystemData.iServerStaggeredCrime >= NUM_NETWORK_PLAYERS
		sWantedSystemData.iServerStaggeredCrime = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Return if the system is active (Server flag)
FUNC BOOL WANTED_SYSTEM_MONITOR_ACTIVE()
	RETURN (GlobalServerBD.sServerWantedSystem.bWSActive)	
ENDFUNC

/// PURPOSE:
///    Steps through the list of active players randomly shuffling them
PROC WANTED_SYSTEM_SHUFFLE_PLAYERS(PLAYER_INDEX &activePlayers[], INT iTotalPlayers)

	INT i
	INT iRand
	PLAYER_INDEX aTempPlayer
	FOR i = 0 TO (iTotalPlayers-1) STEP 1
			
		iRand = GET_RANDOM_INT_IN_RANGE(0, iTotalPlayers)
	
		aTempPlayer = activePlayers[iRand]
		activePlayers[iRand] = activePlayers[i]
		activePlayers[i] = aTempPlayer
	ENDFOR

ENDPROC

/// PURPOSE:
///   Process the current active players, assigning them a Cop or Crook role
FUNC BOOL WANTED_SYSTEM_ASSIGN_ROLES()

	// Reset
	GlobalServerBD.sServerWantedSystem.iServerWSRoleBitset = 0
	
	// Calculate how many cops we will create (always Ceil)
	INT iTotalPlayers = NETWORK_GET_TOTAL_NUM_PLAYERS()
	INT iTotalCops = CEIL(iTotalPlayers * 0.5)
		
	PRINTLN("[WS][SERVER] WANTED_SYSTEM_ASSIGN_ROLES: T(", GET_CLOUD_TIME_AS_INT(), "), iTotalPlayers = ", iTotalPlayers, ", iTotalCops = ", iTotalCops)
		
	INT i
	INT iCount
	PLAYER_INDEX aPlayer
	PLAYER_INDEX activePlayers[NUM_NETWORK_PLAYERS]
	
	// Grab all active players in the session
	REPEAT NUM_NETWORK_PLAYERS i
		
		aPlayer = INT_TO_PLAYERINDEX(i)
		IF aPlayer != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(aPlayer, FALSE, TRUE)
			activePlayers[iCount] = aPlayer
			iCount++
			
			PRINTLN("[WS][SERVER]			ActivePlayer: ", GET_PLAYER_NAME(aPlayer))
		ENDIF
	ENDREPEAT
	
	PRINTLN("[WS][SERVER]-----------------------------------------------------")
	
	// Shuffle our list so the cops are random each time
	WANTED_SYSTEM_SHUFFLE_PLAYERS(activePlayers, iTotalPlayers)
	
	// Set the roles of the cop players
	PRINTLN("[WS][SERVER]	Cops:: ")
	REPEAT iTotalCops i
		
		PRINTLN("[WS][SERVER]				: ", GET_PLAYER_NAME(activePlayers[i]))
		
		SET_BIT(GlobalServerBD.sServerWantedSystem.iServerWSRoleBitset, NATIVE_TO_INT(activePlayers[i]))
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	// Also print the crooks
	PRINTLN("[WS][SERVER]	Crooks:: ")
	REPEAT iTotalPlayers i
		
		IF NOT (IS_BIT_SET(GlobalServerBD.sServerWantedSystem.iServerWSRoleBitset, NATIVE_TO_INT(activePlayers[i])))
			PRINTLN("[WS][SERVER]				: ", GET_PLAYER_NAME(activePlayers[i]))
		ENDIF
	ENDREPEAT
	#ENDIF
	PRINTLN("[WS][SERVER]-----------------------------------------------------")
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Process client side logic of wanted system
PROC WANTED_SYSTEM_PROCESS_CLIENT(WANTED_SYSTEM_DATA &sWantedSystemData)

	SWITCH WANTED_SYSTEM_GET_CLIENT_STATE()
		CASE WS_GLOBAL_INITIALISATION
			WANTED_SYSTEM_SET_CLIENT_STATE(WS_GLOBAL_IDLE)
		BREAK
		
		CASE WS_GLOBAL_IDLE				
			IF (WANTED_SYSTEM_GET_SERVER_STATE() = WS_GLOBAL_RUNNING)
				WANTED_SYSTEM_SET_CLIENT_STATE(WS_GLOBAL_SETUP)
			ENDIF
		BREAK
		
		CASE WS_GLOBAL_SETUP
			WANTED_SYSTEM_SETUP_ROLE(sWantedSystemData, GlobalServerBD.sServerWantedSystem.iServerWSRoleBitset)
			WANTED_SYSTEM_SET_CLIENT_STATE(WS_GLOBAL_RUNNING)
		BREAK
		
		CASE WS_GLOBAL_RUNNING				
			
			#IF IS_DEBUG_BUILD
			IF NOT sWantedSystemData.bDisableWantedLevels
				SET_WANTED_LEVEL_MULTIPLIER(0)
			ENDIF
			#ENDIF
			
			WANTED_SYSTEM_MONITOR_CRIMES()
			WANTED_SYSTEM_MAINTAIN_CRIMES(sWantedSystemData)
			WANTED_SYSTEM_MAINTAIN_PLAYER_BLIPS(sWantedSystemData)
		
		BREAK
		CASE WS_GLOBAL_CLEANUP				
			IF WANTED_SYSTEM_GET_SERVER_STATE() = WS_GLOBAL_IDLE
				WANTED_SYSTEM_SET_CLIENT_STATE(WS_GLOBAL_INITIALISATION)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Process server side logic of wanted system
PROC WANTED_SYSTEM_PROCESS_SERVER(WANTED_SYSTEM_DATA &sWantedSystemData)

	// All clients should exit
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF

	SWITCH WANTED_SYSTEM_GET_SERVER_STATE()
		CASE WS_GLOBAL_IDLE
			IF WANTED_SYSTEM_MONITOR_ACTIVE()			
				WANTED_SYSTEM_SET_SERVER_STATE(WS_GLOBAL_SETUP)	
			ENDIF
		BREAK
		
		CASE WS_GLOBAL_SETUP
			IF WANTED_SYSTEM_ASSIGN_ROLES()
				WANTED_SYSTEM_SET_SERVER_STATE(WS_GLOBAL_RUNNING)	
			ENDIF
		BREAK
		
		CASE WS_GLOBAL_RUNNING
			WANTED_SYSTEM_SERVER_MAINTAIN_CRIME(sWantedSystemData)
		BREAK
		
	ENDSWITCH

ENDPROC

PROC WANTED_SYSTEM_REMOVE_PED_BLIP(INT iBlip, WANTED_SYSTEM_DATA &sWantedSystemData)
	IF DOES_BLIP_EXIST(sWantedSystemData.biPlayers[iBlip])
		REMOVE_BLIP(sWantedSystemData.biPlayers[iBlip])
	ENDIF
ENDPROC

PROC WANTED_SYSTEM_SCRIPT_CLEANUP(WANTED_SYSTEM_DATA &sWantedSystemData)

	#IF IS_DEBUG_BUILD
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_EnableWantedSystem")
		EXIT
	ENDIF
	#ENDIF

	PRINTLN("[WS] ********************************************")
	PRINTLN("[WS] WANTED_SYSTEM_SCRIPT_CLEANUP - Called... ")
	PRINTLN("[WS] ********************************************")
	
	HIDE_ALL_PLAYER_BLIPS(FALSE)
		
	SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
	SET_FAKE_WANTED_LEVEL(0)
	
	WANTED_SYSTEM_SET_CLIENT_ROLE(WS_NONE)
	WANTED_SYSTEM_SET_CLIENT_STATE(WS_GLOBAL_CLEANUP)
		
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		WANTED_SYSTEM_REMOVE_PED_BLIP(i, sWantedSystemData)
		
		// Clean up crime
		WANTED_SYSTEM_CLEANUP_CRIME(i, sWantedSystemData)		
	ENDREPEAT
	
	sWantedSystemData.iStaggeredCrime = 0
	sWantedSystemData.iCrimeAvailableForTag = 0
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sClientWantedSystem.iBSCrimesVisible = 0
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		WANTED_SYSTEM_SET_SERVER_STATE(WS_GLOBAL_IDLE)
		WANTED_SYSTEM_SET_CLIENT_STATE(WS_GLOBAL_INITIALISATION)
		GlobalServerBD.sServerWantedSystem.iServerWSRoleBitset = 0
		GlobalServerBD.sServerWantedSystem.iBSCrimesVisible = 0
	ENDIF
ENDPROC

FUNC BOOL WANTED_SYSTEM_SHOULD_CLEAN_UP(WANTED_SYSTEM_DATA &sWantedSystemData)
		
	UNUSED_PARAMETER(sWantedSystemData)
		
	// If the server has cleaned up, we should too.
	IF WANTED_SYSTEM_GET_SERVER_STATE() = WS_GLOBAL_IDLE
	AND WANTED_SYSTEM_GET_CLIENT_STATE() > WS_GLOBAL_IDLE
		PRINTLN("[WS] WANTED_SYSTEM_SHOULD_CLEAN_UP - Server is in IDLE. Client is not.")
	
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT WANTED_SYSTEM_MONITOR_ACTIVE()
		AND WANTED_SYSTEM_GET_SERVER_STATE() != WS_GLOBAL_IDLE
			PRINTLN("[WS] WANTED_SYSTEM_SHOULD_CLEAN_UP - Trigger no longer active, Server Cleans up")
		
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC


PROC WANTED_SYSTEM_PROCESS_EVENTS(WANTED_SYSTEM_DATA &sWantedSystemData)
	
	INT iEventID
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	STRUCT_ENTITY_DAMAGE_EVENT sei
	PLAYER_INDEX playerID

	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
		
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
	
		SWITCH ThisScriptEvent			
					
			CASE EVENT_NETWORK_SCRIPT_EVENT
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Details, SIZE_OF(Details))
				
				SWITCH Details.Type
					#IF IS_DEBUG_BUILD
					CASE SCRIPT_EVENT_DEBUG_LAUNCH_WANTED_SYSTEM
						WANTED_SYSTEM_PROCESS_LAUNCH_EVENT(iEventID)
					BREAK
					CASE SCRIPT_EVENT_DEBUG_UPDATE_ROLES
						WANTED_SYSTEM_PROCESS_UPDATE_ROLES(iEventID, sWantedSystemData)
					BREAK
					CASE SCRIPT_EVENT_DEBUG_SYNC_WIDGETS
						WANTED_SYSTEM_PROCESS_SYNC_WIDGETS(iEventID)
					BREAK
					#ENDIF
					CASE SCRIPT_EVENT_WANTED_SYSTEM_CRIME
						WANTED_SYSTEM_PROCESS_CRIME_COMMMITTED(iEventID, sWantedSystemData)
					BREAK
					CASE SCRIPT_EVENT_WANTED_SYSTEM_TAGGED_CRIME
						WANTED_SYSTEM_PROCESS_CRIME_HAS_BEEN_TAGGED(iEventID, sWantedSystemData)
					BREAK
					CASE SCRIPT_EVENT_WANTED_SYSTEM_CRIME_LOST
						WANTED_SYSTEM_PROCESS_CRIME_LOST(iEventID, sWantedSystemData)
					BREAK
				ENDSWITCH				
			BREAK
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
			
				IF WANTED_SYSTEM_GET_CLIENT_STATE() = WS_GLOBAL_RUNNING
					IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, sei, SIZE_OF(sei))
						
						// Simply commit a crime if we damage an entity that is not us
						
						IF WANTED_SYSTEM_AM_I_A_CROOK()
						
							IF DOES_ENTITY_EXIST(sei.DamagerIndex)
								IF IS_ENTITY_A_PED(sei.DamagerIndex) 
									IF GET_PED_INDEX_FROM_ENTITY_INDEX(sei.DamagerIndex) = PLAYER_PED_ID()
										IF DOES_ENTITY_EXIST(sei.VictimIndex)
										AND IS_ENTITY_A_PED(sei.VictimIndex) 
											IF GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex) != PLAYER_PED_ID()
												WANTED_SYSTEM_BROADCAST_PLAYER_COMMITTED_CRIME()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(sei.VictimIndex)
						AND IS_ENTITY_A_PED(sei.VictimIndex)
						AND IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex))
							IF sei.VictimDestroyed
								playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(sei.VictimIndex))
								
								IF playerID != INVALID_PLAYER_INDEX()
								AND IS_NET_PLAYER_OK(playerID, FALSE)
									PRINTLN("[WS] WANTED_SYSTEM_PROCESS_EVENTS - Removing a blip for [", GET_PLAYER_NAME(playerID) ,"]")
									WANTED_SYSTEM_REMOVE_PED_BLIP(NATIVE_TO_INT(playerID), sWantedSystemData)
									WANTED_SYSTEM_CLEANUP_CRIME(NATIVE_TO_INT(playerID), sWantedSystemData)
									SET_BIT(sWantedSystemData.iBlipToManage, NATIVE_TO_INT(playerID))
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE EVENT_NETWORK_PLAYER_JOIN_SCRIPT
			CASE EVENT_NETWORK_PLAYER_LEFT_SCRIPT
				WANTED_SYSTEM_REFRESH_PLAYER_WIDGETS(sWantedSystemData)
			BREAK
											
		ENDSWITCH
	ENDREPEAT
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC WANTED_SYSTEM_UPDATE_WIDGETS(WANTED_SYSTEM_DATA &sWantedSystemData)
	
	#IF IS_DEBUG_BUILD
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_EnableWantedSystem")
		EXIT
	ENDIF
	#ENDIF
	
	// Debug way to launch the system
	IF sWantedSystemData.bBeginSystem
		
		WANTED_SYSTEM_BROADCAST_LAUNCH_EVENT(TRUE, FALSE)
	
		sWantedSystemData.bBeginSystem = FALSE
	ENDIF
	
	// Debug way to end the system
	IF sWantedSystemData.bEndSystem
		
		WANTED_SYSTEM_BROADCAST_LAUNCH_EVENT(FALSE, TRUE)
	
		sWantedSystemData.bEndSystem = FALSE
	ENDIF	
	
	// Debug way to update our roles
	IF sWantedSystemData.bApplyRoles
	
		INT i
		INT iServerBS
		REPEAT NUM_NETWORK_PLAYERS i
			IF sWantedSystemData.bCops[i]
				SET_BIT(iServerBS, i)
			ENDIF
		ENDREPEAT
		
		WANTED_SYSTEM_BROADCAST_ROLE_UPDATE(iServerBS)
		
		sWantedSystemData.bApplyRoles = FALSE
	ENDIF
	
	// Debug way to trigger a crime as a criminal
	IF sWantedSystemData.bTriggerCrime
	OR IS_DEBUG_KEY_JUST_PRESSED(KEY_Y, KEYBOARD_MODIFIER_CTRL, "Commit Wanted System Crime")
		IF WANTED_SYSTEM_AM_I_A_CROOK()
			WANTED_SYSTEM_BROADCAST_PLAYER_COMMITTED_CRIME()
			sWantedSystemData.bTriggerCrime = FALSE
		ENDIF
	ENDIF
	
	IF sWantedSystemData.bSyncWidgets
		
		WANTED_SYSTEM_BROADCAST_SYNC_WIDGETS()
		
		sWantedSystemData.bSyncWidgets = FALSE
	ENDIF
	
	
ENDPROC
#ENDIF

PROC WANTED_SYSTEM_MAINTAIN_MAIN_LOOP(WANTED_SYSTEM_DATA &sWantedSystemData)

	#IF IS_DEBUG_BUILD
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_EnableWantedSystem")
		EXIT
	ENDIF
	#ENDIF

	// Cleanup this wanted system if we are bailing
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		WANTED_SYSTEM_SCRIPT_CLEANUP(sWantedSystemData)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		WANTED_SYSTEM_UPDATE_WIDGETS(sWantedSystemData)
	#ENDIF
	
	// Process our events
	WANTED_SYSTEM_PROCESS_EVENTS(sWantedSystemData)

	// Process our client states
	WANTED_SYSTEM_PROCESS_CLIENT(sWantedSystemData)
	
	// Process our server states
	WANTED_SYSTEM_PROCESS_SERVER(sWantedSystemData)
	
	IF WANTED_SYSTEM_SHOULD_CLEAN_UP(sWantedSystemData)
		WANTED_SYSTEM_SCRIPT_CLEANUP(sWantedSystemData)
		EXIT
	ENDIF
ENDPROC





// End of DEBUG and FEATURE directive (should never be included in main project)
#ENDIF
#ENDIF
