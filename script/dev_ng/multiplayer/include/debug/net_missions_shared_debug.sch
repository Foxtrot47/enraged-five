// This header shouldn't be in the release project anyway, but wrap it up just in case
#IF IS_DEBUG_BUILD



USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "net_debug.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Missions_Shared_Debug.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all MP Missiosn Shared debug functionality.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      MP Console Log Shared Missions Output Routines
// ===========================================================================================================

// PURPOSE:	Output pending activities details on one line
//
// INPUT PARAMS:		paramCoords				The Activity Location
//						paramContact			The Activity Contact
//						paramRank				The Activity Rank
// REFERENCE PARAMS:	refMissionIdData		The MissionID Data
PROC Debug_Output_Pending_Activities_Details_Passed_As_Parameters_On_One_Line(MP_MISSION_ID_DATA &refMissionIdData, VECTOR paramCoords, enumCharacterList paramContact, INT paramRank)

	NET_PRINT(refMissionIdData.idCloudFilename)
	NET_PRINT(" [Creator: ")
	NET_PRINT_INT(refMissionIdData.idCreator)
	NET_PRINT(" (Var: ")
	NET_PRINT_INT(refMissionIdData.idVariation)
	NET_PRINT(")]  [Rank: ")
	NET_PRINT_INT(paramRank)
	NET_PRINT("]  [")
	NET_PRINT_VECTOR(paramCoords)
	NET_PRINT("]    - CONTACT: ")
	IF (paramContact = NO_CHARACTER)
		NET_PRINT("none")
	ELSE
		NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[paramContact].label))
	ENDIF
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output pending activities details from an array slot
//
// INPUT PARAMS:		paramSlot			The slot to take the data from
PROC Debug_Output_Pending_Activities_Slot_Details_On_One_Line(INT paramSlot)

	NET_PRINT("Slot: ")
	NET_PRINT_INT(paramSlot)
	IF (GlobalServerBD_MissionsShared.activities[paramSlot].paInUse)
		NET_PRINT(" [IN USE] ")
	ELSE
		NET_PRINT(" [empty]  ")
	ENDIF
	
	MP_MISSION_ID_DATA	theMissionIdData	= GlobalServerBD_MissionsShared.activities[paramSlot].paMissionIdData
	VECTOR				theLocation			= GlobalServerBD_MissionsShared.activities[paramSlot].paLocation
	enumCharacterList	theContact			= GlobalServerBD_MissionsShared.activities[paramSlot].paContact
	INT					theRank				= GlobalServerBD_MissionsShared.activities[paramSlot].paRank
	
	Debug_Output_Pending_Activities_Details_Passed_As_Parameters_On_One_Line(theMissionIdData, theLocation, theContact, theRank)
	
	// Tag on the Group Slot for stored activities
	NET_PRINT("    (GROUP SLOT: ")
	NET_PRINT_INT(GlobalServerBD_MissionsShared.activities[paramSlot].paGroupSlot)
	NET_PRINT(")")

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output Pending Activities array
PROC Debug_Output_Pending_Activities()

	INT	tempLoop	= 0
	INT	numInUse	= 0
	
	NET_PRINT("...KGM MP [MShared]: Pending Activities Details") NET_NL()
	
	REPEAT MAX_PENDING_ACTIVITIES tempLoop
		IF (GlobalServerBD_MissionsShared.activities[tempLoop].paInUse)
			NET_PRINT("         ")
			Debug_Output_Pending_Activities_Slot_Details_On_One_Line(tempLoop)
			NET_NL()
		
			numInUse++
		ENDIF
	ENDREPEAT
	
	NET_PRINT("         TOTAL IN USE: ")
	NET_PRINT_INT(numInUse)
	NET_PRINT(" [MAX: ")
	NET_PRINT_INT(MAX_PENDING_ACTIVITIES)
	NET_PRINT("]")
	NET_NL()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output pending activities groups array
PROC Debug_Output_Pending_Activities_Groups()

	INT		tempLoop	= 0
	BOOL	foundEntry	= FALSE
	
	NET_NL()
	NET_PRINT("...KGM MP [MShared]: Pending Activities Groups Details")
	
	REPEAT MAX_PENDING_ACTIVITIES_GROUPS tempLoop
		IF (GlobalServerBD_MissionsShared.groups[tempLoop].msgNumInGroup > 0)
			IF NOT (foundEntry)
				foundEntry = TRUE
				NET_NL()
			ENDIF
			
			NET_PRINT("         ")
			NET_PRINT_INT(tempLoop)
			NET_PRINT(":      ")
			NET_PRINT_VECTOR(GlobalServerBD_MissionsShared.groups[tempLoop].msgGroupLocation)
			NET_PRINT("  - Total Missions: ")
			NET_PRINT_INT(GlobalServerBD_MissionsShared.groups[tempLoop].msgNumInGroup)
			NET_NL()
		ENDIF
	ENDREPEAT
	
	IF NOT (foundEntry)
		NET_PRINT(": <NONE>") NET_NL()
	ENDIF

ENDPROC




// ===========================================================================================================
//      MP Console Log Shared Missions Cloud Data Output Routines
// ===========================================================================================================

// PURPOSE:	Output the whole Mission Shared Cloud Data array
//
// INPUT PARAMS:		paramWholeArray				[DEFAULT = FALSE] TRUE if the whole array should be output, FALSE if only 'In Use' elements should be output
PROC Debug_Output_Missions_Shared_Cloud_Data_Array(BOOL paramWholeArray = FALSE)

	INT		tempLoop		= 0
	BOOL	outputThisSlot	= FALSE
	BOOL	inUse			= FALSE
	BOOL	anythingOutput	= FALSE
	
	REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA tempLoop
		outputThisSlot	= paramWholeArray
		inUse			= GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscInUse
		
		IF NOT (paramWholeArray)
			outputThisSlot = inUse
		ENDIF
		
		IF (outputThisSlot)
			NET_PRINT("         Slot: ")
			NET_PRINT_INT(tempLoop)
			NET_PRINT("  ")
			IF (inUse)
				NET_PRINT("[IN USE]")
			ELSE
				NET_PRINT("[NOT IN USE]")
			ENDIF
			IF (GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscBeenUsed)
				NET_PRINT("[BEEN USED]")
			ELSE
				NET_PRINT("[NOT BEEN USED]")
			ENDIF
			IF (inUse)
				NET_PRINT(":   Filename: ")
				NET_PRINT(GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscHeaderData.tlName)
				NET_PRINT("   UniqueID: ")
				NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscUniqueID)
				NET_PRINT("   Player Bits: ")
				IF (GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscPlayersReg = ALL_PLAYER_BITS_CLEAR)
					NET_PRINT("[NO PLAYERS]")
				ELSE
					NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscPlayersReg)
				ENDIF
			ENDIF
			NET_NL()
			
			anythingOutput = TRUE
		ENDIF
	ENDREPEAT
	
	IF NOT (anythingOutput)
		NET_PRINT("         <NONE>") NET_NL()
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the full details of a Missions Shared Cloud Data struct
//
// REFERENCE PARAMS:		refMSCloudData			A reference to a Mission Shared Cloud Data struct's contents
PROC Debug_Output_Cloud_Header_Data(g_structMSCloudHeaderData &refMSCloudData)

	NET_PRINT("      Cloud Filename  : ")	NET_PRINT(refMSCloudData.tlName)				NET_NL()
	NET_PRINT("      Owner           : ")	NET_PRINT(refMSCloudData.tlOwner )				NET_NL()
	NET_PRINT("      Mission Name    : ")	NET_PRINT(refMSCloudData.tlMissionName)			NET_NL()
	NET_PRINT("      Mission Desc    : ")	NET_PRINT(refMSCloudData.tlMissionDec)			NET_NL()
	NET_PRINT("      Start Pos       : ")	NET_PRINT_VECTOR(refMSCloudData.vStartPos)		NET_NL()
	NET_PRINT("      Camera Pos      : ")	NET_PRINT_VECTOR(refMSCloudData.vCamPos)		NET_NL()
	NET_PRINT("      Camera Heading  : ")	NET_PRINT_VECTOR(refMSCloudData.vCamHead)		NET_NL()
	NET_PRINT("      FM Type         : ")	NET_PRINT_INT(refMSCloudData.iType)				NET_NL()
	NET_PRINT("      FM Subtype      : ")	NET_PRINT_INT(refMSCloudData.iSubtype)			NET_NL()
	NET_PRINT("      Min Players     : ")	NET_PRINT_INT(refMSCloudData.iMinPlayers)		NET_NL()
	NET_PRINT("      Rank            : ")	NET_PRINT_INT(refMSCloudData.iRank)				NET_NL()
	NET_PRINT("      Max Players     : ")	NET_PRINT_INT(refMSCloudData.iMaxPlayers)		NET_NL()
	NET_PRINT("      Rating          : ")	NET_PRINT_INT(refMSCloudData.iRating)			NET_NL()
	NET_PRINT("      Les Rating      : ")	NET_PRINT_INT(refMSCloudData.iLesRating)		NET_NL()
	NET_PRINT("      Contact (as INT): ")	NET_PRINT_INT(refMSCloudData.iContactCharEnum)	NET_NL()
	NET_PRINT("      Bitset          : ")	NET_PRINT_INT(refMSCloudData.iBitSet)			NET_NL()
	NET_PRINT("      Description Hash: ")	NET_PRINT_INT(refMSCloudData.iMissionDecHash)	NET_NL()

ENDPROC






#ENDIF	// IS_DEBUG_BUILD

