USING "SceneTool_debug.sch"

FUNC STRING Private_Get_Net_Yacht_Name(INT iYachtID)
	TEXT_LABEL_63 str = "YACHT_"
	str += iYachtID
	RETURN GET_STRING_FROM_STRING(str, 0, GET_LENGTH_OF_LITERAL_STRING(str))
ENDFUNC

PROC Private_Save_NET_YACHT_SCENE(STRUCT_NET_YACHT_SCENE& scene, INT iYachtID)

	SceneTool_OpenDebugFile("Private_Get_NET_YACHT_SCENE()",				Private_Get_Net_Yacht_Name(iYachtID))

		SceneTool_ExportPan(	"NET_YACHT_SCENE_PAN_establishing",		scene.mPans[NET_YACHT_SCENE_PAN_establishing])

	//	SceneTool_ExportCut(	"NET_YACHT_SCENE_CUT_null",				scene.mCuts[NET_YACHT_SCENE_CUT_null])

	//	SceneTool_ExportMarker(	"net_YACHT_scene_MARKER_playerWalk0",	scene.mMarkers[net_YACHT_scene_MARKER_playerWalk0])
	//	SceneTool_ExportMarker(	"net_YACHT_scene_MARKER_playerWalk1",	scene.mMarkers[net_YACHT_scene_MARKER_playerWalk1])
	//	SceneTool_ExportMarker(	"net_YACHT_scene_MARKER_playerWalk2",	scene.mMarkers[net_YACHT_scene_MARKER_playerWalk2])

	//	SceneTool_ExportPlacer(	"NET_YACHT_SCENE_PLACER_playerPos",		scene.mPlacers[NET_YACHT_SCENE_PLACER_playerPos])

	//	SceneTool_ExportBool(	"bEnablePans[0]",				scene.bEnablePans[0])
		
		SceneTool_ExportFloat(	"fExitDelay",					scene.fExitDelay)

	SceneTool_CloseDebugFile("Private_Get_NET_YACHT_SCENE()")
	
	
ENDPROC


PROC Private_DebugPlay_NET_YACHT_SCENE(STRUCT_NET_YACHT_SCENE& scene, SceneYachtFunc customYachtCutscene)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT iCutStage = 0
		CAMERA_INDEX hCam0, hCam1
		
		WHILE NOT CALL customYachtCutscene(iCutStage, scene, hCam0, hCam1, TRUE)
			WAIT(0)
		ENDWHILE
		
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF

ENDPROC

PROC Private_Edit_NET_YACHT_SCENE(structSceneTool_Launcher& launcher, INT iYachtID, STRUCT_NET_YACHT_SCENE &scene, SceneYachtFunc customYachtCutscene, WIDGET_GROUP_ID id)

	// Get scene data
	
	//structSceneTool tool
	
	// bOutputMissingNetRealtyScenes
	
	// Add widgets
	SET_CURRENT_WIDGET_GROUP(id)
	START_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool, Private_Get_Net_Yacht_Name(iYachtID))

		START_WIDGET_GROUP("Camera shots")
			START_WIDGET_GROUP(		"Establishing shot")
				ADD_WIDGET_PAN(		"Establish - ",				g_sAmMpPropertyExtSceneTool,scene.mPans,					ENUM_TO_INT(NET_YACHT_SCENE_PAN_establishing))

			STOP_WIDGET_GROUP()
			ADD_WIDGET_FLOAT_SLIDER("fExitDelay",				scene.fExitDelay, 0.0, 10.0, 0.1)
		STOP_WIDGET_GROUP()
		
	//	START_WIDGET_GROUP("Marker")
	//		ADD_WIDGET_MARKER(		"Player Walk 0",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,				ENUM_TO_INT(net_YACHT_scene_MARKER_playerWalk0))
	//		ADD_WIDGET_MARKER(		"Player Walk 1",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,				ENUM_TO_INT(net_YACHT_scene_MARKER_playerWalk1))
	//		ADD_WIDGET_MARKER(		"Player Walk 2",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,				ENUM_TO_INT(net_YACHT_scene_MARKER_playerWalk2))
	//	STOP_WIDGET_GROUP()
	//
	//	START_WIDGET_GROUP("Placer")
	//		ADD_WIDGET_PLACER(		"PLACER_playerPos",			g_sAmMpPropertyExtSceneTool,scene.mPlacers,				ENUM_TO_INT(net_YACHT_scene_PLACER_playerPos))
	//	STOP_WIDGET_GROUP()
		
	//	START_WIDGET_GROUP("*sliders*")
	//		START_WIDGET_GROUP("Marker")
	//			START_WIDGET_GROUP("Player Walk 0")
	//				ADD_WIDGET_VECTOR_SLIDER(	"Player Walk 0", scene.mMarkers[net_YACHT_scene_MARKER_playerWalk0].vPos, -6000, 6000, 0.01)
	//			STOP_WIDGET_GROUP()
	//			START_WIDGET_GROUP("Player Walk 1")
	//				ADD_WIDGET_VECTOR_SLIDER(	"Player Walk 1", scene.mMarkers[net_YACHT_scene_MARKER_playerWalk1].vPos, -6000, 6000, 0.01)
	//			STOP_WIDGET_GROUP()
	//			START_WIDGET_GROUP("Player Walk 2")
	//				ADD_WIDGET_VECTOR_SLIDER(	"Player Walk 2", scene.mMarkers[net_YACHT_scene_MARKER_playerWalk2].vPos, -6000, 6000, 0.01)
	//			STOP_WIDGET_GROUP()
	//		STOP_WIDGET_GROUP()
	//		
	//		START_WIDGET_GROUP("Placer")
	//			START_WIDGET_GROUP("PLACER_playerPos")
	//				ADD_WIDGET_VECTOR_SLIDER(	"PLACER_playerPos",	scene.mPlacers[net_YACHT_scene_PLACER_playerPos].vPos, -6000, 6000, 0.01)
	//			STOP_WIDGET_GROUP()
	//		STOP_WIDGET_GROUP()
	//	STOP_WIDGET_GROUP()
		
	STOP_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool)
	CLEAR_CURRENT_WIDGET_GROUP(id)
	
	// Warp to player pos marker
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[net_YACHT_scene_PLACER_playerPos].vPos)
//	ENDIF
	
	// Update widgets
	WHILE launcher.bEnableTool
	
		// Update system
		SWITCH SceneTool_UpdateTool(g_sAmMpPropertyExtSceneTool)
			
			CASE SCENETOOL_ACTION_RunScene
				Private_DebugPlay_NET_YACHT_SCENE(scene, customYachtCutscene)
			BREAK
			
			CASE SCENETOOL_ACTION_SaveScene
				Private_Save_NET_YACHT_SCENE(scene, iYachtID)
			BREAK
			
			CASE SCENETOOL_ACTION_UpdateData
				SceneTool_UpdatePans(g_sAmMpPropertyExtSceneTool, scene.mPans)
			//	SceneTool_UpdateCuts(g_sAmMpPropertyExtSceneTool, scene.mCuts)
			//	SceneTool_UpdateMarkers(g_sAmMpPropertyExtSceneTool, scene.mMarkers)
			//	SceneTool_UpdatePlacers(g_sAmMpPropertyExtSceneTool, scene.mPlacers)
			BREAK
		
		ENDSWITCH
				
	//	// Draw markers
	//	SceneTool_DrawMarker2(g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(net_YACHT_scene_MARKER_playerWalk0),	0, 255, 175,		scene.mPlacers[net_YACHT_scene_PLACER_playerPos].vPos)
	//	SceneTool_DrawMarker2(g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(net_YACHT_scene_MARKER_playerWalk1),	200,200,200,		scene.mMarkers[net_YACHT_scene_MARKER_playerWalk0].vPos)
	//	SceneTool_DrawMarker2(g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(net_YACHT_scene_MARKER_playerWalk2),	200,200,200,		scene.mMarkers[net_YACHT_scene_MARKER_playerWalk1].vPos)
	//
	//	// Draw placers
	//	SceneTool_DrawPlacer (g_sAmMpPropertyExtSceneTool, scene.mPlacers,	ENUM_TO_INT(net_YACHT_scene_PLACER_playerPos),			255, 0, 75)
		
		WAIT(0)
	ENDWHILE
	
	// Cleanup system
	SWITCH SceneTool_DestroyTool(g_sAmMpPropertyExtSceneTool)
		CASE SCENETOOL_ACTION_SaveScene
			Private_Save_NET_YACHT_SCENE(scene, iYachtID)
		BREAK
	ENDSWITCH



ENDPROC

