#IF IS_DEBUG_BUILD

USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

USING "Tuner\mp_car_meet_ai_vehicles.sch"

STRUCT CAR_MEET_VEHICLE_ADJUSTMENT_WIDGET_OPTIONS
	INT iCurrentVehicle = -1
	INT iPreviousVehicle = -1
	INT iFirstSpecialVeh, iSecondSpecialVeh
	BOOL bAdd35Y= FALSE, bSubtract35Y = FALSE
	BOOL bShouldVehicleDoorsBeOpen = FALSE
	BOOL bShouldVehicleDoorsBeCLosed = FALSE
	BOOL bBlipVehicle = FALSE
	BOOL bTurnOffColliders = FALSE
	BOOL bHealVehicle = FALSE
	BOOL bRenderVehInfo = FALSE
	INT iModelIndex
	FLOAT fCurrentX = -2224.0, fCurrentY = 1065.0, fCurrentZ = -40.862, fCurrentHeading = 180.0
ENDSTRUCT

STRUCT CAR_MEET_VEHICLE_ADJUSTMENT_DATA
	//AI VEHICLES
	INT iAIVehicleModels[ciCAR_MEET_NUMER_OF_AI_VEHICLES]
	CAR_MEET_VEHICLE_ADJUSTMENT_WIDGET_OPTIONS sAIWidgetOpts
	//PLAYER VEHICLES
	BOOL bSpawnDummyPlayerVehicles = FALSE
	BOOL bRemoveDummyPlayerVehicles = FALSE
	INT iPlayerVehicleModels[ciCAR_MEET_NUMBER_OF_PLAYER_VEHICLES]
	VEHICLE_INDEX viDummyPlayerVehicles[ciCAR_MEET_NUMBER_OF_PLAYER_VEHICLES]
	CAR_MEET_VEHICLE_ADJUSTMENT_WIDGET_OPTIONS sPWidgetOpts
	//GENERAL
	BOOL bWerePlayerVehiclesSpawned = FALSE
	BOOL bWerePlayerVehiclesRemoved = FALSE
	BLIP_INDEX biCurrentVehicle
	BOOL bPrintResults = FALSE
	VECTOR vDummyPosition[ciCAR_MEET_NUMBER_OF_PLAYER_VEHICLES]
	FLOAT fDummyHeading[ciCAR_MEET_NUMBER_OF_PLAYER_VEHICLES]
ENDSTRUCT

PROC CREATE_VEHICLE_POSITION_ADJUSTMENT_WIDGET(CAR_MEET_VEHICLE_ADJUSTMENT_DATA &sVehicleAdjustmentWidgetData)
START_WIDGET_GROUP("Vehicle Positions Adjustment Tool")

	ADD_WIDGET_BOOL("OUTPUT DATA",  sVehicleAdjustmentWidgetData.bPrintResults)	
	ADD_WIDGET_BOOL("RENDER VEHICLE INFO " , sVehicleAdjustmentWidgetData.sAIWidgetOpts.bRenderVehInfo)
	ADD_WIDGET_BOOL("BLIP VEHICLE" , sVehicleAdjustmentWidgetData.sAIWidgetOpts.bBlipVehicle)
	ADD_WIDGET_BOOL("HEAL VEHICLE" , sVehicleAdjustmentWidgetData.sAIWidgetOpts.bHealVehicle)
	ADD_WIDGET_INT_SLIDER("VEHICLE INDEX", sVehicleAdjustmentWidgetData.sAIWidgetOpts.iCurrentVehicle , -1, 49, 1)	
	
	ADD_WIDGET_FLOAT_SLIDER("POSITION X: ",sVehicleAdjustmentWidgetData.sAIWidgetOpts.fCurrentX, -2224.0, -2140, 0.1 )
	ADD_WIDGET_FLOAT_SLIDER("POSITION Y: ",sVehicleAdjustmentWidgetData.sAIWidgetOpts.fCurrentY, 1065.0, 1220.0 , 0.1)
	ADD_WIDGET_BOOL(" +3.5 on Y axis:", sVehicleAdjustmentWidgetData.sAIWidgetOpts.bAdd35Y)
	ADD_WIDGET_BOOL(" -3.5 on Y axis:", sVehicleAdjustmentWidgetData.sAIWidgetOpts.bSubtract35Y)
	ADD_WIDGET_FLOAT_SLIDER("POSITION Z: ",sVehicleAdjustmentWidgetData.sAIWidgetOpts.fCurrentZ, -40.862, -20.862 , 0.1)
	ADD_WIDGET_FLOAT_SLIDER("HEADING ", sVehicleAdjustmentWidgetData.sAIWidgetOpts.fCurrentHeading, 0.0, 360.0, 0.1)
	ADD_WIDGET_BOOL("OPEN DOORS " , sVehicleAdjustmentWidgetData.sAIWidgetOpts.bShouldVehicleDoorsBeOpen)
	ADD_WIDGET_BOOL("CLOSE DOORS " , sVehicleAdjustmentWidgetData.sAIWidgetOpts.bShouldVehicleDoorsBeCLosed)
	INT i
	START_NEW_WIDGET_COMBO()
	REPEAT CM_VEH_COUNT i
		ADD_TO_WIDGET_COMBO(CAR_MEET_VEHICLE_INT_TO_STRING(i))
	ENDREPEAT
	ADD_TO_WIDGET_COMBO("SLAMTRUCK")
	ADD_TO_WIDGET_COMBO("SANDKING")
	STOP_WIDGET_COMBO("Model", sVehicleAdjustmentWidgetData.sAIWidgetOpts.iModelIndex)

STOP_WIDGET_GROUP()

START_WIDGET_GROUP("Payer Vehicle Positions Adjustment Tool")

	START_NEW_WIDGET_COMBO()
	REPEAT CM_VEH_COUNT i
		ADD_TO_WIDGET_COMBO(CAR_MEET_VEHICLE_INT_TO_STRING(i))	
	ENDREPEAT
	ADD_TO_WIDGET_COMBO("SLAMTRUCK")
	ADD_TO_WIDGET_COMBO("SANDKING")
	STOP_WIDGET_COMBO("Model", sVehicleAdjustmentWidgetData.sPWidgetOpts.iModelIndex)
	ADD_WIDGET_BOOL("SPAWN VEHICLES", sVehicleAdjustmentWidgetData.bSpawnDummyPlayerVehicles)
	ADD_WIDGET_BOOL("REMOVE VEHICLES (preserves positions and headings for respawning)", sVehicleAdjustmentWidgetData.bRemoveDummyPlayerVehicles)
	
	ADD_WIDGET_BOOL("RENDER PLAYER VEHICLE INFO " , sVehicleAdjustmentWidgetData.sPWidgetOpts.bRenderVehInfo)
	ADD_WIDGET_BOOL("BLIP VEHICLE" , sVehicleAdjustmentWidgetData.sPWidgetOpts.bBlipVehicle)
	ADD_WIDGET_BOOL("HEAL VEHICLE" , sVehicleAdjustmentWidgetData.sPWidgetOpts.bHealVehicle)
	ADD_WIDGET_INT_SLIDER("VEHICLE INDEX", sVehicleAdjustmentWidgetData.sPWidgetOpts.iCurrentVehicle , -1, 49, 1)	
	
	ADD_WIDGET_FLOAT_SLIDER("POSITION X: ",sVehicleAdjustmentWidgetData.sPWidgetOpts.fCurrentX, -2224.0, -2140, 0.1 )
	ADD_WIDGET_FLOAT_SLIDER("POSITION Y: ",sVehicleAdjustmentWidgetData.sPWidgetOpts.fCurrentY, 1065, 1220.0 , 0.1)
	ADD_WIDGET_BOOL(" +3.5 on Y axis:", sVehicleAdjustmentWidgetData.sPWidgetOpts.bAdd35Y)
	ADD_WIDGET_BOOL(" -3.5 on Y axis:", sVehicleAdjustmentWidgetData.sPWidgetOpts.bSubtract35Y)
	ADD_WIDGET_FLOAT_SLIDER("POSITION Z: ",sVehicleAdjustmentWidgetData.sPWidgetOpts.fCurrentZ, -40.862, -20.862 , 0.1)
	ADD_WIDGET_FLOAT_SLIDER("HEADING ", sVehicleAdjustmentWidgetData.sPWidgetOpts.fCurrentHeading, 0.0, 360.0, 0.1)
	ADD_WIDGET_BOOL("OPEN DOORS " , sVehicleAdjustmentWidgetData.sPWidgetOpts.bShouldVehicleDoorsBeOpen)
	ADD_WIDGET_BOOL("CLOSE DOORS ", sVehicleAdjustmentWidgetData.sPWidgetOpts.bShouldVehicleDoorsBeClosed)

	
	
STOP_WIDGET_GROUP()
ENDPROC


PROC INITIALISE_VEHICLE_POSITION_ADJUSTMENT_WIDGET(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData, AI_VEHICLE_DATA &sVehicleData, CAR_MEET_VEHICLE_ADJUSTMENT_DATA &sVehicleAdjustmentWidgetData )
	
	INT i
	INT iCurrentSpecialPedVehicle = 1
	REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES i
		IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[i])					
			sVehicleAdjustmentWidgetData.iAIVehicleModels[i] = ENUM_TO_INT(sVehicleData.sAIVehicle[i].eModel)
		ELSE	
			sVehicleAdjustmentWidgetData.iAIVehicleModels[i] = -1
			IF iCurrentSpecialPedVehicle = 1
				sVehicleAdjustmentWidgetData.sAIWidgetOpts.iFirstSpecialVeh = i
			ELSE
				sVehicleAdjustmentWidgetData.sAIWidgetOpts.iSecondSpecialVeh = i
			ENDIF
			iCurrentSpecialPedVehicle++		
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SPAWN_DUMMY_VEHICLES(CAR_MEET_VEHICLE_ADJUSTMENT_DATA &sVehicleAdjustmentWidgetData, BOOL bAreVehiclesBeingReplaced = FALSE)
	VECTOR vPosition
	FLOAT fHeading
	INT i
	VEHICLE_SETUP_STRUCT_MP sData
	REPEAT ciCAR_MEET_NUMBER_OF_PLAYER_VEHICLES i
		IF NOT DOES_ENTITY_EXIST(sVehicleAdjustmentWidgetData.viDummyPlayerVehicles[i])
			IF sVehicleAdjustmentWidgetData.sPWidgetOpts.iModelIndex < ENUM_TO_INT(CM_VEH_COUNT)
				GET_AI_VEHICLE_DATA(INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, sVehicleAdjustmentWidgetData.sPWidgetOpts.iModelIndex),GET_RANDOM_INT_IN_RANGE(0,5), sData)
			ELIF  sVehicleAdjustmentWidgetData.sPWidgetOpts.iModelIndex = ENUM_TO_INT(CM_VEH_COUNT)
				sData.VehicleSetup.eModel = SLAMTRUCK
			ELSE
				sData.VehicleSetup.eModel = SANDKING
			ENDIF		
			IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)	
				IF NOT bAreVehiclesBeingReplaced
					GET_CAR_MEET_PARKING_SPOT_LOCATION(i, vPosition, fHeading)
				ELSE
					vPosition = sVehicleAdjustmentWidgetData.vDummyPosition[i]
					fHeading = sVehicleAdjustmentWidgetData.fDummyHeading[i]
				ENDIF
			
			    sVehicleAdjustmentWidgetData.iPlayerVehicleModels[i]= sVehicleAdjustmentWidgetData.sPWidgetOpts.iModelIndex
		
				sVehicleAdjustmentWidgetData.viDummyPlayerVehicles[i] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vPosition, fHeading, FALSE)
				RETURN FALSE // ONE VEHICLE PER FRAME
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE //ALL VEHICLES SPAWNED
ENDFUNC
FUNC BOOL REMOVE_DUMMY_VEHICLES(CAR_MEET_VEHICLE_ADJUSTMENT_DATA &sVehicleAdjustmentWidgetData)
	INT i	
	REPEAT ciCAR_MEET_NUMBER_OF_PLAYER_VEHICLES i
		IF DOES_ENTITY_EXIST(sVehicleAdjustmentWidgetData.viDummyPlayerVehicles[i])				
			sVehicleAdjustmentWidgetData.vDummyPosition[i] = GET_ENTITY_COORDS(sVehicleAdjustmentWidgetData.viDummyPlayerVehicles[i])
			sVehicleAdjustmentWidgetData.fDummyHeading[i] = GET_ENTITY_HEADING(sVehicleAdjustmentWidgetData.viDummyPlayerVehicles[i])
			DELETE_VEHICLE(sVehicleAdjustmentWidgetData.viDummyPlayerVehicles[i])
			RETURN FALSE // ONE VEHICLE PER FRAME
		ENDIF
	ENDREPEAT
	
	RETURN TRUE // all vehicles are deleted
ENDFUNC

PROC RENDER_VEHICLE_INFO(CAR_MEET_VEHICLE_ADJUSTMENT_DATA &sVehicleAdjustmentWidgetData, INT i, VECTOR vPosition, FLOAT fHeading, BOOL bPlayerVehicle = FALSE)

	
	HUD_COLOURS HudColour
	
	INT iRow = 0
	INT iAlpha = 255
	INT iRed, iGreen, iBlue
	FLOAT fRowZ = 0.2
	TEXT_LABEL_63 str
	
	GET_HUD_COLOUR(HUD_COLOUR_PURPLELIGHT, iRed, iGreen, iBlue, iAlpha)
	
	IF (VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vPosition) <= cfCAR_MEET_MAX_VEHICLE_DATA_RENDER_DISTANCE OR VDIST(GET_FINAL_RENDERED_CAM_COORD(), vPosition) <= cfCAR_MEET_MAX_VEHICLE_DATA_RENDER_DISTANCE)		
		iRow++
		str = "Heading: "
		str += FLOAT_TO_STRING(fHeading)
		DRAW_DEBUG_TEXT(str, <<vPosition.x, vPosition.y, vPosition.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "Coords: "
		str += VECTOR_TO_STRING(vPosition)
		DRAW_DEBUG_TEXT(str, <<vPosition.x, vPosition.y, vPosition.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "Model: "
		IF bPlayerVehicle
			str += CAR_MEET_VEHICLE_INT_TO_STRING(sVehicleAdjustmentWidgetData.iPlayerVehicleModels[i])
		ELSE
			str += CAR_MEET_VEHICLE_INT_TO_STRING(sVehicleAdjustmentWidgetData.iAIVehicleModels[i])
		ENDIF
		DRAW_DEBUG_TEXT(str, <<vPosition.x, vPosition.y, vPosition.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "INDEX: "
		str += i		
		DRAW_DEBUG_TEXT(str, <<vPosition.x, vPosition.y, vPosition.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		
		DRAW_DEBUG_SPAWN_POINT(vPosition, fHeading, HudColour, 0.0, 0.5)
		DRAW_DEBUG_TEXT(str, <<vPosition.x, vPosition.y, vPosition.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
	ENDIF
	
ENDPROC

PROC OUTPUT_VEH_DATA(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData, CAR_MEET_VEHICLE_ADJUSTMENT_DATA &sVehicleAdjustmentWidgetData)
	INT i	
	STRING sPath = "X:/gta5/titleupdate/dev_ng/"
	TEXT_LABEL_63 tlFile = "car_meet_vehicle_positions.txt"
	CLEAR_NAMED_DEBUG_FILE(sPath, tlFile)	
	OPEN_NAMED_DEBUG_FILE(sPath, tlFile)	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("POSITIONS:", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES i
			TEXT_LABEL_7 sCase = "CASE "
			sCase+=i
			SAVE_STRING_TO_NAMED_DEBUG_FILE(sCase, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, tlFile)
			IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[i])
				SAVE_VECTOR_TO_NAMED_DEBUG_FILE(GET_ENTITY_COORDS(sAIVehicleCreationData.viVehicles[i]),  sPath, tlFile)	
			ELIF i = sVehicleAdjustmentWidgetData.sAIWidgetOpts.iFirstSpecialVeh
				IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viSpecialPedVehicles[1])
					SAVE_VECTOR_TO_NAMED_DEBUG_FILE(GET_ENTITY_COORDS(sAIVehicleCreationData.viSpecialPedVehicles[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS]),  sPath, tlFile)	
				ENDIF
			ELIF i = sVehicleAdjustmentWidgetData.sAIWidgetOpts.iSecondSpecialVeh
				IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viSpecialPedVehicles[2])
					SAVE_VECTOR_TO_NAMED_DEBUG_FILE(GET_ENTITY_COORDS(sAIVehicleCreationData.viSpecialPedVehicles[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1]),  sPath, tlFile)	
				ENDIF
			ENDIF
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	ENDREPEAT
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("HEADINGS:", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES i
			TEXT_LABEL_7 sCase = "CASE "
			sCase+=i
			SAVE_STRING_TO_NAMED_DEBUG_FILE(sCase, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, tlFile)
			IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[i])
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(GET_ENTITY_HEADING(sAIVehicleCreationData.viVehicles[i]),  sPath, tlFile)	
			ELIF i = sVehicleAdjustmentWidgetData.sAIWidgetOpts.iFirstSpecialVeh
				IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viSpecialPedVehicles[1])
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(GET_ENTITY_HEADING(sAIVehicleCreationData.viSpecialPedVehicles[1]),  sPath, tlFile)	
				ENDIF
			ELIF i = sVehicleAdjustmentWidgetData.sAIWidgetOpts.iSecondSpecialVeh
				IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viSpecialPedVehicles[2])
					SAVE_FLOAT_TO_NAMED_DEBUG_FILE(GET_ENTITY_HEADING(sAIVehicleCreationData.viSpecialPedVehicles[2]),  sPath, tlFile)	
				ENDIF
			ENDIF	
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	ENDREPEAT
	
	
	/// PLAYER VEH DATA
	
	IF sVehicleAdjustmentWidgetData.bWerePlayerVehiclesSpawned
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("@@@@@@@   PLAYER VEHICLES @@@@@@@", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	SAVE_STRING_TO_NAMED_DEBUG_FILE("POSITIONS:", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	REPEAT ciCAR_MEET_NUMBER_OF_PLAYER_VEHICLES i
			TEXT_LABEL_7 sCase = "CASE "
			sCase+=i
			SAVE_STRING_TO_NAMED_DEBUG_FILE(sCase, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, tlFile)
			SAVE_VECTOR_TO_NAMED_DEBUG_FILE(GET_ENTITY_COORDS(sVehicleAdjustmentWidgetData.viDummyPlayerVehicles[i]),  sPath, tlFile)			
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	ENDREPEAT
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("HEADINGS:", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	REPEAT ciCAR_MEET_NUMBER_OF_PLAYER_VEHICLES i
			TEXT_LABEL_7 sCase = "CASE "
			sCase+=i
			SAVE_STRING_TO_NAMED_DEBUG_FILE(sCase, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, tlFile)
			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(GET_ENTITY_HEADING(sVehicleAdjustmentWidgetData.viDummyPlayerVehicles[i]),  sPath, tlFile)			
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	ENDREPEAT
	
	ENDIF
	CLOSE_DEBUG_FILE()
//	SAVE_STRING_TO_NAMED_DEBUG_FILE(" fHeading = ", sPath, tlFile)
//			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(GET_ENTITY_HEADING(NET_TO_ENT(VehDebugScriptManagementData.niVehicles[i])), sPath, tlFile)
ENDPROC
PROC UPDATE_DUMMY_PLAYER_VEHICLES(CAR_MEET_VEHICLE_ADJUSTMENT_DATA &sWidgetData)
	IF sWidgetData.sPWidgetOpts.bAdd35Y
		sWidgetData.sPWidgetOpts.fCurrentY+=3.5
		sWidgetData.sPWidgetOpts.bAdd35Y=FALSE
	ENDIF
	IF sWidgetData.sPWidgetOpts.bSubtract35Y
		sWidgetData.sPWidgetOpts.fCurrentY-=3.5
		sWidgetData.sPWidgetOpts.bSubtract35Y=FALSE
	ENDIF
	
	IF NOT (sWidgetData.sPWidgetOpts.iCurrentVehicle = sWidgetData.sPWidgetOpts.iPreviousVehicle)
	AND sWidgetData.sPWidgetOpts.iCurrentVehicle > -1
		IF sWidgetData.sPWidgetOpts.iPreviousVehicle > -1
			SET_VEHICLE_GRAVITY(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iPreviousVehicle], TRUE)
			SET_ENTITY_COLLISION(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iPreviousVehicle], TRUE)
		ENDIF
		sWidgetData.sPWidgetOpts.iPreviousVehicle = sWidgetData.sPWidgetOpts.iCurrentVehicle
		VECTOR vTemp = GET_ENTITY_COORDS(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle])
		sWidgetData.sPWidgetOpts.fCurrentX = vTemp.x
		sWidgetData.sPWidgetOpts.fCurrentY= vTemp.y
		sWidgetData.sPWidgetOpts.fCurrentZ = vTemp.z
		sWidgetData.sPWidgetOpts.fCurrentHeading = GET_ENTITY_HEADING(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle])
		sWidgetData.sPWidgetOpts.iModelIndex = sWidgetData.iPlayerVehicleModels[sWidgetData.sPWidgetOpts.iCurrentVehicle]
		sWidgetData.sPWidgetOpts.bTurnOffColliders = TRUE
	ENDIF
	
	VECTOR vCurrentSetting = <<sWidgetData.sPWidgetOpts.fCurrentX, sWidgetData.sPWidgetOpts.fCurrentY, sWidgetData.sPWidgetOpts.fCurrentZ>>
	IF sWidgetData.sPWidgetOpts.bRenderVehInfo
		INT i
		REPEAT ciCAR_MEET_NUMBER_OF_PLAYER_VEHICLES i
			IF DOES_ENTITY_EXIST(sWidgetData.viDummyPlayerVehicles[i])	
				RENDER_VEHICLE_INFO(sWidgetData, i,GET_ENTITY_COORDS(sWidgetData.viDummyPlayerVehicles[i]), GET_ENTITY_HEADING(sWidgetData.viDummyPlayerVehicles[i]),  TRUE)
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF sWidgetData.sPWidgetOpts.iCurrentVehicle > -1
		IF DOES_ENTITY_EXIST(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle])	
			IF sWidgetData.sPWidgetOpts.bTurnOffColliders
				sWidgetData.sPWidgetOpts.bTurnOffColliders = FALSE
				SET_VEHICLE_GRAVITY(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle], FALSE)
				SET_ENTITY_COLLISION(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle], FALSE)
			ENDIF
			IF sWidgetData.sPWidgetOpts.bBlipVehicle
				sWidgetData.sPWidgetOpts.bBlipVehicle = FALSE
				IF DOES_BLIP_EXIST(sWidgetData.biCurrentVehicle)
					REMOVE_BLIP(sWidgetData.biCurrentVehicle)
				ENDIF
				CREATE_BLIP_FOR_VEHICLE(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle])
			ENDIF
			IF sWidgetData.sPWidgetOpts.bHealVehicle
				sWidgetData.sPWidgetOpts.bHealVehicle = FALSE
				SET_VEHICLE_FIXED(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle])
			ENDIF
			IF NOT (sWidgetData.iPlayerVehicleModels[sWidgetData.sPWidgetOpts.iCurrentVehicle] = sWidgetData.sPWidgetOpts.iModelIndex)	
				DELETE_VEHICLE(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle])			
			ENDIF
			IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle]), vCurrentSetting)
				SET_ENTITY_COORDS(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle], vCurrentSetting)
			ENDIF
			IF NOT (sWidgetData.sPWidgetOpts.fCurrentHeading = GET_ENTITY_HEADING(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle]))
				SET_ENTITY_HEADING(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle], sWidgetData.sPWidgetOpts.fCurrentHeading )
			ENDIF	
			IF sWidgetData.sPWidgetOpts.bShouldVehicleDoorsBeOpen
				sWidgetData.sPWidgetOpts.bShouldVehicleDoorsBeOpen = FALSE				
				SET_VEHICLE_DOOR_OPEN(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle], SC_DOOR_FRONT_LEFT)
				SET_VEHICLE_DOOR_OPEN(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle], SC_DOOR_FRONT_RIGHT)			
			ENDIF
			IF sWidgetData.sPWidgetOpts.bShouldVehicleDoorsBeClosed
				sWidgetData.sPWidgetOpts.bShouldVehicleDoorsBeClosed = FALSE				
				SET_VEHICLE_DOOR_SHUT(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle], SC_DOOR_FRONT_LEFT)
				SET_VEHICLE_DOOR_SHUT(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle], SC_DOOR_FRONT_RIGHT)			
			ENDIF
		ELIF NOT (sWidgetData.iPlayerVehicleModels[sWidgetData.sPWidgetOpts.iCurrentVehicle] = sWidgetData.sPWidgetOpts.iModelIndex)
			VEHICLE_SETUP_STRUCT_MP sData
			IF sWidgetData.sPWidgetOpts.iModelIndex < ENUM_TO_INT(CM_VEH_COUNT)
				GET_AI_VEHICLE_DATA(INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, sWidgetData.sPWidgetOpts.iModelIndex),GET_RANDOM_INT_IN_RANGE(0,5), sData)
			ELIF sWidgetData.sPWidgetOpts.iModelIndex = ENUM_TO_INT(CM_VEH_COUNT)
				sData.VehicleSetup.eModel = SLAMTRUCK
			ELSE
				sData.VehicleSetup.eModel = SANDKING
			ENDIF
			IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)
				sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vCurrentSetting, sWidgetData.sPWidgetOpts.fCurrentHeading, FALSE)	
				SET_VEHICLE_SETUP(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle],sData.VehicleSetup)
				sWidgetData.iPlayerVehicleModels[sWidgetData.sPWidgetOpts.iCurrentVehicle] = sWidgetData.sPWidgetOpts.iModelIndex
				SET_VEHICLE_GRAVITY(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle], FALSE)
				SET_ENTITY_COLLISION(sWidgetData.viDummyPlayerVehicles[sWidgetData.sPWidgetOpts.iCurrentVehicle], FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)
			ENDIF
		ENDIF		
	ENDIF	
ENDPROC

PROC CHECK_ALL_SWITCHES_FOR_VEHICLE(CAR_MEET_VEHICLE_ADJUSTMENT_DATA &sWidgetData, VEHICLE_INDEX vehicle, BOOL isSpecialPedVehicle = FALSE)

	VECTOR vCurrentSetting = <<sWidgetData.sAIWidgetOpts.fCurrentX, sWidgetData.sAIWidgetOpts.fCurrentY, sWidgetData.sAIWidgetOpts.fCurrentZ>>
	IF sWidgetData.sAIWidgetOpts.bTurnOffColliders
		sWidgetData.sAIWidgetOpts.bTurnOffColliders=FALSE
		SET_VEHICLE_GRAVITY(vehicle, FALSE)
		SET_ENTITY_COLLISION(vehicle, FALSE)				
	ENDIF
	IF sWidgetData.sAIWidgetOpts.bBlipVehicle
		sWidgetData.sAIWidgetOpts.bBlipVehicle = FALSE
		IF DOES_BLIP_EXIST(sWidgetData.biCurrentVehicle)
			REMOVE_BLIP(sWidgetData.biCurrentVehicle)
		ENDIF
		sWidgetData.biCurrentVehicle = CREATE_BLIP_FOR_VEHICLE(vehicle)
	ENDIF
	IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(vehicle), vCurrentSetting)
		SET_ENTITY_COORDS(vehicle, vCurrentSetting)
	ENDIF
	IF NOT (sWidgetData.sAIWidgetOpts.fCurrentHeading = GET_ENTITY_HEADING(vehicle))
		SET_ENTITY_HEADING(vehicle, sWidgetData.sAIWidgetOpts.fCurrentHeading )
	ENDIF	
	IF sWidgetData.sAIWidgetOpts.bHealVehicle
		sWidgetData.sAIWidgetOpts.bHealVehicle = FALSE
		SET_VEHICLE_FIXED(vehicle)
	ENDIF
	IF sWidgetData.sAIWidgetOpts.bShouldVehicleDoorsBeOpen 
		sWidgetData.sAIWidgetOpts.bShouldVehicleDoorsBeOpen = FALSE				
		SET_VEHICLE_DOOR_OPEN(vehicle, SC_DOOR_FRONT_LEFT)
		SET_VEHICLE_DOOR_OPEN(vehicle, SC_DOOR_FRONT_RIGHT)
	ENDIF
	IF sWidgetData.sAIWidgetOpts.bShouldVehicleDoorsBeCLosed			
		sWidgetData.sAIWidgetOpts.bShouldVehicleDoorsBeCLosed = FALSE
		SET_VEHICLE_DOOR_SHUT(vehicle, SC_DOOR_FRONT_LEFT)
		SET_VEHICLE_DOOR_SHUT(vehicle, SC_DOOR_FRONT_RIGHT)
	ENDIF
	IF NOT isSpecialPedVehicle
		IF NOT (sWidgetData.iAIVehicleModels[sWidgetData.sAIWidgetOpts.iCurrentVehicle] = sWidgetData.sAIWidgetOpts.iModelIndex)	
			DELETE_VEHICLE(vehicle)			
		ENDIF
	ENDIF
ENDPROC
PROC UPDATE_VEHICLE_POSITION_ADJUSTMENT_WIDGET(CAR_MEET_VEHICLE_ADJUSTMENT_DATA &sWidgetData, CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)

	IF sWidgetData.bPrintResults
	 	OUTPUT_VEH_DATA(sAIVehicleCreationData, sWidgetData)
		sWidgetData.bPrintResults=FALSE
	ENDIF
	IF sWidgetData.sAIWidgetOpts.bAdd35Y
		sWidgetData.sAIWidgetOpts.fCurrentY+=3.5
		sWidgetData.sAIWidgetOpts.bAdd35Y=FALSE
	ENDIF
	IF sWidgetData.sAIWidgetOpts.bSubtract35Y
		sWidgetData.sAIWidgetOpts.fCurrentY-=3.5
		sWidgetData.sAIWidgetOpts.bSubtract35Y=FALSE
	ENDIF
	
	
	IF sWidgetData.bRemoveDummyPlayerVehicles 
		sWidgetData.bWerePlayerVehiclesRemoved = REMOVE_DUMMY_VEHICLES(sWidgetData)
		sWidgetData.bRemoveDummyPlayerVehicles = NOT sWidgetData.bWerePlayerVehiclesRemoved
	ENDIF
	
	IF sWidgetData.bSpawnDummyPlayerVehicles
		sWidgetData.bWerePlayerVehiclesSpawned = SPAWN_DUMMY_VEHICLES(sWidgetData, sWidgetData.bWerePlayerVehiclesRemoved)
		sWidgetData.bSpawnDummyPlayerVehicles = NOT sWidgetData.bWerePlayerVehiclesSpawned
	ENDIF
	
	
	
		
	IF NOT (sWidgetData.sAIWidgetOpts.iCurrentVehicle = sWidgetData.sAIWidgetOpts.iPreviousVehicle)
	AND sWidgetData.sAIWidgetOpts.iCurrentVehicle > -1
		IF sWidgetData.sAIWidgetOpts.iPreviousVehicle > -1
			IF sWidgetData.sAIWidgetOpts.iPreviousVehicle= sWidgetData.sAIWidgetOpts.iFirstSpecialVeh
				SET_VEHICLE_GRAVITY(sAIVehicleCreationData.viSpecialPedVehicles[1], TRUE)
				SET_ENTITY_COLLISION(sAIVehicleCreationData.viSpecialPedVehicles[1], TRUE)		
			ELIF sWidgetData.sAIWidgetOpts.iPreviousVehicle= sWidgetData.sAIWidgetOpts.iSecondSpecialVeh
				SET_VEHICLE_GRAVITY(sAIVehicleCreationData.viSpecialPedVehicles[2], TRUE)
				SET_ENTITY_COLLISION(sAIVehicleCreationData.viSpecialPedVehicles[2], TRUE)		
			ELSE
				SET_VEHICLE_GRAVITY(sAIVehicleCreationData.viVehicles[sWidgetData.sAIWidgetOpts.iPreviousVehicle], TRUE)
				SET_ENTITY_COLLISION(sAIVehicleCreationData.viVehicles[sWidgetData.sAIWidgetOpts.iPreviousVehicle], TRUE)
			ENDIF
		ENDIF
		sWidgetData.sAIWidgetOpts.iPreviousVehicle = sWidgetData.sAIWidgetOpts.iCurrentVehicle
		VECTOR vTemp
		IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[sWidgetData.sAIWidgetOpts.iCurrentVehicle])
			vTemp = GET_ENTITY_COORDS(sAIVehicleCreationData.viVehicles[sWidgetData.sAIWidgetOpts.iCurrentVehicle])
		ELIF sWidgetData.sAIWidgetOpts.iCurrentVehicle = sWidgetData.sAIWidgetOpts.iFirstSpecialVeh	
			vTemp = GET_ENTITY_COORDS(sAIVehicleCreationData.viSpecialPedVehicles[1])
		ELIF sWidgetData.sAIWidgetOpts.iCurrentVehicle = sWidgetData.sAIWidgetOpts.iSecondSpecialVeh	
			vTemp = GET_ENTITY_COORDS(sAIVehicleCreationData.viSpecialPedVehicles[2])
		ENDIF
		sWidgetData.sAIWidgetOpts.fCurrentX = vTemp.x
		sWidgetData.sAIWidgetOpts.fCurrentY = vTemp.y
		sWidgetData.sAIWidgetOpts.fCurrentZ = vTemp.z
		sWidgetData.sAIWidgetOpts.fCurrentHeading = GET_ENTITY_HEADING(sAIVehicleCreationData.viVehicles[sWidgetData.sAIWidgetOpts.iCurrentVehicle])
		sWidgetData.sAIWidgetOpts.iModelIndex = sWidgetData.iAIVehicleModels[sWidgetData.sAIWidgetOpts.iCurrentVehicle]
		sWidgetData.sAIWidgetOpts.bTurnOffColliders=TRUE
	ENDIF
	VECTOR vCurrentSetting = <<sWidgetData.sAIWidgetOpts.fCurrentX, sWidgetData.sAIWidgetOpts.fCurrentY, sWidgetData.sAIWidgetOpts.fCurrentZ>>
	IF sWidgetData.sAIWidgetOpts.bRenderVehInfo OR sWidgetData.sPWidgetOpts.bRenderVehInfo
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	ENDIF
	IF sWidgetData.sAIWidgetOpts.bRenderVehInfo
		INT i
		REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES i
			IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[i])
				RENDER_VEHICLE_INFO(sWidgetData,i, GET_ENTITY_COORDS(sAIVehicleCreationData.viVehicles[i]), GET_ENTITY_HEADING(sAIVehicleCreationData.viVehicles[i]),  FALSE)
			ELIF sWidgetData.sAIWidgetOpts.iCurrentVehicle = sWidgetData.sAIWidgetOpts.iFirstSpecialVeh	
				RENDER_VEHICLE_INFO(sWidgetData,i, GET_ENTITY_COORDS(sAIVehicleCreationData.viSpecialPedVehicles[1]), GET_ENTITY_HEADING(sAIVehicleCreationData.viSpecialPedVehicles[1]),  FALSE)
			ELIF sWidgetData.sAIWidgetOpts.iCurrentVehicle = sWidgetData.sAIWidgetOpts.iSecondSpecialVeh	
				RENDER_VEHICLE_INFO(sWidgetData,i, GET_ENTITY_COORDS(sAIVehicleCreationData.viSpecialPedVehicles[2]), GET_ENTITY_HEADING(sAIVehicleCreationData.viSpecialPedVehicles[2]),  FALSE)
			ENDIF
		ENDREPEAT
	ENDIF
	IF sWidgetData.sAIWidgetOpts.iCurrentVehicle > -1
		
		IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[sWidgetData.sAIWidgetOpts.iCurrentVehicle])
			CHECK_ALL_SWITCHES_FOR_VEHICLE(sWidgetData, sAIVehicleCreationData.viVehicles[sWidgetData.sAIWidgetOpts.iCurrentVehicle])
		ELIF sWidgetData.sAIWidgetOpts.iCurrentVehicle = sWidgetData.sAIWidgetOpts.iFirstSpecialVeh	
			CHECK_ALL_SWITCHES_FOR_VEHICLE(sWidgetData, sAIVehicleCreationData.viSpecialPedVehicles[1], TRUE)
		ELIF sWidgetData.sAIWidgetOpts.iCurrentVehicle = sWidgetData.sAIWidgetOpts.iSecondSpecialVeh	
			CHECK_ALL_SWITCHES_FOR_VEHICLE(sWidgetData, sAIVehicleCreationData.viSpecialPedVehicles[2], TRUE)
		ELIF NOT (sWidgetData.iAIVehicleModels[sWidgetData.sAIWidgetOpts.iCurrentVehicle] = sWidgetData.sAIWidgetOpts.iModelIndex) // ONLY TRIGGERED AFTER CHANGING MODEL
			VEHICLE_SETUP_STRUCT_MP sData
			IF sWidgetData.sAIWidgetOpts.iModelIndex < ENUM_TO_INT(CM_VEH_COUNT)
				GET_AI_VEHICLE_DATA(INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, sWidgetData.sAIWidgetOpts.iModelIndex),GET_RANDOM_INT_IN_RANGE(0,5), sData)
			ELIF sWidgetData.sAIWidgetOpts.iModelIndex = ENUM_TO_INT(CM_VEH_COUNT)
				sData.VehicleSetup.eModel = SLAMTRUCK
			ELSE
				sData.VehicleSetup.eModel = SANDKING
			ENDIF	
			IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)
				sAIVehicleCreationData.viVehicles[sWidgetData.sAIWidgetOpts.iCurrentVehicle] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vCurrentSetting, sWidgetData.sAIWidgetOpts.fCurrentHeading, FALSE)	
				SET_VEHICLE_SETUP(sAIVehicleCreationData.viVehicles[sWidgetData.sAIWidgetOpts.iCurrentVehicle],sData.VehicleSetup)
				sWidgetData.iAIVehicleModels[sWidgetData.sAIWidgetOpts.iCurrentVehicle] = sWidgetData.sAIWidgetOpts.iModelIndex
				SET_VEHICLE_GRAVITY(sAIVehicleCreationData.viVehicles[sWidgetData.sAIWidgetOpts.iCurrentVehicle], FALSE)
				SET_ENTITY_COLLISION(sAIVehicleCreationData.viVehicles[sWidgetData.sAIWidgetOpts.iCurrentVehicle], FALSE)			
				SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)
			ENDIF
		ENDIF		
	ENDIF	
	
	IF sWidgetData.bWerePlayerVehiclesSpawned
		UPDATE_DUMMY_PLAYER_VEHICLES(sWidgetData)
	ENDIF
ENDPROC


#ENDIF
