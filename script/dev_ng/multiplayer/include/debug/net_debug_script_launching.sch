#IF IS_DEBUG_BUILD

USING "Globals.sch"

FUNC STRING _GET_DEBUG_SCRIPT_COMMANDLINE_NAME()
	RETURN "sc_LaunchDebugScript"
ENDFUNC

BOOL _bLaunchingDebugScript = FALSE

PROC MAINTAIN_DEBUG_SCRIPT_LAUNCHING()
	IF _bLaunchingDebugScript
		STRING strCommandline = _GET_DEBUG_SCRIPT_COMMANDLINE_NAME()
	
		IF NOT GET_COMMANDLINE_PARAM_EXISTS(strCommandline)
			_bLaunchingDebugScript = FALSE
			EXIT
		ENDIF
		
		STRING strScriptName = GET_COMMANDLINE_PARAM(strCommandline)
		
		IF NOT DOES_SCRIPT_EXIST(strScriptName)
			_bLaunchingDebugScript = FALSE
			EXIT
		ENDIF
		
		IF NETWORK_IS_SCRIPT_ACTIVE(strScriptName, DEFAULT, TRUE)
			TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME(strScriptName)
		ENDIF
		
		REQUEST_SCRIPT(strScriptName)
		
		IF NOT HAS_SCRIPT_LOADED(strScriptName)
			EXIT
		ENDIF
		
		START_NEW_SCRIPT(strScriptName, DEFAULT_STACK_SIZE) 
		SET_SCRIPT_AS_NO_LONGER_NEEDED(strScriptName)
		_bLaunchingDebugScript = FALSE
	ENDIF
	
	_bLaunchingDebugScript = IS_DEBUG_KEY_PRESSED(KEY_L, KEYBOARD_MODIFIER_CTRL_SHIFT, "Debug script launch")
ENDPROC
#ENDIF
