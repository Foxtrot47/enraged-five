///    Debug functions common to GTA V.

// --- Include Files ------------------------------------------------------------

#IF IS_DEBUG_BUILD

// Game Headers
USING "globals.sch"

USING "commands_network.sch"
USING "commands_lobby.sch"
USING "commands_debug.sch"
USING "commands_script.sch"
USING "commands_hud.sch"
USING "commands_pad.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"

USING "net_prints.sch"
USING "net_events.sch"

USING "script_player.sch"
USING "script_DEBUG.sch"
USING "script_maths.sch"
USING "net_include.sch"

/// PURPOSE:
///   DEBUG:  Gets the closest alive player to vPos
FUNC PLAYER_INDEX DEBUG_GET_CLOSEST_ALIVE_PLAYER()
	
	VECTOR vPos 
	vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	FLOAT fDist
	FLOAT fClosestPlayerDist = HIGHEST_INT
	
	
	PLAYER_INDEX ClosestPlayer = INVALID_PLAYER_INDEX()
	INT iPlayer 
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_NET_PLAYER_OK(playerId)
			IF playerId <> PLAYER_ID()
				PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
				fDist = VDIST2(GET_ENTITY_COORDS(PlayerPedId), vPos )						
				IF fDist < fClosestPlayerDist
					fClosestPlayerDist = fDist
					ClosestPlayer = PlayerId
				ENDIF			
			ENDIF
		ENDIF
	ENDREPEAT
		
 	RETURN ClosestPlayer 
	
ENDFUNC
//
//PROC WARP_PLAYER_TO_HEIST_ROOM()
//	VECTOR vHeistBoard	= <<-778.2488, 332.1039, 206.6209>>
//	VECTOR vOutsideRoom = <<-779.7833, 337.7423, 206.6209>>
//	INT iCurrentProperty 
//	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_X, KEYBOARD_MODIFIER_SHIFT, "")
//		iCurrentProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
//		IF iCurrentProperty = 1 //PROPERTY_HIGH_APT_1
//			IF IS_NET_PLAYER_OK(PLAYER_ID())
//				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHeistBoard, <<1,1,1>>)
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), vOutsideRoom)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 134.5375)
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//				ELSE
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), vHeistBoard)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 144.8592)
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Accepts a bitfield INT and outputs team names to the console log corresponding to the bits
//
// INPUT PARAMS:		paramBits				The Bitfield
//
// NOTES:	The bits should correspond to the TEAM IDs as an INT
PROC Debug_Output_Team_Names_From_Bitfield(INT paramBits)

	IF (paramBits=0)
		// fool compiler
	ENDIF

//	INT tempLoop = 0
//	REPEAT MAX_NUM_TEAMS tempLoop
//		IF (IS_BIT_SET(paramBits, tempLoop))
//			NET_PRINT("           ")
//			NET_PRINT(Convert_Team_To_String(tempLoop))
//			NET_NL()
//		ENDIF
//	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output One Player Name and Team in standard debug output format
//
// INPUT PARAMS:		paramPlayerID			The Player Index
//						paramPlayerAsInt		Used only if the player is no longer in teh network game
//
// NOTES:	The bits should correspond to the PlayerIndex as an INT - as set by the ALL_PLAYERS(), etc functions
PROC Debug_Output_Player_Name_And_Team(PLAYER_INDEX paramPlayerID, INT paramPlayerAsInt)

	NET_PRINT("           ")
	IF (IS_NET_PLAYER_OK(paramPlayerID, FALSE, FALSE))
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT("   (")
		NET_PRINT(Convert_Team_To_String(GET_PLAYER_TEAM(paramPlayerID)))
		NET_PRINT(")")
	ELSE
		NET_PRINT("Player Removed But Bit Still Set: ")
		NET_PRINT_INT(paramPlayerAsInt)
	ENDIF
	
	// NO NET_NL() - The calling function should output that

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Accepts a bitfield INT and outputs player names to the console log corresponding to the bits
//
// INPUT PARAMS:		paramBits				The Bitfield
//
// NOTES:	The bits should correspond to the PlayerIndex as an INT - as set by the ALL_PLAYERS(), etc functions
PROC Debug_Output_Player_Names_From_Bitfield(INT paramBits)

	INT tempLoop = 0
	PLAYER_INDEX thePlayer
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(paramBits, tempLoop))
			thePlayer = INT_TO_PLAYERINDEX(tempLoop)
			
			Debug_Output_Player_Name_And_Team(thePlayer, tempLoop)
			NET_NL()
		ENDIF
	ENDREPEAT

ENDPROC

//Checks to see if Shift Z should work.
FUNC BOOL SHOULD_SHIFT_Z_SKIP_WORK()
	IF 	GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.mdID.idCreator = -1
			AND NOT IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iBitSet, biIwantToVote)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	IF g_b_On_Deathmatch
		RETURN TRUE
	ENDIF
	
    IF g_bFM_ON_TEAM_MISSION
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Do Debug based on keypresses. 
PROC KEYPRESS_DEBUG()

	IF NETWORK_GET_SCRIPT_STATUS() = NETSCRIPT_PLAYING
   		
   		
		// **** COMMENTED THIS OUT BECAUSE NUMPAD8 CLASHES WITH CODERS TIME BARS *****
		
   		// Warp ahead of the nearest player.
		IF IS_NET_PLAYER_OK(PLAYER_ID())

			IF SHOULD_SHIFT_Z_SKIP_WORK()
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_Z, KEYBOARD_MODIFIER_SHIFT, "Warp to nearest player")
					IF NETWORK_GET_TOTAL_NUM_PLAYERS() > 1
						PLAYER_INDEX ClosestPlayerID = DEBUG_GET_CLOSEST_ALIVE_PLAYER() 
						IF ClosestPlayerID <> INVALID_PLAYER_INDEX()
							IF ClosestPlayerID <> PLAYER_ID()					
								
								IF IS_NET_PLAYER_OK(ClosestPlayerID)
									
									NET_PRINT_TIME() NET_PRINT("UPDATE_GAME_DATA_WIDGETS Warping to nearest player...")	NET_NL()
									
									//DO_SCREEN_FADE_OUT(500)
														
									PED_INDEX ped = GET_PLAYER_PED(ClosestPlayerID) 
									VECTOR vPos = GET_ENTITY_COORDS(ped, FALSE)
									//FLOAT fHeading = GET_ENTITY_HEADING(ped)
									J_SKIP_MP(vPos,<<3,3,0>>,FALSE,FALSE)
	
//									// face the other guy
//									VECTOR vVec
//									vVec = GET_ENTITY_COORDS(ped, FALSE) - GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
//									SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(vVec.x, vVec.y))
	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			//Kill Local Player
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_K, KEYBOARD_MODIFIER_SHIFT, "Kill local player")
				IF NOT GET_PLAYER_INVINCIBLE(PLAYER_ID())
					NET_PRINT_TIME() NET_PRINT("UPDATE_GAME_DATA_WIDGETS Kill player.")	NET_NL()	
					
					SCRIPT_EVENT_DATA_TICKER_MESSAGE cheatTickerEventData
					cheatTickerEventData.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_SUICIDE
					cheatTickerEventData.playerID = PLAYER_ID()
					BROADCAST_TICKER_EVENT(cheatTickerEventData, ALL_PLAYERS())
					
					// Dm debug (1320969)
					IF g_b_On_Deathmatch
						IF g_iMyDMScore > 0
							g_iMyDMScore --
							PRINTLN("DEBUG SHIFT > K, DEDUCT_DEATHMATCH_SCORE, g_iMyDMScore ", g_iMyDMScore)
						ENDIF
						PRINTLN("DEBUG SHIFT > K, DEDUCT_DEATHMATCH_SCORE, g_iMyAmountOfDeaths ", g_iMyAmountOfDeaths)
						g_iMyAmountOfDeaths++
					ENDIF
					
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
					SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
				ELSE
					NET_PRINT_TIME() NET_PRINT("UPDATE_GAME_DATA_WIDGETS Can't Kill Player - They are Invincible")	NET_NL()
				ENDIF
			ENDIF
			
			// explode players car
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F7, KEYBOARD_MODIFIER_SHIFT, "explode players car")
				VEHICLE_INDEX CarID
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT IS_ENTITY_DEAD(CarID)
						NETWORK_EXPLODE_VEHICLE(CarID)
					ENDIF
				ENDIF
			ENDIF

			//Print player health
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
			AND IS_KEYBOARD_KEY_PRESSED(KEY_LCONTROL))	
				TEXT_LABEL_63 tl63 = ""
				tl63 += "Player heading = "
				tl63 += GET_ENTITY_HEALTH(PLAYER_PED_ID())
				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", tl63, 5000,1)
				PRINTINT( GET_ENTITY_HEALTH(PLAYER_PED_ID()) ) PRINTNL()
			ENDIF
		ENDIF

		// Dump debug info.
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_SHIFT, "Dump debug info")
		
			NET_PRINT_TIME() NET_PRINT("NET INFO ======================\n")		
			NET_PRINT_SCRIPT() NET_PRINT_THREAD_ID() NET_NL()	
			
			INT iParticipant
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
					PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
					
					NET_PRINT_STRINGS("Name : ", GET_PLAYER_NAME(playerId))
					NET_PRINT_STRING_INT(", Network Slot: ", NATIVE_TO_INT(playerId))
					NET_PRINT_STRING_INT(", Team: ", GET_PLAYER_TEAM(playerId))
					IF NETWORK_GET_HOST_OF_THIS_SCRIPT() = INT_TO_PARTICIPANTINDEX(iParticipant)
						NET_PRINT(", Host: TRUE")
					ELSE
						NET_PRINT(", Host: FALSE")
					ENDIF						
					IF IS_NET_PLAYER_OK(playerID)					
						NET_PRINT(", IsAlive: FALSE")
						NET_PRINT_STRING_VECTOR( ", Ped:", GET_ENTITY_COORDS(GET_PLAYER_PED(playerID), FALSE))			
					ELSE
						NET_PRINT(", IsAlive: TRUE")
						NET_PRINT_STRING_VECTOR( ", Ped:", GET_ENTITY_COORDS(GET_PLAYER_PED(playerID), FALSE))
					ENDIF	
					IF PLAYER_ID() = playerID
						NET_PRINT(" *")
					ENDIF	
					NET_PRINT(".")
					NET_NL()
				ENDIF
			ENDREPEAT			
			NET_PRINT_TIME() NET_PRINT("NET INFO ======================\n")				
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1,KEYBOARD_MODIFIER_SHIFT, "dump script time info")
			METRICS_PRINT_SCRIPT_PROCESSING_TIMES()
			NET_PRINT("METRICS_PRINT_SCRIPT_PROCESSING_TIMES() called \n")		
		ENDIF
		
		IF (IS_DEBUG_KEY_JUST_PRESSED(KEY_X, KEYBOARD_MODIFIER_CTRL, "MP: Kick idle players on/off"))
			BROADCAST_GENERAL_EVENT(GENERAL_EVENT_KICK_IDLE_PLAYERS, ALL_PLAYERS())
			
			NET_PRINT("...DSW Sent Broadcast Message to Server to Toggle kick idle players on/off") NET_NL()
		ENDIF
		
	
		// chat override
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_SHIFT, "Change Voice Chat")
			BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_TOGGLE_CHAT_OVERRIDE, ALL_PLAYERS())
		ENDIF
			
		
	ENDIF

ENDPROC

PROC MAINTAIN_DEBUG_WARP() 
	
ENDPROC
// ===========================================================================================================
//      Metrics Functions - used by code towork out the average FPS for a mission
//		(KGM 28/3/12: Added here in case we need to split the metrics over mission sections)
// ===========================================================================================================

// PURPOSE:	Start Metrics tracking when a mission starts
//
// INPUT PARAMS:		paramMissionID			Mission ID
//						paramVariationID		Mission Variation ID
//
// NOTES:	Based on Ben's SP functions
PROC Debug_Metrics_MP_Mission_Started(MP_MISSION paramMissionID, INT paramVariationID)

	IF NOT (g_bFlowMetricZonesEnabled)
		EXIT
	ENDIF
	
	// Reset existing data
	METRICS_ZONES_HIDE()
	METRICS_ZONES_CLEAR()
	
	CPRINTLN(DEBUG_METRICS, "MP Mission started. Resetting metrics zone system.")
            
	// Generate Zone name
	TEXT_LABEL_63 metricsZoneName = GET_MP_MISSION_NAME(paramMissionID)
	metricsZoneName += " - Variation "
	metricsZoneName += paramVariationID
	
	// Start collecting Metrics data for mission
	METRICS_ZONE_START(metricsZoneName)
	
	CPRINTLN(DEBUG_METRICS, "Started new MP metrics zone. [", metricsZoneName,"]")

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Stop Metrics tracking when a mission ends
//
// INPUT PARAMS:		paramMissionID			Mission ID
//						paramVariationID		Mission Variation ID
//
// NOTES:	Based on Ben's SP functions
PROC Debug_Metrics_MP_Mission_Ended(MP_MISSION paramMissionID, INT paramVariationID)

	IF NOT (g_bFlowMetricZonesEnabled)
		EXIT
	ENDIF
	
	// Stop collecting Metrics data
	METRICS_ZONE_STOP()
            
	// Generate Zone name for debug output
	TEXT_LABEL_63 metricsZoneName = GET_MP_MISSION_NAME(paramMissionID)
	metricsZoneName += " - Variation "
	metricsZoneName += paramVariationID
	CPRINTLN(DEBUG_METRICS, "Stopped running MP metrics zone. [", metricsZoneName,"]")
	
	// Show collected data
	METRICS_ZONES_SHOW()
	g_bDisplayingMetricsZoneResults	= TRUE
	g_iTimeDisplayedMetricsReport_NETWORK = GET_NETWORK_TIME()

ENDPROC


PROC MAINTAIN_PLAYERS_FRAMECOUNT(INT &iCurrentDebugFrameCount, BOOL bUseRockstarCandidateMissions, SCRIPT_TIMER &sDebugSyncTimer)
	//If I have a player ID 
	IF NATIVE_TO_INT(PLAYER_ID()) != -1
		//and 30 frames have passed then send my frame count
		IF iCurrentDebugFrameCount > 60
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iMyFrameCount = GET_FRAME_COUNT()
			iCurrentDebugFrameCount = 0
		ELSE
			iCurrentDebugFrameCount++
		ENDIF
	ENDIF
	g_sRockstarUgcGetStruct.bUseCandidatesMisMatch = FALSE
	g_sRockstarUgcGetStruct.bTunableMisMatch = FALSE
	g_sRockstarUgcGetStruct.bTunableCommandlineMismatch = FALSE
	INT iLoop
	INT iTunableHash = GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iTunablesHash
	BOOL bTunableCommandline = GET_COMMANDLINE_PARAM_EXISTS("tunableCloudFile")
	IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].bTunableCommandlinePresent != bTunableCommandline
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].bTunableCommandlinePresent = bTunableCommandline
	ENDIF
	BOOL bTimerExpired
	IF NOT HAS_NET_TIMER_STARTED(sDebugSyncTimer)
		START_NET_TIMER(sDebugSyncTimer)
	ELIF HAS_NET_TIMER_EXPIRED(sDebugSyncTimer, 1000)
		RESET_NET_TIMER(sDebugSyncTimer)
		bTimerExpired = TRUE
	ENDIF
	//Loop all players
	REPEAT NUM_NETWORK_PLAYERS iLoop	
		//If the frame count doesn't match the last sync
		IF g_debugPlayersFrameCountLast[iLoop] != GlobalplayerBD_FM_3[iLoop].iMyFrameCount
			//then sync it
			g_debugPlayersFrameCount[iLoop] = GlobalplayerBD_FM_3[iLoop].iMyFrameCount
			//Store it so we don't sync every frame
			g_debugPlayersFrameCountLast[iLoop] = GlobalplayerBD_FM_3[iLoop].iMyFrameCount
		ELSE	
			//Incrament
			IF g_debugPlayersFrameCount[iLoop] != 0	
				g_debugPlayersFrameCount[iLoop]++		
			ENDIF
		ENDIF
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iLoop)
		IF bTimerExpired
		AND playerID != INVALID_PLAYER_INDEX()
		AND playerID != PLAYER_ID()
			IF IS_NET_PLAYER_OK(playerID, FALSE)
				INT iPlayer = NATIVE_TO_INT(playerID)
				IF IS_BIT_SET(GlobalplayerBD_FM_2[iPlayer].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_CANDIDATE_ARG) != bUseRockstarCandidateMissions // SHOULD_USE_ROCKSTAR_CANDIDATE_MISSIONS()
					PRINTLN("MAINTAIN_PLAYERS_FRAMECOUNT - IS_BIT_SET(GlobalplayerBD_FM_2[", NATIVE_TO_INT(playerID), "].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_CANDIDATE_ARG) != SHOULD_USE_ROCKSTAR_CANDIDATE_MISSIONS() - player = ", GET_PLAYER_NAME(playerID))
					g_sRockstarUgcGetStruct.bUseCandidatesMisMatch = TRUE
				ENDIF
				IF iTunableHash != 0
				AND GlobalplayerBD_FM_2[iPlayer].iTunablesHash != 0
				AND GlobalplayerBD_FM_2[iPlayer].iTunablesHash != iTunableHash
					g_sRockstarUgcGetStruct.bTunableMisMatch = TRUE
				ENDIF
				
				IF GlobalplayerBD_FM_3[iPlayer].bTunableCommandlinePresent != bTunableCommandline
					PRINTLN("MAINTAIN_PLAYERS_FRAMECOUNT - GlobalplayerBD_FM_3[iPlayer].bTunableCommandlinePresent != bTunableCommandline - player = ", GET_PLAYER_NAME(playerID))
					g_sRockstarUgcGetStruct.bTunableCommandlineMismatch = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	IF bTimerExpired
		INT iLogFile = GET_LOG_FILE_NUMBER()
		IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iMyLogFile != iLogFile
			PRINTLN("GlobalplayerBD_FM_3[NATIVE_TO_INT(ThisPlayer)].iMyLogFile = GET_LOG_FILE_NUMBER() = ", iLogFile)
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iMyLogFile = iLogFile
		ENDIF
		IF IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_CANDIDATE_ARG) != bUseRockstarCandidateMissions // SHOULD_USE_ROCKSTAR_CANDIDATE_MISSIONS()
			IF bUseRockstarCandidateMissions // SHOULD_USE_ROCKSTAR_CANDIDATE_MISSIONS()
				SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_CANDIDATE_ARG)
				SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_CANDIDATE_ARG_SET)
				PRINTLN("MAINTAIN_PLAYERS_FRAMECOUNT - SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_CANDIDATE_ARG)")
			ELSE
				CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_CANDIDATE_ARG)
				PRINTLN("MAINTAIN_PLAYERS_FRAMECOUNT - CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_CANDIDATE_ARG)")
				SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_CANDIDATE_ARG_SET)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


#ENDIF

// EOF

