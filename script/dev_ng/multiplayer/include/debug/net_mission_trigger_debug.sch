// This header shouldn't be in the release project anyway, but wrap it up just in case
#IF IS_DEBUG_BUILD



USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "net_comms_debug.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_Trigger_Debug.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all MP Mission Trigger debug functionality.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      MP Mission Trigger Console Log Output Routines
// ===========================================================================================================

// PURPOSE:	Output the contents of an MP MISSION DATA struct
//
// INPUT PARAMS:		paramMissionData			A Mission Data struct
PROC Debug_Output_MP_MISSION_DATA_Details(MP_MISSION_DATA paramMissionData)

	NET_PRINT("           Mission: ")				NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))	NET_NL()
	NET_PRINT("           Instance: ")				NET_PRINT_INT(paramMissionData.iInstanceId)						NET_NL()
	NET_PRINT("           Primary Location: ")		NET_PRINT_VECTOR(paramMissionData.mdPrimaryCoords)				NET_NL()
	NET_PRINT("           Secondary Location: ")	NET_PRINT_VECTOR(paramMissionData.mdSecondaryCoords)			NET_NL()
	NET_PRINT("           Variation: ")				NET_PRINT_INT(paramMissionData.mdID.idVariation)				NET_NL()
	NET_PRINT("           ContentID: ")				NET_PRINT(paramMissionData.mdID.idCloudFilename)				NET_NL()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Displays the stored MP MISSION DATA details for the mission being triggered
PROC Debug_Output_MP_MISSION_DATA_Details_For_Existing_Request()

	Debug_Output_MP_MISSION_DATA_Details(g_sTriggerMP.mtMissionData)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the contact details for a pre-mission comms
//
// INPUT PARAMS:		paramCharacter				The Character ID string (from the 'Character' dropdown box in dialogueStar)
//						paramSpeakerID				The Speaker ID string (from the 'Speaker' value in dialogueStar)
//						paramCharSheetID			The CharSheetID for the contact making the phonecall
//						paramGroupID				The Subtitle Group ID string for the Conversation (from the 'Subtitle Group ID' value in dialogueStar)
//						paramRootID					The Root ID string for the Conversation (from the 'Root' value in dialogueStar)
PROC Debug_Output_PreMission_Comms_Details(STRING paramCharacter, STRING paramSpeakerID, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID)

	BOOL usingIntroTxtmsg = IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFIELD_USE_TXTMSG)

	IF NOT (usingIntroTxtmsg)
		// Used by phonecall or radio message only, not by text message
		NET_PRINT("           Character: ")
		IF (Is_String_Null_Or_Empty(paramCharacter))
			NET_PRINT("[No Character ID]") NET_NL()
		ELSE
			NET_PRINT(paramCharacter) NET_NL()
		ENDIF

		NET_PRINT("           SpeakerID: ")
		IF (Is_String_Null_Or_Empty(paramSpeakerID))
			NET_PRINT("[No Speaker ID]") NET_NL()
		ELSE
			NET_PRINT(paramSpeakerID) NET_NL()
		ENDIF
	ENDIF

	NET_PRINT("           MP Caller: ") NET_PRINT(Convert_MP_CharSheetID_To_String(paramCharSheetID)) NET_NL()

	IF NOT (usingIntroTxtmsg)
		// Used by phonecall or radio message only, not by text message
		NET_PRINT("           Subtitle Group ID: ")
		IF (Is_String_Null_Or_Empty(paramGroupID))
			NET_PRINT("[No Group ID]") NET_NL()
		ELSE
			NET_PRINT(paramGroupID) NET_NL()
		ENDIF
	ENDIF

	NET_PRINT("           Conversation Root ID: ")
	IF (Is_String_Null_Or_Empty(paramRootID))
		NET_PRINT("[No Root ID]") NET_NL()
	ELSE
		NET_PRINT(paramRootID) NET_NL()
	ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Displays the stored PreMission Comms details for the mission being triggered
PROC Debug_Output_PreMission_Comms_Details_For_Existing_Request()

	enumCharacterList charSheetID = INT_TO_ENUM(enumCharacterList, g_sTriggerMP.mtCharSheetAsInt)
	Debug_Output_PreMission_Comms_Details(g_sTriggerMP.mtCharacter, g_sTriggerMP.mtSpeaker, charSheetID, g_sTriggerMP.mtGroup, g_sTriggerMP.mtRoot)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Displays any Mission Triggering Option flags
PROC Debug_Output_Mission_Triggering_Option_Flags()

	NET_PRINT("           FLAG: Force Onto Mission? ")
	IF (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_FORCE_ONTO_MISSION))
		NET_PRINT("[YES]") NET_NL()
	ELSE
		NET_PRINT("[NO]") NET_NL()
	ENDIF

	NET_PRINT("           FLAG: Show PreMission Cutscene? ")
	IF (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_PREMISSION_CUTSCENE))
		NET_PRINT("[YES]") NET_NL()
	ELSE
		NET_PRINT("[NO]") NET_NL()
	ENDIF

	NET_PRINT("           FLAG: Display Mission Details Box? ")
	IF (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_DISPLAY_MISSION_DETAILS))
		NET_PRINT("[YES]") NET_NL()
	ELSE
		NET_PRINT("[NO]") NET_NL()
	ENDIF

	NET_PRINT("           FLAG: Mission Finished? ")
	IF (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_MISSION_FINISHED))
		NET_PRINT("[YES]") NET_NL()
	ELSE
		NET_PRINT("[NO]") NET_NL()
	ENDIF

	NET_PRINT("           FLAG: Mission Failed To Launch? ")
	IF (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_MISSION_FAILED))
		NET_PRINT("[YES]") NET_NL()
	ELSE
		NET_PRINT("[NO]") NET_NL()
	ENDIF

	NET_PRINT("           FLAG: Mission Full For Team? ")
	IF (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_MISSION_FULL))
		NET_PRINT("[YES]") NET_NL()
	ELSE
		NET_PRINT("[NO]") NET_NL()
	ENDIF

	NET_PRINT("           FLAG: Confirmed Still Ok To Join? ")
	IF (IS_BIT_SET(g_sTriggerMP.mtBitflags, MP_TRIGGER_BITFLAG_RECEIVED_OK_TO_JOIN))
		NET_PRINT("[YES]") NET_NL()
	ELSE
		NET_PRINT("[NO]") NET_NL()
	ENDIF

	NET_PRINT("           CHECK: Is CODE-DEFINED Tutorial Session Active? ")
	IF (NETWORK_IS_IN_TUTORIAL_SESSION())
		NET_PRINT("[YES]") NET_NL()
	ELSE
		NET_PRINT("[NO]") NET_NL()
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Displays any stored Mission Triggering Details
//
// INPUT PARAMS:		paramDisplayMissionComms			[DEFAULT=TRUE] TRUE to display comms details, otherwise FALSE
PROC Debug_Output_Stored_Mission_Triggering_Details(BOOL paramDisplayMissionComms = TRUE)

	// Mission Details
	Debug_Output_MP_MISSION_DATA_Details_For_Existing_Request()

	// Comms Details
	IF (paramDisplayMissionComms)
		Debug_Output_PreMission_Comms_Details_For_Existing_Request()
	ENDIF
	
	// Flag Details
	Debug_Output_Mission_Triggering_Option_Flags()

ENDPROC



#ENDIF	// IS_DEBUG_BUILD

