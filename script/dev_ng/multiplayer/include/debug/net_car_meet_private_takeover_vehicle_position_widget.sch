#IF IS_DEBUG_BUILD

USING "globals.sch"
USING "net_mission.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"

USING "Tuner\mp_car_meet_ai_vehicles.sch"

STRUCT PRIVATE_TAKEOVER_WIDGET_OPTIONS
	INT iCurrentVehicle = -1
	INT iPreviousVehicle = -1
	BOOL bAdd35Y= FALSE, bSubtract35Y = FALSE
	BOOL bShouldVehicleDoorsBeOpen = FALSE
	BOOL bShouldVehicleDoorsBeCLosed = FALSE
	BOOL bBlipVehicle = FALSE
	BOOL bTurnOffColliders = FALSE
	BOOL bHealVehicle = FALSE
	BOOL bRenderVehInfo = FALSE
	INT iModelIndex
	FLOAT fCurrentX = -2224.0, fCurrentY = 1065.0, fCurrentZ = -40.862, fCurrentHeading = 180.0
ENDSTRUCT

STRUCT PRIVATE_TAKEOVER_ADJUSTMENT_DATA
	BOOL bPositionVehicles[ciCAR_MEET_NUMBER_OF_PRIVATE_TAKEOVER_VEHICLES]
	BOOL bSpawnVehicles = FALSE
	BOOL bRemoveVehicles = FALSE
	VEHICLE_INDEX viVehicles[ciCAR_MEET_NUMBER_OF_PRIVATE_TAKEOVER_VEHICLES]
	INT iVehicleModels[ciCAR_MEET_NUMBER_OF_PRIVATE_TAKEOVER_VEHICLES]
	PRIVATE_TAKEOVER_WIDGET_OPTIONS sWidgetOpts
	//GENERAL
	BOOL bWereVehiclesSpawned = FALSE
	BOOL bWereVehiclesRemoved = FALSE
	BLIP_INDEX biCurrentVehicle
	BOOL bPrintResults = FALSE
	VECTOR vPositions[ciCAR_MEET_NUMBER_OF_PRIVATE_TAKEOVER_VEHICLES]
	FLOAT fHeadings[ciCAR_MEET_NUMBER_OF_PRIVATE_TAKEOVER_VEHICLES]
ENDSTRUCT

PROC CREATE_PRIVATE_TAKEOVER_VEHICLE_ADJUSTMENT_WIDGET(PRIVATE_TAKEOVER_ADJUSTMENT_DATA &sPrivTakeWidgetData)
START_WIDGET_GROUP("PRIVATE TAKEOVER Vehicle Positions Adjustment Tool")
	INT i
	ADD_WIDGET_BOOL("OUTPUT VEHICLE DATA", sPrivTakeWidgetData.bPrintResults)
	
	START_NEW_WIDGET_COMBO()
	REPEAT CM_VEH_COUNT i
		ADD_TO_WIDGET_COMBO(CAR_MEET_VEHICLE_INT_TO_STRING(i))	
	ENDREPEAT
	ADD_TO_WIDGET_COMBO("SLAMTRUCK")
	ADD_TO_WIDGET_COMBO("SANDKING")
	STOP_WIDGET_COMBO("Model", sPrivTakeWidgetData.sWidgetOpts.iModelIndex)
	ADD_WIDGET_BOOL("SPAWN VEHICLES", sPrivTakeWidgetData.bSpawnVehicles)
	ADD_WIDGET_BOOL("REMOVE VEHICLES (preserves positions and headings for respawning)", sPrivTakeWidgetData.bRemoveVehicles)
	
	ADD_WIDGET_BOOL("RENDER VEHICLE INFO " , sPrivTakeWidgetData.sWidgetOpts.bRenderVehInfo)
	ADD_WIDGET_BOOL("BLIP VEHICLE" , sPrivTakeWidgetData.sWidgetOpts.bBlipVehicle)
	ADD_WIDGET_BOOL("HEAL VEHICLE" , sPrivTakeWidgetData.sWidgetOpts.bHealVehicle)
	ADD_WIDGET_INT_SLIDER("VEHICLE INDEX", sPrivTakeWidgetData.sWidgetOpts.iCurrentVehicle , -1, 79, 1)	
	
	ADD_WIDGET_FLOAT_SLIDER("POSITION X: ",sPrivTakeWidgetData.sWidgetOpts.fCurrentX, -2224.0, -2140, 0.1 )
	ADD_WIDGET_FLOAT_SLIDER("POSITION Y: ",sPrivTakeWidgetData.sWidgetOpts.fCurrentY, 1065, 1220.0 , 0.1)
	ADD_WIDGET_BOOL(" +3.5 on Y axis:", sPrivTakeWidgetData.sWidgetOpts.bAdd35Y)
	ADD_WIDGET_BOOL(" -3.5 on Y axis:", sPrivTakeWidgetData.sWidgetOpts.bSubtract35Y)
	ADD_WIDGET_FLOAT_SLIDER("POSITION Z: ",sPrivTakeWidgetData.sWidgetOpts.fCurrentZ, -40.862, -20.862 , 0.1)
	ADD_WIDGET_FLOAT_SLIDER("HEADING ", sPrivTakeWidgetData.sWidgetOpts.fCurrentHeading, 0.0, 360.0, 0.1)
	ADD_WIDGET_BOOL("OPEN DOORS " , sPrivTakeWidgetData.sWidgetOpts.bShouldVehicleDoorsBeOpen)
	ADD_WIDGET_BOOL("CLOSE DOORS ", sPrivTakeWidgetData.sWidgetOpts.bShouldVehicleDoorsBeClosed)

	
	
STOP_WIDGET_GROUP()
ENDPROC


PROC INITIALISE_PRIVATE_TAKEOVER_ADJUSTMENT_WIDGET(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData, AI_VEHICLE_DATA &sVehicleData, PRIVATE_TAKEOVER_ADJUSTMENT_DATA &sVehicleAdjustmentWidgetData )
	
	INT i
	REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES i
		IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[i])					
			sVehicleAdjustmentWidgetData.iVehicleModels[i] = ENUM_TO_INT(sVehicleData.sAIVehicle[i].eModel)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SPAWN_VEHICLES(PRIVATE_TAKEOVER_ADJUSTMENT_DATA &sVehicleAdjustmentWidgetData, BOOL bAreVehiclesBeingReplaced = FALSE)
	VECTOR vPosition
	FLOAT fHeading
	INT i
	VEHICLE_SETUP_STRUCT_MP sData
	REPEAT ciCAR_MEET_NUMBER_OF_PRIVATE_TAKEOVER_VEHICLES i
		IF NOT DOES_ENTITY_EXIST(sVehicleAdjustmentWidgetData.viVehicles[i])
			IF sVehicleAdjustmentWidgetData.sWidgetOpts.iModelIndex < ENUM_TO_INT(CM_VEH_COUNT)		
				GET_AI_VEHICLE_DATA(INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, sVehicleAdjustmentWidgetData.sWidgetOpts.iModelIndex),GET_RANDOM_INT_IN_RANGE(0,5), sData)
			ELIF  sVehicleAdjustmentWidgetData.sWidgetOpts.iModelIndex = ENUM_TO_INT(CM_VEH_COUNT)
				sData.VehicleSetup.eModel = SLAMTRUCK
			ELSE
				sData.VehicleSetup.eModel = SANDKING
			ENDIF		
			IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)	
				IF NOT bAreVehiclesBeingReplaced
					 GET_PRIVATE_CAR_MEET_PARKING_SPOT_LOCATION(i, vPosition, fHeading)
				ELSE
					vPosition = sVehicleAdjustmentWidgetData.vPositions[i]
					fHeading = sVehicleAdjustmentWidgetData.fHeadings[i]
				ENDIF
			
			    sVehicleAdjustmentWidgetData.iVehicleModels[i]= sVehicleAdjustmentWidgetData.sWidgetOpts.iModelIndex
				
				sVehicleAdjustmentWidgetData.viVehicles[i] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vPosition, fHeading, FALSE)
				
				IF DOES_ENTITY_EXIST(sVehicleAdjustmentWidgetData.viVehicles[i])
					SET_ENTITY_COORDS_NO_OFFSET(sVehicleAdjustmentWidgetData.viVehicles[i], vPosition)
					
					SET_ENTITY_HEADING(sVehicleAdjustmentWidgetData.viVehicles[i], fHeading)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sVehicleAdjustmentWidgetData.viVehicles[i], TRUE)
				ENDIF
				
				RETURN FALSE // ONE VEHICLE PER FRAME
			ENDIF
		ELIF IS_VEHICLE_DRIVEABLE(sVehicleAdjustmentWidgetData.viVehicles[i])
		AND NOT sVehicleAdjustmentWidgetData.bPositionVehicles[i]
			sVehicleAdjustmentWidgetData.bPositionVehicles[i] = SET_VEHICLE_ON_GROUND_PROPERLY(sVehicleAdjustmentWidgetData.viVehicles[i])
		ENDIF
	ENDREPEAT

	RETURN TRUE //ALL VEHICLES SPAWNED
ENDFUNC
FUNC BOOL REMOVE_VEHICLES(PRIVATE_TAKEOVER_ADJUSTMENT_DATA &sVehicleAdjustmentWidgetData)
	INT i	
	REPEAT ciCAR_MEET_NUMBER_OF_PRIVATE_TAKEOVER_VEHICLES i
		IF DOES_ENTITY_EXIST(sVehicleAdjustmentWidgetData.viVehicles[i])				
			sVehicleAdjustmentWidgetData.vPositions[i] = GET_ENTITY_COORDS(sVehicleAdjustmentWidgetData.viVehicles[i])
			sVehicleAdjustmentWidgetData.fHeadings[i] = GET_ENTITY_HEADING(sVehicleAdjustmentWidgetData.viVehicles[i])
			sVehicleAdjustmentWidgetData.bPositionVehicles[i] = FALSE
			DELETE_VEHICLE(sVehicleAdjustmentWidgetData.viVehicles[i])
			RETURN FALSE // ONE VEHICLE PER FRAME
		ENDIF
	ENDREPEAT
	
	RETURN TRUE // all vehicles are deleted
ENDFUNC

PROC RENDER_PRIVATE_TAKEOVER_VEHICLE_INFO(PRIVATE_TAKEOVER_ADJUSTMENT_DATA &sVehicleAdjustmentWidgetData, INT i, VECTOR vPosition, FLOAT fHeading)

	
	HUD_COLOURS HudColour
	
	INT iRow = 0
	INT iAlpha = 255
	INT iRed, iGreen, iBlue
	FLOAT fRowZ = 0.2
	TEXT_LABEL_63 str
	
	GET_HUD_COLOUR(HUD_COLOUR_PURPLELIGHT, iRed, iGreen, iBlue, iAlpha)
	
	IF (VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vPosition) <= cfCAR_MEET_MAX_VEHICLE_DATA_RENDER_DISTANCE OR VDIST(GET_FINAL_RENDERED_CAM_COORD(), vPosition) <= cfCAR_MEET_MAX_VEHICLE_DATA_RENDER_DISTANCE)		
		iRow++
		str = "Heading: "
		str += FLOAT_TO_STRING(fHeading)
		DRAW_DEBUG_TEXT(str, <<vPosition.x, vPosition.y, vPosition.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "Coords: "
		str += VECTOR_TO_STRING(vPosition)
		DRAW_DEBUG_TEXT(str, <<vPosition.x, vPosition.y, vPosition.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "Model: "
		str += CAR_MEET_VEHICLE_INT_TO_STRING(sVehicleAdjustmentWidgetData.iVehicleModels[i])
		
		DRAW_DEBUG_TEXT(str, <<vPosition.x, vPosition.y, vPosition.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "INDEX: "
		str += i		
		DRAW_DEBUG_TEXT(str, <<vPosition.x, vPosition.y, vPosition.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		
		DRAW_DEBUG_SPAWN_POINT(vPosition, fHeading, HudColour, 0.0, 0.5)
		DRAW_DEBUG_TEXT(str, <<vPosition.x, vPosition.y, vPosition.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
	ENDIF
	
ENDPROC

PROC OUTPUT_PRIVATE_TAKEOVER_DATA(PRIVATE_TAKEOVER_ADJUSTMENT_DATA &sData)
	INT i	
	STRING sPath = "X:/gta5/titleupdate/dev_ng/PRIVATE_TAKEOVER/"
	TEXT_LABEL_63 tlFile = "car_meet_private_takeover_positions.txt"
	CLEAR_NAMED_DEBUG_FILE(sPath, tlFile)	
	OPEN_NAMED_DEBUG_FILE(sPath, tlFile)	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("SWITCH iLocation", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	REPEAT ciCAR_MEET_NUMBER_OF_PRIVATE_TAKEOVER_VEHICLES i
			TEXT_LABEL_15 sCase = "	CASE "
			sCase+=i
			SAVE_STRING_TO_NAMED_DEBUG_FILE(sCase, sPath, tlFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)	
			
			
			IF DOES_ENTITY_EXIST(sData.viVehicles[i])
				SAVE_STRING_TO_NAMED_DEBUG_FILE("		vLoc =	", sPath, tlFile)
				SAVE_VECTOR_TO_NAMED_DEBUG_FILE(GET_ENTITY_COORDS(sData.viVehicles[i]),  sPath, tlFile)	
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
				SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading =	", sPath, tlFile)		
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(GET_ENTITY_HEADING(sData.viVehicles[i]),  sPath, tlFile)	
			ENDIF	
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, tlFile)		
			
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)		
	ENDREPEAT
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDSWITCH", sPath, tlFile)		
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)	

	
	CLOSE_DEBUG_FILE()
//	SAVE_STRING_TO_NAMED_DEBUG_FILE(" fHeading = ", sPath, tlFile)
//			SAVE_FLOAT_TO_NAMED_DEBUG_FILE(GET_ENTITY_HEADING(NET_TO_ENT(VehDebugScriptManagementData.niVehicles[i])), sPath, tlFile)
ENDPROC
PROC UPDATE_VEHICLES(PRIVATE_TAKEOVER_ADJUSTMENT_DATA &sWidgetData)
	IF sWidgetData.sWidgetOpts.bAdd35Y
		sWidgetData.sWidgetOpts.fCurrentY+=3.5
		sWidgetData.sWidgetOpts.bAdd35Y=FALSE
	ENDIF
	IF sWidgetData.sWidgetOpts.bSubtract35Y
		sWidgetData.sWidgetOpts.fCurrentY-=3.5
		sWidgetData.sWidgetOpts.bSubtract35Y=FALSE
	ENDIF
	
	IF NOT (sWidgetData.sWidgetOpts.iCurrentVehicle = sWidgetData.sWidgetOpts.iPreviousVehicle)
	AND sWidgetData.sWidgetOpts.iCurrentVehicle > -1
		IF sWidgetData.sWidgetOpts.iPreviousVehicle > -1
			SET_VEHICLE_GRAVITY(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iPreviousVehicle], TRUE)
			SET_ENTITY_COLLISION(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iPreviousVehicle], TRUE)
		ENDIF
		sWidgetData.sWidgetOpts.iPreviousVehicle = sWidgetData.sWidgetOpts.iCurrentVehicle
		VECTOR vTemp = GET_ENTITY_COORDS(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle])
		sWidgetData.sWidgetOpts.fCurrentX = vTemp.x
		sWidgetData.sWidgetOpts.fCurrentY= vTemp.y
		sWidgetData.sWidgetOpts.fCurrentZ = vTemp.z
		sWidgetData.sWidgetOpts.fCurrentHeading = GET_ENTITY_HEADING(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle])
		sWidgetData.sWidgetOpts.iModelIndex = sWidgetData.iVehicleModels[sWidgetData.sWidgetOpts.iCurrentVehicle]
		sWidgetData.sWidgetOpts.bTurnOffColliders = TRUE
	ENDIF
	
	VECTOR vCurrentSetting = <<sWidgetData.sWidgetOpts.fCurrentX, sWidgetData.sWidgetOpts.fCurrentY, sWidgetData.sWidgetOpts.fCurrentZ>>
	IF sWidgetData.sWidgetOpts.bRenderVehInfo
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		INT i
		REPEAT ciCAR_MEET_NUMBER_OF_PRIVATE_TAKEOVER_VEHICLES i
			IF DOES_ENTITY_EXIST(sWidgetData.viVehicles[i])	
				RENDER_PRIVATE_TAKEOVER_VEHICLE_INFO(sWidgetData, i,GET_ENTITY_COORDS(sWidgetData.viVehicles[i]), GET_ENTITY_HEADING(sWidgetData.viVehicles[i]))
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF sWidgetData.sWidgetOpts.iCurrentVehicle > -1
		IF DOES_ENTITY_EXIST(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle])	
			IF sWidgetData.sWidgetOpts.bTurnOffColliders
				sWidgetData.sWidgetOpts.bTurnOffColliders = FALSE
				SET_VEHICLE_GRAVITY(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle], FALSE)
				SET_ENTITY_COLLISION(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle], FALSE)
			ENDIF
			IF sWidgetData.sWidgetOpts.bBlipVehicle
				sWidgetData.sWidgetOpts.bBlipVehicle = FALSE
				IF DOES_BLIP_EXIST(sWidgetData.biCurrentVehicle)
					REMOVE_BLIP(sWidgetData.biCurrentVehicle)
				ENDIF
				CREATE_BLIP_FOR_VEHICLE(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle])
			ENDIF
			IF sWidgetData.sWidgetOpts.bHealVehicle
				sWidgetData.sWidgetOpts.bHealVehicle = FALSE
				SET_VEHICLE_FIXED(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle])
			ENDIF
			IF NOT (sWidgetData.iVehicleModels[sWidgetData.sWidgetOpts.iCurrentVehicle] = sWidgetData.sWidgetOpts.iModelIndex)	
				DELETE_VEHICLE(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle])			
			ENDIF
			IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle]), vCurrentSetting)
				SET_ENTITY_COORDS(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle], vCurrentSetting)
			ENDIF
			IF NOT (sWidgetData.sWidgetOpts.fCurrentHeading = GET_ENTITY_HEADING(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle]))
				SET_ENTITY_HEADING(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle], sWidgetData.sWidgetOpts.fCurrentHeading )
			ENDIF	
			IF sWidgetData.sWidgetOpts.bShouldVehicleDoorsBeOpen
				sWidgetData.sWidgetOpts.bShouldVehicleDoorsBeOpen = FALSE				
				SET_VEHICLE_DOOR_OPEN(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle], SC_DOOR_FRONT_LEFT)
				SET_VEHICLE_DOOR_OPEN(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle], SC_DOOR_FRONT_RIGHT)			
			ENDIF
			IF sWidgetData.sWidgetOpts.bShouldVehicleDoorsBeClosed
				sWidgetData.sWidgetOpts.bShouldVehicleDoorsBeClosed = FALSE				
				SET_VEHICLE_DOOR_SHUT(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle], SC_DOOR_FRONT_LEFT)
				SET_VEHICLE_DOOR_SHUT(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle], SC_DOOR_FRONT_RIGHT)			
			ENDIF
		ELIF NOT (sWidgetData.iVehicleModels[sWidgetData.sWidgetOpts.iCurrentVehicle] = sWidgetData.sWidgetOpts.iModelIndex)
			VEHICLE_SETUP_STRUCT_MP sData
			IF sWidgetData.sWidgetOpts.iModelIndex < ENUM_TO_INT(CM_VEH_COUNT)
				GET_AI_VEHICLE_DATA(INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, sWidgetData.sWidgetOpts.iModelIndex),GET_RANDOM_INT_IN_RANGE(0,5), sData)
			ELIF sWidgetData.sWidgetOpts.iModelIndex = ENUM_TO_INT(CM_VEH_COUNT)
				sData.VehicleSetup.eModel = SLAMTRUCK
			ELSE
				sData.VehicleSetup.eModel = SANDKING
			ENDIF
			IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)
				sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vCurrentSetting, sWidgetData.sWidgetOpts.fCurrentHeading, FALSE)	
				SET_VEHICLE_SETUP(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle],sData.VehicleSetup)
				sWidgetData.iVehicleModels[sWidgetData.sWidgetOpts.iCurrentVehicle] = sWidgetData.sWidgetOpts.iModelIndex
				SET_VEHICLE_GRAVITY(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle], FALSE)
				SET_ENTITY_COLLISION(sWidgetData.viVehicles[sWidgetData.sWidgetOpts.iCurrentVehicle], FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)
			ENDIF
		ENDIF		
	ENDIF	
ENDPROC


PROC UPDATE_PRIVATE_TAKEOVER_ADJUSTMENT_WIDGET(PRIVATE_TAKEOVER_ADJUSTMENT_DATA &sWidgetData)
	
	IF sWidgetData.bPrintResults
		 OUTPUT_PRIVATE_TAKEOVER_DATA(sWidgetData)
		sWidgetData.bPrintResults=FALSE
	ENDIF
		
	
	IF sWidgetData.bRemoveVehicles 
		sWidgetData.bWereVehiclesRemoved = REMOVE_VEHICLES(sWidgetData)
		sWidgetData.bRemoveVehicles = NOT sWidgetData.bWereVehiclesRemoved
	ENDIF
	
	IF sWidgetData.bSpawnVehicles
		sWidgetData.bWereVehiclesSpawned = SPAWN_VEHICLES(sWidgetData, sWidgetData.bWereVehiclesRemoved)
		sWidgetData.bSpawnVehicles = NOT sWidgetData.bWereVehiclesSpawned
	ENDIF	
	
	UPDATE_VEHICLES(sWidgetData)		
ENDPROC


#ENDIF
