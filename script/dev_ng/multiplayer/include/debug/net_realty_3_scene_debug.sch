USING "SceneTool_debug.sch"

PROC Private_Savenet_realty_3_scene(STRUCT_net_realty_3_SCENE& scene, INT iBuildingID)
	
	TEXT_LABEL_63 str
	str = Private_Get_Net_Realty_Building_Name(iBuildingID)
	str += "	//"
	str += Private_Get_Net_Realty_Building_Description(iBuildingID)
	
	SceneTool_OpenDebugFile("Private_Get_net_realty_3_SCENE()",				str)

		SceneTool_ExportPan(	"net_realty_3_SCENE_PAN_descent",			scene.mPans[net_realty_3_SCENE_PAN_descent])

		SceneTool_ExportCut(	"net_realty_3_SCENE_CUT_null",				scene.mCuts[net_realty_3_SCENE_CUT_null])

		SceneTool_ExportMarker(	"net_realty_3_SCENE_MARKER_gotoA",			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA])
		SceneTool_ExportMarker(	"net_realty_3_SCENE_MARKER_gotoREVERSE",		scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE])
		SceneTool_ExportMarker(	"net_realty_3_SCENE_MARKER_gotoB",			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB])

		SceneTool_ExportPlacer(	"net_realty_3_SCENE_PLACER_resetCoords",	scene.mPlacers[ net_realty_3_SCENE_PLACER_resetCoords])
		
		SceneTool_ExportAngArea("NET_REALTY_3_SCENE_ANGAREA_area0",			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0])

		SceneTool_ExportBool(	"bEnablePans[0]",				scene.bEnablePans[0])
		
		SceneTool_ExportFloat(	"fExitDelay",					scene.fExitDelay)

	SceneTool_CloseDebugFile("Private_Get_net_realty_3_scene()")
	
ENDPROC

TYPEDEF FUNC BOOL SceneRealty3Func(BOOL bResetCoords, STRUCT_NET_REALTY_3_SCENE &scene)

PROC Private_DebugPlaynet_realty_3_scene(STRUCT_NET_REALTY_3_SCENE& scene, SceneRealty3Func customRealty3Cutscene)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			MODEL_NAMES model
			INT class
			GET_RANDOM_VEHICLE_MODEL_IN_MEMORY(TRUE, model, class)
			
			IF model = DUMMY_MODEL_FOR_SCRIPT
				SWITCH GET_RANDOM_INT_IN_RANGE(0,10)
					CASE 0	model = TAILGATER	BREAK
					CASE 1	model = BUFFALO		BREAK
					CASE 2	model = COMET2		BREAK
					CASE 3	model = ISSI2		BREAK
					CASE 4	model = SENTINEL	BREAK
					CASE 5	model = DOMINATOR	BREAK
					CASE 6	model = CHEETAH		BREAK
					CASE 7	model = STINGERGT	BREAK
					CASE 8	model = INFERNUS	BREAK
					CASE 9	model = JACKAL		BREAK
					
					DEFAULT	model = TAILGATER	BREAK
				ENDSWITCH
			ENDIF
			
			REQUEST_MODEL(model)
			WHILE NOT HAS_MODEL_LOADED(model)
				REQUEST_MODEL(model)
				WAIT(0)
			ENDWHILE
			
			VEHICLE_INDEX hVehicle = CREATE_VEHICLE(model, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			SET_MODEL_AS_NO_LONGER_NEEDED(model)
			
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), hVehicle, VS_DRIVER)
		ENDIF
		
		WHILE NOT CALL customRealty3Cutscene(TRUE, scene)
			WAIT(0)
		ENDWHILE
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
								//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF

ENDPROC

PROC Private_Edit_net_realty_3_scene(structSceneTool_Launcher& launcher, INT iBuildingID, SceneRealty3Func customRealty3Cutscene,OBJECT_INDEX &doorBlock, OBJECT_INDEX &garageBlock)

	// Get scene data
	STRUCT_net_realty_3_SCENE		scene
	//structSceneTool tool
	
	// bOutputMissingNetRealtyScenes
	INT iBuilding
	REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
		IF iBuilding = 0
			
		ELSE
			
			PRINTSTRING("property ")
			PRINTSTRING(Private_Get_Net_Realty_Building_Name(iBuilding))
			PRINTSTRING(" ")
			
			IF Private_Get_NET_REALTY_3_SCENE(iBuilding, scene, TRUE)
				PRINTSTRING("has scene three, ")
			ELSE
				PRINTSTRING("missing scene three, ")
			ENDIF
			
			PRINTNL()
		ENDIF
	ENDREPEAT
	PRINTNL()
	//	//	//	//	//	//
	
	Private_Get_net_realty_3_scene(iBuildingID, scene)

	// Add widgets
	START_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool,Private_Get_Net_Realty_Building_Name(iBuildingID))

		START_WIDGET_GROUP("Camera shots")
			START_WIDGET_GROUP(		"Establishing shot")
				ADD_WIDGET_PAN(		"Establish - ",			g_sAmMpPropertyExtSceneTool,scene.mPans,		ENUM_TO_INT(net_realty_3_SCENE_PAN_descent))
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Markers")
			ADD_WIDGET_MARKER(		"MARKER_gotoA",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,	ENUM_TO_INT(net_realty_3_SCENE_MARKER_gotoA))
			ADD_WIDGET_MARKER(		"MARKER_gotoB",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,	ENUM_TO_INT(net_realty_3_SCENE_MARKER_gotoB))
			ADD_WIDGET_MARKER(		"MARKER_gotoREVERSE",	g_sAmMpPropertyExtSceneTool,scene.mMarkers,	ENUM_TO_INT(net_realty_3_SCENE_MARKER_gotoREVERSE))
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Placers")
			ADD_WIDGET_PLACER(		"PLACER_resetCoords",	g_sAmMpPropertyExtSceneTool,scene.mPlacers,	ENUM_TO_INT(net_realty_3_SCENE_PLACER_resetCoords))
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Angled Area")
			ADD_WIDGET_ANGAREA(		"ANGAREA_area0",		g_sAmMpPropertyExtSceneTool,scene.mAngAreas,	ENUM_TO_INT(NET_REALTY_3_SCENE_ANGAREA_area0))
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("*sliders*")
			START_WIDGET_GROUP("Marker")
				START_WIDGET_GROUP("MARKER_gotoA")
					ADD_WIDGET_VECTOR_SLIDER(	"MARKER_gotoA", scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("MARKER_gotoB")
					ADD_WIDGET_VECTOR_SLIDER(	"MARKER_gotoB", scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("MARKER_gotoREVERSE")
					ADD_WIDGET_VECTOR_SLIDER(	"MARKER_gotoREVERSE", scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Placer")
				START_WIDGET_GROUP("PLACER_resetCoords")
					ADD_WIDGET_VECTOR_SLIDER(	"PLACER_resetCoords",	scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
	STOP_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool)
	
	// Warp to player pos marker
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot)
	ENDIF
	
	// Update widgets
	WHILE launcher.bEnableTool
	
		// Update system
		SWITCH SceneTool_UpdateTool(g_sAmMpPropertyExtSceneTool)
			
			CASE SCENETOOL_ACTION_RunScene
				Private_DebugPlaynet_realty_3_scene(scene, customRealty3Cutscene)
			BREAK
			
			CASE SCENETOOL_ACTION_SaveScene
				Private_Savenet_realty_3_scene(scene, iBuildingID)
			BREAK
			
			CASE SCENETOOL_ACTION_UpdateData
				SceneTool_UpdatePans(g_sAmMpPropertyExtSceneTool, scene.mPans)
				SceneTool_UpdateCuts(g_sAmMpPropertyExtSceneTool, scene.mCuts)
				SceneTool_UpdateMarkers(g_sAmMpPropertyExtSceneTool, scene.mMarkers)
				SceneTool_UpdatePlacers(g_sAmMpPropertyExtSceneTool, scene.mPlacers)
				SceneTool_UpdateAngAreas(g_sAmMpPropertyExtSceneTool, scene.mAngAreas)
			BREAK
		
		ENDSWITCH
				
		// Draw markers
		SceneTool_DrawMarker2 (g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(net_realty_3_SCENE_MARKER_gotoA),			200, 0, 200, scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos)
		SceneTool_DrawMarker2 (g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(net_realty_3_SCENE_MARKER_gotoB),			200, 0, 200, scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos)
		
		SceneTool_DrawMarker2 (g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(net_realty_3_SCENE_MARKER_gotoREVERSE),		200, 0, 200, scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos)
		
		
		// Draw placers
		SceneTool_DrawPlacer (g_sAmMpPropertyExtSceneTool, scene.mPlacers,		ENUM_TO_INT(net_realty_3_SCENE_PLACER_resetCoords),		255, 0, 75)
		
		// Draw Angled Areas
		SceneTool_DrawAngArea(g_sAmMpPropertyExtSceneTool, scene.mAngAreas,	ENUM_TO_INT(NET_REALTY_3_SCENE_ANGAREA_area0),			255, 0, 75)
		
		
		KILL_BLOCKING_OBJECTS(doorBlock,garageBlock)
		WAIT(0)

	ENDWHILE
	
	// Cleanup system
	SWITCH SceneTool_DestroyTool(g_sAmMpPropertyExtSceneTool)
		CASE SCENETOOL_ACTION_SaveScene
			Private_Savenet_realty_3_scene(scene, iBuildingID)
		BREAK
	ENDSWITCH
	
ENDPROC

