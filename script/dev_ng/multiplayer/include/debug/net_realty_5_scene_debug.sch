USING "SceneTool_debug.sch"

PROC Private_SaveNET_REALTY_5_SCENE(STRUCT_NET_REALTY_5_SCENE& scene, INT iBuildingID)
	
	TEXT_LABEL_63 str
	str = Private_Get_Net_Realty_Building_Name(iBuildingID)
	str += "	//"
	str += Private_Get_Net_Realty_Building_Description(iBuildingID)
	
	SceneTool_OpenDebugFile("Private_Get_NET_REALTY_5_SCENE()",				str)

		SceneTool_ExportPan(	"NET_REALTY_5_SCENE_PAN_null",				scene.mPans[NET_REALTY_5_SCENE_PAN_null])

		SceneTool_ExportCut(	"NET_REALTY_5_SCENE_CUT_shot",				scene.mCuts[NET_REALTY_5_SCENE_CUT_shot])

		SceneTool_ExportMarker(	"NET_REALTY_5_SCENE_MARKER_walkTo",			scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo])

		SceneTool_ExportPlacer(	"NET_REALTY_5_SCENE_PLACER_startCoords_0",	scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0])
		SceneTool_ExportPlacer(	"NET_REALTY_5_SCENE_PLACER_startCoords_1",	scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1])

		SceneTool_ExportBool(	"bEnablePans[0]",				scene.bEnablePans[0])
		
		SceneTool_ExportFloat(	"fExitDelay",					scene.fExitDelay)

	SceneTool_CloseDebugFile("Private_Get_NET_REALTY_5_SCENE()")
	
ENDPROC

PROC Private_DebugPlayNET_REALTY_5_SCENE(STRUCT_NET_REALTY_5_SCENE& scene, INT iCurrentProperty)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		GARAGE_PED_LEAVE_CUTSCENE_STAGE garagePedLeaveCutStage = GARAGE_PED_LEAVE_CUTSCENE_INIT
		INT iGarageCutsceneBitset, iGarageCutTimer
		BOOL bDisabledRadar
		CAMERA_INDEX camGarageCutscene
		
		WHILE NOT DO_GARAGE_PED_LEAVE_THIS_CUTSCENE(scene, iCurrentProperty,
				garagePedLeaveCutStage,
				iGarageCutsceneBitset, iGarageCutTimer, bDisabledRadar,
				camGarageCutscene)
			WAIT(0)
		ENDWHILE
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
								//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF

ENDPROC

PROC Private_Edit_NET_REALTY_5_SCENE(structSceneTool_Launcher& launcher, INT iBuildingID, INT iCurrentProperty, OBJECT_INDEX &doorBlock, OBJECT_INDEX &garageBlock)

	// Get scene data
	STRUCT_NET_REALTY_5_SCENE		scene
	//structSceneTool tool
	
	// bOutputMissingNetRealtyScenes
	INT iBuilding
	REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
		IF iBuilding = 0
			
		ELSE
			
			PRINTSTRING("property ")
			PRINTSTRING(Private_Get_Net_Realty_Building_Name(iBuilding))
			PRINTSTRING(" ")
			
			IF Private_Get_NET_REALTY_5_SCENE(iBuilding, scene, TRUE)
				PRINTSTRING("has scene five, ")
			ELSE
				PRINTSTRING("missing scene five, ")
			ENDIF
			
			PRINTNL()
		ENDIF
	ENDREPEAT
	PRINTNL()
	//	//	//	//	//	//
	
	Private_Get_NET_REALTY_5_SCENE(iBuildingID, scene)

	// Add widgets
	START_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool,Private_Get_Net_Realty_Building_Name(iBuildingID))

		START_WIDGET_GROUP("Camera shots")
//				START_WIDGET_GROUP(		"Establishing shot")
//					ADD_WIDGET_PAN(		"Establish - ",				g_sAmMpPropertyExtSceneTool,scene.mPans,			ENUM_TO_INT(NET_REALTY_5_SCENE_PAN_walkOut))
//				STOP_WIDGET_GROUP()
			START_WIDGET_GROUP(		"Establishing shot")
				ADD_WIDGET_CUT(		"Establish - ",				g_sAmMpPropertyExtSceneTool,scene.mCuts,			ENUM_TO_INT(NET_REALTY_5_SCENE_CUT_shot))
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Markers")
			ADD_WIDGET_MARKER(		"MARKER_walkTo",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,		ENUM_TO_INT(NET_REALTY_5_SCENE_MARKER_walkTo))
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Placers")
			ADD_WIDGET_PLACER(		"PLACER_startCoords_0",		g_sAmMpPropertyExtSceneTool,scene.mPlacers,		ENUM_TO_INT(NET_REALTY_5_SCENE_PLACER_startCoords_0))
			ADD_WIDGET_PLACER(		"PLACER_startCoords_1",		g_sAmMpPropertyExtSceneTool,scene.mPlacers,		ENUM_TO_INT(NET_REALTY_5_SCENE_PLACER_startCoords_1))
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("*sliders*")
			START_WIDGET_GROUP("Marker")
				START_WIDGET_GROUP("MARKER_walkTo")
					ADD_WIDGET_VECTOR_SLIDER(	"MARKER_walkTo", scene.mMarkers[NET_REALTY_5_SCENE_MARKER_walkTo].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Placer")
				START_WIDGET_GROUP("PLACER_startCoords_0")
					ADD_WIDGET_VECTOR_SLIDER(	"PLACER_startCoords_0",	scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("PLACER_startCoords_1")
					ADD_WIDGET_VECTOR_SLIDER(	"PLACER_startCoords_1",	scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_1].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
	STOP_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool)
	
	// Warp to player pos marker
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].fRot)
	ENDIF
	
	// Update widgets
	WHILE launcher.bEnableTool
	
		// Update system
		SWITCH SceneTool_UpdateTool(g_sAmMpPropertyExtSceneTool)
			
			CASE SCENETOOL_ACTION_RunScene
				Private_DebugPlayNET_REALTY_5_SCENE(scene, iCurrentProperty)	//, customRealty2Cutscene)
			BREAK
			
			CASE SCENETOOL_ACTION_SaveScene
				Private_SaveNET_REALTY_5_SCENE(scene, iBuildingID)
			BREAK
			
			CASE SCENETOOL_ACTION_UpdateData
				SceneTool_UpdatePans(g_sAmMpPropertyExtSceneTool, scene.mPans)
				SceneTool_UpdateCuts(g_sAmMpPropertyExtSceneTool, scene.mCuts)
				SceneTool_UpdateMarkers(g_sAmMpPropertyExtSceneTool, scene.mMarkers)
				SceneTool_UpdatePlacers(g_sAmMpPropertyExtSceneTool, scene.mPlacers)
			BREAK
		
		ENDSWITCH
				
		// Draw markers
		SceneTool_DrawMarker2 (g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(NET_REALTY_5_SCENE_MARKER_walkTo),			200, 0, 200, scene.mPlacers[NET_REALTY_5_SCENE_PLACER_startCoords_0].vPos)
		
		// Draw placers
		SceneTool_DrawPlacer (g_sAmMpPropertyExtSceneTool, scene.mPlacers,	ENUM_TO_INT(NET_REALTY_5_SCENE_PLACER_startCoords_0),		255, 0, 75)
		SceneTool_DrawPlacer (g_sAmMpPropertyExtSceneTool, scene.mPlacers,	ENUM_TO_INT(NET_REALTY_5_SCENE_PLACER_startCoords_1),		255, 0, 75)
		KILL_BLOCKING_OBJECTS(doorBlock,garageBlock)
		WAIT(0)

	ENDWHILE
	
	// Cleanup system
	SWITCH SceneTool_DestroyTool(g_sAmMpPropertyExtSceneTool)
		CASE SCENETOOL_ACTION_SaveScene
			Private_SaveNET_REALTY_5_SCENE(scene, iBuildingID)
		BREAK
	ENDSWITCH

ENDPROC

