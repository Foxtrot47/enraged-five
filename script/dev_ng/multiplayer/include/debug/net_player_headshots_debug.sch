// This header shouldn't be in the release project anyway, but wrap it up just in case
#IF IS_DEBUG_BUILD



USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "net_stat_system.sch"



// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Player_Headshots_Debug.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all Player Headshots debug functionality.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************

PROC Create_Headshot_Widgets()
	START_WIDGET_GROUP("Headshots")
		ADD_WIDGET_BOOL("Start Export Player Headshot Timer", g_sDebugExportPlayerStartExportPlayerHeadshotTimer)
		ADD_WIDGET_BOOL("Force Export Player Headshot", g_sDebugExportPlayerForceImmediateExportPlayerHeadshot)
		ADD_WIDGET_INT_READ_ONLY("Time to Export", g_sDebugExportPlayerHeadshotTimeToExport)
		ADD_WIDGET_INT_READ_ONLY("g_sExportPlayerHeadshotStage", g_sExportPlayerHeadshotStage)
		ADD_WIDGET_BOOL("Reduce Headshot Upload Frequency", g_bReduceHeadshotUploadFrequency)
		ADD_WIDGET_BOOL("g_bUseCachedTransparentLocalPlayerHeadshot[0]", g_bUseCachedTransparentLocalPlayerHeadshot[0])
		ADD_WIDGET_BOOL("g_bUseCachedTransparentLocalPlayerHeadshot[1]", g_bUseCachedTransparentLocalPlayerHeadshot[1])
		ADD_WIDGET_BOOL("g_sExportPlayerHeadshotRequested", g_sExportPlayerHeadshotRequested)
		ADD_WIDGET_BOOL("g_bNewPlayerHeadshotNeedsCreated", g_bNewPlayerHeadshotNeedsCreated)
	STOP_WIDGET_GROUP()
ENDPROC

PROC Update_Headshot_Widgets()
	IF g_bReduceHeadshotUploadFrequency
	#IF IS_DEBUG_BUILD OR GET_COMMANDLINE_PARAM_EXISTS("sc_ReduceHeadshotUploadFrequency") #ENDIF
		IF g_bPlayerHeadshotNeedsUpdating
			g_sDebugExportPlayerHeadshotTimeToExport = GET_TIME_DIFFERENCE(g_sExportPlayerHeadshotCountdown, GET_NETWORK_TIME())
		ELSE
			g_sDebugExportPlayerHeadshotTimeToExport = -1
		ENDIF
	ELSE
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_NEED_TO_UPDATE_HEADSHOT)
			g_sDebugExportPlayerHeadshotTimeToExport = GET_TIME_DIFFERENCE(g_sExportPlayerHeadshotCountdown, GET_NETWORK_TIME())
		ELSE
			g_sDebugExportPlayerHeadshotTimeToExport = -1
		ENDIF
	ENDIF
ENDPROC

// PURPOSE:	Output Details of one Headshot Struct
//
// RETURN PARAMS:		paramHeadshotDetails		a Headshot Details struct
PROC Debug_Output_Headshot_Details_In_One_Line(g_structHeadshotDetailsMP &paramHeadshotDetails, TEXT_LABEL_31 tl31Additional)

	BOOL inNetworkGame = NETWORK_IS_GAME_IN_PROGRESS()
	BOOL isPedHeadshotValid = FALSE
	STRING sTimeout, sValid, sReady, sTXD, sActive
	
	IF (inNetworkGame)
		sTimeout = GET_TIME_AS_STRING(paramHeadshotDetails.hsdTimeout)
	ELSE
		sTimeout = "UNKNOWN - NOT MP"
	ENDIF
	
	isPedHeadshotValid = IS_PEDHEADSHOT_VALID(paramHeadshotDetails.hsdHeadshotID)
	
	IF (isPedHeadshotValid)
		sValid = "(VALID)"
	ELSE
		sValid = "(not valid)"
	ENDIF
	
	IF NOT isPedHeadshotValid
		sReady = "(NOT READY BECAUSE NOT VALID)"
	ELIF (IS_PEDHEADSHOT_READY(paramHeadshotDetails.hsdHeadshotID))
		sReady = "(READY)"
	ELSE
		sReady = "(not ready)"
	ENDIF
	
	IF (IS_PEDHEADSHOT_VALID(paramHeadshotDetails.hsdHeadshotID))
	AND (IS_PEDHEADSHOT_READY(paramHeadshotDetails.hsdHeadshotID))
		sTXD = GET_PEDHEADSHOT_TXD_STRING(paramHeadshotDetails.hsdHeadshotID)
	ELSE
		sTXD = "none"
	ENDIF
	
	IF (paramHeadshotDetails.hsdKeepActive)
		sActive = "(KEEP ACTIVE)"
	ELSE
		sActive = "(DONT KEEP ACTIVE)"
	ENDIF
	
	CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DEBUG === ", tl31Additional, "[", GET_PLAYER_NAME(paramHeadshotDetails.hsdPlayerID), 
									"   (TIMEOUT = ", sTimeout, ")   ",
									sValid, "   ", sReady, "   (TXD = ", sTXD, ")   ", sActive, "]")
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output Details of Generated Headshots
PROC Debug_Output_Generated_MP_Headshots()

	BOOL inNetworkGame = NETWORK_IS_GAME_IN_PROGRESS()
	
	STRING sTime
	IF (inNetworkGame)
		sTime = GET_TIME_AS_STRING(GET_NETWORK_TIME())
	ELSE
		sTime = "UNKNOWN - NOT MP"
	ENDIF
	
	IF (g_numGeneratedHeadshotsMP = 0)
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DEBUG ===      GENERATED HEADSHOTS [Max = ", MAX_MP_HEADSHOTS, "] [Current Time: ", sTime, "]: NONE")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DEBUG ===      GENERATED HEADSHOTS [Max = ", MAX_MP_HEADSHOTS, "] [Current Time: ", sTime, "]:")
	
	INT tempLoop = 0
	TEXT_LABEL_31 tl31Additional
	REPEAT g_numGeneratedHeadshotsMP tempLoop
		tl31Additional = "         "
		tl31Additional += tempLoop
		tl31Additional += ": "
		Debug_Output_Headshot_Details_In_One_Line(g_sGeneratedHeadshotsMP[tempLoop], tl31Additional)
		NET_NL()
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output details of headshot currently being generated
PROC Debug_Output_MP_Headshot_Being_Generated()

	IF NOT (g_sHeadshotBeingGeneratedMP.hsdActive)
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DEBUG ===      CURRENT HEADSHOT BEING GENERATED: NONE")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DEBUG ===      CURRENT HEADSHOT BEING GENERATED: ")
	TEXT_LABEL_31 tl31Additional = "         "
	Debug_Output_Headshot_Details_In_One_Line(g_sHeadshotBeingGeneratedMP, tl31Additional)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output details of headshot currently requested
PROC Debug_Output_MP_Headshots_Requested()
		
	IF (g_sNumPendingHeadshotsMP = 0)
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DEBUG ===      CURRENT HEADSHOT REQUESTS QUEUED [Max = ", MAX_MP_HEADSHOTS, "]: NONE")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DEBUG ===      CURRENT HEADSHOT REQUESTS QUEUED [Max = ", MAX_MP_HEADSHOTS, "]: ")
	
	INT tempLoop = 0
	REPEAT g_sNumPendingHeadshotsMP tempLoop
		IF (IS_NET_PLAYER_OK(g_sPendingHeadshotsMP[tempLoop]))
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DEBUG ===          ", GET_PLAYER_NAME(g_sPendingHeadshotsMP[tempLoop]))
		ELSE
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DEBUG ===          This Player Not OK")
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output details of all generated headshots and requested headshots
PROC Debug_Output_MP_Headshots()

	Debug_Output_Generated_MP_Headshots()
	Debug_Output_MP_Headshot_Being_Generated()
	Debug_Output_MP_Headshots_Requested()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the Code PedHeadshotState ENUM as a string appended to the current line
//
// INPUT PARAMS:		paramHeadshotID			The HeadshotID
PROC Debug_Output_Append_Player_Headshot_State(PEDHEADSHOT_ID paramHeadshotID, PLAYER_INDEX paramPlayerID)

	PEDHEADSHOTSTATE theHeadshotState = GET_PEDHEADSHOT_STATE(paramHeadshotID)
	
	SWITCH (theHeadshotState)
		CASE PHS_INVALID
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== HEADSHOT STATE === ", GET_PLAYER_NAME(paramPlayerID), 
											" Headshot State : PHS_INVALID")
			BREAK
		CASE PHS_QUEUED
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== HEADSHOT STATE === ", GET_PLAYER_NAME(paramPlayerID), 
											" Headshot State : PHS_QUEUED")
			BREAK
		CASE PHS_READY
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== HEADSHOT STATE === ", GET_PLAYER_NAME(paramPlayerID), 
											" Headshot State : PHS_READY")
			BREAK
		CASE PHS_WAITING_ON_TEXTURE
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== HEADSHOT STATE === ", GET_PLAYER_NAME(paramPlayerID), 
											" Headshot State : PHS_WAITING_ON_TEXTURE")
			BREAK
		CASE PHS_FAILED
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== HEADSHOT STATE === ", GET_PLAYER_NAME(paramPlayerID), 
											" Headshot State : PHS_FAILED")
			BREAK
		DEFAULT
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== HEADSHOT STATE === ", GET_PLAYER_NAME(paramPlayerID), 
											" Headshot State : UNKNOWN PEDHEADSHOTSTATE - ADD TO SWITCH STATEMENT")
			SCRIPT_ASSERT("Debug_Output_Append_Headshot_State() - ERROR: Unknown PEDHEADSHOTSTATE. Add to SWITCH Statement. Tell Keith.")
			BREAK
	ENDSWITCH

ENDPROC




#ENDIF	// IS_DEBUG_BUILD

