// This header shouldn't be in the release project anyway, but wrap it up just in case
#IF IS_DEBUG_BUILD



USING "rage_builtins.sch"
USING "globals.sch"

USING "net_missions_shared_debug.sch"
USING "net_heists_hardcoded.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Activity_Selector_Debug.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all MP Activity Selector debug functionality.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************



// ===========================================================================================================
//      Debug Heist Functions
// ===========================================================================================================

// PURPOSE:	Convert Server Heist Stage to String
//
// INPUT PARAMS:		paramServerHeistStageID		The Server Heist Stage ID
// RETURN VALUE:		STRING						The Server Heist Stage as a string
FUNC STRING Convert_Server_Heist_Stage_To_String(g_eHeistStagesServer paramServerHeistStageID)

	SWITCH (paramServerHeistStageID)
		CASE HEIST_SERVER_STAGE_NO_HEIST_SELECTED
			RETURN ("No Heist Selected")
			
		CASE HEIST_SERVER_STAGE_AWAITING_PLAYER_IN_CORONA
			RETURN ("Awaiting Player In Corona")
			
		CASE HEIST_SERVER_STAGE_AWAITING_HEIST_STARTED
			RETURN ("Awaiting Heist Started")
			
		CASE HEIST_SERVER_STAGE_HEIST_IN_PROGRESS
			RETURN ("Heist In Progress")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: SERVER HEIST STAGE ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Convert Client Heist Stage to String
//
// INPUT PARAMS:		paramClientHeistStageID		The Client Heist Stage ID
// RETURN VALUE:		STRING						The Client Heist Stage as a string
FUNC STRING Convert_Client_Heist_Stage_To_String(g_eHeistStagesClient paramClientHeistStageID)

	SWITCH (paramClientHeistStageID)
		CASE HEIST_CLIENT_STAGE_NO_HEIST
			RETURN ("Heist Inactive")
			
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY
			RETURN ("Heist Initial Delay")
			
		CASE HEIST_CLIENT_STAGE_DOWNLOAD_DETAILS_FROM_FINALE
			RETURN ("Downloading Details")
			
		CASE HEIST_CLIENT_STAGE_WAIT_UNTIL_COMMS_ALLOWED
			RETURN ("Heist Comms Wait")
			
		CASE HEIST_CLIENT_STAGE_REQUEST_COMMS
			RETURN ("Heist Comms Request")
			
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_COMMS_TO_END
			RETURN ("Heist Comms Active")
			
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT
			RETURN ("Heist Player Input")
			
		CASE HEIST_CLIENT_STAGE_DISPLAY_HEIST_COST
			RETURN ("Heist Cost Alert")
			
		CASE HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA
			RETURN ("Heist Corona Setup")
			
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE
			RETURN ("Heist Available")
			
		CASE HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_COMPLETE
			RETURN ("Heist Active")
			
		CASE HEIST_CLIENT_STAGE_HEIST_VARIABLES_RESET
			RETURN ("Heist Reset")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: CLIENT HEIST STAGE ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Convert Client Contact Mission Stage to String
//
// INPUT PARAMS:		g_eContactMissionStagesClient		The Client Contact Mission Stage ID
// RETURN VALUE:		STRING								The Client Contact Mission Stage as a string
FUNC STRING Convert_Client_Contact_Mission_Stage_To_String(g_eContactMissionStagesClient paramClientCMStageID)

	SWITCH (paramClientCMStageID)
		CASE CONTACT_MISSION_CLIENT_STAGE_REQUEST_TO_SEND_INVITE		RETURN ("Request To Send Invite")
		CASE CONTACT_MISSION_CLIENT_STAGE_SEND_INVITE					RETURN ("Send Invite")
		CASE CONTACT_MISSION_CLIENT_STAGE_INVITE_SENT					RETURN ("Invite Sent")
		CASE CONTACT_MISSION_CLIENT_STAGE_SEND_INVITE_ADVERSARY			RETURN ("Send Adversary Invite")
		CASE CONTACT_MISSION_CLIENT_STAGE_NO_MISSION					RETURN ("No COntact Mission")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: CLIENT CONTACT MISSION STAGE ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Convert Client Heist Stage to String
//
// INPUT PARAMS:		paramHeistRewardStageID		The Heist Reward Stage ID
// RETURN VALUE:		STRING						The Heist Reward Stage as a string
FUNC STRING Convert_Client_Heist_Reward_Stage_To_String(g_eHeistRewardStage paramHeistRewardStageID)

	SWITCH (paramHeistRewardStageID)
		CASE HEIST_REWARD_STAGE_NO_REWARD
			RETURN ("Heist Reward Inactive")
			
		CASE HEIST_REWARD_STAGE_INITIAL_DELAY
			RETURN ("Heist Reward Delay")
			
		CASE HEIST_REWARD_STAGE_SEND_SMS
			RETURN ("Heist Reward Send SMS")
			
		CASE HEIST_REWARD_STAGE_ACTIVE_SMS
			RETURN ("Heist Reward Active SMS")
			
		CASE HEIST_REWARD_STAGE_DELAY_AFTER_SMS
			RETURN ("Heist Reward Delay After SMS")
			
		CASE HEIST_REWARD_STAGE_SEND_INVITE
			RETURN ("Heist Reward Send Invite")
			
		CASE HEIST_REWARD_STAGE_INVITE_SENT
			RETURN ("Heist Reward Invite Sent")
			
		CASE HEIST_REWARD_STAGE_2nd_INVITE_DELAY
			RETURN ("Heist Reward 2nd Invite Delay")
			
		CASE HEIST_REWARD_STAGE_SEND_2nd_INVITE
			RETURN ("Heist Reward Send 2nd Invite")
			
		CASE HEIST_REWARD_STAGE_ALL_INVITES_SENT
			RETURN ("Heist Reward All Invites Sent")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: HEIST REWARD STAGE ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Convert Heist Reward BatchID to String
//
// INPUT PARAMS:		paramHeistRewardGroupID		The Heist Reward GroupID
// RETURN VALUE:		STRING						The Heist Reward GroupID as a string
FUNC STRING Convert_Reward_GroupID_To_String(g_eHeistRewardGroupID paramHeistRewardGroupID)

	SWITCH (paramHeistRewardGroupID)
		CASE HEIST_REWARD_VERSUS_AFTER_TUTORIAL
			RETURN ("Tutorial Reward")
			
		CASE HEIST_REWARD_VERSUS_AFTER_PRISON
			RETURN ("Prison Reward")
			
		CASE HEIST_REWARD_VERSUS_AFTER_HUMANE_LABS
			RETURN ("Humane Labs Reward")
			
		CASE HEIST_REWARD_VERSUS_AFTER_NARCOTICS
			RETURN ("Narcotics Reward")
			
		CASE HEIST_REWARD_VERSUS_AFTER_ORNATE_BANK
			RETURN ("Ornate Bank Reward")
			
		CASE NO_HEIST_REWARD_GROUP
			RETURN ("No Heist Reward Group")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: SERVER HEIST STAGE ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the details are consistent for Activity Selector
//
// INPUT PARAMS:			paramContact			The activity's Contact (or NO_CHARACTER)
//							paramRank				The activity's rank
// REFERENCE PARAMS:		refMissionIdData		Mission Identification Data
FUNC BOOL Are_Details_Consistent_For_Activity_Selector(MP_MISSION_ID_DATA &refMissionIdData, enumCharacterList paramContact, INT paramRank)

	SWITCH (refMissionIdData.idMission)
		// Allow these
		CASE eFM_MISSION_CLOUD
			RETURN TRUE
	ENDSWITCH
			
	// Assert for everything else
	NET_PRINT("...KGM MP [ActSelect]: Are_Details_Consistent_For_Activity_Selector() - ERROR: Found an illegal activity being registered as a Pending Activity.") NET_NL()
	NET_PRINT("      MissionID     : ")		NET_PRINT(GET_MP_MISSION_NAME(refMissionIdData.idMission))		NET_NL()
	NET_PRINT("      Variation     : ")		NET_PRINT_INT(refMissionIdData.idVariation)						NET_NL()
	NET_PRINT("      CreatorID     : ")		NET_PRINT_INT(refMissionIdData.idCreator)						NET_NL()
	NET_PRINT("      Cloud Filename: ")		NET_PRINT(refMissionIdData.idCloudFilename)						NET_NL()
	NET_PRINT("      SharedRegID   : ")		NET_PRINT_INT(refMissionIdData.idSharedRegID)					NET_NL()
	NET_PRINT("      ContactID     : ")		NET_PRINT_INT(ENUM_TO_INT(paramContact))						NET_NL()
	NET_PRINT("      Rank          : ")		NET_PRINT_INT(paramRank)										NET_NL()
	
	SCRIPT_ASSERT("Are_Details_Consistent_For_Activity_Selector(): ERROR - Illegal Activity Type being Registered as a Pending Activity. Ditching this activity. See Console Log. Tell Keith, to pass to Activity Owner")
	
	RETURN FALSE
		
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the contents of the local Contact Missions Active array
PROC Debug_Output_Active_CMs_Array()
	
	// Use the missionsatcoords stored version of the player rank
	INT playerRank = g_sAtCoordsControlsMP.matccPlayerRank

	INT maxAtOnce = 0
	IF (playerRank >= MIN_CM_RANK_ALLOWING_3_CM_MAX)
		maxAtOnce = 3
	ELIF (playerRank >= MIN_CM_RANK_ALLOWING_2_CM_MAX)
		maxAtOnce = 2
	ELIF (playerRank >= MIN_CM_RANK_ALLOWING_1_CM_MAX)
		maxAtOnce = 1
	ENDIF

	NET_PRINT("...KGM MP [ActSelect][ContactMission]: Local Active Contact Missions Array [Player Rank: ")
	NET_PRINT_INT(playerRank)
	NET_PRINT("] [Max Missions At Once: ")
	NET_PRINT_INT(maxAtOnce)
	NET_PRINT("]")
	NET_NL()
	
	g_eFMHeistContactIDs	thisHeistContactID	= NO_FM_HEIST_CONTACT
	enumCharacterList		thisCharacter		= NO_CHARACTER
	TEXT_LABEL_31			thisMission			= ""
	INT						tempLoop			= 0
	
	REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE tempLoop
		NET_PRINT("      ")
		NET_PRINT_INT(tempLoop)
		NET_PRINT(": ")
		thisHeistContactID = INT_TO_ENUM(g_eFMHeistContactIDs, g_lcacmActiveCMs[tempLoop].lcacmContact)
		IF (thisHeistContactID != NO_FM_HEIST_CONTACT)
			thisCharacter = Convert_FM_Heist_ContactID_To_CharacterListID(thisHeistContactID)
			IF (thisCharacter != NO_CHARACTER)
				NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[thisCharacter].label))
				NET_PRINT("   [")
				thisMission = Get_Mission_Name_For_FM_Cloud_Loaded_Activity(g_lcacmActiveCMs[tempLoop].lcacmMissionIdData)
				NET_PRINT(thisMission)
				NET_PRINT("]")
				IF (g_lcacmActiveCMs[tempLoop].lcacmForcedAtRank)
					NET_PRINT(" [FORCED AT RANK]")
				ENDIF
				IF (g_lcacmActiveCMs[tempLoop].lcacmPreviouslyCompleted)
					NET_PRINT(" [PREVIOUSLY COMPLETED MISSION]")
				ELSE
					NET_PRINT(" [NEW MISSION]")
				ENDIF
			ELSE
				NET_PRINT("NO CONTACT, SO NO MISSION")
			ENDIF
		ELSE
			NET_PRINT("NO CONTACT, SO NO MISSION")
		ENDIF
		IF (tempLoop >= maxAtOnce)
			NET_PRINT("  - EXCEEDS MAX CMs AT ONCE BASED ON RANK, SO IGNORED IN-GAME")
		ENDIF
		NET_NL()
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Debug Output the player broadcast array of Active CMs
PROC Debug_Output_PlayerBD_Active_CMs()

	INT tempLoop = 0
	
	NET_PRINT("...KGM MP [ActSelect][ContactMission]: Updated Array of Player BD active ContentID Hashes") NET_NL()
	
	REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE tempLoop
		NET_PRINT("      ")
		NET_PRINT_INT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcActiveContentIdHashes[tempLoop])
		NET_NL()
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Debug Output Contact Mission Details on one line
//
// INPUT PARAMS:			paramArrayPosCM			The Contact Mission array position containing the data
PROC Debug_Output_Local_Contact_Missions_Slot_Details_On_One_Line(INT paramArrayPosCM)

	NET_PRINT_INT(paramArrayPosCM)
	IF (paramArrayPosCM < 10)
		NET_PRINT(" ")
	ENDIF
	IF (paramArrayPosCM < 100)
		NET_PRINT(" ")
	ENDIF
	NET_PRINT(" [CreatorID: ")			NET_PRINT_INT(g_sLocalMPCMs[paramArrayPosCM].lcmCreatorID)			NET_PRINT("]")
	NET_PRINT(" [Variation: ")			NET_PRINT_INT(g_sLocalMPCMs[paramArrayPosCM].lcmVariation)			NET_PRINT("]")
	NET_PRINT(" [Rank: ")				NET_PRINT_INT(g_sLocalMPCMs[paramArrayPosCM].lcmRank)				NET_PRINT("]")
	NET_PRINT(" [ContentId Hash: ")		NET_PRINT_INT(g_sLocalMPCMs[paramArrayPosCM].lcmContentIdHash)		NET_PRINT("]")
	NET_PRINT(" [RootContentId Hash: ")	NET_PRINT_INT(g_sLocalMPCMs[paramArrayPosCM].lcmRootContentIdHash)	NET_PRINT("]")
	NET_PRINT(" [Character: ")
	NET_PRINT_INT(ENUM_TO_INT(g_sLocalMPCMs[paramArrayPosCM].lcmContact))
	enumCharacterList thisCharacter = Convert_FM_Heist_ContactID_To_CharacterListID(g_sLocalMPCMs[paramArrayPosCM].lcmContact)
	NET_PRINT(" - ")
	IF (thisCharacter = NO_CHARACTER)
		NET_PRINT("none")
	ELSE
		NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[thisCharacter].label))
	ENDIF
	NET_PRINT("]")
	IF (g_sLocalMPCMs[paramArrayPosCM].lcmForcedAtRank)
		NET_PRINT("   [FORCED AT RANK]")
	ENDIF
	IF (g_sLocalMPCMs[paramArrayPosCM].lcmOnePlay)
		NET_PRINT("   [ONE PLAY ONLY]")
	ENDIF
	IF (g_sLocalMPCMs[paramArrayPosCM].lcmPlayed)
		NET_PRINT("  - PLAYED")
	ENDIF
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the contents of the local Contact Missions array after a cloud refresh
PROC Debug_Output_Local_Contact_Missions()

	
	NET_PRINT("...KGM MP [ActSelect][ContactMission]: Local Player's Contact Missions: ")
	IF (g_numLocalMPCMs = 0)
		NET_PRINT("<NONE>")
	ENDIF
	NET_NL()

	INT tempLoop = 0
	
	REPEAT g_numLocalMPCMs tempLoop
		NET_PRINT("      ")
		Debug_Output_Local_Contact_Missions_Slot_Details_On_One_Line(tempLoop)
		NET_NL()
	ENDREPEAT
	
	NET_PRINT("           TOTAL: ")
	NET_PRINT_INT(g_numLocalMPCMs)
	NET_PRINT("  [MAX: ")
	NET_PRINT_INT(MAX_CONTACT_MISSIONS)
	NET_PRINT("]")
	NET_NL()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the number of contact missions played per contact
PROC Debug_Output_Contact_Missions_Played_Per_Contact()
	
	NET_PRINT("...KGM MP [ActSelect][ContactMission]: Contact Missions played per Contact (all at previous CM Ranks, + 'played' at current CM Rank)") NET_NL()

	INT tempLoop = 0
	
	REPEAT MAX_FM_HEIST_CONTACTS tempLoop
		g_eFMHeistContactIDs thisHeistContactID = INT_TO_ENUM(g_eFMHeistContactIDs, tempLoop)
		enumCharacterList thisCharacter = Convert_FM_Heist_ContactID_To_CharacterListID(thisHeistContactID)
		IF (thisCharacter != NO_CHARACTER)
			NET_PRINT("      ")
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[thisCharacter].label))
			NET_PRINT("   [")
			NET_PRINT_INT(g_cmsPlayedPerContact.cmppcPlayed[tempLoop])
			NET_PRINT("]")
			NET_NL()
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Debug Output Heist Strand Details on one line
//
// INPUT PARAMS:			paramArrayPosHeist			The Heist Strand array position containing the data
PROC Debug_Output_Local_Heist_Strands_Slot_Details_On_One_Line(INT paramArrayPosHeist)

	NET_PRINT_INT(paramArrayPosHeist)
	IF (paramArrayPosHeist < 10)
		NET_PRINT(" ")
	ENDIF
	NET_PRINT(" [ContentId: ")			NET_PRINT(g_sLocalMPHeists[paramArrayPosHeist].lhsMissionIdData.idCloudFilename)	NET_PRINT("]")
	NET_PRINT(" [CreatorID: ")			NET_PRINT_INT(g_sLocalMPHeists[paramArrayPosHeist].lhsMissionIdData.idCreator)		NET_PRINT("]")
	NET_PRINT(" [Variation: ")			NET_PRINT_INT(g_sLocalMPHeists[paramArrayPosHeist].lhsMissionIdData.idVariation)	NET_PRINT("]")
	NET_PRINT(" [Rank: ")				NET_PRINT_INT(g_sLocalMPHeists[paramArrayPosHeist].lhsRank)							NET_PRINT("]")
	NET_PRINT(" [Character: ")
	NET_PRINT_INT(ENUM_TO_INT(g_sLocalMPHeists[paramArrayPosHeist].lhsContact))
	enumCharacterList thisCharacter = Convert_FM_Heist_ContactID_To_CharacterListID(g_sLocalMPHeists[paramArrayPosHeist].lhsContact)
	NET_PRINT(" - ")
	IF (thisCharacter = NO_CHARACTER)
		NET_PRINT("none")
	ELSE
		NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[thisCharacter].label))
	ENDIF
	NET_PRINT("]")
		
ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the contents of the local Heist Strands array after a cloud refresh
PROC Debug_Output_Local_Heist_Strands()
	
	NET_PRINT("...KGM MP [ActSelect][Heist]: Local Player's Heist Strands: ")
	IF (g_numLocalMPHeists = 0)
		NET_PRINT("<NONE>")
	ENDIF
	NET_NL()

	INT tempLoop = 0
	
	REPEAT g_numLocalMPHeists tempLoop
		NET_PRINT("      ")
		Debug_Output_Local_Heist_Strands_Slot_Details_On_One_Line(tempLoop)
		NET_NL()
	ENDREPEAT
	
	NET_PRINT("           TOTAL: ")
	NET_PRINT_INT(g_numLocalMPHeists)
	NET_PRINT("  [MAX: ")
	NET_PRINT_INT(MAX_HEIST_STRANDS)
	NET_PRINT("]")
	NET_NL()

ENDPROC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the contents of any local Activity Selector arrays after a cloud refresh
PROC Debug_Output_Local_Activity_Selector_Details_After_Refresh()
	// Contact Missions
	Debug_Output_Local_Contact_Missions()
	Debug_Output_Contact_Missions_Played_Per_Contact()
	Debug_Output_Active_CMs_Array()
	
	// Heist Strands
	Debug_Output_Local_Heist_Strands()
		// FEATURE_HEIST_PLANNING
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the MissionID Data for the chosen Contact Mission
PROC Debug_Output_MissionID_Data_For_Chosen_Contact_Mission_On_One_Line()

	MP_MISSION_ID_DATA	theMissionIdData	= GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcMissionIdData
	TEXT_LABEL_63		theMissionName		= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(theMissionIdData)
	
	NET_PRINT(theMissionName)
	NET_PRINT(" (")
	NET_PRINT(theMissionIdData.idCloudFilename)
	NET_PRINT(") - ")
	NET_PRINT("Creator: ")
	NET_PRINT_INT(theMissionIdData.idCreator)
	NET_PRINT("  Variation: ")
	NET_PRINT_INT(theMissionIdData.idVariation)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the Local Copy of the Contact Mission Details at the current COntact Mission Rank
PROC Debug_Output_Contact_Mission_Rank_Details()

	NET_PRINT("...KGM MP [ActSelect][ContactMission]: RootContentIDs of Contact Missions Played from CM Min Rank: ")
	NET_PRINT_INT(g_CMsPlayed.cmrmMinRank)
	NET_NL()
	
	INT tempLoop = 0
	REPEAT MAX_CM_CONTENTIDS_PLAYED tempLoop
		NET_PRINT("      ")
		NET_PRINT_INT(tempLoop)
		NET_PRINT("  Hash: ")
		NET_PRINT_INT(g_CMsPlayed.cmrmHashContentIdsComplete[tempLoop])
		NET_NL()
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Debug Output Active Gang Attack Details on one line
//
// INPUT PARAMS:			paramArrayPosActiveGA			The Active Gang ATtack array position containing the data
PROC Debug_Output_Active_Gang_Attacks_Slot_Details_On_One_Line(INT paramArrayPosActiveGA)

	NET_PRINT_INT(paramArrayPosActiveGA)
	IF (paramArrayPosActiveGA < 10)
		NET_PRINT(" ")
	ENDIF
	IF (paramArrayPosActiveGA < 100)
		NET_PRINT(" ")
	ENDIF
	NET_PRINT("  [Shared Missions Slot: ")	NET_PRINT_INT(GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[paramArrayPosActiveGA].agasSharedMissionSlot)		NET_PRINT("]")
	NET_PRINT("  [Cloud Filename: ")		NET_PRINT(GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[paramArrayPosActiveGA].agasCloudFilename)				NET_PRINT("]")
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the Active Gang Attacks array
PROC Debug_Output_Active_Gang_Attacks()

	INT numActiveGA 	= GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasNumActive
	INT numCheckedGA	= 0

	NET_PRINT("...KGM MP [ActSelect][GangAttack]: Active Gang Attacks: ")
	IF (numActiveGA = 0)
		NET_PRINT("<NONE>")
	ELSE
		NET_PRINT_INT(numActiveGA)
	ENDIF
	NET_NL()

	INT tempLoop = 0
	
	REPEAT MAX_ACTIVE_GANG_ATTACKS tempLoop
		IF (GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[tempLoop].agasSharedMissionSlot != NO_ACTIVE_GANG_ATTACK_SLOT)
			// This slot contains an active Gang Attack
			NET_PRINT("      ")
			Debug_Output_Active_Gang_Attacks_Slot_Details_On_One_Line(tempLoop)
			NET_NL()
			
			// If we've processed al the active Gang Attacks, then quit
			numCheckedGA++
			IF (numCheckedGA >= numActiveGA)
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Debug Output Server Gang Attack History Details on one line
//
// INPUT PARAMS:			paramServerHistoryGASlot			The Server History List slot to be output
PROC Debug_Output_Gang_Attack_Server_Recent_History_Details_In_One_Line(INT paramServerHistoryGASlot)

	NET_PRINT_INT(paramServerHistoryGASlot)
	IF (paramServerHistoryGASlot < 10)
		NET_PRINT(" ")
	ENDIF
	IF (paramServerHistoryGASlot < 100)
		NET_PRINT(" ")
	ENDIF
	NET_PRINT("  [Cloud Filename: ")	NET_PRINT(GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[paramServerHistoryGASlot].gahsCloudFilename)					NET_PRINT("]")
	NET_PRINT("  [Timeout: ")			NET_PRINT(GET_TIME_AS_STRING(GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[paramServerHistoryGASlot].gahsTimeout))	NET_PRINT("]")
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the Gang Attacks Server History array
PROC Debug_Output_Gang_Attack_Server_Recent_History()

	NET_PRINT("...KGM MP [ActSelect][GangAttack]: Gang Attacks History: Current Time: ")
	NET_PRINT(GET_TIME_AS_STRING(GET_NETWORK_TIME()))
	NET_NL()

	INT		tempLoop	= 0
	BOOL	foundGA		= FALSE
	
	REPEAT MAX_GANG_ATTACK_RECENT_HISTORY tempLoop
		IF NOT (IS_STRING_NULL_OR_EMPTY(GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[tempLoop].gahsCloudFilename))
			// This slot contains a Gang Attack History
			foundGA = TRUE
			NET_PRINT("      ")
			Debug_Output_Gang_Attack_Server_Recent_History_Details_In_One_Line(tempLoop)
			NET_NL()
		ENDIF
	ENDREPEAT
	
	IF NOT (foundGA)
		NET_PRINT("         <NONE>") NET_NL()
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Debug Output the Heist Corona Details in an apartment
PROC Debug_Output_Heist_Corona_Details()

	// KGM 2/7/14: Added safety checks to fix overrun [BUG 1911978]
	INT	playerSafehouseID = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
	IF (playerSafehouseID <= NO_OWNED_SAFEHOUSE)
		EXIT
	ENDIF
	
	VECTOR heistPosition = mpProperties[playerSafehouseID].house.vHeistPlanningLocate

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][Heist][HCorona]: Heist Corona Coords: ")
		NET_PRINT_VECTOR(heistPosition)
		NET_PRINT("  [Apartment ID: ")
		NET_PRINT_INT(playerSafehouseID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD
// PURPOSE:	Debug Output the Planning details stored as Player Broadcast data
PROC Debug_Output_Heist_Planning_Broadcast_Details()

	INT tempLoop = 0
	
	REPEAT MAX_HEIST_PLANNING_MISSIONS tempLoop
		PRINTLN("...KGM MP [ActSelect][Heist]: ...", tempLoop, ") [PBD] Board Level: ", GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[tempLoop].iBoardLevel, ".  Planning RCID: ", GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].sSetupMissionData[tempLoop].tl23MissionRootContID)
	ENDREPEAT
		
ENDPROC
#ENDIF



// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Debug Output the Heist Unlocks Control Bitset
PROC Debug_Output_Heist_Unlocks_Control_Bitset()

	PRINTLN(".KGM [ActSelect][Heist]: Heist Unlocks Control Bitflags:")
	
	IF (g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks = HEIST_UNLOCKS_ALL_CLEAR)
		PRINTLN(".KGM [ActSelect][Heist]: ...ALL LOCKED")
		EXIT
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_AVAILABLE))
		PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST VERSUS MISSIONS        - *** invite unlocked")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST VERSUS MISSIONS        - INVITE LOCKED")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_SMS_SENT))
		PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST VERSUS MISSIONS        - *** sms sent")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST VERSUS MISSIONS        - SMS NOT SENT")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_PLAYED))
		PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST VERSUS MISSIONS        - *** played")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST VERSUS MISSIONS        - not played")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_1_PLAYED_2nd))
		PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST 2nd VERSUS MISSIONS    - *** played")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST 2nd VERSUS MISSIONS    - not played")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_AVAILABLE))
		PRINTLN(".KGM [ActSelect][Heist]: ...PRISON HEIST VERSUS MISSIONS          - *** invite unlocked")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...PRISON HEIST VERSUS MISSIONS          - INVITE LOCKED")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_PLAYED))
		PRINTLN(".KGM [ActSelect][Heist]: ...PRISON HEIST VERSUS MISSIONS          - *** played")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...PRISON HEIST VERSUS MISSIONS          - not played")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_2_PLAYED_2nd))
		PRINTLN(".KGM [ActSelect][Heist]: ...PRISON HEIST 2nd VERSUS MISSIONS      - *** played")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...PRISON HEIST 2nd VERSUS MISSIONS      - not played")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_AVAILABLE))
		PRINTLN(".KGM [ActSelect][Heist]: ...HUMANE_LABS HEIST VERSUS MISSIONS     - *** invite unlocked")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...HUMANE_LABS HEIST VERSUS MISSIONS     - INVITE LOCKED")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_PLAYED))
		PRINTLN(".KGM [ActSelect][Heist]: ...HUMANE_LABS HEIST VERSUS MISSIONS     - *** played")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...HUMANE_LABS HEIST VERSUS MISSIONS     - not played")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_3_PLAYED_2nd))
		PRINTLN(".KGM [ActSelect][Heist]: ...HUMANE_LABS HEIST 2nd VERSUS MISSIONS - *** played")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...HUMANE_LABS HEIST 2nd VERSUS MISSIONS - not played")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_4_AVAILABLE))
		PRINTLN(".KGM [ActSelect][Heist]: ...NARCOTICS HEIST VERSUS MISSIONS       - *** invite unlocked")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...NARCOTICS HEIST VERSUS MISSIONS       - INVITE LOCKED")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_4_PLAYED))
		PRINTLN(".KGM [ActSelect][Heist]: ...NARCOTICS HEIST VERSUS MISSIONS       - *** played")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...NARCOTICS HEIST VERSUS MISSIONS       - not played")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_5_AVAILABLE))
		PRINTLN(".KGM [ActSelect][Heist]: ...ORNATE BANK HEIST VERSUS MISSIONS     - *** invite unlocked")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...ORNATE BANK HEIST VERSUS MISSIONS     - INVITE LOCKED")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_GROUP_5_PLAYED))
		PRINTLN(".KGM [ActSelect][Heist]: ...ORNATE BANK HEIST VERSUS MISSIONS     - *** played")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...ORNATE BANK HEIST VERSUS MISSIONS     - not played")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_1))
		PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST HELP TEXT DISPLAY      - *** ONCE")
		
		IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_2))
			PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST HELP TEXT DISPLAY      - *** TWICE")
		
			IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_TUT_HEIST_HELP_3))
				PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST HELP TEXT DISPLAY      - *** THREE TIMES")
			ENDIF
		ENDIF
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...TUTORIAL HEIST HELP TEXT DISPLAY      - not displayed")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_FLEECA_TUTORIAL))
		PRINTLN(".KGM [ActSelect][Heist]: ...PHONECALL_AFTER_FLEECA_TUTORIAL       - REQUIRED")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...PHONECALL_AFTER_FLEECA_TUTORIAL       - not required")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_PRISON_BREAK))
		PRINTLN(".KGM [ActSelect][Heist]: ...PHONECALL_AFTER_PRISON_BREAK          - REQUIRED")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...PHONECALL_AFTER_PRISON_BREAK          - not required")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_HUMANE_LABS))
		PRINTLN(".KGM [ActSelect][Heist]: ...PHONECALL_AFTER_HUMANE_LABS           - REQUIRED")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...PHONECALL_AFTER_HUMANE_LABS           - not required")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_NARCOTICS))
		PRINTLN(".KGM [ActSelect][Heist]: ...PHONECALL_AFTER_NARCOTICS             - REQUIRED")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...PHONECALL_AFTER_NARCOTICS             - not required")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITSET_PHONECALL_AFTER_PACIFIC_STANDARD))
		PRINTLN(".KGM [ActSelect][Heist]: ...PHONECALL_AFTER_PACIFIC_STANDARD      - REQUIRED")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...PHONECALL_AFTER_PACIFIC_STANDARD      - not required")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_REPLAY_HEIST_HELP_TEXT_REQUIRED))
		PRINTLN(".KGM [ActSelect][Heist]: ...HEIST REPLAY HELP TEXT                - REQUIRED")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...HEIST REPLAY HELP TEXT                - not required")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_ALLOW_DISPLAY_VERSUS_PAUSE_HELP))
		PRINTLN(".KGM [ActSelect][Heist]: ...HEIST VERSUS PAUSE MENU HELP TEXT     - REQUIRED")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...HEIST VERSUS PAUSE MENU HELP TEXT     - not required")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_NEXT_HEIST_QUICK_PHONECALL))
		PRINTLN(".KGM [ActSelect][Heist]: ...NEXT HEIST QUICK PHONECALL            - REQUIRED")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...NEXT HEIST QUICK PHONECALL            - not required")
	ENDIF
	
	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_CUTSCENE_SKIP_HELP_DISPLAYED))
		PRINTLN(".KGM [ActSelect][Heist]: ...HEIST_CUTSCENE_SKIP_HELP              - already displayed")
	ELSE
		PRINTLN(".KGM [ActSelect][Heist]: ...HEIST_CUTSCENE_SKIP_HELP              - TO BE DISPLAYED")
	ENDIF

ENDPROC



// ===========================================================================================================
//      TEMPORARY FUNCTIONS to hardcode the known heist missions into the correct data format
//		(This will be removed when all of this data can be setup and retrieved from the cloud)
// ===========================================================================================================
// PURPOSE:	Check if this rootContentIdHash is for a known Heist Planning Mission
//
// INPUT PARAMS:		paramRcidHash			The RootContentIdHash
// RETURN VALUE:		BOOL					TRUE if a Heist Planning mission, otherwise False
FUNC BOOL TEMP_Debug_Is_This_A_Heist_Planning_Mission(INT paramRcidHash)

	SWITCH (paramRcidHash)
		// Ornate Bank (Pacific)
		CASE -1905677235	// Pacific - Photo Trucks
		CASE -1904218009	// Pacific - Witness Security
		CASE -1596291374	// Pacific - Hack
		CASE 911181645		// Pacific - Steal Bike
		CASE 1756125549		// Pacific - Convoy
		
		// Biolab (Humane Labs)
		CASE -2079336134	// Humane - Key Codes
		CASE 2039847454		// Humane - Steal Armordillos
		CASE 2136235844		// Humane - Steal EMP
		CASE -365482269		// Humane - Steal Valkyrie
		CASE 496643418		// Humane - Deliver EMP
		
		// Prison Break
		CASE 1993250643		// Prison Break - Plane
		CASE 1637355529		// Prison Break - Bus
		CASE 1089168362		// Prison Break - Police Station
		CASE -12340551		// Prison Break - Unfinished Biz
		
		// Chicken (Narcotics)
		CASE -590337633		// Narcotics - Coke
		CASE -291064527		// Narcotics - Trash Truck
		CASE -630015171		// Narcotics - Heroin
		CASE -19483040		// Narcotics - Weed
		CASE 1585746186		// Narcotics - Steal Meth
		
		// Tutorial
		CASE -240596566		// Tutorial - Flecca
		CASE -2049205985	// Tutorial - Car
		
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this rootContentIdHash is for a known Heist Finale Mission
//
// INPUT PARAMS:		paramRcidHash			The RootContentIdHash
// RETURN VALUE:		BOOL					TRUE if a Heist Finale mission, otherwise False
FUNC BOOL TEMP_Debug_Is_This_A_Heist_Finale_Mission(INT paramRcidHash)
	RETURN (Is_This_A_Known_Heist_Finale(paramRcidHash))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve a literal heist decription string based ona Finale rootContentID
//
// INPUT PARAMS:		paramFinaleRCID			The order the heists should get setup when using NUMPAD-7 (ie: 0 = prison, 1 = ornate, 2 = biolab)
// RETURN VALUE:		BOOL					TRUE if a Heist Finale mission, otherwise False
//
// NOTES:	Swap the order in this SWITCH statement to change the NUMPAD-7 cycling oreder
FUNC TEXT_LABEL_63 TEMP_Debug_Get_Heist_Description_From_FinaleRCID(TEXT_LABEL_23 paramFinaleRCID)

	TEXT_LABEL_63 theHeistDescription = "UNKNOWN"
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFinaleRCID))
		RETURN (theHeistDescription)
	ENDIF
	
	// Ornate Bank Heist?
	IF (ARE_STRINGS_EQUAL(paramFinaleRCID, "hKSf9RCT8UiaZlykyGrMwg"))
		theHeistDescription = "Ornate Bank Heist"
		RETURN (theHeistDescription)
	ENDIF
	
	// Biolab Heist?
	IF (ARE_STRINGS_EQUAL(paramFinaleRCID, "a_hWnpMUz0-7Yd_Rc5pJ4w"))
		theHeistDescription = "Biolab Heist"
		RETURN (theHeistDescription)
	ENDIF
	
	// Prison Heist?
	IF (ARE_STRINGS_EQUAL(paramFinaleRCID, "A6UBSyF61kiveglc58lm2Q"))
		theHeistDescription = "Prison Heist"
		RETURN (theHeistDescription)
	ENDIF
	
	// Chicken Heist?
	IF (ARE_STRINGS_EQUAL(paramFinaleRCID, "7r5AKL5aB0qe9HiDy3nW8w"))
		theHeistDescription = "Narcotics Heist"
		RETURN (theHeistDescription)
	ENDIF
	
	// Tutorial Heist?
	IF (ARE_STRINGS_EQUAL(paramFinaleRCID, "33TxqLipLUintwlU_YDzMg"))
		theHeistDescription = "Tutorial Heist"
		RETURN (theHeistDescription)
	ENDIF
	
	RETURN (theHeistDescription)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the rootContentID for the Finale Missions (for NUMPAD-7 launching)
//
// INPUT PARAMS:		paramNumpad7Index		The order the heists should get setup when using NUMPAD-7 (ie: 0 = prison, 1 = ornate, 2 = biolab)
// RETURN VALUE:		BOOL					TRUE if a Heist Finale mission, otherwise False
//
// NOTES:	Swap the order in this SWITCH statement to change the NUMPAD-7 cycling oreder
FUNC TEXT_LABEL_23 TEMP_Debug_Get_Heist_Finale_RCID_From_Numpad7_Value(INT paramNumpad7Index)

	TEXT_LABEL_23 theReturnRCID = ""

	SWITCH (paramNumpad7Index)
		CASE 0		theReturnRCID = "33TxqLipLUintwlU_YDzMg"		BREAK		// Tutorial - Finale
		CASE 1		theReturnRCID = "A6UBSyF61kiveglc58lm2Q"		BREAK		// Prison Break - Finale 
		CASE 2		theReturnRCID = "a_hWnpMUz0-7Yd_Rc5pJ4w"		BREAK		// Biolab Score - Finale
		CASE 3		theReturnRCID = "7r5AKL5aB0qe9HiDy3nW8w"		BREAK		// Narcotics - Finale
		CASE 4		theReturnRCID = "hKSf9RCT8UiaZlykyGrMwg"		BREAK		// Ornate Bank Heist - Finale	
		
		DEFAULT
			PRINTLN(".KGM [Heist][TEMP HARDCODED]: Illegal NUMPAD-7 Index when choosing next NUMPAD-7 Heist Strand")
			BREAK
	ENDSWITCH
	
	RETURN (theReturnRCID)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Ensure that all the known heists get setup as Heists if they aren't tagged as such
PROC TEMP_Debug_Force_Heist_Strand_Data()

	PRINTLN(".KGM [Heist]: TEMP: Checking the known Heist Finale's for any that aren't tagged as 'Heist'")
	
	// Loop through all regular rockstar created missions looking for the Heist missions (ignore locations 500+)
	INT tempLoop = 0
	BOOL foundUntaggedHeist = FALSE
	
	
	REPEAT FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD tempLoop
		IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
			IF (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iType = FMMC_TYPE_MISSION)
			AND (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iSubType != FMMC_MISSION_TYPE_HEIST)
				IF (TEMP_Debug_Is_This_A_Heist_Finale_Mission(g_FMMC_ROCKSTAR_CREATED.sDefaultCoronaOptions[tempLoop].iRootContentIdHash))
					// Found a 'known' heist that isn't tagged as a Heist
					PRINTLN(".KGM [Heist]: ...Found a known Heist Finale not tagged as 'Heist' Array: ", tempLoop, " - ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].tlMissionName)
					SCRIPT_ASSERT("TEMP_Debug_Force_Heist_Strand_Data() - Found a Known Heist Finale not tagged as 'Heist'. IGNORING. Tell Keith.")
					foundUntaggedHeist = TRUE
				ENDIF
			ENDIF
			
			// Safety Check - look for Prep Missions - they shouldn't be in the array
			IF (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iType = FMMC_TYPE_MISSION)
			AND (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].iSubType = FMMC_MISSION_TYPE_PLANNING)
				// Found a Planning Mission - these shouldn't be coming into the game along with the other Rockstar Created content, these should get downloaded when requested
				PRINTLN(".KGM [Heist]: ...Found a mission tagged a 'Planning'. Array: ", tempLoop, " - ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[tempLoop].tlMissionName)
				SCRIPT_ASSERT("TEMP_Debug_Force_Heist_Strand_Data() - Found a mission tagged a 'Planning'. THESE SHOULDN'T BE COMING INTO THE GAME. Tell Keith.")
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT (foundUntaggedHeist)
		PRINTLN(".KGM [Heist]: ...NONE - all should be good")
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Do some consistency checking on the heist being setup
//
// REFERENCE PARAMS:		refMissionIdData		Mission Identification Data
//							paramContact			The activity's Contact (or NO_CHARACTER)
//							paramRank				The activity's rank
// RETURN VALUE:			BOOL					TRUE if this is a Known Heist Finale, FALSE if not
//
FUNC BOOL Debug_Check_Heist_Mission_Data(MP_MISSION_ID_DATA &refMissionIdData, enumCharacterList &paramContact, INT &paramRank)

	// Is this a known heist finale?
	INT finaleRcidHash = Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	IF NOT (Is_This_A_Known_Heist_Finale(finaleRcidHash))
		// ...not a known heist finale, so assert and remove the Heist Tag so that it doesn't get setup as a Heist Strand
		PRINTLN(".KGM [Heist]: Debug_Check_Heist_Mission_Data() - ERROR: This is not a known Heist Finale ContentID: ", refMissionIdData.idCloudFilename)
		SCRIPT_ASSERT("Debug_Check_Heist_Mission_Data() - ERROR: Unrecognised Finale RCID Hash - rootContentID may have changed. Removing 'Heist' tag. Tell Keith.")

		IF (refMissionIdData.idCreator = FMMC_ROCKSTAR_CREATOR_ID)
			// Rockstar Created Heist
			g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[refMissionIdData.idVariation].iSubType = FMMC_MISSION_TYPE_STANDARD
			CLEAR_BIT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[refMissionIdData.idVariation].iBitSet, ciMISSION_OPTION_BS_HEIST_HIGH_END)
			g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[refMissionIdData.idVariation].iContactCharEnum = 0
			g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[refMissionIdData.idVariation].iRank = 9999
		ELSE
			// Non-Rockstar Created Heist
			PRINTLN(".KGM [Heist]: Debug_Check_Heist_Mission_Data() ...the unknown heist is not in the Rockstar Created array")
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	// This is a known Heist Finale, so checking the data consistency
	// ...contact
	IF (paramContact != CHAR_LESTER)
		PRINTLN(".KGM [Heist]: Debug_Check_Heist_Mission_Data() - ERROR: Heist Contact is Not Lester. Temporarily setting as Lester.")
		
		// Update the data
		paramContact = CHAR_LESTER
		IF (refMissionIdData.idCreator = FMMC_ROCKSTAR_CREATOR_ID)
			g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[refMissionIdData.idVariation].iContactCharEnum = -1917614010
		ENDIF
	ENDIF
	
	// ...rank
	IF (paramRank >= 9999)
		PRINTLN(".KGM [Heist]: Debug_Check_Heist_Mission_Data() - ERROR: Finale Heist Rank is: ", paramRank)
		SCRIPT_ASSERT("Debug_Check_Heist_Mission_Data() - ERROR: Finale Heist Rank is too high. NOT SETTING UP HEIST FINALE. Tell Keith.")
		
		RETURN FALSE
	ENDIF
	
	// Allow this Heist Strand to be setup
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// Added by AMEC 01-04-14

// PURPOSE:	Return the Heist Planning Data from the Finale RootContentID
//
// RETURN PARAMS:		paramPlanningData	Return array of Planning Data to simulate the data being extracted from the Heist Finale mission data
PROC TEMP_Debug_Output_Heist_Planning_Data_Struct(g_structHeistPlanningData &paramPlanningData[])

	INT tempLoop = 0
	REPEAT MAX_HEIST_PLANNING_MISSIONS tempLoop
		IF (paramPlanningData[tempLoop].boardLevel >= 0)
			PRINTLN("     ", tempLoop, "  Planning RootContentID: ", paramPlanningData[tempLoop].rootContentID, "[Board Level: ", paramPlanningData[tempLoop].boardLevel, "]")
		ENDIF
	ENDREPEAT
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------

// Added by AMEC 01-04-14
// PURPOSE:	Return the Heist Planning Data from the Finale RootContentID
//
// INPUT PARAMS:		paramRcidHash		The rootContentID for a Finale Heist mission
// RETURN PARAMS:		paramPlanningData	Return array of Planning Data to simulate the data being extracted from the Heist Finale mission data
PROC TEMP_Debug_Extract_Planning_Data_From_Finale_RootContentID(INT paramRcidHash, g_structHeistPlanningData &paramPlanningData[])

	// OBSOLETE: But still used for a debug reason?
	
	PRINTLN(".KGM [Heist][TEMP HARDCODED]: Fill an array of hardcoded data to simulate the data that will be extracted from the Heist Finale mission data")
	
	// Clear the Planning Data struct
	INT tempLoop = 0
	REPEAT MAX_HEIST_PLANNING_MISSIONS tempLoop
		paramPlanningData[tempLoop].rootContentID	= ""
		paramPlanningData[tempLoop].boardLevel		= -1
	ENDREPEAT
	
	// Ensure the rootContentId Hash is a valid Finale Heist mission
	IF NOT (TEMP_Debug_Is_This_A_Heist_Finale_Mission(paramRcidHash))
		PRINTLN(".KGM [Heist][TEMP HARDCODED]: ERROR - TEMP_Debug_Extract_Planning_Data_From_Finale_RootContentID() - the Heist Finale RootContentID isn't recognised: ", paramRcidHash)
		EXIT
	ENDIF
	
//	SWITCH (paramRcidHash)
//		CASE ci_HEIST_STRAND_HASH_ORNATE	// Ornate Bank - Finale
//			paramPlanningData[0].rootContentID	= "NF-_h3E-SEquP-H4ImN5Qg"	// Pacific - Truck Steal
//			paramPlanningData[0].boardLevel		= 0
//			paramPlanningData[1].rootContentID	= "ii4uubTPo0evYw7dTKl_VQ"	// Pacific - Witness Security
//			paramPlanningData[1].boardLevel		= 1
//			paramPlanningData[2].rootContentID	= "OiSO3Z0YdkCaEqVHhhkj4Q"	// Pacific - Hack
//			paramPlanningData[2].boardLevel		= 2
//			paramPlanningData[3].rootContentID	= "Y4zpRQDfvkawfFDR1Uxi2A"	// Pacific - Steal Bike
//			paramPlanningData[3].boardLevel		= 2
//			paramPlanningData[4].rootContentID	= "Cy2OZSwCt0-mSXY00o4SNw"	// Pacific - Convoy
//			paramPlanningData[4].boardLevel		= 2
//			BREAK
			
//		CASE ci_HEIST_STRAND_HASH_BIOLAB	// Biolab Score - Finale
//			paramPlanningData[0].rootContentID	= "k1_TAw7vr0-eqOSfIhdhcg"	// Humane - Key Codes
//			paramPlanningData[0].boardLevel		= 0
//			paramPlanningData[1].rootContentID	= "6k6LOpnf2E-GG38OhjS-TA"	// Humane - Steal Armordillos (old: Gurkhas)
//			paramPlanningData[1].boardLevel		= 0
//			paramPlanningData[2].rootContentID	= "nSWwSwAf3EaHZWsk449lBg"	// Humane - Steal EMP
//			paramPlanningData[2].boardLevel		= 1
//			paramPlanningData[3].rootContentID	= "4J0enuxGhEuNQ56GQsKiRg"	// Humane - Valkyrie
//			paramPlanningData[3].boardLevel		= 2
//			paramPlanningData[4].rootContentID	= "v-8OOQYzxE-Zvqj5xO03DQ"	// Humane - Deliver EMP
//			paramPlanningData[4].boardLevel		= 2
//			BREAK
			
//		CASE ci_HEIST_STRAND_HASH_PRISON		// Prison Break - Finale
//			paramPlanningData[0].rootContentID	= "7yk8blhE3E25VPrKST7-3w"	// Prison Break - Plane
//			paramPlanningData[0].boardLevel		= 0
//			paramPlanningData[1].rootContentID	= "lkqmooVePUq4sh302WGEBw"	// Prison Break - Bus
//			paramPlanningData[1].boardLevel		= 0
//			paramPlanningData[2].rootContentID	= "Hrvux-5DwEGvGaq66R-3vw"	// Prison Break - Station
//			paramPlanningData[2].boardLevel		= 0
//			paramPlanningData[3].rootContentID	= "XMhBkEXGwE6BEdP3cPQcYg"	// Prison Break - Unfinished Biz
//			paramPlanningData[3].boardLevel		= 1
//			BREAK
			
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS		// Narcotics - Finale
//			paramPlanningData[0].rootContentID	= "20Lu41Px20OJMPdZ6wXG3g"	// Narcotics - Coke
//			paramPlanningData[0].boardLevel		= 0
//			paramPlanningData[1].rootContentID	= "q1-80I8t_kWNIQUuxxrQDw"	// Narcotics - Trash Truck
//			paramPlanningData[1].boardLevel		= 0
//			paramPlanningData[2].rootContentID	= "3tYmphlR1ka7IB37wNw45A"	// Narcotics - Heroin
//			paramPlanningData[2].boardLevel		= 0
//			paramPlanningData[3].rootContentID	= "VvP6Ge0YXkGKzQD9wkJfZQ"	// Narcotics - Weed
//			paramPlanningData[3].boardLevel		= 1
//			paramPlanningData[4].rootContentID	= "Fo168mMjCUCeN_IKmL4VnA"	// Narcotics - Steal Meth
//			paramPlanningData[4].boardLevel		= 2
//			BREAK
			
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job		// Tutorial - Finale
//			paramPlanningData[0].rootContentID	= "fMfJTq0PhU2VMY5xoIeeVQ"	// Tutorial Heist - Flecca 
//			paramPlanningData[0].boardLevel		= 0
//			paramPlanningData[1].rootContentID	= "WZuw8hYVjkmnhqxCxkYWnQ"	// Tutorial Heist - Car
//			paramPlanningData[1].boardLevel		= 1
//			BREAK
			
//		DEFAULT
//			PRINTLN(".KGM [Heist][TEMP HARDCODED]: ERROR - TEMP_Debug_Extract_Planning_Data_From_Finale_RootContentID() - the Heist Finale RootContentID has no associated data: ", paramRcidHash)
//			EXIT
//	ENDSWITCH

	IF (Is_This_Tutorial_Heist_RCID_Hash(paramRcidHash))
		paramPlanningData[0].rootContentID	= "fMfJTq0PhU2VMY5xoIeeVQ"	// Tutorial Heist - Flecca 
		paramPlanningData[0].boardLevel		= 0
		paramPlanningData[1].rootContentID	= "WZuw8hYVjkmnhqxCxkYWnQ"	// Tutorial Heist - Car
		paramPlanningData[1].boardLevel		= 1
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(paramRcidHash))
		paramPlanningData[0].rootContentID	= "7yk8blhE3E25VPrKST7-3w"	// Prison Break - Plane
		paramPlanningData[0].boardLevel		= 0
		paramPlanningData[1].rootContentID	= "lkqmooVePUq4sh302WGEBw"	// Prison Break - Bus
		paramPlanningData[1].boardLevel		= 0
		paramPlanningData[2].rootContentID	= "Hrvux-5DwEGvGaq66R-3vw"	// Prison Break - Station
		paramPlanningData[2].boardLevel		= 0
		paramPlanningData[3].rootContentID	= "XMhBkEXGwE6BEdP3cPQcYg"	// Prison Break - Unfinished Biz
		paramPlanningData[3].boardLevel		= 1
	ENDIF

	IF (Is_This_Biolab_Heist_RCID_Hash(paramRcidHash))
		paramPlanningData[0].rootContentID	= "k1_TAw7vr0-eqOSfIhdhcg"	// Humane - Key Codes
		paramPlanningData[0].boardLevel		= 0
		paramPlanningData[1].rootContentID	= "6k6LOpnf2E-GG38OhjS-TA"	// Humane - Steal Armordillos (old: Gurkhas)
		paramPlanningData[1].boardLevel		= 0
		paramPlanningData[2].rootContentID	= "nSWwSwAf3EaHZWsk449lBg"	// Humane - Steal EMP
		paramPlanningData[2].boardLevel		= 1
		paramPlanningData[3].rootContentID	= "4J0enuxGhEuNQ56GQsKiRg"	// Humane - Valkyrie
		paramPlanningData[3].boardLevel		= 2
		paramPlanningData[4].rootContentID	= "v-8OOQYzxE-Zvqj5xO03DQ"	// Humane - Deliver EMP
		paramPlanningData[4].boardLevel		= 2
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(paramRcidHash))
		paramPlanningData[0].rootContentID	= "20Lu41Px20OJMPdZ6wXG3g"	// Narcotics - Coke
		paramPlanningData[0].boardLevel		= 0
		paramPlanningData[1].rootContentID	= "q1-80I8t_kWNIQUuxxrQDw"	// Narcotics - Trash Truck
		paramPlanningData[1].boardLevel		= 0
		paramPlanningData[2].rootContentID	= "3tYmphlR1ka7IB37wNw45A"	// Narcotics - Heroin
		paramPlanningData[2].boardLevel		= 0
		paramPlanningData[3].rootContentID	= "VvP6Ge0YXkGKzQD9wkJfZQ"	// Narcotics - Weed
		paramPlanningData[3].boardLevel		= 1
		paramPlanningData[4].rootContentID	= "Fo168mMjCUCeN_IKmL4VnA"	// Narcotics - Steal Meth
		paramPlanningData[4].boardLevel		= 2
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(paramRcidHash))
		paramPlanningData[0].rootContentID	= "NF-_h3E-SEquP-H4ImN5Qg"	// Pacific - Truck Steal
		paramPlanningData[0].boardLevel		= 0
		paramPlanningData[1].rootContentID	= "ii4uubTPo0evYw7dTKl_VQ"	// Pacific - Witness Security
		paramPlanningData[1].boardLevel		= 1
		paramPlanningData[2].rootContentID	= "OiSO3Z0YdkCaEqVHhhkj4Q"	// Pacific - Hack
		paramPlanningData[2].boardLevel		= 2
		paramPlanningData[3].rootContentID	= "Y4zpRQDfvkawfFDR1Uxi2A"	// Pacific - Steal Bike
		paramPlanningData[3].boardLevel		= 2
		paramPlanningData[4].rootContentID	= "Cy2OZSwCt0-mSXY00o4SNw"	// Pacific - Convoy
		paramPlanningData[4].boardLevel		= 2
	ENDIF

	// Printout the array
	PRINTLN(".KGM [Heist][TEMP HARDCODED]: The Planning Data for this heist Finale rootContentID: ", paramRcidHash)
	TEMP_Debug_Output_Heist_Planning_Data_Struct(paramPlanningData)
	
ENDPROC

FUNC STRING TEMP_Debug_GET_Planning_Mission_Name(INT paramRcidHash, INT iMission)	

//	SWITCH (paramRcidHash)
//		CASE ci_HEIST_STRAND_HASH_ORNATE	// Ornate Bank - Finale
//			SWITCH iMission
//				CASE -1	RETURN "Ornate Bank Heist"
//				CASE 0 	RETURN "Prep - Photo Trucks"			// NF-_h3E-SEquP-H4ImN5Qg
//				CASE 1 	RETURN "Prep - Witness Security"		// ii4uubTPo0evYw7dTKl_VQ
//				CASE 2 	RETURN "Prep - Hack"					// OiSO3Z0YdkCaEqVHhhkj4Q
//				CASE 3 	RETURN "Prep - Steal Bikes"				// Y4zpRQDfvkawfFDR1Uxi2A
//				CASE 4 	RETURN "Prep - Convoy"					// Cy2OZSwCt0-mSXY00o4SNw
//				CASE 5 	RETURN "Finale"							// hKSf9RCT8UiaZlykyGrMwg
//			ENDSWITCH 
//			BREAK
			
//		CASE ci_HEIST_STRAND_HASH_BIOLAB	// Biolab Score - Finale
//			SWITCH iMission
//				CASE -1	RETURN "Biolab Heist"
//				CASE 0 	RETURN "Prep - Key Codes"				// k1_TAw7vr0-eqOSfIhdhcg
//				CASE 1 	RETURN "Prep - Steal Armordillos"		// 6k6LOpnf2E-GG38OhjS-TA
//				CASE 2 	RETURN "Prep - Steal EMP"				// nSWwSwAf3EaHZWsk449lBg
//				CASE 3 	RETURN "Prep - Steal Valkyrie"			// 4J0enuxGhEuNQ56GQsKiRg
//				CASE 4 	RETURN "Prep - Deliver EMP"				// v-8OOQYzxE-Zvqj5xO03DQ
//				CASE 5 	RETURN "Finale"							// a_hWnpMUz0-7Yd_Rc5pJ4w
//			ENDSWITCH 
//			BREAK
			
//		CASE ci_HEIST_STRAND_HASH_PRISON		// Prison Break - Finale
//			SWITCH iMission
//				CASE -1	RETURN "Prison Break Heist"
//				CASE 0 	RETURN "Prep - Plane"					// 7yk8blhE3E25VPrKST7-3w
//				CASE 1 	RETURN "Prep - Bus Steal"				// lkqmooVePUq4sh302WGEBw
//				CASE 2 	RETURN "Prep - Cop Station"				// Hrvux-5DwEGvGaq66R-3vw
//				CASE 3 	RETURN "Prep - Unfinished Biz"			// XMhBkEXGwE6BEdP3cPQcYg
//				CASE 4 	RETURN "Finale"							// A6UBSyF61kiveglc58lm2Q
//			ENDSWITCH 
//			BREAK
			
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS		// Narcotics - Finale
//			SWITCH iMission
//				CASE -1	RETURN "Narcotics Heist"
//				CASE 0 	RETURN "Prep - Coke"					// 20Lu41Px20OJMPdZ6wXG3g
//				CASE 1 	RETURN "Prep - Trash Truck"				// q1-80I8t_kWNIQUuxxrQDw
//				CASE 2 	RETURN "Prep - Heroin"					// 3tYmphlR1ka7IB37wNw45A
//				CASE 3 	RETURN "Prep - Weed"					// VvP6Ge0YXkGKzQD9wkJfZQ
//				CASE 4 	RETURN "Prep - Steal Meth"				// Fo168mMjCUCeN_IKmL4VnA
//				CASE 5 	RETURN "Finale"							// 7r5AKL5aB0qe9HiDy3nW8w
//			ENDSWITCH 
//			BREAK
		
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job		// Tutorial - Finale
//			SWITCH iMission
//				CASE -1	RETURN "Tutorial Heist"
//				CASE 0 	RETURN "Prep - Flecca"					// fMfJTq0PhU2VMY5xoIeeVQ
//				CASE 1 	RETURN "Prep - Car"						// WZuw8hYVjkmnhqxCxkYWnQ
//				CASE 2	RETURN "Finale"							// 33TxqLipLUintwlU_YDzMg
//			ENDSWITCH 
//			BREAK
//	ENDSWITCH
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(paramRcidHash))
		SWITCH iMission
			CASE -1	RETURN "Tutorial Heist"
			CASE 0 	RETURN "Prep - Flecca"					// fMfJTq0PhU2VMY5xoIeeVQ
			CASE 1 	RETURN "Prep - Car"						// WZuw8hYVjkmnhqxCxkYWnQ
			CASE 2	RETURN "Finale"							// 33TxqLipLUintwlU_YDzMg
		ENDSWITCH 
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(paramRcidHash))
		SWITCH iMission
			CASE -1	RETURN "Prison Break Heist"
			CASE 0 	RETURN "Prep - Plane"					// 7yk8blhE3E25VPrKST7-3w
			CASE 1 	RETURN "Prep - Bus Steal"				// lkqmooVePUq4sh302WGEBw
			CASE 2 	RETURN "Prep - Cop Station"				// Hrvux-5DwEGvGaq66R-3vw
			CASE 3 	RETURN "Prep - Unfinished Biz"			// XMhBkEXGwE6BEdP3cPQcYg
			CASE 4 	RETURN "Finale"							// A6UBSyF61kiveglc58lm2Q
		ENDSWITCH 
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(paramRcidHash))
		SWITCH iMission
			CASE -1	RETURN "Biolab Heist"
			CASE 0 	RETURN "Prep - Key Codes"				// k1_TAw7vr0-eqOSfIhdhcg
			CASE 1 	RETURN "Prep - Steal Armordillos"		// 6k6LOpnf2E-GG38OhjS-TA
			CASE 2 	RETURN "Prep - Steal EMP"				// nSWwSwAf3EaHZWsk449lBg
			CASE 3 	RETURN "Prep - Steal Valkyrie"			// 4J0enuxGhEuNQ56GQsKiRg
			CASE 4 	RETURN "Prep - Deliver EMP"				// v-8OOQYzxE-Zvqj5xO03DQ
			CASE 5 	RETURN "Finale"							// a_hWnpMUz0-7Yd_Rc5pJ4w
		ENDSWITCH 
	ENDIF

	IF (Is_This_Chicken_Heist_RCID_Hash(paramRcidHash))
		SWITCH iMission
			CASE -1	RETURN "Narcotics Heist"
			CASE 0 	RETURN "Prep - Coke"					// 20Lu41Px20OJMPdZ6wXG3g
			CASE 1 	RETURN "Prep - Trash Truck"				// q1-80I8t_kWNIQUuxxrQDw
			CASE 2 	RETURN "Prep - Heroin"					// 3tYmphlR1ka7IB37wNw45A
			CASE 3 	RETURN "Prep - Weed"					// VvP6Ge0YXkGKzQD9wkJfZQ
			CASE 4 	RETURN "Prep - Steal Meth"				// Fo168mMjCUCeN_IKmL4VnA
			CASE 5 	RETURN "Finale"							// 7r5AKL5aB0qe9HiDy3nW8w
		ENDSWITCH 
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(paramRcidHash))
		SWITCH iMission
			CASE -1	RETURN "Ornate Bank Heist"
			CASE 0 	RETURN "Prep - Photo Trucks"			// NF-_h3E-SEquP-H4ImN5Qg
			CASE 1 	RETURN "Prep - Witness Security"		// ii4uubTPo0evYw7dTKl_VQ
			CASE 2 	RETURN "Prep - Hack"					// OiSO3Z0YdkCaEqVHhhkj4Q
			CASE 3 	RETURN "Prep - Steal Bikes"				// Y4zpRQDfvkawfFDR1Uxi2A
			CASE 4 	RETURN "Prep - Convoy"					// Cy2OZSwCt0-mSXY00o4SNw
			CASE 5 	RETURN "Finale"							// hKSf9RCT8UiaZlykyGrMwg
		ENDSWITCH 
	ENDIF
	
	// Not Found
	RETURN ""
	
ENDFUNC

FUNC INT GET_HEIST_STRAND_LENGTH(INT paramRcidHash)	

//	SWITCH (paramRcidHash)
//		CASE ci_HEIST_STRAND_HASH_ORNATE					RETURN 6	// Ornate Bank - Finale		1) -1905677235	2) -1904218009	3) -1596291374	4) 911181645	5) 1756125549	F) -231973569
//		CASE ci_HEIST_STRAND_HASH_BIOLAB					RETURN 6	// Biolab Score - Finale	1) -2079336134	2) 2039847454	3) 2136235844	4) -365482269	5) 496643418	F) -1096986654
//		CASE ci_HEIST_STRAND_HASH_PRISON					RETURN 5	// Prison Break - Finale	1) 1993250643	2) 1637355529	3) 1089168362	4) -12340551	F) 979654579
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS					RETURN 6	// Chicken - Finale			1) -590337633	2) -291064527	3) -630015171	4) -19483040	5) 1585746186	F) 164435858
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job		RETURN 3	// Tutorial - Finale		1) -240596566	2) -2049205985	F) -1072870761
//	ENDSWITCH
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(paramRcidHash))
		RETURN 3	// Tutorial - Finale		1) -240596566	2) -2049205985	F) -1072870761
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(paramRcidHash))
		RETURN 5	// Prison Break - Finale	1) 1993250643	2) 1637355529	3) 1089168362	4) -12340551	F) 979654579
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(paramRcidHash))
		RETURN 6	// Biolab Score - Finale	1) -2079336134	2) 2039847454	3) 2136235844	4) -365482269	5) 496643418	F) -1096986654
	ENDIF

	IF (Is_This_Chicken_Heist_RCID_Hash(paramRcidHash))
		RETURN 6	// Chicken - Finale			1) -590337633	2) -291064527	3) -630015171	4) -19483040	5) 1585746186	F) 164435858
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(paramRcidHash))
		RETURN 6	// Ornate Bank - Finale		1) -1905677235	2) -1904218009	3) -1596291374	4) 911181645	5) 1756125549	F) -231973569
	ENDIF
	
	// Not found
	RETURN 0
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Returns the state of the Debug Heist Launch variable
//
// RETURN VALUE:			BOOL		TRUE if the debug flag is TRUE, otherwise FALSE
//
// NOTES:	Assumes various checks have been performed and a new Heist is allowed
FUNC BOOL Check_For_Heist_Debug_Launch()

	IF NOT (g_DEBUG_launchNextHeistStrand)
		RETURN FALSE
	ENDIF
	
	PRINTLN("...KGM MP [ActSelect][Heist]: g_DEBUG_launchNextHeistStrand is TRUE. Assume 'active heist' checks have been done, so allow new Heist")
	g_DEBUG_launchNextHeistStrand = FALSE
	
	RETURN TRUE
		
ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Returns the state of the Debug Heist Cancel variable
//
// RETURN VALUE:			BOOL		TRUE if the debug flag is TRUE, otherwise FALSE
FUNC BOOL Check_For_Heist_Debug_Cancel()

	IF NOT (g_DEBUG_cancelCurrentHeistStrand)
		RETURN FALSE
	ENDIF
	
	PRINTLN("...KGM MP [ActSelect][Heist]: g_DEBUG_cancelCurrentHeistStrand is TRUE. Cancel current Heist strand.")
	g_DEBUG_cancelCurrentHeistStrand = FALSE
	
	RETURN TRUE
		
ENDFUNC
	// FEATURE_HEIST_PLANNING



#ENDIF	// IS_DEBUG_BUILD

