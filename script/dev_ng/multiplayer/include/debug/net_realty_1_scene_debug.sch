USING "SceneTool_debug.sch"

PROC Private_Savenet_realty_1_scene(STRUCT_net_realty_1_scene& scene, INT iBuildingID)

	TEXT_LABEL_63 str
	str = Private_Get_Net_Realty_Building_Name(iBuildingID)
	str += "	//"
	str += Private_Get_Net_Realty_Building_Description(iBuildingID)
	
	SceneTool_OpenDebugFile("Private_Get_net_realty_1_SCENE()",				str)

		SceneTool_ExportPan(	"net_realty_1_scene_PAN_establishing",		scene.mPans[net_realty_1_scene_PAN_establishing])

		SceneTool_ExportCut(	"net_realty_1_scene_CUT_null",				scene.mCuts[net_realty_1_scene_CUT_null])

		SceneTool_ExportMarker(	"net_realty_1_scene_MARKER_playerWalk0",	scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0])
		SceneTool_ExportMarker(	"net_realty_1_scene_MARKER_playerWalk1",	scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1])
		SceneTool_ExportMarker(	"net_realty_1_scene_MARKER_playerWalk2",	scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2])

		SceneTool_ExportPlacer(	"NET_REALTY_1_SCENE_PLACER_playerPos",		scene.mPlacers[NET_REALTY_1_SCENE_PLACER_playerPos])

		SceneTool_ExportBool(	"bEnablePans[0]",				scene.bEnablePans[0])
		
		SceneTool_ExportFloat(	"fExitDelay",					scene.fExitDelay)

	SceneTool_CloseDebugFile("Private_Get_net_realty_1_scene()")
	
ENDPROC



TYPEDEF FUNC BOOL SceneRealtyFunc(BOOL bResetCoords, STRUCT_net_realty_1_scene scene)

PROC Private_DebugPlaynet_realty_1_scene(STRUCT_net_realty_1_scene& scene, SceneRealtyFunc customRealtyCutscene)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		WHILE NOT CALL customRealtyCutscene(TRUE, scene)
			WAIT(0)
		ENDWHILE
		
								//playerId	bHasControl	bVisible	bClearTasks	bHasCollision	bFreezePos
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF

ENDPROC

PROC Private_Edit_net_realty_1_scene(structSceneTool_Launcher& launcher, INT iBuildingID, SceneRealtyFunc customRealtyCutscene, OBJECT_INDEX &doorBlock, OBJECT_INDEX &garageBlock)

	// Get scene data
	STRUCT_net_realty_1_scene		scene
	//structSceneTool tool
	
	// bOutputMissingNetRealtyScenes
	INT iBuilding
	REPEAT MP_PROPERTY_BUILDING_MAX iBuilding
		IF iBuilding = 0
			
		ELSE
			
			PRINTSTRING("property ")
			PRINTSTRING(Private_Get_Net_Realty_Building_Name(iBuilding))
			PRINTSTRING(" ")
			
			IF Private_Get_NET_REALTY_1_SCENE(iBuilding, scene, TRUE)
				PRINTSTRING("has scene one, ")
			ELSE
				PRINTSTRING("missing scene one, ")
			ENDIF
			
			PRINTNL()
		ENDIF
	ENDREPEAT
	PRINTNL()
	//	//	//	//	//	//
	
	Private_Get_net_realty_1_scene(iBuildingID, scene)
	
	// Add widgets
	START_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool,Private_Get_Net_Realty_Building_Name(iBuildingID))

		START_WIDGET_GROUP("Camera shots")
			START_WIDGET_GROUP(			"Establishing shot")
				ADD_WIDGET_PAN(			"Establish - ",				g_sAmMpPropertyExtSceneTool,scene.mPans,					ENUM_TO_INT(net_realty_1_scene_PAN_establishing))

			STOP_WIDGET_GROUP()
			ADD_WIDGET_FLOAT_SLIDER(	"fExitDelay",				scene.fExitDelay, 0.0, 10.0, 0.1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Marker")
			ADD_WIDGET_MARKER(			"Player Walk 0",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,				ENUM_TO_INT(net_realty_1_scene_MARKER_playerWalk0))
			ADD_WIDGET_MARKER(			"Player Walk 1",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,				ENUM_TO_INT(net_realty_1_scene_MARKER_playerWalk1))
			ADD_WIDGET_MARKER(			"Player Walk 2",			g_sAmMpPropertyExtSceneTool,scene.mMarkers,				ENUM_TO_INT(net_realty_1_scene_MARKER_playerWalk2))
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Placer")
			ADD_WIDGET_PLACER(			"PLACER_playerPos",			g_sAmMpPropertyExtSceneTool,scene.mPlacers,				ENUM_TO_INT(net_realty_1_scene_PLACER_playerPos))
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("*sliders*")
			START_WIDGET_GROUP("Marker")
				START_WIDGET_GROUP("Player Walk 0")
					ADD_WIDGET_VECTOR_SLIDER(	"Player Walk 0", scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Player Walk 1")
					ADD_WIDGET_VECTOR_SLIDER(	"Player Walk 1", scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Player Walk 2")
					ADD_WIDGET_VECTOR_SLIDER(	"Player Walk 2", scene.mMarkers[net_realty_1_scene_MARKER_playerWalk2].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Placer")
				START_WIDGET_GROUP("PLACER_playerPos")
					ADD_WIDGET_VECTOR_SLIDER(	"PLACER_playerPos",	scene.mPlacers[net_realty_1_scene_PLACER_playerPos].vPos, -6000, 6000, 0.01)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
	STOP_SCENETOOL_GROUP(launcher, g_sAmMpPropertyExtSceneTool)
	
	// Warp to player pos marker
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[net_realty_1_scene_PLACER_playerPos].vPos)
	ENDIF
	
	// Update widgets
	WHILE launcher.bEnableTool
	
		// Update system
		SWITCH SceneTool_UpdateTool(g_sAmMpPropertyExtSceneTool)
			
			CASE SCENETOOL_ACTION_RunScene
				Private_DebugPlaynet_realty_1_scene(scene, customRealtyCutscene)
			BREAK
			
			CASE SCENETOOL_ACTION_SaveScene
				Private_Savenet_realty_1_scene(scene, iBuildingID)
			BREAK
			
			CASE SCENETOOL_ACTION_UpdateData
				SceneTool_UpdatePans(g_sAmMpPropertyExtSceneTool, scene.mPans)
				SceneTool_UpdateCuts(g_sAmMpPropertyExtSceneTool, scene.mCuts)
				SceneTool_UpdateMarkers(g_sAmMpPropertyExtSceneTool, scene.mMarkers)
				SceneTool_UpdatePlacers(g_sAmMpPropertyExtSceneTool, scene.mPlacers)
			BREAK
		
		ENDSWITCH
				
		// Draw markers
		SceneTool_DrawMarker2(g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(net_realty_1_scene_MARKER_playerWalk0),	0, 255, 175,		scene.mPlacers[net_realty_1_scene_PLACER_playerPos].vPos)
		SceneTool_DrawMarker2(g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(net_realty_1_scene_MARKER_playerWalk1),	200,200,200,		scene.mMarkers[net_realty_1_scene_MARKER_playerWalk0].vPos)
		SceneTool_DrawMarker2(g_sAmMpPropertyExtSceneTool, scene.mMarkers,	ENUM_TO_INT(net_realty_1_scene_MARKER_playerWalk2),	200,200,200,		scene.mMarkers[net_realty_1_scene_MARKER_playerWalk1].vPos)
		
		// Draw placers
		SceneTool_DrawPlacer (g_sAmMpPropertyExtSceneTool, scene.mPlacers,	ENUM_TO_INT(net_realty_1_scene_PLACER_playerPos),			255, 0, 75)
		
		KILL_BLOCKING_OBJECTS(doorBlock,garageBlock)
		WAIT(0)

	ENDWHILE
	
	// Cleanup system
	SWITCH SceneTool_DestroyTool(g_sAmMpPropertyExtSceneTool)
		CASE SCENETOOL_ACTION_SaveScene
			Private_Savenet_realty_1_scene(scene, iBuildingID)
		BREAK
	ENDSWITCH



ENDPROC

