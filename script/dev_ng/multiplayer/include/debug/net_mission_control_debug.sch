// This header shouldn't be in the release project anyway, but wrap it up just in case
#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "net_mission_control_info.sch"

USING "net_debug.sch"

USING "script_clock.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_Control_Debug.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all MP Mission Control debug functionality.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************

// Mission Control F9 Output
TWEAK_FLOAT	MissionControlTextScaleX	0.3130
TWEAK_FLOAT	MissionControlTextScaleY	0.2525

TWEAK_FLOAT	MissionControlRectX			0.0000
TWEAK_FLOAT	MissionControlRectWidth		0.3630
TWEAK_INT	MissionControlRectAlpha		175

TWEAK_FLOAT	MissionControlStartY		0.4800
TWEAK_FLOAT MissionControlAddY			0.0140

TWEAK_FLOAT	MissionControlSlotX			0.0330
TWEAK_FLOAT	MissionControlUniqueIDX		0.0430
TWEAK_FLOAT	MissionControlStatusX		0.0850
TWEAK_FLOAT	MissionControlMissionVarX	0.1250
TWEAK_FLOAT	MissionControlInstanceX		0.2350
TWEAK_FLOAT	MissionControlContentIdX	0.2550
TWEAK_FLOAT	MissionControlSourceX		0.2550
TWEAK_FLOAT	MissionControlTeamConfigX	0.3050

TWEAK_FLOAT	MissionControlNamesScaleX	0.2600
TWEAK_FLOAT	MissionControlNamesScaleY	0.2150

TWEAK_FLOAT MissionControlNamesAddY		0.0130
TWEAK_FLOAT MissionControlGapAddY		0.0030

TWEAK_FLOAT	MissionControlTeamTitleX	0.0430
TWEAK_FLOAT	MissionControlTeamX_Tab1	0.0850
TWEAK_FLOAT	MissionControlTeamX_Tab2	0.1500
TWEAK_FLOAT	MissionControlTeamX_Tab3	0.2150
TWEAK_FLOAT	MissionControlTeamX_Tab4	0.2800

// Mission Control Host globals
TWEAK_FLOAT	MCHostTitleX				0.0430
TWEAK_FLOAT	MCHostNameX					0.0650
TWEAK_FLOAT	MCHostTimesX				0.1500
TWEAK_FLOAT	MCHostResyncX				0.2500


// -----------------------------------------------------------------------------------------------------------

STRUCT m_structDebugPlayerTeam
	PLAYER_INDEX				dptPlayer	// The player Index
	INT							dptTeam		// The player's team
	g_eMPMissionPlayerStatus	dptState	// The Player's current joining state
ENDSTRUCT




// ===========================================================================================================
//      F9 Screen Overlay - couldn't add this to F9 screen header because of cyclic header
// ===========================================================================================================

// PURPOSE:	Set up the Mission Control Client Widgets
PROC Create_MP_Mission_Control_Client_Widgets()

	START_WIDGET_GROUP("Mission Control - F9 Screen")
		ADD_WIDGET_FLOAT_SLIDER("MissionControlTextScaleX",		MissionControlTextScaleX,	0.0, 1.0, 0.005)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlTextScaleY",		MissionControlTextScaleY,	0.0, 1.0, 0.005)
		
		ADD_WIDGET_FLOAT_SLIDER("MissionControlStartY",			MissionControlStartY,		0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlAddY",			MissionControlAddY,			0.0, 1.0, 0.001)

		ADD_WIDGET_FLOAT_SLIDER("MissionControlSlotX",			MissionControlSlotX,		0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlUniqueIDX",		MissionControlUniqueIDX,	0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlStatusX",		MissionControlStatusX,		0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlMissionVarX",	MissionControlMissionVarX,	0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlInstanceX",		MissionControlInstanceX,	0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlContentIdX",		MissionControlContentIdX,	0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlSourceX",		MissionControlSourceX,		0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlTeamConfigX",	MissionControlTeamConfigX,	0.0, 1.0, 0.01)

		ADD_WIDGET_FLOAT_SLIDER("MissionControlNamesScaleX",	MissionControlNamesScaleX,	0.0, 1.0, 0.005)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlNamesScaleY",	MissionControlNamesScaleY,	0.0, 1.0, 0.005)
		
		ADD_WIDGET_FLOAT_SLIDER("MissionControlNamesAddY",		MissionControlNamesAddY,	0.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlExtraGapAddY",	MissionControlGapAddY,		0.0, 1.0, 0.001)

		ADD_WIDGET_FLOAT_SLIDER("MissionControlTeamTitleX",		MissionControlTeamTitleX,	0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlTeamX_Tab1",		MissionControlTeamX_Tab1,	0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlTeamX_Tab2",		MissionControlTeamX_Tab2,	0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlTeamX_Tab3",		MissionControlTeamX_Tab3,	0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MissionControlTeamX_Tab4",		MissionControlTeamX_Tab4,	0.0, 1.0, 0.01)
		
		ADD_WIDGET_FLOAT_SLIDER("MCHostTitleX",					MCHostTitleX,				0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MCHostNameX",					MCHostNameX,				0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MCHostTimesX",					MCHostTimesX,				0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("MCHostResyncX",				MCHostResyncX,				0.0, 1.0, 0.01)
	STOP_WIDGET_GROUP()
	
ENDPROC





// ===========================================================================================================
//      MP Console Log Output Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Host Output to Console Log
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Print the host name as a string to the console log
PROC Print_MC_Host_Name_To_Console_Log()

	TEXT_LABEL_63 returnStringName = "UNKNOWN (all players)"

	// Check if the host is stored
	IF (IS_NET_PLAYER_OK(GlobalServerBD_BlockB.missionControlData.mcHost, FALSE))
		returnStringName = GET_PLAYER_NAME(GlobalServerBD_BlockB.missionControlData.mcHost)
	ENDIF
	
	// Host is not stored
	NET_PRINT(returnStringName)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Data Conversions To String Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Mission Request Source ID to a string
//
// INPUT PARAMS:		paramSource				The Mission Source ID
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Mission_Source_To_String(g_eMPMissionSource paramSource)

	SWITCH (paramSource)
		CASE MP_MISSION_SOURCE_GROUPED
			RETURN ("Group")
			
		CASE MP_MISSION_SOURCE_JOBLIST
			RETURN ("Joblist")
			
		CASE MP_MISSION_SOURCE_INVITE
			RETURN ("Invite")
			
		CASE MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN
			RETURN ("Map (Invite)")
			
		CASE MP_MISSION_SOURCE_MISSION_FLOW_AUTO
			RETURN ("Flow (Auto)")
			
		CASE MP_MISSION_SOURCE_MISSION_FLOW_BLIP
			RETURN ("Flow (Blip)")
			
		CASE MP_MISSION_SOURCE_DEBUG_GROUPED
			RETURN ("Debug Group")
			
		CASE MP_MISSION_SOURCE_DEBUG_MENU
			RETURN ("Debug Menu")
			
		CASE MP_MISSION_SOURCE_AMBIENT
			RETURN ("Ambient")
			
		CASE MP_MISSION_SOURCE_HEIST
			RETURN ("Heist")
			
		CASE UNKNOWN_MISSION_SOURCE
			RETURN ("UNKNOWN")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: NEW MISSION REQUEST SOURCE ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Mission Type ID to a string
//
// INPUT PARAMS:		paramType				The Mission Type ID
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Mission_Type_To_String(MP_MISSION_TYPE paramType)

	SWITCH (paramType)
		CASE eTYPE_SINGLE_MISSION
			RETURN ("Single Mission")
			
		CASE eTYPE_GROUP_MISSION
			RETURN ("Group Mission")
			
		CASE eTYPE_LARGE_COP_MISSION
			RETURN ("Large Cop Mission")
			
		CASE eTYPE_HEIST
			RETURN ("Heist")
			
		CASE eTYPE_PRE_HEIST
			RETURN ("Pre-Heist")
			
		CASE eTYPE_PRE_GROUP
			RETURN ("Pre-Group")
			
		CASE eTYPE_AMBIENCE
			RETURN ("Ambience")
			
		CASE eTYPE_RANDOM_EVENT
			RETURN ("Random Event")
			
		CASE eTYPE_COP_INCIDENT
			RETURN ("Cop Incident")
			
		CASE eTYPE_TWO_TEAMS
			RETURN ("Two Teams")
			
		CASE eTYPE_TUTORIAL
			RETURN ("Tutorial")
			
		CASE UNKNOWN_MISSION_TYPE
			RETURN ("UNKNOWN")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: NEW MISSION TYPE ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Mission Status ID to a string
//
// INPUT PARAMS:		paramStatus				The Mission Status ID
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Mission_Status_To_String(g_eMPMissionStatus paramStatus)

	SWITCH (paramStatus)
		CASE MP_MISSION_STATE_RESERVED
			RETURN ("Reserved")
			
		CASE MP_MISSION_STATE_RECEIVED
			RETURN ("Received")
			
		CASE MP_MISSION_STATE_OFFERED
			RETURN ("Offered")
			
		CASE MP_MISSION_STATE_ACTIVE
			RETURN ("Active")
			
		CASE MP_MISSION_STATE_ACTIVE_SECONDARY
			RETURN ("Active, Add Secondary Players")
			
		CASE NO_MISSION_REQUEST_RECEIVED
			RETURN ("Empty")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: NEW MISSION STATUS ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Mission Team Configuration ID to a string
//
// INPUT PARAMS:		paramTeamConfig			The Team Configuration ID
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Team_Configuration_To_String(g_eMPTeamConfig paramTeamConfig)

	SWITCH (paramTeamConfig)
		CASE MP_TEAMS_COPS
			RETURN ("Cops Only")
			
		CASE MP_TEAMS_COPS_1GANG
			RETURN ("Cops + One Crook Gang")
			
		CASE MP_TEAMS_COPS_1GANG_MIN
			RETURN ("Cops + At Least One Crook Gang")
			
		CASE MP_TEAMS_COPS_1OR2GANGS
			RETURN ("Cops + One or Two Crook Gangs")
			
		CASE MP_TEAMS_1TEAM
			RETURN ("One Team")
			
		CASE MP_TEAMS_2TEAMS_MIN
			RETURN ("At Least Two Teams")
			
		CASE MP_TEAMS_1GANG
			RETURN ("One Crook Gang")
			
		CASE MP_TEAMS_2GANGS
			RETURN ("Two Crook Gangs")
			
		CASE MP_TEAMS_1GANG_MIN
			RETURN ("At Least One Crook Gang")
			
		CASE NO_MP_TEAM_CONFIG
			RETURN ("UNKNOWN")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: NEW TEAM CONFIGURATION ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Mission Launch Teams ID to a string
//
// INPUT PARAMS:		paramLaunchTeams		The Launch Teams ID
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Launch_Teams_To_String(g_eMPLaunchTeams paramLaunchTeams)

	SWITCH (paramLaunchTeams)
		CASE MP_LAUNCH_ALL_PLAYERS
			RETURN ("All Players")
	
		CASE MP_LAUNCH_ALL_TEAMS
			RETURN ("All Teams")
			
		CASE MP_LAUNCH_ALL_COPS
			RETURN ("All Cops")
			
		CASE MP_LAUNCH_ALL_GANGS
			RETURN ("All Crook Gangs")
			
		CASE MP_LAUNCH_ONE_TEAM
			RETURN ("One Team")
			
		CASE MP_LAUNCH_ONE_GANG
			RETURN ("One Crook Gang")
			
		CASE NO_MP_LAUNCH_TEAMS
			RETURN ("UNKNOWN")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: NEW LAUNCH TEAMS ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Location Selection Method ID to a string
//
// INPUT PARAMS:		paramLocationMethod		The Location Selection Method ID
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Location_Selection_Method_To_String(g_eMPLocMethod paramLocationMethod)

	SWITCH (paramLocationMethod)
		CASE MP_LOC_RND_VAR
			RETURN ("Random From Variations")
			
		CASE MP_LOC_FAIR_VAR
			RETURN ("Fair From Variations")
			
		CASE MP_LOC_FAIR_XYZ
			RETURN ("Fair Vector")
			
		CASE MP_LOC_SPECIAL
			RETURN ("Special")
			
		CASE NO_MP_LOC
			RETURN ("UNKNOWN")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: NEW LOCATION SELECTION METHOD ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Instance Restriction ID to a string
//
// INPUT PARAMS:		paramInstanceControlID	The Instance Restriction ID
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Instance_Restrictions_To_String(g_eMPInstances paramInstanceControlID)

	SWITCH (paramInstanceControlID)
		CASE MP_INST_ONE
			RETURN ("One Instance")
			
		CASE MP_INST_ANY
			RETURN ("Multiple Instances")
			
		CASE MP_INST_UNIQUE
			RETURN ("Unique Instances")
			
		CASE NO_MP_INST
			RETURN ("UNKNOWN")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: NEW INSTANCE RESTRICTION ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Player Mission Control State ID to a string
//
// INPUT PARAMS:		g_eMPMissionPlayerStatus	The Player Status ID
// RETURN VALUE:		STRING						The ID as a string
FUNC STRING Convert_Player_Status_To_String(g_eMPMissionPlayerStatus paramPlayerStatusID)

	SWITCH (paramPlayerStatusID)
		CASE MP_PLAYER_STATE_ACTIVE
			RETURN ("Active")
			
		CASE MP_PLAYER_STATE_JOINING
			RETURN ("Joining (or Confirmed)")
			
		CASE MP_PLAYER_STATE_RESERVED
			RETURN ("Reserved")
			
		CASE MP_PLAYER_STATE_NONE
			RETURN ("UNKNOWN")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: NEW PLAYER STATUS ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Mission Control Broadcast Type ID to a string
//
// INPUT PARAMS:		paramMCBroadcastType	The Mission Control Broadcast Type ID
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_MC_Broadcast_Type_To_String(g_eMCBroadcastType paramMCBroadcastType)

	SWITCH (paramMCBroadcastType)
		CASE MCCBT_REQUEST_FAILED
			RETURN ("REQUEST FAILED")
			
		CASE MCCBT_MISSION_FINISHED
			RETURN ("MISSION FINISHED")
			
		CASE MCCBT_MISSION_FULL
			RETURN ("MISSION FULL")
			
		CASE MCCBT_CHANGE_UNIQUE_ID
			RETURN ("CHANGE UNIQUE ID")
			
		CASE MCCBT_PLAYER_ON_MISSION
			RETURN ("PLAYER ON MISSION")
			
		CASE MCCBT_CONFIRMED_RESERVED
			RETURN ("CONFIRMED RESERVED")
			
		CASE MCCBT_NONE
			RETURN ("UNKNOWN")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: NEW MISSION CONTROL BROADCAST TYPE ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Mission Control Resync Activity ID to a string
//
// INPUT PARAMS:		paramResyncActivity		The Triggerer Resync Activity ID
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Resync_Activity_To_String(g_eMCCResyncActivity paramResyncActivity)

	SWITCH (paramResyncActivity)
		CASE MCCRA_PREMISSION
			RETURN ("ON PRE-MISSION ACTIVITY")
			
		CASE MCCRA_AWAITING_SERVER_OK
			RETURN ("AWAITING SERVER OK TO JOIN")
			
		CASE MCCRA_LAUNCHING_MISSION
			RETURN ("LAUNCHING MISSION")
			
		CASE MCCRA_MISSION_ACTIVE
			RETURN ("MISSION ACTIVE")
			
		CASE MCCRA_NO_ACTIVITY
			RETURN ("NO ACTIVITY")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: UNKNOWN MISSION CONTROL RESYNC ACTIVITY ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Mission Reservation Description ID to a string
//
// INPUT PARAMS:		paramReservationDesc	The description of the new reservation type
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Reservation_Description_To_String(g_eMPMissionReservationDescs paramReservationDesc)

	SWITCH (paramReservationDesc)
		CASE MP_RESERVED_BY_PLAYER
			RETURN ("Reserved_By_Player")
			
		CASE MP_RESERVED_BY_LEADER
			RETURN ("Reserved_By_Leader")
			
		CASE MP_RESERVED_BY_GAME
			RETURN ("Reserved_By_Game")
			
		CASE NO_MP_RESERVATION_DESCRIPTION
			RETURN ("No Reservation Description")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: UNKNOWN MISSION RESERVATION DESCRIPTION ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Mission Control Resync State ID to a string
//
// INPUT PARAMS:		paramResyncHostState	The Controller Resync State ID
// RETURN VALUE:		STRING					The ID as a string
FUNC STRING Convert_Resync_Host_State_To_String(g_eMCCResyncHostState paramResyncHostState)

	SWITCH (paramResyncHostState)
		CASE MCCRHS_JOINING
			RETURN ("PLAYER JOINING")
			
		CASE MCCRHS_CONFIRMED
			RETURN ("PLAYER CONFIRMED")
			
		CASE MCCRHS_ACTIVE
			RETURN ("PLAYER ACTIVE")
			
		CASE MCCRHS_NO_ACTIVE_STATE
			RETURN ("NOT ON MISSION (OR RESERVED)")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: UNKNOWN MISSION CONTROL RESYNC HOST STATE ID - ADD TO FUNCTION")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Converts the MP Mission Control Resync CommsTypeID to a string
//
// INPUT PARAMS:		g_eMCCResyncCommsTypeID		The Resync CommsTypeID ID
// RETURN VALUE:		STRING						The ID as a string
FUNC STRING Convert_Resync_CommsTypeID_To_String(g_eMCCResyncCommsTypeID paramCommsTypeID)

	SWITCH (paramCommsTypeID)
		CASE MCCRCH_REQUEST_SPECIFIC_MISSION
			RETURN ("REQUEST_SPECIFIC_MISSION")
			
		CASE MCCRCH_REQUEST_MISSION_OF_TYPE
			RETURN ("REQUEST_MISSION_OF_TYPE")
			
		CASE MCCRCH_REQUEST_JOIN_MISSION
			RETURN ("REQUEST_JOIN_MISSION")
			
		CASE MCCRCH_RESERVE_MISSION_BY_PLAYER
			RETURN ("RESERVE_MISSION_BY_PLAYER")
			
		CASE MCCRCH_RESERVE_MISSION_BY_LEADER
			RETURN ("RESERVE_MISSION_BY_LEADER")
			
		CASE MCCRCH_CANCEL_RESERVATION_BY_PLAYER
			RETURN ("CANCEL_RESERVATION_BY_PLAYER")
			
		CASE MCCRCH_CANCEL_RESERVED_BY_LEADER
			RETURN ("CANCEL_RESERVED_BY_LEADER")
			
		CASE MCCRCH_START_RESERVED_MISSION
			RETURN ("START_RESERVED_MISSION")
			
		CASE MCCRCH_CHANGE_RESERVED_MISSION
			RETURN ("CHANGE_RESERVED_MISSION")
			
		CASE MCCRCH_MISSION_NOT_JOINABLE
			RETURN ("MISSION_NOT_JOINABLE")
			
		CASE MCCRCH_MISSION_JOINABLE_FOR_PLAYERS
			RETURN ("JOINABLE_FOR_PLAYERS")
			
		CASE MCCRCH_READY_FOR_SECONDARY_TEAMS
			RETURN ("READY_FOR_SECONDARY_TEAMS")
			
		CASE MCCRCH_NO_COMMS
			RETURN ("NO COMMS")
	ENDSWITCH
	
	// Needs added to the list
	RETURN ("ERROR: UNKNOWN MISSION CONTROL RESYNC COMMS TYPE ID - ADD TO FUNCTION")

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Data Output Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Outputs the full contents of one MP_MISSION_DATA struct to the console log
//
// INPUT PARAMS:		paramMissionData		The Mission Data contents to be output to the console log
PROC Debug_Output_Full_Contents_Of_MP_Mission_Data_To_Console_Log(MP_MISSION_DATA paramMissionData)

	NET_PRINT("           MISSION          : ") NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))	NET_NL()
	NET_PRINT("           VARIATION        : ") NET_PRINT_INT(paramMissionData.mdID.idVariation)				NET_NL()
	NET_PRINT("           CREATOR          : ") NET_PRINT_INT(paramMissionData.mdID.idCreator)					NET_NL()
	NET_PRINT("           CLOUD FILENAME   : ") NET_PRINT(paramMissionData.mdID.idCloudFilename)				NET_NL()
	NET_PRINT("           SHARED REG ID    : ") NET_PRINT_INT(paramMissionData.mdID.idSharedRegID)				NET_NL()
	NET_PRINT("           INSTANCE ID      : ") NET_PRINT_INT(paramMissionData.iInstanceId)						NET_NL()
	NET_PRINT("           PRIMARY COORDS   : ") NET_PRINT_VECTOR(paramMissionData.mdPrimaryCoords)				NET_NL()
	NET_PRINT("           SECONDARY COORDS : ") NET_PRINT_VECTOR(paramMissionData.mdSecondaryCoords)			NET_NL()
	NET_PRINT("           UNIQUE REQUEST ID: ") NET_PRINT_INT(paramMissionData.mdUniqueID)						NET_NL()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Outputs tjhe main contents of one MP_MISSION_DATA struct to the console log
//
// INPUT PARAMS:		paramMissionData		The Mission Data contents to be output to the console log
PROC Debug_Output_Basic_MP_Mission_Details_As_A_Line_Segment(MP_MISSION_DATA paramMissionData)

	NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))
	NET_PRINT(" (Inst=") NET_PRINT_INT(paramMissionData.iInstanceId) NET_PRINT(")")
	NET_PRINT(" (Var=") NET_PRINT_INT(paramMissionData.mdID.idVariation) NET_PRINT(")")
	NET_PRINT(" (UniqueID=") NET_PRINT_INT(paramMissionData.mdUniqueID) NET_PRINT(")")

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Reserved Players Consistency Checking Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Ensures that all the Reserved Players bitfields are consistent
//
// INPUT PARAMS:		paramArrayPos			The array position of the slot
PROC Debug_Consistency_Check_Reserved_Players_Bitfields(INT paramArrayPos)

	// CHECKING:
	//		- the subset reserved player bitfields are a complete match for the 'all reserved players' bitfield
	//		- there are no duplicate player bits in the subsets
	
	INT testBits		= ALL_MISSION_CONTROL_BITS_CLEAR
	INT allSubsetBits	= ALL_MISSION_CONTROL_BITS_CLEAR
	INT duplicateBits	= ALL_MISSION_CONTROL_BITS_CLEAR
	INT tempLoop		= 0
	PLAYER_INDEX thisPlayer
	
	// Store the Player Choice bits
	allSubsetBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByPlayer
	
	// Add the Leader Choice bits checking for duplicates
	testBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByLeader
	IF NOT (testBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		REPEAT NUM_NETWORK_PLAYERS tempLoop
			IF (IS_BIT_SET(testBits, tempLoop))
				IF (IS_BIT_SET(allSubsetBits, tempLoop))
					// ...this is a duplicate - player must be in two subset bitfields
					SET_BIT(duplicateBits, tempLoop)
				ENDIF
				
				SET_BIT(allSubsetBits, tempLoop)
			ENDIF
		ENDREPEAT
	ENDIF
	
	// Add the Game Choice bits checking for duplicates
	testBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByGame
	IF NOT (testBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		REPEAT NUM_NETWORK_PLAYERS tempLoop
			IF (IS_BIT_SET(testBits, tempLoop))
				IF (IS_BIT_SET(allSubsetBits, tempLoop))
					// ...this is a duplicate - player must be in two subset bitfields
					SET_BIT(duplicateBits, tempLoop)
				ENDIF
				
				SET_BIT(allSubsetBits, tempLoop)
			ENDIF
		ENDREPEAT
	ENDIF
	
	// All Subset Bits should match the All Reserved bitfield
	BOOL inconsistencyFound = FALSE
	
	INT allReservedBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersAll
	IF NOT (allReservedBits = allSubsetBits)
		// ...there is an inconsistency, so highlight it
		NET_PRINT("        Reserved Players Bitfields - INCONSISTENCIES between 'all reserved' and 'total of subset reserved'") NET_NL()
		
		REPEAT NUM_NETWORK_PLAYERS tempLoop
			IF (IS_BIT_SET(allReservedBits, tempLoop))
				// ...player in the 'all reserved' bitfield
				IF NOT (IS_BIT_SET(allSubsetBits, tempLoop))
					// ...player not in any subset bitfield
					NET_PRINT("           Player Classed As Reserved But Not In An Individual Reserved Bitfield: ")
					thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
					IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
						NET_PRINT(GET_PLAYER_NAME(thisPlayer))
					ELSE
						NET_PRINT("[PLAYER NOT OK]")
					ENDIF
					NET_NL()
					
					inconsistencyFound = TRUE
				ENDIF
			ELSE
				// ...player is not in the 'all reserved' bitfield
				IF (IS_BIT_SET(allSubsetBits, tempLoop))
					// ...player is in one of the subset bitfields
					NET_PRINT("           Player NOT Classed As Reserved But Is In An Individual Reserved Bitfield: ")
					thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
					IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
						NET_PRINT(GET_PLAYER_NAME(thisPlayer))
					ELSE
						NET_PRINT("[PLAYER NOT OK]")
					ENDIF
					NET_NL()
					
					inconsistencyFound = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	// Check for duplicate bits within the subsets
	IF NOT (duplicateBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		// ...there is an inconsistency, so highlight it
		NET_PRINT("        Reserved Players Bitfields - INCONSISTENCIES - players found in multiple subset bitfields") NET_NL()
		inconsistencyFound = TRUE
		Debug_Output_Player_Names_From_Bitfield(duplicateBits)
	ENDIF
	
	// Output a script assert to highlight any inconsistency
	IF (inconsistencyFound)
		SCRIPT_ASSERT("Debug_Consistency_Check_Reserved_Players_Bitfields() - Inconsistency found with the reserved players bitfields. Check Console Logs. Tell Keith.")
	ENDIF

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Slot Data Output Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output lists of players based on the mission bits
//
// INPUT PARAMS:		paramArrayPos			The array position of the slot
PROC Debug_Output_Missions_Player_Details(INT paramArrayPos)

	INT theseBits = ALL_MISSION_CONTROL_BITS_CLEAR

	// Requested Teams
	theseBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRequestedTeams
	IF NOT (theseBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("        Teams that Requested To Join This Mission") NET_NL()
		Debug_Output_Team_Names_From_Bitfield(theseBits)
	ENDIF

	// Reserved Teams for Mission
	theseBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedTeams
	IF NOT (theseBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("        Teams Reserved For This Mission") NET_NL()
		Debug_Output_Team_Names_From_Bitfield(theseBits)
	ENDIF

	// Joinable Teams
	theseBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoinableTeams
	IF NOT (theseBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("        Teams Currently Allowed To Join This Mission") NET_NL()
		Debug_Output_Team_Names_From_Bitfield(theseBits)
	ENDIF

	// Active Players
	theseBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsActivePlayers
	IF NOT (theseBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("        Players On Mission") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(theseBits)
	ENDIF

	// Confirmed Players
	theseBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsConfirmedPlayers
	IF NOT (theseBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("        Players Confirmed For Mission") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(theseBits)
	ENDIF

	// Joining Players
	theseBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoiningPlayers
	IF NOT (theseBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("        Players Joining Mission") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(theseBits)
	ENDIF

	// Player Choice Reserved Players (subset of All Reserved Players)
	theseBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByPlayer
	IF NOT (theseBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("        Reserved_By_Player Players For Mission (subset of All Reserved players)") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(theseBits)
	ENDIF

	// Leader Choice Reserved Players (subset of All Reserved Players)
	theseBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByLeader
	IF NOT (theseBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("        Reserved_By_Leader Players For Mission (subset of All Reserved players)") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(theseBits)
	ENDIF

	// Game Choice Reserved Players (subset of All Reserved Players)
	theseBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedPlayersByGame
	IF NOT (theseBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("        Reserved_By_Game Players For Mission (subset of All Reserved players)") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(theseBits)
	ENDIF
	
	// This is a good place to do some consistency checking of the reserved players bitfields
	Debug_Consistency_Check_Reserved_Players_Bitfields(paramArrayPos)

	// Excluded Players
	theseBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsExcludedPlayers
	IF NOT (theseBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("        Players Excluded From Mission") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(theseBits)
	ENDIF

	// Rejected Players
	theseBits = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRejectedPlayers
	IF NOT (theseBits = ALL_MISSION_CONTROL_BITS_CLEAR)
		NET_PRINT("        Players That Rejected The Mission") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(theseBits)
	ENDIF

	// Players on the mission in Other Sessions
	NET_PRINT("        Players On This Mission In Other Sessions: ")
	NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrNumPlayersInOtherSessions)
	NET_NL()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the main details of one mission request slot to the console log
//
// INPUT PARAMS:		paramArrayPos			The array position of the slot
PROC Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(INT paramArrayPos)

	NET_PRINT("           Slot ") NET_PRINT_INT(paramArrayPos)
	NET_PRINT("  [") NET_PRINT(Convert_Mission_Status_To_String(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID)) NET_PRINT("]")
	NET_PRINT("  [Source: ") NET_PRINT(Convert_Mission_Source_To_String(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrSourceID)) NET_PRINT("]")
	NET_PRINT("  [Type: ") NET_PRINT(Convert_Mission_Type_To_String(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionTypeID)) NET_PRINT("]")
	NET_PRINT("  [TutSessionID: ") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrTutorialSessionID) NET_PRINT("]")
	
	NET_PRINT("  [Requester = ")
	IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = NO_MISSION_REQUEST_RECEIVED)
		NET_PRINT("UNKNOWN")
	ELSE
		IF (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrRequesterIsHost)
			NET_PRINT("HOST")
		ELSE
			IF (IS_NET_PLAYER_OK(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrRequester, FALSE))
				NET_PRINT(GET_PLAYER_NAME(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrRequester))
			ELSE
				NET_PRINT("DEAD")
			ENDIF
		ENDIF
	ENDIF
	NET_PRINT("]")
	
	IF NOT (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idMission = eNULL_MISSION)
		NET_PRINT("  [")
		Debug_Output_Basic_MP_Mission_Details_As_A_Line_Segment(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData)
		NET_PRINT("]")
	ENDIF
	NET_NL()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the main details of all mission request slots to the console log
PROC Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()

	INT tempLoop = 0
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(tempLoop)
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Output the main details of all mission request slots and their players to the console log
PROC Debug_Output_Details_Of_All_MP_Mission_Request_Slots_And_Players_To_Console_Log()

	INT tempLoop = 0
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(tempLoop)
		Debug_Output_Missions_Player_Details(tempLoop)
	ENDREPEAT

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Mission Control Broadcast Array Contents (on client)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display one line of stored Mission Control broadcasts data
//
// INPUT PARAMS:		paramBroadcastArrayPos		The Data Position within the MC Broadcast array
PROC Debug_Output_One_Mission_Control_Broadcast(INT paramBroadcastArrayPos)

	NET_PRINT("         ")
	NET_PRINT_INT(paramBroadcastArrayPos)
	NET_PRINT("  ")
	g_eMCBroadcastType theType = g_sMCBroadcasts[paramBroadcastArrayPos].MCB_Type
	NET_PRINT(Convert_MC_Broadcast_Type_To_String(theType))
	IF (g_sMCBroadcasts[paramBroadcastArrayPos].frameRetain)
		NET_PRINT("  [RETAIN]")
	ELSE
		NET_PRINT("          ")
	ENDIF
	NET_PRINT("  UID: ")
	NET_PRINT_INT(g_sMCBroadcasts[paramBroadcastArrayPos].uniqueID)
	SWITCH (theType)
		CASE MCCBT_CHANGE_UNIQUE_ID
			NET_PRINT("  replacement UID: ")
			NET_PRINT_INT(g_sMCBroadcasts[paramBroadcastArrayPos].generalInt1)
			BREAK
		CASE MCCBT_CONFIRMED_RESERVED
			NET_PRINT("  Num Reserved Players: ")
			NET_PRINT_INT(g_sMCBroadcasts[paramBroadcastArrayPos].generalInt1)
			BREAK
	ENDSWITCH
	SWITCH (theType)
		CASE MCCBT_CONFIRMED_RESERVED
			NET_PRINT("  Bits Reserved Players: ")
			NET_PRINT_INT(g_sMCBroadcasts[paramBroadcastArrayPos].generalInt2)
			BREAK
	ENDSWITCH
	NET_NL()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display any entries in the mission control broadcast array
// NOTE: 	This array should contain contiguous data
PROC Debug_Output_Mission_Control_Broadcast_Array()

	IF (g_numMCBroadcasts = 0)
		EXIT
	ENDIF
	
	NET_PRINT("      Contents of the Mission Control Broadcast Array ") NET_PRINT(" (entries: ") NET_PRINT_INT(g_numMCBroadcasts) NET_PRINT(")") NET_NL()

	INT tempLoop = 0
	REPEAT g_numMCBroadcasts tempLoop
		Debug_Output_One_Mission_Control_Broadcast(tempLoop)
	ENDREPEAT

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Mission Control Resync Storage Array Contents (on client)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Outputs one line of stored resync array details
//
// INPUT PARAMS:		paramArrayPos			The array position details to be output
PROC Debug_Output_One_Set_Of_Mission_Control_Resync_Array_Details(INT paramArrayPos)

	NET_PRINT("             ")
	NET_PRINT_INT(paramArrayPos)
	NET_PRINT(") ")
	IF (paramArrayPos < 10)
		NET_PRINT(" ")
	ENDIF
	IF (g_sMCResyncComms[paramArrayPos].mcrbInUseByActiveResync)
		NET_PRINT("[RESYNCING] ")
	ELSE
		NET_PRINT("            ")
	ENDIF
	NET_PRINT("timeout = ")
	NET_PRINT(GET_TIME_AS_STRING(g_sMCResyncComms[paramArrayPos].mcrbTimeout))
	NET_PRINT("   uniqueID = ")
	NET_PRINT_INT(g_sMCResyncComms[paramArrayPos].mcrbUniqueID)
	NET_PRINT("   mission = ")
	NET_PRINT(GET_MP_MISSION_NAME(g_sMCResyncComms[paramArrayPos].mcrbMissionID))
	NET_PRINT(" (var = ")
	NET_PRINT_INT(g_sMCResyncComms[paramArrayPos].mcrbVariation)
	NET_PRINT(")")
	NET_PRINT("   Event = ")
	NET_PRINT(Convert_Resync_CommsTypeID_To_String(g_sMCResyncComms[paramArrayPos].mcrbCommsTypeID))
	NET_NL()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Outputs all the stored resync array details
PROC Debug_Output_Mission_Control_Resync_Array()

	NET_PRINT("          RESYNC ARRAY CONTENTS (current Network Timer: ")
	NET_PRINT(GET_TIME_AS_STRING(GET_NETWORK_TIME()))
	NET_PRINT("):")
	NET_NL()
	
	IF (g_numMCResyncBroadcasts = 0)
		NET_PRINT("          <EMPTY>") NET_NL()
	ENDIF
	
	INT tempLoop = 0
	REPEAT g_numMCResyncBroadcasts tempLoop
		Debug_Output_One_Set_Of_Mission_Control_Resync_Array_Details(tempLoop)
	ENDREPEAT

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Mission Control Hosts on F9 Screen Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the F9 Screen Mission Controller Host data array
PROC Clear_F9_Screen_MC_Hosts_Data()

	INT tempLoop = 0
	REPEAT MAX_MC_F9_SCREEN_HOSTS tempLoop
		g_sF9MCHosts[tempLoop].hostName			= ""
		g_sF9MCHosts[tempLoop].startTimeAsText	= ""
		g_sF9MCHosts[tempLoop].timeAsHostAsText	= ""
		g_sF9MCHosts[tempLoop].startTimeInitialised = FALSE
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The host must have just migrated, so store the new host data for the F9 screen display
//
// INPUT PARAMS:		paramPlayerID			The Player Index of the new host player
PROC Store_New_MC_Host_For_F9_Screen(PLAYER_INDEX paramPlayerID)
	
	INT timeAsHost 

	// If there was a previous host, then calculate and store the length of time that player was a host prior to shuffling
	IF (g_sF9MCHosts[KNOWN_HOST_ARRAY_POS].startTimeInitialised)
		timeAsHost = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_sF9MCHosts[KNOWN_HOST_ARRAY_POS].startTime)
		g_sF9MCHosts[KNOWN_HOST_ARRAY_POS].timeAsHostAsText = Convert_Msec_To_HMS_String(timeAsHost)
	ENDIF

	// Need to shuffle the existing host data down the array
	INT theTo = MAX_MC_F9_SCREEN_HOSTS - 1
	INT theFrom = theTo - 1
	
	WHILE (theFrom >= 0)
		g_sF9MCHosts[theTo] = g_sF9MCHosts[theFrom]
		theTo--
		theFrom--
	ENDWHILE
	
	// Store the new host data
	// ...host name
	IF (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		g_sF9MCHosts[KNOWN_HOST_ARRAY_POS].hostName	= GET_PLAYER_NAME(paramPlayerID)
	ELSE
		g_sF9MCHosts[KNOWN_HOST_ARRAY_POS].hostName	= "Host Name Not Available"
	ENDIF
	
	// ...start time
	g_sF9MCHosts[KNOWN_HOST_ARRAY_POS].startTimeInitialised = TRUE
	g_sF9MCHosts[KNOWN_HOST_ARRAY_POS].startTime			= GET_NETWORK_TIME()
	
	// ...start time as string
	g_sF9MCHosts[KNOWN_HOST_ARRAY_POS].startTimeAsText	= GET_TIME_AS_STRING(GET_NETWORK_TIME())

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Mission Control F9 Screen Overlay - couldn't add this to F9 screen header because of cyclic header
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if cloud-loaded data is being updated
//
// RETURN VALUE:		BOOL			TRUE if the cloud-loaded data is being refreshed
FUNC BOOL Is_Cloud_Loaded_Data_Available()

	// Ensure FMMC_Launcher is running
	IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FMMC_Launcher")) = 0)
			RETURN FALSE
		ENDIF
		
		// Ensures the first cloud load has been done - had to use the global directly to avoid cyclic headers
		IF NOT (g_sAtCoordsControlsMP.matccInitialDataReady)
			RETURN FALSE
		ENDIF
	ENDIF

	// Is a cloud refresh in progress?
	IF (g_sAtCoordsControlsMP.matccRefreshInProgress)
		RETURN FALSE
	ENDIF
	
	// Data available
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Default text output
PROC Debug_Mission_Control_Text_Defaults()

	SET_TEXT_SCALE(MissionControlTextScaleX, MissionControlTextScaleY)
	SET_TEXT_CENTRE(FALSE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Default name text output
PROC Debug_Mission_Control_Name_Text_Defaults()

	SET_TEXT_SCALE(MissionControlNamesScaleX, MissionControlNamesScaleY)
	SET_TEXT_CENTRE(FALSE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Text Colour based on the HUD COlour
//
// INPUT PARAMS:		paramHudColour		The Hud Colour
PROC Debug_Set_Text_Colour_From_Hud_Colour(HUD_COLOURS paramHudColour)

	INT r, g, b, a
	GET_HUD_COLOUR(paramHudColour, r, g, b, a)
	SET_TEXT_COLOUR(r, g, b, a)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Team Colour
//
// INPUT PARAMS:		paramTeamID		The TeamID
PROC Debug_Set_Team_Colour(INT paramTeamID)

	INT r, g, b
	INT a = 255
	GET_TEAM_RGB_COLOUR(paramTeamID, r, g, b)
	SET_TEXT_COLOUR(r, g, b, a)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Team Text Colour based on the HUD Colour, or a bland colour if reserved but not yet on mission
//
// INPUT PARAMS:		paramTeamID				The TeamID
//						paramUseTeamColour		TRUE if the team colour should be used, otherwise FALSE
PROC Debug_Set_TeamName_Text_Colour(INT paramTeamID, BOOL paramUseTeamColour)
	
	IF NOT (paramUseTeamColour)
		Debug_Set_Text_Colour_From_Hud_Colour(HUD_COLOUR_GREYDARK)
		EXIT
	ENDIF

	// Team is Joinable on mission so use the team colours
	Debug_Set_Team_Colour(paramTeamID)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mission Line Text Colour
//
// INPUT PARAMS:		paramStatus			Mission Request Status on this line
// RETURN PARAMS:		paramR				Red
//						paramG				Green
//						paramB				Blue
//						paramA				Alpha
PROC Debug_Mission_Control_Get_Default_Line_Colour(g_eMPMissionStatus paramStatus, INT &paramR, INT &paramG, INT &paramB, INT &paramA)

	SWITCH (paramStatus)
		CASE MP_MISSION_STATE_ACTIVE
		CASE MP_MISSION_STATE_ACTIVE_SECONDARY
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, paramR, paramG, paramB, paramA)
			BREAK
			
		CASE MP_MISSION_STATE_OFFERED
			GET_HUD_COLOUR(HUD_COLOUR_ORANGE, paramR, paramG, paramB, paramA)
			BREAK
			
		DEFAULT
			GET_HUD_COLOUR(HUD_COLOUR_GREY, paramR, paramG, paramB, paramA)
			BREAK
	ENDSWITCH

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mission Control Titles
//
// INPUT PARAMS:		paramY				DisplayLine Y
PROC Debug_Mission_Control_Titles(FLOAT paramY)

	INT r, g, b, a
	GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, r, g, b, a)

	// Slot/UniqueID
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(r, g, b, a)
	DISPLAY_TEXT(MissionControlUniqueIDX, paramY, "F9MC_UNIQUEID")

	// Status
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(r, g, b, a)
	DISPLAY_TEXT(MissionControlStatusX, paramY, "F9MC_STATUS")

	// Mission/Variation
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(r, g, b, a)
	DISPLAY_TEXT(MissionControlMissionVarX, paramY, "F9MC_MISSVAR")

	// Instance Details
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(r, g, b, a)
	DISPLAY_TEXT(MissionControlInstanceX, paramY, "F9MC_INSTANCE")

	// ContentId
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(r, g, b, a)
	DISPLAY_TEXT_WITH_LITERAL_STRING(MissionControlContentIdX, paramY, "STRING", "ContentID")

//	// Source
//	Debug_Mission_Control_Text_Defaults()
//	SET_TEXT_COLOUR(r, g, b, a)
//	DISPLAY_TEXT(MissionControlSourceX, paramY, "F9MC_SOURCE")
//
//	// Team Configuration
//	Debug_Mission_Control_Text_Defaults()
//	SET_TEXT_COLOUR(r, g, b, a)
//	DISPLAY_TEXT(MissionControlTeamConfigX, paramY, "F9MC_CONFIG")
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mission Request Status
//
// INPUT PARAMS:		paramStatus			Mission Request Status on this line
//						paramY				DisplayLine Y
// 						paramR				Red
//						paramG				Green
//						paramB				Blue
//						paramA				Alpha
PROC Debug_Mission_Control_Display_Status(g_eMPMissionStatus paramStatus, FLOAT paramY, INT paramR, INT paramG, INT paramB, INT paramA)

	TEXT_LABEL_15 requestText = "F9MC_UNKNOWN"
	
	SWITCH paramStatus
		CASE MP_MISSION_STATE_ACTIVE
		CASE MP_MISSION_STATE_ACTIVE_SECONDARY
			requestText = "F9ST_ACTIVE"
			BREAK
			
		CASE MP_MISSION_STATE_OFFERED
			requestText = "F9ST_OFFER"
			BREAK
			
		CASE MP_MISSION_STATE_RECEIVED
			requestText = "F9ST_RECEIVED"
			BREAK
			
		CASE MP_MISSION_STATE_RESERVED
			requestText = "F9ST_RESERVED"
			BREAK
	ENDSWITCH

	// Display Status
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(paramR, paramG, paramB, paramA)
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(requestText)		
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(MissionControlStatusX, paramY)
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mission Name and Chosen Variation
//
// INPUT PARAMS:		paramArrayPos		Position within Mission Request Array
//						paramMissionID		Mission ID
//						paramY				DisplayLine Y
// 						paramR				Red
//						paramG				Green
//						paramB				Blue
//						paramA				Alpha
PROC Debug_Mission_Control_Display_MissionVar(INT paramArrayPos, MP_MISSION paramMissionID, FLOAT paramY, INT paramR, INT paramG, INT paramB, INT paramA)

	// Display Status
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(paramR, paramG, paramB, paramA)
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("F9_MISSIONVAR")		
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(GET_MP_MISSION_NAME(paramMissionID))
		ADD_TEXT_COMPONENT_INTEGER(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idVariation)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(MissionControlMissionVarX, paramY)
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Instance and Instance Description
//
// INPUT PARAMS:		paramArrayPos		Position within Mission Request Array
//						paramY				DisplayLine Y
// 						paramR				Red
//						paramG				Green
//						paramB				Blue
//						paramA				Alpha
PROC Debug_Mission_Control_Display_InstanceDetails(INT paramArrayPos, FLOAT paramY, INT paramR, INT paramG, INT paramB, INT paramA)

	// Display Status
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(paramR, paramG, paramB, paramA)
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("NUMBER")		
		ADD_TEXT_COMPONENT_INTEGER(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.iInstanceId)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(MissionControlInstanceX, paramY)
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	ContentID
//
// INPUT PARAMS:		paramArrayPos		Position within Mission Request Array
//						paramY				DisplayLine Y
// 						paramR				Red
//						paramG				Green
//						paramB				Blue
//						paramA				Alpha
PROC Debug_Mission_Control_Display_ContentID(INT paramArrayPos, FLOAT paramY, INT paramR, INT paramG, INT paramB, INT paramA)

	// Display Status
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(paramR, paramG, paramB, paramA)
	
	TEXT_LABEL_31 contentID = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idCloudFilename
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(contentID)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(MissionControlContentIdX, paramY)
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mission Request Source
//
// INPUT PARAMS:		paramArrayPos		Position within Mission Request Array
//						paramY				DisplayLine Y
// 						paramR				Red
//						paramG				Green
//						paramB				Blue
//						paramA				Alpha
PROC Debug_Mission_Control_Display_Source(INT paramArrayPos, FLOAT paramY, INT paramR, INT paramG, INT paramB, INT paramA)

	g_eMPMissionSource theSource = GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrSourceID

	TEXT_LABEL_15 sourceText = "F9MC_UNKNOWN"
	
	SWITCH theSource
		CASE MP_MISSION_SOURCE_GROUPED
			sourceText = "F9SR_GROUP"
			BREAK
			
		CASE MP_MISSION_SOURCE_JOBLIST
			sourceText = "F9SR_JOBLIST"
			BREAK
			
		CASE MP_MISSION_SOURCE_INVITE
			sourceText = "F9SR_INVITE"
			BREAK
			
		CASE MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN
			sourceText = "F9SR_NOTWALKIN"
			BREAK
			
		CASE MP_MISSION_SOURCE_MISSION_FLOW_AUTO
			sourceText = "F9SR_FLOWAUTO"
			BREAK
			
		CASE MP_MISSION_SOURCE_MISSION_FLOW_BLIP
			sourceText = "F9SR_FLOWBLIP"
			BREAK
			
		CASE MP_MISSION_SOURCE_DEBUG_GROUPED
			sourceText = "F9SR_DBGGROUP"
			BREAK
			
		CASE MP_MISSION_SOURCE_DEBUG_MENU
			sourceText = "F9SR_DBGMENU"
			BREAK
			
		CASE MP_MISSION_SOURCE_AMBIENT
			sourceText = "F9SR_AMBIENT"
			BREAK
			
		CASE MP_MISSION_SOURCE_HEIST
			sourceText = "F9SR_HEIST"
			BREAK
	ENDSWITCH

	// Display Source
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(paramR, paramG, paramB, paramA)
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(sourceText)		
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(MissionControlSourceX, paramY)
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Team Configuration Description
//
// INPUT PARAMS:		paramArrayPos		Position within Mission Request Array
//						paramMissionID		Mission ID
//						paramY				DisplayLine Y
// 						paramR				Red
//						paramG				Green
//						paramB				Blue
//						paramA				Alpha
PROC Debug_Mission_Control_Display_Team_Config(INT paramArrayPos, MP_MISSION paramMissionID, FLOAT paramY, INT paramR, INT paramG, INT paramB, INT paramA)

	INT				theVariation	= GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID.idVariation
	g_eMPTeamConfig	teamConfig		= Get_MP_Mission_Variation_Team_Configuration(paramMissionID, theVariation)

	TEXT_LABEL_15 configText = "F9MC_UNKNOWN"
	
	SWITCH teamConfig			
		CASE MP_TEAMS_COPS_1GANG
			configText = "F9CF_CP1G"
			BREAK
			
		CASE MP_TEAMS_COPS_1GANG_MIN
			configText = "F9CF_CP1GMIN"
			BREAK
			
		CASE MP_TEAMS_COPS_1OR2GANGS
			configText = "F9CF_CP1OR2G"
			BREAK
			
		CASE MP_TEAMS_COPS
			configText = "F9CF_CP"
			BREAK
			
		CASE MP_TEAMS_1GANG
			configText = "F9CF_1G"
			BREAK
			
		CASE MP_TEAMS_2GANGS
			configText = "F9CF_2G"
			BREAK
			
		CASE MP_TEAMS_1GANG_MIN
			configText = "F9CF_1GMIN"
			BREAK
			
		CASE MP_TEAMS_1TEAM
			configText = "F9CF_1T"
			BREAK
			
		CASE MP_TEAMS_2TEAMS_MIN
			configText = "F9CF_2TMIN"
			BREAK
	ENDSWITCH

	// Display Status
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(paramR, paramG, paramB, paramA)
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(configText)		
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(MissionControlTeamConfigX, paramY)
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mission Request Unique ID
//
// INPUT PARAMS:		paramArrayPos		Position within Mission Request Array
//						paramY				DisplayLine Y
// 						paramR				Red
//						paramG				Green
//						paramB				Blue
//						paramA				Alpha
PROC Debug_Mission_Control_Display_UniqueID(INT paramArrayPos, FLOAT paramY, INT paramR, INT paramG, INT paramB, INT paramA)

	// Display UniqueID
	Debug_Mission_Control_Text_Defaults()
	SET_TEXT_COLOUR(paramR, paramG, paramB, paramA)
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("NUMBER")		
		ADD_TEXT_COMPONENT_INTEGER(GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdUniqueID)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(MissionControlUniqueIDX, paramY)
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Team Name (appropriately coloured)
//
// INPUT PARAMS:		paramTeamID					TeamID
//						paramJoinable				TRUE if this team is currently joinable, otherwise FALSE
//						paramMaxMembers				Max Players from team allowed on mission
//						paramY						DisplayLine Y
PROC Debug_Mission_Control_Team_Name(INT paramTeamID, BOOL paramJoinable, INT paramMaxMembers, FLOAT paramY)
	
	// Display Team Info
	Debug_Mission_Control_Name_Text_Defaults()
	Debug_Set_TeamName_Text_Colour(paramTeamID, paramJoinable)
	
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("F9_TEAMMAX")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_TEAM_NAME_KEY(paramTeamID, TRUE))
		ADD_TEXT_COMPONENT_INTEGER(paramMaxMembers)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(MissionControlTeamTitleX, paramY)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Player Name (coloured appropriately)
//
// INPUT PARAMS:		paramPlayerID				The player Index
//						paramTeamID					Player Team ID
//						paramPlayerState			Player's current mission joining state
//						paramTab					The TAB position to use
//						paramY						DisplayLine Y
PROC Display_Player_Name_Coloured_For_Mission_State(PLAYER_INDEX paramPlayerID, INT paramTeamID, g_eMPMissionPlayerStatus paramPlayerState, INT paramTab, FLOAT paramY)

	// Get the Player State from the array
	FLOAT tabPos = MissionControlTeamX_Tab1
	
	SWITCH paramTab
		CASE 0
			tabPos = MissionControlTeamX_Tab1
			BREAK
		CASE 1
			tabPos = MissionControlTeamX_Tab2
			BREAK
		CASE 2
			tabPos = MissionControlTeamX_Tab3
			BREAK
		CASE 3
			tabPos = MissionControlTeamX_Tab4
			BREAK
	ENDSWITCH
	
	// Display the Player name
	Debug_Mission_Control_Name_Text_Defaults()
	
	// Use the correct colour
	SWITCH paramPlayerState
		CASE MP_PLAYER_STATE_ACTIVE
			Debug_Set_Team_Colour(paramTeamID)
			BREAK
			
		CASE MP_PLAYER_STATE_JOINING
			Debug_Set_Text_Colour_From_Hud_Colour(HUD_COLOUR_PINK)
			BREAK
			
		CASE MP_PLAYER_STATE_RESERVED
			Debug_Set_Text_Colour_From_Hud_Colour(HUD_COLOUR_GREY)
			BREAK
			
		DEFAULT
			Debug_Set_Text_Colour_From_Hud_Colour(HUD_COLOUR_GREY)
			BREAK
	ENDSWITCH

	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(paramPlayerID))
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(tabPos, paramY)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Team Name and the Players associated with that team in a tabbed format
//
// INPUT PARAMS:			paramMissionIdData		MissionID Data
//							paramTeam				The Team ID (this can also be TEAM_ONVALID)
//							paramJoinable			TRUE if this team is currently joinable for the mission, otherwise FALSE
//							paramNumPlayers			The number of players on the PlayersOnMission array
//							paramY					The on-screen Y position to display the data
// RETURN PARAMS:			paramPlayersOnMission	The array of players with their associated teams on the mission
// RETURN VALUE:			FLOAT					The on-screen Y position to use for the next line of data
FUNC FLOAT Display_One_Teams_Associated_Players(MP_MISSION_ID_DATA &paramMissionIdData, INT paramTeam, BOOL paramJoinable, m_structDebugPlayerTeam &paramPlayersOnMission[], INT paramNumPlayers, FLOAT paramY)

	// Display the Team Details
	INT	maxTeamMembersAllowed = Get_Maximum_Team_Members_For_MP_Mission_Request(paramMissionIdData, paramTeam)
	Debug_Mission_Control_Team_Name(paramTeam, paramJoinable, maxTeamMembersAllowed, paramY)
	
	// Display the players associated with this Team
	CONST_INT	MAX_PLAYERS_PER_SCREEN_LINE		4
	
	FLOAT		thisY		= paramY
	INT			playerLoop	= 0
	INT			tabToUse	= 0
	
	REPEAT paramNumPlayers playerLoop
		IF (paramPlayersOnMission[playerLoop].dptTeam = paramTeam)
    	OR (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
			// Use next screen line?
			IF (tabToUse = MAX_PLAYERS_PER_SCREEN_LINE)
				// ...yes
				tabToUse = 0
				
				thisY += MissionControlNamesAddY
			ENDIF
		
			Display_Player_Name_Coloured_For_Mission_State(paramPlayersOnMission[playerLoop].dptPlayer, paramTeam, paramPlayersOnMission[playerLoop].dptState, tabToUse, thisY)
			tabToUse++
		ENDIF
	ENDREPEAT
	
	// Increase Line Y for Names
	thisY += MissionControlNamesAddY
	
	RETURN thisY

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the number of Cross Session Players on the mission
//
// INPUT PARAMS:		paramNumPlayers		The number of players on the same mission in other transition sessions
//						paramY				DisplayLine Y
// RETURN VALUE:		FLOAT				The Next Display Line Y position
FUNC FLOAT Display_Players_On_Mission_In_Other_Sessions(INT paramNumPlayers, FLOAT paramY)

	Debug_Mission_Control_Name_Text_Defaults()
	Debug_Set_Text_Colour_From_Hud_Colour(HUD_COLOUR_YELLOW)

	FLOAT thisY = paramY

	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("F9_OTHERTRANS")
		ADD_TEXT_COMPONENT_INTEGER(paramNumPlayers)
	END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(MissionControlTeamTitleX, thisY)
	
	// Increase Line Y for Names
	thisY += MissionControlNamesAddY
	
	RETURN thisY

ENDFUNC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mission Request Player and Team Information
//
// INPUT PARAMS:		paramArrayPos		Position within Mission Request Array
//						paramY				DisplayLine Y
// RETURN VALUE:		FLOAT				The Next Display Line Y position
FUNC FLOAT Debug_Mission_Control_Team_And_Player_Details(INT paramArrayPos, FLOAT paramY)
	
	INT		tempLoop	= 0
	FLOAT	thisY		= paramY
	
	m_structDebugPlayerTeam		sPlayerTeamArray[NUM_NETWORK_PLAYERS]
	
	g_eMPMissionPlayerStatus	playerStatus			= MP_PLAYER_STATE_NONE
	MP_MISSION_ID_DATA			theMissionIdData		= GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrMissionData.mdID
	INT							numPlayersOnMission		= 0
	INT							thisPlayerTeam			= TEAM_INVALID
	BOOL						somePlayersHaveNoTeam	= FALSE
	PLAYER_INDEX				thisPlayer
	
	// Fill the Player Team struct with data for all players on this mission
	numPlayersOnMission		= 0
	somePlayersHaveNoTeam	= FALSE
	
	IF theMissionIdData.idVariation = NO_MISSION_VARIATION
		SCRIPT_ASSERT("Debug_Mission_Control_Team_And_Player_Details - theMissionIdData.idVariation = NO_MISSION_VARIATION")
		RETURN thisY
	ENDIF
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		// Is this player active on a mission?
		playerStatus = GlobalServerBD_BlockB.missionPlayerData[tempLoop].mcpState
		IF (playerStatus != MP_PLAYER_STATE_NONE)
			// ...yes, is player on this mission?
			IF (GlobalServerBD_BlockB.missionPlayerData[tempLoop].mcpSlot = paramArrayPos)
				// yes, is this player ok?
				thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
				IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
					// ...yes, so store the data in the next array position
					thisPlayerTeam = GET_PLAYER_TEAM(thisPlayer)
					IF (thisPlayerTeam = TEAM_INVALID)
    				OR (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
						somePlayersHaveNoTeam = TRUE
					ENDIF
					
					sPlayerTeamArray[numPlayersOnMission].dptPlayer	= thisPlayer
					sPlayerTeamArray[numPlayersOnMission].dptTeam	= thisPlayerTeam
					sPlayerTeamArray[numPlayersOnMission].dptState	= playerStatus
					
					numPlayersOnMission++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Gather Team Info
	//INT 	bitsReservedTeams			= GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsReservedTeams
	//INT 	bitsRequestedTeams			= GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsRequestedTeams
	//INT 	bitsJoinableTeams			= GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrBitsJoinableTeams
	INT		numPlayersInOtherSessions	= GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrNumPlayersInOtherSessions
	BOOL	thisTeamIsJoinable			= FALSE
	
//	// Display the Player Info for Players in Teams
//	REPEAT MAX_NUM_TEAMS tempLoop
//		IF (IS_BIT_SET(bitsReservedTeams, tempLoop))
//		OR (IS_BIT_SET(bitsRequestedTeams, tempLoop))
//			thisTeamIsJoinable = IS_BIT_SET(bitsJoinableTeams, tempLoop)
//			thisY = Display_One_Teams_Associated_Players(theMissionIdData, tempLoop, thisTeamIsJoinable, sPlayerTeamArray, numPlayersOnMission, thisY)
//		ENDIF
//	ENDREPEAT
	
	// Display the Player Info for players with no team (for now, always treat this 'team' as joinable)
	IF (somePlayersHaveNoTeam)
		thisTeamIsJoinable = TRUE
		thisY = Display_One_Teams_Associated_Players(theMissionIdData, TEAM_INVALID, thisTeamIsJoinable, sPlayerTeamArray, numPlayersOnMission, thisY)
	ENDIF
	
	// Display the number of Players in Other Sessions Joining the mission
	thisY = Display_Players_On_Mission_In_Other_Sessions(numPlayersInOtherSessions, thisY)
	
	// Return the Next Y line for the next mission details
	RETURN thisY
			
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Host Information (tagged on to the bottom of the mission control information)
//
// RETURN PARAMS:			paramY			The screenY position (this gets passed back out to the calling function)
PROC Display_F9_Screen_Mission_Control_Host_Details(FLOAT &paramY)

	// Skip a screen line
	paramY += MissionControlAddY

	// Display the data specific to the known host screen line
	HUD_COLOURS	knownHostColour = HUD_COLOUR_GREENLIGHT
	
	// ...the 'host:' prefix
	Debug_Mission_Control_Text_Defaults()
	Debug_Set_Text_Colour_From_Hud_Colour(knownHostColour)
	DISPLAY_TEXT(MCHostTitleX, paramY, "F9H_PREFIX")
	
	// ...resync flags - if active, otherise the current network timer
	IF (g_sMCResync.mccrTriggeringTimeoutInitialised)
	OR (g_sMCResync.mccrExternalTimeoutInitialised)
		Debug_Mission_Control_Text_Defaults()
		Debug_Set_Text_Colour_From_Hud_Colour(HUD_COLOUR_YELLOW)
		
		IF (g_sMCResync.mccrTriggeringTimeoutInitialised)
			IF (g_sMCResync.mccrExternalTimeoutInitialised)
				// Triggerer and External Systems Resync in progress
				DISPLAY_TEXT(MCHostResyncX, paramY, "F9H_RESYNC_TE")
			ELSE
				// Triggerer Resync only in progress
				DISPLAY_TEXT(MCHostResyncX, paramY, "F9H_RESYNC_T")
			ENDIF
		ELSE
			// External Systems Resync only in progress
			DISPLAY_TEXT(MCHostResyncX, paramY, "F9H_RESYNC_E")
		ENDIF
	ELSE
		Debug_Mission_Control_Text_Defaults()
		Debug_Set_Text_Colour_From_Hud_Colour(HUD_COLOUR_YELLOW)
		
		DISPLAY_TEXT_WITH_LITERAL_STRING(MCHostResyncX, paramY, "F9H_NETTIME", GET_TIME_AS_STRING(GET_NETWORK_TIME()))
	ENDIF
	
	// Calculate and store the 'time as host' for the known host (array Position 0)
	INT timeAsHost = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_sF9MCHosts[KNOWN_HOST_ARRAY_POS].startTime)
	g_sF9MCHosts[KNOWN_HOST_ARRAY_POS].timeAsHostAsText = Convert_Msec_To_HMS_String(timeAsHost)
	
	// Display the common Host data for current and previous hosts
	INT			tempLoop		= 0
	HUD_COLOURS	thisLineColour	= knownHostColour
	
	REPEAT MAX_MC_F9_SCREEN_HOSTS tempLoop
		IF (g_sF9MCHosts[tempLoop].startTimeInitialised)
		OR (tempLoop = KNOWN_HOST_ARRAY_POS)
			IF (tempLoop = KNOWN_HOST_ARRAY_POS)
				thisLineColour = knownHostColour
			ELSE
				thisLineColour = HUD_COLOUR_GREENDARK
			ENDIF
		
			// ...display Host Name
			Debug_Mission_Control_Text_Defaults()
			Debug_Set_Text_Colour_From_Hud_Colour(thisLineColour)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")	
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_sF9MCHosts[tempLoop].hostName)
			END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(MCHostNameX, paramY)
			
			// ...display Start Time player became host, and the length of time the player was host
			Debug_Mission_Control_Text_Defaults()
			Debug_Set_Text_Colour_From_Hud_Colour(thisLineColour)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("F9H_TIMES")	
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_sF9MCHosts[tempLoop].startTimeAsText)
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_sF9MCHosts[tempLoop].timeAsHostAsText)
			END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(MCHostTimesX, paramY)
			
			// next line
			paramY += MissionControlAddY
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the F9 Screen Output for Mission Requests
PROC Debug_F9_Screen_Mission_Requests()

	INT tempLoop = 0
	BOOL activeMissionRequest = FALSE
	
	g_eMPMissionStatus missionStatus = NO_MISSION_REQUEST_RECEIVED
	MP_MISSION thisMission = eNULL_MISSION
	INT lineR = 0
	INT lineG = 0
	INT lineB = 0
	INT lineA = 0
	
	FLOAT thisLineY	= MissionControlStartY
	
	// Titles
	Debug_Mission_Control_Titles(thisLineY)
	thisLineY += MissionControlAddY
	
	// Is the cloud-loaded data available?
	IF NOT (Is_Cloud_Loaded_Data_Available())
		// ...no, so don't display anything
		Debug_Mission_Control_Text_Defaults()
		Debug_Set_Text_Colour_From_Hud_Colour(HUD_COLOUR_GREY)
		DISPLAY_TEXT(MissionControlUniqueIDX, thisLineY, "F9_NO_MCCLOUD")
		thisLineY += MissionControlAddY
	ELSE
		// ...cloud data should be available
		// Slot Number
		REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
			missionStatus = GlobalServerBD_MissionRequest.missionRequests[tempLoop].mrStatusID
			
			IF NOT (missionStatus = NO_MISSION_REQUEST_RECEIVED)
				thisMission = GlobalServerBD_MissionRequest.missionRequests[tempLoop].mrMissionData.mdID.idMission
				activeMissionRequest = TRUE
				
				// Get the default line colour
				Debug_Mission_Control_Get_Default_Line_Colour(missionStatus, lineR, lineG, lineB, lineA)
				
				// Output mission details
				// ...slot number
				Debug_Mission_Control_Text_Defaults()
				SET_TEXT_COLOUR(lineR, lineG, lineB, lineA)
				DISPLAY_TEXT_WITH_NUMBER(MissionControlSlotX, thisLineY, "NUMBER", tempLoop)
				
				// ...uniqueID
				Debug_Mission_Control_Display_UniqueID(tempLoop, thisLineY, lineR, lineG, lineB, lineA)
				
				// ...slot status
				Debug_Mission_Control_Display_Status(missionStatus, thisLineY, lineR, lineG, lineB, lineA)
				
				// ...mission and variation
				Debug_Mission_Control_Display_MissionVar(tempLoop, thisMission, thisLineY, lineR, lineG, lineB, lineA)
				
				// ...instance and instance description
				Debug_Mission_Control_Display_InstanceDetails(tempLoop, thisLineY, lineR, lineG, lineB, lineA)
				
//				// ...request source
//				Debug_Mission_Control_Display_Source(tempLoop, thisLineY, lineR, lineG, lineB, lineA)
//				
//				// ...team config
//				Debug_Mission_Control_Display_Team_Config(tempLoop, thisMission, thisLineY, lineR, lineG, lineB, lineA)

				// ...contentID
				Debug_Mission_Control_Display_ContentID(tempLoop, thisLineY, lineR, lineG, lineB, lineA)

				// Increase Line Y
				thisLineY += MissionControlAddY
				
				// Team and Player Information
				thisLineY = Debug_Mission_Control_Team_And_Player_Details(tempLoop, thisLineY)
				
				// Add an extra gap between each mission
				thisLineY += MissionControlGapAddY
			ENDIF
		ENDREPEAT
		
		// Is there an active mission request?
		IF NOT (activeMissionRequest)
			Debug_Mission_Control_Text_Defaults()
			Debug_Set_Text_Colour_From_Hud_Colour(HUD_COLOUR_GREY)
			DISPLAY_TEXT(MissionControlUniqueIDX, thisLineY, "F9_NO_MCREQS")
			thisLineY += MissionControlAddY
		ENDIF
	ENDIF
	
	// Output the Host information
	Display_F9_Screen_Mission_Control_Host_Details(thisLineY)
	
	// Draw a background rectangle of the proper size
	// NOTE: I can calculate and draw this last because rectangles are always drawn first
	FLOAT rectY = MissionControlStartY
	FLOAT rectHeight = thisLineY - MissionControlStartY + MissionControlAddY
	
	DRAW_RECT_FROM_CORNER(MissionControlRectX, rectY, MissionControlRectWidth, rectHeight, 0, 0, 0, MissionControlRectAlpha)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Mission Control F9 Screen Output
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the F9 Screen Output when required
PROC Debug_Mission_Control_F9_Screen()

	IF NOT (g_MPF9ScreenIsRendering)
		EXIT
	ENDIF
	
	// Allow it to render over the top of ingame menus
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)

	// Mission Requests details
	Debug_F9_Screen_Mission_Requests()
	
	// Switch off the rendering over menus again
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Tutorial Session Console Log Output
//		(Listens for changes in a player's tutorial session and outputs a message to the logs)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear tutorial session variables
PROC Clear_Tutorial_Session_Debug_Variables()

	g_debugTutorialSessionActive	= FALSE
	g_debugTutorialSessionInstance	= -1

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain curent tutorial session details for this player and output console log details when things change
PROC Debug_Current_Tutorial_Session_Details()

	BOOL	isInTutorialSession				= NETWORK_IS_IN_TUTORIAL_SESSION()
	INT		currentTutorialSessionInstance	= -1
	BOOL	detailsHaveChanged				= FALSE
	
	IF (isInTutorialSession)
		currentTutorialSessionInstance = NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(PLAYER_ID())
	ENDIF
	
	// Check for changes
	IF (isInTutorialSession != g_debugTutorialSessionActive)
		g_debugTutorialSessionActive	= isInTutorialSession
		detailsHaveChanged				= TRUE
	ENDIF
	
	IF (currentTutorialSessionInstance != g_debugTutorialSessionInstance)
		g_debugTutorialSessionInstance	= currentTutorialSessionInstance
		detailsHaveChanged				= TRUE		
	ENDIF
	
	IF NOT (detailsHaveChanged)
		EXIT
	ENDIF
	
	// Output a Tutorial Session message to the console log to record the changes
	NET_PRINT("...KGM MP: TUTORIAL SESSION DETAILS HAVE CHANGED. ")
	IF (g_debugTutorialSessionActive)
		NET_PRINT(" [TUTORIAL SESSION ACTIVE]")
	ELSE
		NET_PRINT(" [Tutorial Session Not Active]")
	ENDIF
	NET_PRINT(" [TUTORIAL INSTANCE: ")
	NET_PRINT_INT(g_debugTutorialSessionInstance)
	NET_PRINT("]")
	NET_NL()

ENDPROC




#ENDIF	// IS_DEBUG_BUILD

