USING "globals.sch"
USING "context_control_public.sch"
USING "website_public.sch"
USING "freemode_header.sch"
USING "net_gang_boss_private.sch"
USING "net_simple_interior.sch"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    VARIABLES & DEFENITIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CONST_FLOAT DEFAULT_MOVER_BLEND_IN_DELTA 1000.0

TWEAK_FLOAT BS_ANIM_TRIGGER_DIST 0.2
TWEAK_FLOAT BS_BOSS_SEAT_TASK_LOCAL_PED_TO_GO_TO_SEAT_COORD_TOLLERANCE 0.5
TWEAK_FLOAT BS_ANIM_TRIGGER_HEADING_LEEWAY 20.0
TWEAK_FLOAT BS_PLAY_COMPUTER_ENTER_ANIM_SPEED 1.0
TWEAK_FLOAT BS_BOSS_SEAT_STATE_ENTER_ANIM_SPEED 1.25
TWEAK_FLOAT BS_BOSS_SEAT_STATE_ENTER_ANIM_SPEED_LEFT 1.25
TWEAK_FLOAT BS_BOSS_SEAT_STATE_PLAYING_BASE_ANIM_SPEED 1.0
TWEAK_FLOAT BS_BOSS_SEAT_STATE_SEAT_EXIT_ANIM_SPEED 1.4
TWEAK_FLOAT BS_BOSS_SEAT_STATE_COMPUTER_EXIT_ANIM_SPEED 1.0
TWEAK_FLOAT BS_BOSS_SEAT_STATE_ENTER_CAPSULE_SIZE 0.25

TWEAK_FLOAT BS_CHAIR_SLIDE_TOLLERANCE 0.02
TWEAK_FLOAT BS_CHAIR_SLIDE_SPEED 0.001
TWEAK_FLOAT BS_CHAIR_SLIDE_ROT_TOLLERANCE 0.05
TWEAK_FLOAT BS_CHAIR_SLIDE_ROT_SPEED 0.001

ENUM SMPL_BOSS_SEAT_BS
	SMPL_BSS_BIT_DATA_SETUP = 0,
	SMPL_BSS_BIT_SUPPORTS_LEFT_SIDE_ENTRY,
	SMPL_BSS_BIT_HELP_TEXT_DISPLAYING,
	SMPL_BSS_BIT_ENTERED_LEFT_SIDE,
	SMPL_BSS_BIT_HAS_ACCESS_TO_BOSS_SEAT,
	SMPL_BSS_BIT_REQUESTED_ANIM_DICT,
	SMPL_BSS_BIT_ENABLE_HEADING_CHECK,
	SMPL_BSS_BIT_LAUNCHING_COMPUTER_SCRIPT,
	SMPL_BSS_BIT_COMPUTER_SCRIPT_LAUNCHED,
	SMPL_BSS_BIT_DISABLED_INTERACTION_MENU,
	SMPL_BSS_BIT_PREVENT_IDLE_ANIM,
	SMPL_BSS_BIT_RESERVED_NET_OBJ,
	SMPL_BSS_BIT_ALLOW_IN_PRIVATE_SESSIONS
ENDENUM

ENUM SMPL_BOSS_SEAT_STATE
	SMPL_BSS_INITIAL,
	SMPL_BSS_CREATE_CHAIR,
	SMPL_BSS_LOCATE_CHECK,
	SMPL_BSS_REQUEST_PROMPT,
	SMPL_BSS_GO_TO_START,
	SMPL_BSS_SEAT_ENTER,
	SMPL_BSS_SEAT_TRANSITION_TO_BASE,
	SMPL_BSS_PLAY_BASE_ANIM,
	SMPL_BSS_USE_COMPUTER,
	SMPL_BSS_COMPUTER_EXIT,
	SMPL_BSS_SEAT_EXIT,
	SMPL_BSS_SEAT_CLEANUP
ENDENUM

ENUM SMPL_BOSS_SEAT_BLOCKED_REASON
	SMPL_BSS_BLOCK_PRIVATE_SESSION,
	SMPL_BSS_BLOCK_REASON_ON_MISSION,
	SMPL_BSS_BLOCK_REASON_NOT_BOSS,
	SMPL_BSS_BLOCK_REASON_MAX_GANGS_REACHED,
	SMPL_BSS_BLOCK_REASON_BEHAVIOUR
ENDENUM

STRUCT SMPL_BOSS_SEAT_SERVER_DATA
	NETWORK_INDEX niSeat
ENDSTRUCT

STRUCT SMPL_BOSS_SEAT_LOCATE
	VECTOR vecA
	VECTOR vecB
	FLOAT fWidth
	FLOAT fHeading
	FLOAT fHeadingLeeway = 90.0
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT SMPL_BOSS_SEAT_DEBUG
	BOOL bEnableSeatPositionEditing
	BOOL bResetChairLoc
	BOOL bOutputSeatLocation
	VECTOR vSeatPosition
	FLOAT fSeatHeading
ENDSTRUCT
#ENDIF

STRUCT SMPL_BOSS_SEAT_DATA
	//General data
	INT iBS
	INT iBlockedBS
	INT iComputerScriptHash
	INT iContextIntention = NEW_CONTEXT_INTENTION
	SMPL_BOSS_SEAT_STATE currentState
	
	//Positioning
	VECTOR vSeatSpawnPosition
	VECTOR vSeatSpawnRotation
	
	//Locate
	SMPL_BOSS_SEAT_LOCATE sRightSideLocate
	SMPL_BOSS_SEAT_LOCATE sLeftSideLocate
	
	//Help text
	STRING sSitDownPrompt
	STRING sRegisterAsBossPrompt
	STRING sUseComputerPrompt
	STRING sComputerNotAvailablePrompt
	
	//Object
	MODEL_NAMES seatModel
	OBJECT_INDEX objSeat
	
	//Animation
	STRING sAnimDict
	
	//Sync Scene
	INT iNetSceneID
	INT iLocalSceneID = -1
	
	//App script to Run
	STRING sScript
	
	//Debug
	#IF IS_DEBUG_BUILD
	SMPL_BOSS_SEAT_DEBUG debug
	#ENDIF
ENDSTRUCT

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    CORE FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_SMPL_BOSS_SEAT_STATE_STRING(SMPL_BOSS_SEAT_STATE eState)
	SWITCH eState
		CASE SMPL_BSS_INITIAL					RETURN "SMPL_BSS_INITIAL"					BREAK
		CASE SMPL_BSS_CREATE_CHAIR				RETURN "SMPL_BSS_CREATE_CHAIR"				BREAK
		CASE SMPL_BSS_LOCATE_CHECK				RETURN "SMPL_BSS_LOCATE_CHECK"				BREAK
		CASE SMPL_BSS_REQUEST_PROMPT			RETURN "SMPL_BSS_REQUEST_PROMPT"			BREAK
		CASE SMPL_BSS_GO_TO_START				RETURN "SMPL_BSS_GO_TO_START"				BREAK
		CASE SMPL_BSS_SEAT_ENTER				RETURN "SMPL_BSS_SEAT_ENTER"				BREAK
		CASE SMPL_BSS_SEAT_TRANSITION_TO_BASE	RETURN "SMPL_BSS_SEAT_TRANSITION_TO_BASE"	BREAK
		CASE SMPL_BSS_PLAY_BASE_ANIM			RETURN "SMPL_BSS_PLAY_BASE_ANIM"			BREAK
		CASE SMPL_BSS_USE_COMPUTER				RETURN "SMPL_BSS_USE_COMPUTER"				BREAK
		CASE SMPL_BSS_COMPUTER_EXIT				RETURN "SMPL_BSS_COMPUTER_EXIT"				BREAK
		CASE SMPL_BSS_SEAT_EXIT					RETURN "SMPL_BSS_SEAT_EXIT"					BREAK
		CASE SMPL_BSS_SEAT_CLEANUP				RETURN "SMPL_BSS_SEAT_CLEANUP"				BREAK
	ENDSWITCH
	
	RETURN "UNKNOWN"
ENDFUNC	
#ENDIF

PROC SLIDE_CHAIR_TO_SPAWN_POSITION(SMPL_BOSS_SEAT_DATA &activityData)

	VECTOR vToTarget
	VECTOR vCurrent
	FLOAT fDistToTarget
	FLOAT fDistToSlide
	
	IF DOES_ENTITY_EXIST(activityData.objSeat)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(activityData.objSeat)	
	
		// do distance slide
		vCurrent = GET_ENTITY_COORDS(activityData.objSeat)
		vToTarget = activityData.vSeatSpawnPosition - vCurrent
		fDistToTarget = VMAG(vToTarget)
		IF (fDistToTarget > BS_CHAIR_SLIDE_TOLLERANCE)			
			vToTarget /= fDistToTarget	
			fDistToSlide = BS_CHAIR_SLIDE_SPEED + TIMESTEP()
			// don't move past the spawn point as this could lead to oscillating.
			IF (fDistToSlide < fDistToTarget)
				vToTarget *= fDistToSlide
			ELSE
				vToTarget *= fDistToTarget 
			ENDIF
			PRINTLN("[SMPL_BOSS_SEAT] SLIDE_CHAIR_TO_SPAWN_POSITION - sliding by ", vToTarget)
			SET_ENTITY_COORDS(activityData.objSeat, vCurrent + vToTarget)
		ENDIF
		
		// do rotation slide
		vCurrent = GET_ENTITY_ROTATION(activityData.objSeat)
		vToTarget = activityData.vSeatSpawnRotation - vCurrent
		fDistToTarget = VMAG(vToTarget)
		IF (fDistToTarget > BS_CHAIR_SLIDE_ROT_TOLLERANCE)	
			vToTarget /= fDistToTarget	
			fDistToSlide = BS_CHAIR_SLIDE_ROT_SPEED + TIMESTEP()
			// don't move past the spawn point as this could lead to oscillating.
			IF (fDistToSlide < fDistToTarget)
				vToTarget *= fDistToSlide
			ELSE
				vToTarget *= fDistToTarget 
			ENDIF
			PRINTLN("[SMPL_BOSS_SEAT] SLIDE_CHAIR_TO_SPAWN_POSITION - sliding rotation by ", vToTarget)
			SET_ENTITY_ROTATION(activityData.objSeat, vCurrent + vToTarget)
		ENDIF
	
	ENDIF

ENDPROC

PROC SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(SMPL_BOSS_SEAT_DATA &activityData,SMPL_BOSS_SEAT_STATE eNewState)
	#IF IS_DEBUG_BUILD
	IF activityData.currentState != eNewState
		PRINTLN("[SMPL_BOSS_SEAT] SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE - new state: ", DEBUG_GET_SMPL_BOSS_SEAT_STATE_STRING(eNewState))
	ENDIF
	#ENDIF
	
	activityData.currentState = eNewState
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_SMPL_BOSS_SEAT_BITSET_STRING(SMPL_BOSS_SEAT_BS bit)
	SWITCH bit
		CASE SMPL_BSS_BIT_DATA_SETUP				RETURN "SMPL_BSS_BIT_DATA_SETUP"				BREAK
		CASE SMPL_BSS_BIT_SUPPORTS_LEFT_SIDE_ENTRY	RETURN "SMPL_BSS_BIT_SUPPORTS_LEFT_SIDE_ENTRY"	BREAK
		CASE SMPL_BSS_BIT_HELP_TEXT_DISPLAYING		RETURN "SMPL_BSS_BIT_HELP_TEXT_DISPLAYING"		BREAK
		CASE SMPL_BSS_BIT_ENTERED_LEFT_SIDE			RETURN "SMPL_BSS_BIT_ENTERED_LEFT_SIDE"			BREAK
		CASE SMPL_BSS_BIT_HAS_ACCESS_TO_BOSS_SEAT	RETURN "SMPL_BSS_BIT_HAS_ACCESS_TO_BOSS_SEAT"	BREAK
		CASE SMPL_BSS_BIT_REQUESTED_ANIM_DICT		RETURN "SMPL_BSS_BIT_REQUESTED_ANIM_DICT"		BREAK
		CASE SMPL_BSS_BIT_ENABLE_HEADING_CHECK		RETURN "SMPL_BSS_BIT_ENABLE_HEADING_CHECK"		BREAK
		CASE SMPL_BSS_BIT_LAUNCHING_COMPUTER_SCRIPT	RETURN "SMPL_BSS_BIT_LAUNCHING_COMPUTER_SCRIPT"	BREAK
		CASE SMPL_BSS_BIT_COMPUTER_SCRIPT_LAUNCHED	RETURN "SMPL_BSS_BIT_COMPUTER_SCRIPT_LAUNCHED"	BREAK
		CASE SMPL_BSS_BIT_DISABLED_INTERACTION_MENU	RETURN "SMPL_BSS_BIT_DISABLED_INTERACTION_MENU"	BREAK
		CASE SMPL_BSS_BIT_PREVENT_IDLE_ANIM			RETURN "SMPL_BSS_BIT_PREVENT_IDLE_ANIM"			BREAK
		CASE SMPL_BSS_BIT_RESERVED_NET_OBJ			RETURN "SMPL_BSS_BIT_RESERVED_NET_OBJ"			BREAK
		CASE SMPL_BSS_BIT_ALLOW_IN_PRIVATE_SESSIONS	RETURN "SMPL_BSS_BIT_ALLOW_IN_PRIVATE_SESSIONS"	BREAK
	ENDSWITCH
	
	RETURN "UNKNOWN"
ENDFUNC	
#ENDIF

PROC SET_SMPL_INTERIOR_BOSS_SEAT_BIT(SMPL_BOSS_SEAT_DATA &activityData, SMPL_BOSS_SEAT_BS bit)
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(activityData.iBS, ENUM_TO_INT(bit))
		PRINTLN("[SMPL_BOSS_SEAT] SET_SMPL_INTERIOR_BOSS_SEAT_BIT - bit: ", DEBUG_GET_SMPL_BOSS_SEAT_BITSET_STRING(bit))
	ENDIF
	#ENDIF
	
	SET_BIT(activityData.iBS, ENUM_TO_INT(bit))
ENDPROC

PROC CLEAR_SMPL_INTERIOR_BOSS_SEAT_BIT(SMPL_BOSS_SEAT_DATA &activityData, SMPL_BOSS_SEAT_BS bit)
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(activityData.iBS, ENUM_TO_INT(bit))
		PRINTLN("[SMPL_BOSS_SEAT] CLEAR_SMPL_INTERIOR_BOSS_SEAT_BIT - bit: ", DEBUG_GET_SMPL_BOSS_SEAT_BITSET_STRING(bit))
	ENDIF
	#ENDIF
	
	CLEAR_BIT(activityData.iBS, ENUM_TO_INT(bit))
ENDPROC

FUNC BOOL SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(SMPL_BOSS_SEAT_DATA &activityData, SMPL_BOSS_SEAT_BS bit)
	RETURN IS_BIT_SET(activityData.iBS, ENUM_TO_INT(bit))
ENDFUNC

PROC _BOSS_SEAT_SAFE_CLEAR_CONTEXT(SMPL_BOSS_SEAT_DATA &activityData)
	IF activityData.iContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(activityData.iContextIntention)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks for an active anim dict and releases it
/// PARAMS:
///    activityData - Primary data struct
PROC _BOSS_SEAT_RELEASE_ANIM_DICT(SMPL_BOSS_SEAT_DATA &activityData)
	IF SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_REQUESTED_ANIM_DICT)
	AND NOT IS_STRING_NULL_OR_EMPTY(activityData.sAnimDict)
		CLEAR_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_REQUESTED_ANIM_DICT)
		REMOVE_ANIM_DICT(activityData.sAnimDict)
	ENDIF
ENDPROC

PROC _BOSS_SEAT_DISABLE_INTERACTION_MENU(SMPL_BOSS_SEAT_DATA &activityData)
	DISABLE_INTERACTION_MENU()
	SET_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_DISABLED_INTERACTION_MENU)
ENDPROC

PROC _BOSS_SEAT_ENABLE_INTERACTION_MENU(SMPL_BOSS_SEAT_DATA &activityData)
	IF SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_DISABLED_INTERACTION_MENU)
		ENABLE_INTERACTION_MENU()
		CLEAR_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_DISABLED_INTERACTION_MENU)
	ENDIF
ENDPROC

PROC _BOSS_SEAT_ACTIVITY_CLEANUP(SMPL_BOSS_SEAT_DATA &activityData)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SMPL_BOSS_SEAT] _BOSS_SEAT_ACTIVITY_CLEANUP")
	g_bSecuroQuickExitOfficeChair = FALSE
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_FORCE_STEP_TYPE(PLAYER_PED_ID(), FALSE, 20, 0)
	ENDIF
	
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	CLEAR_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_HELP_TEXT_DISPLAYING)
	SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_LOCATE_CHECK)
	
	_BOSS_SEAT_SAFE_CLEAR_CONTEXT(activityData)
	_BOSS_SEAT_ENABLE_INTERACTION_MENU(activityData)
	
	activityData.iNetSceneID	= 0
	activityData.iLocalSceneID 	= -1
	_BOSS_SEAT_RELEASE_ANIM_DICT(activityData)
ENDPROC

FUNC BOOL _IS_BOSS_SEAT_SAFE_FOR_LOCAL_PLAYER_TO_USE(SMPL_BOSS_SEAT_DATA &activityData)
	IF NOT SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_HAS_ACCESS_TO_BOSS_SEAT)
		RETURN FALSE	
	ENDIF
	
	IF IS_ANY_INTERACTION_ANIM_PLAYING()
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
	OR IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_RUNNING(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PED_SPRINTING(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF g_iSimpleInteriorState != SIMPLE_INT_STATE_IDLE
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    CHecks if the player is in position to be prompted to use the desk chair
/// PARAMS:
///    activityData - Primary data struct
/// RETURNS:
///    True if the player is in either left or right side angled area
FUNC BOOL _BOSS_SEAT_IS_PLAYER_IN_LOCATE(SMPL_BOSS_SEAT_DATA &activityData)
	
	IF NOT _IS_BOSS_SEAT_SAFE_FOR_LOCAL_PLAYER_TO_USE(activityData)
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), activityData.sRightSideLocate.vecA, activityData.sRightSideLocate.vecB, activityData.sRightSideLocate.fWidth)
	AND IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), activityData.sRightSideLocate.fHeading, activityData.sRightSideLocate.fHeadingLeeway)
		CLEAR_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_ENTERED_LEFT_SIDE)
		RETURN TRUE
	ENDIF
	
	IF SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_SUPPORTS_LEFT_SIDE_ENTRY)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), activityData.sLeftSideLocate.vecA, activityData.sLeftSideLocate.vecB, activityData.sLeftSideLocate.fWidth)
		AND IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), activityData.sLeftSideLocate.fHeading, activityData.sRightSideLocate.fHeadingLeeway)
			SET_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_ENTERED_LEFT_SIDE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC _BOSS_SEAT_REQUEST_ANIM_DICT(SMPL_BOSS_SEAT_DATA &activityData)
	IF NOT IS_STRING_NULL_OR_EMPTY(activityData.sAnimDict)
		SET_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_REQUESTED_ANIM_DICT)
		REQUEST_ANIM_DICT(activityData.sAnimDict)
	ENDIF
ENDPROC

PROC _BOSS_SEAT_REGISTER_SIT_DOWN_PROMPT(SMPL_BOSS_SEAT_DATA &activityData)
	IF activityData.iContextIntention = NEW_CONTEXT_INTENTION
		REGISTER_CONTEXT_INTENTION(activityData.iContextIntention, CP_HIGH_PRIORITY, activityData.sSitDownPrompt)
	ENDIF
ENDPROC

/// PURPOSE:
///    Registers a context intention with help text that checks the current input method
/// PARAMS:
///    activityData - Primary data struct 
PROC _BOSS_SEAT_REGISTER_USE_COMPUTER_PROMPT(SMPL_BOSS_SEAT_DATA &activityData)
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		TEXT_LABEL_15 tlPrompt = activityData.sUseComputerPrompt
		
		tlPrompt += "PC"
		
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlPrompt)
			_BOSS_SEAT_SAFE_CLEAR_CONTEXT(activityData)
		ENDIF
		
		IF activityData.iContextIntention = NEW_CONTEXT_INTENTION
			REGISTER_CONTEXT_INTENTION(activityData.iContextIntention, CP_HIGH_PRIORITY, tlPrompt)
		ENDIF
	ELSE
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(activityData.sUseComputerPrompt)
			_BOSS_SEAT_SAFE_CLEAR_CONTEXT(activityData)
		ENDIF
		
		IF activityData.iContextIntention = NEW_CONTEXT_INTENTION
			REGISTER_CONTEXT_INTENTION(activityData.iContextIntention, CP_HIGH_PRIORITY, activityData.sUseComputerPrompt)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_HIGHER_PRIORITY_HELP_SHOWN()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GB_REACTIVATE")
		// Blocking user input and help text for lower priority tasks		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Requests and loads the defined app script upon request
/// PARAMS:
///    activityData - Primary data struct
PROC _BOSS_SEAT_MAINTAIN_COMPUTER_SCRIPT_LAUNCHING(SMPL_BOSS_SEAT_DATA &activityData)	
	IF SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_LAUNCHING_COMPUTER_SCRIPT)
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(activityData.sScript)) >= 1
			SET_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_COMPUTER_SCRIPT_LAUNCHED)
			CLEAR_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_LAUNCHING_COMPUTER_SCRIPT)
			EXIT
		ENDIF
	
		IF HAS_SCRIPT_LOADED(activityData.sScript)
			START_NEW_SCRIPT(activityData.sScript, APP_INTERNET_STACK_SIZE)
			SET_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_COMPUTER_SCRIPT_LAUNCHED)
			CLEAR_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_LAUNCHING_COMPUTER_SCRIPT)
			SET_SCRIPT_AS_NO_LONGER_NEEDED(activityData.sScript)
		ELSE
			REQUEST_SCRIPT(activityData.sScript)
			PRINTLN("[SMPL_BOSS_SEAT] _BOSS_SEAT_MAINTAIN_COMPUTER_SCRIPT_LAUNCHING: Loading script: ", activityData.sScript)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a script with the hash activityData.sScript is running
/// PARAMS:
///    activityData - Primary data struct
/// RETURNS:
///    True if the computer script has been loaded and launched
FUNC BOOL _BOSS_SEAT_IS_COMPUTER_SCRIPT_RUNNING(SMPL_BOSS_SEAT_DATA &activityData)
	INT iScriptHash = GET_HASH_KEY(activityData.sScript)
	
	RETURN GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(iScriptHash) > 0
ENDFUNC

/// PURPOSE:
///    Manages some camera collision issues with the chair while playing some of the animations
/// PARAMS:
///    activityData - Primary data struct
///    fCapsuleSize - The capsule size the local player peds capsule should be set to
///    bRemoveCollision - Should collision between the player ped and seat object be removed this frame?
PROC _BOSS_SEAT_MANAGE_CAMERA_COLLISION(SMPL_BOSS_SEAT_DATA &activityData, FLOAT fCapsuleSize = 0.25, BOOL bRemoveCollision = TRUE)
	IF DOES_ENTITY_EXIST(activityData.objSeat)

		SET_PED_CAPSULE(PLAYER_PED_ID(), fCapsuleSize)				
		
		SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(activityData.objSeat)
		
		IF bRemoveCollision
			SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), activityData.objSeat, TRUE) // replacing collision between player and office chair.
		ENDIF
	ENDIF
ENDPROC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    PROP CREATION
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC BOOL _BOSS_SEAT_CREATE_SEAT_PROP(NETWORK_INDEX &netObj, SMPL_BOSS_SEAT_DATA &activityData)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netObj)
		RETURN TRUE
	ELIF NOT SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_RESERVED_NET_OBJ)
		IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
			PRINTLN("[SMPL_BOSS_SEAT] _BOSS_SEAT_CREATE_SEAT_PROP: Reserving net mission object")
			RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
			SET_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_RESERVED_NET_OBJ)
		ELSE
			PRINTLN("[SMPL_BOSS_SEAT] _BOSS_SEAT_CREATE_SEAT_PROP: officeSeats - CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT = FALSE")
		ENDIF
	ELIF CAN_REGISTER_MISSION_OBJECTS(1)
		netObj = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(activityData.seatModel, activityData.vSeatSpawnPosition))

		SET_ENTITY_ROTATION(NET_TO_OBJ(netObj), activityData.vSeatSpawnRotation)
		
		NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(netObj), TRUE)
		
		FREEZE_ENTITY_POSITION(NET_TO_OBJ(netObj), TRUE)
		
		PRINTLN("[SMPL_BOSS_SEAT] _BOSS_SEAT_CREATE_SEAT_PROP: CREATING officeSeats, position: ", activityData.vSeatSpawnPosition, " rotation: ", activityData.vSeatSpawnRotation)
		RETURN TRUE
	ELSE
		PRINTLN("[SMPL_BOSS_SEAT] _BOSS_SEAT_CREATE_SEAT_PROP: officeSeats - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL _BOSS_SEAT_ACTIVITY_MAINTAIN_PROP_CREATION(SMPL_BOSS_SEAT_DATA &activityData, SMPL_BOSS_SEAT_SERVER_DATA &serverData)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		RETURN NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverData.niSeat)
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverData.niSeat)
		IF REQUEST_LOAD_MODEL(activityData.seatModel)
			
			IF NETWORK_IS_IN_MP_CUTSCENE()
				SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
			ENDIF
			
			IF _BOSS_SEAT_CREATE_SEAT_PROP(serverData.niSeat, activityData)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(activityData.seatModel)
				
				IF NETWORK_IS_IN_MP_CUTSCENE()
					SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
				ENDIF
				
				PRINTLN("[SMPL_BOSS_SEAT] _BOSS_SEAT_ACTIVITY_MAINTAIN_PROP_CREATION - Created prop with model: ", GET_MODEL_NAME_FOR_DEBUG(activityData.seatModel))
				
				RETURN TRUE
			ENDIF
		ELSE
			IF NOT REQUEST_LOAD_MODEL(activityData.seatModel)
				PRINTLN("[SMPL_BOSS_SEAT] _BOSS_SEAT_ACTIVITY_MAINTAIN_PROP_CREATION: ", GET_MODEL_NAME_FOR_DEBUG(activityData.seatModel), " not loaded")
			ENDIF
	   	ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    SEAT UNAVAILABLE HELPERS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC BOOL SHOULD_BLOCK_PC_ACCESS_FOR_PRIVATE_SESSION(SMPL_BOSS_SEAT_DATA &activityData)
	IF SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_ALLOW_IN_PRIVATE_SESSIONS)
		RETURN FALSE
	ENDIF
	
	IF NOT DID_I_JOIN_A_PRIVATE_SESSION()		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_USE_BOSS_SEAT(SMPL_BOSS_SEAT_DATA &activityData)
	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
	AND NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
	AND NOT SHOULD_BLOCK_PC_ACCESS_FOR_PRIVATE_SESSION(activityData)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_BLOCKED_COMPUTER_HELP(SMPL_BOSS_SEAT_DATA &activityData, SMPL_BOSS_SEAT_BLOCKED_REASON eBlockReason)

	SWITCH eBlockReason
		CASE SMPL_BSS_BLOCK_REASON_ON_MISSION			RETURN "BUNK_PC_BLCK_M"						BREAK
		CASE SMPL_BSS_BLOCK_REASON_NOT_BOSS				RETURN activityData.sRegisterAsBossPrompt	BREAK	
		CASE SMPL_BSS_BLOCK_PRIVATE_SESSION				RETURN "BUNK_PC_BLCK2"						BREAK
		CASE SMPL_BSS_BLOCK_REASON_MAX_GANGS_REACHED	RETURN "BUNK_PC_BLCK3"						BREAK
		CASE SMPL_BSS_BLOCK_REASON_BEHAVIOUR			RETURN "BUNK_PC_BLCK4"						BREAK
	ENDSWITCH
	
	RETURN "UNKNOWN REASON"
ENDFUNC

/// PURPOSE:
///    Registers a context intention with relevant help
/// PARAMS:
///    activityData - Primary data struct
///    eBlockReason - The reason for entry being blocked
PROC _BOSS_SEAT_REGISTER_SEAT_UNAVAILABLE_CONTEXT(SMPL_BOSS_SEAT_DATA &activityData, SMPL_BOSS_SEAT_BLOCKED_REASON eBlockReason)
	IF activityData.iContextIntention != NEW_CONTEXT_INTENTION
	AND NOT IS_BIT_SET(activityData.iBlockedBS, ENUM_TO_INT(eBlockReason))
		RELEASE_CONTEXT_INTENTION(activityData.iContextIntention)
	ENDIF
	
	IF activityData.iContextIntention = NEW_CONTEXT_INTENTION
		REGISTER_CONTEXT_INTENTION(activityData.iContextIntention, CP_HIGH_PRIORITY, GET_BLOCKED_COMPUTER_HELP(activityData, eBlockReason))
		PRINTLN("[SMPL_BOSS_SEAT] RUN_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY - Register seat unavailable reason: ", eBlockReason)
		SET_BIT(activityData.iBlockedBS, ENUM_TO_INT(eBlockReason))
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks conditions for showing blocked seat helper prompts
/// PARAMS:
///    activityData - Main struct
PROC _BOSS_SEAT_CHECK_AND_REGISTER_UNAVAILABLE_CONTEXT(SMPL_BOSS_SEAT_DATA &activityData)
	IF SHOULD_BLOCK_PC_ACCESS_FOR_PRIVATE_SESSION(activityData)
		_BOSS_SEAT_REGISTER_SEAT_UNAVAILABLE_CONTEXT(activityData, SMPL_BSS_BLOCK_PRIVATE_SESSION)
	ELIF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		IF GB_GET_NUM_BOSSES_IN_SESSION() >= GB_GET_MAX_NUM_GANGS()
			_BOSS_SEAT_REGISTER_SEAT_UNAVAILABLE_CONTEXT(activityData, SMPL_BSS_BLOCK_REASON_MAX_GANGS_REACHED)
		ELIF GB_IS_VIP_GAMEPLAY_DISABLED_FOR_PLAYER(PLAYER_ID())
			_BOSS_SEAT_REGISTER_SEAT_UNAVAILABLE_CONTEXT(activityData, SMPL_BSS_BLOCK_REASON_BEHAVIOUR)
		ELIF NETWORK_IS_ACTIVITY_SESSION()
			_BOSS_SEAT_REGISTER_SEAT_UNAVAILABLE_CONTEXT(activityData, SMPL_BSS_BLOCK_REASON_ON_MISSION)
		ELSE
			_BOSS_SEAT_REGISTER_SEAT_UNAVAILABLE_CONTEXT(activityData, SMPL_BSS_BLOCK_REASON_NOT_BOSS)
		ENDIF
	ELIF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		_BOSS_SEAT_REGISTER_SEAT_UNAVAILABLE_CONTEXT(activityData, SMPL_BSS_BLOCK_REASON_ON_MISSION)
	ENDIF
ENDPROC

FUNC BOOL _BOSS_SEAT_IS_SEAT_UNAVAILABLE_PROMPT_DISPLAYED(SMPL_BOSS_SEAT_DATA &activityData)
	RETURN activityData.iBlockedBS != 0
ENDFUNC

PROC _BOSS_SEAT_CLEAR_SEAT_UNAVAILABLE_PROMPT(SMPL_BOSS_SEAT_DATA &activityData)
	IF _BOSS_SEAT_IS_SEAT_UNAVAILABLE_PROMPT_DISPLAYED(activityData)
		activityData.iBlockedBS = 0		
		_BOSS_SEAT_SAFE_CLEAR_CONTEXT(activityData)
	ENDIF
ENDPROC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    LOCATION FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC _BOSS_SEAT_TASK_LOCAL_PED_TO_GO_TO_SEAT_COORD_IF_NEEDED(VECTOR vGotoPos, FLOAT fTargetHeading)
	IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(PLAYER_PED_ID()), vGotoPos, BS_BOSS_SEAT_TASK_LOCAL_PED_TO_GO_TO_SEAT_COORD_TOLLERANCE)
		IF NOT IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), fTargetHeading, BS_ANIM_TRIGGER_HEADING_LEEWAY)
			TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), fTargetHeading)
			PRINTLN("[SMPL_BOSS_SEAT] _BOSS_SEAT_TASK_LOCAL_PED_TO_GO_TO_SEAT_COORD_IF_NEEDED - TASK_ACHIEVE_HEADING")
			EXIT
		ENDIF
		
		PRINTLN("[SMPL_BOSS_SEAT] _BOSS_SEAT_TASK_LOCAL_PED_TO_GO_TO_SEAT_COORD_IF_NEEDED - No tasking required!")
	ELSE
		TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vGotoPos, PEDMOVEBLENDRATIO_WALK, 100, fTargetHeading, 0.1)
		PRINTLN("[SMPL_BOSS_SEAT] _BOSS_SEAT_TASK_LOCAL_PED_TO_GO_TO_SEAT_COORD_IF_NEEDED - TASK_GO_STRAIGHT_TO_COORD")
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns by ref the entry animation initial offset position
/// PARAMS:
///    activityData - Primary data struct
///    vAnimStartPos - Anim start position
///    fAnimStartHead - Anim start heading 
PROC _BOSS_SEAT_GET_ENTER_ANIM_OFFSET_POSITION(SMPL_BOSS_SEAT_DATA &activityData, VECTOR &vAnimStartPos, FLOAT &fAnimStartHead)
	VECTOR vTempRotation
	TEXT_LABEL_15 tlAnim = "enter"
	
	IF SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_ENTERED_LEFT_SIDE)
		tlAnim += "_left"
	ENDIF
	
	vAnimStartPos = GET_ANIM_INITIAL_OFFSET_POSITION(activityData.sAnimDict, tlAnim, activityData.vSeatSpawnPosition, activityData.vSeatSpawnRotation)  
	vTempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(activityData.sAnimDict, tlAnim, activityData.vSeatSpawnPosition, activityData.vSeatSpawnRotation)
	
	fAnimStartHead = vTempRotation.Z	
ENDPROC

/// PURPOSE:
///    Checks the local players position against the initial position of the entry animation
/// PARAMS:
///    activityData - Primary data struct
/// RETURNS:
///    True if the local player is in position to start the entry animation
FUNC BOOL _BOSS_SEAT_IS_LOCAL_PLAYER_IN_POSITION_TO_START_ENTRY(SMPL_BOSS_SEAT_DATA &activityData)
	VECTOR vAnimTriggerPos
	FLOAT fAnimTriggerHead
	
	_BOSS_SEAT_GET_ENTER_ANIM_OFFSET_POSITION(activityData, vAnimTriggerPos, fAnimTriggerHead)
	
	IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(PLAYER_PED_ID()), vAnimTriggerPos, BS_ANIM_TRIGGER_DIST)
	AND IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), fAnimTriggerHead, BS_ANIM_TRIGGER_HEADING_LEEWAY)
	OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
		RETURN TRUE
	ENDIF
	
	PRINTLN("_BOSS_SEAT_IS_LOCAL_PLAYER_IN_POSITION_TO_START_ENTRY - waiting")
	#IF IS_DEBUG_BUILD
		IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(PLAYER_PED_ID()), vAnimTriggerPos, BS_ANIM_TRIGGER_DIST)
			PRINTLN("_BOSS_SEAT_IS_LOCAL_PLAYER_IN_POSITION_TO_START_ENTRY - ARE_VECTORS_ALMOST_EQUAL = FALSE")
		ENDIF
		IF NOT IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), fAnimTriggerHead, BS_ANIM_TRIGGER_HEADING_LEEWAY)
			PRINTLN("_BOSS_SEAT_IS_LOCAL_PLAYER_IN_POSITION_TO_START_ENTRY - IS_HEADING_ACCEPTABLE = FALSE")
		ENDIF
		IF NOT (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK)
			PRINTLN("_BOSS_SEAT_IS_LOCAL_PLAYER_IN_POSITION_TO_START_ENTRY - SCRIPT_TASK_GO_STRAIGHT_TO_COORD = FALSE")
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    ANIM & SCENE FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC STRING _BOSS_SEAT_GET_RANDOM_IDLE_ANIM()
	INT iIdelVar = GET_RANDOM_INT_IN_RANGE(0, 5)
	
	SWITCH iIdelVar
		CASE 0 RETURN "idle_a"	BREAK
		CASE 1 RETURN "idle_b"	BREAK
		CASE 2 RETURN "idle_c"	BREAK
		CASE 3 RETURN "idle_d"	BREAK
		CASE 4 RETURN "idle_e"	BREAK
	ENDSWITCH
	
	RETURN "idle_a"
ENDFUNC

/// PURPOSE:
///    Starts a sync scene using the local player ped and the created chair
/// PARAMS:
///    activityData - Primary data struct
///    sAnim - Animation to play from the dictioanry set at the start
///    pedFlags - scene playback flags set on the ped only
///    chairFlags - scene playback flags set on the chair object only
///    fPedBlendInDelta - Blend in - Ped
///    fPedBlendOutDelta -  Blend out - ped
///    fChairBlendInDelta - Blend in - chair
///    fChairBlendOutDelta - Blend out - chair
///    fMoverBlendInDelta - Mover blend control - ped
///    controlFlags - IK control for the ped
PROC _BOSS_SEAT_START_SYNC_SCENE(SMPL_BOSS_SEAT_DATA &activityData, 
									STRING sAnim, SYNCED_SCENE_PLAYBACK_FLAGS pedFlags, 
									SYNCED_SCENE_PLAYBACK_FLAGS chairFlags, 
									FLOAT fPedBlendInDelta = SLOW_BLEND_IN, 
									FLOAT fPedBlendOutDelta = WALK_BLEND_OUT, 
									FLOAT fChairBlendInDelta = SLOW_BLEND_IN, 
									FLOAT fChairBlendOutDelta = SLOW_BLEND_OUT, 
									FLOAT fMoverBlendInDelta = DEFAULT_MOVER_BLEND_IN_DELTA, 
									IK_CONTROL_FLAGS controlFlags = AIK_NONE, 
									FLOAT fScenePlaybackRate = 1.0, 
									BOOL bResetChair = FALSE)
	
	VECTOR vScenePosition = GET_ENTITY_COORDS(activityData.objSeat)
	VECTOR vSceneRotation = GET_ENTITY_ROTATION(activityData.objSeat)
	IF (bResetChair)
		vScenePosition = activityData.vSeatSpawnPosition 
		vSceneRotation = activityData.vSeatSpawnRotation 
	ENDIF
	
	TEXT_LABEL_31 tlChairAnim = sAnim
	tlChairAnim += "_chair"
	activityData.iLocalSceneID = -1
	
	activityData.iNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePosition, vSceneRotation, EULER_YXZ, TRUE, FALSE, 1, 0, fScenePlaybackRate)		

	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), activityData.iNetSceneID, activityData.sAnimDict, sAnim, fPedBlendInDelta, fPedBlendOutDelta, pedFlags,  RBF_PLAYER_IMPACT, fMoverBlendInDelta, controlFlags)
	NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(activityData.objSeat, activityData.iNetSceneID, activityData.sAnimDict, tlChairAnim, fChairBlendInDelta, fChairBlendOutDelta, chairFlags)
	
	NETWORK_START_SYNCHRONISED_SCENE(activityData.iNetSceneID)
	
	PRINTLN("[SMPL_BOSS_SEAT] _BOSS_SEAT_START_SYNC_SCENE - iNetSceneID: ", activityData.iNetSceneID, ", dict: ", activityData.sAnimDict, ", animClip = ", sAnim, " fScenePlaybackRate = ", fScenePlaybackRate, " bResetChair = ", bResetChair)
ENDPROC

/// PURPOSE:
///    Grabs the local scene ID from the currently running net scene ID
/// PARAMS:
///    activityData - Primary data struct
PROC _BOSS_SEAT_GET_LOCAL_SCENE_ID(SMPL_BOSS_SEAT_DATA &activityData)
	IF activityData.iLocalSceneID = -1
		activityData.iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(activityData.iNetSceneID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Tasks the local player to play computer_enter
/// PARAMS:
///    activityData - Primary data struct
PROC _BOSS_SEAT_PLAY_COMPUTER_ENTER_ANIM(SMPL_BOSS_SEAT_DATA &activityData)
	SYNCED_SCENE_PLAYBACK_FLAGS pedFlags 	= SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS
	SYNCED_SCENE_PLAYBACK_FLAGS chairFlags 	= SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS
	
	_BOSS_SEAT_START_SYNC_SCENE(activityData, "computer_enter", pedFlags, chairFlags, SLOW_BLEND_IN, WALK_BLEND_OUT, SLOW_BLEND_IN, SLOW_BLEND_OUT, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK, BS_PLAY_COMPUTER_ENTER_ANIM_SPEED)
	
	SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_USE_COMPUTER)
ENDPROC

/// PURPOSE:
///    Tasks the local player to play computer_idle
/// PARAMS:
///    activityData - Primary data struct
PROC _BOSS_SEAT_PLAY_COMPUTER_IDLE_ANIM(SMPL_BOSS_SEAT_DATA &activityData)
	SYNCED_SCENE_PLAYBACK_FLAGS pedFlags 	= SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS
	SYNCED_SCENE_PLAYBACK_FLAGS chairFlags 	= SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS
	
	_BOSS_SEAT_START_SYNC_SCENE(activityData, "computer_idle", pedFlags, chairFlags, SLOW_BLEND_IN, WALK_BLEND_OUT, SLOW_BLEND_IN, SLOW_BLEND_OUT, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
ENDPROC

/// PURPOSE:
///    Helper function to check if the currently running syn scene is finished
/// PARAMS:
///    activityData - Primary data struct
///    bCheckControlOfChair - Will prevent a return TRUE until the local player has network control of the chair
/// RETURNS:
///    True if the scene is finished
FUNC BOOL _BOSS_SEAT_IS_SCENE_FINISHED(SMPL_BOSS_SEAT_DATA &activityData, BOOL bCheckControlOfChair = FALSE)
	_BOSS_SEAT_GET_LOCAL_SCENE_ID(activityData)
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(activityData.iLocalSceneID)
		
		IF bCheckControlOfChair
		AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(activityData.objSeat)
			NETWORK_REQUEST_CONTROL_OF_ENTITY(activityData.objSeat)
			RETURN FALSE
		ENDIF
		
		RETURN GET_SYNCHRONIZED_SCENE_PHASE(activityData.iLocalSceneID) >= 1 // Has the idle or enter anim finished yet
	ENDIF
	
	RETURN FALSE
ENDFUNC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    INPUT HELPERS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Chekcks for input to trigger the exit animation
/// RETURNS:
///    True if the player can and has requested to exit
FUNC BOOL _BOSS_SEAT_CHECK_FOR_EXIT_REQUEST()
	
	//Are we using KBM or a controller
	CONTROL_ACTION exitControlAction 
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		exitControlAction = INPUT_SCRIPT_RRIGHT
	ELSE	
		exitControlAction = INPUT_FRONTEND_RIGHT
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, exitControlAction)
	AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND NOT IS_SELECTOR_UI_BUTTON_PRESSED()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if the player wants to use the computer
/// PARAMS:
///    activityData - Primary data struct
/// RETURNS:
///    True if the player can and has requested to use the computer app
FUNC BOOL _BOSS_SEAT_CHECK_FOR_USE_COMPUTER_REQUEST(SMPL_BOSS_SEAT_DATA &activityData)
	IF NOT IS_INTERACTION_MENU_OPEN()
	AND NOT IS_SELECTOR_UI_BUTTON_PRESSED()
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_HAS_ACCESS_TO_BOSS_SEAT)
	AND NOT GB_IS_VIP_GAMEPLAY_DISABLED_FOR_PLAYER(PLAYER_ID())
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if we are passed the phase of computer entry animation to allow the computer script to start
/// PARAMS:
///    activityData - Primary data struct
PROC _BOSS_SEAT_LAUNCH_COMPUTER_SCRIPT(SMPL_BOSS_SEAT_DATA &activityData)
	FLOAT fPhase = 0.22
				
	IF IS_PED_FEMALE(PLAYER_PED_ID())
		fPhase = 0.53
	ENDIF
	
	IF GET_SYNCHRONIZED_SCENE_PHASE(activityData.iLocalSceneID) >= fPhase
	AND IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), activityData.sAnimDict, "COMPUTER_ENTER")
	
		IF NOT SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_LAUNCHING_COMPUTER_SCRIPT)
		AND NOT SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_COMPUTER_SCRIPT_LAUNCHED)
		AND NOT _BOSS_SEAT_IS_COMPUTER_SCRIPT_RUNNING(activityData)
			SET_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_LAUNCHING_COMPUTER_SCRIPT)		
		ENDIF
		
	ENDIF
ENDPROC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    DEBUG
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD
PROC SMPL_BOSS_SEAT_ADD_DEBUG_WIDGETS(SMPL_BOSS_SEAT_DEBUG &activityDebug) 
	START_WIDGET_GROUP("Boss Seat")
		ADD_WIDGET_BOOL("Enable seat position editing", activityDebug.bEnableSeatPositionEditing)
		ADD_WIDGET_BOOL("Reset chair position", activityDebug.bResetChairLoc)
		ADD_WIDGET_BOOL("Output chair position", activityDebug.bOutputSeatLocation)
		ADD_WIDGET_VECTOR_SLIDER("Seat position", activityDebug.vSeatPosition, -2000.0, 3000.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Seat heading", activityDebug.fSeatHeading, -360.0, 360.0, 0.1)
		
		START_WIDGET_GROUP("Tweaks")
			ADD_WIDGET_FLOAT_SLIDER("BS_ANIM_TRIGGER_DIST", BS_ANIM_TRIGGER_DIST, 0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("BS_BOSS_SEAT_TASK_LOCAL_PED_TO_GO_TO_SEAT_COORD_TOLLERANCE", BS_BOSS_SEAT_TASK_LOCAL_PED_TO_GO_TO_SEAT_COORD_TOLLERANCE, 0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("BS_ANIM_TRIGGER_HEADING_LEEWAY", BS_ANIM_TRIGGER_HEADING_LEEWAY, 0, 360.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("BS_PLAY_COMPUTER_ENTER_ANIM_SPEED", BS_PLAY_COMPUTER_ENTER_ANIM_SPEED, 0, 5.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("BS_BOSS_SEAT_STATE_ENTER_ANIM_SPEED", BS_BOSS_SEAT_STATE_ENTER_ANIM_SPEED, 0, 5.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("BS_BOSS_SEAT_STATE_ENTER_ANIM_SPEED_LEFT", BS_BOSS_SEAT_STATE_ENTER_ANIM_SPEED_LEFT, 0, 5.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("BS_BOSS_SEAT_STATE_PLAYING_BASE_ANIM_SPEED", BS_BOSS_SEAT_STATE_PLAYING_BASE_ANIM_SPEED, 0, 5.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("BS_BOSS_SEAT_STATE_SEAT_EXIT_ANIM_SPEED", BS_BOSS_SEAT_STATE_SEAT_EXIT_ANIM_SPEED, 0, 5.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("BS_BOSS_SEAT_STATE_COMPUTER_EXIT_ANIM_SPEED", BS_BOSS_SEAT_STATE_COMPUTER_EXIT_ANIM_SPEED, 0, 5.0, 0.01)		
			ADD_WIDGET_FLOAT_SLIDER("BS_BOSS_SEAT_STATE_ENTER_CAPSULE_SIZE", BS_BOSS_SEAT_STATE_ENTER_CAPSULE_SIZE, 0, 1.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("BS_CHAIR_SLIDE_TOLLERANCE", BS_CHAIR_SLIDE_TOLLERANCE, 0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("BS_CHAIR_SLIDE_SPEED", BS_CHAIR_SLIDE_SPEED, 0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("BS_CHAIR_SLIDE_ROT_TOLLERANCE", BS_CHAIR_SLIDE_ROT_TOLLERANCE, 0, 10.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("BS_CHAIR_SLIDE_ROT_SPEED", BS_CHAIR_SLIDE_ROT_SPEED, 0, 1.0, 0.01)
		STOP_WIDGET_GROUP()		
		
	STOP_WIDGET_GROUP()
ENDPROC

PROC SMPL_BOSS_SEAT_PROCESS_DEBUG_WIDGETS(SMPL_BOSS_SEAT_DATA &activityData)
	IF activityData.debug.bResetChairLoc
		IF DOES_ENTITY_EXIST(activityData.objSeat)
			SET_ENTITY_COORDS(activityData.objSeat, activityData.vSeatSpawnPosition, FALSE)
		ENDIF
		
		activityData.debug.vSeatPosition = activityData.vSeatSpawnPosition
		activityData.debug.fSeatHeading = activityData.vSeatSpawnRotation.z
		activityData.debug.bResetChairLoc = FALSE
	ENDIF
	
	IF activityData.debug.bEnableSeatPositionEditing
		IF IS_VECTOR_ZERO(activityData.debug.vSeatPosition)
			activityData.debug.vSeatPosition = activityData.vSeatSpawnPosition
			activityData.debug.fSeatHeading = activityData.vSeatSpawnRotation.z
		ENDIF
		
		IF DOES_ENTITY_EXIST(activityData.objSeat)
			SET_ENTITY_COORDS(activityData.objSeat, activityData.debug.vSeatPosition, FALSE)
			SET_ENTITY_HEADING(activityData.objSeat, activityData.debug.fSeatHeading)
		ENDIF
	ENDIF
	
	IF activityData.debug.bOutputSeatLocation
		
		IF DOES_ENTITY_EXIST(activityData.objSeat)
			VECTOR vLocation 	= GET_ENTITY_COORDS(activityData.objSeat)
			FLOAT fHeading 		= GET_ENTITY_HEADING(activityData.objSeat)
			
			OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("// -- -- SIMPLE INTERIOR BOSS SEAT LOCATION  -- -- -- //")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("Real world location ")									SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("vSpawnPoint = ") SAVE_VECTOR_TO_DEBUG_FILE(vLocation) 	SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fSpawnHeading = ") SAVE_FLOAT_TO_DEBUG_FILE(fHeading) 	SAVE_NEWLINE_TO_DEBUG_FILE()
			
			vLocation = TRANSFORM_WORLD_COORDS_TO_SIMPLE_INTERIOR_COORDS_GIVEN_ID(vLocation, GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN())
			fHeading = TRANSFORM_WORLD_HEADING_TO_SIMPLE_INTERIOR_HEADING_GIVEN_ID(fHeading, GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN())
			
			SAVE_STRING_TO_DEBUG_FILE("Interior relative location ")							SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("vSpawnPoint = ") SAVE_VECTOR_TO_DEBUG_FILE(vLocation) 	SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("fSpawnHeading = ") SAVE_FLOAT_TO_DEBUG_FILE(fHeading) 	SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			CLOSE_DEBUG_FILE()
		ENDIF
		
		activityData.debug.bOutputSeatLocation = FALSE
	ENDIF
ENDPROC
#ENDIF

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    STATE FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC _RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_PROMPT(SMPL_BOSS_SEAT_DATA &activityData)
	IF _BOSS_SEAT_IS_PLAYER_IN_LOCATE(activityData)
		IF NOT CAN_PLAYER_USE_BOSS_SEAT(activityData)
			//We have a condition where we should show relevant help to inform the player of the block reason
			_BOSS_SEAT_CHECK_AND_REGISTER_UNAVAILABLE_CONTEXT(activityData)
		ELSE
			//Preload the animation dictionary
			_BOSS_SEAT_REQUEST_ANIM_DICT(activityData)
			
			//Clear any unavailable seat help
			_BOSS_SEAT_CLEAR_SEAT_UNAVAILABLE_PROMPT(activityData)
						
			IF IS_HIGHER_PRIORITY_HELP_SHOWN()
				_BOSS_SEAT_SAFE_CLEAR_CONTEXT(activityData)
			ELSE
				//Show the prompt to sit
				_BOSS_SEAT_REGISTER_SIT_DOWN_PROMPT(activityData)
			ENDIF
			
			//Check for the input
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
				SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_GO_TO_START)
				
				_BOSS_SEAT_SAFE_CLEAR_CONTEXT(activityData)
				_BOSS_SEAT_DISABLE_INTERACTION_MENU(activityData)
				
				// Fix for B*3058140
				IF IS_PED_WEARING_PARACHUTE_MP(PLAYER_PED_ID())
					// Remove parachute if we are wearing one to ensure no clipping with seat.
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0,0)
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, FALSE) 
				ENDIF
			ENDIF
		ENDIF
	ELSE
		_BOSS_SEAT_SAFE_CLEAR_CONTEXT(activityData)				
		_BOSS_SEAT_RELEASE_ANIM_DICT(activityData)
	ENDIF
ENDPROC

PROC _RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_GO_TO_START(SMPL_BOSS_SEAT_DATA &activityData)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
	
	IF NOT HAS_ANIM_DICT_LOADED(activityData.sAnimDict)
		_BOSS_SEAT_REQUEST_ANIM_DICT(activityData)
		PRINTLN("[SMPL_BOSS_SEAT] RUN_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY - HAS_ANIM_DICT_LOADED = FALSE - Dictionary = ", activityData.sAnimDict)
		EXIT
	ENDIF
				
	IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
		MP_PROP_OFFSET_STRUCT officeModelLoc
		PRINTLN("[SMPL_BOSS_SEAT] RUN_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY: TASK_LOOK_AT_COORD for boss seat entry as we're in first person")
		TASK_LOOK_AT_COORD(PLAYER_PED_ID(), officeModelLoc.vLoc, 5000)
	ENDIF
	
	//Task GOTO
	VECTOR vAnimStartPos
	FLOAT fAnimStartHeading
	_BOSS_SEAT_GET_ENTER_ANIM_OFFSET_POSITION(activityData, vAnimStartPos, fAnimStartHeading)
	_BOSS_SEAT_TASK_LOCAL_PED_TO_GO_TO_SEAT_COORD_IF_NEEDED(vAnimStartPos, fAnimStartHeading)
	
	SET_FORCE_STEP_TYPE(PLAYER_PED_ID(), TRUE, 20, 0)
	SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_SEAT_ENTER)
ENDPROC

PROC _RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_ENTER(SMPL_BOSS_SEAT_DATA &activityData)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
	
	IF _BOSS_SEAT_IS_LOCAL_PLAYER_IN_POSITION_TO_START_ENTRY(activityData)
		
		STRING sAnim = "enter"
		FLOAT fAnimSpeed = BS_BOSS_SEAT_STATE_ENTER_ANIM_SPEED
		
		IF SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_ENTERED_LEFT_SIDE)
			sAnim = "enter_left"
			fAnimSpeed = BS_BOSS_SEAT_STATE_ENTER_ANIM_SPEED_LEFT
		ENDIF
		
		SYNCED_SCENE_PLAYBACK_FLAGS pedFlags 	= SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_USE_PHYSICS
		SYNCED_SCENE_PLAYBACK_FLAGS chairFlags 	= SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_USE_PHYSICS| SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS
		
		_BOSS_SEAT_START_SYNC_SCENE(activityData, sAnim, pedFlags, chairFlags, WALK_BLEND_IN, WALK_BLEND_OUT, SLOW_BLEND_IN, SLOW_BLEND_OUT, WALK_BLEND_IN, AIK_NONE, fAnimSpeed, TRUE)
		
		SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_SEAT_TRANSITION_TO_BASE)
	ELSE
		SET_PED_CAPSULE(PLAYER_PED_ID(), BS_BOSS_SEAT_STATE_ENTER_CAPSULE_SIZE)
		PRINTLN("[SMPL_BOSS_SEAT] RUN_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY - Waiting for local player to be in position")
	ENDIF
ENDPROC

PROC _SMPL_INTERIOR_BOSS_SEAT_PLAY_BASE_ANIM(SMPL_BOSS_SEAT_DATA &activityData)
	SYNCED_SCENE_PLAYBACK_FLAGS pedFlags 	= SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_USE_PHYSICS
	SYNCED_SCENE_PLAYBACK_FLAGS chairFlags 	= SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS
		
	_BOSS_SEAT_START_SYNC_SCENE(activityData, "base", pedFlags, chairFlags)	
ENDPROC

PROC _RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_TRANSITION_TO_BASE(SMPL_BOSS_SEAT_DATA &activityData)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
	
	_BOSS_SEAT_MANAGE_CAMERA_COLLISION(activityData)
	
	IF _BOSS_SEAT_IS_SCENE_FINISHED(activityData, TRUE)			
		_SMPL_INTERIOR_BOSS_SEAT_PLAY_BASE_ANIM(activityData)
		SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_PLAY_BASE_ANIM)
	ENDIF
ENDPROC

PROC _RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_PLAYING_BASE_ANIM(SMPL_BOSS_SEAT_DATA &activityData)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
		
	_BOSS_SEAT_MANAGE_CAMERA_COLLISION(activityData)
	
	//Prompt the player
	_BOSS_SEAT_REGISTER_USE_COMPUTER_PROMPT(activityData)
	
	//Check if the player wants to leave the seat
	IF _BOSS_SEAT_CHECK_FOR_EXIT_REQUEST()		
		_BOSS_SEAT_SAFE_CLEAR_CONTEXT(activityData)		
		SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_SEAT_EXIT)
		EXIT
	ENDIF
	
	//Check if the player wants to use the computer
	IF _BOSS_SEAT_CHECK_FOR_USE_COMPUTER_REQUEST(activityData)
		_BOSS_SEAT_SAFE_CLEAR_CONTEXT(activityData)		
		_BOSS_SEAT_PLAY_COMPUTER_ENTER_ANIM(activityData)		
		EXIT
	ENDIF
	
	//Transition to the idle anim if required
	IF _BOSS_SEAT_IS_SCENE_FINISHED(activityData, TRUE)
		IF SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_PREVENT_IDLE_ANIM)
			_SMPL_INTERIOR_BOSS_SEAT_PLAY_BASE_ANIM(activityData)
			SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_PLAY_BASE_ANIM)
		ELSE
			STRING sAnim = _BOSS_SEAT_GET_RANDOM_IDLE_ANIM()
			
			SYNCED_SCENE_PLAYBACK_FLAGS pedFlags 	= SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_USE_PHYSICS
			SYNCED_SCENE_PLAYBACK_FLAGS chairFlags 	= SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS
			
			_BOSS_SEAT_START_SYNC_SCENE(activityData, sAnim, pedFlags, chairFlags, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, BS_BOSS_SEAT_STATE_PLAYING_BASE_ANIM_SPEED)
			_BOSS_SEAT_SAFE_CLEAR_CONTEXT(activityData)
			
			SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_SEAT_TRANSITION_TO_BASE)
		ENDIF
	ENDIF
ENDPROC

PROC _RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_SEAT_EXIT(SMPL_BOSS_SEAT_DATA &activityData)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
	
	IF NOT g_bSecuroQuickExitOfficeChair
	OR _BOSS_SEAT_IS_SCENE_FINISHED(activityData, TRUE)
		STRING sAnim = "exit"

		IF SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_ENTERED_LEFT_SIDE)
			sAnim = "exit_left"
		ENDIF
		
		SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_USE_PHYSICS
		
		_BOSS_SEAT_START_SYNC_SCENE(activityData, sAnim, flags, flags, SLOW_BLEND_IN, SLOW_BLEND_OUT, SLOW_BLEND_IN, SLOW_BLEND_OUT, INSTANT_BLEND_IN, DEFAULT, BS_BOSS_SEAT_STATE_SEAT_EXIT_ANIM_SPEED)
		
		SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_SEAT_CLEANUP)
	ENDIF
ENDPROC

PROC _RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_SEAT_USE_COMPUTER(SMPL_BOSS_SEAT_DATA &activityData)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
	
	_BOSS_SEAT_GET_LOCAL_SCENE_ID(activityData)
	_BOSS_SEAT_MANAGE_CAMERA_COLLISION(activityData)
	
	IF _BOSS_SEAT_IS_SCENE_FINISHED(activityData, TRUE)
		_BOSS_SEAT_PLAY_COMPUTER_IDLE_ANIM(activityData)
	ELIF IS_SYNCHRONIZED_SCENE_RUNNING(activityData.iLocalSceneID)
		_BOSS_SEAT_LAUNCH_COMPUTER_SCRIPT(activityData)
	ENDIF
	
	BOOL bAllowExit = IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), activityData.sAnimDict, "computer_idle")
	
	IF NOT _BOSS_SEAT_IS_COMPUTER_SCRIPT_RUNNING(activityData)
	AND bAllowExit
		SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), activityData.objSeat, TRUE)
		SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_COMPUTER_EXIT)
		CLEAR_SMPL_INTERIOR_BOSS_SEAT_BIT(activityData, SMPL_BSS_BIT_COMPUTER_SCRIPT_LAUNCHED)
	ENDIF
ENDPROC

PROC _RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_COMPUTER_EXIT(SMPL_BOSS_SEAT_DATA &activityData)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
	
	_BOSS_SEAT_MANAGE_CAMERA_COLLISION(activityData)
	STRING sAnim = "computer_exit"
	
	SYNCED_SCENE_PLAYBACK_FLAGS pedFlags = SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS
	SYNCED_SCENE_PLAYBACK_FLAGS chairFlags = SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS
	
	_BOSS_SEAT_START_SYNC_SCENE(activityData, sAnim, pedFlags, chairFlags, SLOW_BLEND_IN, WALK_BLEND_OUT, SLOW_BLEND_IN, SLOW_BLEND_OUT, DEFAULT_MOVER_BLEND_IN_DELTA, AIK_DISABLE_HEAD_IK, BS_BOSS_SEAT_STATE_COMPUTER_EXIT_ANIM_SPEED)
	
	IF g_bSecuroQuickExitOfficeChair = TRUE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SMPL_BOSS_SEAT] _RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_COMPUTER_EXIT: exiting because g_bSecuroQuickExitOfficeChair = TRUE")
		SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_SEAT_EXIT)
	ELSE
		SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_SEAT_TRANSITION_TO_BASE)
	ENDIF
ENDPROC

PROC SMPL_INTERIOR_BOSS_SEAT_VERIFY_DATA(SMPL_BOSS_SEAT_DATA &activityData)
	IF activityData.vSeatSpawnRotation.z > 180.0
		//It would be better to have this passed in correctly to start with, but <<0.0, 0.0, fHeading>> is common in the script project so its best to support it
		PRINTLN("[SMPL_BOSS_SEAT] SMPL_INTERIOR_BOSS_SEAT_VERIFY_DATA - vSeatSpawnRotation.z = ", activityData.vSeatSpawnRotation.z, " correcting to within rotation vector range")
		activityData.vSeatSpawnRotation.z =  activityData.vSeatSpawnRotation.z - 180.0
		activityData.vSeatSpawnRotation.z = 180.0 - activityData.vSeatSpawnRotation.z
		activityData.vSeatSpawnRotation.z *= -1
		
		PRINTLN("[SMPL_BOSS_SEAT] SMPL_INTERIOR_BOSS_SEAT_VERIFY_DATA - Correctted spawn location: ", activityData.vSeatSpawnRotation.z)
	ENDIF
ENDPROC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    MAIN LOOP
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

PROC RUN_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY(SMPL_BOSS_SEAT_DATA &activityData, SMPL_BOSS_SEAT_SERVER_DATA &serverData)
	
	IF g_bLaunchedMissionFrmLaptop
	AND NOT g_bSecuroQuickExitOfficeChair
		//No further update required. We are about to be removed from the current property
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	SMPL_BOSS_SEAT_PROCESS_DEBUG_WIDGETS(activityData)
	#ENDIF
	
	_BOSS_SEAT_MAINTAIN_COMPUTER_SCRIPT_LAUNCHING(activityData)
	
	SWITCH activityData.currentState
		CASE SMPL_BSS_INITIAL
			IF SMPL_INTERIOR_BOSS_SEAT_IS_BIT_SET(activityData, SMPL_BSS_BIT_DATA_SETUP)
				SMPL_INTERIOR_BOSS_SEAT_VERIFY_DATA(activityData)
				SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_CREATE_CHAIR)
			ENDIF
		BREAK
		CASE SMPL_BSS_CREATE_CHAIR
			IF _BOSS_SEAT_ACTIVITY_MAINTAIN_PROP_CREATION(activityData, serverData)
				activityData.objSeat = NET_TO_OBJ(serverdata.niSeat)
				SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_LOCATE_CHECK)
			ELSE
				PRINTLN("[SMPL_BOSS_SEAT] RUN_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY - Waiting to create a chair. Host? ", NETWORK_IS_HOST_OF_THIS_SCRIPT())
			ENDIF
		BREAK
		CASE SMPL_BSS_LOCATE_CHECK
			SLIDE_CHAIR_TO_SPAWN_POSITION(activityData)
			IF _BOSS_SEAT_IS_PLAYER_IN_LOCATE(activityData)
				SET_SMPL_INTERIOR_BOSS_SEAT_ACTIVITY_STATE(activityData, SMPL_BSS_REQUEST_PROMPT)
			ENDIF
		BREAK
		CASE SMPL_BSS_REQUEST_PROMPT			
			_RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_PROMPT(activityData)
		BREAK
		CASE SMPL_BSS_GO_TO_START
			_RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_GO_TO_START(activityData)
		BREAK
		CASE SMPL_BSS_SEAT_ENTER
			_RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_ENTER(activityData)
		BREAK
		CASE SMPL_BSS_SEAT_TRANSITION_TO_BASE
			_RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_TRANSITION_TO_BASE(activityData)
		BREAK
		CASE SMPL_BSS_PLAY_BASE_ANIM
			_RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_PLAYING_BASE_ANIM(activityData)
		BREAK
		CASE SMPL_BSS_USE_COMPUTER
			_RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_SEAT_USE_COMPUTER(activityData)
		BREAK
		CASE SMPL_BSS_COMPUTER_EXIT
			_RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_COMPUTER_EXIT(activityData)
		BREAK
		CASE SMPL_BSS_SEAT_EXIT
			_RUN_SMPL_INTERIOR_BOSS_SEAT_STATE_SEAT_EXIT(activityData)
		BREAK
		CASE SMPL_BSS_SEAT_CLEANUP
			DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)			
			_BOSS_SEAT_MANAGE_CAMERA_COLLISION(activityData, DEFAULT, FALSE)
			
			IF _BOSS_SEAT_IS_SCENE_FINISHED(activityData, TRUE)
			OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BREAKOUT_FINISH"))
			OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BREAK_OUT"))
				
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				ENDIF
				
				NETWORK_STOP_SYNCHRONISED_SCENE(activityData.iNetSceneID)
				
				_BOSS_SEAT_ACTIVITY_CLEANUP(activityData)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
