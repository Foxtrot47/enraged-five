

// Name:        net_above_head_display.sch
// Description: Controls displaying information above a players head
// Written By:  Ryan Baker

USING "script_player.sch"

USING "Hud_drawing.sch"
USING "net_weaponswap.sch"
//USING "net_hud_displays.sch"
USING "screen_placements.sch"
USING "hud_drawing.sch"
USING "mp_stat_friend_list.sch"
USING "mp_icons.sch"
USING "net_xp_func.sch"

#IF IS_DEBUG_BUILD
USING "net_debug.sch"
#ENDIF

 
CONST_INT OVERHEAD_MAX_NUMBER_OF_LINES	6


FUNC BOOL SHOULD_MOVE_FOR_HELPTEXT()
	RETURN MPGlobalsHud.bShouldMoveForHelp
ENDFUNC

PROC SET_MOVE_FOR_HELPTEXT(BOOL ShouldMove)
	MPGlobalsHud.bShouldMoveForHelp = ShouldMove
ENDPROC


FUNC FLOAT NET_GET_HEADING_FROM_COORDS(vector oldCoords,vector newCoords)
	FLOAT heading
	FLOAT dX = newCoords.x - oldCoords.x
	FLOAT dY = newCoords.y - oldCoords.y
	IF dY != 0
		heading = ATAN2(dX,dY)
	ELSE
		IF dX < 0
			heading = -90
		ELSE
			heading = 90
		ENDIF
	ENDIF

	RETURN heading
ENDFUNC

//Set text attributes
PROC NET_SET_TEXT_FOR_HUD(int r,int g,int b, float fScale, bool restrictScale=false)
	FLOAT fFontScale=0.319
	IF restrictScale = TRUE
		IF fScale < 0.7
			fScale = 0.7
		ENDIF
		IF fScale > 1.5	
			fScale = 1.5
		ENDIF
	ENDIF
	SET_TEXT_SCALE(fFontScale*fScale,fFontScale*fScale)
	SET_TEXT_COLOUR(r,g,b,200)
	SET_TEXT_DROP_SHADOW()
	SET_TEXT_OUTLINE()
ENDPROC

//Draw the how far away the AHD ped is
PROC NET_AHD_DRAW_RANGE_TEXT(float scale,vector v1, vector v2, float screenX, float screenY, int r, int g, int b)							
	float fRange = GET_DISTANCE_BETWEEN_COORDS(v1,v2,FALSE)
	int iRange = floor(fRange)


	NET_SET_TEXT_FOR_HUD(r,g,b,scale,true)
	
	IF iRange < 10
		DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.015,"AHD_DIST",iRange)
	ELSE
		IF iRange < 100
			DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.015,"AHD_DIST",iRange)
		ELSE	
			IF iRange < 1000								
				DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.015,"AHD_DIST",iRange)
			ELSE
				DISPLAY_TEXT_WITH_NUMBER(screenX,screenY+0.015,"AHD_DIST",iRange)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//If the AHD ped is off screen have an arrow pointing in their direction
PROC NET_DRAW_OFFSCREEN_ARROW_FOR_COORD(VECTOR vDestCoord, INT iR=255, INT iG=255, INT iB=0)
	//IF DOES_CAM_EXIST(GET_RENDERING_CAM())
	
		//NET_PRINT("     ----->     AHD - DO NET_DRAW_OFFSCREEN_ARROW_FOR_COORD      <-----")NET_NL()
	
		VECTOR camCoord = GET_GAMEPLAY_CAM_COORD() 	//GET_CAM_COORD(GET_RENDERING_CAM())
		VECTOR camRot = GET_GAMEPLAY_CAM_ROT()		//GET_CAM_ROT(GET_RENDERING_CAM())
		FLOAT dist = GET_DISTANCE_BETWEEN_COORDS(<<camCoord.x,camCoord.y,0>>,<<vDestCoord.x,vDestCoord.y,0>>)
		FLOAT height = vDestCoord.z - camCoord.z
		FLOAT fPitchToPed
		IF dist > 0
			fPitchToPed = ATAN(height/dist)
		ELSE
			fPitchToPed = 0
		ENDIF
		
		FLOAT fHeadingToPed = NET_GET_HEADING_FROM_COORDS(camCoord,vDestCoord)
		
		FLOAT angleToPoint=ATAN2(COS(camRot.x)*SIN(fPitchToPed)-SIN(camRot.x)*COS(fPitchToPed)*COS((fHeadingToPed*-1)-camRot.z),SIN((fHeadingToPed*-1)-camRot.z)*COS(fPitchToPed))
		
		FLOAT sX = 0.5 - (cos(angleToPoint)*0.3)
		FLOAT sY = 0.5 - (sin(angleToPoint)*0.3)
		
		FLOAT stx = 0.5 - (cos(angleToPoint)*0.25)
		FLOAT sty = 0.5 - (sin(angleToPoint)*0.25) - 0.02
			
		DRAW_SPRITE("helicopterhud", "hudArrow", sX,sY , 0.02, 0.04, angleToPoint-90, iR, iG, iB, 255)						
		NET_AHD_DRAW_RANGE_TEXT(1.0, camCoord, vDestCoord, stx,sty, iR, iG, iB)
	//ENDIF	
ENDPROC

//Get the scale of the AHD based on the targets distance
FUNC FLOAT NET_GET_AHD_SCALE(PED_INDEX TargetPed)
	IF NOT IS_PED_INJURED(TargetPed)	
		FLOAT fPedRange = GET_DISTANCE_BETWEEN_COORDS(GET_GAMEPLAY_CAM_COORD(), GET_ENTITY_COORDS(TargetPed))
		FLOAT fScaler = 50 / (GET_GAMEPLAY_CAM_FOV() * fPedRange)
		
		IF fScaler < 0.25
			fScaler = 0.25
		ENDIF
		IF fScaler > 0.60
			fScaler = 0.60
		ENDIF
		
		RETURN fScaler
	ENDIF
	
	RETURN 0.0
ENDFUNC	

ENUM SKILLSPECIALTY
	SKILLSPECIALTY_DRIVING = 0,
	SKILLSPECIALTY_DRIVEBY,
	SKILLSPECIALTY_BADCOP,
	SKILLSPECIALTY_RELIABILITY,
	SKILLSPECIALTY_SHOOTING,
	SKILLSPECIALTY_HACKING
ENDENUM

//#IF IS_DEBUG_BUILD
//FLOAT OverheadOffsets[20]
//FLOAT OverheadOffsetsText[20]

//
//PROC CREATE_OVERHEAD_WIDGETS(OVERHEADSTRUCT& OverHeadDetails)
////
//	INT I
//	TEXT_LABEL_63 strWidget
//	IF NOT DOES_WIDGET_GROUP_EXIST(OverheadWidgets)
//	OverheadWidgets = START_WIDGET_GROUP("Over Head Placement Widgets")
////	
////		WidgetsAreOn = TRUE
////	
//		START_WIDGET_GROUP("General Placements")
//			
////			ADD_WIDGET_FLOAT_SLIDER("XShiftBox", XShiftBox, -2.0, 2.0, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("YBoxStartDetails", YBoxStartDetails, -2.0, 2.0, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("YStartDetails", OverHeadDetails.YStartDetails, -2.0, 2.0, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("YAboveHead", YAboveHead, -2.0, 4.0, 0.001)			
////
////			ADD_WIDGET_FLOAT_SLIDER("XShiftBar", XShiftBar, -2.0, 2.0, 0.001)			
////			ADD_WIDGET_FLOAT_SLIDER("XShiftStat", XShiftStat, -2.0, 2.0, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("YStatStep", OverHeadDetails.YStatStep, -2.0, 2.0, 0.001)
////			ADD_WIDGET_INT_SLIDER("Percentage", PercentageComplete, 0, 100, 1)
////			ADD_WIDGET_FLOAT_SLIDER("XMinorXOffset", XMinorXOffset, -2.0, 2.0, 0.001)
//			ADD_BIT_FIELD_WIDGET("Specialties BitSet", OverHeadDetails.SpecialtiesBitSet)
//			ADD_WIDGET_BOOL("AlphaDirection", OverHeadDetails.AlphaDirection)
//			ADD_WIDGET_INT_SLIDER("AlphaLevel", OverHeadDetails.AlphaLevel, 0, 266, 1)
//			ADD_WIDGET_INT_SLIDER("AlphaLevelLower", OverHeadDetails.AlphaLevelLower, 0, 266, 1)
//			ADD_WIDGET_INT_SLIDER("Speed", OverHeadDetails.Speed, 0, 266, 1)
////			ADD_WIDGET_FLOAT_SLIDER("ScalerScale", OverHeadDetails.ScalerScale, -2.0, 15.0, 0.001)
//		STOP_WIDGET_GROUP()
////	
//		START_WIDGET_GROUP("Sprite Placements")
//		REPEAT MAX_NUMBER_OF_SPRITES i					
//			strWidget = ""
//			strWidget += i
//			START_WIDGET_GROUP(strWidget)
//				ADD_WIDGET_FLOAT_SLIDER("x", OverHeadDetails.OverHeadSprite[i].x, -2.0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("y", OverHeadDetails.OverHeadSprite[i].y, -2.0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("w", OverHeadDetails.OverHeadSprite[i].w, -2.0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("h", OverHeadDetails.OverHeadSprite[i].h, -2.0, 2.0, 0.001)
//				ADD_WIDGET_INT_SLIDER("r", OverHeadDetails.OverHeadSprite[i].r, 0, 255, 1)
//				ADD_WIDGET_INT_SLIDER("g", OverHeadDetails.OverHeadSprite[i].g, 0, 255, 1)
//				ADD_WIDGET_INT_SLIDER("b", OverHeadDetails.OverHeadSprite[i].b, 0, 255, 1)
//				ADD_WIDGET_INT_SLIDER("a", OverHeadDetails.OverHeadSprite[i].a, 0, 255, 1)
//			STOP_WIDGET_GROUP()
//		ENDREPEAT
//		STOP_WIDGET_GROUP()
//	
//	
////		START_WIDGET_GROUP("Sprite Offsets")
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[0]", OverheadOffsets[0], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[1]", OverheadOffsets[1], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[2]", OverheadOffsets[2], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[3]", OverheadOffsets[3], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[4]", OverheadOffsets[4], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[5]", OverheadOffsets[5], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[6]", OverheadOffsets[6], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[7]", OverheadOffsets[7], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[8]", OverheadOffsets[8], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[9]", OverheadOffsets[9], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[10]", OverheadOffsets[10], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[11]", OverheadOffsets[11], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[12]", OverheadOffsets[12], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[13]", OverheadOffsets[13], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[14]", OverheadOffsets[14], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[15]", OverheadOffsets[15], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[16]", OverheadOffsets[16], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[17]", OverheadOffsets[17], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[18]", OverheadOffsets[18], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsets[19]", OverheadOffsets[19], -10, 10, 0.001)
////
////		STOP_WIDGET_GROUP()
////		START_WIDGET_GROUP("text Offsets")
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[0]", OverheadOffsetsText[0], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[1]", OverheadOffsetsText[1], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[2]", OverheadOffsetsText[2], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[3]", OverheadOffsetsText[3], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[4]", OverheadOffsetsText[4], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[5]", OverheadOffsetsText[5], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[6]", OverheadOffsetsText[6], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[7]", OverheadOffsetsText[7], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[8]", OverheadOffsetsText[8], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[9]", OverheadOffsetsText[9], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[10]", OverheadOffsetsText[10], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[11]", OverheadOffsetsText[11], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[12]", OverheadOffsetsText[12], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[13]", OverheadOffsetsText[13], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[14]", OverheadOffsetsText[14], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[15]", OverheadOffsetsText[15], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[16]", OverheadOffsetsText[16], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[17]", OverheadOffsetsText[17], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[18]", OverheadOffsetsText[18], -10, 10, 0.001)
////			ADD_WIDGET_FLOAT_SLIDER("OverheadOffsetsText[19]", OverheadOffsetsText[19], -10, 10, 0.001)
////
////		STOP_WIDGET_GROUP()
//
//
//
////		NET_PRINT("OverheadWidgets is created = ")NET_PRINT_INT(NATIVE_TO_INT(OverheadWidgets))NET_NL()
//	STOP_WIDGET_GROUP()
//	ENDIF
//ENDPROC
//
//PROC DELETE_OVERHEAD_WIDGETS()
//	
//	IF DOES_WIDGET_GROUP_EXIST(OverheadWidgets)
//		DELETE_WIDGET_GROUP(OverheadWidgets)
//	ENDIF
//
//ENDPROC


//#ENDIF



PROC RESET_SPRITE_ALPHA(SPRITE_PLACEMENT& aSprite, BOOL IsLowerAlpha)
	IF IsLowerAlpha = TRUE
		aSprite.a = 90
	ELSE
		aSprite.a = 180
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_RANGE_OF_PLAYER(PED_INDEX aPlayerPed)
	
	VECTOR PlayerPos = GET_ENTITY_COORDS(aPlayerPed)
	FLOAT fPedRange = GET_DISTANCE_BETWEEN_COORDS(GET_GAMEPLAY_CAM_COORD(), playerPos)
	IF fPedRange > 25.0	
		RETURN TRUE	//FALSE		-	Changed to TRUE for Bug 339051 - Display at all distances.		
		//(May work better to check different ranges i.e. 25m on foot, 50m in Land/Water Vehicle, 100m in Aircraft, 250m when using scope, etc)
	ENDIF

	RETURN TRUE
ENDFUNC
//
//FUNC BOOL CAN_DISPLAY_OVERHEAD_STATS(PLAYER_INDEX PlayerId #IF IS_DEBUG_BUILD, INT iParticipant = -1 #ENDIF)
//	
//	
////	playerPos.z = playerPos.z - 2.0  
//
////	IF IS_PLAYER_IN_RANGE_OF_PLAYER(aPlayerPed) = FALSE
////		RETURN FALSE
////	ENDIF
//	
////	IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(NETWORK_GET_PLAYER_INDEX_FROM_PED(aPlayerPed))
////		RETURN FALSE
////	ENDIF
//	
//	
//	
//	
////	//Fix for bug 479103
////	IF IS_MP_OVERHEAD_STATS_ACTIVE_FOR_PLAYER(PLAYER_ID()) = FALSE
////		RETURN FALSE
////	ENDIF
////	
////	IF IS_MP_OVERHEAD_STATS_ACTIVE_FOR_PLAYER(PlayerId) = FALSE
////		RETURN FALSE
////	ENDIF
//	
//	IF NOT NETWORK_IS_PLAYER_VISIBLE(PlayerId)
//		#IF IS_DEBUG_BUILD
//			IF g_bUse_Code_OverHeads_EXTRA_DEBUG
//			AND g_bUse_Code_OverHeads_DISPLAY_DEBUG
//				IF iParticipant != -1
//					PRINTLN("NETWORK_IS_PLAYER_VISIBLE(PlayerId) = FALSE for iParticipant  = [", iParticipant ,"]")
//				ENDIF 
//			ENDIF 
//		#ENDIF
//		RETURN FALSE
//	ENDIF
//	
//	IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
//		#IF IS_DEBUG_BUILD
//			IF g_bUse_Code_OverHeads_EXTRA_DEBUG
//			AND g_bUse_Code_OverHeads_DISPLAY_DEBUG
//				IF iParticipant != -1
//					PRINTLN("IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID()) = TRUE for iParticipant  = [", iParticipant ,"]")
//				ENDIF 
//			ENDIF 
//		#ENDIF
//		RETURN FALSE
//	ENDIF 
//	
//	IF IS_CINEMATIC_CAM_RENDERING()
//		#IF IS_DEBUG_BUILD
//			IF g_bUse_Code_OverHeads_EXTRA_DEBUG
//			AND g_bUse_Code_OverHeads_DISPLAY_DEBUG
//				IF iParticipant != -1
//					PRINTLN("IF IS_CINEMATIC_CAM_RENDERING() = TRUE for iParticipant  = [", iParticipant ,"]")
//				ENDIF 
//			ENDIF 
//		#ENDIF
//		RETURN FALSE
//	ENDIF
//	
//	IF IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS)
//		#IF IS_DEBUG_BUILD
//			IF g_bUse_Code_OverHeads_EXTRA_DEBUG
//			AND g_bUse_Code_OverHeads_DISPLAY_DEBUG
//				IF iParticipant != -1
//					PRINTLN("IS_SCRIPT_HUD_DISABLED(HUDPART_ALL_OVERHEADS) = TRUE for iParticipant  = [", iParticipant ,"]")
//				ENDIF 
//			ENDIF 
//		#ENDIF
//		RETURN FALSE
//	ENDIF
//	
//	IF IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, PlayerId)
//		#IF IS_DEBUG_BUILD
//			IF g_bUse_Code_OverHeads_EXTRA_DEBUG
//			AND g_bUse_Code_OverHeads_DISPLAY_DEBUG
//				IF iParticipant != -1
//					PRINTLN("IS_SCRIPT_HUD_DISABLED(HUDPART_THISPLAYER_OVERHEADS, PlayerId) = TRUE for iParticipant  = [", iParticipant ,"]")
//				ENDIF 
//			ENDIF 
//		#ENDIF
//		RETURN FALSE
//	ENDIF
//	
//	
////	IF NOT IS_SAFE_TO_DRAW_ON_SCREEN()
////		RETURN FALSE
////	ENDIF
//	
////	IF NOT IS_PED_IN_ANY_VEHICLE(aPlayerPed)
////		REQUEST_PED_VISIBILITY_TRACKING(aPlayerPed)
////		IF NOT IS_TRACKED_PED_VISIBLE(aPlayerPed)
////			RETURN FALSE
////		ENDIF
////	ELSE
////		REQUEST_PED_VEHICLE_VISIBILITY_TRACKING (aPlayerPed, TRUE)
////		IF NOT IS_TRACKED_PED_VISIBLE(aPlayerPed)
////			RETURN FALSE
////		ENDIF
////	
////	ENDIF
//	
//	RETURN TRUE
//ENDFUNC

FUNC BOOL CAN_DISPLAY_PLAYER_STATS()

	IF NOT IS_HUD_PREFERENCE_SWITCHED_ON()
		RETURN FALSE
	ENDIF
	
//	IF IS_TOPLEFT_INFOBOX_ONSCREEN()	//Swapped from a now removed command that just check the mission intro screen.
//		RETURN FALSE
//	ENDIF

RETURN TRUE
ENDFUNC

//PROC RESET_BEST_STATS_FOR_THIS_FRAME(OVERHEADSTRUCT& OverHeadDetails)
//	OverHeadDetails.BestNearbyDrivingSkill = 0
//	OverHeadDetails.BestNearbyDrivebySkill = 0
//	OverHeadDetails.BestNearbyBadCopSkill = 0
//	OverHeadDetails.BestNearbyReliabilitySkill = 0
//	OverHeadDetails.BestNearbyShootingSkill = 0
//	OverHeadDetails.BestNearbyHackingSkill = 0
//	OverHeadDetails.NumberPlayersChecked = 0
//	
//	OverHeadDetails.BestDriver = -1
//	OverHeadDetails.BestDriveBy = -1
//	OverHeadDetails.BestBadCop = -1
//	OverHeadDetails.BestReliability = -1
//	OverHeadDetails.BestShooting = -1
//	OverHeadDetails.BestHacking = -1
//	
//	CLEAR_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_DRIVING))
//	CLEAR_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_DRIVEBY))
//	CLEAR_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_BADCOP))
//	CLEAR_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_RELIABILITY))
//	CLEAR_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_SHOOTING))
//	CLEAR_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_HACKING))
//	
//ENDPROC

//PROC SET_PLAYERS_SPECIALITY(OVERHEADSTRUCT& OverHeadDetails)
//
//	INT I
//	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
//		
//		
//		PARTICIPANT_INDEX PartIndex = INT_TO_PARTICIPANTINDEX(I)
//		IF NETWORK_IS_PARTICIPANT_ACTIVE(PartIndex)
//			PLAYER_INDEX aPlayer = NETWORK_GET_PLAYER_INDEX(PartIndex)
//			IF IS_NET_PLAYER_OK(aPlayer)
//				IF IS_PLAYER_IN_RANGE_OF_PLAYER(GET_PLAYER_PED(aPlayer))
//			
//					IF GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fDrivingSkill >  OverHeadDetails.BestNearbyDrivingSkill
//						OverHeadDetails.BestNearbyDrivingSkill = GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fDrivingSkill
//						OverHeadDetails.BestDriver = I
//					ENDIF
//					IF IS_PLAYER_CRIMINAL(aPlayer)
//						IF GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fDrivebySkill >  OverHeadDetails.BestNearbyDrivebySkill
//							OverHeadDetails.BestNearbyDrivebySkill = GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fDrivebySkill
//							OverHeadDetails.BestDriveBy = I
//						ENDIF
//					ELSE
//						IF GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fBadCopSkill >  OverHeadDetails.BestNearbyBadCopSkill
//							OverHeadDetails.BestNearbyBadCopSkill = GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fBadCopSkill
//							OverHeadDetails.BestBadCop = I
//						ENDIF
//					ENDIF
//					IF GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fReliabilitySkill >  OverHeadDetails.BestNearbyReliabilitySkill
//						OverHeadDetails.BestNearbyReliabilitySkill = GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fReliabilitySkill
//						OverHeadDetails.BestReliability = I
//					ENDIF
//					IF GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fShootingSkill >  OverHeadDetails.BestNearbyShootingSkill
//						OverHeadDetails.BestNearbyShootingSkill = GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fShootingSkill
//						OverHeadDetails.BestShooting = I
//					ENDIF
//					IF GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fHackingSkill >  OverHeadDetails.BestNearbyHackingSkill
//						OverHeadDetails.BestNearbyHackingSkill = GlobalplayerBD[NATIVE_TO_INT(aPlayer)].playerBroadcastStats.fHackingSkill
//						OverHeadDetails.BestHacking = I
//					ENDIF	
//				ENDIF
//			ENDIF
//		ENDIF
//
//	ENDFOR
//
//ENDPROC

//
//
//PROC WORK_OUT_PLAYER_SPECIALTY(OVERHEADSTRUCT& OverHeadDetails, PLAYER_INDEX PlayerId)
//
//		
//		INT ThisPlayer =  NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX(PlayerId))
//	
//		
//		IF ThisPlayer = OverHeadDetails.BestDriver
//			IF NOT IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_DRIVING))
//				SET_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_DRIVING))
//			ENDIF
//		ENDIF
//		IF ThisPlayer = OverHeadDetails.BestDriveBy
//			IF NOT IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_DRIVEBY))
//				SET_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_DRIVEBY))
//			ENDIF
//		ENDIF
//		IF ThisPlayer = OverHeadDetails.BestBadCop
//			IF NOT IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_BADCOP))
//				SET_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_BADCOP))
//			ENDIF
//		ENDIF
//		
//		IF ThisPlayer = OverHeadDetails.BestReliability
//			IF NOT IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_RELIABILITY))
//				SET_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_RELIABILITY))
//			ENDIF
//		ENDIF
//		IF ThisPlayer = OverHeadDetails.BestShooting
//			IF NOT IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_SHOOTING))
//				SET_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_SHOOTING))
//			ENDIF
//		ENDIF
//		IF ThisPlayer = OverHeadDetails.BestHacking
//			IF NOT IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_HACKING))
//				SET_BIT(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_HACKING))
//			ENDIF
//		ENDIF
//
//
//
//ENDPROC



//
//FUNC BOOL ARE_ANY_SLOTS_FLASHING(OVERHEADSTRUCT& OverHeadDetails)
//
//	IF IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_DRIVING))
//		RETURN TRUE
//	ENDIF
//	IF IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_DRIVEBY))
//		RETURN TRUE
//	ENDIF
//	IF IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_BADCOP))
//		RETURN TRUE
//	ENDIF
//	IF IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_RELIABILITY))
//		RETURN TRUE
//	ENDIF
//	IF IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_SHOOTING))
//		RETURN TRUE
//	ENDIF
//	IF IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_HACKING))
//		RETURN TRUE
//	ENDIF
//
//
//RETURN FALSE
//ENDFUNC

//PROC BLINK_SPECIALITY_STATS(OVERHEADSTRUCT& OverHeadDetails,BOOL IsLowerAlpha, BOOL& AlreadyLowered, BOOL& AlreadyLoweredLower)
//
//	OverHeadDetails.speed = 12
//	IF ARE_ANY_SLOTS_FLASHING(OverHeadDetails)
//		IF OverHeadDetails.AlphaDirection = FALSE
//			
//			IF IsLowerAlpha = FALSE
//				IF OverHeadDetails.AlphaLevel > 70
//					IF AlreadyLowered = FALSE
//						OverHeadDetails.AlphaLevel -= OverHeadDetails.speed
//						AlreadyLowered = TRUE
//					ENDIF
//				ELSE
//					OverHeadDetails.AlphaDirection = TRUE
//				ENDIF
//			ELSE
//				IF AlreadyLoweredLower = FALSE 
//					OverHeadDetails.AlphaLevelLower -= OverHeadDetails.speed
//					AlreadyLoweredLower = TRUE
//				ENDIF
//			ENDIF
//		ELSE
//			IF IsLowerAlpha = FALSE
//				IF OverHeadDetails.AlphaLevel < 180
//					IF AlreadyLowered = FALSE
//						OverHeadDetails.AlphaLevel += OverHeadDetails.speed
//						AlreadyLowered = TRUE
//					ENDIF
//				ELSE
//					OverHeadDetails.AlphaDirection = FALSE
//				ENDIF
//			ELSE
//				IF AlreadyLoweredLower = FALSE
//					OverHeadDetails.AlphaLevelLower += OverHeadDetails.speed
//					AlreadyLoweredLower = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

//PROC RUN_UPDATE_ALPHA(OVERHEADSTRUCT& OverHeadDetails, SPRITE_PLACEMENT& aResultSprite,INT iSLot, BOOL& AlreadyLowered, BOOL& AlreadyLoweredLower, BOOL IsLowerAlpha = FALSE)
//
//	SWITCH iSLot
//		CASE 1
//			IF IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_DRIVING))
//				BLINK_SPECIALITY_STATS(OverHeadDetails,  IsLowerAlpha, AlreadyLowered, AlreadyLoweredLower)
//				IF IsLowerAlpha = TRUE
//					aResultSprite.a = OverHeadDetails.AlphaLevelLower
//				ELSE
//					aResultSprite.a = OverHeadDetails.AlphaLevel
//				ENDIF
//			ENDIF
//		BREAK
//		CASE 2
//			IF IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_DRIVEBY))
//			OR IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_BADCOP))
//				IF AlreadyLowered = FALSE
//					BLINK_SPECIALITY_STATS(OverHeadDetails,  IsLowerAlpha, AlreadyLowered, AlreadyLoweredLower)
//				ENDIF
//				IF IsLowerAlpha = TRUE
//					aResultSprite.a = OverHeadDetails.AlphaLevelLower
//				ELSE
//					aResultSprite.a = OverHeadDetails.AlphaLevel
//				ENDIF
//			
//			
//			ENDIF
//			
//		BREAK
//
//		CASE 3
//			IF IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_RELIABILITY))
//				IF AlreadyLowered = FALSE
//					BLINK_SPECIALITY_STATS(OverHeadDetails,  IsLowerAlpha, AlreadyLowered, AlreadyLoweredLower)
//				ENDIF
//				IF IsLowerAlpha = TRUE
//					aResultSprite.a = OverHeadDetails.AlphaLevelLower
//				ELSE
//					aResultSprite.a = OverHeadDetails.AlphaLevel
//				ENDIF
//			ENDIF
//		BREAK
//		CASE 4
//			IF IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_SHOOTING))
//				IF AlreadyLowered = FALSE
//					BLINK_SPECIALITY_STATS(OverHeadDetails, IsLowerAlpha, AlreadyLowered, AlreadyLoweredLower)
//				ENDIF
//				IF IsLowerAlpha = TRUE
//					aResultSprite.a = OverHeadDetails.AlphaLevelLower
//				ELSE
//					aResultSprite.a = OverHeadDetails.AlphaLevel
//				ENDIF
//			ENDIF
//		BREAK
//		CASE 5
//			IF IS_BIT_SET(OverHeadDetails.SpecialtiesBitSet, ENUM_TO_INT(SKILLSPECIALTY_HACKING))
//				IF AlreadyLowered = FALSE
//					BLINK_SPECIALITY_STATS(OverHeadDetails, IsLowerAlpha, AlreadyLowered, AlreadyLoweredLower)
//				ENDIF
//				IF IsLowerAlpha = TRUE
//					aResultSprite.a = OverHeadDetails.AlphaLevelLower
//				ELSE
//					aResultSprite.a = OverHeadDetails.AlphaLevel
//				ENDIF
//			ENDIF
//		BREAK
//	ENDSWITCH
//
//	
//ENDPROC



FUNC SPRITE_PLACEMENT GET_SPRITE_OFFSET(OVERHEADSTRUCT& OverHeadDetails, PED_INDEX aPlayerPed, SPRITE_PLACEMENT aSprite, INT SlotNo, OVERHEADSTATTYPE statType = OVERHEADSTATTYPE_EMPTY)
	
	SPRITE_PLACEMENT TempSprite = aSprite
	
	FLOAT fScreenX = aSprite.x
	FLOAT fScreenY = aSprite.y
	
	FLOAT SlotOffset 
	
	FLOAT YBoxStartDetails = 0.0 // -0.276
//	FLOAT YStartDetails = -0.056   // -0.332

	FLOAT Scaler = GET_SCALER_FROM_DISTANCE(OverHeadDetails, aPlayerPed)

	

	FLOAT YStatStep = 0.018
	FLOAT YStartDetails = -0.056   // -0.332
	FLOAT XShiftBar = 0.006
	FLOAT XShiftStat = -0.016

	FLOAT OldYStatStep = YStatStep
	FLOAT OldYStartDetails = YStartDetails
	FLOAT OldXShiftBar = XShiftBar
	FLOAT OldXShiftStat = XShiftStat

	IF Scaler > 1
		YStatStep /= Scaler
		YStartDetails /= Scaler   // -0.332
		XShiftBar /= Scaler
		XShiftStat /= Scaler
	ENDIF

	SWITCH SlotNo
		CASE 0 
			SlotOffset = YBoxStartDetails //Box
		BREAK
		CASE 1
			SlotOffset = YStartDetails + (YStatStep*SlotNo) //Bar
		BREAK
		CASE 2
			SlotOffset = YStartDetails + (YStatStep*SlotNo)
		BREAK
		CASE 3
			SlotOffset = YStartDetails + (YStatStep*SlotNo)
		BREAK
		CASE 4
			SlotOffset = YStartDetails + (YStatStep*SlotNo)
		BREAK
		CASE 5
			SlotOffset = YStartDetails + (YStatStep*SlotNo)
		BREAK
		default 	
			SlotOffset = YStartDetails + (YStatStep*SlotNo)
		BREAK
	ENDSWITCH
	
	fScreenY += SlotOffset
	
	
	SWITCH statType
		CASE OVERHEADSTATTYPE_BAR
			fScreenX += XShiftBar
			TempSprite.w = OverHeadDetails.OverHeadSprite[2].w
			TempSprite.h = OverHeadDetails.OverHeadSprite[2].h
		BREAK
		CASE OVERHEADSTATTYPE_ICON
			fScreenX += XShiftStat
			TempSprite.w = OverHeadDetails.OverHeadSprite[3].w
			TempSprite.h = OverHeadDetails.OverHeadSprite[3].h
		BREAK
		
	ENDSWITCH

	TempSprite.x = fScreenX
	TempSprite.y = fScreenY
	
	YStatStep = OldYStatStep 
	YStartDetails = OldYStartDetails 
	XShiftBar = OldXShiftBar 
	XShiftStat = OldXShiftStat 
	
RETURN TempSprite
ENDFUNC

FUNC TEXT_PLACEMENT GET_F10_TEXT_OFFSET(OVERHEADSTRUCT& OverHeadDetails,PED_INDEX aPlayerPed, SPRITE_PLACEMENT aSprite, INT SlotNo, OVERHEADSTATTYPE statType = OVERHEADSTATTYPE_EMPTY)
	
	TEXT_PLACEMENT TempText 
	TempText.x = aSprite.x
	TempText.y = aSprite.y
	
	FLOAT fScreenX = TempText.x
	FLOAT fScreenY = TempText.y
	
	FLOAT Scaler = GET_SCALER_FROM_DISTANCE(OverHeadDetails, aPlayerPed)
	
	FLOAT SlotOffset 
	
	FLOAT YBoxStartDetails = 0.0 // -0.276
//	FLOAT YStartDetails = -0.056   // -0.332

	FLOAT YStatStep = 0.018
	FLOAT YStartDetails = -0.056   // -0.332
	FLOAT XShiftPercent = 0.02

	FLOAT OldYStatStep = YStatStep
	FLOAT OldYStartDetails = YStartDetails
	FLOAT OldXShiftPercent = XShiftPercent


	SWITCH SlotNo
		CASE 0 
			SlotOffset = YBoxStartDetails //Box
		BREAK
		CASE 1
			SlotOffset = YStartDetails + (YStatStep*SlotNo) //Bar
		BREAK
		CASE 2
			SlotOffset = YStartDetails + (YStatStep*SlotNo)
		BREAK
		CASE 3
			SlotOffset = YStartDetails + (YStatStep*SlotNo)
		BREAK
		CASE 4
			SlotOffset = YStartDetails + (YStatStep*SlotNo)
		BREAK
		CASE 5
			SlotOffset = YStartDetails + (YStatStep*SlotNo)
		BREAK
		default 	
			SlotOffset = YStartDetails + (YStatStep*SlotNo)
		BREAK
	ENDSWITCH
	
	fScreenY += SlotOffset
	
	SWITCH statType
		CASE OVERHEADSTATTYPE_PERCENT
			fScreenX += XShiftPercent/Scaler
			fScreenY -= 0.01
		BREAK
		
	ENDSWITCH

	TempText.x = fScreenX
	TempText.y = fScreenY/Scaler
	
	YStatStep = OldYStatStep 
	YStartDetails = OldYStartDetails 
	XShiftPercent = OldXShiftPercent
	
RETURN TempText
ENDFUNC



//Chest STats
//PROC DISPLAY_OVERHEAD_STATS_HUD(PLAYER_INDEX PlayerId, OVERHEADSTRUCT& OverHeadDetails, OVERHEAD_OFFSETS OffsetsPosition = OO_NONE)
//
//	#IF IS_DEBUG_BUILD
//	TEXT_STYLE TS_OverheadLabels
//	TS_OverheadLabels.aFont = FONT_STANDARD
//	TS_OverheadLabels.XScale = 0.202 
//	TS_OverheadLabels.YScale = 0.269/GET_SCALER_FROM_DISTANCE(OverHeadDetails,GET_PLAYER_PED(PlayerId))
//	TS_OverheadLabels.r = 255
//	TS_OverheadLabels.g = 255
//	TS_OverheadLabels.b = 255
//	TS_OverheadLabels.a = 255
//	TS_OverheadLabels.drop = DROPSTYLE_ALL
//	#ENDIF
//	
//	INT iTeam = GET_PLAYER_TEAM(PlayerId)
//
//	BOOL AlreadyLowered 
//	BOOL AlreadyLoweredLower
//	PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
//	IF CAN_DISPLAY_OVERHEAD_STATS(PlayerId)
//
//		REQUEST_STREAMED_TEXTURE_DICT("MPOverview")
//		REQUEST_STREAMED_TEXTURE_DICT("MPOverhead")
//		
//		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPOverview") 
//			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPOverhead") 
//
//				IF SET_ALL_SPRITES_OVERHEAD(OverHeadDetails, PlayerPedId, OffsetsPosition)	//returns false if offscreen		
//					
//					SPRITE_PLACEMENT sTempPlacement
//					
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails, PlayerPedId, OverHeadDetails.OverHeadSprite[1])
//					DRAW_2D_SPRITE("MPOverhead", "BoxForAbovePlayerHead", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)	
//
//					//Driving Skill Bar
//					OverHeadDetails.OverHeadSprite[2] = GET_SPRITE_OFFSET(OverHeadDetails,PlayerPedId, OverHeadDetails.OverHeadSprite[1], 1, OVERHEADSTATTYPE_BAR) //Bars
//					OverHeadDetails.OverHeadSprite[3] = GET_SPRITE_OFFSET(OverHeadDetails, PlayerPedId,OverHeadDetails.OverHeadSprite[1], 1, OVERHEADSTATTYPE_ICON) //Stat icon
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[2], 1, AlreadyLowered, AlreadyLoweredLower, FALSE)
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[3], 1, AlreadyLowered, AlreadyLoweredLower, FALSE)
//
//					SET_SPRITE_TO_DARK_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[2], iTeam, FALSE)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[2])
//					DRAW_2D_SPRITE("MPOverhead", "GaugeAbovePlayerHeadMP", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)
//					
//					GET_SPRITE_LEVEL_BAR(GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails, PlayerPedId,OverHeadDetails.OverHeadSprite[2]), (GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBroadcastStats.fDrivingSkill), OverHeadDetails.OverHeadSprite[0])//, GET_SPRITE_LOWEST_X_SIZE(OverHeadSprite[2]))
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[0], 1, AlreadyLowered, AlreadyLoweredLower, TRUE)
//					SET_SPRITE_TO_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[0], iTeam)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[0], TRUE)
//					DRAW_2D_SPRITE("MPOverhead", "GaugeAbovePlayerHeadMP", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)
//					
//					SET_SPRITE_TO_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[3], iTeam)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[3])
//					DRAW_2D_SPRITE("MPOverview", "MP_CharCard_Stats_Icons1", sTempPlacement)
//					
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[2], TRUE)
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[3], FALSE)
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[0], FALSE)
//					
//					//Driveby/BadCop Shooting
//					OverHeadDetails.OverHeadSprite[2] = GET_SPRITE_OFFSET(OverHeadDetails, PlayerPedId,OverHeadDetails.OverHeadSprite[1], 2, OVERHEADSTATTYPE_BAR) //Bars
//					OverHeadDetails.OverHeadSprite[3] = GET_SPRITE_OFFSET(OverHeadDetails, PlayerPedId,OverHeadDetails.OverHeadSprite[1], 2, OVERHEADSTATTYPE_ICON) //Stat icon
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[2], 2, AlreadyLowered, AlreadyLoweredLower, FALSE)
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[3], 2, AlreadyLowered, AlreadyLoweredLower, FALSE)
//					
//					SET_SPRITE_TO_DARK_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[2], iTeam, FALSE)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[2])
//					DRAW_2D_SPRITE("MPOverhead", "GaugeAbovePlayerHeadMP", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)
//					
//					FLOAT PercentInSlot1
//					TEXT_LABEL_63 StatIcon
//					IF IS_PLAYER_COP(PlayerId)
//						PercentInSlot1 =  (GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBroadcastStats.fBadCopSkill)
//						StatIcon = "MP_CharCard_Stats_Icons7"
//					ELSE
//						PercentInSlot1 =  (GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBroadcastStats.fDrivebySkill)
//						StatIcon = "MP_CharCard_Stats_Icons2"
//					ENDIF
//					GET_SPRITE_LEVEL_BAR(GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[2]),PercentInSlot1, OverHeadDetails.OverHeadSprite[0])//, GET_SPRITE_LOWEST_X_SIZE(OverHeadSprite[2]))
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[0], 2, AlreadyLowered, AlreadyLoweredLower, TRUE)
//					SET_SPRITE_TO_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[0], iTeam)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[0], TRUE)
//					DRAW_2D_SPRITE("MPOverhead", "GaugeAbovePlayerHeadMP", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)
//					
//					SET_SPRITE_TO_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[3], iTeam)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[3])
//					DRAW_2D_SPRITE("MPOverview", StatIcon, sTempPlacement)
//
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[2], TRUE)
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[3], FALSE)
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[0], FALSE)
//
//					//Reliability
//					OverHeadDetails.OverHeadSprite[2] = GET_SPRITE_OFFSET(OverHeadDetails,PlayerPedId, OverHeadDetails.OverHeadSprite[1], 3, OVERHEADSTATTYPE_BAR) //Bars
//					OverHeadDetails.OverHeadSprite[3] = GET_SPRITE_OFFSET(OverHeadDetails,PlayerPedId, OverHeadDetails.OverHeadSprite[1], 3, OVERHEADSTATTYPE_ICON) //Stat icon
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[2], 3, AlreadyLowered, AlreadyLoweredLower,  FALSE)
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[3], 3, AlreadyLowered, AlreadyLoweredLower, FALSE)
//					
//					SET_SPRITE_TO_DARK_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[2], iTeam, FALSE)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[2])
//					DRAW_2D_SPRITE("MPOverhead", "GaugeAbovePlayerHeadMP", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)
//					
//					GET_SPRITE_LEVEL_BAR(GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[2]), (GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBroadcastStats.fReliabilitySkill), OverHeadDetails.OverHeadSprite[0])//, GET_SPRITE_LOWEST_X_SIZE(OverHeadSprite[2]))
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[0], 3, AlreadyLowered, AlreadyLoweredLower,TRUE)
//					SET_SPRITE_TO_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[0], iTeam)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[0], TRUE)
//					DRAW_2D_SPRITE("MPOverhead", "GaugeAbovePlayerHeadMP", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)
//					
//					SET_SPRITE_TO_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[3], iTeam)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[3])
//					DRAW_2D_SPRITE("MPOverview", "MP_CharCard_Stats_Icons4", sTempPlacement)//
//					
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[2], TRUE)
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[3], FALSE)
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[0], FALSE)
//					
//					//Shooting
//					OverHeadDetails.OverHeadSprite[2] = GET_SPRITE_OFFSET(OverHeadDetails,PlayerPedId, OverHeadDetails.OverHeadSprite[1], 4, OVERHEADSTATTYPE_BAR) //Bars
//					OverHeadDetails.OverHeadSprite[3] = GET_SPRITE_OFFSET(OverHeadDetails,PlayerPedId, OverHeadDetails.OverHeadSprite[1], 4, OVERHEADSTATTYPE_ICON) //Stat icon
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[2], 4, AlreadyLowered, AlreadyLoweredLower, FALSE)
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[3], 4, AlreadyLowered, AlreadyLoweredLower, FALSE)
//					
//					SET_SPRITE_TO_DARK_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[2], iTeam, FALSE)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[2])
//					DRAW_2D_SPRITE("MPOverhead", "GaugeAbovePlayerHeadMP", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)
//					
//					GET_SPRITE_LEVEL_BAR(GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[2]), (GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBroadcastStats.fShootingSkill), OverHeadDetails.OverHeadSprite[0])//, GET_SPRITE_LOWEST_X_SIZE(OverHeadSprite[2]))
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[0], 4, AlreadyLowered, AlreadyLoweredLower, TRUE)
//					SET_SPRITE_TO_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[0], iTeam)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[0], TRUE)
//					DRAW_2D_SPRITE("MPOverhead", "GaugeAbovePlayerHeadMP", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)
//					
//					SET_SPRITE_TO_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[3], iTeam)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[3])
//					DRAW_2D_SPRITE("MPOverview", "MP_CharCard_Stats_Icons3", sTempPlacement)
//					
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[2], TRUE)
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[3], FALSE)
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[0], FALSE)
//					
//					//Hacking
//					OverHeadDetails.OverHeadSprite[2] = GET_SPRITE_OFFSET(OverHeadDetails, PlayerPedId,OverHeadDetails.OverHeadSprite[1], 5, OVERHEADSTATTYPE_BAR) //Bars
//					OverHeadDetails.OverHeadSprite[3] = GET_SPRITE_OFFSET(OverHeadDetails,PlayerPedId, OverHeadDetails.OverHeadSprite[1], 5, OVERHEADSTATTYPE_ICON) //Stat icon
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[2], 5, AlreadyLowered, AlreadyLoweredLower, FALSE)
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[3], 5, AlreadyLowered, AlreadyLoweredLower, FALSE)
//					
//					SET_SPRITE_TO_DARK_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[2], iTeam, FALSE)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[2])
//					DRAW_2D_SPRITE("MPOverhead", "GaugeAbovePlayerHeadMP", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)
//					
//					GET_SPRITE_LEVEL_BAR(GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[2]), (GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBroadcastStats.fHackingSkill), OverHeadDetails.OverHeadSprite[0])//, GET_SPRITE_LOWEST_X_SIZE(OverHeadSprite[2]))
//					RUN_UPDATE_ALPHA(OverHeadDetails, OverHeadDetails.OverHeadSprite[0], 5, AlreadyLowered, AlreadyLoweredLower, TRUE)
//					SET_SPRITE_TO_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[0], iTeam)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[0], TRUE)
//					DRAW_2D_SPRITE("MPOverhead", "GaugeAbovePlayerHeadMP", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)
//					
//					SET_SPRITE_TO_TEAM_COLOUR(OverHeadDetails.OverHeadSprite[3], iTeam)
//					sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[3])
//					DRAW_2D_SPRITE("MPOverview", "MP_PlayerRoleIcons_1_Hacker", sTempPlacement)
//					
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[2], TRUE)
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[3], FALSE)
//					RESET_SPRITE_ALPHA(OverHeadDetails.OverHeadSprite[0], FALSE)
//					
//					CLEAR_DRAW_ORIGIN()
//					
//				ENDIF
//
//
//			ELSE
//				NET_NL()NET_PRINT("waiting on MPOverhead Streaming in")
//			ENDIF
//		ELSE
//			NET_NL()NET_PRINT("waiting on MPOverview Streaming in")
//		ENDIF
//		
//	ENDIF
////	iDoneSizing++
//
//ENDPROC




////Tag Icon
//PROC DISPLAY_TARGET_ON_PLAYER(PLAYER_INDEX PlayerId)
//
//	OVERHEADSTRUCT OverHeadDetails
//	OVERHEAD_OFFSETS OffsetsPosition = OO_NONE
//
//		//Rank icon
//	OverHeadDetails.OverheadSprite[0].x = 0
//	OverHeadDetails.OverheadSprite[0].y = 0
//	OverHeadDetails.OverheadSprite[0].w = 0.028
//	OverHeadDetails.OverheadSprite[0].h = 0.045
//	OverHeadDetails.OverheadSprite[0].r = 255
//	OverHeadDetails.OverheadSprite[0].g = 255
//	OverHeadDetails.OverheadSprite[0].b = 255
//	OverHeadDetails.OverheadSprite[0].a = 250
//	
//	
//
//	PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
//
//	REQUEST_STREAMED_TEXTURE_DICT("MPPlayerRole")
//	
//		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPPlayerRole") 
//
//			IF SET_ALL_SPRITES_OVERHEAD(OverHeadDetails, PlayerPedId, OffsetsPosition)	//returns false if offscreen		
//				
//				SPRITE_PLACEMENT sTempPlacement
////				
////				sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails, PlayerPedId, OverHeadDetails.OverHeadSprite[1])
////				DRAW_2D_SPRITE("MPPlayerRole", "MP_PlayerRoleIcons_6_Sniper_Biker", sTempPlacement, FALSE, HIGHLIGHT_OPTION_NORMAL)	
//
//				
//				OverHeadDetails.OverHeadSprite[1] = GET_SPRITE_NAME_OFFSET(OverHeadDetails,PlayerPedId, OverHeadDetails.OverHeadSprite[0], 0, OVERHEADSTATTYPE_WANTED)
//				sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[1])
//				DRAW_2D_SPRITE("MPPlayerRole", "MP_PlayerRoleIcons_6_Sniper_Biker", sTempPlacement, TRUE, HIGHLIGHT_OPTION_NORMAL)
//
//				
//				
//				CLEAR_DRAW_ORIGIN()
//				
//			ENDIF
//
//
//		ELSE
//			NET_NL()NET_PRINT("waiting on MPOverhead Streaming in")
//		ENDIF
//	
//
//ENDPROC





PROC PUSH_ALL_DOWN(OVERHEADSTRUCT& OverHeadDetails, FLOAT Amount)
	
	INT I
	FOR I = 0 TO MAX_NUM_OF_RECT-1
		OverHeadDetails.OverHeadRect[I].y += Amount
	ENDFOR
	FOR I = 0 TO MAX_NUMBER_OF_SPRITES-1
		OverHeadDetails.OverHeadSprite[I].y += Amount
	ENDFOR
	FOR I = 0 TO MAX_NUM_OF_TEXT-1
		OverHeadDetails.OverHeadText[I].y += Amount
	ENDFOR

ENDPROC



ENUM RECT_STAT_CRIM_BOX_PLACEMENT
	CRIM_RECT_BIG_BOX_BODY = 0,
	CRIM_RECT_BIG_BOX_TITLE,
	
	CRIM_RECT_SNITCH_LINE
ENDENUM

ENUM SPRITE_STAT_CRIM_BOX_PLACEMENT
	CRIM_SPRITE_GENERAL_UNDERBAR = 0,
	CRIM_SPRITE_PROPERTIES_SPRITE_BAR,
	CRIM_SPRITE_PROPERTIES_ICON,
	CRIM_SPRITE_GANG_CASH_ICON,
	CRIM_SPRITE_GANG_1_HATE_ICON,
	CRIM_SPRITE_GANG_1_HATE_BAR,
	CRIM_SPRITE_GANG_2_HATE_ICON,
	CRIM_SPRITE_GANG_2_HATE_BAR,
	CRIM_SPRITE_DRIVING_ICON,
	CRIM_SPRITE_DRIVING_BAR,
	CRIM_SPRITE_DRIVEBY_ICON,
	CRIM_SPRITE_DRIVEBY_BAR,
	CRIM_SPRITE_RELIABILITY_ICON,
	CRIM_SPRITE_RELIABILITY_BAR,
	CRIM_SPRITE_SHOOTING_ICON,
	CRIM_SPRITE_SHOOTING_BAR,
	CRIM_SPRITE_HACKING_ICON,
	CRIM_SPRITE_HACKING_BAR,
	CRIM_SPRITE_SNITCH_1_ICON,
	CRIM_SPRITE_SNITCH_1_BAR,
	CRIM_SPRITE_GLOBALXP_ICON
	
ENDENUM

ENUM TEXT_STAT_CRIM_BOX_PLACEMENT
	CRIM_TEXT_MY_STATS_TITLE = 0,
	CRIM_TEXT_DRIVING_TEXT,
	CRIM_TEXT_DRIVEBY_TEXT,
	CRIM_TEXT_RELIABILITY_TEXT,
	CRIM_TEXT_SHOOTING_TEXT,
	CRIM_TEXT_ARREST_TEXT,
	CRIM_TEXT_PROPERTIES_TEXT,
	CRIM_TEXT_SNITCH_1_LIKE,
	CRIM_TEXT_GANG_CASH_TITLE,
	CRIM_TEXT_GANG_CASH_VALUE,
	CRIM_TEXT_GANG_1_HATE_TITLE,
	CRIM_TEXT_GANG_2_HATE_TITLE,
	CRIM_TEXT_GLOBALXP_TITLE,
	CRIM_TEXT_GLOBALXP_VALUE
	
ENDENUM

ENUM STATBOX_TEXT_F10
	STATBOX_TEXT_F10_DRIVING = 0,
	STATBOX_TEXT_F10_DRIVEBY,
	STATBOX_TEXT_F10_RELIABILITY,
	STATBOX_TEXT_F10_SHOOTING,
	STATBOX_TEXT_F10_ARRESTING,
	STATBOX_TEXT_F10_PROPERTIES,
	STATBOX_TEXT_F10_SNITCH,
	STATBOX_TEXT_F10_GANG_1_HATE,
	STATBOX_TEXT_F10_GANG_2_HATE
ENDENUM

// KGM 5/3/13: This function is now obsolete, not sure if all the variables inside it are obsolete
PROC INIT_STATBOX_CRIMINAL_DETAILS(OVERHEADSTRUCT& OverHeadDetails)

	OverHeadDetails.AlphaDirection = FALSE
	OverHeadDetails.AlphaLevel = 180
	OverHeadDetails.AlphaLevelLower = 90
	

//	#IF IS_DEBUG_BUILD
//
//		IF NOT DOES_WIDGET_GROUP_EXIST(OverheadWidgets)
//			IF WidgetsAreOn = FALSE
//				CREATE_OVERHEAD_WIDGETS(OverHeadDetails)
//				WidgetsAreOn = TRUE
//			ENDIF
//		ENDIF
//	#ENDIF

	CONST_FLOAT BAR_WIDTH 0.070 
	CONST_FLOAT BAR_HEIGHT 0.016
	
	CONST_FLOAT ICON_WIDTH 0.0180
	CONST_FLOAT ICON_HEIGHT 0.0330

	//General Underbar
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GENERAL_UNDERBAR)].x = 0.0
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GENERAL_UNDERBAR)].y = 0.0
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GENERAL_UNDERBAR)].w = 0.0
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GENERAL_UNDERBAR)].h = 0.0
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GENERAL_UNDERBAR)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GENERAL_UNDERBAR)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GENERAL_UNDERBAR)].b = 0
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GENERAL_UNDERBAR)].a = UNIVERSAL_HUD_ALPHA //180
						

	//Left corner black box
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_BODY)].x = 0.152
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_BODY)].y = 0.215
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_BODY)].w = GENERIC_SLOT_WIDTH
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_BODY)].h = 0.240
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_BODY)].r = 0
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_BODY)].g = 0
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_BODY)].b = 0
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_BODY)].a = UNIVERSAL_HUD_ALPHA //204

	//Left corner Gang Coloured Title box
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_TITLE)].x = 0.152
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_TITLE)].y = 0.0750
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_TITLE)].w = GENERIC_SLOT_WIDTH
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_TITLE)].h = 0.0390
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_TITLE)].r = 230
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_TITLE)].g = 110
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_TITLE)].b = 0
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_BIG_BOX_TITLE)].a = UNIVERSAL_HUD_ALPHA //204
	
		
	//Bad Cop Red Line
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_SNITCH_LINE)].x = 0.223
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_SNITCH_LINE)].y = 0.201
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_SNITCH_LINE)].w = 0.001
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_SNITCH_LINE)].h = 0.011
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_SNITCH_LINE)].r = 255
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_SNITCH_LINE)].g = 0
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_SNITCH_LINE)].b = 0
	OverHeadDetails.OverHeadRect[ENUM_TO_INT(CRIM_RECT_SNITCH_LINE)].a = UNIVERSAL_HUD_ALPHA //255

	
	
	//Stat Titles
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_MY_STATS_TITLE)].x = 0.057
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_MY_STATS_TITLE)].y = 0.062
	
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_DRIVING_TEXT)].x = 0.082
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_DRIVING_TEXT)].y = 0.104
	
	
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_DRIVEBY_TEXT)].x = 0.082
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_DRIVEBY_TEXT)].y = 0.141
	//Reliability
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_RELIABILITY_TEXT)].x = 0.082
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_RELIABILITY_TEXT)].y = 0.178
	
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_SHOOTING_TEXT)].x = 0.082
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_SHOOTING_TEXT)].y = 0.215
	
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_ARREST_TEXT)].x = 0.082
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_ARREST_TEXT)].y = 0.252
	
	//Gang 1 hate text
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GANG_1_HATE_TITLE)].x = 0.174
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GANG_1_HATE_TITLE)].y = 0.104
	
	//Gang Properties text
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_PROPERTIES_TEXT)].x = 0.082
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_PROPERTIES_TEXT)].y = 0.289
	

	
	//Gang 2 Hate title
	
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GANG_2_HATE_TITLE)].x = 0.174
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GANG_2_HATE_TITLE)].y = 0.142

	//Stat values
	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_DRIVING)].x = 0.13
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_DRIVING)].y = 0.099
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_DRIVEBY)].x = 0.13
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_DRIVEBY)].y = 0.136
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_RELIABILITY)].x = 0.13
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_RELIABILITY)].y = 0.173
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_SHOOTING)].x = 0.13
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_SHOOTING)].y = 0.210
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_ARRESTING)].x = 0.13
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_ARRESTING)].y = 0.247
//		
//	//F10 Properties
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_PROPERTIES)].x = 0.130
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_PROPERTIES)].y = 0.284
//
//		
//	//Vagos Snitch Like F10
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_SNITCH)].x = 0.225
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_SNITCH)].y = 0.173
//
//	//Gang 1 Hate F10
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_GANG_1_HATE)].x = 0.225
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_GANG_1_HATE)].y = 0.099
//	
//	//Gang 2 Hate F10
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_GANG_2_HATE)].x = 0.225
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_F10_GANG_2_HATE)].y = 0.136

		
	//Gang Cash text 
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GANG_CASH_TITLE)].x = 0.174
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GANG_CASH_TITLE)].y = 0.215
	
	//Stat Gang Cash
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GANG_CASH_VALUE)].x = 0.175
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GANG_CASH_VALUE)].y = 0.228

	//Vagos Snitch Like
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_SNITCH_1_LIKE)].x = 0.174
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_SNITCH_1_LIKE)].y = 0.178

	//Global XP title
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GLOBALXP_TITLE)].x = 0.174
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GLOBALXP_TITLE)].y = 0.252
	
	//Global XP VALUE
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GLOBALXP_VALUE)].x = 0.175
	OverHeadDetails.OverHeadText[ENUM_TO_INT(CRIM_TEXT_GLOBALXP_VALUE)].y = 0.264


	//Fixed Driving Bar
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_BAR)].x = 0.116
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_BAR)].y = 0.127
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_BAR)].w = BAR_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_BAR)].h = BAR_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_BAR)].r = 200
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_BAR)].g = 80
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_BAR)].b = -30
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_BAR)].a = UNIVERSAL_HUD_ALPHA //90
	
	//Fixed Driving Icon
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_ICON)].x = 0.0670
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_ICON)].y = 0.1180
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_ICON)].w = ICON_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_ICON)].h = ICON_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_ICON)].r = 230
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_ICON)].g = 110
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_ICON)].b = 0
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVING_ICON)].a = UNIVERSAL_HUD_ALPHA //180
	
	
	//Fixed DriveBy Bar
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_BAR)].x = 0.116
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_BAR)].y = 0.164
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_BAR)].w = BAR_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_BAR)].h = BAR_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_BAR)].r = 200
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_BAR)].g = 80
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_BAR)].b = -30
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_BAR)].a = UNIVERSAL_HUD_ALPHA //90
	
	//Fixed DriveBy Icon
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_ICON)].x = 0.0670
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_ICON)].y = 0.155
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_ICON)].w = ICON_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_ICON)].h = ICON_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_ICON)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_ICON)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_ICON)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_DRIVEBY_ICON)].a = UNIVERSAL_HUD_ALPHA //180

	
	//Fixed Reliability Bar
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_BAR)].x = 0.116
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_BAR)].y = 0.201
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_BAR)].w = BAR_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_BAR)].h = BAR_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_BAR)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_BAR)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_BAR)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_BAR)].a = UNIVERSAL_HUD_ALPHA //90
	
	//Fixed Reliability Icon
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_ICON)].x = 0.0670
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_ICON)].y = 0.192
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_ICON)].w = ICON_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_ICON)].h = ICON_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_ICON)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_ICON)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_ICON)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_RELIABILITY_ICON)].a = UNIVERSAL_HUD_ALPHA //180


	//Fixed Shooting Bar
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_BAR)].x = 0.116
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_BAR)].y = 0.238
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_BAR)].w = BAR_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_BAR)].h = BAR_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_BAR)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_BAR)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_BAR)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_BAR)].a = UNIVERSAL_HUD_ALPHA //90
	
	//Fixed Shooting Icon
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_ICON)].x = 0.0670
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_ICON)].y = 0.229
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_ICON)].w = ICON_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_ICON)].h = ICON_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_ICON)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_ICON)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_ICON)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SHOOTING_ICON)].a = UNIVERSAL_HUD_ALPHA //180
	
	
	//Fixed Sniper Bar
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_BAR)].x = 0.116
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_BAR)].y = 0.275
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_BAR)].w = BAR_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_BAR)].h = BAR_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_BAR)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_BAR)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_BAR)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_BAR)].a = UNIVERSAL_HUD_ALPHA //90
	
	//Fixed ARrest Icon
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_ICON)].x = 0.0670
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_ICON)].y = 0.266
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_ICON)].w = ICON_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_ICON)].h = ICON_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_ICON)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_ICON)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_ICON)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_HACKING_ICON)].a = UNIVERSAL_HUD_ALPHA //180
	

	
	//Fixed Property Bar
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_SPRITE_BAR)].x = 0.116
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_SPRITE_BAR)].y = 0.312
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_SPRITE_BAR)].w = BAR_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_SPRITE_BAR)].h = BAR_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_SPRITE_BAR)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_SPRITE_BAR)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_SPRITE_BAR)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_SPRITE_BAR)].a = UNIVERSAL_HUD_ALPHA //90
	
	
	//Fixed Property Icon
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_ICON)].x = 0.067
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_ICON)].y = 0.303
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_ICON)].w = ICON_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_ICON)].h = ICON_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_ICON)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_ICON)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_ICON)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_PROPERTIES_ICON)].a = UNIVERSAL_HUD_ALPHA //180

	
	//Fixed GAng Cash Icon
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_CASH_ICON)].x = 0.163
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_CASH_ICON)].y = 0.229
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_CASH_ICON)].w = ICON_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_CASH_ICON)].h = ICON_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_CASH_ICON)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_CASH_ICON)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_CASH_ICON)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_CASH_ICON)].a = UNIVERSAL_HUD_ALPHA //180
	
	//Fixed Vagos Snitch Icon
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_ICON)].x = 0.163
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_ICON)].y = 0.192
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_ICON)].w = ICON_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_ICON)].h = ICON_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_ICON)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_ICON)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_ICON)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_ICON)].a = UNIVERSAL_HUD_ALPHA //180
	
	//Fixed Vagos Snitch Bar
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_BAR)].x = 0.211
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_BAR)].y = 0.201
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_BAR)].w = BAR_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_BAR)].h = BAR_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_BAR)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_BAR)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_BAR)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_SNITCH_1_BAR)].a = UNIVERSAL_HUD_ALPHA //90
		
	
	//Global XP Sprite
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GLOBALXP_ICON)].x = 0.163
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GLOBALXP_ICON)].y = 0.266
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GLOBALXP_ICON)].w = ICON_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GLOBALXP_ICON)].h = ICON_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GLOBALXP_ICON)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GLOBALXP_ICON)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GLOBALXP_ICON)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GLOBALXP_ICON)].a = UNIVERSAL_HUD_ALPHA //180
	

	//Fixed GANG HATE 1 Bar
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_BAR)].x = 0.211
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_BAR)].y = 0.127
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_BAR)].w = BAR_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_BAR)].h = BAR_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_BAR)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_BAR)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_BAR)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_BAR)].a = UNIVERSAL_HUD_ALPHA //90
	
	//Fixed GANG HATE 1 Icon
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_ICON)].x = 0.163
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_ICON)].y = 0.118
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_ICON)].w = ICON_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_ICON)].h = ICON_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_ICON)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_ICON)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_ICON)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_1_HATE_ICON)].a = UNIVERSAL_HUD_ALPHA //180

	//Fixed GAng 2 Hate Bar
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_BAR)].x = 0.211
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_BAR)].y = 0.164
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_BAR)].w = BAR_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_BAR)].h = BAR_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_BAR)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_BAR)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_BAR)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_BAR)].a = UNIVERSAL_HUD_ALPHA //90
	
	//Fixed GAng 2 Hate Icon
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_ICON)].x = 0.163
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_ICON)].y = 0.155
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_ICON)].w = ICON_WIDTH
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_ICON)].h = ICON_HEIGHT
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_ICON)].r = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_ICON)].g = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_ICON)].b = 255
	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(CRIM_SPRITE_GANG_2_HATE_ICON)].a = UNIVERSAL_HUD_ALPHA //180


ENDPROC

		



ENUM RECT_STAT_COP_BOX_PLACEMENT
	COP_RECT_BIG_BOX_BODY = 0,
	COP_RECT_BIG_BOX_TITLE,
	
	COP_RECT_BAD_COP_REDLINE_70,
	COP_RECT_BAD_COP_REDLINE_30,
	COP_RECT_SNITCH_REDLINE
ENDENUM

ENUM SPRITE_STAT_COP_BOX_PLACEMENT
	COP_SPRITE_GENERAL_UNDERBAR = 0,
	COP_SPRITE_PROPERTIES_SPRITE_BAR,
	COP_SPRITE_PROPERTIES_ICON,
	COP_SPRITE_GANG_CASH_ICON,
	COP_SPRITE_GANG_HATE_ICON,
	COP_SPRITE_DRIVING_ICON,
	COP_SPRITE_DRIVING_BAR,
	COP_SPRITE_DRIVEBY_ICON,
	COP_SPRITE_DRIVEBY_BAR,
	COP_SPRITE_RELIABILITY_ICON,
	COP_SPRITE_RELIABILITY_BAR,
	COP_SPRITE_SHOOTING_ICON,
	COP_SPRITE_SHOOTING_BAR,
	COP_SPRITE_ARREST_ICON,
	COP_SPRITE_ARREST_BAR,
	COP_SPRITE_SNITCH_1_ICON,
	COP_SPRITE_SNITCH_1_BAR,
	COP_SPRITE_GLOBALXP_ICON
	
ENDENUM

ENUM TEXT_STAT_COP_BOX_PLACEMENT
	COP_TEXT_MY_STATS_TITLE = 0,
	COP_TEXT_DRIVING_TEXT,
	COP_TEXT_DRIVEBY_TEXT,
	COP_TEXT_RELIABILITY_TEXT,
	COP_TEXT_SHOOTING_TEXT,
	COP_TEXT_ARREST_TEXT,
	COP_TEXT_PROPERTIES_TEXT,
	COP_TEXT_SNITCH_LIKE,
	COP_TEXT_GANG_CASH_TITLE,
	COP_TEXT_GANG_CASH_VALUE,
	COP_TEXT_GLOBALXP_TITLE,
	COP_TEXT_GLOBALXP_VALUE,
	
	COP_TEXT_F10_DRIVING,
	COP_TEXT_F10_DRIVEBY,
	COP_TEXT_F10_RELIABILITY,
	COP_TEXT_F10_SHOOTING,
	COP_TEXT_F10_ARRESTING,
	COP_TEXT_F10_PROPERTIES,
	COP_TEXT_F10_SNITCH
ENDENUM


//PROC INIT_STATBOX_COP_DETAILS(OVERHEADSTRUCT& OverHeadDetails)
//
//	OverHeadDetails.AlphaDirection = FALSE
//	OverHeadDetails.AlphaLevel = 180
//	OverHeadDetails.AlphaLevelLower = 90
//	
//
////	#IF IS_DEBUG_BUILD
////
////		IF NOT DOES_WIDGET_GROUP_EXIST(OverheadWidgets)
////			IF WidgetsAreOn = FALSE
////				CREATE_OVERHEAD_WIDGETS(OverHeadDetails)
////				WidgetsAreOn = TRUE
////			ENDIF
////		ENDIF
////	#ENDIF
//
//	CONST_FLOAT BAR_WIDTH 0.070 
//	CONST_FLOAT BAR_HEIGHT 0.016
//	
//	CONST_FLOAT ICON_WIDTH 0.0180
//	CONST_FLOAT ICON_HEIGHT 0.0330
//
//	//General Underbar
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GENERAL_UNDERBAR)].x = 0.0
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GENERAL_UNDERBAR)].y = 0.0
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GENERAL_UNDERBAR)].w = 0.0
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GENERAL_UNDERBAR)].h = 0.0
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GENERAL_UNDERBAR)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GENERAL_UNDERBAR)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GENERAL_UNDERBAR)].b = 0
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GENERAL_UNDERBAR)].a = UNIVERSAL_HUD_ALPHA //180
//						
//
//	//Left corner black box
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_BODY)].x = 0.152
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_BODY)].y = 0.192
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_BODY)].w = GENERIC_SLOT_WIDTH
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_BODY)].h = 0.199
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_BODY)].r = 0
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_BODY)].g = 0
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_BODY)].b = 0
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_BODY)].a = UNIVERSAL_HUD_ALPHA //204
//
//	//Left corner Gang Coloured Title box
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_TITLE)].x = 0.152
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_TITLE)].y = 0.0750
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_TITLE)].w = GENERIC_SLOT_WIDTH
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_TITLE)].h = 0.0390
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_TITLE)].r = 230
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_TITLE)].g = 110
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_TITLE)].b = 0
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BIG_BOX_TITLE)].a = UNIVERSAL_HUD_ALPHA //204
//	
//		
//	//Bad Cop Red Line 70%
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_70)].x = 0.129
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_70)].y = 0.165
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_70)].w = 0.001
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_70)].h = 0.010
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_70)].r = 255
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_70)].g = 0
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_70)].b = 0
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_70)].a = UNIVERSAL_HUD_ALPHA //255
//	
//	//Bad Cop Red Line 50%
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_30)].x = 0.103
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_30)].y = 0.165
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_30)].w = 0.001
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_30)].h = 0.010
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_30)].r = 255
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_30)].g = 0
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_30)].b = 0
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_BAD_COP_REDLINE_30)].a = UNIVERSAL_HUD_ALPHA //255
//
//
//	//Snitch Red Line
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_SNITCH_REDLINE)].x = 0.223
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_SNITCH_REDLINE)].y = 0.165
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_SNITCH_REDLINE)].w = 0.002
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_SNITCH_REDLINE)].h = 0.010
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_SNITCH_REDLINE)].r = 255
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_SNITCH_REDLINE)].g = 0
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_SNITCH_REDLINE)].b = 0
//	OverHeadDetails.OverHeadRect[ENUM_TO_INT(COP_RECT_SNITCH_REDLINE)].a = UNIVERSAL_HUD_ALPHA //255
//
//
//	
//	
//	//Stat Titles
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_MY_STATS_TITLE)].x = 0.057
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_MY_STATS_TITLE)].y = 0.062
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_DRIVING_TEXT)].x = 0.082
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_DRIVING_TEXT)].y = 0.104
//	
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_DRIVEBY_TEXT)].x = 0.082
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_DRIVEBY_TEXT)].y = 0.141
//	//Reliability
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_RELIABILITY_TEXT)].x = 0.082
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_RELIABILITY_TEXT)].y = 0.178
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_SHOOTING_TEXT)].x = 0.082
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_SHOOTING_TEXT)].y = 0.215
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_ARREST_TEXT)].x = 0.082
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_ARREST_TEXT)].y = 0.252
//	
//
//	//Stat values
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_DRIVING)].x = 0.13
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_DRIVING)].y = 0.099
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_DRIVEBY)].x = 0.13
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_DRIVEBY)].y = 0.136
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_RELIABILITY)].x = 0.13
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_RELIABILITY)].y = 0.173
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_SHOOTING)].x = 0.13
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_SHOOTING)].y = 0.210
//	
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_ARRESTING)].x = 0.13
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_ARRESTING)].y = 0.247
//		
//	//F10 Properties
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_PROPERTIES)].x = 0.225
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_PROPERTIES)].y = 0.099
//		
//	//Vagos Snitch Like F10
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_SNITCH)].x = 0.225
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_F10_SNITCH)].y = 0.136
//
//		
//	//Gang Properties text
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_PROPERTIES_TEXT)].x = 0.174
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_PROPERTIES_TEXT)].y = 0.104
//	
//	//Gang Cash text 
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_GANG_CASH_TITLE)].x = 0.174
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_GANG_CASH_TITLE)].y = 0.178
//	
//	
//	//Stat Gang Cash
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_GANG_CASH_VALUE)].x = 0.175
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_GANG_CASH_VALUE)].y = 0.192
//
//
//	//Vagos Snitch Like
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_SNITCH_LIKE)].x = 0.174
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_SNITCH_LIKE)].y = 0.141
//
//	//Global XP title
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_GLOBALXP_TITLE)].x = 0.174
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_GLOBALXP_TITLE)].y = 0.215
//	
//	//Global XP VALUE
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_GLOBALXP_VALUE)].x = 0.175
//	OverHeadDetails.OverHeadText[ENUM_TO_INT(COP_TEXT_GLOBALXP_VALUE)].y = 0.227
//
//
//	//Fixed Driving Bar
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_BAR)].x = 0.116
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_BAR)].y = 0.127
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_BAR)].w = BAR_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_BAR)].h = BAR_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_BAR)].r = 200
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_BAR)].g = 80
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_BAR)].b = -30
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_BAR)].a = UNIVERSAL_HUD_ALPHA //90
//	
//	//Fixed Driving Icon
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_ICON)].x = 0.0670
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_ICON)].y = 0.1180
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_ICON)].w = ICON_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_ICON)].h = ICON_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_ICON)].r = 230
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_ICON)].g = 110
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_ICON)].b = 0
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVING_ICON)].a = UNIVERSAL_HUD_ALPHA //180
//	
//	
//	//Fixed DriveBy Bar
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_BAR)].x = 0.116
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_BAR)].y = 0.164
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_BAR)].w = BAR_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_BAR)].h = BAR_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_BAR)].r = 200
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_BAR)].g = 80
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_BAR)].b = -30
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_BAR)].a = UNIVERSAL_HUD_ALPHA //90
//	
//	//Fixed DriveBy Icon
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_ICON)].x = 0.0670
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_ICON)].y = 0.155
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_ICON)].w = ICON_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_ICON)].h = ICON_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_ICON)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_ICON)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_ICON)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_DRIVEBY_ICON)].a = UNIVERSAL_HUD_ALPHA //180
//
//	
//	//Fixed Reliability Bar
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_BAR)].x = 0.116
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_BAR)].y = 0.201
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_BAR)].w = BAR_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_BAR)].h = BAR_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_BAR)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_BAR)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_BAR)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_BAR)].a = UNIVERSAL_HUD_ALPHA //90
//	
//	//Fixed Reliability Icon
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_ICON)].x = 0.0670
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_ICON)].y = 0.192
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_ICON)].w = ICON_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_ICON)].h = ICON_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_ICON)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_ICON)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_ICON)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_RELIABILITY_ICON)].a = UNIVERSAL_HUD_ALPHA //180
//
//
//	//Fixed Shooting Bar
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_BAR)].x = 0.116
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_BAR)].y = 0.238
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_BAR)].w = BAR_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_BAR)].h = BAR_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_BAR)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_BAR)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_BAR)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_BAR)].a = UNIVERSAL_HUD_ALPHA //90
//	
//	//Fixed Shooting Icon
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_ICON)].x = 0.0670
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_ICON)].y = 0.229
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_ICON)].w = ICON_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_ICON)].h = ICON_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_ICON)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_ICON)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_ICON)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SHOOTING_ICON)].a = UNIVERSAL_HUD_ALPHA //180
//	
//	
//	//Fixed Sniper Bar
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_BAR)].x = 0.116
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_BAR)].y = 0.275
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_BAR)].w = BAR_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_BAR)].h = BAR_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_BAR)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_BAR)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_BAR)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_BAR)].a = UNIVERSAL_HUD_ALPHA //90
//	
//	//Fixed ARrest Icon
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_ICON)].x = 0.0670
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_ICON)].y = 0.266
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_ICON)].w = ICON_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_ICON)].h = ICON_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_ICON)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_ICON)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_ICON)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_ARREST_ICON)].a = UNIVERSAL_HUD_ALPHA //180
//	
//
//	
//	//Fixed Property Bar
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_SPRITE_BAR)].x = 0.211
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_SPRITE_BAR)].y = 0.127
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_SPRITE_BAR)].w = BAR_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_SPRITE_BAR)].h = BAR_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_SPRITE_BAR)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_SPRITE_BAR)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_SPRITE_BAR)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_SPRITE_BAR)].a = UNIVERSAL_HUD_ALPHA //90
//	
//	
//	//Fixed Property Icon
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_ICON)].x = 0.163
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_ICON)].y = 0.118
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_ICON)].w = ICON_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_ICON)].h = ICON_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_ICON)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_ICON)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_ICON)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_PROPERTIES_ICON)].a = UNIVERSAL_HUD_ALPHA //180
//
//	
//	//Fixed GAng Cash Icon
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GANG_CASH_ICON)].x = 0.163
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GANG_CASH_ICON)].y = 0.192
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GANG_CASH_ICON)].w = ICON_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GANG_CASH_ICON)].h = ICON_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GANG_CASH_ICON)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GANG_CASH_ICON)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GANG_CASH_ICON)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GANG_CASH_ICON)].a = UNIVERSAL_HUD_ALPHA //180
//	
//	//Fixed Vagos Snitch Icon
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_ICON)].x = 0.163
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_ICON)].y = 0.155
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_ICON)].w = ICON_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_ICON)].h = ICON_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_ICON)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_ICON)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_ICON)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_ICON)].a = UNIVERSAL_HUD_ALPHA //180
//	
//	//Fixed Vagos Snitch Bar
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_BAR)].x = 0.211
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_BAR)].y = 0.164
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_BAR)].w = BAR_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_BAR)].h = BAR_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_BAR)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_BAR)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_BAR)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_SNITCH_1_BAR)].a = UNIVERSAL_HUD_ALPHA //90
//		
//	
//	//Global XP Sprite
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GLOBALXP_ICON)].x = 0.163
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GLOBALXP_ICON)].y = 0.229
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GLOBALXP_ICON)].w = ICON_WIDTH
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GLOBALXP_ICON)].h = ICON_HEIGHT
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GLOBALXP_ICON)].r = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GLOBALXP_ICON)].g = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GLOBALXP_ICON)].b = 255
//	OverHeadDetails.OverHeadSprite[ENUM_TO_INT(COP_SPRITE_GLOBALXP_ICON)].a = UNIVERSAL_HUD_ALPHA //180
//	
//
//
//
//ENDPROC



				


//BOOL bInitClanPos
//FLOAT XShiftClanDebug
//FLOAT YShiftClanDebug
////FLOAT StringWidth
//FLOAT YStartDetails = 0 
//FLOAT Camera_Icon_Distance
//FLOAT YStartDebugShift, XStartDebugShift
FUNC TEXT_PLACEMENT GET_TEXT_NAME_OFFSET(OVERHEADSTRUCT& OverHeadDetails, PED_INDEX aPlayerPed, TEXT_PLACEMENT aText, INT SlotNo, OVERHEADSTATTYPE statType = OVERHEADSTATTYPE_EMPTY, BOOL isRockstarDev = FALSE)
	
	TEXT_PLACEMENT TempText = aText
	
	FLOAT fScreenX = TempText.x
	FLOAT fScreenY = TempText.y
	
	FLOAT SlotOffset 
	
	FLOAT Scaler = GET_SCALER_FROM_DISTANCE(OverHeadDetails, aPlayerPed)

	IF Scaler = 0
	ENDIF
	
	
	
	
	
	FLOAT StringWidth = GET_STRING_WIDTH_PLAYER_NAME(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(aPlayerPed)))
	
	TEXT_LABEL_63 PlayerCncName = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(aPlayerPed))
	StringWidth += GET_STRING_WIDTH_PLAYER_NAME(PlayerCncName)
//	StringWidth += XShiftClanDebug
	
//	
//	IF bInitClanPos = FALSE		
//		START_WIDGET_GROUP("CLAN MAIN SHIFT")
//			ADD_WIDGET_FLOAT_SLIDER("XShiftClanDebug", XShiftClanDebug, -10, 20, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("YShiftClanDebug", YShiftClanDebug, -10, 20, 0.001)
//			ADD_WIDGET_FLOAT_READ_ONLY("Name Width", StringWidth)
//			ADD_WIDGET_FLOAT_SLIDER("XStartDebugShift", XStartDebugShift, -5, 5, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("YStartDebugShift", YStartDebugShift, -5, 5, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("YStartDetails", YStartDetails, -10, 20, 0.001)
//			ADD_WIDGET_FLOAT_READ_ONLY("Camera_Icon_Distance", Camera_Icon_Distance)
//		STOP_WIDGET_GROUP()
//		bInitClanPos = TRUE
//	ENDIF
	
	//FLOAT NormalDistance = 4.0
	FLOAT Camera_Icon_Distance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(NETWORK_GET_PLAYER_INDEX_FROM_PED(aPlayerPed)), GET_GAMEPLAY_CAM_COORD())
	
	/////////Changed by bobby 12/03/12
	//This const int is the amount that the text should be moved down by. 
	CONST_FLOAT cfY_Movement 0.032 
	FLOAT ftexty1 = 0.0
	FLOAT fTextSpacing = 0.0
	FLOAT YStartDetails = fTextSpacing*Camera_Icon_Distance
	
//	IF Camera_Icon_Distance < NormalDistance 
//		FLOAT M = fTextMultiplier
//		FLOAT B = NormalDistance 
//		YStartDetails = ((Camera_Icon_Distance-B)/M) + fTextSpacing	
//	ELSE
		YStartDetails += ftexty1 + 0.031
//	ENDIF
	//YStartDetails += fMoveAllTemp
//	YStartDetails += YStartDebugShift
//	fScreenX += XStartDebugShift
	
	
	FLOAT YStatStep = 0.018
	FLOAT XShiftBar = 0.006
	FLOAT XShiftStat = -0.016
	FLOAT XShiftClan = StringWidth/5.750
	FLOAT YShiftClan = 0 //0.002 //-0.002 with old clan font
	IF isRockstarDev
		YShiftClan = 0.002
	ENDIF
	
//
//	XShiftClan += XShiftClanDebug
//	YShiftClan += YShiftClanDebug

	FLOAT OldYStatStep = YStatStep
	FLOAT OldYStartDetails = YStartDetails
	FLOAT OldXShiftBar = XShiftBar
	FLOAT OldXShiftStat = XShiftStat

//	IF Scaler > 1
////		YStatStep /= Scaler
//		YStartDetails /= Scaler   // -0.332
//		
//		XShiftBar /= Scaler
//		XShiftStat /= Scaler
//	ENDIF
 
	/////////Changed by bobby 12/03/12
	SlotOffset = YStartDetails + (YStatStep*SlotNo) //Changed from - to pluse so additional lines of text go down the way

	fScreenY += SlotOffset
	
	SWITCH statType
		
		CASE OVERHEADSTATTYPE_CLAN
			fScreenX += XShiftClan
			fScreenY += YShiftClan
		BREAK
		
		CASE OVERHEADSTATTYPE_ARROW
			fScreenX += -0.003
			fScreenY += -0.043
		
		BREAK
		
	ENDSWITCH



	TempText.x = fScreenX
	TempText.y = fScreenY
	
	YStatStep = OldYStatStep 
	YStartDetails = OldYStartDetails 
	XShiftBar = OldXShiftBar 
	XShiftStat = OldXShiftStat 
	
RETURN TempText
ENDFUNC




//Tag Icon
PROC DISPLAY_ARROW_ON_PLAYER(PLAYER_INDEX PlayerId, HUD_COLOURS aColour)

	OVERHEADSTRUCT OverHeadDetails
	OVERHEAD_OFFSETS OffsetsPosition = OO_NONE

		//Rank icon
	OverHeadDetails.OverheadSprite[0].x = 0
	OverHeadDetails.OverheadSprite[0].y = 0
	OverHeadDetails.OverheadSprite[0].w = 0.028
	OverHeadDetails.OverheadSprite[0].h = 0.045
	OverHeadDetails.OverheadSprite[0].r = 255
	OverHeadDetails.OverheadSprite[0].g = 255
	OverHeadDetails.OverheadSprite[0].b = 255
	OverHeadDetails.OverheadSprite[0].a = 250
	

	PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)

	REQUEST_STREAMED_TEXTURE_DICT("MPInventory")
	
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPInventory") 

		IF SET_ALL_SPRITES_AND_TEXT_OVERHEAD(OverHeadDetails, PlayerPedId, OffsetsPosition)	//returns false if offscreen		
			

			TEXT_PLACEMENT TextPlace
			TEXT_STYLE TS_PLAYERNAME
			TS_PLAYERNAME.aFont = FONT_STANDARD
			TS_PLAYERNAME.XScale = 0.202 
			TS_PLAYERNAME.YScale = 0.276
			TS_PLAYERNAME.r = 255
			TS_PLAYERNAME.g = 255
			TS_PLAYERNAME.b = 255
			TS_PLAYERNAME.a = 255
			TS_PLAYERNAME.drop = DROPSTYLE_ALL
			TS_PLAYERNAME.WrapEndX = 0.872
			
			SET_TEXT_COLOUR_WITH_HUD_COLOUR(TS_PLAYERNAME, aColour)
			
			TextPlace = GET_TEXT_NAME_OFFSET(OverHeadDetails, PlayerPedId,  OverHeadDetails.OverHeadText[0], 0, OVERHEADSTATTYPE_ARROW)
						
			DRAW_TEXT_WITH_PLAYER_NAME(TextPlace, TS_PLAYERNAME, "V")

//			SPRITE_PLACEMENT sTempPlacement	
//			OverHeadDetails.OverHeadSprite[1] = GET_SPRITE_NAME_OFFSET(OverHeadDetails,PlayerPedId, OverHeadDetails.OverHeadSprite[0], 0, OVERHEADSTATTYPE_ICON)
//			sTempPlacement = GET_SPRITE_POSITION_AND_SCALE(OverHeadDetails,PlayerPedId,OverHeadDetails.OverHeadSprite[1])
//			DRAW_2D_SPRITE("MPInventory", "MP_Arrow", sTempPlacement, TRUE, HIGHLIGHT_OPTION_NORMAL)

			
			
			CLEAR_DRAW_ORIGIN()
			
		ENDIF


	ELSE
		NET_NL()NET_PRINT("waiting on MPInventory Streaming in")
	ENDIF
	

ENDPROC


PROC DISPLAY_OHD_RALLY_ICONS(BOOL bOn)
	
	INT i
	
	IF bOn
		REPEAT NUM_NETWORK_PLAYERS i
			MPGlobalsHud.bShowRallyIcons[1][i] = TRUE
		ENDREPEAT
	ELSE
		REPEAT NUM_NETWORK_PLAYERS i
			MPGlobalsHud.bShowRallyIcons[1][i] = FALSE
		ENDREPEAT
	ENDIF
	
ENDPROC

//
//
//FUNC PLAYER_MISSION_ROLES GET_PLAYER_ROLE_HUD(PLAYER_INDEX aPlayerId)
//	IF aPlayerId <> INVALID_PLAYER_INDEX()
//		RETURN GlobalplayerBD_CNC[INT_TO_NATIVE(aPlayerId)].PlayerMissionRole.MissionRole
//	ENDIF
//	RETURN PMR_EMPTY
//ENDFUNC
//
//FUNC PLAYER_MISSION_ROLES GET_PLAYER_ROLE_HUD_CHEAP(PLAYER_INDEX aPlayerId)
//	RETURN GlobalplayerBD_CNC[INT_TO_NATIVE(aPlayerId)].PlayerMissionRole.MissionRole
//ENDFUNC
//
//
////// PURPOSE:
// ///    Tells the hud system which player is in which role
// /// PARAMS:
// ///    aPlayer - 
// ///    aRole - 
//PROC SET_PLAYER_ROLE_HUD(PLAYER_MISSION_ROLES aRole)
//	GlobalplayerBD_CNC[NATIVE_TO_INT(PLAYER_ID())].PlayerMissionRole.MissionRole = aRole
//ENDPROC
//
//PROC RESET_PLAYER_ROLE_HUD()
//	GlobalplayerBD_CNC[NATIVE_TO_INT(PLAYER_ID())].PlayerMissionRole.MissionRole = PMR_EMPTY
//ENDPROC

//BOOL InitClanRectWidget
//RECT DebugRect

FUNC RECT GET_CLAN_FIRSTSLOT_BACKGROUND(TEXT_PLACEMENT aText, BOOL isRockstarDev)

	RECT aRect
	aRect.x = aText.x
	aRect.y = aText.y
	 
	
//	IF InitClanRectWidget = FALSE
//		CREATE_A_RECT_PLACEMENT_WIDGET(DebugRect, "CLAN FIRST SLOT")
//		
//		InitClanRectWidget = TRUE
//	ENDIF
	
	aRect.x += 0.003
	aRect.y += 0.012 //0.014 with old clan font
	aRect.w += 0.018
	aRect.h += 0.017
	aRect.r = 255
	aRect.g = 255
	aRect.b = 255
	aRect.a = 255
	
	IF isRockstarDev = FALSE
		aRect.x += 0.009
		aRect.y += -0.001
		aRect.w += 0.004
	ENDIF
	
//	aRect.x += DebugRect.x
//	aRect.y += DebugRect.y
//	
//	aRect.w += DebugRect.w
//	aRect.h += DebugRect.h
//
//	aRect.r += DebugRect.r
//	aRect.g += DebugRect.g
//	aRect.b += DebugRect.b
//	aRect.a += DebugRect.a
	

	RETURN aRect
ENDFUNC

//BOOL InitClanTypeWidget
//SPRITE_PLACEMENT DebugSprite

FUNC SPRITE_PLACEMENT GET_CLAN_TYPE_POSITION(TEXT_PLACEMENT aText, BOOL isRockstarDev)

	SPRITE_PLACEMENT aSprite
	aSprite.x = aText.x
	aSprite.y = aText.y
	
//	IF InitClanTypeWidget = FALSE
//		CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugSprite, "CLAN PRIVATE/PUBLIC")
//		InitClanTypeWidget = TRUE
//	ENDIF
	
	aSprite.x += -0.008
	aSprite.y += 0.012 //0.014 with old clan font
	aSprite.w += 0.006
	aSprite.h += 0.017
	aSprite.r = 255
	aSprite.g = 255
	aSprite.b = 255
	aSprite.a = 255
	
	IF isRockstarDev = FALSE
		aSprite.x += 0.007
		aSprite.y += -0.001
	ENDIF
	
//	aSprite.x += DebugSprite.x
//	aSprite.y += DebugSprite.y
//	
//	aSprite.w += DebugSprite.w
//	aSprite.h += DebugSprite.h
//	
//	aSprite.r += DebugSprite.r
//	aSprite.g += DebugSprite.g
//	aSprite.b += DebugSprite.b
//	aSprite.a += DebugSprite.a

	RETURN aSprite
ENDFUNC

//BOOL InitClanIconWidget
//TEXT_PLACEMENT DebugText

FUNC TEXT_PLACEMENT GET_CLAN_NAMEICON_POSITION(TEXT_PLACEMENT aText)

	TEXT_PLACEMENT anIconText
	anIconText.x = aText.x
	anIconText.y = aText.y
	
//	IF InitClanIconWidget = FALSE
//		CREATE_A_TEXT_PLACEMENT_WIDGET(DebugText, "CLAN ICON")
//		InitClanIconWidget = TRUE
//	ENDIF
	
	anIconText.x += -0.007
	anIconText.y += -0.004 //0.002 with old clan font
	
	
//	anIconText.x += DebugText.x
//	anIconText.y += DebugText.y

	RETURN anIconText
ENDFUNC

//BOOL InitClanTypeEndWidget
//SPRITE_PLACEMENT DebugEndSprite

FUNC SPRITE_PLACEMENT GET_CLAN_END_POSITION(TEXT_PLACEMENT aText, BOOL isRockstarDev)

	SPRITE_PLACEMENT aSprite
	aSprite.x = aText.x
	aSprite.y = aText.y
	
//	IF InitClanTypeEndWidget = FALSE
//		CREATE_A_SPRITE_PLACEMENT_WIDGET(DebugEndSprite, "CLAN END")
//		InitClanTypeEndWidget = TRUE
//	ENDIF
	
	aSprite.x += 0.014
	aSprite.y += 0.012 //0.014 with old clan font
	aSprite.w += 0.006
	aSprite.h += 0.019
	aSprite.r = 255
	aSprite.g = 255
	aSprite.b = 255
	aSprite.a = 255
	
	IF isRockstarDev = FALSE
		aSprite.x += 0.011
		aSprite.y += -0.001
	ENDIF
	
//	aSprite.x += DebugEndSprite.x
//	aSprite.y += DebugEndSprite.y
//	
//	aSprite.w += DebugEndSprite.w
//	aSprite.h += DebugEndSprite.h
//	
//	aSprite.r += DebugEndSprite.r
//	aSprite.g += DebugEndSprite.g
//	aSprite.b += DebugEndSprite.b
//	aSprite.a += DebugEndSprite.a

	RETURN aSprite
ENDFUNC











FUNC VECTOR GET_2D_COORDINATE_FROM_WORLD_POSITION(SPRITE_PLACEMENT TheMap, VECTOR WorldPos)

//Bobby's function via email
	VECTOR vResult
	
	CONST_FLOAT MIN_X_DEBUGMAP                                                                                            -3327.8433
	CONST_FLOAT MAX_X_DEBUGMAP                                                                                           3980.9348
	CONST_FLOAT MIN_Y_DEBUGMAP                                                                                            -3466.5078
	CONST_FLOAT MAX_Y_DEBUGMAP                                                                                           7012.5264
//	CONST_FLOAT MINI_MAP_BLIP_WIDTH                                               0.0075
//	CONST_FLOAT MINI_MAP_BLIP_HEIGHT                             0.015

	FLOAT TEXTURE_CENTER_X_DEBUGMAP =              TheMap.x
	FLOAT TEXTURE_CENTER_Y_DEBUGMAP =              TheMap.y
	FLOAT TEXTURE_WIDTH_DEBUGMAP        =              TheMap.w
	FLOAT TEXTURE_HEIGHT_DEBUGMAP      =              TheMap.h

	FLOAT LEFT_SIDE_OF_MAP_DEBUGMAP = TEXTURE_CENTER_X_DEBUGMAP - (TEXTURE_WIDTH_DEBUGMAP*0.5)
	FLOAT TOTAL_MAP_WIDTH_DEBUGMAP = MAX_X_DEBUGMAP + (MIN_X_DEBUGMAP * -1)
	FLOAT TOTAL_MAP_HEIGHT_DEBUGMAP = MAX_Y_DEBUGMAP + (MIN_Y_DEBUGMAP * -1)
	FLOAT TOTAL_RATIO_X_DEBUGMAP = TEXTURE_WIDTH_DEBUGMAP/TOTAL_MAP_WIDTH_DEBUGMAP
	FLOAT TOTAL_RATIO_Y_DEBUGMAP  = TEXTURE_HEIGHT_DEBUGMAP/TOTAL_MAP_HEIGHT_DEBUGMAP
	FLOAT BOTTOM_SIDE_OF_MAP_DEBUGMAP = TEXTURE_CENTER_Y_DEBUGMAP + (TEXTURE_HEIGHT_DEBUGMAP*0.5)

	//make the start of the x coordinate 0 rather than -3327.8433
	WorldPos.x += (MIN_X_DEBUGMAP *-1)
    //return the in X coord * by the ration between the actual map and the texture
    vResult.x =  ((WorldPos.x * TOTAL_RATIO_X_DEBUGMAP) + LEFT_SIDE_OF_MAP_DEBUGMAP)

	 //make the start of the x coordinate 0 rather than -3327.8433
    WorldPos.y += (MIN_Y_DEBUGMAP *-1)
    //return the in Y coord * by the ration between the actual map and the texture
    vResult.y =  (BOTTOM_SIDE_OF_MAP_DEBUGMAP - (WorldPos.y * TOTAL_RATIO_Y_DEBUGMAP))
	
	
	RETURN vResult

ENDFUNC



//FUNC VECTOR GET_RADAR_BLIP_COORDS_OVERHEAD(BLIP_INDEX inBlipID)
//
// 	VECTOR vReturn = << 0.0 , 0.0, 0.0>>
//	VEHICLE_INDEX TempCar
//	PED_INDEX TempChar
//	OBJECT_INDEX TempObject
//	PICKUP_INDEX TempPickup
//
// 	SWITCH GET_BLIP_INFO_ID_TYPE(inBlipID)
//		CASE BLIPTYPE_VEHICLE 
//			TempCar = GET_BLIP_INFO_ID_ENTITY_INDEX(inBlipID)
//			IF IS_VEHICLE_DRIVEABLE(TempCar)
//				vReturn = GET_ENTITY_COORDS(TempCar)
//			ENDIF
//		BREAK
//		CASE BLIPTYPE_CHAR
//			TempChar = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_BLIP_INFO_ID_ENTITY_INDEX(inBlipID))
//			IF NOT IS_PED_INJURED(TempChar)
//				vReturn = GET_ENTITY_COORDS(TempChar)
//			ENDIF
//		BREAK
//		CASE BLIPTYPE_OBJECT
//			TempObject = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_BLIP_INFO_ID_ENTITY_INDEX(inBlipID))
//			IF DOES_ENTITY_EXIST(TempObject)
//				vReturn = GET_ENTITY_COORDS(TempObject)
//			ENDIF
//		BREAK
//		CASE BLIPTYPE_COORDS
//			vReturn = GET_BLIP_COORDS(inBlipID)
//		BREAK 
//		CASE BLIPTYPE_PICKUP
//			TempPickup = GET_BLIP_INFO_ID_PICKUP_INDEX(inBlipID)
//			IF DOES_PICKUP_EXIST(TempPickup)
//				vReturn = GET_PICKUP_COORDS(TempPickup)
//			ENDIF
//		BREAK
//		CASE BLIPTYPE_CONTACT
//			vReturn = GET_BLIP_COORDS(inBlipID)
//		BREAK
////		// not used
////		CASE BLIPTYPE_UNUSED
////		CASE BLIPTYPE_PICKUP
////		CASE BLIPTYPE_RADIUS
////			vReturn = << 0.0 , 0.0, 0.0>>
////		BREAK
//	ENDSWITCH
//
//		
//	RETURN(vReturn)
//
//ENDFUNC


FUNC STRING GET_MINIMAP_FILENAME_FROM_BLIP_SPRITE(BLIP_SPRITE aSprite)

	SWITCH aSprite
		CASE RADAR_TRACE_ACTIVITIES			RETURN "img_radar_activities" 		BREAK			
		CASE RADAR_TRACE_AIR_HOCKEY			RETURN "img_radar_air_hocky" 		BREAK
		CASE RADAR_TRACE_AIRPORT			RETURN "img_radar_airport" 			BREAK
		CASE RADAR_TRACE_ARMENIAN_FAMILY	RETURN "img_radar_armenian_family" 	BREAK
		CASE RADAR_TRACE_ARMS_DEALING		RETURN "img_radar_arms_dealing" 	BREAK
		CASE RADAR_TRACE_ASSASSINS_MARK		RETURN "img_radar_assassins_mark" 	BREAK
		
		CASE RADAR_TRACE_BAR				RETURN "img_radar_bar" 				BREAK
		CASE RADAR_TRACE_BARBER				RETURN "img_radar_barber" 			BREAK
		CASE RADAR_TRACE_BASE_JUMP			RETURN "img_radar_base_jump" 		BREAK
		CASE RADAR_TRACE_BASKETBALL			RETURN "img_radar_basketball" 		BREAK
		CASE RADAR_TRACE_BIOLAB_HEIST		RETURN "img_radar_biolab_heist" 	BREAK
		CASE RADAR_TRACE_BOMB_A				RETURN "img_radar_bomb_a" 			BREAK
		CASE RADAR_TRACE_BOMB_B				RETURN "img_radar_bomb_b" 			BREAK
		CASE RADAR_TRACE_BOMB_C				RETURN "img_radar_bomb_c" 			BREAK
		CASE RADAR_TRACE_BOWLING			RETURN "img_radar_bowling" 			BREAK
		CASE RADAR_TRACE_BURGER_SHOT		RETURN "img_radar_burger_shot" 		BREAK
		
		CASE RADAR_TRACE_CABERET_CLUB		RETURN "img_radar_cabaret_club" 	BREAK
		CASE RADAR_TRACE_CABLE_CAR			RETURN "img_radar_cable_car" 		BREAK
		CASE RADAR_TRACE_CAR_CARRIER		RETURN "img_radar_car_carrier" 		BREAK
		CASE RADAR_TRACE_CAR_MOD_SHOP		RETURN "img_radar_car_mod_shop" 	BREAK
		CASE RADAR_TRACE_BENNYS				RETURN "img_radar_bennys" 			BREAK
		CASE RADAR_TRACE_CAR_WASH			RETURN "img_radar_car_wash" 		BREAK
		CASE RADAR_TRACE_CELEBRITY_THEFT	RETURN "img_radar_celebrity_theft" 	BREAK
		CASE RADAR_TRACE_CHINESE_STRAND		RETURN "img_radar_chinese_strand" 	BREAK
		CASE RADAR_TRACE_CINEMA				RETURN "img_radar_cinema" 			BREAK
		CASE RADAR_TRACE_CLOTHES_STORE		RETURN "img_radar_clothes_store"	BREAK
		CASE RADAR_TRACE_CLUCKIN_BELL		RETURN "img_radar_cluckin_bell"		BREAK
		CASE RADAR_TRACE_COMEDY_CLUB		RETURN "img_radar_comedy_club"		BREAK
		CASE RADAR_TRACE_COP_PATROL			RETURN "img_radar_cop_patrol"		BREAK
		CASE RADAR_TRACE_COP_PLAYER			RETURN "img_radar_cop_player"		BREAK
		CASE RADAR_TRACE_CRIM_ARREST		RETURN "img_radar_crim_arrest"		BREAK
		CASE RADAR_TRACE_CRIM_CARSTEAL		RETURN "img_radar_crim_carsteal"	BREAK
		CASE RADAR_TRACE_CRIM_CUFF_KEYS		RETURN "img_radar_crim_cuff_keys"	BREAK
		CASE RADAR_TRACE_CRIM_DRUGS			RETURN "img_radar_crim_drugs"		BREAK
//		CASE RADAR_TRACE_CRIM_DRUGS_BLUE	RETURN "img_radar_crim_drugs_blue"	BREAK
//		CASE RADAR_TRACE_CRIM_DRUGS_GREEN	RETURN "img_radar_crim_drugs_green"	BREAK
//		CASE RADAR_TRACE_CRIM_DRUGS_RED		RETURN "img_radar_crim_drugs_red"	BREAK
//		CASE RADAR_TRACE_CRIM_DRUGS_YELLOW	RETURN "img_radar_crim_drugs_yellow"BREAK
		CASE RADAR_TRACE_CRIM_HOLDUPS		RETURN "img_radar_crim_holdups"		BREAK
		CASE RADAR_TRACE_CRIM_PIMPING		RETURN "img_radar_crim_pimping"		BREAK
		CASE RADAR_TRACE_CRIM_PLAYER		RETURN "img_radar_crim_player"		BREAK
		CASE RADAR_TRACE_CRIM_SAVED_VEHICLE	RETURN "img_radar_crim_saved_vehicle"BREAK
		CASE RADAR_TRACE_CRIM_WANTED		RETURN "img_radar_crim_wanted"		BREAK
		
		CASE RADAR_TRACE_DARTS				RETURN "img_radar_darts"			BREAK	
		CASE RADAR_TRACE_DOCKS_HEIST		RETURN "img_radar_docks_heist"		BREAK
		CASE RADAR_TRACE_DRAG_RACE			RETURN "img_radar_drag_race"		BREAK
		CASE RADAR_TRACE_DRAG_RACE_FINISH	RETURN "img_radar_drag_race_finish"	BREAK
		CASE RADAR_TRACE_DRIVE_THRU			RETURN "img_radar_drive_thru"		BREAK
		
		CASE RADAR_TRACE_ELEVATOR			RETURN "img_radar_elevator"			BREAK
		CASE RADAR_TRACE_EYE_SKY			RETURN "img_radar_eye_sky"			BREAK
		
		CASE RADAR_TRACE_FBI_HEIST			RETURN "img_radar_fbi_heist"		BREAK
		CASE RADAR_TRACE_FBI_OFFICERS_STRAND RETURN "img_radar_fbi_officers_strand"	BREAK
		CASE RADAR_TRACE_FENCE				RETURN "img_radar_fence"			BREAK
		CASE RADAR_TRACE_FINALE_BANK_HEIST	RETURN "img_radar_finale_bank_heist"BREAK
		CASE RADAR_TRACE_FINANCIER_STRAND	RETURN "img_radar_financier_strand"	BREAK
		CASE RADAR_TRACE_FIRE				RETURN "img_radar_fire"				BREAK
		CASE RADAR_TRACE_FLIGHT_SCHOOL		RETURN "img_radar_flight_school"	BREAK
		CASE RADAR_TRACE_FRANKLIN_FAMILY	RETURN "img_radar_franklin_family"	BREAK
		CASE RADAR_TRACE_FRIEND_FRANKLIN_P	RETURN "img_radar_friend_franklin_p"BREAK
		CASE RADAR_TRACE_FRIEND_FRANKLIN_X	RETURN "img_radar_friend_franklin_x"BREAK
		CASE RADAR_TRACE_FRIEND_LAMAR		RETURN "img_radar_friend_lamar"		BREAK
		CASE RADAR_TRACE_FRIEND_MICHAEL_P	RETURN "img_radar_friend_micheal_p"	BREAK
		CASE RADAR_TRACE_FRIEND_MICHAEL_X	RETURN "img_radar_friend_micheal_x"	BREAK
		CASE RADAR_TRACE_FRIEND_TREVOR_P	RETURN "img_radar_friend_trevor_p"	BREAK
		CASE RADAR_TRACE_FRIEND_TREVOR_X	RETURN "img_radar_friend_trevor_x"	BREAK
		
		CASE RADAR_TRACE_GANG_BIKERS		RETURN "img_radar_gang_bikers"		BREAK
		CASE RADAR_TRACE_GANG_COPS			RETURN "img_radar_gang_cops"		BREAK
		CASE RADAR_TRACE_GANG_COPS_PARTNER	RETURN "img_radar_gang_cops_partner"BREAK
		CASE RADAR_TRACE_GANG_FAMILIES		RETURN "img_radar_gang_families"	BREAK
		CASE RADAR_TRACE_GANG_MEXICANS		RETURN "img_radar_gang_mexicans"	BREAK
		CASE RADAR_TRACE_GANG_PROFESSIONALS	RETURN "img_radar_gang_professionals"	BREAK
		CASE RADAR_TRACE_GOLF				RETURN "img_radar_golf"				BREAK
		CASE RADAR_TRACE_GR_W_UPGRADE		RETURN "img_radar_gr_w_upgrade"		BREAK
		CASE RADAR_TRACE_GUN_SHOP			RETURN "img_radar_gun_shop"			BREAK
		CASE RADAR_TRACE_GYM				RETURN "img_radar_gym"				BREAK
		
		CASE RADAR_TRACE_HEIST				RETURN "img_radar_heist"			BREAK
		CASE RADAR_TRACE_HELICOPTER			RETURN "img_radar_helicopter"		BREAK
		CASE RADAR_TRACE_HOSPITAL			RETURN "img_radar_hospital"			BREAK
		CASE RADAR_TRACE_HUNTING			RETURN "img_radar_hunting"			BREAK
		
		CASE RADAR_TRACE_ILLEGAL_PARKING	RETURN "img_radar_illegal_parking"	BREAK
		CASE RADAR_TRACE_INTERNET_CAFE		RETURN "img_radar_internet_cafe"	BREAK
		
		CASE RADAR_TRACE_JEWELRY_HEIST		RETURN "img_radar_jewelry_heist"	BREAK
		CASE RADAR_TRACE_JIMMY				RETURN "img_radar_jimmy"			BREAK
		CASE RADAR_TRACE_JOYRIDERS			RETURN "img_radar_joyriders"		BREAK
		
		CASE RADAR_TRACE_LAMAR_FAMILY		RETURN "img_radar_lamar_family"		BREAK
		CASE RADAR_TRACE_LESTER_FAMILY		RETURN "img_radar_lester_family"	BREAK
		
		CASE RADAR_TRACE_MICHAEL_FAMILY		RETURN "img_radar_michael_family"	BREAK
		CASE RADAR_TRACE_MICHAEL_FAMILY_EXILE	RETURN "img_radar_michael_family_exile"		BREAK
		CASE RADAR_TRACE_MICHAEL_TREVOR_FAMILY	RETURN "img_radar_michael_trevor_family"	BREAK
//		CASE RADAR_TRACE_MP_NOISE			RETURN ""		BREAK
		CASE RADAR_TRACE_MUSIC_VENUE		RETURN "img_radar_music_venue"		BREAK
		
		CASE RADAR_TRACE_NICE_HOUSE_HEIST	RETURN "img_radar_nice_house_heist"	BREAK
		CASE RADAR_TRACE_NUMBERED_1			RETURN "img_radar_objective_red"		BREAK
		CASE RADAR_TRACE_NUMBERED_10		RETURN "img_radar_objective_red"		BREAK
		CASE RADAR_TRACE_NUMBERED_2			RETURN "img_radar_objective_red"		BREAK
		CASE RADAR_TRACE_NUMBERED_3			RETURN "img_radar_objective_red"		BREAK
		CASE RADAR_TRACE_NUMBERED_4			RETURN "img_radar_objective_red"		BREAK
		CASE RADAR_TRACE_NUMBERED_5			RETURN "img_radar_objective_red"		BREAK
		CASE RADAR_TRACE_NUMBERED_6			RETURN "img_radar_objective_red"		BREAK
		CASE RADAR_TRACE_NUMBERED_7			RETURN "img_radar_objective_red"		BREAK
		CASE RADAR_TRACE_NUMBERED_8			RETURN "img_radar_objective_red"		BREAK
		CASE RADAR_TRACE_NUMBERED_9			RETURN "img_radar_objective_red"		BREAK
		
		CASE RADAR_TRACE_OBJECTIVE			RETURN "img_radar_objective"		BREAK
		CASE RADAR_TRACE_OBJECTIVE_BLUE		RETURN "img_radar_objective_blue"	BREAK
		CASE RADAR_TRACE_OBJECTIVE_GREEN	RETURN "img_radar_objective_green"	BREAK
		CASE RADAR_TRACE_OBJECTIVE_RED		RETURN "img_radar_objective_red"	BREAK
		CASE RADAR_TRACE_OBJECTIVE_YELLOW	RETURN "img_radar_objective_yellow"	BREAK
		CASE RADAR_TRACE_OFF_ROAD_RACING	RETURN "img_radar_off_road_racing"	BREAK
		
		CASE RADAR_TRACE_PLANNING_LOCATIONS	RETURN "img_radar_planning_locations" BREAK
		CASE RADAR_TRACE_POLICE				RETURN "img_radar_police"			BREAK
		CASE RADAR_TRACE_POLICE_CHASE		RETURN "img_radar_police_chase"		BREAK
		CASE RADAR_TRACE_POLICE_HELI		RETURN "img_radar_police_heli"		BREAK
		CASE RADAR_TRACE_POLICE_HELI_SPIN	RETURN "img_radar_police_heli_spin"	BREAK
		CASE RADAR_TRACE_POLICE_PLANE_MOVE	RETURN "img_radar_police_plane_move"BREAK
		CASE RADAR_TRACE_POLICE_STATION		RETURN "img_radar_police_station"	BREAK
		CASE RADAR_TRACE_POLICE_STATION_BLUE RETURN "img_radar_police_station_blue"	BREAK
		CASE RADAR_TRACE_POOL				RETURN "img_radar_pool"				BREAK
		CASE RADAR_TRACE_PROPERTY_TAKEOVER	RETURN "img_radar_property_takeover"BREAK
		CASE RADAR_TRACE_PROPERTY_TAKEOVER_COPS	RETURN "img_radar_property_takeover_cops"BREAK
		CASE RADAR_TRACE_PROPERTY_TAKEOVER_VAGOS	RETURN "img_radar_property_takeover_vagos"BREAK
		CASE RADAR_TRACE_PROPERTY_TAKEOVER_BIKERS	RETURN "img_radar_property_takeover_bikers"BREAK
		
		CASE RADAR_TRACE_RACEFLAG			RETURN "img_radar_raceflag"			BREAK
		CASE RADAR_TRACE_RAMPAGE			RETURN "img_radar_rampage"			BREAK
		CASE RADAR_TRACE_RANDOM_CHARACTER	RETURN "img_radar_random_character"	BREAK
		CASE RADAR_TRACE_RANDOM_FEMALE		RETURN "img_radar_random_female"	BREAK
		CASE RADAR_TRACE_RANDOM_MALE		RETURN "img_radar_random_male"		BREAK
		CASE RADAR_TRACE_REPAIR				RETURN "img_radar_repair"			BREAK
		CASE RADAR_TRACE_REPO				RETURN "img_radar_repo"				BREAK
		CASE RADAR_TRACE_RESTAURANT			RETURN "img_radar_restaurant"		BREAK
		CASE RADAR_TRACE_RURAL_BANK_HEIST	RETURN "img_radar_rural_bank_heist"	BREAK
		
		CASE RADAR_TRACE_SAFEHOUSE			RETURN "img_radar_safehouse"		BREAK
		CASE RADAR_TRACE_SECURITY_VAN		RETURN "img_radar_security_van"		BREAK
		CASE RADAR_TRACE_SHOOTING_RANGE		RETURN "img_radar_shooting_range"	BREAK
		CASE RADAR_TRACE_SKI_LIFT			RETURN "img_radar_ski_lift"			BREAK
		CASE RADAR_TRACE_SNITCH				RETURN "img_radar_snitch"			BREAK
		CASE RADAR_TRACE_SNITCH_RED			RETURN "img_radar_snitch_red"		BREAK
		CASE RADAR_TRACE_SOLOMON_STRAND		RETURN "img_radar_solomon_strand"	BREAK
		CASE RADAR_TRACE_STATION			RETURN "img_radar_station"			BREAK
		CASE RADAR_TRACE_STRIP_CLUB			RETURN "img_radar_strip_club"		BREAK
		
		CASE RADAR_TRACE_TACO_VAN			RETURN "img_radar_taco_van"			BREAK
		CASE RADAR_TRACE_TATTOO				RETURN "img_radar_tattoo"			BREAK
		CASE RADAR_TRACE_TENNIS				RETURN "img_radar_tennis"			BREAK
		CASE RADAR_TRACE_TOW_TRUCK			RETURN "img_radar_tow_truck"		BREAK
		CASE RADAR_TRACE_TREVOR_FAMILY		RETURN "img_radar_trevor_family"	BREAK
		CASE RADAR_TRACE_TREVOR_FAMILY_EXILE RETURN "img_radar_trevor_family_exile"	BREAK
		CASE RADAR_TRACE_TRIATHLON			RETURN "img_radar_triathlon"		BREAK
		
		CASE RADAR_TRACE_VEHICLE_SPAWN		RETURN "img_radar_vehicle_spawn"	BREAK
		CASE RADAR_TRACE_VINEWOOD_TOURS		RETURN "img_radar_vinewood_tours"	BREAK
		
		CASE RADAR_TRACE_WEAPON_ASSAULT_RIFLE RETURN "img_radar_weapon_assault_rifle" BREAK
		CASE RADAR_TRACE_WEAPON_BAT			RETURN "img_radar_weapon_bat"		BREAK
		CASE RADAR_TRACE_WEAPON_GRENADE		RETURN "img_radar_weapon_grenade"	BREAK
		CASE RADAR_TRACE_WEAPON_HEALTH		RETURN "img_radar_weapon_health"	BREAK
		CASE RADAR_TRACE_WEAPON_KNIFE		RETURN "img_radar_weapon_knife"		BREAK
		CASE RADAR_TRACE_WEAPON_MOLOTOV		RETURN "img_radar_weapon_molotov"	BREAK
		CASE RADAR_TRACE_WEAPON_PISTOL		RETURN "img_radar_weapon_pistol"	BREAK
		CASE RADAR_TRACE_WEAPON_ROCKET		RETURN "img_radar_weapon_rocket"	BREAK
		CASE RADAR_TRACE_WEAPON_SHOTGUN		RETURN "img_radar_weapon_shotgun"	BREAK
		CASE RADAR_TRACE_WEAPON_SMG			RETURN "img_radar_weapon_smg"		BREAK
		CASE RADAR_TRACE_WEAPON_SNIPER		RETURN "img_radar_weapon_sniper"	BREAK
		CASE RADAR_TRACE_WEED_STASH			RETURN "img_radar_weed_stash"		BREAK
		
		CASE RADAR_TRACE_YOGA				RETURN "img_radar_yoga"					BREAK
	ENDSWITCH

	RETURN "img_radar_objective_red"

ENDFUNC


PROC GET_ALL_MISSION_BLIPS(BLIP_INDEX& AllMissionBlips[], BLIP_INDEX& WaypointBlip)
	
	INT iCount
	BLIP_SPRITE standardBlipSprite = GET_STANDARD_BLIP_ENUM_ID() 
	BLIP_INDEX tempBlip = GET_FIRST_BLIP_INFO_ID(standardBlipSprite)
	WHILE DOES_BLIP_EXIST(tempBlip)
//		IF (GET_BLIP_INFO_ID_TYPE(tempBlip) = BLIPTYPE_COORDS)
//		OR (GET_BLIP_INFO_ID_TYPE(tempBlip) = BLIPTYPE_PICKUP)
//		OR (GET_BLIP_INFO_ID_TYPE(tempBlip) = BLIPTYPE_OBJECT) 
//		OR (GET_BLIP_INFO_ID_TYPE(tempBlip) = BLIPTYPE_VEHICLE) 
			AllMissionBlips[iCount] = tempBlip
			//NET_NL()NET_PRINT("NUMBER OF MISSION BLIPS = ")NET_PRINT_INT(iCount)
			iCount++		
//		ELSE
			
//			NET_NL()NET_PRINT("GET_BLIP_INFO_ID_TYPE(tempBlip) = ")NET_PRINT_INT(ENUM_TO_INT(GET_BLIP_INFO_ID_TYPE(tempBlip)))
//		ENDIF
		tempBlip = GET_NEXT_BLIP_INFO_ID(standardBlipSprite)
	ENDWHILE
	
	BLIP_SPRITE waypointBlipSprite = GET_WAYPOINT_BLIP_ENUM_ID()
	tempBlip = GET_FIRST_BLIP_INFO_ID(waypointBlipSprite)
	IF DOES_BLIP_EXIST(tempBlip)
//		IF IS_BLIP_VISIBLE(tempBlip)
			IF (GET_BLIP_INFO_ID_TYPE(tempBlip) = BLIPTYPE_COORDS)
				WaypointBlip = tempBlip
				iCount++			
			ENDIF
//		ENDIF
		tempBlip = GET_NEXT_BLIP_INFO_ID(waypointBlipSprite)
	ENDIF

ENDPROC


PROC GET_OTHER_RADAR_BLIPS(BLIP_INDEX& OtherRadarBlips[])

	INT i
	INT iCount
	BLIP_INDEX TempBlip
	iCount = 0

	// these relate to the enum in commands_ny, we are checking for all blip sprites between these 2 values.
	INT iStartSprite = ENUM_TO_INT(RADAR_TRACE_OBJECTIVE)
	INT iEndSprite = ENUM_TO_INT(MAX_RADAR_TRACES)

   	REPEAT COUNT_OF(BLIP_SPRITE) i
		IF (i >= iStartSprite)
		AND (i <= iEndSprite)
			TempBlip = GET_FIRST_BLIP_INFO_ID(INT_TO_ENUM(BLIP_SPRITE, i))
			WHILE DOES_BLIP_EXIST(TempBlip)
				eRADAR_BLIP_TYPE BlipType	
				BlipType = GET_BLIP_INFO_ID_TYPE(TempBlip)
				IF NOT (BlipType = BLIPTYPE_CONTACT)
					OtherRadarBlips[iCount] = TempBlip
					iCount++
				ENDIF
				TempBlip = GET_NEXT_BLIP_INFO_ID(INT_TO_ENUM(BLIP_SPRITE, i))
			ENDWHILE
		ENDIF
	ENDREPEAT
//	RETURN(iCount)

ENDPROC
//
//FUNC SPRITE_PLACEMENT GET_SPRITE_OF_2D_BLIP(BLIP_INDEX aBlip, SPRITE_PLACEMENT aBaseSprite, SPRITE_PLACEMENT TheMapSprite)
//	SPRITE_PLACEMENT resultSprite = aBaseSprite
//	
//	IF DOES_BLIP_EXIST(aBlip)	
//		VECTOR MapBlipPosition = GET_2D_COORDINATE_FROM_WORLD_POSITION(TheMapSprite, GET_RADAR_BLIP_COORDS_OVERHEAD(aBlip))
//		resultSprite.x = MapBlipPosition.x
//		resultSprite.y = MapBlipPosition.y
//	ENDIF
//	
//	RETURN resultSprite
//
//ENDFUNC

FUNC SPRITE_PLACEMENT GET_SPRITE_OF_2D_VECTOR(VECTOR vVector, SPRITE_PLACEMENT aBaseSprite, SPRITE_PLACEMENT TheMapSprite)
	SPRITE_PLACEMENT resultSprite = aBaseSprite
	
	VECTOR MapBlipPosition = GET_2D_COORDINATE_FROM_WORLD_POSITION(TheMapSprite, vVector)
	resultSprite.x = MapBlipPosition.x
	resultSprite.y = MapBlipPosition.y
	
	RETURN resultSprite

ENDFUNC

ENUM BLIP2DSIZE
	BLIP2DSIZE_PLAYER = 0,
	BLIP2DSIZE_SMALL,
	BLIP2DSIZE_MEDIUM, 
	BLIP2DSIZE_BIG
ENDENUM


PROC SET_2DMAP_BLIP_DIMENSIONS(SPRITE_PLACEMENT& BlipSprite, BLIP2DSIZE aSize)
	
	SWITCH aSize
		CASE BLIP2DSIZE_PLAYER
			BlipSprite.w = 0.018
			BlipSprite.h = 0.028
		BREAK
		CASE BLIP2DSIZE_SMALL
			BlipSprite.w = 0.010
			BlipSprite.h = 0.017
		BREAK
		CASE BLIP2DSIZE_MEDIUM
			BlipSprite.w =  0.014
			BlipSprite.h = 0.021
		BREAK
		CASE BLIP2DSIZE_BIG
			BlipSprite.w = 0.018
			BlipSprite.h = 0.028
		BREAK
	ENDSWITCH

ENDPROC 

//PROC INIT_DPADDOWN_MAP(PLACEMENT_TOOLS& Placement)
//
//	//Map 
//	Placement.SpritePlacement[0].x = 0.806
//	Placement.SpritePlacement[0].y = 0.457
//	Placement.SpritePlacement[0].w = 0.280
//	Placement.SpritePlacement[0].h = 0.700
//	Placement.SpritePlacement[0].r = 255
//	Placement.SpritePlacement[0].g = 255
//	Placement.SpritePlacement[0].b = 255
//	Placement.SpritePlacement[0].a = 200
//
//	CONST_FLOAT MINI_MAP_BLIP_WIDTH       0.018 //                                        0.0075
//	CONST_FLOAT MINI_MAP_BLIP_HEIGHT      0.027 //                       0.015
//
//	//Blip
//	Placement.SpritePlacement[1].x = 0.0
//	Placement.SpritePlacement[1].y = 0.0
//	Placement.SpritePlacement[1].w = MINI_MAP_BLIP_WIDTH
//	Placement.SpritePlacement[1].h = MINI_MAP_BLIP_HEIGHT
//	Placement.SpritePlacement[1].r = 255
//	Placement.SpritePlacement[1].g = 255
//	Placement.SpritePlacement[1].b = 255
//	Placement.SpritePlacement[1].a = 200
//	
//	//Rect border
//	Placement.RectPlacement[0].x = 0.848
//	Placement.RectPlacement[0].y = 0.206
//	Placement.RectPlacement[0].w = 0.200
//	Placement.RectPlacement[0].h = 0.002
//	Placement.RectPlacement[0].r = 0
//	Placement.RectPlacement[0].g = 0
//	Placement.RectPlacement[0].b = 0
//	Placement.RectPlacement[0].a = 200
//	
//	Placement.RectPlacement[1].x = 0.848
//	Placement.RectPlacement[1].y = 0.707
//	Placement.RectPlacement[1].w = 0.200
//	Placement.RectPlacement[1].h = 0.002
//	Placement.RectPlacement[1].r = 0
//	Placement.RectPlacement[1].g = 0
//	Placement.RectPlacement[1].b = 0
//	Placement.RectPlacement[1].a = 200
//	
//	Placement.RectPlacement[2].x = 0.747
//	Placement.RectPlacement[2].y = 0.457
//	Placement.RectPlacement[2].w = 0.002
//	Placement.RectPlacement[2].h = 0.502
//	Placement.RectPlacement[2].r = 0
//	Placement.RectPlacement[2].g = 0
//	Placement.RectPlacement[2].b = 0
//	Placement.RectPlacement[2].a = 200
//	
//	Placement.RectPlacement[3].x = 0.947
//	Placement.RectPlacement[3].y = 0.457
//	Placement.RectPlacement[3].w = 0.002
//	Placement.RectPlacement[3].h = 0.502
//	Placement.RectPlacement[3].r = 0
//	Placement.RectPlacement[3].g = 0
//	Placement.RectPlacement[3].b = 0
//	Placement.RectPlacement[3].a = 200
//	
//	#IF IS_DEBUG_BUILD
//	g_iMapPositionX = Placement.SpritePlacement[0].x
//	g_iMapPositionY = Placement.SpritePlacement[0].y
//	g_MapScale = 1
//	g_BlipScale = 1
//	#ENDIF
//
//	REQUEST_STREAMED_TEXTURE_DICT("MPdebugMap")
//	REQUEST_STREAMED_TEXTURE_DICT("MPOverview")
////	#IF IS_DEBUG_BUILD
////	CREATE_ONLY_PLACEMENT_WIDGETS(Placement)
////	#ENDIF
//ENDPROC

//BOOL INIT_MAP


//
//PROC PROCESS_DPADDOWN_MAP(PLACEMENT_TOOLS& Placement, PLAYER_INDEX anotherPlayer)
//	
//		
////	IF INIT_MAP = FALSE
////		CREATE_A_SPRITE_PLACEMENT_WIDGET(Placement.SpritePlacement[0], "MAP SPRITE")
////		CREATE_A_SPRITE_PLACEMENT_WIDGET(Placement.SpritePlacement[1], "BLIP SPRITE")
////		
////		INIT_MAP = TRUE
////	ENDIF
//		
//	REQUEST_STREAMED_TEXTURE_DICT("MPdebugMap")
//	REQUEST_STREAMED_TEXTURE_DICT("minimap")
//	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPdebugMap")
//		IF HAS_STREAMED_TEXTURE_DICT_LOADED("minimap") 
//			IF anotherPlayer = PLAYER_ID()
//				DRAW_2D_SPRITE("MPdebugMap", "debugMap", Placement.SpritePlacement[0])
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	#IF IS_DEBUG_BUILD
//		Placement.SpritePlacement[0].x = g_iMapPositionX
//		Placement.SpritePlacement[0].y = g_iMapPositionY
//		
//		Placement.SpritePlacement[0].w = (0.280*g_MapScale)
//		Placement.SpritePlacement[0].h = (0.700*g_MapScale)
//		
//		Placement.SpritePlacement[1].w = (0.018*g_BlipScale)
//		Placement.SpritePlacement[1].h = (0.027*g_BlipScale)
//	#ENDIF
//	
//	//All the Objectives
//	CONST_INT MAX_NUMBER_MISSION_BLIPS 200
//	CONST_INT MAX_NUMBER_OTHER_BLIPS 200
//	BLIP_INDEX MissionBlips[MAX_NUMBER_MISSION_BLIPS]
//	BLIP_INDEX WayPointBlip 
//	BLIP_INDEX OtherBlips[MAX_NUMBER_OTHER_BLIPS]
//	INT iCounter
//	GET_ALL_MISSION_BLIPS(MissionBlips, WayPointBlip)
//	GET_OTHER_RADAR_BLIPS(OtherBlips)
//	STRING BlipName
//	HUD_COLOURS aColour
//
////	DRAW_RECTANGLE(Placement.RectPlacement[0])
////	DRAW_RECTANGLE(Placement.RectPlacement[1])
////	DRAW_RECTANGLE(Placement.RectPlacement[2])
////	DRAW_RECTANGLE(Placement.RectPlacement[3])
//
//	IF anotherPlayer = PLAYER_ID()
//		WHILE DOES_BLIP_EXIST(OtherBlips[iCounter])
//			BlipName =  GET_MINIMAP_FILENAME_FROM_BLIP_SPRITE(GET_BLIP_SPRITE(OtherBlips[iCounter]))
//			IF NOT IS_STRING_EMPTY_HUD(BlipName)
//				SET_2DMAP_BLIP_DIMENSIONS(Placement.SpritePlacement[1], BLIP2DSIZE_SMALL)
//				aColour = GET_BLIP_HUD_COLOUR(OtherBlips[iCounter])
//				SET_SPRITE_HUD_COLOUR(Placement.SpritePlacement[1], aColour)
//				Placement.SpritePlacement[1] = GET_SPRITE_OF_2D_BLIP(OtherBlips[iCounter], Placement.SpritePlacement[1], Placement.SpritePlacement[0])
//				DRAW_2D_SPRITE("minimap", BlipName, Placement.SpritePlacement[1])
//			ENDIF
//			iCounter++
//		ENDWHILE
//		iCounter = 0
//		WHILE DOES_BLIP_EXIST(MissionBlips[iCounter])	
//			BlipName =  GET_MINIMAP_FILENAME_FROM_BLIP_SPRITE(GET_BLIP_SPRITE(MissionBlips[iCounter]))
//			IF NOT IS_STRING_EMPTY_HUD(BlipName)
//				SET_2DMAP_BLIP_DIMENSIONS(Placement.SpritePlacement[1], BLIP2DSIZE_SMALL)
//				aColour = GET_BLIP_HUD_COLOUR(MissionBlips[iCounter])
//				SET_SPRITE_HUD_COLOUR(Placement.SpritePlacement[1], aColour)
//				Placement.SpritePlacement[1] = GET_SPRITE_OF_2D_BLIP(MissionBlips[iCounter], Placement.SpritePlacement[1], Placement.SpritePlacement[0])
//				DRAW_2D_SPRITE("minimap", BlipName, Placement.SpritePlacement[1])
//			ENDIF
//			iCounter++
//		ENDWHILE
//	ENDIF
//	//Hold Ups
//	IF CNC_FLOW_Get_flow_Flag_State(ciCNC_FLOW_ACTIVATE_SHOP_HOLD_UP)
//		REPEAT ENUM_TO_INT(MAX_NUM_HOLDUP_AREAS) iCounter
//			BlipName =  GET_MINIMAP_FILENAME_FROM_BLIP_SPRITE(RADAR_TRACE_CRIM_HOLDUPS)
//			IF NOT IS_STRING_EMPTY_HUD(BlipName)
//				SET_2DMAP_BLIP_DIMENSIONS(Placement.SpritePlacement[1], BLIP2DSIZE_SMALL)
//				SET_SPRITE_HUD_COLOUR(Placement.SpritePlacement[1], HUD_COLOUR_WHITE)
//				Placement.SpritePlacement[1] = GET_SPRITE_OF_2D_VECTOR(GET_HOLD_UP_LOCATION(iCounter), Placement.SpritePlacement[1], Placement.SpritePlacement[0])
//				DRAW_2D_SPRITE("minimap", BlipName, Placement.SpritePlacement[1])
//			ENDIF
//		ENDREPEAT
//	ENDIF
//
//	//All the players
//	VECTOR MapBlipPosition = GET_2D_COORDINATE_FROM_WORLD_POSITION(Placement.SpritePlacement[0], GET_PLAYER_COORDS(anotherPlayer))
//	Placement.SpritePlacement[1].x = MapBlipPosition.x
//	Placement.SpritePlacement[1].y = MapBlipPosition.y
//
//	INT PlayerTeam = GET_PLAYER_TEAM(anotherPlayer)
//
//	SET_2DMAP_BLIP_DIMENSIONS(Placement.SpritePlacement[1], BLIP2DSIZE_PLAYER)
//	SET_SPRITE_HUD_COLOUR(Placement.SpritePlacement[1], HUD_COLOUR_PURE_WHITE)
//	DRAW_2D_SPRITE("minimap", GET_TEAM_OVERHEAD_IMAGE_SPRITE(PlayerTeam), Placement.SpritePlacement[1])
//	
//
//	IF anotherPlayer = PLAYER_ID()
//		//Waypoint
//		IF DOES_BLIP_EXIST(WayPointBlip)
//			Placement.SpritePlacement[1] = GET_SPRITE_OF_2D_BLIP(WayPointBlip, Placement.SpritePlacement[1], Placement.SpritePlacement[0])
//			SET_2DMAP_BLIP_DIMENSIONS(Placement.SpritePlacement[1], BLIP2DSIZE_BIG)
//			DRAW_2D_SPRITE("minimap", "img_radar_waypoint", Placement.SpritePlacement[1])
//		ENDIF
//	ENDIF
//	
//	
//ENDPROC


//PURPOSE: Displays wanted stars above an NPC's head
PROC DISPLAY_WANTED_STARS_ABOVE_NPC_HEAD(PED_INDEX thisNPC, INT iNumStars, OVERHEAD_OFFSETS oOffset = OO_NONE, VEHICLE_INDEX npcVeh = NULL)
	TEXT_STYLE TS_ABOVENPC
	TEXT_LABEL_63 strOverheadString
	OVERHEADSTRUCT OverHeadDetails
	
	TS_ABOVENPC.aFont = FONT_STANDARD
    TS_ABOVENPC.XScale = 0.202 
    TS_ABOVENPC.YScale = (0.276)//*0.75)
    TS_ABOVENPC.r = 255
    TS_ABOVENPC.g = 255
    TS_ABOVENPC.b = 255
    TS_ABOVENPC.a = 255
    TS_ABOVENPC.drop = DROPSTYLE_ALL
    TS_ABOVENPC.WrapEndX = 0.872
	
	SWITCH iNumStars
          CASE 1
                strOverheadString = "ONE_STAR"
          BREAK
          CASE 2
                strOverheadString = "TWO_STAR"
          BREAK
          CASE 3
                strOverheadString = "THREE_STAR"
          BREAK
          CASE 4
                strOverheadString = "FOUR_STAR"
          BREAK
          CASE 5
                strOverheadString = "FIVE_STAR"
          BREAK
    ENDSWITCH
    
   	IF SET_ALL_SPRITES_AND_TEXT_OVERHEAD(OverHeadDetails, thisNPC, oOffset, npcVeh)
		DRAW_TEXT_WITH_ALIGNMENT(OverHeadDetails.OverHeadText[0], TS_ABOVENPC, strOverheadString, TRUE, FALSE)
		CLEAR_DRAW_ORIGIN()
	ENDIF
ENDPROC
