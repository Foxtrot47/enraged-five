USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"

USING "net_activity_selector_groups_common.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_activity_selector_groups_gang_attacks.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Group-Specific functionality used by Gang Attacks.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Gang Attack Clear Functions
// ===========================================================================================================

// PURPOSE:	Clear one Gang Attack History slot
//
// INPUT PARAMS:			paramHistorySlot		The Gang Attack History slot to be cleared
PROC Server_Clear_One_Gang_Attack_History(INT paramHistorySlot)

	g_structGangAttackHistoryServer		emptyGangAttackHistoryStruct
	
	GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[paramHistorySlot]						= emptyGangAttackHistoryStruct
	GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[paramHistorySlot].gahsCloudFilename		= ""
	GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[paramHistorySlot].gahsTimeout			= GET_NETWORK_TIME()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear one Active Gang Attack server slot
//
// INPUT PARAMS:			paramOnMapSlot			The Gang Attack On Map slot to be cleared
PROC Server_Clear_One_Active_Gang_Attack(INT paramActiveSlot)

	g_structActiveGangAttackServer emptyActiveGangAttackStruct
	
	GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[paramActiveSlot]						= emptyActiveGangAttackStruct
	GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[paramActiveSlot].agasCloudFilename	= ""

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Client's current Gang Attack
PROC Client_Clear_Current_Gang_Attack()

	g_structCurrentGAClient emptyCurrentGAClient
	g_sCurrentClientGA = emptyCurrentGAClient
	
	g_sCurrentClientGA.gacgacCloudFilename		= ""
	g_sCurrentClientGA.gacgacReactivateTimeout	= GET_NETWORK_TIME()

ENDPROC




// ===========================================================================================================
//      General Gang Attack Active List Functions
// ===========================================================================================================

// PURPOSE:	Get the Shared Mission Slot for an Active Gang Attack
//
// INPUT PARAMS:		paramActiveSlot					The slot on the Active Gang Attacks array
// RETURN VALUE:		INT								The Shared Mission Slot stored in the Active Gang Attack slot
FUNC INT Get_Server_Shared_Mission_Slot_For_Active_GA(INT paramActiveSlot)
	RETURN (GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[paramActiveSlot].agasSharedMissionSlot)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Cloud Filename for an Active Gang Attack
//
// INPUT PARAMS:		paramActiveSlot					The slot on the Active Gang Attacks array
// RETURN VALUE:		TEXT_LABEL_23					The Cloud Filename stored in the Active Gang Attack slot
FUNC TEXT_LABEL_23 Get_Server_Cloud_Filename_For_Active_GA(INT paramActiveSlot)
	RETURN (GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[paramActiveSlot].agasCloudFilename)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the number of currently Active Gang Attacks
//
// RETURN VALUE:		INT								The number of currently active Gang Attacks
FUNC INT Get_Server_Num_Active_GA()
	RETURN (GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasNumActive)
ENDFUNC




// ===========================================================================================================
//      General Gang Attack History List Functions
// ===========================================================================================================

// PURPOSE:	Get the Cloud Filename for a Gang Attack on the History List
//
// INPUT PARAMS:		paramHistorySlot				The slot on the Gang Attacks History List array
// RETURN VALUE:		TEXT_LABEL_23					The Cloud Filename stored in the Gang Attack History slot
FUNC TEXT_LABEL_23 Get_Server_Cloud_Filename_For_History_List_GA(INT paramHistorySlot)
	RETURN (GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[paramHistorySlot].gahsCloudFilename)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Expiry Time for a Gang Attack on the History List
//
// INPUT PARAMS:		paramHistorySlot				The slot on the Gang Attacks History List array
// RETURN VALUE:		TIME_DATATYPE					The expiry time stored in the Gang Attack History slot
FUNC TIME_DATATYPE Get_Server_Expiry_Time_For_History_List_GA(INT paramHistorySlot)
	RETURN (GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[paramHistorySlot].gahsTimeout)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Find the Server History Slot containing the oldest Gang Attack
//
// RETURN VALUE:		INT						The Server Gang Attack History slot containing the oldest Gang Attack - empty slots are ignored
FUNC INT Server_Find_Oldest_Gang_Attack_On_History_List()

	INT				tempLoop		= 0
	INT				oldestTimeSlot	= NO_ACTIVE_GANG_ATTACK_SLOT
	TIME_DATATYPE	oldestTime
	TIME_DATATYPE	thisTime
	TEXT_LABEL_23	thisFilename	= ""
	
	REPEAT MAX_GANG_ATTACK_RECENT_HISTORY tempLoop
		thisFilename = Get_Server_Cloud_Filename_For_History_List_GA(tempLoop)
		IF NOT (IS_STRING_NULL_OR_EMPTY(thisFilename))
			// ...found an active entry on the history list
			thisTime = Get_Server_Expiry_Time_For_History_List_GA(tempLoop)
			IF (oldestTimeSlot = NO_ACTIVE_GANG_ATTACK_SLOT)
				// ...this is the first slot
				oldestTimeSlot	= tempLoop
				oldestTime		= thisTime
			ELSE
				// ...check if this time is less than the previous oldest time
				IF (IS_TIME_LESS_THAN(thisTime, oldestTime))
					oldestTimeSlot	= tempLoop
					oldestTime		= thisTime
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Return the slot containing the oldest time (if there are no entries then this will return NO_ACTIVE_GANG_ATTACK_SLOT)
	RETURN (oldestTimeSlot)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store an active Gang Attack on the Server History List
//
// INPUT PARAMS:		paramActiveSlot			The array position of the Gang Attack on the active Gang Attack array
PROC Server_Store_Active_Gang_Attack_On_History_List(INT paramActiveSlot)

	// Find a free slot of the history list
	INT				tempLoop		= 0
	TEXT_LABEL_23	thisFilename	= ""
	
	REPEAT MAX_GANG_ATTACK_RECENT_HISTORY tempLoop
		thisFilename = Get_Server_Cloud_Filename_For_History_List_GA(tempLoop)
		IF (IS_STRING_NULL_OR_EMPTY(thisFilename))
			// ...found an empty history slot
			GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[tempLoop].gahsCloudFilename	= Get_Server_Cloud_Filename_For_Active_GA(paramActiveSlot)
			GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[tempLoop].gahsTimeout		= GET_TIME_OFFSET(GET_NETWORK_TIME(), GANG_ATTACK_PLAY_AGAIN_DELAY_msec)
			
			EXIT
		ENDIF
	ENDREPEAT
	
	// There is no free slot, so find the oldest entry in the history list
	INT oldestSlot = Server_Find_Oldest_Gang_Attack_On_History_List()
	
	// Clear this slot on the Gang Attack History list
	Server_Clear_One_Gang_Attack_History(oldestSlot)
	
	// Store the active details in hte oldest slot
	GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[oldestSlot].gahsCloudFilename	= Get_Server_Cloud_Filename_For_Active_GA(paramActiveSlot)
	GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[oldestSlot].gahsTimeout			= GET_TIME_OFFSET(GET_NETWORK_TIME(), GANG_ATTACK_PLAY_AGAIN_DELAY_msec)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this cloud filename is on the Gang Attack server history list
//
// INPUT PARAMS:		paramFilename			The Cloud Filename being searched
// RETURN VALUE:		BOOL					TRUE if the filename is on teh history list, otherwise FALSE
FUNC BOOL Is_This_GA_Filename_In_Server_History(TEXT_LABEL_23 paramFilename)

	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		RETURN FALSE
	ENDIF
	
	// Search for this filename
	INT				tempLoop		= 0
	TEXT_LABEL_23	thisFilename	= ""
	
	REPEAT MAX_GANG_ATTACK_RECENT_HISTORY tempLoop
		thisFilename = Get_Server_Cloud_Filename_For_History_List_GA(tempLoop)
		IF NOT (IS_STRING_NULL_OR_EMPTY(thisFilename))
			IF (ARE_STRINGS_EQUAL(thisFilename, paramFilename))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      General Gang Attack Functions
// ===========================================================================================================

// PURPOSE:		Update the client stage for a Gang Attack
//
// INPUT PARAMS:		paramStage				The Stage to change to
PROC Client_Change_Gang_Attack_Stage(g_eGangAttackClientStage paramStage)

	g_sCurrentClientGA.gacgacStage = paramStage
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
		NET_PRINT_TIME()
		NET_PRINT("Changing Gang Attack Client Stage: ")
		SWITCH (paramStage)
			CASE GA_CLIENT_STAGE_WAIT_FOR_ON_MISSION	NET_PRINT("WAIT FOR ON MISSION")				BREAK
			CASE GA_CLIENT_STAGE_ON_MISSION				NET_PRINT("ON MISSION")							BREAK
			CASE GA_CLIENT_STAGE_REACTIVATE_MISSION		NET_PRINT("REACTIVATE MISSION")					BREAK
			DEFAULT										NET_PRINT("UNKNOWN - ADD STAGE TO SWITCH")		BREAK
		ENDSWITCH
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Current Gang Attack Stage for this player
//
// RETURN VALUE:		g_eGangAttackClientStage	The Current Gang Attack Stage in slot
FUNC g_eGangAttackClientStage Client_Get_Current_Gang_Attack_Stage()
	RETURN (g_sCurrentClientGA.gacgacStage)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Gang Attack re-activation timer
// NOTES:	This introduces a delay before the player is allowed back on the Gang Attack so that the server can clean it away if required
PROC Client_Set_Gang_Attack_Reactivation_Timer()

	TIME_DATATYPE reactivationTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), GANG_ATTACK_REACTIVATION_DELAY_msec)
	g_sCurrentClientGA.gacgacReactivateTimeout = reactivationTime

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
		NET_PRINT_TIME()
		NET_PRINT("Player Can Rejoin Gang Attack After Delay Timeout: ")
		NET_PRINT(GET_TIME_AS_STRING(reactivationTime))
		NET_PRINT(" [current time: ")
		NET_PRINT(GET_TIME_AS_STRING(GET_NETWORK_TIME()))
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the reactivation time has expired
//
// RETURN VALUE:		BOOL					TRUE if the timer has expired, FALSE if not
FUNC BOOL Client_Has_Gang_Attack_Reactivation_Timer_Expired()
	TIME_DATATYPE reactivationTime = g_sCurrentClientGA.gacgacReactivateTimeout
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), reactivationTime))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If this active Gang Attack is new, store it
//
// INPUT PARAMS:		paramSharedMissionSlot			The Shared Mission Slot containing this active Gang Attack
// REFERENCE PARAMS:	refCloudFilename				The Cloud Filename
PROC Store_MP_Server_Active_Gang_Attack(INT paramSharedMissionSlot, TEXT_LABEL_23 &refCloudFilename)

	// Check if this Gang Attack is already known to exist
	INT				tempLoop		= 0
	TEXT_LABEL_23	thisFilename	= ""
	INT				numActiveGA		= Get_Server_Num_Active_GA()
	
	IF (numActiveGA > 0)
		REPEAT MAX_ACTIVE_GANG_ATTACKS tempLoop
			IF (Get_Server_Shared_Mission_Slot_For_Active_GA(tempLoop) = paramSharedMissionSlot)
				thisFilename = Get_Server_Cloud_Filename_For_Active_GA(tempLoop)
				IF (ARE_STRINGS_EQUAL(thisFilename, refCloudFilename))
					// ...already stored
					EXIT
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	// Add this to the Active Gang Attacks list
	REPEAT MAX_ACTIVE_GANG_ATTACKS tempLoop
		IF (Get_Server_Shared_Mission_Slot_For_Active_GA(tempLoop) = NO_ACTIVE_GANG_ATTACK_SLOT)
			// Found an empty Active Gang Attack slot
			GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[tempLoop].agasSharedMissionSlot	= paramSharedMissionSlot
			GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[tempLoop].agasCloudFilename		= refCloudFilename
			GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasNumActive++
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
				NET_PRINT_TIME()
				NET_PRINT("(Server) Found an active Gang Attack (Shared Missions Slot: ")
				NET_PRINT_INT(paramSharedMissionSlot)
				NET_PRINT(" - ")
				NET_PRINT(refCloudFilename)
				NET_PRINT(")")
				NET_NL()
				Debug_Output_Active_Gang_Attacks()
			#ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT

ENDPROC




// ===========================================================================================================
//      Server Initialisation, Reset, and Maintenance Routines
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//     Server Maintenance - Gang Attacks
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Gang Attack Shard Missions Checker
PROC Maintain_MP_Gang_Attack_Server_Shared_Missions_Check()

	// Maintain next scheduled Shared Mission looking for an active Gang Attack
	IF (g_sASGAControlServerMP.asgacsNextScheduledSharedCheck >= MAX_MISSIONS_SHARED_CLOUD_DATA)
		g_sASGAControlServerMP.asgacsNextScheduledSharedCheck = 0
	ENDIF
	
	// Grab the group before the global is updated
	INT	scheduledUpdateSharedMissonSlot = g_sASGAControlServerMP.asgacsNextScheduledSharedCheck
	
	// Always, update the counter for next maintenance check
	g_sASGAControlServerMP.asgacsNextScheduledSharedCheck++
	
	// Check if this Shared Missions Slot contains a Gang Attack
	IF NOT (Is_Shared_Cloud_Mission_Slot_In_Use(scheduledUpdateSharedMissonSlot))
		EXIT
	ENDIF
	
	IF NOT (Is_All_Shared_Cloud_Mission_Data_Stored(scheduledUpdateSharedMissonSlot))
		EXIT
	ENDIF
	
	MP_MISSION_ID_DATA theMissionIdData = Get_MissionID_Data_For_Shared_Cloud_Loaded_Mission(scheduledUpdateSharedMissonSlot)
	
	IF (theMissionIdData.idMission != eFM_GANG_ATTACK_CLOUD)
		EXIT
	ENDIF
	
	// Found a Shared Mission Gang Attack
	Store_MP_Server_Active_Gang_Attack(scheduledUpdateSharedMissonSlot, theMissionIdData.idCloudFilename)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Active Gang Attack Server every frame
// NOTES:	The Active Gang Attack array is NOT CONTIGUOUS - there's no shuffling because it is broadcast data - there may be gaps
//			The numActiveGA variable hold the number of active GA on the array but this doesn't mean they're all stored at the start of the array
PROC Maintain_MP_Gang_Attack_Server_Active_Gang_Attacks()

	// Check if an active Gang Attack is still active
	// NOTE: Stop processing after the first one is no longer active because there's only space for one 'just finished'
	INT tempLoop			= 0
	INT SharedMissionsSlot	= NO_ACTIVE_GANG_ATTACK_SLOT
	INT numActiveGAChecked	= 0
	INT numInitialActiveGA	= Get_Server_Num_Active_GA()
	
	REPEAT MAX_ACTIVE_GANG_ATTACKS tempLoop
		SharedMissionsSlot = Get_Server_Shared_Mission_Slot_For_Active_GA(tempLoop)
		
		IF (SharedMissionsSlot != NO_ACTIVE_GANG_ATTACK_SLOT)
			// ...found an active Gang Attack, so clean up if it doesn't exist anymore
			IF NOT (Is_Shared_Cloud_Mission_Slot_In_Use(SharedMissionsSlot))
				// Store the filename on the 'History' list
				Server_Store_Active_Gang_Attack_On_History_List(tempLoop)
				
				// Clear the Active Slot
				Server_Clear_One_Active_Gang_Attack(tempLoop)
				
				// Reduce the stored number of active Gang Attacks
				GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasNumActive--
			
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
					NET_PRINT_TIME()
					NET_PRINT(" (Server) A Gang Attack is no longer active. Active Slot: ")
					NET_PRINT_INT(tempLoop)
					NET_NL()
					Debug_Output_Active_Gang_Attacks()
					Debug_Output_Gang_Attack_Server_Recent_History()
				#ENDIF
			ENDIF
			
			// Increase the number of active GAs checked
			numActiveGAChecked++
			
			// If all active GAs have been checked then don't do any more loop processing
			IF (numActiveGAChecked >= numInitialActiveGA)
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain periodic checks to see if the oldest Gang Attack on the Server History list has timed out
PROC Maintain_MP_Gang_Attack_Server_History()

	// Has the maintenance timer expired?
	IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sASGAControlServerMP.asgacsNextHistoryCheckTimeout))
		EXIT
	ENDIF
	
	// Timer has expired, so set it up again
	g_sASGAControlServerMP.asgacsNextHistoryCheckTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), GANG_ATTACK_HISTORY_CHECK_msec)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
		NET_PRINT_TIME()
		NET_PRINT("(Server) Performing History List timeout Check. Setting next timeout to: ")
		NET_PRINT(GET_TIME_AS_STRING(g_sASGAControlServerMP.asgacsNextHistoryCheckTimeout))
		NET_NL()
	#ENDIF

	// Get the oldest slot
	INT oldestSlot = Server_Find_Oldest_Gang_Attack_On_History_List()
	
	IF (oldestSlot = NO_ACTIVE_GANG_ATTACK_SLOT)
		// ...there are no entries on the history list
		EXIT
	ENDIF
	
	// Check if the entry in the oldest slot has expired
	TIME_DATATYPE oldestTime = Get_Server_Expiry_Time_For_History_List_GA(oldestSlot)
	
	IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), oldestTime))
		// ...oldest entry hasn't expired
		EXIT
	ENDIF
	
	// Clear the oldest entry in the server history list
	Server_Clear_One_Gang_Attack_History(oldestSlot)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
		NET_PRINT_TIME()
		NET_PRINT(" (Server) Oldest History List entry has expired. History slot: ")
		NET_PRINT_INT(oldestSlot)
		NET_NL()
		Debug_Output_Gang_Attack_Server_Recent_History()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Gang Attack Server every frame
PROC Maintain_MP_Activity_Selector_Gang_Attack_Group_Server()

	Maintain_MP_Gang_Attack_Server_Active_Gang_Attacks()
	Maintain_MP_Gang_Attack_Server_Shared_Missions_Check()
	Maintain_MP_Gang_Attack_Server_History()

ENDPROC




// ===========================================================================================================
//      Client Initialisation and Maintenance Routines
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//     Client Maintenance - Gang Attacks
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain a Gang Attack waiting for the player to be on mission
PROC Maintain_GA_Client_Stage_Wait_For_On_Mission()

	// Check if the player is actively on the focus mission
	IF NOT (Is_Player_Actively_On_The_Focus_Mission())
		EXIT
	ENDIF
	
	// Player is on mission, so check if it is a Gang Attack
	IF (Get_MissionsAtCoords_Slot_MissionID(Get_MissionsAtCoords_Focus_Mission_Slot()) != eFM_GANG_ATTACK_CLOUD)
		EXIT
	ENDIF
	
	// It is a Gang Attack, so store the filename
	MP_MISSION_ID_DATA theMissionIdData		= Get_MissionsAtCoords_Slot_MissionID_Data(Get_MissionsAtCoords_Focus_Mission_Slot())
	g_sCurrentClientGA.gacgacCloudFilename	= theMissionIdData.idCloudFilename
	
	// Store the Trigger area
	VECTOR	theMin		=	<< 0.0, 0.0, 0.0 >>
	VECTOR	theMax		=	<< 0.0, 0.0, 0.0 >>
	FLOAT	theWidth	=	0.0
	
	IF (Get_MissionsAtCoords_Slot_Angled_Area(Get_MissionsAtCoords_Focus_Mission_Slot(), theMin, theMax, theWidth))
		g_sCurrentClientGA.gacgacAngledAreaMin		= theMin
		g_sCurrentClientGA.gacgacAngledAreaMax		= theMax
		g_sCurrentClientGA.gacgacAngledAreaWidth	= theWidth
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
		NET_PRINT_TIME()
		NET_PRINT("Player is actively on a Gang Attack: ")
		TEXT_LABEL_31 theMissionName = Get_Mission_Name_For_FM_Cloud_Loaded_Activity(theMissionIdData)
		NET_PRINT(theMissionName)
		NET_PRINT(" [Cloud Filename: ")
		NET_PRINT(theMissionIdData.idCloudFilename)
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("      Gang Attack Trigger Area: [MIN] ")
		NET_PRINT_VECTOR(g_sCurrentClientGA.gacgacAngledAreaMin)
		NET_PRINT("   [MAX] ")
		NET_PRINT_VECTOR(g_sCurrentClientGA.gacgacAngledAreaMax)
		NET_PRINT("   [WIDTH] ")
		NET_PRINT_FLOAT(g_sCurrentClientGA.gacgacAngledAreaWidth)
		NET_NL()
	#ENDIF
		
	// Change the client stage
	Client_Change_Gang_Attack_Stage(GA_CLIENT_STAGE_ON_MISSION)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain a Gang Attack waiting for the player to leave the mission
PROC Maintain_GA_Client_Stage_On_Mission()

	// Check if the player is still actively on the focus mission
	IF (Is_Player_Actively_On_The_Focus_Mission())
		EXIT
	ENDIF
	
	// Player is no longer on the focus mission
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
		NET_PRINT_TIME()
		NET_PRINT("Player is no longer actively on the Gang Attack: ")
		NET_PRINT(g_sCurrentClientGA.gacgacCloudFilename)
		NET_NL()
	#ENDIF
	
	// Before passing to re-activation, set a flag indicating the player is still in the area unless there was some problem getting the area
	g_sCurrentClientGA.gacgacStillInArea	= TRUE
	
	IF (ARE_VECTORS_ALMOST_EQUAL(g_sCurrentClientGA.gacgacAngledAreaMin, << 0.0, 0.0, 0.0 >>))
	AND (ARE_VECTORS_ALMOST_EQUAL(g_sCurrentClientGA.gacgacAngledAreaMax, << 0.0, 0.0, 0.0 >>))
		g_sCurrentClientGA.gacgacStillInArea = FALSE
	ENDIF
		
	// Set up the reactivation timer to allow the Gang Attack to be re-activated after a delay
	Client_Set_Gang_Attack_Reactivation_Timer()
	Client_Change_Gang_Attack_Stage(GA_CLIENT_STAGE_REACTIVATE_MISSION)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Re-activate the Gang Attack after a delay if the player leaves it - allows the server a chance to cleanup
PROC Maintain_GA_Client_Stage_Reactivate_Mission()

	// Only check timeout if the player has been out of the trigger area, to prevent a re-launch
	// NOTE: This was mainly added for a Gang Attack being cancelled by external forces while the player was still in the area
	IF (g_sCurrentClientGA.gacgacStillInArea)
		IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
			IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), g_sCurrentClientGA.gacgacAngledAreaMin, g_sCurrentClientGA.gacgacAngledAreaMax, g_sCurrentClientGA.gacgacAngledAreaWidth))
				#IF IS_DEBUG_BUILD
					IF (Client_Has_Gang_Attack_Reactivation_Timer_Expired())
						NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
						NET_PRINT_TIME()
						NET_PRINT(" Gang Attack re-activation timer has expired but Player hasn't left area of Gang Attack, so don't re-activate")
						NET_NL()
					ENDIF
				#ENDIF
				
				EXIT
			ENDIF
		ENDIF
		
		// Set player as no longer in area
		g_sCurrentClientGA.gacgacStillInArea = FALSE
	ENDIF

	// Wait for timeout
	IF NOT (Client_Has_Gang_Attack_Reactivation_Timer_Expired())
		EXIT
	ENDIF
	
	// Only re-activate if the mission is not on the server's recently played history
	IF (Is_This_GA_Filename_In_Server_History(g_sCurrentClientGA.gacgacCloudFilename))
		// ...the Gang Attack is over, so clear out the client variables and don't re-activate the Gang Attack
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
			NET_PRINT_TIME()
			NET_PRINT(" Gang Attack re-activation timer has expired but Gang Attack is now on Server's History List, so clean up")
			NET_NL()
		#ENDIF
	
		Client_Clear_Current_Gang_Attack()
		
		EXIT
	ENDIF
	
	// Reactivate the mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
		NET_PRINT_TIME()
		NET_PRINT(" Gang Attack is being re-activated after player left it: ")
		NET_PRINT(g_sCurrentClientGA.gacgacCloudFilename)
		NET_NL()
	#ENDIF

	// Re-activation function within MissionsAtCoords and change the client stage
	Set_MissionsAtCoords_Mission_With_Filename_Should_Activate(g_sCurrentClientGA.gacgacCloudFilename)
	Client_Change_Gang_Attack_Stage(GA_CLIENT_STAGE_WAIT_FOR_ON_MISSION)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check for a Gang Attack getting Started or Finished
PROC Maintain_Client_Gang_Attack()

	g_eGangAttackClientStage theStage = Client_Get_Current_Gang_Attack_Stage()
	
	SWITCH (theStage)
		CASE GA_CLIENT_STAGE_WAIT_FOR_ON_MISSION
			Maintain_GA_Client_Stage_Wait_For_On_Mission()
			BREAK
			
		CASE GA_CLIENT_STAGE_ON_MISSION
			Maintain_GA_Client_Stage_On_Mission()
			BREAK
			
		CASE GA_CLIENT_STAGE_REACTIVATE_MISSION
			Maintain_GA_Client_Stage_Reactivate_Mission()
			BREAK
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [ActSelect][GangAttack]: Unknown Gang Attack Stage (as Int): ")
				NET_PRINT_INT(ENUM_TO_INT(theStage))
				NET_NL()
				SCRIPT_ASSERT("Maintain_Client_Gang_Attack() - ERROR: UNKNOWN STAGE. See Console Log. Tell Keith.")
			#ENDIF
			BREAK
	ENDSWITCH

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Gang Attack History on the Client - check for differences with the server and act accordingly
PROC Maintain_Client_Gang_Attack_History()

	// Maintain next scheduled History Slot
	IF (g_sHistoryClientGA.gahcNextScheduledHistorySlot >= MAX_GANG_ATTACK_RECENT_HISTORY)
		g_sHistoryClientGA.gahcNextScheduledHistorySlot = 0
	ENDIF
	
	// Grab the slot before the global is updated
	INT	historySlot = g_sHistoryClientGA.gahcNextScheduledHistorySlot
	
	// Always, update the counter for next maintenance check
	g_sHistoryClientGA.gahcNextScheduledHistorySlot++
	
	// Get the client and server details
	TEXT_LABEL_23 clientFilename = g_sHistoryClientGA.gahcCloudFilename[historySlot]
	BOOL clientSlotInUse = TRUE
	IF (IS_STRING_NULL_OR_EMPTY(clientFilename))
		clientSlotInUse = FALSE
	ENDIF
	
	TEXT_LABEL_23 serverFilename = Get_Server_Cloud_Filename_For_History_List_GA(historySlot)
	BOOL serverSlotInUse = TRUE
	IF (IS_STRING_NULL_OR_EMPTY(serverFilename))
		serverSlotInUse = FALSE
	ENDIF
	
	// If both are empty then nothing more to do
	IF NOT (clientSlotInUse)
	AND NOT (serverSlotInUse)
		EXIT
	ENDIF
	
	// Check if both slots are in use
	IF (clientSlotInUse)
	AND (serverSlotInUse)
		// ...both slots are in use then nothing to do if they contain the same Gang Attack
		IF (ARE_STRINGS_EQUAL(clientFilename, serverFilename))
			EXIT
		ENDIF
		
		// ...the Server has now added a different Gang Attack in this slot
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
			NET_PRINT_TIME()
			NET_PRINT("Server has updated history slot: ")
			NET_PRINT_INT(historySlot)
			NET_PRINT(" [")
			NET_PRINT(serverFilename)
			NET_PRINT(" has replaced ")
			NET_PRINT(clientFilename)
			NET_PRINT("] - updating on Client")
			NET_NL()
		#ENDIF
		
		// The Client Gang Attack is no longer on the history list, so re-activate it with MissionsAtCoords
		Set_MissionsAtCoords_Mission_With_Filename_Should_Activate(clientFilename)
		
		// The Server Gang Attack is newly on the history list, so make it await activation with MissionAtCoords
		Set_MissionsAtCoords_Mission_With_Filename_Should_Await_Activation(serverFilename)
		
		// Update Client to match Server
		g_sHistoryClientGA.gahcCloudFilename[historySlot] = serverFilename
		
		EXIT
	ENDIF
	
	// If the Client Slot is in use but the server slot isn't then the server has removed a Gang Attack from the history
	IF (clientSlotInUse)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
			NET_PRINT_TIME()
			NET_PRINT("Server has removed Gang Attack from history slot: ")
			NET_PRINT_INT(historySlot)
			NET_PRINT(" [")
			NET_PRINT(clientFilename)
			NET_PRINT("] - updating on Client")
			NET_NL()
		#ENDIF
		
		// The Client Gang Attack is no longer on the history list, so re-activate it with MissionsAtCoords
		Set_MissionsAtCoords_Mission_With_Filename_Should_Activate(clientFilename)
		
		// Clear the Client History Slot
		g_sHistoryClientGA.gahcCloudFilename[historySlot] = ""
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect][GangAttack]:")
		NET_PRINT_TIME()
		NET_PRINT("Server has added Gang Attack to history slot: ")
		NET_PRINT_INT(historySlot)
		NET_PRINT(" [")
		NET_PRINT(serverFilename)
		NET_PRINT("] - updating on Client")
		NET_NL()
	#ENDIF
	
	// The Server Gang Attack is newly on the history list, so make it await activation with MissionAtCoords
	Set_MissionsAtCoords_Mission_With_Filename_Should_Await_Activation(serverFilename)
	
	// Update the Client History to match Server
	g_sHistoryClientGA.gahcCloudFilename[historySlot] = serverFilename

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Gang Attack Client
PROC Maintain_MP_Activity_Selector_Gang_Attack_Group_Client()
	
	// Only maintain Gang Attacks on client when the Cloud Loaded data is initially available
	IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
		EXIT
	ENDIF

	Maintain_Client_Gang_Attack()
	Maintain_Client_Gang_Attack_History()

ENDPROC










