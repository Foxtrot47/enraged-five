//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_marketing_login_rewards.sc																	//
// Description: Script for managing player login rewards set up through tunables.					. 			//
//				The system goes through event ID validation then enqueues the available rewards to the 			//
//				Marketing Rewards Queue. If any items cannot be added the system will not marked them as done	// 
//				and try again when player re-enters freemode.													//
// Written by:  Bobby (Boyan) Yotov																				//
// Date:  		14/09/2022																						//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
#IF FEATURE_DLC_2_2022
USING "net_marketing_rewards_public.sch"
USING "net_marketing_login_rewards_defs.sch"
USING "net_cash_transactions.sch"
USING "stats_enums.sch"
USING "MP_globals_FM.sch"

FUNC INT GET_MLR_AWARDS_REWARDED_HASH()
	RETURN HASH("MLR_REWARDS_AWARDED0_v0")
ENDFUNC 

FUNC INT GET_MLR_EVENT_ID_HASH()
	RETURN HASH("MLR_EVENTID_v0")
ENDFUNC

FUNC STRING GET_MARKETING_LOGIN_REWARDS_STAGE_NAME(MARKETING_LOGIN_REWARDS_STAGES eStageArg)
	SWITCH eStageArg
		CASE MLRS_INIT						RETURN "MLRS_INIT"						BREAK
		CASE MLRS_RESET_DATA				RETURN "MLRS_RESET_DATA"				BREAK
		CASE MLRS_UPDATE_EVENT_ID			RETURN "MLRS_UPDATE_EVENT_ID"			BREAK
		CASE MLRS_SUBMIT_TO_QUEUE			RETURN "MLRS_SUBMIT_TO_QUEUE"			BREAK
		CASE MLRS_PROCESSING				RETURN "MLRS_PROCESSING"				BREAK
		CASE MLRS_UPDATE_SAVED_BITSETS			RETURN "MLRS_UPDATE_SAVED_BITSETS"			BREAK
		CASE MLRS_NOTIFY_PLAYER				RETURN "MLRS_NOTIFY_PLAYER"				BREAK
		CASE MLRS_FINISHED					RETURN "MLRS_FINISHED"					BREAK
	ENDSWITCH
	
	CASSERTLN (DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - GET_MARKETING_LOGIN_REWARDS_STAGE_NAME - missing/invalid stage value")
	RETURN "INVALID"
ENDFUNC

#IF IS_DEBUG_BUILD
PROC MLR_BIG_PRINT_DATA()
	CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "==============================================")
	CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] BIG PRINT DATA: ")
	CPRINTLN(DEBUG_NET_MARKETING_REWARDS, " - iLatestEventID :              ", g_sMarketingLoginRewardsData.iLatestEventID)
	CPRINTLN(DEBUG_NET_MARKETING_REWARDS, " - iLatestRewardsBitset0 :       ", g_sMarketingLoginRewardsData.iLatestRewardsBitset0)
	CPRINTLN(DEBUG_NET_MARKETING_REWARDS, " - iSavedEventID :               ", g_sMarketingLoginRewardsData.iSavedEventID)
	CPRINTLN(DEBUG_NET_MARKETING_REWARDS, " - iSavedRewardsBitset0 :        ", g_sMarketingLoginRewardsData.iSavedRewardsBitset0)
	CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "         ----------------------------         ")
	CPRINTLN(DEBUG_NET_MARKETING_REWARDS, " - iRewardsToAddBitset0:         ", g_sMarketingLoginRewardsData.iRewardsToAddBitset0)
	CPRINTLN(DEBUG_NET_MARKETING_REWARDS, " - iRewardsQueuedBitset0 :       ", g_sMarketingLoginRewardsData.iRewardsQueuedBitset0)
	CPRINTLN(DEBUG_NET_MARKETING_REWARDS, " - iRewardsFailedToAddBitset0 : ", g_sMarketingLoginRewardsData.iRewardsFailedToAddBitset0)
	CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "===============================================")
ENDPROC
#ENDIF

PROC SET_MARKETING_LOGIN_REWARDS_STAGE(MARKETING_LOGIN_REWARDS_STAGES eNextStage)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("mlrMoreInfo")
		MLR_BIG_PRINT_DATA()
	ENDIF
	#ENDIF
	
	IF (eNextStage != g_sMarketingLoginRewardsData.eCurrentStage)
		CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - SET_MARKETING_LOGIN_REWARDS_STAGE - From: ", GET_MARKETING_LOGIN_REWARDS_STAGE_NAME(g_sMarketingLoginRewardsData.eCurrentStage), " To: ", GET_MARKETING_LOGIN_REWARDS_STAGE_NAME(eNextStage))
	ENDIF
	g_sMarketingLoginRewardsData.eCurrentStage = eNextStage
ENDPROC

/// PURPOSE:
///    Resets the saved rewards bitsets and updates the saved event id to match EventID. 
/// PARAMS:
///    iEventID - event id to be saved
PROC MARKETING_LOGIN_REWARDS_RESET_DATA(MARKETING_LOGIN_REWARDS_STAGES eStageToJumpToAfterReset, INT iEventID)
	IF USE_SERVER_TRANSACTIONS()
		INT iInventoryItem[2], iInventoryValue[2]
		iInventoryItem[0] = GET_MLR_AWARDS_REWARDED_HASH()
		iInventoryValue[0] = 0
		iInventoryItem[1] = GET_MLR_EVENT_ID_HASH()
		iInventoryValue[1] = iEventID
		IF PROCESS_UPDATE_STORAGE_DATA_TRANSACTION(g_sMarketingLoginRewardsData.eTransactionResult, iInventoryItem, iInventoryValue)
			IF g_sMarketingLoginRewardsData.eTransactionResult = TRANSACTION_STATE_SUCCESS
				SET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_REWARDS_AWARDED0, 0)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_EVENTID, iEventID)
				g_sMarketingLoginRewardsData.iSavedRewardsBitset0 = 0
				g_sMarketingLoginRewardsData.iSavedEventID 	= iEventID
				SET_MARKETING_LOGIN_REWARDS_STAGE(eStageToJumpToAfterReset)
				CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_RESET_DATA: Transaction Successfull - Reset bitsets and updated EventID to ", iEventID)
			ELIF g_sMarketingLoginRewardsData.eTransactionResult = TRANSACTION_STATE_FAILED
				CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_RESET_DATA: Transaction Failed")
				SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_FINISHED)
			ENDIF
			g_sMarketingLoginRewardsData.eTransactionResult = TRANSACTION_STATE_DEFAULT
		ELSE
			CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_RESET_DATA: waiting for PROCESS_UPDATE_STORAGE_DATA_TRANSACTION: ", g_sMarketingLoginRewardsData.eTransactionResult)
		ENDIF
	ELSE
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_REWARDS_AWARDED0, 0)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_EVENTID, iEventID)
		g_sMarketingLoginRewardsData.iSavedRewardsBitset0 = 0
		g_sMarketingLoginRewardsData.iSavedEventID = iEventID
		SET_MARKETING_LOGIN_REWARDS_STAGE(eStageToJumpToAfterReset)
		CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_RESET_DATA: Reset bitsets and updated EventID to ", iEventID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Processes the transaction to the server for the event id (MP_STAT_MLR_EVENTID)
PROC MARKETING_LOGIN_REWARDS_UPDATE_SAVED_EVENT_ID(INT iEventID)
	IF USE_SERVER_TRANSACTIONS()
		INT iInventoryItem[1], iInventoryValue[1]
		iInventoryItem[0] = GET_MLR_EVENT_ID_HASH()
		iInventoryValue[0] = iEventID
		// Returns true if finished
		IF PROCESS_UPDATE_STORAGE_DATA_TRANSACTION(g_sMarketingLoginRewardsData.eTransactionResult, iInventoryItem, iInventoryValue)
			IF g_sMarketingLoginRewardsData.eTransactionResult = TRANSACTION_STATE_SUCCESS
				CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_UPDATE_SAVED_EVENT_ID: Transaction Successfull - Updated EventID to ", iEventID)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_EVENTID, iEventID)
				SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_SUBMIT_TO_QUEUE)
				g_sMarketingLoginRewardsData.iSavedEventID = iEventID
			ELIF g_sMarketingLoginRewardsData.eTransactionResult = TRANSACTION_STATE_FAILED
				CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_UPDATE_SAVED_EVENT_ID: Transaction Failed")
				SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_FINISHED)
			ENDIF
			
			g_sMarketingLoginRewardsData.eTransactionResult = TRANSACTION_STATE_DEFAULT
		ELSE
			CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_UPDATE_SAVED_EVENT_ID: waiting for PROCESS_UPDATE_STORAGE_DATA_TRANSACTION: ", g_sMarketingLoginRewardsData.eTransactionResult)
		ENDIF
	ELSE
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_EVENTID, iEventID)
		SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_SUBMIT_TO_QUEUE)
		g_sMarketingLoginRewardsData.iSavedEventID = iEventID
		CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_UPDATE_SAVED_EVENT_ID: Updated EventID to ", iEventID)
	ENDIF
ENDPROC

/// PURPOSE:
///    Processes the transaction to the server for the awarded rewards stat (MP_STAT_MLR_REWARDS_AWARDED0)
PROC MARKETING_LOGIN_REWARDS_UPDATE_SAVED_BITSETS(INT updatedRewardsBitset0)		
	IF USE_SERVER_TRANSACTIONS()
		INT iInventoryItem[1], iInventoryValue[1]
		iInventoryItem[0] = GET_MLR_AWARDS_REWARDED_HASH()
		iInventoryValue[0] = updatedRewardsBitset0
		IF PROCESS_UPDATE_STORAGE_DATA_TRANSACTION(g_sMarketingLoginRewardsData.eTransactionResult, iInventoryItem, iInventoryValue)
			IF g_sMarketingLoginRewardsData.eTransactionResult = TRANSACTION_STATE_SUCCESS
				SET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_REWARDS_AWARDED0, updatedRewardsBitset0)
				g_sMarketingLoginRewardsData.iSavedRewardsBitset0 = updatedRewardsBitset0
				SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_NOTIFY_PLAYER)
				CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_UPDATE_SAVED_BITSETS: Transaction Successfull - Updated rewards awarded: ", updatedRewardsBitset0)
			ELIF g_sMarketingLoginRewardsData.eTransactionResult = TRANSACTION_STATE_FAILED
				SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_FINISHED)
				CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_UPDATE_SAVED_BITSETS: Transaction Failed")
			ENDIF
			
			g_sMarketingLoginRewardsData.eTransactionResult = TRANSACTION_STATE_DEFAULT
		ELSE
			CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_UPDATE_SAVED_BITSETS: waiting for PROCESS_UPDATE_STORAGE_DATA_TRANSACTION: ", g_sMarketingLoginRewardsData.eTransactionResult)
		ENDIF
	ELSE
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_REWARDS_AWARDED0, updatedRewardsBitset0)
		g_sMarketingLoginRewardsData.iSavedRewardsBitset0 = updatedRewardsBitset0
		SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_NOTIFY_PLAYER)
		CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MARKETING_LOGIN_REWARDS_UPDATE_SAVED_BITSETS: Updated EventID to ", updatedRewardsBitset0)
	ENDIF
ENDPROC




/// PURPOSE:
///    Processes the state machine to handle updating,receiving and synchronising the marketing login rewards for the player. 
///    Returns FALSE if it has finished and there is no need to maintain it, TRUE if it has to run
FUNC BOOL MAINTAIN_MARKETING_LOGIN_REWARDS()
	IF g_sMarketingLoginRewardsData.eCurrentStage = MLRS_FINISHED
		IF (GET_FRAME_COUNT() % 120) = 0
			CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - Leaving because finished")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT g_sMPTunables.bWINTER_22_ENABLE_MARKETING_LOGIN_REWARDS
	#IF IS_DEBUG_BUILD
	AND NOT GET_COMMANDLINE_PARAM_EXISTS("mlrForceEnable")
	#ENDIF
		CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - Rewards Not Enabled")
		
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_REWARDS_AWARDED0) != 0
			MARKETING_LOGIN_REWARDS_RESET_DATA(MLRS_FINISHED, 0)
		ELSE
			SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_FINISHED)
		ENDIF
		RETURN TRUE
	ENDIF
	
	INT bitIndex = 0
	MARKETING_REWARDS_TYPE rewardType
	
	SWITCH g_sMarketingLoginRewardsData.eCurrentStage
		CASE MLRS_INIT
			// Reset data for processing
			g_sMarketingLoginRewardsData.iRewardsFailedToAddBitset0 	= 0
			g_sMarketingLoginRewardsData.iRewardsQueuedBitset0 			= 0
			g_sMarketingLoginRewardsData.iRewardsToAddBitset0			= 0
			g_sMarketingLoginRewardsData.eTransactionResult 			= TRANSACTION_STATE_DEFAULT
			
			// Collect event data
			g_sMarketingLoginRewardsData.iSavedEventID 					= GET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_EVENTID)
			g_sMarketingLoginRewardsData.iSavedRewardsBitset0 			= GET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_REWARDS_AWARDED0)
			#IF IS_DEBUG_BUILD
			IF !g_sMarketingLoginRewardsData.bEnableOverrideLoginRewards
			#ENDIF
				g_sMarketingLoginRewardsData.iLatestEventID 			= g_sMPTunables.iWINTER_22_MARKETING_LOGIN_REWARDS_EVENTID
				g_sMarketingLoginRewardsData.iLatestRewardsBitset0 		= g_sMPTunables.iWINTER_22_MARKETING_LOGIN_REWARDS_BITSET0
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			
			// Run the state machine
			IF g_sMarketingLoginRewardsData.iSavedEventID != g_sMarketingLoginRewardsData.iLatestEventID
				IF g_sMarketingLoginRewardsData.iSavedRewardsBitset0 != 0
					SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_RESET_DATA)
				ELSE
					SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_UPDATE_EVENT_ID)
				ENDIF
			ELSE
				IF g_sMarketingLoginRewardsData.iSavedRewardsBitset0 != g_sMarketingLoginRewardsData.iLatestRewardsBitset0
					SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_SUBMIT_TO_QUEUE)
				ELSE
					SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_FINISHED)
				ENDIF
			ENDIF	
		BREAK
		
		CASE MLRS_RESET_DATA
			MARKETING_LOGIN_REWARDS_RESET_DATA(MLRS_SUBMIT_TO_QUEUE, g_sMarketingLoginRewardsData.iLatestEventID)
		BREAK
		
		CASE MLRS_UPDATE_EVENT_ID
			MARKETING_LOGIN_REWARDS_UPDATE_SAVED_EVENT_ID(g_sMarketingLoginRewardsData.iLatestEventID)
		BREAK
		
		CASE MLRS_SUBMIT_TO_QUEUE
			g_sMarketingLoginRewardsData.iRewardsToAddBitset0 = (g_sMarketingLoginRewardsData.iSavedRewardsBitset0 ^ g_sMarketingLoginRewardsData.iLatestRewardsBitset0) // we want only the ones that have not been processed yet
			#IF IS_DEBUG_BUILD 
			IF GET_COMMANDLINE_PARAM_EXISTS("mlrMoreInfo")
				CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - g_sMarketingLoginRewardsData.iRewardsToAddBitset0 = (iSavedRewardsBitset0 ^ iLatestRewardsBitset0) =  ", g_sMarketingLoginRewardsData.iRewardsToAddBitset0)
			ENDIF
			#ENDIF
			
			REPEAT MARKETING_REWARDS_COUNT rewardType
				bitIndex = ENUM_TO_INT(rewardType)
				IF IS_BIT_SET(g_sMarketingLoginRewardsData.iRewardsToAddBitset0, bitIndex)
					IF CAN_PLAYER_RECEIVE_MARKETING_REWARD (rewardType)
					AND ADD_MARKETING_REWARD_TO_QUEUE(rewardType)
						SET_BIT(g_sMarketingLoginRewardsData.iRewardsQueuedBitset0, bitIndex)
						CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MLRS_SUBMIT_TO_QUEUE: Added ", GET_MARKETING_REWARDS_TYPE_NAME(rewardType))
					ELSE
						SET_BIT (g_sMarketingLoginRewardsData.iRewardsFailedToAddBitset0, bitIndex)
						CLEAR_BIT(g_sMarketingLoginRewardsData.iRewardsToAddBitset0, bitIndex)
						CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MLRS_SUBMIT_TO_QUEUE: Cannot add ", GET_MARKETING_REWARDS_TYPE_NAME(rewardType))
					ENDIF
				ENDIF
			ENDREPEAT
			
			// Safety check if everything went well - the queued bitset should be containing the iRewardsQueuedBitset0
			// (only if all items are enqueued in the same frame, won't work for staggered as the queue might process items)
			IF (g_sMarketingRewardsData.iRewardsInQueueBitset0 & g_sMarketingLoginRewardsData.iRewardsQueuedBitset0 ) != g_sMarketingLoginRewardsData.iRewardsQueuedBitset0
				CASSERTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MLRS_SUBMIT_TO_QUEUE: Missing queued items from successfully queued rewards")
			ENDIF
			
			// Update the add bitset to match the queued as the queue bitset would be used to check which are processed
			// The add bitset would later be used to update the saved bitset as we only want to add items that get processed
			g_sMarketingLoginRewardsData.iRewardsToAddBitset0 = g_sMarketingLoginRewardsData.iRewardsQueuedBitset0
			
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("mlrMoreInfo")
				CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MLRS_SUBMIT_TO_QUEUE: Setting: iRewardsToAddBitset0 = iRewardsQueuedBitset0 = ", g_sMarketingLoginRewardsData.iRewardsQueuedBitset0)
			ENDIF
			#ENDIF
			
			IF g_sMarketingLoginRewardsData.iRewardsToAddBitset0 != 0
				SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_PROCESSING)
			ELIF g_sMarketingLoginRewardsData.iRewardsFailedToAddBitset0 != 0
				SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_NOTIFY_PLAYER)
			ELSE
				SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_FINISHED)
			ENDIF
		BREAK
		
		// There is no feedback from rewards queue so we have to go through all and check them
		CASE MLRS_PROCESSING		
			IF g_sMarketingLoginRewardsData.iRewardsQueuedBitset0 != 0 
				REPEAT MARKETING_REWARDS_COUNT rewardType
					bitIndex = ENUM_TO_INT(rewardType)
					IF IS_BIT_SET(g_sMarketingLoginRewardsData.iRewardsQueuedBitset0, bitIndex)
						IF NOT IS_MARKETING_REWARD_IN_QUEUE (rewardType)
							CLEAR_BIT(g_sMarketingLoginRewardsData.iRewardsQueuedBitset0, bitIndex)
							CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MLRS_PROCESSING: Reward of type: ", GET_MARKETING_REWARDS_TYPE_NAME(rewardType), " was processed")
						ENDIF
					ENDIF 
				ENDREPEAT
			ELSE
				IF g_sMarketingLoginRewardsData.iSavedRewardsBitset0 != g_sMarketingLoginRewardsData.iRewardsToAddBitset0
					SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_UPDATE_SAVED_BITSETS)
				ELIF g_sMarketingLoginRewardsData.iRewardsFailedToAddBitset0 != 0
					SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_NOTIFY_PLAYER)
				ELSE
					SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_FINISHED)
				ENDIF
			ENDIF
		BREAK
		
		CASE MLRS_UPDATE_SAVED_BITSETS
			//INT updatedRewardsBitset0 = iSavedRewardsBitset0 | g_sMarketingLoginRewardsData.iRewardsToAddBitset0
			MARKETING_LOGIN_REWARDS_UPDATE_SAVED_BITSETS((g_sMarketingLoginRewardsData.iSavedRewardsBitset0 | g_sMarketingLoginRewardsData.iRewardsToAddBitset0))
		BREAK
		
		CASE MLRS_NOTIFY_PLAYER
			IF g_sMarketingLoginRewardsData.iRewardsFailedToAddBitset0 != 0
				// Handle notifications then go to finished state
				// @bty: todo - notify player
				SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_FINISHED)
			ELSE 
				// Go to finished state
				SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_FINISHED)
			ENDIF
		BREAK
		
		CASE MLRS_FINISHED
			// Updates the local data in case anything needs to access the updated variables after it has finished
			g_sMarketingLoginRewardsData.iSavedEventID 					= GET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_EVENTID)
			g_sMarketingLoginRewardsData.iSavedRewardsBitset0 			= GET_MP_INT_CHARACTER_STAT(MP_STAT_MLR_REWARDS_AWARDED0)
			CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - MLRS_FINISHED: Updating local data")
			RETURN FALSE
		BREAK
		
		DEFAULT
			CASSERTLN (DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_LOGIN_REWARDS] - missing/invalid stage value")
			RETURN FALSE
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_MARKETING_LOGIN_REWARDS_WIDGETS(MARKETING_LOGIN_REWARDS_WIDGET_DATA &sMLRWidgetData)
	START_WIDGET_GROUP("Marketing Rewards (Login) ")
		ADD_WIDGET_BOOL("Enable testing of login rewards", sMLRWidgetData.bMLREnableTestingThroughWidget)
		
		START_WIDGET_GROUP("How To Run Testing")
			ADD_WIDGET_STRING("1. Testing should be enabled.")
			ADD_WIDGET_STRING("2. Select rewards in Test Setup ")
			ADD_WIDGET_STRING("3. Select [Stop Event] (used as a reset for the state machine) ")
			ADD_WIDGET_STRING("4. Select [Run event] to run state machine ")
			ADD_WIDGET_STRING("5. EventID value is randomly generated to not match.")
			ADD_WIDGET_STRING("6. When processing stage finishes when it reaches stage *7*")
			ADD_WIDGET_STRING("Note: Don't reload freemode ^^")
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Test setup")
		MARKETING_REWARDS_TYPE type
		REPEAT MARKETING_REWARDS_COUNT type 
			ADD_WIDGET_BOOL(GET_MARKETING_REWARDS_TYPE_NAME_FOR_WIDGETS(type), sMLRWidgetData.bMLRWidgetItemsToggles[ENUM_TO_INT(type)])
		ENDREPEAT
		ADD_WIDGET_INT_READ_ONLY("Test event bitset: ", sMLRWidgetData.iMLRWidgetBitset)
		ADD_WIDGET_INT_READ_ONLY("Test event ID: ", sMLRWidgetData.iMLRWidgetEventID)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Player Saved Data:")
		ADD_WIDGET_INT_READ_ONLY("Current saved bitset: ", sMLRWidgetData.iMLRWidgetSavedBitset)
		ADD_WIDGET_INT_READ_ONLY("Current saved eventID: ", sMLRWidgetData.iMLRWidgetSavedEventID)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Processing")
		ADD_WIDGET_BOOL("Stop event", sMLRWidgetData.bMLRWidgetResetRun)
		ADD_WIDGET_BOOL("Run event", sMLRWidgetData.bMLRWidgetRunBitset)
		ADD_WIDGET_INT_READ_ONLY("Processing Stage:", sMLRWidgetData.iMLRWidgetStage)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

FUNC BOOL MAINTAIN_MARKETING_LOGIN_REWARDS_WIDGETS(MARKETING_LOGIN_REWARDS_WIDGET_DATA &sMLRWidgetData)
	IF !sMLRWidgetData.bMLREnableTestingThroughWidget
		g_sMarketingLoginRewardsData.bEnableOverrideLoginRewards = FALSE
		
		IF !sMLRWidgetData.bMLRWidgetResetRun
			SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_FINISHED)
			sMLRWidgetData.iMLRWidgetStage = ENUM_TO_INT(MLRS_FINISHED)
			sMLRWidgetData.bMLRWidgetRunBitset = FALSE
			sMLRWidgetData.bMLRWidgetRunStarted = FALSE
			sMLRWidgetData.bMLRWidgetResetRun = FALSE
		ENDIF
		RETURN FALSE
	ENDIF
	
	g_sMarketingLoginRewardsData.bEnableOverrideLoginRewards = TRUE
	
	sMLRWidgetData.iMLRWidgetSavedBitset = g_sMarketingLoginRewardsData.iSavedRewardsBitset0
	sMLRWidgetData.iMLRWidgetSavedEventID = g_sMarketingLoginRewardsData.iSavedEventID
	
	INT i
	IF (g_sMarketingLoginRewardsData.eCurrentStage = MLRS_FINISHED)
		REPEAT (ENUM_TO_INT(MARKETING_REWARDS_COUNT)) i
			IF sMLRWidgetData.bMLRWidgetItemsToggles[i]
				SET_BIT(sMLRWidgetData.iMLRWidgetBitset, i)
			ELSE
				CLEAR_BIT(sMLRWidgetData.iMLRWidgetBitset, i)
			ENDIF
		ENDREPEAT
	
		IF NOT sMLRWidgetData.bMLRWidgetRunStarted
			IF sMLRWidgetData.bMLRWidgetRunBitset
				// Update widget values
				sMLRWidgetData.bMLRWidgetRunStarted = TRUE
				sMLRWidgetData.bMLRWidgetRunBitset = FALSE
				sMLRWidgetData.bMLRWidgetResetRun = FALSE
				
				// Generate new EventID
				INT iLastEventID = sMLRWidgetData.iMLRWidgetEventID
				sMLRWidgetData.iMLRWidgetEventID = GET_RANDOM_INT_IN_RANGE()
				
				IF sMLRWidgetData.iMLRWidgetEventID = iLastEventID
				OR sMLRWidgetData.iMLRWidgetEventID = g_sMarketingLoginRewardsData.iSavedEventID
					sMLRWidgetData.iMLRWidgetEventID++
				ENDIF
				
				// Force override MLR data to use
				g_sMarketingLoginRewardsData.iLatestEventID = sMLRWidgetData.iMLRWidgetEventID
				g_sMarketingLoginRewardsData.iLatestRewardsBitset0 = sMLRWidgetData.iMLRWidgetBitset
				
				// Run MLR
				SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_INIT)
				sMLRWidgetData.iMLRWidgetStage = ENUM_TO_INT(MLRS_INIT)
				RETURN TRUE
			ENDIF
		ELSE 
			sMLRWidgetData.bMLRWidgetRunBitset = FALSE
		ENDIF
	ELSE
		// Keep test toggles the same as the processed event
		REPEAT (ENUM_TO_INT(MARKETING_REWARDS_COUNT)) i
			sMLRWidgetData.bMLRWidgetItemsToggles[i] = IS_BIT_SET(sMLRWidgetData.iMLRWidgetBitset, i)
		ENDREPEAT
	ENDIF
	
	// Reset event if clicked
	IF sMLRWidgetData.bMLRWidgetResetRun
		SET_MARKETING_LOGIN_REWARDS_STAGE(MLRS_FINISHED)
		sMLRWidgetData.iMLRWidgetStage = ENUM_TO_INT(MLRS_FINISHED)
		sMLRWidgetData.bMLRWidgetRunBitset = FALSE
		sMLRWidgetData.bMLRWidgetRunStarted = FALSE
		sMLRWidgetData.bMLRWidgetResetRun = FALSE
	ENDIF
	
	sMLRWidgetData.iMLRWidgetStage = ENUM_TO_INT(g_sMarketingLoginRewardsData.eCurrentStage)
	RETURN FALSE
ENDFUNC
#ENDIF
#ENDIF
