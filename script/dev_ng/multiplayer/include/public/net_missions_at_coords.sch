USING "rage_builtins.sch"
USING "globals.sch"

USING "Script_Player.sch"
USING "Script_Network.sch"

USING "net_mission_joblist_public.sch"
USING "net_mission_info.sch"
USING "net_mission_control_public.sch"
USING "net_vote_at_blip_public.sch"
USING "net_mission_locate_message.sch"
USING "net_missions_shared_cloud_data_public.sch"

USING "fm_post_mission_cleanup_public.sch"

USING "Net_Prints.sch"

#IF IS_DEBUG_BUILD
	USING "Net_Missions_At_Coords_Debug.sch"
#ENDIF
	USING "net_heists_hardcoded.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Missions_At_Coords.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Handles all MP at-blip missions that get triggered through the Mission Controller. Deals with drawing the
//								blip, drawing the corona, displaying the name in the corona, communicating with Mission Controller,
//								voting, etc. Intended for use in CnC and Freemode.
//
//		NOTES			:	MARKED FOR REFRESH - KGM 10/2/13
//								When the cloud data is going to be reloaded, all missions that use cloud-loaded data will be Marked for
//								Refresh. During re-initialisation, any duplicate missions found will be left as-is with the 'Marked
//								for Refresh' flag removed. At the end of the re-initialisation, any missions still Marked for Refresh
//								must no longer be available from the cloud and will be 'Marked for Delete' instead.
//
//							LOCKED and TEMPORARILY UNLOCKED MISSIONS - KGM 18/2/13
//								Missions are locked when the player is too low a level to join them be entering the corona. A low-rank
//								player can still accept an invite to a locked mission and this will temporarily unlock the mission
//								after the player has been warped to the coordinates. When the player moves back out of the Mission Name
//								Display range the mission will become Locked again which will make the mission move out to the 'Outside
//								All Ranges' range and wait there until it is unlocked. An additional option for the Temporarily Unlocked
//								behaviour is to keep the mission unlocked until played: this was added originally for the Race Tutorial.
//
//							DISPLAY BLIPS IN INTERIOR ONLY - KGM 18/2/13
//								This was added to hide the Shooting Range blip until the player enters the GunShop to prevent it being
//								displayed over the GunShop blip. The basic implementation is, if the player is within Display Corona
//								range and the player is in an interior, display the blip, otherwise hide it. This implemetation method
//								has many potential loopholes (two nearby interiors, etc.) but should be fine for the initial, and most
//								likely subsequent, uses. The general 'Hide Blip' function will always hide an 'Interior Only' blip, but
//								the general 'Show Blip' function will always ignore it. A general function while in range will
//								display it or not as appropriate.
//
//							TICKS ON 'PLAYED' ACTIVITY BLIPS - KGM 23/2/13
//								Code can now display a 'tick' on a blip. I'll use this to mark missions as having been played. The
//								header data for a mission contains this information, so I'll pass it in at the same time as I register
//								the mission so that the tick can be displayed. The 'played' cloud-data is modified externally so it
//								doesn't need updated here, however the local 'played' flag should be updated to maintain consistency
//								within the current session.
//
//							INITIAL LONG-RANGE BLIPS - KGM 2/3/13
//								If there are no blips within Corona Display range when the player first enters the game, then mark
//								a number of blips that are just outside the Corona Display range as long-range so that the player is
//								always has a feeling there is something to do. These need to get unmarked as long-range - I currently
//								do this when the next get re-displayed.
//
//							DON'T TRIGGER A MISSION AT THE PLAYER's INITIAL SPAWN LOCATION - KGM 3/3/13
//								It is possible for there to be a mission at the player's initial spawn location. If this happens, the
//								mission will not be allowed to enter the corona display range until the player has moved away. This
//								is controlled by a global g_vInitialSpawnLocation set by Neil as the player enters the game. I clear
//								this global after the player has moved away. If the global contains a vector, I don't display the
//								corona if it is near the vector.
//
//							VEHICLE GENERATORS AT A CORONA - KGM 3/3/13
//								If a player goes within Corona Display radius of a corona then I'll switch off vehicle generation
//								in the area for all players. I'm not going to bother switching them back on because there should
//								never be vehicle generated cars at a corona.
//
//							SHARING CLOUD-LOADED HEADER DATA - KGM 10/3/13
//								UGC mission header data is only loaded on the UGC owner's machine and only when the owner enters a
//								corona will the mission then become available to all players. The game needs to share the cloud header
//								data with all players at this point so that they all have the details needed to load the full mission
//								data. In addition, whenever a player enters any Rockstar Created or Rockstar Verified mission creator
//								corona, the data will also now get shared - this is to safeguard against the cloud data being out of
//								sync on different machines and also the possibility that the header data for a mission failed to
//								download for one player allowing that player to still have access to the mission header data. The
//								sharing of the mission is done using the Missions Shared Cloud Data system.
//
//							ALLOW DURING AMBIENT TUTORIALS - KGM 11/7/13
//								Normally, if the player is on an Ambient Tutorial then all missions are functionally blocked.
//								We now need to allow specific missions to be active during an Ambient Tutorial, but the way to do this
//								is counter-intuitive. If any mission is set as 'Allow During Ambient Tutorial' then we have to REMOVE
//								the functional block for ALL missions (this is because it's really tricky to only remove this for one
//								mission because it's in a general checking function which isn't associated with any specific mission).
//								What we then do is class ALL missions as LOCKED except for the missions that are allowed during an
//								ambient tutorial. It's important that the global state is accurate, so I've made it a counter that
//								counts how many missions are classed as 'allowed during ambient tutorial'. When the counter is 0
//								then the game returns to normal operation and Ambient Tutorials block all missions again. This counter
//								needs to be accurate so I'll add a periodic safety check that compares it with all individual missions.
//
//							GANG ATTACK AVAILABILITY - KGM 25/8/13
//								Gang Attacks, if active and joinable, ignore closing time rules and will now also become available to
//								lower-rank players. This is based on the MATC_BITFLAG_REMAIN_OPEN_IF_ACTIVE flag and updated in the
//								mission lock maintenance function.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      General Routines
// ===========================================================================================================

// PURPOSE:	Check if the player is in an apartment
//
// RETURN VALUE:			BOOL				TRUE if the player is in an apartment, FALSE if not
FUNC BOOL Is_MissionsAtCoords_Player_In_An_Apartment()
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	RETURN GlobalplayerBD_FM[iPlayer].propertyDetails.iCurrentlyInsideProperty > 0 
		OR globalPlayerBD[iPlayer].SimpleInteriorBD.eCurrentSimpleInterior != SIMPLE_INTERIOR_INVALID //IS_PLAYER_IN_ANY_SIMPLE_INTERIOR
		OR IS_BIT_SET(GlobalplayerBD_FM[iPlayer].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY) //IS_PLAYER_EXITING_PROPERTY
ENDFUNC

// ===========================================================================================================
//      Cleared Wanted Level Routines
// ===========================================================================================================

// PURPOSE:	Store the current Wanted Level prior to it being cleared
//
// INPUT PARAMS:		paramCost			Cost of clearing the wanted level
PROC Store_MissionsAtCoords_Cleared_Wanted_Level(INT paramCost)

	g_matcClearedWantedLevel		= GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
	g_matcCostOfClearedWantedLevel	= paramCost
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords][WL]: Store - Cleared_Wanted_Level: ")
		PRINTINT(g_matcClearedWantedLevel)
		PRINTSTRING(". Cost $")
		PRINTINT(g_matcCostOfClearedWantedLevel)
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Cleared Wanted Level
PROC Clear_MissionsAtCoords_Cleared_Wanted_Level()

	IF (g_matcClearedWantedLevel = 0)
	AND (g_matcCostOfClearedWantedLevel = 0)
		EXIT
	ENDIF

	g_matcClearedWantedLevel		= 0
	g_matcCostOfClearedWantedLevel	= 0
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords][WL]: Clear - Cleared_Wanted_Level: ")
		PRINTINT(g_matcClearedWantedLevel)
		PRINTSTRING(". Cost $")
		PRINTINT(g_matcCostOfClearedWantedLevel)
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Restore the Cleared Wanted Level and refund the cost of clearing it
PROC Restore_And_Refund_MissionsAtCoords_Cleared_Wanted_Level()

	IF (g_matcClearedWantedLevel = 0)
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords][WL]: Restore - Cleared_Wanted_Level: ")
			PRINTINT(g_matcClearedWantedLevel)
			PRINTSTRING(". Cost $")
			PRINTINT(g_matcCostOfClearedWantedLevel)
			PRINTSTRING(". IGNORE: Nothing to restore.")
			PRINTNL()
		#ENDIF
		
		Clear_MissionsAtCoords_Cleared_Wanted_Level()
		
		EXIT
	ENDIF
	
	// If the player is not ok, then clear the variables but don't refund or restore
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords][WL]: Restore - Cleared_Wanted_Level: ")
			PRINTINT(g_matcClearedWantedLevel)
			PRINTSTRING(". Cost $")
			PRINTINT(g_matcCostOfClearedWantedLevel)
			PRINTSTRING(". IGNORE: Player not OK.")
			PRINTNL()
		#ENDIF
		
		Clear_MissionsAtCoords_Cleared_Wanted_Level()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords][WL]: Restore - Cleared_Wanted_Level: ")
		PRINTINT(g_matcClearedWantedLevel)
		PRINTSTRING(". Refund: $")
		PRINTINT(g_matcCostOfClearedWantedLevel)
		PRINTSTRING(". Player walked out of Corona.")
		PRINTNL()
	#ENDIF
	
	// Only refund and restore if player not in property
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		PRINTSTRING(".SAR: Player is in property - No refund or restore")
		Clear_MissionsAtCoords_Cleared_Wanted_Level()
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		PRINTSTRING(".SAR: Player is not in property - Give refund and restore")
	
		// Restore the Wanted Level immediately
		SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), g_matcClearedWantedLevel)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		
		// Refund the cost of losing the wanted level
		IF g_matcCostOfClearedWantedLevel != 0
			NETWORK_REFUND_CASH_TYPE(g_matcCostOfClearedWantedLevel, MP_REFUND_TYPE_CLEAR_WANTED_LEVEL, MP_REFUND_REASON_NOT_USED)
		ENDIF
		
		PRINT_HELP_WITH_NUMBER("RESTORE_WANTED", g_matcCostOfClearedWantedLevel)
			
		Clear_MissionsAtCoords_Cleared_Wanted_Level()
	ENDIF
	
ENDPROC




// ===========================================================================================================
//      Mission At Coords Band Access Routines
// ===========================================================================================================

// PURPOSE:	Retrieve the current number of missions in a Band
//
// INPUT PARAMS:			paramBand			The Band being checked
// RETURN VALUE:			INT					The number of missions in the Band
FUNC INT Get_Number_Of_MissionsAtCoords_In_Band(g_eMatCBandings paramBand)
	RETURN (g_sAtCoordsBandsMP[paramBand].matcbNumInBand)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the first mission slot in a Band
//
// INPUT PARAMS:			paramBand			The Band being checked
// RETURN VALUE:			INT					The first mission slot in the Band
FUNC INT Get_First_MissionsAtCoords_Slot_For_Band(g_eMatCBandings paramBand)
	RETURN (g_sAtCoordsBandsMP[paramBand].matcbFirstSlot)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the last mission slot in a Band
//
// INPUT PARAMS:			paramBand			The Band being checked
// RETURN VALUE:			INT					The last mission slot in the Band
FUNC INT Get_Last_MissionsAtCoords_Slot_For_Band(g_eMatCBandings paramBand)
	RETURN (g_sAtCoordsBandsMP[paramBand].matcbLastSlot)
ENDFUNC




// ===========================================================================================================
//      Mission At Coords Slot Access Routines
// ===========================================================================================================

// PURPOSE:	Retrieve the MissionID Data associated with a slot
//
// RETURN VALUE:		MP_MISSIONID_DATA	The MissionID data in this slot
FUNC MP_MISSION_ID_DATA Get_MissionsAtCoords_Slot_MissionID_Data(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcMissionIdData)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the MissionID associated with a slot
//
// RETURN VALUE:		MP_MISSION			The Mission ID in this slot
FUNC MP_MISSION Get_MissionsAtCoords_Slot_MissionID(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcMissionIdData.idMission)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Mission Variation associated with a slot
//
// RETURN VALUE:		INT				The Mission Variation for this slot
FUNC INT Get_MissionsAtCoords_Slot_Mission_Variation(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcMissionIdData.idVariation)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the ContentID associated with a slot
//
// RETURN VALUE:		TEXT_LABEL_23	The Content ID in this slot
FUNC TEXT_LABEL_23 Get_MissionsAtCoords_Slot_ContentID(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcMissionIdData.idCloudFilename)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Mission Coords associated with a slot
//
// RETURN VALUE:		VECTOR			The Coords for this slot
FUNC VECTOR Get_MissionsAtCoords_Slot_Coords(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcCoords)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Blip Index associated with a slot
//
// RETURN VALUE:		BLIP_INDEX			The Blip Index for this slot
FUNC BLIP_INDEX Get_MissionsAtCoords_Slot_BlipIndex(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcBlipIndex)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the InCorona ExternalID associated with a slot
//
// RETURN VALUE:		g_eMatCInCoronaExternalID			The InCorona ExternalID for this slot
FUNC g_eMatCInCoronaExternalID Get_MissionsAtCoords_Slot_InCorona_ExternalID_To_Use(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcExternalID)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the InCorona CreatorID associated with a slot
//
// RETURN VALUE:		INT				The InCorona CreatorID for this slot
FUNC INT Get_MissionsAtCoords_Slot_CreatorID(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcMissionIdData.idCreator)
ENDFUNC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the SharedRegID associated with this slot
//
// RETURN VALUE:		INT				The SharedRegID for this slot
FUNC INT Get_MissionsAtCoords_Slot_SharedRegID(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcMissionIdData.idSharedRegID)
ENDFUNC


/// PURPOSE:
///    Checks to see if the mission is a type that should not get blocked via the Block_All_MissionsAtCoords_Missions routines
FUNC BOOL Should_MissionAtCoords_Ignore_Block_On_Mission(INT paramSlot)

	MP_MISSION_ID_DATA coronaMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	IF TEMP_Check_If_Flow_Using_Mission_Subtype(coronaMissionIdData)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE: Checks and returns the mission id data if the cloudfilename matches a Mission at coords
///    
/// PARAMS:
///    cloudFilename - cloudFilename of the mission you want to retrieve the mission ID data for
///    thisMissionIdData - pass in a MP_MISSION_ID_DATA value to retrieve the matching mission ID
/// RETURNS: TRUE if a mission is found with the cloudFilename, 
///    
FUNC BOOL Get_MissionsOfName_MissionIdData(TEXT_LABEL_23 cloudFilename,MP_MISSION_ID_DATA &thisMissionIdData)
	
	INT	tempLoop = 0
	int filenameHash = GET_HASH_KEY(cloudFilename)
	
	REPEAT g_numAtCoordsMP tempLoop
		IF g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash		
			thisMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(temploop)
			RETURN TRUE		
		ENDIF
	ENDREPEAT	
	
	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the SharedRegID associated with this slot
//
// RETURN VALUE:		INT				The SharedRegID for this slot
FUNC STRING Get_MissionsAtCoords_Slot_MissionTitle(MP_MISSION_ID_DATA thisMissionIdData)
	
	if Is_FM_Cloud_Loaded_Activity_A_Heist(thisMissionIdData)
		RETURN "FMMC_GRP_HS" 	// HEIST
	elif Is_FM_Cloud_Loaded_Activity_Heist_Planning(thisMissionIdData)
		RETURN "FMMC_RSTAR_HP" 	// HEIST PLANNING
	elif Is_FM_Cloud_Loaded_Activity_A_LTS_Mission(thisMissionIdData)
		RETURN "FM_MISTYPE_LTS"	// LTS
	elif Is_FM_Cloud_Loaded_Activity_A_CTF_Mission(thisMissionIdData)
		RETURN "FM_MISTYPE_CTF"	// CTF
	endif
	
	BOOL bIsTeam
	if Is_FM_Cloud_Loaded_Activity_A_King_Of_The_Hill(thisMissionIdData, bIsTeam)
		IF bIsTeam
			RETURN "FM_MTYPE_KOTH"
		ENDIF
		RETURN "FM_MTYPE_KOTH"
	endif
	
	RETURN ""
ENDFUNC




// ===========================================================================================================
//      Mission At Coords LOCKED BITFLAG access routines
// ===========================================================================================================

// PURPOSE:	Get the Lock status for this mission
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
FUNC BOOL Is_MissionsAtCoords_Mission_Locked(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_ACTIVITY_LOCKED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Mission Lock status as LOCKED
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_As_Locked(INT paramSlot)

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_ACTIVITY_LOCKED)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Mission Locked. ")
		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
		PRINTSTRING(". Var: ")
		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Mission Lock status as UNLOCKED
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_As_Unlocked(INT paramSlot)

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_ACTIVITY_LOCKED)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Mission Unlocked. ")
		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
		PRINTSTRING(". Var: ")
		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
		PRINTNL()
	#ENDIF

ENDPROC





// ===========================================================================================================
//      Mission At Coords ALLOWED TO TRIGGER access routines
// ===========================================================================================================

// PURPOSE:	Check if the mission is allowed to trigger
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission is allowed to trigger as normal, otherwise FALSE if it can show the corona but can't trigger the mission
//
// NOTES:	This was added for some tutorial routine where a cutscene is required before triggering the mission
FUNC BOOL Is_MissionsAtCoords_Mission_Marked_As_Do_Not_Trigger(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_DO_NOT_TRIGGER))
ENDFUNC





// ===========================================================================================================
//      Mission At Coords TEMP LONG-RANGE access routines
// ===========================================================================================================

// PURPOSE:	Check if the mission blip is long-range
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission is temporarily long-range, otherwise FALSE if it is short-range
//
// NOTES:	This was added for some tutorial blips
FUNC BOOL Is_MissionsAtCoords_Mission_Blip_Temporarily_LongRange(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_SHOW_BLIP_LONG_RANGE))
ENDFUNC





// ===========================================================================================================
//      Mission At Coords SHARED MISSION access routines
// ===========================================================================================================

// PURPOSE:	Check if the mission was setup as a Shared Mission
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission was setup as a Shared Mission, FALSE if setup or updated by another setup routine
FUNC BOOL Is_MissionsAtCoords_Mission_A_Shared_Mission(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_ADDED_AS_SHARED_MISSION))
ENDFUNC





// ===========================================================================================================
//      Mission At Coords ON HOURS access routines
// ===========================================================================================================

// PURPOSE:	Check if the mission should be unlocked at this hour of the day
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:		BOOL				TRUE if the mission should be unlocked at this hour, otherwise FALSE
FUNC BOOL Is_MissionsAtCoords_Mission_Unlocked_At_This_Time_Of_Day(INT paramSlot)
	INT timeOfDayHour = GET_CLOCK_HOURS()
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcCorona.theOnHours, timeOfDayHour))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission has an on/off time
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:		BOOL				TRUE if the mission can become locked or unlocked at differen times of hte day, otherwise always unlocked
FUNC BOOL Is_MissionsAtCoords_Mission_Unlocked_All_Day(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcCorona.theOnHours = MATC_ALL_DAY)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	return the next valid TOD as INT
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:		INT					Returns -1 if all day active, otherwise returns next hour mission is active
FUNC INT Get_MissionsAtCoords_Mission_Unlocked_Next_Time_Of_Day_Active(INT paramSlot)
	if not Is_MissionsAtCoords_Mission_Unlocked_All_Day(paramSlot)	
		
		int TimeHour = 0
		
		for timehour = GET_CLOCK_HOURS() to 23
			if IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcCorona.theOnHours,timehour)
				RETURN timehour	
			endif
		endfor	
		for timehour = 0 to 23
			if IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcCorona.theOnHours,timehour)
				RETURN timehour	
			endif
		endfor	
		
		RETURN timehour	
	endif
	RETURN -1	
ENDFUNC




// ===========================================================================================================
//      Mission At Coords: Specific Invite Accepted Mission ContentID
// ===========================================================================================================


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the most recent ContentID for an invite acceptance belongs to the specified mission
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the invite accepted ContentID belongs to the specified mission, otherwise FALSE
FUNC BOOL Does_MissionsAtCoords_Slot_Contain_Most_Recent_Invite_Accepted_ContentID(INT paramSlot)

	IF NOT (Is_There_A_Most_Recent_Invite_Accepted_ContentID())
		RETURN FALSE
	ENDIF
	
	INT slotContentIdHash = g_sAtCoordsMP[paramSlot].matcIdCloudnameHash
	
	RETURN (g_sAtCoordsControlsMP_AddTU.matccInviteAcceptContentIdHash = slotContentIdHash)
	
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the ContentID Hash of the mission if it belongs to this slot's mission
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_MissionsAtCoords_If_Most_Recent_Invite_Accepted_ContentID(INT paramSlot)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: ")
		NET_PRINT_TIME()
		PRINTSTRING("Invite Accepted ContentID - Clear. Slot: ")
		PRINTINT(paramSlot)
		PRINTSTRING(" -")
	#ENDIF

	INT slotContentIdHash = g_sAtCoordsMP[paramSlot].matcIdCloudnameHash
	
	// Debug Output details of the value already stored (if there is one)
	#IF IS_DEBUG_BUILD
		IF (Is_There_A_Most_Recent_Invite_Accepted_ContentID())
			PRINTSTRING(" EXISTING HASH: ")
			PRINTINT(g_sAtCoordsControlsMP_AddTU.matccInviteAcceptContentIdHash)
		ENDIF
	#ENDIF
	
	// Nothing to do if there is not a Hash value to overwrite
	IF NOT (Is_There_A_Most_Recent_Invite_Accepted_ContentID())
		#IF IS_DEBUG_BUILD
			PRINTSTRING("IGNORE: No Hash")
			PRINTNL()
		#ENDIF

		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD	
		PRINTSTRING(" HASH TO CLEAR: ")
		PRINTINT(slotContentIdHash)
		PRINTSTRING(" - ")
	#ENDIF
	
	// Nothing to do if the stored Hash doesn't match the value to overwrite
	IF (g_sAtCoordsControlsMP_AddTU.matccInviteAcceptContentIdHash != slotContentIdHash)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("IGNORE: Different Hash.")
			PRINTNL()
		#ENDIF

		EXIT
	ENDIF
	
	// Clear the Hash and the safety timeout
	g_sAtCoordsControlsMP_AddTU.matccInviteAcceptContentIdHash	= 0
	g_sAtCoordsControlsMP_AddTU.matccInviteAcceptTimeout		= GET_GAME_TIMER()
	
	#IF IS_DEBUG_BUILD
		g_sAtCoordsControlsMP_AddTU.matccDebugInviteAcceptContentID = ""
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("CLEARED. ")
		PRINTSTRING(g_sAtCoordsMP[paramSlot].matcMissionIdData.idCloudFilename)
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Invite Accepted variable on safety timeout
// NOTES:	KGM 6/11/14 [BUG 2107595]: Ensures an 'Invite Accepted' restriction gets lifted after a period of time in case the player never made it into the corona
PROC Maintain_MissionsAtCoords_Most_Recent_Invite_Accepted_ContentID()

	IF NOT (Is_There_A_Most_Recent_Invite_Accepted_ContentID())
		EXIT
	ENDIF
	
	// Has the safety timeout expired?
	IF (GET_GAME_TIMER() < g_sAtCoordsControlsMP_AddTU.matccInviteAcceptTimeout)
		EXIT
	ENDIF
	
	// Safety timeout has expired. Remove the invite accepted contentID
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: ")
		NET_PRINT_TIME()
		PRINTSTRING("Invite Accepted ContentID - Safety Timeout. ContentID Hash: ")
		PRINTINT(g_sAtCoordsControlsMP_AddTU.matccInviteAcceptContentIdHash)
		PRINTSTRING(" [")
		PRINTSTRING(g_sAtCoordsControlsMP_AddTU.matccDebugInviteAcceptContentID)
		PRINTSTRING("]")
		PRINTNL()
	#ENDIF
	
	g_sAtCoordsControlsMP_AddTU.matccInviteAcceptContentIdHash = 0
	
	#IF IS_DEBUG_BUILD
		g_sAtCoordsControlsMP_AddTU.matccDebugInviteAcceptContentID = ""
	#ENDIF

ENDPROC



// ===========================================================================================================
//      Mission At Coords: Exclusive ContentID
// ===========================================================================================================

// PURPOSE:	Clear the Missions At Coords exclusive ContentID variables
// NOTES:	These are used to make a heist contentID exclusive even if the registration is delayed while the apartment loads
PROC Clear_MissionsAtCoords_Exclusive_ContentID()

	PRINTLN(".KGM [At Coords]: Clear Mission At Coords Exclusive ContentID variables")
	
	g_structMatcExclusiveCorona	emptyExclusiveCoronaStruct
	g_sMatcExclusiveContentID = emptyExclusiveCoronaStruct
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there is an exclusive contentID
//
// RETURN VALUE:			BOOL		TRUE if there is an exclusive contentID, otherwise FALSE
FUNC BOOL Is_There_An_Exclusive_ContentID()
	RETURN (g_sMatcExclusiveContentID.matcecContentIdHash != 0)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the contentID within this slot matches the exclusive contentID
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the contentIDs match, otherwise FALSE
FUNC BOOL Does_MissionsAtCoords_Slot_Contain_Exclusive_ContentID(INT paramSlot)

	IF NOT (Is_There_An_Exclusive_ContentID())
		RETURN FALSE
	ENDIF
	
	INT slotContentIdHash = g_sAtCoordsMP[paramSlot].matcIdCloudnameHash
	
	RETURN (g_sMatcExclusiveContentID.matcecContentIdHash = slotContentIdHash)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the exclusive contentID for a short period of time
PROC Maintain_MissionsAtCoords_Exclusive_ContentID()

	IF NOT (Is_There_An_Exclusive_ContentID())
		EXIT
	ENDIF
	
	IF (GET_GAME_TIMER() < g_sMatcExclusiveContentID.matcexTimeout)
		EXIT
	ENDIF
	
	// Timeout
	PRINTLN(".KGM [At Coords]: Exclusive ContentID has timed out. ContentID Hash: ", g_sMatcExclusiveContentID.matcecContentIdHash)
	Clear_MissionsAtCoords_Exclusive_ContentID()
	
ENDPROC








// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear that an Invite to the Mission has been accepted
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(INT paramSlot)

	IF NOT (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
		EXIT
	ENDIF

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_INVITE_ACCEPTED)

	// Clear the ContentID if it was set for this mission
	Clear_MissionsAtCoords_If_Most_Recent_Invite_Accepted_ContentID(paramSlot)
	
ENDPROC




// ===========================================================================================================
//      Player Has Wanted Level Access routines
// ===========================================================================================================

// PURPOSE:	Check if the player has a Wanted Level
//
// RETURN VALUE:		BOOL			TRUE if the player has a Wanted Level, otherwise FALSE
FUNC BOOL Does_MatC_Player_Have_Wanted_Level()
	RETURN (g_sAtCoordsControlsMP.matccPlayerHasWantedLevel)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Block a Walk-In Focus Mission if the player recently paid the JobList to remove a Wanted Level
//
// RETURN VALUE:		BOOL			TRUE if there is a delay outstanding, otherwise FALSE
//
// NOTES:	KGM 21/2/15 [BUG 2238981]: Introduced to fix as deadlock situtaion between a Walk-In and a Joblist Invite
FUNC BOOL Has_MatC_Player_Recently_Paid_To_Remove_Wanted_Level()

	// No timer?
	IF (g_sAtCoordsControlsMP_AddTU.matccPaidToClearWantedTimeout = 0)
		RETURN FALSE
	ENDIF
	
	// Timer expired?
	IF (GET_GAME_TIMER() > g_sAtCoordsControlsMP_AddTU.matccPaidToClearWantedTimeout)
		// ...expired, so clear the timer
		g_sAtCoordsControlsMP_AddTU.matccPaidToClearWantedTimeout = 0
		RETURN FALSE
	ENDIF
	
	// Timer still active
	RETURN TRUE
	
ENDFUNC




// ===========================================================================================================
//      Mission Is For Cutscene Triggering Only access routines
// ===========================================================================================================

// PURPOSE:	Check if the corona is setup purely to trigger a mocap cutscene
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the corona is being only used to trigger a mocap cutscene, otherwise FALSE
FUNC BOOL Is_MissionsAtCoords_Mission_Only_For_Cutscene_Triggering(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_FOR_CUTSCENE_ONLY))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the corona is setup purely to trigger specifically a Heist mid-strand mocap cutscene
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the corona is being only used to trigger a Heist Mid-Strand cutscene, otherwise FALSE
FUNC BOOL Is_MissionsAtCoords_Mission_For_Heist_MidStrand_Cutscene(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_HEIST_MIDSTRAND_MOCAP))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the corona is setup to trigger a Heist Tutorial mocap cutscene (allowing the Prep Mission to trigger immediately afterwards)
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the corona is being only used to trigger a Heist Tutorial cutscene, otherwise FALSE
FUNC BOOL Is_MissionsAtCoords_Mission_For_Heist_Tutorial_Cutscene(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_HEIST_TUTORIAL_MOCAP))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player should host their own instance of the corona
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the player should host their own instance of the corona, otherwise FALSE
FUNC BOOL Is_MissionsAtCoords_Mission_Hosting_Own_Instance(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_HOST_OWN_INSTANCE))
ENDFUNC




// ===========================================================================================================
//      Mission Launch Restriction routines
// ===========================================================================================================

// PURPOSE:	Check if a mission is Locked
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
//						paramGiveOutput		[DEFAULT = TRUE] TRUE to Display console log output, FALSE if no output
// RETURN VALUE:		BOOL				TRUE if the mission is Locked, otherwise FALSE
FUNC BOOL Does_MissionsAtCoords_Lock_Restriction_Exist(INT paramSlot, BOOL paramGiveOutput = TRUE)

	IF (Is_MissionsAtCoords_Mission_Locked(paramSlot))
	//If we've not accepted an invite and we're not on the second job of a playlist
	// KGM 16/1/15 [BUG 2171096]: Also ensure not playing rounds
	AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_SP()
	AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
	AND NOT HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
	AND NOT (IS_THIS_A_ROUNDS_MISSION())
		IF (paramGiveOutput)
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Lock Restriction? ")
				PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
				PRINTSTRING(". Var: ")
				PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
				PRINTSTRING(" - RESTRICTION: Mission Locked")
				MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
				IF (g_sAtCoordsControlsMP.matccPlayerRank < Get_Rank_Required_For_MP_Mission_Request(theMissionIdData))
					PRINTSTRING(" [Rank Too Low]")
				ELSE
					PRINTSTRING(" [Other Reason (time of day?)]")
				ENDIF
				PRINTNL()
			#ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player can afford to play this Heist
//
// INPUT PARAMS:		paramSlot			The array position for the Heist within the Mission At Coords array
//						paramGiveOutput		TRUE to Display console log output, FLASE if no output (ie: for the QuickCheck variety of this command)
// RETURN VALUE:		BOOL				TRUE if the player can afford to play, otherwise FALSE
FUNC BOOL Check_MissionsAtCoords_Can_Afford_Heist(INT paramSlot, BOOL paramGiveOutput)

	// KGM 11/10/14: The Heist 'can afford' check is now done upfront in the Heist Controller, so ignore it here
	UNUSED_PARAMETER(paramSlot)
	UNUSED_PARAMETER(paramGiveOutput)
	
	RETURN TRUE


ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player can afford to play the mission taking the cost of a Heist into account
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
//						paramMissionID		The MissionID
//						paramSubtype		The Subtype
//						paramGiveOutput		TRUE to Display console log output, FLASE if no output (ie: for the QuickCheck variety of this command)
// RETURN VALUE:		BOOL				TRUE if the player can afford to play, otherwise FALSE
//
// NOTES:	ADDED 18/5/14 to deal with Heists separately. For non-Heists, still uses the original call: Check_If_Afford_Mission()
FUNC BOOL Check_MissionsAtCoords_Can_Afford_Mission(INT paramSlot, MP_MISSION paramMissionID, INT paramSubtype, BOOL paramGiveOutput)

	// TEMP: Unused variable while FEATURE_HEIST_PLANNING is being used
	UNUSED_PARAMETER(paramSlot)
	UNUSED_PARAMETER(paramGiveOutput)

	// Use a different check for Heists
	IF (paramMissionID = eFM_MISSION_CLOUD)
		IF (paramSubtype = FMMC_MISSION_TYPE_HEIST)
			RETURN (Check_MissionsAtCoords_Can_Afford_Heist(paramSlot, paramGiveOutput))
		ENDIF
	ENDIF
		// FEATURE_HEIST_PLANNING

	// Not a Heist, so use the original check
	RETURN (Check_If_Afford_Mission(paramMissionID, paramSubtype))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there is any internal restriction that would prevent a mission launch
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
//						paramGiveOutput		[DEFAULT = TRUE] TRUE to Display console log output, FLASE if no output (ie: for the QuickCheck variety of this command)
// RETURN PARAMS:		paramReason			The reason for the mission launch restriction
// RETURN VALUE:		BOOL				TRUE if a restriction exists, otherwise FALSE
//
// NOTES:	When setting up the external routine, this would immediately make the player walk out of the corona
//			Use QuickCheck_If_MissionsAtCoords_Mission_Launch_Restriction() just to get TRUE or FALSE with no reason
//			If player is accepting an Invite, then none of these restrictions should be enforced
FUNC BOOL Check_For_MissionsAtCoords_Mission_Launch_Restriction(INT paramSlot, g_eMatCLaunchFailReasonID &paramReason, BOOL paramGiveOutput = TRUE)
	
	// When Adding new restrictions make sure a Corrisponding Bitset is also set up. e.g. g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_WANTED
	
	BOOL		acceptingInvite	= Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot)
	MP_MISSION	theMissionID	= Get_MissionsAtCoords_Slot_MissionID(paramSlot)

	// Setup a default return value for the return parameter
	paramReason = MATCLFR_NONE

	// Wanted Level?
	IF (Does_MatC_Player_Have_Wanted_Level())
		IF (paramGiveOutput)
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Lock Restriction? ")
				PRINTSTRING(GET_MP_MISSION_NAME(theMissionID))
				PRINTSTRING(". Var: ")
				PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
				PRINTSTRING(" - RESTRICTION: Wanted Level.")
				PRINTNL()
			#ENDIF
		ENDIF
		
		// Ignore the restriction if the player is accepting an invite
		IF (acceptingInvite)
			IF (paramGiveOutput)
				#IF IS_DEBUG_BUILD
					PRINTSTRING("      - IGNORE RESTRICTION: Accepting Invite.")
					PRINTNL()
				#ENDIF
			ENDIF
		
			RETURN FALSE
		ENDIF
		
		paramReason = MATCLFR_WANTED_LEVEL
		
		RETURN TRUE
	ENDIF

	// Mission Locked?
	MP_MISSION_ID_DATA theMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	IF (Does_MissionsAtCoords_Lock_Restriction_Exist(paramSlot, paramGiveOutput))
		INT iMissionRank = Get_Rank_Required_For_MP_Mission_Request(theMissionIdData)
		IF (g_sAtCoordsControlsMP.matccPlayerRank < iMissionRank)
			PRINTLN(".KGM [At Coords] Check_For_MissionsAtCoords_Mission_Launch_Restriction - g_sAtCoordsControlsMP.matccPlayerRank = ", g_sAtCoordsControlsMP.matccPlayerRank)
			PRINTLN(".KGM [At Coords] Check_For_MissionsAtCoords_Mission_Launch_Restriction - Get_Rank_Required_For_MP_Mission_Request(theMissionIdData) = ", iMissionRank)
			PRINTLN(".KGM [At Coords] Check_For_MissionsAtCoords_Mission_Launch_Restriction - paramReason = MATCLFR_RANK_TOO_LOW for mission ", theMissionIdData.idCloudFilename)
			paramReason = MATCLFR_RANK_TOO_LOW
		ELSE
			// ...must be because the time of day is wrong
			paramReason = MATCLFR_WRONG_TIMEOFDAY
		ENDIF
		
		RETURN TRUE
	ENDIF

	// Can't afford it?
	// KGM 18/5/14: This now calls a Matc function that takes the cost of a Heist into account
	INT missionSubtype = Get_SubType_For_FM_Cloud_Loaded_Activity(theMissionIdData)
	IF NOT (Check_MissionsAtCoords_Can_Afford_Mission(paramSlot, theMissionID, missionSubtype, paramGiveOutput))
		IF (paramGiveOutput)
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Lock Restriction? ")
				PRINTSTRING(GET_MP_MISSION_NAME(theMissionID))
				PRINTSTRING(". Subtype: ") PRINTINT(missionSubtype)
				PRINTSTRING(". Var: ")
				PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
				PRINTSTRING(" - RESTRICTION: No Cash. $")
				PRINTINT(NETWORK_GET_VC_WALLET_BALANCE() + NETWORK_GET_VC_BANK_BALANCE())
				PRINTSTRING(" < $")
				PRINTINT(GET_FM_ACTIVITY_COST(Convert_MissionID_To_FM_Mission_Type(theMissionID), missionSubtype))
				PRINTNL()
			#ENDIF
		ENDIF
		
		// Ignore the restriction if the player is accepting an invite
		IF (acceptingInvite)
			IF (paramGiveOutput)
				#IF IS_DEBUG_BUILD
					PRINTSTRING("      - IGNORE RESTRICTION: Accepting Invite.")
					PRINTNL()
				#ENDIF
			ENDIF
		
			RETURN FALSE
		ENDIF
		
		paramReason = MATCLFR_NOT_ENOUGH_CASH
		
		RETURN TRUE
	ENDIF
	
	// Bounty set
//	IF GlobalServerBD_FM.currentBounties[NATIVE_TO_INT(PLAYER_ID())].bTargeted
//		IF (paramGiveOutput)
//			#IF IS_DEBUG_BUILD
//				PRINTSTRING(".KGM [At Coords]: Lock Restriction? ")
//				PRINTSTRING(GET_MP_MISSION_NAME(theMissionID))
//				PRINTSTRING(". Var: ")
//				PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
//				PRINTSTRING(" - RESTRICTION: Bounty.")
//				PRINTNL()
//			#ENDIF
//		ENDIF
//		
//		paramReason = MATCLFR_BOUNTY
//		
//		RETURN TRUE
//	ENDIF
	// No restrictions
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// Quick Check for a mission launch restriction that just returns TRUE or FALSE without a reason
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:		BOOL				TRUE if a restriction exists, otherwise FALSE
FUNC BOOL QuickCheck_If_MissionsAtCoords_Mission_Launch_Restriction(INT paramSlot)

	g_eMatCLaunchFailReasonID	ignoreReason	= MATCLFR_NONE
	BOOL						giveOutput		= FALSE
	
	IF NOT (Check_For_MissionsAtCoords_Mission_Launch_Restriction(paramSlot, ignoreReason, giveOutput))
		RETURN FALSE
	ENDIF
	
	// A restriction exists, if this is during a transition then assert but allow processing to continue
	IF NOT (IS_TRANSITION_SESSION_RESTARTING())
	AND NOT (HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM())
		RETURN TRUE
	ENDIF
	
	// Transition session is restarting, so assert but allow processing to continue to avoid getting stuck in skycam
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: QuickCheck. Launch Restriction.")
		PRINTNL()
		PRINTSTRING("     IGNORE: Transition session ")
		IF (IS_TRANSITION_SESSION_RESTARTING())
			PRINTSTRING("Restarting")
		ELSE
			PRINTSTRING("Restart/Random")
		ENDIF
		PRINTNL()
		// Extra details to uncomment if needed
//		PRINTSTRING("     RESTRICTION IS: ")
//		SWITCH (ignoreReason)
//			CASE MATCLFR_RANK_TOO_LOW		PRINTSTRING("Rank Too Low")						BREAK
//			CASE MATCLFR_WRONG_TIMEOFDAY	PRINTSTRING("Wrong TimeOfDay")					BREAK
//			CASE MATCLFR_WANTED_LEVEL		PRINTSTRING("Wanted Level")						BREAK
//			CASE MATCLFR_NOT_ENOUGH_CASH	PRINTSTRING("Not Enough Cash")					BREAK
//			CASE MATCLFR_NONE				PRINTSTRING("MATCLFR_NONE ???")					BREAK
//			DEFAULT							PRINTSTRING("UNKNOWN - ADD TO SWITCH STATEMENT")	BREAK
//		ENDSWITCH
//		PRINTNL()
//		SCRIPT_ASSERT("QuickCheck_If_MissionsAtCoords_Mission_Launch_Restriction() found launch restriction while transition session restarting. Ignoring. See console log for reason. Tell Keith")
	#ENDIF
	
	// Ignore restriction if transition session restarting
	RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      External Checking Routines
// ===========================================================================================================

// PURPOSE:	Check if there are any external reasons that should delay requesting a mission from the Mission Controller
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:		BOOL				TRUE if there is an external reason to delay player reservation, FALSE if reservation is allowed
FUNC BOOL Check_For_External_Reasons_To_Delay_Becoming_Focus_Mission(INT paramSlot)
	
	// Delay if the player is dead
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Delay Focus - Dead") PRINTNL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Delay if the player is still teleporting
	IF (IS_PLAYER_TELEPORT_ACTIVE())
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Delay Focus - Teleporting") PRINTNL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Delay if the player is still in skyswoop and a launch restriction exists
	IF (QuickCheck_If_MissionsAtCoords_Mission_Launch_Restriction(paramSlot))
		IF NOT (IS_SKYSWOOP_AT_GROUND())
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Delay Focus - Skyswooping Restriction") PRINTNL()
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	// KGM 16/11/14 {BUG 2123860]: Delay if the player has the camera app active
	IF (IS_CELLPHONE_CAMERA_IN_USE())
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Delay Focus - Using Camera") PRINTNL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	//If we're starting a quick match 
	IF AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH()
	//And we're still loading it
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
		//And it's not the mission we're loading is not the same as this corona one
		TEXT_LABEL_23 slotContentIDQm = GET_TRANSITION_SESSION_LOADED_FILE_NAME()
		TEXT_LABEL_23 slotContentID = Get_MissionsAtCoords_Slot_ContentID(paramSlot)
		IF NOT IS_STRING_NULL_OR_EMPTY(slotContentIDQm)
		AND ARE_STRINGS_EQUAL(slotContentIDQm, slotContentID)
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Delay Focus - AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH Restriction - slotContentIDQm != slotContentID") PRINTNL()
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Delay if the player is quitting the corona
	IF (IS_TRANSITION_SESSION_QUITING_CORONA())
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Delay Focus - Quitting Prev Corona") PRINTNL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// So we can cleanly swoop into the "play again" and "random" cornas - william.kennedy@rockstarnorth.com, 11/04/2013.
	IF (g_TransitionSessionNonResetVars.sPostMissionCleanupData.iPostCleanupReadyForPlayAgainSwoopDownStage = 1)
		RETURN FALSE
	ENDIF
	
	// Delay if the previous mission is still being cleaned up after the mission script has terminated
	IF (IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID()))
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Delay Focus - Post Mission Cleanup") PRINTNL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Check if thre has been an external request to prevent the mission from triggering (added for Tutorial so a cutscene could play)
	IF (Is_MissionsAtCoords_Mission_Marked_As_Do_Not_Trigger(paramSlot))
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Delay Focus - 'Do Not Trigger'")
			PRINTNL()
			PRINTSTRING("          ")
			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
			PRINTNL()
		#ENDIF
		
		RETURN TRUE
	ENDIF
		
	//If we're starting a quick match 
	IF AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH()
	//And we're still loading it
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
		//And it's not the mission we're loading is not the same as this corona one
		TEXT_LABEL_23 slotContentIDQm = GET_TRANSITION_SESSION_LOADED_FILE_NAME()
		TEXT_LABEL_23 slotContentID = Get_MissionsAtCoords_Slot_ContentID(paramSlot)
		IF NOT IS_STRING_NULL_OR_EMPTY(slotContentIDQm)
		AND ARE_STRINGS_EQUAL(slotContentIDQm, slotContentID)
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Delay Focus - AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH Restriction - slotContentIDQm != slotContentID") PRINTNL()
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	// No external reason to delay reservation attempt
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this mission is being restarted after transitioning from Invite to Betting screens
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission is restarting after transition, FALSE if not
FUNC BOOL Is_This_Mission_Restarting_During_Transition(INT paramSlot)

	IF NOT (IS_TRANSITION_SESSION_RESTARTING())
		RETURN FALSE
	ENDIF
	
	MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	
	// Check if this mission is the one being restarted
	TEXT_LABEL_31 cloudFilenameFromTransition	= GET_TRANSITION_SESSION_LOADED_FILE_NAME()
	TEXT_LABEL_31 thisFilename					= theMissionIdData.idCloudFilename
	
	IF NOT (ARE_STRINGS_EQUAL(cloudFilenameFromTransition, thisFilename))
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Force Focus. Slot: ")
		PRINTINT(paramSlot)
		PRINTSTRING(" - Mission Restarting After Transition: ")
		PRINTSTRING(cloudFilenameFromTransition)
		PRINTNL()
//		PRINTSTRING("          ")
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//		PRINTNL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC




// ===========================================================================================================
//      Mission At Coords Marked For Refresh Bitflag routines
// ===========================================================================================================

// PURPOSE:	Mark a mission as potentially being refreshed during a cloud reload
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Mark_MissionsAtCoords_Mission_For_Refresh(INT paramSlot)
	
	// Set the Mark For Refresh bitflag
	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MARKED_FOR_REFRESH)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Refresh. Slot: ")
		PRINTINT(paramSlot)
		PRINTNL()
//		PRINTSTRING("         ")
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Remove the Marked For Refresh bitflag from a mission
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Mark_MissionsAtCoords_Mission_As_Not_For_Refresh(INT paramSlot)
	
	// Clear the Mark For Refresh bitflag
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MARKED_FOR_REFRESH)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Not for Refresh. Slot: ")
		PRINTINT(paramSlot)
		PRINTNL()
//		PRINTSTRING("         ")
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission is Marked for Refresh
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission is Marked For Refresh, otherwise FALSE
FUNC BOOL Is_MissionsAtCoords_Mission_Marked_For_Refresh(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MARKED_FOR_REFRESH))
ENDFUNC




// ===========================================================================================================
//      Mission At Coords Marked For Delete Mission Bitflag routines
// ===========================================================================================================

// PURPOSE:	Mark a mission for deletion
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Mark_MissionsAtCoords_Mission_For_Delete(INT paramSlot)

	IF (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MARKED_FOR_DELETE))
		EXIT
	ENDIF
	
	// Set the Mark For Delete bitflag
	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MARKED_FOR_DELETE)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Mark For Delete.")
//		IF (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_DELETE_AFTER_FOCUS))
//			PRINTSTRING(" [This Mission has been marked as Delete After Being Focus Mission]")
//		ENDIF
//		PRINTSTRING(": ")
		PRINTNL()
		PRINTSTRING("         ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	INTENDED FOR USE UNDER SPECIFIC CIRCUMSTANCES ONLY: Unmark mission as 'for delete'
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Unmark_MissionsAtCoords_Mission_For_Delete(INT paramSlot)

	IF NOT (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MARKED_FOR_DELETE))
		EXIT
	ENDIF
	
	// Clear the Mark For Delete bitflag
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MARKED_FOR_DELETE)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Mark For Delete - NOW UNMARKED FOR DELETE (INTENDED FOR USE IN SPECIFIC SITUATIONS ONLY).")
		PRINTNL()
		PRINTSTRING("         ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission is marked for delete
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission is Marked For Delete, otherwise FALSE
FUNC BOOL Is_MissionsAtCoords_Mission_Marked_For_Delete(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MARKED_FOR_DELETE))
ENDFUNC




// ===========================================================================================================
//      Mission At Coords Awaiting Activation Bitflag routines
// ===========================================================================================================

// PURPOSE:	Check if a mission is awaiting activation
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission is Awaiting Activation, otherwise FALSE
FUNC BOOL Is_MissionsAtCoords_Mission_Awaiting_Activation(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_AWAITING_ACTIVATION))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set a mission as awaiting activation
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//
// NOTES:	This was added for Gang Attacks to prevent them being immediately re-triggered after being played - it requires something to activate them again
PROC Set_MissionsAtCoords_Mission_As_Awaiting_Activation(INT paramSlot)
	
	IF (Is_MissionsAtCoords_Mission_Awaiting_Activation(paramSlot))
		EXIT
	ENDIF
	
	// Set the Awaiting Activation bitflag
	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_AWAITING_ACTIVATION)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Await Activation. Slot: ")
		PRINTINT(paramSlot)
		PRINTNL()
//		PRINTSTRING("         ")
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set a mission as activated
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//
// NOTES:	This was added for Gang Attacks to prevent them being immediately re-triggered after being playedm - this re-activates them
PROC Set_MissionsAtCoords_Mission_As_Activated(INT paramSlot)
	
	IF NOT (Is_MissionsAtCoords_Mission_Awaiting_Activation(paramSlot))
		EXIT
	ENDIF
	
	// Clear the Awaiting Activation bitflag
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_AWAITING_ACTIVATION)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Activated. Slot: ")
		PRINTINT(paramSlot)
		PRINTNL()
//		PRINTSTRING("         ")
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//		PRINTNL()
	#ENDIF
	
ENDPROC




// ===========================================================================================================
//      Mission At Coords Registration ID routines
// ===========================================================================================================

// PURPOSE:	Returns a unique registration ID for a mission being controlled by this system
//
// RETURN VALUE:			INT			The Next RegistrationID
FUNC INT Generate_At_Coords_RegistrationID()
	
	// Get the next registration ID
	INT returnID = g_sAtCoordsControlsMP.matccNextRegID
	
	// Increment the ID for next time
	g_sAtCoordsControlsMP.matccNextRegID++
	
	// Return this generated ID
	RETURN (returnID)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the registrationID for a mission slot
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			INT					The RegistrationID for the mission
FUNC INT Get_MissionsAtCoords_RegistrationID_For_Mission(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcRegID)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Search the slots to find the specified registrationID and pass back the associated slotID
//
// INPUT PARAMS:			paramRegID			The RegistrationID being searched for
// RETURN VALUE:			INT					The SlotID containing the mission registered with this ID, or INVALID_MATC_SLOT
//
// NOTES:	The return value should be used immediately and not stored for later - the slot for a mission is not fixed.
FUNC INT Get_MissionsAtCoords_Current_Slot_From_RegistrationID(INT paramRegID)

	// Search the array looking for the mission with this registration ID
	INT tempLoop = 0
	REPEAT g_numAtCoordsMP tempLoop
		IF (g_sAtCoordsMP[tempLoop].matcRegID = paramRegID)
			// ...found it
			RETURN tempLoop
		ENDIF
	ENDREPEAT
	
	// ID doesn't exist
	RETURN INVALID_MATC_SLOT

ENDFUNC




// ===========================================================================================================
//      Mission At Coords Focus Mission Data routines
// ===========================================================================================================

// PURPOSE:	Set an InCorona Settle timeout and frame timeout
//
// INPUT PARAMS:		paramDelay			[DEFAULT = INCORONA_ACTIONS_SETTLE_DELAY_msec] The delay in msec to use
PROC Set_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout(INT paramDelay = INCORONA_ACTIONS_SETTLE_DELAY_msec)
	
	g_sAtCoordsFocusMP.focusInCoronaTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), paramDelay)
	g_sAtCoordsFocusMP_TU.focusFrameTimeout	= GET_FRAME_COUNT() + INCORONA_ACTIONS_CORONA_SETTLE_DELAY_frames

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: ")
		NET_PRINT_TIME()
		PRINTSTRING(" [Frame: ")
		PRINTINT(GET_FRAME_COUNT())
		PRINTSTRING("] Focus Mission Settle Timeout: ")
		PRINTINT(paramDelay)
		PRINTSTRING(" msec. ")
		PRINTINT(INCORONA_ACTIONS_CORONA_SETTLE_DELAY_frames)
		PRINTSTRING(" frames.")
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Force InCorona Settle timeout expiry
PROC Force_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout_Expiry()
	
	g_sAtCoordsFocusMP.focusInCoronaTimeout = GET_NETWORK_TIME()
	g_sAtCoordsFocusMP_TU.focusFrameTimeout	= 0

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Force Focus Mission Settle Timeout Expiry")
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if an InCorona Settle Timeout has expired
//
// RETURN VALUE:			BOOL				TRUE if the timeout has expired, otherwise FALSE
FUNC BOOL Has_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout_Expired()

	IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sAtCoordsFocusMP.focusInCoronaTimeout))
		RETURN FALSE
	ENDIF

	IF (GET_FRAME_COUNT() < g_sAtCoordsFocusMP_TU.focusFrameTimeout)
		RETURN FALSE
	ENDIF
	
	// Both have expired
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set an InCorona timeout to Check If Registered As a Shared Mission
PROC Set_MissionsAtCoords_InCorona_Focus_Mission_Check_Shared_Timeout()
	
	g_sAtCoordsFocusMP.focusCheckSharedTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), INCORONA_ACTIONS_CHECK_SHARED_DELAY_msec)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: ")
		NET_PRINT_TIME()
		PRINTSTRING("Focus Mission - Check Shared Timeout: ")
		PRINTSTRING(GET_TIME_AS_STRING(g_sAtCoordsFocusMP.focusCheckSharedTimeout))
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if an InCorona Timeout to Check If Registered As a Shared Mission has expired
//
// RETURN VALUE:			BOOL				TRUE if the timeout has expired, otherwise FALSE
FUNC BOOL Has_MissionsAtCoords_InCorona_Focus_Mission_Check_Shared_Timeout_Expired()
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sAtCoordsFocusMP.focusCheckSharedTimeout))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the InCorona Safety Timeout waiting for a reservation broadcast to get a reply
PROC Set_MissionsAtCoords_Focus_Mission_Reservation_Safety_Timeout()
	
	g_sAtCoordsFocusMP.focusReserveTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), MATC_RESERVATION_SAFETY_TIMEOUT_msec)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: ")
		NET_PRINT_TIME()
		PRINTSTRING("Set Reservation Safety Timeout: ")
		PRINTSTRING(GET_TIME_AS_STRING(g_sAtCoordsFocusMP.focusReserveTimeout))
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the InCorona Safety Timeout waiting for a reservation broadcast to get a reply
PROC Clear_MissionsAtCoords_Focus_Mission_Reservation_Safety_Timeout()
	
	g_sAtCoordsFocusMP.focusReserveTimeout = GET_NETWORK_TIME()

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: ")
		NET_PRINT_TIME()
		PRINTSTRING("Clear Reservation Safety Timeout")
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the InCorona Reservation Broadcast Safety Timeout has expired
//
// RETURN VALUE:			BOOL				TRUE if the timeout has expired, otherwise FALSE
FUNC BOOL Has_MissionsAtCoords_Focus_Mission_Reservation_Safety_Timeout_Expired()
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sAtCoordsFocusMP.focusReserveTimeout))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Waiting For Cloud Data Safety Timeout
PROC Set_MissionsAtCoords_Focus_Mission_Cloud_Data_Safety_Timeout()
	
	g_sAtCoordsFocusMP.focusCloudDataTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), MATC_WAITING_FOR_CLOUD_DATA_SAFETY_TIMEOUT_msec)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: ")
		NET_PRINT_TIME()
		PRINTSTRING("Set Cloud Data Ready Safety Timeout: ")
		PRINTSTRING(GET_TIME_AS_STRING(g_sAtCoordsFocusMP.focusCloudDataTimeout))
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Waiting For Cloud Data Safety Timeout
PROC Clear_MissionsAtCoords_Focus_Mission_Cloud_Data_Safety_Timeout()
	
	g_sAtCoordsFocusMP.focusCloudDataTimeout = GET_NETWORK_TIME()

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: ")
		NET_PRINT_TIME()
		PRINTSTRING("Clear Cloud Data Ready Safety Timeout")
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the InCorona Waiting For Cloud Data Safety Timeout has expired
//
// RETURN VALUE:			BOOL				TRUE if the timeout has expired, otherwise FALSE
FUNC BOOL Has_MissionsAtCoords_Focus_Mission_Cloud_Data_Safety_Timeout_Expired()
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sAtCoordsFocusMP.focusCloudDataTimeout))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Mission is being Played Again
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
FUNC BOOL Is_MissionsAtCoords_Mission_Being_Played_Again(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MISSION_BEING_PLAYED_AGAIN))
ENDFUNC





// ===========================================================================================================
//      Mission At Coords calls to Mission Shared
// ===========================================================================================================

// PURPOSE:	Check if the mission should share cloud-loaded header details with all players
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission should share the cloud-loaded header details with all palyers, otherwise FALSE if sharing is not required or if the data all comes from Mission Data
FUNC BOOL Should_MissionsAtCoords_Mission_Share_Cloud_Data(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_SHARE_CLOUD_DATA))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If required, register the player with the Mission Shared system
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Store_Shared_Missions_Data_For_Slot_If_Applicable(INT paramSlot)

	IF NOT (Should_MissionsAtCoords_Mission_Share_Cloud_Data(paramSlot))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: This mission does not share its cloud-loaded header data - marking 'registered as shared' as TRUE to prevent attempts at re-registration")
//			PRINTNL()
//		#ENDIF
		
		// The data doesn't need to be shared, so mark the 'shared Reg' flag as TRUE so that this function doesn't get called every few seconds
		g_sAtCoordsFocusMP.focusRegisteredAsShared	= TRUE
		
		EXIT
	ENDIF
	
	// KGM 14/9/14 [BUG 2018528]: Don't share the header data if the mission is marked for delete
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		PRINTLN(".KGM [At Coords]: Share Header Data - IGNORING: The Mission Has Been Marked For Delete so is about to be cleaned up")
		
		// The data doesn't need to be shared, so mark the 'shared Reg' flag as TRUE so that this function doesn't get called every few seconds
		g_sAtCoordsFocusMP.focusRegisteredAsShared	= TRUE
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Share Header Data") PRINTNL()
	#ENDIF

	// Prior to setting up the Shared Cloud Data, remove the local storage of the Shared Reg ID if it is no longer valid
	MP_MISSION_ID_DATA	theID				= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	VECTOR				theCoords			= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	BOOL				isCutsceneOnly		= Is_MissionsAtCoords_Mission_Only_For_Cutscene_Triggering(paramSlot)
	BOOL				heistMidStrandCut	= Is_MissionsAtCoords_Mission_For_Heist_MidStrand_Cutscene(paramSlot)
	BOOL				heistTutorialCut	= Is_MissionsAtCoords_Mission_For_Heist_Tutorial_Cutscene(paramSlot)
	
	IF NOT (Is_Cloud_Data_Stored_As_A_Shared_Activity(theID))
		g_sAtCoordsMP[paramSlot].matcMissionIdData.idSharedRegID	= ILLEGAL_SHARED_REG_ID
		
		// Get the details again
		theID = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
		
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING("         BUT FIRST: Clear out obsolete SharedRegID still stored with this mission's details") PRINTNL()
//		#ENDIF
	ENDIF
	
	Request_Setup_Missions_Shared_Cloud_Data(theID, g_sAtCoordsFocusMP.focusUniqueID, theCoords, isCutsceneOnly, heistMidStrandCut, heistTutorialCut)
	
	// Setup Timeout to see if it succeeded
	Set_MissionsAtCoords_InCorona_Focus_Mission_Check_Shared_Timeout()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request that the Missions Shared system unregisters the player
//
// INPUT PARAMS:		paramSlot				The array position for the mission within the Mission At Coords array
//						paramKeepGeneratedData	[DEFAULT = FALSE] TRUE if the Generated Data shouldn't be cleaned up, otherwise FALSE
//
// NOTE:	This won't remove the corona or the shared missions data, it will just unregister this player from the shared mission because the player is no longer actively in the corona
//			KGM 29/8/14: The KeepGeneratedData is part of fix for BUG 2004116 where LTS rounds on another player's UGC caused the generated data to get cleaned up on Play Again
PROC Cleanup_Shared_Missions_Data_For_Slot(INT paramSlot, BOOL paramKeepGeneratedData = FALSE)

	IF NOT (Should_MissionsAtCoords_Mission_Share_Cloud_Data(paramSlot))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Cleanup Shared Missions Data for Mission At Coords slot: ")
//			PRINTINT(paramSlot)
//			PRINTSTRING(" - NOT REQUIRED (activity doesn't share cloud data)")
//			PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Cleanup Shared Data. Slot: ")
		PRINTINT(paramSlot)
		PRINTSTRING(". MatcRegID: ")
		PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
		PRINTNL()
	#ENDIF

	// Request to unregister the player, and clear the Shared RegID flag
	MP_MISSION_ID_DATA	thisMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	Request_Cleanup_Missions_Shared_Cloud_Data(thisMissionIdData, g_sAtCoordsFocusMP.focusUniqueID)
	g_sAtCoordsMP[paramSlot].matcMissionIdData.idSharedRegID = ILLEGAL_SHARED_REG_ID
	
	// This may be the wrong place for this, but also cleanup the header data if it was Generated Cloud Header Data following a transition
	INT theVariation = thisMissionIdData.idVariation
	
	IF (Is_This_Generated_Cloud_Header_Data(theVariation))
		IF (paramKeepGeneratedData)
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [At Coords]: This used Generated Header Data but don't clean it up - still required for Play Again")
				Debug_Output_Generated_Header_Data()
			#ENDIF
		ELSE
			PRINTLN(".KGM [At Coords]: Also cleanup Generated Header Data")
			
			IF (g_sGeneratedCloudHeaderData.gchdInUse)
			AND (ARE_STRINGS_EQUAL(thisMissionIdData.idCloudFilename, g_sGeneratedCloudHeaderData.gchdHeaderData.tlName))
				Clear_Generated_Cloud_Header_Data()
			ELSE
				PRINTLN(".KGM [At Coords]: ...But Generated Header Data contentID is not for this mission, so leave as-is.")
			ENDIF
		ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the attempt to register with the Shared Mission succeeded - if not re-register
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Maintain_Attempt_Registration_With_Shared_Mission(INT paramSlot)

	// Nothing to do if registration has already been successful
	IF (g_sAtCoordsFocusMP.focusRegisteredAsShared)
		EXIT
	ENDIF
	
	// Nothing to do just now if timeout hasn't expired
	IF NOT (Has_MissionsAtCoords_InCorona_Focus_Mission_Check_Shared_Timeout_Expired())
		EXIT
	ENDIF
	
	// The timeout has expired
	// Possibly a redudant check in this function - but better to be safe
	IF NOT (Should_MissionsAtCoords_Mission_Share_Cloud_Data(paramSlot))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Maintain_Attempt_Registration_With_Shared_Mission for slot: ")
//			PRINTINT(paramSlot)
//			PRINTSTRING(" - NOT REQUIRED (activity doesn't share cloud data) - marking 'registered as shared' as TRUE to prevent attempts at re-registration")
//			PRINTNL()
//		#ENDIF
		
		// The data doesn't need to be shared, so mark the 'shared Reg' flag as TRUE so that this function doesn't get called every few seconds
		g_sAtCoordsFocusMP.focusRegisteredAsShared	= TRUE
		
		EXIT
	ENDIF
	
	// Check if the previous registration with the Shared Missions succeeded
	MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Header Data Shared? Slot: ")
		PRINTINT(paramSlot)
		PRINTSTRING(". ContentID: ")
		PRINTSTRING(theMissionIdData.idCloudFilename)
		PRINTSTRING(". UniqueID: ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
	#ENDIF
		
	IF (Is_Player_Registered_For_Shared_Cloud_Mission_With_These_Details(PLAYER_ID(), theMissionIdData.idCloudFilename, g_sAtCoordsFocusMP.focusUniqueID))
		// ...yes, recognised as registered
		#IF IS_DEBUG_BUILD
			PRINTSTRING(" - YES")
			PRINTNL()
		#ENDIF

		g_sAtCoordsFocusMP.focusRegisteredAsShared = TRUE
		
		EXIT
	ENDIF
	
	// Not recognised as registered, so attempt registration again and re-start the timer
	#IF IS_DEBUG_BUILD
		PRINTSTRING(" - NOT YET")
		PRINTNL()
	#ENDIF

	Store_Shared_Missions_Data_For_Slot_If_Applicable(paramSlot)
	Set_MissionsAtCoords_InCorona_Focus_Mission_Check_Shared_Timeout()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is being 'played again'
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:		BOOL				TRUE if the mission is being 'played again', FALSE if not
//
// NOTES:	This may be a bit dodgy if things change. The data I'm checking was intended for cross-transitional use but should still be hanging around here if not cross-transition.
FUNC BOOL Is_Copy_Of_Shared_Mission_Being_Played_Again(INT paramSlot)

	MP_MISSION_ID_DATA	thisMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: 'play again'? ")
	#ENDIF
	
	// Has the use of the Copy of the Shared Mision Data been set to 'play again'
	IF (g_eUseOfCopyRegSharedMissionData != COPY_MISSION_USE_IS_PLAY_AGAIN)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("NO.") PRINTNL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Is the copy mission the same as this mission?
	IF NOT (ARE_STRINGS_EQUAL(g_sCopyRegSharedMissionData.crsmdHeaderData.tlName, thisMissionIdData.idCloudFilename))
		#IF IS_DEBUG_BUILD
			PRINTSTRING(" NO - Filenames differ:") PRINTNL()
			PRINTSTRING("         Copy Filename   : ") PRINTSTRING(g_sCopyRegSharedMissionData.crsmdHeaderData.tlName) PRINTNL()
			PRINTSTRING("         Mission Filename: ") PRINTSTRING(thisMissionIdData.idCloudFilename) PRINTNL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// The mission has been chosen to be played again
	#IF IS_DEBUG_BUILD
		PRINTSTRING(" YES") PRINTNL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC




// ===========================================================================================================
//      Mission At Coords Slot Checking Routines
// ===========================================================================================================

// PURPOSE:	Check if a mission with matching details already exists within the Missions At Coords system
//
// INPUT PARAMS:		paramSourceID			The source of the request
//						paramCoords				The Mission Coords
//						paramMissionIdData		The MissionID Data
// REUTRN VALUE:		INT						The slot containing the matching data
//
// NOTES:	The variation for cloud-loaded missions is an array position and this may genuinely change during a cloud refresh, so don't match variation during refresh
//			Generated header data should match and replace missions that are marked for refresh - the data has been generated because the mission is no longer in the arrays
FUNC INT Find_Slot_For_Mission_with_Matching_Details(	g_eMPMissionSource		paramSourceID,
														VECTOR					paramCoords,
														MP_MISSION_ID_DATA		paramMissionIdData)

	// Go through all active slots searching for duplicate data
	INT tempLoop		= 0
	INT	contentIdHash	= 0
	IF NOT (IS_STRING_NULL_OR_EMPTY(paramMissionIdData.idCloudFilename))
		contentIdHash = GET_HASH_KEY(paramMissionIdData.idCloudFilename)
	ENDIF
	
	// Need to do a separate check for when the mission has a contentID has or not
	IF (contentIdHash != 0)
		// Mission has a contentID
		REPEAT g_numAtCoordsMP tempLoop
			// Ignore missions marked for delete
			IF NOT (Is_MissionsAtCoords_Mission_Marked_For_Delete(tempLoop))
				IF (g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = contentIdHash)
					// Found a matching filename
					// Do a string comparison to be sure
					IF (ARE_STRINGS_EQUAL(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename, paramMissionIdData.idCloudFilename))
						// ..definitely a match, so check generated header data against locally available data marked for refresh
						// Check if the passed-in header data is generated header data
						IF (paramMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
							// ...the new data is generated header data
							// KGM NOTE 2/9/14: All checks below are now being commented out as no longer necessary as new generated data should always match existing data.
							
//							// If the existing mission registration is marked for refresh then it is probably no longer locally available.
//							//		(if it was locally available then the game wouldn't have needed to generate the header data)
//							//		(KEITH 22/9/13: There is a flaw with this statement - Generated Header Data matching Shared Mission Data, updating checks below to cater for this)
//							IF (Is_MissionsAtCoords_Mission_Marked_For_Refresh(tempLoop))
//								// ...treat this as a match - the new generated data should replace the registered data that is marked for refresh and most likely about to be removed
//								// The external function will need to modify the Creator and the Variation and remove/set a couple of flags
//								RETURN tempLoop
//							ENDIF
//							
//							// If the existing data is Shared Mission Data then the Generated Data should replace it
//							IF (Is_MissionsAtCoords_Mission_A_Shared_Mission(tempLoop))
//								// ...treat as a match - the new generated data should replace the Shared Mission Data, because Generated Data is for content to be played
//								RETURN tempLoop
//							ENDIF
//							
//							// BUG 1980956: If the existing data is also Generated Data, then treat as a match
//							IF (g_sAtCoordsMP[tempLoop].matcMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
//								// ...treat as a match - both registration attempts are being set up as generated data
//								RETURN tempLoop
//							ENDIF
							
							// BUG 2004116: Generated Data (ie: Shared) isn't matching with locally available data - it should to ensure a second copy of the mission isn't registered.
							// KGM 2/9/14: NOTE: The check for this is: IF (g_sAtCoordsMP[tempLoop].matcMissionIdData.idVariation != GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
							//			which is the exact opposite of the check above which means generated data should always be treated as a match with existing data and the
							//			calling function can then decide what to do with it. Because of this, I'm commenting out all the other checks above this.
							RETURN temploop
						ELSE
							// ...the new data is not generated header data
							// If the existing mission registration is from generated header data then replace it with this real data
							IF (g_sAtCoordsMP[tempLoop].matcMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
								// ...treat this as a match - the registered data was generated data and should be replaced with this locally available data
								// The external function will need to modify the Creator and the Variation and remove/set a couple of flags
								RETURN tempLoop
							ENDIF
							
							// The existing data isn't generated data (and neither is the new data) - so treat as a match - the ccontentID should be enough to classify as a match
							RETURN tempLoop
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		// Not found
		RETURN INVALID_MATC_SLOT
	ENDIF
	
	// Mission doesn't have a contentID, so must be hardcoded therefore the variation won't change
	REPEAT g_numAtCoordsMP tempLoop
		// Ignore missions marked for delete
		IF NOT (Is_MissionsAtCoords_Mission_Marked_For_Delete(tempLoop))
			// Do full data comparison
			IF	(g_sAtCoordsMP[tempLoop].matcSourceID					= paramSourceID)
			AND	(g_sAtCoordsMP[tempLoop].matcMissionIdData.idMission	= paramMissionIdData.idMission)
			AND (g_sAtCoordsMP[tempLoop].matcMissionIdData.idVariation	= paramMissionIdData.idVariation)
				// Found a match so far, so check vectors
				IF (ARE_VECTORS_ALMOST_EQUAL(g_sAtCoordsMP[tempLoop].matcCoords, paramCoords))
					// Everything matches, so treat as a match
					RETURN tempLoop
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not found
	RETURN INVALID_MATC_SLOT

ENDFUNC




// ===========================================================================================================
//      Mission At Coords Corona Details Slot Access Routines
// ===========================================================================================================

// PURPOSE:	Retrieve the Corona Hud Colour associated with a slot
//
// RETURN VALUE:		VECTOR			The Corona Hud Colour for this slot
FUNC HUD_COLOURS Get_MissionsAtCoords_Slot_Corona_Hud_Colour(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcCorona.theCoronaColour)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the InCorona Text Hud Colour associated with a slot
//
// RETURN VALUE:		VECTOR			The InCorona text Hud Colour for this slot
FUNC HUD_COLOURS Get_MissionsAtCoords_Slot_InCorona_Text_Hud_Colour(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcCorona.theTextColour)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Corona Triggering Radius associated with a slot
//
// RETURN VALUE:		FLOAT			The Corona Trigger Radius for this slot
FUNC FLOAT Get_MissionsAtCoords_Slot_Corona_Trigger_Radius(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcCorona.triggerRadius)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Corona Icon associated with a slot
//
// RETURN VALUE:		eMP_TAG_SCRIPT	The Corona Icon for this slot
FUNC eMP_TAG_SCRIPT Get_MissionsAtCoords_Slot_Corona_Icon(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcCorona.theIcon)
ENDFUNC





// ===========================================================================================================
//      Mission At Coords Optional Setup Bitflag routines
// ===========================================================================================================

// PURPOSE:	Store all the options passed in with the Options Struct
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//							paramOptions		A struct containing the options required for this mission
PROC Store_MissionsAtCoords_Options(INT paramSlot, g_structMatCOptionsMP paramOptions)

	// MatC should handle the voting?
	IF (paramOptions.matcoHandleVoting)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_HANDLE_VOTING)
	ENDIF

	// Delete this mission after it has been played?
	IF (paramOptions.matcoDeleteOnPlay)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_DELETE_ON_PLAY)
	ENDIF

	// Delete this mission if it is no longer the focus mission?
	IF (paramOptions.matcoDeleteAfterFocus)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_DELETE_AFTER_FOCUS)
	ENDIF
	
	// Mission should await activation after being played?
	IF (paramOptions.matcoInactiveAfterPlay)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_AWAIT_ACTIVATION_AFTER_PLAY)
	ENDIF
	
	// Mission has a pre-mission cutscene (old CnC-style)?
	IF (paramOptions.matcoHasCutscene)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_HAS_CUTSCENE)
	ENDIF
	
	// Mission Blip should only appear when player nearby in an interior?
	IF (paramOptions.matcoInteriorBlipOnly)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_INTERIOR_BLIP_ONLY)
	ENDIF
	
	// Mission Blip should display on the minimap only, not the FrontEnd map?
	IF (paramOptions.matcoMinimapBlipOnly)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_MINIMAP_BLIP_ONLY)
	ENDIF
	
	// Mission should be marked as 'played'?
	IF (paramOptions.matcoHasBeenPlayed)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MISSION_HAS_BEEN_PLAYED)
	ENDIF
	
	// Mission should ignore 'cloud refresh' rules?
	IF (paramOptions.matcoIgnoreRefresh)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_IGNORE_REFRESH)
	ENDIF
	
	// Mission should share the cloud-loaded header details when the player enters the corona
	IF (paramOptions.matcoShareCloudData)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_SHARE_CLOUD_DATA)
	ENDIF
	
	// Mission should not display a blip?
	IF (paramOptions.matcoDisplayNoBlip)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_NO_BLIP_TO_BE_DISPLAYED)
	ENDIF
	
	// Mission should not display a corona?
	IF (paramOptions.matcoDisplayNoCorona)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_NO_CORONA_TO_BE_DISPLAYED)
	ENDIF
	
	// Mission should Quick Launch?
	IF (paramOptions.matcoQuickLaunch)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_QUICK_LAUNCH)
	ENDIF
	
	// Mission is Angled Area Triggered?
	IF (paramOptions.matcoUseAngledArea)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_AREA_TRIGGERED)
	ENDIF
	
	// Mission should Remain Open If Active?
	IF (paramOptions.matcoRemainOpenIfActive)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_REMAIN_OPEN_IF_ACTIVE)
	ENDIF
	
	// Mission should Launch Immediately (when unlocked)?
	IF (paramOptions.matcoLaunchImmediately)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_LAUNCH_IMMEDIATELY)
	ENDIF
	
	// Is mission Invite Only?
	IF (paramOptions.matcoInviteOnly)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_INVITE_ONLY)
	ENDIF

	// Unlock with Secondary Unlock of mission type?
	IF (paramOptions.matcoSecondaryUnlock)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_WAIT_FOR_SECONDARY_UNLOCK)
	ENDIF
	
	// Is mission being setup as a Shared Mission?
	IF (paramOptions.matcoSharedMission)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_ADDED_AS_SHARED_MISSION)
	ENDIF
	
	// Is mission being setup only as a way to trigger a mocap cutscene (ie: Heist pre-planning cutscene will be triggered through Heist Finale corona)?
	IF (paramOptions.matcoForCutsceneOnly)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_FOR_CUTSCENE_ONLY)
	ENDIF
	
	// In addition to 'FOR_CUTSCENE_ONLY', is this mission being setup to trigger specifically a heist mid-strand mocap (NOTE: Both this and the option above should be set)?
	IF (paramOptions.matcoHeistMidStrandCut)
		IF NOT (paramOptions.matcoForCutsceneOnly)
			PRINTLN(".KGM [At Coords][Heist]: Matc Corona Flag matcoHeistMidStrandCut set BUT matcoForCutsceneOnly is not set. Both should be set. Ignoring.")
			SCRIPT_ASSERT("Store_MissionsAtCoords_Options(): ERROR - Matc Corona Flag matcoHeistMidStrandCut set BUT matcoForCutsceneOnly is not set. Both should be set. Ignoring. Tell Keith.")
		ELSE
			SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_HEIST_MIDSTRAND_MOCAP)
		ENDIF
	ENDIF
	
	// In addition to 'FOR_CUTSCENE_ONLY', is this mission being setup to trigger specifically the Tutorial Cutscene (where the mission immediately follows)?
	IF (paramOptions.matcoHeistTutorialCuts)
		IF NOT (paramOptions.matcoForCutsceneOnly)
			PRINTLN(".KGM [At Coords][Heist]: Matc Corona Flag matcoHeistTutorialCuts set BUT matcoForCutsceneOnly is not set. Both should be set. Ignoring.")
			SCRIPT_ASSERT("Store_MissionsAtCoords_Options(): ERROR - Matc Corona Flag matcoHeistTutorialCuts set BUT matcoForCutsceneOnly is not set. Both should be set. Ignoring. Tell Keith.")
		ELSE
			SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_HEIST_TUTORIAL_MOCAP)
		ENDIF
	ENDIF
	
	// Should this player always host their own instance of the mission?
	IF (paramOptions.matcoHostOwnInstance)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_HOST_OWN_INSTANCE)
	ENDIF
	
	// Store the externalID indicating which script handles this mission's InCorona Actions
	g_sAtCoordsMP[paramSlot].matcExternalID = paramOptions.matcoInCoronaExternalID
	
	// By default, always lock new missions - this allows an immediate 'temporary unlock' to work, otherwise it gets ignored
	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_ACTIVITY_LOCKED)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission has a cutscene
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission has a cutscene, otherwise FALSE
FUNC BOOL Does_MissionsAtCoords_Mission_Have_Cutscene(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_HAS_CUTSCENE))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission should be deleted after it has been played
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission should be deleted after it has been played, otherwise FALSE
FUNC BOOL Should_MissionsAtCoords_Mission_Be_Deleted_After_Play(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_DELETE_ON_PLAY))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission should be deleted after it has been the focus mission
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission should be deleted after it has been the focus mission, otherwise FALSE
FUNC BOOL Should_MissionsAtCoords_Mission_Be_Deleted_After_Focus(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_DELETE_AFTER_FOCUS))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission should await activation after it has been played
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission should await activation after it has been played, otherwise FALSE
//
// NOTES:	If the mission should await activation it will be sent to sleep and need some form of trigger to re-awaken it
FUNC BOOL Should_MissionsAtCoords_Mission_Await_Activation_After_Play(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_AWAIT_ACTIVATION_AFTER_PLAY))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission requires Missions At Coords system to handle voting
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission requires MatC to handle voting, otherwise FALSE
FUNC BOOL Should_MissionsAtCoords_Handle_Voting_For_This_Mission(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_HANDLE_VOTING))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission blip should only be displayed when player nearby and in an interior
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the blip is an Interior Blip only, otherwise FALSE
FUNC BOOL Should_MissionsAtCoords_Mission_Blip_Display_In_Interior_Only(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_INTERIOR_BLIP_ONLY))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission blip should only be displayed on hte Minimap, not on the FrontEnd map
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the blip is a Minimap blip only, otherwise FALSE
FUNC BOOL Should_MissionsAtCoords_Mission_Blip_Display_On_Minimap_Only(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_MINIMAP_BLIP_ONLY))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission should ignore the usual cloud-refresh rules
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission should ignore the usual cloud-refresh rules, otherwise FALSE to obey them them
FUNC BOOL Should_MissionsAtCoords_Mission_Ignore_Cloud_Refresh(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_IGNORE_REFRESH))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission blip should not be displayed for this mission
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission requires a blip to be displayed, otherwise FALSE for no blip
FUNC BOOL Should_MissionsAtCoords_Mission_Display_A_Blip(INT paramSlot)
	IF (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_NO_BLIP_TO_BE_DISPLAYED))
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission corona should not be displayed for this mission
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission requires a corona to be displayed, otherwise FALSE for no corona
FUNC BOOL Should_MissionsAtCoords_Mission_Display_A_Corona(INT paramSlot)
	IF (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_NO_CORONA_TO_BE_DISPLAYED))
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission should Quick Launch (this won't reserve the player or use an incorona sequence or wait for 'start' - it will download cloud data then launch immediately)
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission should Quick Launch, otherwise FALSE to obey the usual mission launching sequence
FUNC BOOL Should_MissionsAtCoords_Mission_Quick_Launch(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_QUICK_LAUNCH))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission should Remain Open If Active (this will ignore opening and closing times - was added for Gang Attacks)
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission should Remain Open If Active, otherwise FALSE to obey the usual opening and closing hours
FUNC BOOL Should_MissionsAtCoords_Mission_Remain_Open_If_Active(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_REMAIN_OPEN_IF_ACTIVE))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is Area Triggered
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission should trigger when the player walks into an Area, otherwise FALSE to obey the usual mission launching sequence
FUNC BOOL Is_MissionsAtCoords_Mission_Area_Triggered(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_AREA_TRIGGERED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission should Launch Immediately when unlocked (this will act as through the player is in the corona, even if the player is far away from the corona)
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission should Launch Immediately when unlocked, otherwise FALSE to launch when the player reaches teh corona, as normal
FUNC BOOL Should_MissionsAtCoords_Mission_Launch_Immediately(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_LAUNCH_IMMEDIATELY))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is Invite Only (the mission won't be unlocked unless the player has accepted an invite)
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission in Invite Only, otherwise FALSE to also allow walk-ins
FUNC BOOL Is_MissionsAtCoords_Mission_Invite_Only(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_INVITE_ONLY))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission should use the Extended Mission Name In Corona display range (this was added for the tutorial camera shot)
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the extended mission name in corona display range should be used, FALSE for standard range
FUNC BOOL Should_MissionsAtCoords_Mission_Use_Extended_Name_Display_Range(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_USE_EXTENDED_NAME_RANGE))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this mission uses external functions to handle In-Corona Actions (ie: displaying menus, leaderboards, voting, etc)
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission requires MatC to handle voting, otherwise FALSE
FUNC BOOL Does_MissionsAtCoords_Use_External_InCorona_Actions(INT paramSlot)
	RETURN (Get_MissionsAtCoords_Slot_InCorona_ExternalID_To_Use(paramSlot) != MATCICE_NONE)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission should only unlock with the secondary unlock of its type
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission should only unlock with the secondary unlock, otherwise FALSE to unlock with the primary unlock
//
// NOTES:	This was added for Survival where we want two to open to the player initially, followed by the rest when one of those two has been played
FUNC BOOL Should_MissionsAtCoords_Mission_Unlock_With_Secondary_Unlock(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_WAIT_FOR_SECONDARY_UNLOCK))
ENDFUNC




// ===========================================================================================================
//      Mission At Coords Option Bitflag: Allow During Ambient Tutorial
// ===========================================================================================================

// PURPOSE:	Check if the mission is active and shows visuals during an ambient tutorial (this was added for the tutorial that unlocks races and deathmatches)
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the the mission is active during an ambient tutorial, FALSE if not
FUNC BOOL Is_MissionsAtCoords_Mission_Allowed_During_Ambient_Tutorial(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_ALLOW_DURING_AMBIENT_TUTORIAL))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If not already set, make this mission allowed during an ambient tutorial and increase the global counter
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_As_Allowed_During_Ambient_Tutorial(INT paramSlot)

	IF (Is_MissionsAtCoords_Mission_Allowed_During_Ambient_Tutorial(paramSlot))
		// ...already set, so do nothing
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: IGNORE: Set mission as allowed during ambient tutorial, but already set as allowed.")
//			PRINTNL()
//			PRINTSTRING("          ")
//			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//			PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF
		
	// Set as allowed during ambient tutorial
	SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_ALLOW_DURING_AMBIENT_TUTORIAL)
	
	// Increase the global counter
	g_iMatccCounterOnDuringAmbTut++
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Set - Mission Allowed During Ambient Tutorial. Total: ")
		PRINTINT(g_iMatccCounterOnDuringAmbTut)
		PRINTNL()
		PRINTSTRING("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF
				
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If not already clear, make this mission not allowed during an ambient tutorial and decrease the global counter
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_MissionsAtCoords_Mission_As_Allowed_During_Ambient_Tutorial(INT paramSlot)

	IF NOT (Is_MissionsAtCoords_Mission_Allowed_During_Ambient_Tutorial(paramSlot))
		// ...already clear, so do nothing
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: IGNORE: Clear mission as allowed during ambient tutorial, but already clear.")
//			PRINTNL()
//			PRINTSTRING("          ")
//			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//			PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF
		
	// Clear as allowed during ambient tutorial
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_ALLOW_DURING_AMBIENT_TUTORIAL)
	
	// Decrease the global counter
	IF (g_iMatccCounterOnDuringAmbTut > 0)
		g_iMatccCounterOnDuringAmbTut--
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: ERROR: Missions allowed during ambient tutorial already 0")
			PRINTNL()
			SCRIPT_ASSERT("Clear_MissionsAtCoords_Mission_As_Allowed_During_Ambient_Tutorial(): Trying to decrease g_iMatccCounterOnDuringAmbTut, but already 0. Tell Keith.")
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Clear - Mission allowed during ambient tutorial. Total: ")
		PRINTINT(g_iMatccCounterOnDuringAmbTut)
		PRINTNL()
		PRINTSTRING("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF
				
ENDPROC




// ===========================================================================================================
//      Mission At Coords Option Bitflag: Current Playlist Mission
// ===========================================================================================================

// PURPOSE:	Check if this mission is the currently active playlist mission
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if this mission is the currently active playlist mission, otherwise FALSE
FUNC BOOL Is_This_The_MissionsAtCoords_Currently_Active_Playlist_Activity(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_CURRENT_PLAYLIST_ACTIVITY))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that this is the currently active playlist mission
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Currently_Active_Playlist_Activity(INT paramSlot)

	IF (Is_This_The_MissionsAtCoords_Currently_Active_Playlist_Activity(paramSlot))
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [At Coords]: Set Active Playlist Mission. Slot: ", paramSlot, " - IGNORED. Already the next active playlist mission.")
		#ENDIF
		
		EXIT
	ENDIF

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_CURRENT_PLAYLIST_ACTIVITY)
	
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [At Coords]: Set Active Playlist Mission. Slot: ", paramSlot)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear this as the currently active playlist mission
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_MissionsAtCoords_Currently_Active_Playlist_Activity(INT paramSlot)

	IF NOT (Is_This_The_MissionsAtCoords_Currently_Active_Playlist_Activity(paramSlot))
		EXIT
	ENDIF

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_CURRENT_PLAYLIST_ACTIVITY)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Clear Active Playlist Mission. Slot: ") PRINTINT(paramSlot) PRINTNL()
	#ENDIF
	
ENDPROC




// ============================================================================================================
//      Mission At Coords Exception to allow one mission to trigger when all missions are blocked for FM Events
// ============================================================================================================

// PURPOSE:	Check if this mission is the current exception when all missions are blocked
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if this mission is the current exception when all other missions are blocked, otherwise FALSE
FUNC BOOL Is_This_The_MissionsAtCoords_Currently_Unblocked_Mission(INT paramSlot)

	// Are all missions blocked?
	IF NOT (g_matcBlockAndHideAllMissions)
		RETURN FALSE
	ENDIF
	
	// All missions are blocked
	// Is there an exception?
	IF (g_matcUnblockOneMissionHash = 0)
		RETURN FALSE
	ENDIF
	
	// There is an exception
	// Does the exception's timer still exist?
	IF (g_matcUnblockOneMissionTimeout = 0)
		RETURN FALSE
	ENDIF
	
	IF (GET_GAME_TIMER() > g_matcUnblockOneMissionTimeout)
		g_matcUnblockOneMissionHash = 0
		g_matcUnblockOneMissionTimeout = 0
		RETURN FALSE
	ENDIF
	
	// Timer hasn't expired, so check if this is the exception mission
	TEXT_LABEL_23 contentID = Get_MissionsAtCoords_Slot_ContentID(paramSlot)
	INT slotHashCID = GET_HASH_KEY(contentID)
	
	RETURN (slotHashCID = g_matcUnblockOneMissionHash)

ENDFUNC




// ===========================================================================================================
//      Mission At Coords Focus Mission access routines
// ===========================================================================================================

// PURPOSE:	Check if there is currently a focus mission
//
// RETURN VALUE:			BOOL				TRUE if there is a focus mission, FALSE if not
FUNC BOOL Is_There_A_MissionsAtCoords_Focus_Mission()
	RETURN (g_sAtCoordsBandsMP[MATCB_FOCUS_MISSION].matcbNumInBand > 0)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the slot for the focus mission
//
// RETURN VALUE:			INT					The Focus Mission Slot
//
// NOTES:	This should always be 0, but this fucntion prevents assumptions
FUNC INT Get_MissionsAtCoords_Focus_Mission_Slot()
	RETURN (Get_First_MissionsAtCoords_Slot_For_Band(MATCB_FOCUS_MISSION))
ENDFUNC




// ===========================================================================================================
//      Mission At Coords Stages Access Routines
// ===========================================================================================================

// PURPOSE:	Get the mission's stage
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			g_eMatCStages		The mission's current stage
FUNC g_eMatCStages Get_MissionsAtCoords_Stage_For_Mission(INT paramSlot)
	RETURN (g_sAtCoordsMP[paramSlot].matcStage)
ENDFUNC





// ===========================================================================================================
//      Mission At Coords External In-Corona Actions struct access routines
// ===========================================================================================================

// PURPOSE:	Check if the external corona routines are still in control
//
// INPUT PARAMS:		paramSlot		
// RETURN VALUE:		BOOL			TRUE if still processing, FALSE if they have stopped processing
//
// NOTES:	This is checking an external function directly, not an internal flag
FUNC BOOL Is_External_Corona_Routine_Still_In_Control_Of_Player(INT paramSlot)

	SWITCH (Get_MissionsAtCoords_Slot_InCorona_ExternalID_To_Use(paramSlot))
		CASE MATCICE_FMMC_LAUNCHER
			RETURN (IS_PLAYER_IN_CORONA())
			
		CASE MATCICE_FM_TUTORIAL
			// The tutorial script should always return TRUE because the player can't quit it
			RETURN TRUE
			
		CASE MATCICE_FM_CORONA_TUT
			// The FM Corona Tut script should always return TRUE because the player can't quit it
			RETURN TRUE
		
		CASE MATCICE_NONE
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Is_External_Corona_Routine_Still_In_Control_Of_Player() = MATCICE_NONE")
				PRINTNL()
			#ENDIF
			
			RETURN FALSE
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: ERROR: ExternalID Unknown.")
		PRINTNL()
		SCRIPT_ASSERT("Is_External_Corona_Routine_Still_In_Control_Of_Player(): ERROR - External ID is Unknown. Add to Switch. Tell Keith")
	#ENDIF

	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the External In-Corona Routines should Initialise
//
// RETURN VALUE:			BOOL				TRUE if the External In-Corona stage is INITIALISE, otherwise TRUE
FUNC BOOL Is_MissionsAtCoords_InCorona_Stage_Initialise()
	RETURN (g_sAtCoordsInCoronaMP.matcicInitialise)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the External In-Corona Routines should continue after mission reservation
//
// RETURN VALUE:			BOOL				TRUE if the External In-Corona routines can continue after mission reservation, otherwise TRUE
FUNC BOOL Is_MissionsAtCoords_InCorona_Stage_Player_Reserved()
	RETURN (g_sAtCoordsInCoronaMP.matcicPlayerReserved)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the External In-Corona Stage should Force Quit
//
// RETURN VALUE:			BOOL				TRUE if the External In-Corona stage is FORCE QUIT, otherwise TRUE
FUNC BOOL Should_MissionAtCoords_External_InCorona_Actions_Force_Quit()
	RETURN (g_sAtCoordsInCoronaMP.matcicForceQuit)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve a piece of help text to explain why there is a restriction
//
// REFERENCE_PARAM:		refShowCost			TRUE if the cost of the mission should be displayed, otherwise FALSE
// RETURN VALUE:		STRING				Text Label containing 'join restriction reason'
FUNC STRING Get_MissionsAtCoords_External_InCorona_Actions_Force_Quit_Help_Text(BOOL &refShowCost, BOOL isHeistMission)

	// Most activities don't need to Show Cost
	refShowCost = FALSE
		
	SWITCH (g_sAtCoordsInCoronaMP.matcicLaunchFailReason)
		CASE MATCLFR_RANK_TOO_LOW
			RETURN ("FMMC_TOO_LOW_LV")
			
		CASE MATCLFR_WRONG_TIMEOFDAY
			RETURN ("FMMC_WRONG_TIME")
			
		CASE MATCLFR_WANTED_LEVEL
			IF (g_sAtCoordsInCoronaMP.matcicMissionIdData.idCreator = FMMC_MINI_GAME_CREATOR_ID)
				RETURN ("FM_MG_WANTED_LEVEL")
			ENDIF
			RETURN ("FMMC_WANTED_LEVEL")
			
		CASE MATCLFR_NOT_ENOUGH_CASH
			refShowCost = TRUE
			IF (IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM())
				IF isHeistMission
					RETURN ("FMMC_NAECASH_HEIST_X")
				ELSE
					RETURN ("FMMC_NAECASH_X")
				ENDIF
			ELIF (IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM())
				IF isHeistMission
					RETURN ("FMMC_NAECASH_HEIST_P")
				ELSE
					RETURN ("FMMC_NAECASH_P")
				ENDIF
			ENDIF
			
			IF isHeistMission
				RETURN ("FMMC_NAECASH_HEIST")
			ENDIF
			
			RETURN ("FMMC_NAE_CASH")
			
			
//		CASE MATCLFR_BOUNTY
//			RETURN ("FMMC_NJ_BOUNTY")	
			
		CASE MATCLFR_NONE
			RETURN ("")
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: ERROR: Unknown matcicLaunchFailReason.") PRINTNL()
				SCRIPT_ASSERT("Get_MissionsAtCoords_External_InCorona_Actions_Force_Quit_Help_Text(): ERROR - Unknown Fail Reason. Add to Switch Statement. Tell Keith.")
			#ENDIF
			
			BREAK
	ENDSWITCH
	
	// No Reason
	RETURN ("")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the External In-Corona Actions as Initialised
PROC Set_MissionsAtCoords_InCorona_Stage_As_Initialised()

	// Safety Check: Current Stage should be 'Initialise'
	IF NOT (Is_MissionsAtCoords_InCorona_Stage_Initialise())
		PRINTSTRING(".KGM [At Coords]: External script - ")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING(". Set InCorona Actions INITIALISED, but not in INITIALISE state")
		PRINTNL()
		SCRIPT_ASSERT("FAILED: Current Stage should be INITIALISE when being informed the InCorona Actions have been Initialised. Tell Keith. Allowing processing to continue.")
	ENDIF

	// Clear Initialise
	g_sAtCoordsInCoronaMP.matcicInitialise	= FALSE
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: External script - ")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING(". InCorona Actions now INITIALISED. Wait for ALLOW RESERVATION.")
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the External In-Corona Stage to Allow Reservation
PROC Set_MissionsAtCoords_InCorona_Stage_As_Allow_Reservation()

	// Safety Check: Current Stage should not be 'Initialise'
	IF (Is_MissionsAtCoords_InCorona_Stage_Initialise())
		PRINTSTRING(".KGM [At Coords]: External script - ")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING(". Set InCorona Actions ALLOW RESERVATION, but expected INITIALISED")
		PRINTNL()
		SCRIPT_ASSERT("FAILED: Current Stage should not be INITIALISE in order to change to ALLOW RESERVATION. Tell Keith. Allowing processing to continue.")
	ENDIF

	// Clear Initialise, Set Allow Reservation
	g_sAtCoordsInCoronaMP.matcicInitialise		= FALSE
	g_sAtCoordsInCoronaMP.matcicAllowReserve	= TRUE
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: External script - ")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING(". InCorona Actions now ALLOW RESERVATION")
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the External In-Corona Stage is Allow Reservation
//
// RETURN VALUE:			BOOL				TRUE if the External In-Corona stage is ALLOW RESERVATION, otherwise FALSE
FUNC BOOL Is_MissionsAtCoords_InCorona_Stage_Allow_Reservation()
	RETURN (g_sAtCoordsInCoronaMP.matcicAllowReserve)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the External In-Corona Stage to Cloud_Data_Loaded
PROC Set_MissionsAtCoords_InCorona_Stage_As_Cloud_Data_Loaded()

	// Safety Check: Current Stage should not be 'Initialise'
	IF (Is_MissionsAtCoords_InCorona_Stage_Initialise())
		PRINTSTRING(".KGM [At Coords]: External script - ")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING(". Set InCorona Actions CLOUD DATA LOADED, but expected INITIALISED")
		PRINTNL()
		SCRIPT_ASSERT("FAILED: Current Stage should not be INITIALISE in order to change to CLOUD DATA LOADED. Tell Keith. Allowing processing to continue.")
	ENDIF

	// Safety Check: Current Stage should not be 'Allow Reservation'
	IF (Is_MissionsAtCoords_InCorona_Stage_Allow_Reservation())
		PRINTSTRING(".KGM [At Coords]: External script - ")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING(". Set InCorona Actions CLOUD DATA LOADED, but expected ALLOW RESERVATION")
		PRINTNL()
		SCRIPT_ASSERT("FAILED: Current Stage should not be ALLOW RESERVATION in order to change to CLOUD DATA LOADED. Tell Keith. Allowing processing to continue.")
	ENDIF

	// Clear Initialise, Set Cloud Data Loaded
	g_sAtCoordsInCoronaMP.matcicInitialise		= FALSE
	g_sAtCoordsInCoronaMP.matcicAllowReserve	= FALSE
	g_sAtCoordsInCoronaMP.matcicPlayerReserved	= FALSE
	g_sAtCoordsInCoronaMP.matcicCloudDataLoaded	= TRUE
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: External script ")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING(". InCorona Actions now CLOUD DATA LOADED")
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the External In-Corona Stage is Cloud Data Loaded
//
// RETURN VALUE:			BOOL				TRUE if the External In-Corona stage is CLOUD DATA LOADED, otherwise FALSE
FUNC BOOL Is_MissionsAtCoords_InCorona_Stage_Cloud_Data_Loaded()
	RETURN (g_sAtCoordsInCoronaMP.matcicCloudDataLoaded)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the External In-Corona routines should be doing any processing
//
// RETURN VALUE:			BOOL				TRUE if the External In-Corona stage should be doing any processing, otherwise FALSE
FUNC BOOL Should_External_Scripts_Process_MissionsAtCoords_InCorona_Actions()
	RETURN (g_sAtCoordsInCoronaMP.matcicDoProcessing)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the External Script as having found an error (usually during Cloud Data Download)
// NOTES:	KGM 15/1/15: The external script used to call Set_MissionsAtCoords_InCorona_Stage_As_Cloud_Data_Loaded() which allowed Matc processing to continue
//				which meant the error could be found in the next processing stage. Under extreme circumstances this can still produce problems so I've added
//				this explicit error condition. TEMPORARILY I will still also call 'cloud data loaded' and over time we should be able to plug the gaps caused
//				by extreme conditions by either:
//					Changing over the external script to call this function when an error is found
//					OR Change Matc to check for the error flag in appropriate situations and handle it better
PROC Set_MissionsAtCoords_InCorona_External_Script_Error()

	// Safety Check: Ignore if external scripts aren't expected to be processing
	IF NOT (Should_External_Scripts_Process_MissionsAtCoords_InCorona_Actions())
		PRINTSTRING(".KGM [At Coords]: External script - ")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING(". Set_MissionsAtCoords_InCorona_External_Script_Error(), but External Scripts are not expected to be processing. IGNORING.")
		PRINTNL()
		SCRIPT_ASSERT("Set_MissionsAtCoords_InCorona_External_Script_Error() - g_sAtCoordsInCoronaMP.matcicDoProcessing is FALSE. Tell Keith.")
		
		EXIT
	ENDIF
	
	// For now, don't set the 'external error' flag if not in the 'waiting for cloud data' stage - I'll assert so that I can investigate
	// This is purely a safety measure - I'm worried about this flag not being handled correctly, or being left set, etc
	BOOL allowExternalErrorFlag = FALSE
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		IF (Get_MissionsAtCoords_Stage_For_Mission(Get_MissionsAtCoords_Focus_Mission_Slot()) = MATCS_WAITING_FOR_CLOUD_DATA)
			allowExternalErrorFlag = TRUE
		ENDIF
	ENDIF

	// Set the error flag
	IF (allowExternalErrorFlag)
		g_sAtCoordsInCoronaMP.matcicExternalError = TRUE
		
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: External script ")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING(". Set_MissionsAtCoords_InCorona_External_Script_Error() called. Setting flag.")
			PRINTNL()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: External script ")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING(". Set_MissionsAtCoords_InCorona_External_Script_Error() called. IGNORE.")
			PRINTNL()
			PRINTSTRING(".KGM [At Coords]: ...Ignored because Focus Mission isn't at stage: MATCS_WAITING_FOR_CLOUD_DATA")
			PRINTNL()
			SCRIPT_ASSERT("Set_MissionsAtCoords_InCorona_External_Script_Error() called when Focus Mission not in MATCS_WAITING_FOR_CLOUD_DATA stage. Tell Keith.")
		#ENDIF
	ENDIF
	
	// TEMP: the old workaround behaviour was for the external InCorona actions to call Set_MissionsAtCoords_InCorona_Stage_As_Cloud_Data_Loaded()
	//			To ensure I don't introduce new problems with this change, I'm also going to call that function here and then tidy up any issues
	//			that still remain on a case by case basis - issues due to this are very rare.
	PRINTLN(".KGM [At Coords]: External script reported External Error - also calling the old workaround: 'cloud data loaded'")
	Set_MissionsAtCoords_InCorona_Stage_As_Cloud_Data_Loaded()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the External Script has reported an error (this is usually a cliud data download failure or whatever)
//
// RETURN VALUE:			BOOL				TRUE if the External script has reported an error, otherwise FALSE
FUNC BOOL Is_MissionsAtCoords_External_Script_Error_Reported()
	RETURN (g_sAtCoordsInCoronaMP.matcicExternalError)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the External Script has reported an error (this is usually a cliud data download failure or whatever)
PROC Clear_MissionsAtCoords_External_Error()
	IF NOT (Is_MissionsAtCoords_External_Script_Error_Reported())
		EXIT
	ENDIF
	
	PRINTLN(".KGM [At Coords]: Clear_MissionsAtCoords_External_Error() flag")
	g_sAtCoordsInCoronaMP.matcicExternalError = FALSE
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Allows The External Script to tell MatC it can Start the Mission (this probably means the external script has handled the player vote)
PROC Set_MissionsAtCoords_External_InCorona_Actions_To_Start_Mission()

	// Safety Check: Current Stage should be 'Cloud Data Loaded'
	IF NOT (Is_MissionsAtCoords_InCorona_Stage_Cloud_Data_Loaded())
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: External script - ")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING(". Set InCorona Actions START MISSION, but it should currently be CLOUD DATA LOADED")
			PRINTNL()
// KGM 4/7/13: The game handles this situation, and there can now be 2 START MISSIONS (probably an error, but not going to chase it) with the re-arrangement of the InCorona sequence
//				to handle the mission loading immediately after the transition while the bettig screen is also displayed. The console output above is warning enough.
//			SCRIPT_ASSERT("Set_MissionsAtCoords_External_InCorona_Actions_To_Start_Mission() - FAILED: Current Stage should be CLOUD DATA LOADED in order to change to START MISSION. Tell Keith. Allowing processing to continue.")
		#ENDIF
	ENDIF

	// Clear Working, Set Start Mission
	g_sAtCoordsInCoronaMP.matcicCloudDataLoaded	= FALSE
	g_sAtCoordsInCoronaMP.matcicStartMission	= TRUE
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: External script - ")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING(". InCorona Actions now START MISSION")
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the External In-Corona Stage is Terminate
//
// RETURN VALUE:			BOOL				TRUE if the External In-Corona stage is TERMINATE, otherwise TRUE
FUNC BOOL Has_MissionsAtCoords_InCorona_Actions_Received_Start_Mission()
	RETURN (g_sAtCoordsInCoronaMP.matcicStartMission)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if any In-Corona Stage flags are set
//
// RETURN VALUE:			BOOL				TRUE if any External In-Corona stage are set, otherwise FALSE
FUNC BOOL Are_MissionsAtCoords_InCorona_Stage_Flags_Set()

	IF (Is_MissionsAtCoords_InCorona_Stage_Initialise())
	OR (Is_MissionsAtCoords_InCorona_Stage_Cloud_Data_Loaded())
	OR (Should_MissionAtCoords_External_InCorona_Actions_Force_Quit())
	OR (Has_MissionsAtCoords_InCorona_Actions_Received_Start_Mission())
		RETURN TRUE
	ENDIF
	
	// No Flags Set
	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Get the ExternalID of the InCorona routine that should be getting processed
//
// RETURN VALUE:			g_eMatCInCoronaExternalID				The External In-Corona stage
FUNC g_eMatCInCoronaExternalID Get_MissionsAtCoords_InCorona_Actions_ExternalID_To_Process()
	RETURN (g_sAtCoordsInCoronaMP.matcicExternalID)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Mission Coords to use in the external InCorona routine
//
// RETURN VALUE:			VECTOR			The External In-Corona Coords to use
FUNC VECTOR Get_MissionsAtCoords_InCorona_Actions_Coords()
	RETURN (g_sAtCoordsInCoronaMP.matcicCoords)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the MissionsAtCoords RegistrationID being checked by the external InCorona routine
//
// RETURN VALUE:			INT				The MissionsAtCoords RegistrationID to use
FUNC INT Get_MissionsAtCoords_InCorona_Actions_RegID()
	RETURN (g_sAtCoordsInCoronaMP.matcicRegID)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the MissionsAtCoords MissionID Data to be used by the external InCorona routine
//
// RETURN VALUE:			MP_MISSION_ID_DATA		The MissionsAtCoords MissionID Data to use
FUNC MP_MISSION_ID_DATA Get_MissionsAtCoords_InCorona_Actions_MissionID_Data()
	RETURN (g_sAtCoordsInCoronaMP.matcicMissionIdData)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the MissionsAtCoords Focus Mission UniqueID to be used by the external InCorona routine
//
// RETURN VALUE:			INT									The MissionsAtCoords Focus Mission UniqueID
FUNC INT Get_MissionsAtCoords_InCorona_Actions_UniqueID()
	
	// External InCorona Actions should be processing
	IF NOT (Should_External_Scripts_Process_MissionsAtCoords_InCorona_Actions())
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: ERROR: NO_UNIQUE_ID returned to external script - don't process") PRINTNL()
//		#ENDIF
		
		RETURN NO_UNIQUE_ID
	ENDIF
	
	// Ensure the InCorona data contains a valid RegID
	IF (Get_MissionsAtCoords_InCorona_Actions_RegID() = ILLEGAL_AT_COORDS_ID)
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: ERROR: NO_UNIQUE_ID returned to external script - illegal RegID") PRINTNL()
//		#ENDIF
		
		RETURN NO_UNIQUE_ID
	ENDIF
	
	// Return the focus mission RegID
	RETURN (g_sAtCoordsFocusMP.focusUniqueID)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Host/Join screen should be used by the external InCorona routine
//
// RETURN VALUE:			BOOL		TRUE if the Host/Join screen should be used, otherwise FALSE
FUNC BOOL Get_MissionsAtCoords_InCorona_Actions_Should_Use_Host_Join()
	RETURN (g_sAtCoordsInCoronaMP.matcicOfferHostJoin)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this corona should be used by the external InCorona routine ONLY to trigger a mocap cutscene
//
// RETURN VALUE:			BOOL		TRUE if this corona should only be used to trigger a mocap cutscene, otherwise FALSE
FUNC BOOL Get_MissionsAtCoords_InCorona_Actions_Corona_Is_For_Cutscene()
	RETURN (g_sAtCoordsInCoronaMP.matcicForCutsceneOnly)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this corona should be used by the external InCorona routine ONLY to trigger a Heist Mid-Strand mocap cutscene
//
// RETURN VALUE:			BOOL		TRUE if this corona should only be used to trigger a heist mid-strand mocap cutscene, otherwise FALSE
FUNC BOOL Get_MissionsAtCoords_InCorona_Actions_Corona_Is_For_Heist_MidStrand_Cut()
	RETURN (g_sAtCoordsInCoronaMP.matcicHeistMidStrandCut)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this corona should be used by the external InCorona routine to trigger a Heist Tutorial mocap cutscene (followed by placing the player immediately on the mission)
//
// RETURN VALUE:			BOOL		TRUE if this corona should be used to trigger a heist Tutorial mocap cutscene, otherwise FALSE
FUNC BOOL Get_MissionsAtCoords_InCorona_Actions_Corona_Is_For_Heist_Tutorial_Cut()
	RETURN (g_sAtCoordsInCoronaMP.matcicHeistTutorialCut)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the External InCorona Force Quit flag
// NOTES:	This flag is used in two places for separate purposes so needs cleared if the game advances past use 1:
//				1) as the player walks into a corona to indicate the player should immediately walk out
//				2) if the mission request fails or becomes full so the local player should walk out
//				The tutorials ignore the first check so the flag can remain set incorrectly for the second check
PROC Clear_MissionsAtCoords_InCorona_Actions_Force_Quit()

	g_sAtCoordsInCoronaMP.matcicForceQuit 	= FALSE
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Clear InCorona Actions force quit")
		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Cleanup the External InCorona Actions
//
// INPUT PARAMS:		paramInternal		[DEFAULT = FALSE] TRUE if this script was called internally, FALSE if called externally
//
// NOTES:	This should be called by the external routine when it has finished performing InCorona actions
PROC Clear_MissionsAtCoords_InCorona_Actions_Variables(BOOL paramInternal = FALSE)

	g_sAtCoordsInCoronaMP.matcicDoProcessing 	= FALSE
	g_sAtCoordsInCoronaMP.matcicInitialise		= FALSE
	g_sAtCoordsInCoronaMP.matcicAllowReserve	= FALSE
	g_sAtCoordsInCoronaMP.matcicPlayerReserved	= FALSE
	g_sAtCoordsInCoronaMP.matcicCloudDataLoaded	= FALSE
	g_sAtCoordsInCoronaMP.matcicStartMission	= FALSE
	g_sAtCoordsInCoronaMP.matcicExternalError	= FALSE
	g_sAtCoordsInCoronaMP.matcicForceQuit		= FALSE
	
	IF (paramInternal)
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Cleanup InCorona Actions variables")
			PRINTNL()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: External script - ")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING(". Cleanup InCorona Actions variables")
			PRINTNL()
		#ENDIF
	ENDIF
	
ENDPROC




// ===========================================================================================================
//      Mission At Coords Player Current Activity routines
//		This gathers the details of the players current activity to avoid multiple external calls each frame
// ===========================================================================================================

// PURPOSE:	Gather and store the details about the player's current activity
PROC Gather_And_Store_Players_Current_Activity_Details()

	// Clear the details
	g_structMatCPlayerActivityMP emptyDetails
	g_sAtCoordsControlsMP.matccActivity = emptyDetails
	
	g_sAtCoordsControlsMP_AddTU.matccActivityContentID	= ""
	
	// If the player has a focus mission then treat the player as onActivity for this frame's update (even if the focus mission has just ended)
	// Let this fall through so that more details can be set if they are available
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		g_sAtCoordsControlsMP.matccActivity.matcadOnActivity = TRUE
	ENDIF
	
	// GATHER THE DETAILS
	// Is the player on an ambient activity?
// KGM 7/1/13: FM Crate Drop and Security Van both set this when they are available and it makes all activity blips disappear even though the players' aren't 'on' the ambient, so removing for now
//	IF IS_PLAYER_ON_ANY_MP_AMBIENT_SCRIPT(PLAYER_ID())
//		g_sAtCoordsControlsMP.matccActivity.matcadOnActivity	= TRUE
//		g_sAtCoordsControlsMP.matccActivity.matcadOnAmbient		= TRUE
//		EXIT
//	ENDIF
	
	// KGM TEMP 23/11/12: Need to block triggering if a non-MatC-triggered mission is running in FM
	// THIS CAN BE REMOVED ONCE ALL FREEMODE ACTIVITIES LAUNCH USING THE MISSION CONTROLLER (via MatC or not)
	// Let this fall through so that more details can be set if they are available
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		//-- Dave W 18/04/13: Don't do this check if player is on the intro mission, as otherwise there's a delay in the race corona
		//-- appearing when the tutorial race is unclocked.
		IF NOT IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GUN_INTRO)
			// ...a freemode-launched mission is running, so don't allow MatC to launch a mission
			g_sAtCoordsControlsMP.matccActivity.matcadOnActivity		= TRUE
		ENDIF
	ENDIF
	// END KGM TEMP
	
	// Is this player on a mission?
	IF NOT (Is_Player_On_Or_Triggering_Any_Mission())
		EXIT
	ENDIF
	
	// The player is on or triggering a mission
	// Gather more details about the mission being triggered
	MP_MISSION		thisMission		= eNULL_MISSION
	INT				thisVariation	= NO_MISSION_VARIATION
	TEXT_LABEL_23	thisContentID	= ""
	
	Get_Mission_Variation_ContentID_Being_Triggered(thisMission, thisVariation, thisContentID)
	
	IF (thisMission = eNULL_MISSION)
		EXIT
	ENDIF
	
	// Original variables in the struct
	g_sAtCoordsControlsMP.matccActivity.matcadOnActivity		= TRUE
	g_sAtCoordsControlsMP.matccActivity.matcadOnMission			= TRUE
	g_sAtCoordsControlsMP.matccActivity.matcadActivelyOnMission	= Is_Player_Actively_On_This_Mission(thisMission, thisVariation)
	g_sAtCoordsControlsMP.matccActivity.matcadMissionUniqueID	= Get_UniqueID_For_Mission_Being_Triggered()
	g_sAtCoordsControlsMP.matccActivity.matcadMissionID			= thisMission
	g_sAtCoordsControlsMP.matccActivity.matcadVariation			= thisVariation
	
	// Additional Variables in the struct
	g_sAtCoordsControlsMP_AddTU.matccActivityContentID			= thisContentID

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is on an activity based on the player's current stored details this frame
//
// RETURN VALUE:		BOOL			TRUE if the player is on an activity, otherwise FALSE
FUNC BOOL Is_MatC_Player_On_An_Activity()
	RETURN (g_sAtCoordsControlsMP.matccActivity.matcadOnActivity)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is on or triggering a mission with the specified uniqueID
//
// INPUT PARAMS:		paramUniqueID			The UniqueID being checked (this will likely be the focus missionID)
// RETURN VALUE:		BOOL					TRUE if the player is on or triggering the mission with the specified uniqueID, otherwise FALSE
FUNC BOOL Is_MatC_Player_On_Or_Triggering_A_Mission_With_This_UniqueID(INT paramUniqueID)

	IF NOT (g_sAtCoordsControlsMP.matccActivity.matcadOnMission)
		RETURN FALSE
	ENDIF
	
	IF NOT (g_sAtCoordsControlsMP.matccActivity.matcadMissionUniqueID = paramUniqueID)
		RETURN FALSE
	ENDIF
	
	// Player is on this mission
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is actively on a mission with the specified uniqueID
//
// INPUT PARAMS:		paramUniqueID			The UniqueID being checked (this will likely be the focus missionID)
// RETURN VALUE:		BOOL					TRUE if the player is actively on the mission with the specified uniqueID, otherwise FALSE
FUNC BOOL Is_MatC_Player_Actively_On_A_Mission_With_This_UniqueID(INT paramUniqueID)

	IF NOT (g_sAtCoordsControlsMP.matccActivity.matcadOnMission)
		RETURN FALSE
	ENDIF

	IF NOT (g_sAtCoordsControlsMP.matccActivity.matcadActivelyOnMission)
		RETURN FALSE
	ENDIF
	
	IF NOT (g_sAtCoordsControlsMP.matccActivity.matcadMissionUniqueID = paramUniqueID)
		RETURN FALSE
	ENDIF
	
	// Player is on this mission
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is on or triggering the mission variation in the specified slot
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the player is on or triggering the mission in the specified slot, otherwise FALSE
FUNC BOOL Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(INT paramSlot)

	IF NOT (g_sAtCoordsControlsMP.matccActivity.matcadOnMission)
		RETURN FALSE
	ENDIF
	
	// Check the missionID
	MP_MISSION	slotMissionID	= Get_MissionsAtCoords_Slot_MissionID(paramSlot)
	INT			slotVariation	= Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot)
	BOOL		usesCloudData	= Does_MP_Mission_Variation_Get_Data_From_The_Cloud(slotMissionID, slotVariation)
	
	IF (g_sAtCoordsControlsMP.matccActivity.matcadMissionID != slotMissionID)
		RETURN FALSE
	ENDIF
	
	// If cloud loaded then check the contentID, otherwise check the variation
	IF (usesCloudData)
		// Cloud-Loaded mission, so check the contentID
		TEXT_LABEL_23	slotContentID	= Get_MissionsAtCoords_Slot_ContentID(paramSlot)
		TEXT_LABEL_23	storedContentID	= g_sAtCoordsControlsMP_AddTU.matccActivityContentID
		
		IF (IS_STRING_NULL_OR_EMPTY(slotContentID))
		OR (IS_STRING_NULL_OR_EMPTY(storedContentID))
			RETURN FALSE
		ENDIF
		
		IF NOT (ARE_STRINGS_EQUAL(slotContentID, storedContentID))
			RETURN FALSE
		ENDIF
	ELSE
		// Not a cloud-loaded mission, so check the variation if it's important
		IF (slotVariation != NO_MISSION_VARIATION)
			IF (g_sAtCoordsControlsMP.matccActivity.matcadVariation != slotVariation)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Player is on this mission
	RETURN TRUE

ENDFUNC




// ===========================================================================================================
//      Player Moving Fast Access routines
// ===========================================================================================================

// PURPOSE:	Check if the player is moving fast on foot
//
// RETURN VALUE:		BOOL			TRUE if the player is moving fast on foot, otherwise FALSE
//
// NOTES:	When implemented, moving fast meant player running or sprinting (ignore player in vehicles)
FUNC BOOL Is_MatC_Player_Moving_Fast_On_Foot()
	//Ross W: Corona walk in camera now handles what speed the player arrives at and has them tasked accordingly.
	RETURN FALSE//(g_sAtCoordsControlsMP.matccPlayerMovingFast)
ENDFUNC




// ===========================================================================================================
//      Player In Interior Access routines
// ===========================================================================================================

// PURPOSE:	Check if the player is in an interior
//
// RETURN VALUE:		BOOL			TRUE if the player is in an interior, otherwise FALSE
FUNC BOOL Is_MatC_Player_In_Interior()
	RETURN (g_sAtCoordsControlsMP.matccPlayerInInterior)
ENDFUNC




// ===========================================================================================================
//      Corona Active TXD Access routines
// ===========================================================================================================

// PURPOSE:	Check if there is a defined Active Corona Texture - if not then the game isn't using an active corona
//
// RETURN VALUE:		BOOL			TRUE if there is an active corona texture, FALSE if not
FUNC BOOL Does_Game_Have_A_Matc_Corona_Active_TXD()

	IF (IS_STRING_NULL_OR_EMPTY(MATC_CORONA_ACTIVE_TXD_NAME))
	OR (IS_STRING_NULL_OR_EMPTY(MATC_CORONA_ACTIVE_TEXTURE_NAME))
		// ...texture not defined
		RETURN FALSE
	ENDIF
	// TXD and Texture defined
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the active corona TXD has loaded (ie: the spinning texture)
//
// RETURN VALUE:		BOOL			TRUE if the active corona TXD has loaded, otherwise FALSE
FUNC BOOL Is_MatC_Corona_Active_TXD_Loaded()
	RETURN (g_sAtCoordsControlsMP.matccCoronaActiveTXDLoaded)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the active corona TXD has loaded (ie: the spinning texture)
PROC Set_MatC_Corona_Active_TXD_Has_Loaded()
	
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: TXD available for use") PRINTNL()
//		
//		IF NOT (Does_Game_Have_A_Matc_Corona_Active_TXD())
//			PRINTSTRING("      BUT: Game isn't using an Active Corona Texture") PRINTNL()
//			SCRIPT_ASSERT("Set_MatC_Corona_Active_TXD_Has_Loaded() - ERROR: Game isn't using an active texture, so this function should not get called.")
//			
//			EXIT
//		ENDIF
//	#ENDIF

	g_sAtCoordsControlsMP.matccCoronaActiveTXDLoaded = TRUE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the active corona TXD is not loaded (ie: the spinning texture)
PROC Clear_MatC_Corona_Active_TXD_Has_Loaded()
	
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Clear_MatC_Corona_Active_TXD_Has_Loaded() - TXD not available for use") PRINTNL()
//		
//		IF NOT (Does_Game_Have_A_Matc_Corona_Active_TXD())
//			PRINTSTRING("      BUT: Game isn't using an Active Corona Texture") PRINTNL()
//			SCRIPT_ASSERT("Clear_MatC_Corona_Active_TXD_Has_Loaded() - ERROR: Game isn't using an active texture, so this function should not get called.")
//		ENDIF
//	#ENDIF

	g_sAtCoordsControlsMP.matccCoronaActiveTXDLoaded = FALSE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the active corona TXD is being requested (ie: the spinning texture)
//
// RETURN VALUE:		BOOL			TRUE if the active corona TXD is being requested, otherwise FALSE
FUNC BOOL Is_MatC_Corona_Active_TXD_Being_Requested()
	RETURN (g_sAtCoordsControlsMP.matccCoronaActiveTXDRequested)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the active corona TXD is being requested (ie: the spinning texture)
PROC Set_MatC_Corona_Active_TXD_Is_Being_Requested()
	
//	#IF IS_DEBUG_BUILD
//		IF NOT (g_sAtCoordsControlsMP.matccCoronaActiveTXDRequested)
//			PRINTSTRING(".KGM [At Coords]: Set_MatC_Corona_Active_TXD_Is_Being_Requested()") PRINTNL()
//		ENDIF
//	
//		IF NOT (Does_Game_Have_A_Matc_Corona_Active_TXD())
//			SCRIPT_ASSERT("Set_MatC_Corona_Active_TXD_Is_Being_Requested() - ERROR: Game isn't using an active texture, so this function should not get called.")
//			
//			EXIT
//		ENDIF
//	#ENDIF

	g_sAtCoordsControlsMP.matccCoronaActiveTXDRequested = TRUE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the active corona TXD is not being requested
PROC Clear_MatC_Corona_Active_TXD_Is_Being_Requested()
	
//	#IF IS_DEBUG_BUILD
//		IF (g_sAtCoordsControlsMP.matccCoronaActiveTXDRequested)
//			PRINTSTRING(".KGM [At Coords]: Clear_MatC_Corona_Active_TXD_Is_Being_Requested()") PRINTNL()
//		ENDIF
//	
//		IF NOT (Does_Game_Have_A_Matc_Corona_Active_TXD())
//			SCRIPT_ASSERT("Clear_MatC_Corona_Active_TXD_Is_Being_Requested() - ERROR: Game isn't using an active texture, so this function should not get called.")
//		ENDIF
//	#ENDIF

	g_sAtCoordsControlsMP.matccCoronaActiveTXDRequested = FALSE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the active corona TXD is required by any corona
//
// RETURN VALUE:		BOOL			TRUE if the active corona TXD is required by any corona, otherwise FALSE
FUNC BOOL Is_MatC_Corona_Active_TXD_Required_By_Any_Corona()
	RETURN (g_sAtCoordsControlsMP.matccCoronaRequiresActiveTXD)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that a corona requires the active corona TXD
PROC Set_MatC_Corona_Requires_Active_TXD()
	
//	#IF IS_DEBUG_BUILD
//		IF NOT (Is_MatC_Corona_Active_TXD_Loaded())
//			PRINTSTRING(".KGM [At Coords]: Set_MatC_Corona_Requires_Active_TXD() - Corona Requires Active TXD") PRINTNL()
//		ENDIF
//	
//		IF NOT (Does_Game_Have_A_Matc_Corona_Active_TXD())
//			SCRIPT_ASSERT("Set_MatC_Corona_Requires_Active_TXD() - ERROR: Game isn't using an active texture, so this function should not get called.")
//			
//			EXIT
//		ENDIF
//	#ENDIF

	g_sAtCoordsControlsMP.matccCoronaRequiresActiveTXD = TRUE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the active corona TXD is not required by any corona
PROC Clear_MatC_Corona_Requires_Active_TXD()
	
//	#IF IS_DEBUG_BUILD
//		IF NOT (Does_Game_Have_A_Matc_Corona_Active_TXD())
//			SCRIPT_ASSERT("Clear_MatC_Corona_Requires_Active_TXD() - ERROR: Game isn't using an active texture, so this function should not get called.")
//		ENDIF
//	#ENDIF
	
	g_sAtCoordsControlsMP.matccCoronaRequiresActiveTXD = FALSE
	
ENDPROC




// ===========================================================================================================
//      Corona Ground Projection TXD Access routines
// ===========================================================================================================

// PURPOSE:	Check if the corona Ground Projection TXD has loaded
//
// RETURN VALUE:		BOOL			TRUE if the corona ground projection TXD has loaded, otherwise FALSE
FUNC BOOL Is_MatC_Corona_Ground_Projection_TXD_Loaded()
	RETURN (g_sAtCoordsControlsMP.matccCoronaGroundTXDLoaded)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the corona Ground Projection TXD has loaded
PROC Set_MatC_Corona_Ground_Projection_TXD_Has_Loaded()

	g_sAtCoordsControlsMP.matccCoronaGroundTXDLoaded = TRUE
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Ground TXD - loaded") PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the corona Ground Projection TXD is not loaded
PROC Clear_MatC_Corona_Ground_Projection_TXD_Has_Loaded()

	g_sAtCoordsControlsMP.matccCoronaGroundTXDLoaded = FALSE
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Ground TXD - unloaded") PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the corona Ground Projection TXD is being requested
//
// RETURN VALUE:		BOOL			TRUE if the corona Ground Projection TXD is being requested, otherwise FALSE
FUNC BOOL Is_MatC_Corona_Ground_Projection_TXD_Being_Requested()
	RETURN (g_sAtCoordsControlsMP.matccCoronaGroundTXDRequested)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the corona Ground Projection TXD is being requested
PROC Set_MatC_Corona_Ground_Projection_TXD_Is_Being_Requested()
	
	#IF IS_DEBUG_BUILD
		IF NOT (g_sAtCoordsControlsMP.matccCoronaGroundTXDRequested)
			PRINTSTRING(".KGM [At Coords]: Ground TXD Requested") PRINTNL()
		ENDIF
	#ENDIF

	g_sAtCoordsControlsMP.matccCoronaGroundTXDRequested = TRUE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the corona Ground Projection TXD is not being requested
PROC Clear_MatC_Corona_Ground_Projection_TXD_Is_Being_Requested()
	
	#IF IS_DEBUG_BUILD
		IF (g_sAtCoordsControlsMP.matccCoronaGroundTXDRequested)
			PRINTSTRING(".KGM [At Coords]: Ground TXD Request Cleared") PRINTNL()
		ENDIF
	#ENDIF

	g_sAtCoordsControlsMP.matccCoronaGroundTXDRequested = FALSE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the unique Checkpoint Type being used for the Projection Details array postion
//
// INPUT PARAMS:			paramProjectionSlot			The Projection Array Position
// RETURN VALUE:			CHECKPOINT_TYPE				The unique Checkpoint Type being used within that projection array position
//
// NOTES:	This should be consistent with the Decal (function below)
FUNC CHECKPOINT_TYPE Get_MissionsAtCoords_Checkpoint_Type_For_Projection_ArrayPos(INT paramProjectionSlot)
	
	SWITCH (paramProjectionSlot)
		CASE 0		RETURN (CHECKPOINT_MP_LOCATE_1)
		CASE 1		RETURN (CHECKPOINT_MP_LOCATE_2)
		CASE 2		RETURN (CHECKPOINT_MP_LOCATE_3)
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Get_MissionsAtCoords_Checkpoint_Type_For_Projection_ArrayPos() - ERROR: Passed in Projection Array Slot needs added to SWITCH statement.") PRINTNL()
//		PRINTSTRING("      ArrayPos Passed-In: ") PRINTINT(paramProjectionSlot) PRINTNL()
//		PRINTSTRING("      Max Slots on array: ") PRINTINT(MATC_CORONA_MAX_PROJECTIONS) PRINTNL()
		SCRIPT_ASSERT("Get_MissionsAtCoords_Checkpoint_Type_For_Projection_ArrayPos(): ERROR - Passed-In ArrayPos needs data added to SWITCH statement. Check console log. Tell Keith.")
	#ENDIF
	
	RETURN (CHECKPOINT_PARACHUTE_LANDING)	// Arbitrary Checkpoint_type
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the unique Decal being used for the Projection Details array position
//
// INPUT PARAMS:			paramProjectionSlot			The Projection Array Position
// RETURN VALUE:			DECAL_RENDERSETTING_ID		The unique Decal being used within that projection array position
//
// NOTES:	This should be consistent with the Checkpoint Type (function above)
FUNC DECAL_RENDERSETTING_ID Get_MissionsAtCoords_Decal_For_Projection_ArrayPos(INT paramProjectionSlot)
	
	// NOTE: These Decals must match teh checkpoint types
	
	SWITCH (paramProjectionSlot)
		CASE 0		RETURN (DECAL_RSID_MP_LOCATE_1)
		CASE 1		RETURN (DECAL_RSID_MP_LOCATE_2)
		CASE 2		RETURN (DECAL_RSID_MP_LOCATE_3)
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Get_MissionsAtCoords_Decal_For_Projection_ArrayPos() - ERROR: Passed in Projection Array Slot needs added to SWITCH statement.") PRINTNL()
//		PRINTSTRING("      ArrayPos Passed-In: ") PRINTINT(paramProjectionSlot) PRINTNL()
//		PRINTSTRING("      Max Slots on array: ") PRINTINT(MATC_CORONA_MAX_PROJECTIONS) PRINTNL()
		SCRIPT_ASSERT("Get_MissionsAtCoords_Decal_For_Projection_ArrayPos(): ERROR - Passed-In ArrayPos needs data added to SWITCH statement. Check console log. Tell Keith.")
	#ENDIF
	
	RETURN (DECAL_RSID_PARACHUTE_LANDING)	// Arbitrary Decal
	
ENDFUNC




// ===========================================================================================================
//      Mission At Coords IN CORONA routines
//		Controls any actions taking place while the player is in the corona waiting to start an activity
//		- this can include voting if being handled by the Missions At Coords system
// ===========================================================================================================

// PURPOSE:	Keeps the player's active vote going to prevent auto-cleanup
// NOTES:	Uses a new temp flag rather than the 'dont clean up' flag because the 'dont clean up' flag is maintained by flow controller and if set by this function
//				(maintained by CnC or freemode.sc) was getting cleared up by flow controller prior to being acted on. I've updated the flow controller maintenance
//				routine to keep the vote going if either the original 'dont clean up' flag is set or this new temp flag is set.
//			This is CnC-specific, so it won't work in Freemode.
//			POSSIBLY TEMP: A re-write of the voting system will hopefully get rid of the need for this flag
PROC Keep_My_MissionsAtCoords_Vote_Ongoing()
	sCNCFlowVoteStruct.kgmTemp_KeepVoteActive = TRUE
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the flag that keeps the player's vote active to allow auto-cleanup if is doesn't get set again
// NOTES:	This is CnC-specific, so it won't work in Freemode.
//			POSSIBLY TEMP: A re-write of the voting system will hopefully get rid of the need for this flag
PROC Clear_My_MissionsAtCoords_Vote_KeepActive_Flag()
	sCNCFlowVoteStruct.kgmTemp_KeepVoteActive = FALSE
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The player is now standing in a corona and this activity uses external InCorona Actions, so tell the external script to initialise them
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_External_InCorona_Actions_To_Initialise(INT paramSlot)

	// Assert if the handshaking struct is already active
	IF (g_sAtCoordsInCoronaMP.matcicDoProcessing)
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: ERROR: Set Incorona Actions Initialise - BUT already Do Processing.") PRINTNL()
			SCRIPT_ASSERT("Set_External_InCorona_Actions_To_Initialise(): ERROR - The InCorona Actions struct is already active. Tell Keith. NOTE: Walking away from the corona and back may fix this.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Safety check - there should be no active stage flags
	IF (Are_MissionsAtCoords_InCorona_Stage_Flags_Set())
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: ERROR: Set Incorona Actions Initialise - BUT these InCorona Actions flags set: ")
			PRINTSTRING(Convert_MissionsAtCoords_InCorona_Stage_Flags_To_Strings())
			PRINTNL()
			
			SCRIPT_ASSERT("Set_External_InCorona_Actions_To_Initialise(): ERROR - The InCorona Actions stage should be Inactive. Look at Console Logs. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Clear the handshaking struct
	g_structMatCInCoronaMP emptyInCoronaStruct
	g_sAtCoordsInCoronaMP = emptyInCoronaStruct
	
	// Set the ExternalID - this indicates which external routine should process this mission's InCorona Actions
	g_sAtCoordsInCoronaMP.matcicExternalID	= Get_MissionsAtCoords_Slot_InCorona_ExternalID_To_Use(paramSlot)
	
	// Set stage
	g_sAtCoordsInCoronaMP.matcicInitialise		= TRUE
	
	// Set the InCorona Actions as DoProcessing - this will allow the external routines to start processing
	g_sAtCoordsInCoronaMP.matcicDoProcessing	= TRUE
	
	// To be safe (although should be unneeded), set the handshaking flags as false
	g_sAtCoordsInCoronaMP.matcicAllowReserve	= FALSE
	g_sAtCoordsInCoronaMP.matcicPlayerReserved	= FALSE
	g_sAtCoordsInCoronaMP.matcicStartMission	= FALSE
	
	// Set the 'force quit' flag and fail reason as false
	g_sAtCoordsInCoronaMP.matcicForceQuit		= Check_For_MissionsAtCoords_Mission_Launch_Restriction(paramSlot, g_sAtCoordsInCoronaMP.matcicLaunchFailReason)
	
	// Store the details of the mission so that it can be identified by the external function
	g_sAtCoordsInCoronaMP.matcicCoords			= g_sAtCoordsMP[paramSlot].matcCoords
	g_sAtCoordsInCoronaMP.matcicRegID			= g_sAtCoordsMP[paramSlot].matcRegID
	g_sAtCoordsInCoronaMP.matcicMissionIdData	= g_sAtCoordsMP[paramSlot].matcMissionIdData
	
	// Should the Host/Join screen be displayed?
	g_sAtCoordsInCoronaMP.matcicOfferHostJoin	= TRUE
	IF (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
		g_sAtCoordsInCoronaMP.matcicOfferHostJoin = FALSE
	ENDIF
	
	// Is this being used to trigger a mocap cutscene only (ie: Heist Pre-Planning cutscene)
	// KGM 17/8/14: Additional flag if it is being used to trigger a Heist Mid-Strand cutscene
	// KGM 27/9/14: Additional flag if it is being used to trigger a Heist Tutorial cutscene
	g_sAtCoordsInCoronaMP.matcicForCutsceneOnly		= Is_MissionsAtCoords_Mission_Only_For_Cutscene_Triggering(paramSlot)
	g_sAtCoordsInCoronaMP.matcicHeistMidStrandCut	= Is_MissionsAtCoords_Mission_For_Heist_MidStrand_Cutscene(paramSlot)
	g_sAtCoordsInCoronaMP.matcicHeistTutorialCut	= Is_MissionsAtCoords_Mission_For_Heist_Tutorial_Cutscene(paramSlot)
	
	// KGM 25/2/14: TEMP FOR HEIST
	IF (Is_FM_Cloud_Loaded_Activity_A_Heist(g_sAtCoordsInCoronaMP.matcicMissionIdData))
	OR (Is_FM_Cloud_Loaded_Activity_Heist_Planning(g_sAtCoordsInCoronaMP.matcicMissionIdData))
		PRINTLN(".KGM [At Coords]: Set Offer Host/Join to FALSE for Heists and Heist Planning")
		g_sAtCoordsInCoronaMP.matcicOfferHostJoin = FALSE
	ENDIF
	
	// Some debug output
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Set InCorona Actions to INITIALISED and ALLOW EXTERNAL PROCESSING. RegID: ")
		PRINTINT(g_sAtCoordsInCoronaMP.matcicRegID)
		PRINTNL()
		PRINTSTRING(".KGM [At Coords]: ")
		IF (g_sAtCoordsInCoronaMP.matcicOfferHostJoin)
			PRINTSTRING("...Offer Host/Join")
		ELSE
			PRINTSTRING("...Host/Join not required")
		ENDIF
		IF (g_sAtCoordsInCoronaMP.matcicForCutsceneOnly)
			PRINTSTRING(" [FOR MOCAP CUTSCENE TRIGGERING ONLY]")
		ENDIF
		IF (g_sAtCoordsInCoronaMP.matcicHeistMidStrandCut)
			PRINTSTRING(" [HEIST MID-STRAND CUT]")
		ENDIF
		IF (g_sAtCoordsInCoronaMP.matcicHeistTutorialCut)
			PRINTSTRING(" [HEIST TUTORIAL CUT]")
		ENDIF
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The mission has now been reserved, so let the external InCorona Actions know that the rest of the corona sequence can continue
PROC Set_External_InCorona_Actions_To_Player_Reserved()

	// Assert if the handshaking struct is already active
	IF NOT (g_sAtCoordsInCoronaMP.matcicDoProcessing)
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Set_External_InCorona_Actions_To_Player_Reserved() - ERROR: The InCorona Actions struct is not set to Do Processing.") PRINTNL()
			SCRIPT_ASSERT("ERROR - The InCorona Actions struct is not active. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Clear the 'Allow Reserve' flag
	g_sAtCoordsInCoronaMP.matcicAllowReserve	= FALSE
	
	// Set the 'Player Reserved' flag
	g_sAtCoordsInCoronaMP.matcicPlayerReserved	= TRUE
	
	// Some debug output
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Set InCorona Actions to PLAYER RESERVED. RegID: ")
		PRINTINT(g_sAtCoordsInCoronaMP.matcicRegID)
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Tell the External routines to Force Quit the InCorona Actions
PROC Set_External_InCorona_Actions_To_Force_Quit()

	// Do nothing if not active
	IF NOT (g_sAtCoordsInCoronaMP.matcicDoProcessing)
		EXIT
	ENDIF

	// Set the 'Force Quit' flag
	g_sAtCoordsInCoronaMP.matcicForceQuit	= TRUE
	
	// Some debug output
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: MatC wants External InCorona Actions to FORCE QUIT")
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup any actions that need to take place while the player is standing in a corona - this may include voting
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Setup_InCorona_Actions(INT paramSlot)

	MP_MISSION	thisMission		= Get_MissionsAtCoords_Slot_MissionID(paramSlot)
	INT			thisVariation	= Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot)
		
//	#IF IS_DEBUG_BUILD
//		BOOL	hasAnySetupBeenDone = FALSE
//	#ENDIF
	
	IF (Should_MissionsAtCoords_Handle_Voting_For_This_Mission(paramSlot))
		// This activity needs the Missions At Coords system to handle the voting	
		INT			thisPlayerGBD	= NATIVE_TO_INT(PLAYER_ID())
		
		// TEMP: this will only work for CnC missions until the voting data is stored centrally
		IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
			#IF IS_DEBUG_BUILD
//				PRINTSTRING(".KGM [At Coords]: Setup_InCorona_Actions() ")
//				PRINTSTRING(GET_MP_MISSION_NAME(thisMission))
//				PRINTSTRING(" (Variation = ")
//				PRINTINT(thisVariation)
//				PRINTSTRING(") - ERROR: A non-CnC activity has requested MatC to handle the voting.")
//				PRINTNL()
				
				SCRIPT_ASSERT("Setup_InCorona_Actions() - ERROR: A non-CnC mission has requested the MissionsAtCoords System to Handle Voting. This won't work yet. Tell Keith.")
			#ENDIF
			
			EXIT
		ENDIF
		
		// Setup the vote
		Setup_My_Vote(thisPlayerGBD, thisMission, thisVariation)
		Keep_My_MissionsAtCoords_Vote_Ongoing()
		
//		#IF IS_DEBUG_BUILD
//			hasAnySetupBeenDone	= TRUE
//		#ENDIF
	ENDIF
	
	// TO DO: Add InCorona external action checks here (for Freemode)
	IF (Does_MissionsAtCoords_Use_External_InCorona_Actions(paramSlot))
		Set_External_InCorona_Actions_To_Initialise(paramSlot)
		
//		#IF IS_DEBUG_BUILD
//			hasAnySetupBeenDone	= TRUE
//		#ENDIF
	ENDIF

	// Additional Debug Output if no vote and no external actions are required
//	#IF IS_DEBUG_BUILD
//		IF NOT (hasAnySetupBeenDone)
//			PRINTSTRING(".KGM [At Coords]: Setup_InCorona_Actions() ")
//			PRINTSTRING(GET_MP_MISSION_NAME(thisMission))
//			PRINTSTRING(". Var: ")
//			PRINTINT(thisVariation)
//			PRINTSTRING(") - No Vote nor external actions needed.")
//			PRINTNL()
//		ENDIF
//	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Cancel any internal actions taking place while the player is standing in a corona - this may include voting
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//
// NOTES:	An Internal action is one controlled within MissionsAtCoords system
PROC Cleanup_Internal_InCorona_Actions(INT paramSlot)
	
	IF (Should_MissionsAtCoords_Handle_Voting_For_This_Mission(paramSlot))
		// This activity needs the Missions At Coords system to handle the voting	
		INT			thisPlayerGBD	= NATIVE_TO_INT(PLAYER_ID())
		
		// TEMP: this will only work for CnC missions until the voting data is stored centrally
		IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
			#IF IS_DEBUG_BUILD
//				MP_MISSION	thisMission		= Get_MissionsAtCoords_Slot_MissionID(paramSlot)
//				INT			thisVariation	= Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot)
//				
//				PRINTSTRING(".KGM [At Coords]: Cleanup_Internal_InCorona_Actions() ")
//				PRINTSTRING(GET_MP_MISSION_NAME(thisMission))
//				PRINTSTRING(" (Variation = ")
//				PRINTINT(thisVariation)
//				PRINTSTRING(") - ERROR: A non-CnC activity has requested MatC to handle the voting.")
//				PRINTNL()
				
				SCRIPT_ASSERT("Cleanup_Internal_InCorona_Actions() - ERROR: A non-CnC mission has requested the MissionsAtCoords System to Handle Voting. This won't work yet. Tell Keith.")
			#ENDIF
			
			EXIT
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Clear Internal InCorona Actions")
		#ENDIF
		
		// CleanUp the vote
		CLEAN_UP_MY_VOTE_VARIABLES(thisPlayerGBD)
	ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Cancel any internal and external actions taking place while the player is standing in a corona - this may include voting
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//
// NOTES:	An Internal action is one controlled within MissionsAtCoords system, an external one is controlled through an external routine
PROC Cancel_Internal_And_External_InCorona_Actions(INT paramSlot)

	// Internal
	Cleanup_Internal_InCorona_Actions(paramSlot)
	
	// External
	Set_External_InCorona_Actions_To_Force_Quit()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain any actions taking place while the player is standing in a corona - this may include voting
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Maintain_InCorona_Actions(INT paramSlot)
	
	IF (Should_MissionsAtCoords_Handle_Voting_For_This_Mission(paramSlot))
		// TEMP: this will only work for CnC missions until the voting data is stored centrally
		IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
			EXIT
		ENDIF
		
		// This activity needs the Missions At Coords system to handle the voting, so keep the vote going
		Keep_My_MissionsAtCoords_Vote_Ongoing()
	ENDIF
	
	// KGM 22/11/12: Currently not doing any maintenance for External InCorona Actions (it should just tick over) but this may be needed in future

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the activity is ready to start
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the activity is ready to start
FUNC BOOL Is_MissionsAtCoords_Activity_Ready_To_Start(INT paramSlot)
	
//	#IF IS_DEBUG_BUILD
//		MP_MISSION	thisMission		= Get_MissionsAtCoords_Slot_MissionID(paramSlot)
//		INT			thisVariation	= Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot)
//	#ENDIF
				
	IF (Should_MissionsAtCoords_Handle_Voting_For_This_Mission(paramSlot))
		// This activity needs the Missions At Coords system to handle the voting	
		INT			thisPlayerGBD	= NATIVE_TO_INT(PLAYER_ID())
		
		// TEMP: this will only work for CnC missions until the voting data is stored centrally
		IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
			#IF IS_DEBUG_BUILD
//				PRINTSTRING(".KGM [At Coords]: Is_MissionsAtCoords_Activity_Ready_To_Start() ")
//				PRINTSTRING(GET_MP_MISSION_NAME(thisMission))
//				PRINTSTRING(" (Variation = ")
//				PRINTINT(thisVariation)
//				PRINTSTRING(") - ERROR: A non-CnC activity has requested MatC to handle the voting. Always returning TRUE.")
//				PRINTNL()
				
				SCRIPT_ASSERT("Is_MissionsAtCoords_Activity_Ready_To_Start() - ERROR: A non-CnC mission has requested the MissionsAtCoords System to Handle Voting. This won't work yet. Always Returning TRUE. Tell Keith.")
			#ENDIF
			
			RETURN TRUE
		ENDIF
		
		// This activity needs the Missions At Coords system to handle the voting	
		RETURN (Has_My_Vote_Passed(thisPlayerGBD))
	ENDIF
	
	// TO DO: Add InCorona external actions checks here (for Freemode)
	IF (Does_MissionsAtCoords_Use_External_InCorona_Actions(paramSlot))
		RETURN (Has_MissionsAtCoords_InCorona_Actions_Received_Start_Mission())
	ENDIF
	
	// If it reaches here then return TRUE - the mission musn't need any form of vote
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Is_MissionsAtCoords_Activity_Ready_To_Start() ")
//		PRINTSTRING(GET_MP_MISSION_NAME(thisMission))
//		PRINTSTRING(" (Variation = ")
//		PRINTINT(thisVariation)
//		PRINTSTRING(") - Activity starts immediately (ie: no vote nor external action).")
//		PRINTNL()
//	#ENDIF
	
	RETURN TRUE

ENDFUNC




// ===========================================================================================================
//      Mission At Coords SLEEPING MISSIONS access routines
// ===========================================================================================================

// PURPOSE:	Check if the mission should be sleeping
FUNC BOOL Should_MissionsAtCoords_Mission_Be_Sleeping(INT paramSlot)

	// If the mission is awaiting activation then it is 'sleeping'
	IF (Is_MissionsAtCoords_Mission_Awaiting_Activation(paramSlot))
		RETURN TRUE
	ENDIF

	// No
	RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      Mission At Coords FORCE UPDATE access routines
// ===========================================================================================================

// PURPOSE:	Check if the mission is being forced to update
FUNC BOOL Should_MissionsAtCoords_Mission_Do_A_Forced_Update(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_FORCE_UPDATE))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Mission as requiring a FORCED UPDATE
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_As_Force_Update(INT paramSlot)

	IF (Should_MissionsAtCoords_Mission_Do_A_Forced_Update(paramSlot))
		// Mission is already set as force update, so ensure the overall flag is set too
		g_sAtCoordsControlsMP.matccForceUpdateRequested = TRUE
		
		EXIT
	ENDIF

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_FORCE_UPDATE)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Force Mission Update. ")
		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
		PRINTSTRING(". Var: ")
		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
		PRINTNL()
	#ENDIF
	
	// Set the general flag that indicates at least one mission has requested a forced update on the next frame
	g_sAtCoordsControlsMP.matccForceUpdateRequested = TRUE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Mission as requiring a FORCED UPDATE
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_MissionsAtCoords_Mission_As_Force_Update(INT paramSlot)

	IF NOT (Should_MissionsAtCoords_Mission_Do_A_Forced_Update(paramSlot))
		EXIT
	ENDIF

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_FORCE_UPDATE)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Clear Force Mission Update. ")
		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
		PRINTSTRING(". Var: ")
		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
		PRINTNL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Mission At Coords HIDDEN_BY_PI_MENU access routines
//		(KGM 12/5/15: BUG 2313555 - PI Menu Hide Job Blips Option)
// ===========================================================================================================

// PURPOSE:	Check if the mission is being hidden by a PI Menu Hide Blip setting
FUNC BOOL Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_BY_PI_MENU))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Mission as HIDDEN_BY_PI_MENU
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_As_Hidden_By_PI_Menu(INT paramSlot)

	IF (Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu(paramSlot))
		EXIT
	ENDIF

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_BY_PI_MENU)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: PI Menu Setting - Job Type Hidden. REGID: ")
		PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
		PRINTNL()
		PRINTSTRING("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Mission as HIDDEN_BY_PI_MENU
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_MissionsAtCoords_Mission_As_Hidden_By_PI_Menu(INT paramSlot)

	IF NOT (Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu(paramSlot))
		EXIT
	ENDIF

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_BY_PI_MENU)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: PI Menu Setting - Job Type Not Hidden. [REGID: ")
		PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
		PRINTSTRING("]")
		PRINTNL()
		PRINTSTRING("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Mission At Coords HIDDEN_BY_DISABLED_AREA access routines
//		(KGM 12/5/15: BUG 2313555 - PI Menu Hide Job Blips Option)
// ===========================================================================================================

// PURPOSE:	KGM 17/6/15 [BUG 2316703]: Check if the mission is being hidden by a Disabled Area (ie: a pickup area in an FM Event)
FUNC BOOL Is_MissionsAtCoords_Mission_Hidden_By_Disabled_Area(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_BY_DISABLED_AREA))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	KGM 17/6/15 [BUG 2316703]: Set the Mission as HIDDEN_BY_DISABLED_AREA
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_As_Hidden_By_Disabled_Area(INT paramSlot)

	IF (Is_MissionsAtCoords_Mission_Hidden_By_Disabled_Area(paramSlot))
		EXIT
	ENDIF

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_BY_DISABLED_AREA)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Disabled Area is set - Job Hidden. REGID: ")
		PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
		PRINTNL()
		PRINTSTRING("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	KGM 17/6/15 [BUG 2316703]: Clear the Mission as HIDDEN_BY_DISABLED_AREA
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_MissionsAtCoords_Mission_As_Hidden_By_Disabled_Area(INT paramSlot)

	IF NOT (Is_MissionsAtCoords_Mission_Hidden_By_Disabled_Area(paramSlot))
		EXIT
	ENDIF

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_BY_DISABLED_AREA)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Disabled Area is no set - Job Not Hidden. [REGID: ")
		PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
		PRINTSTRING("]")
		PRINTNL()
		PRINTSTRING("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Mission At Coords HIDDEN_GANG_ATTACK access routines
//		(KGM 22/7/15: BUG 2382702 - Gang Attack Specifically Hidden using Block_MissionsAtCoords_Specific_Gang_Attack)
// ===========================================================================================================

// PURPOSE:	KGM 22/7/15 [BUG 2382702]: Check if the Gang Attack is being specifically hidden (by an FM Event)
FUNC BOOL Is_MissionsAtCoords_Gang_Attack_Specifically_Hidden(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_GANG_ATTACK))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	KGM 22/7/15 [BUG 2382702]: Set Gang Attack as Specifically Hidden (HIDDEN_GANG_ATTACK)
//
// INPUT PARAMS:		paramSlot			The array position for the Gang Attack within the Mission At Coords array
PROC Set_MissionsAtCoords_Gang_Attack_As_Specifically_Hidden(INT paramSlot)

	// NOTE: This flag may be getting set directly from a duplicate function in net_missions_at_coords_public_checks.sch

	IF (Is_MissionsAtCoords_Gang_Attack_Specifically_Hidden(paramSlot))
		EXIT
	ENDIF

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_GANG_ATTACK)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Gang Attack Specifically Hidden. REGID: ")
		PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
		PRINTNL()
		PRINTSTRING("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	KGM 22/7/15 [BUG 2382702]: Clear the Mission as HIDDEN_GANG_ATTACK
//
// INPUT PARAMS:		paramSlot			The array position for the Gang Attack within the Mission At Coords array
PROC Clear_MissionsAtCoords_Gang_Attack_As_Specifically_Hidden(INT paramSlot)

	IF NOT (Is_MissionsAtCoords_Gang_Attack_Specifically_Hidden(paramSlot))
		EXIT
	ENDIF

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_GANG_ATTACK)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Gang Attack No Longer Specifically Hidden. [REGID: ")
		PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
		PRINTSTRING("]")
		PRINTNL()
		PRINTSTRING("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Mission At Coords HIDDEN_BY_OVERLAPPING_CORONA access routines
// ===========================================================================================================

// PURPOSE:	Check if the mission is being hidden by an overlapping corona
FUNC BOOL Is_MissionsAtCoords_Mission_Hidden_By_Overlapping_Corona(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_BY_OVERLAPPING_CORONA))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Mission as HIDDEN BY OVERLAPPING CORONA
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:		BOOL				TRUE if the flag got set, FALSE if it was already set - this is just being used to supply extra debug info
FUNC BOOL Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(INT paramSlot)

	IF (Is_MissionsAtCoords_Mission_Hidden_By_Overlapping_Corona(paramSlot))
		RETURN FALSE
	ENDIF

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_BY_OVERLAPPING_CORONA)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("       Overlapped Mission - Hide. REGID: ")
		PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
		PRINTNL()
		PRINTSTRING("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Mission as HIDDEN BY OVERLAPPING CORONA
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:		BOOL				TRUE if the flag got cleared, FALSE if it was already cleared - this is just being used to supply extra debug info
FUNC BOOL Clear_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(INT paramSlot)

	IF NOT (Is_MissionsAtCoords_Mission_Hidden_By_Overlapping_Corona(paramSlot))
		RETURN FALSE
	ENDIF

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_HIDDEN_BY_OVERLAPPING_CORONA)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("       Overlapped Mission - Not Hidden. [REGID: ")
		PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
		PRINTSTRING("]")
		PRINTNL()
		PRINTSTRING("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC




// ===========================================================================================================
//      Mission At Coords PLAYER FORCED ONTO MISSION access routines
// ===========================================================================================================

// PURPOSE:	Check if the player was forced onto the mission by the Mission Controller without using a corona
FUNC BOOL Was_Player_Forced_Onto_MissionsAtCoords_Mission(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_WAS_FORCED_ONTO_MISSION))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the player was forced onto the mission by the mission controller without using a corona
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_Player_Forced_Onto_MissionsAtCoords_Mission(INT paramSlot)

	IF (Was_Player_Forced_Onto_MissionsAtCoords_Mission(paramSlot))
		EXIT
	ENDIF

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_WAS_FORCED_ONTO_MISSION)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Forced onto Mission By Mission Controller") PRINTNL()
		PRINTSTRING("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear that the player was forced onto the mission by the mission controller without using a corona
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_Player_Forced_Onto_MissionsAtCoords_Mission(INT paramSlot)

	IF NOT (Was_Player_Forced_Onto_MissionsAtCoords_Mission(paramSlot))
		EXIT
	ENDIF

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_WAS_FORCED_ONTO_MISSION)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Forced onto Mission By Mission Controller - Clear. Slot ")
		PRINTINT(paramSlot)
		PRINTNL()
//		PRINTSTRING("          ")
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//		PRINTNL()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Mission At Coords PLAYED DURING SESSION access routines
// ===========================================================================================================


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Played During Session status as PLAYED
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_As_Played(INT paramSlot)

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MISSION_HAS_BEEN_PLAYED)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Mission Played - Set. ")
		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
		PRINTSTRING(" Var: ")
		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
		PRINTNL()
	#ENDIF
	
	// Also perform an instant update of the local cloud-loaded data so that the tick remains after transition
	// NOTE: This is because it takes time for the 'played' status to make it into the game from the cloud after it has been updated - approx 5 minutes
	MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	Instant_Local_Update_Of_Played_Status_For_FM_Cloud_Loaded_Activity(theMissionIdData)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Mission Played status as NOT PLAYED
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_As_Not_Played(INT paramSlot)

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MISSION_HAS_BEEN_PLAYED)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Mission Played - Clear. ")
		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
		PRINTSTRING(". Var: ")
		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
		PRINTNL()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Mission At Coords FLASH BLIP BITFLAG access routines
// ===========================================================================================================

// PURPOSE:	Get the Flash Blip status for this mission
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
FUNC BOOL Should_MissionsAtCoords_Blip_Flash(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_FLASH_BLIP))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Flash Blip status as FLASH
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Blip_As_Flash(INT paramSlot)
	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_FLASH_BLIP)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Flash Blip status as NO FLASH
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Blip_As_No_Flash(INT paramSlot)
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_FLASH_BLIP)
ENDPROC




// ===========================================================================================================
//      Mission At Coords TEMPORARILY UNLOCKED BITFLAG access routines
// ===========================================================================================================

// PURPOSE:	Check if the mission remains temporarily unlocked until played
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
FUNC BOOL Does_MissionsAtCoords_Mission_Remain_Temporarily_Unlocked_Until_Played(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_TEMP_UNLOCKED_UNTIL_PLAYED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Temporarily Unlocked status for this mission
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
FUNC BOOL Is_MissionsAtCoords_Mission_Temporarily_Unlocked(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_ACTIVITY_TEMP_UNLOCKED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Mission Temporarily Unlocked status as Temporarily Unlocked
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_As_Temporarily_Unlocked(INT paramSlot)

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_ACTIVITY_TEMP_UNLOCKED)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Temporarily Unlocked - Set. ")
		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
		PRINTSTRING(" Var: ")
		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Mission Temporarily Unlocked status as Temporarily Unlocked
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_As_Temporarily_Unlocked_Until_Played(INT paramSlot)

	// Also need to set the mission as temporarily unlocked to allow the standard functionality to work
	Set_MissionsAtCoords_Mission_As_Temporarily_Unlocked(paramSlot)

	// And set the additional parameter that keeps it unlocked until played
	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_TEMP_UNLOCKED_UNTIL_PLAYED)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Temporarily Unlocked Until Played - Set. ")
		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
		PRINTSTRING(" Var: ")
		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Mission as being Temporarily Unlocked Until Played
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_MissionsAtCoords_Mission_As_Temporarily_Unlocked_Until_Played(INT paramSlot)

	// Don't lift the temporary unlock if the mission is being played again
	IF (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
		EXIT
	ENDIF

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_TEMP_UNLOCKED_UNTIL_PLAYED)
	
	#IF IS_DEBUG_BUILD
		IF (Does_MissionsAtCoords_Mission_Remain_Temporarily_Unlocked_Until_Played(paramSlot))
			PRINTSTRING(".KGM [At Coords]: Temporarily Unlocked Until Played - Clear. ")
			PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
			PRINTSTRING(" Var: ")
			PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
			PRINTNL()
		ENDIF
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Mission Temporarily Unlocked status as Not Temporarily Unlocked unless there is a secondary restriction that keeps it unlocked
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(INT paramSlot)

	// Don't lift the temporary unlock if the mission is being played again
	IF (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
		EXIT
	ENDIF

	IF (Does_MissionsAtCoords_Mission_Remain_Temporarily_Unlocked_Until_Played(paramSlot))
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (Is_MissionsAtCoords_Mission_Temporarily_Unlocked(paramSlot))
			PRINTSTRING(".KGM [At Coords]: Temporarily Unlocked - Clear. ")
			PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
			PRINTSTRING(" Var: ")
			PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
			PRINTNL()
		ENDIF
	#ENDIF

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_ACTIVITY_TEMP_UNLOCKED)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Mission As Being Played Again
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_As_Being_Played_Again(INT paramSlot)

	// And set the additional parameter that keeps it unlocked until played
	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MISSION_BEING_PLAYED_AGAIN)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Play Again - Set. ")
		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
		PRINTSTRING(" Var: ")
		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Mission as being Played Again
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_MissionsAtCoords_Mission_As_Being_Played_Again(INT paramSlot)

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_MISSION_BEING_PLAYED_AGAIN)
	
	#IF IS_DEBUG_BUILD
		IF (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
		PRINTSTRING(".KGM [At Coords]: Play Again - Clear. ")
		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
		PRINTSTRING(" Var: ")
		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
		PRINTNL()
		ENDIF
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Mission At Coords HELP TEXT DISPLAYED FOR IN VEHICLE access routines
// ===========================================================================================================

// PURPOSE:	Get the In-Vehicle Help Text Displayed status for this mission
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
FUNC BOOL Is_MissionsAtCoords_Mission_In_Vehicle_Help_Text_Displayed(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_IN_VEHICLE_HELP_DISPLAYED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the In-Vehicle Help Text Displayed status as Displayed for this mission
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_In_Vehicle_Help_Text_As_Displayed(INT paramSlot)

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_IN_VEHICLE_HELP_DISPLAYED)
	
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Set_MissionsAtCoords_Mission_In_Vehicle_Help_Text_As_Displayed ( ")
//		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
//		PRINTSTRING(") (Variation = ")
//		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
//		PRINTSTRING(")")
//		PRINTNL()
//	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the In-Vehicle Help Text Displayed status as Not Displayed for this mission
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_In_Vehicle_Help_Text_As_Not_Displayed(INT paramSlot)

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_IN_VEHICLE_HELP_DISPLAYED)
	
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Set_MissionsAtCoords_Mission_In_Vehicle_Help_Text_As_Not_Displayed ( ")
//		PRINTSTRING(GET_MP_MISSION_NAME(Get_MissionsAtCoords_Slot_MissionID(paramSlot)))
//		PRINTSTRING(") (Variation = ")
//		PRINTINT(Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot))
//		PRINTSTRING(")")
//		PRINTNL()
//	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is in a vehicle, and maintain the help message
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:		BOOL				TRUE if the player is in, entering, or leaving a vehicle (in a corona), FALSE if on foot or injured
//
// NOTES:	After pressing triangle there seems to be some lead in where the player is NOT ON FOOT before ENTERING VEHICLE - same with exiting vehicle.
//				Because of this I'm going to check for NOT ON FOOT instead of IN VEHICLE, but if this gets set at other times there may be an inconsistency.
FUNC BOOL Check_For_In_Vehicle_And_Maintain_Help(INT paramSlot)

	// KGM 12/3/13: See NOTE - checking for NOT ON FOOT instead
	// KGM 11/1/15 [BUG 2160696]: Don't do this if a Part Two mission has loaded - it delays the launching of the part two until after the cutscene
	IF (HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
		PRINTLN(".KGM [At Coords]: IGNORE Check_For_In_Vehicle_And_Maintain_Help() because HAS_FIRST_STRAND_MISSION_BEEN_PASSED() is TRUE")
		RETURN FALSE
	ENDIF

	IF NOT (g_matcBlockAndHideAllMissions AND NOT Should_MissionAtCoords_Ignore_Block_On_Mission(paramSlot))
	AND NOT (IS_PED_INJURED(PLAYER_PED_ID()))
		IF NOT (IS_PED_ON_FOOT(PLAYER_PED_ID()))
			IF NOT (Is_MissionsAtCoords_Mission_In_Vehicle_Help_Text_Displayed(paramSlot))
				IF NOT (IS_HELP_MESSAGE_BEING_DISPLAYED())
					#IF IS_DEBUG_BUILD
						PRINTSTRING(".KGM [At Coords]: Delay Mission Reservation: Player is not on foot (or is entering car)") PRINTNL()
					#ENDIF
					
					// Only display this help text if the mission isn't overlapped
					// KGM 12/5/15: BUG 2313555 - Also don't display help text if mission blip hidden by PI Menu option
					// KGM 11/6/15: BUG 2316703 - Also hide if the mission is within a disabled area
					IF NOT (Is_MissionsAtCoords_Mission_Hidden_By_Overlapping_Corona(paramSlot))
					AND NOT (Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu(paramSlot))
					AND NOT (Is_MissionsAtCoords_Mission_Hidden_By_Disabled_Area(paramSlot))
						// Only display the help text if actually sitting in the vehicle
						IF (IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
							IF (Get_MissionsAtCoords_Slot_CreatorID(paramSlot) = FMMC_MINI_GAME_CREATOR_ID)
								PRINT_HELP("FM_MG_NAE_CAR")
							ELSE
								PRINT_HELP("FMMC_NAE_CAR")
							ENDIF
							
							Set_MissionsAtCoords_Mission_In_Vehicle_Help_Text_As_Displayed(paramSlot)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MATC_DPADRIGHT")
				AND (IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
					PRINTSTRING(".KGM [At Coords]: IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(\"MATC_DPADRIGHT\")") PRINTNL()
					CLEAR_HELP()
					Set_MissionsAtCoords_Mission_In_Vehicle_Help_Text_As_Not_Displayed(paramSlot)
					g_matcDpadRightHelpFrameTimeout	= 0
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Player is injured, or is not in, getting into, or getting out of a vehicle
	RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      Mission At Coords BLIP CONTROL routines
// ===========================================================================================================

// PURPOSE:	Check the Blip Details to ensure all the required info are valid
//
// INPUT PARAMS:		paramBlipDetails		The Blip Details struct
// RETURN VALUE:		BOOL					TRUE if the required details are valid, otherwise FALSE
FUNC BOOL Are_MissionsAtCoords_Blip_Details_Valid(g_structMatCBlipMP paramBlipDetails)

	// Check the Blip Sprite
	IF (paramBlipDetails.theBlipSprite = RADAR_TRACE_INVALID)
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING("      FAILED: Blip Sprite is RADAR_TRACE_INVALID.") PRINTNL()
//		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// All required details seem ok
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup all the blip details for a MissionsAtCoords mission, but don't display it - this will be handled elsewhere
//
// INPUT PARAMS:		paramBlipDetails			The Blip Details struct
//						paramCoords					The Blip Coords
//						paramAreaTriggered			TRUE if the mission is area-triggered, otherwise FALSE
// RETURN VALUE:		BLIP_INDEX					The Blip Index
//
// NOTES:	Gang Attack blips should use a radius blip - since they are the only area-triggered mission I've used that as the decision maker for now (for speed - saves adding a new option flag)
FUNC BLIP_INDEX Setup_MissionsAtCoords_Blip_Undisplayed(g_structMatCBlipMP paramBlipDetails, VECTOR paramCoords, BOOL paramAreaTriggered = FALSE)

	// Ensure all required Blip Details have been filled
	IF NOT (Are_MissionsAtCoords_Blip_Details_Valid(paramBlipDetails))
		#IF IS_DEBUG_BUILD
			PRINTSTRING("      Setup_MissionsAtCoords_Blip_Undisplayed: Some Blip Details are invalid.") PRINTNL()
		#ENDIF
		
		RETURN NULL
	ENDIF
	
	// Generally use the passed-in coords, but if the alternative blip coords contains a value, use those
	// KGM 3/4/13: This was added for Heists where the corona is in an interior under the ground and far away from the exterior on the map
	VECTOR theCoordsToUse = paramCoords
	
	IF NOT (ARE_VECTORS_ALMOST_EQUAL(paramBlipDetails.alternativeBlipCoords, << 0.0, 0.0, 0.0 >>))
		theCoordsToUse = paramBlipDetails.alternativeBlipCoords
		
		#IF IS_DEBUG_BUILD
			PRINTSTRING("      Setup_MissionsAtCoords_Blip_Undisplayed: Using alternative blip coordinates: ") PRINTVECTOR(theCoordsToUse) PRINTNL()
		#ENDIF
	ENDIF
	
	// Setup the blip
	BLIP_INDEX theBlipIndex = NULL
	
	IF (paramAreaTriggered)
		theBlipIndex = ADD_BLIP_FOR_RADIUS(theCoordsToUse, ANGLED_AREA_BLIP_RADIUS_m)
	ELSE
		theBlipIndex = ADD_BLIP_FOR_COORD(theCoordsToUse)
	ENDIF
	
	IF (theBlipIndex = NULL)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("      Setup_MissionsAtCoords_Blip_Undisplayed: ADD_BLIP_FOR_COORD() returned NULL.") PRINTNL()
		#ENDIF
		
		RETURN NULL
	ENDIF
	
	// Modify the details
	IF NOT (paramAreaTriggered)
		// ...specific to 'coords' blips
		SET_BLIP_SCALE(theBlipIndex, BLIP_SIZE_NETWORK_COORD)
		SET_BLIP_SPRITE(theBlipIndex, paramBlipDetails.theBlipSprite)
	ENDIF

	IF (paramAreaTriggered)
		// ...specific to 'radius' blips
		SET_BLIP_ALPHA(theBlipIndex, ANGLED_AREA_BLIP_ALPHA)
	ENDIF
	
	SHOW_HEIGHT_ON_BLIP(theBlipIndex, FALSE)
	IF paramBlipDetails.theBlipSprite = RADAR_TRACE_MP_LAMAR
	OR paramBlipDetails.theBlipSprite = RADAR_TRACE_CORNER_NUMBER_1
	OR paramBlipDetails.theBlipSprite = RADAR_TRACE_CORNER_NUMBER_2
	OR paramBlipDetails.theBlipSprite = RADAR_TRACE_CORNER_NUMBER_3
	OR paramBlipDetails.theBlipSprite = RADAR_TRACE_CORNER_NUMBER_4
	OR paramBlipDetails.theBlipSprite = RADAR_TRACE_CORNER_NUMBER_5
	OR paramBlipDetails.theBlipSprite = RADAR_TRACE_CORNER_NUMBER_6
	OR paramBlipDetails.theBlipSprite = RADAR_TRACE_CORNER_NUMBER_7
	OR paramBlipDetails.theBlipSprite = RADAR_TRACE_CORNER_NUMBER_8
		PRINTLN("[LOWFLOW] SET_BLIP_AS_SHORT_RANGE - paramBlipDetails.isShortRange = ", paramBlipDetails.isShortRange)
//		IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
			SET_BLIP_AS_SHORT_RANGE(theBlipIndex, TRUE)
//		ENDIF
		SET_BLIP_PRIORITY(theBlipIndex, BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
		PRINTLN("[LOWFLOW] SET_BLIP_FLASH_TIMER(theBlipIndex, ciLOW_FLOW_BLIP_FLASH_TIMER) ", ciLOW_FLOW_BLIP_FLASH_TIMER)
		SET_BLIP_FLASH_TIMER(theBlipIndex, ciLOW_FLOW_BLIP_FLASH_TIMER)
		SET_BLIP_NAME_FROM_TEXT_FILE(theBlipIndex, "FMMC_RSTAR_MSL")
		INT iProgress = GET_FM_STRAND_PROGRESS(ciFLOW_STRAND_LOW_RIDER) - 1
		IF iProgress >= ciLOW_FLOW_MISSION_DRIVE_BY_SHOOTOUT
		AND iProgress <= ciLOW_FLOW_MISSION_LOWRIDER_FINALE
			IF LOW_FLOW_GET_BLIP_SPRITE_FROM_FLOW_POS(ciFLOW_STRAND_LOW_RIDER, iProgress) =  paramBlipDetails.theBlipSprite
				PRINTLN("[LOWFLOW] Setup_MissionsAtCoords_Blip_Undisplayed - g_blipLowriderCut = theBlipIndex")
				g_blipLowriderCut = theBlipIndex
			ENDIF
		ENDIF
		
	ELSE
		SET_BLIP_AS_SHORT_RANGE(theBlipIndex, paramBlipDetails.isShortRange)
		SET_BLIP_PRIORITY(theBlipIndex, paramBlipDetails.theBlipPriority)
	ENDIF
	SET_BLIP_COLOUR(theBlipIndex, paramBlipDetails.theBlipColour)
	SET_BLIP_AS_MISSION_CREATOR_BLIP(theBlipIndex, paramBlipDetails.fromMissionCreator)
	SHOW_TICK_ON_BLIP(theBlipIndex, FALSE)
	
	// If required, change the Blip Display Name on the FrontEnd Map
	IF NOT (IS_STRING_NULL_OR_EMPTY(paramBlipDetails.theBlipName))
		BEGIN_TEXT_COMMAND_SET_BLIP_NAME(paramBlipDetails.theBlipName)
			IF NOT (IS_STRING_NULL_OR_EMPTY(paramBlipDetails.thePlayerNameForBlip))
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(paramBlipDetails.thePlayerNameForBlip)
			ENDIF
		END_TEXT_COMMAND_SET_BLIP_NAME(theBlipIndex)
	ENDIF

	// Keep the blip switched off for now
	SET_BLIP_DISPLAY(theBlipIndex, DISPLAY_NOTHING)
	
	// Return the Blip Index
	RETURN theBlipIndex

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Add and immediately display this area-triggered blip
//
// INPUT PARAMS:		paramCoords			The coords to place the blip
//						paramColour			The blip colour
// RETURN VALUE:		BLIP_INDEX			The Blip Index of the added blip
FUNC BLIP_INDEX Add_And_Display_MissionsAtCoords_Area_Triggered_Blip(VECTOR paramCoords, INT paramColour)
	
	// Setup the blip
	BLIP_INDEX theBlipIndex = ADD_BLIP_FOR_RADIUS(paramCoords, ANGLED_AREA_BLIP_RADIUS_m)
	
	IF (theBlipIndex = NULL)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("      Add_And_Display_MissionsAtCoords_Area_Triggered_Blip: ADD_BLIP_FOR_RADIUS() returned NULL.") PRINTNL()
		#ENDIF
		
		RETURN NULL
	ENDIF
	
	SET_BLIP_ALPHA(theBlipIndex, ANGLED_AREA_BLIP_ALPHA)
	SHOW_HEIGHT_ON_BLIP(theBlipIndex, FALSE)
	SET_BLIP_AS_SHORT_RANGE(theBlipIndex, TRUE)
	SET_BLIP_COLOUR(theBlipIndex, paramColour)
	SET_BLIP_PRIORITY(theBlipIndex, BLIPPRIORITY_LOWEST)
	SHOW_TICK_ON_BLIP(theBlipIndex, FALSE)

	// Only display on Radar
	SET_BLIP_DISPLAY(theBlipIndex, DISPLAY_MINIMAP_OR_BIGMAP)
	
	// Return the Blip Index
	RETURN theBlipIndex

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup the Retained Gang Attack Radius blip that stays active until the Gang Attack kicks off properly
//
// INPUT PARAMS:			paramSlot		The array position for the mission within the Mission At Coords array
PROC Setup_Retained_Gang_Attack_Radius_Blip(INT paramSlot)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Retained Gang Attack Blip - Add. RegID: ")
		PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
		PRINTNL()
//		PRINTSTRING("          ")
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//		PRINTNL()
	#ENDIF
	
	// Ensure there isn't already another GA blip setup
	IF (g_sMatcRetainedRadiusBlip.mrbBlipIndex != NULL)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("          *** IGNORING: There is already a retained Gang Attack blip setup") PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF

	BLIP_INDEX	missionBlip	= Get_MissionsAtCoords_Slot_BlipIndex(paramSlot)
	INT			theColour	= GET_BLIP_COLOUR(missionBlip)
	VECTOR		theCoords	= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	BLIP_INDEX	radiusBlip	= Add_And_Display_MissionsAtCoords_Area_Triggered_Blip(theCoords, theColour)
	
	IF (radiusBlip != NULL)
		g_sMatcRetainedRadiusBlip.mrbBlipIndex	= radiusBlip
		g_sMatcRetainedRadiusBlip.mrbRegID		= Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	ENDIF
						
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gather details of a few of the closest blips of each type that have just unlocked and should flash in unison
//
// INPUT PARAMS:			paramSlot		The array position for the mission within the Mission At Coords array
PROC Store_MissionsAtCoords_Flashing_Blip(INT paramSlot)

	IF NOT (g_sMatcFlashingBlipsMP.matcafbCollecting)
		EXIT
	ENDIF

	INT tempLoop = 0
		
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Flashing Blip - Store. Slot ") PRINTINT(paramSlot) PRINTNL()
//	#ENDIF
	
	// Get Player position
	VECTOR	playerPos = << 0.0, 0.0, 0.0 >>
	
	IF (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
		playerPos = GET_PLAYER_COORDS(PLAYER_ID())
	ENDIF
	
	// Get Details of this blip
	BLIP_INDEX	thisBlipIndex	= Get_MissionsAtCoords_Slot_BlipIndex(paramSlot)
	IF (thisBlipIndex = NULL)
		EXIT
	ENDIF
	
	MP_MISSION	thisMissionID	= Get_MissionsAtCoords_Slot_MissionID(paramSlot)
	IF (thisMissionID = eNULL_MISSION)
		EXIT
	ENDIF
	
	VECTOR		thisCoords		= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	FLOAT		thisDistance	= GET_DISTANCE_BETWEEN_COORDS(playerPos, thisCoords)
	
	INT			numBlipsOfType			= 0
	FLOAT		farthestDistanceOfType	= 0.0
	INT			farthestBlipOfTypePos	= 0
	
	// Store this blip if there is room and it doesn't exceed the maximum blips of this type (or replaces the farthest blip of this type if closer and the max is already full)
	REPEAT MATC_MAX_FLASHING_BLIPS_AT_ONCE tempLoop
		// Potentially store the blip here if there isn't a blip in this array position
		IF (g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofcBlipIndex	= NULL)
			// ...store the blip here if the maximum of this type of blip hasn't been reached
			IF (numBlipsOfType < MATC_MAX_FLASHING_UNLOCKED_BLIPS_OF_TYPE)
				g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofbMissionID	= thisMissionID
				g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofbDistance	= thisDistance
				g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofcBlipIndex	= thisBlipIndex
				
//				#IF IS_DEBUG_BUILD
//					PRINTSTRING(".KGM [At Coords]: Storing Flashing Blip Details in array position: ") PRINTINT(tempLoop) PRINTNL()
//				#ENDIF
				
				EXIT
			ENDIF
			
			// The maximum of this type has been reached, so replace the farthest away blip if this is closer
			IF (thisDistance < farthestDistanceOfType)
				g_sMatcFlashingBlipsMP.matcafbBlips[farthestBlipOfTypePos].matcofbMissionID	= thisMissionID
				g_sMatcFlashingBlipsMP.matcafbBlips[farthestBlipOfTypePos].matcofbDistance	= thisDistance
				g_sMatcFlashingBlipsMP.matcafbBlips[farthestBlipOfTypePos].matcofcBlipIndex	= thisBlipIndex
				
//				#IF IS_DEBUG_BUILD
//					PRINTSTRING(".KGM [At Coords]: Replacing Flashing Blip Details in array position: ") PRINTINT(tempLoop) PRINTNL()
//				#ENDIF
			ENDIF
			
			// Quit - the blip is either stored or not
			EXIT
		ENDIF
		
		// This position contains existing blip details, so gather some information if it is the same type
		IF (g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofbMissionID = thisMissionID)
			// Quit if duplicate
			IF (g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofcBlipIndex = thisBlipIndex)
				EXIT
			ENDIF
			
			// Update details if required
			IF (g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofbDistance > farthestDistanceOfType)
				farthestDistanceOfType	= g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofbDistance
				farthestBlipOfTypePos	= tempLoop
			ENDIF
			
			numBlipsOfType++
		ENDIF
	ENDREPEAT
	
	// The array is full, so check if this blip needs to replace the farthest blip of type
	IF (numBlipsOfType = 0)
		// ...nothing stored for this type, just have to forget it
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Array is full and there are no blips of type to replace") PRINTNL()
//		#ENDIF
				
		EXIT
	ENDIF
	
	IF (thisDistance < farthestDistanceOfType)
		g_sMatcFlashingBlipsMP.matcafbBlips[farthestBlipOfTypePos].matcofbMissionID	= thisMissionID
		g_sMatcFlashingBlipsMP.matcafbBlips[farthestBlipOfTypePos].matcofbDistance	= thisDistance
		g_sMatcFlashingBlipsMP.matcafbBlips[farthestBlipOfTypePos].matcofcBlipIndex	= thisBlipIndex
		
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Array Is Full, but Replacing Flashing Blip Details in array position: ") PRINTINT(tempLoop) PRINTNL()
//		#ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if Coronas should be blocked even if there is not a focus mission
//
// INPUT PARAMS:			paramSlot		[DEFAULT = INVALID_MATC_SLOT] The array position of a specific mission within the Mission At Coords array, or INVALID_MATC_SLOT for a general check
//							paramOutput		[DEFAULT = FALSE] TRUE means give additional reasons for failure when functionally blocked, otherwise FALSE
// RETURN VALUE:			BOOL			TRUE if Coronas should be blocked, FALSE if not
//
// NOTES:	This is generally to cover FM mission activations that aren't recognosed by Mission Controller
FUNC BOOL Should_MissionsAtCoords_Coronas_Be_Functionally_Blocked(INT paramSlot = INVALID_MATC_SLOT, BOOL paramOutput = FALSE)

	#IF NOT IS_DEBUG_BUILD
		UNUSED_PARAMETER(paramOutput)
	#ENDIF
	
	// Block them if the player is on a One-To-One Deathmatch or a Race-To-Point
	IF (IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()))
		IF (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_IMPROMPTU_DM)
		OR (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_RACE_TO_POINT)
		OR (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_PENNED_IN)
		OR (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_KILL_LIST)
		OR (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GB_BOSS_DEATHMATCH)
			#IF IS_DEBUG_BUILD
				IF (paramOutput)
					PRINTSTRING(".KGM [At Coords]: Corona Functionally Blocked. On Impromptu Race or One on One Deathmatch") PRINTNL()
				ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	// KGM 22/5/15 [BUG 2354093]: Allow external systems to functionally block all missions and hide their blips and coronas
	IF (g_matcBlockAndHideAllMissions)
		// If this is a general check, then return that all missions are functionally blocked
		IF (paramSlot = INVALID_MATC_SLOT)
			#IF IS_DEBUG_BUILD
				IF (paramOutput)
					PRINTLN(".KGM [At Coords]: Corona Functionally Blocked. g_matcBlockAndHideAllMissions = TRUE so hidden by external function call.")
				ENDIF
			#ENDIF
		
			RETURN TRUE
		ENDIF
		
		// If a specific mission has been passed, then check if there is a specific exception to the 'block all'
		IF NOT (Is_This_The_MissionsAtCoords_Currently_Unblocked_Mission(paramSlot))
		AND NOT (Should_MissionAtCoords_Ignore_Block_On_Mission(paramSlot))
			#IF IS_DEBUG_BUILD
				IF (paramOutput)
					PRINTLN(".KGM [At Coords]: Corona Functionally Blocked. g_matcBlockAndHideAllMissions = TRUE and the one unblocked exception is not this one. Returning TRUE")
				ENDIF
			#ENDIF
		
			RETURN TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF (paramOutput)			
				PRINTLN(".KGM [At Coords]: Corona Functionally Blocked. g_matcBlockAndHideAllMissions = TRUE BUT this mission is the unblocked exception, so ALLOW")
			ENDIF
		#ENDIF
	ENDIF
	
	// if player is on pause menu
	IF IS_PAUSE_MENU_ACTIVE()
	AND GET_CURRENT_FRONTEND_MENU_VERSION() != FE_MENU_VERSION_EMPTY_NO_BACKGROUND

		IF NOT IS_TRANSITION_SESSION_LAUNCHING()
		AND NOT IS_TRANSITION_SESSION_RESTARTING()
		AND IS_ENTITY_VISIBLE(PLAYER_PED_ID())
			
			#IF IS_DEBUG_BUILD
				IF (paramOutput)
					PRINTSTRING(".KGM [At Coords]: Corona Functionally Blocked. Player is on pause menu. (neil did this, blame him)") PRINTNL()
				ENDIF
			#ENDIF		
			RETURN TRUE	
		
		ENDIF
	ENDIF
	
	// if player is at store
	IF IS_COMMERCE_STORE_OPEN() 
	AND NOT IS_PC_VERSION()
		#IF IS_DEBUG_BUILD
			IF (paramOutput)
				PRINTSTRING(".KGM [At Coords]: Corona Functionally Blocked. Player is at store. (neil did this, blame him)") PRINTNL()
			ENDIF
		#ENDIF		
		RETURN TRUE
	ENDIF
	
	// Block all coronas if the player is on a Playlist unless this corona is the current active corona for the playlist
	IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
		// If doing a general check rather than a slot-specific check then no mission should get past here
		// General Checks are made by the 'display corona' routine
		IF (paramSlot = INVALID_MATC_SLOT)
			#IF IS_DEBUG_BUILD
				IF (paramOutput)
					PRINTSTRING(".KGM [At Coords]: Corona Functionally Blocked. On Playlist") PRINTNL()
				ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
		
		// If doing a slot-specific check, only the currently active playlist activity should get past here
		// Specific checks are made by the 'try to reserve mission' routine
		IF NOT (Is_This_The_MissionsAtCoords_Currently_Active_Playlist_Activity(paramSlot))
			#IF IS_DEBUG_BUILD
				IF (paramOutput)
					PRINTSTRING(".KGM [At Coords]: Corona Functionally Blocked. On Playlist and Playlist Mission is not this one. Slot: ")
					PRINTINT(paramSlot)
					PRINTNL()
				ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Block all coronas if the player is on an Ambient Tutorial
	// KGM 11/7/13: Exception if any missions are allowed to be active during an Ambient Tutorial - Don't functionally block the missions, but LOCK all missions instead except the ones that are allowed
	IF (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())
	
		#IF IS_DEBUG_BUILD
			IF (paramOutput)
				PRINTSTRING(".KGM [At Coords]: Corona Functionally Blocked. On ambient tutorial") PRINTNL()
			ENDIF
		#ENDIF
		
		IF (g_iMatccCounterOnDuringAmbTut <= 0)
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD
				IF (paramOutput)
					PRINTSTRING("      EXCEPTION: At least one mission needs to be active during an Ambient Tutorial, so ignoring functional Block for ALL missions - they'll get LOCKED instead") PRINTNL()
				ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	// They should not be blocked
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if Coronas should be on display or not
//
// RETURN VALUE:			BOOL			TRUE if Coronas should be on display, FALSE if not
//
// NOTES:	This performs the general check above plus a couple of other corona-specific checks
FUNC BOOL Should_MissionsAtCoords_Coronas_Be_Displayed()
	
	//Commented out as we got a command from code to hide this in the transition. 
	// Don't display them if a skyswoop is in progress
//	IF NOT IS_SKYSWOOP_AT_GROUND()
//		RETURN FALSE
//	ENDIF
	
	// Don't display them if the Mission At Coords system has a Focus Mission and the corona graphics aren’t being retained
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
	AND NOT (g_sCoronaFocusVisuals.retainedCoronaActive)
		RETURN FALSE
	ENDIF
	
	// Don't display them if the player is on or triggering a mission
	// KGM 16/5/13: using a global variable check - this may be temp - but the function also checks for having a focus mission and we need to allow coronas for a short while now under those conditions
//	IF (Is_MatC_Player_On_An_Activity())
	IF (g_sAtCoordsControlsMP.matccActivity.matcadActivelyOnMission)
		RETURN FALSE
	ENDIF
	
	// Don't display them if coronas shouldn't function
	IF (Should_MissionsAtCoords_Coronas_Be_Functionally_Blocked())
		RETURN FALSE
	ENDIF
		
	// Don't display if the pause menu is active (or loading, the _EX version caters for that)
	IF (IS_PAUSE_MENU_ACTIVE_EX())
		RETURN FALSE
	ENDIF
	
	// They should be displayed
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if Blips should be on display or not
//
// RETURN VALUE:			BOOL			TRUE if Blips should be on display, FALSE if not
//
// NOTES:	This performs the general check above plus a couple of other blip-specific checks
FUNC BOOL Should_MissionsAtCoords_Blips_Be_Displayed()
	
	// Don't display them if the player is on or triggering a mission
	IF (Is_MatC_Player_On_An_Activity())
		RETURN FALSE
	ENDIF
	
	// Don't display them if the Mission At Coords system has a Focus Mission
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		RETURN FALSE
	ENDIF
	
	// Don't display them if the player has a Wanted Level
	IF (Does_MatC_Player_Have_Wanted_Level())
		RETURN FALSE
	ENDIF
	
	// Don't display if the player is on a Playlist
	IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
		RETURN FALSE
	ENDIF
	
	// Don't display if teh player is on SCTV
	IF (IS_PLAYER_SCTV(PLAYER_ID()))
		RETURN FALSE
	ENDIF
	
	// Don't display if the player is on an Ambient Tutorial mission (ie: going to Lester's house for something)
	// KGM 11/7/13: EXCEPTION - If any mission should be active during an Ambient Tutorial then we need to make ALL active but LOCK the ones that shouldn't be
	IF (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())
		IF (g_iMatccCounterOnDuringAmbTut <= 0)
			// ...no mission has requested to be active during an ambient tutorial
			RETURN FALSE
		ENDIF
	ENDIF
	
	// They should be displayed
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the appropriate help-text-helper global flag if a specific type of activity blip gets displayed
//
// INPUT PARAMS:		paramSlot			// Mission Slot containing the activity blip being displayed
//
// NOTES:	These flags will get set TRUE if ANY matching blip gets displayed. They'll get set FALSE if all blips are getting switched off.
PROC Update_Help_Text_Helper_Flags_When_Blip_Displayed(INT paramSlot)

	// Is there any work to do?
	IF	(g_isAnyRSVerifiedRaceBlipOnDisplay)
	AND	(g_isAnyRSVerifiedDMBlipOnDisplay)
		// All helper flags already TRUE, so nothing to do
		EXIT
	ENDIF
	
	// Check if any flag needs set TRUE
	MP_MISSION	thisMissionID		= Get_MissionsAtCoords_Slot_MissionID(paramSlot)
	INT			thisCreatorID		= Get_MissionsAtCoords_Slot_CreatorID(paramSlot)

	// Is this a Rockstar-Verified Race blip being switched on?
	IF NOT (g_isAnyRSVerifiedRaceBlipOnDisplay)
		IF	(thisCreatorID = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
		AND	(thisMissionID = eFM_RACE_CLOUD)
			g_isAnyRSVerifiedRaceBlipOnDisplay = TRUE
		ENDIF
	ENDIF
	
	// Is this a Rockstar-Verified Deathmatch blip being switched on?
	IF NOT (g_isAnyRSVerifiedDMBlipOnDisplay)
		IF	(thisCreatorID = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
		AND	(thisMissionID = eFM_DEATHMATCH_CLOUD)
			g_isAnyRSVerifiedDMBlipOnDisplay = TRUE
		ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear all help text helper flags (because all blips being switched off)
PROC Clear_All_Help_Text_Helper_Flags()

	g_isAnyRSVerifiedRaceBlipOnDisplay	= FALSE
	g_isAnyRSVerifiedDMBlipOnDisplay	= FALSE

ENDPROC




// ===========================================================================================================
//      Mission At Coords NEARBY_LONG-RANGE BLIP routines
// ===========================================================================================================

// PURPOSE:	During Data Gathering, an array of the potential long-range blips needs maintained so they can be all switched on at once
//
// INPUT PARAMS:		paramRegID			The mission's RegID
//						paramBlipIndex		The mission's Blip Index
PROC Add_Nearby_Long_Range_Blip_To_Data_Gathering(INT paramRegID, BLIP_INDEX paramBlipIndex)

	// Find this blip or a free space on the array
	INT tempLoop = 0
	INT freeSpace = ILLEGAL_ARRAY_POSITION
	
	#IF IS_DEBUG_BUILD
		INT displayLoop = 0
	#ENDIF
	
	REPEAT INITIAL_LONG_RANGE_BLIPS_ARRAY_SIZE tempLoop
		// Is this the blip?
		IF (g_sLongRangeBlips.potentialInitialBlips[tempLoop].regID = paramRegID)
			// ...surprised to find it, but just update it
			g_sLongRangeBlips.potentialInitialBlips[tempLoop].blipIndex = paramBlipIndex
			
			#IF IS_DEBUG_BUILD
				PRINTSTRING("           Updating Blip on Data Gathering Array.   RegID: ")
				PRINTINT(paramRegID)
				PRINTSTRING("   ARRAY: ")
				
				REPEAT INITIAL_LONG_RANGE_BLIPS_ARRAY_SIZE displayLoop
					PRINTINT(g_sLongRangeBlips.potentialInitialBlips[displayLoop].regID)
					PRINTSTRING("  ")
				ENDREPEAT
				
				PRINTNL()
			#ENDIF
			
			EXIT
		ENDIF
		
		// Is this a free space?
		IF (freeSpace = ILLEGAL_ARRAY_POSITION)
			IF (g_sLongRangeBlips.potentialInitialBlips[tempLoop].regID = ILLEGAL_AT_COORDS_ID)
				freeSpace = tempLoop
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Found a free space?
	IF (freeSpace = ILLEGAL_ARRAY_POSITION)
		SCRIPT_ASSERT("Add_Nearby_Long_Range_Blip_To_Data_Gathering() - Failed to find a free space on the data gathering array. Tell Keith.")
		
		EXIT
	ENDIF
	
	// Store the data
	g_sLongRangeBlips.potentialInitialBlips[freeSpace].regID		= paramRegID
	g_sLongRangeBlips.potentialInitialBlips[freeSpace].blipIndex	= paramBlipIndex
			
	#IF IS_DEBUG_BUILD
		PRINTSTRING("           Adding Blip on Data Gathering Array.     RegID: ")
		PRINTINT(paramRegID)
		PRINTSTRING("   ARRAY: ")
		
		REPEAT INITIAL_LONG_RANGE_BLIPS_ARRAY_SIZE displayLoop
			PRINTINT(g_sLongRangeBlips.potentialInitialBlips[displayLoop].regID)
			PRINTSTRING("  ")
		ENDREPEAT
		
		PRINTNL()
	#ENDIF
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	During Data Gathering, an array of the potential long-range blips needs maintained so they can be all switched on at once
//
// INPUT PARAMS:		paramRegID			The mission's RegID
PROC Remove_Nearby_Long_Range_Blip_From_Data_Gathering(INT paramRegID)

	IF (paramRegID = ILLEGAL_AT_COORDS_ID)
		EXIT
	ENDIF

	// Find this blip and clear the data
	INT tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		INT displayLoop = 0
	#ENDIF
	
	REPEAT INITIAL_LONG_RANGE_BLIPS_ARRAY_SIZE tempLoop
		// Is this the blip?
		IF (g_sLongRangeBlips.potentialInitialBlips[tempLoop].regID = paramRegID)
			// ...clear it
			g_sLongRangeBlips.potentialInitialBlips[tempLoop].blipIndex = NULL
			g_sLongRangeBlips.potentialInitialBlips[tempLoop].regID		= ILLEGAL_AT_COORDS_ID
			
			#IF IS_DEBUG_BUILD
				PRINTSTRING("           Removing Blip from Data Gathering Array. RegID: ")
				PRINTINT(paramRegID)
				PRINTSTRING("   ARRAY: ")
				
				REPEAT INITIAL_LONG_RANGE_BLIPS_ARRAY_SIZE displayLoop
					PRINTINT(g_sLongRangeBlips.potentialInitialBlips[displayLoop].regID)
					PRINTSTRING("  ")
				ENDREPEAT
				PRINTNL()
			#ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the long-range blip type for this mission
//
// INPUT PARAMS:			paramSlot					The Slot containing the mission to be maintained
// RETURN VALUE:			g_eLongRangeBlipTypes		The long-range blip type, or NO_LONG_RANGE_BLIP_TYPE
FUNC g_eLongRangeBlipTypes Get_MissionsAtCoords_Long_Range_Blip_Type(INT paramSlot)

	MP_MISSION thisMissionType = Get_MissionsAtCoords_Slot_MissionID(paramSlot)
	
	SWITCH (thisMissionType)
		CASE eFM_MISSION_CLOUD		RETURN (LRBT_MISSION)
		CASE eFM_DEATHMATCH_CLOUD	RETURN (LRBT_DEATHMATCH)
		CASE eFM_RACE_CLOUD			RETURN (LRBT_RACE)
		CASE eFM_SURVIVAL_CLOUD		RETURN (LRBT_SURVIVAL)
		CASE eFM_BASEJUMP_CLOUD		RETURN (LRBT_BASEJUMP)
	ENDSWITCH
	
	RETURN (NO_LONG_RANGE_BLIP_TYPE)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Make a nearby blip short-range again unless something else wants it to remain long-range
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
PROC Clear_Mission_Blip_As_Nearby_Long_Range(INT paramSlot)

	IF (Is_MissionsAtCoords_Mission_Blip_Temporarily_LongRange(paramSlot))
		EXIT
	ENDIF
	
	// During data gathering, don't change the blip range
	IF (g_sLongRangeBlips.gatherDataOnly)
		INT regID = Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
		Remove_Nearby_Long_Range_Blip_From_Data_Gathering(regID)
		
		EXIT
	ENDIF
	
	BLIP_INDEX theBlipIndex = Get_MissionsAtCoords_Slot_BlipIndex(paramSlot)

	// KGM 29/9/14 [BUG 2057685]: Before removing the mission from the nearby long-range mission array, set the blip alpha value back to 255
	IF (DOES_BLIP_EXIST(theBlipIndex))
		SET_BLIP_ALPHA(theBlipIndex, 255)
	ENDIF
	
	// Switch to short-range
	IF (IS_BLIP_SHORT_RANGE(theBlipIndex))
		EXIT
	ENDIF
	
	SET_BLIP_AS_SHORT_RANGE(theBlipIndex, TRUE)
	
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Nearby Long-Range Blip is being set Short-Range.") PRINTNL()
//		PRINTSTRING("         ") Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot) PRINTNL()
//	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_HighlightCloseBlips"))
			SET_BLIP_COLOUR(theBlipIndex, BLIP_COLOUR_BLUE)
		ENDIF
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Make a nearby blip long-range
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
PROC Set_Mission_Blip_As_Nearby_Long_Range(INT paramSlot)

	// Don't do this until all the initial data is available
	IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
		EXIT
	ENDIF
	
	// Don't do this if there are new missions to be distributed
	IF (g_sAtCoordsBandsMP[MATCB_NEW_MISSIONS].matcbNumInBand > 0)
		EXIT
	ENDIF
	
	BLIP_INDEX theBlipIndex = Get_MissionsAtCoords_Slot_BlipIndex(paramSlot)
	
	// During data gathering, don't change the blip range
	IF (g_sLongRangeBlips.gatherDataOnly)
		INT regID = Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
		Add_Nearby_Long_Range_Blip_To_Data_Gathering(regID, theBlipIndex)
		
		EXIT
	ENDIF
	
	// Switch to long-range unless the player is in an apartment
	IF NOT (IS_BLIP_SHORT_RANGE(theBlipIndex))
		// Blip is already long-range, so switch back to short-range
		IF (Is_MissionsAtCoords_Player_In_An_Apartment())
			SET_BLIP_AS_SHORT_RANGE(theBlipIndex, TRUE)
	
//			#IF IS_DEBUG_BUILD
//				PRINTSTRING(".KGM [At Coords]: Mission Blip is nearby long-range, but making short-range since player is in an apartment") PRINTNL()
//				PRINTSTRING("         ") Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot) PRINTNL()
//			#ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	// Don't switch to long-range if the player is in an apartment, but still highlight it
	IF (Is_MissionsAtCoords_Player_In_An_Apartment())
		#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_HighlightCloseBlips"))
				SET_BLIP_COLOUR(theBlipIndex, BLIP_COLOUR_GREEN)
			ENDIF
		#ENDIF

		EXIT
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
	AND NOT IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
		EXIT
	ENDIF
	
	SET_BLIP_AS_SHORT_RANGE(theBlipIndex, FALSE)
	
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Mission Blip is being set as nearby long-range.") PRINTNL()
//		PRINTSTRING("         ") Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot) PRINTNL()
//	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_HighlightCloseBlips"))
			SET_BLIP_COLOUR(theBlipIndex, BLIP_COLOUR_GREEN)
		ENDIF
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Find the farthest long-range blip for this long-range blip type
//
// INPUT PARAMS:			paramLRBlipType		The long Range Blip Type
PROC Store_Farthest_Distance_For_Long_Range_Blip_Type(g_eLongRangeBlipTypes paramLRBlipType)

	INT		tempLoop			= 0
	FLOAT	farthestDistance	= 0.0
	
	REPEAT MAX_NUM_LONG_RANGE_BLIPS_PER_TYPE tempLoop
		IF (g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[tempLoop].theDistance > farthestDistance)
			farthestDistance = g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[tempLoop].theDistance
		ENDIF
	ENDREPEAT
	
	g_sLongRangeBlips.allBlips[paramLRBlipType].farthestBlipOfType = farthestDistance

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this mission is on the nearby missions array for this blip type
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
//							paramLRBlipType		The long Range Blip Type
// RETURN VALUE:			INT					The array position, or ILLEGAL_ARRAY_POSITION
FUNC INT Get_Position_Of_Mission_On_Nearby_Missions_Array(INT paramSlot, g_eLongRangeBlipTypes paramLRBlipType)

	INT regID		= Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	INT tempLoop	= 0
	
	REPEAT MAX_NUM_LONG_RANGE_BLIPS_PER_TYPE tempLoop
		IF (g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[tempLoop].regID = regID)
			RETURN (tempLoop)
		ENDIF
	ENDREPEAT
	
	RETURN (ILLEGAL_ARRAY_POSITION)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this mission should be added to the nearby missions array for this blip type
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
//							paramLRBlipType		The long Range Blip Type
//							paramlrArrayPos		The array position for this blip type
FUNC BOOL Add_Mission_To_Nearby_Missions_Array(INT paramSlot, g_eLongRangeBlipTypes paramLRBlipType)

	// Get the distance from the player
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
		RETURN FALSE
	ENDIF
	
	VECTOR	missionCoords	= Get_MissionsAtCoords_Slot_Coords(paramSlot)	
	FLOAT	theDistance		= GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), missionCoords)
	
	// Is this closer than the current farthest distance for that blip type?
	IF (theDistance >= g_sLongRangeBlips.allBlips[paramLRBlipType].farthestBlipOfType)
		RETURN FALSE
	ENDIF
	
	// Distance is closer than the current farthest nearby long-range mission of this type
	// Find the farthest distance of type by searching the array
	INT		tempLoop = 0
	FLOAT	farthestDistance = 0.0
	INT		farthestArrayPos = ILLEGAL_ARRAY_POSITION
	
	REPEAT MAX_NUM_LONG_RANGE_BLIPS_PER_TYPE tempLoop
		IF (g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[tempLoop].theDistance > farthestDistance)
			farthestDistance = g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[tempLoop].theDistance
			farthestArrayPos = tempLoop
		ENDIF
	ENDREPEAT
	
	// As a safety precaution, make sure this new blip is closer than the farthest mission based on scanning the array
	BOOL theReturn = FALSE
	
	IF (theDistance < farthestDistance)
		// ...this mission distance is closer than the farthest distance, so replace that array details
		// NOTE: This should always be the case, just being paranoid
		INT			regID			= Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
		BLIP_INDEX	theBlipIndex	= Get_MissionsAtCoords_Slot_BlipIndex(paramSlot)
		
		// During the data gathering stage we need to remove a mission from the array that is being replaced
		IF (g_sLongRangeBlips.gatherDataOnly)
			INT replacementRegID = g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[farthestArrayPos].regID
			Remove_Nearby_Long_Range_Blip_From_Data_Gathering(replacementRegID)
		ENDIF
		
		// If not data gathering, then we need to make any overwritten blip instantly short-range to avoid cluttering the radar edge in extreme situations
		IF NOT (g_sLongRangeBlips.gatherDataOnly)
			BLIP_INDEX	existingBlipIndex	= g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[farthestArrayPos].blipIndex
			
			IF (DOES_BLIP_EXIST(existingBlipIndex))
				SET_BLIP_AS_SHORT_RANGE(existingBlipIndex, TRUE)
				
				// KGM 29/9/14 [BUG 2057685]: Also need to ensure the blip alpha is 255 again
				SET_BLIP_ALPHA(existingBlipIndex, 255)
	
				// Remove the highlighting of the blip
				#IF IS_DEBUG_BUILD
//					INT existingRegID = g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[farthestArrayPos].regID
//					PRINTSTRING(".KGM [At Coords]: Nearby Long-Range Blip is being overwritten by a closer blip. Set as Short-Range. RegID: ") PRINTINT(existingRegID) PRINTNL()
					
					IF (GET_COMMANDLINE_PARAM_EXISTS("sc_HighlightCloseBlips"))
						SET_BLIP_COLOUR(existingBlipIndex, BLIP_COLOUR_BLUE)
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	
		g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[farthestArrayPos].regID	 		= regID
		g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[farthestArrayPos].theDistance	= theDistance
		g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[farthestArrayPos].blipIndex		= theBlipIndex
		
		// ...set this mission blip as long range
		Set_Mission_Blip_As_Nearby_Long_Range(paramSlot)
		
		theReturn = TRUE
	ENDIF
	
	// Always, update the farthest distance
	Store_Farthest_Distance_For_Long_Range_Blip_Type(paramLRBlipType)
	
	RETURN (theReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Remove this mission from the nearby missions array for this blip type
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
//							paramLRBlipType		The long Range Blip Type
//							paramlrArrayPos		The array position for this blip type
PROC Remove_Mission_From_Nearby_Missions_Array(INT paramSlot, g_eLongRangeBlipTypes paramLRBlipType, INT paramlrArrayPos)

	// Set the array details back to the defaults
	g_structOneLongRangeBlip emptyOneLongRangeBlipStruct
	g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[paramlrArrayPos] = emptyOneLongRangeBlipStruct
	
	// Set the mission blip back to short-range unless something else wants it to remain long-range
	Clear_Mission_Blip_As_Nearby_Long_Range(paramSlot)
	
	// Store the new farthest distance for this long-range Blip Type
	Store_Farthest_Distance_For_Long_Range_Blip_Type(paramLRBlipType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Update the Mission's distance on the nearby mission's array
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
//							paramLRBlipType		The long Range Blip Type
//							paramlrArrayPos		The array position for this blip type
PROC Update_Mission_On_Nearby_Missions_Array(INT paramSlot, g_eLongRangeBlipTypes paramLRBlipType, INT paramlrArrayPos)

	// Update the distance from the player
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
		EXIT
	ENDIF
	
	VECTOR	missionCoords	= Get_MissionsAtCoords_Slot_Coords(paramSlot)	
	FLOAT	theDistance		= GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), missionCoords)
	
	g_sLongRangeBlips.allBlips[paramLRBlipType].blipsOfType[paramlrArrayPos].theDistance = theDistance
	
	// Ensure this mission's blip is long-range
	Set_Mission_Blip_As_Nearby_Long_Range(paramSlot)
	
	// Store the new farthest distance for this long-range Blip Type
	Store_Farthest_Distance_For_Long_Range_Blip_Type(paramLRBlipType)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this mission should show it's blip as long-range for being one of the closest of its type
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
PROC Maintain_MissionsAtCoords_Nearby_Long_Range_Blip(INT paramSlot)

	// Only do this when the functionality has been activated
	IF NOT (g_sLongRangeBlips.allowUpdates)
		EXIT
	ENDIF

	g_eLongRangeBlipTypes longRangeBlipType = Get_MissionsAtCoords_Long_Range_Blip_Type(paramSlot)
	
	IF (longRangeBlipType = NO_LONG_RANGE_BLIP_TYPE)
		EXIT
	ENDIF

	INT lrBlipArrayPos = Get_Position_Of_Mission_On_Nearby_Missions_Array(paramSlot, longRangeBlipType)
	IF (lrBlipArrayPos != ILLEGAL_ARRAY_POSITION)
		// ...yes, this mission was one of the closest of its type
		// Is the mission now locked?
		IF (Is_MissionsAtCoords_Mission_Locked(paramSlot))
			// ...yes, mission is now locked, so the mission shouldn't be one of the closest of type
			Remove_Mission_From_Nearby_Missions_Array(paramSlot, longRangeBlipType, lrBlipArrayPos)
			EXIT
		ENDIF
		
		// Mission isn't locked, so update this mission's details on the array
		Update_Mission_On_Nearby_Missions_Array(paramSlot, longRangeBlipType, lrBlipArrayPos)
		EXIT
	ENDIF
	
	// This mission blip is not one of the closest missions of type
	// Is the mission Locked?
	IF (Is_MissionsAtCoords_Mission_Locked(paramSlot))
		// ...yes, so nothing to do
		EXIT
	ENDIF
	
	// Mission is not locked
	// Is Mission now one of the closest of its type?
	IF (Add_Mission_To_Nearby_Missions_Array(paramSlot, longRangeBlipType))
		// ...mission got added to the array
		EXIT
	ENDIF
	
	// Mission isn't one of the closest of type, so ensure the blip isn't marked as long-range for being one of the closest
	Clear_Mission_Blip_As_Nearby_Long_Range(paramSlot)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this mission's blip is nearby long-range
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
FUNC BOOL Is_MissionsAtCoords_Mission_Blip_Nearby_Long_Range(INT paramSlot)

	g_eLongRangeBlipTypes longRangeBlipType = Get_MissionsAtCoords_Long_Range_Blip_Type(paramSlot)
	
	IF (longRangeBlipType = NO_LONG_RANGE_BLIP_TYPE)
		RETURN FALSE
	ENDIF

	INT lrBlipArrayPos = Get_Position_Of_Mission_On_Nearby_Missions_Array(paramSlot, longRangeBlipType)
	
	IF (lrBlipArrayPos = ILLEGAL_ARRAY_POSITION)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC




// ===========================================================================================================
//      Mission At Coords BLIP DISPLAY routines
// ===========================================================================================================

// PURPOSE:	Display an already setup blip if the mission is unlocked
//
// INPUT PARAMS:			paramSlot					The array position for the mission within the Mission At Coords array
//							paramCheckInteriorBlip		[DEFAULT = FALSE] TRUE if an Interior Only blip can get processed, FALSE if an Interior Only blip should be ignored
//							paramDisplayOutput			[DEFAULT = TRUE] TRUE if console log output should be displayed, FALSE if not
PROC Display_MissionsAtCoords_Blip_For_Mission(INT paramSlot, BOOL paramCheckInteriorBlip = FALSE, BOOL paramDisplayOutput = TRUE)

	#IF NOT IS_DEBUG_BUILD
		UNUSED_PARAMETER(paramDisplayOutput)
	#ENDIF

	// Only check Interiors Only blips if the parameter is TRUE
	IF NOT (paramCheckInteriorBlip)
		IF (Should_MissionsAtCoords_Mission_Blip_Display_In_Interior_Only(paramSlot))
			//PRINTLN(".KGM [At Coords]: Display_MissionsAtCoords_Blip_For_Mission -  Should_MissionsAtCoords_Mission_Blip_Display_In_Interior_Only - paramSlot = ", paramSlot)
			EXIT
		ENDIF
	ENDIF

	// Only display the blip if the mission is unlocked
	IF (Is_MissionsAtCoords_Mission_Locked(paramSlot))
		//PRINTLN(".KGM [At Coords]: Display_MissionsAtCoords_Blip_For_Mission -  Is_MissionsAtCoords_Mission_Locked - paramSlot = ", paramSlot)
		EXIT
	ENDIF
	
	// Only display the blip if an external routine hasn't blocked it
	IF (g_matcBlockAndHideAllMissions)
		IF NOT (Should_MissionAtCoords_Ignore_Block_On_Mission(paramSlot))
			//PRINTLN(".KGM [At Coords]: Display_MissionsAtCoords_Blip_For_Mission -  Should_MissionAtCoords_Ignore_Block_On_Mission - paramSlot = ", paramSlot)
			EXIT
		ENDIF
	ENDIF
	
	// Only display the blip if the mission isn't marked for delete
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		//PRINTLN(".KGM [At Coords]: Display_MissionsAtCoords_Blip_For_Mission -  Is_MissionsAtCoords_Mission_Marked_For_Delete - paramSlot = ", paramSlot)
		EXIT
	ENDIF
	
	// Only display the blip if the mission isn't sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		//PRINTLN(".KGM [At Coords]: Display_MissionsAtCoords_Blip_For_Mission -  Should_MissionsAtCoords_Mission_Be_Sleeping - paramSlot = ", paramSlot)
		EXIT
	ENDIF
	
	// Only display if the mission requires a blip
	IF NOT (Should_MissionsAtCoords_Mission_Display_A_Blip(paramSlot))
		//PRINTLN(".KGM [At Coords]: Display_MissionsAtCoords_Blip_For_Mission -  Should_MissionsAtCoords_Mission_Display_A_Blip - paramSlot = ", paramSlot)
		EXIT
	ENDIF
	
	// Don't draw a blip if it is an overlapped mission (unless on a commandline parameter)
	IF (Is_MissionsAtCoords_Mission_Hidden_By_Overlapping_Corona(paramSlot))
		BOOL quitIfOverlapped = TRUE
		
		#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_ShowOverlappingCoronas"))
				quitIfOverlapped = FALSE
			ENDIF
		#ENDIF
		
		IF (quitIfOverlapped)		
			//PRINTLN(".KGM [At Coords]: Display_MissionsAtCoords_Blip_For_Mission -  Is_MissionsAtCoords_Mission_Hidden_By_Overlapping_Corona - paramSlot = ", paramSlot)
			EXIT
		ENDIF
	ENDIF
		
	// KGM 12/5/15: BUG 2313555 - Don't draw a blip if hidden by a PI Menu Job Type Hide Blip option
	// KGM 11/6/15: BUG 2316703 - Also hide if the mission is within a disabled area
	IF (Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu(paramSlot))
	OR (Is_MissionsAtCoords_Mission_Hidden_By_Disabled_Area(paramSlot))
		//PRINTLN(".KGM [At Coords]: Display_MissionsAtCoords_Blip_For_Mission -  Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu - paramSlot = ", paramSlot)
		EXIT
	ENDIF

	// KGM 11/7/13: If blips are hidden by being on Ambient Tutorial and then a mission is requested to be displayed during ambient tutorial then this locks the missions,
	//				but the missions lock in a staggered manner meaning all the blips appear and then disappear over time - this tries to leave them hidden
	IF (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())
		IF (g_iMatccCounterOnDuringAmbTut > 0)
			// ...some missions want to be allowed during an Ambient Tutorial, so lock all other missions except the ones that want to be allowed
			IF NOT (Is_MissionsAtCoords_Mission_Allowed_During_Ambient_Tutorial(paramSlot))
				//PRINTLN(".KGM [At Coords]: Display_MissionsAtCoords_Blip_For_Mission -  IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL - paramSlot = ", paramSlot)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	BLIP_INDEX theBlip	= Get_MissionsAtCoords_Slot_BlipIndex(paramSlot)
	
	// Do nothing if the blip is already on display
	IF (GET_BLIP_INFO_ID_DISPLAY(theBlip) != DISPLAY_NOTHING)
		//PRINTLN(".KGM [At Coords]: Display_MissionsAtCoords_Blip_For_Mission -  GET_BLIP_INFO_ID_DISPLAY - paramSlot = ", paramSlot)
		EXIT
	ENDIF

	// Display the blip
	IF NOT ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
		//PRINTLN(".KGM [At Coords]: Display_MissionsAtCoords_Blip_For_Mission -  ARE_TEMP_HIDDEN_BLIPS_ACTIVE - paramSlot = ", paramSlot)
		IF (Should_MissionsAtCoords_Mission_Blip_Display_On_Minimap_Only(paramSlot))
			SET_BLIP_DISPLAY(theBlip, DISPLAY_RADAR_ONLY)
		ELSE
			SET_BLIP_DISPLAY(theBlip, DISPLAY_BOTH)
		ENDIF
	ENDIF
	
	// Setup the blip range
	IF ((Is_MissionsAtCoords_Mission_Blip_Temporarily_LongRange(paramSlot))
	OR (Is_MissionsAtCoords_Mission_Blip_Nearby_Long_Range(paramSlot)))
	AND (NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
	OR IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART())
		SET_BLIP_AS_SHORT_RANGE(theBlip, FALSE)
	ELSE
		SET_BLIP_AS_SHORT_RANGE(theBlip, TRUE)
	ENDIF
	
	// Display Les' Rating
	#IF IS_DEBUG_BUILD
		BOOL	displayLesRating	= TRUE
		INT		LesRating			= 0
			
		IF (paramDisplayOutput)
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_noLesRatingOnBlips"))
				displayLesRating = FALSE
			ENDIF
			
			IF (displayLesRating)
				MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
				LesRating = Get_Les_Rating_For_FM_Cloud_Loaded_Activity(theMissionIdData)
				IF (LesRating > 0)
					SET_BLIP_DEBUG_NUMBER(theBlip, LesRating)
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	// Only the blip needs to flash, only flash if the activity is properly unlocked and not just temporarily unlocked	
	IF (Should_MissionsAtCoords_Blip_Flash(paramSlot))
		MP_MISSION_ID_DATA	theMissionIdData		= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
		BOOL				waitForSecondaryUnlock	= Should_MissionsAtCoords_Mission_Unlock_With_Secondary_Unlock(paramSlot)
		BOOL				missionLocked			= Check_If_Mission_Locked(theMissionIdData, waitForSecondaryUnlock)
		
		IF NOT (missionLocked)
			// KGM 12/7/13: Don;t flash the blips now as they unlock, pass them to a function that will flash a few of each type closest to the player in unison
			Store_MissionsAtCoords_Flashing_Blip(paramSlot)
			Set_MissionsAtCoords_Blip_As_No_Flash(paramSlot)
		ENDIF
	ENDIF
	
	// Should the mission blip display the 'played' indicator?
	IF (Has_MissionsAtCoords_Activity_Been_Played(paramSlot))
		SHOW_TICK_ON_BLIP(theBlip, TRUE)
	ENDIF
	
	// Maintain the help text globals
	Update_Help_Text_Helper_Flags_When_Blip_Displayed(paramSlot)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Hide an already setup blip
//
// INPUT PARAMS:			paramSlot					The array position for the mission within the Mission At Coords array
//							paramDisplayOutput			[DEFAULT = TRUE] TRUE if console log output should be displayed, FALSE if not
PROC Hide_MissionsAtCoords_Blip_For_Mission(INT paramSlot, BOOL paramDisplayOutput = TRUE)

//	#IF NOT IS_DEBUG_BUILD
		UNUSED_PARAMETER(paramDisplayOutput)
//	#ENDIF

	BLIP_INDEX theBlip = Get_MissionsAtCoords_Slot_BlipIndex(paramSlot)
	
	SET_BLIP_DISPLAY(theBlip, DISPLAY_NOTHING)
	
//	#IF IS_DEBUG_BUILD
//		IF (paramDisplayOutput)
//			PRINTSTRING(".KGM [At Coords]: Hiding Blip - ")
//			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//			PRINTNL()
//		ENDIF
//	#ENDIF
	
ENDPROC




// ===========================================================================================================
//      Mission At Coords OVERLAPPING MISSIONS access routines
// ===========================================================================================================

// PURPOSE:	Check if this mission should be treated as Always Overlapped
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:		BOOL				TRUE if treat as Always Overlapped, FALSE otherwise
FUNC BOOL Should_This_Mission_Be_Classed_As_Always_Overlapped(INT paramSlot)

	IF (Is_MissionsAtCoords_Mission_Locked(paramSlot))
		RETURN TRUE
	ENDIF
	
	// This mission should be treated as overlapped if sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		RETURN TRUE
	ENDIF
	
	// This mission should be treated as overlapped if an overlap exists and this mission does not display a corona and is not Area Triggered"
	IF NOT (Should_MissionsAtCoords_Mission_Display_A_Corona(paramSlot))
	AND NOT (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
		RETURN TRUE
	ENDIF
	
	// Don't treat as always overlapped
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if these Missions overlap
//
// INPUT PARAMS:		paramSlot					The array position for the mission within the Mission At Coords array
//						paramAreaTriggered			TRUE if this mission is Area Triggered, otherwise FALSE
//						paramCheckSlot				The array position of the mission being checked against within the Missions At Coords array
//						paramCheckIsAreaTriggered	TRUE if the check mission is Area Triggered, otherwise FALSE
// RETURN VALUE:		BOOL						TRUE if treat as Always Overlapped, FALSE otherwise
FUNC BOOL Do_These_Missions_Overlap(INT paramSlot, BOOL paramAreaTriggered, INT paramCheckSlot, BOOL paramCheckIsAreaTriggered)

	// Get the comparison radius
	FLOAT	theComparisonRadius = 0.0
	
	// ...first mission
	IF (paramAreaTriggered)
		theComparisonRadius += ANGLED_AREA_BLIP_RADIUS_m
	ELSE
		theComparisonRadius += Get_MissionsAtCoords_Slot_Corona_Trigger_Radius(paramSlot)
	ENDIF
	
	// ...second Mission
	IF (paramCheckIsAreaTriggered)
		theComparisonRadius += ANGLED_AREA_BLIP_RADIUS_m
	ELSE
		theComparisonRadius += Get_MissionsAtCoords_Slot_Corona_Trigger_Radius(paramCheckSlot)
	ENDIF
	
	// ...a buffer area
	theComparisonRadius += CORONA_GENERAL_COSMETIC_LEEWAY_m
	
	// Check for overlap
	VECTOR	coronaPos		= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	VECTOR	checkCoronaPos	= Get_MissionsAtCoords_Slot_Coords(paramCheckSlot)
	
	RETURN (ARE_VECTORS_ALMOST_EQUAL(coronaPos, checkCoronaPos, theComparisonRadius))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Calculate the overlap priority for this mission based on various factors - with a higher value being a highest priority
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
//						paramCreatorID		The CreatorID for the mission
// RETURN VALUE:		INT					An overlap priority (with a higher value being higher priority)
//
// NOTES:	Your own UGC always takes priority over most other stuff, followed by Rockstar content, followed by other stuff
//			Area-Triggered missions, while active, take priority over everything since they can't be invited into
//			Top Priority will have to be Tutorials, but this will need to be based on a function
FUNC INT Calculate_Corona_Overlap_Priority(INT paramSlot, INT paramCreatorID)

	// Generate the Priorities as CONST_INTS - the higher the value the higher the priority with lower priority coronas classed as 'overlapped'
	CONST_INT	UNKNOWN_PRIORITY				0
	CONST_INT	SHARED_MISSION_PRIORITY			1
	CONST_INT	MINIGAME_PRIORITY				2
	CONST_INT	ROCKSTAR_CREATED_PRIORITY		3
	CONST_INT	ROCKSTAR_VERIFIED_PRIORITY		4
	CONST_INT	MY_UGC_PRIORITY					5
	CONST_INT	AREA_TRIGGERED_PRIORITY			6
	CONST_INT	TUTORIAL_PRIORITY				7
	
	IF (paramCreatorID = FMMC_MINI_GAME_CREATOR_ID)
		RETURN MINIGAME_PRIORITY
	ENDIF
	
	IF (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
		RETURN AREA_TRIGGERED_PRIORITY
	ENDIF
	
	IF (Is_MissionsAtCoords_Mission_A_Shared_Mission(paramSlot))
		RETURN SHARED_MISSION_PRIORITY
	ENDIF
	
	IF (paramCreatorID = NATIVE_TO_INT(PLAYER_ID()))
		RETURN MY_UGC_PRIORITY
	ENDIF
	
	IF (paramCreatorID = FMMC_ROCKSTAR_CREATOR_ID)
		RETURN ROCKSTAR_CREATED_PRIORITY
	ENDIF
	
	IF (paramCreatorID = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
		RETURN ROCKSTAR_VERIFIED_PRIORITY
	ENDIF
	
	// TO DO: Integrate Tutorial Priority
	
	// Shouldn't reach here
	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: ERROR: Calculate_Corona_Overlap_Priority(): Mission didn't fit any priority check. Returning UNKNOWN_PRIORITY (0).") PRINTNL()
//		PRINTSTRING("          ")
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//		PRINTNL()
		
		PRINTLN("Calculate_Corona_Overlap_Priority(): Mission didn't fit any priority check. Returning UNKNOWN_PRIORITY. See Console Log. Tell Keith.")
	#ENDIF
	
	RETURN UNKNOWN_PRIORITY

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	These two missions overlap, so resolve their priorities
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
//						paramAlwaysOverlap	TRUE if this mission should be always overlapped if an overlap exists, FALSE to obey usual rules
//						paramCheckSlot		The array position of the mission being checked within the Mission At Coords array
PROC Resolve_Overlapping_Missions(INT paramSlot, BOOL paramAlwaysOverlap, INT paramCheckSlot)
	
	BOOL checkMissionAlwaysOverlapped = Should_This_Mission_Be_Classed_As_Always_Overlapped(paramCheckSlot)
	
	// If both are always overlapped then leave both missions as they are to prevent undoing the result of another overlap
	IF (paramAlwaysOverlap = checkMissionAlwaysOverlapped)
		IF (paramAlwaysOverlap)
			// ...both are 'always overlapped' so do nothing
			#IF IS_DEBUG_BUILD
				PRINTSTRING("          OVERLAPPING: [DO NOTHING] Both slots ") PRINTINT(paramSlot) PRINTSTRING(" and ") PRINTINT(paramCheckSlot) PRINTSTRING(" are 'always overlapped'") PRINTNL()
			#ENDIF
			
			EXIT
		ENDIF
	ELSE
		// ...only one must be 'always overlap', so it needs to become marked as 'overlapped'
		// Is it the main mission?
		IF (paramAlwaysOverlap)
			// ...yes, so mark it as overlapped
			IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramSlot))
				Hide_MissionsAtCoords_Blip_For_Mission(paramSlot)
			
				#IF IS_DEBUG_BUILD
					PRINTSTRING("          OVERLAPPING: Slot ") PRINTINT(paramCheckSlot) PRINTSTRING(" takes priority because slot ") PRINTINT(paramSlot) PRINTSTRING(" is 'always overlapped'") PRINTNL()
				#ENDIF
			ENDIF
			
			EXIT
		ENDIF
		
		// Must be the other mission marked as 'always overlap', so mark it as 'overlapped'
		IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramCheckSlot))
			Hide_MissionsAtCoords_Blip_For_Mission(paramCheckSlot)
				
			#IF IS_DEBUG_BUILD
				PRINTSTRING("          OVERLAPPING: Slot ") PRINTINT(paramSlot) PRINTSTRING(" takes priority because slot ") PRINTINT(paramCheckSlot) PRINTSTRING(" is 'always overlapped'") PRINTNL()
			#ENDIF
		ENDIF
			
		EXIT
	ENDIF
	
	// Neither must be classed as 'always overlap', so resolve them based on priorities
	MP_MISSION_ID_DATA coronaMissionIdData		= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	MP_MISSION_ID_DATA checkCoronaMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(paramCheckSlot)
	
	INT coronaPriority		= Calculate_Corona_Overlap_Priority(paramSlot, coronaMissionIdData.idCreator)
	INT checkCoronaPriority	= Calculate_Corona_Overlap_Priority(paramCheckSlot, checkCoronaMissionIdData.idCreator)
	
	IF TEMP_Check_If_Flow_Using_Mission_Subtype(coronaMissionIdData)
		IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramCheckSlot))
			Hide_MissionsAtCoords_Blip_For_Mission(paramCheckSlot)
			#IF IS_DEBUG_BUILD
				PRINTSTRING("[LOWFLOW] OVERLAPPING: Slot ") PRINTINT(paramSlot) PRINTSTRING(" is higher priority than slot ") PRINTINT(paramCheckSlot)
				PRINTSTRING(" [") PRINTINT(coronaPriority) PRINTSTRING(" > ") PRINTINT(checkCoronaPriority) PRINTSTRING("]") PRINTNL()
			#ENDIF
		ENDIF
		EXIT
	ELIF TEMP_Check_If_Flow_Using_Mission_Subtype(checkCoronaMissionIdData)
		IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramSlot))
			Hide_MissionsAtCoords_Blip_For_Mission(paramSlot)
			#IF IS_DEBUG_BUILD
				PRINTSTRING("[LOWFLOW]  OVERLAPPING: Slot ") PRINTINT(paramCheckSlot) PRINTSTRING(" is higher priority than slot ") PRINTINT(paramSlot)
				PRINTSTRING(" [") PRINTINT(checkCoronaPriority) PRINTSTRING(" > ") PRINTINT(coronaPriority) PRINTSTRING("]") PRINTNL()
			#ENDIF
		ENDIF
		EXIT
	ENDIF
	
	
	// The higher value needs marked as 'overlapped'
	// ...main corona is overlapped by other corona
	IF (coronaPriority < checkCoronaPriority)
		IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramSlot))
			Hide_MissionsAtCoords_Blip_For_Mission(paramSlot)
				
			#IF IS_DEBUG_BUILD
				PRINTSTRING("[TS] *           OVERLAPPING: Slot ") PRINTINT(paramCheckSlot) PRINTSTRING(" is higher priority than slot ") PRINTINT(paramSlot)
				PRINTSTRING(" [") PRINTINT(checkCoronaPriority) PRINTSTRING(" > ") PRINTINT(coronaPriority) PRINTSTRING("]") PRINTNL()
			#ENDIF
		ENDIF
		
		EXIT
	ENDIF

	// ...other corona is overlapped by main corona
	IF (checkCoronaPriority < coronaPriority)
		IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramCheckSlot))
			Hide_MissionsAtCoords_Blip_For_Mission(paramCheckSlot)
				
			#IF IS_DEBUG_BUILD
				PRINTSTRING("[TS] *           OVERLAPPING: Slot ") PRINTINT(paramSlot) PRINTSTRING(" is higher priority than slot ") PRINTINT(paramCheckSlot)
				PRINTSTRING(" [") PRINTINT(coronaPriority) PRINTSTRING(" > ") PRINTINT(checkCoronaPriority) PRINTSTRING("]") PRINTNL()
			#ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	// Both coronas have the same priority, so prioritise the highest 'like'
	INT coronaUserRating		= Get_User_Rating_For_FM_Cloud_Loaded_Activity(coronaMissionIdData)
	INT checkCoronaUserRating	= Get_User_Rating_For_FM_Cloud_Loaded_Activity(checkCoronaMissionIdData)
	
	// ...main corona has a lower User Rating than the other corona, so treat as overlapped
	IF (coronaUserRating < checkCoronaUserRating)
		IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramSlot))
			Hide_MissionsAtCoords_Blip_For_Mission(paramSlot)
				
			#IF IS_DEBUG_BUILD
				PRINTSTRING("[TS] *           OVERLAPPING: Slots have same priority but Slot ") PRINTINT(paramCheckSlot) PRINTSTRING(" has higher User Rating than slot ") PRINTINT(paramSlot)
				PRINTSTRING(" [") PRINTINT(checkCoronaUserRating) PRINTSTRING(" > ") PRINTINT(coronaUserRating) PRINTSTRING("]") PRINTNL()
			#ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	// ...other corona has a lower User Rating than the main corona, so treat as overlapped
	IF (checkCoronaUserRating < coronaUserRating)
		IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramCheckSlot))
			Hide_MissionsAtCoords_Blip_For_Mission(paramCheckSlot)
				
			#IF IS_DEBUG_BUILD
				PRINTSTRING("[TS] *           OVERLAPPING: Slots have same priority but Slot ") PRINTINT(paramSlot) PRINTSTRING(" has higher User Rating than slot ") PRINTINT(paramCheckSlot)
				PRINTSTRING(" [") PRINTINT(coronaUserRating) PRINTSTRING(" > ") PRINTINT(checkCoronaUserRating) PRINTSTRING("]") PRINTNL()
			#ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	// Both User Ratings must be the same, so overlap the one with the lower variation as a deadlock-breaker
	// ...main corona has a lower variation, so treat as overlapped
	IF (coronaMissionIdData.idVariation < checkCoronaMissionIdData.idVariation)
		IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramSlot))
			Hide_MissionsAtCoords_Blip_For_Mission(paramSlot)
				
			#IF IS_DEBUG_BUILD
				PRINTSTRING("[TS] *           OVERLAPPING: Slots have same priority and User Rating but Slot ") PRINTINT(paramCheckSlot) PRINTSTRING(" has higher variationID than slot ") PRINTINT(paramSlot)
				PRINTSTRING(" [") PRINTINT(checkCoronaMissionIdData.idVariation) PRINTSTRING(" takes priority over ") PRINTINT(coronaMissionIdData.idVariation) PRINTSTRING("]") PRINTNL()
			#ENDIF
		ENDIF
		
		EXIT
	ENDIF

	// ...other corona has a lower variation, so treat as overlapped
	IF (checkCoronaMissionIdData.idVariation < coronaMissionIdData.idVariation)
		IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramCheckSlot))
			Hide_MissionsAtCoords_Blip_For_Mission(paramCheckSlot)
				
			#IF IS_DEBUG_BUILD
				PRINTSTRING("[TS] *           OVERLAPPING: Slots have same priority and User Rating but Slot ") PRINTINT(paramSlot) PRINTSTRING(" has higher variationID than slot ") PRINTINT(paramCheckSlot)
				PRINTSTRING(" [") PRINTINT(coronaMissionIdData.idVariation) PRINTSTRING(" takes priority over ") PRINTINT(checkCoronaMissionIdData.idVariation) PRINTSTRING("]") PRINTNL()
			#ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	// Everything is the same which shouldn't happen - treat the other mission as overlapped and assert
	IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramCheckSlot))
		Hide_MissionsAtCoords_Blip_For_Mission(paramCheckSlot)
	ENDIF
	
	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: ERROR: Resolve_Overlapping_Missions(): Both Mission's details are the same. Slot: ")
//		PRINTINT(paramSlot)
//		PRINTSTRING(" and Slot: ")
//		PRINTINT(paramCheckSlot)
//		PRINTNL()
		SCRIPT_ASSERT("Resolve_Overlapping_Missions() - Both Mission's details are the same. This 'should' be impossible. Tell Keith.")
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Compare two missions to see if they overlap and to update the 'hidden by overlapping mission' flag as appropriate
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
//						paramAreaTriggered	TRUE if this mission is Area Triggered, otherwise FALSE
//						paramAlwaysOverlap	TRUE if this mission should be always overlapped if an overlap exists, FALSE to obey usual rules
//						paramCheckSlot		The array position of the mission being checked within the Mission At Coords array
//						paramCheckBand		The Band containing the mission being checked - only required to always give the focus mission display priority in cse of strange error
PROC Maintain_Missions_If_Overlapping(INT paramSlot, BOOL paramAreaTriggered, BOOL paramAlwaysOverlap, INT paramCheckSlot, g_eMatCBandings paramCheckBand)

	// Ignore if the checking and checked missions are the same mission
	//	(although this shouldn't happen since the mission being checked shouldn't yet have moved into the Inside Corona Range band)
	IF (paramSlot = paramCheckSlot)
		EXIT
	ENDIF
	
	// Do these missions overlap?
	BOOL checkMissionIsAreaTriggered = Is_MissionsAtCoords_Mission_Area_Triggered(paramCheckSlot)
	
	IF NOT (Do_These_Missions_Overlap(paramSlot, paramAreaTriggered, paramCheckSlot, checkMissionIsAreaTriggered))
		EXIT
	ENDIF
	
	// These missions do overlap, but if the other mission is the focus mission then it can't be marked as overlapped, so need to mark this one as overlapped
	IF (paramCheckBand = MATCB_FOCUS_MISSION)
		// ...other mission is the focus mission, so mark this one as overlapped
		IF (Set_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramSlot))
			Hide_MissionsAtCoords_Blip_For_Mission(paramSlot)
		
			#IF IS_DEBUG_BUILD
				PRINTSTRING("          OVERLAPPING: Slot ") PRINTINT(paramCheckSlot) PRINTSTRING(" takes priority as the Focus Mission") PRINTNL()
			#ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	// The missions overlap, so decide what action to take
	Resolve_Overlapping_Missions(paramSlot, paramAlwaysOverlap, paramCheckSlot)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Compare the specified mission with all missions in the display corona range and prioritise if they overlap
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
//
// NOTES:	The test mission is marked as 'not overlapped'. No other mission can be marked here as 'not overlapped' because that could undo
//				the overlap from another test. Both the main mission and another mission can be marked as 'overlapped'.
PROC Perform_MissionsAtCoords_Overlapping_Missions_Check(INT paramSlot)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Overlapping missions? Slot: ") PRINTINT(paramSlot) PRINTNL()
	#ENDIF
	
	// Gather any important details about this mission
	BOOL isAreaTriggered	= Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot)
	BOOL alwaysOverlapped	= Should_This_Mission_Be_Classed_As_Always_Overlapped(paramSlot)
	
	// Clear this mission's 'hidden by overlapping mission' flag (it may get set again)
	IF (Clear_MissionsAtCoords_Mission_As_Hidden_By_Overlapping_Corona(paramSlot))
		IF (Should_MissionsAtCoords_Blips_Be_Displayed())
			Display_MissionsAtCoords_Blip_For_Mission(paramSlot)
		ENDIF
	ENDIF
	
	// Update the overlapping status of all missions within corona display range
	INT 			firstCoronaSlot	= 0
	INT 			lastCoronaSlot	= 0
	INT 			bandLoop		= 0
	INT 			slotLoop		= 0
	g_eMatCBandings	thisBandID
	
	FOR bandLoop = MATCB_FOCUS_MISSION TO MATCB_MISSIONS_INSIDE_CORONA_RANGE
		thisBandID = INT_TO_ENUM(g_eMatCBandings, bandLoop)
		
		IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
			firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
			lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
			
			FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
				Maintain_Missions_If_Overlapping(paramSlot, isAreaTriggered, alwaysOverlapped, slotLoop, thisBandID)
			ENDFOR
		ENDIF
	ENDFOR

ENDPROC




// ===========================================================================================================
//      Mission At Coords LOCKED BITFLAG maintenance routines
// ===========================================================================================================

// PURPOSE:	KGM 22/7/15 [BUG 2421773]: Check if the currently active FM Event should still lock Gang Attacks (this is for FM Events that still allow other activities)
FUNC BOOL Should_This_Active_FM_Event_Lock_Gang_Attacks()

	// Block if Urban Warfare, Penned In, or Time Trial is still continuing
	IF (FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_KILL_LIST)
	OR (FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_PENNED_IN)
	OR (IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL))
	OR (IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL))
		// Lock
		RETURN TRUE
	ENDIF
	
	// KGM 29/7/15 [BUG 2421773]: Gang Attacks should still be blocked if the player was on some FM Events and died but the event is still running for other players.
	//	It looks like the best way to handle this is by checking if the scripts themselves are still running and then checking if the player is permanent or critical.
	//	(If they aren't permanent or critical then the GAs shouldn't be blocked - they may not have joined the event. After dying on the event they'll still be permanent or critical).
	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_KILL_LIST")) > 0)
	OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_PENNED_IN")) > 0)
		IF (FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID()))
		OR (FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID()))
			// Lock
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_GANG_ATTACKS_BE_BLOCKED_FOR_BOSS_WORK()

	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
	OR GB_IS_PLAYER_ON_ANY_GANG_BOSS_CHALLENGE(PLAYER_ID())
		IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		OR GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


// PURPOSE:	Update the Locked/Unlocked status for this mission
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
//
// NOTES:	KGM 14/1/13: The initial intention is for this to only be called when the player is outside the corona range
//			KGM 26/1/13: Now takes the temporarily unlocked flag into account - this was added so that a player could be invited to a locked activity
//			KGM 26/1/13: This may now be called as a one-off under some circumstances inside the corona range to tidy up after a temporarily unlocked mission
//			KGM 16/2/13: Some missions (race tutorial) now require the mission to remain temporarily unlocked until played.
//			KGM 28/4/13: Missions can now available only at certain hours of the day
//			KGM 7/7/13:  Missions can remain open during closing hours if another player in the session is actively playing it
PROC Maintain_MissionsAtCoords_Lock_Status_For_Mission(INT paramSlot)

	// Only update the lock status if the GameMode is a valid MP GameMode
	// NOTE: Joining a Party sets the gamemode to 'EMPTY' for a while and this caused Check_If_Mission_Locked() to unlock missions it shouldn't have
	IF (GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY)
	OR (GET_CURRENT_GAMEMODE() = GAMEMODE_SP)
		// Leave the lock status of this mission as-is until the Gamemode sorts itself out
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: GameMode is EMPTY or SP. Leaving Lock Status for this mission unchanged. Slot: ")
			PRINTINT(paramSlot)
			PRINTNL()
//			PRINTSTRING("          ")
//			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//			PRINTNL()
		#ENDIF
	
		EXIT
	ENDIF
	
	MP_MISSION_ID_DATA	theMissionIdData			= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	BOOL				missionStatusIsLocked		= Is_MissionsAtCoords_Mission_Locked(paramSlot)
	BOOL				waitForSecondaryUnlock		= Should_MissionsAtCoords_Mission_Unlock_With_Secondary_Unlock(paramSlot)
	BOOL				missionStatusShouldBeLocked	= Check_If_Mission_Locked(theMissionIdData, waitForSecondaryUnlock)

	// Check if the mission should be locked because of the player's rank, even if missions of that type are now generally unlocked
	// KGM 28/2/14: To enable CnC-specific checking I now use a new flag defaulting to TRUE, only CnC can make it FALSE, so the default behaviour is the same as it has always been
	IF NOT (missionStatusShouldBeLocked)
		BOOL checkPlayerRankAgainstMissionRank = TRUE
		// This was the original non-CnC code (but now wrapped in a checkPlayerRankAgainstMissionRank check, which wasn't needed before)
		IF (checkPlayerRankAgainstMissionRank)
			IF (g_sAtCoordsControlsMP.matccPlayerRank < Get_Rank_Required_For_MP_Mission_Request(theMissionIdData))
				missionStatusShouldBeLocked = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// KGM 25/8/13: Check if the mission should be unlocked and open if active, even if the player rank is too low or the closing time has passed
	// NOTE: the 'unlocked if active' should really use a separate flag from 'remain open if active', but they're both used by Gang Attacks so I'll use one flag for now
	BOOL UnlockedAndOpenBecauseMissionActive = FALSE
	
	IF (Should_MissionsAtCoords_Mission_Remain_Open_If_Active(paramSlot))
		IF (Is_This_Mission_Request_Active_And_Joinable(theMissionIdData))
			UnlockedAndOpenBecauseMissionActive = TRUE
		ENDIF
	ENDIF
	
	IF (missionStatusShouldBeLocked)
		IF (UnlockedAndOpenBecauseMissionActive)
			missionStatusShouldBeLocked = FALSE
		ENDIF
	ENDIF
	
	// Check if the mission should be locked because of the time of day
	BOOL keepOpenDuringClosingHours = FALSE
	IF NOT (missionStatusShouldBeLocked)
		IF NOT (Is_MissionsAtCoords_Mission_Unlocked_At_This_Time_Of_Day(paramSlot))
			keepOpenDuringClosingHours = FALSE
			
			// If the mission should be locked, check if it should remain open during closing hours if another player is actively playing it
			IF (UnlockedAndOpenBecauseMissionActive)
				keepOpenDuringClosingHours = TRUE
			ENDIF
				
			IF NOT (keepOpenDuringClosingHours)
				missionStatusShouldBeLocked = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Take the temporary unlocked flag into account
	IF (Is_MissionsAtCoords_Mission_Temporarily_Unlocked(paramSlot))
		IF (missionStatusShouldBeLocked)
			missionStatusShouldBeLocked = FALSE
		ELSE
			// ...now properly unlocked, so the temporary unlock status isn't needed
			// KGM 18/6/13: Only remove the temporary unlock if the mission is unlocked all day.
			// 		If it does have an on/off time then it can become locked at a different time of day and so would still need the temporary unlock in place.
			IF (Is_MissionsAtCoords_Mission_Unlocked_All_Day(paramSlot))
				Clear_MissionsAtCoords_Mission_As_Temporarily_Unlocked_Until_Played(paramSlot)
				Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
			ENDIF
		ENDIF
	ENDIF
	
	// Regardless of what has been decided above (including the temporary unlock flag), if the mission is invite only and an invite has not been accepted then it is locked
	IF (Is_MissionsAtCoords_Mission_Invite_Only(paramSlot))
		IF NOT (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
			// ...still locked, an invite only mission has not had an invite accepted
			missionStatusShouldBeLocked = TRUE
		ENDIF
	ENDIF
	
	// KGM 11/7/13: Special Check for specific missions that should be active during an Ambient Tutorial. All missions are usually functionally blocked during an
	//				ambient tutorial, but if any mission needs to be active during it then none can be functionally blocked - instead all should be locked except
	//				the missions that are supposed to be active which follow the usual rules.
	IF (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())

		IF (g_iMatccCounterOnDuringAmbTut > 0)
			// ...some missions want to be allowed during an Ambient Tutorial, so lock all other missions except the ones that want to be allowed
			IF NOT (Is_MissionsAtCoords_Mission_Allowed_During_Ambient_Tutorial(paramSlot))
				// ...this one doesn't want to be allowed during an ambient tutorial, so keep it locked
				missionStatusShouldBeLocked = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// KGM 7/7/15 [BUG 2409846]: SPECIAL CHECK: If the mission is a Gang Attack and the player is on an FM EVENT and the mission is unlocked, then lock it
	// KGM 22/7/15 [BUG 2382702]: ADDITIONAL CHECK: FM Events can now block specific Gang Attacks - check for those here and lock them if blocked.
	// KGM 22/7/15 [BUG 2421773]: ADDITIONAL CHECK: Even though no longer competing in the event, some FM Events don't end for a player until it ends overall, so Gang Attacks still need blocked
	IF (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
		IF (FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID()))
		OR (Is_MissionsAtCoords_Gang_Attack_Specifically_Hidden(paramSlot))
		OR (Should_This_Active_FM_Event_Lock_Gang_Attacks())
		OR (SHOULD_GANG_ATTACKS_BE_BLOCKED_FOR_BOSS_WORK())
			IF NOT (missionStatusShouldBeLocked)
				// ...this Gang Attack should be locked during the FM Event to prevent the blip from being displayed
				missionStatusShouldBeLocked = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Nothing to do if the status hasn't changed
	IF (missionStatusIsLocked = missionStatusShouldBeLocked)
		EXIT
	ENDIF
	
	// Locked status has changed, so store the new status
	IF (missionStatusShouldBeLocked)
		// Update lock status to 'Locked'
		Set_MissionsAtCoords_Mission_As_Locked(paramSlot)
		
		// Hide the blips
		Hide_MissionsAtCoords_Blip_For_Mission(paramSlot)
		
		EXIT
	ENDIF
	
	// Update lock status to be 'unlocked'
	Set_MissionsAtCoords_Mission_As_Unlocked(paramSlot)
	
	// If blips are allowed to be on display, then display the blip
	IF (Should_MissionsAtCoords_Blips_Be_Displayed())
		Display_MissionsAtCoords_Blip_For_Mission(paramSlot)
	ENDIF

ENDPROC




// ===========================================================================================================
//      Mission At Coords Corona Locate Message Routines
// ===========================================================================================================

// PURPOSE:	Get the Mission launch Retriction Text to display as part of the corona text
//
// INPUT PARAMS:		paramRestrictionID			RestrictionID
// RETURN VALUE:		TEXT_LABEL_31				The text label containing the restriction text to display
FUNC STRING Get_Corona_Locate_Message_Restriction_Text(g_eMatCLaunchFailReasonID paramRestrictionID)

	SWITCH (paramRestrictionID)
		CASE MATCLFR_RANK_TOO_LOW
			RETURN ("CLM_ERROR_RANK")
			
		CASE MATCLFR_WRONG_TIMEOFDAY
			RETURN ("CLM_ERROR_TIME")
			
		CASE MATCLFR_WANTED_LEVEL
			RETURN ("CLM_ERROR_WANTED")
			
		CASE MATCLFR_NOT_ENOUGH_CASH
			RETURN ("CLM_ERROR_CASH")
			
		CASE MATCLFR_NONE
			RETURN ("")
			
		DEFAULT
			#IF IS_DEBUG_BUILD
//				PRINTSTRING(".KGM [At Coords]: Get_Corona_Locate_Message_Restriction_Text() - ERROR: Unknown Fail Reason.") PRINTNL()
				SCRIPT_ASSERT("Get_Corona_Locate_Message_Restriction_Text(): ERROR - Unknown Fail Reason. Add to Switch Statement. Tell Keith.")
			#ENDIF
			
			BREAK
	ENDSWITCH
	
	// No Reason
	RETURN ("")

ENDFUNC




// ===========================================================================================================
//      Mission At Coords Corona Ground Projection routines
// ===========================================================================================================

// PURPOSE:	Clear a Corona Ground Projection with this RegID (if one exists)
//
// INPUT PARAMS:		paramRegID			The RegID for the mission trying to clear a ground projection
PROC Clear_MissionsAtCoords_Corona_Ground_Projection_For_RegID(INT paramRegID)

	// Nothing to do if the TXD hasn't loaded (this is handled by an every frame maintenance routine which automaticlly cleans up if necessary)
	IF NOT (Is_MatC_Corona_Ground_Projection_TXD_Loaded())
		EXIT
	ENDIF

	// TXD is loaded
	INT tempLoop = 0

	// Find an existing entry for this mission
	REPEAT MATC_CORONA_MAX_PROJECTIONS tempLoop
		IF (g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].regID = paramRegID)
			// Details found, delete the checkpoint and mark the decal for unpatching
			IF (g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].checkpointID != NULL)
				DELETE_CHECKPOINT(g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].checkpointID)
				
				g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].checkpointID	= NULL
				g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].stage			= GTXD_STAGE_UNPATCH_DECAL
				
				#IF IS_DEBUG_BUILD
					PRINTSTRING(".KGM [At Coords]: Deleted Ground TXD and marked decal to be unpatched. RegID: ")
					PRINTINT(g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].regID)
					PRINTNL()
				#ENDIF
			ENDIF
			
			g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].regID = ILLEGAL_AT_COORDS_ID
			
			#IF IS_DEBUG_BUILD
				Debug_Output_Ground_Projections()
			#ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear a Corona Ground Projection for this mission (if one exists)
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_MissionsAtCoords_Slot_Corona_Ground_Projection(INT paramSlot)

	INT myRegID = Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	
	Clear_MissionsAtCoords_Corona_Ground_Projection_For_RegID(myRegID)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup a Corona Ground Projection for this mission (if there is a free one and one is not already setup)
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
PROC Setup_MissionsAtCoords_Corona_Ground_Projection(INT paramSlot)
	// Nothing to do if the mission shouldn't display anything corona related
	IF NOT (Should_MissionsAtCoords_Mission_Display_A_Corona(paramSlot))
		EXIT
	ENDIF	
	
	// Don't draw a ground projection if it is an overlapped mission (unless on a commandline parameter)
	IF (Is_MissionsAtCoords_Mission_Hidden_By_Overlapping_Corona(paramSlot))
		BOOL quitIfOverlapped = TRUE
		
		#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_ShowOverlappingCoronas"))
				quitIfOverlapped = FALSE
			ENDIF
		#ENDIF
		
		IF (quitIfOverlapped)
			EXIT
		ENDIF
	ENDIF
		
	// KGM 12/5/15: BUG 2313555 - Don't draw a ground projection if hidden by a PI Menu Job Type Hide Blip option
	// KGM 11/6/15: BUG 2316703 - Also hide if the mission is within a disabled area
	IF (Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu(paramSlot))
	OR (Is_MissionsAtCoords_Mission_Hidden_By_Disabled_Area(paramSlot))
		EXIT
	ENDIF

	// Nothing to do if the TXD hasn't loaded (this is handled by an every frame maintenance routine)
	IF NOT (Is_MatC_Corona_Ground_Projection_TXD_Loaded())
		EXIT
	ENDIF

	// TXD is loaded
	INT						myRegID				= Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	INT						freeProjectionSlot	= ILLEGAL_ARRAY_POSITION
	INT						tempLoop			= 0
	INT						thisRegID			= ILLEGAL_AT_COORDS_ID
	g_eGroundTXDRenderStage	thisStage			= GTXD_STAGE_NOT_IN_USE

	// Find an existing entry for this mission, or a free space on the array
	// NOTE: Ignore stages where a decal is being cleaned up
	REPEAT MATC_CORONA_MAX_PROJECTIONS tempLoop
		thisRegID = g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].regID
		thisStage = g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].stage
		
		IF (thisStage = GTXD_STAGE_NOT_IN_USE)
			// Found a free slot if needed
			IF (freeProjectionSlot = ILLEGAL_ARRAY_POSITION)
				freeProjectionSlot = tempLoop
			ENDIF
		ELIF (thisStage = GTXD_STAGE_BEING_RENDERED)
			IF (thisRegID = myRegID)
				// Details already setup
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Details aren't already setup, but nothing to do if there are no free slots
	IF (freeProjectionSlot = ILLEGAL_ARRAY_POSITION)
		EXIT
	ENDIF
	
	// Found a free slot, so setup this corona with the correct texture that matches the corona icon
	eMP_TAG_SCRIPT			theIcon				= Get_MissionsAtCoords_Slot_Corona_Icon(paramSlot)
	STRING					theTexture			= Get_Ground_Projection_Texture_That_Matches_Corona_Icon(theIcon)
	VECTOR					theCoords			= << 0.0, 0.0, 0.0 >>
	HUD_COLOURS				theColour			= Get_MissionsAtCoords_Slot_Corona_Hud_Colour(paramSlot)
	CHECKPOINT_INDEX		thisCheckpointID	= NULL
	CHECKPOINT_TYPE			thisCheckpointType	= Get_MissionsAtCoords_Checkpoint_Type_For_Projection_ArrayPos(freeProjectionSlot)
	DECAL_RENDERSETTING_ID	thisDecal			= Get_MissionsAtCoords_Decal_For_Projection_ArrayPos(freeProjectionSlot)
	FLOAT					theSize				= (Get_MissionsAtCoords_Slot_Corona_Trigger_Radius(paramSlot) * 2 * MATC_CORONA_PROJECTION_SIZE_MODIFIER)
	INT						theRed				= 0
	INT						theGreen			= 0
	INT						theBlue				= 0
	INT						theAlpha			= 0

	theCoords	= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	theCoords.z += 1.0
	GET_GROUND_Z_FOR_3D_COORD(theCoords, theCoords.z)
	
	GET_HUD_COLOUR(theColour, theRed, theGreen, theBlue, theAlpha)
	theAlpha = MATC_CORONA_PROJECTION_ALPHA
	
	// Create the Checkpoint
	thisCheckpointID = CREATE_CHECKPOINT(thisCheckpointType, theCoords ,theCoords ,theSize, theRed, theGreen, theBlue, theAlpha)
		
	// Patch the Checkpoint default Decal with the correct texture
	PATCH_DECAL_DIFFUSE_MAP(thisDecal,  MATC_CORONA_PROJECTION_TXD_NAME, theTexture)
	
	// Store the details in the Projection Control Array
	g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[freeProjectionSlot].stage			= GTXD_STAGE_BEING_RENDERED
	g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[freeProjectionSlot].regID			= myRegID
	g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[freeProjectionSlot].checkpointID	= thisCheckpointID
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Displaying Ground Texture for slot: ")	PRINTINT(paramSlot)			PRINTNL()
//		PRINTSTRING("      Using Projection ArrayPos: ")								PRINTINT(freeProjectionSlot)	PRINTNL()
//		PRINTSTRING("      For MatC RegID           : ")								PRINTINT(myRegID)				PRINTNL()
//		PRINTSTRING("      Using Projection Texture : ")								PRINTSTRING(theTexture)				PRINTNL()
		Debug_Output_Ground_Projections()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Mission At Coords Global Variable Clear and Set Routines
// ===========================================================================================================

// PURPOSE:	Return an empty corona-data struct
//
// RETURN PARAMS:			paramCoronaData		The Corona Data struct to be cleared
PROC Clear_One_MP_Mission_At_Coords_Corona_Variables(g_structMatCCoronaMP &paramCoronaData)

	g_structMatCCoronaMP emptyCoronaData
	paramCoronaData = emptyCoronaData

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out one Mission At Coords control variables array position
//
// INPUT PARAMS:		paramArrayPos			The element within the array to be cleared out
PROC Clear_One_MP_Mission_At_Coords_Variables(INT paramArrayPos)

	// Ensure the blip has been cleaned up. If not, assert then clean it up.
	IF (DOES_BLIP_EXIST(g_sAtCoordsMP[paramArrayPos].matcBlipIndex))
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Clear_One_MP_Mission_At_Coords_Variables() - ERROR: Blip still in use, but it shouldn't be at this stage. Removing. Slot: ")
			PRINTINT(paramArrayPos)
			PRINTNL()
		
			SCRIPT_ASSERT("Clear_One_MP_Mission_At_Coords_Variables(): ERROR - Blip still in use when clearing Mission At Coords array details")
		#ENDIF
		
		REMOVE_BLIP(g_sAtCoordsMP[paramArrayPos].matcBlipIndex)
	ENDIF

	g_structMissionsAtCoordsMP emptyArray
	g_sAtCoordsMP[paramArrayPos] = emptyArray
	
	g_sAtCoordsMP[paramArrayPos].matcCoords		= << 0.0, 0.0, 0.0 >>
	
	Clear_One_MP_Mission_At_Coords_Corona_Variables(g_sAtCoordsMP[paramArrayPos].matcCorona)
	Clear_MP_MISSION_ID_DATA_Struct(g_sAtCoordsMP[paramArrayPos].matcMissionIdData)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out the Focus Mission additional details
PROC Clear_MP_Mission_At_Coords_Focus_Mission_Variables()

	g_structMatCFocusMissionMP emptyFocusMission
	g_sAtCoordsFocusMP = emptyFocusMission
	
	g_sAtCoordsFocusMP.focusInCoronaTimeout		= GET_NETWORK_TIME()
	g_sAtCoordsFocusMP.focusCheckSharedTimeout	= GET_NETWORK_TIME()
	g_sAtCoordsFocusMP.focusReserveTimeout		= GET_NETWORK_TIME()
	g_sAtCoordsFocusMP.focusCloudDataTimeout	= GET_NETWORK_TIME()
	
	g_structMatCFocusMissionMP_TU emptyFocusMission_TU
	g_sAtCoordsFocusMP_TU = emptyFocusMission_TU
	g_sV2CoronaVars.g_isplayer_inside_corona = FALSE
	PRINTLN("g_sV2CoronaVars.g_isplayer_inside_corona = FALSE 5")
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out one Mission At Coords feedback array position
//
// INPUT PARAMS:		paramArrayPos			The element within the array to be cleared out
PROC Clear_One_MP_MatC_Feedback_Slot(INT paramArrayPos)

	g_structMatCFeedbackMP emptyArray
	g_sAtCoordsFeedbackMP[paramArrayPos] = emptyArray
	
	g_sAtCoordsFeedbackMP[paramArrayPos].matcfbTimeout	= GET_NETWORK_TIME()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out one Mission At Coords Speed Zone array position deleting the Speed Zone if it exists
//
// INPUT PARAMS:		paramArrayPos			The element within the array to be cleared out
PROC Clear_And_Delete_One_MP_MatC_Speed_Zone(INT paramArrayPos)

	// Ensure an existing speed zone is deleted
	IF (g_sMatCSpeedZonesMP[paramArrayPos].matcszSpeedZoneID != ILLEGAL_MATC_SPEED_ZONE_ID)
		// Speed Zone exists, so delete the speed zone
		IF (REMOVE_ROAD_NODE_SPEED_ZONE(g_sMatCSpeedZonesMP[paramArrayPos].matcszSpeedZoneID))
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Speed Zone - Remove. RegID: ")
				PRINTINT(g_sMatCSpeedZonesMP[paramArrayPos].matcszRegID)
				PRINTNL()
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Speed Zone - Remove. RegID: ")
				PRINTINT(g_sMatCSpeedZonesMP[paramArrayPos].matcszRegID)
				PRINTSTRING(" - FAILED")
				PRINTNL()
			#ENDIF
		ENDIF
	ENDIF

	g_structMatCSpeedZonesMP emptyArray
	g_sMatCSpeedZonesMP[paramArrayPos] = emptyArray

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out one Mission At Coords Scenario Blocking array position deleting the Scenarion Blocking if it exists
//
// INPUT PARAMS:		paramArrayPos			The element within the array to be cleared out
PROC Clear_And_Delete_One_MP_MatC_Scenario_Blocking(INT paramArrayPos)

	// Ensure an existing scenario blocking is deleted
	IF (g_sMatCScenariosMP[paramArrayPos].matcsbScenarioBlockingID != NULL)
		// Scenario Blocking exists, so delete it
		REMOVE_SCENARIO_BLOCKING_AREA(g_sMatCScenariosMP[paramArrayPos].matcsbScenarioBlockingID)
		
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Scenario Blocking - Remove. RegID: ")
			PRINTINT(g_sMatCScenariosMP[paramArrayPos].matcsbRegID)
			PRINTNL()
		#ENDIF
	ENDIF

	g_structMatCScenarioBlockingMP emptyArray
	g_sMatCScenariosMP[paramArrayPos] = emptyArray
	
	g_sMatCScenariosMP[paramArrayPos].matcsbScenarioBlockingID	= NULL

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out all the Mission At Coords control variables
PROC Clear_All_MP_Mission_At_Coords_Variables()

	// Clear the Missions array
	INT tempLoop = 0
	REPEAT MAX_NUM_MP_MISSIONS_AT_COORDS tempLoop
		g_sAtCoordsMP[tempLoop].matcBlipIndex	= NULL
		Clear_One_MP_Mission_At_Coords_Variables(tempLoop)
	ENDREPEAT
	
	g_numAtCoordsMP = 0
	
	// Clear the Bandings array (containing pointers into the Missions array as a way to optimise processing)
	g_structMatCBandingsMP emptyBandingStruct
	REPEAT MAX_MISSIONS_AT_COORDS_BANDINGS tempLoop
		g_sAtCoordsBandsMP[tempLoop] = emptyBandingStruct
	ENDREPEAT
	
	// Clear the Stages array (containing distance a stage becomes active, and the scheduling Band teh mission should be placed in)
	g_structMatCStagesMP emptyStagesStruct
	REPEAT MAX_MISSIONS_AT_COORDS_STAGES tempLoop
		g_sAtCoordsStageInfo[tempLoop] = emptyStagesStruct
	ENDREPEAT
	
	// Clear the Focus Mission details (containing additional details for the one mission that is the current Focus Mission)
	Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
	
	// Clear any Control variables and setup the first scheduled oldest feedback timeout and the initial player rank using the expensive function
	g_structMatCControlsMP emptyControlStruct
	g_sAtCoordsControlsMP = emptyControlStruct
	
	REPEAT MATC_CORONA_MAX_PROJECTIONS tempLoop
		g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].stage			= GTXD_STAGE_NOT_IN_USE
		g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].regID			= ILLEGAL_AT_COORDS_ID
		g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].checkpointID	= NULL
	ENDREPEAT
	
	g_sAtCoordsControlsMP.matccFeedbackTimeout	= GET_TIME_OFFSET(GET_NETWORK_TIME(), MATC_FEEDBACK_CHECK_OLDEST_TIME_msec)
	g_sAtCoordsControlsMP.matccPlayerRank		= GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE)
	
	// ...additional controls
	g_structMatCControlsMP_TU	emptyDetailsTU
	g_sAtCoordsControlsMP_AddTU = emptyDetailsTU
	
	g_sAtCoordsControlsMP_AddTU.matccActivityContentID = ""
	
	#IF IS_DEBUG_BUILD
		g_sAtCoordsControlsMP_AddTU.matccDebugInviteAcceptContentID = ""
	#ENDIF
	
	// Clear the Feedback array
	REPEAT MAX_NUM_MP_MATC_FEEDBACK_SLOTS tempLoop
		Clear_One_MP_MatC_Feedback_Slot(tempLoop)
	ENDREPEAT
	
	g_numAtCoordsFeedbackMP = 0
	
	// Clear the SpeedZones array
	REPEAT MAX_MATC_SPEED_ZONES tempLoop
		g_sMatCSpeedZonesMP[temploop].matcszSpeedZoneID	= ILLEGAL_MATC_SPEED_ZONE_ID
		Clear_And_Delete_One_MP_MatC_Speed_Zone(tempLoop)
	ENDREPEAT
	
	// Clear the Scenario Blocking array
	REPEAT MAX_MATC_SCENARIO_BLOCKING tempLoop
		g_sMatCScenariosMP[temploop].matcsbScenarioBlockingID	= NULL
		Clear_And_Delete_One_MP_MatC_Scenario_Blocking(tempLoop)
	ENDREPEAT
	
	// Clear the AngledAreas array
	REPEAT MAX_MATC_ANGLED_AREAS tempLoop
		g_sMatCAngledAreasMP[tempLoop].matcaaRegID		= ILLEGAL_AT_COORDS_ID
		g_sMatCAngledAreasMP[tempLoop].matcaaMin		= << 0.0, 0.0, 0.0 >>
		g_sMatCAngledAreasMP[tempLoop].matcaaMax		= << 0.0, 0.0, 0.0 >>
		g_sMatCAngledAreasMP[tempLoop].matcaaWidth		= 0.0
		g_sMatCAngledAreasMP_TitleUpdate[tempLoop].matcaaBlipPos	= << 0.0, 0.0, 0.0 >>
	ENDREPEAT
	g_numMatcAngledAreas = 0
	
	// Clear the InCorona struct
	g_structMatCInCoronaMP emptyInCoronaStruct
	g_sAtCoordsInCoronaMP = emptyInCoronaStruct
	g_sAtCoordsInCoronaMP.matcicCoords							= << 0.0, 0.0, 0.0 >>
	g_sAtCoordsInCoronaMP.matcicMissionIdData.idCloudFilename	= ""
	
	// Clear the Initial Actions structs
	// ...original
	g_structMatcInitialActionsMP emptyInitialActionsStruct
	g_sMatcInitialActions = emptyInitialActionsStruct

	g_structMatcInitialBlipsMP_OLD	emptyInitialBlipsStruct
	g_structMatcInitialSpawnMP	emptyInitialSpawnStruct
	g_sMatcInitialActions.matciaLongRangeBlips		= emptyInitialBlipsStruct
	g_sMatcInitialActions.matciaSpawnCoords			= emptyInitialSpawnStruct
	g_sMatcInitialActions.matciaScheduledTimeout	= GET_TIME_OFFSET(GET_NETWORK_TIME(), INITIAL_ACTIONS_SCHEDULING_DELAY_msec)
	
	// ...TU additional
	g_structMatcInitialActionsMP_TU emptyInitialActionsStruct_TU
	g_sMatcInitialActions_TU = emptyInitialActionsStruct_TU
	
	g_structMatcInitialBlipsMP_TU	emptyInitialBlipsStruct_TU
	g_sMatcInitialActions_TU.matciaLongRangeBlips	= emptyInitialBlipsStruct_TU
	
	
	// Clear the Area-Triggered Blip variables
	g_structMatcRadiusBlip emptyAreaTriggeredBlip
	g_sMatcRadiusBlip = emptyAreaTriggeredBlip
	g_sMatcRadiusBlip.mrbBlipIndex = NULL
	
	// Clear the Retained GangAttack radius blip
	g_sMatcRetainedRadiusBlip = emptyAreaTriggeredBlip
	g_sMatcRetainedRadiusBlip.mrbBlipIndex = NULL
	g_removeRetainedGABlip = FALSE
	
	// Clear the Nearby Long-Range Blips variables
	g_structAllLongRangeBlips emptyAllLongRangeBlipsStruct
	g_sLongRangeBlips = emptyAllLongRangeBlipsStruct
	
	g_structLongRangeBlipsOfType emptyLongRangeBlipsOfType
	g_structOneLongRangeBlip emptyOneLongRangeBlipStruct
	
	INT blipsOfTypeLoop = 0
	REPEAT MAX_LONG_RANGE_BLIP_TYPES blipsOfTypeLoop
		g_sLongRangeBlips.allBlips[blipsOfTypeLoop] = emptyLongRangeBlipsOfType
		
		INT blipLoop = 0
		REPEAT MAX_NUM_LONG_RANGE_BLIPS_PER_TYPE blipLoop
			g_sLongRangeBlips.allBlips[blipsOfTypeLoop].blipsOfType[blipLoop] = emptyOneLongRangeBlipStruct
		ENDREPEAT
	ENDREPEAT

	// Clear the Corona Visuals data
	g_structFocusMissionVisuals		emptyFocusMissionVisuals
	
	g_sCoronaFocusVisuals					= emptyFocusMissionVisuals
	g_sCoronaFocusVisuals.coronaPosition	= << 0.0, 0.0, 0.0 >>
	
	// Clear the Quick Update All Missions variables
	g_structQuickUpdateAllMissions	emptyQuickUpdateStruct
	
	g_sMatcQuickUpdate = emptyQuickUpdateStruct
	
	// Clear the 'Cleared Wanted Level' values
	Clear_MissionsAtCoords_Cleared_Wanted_Level()
	
	// Clear the Missions At Coords exclusive contentID variables
	Clear_MissionsAtCoords_Exclusive_ContentID()
	

	// Debug confirmation
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Clear_MP_Mission_At_Coords_Variables() - All details cleared") PRINTNL()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Mission At Coords Speed Zone Routines
// ===========================================================================================================

// PURPOSE:	Check if a speed zone already exists for this mission
//
// INPUT PARAMS:			paramRegID			The MissionsAtCoords registrationID
// RETURN VALUE:			BOOL				TRUE if the registrationID already exists
FUNC BOOL Does_MissionsAtCoords_SpeedZone_Exist_For_Mission(INT paramRegID)

	INT tempLoop = 0
	
	REPEAT MAX_MATC_SPEED_ZONES tempLoop
		IF (g_sMatCSpeedZonesMP[tempLoop].matcszSpeedZoneID != ILLEGAL_MATC_SPEED_ZONE_ID)
			IF (g_sMatCSpeedZonesMP[tempLoop].matcszRegID = paramRegID)
				// Found
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not found
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Add a new speed zone for the specified mission RegID at the specified Speed Zone array position
//
// INPUT PARAMS:			paramSlot					The array position for the mission within the Mission At Coords array
//							paramSpeedZoneArrayPos		The MissionsAtCoords speed zone array position
PROC private_Add_MissionsAtCoords_SpeedZone_At_ArrayPosition(INT paramSlot, INT paramSpeedZoneArrayPos)

	VECTOR	speedZoneCoords			= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	FLOAT	speedZoneRadius			= Get_MissionsAtCoords_Slot_Corona_Trigger_Radius(paramSlot) + CORONA_GENERAL_COSMETIC_LEEWAY_m
	BOOL	affectMissionVehicles	= FALSE	//TRUE - [RLB] SWAPPED TO FALSE FOR BUG 1608567
	INT		regID					= Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	
	g_sMatCSpeedZonesMP[paramSpeedZoneArrayPos].matcszSpeedZoneID	= ADD_ROAD_NODE_SPEED_ZONE(speedZoneCoords, speedZoneRadius, MATC_SPEED_ZONE_MAX_SPEED, affectMissionVehicles)
	g_sMatCSpeedZonesMP[paramSpeedZoneArrayPos].matcszRegID			= regID
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Speed Zone - Add. RegID: ") PRINTINT(regID)
		PRINTSTRING(".  Coords: ") PRINTVECTOR(speedZoneCoords)
		PRINTSTRING(". Radius: ") PRINTFLOAT(speedZoneRadius)
		PRINTSTRING(". Speed: ") PRINTFLOAT(MATC_SPEED_ZONE_MAX_SPEED)
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup a new Speed Zone
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//							paramMustSucceed	[DEFAULT = FALSE] TRUE if this request must succeed, otherwise FALSE
//
// NOTES:	The 'must succeed' flag is used in case the mission becomes the Focus Mission and the array was full so no speed zone got set up - the Focus Mission must have one
PROC Setup_MissionsAtCoords_SpeedZone(INT paramSlot, BOOL paramMustSucceed = FALSE)

	// KGM 19/5/13: Making an assumption that an area-triggered mission won't need a speedzone
	IF (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: SpeedZone not required for Area-Triggered Mission") PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF

	// KGM 9/6/13: POSSIBLY TEMP: Immediate Launch missions not getting a speedzone - this may need to change to set one up at the player's position
	IF (Should_MissionsAtCoords_Mission_Launch_Immediately(paramSlot))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: SpeedZone not being setup for a mission that is Launching Immediately") PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF

	INT regID = Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	
	IF (Does_MissionsAtCoords_SpeedZone_Exist_For_Mission(regID))
		EXIT
	ENDIF
	
	// Setup a new speed zone
	INT tempLoop = 0
	
	REPEAT MAX_MATC_SPEED_ZONES tempLoop
		IF (g_sMatCSpeedZonesMP[tempLoop].matcszSpeedZoneID = ILLEGAL_MATC_SPEED_ZONE_ID)
			// ...found an empty speed zone slot
			private_Add_MissionsAtCoords_SpeedZone_At_ArrayPosition(paramSlot, tempLoop)

			EXIT
		ENDIF
	ENDREPEAT
	
	// Array full
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: All MatC Speed Zone slots already in use. Failed to find free Speed Zone slot for mission:") PRINTNL()
//		PRINTSTRING("          ") Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot) PRINTNL()
//	#ENDIF
	
	// Should the speed zone be forced?
	IF NOT (paramMustSucceed)
		EXIT
	ENDIF
	
	// This mission must have a speed zone, so randomly free up a slot
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING("         BUT: This mission MUST HAVE a speed zone set up, so choosing a random one to cleanup") PRINTNL()
//	#ENDIF
	
	tempLoop = GET_RANDOM_INT_IN_RANGE(0, MAX_MATC_SPEED_ZONES)
	Clear_And_Delete_One_MP_MatC_Speed_Zone(tempLoop)
	
	// Register this Speed Zone in the slot
	private_Add_MissionsAtCoords_SpeedZone_At_ArrayPosition(paramSlot, tempLoop)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Remove a Speed Zone
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Remove_MissionsAtCoords_SpeedZone(INT paramSlot)

	INT regID = Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	
	// Find an existing speed zone and remove it
	INT tempLoop = 0
	
	REPEAT MAX_MATC_SPEED_ZONES tempLoop
		IF (g_sMatCSpeedZonesMP[tempLoop].matcszSpeedZoneID != ILLEGAL_MATC_SPEED_ZONE_ID)
			IF (g_sMatCSpeedZonesMP[tempLoop].matcszRegID = regID)
				// Found it
				Clear_And_Delete_One_MP_MatC_Speed_Zone(tempLoop)
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC




// ===========================================================================================================
//      Mission At Coords Scenario Blocking Routines
// ===========================================================================================================

// PURPOSE:	Check if scenario blocking already exists for this mission
//
// INPUT PARAMS:			paramRegID			The MissionsAtCoords registrationID
// RETURN VALUE:			BOOL				TRUE if the registrationID already exists
FUNC BOOL Does_MissionsAtCoords_ScenarioBlocking_Exist_For_Mission(INT paramRegID)

	INT tempLoop = 0
	
	REPEAT MAX_MATC_SCENARIO_BLOCKING tempLoop
		IF (g_sMatCScenariosMP[tempLoop].matcsbScenarioBlockingID != NULL)
			IF (g_sMatCScenariosMP[tempLoop].matcsbRegID = paramRegID)
				// Found
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not found
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Add a new scenario blocking for the specified mission RegID at the specified Scenario Blocking array position
//
// INPUT PARAMS:			paramSlot							The array position for the mission within the Mission At Coords array
//							paramScenarioBlockingArrayPos		The MissionsAtCoords scenario blocking array position
PROC private_Add_MissionsAtCoords_ScenarioBlocking_At_ArrayPosition(INT paramSlot, INT paramScenarioBlockingArrayPos)

	VECTOR	missionCoords			= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	FLOAT	blockingRange			= Get_MissionsAtCoords_Slot_Corona_Trigger_Radius(paramSlot) + CORONA_GENERAL_COSMETIC_LEEWAY_m
	INT		regID					= Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	
	// Calculate the minimum and maximum coords for the area based on the mission coords
	VECTOR	minimumCoords			= missionCoords
	VECTOR	maximumCoords			= missionCoords
	
	minimumCoords.x	-= blockingRange
	minimumCoords.y -= blockingRange
	minimumCoords.z -= CORONA_GENERAL_COSMETIC_LEEWAY_m
	
	maximumCoords.x	+= blockingRange
	maximumCoords.y += blockingRange
	maximumCoords.z += CORONA_GENERAL_COSMETIC_LEEWAY_m
	
	g_sMatCScenariosMP[paramScenarioBlockingArrayPos].matcsbScenarioBlockingID	= ADD_SCENARIO_BLOCKING_AREA(minimumCoords, maximumCoords)
	g_sMatCScenariosMP[paramScenarioBlockingArrayPos].matcsbRegID				= regID
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Scenario Blocking - Add. RegID: ") PRINTINT(regID)
		PRINTSTRING(". Area: ") PRINTVECTOR(minimumCoords)
		PRINTSTRING(" to ") PRINTVECTOR(maximumCoords)
		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup a new Scenario Blocking
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//							paramMustSucceed	[DEFAULT = FALSE] TRUE if this request must succeed, otherwise FALSE
//
// NOTES:	The 'must succeed' flag is used in case the mission becomes the Focus Mission and the array was full so no scenario blocking got set up - the Focus Mission must have one
PROC Setup_MissionsAtCoords_ScenarioBlocking(INT paramSlot, BOOL paramMustSucceed = FALSE)

	// Making an assumption that an area-triggered mission won't need a scenarioblocking
	IF (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Scenario Blocking not required for Area-Triggered Mission") PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF

	// Immediate Launch missions not getting a speedzone - this may need to change to set one up at the player's position
	IF (Should_MissionsAtCoords_Mission_Launch_Immediately(paramSlot))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Scenario Blocking not being setup for a mission that is Launching Immediately") PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF

	INT regID = Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	
	IF (Does_MissionsAtCoords_ScenarioBlocking_Exist_For_Mission(regID))
		EXIT
	ENDIF
	
	// Setup a new scenario blocking
	INT tempLoop = 0
	
	REPEAT MAX_MATC_SCENARIO_BLOCKING tempLoop
		IF (g_sMatCScenariosMP[tempLoop].matcsbScenarioBlockingID = NULL)
			// ...found an empty scenario blocking slot
			private_Add_MissionsAtCoords_ScenarioBlocking_At_ArrayPosition(paramSlot, tempLoop)

			EXIT
		ENDIF
	ENDREPEAT
	
	// Array full
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: All MatC Scenario Blocking slots already in use. Failed to find free Scenario Blocking slot for mission:") PRINTNL()
//		PRINTSTRING("          ") Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot) PRINTNL()
//	#ENDIF
	
	// Should the scenario blocking be forced?
	IF NOT (paramMustSucceed)
		EXIT
	ENDIF
	
	// This mission must have scenario blocking, so randomly free up a slot
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING("         BUT: This mission MUST HAVE Scenario Blocking set up, so choosing a random one to cleanup") PRINTNL()
//	#ENDIF
	
	tempLoop = GET_RANDOM_INT_IN_RANGE(0, MAX_MATC_SCENARIO_BLOCKING)
	Clear_And_Delete_One_MP_MatC_Scenario_Blocking(tempLoop)
	
	// Register this Scenario Blocking in the slot
	private_Add_MissionsAtCoords_ScenarioBlocking_At_ArrayPosition(paramSlot, tempLoop)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Remove Scenario Blocking
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Remove_MissionsAtCoords_ScenarioBlocking(INT paramSlot)

	INT regID = Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	
	// Find an existing scenario blocking and remove it
	INT tempLoop = 0
	
	REPEAT MAX_MATC_SCENARIO_BLOCKING tempLoop
		IF (g_sMatCScenariosMP[tempLoop].matcsbScenarioBlockingID != NULL)
			IF (g_sMatCScenariosMP[tempLoop].matcsbRegID = regID)
				// Found it
				Clear_And_Delete_One_MP_MatC_Scenario_Blocking(tempLoop)
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC




// ===========================================================================================================
//      Mission At Coords Angled Area Routines
// ===========================================================================================================

// PURPOSE:	Register a mission with an Angled Area
//
// INPUT PARAMS:			paramRegID			The MatcRegID
//							paramBlipPos		The Blip Coords
// REFERENCE PARAMS:		refOptions			The Options struct containing the Angled Area details
PROC Store_MissionsAtCoords_Angled_Area(INT paramRegID, VECTOR paramBlipPos, g_structMatCOptionsMP &refOptions)

	// Find a free slot in the array
	INT tempLoop = 0
	
	REPEAT MAX_MATC_ANGLED_AREAS tempLoop
		IF (g_sMatCAngledAreasMP[tempLoop].matcaaRegID = ILLEGAL_AT_COORDS_ID)
			// Found a Free Slot
			g_sMatCAngledAreasMP[tempLoop].matcaaRegID		= paramRegID
			g_sMatCAngledAreasMP[tempLoop].matcaaMin		= refOptions.matcoAngledAreaMin
			g_sMatCAngledAreasMP[tempLoop].matcaaMax		= refOptions.matcoAngledAreaMax
			g_sMatCAngledAreasMP[tempLoop].matcaaWidth		= refOptions.matcoAngledAreaWidth
			g_sMatCAngledAreasMP_TitleUpdate[tempLoop].matcaaBlipPos	= paramBlipPos
			
//			#IF IS_DEBUG_BUILD
//				PRINTSTRING(".KGM [At Coords]: Angled Area - Store. RegID: ") PRINTINT(paramRegID)
//				PRINTSTRING(". Min: ") PRINTVECTOR(g_sMatCAngledAreasMP[tempLoop].matcaaMin)
//				PRINTSTRING(". Max: ") PRINTVECTOR(g_sMatCAngledAreasMP[tempLoop].matcaaMax)
//				PRINTSTRING(". Width: ") PRINTFLOAT(g_sMatCAngledAreasMP[tempLoop].matcaaWidth)
//				PRINTSTRING(". BlipPos: ") PRINTVECTOR(g_sMatCAngledAreasMP_TitleUpdate[tempLoop].matcaaBlipPos)
//				PRINTNL()
//			#ENDIF
			
			g_numMatcAngledAreas++
			
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find a free slot
	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Failed to find a free slot on Angled Area array. Contents:")
//		REPEAT MAX_MATC_ANGLED_AREAS tempLoop
//			PRINTSTRING("      ")
//			PRINTINT(tempLoop)
//			IF (tempLoop < 10)
//				PRINTSTRING(" ")
//			ENDIF
//			IF (tempLoop < 100)
//				PRINTSTRING(" ")
//			ENDIF
//			PRINTSTRING(" Min: ") PRINTVECTOR(g_sMatCAngledAreasMP[tempLoop].matcaaMin)
//			PRINTSTRING(" Max: ") PRINTVECTOR(g_sMatCAngledAreasMP[tempLoop].matcaaMax)
//			PRINTSTRING(" Width: ") PRINTFLOAT(g_sMatCAngledAreasMP[tempLoop].matcaaWidth)
//			PRINTNL()
//		ENDREPEAT
		SCRIPT_ASSERT("Store_MissionsAtCoords_Angled_Area(): ERROR - Failed to find a free slot on the Angled Area array. See Console Log. Tell Keith.")
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Angled Area details for a RegID
//
// INPUT PARAMS:		paramRegID					MatcRegID
// RETURN VALUE:		INT							Array position of a set of Angled Area details, or ILLEGAL_ARRAY_POSITION
FUNC INT Get_MissionsAtCoords_Angled_Area_ArrayPos_For_RegID(int paramRegID)

	INT tempLoop = 0
	
	REPEAT g_numMatcAngledAreas tempLoop
		IF (paramRegID = g_sMatCAngledAreasMP[tempLoop].matcaaRegID)
			RETURN tempLoop
		ENDIF
	ENDREPEAT
	
	// Not Found
	RETURN (ILLEGAL_ARRAY_POSITION)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Angled Area details for a Slot
//
// INPUT PARAMS:		paramSlot				The array position for the mission within the Mission At Coords array
// RETURN VALUE:		INT						Array position of a set of Angled Area details, or ILLEGAL_ARRAY_POSITION
FUNC INT Get_MissionsAtCoords_Angled_Area_ArrayPos_For_Slot(int paramSlot)
	INT regID = Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	RETURN (Get_MissionsAtCoords_Angled_Area_ArrayPos_For_RegID(regID))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Delete the Angled Area details for a Slot
//
// INPUT PARAMS:		paramSlot				The array position for the mission within the Mission At Coords array
PROC Delete_MissionsAtCoords_Angled_Area_ArrayPos_For_Slot(int paramSlot)

	INT regID = Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	INT aaArrayPos = Get_MissionsAtCoords_Angled_Area_ArrayPos_For_RegID(regID)
	
	IF (aaArrayPos = ILLEGAL_ARRAY_POSITION)
		EXIT
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Cleared Angled Area for RegID: ") PRINTINT(regID) PRINTNL()
//	#ENDIF
	
	// Clear the slot details
	g_sMatCAngledAreasMP[aaArrayPos].matcaaRegID	= ILLEGAL_AT_COORDS_ID
	g_sMatCAngledAreasMP[aaArrayPos].matcaaMin		= << 0.0, 0.0, 0.0 >>
	g_sMatCAngledAreasMP[aaArrayPos].matcaaMax		= << 0.0, 0.0, 0.0 >>
	g_sMatCAngledAreasMP[aaArrayPos].matcaaWidth	= 0.0
	g_sMatCAngledAreasMP_TitleUpdate[aaArrayPos].matcaaBlipPos	= << 0.0, 0.0, 0.0 >>
			
	g_numMatcAngledAreas--
	
	// Mark the mission as not being area-triggered
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_AREA_TRIGGERED)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is inside the angled area
//
// INPUT PARAMS:		paramSlot				The array position for the mission within the Mission At Coords array
// RETURN VALUE:		BOOL					TRUE if the player is inside the angled area, FALSE if not
FUNC BOOL Is_Player_Inside_MissionAtCoords_Angled_Area(INT paramSlot)

	INT areaIndex = Get_MissionsAtCoords_Angled_Area_ArrayPos_For_Slot(paramSlot)
	IF (areaIndex = ILLEGAL_ARRAY_POSITION)
		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Mission is classed as Area-Triggered but has no area details stored.") PRINTNL()
//			PRINTSTRING("      ")
//			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//			PRINTNL()
			SCRIPT_ASSERT("Is_Player_Inside_MissionAtCoords_Angled_Area() - ERROR: No Angled Area information is stored. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (IS_PED_INJURED(PLAYER_PED_ID()))
		RETURN FALSE
	ENDIF

	VECTOR	theMin		= g_sMatCAngledAreasMP[areaIndex].matcaaMin
	VECTOR	theMax		= g_sMatCAngledAreasMP[areaIndex].matcaaMax
	FLOAT	theWidth	= g_sMatCAngledAreasMP[areaIndex].matcaaWidth
	
	RETURN (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), theMin, theMax, theWidth))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Return the Angled Area for the slot (this is really for external use by the Gang Attack activity selector routines)
//
// INPUT PARAMS:		paramSlot			The array position for the mission within the Mission At Coords array
// REFERENCE PARAMS:	refMin				The Minimum coords for the angled area
//						refMax				The Maximum coords for the angled area
//						refWidth			The Width of the angled area
// RETURN VALUE			BOOL				TRUE if the angled area exists, FALSE if not
FUNC BOOL Get_MissionsAtCoords_Slot_Angled_Area(INT paramSlot, VECTOR &refMin, VECTOR &refMax, FLOAT &refWidth)

	INT areaIndex = Get_MissionsAtCoords_Angled_Area_ArrayPos_For_Slot(paramSlot)
	IF (areaIndex = ILLEGAL_ARRAY_POSITION)
		RETURN FALSE
	ENDIF

	refMin		= g_sMatCAngledAreasMP[areaIndex].matcaaMin
	refMax		= g_sMatCAngledAreasMP[areaIndex].matcaaMax
	refWidth	= g_sMatCAngledAreasMP[areaIndex].matcaaWidth
	
	RETURN TRUE

ENDFUNC



// ===========================================================================================================
//      Mission At Coords Vehicle Generator routines
// ===========================================================================================================

// PURPOSE: Switch state of vehicle generators near the corona (and clear the area of vehicles if switching off)
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//							paramMakeActive		TRUE if the vehicle generators are being switched back on, FALSE if they are being blocked
PROC Change_Active_State_Of_Vehicles_Generators_Near_MissionsAtCoords_Corona(INT paramSlot, BOOL paramMakeActive)

	// Check consistency
	BOOL	currentlyBlocked	= IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_VEHICLE_GENERATORS_BLOCKED)
	BOOL	currentlyActive		= NOT (currentlyBlocked)
	
	IF (currentlyActive = paramMakeActive)
		// The current state and the requested state are the same, so no change is required - I'm going to flag this as an error if trying to block vehicle generators twice without lifting the block
		#IF IS_DEBUG_BUILD
			IF NOT (paramMakeActive)
//				PRINTSTRING(".KGM [At Coords]: Change_Active_State_Of_Vehicles_Generators_Near_MissionsAtCoords_Corona(): IGNORING. Trying to block the same vehicle generators twice.") PRINTNL()
//				PRINTSTRING("         ")
//				Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//				PRINTNL()
				SCRIPT_ASSERT("Change_Active_State_Of_Vehicles_Generators_Near_MissionsAtCoords_Corona() - Trying to block the same vehicle generators twice. See Console Log. Tell Keith.")
			ENDIF
		#ENDIF
		
		EXIT
	ENDIF

	VECTOR	minCoords		= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	VECTOR	maxCoords		= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	FLOAT	theRange		= Get_MissionsAtCoords_Slot_Corona_Trigger_Radius(paramSlot) + CORONA_GENERAL_COSMETIC_LEEWAY_m
	BOOL	syncOverNetwork	= FALSE
	
	// Adjust the min and max coords to cover an area
	minCoords.x -= theRange
	minCoords.y -= theRange
	minCoords.z -= CORONA_GENERAL_COSMETIC_LEEWAY_m
	
	maxCoords.x += theRange
	maxCoords.y += theRange
	maxCoords.z += CORONA_GENERAL_COSMETIC_LEEWAY_m
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(minCoords, maxCoords, paramMakeActive, syncOverNetwork)
	
	// Clear the area of cars if vehicle generators are being switched off
	IF NOT (paramMakeActive)
	AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
	AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_CHALLENGE(PLAYER_ID())
		CLEAR_AREA_OF_VEHICLES(Get_MissionsAtCoords_Slot_Coords(paramSlot), theRange)
	ENDIF
	
	// Update the state of the bitflag
	IF (paramMakeActive)
		// Making generators active, so changing bitflag state to NOT-BLOCKED
		CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_VEHICLE_GENERATORS_BLOCKED)
	ELSE
		// Blocking generators, so changing bitflag to BLOCKED
		SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_VEHICLE_GENERATORS_BLOCKED)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Changing State of Vehicle Generators near corona to: ")
		IF (paramMakeActive)
			PRINTSTRING(" Allow Generators.")
		ELSE
			PRINTSTRING(" Disable Generators.")
		ENDIF
		PRINTSTRING(" AREA FROM: ")
		PRINTVECTOR(minCoords)
		PRINTSTRING("   TO: ")
		PRINTVECTOR(maxCoords)
		PRINTSTRING(". Slot: ")
		PRINTINT(paramSlot)
		PRINTNL()
//		PRINTSTRING("         ")
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//		PRINTNL()
	#ENDIF

ENDPROC



// ===========================================================================================================
//      Mission At Coords Feedback routines
// ===========================================================================================================

// PURPOSE:	Delete the oldest entry in the Feedback array
// NOTES:	The array is sorted oldest first, so just delete array position 0 and shuffle up the entries
PROC Delete_MissionsAtCoords_Oldest_Feedback_Entry()

	IF (g_numAtCoordsFeedbackMP = 0)
		EXIT
	ENDIF
	
	INT toPos	= 0
	INT fromPos	= toPos + 1
	
	WHILE (fromPos < g_numAtCoordsFeedbackMP)
		g_sAtCoordsFeedbackMP[toPos] = g_sAtCoordsFeedbackMP[fromPos]
		toPos++
		fromPos++
	ENDWHILE
	
	// Clear the last entry
	Clear_One_MP_MatC_Feedback_Slot(toPos)
	
	// Reduce the number of entries in the array
	g_numAtCoordsFeedbackMP--

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Delete the oldest entry if its timeout has expired
PROC Maintain_MissionsAtCoords_Feedback_Entries()

	IF (g_numAtCoordsFeedbackMP = 0)
		EXIT
	ENDIF
	
	// Is it time to schedule a new timeout (this will be immediate if there hasn't been any entries in the queue for a while, but it'll trigger the scheduling again)
	IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sAtCoordsControlsMP.matccFeedbackTimeout))
		EXIT
	ENDIF
	
	// Perform an update, so re-start the scheduling timer
	g_sAtCoordsControlsMP.matccFeedbackTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), MATC_FEEDBACK_CHECK_OLDEST_TIME_msec)
	
	// Has the oldest entry expired?
	INT oldestEntrySlot = 0
	IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sAtCoordsFeedbackMP[oldestEntrySlot].matcfbTimeout))
		EXIT
	ENDIF
	
	// Timeout has occurred, so delete the oldest entry
	Delete_MissionsAtCoords_Oldest_Feedback_Entry()
	
	// Output the updated array
//	#IF IS_DEBUG_BUILD
//		PRINTNL()
//		PRINTSTRING(".KGM [At Coords]: Oldest Feedback Entry Timeout has expired - entry deleted. Updated Feedback Array Contents: ") PRINTNL()
//		Debug_Output_Missions_At_Coords_Feedback_Array()
//	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Find a free Feedback array slot for new data
//
// RETURN VALUE:			INT					The free slot
//
// NOTES:	If there are no free slots, output an assert then delete the oldest entry
FUNC INT Get_Free_MissionsAtCoords_Feedback_Slot()

	// If the array isn't full, pass back the current max which will also point to the next free array position
	IF (g_numAtCoordsFeedbackMP < MAX_NUM_MP_MATC_FEEDBACK_SLOTS)
		RETURN (g_numAtCoordsFeedbackMP)
	ENDIF
	
	// There are no free slots, so output the details of the feedback array to the console log
	#IF IS_DEBUG_BUILD
//		PRINTNL()
//		PRINTSTRING(".KGM [At Coords]: FAILED TO FIND A FREE FEEDBACK ARRAY POSITION. Current Feedback Array Contents: ") PRINTNL()
//		Debug_Output_Missions_At_Coords_Feedback_Array()
		SCRIPT_ASSERT("Get_Free_MissionsAtCoords_Feedback_Slot() - ERROR: The Feedback Array is full. See Console log. About to delete oldest entry so processing can continue. Tell Keith.")
	#ENDIF
	
	Delete_MissionsAtCoords_Oldest_Feedback_Entry()
	
	// Output the details again after the oldest entry has been deleted
//	#IF IS_DEBUG_BUILD
//		PRINTNL()
//		PRINTSTRING(".KGM [At Coords]: Array contents after deleting Oldest Feedback Entry to make space in the array: ") PRINTNL()
//		Debug_Output_Missions_At_Coords_Feedback_Array()
//	#ENDIF
	
	// There will now be a free slot at the end of the array
	RETURN (g_numAtCoordsFeedbackMP)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check for identical feedback is in the array
//
// INPUT PARAMS:			paramRegID			The registrationID for this mission entry
//							paramType			The Feedback Type
// RETURN VALUE:			BOOL				TRUE if the details exist, otherwise FALSE
FUNC BOOL Does_MissionsAtCoords_Feedback_Exist(INT paramRegID, g_eMatCFeedbackType paramType)

	// Bomb out early if there are no entries in the array
	IF (g_numAtCoordsFeedbackMP = 0)
		RETURN FALSE
	ENDIF

	// Ensure the type is valid
	SWITCH (paramType)
		// Valid Types
		CASE MATCFBT_FINISHED
		CASE MATCFBT_STARTED
			BREAK
			
		// Ignore No Type
		CASE MATCFBT_NONE
			#IF IS_DEBUG_BUILD
//				PRINTSTRING(".KGM [At Coords]: Does_MissionsAtCoords_Feedback_Exist() - ERROR: Checking for MATCFBT_NONE.") PRINTNL()
				SCRIPT_ASSERT("ERROR: Feedback Type checking for MATCFBT_NONE. Tell Keith.")
			#ENDIF
			
			RETURN FALSE
			
		// Unknown Types
		DEFAULT
			#IF IS_DEBUG_BUILD
//				PRINTSTRING(".KGM [At Coords]: Does_MissionsAtCoords_Feedback_Exist() - ERROR: Unknown Feedback Type. Add to SWITCH statement.") PRINTNL()
				SCRIPT_ASSERT("ERROR: Unknown Feedback Type. Add to SWITCH statement. Tell Keith.")
			#ENDIF
			
			RETURN FALSE
	ENDSWITCH
	
	// Search the array for these details
	INT	tempLoop	= 0
	REPEAT g_numAtCoordsFeedbackMP tempLoop
		IF (g_sAtCoordsFeedbackMP[tempLoop].matcfbRegID = paramRegID)
			IF (g_sAtCoordsFeedbackMP[tempLoop].matcfbType = paramType)
				// Found it
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not found
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Adds a new item of mission feedback to the feedback array
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//							paramType			The type of feedback
PROC Add_MissionsAtCoords_Feedback(INT paramSlot, g_eMatCFeedbackType paramType)

	// Retrieve the registration ID from the slot
	INT regID = Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)

	// Ensure the RegistrationID is valid - debug only, its from an internal function call
	#IF IS_DEBUG_BUILD
		IF (regID = ILLEGAL_AT_COORDS_ID)
//			PRINTSTRING(".KGM [At Coords]: Add_MissionsAtCoords_Feedback() - Illegal RegistrationID: ")
//			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//			PRINTNL()
			
			SCRIPT_ASSERT("Add_MissionsAtCoords_Feedback(): ERROR - Illegal RegistrationID. See console log. Tell Keith.")
			
			EXIT
		ENDIF
		
	#ENDIF
	
	// Ensure the Feedback Type is valid
	IF (paramType = MATCFBT_NONE)
		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Add_MissionsAtCoords_Feedback() - Illegal Feedback Type: ")
//			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//			PRINTNL()
			
			SCRIPT_ASSERT("Add_MissionsAtCoords_Feedback(): ERROR - Illegal Feedback Type. See console log. Tell Keith.")
		#ENDIF
			
		EXIT
	ENDIF
	
	// Check if the feedback already exists
	IF (Does_MissionsAtCoords_Feedback_Exist(regID, paramType))
		// Feedback already exists, so report an error and quit
		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Add_MissionsAtCoords_Feedback() - Feedback Already Exists: [RegID = ")
//			PRINTINT(regID)
//			PRINTSTRING("   Type = ")
//			Convert_Missions_At_Coords_Feedback_Type_To_String(paramType)
//			PRINTSTRING("]")
//			PRINTNL()
			
			SCRIPT_ASSERT("Add_MissionsAtCoords_Feedback(): ERROR - Feedback Already Exists. See console log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Find a slot on the array for the feedback
	// NOTE: Assume this always works - I'll delete the oldest entry if there are no slots left
	INT freeSlot = Get_Free_MissionsAtCoords_Feedback_Slot()
	
	// Store the details in the feedback slot
	g_sAtCoordsFeedbackMP[freeSlot].matcfbType		= paramType
	g_sAtCoordsFeedbackMP[freeSlot].matcfbRegID		= regID
	g_sAtCoordsFeedbackMP[freeSlot].matcfbTimeout	= GET_TIME_OFFSET(GET_NETWORK_TIME(), MATC_FEEDBACK_ACTIVE_TIME_msec)
	
	// Increment the number of stored feedback details
	g_numAtCoordsFeedbackMP++
	
	// Output the details of the feedback array to the console log
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Details of Feedback Array after new entry added: ") PRINTNL()
//		Debug_Output_Missions_At_Coords_Feedback_Array()
//	#ENDIF

ENDPROC





// ===========================================================================================================
//      Mission At Coords LOCATE MESSAGE BITS control routines
//		This is specialised because there are currently three scaleform movies that can be used to display
//			these Locate Messages (mission names in a corona), so the Locate Message system needs to be passed
//			an ID for consistency and I store these as Bitflags here to save having to store an extra INT
//			for every mission
// ===========================================================================================================

// PURPOSE:	Clears all the bitflags associated with Locate Messages
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Clear_MissionsAtCoords_Locate_Message_Bitflags(INT paramSlot)

	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_FIRST_LOCATE_MESSAGE)
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_SECOND_LOCATE_MESSAGE)
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_THIRD_LOCATE_MESSAGE)
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_WANTED)
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_CASH)
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_RANK)	
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_TOD)
	CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_SHOW_RESERVED)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: All Locate Message Bitflags cleared. Slot ")
		PRINTINT(paramSlot)
		PRINTNL()
//		PRINTSTRING("         ")
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//		PRINTNL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the appropriate bitflag based on the Locate Message ID
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//							paramMessageID		The Locate Message ID
//
// NOTES:	This is a nasty conversion function used to prevent me having to store an INT with every set of mission data by using bitflags instead
PROC Set_MissionsAtCoords_Locate_Message_Bitflag_From_Locate_MessageID(INT paramSlot, INT paramMessageID)
	
	// Set the appropriate bitflag, clear the rest
	SWITCH (paramMessageID)
		// There is no allocated Locate Message ID, so clear the bits
		CASE NO_LOCATE_MESSAGE_ID
			Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
			EXIT
			
		// First Locate Message ID
		CASE FIRST_LOCATE_MESSAGE_ID
			SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_FIRST_LOCATE_MESSAGE)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_SECOND_LOCATE_MESSAGE)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_THIRD_LOCATE_MESSAGE)
			EXIT
			
		// Second Locate Message ID
		CASE SECOND_LOCATE_MESSAGE_ID
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_FIRST_LOCATE_MESSAGE)
			SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_SECOND_LOCATE_MESSAGE)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_THIRD_LOCATE_MESSAGE)
			EXIT
			
		// Third Locate Message ID
		CASE THIRD_LOCATE_MESSAGE_ID
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_FIRST_LOCATE_MESSAGE)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_SECOND_LOCATE_MESSAGE)
			SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_THIRD_LOCATE_MESSAGE)
			EXIT
	ENDSWITCH

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Set Bitflag. Unknown Locate Message ID: ")
		PRINTINT(paramMessageID)
		PRINTNL()
		
		SCRIPT_ASSERT("Set_MissionsAtCoords_Locate_Message_Bitflag_From_Locate_MessageID(): Unknown Locate Message ID. See Console Log. Clearing out the Message Bits to allow processing to continue. Tell Keith.")
	#ENDIF
	
	// Clear out the Bits to allow processing to continue
	Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gets the Locate Message ID based on the appropriate Bitflag
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE				INT					The Locate Message ID, or NO_LOCATE_MESSAGE_ID if there is none
//
// NOTES:	This is a nasty conversion function used to prevent me having to store an INT with every set of mission data by using bitflags instead
FUNC INT Get_MissionsAtCoords_Locate_MessageID_From_Bitflags(INT paramSlot)

	// First Bit Set?
	IF (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_FIRST_LOCATE_MESSAGE))
		RETURN FIRST_LOCATE_MESSAGE_ID
	ENDIF
	
	// Second Bit Set?
	IF (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_SECOND_LOCATE_MESSAGE))
		RETURN SECOND_LOCATE_MESSAGE_ID
	ENDIF
	
	// Third Bit Set?
	IF (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_USING_THIRD_LOCATE_MESSAGE))
		RETURN THIRD_LOCATE_MESSAGE_ID
	ENDIF
	
	// Not bits set
	RETURN NO_LOCATE_MESSAGE_ID

ENDFUNC




// ===========================================================================================================
//      Mission At Coords Stages Access Routines
// ===========================================================================================================

// PURPOSE:	Set the mission's stage to be 'Outside Display Rnages'
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Stage_For_Mission_To_Outside_Display_Ranges(INT paramSlot)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: New Mission Stage. Slot ")
		PRINTINT(paramSlot)
		PRINTSTRING(". ")
		PRINTSTRING(GET_MP_MISSION_NAME(g_sAtCoordsMP[paramSlot].matcMissionIdData.idMission))
		PRINTSTRING(". From ")
		PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(Get_MissionsAtCoords_Stage_For_Mission(paramSlot)))
		PRINTSTRING(" to ")
		PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(MATCS_OUTSIDE_DISPLAY_RANGES))
		PRINTNL()
		
		MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
		
		BOOL onAtThisTime = Is_MissionsAtCoords_Mission_Unlocked_At_This_Time_Of_Day(paramSlot)
		IF NOT (onAtThisTime)
			PRINTSTRING("      CLOSED. HOUR: ") PRINTINT(GET_CLOCK_HOURS()) PRINTNL()
			
			IF (Should_MissionsAtCoords_Mission_Remain_Open_If_Active(paramSlot))
				IF (Is_This_Mission_Request_Active_And_Joinable(theMissionIdData))
					PRINTSTRING("      BUT: KEEP OPEN IF ACTIVE - Active and Joinable") PRINTNL()
				ENDIF
			ENDIF
		ENDIF
		
		IF (g_sAtCoordsControlsMP.matccPlayerRank < Get_Rank_Required_For_MP_Mission_Request(theMissionIdData))
			IF (Should_MissionsAtCoords_Mission_Remain_Open_If_Active(paramSlot))
				IF (Is_This_Mission_Request_Active_And_Joinable(theMissionIdData))
					PRINTSTRING("      BUT: KEEP OPEN IF ACTIVE - Unlocking for low-rank player)") PRINTNL()
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	// Allow vehicle generators tobe active in the area of the corona again
	BOOL activeVehicleGenerators = TRUE
	Change_Active_State_Of_Vehicles_Generators_Near_MissionsAtCoords_Corona(paramSlot, activeVehicleGenerators)
	
	g_sAtCoordsMP[paramSlot].matcStage = MATCS_OUTSIDE_DISPLAY_RANGES

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the mission's stage to be 'Display Corona Range'
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(INT paramSlot)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: New Mission Stage. Slot ")
		PRINTINT(paramSlot)
		PRINTSTRING(". ")
		PRINTSTRING(GET_MP_MISSION_NAME(g_sAtCoordsMP[paramSlot].matcMissionIdData.idMission))
		PRINTSTRING(". From ")
		PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(Get_MissionsAtCoords_Stage_For_Mission(paramSlot)))
		PRINTSTRING(" to ")
		PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(MATCS_DISPLAY_CORONA_RANGE))
		PRINTSTRING(")")
		PRINTNL()
	#ENDIF
	
	Remove_MissionsAtCoords_SpeedZone(paramSlot)
	Remove_MissionsAtCoords_ScenarioBlocking(paramSlot)
	
	g_sAtCoordsMP[paramSlot].matcStage = MATCS_DISPLAY_CORONA_RANGE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the mission's stage to be 'Requesting Mission Name'
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(INT paramSlot)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: New Mission Stage. Slot ")
		PRINTINT(paramSlot)
		PRINTSTRING(". ")
		PRINTSTRING(GET_MP_MISSION_NAME(g_sAtCoordsMP[paramSlot].matcMissionIdData.idMission))
		PRINTSTRING(". From ")
		PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(Get_MissionsAtCoords_Stage_For_Mission(paramSlot)))
		PRINTSTRING(" to ")
		PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(MATCS_REQUESTING_MISSION_NAME))
		PRINTSTRING(")")
		PRINTNL()
	#ENDIF
	
	Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
	
	// Ignore speed zone and scenario blocking requests if coronas and blips are curently not on display - the speed zone won't be needed
	IF NOT (Should_MissionsAtCoords_Coronas_Be_Displayed())
	AND NOT (Should_MissionsAtCoords_Blips_Be_Displayed())
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Ignoring Speed Zone request - blips and coronas are not currently allowed so a speed zone won't be needed") PRINTNL()
//			PRINTSTRING(".KGM [At Coords]: Ignoring Scenario Blocking request - blips and coronas are not currently allowed so scenario blocking won't be needed") PRINTNL()
//		#ENDIF
	ELSE
		Setup_MissionsAtCoords_SpeedZone(paramSlot)
		Setup_MissionsAtCoords_ScenarioBlocking(paramSlot)
	ENDIF
	
	g_sAtCoordsMP[paramSlot].matcStage = MATCS_REQUESTING_MISSION_NAME

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the mission's stage to be 'Waiting For On Mission'
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Stage_For_Mission_To_Waiting_For_On_Mission(INT paramSlot)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: New Mission Stage. Slot ")
		PRINTINT(paramSlot)
		PRINTSTRING(". ")
		PRINTSTRING(GET_MP_MISSION_NAME(g_sAtCoordsMP[paramSlot].matcMissionIdData.idMission))
		PRINTSTRING(". From ")
		PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(Get_MissionsAtCoords_Stage_For_Mission(paramSlot)))
		PRINTSTRING(" to ")
		PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(MATCS_WAITING_FOR_ON_MISSION))
		PRINTSTRING(")")
		PRINTNL()
	#ENDIF
	
	g_sAtCoordsFocusMP.focusUniqueID = g_sAtCoordsControlsMP.matccActivity.matcadMissionUniqueID
	
	g_sAtCoordsMP[paramSlot].matcStage = MATCS_WAITING_FOR_ON_MISSION

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the mission's stage
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//							paramStage			The new mission stage
//
// NOTES:	This should not be used for setting the 'Requesting Mission Name' stage - it needs to do some specialised work
PROC Set_MissionsAtCoords_Stage_For_Mission(INT paramSlot, g_eMatCStages paramStage)

	IF (paramStage = MATCS_REQUESTING_MISSION_NAME)
		// ...redirect to the specialised function
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		EXIT
	ENDIF

	IF (paramStage = MATCS_WAITING_FOR_ON_MISSION)
		// ...redirect to the specialised function
		Set_MissionsAtCoords_Stage_For_Mission_To_Waiting_For_On_Mission(paramSlot)
		EXIT
	ENDIF

	IF (paramStage = MATCS_DISPLAY_CORONA_RANGE)
		// ...redirect to the specialised function
		Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
		EXIT
	ENDIF

	IF (paramStage = MATCS_OUTSIDE_DISPLAY_RANGES)
		// ...redirect to the specialised function
		Set_MissionsAtCoords_Stage_For_Mission_To_Outside_Display_Ranges(paramSlot)
		EXIT
	ENDIF

	#IF IS_DEBUG_BUILD
		// KGM 9/2/13: To prevent initial overwhelming console log output, don't display any details if a new mission is moving to its initial stage
		IF (g_sAtCoordsMP[paramSlot].matcStage != MATCS_STAGE_NEW)
			PRINTSTRING(".KGM [At Coords]: New Mission Stage. Slot ")
			PRINTINT(paramSlot)
			PRINTSTRING(". ")
			PRINTSTRING(GET_MP_MISSION_NAME(g_sAtCoordsMP[paramSlot].matcMissionIdData.idMission))
			PRINTSTRING(". From ")
			PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(Get_MissionsAtCoords_Stage_For_Mission(paramSlot)))
			PRINTSTRING(" to ")
			PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(paramStage))
			PRINTSTRING(")")
			PRINTNL()
		ENDIF
	#ENDIF
	
	g_sAtCoordsMP[paramSlot].matcStage = paramStage

ENDPROC




// ===========================================================================================================
//      Mission At Coords Bands Access Routines
// ===========================================================================================================

// PURPOSE:	Get the inner range boundary for the specified stage
//
// INPUT PARAMS:			paramBand			The band range against which the distance is being checked
//							paramSlot			The mission slot for the mission
// RETURN VALUE:			FLOAT				The boundary used to check if the player has moved inside the range
FUNC FLOAT Get_MissionsAtCoords_Inner_Range_Boundary(g_eMatCBandings paramBand, INT paramSlot)

	IF (Should_MissionsAtCoords_Mission_Use_Extended_Name_Display_Range(paramSlot))
		SWITCH (paramBand)
			// These drop into the standard switch statement
			CASE MATCB_FOCUS_MISSION
			CASE MATCB_MISSIONS_BEING_DELETED
			CASE MATCB_NEW_MISSIONS
			CASE MAX_MISSIONS_AT_COORDS_BANDINGS
				BREAK
				
			// These Bands should return the whole map
			CASE MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE
			CASE MATCB_MISSIONS_INSIDE_CORONA_RANGE
			CASE MATCB_MISSIONS_OUTSIDE_CORONA_RANGE
			CASE MATCB_MISSIONS_FAR_AWAY_RANGE
				RETURN INVALID_MATC_RANGE_CHECK
				
			// Unknown Band
			DEFAULT
				#IF IS_DEBUG_BUILD
//					PRINTSTRING(".KGM [At Coords]: ERROR - Get_MissionsAtCoords_Inner_Range_Boundary() Extended Name Range called with Unknown Band ID.") PRINTNL()
					SCRIPT_ASSERT("Get_MissionsAtCoords_Inner_Range_Boundary(): ERROR - called with Unknown Band ID. Add to Extended Name Range Switch Statement. Tell Keith.")
				#ENDIF
				BREAK
		ENDSWITCH
	ENDIF

	SWITCH (paramBand)
		CASE MATCB_FOCUS_MISSION
			RETURN (g_sAtCoordsMP[paramSlot].matcCorona.triggerRadius)
		CASE MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE
			RETURN (g_sAtCoordsMP[paramSlot].matcCorona.triggerRadius + MATC_RANGE_ADDITION_FOR_NAMED_RANGE_m)
		CASE MATCB_MISSIONS_INSIDE_CORONA_RANGE
			RETURN MATC_RANGE_DEFAULT_CORONA_RANGE_m
		CASE MATCB_MISSIONS_OUTSIDE_CORONA_RANGE
			RETURN MATC_RANGE_CLOSE_TO_DISPLAY_RANGE_m
		CASE MATCB_MISSIONS_FAR_AWAY_RANGE
			RETURN INVALID_MATC_RANGE_CHECK
		CASE MATCB_MISSIONS_BEING_DELETED
			RETURN INVALID_MATC_RANGE_CHECK
		CASE MATCB_NEW_MISSIONS
			RETURN INVALID_MATC_RANGE_MAX_VALUE
		CASE MAX_MISSIONS_AT_COORDS_BANDINGS
			#IF IS_DEBUG_BUILD
//				PRINTSTRING(".KGM [At Coords]: ERROR - Get_MissionsAtCoords_Inner_Range_Boundary() called with MAX_MISSIONS_AT_COORDS_BANDINGS.") PRINTNL()
				SCRIPT_ASSERT("Get_MissionsAtCoords_Inner_Range_Boundary(): ERROR - called with MAX_MISSIONS_AT_COORDS_BANDINGS. Tell Keith.")
			#ENDIF
			
			RETURN INVALID_MATC_RANGE_MAX_VALUE
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: ERROR - Get_MissionsAtCoords_Inner_Range_Boundary() called with Unknown Band ID.") PRINTNL()
		SCRIPT_ASSERT("Get_MissionsAtCoords_Inner_Range_Boundary(): ERROR - called with Unknown Band ID. Add to Switch Statement. Tell Keith.")
	#ENDIF
	
	RETURN INVALID_MATC_RANGE_MAX_VALUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the outer range boundary for the specified stage
//
// INPUT PARAMS:			paramBand			The band range + leeway against which the distance is being checked
//							paramSlot			The mission slot for the mission
// RETURN VALUE:			FLOAT				The boundary (+ leeway) used to check if the player has moved outside the range)
FUNC FLOAT Get_MissionsAtCoords_Outer_Range_Boundary(g_eMatCBandings paramBand, INT paramSlot)
	RETURN (Get_MissionsAtCoords_Inner_Range_Boundary(paramBand, paramSlot) + MATC_RANGE_LEEWAY)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the specified distance is inside the range for the specified stage
//
// INPUT PARAMS:			paramBand			The band against which the distance is being checked
//							paramSlot			The mission slot for the mission
//							paramDistance		The distance being checked
// RETURN VALUE:			BOOL				TRUE if the distance is inside the range
FUNC BOOL Is_Inside_MissionsAtCoords_Range_Boundary(g_eMatCBandings paramBand, INT paramSlot, FLOAT paramDistance)
	RETURN (paramDistance < Get_MissionsAtCoords_Inner_Range_Boundary(paramBand, paramSlot))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the specified distance is outside the range for the specified band
//
// INPUT PARAMS:			paramBand			The band against which the distance is being checked
//							paramSlot			The mission slot for the mission
//							paramDistance		The distance being checked
// RETURN VALUE:			BOOL				TRUE if the distance is outside the range (+ leeway)
//
// NOTES:	The outer boundary includes a leeway value to make it slightly farther from the coords - this prevents a player on the boundary continually swapping stages
FUNC BOOL Is_Outside_MissionsAtCoords_Range_Boundary(g_eMatCBandings paramBand, INT paramSlot, FLOAT paramDistance)
	RETURN (paramDistance > Get_MissionsAtCoords_Outer_Range_Boundary(paramBand, paramSlot))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the scheduling Band that a mission at the passed-in Stage should be in
//
// INPUT PARAMS:			paramStage			The stage
// RETURN PARAMS:			g_eMatCBandings		The Band that a mission in the passed-in Band should be in
FUNC g_eMatCBandings Get_MissionsAtCoords_Band_For_Stage(g_eMatCStages paramStage)
	RETURN (g_sAtCoordsStageInfo[paramStage].matcbBand)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the update mission slot in a Band
//
// INPUT PARAMS:			paramBand			The Band being checked
// RETURN VALUE:			INT					The update mission slot in the Band
FUNC INT Get_Update_MissionsAtCoords_Slot_For_Band(g_eMatCBandings paramBand)
	RETURN (g_sAtCoordsBandsMP[paramBand].matcbUpdateSlot)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the update mission slot in a Band
//
// INPUT PARAMS:			paramBand			The Band being checked
//							paramSlot			The mission slot being set as teh next update slot
PROC Set_MissionsAtCoords_Update_Slot_For_Band(g_eMatCBandings paramBand, INT paramSlot)

	IF (paramSlot = INVALID_MATC_SLOT)
		// Update Slot is being cleared
		IF (Get_Number_Of_MissionsAtCoords_In_Band(paramBand) = 0)
			g_sAtCoordsBandsMP[paramBand].matcbUpdateSlot = INVALID_MATC_SLOT
			EXIT
		ELSE
			// ...but there are entries in the Band, so the update slot shouldn't be cleared
			#IF IS_DEBUG_BUILD
//				PRINTSTRING(".KGM [At Coords]: Setting Update Slot for Band ")
//				PRINTSTRING(Convert_Missions_At_Coords_Banding_To_SameLength_String(paramBand))
//				PRINTSTRING(" - ERROR: There are missions in the Band, but the Update Slot is being set to INVALID_MATC_SLOT")
//				PRINTNL()
//				Debug_Output_Missions_At_Coords_Banding_Array()
//				PRINTSTRING("      SETTING UPDATE SLOT TO FIRST SLOT TO ALLOW PROCESSING TO CONTINUE")
//				PRINTNL()
				
				SCRIPT_ASSERT("Set_MissionsAtCoords_Update_Slot_For_Band(): Setting an update slot for a Band to INVALID_MATC_SLOT when there are missions in the Band. Setting Update Slot to be First Slot. See Console Log. Tell Keith.")
			#ENDIF
			
			paramSlot = Get_First_MissionsAtCoords_Slot_For_Band(paramBand)
		ENDIF
	ENDIF

	IF (Get_Number_Of_MissionsAtCoords_In_Band(paramBand) = 0)
		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Setting Update Slot for Band ")
//			PRINTSTRING(Convert_Missions_At_Coords_Banding_To_SameLength_String(paramBand))
//			PRINTSTRING(" - ERROR: Band contains no missions")
//			PRINTNL()
//			Debug_Output_Missions_At_Coords_Banding_Array()
			
			SCRIPT_ASSERT("Set_MissionsAtCoords_Update_Slot_For_Band(): Setting an update slot for a Band with no missions. See Console Log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF

	IF (Get_First_MissionsAtCoords_Slot_For_Band(paramBand) > paramSlot)
		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Setting Update Slot for Band ")
//			PRINTSTRING(Convert_Missions_At_Coords_Banding_To_SameLength_String(paramBand))
//			PRINTSTRING(" - ERROR: Slot is less than first mission slot in Band")
//			PRINTNL()
//			Debug_Output_Missions_At_Coords_Banding_Array()
//			PRINTSTRING("      SETTING UPDATE SLOT TO FIRST SLOT TO ALLOW PROCESSING TO CONTINUE")
//			PRINTNL()
			
			SCRIPT_ASSERT("Set_MissionsAtCoords_Update_Slot_For_Band(): Setting an update slot for a Band to less than the Band's first slot. Setting Update Slot to be First Slot. See Console Log. Tell Keith.")
		#ENDIF
		
		paramSlot = Get_First_MissionsAtCoords_Slot_For_Band(paramBand)
	ENDIF

	IF (Get_Last_MissionsAtCoords_Slot_For_Band(paramBand) < paramSlot)
		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Setting Update Slot for Band ")
//			PRINTSTRING(Convert_Missions_At_Coords_Banding_To_SameLength_String(paramBand))
//			PRINTSTRING(" - ERROR: Slot is greater than last mission slot in Band")
//			PRINTNL()
//			Debug_Output_Missions_At_Coords_Banding_Array()
//			PRINTSTRING("      SETTING UPDATE SLOT TO FIRST SLOT TO ALLOW PROCESSING TO CONTINUE")
//			PRINTNL()
			
			SCRIPT_ASSERT("Set_MissionsAtCoords_Update_Slot_For_Band(): Setting an update slot for a Band to greater than the Band's last slot. Setting Update Slot to be First Slot. See Console Log. Tell Keith.")
		#ENDIF
		
		paramSlot = Get_First_MissionsAtCoords_Slot_For_Band(paramBand)
	ENDIF

	g_sAtCoordsBandsMP[paramBand].matcbUpdateSlot = paramSlot
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the next valid mission slot in a Band
//
// INPUT PARAMS:			paramBand			The Band being checked
// RETURN VALUE:			INT					The next valid update mission slot in the Band
FUNC INT Get_Next_Valid_Update_Slot_For_Band(g_eMatCBandings paramBand)

	// If there are no entries, then there should be no update slot
	IF (Get_Number_Of_MissionsAtCoords_In_Band(paramBand) = 0)
		Set_MissionsAtCoords_Update_Slot_For_Band(paramBand, INVALID_MATC_SLOT)
		RETURN INVALID_MATC_SLOT
	ENDIF
	
	// Move on to the next update slot
	INT theUpdateSlot = Get_Update_MissionsAtCoords_Slot_For_Band(paramBand)
	IF (theUpdateSlot = INVALID_MATC_SLOT)
		// There is no update slot at the moment, so set the update slot to -1 so that the checks below automatically set it to the first valid slot for the band
		theUpdateSlot = -1
	ELSE
		theUpdateSlot++
	ENDIF
	
	// If the update slot is less than the first slot or greater than the last slot, then move the update slot to the first slot
	IF (Get_First_MissionsAtCoords_Slot_For_Band(paramBand) > theUpdateSlot)
	OR (Get_Last_MissionsAtCoords_Slot_For_Band(paramBand) < theUpdateSlot)
		theUpdateSlot = Get_First_MissionsAtCoords_Slot_For_Band(paramBand)
	ENDIF
	
	// Need to store the modified update slot
	Set_MissionsAtCoords_Update_Slot_For_Band(paramBand, theUpdateSlot)
		
	RETURN (theUpdateSlot)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: The Player must have moved closer to the coordinates, so move the Mission into a higher priority scheduling band
//
// INPUT PARAMS:			paramSlot				The mission's current slot in the Missions At Coords array
//							paramCurrentBandAsInt	The mission's current scheduling Band (as an INT)
//							paramRequiredBandAsInt	The Band the mission should move to (as an INT)
//
// NOTES:	Refer to extensive comments in Move_Mission_In_Slot_To_New_Band() function
//			The mission is moving closer to element 0 in the missions array, so the gap to fill will be farther away from element 0 and we'll need to make a gap closer to element 0
PROC Move_Mission_In_Slot_To_Higher_Priority_Band(INT paramSlot, INT paramCurrentBandAsInt, INT paramRequiredBandAsInt)

	INT				thisBandAsInt	= paramCurrentBandAsInt
	g_eMatCBandings	thisBand		= INT_TO_ENUM(g_eMatCBandings, thisBandAsInt)
	
	// The current Gap Slot needs to be recorded in case the Required Band doesn't contain any data
	INT				theGapSlot		= paramSlot

	// Copy the mission into a holding variable
	g_structMissionsAtCoordsMP holdMission = g_sAtCoordsMP[paramSlot]
	
	// DEAL WITH THE BAND THE DATA IS BEING REMOVED FROM
	// If the first slot in this Band is not where the mission is being removed from, then copy the mission current in the first slot into the new gap slot within the Band
	INT firstSlot = Get_First_MissionsAtCoords_Slot_For_Band(thisBand)
	IF (paramSlot != firstSlot)
		// Need to move the mission currently in the first slot for this band into the gap slot
		g_sAtCoordsMP[paramSlot] = g_sAtCoordsMP[firstSlot]
		
		// The gap slot is now where the first slot for this Band used to be
		theGapSlot = firstSlot
	ENDIF
	
	// Update the Banding data - this slot now has one less mission and starts one slot higher
	IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBand) = 1)
		// ...the mission being removed was the only mission in this Band, so clear out this Band's data
		g_structMatCBandingsMP emptyBandingStruct
		g_sAtCoordsBandsMP[thisBand] = emptyBandingStruct
	ELSE
		// ...there were other missions in this Band, so update the Band data
		g_sAtCoordsBandsMP[thisBand].matcbNumInBand--
		g_sAtCoordsBandsMP[thisBand].matcbFirstSlot++
	ENDIF
	
	// DEAL WITH ANY IN-BETWEEN BANDS THAT ARE NOT THE BAND THE DATA IS BEING MOVED TO
	INT lastSlot = 0
	
	thisBandAsInt--
	WHILE (thisBandAsInt != paramRequiredBandAsInt)
		thisBand = INT_TO_ENUM(g_eMatCBandings, thisBandAsInt)
	
		IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBand) > 0)
			// ...need to shuffle this Band away from element 0 of the Missions array to make room, but the contents of this Band remain unchanged
			// NOTE: We do this by taking the current 'first slot' mission and moving it into what will be the new 'last slot' for the Band
			
			// Move the 'last slot' for this band farther away from element 0 of the array to fill the gap left by the removed mission
			// ...set the new 'last slot'
			g_sAtCoordsBandsMP[thisBand].matcbLastSlot++
			
			// ...copy the current 'first slot' mission details into the new 'last slot'
			lastSlot = Get_Last_MissionsAtCoords_Slot_For_Band(thisBand)
			firstSlot = Get_First_MissionsAtCoords_Slot_For_Band(thisBand)
			g_sAtCoordsMP[lastSlot] = g_sAtCoordsMP[firstSlot]
			
			// ...the Gap Slot will now be where this Band's first slot used to be
			theGapSlot = firstSlot
			
			// ...set the new 'first slot'
			g_sAtCoordsBandsMP[thisBand].matcbFirstSlot++
		ENDIF
		
		// This Band has been dealt with so move on to the next Band
		thisBandAsInt--
	ENDWHILE
	
	// DEAL WITH THE FINAL BAND WHERE THE DATA IS BEING INSERTED INTO
	thisBand = INT_TO_ENUM(g_eMatCBandings, thisBandAsInt)
	
	// Store the Mission in the Gap Slot
	g_sAtCoordsMP[theGapSlot] = holdMission
	
	// Check if this Band already contains missions
	IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBand) = 0)
		// ...Band doesn't currently contain data, so create new data for the band
		g_sAtCoordsBandsMP[thisBand].matcbNumInBand		= 1
		g_sAtCoordsBandsMP[thisBand].matcbFirstSlot		= theGapSlot
		g_sAtCoordsBandsMP[thisBand].matcbLastSlot		= theGapSlot
		g_sAtCoordsBandsMP[thisBand].matcbUpdateSlot	= theGapSlot
	ELSE
		// ...the band already contains data, so update the data - there is one more entry in the Band and the Last Slot is farther away from element 0 of the array
		g_sAtCoordsBandsMP[thisBand].matcbNumInBand++
		g_sAtCoordsBandsMP[thisBand].matcbLastSlot++
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: The Player must have moved farther away from the coordinates, so move the Mission into a lower priority scheduling band
//
// INPUT PARAMS:			paramSlot			The mission's current slot in the Missions At Coords array
//							paramCurrentBandAsInt	The mission's current scheduling Band (as an INT)
//							paramRequiredBandAsInt	The Band the mission should move to (as an INT)
//
// NOTES:	Refer to extensive comments in Move_Mission_In_Slot_To_New_Band() function
//			The mission is moving farther away from element 0 in the missions array, so the gap to fill will be closer to element 0 and we'll need to make a gap farther away from element 0
PROC Move_Mission_In_Slot_To_Lower_Priority_Band(INT paramSlot, INT paramCurrentBandAsInt, INT paramRequiredBandAsInt)

	INT				thisBandAsInt	= paramCurrentBandAsInt
	g_eMatCBandings	thisBand		= INT_TO_ENUM(g_eMatCBandings, thisBandAsInt)
	
	// The current Gap Slot needs to be recorded in case the Required Band doesn't contain any data
	INT				theGapSlot		= paramSlot

	// Copy the mission into a holding variable
	g_structMissionsAtCoordsMP holdMission = g_sAtCoordsMP[paramSlot]
	
	// DEAL WITH THE BAND THE DATA IS BEING REMOVED FROM
	// If the last slot in this Band is not where the mission is being removed from, then copy the mission current in the last slot into the new gap slot within the Band
	INT lastSlot = Get_Last_MissionsAtCoords_Slot_For_Band(thisBand)
	IF (paramSlot != lastSlot)
		// Need to move the mission currently in the last slot for this band into the gap slot
		g_sAtCoordsMP[paramSlot] = g_sAtCoordsMP[lastSlot]
		
		// The gap slot is now where the last slot for this Band used to be
		theGapSlot = lastSlot
	ENDIF
	
	// Update the Banding data - this slot now has one less mission and ends one slot lower
	IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBand) = 1)
		// ...the mission being removed was the only mission in this Band, so clear out this Band's data
		g_structMatCBandingsMP emptyBandingStruct
		g_sAtCoordsBandsMP[thisBand] = emptyBandingStruct
	ELSE
		// ...there were other missions in this Band, so update the Band data
		g_sAtCoordsBandsMP[thisBand].matcbNumInBand--
		g_sAtCoordsBandsMP[thisBand].matcbLastSlot--
	ENDIF
	
	// DEAL WITH ANY IN-BETWEEN BANDS THAT ARE NOT THE BAND THE DATA IS BEING MOVED TO
	INT firstSlot		= 0
	INT maxBandAsInt	= ENUM_TO_INT(MAX_MISSIONS_AT_COORDS_BANDINGS)

	thisBandAsInt++
	WHILE (thisBandAsInt != paramRequiredBandAsInt)
	AND (thisbandAsInt < maxBandAsInt)
		thisBand = INT_TO_ENUM(g_eMatCBandings, thisBandAsInt)
	
		IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBand) > 0)
			// ...need to shuffle this Band towards element 0 of the Missions array to make room, but the contents of this Band remain unchanged
			// NOTE: We do this by taking the current 'last slot' mission and moving it into what will be the new 'first slot' for the Band
			
			// Move the 'first slot' for this band towards element 0 of the array to fill the gap left by the removed mission
			// ...set the new 'first slot'
			g_sAtCoordsBandsMP[thisBand].matcbFirstSlot--
			
			// ...copy the current 'last slot' mission details into the new 'first slot'
			lastSlot = Get_Last_MissionsAtCoords_Slot_For_Band(thisBand)
			firstSlot = Get_First_MissionsAtCoords_Slot_For_Band(thisBand)
			g_sAtCoordsMP[firstSlot] = g_sAtCoordsMP[lastSlot]
			
			// ...the Gap Slot will now be where this Band's last slot used to be
			theGapSlot = lastSlot
			
			// ...set the new 'last slot'
			g_sAtCoordsBandsMP[thisBand].matcbLastSlot--
		ENDIF
		
		// This Band has been dealt with so move on to the next Band
		thisBandAsInt++
	ENDWHILE
	
	// Store the Mission in the Gap Slot - even if the gap slot is just about to be deleted, otherwise it could contain a copy of a valid Blip_index that has been moved elsewhere
	g_sAtCoordsMP[theGapSlot] = holdMission
	
	// If the desiredband is the 'delete' band, then don't store the mission back in the array, clear the 'gap' entry and decrement the number of missions.
	g_eMatCBandings requiredBand = INT_TO_ENUM(g_eMatCBandings, paramRequiredBandAsInt)
	IF (requiredBand = MATCB_MISSIONS_BEING_DELETED)
		// Clear the Gap entry
		Clear_One_MP_Mission_At_Coords_Variables(theGapSlot)
		
		// Decrement total missions in array
		g_numAtCoordsMP--
		
		EXIT
	ENDIF
	
	// DEAL WITH THE FINAL BAND WHERE THE DATA IS BEING INSERTED INTO
	thisBand = INT_TO_ENUM(g_eMatCBandings, thisBandAsInt)
	
	// Check if this Band already contains missions
	IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBand) = 0)
		// ...Band doesn't currently contain data, so create new data for the band
		g_sAtCoordsBandsMP[thisBand].matcbNumInBand		= 1
		g_sAtCoordsBandsMP[thisBand].matcbFirstSlot		= theGapSlot
		g_sAtCoordsBandsMP[thisBand].matcbLastSlot		= theGapSlot
		g_sAtCoordsBandsMP[thisBand].matcbUpdateSlot	= theGapSlot
	ELSE
		// ...the band already contains data, so update the data - there is one more entry in the Band and the First Slot is closer to element 0 of the array
		g_sAtCoordsBandsMP[thisBand].matcbNumInBand++
		g_sAtCoordsBandsMP[thisBand].matcbFirstSlot--
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Move the mission in the specified slot to a differnet Scheduling band
//
// INPUT PARAMS:			paramSlot			The mission's current slot in the Missions At Coords array
//							paramCurrentBand	The mission's current scheduling Band
//							paramRequiredBand	The Band the mission should move to
PROC Move_Mission_In_Slot_To_New_Band(INT paramSlot, g_eMatCBandings paramCurrentBand, g_eMatCBandings paramRequiredBand)

	// NOTES: We need to do this move as an atomic function. Each Band contains an unordered bunch of missions, so we don't need to shuffle the
	//			whole mission array up or down between the current slot and the required slot, instead we just need to remove the mission from
	//			the current slot, and stick it anywhere in with the missions in the required band. This should only required a small amount of
	//			data shuffling instead of large array movements. The method will differ based on whether the mission is moving up or down the array.
	
	// EXAMPLE:
	//		BEFORE:
	//		Focus Mission Band:		0 entries
	//		Nearby Mission Band:	2 entries (slots 0 - 1)
	//		MidRange Mission Band:	3 entries (slots 2 - 4)
	//		Distant Mission Band:	12 entries (slots 5 - 16)
	//
	//		We want to move the mission in slot 1 (Nearby Band) to the Distant Mission Band, so it needs to be done as follows:
	//			1) Copy Mission Details from Slot 1 into temporary holding details
	//			2) Remove Mission from Band
	//				a) If 'last slot' in Band is greater than 'gap slot' then Copy mission from 'last slot' into 'gap slot' - in this example nothing happens
	//				b) Decrease 'total slots used' in Band (from 2 to 1)
	//				c) Decrement 'last slot' by 1 (from 1 to 0)
	//			3) Move the MidRange Band down to fill the gap - because these missions are in no order, this is done as follows:
	//				a) Decrement 'first slot' by 1 (from 2 to 1)
	//				a) Copy Mission in 'last slot' (slot 4) into new 'first slot' (slot 1)
	//				b) Decrement 'last slot' by 1 (from 4 to 3)
	//			4) Insert Mission into Band
	//				a) Decrement 'first slot' by 1 (from 5 to 4)
	//				b) Copy Temporary Holding Details into new 'first slot' (slot 4)
	//				c) Increment 'total slots used' in band (from 12 to 13)
	//
	//		AFTER:
	//		Focus Mission Band:		0 entries
	//		Nearby Mission Band:	1 entry (slot 0)
	//		MidRange Mission Band:	3 entries (slots 1 - 3)
	//		Distant Mission Band:	13 entries (slots 4 - 16)
	
//	#IF IS_DEBUG_BUILD
//		IF (paramCurrentBand != MATCB_NEW_MISSIONS)
//			PRINTSTRING(".KGM [At Coords]: Move_Mission_In_Slot_To_New_Band(): ")
//			PRINTNL()
//			PRINTSTRING("         ")
//			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//			PRINTNL()
//			PRINTSTRING("         ")
//			PRINTSTRING("Moving From: ")
//			PRINTSTRING(Convert_Missions_At_Coords_Banding_To_SameLength_String(paramCurrentBand))
//			PRINTSTRING(" To: ")
//			PRINTSTRING(Convert_Missions_At_Coords_Banding_To_SameLength_String(paramRequiredBand))
//			PRINTNL()
//		ENDIF
//	#ENDIF
	
	INT currentBandAsInt	= ENUM_TO_INT(paramCurrentBand)
	INT requiredBandAsInt	= ENUM_TO_INT(paramRequiredBand)
	
	IF (requiredBandAsInt < currentBandAsInt)
		// ie: player has moved closer to mission coords
		Move_Mission_In_Slot_To_Higher_Priority_Band(paramSlot, currentBandAsInt, requiredBandAsInt)
	ENDIF
	
	IF (requiredBandAsInt > currentBandAsInt)
		// ie: player has moved farther away from mission coords
		Move_Mission_In_Slot_To_Lower_Priority_Band(paramSlot, currentBandAsInt, requiredBandAsInt)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Mission's stage is in a different scheduling Band from the mission's previous scheduling Band and move it if necessary
//
// INPUT PARAMS:			paramSlot			The mission's current slot (this is still within the pre-update scheduling Band but the Stage will have been updated)
//							paramBand			The pre-update scheduling Band
PROC Update_MissionsAtCoords_Mission_Band(INT paramSlot, g_eMatCBandings paramBand)

	// Check if the mission's current stage should still place the mission in the same scheduling Band
	g_eMatCStages	currentStage	= Get_MissionsAtCoords_Stage_For_Mission(paramSlot)
	g_eMatCBandings	desiredBand		= Get_MissionsAtCoords_Band_For_Stage(currentStage)
	
	// Nothing to do if the desiredBand matches the mission's pre-update Band
	IF (desiredBand = paramBand)
		EXIT
	ENDIF
	
	// The desired Band is different from the pre-update Band so the mission needs to move into the new Band
	Move_Mission_In_Slot_To_New_Band(paramSlot, paramBand, desiredBand)
	
	#IF IS_DEBUG_BUILD
		// Output the Banding Array Details
		// KGM 11/1/13: To prevent excess spamming on entering Freemode, don't display the details if this is a new mission being distributed and there are other new missions still to process
		BOOL outputConsoleLogDetails = TRUE
		
		IF (paramBand = MATCB_NEW_MISSIONS)
			IF (g_sAtCoordsBandsMP[paramBand].matcbNumInBand > 0)
				outputConsoleLogDetails = FALSE
			ENDIF
		ENDIF
		
		IF (outputConsoleLogDetails)
			Debug_Output_Missions_At_Coords_Banding_Array()
		ENDIF
		
		// Check the Banding Array Consistency
		Do_Missions_At_Coords_Banding_Array_Consistency_Checking()
	#ENDIF

ENDPROC

TWEAK_INT tciCORONA_FLOW_R 36
TWEAK_INT tciCORONA_FLOW_G 120
TWEAK_INT tciCORONA_FLOW_B 255


// ===========================================================================================================
//      Corona Lights
// ===========================================================================================================

// PURPOSE:	Add a light at the corona position
//
// INPUT PARAMS:		paramColour			The colour of the corona
//						paramCoords			The ground coords of the corona
PROC Draw_Light_At_Corona(HUD_COLOURS paramColour, VECTOR paramCoords)

	// Get the Coords - the light source should be central at the 'jobtype' text
	VECTOR theCoords = paramCoords
	theCoords.z += MATC_CORONA_LIGHTS_ABOVE_GROUND_m
	
	// Setup default values (based on a Rockstar Created corona)
	INT 	theR 			= 36
	INT 	theG 			= 120
	INT 	theB 			= 255
	FLOAT	theRange		= 10.0
	FLOAT	theIntensity	= 5.0
	FLOAT	theFalloffExp	= 64.0

	// Assume that all supported corona colours will have a different RGB value for the lights
	// ...UGC and MiniGames
	IF (paramColour = HUD_COLOUR_WHITE)
	OR (paramColour = HUD_COLOUR_ADVERSARY)
		// No Specific RGB: Using the defaults
		DRAW_LIGHT_WITH_RANGEEX(theCoords, theR, theG, theB, theRange, theIntensity, theFalloffExp)
		
		EXIT
	ENDIF
	
	IF (paramColour = HUD_COLOUR_PINKLIGHT)
	OR (paramColour = HUD_COLOUR_BLUELIGHT)
	OR (paramColour = HUD_COLOUR_ORANGE)
	OR (paramColour = HUD_COLOUR_ORANGELIGHT)
		theR 			= tciCORONA_FLOW_R
		theG 			= tciCORONA_FLOW_G
		theB 			= tciCORONA_FLOW_B
		theRange		= 10.0
		theIntensity	= 5.0
		theFalloffExp	= 64.0
		// No Specific RGB: Using the defaults
		DRAW_LIGHT_WITH_RANGEEX(theCoords, theR, theG, theB, theRange, theIntensity, theFalloffExp)
		EXIT
	ENDIF
	
	// ...Rockstar Created and Rockstar Verified
	IF (paramColour = HUD_COLOUR_NORTH_BLUE)
		theR 			= 36
		theG 			= 120
		theB 			= 255
		theRange		= 10.0
		theIntensity	= 5.0
		theFalloffExp	= 64.0
		DRAW_LIGHT_WITH_RANGEEX(theCoords, theR, theG, theB, theRange, theIntensity, theFalloffExp)
		
		EXIT
	ENDIF
	
	// ...Bookmarked
	IF (paramColour = HUD_COLOUR_YELLOW)
		// No Specific RGB: Using the defaults
		DRAW_LIGHT_WITH_RANGEEX(theCoords, theR, theG, theB, theRange, theIntensity, theFalloffExp)
		
		EXIT
	ENDIF
	
	// ...Time Trial
	IF (paramColour = HUD_COLOUR_PURPLE)
		// No Specific RGB: Using the defaults
		DRAW_LIGHT_WITH_RANGEEX(theCoords, theR, theG, theB, theRange, theIntensity, theFalloffExp)
		
		EXIT
	ENDIF
	
	
	theR 			= tciCORONA_FLOW_R
	theG 			= tciCORONA_FLOW_G
	theB 			= tciCORONA_FLOW_B
	theRange		= 10.0
	theIntensity	= 5.0
	theFalloffExp	= 64.0
	// No Specific RGB: Using the defaults
	DRAW_LIGHT_WITH_RANGEEX(theCoords, theR, theG, theB, theRange, theIntensity, theFalloffExp)

ENDPROC



// ===========================================================================================================
//      Player Broadcast Corona Data Storage for SCTV (spectating) players
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//      The Spectated Player - Storing the corona data for SCTV Players to access
// -----------------------------------------------------------------------------------------------------------

// Clear the details of one closest corona array position
//
// INPUT PARAMS:		paramPlayerAsInt		The PLAYER_ID as an INT for storing Global Player Broadcast Data
//						paramArrayPos			The array position for the data within the SCTV corona storage array
//
// NOTES:	Updated by the SPECTATED player
PROC Clear_One_Corona_Data_For_Storage_For_SCTV(INT paramPlayerAsInt, INT paramArrayPos)

	GlobalPlayerBD_SCTV[paramPlayerAsInt].coronasSCTV[paramArrayPos].ccsctv_inUse		= FALSE
	GlobalPlayerBD_SCTV[paramPlayerAsInt].coronasSCTV[paramArrayPos].ccsctv_position	= << 0.0, 0.0, 0.0 >>
	GlobalPlayerBD_SCTV[paramPlayerAsInt].coronasSCTV[paramArrayPos].ccsctv_colour		= HUD_COLOUR_PURE_WHITE
	GlobalPlayerBD_SCTV[paramPlayerAsInt].coronasSCTV[paramArrayPos].ccsctv_radius		= 0.0
	
	g_localCoronaSCTV[paramArrayPos].ldccsctv_keep		= FALSE
	g_localCoronaSCTV[paramArrayPos].ldccsctv_regID		= ILLEGAL_AT_COORDS_ID
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Untick all the local 'keep' flags for the stored corona data
// NOTES:	Updated by the SPECTATED player
PROC PreUpdate_Corona_Data_Storage_For_SCTV()

	// KGM 12/1/15 [BUG 2190862]: Optimisation
	IF (g_numCoronaSCTV = 0)
		EXIT
	ENDIF
	
	INT tempLoop = 0
	REPEAT MAX_SCTV_CORONAS tempLoop
		g_localCoronaSCTV[tempLoop].ldccsctv_keep = FALSE
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Delete all stored corona data where the 'keep' flag has not been ticked again
// NOTES:	Updated by the SPECTATED player
PROC PostUpdate_Corona_Data_Storage_For_SCTV()

	// KGM 12/1/15 [BUG 2190862]: Optimisation
	IF (g_numCoronaSCTV = 0)
		EXIT
	ENDIF
	
	INT tempLoop	= 0
	INT thisPlayer	= NATIVE_TO_INT(PLAYER_ID())

	REPEAT MAX_SCTV_CORONAS tempLoop
		IF NOT (g_localCoronaSCTV[tempLoop].ldccsctv_keep)
			// ...don't keep this entry, so clear it out if it contains data
			IF (g_localCoronaSCTV[tempLoop].ldccsctv_regID != ILLEGAL_AT_COORDS_ID)
				// ...contains data, so clear it out
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [At Coords][SCTV]: Clear Corona Stored for SCTV. RegID: ", g_localCoronaSCTV[tempLoop].ldccsctv_regID,
							". Position: ", GlobalPlayerBD_SCTV[thisPlayer].coronasSCTV[tempLoop].ccsctv_position,
							"TOTAL CORONAS: ", (g_numCoronaSCTV-1))
				#ENDIF
				
				Clear_One_Corona_Data_For_Storage_For_SCTV(thisPlayer, tempLoop)
				// ...there is one less piece of storage being used
				g_numCoronaSCTV--
				
				IF (g_numCoronaSCTV < 0)
					PRINTLN(".KGM [At Coords][SCTV]: Error - g_numCoronaSCTV < 0")
					SCRIPT_ASSERT("ERROR: g_numCoronaSCTV < 0. Tell Keith")
					
					g_numCoronaSCTV = 0
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// Re-tick an existing stored corona data entry, or add it as a new one if there is room in the array
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//
// NOTES:	Updated by the SPECTATED player
PROC Set_Corona_Data_For_Storage_For_SCTV(INT paramSlot)

	// Check if the corona is already registered
	INT theRegID	= Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
	INT tempLoop	= 0
	
	REPEAT MAX_SCTV_CORONAS tempLoop
		IF (g_localCoronaSCTV[tempLoop].ldccsctv_regID = theRegID)
			// ...found the entry already stored, so 'keep' it
			g_localCoronaSCTV[tempLoop].ldccsctv_keep = TRUE
			
			EXIT
		ENDIF
	ENDREPEAT
	
	// Entry isn't stored, so add it if there is a free space
	IF (g_numCoronaSCTV >= MAX_SCTV_CORONAS)
		// ...there is no space on the array
		#IF IS_DEBUG_BUILD
			// Debug Only check to ensure the variable does match the expected state of the data
			g_numCoronaSCTV = MAX_SCTV_CORONAS
			REPEAT MAX_SCTV_CORONAS tempLoop
				IF (g_localCoronaSCTV[tempLoop].ldccsctv_regID = ILLEGAL_AT_COORDS_ID)
					PRINTLN(".KGM [At Coords][SCTV]: Error - g_numCoronaSCTV is MAX, but spaces exist on array")
					SCRIPT_ASSERT("ERROR: g_numCoronaSCTV is MAX, but spaces exist on array. Tell Keith")
					
					g_numCoronaSCTV--
				ENDIF
			ENDREPEAT
		#ENDIF
	
		EXIT
	ENDIF
	
	// There is space on the array, so find a free space
	INT thisPlayer	= NATIVE_TO_INT(PLAYER_ID())
	
	REPEAT MAX_SCTV_CORONAS tempLoop
		IF (g_localCoronaSCTV[tempLoop].ldccsctv_regID = ILLEGAL_AT_COORDS_ID)
			// ...found a free space, so store the data
			GlobalPlayerBD_SCTV[thisPlayer].coronasSCTV[tempLoop].ccsctv_inUse		= TRUE
			GlobalPlayerBD_SCTV[thisPlayer].coronasSCTV[tempLoop].ccsctv_position	= Get_MissionsAtCoords_Slot_Coords(paramSlot)
			GlobalPlayerBD_SCTV[thisPlayer].coronasSCTV[tempLoop].ccsctv_colour		= Get_MissionsAtCoords_Slot_Corona_Hud_Colour(paramSlot)
			GlobalPlayerBD_SCTV[thisPlayer].coronasSCTV[tempLoop].ccsctv_radius		= Get_MissionsAtCoords_Slot_Corona_Trigger_Radius(paramSlot)
			
			g_localCoronaSCTV[tempLoop].ldccsctv_keep		= TRUE
			g_localCoronaSCTV[tempLoop].ldccsctv_regID		= theRegID
			
			g_numCoronaSCTV++
			
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [At Coords][SCTV]: Add Corona Stored for SCTV. RegID: ", g_localCoronaSCTV[tempLoop].ldccsctv_regID,
						". Position: ", GlobalPlayerBD_SCTV[thisPlayer].coronasSCTV[tempLoop].ccsctv_position,
						". ColourAsInt: ", ENUM_TO_INT(GlobalPlayerBD_SCTV[thisPlayer].coronasSCTV[tempLoop].ccsctv_colour),
						". Radius: ", GlobalPlayerBD_SCTV[thisPlayer].coronasSCTV[tempLoop].ccsctv_radius,
						"TOTAL CORONAS: ", g_numCoronaSCTV)
			#ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT
	
	// Didn't find a free space
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [At Coords][SCTV]: Error - g_numCoronaSCTV is not MAX, but there are no spaces on array")
		SCRIPT_ASSERT("Error - g_numCoronaSCTV is not MAX, but there are no spaces on array. Tell Keith")
		
		g_numCoronaSCTV = MAX_SCTV_CORONAS
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      The Spectated Player - PlayerBD details to indicate if this spectated player is in the Pause Menu
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Details that allows a spectating player to continue displaying a corona even though this player being spectated can't see them (ie: when paused)
//
// INPUT PARAMS:		paramPlayerAsInt		The PLAYER_ID as an INT for storing Global Player Broadcast Data
//
// NOTES:	Updated by SPECTATED player
PROC Clear_Paused_Corona_Details_For_SCTV_Player(INT paramPlayerAsInt)

	g_pausedCoronasSCTV emptyPausedCoronaDetails 
	GlobalPlayerBD_SCTV[paramPlayerAsInt].pausedSCTV = emptyPausedCoronaDetails
	
	GlobalPlayerBD_SCTV[paramPlayerAsInt].pausedSCTV.pcsctv_locationWhenPaused = << 0.0, 0.0, 0.0 >>
	
	PRINTLN(".KGM [At Coords][SCTV]: Clearing Paused Corona Details for SCTV variables")
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	This is ONLY to check if the spectated player is doing something that would temporarily remove the corona details on the machines of players spectating this player
// NOTES:	The main corona storage details for SCTV are maintained in another maintenance function - goto where this function gets called from to find it
// NOTES:	This is updated by the 'SPECTATED' player
PROC Maintain_Missions_At_Coords_Paused_Details_For_SCTV()

	// Check if this player is in the pause menu and update the details - spectating players should still draw coronas of spectated players if the spectated players are not moving and in the pause menu
	INT playerAsInt = NATIVE_TO_INT(PLAYER_ID())
	
	IF (IS_NET_PLAYER_OK(PLAYER_ID()))
		IF (IS_PAUSE_MENU_ACTIVE_EX())
			// ...this player is in the pause menu
			IF NOT (GlobalPlayerBD_SCTV[playerAsInt].pausedSCTV.pcsctv_isOnPause)
				// ...this player is not known to be in the pause menu, so store the details
				GlobalPlayerBD_SCTV[playerAsInt].pausedSCTV.pcsctv_isOnPause			= TRUE
				GlobalPlayerBD_SCTV[playerAsInt].pausedSCTV.pcsctv_locationWhenPaused	= GET_PLAYER_COORDS(PLAYER_ID())
				
				PRINTLN(".KGM [At Coords][SCTV]: Player has activated Pause Menu. Setting Flag and current coords: ", GlobalPlayerBD_SCTV[playerAsInt].pausedSCTV.pcsctv_locationWhenPaused)
			ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	// Clear the 'paused' details if necessary
	IF NOT (GlobalPlayerBD_SCTV[playerAsInt].pausedSCTV.pcsctv_isOnPause)
		EXIT
	ENDIF
	
	Clear_Paused_Corona_Details_For_SCTV_Player(playerAsInt)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      The SCTV (Spectating) Player - Copy and Retain Current Displayed Coronas in case they need to keep being displayed while spectated player is in pause menu
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear one set of copied corona data
//
// INPUT PARAMS:		paramArrayPos			Array position within the copied corona array
//
// NOTES:	Updated by the SCTV (Spectating) player
PROC Clear_One_Copy_Corona_Data_For_SCTV(INT paramArrayPos)

	g_localCopyCoronaSCTV[paramArrayPos].ccsctv_inUse		= FALSE
	g_localCopyCoronaSCTV[paramArrayPos].ccsctv_position	= << 0.0, 0.0, 0.0 >>
	g_localCopyCoronaSCTV[paramArrayPos].ccsctv_colour		= HUD_COLOUR_PURE_WHITE
	g_localCopyCoronaSCTV[paramArrayPos].ccsctv_radius		= 0.0

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      The SCTV (Spectator) Player - Reading the Paused information from the spectated player
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Spectator should still display corona details even though the Spectated Player is no longer displaying them (ie: becasue on pause menu)
//
// INPUT PARAMS:			paramPlayerAsInt			The Spectated Player's ID as an int
// RETURN VALUE:			BOOL						TRUE if the corona and details should still be displayed, otherwise FALSE
//
// NOTES:	Checked by SCTV (Spectating) player
FUNC BOOL Should_Spectator_Use_Paused_SCTV_Corona_Details(INT paramPlayerAsInt)

	IF NOT (GlobalPlayerBD_SCTV[paramPlayerAsInt].pausedSCTV.pcsctv_isOnPause)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
				
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      The SCTV (Spectator) Player - Dealing with the retained Corona Details information
//		(used to still display the corona text if the Spectated Player is in the pause menu)
// -----------------------------------------------------------------------------------------------------------

// Clear the additional control data needed by an SCTV player to load a corona details scaleform movie to display similar info as the spectated player
PROC Clear_One_Corona_Details_Data_For_SCTV_Player(INT paramArrayPos)

	g_localCoronaDetailsSCTV[paramArrayPos].ldcdsctv_messageID		= NO_LOCATE_MESSAGE_ID
	g_localCoronaDetailsSCTV[paramArrayPos].ldcdsctv_detailsPos		= << 0.0, 0.0, 0.0 >>
	g_localCoronaDetailsSCTV[paramArrayPos].ldcdsctv_okToDisplay	= FALSE
	g_localCoronaDetailsSCTV[paramArrayPos].ldcdsctv_copyDisplayed	= FALSE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// Draw an SCTV corona
//
// INPUT PARAMS:			paramPosition			The Corona Position
//							paramColour				The Corona Colour
//							paramRadius				The Corona Radius
PROC Draw_SCTV_Corona(VECTOR paramPosition, HUD_COLOURS paramColour, FLOAT paramRadius)
	
	// Get the corona details
	// ...colour
	INT         theRed				= 0
	INT         theGreen			= 0
	INT         theBlue				= 0
	INT         theAlpha			= 0
	
	// ...scale
	FLOAT       displayDiameter		= paramRadius * 2 * MATC_CORONA_DRAW_SIZE_MODIFIER
	FLOAT       coronaHeight		= MATC_CORONA_INACTIVE_HEIGHT_m
	VECTOR      theScale			= << displayDiameter, displayDiameter, coronaHeight >>
		
	// ...unchanged values
	VECTOR      ignoreDirection		= << 0.0, 0.0, 0.0 >>
	VECTOR      ignoreRotation		= << 0.0, 0.0, 0.0 >>

	paramPosition.z -= MATC_CORONA_BELOW_GROUND_m
	
	GET_HUD_COLOUR(paramColour, theRed, theGreen, theBlue, theAlpha)
	theAlpha = MATC_CORONA_INACTIVE_ALPHA

	DRAW_MARKER(MARKER_CYLINDER, paramPosition, ignoreDirection, ignoreRotation, theScale, theRed, theGreen, theBlue, theAlpha)
	
	// Corona Light
	Draw_Light_At_Corona(paramColour, paramPosition)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// This player is an SCTV player (ie: spectating another player), so display any coronas near the spectated player (by accessing playerBD)
PROC Maintain_SCTV_Corona_Data()

	PLAYER_INDEX spectatedPlayerID = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
	IF spectatedPlayerID = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF
	IF NOT (IS_NET_PLAYER_OK(spectatedPlayerID, FALSE))
		EXIT
	ENDIF
	
	// Get the corona details
	// ...colour
	HUD_COLOURS	theColour				= HUD_COLOUR_WHITE
	
	// Draw the Corona
	INT			tempLoop				= 0
	VECTOR		coronaXYZ				= << 0.0, 0.0, 0.0 >>
	FLOAT		theRadius				= ciIN_LOCATE_DISTANCE
	
	// General Info
	INT			playerAsInt				= NATIVE_TO_INT(spectatedPlayerID)
	BOOL		spectatedPlayerIsPaused	= Should_Spectator_Use_Paused_SCTV_Corona_Details(playerAsInt)
	
	REPEAT MAX_SCTV_CORONAS tempLoop
		IF (GlobalPlayerBD_SCTV[playerAsInt].coronasSCTV[tempLoop].ccsctv_inUse)
			// ...found the entry, so display it
			coronaXYZ = GlobalPlayerBD_SCTV[playerAsInt].coronasSCTV[tempLoop].ccsctv_position
			theColour = GlobalPlayerBD_SCTV[playerAsInt].coronasSCTV[tempLoop].ccsctv_colour
			theRadius = GlobalPlayerBD_SCTV[playerAsInt].coronasSCTV[tempLoop].ccsctv_radius
			Draw_SCTV_Corona(coronaXYZ, theColour, theRadius)
			
			// ...and store the information in case the spectated player goes onto the pause menu
			g_localCopyCoronaSCTV[tempLoop].ccsctv_inUse		= TRUE
			g_localCopyCoronaSCTV[tempLoop].ccsctv_position		= coronaXYZ
			g_localCopyCoronaSCTV[tempLoop].ccsctv_colour		= theColour
			g_localCopyCoronaSCTV[tempLoop].ccsctv_radius		= theRadius
		ELSE
			// ...not in use, but if the spectated player is paused then use the copies data, otherwise clear the copied data
			IF (spectatedPlayerIsPaused)
				// ...spectated player is paused, so continue drawing the corona using the local copy
				IF (g_localCopyCoronaSCTV[tempLoop].ccsctv_inUse)
					Draw_SCTV_Corona(g_localCopyCoronaSCTV[tempLoop].ccsctv_position, g_localCopyCoronaSCTV[tempLoop].ccsctv_colour, g_localCopyCoronaSCTV[tempLoop].ccsctv_radius)
				ENDIF
			ELSE
				// ...spectated player is not paused, so clear the local copy corona data
				IF (g_localCopyCoronaSCTV[tempLoop].ccsctv_inUse)
					Clear_One_Copy_Corona_Data_For_SCTV(tempLoop)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	BOOL clearTheCoronaDetails = FALSE
	
	// Display any 'Corona Details Text' on display on the spectated player's machine
	REPEAT MAX_LOCATE_MESSAGE_MOVIES tempLoop
		// Details stored for this array position?
		IF (GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_missionID != eNULL_MISSION)
			// ...yes, there are details in this array position
			// Is a movie already setup for display with these details?
			IF NOT (g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_okToDisplay)
				// ...no, not yet on display, so request a scaleform movie
				IF (Request_Locate_Message(g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_messageID))
					// ...scaleform movie now loaded, so store the display details and mark locally as 'on display'
					g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_okToDisplay = TRUE
					
					Store_Locate_Message_Details_For_SCTV(	g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_messageID,
															GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_missionID,
															GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_missionTitle,
															GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_textColour,
															GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_secondLine,
															GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_verified,
															GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_restriction,
															GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_tod,
															GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_minPlayers,
															GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_maxPlayers,
															GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_userRating)
				ENDIF
			ELSE
				// ...yes, on display, but if it was a copy of the data that was displayed in the previous frame then the details may have changed and need tidied up
				IF (g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_copyDisplayed)
					IF NOT (ARE_VECTORS_ALMOST_EQUAL(g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_detailsPos, GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_position))
						// ...the vectors are different so these details shouldn't be displayed to allow new details to load
						Clear_One_Corona_Details_Data_For_SCTV_Player(tempLoop)
						PRINTLN(".KGM [At Coords][SCTV]: Spectated Player must be off the Pause Menu and the Corona Details have changed, so cleaning up local copy: ", tempLoop)
					ENDIF
				ENDIF
				
				g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_copyDisplayed = FALSE
			ENDIF
			
			// Movie is now setup for display?
			IF (g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_okToDisplay)
				// Store the Details Position (in case the spectator needs to display this stuff when the spectator goes into the pause menu)
				g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_detailsPos = GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_position
				
				Display_Locate_Message(g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_messageID, GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_position)
			ENDIF
		ELSE
			// ...no, so clear out any local details if previously stored
			clearTheCoronaDetails = TRUE
			
			// KGM 10/4/14: Don't clear these details if the 'retain corona' details are stored meaning the spectated player can't see the corona but spectators still should
			//				(ie: because spectated player has activated the pause menu)
			IF (g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_okToDisplay)
				IF (spectatedPlayerIsPaused)
					// ...spectated Player is on the Pause Menu, so continue to display the details and indicate that it was a copy of the details that were displayed
					Display_Locate_Message(g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_messageID, g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_detailsPos)
					g_localCoronaDetailsSCTV[tempLoop].ldcdsctv_copyDisplayed = TRUE
					clearTheCoronaDetails = FALSE
					PRINTLN(".KGM [At Coords][SCTV]: Spectated Player is on the pause menu, so keep displaying the details: ", tempLoop)
				ENDIF
			ENDIF
			
			// ...safe to clear the details
			IF (clearTheCoronaDetails)
				Clear_One_Corona_Details_Data_For_SCTV_Player(tempLoop)
			ENDIF
		ENDIF
	ENDREPEAT

//	// TEMP OUTPUT
//	PRINTSTRING("KGMDETAILS [SCTV]: ")
//	REPEAT MAX_LOCATE_MESSAGE_MOVIES tempLoop
//		IF (GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_missionID != eNULL_MISSION)
//			PRINTINT(tempLoop)
//			PRINTSTRING(": ")
//			PRINTSTRING(GET_MP_MISSION_NAME(GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_missionID))
//			PRINTSTRING(" [")
//			PRINTSTRING(GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_missionTitle)
//			PRINTSTRING("] Name: ")
//			PRINTSTRING(GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_missionName)
//			PRINTSTRING(" ")
//			IF (GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_restriction)
//				PRINTSTRING("- HAS RESTRICTION  ")
//			ENDIF
//			IF (GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_verified)
//				PRINTSTRING("- R* VERIFIED  ")
//			ENDIF
//			IF (GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_onDisplay)
//				PRINTVECTOR(GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[tempLoop].cdsctv_position)
//				PRINTSTRING(" - ON DISPLAY  ")
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	PRINTNL()

ENDPROC




// ===========================================================================================================
//      Mission At Coords Corona Display Routines
// ===========================================================================================================

// PURPOSE:	Get the mission corona scale
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//							paramUseActiveTXD	TRUE if the activeTXD should be used, otherwise FALSE
FUNC VECTOR Get_MissionsAtCoords_Corona_Scale(INT paramSlot, BOOL paramUseActiveTXD)

	FLOAT	triggerRadius	= Get_MissionsAtCoords_Slot_Corona_Trigger_Radius(paramSlot)
	FLOAT	displayDiameter	= triggerRadius * 2 * MATC_CORONA_DRAW_SIZE_MODIFIER
	FLOAT	coronaHeight	= MATC_CORONA_INACTIVE_HEIGHT_m
	
	IF	(paramUseActiveTXD)
	AND	(Is_MatC_Corona_Active_TXD_Loaded())
		coronaHeight = MATC_CORONA_ACTIVE_HEIGHT_m
	ENDIF

	VECTOR returnScale = << displayDiameter, displayDiameter, coronaHeight >>
	RETURN (returnScale)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Draw a mission corona
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//
// NOTES:	Assumes all checks have been made prior to calling this routine, so just draw the corona
PROC Draw_MissionsAtCoords_Corona(INT paramSlot)

	// Should this mission draw a corona?
	IF NOT (Should_MissionsAtCoords_Mission_Display_A_Corona(paramSlot))
		EXIT
	ENDIF
	
	// Don't draw this corona if it is an overlapped mission (unless on a commandline parameter)
	IF (Is_MissionsAtCoords_Mission_Hidden_By_Overlapping_Corona(paramSlot))
		BOOL quitIfOverlapped = TRUE
		
		#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_ShowOverlappingCoronas"))
				quitIfOverlapped = FALSE
			ENDIF
		#ENDIF
		
		IF (quitIfOverlapped)
			EXIT
		ENDIF
	ENDIF
		
	// KGM 12/5/15: BUG 2313555 - Don't draw a corona if hidden by a PI Menu Job Type Hide Blip option
	// KGM 11/6/15: BUG 2316703 - Also hide if the mission is within a disabled area
	IF (Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu(paramSlot))
	OR (Is_MissionsAtCoords_Mission_Hidden_By_Disabled_Area(paramSlot))
		EXIT
	ENDIF

	// Check if this corona should display the active corona TXD
	// KGM 25/3/13: This is quite an expensive method so perhaps the work can be staggered
	MP_MISSION_ID_DATA	theMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	BOOL				useActiveCoronaTXD	= Does_Game_Have_A_Matc_Corona_Active_TXD()
	
	// If there is an active texture defined for use in the game, then check if it needs to be displayed for this corona
	IF (useActiveCoronaTXD)
		useActiveCoronaTXD	= Is_This_Mission_Request_Active_And_Joinable(theMissionIdData)
	ENDIF
	
	// Set whether this corona needs to use the active TXD
	IF (useActiveCoronaTXD)
		Set_MatC_Corona_Requires_Active_TXD()
	ENDIF

	// Get the corona details
	// ...colour
	INT		theRed		= 0
	INT		theGreen	= 0
	INT		theBlue		= 0
	INT		theAlpha	= 0
	GET_HUD_COLOUR(Get_MissionsAtCoords_Slot_Corona_Hud_Colour(paramSlot), theRed, theGreen, theBlue, theAlpha)
	
	// ...coords (should be positioned on the ground)
	VECTOR	theCoords	= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	theCoords.z -= MATC_CORONA_BELOW_GROUND_m
	
	// ...scale
	VECTOR	theScale	= Get_MissionsAtCoords_Corona_Scale(paramSlot, useActiveCoronaTXD)
	
	// ...unchanged values
	VECTOR	ignoreDirection	= << 0.0, 0.0, 0.0 >>
	VECTOR	ignoreRotation	= << 0.0, 0.0, 0.0 >>
	
	// Draw the Corona
	IF	(useActiveCoronaTXD)
	AND (Is_MatC_Corona_Active_TXD_Loaded())
		// ...draw Active Corona
		BOOL spinCorona	= TRUE
		DRAW_MARKER(MARKER_CYLINDER, theCoords, ignoreDirection, ignoreRotation, theScale,
					theRed, theGreen, theBlue, MATC_CORONA_ACTIVE_ALPHA,
					DEFAULT, DEFAULT, DEFAULT,
					spinCorona, MATC_CORONA_ACTIVE_TXD_NAME, MATC_CORONA_ACTIVE_TEXTURE_NAME)
	ELSE
		// ...draw Inactive Corona
		DRAW_MARKER(MARKER_CYLINDER, theCoords, ignoreDirection, ignoreRotation, theScale,
					theRed, theGreen, theBlue, MATC_CORONA_INACTIVE_ALPHA)
	ENDIF
	
	// Corona Light
	Draw_Light_At_Corona(Get_MissionsAtCoords_Slot_Corona_Hud_Colour(paramSlot), Get_MissionsAtCoords_Slot_Coords(paramSlot))
	
	// If a corona has been drawn, update the SCTV data
	Set_Corona_Data_For_Storage_For_SCTV(paramSlot)
				
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// KGM 27/7/13: Corona Icons are no longer being displayed
//// PURPOSE:	Draw a mission corona icon
////
//// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
////
//// NOTES:	Assumes all checks have been made prior to calling this routine, so just draw the corona
//PROC Draw_MissionsAtCoords_Corona_Icons(INT paramSlot)
//
//	// Should this mission draw a corona icon?
//	IF NOT (Should_MissionsAtCoords_Mission_Display_A_Corona(paramSlot))
//		EXIT
//	ENDIF
//
//	// The Icon
//	eMP_TAG_SCRIPT theIcon = Get_MissionsAtCoords_Slot_Corona_Icon(paramSlot)
//	
//	IF (theIcon = MP_TAG_SCRIPT_NONE)
//		EXIT
//	ENDIF
//	
//	// Only draw the corona icon if the blip is on the minimap
//	BLIP_INDEX theBlip = Get_MissionsAtCoords_Slot_BlipIndex(paramSlot)
//	
//	IF NOT (IS_BLIP_ON_MINIMAP(theBlip))
//		EXIT
//	ENDIF
//	
//	// The placement details
//	VECTOR			theCoords		= << 0.0, 0.0, 0.0 >>
//	HUD_COLOURS		theColour		= Get_MissionsAtCoords_Slot_Corona_Hud_Colour(paramSlot)
//	BOOL			doFadeIn		= TRUE
//	FLOAT			fadeInStart		= CORONA_ICON_FAR_RANGE_START_m
//	FLOAT			fadeInEnd		= fadeInStart - CORONA_ICON_FADE_IN_RANGE_m
//	BOOL			doFadeOut		= FALSE
////	FLOAT			fadeOutEnd		= Get_MissionsAtCoords_Inner_Range_Boundary(MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE, paramSlot) - CORONA_ICON_FADE_OUT_CROSSOVER_RANGE_m
////	FLOAT			fadeOutStart	= fadeOutEnd + CORONA_ICON_FADE_OUT_RANGE_m
//
//	// KGM 22/3/13: Don't do a fade out anymore, instead stop displaying if the locate message is on display
//	IF (Get_MissionsAtCoords_Locate_MessageID_From_Bitflags(paramSlot) != NO_LOCATE_MESSAGE_ID)
//		IF (Get_MissionsAtCoords_Stage_For_Mission(paramSlot) != MATCS_REQUESTING_MISSION_NAME)
//			EXIT
//		ENDIF
//	ENDIF
//
//	// Calculate the coords
//	theCoords	= Get_MissionsAtCoords_Slot_Coords(paramSlot)
//	theCoords.z += CORONA_ICON_HEIGHT_m
//
//	// Draw the icon
//	DRAW_SPRITE_ON_OBJECTIVE_COORD_THIS_FRAME_WITH_FADES_IN_AND_OUT(theCoords, theIcon, theColour, doFadeIn, fadeInStart, fadeInEnd, doFadeOut)	//, fadeOutStart, fadeOutEnd)
//
//ENDPROC




// ===========================================================================================================
//      Mission At Coords Slot Update Routines
// ===========================================================================================================

// PURPOSE:	Set the mission in the specified slot as 'on mission'
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//
// NOTES:	Assumes all checks have already been made to ensure this is safe to do
PROC Update_MissionsAtCoords_Slot_To_Be_On_Mission(INT paramSlot)

	// Change the mission's stage to be 'On Mission'
	Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_ON_MISSION)
	
	// Clear out the 'Being Played Again' flag
	Clear_MissionsAtCoords_Mission_As_Being_Played_Again(paramSlot)
	
	// Clear out the 'Random' and Copy Use data (this is to fix bug 1602754 where Quick Restart of a CM then cancelling on the phone still had teh 'play again' details stored and relaunched)
	Clear_Random_And_Play_Again_Data()
	
	// Clear out the 'Cleared Wanted Level' values
	Clear_MissionsAtCoords_Cleared_Wanted_Level()
	
	// Inform the requesting function that the mission is started
	Add_MissionsAtCoords_Feedback(paramSlot, MATCFBT_STARTED)
	
ENDPROC




// ===========================================================================================================
//      The MP Mission At Coords Focus Mission Visuals routines
// ===========================================================================================================

// PURPOSE:	Setup the Focus Mission Corona Visuals
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
PROC Setup_Focus_Mission_Corona_Visuals(INT paramSlot)

	// Are Corona Focus Visuals required?
	IF (HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM())
	OR (IS_TRANSITION_SESSION_RESTARTING())
	OR (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
	OR (Should_MissionsAtCoords_Mission_Launch_Immediately(paramSlot))
	OR (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
	OR NOT (Should_MissionsAtCoords_Mission_Display_A_Corona(paramSlot))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Focus Mission Corona Visuals are NOT REQUIRED.") PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF
	
	// Setup Corona Mission Focus Visuals
	g_sCoronaFocusVisuals.visualsActive			= TRUE
	g_sCoronaFocusVisuals.coronaFlashStartTime	= GET_GAME_TIMER()
	g_sCoronaFocusVisuals.coronaPosition		= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	g_sCoronaFocusVisuals.coronaScale			= Get_MissionsAtCoords_Corona_Scale(paramSlot, FALSE)
	g_sCoronaFocusVisuals.retainedCoronaActive	= TRUE
	g_sCoronaFocusVisuals.retainedLocateMessage	= Get_MissionsAtCoords_Locate_MessageID_From_Bitflags(paramSlot)
	g_sCoronaFocusVisuals.coronaNoFlashColour	= Get_MissionsAtCoords_Slot_Corona_Hud_Colour(paramSlot)
	
	// If there is a cutscene ongoing, then don't flash the marker, retain it instead (this is for the end of the Cypress Flats LTS cut)
	// Also, don't let the race tut corona flash (ToDo: 1626243)
	IF (MPGlobals.bStartedMPCutscene)
	OR NOT (HAS_FM_RACE_TUT_BEEN_DONE())
		// ...keep the corona on display, no flash
		g_sCoronaFocusVisuals.coronaFlashActive		= FALSE
		g_sCoronaFocusVisuals.coronaNoFlashActive	= TRUE
	ELSE
		// ...flash the corona
		g_sCoronaFocusVisuals.coronaFlashActive		= TRUE
		g_sCoronaFocusVisuals.coronaNoFlashActive	= FALSE
	ENDIF
		

//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Focus Mission Corona Visuals SET UP.") PRINTNL()
//	#ENDIF
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Draw the Focus Mission Visuals - Generic (flash or retain)
//
// INPUT PARAMS:		paramColour			The corona colour
//						paramAlpha			The alpha 
PROC Draw_Focus_Mission_Visuals_Retained_Corona_Generic(HUD_COLOURS paramColour, INT paramAlpha)

	// Get the corona details
	// ...colour
	INT		theRed		= 0
	INT		theGreen	= 0
	INT		theBlue		= 0
	INT		theAlpha	= 0
	GET_HUD_COLOUR(paramColour, theRed, theGreen, theBlue, theAlpha)
	
	// ...coords (should be positioned on the ground)
	VECTOR	theCoords = g_sCoronaFocusVisuals.coronaPosition
	theCoords.z -= MATC_CORONA_BELOW_GROUND_m
	
	// ...scale
	VECTOR	theScale = g_sCoronaFocusVisuals.coronaScale
	
	// ...unchanged values
	VECTOR	ignoreDirection	= << 0.0, 0.0, 0.0 >>
	VECTOR	ignoreRotation	= << 0.0, 0.0, 0.0 >>
	
	// ...the alpha
	theAlpha = paramAlpha
	
	// Draw the Corona
	DRAW_MARKER(MARKER_CYLINDER, theCoords, ignoreDirection, ignoreRotation, theScale,
				theRed, theGreen, theBlue, theAlpha)
			
// Doing this here is too short, doing it within 'waiting for allow reservation' instead
//	// Corona Light
//	Draw_Light_At_Corona(paramColour, g_sCoronaFocusVisuals.coronaPosition)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Draw the Focus Mission Visuals - Corona Flash
PROC Draw_Focus_Mission_Visuals_Corona_Flash()

	IF NOT (g_sCoronaFocusVisuals.coronaFlashActive)
		EXIT
	ENDIF

	// If there is no longer a focus mission then stop drawing the marker and clear the visuals flag.
	// This is to stop an array overrrun where the focus mission slot is -1.
	IF NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
		g_sCoronaFocusVisuals.coronaFlashActive = FALSE
		
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Draw_Focus_Mission_Visuals_Corona_Flash() - There is no longer a focus mission, so kill the corona flash now") PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF

	CONST_INT	CORONA_FLASH_TIME_msec			400
	CONST_INT	CORONA_FLASH_MAX_ALPHA			200
	
	HUD_COLOURS	CORONA_FLASH_COLOUR				= HUD_COLOUR_WHITE
	
	// ...calculate the alpha based on the display time so far
	BOOL	cleanupVisuals		= FALSE
	INT		displayTimeSoFar	= GET_GAME_TIMER() - g_sCoronaFocusVisuals.coronaFlashStartTime
	
	IF (displayTimeSoFar >= CORONA_FLASH_TIME_msec)
		displayTimeSoFar	= CORONA_FLASH_TIME_msec
		cleanupVisuals		= TRUE
	ENDIF
	
	INT theAlpha = ((CORONA_FLASH_TIME_msec - displayTimeSoFar) * 100 / CORONA_FLASH_TIME_msec) * CORONA_FLASH_MAX_ALPHA / 100
	
	// Call the common corona draw routine
	Draw_Focus_Mission_Visuals_Retained_Corona_Generic(CORONA_FLASH_COLOUR, theAlpha)

//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Drawing Focus Mission Corona Flash. Alpha: ")
//		PRINTINT(theAlpha)
//		PRINTNL()
//	#ENDIF
	
	IF (cleanupVisuals)
		g_sCoronaFocusVisuals.coronaFlashActive = FALSE
		
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING("      - SWITCHING OFF CORONA FLASH VISUALS") PRINTNL()
//		#ENDIF
	ENDIF
					
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Draw the Locate Message and the Ground Projection for the retained corona
PROC Draw_Focus_Mission_Retained_Corona_Active()

	IF NOT (g_sCoronaFocusVisuals.retainedCoronaActive)
		EXIT
	ENDIF

	// If there is no longer a focus mission then clear the retained flag which will allow the ground projection to clear up, and stop maintaining the locate message.
	IF NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
		g_sCoronaFocusVisuals.retainedCoronaActive = FALSE
		
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Draw_Focus_Mission_Retained_Corona_Active() - There is no longer a focus mission, so allow Locate Message and Ground Projection to clear up") PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF
	
	// Clean up if the player is actively on a mission
	// This is intended as a safety catch-all
	IF (Is_Player_On_Or_Triggering_Any_Mission())
		g_sCoronaFocusVisuals.retainedCoronaActive = FALSE
		
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Draw_Focus_Mission_Retained_Corona_Active() - Player is on or triggering a mission (safety check), so allow Locate Message and Ground Projection to clear up") PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF
	
	// Clean up if the corona status is 'walk-in' and an MP Cutscene isn't in progress
	// This is intended as the check that gets rid of the retained coronas during a normal walk-in
	// (With the MP Cutscene addition intended to keep everything on display at the end of the Cypress Falls LTS cutscene)
	CORONA_STATUS_ENUM theCoronaStatus = GET_CORONA_STATUS()
	
	IF (theCoronaStatus = CORONA_STATUS_WALK_IN)
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Draw_Focus_Mission_Retained_Corona_Active() - Corona Status is 'Walk-in', so allow Locate Message and Ground Projection to clear up") PRINTNL()
//		#ENDIF
		
		IF (MPGlobals.bStartedMPCutscene)
//			#IF IS_DEBUG_BUILD
//				PRINTSTRING("      - BUT: MP Cutscene is still active, so don't clear up") PRINTNL()
//			#ENDIF
		ELSE
			g_sCoronaFocusVisuals.retainedCoronaActive = FALSE
		
			EXIT
		ENDIF
	ENDIF
	
	// Clean up if the corona status is 'join/host'
	// This is in case the walk-in gets ignored
	IF (theCoronaStatus = CORONA_STATUS_JOIN_HOST)
		g_sCoronaFocusVisuals.retainedCoronaActive = FALSE
		
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Draw_Focus_Mission_Retained_Corona_Active() - Corona Status is 'Join/Host', so allow Locate Message and Ground Projection to clear up") PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF
	
	// Clean up if the corona status is 'HandShake' (ie: waiting to be told mission reservation has ended)
	// This is in case the walk-in gets ignored
	IF (theCoronaStatus = CORONA_STATUS_MatC_HAND_SHAKE)
		g_sCoronaFocusVisuals.retainedCoronaActive = FALSE
		
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Draw_Focus_Mission_Retained_Corona_Active() - Corona Status is 'Matc HandShake', so allow Locate Message and Ground Projection to clear up") PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF
	
	// Clean up if the corona status is beyond 'handshake' - this is a safety catch-all, just in case
	// This is in case of an unknown circumstance
	IF (ENUM_TO_INT(theCoronaStatus) > ENUM_TO_INT(CORONA_STATUS_MatC_HAND_SHAKE))
		g_sCoronaFocusVisuals.retainedCoronaActive = FALSE
		
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Draw_Focus_Mission_Retained_Corona_Active() - Corona Status is beyond 'Matc HandShake' (safety check), so allow Locate Message and Ground Projection to clear up") PRINTNL()
//		#ENDIF
		
		EXIT
	ENDIF
	
	// The Ground Projection is maintained through the retainedCoronaActive flag being TRUE, the locate message stops being retained by the game when the player
	//		is no longer requesting or displaying the locate message, so I'll maintain it here
	INT locateMessageID	= g_sCoronaFocusVisuals.retainedLocateMessage
	VECTOR theCoords = g_sCoronaFocusVisuals.coronaPosition

	// Keep the Locate Message Alive
	// KGM 9/6/14: BUG 1874177 - player was placed in tutorial corona which activated before the locate message was loaded in causing an error when trying to display a retained version of it
	IF (locateMessageID != NO_LOCATE_MESSAGE_ID)
	AND (Is_This_Locate_Message_Ready_To_Use(locateMessageID))
		Display_Locate_Message(locateMessageID, theCoords)
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Temporarily retaining Focus Mission Ground Projection and the Locate Message (if it had been on display).") PRINTNL()
//	#ENDIF
	
	// If the corona itself is required to be retained, the redraw it
	IF (g_sCoronaFocusVisuals.coronaNoFlashActive)
		HUD_COLOURS theColour = g_sCoronaFocusVisuals.coronaNoFlashColour
		
		Draw_Focus_Mission_Visuals_Retained_Corona_Generic(theColour, MATC_CORONA_INACTIVE_ALPHA)

//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Drawing Retained Focus Mission Corona.")
//			PRINTNL()
//		#ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain any Focus Mission Corona Visuals when a player has just walked into a corona
PROC Maintain_MissionsAtCoords_Focus_Mission_Corona_Visuals()

	IF NOT (g_sCoronaFocusVisuals.visualsActive)
		EXIT
	ENDIF
	
	Draw_Focus_Mission_Visuals_Corona_Flash()
	Draw_Focus_Mission_Retained_Corona_Active()

	// All finished?
	IF NOT (g_sCoronaFocusVisuals.coronaFlashActive)
	AND NOT (g_sCoronaFocusVisuals.retainedCoronaActive)
		// ...Clear the struct
		g_structFocusMissionVisuals emptyFocusViualsStruct
		g_sCoronaFocusVisuals = emptyFocusViualsStruct
		g_sCoronaFocusVisuals.coronaPosition = << 0.0, 0.0, 0.0 >>
		
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Switching off all Corona Visuals") PRINTNL()
//		#ENDIF
	ENDIF

ENDPROC




// ===========================================================================================================
//      The MP Mission At Coords Stage Check routines
// ===========================================================================================================

// PURPOSE:	To get the Leader's PlayerID as an INT of the Heist Corona being joined
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			INT					The PlayerIndex as INT of the Heist Corona Leader, or -1 if not a heist-related mission, or no Leader, etc
FUNC INT Get_Heist_LeaderAsInt_Of_Heist_Corona_Being_Joined(INT paramSlot)

	MP_MISSION_ID_DATA	theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)

	IF NOT (Is_FM_Cloud_Loaded_Activity_A_Heist(theMissionIdData))
	AND NOT (Is_FM_Cloud_Loaded_Activity_Heist_Planning(theMissionIdData))
		RETURN -1
	ENDIF
	
	PRINTLN(".KGM [At Coords][Heist]: Retrieving Heist Corona Leader for Heist-Related Mission Join Request")
	
	IF (NETWORK_IS_ACTIVITY_SESSION())
		PRINTLN(".KGM [At Coords][Heist]: ...in an Activity Session, so don't need to know the Leader. Returning -1")
		RETURN (-1)
	ENDIF
	
	IF (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
		PRINTLN(".KGM [At Coords][Heist]: ...mission is being played again, so don't need to know the Leader. Returning -1")
		RETURN (-1)
	ENDIF
	
	IF (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
		// If player is in own property then return -1 for 'leader unknown'. This player may be Leader, or may have accepted a cross-session invite into another Heist corona.
		// Either way, doesn't matter - a new mission will either get started, or the player will join based on the transition session info
		PRINTLN(".KGM [At Coords][Heist]: ...in my own property, so treat Heist Corona Leader is unknown (It's either this player, or this is a CSInvite). Returning -1")
		RETURN (-1)
	ENDIF

	// Loop through all active players in the session
	PLAYER_INDEX	thisPlayerID	= INVALID_PLAYER_INDEX()
	INT				playerLoop		= 0
	
	REPEAT NUM_NETWORK_PLAYERS playerLoop
		thisPlayerID = INT_TO_PLAYERINDEX(playerLoop)
		IF (IS_NET_PLAYER_OK(thisPlayerID, FALSE))
			IF (thisPlayerID != PLAYER_ID())
				// ...found another player, so check if players are in the same apartment?
				IF (ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), thisPlayerID, TRUE))
					// ...players are in the same apartment, so see if this player is registered as the first player to share Mission Data (which would make the player the Leader)
					// Loop through all 'active' pieces of Shared Mission Data (this should really be a routine within the Shared Mission Data header)
					INT sharedLoop = 0
					REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA sharedLoop
						IF (GlobalServerBD_MissionsShared.cloudDetails[sharedLoop].mscInUse)
							// ...found shared data in use, so check if the player is teh first registered player
							IF (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[sharedLoop].mscBitFirstPlayerReg, playerLoop))
								// ...player is first registered player, so found the Leader
								PRINTLN(".KGM [At Coords][Heist]: ...in another player's property with a Leader. Returning: ", playerLoop, " [", GET_PLAYER_NAME(thisPlayerID), "]")
								RETURN (playerLoop)
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// ...not found
	PRINTLN(".KGM [At Coords][Heist]: ...heist corona Leader not found (possible JIP, etc). Returning -1")
	RETURN (-1)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To get the Leader's PlayerID as an INT of the Heist Corona being joined
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if there is room for another player in Leader's apartment to walk-in, otherwise FALSE
FUNC BOOL Is_There_Room_On_Leader_Heist_For_Player_To_Walk_In(INT paramSlot)

	// Is this a Heist-related mission?
	MP_MISSION_ID_DATA	theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)

	IF NOT (Is_FM_Cloud_Loaded_Activity_A_Heist(theMissionIdData))
	AND NOT (Is_FM_Cloud_Loaded_Activity_Heist_Planning(theMissionIdData))
		// ...not heist-related, so assume the mission has room
		PRINTLN(".KGM [At Coords][Heist]: Is_There_Room_On_Leader_Heist_For_Player_To_Walk_In() - Not Heist-Related, so allow")
		RETURN TRUE
	ENDIF
	
	// Is there a known Heist Leader?
	INT leaderAsInt = Get_Heist_LeaderAsInt_Of_Heist_Corona_Being_Joined(paramSlot)
	IF (leaderAsInt = -1)
		// ...no known heist leader, so assume the mission has room
		PRINTLN(".KGM [At Coords][Heist]: Is_There_Room_On_Leader_Heist_For_Player_To_Walk_In() - No known heist Leader, so allow")
		RETURN TRUE
	ENDIF
	
	// Ignore if I am Heist Leader
	PLAYER_INDEX theLeader = INT_TO_PLAYERINDEX(leaderAsInt)
	IF NOT (IS_NET_PLAYER_OK(theLeader, FALSE))
		// ...Leader is not ok, so let things play out as normal
		PRINTLN(".KGM [At Coords][Heist]: Is_There_Room_On_Leader_Heist_For_Player_To_Walk_In() - Leader is not ok, so continue to let the usual rules apply")
		RETURN TRUE
	ENDIF
	
	IF (theLeader = PLAYER_ID())
		// ...I am Leader
		PRINTLN(".KGM [At Coords][Heist]: Is_There_Room_On_Leader_Heist_For_Player_To_Walk_In() - I am Leader, so allow")
		RETURN TRUE
	ENDIF

	// Ignore this check if this player is already allocated to a Mission Control mission - it may already be the Leader's mission following invite acceptance
	IF (Is_Player_Allocated_For_Any_Mission(PLAYER_ID()))
		PRINTLN(".KGM [At Coords][Heist]: Is_There_Room_On_Leader_Heist_For_Player_To_Walk_In() - Player is allocated to a mission - assume not walking in, so allow")
		RETURN TRUE
	ENDIF
	
	// Check if the Heist Leader's mission can take more players
	IF (Can_Another_Player_Be_Reserved_On_This_Players_Mission(theLeader))
		// ...Leader's mission is not full
		PRINTLN(".KGM [At Coords][Heist]: Is_There_Room_On_Leader_Heist_For_Player_To_Walk_In() - Leader mission is not full, so allow")
		RETURN TRUE
	ENDIF
	
	// Leader's mission is full, but that includes checking for players in different session, which could be this player - so check if the Leader is in this player's transition session
	IF (NETWORK_IS_IN_TRANSITION())
    	GAMER_HANDLE leaderGH = GET_GAMER_HANDLE_PLAYER(theLeader)
		IF (IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(leaderGH))
			// ...this player is already in the Leader's transition session, so ok to join
			PRINTLN(".KGM [At Coords][Heist]: Is_There_Room_On_Leader_Heist_For_Player_To_Walk_In() - Player is already in Leader's transition session, so allow")
			RETURN TRUE
		ENDIF
	ENDIF
	
	PRINTLN(".KGM [At Coords][Heist]: There is no room for more players on the Leader's mission: ", GET_PLAYER_NAME(theLeader))
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Focus Mission has received confirmation of the mission being reserved, and update the Focus Mission variables
//
// RETURN VALUE:			BOOL			TRUE if the mission confirmation has been received, FALSE if not received
//
// NOTES:	Assumes the Focus Variables are valid (this function should only be called from a valid Focus Mission update)
FUNC BOOL Check_For_Reserve_Focus_Mission_Confirmation_And_Update_Focus_Variables()

	INT numReservedPlayers	= 0
	INT bitsReservedPlayers	= ALL_MISSIONS_AT_COORDS_BITS_CLEAR
	
	// NOTE: Use the current uniqueID for the check - it may then get updated
	IF NOT (Has_MP_Mission_Request_Just_Broadcast_Confirm_Reservation(g_sAtCoordsFocusMP.focusUniqueID, numReservedPlayers, bitsReservedPlayers))
		// ...confirmation still not received
		RETURN FALSE
	ENDIF
	
	// Confirmation has been received, so store the reserved players details received with confirmation
	g_sAtCoordsFocusMP.focusBitsPlayers	= bitsReservedPlayers

	// The unique ID may also have changed - it will always have changed unless this player is the first one reserved for the mission
	INT tempUniqueID = Get_MP_Mission_Request_Changed_UniqueID(g_sAtCoordsFocusMP.focusUniqueID)
						
	// Store the modified uniqueID
	IF NOT (tempUniqueID = g_sAtCoordsFocusMP.focusUniqueID)
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Focus Mission uniqueID change. oldUniqueID = ")
			PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
			PRINTSTRING(". newUniqueID = ")
			PRINTINT(tempUniqueID)
			PRINTNL()
		#ENDIF
	
		g_sAtCoordsFocusMP.focusUniqueID = tempUniqueID
	ENDIF
	
	// Output Details
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Focus Mission Reservation Confirmed [uniqueID = ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTSTRING("]")
		PRINTNL()
//		PRINTSTRING("         ")
//		INT assumedFocusMissionSlot = Get_Update_MissionsAtCoords_Slot_For_Band(MATCB_FOCUS_MISSION)
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(assumedFocusMissionSlot)
//		PRINTNL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Focus Mission has received confirmation of the player being on the mission, and update the Focus Mission variables
//
// RETURN VALUE:			BOOL			TRUE if confirmation of the player being on the mission has been received, FALSE if not received
//
// NOTES:	Assumes the Focus Variables are valid (this function should only be called from a valid Focus Mission update)
FUNC BOOL Check_For_Player_On_Focus_Mission_Confirmation()

	IF NOT (Is_Player_On_Mission_On_MP_Mission_Request(g_sAtCoordsFocusMP.focusUniqueID))
		// ...player not on mission yet
		RETURN FALSE
	ENDIF
	
	// Player is now actively on this mission
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Confirmed Actively On Focus Mission [uniqueID = ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTSTRING("]")
		PRINTNL()
//		PRINTSTRING("         ")
//		INT assumedFocusMissionSlot = Get_Update_MissionsAtCoords_Slot_For_Band(MATCB_FOCUS_MISSION)
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(assumedFocusMissionSlot)
//		PRINTNL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Focus Mission has received FAILED for the Mission Request
//
// RETURN VALUE:			BOOL			TRUE if the mission request FAILED has been received, FALSE if not received
//
// NOTES:	Assumes the Focus Variables are valid (this function should only be called from a valid Focus Mission update)
FUNC BOOL Check_For_Focus_Mission_Request_Failed()

	BOOL hasFailed = Has_MP_Mission_Request_Failed(g_sAtCoordsFocusMP.focusUniqueID)
	IF NOT (hasFailed)
		// ...mission hasn't broadcast 'FAILED'
		RETURN FALSE
	ENDIF
	
	// FAILED has been received
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Focus Mission Reservation FAILED [uniqueID = ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTSTRING("]")
		PRINTNL()
//		PRINTSTRING("         ")
//		INT focusMissionSlot = Get_MissionsAtCoords_Focus_Mission_Slot()
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(focusMissionSlot)
//		PRINTNL()
	#ENDIF
		
// KGM 7/7/13: Taking out the 'failed to launch mission' text - it only seems to trigger when it shouldn't
//	// Display a reason for why the request failed
//	// NOTE: This needs updated when the new error reporting functionality is implemented (see bug 754330)
//	#IF IS_DEBUG_BUILD
// KGM 18/1/13: Temporarily comment all this out until I can get the message correct
//		IF (Is_Player_Allocated_For_Any_Mission(PLAYER_ID()))
//			// Due to network lag, there is a possibility the mission the player is allocated for hasn't yet been broadcast, so display as much detail as is available
//			INT playerIndexAsInt = NATIVE_TO_INT(PLAYER_ID())
//			IF (GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpState = MP_PLAYER_STATE_NONE)
//				// ...details are not stored, so display a generic message
//				BEGIN_TEXT_COMMAND_PRINT("MC_GENALLOC")
//				END_TEXT_COMMAND_PRINT(DEFAULT_GOD_TEXT_TIME, TRUE)
//			ELSE
//				// ...details are stored, so display a specific message if the data is valid
//				INT missionRequestSlot = GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpSlot
//				MP_MISSION theMissionID = GlobalServerBD_MissionRequest.missionRequests[missionRequestSlot].mrMissionData.mdID.idMission
//				IF (theMissionID = eNULL_MISSION)
//					// ...data is not valid, so display a generic message
//					BEGIN_TEXT_COMMAND_PRINT("MC_GENALLOC")
//					END_TEXT_COMMAND_PRINT(DEFAULT_GOD_TEXT_TIME, TRUE)
//				ELSE
//					// ...data is valid, so display a specific message
//					BEGIN_TEXT_COMMAND_PRINT("MC_MISSALLOC")
//						ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(GET_MP_MISSION_NAME(theMissionID))
//						ADD_TEXT_COMPONENT_INTEGER(GlobalServerBD_MissionRequest.missionRequests[missionRequestSlot].mrMissionData.iInstanceId)
//					END_TEXT_COMMAND_PRINT(DEFAULT_GOD_TEXT_TIME, TRUE)
//				ENDIF
//			ENDIF
//		ENDIF
//		
//// KGM 18/1/13: Temporary very generic replacement message
//		BEGIN_TEXT_COMMAND_PRINT("MC_NOLAUNCH")
//		END_TEXT_COMMAND_PRINT(DEFAULT_GOD_TEXT_TIME, TRUE)
//	#ENDIF
	
	// FAILURE has been received
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Focus Mission has received MISSION FULL for the Mission Request
//
// RETURN VALUE:			BOOL			TRUE if the mission request FAILED has been received, FALSE if not received
//
// NOTES:	Assumes the Focus Variables are valid (this function should only be called from a valid Focus Mission update)
FUNC BOOL Check_For_Focus_Mission_Full()

	BOOL missionFull = Has_MP_Mission_Request_Just_Broadcast_Mission_Full(g_sAtCoordsFocusMP.focusUniqueID)
	IF NOT (missionFull)
		// ...mission hasn't broadcast 'mission full'
		RETURN FALSE
	ENDIF
	
	// MISSION FULL has been received
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Focus Mission FULL [uniqueID = ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTSTRING("]")
		PRINTNL()
//		PRINTSTRING("         ")
//		INT assumedFocusMissionSlot = Get_Update_MissionsAtCoords_Slot_For_Band(MATCB_FOCUS_MISSION)
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(assumedFocusMissionSlot)
//		PRINTNL()
	#ENDIF
	
	// MISSION FULL has been received
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Focus Mission has FINISHED
//
// RETURN VALUE:			BOOL			TRUE if the mission FINISHED has been received, FALSE if not received
//
// NOTES:	Assumes the Focus Variables are valid (this function should only be called from a valid Focus Mission update)
FUNC BOOL Check_For_Focus_Mission_Finished()

	BOOL isFinished = Has_MP_Mission_Request_Finished(g_sAtCoordsFocusMP.focusUniqueID)
	IF NOT (isFinished)
		// ...mission FINISHED has not been received
		RETURN FALSE
	ENDIF
	
	// ...mission has finished
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Focus Mission FINISHED [uniqueID = ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTSTRING("]")
		PRINTNL()
//		PRINTSTRING("         ")
//		INT assumedFocusMissionSlot = Get_Update_MissionsAtCoords_Slot_For_Band(MATCB_FOCUS_MISSION)
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(assumedFocusMissionSlot)
//		PRINTNL()
	#ENDIF
	
	// FINISHED has been receive
	RETURN TRUE
			
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To check if the focus mission details are still the same on the Mission Controller, and to update MissionsAtCorods details if different
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//
// NOTES:	The details can change for FM Heists where the player gets to choose the mission from a list once a placeholder mission has been setup and reserved on the Mission Controller
//			MAY NEED TO CHANGE THIS SO IT ONLY PROCESSES IF THE MISSION IS TAGGED AS 'CHANGEABLE'
//			NOTE: Any change will have been instigated by the External InCorona routines, so assume they've handled their own data and just update the MissionAtCorods data here
PROC Maintain_Focus_Mission_Details(INT paramSlot)

	INT missionControllerSlot = Find_Slot_Containing_This_MP_Mission(g_sAtCoordsFocusMP.focusUniqueID)
	
	IF (missionControllerSlot = FAILED_TO_FIND_MISSION)
		EXIT
	ENDIF
	
	// Check the mission details in this mission slot are the same as the MissionsAtCoords known details and update if not
	// NOTE: this will be checking the server broadcast data, so it may be slightly out of date if this player isn't the server, but it shouldn't matter
	MP_MISSION_ID_DATA	mcMissionIdData		= Get_MP_Mission_Request_MissionID_Data(missionControllerSlot)
	MP_MISSION_ID_DATA	localMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	BOOL				usesCloudData		= Does_MP_Mission_Variation_Get_Data_From_The_Cloud(localMissionIdData.idMission, localMissionIdData.idVariation)
	
	IF (usesCloudData)
		// ...this mission uses cloud data, so don't check for variation changes because they are machine-specific and could be different on server from client
		IF	(localMissionIdData.idMission			= mcMissionIdData.idMission)
		AND	(localMissionIdData.idCreator			= mcMissionIdData.idCreator)
		AND	(localMissionIdData.idSharedRegID		= mcMissionIdData.idSharedRegID)
		AND (ARE_STRINGS_EQUAL(localMissionIdData.idCloudFilename, mcMissionIdData.idCloudFilename))
			// ...no changes
			EXIT
		ENDIF
		
		// BUG 1911260: If MissionAtCoords has 'real' data, don't swap it for 'generated' data passed from Mission Controller, just update the Shared RegID if appropriate
		IF (localMissionIdData.idVariation != GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
		AND (mcMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
			// KGM 2/9/14 [BUG: 2004116] - Problems when triggering LTS UGC Rounds
			// Although this data is 'real', the mission controller is dealing with 'generated' data and passing GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION around the session.
			//		To prevent various error outputs and potential inconsistencies, copy the 'real' data into the 'generated' data so that any requests on this machine that
			//		uses the GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION from the mission controller will still access the correct data.
			//		DON'T UPDATE THE MATC VARIABLES THOUGH - They should continue to access the real data for local requests.
			#IF IS_DEBUG_BUILD
				IF NOT (IS_STRING_NULL_OR_EMPTY(g_sGeneratedCloudHeaderData.gchdHeaderData.tlName))
					IF (ARE_STRINGS_EQUAL(g_sGeneratedCloudHeaderData.gchdHeaderData.tlName, localMissionIdData.idCloudFilename))
						PRINTLN(".KGM [At Coords]: Focus Mission SharedRegID on Mission Controller has changed. My data is local. Other data is Generated. Storing my local data in my generated variables.")
					ENDIF
				ENDIF
			#ENDIF
			
			Generate_Cloud_Header_Data_Using_Local_Data(localMissionIdData)
		
			// Only update if the Shared RegID has changed, all the other data should stay the same
			IF (localMissionIdData.idSharedRegID = mcMissionIdData.idSharedRegID)
				EXIT
			ENDIF
			
			// The local data is not generated, but the mission Controller data IS generated, and the sharedRegID has changed, so just update the SharedRegID, but allow debug output
			// ...easiest way is to copy the local data over the updated data except for the shared RegID, then let it pass through the rest of this function
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Focus Mission SharedRegID on Mission Controller has changed. IGNORING OTHER DATA because new data is 'generated' while local data is 'real'.")
				PRINTNL()
			#ENDIF
			
			// ...store the update Shared RegID
			INT holdSharedRegID = mcMissionIdData.idSharedRegID
			
			// Copy the local data over the local variable data containing the MC struct
			mcMissionIdData = g_sAtCoordsMP[paramSlot].matcMissionIdData
			
			// ...restore the Shared RegID then allow this to be passed through the rest of the function
			mcMissionIdData.idSharedRegID = holdSharedRegID
		ENDIF
	ELSE
		// ...this mission uses hardcoded data, so just need to check mission and variation
		IF	(localMissionIdData.idMission			= mcMissionIdData.idMission)
		AND	(localMissionIdData.idVariation			= mcMissionIdData.idVariation)
			// ...no changes
			EXIT
		ENDIF
	ENDIF
	
	// There have been changes
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Focus Mission on Mission Controller has changed. UniqueID: ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTNL()
		PRINTSTRING("          MissionID  : ")
		PRINTSTRING(GET_MP_MISSION_NAME(mcMissionIdData.idMission))
		PRINTSTRING(". From: ")
		PRINTSTRING(GET_MP_MISSION_NAME(localMissionIdData.idMission))
		PRINTNL()
		PRINTSTRING("          Variation  : ")
		PRINTINT(mcMissionIdData.idVariation)
		PRINTSTRING(". From: ")
		PRINTINT(localMissionIdData.idVariation)
		PRINTNL()
		PRINTSTRING("          CreatorID  : ")
		PRINTINT(mcMissionIdData.idCreator)
		PRINTSTRING(". From: ")
		PRINTINT(localMissionIdData.idCreator)
		PRINTNL()
		PRINTSTRING("          Filename   : ")
		PRINTSTRING(mcMissionIdData.idCloudFilename)
		PRINTSTRING(". From: ")
		PRINTSTRING(localMissionIdData.idCloudFilename)
		PRINTNL()
		PRINTSTRING("          SharedRegID: ")
		PRINTINT(mcMissionIdData.idSharedRegID)
		PRINTSTRING(". From: ")
		PRINTINT(localMissionIdData.idSharedRegID)
		PRINTNL()
	#ENDIF
	
	// BUG 1705950: Addition. My previous fix sorted the data, but if the joining player hit the timing issue and then walked out of the corona the corona was removed (since it was
	//				never setup as Shared Mission data, just as Generated Data). This was fine unless the same-session host then invited the player into the mission whereby there
	//				was no corona for the invited player to be in. This change fixes that by checking if a Generated Mission is being changed into Shared Mission data (by the Shared
	//				Mission server updating the data held on the Mission Controller), and then removing the 'delete corona on walk out' option (this shouldn't apply to Shared Mission
	//				data) and adding that it is 'Share Mission' data so that it cleans up if the host walks out of the corona. The only issue I've found is that the mission blip
	//				displays as white instead of yellow, but this is a minor detail.
	IF (usesCloudData)
		IF (localMissionIdData.idSharedRegID = ILLEGAL_SHARED_REG_ID)
		AND (mcMissionIdData.idSharedRegID != ILLEGAL_SHARED_REG_ID)
			IF (localMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
			AND (mcMissionIdData.idVariation != GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
				PRINTSTRING(".KGM [At Coords]: Generated Mission replaced by Shared Mission")
				CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_DELETE_AFTER_FOCUS)
				SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_ADDED_AS_SHARED_MISSION)
			ENDIF
		ENDIF
	ENDIF
	
	// Update the data (just update all of it - for cloud-loaded missions, if the mission has changed then the variation data will be different too)
	g_sAtCoordsMP[paramSlot].matcMissionIdData	= mcMissionIdData
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Cancel the Mission Reservation
// NOTES:	Assumes the player is reserved for the mission
//			Don't wait for any response, just assume the player is off the mission now and move on
//				- it may be too late and the player will get dragged on somehow but this should get handled elsewhere
PROC Cancel_Focus_Mission_Reservation()

	Broadcast_Cancel_Mission_Reservation_By_Player(g_sAtCoordsFocusMP.focusUniqueID)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Focus Mission Reservation - Cancel. UniqueID = ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTNL()
//		PRINTSTRING("         ")
//		INT assumedFocusMissionSlot = Get_Update_MissionsAtCoords_Slot_For_Band(MATCB_FOCUS_MISSION)
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(assumedFocusMissionSlot)
//		PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Cancel the Mission Reservation and clear the Focus Variables
// NOTES:	Assumes the player is reserved for the mission
//			Don't wait for any response, just assume the player is off the mission now and move on
//				- it may be too late and the player will get dragged on somehow but this should get handled elsewhere
PROC Cancel_Focus_Mission_Reservation_And_Clear_Focus_Variables()

	// Cancel the Mission Reservation
	Cancel_Focus_Mission_Reservation()
	
	// Clear out the Additional Focus Mission control variables
	Clear_MP_Mission_At_Coords_Focus_Mission_Variables()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if DPAD-RIGHT is required before the mission can be launched
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if DPAD-RIGHT is required to launch the mission, otherwise FALSE
//
// NOTES:	KGM 30/6/15 [BUG 2358978]: All coronas require DPAD-RIGHT to trigger them
FUNC BOOL Is_DPadRight_Required_To_Launch_Mission(INT paramSlot)


	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_CoronasDpadRightNotNeeded"))
			PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - NOT REQUIRED because of -sc_CoronasDpadRightNotNeeded commandline")
			RETURN FALSE
		ENDIF
	#ENDIF
	
	// Has the DPADRIGHT functionality been disabled through tunable?
	IF (g_sMPTunables.bdpadright_corona_disable)
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - NOT REQUIRED because disabled by g_sMPTunables.bdpadright_corona_disable")
		RETURN FALSE
	ENDIF

	// If Invite Accepted, the player has already indicated a need to join the mission so DPAD-RIGHT not needed (ie: from a JIP or accepting an invite, etc)
	IF (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - NOT REQUIRED because Invite Accepted")
		RETURN FALSE
	ENDIF

	// If in an Activity Session then not in open Freemode, so don't need DPAD-RIGHT
	IF (NETWORK_IS_ACTIVITY_SESSION())
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - NOT REQUIRED because NETWORK_IS_ACTIVITY_SESSION")
		RETURN FALSE
	ENDIF
	
	// Playlists shouldn't require DPAD-RIGHT
	IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - NOT REQUIRED because IS_PLAYER_ON_A_PLAYLIST")
		RETURN FALSE
	ENDIF
	
	// Gang Attacks and Contact Missions shoudn't required DPAD-RIGHT
	IF (Should_MissionsAtCoords_Mission_Quick_Launch(paramSlot))
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - NOT REQUIRED because Should_MissionsAtCoords_Mission_Quick_Launch (ie: Gang Attack)")
		RETURN FALSE
	ENDIF
	
	// Heist-Related missions shouldn't require DPAD-RIGHT
	MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	IF (Is_FM_Cloud_Loaded_Activity_A_Heist(theMissionIdData))
	OR (Is_FM_Cloud_Loaded_Activity_Heist_Planning(theMissionIdData))
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - NOT REQUIRED because mission is Heist-Related")
		RETURN FALSE
	ENDIF
	
	//Flow related, so ignore
	IF FM_FLOW_DISABLE_DPAD_RIGHT_AND_WALK_IN_FOR_CORONA()
		PRINTLN(".KGM [At Coords][DPadRight]: Is_FM_Cloud_Loaded_Activity_A_Flow_Mission() - NOT REQUIRED because FM_FLOW_DISABLE_DPAD_RIGHT_AND_WALK_IN_FOR_CORONA")
		RETURN FALSE
	ENDIF
	
	//Don't need it if it's a transition session invite.
	IF IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - NOT REQUIRED because IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED")
		RETURN FALSE
	ENDIF
	
	//get out if we're quick matching and we've not warped. 
	IF (AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH())
	AND ARE_VECTORS_ALMOST_EQUAL(g_vInitialSpawnLocation, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - NOT REQUIRED because AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH")
		RETURN TRUE
	ENDIF
	
	// Tutorial missions shouldn't need DPAD-RIGHT
	IF (Get_Rank_For_FM_Cloud_Loaded_Activity(theMissionIdData) = 9999)
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - NOT REQUIRED because mission is a Tutorial")
		RETURN FALSE
	ENDIF
	
	// Specifically check for the LTS Tutorial - which won't need DPAD-RIGHT
	IF (g_isOnOrTriggeringTutorialLTS)
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - NOT REQUIRED because g_isOnOrTriggeringTutorialLTS = TRUE")
		RETURN FALSE
	ENDIF
	
	// Block corona if accepting invite to apartment.
	IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - REQUIRE but aborting due to apartment invite or entering a property.")
		RETURN TRUE
	ENDIF
	
	
	//If we're starting a quick match and we've got a content ID, and it's been set up, and this is not the mission then bomb out
	IF AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH()
	AND TRANSITION_SESSIONS_PICKED_SPECIFIC_JOB()
		TEXT_LABEL_23 tlTemp = Get_MissionsAtCoords_Slot_ContentID(paramSlot)
		IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.stFileName)
		AND NOT ARE_STRINGS_EQUAL(g_sTransitionSessionData.stFileName, tlTemp)
			PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - ARE_STRINGS_EQUAL")
			RETURN TRUE
		ENDIF
	ENDIF
	
	
	//If we're starting on call, and it's been set up, and this is not the mission then bomb out
	IF HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP()
	AND AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
		TEXT_LABEL_23 tlTemp = Get_MissionsAtCoords_Slot_ContentID(paramSlot)
		IF NOT ARE_STRINGS_EQUAL(g_sTransitionSessionData.stFileName, tlTemp)
			PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - ARE_STRINGS_EQUAL")
			RETURN TRUE
		ENDIF
	ENDIF

	IF IS_WARNING_MESSAGE_ACTIVE()
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - IS_WARNING_MESSAGE_ACTIVE")
		RETURN TRUE
	ENDIF	
	
	// Check for DPADRIGHT
	IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT))
		PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - DPAD-RIGHT has been pressed")
		RETURN FALSE
	ENDIF
	
	
	// DPAD RIGHT still required, so display help if required
	IF NOT (IS_HUD_HIDDEN())
	AND NOT (IS_RADAR_HIDDEN())
		IF NOT (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MATC_DPADRIGHT"))
			PRINTLN(".KGM [At Coords][DPadRight]: Is_DPadRight_Required_To_Launch_Mission() - Display Help: Press 'DpadRight'")
			PRINT_HELP("MATC_DPADRIGHT", 30000)
			g_sV2CoronaVars.g_isplayer_inside_corona = TRUE
		ENDIF
		
		// KGM 14/7/15 [BUG 2418849]: To prevent dpadright message getting cleared by another nearby mission, add a refresh every frame it should be on display.
		//	The refresh will ensure it stays active for a couple of frames and will be auto-cleared if the refresh times out (because nothing is keeping it active).
		g_matcDpadRightHelpFrameTimeout = GET_FRAME_COUNT() + DPADRIGHT_HELP_REMAIN_ACTIVE_FRAMES
	ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Try to become the Focus Mission and start external InCorona actions
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
FUNC BOOL Try_To_Become_MissionsAtCoords_Focus_Mission(INT paramSlot, FLOAT paramDistance)
	
	// Don't reserve the mission if it is an overlapped mission and the player isn't accepting an invite into it
	IF (Is_MissionsAtCoords_Mission_Hidden_By_Overlapping_Corona(paramSlot))
	AND NOT (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
		// ...this is the player walking in to an overlapped mission, so don't make this the focus mission
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: BLOCK FOCUS MISSION: PLAYER IS WALKING IN TO AN OVERLAPPED MISSION.") PRINTNL()
//		#ENDIF
		RETURN FALSE
	ENDIF
	
	// KGM 12/5/15: BUG 2313555 - Don't reserve the mission if hidden by PI Menu option and the player isn't accepting an invite into it
	IF (Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu(paramSlot))
	AND NOT (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
		// ...this is the player walking in to a hidden mission, so don't make this the focus mission
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: BLOCK FOCUS MISSION: PLAYER IS WALKING IN TO A JOB TYPE HIDDEN BY PI MENU.") PRINTNL()
//		#ENDIF
		RETURN FALSE
	ENDIF
	
	// KGM 11/6/15: BUG 2316703 - Don't reserve the mission if within a Disabled Area and the player isn't accepting an invite into it
	IF (Is_MissionsAtCoords_Mission_Hidden_By_Disabled_Area(paramSlot))
	AND NOT (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
		// ...this is the player walking in to a hidden mission, so don't make this the focus mission
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: BLOCK FOCUS MISSION: PLAYER IS WALKING IN TO A JOB TYPE HIDDEN BY PI MENU.") PRINTNL()
//		#ENDIF
		RETURN FALSE
	ENDIF
	
	// KGM 8/11/14 [BUG 2111421]: Don't reserve the mission if the player has a wanted level and hasn't been invited to it.
	//				NOTE: Previously it used to enter the corona and then get thrown out with an error message.
	IF (Does_MatC_Player_Have_Wanted_Level())
	AND NOT (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
		// ...this is the player walking in to a corona with a wanted level, so don't make this the focus mission
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: BLOCK FOCUS MISSION: PLAYER IS WALKING IN TO A CORONA WITH A WANTED LEVEL.") PRINTNL()
//		#ENDIF
		RETURN FALSE
	ENDIF
	
	// KGM 21/2/15 [BUG 2238981]: Block this as a Focus Mission if the player has very recently paid on the JobList to lose the wanted level.
	//		This is to break a deadlock situation with a player in another player's apartment walking into a Heist Corona with a Wanted Level (blocking the corona)
	//		and then accepting an invite into the corona from the Leader and paying to lose the Wanted Level allowing a Focus Mission while the Joblist Reservationis being actioned.
	IF (Has_MatC_Player_Recently_Paid_To_Remove_Wanted_Level())
	AND NOT (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
		// ...this is the player walking in to a corona with a wanted level, so don't make this the focus mission
		PRINTLN(".KGM [At Coords]: SHORT DELAY FOCUS MISSION: PLAYER IS WALKING IN TO A CORONA WHEN THEY RECENTLY PAID THE JOBLIST TO REMOVE A WANTED LEVEL.")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		// ...this is the player walking in to a corona with a wanted level, so don't make this the focus mission
		PRINTLN(".KGM [At Coords]: SHORT DELAY FOCUS MISSION: IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR")
		RETURN FALSE
	ENDIF
	
	IF g_BG_Block_MatcLobby
		// ...this is if the BG script is blocking coronas
		PRINTLN(".KGM [At Coords]: SHORT DELAY FOCUS MISSION: g_BG_Block_MatcLobby")
		RETURN FALSE
	ENDIF
	
	// KGM 24/6/15 [BUG 2365221]: Don't reserve this mission if it is a Rounds Mission and the contentID Hashes don't match
	IF (IS_THIS_A_ROUNDS_MISSION())
		IF (g_roundsMissionHashCID != 0)
			TEXT_LABEL_23 thisCID = Get_MissionsAtCoords_Slot_ContentID(paramSlot)
			INT thisHashCID = GET_HASH_KEY(thisCID)
			IF (g_roundsMissionHashCID != thisHashCID)
				// This is a rounds mission and the stored details do not match this mission, so don't join this mission
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [At Coords][Rounds]: BLOCK FOCUS MISSION: This is a Rounds Mission, but the Stored ContentID Hash doesn't match. Stored: ", g_roundsMissionHashCID, ".  This: ", thisHashCID)
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
		PRINTLN(".KGM [At Coords]: SHORT DELAY FOCUS MISSION: IS_PLAYER_USING_RCBANDITO")
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF IS_PLAYER_USING_RC_TANK(PLAYER_ID())
		PRINTLN(".KGM [At Coords]: SHORT DELAY FOCUS MISSION: IS_PLAYER_USING_RC_TANK")
		RETURN FALSE
	ENDIF
	#ENDIF
	
	#IF FEATURE_DLC_1_2022
		IF IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
			PRINTLN(".KGM [At Coords]: SHORT DELAY FOCUS MISSION: IS_PLAYER_TEST_DRIVING_A_VEHICLE ")
			RETURN FALSE
		ENDIF
	#ENDIF // #IF FEATURE_DLC_1_2022
	
	// Only allow this mission to become the Focus Mission if there isn't already a Focus Mission
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		// ...there is already a Focus Mission, so do nothing more with this one at the moment
		RETURN FALSE
	ENDIF
	
	// Delay reserving the mission if there are any external reasons to prevent this
	IF (Check_For_External_Reasons_To_Delay_Becoming_Focus_Mission(paramSlot))
		RETURN FALSE
	ENDIF
	
	// Delay if this is an activity session (so a mission has transitioned past the invite screen) and this isn't the loaded contentID
	// Assume for now a NULL contentID (transition or game) is for a minigame and let processing through (NOTE: it's possible the transition contentID still retains old data)
	// Ignore for now if the player is on a playlist - this is dealt with elsewhere
	IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
		IF NETWORK_IS_ACTIVITY_SESSION()
			IF IM_ABOUT_TO_JIP_PLAYLIST_MISSION()
			OR IS_THIS_PLAYER_ON_JIP_WARNING(PLAYER_ID())
			OR IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA()
				PRINTLN(".KGM [At Coords]: Become Focus Mission? [TS] IM_ABOUT_TO_JIP_PLAYLIST_MISSION = TRUE")
				RETURN FALSE
			ENDIF
			IF IS_PLAYER_SCTV(PLAYER_ID())
				IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
				AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_RUN_PLAY_LIST
					PRINTLN(".KGM [At Coords]: Become Focus Mission? IS_PLAYER_SCTV - IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF (NETWORK_IS_ACTIVITY_SESSION())
			
			IF NOT (Get_MissionsAtCoords_Slot_CreatorID(paramSlot) = FMMC_MINI_GAME_CREATOR_ID)
				TEXT_LABEL_23 cloudFilenameFromTransition	= GET_TRANSITION_SESSION_LOADED_FILE_NAME()
				TEXT_LABEL_23 missionFilename				= Get_MissionsAtCoords_Slot_ContentID(paramSlot)
				IF NOT (IS_STRING_NULL_OR_EMPTY(cloudFilenameFromTransition))
					IF NOT (ARE_STRINGS_EQUAL(missionFilename, cloudFilenameFromTransition))
						PRINTLN(".KGM [At Coords]: Become Focus Mission? IF NOT (ARE_STRINGS_EQUAL(missionFilename, cloudFilenameFromTransition))")
						RETURN FALSE
					ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_INITILISE_MISSION
						PRINTLN(".KGM [At Coords]: Become Focus Mission? GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_INITILISE_MISSION")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Become Focus Mission? Slot: ")
		PRINTINT(paramSlot)
		PRINTSTRING(". ")
		TEXT_LABEL_23 slotContentID = Get_MissionsAtCoords_Slot_ContentID(paramSlot)
		PRINTSTRING(slotContentID)
		PRINTNL()
	#ENDIF
	
	// Delay reserving mission if coronas currently shouldn't function
	BOOL outputReasonForFailure = TRUE
	IF (Should_MissionsAtCoords_Coronas_Be_Functionally_Blocked(paramSlot, outputReasonForFailure))
		RETURN FALSE
	ENDIF
	
	// KGM 13/10/14: Delay reserving mission if there is an exclusive contentID that doesn't belong to this mission
	// BUG 2066538: This is to allow a Heist contetnID to become temporarily exclusive even if the Matc registration of the corona hasn't yet happened
	IF (Is_There_An_Exclusive_ContentID())
		IF NOT (Does_MissionsAtCoords_Slot_Contain_Exclusive_ContentID(paramSlot))
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [At Coords]: Become Focus Mission? Delay - There is a temporary exclusive contentID Hash that isn't for this mission: ", g_sMatcExclusiveContentID.matcecContentIdHash)
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		// The exclusive contentID must be for this mission, so clean it up
		PRINTLN(".KGM [At Coords]: This mission matches the exclusive contentID Hash: ", g_sMatcExclusiveContentID.matcecContentIdHash)
		Clear_MissionsAtCoords_Exclusive_ContentID()
	ENDIF
	
	// KGM 25/10/13: Delay reserving mission if there is an invite accepted contentID that doesn't belong to this mission
	//				 (This is to identify the mission being joined if there are overlapping missions and another has visual priority)
	IF (Is_There_A_Most_Recent_Invite_Accepted_ContentID())
		IF NOT (Does_MissionsAtCoords_Slot_Contain_Most_Recent_Invite_Accepted_ContentID(paramSlot))
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Become Focus Mission? Delay - Another Mission is Invite Accepted: ")
				PRINTSTRING(g_sAtCoordsControlsMP_AddTU.matccDebugInviteAcceptContentID)
//				PRINTSTRING(". HASH: ")
//				PRINTINT(g_sAtCoordsControlsMP_AddTU.matccInviteAcceptContentIdHash)
				PRINTNL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// At this stage, ensure the mission has a speed zone and scenario blocking get set up
	BOOL mustSucceed = TRUE
	Setup_MissionsAtCoords_SpeedZone(paramSlot, mustSucceed)
	Setup_MissionsAtCoords_ScenarioBlocking(paramSlot, mustSucceed)
	
	// Delay becoming the Focus Mission if the player is moving fast on foot, the player may be running through the corona
	// KGM 19/5/13: Ignore for Area-Triggered Missions
	// KGM 9/6/13: Ignore for Immediate Launch missions
	IF NOT (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
	AND NOT (Should_MissionsAtCoords_Mission_Launch_Immediately(paramSlot))
		IF (Is_MatC_Player_Moving_Fast_On_Foot())
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Become Focus Mission? Delay - Moving fast on foot.") PRINTNL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
		
	// Area-Triggered Missions need a couple of special checks
	IF (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
		// Delay if this is an area triggered mission and the radius blip is not on display
		// (This is to provide feedback if the player happens to be in the area to launch the mission before the radius blip gets setup)
		IF (g_sMatcRadiusBlip.mrbRegID != Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Become Focus Mission? Delay - Radius Blip not yet displayed for Area Triggered Mission")
				PRINTNL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		// Delay if the player is performing a Quick Match transition (A Gang Attack is never the subject of a quick match transition)
		IF (AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH())
		OR AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
		OR SHOULD_TRANSITION_SESSION_QUICK_MATCH_RANDOM_FROM_NJVS()
		OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
		OR (SHOULD_GANG_ATTACKS_BE_BLOCKED_FOR_BOSS_WORK())
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Become Focus Mission? Delay - Quick Match Transition in progress")
				PRINTNL()
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Ensure there is room on the Leader's mission for a non-Leader player being sucked into a Heist Mission in the Leader's apartment
	IF NOT (Is_There_Room_On_Leader_Heist_For_Player_To_Walk_In(paramSlot))
		// ...there is no room on the mission for a player to walk-in
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Become Focus Mission? Delay - There is no room on Leader's Heist mission for a Walk-In player")
			PRINTNL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Keith 30/6/15 [BUG 2358798]: Handle DPADRIGHT checking
	IF (Is_DPadRight_Required_To_Launch_Mission(paramSlot))
		// ...DPAD-RIGHT is still required
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [At Coords][DPadRight]: Become Focus Mission? Delay - DPAD-RIGHT is still required to allow this mission to trigger")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// Clear 'Press DPADRIGHT' help text if appropriate
	IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MATC_DPADRIGHT"))
		PRINTLN(".KGM [At Coords][DPadRight]: Become Focus Mission? Clear 'Press DPAD-RIGHT' help message")
		CLEAR_HELP()
	ENDIF
	
	// Clear the additional variables required for the Focus Mission
	Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
	
	//If I'm on call and we're not in a transition session then we need to clean it up
	IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()	
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
		SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()	
		//If I'm on a playlist then clen it up
		IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		//And it's a playlist that I'm starting
		AND GET_TRANSITION_SESSIONS_QUICK_MATCH_TYPE() = FM_GAME_MODE_PLAYLIST
			SET_PLAYER_ON_A_PLAYLIST(TRUE)
		ENDIF
	ENDIF
	
	// Check for a Quick Launch and use the Quick Launch sequence instead (ie: Gang Attacks)
	IF (Should_MissionsAtCoords_Mission_Quick_Launch(paramSlot))
		// Prepare for downloading the cloud mission data
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Quick Launch required - download mission data")
			PRINTNL()
		#ENDIF
		
		// Prepare to download the mission
		Clear_Cloud_Loaded_Mission_Data()
		
		// Change the mission's stage to be 'download for quick launch'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_DOWNLOAD_FOR_QUICK_LAUNCH)
		PRINTSTRING("          Moving Closer: Distance from Mission Coords: ") PRINTFLOAT(paramDistance) PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// KGM 28/7/13 (bug - 1532789): When using Play Again or Random, the player should be allowed to play even if they can't afford it.
	// I need to set a flag that gets maintained while the player has a focus mission so that all cash checks allow play.
	IF (HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM())
		MP_MISSION			theMissionID		= Get_MissionsAtCoords_Slot_MissionID(paramSlot)
		MP_MISSION_ID_DATA	theMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
		INT					theSubtype			= Get_SubType_For_FM_Cloud_Loaded_Activity(theMissionIdData)
		
		IF NOT (Check_If_Afford_Mission(theMissionID, theSubtype))
			// ...player can't afford mission during 'play again/random' so need to force the game to allow a poor player to play it
			g_allowPoorPlayerToPlayAgainForFree = TRUE
			
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: 'Free To Play' is TRUE") PRINTNL()
			#ENDIF
		ENDIF
	ENDIF
	
	// Disable entering vehicles while in the corona but before the corona takes control
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	
	// Get rid of the phone
	HANG_UP_AND_PUT_AWAY_PHONE()
	
	// Set up any in-corona actions (including the voting, if needed)
	Setup_InCorona_Actions(paramSlot)
	
	// Re-use the 'settle timeout' timer while waiting for the corona to pick up the player - this should be safe, the other use is at a later stage
	Set_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout(INCORONA_ACTIONS_CORONA_PICKUP_SETTLE_DELAY_msec)
	
	// Activate Visual Corona Controls for Focus Missions
	Setup_Focus_Mission_Corona_Visuals(paramSlot)

	// With the mission being reserved, clear the Locate Message bits
	// (if the mission name needs displayed for a while longer the bitfield will be stored in the corona visuals struct: Setup_Focus_Mission_Corona_Visuals())
	Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
	
	// Set the player as Invincible while in the corona
	SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
			
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Set INVINCIBLE for Focus Mission") PRINTNL()
	#ENDIF
	
	// Change the mission's stage to be 'waiting for allow reservation'
	Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_ALLOW_RESERVATION)
	PRINTSTRING("          Moving Closer: Distance from Mission Coords: ") PRINTFLOAT(paramDistance) PRINTNL()
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check the GroundZ for this mission's coords to ensure they are within reasonable tolerance of the GroundZ
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
PROC Perform_MissionsAtCoords_Mission_GroundZ_Check(INT paramSlot)

	CONST_FLOAT		GROUND_DIFF_OK_TOLERANCE_m		0.3
	CONST_FLOAT		GROUND_DIFF_NO_COLLISION_m		-3.0

	VECTOR	theCoords	= Get_MissionsAtCoords_Slot_Coords(paramSlot)
	FLOAT	coronaZ		= theCoords.z
	FLOAT	groundZ		= 0.0
	
	theCoords.z += 2.0
	
	IF NOT (GET_GROUND_Z_FOR_3D_COORD(theCoords, groundZ))
		EXIT
	ENDIF
	
	// Check the difference between corona Z and ground Z
	FLOAT diffZ = coronaZ - groundZ
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Checking GroundZ. Slot: ")
		PRINTINT(paramSlot)
		PRINTSTRING(". CoronaZ: ")
		PRINTFLOAT(coronaZ)
		PRINTSTRING(". GroundZ: ")
		PRINTFLOAT(groundZ)
		PRINTSTRING(". Diff: ")
		PRINTFLOAT(diffZ)
	#ENDIF
	
	// Is there an OK difference between the corona Z and the ground Z - if yes, adjust the coronaZ and get out
	// KGM 9/1/15 [BUG 2188306]: No longer adjust coronaZ, the adjustments are minor but it means vehicle generator blocking areas can be minutely different causing duplicates
	IF (diffZ < GROUND_DIFF_OK_TOLERANCE_m)
	AND (diffZ > -GROUND_DIFF_OK_TOLERANCE_m)
		// ...ok difference - adjust corona coords
		#IF IS_DEBUG_BUILD
			PRINTSTRING(" - WITHIN TOLERANCE") PRINTNL()
		#ENDIF
		
		// KGM 9/1/15: don't update corona to groundZ, the differences are minute
		//g_sAtCoordsMP[paramSlot].matcCoords.Z = groundZ
		EXIT
	ENDIF
	
	// Is the diff is much less that the 'no collision' tolerance the the groundZ is far below the coronaZ which probably means the ground collision hasn't loaded, so leave coronaZ as-is for now
	IF (diffZ < GROUND_DIFF_NO_COLLISION_m)
		// ...groundZ is far below coronaZ - assume ground collision hasn't loaded and ignore for now
		#IF IS_DEBUG_BUILD
			PRINTSTRING(" - groundZ far below coronaZ (ignore - assume collision not loaded)") PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// There is a large difference between the coronaZ and the groundZ but it doesn't seem to indicate collision hasn't loaded
	// Assert, but leave coronaZ as-is
	#IF IS_DEBUG_BUILD
//		PRINTSTRING(" - large difference between groundZ and coronaZ - assume corona placement error")
//		PRINTNL()
//		PRINTSTRING("         ")
//		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//		PRINTNL()
		
		// Changing to a PRINT instead of an ASSERT - the creator has other controls in place to ensure this isn't an issue and there has never been a genuine reason for the assert
		// NOTE: Usually it is caused by slow-loading geometry so the 'ground' hasn't loaded in, and so usually a waive
		PRINTLN(".KGM [At Coords]: Perform_MissionsAtCoords_Mission_GroundZ_Check() - large difference between groundZ and coronaZ - assume a delay in loading geometry. IGNORE.")
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Calculate the player distance from the corona
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
// RETURN VALUE:			FLOAT				The distance from the corona
//
// NOTES:	If the player and the corona are at roughly the same z then calcualte distance based purely on x/y
FUNC FLOAT Calculate_Player_Distance_From_Corona(INT paramSlot)

	VECTOR	myCoords		= GET_ENTITY_COORDS(GET_PLAYER_PED(PLAYER_ID()), FALSE)
	VECTOR	coronaCoords	= g_sAtCoordsMP[paramSlot].matcCoords
	FLOAT	heightDiff		= 0.0
	
	// The player's z is in the torso, but use the player's feet position
	myCoords.z -= 1.0
	
	// If the corona and the player Z are very different then calculate distance based on x/y/z as normal - the player is too far away anyway
	heightDiff = myCoords.z - coronaCoords.z
	IF (heightDiff < CORONA_HEIGHT_LEEWAY_BELOW_m)
	OR (heightDiff > CORONA_HEIGHT_LEEWAY_ABOVE_m)
		// ...height difference is too much, so calculate distance using x/y/z
		RETURN (GET_DISTANCE_BETWEEN_COORDS(myCoords, coronaCoords))
	ENDIF
	
	// Height difference is within range, so calculate distance based on x/y only
	myCoords.z		= 0.0
	coronaCoords.z	= 0.0

	RETURN (GET_DISTANCE_BETWEEN_COORDS(myCoords, coronaCoords))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Calls a bunch of standard cleanup calls when the player is no longer actively in a corona
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
PROC Cleanup_MissionsAtCoords_After_Quit_Corona(INT paramSlot)

	// Unregister this player from this shared mission data (if applicable)
	Cleanup_Shared_Missions_Data_For_Slot(paramSlot)
		
	// Delete this mission if it should be deleted after being the Focus mission
	IF (Should_MissionsAtCoords_Mission_Be_Deleted_After_Focus(paramSlot))
		Mark_MissionsAtCoords_Mission_For_Delete(paramSlot)
	ENDIF
		
	// Clear the flag if it was the currently active playlist activity
	Clear_MissionsAtCoords_Currently_Active_Playlist_Activity(paramSlot)

	// Clear the Invite Accepted flag
	Clear_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(paramSlot)
	
// KGM 15/9/13: Clear the temporary unlock for every walk-out to prevent low-randked players from starting their own instance
//	// If the mission was being played again, then remove any temporary unlock if the player no longer wants to play it
//	IF (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
		Clear_MissionsAtCoords_Mission_As_Being_Played_Again(paramSlot)
		Clear_MissionsAtCoords_Mission_As_Temporarily_Unlocked_Until_Played(paramSlot)
		Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
//	ENDIF
		
	// Player is no longer inside focus triggering range, so clean up the Focus Mission variables
	Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
		
	// Clear the External InCorona Actions variables
	Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
	
	// Restore Wanted Level and refund the cost if the player paid to lose it
	Restore_And_Refund_MissionsAtCoords_Cleared_Wanted_Level()
		
ENDPROC




// ===========================================================================================================
//      The MP Mission At Coords Stage Check routines
// ===========================================================================================================

// PURPOSE:	Check if the mission is still within the NEW range and update as appropriate
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	Ignore checking for the player being on Other Activities - the check will occur next update instead
FUNC BOOL Maintain_MissionsAtCoords_Stage_New(INT paramSlot)
	
	// Always, change the mission's stage to be 'outside all ranges' to allow more processing on this mission
	Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_OUTSIDE_ALL_RANGES)
	
	// Do an initial 'unlock' update
	Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
	
	// Activate the already setup blip (do this after the above routine which sets the initial lock status)
	IF (Should_MissionsAtCoords_Blips_Be_Displayed())
		Display_MissionsAtCoords_Blip_For_Mission(paramSlot)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Delete a mission
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramPreUpdateBand	The pre-update Band the mission was originally in
// RETURN VALUE:			BOOL				TRUE if the mission has been deleted, otherwise FALSE
//
// NOTES:	Ignore checking for the player being on Other Activities - the mission is marked for delete
FUNC BOOL Maintain_MissionsAtCoords_Stage_Delete(INT paramSlot, g_eMatCBandings paramPreUpdateBand)

	// Delete the blip
	BLIP_INDEX theBlip = Get_MissionsAtCoords_Slot_BlipIndex(paramSlot)

	IF (DOES_BLIP_EXIST(theBlip))
		REMOVE_BLIP(theBlip)
		
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: REMOVE_BLIP Blip being deleted because mission being deleted. Slot: ") PRINTINT(paramSlot) PRINTNL()
		#ENDIF
	ENDIF
	
	// Remove any Angled Areas that were setup on registration
	Delete_MissionsAtCoords_Angled_Area_ArrayPos_For_Slot(paramSlot)
	
	// Clear 'Allow During Ambient Tutorial' flag if set
	Clear_MissionsAtCoords_Mission_As_Allowed_During_Ambient_Tutorial(paramSlot)
	
	// Clear the Most Recent Invite Accepted ContentID
	IF (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
		Clear_MissionsAtCoords_If_Most_Recent_Invite_Accepted_ContentID(paramSlot)
	ENDIF
	
	// Delete the mission (use the MATCB_MISSIONS_BEING_DELETED as the desired band)
	Move_Mission_In_Slot_To_New_Band(paramSlot, paramPreUpdateBand, MATCB_MISSIONS_BEING_DELETED)
	
	#IF IS_DEBUG_BUILD
//		// Confirmation of deletion
//		PRINTSTRING("         MissionsAtCoords Mission in slot: ") PRINTINT(paramSlot) PRINTSTRING(" - DELETED (all remaining entries shuffled up the array to fill the gap)") PRINTNL()
//		
//		// Output the Banding Array Details
//		Debug_Output_Missions_At_Coords_Banding_Array()
		
		// Check the Banding Array Consistency
		Do_Missions_At_Coords_Banding_Array_Consistency_Checking()
	#ENDIF
	
	// Mission Deleted
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The Mission is Sleeping so a Blip and Corona is not currently required and the mission can't be triggered - it needs wakened up
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	Sleeping Missions need some trigger to awaken them, which may be an external trigger
//			This stage was added for gang Attacks just after they've been played to prevent them getting re-triggered immediately
FUNC BOOL Maintain_MissionsAtCoords_Stage_Sleeping(INT paramSlot)

	// Check if the mission should still be sleeping
	IF NOT (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		// ...Mission should no longer be sleeping, so re-display the blip and move to 'outside all ranges'
		IF (Should_MissionsAtCoords_Blips_Be_Displayed())
			Display_MissionsAtCoords_Blip_For_Mission(paramSlot)
		ENDIF
	
		// Change the mission's stage to be 'outside all ranges'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_OUTSIDE_ALL_RANGES)
		PRINTSTRING("          Mission is no longer sleeping") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Still sleeping
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still Outside All Ranges and update as appropriate
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
FUNC BOOL Maintain_MissionsAtCoords_Stage_Outside_All_Ranges(INT paramSlot, FLOAT paramDistance)

	// Deal with this mission becoming the Focus Mission without being in the corona
	IF (Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(paramSlot))
		// ...the mission has become the active mission
		// The mission name should not be on display, but clear this mission's Locate Message bits - this SHOULD be redundant, but there's no harm in doing it to be safe
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
	
		// Change the mission's stage to be 'forcing as Focus Mission'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_FORCING_AS_FOCUS_MISSION)
		PRINTSTRING("          Player is being dragged onto this mission without using a Corona") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission has been marked For Delete
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		// ...Mission has been marked for delete, so move to the 'delete' stage
	
		// Change the mission's stage to be 'delete'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_STAGE_DELETE)
		PRINTSTRING("          Mission Marked For Delete") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission should be sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		// ...Mission should be sleeping, so hide the blip and move to the 'sleeping' stage
		Hide_MissionsAtCoords_Blip_For_Mission(paramSlot)
	
		// Change the mission's stage to be 'sleeping'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_SLEEPING)
		PRINTSTRING("          Mission Sleeping") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// If the mission gets launched immediately then move to the 'Launch Immediately' checking stage
	IF (Should_MissionsAtCoords_Mission_Launch_Immediately(paramSlot))
		// ...mission should launch immediately
	
		// Change the mission's stage to be 'mission launch immediately'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_MISSION_LAUNCH_IMMEDIATELY)
		PRINTSTRING("          Mission should Launch Immediately when unlocked") PRINTNL()
		
		RETURN TRUE
	ENDIF
		
	// Check if the distance is now closer to the Display Ranges
	IF NOT (Is_Inside_MissionsAtCoords_Range_Boundary(MATCB_MISSIONS_OUTSIDE_CORONA_RANGE, paramSlot, paramDistance))
		// ...distance is not inside the Range that is closer to the InCorona range (but not in it), so maintain the lock status of the mission then stay here
		Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)

		RETURN FALSE
	ENDIF
	
	// Change the mission's stage to be 'outside display ranges'
	Set_MissionsAtCoords_Stage_For_Mission_To_Outside_Display_Ranges(paramSlot)
	PRINTSTRING("          Moving Closer: Distance from Mission Coords: ") PRINTFLOAT(paramDistance) PRINTNL()
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still Outside Display Ranges and update as appropriate
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
FUNC BOOL Maintain_MissionsAtCoords_Stage_Outside_Display_Ranges(INT paramSlot, FLOAT paramDistance)

	// Deal with this mission becoming the Focus Mission without being in the corona
	IF (Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(paramSlot))
		// ...the mission has become the active mission
		// If the player has already been forced on to this mission then the mission must be cleaning up, so don't go 'on mission', wait for the variables to clear
		IF (Was_Player_Forced_Onto_MissionsAtCoords_Mission(paramSlot))
			// Player was already forced onto this mission, so wait for it to clean up instead of putting player back on it
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Already forced onto mission - waiting for full cleanup") PRINTNL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		// The mission name should not be on display, but clear this mission's Locate Message bits - this SHOULD be redundant, but there's no harm in doing it to be safe
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
	
		// Change the mission's stage to be 'forcing as Focus Mission' - not activated through corona
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_FORCING_AS_FOCUS_MISSION)
		PRINTSTRING("          Dragged onto mission") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Clear the 'dragged on' flag if the player isn't being dragged onto the mission without using the corona
	Clear_Player_Forced_Onto_MissionsAtCoords_Mission(paramSlot)

	// Check if the mission has been marked For Delete
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		// ...Mission has been marked for delete, so move to the 'delete' stage
	
		// Change the mission's stage to be 'Outside All Ranges'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_OUTSIDE_ALL_RANGES)
		PRINTSTRING("          Mission Marked For Delete") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission should be sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		// ...Mission should be sleeping
	
		// Change the mission's stage to be 'Outside All Ranges'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_OUTSIDE_ALL_RANGES)
		PRINTSTRING("          Mission Sleeping") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// If the mission gets launched immediately then move to the 'Launch Immediately' checking stage
	IF (Should_MissionsAtCoords_Mission_Launch_Immediately(paramSlot))
		// ...mission should launch immediately
	
		// Change the mission's stage to be 'display mission name'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_MISSION_LAUNCH_IMMEDIATELY)
		PRINTSTRING("          Mission should Launch Immediately when unlocked") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the player has moved back outside the Near The InCorona Display Range
	IF (Is_Outside_MissionsAtCoords_Range_Boundary(MATCB_MISSIONS_OUTSIDE_CORONA_RANGE, paramSlot, paramDistance))
		// ...distance is back outside the Near the InCorona Display range
		
		// KGM 21/1/15 [BUG 2187403]: If this is a 'Play Again' mission, don't allow it to get moved out to the 'Ouside All Ranges' Band to ensure it still gets processed more frequently within this band
		IF (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
			// ...mission is far away marked for play again, so maintain the lock status of the mission and stay here - should be ok to spam a message here
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Far Away mission marked 'Play Again'. Keep in ")
				PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(Get_MissionsAtCoords_Stage_For_Mission(paramSlot)))
				PRINTSTRING(" stage for frequent processing. Slot ")
				PRINTINT(paramSlot)
				PRINTSTRING(". ")
				PRINTSTRING(GET_MP_MISSION_NAME(g_sAtCoordsMP[paramSlot].matcMissionIdData.idMission))
				PRINTSTRING(". REGID: ")
				PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot))
				PRINTNL()
			#ENDIF
		
			Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
			Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)

			RETURN FALSE
		ENDIF
	
		// Change the mission's stage to be 'ouside all ranges'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_OUTSIDE_ALL_RANGES)
		PRINTSTRING("          Moving Farther Away: Distance from Mission Coords: ") PRINTFLOAT(paramDistance) PRINTNL()
		
		RETURN TRUE
	ENDIF
		
	// Check if the distance is now inside the Corona Display Ranges
	IF NOT (Is_Inside_MissionsAtCoords_Range_Boundary(MATCB_MISSIONS_INSIDE_CORONA_RANGE, paramSlot, paramDistance))
		// ...distance is not inside the InCorona range, so maintain the lock status of the mission then stay here
		Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)

		RETURN FALSE
	ENDIF

	// Don't display corona if the mission is locked
	IF (Does_MissionsAtCoords_Lock_Restriction_Exist(paramSlot, FALSE))
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
		RETURN FALSE
	ENDIF
	
	// Don't display corona if we are accepting a follow invite
	IF NATIVE_TO_INT(PLAYER_ID()) != -1
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_INITILISE_MISSION
		PRINTLN(".KGM [At Coords]: Corona Display - GlobalplayerBD_FM[iMyGBD].iFmLauncherGameState = FMMC_LAUNCHER_STATE_INITILISE_MISSION, don't do the corona.")
		RETURN FALSE
	ENDIF
	
	// Don't display corona if we are accepting a follow invite
	IF IS_TRANSITION_SESSION_ACCEPTING_FOLLOW_INVITE()
		PRINTLN(".KGM [At Coords]: Corona Display - IS_TRANSITION_SESSION_ACCEPTING_FOLLOW_INVITE - processing a follow invite, don't do the corona.")
		RETURN FALSE
	ENDIF
	
	// The player is inside the corona display range
	// To avoid the player spawning into a new game at a corona and immediately triggering it, don't display this corona if the initial spawn coords are still valid and near the mission
	IF NOT (ARE_VECTORS_ALMOST_EQUAL(g_vInitialSpawnLocation, << 0.0, 0.0, 0.0 >>))
		// ...the initial spawn location is still in force, so check if it is near this corona
		FLOAT	theTolerance	= ciIN_LOCATE_DISTANCE + CORONA_GENERAL_COSMETIC_LEEWAY_m
	
		IF (ARE_VECTORS_ALMOST_EQUAL(g_vInitialSpawnLocation, Get_MissionsAtCoords_Slot_Coords(paramSlot), theTolerance))
			// Don't do this delay for heist Finale and Planning missions - the planning room is too small
			IF (IS_POINT_IN_A_PROPERTY_HOUSE_INTERIOR(GET_PLAYER_COORDS(PLAYER_ID())))
				// ...if player is in a property then this is likely to be a Heist corona, so don't delay and clear the initial spawn vector
				PRINTLN(".KGM [At Coords]: Corona Display - Initial spawn location too close. BUT in an apartment so clear g_vInitialSpawnLocation instead")
				
				g_vInitialSpawnLocation = << 0.0, 0.0, 0.0 >>
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTSTRING(".KGM [At Coords]: Corona Display - Delay. Initial spawn location too close") PRINTNL()
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Do a GroundZ coords check before displaying all the corona details
	// KGM 5/2/14: BUG 1740223 - the GroundZ check can minutely adjust the height of the corona so this must be done before switching off the vehicle generators
	//		or the vectors I pass when switching the generators back on will be minutely different and they won't get cleaned up in code.
	Perform_MissionsAtCoords_Mission_GroundZ_Check(paramSlot)
	
	// Clear the corona area of vehicles and switch off vehicle generators
	// Only do this if there is not a focus mission
	IF NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
		BOOL activeVehicleGenerators = FALSE
		Change_Active_State_Of_Vehicles_Generators_Near_MissionsAtCoords_Corona(paramSlot, activeVehicleGenerators)
	ENDIF
	
	// Do overlapping missions check
	Perform_MissionsAtCoords_Overlapping_Missions_Check(paramSlot)
	
	// Change the mission's stage to be 'display corona range'
	Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
	#IF IS_DEBUG_BUILD
		PRINTSTRING("          Moving Closer: Distance from Mission Coords: ")
		PRINTFLOAT(paramDistance)
		PRINTSTRING(" [Corona Position: ")
		PRINTVECTOR(Get_MissionsAtCoords_Slot_Coords(paramSlot))
		PRINTSTRING("]")
		PRINTNL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still Inside Corona Display Range and update as appropriate
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
FUNC BOOL Maintain_MissionsAtCoords_Stage_Display_Corona_Range(INT paramSlot, FLOAT paramDistance)

	// Always at this range, clear the In Vehicles Help Displayed flag - putting it here means this only gets processed if player within Display Corona range.
	IF (Is_MissionsAtCoords_Mission_In_Vehicle_Help_Text_Displayed(paramSlot))
		IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMMC_NAE_CAR"))
		OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_MG_NAE_CAR"))
			CLEAR_HELP(TRUE)
		ENDIF
	
		Set_MissionsAtCoords_Mission_In_Vehicle_Help_Text_As_Not_Displayed(paramSlot)
	ENDIF

	// Deal with this mission becoming the Focus Mission without being in the corona
	IF (Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(paramSlot))
		// ...the mission has become the active mission
		
		// The mission name should not be on display, but clear this mission's Locate Message bits - this SHOULD be redundant, but there's no harm in doing it to be safe
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
	
		// Change the mission's stage to be 'forcing as Focus Mission'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_FORCING_AS_FOCUS_MISSION)
		PRINTSTRING("          Player is being dragged onto this mission without using a Corona") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission has been marked For Delete
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		// ...Mission has been marked for delete, so move back to the previous stage to allow it to cleanup too if necessary
		
		// NOTE: The corona will automatically switch off if not updated every frame (the update is done in an every-frame maintenance routine based on the mission stage)
	
		// Change the mission's stage to be 'ouside display ranges'
		Set_MissionsAtCoords_Stage_For_Mission_To_Outside_Display_Ranges(paramSlot)
		PRINTSTRING("          Mission Marked For Delete") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission should be sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		// ...Mission should be sleeping
	
		// Change the mission's stage to be 'ouside display ranges'
		Set_MissionsAtCoords_Stage_For_Mission_To_Outside_Display_Ranges(paramSlot)
		PRINTSTRING("          Mission Sleeping") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// If the mission gets launched immediately then move to the 'Launch Immediately' checking stage
	IF (Should_MissionsAtCoords_Mission_Launch_Immediately(paramSlot))
		// ...mission should launch immediately
	
		// Change the mission's stage to be 'display mission name'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_MISSION_LAUNCH_IMMEDIATELY)
		PRINTSTRING("          Mission should Launch Immediately when unlocked") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the player has moved back outside the InCorona Range
	IF (Is_Outside_MissionsAtCoords_Range_Boundary(MATCB_MISSIONS_INSIDE_CORONA_RANGE, paramSlot, paramDistance))
		// ...distance is back outside the InCorona range
		
		// NOTE: The corona will automatically switch off if not updated every frame (the update is done in an every-frame maintenance routine based on the mission stage)
		
		// As a one-off, hide the blip if it is an 'Interiors Only' blip - it should only display within 'Display Corona' range
		IF (Should_MissionsAtCoords_Mission_Blip_Display_In_Interior_Only(paramSlot))
			Hide_MissionsAtCoords_Blip_For_Mission(paramSlot)
		ENDIF
	
		// Change the mission's stage to be 'outside display ranges'
		Set_MissionsAtCoords_Stage_For_Mission_To_Outside_Display_Ranges(paramSlot)
		PRINTSTRING("          Moving Farther Away: Distance from Mission Coords: ") PRINTFLOAT(paramDistance) PRINTNL()

		RETURN TRUE
	ENDIF
	
	// If the mission is Area-Triggered then move to the 'Area Triggered' checking stage
	IF (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
		// ...mission is Area-Triggered
	
		// Change the mission's stage to be 'area-triggered mission'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_AREA_TRIGGERED_MISSION)
		PRINTSTRING("          Mission is Area-Triggered") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the distance is now inside the Mission Name Range
	IF NOT (Is_Inside_MissionsAtCoords_Range_Boundary(MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE, paramSlot, paramDistance))
		// ...distance is not inside the Mission Name range, so maintain the lock status of the mission then stay here
		Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
		
		RETURN FALSE
	ENDIF
		
	// As a one-off, display the blip if it is an 'Interiors Only' blip and the player is in an interior - it should only display within 'Display Corona' range
	IF (Should_MissionsAtCoords_Mission_Blip_Display_In_Interior_Only(paramSlot))
		IF (Should_MissionsAtCoords_Blips_Be_Displayed())
			IF (Is_MatC_Player_In_Interior())
				Display_MissionsAtCoords_Blip_For_Mission(paramSlot, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	// Distance is now inside the Mission Name Display range
	// Change the mission's stage to be 'Requesting Mission Name'
	Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
	PRINTSTRING("          Moving Closer: Distance from Mission Coords: ") PRINTFLOAT(paramDistance) PRINTNL()
	PRINTSTRING("          (Distance Required To Become Focus Mission : ") PRINTFLOAT(Get_MissionsAtCoords_Inner_Range_Boundary(MATCB_FOCUS_MISSION, paramSlot)) PRINTSTRING(")") PRINTNL()
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is in the Triggering Area
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
FUNC BOOL Maintain_MissionsAtCoords_Stage_Area_Triggered_Mission(INT paramSlot, FLOAT paramDistance)

	// Deal with this mission becoming the Focus Mission without being in the corona
	IF (Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(paramSlot))
		// ...the mission has become the active mission
		
		// The mission name should not be on display, but clear this mission's Locate Message bits - this SHOULD be redundant, but there's no harm in doing it to be safe
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
	
		// Change the mission's stage to be 'forcing as Focus Mission'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_FORCING_AS_FOCUS_MISSION)
		PRINTSTRING("          Player is being dragged onto this mission without using a Corona") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission has been marked For Delete
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		// ...Mission has been marked for delete, so move back to the previous stage to allow it to cleanup too if necessary
	
		// Change the mission's stage to be 'Display Corona range'
		Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
		PRINTSTRING("          Mission Marked For Delete") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission should be sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		// ...Mission should be sleeping
	
		// Change the mission's stage to be 'Display Corona range'
		Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
		PRINTSTRING("          Mission Sleeping") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the player has moved back outside the InCorona Range
	IF (Is_Outside_MissionsAtCoords_Range_Boundary(MATCB_MISSIONS_INSIDE_CORONA_RANGE, paramSlot, paramDistance))
		// ...distance is back outside the InCorona range, but move back to the 'display corona' stage in case it needs to clean anything up first
	
		// Change the mission's stage to be 'Display Corona range'
		Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
		PRINTSTRING("          Moving Farther Away: Distance from Mission Coords: ") PRINTFLOAT(paramDistance) PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Stay here if the mission is locked
	IF (Is_MissionsAtCoords_Mission_Locked(paramSlot))
		// ...mission is locked, so maintain the lock status then stay here
		Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
	
		RETURN FALSE
	ENDIF
	
	// Check if the player has moved inside the triggering area
	IF NOT (Is_Player_Inside_MissionAtCoords_Angled_Area(paramSlot))
		// ...player hasn't moved into the triggering area, so maintain the lock status of the mission then stay here
		Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
		
		RETURN FALSE
	ENDIF
	
	// Player is inside the triggering area, but ignore if there is already a focus mission
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		// ...there is already a focus mission, so maintain the lock status of the mission then stay here
		Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
	
		RETURN FALSE
	ENDIF
	
	// The cloud is not available, so ignore until it becomes available again
	IF (IS_CLOUD_DOWN_CLOUD_LOADER())
		// ...the cloud is down, so maintain the lock status of the mission then stay here
		Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
	
		RETURN FALSE
	ENDIF
	
	// Stay here if the synced interaction at the end of the mission is active
	IF (IS_POST_MISSION_SCENE_ACTIVE())
	    // ...synced interaction is active, so maintain the lock status then stay here
	    Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
	    Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)

	    RETURN FALSE
	ENDIF
	
	// KGM 18/2/15 [BUG 2241726]: Stay here if the player is actioning a Transition Session Invite
	IF (IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED())
	    // ...synced interaction is active, so maintain the lock status then stay here
	    Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
	    Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)

	    RETURN FALSE
	ENDIF
	
	// KGM 1/4/15 [BUG 2281100]: Stay here if the game is currently Getting Content By ID (this is aminly to fix a timing problem with walking into a Gang Attack when On Call)
	IF (ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID())
	    // ...synced interaction is active, so maintain the lock status then stay here
	    Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
	    Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
		
		PRINTLN(".KGM [At Coords]: Gang Attack - Delay. ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID() is TRUE. Slot: ", paramSlot)

	    RETURN FALSE
	ENDIF
	
	// KGM 1/7/15 [BUG 2370916]: Don't trigger if the player is on an FM Event
	IF (FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID()))
		// ...player is currently on an FM Event, so maintain the lock status of the mission then stay here
		Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
		
		PRINTLN(".KGM [At Coords]: Gang Attack - Delay. FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT() is TRUE. Slot: ", paramSlot)
		
		RETURN FALSE
	ENDIF
	
	// KEITH VERY TEMP 3/7/13: TO AVOID A GANG ATTACK JUMPING IN AND KICKING OFF WHEN THE PLAYER IS IN THE CORONA FOR A MISSION
	// NOTE: This will also prevent Gang Attacks from triggering when the mission wouldn't have been able to trigger anyway (rank restrictions, etc) - which isn't desirable
	// NOTE: This should be replaced when the overlapping missions routines are added
	IF (IS_NET_PLAYER_OK(PLAYER_ID()))
		FLOAT	checkingRadius	= 0.0
		INT		tempLoop		= 0
		VECTOR	playerCoords	= GET_PLAYER_COORDS(PLAYER_ID())
		
		// Check only missions with coronas
		REPEAT g_numAtCoordsMP tempLoop
			IF NOT (IS_BIT_SET(g_sAtCoordsMP[tempLoop].matcOptionBitflags, MATC_BITFLAG_AREA_TRIGGERED))
				// ...mission is triggered at a corona, so check if the vector is in the corona
				checkingRadius = g_sAtCoordsMP[tempLoop].matcCorona.triggerRadius + 0.5
			
				IF (ARE_VECTORS_ALMOST_EQUAL(g_sAtCoordsMP[tempLoop].matcCoords, playerCoords, checkingRadius))
					// Player is in the corona for another mission so don't trigger the Gang Attack
					Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
					Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
				
					#IF IS_DEBUG_BUILD
						PRINTSTRING(".KGM [At Coords]: Gang Attack - Delay. In mission corona within the Gang Attack area. Mission slot: ")
						PRINTINT(tempLoop)
						PRINTNL()
					#ENDIF
				
					RETURN FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	// END KEITH VERY TEMP
	
	// Try to become Focus Mission and start external InCorona screens
	RETURN (Try_To_Become_MissionsAtCoords_Focus_Mission(paramSlot, paramDistance))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission should launch immediately when unlocked
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
FUNC BOOL Maintain_MissionsAtCoords_Stage_Mission_Launch_Immediately(INT paramSlot, FLOAT paramDistance)

	// Deal with this mission becoming the Focus Mission without being in the corona
	IF (Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(paramSlot))
		// ...the mission has become the active mission
		
		// The mission name should not be on display, but clear this mission's Locate Message bits - this SHOULD be redundant, but there's no harm in doing it to be safe
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
	
		// Change the mission's stage to be 'forcing as Focus Mission'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_FORCING_AS_FOCUS_MISSION)
		PRINTSTRING("          Player is being dragged onto this mission without using a Corona") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission has been marked For Delete
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		// ...Mission has been marked for delete, so move back to the previous stage to allow it to cleanup too if necessary
	
		// Change the mission's stage to be 'Display Corona range'
		Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
		PRINTSTRING("          Mission Marked For Delete") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission should be sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		// ...Mission should be sleeping
	
		// Change the mission's stage to be 'Display Corona range'
		Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
		PRINTSTRING("          Mission Sleeping") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Mission should launch immediately if unlocked
	Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
	
	// Nothing more to do if the mission is locked
	IF (Is_MissionsAtCoords_Mission_Locked(paramSlot))
		// Mission is locked, so stay here
		RETURN FALSE
	ENDIF
	
	// Mission is not locked and so should launch immediately, but ignore if there is already a focus mission
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		// ...there is already a focus mission, so stay here
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Immediate Launch - IGNORE: already a Focus Mission") PRINTNL()
		#ENDIF
			
		RETURN FALSE
	ENDIF
	
	// Try to become Focus Mission and start external InCorona screens
	RETURN (Try_To_Become_MissionsAtCoords_Focus_Mission(paramSlot, paramDistance))
	
ENDFUNC
/// PURPOSE: Update the Bit sets that relay which retriction state it is in.

PROC Maintain_restriction_Bits(INT paramSlot,g_eMatCLaunchFailReasonID restrictionID)
// update the BIT sets for each restiction depending on the current state
	if 	restrictionID	!= MATCLFR_NONE
		if 		restrictionID	= MATCLFR_WANTED_LEVEL
			SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_WANTED)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_CASH)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_RANK)	
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags,	MATC_BITFLAG_RESTRICTED_TOD)
		elif	restrictionID	= MATCLFR_NOT_ENOUGH_CASH
			SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_CASH)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_WANTED)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_RANK)	
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags,	MATC_BITFLAG_RESTRICTED_TOD)
		elif	restrictionID	= MATCLFR_RANK_TOO_LOW
			SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_RANK)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_CASH)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_WANTED)	
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags,	MATC_BITFLAG_RESTRICTED_TOD)
		elif	restrictionID	= MATCLFR_WRONG_TIMEOFDAY
			SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags,		MATC_BITFLAG_RESTRICTED_TOD)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_CASH)
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_RANK)	
			CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, 	MATC_BITFLAG_RESTRICTED_WANTED)
		endif				
	ELSE
		CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_WANTED)
		CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_CASH)
		CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_RANK)	
		CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_TOD)
	ENDIF	
	
ENDPROC
/// PURPOSE: Update the state of the message the locate is displaying.
 
FUNC BOOL Maintain_Locate_Message_Details(int paramSlot)
	
	INT thisLocateMessageID = Get_MissionsAtCoords_Locate_MessageID_From_Bitflags(paramSlot)
	IF (Request_Locate_Message(thisLocateMessageID))
		// The locate message request has succeeded, so store the details
		MP_MISSION			thisMissionID		= Get_MissionsAtCoords_Slot_MissionID(paramSlot)
		INT					thisVariation		= Get_MissionsAtCoords_Slot_Mission_Variation(paramSlot)
		INT					thisCreatorID		= Get_MissionsAtCoords_Slot_CreatorID(paramSlot)
		HUD_COLOURS			theTextColour		= Get_MissionsAtCoords_Slot_InCorona_Text_Hud_Colour(paramSlot)
		MP_MISSION_ID_DATA	thisMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
				
		// 1905690 - Gets the XP and Cash Multiplier values from tunables (if there are any)
		FLOAT xpMulti = 0
		FLOAT cashMulti = 0
		FLOAT apMulti = 0
		
		IF g_sMPTunables.bdisable_modifier_badges
//		OR (Convert_MissionID_To_FM_Mission_Type(thisMissionID) = FMMC_TYPE_MISSION
//		AND Get_SubType_For_FM_Cloud_Loaded_Activity(thisMissionIdData) = FMMC_MISSION_TYPE_FLOW_MISSION)
			PRINTLN("[MMM] No Badge Displayed on Coords Corona - Displaying of badges has been overridden by g_sMPTunables.bdisable_modifier_badges")
		ELSE
			BOOL bAdversary
			IF (thisMissionIDdata.idCreator) = FMMC_ROCKSTAR_CREATOR_ID 
			OR (thisMissionIDdata.idCreator) = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
				bAdversary = IS_THIS_AN_ADVERSARY_MODE_MISSION(Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(thisMissionIDdata), Get_AdversaryModeType_For_FM_Cloud_Loaded_Activity(thisMissionIDdata))
			ELSE
				bAdversary = FALSE
			ENDIF
			BOOL bGangOps = GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(thisMissionIDdata))
			BOOL bArenaWar = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(thisMissionIDdata))
			BOOL bMember = FALSE
			#IF FEATURE_GTAO_MEMBERSHIP
			IF IS_GTAO_MEMBERSHIP_CONTENT_ENABLED()
				bMember = TRUE
			ENDIF
			#ENDIF
			Get_RP_And_Cash_Values(xpMulti, cashMulti, apMulti, GET_HASH_KEY(thisMissionIdData.idCloudFilename), 
													   Convert_MissionID_To_FM_Mission_Type(thisMissionID), 
													   Get_SubType_For_FM_Cloud_Loaded_Activity(thisMissionIdData),
													   Is_FM_Cloud_Loaded_Activity_A_Bicycle_Race(thisMissionIDdata), 
													   bAdversary,
													   bGangOps,
													   bArenaWar,
													   bMember)
		ENDIF
		// ...gather any additional cloud-loaded details if applicable
		TEXT_LABEL_23		thisCreatorName		= ""
		TEXT_LABEL_63		secondTextLine		= ""
		BOOL				secondLineIsString	= TRUE
		INT					thisRating			= -1
		INT					thisLesRating		= -1
		INT					minimumPlayers		= Get_Minimum_Players_Required_For_MP_Mission_Request(thisMissionIdData)
		INT					maximumPlayers		= Get_Maximum_Players_Required_For_MP_Mission_Request(thisMissionIdData)			
		TEXT_LABEL_23		thisMissionTitle	= Get_MissionsAtCoords_Slot_MissionTitle(thisMissionIdData)			
		INT					paramTODActive		= Get_MissionsAtCoords_Mission_Unlocked_Next_Time_Of_Day_Active(paramSlot)
		BOOL				isVerified			= FALSE
		BOOL				showReserved		= FALSE
		
		// ...if there are any launch restrictions, the second line of text in the corona will change to explain the restriction
		g_eMatCLaunchFailReasonID	restrictionID	= MATCLFR_NONE
		BOOL						hasRestriction	= Check_For_MissionsAtCoords_Mission_Launch_Restriction(paramSlot, restrictionID,true)
					
		IF (hasRestriction)
			if Is_MissionsAtCoords_Mission_Unlocked_At_This_Time_Of_Day(paramSlot)
			or restrictionID != MATCLFR_WRONG_TIMEOFDAY
				// make sure the TOD is not being used in the second param when displaying a different restriction reason than TOD.
				paramTODActive = -1
			endif
			secondTextLine		= Get_Corona_Locate_Message_Restriction_Text(restrictionID)
			secondLineIsString	= FALSE	
		endif
		//update the Bit set that look at what restrictions to display.
		Maintain_restriction_Bits(paramSlot, restrictionID)
					
		IF (Does_MP_Mission_Variation_Get_Data_From_The_Cloud(thisMissionID, thisVariation))
			thisCreatorName	= Get_User_Name_For_FM_Cloud_Loaded_Activity(thisMissionIdData)
			thisRating		= Get_User_Rating_For_FM_Cloud_Loaded_Activity(thisMissionIdData)
			
			#IF IS_DEBUG_BUILD
				thisLesRating	= Get_Les_Rating_For_FM_Cloud_Loaded_Activity(thisMissionIdData)
			#ENDIF
			
			IF NOT (hasRestriction)
				secondTextLine		= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(thisMissionIdData)
				secondLineIsString	= TRUE
			ENDIF
			
			// Rockstar Candidate missions are classed as 'verified'
			IF (thisCreatorID = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
				isVerified = TRUE
			ENDIF
		ENDIF
		
		// MiniGames should indicate that players are waiting to play in the corona
		IF (thisCreatorID = FMMC_MINI_GAME_CREATOR_ID)
			IF (Is_This_Mission_Request_Reserved_And_Joinable(thisMissionIdData))
				showReserved = TRUE
			ENDIF
		ENDIF
		
		if showReserved
			SET_BIT(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_SHOW_RESERVED)		
		else
			CLEAR_BIT(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_SHOW_RESERVED)
		endif
		
		// KGM 21/7/15 [BUG 2425819]: A new string has been added for FM Event Time Trials to show a target time - this is not currently required within MissionsAtCoords
		STRING	ignoreTimeString = ""
		INT		ignoreTimeAsMsec = 0
		
		Store_Locate_Message_Details(thisLocateMessageID, thisMissionID, thisVariation, 
									 thisMissionTitle, theTextColour, secondTextLine, 
									 secondLineIsString, thisCreatorName, thisRating, 
									 thisLesRating, minimumPlayers, maximumPlayers, 
									 paramTODActive, isVerified, hasRestriction, 
									 showReserved, ignoreTimeString, ignoreTimeAsMsec,
									 xpMulti, cashMulti)
		
		// Update the Locate Message bitfields, just in case the LocateMessageID changed
		Set_MissionsAtCoords_Locate_Message_Bitflag_From_Locate_MessageID(paramSlot, thisLocateMessageID)
					
		RETURN TRUE
	ENDIF			
	
	// Update the Locate Message bitfields
	Set_MissionsAtCoords_Locate_Message_Bitflag_From_Locate_MessageID(paramSlot, thisLocateMessageID)
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still Inside Mission Name Range and continue to request the Locate Message as appropriate
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
FUNC BOOL Maintain_MissionsAtCoords_Stage_Requesting_Mission_Name(INT paramSlot, FLOAT paramDistance)

	// Deal with this mission becoming the Focus Mission without being in the corona
	IF (Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(paramSlot))
		// ...the mission has become the active mission
		// If the player has already been forced on to this mission then the mission must be cleaning up, so don't go 'on mission', wait for the variables to clear
		IF (Was_Player_Forced_Onto_MissionsAtCoords_Mission(paramSlot))
			// Player was already forced onto this mission, so wait for it to clean up instead of putting player back on it
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Already forced onto mission - waiting for full cleanup") PRINTNL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		// The mission name should not be on display, but clear this mission's Locate Message bits - this SHOULD be redundant, but there's no harm in doing it to be safe
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
	
		// Change the mission's stage to be 'forcing as Focus Mission'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_FORCING_AS_FOCUS_MISSION)
		PRINTSTRING("          Dragged onto mission") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Clear the 'dragged on' flag if the player isn't being dragged onto the mission without using the corona
	Clear_Player_Forced_Onto_MissionsAtCoords_Mission(paramSlot)

	// Check if the mission has been marked For Delete
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		// ...Mission has been marked for delete, so move back to the previous stage to allow it to cleanup too if necessary
		
		// The mission name should not be on display, but clear this mission's Locate Message bits - this SHOULD be redundant, but there's no harm in doing it to be safe
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
	
		// Change the mission's stage to be 'Display Corona range'
		Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
		PRINTSTRING("          Marked For Delete") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission should be sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		// ...Mission should be sleeping
		
		// The mission name should not be on display, but clear this mission's Locate Message bits - this SHOULD be redundant, but there's no harm in doing it to be safe
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
	
		// Change the mission's stage to be 'Display Corona range'
		Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
		PRINTSTRING("          Sleeping") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// If the mission gets launched immediately then move to the 'Launch Immediately' checking stage
	IF (Should_MissionsAtCoords_Mission_Launch_Immediately(paramSlot))
		// ...mission should launch immediately
	
		// Change the mission's stage to be 'mission launch immediately'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_MISSION_LAUNCH_IMMEDIATELY)
		PRINTSTRING("          Launch Immediately when unlocked") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the player has moved back outside the Mission Name Range
	IF (Is_Outside_MissionsAtCoords_Range_Boundary(MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE, paramSlot, paramDistance))
		// ...distance is back outside the Mission Name range
		
		// The mission name should not be on display, but clear this mission's Locate Message bits - this SHOULD be redundant, but there's no harm in doing it to be safe
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
	
		// Is the activity now locked? If locked then make the mission's stage 'outside display ranges', otherwise make it 'inside corona range'
		IF (Is_MissionsAtCoords_Mission_Locked(paramSlot))
			// ...locked, so change the mission's stage to be 'outside display ranges'
			Set_MissionsAtCoords_Stage_For_Mission_To_Outside_Display_Ranges(paramSlot)
			PRINTSTRING("          Moving Farther Away and Mission Locked. Distance: ") PRINTFLOAT(paramDistance) PRINTNL()
		ELSE
			// ...not locked, so change the mission's stage to be 'Display Corona range'
			Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
			PRINTSTRING("          Moving Farther Away. Distance: ") PRINTFLOAT(paramDistance) PRINTNL()
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	// If the mission is Area-Triggered then move to the 'Area Triggered' checking stage
	IF (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
		// ...mission is Area-Triggered
	
		// Change the mission's stage to be 'area-triggered mission'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_AREA_TRIGGERED_MISSION)
		PRINTSTRING("          Area-Triggered") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Even if the player moves into the 'reserve mission' range, there is no guarantee this mission will become the Focus Mission,
	//		so the Locate Message needs to continue being maintained until this mission has become the Focus mission
	
	BOOL displayCoronas = Should_MissionsAtCoords_Coronas_Be_Displayed()
	
	IF g_matcBlockAndHideAllMissions
		IF Should_MissionAtCoords_Ignore_Block_On_Mission(paramSlot)
			displayCoronas = TRUE
		ENDIF
	ENDIF
	
	// Request the Locate Message if they are currently allowed to be displayed
	IF (displayCoronas)
		// If a corona is not needed for this mission then just move on to the 'displaying details' stage
		IF NOT (Should_MissionsAtCoords_Mission_Display_A_Corona(paramSlot))
			Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_DISPLAYING_MISSION_NAME)
			PRINTSTRING("          Mission Name not required") PRINTNL()
			
			RETURN TRUE
		ENDIF
	
		// Don't draw a locate message if it is an overlapped mission (unless on a commandline parameter)
		IF (Is_MissionsAtCoords_Mission_Hidden_By_Overlapping_Corona(paramSlot))
			BOOL quitIfOverlapped = TRUE
			
			#IF IS_DEBUG_BUILD
				IF (GET_COMMANDLINE_PARAM_EXISTS("sc_ShowOverlappingCoronas"))
					quitIfOverlapped = FALSE
				ENDIF
			#ENDIF
			
			IF (quitIfOverlapped)
				Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_DISPLAYING_MISSION_NAME)
				PRINTSTRING("          Overlapped Mission - Mission name Not Required") PRINTNL()
				
				RETURN TRUE
			ENDIF
		ENDIF
		
		// KGM 12/5/15: BUG 2313555 - Don't draw a locate message if hidden by a PI Menu Job Type Hide Blip option
		IF (Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu(paramSlot))
			Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_DISPLAYING_MISSION_NAME)
			PRINTSTRING("          Job Type Hidden By PI Menu Option - Mission name Not Required") PRINTNL()
			
			RETURN TRUE
		ENDIF
		
		// KGM 11/6/15: BUG 2316703 - Don't draw a locate message if mission is within a disabled area
		IF (Is_MissionsAtCoords_Mission_Hidden_By_Disabled_Area(paramSlot))
			Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_DISPLAYING_MISSION_NAME)
			PRINTSTRING("          Job Type Hidden By Disabled Area - Mission name Not Required") PRINTNL()
			
			RETURN TRUE
		ENDIF
		
		//Get and store the mission details that are to be displayed.
		IF Maintain_Locate_Message_Details(paramSlot)		
			
			// Update the stage to the 'displaying mission name' stage to maintain the Locate Message while required
			Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_DISPLAYING_MISSION_NAME)
			PRINTSTRING("          Mission Name on display") PRINTNL()
			
			RETURN TRUE // The locate message requested for this mission has succeeded
		ENDIF
		
		// The request failed, but update the Locate Message bitfields, just in case the LocateMessageID changed
		INT thisLocateMessageID = Get_MissionsAtCoords_Locate_MessageID_From_Bitflags(paramSlot)
		Set_MissionsAtCoords_Locate_Message_Bitflag_From_Locate_MessageID(paramSlot, thisLocateMessageID)
	ENDIF

	// KGM 22/5/13: During a transition from Invite screen to Betting screen the players all warp to the corona position and
	//				sometimes they get pushed out of the corona if multiple players are there at the same time.
	//				Under these circumstances, ensure the player still tries to reserve the mission.
	IF NOT (Is_This_Mission_Restarting_During_Transition(paramSlot))
	
		// This mission isn't restarting during a transition, check if the distance is now inside the Focus Mission Range
		IF NOT (Is_Inside_MissionsAtCoords_Range_Boundary(MATCB_FOCUS_MISSION, paramSlot, paramDistance))
			// ...distance is not inside the Focus Mission range, so maintain the lock status of the mission then stay here
			Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Player is inside the focus range, but ignore if there is already a focus mission
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		// ...there is already a focus mission, so maintain the lock status of the mission then stay here
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
	
		RETURN FALSE
	ENDIF
	
	// Maintain vehicle checks
	IF (Check_For_In_Vehicle_And_Maintain_Help(paramSlot))
		// ...player is in a vehicle, so maintain the lock status of the mission then stay here
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
	
		RETURN FALSE
	ENDIF
	
	// Distance is now inside the Focus Mission range, so try to become Focus Mission and start external InCorona screens
	RETURN (Try_To_Become_MissionsAtCoords_Focus_Mission(paramSlot, paramDistance))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still Inside Mission Name Range and continue to allow the Locate Message to be displayed
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
FUNC BOOL Maintain_MissionsAtCoords_Stage_Displaying_Mission_Name(INT paramSlot, FLOAT paramDistance)

	// Deal with this mission becoming the Focus Mission without being in the corona
	IF (Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(paramSlot))
		// ...the mission has become the active mission
		
		// The mission name is on display, so clear this mission's Locate Message bits
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
		
		// Clear the Corona Ground Projection if on display
		Clear_MissionsAtCoords_Slot_Corona_Ground_Projection(paramSlot)
	
		// Change the mission's stage to be 'forcing as Focus Mission'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_FORCING_AS_FOCUS_MISSION)
		PRINTSTRING("          Player is being dragged onto this mission without using a Corona") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission has been Marked For Deletion, then go back to a previous stage to allow it to clean up if necessary
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		// ...mission is Marked For Delete
		
		// Clear the Corona Ground Projection if on display
		Clear_MissionsAtCoords_Slot_Corona_Ground_Projection(paramSlot)
	
		// Change the mission's stage to be 'Display Corona range'
		Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
		PRINTSTRING("          Mission Marked For Delete") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission should be sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		// ...Mission should be sleeping
		
		// Clear the Corona Ground Projection if on display
		Clear_MissionsAtCoords_Slot_Corona_Ground_Projection(paramSlot)
	
		// Change the mission's stage to be 'Display Corona range'
		Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
		PRINTSTRING("          Mission Sleeping") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// If the mission gets launched immediately then move to the 'Launch Immediately' checking stage
	IF (Should_MissionsAtCoords_Mission_Launch_Immediately(paramSlot))
		// ...mission should launch immediately
	
		// Change the mission's stage to be 'mission launch immediately'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_MISSION_LAUNCH_IMMEDIATELY)
		PRINTSTRING("          Mission should Launch Immediately when unlocked") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the player has moved back outside the Mission Name Range
	IF (Is_Outside_MissionsAtCoords_Range_Boundary(MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE, paramSlot, paramDistance))
		// ...distance is back outside the Mission Name range
		
		// The mission name is on display, so clear this mission's Locate Message bits
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
		
		// Clear the Corona Ground Projection if on display
		Clear_MissionsAtCoords_Slot_Corona_Ground_Projection(paramSlot)
		
		// Is the activity now locked? If locked then make the mission's stage 'outside display ranges', otherwise make it 'inside corona range'
		IF (Is_MissionsAtCoords_Mission_Locked(paramSlot))
			// ...locked, so change the mission's stage to be 'outside display ranges'
			Set_MissionsAtCoords_Stage_For_Mission_To_Outside_Display_Ranges(paramSlot)
			PRINTSTRING("          Moving Farther Away and Mission Locked: Distance from Mission Coords: ") PRINTFLOAT(paramDistance) PRINTNL()
		ELSE
			// ...not locked, so change the mission's stage to be 'Display Corona range'
			Set_MissionsAtCoords_Stage_For_Mission_To_Display_Corona_Range(paramSlot)
			PRINTSTRING("          Moving Farther Away: Distance from Mission Coords: ") PRINTFLOAT(paramDistance) PRINTNL()
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	BOOL displayCoronas = Should_MissionsAtCoords_Coronas_Be_Displayed()
	
	IF g_matcBlockAndHideAllMissions
		IF Should_MissionAtCoords_Ignore_Block_On_Mission(paramSlot)
			displayCoronas = TRUE
		ENDIF
	ENDIF
	
	// If Mission Names are currently not allowed to be displayed, then go back to requesting the mission name again
	IF NOT (displayCoronas)
		// ...mission names are not allowed to be displayed
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Coronas and Blips - Being Hidden") PRINTNL()
		#ENDIF
		
		// The mission name is on display, so clear this mission's Locate Message bits
		Clear_MissionsAtCoords_Locate_Message_Bitflags(paramSlot)
		
		// Clear the Corona Ground Projection if on display
		Clear_MissionsAtCoords_Slot_Corona_Ground_Projection(paramSlot)
		
		// Clear Speed Zone
		INT regID = Get_MissionsAtCoords_RegistrationID_For_Mission(paramSlot)
		IF (Does_MissionsAtCoords_SpeedZone_Exist_For_Mission(regID))
			// Clean up the speed zone
			Remove_MissionsAtCoords_SpeedZone(paramSlot)
		ENDIF
		
		// Clear Scenario Blocking
		IF (Does_MissionsAtCoords_ScenarioBlocking_Exist_For_Mission(regID))
			// Clean up the scenario blocking
			Remove_MissionsAtCoords_ScenarioBlocking(paramSlot)
		ENDIF
	
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Mission Names not required") PRINTNL()
		
		RETURN TRUE
	ENDIF
		
	// Update the lock status of the mission
	Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)	
	
	// If the mission is Area-Triggered then move to the 'Area Triggered' checking stage
	IF (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
		// ...mission is Area-Triggered
	
		// Change the mission's stage to be 'area-triggered mission'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_AREA_TRIGGERED_MISSION)
		PRINTSTRING("          Area-Triggered") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// KGM 22/5/13: During a transition from Invite screen to Betting screen the players all warp to the corona position and
	//				sometimes they get pushed out of the corona if multiple players are there at the same time.
	//				Under these circumstances, ensure the player still tries to reserve the mission.
	IF NOT (Is_This_Mission_Restarting_During_Transition(paramSlot))
	
		// This mission isn't restarting during a transition, check if the distance is now inside the Focus Mission Range
		IF NOT (Is_Inside_MissionsAtCoords_Range_Boundary(MATCB_FOCUS_MISSION, paramSlot, paramDistance))
			// ...distance is not inside the Focus Mission range
		
			// Setup the Corona Ground Projection
			Setup_MissionsAtCoords_Corona_Ground_Projection(paramSlot)
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Player is inside the focus range, but ignore if there is already a focus mission
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		// ...there is already a focus mission, so stay here
		RETURN FALSE
	ENDIF
	
	// Maintain vehicle checks
	IF (Check_For_In_Vehicle_And_Maintain_Help(paramSlot))
		// ...player is in a vehicle, so stay here
		RETURN FALSE
	ENDIF
	
	// NOTE: All Corona Ground Projections get cleaned up when the player gets a Focus Mission, so just ignore the projections here
	
	// Distance is now inside the Focus Mission range, so try to become the Focus Mission and activate external Corona routines
	RETURN (Try_To_Become_MissionsAtCoords_Focus_Mission(paramSlot, paramDistance))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The player is being dragged onto a mission without using a corona, so need to make this mission the Focus Mission
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
FUNC BOOL Maintain_MissionsAtCoords_Stage_Forcing_As_Focus_Mission(INT paramSlot)

	// Check if the player is still being dragged onto the mission
	IF NOT (Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(paramSlot))
		// ...player is no longer being dragged onto the mission, so go back to the 'Requesting Mission Name' stage so that the mission can find its appropriate stage again based on distance
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Player was being dragged onto a mission without using a corona, but player is no longer being dragged onto the mission") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Check if there is already a focus mission
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		// ...there is a focus mission, so do nothing until the current focus mission has cleaned up - assume the focus mission isn't this slot's mission
		RETURN FALSE
	ENDIF
	
	// There isn't a Focus Mission, so change the mission's stage to be 'Waiting For On Mission'
	Set_MissionsAtCoords_Stage_For_Mission_To_Waiting_For_On_Mission(paramSlot)
			
	#IF IS_DEBUG_BUILD
		PRINTSTRING("          Player is being dragged onto this mission without using a Corona") PRINTNL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If quitting during 'allow reservation', ensure the player gets unreserved for any pre-reserved mission
// NOTES:	This is to prevent a problem should the player get reserved by accepting an invite, and then being forced out of the corona before being properly reserved (ie: download problem)
PROC Unreserve_MissionsAtCoords_Player_From_Any_PreReserved_Mission()

	INT myUniqueID = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	
	IF (myUniqueID = NO_UNIQUE_ID)
		EXIT
	ENDIF
	
	// Player must have been preReserved for a mission, probably by accepting an invite. Need to remove the player from the mission.
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Cancel Mission preReservation. UniqueID: ")
		PRINTINT(myUniqueID)
		PRINTNL()
	#ENDIF
	
	Broadcast_Cancel_Mission_Reservation_By_Player(myUniqueID)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still Waiting For Allow Reservation and update as appropriate
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	Ignore checking for the player being on Other Activities when the mission is the Focus Mission - feedback from other sources will allow appropriate action to take place
FUNC BOOL Maintain_MissionsAtCoords_Stage_Waiting_For_Allow_Reservation(INT paramSlot, FLOAT paramDistance)
	
	// Disable entering vehicles while in the corona but before the corona takes control
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	
	// Check if the mission has been marked for delete, clean up and move back to a previous stage so it can clean up too if necessary
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		// ...mission is marked for deletion
		Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
		
		// Clear the External InCorona Actions variables
		Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
		
		// Cancel reservation if player was pre-reserved for the mission (ie: by accepting an invite)
		Unreserve_MissionsAtCoords_Player_From_Any_PreReserved_Mission()
	
		// Clear Invite Accepted
		Clear_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(paramSlot)
	
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Mission Marked For Delete") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission should be sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		// ...Mission should be sleeping
		Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
		
		// Clear the External InCorona Actions variables
		Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
		
		// Cancel reservation if player was pre-reserved for the mission (ie: by accepting an invite)
		Unreserve_MissionsAtCoords_Player_From_Any_PreReserved_Mission()
	
		// Clear Invite Accepted
		Clear_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(paramSlot)
	
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Mission Sleeping") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Check if the mission has been marked for delete, clean up and move back to a previous stage so it can clean up too if necessary
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
		// ...player is dead
		Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
		
		// Clear the External InCorona Actions variables
		Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
		
		// Cancel reservation if player was pre-reserved for the mission (ie: by accepting an invite)
		Unreserve_MissionsAtCoords_Player_From_Any_PreReserved_Mission()
	
		// Clear Invite Accepted
		Clear_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(paramSlot)
	
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Player Dead") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Nothing more to do if the player hasn't chosen to Allow Reservation (by choosing a Host/Join option)
	IF NOT (Is_MissionsAtCoords_InCorona_Stage_Allow_Reservation())
		// ... haven't received permission to reserve mission yet
		// Has the player already been dragged on to this mission?
		IF (Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(paramSlot))
			// Change the mission's stage to be 'forcing as focus mission'
			Set_MissionsAtCoords_Stage_For_Mission_To_Waiting_For_On_Mission(paramSlot)
			
			#IF IS_DEBUG_BUILD
				PRINTSTRING("          Player has already been dragged on to this mission and it's already triggering") PRINTNL()
			#ENDIF
			
			RETURN TRUE
		ENDIF
	
		// Check if the player is no longer being controlled by the Corona (but wait for a settle timeout period to ensure the corona has picked up the player)
		IF (Has_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout_Expired())
			IF NOT (Is_External_Corona_Routine_Still_In_Control_Of_Player(paramSlot))
				// ...player is no longer being controlled by the corona
				#IF IS_DEBUG_BUILD
					PRINTSTRING(".KGM [At Coords]: Waiting For Allow Reservation: Settle Timeout Expired - cleaning up.")
					NET_PRINT_TIME()
					PRINTSTRING(". Frame: ")
					PRINTINT(GET_FRAME_COUNT())
					PRINTNL()
				#ENDIF
				
				Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
		
				// Cancel reservation if player was pre-reserved for the mission (ie: by accepting an invite) - there must have been a problem being accepted by the corona (ie: download issue?)
				Unreserve_MissionsAtCoords_Player_From_Any_PreReserved_Mission()
	
				// Clear Invite Accepted
				Clear_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(paramSlot)
					
				// Cleanup corona control variables
				Cleanup_MissionsAtCoords_After_Quit_Corona(paramSlot)

				Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_OUTSIDE_FOCUS_RANGE)
				PRINTSTRING("          Player has quit corona - waiting for player to walk out of triggering range") PRINTNL()
				
				RETURN TRUE
			ENDIF
		ELSE
			// Settle Timeout hasn't expired, but force it as expired once the player is picked up by the corona
			#IF IS_DEBUG_BUILD
				IF NOT (IS_PLAYER_IN_CORONA())
					PRINTSTRING(".KGM [At Coords]:")
					NET_PRINT_TIME()
					PRINTSTRING("Still Waiting for 'Allow Reservation'")
					PRINTNL()
				ENDIF
			#ENDIF
				
			IF (IS_PLAYER_IN_CORONA())
				// Force the settle timeout to expire
				#IF IS_DEBUG_BUILD
					PRINTSTRING(".KGM [At Coords]:")
					NET_PRINT_TIME()
					PRINTSTRING("Force 'Allow Reservation' timeout - Player in corona")
					PRINTNL()
				#ENDIF
	
				Force_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout_Expiry()
			ENDIF
		ENDIF
	
		// Keep drawing the Corona Light until picked up by the corona
		IF (Should_MissionsAtCoords_Mission_Display_A_Corona(paramSlot))
			Draw_Light_At_Corona(Get_MissionsAtCoords_Slot_Corona_Hud_Colour(paramSlot), Get_MissionsAtCoords_Slot_Coords(paramSlot))
		ENDIF
	
		// Keep waiting
		RETURN FALSE
	ENDIF
	
	// Player is being allowed to reserve mission
	IF (g_prepMissionDownloadInProgress)
		PRINTLN(".KGM [At Coords]: Attempt Reservation allowed BUT Prep Mission Download is in progress. DELAY.")
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [At Coords]: Attempt Reservation")
	#ENDIF
	
	// Ensure there is room on the Leader's mission for a non-Leader player being sucked into a Heist Mission in the Leader's apartment
	IF NOT (Is_There_Room_On_Leader_Heist_For_Player_To_Walk_In(paramSlot))
		// There is no room on Leader's Heist for this walk-in player
		Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
		
		// Clear the External InCorona Actions variables
		Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
		
		// Cancel reservation if player was pre-reserved for the mission (ie: by accepting an invite)
		Cancel_Internal_And_External_InCorona_Actions(paramSlot)
	
		// Clear Invite Accepted
		Clear_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(paramSlot)
		
		// NOTE: Need to delay the help message because other parts of the game does a general 'clear help' after walk out that clears the help if displayed here
		PRINTSTRING(".KGM [At Coords][Heist]: Attempt Reservation BUT Inviting Player's Heist is full - setting up a delayed 'mission full' help message") PRINTNL()
		
		// Indicate help is required but add a safety frame counter to quit if necessary
		g_matcHeistFullHelpNeeded = TRUE
		g_matcHeistFullHelpSafetyFrame = GET_FRAME_COUNT() + HEIST_FULL_HELP_MESSAGE_SAFETY_FRAMES
	
		// Change the mission's stage to be 'Force Quit'
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_QUIT_CORONA)
		PRINTSTRING("          Heist Has No Room For Walk-In Player, so walk out and quit corona") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Force the settle timeout to expire
	Force_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout_Expiry()
	
	// Set up the details for the mission
	MP_MISSION_DATA		theMissionData
	BOOL				hasCutscene			= Does_MissionsAtCoords_Mission_Have_Cutscene(paramSlot)
	
	// If the 'invited' flag is set then use the new source: MAP_BUT_NOT_WALK_IN - this will not be blocked as a 'Walk-in' if matchmaking is closed
	//		but will allow a new instance of a mission to start if there isn't a suitable existing instance.
	g_eMPMissionSource	theRequestSource	= g_sAtCoordsMP[paramSlot].matcSourceID
	
	IF (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Using SourceID MAP_BUT_NOT_WALK_IN - a mix between 'mission_flow_blip' and 'invite'") PRINTNL()
		#ENDIF
	
		theRequestSource = MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN
	ENDIF
	
	// This gets set by the Host/Join screen
	//		ownInstance = TRUE means never join a local instance of the mission, always start a new one
	BOOL ownInstance = IS_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA(PLAYER_ID())
	
	#IF IS_DEBUG_BUILD
		IF (ownInstance)
			PRINTLN(".KGM [At Coords]: Hosting Own Instance of corona. IS_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA() returned TRUE")
		ENDIF
	#ENDIF
	
	IF NOT (ownInstance)
		// ...if the player isn't hosting their own instance, check if the mission has requested that it hosts a new instance (initially used by Heist Leaders)
		IF (Is_MissionsAtCoords_Mission_Hosting_Own_Instance(paramSlot))
			IF NOT (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
				ownInstance = TRUE
				
				PRINTLN(".KGM [At Coords]: Hosting Own Instance of corona. The 'Host Own Instance' option flag is set'")
			ELSE
				PRINTLN(".KGM [At Coords]: Request to Host Own Instance of corona because The 'Host Own Instance' option flag is set' - IGNORED FOR PLAY AGAIN")				
			ENDIF
		ENDIF
	ENDIF
	
	// Check if the player is in an existing tutorial session that should restrict the available opponents
	INT tutorialSessionID = Get_Players_Existing_Tutorial_Session()
	
	// ...fill the required mission data
	theMissionData.mdID	= g_sAtCoordsMP[paramSlot].matcMissionIdData
	
	// KGM 1/10/14 [BUG 2061552]: To ensure the player joins the correct instance of a Heist corona
	INT		passPlayerToJoinAsInt	= -1
	BOOL	isInLeaderTransition	= FALSE
	
	IF NOT (ownInstance)
		passPlayerToJoinAsInt = Get_Heist_LeaderAsInt_Of_Heist_Corona_Being_Joined(paramSlot)
		
		// KGM 1/12/14 [BUG 2150045]: If there is a known Heist Leader, then check if this player is already in the Leader's transition session
		//	- this is because JIP players enter the transition session before they register with mission controller and may get rejected for the mission being 'full' when they
		//		are the one of the players registered in a different session
		IF (passPlayerToJoinAsInt != -1)
			IF (NETWORK_IS_IN_TRANSITION())
				PLAYER_INDEX theLeader = INT_TO_PLAYERINDEX(passPlayerToJoinAsInt)
		    	GAMER_HANDLE leaderGH = GET_GAMER_HANDLE_PLAYER(theLeader)
				IF (IS_THIS_GAMER_HANDLE_IN_MY_TRANSITION_SESSION(leaderGH))
					// ...this player is already in the Leader's transition session, so ok to join
					PRINTLN(".KGM [At Coords]: Player is already in Leader's Transition Session, so let Server know")
					isInLeaderTransition = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Broadcast the mission request to the Mission Controller, store the return value which is a unique ID for the request
	g_sAtCoordsFocusMP.focusUniqueID = Broadcast_Reserve_Mission_By_Player(theRequestSource, theMissionData, hasCutscene, ownInstance, DEFAULT, tutorialSessionID, passPlayerToJoinAsInt, isInLeaderTransition)
	
	// Although unlikely, it is possible that the request can return 'NO_UNIQUE_ID' so this needs to be checked
	IF (g_sAtCoordsFocusMP.focusUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Reserving Focus Mission - Failed: NO_UNIQUE_ID returned")
//			PRINTNL()
//			PRINTSTRING("         ")
//			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//			PRINTNL()
			
			SCRIPT_ASSERT("Maintain_MissionsAtCoords_Stage_Waiting_For_Allow_Reservation(): ERROR: Attempting to Reserve Mission, but NO_UNIQUE_ID returned. Mission is not Reserved. Tell Keith.")
		#ENDIF
		
		// Don't allow the Mission Stage to move on
		RETURN FALSE
	ENDIF
	
	// Request successfully issued, need to wait for a reply
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Reserving Focus Mission. UniqueID: ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTNL()
		PRINTSTRING("         ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF
	
	// Set up a safety timeout so we don't wait forever for a response to the reservation broadcast
	Set_MissionsAtCoords_Focus_Mission_Reservation_Safety_Timeout()
	
	// Change the mission's stage to be 'reserving mission'
	Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_RESERVING_MISSION)
	PRINTSTRING("          Moving Closer: Distance: ") PRINTFLOAT(paramDistance) PRINTNL()
	
	RETURN TRUE
		
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still Reserving the Mission and update as appropriate
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	Ignore checking for the player being on Other Activities when the mission is the Focus Mission - feedback from other sources will allow appropriate action to take place
FUNC BOOL Maintain_MissionsAtCoords_Stage_Reserving_Mission(INT paramSlot, FLOAT paramDistance)

	// Check for Reserve Mission Confirmation or Failure
	BOOL confirmationReceived	= Check_For_Reserve_Focus_Mission_Confirmation_And_Update_Focus_Variables()
	BOOL failureReceived		= Check_For_Focus_Mission_Request_Failed()
	BOOL missionFullReceived	= Check_For_Focus_Mission_Full()
	
	// Disable entering vehicles while in the corona but before the corona takes control
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	
	// Nothing more to do if Confirmation or Failure hasn't been received
	IF NOT (confirmationReceived)
	AND NOT (failureReceived)
		// Check if the safety timeout has expired to ensure the game doesn't wait for a broadcast response forever
		IF (Has_MissionsAtCoords_Focus_Mission_Reservation_Safety_Timeout_Expired())
			// ...safety timeout has expired
			Cancel_Focus_Mission_Reservation_And_Clear_Focus_Variables()
			Clear_MissionsAtCoords_Focus_Mission_Reservation_Safety_Timeout()
		
			// Change the mission's stage to be 'Requesting Mission Name'
			// 3/5/14: BUG 1781150 - the reservation safety timeout expired because the host hadn't responded. The safety timeout is 90 seconds, if there is no
			//						reply in this time then don't place in 'requesting mission name' to try reservation again, instead turf the player out with
			//						an error message to prevent the chance of getting stuck.
			Cancel_Internal_And_External_InCorona_Actions(paramSlot)

			Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_QUIT_CORONA)
			PRINTSTRING("          Safety Timeout expired - quit waiting for a response to the reservation broadcast - so walk out") PRINTNL()
			
			RETURN TRUE
		ENDIF
	
		RETURN FALSE
	ENDIF

	// Failure Received, so clear the Focus Variables and move the player back out to the Mission Name stage to allow mission reservation to take place again
	IF (failureReceived)
		// ...it's possible failure was received because the player has already accepted an invite to this mission and was taken on to it during the warp
		IF (Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(paramSlot))
			// Change the mission's stage to be 'forcing as focus mission'
			Set_MissionsAtCoords_Stage_For_Mission_To_Waiting_For_On_Mission(paramSlot)
			
			#IF IS_DEBUG_BUILD
				PRINTSTRING("          Reservation failed, but player has already been dragged on to this mission and it's already triggering") PRINTNL()
			#ENDIF
			
			RETURN TRUE
		ENDIF
		
		// On the off-chance that the failure was because the server mission request array was full (ie: more than 16 players trying to HOST a mission on NextGen in same session), force quit
		// NOTE: KGM 2/5/14: This checks a 'server' function, but if, due to network lag, this player thinks the array isn't full when it is,
		//						the game will just try to reserve the mission again and deal with the array being full on next failure message
		IF (Find_Free_MP_Mission_Request_Slot() = NO_FREE_MISSION_REQUEST_SLOT)
			// ...server's mission request array is full, so walk out
			Cancel_Internal_And_External_InCorona_Actions(paramSlot)

			Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_QUIT_CORONA)
			PRINTSTRING("          Requesting Quit Corona - Mission Request Failed because the server Mission Request array is full, so walk out") PRINTNL()
			RETURN TRUE
		ENDIF
		
		// KGM 13/1/15 [BUG 2143257]: Ensure that a help message gets displayed if the player failed to join the heist because it was 'full'
		// - this is needed if excess players are in the Leader's Planning Room when the Leader sets up the heist from the board
		IF (missionFullReceived)
			MP_MISSION_ID_DATA	theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)

			IF (Is_FM_Cloud_Loaded_Activity_A_Heist(theMissionIdData))
			OR (Is_FM_Cloud_Loaded_Activity_Heist_Planning(theMissionIdData))
				// NOTE: Need to delay the help message because other parts of the game does a general 'clear help' after walk out that clears the help if displayed here
				PRINTSTRING(".KGM [At Coords][Heist]: Inviting Player's Heist is full - setting up a delayed 'mission full' help message") PRINTNL()
				
				// Indicate help is required but add a safety frame counter to quit if necessary
				g_matcHeistFullHelpNeeded = TRUE
				g_matcHeistFullHelpSafetyFrame = GET_FRAME_COUNT() + HEIST_FULL_HELP_MESSAGE_SAFETY_FRAMES
			ENDIF
		ENDIF
	
		Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
	
		// Retry the mission reservation - this doesn't need the corona to change out of the handshake stage and so seems to prevent the issues in bug 1922242
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_ALLOW_RESERVATION)
		PRINTSTRING("          Failed to Reserve Mission, so modifying the mission stage to allow a retry of the reservation attempt") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Check if the player is no longer being controlled by the corona
	IF NOT (Is_External_Corona_Routine_Still_In_Control_Of_Player(paramSlot))
		Cancel_Focus_Mission_Reservation()
				
		// Cleanup corona control variables
		Cleanup_MissionsAtCoords_After_Quit_Corona(paramSlot)

		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_OUTSIDE_FOCUS_RANGE)
		PRINTSTRING("          Player has quit corona - waiting for player to walk out of triggering range") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Check if the mission has been marked for delete, clean up and move back to a previous stage so it can clean up too if necessary
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		// ...Mission has been deleted
		// KGM: 11/6/14: Added this line back in - it was purposely removed in version 39 from here - I don't know why, which makes me nervous.
		// Added this to prevent the player being left reserved for a mission if the mission gets marked for delete as they are getting reserved for it
		//		the player is now forced out of the corona in that situation which wouldn't have been done in version 39, so hopefully that fixes whatever
		//		the reason was that led to this line being removed.
		Cancel_Focus_Mission_Reservation_And_Clear_Focus_Variables()
		
		// Clear the External InCorona Actions variables
		Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
	
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Mission Marked For Delete") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check if the mission should be sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		// ...Mission should be sleeping
		Cancel_Focus_Mission_Reservation_And_Clear_Focus_Variables()
		
		// Clear the External InCorona Actions variables
		Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
	
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Mission Sleeping") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Check if the player is dead, clean up and move back to a previou sstage so it can clean up too if necessary
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
		// ...player is dead
		Cancel_Focus_Mission_Reservation_And_Clear_Focus_Variables()
		
		// Clear the External InCorona Actions variables
		Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
	
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Player Dead") PRINTNL()
		
		RETURN TRUE
	ENDIF	
	
	// Let the external InCorona sequence know the mission has been reserved
	Set_External_InCorona_Actions_To_Player_Reserved()
	
	// Setup the Waiting For Cloud Data safety timeout
	Set_MissionsAtCoords_Focus_Mission_Cloud_Data_Safety_Timeout()
	
	// KGM 22/10/14 [BUG 2008228]: For heist-related missions, make the ped sober
	MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	IF (Is_FM_Cloud_Loaded_Activity_A_Heist(theMissionIdData))
	OR (Is_FM_Cloud_Loaded_Activity_Heist_Planning(theMissionIdData))
		PRINTLN(".KGM [At Coords]: Player reserved for a heist-related mission. Making the ped sober.")
		Force_All_Drunk_Peds_To_Become_Sober_And_Quit_Camera()
	ENDIF

	
	// Change the mission's stage to be 'waiting for cloud data'
	Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_CLOUD_DATA)
	PRINTSTRING("          Distance from Mission Coords: ") PRINTFLOAT(paramDistance) PRINTNL()
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still Waiting For Cloud Data and update as appropriate
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	Ignore checking for the player being on Other Activities when the mission is the Focus Mission - feedback from other sources will allow appropriate action to take place
FUNC BOOL Maintain_MissionsAtCoords_Stage_Waiting_For_Cloud_Data(INT paramSlot, FLOAT paramDistance)

	// Check for the Mission Request being cancelled
	BOOL	failureReceived	= Check_For_Focus_Mission_Request_Failed()
	BOOL	missionFull		= Check_For_Focus_Mission_Full()
	
	// Disable entering vehicles while in the corona but before the corona takes control
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	
	// Check if the player is dead
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
		// ...player is dead
		Cancel_Focus_Mission_Reservation_And_Clear_Focus_Variables()
		
		// Clear the External InCorona Actions variables
		Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
	
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Player Dead") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Failure or Full received, so Mark the player as having to be forced out of the corona when the data has loaded
	IF (failureReceived)
	OR (missionFull)
		g_sAtCoordsFocusMP.focusForceWalkOut = TRUE
	ENDIF

	// If cloud-loaded mission data is required, has it been received?
	// NOTE: Only missions with external InCorona actions will load cloud data for now.
	//		 The External script will mark the InCorona Actions as 'cloud data loaded' when the data is loaded
	BOOL isCloudDataReady = TRUE
	IF (Does_MissionsAtCoords_Use_External_InCorona_Actions(paramSlot))
		isCloudDataReady = Is_MissionsAtCoords_InCorona_Stage_Cloud_Data_Loaded()
	ENDIF
	
	// Has an external script error been received (ie: failed to download)
	BOOL isExternalError = FALSE
	IF (Does_MissionsAtCoords_Use_External_InCorona_Actions(paramSlot))
		isExternalError = Is_MissionsAtCoords_External_Script_Error_Reported()
	ENDIF

	// Is the cloud data ready?
	IF NOT (isCloudDataReady)
	AND NOT (isExternalError)
		// KGM 7/7/13: Check if we've waited long enough - assume an error and try again
		IF (Has_MissionsAtCoords_Focus_Mission_Cloud_Data_Safety_Timeout_Expired())
			// Safety timeout has expired
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Cloud data safety timeout expired.") PRINTNL()
			#ENDIF
			
			Cancel_Focus_Mission_Reservation_And_Clear_Focus_Variables()
			Clear_MissionsAtCoords_Focus_Mission_Cloud_Data_Safety_Timeout()
			
			// Clear the External InCorona Actions variables
			Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
		
			// Change the mission's stage to be 'Requesting Mission Name'
			Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
			PRINTSTRING("          Cloud Data Safety Timeout expired - try again") PRINTNL()
			
			RETURN TRUE
		ENDIF
	
		// KGM 12/2/13: No Longer Check if the mission is marked for delete while Cloud Loading is going on - this will be checked once the load has completed
		#IF IS_DEBUG_BUILD
			IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
				PRINTSTRING(".KGM [At Coords]: Focus Mission Marked For Delete. Ignoring until mission load complete.") PRINTNL()
			ENDIF
		#ENDIF
		
		// KGM 12/2/13: No Longer Check if the mission is Sleeping while Cloud Loading is going on - this will be checked once the load has completed
		#IF IS_DEBUG_BUILD
			IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
				PRINTSTRING(".KGM [At Coords]: Focus Mission set to Sleeping. Ignoring until mission load complete.") PRINTNL()
			ENDIF
		#ENDIF
		
		// Mission is not marked for deletion, so maintain any InCorona Actions and continue waiting
		Maintain_InCorona_Actions(paramSlot)
		
		// KGM 3/3/13: Check if the mission details on the Mission Controller have changed (this can happen with FM Heists where the player can choose which Heist to play)
		Maintain_Focus_Mission_Details(paramSlot)
		
		RETURN FALSE
	ENDIF
	
	// Cloud data, if needed, is ready
	#IF IS_DEBUG_BUILD
		// NOTE: This can still be reported as TRUE when an External Error has been reported to allow stage advancement - the external error will get dealt with as appropriate in the next stage
		IF (isCloudDataReady)
			PRINTSTRING(".KGM [At Coords]: Cloud data reported as ready. Moving to Start Focus Mission stage. UniqueID: ")
			PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
			PRINTNL()
			PRINTSTRING("         ")
			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
			PRINTNL()
		ENDIF
		
		IF (isExternalError)
			PRINTSTRING(".KGM [At Coords]: External Script has reported an error. This may be due to a cloud data download error. UniqueID: ")
			PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
			PRINTNL()
			PRINTSTRING("         ")
			Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
			PRINTNL()
		ENDIF
	#ENDIF
	
	// KGM 20/11/14 [BUG 2139776]: If this mission has been Quick Restarted and it's a Strand Mission Part Two, then it may need to update the mission data subtype
	IF (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
		MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
		IF (Is_FM_Cloud_Loaded_Activity_A_Heist(theMissionIdData))
		OR (Is_FM_Cloud_Loaded_Activity_Heist_Planning(theMissionIdData))
			IF (HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
				IF (g_FMMC_STRUCT.iMissionType != g_sMDStrandMissionsMP.dfsmFmmcType)
				OR (g_FMMC_STRUCT.iMissionSubType != g_sMDStrandMissionsMP.dfsmSubtype)
					// Update the part two mission details to be the same as part one - this is because A Finale Part2 is tagged as 'Planning' so it only gets downloaded when needed
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [At Coords]: Quick Restarting Part 2. Setting Part2 to same Type and Subtype as Part 1")
						PRINTLN(".KGM [At Coords]: From Type: ", g_FMMC_STRUCT.iMissionType, ". Subtype: ", g_FMMC_STRUCT.iMissionSubType)
						PRINTLN(".KGM [At Coords]: To   Type: ", g_sMDStrandMissionsMP.dfsmFmmcType, ". Subtype: ", g_sMDStrandMissionsMP.dfsmSubtype)
					#ENDIF
			
					g_FMMC_STRUCT.iMissionType		= g_sMDStrandMissionsMP.dfsmFmmcType
					g_FMMC_STRUCT.iMissionSubType	= g_sMDStrandMissionsMP.dfsmSubtype
					
					// Also ensure this gets classed as a STRAND MISSION again
					IF NOT (IS_THIS_IS_A_STRAND_MISSION())
						PRINTLN(".KGM [At Coords]: Quick Restarting Part 2. Ensuring mission is classed as a Strand Mission again.")
				
						SET_THIS_IS_A_STRAND_MISSION()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// When the cloud data is ready, clear the 'force quit' flag. If the cloud data gets loaded even though this flag is set then the FMMC_Launcher must have ignored the 'walk out of corona'
	//		check that occurs prior to loading. The tutorials ignore that check. I need to clear it here because the flag can also force a walk out if the mission request fails.
	Clear_MissionsAtCoords_InCorona_Actions_Force_Quit()
	
	// Although the cloud data is ready, there is a brief pause before the player is warped to the edge of the corona, so this timeout will cover that
	// NOTE: Without this, it's possible for the player to run out of the corona and for it to cleanup briefly before the player is warped back to the edge setting everything up again
	Set_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout()
	
	// If the mission needs to share its cloud-loaded header data with all players, then do that here
	Store_Shared_Missions_Data_For_Slot_If_Applicable(paramSlot)

	// Change the mission's stage to be 'Waiting To Start'
	Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_TO_START)
	PRINTSTRING("          Distance: ") PRINTFLOAT(paramDistance) PRINTNL()
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still Waiting For Start and update as appropriate
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	Ignore checking for the player being on Other Activities when the mission is the Focus Mission - feedback from other sources will allow appropriate action to take place
FUNC BOOL Maintain_MissionsAtCoords_Stage_Waiting_To_Start(INT paramSlot, FLOAT paramDistance)

	// Check for the Mission Request being cancelled
	BOOL	failureReceived	= Check_For_Focus_Mission_Request_Failed()
	BOOL	missionFull		= Check_For_Focus_Mission_Full()

	// Failure or Full received, so clear the Focus Variables, cleanup the InCorona Actions, and move the player back out to the Mission Name stage to allow mission reservation to take place again
	IF (failureReceived)
	OR (missionFull)
		// Cleanup variables and Force Quit the Corona
		Cancel_Internal_And_External_InCorona_Actions(paramSlot)
	
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_QUIT_CORONA)
		PRINTSTRING("          Requesting Quit Corona - Mission Request Failed or Mission was full") PRINTNL()
		
		RETURN TRUE
	ENDIF

	// Check for the activity starting
	BOOL isActivityReadyToStart = Is_MissionsAtCoords_Activity_Ready_To_Start(paramSlot)
	
	// Check for an external error being reported and clear the flag
	BOOL isExternalError = FALSE
	IF (Does_MissionsAtCoords_Use_External_InCorona_Actions(paramSlot))
		isExternalError = Is_MissionsAtCoords_External_Script_Error_Reported()
		
		IF (isExternalError)
			PRINTLN(".KGM [At Coords]: Maintain_MissionsAtCoords_Stage_Waiting_To_Start() - An external script has reported an error, so this stage should cleanup soon")
			IF NOT (Has_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout_Expired())
				Force_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout_Expiry()
				#IF IS_DEBUG_BUILD
					PRINTSTRING("         BECAUSE: External Script has reported an error - so cleaning up") PRINTNL()
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Is the activity ready to start?
	IF NOT (isActivityReadyToStart)
		// ...sometimes a player in a group vote can miss out on being told the vote has passed, so check if the player is now on or triggering this mission
		IF (Is_MatC_Player_On_Or_Triggering_A_Mission_With_This_UniqueID(g_sAtCoordsFocusMP.focusUniqueID))
			// ...player is on or triggering this mission, vote must have been missed, so cleanup the vote and move on
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Activity isn't classed as ready to start, but already on or triggering mission. Treat as Ready To Start. UniqueID: ")
				PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
				PRINTNL()
				PRINTSTRING("         ")
				Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
				PRINTNL()
			#ENDIF
		ELSE
			// ...activity isn't ready to start, so check if the player has quit the corona processing routines
			// NOTE: Don't do any of these checks until the brief settling down period is over
			//			- this is to cover a brief delay between the Mission Data being loaded and the player being warped to the corona edge
			IF (Has_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout_Expired())
				// Check if the player has quit the corona
				IF NOT (Is_External_Corona_Routine_Still_In_Control_Of_Player(paramSlot))
					// ...player has quit the corona, so cancel Mission Reservation - don't clear the focus variables yet, it'll get done in the function below
					#IF IS_DEBUG_BUILD
						PRINTSTRING(".KGM [At Coords]: Player not processing external corona routines. Must have quit corona. UniqueID: ")
						PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
						PRINTNL()
//						PRINTSTRING("         ")
//						Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
//						PRINTNL()
					#ENDIF
					
					Cancel_Focus_Mission_Reservation()
					
					// Cleanup corona control variables
					Cleanup_MissionsAtCoords_After_Quit_Corona(paramSlot)
	
					// 24/7/14: To allow a Heist Planning mission to be chosen while still in the Heist Intro cutscene Activity Session, need to delete the Heist Cutscene Corona
					IF (NETWORK_IS_ACTIVITY_SESSION())
						IF (Is_MissionsAtCoords_Mission_Only_For_Cutscene_Triggering(paramSlot))
							// This is an Intro Cutscene Corona being cleaned up while still in a Transition Session, so mark the corona for delete
							PRINTLN(".KGM [At Coords][Heist][HCorona): Trying to cleanup a Cutscene Corona (presumably for Heists) while still in a Transition Session - Mark Corona for delete")
							Mark_MissionsAtCoords_Mission_For_Delete(paramSlot)
						ENDIF
					ENDIF	
	
					// KGM 14/6/14: If the mission has been marked for delete then don't wait for the player to be outside focus range
					// NOTE: This was added for quitting a Heist corona when it gets deleted after Focus
					IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
						Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
						PRINTSTRING("          Player quit corona and mission marked for delete") PRINTNL()
					ELSE
						Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_OUTSIDE_FOCUS_RANGE)
						PRINTSTRING("          Player quit corona - waiting for Walk Out") PRINTNL()
					ENDIF
	
					RETURN TRUE
				ENDIF
				
				// Player is still processing the corona routines, so check if the mission is marked for deletion
				IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
					// ...mission is Marked For Delete, so clean up this stage and move back to the previous stage so it can clean up too
					// Unregister this player from this shared mission data (if applicable)
					Cleanup_Shared_Missions_Data_For_Slot(paramSlot)
		
					Cancel_Focus_Mission_Reservation_And_Clear_Focus_Variables()
					
					// This will force the player to walk out of an external corona, if they aren't already
					Cancel_Internal_And_External_InCorona_Actions(paramSlot)
				
					// Wait for the player to be out of the corona, so the incorona variables get cleaned up properly (this is to fix bug 1650573)
					// KGM 15/1/15: If an external error has been reported than backtrack to allow corona delete immediately
					IF (isExternalError)
						Clear_MissionsAtCoords_External_Error()
						Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
						PRINTSTRING("          External Script Error (probably data download problem) - Mission already marked For Delete so allow Delete now") PRINTNL()
					ELSE
						Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_OUTSIDE_FOCUS_RANGE)
						PRINTSTRING("          Requesting Walk Out, so still waiting - Mission marked For Delete") PRINTNL()
					ENDIF
					
					RETURN TRUE
				ENDIF
				
				// Ignore sleeping missions at this stage - this may be a mistake but trying to clean it up may lead to more problems

				// The Focus Mission has been marked for walk out, so there must have been a problem during the cloud loading phase
				IF (g_sAtCoordsFocusMP.focusForceWalkOut)
					Cancel_Internal_And_External_InCorona_Actions(paramSlot)
				
					Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_QUIT_CORONA)
					PRINTSTRING("          Requesting Walk Out - Probably problem while cloud data loading") PRINTNL()
					
					RETURN TRUE
				ENDIF
			ELSE
				// InCorona Settle Timeout hasn't expired
				#IF IS_DEBUG_BUILD
					PRINTSTRING(".KGM [At Coords]: InCorona Settle timeout hasn't expired. Not yet checking for player out of corona, etc.") PRINTNL()
					IF (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
						IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
							PRINTSTRING("         Player Position     :") PRINTVECTOR(GET_PLAYER_COORDS(PLAYER_ID())) PRINTNL()
							PRINTSTRING("         Distance From Centre: ") PRINTFLOAT(paramDistance) PRINTNL()
							PRINTSTRING("         Focus Mission Range : ") PRINTFLOAT(Get_MissionsAtCoords_Inner_Range_Boundary(MATCB_FOCUS_MISSION, paramSlot)) PRINTNL()
						ENDIF
					ENDIF
				#ENDIF
				
				// Disable entering vehicles while in the corona but before the corona takes control
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				
				// Force Timer Expity if player control has been removed
				IF NOT (IS_PLAYER_CONTROL_ON(PLAYER_ID()))
					Force_MissionsAtCoords_InCorona_Focus_Mission_Settle_Timeout_Expiry()
					
					#IF IS_DEBUG_BUILD
						PRINTSTRING("         BECAUSE: Player control OFF, assume at corona edge") PRINTNL()
//						IF (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
//							IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
//								PRINTSTRING("         Player Position     :") PRINTVECTOR(GET_PLAYER_COORDS(PLAYER_ID())) PRINTNL()
//								PRINTSTRING("         Distance From Centre: ") PRINTFLOAT(paramDistance) PRINTNL()
//								PRINTSTRING("         Focus Mission Range : ") PRINTFLOAT(Get_MissionsAtCoords_Inner_Range_Boundary(MATCB_FOCUS_MISSION, paramSlot)) PRINTNL()
//							ENDIF
//						ENDIF
					#ENDIF
				ENDIF
			ENDIF
			
			// Mission is not marked for deletion, so maintain any InCorona Actions and continue waiting
			Maintain_InCorona_Actions(paramSlot)
			
			// Ensure the Registration on the Shared Mission has succeeded - re-issue if not (this is in case the original request got dumped due to host migration or similar)
			Maintain_Attempt_Registration_With_Shared_Mission(paramSlot)
		
			// KGM 3/3/13: Check if the mission details on the Mission Controller have changed (this can happen with FM Heists where the player can choose which Heist to play)
			Maintain_Focus_Mission_Details(paramSlot)
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Mission is Ready To Start
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Start Focus Mission. UniqueID: ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTNL()
		PRINTSTRING("         ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF
	
	// Stop Processing Internal InCorona Actions (ignore external action - they will cleanup at an appropriate time determined by the external routines)
	Cleanup_Internal_InCorona_Actions(paramSlot)
				
	IF NOT (Broadcast_Start_Reserved_Mission(g_sAtCoordsFocusMP.focusUniqueID, g_sAtCoordsFocusMP.focusBitsPlayers))
		// The Broadcast function failed, so there must be a data error somewhere (there will be more details and assert from within the broadcast function)
		PRINTSTRING(".KGM [At Coords]: Problem starting reserved mission. See Console Log. Clearing the Focus Mission.") PRINTNL()
		
		// Unregister this player from this shared mission data (if applicable)
		Cleanup_Shared_Missions_Data_For_Slot(paramSlot)
			
		// Delete this mission if it should be deleted after being the Focus mission
		IF (Should_MissionsAtCoords_Mission_Be_Deleted_After_Focus(paramSlot))
			Mark_MissionsAtCoords_Mission_For_Delete(paramSlot)
		ENDIF
		
		// Clear the flag if it was the currently active playlist activity
		Clear_MissionsAtCoords_Currently_Active_Playlist_Activity(paramSlot)
	
		// Clear the Invite Accepted flag
		Clear_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(paramSlot)
		
		// Cancel Mission Reservation
		Cancel_Focus_Mission_Reservation_And_Clear_Focus_Variables()
		
		// Clear the External InCorona Actions variables
		Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
	
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          The 'Start Mission' broadcast returned FALSE") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Change the mission's stage to be 'Mission Starting'
	Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_MISSION_STARTING)
	PRINTSTRING("          Distance: ") PRINTFLOAT(paramDistance) PRINTNL()
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still Waiting for Confirmation of the Mission Starting and update as appropriate
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	Ignore missions marked for delete in here - we're too far into the mission triggering routines to delete it now
// NOTES:	Ignore checking for the player being on Other Activities when the mission is the Focus Mission - feedback from other sources will allow appropriate action to take place
FUNC BOOL Maintain_MissionsAtCoords_Stage_Mission_Starting(INT paramSlot)

	// Check for Mission Start Confirmation or Refusal
	BOOL playerOnMission		= Check_For_Player_On_Focus_Mission_Confirmation()
	BOOL failureReceived		= Check_For_Focus_Mission_Request_Failed()
	BOOL missionFullReceived	= Check_For_Focus_Mission_Full()
	
	// Keep waiting if no reply has been received
	IF NOT (playerOnMission)
	AND NOT (failureReceived)
	AND NOT (missionFullReceived)
		// KGM 3/3/13: Check if the mission details on the Mission Controller have changed (this can happen with FM Heists where the player can choose which Heist to play)
		Maintain_Focus_Mission_Details(paramSlot)
		
		RETURN FALSE
	ENDIF

	// Failure or Full received, so move the player back out to the Mission Name stage to allow mission reservation to take place again
	IF (failureReceived)
	OR (missionFullReceived)
		// Cleanup variables and Force Quit the Corona
		Cancel_Internal_And_External_InCorona_Actions(paramSlot)
		
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_QUIT_CORONA)
		PRINTSTRING("          Requesting Quit Corona - Mission Request Failed or Mission was full") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Player is now on the mission
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: On Focus Mission. UniqueID: ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTNL()
		PRINTSTRING("         ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF
	
	// Clean up the speedzone and scenario blocking to free them up for other parts of the game
	Remove_MissionsAtCoords_SpeedZone(paramSlot)
	Remove_MissionsAtCoords_ScenarioBlocking(paramSlot)
	
	// This mission is now active
	Update_MissionsAtCoords_Slot_To_Be_On_Mission(paramSlot)
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The player has been dragged onto this mission but isn't yet actively on the mission (so a pre-mission phonecall or cutscene is active)
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	Ignore missions marked for delete in here - we're too far into the mission triggering routines to delete it now
// NOTES:	Ignore checking for the player being on Other Activities when the mission is the Focus Mission - feedback from other sources will allow appropriate action to take place
FUNC BOOL Maintain_MissionsAtCoords_Stage_Waiting_For_On_Mission(INT paramSlot)

	// Is the player still on or triggering this mission?
	IF NOT (Is_MatC_Player_On_Or_Triggering_This_MissionsAtCoords_Slot_Mission(paramSlot))
		// ...no, so change the stage to be 'requesting mission name' again so this mission can find its correct stage based on distance
		Cleanup_MissionsAtCoords_After_Quit_Corona(paramSlot)
	
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Player was being dragged onto a mission without using a corona, but player is no longer on or triggering the mission") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// The player is still on or triggering this mission, so check if the player is now actively on the mission
	IF NOT (Is_MatC_Player_Actively_On_A_Mission_With_This_UniqueID(g_sAtCoordsFocusMP.focusUniqueID))
		// ...the player is still not actively on the mission
		RETURN FALSE
	ENDIF
	
	// Player is now on the mission
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Actively on Focus Mission without using corona. UniqueID: ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTNL()
		PRINTSTRING("         ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF
	
	// Set the flag that indicates the player was forced onto the mission - this will be used when the mission ends to ensure the mission doesn't immediately restart
	//		as the triggering variables are cleaning up after Mission Controller has already broadcast 'mission complete'
	Set_Player_Forced_Onto_MissionsAtCoords_Mission(paramSlot)
	
	// This mission is now active
	Update_MissionsAtCoords_Slot_To_Be_On_Mission(paramSlot)
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Waiting for the player to be out of the corona after being forced to quit the corona
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	This is used when the game knows the player needs to quit the corona but the external InCorona processing routines are still active
//			Ignore missions marked for delete in here - we're too far into the mission triggering routines to delete it now
//			Ignore checking for the player being on Other Activities when the mission is the Focus Mission - feedback from other sources will allow appropriate action to take place
FUNC BOOL Maintain_MissionsAtCoords_Stage_Waiting_For_Quit_Corona(INT paramSlot)
	
	// Go back to requesting mission name stage immediately if the InCorona Actions for this mission are not being handled by an external routine
	IF NOT (Does_MissionsAtCoords_Use_External_InCorona_Actions(paramSlot))
		Cleanup_MissionsAtCoords_After_Quit_Corona(paramSlot)
		
		Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_OUTSIDE_FOCUS_RANGE)
		PRINTSTRING("          InCorona Actions are not being handled by an External Routine, so wait for the player to leave the focus mission triggering range") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Wait for the player to have quit the external InCorona processing routines
	IF (Is_External_Corona_Routine_Still_In_Control_Of_Player(paramSlot))
		// Player is still processing the External InCorona sequence
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [At Coords]: Still Quitting External InCorona routines.")
		#ENDIF
	
		// Disable entering vehicles while still in the corona
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		
		RETURN FALSE
	ENDIF
	
	// Player is no longer processing the corona routines, so cleanup and wait for the player to be outside the triggering radius
	// KGM 27/1/15 [BUG 211633]: The player could still be reserved for a mission (ie: after accepting an invite), so unreserve the player here.
	Unreserve_MissionsAtCoords_Player_From_Any_PreReserved_Mission()
	Cleanup_MissionsAtCoords_After_Quit_Corona(paramSlot)
		
	Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_WAITING_FOR_OUTSIDE_FOCUS_RANGE)
//	PRINTSTRING("          InCorona Actions are not being handled by an External Routine, so wait for the player to leave the focus mission triggering range") PRINTNL()
		
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Waiting for the player to be out of the focus mission triggering range
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
//							paramDistance		The distance between the player and the mission coords
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	This is used to ensure the player is back outside the focus mission triggering range before allowing the corona to re-activate
//			Ignore missions marked for delete in here - we're too far into the mission triggering routines to delete it now
//			Ignore checking for the player being on Other Activities when the mission is the Focus Mission - feedback from other sources will allow appropriate action to take place
FUNC BOOL Maintain_MissionsAtCoords_Stage_Waiting_For_Outside_Focus_Range(INT paramSlot, FLOAT paramDistance)

	PRINTLN(".KGM [At Coords]: Player waiting to be outside Focus Range. paramSlot: ", paramSlot, "; paramDistance: ", paramDistance)
	BOOL bCanProgress
	
	IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
	OR GANGOPS_FLOW_IS_A_GANGOPS_FINALE_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
			PRINTLN(".KGM [JobList][Heist]: Maintain_MissionsAtCoords_Stage_Waiting_For_Outside_Focus_Range - Mission is gangops flow or gangops finale and player is inside defunct base, return true")
			bCanProgress = TRUE
		ENDIF
	ENDIF
	
	IF IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		OR (NETWORK_IS_ACTIVITY_SESSION() 
		AND IS_A_STRAND_MISSION_BEING_INITIALISED())
			PRINTLN(".KGM [JobList][Heist]: Maintain_MissionsAtCoords_Stage_Waiting_For_Outside_Focus_Range - Mission is casino heist finale and player is inside arcade property base, return true")
			bCanProgress = TRUE
		ENDIF
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		OR (NETWORK_IS_ACTIVITY_SESSION() 
		AND IS_A_STRAND_MISSION_BEING_INITIALISED())
			PRINTLN(".KGM [JobList][Heist]: Maintain_MissionsAtCoords_Stage_Waiting_For_Outside_Focus_Range - Mission is island heist finale and player is inside submarine property, return true")
			bCanProgress = TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	// Go back to requesting mission name stage if the player is now back outside the Focus Mission range
	// NOTE: Check if NOT INSIDE Focus Range here rather than OUTSIDE Focus Range (because 'outside' adds on a bit of leeway)
	IF NOT (Is_Inside_MissionsAtCoords_Range_Boundary(MATCB_FOCUS_MISSION, paramSlot, paramDistance))
	OR bCanProgress
		IF (Is_External_Corona_Routine_Still_In_Control_Of_Player(paramSlot))
			// Player is still processing the External InCorona sequence
			#IF IS_DEBUG_BUILD
//				PRINTSTRING(".KGM [At Coords]: Player is back outside the corona range, so External InCorona Processing shouldn't be going on, but it is.") PRINTNL()
				SCRIPT_ASSERT("Maintain_MissionsAtCoords_Stage_Waiting_For_Outside_Focus_Range(): ERROR - Player shouldn't be processing external Corona routines.")
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		// Clear the External InCorona Actions variables
		// NOTE: This was added to help fix 1650573 by tidying up to doprocessing variable after the player was out of the corona
		Clear_MissionsAtCoords_InCorona_Actions_Variables()
		
		// KGM 13/2/15 [BUG 2236051]: Clear the cross-session invite into Heist flag if the player is walking out of the corona to ensure the flag doesn't get left TRUE
		g_sPlayerAcceptedCrossSessionInviteToHeist = FALSE
		PRINTLN(".KGM [JobList][Heist]: g_sPlayerAcceptedCrossSessionInviteToHeist set to FALSE - walk out of a corona")
			
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Back outside Focus Mission Range") PRINTNL()
		PRINTSTRING("          Distance: ") PRINTFLOAT(paramDistance) PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// KGM 4/11/14 [BUG 2100109]: Player is still inside the focus range, but if the corona routines are no longer processing and the mission is marked for delete, cleanup
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		IF NOT (Is_External_Corona_Routine_Still_In_Control_Of_Player(paramSlot))
			// Cleanup
			// ...calling this - see above for reason
			Clear_MissionsAtCoords_InCorona_Actions_Variables()
		
			// KGM 13/2/15 [BUG 2236051]: Clear the cross-session invite into Heist flag if the player is walking out of a corona marked for delete to ensure the flag doesn't get left TRUE
			g_sPlayerAcceptedCrossSessionInviteToHeist = FALSE
			PRINTLN(".KGM [JobList][Heist]: g_sPlayerAcceptedCrossSessionInviteToHeist set to FALSE - Walking out of corona marked for delete")
				
			Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
			PRINTSTRING("          Marked for Delete and External Corona routines are no longer processing") PRINTNL()
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Disable entering vehicles while still in the corona
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the download of the cloud mission data for the Quick Launch of the mission has completed
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	Once the download has started, let it finish
FUNC BOOL Maintain_MissionsAtCoords_Stage_Download_For_Quick_Launch(INT paramSlot)

	// Only download if the mission uses cloud-loaded data
	MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	
	IF (Does_MP_Mission_Variation_Get_Data_From_The_Cloud(theMissionIdData.idMission, theMissionIdData.idVariation))
		// ...load the Mission Data from the cloud
		// Safety Check - relaunch the mission loading script if it isn't launched (this should never be the case)
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("net_cloud_mission_loader")) = 0)
			REQUEST_SCRIPT("net_cloud_mission_loader")
		    IF (HAS_SCRIPT_LOADED("net_cloud_mission_loader"))
				START_NEW_SCRIPT("net_cloud_mission_loader", FRIEND_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED("net_cloud_mission_loader")
			ELSE
				#IF IS_DEBUG_BUILD
		        	PRINTSTRING(".KGM [At Coords]: Launching net_cloud_mission_loader") PRINTNL()
				#ENDIF
					
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Load the mission data from the cloud
		TEXT_LABEL_31	ignoreOfflineContentID	= ""
		BOOL			useOfflineContentID		= FALSE
		
		IF NOT (Load_Cloud_Loaded_Mission_Data(theMissionIdData, ignoreOfflineContentID, useOfflineContentID))
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Waiting for Cloud-Loaded Mission Data") PRINTNL()
			#ENDIF
				
			RETURN FALSE
		ENDIF
		
		// Cloud-Load complete
		// Check if it downloaded successfully
		IF NOT (Did_Cloud_Loaded_Mission_Data_Load_Successfully())
			// ...mission failed to download the mission data correctly, so clean up this stage and move back to the previous stage
			Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
		
			Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
			PRINTSTRING("          Cloud-Based Mission Data Failed To Download") PRINTNL()
			
			RETURN TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Cloud-Loaded Mission Data available.") PRINTNL()
		#ENDIF
		
		// KGM 12/5/13: For leaderboards and mission balancing to work properly, need to update some variables
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen	= 0
		PRINTLN("Maintain_MissionsAtCoords_Stage_Download_For_Quick_Launch - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen	= 0")
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName	= theMissionIdData.idCloudFilename
	ENDIF
	
	// Check if the mission been marked for delete
	IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
		// ...mission is Marked For Delete, so clean up this stage and move back to the previous stage so it can clean up too
		Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
	
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Mission marked For Delete") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Check if the mission is sleeping
	IF (Should_MissionsAtCoords_Mission_Be_Sleeping(paramSlot))
		// ...mission is Sleeping
		Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
	
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Mission Sleeping") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Check if the player is dead
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
		// ...player is dead
		Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
	
		// Change the mission's stage to be 'Requesting Mission Name'
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Player Dead") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// For Area-Triggered activities, don't trigger the mission after download if the player has moved out of the area
	// KGM 1/7/15 [BUG 2370916]: Also don;t trigger if the player is now on an FM Event
	IF (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
		IF NOT (Is_Player_Inside_MissionAtCoords_Angled_Area(paramSlot))
			// ...player is no longer inside the are, so don't trigger the mission
			Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
		
			// Change the mission's stage to be 'Requesting Mission Name'
			Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
			PRINTSTRING("          Download finished, but Player out of area") PRINTNL()
			
			RETURN TRUE
		ENDIF
		
		// KGM 1/7/15 [BUG 2370916]: Don't trigger if the player is on an FM Event
		IF (FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID()))
			// ...player is currently on an FM Event, so maintain the lock status of the mission then stay here
			Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
		
			// Change the mission's stage to be 'Requesting Mission Name'
			Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
			PRINTSTRING("          Download finished, but Player is now on an FM Event") PRINTNL()
			
			RETURN TRUE
		ENDIF
	ENDIF

	// Broadcast the immediate launch mission request to the Mission Controller, store the return value which is a unique ID for the request
	// NOTE: Player is requesting the mission as an individual trying to start the mission, not as a script host.
	// NOTE: Ignore the optional 'team' parameter - this is used by script hosts trying to ensure a specific team is reserved for the mission.
	MP_MISSION_DATA theMissionData
	theMissionData.mdID = theMissionIdData
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Quick Launch mission.") PRINTNL()
	#ENDIF
	
	g_sAtCoordsFocusMP.focusUniqueID = Broadcast_Request_New_Specific_Mission(FALSE, MP_MISSION_SOURCE_MISSION_FLOW_AUTO, theMissionData)
			
	// Get rid of the phone
	HANG_UP_AND_PUT_AWAY_PHONE()
		
	Set_MissionsAtCoords_Stage_For_Mission(paramSlot, MATCS_MISSION_QUICK_LAUNCHING)
	PRINTSTRING("          Mission Downloaded") PRINTNL()
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission controller has responded to the Quick Launch request
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
//
// NOTES:	A Quick Launch is when the player is put directly onto the mission without first being reserved for it.
FUNC BOOL Maintain_MissionsAtCoords_Stage_Mission_Quick_Launching(INT paramSlot)

	// The unique ID may have changed
	INT tempUniqueID = Get_MP_Mission_Request_Changed_UniqueID(g_sAtCoordsFocusMP.focusUniqueID)
						
	// Store the modified uniqueID
	IF NOT (tempUniqueID = g_sAtCoordsFocusMP.focusUniqueID)
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: UniqueID changed. oldUniqueID = ")
			PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
			PRINTSTRING(". newUniqueID = ")
			PRINTINT(tempUniqueID)
			PRINTNL()
		#ENDIF
	
		g_sAtCoordsFocusMP.focusUniqueID = tempUniqueID
	ENDIF
	
	// Check for Mission Start Confirmation or Refusal
	BOOL playerOnMission		= Check_For_Player_On_Focus_Mission_Confirmation()
	BOOL failureReceived		= Check_For_Focus_Mission_Request_Failed()
	BOOL missionFullReceived	= Check_For_Focus_Mission_Full()
	BOOL missionSomeHowFinished	= Check_For_Focus_Mission_Finished()
	
	// Keep waiting if no reply has been received
	IF NOT (playerOnMission)
	AND NOT (failureReceived)
	AND NOT (missionFullReceived)
		IF !missionSomeHowFinished
		OR g_sMPTunables.bmissionSomeHowFinished_disable
			// Check if the mission details on the Mission Controller have changed
			Maintain_Focus_Mission_Details(paramSlot)
			
			RETURN FALSE
		ENDIF
	ENDIF

	// Failure or Full received, so move the player back out to the Mission Name stage to allow mission reservation to take place again
	IF !g_sMPTunables.bmissionSomeHowFinished_disable
		IF missionSomeHowFinished
			Cancel_Internal_And_External_InCorona_Actions(paramSlot)
			
			Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
			
			Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
			PRINTSTRING("          Mission Request Failed or Mission has finished") PRINTNL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Failure or Full received, so move the player back out to the Mission Name stage to allow mission reservation to take place again
	IF (failureReceived)
	OR (missionFullReceived)
		
		Cancel_Internal_And_External_InCorona_Actions(paramSlot)
		
		Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
		
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          Mission Request Failed or Mission was full") PRINTNL()
		
		RETURN TRUE
	ENDIF
	
	// Player is now on the mission
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: On Focus Mission. UniqueID: ")
		PRINTINT(g_sAtCoordsFocusMP.focusUniqueID)
		PRINTNL()
		PRINTSTRING("         ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		PRINTNL()
	#ENDIF
	
	// Clean up the speedzone and scenario blocking to free them up for other parts of the game
	Remove_MissionsAtCoords_SpeedZone(paramSlot)
	Remove_MissionsAtCoords_ScenarioBlocking(paramSlot)
	
	// If the mission was cloud-loaded, need to set a couple of player broadcast variables needed by other parts of the game
	MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = Convert_MissionID_To_FM_Mission_Type(theMissionIdData.idMission)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl23CurrentMissionOwner = "Rockstar"
	
	// Share the mission data if required
	Store_Shared_Missions_Data_For_Slot_If_Applicable(paramSlot)
	
	// This mission is now active
	Update_MissionsAtCoords_Slot_To_Be_On_Mission(paramSlot)
	
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the mission is still On Mission and update as appropriate
//
// INPUT PARAMS:			paramSlot			The array position of the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if the mission has moved to a different Stage, otherwise FALSE
FUNC BOOL Maintain_MissionsAtCoords_Stage_On_Mission(INT paramSlot)

	// Check for Mission Finished
	BOOL missionFinished = Check_For_Focus_Mission_Finished()
	
	// Nothing more to do if Finished hasn't been received
	IF NOT (missionFinished)
		RETURN FALSE
	ENDIF
	
	// MISSION IS FINISHED
	IF (Is_Copy_Of_Shared_Mission_Being_Played_Again(paramSlot))
		Set_MissionsAtCoords_Mission_As_Being_Played_Again(paramSlot)
	ENDIF
		
	// Clear the Cleared Wanted Level values
	Clear_MissionsAtCoords_Cleared_Wanted_Level()
	
	// Unregister this player from this shared mission data (if applicable)
	BOOL keepGeneratedData = Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot)
	Cleanup_Shared_Missions_Data_For_Slot(paramSlot, keepGeneratedData)
		
	// Clean Up the Focus Variables
	Clear_MP_Mission_At_Coords_Focus_Mission_Variables()
	
	// Clear the InCorona Actions external control variables
	Clear_MissionsAtCoords_InCorona_Actions_Variables(TRUE)
	
	// Inform the requesting function that the mission is finished
	Add_MissionsAtCoords_Feedback(paramSlot, MATCFBT_FINISHED)
		
	// As a one-off, update the lock status of the mission just in case the mission was temporarily unlocked to prevent the blip re-appearing for this player
	// NOTE: Since the mission has been played, always clear the 'temporarily unlocked' flags starting with the 'until played' flag
	// NOTE: 13/5/13: If the player chose to play again, need to leave the temporary unlock in place in case the player is too low a rank to play the mission again
	Clear_MissionsAtCoords_Mission_As_Temporarily_Unlocked_Until_Played(paramSlot)
	Try_To_Set_MissionsAtCoords_Mission_As_Not_Temporarily_Unlocked(paramSlot)
	Maintain_MissionsAtCoords_Lock_Status_For_Mission(paramSlot)
	
	// Mark the finished mission for deletion after play if required, unless marked for 'play again'
	IF (Should_MissionsAtCoords_Mission_Be_Deleted_After_Play(paramSlot))
	OR (Should_MissionsAtCoords_Mission_Be_Deleted_After_Focus(paramSlot))
		IF NOT (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
			Mark_MissionsAtCoords_Mission_For_Delete(paramSlot)
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [At Coords]: Mission should get deleted after Play, but retain to 'Play Again'")
			#ENDIF
				
			// Ensure the Mission gets 'unmarked' for delete, but also set as 'delete after focus' so it tidy's up immediately afterwards
			IF (Is_MissionsAtCoords_Mission_Marked_For_Delete(paramSlot))
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [At Coords]: ...Mission had been 'marked for delete' already, remove this to ensure 'Play Again' works")
					PRINTLN(".KGM [At Coords]: ...AND: Set 'Delete After Focus' to ensure the removal of 'marked for delete' doesn't cause issues")
				#ENDIF
			
				Unmark_MissionsAtCoords_Mission_For_Delete(paramSlot)
				SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_DELETE_AFTER_FOCUS)
			ENDIF
		ENDIF
	ENDIF
	
	// Clear the Invite Accepted flag unless the mission is being played again
	IF NOT (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
		Clear_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(paramSlot)
	ELSE
		PRINTLN(".KGM [At Coords]: Mission 'Invite Accepted' should get cleared after Play, but retain to 'Play Again'")
	ENDIF
	
	// Assume for now that playlist missions can't be replayed, so clear this mission if it was the currently active playlist activity
	// KGM 5/9/14: BUG 2018946 - LTS Missions on a Playlist CAN be replayed if they are playing rounds, so retain the Active Playlist flag
	IF NOT (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
		Clear_MissionsAtCoords_Currently_Active_Playlist_Activity(paramSlot)
	ELSE
		PRINTLN(".KGM [At Coords]: Mission as 'Active Playlist Mission' should get cleared after Play, but retain to 'Play Again'")
	ENDIF

	// KGM 24/6/15 [BUG 2365221]: Clear the stored 'Rounds' ContentID hash if this mission is not being played again
	IF NOT (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
		g_roundsMissionHashCID = 0
		PRINTLN(".KGM [At Coords][Rounds]: Mission is not being 'Played Again', so clear the stored 'Rounds' ContentID Hash.")
	ENDIF
	
	// KGM 3/10/14 [BUG 2048347]: Dave needs to know about a Heist-related mission being Quick Restarte
	IF (Is_MissionsAtCoords_Mission_Being_Played_Again(paramSlot))
		MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
		IF (TEMP_Check_If_Heist_Using_Mission_Subtype(theMissionIdData))
		OR (TEMP_Check_If_Heist_Planning_Using_Mission_Subtype(theMissionIdData))
			PRINTLN(".KGM [At Coords]: Mission is Heist-Related being Quick Restarted. Let Dave know.")
			SET_LOCAL_PLAYER_SHOULD_DO_HEIST_FAIL_CASH_HELP_TEXT()
		ENDIF
	ENDIF
	
	// Mark the mission as 'played' - this will add a tick to the blip (and anything else if required later)
	Set_MissionsAtCoords_Mission_As_Played(paramSlot)
	
	// Check if the mission should await activation after being played
	IF (Should_MissionsAtCoords_Mission_Await_Activation_After_Play(paramSlot))
		Set_MissionsAtCoords_Mission_As_Awaiting_Activation(paramSlot)
	ENDIF
		
	// Is the activity now locked? If locked then make the mission's stage 'outside display ranges', otherwise make it 'Requesting Mission Name'
	// KGM 16/1/15 [BUG 2171096]: Network error when a Low-Ranked JIP player tried to join a later round of an Versus Mission but the mission wasn't unlocked.
	IF (IS_THIS_A_ROUNDS_MISSION())
		// ...on a rounds mission, so just let the player play
		Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
		PRINTSTRING("          The Mission Has Finished, so removing it as the Focus Mission - this is a rounds mission so let the player play") PRINTNL()
	ELSE
		IF (Is_MissionsAtCoords_Mission_Locked(paramSlot))
			// ...locked, so change the mission's stage to be 'outside display ranges'
			Set_MissionsAtCoords_Stage_For_Mission_To_Outside_Display_Ranges(paramSlot)
			PRINTSTRING("          Mission Has Finished and Mission Locked, so removing as Focus Mission and treating as outside display ranges") PRINTNL()
		ELSE
			// ...not locked, so change the mission's stage to be 'Requesting Mission Name'
			Set_MissionsAtCoords_Stage_For_Mission_To_Requesting_Mission_Name(paramSlot)
			PRINTSTRING("          The Mission Has Finished, so removing it as the Focus Mission") PRINTNL()
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: As a mission updates, ensure a SharedRegID remains valid and clear it if not
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
//							paramBand			The current scheduling Band for the mission
PROC Maintain_General_MissionsAtCoords_Mission_SharedRegID(INT paramSlot, g_eMatCBandings paramBand)

	SWITCH (paramBand)
		// Valid Bands within which to update the SharedRegID
		CASE MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE
		CASE MATCB_MISSIONS_INSIDE_CORONA_RANGE
		CASE MATCB_MISSIONS_OUTSIDE_CORONA_RANGE
		CASE MATCB_MISSIONS_FAR_AWAY_RANGE
			BREAK
			
		// Ignore all other bands
		DEFAULT
			EXIT
	ENDSWITCH
	
	MP_MISSION_ID_DATA theMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	
	IF (theMissionIdData.idSharedRegID = ILLEGAL_SHARED_REG_ID)
		EXIT
	ENDIF
	
	// Check if the SharedRegID is still valid
	IF (Is_Cloud_Data_Stored_As_A_Shared_Activity(theMissionIdData))
		EXIT
	ENDIF
	
	// SharedRedID is no longer valid (mission must no longer be shared)
	g_sAtCoordsMP[paramSlot].matcMissionIdData.idSharedRegID = ILLEGAL_SHARED_REG_ID
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: SharedRegID invalid. Clear it.") PRINTNL()
//		PRINTSTRING("         ") Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot) PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Get the PI Menu's current display requirement for this mission's Job Types
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
// RETURN VALUE:			BOOL				TRUE if the PI Menu's current status for this mission's job type is to hide the blip/corona, FALSE if they should be on show
// NOTES:	KGM 12/5/15: BUG 2313555 - PI Menu Hide Job Blips Option
FUNC BOOL Check_If_Mission_JobType_Has_Been_Hidden_By_PI_Menu(INT paramSlot)

	// KGM 16/7/15 [BUG 2363887]: Ignore PI Menu settings for hiding blips when in an activity session - this can interfere with rounds and probably Quick Restart
	//		(Although perhaps only if joining late and being a temporary spectator)
	IF (NETWORK_IS_ACTIVITY_SESSION())
		RETURN FALSE
	ENDIF
	
	// KGM 6/8/15 [BUG 2457730]: Ignore PI Menu Settings fir hiding blips if the option has not yet been unlocked
	IF NOT (IS_FM_TYPE_UNLOCKED(FMMC_TYPE_HIDE_BLIPS_AND_EVENTS))
		RETURN FALSE
	ENDIF

	MP_MISSION			theMissionID		= Get_MissionsAtCoords_Slot_MissionID(paramSlot)
	MP_MISSION_ID_DATA	theMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(paramSlot)
	INT					theSubtype			= Get_SubType_For_FM_Cloud_Loaded_Activity(theMissionIdData)
	
	SWITCH (theMissionID)
		// PI Job: Races
		CASE eFM_RACE_CLOUD
			// Return the option based on the Race Subtype
			SWITCH (theSubtype)
				CASE FMMC_RACE_TYPE_STANDARD
				CASE FMMC_RACE_TYPE_P2P
				CASE FMMC_RACE_TYPE_ON_FOOT
				CASE FMMC_RACE_TYPE_ON_FOOT_P2P
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_LAND_RACE))
					
				CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE		
				CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE_P2P
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_BIKE_RACE))
					
				CASE FMMC_RACE_TYPE_AIR
				CASE FMMC_RACE_TYPE_AIR_P2P
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_AIR_RACE))
					
				CASE FMMC_RACE_TYPE_BOAT
				CASE FMMC_RACE_TYPE_BOAT_P2P
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_SEA_RACE))
					
				CASE FMMC_RACE_TYPE_STUNT
				CASE FMMC_RACE_TYPE_STUNT_P2P
				CASE FMMC_RACE_TYPE_TARGET
				CASE FMMC_RACE_TYPE_TARGET_P2P
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_STUNT_RACE))
					
				CASE FMMC_RACE_TYPE_OPEN_WHEEL
				CASE FMMC_RACE_TYPE_OPEN_WHEEL_P2P
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_OPEN_WHEEL))
					
				//url:bugstar:7201354 - Tuner - Currently able to see individual Street Race and Pursuit Series blips on the map to enter - can we hide/remove these as the only way to enter is via the series blip? (and the latter is locked by rep so we're short cutting here)
				CASE  FMMC_RACE_TYPE_PURSUIT
				CASE  FMMC_RACE_TYPE_STREET	
				#IF FEATURE_GEN9_EXCLUSIVE
				CASE FMMC_RACE_TYPE_HSW
				CASE FMMC_RACE_TYPE_HSW_P2P
				#ENDIF
					RETURN TRUE
				
			ENDSWITCH
			BREAK
			
		// PI Job: Deathmatches
		CASE eFM_DEATHMATCH_CLOUD
			// Return the option based on the Deathmatch Subtype
			SWITCH (theSubtype)
				CASE FMMC_DEATHMATCH_STANDARD
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_DM))
					
				CASE FMMC_DEATHMATCH_TEAM
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_TEAM_DM))
					
				CASE FMMC_DEATHMATCH_VEHICLE
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_VEHICLE_DM))
					
				CASE FMMC_KING_OF_THE_HILL
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_KING_OF_THE_HILL))
				CASE FMMC_KING_OF_THE_HILL_TEAM
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_KING_OF_THE_HILL))

			ENDSWITCH
			BREAK
			
		// PI Job: Parachuting
		CASE eFM_BASEJUMP_CLOUD
			RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_PARACHUTE))
		
		CASE eFM_PILOT_SCHOOL
			IF SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
				RETURN TRUE
			ENDIF
			RETURN (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_FLIGHT_SCHOOL))
			
		// PI Job: Mission
		CASE eFM_MISSION_CLOUD
			// Return the correct option based onthe Mission Subtype
			// NOTE: Some subtypes will be ignores - ie: Heist-related and Contact Missions, etc
			SWITCH (theSubtype)
				CASE FMMC_MISSION_TYPE_STANDARD
				CASE FMMC_MISSION_TYPE_RANDOM
				CASE FMMC_MISSION_TYPE_VERSUS
				CASE FMMC_MISSION_TYPE_COOP
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_MISSION))
					
				CASE FMMC_MISSION_TYPE_LTS
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_LTS))
					
				CASE FMMC_MISSION_TYPE_CTF
					RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_CAPTURE))
					
				// Don't allow these to be affected by the PI Menu option
				CASE FMMC_MISSION_TYPE_HEIST
				CASE FMMC_MISSION_TYPE_PLANNING
				CASE FMMC_MISSION_TYPE_CONTACT
				CASE FMMC_MISSION_TYPE_FLOW_MISSION
					RETURN FALSE
			ENDSWITCH
			BREAK
			
		// PI Job: Survival
		CASE eFM_SURVIVAL_CLOUD
			// KGM 22/7/15 [BUG 2435434]: Ignore the PI Menu Show/Hide option for survivals until the survival tutorial has been completed
			IF NOT (HAS_PRIMARY_SURVIVAL_BEEN_COMPLETED())
				// Survival Tutorial hasn't been done, so ignore the PI Menu Show/Hide blip option
				RETURN FALSE
			ENDIF
			
			RETURN (SHOULD_HIDE_JOB_BLIP(ciPI_HIDE_MENU_ITEM_JOBS_JOB_SURVIVAL))
			
		// PI Misc: Darts
		CASE eFM_DARTS
			IF SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
				RETURN TRUE
			ENDIF
			RETURN (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_DARTS))
			
		// PI Misc: Arm Wrestling
		CASE eFM_ARM_WRESTLING
			IF SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
				RETURN TRUE
			ENDIF
			RETURN (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_ARM_WRESTLING))
			
		// PI Misc: Tennis
		CASE eFM_TENNIS
			IF SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
				RETURN TRUE
			ENDIF
			RETURN (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_TENNIS))
			
		// PI Misc: Golf
		CASE eFM_GOLF
			IF SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
			#IF FEATURE_FIXER
			OR IS_BIT_SET(g_sFixerFlow.Bitset2, ciFIXER_FLOW_BITSET2__HIDE_GOLF_BLIP)
			#ENDIF
				RETURN TRUE
			ENDIF
			RETURN (SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_GOLF))
	ENDSWITCH
	
	// Everything else isn't affected
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the PI Menu 'hide job blips' option has changed the display status of blips/coronas in this band
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
//							paramBand			The current scheduling Band for the mission
// NOTES:	KGM 12/5/15: BUG 2313555 - PI Menu Hide Job Blips Option
PROC Maintain_MissionsAtCoords_Mission_Status_Based_On_PI_Menu(INT paramSlot, g_eMatCBandings paramBand)

	// Ignore this for missions in certain bands
	SWITCH (paramBand)
		// Ignore missions in these bands
		CASE MATCB_FOCUS_MISSION
		CASE MATCB_MISSIONS_BEING_DELETED
			EXIT
	ENDSWITCH
	
	// Has the status for this mission type changed?
	BOOL currentlyHidden	= Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu(paramSlot)
	BOOL shouldBeHidden		= Check_If_Mission_JobType_Has_Been_Hidden_By_PI_Menu(paramSlot)
	
	IF (currentlyHidden = shouldBeHidden)
		EXIT
	ENDIF
	
	// The PI Menu Status has changed, so update the flag
	IF (shouldBeHidden)
		// ...The PI Menu Status has requested that this mission's Job Type should be hidden
		Set_MissionsAtCoords_Mission_As_Hidden_By_PI_Menu(paramSlot)
		
		// Hide the Blip
		Hide_MissionsAtCoords_Blip_For_Mission(paramSlot)
		
		// Clear the Corona Ground Projection if on display
		Clear_MissionsAtCoords_Slot_Corona_Ground_Projection(paramSlot)
		
		EXIT
	ENDIF
	
	// The PI Menu Stats has requested that these blips get revealed, so show them
	Clear_MissionsAtCoords_Mission_As_Hidden_By_PI_Menu(paramSlot)
	
	IF (Should_MissionsAtCoords_Blips_Be_Displayed())
		Display_MissionsAtCoords_Blip_For_Mission(paramSlot)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the Disable Area option has changed the display status of blips/coronas in this band
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
//							paramBand			The current scheduling Band for the mission
// NOTES:	KGM 11/6/15: BUG 2316703 - PI Menu Hide Job Blips Option
PROC Maintain_MissionsAtCoords_Mission_Status_Based_On_Disabled_Area(INT paramSlot, g_eMatCBandings paramBand)

	// Ignore this for missions in certain bands
	SWITCH (paramBand)
		// Ignore missions in these bands
		CASE MATCB_FOCUS_MISSION
		CASE MATCB_MISSIONS_BEING_DELETED
			EXIT
	ENDSWITCH
	
	// Has the status for this mission type changed?
	BOOL currentlyHidden	= Is_MissionsAtCoords_Mission_Hidden_By_Disabled_Area(paramSlot)
	BOOL shouldBeHidden		= g_matcHasLocalDisabledArea
	
	IF (shouldBeHidden)
		// ...there is a Disabled Area, so check if this mission is within it
		// Is this a Gang Attack area or a Corona mission?
		IF (Is_MissionsAtCoords_Mission_Area_Triggered(paramSlot))
			// Gang Attack area
			// If the point is within the Gang Attack triggering area or the Gang Attack blip radius area, hide the Gang Attack
			VECTOR blipCoords = Get_MissionsAtCoords_Slot_Coords(paramSlot)
			VECTOR disabledArea = g_matcLocalDisabledArea
			disabledArea.z = blipCoords.z
			
			IF (GET_DISTANCE_BETWEEN_COORDS(blipCoords, disabledArea) > ANGLED_AREA_BLIP_RADIUS_m)
				// ...point is outside the Gang Attack blip radius, so if it is also outside the Gang Attack triggering area then it shouldn't be hidden 
				INT areaIndex = Get_MissionsAtCoords_Angled_Area_ArrayPos_For_Slot(paramSlot)
				IF (areaIndex != ILLEGAL_ARRAY_POSITION)
					VECTOR	theMin		= g_sMatCAngledAreasMP[areaIndex].matcaaMin
					VECTOR	theMax		= g_sMatCAngledAreasMP[areaIndex].matcaaMax
					FLOAT	theWidth	= g_sMatCAngledAreasMP[areaIndex].matcaaWidth
		
					IF NOT (IS_POINT_IN_ANGLED_AREA(g_matcLocalDisabledArea, theMin, theMax, theWidth))
						// Also not in the trigger area, so Gang Attack shouldn't be hidden
						shouldBeHidden = FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// Corona Mission
			IF (GET_DISTANCE_BETWEEN_COORDS(Get_MissionsAtCoords_Slot_Coords(paramSlot), g_matcLocalDisabledArea) > g_matcLocalDisabledAreaRange)
				// ...mission is not within it
				shouldBeHidden = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF (currentlyHidden = shouldBeHidden)
		EXIT
	ENDIF
	
	// The Disabled Area status for this mission has changed, so update the flag
	IF (shouldBeHidden)
		// ...This mission is now within the Disabled Area and should be hidden
		Set_MissionsAtCoords_Mission_As_Hidden_By_Disabled_Area(paramSlot)
		
		// Hide the Blip
		Hide_MissionsAtCoords_Blip_For_Mission(paramSlot)
		
		// Clear the Corona Ground Projection if on display
		Clear_MissionsAtCoords_Slot_Corona_Ground_Projection(paramSlot)
		
		EXIT
	ENDIF
	
	// This mission is no longer inside the Disabled Area, so show it
	Clear_MissionsAtCoords_Mission_As_Hidden_By_Disabled_Area(paramSlot)
	
	IF (Should_MissionsAtCoords_Blips_Be_Displayed())
		Display_MissionsAtCoords_Blip_For_Mission(paramSlot)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Perform some functions that need done each time a mission updates, but only when the mission updates - this is before the update
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
//							paramBand			The current scheduling Band for the mission
PROC Maintain_General_MissionsAtCoords_Mission_PreUpdate(INT paramSlot, g_eMatCBandings paramBand)

	// Clear an existing SharedRegID if the mission is no longer a Shared Mission - active bands only except focus mission band
	Maintain_General_MissionsAtCoords_Mission_SharedRegID(paramSlot, paramBand)
	
	// Check if the player has chosen to hide/show this Job Type on the map using the 'Hide Job Blips' option of the PI menu
	// KGM 12/5/15: BUG 2313555 - PI Menu Hide Job Blips Option
	Maintain_MissionsAtCoords_Mission_Status_Based_On_PI_Menu(paramSlot, paramBand)
	
	// Check if this mission is now in, or was in, a Disabled Area
	Maintain_MissionsAtCoords_Mission_Status_Based_On_Disabled_Area(paramSlot, paramBand)
	
	// This mission has updated, so clear the 'force update' flag
	Clear_MissionsAtCoords_Mission_As_Force_Update(paramSlot)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Perform some functions that need done each time a mission updates, but only when the mission updates - this is after the update
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
PROC Maintain_General_MissionsAtCoords_Mission_PostUpdate(INT paramSlot)
	
	// Check if the mission's blip should appear long-range for being one of the closest to the player
	Maintain_MissionsAtCoords_Nearby_Long_Range_Blip(paramSlot)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check one mission for Stage changes and move the mission to a different scheduling Band if appropriate
//
// INPUT PARAMS:			paramSlot			The Slot containing the mission to be maintained
//							paramBand			The current scheduling Band for the mission
//
// NOTES:	In one frame the same stage shouldn't get hit more than once. Under bizarre circumstances (bug 1333723) it's possible
//				to hit an endless loop so the new bitfield will be used to monitor this and avoid it.
PROC Maintain_MissionsAtCoords_Mission_Stage(INT paramSlot, g_eMatCBandings paramBand)

	// Some things need pre-processed everytime a mission performs an update
	Maintain_General_MissionsAtCoords_Mission_PreUpdate(paramSlot, paramBand)

	// A mission could potentially jump multiple stages at once, so keep looping until the stage stabilises
	BOOL			continueChecking		= TRUE
	BOOL			missionDeleted			= FALSE
	g_eMatCStages	currentStage			= Get_MissionsAtCoords_Stage_For_Mission(paramSlot)
	FLOAT			distanceFromCoords		= Calculate_Player_Distance_From_Corona(paramSlot)
	INT				avoidEndlessLoopBits	= 0
	INT				thisStageAsBitflag		= -1
	
	WHILE (continueChecking)
		continueChecking	= FALSE
	
		// Perform the Update based on the player's current stage
		SWITCH (currentStage)
			CASE MATCS_STAGE_NEW
				continueChecking = Maintain_MissionsAtCoords_Stage_New(paramSlot)
				BREAK

			CASE MATCS_STAGE_DELETE
				missionDeleted		= Maintain_MissionsAtCoords_Stage_Delete(paramSlot, paramBand)
				continueChecking	= FALSE
				BREAK

			CASE MATCS_SLEEPING
				continueChecking = Maintain_MissionsAtCoords_Stage_Sleeping(paramSlot)
				BREAK

			CASE MATCS_OUTSIDE_ALL_RANGES
				continueChecking = Maintain_MissionsAtCoords_Stage_Outside_All_Ranges(paramSlot, distanceFromCoords)
				BREAK
				
			CASE MATCS_OUTSIDE_DISPLAY_RANGES
				continueChecking = Maintain_MissionsAtCoords_Stage_Outside_Display_Ranges(paramSlot, distanceFromCoords)
				BREAK

			CASE MATCS_DISPLAY_CORONA_RANGE
				continueChecking = Maintain_MissionsAtCoords_Stage_Display_Corona_Range(paramSlot, distanceFromCoords)
				BREAK
				
			CASE MATCS_AREA_TRIGGERED_MISSION
				continueChecking = Maintain_MissionsAtCoords_Stage_Area_Triggered_Mission(paramSlot, distanceFromCoords)
				BREAK
				
			CASE MATCS_MISSION_LAUNCH_IMMEDIATELY
				continueChecking = Maintain_MissionsAtCoords_Stage_Mission_Launch_Immediately(paramSlot, distanceFromCoords)
				BREAK

			CASE MATCS_REQUESTING_MISSION_NAME
				continueChecking = Maintain_MissionsAtCoords_Stage_Requesting_Mission_Name(paramSlot, distanceFromCoords)
				BREAK

			CASE MATCS_DISPLAYING_MISSION_NAME
				continueChecking = Maintain_MissionsAtCoords_Stage_Displaying_Mission_Name(paramSlot, distanceFromCoords)
				BREAK

			CASE MATCS_FORCING_AS_FOCUS_MISSION
				continueChecking = Maintain_MissionsAtCoords_Stage_Forcing_As_Focus_Mission(paramSlot)
				BREAK

			CASE MATCS_WAITING_FOR_ALLOW_RESERVATION
				continueChecking = Maintain_MissionsAtCoords_Stage_Waiting_For_Allow_Reservation(paramSlot, distanceFromCoords)
				BREAK

			CASE MATCS_RESERVING_MISSION
				continueChecking = Maintain_MissionsAtCoords_Stage_Reserving_Mission(paramSlot, distanceFromCoords)
				BREAK

			CASE MATCS_WAITING_FOR_CLOUD_DATA
				continueChecking = Maintain_MissionsAtCoords_Stage_Waiting_For_Cloud_Data(paramSlot, distanceFromCoords)
				BREAK

			CASE MATCS_WAITING_TO_START
				continueChecking = Maintain_MissionsAtCoords_Stage_Waiting_To_Start(paramSlot, distanceFromCoords)
				BREAK

			CASE MATCS_MISSION_STARTING
				continueChecking = Maintain_MissionsAtCoords_Stage_Mission_Starting(paramSlot)
				BREAK

			CASE MATCS_WAITING_FOR_ON_MISSION
				continueChecking = Maintain_MissionsAtCoords_Stage_Waiting_For_On_Mission(paramSlot)
				BREAK

			CASE MATCS_WAITING_FOR_QUIT_CORONA
				continueChecking = Maintain_MissionsAtCoords_Stage_Waiting_For_Quit_Corona(paramSlot)
				BREAK

			CASE MATCS_WAITING_FOR_OUTSIDE_FOCUS_RANGE
				continueChecking = Maintain_MissionsAtCoords_Stage_Waiting_For_Outside_Focus_Range(paramSlot, distanceFromCoords)
				BREAK
				
			CASE MATCS_DOWNLOAD_FOR_QUICK_LAUNCH
				continueChecking = Maintain_MissionsAtCoords_Stage_Download_For_Quick_Launch(paramSlot)
				BREAK
				
			CASE MATCS_MISSION_QUICK_LAUNCHING
				continueChecking = Maintain_MissionsAtCoords_Stage_Mission_Quick_Launching(paramSlot)
				BREAK

			CASE MATCS_ON_MISSION
				continueChecking = Maintain_MissionsAtCoords_Stage_On_Mission(paramSlot)
				BREAK

			CASE MAX_MISSIONS_AT_COORDS_STAGES
				#IF IS_DEBUG_BUILD
//					PRINTSTRING(".KGM [At Coords]: Maintain_MissionsAtCoords_Mission_Stage() - Attempting to update illegal stage: MAX_MISSIONS_AT_COORDS_STAGES, from slot:")
//					PRINTNL()
//					PRINTSTRING("         ")
//					Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot) PRINTNL()
					
					SCRIPT_ASSERT("Maintain_MissionsAtCoords_Mission_Stage(): ERROR: Attempting to update a slot with a current stage of MAX_MISSIONS_AT_COORDS_STAGES. See Console Log. Tell Keith.")
				#ENDIF
				BREAK
				
			DEFAULT
				#IF IS_DEBUG_BUILD
//					PRINTSTRING(".KGM [At Coords]: Maintain_MissionsAtCoords_Mission_Stage() - Need to add a new StageID to SWITCH, called from slot:")
//					PRINTNL()
//					PRINTSTRING("         ")
//					Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot) PRINTNL()
					
					SCRIPT_ASSERT("Maintain_MissionsAtCoords_Mission_Stage(): ERROR: Attempting to update an unknown stage. Add StageID to SWITCH. See Console Log. Tell Keith.")
				#ENDIF
				BREAK
		ENDSWITCH
		
		// Update the endless loop checking
		IF (continueChecking)
			thisStageAsBitflag = ENUM_TO_INT(currentStage)
			
			IF (IS_BIT_SET(avoidEndlessLoopBits, thisStageAsBitflag))
				// ...this stage had been hit before, so stop processing now for this frame
				#IF IS_DEBUG_BUILD
//					PRINTSTRING(".KGM [At Coords]: Maintain_MissionsAtCoords_Mission_Stage() - Processed a stage for the second time in this frame.") PRINTNL()
//					PRINTSTRING("                       Quit processing to avoid possible endless loop. Repeated Stage: ") Convert_Missions_At_Coords_Stage_To_String(currentStage) PRINTNL()
					SCRIPT_ASSERT("Maintain_MissionsAtCoords_Mission_Stage(): ERROR - Potential endless loop. Processed same stage for a second time in one frame. Quitting for this frame. Tell Keith.")
				#ENDIF
				
				continueChecking = FALSE
			ELSE
				SET_BIT(avoidEndlessLoopBits, thisStageAsBitflag)
			ENDIF
		ENDIF
		
		IF (continueChecking)
			currentStage = Get_MissionsAtCoords_Stage_For_Mission(paramSlot)
		ENDIF
	ENDWHILE
	
	// Move the mission to a different scheduling Band if required and perform a post-update
	IF NOT (missionDeleted)
		Update_MissionsAtCoords_Mission_Band(paramSlot, paramBand)
		
		Maintain_General_MissionsAtCoords_Mission_PostUpdate(paramSlot)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If a Force Update is required this frame, loop through all missions to find the one(s) that requested it
PROC Maintain_MissionsAtCoords_Force_Updates()

	IF NOT (g_sAtCoordsControlsMP.matccForceUpdateRequested)
		EXIT
	ENDIF

	// A force update has been requested
	// Always, clear the flag
	g_sAtCoordsControlsMP.matccForceUpdateRequested = FALSE
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]:")
		NET_PRINT_TIME()
		PRINTSTRING("Performing Force Updates requested in previous frame") PRINTNL()
	#ENDIF
	
	g_eMatCStages	thisStage	= MAX_MISSIONS_AT_COORDS_STAGES
	g_eMatCBandings	thisBand	= MAX_MISSIONS_AT_COORDS_BANDINGS
	INT				tempLoop	= 0
	
//	#IF IS_DEBUG_BUILD
//		INT numUpdates	= 0
//	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		IF (Should_MissionsAtCoords_Mission_Do_A_Forced_Update(tempLoop))
			#IF IS_DEBUG_BUILD
				PRINTSTRING(".KGM [At Coords]: Force Update. Slot: ") PRINTINT(tempLoop) PRINTNL()
			#ENDIF
		
			// Get the Band for the specified slot
			thisStage	= Get_MissionsAtCoords_Stage_For_Mission(tempLoop)
			thisBand	= Get_MissionsAtCoords_Band_For_Stage(thisStage)
			
			// Process the slot
			Maintain_MissionsAtCoords_Mission_Stage(tempLoop, thisBand)
			
//			#IF IS_DEBUG_BUILD
//				numUpdates++
//			#ENDIF
		ENDIF
	ENDREPEAT
	
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Num Missions Force Updated this frame: ") PRINTINT(numUpdates) PRINTNL()
//	#ENDIF
	
ENDPROC



// ===========================================================================================================
//      The Main MP Mission At Coords Control routines
// ===========================================================================================================

// PURPOSE: Set up one set of Stages info
//
// INPUT PARAMS:		paramStage			The processing stage being setup
//						paramBand			The scheduling Band the mission will move to when it goes within this processing stage
PROC Initialise_One_MatC_Stage(g_eMatCStages paramStage, g_eMatCBandings paramBand)

	// Ensure the Band hasn't already been setup
	IF (g_sAtCoordsStageInfo[paramStage].matcbBand != MAX_MISSIONS_AT_COORDS_BANDINGS)
		// ...details already stored for this Stage, so ignore these new details
		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Initialise_One_MatC_Stage - ERROR: Duplicate Stage being setup.") PRINTNL()
			SCRIPT_ASSERT("Initialise_One_MatC_Stage(): ERROR - Data for a Duplicate Stage is being setup. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Store the details
	g_sAtCoordsStageInfo[paramStage].matcbBand	= paramBand

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set up the Stages Info, which records an activation range and a scheduling Band for each stage (and anything else needed)
PROC Initialise_MP_Missions_At_Coords_Stages_Info()

	// Initialise Each Stage
	Initialise_One_MatC_Stage(MATCS_STAGE_NEW,							MATCB_NEW_MISSIONS)							// New mission awaiting initial processing
	Initialise_One_MatC_Stage(MATCS_STAGE_DELETE,						MATCB_MISSIONS_BEING_DELETED)				// A mission marked for deletion - it will be moved here so that it cleans up other stages before deletion
	Initialise_One_MatC_Stage(MATCS_SLEEPING,							MATCB_MISSIONS_FAR_AWAY_RANGE)				// Distant mission - infrequent scheduling to check if the mission has woken up
	Initialise_One_MatC_Stage(MATCS_OUTSIDE_ALL_RANGES,					MATCB_MISSIONS_FAR_AWAY_RANGE)				// Distant mission - infrequent scheduling to check if player is moving closer
	Initialise_One_MatC_Stage(MATCS_OUTSIDE_DISPLAY_RANGES,				MATCB_MISSIONS_OUTSIDE_CORONA_RANGE)		// LongRange mission - regular scheduling to check if player is getting closer to a display range
	Initialise_One_MatC_Stage(MATCS_DISPLAY_CORONA_RANGE,				MATCB_MISSIONS_INSIDE_CORONA_RANGE)			// MidRange mission - regular scheduling to check for mission name display range
	Initialise_One_MatC_Stage(MATCS_AREA_TRIGGERED_MISSION,				MATCB_MISSIONS_INSIDE_CORONA_RANGE)			// MidRange mission - regular scheduling to check if the player is in the area to trigger the mission
	Initialise_One_MatC_Stage(MATCS_MISSION_LAUNCH_IMMEDIATELY,			MATCB_MISSIONS_INSIDE_CORONA_RANGE)			// MidRange mission - regular scheduling to check if the mission has unlocked and should be triggered immediately
	Initialise_One_MatC_Stage(MATCS_REQUESTING_MISSION_NAME,			MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE)	// Nearby mission - frequent scheduling to check for becoming the focal mission
	Initialise_One_MatC_Stage(MATCS_DISPLAYING_MISSION_NAME,			MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE)	// Nearby mission - frequent scheduling to check for becoming the focal mission
	Initialise_One_MatC_Stage(MATCS_FORCING_AS_FOCUS_MISSION,			MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE)	// Nearby mission - frequent scheduling to check for becoming the focal mission
	Initialise_One_MatC_Stage(MATCS_WAITING_FOR_ALLOW_RESERVATION,		MATCB_FOCUS_MISSION)						// Focal mission - one-of, scheduled every frame
	Initialise_One_MatC_Stage(MATCS_RESERVING_MISSION,					MATCB_FOCUS_MISSION)						// Focal mission - one-of, scheduled every frame
	Initialise_One_MatC_Stage(MATCS_WAITING_FOR_CLOUD_DATA,				MATCB_FOCUS_MISSION)						// Focal mission - one-of, scheduled every frame
	Initialise_One_MatC_Stage(MATCS_WAITING_TO_START,					MATCB_FOCUS_MISSION)						// Focal mission - one-of, scheduled every frame
	Initialise_One_MatC_Stage(MATCS_MISSION_STARTING,					MATCB_FOCUS_MISSION)						// Focal mission - one-of, scheduled every frame
	Initialise_One_MatC_Stage(MATCS_WAITING_FOR_ON_MISSION,				MATCB_FOCUS_MISSION)						// Focal mission - one-of, scheduled every frame
	Initialise_One_MatC_Stage(MATCS_WAITING_FOR_QUIT_CORONA,			MATCB_FOCUS_MISSION)						// Focal mission - one-of, scheduled every frame
	Initialise_One_MatC_Stage(MATCS_WAITING_FOR_OUTSIDE_FOCUS_RANGE,	MATCB_FOCUS_MISSION)						// Focal mission - one-of, scheduled every frame
	Initialise_One_MatC_Stage(MATCS_DOWNLOAD_FOR_QUICK_LAUNCH,			MATCB_FOCUS_MISSION)						// Focal mission - one-of, scheduled every frame
	Initialise_One_MatC_Stage(MATCS_MISSION_QUICK_LAUNCHING,			MATCB_FOCUS_MISSION)						// Focal mission - one-of, scheduled every frame
	Initialise_One_MatC_Stage(MATCS_ON_MISSION,							MATCB_FOCUS_MISSION)						// Focal mission - one-of, scheduled every frame
	
	// Ensure all stages have been setup
	#IF IS_DEBUG_BUILD
		INT tempLoop = 0
		
		REPEAT MAX_MISSIONS_AT_COORDS_STAGES tempLoop
			IF (g_sAtCoordsStageInfo[tempLoop].matcbBand = MAX_MISSIONS_AT_COORDS_BANDINGS)
//				PRINTSTRING(".KGM [At Coords]: Initialise_MP_Missions_At_Coords_Stages_Info(): Stage hasn't been setup with details: ")
//				PRINTSTRING(Convert_Missions_At_Coords_Stage_To_String(INT_TO_ENUM(g_eMatCStages, tempLoop)))
//				PRINTNL()
				
				SCRIPT_ASSERT("Initialise_MP_Missions_At_Coords_Stages_Info(): ERROR - One or more Stages have not received range and scheduling information. Look at Console Log. Tell Keith.")
			ENDIF
		ENDREPEAT
	#ENDIF
	
	// Display the Stages info in the console log
	#IF IS_DEBUG_BUILD
		Debug_Output_Missions_At_Coords_Stages_Array()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	A one-off Missions At Coords initialisation.
PROC Initialise_MP_Missions_At_Coords()

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Initialise_MP_Missions_At_Coords") PRINTNL()
	#ENDIF

	Clear_All_MP_Mission_At_Coords_Variables()
	Initialise_MP_Missions_At_Coords_Stages_Info()
	
	// Store the corona active TXD and Texture Names
	// NOTE: If undefined then the game is not using a Texture when a corona is active
	MATC_CORONA_ACTIVE_TXD_NAME 	= ""
	MATC_CORONA_ACTIVE_TEXTURE_NAME	= ""
	
	// Store the corona projections TXD name
	MATC_CORONA_PROJECTION_TXD_NAME	= "MPMissMarkers256"
	
	// Initially, blips should be classed as 'display' - they'll get switched off immediately if a transition session is placing hte player in a corona
	// NOTE: If FALSE, then this causes a subtle bug where some blips don't get switched off if the player gets a focus mission on the first frame because it thinks they're already off
	g_sAtCoordsControlsMP.matccBlipsOnDisplay = TRUE
	
	// Clear all the cloud refresh control variables
	g_sAtCoordsControlsMP.matccDownloadInProgress	= FALSE
	g_sAtCoordsControlsMP.matccRefreshInProgress	= FALSE
	g_sAtCoordsControlsMP.matccInitialDataReady		= FALSE
	
	// Clear the Allow Ambient Tutorial flag
	g_iMatccCounterOnDuringAmbTut = 0
	
	// Clear the Global Player Broadcast data and the local control data that stores this player's nearest coronas for any SCTV players to refer to
	INT thisPlayer = NATIVE_TO_INT(PLAYER_ID())
	INT coronaLoop = 0
	REPEAT MAX_SCTV_CORONAS coronaLoop
		Clear_One_Corona_Data_For_Storage_For_SCTV(thisPlayer, coronaLoop)
		Clear_One_Copy_Corona_Data_For_SCTV(coronaLoop)
	ENDREPEAT
	g_numCoronaSCTV	= 0
	
	// ...also clear the local control data for the spectator (SCTV) player, used locally to maintain the state of a corona message scaleform movie being loaded
	REPEAT MAX_LOCATE_MESSAGE_MOVIES coronaLoop
		Clear_One_Corona_Details_Data_For_SCTV_Player(coronaLoop)
	ENDREPEAT
	
	// ...clear the Paused Corona SCTV struct
	Clear_Paused_Corona_Details_For_SCTV_Player(thisPlayer)
	
	// Clear the 'reduced performance' global - added so that it does less processing while a Heist is active
	g_matcUseReducedProcessing = FALSE
	
	// Clear the 'heist mission full' delayed help message controls
	g_matcHeistFullHelpNeeded = FALSE
	g_matcHeistFullHelpSafetyFrame = 0
	
	// Clear any local disabled area
	g_matcLocalDisabledArea		= << 0.0, 0.0, 0.0 >>
	g_matcHasLocalDisabledArea	= FALSE
	
	// Clear any 'unblocked mission' used to allow triggering when all other missions are blocked and hidden by FM Event (ie: to allow Pause Menu triggering)
	g_matcUnblockOneMissionHash = 0
	g_matcUnblockOneMissionTimeout = 0

	// Clear 'Press DPADRIGHT' help text if appropriate and clear the 'stay active' frame counter
	IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MATC_DPADRIGHT"))
		PRINTLN(".KGM [At Coords][DPadRight]: Initialise_MP_Missions_At_Coords() Clear 'Press DPAD-RIGHT' help message")
		CLEAR_HELP()
	ENDIF
	g_matcDpadRightHelpFrameTimeout = 0
	g_sV2CoronaVars.g_isplayer_inside_corona = FALSE
	PRINTLN("g_sV2CoronaVars.g_isplayer_inside_corona = FALSE 6")
	// Display the current PI Menu option
	#IF IS_DEBUG_BUILD
	Debug_Output_PI_Menu_Hide_Blips_Status()
	#ENDIF

//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Initialise_MP_Missions_At_Coords: Clearing Cloud Download and Refresh Control flags")
//		PRINTNL()
//	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Focus Mission
PROC Maintain_MissionsAtCoords_Focus_Mission()

	g_eMatCBandings theBand = MATCB_FOCUS_MISSION
	
	IF (Get_Number_Of_MissionsAtCoords_In_Band(theBand) = 0)
		EXIT
	ENDIF
	
	// Ensure the 'update' slot is consistent with the rest of the data
	INT theUpdateSlot = Get_Next_Valid_Update_Slot_For_Band(theBand)
	IF (theUpdateSlot = INVALID_MATC_SLOT)
		EXIT
	ENDIF
	
	// Process the Focus Mission
	Maintain_MissionsAtCoords_Mission_Stage(theUpdateSlot, theBand)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Nearby Missions
// NOTES:	Only going to process one mission per frame because there shouldn't be a lot of nearby missions
PROC Maintain_MissionsAtCoords_Missions_Inside_Mission_Name_Range()

	// KGM 12/1/15 [BUG 2190862]: Don't process missions in this band if 'reduced processing' is active
	IF (g_matcUseReducedProcessing)
		EXIT
	ENDIF

	g_eMatCBandings theBand = MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE
	
	IF (Get_Number_Of_MissionsAtCoords_In_Band(theBand) = 0)
		EXIT
	ENDIF
	
	// Ensure the 'update' slot is consistent with the rest of the data
	INT theUpdateSlot = Get_Next_Valid_Update_Slot_For_Band(theBand)
	IF (theUpdateSlot = INVALID_MATC_SLOT)
		EXIT
	ENDIF
	
	// Process the Scheduled Nearby Mission
	Maintain_MissionsAtCoords_Mission_Stage(theUpdateSlot, theBand)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the MidRange Missions
// NOTES:	Only going to process one mission per frame - these missions are close to the player but not close enough to be focus mission candidates
PROC Maintain_MissionsAtCoords_Missions_Inside_Corona_Range()

	// KGM 12/1/15 [BUG 2190862]: Don't process missions in this band if 'reduced processing' is active
	IF (g_matcUseReducedProcessing)
		EXIT
	ENDIF

	g_eMatCBandings theBand = MATCB_MISSIONS_INSIDE_CORONA_RANGE
	
	IF (Get_Number_Of_MissionsAtCoords_In_Band(theBand) = 0)
		EXIT
	ENDIF
	
	// Ensure the 'update' slot is consistent with the rest of the data
	INT theUpdateSlot = Get_Next_Valid_Update_Slot_For_Band(theBand)
	IF (theUpdateSlot = INVALID_MATC_SLOT)
		EXIT
	ENDIF
	
	// Process the Scheduled MidRange Mission
	Maintain_MissionsAtCoords_Mission_Stage(theUpdateSlot, theBand)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Missions Outside Corona Range
// NOTES:	Only going to process one mission per frame - these missions are a decent distance away from the player
PROC Maintain_MissionsAtCoords_Missions_Outside_Corona_Range()

	// KGM 12/1/15 [BUG 2190862]: Don't process missions in this band if 'reduced processing' is active
	IF (g_matcUseReducedProcessing)
		EXIT
	ENDIF

	g_eMatCBandings theBand = MATCB_MISSIONS_OUTSIDE_CORONA_RANGE
	
	IF (Get_Number_Of_MissionsAtCoords_In_Band(theBand) = 0)
		EXIT
	ENDIF
	
	// Ensure the 'update' slot is consistent with the rest of the data
	INT theUpdateSlot = Get_Next_Valid_Update_Slot_For_Band(theBand)
	IF (theUpdateSlot = INVALID_MATC_SLOT)
		EXIT
	ENDIF
	
	// Process the Scheduled LongRange Mission
	Maintain_MissionsAtCoords_Mission_Stage(theUpdateSlot, theBand)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Missions In Far Away Range
// NOTES:	Only going to process one mission per frame - these missions are a long distance away from the player
PROC Maintain_MissionsAtCoords_Missions_In_Far_Away_Range()

	// KGM 12/1/15 [BUG 2190862]: Don't process missions in this band if 'reduced processing' is active
	IF (g_matcUseReducedProcessing)
		EXIT
	ENDIF

	g_eMatCBandings theBand = MATCB_MISSIONS_FAR_AWAY_RANGE
	
	IF (Get_Number_Of_MissionsAtCoords_In_Band(theBand) = 0)
		EXIT
	ENDIF
	
	// Ensure the 'update' slot is consistent with the rest of the data
	INT theUpdateSlot = Get_Next_Valid_Update_Slot_For_Band(theBand)
	IF (theUpdateSlot = INVALID_MATC_SLOT)
		EXIT
	ENDIF
	
	// Process the Scheduled FarAway Mission
	Maintain_MissionsAtCoords_Mission_Stage(theUpdateSlot, theBand)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the New Missions
PROC Maintain_MissionsAtCoords_New_Missions()

	g_eMatCBandings	theBand			= MATCB_NEW_MISSIONS
	INT				theUpdateSlot	= 0
	INT				maxToProcess	= 10
	INT				numProcessed	= 0
	
	// Process all new missions in a frame
	WHILE (Get_Number_Of_MissionsAtCoords_In_Band(theBand) > 0)
		// Always set the update slot to be the first mission in the Band (it will always get moved to a different band)
		theUpdateSlot = Get_First_MissionsAtCoords_Slot_For_Band(theBand)
		Set_MissionsAtCoords_Update_Slot_For_Band(theBand, theUpdateSlot)
		
		IF (theUpdateSlot != INVALID_MATC_SLOT)
			// Process the Scheduled New Mission
			Maintain_MissionsAtCoords_Mission_Stage(theUpdateSlot, theBand)
		ENDIF
		
		// Only process a maximum number of new missions in a frame
		numProcessed++
		IF (numProcessed >= maxToProcess)
			EXIT
		ENDIF
	ENDWHILE

ENDPROC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain Hide/Show update based on PI Menu change
PROC Maintain_MissionsAtCoords_PI_Menu_Change_Forced_Update()

	IF NOT (g_matcImmediateUpdateForPIMenuHideBlip)
		EXIT
	ENDIF
	
	g_matcImmediateUpdateForPIMenuHideBlip = FALSE
	
	PRINTLN(".KGM [At Coords]: Maintain_MissionsAtCoords_PI_Menu_Change_Forced_Update() because PI Menu set g_matcImmediateUpdateForPIMenuHideBlip to TRUE")
	
	// Update all blips of all missions in all Bands
	INT 			firstCoronaSlot	= 0
	INT 			lastCoronaSlot	= 0
	INT 			bandLoop		= 0
	INT				endBandLoop		= (ENUM_TO_INT(MAX_MISSIONS_AT_COORDS_BANDINGS) - 1)
	INT 			slotLoop		= 0
	g_eMatCBandings	thisBandID
	
	FOR bandLoop = MATCB_FOCUS_MISSION TO endBandLoop
		thisBandID = INT_TO_ENUM(g_eMatCBandings, bandLoop)
		
		IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
			firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
			lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
			
			FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
				Maintain_MissionsAtCoords_Mission_Status_Based_On_PI_Menu(slotLoop, thisBandID)
			ENDFOR
		ENDIF
	ENDFOR
	IF g_sV2CoronaVars.g_isplayer_inside_corona = TRUE
		g_sV2CoronaVars.g_isplayer_inside_corona = FALSE
		PRINTLN("g_sV2CoronaVars.g_isplayer_inside_corona = FALSE 8")
	ENDIF
	//Clear the help to fix - url:bugstar:2418118 - Help text "Press d-pad right to enter" corona remains on screen after standing in a corona and then using Hide Option>Hide All Jobs.
	IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MATC_DPADRIGHT"))
		PRINTLN(".KGM [At Coords]: Maintain_MissionsAtCoords_PI_Menu_Change_Forced_Update() Clear 'Press DPAD-RIGHT' help message - MATC_DPADRIGHT")
		CLEAR_HELP()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		Debug_Output_PI_Menu_Hide_Blips_Status()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Wanted Level status so 
PROC Maintain_MissionsAtCoords_Wanted_Level()
	g_sAtCoordsControlsMP.matccPlayerHasWantedLevel = (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain all Blips, switching them on and off en-masse based on what's happening in the game
// NOTES:	All missions except new missions will need their blip state updated if a change is detected
PROC Maintain_MissionsAtCoords_Blips()

	
	// Check for a state change
	BOOL blipsShouldBeDisplayed	= Should_MissionsAtCoords_Blips_Be_Displayed()
	BOOL blipsAreDisplayed		= g_sAtCoordsControlsMP.matccBlipsOnDisplay
	
	// ...nothing to do if there is no change
	IF (blipsShouldBeDisplayed = blipsAreDisplayed)
		EXIT
	ENDIF
	
	
	
	// Change the blip display state of all missions in all Bands (except for New Missions)
	INT 			firstCoronaSlot	= 0
	INT 			lastCoronaSlot	= 0
	INT 			bandLoop		= 0
	INT 			slotLoop		= 0
	BOOL			outputLogging	= FALSE
	g_eMatCBandings	thisBandID
	
	
	IF !NETWORK_IS_ACTIVITY_SESSION()
	OR !blipsShouldBeDisplayed
	
		FOR bandLoop = MATCB_FOCUS_MISSION TO MATCB_MISSIONS_FAR_AWAY_RANGE
			thisBandID = INT_TO_ENUM(g_eMatCBandings, bandLoop)
			
			IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
				firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
				lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
				
				FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
					IF (blipsShouldBeDisplayed)
						Display_MissionsAtCoords_Blip_For_Mission(slotLoop, DEFAULT, outputLogging)
					ELSE
						Hide_MissionsAtCoords_Blip_For_Mission(slotLoop, outputLogging)
					ENDIF
				ENDFOR
			ENDIF
		ENDFOR
	ENDIF
	
	// The blip state has changed, so update the control variable
	g_sAtCoordsControlsMP.matccBlipsOnDisplay = blipsShouldBeDisplayed
	
	// If blips are being switched off, then clear any help text helper globals
	IF NOT (blipsShouldBeDisplayed)
		Clear_All_Help_Text_Helper_Flags()
	ENDIF
	
	// Some console log output
	#IF IS_DEBUG_BUILD
		PRINTSTRING("****************************************************************") PRINTNL()
		PRINTNL()
		PRINTSTRING(".KGM [At Coords]: Game State Changed - All Blips: ")
		IF (blipsShouldBeDisplayed)
			PRINTSTRING("ON")
		ELSE
			PRINTSTRING("OFF")
		ENDIF
		PRINTNL()
		PRINTNL()
		PRINTSTRING("****************************************************************") PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain Player In Interior flag, and any Interior Only blips if the flag has changed state
PROC Maintain_MissionsAtCoords_Player_In_Interior_And_Interior_Only_Blips()

	// Check for an InInterior state change
	BOOL inInterior	= FALSE
	IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
		inInterior = IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
	ENDIF
	
	// ...nothing to do if there is no change in the players 'InInterior' state
	IF (inInterior = Is_MatC_Player_In_Interior())
		EXIT
	ENDIF
	
	// The InInterior state has changed, so update the player variable
	g_sAtCoordsControlsMP.matccPlayerInInterior = inInterior
	
	// Check for a Interior Only blips display state change
	BOOL blipsShouldBeDisplayed	= Should_MissionsAtCoords_Blips_Be_Displayed()
	
	IF (blipsShouldBeDisplayed)
	AND NOT (inInterior)
		blipsShouldBeDisplayed = FALSE
	ENDIF
	
	// Change the blip display state of all missions with Interior Only blips in all Bands within Corona Range
	INT 			firstCoronaSlot	= 0
	INT 			lastCoronaSlot	= 0
	INT 			bandLoop		= 0
	INT 			slotLoop		= 0
	g_eMatCBandings	thisBandID
	
	FOR bandLoop = MATCB_FOCUS_MISSION TO MATCB_MISSIONS_INSIDE_CORONA_RANGE
		thisBandID = INT_TO_ENUM(g_eMatCBandings, bandLoop)
		
		IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
			firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
			lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
			
			FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
				IF (Should_MissionsAtCoords_Mission_Blip_Display_In_Interior_Only(slotLoop))
					IF (blipsShouldBeDisplayed)
						Display_MissionsAtCoords_Blip_For_Mission(slotLoop, TRUE)
					ELSE
						Hide_MissionsAtCoords_Blip_For_Mission(slotLoop)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	
	// Some console log output
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Change In 'Player In Interior' State Detected - All Nearby Interior Only Blips Are Being Switched ")
//		IF (blipsShouldBeDisplayed)
//			PRINTSTRING("ON")
//		ELSE
//			PRINTSTRING("OFF")
//		ENDIF
//		PRINTNL()
//	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain all active Coronas
// NOTES:	All missions inside Corona range will need their corona maintained, except for missions that have started
PROC Maintain_MissionsAtCoords_Coronas()

	// IF MATC_CORONA_ACTIVE_TXD_NAME is not defined, then the game isn't using a Texture to represent an active corona
	BOOL doesGameHaveCoronaActiveTXD = Does_Game_Have_A_Matc_Corona_Active_TXD()
	
	IF (doesGameHaveCoronaActiveTXD)
		// If the Active Corona TXD is being requested then maintain the request until loaded
		IF (Is_MatC_Corona_Active_TXD_Being_Requested())
			// ...active corona TXD is being requested, so request it again (each frame)
			REQUEST_STREAMED_TEXTURE_DICT(MATC_CORONA_ACTIVE_TXD_NAME)
			IF (HAS_STREAMED_TEXTURE_DICT_LOADED(MATC_CORONA_ACTIVE_TXD_NAME))
				Clear_MatC_Corona_Active_TXD_Is_Being_Requested()
				Set_MatC_Corona_Active_TXD_Has_Loaded()
			ENDIF
		ENDIF
		
		// Each frame, clear the variable that indicates any corona requires the active texture - if still clear after drawing then the TXD can be freed
		Clear_MatC_Corona_Requires_Active_TXD()
	ENDIF

	INT 			firstCoronaSlot	= 0
	INT 			lastCoronaSlot	= 0
	INT 			bandLoop		= 0
	INT 			slotLoop		= 0
	g_eMatCBandings	thisBandID

	// Don't need to do this if the player is on a mission
	IF NOT (Should_MissionsAtCoords_Coronas_Be_Displayed())
		
		BOOL coronaStillNeedsToBeDrawn
		IF (g_matcBlockAndHideAllMissions)
			FOR bandLoop = MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE TO MATCB_MISSIONS_INSIDE_CORONA_RANGE
				thisBandID = INT_TO_ENUM(g_eMatCBandings, bandLoop)
				
				IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
					firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
					lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
						
					FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
						IF Should_MissionAtCoords_Ignore_Block_On_Mission(slotLoop)
							Draw_MissionsAtCoords_Corona(slotLoop)
							coronaStillNeedsToBeDrawn = TRUE
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
		ENDIF
		
		// If the Active TXD has been loaded, it can be freed
		IF NOT coronaStillNeedsToBeDrawn
			IF (doesGameHaveCoronaActiveTXD)
				IF (Is_MatC_Corona_Active_TXD_Loaded())
					SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(MATC_CORONA_ACTIVE_TXD_NAME)
					Clear_MatC_Corona_Active_TXD_Has_Loaded()
				ENDIF
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	// Otherwise maintain the coronas of all missions up to and including the Inside Corona Range Band (Coronas are Team Coloured
	FOR bandLoop = MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE TO MATCB_MISSIONS_INSIDE_CORONA_RANGE
		thisBandID = INT_TO_ENUM(g_eMatCBandings, bandLoop)
		
		IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
			firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
			lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
			
			FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
				Draw_MissionsAtCoords_Corona(slotLoop)
			ENDFOR
		ENDIF
	ENDFOR
	
	// If the game doesn't have an active corona TXD defined then nothing more to do
	IF NOT (doesGameHaveCoronaActiveTXD)
		EXIT
	ENDIF
	
	// Check if any corona requires the active corona TXD
	IF (Is_MatC_Corona_Active_TXD_Required_By_Any_Corona())
		// ...yes, a corona requires the active corona TXD
		// Nothing to do if already loaded
		IF (Is_MatC_Corona_Active_TXD_Loaded())
			EXIT
		ENDIF
		
		// Request it if not loaded - this can be set again even if already set
		Set_MatC_Corona_Active_TXD_Is_Being_Requested()
		
		EXIT
	ENDIF
	
	// No corona requires the active corona TXD
	// Nothing to do if not loaded
	IF NOT (Is_MatC_Corona_Active_TXD_Loaded())
		EXIT
	ENDIF
	
	// Active Corona TXD is loaded, so free it
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(MATC_CORONA_ACTIVE_TXD_NAME)
	Clear_MatC_Corona_Active_TXD_Has_Loaded()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there is a condition that should prevent the display of Ground Projections
//
// RETURN VALUE:		BOOL			TRUE if a restriction exists, otherwise FALSE
FUNC BOOL Does_Corona_Ground_Projection_Restriction_Exist()

	// Are corona allowed to be on display?
	IF NOT (Should_MissionsAtCoords_Coronas_Be_Displayed())
		// We should not remove this TXD if we are blocking the maps (may still need to display some Jobs in world)
		IF NOT (g_matcBlockAndHideAllMissions AND GET_FM_STRAND_PROGRESS(ciFLOW_STRAND_LOW_RIDER) >= 1 )
			
			// Restriction exists
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Is there a focus mission?
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
	AND NOT (g_sCoronaFocusVisuals.retainedCoronaActive)
//	AND (IS_TRANSITION_SESSIONS_CORONA_CONTROLLER_IS_RUNNING())	- This is what was used to delay the ground projection from being cleaned up for a little while after walk-in - no longer needed
		// Restriction exists
		RETURN TRUE
	ENDIF
		
	// Are there any missions within projection display (mission name display) range?
	IF (Get_Number_Of_MissionsAtCoords_In_Band(MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE) = 0)
	AND (Get_Number_Of_MissionsAtCoords_In_Band(MATCB_FOCUS_MISSION) = 0)
		// Restriction exists
		RETURN TRUE
	ENDIF
	
	// There are no restrictions
	RETURN FALSE
		
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Unpatch any decals that need it
PROC Maintain_MissionsAtCoords_Corona_Ground_Projection_Decal_Unpatching()

	DECAL_RENDERSETTING_ID		thisDecal	= DECAL_RSID_RACE_LOCATE_GROUND
	INT							tempLoop	= 0
	
	REPEAT MATC_CORONA_MAX_PROJECTIONS tempLoop
		SWITCH (g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].stage)
			CASE GTXD_STAGE_UNPATCH_DECAL
				#IF IS_DEBUG_BUILD
					PRINTSTRING(".KGM [At Coords]: Decal - Unpatch. Ground TXD Slot: ")
					PRINTINT(tempLoop)
					PRINTNL()
				#ENDIF
				
				thisDecal = Get_MissionsAtCoords_Decal_For_Projection_ArrayPos(tempLoop)
				UNPATCH_DECAL_DIFFUSE_MAP(thisDecal)
				
				g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].stage = GTXD_STAGE_ALLOW_REUSE_NEXT_FRAME
				
				#IF IS_DEBUG_BUILD
					Debug_Output_Ground_Projections()
				#ENDIF
				BREAK
				
			CASE GTXD_STAGE_ALLOW_REUSE_NEXT_FRAME
				g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].stage = GTXD_STAGE_NOT_IN_USE
				
				#IF IS_DEBUG_BUILD
					PRINTSTRING(".KGM [At Coords]: Decal - Allow re-use. Ground TXD Slot: ")
					PRINTINT(tempLoop)
					PRINTNL()
					Debug_Output_Ground_Projections()
				#ENDIF
				BREAK
				
			// Ignore other CASEs - irrelevant here
		ENDSWITCH
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Corona Ground Projections TXD (The Display Mission Name maintenance routine will deal with creating the texture if needed)
// NOTES:	All Missions within Mission Name Range (but not the focus mission) should get a ground projection
PROC Maintain_MissionsAtCoords_Corona_Ground_Projection_TXD()

	// Unpatch any decals that are no longer needed - this needs done at least a frame after the checkpoint has been deleted to avoid a nasty rendering artifact as it is being switched off
	// NOTE 1/5/13: This one-frame delay before unpatching should no longer be necessary following a code change, but I'll leave it as-is for now
	Maintain_MissionsAtCoords_Corona_Ground_Projection_Decal_Unpatching()

	// If the Corona Ground Projection TXD is being requested then maintain the request until loaded
	IF (Is_MatC_Corona_Ground_Projection_TXD_Being_Requested())
		// ...corona Ground Projection TXD is being requested, so request it again (each frame)
		REQUEST_STREAMED_TEXTURE_DICT(MATC_CORONA_PROJECTION_TXD_NAME)
		IF (HAS_STREAMED_TEXTURE_DICT_LOADED(MATC_CORONA_PROJECTION_TXD_NAME))
			// ...TXD has loaded
			Clear_MatC_Corona_Ground_Projection_TXD_Is_Being_Requested()
			Set_MatC_Corona_Ground_Projection_TXD_Has_Loaded()
		ELSE
			// ...TXD has been requested but hasn't loaded, so nothing to do until it loads
			EXIT
		ENDIF
	ENDIF
	
	// TXD is not being requested (it is either not loaded, or already loaded)
	// Check if a restriction exists that prevents the Projection from being displayed
	BOOL areCoronaProjectionsAllowedAndRequired = TRUE
	
	IF (Does_Corona_Ground_Projection_Restriction_Exist())
		areCoronaProjectionsAllowedAndRequired = FALSE
	ENDIF
	
	// If not loaded, check if it needs to be requested
	IF NOT (Is_MatC_Corona_Ground_Projection_TXD_Loaded())
		// ...not loaded, so should it get requested?
		// Are projections allowed?
		IF NOT (areCoronaProjectionsAllowedAndRequired)
			// ...no
			EXIT
		ENDIF

		// Request the TXD
		Set_MatC_Corona_Ground_Projection_TXD_Is_Being_Requested()
		
		EXIT
	ENDIF
	
	// TXD has loaded, so check if it should clean up
	IF (areCoronaProjectionsAllowedAndRequired)
		// ...projections are allowed and required, so nothing more to do
		EXIT
	ENDIF
	
	// Projections are either not allowed or not required, so delete any that are setup
	INT tempLoop = 0
	
	REPEAT MATC_CORONA_MAX_PROJECTIONS tempLoop
		IF (g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].stage = GTXD_STAGE_BEING_RENDERED)
			// ...need to tidy this one up
			Clear_MissionsAtCoords_Corona_Ground_Projection_For_RegID(g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].regID)
		ENDIF
	ENDREPEAT
	
	// All projections should be deleted, so free the TXD
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(MATC_CORONA_PROJECTION_TXD_NAME)
	Clear_MatC_Corona_Ground_Projection_TXD_Has_Loaded()

ENDPROC


// PURPOSE:	Check if the retriction state has updated to a different state for the mission locate
// NOTES:	Will return once the state of a locate changes
//    
FUNC BOOL Check_reserved_changed(int paramslot,bool showReserved)
	if  showReserved			
		IF NOT IS_BIT_SET(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_SHOW_RESERVED)
			RETURN TRUE								
		ENDIF
	else
		IF IS_BIT_SET(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_SHOW_RESERVED)
			RETURN TRUE								
		ENDIF
	endif
	
	RETURN FALSE
ENDFUNC


// PURPOSE:	Check if the retriction state has updated to a different state for the mission locate
// NOTES:	Will return once the state of a locate changes
//    
FUNC BOOL Check_restrictions_changed(int paramslot,g_eMatCLaunchFailReasonID restrictionID)
// check to see if the restricion has changed 
	if restrictionID	!= MATCLFR_NONE		
		if 	restrictionID	= MATCLFR_WANTED_LEVEL						
			IF NOT IS_BIT_SET(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_WANTED)
				RETURN TRUE								
			ENDIF
		elif	restrictionID	= MATCLFR_NOT_ENOUGH_CASH
			IF NOT IS_BIT_SET(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_CASH)
				RETURN TRUE								
			ENDIF
		elif	restrictionID	= MATCLFR_RANK_TOO_LOW
			IF NOT IS_BIT_SET(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_RANK)
				RETURN TRUE								
			ENDIF
		elif	restrictionID	= MATCLFR_WRONG_TIMEOFDAY
			IF NOT IS_BIT_SET(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_TOD)
				RETURN TRUE								
			ENDIF	
		endif
	else
		IF IS_BIT_SET(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_WANTED)
		or IS_BIT_SET(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_CASH)
		or IS_BIT_SET(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_RANK)
		or IS_BIT_SET(g_sAtCoordsMP[paramslot].matcStateBitflags, MATC_BITFLAG_RESTRICTED_TOD)
			RETURN TRUE
		endif
	endif
	
	
	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// KGM 27/7/13: Corona Icons are no longer being displayed
//// PURPOSE:	Maintain the icons at all active coronas
//// NOTES:	All missions inside Corona range will need their corona icons maintained, except for missions that have started
//PROC Maintain_MissionsAtCoords_Corona_Icons()
//	
//	// Don't need to do this if the player is on a mission
//	IF NOT (Should_MissionsAtCoords_Coronas_Be_Displayed())
//		EXIT
//	ENDIF
//	
//	// Otherwise maintain the coronas of all missions up to and including the Inside Corona Range Band (Coronas are Team Coloured)
//	INT 			firstCoronaSlot	= 0
//	INT 			lastCoronaSlot	= 0
//	INT 			bandLoop		= 0
//	INT 			slotLoop		= 0
//	g_eMatCBandings	thisBandID
//	
//	FOR bandLoop = MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE TO MATCB_MISSIONS_INSIDE_CORONA_RANGE
//		thisBandID = INT_TO_ENUM(g_eMatCBandings, bandLoop)
//		
//		IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
//			firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
//			lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
//			
//			FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
//				Draw_MissionsAtCoords_Corona_Icons(slotLoop)
//			ENDFOR
//		ENDIF
//	ENDFOR
//
//ENDPROC

PROC SET_ALL_POTENTIAL_INITIAL_BLIPS_AS_SHORT_RANGE()
	PRINTSTRING(".KGM [At Coords]: SET_ALL_POTENTIAL_INITIAL_BLIPS_AS_SHORT_RANGE")
	INT tempLoop
	REPEAT INITIAL_LONG_RANGE_BLIPS_ARRAY_SIZE tempLoop
		IF (g_sLongRangeBlips.potentialInitialBlips[tempLoop].regID != ILLEGAL_AT_COORDS_ID)
		AND (g_sLongRangeBlips.potentialInitialBlips[tempLoop].blipIndex != NULL)
			IF (DOES_BLIP_EXIST(g_sLongRangeBlips.potentialInitialBlips[tempLoop].blipIndex))
				// ...change to long-range
				SET_BLIP_AS_SHORT_RANGE(g_sLongRangeBlips.potentialInitialBlips[tempLoop].blipIndex, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the quick update loop through all missions (this is expensive but needed to get fast visual updates, usually for blips)
PROC Maintain_Expensive_Quick_Update_All_Missions()

	IF NOT (g_sMatcQuickUpdate.isActive)
		EXIT
	ENDIF
	
	INT 	thisSlot			= g_sMatcQuickUpdate.nextSlot
	INT 	lastSlot			= thisSlot + QUICK_UPDATE_NUM_MISSIONS_PER_FRAME
	BOOL	finalStaggeredLoop	= FALSE
	
	IF (lastSlot >= g_numAtCoordsMP)
		lastSlot			= g_numAtCoordsMP
		finalStaggeredLoop	= TRUE
	ENDIF

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Staggered Quick Update. First Slot: ")
		PRINTINT(thisSlot)
		PRINTSTRING(" Last Slot: ")
		PRINTINT(lastSlot)
		IF (finalStaggeredLoop)
			PRINTSTRING(" - FINAL LOOP")
		ENDIF
		PRINTNL()
	#ENDIF
	
	// If no processing has been done yet, then delay under certain conditions
	IF (g_sMatcQuickUpdate.nextSlot = 0)
		// Delay if there are new mission to process
		IF (Get_Number_Of_MissionsAtCoords_In_Band(MATCB_NEW_MISSIONS) > 0)
			#IF IS_DEBUG_BUILD
				PRINTSTRING("        DELAY: distributing new missions") PRINTNL()
			#ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	g_eMatCStages	thisStageID
	g_eMatCBandings	thisBandID
	
	// Need to also check against the end of the array - if missions get deleted then the number of valid missions will drop - this leads to using an array index of -1
	WHILE (thisSlot < lastSlot)
		IF (thisSlot < g_numAtCoordsMP)
			thisStageID	= Get_MissionsAtCoords_Stage_For_Mission(thisSlot)
			thisBandID	= Get_MissionsAtCoords_Band_For_Stage(thisStageID)
			
			Maintain_MissionsAtCoords_Mission_Stage(thisSlot, thisBandID)
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTSTRING("         IGNORE: No Data. Slot ") PRINTINT(thisSlot) PRINTNL()
			#ENDIF
		ENDIF
		
		thisSlot++
	ENDWHILE
	
	IF NOT (finalStaggeredLoop)
		g_sMatcQuickUpdate.nextSlot = lastSlot
		
		EXIT
	ENDIF
	
	// All finished, so perform any actions that need performed at the end of processing
	INT tempLoop = 0
		
	// ...long-range blips
	IF (IS_BIT_SET(g_sMatcQuickUpdate.bitsReasons, QUICK_UPDATE_REASON_BITFLAG_LONG_RANGE_BLIPS))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Display Long-Range Blips") PRINTNL()
//		#ENDIF
		
		// Switch off long-range blips data gathering and display the initital long-range blips?
		g_sLongRangeBlips.gatherDataOnly = FALSE
		
		// Only set them long-range if not in an apartment
		IF NOT (Is_MissionsAtCoords_Player_In_An_Apartment())
			IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
			OR IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
				REPEAT INITIAL_LONG_RANGE_BLIPS_ARRAY_SIZE tempLoop
					IF (g_sLongRangeBlips.potentialInitialBlips[tempLoop].regID != ILLEGAL_AT_COORDS_ID)
					AND (g_sLongRangeBlips.potentialInitialBlips[tempLoop].blipIndex != NULL)
						IF (DOES_BLIP_EXIST(g_sLongRangeBlips.potentialInitialBlips[tempLoop].blipIndex))
							// ...change to long-range
							SET_BLIP_AS_SHORT_RANGE(g_sLongRangeBlips.potentialInitialBlips[tempLoop].blipIndex, FALSE)
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ELSE
//			#IF IS_DEBUG_BUILD
//				PRINTSTRING("      BUT: In apartment - display nothing") PRINTNL()
//			#ENDIF
		ENDIF
	ENDIF
	
	// ...flashing blips
	IF (IS_BIT_SET(g_sMatcQuickUpdate.bitsReasons, QUICK_UPDATE_REASON_BITFLAG_FLASHING_BLIPS))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Flash Newly Unlocked Blips.") PRINTNL()
//		#ENDIF
		
		// Stop Collecting
		g_sMatcFlashingBlipsMP.matcafbCollecting	= FALSE
		g_sMatcFlashingBlipsMP.matcafbFrameExpiry	= 0
		
		// Flash all the blips
		REPEAT MATC_MAX_FLASHING_BLIPS_AT_ONCE tempLoop
			IF (g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofcBlipIndex != NULL)
				SET_BLIP_FLASHES(g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofcBlipIndex, TRUE)
				SET_BLIP_FLASH_TIMER(g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofcBlipIndex, MATC_BLIP_FLASH_TIME_msec)
			ENDIF
		ENDREPEAT
	ENDIF

	// Clear out the control valriables
	g_structQuickUpdateAllMissions	emptyQuickUpdateStruct
	g_sMatcQuickUpdate = emptyQuickUpdateStruct
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Staggered Quick Update - COMPLETE") PRINTNL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Start a quick update loop through all missions (this is expensive but needed to get fast visual updates, usually for blips)
//
// INPUT PARAMS:		paramBitReason			A bitflag containing the reason for the request
PROC Start_Expensive_Quick_Update_All_Missions(INT paramBitReason)

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Staggered Quick Update - all missions: ")
		SWITCH (paramBitReason)
			CASE QUICK_UPDATE_REASON_BITFLAG_LONG_RANGE_BLIPS		PRINTSTRING("LONG-RANGE BLIPS")		BREAK
			CASE QUICK_UPDATE_REASON_BITFLAG_FLASHING_BLIPS			PRINTSTRING("FLASHING BLIPS")			BREAK
			CASE QUICK_UPDATE_REASON_BITFLAG_ALL_MISSIONS			PRINTSTRING("ALL MISSIONS")			BREAK
			DEFAULT													PRINTSTRING("UNKNOWN REASON")			BREAK
		ENDSWITCH
		IF (g_sMatcQuickUpdate.isActive)
			PRINTSTRING("    - ALREADY PROCESSING: Next Slot: ")
			PRINTINT(g_sMatcQuickUpdate.nextSlot)
		ENDIF
		PRINTNL()
	#ENDIF
	
	// Store the reason bit
	SET_BIT(g_sMatcQuickUpdate.bitsReasons, paramBitReason)
	
	// If already processing then nothing else to do
	IF (g_sMatcQuickUpdate.isActive)
		EXIT
	ENDIF
	
	// Activate
	g_sMatcQuickUpdate.isActive	= TRUE
	g_sMatcQuickUpdate.nextSlot	= 0
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain all active Locate Messages showing mission names and details in Mission Corona
// NOTES:	All missions inside Corona range showing an active Locate Message will need their corona maintained, except for missions that have started
PROC Maintain_MissionsAtCoords_Locate_Messages()

	// Don't need to do this if the player is on a mission
	IF NOT (Should_MissionsAtCoords_Coronas_Be_Displayed())
		IF NOT (g_matcBlockAndHideAllMissions AND GET_FM_STRAND_PROGRESS(ciFLOW_STRAND_LOW_RIDER) >= 1 )
			EXIT
		ENDIF
	ENDIF
	
	// Don't need to do this if there are no missions in the Mission Name Band
	g_eMatCBandings	thisBandID		= MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE
	
	IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) = 0)
		EXIT
	ENDIF
	
	// Maintain the Locate messages of all missions only in the Inside Mission Name Range Band in the 'Displaying Mission Name' stage
	// Active Locate Messages will maintain the Locate Message Display,
	// Requested Locate Messages will call a special low-maintenance function to keep the request alive until the script is next scheduled to do real processing
	//		(without this, the request was getting cleaned up since the request wasn't being maintained every frame to keep it alive)
	INT 			firstCoronaSlot	= 0
	INT 			lastCoronaSlot	= 0
	INT 			slotLoop		= 0
	INT				locateMessageID	= NO_LOCATE_MESSAGE_ID
	VECTOR			theCoords		= << 0.0, 0.0, 0.0 >>
	g_eMatCStages	thisStage		= MAX_MISSIONS_AT_COORDS_STAGES
	
	bool				showReserved 		= FALSE	
	INT					thisCreatorID 		
	MP_MISSION_ID_DATA	thisMissionIdData			
	
	firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
	lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
		
	FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
		// Only maintain for a mission if coronas are required for this mission and the mission is not an overlapped mission
		// KGM 12/5/15: BUG 2313555 - Also hide if the PI Menu Hide Job Blips Option has requested it
		// KGM 11/6/15: BUG 2316703 - Also hide if the mission is within a disabled area
		IF (Should_MissionsAtCoords_Mission_Display_A_Corona(slotLoop))
		AND NOT (Is_MissionsAtCoords_Mission_Hidden_By_Overlapping_Corona(slotLoop))
		AND NOT (Is_MissionsAtCoords_Mission_Hidden_By_PI_Menu(slotLoop))
		AND NOT (Is_MissionsAtCoords_Mission_Hidden_By_Disabled_Area(slotLoop))
			locateMessageID	= Get_MissionsAtCoords_Locate_MessageID_From_Bitflags(slotLoop)
			thisStage 		= Get_MissionsAtCoords_Stage_For_Mission(slotLoop)
			
			SWITCH (thisStage)
				// Locate Message Request - call a cheap function intended to keep the request alive until this mission gets its scheduled update
				CASE MATCS_REQUESTING_MISSION_NAME
					IF (locateMessageID != NO_LOCATE_MESSAGE_ID)
						Request_Locate_Message_Cheap_Update_To_Keep_Request_Active(locateMessageID)
					ENDIF
					BREAK
			
				// Locate Message On Display - keep an active Locate Message alive by displaying it each frame
				CASE MATCS_DISPLAYING_MISSION_NAME
				
					theCoords = Get_MissionsAtCoords_Slot_Coords(slotLoop)	
					
					//Check if restriction has been updated if it has updated the stored info to be displayed
					g_eMatCLaunchFailReasonID	restrictionID
					restrictionID	= MATCLFR_NONE
					Check_For_MissionsAtCoords_Mission_Launch_Restriction(slotLoop, restrictionID,false)
					
					// MiniGames should indicate that players are waiting to play in the corona					
					thisCreatorID 		= Get_MissionsAtCoords_Slot_CreatorID(slotLoop)
					thisMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(slotLoop)
					IF (thisCreatorID = FMMC_MINI_GAME_CREATOR_ID)
						IF (Is_This_Mission_Request_Reserved_And_Joinable(thisMissionIdData))
							showReserved = TRUE
						ENDIF
					ENDIF					
					
					if Check_restrictions_changed(slotLoop, restrictionID)		
					or Check_reserved_changed(slotLoop,showReserved)
						//update the mission info being displayed to reset to no restriction.
						Maintain_Locate_Message_Details(slotLoop)
					endif
					
					IF (locateMessageID != NO_LOCATE_MESSAGE_ID)
						IF Is_This_Locate_Message_Ready_To_Use(locateMessageID)
							Display_Locate_Message(locateMessageID, theCoords)
						ENDIF
					ENDIF
						
					BREAK
			ENDSWITCH
		ENDIF
	ENDFOR

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain flag indicating the player is moving fast
// NOTES:	Only maintained when the player is withinName Display range of any missions
PROC Maintain_MissionsAtCoords_Player_Moving_Fast()

	//Ross W: Corona walk in camera now handles what speed the player arrives at and has them tasked accordingly.
	// KGM 12/1/15 [BUG 2190862]: Just realised the result of this is never used now, so preventing any checking
	EXIT

	// Is the player close to any mission blips?
	IF	(Get_Number_Of_MissionsAtCoords_In_Band(MATCB_FOCUS_MISSION) = 0)
	AND	(Get_Number_Of_MissionsAtCoords_In_Band(MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE) = 0)
		// ...no, so don't maintain
		g_sAtCoordsControlsMP.matccPlayerMovingFast = FALSE
		EXIT
	ENDIF
	
	// Player Injured?
	IF (IS_PED_INJURED(PLAYER_PED_ID()))
		// ...no, so don't maintain
		g_sAtCoordsControlsMP.matccPlayerMovingFast = FALSE
		EXIT
	ENDIF
	
	// Running or sprinting?
	IF (IS_PED_RUNNING(PLAYER_PED_ID()))
	OR (IS_PED_SPRINTING(PLAYER_PED_ID()))
		// ...moving fast
		g_sAtCoordsControlsMP.matccPlayerMovingFast = TRUE
		EXIT
	ENDIF
	
	// Not moving fast
	g_sAtCoordsControlsMP.matccPlayerMovingFast = FALSE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if any blips need to be marked as Long-Range
PROC Maintain_MissionsAtCoords_Initial_Actions_Long_Range_Blips()

	// KGM 5/9/13: Going to re-use this data struct to check for when we can activate the 3 of each type long-range blips routine
	//				On activation we'll do an initial pass through all the missions to get a starting display

	IF (g_sMatcInitialActions_TU.matciaLongRangeBlips.matcibCompleted)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("      - Checking if the long-range blips functionality can be switched on") PRINTNL()
	#ENDIF
	
	// Ignore for now if the initial cloud load hasn't been done
	IF NOT (g_sAtCoordsControlsMP.matccInitialDataReady)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("        BUT: the initial cloud load hasn't been completed - try again later") PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Ignore for now if there are no stored activities
	IF (g_numAtCoordsMP = 0)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("        BUT: there are no activities - try again later") PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Ignore for now if there are still new missions to be distributed
	IF (Get_Number_Of_MissionsAtCoords_In_Band(MATCB_NEW_MISSIONS) > 0)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("        BUT: there are still new missions to be distributed - try again later") PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF
	
	INT tempLoop = 0
	
	// Switch on the functionality and perform the initial pass through all the missions to gather the data
	#IF IS_DEBUG_BUILD
		PRINTSTRING("        The initial data is ready, so do an initial data gathering pass through all missions to find the long-range blips") PRINTNL()
	#ENDIF

	// ...switch on for data gathering only
	g_sLongRangeBlips.allowUpdates		= TRUE
	g_sLongRangeBlips.gatherDataOnly	= TRUE
	
	g_structInitialBlips emptyInitialBlipsStruct
	
	// ..clear out the data gathering struct
	REPEAT INITIAL_LONG_RANGE_BLIPS_ARRAY_SIZE tempLoop
		g_sLongRangeBlips.potentialInitialBlips[tempLoop] = emptyInitialBlipsStruct
	ENDREPEAT

	// ...initial data gathering pass through all missions and get the quick update function to display them
	Start_Expensive_Quick_Update_All_Missions(QUICK_UPDATE_REASON_BITFLAG_LONG_RANGE_BLIPS)
		
	#IF IS_DEBUG_BUILD
		PRINTSTRING("        Initial long-range blip data gathering can be classed as COMPLETE - The Quick Update function has been set up to gather the data") PRINTNL()
	#ENDIF

	// ...don't need to do this work again, so mark as complete
	g_sMatcInitialActions_TU.matciaLongRangeBlips.matcibCompleted = TRUE
	
// KGM 5/9/13: The old functionality - just in case we need to re-activate it
//	// This initial action doesn't need done at all if there are any nearby missions
//	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING("        BUT: there is a focus mission - so this initial action can be marked as complete") PRINTNL()
//		#ENDIF
//		
//		g_sMatcInitialActions.matciaLongRangeBlips.matcibCompleted = TRUE
//		
//		EXIT
//	ENDIF
//	
//	// This initial action doesn't need done at all if the game hasn't completed the initial tutorial
//	IF NOT (HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_ID()))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING("        BUT: the player hasn't completed initial ambient tutorials - so this initial action is not required and can be marked as complete") PRINTNL()
//		#ENDIF
//		
//		g_sMatcInitialActions.matciaLongRangeBlips.matcibCompleted = TRUE
//		
//		EXIT
//	ENDIF
//	
//	INT 			firstCoronaSlot		= 0
//	INT 			lastCoronaSlot		= 0
//	INT 			slotLoop			= 0
//	g_eMatCBandings	thisBandID			= MATCB_MISSIONS_OUTSIDE_CORONA_RANGE
//	BLIP_INDEX		thisBlip
//	
//	// If there are nearby blips but none are on the minimap, make them long-range 
//	IF (Get_Number_Of_MissionsAtCoords_In_Band(MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE) > 0)
//	OR (Get_Number_Of_MissionsAtCoords_In_Band(MATCB_MISSIONS_INSIDE_CORONA_RANGE) > 0)
//		// There are nearby blips, so confine the search to them
//		// Look for a blip on the minimap
//		FOR thisBandID = MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE TO MATCB_MISSIONS_INSIDE_CORONA_RANGE
//			IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
//				firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
//				lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
//				
//				FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
//					thisBlip = Get_MissionsAtCoords_Slot_BlipIndex(slotLoop)
//					
//					IF (GET_BLIP_INFO_ID_DISPLAY(thisBlip) = DISPLAY_BOTH)
//					AND (IS_BLIP_ON_MINIMAP(thisBlip))
//						// Found a blip that was on display, so quit
//						#IF IS_DEBUG_BUILD
//							PRINTSTRING("        BUT: there is a nearby blip on the minimap - so this initial action can be marked as complete") PRINTNL()
//						#ENDIF
//						
//						g_sMatcInitialActions.matciaLongRangeBlips.matcibCompleted = TRUE
//						
//						EXIT
//					ENDIF
//				ENDFOR
//			ENDIF
//		ENDFOR
//		
//		// There are no blips on the minimap, so mark all nearby blips as long-range
//		FOR thisBandID = MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE TO MATCB_MISSIONS_INSIDE_CORONA_RANGE
//			IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
//				firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
//				lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
//				
//				FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
//					thisBlip = Get_MissionsAtCoords_Slot_BlipIndex(slotLoop)
//					
//					IF (GET_BLIP_INFO_ID_DISPLAY(thisBlip) = DISPLAY_BOTH)
//						// Found a nearby blip not on the minimap - so display as long-range
//						#IF IS_DEBUG_BUILD
//							PRINTSTRING("NEARBY BLIP ON DISPLAY IS NOT ON MINIMAP - setting as long-range")
//							PRINTNL()
//						#ENDIF
//						
//						SET_BLIP_AS_SHORT_RANGE(thisBlip, FALSE)
//					ENDIF
//				ENDFOR
//			ENDIF
//		ENDFOR
//		
//		EXIT
//	ENDIF
//	
//	// Mark nearby missions as long-range
//	CONST_INT		MAXIMUM_BLIPPED_MISSIONS		6
//	
//	INT				totalMissionsBlipped			= 0
//	
//	thisBandID = MATCB_MISSIONS_OUTSIDE_CORONA_RANGE
//			
//	IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
//		firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
//		lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
//		
//		FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
//			IF (totalMissionsBlipped < MAXIMUM_BLIPPED_MISSIONS)
//				#IF IS_DEBUG_BUILD
//					PRINTSTRING("        BAND: ")
//					PRINTSTRING(Convert_Missions_At_Coords_Banding_To_SameLength_String(thisBandID))
//					PRINTSTRING(" Slot: ")
//					PRINTINT(slotLoop)
//					PRINTSTRING(" - ")
//				#ENDIF
//		
//				thisBlip = Get_MissionsAtCoords_Slot_BlipIndex(slotLoop)
//				
//				IF (GET_BLIP_INFO_ID_DISPLAY(thisBlip) = DISPLAY_BOTH)
//					#IF IS_DEBUG_BUILD
//						PRINTSTRING("BLIP ON DISPLAY - setting as long-range")
//						PRINTNL()
//					#ENDIF
//					
//					SET_BLIP_AS_SHORT_RANGE(thisBlip, FALSE)
//					
//					totalMissionsBlipped++
//				ELSE
//					#IF IS_DEBUG_BUILD
//						PRINTSTRING("IGNORE - blip not on display")
//						PRINTNL()
//					#ENDIF
//				ENDIF
//			ENDIF
//		ENDFOR
//	ENDIF
//
//	// Finished?
//	IF (totalMissionsBlipped > 0)
//		// ...yes, some blips were displayed long-range
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING("        TOTAL LONG-RANGE BLIPS: ")
//			PRINTINT(totalMissionsBlipped)
//			PRINTSTRING(" - setting this initial action as complete")
//			PRINTNL()
//		#ENDIF
//
//		g_sMatcInitialActions.matciaLongRangeBlips.matcibCompleted = TRUE
//		
//		EXIT
//	ENDIF
//		
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: No blips were found near player's initial position: ")
//		IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
//			PRINTVECTOR(GET_PLAYER_COORDS(PLAYER_ID()))
//		ENDIF
//		PRINTSTRING(" - RESORTING TO A LONG-WINDED FAR AWAY BLIP SEARCH WITH DISTANCE SORTING")
//		PRINTNL()
//	#ENDIF
//	
//	// Perform a distance-sorted long-range blip search to find the nearest blips to mark as long-range
//	FLOAT	nearestBlipsDistances[MAXIMUM_BLIPPED_MISSIONS]
//	INT		nearestBlipsArrayPos[MAXIMUM_BLIPPED_MISSIONS]
//	
//	INT		nearBlipsLoop	= 0
//	
//	// Set the values to initial defaults
//	REPEAT MAXIMUM_BLIPPED_MISSIONS nearBlipsLoop
//		nearestBlipsDistances[nearBlipsLoop]	= 99999.99
//		nearestBlipsArrayPos[nearBlipsLoop]		= ILLEGAL_ARRAY_POSITION
//	ENDREPEAT
//	
//	// Go through all the far away blips finding the closest few to the player
//	IF (IS_PED_INJURED(PLAYER_PED_ID()))
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: Player is injured so can't currently get player's position - marking 'long-range blips' initial action as complete") PRINTNL()
//		#ENDIF
//
//		g_sMatcInitialActions.matciaLongRangeBlips.matcibCompleted = TRUE
//		
//		EXIT
//	ENDIF
//	
//	VECTOR	playerCoords = GET_PLAYER_COORDS(PLAYER_ID())
//	
//	thisBandID = MATCB_MISSIONS_FAR_AWAY_RANGE
//	IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) <= 0)
//		#IF IS_DEBUG_BUILD
//			PRINTSTRING(".KGM [At Coords]: There are no blips in the 'Far Away' band - marking 'long-range blips' initial action as complete") PRINTNL()
//		#ENDIF
//
//		g_sMatcInitialActions.matciaLongRangeBlips.matcibCompleted = TRUE
//		
//		EXIT
//	ENDIF
//	
//	firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
//	lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
//	
//	FLOAT	thisDistance	= 0.0
//	INT		lastArrayPos	= MAXIMUM_BLIPPED_MISSIONS - 1
//	INT		theFrom			= 0
//	INT		theTo			= 0
//	
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING(".KGM [At Coords]: Long-Winded Search through the blips in the Far Away band") PRINTNL()
//	#ENDIF
//	
//	FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
//		thisBlip = Get_MissionsAtCoords_Slot_BlipIndex(slotLoop)
//				
//		IF (GET_BLIP_INFO_ID_DISPLAY(thisBlip) = DISPLAY_BOTH)
//			// Is the distance between the player and this blip one of the closest so far?
//			thisDistance = VDIST(playerCoords, Get_MissionsAtCoords_Slot_Coords(slotLoop))
//			
//			#IF IS_DEBUG_BUILD
//				PRINTSTRING("      Slot ")
//				PRINTINT(slotLoop)
//				PRINTSTRING(": Distance = ")
//				PRINTFLOAT(thisDistance)
//			#ENDIF
//			
//			IF (thisDistance < nearestBlipsDistances[lastArrayPos])
//				// ...this is one of the closest blips so far, so shuffle the array
//				theTo	= lastArrayPos
//				theFrom	= theTo - 1
//				
//				WHILE (theFrom >= 0)
//					IF (thisDistance < nearestBlipsDistances[theFrom])
//						// ...the new blip is closer than the 'from' position, so need to shuffle the from position down
//						nearestBlipsDistances[theTo]	= nearestBlipsDistances[theFrom]
//						nearestBlipsArrayPos[theTo]		= nearestBlipsArrayPos[theFrom]
//						
//						theFrom--
//						theTo--
//					ELSE
//						// ...found the slot to put the new blip in, so quit the shuffle
//						theFrom = -1
//					ENDIF
//				ENDWHILE
//				
//				// Store the Blip
//				nearestBlipsDistances[theTo]	= thisDistance
//				nearestBlipsArrayPos[theTo]		= slotLoop
//				
//				#IF IS_DEBUG_BUILD
//					PRINTSTRING(" [Storing as one of the closest so far] - Max Closest Distance is now: ")
//					PRINTFLOAT(nearestBlipsDistances[lastArrayPos])
//				#ENDIF
//			ENDIF
//			
//			#IF IS_DEBUG_BUILD
//				PRINTNL()
//			#ENDIF
//		ELSE
//			#IF IS_DEBUG_BUILD
//				PRINTSTRING("      Slot ")
//				PRINTINT(slotLoop)
//				PRINTSTRING(": Blip is not on display - IGNORED")
//				PRINTNL()
//			#ENDIF
//		ENDIF
//	ENDFOR
//	
//	// Make the nearest blips long-range
//	#IF IS_DEBUG_BUILD
//		PRINTNL() PRINTSTRING(".KGM [At Coords]: Marking closest 'Far Away' blips as long-range:") PRINTNL()
//	#ENDIF
//	
//	REPEAT MAXIMUM_BLIPPED_MISSIONS nearBlipsLoop
//		IF NOT (nearestBlipsArrayPos[nearBlipsLoop] = ILLEGAL_ARRAY_POSITION)
//			thisBlip = Get_MissionsAtCoords_Slot_BlipIndex(nearestBlipsArrayPos[nearBlipsLoop])
//			
//			SET_BLIP_AS_SHORT_RANGE(thisBlip, FALSE)
//			
//			#IF IS_DEBUG_BUILD
//				PRINTSTRING("      Slot ")
//				PRINTINT(nearestBlipsArrayPos[nearBlipsLoop])
//				PRINTSTRING(": Distance = ")
//				PRINTFLOAT(nearestBlipsDistances[nearBlipsLoop])
//				PRINTSTRING(" - MARKING BLIP AS LONG-RANGE")
//				PRINTNL()
//			#ENDIF
//		ENDIF
//	ENDREPEAT
//
//	// Marking this initial action as complete
//	g_sMatcInitialActions.matciaLongRangeBlips.matcibCompleted = TRUE
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the initial spawn vector still needs to be maintained
PROC Maintain_MissionsAtCoords_Initial_Actions_Initial_Spawn()

	IF (g_sMatcInitialActions.matciaSpawnCoords.matcisCompleted)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("      - Checking if the player's initial spawn vector still needs maintained") PRINTNL()
	#ENDIF
	
	// Added By Bobby 14/6/13: Get out if we are JIPPING a mission
	// JIPPING a mission now fake sets the initial spawn location to the mission coords so this makes sure the initial spawn location isn't cleared too quickly
	IF SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()
		#IF IS_DEBUG_BUILD
			PRINTSTRING("        BUT: SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION() is still TRUE - try again later") PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Ignore for now if there are no stored activities
	IF (g_numAtCoordsMP = 0)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("        BUT: there are no activities - try again later") PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Ignore for now if there are still new missions to be distributed
	IF (Get_Number_Of_MissionsAtCoords_In_Band(MATCB_NEW_MISSIONS) > 0)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("        BUT: there are still new missions to be distributed - try again later") PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Delay if the player is not yet in the game
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
		#IF IS_DEBUG_BUILD
			PRINTSTRING("        BUT: the player is not OK - try again later") PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Delay if the player is injured
	IF (IS_PED_INJURED(PLAYER_PED_ID()))
		#IF IS_DEBUG_BUILD
			PRINTSTRING("        BUT: the player ped is injured - try again later") PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Check if the player is still near the spawn location
	FLOAT	theTolerance	= ciIN_LOCATE_DISTANCE + CORONA_GENERAL_COSMETIC_LEEWAY_m
	VECTOR	playerCoords	= GET_PLAYER_COORDS(PLAYER_ID())
	
	IF (ARE_VECTORS_ALMOST_EQUAL(g_vInitialSpawnLocation, playerCoords, theTolerance))
		#IF IS_DEBUG_BUILD
			PRINTSTRING("        BUT: the player is still near the spawn location - try again later") PRINTNL()
			PRINTSTRING("             PLAYER: ")
			PRINTVECTOR(playerCoords)
			PRINTSTRING("   SPAWN: ")
			PRINTVECTOR(g_vInitialSpawnLocation)
			PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF

	// Finished, so clear the initial spawn vector
	g_vInitialSpawnLocation = << 0.0, 0.0, 0.0 >>
	PRINTLN("g_vInitialSpawnLocation = ", g_vInitialSpawnLocation)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("        Player has moved away from the initial spawn location, so clearing the initial spawn location - setting this initial action as complete")
		PRINTNL()
	#ENDIF

	g_sMatcInitialActions.matciaSpawnCoords.matcisCompleted = TRUE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the initial use of the expensive Player Rank function still needs to be used
PROC Maintain_MissionsAtCoords_Initial_Actions_Player_Rank()

	IF (g_sMatcInitialActions.matciaPlayerRank.matcirUseCheapFunction)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("      - Checking if we can switch to using the cheaper Player Rank function") PRINTNL()
	#ENDIF
	
	// Delay if the player is not yet in the game
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
		#IF IS_DEBUG_BUILD
			PRINTSTRING("        BUT: the player is not OK - try again later") PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Check if the cheaper Player Rank check is now returning the same value as the expensive Player Rank check (which will have alredy been carried out and stored, so use stored value)
	INT cheapPlayerRank			= GET_PLAYER_RANK(PLAYER_ID())
	
	IF (cheapPlayerRank != g_sAtCoordsControlsMP.matccPlayerRank)
		// Cheap Player Rank still isn't returning same value as expensive Player Rank, so keep using expensive player rank
		#IF IS_DEBUG_BUILD
			PRINTSTRING("        BUT: than rank values returned are still different, so keep using expensive rank check. Cheap Rank: ")
			PRINTINT(cheapPlayerRank)
			PRINTSTRING("   Expensive Rank: ")
			PRINTINT(g_sAtCoordsControlsMP.matccPlayerRank)
			PRINTNL()
		#ENDIF
		
		EXIT
	ENDIF

	// Cheap Rank has now started to return relevant values, so switch to using it
	#IF IS_DEBUG_BUILD
		PRINTSTRING("        GET_PLAYER_RANK() is now returning relevant values matching the expensive function, so switch to using the cheap function. Player Rank: ")
		PRINTINT(cheapPlayerRank)
		PRINTNL()
	#ENDIF

	g_sMatcInitialActions.matciaPlayerRank.matcirUseCheapFunction = TRUE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain any initial actions to be performed as a one-off when the player first enters the game
PROC Maintain_MissionsAtCoords_Initial_Actions()

	IF (g_sMatcInitialActions.matciaAllComplete)
		EXIT
	ENDIF
	
	// Still performing initial actions, so check for timeout
	IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sMatcInitialActions.matciaScheduledTimeout))
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Initial Actions check.") PRINTNL()
	#ENDIF
	
	// Scheduling timer has expired, so reset it
	g_sMatcInitialActions.matciaScheduledTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), INITIAL_ACTIONS_SCHEDULING_DELAY_msec)
	
	// Maintain individual initial actions
	Maintain_MissionsAtCoords_Initial_Actions_Long_Range_Blips()
	Maintain_MissionsAtCoords_Initial_Actions_Initial_Spawn()
	Maintain_MissionsAtCoords_Initial_Actions_Player_Rank()
	
	// All initial actions complete?
	IF NOT (g_sMatcInitialActions.matciaLongRangeBlips.matcibCompleted)
	OR NOT (g_sMatcInitialActions.matciaSpawnCoords.matcisCompleted)
	OR NOT (g_sMatcInitialActions.matciaPlayerRank.matcirUseCheapFunction)
		// ...no, need to check again when the scheduling timer expires again
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: All initial actions complete.") PRINTNL()
	#ENDIF
	
	// Yes, so don't do any more work with initial actions
	g_sMatcInitialActions.matciaAllComplete = TRUE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the player rank using either the cheap or expensive function as appropriate
PROC Maintain_MissionsAtCoords_Player_Rank()

	IF (g_sMatcInitialActions.matciaPlayerRank.matcirUseCheapFunction)
		// Ok to use cheap function - it is now returning initialised data
		g_sAtCoordsControlsMP.matccPlayerRank = GET_PLAYER_RANK(PLAYER_ID())
		
		EXIT
	ENDIF
	
	// Still need to use the expensive function
	g_sAtCoordsControlsMP.matccPlayerRank = GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain Flashing Blips
// NOTES:	KGM 22/8/13: Dave will let me know when the help text is on and I'll do a processor intensive update all so that the new blip details can be gathered and flashed
PROC Maintain_MissionsAtCoords_Flashing_Blips()

	IF NOT (HAS_FM_ACTIVITY_UNLOCK_HELP_TEXT_JUST_BEEN_DISPLAYED())
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Blip Help on screen, Flash Blips now") PRINTNL()
	#ENDIF
	
	INT tempLoop = 0

	// Clear out the struct
	REPEAT MATC_MAX_FLASHING_BLIPS_AT_ONCE tempLoop
		g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofbMissionID	= eNULL_MISSION
		g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofbDistance	= 0.0
		g_sMatcFlashingBlipsMP.matcafbBlips[tempLoop].matcofcBlipIndex	= NULL
	ENDREPEAT
	
	g_sMatcFlashingBlipsMP.matcafbCollecting	= TRUE
	g_sMatcFlashingBlipsMP.matcafbFrameExpiry	= (GET_FRAME_COUNT() + g_numAtCoordsMP)
	
	// The help text is updated and requires the blips to flash, so do an update of all missions which will gather data about flashing blips as it goes
	Start_Expensive_Quick_Update_All_Missions(QUICK_UPDATE_REASON_BITFLAG_FLASHING_BLIPS)

// KGM 22/8/13: Don't need these two checks anymore - they were for staggered collection (I'll comment out for now in case we need them again)
//	IF NOT (g_sMatcFlashingBlipsMP.matcafbCollecting)
//		EXIT
//	ENDIF
//	
//	IF (GET_FRAME_COUNT() < g_sMatcFlashingBlipsMP.matcafbFrameExpiry)
//		EXIT
//	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Retained Gang Attack radius blip
// NOTES:	This gets deleted if the Gang ATtack is no longer the focus mission or if teh Gang ATtack itself has requested removal
PROC Maintain_MissionsAtCoords_Gang_Attack_Radius_Blip()

	// Store the removal request so that the global can be cleared
	BOOL removeRetainedBlip = g_removeRetainedGABlip

	g_removeRetainedGABlip = FALSE
	
	IF (g_sMatcRetainedRadiusBlip.mrbBlipIndex = NULL)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (removeRetainedBlip)
			PRINTSTRING(".KGM [At Coords]: Retained Gang Attack Blip - delete. External Request.") PRINTNL()
		ENDIF
	#ENDIF
	
	// There is a Retained GA blip
	IF NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
		// ...there is no longer a focus mission, so remove the blip
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Retained Gang Attack Blip - delete. No Focus Mission") PRINTNL()
		#ENDIF
		
		removeRetainedBlip = TRUE
	ENDIF
	
	// Remove the blip?
	IF NOT (removeRetainedBlip)
		EXIT
	ENDIF
	
	// Clean up
	REMOVE_BLIP(g_sMatcRetainedRadiusBlip.mrbBlipIndex)
	g_sMatcRetainedRadiusBlip.mrbBlipIndex	= NULL
	g_sMatcRetainedRadiusBlip.mrbRegID		= ILLEGAL_AT_COORDS_ID

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the (currently) one Area Triggered blip near the player
// NOTES:	If a mission is currently using the area-triggered blip then check if it still needs it, otherwise check if a mission wants to use it
PROC Maintain_MissionsAtCoords_Area_Triggered_Blips()

	INT 			firstCoronaSlot	= 0
	INT 			lastCoronaSlot	= 0
	INT 			bandLoop		= 0
	INT 			slotLoop		= 0
	g_eMatCBandings	thisBandID
	BLIP_INDEX		thisBlip		= NULL
	BLIP_INDEX		radiusBlip		= NULL
	VECTOR			theCoords		= << 0.0, 0.0, 0.0 >>
	INT				theColour		= BLIP_COLOUR_WHITE
	
	IF (g_sMatcRadiusBlip.mrbRegID != ILLEGAL_AT_COORDS_ID)
		// ...there is an existing mission using this blip, so check if it still needs to use it
		FOR bandLoop = MATCB_FOCUS_MISSION TO MATCB_MISSIONS_INSIDE_CORONA_RANGE
			thisBandID = INT_TO_ENUM(g_eMatCBandings, bandLoop)
			
			IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
				firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
				lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
				
				FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
					IF (Get_MissionsAtCoords_RegistrationID_For_Mission(slotLoop) = g_sMatcRadiusBlip.mrbRegID)
						// ...found the mission, so check if it is a Gang Attack that has become the Focus Mission and setup the 'retained' blip instead
						IF (Is_There_A_MissionsAtCoords_Focus_Mission())
						AND (thisBandID = MATCB_FOCUS_MISSION)
							IF (Get_MissionsAtCoords_Slot_MissionID(slotLoop) = eFM_GANG_ATTACK_CLOUD)
								// This is a Gang Attack that has just become the Focus Mission, so try to set up the retained blip
								Setup_Retained_Gang_Attack_Radius_Blip(slotLoop)
							ENDIF
						ENDIF
						
						// Check if the mission blip is still on display - if it is, keep this one on display too
						thisBlip = Get_MissionsAtCoords_Slot_BlipIndex(slotLoop)
						IF (GET_BLIP_INFO_ID_DISPLAY(thisBlip) != DISPLAY_NOTHING)
							// ...still required
							EXIT
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDFOR
		
		// Failed to find the mission or the mission's own blip is not on display, so the Area-Triggered blip is not required
		#IF IS_DEBUG_BUILD
			PRINTSTRING(".KGM [At Coords]: Area-Triggered Blip - Delete. RegID: ") PRINTINT(g_sMatcRadiusBlip.mrbRegID) PRINTNL()
		#ENDIF
		
		// Clean up
		REMOVE_BLIP(g_sMatcRadiusBlip.mrbBlipIndex)
		g_sMatcRadiusBlip.mrbBlipIndex	= NULL
		g_sMatcRadiusBlip.mrbRegID		= ILLEGAL_AT_COORDS_ID
		
		EXIT
	ENDIF
	
	// The Area-Triggered blip is currently unused, so check if a mission wants to use it
	FOR bandLoop = MATCB_FOCUS_MISSION TO MATCB_MISSIONS_INSIDE_CORONA_RANGE
		thisBandID = INT_TO_ENUM(g_eMatCBandings, bandLoop)
		
		IF (Get_Number_Of_MissionsAtCoords_In_Band(thisBandID) > 0)
			firstCoronaSlot	= Get_First_MissionsAtCoords_Slot_For_Band(thisBandID)
			lastCoronaSlot	= Get_Last_MissionsAtCoords_Slot_For_Band(thisBandID)
			
			FOR slotLoop = firstCoronaSlot TO lastCoronaSlot
				// Does this slot contain an area-triggered mission and there is access to the cloud
				IF (Is_MissionsAtCoords_Mission_Area_Triggered(slotLoop))
					IF NOT (IS_CLOUD_DOWN_CLOUD_LOADER())
						// ...yes area-triggered and access to the cloud, so check if the mission's own blip is currently on display
						thisBlip = Get_MissionsAtCoords_Slot_BlipIndex(slotLoop)
						IF (GET_BLIP_INFO_ID_DISPLAY(thisBlip) != DISPLAY_NOTHING)
							// ...yes on display, so claim the area-triggered blip and display it too
							#IF IS_DEBUG_BUILD
								PRINTSTRING(".KGM [At Coords]: Area-Triggered Blip - Add. RegID: ")
								PRINTINT(Get_MissionsAtCoords_RegistrationID_For_Mission(slotLoop))
								PRINTNL()
								PRINTSTRING("          ")
								Debug_Output_Missions_At_Coords_Slot_In_One_Line(slotLoop)
								PRINTNL()
							#ENDIF
			
							theCoords	= Get_MissionsAtCoords_Slot_Coords(slotLoop)
							theColour	= GET_BLIP_COLOUR(thisBlip)
							radiusBlip	= Add_And_Display_MissionsAtCoords_Area_Triggered_Blip(theCoords, theColour)
							
							IF (radiusBlip != NULL)
								g_sMatcRadiusBlip.mrbBlipIndex	= radiusBlip
								g_sMatcRadiusBlip.mrbRegID		= Get_MissionsAtCoords_RegistrationID_For_Mission(slotLoop)
							
								EXIT
							ENDIF
						ENDIF
					ELSE
						PRINTLN(".KGM [At Coords]: Area-Triggered Blip - DELAY ADD - The Cloud Is Down")
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the 'free' Play Again flag - allowing a poor play to continue playing missions again after completing one (bug 1532789)
// NOTES:	If a mission is currently using the area-triggered blip then check if it still needs it, otherwise check if a mission wants to use it
PROC Maintain_MissionsAtCoords_Free_Play_Again()

	IF NOT (g_allowPoorPlayerToPlayAgainForFree)
		EXIT
	ENDIF
	
	// Maintain this while the player is in the corona or has a focus mission
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
	OR (IS_PLAYER_IN_CORONA())
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: 'Free To Play' - CLEARED") PRINTNL()
	#ENDIF
	
	g_allowPoorPlayerToPlayAgainForFree = FALSE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain anything that needs to change based on the player entering and leaving an apartment
// NOTES:	This was originally added to maintain the nearby long-range blips
PROC Maintain_MissionsAtCoords_In_Apartment()

	BOOL isInApartment	= Is_MissionsAtCoords_Player_In_An_Apartment()
	
	IF (isInApartment = g_sAtCoordsControlsMP_AddTU.matccWasInApartment)
		// ...nothing has changed
		EXIT
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		PRINTLN(".KGM [At Coords]: Maintain_MissionsAtCoords_In_Apartment NETWORK_IS_ACTIVITY_SESSION")
		EXIT
	ENDIF
	
	// The player has either entered or left an apartment, so update the previous state variable
	g_sAtCoordsControlsMP_AddTU.matccWasInApartment = isInApartment
	
	// Is the player now in an apartment?
	#IF IS_DEBUG_BUILD
	IF (isInApartment)
		PRINTLN(".KGM [At Coords]: Maintain_MissionsAtCoords_In_Apartment In apartment, long-range blips cleared")
	ELSE
		PRINTLN(".KGM [At Coords]: Maintain_MissionsAtCoords_In_Apartment Not in apartment, long-range blips displayed")
	ENDIF
	#ENDIF

	// Update all mission blips on the nearby long-range array
	IF NOT (g_sLongRangeBlips.allowUpdates)
		EXIT
	ENDIF

	INT	tempLoop = 0
	g_eLongRangeBlipTypes longRangeBlipType = NO_LONG_RANGE_BLIP_TYPE
	INT lrBlipArrayPos = 0
	
	REPEAT g_numAtCoordsMP tempLoop
		longRangeBlipType = Get_MissionsAtCoords_Long_Range_Blip_Type(tempLoop)
		
		IF (longRangeBlipType != NO_LONG_RANGE_BLIP_TYPE)
			lrBlipArrayPos = Get_Position_Of_Mission_On_Nearby_Missions_Array(tempLoop, longRangeBlipType)
			
			IF (lrBlipArrayPos != ILLEGAL_ARRAY_POSITION)
				// ...yes, this mission was one of the closest of its type
				Update_Mission_On_Nearby_Missions_Array(tempLoop, longRangeBlipType, lrBlipArrayPos)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF g_matcBlockAndHideAllMissions
		PRINTLN(".KGM [At Coords]: Maintain_MissionsAtCoords_In_Apartment g_matcBlockAndHideAllMissions")
		EXIT
	ENDIF
	
	IF ARE_TEMP_HIDDEN_BLIPS_ACTIVE()
		PRINTLN(".KGM [At Coords]: Maintain_MissionsAtCoords_In_Apartment ARE_TEMP_HIDDEN_BLIPS_ACTIVE")
		EXIT
	ENDIF
	
	PRINTLN(".KGM [At Coords]: Maintain_MissionsAtCoords_In_Apartment Display_MissionsAtCoords_Blip_For_Mission")
	
	// Show all blips that should be on show
	REPEAT g_numAtCoordsMP tempLoop
		Display_MissionsAtCoords_Blip_For_Mission(tempLoop)
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Update the alpha of all nearby long-range blips.
// NOTES:	KGM 29/9/14 {BUG 2057685]: Used in NextGen only to fade out the nearby long-range blips based on their distance
PROC Maintain_MissionsAtCoords_Nearby_Long_Range_Blip_Alphas()

	// Allow Update all mission blips on the nearby long-range array?
	IF NOT (g_sLongRangeBlips.allowUpdates)
		EXIT
	ENDIF
	
	// Update the alphas of all nearby long-range blips
	INT blipTypeLoop = 0
	INT blipLoop = 0
	
	// ...loop through each main blip type
	REPEAT MAX_LONG_RANGE_BLIP_TYPES blipTypeLoop
		// ...loop through nearest blips of each main blip type, ensuring the min alpha doesn't get set to 0
		REPEAT MAX_NUM_LONG_RANGE_BLIPS_PER_TYPE blipLoop
			IF DOES_BLIP_EXIST(g_sLongRangeBlips.allBlips[blipTypeLoop].blipsOfType[blipLoop].blipIndex)
				SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(g_sLongRangeBlips.allBlips[blipTypeLoop].blipsOfType[blipLoop].blipIndex, MIN_BLIP_ALPHA_FOR_DIST_ON_MISSION)
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Update the alpha of all nearby long-range blips.
// NOTES:	KGM 29/9/14 {BUG 2057685]: Used in NextGen only to fade out the nearby long-range blips based on their distance
PROC Maintain_MissionsAtCoords_DpadRight_Help_Text_Cleanup()

	// Is the 'keep alive' timer active?
	IF (g_matcDpadRightHelpFrameTimeout = 0)
		EXIT
	ENDIF
	
	// Has the timer timed out?
	IF (GET_FRAME_COUNT() <= g_matcDpadRightHelpFrameTimeout)
		EXIT
	ENDIF
	
	// Timed Out, so clear the help if on display and clear the timer
	g_matcDpadRightHelpFrameTimeout = 0
	g_sV2CoronaVars.g_isplayer_inside_corona = FALSE
	PRINTLN("g_sV2CoronaVars.g_isplayer_inside_corona = FALSE 9")
	// Clear 'Press DPADRIGHT' help text if appropriate
	IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MATC_DPADRIGHT"))
		PRINTLN(".KGM [At Coords][DPadRight]: Maintain_MissionsAtCoords_DpadRight_Help_Text_Cleanup() Clear 'Press DPAD-RIGHT' help message [auto-cleanup]")
		CLEAR_HELP()
	ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Some display features (corona, etc) need run every frame to maintain them
PROC Maintain_MissionsAtCoords_Every_Frame()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("Maintain_MissionsAtCoords_Every_Frame")
	#ENDIF
	#ENDIF
	
	// KGM 12/5/15 [BUG 2313555]: Check if there needs to be a full update because of a PI Menu Hide/Show Blips option change
	Maintain_MissionsAtCoords_PI_Menu_Change_Forced_Update()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_PI_Menu_Change_Forced_Update")
	#ENDIF
	#ENDIF
	
	// Maintain Wanted Level (do this before maintaining blips)
	Maintain_MissionsAtCoords_Wanted_Level()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Wanted_Level")
	#ENDIF
	#ENDIF
	
	// Maintain mission blips
	Maintain_MissionsAtCoords_Blips()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Blips")
	#ENDIF
	#ENDIF
	
	// Maintain Player In Interior, and update Interior Only blips if the state has changed
	Maintain_MissionsAtCoords_Player_In_Interior_And_Interior_Only_Blips()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Player_In_Interior_And_Interior_Only_Blips")
	#ENDIF
	#ENDIF

	// Maintain mission coronas (includes updating SCTV data storage to be used if this player is SPECTATED)
	PreUpdate_Corona_Data_Storage_For_SCTV()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PreUpdate_Corona_Data_Storage_For_SCTV")
	#ENDIF
	#ENDIF

	Maintain_Missions_At_Coords_Paused_Details_For_SCTV()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_Missions_At_Coords_Paused_Details_For_SCTV")
	#ENDIF
	#ENDIF

	Maintain_MissionsAtCoords_Coronas()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Coronas")
	#ENDIF
	#ENDIF

	PostUpdate_Corona_Data_Storage_For_SCTV()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PostUpdate_Corona_Data_Storage_For_SCTV")
	#ENDIF
	#ENDIF

	// Maintain focus mission corona visuals
	Maintain_MissionsAtCoords_Focus_Mission_Corona_Visuals()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Focus_Mission_Corona_Visuals")
	#ENDIF
	#ENDIF
	
	// Maintain corona icons
// KGM 27/7/13: Corona Icons are no longer being displayed
//	Maintain_MissionsAtCoords_Corona_Icons()
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE 
//		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Corona_Icons")
//	#ENDIF
//	#ENDIF
	
	// Maintain corona ground projection TXD (only)
	// Individual missions switch ground projections on and off as required outside this maintenance function
	Maintain_MissionsAtCoords_Corona_Ground_Projection_TXD()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Corona_Ground_Projections")
	#ENDIF
	#ENDIF
	
	// Maintain Locate Messages showing the mission name in the corona
	Maintain_MissionsAtCoords_Locate_Messages()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Locate_Messages")
	#ENDIF
	#ENDIF
	
	// Maintain the Feedback array, periodically deleting old entries
	Maintain_MissionsAtCoords_Feedback_Entries()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Feedback_Entries")
	#ENDIF
	#ENDIF
	
	// Maintain Player moving fast (this will prevent the triggering of blips)
	Maintain_MissionsAtCoords_Player_Moving_Fast()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Player_Moving_Fast")
	#ENDIF
	#ENDIF
	
	// Maintain the Player's Rank for this frame
	// NOTE: Call this before calling the 'Initial Actions' fucntion below
	Maintain_MissionsAtCoords_Player_Rank()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Player_Rank")
	#ENDIF
	#ENDIF
	
	// Ensures newly unlocked activitiy blips flash in unison (and only a limited number flash)
	Maintain_MissionsAtCoords_Flashing_Blips()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Flashing_Blips")
	#ENDIF
	#ENDIF
	
	// Maintain the Area-Triggered blip near the player
	Maintain_MissionsAtCoords_Area_Triggered_Blips()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Area_Triggered_Blips")
	#ENDIF
	#ENDIF
	
	// Maintain the retained Gang Attack Radius blip
	Maintain_MissionsAtCoords_Gang_Attack_Radius_Blip()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Gang_Attack_Radius_Blip")
	#ENDIF
	#ENDIF
	
	// Maintain the 'free to play again' flag while the player continues to have a focus mission
	Maintain_MissionsAtCoords_Free_Play_Again()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Free_Play_Again")
	#ENDIF
	#ENDIF
	
	// Maintain switching of the nearby long-range blips on and off when the player enters and leaves an apartment
	Maintain_MissionsAtCoords_In_Apartment()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_In_Apartment")
	#ENDIF
	#ENDIF
	
	// KGM 29/9/14 [BUG 2057685]: Maintain the alpha of the nearby long-range blips based on distance from player
	Maintain_MissionsAtCoords_Nearby_Long_Range_Blip_Alphas()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Nearby_Long_Range_Blip_Alphas")
	#ENDIF
	#ENDIF
	
	// Maintain Initial Actions - anything performed as a one-off when the player first enters the game
	Maintain_MissionsAtCoords_Initial_Actions()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Initial_Actions")
	#ENDIF
	#ENDIF
	
	// Maintain Expensive Quick Update of all missions, usually for getting blip info
	Maintain_Expensive_Quick_Update_All_Missions()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_Expensive_Quick_Update_All_Missions")
	#ENDIF
	#ENDIF
	
	// Maintain Exclusive ContentID
	Maintain_MissionsAtCoords_Exclusive_ContentID()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Exclusive_ContentID")
	#ENDIF
	#ENDIF
	
	// Maintain Invite Accepted ContentID
	Maintain_MissionsAtCoords_Most_Recent_Invite_Accepted_ContentID()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Most_Recent_Invite_Accepted_ContentID")
	#ENDIF
	#ENDIF

	// Maintain DpadRight Help Text cleanup
	Maintain_MissionsAtCoords_DpadRight_Help_Text_Cleanup()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_DpadRight_Help_Text_Cleanup")
	#ENDIF
	#ENDIF

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF
	
	// DEBUG ERROR CHECKING
	#IF IS_DEBUG_BUILD
		// If the player is not on an Ambient Tutorial, make sure no missions are classed as active during an Ambient Tutorial
		IF NOT (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())
			IF (g_iMatccCounterOnDuringAmbTut > 0)
//				PRINTSTRING(".KGM [At Coords]: ERROR: Player is not on an Ambient Tutorial but the global counter of missions marked as Allow During Ambient Tutorial = ")
//				PRINTINT(g_iMatccCounterOnDuringAmbTut)
//				PRINTNL()
				SCRIPT_ASSERT("g_iMatccCounterOnDuringAmbTut > 0 WHEN NOT ON AN AMBIENT TUTORIAL. Tell Keith.")
			ENDIF
		ENDIF
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Decide if less work should be done this frame.
// NOTES:	This was added to try to minimise processing while Heist-related missions are active
PROC Decide_If_Reduced_Processing_Is_Required_This_Frame()

	#IF IS_DEBUG_BUILD
		BOOL holdReducedProcessing = g_matcUseReducedProcessing
	#ENDIF
	
	g_matcUseReducedProcessing = FALSE
	IF (Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
	OR (Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))
		g_matcUseReducedProcessing = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (holdReducedProcessing != g_matcUseReducedProcessing)
			IF (g_matcUseReducedProcessing)
				PRINTLN(".KGM [At Coords]: REDUCED PROCESSING HAS BEEN SWITCHED ON")
			ELSE
				PRINTLN(".KGM [At Coords]: REDUCED PROCESSING HAS BEEN SWITCHED OFF")
			ENDIF
		ENDIF
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If the 'hide all coronas' flag has changed, then update the 'hide all blips' status to match
// NOTES:	KGM 3/6/15: This is separated out to avoid cyclic headers when calling Block_All_MissionsAtCoords_Missions()
PROC Maintain_All_Blips_To_Match_All_Coronas()

	IF (g_matcBlockAndHideAllMissions = g_matcAllMissionBlipsHidden)
		EXIT
	ENDIF
	
	g_matcAllMissionBlipsHidden = g_matcBlockAndHideAllMissions
	
	#IF IS_DEBUG_BUILD
		NET_PRINT(".KGM [At Coords]: Maintain_All_Blips_To_Match_All_Coronas() detected corona status change to ")
		IF (g_matcBlockAndHideAllMissions)
			NET_PRINT(" TRUE. Also hide all blips.")
		ELSE
			NET_PRINT(" FALSE. Also show all blips.")
		ENDIF
		NET_NL()
	#ENDIF
	
	// Hide/Show all blips
	INT tempLoop = 0
	
	IF (g_matcBlockAndHideAllMissions)
		// Hide all blips
		REPEAT g_numAtCoordsMP tempLoop
			IF NOT Should_MissionAtCoords_Ignore_Block_On_Mission(tempLoop)
			OR SHOULD_HIDE_ALL_JOB_BLIPS_FOR_YACHT()
				Hide_MissionsAtCoords_Blip_For_Mission(tempLoop)
			ENDIF
		ENDREPEAT
		
		EXIT
	ENDIF
	
	// Show all blips that should be on show
	REPEAT g_numAtCoordsMP tempLoop
		Display_MissionsAtCoords_Blip_For_Mission(tempLoop)
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	This procedure should be called every frame to control access to the Mission At Coords systems.
// NOTES:	Missions can get added to and removed from Bands during an update and this can upset the Update Slot for Bands meaning missions can get missed or
//				updated twice during one scheduled update loop. This shouldn't matter. There shouldn't be too much movement between Bands because it's all
//				based on player distance. A full scheduled update loop shouldn't take too many frames to perform so the mission should get updated next loop.
PROC Maintain_MP_Missions_At_Coords()

	// KGM 13/1/15 [BUG 2143257]: Display a delayed 'mission full' help message if excess players are in the Leader's Planning Room when the heist gets setup
	IF (g_matcHeistFullHelpNeeded)
		IF NOT (IS_PLAYER_IN_CORONA())
			// If the player is no longer processing corona routines (so a general 'clear help' has been and gone) - display the help
			PRINT_HELP("HEIST_FULL")
			PRINTLN(".KGM [At Coords][Heist]: Displaying Delayed 'Heist Full' Help Message to player that was exces to requirements in Leader's Planning Room")
			
			g_matcHeistFullHelpNeeded = FALSE
			g_matcHeistFullHelpSafetyFrame = 0
		ELSE
			// Otherwise, check for the safety frame timeout
			IF (GET_FRAME_COUNT() > g_matcHeistFullHelpSafetyFrame)
				PRINTLN(".KGM [At Coords][Heist]: Safety Frame Count timed out without displaying delayed 'Heist Full' Help Message.")
				g_matcHeistFullHelpNeeded = FALSE
				g_matcHeistFullHelpSafetyFrame = 0
			ENDIF
		ENDIF
	ENDIF

	INT	tempLoop = 0
	
	// KGM 12/1/15 [BUG 2190862]: Decide if reduced processing is required this frame
	Decide_If_Reduced_Processing_Is_Required_This_Frame()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("Maintain_MP_Missions_At_Coords")
	#ENDIF
	#ENDIF

	// KGM 15/9/12: Possibly Temp: Always clear the 'keep vote going' flag - it will get set if needed within this maintenance routine and then acted upon by the flow controller maintenance routines
	Clear_My_MissionsAtCoords_Vote_KeepActive_Flag()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Clear_My_MissionsAtCoords_Vote_KeepActive_Flag")
	#ENDIF
	#ENDIF

	// KGM 3/6/15: Check if all blips need hidden/revealed to match the state of the coronas (see Block_All_MissionsAtCoords_Missions())
	Maintain_All_Blips_To_Match_All_Coronas()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_All_Blips_To_Match_All_Coronas")
	#ENDIF
	#ENDIF

	// Ignore this if there are no missions to maintain
	// KGM 14/2/14: But if the player is SCTV then perform some corona display maintenance based on the spectated player's corona data
	// KGM 11/4/14: To cater for starting SCTV on a player in your own session, clearout any setup missions
	IF (IS_PLAYER_SCTV(PLAYER_ID()))
	
		// If the SCTV player is on a mission and not in a corona (ie. rounds / strands) then don't process any mission at coords logic.
		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
			IF NOT IS_PLAYER_IN_CORONA()
				#IF IS_DEBUG_BUILD
				IF GET_GAME_TIMER() % 3000 < 50
					PRINTSTRING(".KGM [At Coords]: Player is SCTV and NOT IS_PLAYER_IN_CORONA- IGNORE UPDATE") PRINTNL()
				ENDIF
				#ENDIF
				
				EXIT
			ENDIF
		ENDIF
	
		Maintain_SCTV_Corona_Data()
		
		IF (g_numAtCoordsMP > 0)
		AND NOT NETWORK_IS_ACTIVITY_SESSION()
			// ...there are missions, so mark a number of them for delete and force an update
			PRINTLN(".KGM [At Coords]: SCTV Player has local missions. Mark for delete. Num Missions:", g_numAtCoordsMP)
			INT processingLimit = 50
			IF (g_numAtCoordsMP < processingLimit)
				processingLimit = g_numAtCoordsMP
			ENDIF
			REPEAT processingLimit tempLoop
				Mark_MissionsAtCoords_Mission_For_Delete(tempLoop)
				Set_MissionsAtCoords_Mission_As_Force_Update(tempLoop)
			ENDREPEAT
		ENDIF
	ENDIF
	
// ORIGINAL ROUTINE - Can be deleted if this works
//	IF (g_numAtCoordsMP = 0)
//		IF (IS_PLAYER_SCTV(PLAYER_ID()))
//			Maintain_SCTV_Corona_Data()
//		ENDIF
//	
//		EXIT
//	ENDIF
	
	// Ignore this if the player is not ok
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
		PRINTSTRING(".KGM [At Coords]: Player is not OK - IGNORE UPDATE") PRINTNL()
		EXIT
	ENDIF
	
	// Fill the Player's Activity details for this frame
	Gather_And_Store_Players_Current_Activity_Details()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Gather_And_Store_Players_Current_Activity_Details")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_PRE_LOOP_MARKER("loop")
	#ENDIF
	#ENDIF

	// Maintain any missions from each Band
	g_eMatCBandings	thisBand
	
	REPEAT MAX_MISSIONS_AT_COORDS_BANDINGS tempLoop
	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		
		#ENDIF
		#ENDIF
	
		thisBand = INT_TO_ENUM(g_eMatCBandings, tempLoop)
		
		SWITCH (thisBand)
			CASE MATCB_FOCUS_MISSION
				Maintain_MissionsAtCoords_Focus_Mission()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Focus_Mission")
				#ENDIF
				#ENDIF
				BREAK
			
			CASE MATCB_MISSIONS_INSIDE_MISSION_NAME_RANGE
				Maintain_MissionsAtCoords_Missions_Inside_Mission_Name_Range()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Missions_Inside_Mission_Name_Range")
				#ENDIF
				#ENDIF
				BREAK
			
			CASE MATCB_MISSIONS_INSIDE_CORONA_RANGE
				Maintain_MissionsAtCoords_Missions_Inside_Corona_Range()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Missions_Inside_Corona_Range")
				#ENDIF
				#ENDIF
				BREAK
				
			CASE MATCB_MISSIONS_OUTSIDE_CORONA_RANGE
				Maintain_MissionsAtCoords_Missions_Outside_Corona_Range()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Missions_Outside_Corona_Range")
				#ENDIF
				#ENDIF
				BREAK
				
			CASE MATCB_MISSIONS_FAR_AWAY_RANGE
				Maintain_MissionsAtCoords_Missions_In_Far_Away_Range()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Missions_In_Far_Away_Range")
				#ENDIF
				#ENDIF
				BREAK
				
			CASE MATCB_NEW_MISSIONS
				Maintain_MissionsAtCoords_New_Missions()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_New_Missions")
				#ENDIF
				#ENDIF
				BREAK
				
			CASE MAX_MISSIONS_AT_COORDS_BANDINGS
				SCRIPT_ASSERT("Maintain_MP_Missions_At_Coords() - Attempting to update MAX_MISSIONS_AT_COORDS_BANDINGS. This should be impossible. Tell Keith.")				
				BREAK
				
			CASE MATCB_MISSIONS_BEING_DELETED
				SCRIPT_ASSERT("Maintain_MP_Missions_At_Coords() - Attempting to update MATCB_MISSIONS_BEING_DELETED. This should be impossible. Tell Keith.")
				BREAK
				
			DEFAULT
//				PRINTSTRING(".KGM [At Coords]: Maintain_MP_Missions_At_Coords(): Unknown Band ID in switch statement: ")
//				PRINTSTRING(Convert_Missions_At_Coords_Banding_To_SameLength_String(thisBand))
//				PRINTNL()
				
				SCRIPT_ASSERT("Maintain_MP_Missions_At_Coords(): UNKNOWN BANDING ID IN SWITCH STATEMENT. See Console Logs. Add new Banding ID to SWITCH. Tell Keith.")
				BREAK
		ENDSWITCH
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_POST_LOOP_MARKER()
	#ENDIF
	#ENDIF
	
	// Maintain any missions that have been forced to update this frame
	Maintain_MissionsAtCoords_Force_Updates()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Force_Updates")
	#ENDIF
	#ENDIF
	
	// Some routines need run every frame
	Maintain_MissionsAtCoords_Every_Frame()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MissionsAtCoords_Every_Frame")
	#ENDIF
	#ENDIF

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Deletes the checkpoints, unpatches the decals, and marks the TXD as no longer needed for any active ground projections
// NOTES:	This is to avoid a TXD leak
PROC Terminate_MP_Missions_At_Coords_Ground_Projections()

	IF NOT (Is_MatC_Corona_Ground_Projection_TXD_Loaded())
		EXIT
	ENDIF
	
	// Ground Projection TXD is loaded, so delete any active checkpoints that use it
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Ground TXDs - Delete and Unpatch") PRINTNL()
	#ENDIF
	
	INT tempLoop = 0
	
	REPEAT MATC_CORONA_MAX_PROJECTIONS tempLoop
		IF (g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].stage = GTXD_STAGE_BEING_RENDERED)
			// ...need to tidy this one up
			Clear_MissionsAtCoords_Corona_Ground_Projection_For_RegID(g_sAtCoordsControlsMP.matccCoronaGroundTXDControl[tempLoop].regID)
		ENDIF
	ENDREPEAT
	
	// Immediately, unpatch the decal
	Maintain_MissionsAtCoords_Corona_Ground_Projection_Decal_Unpatching()
	
	// Mark the TXD as no longer needed
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(MATC_CORONA_PROJECTION_TXD_NAME)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Frees up any Vehicle Generators set at termination
// NOTES:	KGM 18/2/15 [BUG 2238858]: Fix for a potential leak that causes the code array to overfill (ignorable assert)
PROC Terminate_MP_Mission_At_Coords_Vehicle_Generators()

	PRINTSTRING(".KGM [At Coords]: Terminate_MP_Mission_At_Coords_Vehicle_Generators() - Clearing up all active Vehicle Generators Blocks")

	INT tempLoop = 0

	REPEAT g_numAtCoordsMP tempLoop
		IF (IS_BIT_SET(g_sAtCoordsMP[tempLoop].matcStateBitflags, MATC_BITFLAG_VEHICLE_GENERATORS_BLOCKED))
			Change_Active_State_Of_Vehicles_Generators_Near_MissionsAtCoords_Corona(tempLoop, TRUE)
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Cleanup any assets that need to be removed
// NOTES:	Added for: Speed Zones and Scenario Blocking
PROC Terminate_MP_Missions_At_Coords()

	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: Terminate_MP_Missions_At_Coords") PRINTNL()
	#ENDIF

	INT tempLoop = 0
		
	// Clear the SpeedZones array, deleting any setup SpeedZones
	REPEAT MAX_MATC_SPEED_ZONES tempLoop
		Clear_And_Delete_One_MP_MatC_Speed_Zone(tempLoop)
	ENDREPEAT
		
	// Clear the ScenarioBlocking array, deleting any setup ScenarioBlocking
	REPEAT MAX_MATC_SCENARIO_BLOCKING tempLoop
		Clear_And_Delete_One_MP_MatC_Scenario_Blocking(tempLoop)
	ENDREPEAT
	
	// Cleanup any Ground Projections on termination
	Terminate_MP_Missions_At_Coords_Ground_Projections()
	
	// Clean up any Vehicle Generators that have been switched off on Termination
	Terminate_MP_Mission_At_Coords_Vehicle_Generators()
	
	// Also clear the Invite Accepted ContentID variables
	g_sAtCoordsControlsMP_AddTU.matccActivityContentID = 0
	#IF IS_DEBUG_BUILD
		g_sAtCoordsControlsMP_AddTU.matccDebugInviteAcceptContentID = ""
	#ENDIF
	
	// Reset the 'reduced performance' global
	g_matcUseReducedProcessing = FALSE
	
	// Clear the 'heist mission full' delayed help message controls
	g_matcHeistFullHelpNeeded = FALSE
	g_matcHeistFullHelpSafetyFrame = 0
	g_sV2CoronaVars.g_isplayer_inside_corona = FALSE
	// Clear 'Press DPADRIGHT' help text if appropriate
	IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MATC_DPADRIGHT"))
		PRINTLN(".KGM [At Coords][DPadRight]: Terminate_MP_Missions_At_Coords() Clear 'Press DPAD-RIGHT' help message")
		CLEAR_HELP()
	ENDIF
	g_matcDpadRightHelpFrameTimeout = 0

ENDPROC






