#IF IS_DEBUG_BUILD

USING "mp_globals_fm.sch"
USING "fmmc_vars.sch"
USING "shared_debug.sch"

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: CONTS / ENUMS ------------------------------------------------------------------
// ##### Description:  --------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

// For colours, tag on print, etc.
ENUM ENTITY_RUNTIME_ERROR_TYPE
	// Generic
	ENTITY_RUNTIME_ERROR_TYPE_GENERIC = 0,
	
	// Notifications
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION, // Keep at top.
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_PED,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_VEH,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_OBJ,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_INTERACTABLE,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_DYNO_PROP,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_PROP,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_ZONE,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_LOCATION,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_DIALOGUE_TRIGGER,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_WARP_PORTAL,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_TRAIN,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_SPAWN_POINT,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_CUTSCENE,
	ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_MAX,
	
	// ---
	
	
	// Warnings
	ENTITY_RUNTIME_ERROR_TYPE_WARNING, // Keep at top.
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_PED,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_VEH,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_OBJ,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_INTERACTABLE,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_DYNO_PROP,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_PROP,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_ZONE,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_LOCATION,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_DIALOGUE_TRIGGER,	
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_WARP_PORTAL,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_TRAIN,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_SPAWN_POINT,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_CUTSCENE,
	ENTITY_RUNTIME_ERROR_TYPE_WARNING_MAX,
	
	// ---
	
	
	// Critical
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, // Keep at top.
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_PED,	
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_VEH,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_OBJ,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_INTERACTABLE,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_DYNO_PROP,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_PROP,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_ZONE,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_LOCATION,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_DIALOGUE_TRIGGER,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_WARP_PORTAL,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_TRAIN,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_SPAWN_POINT,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_CUTSCENE,
	ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_MAX,	
	
	// ---
	
	
	ENTITY_RUNTIME_ERROR_TYPE_MAX
ENDENUM

// To ensure in a cheap way duplicate errors cannot be entered. Script will have to manage the dupe itself if we use _IGNORE
// These do not need to be kept in order feel free to arrange/group them.
ENUM ENTITY_RUNTIME_ERROR_UNIQUE_ID

	// Misc - (Triggers Once)
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_NON_EXISTENT_TEXT_BLOCK_LOAD = 0,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INTERIORS_LOAD_FAILSAFE_HIT,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_PRE_REQ_COUNTERS,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_PLAYER_BD_0,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_PLAYER_BD_1,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_SERVER_BD_0,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_SERVER_BD_1,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_SERVER_BD_2,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_SERVER_BD_3,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_SERVER_BD_4,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_TEAMS_FROM_LOBBY,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_OBJECTIVE_TEXT_OVERRIDE_LIMIT,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INCREMENTAL_RULE_TIMER_TOO_LOW,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_LOOT_THRESHOLD_INCORRECT_SETUP,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_USED_J_SKIP,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_USED_1_MENU_RULE_SKIP,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_USED_S_PASS,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_USED_F_FAIL,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_FAILSAFE_WAITING_FOR_LOBBY_LEADER,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_SET_COP_PERCEPTION_OVERRIDES,
	
	// Gangs - (Triggers Once)
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_GANG_TYPE,
	
	// Cutscene - (Triggers Once)
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_CUTSCENE_NOT_AUTHORISED,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_FALLBACK_CUTSCENE_PLAYER_ID,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_ATTACH_CAM_ENTITY_DOES_NOT_EXIST,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_ATTACH_CAM_VEHICLE_DOES_NOT_EXIST,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_SELECT_CUTSCENE_PLAYERS_FOR_TEAM_EXITTED,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_SUPPRESS_VEHICLE_MODELS,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_SCRIPTED_CUTSCENE_FORCED_TO_CLEANUP_FROM_STUCK,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_MOCAP_CUTSCENE_FORCED_TO_CLEANUP_FROM_STUCK,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_MOCAP_CUTSCENE_TRYING_TO_HIDE_REG_ENTS,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_END_CUTSCENE_TRYING_TO_HIDE_REG_ENTS,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_CUTSCENE_IS_TOO_SHORT_FOR_INTRO_SCENE,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_CUTSCENE_START_FINISH_DATA_IS_DIRTY,
	
	// Spawn Group - (Triggers Once)
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_SPAWN_GROUPS_LOOP,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_GAMEPLAY_MODIFIED,

	// InitPos / Start Position systems - (Triggers Once)
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_FALLBACK_SPAWNING_USED,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_START_PV_DID_NOT_SPAWN,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_PLACED_VEH_DID_NOT_SPAWN,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_SPAWNING_FINISHED_EARLY_FALLBACK,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_NOTE_START_POSITION_TIME_ELAPSED,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_COULD_NOT_FIND_VALID_WATER_POSITION,
	
	// Entities - (Triggers Once)
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_NO_VEHICLES_PLACE_IN_NEAREST,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_WATER_DAMP_ZONES,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_WARP_PORTAL_MISSING_CAM_ANIM,	
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INCORRECT_INVALID_RENDER_TARGET_OPTIONS,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_RENDER_TARGET_ENTITIES,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_BOUNDS_SPAWN_GROUPS_DOES_NOT_EXIST_YET,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_VEHICLES_TO_SPAWN_TRAIN,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_SET_PED_INTO_VEHICLE_INVALID,	
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INTERACTABLE_FAILED_TO_CREATE_SCENE,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_CARD_MINIGAME_BEING_USED_WITH_INVALID_MODEL,	
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INTERACTABLE_HAS_NO_INTERACTION_TYPE_PREP,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INTERACTABLE_HAS_NO_LINKED_SYNC_LOCK_PANEL,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_INTERACTIONS_GOING_ON_BLOCK,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_EVERY_FRAME_PROPS,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_PED_RAPPEL_NO_VEHICLE,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_PROXIMITY_SPAWNING_INCOMPATIBILITY_PED,	
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_NO_MORE_COMPANION_IDS,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_PED_GO_TO_LOCATION_INCORRECT_SET_UP,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_OBJECTS_CREATED_THROUGH_PED_ANIMS,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_PED_GO_TO_TOO_MANY_CUSTOM_SCENARIOS,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_SEAT_OUT_OF_RANGE_FOR_PED,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_WEATHER_OVERRIDE_ON_RULE,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_WARP_PORTAL_NOT_LINKED,
	
	// Alt Vars - (Triggers Once)
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_NO_GANGBOSS,
	
	// Player Count - (Triggers Once)
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_PLAYER_COUNT_USED,
	
	// Custom Vars - (Triggers Once)
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_CUSTOM_VARS_SETUP__NOT_SUPPORTED,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_CUSTOM_VARS_SETUP__REQUIRES_ENTITY,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_CUSTOM_VARS_SETUP__WEAPON_POOL,
	
	// ############################################################
	
	// Used for debugging only. Should never be submitted - (Triggers Once)
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TEMP_1,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TEMP_2,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TEMP_3,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TEMP_4,
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_TEMP_5,
		
	// This can be used if the error is firing in a part of script which does not repeatedly call the error. - (Triggers Forever)
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE,
	
	// Max Number
	ENTITY_RUNTIME_ERROR_UNIQUE_ID_MAX
ENDENUM

CONST_INT ci_MAX_NUMBER_OF_RUNTIME_ERRORS 30
CONST_INT ci_MAX_NUMBER_OF_UNQIUE_RUNTIME_ERRORS_BS 4
CONST_FLOAT cfDEBUG_ERROR_START_LEFT_X	0.035
CONST_FLOAT cfDEBUG_ERROR_START_LEFT_Y	0.3
CONST_INT ci_MAX_NUMBER_OF_MISSION_ALERT_LINES 3

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Vars ---------------------------------------------------------------------------
// ##### Description:  --------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

INT iEntityRunTimeErrorAddedBSLong[ci_MAX_NUMBER_OF_UNQIUE_RUNTIME_ERRORS_BS] // Based on ENTITY_RUNTIME_ERROR_TYPE_MAX
TEXT_LABEL_63 tlEntityRuntimeErrorText[ci_MAX_NUMBER_OF_RUNTIME_ERRORS]

INT iEntityRunTimeErrorDisplayedBSLong[1] 			 // based on ci_MAX_NUMBER_OF_RUNTIME_ERRORS
INT iEntityRunTimeErrorShouldDisplayBSLong[1] 		 // based on ci_MAX_NUMBER_OF_RUNTIME_ERRORS
INT iEntityRunTimeErrorToDisplay = -1
INT iEntityRunTimeErrorToDisplayIterator
FLOAT fVisibilityModifier

SCRIPT_TIMER tdTypeWriter_TextTyping_Errorhelp[ci_MAX_NUMBER_OF_MISSION_ALERT_LINES]
SCRIPT_TIMER tdTypeWriter_TextFullyDrawn_ErrorHelp
INT iTypeWriterCharCounter_ErrorHelp[ci_MAX_NUMBER_OF_MISSION_ALERT_LINES]



// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Shared utility helper functions ------------------------------------------------
// ##### Description: Helper functions that are used in other non-script headers ----------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

FUNC STRING GET_MISSION_STATE_RUNTIME_ERROR_TEXT_FOR_TYPE(ENTITY_RUNTIME_ERROR_TYPE eNewErrorType)
	SWITCH eNewErrorType
		
		// General
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION
			RETURN "[Note] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING
			RETURN "~y~[Warning]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL
			RETURN "~r~[Critical]~s~ - "
		BREAK
		
		// Ped Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_PED
			RETURN "[Ped #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_PED
			RETURN "~y~[Ped #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_PED
			RETURN "~r~[Ped #]~s~ - "
		BREAK
		
		// Vehicle Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_VEH
			RETURN "[Veh #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_VEH
			RETURN "~y~[Veh #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_VEH
			RETURN "~r~[Veh #]~s~ - "
		BREAK
				
		// Object Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_OBJ
			RETURN "[Obj #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_OBJ
			RETURN "~y~[Obj #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_OBJ
			RETURN "~r~[Obj #]~s~ - "
		BREAK
		
		// Interactable Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_INTERACTABLE
			RETURN "[Interact #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_INTERACTABLE
			RETURN "~y~[Interact #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_INTERACTABLE
			RETURN "~r~[Interact #]~s~ - "
		BREAK
		
		// Dynoprop Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_DYNO_PROP
			RETURN "[Dynoprop #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_DYNO_PROP
			RETURN "~y~[Dynoprop #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_DYNO_PROP
			RETURN "~r~[Dynoprop #]~s~ - "
		BREAK
		
		// Prop Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_PROP
			RETURN "[Prop #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_PROP
			RETURN "~y~[Prop #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_PROP
			RETURN "~r~[Prop #]~s~ - "
		BREAK
		
		// Zone Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_ZONE
			RETURN "[Zone #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_ZONE
			RETURN "~y~[Zone #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_ZONE
			RETURN "~r~[Zone #]~s~ - "
		BREAK
		
		// Location Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_LOCATION
			RETURN "[Location #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_LOCATION
			RETURN "~y~[Location #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_LOCATION
			RETURN "~r~[Location #]~s~ - "
		BREAK
		
		// Dialogue Trigger Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_DIALOGUE_TRIGGER
			RETURN "[DTrigger #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_DIALOGUE_TRIGGER
			RETURN "~y~[DTrigger #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_DIALOGUE_TRIGGER
			RETURN "~r~[DTrigger #]~s~ - "
		BREAK
		
		// Warp Portal Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_WARP_PORTAL
			RETURN "[Warp Port #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_WARP_PORTAL
			RETURN "~y~[Warp Port #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_WARP_PORTAL
			RETURN "~r~[Warp Port #]~s~ - "
		BREAK
		
		// Train Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_TRAIN
			RETURN "[Train #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_TRAIN
			RETURN "~y~[Train #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_TRAIN
			RETURN "~r~[Train #]~s~ - "
		BREAK
		
		// Spawn Point Entity
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_SPAWN_POINT
			RETURN "[SpawnP #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_SPAWN_POINT
			RETURN "~y~[SpawnP #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_SPAWN_POINT
			RETURN "~r~[SpawnP #]~s~ - "
		BREAK
		
		// Cutscene
		CASE ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_CUTSCENE
			RETURN "[Cutscene #] - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_WARNING_CUTSCENE
			RETURN "~y~[Cutscene #]~s~ - "
		BREAK
		CASE ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_CUTSCENE
			RETURN "~r~[Cutscene #]~s~ - "
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL IS_RUNTIME_ERROR_A_CRITICAL(ENTITY_RUNTIME_ERROR_TYPE eNewErrorType)
	RETURN eNewErrorType > ENTITY_RUNTIME_ERROR_TYPE_CRITICAL AND eNewErrorType < ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_MAX
ENDFUNC

FUNC BOOL IS_RUNTIME_ERROR_A_WARNING(ENTITY_RUNTIME_ERROR_TYPE eNewErrorType)
	RETURN eNewErrorType > ENTITY_RUNTIME_ERROR_TYPE_WARNING AND eNewErrorType < ENTITY_RUNTIME_ERROR_TYPE_WARNING_MAX
ENDFUNC

FUNC BOOL IS_RUNTIME_ERROR_A_NOTIFICATION(ENTITY_RUNTIME_ERROR_TYPE eNewErrorType)
	RETURN eNewErrorType > ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION AND eNewErrorType < ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION_MAX
ENDFUNC

FUNC INT GET_FREE_RUN_TIME_ERROR_INDEX()
	
	INT i, iRunTimeError
	FOR i = 0 TO (ci_MAX_NUMBER_OF_RUNTIME_ERRORS/ci_MAX_NUMBER_OF_MISSION_ALERT_LINES)-1
		
		iRunTimeError = (ci_MAX_NUMBER_OF_MISSION_ALERT_LINES*i)
		
		IF iRunTimeError >= ci_MAX_NUMBER_OF_RUNTIME_ERRORS			
			BREAKLOOP
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tlEntityRuntimeErrorText[iRunTimeError])
			PRINTLN("[Alerts ", iRunTimeError, "] - GET_FREE_RUN_TIME_ERROR_INDEX - iterator string is empty.")
			RELOOP
		ENDIF
		
		IF FMMC_IS_LONG_BIT_SET(iEntityRunTimeErrorDisplayedBSLong, iRunTimeError)
			PRINTLN("[Alerts ", iRunTimeError, "] - GET_FREE_RUN_TIME_ERROR_INDEX - iterator has already displayed.")
			RELOOP
		ENDIF
		
		RETURN iRunTimeError
		
	ENDFOR
	
	RETURN -1
ENDFUNC

// ##### ----------------------------------------------------------------------------------------------
// ##### Section Name: Main ---------------------------------------------------------------------------
// ##### Description: Main Functions to add or display errors -----------------------------------------
// ##### ----------------------------------------------------------------------------------------------
// ##### ----------------------------------------------------------------------------------------------

PROC PRINT_ALL_CURRENT_MISSION_STATE_RUNTIME_ERROR()
	INT i = 0	
	FOR i = 0 TO ci_MAX_NUMBER_OF_RUNTIME_ERRORS-1		
		PRINTLN("[Alerts ", i, "] - MISSION_STATE_RUNTIME_ERROR: ", tlEntityRuntimeErrorText[i])
		
		IF i % ci_MAX_NUMBER_OF_RUNTIME_ERRORS = 0
			PRINTLN("-----------")	
		ENDIF
	ENDFOR	
ENDPROC

FUNC BOOL DOES_MISSION_STATE_RUNTIME_ERROR_EXIST(ENTITY_RUNTIME_ERROR_UNIQUE_ID eNewErrorTypeID)	
	IF eNewErrorTypeID = ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE
		RETURN FALSE
	ENDIF
	RETURN FMMC_IS_LONG_BIT_SET(iEntityRunTimeErrorAddedBSLong, ENUM_TO_INT(eNewErrorTypeID))	
ENDFUNC

FUNC INT GET_NEXT_WORD_LENGTH(INT iStart, TEXT_LABEL_63 tl63)

	STRING sChar
	INT iLength, iEndPoint, iStartPoint
	INT iChar = 0
	
	INT iStringLength = GET_LENGTH_OF_LITERAL_STRING(tl63)	
	
	FOR iChar = 0 TO 63
		
		iStartPoint = iStart+iChar
		iEndPoint = iStart+iChar+1
		
		IF iEndPoint > iStringLength
			BREAKLOOP
		ENDIF
		
		sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63, iStartPoint, iEndPoint)
		
		IF IS_STRING_NULL_OR_EMPTY(sChar)
		OR ARE_STRINGS_EQUAL(sChar, "NULL")
		OR ARE_STRINGS_EQUAL(sChar, " ")
			BREAKLOOP
		ENDIF
		
		iLength++
	ENDFOR
	
	RETURN iLength
ENDFUNC

PROC ADD_MISSION_STATE_RUNTIME_ERROR_CHARACTER(INT &ErrorIndex, STRING sNewErrorText, INT &iIntCounter, INT &iFloatCounter, INT iIntToAdd1 = 0, INT iIntToAdd2 = 0, INT iIntToAdd3 = 0, FLOAT fFloatToAdd1 = 0.0, FLOAT fFloatToAdd2 = 0.0, FLOAT fFloatToAdd3 = 0.0)

	INT iChar
	STRING sChar
	INT iNextWordLength
	INT iStringLength = GET_LENGTH_OF_LITERAL_STRING(sNewErrorText)	
	
	FOR iChar = 0 TO (63*ci_MAX_NUMBER_OF_MISSION_ALERT_LINES)
		
		IF iChar+1 > iStringLength
			BREAKLOOP
		ENDIF
		
		sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(sNewErrorText, iChar, iChar+1)
		
		IF IS_STRING_NULL_OR_EMPTY(sChar)
		OR ARE_STRINGS_EQUAL(sChar, "NULL")
			BREAKLOOP
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sChar, " ")
			iNextWordLength = GET_NEXT_WORD_LENGTH(iChar+1, tlEntityRuntimeErrorText[ErrorIndex])
			iNextWordLength += 1 // for current char (space)			
		ELIF ARE_STRINGS_EQUAL(sChar, "#")
		OR ARE_STRINGS_EQUAL(sChar, "@")
			iNextWordLength = 8
		ELSE
			iNextWordLength = 0
		ENDIF
		
		// Note: Calling GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME again in GET_NEXT_WORD_LENGTH is corrupting the results of the previous call. So we need to add another assignment here.
		sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(sNewErrorText, iChar, iChar+1)		
				
		IF GET_LENGTH_OF_LITERAL_STRING(tlEntityRuntimeErrorText[ErrorIndex]) + iNextWordLength >= 62
			IF GET_LENGTH_OF_LITERAL_STRING(tlEntityRuntimeErrorText[ErrorIndex]) = 62
				tlEntityRuntimeErrorText[ErrorIndex] += "-"
			ENDIF				
			ErrorIndex++
		ENDIF
		
		IF ARE_STRINGS_EQUAL(sChar, "#")
			IF iIntCounter = 0
				tlEntityRuntimeErrorText[ErrorIndex] += iIntToAdd1
			ELIF iIntCounter = 1
				tlEntityRuntimeErrorText[ErrorIndex] += iIntToAdd2
			ELIF iIntCounter = 2
				tlEntityRuntimeErrorText[ErrorIndex] += iIntToAdd3				
			ENDIF
			iIntCounter++
			
		ELIF ARE_STRINGS_EQUAL(sChar, "@")
			IF iFloatCounter = 0
				tlEntityRuntimeErrorText[ErrorIndex] += FLOAT_TO_STRING(fFloatToAdd1, 2)
			ELIF iFloatCounter = 1
				tlEntityRuntimeErrorText[ErrorIndex] += FLOAT_TO_STRING(fFloatToAdd2, 2)				
			ELIF iFloatCounter = 3
				tlEntityRuntimeErrorText[ErrorIndex] += FLOAT_TO_STRING(fFloatToAdd3, 2)
			ENDIF
			iFloatCounter++
			
		ELSE
			//PRINTLN("iChar : ", iChar, " Adding sChar: ", sChar, " tlEntityRuntimeErrorText: ", tlEntityRuntimeErrorText[ErrorIndex])
			tlEntityRuntimeErrorText[ErrorIndex] += sChar
			
		ENDIF
	ENDFOR
	
ENDPROC

// for sNewErrorText, ints/floats can be injected into the print:
// # Int - iIntToAdd1 is added to the first instance of # in the string etc.
// @ Float - fFloatToAdd1 is added to the first instance of @ in the string etc.
PROC ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID eNewErrorTypeID, ENTITY_RUNTIME_ERROR_TYPE eNewErrorType, STRING sNewErrorText, INT iIntToAdd1 = 0, INT iIntToAdd2 = 0, INT iIntToAdd3 = 0, FLOAT fFloatToAdd1 = 0.0, FLOAT fFloatToAdd2 = 0.0, FLOAT fFloatToAdd3 = 0.0)
		
	IF ENUM_TO_INT(eNewErrorTypeID) > ci_MAX_NUMBER_OF_UNQIUE_RUNTIME_ERRORS_BS*32
		SCRIPT_ASSERT("ADD_MISSION_STATE_RUNTIME_ERROR unique ID out of BS range. Up this const: ci_MAX_NUMBER_OF_UNQIUE_RUNTIME_ERRORS_BS")
		EXIT
	ENDIF
	
	IF DOES_MISSION_STATE_RUNTIME_ERROR_EXIST(eNewErrorTypeID)
		EXIT
	ENDIF
	
	INT iErrorIndex = GET_FREE_RUN_TIME_ERROR_INDEX()
	IF iErrorIndex >= ci_MAX_NUMBER_OF_RUNTIME_ERRORS
	OR iErrorIndex = -1
		iErrorIndex = ci_MAX_NUMBER_OF_RUNTIME_ERRORS-ci_MAX_NUMBER_OF_MISSION_ALERT_LINES-1
		EXIT
	ENDIF
	
	PRINTLN("[Alerts ", iErrorIndex-2, "] - ADD_MISSION_STATE_RUNTIME_ERROR - iErrorIndex: ", iErrorIndex)
	
	PRINTLN("[Alerts ", iErrorIndex-2, "] - ADD_MISSION_STATE_RUNTIME_ERROR - sNewErrorText: ", sNewErrorText)
	
	INT iIntCounter
	INT iFloatCounter

	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_BlockOnScreenNotificationPopups")
	OR NOT IS_RUNTIME_ERROR_A_NOTIFICATION(eNewErrorType)
		FMMC_SET_LONG_BIT(iEntityRunTimeErrorShouldDisplayBSLong, iErrorIndex)
	ENDIF
	
	ADD_MISSION_STATE_RUNTIME_ERROR_CHARACTER(iErrorIndex, GET_MISSION_STATE_RUNTIME_ERROR_TEXT_FOR_TYPE(eNewErrorType), iIntCounter, iFloatCounter, iIntToAdd1, iIntToAdd2, iIntToAdd3, fFloatToAdd1, fFloatToAdd2, fFloatToAdd3)
	
	ADD_MISSION_STATE_RUNTIME_ERROR_CHARACTER(iErrorIndex, sNewErrorText, iIntCounter, iFloatCounter, iIntToAdd1, iIntToAdd2, iIntToAdd3, fFloatToAdd1, fFloatToAdd2, fFloatToAdd3)
		
	FMMC_SET_LONG_BIT(iEntityRunTimeErrorAddedBSLong, ENUM_TO_INT(eNewErrorTypeID))	
	
	IF iErrorIndex-2 > -1
		PRINTLN("[Alerts ", iErrorIndex-2, "] - ADD_MISSION_STATE_RUNTIME_ERROR - ", tlEntityRuntimeErrorText[iErrorIndex-2])
	ENDIF
	IF iErrorIndex-1 > -1
		PRINTLN("[Alerts ", iErrorIndex-1, "] - ADD_MISSION_STATE_RUNTIME_ERROR - ", tlEntityRuntimeErrorText[iErrorIndex-1])
	ENDIF
	IF iErrorIndex > -1
		PRINTLN("[Alerts ", iErrorIndex, "] - ADD_MISSION_STATE_RUNTIME_ERROR - ", tlEntityRuntimeErrorText[iErrorIndex])
	ENDIF
	
	IF IS_RUNTIME_ERROR_A_CRITICAL(eNewErrorType)
		ASSERTLN(sNewErrorText)
	ENDIF
ENDPROC

PROC CLEANUP_RUNTIME_ERROR_ALERT_ON_SCREEN()
	INT i
	FOR i = 0 TO ci_MAX_NUMBER_OF_MISSION_ALERT_LINES-1
		RESET_NET_TIMER(tdTypeWriter_TextTyping_Errorhelp[i])
		iTypeWriterCharCounter_ErrorHelp[i] = 0
	ENDFOR		
	RESET_NET_TIMER(tdTypeWriter_TextFullyDrawn_ErrorHelp)
	
	iEntityRunTimeErrorToDisplayIterator = 0
	iEntityRunTimeErrorToDisplay = -1
	fVisibilityModifier = 0.00
ENDPROC

PROC PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN(BOOL bScriptReady = TRUE)

	IF NOT bScriptReady
	OR (NOT GET_COMMANDLINE_PARAM_EXISTS("sc_UseOnScreenWarningPopups") AND NOT g_Private_IsMultiplayerCreatorRunning)
		EXIT
	ENDIF
	
	IF iEntityRunTimeErrorToDisplay = -1
		iEntityRunTimeErrorToDisplayIterator += ci_MAX_NUMBER_OF_MISSION_ALERT_LINES
		
		IF iEntityRunTimeErrorToDisplayIterator >= ci_MAX_NUMBER_OF_RUNTIME_ERRORS
			iEntityRunTimeErrorToDisplayIterator = 0
		ENDIF
		
		IF NOT FMMC_IS_LONG_BIT_SET(iEntityRunTimeErrorShouldDisplayBSLong, iEntityRunTimeErrorToDisplayIterator)
			PRINTLN("[Alerts ", iEntityRunTimeErrorToDisplayIterator, "] - PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN - iterator should not display.")
			EXIT		
		ENDIF
		
		IF IS_STRING_NULL_OR_EMPTY(tlEntityRuntimeErrorText[iEntityRunTimeErrorToDisplayIterator])
			PRINTLN("[Alerts ", iEntityRunTimeErrorToDisplayIterator, "] - PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN - iterator string is empty.")
			EXIT
		ENDIF
		
		IF FMMC_IS_LONG_BIT_SET(iEntityRunTimeErrorDisplayedBSLong, iEntityRunTimeErrorToDisplayIterator)
			PRINTLN("[Alerts ", iEntityRunTimeErrorToDisplayIterator, "] - PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN - iterator has already displayed.")
			EXIT
		ENDIF
		
		iEntityRunTimeErrorToDisplay = iEntityRunTimeErrorToDisplayIterator
	ENDIF
	
	IF iEntityRunTimeErrorToDisplay = -1
		EXIT
	ENDIF

	PRINTLN("[Alerts ", iEntityRunTimeErrorToDisplay, "] - PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN - Processing.")
	
	BOOL bDone1, bDone2, bDone3, bFinished
	IF NOT FMMC_IS_LONG_BIT_SET(iEntityRunTimeErrorDisplayedBSLong, iEntityRunTimeErrorToDisplay)
		fVisibilityModifier += GET_FRAME_TIME() * (2.0-(fVisibilityModifier))
		IF fVisibilityModifier >= 1.0
			fVisibilityModifier = 1.0
		ENDIF
	ELSE
		fVisibilityModifier -= GET_FRAME_TIME() * (1.0+(fVisibilityModifier))
		IF fVisibilityModifier <= 0.0
			fVisibilityModifier = 0.0
			bFinished = TRUE
		ENDIF			
	ENDIF	
	
	INT iPrint
	TEXT_LABEL_63 tl63 = ""	
	FLOAT fSpacingY = 0.02
	VECTOR vPos = <<cfDEBUG_ERROR_START_LEFT_X, cfDEBUG_ERROR_START_LEFT_Y, 0.0>>
	VECTOR vNewPos	
	FLOAT fRectX = 0.28*fVisibilityModifier		
	
	vNewPos = <<vPos.x*fVisibilityModifier, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
	tl63 = tlEntityRuntimeErrorText[iEntityRunTimeErrorToDisplay]
	SET_TEXT_SCALE(0.25, 0.25)
	SET_TEXT_COLOUR(255, 255, 255, ROUND(255.0*fVisibilityModifier))
	SET_TEXT_DROPSHADOW(4, 0, 0, 0, ROUND(175.0*fVisibilityModifier))
	iPrint++
	IF PROCESS_TYPE_WRITER_TEXT_ANIMATION(tdTypeWriter_TextTyping_ErrorHelp[0], tdTypeWriter_TextFullyDrawn_ErrorHelp, 4000, 15, vNewPos, iTypeWriterCharCounter_ErrorHelp[0], tl63, TRUE, FALSE, TRUE, IS_STRING_NULL_OR_EMPTY(tlEntityRuntimeErrorText[iEntityRunTimeErrorToDisplay+1]))
	
		PRINTLN("[Alerts ", iEntityRunTimeErrorToDisplay, "] - PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN - PROCESS_TYPE_WRITER_TEXT_ANIMATION (1) is done.")
	
		bDone1 = TRUE
		
		vNewPos = <<vPos.x*fVisibilityModifier, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
		tl63 = tlEntityRuntimeErrorText[iEntityRunTimeErrorToDisplay+1]
		SET_TEXT_SCALE(0.25, 0.25)
		SET_TEXT_COLOUR(255, 255, 255, ROUND(255.0*fVisibilityModifier))
		SET_TEXT_DROPSHADOW(4, 0, 0, 0, ROUND(175.0*fVisibilityModifier))
		iPrint++

		IF PROCESS_TYPE_WRITER_TEXT_ANIMATION(tdTypeWriter_TextTyping_ErrorHelp[1], tdTypeWriter_TextFullyDrawn_ErrorHelp, 4000, 15, vNewPos, iTypeWriterCharCounter_ErrorHelp[1], tl63, TRUE, FALSE, TRUE, IS_STRING_NULL_OR_EMPTY(tlEntityRuntimeErrorText[iEntityRunTimeErrorToDisplay+2]))
		
			PRINTLN("[Alerts ", iEntityRunTimeErrorToDisplay, "] - PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN - PROCESS_TYPE_WRITER_TEXT_ANIMATION (2) is done.")
	
			bDone2 = TRUE
			
			vNewPos = <<vPos.x*fVisibilityModifier, vPos.y+(fSpacingY*iPrint), vPos.y+(fSpacingY*iPrint)>>
			tl63 = tlEntityRuntimeErrorText[iEntityRunTimeErrorToDisplay+2]
			SET_TEXT_SCALE(0.25, 0.25)
			SET_TEXT_COLOUR(255, 255, 255, ROUND(255.0*fVisibilityModifier))
			SET_TEXT_DROPSHADOW(4, 0, 0, 0, ROUND(175.0*fVisibilityModifier))
			IF PROCESS_TYPE_WRITER_TEXT_ANIMATION(tdTypeWriter_TextTyping_ErrorHelp[2], tdTypeWriter_TextFullyDrawn_ErrorHelp, 4000, 15, vNewPos, iTypeWriterCharCounter_ErrorHelp[2], tl63, TRUE, FALSE, TRUE, TRUE)	
				
				PRINTLN("[Alerts ", iEntityRunTimeErrorToDisplay, "] - PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN - PROCESS_TYPE_WRITER_TEXT_ANIMATION (3) is done.")
	
				bDone3 = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	vNewPos = <<vPos.x*fVisibilityModifier, vPos.y+0.03, vPos.y+0.03>>
	DRAW_RECT(vNewPos.x+(fRectX/2), vNewPos.y, fRectX, 0.06, 0, 0, 0, ROUND(140.0*fVisibilityModifier))
	
	vNewPos = <<vPos.x*fVisibilityModifier, vPos.y+0.03, vPos.y+0.03>>
	DRAW_RECT(vNewPos.x-0.005, vNewPos.y, 0.01, 0.06, 200, 0, 0, ROUND(160.0*fVisibilityModifier))
	
	vNewPos = <<vPos.x*fVisibilityModifier, vPos.y+0.03, vPos.y+0.03>>
	DRAW_RECT(vNewPos.x+(fRectX/2)-0.005, vNewPos.y+0.0325, fRectX+0.01, 0.005, 200, 200, 200, ROUND(75.0*fVisibilityModifier))
	
	vNewPos = <<vPos.x*fVisibilityModifier, vPos.y+0.03, vPos.y+0.03>>
	DRAW_RECT(vNewPos.x+(fRectX/2)-0.005, vNewPos.y-0.0325, fRectX+0.01, 0.005, 200, 200, 200, ROUND(75.0*fVisibilityModifier))
	
	IF (bDone1 OR IS_STRING_NULL_OR_EMPTY(tlEntityRuntimeErrorText[iEntityRunTimeErrorToDisplay]))
	AND (bDone2 OR IS_STRING_NULL_OR_EMPTY(tlEntityRuntimeErrorText[iEntityRunTimeErrorToDisplay+1]))
	AND (bDone3 OR IS_STRING_NULL_OR_EMPTY(tlEntityRuntimeErrorText[iEntityRunTimeErrorToDisplay+2]))
		FMMC_SET_LONG_BIT(iEntityRunTimeErrorDisplayedBSLong, iEntityRunTimeErrorToDisplay)
		PRINTLN("[Alerts ", iEntityRunTimeErrorToDisplay, "] - PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN - iEntityRunTimeErrorDisplayedBSLong Setting has displayed")
	ENDIF
	
	IF bFinished
		PRINTLN("[Alerts ", iEntityRunTimeErrorToDisplay, "] - PROCESS_RUNTIME_ERROR_ALERT_ON_SCREEN - Fisished and resetting")
		CLEANUP_RUNTIME_ERROR_ALERT_ON_SCREEN()
	ENDIF
ENDPROC

#ENDIF
