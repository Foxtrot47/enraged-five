//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_CREATION.sch																					//
// Description: Header file containing functionality for peds/ped props creation and cleanup. 							//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_patrol.sch"
USING "net_peds_clothing.sch"
USING "net_peds_vfx.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ SET PARENT PED DATA ╞══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets the local ped parent data.
///    Populates the local parent array. Array size: MAX_NUM_LOCAL_PARENT_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_PED_PARENT_LOCAL_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iArrayID
	INT iChildPed
	REPEAT MAX_NUM_LOCAL_PARENT_PEDS iArrayID
		RESET_PED_PARENT_DATA(LocalData.ParentPedData[iArrayID])
		
		REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPed
			LocalData.ParentPedData[iArrayID].iChildID[iChildPed] = -1
		ENDREPEAT
		
		SET_PED_PARENT_DATA(ePedLocation, LocalData.ParentPedData[iArrayID], ServerBD.iLayout, iArrayID, LocalData.iParentPedID)
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the network ped parent data.
///    Populates the network parent array. Array size: MAX_NUM_NETWORK_PARENT_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_PED_PARENT_NETWORK_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	
	INT iArrayID
	INT iChildPed
	REPEAT MAX_NUM_NETWORK_PARENT_PEDS iArrayID
		RESET_PED_PARENT_DATA(ServerBD.ParentPedData[iArrayID])
		
		REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPed
			ServerBD.ParentPedData[iArrayID].iChildID[iChildPed] = -1
		ENDREPEAT
		
		SET_PED_PARENT_DATA(ePedLocation, ServerBD.ParentPedData[iArrayID], ServerBD.iLayout, iArrayID, ServerBD.iParentPedID, TRUE)
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Initialises the ped parent local array.
///    Sets all elements to -1.
/// PARAMS:
///    LocalData - Local ped data to set.
PROC INITIALISE_PED_PARENT_LOCAL_ARRAY_ID(SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		LocalData.iParentPedID[iPed] = -1
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Initialises the ped parent network array.
///    Sets all elements to -1.
/// PARAMS:
///    ServerBD - Server ped data to set.
PROC INITIALISE_PED_PARENT_NETWORK_ARRAY_ID(SERVER_PED_DATA &ServerBD)
	
	INT iPed
	REPEAT MAX_NUM_TOTAL_NETWORK_PEDS iPed
		ServerBD.iParentPedID[iPed] = -1
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the parent data for all peds.
///    Populates the parent arrays. 
///    Local array max: MAX_NUM_LOCAL_PARENT_PEDS
///    Network array size: MAX_NUM_NETWORK_PARENT_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_ALL_PED_PARENT_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	INITIALISE_PED_PARENT_LOCAL_ARRAY_ID(LocalData)
	SET_PED_PARENT_LOCAL_DATA(ePedLocation, ServerBD, LocalData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INITIALISE_PED_PARENT_NETWORK_ARRAY_ID(ServerBD)
		SET_PED_PARENT_NETWORK_DATA(ePedLocation, ServerBD)
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ ASSET MANAGEMENT ╞═══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Requests all the anims and props for a ped.
///    Use HAVE_PED_ASSETS_LOADED() to check if the assets have loaded.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    eMusicIntensity - For dancing peds. Requests the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC REQUEST_PED_ASSETS(PED_LOCATIONS ePedLocation, PEDS_DATA Data, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	// Ped Dict
	PED_ANIM_DATA pedAnimData
	GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
		REQUEST_ANIM_DICT(pedAnimData.sAnimDict)
	ENDIF
	
	// Prop Dict
	PED_PROP_ANIM_DATA pedPropAnimData
	GET_PED_PROP_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedPropAnimData, DEFAULT, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HEELED_PED))
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimDict)
		REQUEST_ANIM_DICT(pedPropAnimData.sPropAnimDict)
	ENDIF
	
	// Ped Model
	MODEL_NAMES pedModel = GET_PEDS_MODEL(INT_TO_ENUM(PED_MODELS, Data.iPedModel))
	IF (IS_MODEL_VALID(pedModel) AND pedModel != DUMMY_MODEL_FOR_SCRIPT)
		REQUEST_MODEL(pedModel)
	ENDIF
	
	// Prop Models
	INT iProp
	PED_PROP_DATA pedPropData
	
	REPEAT MAX_NUM_PED_PROPS iProp
		GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), iProp, pedPropData)
		IF (IS_MODEL_VALID(pedPropData.ePropModel) AND pedPropData.ePropModel != DUMMY_MODEL_FOR_SCRIPT)
			REQUEST_MODEL(pedPropData.ePropModel)
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Checks if all the assets for a ped have loaded.
///    Use REQUEST_PED_ASSETS() to request the assets.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    eMusicIntensity - For dancing peds. Requests the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
/// RETURNS: TRUE is all assets have loaded.
FUNC BOOL HAVE_PED_ASSETS_LOADED(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	BOOL bPedModelLoaded 		= FALSE
	BOOL bPropModelsLoaded 		= FALSE
	BOOL bPedAnimDictLoaded 	= FALSE
	BOOL bPropAnimDictLoaded 	= FALSE
	
	// Ped Dict
	PED_ANIM_DATA pedAnimData
	GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
		IF HAS_ANIM_DICT_LOADED(pedAnimData.sAnimDict)
			bPedAnimDictLoaded = TRUE
		ELSE
			PRINTLN("[AM_MP_PEDS] HAVE_PED_ASSETS_LOADED - Bail: Waiting on the ped anim dictionary ", pedAnimData.sAnimDict, " to finished loading")
		ENDIF
	ELSE
		bPedAnimDictLoaded = TRUE
	ENDIF
	
	// Prop Dict
	PED_PROP_ANIM_DATA pedPropAnimData
	GET_PED_PROP_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedPropAnimData, DEFAULT, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HEELED_PED))
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimDict)
		IF HAS_ANIM_DICT_LOADED(pedPropAnimData.sPropAnimDict)
			bPropAnimDictLoaded = TRUE
		ELSE
			PRINTLN("[AM_MP_PEDS] HAVE_PED_ASSETS_LOADED - Bail: Waiting on the prop anim dictionary ", pedPropAnimData.sPropAnimDict, " to finished loading")
		ENDIF
	ELSE
		bPropAnimDictLoaded = TRUE
	ENDIF
	
	// Ped Model
	MODEL_NAMES pedModel = GET_PEDS_MODEL(INT_TO_ENUM(PED_MODELS, Data.iPedModel))
	IF (IS_MODEL_VALID(pedModel) AND pedModel != DUMMY_MODEL_FOR_SCRIPT)
		IF HAS_MODEL_LOADED(pedModel)
			bPedModelLoaded = TRUE
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("[AM_MP_PEDS] HAVE_PED_ASSETS_LOADED - Bail: Waiting on the ped model ", GET_MODEL_NAME_FOR_DEBUG(pedModel), " to finished loading")
			#ENDIF
		ENDIF
	ELSE
		bPedModelLoaded = TRUE
	ENDIF
	
	// Prop Models
	INT iProp
	INT iPropsLoaded = 0
	PED_PROP_DATA pedPropData
	
	REPEAT MAX_NUM_PED_PROPS iProp
		GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), iProp, pedPropData)
		IF (IS_MODEL_VALID(pedPropData.ePropModel) AND pedPropData.ePropModel != DUMMY_MODEL_FOR_SCRIPT)
			IF HAS_MODEL_LOADED(pedPropData.ePropModel)
				iPropsLoaded++
			ENDIF
		ELSE
			iPropsLoaded++
		ENDIF
	ENDREPEAT
	
	IF (iPropsLoaded = MAX_NUM_PED_PROPS)
		bPropModelsLoaded = TRUE
	ELSE
		PRINTLN("[AM_MP_PEDS] HAVE_PED_ASSETS_LOADED - Bail: Waiting on prop model ", iPropsLoaded, " to finished loading")
	ENDIF
	
	RETURN (bPedModelLoaded AND bPropModelsLoaded AND bPedAnimDictLoaded AND bPropAnimDictLoaded)
ENDFUNC

/// PURPOSE:
///    Loops through all ped props and checks if they have been created.
/// PARAMS:
///    Data - Ped data to query.
/// RETURNS: TRUE when all ped props have been created.
FUNC BOOL HAS_PED_PROPS_BEEN_CREATED(PED_LOCATIONS ePedLocation, PEDS_DATA Data)
	
	INT iProp
	INT iPropsCreated = 0
	PED_PROP_DATA pedPropData
	
	REPEAT MAX_NUM_PED_PROPS iProp
		GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), iProp, pedPropData)
		IF (IS_MODEL_VALID(pedPropData.ePropModel) AND pedPropData.ePropModel != DUMMY_MODEL_FOR_SCRIPT)
			IF DOES_ENTITY_EXIST(Data.PropID[iProp])
				iPropsCreated++
			ENDIF
		ELSE
			iPropsCreated++
		ENDIF
	ENDREPEAT
	
	RETURN (iPropsCreated = MAX_NUM_PED_PROPS)
ENDFUNC

/// PURPOSE:
///    Unloads the peds anim dicts. Includes any ped prop anim dicts.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    eMusicIntensity - For dancing peds. Unloads the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC UNLOAD_PED_ANIM_DICTS(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	// Ped Dict
	PED_ANIM_DATA pedAnimData
	GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
		REMOVE_ANIM_DICT(pedAnimData.sAnimDict)
	ENDIF
	
	// Prop Dict
	PED_PROP_ANIM_DATA pedPropAnimData
	GET_PED_PROP_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedPropAnimData, DEFAULT, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HEELED_PED))
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimDict)
		REMOVE_ANIM_DICT(pedPropAnimData.sPropAnimDict)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Unloads the peds models. Includes any ped prop models.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ePedModel - Ped model to unload.
///    eActivity - Ped activity assets to unload.
PROC UNLOAD_PED_MODELS(PED_LOCATIONS ePedLocation, PED_MODELS ePedModel, PED_ACTIVITIES eActivity)
	
	// Ped Model
	MODEL_NAMES eModel = GET_PEDS_MODEL(ePedModel)
	IF (IS_MODEL_VALID(eModel) AND eModel != DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
	ENDIF
	
	// Prop Models
	INT iProp
	PED_PROP_DATA pedPropData
	
	REPEAT MAX_NUM_PED_PROPS iProp
		GET_PED_PROP_DATA(ePedLocation, eActivity, iProp, pedPropData)
		IF (IS_MODEL_VALID(pedPropData.ePropModel) AND pedPropData.ePropModel != DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(pedPropData.ePropModel)
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Unloads all the peds assets. 
///    This includes the peds anim dicts, any prop dicts, the peds models, and any prop models.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server data to query.
///    LocalData - All local ped data to query.
PROC UNLOAD_ALL_PED_ASSETS(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	INT iProp
	INT iActivity
	INT iIntensity
	PED_ANIM_DATA pedAnimData
	PED_PROP_ANIM_DATA pedPropAnimData
	
	REPEAT PED_ACT_TOTAL iActivity
		
		// Ped Dicts
		REPEAT ENUM_TO_INT(CLUB_MUSIC_INTENSITY_TOTAL)-1 iIntensity
			GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iPed].iActivity), pedAnimData, DEFAULT, INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity), INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity))
			IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
				REMOVE_ANIM_DICT(pedAnimData.sAnimDict)
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		// Dancing Transition Dicts
		REPEAT ENUM_TO_INT(CLUB_MUSIC_INTENSITY_TOTAL)-1 iIntensity
			GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iPed].iActivity), pedAnimData, DEFAULT, INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity), INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity), TRUE)
			IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
				REMOVE_ANIM_DICT(pedAnimData.sAnimDict)
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		// Transition State Dicts
		REPEAT ENUM_TO_INT(PED_TRANS_STATE_TOTAL)-1 iIntensity
			GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iPed].iActivity), pedAnimData, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, INT_TO_ENUM(PED_TRANSITION_STATES, iIntensity))
			IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
				REMOVE_ANIM_DICT(pedAnimData.sAnimDict)
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		// Transition State Trans Dicts
		REPEAT ENUM_TO_INT(PED_TRANS_STATE_TOTAL)-1 iIntensity
			GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iPed].iActivity), pedAnimData, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, INT_TO_ENUM(PED_TRANSITION_STATES, iIntensity), TRUE)
			IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
				REMOVE_ANIM_DICT(pedAnimData.sAnimDict)
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		// Prop Dicts
		REPEAT MAX_NUM_PED_PROPS iProp
			GET_PED_PROP_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iPed].iActivity), pedPropAnimData, iProp)
			IF NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimDict)
				REMOVE_ANIM_DICT(pedPropAnimData.sPropAnimDict)
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
	ENDREPEAT
	
	// Ped Models
	IF (ServerBD.iMaxLocalPeds > 0)
		REPEAT ServerBD.iMaxLocalPeds iPed
			UNLOAD_PED_MODELS(ePedLocation, INT_TO_ENUM(PED_MODELS, LocalData.PedData[iPed].iPedModel), INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iPed].iActivity))
		ENDREPEAT
	ENDIF
	
	// VFX Effects
	IF (ServerBD.iMaxLocalPeds > 0)
		REPEAT ServerBD.iMaxLocalPeds iPed
			IF (LocalData.iVFXPedID[iPed] != -1)
				CLEANUP_PED_VFX(LocalData.VFXData[LocalData.iVFXPedID[iPed]].LoopingPTFX)
			ENDIF
		ENDREPEAT
	ENDIF
	
	// Patrol Anim Dicts
	UNLOAD_PED_PATROL_ANIM_DICTS(ePedLocation)
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PRINTLN("[AM_MP_PEDS] UNLOAD_ALL_PED_ASSETS - All local peds assets have been unloaded")
		EXIT
	ENDIF
	
	// Prop Models
	IF (ServerBD.iMaxNetworkPeds > 0)
		REPEAT ServerBD.iMaxNetworkPeds iPed
			UNLOAD_PED_MODELS(ePedLocation ,INT_TO_ENUM(PED_MODELS, ServerBD.NetworkPedData[iPed].Data.iPedModel), INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[iPed].Data.iActivity))
		ENDREPEAT
	ENDIF
	
	// VFX Effects
	IF (ServerBD.iMaxNetworkPeds > 0)
		REPEAT ServerBD.iMaxNetworkPeds iPed
			IF (ServerBD.iVFXPedID[iPed] != -1)
				CLEANUP_PED_VFX(ServerBD.VFXData[ServerBD.iVFXPedID[iPed]].LoopingPTFX)
			ENDIF
		ENDREPEAT
	ENDIF
	
	PRINTLN("[AM_MP_PEDS] UNLOAD_ALL_PED_ASSETS - All peds assets have been unloaded")
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ CUTSCENES ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets the visibility state of a local ped.
/// PARAMS:
///    Data - Ped data to query.
///    bVisible - Visibility state.
PROC SET_LOCAL_PED_VISIBLE(PEDS_DATA &Data, BOOL bVisible)
	IF NOT DOES_ENTITY_EXIST(Data.PedID)
		EXIT
	ENDIF
	
	SET_ENTITY_VISIBLE(Data.PedID, bVisible)
	SET_LOCAL_PED_PROPS_VISIBLE(Data, bVisible)
ENDPROC

/// PURPOSE:
///     Sets the visibility state of a networked ped.
/// PARAMS:
///    NetworkPedData - Network ped data to query.
///    bVisible - Visibility state.
///    iPedsToUpdate - Counts the number of peds still needing their visibility state changed.
///    iPropsToUpdate - Counts the number of props still needing their visibility state changed.
PROC SET_NETWORK_PED_VISIBLE(NETWORK_PED_DATA &NetworkPedData, BOOL bVisible, INT &iPedsToUpdate, INT &iPropsToUpdate, BOOL RemotelyVisible = FALSE)
	IF NOT DOES_ENTITY_EXIST(NetworkPedData.Data.PedID)
		EXIT
	ENDIF
	
	IF (bVisible AND NOT IS_ENTITY_VISIBLE(NetworkPedData.Data.PedID))	// Still to turn visible
	OR (NOT bVisible AND IS_ENTITY_VISIBLE(NetworkPedData.Data.PedID))	// Still to turn invisible
		iPedsToUpdate++
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(NetworkPedData.niPedID)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(NetworkPedData.niPedID)
				SET_ENTITY_VISIBLE(NetworkPedData.Data.PedID, bVisible)
				SET_ENTITY_VISIBLE_IN_CUTSCENE(NET_TO_OBJ(NetworkPedData.niPedID), bVisible)
				SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedData.niPedID, bVisible, RemotelyVisible)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(NetworkPedData.niPedID)
			ENDIF
		ENDIF
	ENDIF
	
	SET_NETWORK_PED_PROPS_VISIBLE(NetworkPedData, bVisible, iPropsToUpdate)
ENDPROC

/// PURPOSE:
///    Set the visibility state for all peds (local and networked).
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server data to query.
///    LocalData - Local ped data to query.
///    bVisible - Visibility state.
///    bCutscene - Changing peds visibility for a cutscene. The ped data bit BS_PED_DATA_USE_PED_IN_CUTSCENE
///    			   is checked here. If this bit is set the ped will remain visible.
/// RETURNS: TRUE when all peds have set their visibility to the bVisible param
FUNC BOOL SET_ALL_SCRIPT_PEDS_VISIBILITY(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, BOOL bVisible, BOOL bCutscene)
	
	INT iPed
	REPEAT ServerBD.iMaxLocalPeds iPed
		IF (bCutscene)
			IF IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
			AND SHOULD_HIDE_CUTSCENE_PED(ePedLocation, LocalData, ServerBD.iLayout, iPed)
				SET_LOCAL_PED_VISIBLE(LocalData.PedData[iPed], bVisible)
			ENDIF
		ELSE
			SET_LOCAL_PED_VISIBLE(LocalData.PedData[iPed], bVisible)
		ENDIF
	ENDREPEAT
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		RETURN TRUE
	ENDIF
	
	INT iPedsToUpdate
	INT iPropsToUpdate

	// Host hides the networked peds
	REPEAT ServerBD.iMaxNetworkPeds iPed
		IF (bCutscene)
			IF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPed].Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
			AND SHOULD_HIDE_CUTSCENE_PED(ePedLocation, LocalData, ServerBD.iLayout, iPed)
				SET_NETWORK_PED_VISIBLE(ServerBD.NetworkPedData[iPed], bVisible, iPedsToUpdate, iPropsToUpdate, IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPed].Data.iBS, BS_PED_DATA_REMOTELY_VISIBLE))
			ENDIF
		ELSE
			SET_NETWORK_PED_VISIBLE(ServerBD.NetworkPedData[iPed], bVisible, iPedsToUpdate, iPropsToUpdate, IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPed].Data.iBS, BS_PED_DATA_REMOTELY_VISIBLE))
		ENDIF
	ENDREPEAT
	
	PRINTLN("[AM_MP_PEDS] HIDE_ALL_SCRIPT_PEDS - Network Peds - bVisible: ", bVisible, " bCutscene: ", bCutscene, " iPedsToUpdate: ", iPedsToUpdate, " iPropsToUpdate: ", iPropsToUpdate)
	RETURN (iPedsToUpdate = 0 AND iPropsToUpdate = 0)
ENDFUNC

PROC HIDE_NETWORK_PED_PROPS_DURING_CUTSCENE(SERVER_PED_DATA &ServerBD, INT iNetworkPed)
	
	INT iProp
	REPEAT MAX_NUM_PED_PROPS iProp
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[iNetworkPed].niPropID[iProp])
			SET_ENTITY_LOCALLY_INVISIBLE(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[iNetworkPed].niPropID[iProp]))
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC HIDE_NETWORK_PEDS_DURING_CUTSCENE(SERVER_PED_DATA &ServerBD)
	
	INT iNetworkPed
	REPEAT ServerBD.iMaxNetworkPeds iNetworkPed
		IF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iNetworkPed].Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[iNetworkPed].niPedID)
				SET_ENTITY_LOCALLY_INVISIBLE(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[iNetworkPed].niPedID))
				HIDE_NETWORK_PED_PROPS_DURING_CUTSCENE(ServerBD, iNetworkPed)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CREATION ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Determines if a ped should be created.
/// PARAMS:
///    iPedBS - Ped bitset to query.
///    iPedLevel - Ped level to query.
///    iServerBDPedLevel - Server ped level to query.
/// RETURNS: TRUE if the ped should be created.
FUNC BOOL SHOULD_PED_BE_CREATED(INT &iPedBS[PEDS_DATA_BITSET_ARRAY_SIZE], INT iPedLevel, INT iServerPedLevel)
	IF IS_PEDS_BIT_SET(iPedBS, BS_PED_DATA_SKIP_PED)
	OR IS_PEDS_BIT_SET(iPedBS, BS_PED_DATA_FADE_PED)
	OR IS_PEDS_BIT_SET(iPedBS, BS_PED_DATA_FADE_PED_COMPLETE)
	OR IS_PEDS_BIT_SET(iPedBS, BS_PED_DATA_PROPS_ONLY_NO_PED)
		RETURN FALSE
	ENDIF
	
	RETURN (iPedLevel <= iServerPedLevel)
ENDFUNC

/// PURPOSE:
///    Checks if a peds spawn coords are currently being blocking by a player.
/// PARAMS:
///    Data - Ped data to query.
/// RETURNS: TRUE if a player is blocking a peds spawn coords.
FUNC BOOL IS_ANY_PLAYER_BLOCKING_PED_COORDS(PEDS_DATA &Data)
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		    PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
		    IF IS_NET_PLAYER_OK(playerID)
				IF ARE_VECTORS_ALMOST_EQUAL(GET_PLAYER_COORDS(playerID), Data.vPosition, 0.75, TRUE)
					RETURN TRUE
				ENDIF
			ENDIF
	    ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Wrapper function for the global g_bInitPedsCreated.
/// RETURNS:TRUE if all peds have been created.
FUNC BOOL HAVE_ALL_PEDS_BEEN_CREATED()
	RETURN g_bInitPedsCreated
ENDFUNC

/// PURPOSE:
///    Gets the peds prop spawn coords.
///    If a ph bone is specified in the data, it will use: GET_PED_BONE_COORDS.
///    Otherwise it will use specific spawn coords for non interactive props, or GET_ANIM_INITIAL_OFFSET_POSITION for props that are animated.
/// PARAMS:
///    Data - Ped data to query.
///    pedPropData - Ped prop data to query. Checks bone indexes.
///    pedPropAnimData - Ped prop anim data to query. Checks starting coords based off anim offsets.
/// RETURNS: The peds spawn coords.
FUNC VECTOR GET_PED_PROP_SPAWN_COORDS(PEDS_DATA &Data, PED_PROP_DATA pedPropData, PED_PROP_ANIM_DATA pedPropAnimData)
	VECTOR vSpawnCoords = Data.vPosition
	
	IF (pedPropData.ePedBone = BONETAG_NULL)
		IF pedPropData.bUseCustomSpawnLocation
			vSpawnCoords = pedPropData.vPropSpawnCoords
		ELSE
			IF NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimDict)
			AND NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimClip)
				vSpawnCoords = GET_ANIM_INITIAL_OFFSET_POSITION(pedPropAnimData.sPropAnimDict, pedPropAnimData.sPropAnimClip, Data.vPosition, Data.vRotation)
				IF IS_VECTOR_ZERO(vSpawnCoords)
					vSpawnCoords = Data.vPosition
				ENDIF
			ENDIF
		ENDIF
	ELSE
		vSpawnCoords = GET_PED_BONE_COORDS(Data.PedID, pedPropData.ePedBone, pedPropData.vBoneOffsetPosition)
	ENDIF
	
	RETURN vSpawnCoords
ENDFUNC

/// PURPOSE:
///    Gets the peds spawn rotation.
/// PARAMS:
///    Data - Ped data to query.
///    pedPropData - Ped prop data to query. Checks bone indexes.
///    pedPropAnimData - Ped prop anim data to query. Checks starting coords based off anim offsets.
/// RETURNS: The peds spawn rotation. 
FUNC VECTOR GET_PED_PROP_SPAWN_ROTATION(PEDS_DATA &Data, PED_PROP_DATA pedPropData, PED_PROP_ANIM_DATA pedPropAnimData)
	VECTOR vSpawnRotation = <<0.0, 0.0, 0.0>>
	
	IF (pedPropData.ePedBone = BONETAG_NULL)
		IF pedPropData.bUseCustomSpawnLocation
			vSpawnRotation = pedPropData.vPropSpawnRotation
		ELSE
			IF NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimDict)
			AND NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimClip)
				vSpawnRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(pedPropAnimData.sPropAnimDict, pedPropAnimData.sPropAnimClip, Data.vPosition, Data.vRotation)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN vSpawnRotation
ENDFUNC

/// PURPOSE:
///    Sets the peds components and clothing components based on the packed drawables and textures.
/// PARAMS:
///    PedID - Ped to set clothing components on.
///    iPackedDrawable - Packed drawable array which holds all the packed peds clothing component drawables.
///    iPackedTexture - Packed texture array which holds all the packed peds clothing component textures.
PROC SET_PED_CLOTHING_COMPONENTS(PED_INDEX &PedID, INT &iPackedDrawable[MAX_NUM_PED_CLOTHING_COMPONENT_VARS], INT &iPackedTexture[MAX_NUM_PED_CLOTHING_COMPONENT_VARS])
	IF IS_PED_CLOTHING_COMPONENT_CONFIG_VALID(iPackedDrawable, iPackedTexture)
		PED_CLOTHING_COMPONENT_CONFIG ComponentConfig
		UNPACK_PED_CLOTHING_COMPONENT_CONFIG(iPackedDrawable, iPackedTexture, ComponentConfig)
		APPLY_PED_CLOTHING_COMPONENT_CONFIG(PedID, ComponentConfig)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives the ped a weapon to equip if the weapon data is valid.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    iPed - Ped to query.
PROC GIVE_PED_WEAPON_PROP(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, INT iPed)
	PED_PROP_WEAPON_DATA pedPropWeaponData
	GET_PED_PROP_WEAPON_DATA(ePedLocation, pedPropWeaponData, iPed)
	IF (pedPropWeaponData.eWeapon != WEAPONTYPE_INVALID)
		GIVE_WEAPON_TO_PED(Data.PedID, pedPropWeaponData.eWeapon, pedPropWeaponData.iAmmo, pedPropWeaponData.bForceIntoHand, pedPropWeaponData.bEquipWeapon)
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates local ped props.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    iPed - Ped to query.
///    eMusicIntensity - For dancing peds. Requests the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC CREATE_LOCAL_SCRIPT_PED_PROPS(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, INT iPed, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bVisible = TRUE)
	
	INT iProp
	PED_PROP_DATA pedPropData
	PED_PROP_ANIM_DATA pedPropAnimData
	
	REPEAT MAX_NUM_PED_PROPS iProp
		GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), iProp, pedPropData)
		IF (IS_MODEL_VALID(pedPropData.ePropModel) AND pedPropData.ePropModel != DUMMY_MODEL_FOR_SCRIPT)
			IF NOT DOES_ENTITY_EXIST(Data.PropID[iProp])
				
				GET_PED_PROP_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedPropAnimData, iProp, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HEELED_PED))
				
				VECTOR vSpawnCoord = GET_PED_PROP_SPAWN_COORDS(Data, pedPropData, pedPropAnimData)
				VECTOR vSpawnRotation = GET_PED_PROP_SPAWN_ROTATION(Data, pedPropData, pedPropAnimData)
				
				Data.PropID[iProp] = CREATE_OBJECT_NO_OFFSET(pedPropData.ePropModel, vSpawnCoord, FALSE, FALSE)
				
				IF !bVisible
					SET_LOCAL_PED_VISIBLE(Data, FALSE)
				ENDIF
				
				IF NOT IS_VECTOR_ZERO(vSpawnRotation)
					SET_ENTITY_ROTATION(Data.PropID[iProp], vSpawnRotation)
				ENDIF
				
				IF pedPropData.bAttachToBone
					ATTACH_ENTITY_TO_ENTITY(Data.PropID[iProp], Data.PedID, GET_PED_BONE_INDEX(Data.PedID, pedPropData.ePedBone), pedPropData.vBoneOffsetPosition, pedPropData.vBoneOffsetRotation, TRUE, TRUE)
				ENDIF
				
				IF pedPropData.bFreezeProp
					FREEZE_ENTITY_POSITION(Data.PropID[iProp], TRUE)
				ENDIF
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	GIVE_PED_WEAPON_PROP(ePedLocation, Data, iPed)
	
ENDPROC

/// PURPOSE:
///    Gets the max number of peds that can be created per frame.
///    This uses the global INT g_iMaxPedsCreatedOnInit for creating peds on initialise.
///    This uses the global INT g_iMaxPedsCreatedPerFrame for creating peds via culling.
/// RETURNS: The max number of peds that can be created per frame as an INT.
FUNC INT GET_MAX_PEDS_CREATED_PER_FRAME()
	IF NOT HAVE_ALL_PEDS_BEEN_CREATED()
		RETURN g_iMaxPedsCreatedOnInit
	ENDIF
	RETURN g_iMaxPedsCreatedPerFrame
ENDFUNC

/// PURPOSE:
///    Creates a local ped.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    LocalData - Local script data that contains the ped data.
///    iLayout - Ped layout to query.
///    iPed - Ped to create.
///    iPedsCreatedPerFrame - Numbers of peds created this frame.
///    eMusicIntensity - For dancing peds. Requests the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC CREATE_LOCAL_SCRIPT_PED(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, INT iLayout, INT iPed, INT &iPedsCreatedPerFrame, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	IF (iPedsCreatedPerFrame < GET_MAX_PEDS_CREATED_PER_FRAME())	
		REQUEST_PED_ASSETS(ePedLocation, Data, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		IF HAVE_PED_ASSETS_LOADED(ePedLocation, Data, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
			Data.PedID = CREATE_PED(PEDTYPE_MISSION, GET_PEDS_MODEL(INT_TO_ENUM(PED_MODELS, Data.iPedModel)), Data.vPosition, Data.vRotation.z, FALSE, FALSE)
			
			//BOOL bVisible = TRUE
			IF NETWORK_IS_IN_MP_CUTSCENE()
				IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				AND SHOULD_HIDE_CUTSCENE_PED(ePedLocation, LocalData, iLayout, iPed)
					SET_LOCAL_PED_VISIBLE(Data, FALSE)
					//bVisible = FALSE
				ENDIF
			ENDIF
			
			
			IF DOES_ENTITY_EXIST(Data.PedID)
				SET_ENTITY_ROTATION(Data.PedID, Data.vRotation, DEFAULT, FALSE)
				
				SET_PED_CLOTHING_COMPONENTS(Data.PedID, Data.iPackedDrawable, Data.iPackedTexture)
				SET_LOCAL_PED_PROPERTIES(ePedLocation, Data.PedID, iPed)
				SET_PED_PROP_INDEXES(ePedLocation, Data.PedID, iPed, iLayout, INT_TO_ENUM(PED_MODELS, Data.iPedModel))
				
				SET_PED_ALT_MOVEMENT(ePedLocation, Data.PedID, iPed)
				CREATE_LOCAL_SCRIPT_PED_PROPS(ePedLocation, Data, iPed, eMusicIntensity, ePedMusicIntensity, bDancingTransition)//, bVisible)
				
				IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_FREEZE_PED)
					FREEZE_ENTITY_POSITION(Data.PedID, TRUE)
				ENDIF
				
				IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_FADE_IN_PED)
					SET_ENTITY_ALPHA(Data.PedID, 0, FALSE)
					INT iProp
					REPEAT MAX_NUM_PED_PROPS iProp
						IF DOES_ENTITY_EXIST(Data.PropID[iProp])
							SET_ENTITY_ALPHA(Data.PropID[iProp], 0, FALSE)
						ELSE
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
			
				
				iPedsCreatedPerFrame++
				PRINTLN("[AM_MP_PEDS] CREATE_LOCAL_SCRIPT_PED - Created local ped: ", iPed, " this frame ped index: ", NATIVE_TO_INT(Data.PedID))
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[AM_MP_PEDS] CREATE_LOCAL_SCRIPT_PED - Bail: Already created ", GET_MAX_PEDS_CREATED_PER_FRAME(), " peds this frame")
	ENDIF
	
ENDPROC

PROC RESET_LOCAL_SCRIPT_PED(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, SERVER_PED_DATA &ServerBD, INT iPed)
	
	PRINTLN("[AM_MP_PEDS] RESET_LOCAL_SCRIPT_PED ")
	Data.vRotation = <<0.0, 0.0, -1.0>>
	RESET_PED_PACKED_CLOTHING_COMPONENT_VARS(Data.iPackedDrawable, Data.iPackedTexture)
	
	SET_PED_DATA(ePedLocation, ServerBD, Data, iPed, ServerBD.iLayout)
			
	IF DOES_ENTITY_EXIST(Data.PedID)
		SET_ENTITY_COORDS(Data.PedID, Data.vPosition)
		SET_ENTITY_ROTATION(Data.PedID, Data.vRotation, DEFAULT, FALSE)
		
		SET_PED_CLOTHING_COMPONENTS(Data.PedID, Data.iPackedDrawable, Data.iPackedTexture)
		SET_LOCAL_PED_PROPERTIES(ePedLocation, Data.PedID, iPed)
		SET_PED_PROP_INDEXES(ePedLocation, Data.PedID, iPed, ServerBD.iLayout, INT_TO_ENUM(PED_MODELS, Data.iPedModel))
		
		SET_PED_ALT_MOVEMENT(ePedLocation, Data.PedID, iPed)
		CREATE_LOCAL_SCRIPT_PED_PROPS(ePedLocation, Data, iPed)//, bVisible)
		
		IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_FREEZE_PED)
			FREEZE_ENTITY_POSITION(Data.PedID, TRUE)
		ENDIF
		
		PRINTLN("[AM_MP_PEDS] RESET_LOCAL_SCRIPT_PED - Restarted local ped: ", iPed, " this frame ped index: ", NATIVE_TO_INT(Data.PedID))
	ELSE
		PRINTLN("[AM_MP_PEDS] RESET_LOCAL_SCRIPT_PED  local ped: ", iPed, " doesn't exist.")
	ENDIF
	
ENDPROC

FUNC BOOL ARE_NETWORK_SCRIPT_PED_PROPS_CREATED(PED_LOCATIONS ePedLocation, NETWORK_PED_DATA &NetworkPedData)
	
	INT iProp
	PED_PROP_DATA pedPropData
		
	REPEAT MAX_NUM_NETWORKED_SYNCED_SCENE_PED_PROPS iProp
		GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, NetworkPedData.Data.iActivity), iProp, pedPropData)
		IF (IS_MODEL_VALID(pedPropData.ePropModel) AND pedPropData.ePropModel != DUMMY_MODEL_FOR_SCRIPT)
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(NetworkPedData.niPropID[iProp])
				RETURN FALSE
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates networked ped props.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    NetworkPedData - The network server BD data to query.
///    iPed - Ped to query.
///    eMusicIntensity - For dancing peds. Requests the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC CREATE_NETWORK_SCRIPT_PED_PROPS(PED_LOCATIONS ePedLocation, NETWORK_PED_DATA &NetworkPedData, INT iPed, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	INT iProp
	PED_PROP_DATA pedPropData
	PED_PROP_ANIM_DATA pedPropAnimData
	
	REPEAT MAX_NUM_NETWORKED_SYNCED_SCENE_PED_PROPS iProp
		GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, NetworkPedData.Data.iActivity), iProp, pedPropData)
		IF (IS_MODEL_VALID(pedPropData.ePropModel) AND pedPropData.ePropModel != DUMMY_MODEL_FOR_SCRIPT)
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(NetworkPedData.niPropID[iProp])
				
				GET_PED_PROP_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, NetworkPedData.Data.iActivity), pedPropAnimData, iProp, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(NetworkPedData.Data.iBS, BS_PED_DATA_HEELED_PED))
				
				BOOL bMPCutsceneActive 	= NETWORK_IS_IN_MP_CUTSCENE()
				VECTOR vSpawnCoord 		= GET_PED_PROP_SPAWN_COORDS(NetworkPedData.Data, pedPropData, pedPropAnimData)
				VECTOR vSpawnRotation 	= GET_PED_PROP_SPAWN_ROTATION(NetworkPedData.Data, pedPropData, pedPropAnimData)
				
				IF bMPCutsceneActive
					SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
				ENDIF
				
				IF CREATE_NET_OBJ(NetworkPedData.niPropID[iProp], pedPropData.ePropModel, vSpawnCoord)
					NetworkPedData.Data.PropID[iProp] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(NetworkPedData.niPropID[iProp]))
					SET_NETWORK_PED_PROPERTIES(ePedLocation, NetworkPedData.niPropID[iProp], NetworkPedData.Data.iBS)
					
					IF NOT IS_VECTOR_ZERO(vSpawnRotation)
						SET_ENTITY_ROTATION(NetworkPedData.Data.PropID[iProp], vSpawnRotation)
					ENDIF
					
					IF pedPropData.bAttachToBone
					AND NOT IS_PEDS_BIT_SET(NetworkPedData.Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED) //just in case someone accidentally tries to attach prop to ped that doesn't exist
						ATTACH_ENTITY_TO_ENTITY(NetworkPedData.Data.PropID[iProp], NetworkPedData.Data.PedID, GET_PED_BONE_INDEX(NetworkPedData.Data.PedID, pedPropData.ePedBone), pedPropData.vBoneOffsetPosition, pedPropData.vBoneOffsetRotation, TRUE, TRUE)
					ENDIF
				ENDIF
				
				IF bMPCutsceneActive
					SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
				ENDIF
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	GIVE_PED_WEAPON_PROP(ePedLocation, NetworkPedData.Data, iPed)
	
ENDPROC

/// PURPOSE:
///    Creates a networked ped.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    NetworkPedData - The network server BD data to query.
///    iNetworkPedArrayID - The network array ID (0-MAX_NUM_TOTAL_NETWORK_PEDS)
///    iPed - The converted Ped ID (125-134, consts NETWORK_PED_ID_0 etc.)
///    eMusicIntensity - For dancing peds. Requests the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC CREATE_NETWORK_SCRIPT_PED(PED_LOCATIONS ePedLocation, NETWORK_PED_DATA &NetworkPedData, INT iPed, INT iLayout, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	REQUEST_PED_ASSETS(ePedLocation, NetworkPedData.Data, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	IF HAVE_PED_ASSETS_LOADED(ePedLocation, NetworkPedData.Data, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		IF CREATE_NET_PED(NetworkPedData.niPedID, PEDTYPE_MISSION, GET_PEDS_MODEL(INT_TO_ENUM(PED_MODELS, NetworkPedData.Data.iPedModel)), NetworkPedData.Data.vPosition, NetworkPedData.Data.vRotation.z)
			
			NetworkPedData.Data.PedID = GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(NetworkPedData.niPedID))
			SET_ENTITY_ROTATION(NetworkPedData.Data.PedID, NetworkPedData.Data.vRotation, DEFAULT, FALSE)
			SET_PED_CLOTHING_COMPONENTS(NetworkPedData.Data.PedID, NetworkPedData.Data.iPackedDrawable, NetworkPedData.Data.iPackedTexture)
			SET_LOCAL_PED_PROPERTIES(ePedLocation, NetworkPedData.Data.PedID, iPed)
			SET_NETWORK_PED_PROPERTIES(ePedLocation, NetworkPedData.niPedID, NetworkPedData.Data.iBS)
			SET_PED_PROP_INDEXES(ePedLocation, NetworkPedData.Data.PedID, iPed, iLayout, INT_TO_ENUM(PED_MODELS, NetworkPedData.Data.iPedModel))
			
			SET_PED_ALT_MOVEMENT(ePedLocation, NetworkPedData.Data.PedID, iPed)
			CREATE_NETWORK_SCRIPT_PED_PROPS(ePedLocation, NetworkPedData, iPed, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
			IF IS_PEDS_BIT_SET(NetworkPedData.Data.iBS, BS_PED_DATA_FREEZE_PED)
				FREEZE_ENTITY_POSITION(NetworkPedData.Data.PedID, TRUE)
			ENDIF
			
			PRINTLN("[AM_MP_PEDS] CREATE_NETWORK_SCRIPT_PED - Created network ped: ", (iPed-MAX_NUM_TOTAL_LOCAL_PEDS), " (", iPed, ") this frame")
		ENDIF
	ENDIF
	
ENDPROC

PROC CREATE_NETWORK_SCRIPT_NO_PED_ONLY_PROPS(PED_LOCATIONS ePedLocation, NETWORK_PED_DATA &NetworkPedData, INT iPed, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	REQUEST_PED_ASSETS(ePedLocation, NetworkPedData.Data, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	IF HAVE_PED_ASSETS_LOADED(ePedLocation, NetworkPedData.Data, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		CREATE_NETWORK_SCRIPT_PED_PROPS(ePedLocation, NetworkPedData, iPed, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		PRINTLN("[AM_MP_PEDS] CREATE_NETWORK_SCRIPT_PED - Created network no-ped only props : ", (iPed-MAX_NUM_TOTAL_LOCAL_PEDS), " (", iPed, ") this frame")
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the global flag g_bInitPedsCreated to tell the parent script all the peds (local and networked) have been created.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    ServerBD - Server data to query.
PROC SET_PEDS_CREATED_FLAG(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data[])
	IF NOT g_bInitPedsCreated
		
		INT iPed
		INT iLocalPedsCreated = 0
		INT iNetworkPedsCreated = 0
		
		REPEAT ServerBD.iMaxLocalPeds iPed
			IF HAS_PED_BEEN_CREATED(ePedLocation, Data[iPed], ServerBD.iLevel)
				iLocalPedsCreated++
			ELSE
				PRINTLN("[AM_MP_PEDS] SET_PEDS_CREATED_FLAG - Local Ped: ", iPed, " Has Yet To Be Created")
			ENDIF
		ENDREPEAT
		
		PRINTLN("[AM_MP_PEDS] SET_PEDS_CREATED_FLAG - Local Peds Created: ", iLocalPedsCreated, "/", ServerBD.iMaxLocalPeds)
		
		IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF (iLocalPedsCreated = ServerBD.iMaxLocalPeds)
				PRINTLN("[AM_MP_PEDS] SET_PEDS_CREATED_FLAG - g_bInitPedsCreated = TRUE")
				PRINTLN("[AM_MP_PEDS] ")
				g_bInitPedsCreated = TRUE
				
				REPEAT ServerBD.iMaxLocalPeds iPed
					UNLOAD_PED_MODELS(ePedLocation, INT_TO_ENUM(PED_MODELS, Data[iPed].iPedModel), INT_TO_ENUM(PED_ACTIVITIES, Data[iPed].iActivity))
				ENDREPEAT
			ENDIF
			EXIT
		ENDIF
		
		REPEAT ServerBD.iMaxNetworkPeds iPed
			IF HAS_PED_BEEN_CREATED(ePedLocation, ServerBD.NetworkPedData[iPed].Data, ServerBD.iLevel)
				iNetworkPedsCreated++
			ELSE
				PRINTLN("[AM_MP_PEDS] SET_PEDS_CREATED_FLAG - Network Ped: ", iPed, " Has Yet To Be Created")
			ENDIF
		ENDREPEAT
		
		PRINTLN("[AM_MP_PEDS] SET_PEDS_CREATED_FLAG - Network Peds Created: ", iNetworkPedsCreated, "/", ServerBD.iMaxNetworkPeds)
		
		IF (iLocalPedsCreated = ServerBD.iMaxLocalPeds AND iNetworkPedsCreated = ServerBD.iMaxNetworkPeds)
			PRINTLN("[AM_MP_PEDS] SET_PEDS_CREATED_FLAG - g_bInitPedsCreated = TRUE")
			PRINTLN("[AM_MP_PEDS] ")
			g_bInitPedsCreated = TRUE
			
			REPEAT ServerBD.iMaxNetworkPeds iPed
				UNLOAD_PED_MODELS(ePedLocation, INT_TO_ENUM(PED_MODELS, ServerBD.NetworkPedData[iPed].Data.iPedModel), INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[iPed].Data.iActivity))
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DELETION ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Deletes all the props of a local ped
/// PARAMS:
///    Data - Ped data to query.
PROC DELETE_LOCAL_SCRIPT_PED_PROPS(PEDS_DATA &Data)
	
	INT iProp
	REPEAT MAX_NUM_PED_PROPS iProp
		#IF IS_DEBUG_BUILD
			PRINTLN("[AM_MP_PEDS] DELETE_LOCAL_SCRIPT_PED_PROPS 		PROP: " , iProp)
		#ENDIF
		SAFE_DELETE_OBJECT(Data.PropID[iProp])
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Deletes a local ped.
/// PARAMS:
///    Data - Ped data to query.
PROC DELETE_LOCAL_SCRIPT_PED(PEDS_DATA &Data)
	PRINTLN("[AM_MP_PEDS] DELETE_LOCAL_SCRIPT_PED " , NATIVE_TO_INT(Data.PedID))
	#IF IS_DEBUG_BUILD
	PRINTLN("[AM_MP_PEDS] DELETE_LOCAL_SCRIPT_PED MODEL: " , DEBUG_GET_PED_MODEL_STRING(INT_TO_ENUM(PED_MODELS, Data.iPedModel)))
	PRINTLN("[AM_MP_PEDS] DELETE_LOCAL_SCRIPT_PED MODEL: " , DEBUG_GET_PED_ACTIVITY_STRING(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity)))
	#ENDIF
	DELETE_LOCAL_SCRIPT_PED_PROPS(Data)
	SAFE_DELETE_PED(Data.PedID)
ENDPROC

/// PURPOSE:
///    Used to delete peds while inside a ped location during update. E.G. active ped total changes due to player count.
/// PARAMS:
///    Data - Ped data to query.
///    iPedsDeletedPerFrame - Numbers of peds deleted this frame.
///    iPedID - Ped to delete.
PROC DELETE_LOCAL_SCRIPT_PED_UPDATE(PEDS_DATA &Data, INT &iPedsDeletedPerFrame, INT iPedID)
	UNUSED_PARAMETER(iPedID)
	
	IF (iPedsDeletedPerFrame < GET_MAX_PEDS_CREATED_PER_FRAME())	// Reuse peds created globals
		DELETE_LOCAL_SCRIPT_PED_PROPS(Data)
		SAFE_DELETE_PED(Data.PedID)
		iPedsDeletedPerFrame++
		PRINTLN("[AM_MP_PEDS] DELETE_LOCAL_SCRIPT_PED_UPDATE - Deleted local ped: ", iPedID, " this frame")
	ELSE
		PRINTLN("[AM_MP_PEDS] DELETE_LOCAL_SCRIPT_PED_UPDATE - Bail: Already deleted ", GET_MAX_PEDS_CREATED_PER_FRAME(), " peds this frame")
	ENDIF
ENDPROC

/// PURPOSE:
///    Used to delete peds while inside a ped location during update. E.G. active ped total changes due to player count.
/// PARAMS:
///    Data - Ped data to query.
///    iPedsDeletedPerFrame - Numbers of peds deleted this frame.
///    iPedID - Ped to delete.
PROC DELETE_NETWORK_SCRIPT_PED_UPDATE(PEDS_DATA &Data, NETWORK_INDEX niPed, INT &iPedsDeletedPerFrame, INT iPedID)
	UNUSED_PARAMETER(iPedID)
	UNUSED_PARAMETER(Data)
	IF (iPedsDeletedPerFrame < GET_MAX_PEDS_CREATED_PER_FRAME())	// Reuse peds created globals
		PED_INDEX piPed = GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(niPed))
		SAFE_DELETE_PED(piPed)
		iPedsDeletedPerFrame++
		PRINTLN("[AM_MP_PEDS] DELETE_LOCAL_SCRIPT_PED_UPDATE - Deleted local ped: ", iPedID, " this frame")
	ELSE
		PRINTLN("[AM_MP_PEDS] DELETE_LOCAL_SCRIPT_PED_UPDATE - Bail: Already deleted ", GET_MAX_PEDS_CREATED_PER_FRAME(), " peds this frame")
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes all the props of a network ped
/// PARAMS:
///    NetworkPedData - Network ped data to query.
/// RETURNS: TRUE when all network ped props have been deleted.
FUNC BOOL DELETE_NETWORK_SCRIPT_PED_PROPS(NETWORK_PED_DATA &NetworkPedData)
	
	INT iProp
	INT iDeletedProps = 0
	
	REPEAT MAX_NUM_PED_PROPS iProp
		IF DOES_ENTITY_EXIST(NetworkPedData.Data.PropID[iProp])
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(NetworkPedData.niPropID[iProp])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(NetworkPedData.niPropID[iProp])
					SAFE_DELETE_OBJECT(NetworkPedData.Data.PropID[iProp])
					DELETE_NET_ID(NetworkPedData.niPropID[iProp])
					iDeletedProps++
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(NetworkPedData.niPropID[iProp])
				ENDIF
			ENDIF
		ELSE
			iDeletedProps++
		ENDIF
	ENDREPEAT
	
	RETURN (iDeletedProps = MAX_NUM_PED_PROPS)
ENDFUNC

/// PURPOSE:
///    Deletes a network ped.
/// PARAMS:
///    NetworkPedData - Network ped data to query.
///    iNetworkPedArrayID - Array ID of network ped to delete.
/// RETURNS: TRUE when the network ped has been deleted.
FUNC BOOL DELETE_NETWORK_SCRIPT_PED(NETWORK_PED_DATA &NetworkPedData, INT iNetworkPedArrayID)
	UNUSED_PARAMETER(iNetworkPedArrayID)
	
	BOOL bPedDeleted = FALSE
	BOOL bPropsDeleted = FALSE
	
	IF DELETE_NETWORK_SCRIPT_PED_PROPS(NetworkPedData)
		bPropsDeleted = TRUE
	ENDIF
	
	IF NOT bPropsDeleted
		RETURN FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(NetworkPedData.Data.PedID)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(NetworkPedData.niPedID)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(NetworkPedData.niPedID)
				SAFE_DELETE_PED(NetworkPedData.Data.PedID)
				DELETE_NET_ID(NetworkPedData.niPedID)
				bPedDeleted = TRUE
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(NetworkPedData.niPedID)
			ENDIF
		ENDIF
	ELSE
		bPedDeleted = TRUE
	ENDIF
	
	PRINTLN("[AM_MP_PEDS] DELETE_NETWORK_SCRIPT_PED - Network ped: ", iNetworkPedArrayID, " bPropsDeleted:", bPropsDeleted, " bPedDeleted: ", bPedDeleted)
	
	IF (bPropsDeleted AND bPedDeleted)
		PRINTLN("[AM_MP_PEDS] DELETE_NETWORK_SCRIPT_PED - Deleted network ped: ", iNetworkPedArrayID)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    	Deletes all peds (local and networked) and their props.
///     This is a bool function as requesting control of network peds to delete may take multiple frames.
/// PARAMS:
///    ServerBD - Server data to query.
///    Data - Ped data to query.
/// RETURNS: TRUE when all the peds have been deleted.
FUNC BOOL DELETE_ALL_SCRIPT_PEDS(SERVER_PED_DATA &ServerBD, PEDS_DATA &Data[])
	
	INT iPed
	IF (ServerBD.iMaxLocalPeds > 0)
		REPEAT ServerBD.iMaxLocalPeds iPed
			DELETE_LOCAL_SCRIPT_PED(Data[iPed])
			PRINTLN("[AM_MP_PEDS] DELETE_ALL_SCRIPT_PEDS - Local ped: ", iPed, " has been deleted")
		ENDREPEAT
	ENDIF
	
	PRINTLN("[AM_MP_PEDS] DELETE_ALL_SCRIPT_PEDS - All local peds have been deleted")
	
	INT iProp
	IF (ServerBD.iMaxNetworkPeds > 0)
		REPEAT ServerBD.iMaxNetworkPeds iPed
			
			// Let network ped migrate
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[iPed].niPedID)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(ServerBD.NetworkPedData[iPed].niPedID)
					SET_NETWORK_ID_CAN_MIGRATE(ServerBD.NetworkPedData[iPed].niPedID, TRUE)
					PRINTLN("[AM_MP_PEDS] DELETE_ALL_SCRIPT_PEDS - Setting network ped: ", iPed, " as migratable")
				ENDIF
			ENDIF
			
			// Let network props migrate
			REPEAT MAX_NUM_PED_PROPS iProp
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[iPed].niPropID[iProp])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(ServerBD.NetworkPedData[iPed].niPropID[iProp])
						SET_NETWORK_ID_CAN_MIGRATE(ServerBD.NetworkPedData[iPed].niPropID[iProp], TRUE)
						PRINTLN("[AM_MP_PEDS] DELETE_ALL_SCRIPT_PEDS - Setting network ped: ", iPed, " Prop: ", iProp, " as migratable")
					ENDIF
				ENDIF
			ENDREPEAT
			
		ENDREPEAT
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    	Deletes all peds (local and networked) and their props.
///     This is a bool function as requesting control of network peds to delete may take multiple frames.
/// PARAMS:
///    ServerBD - Server data to query.
///    Data - Ped data to query.
/// RETURNS: TRUE when all the peds have been deleted.
FUNC BOOL DELETE_LOCAL_RESETTABLE_SCRIPT_PEDS(SCRIPT_PED_DATA &LocalData, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data[])
	
	INT iPed
	IF (ServerBD.iMaxLocalPeds > 0)
		REPEAT ServerBD.iMaxLocalPeds iPed
			IF IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS,  BS_PED_DATA_RESETTABLE_PED)
				DELETE_LOCAL_SCRIPT_PED(Data[iPed])
				PRINTLN("[AM_MP_PEDS] DELETE_LOCAL_RESETTABLE_SCRIPT_PEDS - Local ped: ", iPed, " has been deleted")
			ENDIF
		ENDREPEAT
	ENDIF
	
	PRINTLN("[AM_MP_PEDS] DELETE_LOCAL_RESETTABLE_SCRIPT_PEDS - All local peds have been deleted")
	RETURN TRUE
ENDFUNC

PROC INITIALISE_PED_PROCESS_ARRAY(SCRIPT_PED_DATA &LocalData)
	LocalData.iHighPriorityPeds = 0
	
	// iPedsProcessArray array is MAX_NUM_TOTAL_LOCAL_PEDS in length
	// in case the global g_iMaxPedsProcessedPerFrame increases.
	INT iSlot
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iSlot
		LocalData.iPedsProcessArray[iSlot] = -1
	ENDREPEAT
	
	// High Priority peds are those that need to be call each frame
	// Add high priority peds to LocalData.iPedsProcessArray srray
	// Store the total number of high priority peds in LocalData.iPedsProcessArray
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iSlot
		IF (LocalData.iHighPriorityPeds < MAX_NUM_HIGH_PRIORITY_PEDS)
			IF IS_PEDS_BIT_SET(LocalData.PedData[iSlot].iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
				LocalData.iPedsProcessArray[LocalData.iHighPriorityPeds] = iSlot
				LocalData.iHighPriorityPeds++
				PRINTLN("[AM_MP_PEDS] INITIALISE_PED_PROCESS_ARRAY - LocalData.iHighPriorityPeds: ", LocalData.iHighPriorityPeds)
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ FADING ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC SET_PED_FADE_TIMER(PEDS_DATA &Data)
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SET_FADE_TIMER)
		Data.tdPedFadeTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), MAX_NUM_PED_FADE_TIME_MS)
		SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SET_FADE_TIMER)
	ENDIF
ENDPROC

PROC SET_LOCAL_PED_PROPS_ALPHA(PEDS_DATA &Data, INT iAlpha)
	INT iProp
	REPEAT MAX_NUM_PED_PROPS iProp
		IF DOES_ENTITY_EXIST(Data.PropID[iProp])
			SET_ENTITY_ALPHA(Data.PropID[iProp], iAlpha, FALSE)
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
ENDPROC

PROC UPDATE_LOCAL_PED_FADE(PEDS_DATA &Data, BOOL bFadeIn)
	INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Data.tdPedFadeTimer))
	INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / MAX_NUM_PED_FADE_TIME_MS) * 255)
	
	IF bFadeIn
		iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / MAX_NUM_PED_FADE_TIME_MS) * 255)))
	ENDIF
	
	SET_ENTITY_ALPHA(Data.PedID, iAlpha, FALSE)
	SET_LOCAL_PED_PROPS_ALPHA(Data, iAlpha)
ENDPROC

PROC FINALISE_LOCAL_PED_FADE(PEDS_DATA &Data, BOOL bFadeIn)
	IF bFadeIn
		SET_ENTITY_ALPHA(Data.PedID, 255, FALSE)
		SET_LOCAL_PED_PROPS_ALPHA(Data, 255)
	ELSE
		SET_ENTITY_ALPHA(Data.PedID, 0, FALSE)
		SET_LOCAL_PED_PROPS_ALPHA(Data, 0)
	ENDIF
ENDPROC

FUNC BOOL PERFORM_LOCAL_PED_FADE(PEDS_DATA &Data, BOOL bFadeIn)
	SET_PED_FADE_TIMER(Data)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), Data.tdPedFadeTimer)
		UPDATE_LOCAL_PED_FADE(Data, bFadeIn)
	ELSE
		FINALISE_LOCAL_PED_FADE(Data, bFadeIn)
		CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_SET_FADE_TIMER)
		CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_FADE_PED)
		CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_FADE_IN_PED)
		SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FADE_PED_COMPLETE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_PED_FADE_EVENTS(SCRIPT_PED_DATA &LocalData, SCRIPTED_EVENT_TYPES eEventType, INT iEventID)
	
	SWITCH eEventType
		CASE SCRIPT_EVENT_SET_PED_FADE_OUT
			SCRIPT_EVENT_DATA_PED_FADE_OUT fadeOutEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, fadeOutEvent, SIZE_OF(fadeOutEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(fadeOutEvent.Details.FromPlayerIndex)
				
					IF (fadeOutEvent.iPed = -1)
						PRINTLN("[AM_MP_PEDS] PROCESS_PED_FADE_EVENTS - Fade out ped ID invalid")
						EXIT
					ENDIF
					
					IF IS_ENTITY_ALIVE(LocalData.PedData[fadeOutEvent.iPed].PedID)
						SET_PEDS_BIT(LocalData.PedData[fadeOutEvent.iPed].iBS, BS_PED_DATA_FADE_PED)
						PRINTLN("[AM_MP_PEDS] PROCESS_PED_FADE_EVENTS - Setting ped: ", fadeOutEvent.iPed, " to fade out")
					ELSE
						PRINTLN("[AM_MP_PEDS] PROCESS_PED_FADE_EVENTS - Fade out ped: ", fadeOutEvent.iPed, " does not exist")
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPT_EVENT_SET_PED_FADE_OUT_FOR_OTHER_PLAYERS
			SCRIPT_EVENT_DATA_PED_FADE_OUT fadeOutEvent2
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, fadeOutEvent2, SIZE_OF(fadeOutEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(fadeOutEvent.Details.FromPlayerIndex)
				
					IF (fadeOutEvent2.iPed = -1)
						PRINTLN("[AM_MP_PEDS] PROCESS_PED_FADE_EVENTS_FOR_OTHER_PLAYERS - Fade out ped ID invalid")
						EXIT
					ENDIF
					
					IF (fadeOutEvent2.Details.FromPlayerIndex = PLAYER_ID())
						PRINTLN("[AM_MP_PEDS] PROCESS_PED_FADE_EVENTS_FOR_OTHER_PLAYERS - Not processing for sender")
						EXIT
					ENDIF
					
					IF IS_ENTITY_ALIVE(LocalData.PedData[fadeOutEvent2.iPed].PedID)
						SET_PEDS_BIT(LocalData.PedData[fadeOutEvent2.iPed].iBS, BS_PED_DATA_FADE_PED)
						PRINTLN("[AM_MP_PEDS] PROCESS_PED_FADE_EVENTS_FOR_OTHER_PLAYERS - Setting ped: ", fadeOutEvent2.iPed, " to fade out")
					ELSE
						PRINTLN("[AM_MP_PEDS] PROCESS_PED_FADE_EVENTS_FOR_OTHER_PLAYERS - Fade out ped: ", fadeOutEvent2.iPed, " does not exist")
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_LAUNCH_CUSTOM_PED_UPDATE_ACTION_EVENTS(SCRIPT_PED_DATA &LocalData, SCRIPTED_EVENT_TYPES eEventType, INT iEventID)
	
	SWITCH eEventType
		CASE SCRIPT_EVENT_LAUNCH_CUSTOM_PED_UPDATE_ACTION
			SCRIPT_EVENT_DATA_PED_CUSTOM_UPDATE_ACTION customUpdateActionEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, customUpdateActionEvent, SIZE_OF(customUpdateActionEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(customUpdateActionEvent.Details.FromPlayerIndex)
				
					IF (customUpdateActionEvent.iPed = -1)
						PRINTLN("[AM_MP_PEDS] PROCESS_LAUNCH_CUSTOM_PED_UPDATE_ACTION_EVENTS - Fade out ped ID invalid")
						EXIT
					ENDIF
					
					//IF IS_ENTITY_ALIVE(LocalData.PedData[customUpdateActionEvent.iPed].PedID)
						SET_PEDS_BIT(LocalData.PedData[customUpdateActionEvent.iPed].iBS, BS_PED_DATA_LAUNCH_CUSTOM_TASK)
						PRINTLN("[AM_MP_PEDS] PROCESS_LAUNCH_CUSTOM_PED_UPDATE_ACTION_EVENTS - Setting ped: ", customUpdateActionEvent.iPed, " to launch custom action")
					//ELSE
					//	PRINTLN("[AM_MP_PEDS] PROCESS_LAUNCH_CUSTOM_PED_UPDATE_ACTION_EVENTS - Custom action Ped: ", customUpdateActionEvent.iPed, " does not exist")
					//ENDIF
					
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC
#ENDIF	// FEATURE_HEIST_ISLAND
