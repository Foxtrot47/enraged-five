//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_race_to_point.sch																		//
// Description: Controls the Race to Point functionality. 													//
// Written by:  Ryan Baker																					//
// Date: 23/08/2012																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
//USING "building_control_public.sch"
USING "menu_public.sch"
USING "shop_public.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
//USING "net_cop_dispatch.sch"
USING "net_comms_public.sch"
USING "net_mission_joblist.sch"

// CnC Headers
USING "net_scoring_common.sch"
USING "net_mission.sch"
USING "shared_hud_displays.sch"
USING "fm_unlocks_header.sch"
USING "freemode_events_header_private.sch"

CONST_INT 	R2P_INVITE_RESPONSE_TIME	15000
CONST_FLOAT R2P_START_RADIUS 			350.0	//12.5
CONST_INT 	R2P_INVITE_RADIUS			300		//10
CONST_INT 	R2P_MIN_WAGER				50
//CONST_INT 	R2P_CASH_POT_MAX			20000	//REPALCED BY g_sMPTunables.iImpromptuRaceCashRewardCap

//PURPOSE: Returns TRUE if the Race to Point has been disabled by a call to 
FUNC BOOL IS_RACE_TO_POINT_DISABLED()
	RETURN MPGlobalsAmbience.R2Pdata.bDisabled
ENDFUNC

//PURPOSE: Disables the Race to Point
PROC DISABLE_RACE_TO_POINT()
	IF MPGlobalsAmbience.R2Pdata.bDisabled = FALSE
		MPGlobalsAmbience.R2Pdata.bDisabled = TRUE
	ENDIF
ENDPROC

//PURPOSE: Enables the Race to Point
PROC ENABLE_RACE_TO_POINT()
	IF MPGlobalsAmbience.R2Pdata.bDisabled = TRUE
		MPGlobalsAmbience.R2Pdata.bDisabled = FALSE
	ENDIF
ENDPROC

//PURPOSE: Checks to see if the player should end the race to point
FUNC BOOL CAN_DO_RACE_TO_POINT(BOOL bDoUnlockedCheck = TRUE, BOOL bDoAlreadyRunningCheck = TRUE, BOOL bDoOnMissionCheck = TRUE)
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		RETURN FALSE	
	ENDIF
	#ENDIF
	
	IF IS_RACE_TO_POINT_DISABLED()
		//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - DISABLED      <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF bDoUnlockedCheck = TRUE
		IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_RACE_TO_POINT)
			//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - NOT UNLOCKED      <----------     ") NET_NL()
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - NETWORK_IS_IN_TUTORIAL_SESSION      <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_DARTS
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_ARM_WRESTLING
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GANGHIDEOUT
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_IMPROMPTU_DM
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GB_BOSS_DEATHMATCH
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_FIXER_SHORT_TRIP
		//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - MINIGAME      <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - IN CINEMA      <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
		//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - ON TIME TRIAL      <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
		//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - ON RC TIME TRIAL      <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_PENNED_IN
		//-- Actively taking part in Penned In
		RETURN FALSE
	ENDIF
	
	IF LOCAL_PLAYER_IS_ON_AGGRESSVE_FM_EVENT()
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID(), TRUE)
		RETURN FALSE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_IN_KILL_LIST_VEHICLE()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
	OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
	OR IS_PLAYER_IN_CLUBHOUSE(PLAYER_ID())
	OR IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR_OF_TYPE(SIMPLE_INTERIOR_TYPE_BUNKER)
	OR IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR_OF_TYPE(SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK)
	OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	OR IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
	OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
		//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - IN PROPERTY      <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE
		//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - IN CORONA     <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF bDoOnMissionCheck = TRUE
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_MISSION	//ENABLE FOR NORMAL MISSIONS - 1594775 
		AND IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), TRUE)
			//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - ON MISSION      <----------     ") NET_NL()
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
	OR Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
		//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - ON LTS OR CTF OR VS      <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - BROWSING SHOP      <----------     ") NET_NL()
		RETURN FALSE
	ENDIF
	
	IF bDoAlreadyRunningCheck = TRUE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("MG_RACE_TO_POINT")) > 0
			//NET_PRINT_TIME() NET_PRINT("     ---------->     RACE TO POINT - CAN_DO_RACE_TO_POINT - ALREADY RUNNING      <----------     ") NET_NL()
			RETURN FALSE
		ENDIF
	ENDIF
	
	// checks if player is in an adversary mode BUG 3317627 KW 21 Jan 2017
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Checks to see if the player should end the race to point
//FUNC BOOL SAFE_TO_LAUNCH_RACE_TO_POINT()
//	IF IS_RACE_TO_POINT_DISABLED()
//		NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - SAFE_TO_LAUNCH_RACE_TO_POINT - IS_RACE_TO_POINT_DISABLED = TRUE") NET_NL()
//		RETURN FALSE
//	ENDIF
//	
//	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("MG_RACE_TO_POINT")) > 0
//		NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - SAFE_TO_LAUNCH_RACE_TO_POINT - ALREADY RUNNING") NET_NL()
//		RETURN FALSE
//	ENDIF
//	
//	//IF NOT IS_PED_STOPPED(PLAYER_PED_ID())
//	IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 5.0
//		NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - SAFE_TO_LAUNCH_RACE_TO_POINT - PLAYER NOT STATIONARY") NET_NL()
//		RETURN FALSE
//	ENDIF
//	
//	IF g_b_On_Deathmatch = TRUE
//		NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - SAFE_TO_LAUNCH_RACE_TO_POINT - ON DEATHMATCH") NET_NL()
//		RETURN FALSE
//	ENDIF
//	
//	IF g_b_On_Race = TRUE
//		NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - SAFE_TO_LAUNCH_RACE_TO_POINT - ON RACE") NET_NL()
//		RETURN FALSE
//	ENDIF
//	
//	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
//		NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - SAFE_TO_LAUNCH_RACE_TO_POINT - BROWSING SHOP") NET_NL()
//		RETURN FALSE
//	ENDIF
//	
//	//IF IS_CUSTOM_MENU_ON_SCREEN()
//	//	NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - SAFE_TO_LAUNCH_RACE_TO_POINT - CUSTOM MENU ON SCREEN") NET_NL()
//	//	RETURN FALSE
//	//ENDIF
//	
//	RETURN TRUE
//ENDFUNC

//PURPOSE: Clears the player triggered with variable
/*PROC R2P_CLEAR_PLAYER_TRIGGERED_WITH()
	MPGlobalsAmbience.R2Pdata.PlayerTriggeredWith = INT_TO_NATIVE(PLAYER_INDEX, -1)
	NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - R2P_CLEAR_PLAYER_TRIGGERED_WITH  <-----     ") NET_NL()
ENDPROC*/

//PURPOSE: Clears the player invited from pause menu variable
/*PROC R2P_CLEAR_PLAYER_INVITED_FROM_PAUSE_MENU()
	MPGlobalsAmbience.R2Pdata.iInvitedFromPlayerListBS = 0
	//NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - R2P_CLEAR_PLAYER_INVITED_FROM_PAUSE_MENU  <-----     ") NET_NL()
ENDPROC*/
		
//PURPOSE: Returns the next free Race to Point instance
FUNC INT GET_FREE_RACE_TO_POINT_INSTANCE()
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		//NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - GET_FREE_RACE_TO_POINT_INSTANCE - CHECK    i = ") NET_PRINT_INT(i) NET_NL()
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("MG_RACE_TO_POINT", i)
			NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - GET_FREE_RACE_TO_POINT_INSTANCE - FOUND    i = ") NET_PRINT_INT(i) NET_NL()
			RETURN i
		ENDIF
	ENDREPEAT
	
	NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - GET_FREE_RACE_TO_POINT_INSTANCE - FAILED    i = ") NET_PRINT_INT(i) NET_NL()
	NET_SCRIPT_ASSERT("GET_FREE_RACE_TO_POINT_INSTANCE - Couldn't find free instance")
	RETURN -1
ENDFUNC

//**TWH - CMcM - 1425706 - Added new function to calculate player pot contribution for Impromptu Races.
FUNC INT CALCULATE_PLAYER_POT_CONTRIBUTION(VECTOR vStart, VECTOR vDestination)
	INT iMinimumBet = R2P_MIN_WAGER, iBetIncrement = 25, iRankMultiplier = 4
	FLOAT fDistanceMultiplier = 0.25
	// a. Get rank and limit from 10 to 40, then multiply
	INT iRank = GET_PLAYER_RANK(PLAYER_ID())
	
	IF iRank < 10
		iRank = 10
	ELIF iRank > 40	//LOWERED FROM 50 FOR BUG 1669472
		iRank = 40
	ENDIF
	NET_PRINT_TIME() NET_PRINT("Impromptu Race - iRank = ") NET_PRINT_INT(iRank) NET_NL()
	iRank *= iRankMultiplier
	NET_PRINT_TIME() NET_PRINT("Impromptu Race - (iRank *= 4) = ") NET_PRINT_INT(iRank) NET_NL()
	
	// b. Convert distance into km, if less than 1 make it 1.
	FLOAT fDistance = VDIST(vDestination, vStart)
	
	NET_PRINT_TIME() NET_PRINT("Impromptu Race - fDistance  = ") NET_PRINT_FLOAT(fDistance) NET_NL()
	
	IF fDistance > 1000
		fDistance/=1000
	ENDIF
	fDistance *= fDistanceMultiplier
	
	NET_PRINT_TIME() NET_PRINT("Impromptu Race - fDistance rounded = ") NET_PRINT_FLOAT(fDistance) NET_NL()
	
	// Multiply a. by b.
	INT iReturn = ROUND(iRank*fDistance)
	
	NET_PRINT_TIME() NET_PRINT("Impromptu Race - iReturn a x b = ") NET_PRINT_INT(iReturn) NET_NL()
	
	// Round to nearest 50
	INT iIncrements = ROUND(TO_FLOAT(iReturn)/TO_FLOAT(iBetIncrement))
	iReturn = iIncrements * iBetIncrement
	
	NET_PRINT_TIME() NET_PRINT("Impromptu Race - iReturn in iIncrements = ") NET_PRINT_INT(iReturn) NET_NL()
	
	iReturn = ROUND(TO_FLOAT(iReturn)*g_sMPTunables.fImpromptuRaceExpensesMultiplier)
	NET_PRINT_TIME() NET_PRINT("Impromptu Race - iReturn after Tunable Multiplier = ") NET_PRINT_INT(iReturn) NET_NL()
	
	IF iReturn > g_sMPTunables.iImpromptuRaceEntryFeeCap
		iReturn = g_sMPTunables.iImpromptuRaceEntryFeeCap
		NET_PRINT_TIME() NET_PRINT("Impromptu Race - iReturn capped to = ") NET_PRINT_INT(iReturn) NET_NL()
	ENDIF
	
	// Use a minimum of $50*Tunable
	IF iReturn >= ROUND(iMinimumBet*g_sMPTunables.fImpromptuRaceExpensesMultiplier)
		RETURN iReturn
	ENDIF
	
	RETURN ROUND(iMinimumBet*g_sMPTunables.fImpromptuRaceExpensesMultiplier)
ENDFUNC

//PURPOSE: Controls the player choosing to launch a Race to Point
PROC PROCESS_START_R2P()
	/*#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
		MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint = TRUE
		NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_START_R2P - RACE TO POINT TRIGGERED WITH 9-KEY - START LAUNCH - iLaunchStage = 0  <-----     ") NET_NL()
	ENDIF
	#ENDIF*/
		
	IF MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint = TRUE
		IF MPGlobals.PlayerInteractionData.iLaunchStage = -1	//GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_PI_MENU")) = 0
			IF CAN_DO_RACE_TO_POINT()	//SAFE_TO_LAUNCH_RACE_TO_POINT()
				INT iFreeInstance = GET_FREE_RACE_TO_POINT_INSTANCE()
				IF iFreeInstance > -1
				AND iFreeInstance < NUM_NETWORK_PLAYERS
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
					MPGlobalsAmbience.R2Pdata.iLaunchStage = 0
					MPGlobalsAmbience.R2Pdata.missionData.iInstanceId = iFreeInstance	//PARTICIPANT_ID_TO_INT()
					NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_START_R2P - RACE TO POINT TRIGGERED - START LAUNCH - iLaunchStage = 0  iInstanceId = ") NET_PRINT_INT(MPGlobalsAmbience.R2Pdata.missionData.iInstanceId) NET_NL()
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_START_R2P - RACE TO POINT TRIGGERED - START LAUNCH FAILED - INVALID INSTANCE  iInstanceId = ") NET_PRINT_INT(MPGlobalsAmbience.R2Pdata.missionData.iInstanceId) NET_NL()
				ENDIF
			ELSE
				//R2P_CLEAR_PLAYER_TRIGGERED_WITH()
				//R2P_CLEAR_PLAYER_INVITED_FROM_PAUSE_MENU()
				NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_START_R2P - NOT SAFE TO LAUNCH  <-----     ") NET_NL()
			ENDIF
			MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint = FALSE
			NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_START_R2P - MPGlobalsAmbience.R2Pdata.bLaunchRaceToPoint = FALSE  <-----     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls receiveing and dealing with an invite from a Race to Point
PROC PROCESS_R2P_INVITE()
			
	//Check if Race to Point has been launched
	IF MPGlobalsAmbience.R2Pdata.iInviteStage != -1
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("MG_RACE_TO_POINT")) > 0
		OR MPGlobalsAmbience.R2Pdata.iLaunchStage != -1
			MPGlobalsAmbience.R2Pdata.iInviteStage = -1
			MPGlobalsAmbience.R2Pdata.missionData.iInstanceId = -1
			MPGlobalsAmbience.R2Pdata.iInvitedToInstance = -1
			MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet = 0
			NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_R2P_INVITE - SCRIPT LAUNCHED OR ABOUT TO - RESET  <-----     ") NET_NL()
			EXIT
		ENDIF
	ENDIF
	
	//Control Accept Invite Stages
	SWITCH MPGlobalsAmbience.R2Pdata.iInviteStage
		
		//Control having received an invite
		CASE -1
			IF MPGlobalsAmbience.R2Pdata.iInvitedToInstance != -1
			AND MPGlobalsAmbience.R2Pdata.iLaunchStage = -1
				IF CAN_DO_RACE_TO_POINT(FALSE)
// KEITH 20/6/13: Switch to Joblist Invites					
//						Delete_Text_Messages_With_This_Label("R2P_INV_TXT")
					MPGlobalsAmbience.R2Pdata.iInviteStage = 0
					MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet = 0
					NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_R2P_INVITE - INVITE RECEIVED - iInviteStage = 0  <-----     ") NET_NL()
				ELSE
					MPGlobalsAmbience.R2Pdata.iInvitedToInstance = -1
					MPGlobalsAmbience.R2Pdata.iInviteStage = -1
					MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet = 0
					RESET_NET_TIMER(MPGlobalsAmbience.R2Pdata.iInviteTimer)
					NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_R2P_INVITE - INVITE RECEIVED - CAN'T DO RACE TO POINT - iInviteStage = -1  <-----     ") NET_NL()
				ENDIF
			ENDIF
		BREAK
		
		//Control having received an invite
		CASE 0
			INT iTempBitSet
			SET_BIT(iTempBitSet, MP_COMMS_MODIFIER_IGNORE_RECENT_INVITE) // KGM 17/6/13: doesn't get delayed by recent invite
			IF IS_NET_PLAYER_OK(MPGlobalsAmbience.R2Pdata.HostPlayer)
				NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_R2P_INVITE - TEXT MESSAGE INVITE FROM ") NET_PRINT(GET_PLAYER_NAME(MPGlobalsAmbience.R2Pdata.HostPlayer)) NET_NL()
// KEITH 20/6/13: Switch to Joblist Invites					
//					IF Request_MP_Comms_Txtmsg_With_Components_From_Player(MPGlobalsAmbience.R2Pdata.HostPlayer, "R2P_INV_TXT", GET_PLAYER_NAME(MPGlobalsAmbience.R2Pdata.HostPlayer), TRUE, NO_INT_SUBSTRING_COMPONENT_VALUE, "", "", TRUE)//, iTempBitSet)	Request_MP_Comms_Txtmsg_From_Player
				
				
				INT iDistance
				INT iCost
				iDistance = ROUND(GET_DISTANCE_BETWEEN_COORDS(MPGlobalsAmbience.R2Pdata.vInvitedStartPos, MPGlobalsAmbience.R2Pdata.vInvitedDestination))
				iCost = CALCULATE_PLAYER_POT_CONTRIBUTION(MPGlobalsAmbience.R2Pdata.vInvitedStartPos, MPGlobalsAmbience.R2Pdata.vInvitedDestination)
				Store_Basic_Invite_Details_For_Joblist_From_Player_With_Params(MPGlobalsAmbience.R2Pdata.HostPlayer, FMMC_TYPE_RACE_TO_POINT, "", MPGlobalsAmbience.R2Pdata.tlInvitedDestination, TRUE, iDistance, iCost)
				
				
				MPGlobalsAmbience.R2Pdata.iInviteStage = 1
				NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_R2P_INVITE - TEXT MESSAGE RECEIVED - iInviteStage = 1  <-----     ") NET_NL()
//					ENDIF
			ELSE
// KEITH 20/6/13: Switch to Joblist Invites					
//					Kill_This_MP_Txtmsg_Comms("R2P_INV_TXT")
				Cancel_Basic_Invite_From_Player(MPGlobalsAmbience.R2Pdata.HostPlayer, FMMC_TYPE_RACE_TO_POINT)
				IF MPGlobalsAmbience.R2Pdata.iInvitedToInstance != -1
					CLEAR_BIT(MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet, MPGlobalsAmbience.R2Pdata.iInvitedToInstance)
				ENDIF
				MPGlobalsAmbience.R2Pdata.iInvitedToInstance = -1
				MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet = 0
				MPGlobalsAmbience.R2Pdata.iInviteStage = -1
				NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_R2P_INVITE - INVITE SENDER NOT OKAY - A  <-----     ") NET_NL()
//					HANG_UP_AND_PUT_AWAY_PHONE()
				RESET_NET_TIMER(MPGlobalsAmbience.R2Pdata.iInviteTimer)
			ENDIF
		BREAK
		
		//Wait for Response
		CASE 1
			//Check for Timeout
			IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.R2Pdata.iInviteTimer, R2P_INVITE_RESPONSE_TIME)
// KEITH 20/6/13: Switch to Joblist Invites					
//					Kill_This_MP_Txtmsg_Comms("R2P_INV_TXT")
				Cancel_Basic_Invite_From_Player(MPGlobalsAmbience.R2Pdata.HostPlayer, FMMC_TYPE_RACE_TO_POINT)
				IF MPGlobalsAmbience.R2Pdata.iInvitedToInstance != -1
					CLEAR_BIT(MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet, MPGlobalsAmbience.R2Pdata.iInvitedToInstance)
				ENDIF
				MPGlobalsAmbience.R2Pdata.iInvitedToInstance = -1
				MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet = 0
				MPGlobalsAmbience.R2Pdata.iInviteStage = -1
				NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_R2P_INVITE - CLEANUP - TIME EXPIRED  <-----     ") NET_NL()
//					HANG_UP_AND_PUT_AWAY_PHONE()
				RESET_NET_TIMER(MPGlobalsAmbience.R2Pdata.iInviteTimer)
			ENDIF
			//Check Out of Range
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), MPGlobalsAmbience.R2Pdata.vInvitedStartPos, <<R2P_START_RADIUS, R2P_START_RADIUS, R2P_START_RADIUS>>)
// KEITH 20/6/13: Switch to Joblist Invites					
//					Kill_This_MP_Txtmsg_Comms("R2P_INV_TXT")
				Cancel_Basic_Invite_From_Player(MPGlobalsAmbience.R2Pdata.HostPlayer, FMMC_TYPE_RACE_TO_POINT)
				IF MPGlobalsAmbience.R2Pdata.iInvitedToInstance != -1
					CLEAR_BIT(MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet, MPGlobalsAmbience.R2Pdata.iInvitedToInstance)
				ENDIF
				MPGlobalsAmbience.R2Pdata.iInvitedToInstance = -1
				MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet = 0
				MPGlobalsAmbience.R2Pdata.iInviteStage = -1
				NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_R2P_INVITE - CLEANUP - PLAYER NOT IN START RADIUS  <-----     ") NET_NL()
//					HANG_UP_AND_PUT_AWAY_PHONE()
				RESET_NET_TIMER(MPGlobalsAmbience.R2Pdata.iInviteTimer)
			ENDIF
			//Race to Point Invite Cancelled
			IF MPGlobalsAmbience.R2Pdata.iInvitedToInstance != -1
				IF IS_BIT_SET(MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet, MPGlobalsAmbience.R2Pdata.iInvitedToInstance)
// KEITH 20/6/13: Switch to Joblist Invites					
//						Kill_This_MP_Txtmsg_Comms("R2P_INV_TXT")
					Cancel_Basic_Invite_From_Player(MPGlobalsAmbience.R2Pdata.HostPlayer, FMMC_TYPE_RACE_TO_POINT)
					IF MPGlobalsAmbience.R2Pdata.iInvitedToInstance != -1
						CLEAR_BIT(MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet, MPGlobalsAmbience.R2Pdata.iInvitedToInstance)
					ENDIF
					MPGlobalsAmbience.R2Pdata.iInvitedToInstance = -1
					MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet = 0
					MPGlobalsAmbience.R2Pdata.iInviteStage = -1
					NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_R2P_INVITE - CLEANUP - CANCEL INVITE BIT SET  <-----     ") NET_NL()
//						HANG_UP_AND_PUT_AWAY_PHONE()
					RESET_NET_TIMER(MPGlobalsAmbience.R2Pdata.iInviteTimer)
				ENDIF
			ENDIF
			//Sender Not Okay
			IF NOT IS_NET_PLAYER_OK(MPGlobalsAmbience.R2Pdata.HostPlayer)
// KEITH 20/6/13: Switch to Joblist Invites					
//					Kill_This_MP_Txtmsg_Comms("R2P_INV_TXT")
				Cancel_Basic_Invite_From_Player(MPGlobalsAmbience.R2Pdata.HostPlayer, FMMC_TYPE_RACE_TO_POINT)
				IF MPGlobalsAmbience.R2Pdata.iInvitedToInstance != -1
					CLEAR_BIT(MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet, MPGlobalsAmbience.R2Pdata.iInvitedToInstance)
				ENDIF
				MPGlobalsAmbience.R2Pdata.iInvitedToInstance = -1
				MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet = 0
				MPGlobalsAmbience.R2Pdata.iInviteStage = -1
				NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_R2P_INVITE - INVITE SENDER NOT OKAY - B  <-----     ") NET_NL()
//					HANG_UP_AND_PUT_AWAY_PHONE()
				RESET_NET_TIMER(MPGlobalsAmbience.R2Pdata.iInviteTimer)
			ENDIF
			
			//Check for Response
// KEITH 20/6/13: Switch to Joblist Invites					
//				IF NOT Is_MP_Comms_Still_Playing()
//					IF Did_Player_Reply_Yes_To_MP_Comms()
				IF (Has_Player_Accepted_Basic_Invite_From_Player(MPGlobalsAmbience.R2Pdata.HostPlayer, FMMC_TYPE_RACE_TO_POINT))
					//SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
					//TickerData.TickerEvent = TICKER_EVENT_ACCEPTED_RACE_TO_POINT
					//BROADCAST_TICKER_EVENT(TickerData, SPECIFIC_PLAYER(MPGlobalsAmbience.R2Pdata.HostPlayer))
					
//						Kill_This_MP_Txtmsg_Comms("R2P_INV_TXT")
					MPGlobalsAmbience.R2Pdata.iLaunchStage = 0
					MPGlobalsAmbience.R2Pdata.missionData.iInstanceId = MPGlobalsAmbience.R2Pdata.iInvitedToInstance
					NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_START_R2P - INVITE ACCEPTED - START LAUNCH - iInvitedToInstance = ") NET_PRINT_INT(MPGlobalsAmbience.R2Pdata.iInvitedToInstance) NET_NL()
					MPGlobalsAmbience.R2Pdata.iInviteStage = -1
					NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_START_R2P - INVITE ACCEPTED - START LAUNCH - iInviteStage = -1  <-----     ") NET_NL()
					NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_START_R2P - INVITE ACCEPTED - START LAUNCH - iLaunchStage = 0  <-----     ") NET_NL()
// KEITH 20/6/13: Switch to Joblist Invites - there won't be a 'no' reply, just a timeout
//					ELSE	//IF Did_Player_Reply_No_To_MP_Comms()
//						Kill_This_MP_Txtmsg_Comms("R2P_INV_TXT")
//						MPGlobalsAmbience.R2Pdata.iInvitedToInstance = -1
//						MPGlobalsAmbience.R2Pdata.iCancelInviteBitSet = 0
//						MPGlobalsAmbience.R2Pdata.iInviteStage = -1
//						NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_START_R2P - INVITE DECLINED - iInviteStage = -1  <-----     ") NET_NL()
				ENDIF
				RESET_NET_TIMER(MPGlobalsAmbience.R2Pdata.iInviteTimer)
//				ENDIF
		BREAK
				
	ENDSWITCH
ENDPROC

//PURPOSE: Launches the Race to Point script
PROC PROCESS_LAUNCH_R2P()
	SWITCH MPGlobalsAmbience.R2Pdata.iLaunchStage
		//Launch Race to Point
		CASE 0
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
			MPGlobalsAmbience.R2Pdata.missionData.mdID.idMission = eMG_RACE_TO_POINT	//eNULL_MISSION
			PRINTLN(" --> Race to Point will launch with instance ID ", MPGlobalsAmbience.R2Pdata.missionData.iInstanceId)
		   	IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(MPGlobalsAmbience.R2Pdata.missionData) 
				//BROADCAST_JOIN_THIS_AMBIENT_MP_MISSION_EVENT(MPGlobalsAmbience.R2Pdata.missionData, SPECIFIC_PLAYER(PLAYER_ID()))
				MPGlobalsAmbience.R2Pdata.iLaunchStage = 1
				NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_LAUNCH_R2P - LAUNCHED - iLaunchStage = 1  <-----     ") NET_NL()
			ENDIF
		BREAK
		
		//Check if Race to Point has Started
		CASE 1
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("MG_RACE_TO_POINT")) > 0
				MPGlobalsAmbience.R2Pdata.iLaunchStage = -1
				MPGlobalsAmbience.R2Pdata.missionData.iInstanceId = -1
				MPGlobalsAmbience.R2Pdata.iInvitedToInstance = -1
				NET_PRINT_TIME() NET_PRINT("     ----->   RACE TO POINT - PROCESS_LAUNCH_R2P - STARTED - iLaunchStage = -1  <-----     ") NET_NL()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Launches the Race to Point script
PROC MAINTAIN_STARTING_RACE_TO_POINT()
	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		EXIT
	ENDIF
	PROCESS_START_R2P()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		PROCESS_R2P_INVITE()
		PROCESS_LAUNCH_R2P()
	ENDIF
ENDPROC
