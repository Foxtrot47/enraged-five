USING "globals.sch"
USING "net_blips.sch"
USING "net_include.sch"
USING "net_ambience.sch"
USING "net_vehicle_drop.sch"
USING "net_spawn_vehicle.sch"

#IF FEATURE_HEIST_ISLAND

CONST_INT NUM_MOON_POOL_VEHICLES			2

CONST_INT MOON_POOL_AVISA					1
CONST_INT MOON_POOL_SEASPARROW2				0

CONST_INT MOON_POOL_BS_VEHICLE_CREATED		0
CONST_INT MOON_POOL_BS_COORDS_FOUND			1
CONST_INT MOON_POOL_BS_BLIP_CREATED			2
CONST_INT MOON_POOL_BS_COORDS_INIT			3
CONST_INT MOON_POOL_BS_RESET_LOCKSTATE		4

STRUCT MOON_POOL_VEHICLE_DATA
	BLIP_INDEX blipMoonPoolVehicles[NUM_MOON_POOL_VEHICLES]
	
	INT iBS[NUM_MOON_POOL_VEHICLES]
	INT iNode[NUM_MOON_POOL_VEHICLES]
	INT iSpawnNode[NUM_MOON_POOL_VEHICLES]
	INT iLockState = -1
	
	NETWORK_INDEX netMoonPoolVehicles[NUM_MOON_POOL_VEHICLES]
	
	SCRIPT_TIMER stMoonPoolLockstateTimer[NUM_MOON_POOL_VEHICLES]
	SCRIPT_TIMER stMoonPoolInvincibleTimer
	
	VECTOR vDropLocation[NUM_MOON_POOL_VEHICLES]
ENDSTRUCT

FUNC BOOL IS_VEHICLE_A_PLAYER_MOON_POOL_VEHICLE(VEHICLE_INDEX tempveh, BOOL DoDeadCheck = TRUE)
	IF g_bInMultiplayer
		IF DOES_ENTITY_EXIST(tempveh)
		AND (NOT DoDeadCheck OR IS_VEHICLE_DRIVEABLE(tempveh))
			IF DECOR_EXIST_ON(tempveh, "Player_Moon_Pool")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL AM_I_IN_SOMEONE_ELSES_MOON_POOL_VEHICLE()
	VEHICLE_INDEX VehicleID
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF IS_VEHICLE_A_PLAYER_MOON_POOL_VEHICLE(VehicleID)
		AND NOT IS_VEHICLE_MY_MOON_POOL_VEHICLE(VehicleID)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_MOON_POOL_VEHICLE_MODEL(INT iVehicleIndex)
	SWITCH iVehicleIndex
		CASE MOON_POOL_AVISA		RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("AVISA"))
		CASE MOON_POOL_SEASPARROW2	RETURN SEASPARROW2
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC SET_MOON_POOL_VEHICLE_OUTSIDE(INT iVehicleIndex, BOOL bSet)
	SWITCH iVehicleIndex
		CASE MOON_POOL_AVISA
			SET_PLAYER_AVISA_OUTSIDE(bSet)
			
			IF bSet
				SET_PLAYER_SEASPARROW2_OUTSIDE(FALSE)
			ENDIF
		BREAK
		
		CASE MOON_POOL_SEASPARROW2
			SET_PLAYER_SEASPARROW2_OUTSIDE(bSet)
			
			IF bSet
				SET_PLAYER_AVISA_OUTSIDE(FALSE)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL CREATE_MOON_POOL_VEHICLE(MOON_POOL_VEHICLE_DATA &sMoonPoolVehicleData, INT iVehicleIndex)
	FLOAT fHeading
	
	VECTOR vCoords
	
	VEHICLE_SPAWN_LOCATION_PARAMS Params
	Params.fMinDistFromCoords = VD_VEHICLE_MIN_SPAWN_RANGE
	Params.fMaxDistance = VD_VEHICLE_MAX_SPAWN_RANGE
	Params.bIgnoreCustomNodesForArea = TRUE
	
	IF NOT IS_BIT_SET(sMoonPoolVehicleData.iBS[iVehicleIndex], MOON_POOL_BS_COORDS_INIT)
		sMoonPoolVehicleData.vDropLocation[iVehicleIndex] = GET_PLAYER_COORDS(PLAYER_ID())
		
		SET_BIT(sMoonPoolVehicleData.iBS[iVehicleIndex], MOON_POOL_BS_COORDS_INIT)
		
		IF iVehicleIndex = MOON_POOL_AVISA
			ADD_CUSTOM_NODES_FOR_ALL_SUBMARINE_DINGHY_SPAWNS()
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_FORT_ZANCUDO() OR IS_LOCAL_PLAYER_IN_LSIA()
		IF DOES_PLAYER_OWN_A_HANGER(PLAYER_ID())
		OR (GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID()) AND DOES_PLAYER_OWN_A_HANGER(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
			Params.bIgnoreCustomNodesForArea = FALSE
			Params.bAvoidSpawningInExclusionZones = FALSE
			Params.bUseExactCoordsIfPossible = TRUE
			Params.bStartOfMissionPVSpawn = TRUE
		ENDIF
	ENDIF
	
	IF iVehicleIndex = MOON_POOL_AVISA
		Params.fHigherZLimit = 9999.0
		Params.fMaxDistance = 9999.0
		Params.bIgnoreCustomNodesForArea = TRUE
		Params.bCheckEntityArea = TRUE
		Params.bCheckThisPlayerVehicle = FALSE
		Params.bBoatNodesOnly = TRUE
	ENDIF
	
	IF REQUEST_LOAD_MODEL(GET_MOON_POOL_VEHICLE_MODEL(iVehicleIndex))
		IF IS_BIT_SET(sMoonPoolVehicleData.iBS[iVehicleIndex], MOON_POOL_BS_COORDS_FOUND)
		OR HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(sMoonPoolVehicleData.vDropLocation[iVehicleIndex], <<0.0, 0.0, 0.0>>, SEASPARROW2, (iVehicleIndex = MOON_POOL_AVISA), vCoords, fHeading, Params)
			SET_BIT(sMoonPoolVehicleData.iBS[iVehicleIndex], MOON_POOL_BS_COORDS_FOUND)
			
			IF NOT IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP(sMoonPoolVehicleData.vDropLocation[iVehicleIndex], vCoords, 1000, 10, TRUE)
				CLEAR_BIT(sMoonPoolVehicleData.iBS[iVehicleIndex], MOON_POOL_BS_COORDS_FOUND)
				GET_CLOSE_VEHICLE_NODE(GET_PLAYER_COORDS(PLAYER_ID()), sMoonPoolVehicleData.vDropLocation[iVehicleIndex], sMoonPoolVehicleData.iNode[iVehicleIndex])
				NET_PRINT("[MAGNATE_GANG_BOSS][BOSS_LIMO] - GB_CREATE_BOSS_LIMO - OVER MAX VEHICLE_SPAWN_RANGE LIMIT") NET_PRINT_FLOAT(GB_VEHICLE_MAX_SPAWN_RANGE) NET_NL()
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(sMoonPoolVehicleData.iBS[iVehicleIndex], MOON_POOL_BS_COORDS_FOUND)
			CLEAR_AREA_OF_VEHICLES(vCoords, 5.0)
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex])
				IF CAN_REGISTER_MISSION_VEHICLES(1)
					CLEAR_AREA(vCoords, 2.0, TRUE)
					
					IF CREATE_NET_VEHICLE(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex], GET_MOON_POOL_VEHICLE_MODEL(iVehicleIndex), vCoords, fHeading, FALSE, TRUE, TRUE)
						IF iVehicleIndex = MOON_POOL_AVISA
							CLEAR_CUSTOM_VEHICLE_NODES()
						ENDIF
						
						SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex], PLAYER_ID(), TRUE)
						
						VEHICLE_INDEX tempVeh = NET_TO_VEH(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex])
						
						SET_LOCK_STATE_OF_VEHICLE(tempVeh, sMoonPoolVehicleData.iLockState, -1, PLAYER_ID())
						
						START_VEHICLE_AND_TURN_ON_RADIO(tempVeh, "RADIO_35_DLC_HEI4_MLR")
						
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
						APPLY_MODS_TO_SUB_HELI(tempVeh,GET_PACKED_STAT_INT(PACKED_MP_INT_SUB_HELI_W),GET_PACKED_STAT_INT(PACKED_MP_INT_SUB_HELI_C) #IF FEATURE_DLC_1_2022 , GET_PACKED_STAT_INT(PACKED_MP_INT_SUB_HELI_CM) #ENDIF )
						SET_VEHICLE_ON_GROUND_PROPERLY(tempVeh)
						
						DECOR_SET_INT(tempVeh, "Not_Allow_As_Saved_Veh", 1)
						
						IF DECOR_IS_REGISTERED_AS_TYPE("Player_Moon_Pool", DECOR_TYPE_INT)
							IF NOT DECOR_EXIST_ON(tempVeh, "Player_Moon_Pool")
								DECOR_SET_INT(tempVeh, "Player_Moon_Pool", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
							ENDIF
						ENDIF
						
						IF iVehicleIndex = MOON_POOL_AVISA
							SET_VEHICLE_COLOURS(tempVeh, 70, 0)
							SET_VEHICLE_EXTRA_COLOURS(tempVeh, 70, 0)
							SET_VEHICLE_EXTRA_COLOUR_5(tempVeh, 121)
						ENDIF
						
						IF iVehicleIndex = MOON_POOL_SEASPARROW2
							SET_ENTITY_INVINCIBLE(tempVeh, TRUE)
							
							REINIT_NET_TIMER(sMoonPoolVehicleData.stMoonPoolInvincibleTimer)
						ENDIF
						
						CLEAR_BIT(sMoonPoolVehicleData.iBS[iVehicleIndex], MOON_POOL_BS_COORDS_FOUND)
						
						SET_BIT(sMoonPoolVehicleData.iBS[iVehicleIndex], MOON_POOL_BS_VEHICLE_CREATED)
						
						SET_MOON_POOL_VEHICLE_OUTSIDE(iVehicleIndex, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex])
				RETURN FALSE
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_MOON_POOL_VEHICLE_MODEL(iVehicleIndex))
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_MOON_POOL_VEHICLE(MOON_POOL_VEHICLE_DATA &sMoonPoolVehicleData, INT iVehicleIndex, BOOL bDelete)
	IF NETWORK_DOES_NETWORK_ID_EXIST(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex])
		IF TAKE_CONTROL_OF_NET_ID(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex])
			
			PRINTLN("[MOON_POOL] CLEANUP_MOON_POOL_VEHICLE - cleaning up vehicle index ", iVehicleIndex)	
			
			IF NOT IS_VEHICLE_EMPTY(NET_TO_VEH(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex]), TRUE, TRUE)
				INT i
				PLAYER_INDEX tempPlayer
				
				REPEAT NUM_NETWORK_PLAYERS i
					tempPlayer = INT_TO_PLAYERINDEX(i)
					
					IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), NET_TO_VEH(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex]))
							PRINTLN("[MOON_POOL] CLEANUP_MOON_POOL_VEHICLE - Kicking player ", i, " out of vehicle")
							
							BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(tempPlayer), FALSE, 0, 0)
							
							SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(NET_TO_VEH(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex]), tempPlayer, TRUE)
						ENDIF
					ENDIF
				ENDREPEAT
				
				EXIT
			ENDIF
			
			IF DOES_BLIP_EXIST(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex])
				REMOVE_BLIP(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex])
			ENDIF		
			
			IF bDelete
				DELETE_NET_ID(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex])
			ELSE
				IF iVehicleIndex = MOON_POOL_SEASPARROW2
					REINIT_NET_TIMER(g_TransitionSessionNonResetVars.contactRequests.stCDTimer[ENUM_TO_INT(REQUEST_MOON_POOL)], TRUE)
					CONTACT_REQUEST_SET_CD_TIMER(REQUEST_MOON_POOL)
				ENDIF
				
				CLEANUP_NET_ID(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex])
			ENDIF
			
			sMoonPoolVehicleData.iBS[iVehicleIndex] = 0
			sMoonPoolVehicleData.iSpawnNode[iVehicleIndex] = 0
		ELSE
			PRINTLN("[MOON_POOL] CLEANUP_MOON_POOL_VEHICLE - waiting to take control.")	
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex])
			REMOVE_BLIP(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex])
		ENDIF
		
		sMoonPoolVehicleData.iBS[iVehicleIndex] = 0
		sMoonPoolVehicleData.iSpawnNode[iVehicleIndex] = 0
	ENDIF
	
	SET_MOON_POOL_VEHICLE_OUTSIDE(iVehicleIndex, FALSE)
	
	g_bRequestMoonPoolCleanup = FALSE
ENDPROC

FUNC BLIP_SPRITE GET_MOON_POOL_BLIP_SPRITE(INT iVehicleIndex)
	SWITCH iVehicleIndex
		CASE MOON_POOL_AVISA		RETURN RADAR_TRACE_MINI_SUB
		CASE MOON_POOL_SEASPARROW2	RETURN RADAR_TRACE_SEASPARROW2
	ENDSWITCH
	
	RETURN RADAR_TRACE_GANG_VEHICLE
ENDFUNC

FUNC STRING GET_MOON_POOL_BLIP_NAME(INT iVehicleIndex)
	SWITCH iVehicleIndex
		CASE MOON_POOL_AVISA		RETURN "MOON_POOL_1"
		CASE MOON_POOL_SEASPARROW2	RETURN "MOON_POOL_0"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_MOON_POOL_DELIVERED_HELP(INT iVehicleIndex, BOOL bBlipText = FALSE)
	IF bBlipText
		SWITCH iVehicleIndex
			CASE MOON_POOL_AVISA		RETURN "MOON_POOL_BLIP1"
			CASE MOON_POOL_SEASPARROW2	RETURN "MOON_POOL_BLIP2"
		ENDSWITCH
	ELSE
		SWITCH iVehicleIndex
			CASE MOON_POOL_AVISA		RETURN "MOON_POOL_DLVR1"
			CASE MOON_POOL_SEASPARROW2	RETURN "MOON_POOL_DLVR2"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

PROC MAINTAIN_MOON_POOL_VEHICLE_BLIP(MOON_POOL_VEHICLE_DATA &sMoonPoolVehicleData, INT iVehicleIndex)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex])
		IF IS_VEHICLE_SEAT_FREE(NET_TO_VEH(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex]), DEFAULT, TRUE)
			IF NOT DOES_BLIP_EXIST(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex])
				sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(sMoonPoolVehicleData.netMoonPoolVehicles[iVehicleIndex]))
				
				SET_BLIP_SPRITE(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex], GET_MOON_POOL_BLIP_SPRITE(iVehicleIndex))
				SET_BLIP_PRIORITY(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex], BLIPPRIORITY_HIGH)
				SET_BLIP_NAME_FROM_TEXT_FILE(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex], GET_MOON_POOL_BLIP_NAME(iVehicleIndex))
				
				HUD_COLOURS blipColour = GET_HUD_COLOUR_FOR_GANG_ID(GET_GANG_ID_FOR_PLAYER(PLAYER_ID()))
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex], blipColour)
				
				IF NOT IS_BIT_SET(sMoonPoolVehicleData.iBS[iVehicleIndex], MOON_POOL_BS_BLIP_CREATED)
					SET_BLIP_FLASH_TIMER(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex], 5000)
					
					PRINT_HELP_WITH_COLOURED_STRING(GET_MOON_POOL_DELIVERED_HELP(iVehicleIndex), GET_MOON_POOL_DELIVERED_HELP(iVehicleIndex, TRUE), GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
					
					SET_BIT(sMoonPoolVehicleData.iBS[iVehicleIndex], MOON_POOL_BS_BLIP_CREATED)
				ENDIF
			ELSE
				HUD_COLOURS blipColour = GET_HUD_COLOUR_FOR_GANG_ID(GET_GANG_ID_FOR_PLAYER(PLAYER_ID()))
				
				IF GET_BLIP_COLOUR(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex]) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(blipColour)
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex], blipColour)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex])
				REMOVE_BLIP(sMoonPoolVehicleData.blipMoonPoolVehicles[iVehicleIndex])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_MOON_POOL_VEHICLE_REQUESTED(INT iVehicleIndex)
	SWITCH iVehicleIndex
		CASE MOON_POOL_AVISA		RETURN g_bRequestMoonPoolAvisa
		CASE MOON_POOL_SEASPARROW2	RETURN g_bRequestMoonPoolSeasparrow2
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_MOON_POOL_REQUEST(INT iVehicleIndex)
	SWITCH iVehicleIndex
		CASE MOON_POOL_AVISA		g_bRequestMoonPoolAvisa = FALSE			BREAK
		CASE MOON_POOL_SEASPARROW2	g_bRequestMoonPoolSeasparrow2 = FALSE	BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SET_VEHICLE_AS_MOON_POOL_VEHICLE(MOON_POOL_VEHICLE_DATA &sMoonPoolVehicleData, VEHICLE_INDEX tempVeh)
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(tempVeh)
	AND NOT IS_VEHICLE_MY_PERSONAL_VEHICLE(tempVeh)
		SCRIPT_ASSERT("SET_VEHICLE_AS_MOON_POOL_VEHICLE - trying to set another players personal vehicle as my moon pool vehicle!")
		PRINTLN("[MOON_POOL] SET_VEHICLE_AS_MOON_POOL_VEHICLE - trying to set another players personal vehicle as my moon pool vehicle.")
		
		g_bAssignMoonPoolVehicle = FALSE
		
		RETURN FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(tempVeh)
		IF GET_ENTITY_MODEL(tempVeh) != AVISA
		AND GET_ENTITY_MODEL(tempVeh) != SEASPARROW2
			PRINTLN("[MOON_POOL] SET_VEHICLE_AS_MOON_POOL_VEHICLE - This will not work - model is not valid for moon pool vehicle.")
			SCRIPT_ASSERT("SET_VEHICLE_AS_MOON_POOL_VEHICLE - This will not work - model is not valid for moon pool vehicle.")
			
			g_bAssignMoonPoolVehicle = FALSE
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_CUTSCENE_PLAYING()
		IF NOT NETWORK_IS_IN_MP_CUTSCENE()
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				IF NETWORK_GET_ENTITY_IS_NETWORKED(tempVeh)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
						IF CAN_REGISTER_MISSION_VEHICLES(1)
							IF NOT IS_ENTITY_A_MISSION_ENTITY(tempVeh)
								SET_ENTITY_AS_MISSION_ENTITY(tempVeh, FALSE, TRUE)
								PRINTLN("[MOON_POOL] SET_VEHICLE_AS_MOON_POOL_VEHICLE - not a mission entity, setting as one.")
							ENDIF
							
							IF IS_ENTITY_A_MISSION_ENTITY(tempVeh)
								IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(tempVeh)
									SET_ENTITY_AS_MISSION_ENTITY(tempVeh, FALSE, TRUE)
									PRINTLN("[MOON_POOL] SET_VEHICLE_AS_MOON_POOL_VEHICLE - waiting for vehicle to belong to this script.")
								ENDIF
								
								IF GET_ENTITY_MODEL(tempVeh) = AVISA
									sMoonPoolVehicleData.netMoonPoolVehicles[MOON_POOL_AVISA] = VEH_TO_NET(tempVeh)
									SET_BIT(sMoonPoolVehicleData.iBS[MOON_POOL_AVISA], MOON_POOL_BS_VEHICLE_CREATED)
									SET_BIT(sMoonPoolVehicleData.iBS[MOON_POOL_AVISA], MOON_POOL_BS_BLIP_CREATED)
									SET_BIT(sMoonPoolVehicleData.iBS[MOON_POOL_AVISA], MOON_POOL_BS_RESET_LOCKSTATE)
									SET_MOON_POOL_VEHICLE_OUTSIDE(MOON_POOL_AVISA, TRUE)
									
									SET_VEHICLE_COLOURS(tempVeh, 70, 0)
									SET_VEHICLE_EXTRA_COLOURS(tempVeh, 70, 0)
									SET_VEHICLE_EXTRA_COLOUR_5(tempVeh, 121)
								ELIF GET_ENTITY_MODEL(tempVeh) = SEASPARROW2
									sMoonPoolVehicleData.netMoonPoolVehicles[MOON_POOL_SEASPARROW2] = VEH_TO_NET(tempVeh)
									SET_BIT(sMoonPoolVehicleData.iBS[MOON_POOL_SEASPARROW2], MOON_POOL_BS_VEHICLE_CREATED)
									SET_BIT(sMoonPoolVehicleData.iBS[MOON_POOL_SEASPARROW2], MOON_POOL_BS_BLIP_CREATED)
									SET_BIT(sMoonPoolVehicleData.iBS[MOON_POOL_SEASPARROW2], MOON_POOL_BS_RESET_LOCKSTATE)
									SET_MOON_POOL_VEHICLE_OUTSIDE(MOON_POOL_SEASPARROW2, TRUE)
								ENDIF
								
								IF DECOR_IS_REGISTERED_AS_TYPE("Player_Moon_Pool", DECOR_TYPE_INT)
									IF NOT DECOR_EXIST_ON(tempVeh, "Player_Moon_Pool")
										DECOR_SET_INT(tempVeh, "Player_Moon_Pool", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
									ENDIF
								ENDIF
								
								SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(VEH_TO_NET(tempVeh), PLAYER_ID(), TRUE) 
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempVeh, TRUE)
								
								PRINTLN("[MOON_POOL] SET_VEHICLE_AS_MOON_POOL_VEHICLE - new moon pool vehicle registered.")
								
								RETURN TRUE
							ENDIF
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[MOON_POOL] SET_VEHICLE_AS_MOON_POOL_VEHICLE - NETWORK_IS_IN_MP_CUTSCENE.")
		ENDIF
	ELSE
		PRINTLN("[MOON_POOL] SET_VEHICLE_AS_MOON_POOL_VEHICLE - IS_CUTSCENE_PLAYING.")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MOON_POOL_VEHICLE_OUTSIDE(INT iVehicleIndex)
	SWITCH iVehicleIndex
		CASE MOON_POOL_AVISA		RETURN IS_PLAYER_AVISA_OUTSIDE(PLAYER_ID())
		CASE MOON_POOL_SEASPARROW2	RETURN IS_PLAYER_SEASPARROW2_OUTSIDE(PLAYER_ID())
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OTHER_MOON_POOL_VEHICLE_ACTIVE(INT iVehicleIndex)
	SWITCH iVehicleIndex
		CASE MOON_POOL_AVISA		RETURN IS_PLAYER_SEASPARROW2_OUTSIDE(PLAYER_ID())
		CASE MOON_POOL_SEASPARROW2	RETURN IS_PLAYER_AVISA_OUTSIDE(PLAYER_ID())
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_MOON_POOL_VEHICLE_SPAWNING(MOON_POOL_VEHICLE_DATA &sMoonPoolVehicleData)
	INT i
	
	IF sMoonPoolVehicleData.iLockState != GET_MP_INT_CHARACTER_STAT(MP_STAT_SUBMARINE_MOON_POOL_ACCESS)
		sMoonPoolVehicleData.iLockState = GET_MP_INT_CHARACTER_STAT(MP_STAT_SUBMARINE_MOON_POOL_ACCESS)
		
		REPEAT NUM_MOON_POOL_VEHICLES i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sMoonPoolVehicleData.netMoonPoolVehicles[i])
				VEHICLE_INDEX tempVeh = NET_TO_VEH(sMoonPoolVehicleData.netMoonPoolVehicles[i])
				
				SET_LOCK_STATE_OF_VEHICLE(tempVeh, sMoonPoolVehicleData.iLockState, -1, PLAYER_ID())
			ENDIF
		ENDREPEAT
	ENDIF
	
	REPEAT NUM_MOON_POOL_VEHICLES i
		IF IS_MOON_POOL_VEHICLE_REQUESTED(i)
			IF CREATE_MOON_POOL_VEHICLE(sMoonPoolVehicleData, i)
				CLEAR_MOON_POOL_REQUEST(i)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(sMoonPoolVehicleData.iBS[i], MOON_POOL_BS_VEHICLE_CREATED)
			MAINTAIN_MOON_POOL_VEHICLE_BLIP(sMoonPoolVehicleData, i)
			
			IF (NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sMoonPoolVehicleData.netMoonPoolVehicles[i])
			OR NOT IS_NET_VEHICLE_DRIVEABLE(sMoonPoolVehicleData.netMoonPoolVehicles[i])
			OR g_bRequestMoonPoolCleanup
			OR IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
			OR NOT IS_MOON_POOL_VEHICLE_OUTSIDE(i))
			AND NOT IS_BIT_SET(g_SimpleInteriorData.iThirdBS, BS3_SIMPLE_INTERIOR_GLOBAL_DATA_DOING_EXIT_SCENE_IN_INTERIOR)
				BOOL bDelete = TRUE
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sMoonPoolVehicleData.netMoonPoolVehicles[i])
				AND NOT IS_NET_VEHICLE_DRIVEABLE(sMoonPoolVehicleData.netMoonPoolVehicles[i])
					PRINTLN("[MAINTAIN_MOON_POOL_VEHICLE_SPAWNING] - Setting destroyed vehicle as no longer needed.")
					
					bDelete = FALSE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				PRINTLN("[MAINTAIN_MOON_POOL_VEHICLE_SPAWNING] g_bRequestMoonPoolCleanup? ", GET_STRING_FROM_BOOL(g_bRequestMoonPoolCleanup))
				PRINTLN("[MAINTAIN_MOON_POOL_VEHICLE_SPAWNING] NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID()? ", GET_STRING_FROM_BOOL(NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sMoonPoolVehicleData.netMoonPoolVehicles[i])))
				PRINTLN("[MAINTAIN_MOON_POOL_VEHICLE_SPAWNING] IS_NET_VEHICLE_DRIVEABLE(sMoonPoolVehicleData.netMoonPoolVehicles[i])? ", GET_STRING_FROM_BOOL(IS_NET_VEHICLE_DRIVEABLE(sMoonPoolVehicleData.netMoonPoolVehicles[i])))
				#ENDIF
				
				CLEANUP_MOON_POOL_VEHICLE(sMoonPoolVehicleData, i, bDelete)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(sMoonPoolVehicleData.iBS[i], MOON_POOL_BS_RESET_LOCKSTATE)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sMoonPoolVehicleData.netMoonPoolVehicles[i])
			AND IS_NET_VEHICLE_DRIVEABLE(sMoonPoolVehicleData.netMoonPoolVehicles[i])
			AND IS_MOON_POOL_VEHICLE_OUTSIDE(i)
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
			AND IS_SCREEN_FADED_IN()
				IF NOT HAS_NET_TIMER_STARTED(sMoonPoolVehicleData.stMoonPoolLockstateTimer[i])
					REINIT_NET_TIMER(sMoonPoolVehicleData.stMoonPoolLockstateTimer[i])
				ELIF HAS_NET_TIMER_EXPIRED(sMoonPoolVehicleData.stMoonPoolLockstateTimer[i], 5000)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sMoonPoolVehicleData.netMoonPoolVehicles[i])
						VEHICLE_INDEX vehTemp = NET_TO_VEH(sMoonPoolVehicleData.netMoonPoolVehicles[i])
						
						SET_LOCK_STATE_OF_VEHICLE(vehTemp, sMoonPoolVehicleData.iLockState, -1, PLAYER_ID())
						
						RESET_NET_TIMER(sMoonPoolVehicleData.stMoonPoolLockstateTimer[i])
						
						CLEAR_BIT(sMoonPoolVehicleData.iBS[i], MOON_POOL_BS_RESET_LOCKSTATE)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sMoonPoolVehicleData.netMoonPoolVehicles[i])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF i = MOON_POOL_SEASPARROW2
			IF HAS_NET_TIMER_STARTED(sMoonPoolVehicleData.stMoonPoolInvincibleTimer)
			AND HAS_NET_TIMER_EXPIRED(sMoonPoolVehicleData.stMoonPoolInvincibleTimer, 10000)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sMoonPoolVehicleData.netMoonPoolVehicles[i])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sMoonPoolVehicleData.netMoonPoolVehicles[i])
						SET_ENTITY_INVINCIBLE(NET_TO_VEH(sMoonPoolVehicleData.netMoonPoolVehicles[i]), FALSE)
						
						RESET_NET_TIMER(sMoonPoolVehicleData.stMoonPoolInvincibleTimer)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sMoonPoolVehicleData.netMoonPoolVehicles[i])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF g_bAssignMoonPoolVehicle
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX tempveh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(tempveh)
			AND IS_VEHICLE_DRIVEABLE(tempveh)
			AND NETWORK_GET_ENTITY_IS_NETWORKED(tempveh)
				IF SET_VEHICLE_AS_MOON_POOL_VEHICLE(sMoonPoolVehicleData, tempVeh)
					g_bAssignMoonPoolVehicle = FALSE
				ENDIF
			ELSE
				PRINTLN("[MOON_POOL] SET_VEHICLE_AS_MOON_POOL_VEHICLE - Unable to process vehicle")
				
				g_bAssignMoonPoolVehicle = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_REQUEST_MOON_POOL_VEHICLE(INT iVehicleIndex)
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
	OR IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	SWITCH iVehicleIndex
		CASE MOON_POOL_AVISA
			IF IS_PLAYER_SEASPARROW2_OUTSIDE(PLAYER_ID())
				RETURN FALSE
			ENDIF
			
			RETURN IS_PLAYER_AVISA_PURCHASED(PLAYER_ID()) AND NOT IS_PLAYER_AVISA_OUTSIDE(PLAYER_ID())
		BREAK
		
		CASE MOON_POOL_SEASPARROW2
			IF IS_PLAYER_AVISA_OUTSIDE(PLAYER_ID())
				RETURN FALSE
			ENDIF
			
			IF GET_LOCAL_CD_TIME(REQUEST_MOON_POOL) > 0
				RETURN FALSE
			ELSE
				RETURN IS_PLAYER_SEASPARROW2_PURCHASED(PLAYER_ID()) AND NOT IS_PLAYER_SEASPARROW2_OUTSIDE(PLAYER_ID())
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_MOON_POOL_VEHICLE_PURCHASED()
	PLAYER_INDEX PlayerID = PLAYER_ID()
	IF IS_PLAYER_AVISA_PURCHASED(PlayerID)
	OR IS_PLAYER_SEASPARROW2_PURCHASED(PlayerID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

#ENDIF
