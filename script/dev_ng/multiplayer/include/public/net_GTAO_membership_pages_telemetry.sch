

USING "MP_globals_FM.sch"


#IF FEATURE_GTAO_MEMBERSHIP
FUNC STRING MEMBERSHIP_PAGE_INDEX_TO_STRING(INT PAGE)
	SWITCH PAGE
		CASE MEMBERSHIP_PAGES_PAUSE_MENU
			RETURN "PAUSE_MENU"
		BREAK
		CASE MEMBERSHIP_PAGES_MEMBERSHIP_UI
			RETURN "MEMBERSHIP_UI"
		BREAK
		CASE MEMBERSHIP_PAGES_STORE
			RETURN "STORE"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC MEMBERSHIP_TELEMETRY_MARK_MEMBERSHIP_PAGE_AS_SEEN(INT PAGE)
	IF NOT g_sMembershipTelemetryData.bIsMeasuringTime
		PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY] Time is not being tracked.")
		g_sMembershipTelemetryData.bIsMeasuringTime = TRUE
		PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY] Marked ", MEMBERSHIP_PAGE_INDEX_TO_STRING(PAGE), " AS SEEN")
		SET_BIT(g_sMembershipTelemetryData.iBS_pageView, PAGE)
		g_sMembershipTelemetryData.iStartPosix = GET_CLOUD_TIME_AS_INT()
	ELSE
		IF NOT IS_BIT_SET(g_sMembershipTelemetryData.iBS_pageView, PAGE)
			PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY] Marked ", MEMBERSHIP_PAGE_INDEX_TO_STRING(PAGE), " AS SEEN")
			SET_BIT(g_sMembershipTelemetryData.iBS_pageView, PAGE)
		ENDIF
	ENDIF
ENDPROC

PROC MEMBERSHIP_TELEMETRY_PRINT_DATA()
	
	IF g_sMembershipTelemetryData.bRunDelayedSend
		PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY] DATA SENT: (DELAYED BY 5sec due to visiting the store first)")
	ELSE
		PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY] DATA SENT:")
	ENDIF
	IF IS_BIT_SET(g_sMembershipTelemetryData.iBS_pageView, MEMBERSHIP_PAGES_PAUSE_MENU)
		PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY] VISITED PAGE: PAUSE_MENU")
	ENDIF
	IF IS_BIT_SET(g_sMembershipTelemetryData.iBS_pageView, MEMBERSHIP_PAGES_MEMBERSHIP_UI)
		PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY] VISITED PAGE: MEMBERSHIP_UI")
	ENDIF
	IF IS_BIT_SET(g_sMembershipTelemetryData.iBS_pageView, MEMBERSHIP_PAGES_STORE)
		PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY] VISITED PAGE: STORE")
	ENDIF
	IF g_sMembershipTelemetryData.leftBy = MEMBERSHIP_PAGES_LEAVE_REASON_CLOSED_PAUSE_MENU
		PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY]  LEAVE REASON : CLOSED_PAUSE_MENU")
	ELIF g_sMembershipTelemetryData.leftBy =  MEMBERSHIP_PAGES_LEAVE_REASON_PURCHASED_MEMBERSHIP
		PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY]  LEAVE REASON : PURCHASED_MEMBERSHIP")
	ELSE
		PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY]  LEAVE REASON : INVALID_REASON")
	ENDIF
	PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY]  DURATION :", g_sMembershipTelemetryData.iDuration)
ENDPROC

PROC MEMBERSHIP_TELEMETRY_RESET_DATA()
	g_sMembershipTelemetryData.iBS_pageView = 0
	g_sMembershipTelemetryData.iDuration = 0
	g_sMembershipTelemetryData.leftBy =  MEMBERSHIP_PAGES_LEAVE_REASON_INVALID
	g_sMembershipTelemetryData.iStartPosix = 0
	g_sMembershipTelemetryData.bIsMeasuringTime = FALSE
	g_sMembershipTelemetryData.bRunDelayedSend = FALSE
	RESET_NET_TIMER(g_sMembershipTelemetryData.timerSendDelay)
ENDPROC
PROC MEMBERSHIP_TELEMETRY_SEND_RESULTS(MEMBERSHIP_PAGES_LEAVE_REASON_ENUM LEAVE_REASON)
	IF g_sMembershipTelemetryData.bIsMeasuringTime	
		PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY] Time tracking finished. Sending telemetry data.")
		g_sMembershipTelemetryData.iDuration = GET_CLOUD_TIME_AS_INT() - g_sMembershipTelemetryData.iStartPosix
		IF g_sMembershipTelemetryData.iDuration>2
			g_sMembershipTelemetryData.leftBy = LEAVE_REASON
			
			IF g_sMembershipTelemetryData.leftBy = MEMBERSHIP_PAGES_LEAVE_REASON_CLOSED_PAUSE_MENU			//if leaving the membership page after visiting store wait before sending results
			AND IS_BIT_SET(g_sMembershipTelemetryData.iBS_pageView, MEMBERSHIP_PAGES_STORE)
				g_sMembershipTelemetryData.bRunDelayedSend = TRUE
			ELSE	//if player left through any other means send immediately
				PLAYSTATS_PM_NAV(g_sMembershipTelemetryData.iBS_pageView, ENUM_TO_INT(g_sMembershipTelemetryData.leftBy), g_sMembershipTelemetryData.iDuration)
				MEMBERSHIP_TELEMETRY_PRINT_DATA()
				MEMBERSHIP_TELEMETRY_RESET_DATA()
			ENDIF
		ELSE
		
			MEMBERSHIP_TELEMETRY_RESET_DATA()
			PRINTLN("[GTA+ MEMBERSHIP][TELEMETRY] Duration was <2s. Not sending data.")
		ENDIF
	ENDIF
ENDPROC

//this is a failsafe if player purchases a membership and quickly leaves the membership page and exits pause menu before the telemetry is sent
PROC MAINTAIN_TELEMETRY_SENDING_DELAY_TIMER()
	IF g_sMembershipTelemetryData.bRunDelayedSend
		IF NOT HAS_NET_TIMER_STARTED(g_sMembershipTelemetryData.timerSendDelay)
			START_NET_TIMER(g_sMembershipTelemetryData.timerSendDelay)
		ELIF HAS_NET_TIMER_EXPIRED(g_sMembershipTelemetryData.timerSendDelay, 5000)
			PLAYSTATS_PM_NAV(g_sMembershipTelemetryData.iBS_pageView, ENUM_TO_INT(g_sMembershipTelemetryData.leftBy), g_sMembershipTelemetryData.iDuration)
			MEMBERSHIP_TELEMETRY_PRINT_DATA()
			MEMBERSHIP_TELEMETRY_RESET_DATA()	
		ENDIF
	ENDIF
ENDPROC
#ENDIF //FEATURE_GTAO_MEMBERSHIP
