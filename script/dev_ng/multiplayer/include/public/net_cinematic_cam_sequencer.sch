////////////////////////////////////////////////////////////////////////////////////////////
//																						  //
//		SCRIPT NAME		:	net_cinematic_cam_sequencer.sch								  //
//		AUTHOR			:	Tom Turner													  //
//		DESCRIPTION		:	Reuseable system for running camera behaviours in a sequence. //
//							Behaviours can be set to run only in specific areas			  //
//																						  //
////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "cinematic_cam_behaviour.sch"
USING "net_cinematic_cam_sequencer_typedefs.sch"
USING "net_cinematic_cam_sequencer_debug.sch"


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡    SEQUENCER PROCS/FUNCS    ╞═════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC FLOAT _CINEMATIC_CAM_SEQUENCE__GET_WEIGHT_OF_CAM_AT_INDEX(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst, INT iIndex)
	
	#IF IS_DEBUG_BUILD
		IF sInst.sDebug.bEdit
			// Because cams are recycled we need to only ccumultate weights for added cams
			IF sInst.sDebug.sDebugCams[iIndex].bAdded
				PRINTLN("[CINEMATIC_CAM_SEQUENCE][WEIGHTING] GET_WEIGHT_OF_CAM_AT_INDEX - (", iIndex, ") grabbed debug cam weighting")
				RETURN sInst.sDebug.sDebugCams[iIndex].sCam.fWeighting
			ENDIF
			
			PRINTLN("[CINEMATIC_CAM_SEQUENCE][WEIGHTING] GET_WEIGHT_OF_CAM_AT_INDEX - (", iIndex, ") un-added debug cam, returning invalid")
			RETURN -1.0
		ENDIF
	#ENDIF	
	
	PRINTLN("[CINEMATIC_CAM_SEQUENCE][WEIGHTING] GET_WEIGHT_OF_CAM_AT_INDEX - (", iIndex, ") grabbed lookup cam weighting")
	
	CINEMATIC_CAM_BEHAVIOUR_STRUCT sCam
	sCam = CALL sInst.fpCamLookup(iIndex)
	RETURN sCam.fWeighting
	
ENDFUNC

FUNC INT _CINEMATIC_CAM_SEQUENCE__GET_RANDOM_WEIGHTED_CAM_INDEX(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE() * sInst.fTotalAccumulatedWeights
	INT iMaxWeight = sInst.iMaxCam
	
	#IF IS_DEBUG_BUILD
	IF sInst.sDebug.bEdit
		iMaxWeight = COUNT_OF(sInst.sDebug.sDebugCams)// In debug loop through all cams, because they may be unsorted
	ENDIF
	#ENDIF
	
	INT iCam = 0
	FLOAT fBiggestWeight = -1.0
	INT iCamWithBiggestWeight = 0
	FLOAT fWeight = 0.0
	
	REPEAT iMaxWeight iCam
		PRINTLN("[CINEMATIC_CAM_SEQUENCE][WEIGHTING] GET_RANDOM_WEIGHTED_CAM_INDEX - (", iCam, ") fRand: ", fRand)
		
		fWeight = _CINEMATIC_CAM_SEQUENCE__GET_WEIGHT_OF_CAM_AT_INDEX(sInst, iCam)
		
		PRINTLN("[CINEMATIC_CAM_SEQUENCE][WEIGHTING] GET_RANDOM_WEIGHTED_CAM_INDEX - (", iCam, ") checking weight: ", fWeight)
		
		// Invalid weights should never be picked, this will only be true if we hit a camera that was removed by the editor
		IF fWeight = -1.0
			PRINTLN("[CINEMATIC_CAM_SEQUENCE][WEIGHTING] GET_RANDOM_WEIGHTED_CAM_INDEX - (", iCam, ") weight invalid relooping")
			RELOOP
		ENDIF
		
		IF fWeight > fBiggestWeight
			fBiggestWeight = fWeight
			iCamWithBiggestWeight = iCam
			PRINTLN("[CINEMATIC_CAM_SEQUENCE][WEIGHTING] GET_RANDOM_WEIGHTED_CAM_INDEX - (", iCam, ") found new biggest weight of: ", fBiggestWeight)
		ENDIF
		
		IF fWeight >= fRand
			PRINTLN("[CINEMATIC_CAM_SEQUENCE][WEIGHTING] GET_RANDOM_WEIGHTED_CAM_INDEX - (", iCam, ") found cam")
			RETURN iCam
		ENDIF
		
		// Subtract weight from rand so later cams are more likely
		fRand -= fWeight
		PRINTLN("[CINEMATIC_CAM_SEQUENCE][WEIGHTING] GET_RANDOM_WEIGHTED_CAM_INDEX - (", iCam, ") subtracting cam weight from rand")
		
	ENDREPEAT	
	
	PRINTLN("[CINEMATIC_CAM_SEQUENCE][WEIGHTING] GET_RANDOM_WEIGHTED_CAM_INDEX - Reached end of cam list, falling back to cam with biggest weight: ", iCam)
	RETURN iCamWithBiggestWeight
	
ENDFUNC

/// PURPOSE:
///    Gets the next valid camera behaviour in a given area
/// PARAMS:
///    currentCamIndex - the index of the current camera behaviour
///    area - the current active area
/// RETURNS:
///    The index of the next camera behaviour
FUNC INT _CINEMATIC_CAM_SEQUENCE__GET_NEXT_CAM_BEHAVIOUR_INDEX(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	
	INT iMaxValue = _CINEMATIC_CAM_SEQUENCE__GET_MAX_CAMERA(sInst)
	INT iLastCam = sInst.iCurrentCamBehaviourIndex
	INT iCurrentCamIndex = sInst.iCurrentCamBehaviourIndex
	
	IF sInst.bRandomise
	
		IF sInst.bUseWeightings
			iCurrentCamIndex = _CINEMATIC_CAM_SEQUENCE__GET_RANDOM_WEIGHTED_CAM_INDEX(sInst)
			PRINTLN("[CINEMATIC_CAM_SEQUENCE] GET_NEXT_CAM_BEHAVIOUR_INDEX - iCurrentCamIndex: ", iCurrentCamIndex)
		ELSE
			iCurrentCamIndex = GET_RANDOM_INT_IN_RANGE(0, iMaxValue + 1)
		ENDIF
		
		/// If editing don't bother making sure we didn't hit last and don't wrap because 
		/// the debubg cam indexs are out of order and won't wrap correctly
		#IF IS_DEBUG_BUILD
		IF sInst.sDebug.bEdit
			RETURN iCurrentCamIndex
		ENDIF
		#ENDIF
		
		// Make sure we grab a new one
		IF iCurrentCamIndex = iLastCam
			++iCurrentCamIndex
			PRINTLN("[CINEMATIC_CAM_SEQUENCE] GET_NEXT_CAM_BEHAVIOUR_INDEX - picked last cam incrementing to next, iCurrentCamIndex: ", iCurrentCamIndex)
		ENDIF
	ELSE
		++iCurrentCamIndex
	ENDIF		
	
	// Wrap index
	iCurrentCamIndex = ((iCurrentCamIndex % iMaxValue) + iMaxValue) % iMaxValue
	PRINTLN("[CINEMATIC_CAM_SEQUENCE] GET_NEXT_CAM_BEHAVIOUR_INDEX - wrapped result, iCurrentCamIndex: ", iCurrentCamIndex)
	RETURN iCurrentCamIndex
	
ENDFUNC

PROC _CINEMATIC_CAM_SEQUENCE__START_CURRENT_CAMERA(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	
	#IF IS_DEBUG_BUILD
	IF sInst.sDebug.bEdit
		CINEMATIC_CAM_BEHAVIOUR__START(sInst.ciCamera, sInst.sDebug.sDebugCams[sInst.iCurrentCamBehaviourIndex].sCam)
		CINEMATIC_CAM_BEHAVIOUR__MAINTAIN(sInst.ciCamera, sInst.sDebug.sDebugCams[sInst.iCurrentCamBehaviourIndex].sCam)
		EXIT
	ENDIF
	#ENDIF
	
	CINEMATIC_CAM_BEHAVIOUR__START(sInst.ciCamera, sInst.sCurrentBehaviour)
	
ENDPROC

FUNC BOOL _CINEMATIC_CAM_SEQUENCE__MAINTAIN_CURRENT_CAMERA(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	
	#IF IS_DEBUG_BUILD
	IF sInst.sDebug.bEdit
		PRINTLN("[CINEMATIC_CAM_SEQUENCE] MAINTAIN_CURRENT_CAMERA - maintaing debug editor cam: ", sInst.iCurrentCamBehaviourIndex)
		RETURN CINEMATIC_CAM_BEHAVIOUR__MAINTAIN(sInst.ciCamera, sInst.sDebug.sDebugCams[sInst.iCurrentCamBehaviourIndex].sCam)
	ENDIF
	#ENDIF

	RETURN CINEMATIC_CAM_BEHAVIOUR__MAINTAIN(sInst.ciCamera, sInst.sCurrentBehaviour)
	
ENDFUNC


/// PURPOSE:
///    Swaps sequencer to next camera behaviour to run
/// PARAMS:
///    sInst - sequencer instance
///    area - the active area
FUNC BOOL _CINEMATIC_CAM_SEQUENCE__SWAP_TO_NEXT_CAM_BEHAVIOUR(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	
	IF sInst.fpCamLookup = NULL 
		RETURN FALSE
	ENDIF
	
	// Get next behaviour
	sInst.iCurrentCamBehaviourIndex = _CINEMATIC_CAM_SEQUENCE__GET_NEXT_CAM_BEHAVIOUR_INDEX(sInst)
	PRINTLN("[CINEMATIC_CAM_SEQUENCE] SWAP_TO_NEXT_CAM_BEHAVIOUR - next cam: ", sInst.iCurrentCamBehaviourIndex)
	
	#IF IS_DEBUG_BUILD
		IF sInst.sDebug.iPreviewCam > -1
			sInst.iCurrentCamBehaviourIndex = sInst.sDebug.iPreviewCam		
			PRINTLN("[CINEMATIC_CAM_SEQUENCE] SWAP_TO_NEXT_CAM_BEHAVIOUR - debug editor overriding to preview cam: ", sInst.iCurrentCamBehaviourIndex)
		ENDIF
	#ENDIF
	
	CINEMATIC_CAM_BEHAVIOUR__CLEAR(sInst.sCurrentBehaviour)
	sInst.sCurrentBehaviour = CALL sInst.fpCamLookup(sInst.iCurrentCamBehaviourIndex)
	
	_CINEMATIC_CAM_SEQUENCE__START_CURRENT_CAMERA(sInst)
	_CINEMATIC_CAM_SEQUENCE__MAINTAIN_CURRENT_CAMERA(sInst) // Warm up one frame
	
	// Set next camera active
	SET_CAM_ACTIVE(sInst.ciCamera, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	RETURN TRUE
	
ENDFUNC


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡    INIT/CLEAN UP FUNCTIONS    ╞═════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Creates the camera to be used in the cinematic
/// PARAMS:
///    sInst - the sequencer instance
PROC _CINEMATIC_CAM_SEQUENCE__CREATE_CAMERAS(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	
	IF NOT DOES_CAM_EXIST(sInst.ciCamera)
		sInst.ciCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, 65)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Stops the camera sequence and resets it
/// PARAMS:
///    sInst - the sequencer instance
PROC CINEMATIC_CAM_SEQUENCE__RESET(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	IF sInst.ciCamera != NULL
		SET_CAM_ACTIVE(sInst.ciCamera, FALSE)
	ENDIF
	
	// Clean up camera
	DESTROY_CAM(sInst.ciCamera)
	sInst.ciCamera = NULL
	sInst.iCurrentCamBehaviourIndex = 0
	
ENDPROC

/// PURPOSE:
///    Cleans up the cinematic sequence, this puts it in an unitialized state
///    if cleaned up you must reinitialise with the camera sequence
/// PARAMS:
///    sInst - the sequencer instance
PROC CINEMATIC_CAM_SEQUENCE__CLEANUP(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	
	CINEMATIC_CAM_SEQUENCE__RESET(sInst)
	
	sInst.fpCamLookup = NULL
	sInst.iMaxCam = 0
	#IF IS_DEBUG_BUILD
	CINEMATIC_CAM_SEQUENCE_DEBUG__CLEANUP(sInst)
	#ENDIF
	sInst.bInitialised = FALSE
	
ENDPROC

/// PURPOSE:
///    Attempts to initialize the camera used by the sequencer if it deosn't exist yet
/// PARAMS:
///    sequencerInstance - the seqencer "instance" to init
PROC _CINEMATIC_CAM_SEQUENCE__INIT_CAMERA(CINEMATIC_CAM_SEQUENCE_STRUCT& sInst)
	
	IF NOT DOES_CAM_EXIST(sInst.ciCamera)	
		_CINEMATIC_CAM_SEQUENCE__CREATE_CAMERAS(sInst)
		_CINEMATIC_CAM_SEQUENCE__SWAP_TO_NEXT_CAM_BEHAVIOUR(sInst)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)	
	ENDIF
	
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡    ENTRY POINT    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC CINEMATIC_CAM_SEQUENCE__INIT(CINEMATIC_CAM_SEQUENCE_STRUCT& sInst, INT iBehaviourCount, GET_CINEMATIC_CAMERA_BEHAVIOUR fpGetCameraBehaviour, BOOL bRandomOrder = FALSE, BOOL bUseWeightings = FALSE)
	
	sInst.fpCamLookup = fpGetCameraBehaviour
	sInst.iMaxCam = iBehaviourCount
	sInst.bInitialised = TRUE
	sInst.bRandomise = bRandomOrder
	sInst.bUseWeightings = bUseWeightings
	
	IF sInst.bUseWeightings
		CINEMATIC_CAM_SEQUENCE__REFRESH_CAM_WEIGHTINGS(sInst)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Runs a given cinematic camera sequence
/// PARAMS:
///    camSequence - The cinematic sequence to be run
///    bNoInteruption - If the cinematic cam forces itself as the rendered cam every frame
/// RETURNS:
///    FALSE if failed to initialise or switch to next camera
FUNC BOOL CINEMATIC_CAM_SEQUENCE__MAINTAIN(CINEMATIC_CAM_SEQUENCE_STRUCT& sInst, BOOL bNoInteruption = FALSE)
	
	DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF NOT sInst.bInitialised
		PRINTLN("[CINEMATIC_CAM_SEQUENCE] - Not initialized!")
		RETURN FALSE
	ENDIF
	
	_CINEMATIC_CAM_SEQUENCE__INIT_CAMERA(sInst)
	
	#IF IS_DEBUG_BUILD
		CINEMATIC_CAM_SEQUENCE_DEBUG__MAINTAIN(sInst)
	#ENDIF
	
	// Ensure cinematic is not interupted
	IF bNoInteruption
		SET_CAM_ACTIVE(sInst.ciCamera, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// Always refresh these in debug in case the cams are edited
		CINEMATIC_CAM_SEQUENCE__REFRESH_CAM_WEIGHTINGS(sInst)
		
		IF sInst.sDebug.iPreviewCam != -1 AND sInst.sDebug.iPreviewCam != sInst.iCurrentCamBehaviourIndex
			_CINEMATIC_CAM_SEQUENCE__SWAP_TO_NEXT_CAM_BEHAVIOUR(sInst)
		ENDIF
	#ENDIF
	
	IF _CINEMATIC_CAM_SEQUENCE__MAINTAIN_CURRENT_CAMERA(sInst)
		
		// Swap to next camera
		IF NOT _CINEMATIC_CAM_SEQUENCE__SWAP_TO_NEXT_CAM_BEHAVIOUR(sInst)
			// End cinematic if failed
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	RETURN TRUE
	
ENDFUNC
