///    Print functions.

// --- Include Files ------------------------------------------------------------

USING "types.sch"
USING "commands_network.sch"
USING "commands_script.sch"

USING "script_network.sch"

USING "script_DEBUG.sch"

ENUM NET_PRINT_LEVEL
	ePRINT_STD,			// Normal prints, using existing PRINTSTRING ect, use NET_NL() or \n.
	ePRINT_NET_WARN,	// Prints using NET_WARNING, auto newline.
	ePRINT_NET_ERROR	// Prints using NET_ERROR, auto newline.
ENDENUM 

// --- Functions ----------------------------------------------------------------

/// PURPOSE:
///    Prints a newline, and adds a newline if the log file is active.
DEBUGONLY PROC NET_NL()
	PRINTNL()		
ENDPROC



/// PURPOSE:
///    Print a net debug string.
DEBUGONLY PROC NET_PRINT(STRING sText, NET_PRINT_LEVEL ePrintLevel = ePRINT_STD)

	// KGM 11/10/11: Always print the text - this ensures it gets output to the 'script' debug tab too (but does mean the message will be duplicated in the 'output' console log).
	PRINTSTRING(sText)

	SWITCH ePrintLevel
		
		CASE ePRINT_NET_WARN
			NET_WARNING(sText, TRUE)	 
		BREAK
		
		CASE ePRINT_NET_ERROR
			NET_ERROR(sText, TRUE)
		BREAK		
		
	ENDSWITCH

ENDPROC



/// PURPOSE:
///    Print a net debug int.
DEBUGONLY PROC NET_PRINT_INT(INT iInt, NET_PRINT_LEVEL ePrintLevel = ePRINT_STD )

	TEXT_LABEL_15 tl = iInt
	NET_PRINT(tl,ePrintLevel)
	
ENDPROC

/// PURPOSE:
///    Print a net debug bool - prints a string 'TRUE' or 'FALSE'.
DEBUGONLY PROC NET_PRINT_BOOL(BOOL bBool,  NET_PRINT_LEVEL ePrintLevel = ePRINT_STD )

	IF (bBool)
		NET_PRINT("TRUE", ePrintLevel)
		EXIT
	ENDIF
	
	NET_PRINT("FALSE", ePrintLevel)
		
ENDPROC

/// PURPOSE:
///    Print a net debug float.
DEBUGONLY PROC NET_PRINT_FLOAT(FLOAT fFloat, NET_PRINT_LEVEL ePrintLevel = ePRINT_STD )
	NET_PRINT(GET_STRING_FROM_FLOAT(fFloat),ePrintLevel)
ENDPROC

/// PURPOSE:
///    Gets a text label from a vector.
DEBUGONLY FUNC STRING GET_STRING_FROM_VECTOR(VECTOR vVec)

	// KGM NOTE: Even though this function is DEBUGONLY, it's calling a function that is wrapped with #IF_IS_DEBUG_BUILD, so we need to wrap that call in #IF_IS_DEBUG_BUILD too.
	//			 Not entirely sure why it's a problem but my guess is that the script compiles as a two-pass operation and the first pass thinks there is a problem and complains.
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tl63Vec 
	
		tl63Vec = "<<"
		tl63Vec += GET_STRING_FROM_FLOAT(vVec.x)
	  	tl63Vec += ","
		tl63Vec += GET_STRING_FROM_FLOAT(vVec.y)
	  	tl63Vec += ","
		tl63Vec += GET_STRING_FROM_FLOAT(vVec.z)
		tl63Vec += ">>"
	
		RETURN GET_FIRST_N_CHARACTERS_OF_LITERAL_STRING(tl63Vec, GET_LENGTH_OF_LITERAL_STRING(tl63Vec))
	#ENDIF

	// Also need to do a fake return and a fake usage of the input parameter to avoid a compile error in release because of the same reason noted above
	// In release though, this whole function will be ignored - but that must be after it's done these error checks
	vVec = vVec
	
	STRING fakeReturn = ""
	RETURN (fakeReturn)
		
ENDFUNC

/// PURPOSE:
///    Print a net debug vector.
DEBUGONLY PROC NET_PRINT_VECTOR(VECTOR vVec, NET_PRINT_LEVEL ePrintLevel = ePRINT_STD )	
	NET_PRINT(GET_STRING_FROM_VECTOR(vVec), ePrintLevel)		
ENDPROC

/// PURPOSE:
///    Prints net debug string and int.
DEBUGONLY PROC NET_PRINT_STRING_INT(STRING sText, INT iInt, NET_PRINT_LEVEL ePrintLevel = ePRINT_STD )

	// KGM 11/10/11: Always print the text - this ensures it gets output to the 'script' debug tab too (but does mean the message will be duplicated in the 'output' console log).
	PRINTSTRING(sText)
	PRINTINT(iInt)

	SWITCH ePrintLevel		
	
		CASE ePRINT_NET_WARN
			NET_DEBUG_INT(sText,iInt, TRUE)	
		BREAK
		
		CASE ePRINT_NET_ERROR
			NET_DEBUG_INT(sText,iInt, TRUE)
		BREAK		
		
	ENDSWITCH
		
ENDPROC

/// PURPOSE:
///    Prints net debug string and int.
DEBUGONLY PROC NET_PRINT_STRING_FLOAT(STRING sText, FLOAT fFloat, NET_PRINT_LEVEL ePrintLevel = ePRINT_STD )
	
	// KGM 11/10/11: Always print the text - this ensures it gets output to the 'script' debug tab too (but does mean the message will be duplicated in the 'output' console log).
	PRINTSTRING(sText)
	PRINTFLOAT(fFloat)

	SWITCH ePrintLevel		
	
		CASE ePRINT_NET_WARN
			NET_DEBUG_FLOAT(sText,fFloat, TRUE)	
		BREAK
		
		CASE ePRINT_NET_ERROR
			NET_DEBUG_FLOAT(sText,fFloat, TRUE)
		BREAK		
		
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Prints net debug string and int.
DEBUGONLY PROC NET_PRINT_STRING_VECTOR(STRING sText, VECTOR vVec, NET_PRINT_LEVEL ePrintLevel = ePRINT_STD )
	
	// KGM 11/10/11: Always print the text - this ensures it gets output to the 'script' debug tab too (but does mean the message will be duplicated in the 'output' console log).
	PRINTSTRING(sText)
	PRINTSTRING(GET_STRING_FROM_VECTOR(vVec))
	
	SWITCH ePrintLevel
		
		CASE ePRINT_NET_WARN
			NET_DEBUG_VECTOR(sText,vVec, TRUE)	
		BREAK
		
		CASE ePRINT_NET_ERROR
			NET_DEBUG_VECTOR(sText,vVec, TRUE)
		BREAK	
		
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Prints two net debug strings.
DEBUGONLY PROC NET_PRINT_STRINGS(STRING sText1, STRING sText2, NET_PRINT_LEVEL ePrintLevel = ePRINT_STD )
	
	// KGM 11/10/11: Always print the text - this ensures it gets output to the 'script' debug tab too (but does mean the message will be duplicated in the 'output' console log).
	PRINTSTRING(sText1)
	PRINTSTRING(sText2)

	SWITCH ePrintLevel
		
		CASE ePRINT_NET_WARN
			NET_DEBUG_STRING(sText1,sText2, TRUE)	
		BREAK
		
		CASE ePRINT_NET_ERROR
			NET_DEBUG_STRING(sText1,sText2, TRUE)
		BREAK		
		
	ENDSWITCH
		
ENDPROC

/// PURPOSE:
///    PRINTS the net time in human readable format. 
DEBUGONLY PROC NET_PRINT_TIME(NET_PRINT_LEVEL ePrintLevel = ePRINT_STD)

	TEXT_LABEL_31 tl31
	IF NETWORK_IS_GAME_IN_PROGRESS()			
		// KGM NOTE: The function below is wrapped in #IF_IS_DEBUG_BUILD so I also need to wrap the use of the function even though this whole function is DEBUGONLY.
		//			 As explained in an earlier function, the reason for this, I think, is that it performs error checking before removing the whole function in release.
		#IF IS_DEBUG_BUILD		
			tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
		#ENDIF
		tl31 += ": "			
	ELSE
		tl31 = " 00:00:00 "
	ENDIF
	NET_PRINT(tl31,ePrintLevel)

ENDPROC

/// PURPOSE:
///    PRINTS the current frame number 
DEBUGONLY PROC NET_PRINT_FRAME(NET_PRINT_LEVEL ePrintLevel = ePRINT_STD)
	TEXT_LABEL_31 tl31
	tl31 = "[frame:"
	tl31 += GET_FRAME_COUNT()
	tl31 += "] "
	NET_PRINT(tl31, ePrintLevel)
ENDPROC


/// PURPOSE:
///    PRINTS the thread id of the calling script.
DEBUGONLY PROC NET_PRINT_THREAD_ID(NET_PRINT_LEVEL ePrintLevel = ePRINT_STD)

	TEXT_LABEL_31 tl31
	tl31 = " Thread ID = "
	tl31 += NATIVE_TO_INT(GET_ID_OF_THIS_THREAD())
	tl31 += " "
	NET_PRINT(tl31, ePrintLevel)
	
ENDPROC

/// PURPOSE:
///    PRINTS the thread id of the calling script.
DEBUGONLY PROC NET_PRINT_SCRIPT(NET_PRINT_LEVEL ePrintLevel = ePRINT_STD)

	TEXT_LABEL_31 tl31
	tl31 = " "
	tl31 += GET_THIS_SCRIPT_NAME()
	tl31 += " "
	NET_PRINT(tl31, ePrintLevel)
		
ENDPROC


/// PURPOSE:
///    Prints a time stamp, followed by thread id, "msg", then calls SCRIPT_ASSERT passing in "msg".
/// PARAMS:
///    msg - The assert msg.
DEBUGONLY PROC NET_SCRIPT_ASSERT(STRING msg)

	NET_PRINT_TIME(ePRINT_NET_ERROR) 
	NET_PRINT_THREAD_ID(ePRINT_NET_ERROR)
	NET_PRINT_SCRIPT(ePRINT_NET_ERROR)
	NET_PRINT_SCRIPT(ePRINT_NET_ERROR)
	NET_PRINT(msg, ePRINT_NET_ERROR)
	SCRIPT_ASSERT(msg)
		
ENDPROC


/// PURPOSE:
///    Asserts and prints and error when the player calling sFunctionName is not the script host.
/// PARAMS:
///    sFunctionName - The command the player is calling.
DEBUGONLY PROC NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT(STRING sFunctionName)

	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF

	NET_PRINT_TIME()
	NET_PRINT_SCRIPT()
	NET_PRINT("NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT: ", ePRINT_NET_ERROR)  
	NET_PRINT(sFunctionName, ePRINT_NET_ERROR) 
	NET_PRINT(" Only the script host can call ", ePRINT_NET_ERROR) 
	
	NET_SCRIPT_ASSERT(sFunctionName)
		
ENDPROC


DEBUGONLY PROC NET_PRINT_DATE(STRUCT_STAT_DATE& aDate, NET_PRINT_LEVEL ePrintLevel = ePRINT_STD )

//	NET_NL()
//	NET_PRINT("Year = ", ePrintLevel)NET_PRINT_INT(aDate.Year, ePrintLevel)
//	NET_NL()
//	NET_PRINT("Month = ", ePrintLevel)NET_PRINT_INT(aDate.Month, ePrintLevel)
//	NET_NL()
//	NET_PRINT("Day = ", ePrintLevel)NET_PRINT_INT(aDate.Day, ePrintLevel)
//	NET_NL()
//	NET_PRINT("Hour = ", ePrintLevel)NET_PRINT_INT(aDate.Hour, ePrintLevel)
//	NET_NL()
//	NET_PRINT("Minute = ", ePrintLevel)NET_PRINT_INT(aDate.Minute, ePrintLevel)
//	NET_NL()
//	NET_PRINT("Seconds = ", ePrintLevel)NET_PRINT_INT(aDate.Seconds, ePrintLevel)

	NET_PRINT_INT(aDate.Hour, ePrintLevel)
	NET_PRINT(":", ePrintLevel)
	NET_PRINT_INT(aDate.Minute, ePrintLevel)
	NET_PRINT(":", ePrintLevel)
	NET_PRINT_INT(aDate.Seconds, ePrintLevel)
	NET_PRINT(":", ePrintLevel)
	NET_PRINT_INT(aDate.Milliseconds, ePrintLevel)
	NET_PRINT(" ", ePrintLevel)
	NET_PRINT_INT(aDate.Day, ePrintLevel)
	NET_PRINT("/", ePrintLevel)
	NET_PRINT_INT(aDate.Month, ePrintLevel)
	NET_PRINT("/", ePrintLevel)
	NET_PRINT_INT(aDate.Year, ePrintLevel)

ENDPROC

DEBUGONLY FUNC STRING NET_GET_STRING_UGC_DATE(UGC_DATE& aDate)
	 IF (aDate.nHour = 0)
	AND (aDate.nMinute = 0)
	AND (aDate.nSecond = 0)
	AND (aDate.nDay = 1)
	AND (aDate.nMonth = 1)
	AND (aDate.nYear = 1970)
		RETURN "NONE"
	ENDIF
	
	TEXT_LABEL_23 tl23 = ""
	tl23 += (aDate.nHour)
	tl23 += (":")
	IF (aDate.nMinute < 10)
		tl23 += ("0")
	ENDIF
	tl23 += (aDate.nMinute)
	tl23 += (":")
	IF (aDate.nSecond < 10)
		tl23 += ("0")
	ENDIF
	tl23 += (aDate.nSecond)
	tl23 += (", ")
	tl23 += (aDate.nDay)
	tl23 += (" ")
	SWITCH (aDate.nMonth)
		CASE 1	tl23 += ("Jan") BREAK
		CASE 2	tl23 += ("Feb") BREAK
		CASE 3	tl23 += ("Mar") BREAK
		CASE 4	tl23 += ("Apr") BREAK
		CASE 5	tl23 += ("May") BREAK
		CASE 6	tl23 += ("Jun") BREAK
		CASE 7	tl23 += ("Jul") BREAK
		CASE 8	tl23 += ("Aug") BREAK
		CASE 9	tl23 += ("Sep") BREAK
		CASE 10	tl23 += ("Oct") BREAK
		CASE 11	tl23 += ("Nov") BREAK
		CASE 12	tl23 += ("Dec") BREAK
		DEFAULT
			tl23 += ("M_")
			tl23 += (aDate.nMonth)
		BREAK
	ENDSWITCH
	tl23 += (" ")
	tl23 += (aDate.nYear)
	
	#IF IS_DEBUG_BUILD
	RETURN GET_STRING_FROM_STRING(tl23, 0, GET_LENGTH_OF_LITERAL_STRING(tl23))
	#ENDIF
	#IF NOT IS_DEBUG_BUILD
	UNUSED_PARAMETER(tl23)
	RETURN ""
	#ENDIF
ENDFUNC

DEBUGONLY PROC NET_PRINT_UGC_DATE(UGC_DATE& aDate, NET_PRINT_LEVEL ePrintLevel = ePRINT_STD )

//	NET_NL()
//	NET_PRINT("Year = ", ePrintLevel)NET_PRINT_INT(aDate.Year, ePrintLevel)
//	NET_NL()
//	NET_PRINT("Month = ", ePrintLevel)NET_PRINT_INT(aDate.Month, ePrintLevel)
//	NET_NL()
//	NET_PRINT("Day = ", ePrintLevel)NET_PRINT_INT(aDate.Day, ePrintLevel)
//	NET_NL()
//	NET_PRINT("Hour = ", ePrintLevel)NET_PRINT_INT(aDate.Hour, ePrintLevel)
//	NET_NL()
//	NET_PRINT("Minute = ", ePrintLevel)NET_PRINT_INT(aDate.Minute, ePrintLevel)
//	NET_NL()
//	NET_PRINT("Seconds = ", ePrintLevel)NET_PRINT_INT(aDate.Seconds, ePrintLevel)

	NET_PRINT(NET_GET_STRING_UGC_DATE(aDate), ePrintLevel)

ENDPROC

DEBUGONLY FUNC PLAYER_INDEX private__GET_PLAYER_INDEX_FROM_HASH(INT iHash)
	PLAYER_INDEX tempPlayer
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		tempPlayer = INT_TO_PLAYERINDEX(i)
		IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
			IF iHash = NETWORK_HASH_FROM_PLAYER_HANDLE(tempPlayer)
				RETURN tempPlayer
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN INVALID_PLAYER_INDEX()	
ENDFUNC

DEBUGONLY FUNC STRING private__GET_PLAYER_NAME_FROM_HASH(INT iHash)
	PLAYER_INDEX tempPlayer
	tempPlayer = private__GET_PLAYER_INDEX_FROM_HASH(iHash)
	IF NOT (tempPlayer = INVALID_PLAYER_INDEX())
		RETURN GET_PLAYER_NAME(tempPlayer)
	ENDIF
	RETURN "invalid playerid for name"
ENDFUNC

DEBUGONLY PROC NET_PRINT_ENTITY_DETAILS(ENTITY_INDEX EntityID)
	NET_PRINT("Entity Details: Index = ") NET_PRINT_INT(NATIVE_TO_INT(EntityID))
	
	IF DOES_ENTITY_EXIST(EntityID)
		NET_PRINT(" : model ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(EntityID)))	
		IF IS_ENTITY_DEAD(EntityID)
			NET_PRINT(" : is dead")
		ELSE
			NET_PRINT(" : is NOT dead")
		ENDIF
		
		IF IS_ENTITY_A_PED(EntityID)
			PED_INDEX pedID = GET_PED_INDEX_FROM_ENTITY_INDEX(EntityID)
			NET_PRINT(" : is ped")
			IF IS_PED_INJURED(pedID)
				NET_PRINT(" : is injured") 
			ENDIF
			IF IS_PED_A_PLAYER(pedID)
				NET_PRINT(" : player ") NET_PRINT_INT(NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedID)))
				NET_PRINT(" : ") NET_PRINT(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedID)))
			ENDIF
		ENDIF
		
		IF IS_ENTITY_A_VEHICLE(EntityID)
			NET_PRINT(" : is vehicle")
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(EntityID))
				NET_PRINT(" : is driveable")
			ELSE
				NET_PRINT(" : is NOT driveable")
			ENDIF
			IF DECOR_EXIST_ON(EntityID, "Player_Vehicle")
				NET_PRINT(" : Player_Vehicle, hash = ") NET_PRINT_INT(DECOR_GET_INT(EntityID, "Player_Vehicle"))
				NET_PRINT("(") NET_PRINT(private__GET_PLAYER_NAME_FROM_HASH(DECOR_GET_INT(EntityID, "Player_Vehicle"))) NET_PRINT(")")
			ENDIF
			IF DECOR_EXIST_ON(EntityID, "Previous_Owner")
				NET_PRINT(" : Previous_Owner, hash = ") NET_PRINT_INT(DECOR_GET_INT(EntityID, "Previous_Owner"))
				NET_PRINT("(") NET_PRINT(private__GET_PLAYER_NAME_FROM_HASH(DECOR_GET_INT(EntityID, "Previous_Owner"))) NET_PRINT(")")
			ENDIF
			IF DECOR_EXIST_ON(EntityID, "MPBitset")
				NET_PRINT(" : MPBitset = ")  NET_PRINT_INT(DECOR_GET_INT(EntityID, "MPBitset"))
			ENDIF
		ENDIF					
	ELSE
		NET_PRINT(" : does not exist!") 
	ENDIF
ENDPROC
