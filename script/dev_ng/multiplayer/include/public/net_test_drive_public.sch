///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        net_test_drive_public.sch																			///																			
///																													///				
/// Description:  Public functions used for test drving vehicles. 													///
///																													///
/// Written by:  Joe Davis																							///		
/// Date:		04/04/2022																							///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_include.sch"
USING "net_realty_new.sch"
USING "net_test_drive_vars.sch"
USING "fmmc_header.sch"

#IF FEATURE_DLC_1_2022

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Main prefix for test drive flow, used for debug prints across launching and test drive script.
FUNC STRING TD_PRINT_LABEL()
	RETURN "[NET_TEST_DRIVE] "
ENDFUNC

#ENDIF // #IF IS_DEBUG_BUILD

/// PURPOSE:
///    Detects when the local players ped is near a given vehicle to test drive.
FUNC BOOL IS_LOCAL_PLAYER_NEAR_VEHICLE(VEHICLE_INDEX vehToCheck, FLOAT fRange = cfNEAR_TEST_DRIVE_VEHICLE_RANGE_DEFAULT, BOOL bIncludeFrontAndRear = TRUE, BOOL bCheckHeading = TRUE)
	IF IS_ENTITY_ALIVE(vehToCheck)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		
		// --- Heading ---
		IF bCheckHeading
			VECTOR vTargetHeadingCoord = GET_ENTITY_COORDS(vehToCheck)
			VECTOR vPedCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
			FLOAT fPedHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
			FLOAT fDirHeading = GET_HEADING_BETWEEN_VECTORS_2D(vPedCoord, vTargetHeadingCoord)
			
			IF NOT IS_HEADING_ACCEPTABLE_CORRECTED(fDirHeading, fPedHeading, 80.0)
				RETURN FALSE
			ENDIF
		ENDIF
		
		// --- Near door ---
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(vehToCheck, FALSE)) < fRange
			RETURN TRUE
		ENDIF
		
		// --- Near surrounding vehicle ---
		IF bIncludeFrontAndRear
			MODEL_NAMES eModel = GET_ENTITY_MODEL(vehToCheck)
			
			FLOAT fWidth = GET_MODEL_WIDTH(eModel)
			FLOAT fLength = GET_MODEL_LENGTH(eModel)
			FLOAT fHeight = GET_MODEL_HEIGHT(eModel)
			FLOAT fPlayerHeight = GET_MODEL_HEIGHT(GET_PLAYER_MODEL())
			
			VECTOR vFront = ZERO_VECTOR() + <<0, (fLength/1.5), (fPlayerHeight/1.5)>>
			VECTOR vBack = ZERO_VECTOR() - <<0, (fLength/1.5), (fHeight/2)>>
			
			VECTOR vFrontFinal = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehToCheck, vFront)
			VECTOR vBackFinal = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehToCheck, vBack)
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vFrontFinal, vBackFinal, (fWidth + (fWidth/2)))
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Wrapper which returns the name of the script which controls the test drive functionality. 
FUNC STRING GET_TEST_DRIVE_SCRIPT_NAME()
	RETURN "net_test_drive"
ENDFUNC

/// PURPOSE:
///    Contains various, basic checks to determine if the player can start the test drive sequence.
/// RETURNS:
///    TRUE if none of the conditions are met.
FUNC BOOL DO_BASIC_START_TEST_DRIVE_CHECKS()
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] INVALID_PLAYER_INDEX.")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iBS, PROPERTY_BROADCAST_BS_I_AM_REQUESTING_TO_USE_PERSONAL_CAR_MOD)
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] PROPERTY_BROADCAST_BS_I_AM_REQUESTING_TO_USE_PERSONAL_CAR_MOD is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_PLAYER_SWITCH_IN_PROGRESS is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_SELECTOR_ONSCREEN()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_SELECTOR_ONSCREEN is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF PROPERTY_HAS_JUST_ACCEPTED_A_MISSION()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] PROPERTY_HAS_JUST_ACCEPTED_A_MISSION is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE_EX()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_PAUSE_MENU_ACTIVE_EX is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_COMMERCE_STORE_OPEN()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_COMMERCE_STORE_OPEN is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_CELLPHONE_CAMERA_IN_USE()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_CELLPHONE_CAMERA_IN_USE is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] FALSE - Browser is open.")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] FALSE - Browser is open.")
		RETURN FALSE
	ENDIF
	
	IF SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_SCREEN_FADED_IN is FALSE.")
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_INTERACTION_MENU_OPEN is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID(), TRUE)
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_BEAST(PLAYER_ID())
		PRINTLN("[DO_BASIC_START_TEST_DRIVE_CHECKS] IS_PLAYER_BEAST is TRUE.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Delete all assets created, and clears various flags set in PLAY_ENTER_VEHICLE_CUTSCENE.
/// PARAMS:
///    sData - Data struct
///    vehToEnter - Vehicle to enter.
PROC CLEANUP_ENTER_VEHICLE_CUTSCENE(VEHICLE_TEST_DRIVE_CUTSCENE_DATA &sData, VEHICLE_INDEX &vehToEnter)
	IF sData.eState >= VTDCS_CREATE_ASSETS
		
		IF DOES_ENTITY_EXIST(sData.pedPlayerClone)
			DELETE_PED(sData.pedPlayerClone)
		ENDIF
		
		IF DOES_ENTITY_EXIST(sData.sCutsceneVeh.vehicle)
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), sData.sCutsceneVeh.vehicle)
			AND NOT IS_PED_PERFORMING_SCRIPT_TASK(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE)
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), DEFAULT, ECF_WARP_PED)
			ENDIF
			
			DELETE_VEHICLE(sData.sCutsceneVeh.vehicle)
		ENDIF
		
		IF DOES_CAM_EXIST(sData.camera)
			IF IS_CAM_ACTIVE(sData.camera)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_CAM_ACTIVE(sData.camera, FALSE)
			ENDIF
			
			DESTROY_CAM(sData.camera)
		ENDIF
		
		IF IS_ENTITY_ALIVE(vehToEnter)
			SET_ENTITY_VISIBLE(vehToEnter, TRUE)
			SET_ENTITY_COLLISION(vehToEnter, TRUE)
			FREEZE_ENTITY_POSITION(vehToEnter, FALSE)
			IF NETWORK_GET_ENTITY_IS_NETWORKED(vehToEnter)
				SET_ENTITY_VISIBLE_IN_CUTSCENE(vehToEnter, TRUE, TRUE)
			ENDIF
		ENDIF
		
		CLEANUP_MP_CUTSCENE()
		PRINTLN(TD_PRINT_LABEL(), "CLEANUP_ENTER_VEHICLE_CUTSCENE - Cleaned up. sData.eState = VTDCS_DEFAULT")
		sData.eState = VTDCS_DEFAULT
	ENDIF
ENDPROC

/// PURPOSE:
///    Points the script camera at the vehicle the player is entering. 
/// PARAMS:
///    sData - Data struct
///    cutsceneVeh - Vehicle created for cutscene. 
PROC SET_ENTER_VEHICLE_CAMERA_PARAMS(VEHICLE_TEST_DRIVE_CUTSCENE_DATA &sData, VEHICLE_INDEX cutsceneVeh)
	FLOAT fGroundZ
	VECTOR vVehCoords = GET_ENTITY_COORDS(cutsceneVeh)
	FLOAT fDefaultYOffset = -5.0
	FLOAT fDefaultZOffset = 1.3
	VECTOR vCamPos
	VECTOR vDistanceFromVeh 	= <<0, 4.5, 0>>
	FLOAT fMaxFOV 				= 42.5
	FLOAT fStartHeadingOffset	= 30.0
	FLOAT fNearDOF 				= 0.15
	FLOAT fFarDOF 				= 10.0
	FLOAT fShakeAmp 			= 0.1
	FLOAT fDOFStrength 			= 0.88
	
	FLOAT fHeading = (GET_ENTITY_HEADING(cutsceneVeh) + fStartHeadingOffset) // Start with a cool side view of vehicle.
	fHeading = NORMALIZE_ANGLE(fHeading)
	
	GET_GROUND_Z_FOR_3D_COORD(vVehCoords, fGroundZ)
	
	vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vVehCoords, (fHeading + fDefaultYOffset), vDistanceFromVeh)
	vCamPos.z = fGroundZ + fDefaultZOffset
	
	SET_CAM_COORD(sData.camera, vCamPos)
	POINT_CAM_AT_COORD(sData.camera, vVehCoords)
	
	SET_CAM_USE_SHALLOW_DOF_MODE(sData.camera, TRUE)
	SET_CAM_NEAR_DOF(sData.camera, fNearDOF)
	SET_CAM_FAR_DOF(sData.camera, fFarDOF)
	SET_CAM_DOF_STRENGTH(sData.camera, fDOFStrength)
	SET_CAM_FOV(sData.camera, fMaxFOV)
	SHAKE_CAM(sData.camera, "HAND_SHAKE", fShakeAmp)
ENDPROC

/// PURPOSE:
///    Plays a cutscene of the local player entering a vehicle. 
/// PARAMS:
///    sData - Cutscene data struct
///    vehToEnter - Vehicle to enter.
/// RETURNS:
///    TRUE when the cutscene has finished.
FUNC BOOL PLAY_ENTER_VEHICLE_CUTSCENE(VEHICLE_TEST_DRIVE_CUTSCENE_DATA &sData, VEHICLE_INDEX &vehToEnter, BOOL bFadeOutScreen)
	// Clean up and return true if not safe to start.
	IF sData.eState != VTDCS_CLEANUP
	AND NOT IS_PLAYER_OK_TO_START_MP_CUTSCENE()
		PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Not safe to start cutscene. sData.eState = VTDCS_CLEANUP")
		sData.eState = VTDCS_CLEANUP
	ENDIF
	
	VEHICLE_SETUP_STRUCT_MP vehicleSetup
	INT iOn, iFullBeam
				
	// Main state
	SWITCH sData.eState
		CASE VTDCS_DEFAULT
			PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - sData.eState = VTDCS_CREATE_ASSETS")
			sData.eState = VTDCS_CREATE_CLONE
			RETURN FALSE
		BREAK
		
		CASE VTDCS_CREATE_CLONE
			IF NOT IS_ENTITY_ALIVE(vehToEnter)
			OR NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
				ASSERTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Vehicle / player is dead? Ending cutscene.")
				sData.eState = VTDCS_CLEANUP
				RETURN FALSE
			ENDIF
			
			// player clone
			IF NOT DOES_ENTITY_EXIST(sData.pedPlayerClone)
				IF CUTSCENE_HELP_CLONE_PED(sData.pedPlayerClone, PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(sData.pedPlayerClone, TRUE)
					SET_ENTITY_COLLISION(sData.pedPlayerClone, FALSE)
					SET_ENTITY_VISIBLE(sData.pedPlayerClone, FALSE)
					SET_ENTITY_COORDS(sData.pedPlayerClone, GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<0.0,0.0,10.0>>)
					SET_ENTITY_HEADING(sData.pedPlayerClone, GET_ENTITY_HEADING(PLAYER_PED_ID()))
					SET_ENTITY_INVINCIBLE(sData.pedPlayerClone, TRUE)
					
					PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Clone created")
					sData.eState = VTDCS_CREATE_ASSETS
				ENDIF
				RETURN FALSE
			ENDIF
		BREAK

		CASE VTDCS_CREATE_ASSETS
			// Bail if vehToEnter is dead,
			IF NOT IS_ENTITY_ALIVE(vehToEnter)
			OR NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
				ASSERTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Vehicle / player is dead? Ending cutscene.")
				sData.eState = VTDCS_CLEANUP
				RETURN FALSE
			ENDIF
			
			// Cutscene Vehicle
			IF NOT DOES_ENTITY_EXIST(sData.sCutsceneVeh.vehicle)
				IF CREATE_VEHICLE_CLONE(sData.sCutsceneVeh.vehicle, vehToEnter, GET_ENTITY_COORDS(vehToEnter), GET_ENTITY_HEADING(vehToEnter), FALSE, FALSE)
					
					GET_VEHICLE_SETUP_MP(vehToEnter, vehicleSetup)
					SET_VEHICLE_SETUP_MP(sData.sCutsceneVeh.vehicle, vehicleSetup)
					VEHICLE_COPY_EXTRAS(vehToEnter, sData.sCutsceneVeh.vehicle)
					SET_ENTITY_COLLISION(sData.sCutsceneVeh.vehicle, FALSE)
					SET_ENTITY_VISIBLE(sData.sCutsceneVeh.vehicle, FALSE)
					SET_ENTITY_INVINCIBLE(sData.sCutsceneVeh.vehicle, TRUE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(sData.sCutsceneVeh.vehicle, FALSE)
					SET_VEHICLE_ON_GROUND_PROPERLY(sData.sCutsceneVeh.vehicle)
					FREEZE_ENTITY_POSITION(sData.sCutsceneVeh.vehicle, TRUE)
					SET_ENTITY_COORDS_NO_OFFSET(sData.sCutsceneVeh.vehicle, GET_ENTITY_COORDS(vehToEnter))
					SET_ENTITY_HEADING(sData.sCutsceneVeh.vehicle, GET_ENTITY_HEADING(vehToEnter))
					SET_VEHICLE_RADIO_ENABLED(sData.sCutsceneVeh.vehicle, FALSE)
					SET_VEHICLE_DIRT_LEVEL (sData.sCutsceneVeh.vehicle, 0.0)
					
					//LIGHTS
					GET_VEHICLE_LIGHTS_STATE(vehToEnter, iOn, iFullBeam)
					IF iOn != 0
						SET_VEHICLE_LIGHTS(sData.sCutsceneVeh.vehicle, FORCE_VEHICLE_LIGHTS_ON)
					ENDIF
					
					IF iFullBeam != 0
						SET_VEHICLE_FULLBEAM(sData.sCutsceneVeh.vehicle, TRUE)
					ENDIF
					
					SET_VEHICLE_ENGINE_ON(sData.sCutsceneVeh.vehicle, FALSE, TRUE)
					CUTSCENE_HELP_CLONE_VEH_TRAILER(vehToEnter, sData.sCutsceneVeh.trailer)
					CUTSCENE_HELP_DISABLE_VEHICLE_AUTOMATIC_CRASH(sData.sCutsceneVeh.vehicle)
				ENDIF
				
				RETURN FALSE
			ENDIF
			
			// Camera
			IF NOT DOES_CAM_EXIST(sData.camera)
				sData.camera = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
				PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Camera created!")
			ENDIF
			
			PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - sData.eState = VTDCS_UPDATE")
			sData.eState = VTDCS_UPDATE
			RETURN FALSE
		BREAK
		CASE VTDCS_UPDATE
			IF IS_ENTITY_ALIVE(sData.sCutsceneVeh.vehicle)
			AND IS_ENTITY_ALIVE(sData.pedPlayerClone)
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				
				// First frame.
				IF NOT MPGlobals.bStartedMPCutscene
				AND IS_ENTITY_ALIVE(vehToEnter)
				
					// Player
					MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE)
					
					// Test drive Vehicle
					FREEZE_ENTITY_POSITION(vehToEnter, TRUE)
					SET_ENTITY_COLLISION(vehToEnter, FALSE)
					
					IF NETWORK_GET_ENTITY_IS_NETWORKED(vehToEnter)
						SET_ENTITY_VISIBLE_IN_CUTSCENE(vehToEnter, FALSE, TRUE)
					ELSE
						SET_ENTITY_VISIBLE(vehToEnter, FALSE)
					ENDIF
					
					// Cutscene Vehicle
					SET_ENTITY_COLLISION(sData.sCutsceneVeh.vehicle, TRUE)
					SET_ENTITY_VISIBLE(sData.sCutsceneVeh.vehicle, TRUE)
					FREEZE_ENTITY_POSITION(sData.sCutsceneVeh.vehicle, FALSE)
					SET_VEHICLE_ON_GROUND_PROPERLY(sData.sCutsceneVeh.vehicle)
					
					// ped
					SET_ENTITY_COLLISION(sData.pedPlayerClone, TRUE)
					SET_ENTITY_VISIBLE(sData.pedPlayerClone, TRUE)
					FREEZE_ENTITY_POSITION(sData.pedPlayerClone, FALSE)
					SET_ENTITY_COORDS_NO_OFFSET(sData.pedPlayerClone, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					
					// Camera
					IF DOES_CAM_EXIST(sData.camera)
					AND NOT IS_CAM_ACTIVE(sData.camera)
						SET_ENTER_VEHICLE_CAMERA_PARAMS(sData, sData.sCutsceneVeh.vehicle)
						SET_CAM_ACTIVE(sData.camera, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
					
					// Start Cutscene
					START_MP_CUTSCENE(TRUE)
					REINIT_NET_TIMER(sData.sBailTimer)
					PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Starting cutscene.")
				ENDIF
				
				// Every frame
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				DISABLE_FRONTEND_THIS_FRAME()
				SET_PED_RESET_FLAG(sData.pedPlayerClone, PRF_IgnoreVehicleEntryCollisionTests, TRUE)
				//SET_PED_RESET_FLAG(sData.pedPlayerClone, PRF_SuppressNavmeshForEnterVehicleTask, TRUE)
				//SET_PED_RESET_FLAG(sData.pedPlayerClone, PRF_UseTighterEnterVehicleSettings, TRUE)
				
				// End the cutscene if player is taking too long to enter vehicle. 
				IF HAS_NET_TIMER_STARTED_AND_EXPIRED(sData.sBailTimer, 10000)
					PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Bail timer expired. Forcing cleanup. sData.eState = VTDCS_CLEANUP")
					sData.eState = VTDCS_CLEANUP
				ENDIF
				
				IF NOT IS_PED_SITTING_IN_THIS_VEHICLE(sData.pedPlayerClone, sData.sCutsceneVeh.vehicle)
					IF NOT IS_PED_PERFORMING_SCRIPT_TASK(sData.pedPlayerClone, SCRIPT_TASK_ENTER_VEHICLE)
						PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Tasking ped to enter vehicle.")
						ENTER_EXIT_VEHICLE_FLAGS eFlags
						
						IF IS_PED_ON_VEHICLE(sData.pedPlayerClone)
							PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - On top of vehicle. Warping to entry point.")
							eFlags = ECF_RESUME_IF_INTERRUPTED | ECF_WARP_ENTRY_POINT
						ELSE
							eFlags = ECF_RESUME_IF_INTERRUPTED
						ENDIF
						
						TASK_ENTER_VEHICLE(sData.pedPlayerClone, sData.sCutsceneVeh.vehicle, 10000, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, eFlags)
					ENDIF
				ELSE
					IF GET_IS_VEHICLE_ENGINE_RUNNING(sData.sCutsceneVeh.vehicle)
						PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Player is in vehicle and engine is running! sData.eState = VTDCS_CLEANUP")
						sData.eState = VTDCS_CLEANUP
					ELSE
						PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Turning on engine.")
						SET_VEHICLE_ENGINE_ON(sData.sCutsceneVeh.vehicle, TRUE, FALSE)
					ENDIF
				ENDIF
			ELSE
				PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Cutscene vehicle / player died. sData.eState = VTDCS_CLEANUP")
				sData.eState = VTDCS_CLEANUP
			ENDIF
			
			RETURN FALSE
		BREAK
		CASE VTDCS_CLEANUP
			IF bFadeOutScreen
				IF IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bFadeOutScreen
		IF IS_SCREEN_FADED_OUT()
			PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Cutscene ended. (Screen faded out)")
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN(TD_PRINT_LABEL(), "PLAY_ENTER_VEHICLE_CUTSCENE - Cutscene ended.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#ENDIF // #IF FEATURE_DLC_1_2022

