USING "globals.sch"

#IF IS_DEBUG_BUILD
	
STRUCT DEBUG_VEHICLE_GENERATOR_DATA
	VECTOR vMin
	VECTOR vMax
	
	BOOL bRenderBoundingVolume = FALSE
	BOOL bDisableVehicleGenerator = FALSE
	BOOL bSyncOverNetwork = FALSE
	BOOL bOutputDataToFile = FALSE
ENDSTRUCT

PROC DEBUG_VEHICLE_GENERATOR_WIDGET(DEBUG_VEHICLE_GENERATOR_DATA &data)

	START_WIDGET_GROUP("Vehicle Generators")
		
		ADD_WIDGET_FLOAT_SLIDER("Min X: ", data.vMin.X, -9999.9, 9999.9, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Min Y: ", data.vMin.Y, -9999.9, 9999.9, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Min Z: ", data.vMin.Z, -9999.9, 9999.9, 0.1)
		
		ADD_WIDGET_FLOAT_SLIDER("Max X: ", data.vMax.X, -9999.9, 9999.9, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Max Y: ", data.vMax.Y, -9999.9, 9999.9, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Max Z: ", data.vMax.Z, -9999.9, 9999.9, 0.1)
		
		ADD_WIDGET_BOOL("Render bounding volume.", data.bRenderBoundingVolume)
		ADD_WIDGET_BOOL("Disable active vehicle generator(s) in area.", data.bDisableVehicleGenerator)
		ADD_WIDGET_BOOL("Sync vehicle generator(s) state over network.", data.bSyncOverNetwork)
		ADD_WIDGET_BOOL("Output bounding volume data to file.", data.bOutputDataToFile)
		
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_VEHICLE_GENERATOR_DEBUG_WIDGET(DEBUG_VEHICLE_GENERATOR_DATA &data)

	IF data.bRenderBoundingVolume
		DRAW_BOX(data.vMin, data.vMax, 0, 255, 0, 64)
	ENDIF

	IF data.bDisableVehicleGenerator
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(data.vMin, data.vMax, FALSE, data.bSyncOverNetwork)
	ELSE
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(data.vMin, data.vMax, TRUE, data.bSyncOverNetwork)
	ENDIF
	
	IF data.bOutputDataToFile
		SAVE_STRING_TO_DEBUG_FILE("vMin: ")
		SAVE_VECTOR_TO_DEBUG_FILE(data.vMin)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("vMax: ")
		SAVE_VECTOR_TO_DEBUG_FILE(data.vMax)
		
		data.bOutputDataToFile = FALSE
	ENDIF
ENDPROC
	
#ENDIF
