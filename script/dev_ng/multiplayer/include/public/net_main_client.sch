USING "rage_builtins.sch"
USING "globals.sch"

USING "net_reset_MP_Globals.sch"

USING "net_comms.sch"
USING "net_comms_EOM.sch"
USING "net_comms_stored.sch"
USING "net_comms_txtmsg.sch"

USING "net_mission_trigger.sch"
USING "net_mission_control.sch"
USING "net_mission_details_overlay.sch"
USING "net_mission_joblist.sch"

USING "net_objective_text.sch"
USING "net_player_headshots.sch"
USING "net_ambient_manager.sch"

USING "net_missions_at_coords.sch"
USING "net_mission_locate_message.sch"
USING "net_activity_selector.sch"
USING "net_missions_shared.sch"

USING "net_visible_player.sch"
USING "net_ticker_UI.sch"
USING "net_spawn.sch"
USING "net_process_events.sch"
USING "net_xp_animation.sch"
USING "net_big_message.sch"
USING "Net_Entity_Icon_Events.sch"
USING "net_near_players.sch"
USING "net_friend_request.sch"
USING "net_active_players.sch"
USING "net_blips.sch"
USING "net_car_boost.sch"
USING "net_crew_updates.sch"
USING "net_interactions.sch"
USING "net_chat.sch"
USING "net_stats_heavily_accessed.sch"

#IF IS_DEBUG_BUILD
	USING "net_debug_log.sch"
#ENDIF

	USING "net_spawn_group.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   Net_Main_Client.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all client-only functionality that is shared
//								between all game modes: FreeRoam, etc.
//		NOTES			:	Over this will fill out to contain all shared functionality.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// ===========================================================================================================
//      The Gamemode-Independent Main Client-Only Debug Routines
// ===========================================================================================================

// PURPOSE:	Generic client-only debug widgets
//
// INPUT PARAMS:		paramParentWidget				[DEFAULT=NULL] The widget group that this needs to be added to
#IF IS_DEBUG_BUILD
PROC Create_Main_Generic_Client_Widgets(WIDGET_GROUP_ID paramParentWidget = NULL)

	// Add these to the correct widget group
	IF (DOES_WIDGET_GROUP_EXIST(paramParentWidget))
		SET_CURRENT_WIDGET_GROUP(paramParentWidget)
	ENDIF

	// Communications System
	Create_MP_End_Of_Mission_Widgets()
	Create_MP_Stored_Communication_Widgets()
	Create_MP_Communications_Widgets()
	
	// Mission Helper Systems
	Create_MP_Objective_Text_Widgets()
	
	// Mission Control Systemp (Client Routines)
	Create_MP_Mission_Control_Client_Widgets()
	
	// Activity Selector Widgets
	Create_MP_Activity_Selector_Widgets()
	
	// Debug Log Widgets
	Create_MP_Debug_Log_Widgets()
	
	Create_Visible_Player_Widget()
	
	// blips
	ADD_BLIP_WIDGETS()	
	
	ADD_TICKER_WIDGETS()
	
	ADD_SPAWNING_WIDGETS()
	
	ADD_CREW_INFO_WIDGET()
	
	// Unset the widget group again
	IF (DOES_WIDGET_GROUP_EXIST(paramParentWidget))
		CLEAR_CURRENT_WIDGET_GROUP(paramParentWidget)
	ENDIF
	
	CREATE_KILL_STRIP_WIDGETS()
	
	CREATE_BIG_MESSAGE_WIDGETS()
	
	// add widget for near players
	CreateNearPlayersWidget()
	
	CREATE_VEHICLE_BOOST_WIDGET()
	
	CREATE_INTERACTION_ANIM_WIDGET()
	
	CREATE_CHAT_WIDGET()

	
ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Destroy any client-only debug widgets
#IF IS_DEBUG_BUILD
PROC Destroy_Main_Generic_Client_Widgets()

	// Communications System
	Destroy_MP_End_Of_Mission_Widgets()
	Destroy_MP_Stored_Communication_Widgets()
	Destroy_MP_Communications_Widgets()
	
	// Mission Helper Systems
	Destroy_MP_Objective_Text_Widgets()
	
ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD	
PROC Maintain_Main_Generic_Debug()

	UPDATE_SPAWNING_WIDGETS()
	
	UPDATE_KILL_STRIP_WIDGETS()
	
	UPDATE_VEHICLE_BOOST_WIDGET()

	UPDATE_CHAT_OVERRIDE()

	
ENDPROC
#ENDIF


PROC Cleanup_Main_Generic_Client()

	// cleanup any dangling teleport
	IF IS_PLAYER_TELEPORT_ACTIVE()
		STOP_PLAYER_TELEPORT()
	ENDIF

	// cleanup any searches left dangling.
		
	NETWORK_CANCEL_RESPAWN_SEARCH()	
	SPAWNPOINTS_CANCEL_SEARCH()
	
	// clear the feed
	THEFEED_FLUSH_QUEUE()
	 g_b_ReapplyStickySaveFailedFeed = FALSE
	 
	// clear any existing player blips (if player is swapping team for instance)
	CLEANUP_ALL_PLAYER_BLIPS()
	
	//need to be done bfore data reset.
	CLEANUP_MP_SAVED_VEHICLE()	
	
	CLEANUP_MP_SAVED_TRUCK()
	
	CLEANUP_MP_SAVED_AVENGER()
	
	CLEANUP_MP_SAVED_HACKERTRUCK()
	
	#IF FEATURE_DLC_2_2022
	CLEANUP_MP_SAVED_ACIDLAB()
	#ENDIF
	
	RESET_PLAYER_ARROW_TO_WHITE()
	
	// Mission Locate Message System - for the mission names that appear in the middle of a corona
	Terminate_MP_Locate_Messages()
	
	// MP Player Headshots system
	Terminate_MP_Player_Headshots()
	
	// MP Mission Triggering
	Terminate_MP_Mission_Triggering()
	
	// MP Joblist
	Terminate_MP_Joblist_Update()
	
	// Missions At Coords System
	IF GET_CURRENT_GAMEMODE() != GAMEMODE_MPTESTBED
		Terminate_MP_Missions_At_Coords()
	ENDIF
	
	IF IS_TRANSITION_SESSION_LAUNCHING()
		g_sTransitionSessionData.sTSPBvars.currentMissionDataBitSetTransition 	= GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet
		g_sTransitionSessionData.sTSPBvars.eCoronaStatusTransition	 			= GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].eCoronaStatus
		PRINTLN("[TS] Cleanup_Main_Generic_Client - Backing up player broadcast data")
		PRINTLN("[TS] g_sTransitionSessionData.sTransVars.sTSPBvars.currentMissionDataBitSetTransition = ", g_sTransitionSessionData.sTSPBvars.currentMissionDataBitSetTransition)
		PRINTLN("[TS] g_sTransitionSessionData.sTransVars.sTSPBvars.eCoronaStatusTransition            = ", ENUM_TO_INT(g_sTransitionSessionData.sTSPBvars.eCoronaStatusTransition))
	ENDIF
	
	// clear all the globals
	CLEAR_ALL_GLOBAL_BROADCAST_DATA(TRUE)

ENDPROC

PROC RESET_playerSCTVTargetTO_INVALID()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		GlobalplayerBD_FM[i].playerSCTVTarget = INT_TO_NATIVE(PLAYER_INDEX, -1)
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === RESET_playerSCTVTargetTO_INVALID")
	#ENDIF
ENDPROC



// ===========================================================================================================
//      The Gamemode-Independent Main Client-Only Routines
// ===========================================================================================================
// PURPOSE:	A one-off generic client-only initialisation.
PROC Initialise_Main_Generic_Client()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: INITIALISE_MAIN_GENERIC_CLIENT") NET_NL()
	#ENDIF
	
	EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
	
	IF NOT IS_TRANSITION_SESSION_LAUNCHING()
	AND NOT SHOULD_SKIP_WAIT_FOR_TRANSITION_CAMERA()
	AND NOT SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
	AND NOT SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
		// Set the Global Instance Priority (this determines which objects get rendered) - MP should only render the most important object in all modes
		SET_INSTANCE_PRIORITY_MODE(INSTANCE_MODE_MULTIPLAYER)
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[TS] Initialise_Main_Generic_Client - SET_GLOBAL_INSTANCE_PRIORITY_MP not called!")
	#ENDIF
	ENDIF
	
	TOGGLE_STEALTH_RADAR(TRUE) // so radar will show sound that player makes
	SET_PLAYER_NOISE_MULTIPLIER(PLAYER_ID(), 1.0)

	// Communications System
	Initialise_MP_End_Of_Mission_Message()
	Initialise_MP_Stored_Communication()
	Initialise_MP_Txtmsg()
	Initialise_MP_Communications()
	
	// Mission Triggering System
	Initialise_MP_Mission_Triggering()
	Initialise_MP_Mission_Details_Overlay()
	
	// Mission Helper Systems
	Initialise_MP_Objective_Text()
	
	// Mission Control System - Client-side
	Initialise_MP_Mission_Control_Client()
	
	// Missions At Coords System
	IF GET_CURRENT_GAMEMODE() != GAMEMODE_MPTESTBED
		Initialise_MP_Missions_At_Coords()
	ENDIF
	
	// Activity Selector System - Client-side
	Initialise_MP_Activity_Selector_Client()
	
	// Missions Shared System
	Initialise_MP_Missions_Shared_Client()
	
	// Mission Locate Message System - for the mission names that appear in the middle of a corona
	Initialise_MP_Locate_Messages()
	
	// Player Headshots System
	Initialise_MP_Player_Headshots()
	
	// Joblist Updating
	Initialise_MP_Joblist_Update()
	
	// make sure the players arrow is white
	RESET_PLAYER_ARROW_TO_WHITE()
	
	// maker sure all the near players are set to invalid to start with
	Init_Near_Players_Array()
	
	// update the players crew info, only need to get called this one time because they quit out if they change crew.
	UPDATE_LOCAL_PLAYER_GLOBAL_BD_CREW_DATA()
	
	//Turn off creator functionality
	USING_MISSION_CREATOR(FALSE)
	ALLOW_MISSION_CREATOR_WARP(FALSE)

// DEBUG ONLY - Debug Log
	#IF IS_DEBUG_BUILD
		Initialise_MP_Debug_Log()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC find_next_non_spectating_participant()
	
	INT iParticipant
	PARTICIPANT_INDEX ParticipantID
	PLAYER_INDEX PlayerID
	INT i
		
	FOR i=1 TO NUM_NETWORK_PLAYERS
	
		iParticipant = g_iStaggeredParticipant + i
		IF (iParticipant >= NUM_NETWORK_PLAYERS)
			iParticipant -= NUM_NETWORK_PLAYERS
		ENDIF
		
		ParticipantID = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(ParticipantID)
			PlayerID = NETWORK_GET_PLAYER_INDEX(ParticipantID)
			IF NOT IS_PLAYER_SCTV(PlayerID)
				g_iStaggeredParticipant_LastFrame = g_iStaggeredParticipant
				g_iStaggeredParticipant = iParticipant	
				g_StaggeredParticipant = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
				g_StaggeredPlayer = PlayerID
				//NET_PRINT("g_StaggeredPlayer = ") NET_PRINT_INT(NATIVE_TO_INT(g_StaggeredPlayer)) NET_NL()
				EXIT
			ENDIF
		ENDIF	
			
	ENDFOR
	
ENDPROC

PROC find_next_staggered_participant()
	
	INT iParticipant

	iParticipant = g_iStaggeredParticipant + 1
	IF (iParticipant >= NUM_NETWORK_PLAYERS)
		iParticipant -= NUM_NETWORK_PLAYERS
	ENDIF
	
	g_iStaggeredParticipant_LastFrame = g_iStaggeredParticipant
	g_iStaggeredParticipant = iParticipant	
	g_StaggeredParticipant = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
	g_StaggeredPlayer = INT_TO_NATIVE(PLAYER_INDEX, iParticipant)

ENDPROC

// PURPOSE:	Checks if the passed player is trying to affect the local player
PROC PROCESS_PHONE_CALL_SPECIAL_ABILITY_REACTIONS(PLAYER_INDEX PlayerID)
	IF IS_NET_PLAYER_OK(PlayerID, FALSE)																		//Stagger player exists
		INT iPlayer = NATIVE_TO_INT(PlayerID)
		IF GlobalplayerBD[iPlayer].iWantedLevelTargetPlayer <> -1												//Has flagged looking for someone
			IF GlobalplayerBD[iPlayer].iWantedLevelTargetAmount <> -1											//Has flagged desire to increase wanted
				IF GlobalplayerBD[iPlayer].iWantedLevelTargetPlayer = NATIVE_TO_INT(PLAYER_ID())				//It's me!
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < GlobalplayerBD[iPlayer].iWantedLevelTargetAmount	//My wanted level is less than desired
						
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), GlobalplayerBD[iPlayer].iWantedLevelTargetAmount)	//Set wanted level
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						NET_PRINT("PROCESS_PHONE_CALL_SPECIAL_ABILITY_REACTIONS - given wanted level of ") NET_PRINT_INT(GlobalplayerBD[iPlayer].iWantedLevelTargetAmount) NET_NL()
						BROADCAST_TELL_SERVER_CONTACT_REQUEST_COMPLETE(ALL_PLAYERS(),REQUEST_GIVE_WANTED,PLAYER_ID())

					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// PURPOSE: Checks various flags to decide whether AI should be blocked from targetting the player this frame
PROC PROCESS_LOCAL_PLAYER_TARGETTED_BY_AI()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PLAYER_SPECTATING(PLAYER_ID())
		OR IS_PLAYER_SWITCH_IN_PROGRESS()
		OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_CannotBeTargetedByAI, TRUE)
			IF NOT GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRequiresCannotBeTargetedByAI
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRequiresCannotBeTargetedByAI = TRUE
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== AI TARGET === GlobalplayerBD[", NATIVE_TO_INT(PLAYER_ID()), "].bRequiresCannotBeTargetedByAI = TRUE")
				#ENDIF
			ENDIF
		ELSE
			IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRequiresCannotBeTargetedByAI
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRequiresCannotBeTargetedByAI = FALSE
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== AI TARGET === GlobalplayerBD[", NATIVE_TO_INT(PLAYER_ID()), "].bRequiresCannotBeTargetedByAI = FALSE")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC StorePlayersInSameSession(PLAYER_INDEX PlayerID)
	IF IS_NET_PLAYER_OK(PlayerID, FALSE)
		IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PlayerID, PLAYER_ID())
			SET_BIT(MPGlobals.iBS_WereInSameSession, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(MPGlobals.iBS_WereInSameSession, NATIVE_TO_INT(PlayerID))	
		ENDIF
	ENDIF
ENDPROC

PROC Process_client_shared_staggered_and_non_staggered_functions(PLAYER_INDEX PlayerID)

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("Process_client_shared_staggered_and_non_staggered_functions")
	#ENDIF
	#ENDIF

	IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)			
		PROCESS_SEND_FRIEND_REQUEST(playerID)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_SEND_FRIEND_REQUEST")
		#ENDIF
		#ENDIF
	ENDIF
	
	PROCESS_PHONE_CALL_SPECIAL_ABILITY_REACTIONS(PlayerID)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("PROCESS_PHONE_CALL_SPECIAL_ABILITY_REACTIONS")
	#ENDIF
	#ENDIF
	
	PROCESS_LOCAL_PLAYER_TARGETTED_BY_AI()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("PROCESS_LOCAL_PLAYER_TARGETTED_BY_AI")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF

ENDPROC

PROC RENDER_MP_INTRO_CREDITS()
	IF g_bMPCreditsOn
		IF NOT g_bMPCreditsLoaded
			g_sfMPCredits = REQUEST_SCALEFORM_MOVIE("OPENING_CREDITS")
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === REQUESTED")
				#ENDIF
			g_bMPCreditsLoaded = TRUE
		ELSE
			IF HAS_SCALEFORM_MOVIE_LOADED(g_sfMPCredits)
				IF NOT g_bMPCreditsIntroDraw
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(g_sfMPCredits, 255, 255, 255, 255)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== LAST CREDIT === g_sfMPCredits NOT LOADED")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC Update_Friend_List_Stagger(PLAYER_INDEX PlayerID)
	IF NOT (PlayerID = PLAYER_ID())
	AND IS_PLAYER_FRIEND(PlayerID)
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS_Friends, NATIVE_TO_INT(PlayerID))
	ELSE
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBS_Friends, NATIVE_TO_INT(PlayerID))
	ENDIF
ENDPROC

PROC Main_32_Player_Staggered_Repeat_Client_Update()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("Main_32_Player_Staggered_Repeat_Client_Update")
	#ENDIF
	#ENDIF

	IF NOT (g_StaggeredPlayer = INVALID_PLAYER_INDEX())

		// update the near players array - do this first.
		Update_Near_Players_Array_For_Player(g_StaggeredPlayer)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("Update_Near_Players_Array_For_Player")
		#ENDIF
		#ENDIF
			
		Process_client_shared_staggered_and_non_staggered_functions(g_StaggeredPlayer)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("Process_client_shared_staggered_and_non_staggered_functions")
		#ENDIF
		#ENDIF
		
		RENDER_MP_INTRO_CREDITS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RENDER_MP_INTRO_CREDITS")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF GET_INDEX_OF_CURRENT_LEVEL() != LEVEL_NET_TEST
		#ENDIF
			UPDATE_BLIP_FOR_PLAYER(g_StaggeredPlayer)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("HANDLE_PLAYER_BLIPS")
			#ENDIF
			#ENDIF
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		LISTEN_FOR_SESSION_KICK(g_StaggeredPlayer)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("LISTEN_FOR_SESSION_KICK")
		#ENDIF
		#ENDIF
		#ENDIF
		
		StorePlayersInSameSession(g_StaggeredPlayer)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("StorePlayersInSameSession")
		#ENDIF
		#ENDIF
		
		Update_Friend_List_Stagger(g_StaggeredPlayer)
		
		IF IS_NET_PLAYER_OK(g_StaggeredPlayer)

			RUN_OVERHEAD_FREEMODE_STAGGERED_LOGIC_CHECKS(g_StaggeredPlayer)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("RUN_OVERHEAD_FREEMODE_STAGGERED_LOGIC_CHECKS")
			#ENDIF
			#ENDIF
		ENDIF
		
	ENDIF


	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Maintain_Main_Generic_Client_Start_Of_Frame_Every_Frame(HEIST_ISLAND_TRAVEL_STRUCT &sHeistIslandTravel, FM_RANDOM_EVENT_LOCAL_SCRIPT_EVENT &sFMREScriptEventdata)

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		OPEN_SCRIPT_PROFILE_MARKER_GROUP("Maintain_Main_Generic_Client_Start_Of_Frame_Every_Frame")
	#ENDIF
	#ENDIF

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_Main_Generic_Client_Start_Of_Frame_Every_Frame ...start")
	#ENDIF
	#ENDIF
	
	IF NOT (g_bStaggeredConsidersEveryone)
	
		find_next_non_spectating_participant()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("find_next_non_spectating_participant")
		#ENDIF
		#ENDIF
	
	ELSE
	
		find_next_staggered_participant()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("find_next_staggered_participant")
		#ENDIF
		#ENDIF
		
	ENDIF

	// process any events in the event queue (Processing events must happen before anything else to make sure the correct players are active etc)
	PROCESS_NET_EVENTS(sHeistIslandTravel, sFMREScriptEventdata)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_NET_EVENTS")
	#ENDIF
	#ENDIF
	
	//process any AI events in the event queue (currently only looking at players destroying and damaging entities for bad cop stat - R.C.)
	PROCESS_NET_AI_EVENTS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_NET_AI_EVENTS")
	#ENDIF
	#ENDIF
	
	
	STORE_LOCAL_PLAYER_ID()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("STORE_LOCAL_PLAYER_ID")
	#ENDIF
	#ENDIF
	
	MAINTAIN_MP_AMBIENT_MANAGER()
	#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_AMBIENT_MANAGER")
		#ENDIF
	#ENDIF
	
	DO_ALL_PROCESS_VISIBLE_PLAYER_CHECKS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("DO_ALL_PROCESS_VISIBLE_PLAYER_CHECKS")
	#ENDIF
	#ENDIF

//	// debug
//	#IF IS_DEBUG_BUILD	
//		Maintain_Main_Generic_Debug()
//		#IF SCRIPT_PROFILER_ACTIVE 
//		ADD_SCRIPT_PROFILE_MARKER("Maintain_Main_Generic_Debug")
//		#ENDIF
//	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF

ENDPROC

PROC Generic_Process_Near_Players_Loop()
	

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("Generic_Process_Near_Players_Loop")
	#ENDIF
	#ENDIF	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Generic_Process_Near_Players_Loop ...start")
	#ENDIF
	#ENDIF	
	
	INT i
	PLAYER_INDEX PlayerID
	PARTICIPANT_INDEX ParticipantID

		
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_PRE_LOOP_MARKER("loop")
	#ENDIF
	#ENDIF	

	REPEAT MAX_NUM_NEAR_PLAYERS i
	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			
		#ENDIF
		#ENDIF		
	
		PlayerId = INT_TO_NATIVE(PLAYER_INDEX, NearPlayers[i])
		
		IF NOT (PlayerID = INVALID_PLAYER_INDEX())
		
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerID)
				ParticipantID = NETWORK_GET_PARTICIPANT_INDEX(PlayerId)
				
				IF NOT (g_iStaggeredParticipant = NATIVE_TO_INT(ParticipantID)) // this player has already been processed in the staggered loop
				AND  IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
						
					Process_client_shared_staggered_and_non_staggered_functions(PlayerID)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Process_client_shared_staggered_and_non_staggered_functions")
					#ENDIF
					#ENDIF			
					
				ENDIF
			ENDIF
			
		ENDIF
			
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_POST_LOOP_MARKER()
	#ENDIF
	#ENDIF	
	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF		
	

ENDPROC

PROC MAINTAIN_PLAYER_JUMP_OFF_HALFTRACK()
	VEHICLE_INDEX VehicleID[5] 
	INT iNumVehicles, i
	iNumVehicles = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), VehicleID)
	IF NOT g_bDisableJumpOffHalfTrackFunctionality
		IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
		OR IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
		OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		OR IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
		OR IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
		OR IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_VALET_GARAGE(PLAYER_ID())
		OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		#IF FEATURE_TUNER
		OR IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
		#ENDIF
			IF NOT g_bEnableJumpingOffHalfTrackInProperty
				REPEAT iNumVehicles i
				
					IF DOES_ENTITY_EXIST(VehicleID[i])
					AND NOT IS_ENTITY_DEAD(VehicleID[i])
						//PRINTLN("MAINTAIN_PLAYER_JUMP_OFF_HALFTRACK: nearby vehicle detected model = ", GET_ENTITY_MODEL(VehicleID[i]), " half track = ",HALFTRACK )
						IF IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(),VehicleID[i])
							IF GET_ENTITY_MODEL(VehicleID[i]) = HALFTRACK
								PRINTLN("MAINTAIN_PLAYER_JUMP_OFF_HALFTRACK -g_bEnableJumpingOffHalfTrackInProperty  set true this frame-1")
								g_bEnableJumpingOffHalfTrackInProperty = TRUE
								EXIT
							ELIF GET_ENTITY_MODEL(VehicleID[i]) = CARACARA
								IF NOT IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()),GET_ENTITY_HEADING(VehicleID[i]),95)
									PRINTLN("MAINTAIN_PLAYER_JUMP_OFF_HALFTRACK -g_bEnableJumpingOffHalfTrackInProperty  set true this frame- 2")
									g_bEnableJumpingOffHalfTrackInProperty = TRUE
									EXIT
								ELSE
									PRINTLN("MAINTAIN_PLAYER_JUMP_OFF_HALFTRACK -g_bEnableJumpingOffHalfTrackInProperty not allowed this frame facing front of caracara")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// PURPOSE:	This procedure should be called every frame to control the generic client routines.
PROC Maintain_Main_Generic_Client()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("Maintain_Main_Generic_Client")
	#ENDIF
	#ENDIF
			
	Main_32_Player_Staggered_Repeat_Client_Update()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Main_32_Player_Staggered_Repeat_Client_Update")
	#ENDIF
	#ENDIF

	Generic_Process_Near_Players_Loop()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("Generic_Process_Near_Players_Loop()")
	#ENDIF
	#ENDIF
	
	// Performs any End of Mission message maintenance and any stored communication maintenance
	// NOTE: These should be done before the communications system is updated
	Maintain_MP_End_Of_Mission_Message()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_End_Of_Mission_Message()")
	#ENDIF
	#ENDIF
	
	Maintain_MP_Stored_Communication()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Stored_Communication()")
	#ENDIF
	#ENDIF
	
	Maintain_MP_Txtmsgs()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Txtmsgs()")
	#ENDIF
	#ENDIF

	// Control access to MP communications devices (cellphone, radio, laptop, whatever)
	Maintain_MP_Communications()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Communications()")
	#ENDIF
	#ENDIF
	
	// Updates mission triggering routines, including making the phonecall to offer the mission to the player
	Maintain_MP_Mission_Triggering()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Mission_Triggering()")
	#ENDIF
	#ENDIF
	
		
	// Maintain Joblist Updating
	Maintain_MP_Joblist_Update()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Joblist_Update()")
	#ENDIF
	#ENDIF
	
	// Updates mission helper systems
	Maintain_MP_Objective_Text()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Objective_Text()")
	#ENDIF
	#ENDIF
	
	// Check for player being idle
	//Maintain_Player_Idle_Check()
	
	// Mission Control System - Client-side
	Maintain_MP_Mission_Control_Client()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Mission_Control_Client()")
	#ENDIF
	#ENDIF	
	
	IF IS_FREEMODE_ONLINE()
		
		// Missions At Coords System
		Maintain_MP_Missions_At_Coords()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Missions_At_Coords()")
		#ENDIF
		#ENDIF
			
		// Activity Selector System - Client-side
		Maintain_MP_Activity_Selector_Client()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Activity_Selector_Client()")
		#ENDIF
		#ENDIF
	
		// Missions Shared System
		Maintain_MP_Missions_Shared_Client()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Missions_Shared_Client()")
		#ENDIF
		#ENDIF
		
		// Mission Locate Message System - for the mission names that appear in the middle of a corona
		Maintain_MP_Locate_Messages()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Locate_Messages()")
		#ENDIF
		#ENDIF			
		
	ENDIF	

	// Player Headshots System - player pictures that appear on cellphone screens and in the feed, etc
	Maintain_MP_Player_Headshots()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Player_Headshots()")
	#ENDIF
	#ENDIF	
	
	// Deal with spawning.
	UPDATE_PLAYER_RESPAWN_STATE()		
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_PLAYER_RESPAWN_STATE()")
	#ENDIF
	#ENDIF				
			
	// update general info (timer for flashing etc)
	MAINTAIN_GENERAL_BLIP_INFO()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GENERAL_BLIP_INFO()")
	#ENDIF
	#ENDIF		
	
	// JA: Display any XP animations or big messages
	MAINTAIN_XP_ANIMS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_XP_ANIMS")
	#ENDIF
	#ENDIF
	
	MAINTAIN_BIG_MESSAGE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BIG_MESSAGE")
	#ENDIF
	#ENDIF
	
	MAINTAIN_KILL_STRIP_VOICE_SETTINGS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_KILL_STRIP_VOICE_SETTINGS")
	#ENDIF
	#ENDIF
		
	MAINTAIN_PLAYERS_GLOBAL_OHD_DATA()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYERS_GLOBAL_OHD_DATA")
	#ENDIF
	#ENDIF
	
	MAINTAIN_PLAYER_JUMP_OFF_HALFTRACK()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYER_JUMP_OFF_HALFTRACK")
	#ENDIF
	#ENDIF
	
	UPDATE_VEHICLE_BOOSTING()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_VEHICLE_BOOSTING")
	#ENDIF
	#ENDIF
	
	Maintain_Script_Bails()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("Maintain_Script_Bails")
	#ENDIF
	#ENDIF
	
	UPDATE_INTERACTION_ANIMS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_INTERACTION_ANIMS")
	#ENDIF
	#ENDIF
	
		UPDATE_GROUP_WARP()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_GROUP_WARP")
		#ENDIF
		#ENDIF		
	
	

	// ********************* DEBUG BELOW HERE **********************
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("debuggery")
	#ENDIF
	#ENDIF
	
	
	// DEBUG ONLY - Debug Log
	#IF IS_DEBUG_BUILD
		Maintain_MP_Debug_Log()
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Debug_Log()")
		#ENDIF
	#ENDIF
	
	// maintain j skips
	#IF IS_DEBUG_BUILD
		MAINTAIN_J_SKIP_WARP()
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_J_SKIP_WARP()")
		#ENDIF
	#ENDIF
	
	// debug only, update the scripts on f9 screen
	#IF IS_DEBUG_BUILD
	UPDATE_F9_SCRIPT(F9ScriptInfo[iF9ScriptToUpdate])
	iF9ScriptToUpdate += 1
	IF (iF9ScriptToUpdate >= TOTAL_NUMBER_OF_F9_SCRIPTS)
		iF9ScriptToUpdate = 0
	ENDIF
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_F9_SCRIPTS")
	#ENDIF	
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	MAINTAIN_DEBUG_MENU_WARP()
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DEBUG_MENU_WARP")
	#ENDIF	
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("Debug Functions")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF

ENDPROC





#IF IS_DEBUG_BUILD


// PURPOSE:	This procedure should be called every frame to control the generic client routines.
PROC Maintain_Main_Generic_Client_TESTBED()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("Maintain_Main_Generic_Client_TESTBED")
	#ENDIF
	#ENDIF
			
	Main_32_Player_Staggered_Repeat_Client_Update()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Main_32_Player_Staggered_Repeat_Client_Update")
	#ENDIF
	#ENDIF

	Generic_Process_Near_Players_Loop()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("Generic_Process_Near_Players_Loop()")
	#ENDIF
	#ENDIF
	
	// Performs any End of Mission message maintenance and any stored communication maintenance
	// NOTE: These should be done before the communications system is updated
	Maintain_MP_End_Of_Mission_Message()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_End_Of_Mission_Message()")
	#ENDIF
	#ENDIF
	
	Maintain_MP_Stored_Communication()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Stored_Communication()")
	#ENDIF
	#ENDIF
	
	Maintain_MP_Txtmsgs()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Txtmsgs()")
	#ENDIF
	#ENDIF

	// Control access to MP communications devices (cellphone, radio, laptop, whatever)
	Maintain_MP_Communications()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Communications()")
	#ENDIF
	#ENDIF
	
	// Updates mission triggering routines, including making the phonecall to offer the mission to the player
	Maintain_MP_Mission_Triggering()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Mission_Triggering()")
	#ENDIF
	#ENDIF
	
		
	// Maintain Joblist Updating
	Maintain_MP_Joblist_Update()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Joblist_Update()")
	#ENDIF
	#ENDIF
	
	// Updates mission helper systems
	Maintain_MP_Objective_Text()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Objective_Text()")
	#ENDIF
	#ENDIF
	
	// Check for player being idle
	//Maintain_Player_Idle_Check()
	
	// Mission Control System - Client-side
	Maintain_MP_Mission_Control_Client()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Mission_Control_Client()")
	#ENDIF
	#ENDIF	
	
	// Missions At Coords System
	Maintain_MP_Missions_At_Coords()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Missions_At_Coords()")
	#ENDIF
	#ENDIF
	
	// Activity Selector System - Client-side
	Maintain_MP_Activity_Selector_Client()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Activity_Selector_Client()")
	#ENDIF
	#ENDIF
	
	// Missions Shared System
	Maintain_MP_Missions_Shared_Client()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Missions_Shared_Client()")
	#ENDIF
	#ENDIF
	
	// Mission Locate Message System - for the mission names that appear in the middle of a corona
	Maintain_MP_Locate_Messages()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Locate_Messages()")
	#ENDIF
	#ENDIF			
	
	// Player Headshots System - player pictures that appear on cellphone screens and in the feed, etc
	Maintain_MP_Player_Headshots()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Player_Headshots()")
	#ENDIF
	#ENDIF	
	
	// Deal with spawning.
	UPDATE_PLAYER_RESPAWN_STATE()		
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_PLAYER_RESPAWN_STATE()")
	#ENDIF
	#ENDIF				
			
	// update general info (timer for flashing etc)
	MAINTAIN_GENERAL_BLIP_INFO()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GENERAL_BLIP_INFO()")
	#ENDIF
	#ENDIF		
	
	// JA: Display any XP animations or big messages
	MAINTAIN_XP_ANIMS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_XP_ANIMS")
	#ENDIF
	#ENDIF
	
	MAINTAIN_BIG_MESSAGE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BIG_MESSAGE")
	#ENDIF
	#ENDIF
	
	MAINTAIN_KILL_STRIP_VOICE_SETTINGS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_KILL_STRIP_VOICE_SETTINGS")
	#ENDIF
	#ENDIF
		
	MAINTAIN_PLAYERS_GLOBAL_OHD_DATA()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYERS_GLOBAL_OHD_DATA")
	#ENDIF
	#ENDIF
	
	UPDATE_VEHICLE_BOOSTING()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_VEHICLE_BOOSTING")
	#ENDIF
	#ENDIF
	
	Maintain_Script_Bails()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("Maintain_Script_Bails")
	#ENDIF
	#ENDIF
	

	// ********************* DEBUG BELOW HERE **********************
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("more debuggery")
	#ENDIF
	#ENDIF
	
	
	// DEBUG ONLY - Debug Log
	#IF IS_DEBUG_BUILD
		Maintain_MP_Debug_Log()
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Debug_Log()")
		#ENDIF
	#ENDIF
	
	// maintain j skips
	#IF IS_DEBUG_BUILD
		MAINTAIN_J_SKIP_WARP()
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_J_SKIP_WARP()")
		#ENDIF
	#ENDIF
	
	// debug only, update the scripts on f9 screen
	#IF IS_DEBUG_BUILD
	UPDATE_F9_SCRIPT(F9ScriptInfo[iF9ScriptToUpdate])
	iF9ScriptToUpdate += 1
	IF (iF9ScriptToUpdate >= TOTAL_NUMBER_OF_F9_SCRIPTS)
		iF9ScriptToUpdate = 0
	ENDIF
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_F9_SCRIPTS")
	#ENDIF	
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	MAINTAIN_DEBUG_MENU_WARP()
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DEBUG_MENU_WARP")
	#ENDIF	
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("Debug Functions")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF

ENDPROC






#ENDIF




