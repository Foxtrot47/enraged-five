USING "timer_public.sch"
USING "rage_builtins.sch"
USING "globals.sch"
//USING "commands_audio.sch"
//USING "commands_camera.sch"
//USING "commands_clock.sch"
//USING "commands_debug.sch"
//USING "commands_fire.sch"
//USING "commands_hud.sch"
//USING "commands_misc.sch"
//USING "commands_object.sch"
//USING "commands_pad.sch"
//USING "commands_ped.sch"
//USING "commands_player.sch"
//USING "commands_script.sch"
//USING "commands_streaming.sch"
//USING "commands_task.sch"
//USING "commands_vehicle.sch"
//USING "commands_interiors.sch"
//USING "net_include.sch"
//USING "freemode_header.sch"
//USING "net_mp_tv.sch"
//USING "model_enums.sch"
//USING "script_player.sch"
//USING "script_misc.sch"
//USING "selector_public.sch"
//USING "timer_public.sch"
//USING "net_gangops_hacking_common.sch"
USING "minigames_helpers.sch"

ENUM HACKING_GAME_STATE
	HACKING_GAME_SLEEP = 0,
	HACKING_GAME_INIT,
	HACKING_GAME_LOAD_LEVEL,
	HACKING_GAME_STARTUP,
	HACKING_GAME_PLAY,
	HACKING_GAME_PASS,
	HACKING_GAME_FAIL,
	HACKING_GAME_CLEANUP,
	HACKING_GAME_VISUAL_TEST
ENDENUM

ENUM HACKING_GAME_ANIMATION_SPEED
	HG_ANIMATION_DEFAULT,
	HG_ANIMATION_SLOW,
	HG_ANIMATION_FAST
ENDENUM

CONST_FLOAT cfHG_BG_XPOS 0.5
CONST_FLOAT cfHG_BG_YPOS 0.5
CONST_FLOAT cfHG_BG_WIDTH 1920.0
CONST_FLOAT cfHG_BG_HEIGHT 1080.0

CONST_FLOAT cfHG_tech1_XPOS 1.0
CONST_FLOAT cfHG_tech1_YPOS 0.358
CONST_FLOAT cfHG_tech1_WIDTH 780.0
CONST_FLOAT cfHG_tech1_HEIGHT 512.0
CONST_INT ciHG_tech1_FRAME_COUNT 10

CONST_FLOAT cfHG_tech2_XPOS 0.993
CONST_FLOAT cfHG_tech2_YPOS 0.642
CONST_FLOAT cfHG_tech2_WIDTH 840.0
CONST_FLOAT cfHG_tech2_HEIGHT 580.0
CONST_INT ciHG_tech2_FRAME_COUNT 11

CONST_FLOAT cfHG_tech3_XPOS 0.073
CONST_FLOAT cfHG_tech3_YPOS 0.489
CONST_FLOAT cfHG_tech3_WIDTH 980.0
CONST_FLOAT cfHG_tech3_HEIGHT 780.0
CONST_INT ciHG_tech3_FRAME_COUNT 1

CONST_FLOAT cfHG_tech4_XPOS 0.044
CONST_FLOAT cfHG_tech4_YPOS 0.672
CONST_FLOAT cfHG_tech4_WIDTH 980.0
CONST_FLOAT cfHG_tech4_HEIGHT 580.0
CONST_INT ciHG_tech4_FRAME_COUNT 2

CONST_FLOAT cfHG_PLAYAREA_WIDTH 1264.0
CONST_FLOAT cfHG_PLAYAREA_HEIGHT 940.0
CONST_FLOAT cfHG_TECHARATION_WIDTH 1264.0
CONST_FLOAT cfHG_TECHARATION_HEIGHT 940.0

CONST_FLOAT cfHG_STATUS_MESSAGE_POSITIONX 0.5
CONST_FLOAT cfHG_STATUS_MESSAGE_POSITIONY 0.5
CONST_FLOAT cfHG_STATUS_MESSAGE_WIDTH 600.0 // 0.394
CONST_FLOAT cfHG_STATUS_MESSAGE_HEIGHT 140.0

CONST_FLOAT cfHG_STATUS_MESSAGE_LOADING_WIDTH 480.0
CONST_FLOAT cfHG_STATUS_MESSAGE_LOADING_HEIGHT 128.0
CONST_FLOAT cfHG_RESULTS_LOADING_POSITIONY 0.516
CONST_FLOAT cfHG_RESULTS_LOADING_INITIALX 0.320
CONST_FLOAT cfHG_RESULTS_LOADING_XDIFF 0.01
CONST_FLOAT cfHG_RESULTS_LOADING_HEIGHT 40.0
CONST_FLOAT cfHG_RESULTS_LOADING_WIDTH 12.0
CONST_INT ciHG_RESULTS_LOADING_MAX_SEGMENTS 35

CONST_FLOAT cfHG_SCRAMBLE_WIDTH 400.0
CONST_FLOAT cfHG_SCRAMBLE_HEIGHT 64.0
CONST_FLOAT cfHG_SCRAMBLE_ELEMENT_WIDTH 12.0
CONST_FLOAT cfHG_SCRAMBLE_ELEMENT_HEIGHT 80.0
CONST_FLOAT cfHG_SCRAMBLE_DIFFBETWEENSEGMENT 0.0115
CONST_FLOAT cfHG_SCRAMBLE_Y_OFFSET 0.005
CONST_INT ciHG_SCRAMBLE_MAX_SEGMENTS 31
CONST_INT ciHG_SCRAMBLE_START_OF_MEDIUM_INTENSITY 50
CONST_INT ciHG_SCRAMBLE_START_OF_HIGH_INTENSITY 17 

CONST_FLOAT cfHG_TIMER_WIDTH 40.0
CONST_FLOAT cfHG_TIMER_HEIGHT 60.0

CONST_FLOAT cfHG_LIVES_WIDTH 64.0
CONST_FLOAT cfHG_LIVES_HEIGHT 64.0

CONST_FLOAT cfHG_FAST_ANIM_FRAME_TIME 155.0
CONST_FLOAT cfHG_DEFAULT_ANIM_FRAME_TIME 311.0
CONST_FLOAT cfHG_SLOW_ANIM_FRAME_TIME 365.0



CONST_INT ciHG_DRAW_COUNTER 20
CONST_INT ciHG_BACKGROUND_TECH_COUNT 4
CONST_INT ciHG_TECHARATION_FRAMES 4
CONST_INT ciHG_RESULT_FRAME_COUNT 2

FLOAT fHG_AspectRatio
FLOAT fHG_AspectRatioMod
CONST_FLOAT HG_DEFAULT_ASPECT_RATIO_MOD 1.778
CONST_FLOAT cfHG_SCREEN_CENTER 0.5

CONST_INT ciHGBS_PASSED 0
CONST_INT ciHGBS_FAILED 1
CONST_INT ciHGBS_QUIT 	2
CONST_INT ciHGBS_TIMED_OUT 3
CONST_INT ciHGBS_OUT_OF_LIVES 4
CONST_INT ciHGBS_FIRST_PLAYED 5
CONST_INT ciHGBS_LOST_LIFE 6
CONST_INT ciHGBS_PASSED_SECTION 7
CONST_INT ciHGBS_TEAM_HAS_AGGRO 8

CONST_INT ciHGBS_UP 0
CONST_INT ciHGBS_DOWN 1
CONST_INT ciHGBS_LEFT 	2
CONST_INT ciHGBS_RIGHT 3

CONST_INT ci_HG_QUIT_TIME 1500
CONST_INT ciHG_ABORT_WARNING_TIME 2000
CONST_INT ciHG_ABORT_WARNING 30
CONST_INT ciHG_FIRST_ABORT_TIME 10000

CONST_INT ciHG_MIN_PATTERNS 1
CONST_INT ciHG_MAX_PATTERNS 4

TWEAK_INT ciHG_OVERLAY_ALPHA 255
TWEAK_INT ciHG_SHADOW_ALPHA 100
TWEAK_INT ciHG_OVERLAY_R 255
TWEAK_INT ciHG_OVERLAY_G 255
TWEAK_INT ciHG_OVERLAY_B 255

CONST_INT ciHG_BS_SOUND_STARTUP 0
CONST_INT ciHG_BS_SOUND_DRAWSCREEN 1
CONST_INT ciHG_BS_SOUND_PLAYSTART 2
CONST_INT ciHG_BS_SOUND_PLAYFINALE 3
CONST_INT ciHG_BS_SOUND_SECTIONCLEAR 4
CONST_INT ciHG_BS_SOUND_WINDOW_APPEAR 5

STRUCT HG_RGBA_COLOUR_STRUCT
	INT iR
	INT iG
	INT iB
	INT iA = 255
ENDSTRUCT

PROC HG_STORE_END_TIME(HG_CONTROL_STRUCT &sControllerStruct, SCRIPT_TIMER &tdTimer, INT iMaxTime)
	sControllerStruct.iTimeElapsed = CLAMP_INT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTimer), 0, iMaxTime)
ENDPROC

PROC HG_GET_CUSTOM_SCREEN_ASPECT_RATIO()
	fHG_AspectRatio = GET_ASPECT_RATIO(FALSE)
	//Fix for Eyefinity triple monitors: the resolution returns as the full three-screen width but the game is only drawn on one screen.
	IF IS_PC_VERSION()
		IF fHG_AspectRatio >= 4.0
			fHG_AspectRatio = fHG_AspectRatio / 3.0
		ENDIF
	ENDIF
ENDPROC

PROC HG_GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO()
	fHG_AspectRatioMod = HG_DEFAULT_ASPECT_RATIO_MOD / fHG_AspectRatio
	fHG_AspectRatioMod = fHG_AspectRatioMod
ENDPROC

FUNC FLOAT HG_APPLY_ASPECT_MOD_TO_X(FLOAT fX)
	fX =  cfHG_SCREEN_CENTER - ((cfHG_SCREEN_CENTER - fX) / fHG_AspectRatio)
	RETURN fX
ENDFUNC

STRUCT HACKING_GAME_STRUCT
	HACKING_GAME_STATE eHackingGameState = HACKING_GAME_INIT
	BOOL bUpdatePrompts
	
	INT iDrawCounter
	INT iCurrentLives
	INT iMaxLives
	
	BINK_MOVIE_ID movieId
	
	#IF IS_DEBUG_BUILD
	BOOL bDebugReset
	BOOL bDebugCreatedWidgets
	WIDGET_GROUP_ID widgetFingerprintClone
	INT iDebugLivesToSet
	INT iScrambleWipeChanceToSet
	FLOAT fDebugTimeToSet
	FLOAT fDebugScrambleTimeToSet
	BOOL bDebugShowGuide
	BOOL bDebugHideOverlay
	BOOL bDebugHideShadow
	#ENDIF
	
	INT iDefaultFrameTimeCounter
	INT iDefaultUpdateFrames
	
	INT iSlowFrameTimeCounter
	INT iSlowUpdateFrames
	
	INT iFastFrameTimeCounter
	INT iFastUpdateFrames
	
	INT iBackgroundAnimFrames[ciHG_BACKGROUND_TECH_COUNT]
	INT iTecharationCounter
	
	INT iCancelTimeData
	INT iPreviousCancelTimeData
	SCRIPT_CONTROL_HOLD_TIMER sQuitTimer
	SCRIPT_CONTROL_HOLD_TIMER sQuitTimerPC
	
	SCALEFORM_INDEX scaleformInstructionalButtonsIndex
	SCALEFORM_INSTRUCTIONAL_BUTTONS scaleformInstructionalButtons
	
	// Scramble
	SCRIPT_TIMER tdScrambleTimer
	INT iResultCounter
	
	INT iMaxTime
	INT iScrambleTime
	
	INT iScrambleWipeChance
	
	INT iNumberOfStages
	
	INT iControlBits
	
	INT iPreviousScrambleSegmentCount
	INT iCurrentScrambleSegmentCount
	
	INT iHGBackgroundLoopSndId = -1
	INT iHGSoundBitset
	
	STRING sAudioSet
	
	SCRIPT_TIMER tdTimer
	SCRIPT_TIMER tdEndDelay
	SCRIPT_TIMER tdCancelTimer
	SCRIPT_TIMER tdRetroTimer
	SCRIPT_TIMER tdAnimationTimer
ENDSTRUCT

FUNC STRING GET_STRING_FROM_HACKING_GAME_STATE(HACKING_GAME_STATE eState)
	SWITCH eState
		CASE HACKING_GAME_SLEEP			RETURN "HACKING_GAME_SLEEP"
		CASE HACKING_GAME_INIT			RETURN "HACKING_GAME_INIT"
		CASE HACKING_GAME_LOAD_LEVEL	RETURN "HACKING_GAME_LOAD_LEVEL"
		CASE HACKING_GAME_STARTUP		RETURN "HACKING_GAME_STARTUP"
		CASE HACKING_GAME_PLAY			RETURN "HACKING_GAME_PLAY"
		CASE HACKING_GAME_PASS			RETURN "HACKING_GAME_PASS"
		CASE HACKING_GAME_FAIL			RETURN "HACKING_GAME_FAIL"
		CASE HACKING_GAME_CLEANUP		RETURN "HACKING_GAME_CLEANUP"
		CASE HACKING_GAME_VISUAL_TEST	RETURN "HACKING_GAME_VISUAL_TEST"
	ENDSWITCH		
	RETURN "GET_STRING_FROM_HACKING_GAME_STATE - No Such State"
ENDFUNC

PROC UPDATE_HACKING_GAME_STATE(HACKING_GAME_STRUCT &sHackingGameStruct, HACKING_GAME_STATE eNewState)
	#IF IS_DEBUG_BUILD
	PRINTLN("[MAC][HackingGame] - UPDATE_HACKING_GAME_STATE - Updating state from ",GET_STRING_FROM_HACKING_GAME_STATE(sHackingGameStruct.eHackingGameState)," to ",GET_STRING_FROM_HACKING_GAME_STATE(eNewState))
	#ENDIF
	sHackingGameStruct.eHackingGameState = eNewState
ENDPROC

FUNC BOOL ARRAY_CONTAINS_INT(INT &array[], INT iTargetValue, INT iArrayLength)
	INT i
	
	FOR i = 0 TO iArrayLength-1
		IF array[i] = iTargetValue
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_OUTPUT_WIDTH_FOR_IMAGE(FLOAT fInput)
	FLOAT result = fInput/1920.0
	RETURN result
ENDFUNC

FUNC FLOAT GET_OUTPUT_HEIGHT_FOR_IMAGE(FLOAT fInput)
	FLOAT result = fInput/1080.0
	RETURN result
ENDFUNC

PROC HACKING_GAMES_UPDATE_ANIM_FRAMES(HACKING_GAME_STRUCT &sHackingGameStruct)
	
	//Slow anim update
	sHackingGameStruct.iSlowFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
	sHackingGameStruct.iSlowUpdateFrames = FLOOR(sHackingGameStruct.iSlowFrameTimeCounter / cfHG_SLOW_ANIM_FRAME_TIME)
	sHackingGameStruct.iSlowFrameTimeCounter -= ROUND(cfHG_SLOW_ANIM_FRAME_TIME * sHackingGameStruct.iSlowUpdateFrames)
	
	//Default anim update
	sHackingGameStruct.iDefaultFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
	sHackingGameStruct.iDefaultUpdateFrames = FLOOR(sHackingGameStruct.iDefaultFrameTimeCounter / cfHG_DEFAULT_ANIM_FRAME_TIME)
	sHackingGameStruct.iDefaultFrameTimeCounter -= ROUND(cfHG_DEFAULT_ANIM_FRAME_TIME * sHackingGameStruct.iDefaultUpdateFrames)
	
		//Default anim update
	sHackingGameStruct.iFastFrameTimeCounter += ROUND(GET_FRAME_TIME() * 1000)
	sHackingGameStruct.iFastUpdateFrames = FLOOR(sHackingGameStruct.iFastFrameTimeCounter / cfHG_FAST_ANIM_FRAME_TIME)
	sHackingGameStruct.iFastFrameTimeCounter -= ROUND(cfHG_FAST_ANIM_FRAME_TIME * sHackingGameStruct.iFastUpdateFrames)
	
ENDPROC

PROC DRAW_HACKING_SPRITE(STRING pTextureDictionaryName, STRING pTextureName, FLOAT CentreX, FLOAT CentreY, FLOAT Width, FLOAT Height, FLOAT Rotation, INT R, INT G, INT B, INT A, BOOL DoStereorize = FALSE)
	DEBUG_PRINTCALLSTACK()
	DRAW_SPRITE(pTextureDictionaryName, pTextureName, HG_APPLY_ASPECT_MOD_TO_X(CentreX), CentreY, GET_OUTPUT_WIDTH_FOR_IMAGE(Width) * fHG_AspectRatioMod , GET_OUTPUT_HEIGHT_FOR_IMAGE(Height), Rotation, R, G, B, A, DoStereorize)
ENDPROC

PROC DRAW_HACKING_SPRITE_CORRECTED(STRING pTextureDictionaryName, STRING pTextureName, FLOAT CentreX, FLOAT CentreY, FLOAT Width, FLOAT Height, FLOAT Rotation, INT R, INT G, INT B, INT A, BOOL DoStereorize = FALSE)
	CentreX += (CentreX%0.00105)
	CentreY += (CentreY%0.00185)
	
	Width += (Width%2)
	Height += (Height%2)
	
	DRAW_HACKING_SPRITE(pTextureDictionaryName, pTextureName, CentreX, CentreY, Width, Height, Rotation, R, G, B, A, DoStereorize)
ENDPROC

PROC DRAW_HACKING_ANIMATED_SPRITE(HACKING_GAME_STRUCT &sHackingGameStruct, INT &iSpriteFrame, INT iSpriteMaxFrame, HACKING_GAME_ANIMATION_SPEED eAnimationSpeed , STRING pTextureDictionaryName, STRING pTextureName, FLOAT CentreX, FLOAT CentreY, FLOAT Width, FLOAT Height, FLOAT Rotation, INT R, INT G, INT B, INT A, BOOL DoStereorize = FALSE)
	// DO processing for animated sprites
	IF eAnimationSpeed = HG_ANIMATION_SLOW
		iSpriteFrame += sHackingGameStruct.iSlowUpdateFrames
	ELIF eAnimationSpeed = HG_ANIMATION_FAST
		iSpriteFrame += sHackingGameStruct.iDefaultUpdateFrames
		
	ELSE
		iSpriteFrame += sHackingGameStruct.iDefaultUpdateFrames
	ENDIF
	
	IF iSpriteFrame >= iSpriteMaxFrame
		iSpriteFrame = 0
	ENDIF
	
	// assemble sprite name
	TEXT_LABEL_63 tl23Sprite = pTextureName
	tl23Sprite += iSpriteFrame
	
	// Draw it
	DRAW_HACKING_SPRITE(pTextureDictionaryName, tl23Sprite, CentreX, CentreY, Width, Height, Rotation, r, g, b, a, DoStereorize)
ENDPROC

//VECTOR OBJECT STRUCTURE
STRUCT HG_VECTOR_2D
	FLOAT x
	FLOAT y
ENDSTRUCT

FUNC HG_VECTOR_2D INIT_HG_VECTOR(FLOAT x, FLOAT y)
	HG_VECTOR_2D vectorHG
	
	vectorHG.x = x
	vectorHG.y = y
	
	RETURN vectorHG
ENDFUNC

FUNC HG_VECTOR_2D HACKING_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(HG_VECTOR_2D vPixelSpace)
	HG_VECTOR_2D returnVector
	
	returnVector.x = (vPixelSpace.x * (1 / 1980))
	returnVector.y = (vPixelSpace.y * (1 / 1080))
	
	RETURN returnVector
ENDFUNC

PROC LOAD_BINK_MOVIE(HACKING_GAME_STRUCT &sHackingGameStruct, STRING sMovieName)
	PRINTLN("[MC][HackingGame][LOAD_BINK_MOVIE] Loading ", sMovieName)
	PRINTLN("[MC][HackingGame][LOAD_BINK_MOVIE] Starting MovieId = ", NATIVE_TO_INT(sHackingGameStruct.movieId))
	
	sHackingGameStruct.movieId = SET_BINK_MOVIE(sMovieName)
	
	PRINTLN("[MC][HackingGame][LOAD_BINK_MOVIE] MovieId = ", NATIVE_TO_INT(sHackingGameStruct.movieId))
ENDPROC

FUNC BOOL MAINTAIN_BINK_MOVIE(HACKING_GAME_STRUCT &sHackingGameStruct)
	
	PLAY_BINK_MOVIE(sHackingGameStruct.movieId)
	DRAW_BINK_MOVIE(sHackingGameStruct.movieId, HG_APPLY_ASPECT_MOD_TO_X(cfHG_BG_XPOS), cfHG_BG_YPOS, GET_OUTPUT_WIDTH_FOR_IMAGE(cfHG_BG_WIDTH)*fHG_AspectRatioMod, GET_OUTPUT_HEIGHT_FOR_IMAGE(cfHG_BG_HEIGHT), 0.0, 255, 255, 255, 255)
	SET_BINK_SHOULD_SKIP(sHackingGameStruct.movieId , TRUE)
	
	IF GET_BINK_MOVIE_TIME(sHackingGameStruct.movieId) >= 99.0
		STOP_BINK_MOVIE(sHackingGameStruct.movieId)
		RELEASE_BINK_MOVIE(sHackingGameStruct.movieId)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC CONTROL_ACTION HG_GET_LEFT_STICK_ACTION(HACKING_GAME_STRUCT &sGameStruct)
	INT iLeftX = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
	INT iLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
	
	IF (iLeftY < 100
	AND NOT IS_BIT_SET(sGameStruct.iControlBits, ciHGBS_UP))
		SET_BIT(sGameStruct.iControlBits, ciHGBS_UP)
		RETURN INPUT_FRONTEND_UP
	ELIF (iLeftY > 150
	AND NOT IS_BIT_SET(sGameStruct.iControlBits, ciHGBS_DOWN))
		SET_BIT(sGameStruct.iControlBits, ciHGBS_DOWN)
		RETURN INPUT_FRONTEND_DOWN
	ELIF (iLeftX < 100
	AND NOT IS_BIT_SET(sGameStruct.iControlBits, ciHGBS_LEFT))
		SET_BIT(sGameStruct.iControlBits, ciHGBS_LEFT)
		RETURN INPUT_FRONTEND_LEFT
	ELIF (iLeftX > 150
	AND NOT IS_BIT_SET(sGameStruct.iControlBits, ciHGBS_RIGHT))
		SET_BIT(sGameStruct.iControlBits, ciHGBS_RIGHT)
		RETURN INPUT_FRONTEND_RIGHT
	ELSE
		/// Clear Bits
		IF iLeftY > 100
		AND iLeftY < 150
		AND iLeftX > 100
		AND iLeftX < 150
			//PRINTLN("[MC][HackingGame] ClearingControlBits")
			sGameStruct.iControlBits = 0
		ENDIF
	
		RETURN INPUT_FRONTEND_CANCEL
	ENDIF
ENDFUNC

PROC HACKING_GAME_HANDLE_LOOPING_SOUNDS(HACKING_GAME_STRUCT &sCommonStruct, STRING sAudioScene)
	IF NOT IS_AUDIO_SCENE_ACTIVE(sAudioScene)
		START_AUDIO_SCENE(sAudioScene)
	ENDIF
	
	IF sCommonStruct.eHackingGameState > HACKING_GAME_LOAD_LEVEL 
		IF HAS_SOUND_FINISHED(sCommonStruct.iHGBackgroundLoopSndId)
			PLAY_SOUND_FRONTEND(sCommonStruct.iHGBackgroundLoopSndId, "Background_Hum", sCommonStruct.sAudioSet)
			PRINTLN("[MC][HackingGame] Playing Background_Hum")
		ENDIF
	ENDIF
ENDPROC

PROC HACKING_GAME_CLOSE_LOOPING_SOUNDS(HACKING_GAME_STRUCT &sCommonStruct, STRING sAudioScene)
	IF IS_AUDIO_SCENE_ACTIVE(sAudioScene)
		STOP_AUDIO_SCENE(sAudioScene)
	ENDIF
	
	IF NOT HAS_SOUND_FINISHED(sCommonStruct.iHGBackgroundLoopSndId)
		STOP_SOUND(sCommonStruct.iHGBackgroundLoopSndId)
		PRINTLN("[MC][HackingGame] Stopping Background_Hum")
	ENDIF
ENDPROC

PROC PROCESS_HACKING_GAME_SLEEP(HACKING_GAME_STRUCT &sGameStruct, HG_CONTROL_STRUCT &sControllerStruct)
	//IF sControllerStruct.iBS != 0 // Don't re-enter Init when the game is cleaning up
	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_FAILED )
	OR IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED )
		PRINTLN("[MC][HackingGame] Staying Asleep")
		EXIT
	ENDIF
	
	PRINTLN("[MC][HackingGame] Entering Hacking Game Sleep")
	UPDATE_HACKING_GAME_STATE(sGameStruct, HACKING_GAME_INIT)
ENDPROC

PROC PROCESS_COMMON_HACKING_EVERY_FRAME_MISC(HACKING_GAME_STRUCT &sGameStruct, HG_CONTROL_STRUCT &sControllerStruct)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE) 
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	Pause_Objective_Text()
	DISABLE_SELECTOR_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	IF NOT IS_SKYSWOOP_AT_GROUND() 
		UPDATE_HACKING_GAME_STATE(sGameStruct, HACKING_GAME_CLEANUP)
	ENDIF
	#IF USE_REPLAY_RECORDING_TRIGGERS
		DISABLE_REPLAY_RECORDING_UI_THIS_FRAME()
	#ENDIF
	REPLAY_PREVENT_RECORDING_THIS_FRAME()
	IF NOT IS_PAUSE_MENU_ACTIVE()
		//HIDE_HUD_AND_RADAR_THIS_FRAME()
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	ENDIF
	HG_GET_CUSTOM_SCREEN_ASPECT_RATIO()
	HG_GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO()
	
	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED_SECTION)
		CLEAR_BIT(sControllerStruct.iBS, ciHGBS_PASSED_SECTION)
	ENDIF
	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_LOST_LIFE)
		CLEAR_BIT(sControllerStruct.iBS, ciHGBS_LOST_LIFE)
	ENDIF
	
	HACKING_GAMES_UPDATE_ANIM_FRAMES(sGameStruct)
ENDPROC

FUNC BOOL PROCESS_HACKING_GAME_INIT(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sGameStruct)
	PRINTLN("[MC][HackingGame] Running Init")
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		DISABLE_INTERACTION_MENU() 
	ELSE
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	
	TEXT_LABEL_23 tl23 = "MPHackingGame"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	tl23 = "MPHackingGameBG"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	tl23 = "MPHackingGameWin"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	tl23 = "MPHackingGameWin1"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	tl23 = "MPHackingGameWin2"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	tl23 = "MPHackingGameWin2_1"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	tl23 = "MPHackingGameWin2_2"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	tl23 = "MPHackingGameWin2_3"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	tl23 = "MPHackingGameWin3"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	tl23 = "mphackinggameoverlay"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	tl23 = "mphackinggameoverlay1"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	//Turn on blinders for multihead displays.
	SET_MULTIHEAD_SAFE(TRUE, TRUE)
	g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
	
	//Prevent notifications from appearing during the minigame.
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = TRUE
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	
	sGameStruct.sQuitTimer.action = INPUT_FRONTEND_CANCEL
	sGameStruct.sQuitTimer.control = FRONTEND_CONTROL
	
	sGameStruct.sQuitTimerPC.control = FRONTEND_CONTROL
	sGameStruct.sQuitTimerPC.action = INPUT_FRONTEND_DELETE
	
	sGameStruct.iHGBackgroundLoopSndId = GET_SOUND_ID()
	
	CLEAR_BIT(sControllerStruct.iBS, ciHGBS_QUIT)
	RETURN TRUE
ENDFUNC

PROC HACKING_GAME_SET_NUMBER_OF_PATTERNS(HACKING_GAME_STRUCT &sGameStruct, INT iNumberOfStages)
	PRINTLN("[MC][HackingGame] Passed in Number of Patterns ", iNumberOfStages)
	
	IF iNumberOfStages < ciHG_MIN_PATTERNS
		sGameStruct.iNumberOfStages = ciHG_MIN_PATTERNS
		PRINTLN("[MC][HackingGame] Patterns number passed in is too low - clamping to min patterns")
	ELIF iNumberOfStages > ciHG_MAX_PATTERNS
		sGameStruct.iNumberOfStages = ciHG_MAX_PATTERNS
		PRINTLN("[MC][HackingGame] Patterns number passed in is too high - clamping to max patterns")
	ELSE
		sGameStruct.iNumberOfStages = iNumberOfStages
	ENDIF
	
	PRINTLN("[MC][HackingGame] Patterns to be used ", sGameStruct.iNumberOfStages)
ENDPROC

PROC PROCESS_HACKING_GAME_LOAD_LEVEL(HG_CONTROL_STRUCT &sControllerStruct)
	UNUSED_PARAMETER(sControllerStruct)

ENDPROC

PROC PROCESS_HACKING_GAME_STARTUP(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sCommonStruct)
	UNUSED_PARAMETER(sControllerStruct)
	
	IF GET_BINK_MOVIE_TIME(sCommonStruct.movieId) > 0.1
	AND NOT IS_BIT_SET(sCommonStruct.iHGSoundBitset, ciHG_BS_SOUND_STARTUP)
		PLAY_SOUND_FRONTEND(-1, "Startup_Sequence", sCommonStruct.sAudioSet)
		SET_BIT(sCommonStruct.iHGSoundBitset, ciHG_BS_SOUND_STARTUP)
	ENDIF
	
	IF GET_BINK_MOVIE_TIME(sCommonStruct.movieId) > 75.0
	AND NOT IS_BIT_SET(sCommonStruct.iHGSoundBitset, ciHG_BS_SOUND_DRAWSCREEN)
		PLAY_SOUND_FRONTEND(-1, "Draw_Screen", sCommonStruct.sAudioSet)
		SET_BIT(sCommonStruct.iHGSoundBitset, ciHG_BS_SOUND_DRAWSCREEN)
	ENDIF

// Run the start up process here - should be common to both games
// Will run as a bink in full game, for now run

ENDPROC

PROC PROCESS_HACKING_GAME_PLAY(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sCommonStruct, INT iNumberOfPlays, BOOL bCanInteract)
//	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//		PRINTLN("[HACKING_GAME] Player has backed out by pressing CANCEL")
//		HG_STORE_END_TIME(sControllerStruct, sCommonStruct.tdTimer, sCommonStruct.iMaxTime)
//		SET_BIT(sControllerStruct.iBS, ciHGBS_QUIT)
//	ENDIF

	IF iNumberOfPlays = 0
	AND NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_FIRST_PLAYED)
		SET_BIT(sControllerStruct.iBS, ciHGBS_FIRST_PLAYED)
	ENDIF

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_HELD(sCommonStruct.sQuitTimerPC, TRUE, ci_HG_QUIT_TIME)
		AND bCanInteract
			PRINTLN("[HACKING_GAME] Player has backed out by pressing CANCEL PC")
			HG_STORE_END_TIME(sControllerStruct, sCommonStruct.tdTimer, sCommonStruct.iMaxTime)
			//SET_BIT(sControllerStruct.iBS, ciHGBS_FAILED)
			UPDATE_HACKING_GAME_STATE(sCommonStruct, HACKING_GAME_FAIL)
			SET_BIT(sControllerStruct.iBS, ciHGBS_QUIT)
		ENDIF
	ELSE
		IF IS_CONTROL_HELD(sCommonStruct.sQuitTimer, TRUE, ci_HG_QUIT_TIME)
		AND bCanInteract
			PRINTLN("[HACKING_GAME] Player has backed out by pressing CANCEL")
			HG_STORE_END_TIME(sControllerStruct, sCommonStruct.tdTimer, sCommonStruct.iMaxTime)
			//SET_BIT(sControllerStruct.iBS, ciHGBS_FAILED)
			UPDATE_HACKING_GAME_STATE(sCommonStruct, HACKING_GAME_FAIL)
			SET_BIT(sControllerStruct.iBS, ciHGBS_QUIT)
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(sCommonStruct.tdTimer, sCommonStruct.iMaxTime)
		PRINTLN("[HACKING_GAME] Game has timed out")
		HG_STORE_END_TIME(sControllerStruct, sCommonStruct.tdTimer, sCommonStruct.iMaxTime)
		//SET_BIT(sControllerStruct.iBS, ciHGBS_FAILED)
		UPDATE_HACKING_GAME_STATE(sCommonStruct, HACKING_GAME_FAIL)
		SET_BIT(sControllerStruct.iBS, ciHGBS_TIMED_OUT)
	ENDIF
		
	sCommonStruct.iDrawCounter++
	IF sCommonStruct.iDrawCounter >= ciHG_DRAW_COUNTER
		sCommonStruct.iDrawCounter = 0
	ENDIF
ENDPROC

PROC PROCESS_HACKING_GAME_PASS(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sCommonStruct, INT iDelayTime, STRING sClipName)
	UNUSED_PARAMETER(sControllerStruct)
	IF NOT HAS_NET_TIMER_STARTED(sCommonStruct.tdEndDelay)
		REINIT_NET_TIMER(sCommonStruct.tdEndDelay)
		PRINTLN("[HACKING_GAME] Pass = Starting timer")
		LOAD_BINK_MOVIE(sCommonStruct, sClipName)
		
		PLAY_SOUND_FRONTEND(-1, "Hack_Success", sCommonStruct.sAudioSet)
	ELSE
		IF MAINTAIN_BINK_MOVIE(sCommonStruct)
		OR HAS_NET_TIMER_EXPIRED(sCommonStruct.tdEndDelay, iDelayTime)
			PRINTLN("[HACKING_GAME] Pass = Moving to cleanup")
			UPDATE_HACKING_GAME_STATE(sCommonStruct, HACKING_GAME_CLEANUP)
			SET_BIT(sControllerStruct.iBS, ciHGBS_PASSED)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HACKING_GAME_FAIL(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sCommonStruct, INT iDelayTime, STRING sClipName)
	UNUSED_PARAMETER(sControllerStruct)
	IF NOT HAS_NET_TIMER_STARTED(sCommonStruct.tdEndDelay)
		REINIT_NET_TIMER(sCommonStruct.tdEndDelay)
		PRINTLN("[HACKING_GAME] Fail = Starting time")
		LOAD_BINK_MOVIE(sCommonStruct, sClipName)
		
		PLAY_SOUND_FRONTEND(-1, "Hack_Failed", sCommonStruct.sAudioSet)
		
	ELSE
		IF MAINTAIN_BINK_MOVIE(sCommonStruct)
		OR HAS_NET_TIMER_EXPIRED(sCommonStruct.tdEndDelay, iDelayTime)
			PRINTLN("[HACKING_GAME] Fail = Moving to cleanup")
			
		IF sCommonStruct.iCurrentLives <= 0
			SET_BIT(sControllerStruct.iBS, ciHGBS_OUT_OF_LIVES)
		ENDIF
			
			SET_BIT(sControllerStruct.iBS, ciHGBS_FAILED)
			UPDATE_HACKING_GAME_STATE(sCommonStruct, HACKING_GAME_CLEANUP)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HACKING_GAME_VISUAL_TEST(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sGameStruct)
	UNUSED_PARAMETER(sControllerStruct)
	
	sGameStruct.iDrawCounter++
	IF sGameStruct.iDrawCounter >= ciHG_DRAW_COUNTER
		sGameStruct.iDrawCounter = 0
	ENDIF
ENDPROC

PROC DRAW_HACKING_GAME_WINDOW_1(HACKING_GAME_STRUCT &sCommonStruct)

	sCommonStruct.iBackgroundAnimFrames[0] += sCommonStruct.iDefaultUpdateFrames
	
	IF sCommonStruct.iBackgroundAnimFrames[0] >= ciHG_tech1_FRAME_COUNT
		sCommonStruct.iBackgroundAnimFrames[0] = 0
	ENDIF
	
	// Setup dictionary
	TEXT_LABEL_23 tl23Dict = "mphackinggamewin"
	
	IF sCommonStruct.iBackgroundAnimFrames[0] > 4
		tl23Dict += "1"
	ENDIF
	
	// assemble sprite name
	TEXT_LABEL_23 tl23Sprite = "tech_1_"
	tl23Sprite += sCommonStruct.iBackgroundAnimFrames[0]
	
	// Draw it
	DRAW_HACKING_SPRITE( tl23Dict, tl23Sprite, cfHG_tech1_XPOS, cfHG_tech1_YPOS, cfHG_tech1_WIDTH, cfHG_tech1_HEIGHT, 0.0, 255, 255, 255, 255)


ENDPROC

PROC DRAW_HACKING_GAME_WINDOW_2(HACKING_GAME_STRUCT &sCommonStruct)

	sCommonStruct.iBackgroundAnimFrames[1] += sCommonStruct.iDefaultUpdateFrames
	
	IF sCommonStruct.iBackgroundAnimFrames[1] >= ciHG_tech2_FRAME_COUNT
		sCommonStruct.iBackgroundAnimFrames[1] = 0
	ENDIF
	
	// Setup dictionary
	TEXT_LABEL_23 tl23Dict = "mphackinggamewin2"
	
	IF sCommonStruct.iBackgroundAnimFrames[1] > 8
		tl23Dict += "_3"
	ELIF sCommonStruct.iBackgroundAnimFrames[1] > 5
		tl23Dict += "_2"
	ELIF sCommonStruct.iBackgroundAnimFrames[1] > 2
		tl23Dict += "_1"
	ENDIF
	
	// assemble sprite name
	TEXT_LABEL_23 tl23Sprite = "tech_2_"
	tl23Sprite += sCommonStruct.iBackgroundAnimFrames[1]
	
	// Draw it
	DRAW_HACKING_SPRITE( tl23Dict, tl23Sprite, cfHG_tech2_XPOS, cfHG_tech2_YPOS, cfHG_tech2_WIDTH, cfHG_tech2_HEIGHT, 0.0, 255, 255, 255, 255)


ENDPROC

PROC DRAW_PLAY_AREA_COMMON(HACKING_GAME_STRUCT &sCommonStruct)
	UNUSED_PARAMETER(sCommonStruct)
	//DRAW_RECT_FROM_CORNER(0.0, 0.0, 1.0, 1.0, 0, 0, 0, 255)

	DRAW_HACKING_SPRITE("mphackinggamebg", "bg", cfHG_BG_XPOS, cfHG_BG_YPOS, cfHG_BG_WIDTH, cfHG_BG_HEIGHT, 0.0, 255, 255, 255, 255)
	
	//DRAW_HACKING_SPRITE("mphackinggamewin", "tech_1_0", cfHG_tech1_XPOS, cfHG_tech1_YPOS, cfHG_tech1_WIDTH, cfHG_tech1_HEIGHT, 0.0, 255, 255, 255, 255)
	//DRAW_HACKING_ANIMATED_SPRITE(sCommonStruct, sCommonStruct.iBackgroundAnimFrames[0], ciHG_tech1_FRAME_COUNT, HG_ANIMATION_FAST, "mphackinggamewin", "tech_1_", cfHG_tech1_XPOS, cfHG_tech1_YPOS, cfHG_tech1_WIDTH, cfHG_tech1_HEIGHT, 0.0, 255, 255, 255, 255 )
	DRAW_HACKING_GAME_WINDOW_1(sCommonStruct)
	
	//DRAW_HACKING_ANIMATED_SPRITE(sCommonStruct, sCommonStruct.iBackgroundAnimFrames[1], ciHG_tech2_FRAME_COUNT, HG_ANIMATION_FAST, "mphackinggamewin2", "tech_2_", cfHG_tech2_XPOS, cfHG_tech2_YPOS, cfHG_tech2_WIDTH, cfHG_tech2_HEIGHT, 0.0, 255, 255, 255, 255)
	DRAW_HACKING_GAME_WINDOW_2(sCommonStruct)
	
	DRAW_HACKING_ANIMATED_SPRITE(sCommonStruct, sCommonStruct.iBackgroundAnimFrames[2], ciHG_tech3_FRAME_COUNT, HG_ANIMATION_DEFAULT, "mphackinggamewin", "tech_3_", cfHG_tech3_XPOS, cfHG_tech3_YPOS, cfHG_tech3_WIDTH, cfHG_tech3_HEIGHT, 0.0, 255, 255, 255, 255)
	
	DRAW_HACKING_ANIMATED_SPRITE(sCommonStruct, sCommonStruct.iBackgroundAnimFrames[3], ciHG_tech4_FRAME_COUNT, HG_ANIMATION_DEFAULT, "mphackinggamewin3", "tech_4_", cfHG_tech4_XPOS, cfHG_tech4_YPOS, cfHG_tech4_WIDTH, cfHG_tech4_HEIGHT, 0.0, 255, 255, 255, 255)

	#IF IS_DEBUG_BUILD
		IF sCommonStruct.bDebugHideShadow
			EXIT
		ENDIF
	#ENDIF

	DRAW_HACKING_SPRITE("mphackinggame", "Black_BG", cfHG_BG_XPOS, cfHG_BG_YPOS, cfHG_BG_WIDTH, cfHG_BG_HEIGHT, 0.0, 5, 5, 5, ciHG_SHADOW_ALPHA)
ENDPROC

PROC DRAW_PLAY_AREA_OVERLAY(HACKING_GAME_STRUCT &sCommonStruct)
	#IF IS_DEBUG_BUILD
		IF sCommonStruct.bDebugHideOverlay
			EXIT
		ENDIF
	#ENDIF

	DRAW_HACKING_SPRITE("mphackinggameoverlay", "grid_rgb_pixels", cfHG_BG_XPOS, cfHG_BG_YPOS, cfHG_BG_WIDTH, cfHG_BG_HEIGHT, 0.0, ciHG_OVERLAY_R, ciHG_OVERLAY_G, ciHG_OVERLAY_B, ciHG_OVERLAY_ALPHA)
	DRAW_HACKING_SPRITE("mphackinggameoverlay1", "ScreenGrid", cfHG_BG_XPOS, cfHG_BG_YPOS, cfHG_BG_WIDTH, cfHG_BG_HEIGHT, 0.0, ciHG_OVERLAY_R, ciHG_OVERLAY_G, ciHG_OVERLAY_B, ciHG_OVERLAY_ALPHA)
	PRINTLN("[MC][HackingGame] Drawing Overlay")

ENDPROC

PROC DRAW_PLAY_AREA_OVERLAY_RETRO()
	DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro", "PIXEL_OVERLAY", cfHG_BG_XPOS, cfHG_BG_YPOS, 1920.0, 1080.0, 0.0, 255,255,255,255)
ENDPROC

PROC DRAW_HACKING_GAME_LIVES(HACKING_GAME_STRUCT &sGameStruct, BOOL IsVertical, FLOAT fPositionX, FLOAT fPositionY, FLOAT fWidth, FLOAT fHeight, HG_RGBA_COLOUR_STRUCT sColourStruct, FLOAT fOffset )
	INT i
	
	FLOAT fCustomPositionX
	FLOAT fCustomPositionY
	
	FOR i = 1 TO sGameStruct.iCurrentLives
	  IF IsVertical
	  	fCustomPositionX = fPositionX
		fCustomPositionY = fPositionY + (fOffset * i)
	  ELSE
	  	fCustomPositionX = fPositionX + (fOffset * i)
		fCustomPositionY = fPositionY
	  ENDIF
	  
	  DRAW_HACKING_SPRITE("mphackinggame", "Life", fCustomPositionX, fCustomPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA / 2 )
	ENDFOR
ENDPROC

PROC SETUP_HACKING_GAME_TIMER_DIGIT(TEXT_LABEL_23 &tTextElement, INT iDigt)
	tTextElement = "Numbers_"
	
	IF iDigt > 9
		tTextElement += 9
	ELIF iDigt < 0
		tTextElement += 0
	ELSE
		tTextElement += iDigt
	ENDIF
	
ENDPROC

PROC DRAW_HACKING_GAME_TIMER(HACKING_GAME_STRUCT &sGameStruct, int iTotalTime, FLOAT fPositionX, FLOAT fPositionY, FLOAT fWidth, FLOAT fHeight, HG_RGBA_COLOUR_STRUCT sColourStruct, FLOAT fDigitDifference, BOOL bUpdate = TRUE)
	
	IF HAS_NET_TIMER_STARTED(sGameStruct.tdTimer)
		INT iTimeLeft = iTotalTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sGameStruct.tdTimer)

		IF sGameStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
		OR NOT bUpdate
			iTimeLeft = iTotalTime
		ENDIF

		IF iTimeLeft < 0
			PRINTLN("[HACKINGGAME] Game Timer less than 0")
		ENDIF
		
		INT iMinutes        = (iTimeLeft/1000)/60
    	INT iSeconds        = (iTimeLeft-(iMinutes*60*1000))/1000
    	INT iMiliseconds    =  iTimeLeft-((iSeconds+(iMinutes*60))*1000)
		
		TEXT_LABEL_23 tElementName


		// Draw the individual digits using the central position as a starting point
		// Minutes
		IF iMinutes <= 0
			DRAW_HACKING_SPRITE("mphackinggame", "Numbers_0", fPositionX - (fDigitDifference*2), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
			DRAW_HACKING_SPRITE("mphackinggame", "Numbers_0", fPositionX - fDigitDifference, fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
		ELSE
			//first Digit
			
			SETUP_HACKING_GAME_TIMER_DIGIT(tElementName, iMinutes % 10 )
			
			DRAW_HACKING_SPRITE("mphackinggame", tElementName, fPositionX - (fDigitDifference), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
			
			iMinutes /= 10
			
			SETUP_HACKING_GAME_TIMER_DIGIT(tElementName, iMinutes)
			
			// second Digit
			DRAW_HACKING_SPRITE("mphackinggame", tElementName, fPositionX - (fDigitDifference*2), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
		ENDIF

		// Colon
		DRAW_HACKING_SPRITE("mphackinggame", "Numbers_Colon", fPositionX, fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )

		// Seconds
		//first Digit
			SETUP_HACKING_GAME_TIMER_DIGIT(tElementName, iSeconds % 10)
			
			DRAW_HACKING_SPRITE("mphackinggame", tElementName, fPositionX + (fDigitDifference * 2), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
			
			iSeconds /= 10
			
			SETUP_HACKING_GAME_TIMER_DIGIT(tElementName, iSeconds)
			
			// second Digit
			DRAW_HACKING_SPRITE("mphackinggame", tElementName, fPositionX + fDigitDifference, fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
		
		DRAW_HACKING_SPRITE("mphackinggame", "Numbers_Colon", fPositionX + (fDigitDifference * 3), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
		
		// Miliseconds
			
			SETUP_HACKING_GAME_TIMER_DIGIT(tElementName, iMiliseconds % 10)
			
			DRAW_HACKING_SPRITE("mphackinggame", tElementName, fPositionX + (fDigitDifference * 4), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
			
			iMiliseconds /= 10
			
			iMiliseconds = iMiliseconds % 10
			
			SETUP_HACKING_GAME_TIMER_DIGIT(tElementName, iMiliseconds)
			// second Digit
			DRAW_HACKING_SPRITE("mphackinggame", tElementName, fPositionX + (fDigitDifference * 5), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
	
	ENDIF
ENDPROC

PROC PLAY_SCRAMBLER_AUDIO(HACKING_GAME_STRUCT &sGameStruct, INT iCurrentScrambleSegmentCount, INT maxSegments)
	
	FLOAT floatPercentageOfScrambleCount = (TO_FLOAT(iCurrentScrambleSegmentCount) / TO_FLOAT(maxSegments)) *100
	INT percentageOfScrambleCount = FLOOR(floatPercentageOfScrambleCount)
	IF percentageOfScrambleCount > ciHG_SCRAMBLE_START_OF_MEDIUM_INTENSITY
		PLAY_SOUND_FRONTEND(-1, "Scramble_Countdown_Low", sGameStruct.sAudioSet )
		PRINTLN("[HACKINGGAME] PLAY_SCRAMBLER_AUDIO Scramble_Countdown_Low, percentage = ", percentageOfScrambleCount)
	ELIF percentageOfScrambleCount < ciHG_SCRAMBLE_START_OF_HIGH_INTENSITY
		PLAY_SOUND_FRONTEND(-1, "Scramble_Countdown_High", sGameStruct.sAudioSet )
		PRINTLN("[HACKINGGAME] PLAY_SCRAMBLER_AUDIO Scramble_Countdown_High, percentage = ", percentageOfScrambleCount)
	ELSE 
		PLAY_SOUND_FRONTEND(-1, "Scramble_Countdown_Med", sGameStruct.sAudioSet )
		PRINTLN("[HACKINGGAME] PLAY_SCRAMBLER_AUDIO Scramble_Countdown_Medium, percentage = ", percentageOfScrambleCount)
	ENDIF
ENDPROC

PROC DRAW_HACKING_GAME_SCRAMBLER(HACKING_GAME_STRUCT &sGameStruct, int iTotalTime, FLOAT fPositionX, FLOAT fPositionY, FLOAT fWidth, FLOAT fHeight, HG_RGBA_COLOUR_STRUCT sColourStruct, FLOAT fLeftMost, FLOAT fDiff, FlOAT fElementWidth, FLOAT fElementHeight, BOOL bShouldAnimate, FLOAT fElementOffsetY, BOOL bIsScrambling)
	
	INT timeLeft
	
	IF (HAS_NET_TIMER_STARTED(sGameStruct.tdScrambleTimer)
	AND sGameStruct.eHackingGameState = HACKING_GAME_PLAY)
	AND bShouldAnimate
		timeLeft = iTotalTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sGameStruct.tdScrambleTimer)
		PRINTLN("[HACKINGGAME] Scramble Time left is ", timeLeft)
		
		IF timeLeft < 0
			PRINTLN("[HACKINGGAME] Scramble Timer less than 0")
		ENDIF
	ELSE
		timeLeft = iTotalTime
	ENDIF
	
	DRAW_HACKING_SPRITE("mphackinggame", "Scrambler_BG", fPositionX, fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA / 2)
	
	// Draw however many segements are needed
	FLOAT percentageOfSegments = TO_FLOAT(timeLeft) / TO_FLOAT(iTotalTime)
	percentageOfSegments = percentageOfSegments * 100
	PRINTLN("[HACKINGGAME] [DRAW_SCRAMBLE] Percentage of segments is ", percentageOfSegments)
	
	FLOAT numberOfSegments = (TO_FLOAT(ciHG_SCRAMBLE_MAX_SEGMENTS) / 100)
	numberOfSegments = numberOfSegments * percentageOfSegments
	
	sGameStruct.iCurrentScrambleSegmentCount = FLOOR(numberOfSegments)
	
	IF sGameStruct.iCurrentScrambleSegmentCount != sGameStruct.iPreviousScrambleSegmentCount
	AND NOT bIsScrambling
		PLAY_SCRAMBLER_AUDIO(sGameStruct, sGameStruct.iCurrentScrambleSegmentCount, ciHG_SCRAMBLE_MAX_SEGMENTS)
	ENDIF
	
	PRINTLN("[HACKINGGAME] Number of segments is ", numberOfSegments)
	INT i
	FLOAT posX
	FOR i = 1 TO sGameStruct.iCurrentScrambleSegmentCount
		posX = (fPositionX - fLeftMost) + fDiff*i
		DRAW_HACKING_SPRITE("mphackinggame", "Scrambler_Fill_Segment", posX, fPositionY+fElementOffsetY, fElementWidth, fElementHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
	ENDFOR

	sGameStruct.iPreviousScrambleSegmentCount = sGameStruct.iCurrentScrambleSegmentCount
ENDPROC

PROC DRAW_HACKING_GAME_DECOR(HACKING_GAME_STRUCT &sGameStruct, STRING sDictionary, FLOAT fPositionX, FLOAT fPositionY, FLOAT fWidth, FLOAT fHeight, HG_RGBA_COLOUR_STRUCT sColourStruct)
	UNUSED_PARAMETER(sGameStruct)
	
	sGameStruct.iTecharationCounter += sGameStruct.iSlowUpdateFrames
	
	IF sGameStruct.iTecharationCounter >= ciHG_TECHARATION_FRAMES
		sGameStruct.iTecharationCounter = 0
	ENDIF
	
	// assemble sprite name
	TEXT_LABEL_23 tl23Sprite = "techaration_"
	tl23Sprite += sGameStruct.iTecharationCounter
	
	TEXT_LABEL_23 tl23Dict = sDictionary
	
	IF sGameStruct.iTecharationCounter > 1
		tl23Dict += "1"
	ENDIF
	
	// Draw it
	DRAW_HACKING_SPRITE(tl23Dict, tl23Sprite,  fPositionX, fPositionY, fWidth, fHeight, 0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA)
ENDPROC

PROC HACKING_GAME_UPDATE_TELEMETRY(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sGameStruct, INT iType, INT iPlaythroughId, INT iHashGame, INT iStep)
	INT iActualStep
	
	// Time
	HG_STORE_END_TIME(sControllerStruct, sGameStruct.tdTimer, sGameStruct.iMaxTime)
	
	INT timeElapsed = sControllerStruct.iTimeElapsed
	
	PRINTLN("[HACKING_GAME] Quit time = ", timeElapsed)
	
	// STEP
	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED)
		iActualStep = 0
	ELSE	
		iActualStep = iStep
	ENDIF
	
	PLAYSTATS_HEIST3_HACK(iType, iPlaythroughId, iHashGame, IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED), iActualStep, timeElapsed )
ENDPROC

PROC HACKING_GAME_UPDATE_TELEMETRY_H4(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sGameStruct, INT iPlaythroughId, INT iHashGame, INT iStep, INT iTimeElapsed = 0)
	INT iActualStep
	
	// Time
	HG_STORE_END_TIME(sControllerStruct, sGameStruct.tdTimer, sGameStruct.iMaxTime)

	// STEP
	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED)
		iActualStep = 0
	ELSE	
		iActualStep = iStep
	ENDIF
	
	PLAYSTATS_HEIST4_HACK(iPlaythroughId, iHashGame, IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED), iActualStep, iTimeElapsed)
ENDPROC

PROC HACKING_COMMON_CLEANUP(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sGameStruct, BOOL bFinalCleanup = FALSE)
	IF sGameStruct.eHackingGameState != HACKING_GAME_CLEANUP
		PRINTLN("[HACKING_GAME] TRYING TO CLEAN UP WHEN NOT READY TO CLEAN UP")
	ENDIF
	
	
	DEBUG_PRINTCALLSTACK()
	Unpause_Objective_Text()
	//BEAMHACK_END_LOOPING_SOUNDS()
	g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE
	MP_FORCE_TERMINATE_INTERNET_CLEAR()
	//Renable notifications
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	//Disable multiple displays/blinders.
	SET_MULTIHEAD_SAFE(FALSE, TRUE)
	ENABLE_INTERACTION_MENU()
	IF NOT bFinalCleanup
		IF IS_SKYSWOOP_AT_GROUND()
		AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
	ENDIF
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHackingGame")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHackingGameBG")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHackingGameWin")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHackingGameWin1")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHackingGameWin2")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHackingGameWin2_1")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHackingGameWin2_2")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHackingGameWin2_3")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHackingGameWin3")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHackingGameoverlay")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHackingGameoverlay1")
	
	IF sGameStruct.movieId != NULL
		PRINTLN("[HACKING_GAME] Releasing Bink Movie")
		STOP_BINK_MOVIE(sGameStruct.movieId)
		RELEASE_BINK_MOVIE(sGameStruct.movieId)
	ENDIF
	
	//RESET_NET_TIMER(sBeamHackGameplayData.tdBeamEndDelay)
	UPDATE_HACKING_GAME_STATE(sGameStruct, HACKING_GAME_SLEEP)
	sControllerStruct.iBS = 0
ENDPROC

FUNC BOOL HACKING_IS_SAFE_TO_HACK()
	IF IS_CELLPHONE_CAMERA_IN_USE()
		RETURN FALSE
	ENDIF	
	
	IF IS_PHONE_ONSCREEN()
		RETURN FALSE
	ENDIF	
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF
	
	IF Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) >= 10
		RETURN FALSE
	ENDIF
	
	IF g_SpawnData.bPassedOutDrunk 
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_RAGDOLL(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME pause menu active")
		RETURN FALSE
	ENDIF
	
	IF IS_SELECTOR_ONSCREEN()
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME selector is open")
		RETURN FALSE
	ENDIF

	IF IS_MP_PASSIVE_MODE_ENABLED()
		PRINTLN("[AM_ARC_CAB] IS_SAFE_TO_START_ARCADE_GAME passive mode is active")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


// DEBUG FUNCTIONS

