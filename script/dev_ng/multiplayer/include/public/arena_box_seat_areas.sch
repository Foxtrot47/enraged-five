///    Author 	: 	Alexander Murphy
///    Team 	: 	Online Tech
///    Info		: 	Keeping all the seating area data in one place because the script controlling the
///    				seating area and the seating area script both need to know when the seating area
///    				script should be running.
///    				FUNC BOOL 	IS_ENTITY_IN_[AREA]

USING "commands_entity.sch"
USING "script_ped.sch"

FUNC BOOL IS_LOCAL_PLAYER_IN_ARENA_BOX_BENCH_SEAT_AREA()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2797.987793,-3943.898438,181.000488>>, <<2797.949219,-3919.850342,187.411407>>, 30.0)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
