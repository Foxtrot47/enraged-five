USING "globals.sch"
USING "mp_globals_fm.sch"
USING "fmmc_vars.sch" 
USING "net_heists_hardcoded.sch"
USING "script_conversion.sch"
USING "net_masks.sch"
USING "net_realty_new.sch"

CONST_FLOAT MAP_BOUNDS_POS_X 7000.0
CONST_FLOAT MAP_BOUNDS_NEG_X -7000.0
CONST_FLOAT MAP_BOUNDS_POS_Y 8000.0
CONST_FLOAT MAP_BOUNDS_NEG_Y -7000.0
CONST_FLOAT MAP_BOUNDS_POS_Z 2000.0


FUNC INT GET_NUMBER_OF_HESIT_SETUP_MISSIONS_FROM_CASH_BAND(INT iCashReward)

	INT ireturn = 5

	SWITCH iCashReward
	
		CASE FMMC_CASH_ORNATE_BANK
			ireturn = 5
		BREAK
		CASE FMMC_CASH_PRISON_BREAK
			ireturn = 5
		BREAK
		CASE FMMC_CASH_BIOLAB
			ireturn = 5
		BREAK
		
	ENDSWITCH
	
	RETURN ireturn

ENDFUNC

FUNC INT GET_BONUS_CASH_GAINED(INT iCurrentHeistMissionIndex)

	IF NOT(g_FMMC_STRUCT.iDifficulity = DIFF_EASY)

		IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
			IF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_IAABASE_FINALE
				IF g_sMPTunables.IH2_CASH_REWARD_ELITE_OBJECTIVES_IAA_JOB > 0
					PRINTLN("[JS][CASH_RWD] - GET_BONUS_CASH_GAINED - IAA using tunable value: ", g_sMPTunables.IH2_CASH_REWARD_ELITE_OBJECTIVES_IAA_JOB)
					RETURN g_sMPTunables.IH2_CASH_REWARD_ELITE_OBJECTIVES_IAA_JOB
				ENDIF
			ELIF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE
				IF g_sMPTunables.IH2_CASH_REWARD_ELITE_OBJECTIVES_SUB_JOB > 0
					PRINTLN("[JS][CASH_RWD] - GET_BONUS_CASH_GAINED - SUB JOB using tunable value: ", g_sMPTunables.IH2_CASH_REWARD_ELITE_OBJECTIVES_SUB_JOB)
					RETURN g_sMPTunables.IH2_CASH_REWARD_ELITE_OBJECTIVES_SUB_JOB
				ENDIF
			ELIF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2
				IF g_sMPTunables.IH2_CASH_REWARD_ELITE_OBJECTIVES_SILO_OPERATION > 0
					PRINTLN("[JS][CASH_RWD] - GET_BONUS_CASH_GAINED - SILO using tunable value: ", g_sMPTunables.IH2_CASH_REWARD_ELITE_OBJECTIVES_SILO_OPERATION)
					RETURN g_sMPTunables.IH2_CASH_REWARD_ELITE_OBJECTIVES_SILO_OPERATION
				ENDIF
			ENDIF
		ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
			FLOAT fCasinoHeistDifficultyMod = 0
			INT iCasinoHeistCash = 0
			IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
			AND g_sMPTunables.iCH_ELITE_CHALLENGE_REWARD_STEALTH > 0
				PRINTLN("[JS][CASH_RWD] - GET_BONUS_CASH_GAINED - CH STEALTH using tunable value: ", g_sMPTunables.iCH_ELITE_CHALLENGE_REWARD_STEALTH)
				iCasinoHeistCash = g_sMPTunables.iCH_ELITE_CHALLENGE_REWARD_STEALTH
				IF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
				AND g_sMPTunables.fCH_ELITE_CHALLENGE_HARD_MOD_STEALTH > 0.0
					PRINTLN("[JS][CASH_RWD] - GET_BONUS_CASH_GAINED - CH STEALTH using tunable hard mod: ", g_sMPTunables.fCH_ELITE_CHALLENGE_HARD_MOD_STEALTH)
					fCasinoHeistDifficultyMod = g_sMPTunables.fCH_ELITE_CHALLENGE_HARD_MOD_STEALTH
				ENDIF
			ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			AND g_sMPTunables.iCH_ELITE_CHALLENGE_REWARD_SUBTERFUGE > 0
				PRINTLN("[JS][CASH_RWD] - GET_BONUS_CASH_GAINED - CH SUBTERFUGE using tunable value: ", g_sMPTunables.iCH_ELITE_CHALLENGE_REWARD_SUBTERFUGE)
				iCasinoHeistCash = g_sMPTunables.iCH_ELITE_CHALLENGE_REWARD_SUBTERFUGE
				IF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
				AND g_sMPTunables.fCH_ELITE_CHALLENGE_HARD_MOD_SUBTERFUGE > 0.0
					PRINTLN("[JS][CASH_RWD] - GET_BONUS_CASH_GAINED - CH STEALTH using tunable hard mod: ", g_sMPTunables.fCH_ELITE_CHALLENGE_HARD_MOD_SUBTERFUGE)
					fCasinoHeistDifficultyMod = g_sMPTunables.fCH_ELITE_CHALLENGE_HARD_MOD_SUBTERFUGE
				ENDIF
			ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT
			AND g_sMPTunables.iCH_ELITE_CHALLENGE_REWARD_DIRECT > 0
				PRINTLN("[JS][CASH_RWD] - GET_BONUS_CASH_GAINED - CH DIRECT using tunable value: ", g_sMPTunables.iCH_ELITE_CHALLENGE_REWARD_DIRECT)
				iCasinoHeistCash = g_sMPTunables.iCH_ELITE_CHALLENGE_REWARD_DIRECT
				IF g_FMMC_STRUCT.iDifficulity = DIFF_HARD
				AND g_sMPTunables.fCH_ELITE_CHALLENGE_HARD_MOD_DIRECT > 0.0
					PRINTLN("[JS][CASH_RWD] - GET_BONUS_CASH_GAINED - CH STEALTH using tunable hard mod: ", g_sMPTunables.fCH_ELITE_CHALLENGE_HARD_MOD_DIRECT)
					fCasinoHeistDifficultyMod = g_sMPTunables.fCH_ELITE_CHALLENGE_HARD_MOD_DIRECT
				ENDIF
			ENDIF
			
			IF fCasinoHeistDifficultyMod > 0.0
				PRINTLN("[JS][CASH_RWD] - GET_BONUS_CASH_GAINED - using difficulty modifier: ", fCasinoHeistDifficultyMod)
				iCasinoHeistCash = ROUND(iCasinoHeistCash * fCasinoHeistDifficultyMod)
			ENDIF
			
			IF iCasinoHeistCash > 0
				PRINTLN("[JS][CASH_RWD] - GET_BONUS_CASH_GAINED - final value: ", iCasinoHeistCash)
				RETURN iCasinoHeistCash
			ENDIF
		ELSE
			IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE2
				IF g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Pacific_Standard > 0
					PRINTLN("[RCC MISSION] [CASH_RWD] GET_BONUS_CASH_GAINED - return g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Pacific_Standard = ",g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Pacific_Standard)
					RETURN g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Pacific_Standard
				ENDIF
			ELIF iCurrentHeistMissionIndex = HBCA_BS_HUMANE_LABS_FINALE
				IF g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Humane_Labs > 0
					PRINTLN("[RCC MISSION] [CASH_RWD] GET_BONUS_CASH_GAINED - return g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Humane_Labs = ",g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Humane_Labs)
					RETURN g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Humane_Labs
				ENDIF
			ELIF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
				IF g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Prison_Break > 0
					PRINTLN("[RCC MISSION] [CASH_RWD] GET_BONUS_CASH_GAINED - return g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Prison_Break = ",g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Prison_Break)
					RETURN g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Prison_Break
				ENDIF
			ELIF iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_FINALE
				IF g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Series_A  > 0
					PRINTLN("[RCC MISSION] [CASH_RWD] GET_BONUS_CASH_GAINED - return g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Series_A = ",g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Series_A)
					RETURN g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Series_A 
				ENDIF
			ELIF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_FINALE
				IF g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Fleeca  > 0
					PRINTLN("[RCC MISSION] [CASH_RWD] GET_BONUS_CASH_GAINED - return g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Fleeca = ",g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Fleeca)
					RETURN g_sMPTunables.Cash_Reward_Override_Elite_Objectives_Fleeca 
				ENDIF
			ENDIF
		ENDIF
		
		
		PRINTLN("[RCC MISSION] [CASH_RWD] GET_BONUS_CASH_GAINED - return g_fmmc_struct.iHeistEliteChallengeReward = ",g_fmmc_struct.iHeistEliteChallengeReward)
		RETURN g_fmmc_struct.iHeistEliteChallengeReward
	ELSE
		PRINTLN("[RCC MISSION] [CASH_RWD] GET_BONUS_CASH_GAINED - EASYMODE - no cash")
		RETURN 0
	ENDIF
	
ENDFUNC

FUNC INT GET_BONUS_TIME_THRESHOLD(INT iCurrentHeistMissionIndex)

	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
		IF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_IAABASE_FINALE
			IF g_sMPTunables.IH2_ELITE_OBJECTIVES_TIME_IAA_JOB > 0
				PRINTLN("[JS][CASH_RWD] - GET_BONUS_TIME_THRESHOLD - IAA using tunable value: ", g_sMPTunables.IH2_ELITE_OBJECTIVES_TIME_IAA_JOB)
				RETURN g_sMPTunables.IH2_ELITE_OBJECTIVES_TIME_IAA_JOB
			ENDIF
		ELIF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE
			IF g_sMPTunables.IH2_ELITE_OBJECTIVES_TIME_SUB_JOB > 0
				PRINTLN("[JS][CASH_RWD] - GET_BONUS_TIME_THRESHOLD - SUB JOB using tunable value: ", g_sMPTunables.IH2_ELITE_OBJECTIVES_TIME_SUB_JOB)
				RETURN g_sMPTunables.IH2_ELITE_OBJECTIVES_TIME_SUB_JOB
			ENDIF
		ELIF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2
			IF g_sMPTunables.IH2_ELITE_OBJECTIVES_TIME_SILO_OPERATION > 0
				PRINTLN("[JS][CASH_RWD] - GET_BONUS_TIME_THRESHOLD - SILO using tunable value: ", g_sMPTunables.IH2_ELITE_OBJECTIVES_TIME_SILO_OPERATION)
				RETURN g_sMPTunables.IH2_ELITE_OBJECTIVES_TIME_SILO_OPERATION
			ENDIF
		ENDIF
	ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
			PRINTLN("[JS][CASH_RWD] - GET_BONUS_TIME_THRESHOLD - CH STEALTH using tunable value: ", g_sMPTunables.iCH_ELITE_OBJECTIVES_TIME_STEALTH)
			RETURN g_sMPTunables.iCH_ELITE_OBJECTIVES_TIME_STEALTH
		ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			PRINTLN("[JS][CASH_RWD] - GET_BONUS_TIME_THRESHOLD - CH SUBTERFUGE using tunable value: ", g_sMPTunables.iCH_ELITE_OBJECTIVES_TIME_SUBTERFUGE)
			RETURN g_sMPTunables.iCH_ELITE_OBJECTIVES_TIME_SUBTERFUGE
		ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT
			PRINTLN("[JS][CASH_RWD] - GET_BONUS_TIME_THRESHOLD - CH DIRECT using tunable value: ", g_sMPTunables.iCH_ELITE_OBJECTIVES_TIME_DIRECT)
			RETURN g_sMPTunables.iCH_ELITE_OBJECTIVES_TIME_DIRECT
		ENDIF
	ELSE
		IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE2
			IF g_sMPTunables.Target_Override_Elite_Objectives_Pacific_Time > 0
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Pacific_Time
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_HUMANE_LABS_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Humane_Labs_Time > 0
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Humane_Labs_Time
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Prison_Time  > 0
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Prison_Time  
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Series_A_Time > 0
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Series_A_Time 
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Fleeca_Time  > 0
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Fleeca_Time
			ENDIF
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT.iHeistRewardTimeThreshold
	
ENDFUNC

FUNC INT GET_BONUS_VEHICLE_DAMAGE_THRESHOLD(INT iCurrentHeistMissionIndex)

	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
		IF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE
			IF g_sMPTunables.iH2_ELITE_TARGET_SUB_JOB_AVENGER_DAMAGE > -1
				RETURN g_sMPTunables.iH2_ELITE_TARGET_SUB_JOB_AVENGER_DAMAGE
			ENDIF
		ENDIF
	ELSE
		IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE2
			IF g_sMPTunables.Target_Override_Elite_Objectives_Pacific_Vehicle_Damage > -1
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Pacific_Vehicle_Damage
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_HUMANE_LABS_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Humane_Labs_Vehicle_Damage > -1
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Humane_Labs_Vehicle_Damage
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Prison_Vehicle_Damage  > -1
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Prison_Vehicle_Damage
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Series_A_Vehicle_Damage > -1
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Series_A_Vehicle_Damage
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Fleeca_Vehicle_Damage > -1
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Fleeca_Vehicle_Damage 
			ENDIF
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT.iHeistRewardVehDamageThreshold
	
ENDFUNC

FUNC INT GET_BONUS_PED_DAMAGE_THRESHOLD(INT iCurrentHeistMissionIndex)

	IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE2
		IF g_sMPTunables.Target_Override_Elite_Objectives_Pacific_Ped_Damage  > -1
			RETURN g_sMPTunables.Target_Override_Elite_Objectives_Pacific_Ped_Damage 
		ENDIF
	ELIF iCurrentHeistMissionIndex = HBCA_BS_HUMANE_LABS_FINALE
		IF g_sMPTunables.Target_Override_Elite_Objectives_Humane_Labs_Ped_Damage > -1
			RETURN g_sMPTunables.Target_Override_Elite_Objectives_Humane_Labs_Ped_Damage
		ENDIF
	ELIF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
		IF g_sMPTunables.Target_Override_Elite_Objectives_Prison_Ped_Damage   > -1
			RETURN g_sMPTunables.Target_Override_Elite_Objectives_Prison_Ped_Damage 
		ENDIF
	ELIF iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_FINALE
		IF g_sMPTunables.Target_Override_Elite_Objectives_Series_A_Ped_Damage  > -1
			RETURN g_sMPTunables.Target_Override_Elite_Objectives_Series_A_Ped_Damage 
		ENDIF
	ELIF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_FINALE
		IF g_sMPTunables.Target_Override_Elite_Objectives_Fleeca_Ped_Damage  > -1
			RETURN g_sMPTunables.Target_Override_Elite_Objectives_Fleeca_Ped_Damage 
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT.iHeistRewardPedDamageThreshold
	
ENDFUNC

FUNC INT GET_BONUS_ENEMY_KILLS_THRESHOLD(INT iCurrentHeistMissionIndex)

	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
		IF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_IAABASE_FINALE
			IF g_sMPTunables.iH2_ELITE_TARGET_IAA_JOB_KILLS > -1
				RETURN g_sMPTunables.iH2_ELITE_TARGET_IAA_JOB_KILLS 
			ENDIF
		ENDIF
	ELSE
		IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE2
			IF g_sMPTunables.Target_Override_Elite_Objectives_Pacific_Enemy_Kills   > -1
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Pacific_Enemy_Kills 
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_HUMANE_LABS_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Humane_Labs_Enemy_Kills > -1
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Humane_Labs_Enemy_Kills
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Prison_Enemy_Kills   > -1
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Prison_Enemy_Kills
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Series_A_Enemy_Kills   > -1
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Series_A_Enemy_Kills 
			ENDIF
		ELIF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_FINALE
			IF g_sMPTunables.Target_Override_Elite_Objectives_Fleeca_Enemy_Kills  > -1
				RETURN g_sMPTunables.Target_Override_Elite_Objectives_Fleeca_Enemy_Kills 
			ENDIF
		ENDIF
	ENDIF
	
	RETURN g_FMMC_STRUCT.iHeistRewardKillsThreshold
	
ENDFUNC

FUNC INT GET_BONUS_PLAYER_HEALTH_THRESHOLD(INT iCurrentHeistMissionIndex)

	IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE2
		IF g_sMPTunables.Target_Override_Elite_Objectives_Pacific_Health_Damage  > -1
			RETURN g_sMPTunables.Target_Override_Elite_Objectives_Pacific_Health_Damage 
		ENDIF
	ELIF iCurrentHeistMissionIndex = HBCA_BS_HUMANE_LABS_FINALE
		IF g_sMPTunables.Target_Override_Elite_Objectives_Humane_Labs_Health_Damage > -1
			RETURN g_sMPTunables.Target_Override_Elite_Objectives_Humane_Labs_Health_Damage
		ENDIF
	ELIF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
		IF g_sMPTunables.Target_Override_Elite_Objectives_Prison_Health_Damage   > -1
			RETURN g_sMPTunables.Target_Override_Elite_Objectives_Prison_Health_Damage
		ENDIF
	ELIF iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_FINALE
		IF g_sMPTunables.Target_Override_Elite_Objectives_Series_A_Health_Damage   > -1
			RETURN g_sMPTunables.Target_Override_Elite_Objectives_Series_A_Health_Damage
		ENDIF
	ELIF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_FINALE
		IF g_sMPTunables.Target_Override_Elite_Objectives_Fleeca_Health_Damage  > -1
			RETURN g_sMPTunables.Target_Override_Elite_Objectives_Fleeca_Health_Damage
		ENDIF
	ENDIF
	
	RETURN g_fmmc_Struct.iHeistRewardPlayerHealthThreshold
	
ENDFUNC

FUNC INT GET_BONUS_NUM_HEADSHOTS_THRESHOLD()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
		IF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE
		OR GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2
			IF g_sMPTunables.iH2_ELITE_TARGET_SILO_JOB_HEADSHOTS > -1
				RETURN g_sMPTunables.iH2_ELITE_TARGET_SILO_JOB_HEADSHOTS 
			ENDIF
		ENDIF
	ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT
			RETURN g_sMPTunables.iCH_ELITE_OBJECTIVES_HEADSHOTS_DIRECT
		ENDIF
	ENDIF
	RETURN g_FMMC_STRUCT.iHeistRewardHeadshotThreshold
ENDFUNC

FUNC INT GET_BONUS_NUM_HACK_FAILS_THRESHOLD()
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
		IF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE
		OR GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2
			IF g_sMPTunables.iH2_ELITE_TARGET_SILO_JOB_HACKING_MISTAKES > -1
				RETURN g_sMPTunables.iH2_ELITE_TARGET_SILO_JOB_HACKING_MISTAKES
			ENDIF
		ENDIF
	ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
			RETURN g_sMPTunables.iCH_ELITE_OBJECTIVES_HACK_FAILS_STEALTH
		ENDIF
	ENDIF
	RETURN g_FMMC_STRUCT.iHeistRewardHackThreshold
ENDFUNC

FUNC INT GET_EXTRACTION_TIME_THRESHOLD()

	IF g_sMPTunables.Target_Override_Elite_Objectives_Prison_Extraction   > -1
		RETURN g_sMPTunables.Target_Override_Elite_Objectives_Prison_Extraction
	ENDIF

	RETURN g_FMMC_STRUCT.iHeistRewardMMTimerThreshold
	
ENDFUNC

FUNC BOOL IS_BONUS_DISABLED_FOR_THIS_HEIST(INT iCurrentHeistMissionIndex)

	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
		IF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_IAABASE_FINALE
			IF g_sMPTunables.BH2_DISABLE_ELITE_OBJECTIVES_IAA_JOB
				PRINTLN("[JS][CASH_RWD] - IS_BONUS_DISABLED_FOR_THIS_HEIST - IAA using tunable value: ", g_sMPTunables.BH2_DISABLE_ELITE_OBJECTIVES_IAA_JOB)
				RETURN g_sMPTunables.BH2_DISABLE_ELITE_OBJECTIVES_IAA_JOB
			ENDIF
		ELIF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE
			IF g_sMPTunables.BH2_DISABLE_ELITE_OBJECTIVES_SUB_JOB
				PRINTLN("[JS][CASH_RWD] - IS_BONUS_DISABLED_FOR_THIS_HEIST - SUB JOB using tunable value: ", g_sMPTunables.BH2_DISABLE_ELITE_OBJECTIVES_SUB_JOB)
				RETURN g_sMPTunables.BH2_DISABLE_ELITE_OBJECTIVES_SUB_JOB
			ENDIF
		ELIF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2
			IF g_sMPTunables.BH2_DISABLE_ELITE_OBJECTIVES_SILO_OPERATION
				PRINTLN("[JS][CASH_RWD] - IS_BONUS_DISABLED_FOR_THIS_HEIST - SILO using tunable value: ", g_sMPTunables.BH2_DISABLE_ELITE_OBJECTIVES_SILO_OPERATION)
				RETURN g_sMPTunables.BH2_DISABLE_ELITE_OBJECTIVES_SILO_OPERATION
			ENDIF
		ENDIF
	ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
		IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
			PRINTLN("[JS][CASH_RWD] - IS_BONUS_DISABLED_FOR_THIS_HEIST - CH STEALTH using tunable value: ", g_sMPTunables.bCH_DISABLE_ELITE_OBJECTIVES_STEALTH)
			RETURN g_sMPTunables.bCH_DISABLE_ELITE_OBJECTIVES_STEALTH
		ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			PRINTLN("[JS][CASH_RWD] - IS_BONUS_DISABLED_FOR_THIS_HEIST - CH SUBTERFUGE using tunable value: ", g_sMPTunables.bCH_DISABLE_ELITE_OBJECTIVES_SUBTERFUGE)
			RETURN g_sMPTunables.bCH_DISABLE_ELITE_OBJECTIVES_SUBTERFUGE
		ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT
			PRINTLN("[JS][CASH_RWD] - IS_BONUS_DISABLED_FOR_THIS_HEIST - CH DIRECT using tunable value: ", g_sMPTunables.bCH_DISABLE_ELITE_OBJECTIVES_DIRECT)
			RETURN g_sMPTunables.bCH_DISABLE_ELITE_OBJECTIVES_DIRECT
		ENDIF
	ELSE
		IF iCurrentHeistMissionIndex = HBCA_BS_PACIFIC_STANDARD_FINALE2
			RETURN g_sMPTunables.Disable_Elite_Objectives_Pacific_Standard_Time
		ELIF iCurrentHeistMissionIndex = HBCA_BS_HUMANE_LABS_FINALE
			RETURN g_sMPTunables.Disable_Elite_Objectives_Humane_Labs_Time 
		ELIF iCurrentHeistMissionIndex = HBCA_BS_PRISON_BREAK_FINALE
			RETURN g_sMPTunables.Disable_Elite_Objectives_Prison_Break_Time
		ELIF iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_FINALE
			RETURN g_sMPTunables.Disable_Elite_Objectives_Series_A_Time 
		ELIF iCurrentHeistMissionIndex = HBCA_BS_TUTORIAL_FINALE
			RETURN g_sMPTunables.Disable_Elite_Objectives_Fleeca
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


PROC MARK_ELITE_CHALLENGE_AS_COMPLETE(INT iHeistIndex)

	SWITCH(iHeistIndex)
		CASE HBCA_BS_TUTORIAL_FINALE
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_HEIST_CHALLENGE_0,TRUE)
			PRINTLN("[RCC MISSION] - FLEECA - PACKED_MP_STAT_COMPLETED_ELITE_HEIST_CHALLENGE_0 = TRUE")
		BREAK
		CASE HBCA_BS_PRISON_BREAK_FINALE
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_HEIST_CHALLENGE_1,TRUE)
			PRINTLN("[RCC MISSION] - PRISON - PACKED_MP_STAT_COMPLETED_ELITE_HEIST_CHALLENGE_1 = TRUE")
		BREAK
		CASE HBCA_BS_HUMANE_LABS_FINALE
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_HEIST_CHALLENGE_2,TRUE)
			PRINTLN("[RCC MISSION] - HUMANE - PACKED_MP_STAT_COMPLETED_ELITE_HEIST_CHALLENGE_2 = TRUE")
		BREAK
		CASE HBCA_BS_SERIES_A_FINALE
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_HEIST_CHALLENGE_3,TRUE)
			PRINTLN("[RCC MISSION] - SERIES A - PACKED_MP_STAT_COMPLETED_ELITE_HEIST_CHALLENGE_3 = TRUE")
		BREAK
		CASE HBCA_BS_PACIFIC_STANDARD_FINALE2
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_HEIST_CHALLENGE_4,TRUE)
			PRINTLN("[RCC MISSION] - PACIFIC - PACKED_MP_STAT_COMPLETED_ELITE_HEIST_CHALLENGE_4 = TRUE")
		BREAK
	ENDSWITCH
ENDPROC

PROC MARK_GANGOPS_ELITE_CHALLENGE_AS_COMPLETE(INT iGangopsMission)
	SWITCH(iGangopsMission)
		CASE ciGANGOPS_FLOW_MISSION_IAABASE_FINALE
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_HEIST2_CHALLENGE_0, TRUE)
			PRINTLN("[RCC MISSION] - IAA - PACKED_MP_STAT_COMPLETED_ELITE_HEIST2_CHALLENGE_0 = TRUE")
		BREAK
		CASE ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_HEIST2_CHALLENGE_1, TRUE)
			PRINTLN("[RCC MISSION] - SUBMARINE - PACKED_MP_STAT_COMPLETED_ELITE_HEIST2_CHALLENGE_1 = TRUE")
		BREAK
		CASE ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_HEIST2_CHALLENGE_2, TRUE)
			PRINTLN("[RCC MISSION] - SILO - PACKED_MP_STAT_COMPLETED_ELITE_HEIST2_CHALLENGE_2 = TRUE")
		BREAK
	ENDSWITCH
	
	BOOL iChallenge0 = GET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_HEIST2_CHALLENGE_0)
	BOOL iChallenge1 = GET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_HEIST2_CHALLENGE_1)
	BOOL iChallenge2 = GET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_HEIST2_CHALLENGE_2)
	
	IF iChallenge0 AND iChallenge1 AND iChallenge2 AND NOT HAS_ACHIEVEMENT_BEEN_AWARDED(ACHGO7)
		SET_GANGOPS_ACHIEVEMENT_DONE(GOPS_ACH_7_DONE)
	ENDIF
ENDPROC

PROC MARK_CASINO_HEIST_CHALLENGE_AS_COMPLETE()
	
	IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_CASINO_HEIST_STEALTH, TRUE)
		PRINTLN("[JS][CASH_RWD] - MARK_CASINO_HEIST_CHALLENGE_AS_COMPLETE - CH STEALTH")
	ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_CASINO_HEIST_SUBTERFUGE, TRUE)
		PRINTLN("[JS][CASH_RWD] - MARK_CASINO_HEIST_CHALLENGE_AS_COMPLETE - CH SUBTERFUGE")
	ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_CASINO_HEIST_DIRECT, TRUE)
		PRINTLN("[JS][CASH_RWD] - MARK_CASINO_HEIST_CHALLENGE_AS_COMPLETE - CH DIRECT")
	ENDIF
	
	BOOL iChallenge0 = GET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_CASINO_HEIST_STEALTH)
	BOOL iChallenge1 = GET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_CASINO_HEIST_SUBTERFUGE)
	BOOL iChallenge2 = GET_PACKED_STAT_BOOL(PACKED_MP_STAT_COMPLETED_ELITE_CASINO_HEIST_DIRECT)
	
	IF iChallenge0 AND iChallenge1 AND iChallenge2 AND NOT GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_ELITETHEIF)
		PRINTLN("[JS][CASH_RWD] - MARK_CASINO_HEIST_CHALLENGE_AS_COMPLETE - AWARDING ELITE THIEF")
		SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_ELITETHEIF, TRUE)
	ENDIF
ENDPROC

FUNC INT ADD_HEIST_BONUS_CASH_REWARDS(INT iCurrentHeistMissionIndex,INT iTotalMissionEndTime, FLOAT fHeistVehDamage, FLOAT fVIPDamage, INT &iTeamKills[], BOOL bSwatInvolved, BOOL bBeatCashGrab, INT iPlayerMostHealthLost, INT iExtractionTime, INT &iTeamDeaths[],BOOL bTripSkipAvailable = FALSE,BOOL bTripSkipUsed = FALSE, INT iHackFails = 0, INT iHeadshots = 0, BOOL bDroppedToDirect = FALSE)
	
	BOOL bGiveBonus = TRUE
	INT iReturnCash = 0
	INT iNumEliteComponents = 0

	PRINTLN("[RCC MISSION] [CASH_RWD] ADD_HEIST_BONUS_CASH_REWARDS")
	
	IF NOT(g_FMMC_STRUCT.iDifficulity = DIFF_EASY)
		IF NOT IS_BONUS_DISABLED_FOR_THIS_HEIST(iCurrentHeistMissionIndex)
		
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset,ci_bs_HeistTimeReward)
			OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
				INT iBonusTimeThreshold = GET_BONUS_TIME_THRESHOLD(iCurrentHeistMissionIndex)
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentMissionTime = TRUE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetMissionTime = iBonusTimeThreshold
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMissionTime = iTotalMissionEndTime
				iNumEliteComponents++
				PRINTLN("[RCC MISSION] [CASH_RWD] Time Bonus Set - ",iTotalMissionEndTime, " <= ",iBonusTimeThreshold)
				
				IF iTotalMissionEndTime <= iBonusTimeThreshold
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMissionTimeChallengeComplete = TRUE
					PRINTLN("[RCC MISSION] [CASH_RWD] adding heist time bonus")
				ELSE
					bGiveBonus = FALSE
				ENDIF
				
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset,ci_bs_VehDamageReward)
				INT iBonusVehicleThreshold = GET_BONUS_VEHICLE_DAMAGE_THRESHOLD(iCurrentHeistMissionIndex)
				INT iHeistVehDamage = CEIL(fHeistVehDamage) //CEIL to make it harder
				PRINTLN("[RCC MISSION] [CASH_RWD] Rounding - ",fHeistVehDamage, " to ",iHeistVehDamage)
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentVehicleDamage = TRUE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageTarget = iBonusVehicleThreshold
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageActual = iHeistVehDamage
				iNumEliteComponents++
				PRINTLN("[RCC MISSION] [CASH_RWD] Vehicle Damage Bonus Set - ",iHeistVehDamage," <= ",iBonusVehicleThreshold)
				
				IF iHeistVehDamage <= iBonusVehicleThreshold
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bVehicleDamagePercentagePassed = TRUE
					PRINTLN("[RCC MISSION] [CASH_RWD] adding heist veh damage bonus")
				ELSE
					bGiveBonus = FALSE
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset,ci_bs_PedDamageReward)
				INT iPedDamageThreshold = GET_BONUS_PED_DAMAGE_THRESHOLD(iCurrentHeistMissionIndex)
				INT iVIPDamage = CEIL(fVIPDamage) //CEIL to make it harder
				PRINTLN("[RCC MISSION] [CASH_RWD] Rounding - ",fVIPDamage, " to ",iVIPDamage)
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentRachDamage = TRUE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageTarget = iPedDamageThreshold
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageActual = iVIPDamage
				iNumEliteComponents++
				PRINTLN("[RCC MISSION] [CASH_RWD] Ped Damage Bonus Set - ",iVIPDamage," <= ",iPedDamageThreshold)
				
				IF iVIPDamage <= iPedDamageThreshold
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bRachDamagePercentagePassed = TRUE
					PRINTLN("[RCC MISSION] [CASH_RWD] adding heist Ped damage bonus")
				ELSE
					bGiveBonus = FALSE	
				ENDIF
			
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset,ci_bs_KillsReward)
				INT iTeamKillThreshold = GET_BONUS_ENEMY_KILLS_THRESHOLD(iCurrentHeistMissionIndex)
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedKills = TRUE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKillsTarget = iTeamKillThreshold
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKills = iTeamKills[0] + iTeamKills[1]+ iTeamKills[2]+ iTeamKills[3]
				iNumEliteComponents++
				PRINTLN("[RCC MISSION] [CASH_RWD] Kills Bonus Set - ",iTeamKills[0] + iTeamKills[1]+ iTeamKills[2]+ iTeamKills[3]," >= ",iTeamKillThreshold)
				
				IF (iTeamKills[0] + iTeamKills[1]+ iTeamKills[2]+ iTeamKills[3]) >= iTeamKillThreshold
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedKillsChallengeComplete = TRUE
					PRINTLN("[RCC MISSION] [CASH_RWD] adding heist Kills bonus")
				ELSE
					bGiveBonus = FALSE		
				ENDIF
			
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset,ci_bs_SWATReward)
				IF NOT g_sMPTunables.Target_Override_Elite_Objectives_Pacific_Disable_No_NOOSE  
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNooseCalled = TRUE
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNooseCalled = bSwatInvolved
					iNumEliteComponents++
					PRINTLN("[RCC MISSION] [CASH_RWD] Swat Bonus Set")
					
					IF NOT bSwatInvolved
						PRINTLN("[RCC MISSION] [CASH_RWD] adding heist SWAT bonus")
					ELSE
						bGiveBonus = FALSE			
					ENDIF
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset,ci_bs_CashBagReward)
				iNumEliteComponents++
				PRINTLN("[RCC MISSION] [CASH_RWD] Cash Bag Bonus Set (This should probably be removed)")
				
				IF bBeatCashGrab
					PRINTLN("[RCC MISSION] [CASH_RWD] adding heist cash grab bonus")
				ELSE
					bGiveBonus = FALSE
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset,ci_bs_PlayerHealth)
				INT iPlayerHealthThreshold = GET_BONUS_PLAYER_HEALTH_THRESHOLD(iCurrentHeistMissionIndex)
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHealthDamage = TRUE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageTarget = iPlayerHealthThreshold
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageActual = iPlayerMostHealthLost
				iNumEliteComponents++
				PRINTLN("[RCC MISSION] [CASH_RWD] Player Health Bonus Set - ",iPlayerMostHealthLost," <= ",iPlayerHealthThreshold)
				
				IF iPlayerMostHealthLost <= GET_BONUS_PLAYER_HEALTH_THRESHOLD(iCurrentHeistMissionIndex)
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHealthDamagePercentagePassed = TRUE
					PRINTLN("[RCC MISSION] [CASH_RWD] adding heist player health bonus")
				ELSE
					bGiveBonus = FALSE
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset,ci_bs_HeistMMTimerReward)
				INT iExtractionTimeThreshold = GET_EXTRACTION_TIME_THRESHOLD()
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentExtractionTime = TRUE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetExtractionTime = iExtractionTimeThreshold
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iExtractionTime = iExtractionTime
				iNumEliteComponents++
				PRINTLN("[RCC MISSION] [CASH_RWD] Extraction Time Bonus Set - ",iExtractionTime," <= ",iExtractionTimeThreshold)
				
				IF iExtractionTime <= iExtractionTimeThreshold
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bExtractionTimeChallengeComplete = TRUE
					PRINTLN("[RCC MISSION] [CASH_RWD] adding extraction time bonus")
				ELSE
					bGiveBonus = FALSE
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset,ci_bs_RewardsVoidOnTeamDeath)
			OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentLivesLost = TRUE
				iNumEliteComponents++
				PRINTLN("[RCC MISSION] [CASH_RWD] No Lives Lost Bonus Set - Lost ",iTeamDeaths[0] + iTeamDeaths[1]+ iTeamDeaths[2]+ iTeamDeaths[3])
				
				IF (iTeamDeaths[0] + iTeamDeaths[1]+ iTeamDeaths[2]+ iTeamDeaths[3]) = 0
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumLivesLostChallengeComplete = TRUE
					PRINTLN("[RCC MISSION] [CASH_RWD] adding no lives lost bonus")
				ELSE
					bGiveBonus = FALSE
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset, ci_bs_RewardsNoHackFails)
			OR (CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE() AND g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH)
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHackFailed = TRUE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailedTarget = GET_BONUS_NUM_HACK_FAILS_THRESHOLD()
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailed = iHackFails
				iNumEliteComponents++
				PRINTLN("[RCC MISSION] [CASH_RWD] Hack Bonus Set - ",iHackFails," <= ",GET_BONUS_NUM_HACK_FAILS_THRESHOLD())

				IF iHackFails <= GET_BONUS_NUM_HACK_FAILS_THRESHOLD()
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumHackFailedChallengeComplete = TRUE
					PRINTLN("[RCC MISSION] [CASH_RWD] adding no hacks failed bonus")
				ELSE
					bGiveBonus = FALSE		
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iHeistRewardBonusBitset,ci_bs_RewardsHeadshots)
			OR (CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE() AND g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT)
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedHeadshots = TRUE
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshotsTarget = GET_BONUS_NUM_HEADSHOTS_THRESHOLD()
				g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshots = iHeadshots
				iNumEliteComponents++
				PRINTLN("[RCC MISSION] [CASH_RWD] Headshots Bonus Set - ",iHeadshots," >= ",GET_BONUS_NUM_HEADSHOTS_THRESHOLD())
				
				IF iHeadshots >= GET_BONUS_NUM_HEADSHOTS_THRESHOLD()
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedHeadshotsChallengeComplete = TRUE
					PRINTLN("[RCC MISSION] [CASH_RWD] adding heist headshots bonus")
				ELSE
					bGiveBonus = FALSE		
				ENDIF
			ENDIF
			IF bTripSkipAvailable
				IF bTripSkipUsed
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentTripSkip = TRUE
					bGiveBonus = FALSE
					PRINTLN("[RCC MISSION] [CASH_RWD] Used trip skip, challenge void")
				ENDIF
			ENDIF
			IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
				IF g_sCasinoHeistMissionConfigData.eChosenApproachType != CASINO_HEIST_APPROACH_TYPE__DIRECT
					IF bDroppedToDirect
						g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDroppedToDirect = TRUE
						bGiveBonus = FALSE
						PRINTLN("[RCC MISSION] [CASH_RWD] Dropped to the direct route, challenge void")
					ENDIF
				ENDIF
			ENDIF
			IF iNumEliteComponents > 0
				IF bGiveBonus
					IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
						MARK_GANGOPS_ELITE_CHALLENGE_AS_COMPLETE(GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION())
					ELIF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
						MARK_CASINO_HEIST_CHALLENGE_AS_COMPLETE()
					ELSE
						MARK_ELITE_CHALLENGE_AS_COMPLETE(iCurrentHeistMissionIndex)
					ENDIF
					iReturnCash = GET_BONUS_CASH_GAINED(iCurrentHeistMissionIndex)
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete = TRUE
					g_TransitionSessionNonResetVars.sGlobalCelebrationData.iEliteChallengeBonusValue = iReturnCash
					PRINTLN("[RCC MISSION] - [CASH_RWD] - passed heist elite bonuses - returning ", iReturnCash)
				ELSE
					PRINTLN("[RCC MISSION] - [CASH_RWD] - failed heist elite bonuses - returning ", iReturnCash)
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] - [CASH_RWD] - no heist elite bonuses set - returning ", iReturnCash)
			ENDIF
		
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] [CASH_RWD] Playing on EASY, no elite challenges")
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDifficultyHighEnoughForEliteChallenge = FALSE
	ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumEliteChallengeComponents = iNumEliteComponents
	
	RETURN iReturnCash 

ENDFUNC

#IF FEATURE_GTAO_MEMBERSHIP

FUNC BOOL CAN_PLAYER_ACCESS_HEIST_MEMBERSHIP_REWARDS(PLAYER_INDEX piPlayer, PLAYER_INDEX piHeistLeader)
	
	IF g_sMPTunables.bMEMBERSHIP_Heists_ENABLE_ALL_MEMBERSHIP_PLAYERS
		//Valid for all players with a membership
		IF (piPlayer = GET_PLAYER_INDEX() AND IS_GTAO_MEMBERSHIP_CONTENT_ENABLED())
		OR (piPlayer != GET_PLAYER_INDEX() AND IS_REMOTE_PLAYERS_GTAO_MEMBERSHIP_CONTENT_ENABLED(piPlayer))
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bMEMBERSHIP_Heists_ENABLE_MEMBERSHIP_HOST
		IF piHeistLeader != INVALID_PLAYER_INDEX()
			IF (piHeistLeader = GET_PLAYER_INDEX() AND IS_GTAO_MEMBERSHIP_CONTENT_ENABLED())
			OR (piHeistLeader != GET_PLAYER_INDEX() AND IS_REMOTE_PLAYERS_GTAO_MEMBERSHIP_CONTENT_ENABLED(piHeistLeader))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
			
ENDFUNC

#ENDIF

FUNC INT CALCULATE_HEIST_FINALE_CASH_REWARD( BOOL bPass, BOOL bReturnJustMyCutOfTotal,INT iTotalMissionEndTime, #IF FEATURE_GTAO_MEMBERSHIP PLAYER_INDEX piHeistLeader, BOOL bFinalTake, #ENDIF INT iTripSkipCost = 0,INT iCashDropped = 0)

	INT iCashGained = GET_CASH_VALUE_FROM_CREATOR(g_FMMC_STRUCT.iCashReward)
	PRINTLN("[RCC MISSION] [CASH_RWD] setting heist finale cash raw reward : ",iCashGained)
	
	INT iRootContentID = GET_STRAND_ROOT_CONTENT_ID_HASH()
	
	IF iRootContentID = 0
		PRINTLN("[MJM][HEIST_MISC] - CALCULATE_HEIST_FINALE_CASH_REWARD - RootContentID is 0, using g_FMMC_STRUCT data: ", g_FMMC_STRUCT.iRootContentIdHash)
		iRootContentID = g_FMMC_STRUCT.iRootContentIdHash
	ELSE
		PRINTLN("[MJM][HEIST_MISC] - CALCULATE_HEIST_FINALE_CASH_REWARD - GET_STRAND_ROOT_CONTENT_ID_HASH() = ", iRootContentID)
	ENDIF
	
	SWITCH(g_FMMC_STRUCT.iDifficulity)
		CASE DIFF_EASY
			iCashGained = ROUND(iCashGained * g_sMPTunables.fheist_difficulty_easy)
			PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward to " ,iCashGained," since difficulty = EASY and difficulty modifier = ",g_sMPTunables.fheist_difficulty_easy)
		BREAK 
		CASE DIFF_NORMAL
			iCashGained = ROUND(iCashGained * g_sMPTunables.fheist_difficulty_normal)
			PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward to " ,iCashGained," since difficulty = NORMAL and difficulty modifier = ",g_sMPTunables.fheist_difficulty_normal)
		BREAK
		CASE DIFF_HARD
			iCashGained = ROUND(iCashGained * g_sMPTunables.fheist_difficulty_hard)
			PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward to " ,iCashGained," since difficulty = HARD and difficulty modifier = ",g_sMPTunables.fheist_difficulty_hard)
		BREAK
	ENDSWITCH
	
	IF iCashGained < g_sMPTunables.iEarnings_Heists_Finale_minimum_total_cash_take
		iCashGained = g_sMPTunables.iEarnings_Heists_Finale_minimum_total_cash_take
		PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward min capped: ",iCashGained)
	ENDIF
	IF iCashGained > g_sMPTunables.iEarnings_Heists_Finale_maximum_total_cash_take
		iCashGained = g_sMPTunables.iEarnings_Heists_Finale_maximum_total_cash_take
		PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward max capped: ",iCashGained)
	ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake = iCashGained
	
	FLOAT fCashGained = TO_FLOAT(iCashGained)
	
	PRINTLN("[RCC MISSION] [CASH_RWD] FLOAT fCashGained = ", fCashGained)
	
	IF bPass

		INT iNumPlayers, iTeam
		REPEAT FMMC_MAX_TEAMS iTeam
			iNumPlayers += g_FMMC_STRUCT.iCriticalMinimumForTeam[iTeam]
		ENDREPEAT
		PRINTLN("[RCC MISSION] [CASH_RWD] iNumPlayers = ",iNumPlayers)
		
		fCashGained -= (iTripSkipCost * iNumPlayers)
		PRINTLN("[RCC MISSION] [CASH_RWD] cash after TripSkip Cost = : ",fCashGained)
		
		IF iCashDropped > 0
			fCashGained -= iCashDropped
			PRINTLN("[RCC MISSION] [CASH_RWD] TRIP SKIP - After cash dropped fCashGained = ", fCashGained)
		ENDIF

		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iActualTake = ROUND(fCashGained)

		IF bReturnJustMyCutOfTotal
			FLOAT fCut = TO_FLOAT(g_TransitionSessionNonResetVars.iHeistCutPercent)
			PRINTLN("[RCC MISSION] [CASH_RWD] FLOAT fCut = ", fCut)
			fCashGained = (fCashGained * (fCut/100))
			PRINTLN("[RCC MISSION] [CASH_RWD] fCashGained = ", fCashGained)
			PRINTLN("[RCC MISSION] [CASH_RWD] iHeistCutPercent = ", g_TransitionSessionNonResetVars.iHeistCutPercent)
		ENDIF
		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyCutPercentage = g_TransitionSessionNonResetVars.iHeistCutPercent
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake = ROUND(fCashGained)
		
		PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward to " ,fCashGained," since your cut is: ",g_TransitionSessionNonResetVars.iHeistCutPercent)
		IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
			IF GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(iRootContentID,TRUE) > 1 //Stat is already incremented for this mission
				fCashGained = fCashGained*g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION] [CASH_RWD] setting HOST heist cash reward to " ,iCashGained," since not first play, tunable: ",g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward)
			ELSE
				fCashGained = fCashGained*g_sMPTunables.fEarnings_Heists_Finale_first_play_cash_reward	
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION] [CASH_RWD] setting HOST heist cash reward to " ,iCashGained," since first play, tunable: ",g_sMPTunables.fEarnings_Heists_Finale_first_play_cash_reward)
			ENDIF
		ELSE
			IF GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(iRootContentID,FALSE) > 1 //Stat is already incremented for this mission
				fCashGained = fCashGained*g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward to " ,iCashGained," since not first play, tunable: ",g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward)
			ELSE
				fCashGained = fCashGained*g_sMPTunables.fEarnings_Heists_Finale_first_play_cash_reward	
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward to " ,iCashGained," since first play, tunable: ",g_sMPTunables.fEarnings_Heists_Finale_first_play_cash_reward)
			ENDIF
		ENDIF
	ELSE
		IF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_1*60000)
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE HEIST FINALE setting cash reward to 500 since under 2 mins: ",iCashGained)
			FLOAT fFailCashMulti = (g_sMPTunables.fHeists_Finale_fail_Cash_percentage_period_1/100)  
			fCashGained = fCashGained*fFailCashMulti
			iCashGained = ROUND(fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_2*60000)
			FLOAT fFailCashMulti = (g_sMPTunables.fHeists_Finale_fail_Cash_percentage_period_2/100)  
			fCashGained = fCashGained*fFailCashMulti
			iCashGained = ROUND(fCashGained)
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE HEIST FINALE setting cash reward since under 4 mins to: ",iCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_3*60000)
			FLOAT fFailCashMulti = (g_sMPTunables.fHeists_Finale_fail_Cash_percentage_period_3/100)  
			fCashGained = fCashGained*fFailCashMulti
			iCashGained = ROUND(fCashGained)
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE HEIST FINALE setting cash reward since under 6 mins to: ",iCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_4*60000)
			FLOAT fFailCashMulti = (g_sMPTunables.fHeists_Finale_fail_Cash_percentage_period_4/100)  
			fCashGained = fCashGained*fFailCashMulti
			iCashGained = ROUND(fCashGained)
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE HEIST FINALE setting cash reward since under 8 mins to: ",iCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_5*60000)
			FLOAT fFailCashMulti = (g_sMPTunables.fHeists_Finale_fail_Cash_percentage_period_5/100)  
			fCashGained = fCashGained*fFailCashMulti
			iCashGained = ROUND(fCashGained)
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE HEIST FINALE setting cash reward since under 10 mins to: ",iCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_6*60000)
			FLOAT fFailCashMulti = (g_sMPTunables.fHeists_Finale_fail_Cash_percentage_period_6/100)  
			fCashGained = fCashGained*fFailCashMulti
			iCashGained = ROUND(fCashGained)
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE HEIST FINALE setting cash reward since under 15 mins to: ",iCashGained)
		ELSE
			FLOAT fFailCashMulti = (g_sMPTunables.fHeists_Finale_fail_Cash_percentage_period_7/100)  
			fCashGained = fCashGained*fFailCashMulti
			iCashGained = ROUND(fCashGained)
			PRINTLN("[RCC MISSION][CASH_RWD] LOSE HEIST FINALE setting cash reward since over 15 mins to: ",iCashGained)
		ENDIF
	ENDIF
	
#IF FEATURE_GTAO_MEMBERSHIP
	IF bFinalTake
	AND CAN_PLAYER_ACCESS_HEIST_MEMBERSHIP_REWARDS(GET_PLAYER_INDEX(), piHeistLeader)
		IF g_sMPTunables.fMEMBERSHIP_Heists_FINALE_REWARD_MULTIPLIER > 1.0
			iCashGained = ROUND(iCashGained * g_sMPTunables.fMEMBERSHIP_Heists_FINALE_REWARD_MULTIPLIER)
			PRINTLN("[RCC MISSION][CASH_RWD] FINALE Membership reward boosted: ", iCashGained)
		ENDIF
	ENDIF
#ENDIF
				
	IF NOT bPass
		IF iCashGained < 500
			iCashGained = 500
		ENDIF
		IF IS_THIS_A_QUICK_RESTART_JOB() 
			iCashGained = 0
			PRINTLN("[RCC MISSION][CASH_RWD] lose Heist FINALE IS_THIS_A_QUICK_RESTART_JOB TRUE: ",iCashGained)
		ENDIF
	ENDIF
	
	IF iCashGained < 0
		iCashGained = 0
		PRINTLN("[RCC MISSION][CASH_RWD] - iCashGained < 0 setting to 0 ")
	ENDIF
	
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [CASH_RWD] - CALCULATE_HEIST_FINALE_CASH_REWARD - returning ", iCashGained)
	RETURN iCashGained
	
ENDFUNC

FUNC BOOL GANGOPS_HAS_STRAND_BEEN_COMPLETED_BEFORE()
	INT iGangOpsPassedBS = GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_FLOW_PASSED_BITSET)
	SWITCH GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION()
		CASE ciGANGOPS_FLOW_MISSION_MORGUE
		CASE ciGANGOPS_FLOW_MISSION_DELOREAN
		CASE ciGANGOPS_FLOW_MISSION_SERVERFARM
		CASE ciGANGOPS_FLOW_MISSION_IAABASE_FINALE	
			RETURN IS_BIT_SET(iGangOpsPassedBS, ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		CASE ciGANGOPS_FLOW_MISSION_STEALOSPREY
		CASE ciGANGOPS_FLOW_MISSION_FOUNDRY
		CASE ciGANGOPS_FLOW_MISSION_RIOTVAN
		CASE ciGANGOPS_FLOW_MISSION_SUBMARINECAR
		CASE ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE
			RETURN IS_BIT_SET(iGangOpsPassedBS, ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE)
		CASE ciGANGOPS_FLOW_MISSION_PREDATOR
		CASE ciGANGOPS_FLOW_MISSION_BMLAUNCHER
		CASE ciGANGOPS_FLOW_MISSION_BCCUSTOM
		CASE ciGANGOPS_FLOW_MISSION_STEALTHTANKS
		CASE ciGANGOPS_FLOW_MISSION_SPYPLANE
		CASE ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE
		CASE ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2
			RETURN IS_BIT_SET(iGangOpsPassedBS, ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC INT CALCULATE_GANGOPS_FINALE_CASH_REWARD(BOOL bPass, BOOL bReturnJustMyCutOfTotal,INT iTotalMissionEndTime, INT iNumPlayers = 0, INT iTripSkipCost = 0)

	IF NOT bPass
	AND IS_THIS_A_QUICK_RESTART_JOB() 
		PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - Failed and quick restart - NO CASH")
		RETURN 0
	ENDIF
	
	INT iCashGained = GET_CASH_VALUE_FROM_CREATOR(g_FMMC_STRUCT.iCashReward)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - Base Reward: ", iCashGained)
	
	SWITCH(g_FMMC_STRUCT.iDifficulity)
		CASE DIFF_NORMAL
			iCashGained = ROUND(iCashGained * g_sMPTunables.fGangops_difficulty_normal)
		BREAK
		CASE DIFF_HARD
			iCashGained = ROUND(iCashGained * g_sMPTunables.fGangops_difficulty_hard)
		BREAK
	ENDSWITCH
	PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - Reward after difficulty scaling: ", iCashGained)
	
	IF iCashGained < g_sMPTunables.iEarnings_Gangops_Finale_minimum_total_cash_take
		iCashGained = g_sMPTunables.iEarnings_Gangops_Finale_minimum_total_cash_take
	ENDIF
	IF iCashGained > g_sMPTunables.iEarnings_Gangops_Finale_maximum_total_cash_take
		iCashGained = g_sMPTunables.iEarnings_Gangops_Finale_maximum_total_cash_take
	ENDIF
	PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - Capped Take: ", iCashGained)
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake = iCashGained
	FLOAT fCashGained = TO_FLOAT(iCashGained)

	IF bPass
		PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - iNumPlayers: ", iNumPlayers)
		fCashGained -= (iTripSkipCost * iNumPlayers)
		PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - After TripSkip Cost: ", fCashGained)

		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iActualTake = ROUND(fCashGained)

		IF bReturnJustMyCutOfTotal
			FLOAT fCut = TO_FLOAT(g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent)
			PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - fCut %: ", fCut)
			fCashGained = (fCashGained * (fCut/100))
			PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - My Cut: ", fCashGained)
		ENDIF
		
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyCutPercentage = g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake = ROUND(fCashGained)
		
		IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = PLAYER_ID()
			IF NOT GANGOPS_HAS_STRAND_BEEN_COMPLETED_BEFORE()
				fCashGained = fCashGained * g_sMPTunables.fEarnings_Gangops_Finale_replay_cash_reward
				iCashGained = ROUND(fCashGained)
				PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - Boss replay cash: ", iCashGained)
			ELSE
				fCashGained = fCashGained * g_sMPTunables.fEarnings_Gangops_Finale_first_play_cash_reward	
				iCashGained = ROUND(fCashGained)
				PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - Boss first play cash: ", iCashGained)
			ENDIF
		ELSE
			IF NOT GANGOPS_HAS_STRAND_BEEN_COMPLETED_BEFORE()
				fCashGained = fCashGained * g_sMPTunables.fEarnings_Gangops_Finale_replay_cash_reward
				iCashGained = ROUND(fCashGained)
				PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - Goon replay cash: ", iCashGained)
			ELSE
				fCashGained = fCashGained * g_sMPTunables.fEarnings_Gangops_Finale_first_play_cash_reward	
				iCashGained = ROUND(fCashGained)
				PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - Goon first play cash: ", iCashGained)
			ENDIF
		ENDIF
	ELSE
		FLOAT fFailCashMulti
		IF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_1 * 60000)
			fFailCashMulti = (g_sMPTunables.fGangops_Finale_fail_Cash_percentage_period_1 / 100)  
			fCashGained = fCashGained * fFailCashMulti
			iCashGained = ROUND(fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_2 * 60000)
			fFailCashMulti = (g_sMPTunables.fGangops_Finale_fail_Cash_percentage_period_2 / 100)  
			fCashGained = fCashGained * fFailCashMulti
			iCashGained = ROUND(fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_3 * 60000)
			fFailCashMulti = (g_sMPTunables.fGangops_Finale_fail_Cash_percentage_period_3 / 100)  
			fCashGained = fCashGained * fFailCashMulti
			iCashGained = ROUND(fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_4 * 60000)
			fFailCashMulti = (g_sMPTunables.fGangops_Finale_fail_Cash_percentage_period_4 / 100)  
			fCashGained = fCashGained * fFailCashMulti
			iCashGained = ROUND(fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_5 * 60000)
			fFailCashMulti = (g_sMPTunables.fGangops_Finale_fail_Cash_percentage_period_5 / 100)  
			fCashGained = fCashGained * fFailCashMulti
			iCashGained = ROUND(fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_6 * 60000)
			fFailCashMulti = (g_sMPTunables.fGangops_Finale_fail_Cash_percentage_period_6 / 100)  
			fCashGained = fCashGained * fFailCashMulti
			iCashGained = ROUND(fCashGained)
		ELSE
			fFailCashMulti = (g_sMPTunables.fGangops_Finale_fail_Cash_percentage_period_7 / 100)  
			fCashGained = fCashGained * fFailCashMulti
			iCashGained = ROUND(fCashGained)
		ENDIF
		PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - Failed, scaling by time: iTotalMissionEndTime = ", iTotalMissionEndTime, " fFailCashMulti = ", fFailCashMulti)
		
		IF iCashGained < g_sMPTunables.iEarnings_Gangops_Finale_min_cash_reward
			iCashGained = g_sMPTunables.iEarnings_Gangops_Finale_min_cash_reward
			PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - Capping to min reward: ", iCashGained)
		ENDIF
	ENDIF
	
	PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - Final Cash Reward: ", iCashGained)
	RETURN iCashGained
	
ENDFUNC

#IF FEATURE_GTAO_MEMBERSHIP

FUNC BOOL CAN_PLAYER_ACCESS_CASINO_HEIST_MEMBERSHIP_REWARDS(PLAYER_INDEX piPlayer)
	
	IF g_sMPTunables.bMEMBERSHIP_HEIST3_ENABLE_ALL_MEMBERSHIP_PLAYERS
		//Valid for all players with a membership
		IF (piPlayer = GET_PLAYER_INDEX() AND IS_GTAO_MEMBERSHIP_CONTENT_ENABLED())
		OR (piPlayer != GET_PLAYER_INDEX() AND IS_REMOTE_PLAYERS_GTAO_MEMBERSHIP_CONTENT_ENABLED(piPlayer))
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bMEMBERSHIP_HEIST3_ENABLE_MEMBERSHIP_HOST
		PLAYER_INDEX piGangBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		IF piGangBoss != INVALID_PLAYER_INDEX()
			IF (piGangBoss = GET_PLAYER_INDEX() AND IS_GTAO_MEMBERSHIP_CONTENT_ENABLED())
			OR (piGangBoss != GET_PLAYER_INDEX() AND IS_REMOTE_PLAYERS_GTAO_MEMBERSHIP_CONTENT_ENABLED(piGangBoss))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
			
ENDFUNC

FUNC FLOAT GET_CASINO_HEIST_LESTERS_CUT_MEMBERSHIP()
	IF g_sMPTunables.fMEMBERSHIP_CH_LESTER_CUT < 0.0
		RETURN TO_FLOAT(g_sMPTunables.iCH_LESTER_CUT)
	ENDIF
	
	RETURN g_sMPTunables.fMEMBERSHIP_CH_LESTER_CUT
ENDFUNC

FUNC FLOAT GET_CASINO_HEIST_GUNMAN_CUT_MEMBERSHIP(CASINO_HEIST_WEAPON_EXPERTS eGunman)
	SWITCH eGunman
		CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI			RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_PREPBOARD_GUNMEN_KARL_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_PREPBOARD_GUNMEN_KARL_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_PREPBOARD_GUNMEN_KARL_CUT)
		CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA			RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_PREPBOARD_GUNMEN_GUSTAVO_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_PREPBOARD_GUNMEN_GUSTAVO_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_PREPBOARD_GUNMEN_GUSTAVO_CUT)
		CASE CASINO_HEIST_WEAPON_EXPERT__CHARLIE				RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_PREPBOARD_GUNMEN_CHARLIE_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_PREPBOARD_GUNMEN_CHARLIE_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_PREPBOARD_GUNMEN_CHARLIE_CUT)
		CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT			RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_PREPBOARD_GUNMEN_CHESTER_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_PREPBOARD_GUNMEN_CHESTER_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_PREPBOARD_GUNMEN_CHESTER_CUT)
		CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY			RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_PREPBOARD_GUNMEN_PATRICK_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_PREPBOARD_GUNMEN_PATRICK_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_PREPBOARD_GUNMEN_PATRICK_CUT)
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_CASINO_HEIST_DRIVER_CUT_MEMBERSHIP(CASINO_HEIST_DRIVERS eDriver)
	SWITCH eDriver
		CASE CASINO_HEIST_DRIVER__KARIM_DENZ					RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_DRIVERS_KARIM_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_DRIVERS_KARIM_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_DRIVERS_KARIM_CUT)
		CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ				RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_DRIVERS_TALIANA_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_DRIVERS_TALIANA_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_DRIVERS_TALIANA_CUT)
		CASE CASINO_HEIST_DRIVER__EDDIE_TOH						RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_DRIVERS_EDDIE_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_DRIVERS_EDDIE_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_DRIVERS_EDDIE_CUT)
		CASE CASINO_HEIST_DRIVER__ZACH							RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_DRIVERS_ZACH_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_DRIVERS_ZACH_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_DRIVERS_ZACH_CUT)
		CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT				RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_DRIVERS_CHESTER_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_DRIVERS_CHESTER_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_DRIVERS_CHESTER_CUT)
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_CASINO_HEIST_HACKER_CUT_MEMBERSHIP(CASINO_HEIST_HACKERS eHacker)
	SWITCH eHacker
		CASE CASINO_HEIST_HACKER__RICKIE_LUKENS					RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_HACKERS_RICKIE_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_HACKERS_RICKIE_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_HACKERS_RICKIE_CUT)
		CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ				RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_HACKERS_CHRISTIAN_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_HACKERS_CHRISTIAN_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_HACKERS_CHRISTIAN_CUT)
		CASE CASINO_HEIST_HACKER__YOHAN							RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_HACKERS_YOHAN_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_HACKERS_YOHAN_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_HACKERS_YOHAN_CUT)
		CASE CASINO_HEIST_HACKER__AVI_SCHWARTZMAN				RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_HACKERS_AVI_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_HACKERS_AVI_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_HACKERS_AVI_CUT)
		CASE CASINO_HEIST_HACKER__PAIGE_HARRIS					RETURN PICK_FLOAT(g_sMPTunables.fMEMBERSHIP_HEIST3_HACKERS_PAIGE_CUT < 0.0, TO_FLOAT(g_sMPTunables.iHEIST3_HACKERS_PAIGE_CUT), g_sMPTunables.fMEMBERSHIP_HEIST3_HACKERS_PAIGE_CUT)
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP(FLOAT fTotalTake, BOOL bLocalPlayerMember)
	
	FLOAT fSupportCrewCut 				= 0.0	
	fSupportCrewCut 					+= (GET_CASINO_HEIST_LESTERS_CUT_MEMBERSHIP() / 100)
	PRINTLN("[JS][CASH_RWD][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Lester's Cut: ", GET_CASINO_HEIST_LESTERS_CUT_MEMBERSHIP())	
	IF bLocalPlayerMember
		missionEndShardData.npcTake[MISSION_END_SHARD_NPC_LESTER].iTake = ROUND(fTotalTake * (GET_CASINO_HEIST_LESTERS_CUT_MEMBERSHIP() / 100))
		missionEndShardData.npcTake[MISSION_END_SHARD_NPC_LESTER].iTake = (-missionEndShardData.npcTake[MISSION_END_SHARD_NPC_LESTER].iTake)
		PRINTLN("[JS][CASH_RWD][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Lester's Cut in Cash: ", missionEndShardData.npcTake[MISSION_END_SHARD_NPC_LESTER].iTake)	
	ENDIF
	
	CASINO_HEIST_WEAPON_EXPERTS eWeaponExpert = GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE)
	fSupportCrewCut 					+= (GET_CASINO_HEIST_GUNMAN_CUT_MEMBERSHIP(eWeaponExpert) / 100)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Weapon Expert: ", ENUM_TO_INT(eWeaponExpert))
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Weapon Expert's Cut: ", GET_CASINO_HEIST_GUNMAN_CUT_MEMBERSHIP(eWeaponExpert))
	IF bLocalPlayerMember
		missionEndShardData.npcTake[MISSION_END_SHARD_NPC_WEAPON].iTake = ROUND(fTotalTake * (GET_CASINO_HEIST_GUNMAN_CUT_MEMBERSHIP(eWeaponExpert) / 100))
		missionEndShardData.npcTake[MISSION_END_SHARD_NPC_WEAPON].iTake = (-missionEndShardData.npcTake[MISSION_END_SHARD_NPC_WEAPON].iTake)
		PRINTLN("[JS][CASH_RWD][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Weapon Expert's Cut In Cash: ", missionEndShardData.npcTake[MISSION_END_SHARD_NPC_WEAPON].iTake)
	ENDIF
	
	CASINO_HEIST_DRIVERS eDriver 		= GET_PLAYER_CASINO_HEIST_DRIVER(GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE)
	fSupportCrewCut 					+= (GET_CASINO_HEIST_DRIVER_CUT_MEMBERSHIP(eDriver) / 100)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Driver: ", ENUM_TO_INT(eDriver))
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Driver's Cut: ", GET_CASINO_HEIST_DRIVER_CUT_MEMBERSHIP(eDriver))
	IF bLocalPlayerMember
		missionEndShardData.npcTake[MISSION_END_SHARD_NPC_DRIVER].iTake  = ROUND(fTotalTake * (GET_CASINO_HEIST_DRIVER_CUT_MEMBERSHIP(eDriver) / 100))
		missionEndShardData.npcTake[MISSION_END_SHARD_NPC_DRIVER].iTake  = (-missionEndShardData.npcTake[MISSION_END_SHARD_NPC_DRIVER].iTake )
		PRINTLN("[JS][CASH_RWD][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Driver's Cut In Cash: ", missionEndShardData.npcTake[MISSION_END_SHARD_NPC_DRIVER].iTake)
	ENDIF
	
	CASINO_HEIST_HACKERS eHacker = GET_PLAYER_CASINO_HEIST_HACKER(GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE)
	fSupportCrewCut += (GET_CASINO_HEIST_HACKER_CUT_MEMBERSHIP(eHacker) / 100)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Hacker: ", ENUM_TO_INT(eHacker))
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Hacker's Cut: ", GET_CASINO_HEIST_HACKER_CUT_MEMBERSHIP(eHacker))
	IF bLocalPlayerMember
		missionEndShardData.npcTake[MISSION_END_SHARD_NPC_HACKER].iTake  = ROUND(fTotalTake * (GET_CASINO_HEIST_HACKER_CUT_MEMBERSHIP(eHacker) / 100))
		missionEndShardData.npcTake[MISSION_END_SHARD_NPC_HACKER].iTake  = (-missionEndShardData.npcTake[MISSION_END_SHARD_NPC_HACKER].iTake )
		PRINTLN("[JS][CASH_RWD][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Hacker's Cut in Cash: ", missionEndShardData.npcTake[MISSION_END_SHARD_NPC_HACKER].iTake)	
	ENDIF
	
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Total Support Crew Cut: ", fSupportCrewCut)
	
	FLOAT fSupportCrewTake = fTotalTake * fSupportCrewCut
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP - Total Support Crew Take: ", fSupportCrewTake)
	
	RETURN fSupportCrewTake
	
ENDFUNC

#ENDIF

FUNC FLOAT GET_CASINO_HEIST_SUPPORT_CREW_TAKE(FLOAT fTotalTake)
	
	FLOAT fSupportCrewCut 				= 0.0	
	fSupportCrewCut 					+= (TO_FLOAT(g_sMPTunables.iCH_LESTER_CUT) / 100)
	missionEndShardData.npcTake[MISSION_END_SHARD_NPC_LESTER].iTake 	= ROUND(fTotalTake * (TO_FLOAT(g_sMPTunables.iCH_LESTER_CUT) / 100))
	missionEndShardData.npcTake[MISSION_END_SHARD_NPC_LESTER].iTake = (-missionEndShardData.npcTake[MISSION_END_SHARD_NPC_LESTER].iTake)
	PRINTLN("[JS][CASH_RWD][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Lester's Cut: ", g_sMPTunables.iCH_LESTER_CUT, " Cut in Cash: ", missionEndShardData.npcTake[MISSION_END_SHARD_NPC_LESTER].iTake)	
	
	CASINO_HEIST_WEAPON_EXPERTS eWeaponExpert = GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE)
	fSupportCrewCut 					+= (TO_FLOAT(GET_CASINO_HEIST_GUNMAN_CUT(eWeaponExpert)) / 100)
	missionEndShardData.npcTake[MISSION_END_SHARD_NPC_WEAPON].iTake = ROUND(fTotalTake * (TO_FLOAT(GET_CASINO_HEIST_GUNMAN_CUT(eWeaponExpert)) / 100))
	missionEndShardData.npcTake[MISSION_END_SHARD_NPC_WEAPON].iTake = (-missionEndShardData.npcTake[MISSION_END_SHARD_NPC_WEAPON].iTake)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Weapon Expert: ", ENUM_TO_INT(eWeaponExpert))
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Weapon Expert's Cut: ", GET_CASINO_HEIST_GUNMAN_CUT(eWeaponExpert))
	PRINTLN("[JS][CASH_RWD][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Weapon Expert's Cut In Cash: ", missionEndShardData.npcTake[MISSION_END_SHARD_NPC_WEAPON].iTake)
	
	CASINO_HEIST_DRIVERS eDriver 		= GET_PLAYER_CASINO_HEIST_DRIVER(GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE)
	fSupportCrewCut 					+= (TO_FLOAT(GET_CASINO_HEIST_DRIVER_CUT(eDriver)) / 100)
	missionEndShardData.npcTake[MISSION_END_SHARD_NPC_DRIVER].iTake  = ROUND(fTotalTake * (TO_FLOAT(GET_CASINO_HEIST_DRIVER_CUT(eDriver)) / 100))
	missionEndShardData.npcTake[MISSION_END_SHARD_NPC_DRIVER].iTake  = (-missionEndShardData.npcTake[MISSION_END_SHARD_NPC_DRIVER].iTake )
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Driver: ", ENUM_TO_INT(eDriver))
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Driver's Cut: ", GET_CASINO_HEIST_DRIVER_CUT(eDriver))
	PRINTLN("[JS][CASH_RWD][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Driver's Cut In Cash: ", missionEndShardData.npcTake[MISSION_END_SHARD_NPC_DRIVER].iTake)
	
	CASINO_HEIST_HACKERS eHacker = GET_PLAYER_CASINO_HEIST_HACKER(GB_GET_LOCAL_PLAYER_GANG_BOSS(), TRUE)
	fSupportCrewCut += (TO_FLOAT(GET_CASINO_HEIST_HACKER_CUT(eHacker)) / 100)
	missionEndShardData.npcTake[MISSION_END_SHARD_NPC_HACKER].iTake  = ROUND(fTotalTake * (TO_FLOAT(GET_CASINO_HEIST_HACKER_CUT(eHacker)) / 100))
	missionEndShardData.npcTake[MISSION_END_SHARD_NPC_HACKER].iTake  = (-missionEndShardData.npcTake[MISSION_END_SHARD_NPC_HACKER].iTake )
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Hacker: ", ENUM_TO_INT(eHacker))
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Hacker's Cut: ", GET_CASINO_HEIST_HACKER_CUT(eHacker))
	PRINTLN("[JS][CASH_RWD][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Hacker's Cut in Cash: ", missionEndShardData.npcTake[MISSION_END_SHARD_NPC_HACKER].iTake)	
	
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Total Support Crew Cut: ", fSupportCrewCut)
	
	FLOAT fSupportCrewTake = fTotalTake * fSupportCrewCut
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - GET_CASINO_HEIST_SUPPORT_CREW_TAKE - Total Support Crew Take: ", fSupportCrewTake)
	
	RETURN fSupportCrewTake
	
ENDFUNC

FUNC FLOAT GET_CASINO_HEIST_TOTAL_TAKE(BOOL bPlayerValidForMembershipReward, FLOAT fTotalTake, FLOAT fSupportCrewCut, FLOAT fSupportCrewCutMembership, BOOL bReturnMyCut)
	
	FLOAT fReturnTake = fTotalTake - fSupportCrewCut
	
	IF bPlayerValidForMembershipReward
		fReturnTake = fTotalTake - fSupportCrewCutMembership
	ENDIF
	
	IF bReturnMyCut
		FLOAT fCut = TO_FLOAT(g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent)
		fReturnTake = fReturnTake * (fCut/100)
	ENDIF
	
	RETURN fReturnTake
	
ENDFUNC

FUNC INT CALCULATE_CASINO_HEIST_CASH_REWARD(BOOL bPass, INT iTotalTake, INT iNumPlayers = 0, INT iTripSkipCost = 0)
	
	IF NOT bPass
		PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - FAILED: No Cash Reward")
		RETURN 0
	ENDIF
	
	BOOL bLocalPlayerValidForMembershipReward = FALSE
	#IF FEATURE_GTAO_MEMBERSHIP
	bLocalPlayerValidForMembershipReward = CAN_PLAYER_ACCESS_CASINO_HEIST_MEMBERSHIP_REWARDS(GET_PLAYER_INDEX())
	#ENDIF
	
	CASINO_HEIST_TARGET_TYPE eTarget = GET_PLAYER_CASINO_HEIST_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Target: ", ENUM_TO_INT(eTarget))
	INT iMaxTake = GET_CASINO_HEIST_TARGET_VALUE(eTarget)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Target Value: ", iMaxTake)
	
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Difficulty: ", g_FMMC_STRUCT.iDifficulity)
	FLOAT fDifficultyMod = GET_CASINO_HEIST_TARGET_VALUE_DIFFICULTY_MODIFIER(g_FMMC_STRUCT.iDifficulity)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Difficulty Mod: ", fDifficultyMod)
	
	FLOAT fBuyerVariance = GET_CASINO_HEIST_REWARD_BUYER_MOD(g_sCasinoHeistMissionConfigData.eDropoffLocation)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Buyer Variance: ", fBuyerVariance)
	
	FLOAT fMaxTargetTake = iMaxTake * fDifficultyMod
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Difficulty Adjusted Target Value: ", fMaxTargetTake)
	
	INT iMaxDailyCashRoom = g_sMPTunables.iCasino_Heist_Max_Daily_Cash_Reward * ciMAX_DAILY_VAULT_CASH_STACKS
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Max Daily Cash Take: ", iMaxDailyCashRoom)
	
	INT iMaxDepositBox = g_sMPTunables.iCH_VAULT_SAFETY_DEPOSIT_BOX_VALUE_5Percent_2 * (ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOX_CABINETS_DYNAMIC * ciMAX_NUMBER_OF_HANDHELD_DRILL_LOCKBOXES)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Max Deposit Box Take: ", iMaxDepositBox)
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake = ROUND(fMaxTargetTake * fBuyerVariance)
	
	iMaxTake = ROUND(fMaxTargetTake)
	
	missionEndShardData.iTake[MISSION_END_SHARD_CH_POT_TAKE] = ROUND(fMaxTargetTake)
	PRINTLN("[RCC MISSION][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - missionEndShardData.iTake[MISSION_END_SHARD_CH_POT_TAKE] 	 					= ", missionEndShardData.iTake[MISSION_END_SHARD_CH_POT_TAKE])
	
	iMaxTake += (iMaxDailyCashRoom + iMaxDepositBox)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Max Take: ", iMaxTake)
	
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Actual Take Before Deductions: ", iTotalTake)
	
	IF iTotalTake > iMaxTake
		iTotalTake = iMaxTake
		PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Actual Take Clamped to Max Take: ", iTotalTake)
	ENDIF
	
	FLOAT fTotalTake = TO_FLOAT(iTotalTake)
	
	missionEndShardData.iTake[MISSION_END_SHARD_CH_TAKE] 							= iTotalTake
	PRINTLN("[RCC MISSION][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - missionEndShardData.iTake[MISSION_END_SHARD_CH_TAKE] 	 						= ", missionEndShardData.iTake[MISSION_END_SHARD_CH_TAKE])
	
	missionEndShardData.iMissionFee = ROUND(fTotalTake * (1.0-fBuyerVariance))  //buyerfee
	missionEndShardData.iMissionFee = (-missionEndShardData.iMissionFee)
	PRINTLN("[RCC MISSION][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - missionEndShardData.iMissionFee 	 											= ", missionEndShardData.iMissionFee)
	
	fTotalTake *= fBuyerVariance
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Total Take After Buyer: ", fTotalTake)	
	
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Trip Skip Cost: ", iTripSkipCost)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Num Players: ", iNumPlayers)
	fTotalTake -= (iTripSkipCost * iNumPlayers)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Total Take After Trip Skip: ", fTotalTake)
		
	FLOAT fSupportCrewTake = GET_CASINO_HEIST_SUPPORT_CREW_TAKE(fTotalTake)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Support Crew's Cut: $", fSupportCrewTake)
	
	FLOAT fSupportCrewTakeMembership = fSupportCrewTake
	#IF FEATURE_GTAO_MEMBERSHIP
	fSupportCrewTakeMembership = GET_CASINO_HEIST_SUPPORT_CREW_TAKE_MEMBERSHIP(fTotalTake, bLocalPlayerValidForMembershipReward)
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Support Crew's Cut (Membership): $", fSupportCrewTakeMembership)
	#ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iActualTake = ROUND(GET_CASINO_HEIST_TOTAL_TAKE(bLocalPlayerValidForMembershipReward, fTotalTake, fSupportCrewTake, fSupportCrewTakeMembership, FALSE))
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyCutPercentage = g_TransitionSessionNonResetVars.iGangOpsHeistCutPercent
	g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake = ROUND(GET_CASINO_HEIST_TOTAL_TAKE(bLocalPlayerValidForMembershipReward, fTotalTake, fSupportCrewTake, fSupportCrewTakeMembership, TRUE))
	
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - My Cut : ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyCutPercentage, "%")
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - My Cut: ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake)

	iTotalTake = ROUND(GET_CASINO_HEIST_TOTAL_TAKE(bLocalPlayerValidForMembershipReward, fTotalTake, fSupportCrewTake, fSupportCrewTakeMembership, TRUE))
	PRINTLN("[JS][CASH_RWD] - CALCULATE_CASINO_HEIST_CASH_REWARD - Returning: ", iTotalTake)	
	
	INT iGangBoss = NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	IF iGangBoss > -1
		INT iPlayer = 0
		FOR iPlayer = 0 TO MAX_CASINO_HEIST_PLAYERS-1
			missionEndShardData.playerCut[iPlayer].bLeader 					= FALSE
			missionEndShardData.playerCut[iPlayer].playerID 					= INVALID_PLAYER_INDEX()
			missionEndShardData.playerCut[iPlayer].playerName				= ""
			missionEndShardData.playerCut[iPlayer].iCut						= 0
		ENDFOR
		BOOL bPlayerValidForMembershipReward = FALSE
		FOR iPlayer = 0 TO MAX_CASINO_HEIST_PLAYERS-1
			missionEndShardData.playerCut[iPlayer].bLeader 					= (iGangBoss = g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerID[iPlayer])
			missionEndShardData.playerCut[iPlayer].playerID 					= INT_TO_PLAYERINDEX(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerID[iPlayer])
			bPlayerValidForMembershipReward = FALSE
			IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerID[iPlayer]),FALSE)
				missionEndShardData.playerCut[iPlayer].playerName 					= GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerID[iPlayer]))
				PRINTLN("[RCC MISSION][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - g_TransitionSessionNonResetVars.sGlobalCelebrationData[", iPlayer, "].name		= ", missionEndShardData.playerCut[iPlayer].playerName)	
				#IF FEATURE_GTAO_MEMBERSHIP
				bPlayerValidForMembershipReward = CAN_PLAYER_ACCESS_CASINO_HEIST_MEMBERSHIP_REWARDS(INT_TO_PLAYERINDEX(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerID[iPlayer]))
				#ENDIF
			ENDIF

			missionEndShardData.playerCut[iPlayer].iCut						= ROUND( (GET_CASINO_HEIST_TOTAL_TAKE(bPlayerValidForMembershipReward, fTotalTake, fSupportCrewTake, fSupportCrewTakeMembership, FALSE) * (TO_FLOAT(g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRewardCut[iPlayer]) / 100 )))	

			PRINTLN("[RCC MISSION][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - iGangBoss 	 																			= ", iGangBoss)
			PRINTLN("[RCC MISSION][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerID[", iPlayer, "]			= ", g_TransitionSessionNonResetVars.sGlobalCelebrationData.iPlayerID[iPlayer])			
			PRINTLN("[RCC MISSION][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - missionEndShardData.playerCut[", iPlayer, "].bLeader 	 								= ", missionEndShardData.playerCut[iPlayer].bLeader 					)
			PRINTLN("[RCC MISSION][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - missionEndShardData.playerCut[", iPlayer, "].playerID 	 								= ", NATIVE_TO_INT(missionEndShardData.playerCut[iPlayer].playerID) 	)
			PRINTLN("[RCC MISSION][HeistPassedShard] - CALCULATE_CASINO_HEIST_CASH_REWARD - missionEndShardData.playerCut[", iPlayer, "].iCut		 								= ", missionEndShardData.playerCut[iPlayer].iCut						)			
		ENDFOR
	ENDIF
	
	missionEndShardData.iApproachID										= ENUM_TO_INT(GET_PLAYER_CASINO_HEIST_APPROACH(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
	missionEndShardData.iTargetID 										= ENUM_TO_INT(GET_PLAYER_CASINO_HEIST_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
	missionEndShardData.npcTake[MISSION_END_SHARD_NPC_WEAPON].iNPCID	= ENUM_TO_INT(GET_PLAYER_CASINO_HEIST_WEAPONS_EXPERT(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
	missionEndShardData.npcTake[MISSION_END_SHARD_NPC_HACKER].iNPCID	= ENUM_TO_INT(GET_PLAYER_CASINO_HEIST_HACKER(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
	missionEndShardData.npcTake[MISSION_END_SHARD_NPC_DRIVER].iNPCID 	= ENUM_TO_INT(GET_PLAYER_CASINO_HEIST_DRIVER(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
	
	RETURN iTotalTake
	
ENDFUNC

FUNC FLOAT GET_HEISTS_PREP_MISSION_PERCENTAGE_CUT_VALUE(INT iCashReward)

	SWITCH(iCashReward)

		CASE FMMC_CASH_TUTORIAL
			PRINTLN("[RCC MISSION] [CASH_RWD] GET_HEISTS_PREP_MISSION_PERCENTAGE_CUT_VALUE return g_sMPTunables.fHeists_Prep_Percentage_Cut_Fleeca = ",g_sMPTunables.fHeists_Prep_Percentage_Cut_Fleeca)
			RETURN g_sMPTunables.fHeists_Prep_Percentage_Cut_Fleeca
			
		CASE FMMC_CASH_PRISON_BREAK
			PRINTLN("[RCC MISSION] [CASH_RWD] GET_HEISTS_PREP_MISSION_PERCENTAGE_CUT_VALUE return g_sMPTunables.fHeists_Prep_Percentage_Cut_Prison_Break = ",g_sMPTunables.fHeists_Prep_Percentage_Cut_Prison_Break)
			RETURN g_sMPTunables.fHeists_Prep_Percentage_Cut_Prison_Break	
			
		CASE FMMC_CASH_BIOLAB
			PRINTLN("[RCC MISSION] [CASH_RWD] GET_HEISTS_PREP_MISSION_PERCENTAGE_CUT_VALUE return g_sMPTunables.fHeists_Prep_Percentage_Cut_Humane_Labs = ",g_sMPTunables.fHeists_Prep_Percentage_Cut_Humane_Labs)
			RETURN g_sMPTunables.fHeists_Prep_Percentage_Cut_Humane_Labs
			
		CASE FMMC_CASH_NARCOTICS
			PRINTLN("[RCC MISSION] [CASH_RWD] GET_HEISTS_PREP_MISSION_PERCENTAGE_CUT_VALUE return g_sMPTunables.fHeists_Prep_Percentage_Cut_Series_A = ",g_sMPTunables.fHeists_Prep_Percentage_Cut_Series_A)
			RETURN g_sMPTunables.fHeists_Prep_Percentage_Cut_Series_A
			
		CASE FMMC_CASH_ORNATE_BANK
			PRINTLN("[RCC MISSION] [CASH_RWD] GET_HEISTS_PREP_MISSION_PERCENTAGE_CUT_VALUE return g_sMPTunables.fHeists_Prep_Percentage_Cut_Pacific_Standard = ",g_sMPTunables.fHeists_Prep_Percentage_Cut_Pacific_Standard)
			RETURN g_sMPTunables.fHeists_Prep_Percentage_Cut_Pacific_Standard
		
	ENDSWITCH
	
	PRINTLN("[RCC MISSION] [CASH_RWD] GET_HEISTS_PREP_MISSION_PERCENTAGE_CUT_VALUE No strand found returning 3.8")
	RETURN 3.8
	
ENDFUNC

FUNC INT CALCULATE_HEIST_PREP_CASH_REWARD(BOOL bPass,INT iTotalMissionEndTime,INT iHeistVehRepairCost, #IF FEATURE_GTAO_MEMBERSHIP PLAYER_INDEX piHeistLeader, BOOL bFinalTake, #ENDIF INT iTripSkipCost = 0)
	
	INT iCashGained = GET_CASH_VALUE_FROM_CREATOR(g_FMMC_STRUCT.iCashReward)
	
	//Cap max take for preps
	IF iCashGained > g_sMPTunables.iheist_setup_cash_calculation_max_take
		iCashGained  = g_sMPTunables.iheist_setup_cash_calculation_max_take
		PRINTLN("[RCC MISSION] [CASH_RWD] cap applied for preps: ",iCashGained)
	ENDIF
	
	SWITCH(g_FMMC_STRUCT.iDifficulity)
		CASE DIFF_EASY
			iCashGained = ROUND(iCashGained * g_sMPTunables.fheist_difficulty_easy)
			PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward to " ,iCashGained," since difficulty = EASY and difficulty modifier = ",g_sMPTunables.fheist_difficulty_easy)
		BREAK 
		CASE DIFF_NORMAL
			iCashGained = ROUND(iCashGained * g_sMPTunables.fheist_difficulty_normal)
			PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward to " ,iCashGained," since difficulty = NORMAL and difficulty modifier = ",g_sMPTunables.fheist_difficulty_normal)
		BREAK
		CASE DIFF_HARD
			iCashGained = ROUND(iCashGained * g_sMPTunables.fheist_difficulty_hard)
			PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward to " ,iCashGained," since difficulty = HARD and difficulty modifier = ",g_sMPTunables.fheist_difficulty_hard)
		BREAK
	ENDSWITCH
			
	
	FLOAT fCashGained = TO_FLOAT(iCashGained)
	fCashGained = (fCashGained * GET_HEISTS_PREP_MISSION_PERCENTAGE_CUT_VALUE(g_FMMC_STRUCT.iCashReward)) / 100.0
	PRINTLN("[RCC MISSION] [CASH_RWD] cash after ", GET_HEISTS_PREP_MISSION_PERCENTAGE_CUT_VALUE(g_FMMC_STRUCT.iCashReward), "% of total reward: ",fCashGained)

	IF bPass
		IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
			fCashGained = 0 
			iCashGained = ROUND(fCashGained)
			PRINTLN("[RCC MISSION] [CASH_RWD] setting heist planning cash reward to 0 since host: ",iCashGained)
		ELSE
			fCashGained -= TO_FLOAT(iTripSkipCost)
			PRINTLN("[RCC MISSION] [CASH_RWD] TRIP SKIP - cash after TripSkip Cost = : ",fCashGained)
		
			IF GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(g_FMMC_STRUCT.iRootContentIDHash,FALSE) > 0
				fCashGained = fCashGained*1.0
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward to full since not first play: ",iCashGained)
			ELSE
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION] [CASH_RWD] setting heist cash reward to full since first play: ",iCashGained)
			ENDIF
			IF iHeistVehRepairCost > 0
				iCashGained -= IMAX(IMIN(iHeistVehRepairCost,g_sMPTunables.imax_vehicle_repair_cost),g_sMPTunables.imin_vehicle_repair_cost)
				PRINTLN("[RCC MISSION] [CASH_RWD] deducting vehicle repair cost: ",iCashGained)
			ENDIF

			
			
		ENDIF
	ELSE
		IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
			fCashGained = 0
			iCashGained = ROUND(fCashGained)
			PRINTLN("[RCC MISSION] [CASH_RWD] lose setting heist planning cash reward to 0 since host: ",iCashGained)
		ELSE
			IF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_1*60000)
				FLOAT fTunableMulti = TO_FLOAT(g_sMPTunables.iHeists_Prep_fail_Cash_percentage_period_1)
				fTunableMulti = (fTunableMulti/100)
				fCashGained = fTunableMulti*fCashGained
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION][CASH_RWD] lose Heist prep setting cash reward since under 2 mins and tuneable set: ",iCashGained)
			ELIF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_2*60000)
				FLOAT fTunableMulti = TO_FLOAT(g_sMPTunables.iHeists_Prep_fail_Cash_percentage_period_2)
				fTunableMulti = (fTunableMulti/100)
				fCashGained = fTunableMulti*fCashGained
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION][CASH_RWD] lose Heist prep setting cash reward since under 4 mins to: ",iCashGained)
			ELIF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_3*60000)
				FLOAT fTunableMulti = TO_FLOAT(g_sMPTunables.iHeists_Prep_fail_Cash_percentage_period_3)
				fTunableMulti = (fTunableMulti/100)
				fCashGained = fTunableMulti*fCashGained
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION][CASH_RWD] lose Heist prep setting cash reward since under 7 mins to: ",iCashGained)
			ELIF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_4*60000)
				FLOAT fTunableMulti = TO_FLOAT(g_sMPTunables.iHeists_Prep_fail_Cash_percentage_period_4)
				fTunableMulti = (fTunableMulti/100)
				fCashGained = fTunableMulti*fCashGained
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION][CASH_RWD]lose Heist prep setting cash reward since under 10 mins to: ",iCashGained)
			ELIF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_5*60000)
				FLOAT fTunableMulti = TO_FLOAT(g_sMPTunables.iHeists_Prep_fail_Cash_percentage_period_5)
				fTunableMulti = (fTunableMulti/100)
				fCashGained = fTunableMulti*fCashGained
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION][CASH_RWD] lose Heist prep setting cash reward since under 12 mins to: ",iCashGained)
			ELIF iTotalMissionEndTime <= (g_sMPTunables.iHeist_Fail_Cash_time_period_6*60000)
				FLOAT fTunableMulti = TO_FLOAT(g_sMPTunables.iHeists_Prep_fail_Cash_percentage_period_6)
				fTunableMulti = (fTunableMulti/100)
				fCashGained = fTunableMulti*fCashGained
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION][CASH_RWD] lose Heist prep setting cash reward since under 15 mins to: ",iCashGained)
			ELSE
				FLOAT fTunableMulti = TO_FLOAT(g_sMPTunables.iHeists_Prep_fail_Cash_percentage_period_7)
				fTunableMulti = (fTunableMulti/100)
				fCashGained = fTunableMulti*fCashGained
				iCashGained = ROUND(fCashGained)
				PRINTLN("[RCC MISSION][CASH_RWD] lose Heist prep setting cash reward since over 15 mins to: ",iCashGained)
			ENDIF
		ENDIF
		
		IF IS_THIS_A_QUICK_RESTART_JOB() 
			iCashGained = 0
			PRINTLN("[RCC MISSION][CASH_RWD] lose Heist prep IS_THIS_A_QUICK_RESTART_JOB TRUE: ",iCashGained)
		ENDIF
	ENDIF
	
#IF FEATURE_GTAO_MEMBERSHIP
	IF bFinalTake
	AND CAN_PLAYER_ACCESS_HEIST_MEMBERSHIP_REWARDS(GET_PLAYER_INDEX(), piHeistLeader)
		IF g_sMPTunables.fMEMBERSHIP_Heists_PREP_REWARD_MULTIPLIER > 1.0
			iCashGained = ROUND(iCashGained * g_sMPTunables.fMEMBERSHIP_Heists_PREP_REWARD_MULTIPLIER)
			PRINTLN("[RCC MISSION][CASH_RWD] Membership reward boosted: ", iCashGained)
		ENDIF
	ENDIF
#ENDIF
	
	IF iCashGained < 0
		iCashGained = 0
		PRINTLN("[RCC MISSION][CASH_RWD] - iCashGained < 0 setting to 0 ")
	ENDIF
	
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [CASH_RWD] - CALCULATE_HEIST_PREP_CASH_REWARD - returning ", iCashGained)
	RETURN iCashGained
	
ENDFUNC

FUNC INT CALCULATE_GANGOPS_PREP_CASH_REWARD(BOOL bPass,INT iTotalMissionEndTime, INT iTripSkipCost = 0, INT iVehRepairCost = 0)
	
	IF NOT bPass
	AND IS_THIS_A_QUICK_RESTART_JOB() 
		PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_PREP_CASH_REWARD - Quick Restart Fail - NO CASH!")
		RETURN 0
	ENDIF
	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = PLAYER_ID()
		PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_PREP_CASH_REWARD - Boss - NO CASH!")
		RETURN 0
	ENDIF
	
	INT iCashGained = GET_CASH_VALUE_FROM_CREATOR(g_FMMC_STRUCT.iCashReward)
	
	SWITCH(g_FMMC_STRUCT.iDifficulity)
		CASE DIFF_NORMAL
			iCashGained = ROUND(iCashGained * g_sMPTunables.fGangops_difficulty_normal)
		BREAK
		CASE DIFF_HARD
			iCashGained = ROUND(iCashGained * g_sMPTunables.fGangops_difficulty_hard)
		BREAK
	ENDSWITCH
	PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_PREP_CASH_REWARD - Reward after difficulty scaling: ", iCashGained)
	
	FLOAT fCashGained = TO_FLOAT(iCashGained)

	IF bPass
		fCashGained -= TO_FLOAT(iTripSkipCost)
		PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_PREP_CASH_REWARD - Reward after trip sckip costs: ", fCashGained)
	
		IF NOT GANGOPS_HAS_STRAND_BEEN_COMPLETED_BEFORE()
			fCashGained = fCashGained * g_sMPTunables.fEarnings_Gangops_Prep_replay_cash_reward
			iCashGained = ROUND(fCashGained)
			PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_PREP_CASH_REWARD - Boss replay cash: ", iCashGained)
		ELSE
			fCashGained = fCashGained * g_sMPTunables.fEarnings_Gangops_Prep_first_play_cash_reward	
			iCashGained = ROUND(fCashGained)
			PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_PREP_CASH_REWARD - Boss first play cash: ", iCashGained)
		ENDIF
		
		IF iVehRepairCost > 0
			iCashGained -= IMAX(IMIN(iVehRepairCost,g_sMPTunables.iGangops_max_vehicle_repair_cost),g_sMPTunables.iGangops_min_vehicle_repair_cost)
			PRINTLN("[JS][CASH_RWD] CALCULATE_GANGOPS_PREP_CASH_REWARD - Deducting vehicle repair cost: ",iCashGained)
		ENDIF
	ELSE
		FLOAT fTunableMulti
		IF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_1*60000)
			fTunableMulti = TO_FLOAT(g_sMPTunables.iGangops_Prep_fail_Cash_percentage_period_1) / 100
			fCashGained = fTunableMulti * fCashGained
			iCashGained = ROUND(fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_2*60000)
			fTunableMulti = TO_FLOAT(g_sMPTunables.iGangops_Prep_fail_Cash_percentage_period_2) / 100
			fCashGained = fTunableMulti * fCashGained
			iCashGained = ROUND(fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_3*60000)
			fTunableMulti = TO_FLOAT(g_sMPTunables.iGangops_Prep_fail_Cash_percentage_period_3) / 100
			fCashGained = fTunableMulti * fCashGained
			iCashGained = ROUND(fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_4*60000)
			fTunableMulti = TO_FLOAT(g_sMPTunables.iGangops_Prep_fail_Cash_percentage_period_4) / 100
			fCashGained = fTunableMulti * fCashGained
			iCashGained = ROUND(fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_5*60000)
			fTunableMulti = TO_FLOAT(g_sMPTunables.iGangops_Prep_fail_Cash_percentage_period_5) / 100
			fCashGained = fTunableMulti * fCashGained
			iCashGained = ROUND(fCashGained)
		ELIF iTotalMissionEndTime <= (g_sMPTunables.iGangops_Fail_Cash_time_period_6*60000)
			fTunableMulti = TO_FLOAT(g_sMPTunables.iGangops_Prep_fail_Cash_percentage_period_6) / 100
			fCashGained = fTunableMulti * fCashGained
			iCashGained = ROUND(fCashGained)
		ELSE
			fTunableMulti = TO_FLOAT(g_sMPTunables.iGangops_Prep_fail_Cash_percentage_period_7) / 100
			fCashGained = fTunableMulti*fCashGained
			iCashGained = ROUND(fCashGained)
		ENDIF
		PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_PREP_CASH_REWARD - After Time Scaling: iTotalMissionEndTime = ", iTotalMissionEndTime, " iCashGained = ", iCashGained, " fTunableMulti = ", fTunableMulti)
	ENDIF
	
	IF iCashGained < 0
		iCashGained = 0
	ENDIF
		
	PRINTLN("[JS][CASH_RWD] - CALCULATE_GANGOPS_PREP_CASH_REWARD - Final Cash Reward: ", iCashGained)
	RETURN iCashGained
	
ENDFUNC

FUNC INT GET_HEIST_MAX_TAKE_FOR_EVERYONE(INT iTotalMissionEndTime, BOOL bForIntroShard = FALSE)
	INT iReturn = CALCULATE_HEIST_FINALE_CASH_REWARD( TRUE, FALSE, iTotalMissionEndTime #IF FEATURE_GTAO_MEMBERSHIP , NULL, FALSE #ENDIF )
	
	IF NOT bForIntroShard
		//iReturn += GET_BONUS_CASH_GAINED()
	ENDIF
	
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [CASH_RWD] - CALCULATE_HEIST_FINALE_CASH_REWARD - returning ", iReturn)
	RETURN iReturn
ENDFUNC

FUNC INT GET_HEIST_MAX_TAKE_FOR_THIS_PLAYER(INT iTotalMissionEndTime,BOOL bForIntroShard = FALSE)
	INT iReturn = CALCULATE_HEIST_FINALE_CASH_REWARD( TRUE, TRUE, iTotalMissionEndTime #IF FEATURE_GTAO_MEMBERSHIP , NULL, FALSE #ENDIF )
	
	IF NOT bForIntroShard
		//iReturn += GET_BONUS_CASH_GAINED()
	ENDIF
	
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [CASH_RWD] - GET_HEIST_MAX_TAKE_FOR_THIS_PLAYER - returning ", iReturn)
	RETURN iReturn
ENDFUNC



FUNC INT GET_HEIST_PREP_MAX_TAKE(INT iTotalMissionEndTime,INT iHeistVehRepairCost, BOOL bForIntroShard = FALSE)
	INT iCashGained
	iCashGained = CALCULATE_HEIST_PREP_CASH_REWARD(TRUE, iTotalMissionEndTime, iHeistVehRepairCost #IF FEATURE_GTAO_MEMBERSHIP , NULL, FALSE #ENDIF )
	
	IF NOT bForIntroShard

	ENDIF
	
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [CASH_RWD] - GET_HEIST_PREP_MAX_TAKE - returning ", iCashGained)
	RETURN iCashGained
ENDFUNC


FUNC INT GET_GANGOPS_MAX_TAKE_FOR_THIS_PLAYER(INT iTotalMissionEndTime)
	INT iReturn = CALCULATE_GANGOPS_FINALE_CASH_REWARD(TRUE, TRUE, iTotalMissionEndTime)
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [CASH_RWD] - CALCULATE_GANGOPS_FINALE_CASH_REWARD - returning ", iReturn)
	RETURN iReturn
ENDFUNC

FUNC INT GET_GANGOPS_PREP_MAX_TAKE(INT iTotalMissionEndTime)
	INT iCashGained
	iCashGained = CALCULATE_GANGOPS_PREP_CASH_REWARD(TRUE, iTotalMissionEndTime)
	
	PRINTLN("[RCC MISSION] - [NETCELEBRATION] - [CASH_RWD] - GET_GANGOPS_PREP_MAX_TAKE - returning ", iCashGained)
	RETURN iCashGained
ENDFUNC

/// PURPOSE: Sets the peds component variations and props, doesn't change any stats
PROC SET_PED_VARIATIONS_TESTMODE(PED_INDEX pedID, PED_VARIATION_STRUCT &sVariations)
	IF NOT IS_PED_INJURED(pedID)
		
		#IF IS_DEBUG_BUILD
		enumCharacterList eChar = GET_PLAYER_PED_ENUM(pedID)
		CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS_TESTMODE for ", eChar, " called by...", GET_THIS_SCRIPT_NAME())
		#ENDIF
		
		INT i
		REPEAT NUM_PED_COMPONENTS i
			SET_PED_COMPONENT_VARIATION(pedID, INT_TO_ENUM(PED_COMPONENT, i), sVariations.iDrawableVariation[i], sVariations.iTextureVariation[i], sVariations.iPaletteVariation[i])
			//CPRINTLN(DEBUG_PED_COMP, GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, i)), "...component variation [",sVariations.iDrawableVariation[i], ",", sVariations.iTextureVariation[i], "]")
		ENDREPEAT
		
		REPEAT NUM_PLAYER_PED_PROPS i
			IF sVariations.iPropIndex[i] != -1
			AND sVariations.iPropIndex[i] != 255
				//PRINTSTRING("...prop [")PRINTINT(sVariations.iPropIndex[i])PRINTSTRING(",")PRINTINT(sVariations.iPropTexture[i])PRINTSTRING("]")PRINTNL()
				SET_PED_PROP_INDEX(pedID, INT_TO_ENUM(PED_PROP_POSITION, i), sVariations.iPropIndex[i], sVariations.iPropTexture[i])
			ELSE
				CLEAR_PED_PROP(pedID, INT_TO_ENUM(PED_PROP_POSITION, i))
			ENDIF
		ENDREPEAT
		
	ENDIF
ENDPROC

FUNC BOOL IS_WEAPON_ONE_HANDED(WEAPON_TYPE weaponType)
	
	BOOL bTwoHanded = FALSE
	
	SWITCH weaponType
		CASE WEAPONTYPE_PISTOL
		CASE WEAPONTYPE_COMBATPISTOL
		CASE WEAPONTYPE_APPISTOL
		CASE WEAPONTYPE_MICROSMG
		CASE WEAPONTYPE_GRENADE
		CASE WEAPONTYPE_SMOKEGRENADE
		CASE WEAPONTYPE_STICKYBOMB
		CASE WEAPONTYPE_MOLOTOV
		CASE WEAPONTYPE_FLARE
		CASE WEAPONTYPE_STUNGUN
		CASE WEAPONTYPE_PETROLCAN
		CASE WEAPONTYPE_KNIFE
		CASE WEAPONTYPE_NIGHTSTICK
		CASE WEAPONTYPE_HAMMER
		CASE WEAPONTYPE_CROWBAR
		CASE WEAPONTYPE_DLC_PISTOL50
		CASE WEAPONTYPE_DLC_BOTTLE
		CASE WEAPONTYPE_DLC_HEAVYPISTOL
		CASE WEAPONTYPE_DLC_SNSPISTOL
		CASE WEAPONTYPE_DLC_DAGGER
		CASE WEAPONTYPE_DLC_VINTAGEPISTOL
		CASE WEAPONTYPE_DLC_FLAREGUN
		CASE WEAPONTYPE_DLC_PROXMINE
		CASE WEAPONTYPE_DLC_HATCHET
			bTwoHanded = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bTwoHanded
ENDFUNC

FUNC BOOL IS_WEAPON_A_THROWING_WEAPON(WEAPON_TYPE weaponType)
	
	BOOL bThrowing = FALSE
	
	SWITCH weaponType
		CASE WEAPONTYPE_GRENADE				
		CASE WEAPONTYPE_MOLOTOV
		CASE WEAPONTYPE_STICKYBOMB
		CASE WEAPONTYPE_SMOKEGRENADE
		CASE WEAPONTYPE_DLC_PROXMINE
			bThrowing = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bThrowing
	
ENDFUNC

///Find out what scenario anim this ped should use, depending on their equipped weapon
FUNC STRING GET_PED_FMMC_GOTO_WAIT_SCENARIO_STRING(PED_INDEX tempPed)
	
	STRING strScenario = "StandIgnorePavement" //Should be _0H (no weapons / throwing weapons equipped), but there isn't one...
	
	WEAPON_TYPE weaponType
	IF GET_CURRENT_PED_WEAPON(tempPed, weaponType)
		IF (weaponType != WEAPONTYPE_UNARMED)
		AND (weaponType != WEAPONTYPE_INVALID)
		AND (NOT IS_WEAPON_A_THROWING_WEAPON(weaponType))
			IF IS_WEAPON_A_MELEE_WEAPON(weaponType)
				IF IS_WEAPON_ONE_HANDED(weaponType)
					//Knives/nightstick/bottle etc grip - no specific anim, just use the one handed
					strScenario = "CODE_HUMAN_PATROL_1H"
				ELSE
					//Baseball bat/golf club grip - no specific anim, just use the one handed
					strScenario = "CODE_HUMAN_PATROL_1H"
				ENDIF
			ELSE
				//Weapon is a gun:
				IF IS_WEAPON_ONE_HANDED(weaponType)
					strScenario = "CODE_HUMAN_PATROL_1H"
				ELSE
					//Two handed:
					strScenario = "CODE_HUMAN_PATROL_2H"
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN strScenario
	
ENDFUNC

FUNC VECTOR GET_PED_COVER_LOCATION(INT iPed, PED_INDEX tempPed, INT iGotoProgress)
	
	VECTOR vCover
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vCoverLoc)
		vCover = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vCoverLoc
	ELSE
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetSix, ciPED_BSSix_DefensiveAreaMovesWithGotos)
			IF iGotoProgress > 0
			AND iGotoProgress < (MAX_ASSOCIATED_GOTO_TASKS + 1)
				vCover = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData.sPointData[(iGotoProgress - 1)].vPosition
			ENDIF
		ENDIF
		
		IF IS_VECTOR_ZERO(vCover)
			vCover = GET_ENTITY_COORDS(tempPed, FALSE)
		ENDIF
	ENDIF
	
	RETURN vCover
	
ENDFUNC

FUNC BOOL IS_PED_A_HIGH_PRIORITY_PED(INT iPed)
	INT i
	REPEAT FMMC_MAX_PRIORITY_PEDS i
		IF g_FMMC_STRUCT.iHighPriorityPed[i] = iped
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_WEAPON_MODEL_NAME(WEAPON_TYPE wtWeapon)
	
	MODEL_NAMES mnGun = DUMMY_MODEL_FOR_SCRIPT
	
	IF (wtWeapon != WEAPONTYPE_UNARMED AND wtWeapon != WEAPONTYPE_INVALID)
		mnGun = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_WEAPON_TEXTURE_NAME_FOR_UNLOCK_SCREEN(wtWeapon)))
	ENDIF
	
	RETURN mnGun
	
ENDFUNC

FUNC STRING GET_TREVOR_SECONDARY_ANIM(INT iAnimation)
	
	STRING sAnim
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_A
			sAnim = "shout_a"
		BREAK
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_B
			sAnim = "shout_b"
		BREAK
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_SHOUT_C
			sAnim = "shout_c"
		BREAK
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_A
			sAnim = "idle_a"
		BREAK
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_B
			sAnim = "idle_b"
		BREAK
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_C
			sAnim = "idle_c"
		BREAK
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_COVERIDLE_D
			sAnim = "idle_d"
		BREAK
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_01
			sAnim = "trevor_shout_vig_01"
		BREAK
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_L
			sAnim = "trevor_shout_vig_02_l"
		BREAK
		CASE ciPED_IDLE_ANIM__TREVOR_NARCFIN_VIGNETTE_02_R
			sAnim = "trevor_shout_vig_02_r"
		BREAK
	ENDSWITCH
	
	RETURN sAnim
	
ENDFUNC

FUNC BOOL IS_THIS_A_ROUNDS_RESTART()
	
	BOOL bRoundsRestart = FALSE
	
	IF IS_THIS_A_ROUNDS_MISSION()
	AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
		bRoundsRestart = TRUE
	ENDIF
	
	RETURN bRoundsRestart
	
ENDFUNC

FUNC BOOL IS_VECTOR_OUT_OF_MAP_BOUNDS(VECTOR vPos, FLOAT fTolerance = 0.0)
	
	BOOL bOutOfBounds
	
	IF vPos.X > (MAP_BOUNDS_POS_X + fTolerance)
	OR vPos.X < (MAP_BOUNDS_NEG_X - fTolerance)
	OR vPos.Y > (MAP_BOUNDS_POS_Y + fTolerance)
	OR vPos.Y < (MAP_BOUNDS_NEG_Y - fTolerance)
	OR vPos.Z > (MAP_BOUNDS_POS_Z + fTolerance)
		bOutOfBounds = TRUE
	ENDIF
	
	RETURN bOutOfBounds
	
ENDFUNC

////////////////////////////////////////////////////////////////////////////////////
///    							PULL CAM BACK (USED IN TRIP SKIP)
////////////////////////////////////////////////////////////////////////////////////
/*INT hintInterpInTime = 4000
INT hintHoldTime = 0
INT fadeStartTime = 2000
INT fadeDuration = 1000*/
FLOAT relativeVerticalOffset = 3.0
FLOAT relativePitchOffset = 0.0
FLOAT followDistanceScalar = 2.0
FLOAT vehicleLeadDistance = 100.0

PROC DO_PULL_BACK_CAM(INT hintInterpInTime = 4000, INT hintHoldTime = 0)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VECTOR min, max
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())), min, max)
		FLOAT scale = max.z - min.z
		SET_GAMEPLAY_VEHICLE_HINT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<0.0, vehicleLeadDistance, 0.0>>, TRUE, hintHoldTime, hintInterpInTime, 0)
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(relativeVerticalOffset*scale)
		SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(relativePitchOffset)
		SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(followDistanceScalar)
		PRINTLN(" PULL BACK CAM - DO_PULL_BACK_CAM - CALLED")
	ENDIF
ENDPROC

PROC CLEANUP_PULL_BACK_CAM()
	STOP_GAMEPLAY_HINT(TRUE)
	PRINTLN(" PULL BACK CAM - CLEANUP_PULL_BACK_CAM - CALLED")
ENDPROC

////////////////////////////////////////////////////////////////////////////////////
///    							HEIST TRIP SKIP
////////////////////////////////////////////////////////////////////////////////////

ENUM TRIP_SKIP_STAGE_ENUM
	TSS_INIT,			//0
	TSS_WAIT,			//1
	TSS_VOTE,			//2
	TSS_FADE_OUT,		//3
	TSS_WARP,			//4
	TSS_CAR,			//5
	TSS_SETUP_CAM,		//6
	TSS_INTERP_CAM,		//7
	TSS_CLEANUP,		//8
	TSS_FINISHED		//9
ENDENUM

CONST_INT TRIP_SKIP_VOTE_TIME 			0
CONST_INT TRIP_SKIP_INTERP_TIME 		7000
CONST_INT TRIP_SKIP_INTERP_TIME_FAST 	3000
CONST_INT TRIP_SKIP_DELAY_BEFORE_FADE	2000
CONST_INT TRIP_SKIP_FADE_OUT_TIME		1000
CONST_INT TRIP_SKIP_FADE_IN_TIME		4000
CONST_INT TRIP_SKIP_VOTE_PASSED_DELAY	1500

//SERVER BD TRIP SKIP DATA
STRUCT TRIP_SKIP_SERVER_BD
	TRIP_SKIP_STAGE_ENUM iStage = TSS_INIT
	INT iBitSet
	
	INT iTotalPlayers = 4
	FLOAT fPlayersVoted
	FLOAT fPlayersVotedStored
	INT iPlayersFadeOut
	INT iPlayersInCar
	INT iPlayersWarped
	INT iPlayersInterped
	
	INT iCar
	VEHICLE_INDEX viCar
	
	//NETWORK_INDEX niCar
	//MODEL_NAMES CarModel
	//PLAYER_INDEX PlayerInSeat[4]
	VECTOR vCarPos
	FLOAT fCarHeading
	
	VECTOR CamPos
	VECTOR CamRot
	FLOAT CamFOV
	
	//INT iCost
	//INT iStoredCost
	
	SCRIPT_TIMER InitDelay
	SCRIPT_TIMER TimeOut
	SCRIPT_TIMER VotePassedDelay
ENDSTRUCT 

//Trip Skip ServerBD BitSet Flags
CONST_INT TSS_BI_VOTE_PASSED				0
CONST_INT TSS_BI_ALL_FADED_OUT				1
CONST_INT TSS_BI_ALL_IN_CAR					2
CONST_INT TSS_BI_ALL_WARPED					3
CONST_INT TSS_BI_ALL_INTERPED				4
CONST_INT TSS_BI_TRIP_SKIP_AVAILABLE		5
CONST_INT TSS_BI_CAR_FOUND					6
CONST_INT TSS_BI_ALL_PLAYED_BEFORE			7
CONST_INT TSS_BI_DO_NOT_ALL_PLAYED_HELP		8
CONST_INT TSS_BI_NO_WANTED_LEVELS			9
CONST_INT TSS_BI_GOT_VEHICLE				10
CONST_INT TSS_BI_BLOCKED					11

//PLAYER BD TRIP SKIP DATA
STRUCT TRIP_SKIP_PLAYER_BD
	TRIP_SKIP_STAGE_ENUM iStage = TSS_INIT
	INT iBitSet
	INT iCost
	INT iStoredCost
ENDSTRUCT

//Trip Skip PlayerBD BitSet Flags
CONST_INT TSP_BI_VOTED			0
CONST_INT TSP_BI_FADED_OUT		1
CONST_INT TSP_BI_IN_CAR			2
CONST_INT TSP_BI_WARP_DONE		3
CONST_INT TSP_BI_INTERP_DONE	4
CONST_INT TSP_BI_PLAYED_BEFORE	5
CONST_INT TSP_BI_TOO_CLOSE		6

VEHICLE_INDEX viTripSkipPrevVeh

//LOCAL TRIP SKIP DATA
STRUCT TRIP_SKIP_LOCAL_DATA
	INT iBitSet
	SCRIPT_TIMER InterpTimer
	SCRIPT_TIMER FreezeEffectTimer
	SCRIPT_TIMER LocalVoteDisplayTimer
	INT iLocalVoteCount
	CAMERA_INDEX Cam
ENDSTRUCT 

//Trip Skip Local BitSet Flags
CONST_INT TSL_BI_HELP_TEXT_SHOWN			0
CONST_INT TSL_BI_NOT_ALL_PLAYED_HELP_SHOWN	1
CONST_INT TSL_BI_HELP_TEXT_SHOWN2			2
CONST_INT TSL_BI_CLEAR_TASKS_DONE			3

//PURPOSE: Returns TRUE when a Trip Skip is happening. Ser bIncludeCamInterp to make this include the interp from Trip SKip cam down to player cam.
FUNC BOOL IS_TRIP_SKIP_IN_PROGRESS(TRIP_SKIP_PLAYER_BD &TSPlayerBD, BOOL bIncludeCamInterp = FALSE)
	IF bIncludeCamInterp = FALSE
		IF TSPlayerBD.iStage >= TSS_FADE_OUT
		AND TSPlayerBD.iStage < TSS_INTERP_CAM
			RETURN TRUE
		ENDIF
	ELSE
		IF TSPlayerBD.iStage >= TSS_FADE_OUT
		AND TSPlayerBD.iStage <= TSS_INTERP_CAM
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Checks a global flag to see if a Trip Skip is in progress
FUNC BOOL IS_TRIP_SKIP_IN_PROGRESS_GLOBAL()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_TRIP_SKIP_IN_PROGRESS)
ENDFUNC

//PURPOSE: Runs Initialisation Checks at Start of Heist
PROC INIT_TRIP_SKIP(TRIP_SKIP_PLAYER_BD &TSPlayerBD, BOOL bPlayedBefore)
	IF NOT IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_PLAYED_BEFORE)
		IF bPlayedBefore
		OR IS_FAKE_MULTIPLAYER_MODE_SET()
		#IF IS_DEBUG_BUILD OR (GET_COMMANDLINE_PARAM_EXISTS("sc_IvePlayedAllMissions")) #ENDIF
			SET_BIT(TSPlayerBD.iBitSet, TSP_BI_PLAYED_BEFORE)
			PRINTLN(" TRIP SKIP - CLIENT - INIT_TRIP_SKIP - TSP_BI_PLAYED_BEFORE - SET - A")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Checks if the player is using a vehicle with a friendly team member
FUNC BOOL IS_PLAYER_IN_VALID_TRIP_SKIP_CAR(PLAYER_INDEX playerID)
	IF IS_NET_PLAYER_OK(playerID)
		IF IS_PED_SITTING_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
			
			VEHICLE_INDEX thisVeh = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(playerID))
			
			IF IS_VEHICLE_DRIVEABLE(thisVeh)
				IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(thisVeh))
				OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(thisVeh))
				OR GET_ENTITY_MODEL(thisVeh) = STROMBERG
					IF IS_VEHICLE_ON_ALL_WHEELS(thisVeh)
					OR (GET_ENTITY_HEIGHT_ABOVE_GROUND(thisVeh) < 0.5 AND GET_ENTITY_SPEED(thisVeh) > 2.0) // This stops slightly bumping terrain on fast cars breaking the Trip Skip availability.
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL) //ONLY DO TEAM CHECK FOR SEPARATE TEAM TRIP SKIPS
							INT i
							REPEAT (GET_VEHICLE_NUMBER_OF_PASSENGERS(thisVeh)+1) i
								PED_INDEX thisPassenger
								thisPassenger = GET_PED_IN_VEHICLE_SEAT(thisVeh, INT_TO_ENUM(VEHICLE_SEAT, i-1))
								IF thisPassenger != NULL
									IF NOT IS_PED_INJURED(thisPassenger)
										IF IS_PED_A_PLAYER(thisPassenger)
											IF GET_PLAYER_PED(playerID) != thisPassenger
												IF GET_PLAYER_TEAM(playerID) != GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPassenger))
													PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_VALID_TRIP_SKIP_CAR = FALSE - INSIDE WITH OTHER TEAM - PLAYER = ", GET_PLAYER_NAME(playerID))
													RETURN FALSE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELIF i = 0
									PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_VALID_TRIP_SKIP_CAR = FALSE - NO DRIVER!")
									RETURN FALSE
								ENDIF
							ENDREPEAT
						ENDIF
					ELSE
						PRINTLN(" TRIP SKIP - CLIENT - IS_VEHICLE_ON_ALL_WHEELS = FALSE - VEHICLE NOT ON ALL WHEELS - PLAYER = ", GET_PLAYER_NAME(playerID))
						RETURN FALSE
					ENDIF
				ELSE					
					PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_VALID_TRIP_SKIP_CAR = FALSE - VEHICLE NOT A CAR OR BIKE - PLAYER = ", GET_PLAYER_NAME(playerID))
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_VALID_TRIP_SKIP_CAR = FALSE - VEHICLE NOT DRIVEABLE - PLAYER = ", GET_PLAYER_NAME(playerID))
				RETURN FALSE
			ENDIF
		ELSE
			PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_VALID_TRIP_SKIP_CAR = FALSE - NOT IN VEHICLE - PLAYER = ", GET_PLAYER_NAME(playerID))
			RETURN FALSE
		ENDIF
	ELSE
		PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_VALID_TRIP_SKIP_CAR = FALSE - PLAYER NOT OKAY - PLAYER = ", GET_PLAYER_NAME(playerID))
		RETURN FALSE
	ENDIF
	
	PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_VALID_TRIP_SKIP_CAR = TRUE - PLAYER = ", GET_PLAYER_NAME(playerID))
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ALTERNATIVE_TRIPSKIP_VEHICLE(VEHICLE_INDEX vehIndex)
	IF DOES_ENTITY_EXIST(vehIndex)	
		MODEL_NAMES vehMN = GET_ENTITY_MODEL(vehIndex)
		
		IF IS_THIS_MODEL_A_TURRET_VEHICLE(vehMN)
		OR vehMN = STROMBERG
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_BUTTON_PRESS_FOR_TRIPSKIP_BASED_ON_VEH(VEHICLE_INDEX vehIndex)
	IF IS_ALTERNATIVE_TRIPSKIP_VEHICLE(vehIndex)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
		RETURN IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN) OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	ELSE
		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
		AND IS_VEHICLE_SIREN_ON(vehIndex)
			SET_VEHICLE_SIREN(vehIndex, FALSE)
		ENDIF
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
		RETURN IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Display Help Text when safe to tell players a Trip Skip is available.
PROC DISPLAY_TRIP_SKIP_HELP(TRIP_SKIP_SERVER_BD &TSServerBD, TRIP_SKIP_PLAYER_BD &TSPlayerBD, TRIP_SKIP_LOCAL_DATA &TSLocalData, INT iCost)
	IF NOT IS_BIT_SET(TSLocalData.iBitSet, TSL_BI_HELP_TEXT_SHOWN)
	OR NOT IS_BIT_SET(TSLocalData.iBitSet, TSL_BI_HELP_TEXT_SHOWN2)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		//AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND NOT g_bAtmShowing
		AND NOT IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_BROWSER_OPEN()
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		AND NOT IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_VOTED)
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
			INT iStatInt
			iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT7)
			IF NOT IS_BIT_SET(iStatInt, biNmh7_TripSkipHelp1)
			AND NOT IS_BIT_SET(TSLocalData.iBitSet, TSL_BI_HELP_TEXT_SHOWN)
				PRINT_HELP("TS_HELP3") //Trip Skip available. All players traveling to the same destination must be in a vehicle together in order to activate it. Trip skipping is not allowed when players going to separate destinations are in the same vehicle.
				SET_BIT(TSLocalData.iBitSet, TSL_BI_HELP_TEXT_SHOWN)
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSL_BI_HELP_TEXT_SHOWN - SET")
				SET_BIT(iStatInt, biNmh7_TripSkipHelp1)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT7, iStatInt)
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - biNmh7_TripSkipHelp1 - SET")
									
			ELIF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_IN_CAR)
			AND NOT IS_BIT_SET(TSLocalData.iBitSet, TSL_BI_HELP_TEXT_SHOWN2)
				IF IS_PLAYER_IN_VALID_TRIP_SKIP_CAR(PLAYER_ID())
					IF NOT IS_BIT_SET(iStatInt, biNmh7_TripSkipHelp2)
						
						TEXT_LABEL tl15 = "TS_HELP"
						IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
							tl15 = "TS_HELPALT"
						ENDIF
						
						PRINT_HELP_WITH_NUMBER(tl15, iCost, 20000) //Vote to skip your team's journey by pressing ~INPUT_SCRIPT_PAD_RIGHT~. The skip will deduct $~1~ from each player's earnings on successful completion. The Heist Leader has the deciding vote if the result is tied.						
						
						SET_BIT(TSLocalData.iBitSet, TSL_BI_HELP_TEXT_SHOWN2)
						PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSL_BI_HELP_TEXT_SHOWN2 - SET")
						SET_BIT(iStatInt, biNmh7_TripSkipHelp2)
						SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT7, iStatInt)
						PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - biNmh7_TripSkipHelp2 - SET")
					ELSE
						VEHICLE_INDEX vehID
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						ENDIF
						
						IF DOES_ENTITY_EXIST(vehID)
						AND IS_ALTERNATIVE_TRIPSKIP_VEHICLE(vehID)
							PRINT_HELP_WITH_NUMBER("TS_HELP5", iCost, 20000) //Press ~INPUT_VEH_HORN~ to Trip Skip - $~1~.
							SET_BIT(TSLocalData.iBitSet, TSL_BI_HELP_TEXT_SHOWN2)
							PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSL_BI_HELP_TEXT_SHOWN2 - SET")	//ADDED FOR BUG 2225727
						ELSE
							PRINT_HELP_WITH_NUMBER("TS_HELP4", iCost, 20000) //Press ~INPUT_SCRIPT_PAD_RIGHT~ to Trip Skip - $~1~.
							SET_BIT(TSLocalData.iBitSet, TSL_BI_HELP_TEXT_SHOWN2)
							PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSL_BI_HELP_TEXT_SHOWN2 - SET")	//ADDED FOR BUG 2225727
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: 
PROC TRACK_AND_DISPLAY_VOTE(TRIP_SKIP_SERVER_BD &TSServerBD, TRIP_SKIP_PLAYER_BD &TSPlayerBD, TRIP_SKIP_LOCAL_DATA &TSLocalData)
	//Check if I have Voted
	IF NOT IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_VOTED)
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
			IF GET_BUTTON_PRESS_FOR_TRIPSKIP_BASED_ON_VEH(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP", TSPlayerBD.iStoredCost)
				OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP4", TSPlayerBD.iStoredCost)
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TS_HELP3")
				OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP5", TSPlayerBD.iStoredCost)
					CLEAR_HELP()
				ENDIF
				SET_BIT(TSPlayerBD.iBitSet, TSP_BI_VOTED)
				PRINTLN(" TRIP SKIP - CLIENT - TRACK_AND_DISPLAY_VOTE - TSP_BI_VOTED - SET")
				//Setup for Local Vote Display (so we can see our vote instantly)
				TSLocalData.iLocalVoteCount = FLOOR(TSServerBD.fPlayersVotedStored + 1)
				REINIT_NET_TIMER(TSLocalData.LocalVoteDisplayTimer)
				PRINTLN(" TRIP SKIP - CLIENT - TRACK_AND_DISPLAY_VOTE - START LocalVoteDisplayTimer")
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			CLEAR_BIT(TSPlayerBD.iBitSet, TSP_BI_VOTED)
			PRINTLN(" TRIP SKIP - CLIENT - TRACK_AND_DISPLAY_VOTE - TSP_BI_VOTED - CLEARED")
		ENDIF
	ENDIF
	
	//Draw Vote Display
	IF NOT HAS_NET_TIMER_STARTED(TSLocalData.LocalVoteDisplayTimer)
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(FLOOR(TSServerBD.fPlayersVotedStored), TSServerBD.iTotalPlayers, "TS_VOTE")
	ELSE
		INT iVote = TSLocalData.iLocalVoteCount
		IF FLOOR(TSServerBD.fPlayersVotedStored) > TSLocalData.iLocalVoteCount
			iVote = FLOOR(TSServerBD.fPlayersVotedStored)
		ENDIF
		
		// Sometimes this can happen when people are all spamming the buttons, the ui briefly changes to 5/4 for example. Due to this:TSLocalData.iLocalVoteCount = FLOOR(TSServerBD.fPlayersVotedStored + 1) above.
		IF iVote > TSServerBD.iTotalPlayers
			iVote = TSServerBD.iTotalPlayers
		ENDIF
				
		IF HAS_NET_TIMER_EXPIRED(TSLocalData.LocalVoteDisplayTimer, 5000)
			RESET_NET_TIMER(TSLocalData.LocalVoteDisplayTimer)
			PRINTLN(" TRIP SKIP - CLIENT - TRACK_AND_DISPLAY_VOTE - RESET LocalVoteDisplayTimer")
		ENDIF
		
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(iVote, TSServerBD.iTotalPlayers, "TS_VOTE")
	ENDIF
ENDPROC

//PURPOSE: Return TRUE once the local player is in the Vehicle
//FUNC BOOL PLAYER_IN_TRIP_SKIP_CAR(TRIP_SKIP_SERVER_BD &TSServerBD, TRIP_SKIP_PLAYER_BD &TSPlayerBD)
//	IF DOES_ENTITY_EXIST(NET_TO_VEH(TSServerBD.niCar))
//		IF IS_NET_PLAYER_OK(PLAYER_ID())
//			IF NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(TSServerBD.niCar))
//				INT i
//				REPEAT 4 i
//					IF TSServerBD.PlayerInSeat[i] = PLAYER_ID()
//						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(TSServerBD.niCar), INT_TO_ENUM(VEHICLE_SEAT, i-1))
//						i = 4
//						PRINTLN(" TRIP SKIP - PLAYER_IN_TRIP_SKIP_CAR - PUT PLAYER IN SEAT ", i-1)
//					ENDIF
//				ENDREPEAT
//			ELSE
//				SET_BIT(TSPlayerBD.iBitSet, TSP_BI_IN_CAR)
//				PRINTLN(" TRIP SKIP - PLAYER_IN_TRIP_SKIP_CAR - TSP_BI_IN_CAR - SET")
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

//PURPOSE: Makes sure the player is in the Trip Skip Car.
FUNC BOOL IS_PLAYER_IN_TRIP_SKIP_CAR(BOOL bWholeCrew)
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX thisCar
		
		thisCar = GET_LAST_DRIVEN_VEHICLE()
		IF NOT DOES_ENTITY_EXIST(thisCar)
			INT i
			REPEAT NUM_NETWORK_PLAYERS i
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
					PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
					IF IS_NET_PLAYER_OK(PlayerId)
						IF IS_PED_IN_ANY_VEHICLE(PlayerPedId)
							IF bWholeCrew
								IF IS_PED_IN_ANY_VEHICLE(PlayerPedId)
									thisCar = GET_VEHICLE_PED_IS_USING(PlayerPedId)
									i = NUM_NETWORK_PLAYERS
								ENDIF
							ELSE
								IF GET_PLAYER_TEAM(PLAYER_ID()) = GET_PLAYER_TEAM(PlayerId)
									IF IS_PED_IN_ANY_VEHICLE(PlayerPedId)
										thisCar = GET_VEHICLE_PED_IS_USING(PlayerPedId)
										i = NUM_NETWORK_PLAYERS
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF DOES_ENTITY_EXIST(thisCar)
			IF IS_ENTITY_A_VEHICLE(thisCar)
				IF IS_VEHICLE_DRIVEABLE(thisCar)
					VEHICLE_SEAT VehSeat = GET_FIRST_FREE_VEHICLE_SEAT(thisCar)
					TASK_ENTER_VEHICLE(PLAYER_PED_ID(), thisCar, -1, VehSeat, DEFAULT, ECF_WARP_PED)
					PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_TRIP_SKIP_CAR - WARP PLAYER INTO CAR = SEAT = ", ENUM_TO_INT(VehSeat))
					RETURN TRUE
				ELSE
					PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_TRIP_SKIP_CAR - Vehicle is no longer driveable..")
				ENDIF
			ENDIF
		ELSE
			PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_TRIP_SKIP_CAR - Does not exist.")
		ENDIF
	ELSE
		PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_TRIP_SKIP_CAR - ALREADY IN THE CAR")
		RETURN TRUE
	ENDIF
	
	PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_TRIP_SKIP_CAR - Returning False")
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Makes sure the player is in the Trip Skip Car.
FUNC BOOL IS_PLAYER_IS_IN_FORCED_TRIP_SKIP_CAR(TRIP_SKIP_SERVER_BD &TSServerBD)
	//MAKE SURE HOST IS IN STORED CAR
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF TSServerBD.viCar != NULL
			IF DOES_ENTITY_EXIST(TSServerBD.viCar)
				IF IS_ENTITY_A_VEHICLE(TSServerBD.viCar)
					IF IS_VEHICLE_DRIVEABLE(TSServerBD.viCar)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), TSServerBD.viCar)
							TASK_ENTER_VEHICLE(PLAYER_PED_ID(), TSServerBD.viCar, -1, GET_FIRST_FREE_VEHICLE_SEAT(TSServerBD.viCar), DEFAULT, ECF_WARP_PED)
							PRINTLN(" TRIP SKIP - SERVER - IS_PLAYER_IN_TRIP_SKIP_CAR - WARP PLAYER INTO CAR - CarID = ", NATIVE_TO_INT(TSServerBD.viCar))
							RETURN TRUE
						ELSE
							PRINTLN(" TRIP SKIP - SERVER - IS_PLAYER_IN_TRIP_SKIP_CAR - ALREADY IN CAR - CarID = ", NATIVE_TO_INT(TSServerBD.viCar))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			TSServerBD.viCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			SET_BIT(TSServerBD.iBitSet, TSS_BI_GOT_VEHICLE)
			#IF IS_DEBUG_BUILD PRINTLN(" TRIP SKIP - SERVER - IS_PLAYER_IN_TRIP_SKIP_CAR - STORING THIS SERVERS CAR FOR CHECKS ", " - CarID = ", NATIVE_TO_INT(TSServerBD.viCar)) #ENDIF
		ENDIF
	
	//MAKE SURE CLIENT IS IN HOST CAR
	ELSE
		PED_INDEX HostPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
		IF DOES_ENTITY_EXIST(HostPed)
			VEHICLE_INDEX thisCar = GET_VEHICLE_PED_IS_IN(HostPed)
			IF DOES_ENTITY_EXIST(thisCar)
				IF IS_VEHICLE_DRIVEABLE(thisCar)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), thisCar)
						TASK_ENTER_VEHICLE(PLAYER_PED_ID(), thisCar, -1, GET_FIRST_FREE_VEHICLE_SEAT(thisCar), DEFAULT, ECF_WARP_PED)
						PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_TRIP_SKIP_CAR - WARP PLAYER INTO CAR")
						RETURN TRUE
					ELSE
						PRINTLN(" TRIP SKIP - CLIENT - IS_PLAYER_IN_TRIP_SKIP_CAR - ALREADY IN THE CAR")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Makes the Car drive along slowly
PROC MAINTAIN_TRIP_SKIP_CAR_DRIVING(FLOAT fSpeed = 15.0)
	VEHICLE_INDEX thisCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
	IF DOES_ENTITY_EXIST(thisCar)
		IF IS_VEHICLE_DRIVEABLE(thisCar)
			IF GET_PED_IN_VEHICLE_SEAT(thisCar) = PLAYER_PED_ID()
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != WAITING_TO_START_TASK
					TASK_VEHICLE_DRIVE_WANDER(PLAYER_PED_ID(), thisCar, fSpeed, DRIVINGMODE_STOPFORCARS)
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP_CAR_DRIVING - TASK DRIVE WANDER - SPEED = ", fSpeed)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Makes the Car lock and kicks out unwanted players
PROC LOCK_TRIP_SKIP_CAR()
	VEHICLE_INDEX thisCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
	IF DOES_ENTITY_EXIST(thisCar)
		IF IS_VEHICLE_DRIVEABLE(thisCar)
			IF GET_PED_IN_VEHICLE_SEAT(thisCar) = PLAYER_PED_ID()
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL) //ONLY DO KICK OUT FOR SEPARATE TEAM TRIP SKIPS
					INT i
					REPEAT (GET_VEHICLE_NUMBER_OF_PASSENGERS(thisCar)+1) i
						PED_INDEX thisPassenger
						thisPassenger = GET_PED_IN_VEHICLE_SEAT(thisCar, INT_TO_ENUM(VEHICLE_SEAT, i-1))
						IF thisPassenger != NULL
							IF NOT IS_PED_INJURED(thisPassenger)
								IF IS_PED_A_PLAYER(thisPassenger)
									IF PLAYER_PED_ID() != thisPassenger
										IF GET_PLAYER_TEAM(PLAYER_ID()) != GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPassenger))
											BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPassenger)), FALSE, 0, 0, FALSE, FALSE)
											PRINTLN(" TRIP SKIP - CLIENT - LOCK_TRIP_SKIP_CAR - KICKED OUT ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPassenger)))
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				SET_VEHICLE_DOORS_LOCKED(thisCar, VEHICLELOCK_LOCKED)
				SET_ENTITY_INVINCIBLE(thisCar, TRUE)
				PRINTLN(" TRIP SKIP - CLIENT - LOCK_TRIP_SKIP_CAR - DONE")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Unlocks the Car
PROC UNLOCK_TRIP_SKIP_CAR()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX thisCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
		IF DOES_ENTITY_EXIST(thisCar)
			IF IS_VEHICLE_DRIVEABLE(thisCar)
				IF GET_PED_IN_VEHICLE_SEAT(thisCar) = PLAYER_PED_ID()
					SET_VEHICLE_DOORS_LOCKED(thisCar, VEHICLELOCK_UNLOCKED)
					SET_ENTITY_INVINCIBLE(thisCar, FALSE)
					PRINTLN(" TRIP SKIP - CLIENT - UNLOCK_TRIP_SKIP_CAR - DONE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Attempts to fade out the Vehicle and returns TRUE when ready
FUNC BOOL CAR_READY_FOR_TRIP_SKIP_WARP()
	VEHICLE_INDEX thisCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	IF DOES_ENTITY_EXIST(thisCar)
		IF IS_VEHICLE_DRIVEABLE(thisCar)
			IF IS_ENTITY_VISIBLE_TO_SCRIPT(thisCar)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(thisCar)
					NETWORK_FADE_OUT_ENTITY(thisCar, TRUE, TRUE) 
					PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_WARP - NETWORK_FADE_OUT_ENTITY RUNNING - BY ME")
				ENDIF
				
				PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_WARP - FALSE - NETWORK_FADE_OUT_ENTITY RUNNING")
				RETURN FALSE
				
			ELSE
				IF NETWORK_IS_ENTITY_FADING(thisCar)
					PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_WARP - FALSE - CAR FADING")
					RETURN FALSE
				ENDIF
				
				PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_WARP - CAR NOT VISIBLE - OKAY TO WARP")
			ENDIF
		ELSE
			PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_WARP - CAR NOT DRIVEABLE")
		ENDIF
	ELSE
		PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_WARP - CAR DOESN'T EXIST")
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Attempts to fade out the Vehicle and returns TRUE when ready
FUNC BOOL CAR_READY_FOR_TRIP_SKIP_FADE_IN()
	VEHICLE_INDEX thisCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	IF DOES_ENTITY_EXIST(thisCar)
		IF IS_VEHICLE_DRIVEABLE(thisCar)
			IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(thisCar)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(thisCar)
					NETWORK_FADE_IN_ENTITY(thisCar, TRUE) 
					PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_FADE_IN - NETWORK_FADE_IN_ENTITY RUNNING - BY ME")
				ENDIF
				
				PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_FADE_IN - FALSE - NETWORK_FADE_IN_ENTITY RUNNING")
				RETURN FALSE
				
			ELSE
				IF NETWORK_IS_ENTITY_FADING(thisCar)
					PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_FADE_IN - FALSE - CAR FADING")
					RETURN FALSE
				ENDIF
				
				PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_FADE_IN - CAR NOT VISIBLE - OKAY TO WARP")
			ENDIF
		ELSE
			PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_FADE_IN - CAR NOT DRIVEABLE")
		ENDIF
	ELSE
		PRINTLN(" TRIP SKIP - CLIENT - CAR_READY_FOR_TRIP_SKIP_FADE_IN - CAR DOESN'T EXIST")
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates and sets up the Trip Skip Camera
FUNC BOOL CREATE_TRIP_SKIP_CAM(TRIP_SKIP_SERVER_BD &TSServerBD, TRIP_SKIP_LOCAL_DATA &TSLocalData)
	IF NOT DOES_CAM_EXIST(TSLocalData.Cam)
		TSLocalData.Cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
		SET_CAM_ACTIVE(TSLocalData.Cam, TRUE)
		SET_CAM_PARAMS(TSLocalData.Cam, TSServerBD.CamPos, TSServerBD.CamRot, TSServerBD.CamFOV)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		PRINTLN(" TRIP SKIP - CLIENT - CREATE_TRIP_SKIP_CAM - CAM CREATED")
	ELSE
		BOOL bShouldInterp = TRUE
		INT iInterpTime = TRIP_SKIP_INTERP_TIME
		
		IF IS_PLAYER_USING_TURRET()			
			IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PLAYER_ID()))) = BARRAGE
			AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_BACK_RIGHT // second turret (back)
				bShouldInterp = FALSE
				iInterpTime = TRIP_SKIP_INTERP_TIME_FAST
			ENDIF
		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		SET_CAM_ACTIVE(TSLocalData.Cam, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, bShouldInterp, iInterpTime)
		PRINTLN(" TRIP SKIP - CLIENT - CREATE_TRIP_SKIP_CAM - INTERP TO GAME CAM")		
		ANIMPOSTFX_PLAY("HeistTripSkipFade", 0, FALSE)
		PRINTLN(" TRIP SKIP - CLIENT - CREATE_TRIP_SKIP_CAM - ANIMPOSTFX_PLAY - HeistTripSkipFade")
		//TOGGLE_RENDERPHASES(TRUE)
		//PRINTLN(" TRIP SKIP - CLIENT - CREATE_TRIP_SKIP_CAM - TOGGLE_RENDERPHASES(TRUE)")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns the Trip Skip cost in $ based on distance
FUNC INT GET_TRIP_SKIP_COST(TRIP_SKIP_SERVER_BD &TSServerBD)
	FLOAT fDistance
	fDistance = CONVERT_METERS_TO_MILES(GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), TSServerBD.vCarPos))
	PRINTLN(" TRIP SKIP - CLIENT - GET_TRIP_SKIP_COST - Meters = ", GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), TSServerBD.vCarPos))
	PRINTLN(" TRIP SKIP - CLIENT - GET_TRIP_SKIP_COST - Miles = ", fDistance)
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
		IF fDistance < g_sMPTunables.IH3_TRIP_SKIP_DISTANCE_SHORT_MAX
			RETURN g_sMPTunables.IH3_TRIP_SKIP_COST_LOW
		ELIF fDistance >= g_sMPTunables.IH3_TRIP_SKIP_DISTANCE_MED_MIN AND fDistance <= g_sMPTunables.IH3_TRIP_SKIP_DISTANCE_MED_MAX
			RETURN g_sMPTunables.IH3_TRIP_SKIP_COST_MED
		ELIF fDistance > g_sMPTunables.IH3_TRIP_SKIP_DISTANCE_MED_MAX
			RETURN g_sMPTunables.IH3_TRIP_SKIP_COST_FAR
		ENDIF
	ELIF NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		IF fDistance < 4
			RETURN 4000
		ELIF fDistance >= 10
			RETURN 10000
		ENDIF
	ELSE
		IF fDistance < g_sMPTunables.IH2_TRIP_SKIP_DISTANCE_SHORT_MAX
			RETURN g_sMPTunables.IH2_TRIP_SKIP_COST_LOW
		ELIF fDistance >= g_sMPTunables.IH2_TRIP_SKIP_DISTANCE_MED_MIN AND fDistance <= g_sMPTunables.IH2_TRIP_SKIP_DISTANCE_MED_MAX
			RETURN g_sMPTunables.IH2_TRIP_SKIP_COST_MED
		ELIF fDistance > g_sMPTunables.IH2_TRIP_SKIP_DISTANCE_MED_MAX
			RETURN g_sMPTunables.IH2_TRIP_SKIP_COST_FAR
		ENDIF
	ENDIF
	
	RETURN 7000
ENDFUNC

//PURPOSE: Sets up the Server Data required
PROC SET_SERVER_TRIP_SKIP_DATA(TRIP_SKIP_SERVER_BD &TSServerBD, INT iTeam)	
	//General
	//TSServerBD.iTotalPlayers	= 2 //SET IN PRE LOOP PROC USING MISSION DATA
	
	//Car
	TSServerBD.vCarPos 		= g_fmmc_struct.vTripSkipPos[iTeam]
	TSServerBD.fCarHeading 	= g_fmmc_struct.fTripSkipHeading[iTeam]	
	
	//Cam
	TSServerBD.CamPos		= g_fmmc_struct.vTripSkipCamPos[iTeam]
	TSServerBD.CamRot		= g_fmmc_struct.vTripSkipCamRot[iTeam]
	TSServerBD.CamFOV		= g_fmmc_struct.fTripSkipCamFOV[iTeam]
	
	/*//TEMP FOR TESTING
	//Car
	TSServerBD.vCarPos 		= <<-832.4045, 1666.5229, 194.0061>>
	TSServerBD.fCarHeading 	= 263.9138
	//Cam
	TSServerBD.CamPos		= <<-837.838196,1648.758301,199.446121>>
	TSServerBD.CamRot		= <<-4.281904,-0.000000,-28.199291>>
	TSServerBD.CamFOV		= 50.0*/
	
	//Cost
	//TSServerBD.iStoredCost	= GET_TRIP_SKIP_COST(TSServerBD)
	
	PRINTLN(" TRIP SKIP - SERVER - SET_SERVER_TRIP_SKIP_DATA - iTeam = ", iTeam)
	PRINTLN(" TRIP SKIP - SERVER - SET_SERVER_TRIP_SKIP_DATA - vCarPos = ", TSServerBD.vCarPos)
	PRINTLN(" TRIP SKIP - SERVER - SET_SERVER_TRIP_SKIP_DATA - fCarHeading = ", TSServerBD.fCarHeading)
	PRINTLN(" TRIP SKIP - SERVER - SET_SERVER_TRIP_SKIP_DATA - CamPos = ", TSServerBD.CamPos)
	PRINTLN(" TRIP SKIP - SERVER - SET_SERVER_TRIP_SKIP_DATA - CamRot = ", TSServerBD.CamRot)
	PRINTLN(" TRIP SKIP - SERVER - SET_SERVER_TRIP_SKIP_DATA - CamFOV = ", TSServerBD.CamFOV)
	//PRINTLN(" TRIP SKIP - SERVER - SET_SERVER_TRIP_SKIP_DATA - iStoredCost = $", TSServerBD.iStoredCost)
	PRINTLN(" TRIP SKIP - SERVER - SET_SERVER_TRIP_SKIP_DATA - DONE")
ENDPROC

//PURPOSE: Returns TRUE once the Trip Skip Camera has finished Interping
FUNC BOOL HAS_TRIP_SKIP_CAM_FINISHED(TRIP_SKIP_LOCAL_DATA &TSLocalData)
	//RETURN NOT IS_CAM_INTERPOLATING(GET_RENDERING_CAM())

	IF HAS_NET_TIMER_EXPIRED(TSLocalData.InterpTimer, TRIP_SKIP_INTERP_TIME)
		PRINTLN(" TRIP SKIP - CLIENT - HAS_TRIP_SKIP_CAM_FINISHED - TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if the TripSkip has been enabled via the Creator
FUNC BOOL IS_TRIP_SKIP_AVAILABLE(TRIP_SKIP_SERVER_BD &TSServerBD)
	
	IF g_bCelebrationScreenIsActive
		RETURN FALSE
	ENDIF
	
	PRINTLN(" TRIP SKIP - CLIENT - IS_TRIP_SKIP_AVAILABLE: ", IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_TRIP_SKIP_AVAILABLE))
		
	RETURN IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_TRIP_SKIP_AVAILABLE)
ENDFUNC

//PURPOSE: Sets flags on the player 
PROC SET_PLAYER_FOR_TRIP_SKIP(BOOL bCleanup = FALSE)
	//SETUP
	IF bCleanup = FALSE
		//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_DONT_STOP_OTHER_CARS_AROUND_PLAYER)//, NSPC_CLEAR_TASKS|NSPC_FREEZE_POSITION)
		PRINTLN(" TRIP SKIP - CLIENT - SET_PLAYER_FOR_TRIP_SKIP - DONE")
		
		FLOAT fSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
		IF fSpeed > 1.0
			MAINTAIN_TRIP_SKIP_CAR_DRIVING(fSpeed+5)
		ENDIF
		
	//CLEANUP
	ELSE
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_TRIP_SKIP_IN_PROGRESS)
				IF g_bMissionEnding = FALSE
				AND g_bMissionOver = FALSE
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					PRINTLN(" TRIP SKIP - CLIENT - SET_PLAYER_FOR_TRIP_SKIP - CLEANUP - PLAYER CONTROL BACK ON")
				ELSE
					PRINTLN(" TRIP SKIP - CLIENT - SET_PLAYER_FOR_TRIP_SKIP - CLEANUP - MISSION ENDING OR OVER - LEAVE PLAYER CONTROL ALONE")
				ENDIF
			ELSE
				PRINTLN(" TRIP SKIP - CLIENT - SET_PLAYER_FOR_TRIP_SKIP - CLEANUP - TRIP SKIP DIDN'T TURN OFF PLAYER CONTROL")
			ENDIF
		ELSE
			PRINTLN(" TRIP SKIP - CLIENT - SET_PLAYER_FOR_TRIP_SKIP - CLEANUP - PLAYER CONTROL ALREADY ON")
		ENDIF
		//Clear Driving Task
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = WAITING_TO_START_TASK
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			PRINTLN(" TRIP SKIP - CLIENT - SET_PLAYER_FOR_TRIP_SKIP - CLEANUP - DRIVE WANDER CLEARED")
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_TRIP_SKIP(TRIP_SKIP_LOCAL_DATA &TSLocalData, TRIP_SKIP_PLAYER_BD &TSPlayerBD, BOOL &bHasTripSkipJustFinished)
	IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP", TSPlayerBD.iStoredCost)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP4", TSPlayerBD.iStoredCost)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP5", TSPlayerBD.iStoredCost)
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TS_HELP3")
		CLEAR_HELP()
	ENDIF
	
	IF IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_FADED_OUT)
	OR IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_TRIP_SKIP_IN_PROGRESS)
		IF NOT IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_INTERP_DONE)
			IF IS_SCREEN_FADED_OUT()
			OR IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_IN(TRIP_SKIP_FADE_IN_TIME)
				PRINTLN(" TRIP SKIP - CLIENT - CLEANUP_TRIP_SKIP - FADE IN ", TRIP_SKIP_FADE_IN_TIME)
			ENDIF
			g_sSctvTickerQueue.bActive = FALSE
			DONT_RENDER_IN_GAME_UI(FALSE)
			SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
			TOGGLE_RENDERPHASES(TRUE)
			PRINTLN(" TRIP SKIP - CLIENT - CLEANUP_TRIP_SKIP - TOGGLE_RENDERPHASES(TRUE)")
			CLEANUP_PULL_BACK_CAM()
			ANIMPOSTFX_STOP("MP_Celeb_Preload")
			ANIMPOSTFX_STOP("HeistTripSkipFade")
			PRINTLN(" TRIP SKIP - CLIENT - CLEANUP_TRIP_SKIP - ANIMPOSTFX_STOP - MP_Celeb_Preload - B")
			PRINTLN(" TRIP SKIP - CLIENT - CLEANUP_TRIP_SKIP - ANIMPOSTFX_STOP - HeistTripSkipFade - B")
		ENDIF
	ENDIF
	
	DESTROY_CAM(TSLocalData.Cam)
	
	SET_PLAYER_FOR_TRIP_SKIP(TRUE)
	
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_TRIP_SKIP_IN_PROGRESS)
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_TRIP_SKIP_IN_PROGRESS)
		PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - iABI_TRIP_SKIP_IN_PROGRESS - CLEARED")
	ENDIF
	
	viTripSkipPrevVeh = NULL
	
	Unpause_Objective_Text()
	bHasTripSkipJustFinished = TRUE
	SET_SCTV_TICKER_SCORE_ON()
	
	UNLOCK_TRIP_SKIP_CAR()
	PRINTLN(" TRIP SKIP - CLIENT - CLEANUP_TRIP_SKIP - DONE")
ENDPROC

PROC PRE_TRIP_SKIP_SERVER_LOOP(TRIP_SKIP_SERVER_BD &TSServerBD, INT iTotalPlayers)
	
	//Set Total Players
	IF TSServerBD.iTotalPlayers != iTotalPlayers
		TSServerBD.iTotalPlayers = iTotalPlayers
		PRINTLN(" TRIP SKIP - SERVER - PRE_TRIP_SKIP_SERVER_LOOP - iTotalPlayers = ", TSServerBD.iTotalPlayers)
	ENDIF
	
	IF TSServerBD.iStage > TSS_INIT
		SET_BIT(TSServerBD.iBitSet, TSS_BI_ALL_PLAYED_BEFORE)
		SET_BIT(TSServerBD.iBitSet, TSS_BI_NO_WANTED_LEVELS)
	ENDIF
	
	IF TSServerBD.iStage > TSS_WAIT
		//Reset Variables
		//TSServerBD.viCar = NULL
		CLEAR_BIT(TSServerBD.iBitSet, TSS_BI_GOT_VEHICLE)
		IF TSServerBD.iStage < TSS_WARP
			TSServerBD.fPlayersVoted = 0
		ENDIF
		IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_FADED_OUT)
			TSServerBD.iPlayersFadeOut = 0
		ENDIF
		//CLEAR_BIT(TSServerBD.iBitSet, TSS_BI_ALL_IN_CAR)
		TSServerBD.iPlayersInCar = 0
		IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_WARPED)
			TSServerBD.iPlayersWarped = 0
		ENDIF
		IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_INTERPED)
			TSServerBD.iPlayersInterped = 0
		ENDIF
	ENDIF
ENDPROC

PROC POST_TRIP_SKIP_SERVER_LOOP(TRIP_SKIP_SERVER_BD &TSServerBD, BOOL bTripSkipAvailable, BOOL bForcedTripSkip = FALSE)
	IF TSServerBD.iStage > TSS_WAIT 
	AND TSServerBD.iTotalPlayers > 0
		//Check Flags 
		IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_FADED_OUT)
			IF TSServerBD.fPlayersVotedStored != TSServerBD.fPlayersVoted
				TSServerBD.fPlayersVotedStored = TSServerBD.fPlayersVoted
				PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - fPlayersVotedStored = ", TSServerBD.fPlayersVotedStored)
			ENDIF
			PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - fPlayersVotedStored = ", TSServerBD.fPlayersVotedStored, "  iTotalPlayers = ", TSServerBD.iTotalPlayers)
			
			IF TSServerBD.fPlayersVoted > (TO_FLOAT(TSServerBD.iTotalPlayers)/2)
			AND IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_NO_WANTED_LEVELS)
			AND TSServerBD.iPlayersInCar >= TSServerBD.iTotalPlayers
				IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_VOTE_PASSED)
					SET_BIT(TSServerBD.iBitSet, TSS_BI_VOTE_PASSED)
					PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - TSS_BI_VOTE_PASSED - SET")
				ENDIF
			ELSE
				IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_VOTE_PASSED)
				AND NOT HAS_NET_TIMER_STARTED(TSServerBD.VotePassedDelay)
					CLEAR_BIT(TSServerBD.iBitSet, TSS_BI_VOTE_PASSED)
					PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - TSS_BI_VOTE_PASSED - CLEARED")
				ENDIF
			ENDIF			
		ENDIF		
		
		IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_FADED_OUT)
		AND IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_VOTE_PASSED)
			IF TSServerBD.iPlayersFadeOut >= TSServerBD.iTotalPlayers
				SET_BIT(TSServerBD.iBitSet, TSS_BI_ALL_FADED_OUT)
				PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - TSS_BI_ALL_FADED_OUT - SET")
			ENDIF
		ENDIF
		PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - iPlayersFadeOut = ", TSServerBD.iPlayersFadeOut, "  iTotalPlayers = ", TSServerBD.iTotalPlayers)
		
		IF TSServerBD.iPlayersInCar >= TSServerBD.iTotalPlayers
			IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_IN_CAR)
				SET_BIT(TSServerBD.iBitSet, TSS_BI_ALL_IN_CAR)
				PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - TSS_BI_ALL_IN_CAR - SET") 	
			ENDIF
		ELSE
			IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_IN_CAR)
				CLEAR_BIT(TSServerBD.iBitSet, TSS_BI_ALL_IN_CAR)
				PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - TSS_BI_ALL_IN_CAR - CLEARED") 
			ENDIF
		ENDIF
		PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - iPlayersInCar = ", TSServerBD.iPlayersInCar, "  iTotalPlayers = ", TSServerBD.iTotalPlayers)
		
		IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_WARPED)
			IF TSServerBD.iPlayersWarped >= TSServerBD.iTotalPlayers
				SET_BIT(TSServerBD.iBitSet, TSS_BI_ALL_WARPED)
				PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - TSS_BI_ALL_WARPED - SET")
			ENDIF			
		ENDIF
		PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - iPlayersWarped = ", TSServerBD.iPlayersWarped, "  iTotalPlayers = ", TSServerBD.iTotalPlayers)
		
		IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_INTERPED)
			IF TSServerBD.iPlayersInterped >= TSServerBD.iTotalPlayers
				SET_BIT(TSServerBD.iBitSet, TSS_BI_ALL_INTERPED)
				PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - TSS_BI_ALL_INTERPED - SET")
			ENDIF
		ENDIF
		PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - iPlayersInterped = ", TSServerBD.iPlayersInterped, "  iTotalPlayers = ", TSServerBD.iTotalPlayers)		
		PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - TSS_BI_VOTE_PASSED: ", IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_VOTE_PASSED), " TSS_BI_ALL_FADED_OUT: ", IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_FADED_OUT), " TSS_BI_ALL_IN_CAR: ", IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_IN_CAR), " TSS_BI_ALL_INTERPED: ", IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_INTERPED))
	ELSE
		IF TSServerBD.iTotalPlayers = 0
			PRINTLN(" TRIP SKIP - SERVER - POST_TRIP_SKIP_SERVER_LOOP - iTotalPlayers = 0 - Waiting for it to be set up")
		ENDIF
	ENDIF
		
	//Check if TripSkip should be Available
	IF bForcedTripSkip = FALSE
		IF TSServerBD.iStage <= TSS_VOTE
			IF IS_TRIP_SKIP_AVAILABLE(TSServerBD)
				IF (bTripSkipAvailable = FALSE
				OR NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_PLAYED_BEFORE))
				#IF IS_DEBUG_BUILD	AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_AllowTripSkipNoPlay") #ENDIF
				//OR ( IS_ENTITY_AT_COORD(PLAYER_PED_ID(), TSServerBD.vCarPos, <<300, 300, 300>>) AND TSServerBD.iStage <= TSS_VOTE )
					CLEAR_BIT(TSServerBD.iBitSet, TSS_BI_TRIP_SKIP_AVAILABLE)
					PRINTLN(" TRIP SKIP - MAINTAIN_TRIP_SKIP_SERVER_LOOP - TSS_BI_TRIP_SKIP_AVAILABLE - CLEARED")
				ENDIF
			ELSE
				IF bTripSkipAvailable = TRUE
				AND IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_PLAYED_BEFORE)
				//AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), TSServerBD.vCarPos, <<300, 300, 300>>)
					SET_BIT(TSServerBD.iBitSet, TSS_BI_TRIP_SKIP_AVAILABLE)
					PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP_SERVER_LOOP - TSS_BI_TRIP_SKIP_AVAILABLE - SET")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_AllowTripSkipNoPlay")
		IF bTripSkipAvailable
			IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_TRIP_SKIP_AVAILABLE)
				SET_BIT(TSServerBD.iBitSet, TSS_BI_TRIP_SKIP_AVAILABLE)
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP_SERVER_LOOP - TSS_BI_TRIP_SKIP_AVAILABLE - SET - USING COMMAND LINE")
			ENDIF
		ELIF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_TRIP_SKIP_AVAILABLE)
			PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP_SERVER_LOOP - TSS_BI_TRIP_SKIP_AVAILABLE - CLEAR - USING COMMAND LINE")
			CLEAR_BIT(TSServerBD.iBitSet, TSS_BI_TRIP_SKIP_AVAILABLE)
		ENDIF
	ELSE
		PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP_SERVER_LOOP - Commandline does not exist!!!")
	ENDIF
	#ENDIF
	
	//DEBUG
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
		PRINTLN(" TRIP SKIP - SERVER - TSServerBD.iStage = ", TSServerBD.iStage)
		PRINTLN(" TRIP SKIP - SERVER - bTripSkipAvailable = ", PICK_STRING(bTripSkipAvailable, "TRUE", "FALSE"))
		PRINTLN(" TRIP SKIP - SERVER - TSS_BI_ALL_PLAYED_BEFORE = ", PICK_STRING(IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_PLAYED_BEFORE), "TRUE", "FALSE"))
		PRINTLN(" TRIP SKIP - SERVER - iPlayersInCar = ", TSServerBD.iPlayersInCar)
		PRINTLN(" TRIP SKIP - SERVER - TSS_BI_NO_WANTED_LEVELS = ", PICK_STRING(IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_NO_WANTED_LEVELS), "TRUE", "FALSE"))
	ENDIF
	#ENDIF
ENDPROC


PROC MAINTAIN_TRIP_SKIP_SERVER_LOOP(TRIP_SKIP_SERVER_BD &TSServerBD, TRIP_SKIP_PLAYER_BD &TSPlayerBD, INT iparticipant, BOOL bHeistLeader, BOOL bForceTripSkip = FALSE)
	
	PLAYER_INDEX thisPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iparticipant))
	
	//Skip Spectator
	IF thisPlayer != INVALID_PLAYER_INDEX()
		IF IS_PLAYER_SPECTATING(thisPlayer)
			EXIT
		ENDIF
	ENDIF
	
	//Check if any player has not played before
	IF bForceTripSkip = FALSE
		IF TSServerBD.iStage > TSS_INIT
		#IF IS_DEBUG_BUILD 
		AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_AllowTripSkipNoPlay")
		#ENDIF
			IF NOT IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_PLAYED_BEFORE)
				CLEAR_BIT(TSServerBD.iBitSet, TSS_BI_ALL_PLAYED_BEFORE)
				IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_DO_NOT_ALL_PLAYED_HELP)
					SET_BIT(TSServerBD.iBitSet, TSS_BI_DO_NOT_ALL_PLAYED_HELP)
					PRINTLN(" TRIP SKIP - SERVER - TSS_BI_DO_NOT_ALL_PLAYED_HELP - SET - DUE TO PLAYER ", GET_PLAYER_NAME(thisPlayer))
				ENDIF
				PRINTLN(" TRIP SKIP - SERVER - TSS_BI_ALL_PLAYED_BEFORE = FALSE - DUE TO PLAYER ", GET_PLAYER_NAME(thisPlayer))
			ENDIF
			IF GET_PLAYER_WANTED_LEVEL(thisPlayer) > 0
				CLEAR_BIT(TSServerBD.iBitSet, TSS_BI_NO_WANTED_LEVELS)
				PRINTLN(" TRIP SKIP - SERVER - TSS_BI_NO_WANTED_LEVELS = FALSE - DUE TO PLAYER ", GET_PLAYER_NAME(thisPlayer))
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_TRIP_SKIP_AVAILABLE(TSServerBD)
	OR bForceTripSkip = TRUE
		IF TSServerBD.iStage > TSS_WAIT
			//All Voted Check
			
			PRINTLN(" TRIP SKIP - SERVER - Checking Num Of Players stages.")
			
			IF bForceTripSkip = FALSE
				//IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_VOTE_PASSED)
					IF IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_VOTED)
						IF bHeistLeader = TRUE
							TSServerBD.fPlayersVoted += 1.5 //Leader Vote
							PRINTLN(" TRIP SKIP - SERVER - Adding Player  ", GET_PLAYER_NAME(thisPlayer), " - Vote. (1.5)")
						ELSE
							TSServerBD.fPlayersVoted++
							PRINTLN(" TRIP SKIP - SERVER - Adding Player  ", GET_PLAYER_NAME(thisPlayer), " - Vote. (1.0)")
						ENDIF
					ENDIF
				//ENDIF
			ENDIF
			//All Faded Out Check
			IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_FADED_OUT)
				IF IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_FADED_OUT)
					TSServerBD.iPlayersFadeOut++
					PRINTLN(" TRIP SKIP - SERVER - THIS PLAYER HAS FADEOUT ", GET_PLAYER_NAME(thisPlayer), "   FADEOUT TOTAL = ", TSServerBD.iPlayersFadeOut)
				ENDIF
			ENDIF
			//All In Car Check
			//IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_IN_CAR)
			//IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			//AND NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
			//AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			//AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			//	IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iparticipant))), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			//	IF IS_PLAYER_IN_VALID_TRIP_SKIP_CAR(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant)))
				IF bForceTripSkip = FALSE
					IF IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_IN_CAR)
					AND IS_PED_SITTING_IN_ANY_VEHICLE(GET_PLAYER_PED(thisPlayer))
						IF TSServerBD.viCar = NULL
						OR NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_GOT_VEHICLE)
							TSServerBD.viCar = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(thisPlayer))	//PLAYER_PED_ID())
							SET_BIT(TSServerBD.iBitSet, TSS_BI_GOT_VEHICLE)
							PRINTLN(" TRIP SKIP - SERVER - STORING THIS PLAYERS CAR FOR CHECKS ", GET_PLAYER_NAME(thisPlayer), " - CarID = ", NATIVE_TO_INT(TSServerBD.viCar))
						ENDIF
						IF TSServerBD.viCar = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(thisPlayer))
							TSServerBD.iPlayersInCar++
							PRINTLN(" TRIP SKIP - SERVER - THIS PLAYER IS IN THE CAR ", GET_PLAYER_NAME(thisPlayer), "   IN CAR TOTAL = ", TSServerBD.iPlayersInCar)
						ENDIF
					ELSE
						PRINTLN(" TRIP SKIP - SERVER - THIS PLAYER IS NOT IN A VALID CAR ", GET_PLAYER_NAME(thisPlayer), "   IN CAR TOTAL = ", TSServerBD.iPlayersInCar)
					ENDIF
				ENDIF
			//ENDIF
			//All Warped Check
			IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_WARPED)
				IF IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_WARP_DONE)
					TSServerBD.iPlayersWarped++
					PRINTLN(" TRIP SKIP - SERVER - THIS PLAYER HAS WARPED ", GET_PLAYER_NAME(thisPlayer), "   WARPED TOTAL = ", TSServerBD.iPlayersWarped)
				ENDIF
			ENDIF
			//All Interped Check
			IF NOT IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_INTERPED)
				IF IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_INTERP_DONE)
					TSServerBD.iPlayersInterped++
					PRINTLN(" TRIP SKIP - SERVER - THIS PLAYER HAS INTERPED ", GET_PLAYER_NAME(thisPlayer), "   INTERPED TOTAL = ", TSServerBD.iPlayersInterped)
				ENDIF
			ENDIF
			
			//Cleanup if one player is using a Property
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thisPlayer)].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_USING_EXT)
			AND TSServerBD.iStage > TSS_VOTE
				TSServerBD.iStage = TSS_CLEANUP
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - PROPERTY_BROADCAST_BS_PLAYER_USING_EXT - TSServerBD.iStage -> TSS_CLEANUP - PLAYER ", GET_PLAYER_NAME(thisPlayer))
			ENDIF
			
			//Cleanup if one player has got too close to the Destination
			IF IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_TOO_CLOSE)
			AND TSServerBD.iStage = TSS_VOTE
				TSServerBD.iStage = TSS_CLEANUP
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSP_BI_TOO_CLOSE - TSServerBD.iStage -> TSS_CLEANUP - PLAYER ", GET_PLAYER_NAME(thisPlayer))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Displays Help Text if necessary 
PROC DO_TRIP_SKIP_UNAVAILABLE_HELP(TRIP_SKIP_SERVER_BD &TSServerBD, TRIP_SKIP_LOCAL_DATA &TSLocalData, BOOL bPlayedBefore)
	IF NOT IS_BIT_SET(TSLocalData.iBitSet, TSL_BI_NOT_ALL_PLAYED_HELP_SHOWN)
		IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_DO_NOT_ALL_PLAYED_HELP)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND bPlayedBefore
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT g_bAtmShowing
			AND NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_BROWSER_OPEN()
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			#IF IS_DEBUG_BUILD AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_AllowTripSkipNoPlay") #ENDIF
				PRINT_HELP_WITH_STRING("TS_HELP2", PICK_STRING(Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()), "TS_HELP2S", "TS_HELP2F"))	//Trip Skip is currently unavailable. All players must have previously completed this ~a~ in order to access Trip Skip.
				SET_BIT(TSLocalData.iBitSet, TSL_BI_NOT_ALL_PLAYED_HELP_SHOWN)
				PRINTLN(" TRIP SKIP - CLIENT - DO_TRIP_SKIP_UNAVAILABLE_HELP - DONE")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if the player is too close to the Trip SKip warp location or the Destination
FUNC BOOL IS_PLAYER_TOO_CLOSE_FOR_TRIP_SKIP(TRIP_SKIP_SERVER_BD &TSServerBD, TRIP_SKIP_PLAYER_BD &TSPlayerBD, VECTOR vDestination)
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), TSServerBD.vCarPos, <<300, 300, 300>>)
		IF NOT IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_TOO_CLOSE)
			SET_BIT(TSPlayerBD.iBitSet, TSP_BI_TOO_CLOSE)
			PRINTLN(" TRIP SKIP - IS_PLAYER_TOO_CLOSE_FOR_TRIP_SKIP - TSP_BI_TOO_CLOSE - SET - WARP LOCATION")
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vDestination)
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDestination, <<300, 300, 300>>)
			IF NOT IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_TOO_CLOSE)
				SET_BIT(TSPlayerBD.iBitSet, TSP_BI_TOO_CLOSE)
				PRINTLN(" TRIP SKIP - IS_PLAYER_TOO_CLOSE_FOR_TRIP_SKIP - TSP_BI_TOO_CLOSE - SET - DESTINATION")
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if it is okay for this plkayer to Trip Skip.
FUNC BOOL IS_SAFE_TO_TRIP_SKIP(TRIP_SKIP_SERVER_BD &TSServerBD, TRIP_SKIP_PLAYER_BD &TSPlayerBD, VECTOR vDestination)
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		PRINTLN(" TRIP SKIP - CLIENT - IS_SAFE_TO_TRIP_SKIP = FALSE - Net Play is not OK")
		RETURN FALSE
	ENDIF
	IF IS_PLAYER_TOO_CLOSE_FOR_TRIP_SKIP(TSServerBD, TSPlayerBD, vDestination)
		PRINTLN(" TRIP SKIP - CLIENT - IS_SAFE_TO_TRIP_SKIP = FALSE - Player is too close for TRIP SKIP")
		RETURN FALSE
	ENDIF
	IF IS_PLAYER_IN_MP_GARAGE(PLAYER_ID(), FALSE)
		PRINTLN(" TRIP SKIP - CLIENT - IS_SAFE_TO_TRIP_SKIP = FALSE - Inside MP Garage")
		RETURN FALSE
	ENDIF
	IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
		PRINTLN(" TRIP SKIP - CLIENT - IS_SAFE_TO_TRIP_SKIP = FALSE - In the Air")
		RETURN FALSE
	ENDIF
	IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		PRINTLN(" TRIP SKIP - CLIENT - IS_SAFE_TO_TRIP_SKIP = FALSE - Water")
		RETURN FALSE
	ENDIF
	/*IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 3
		RETURN FALSE
	ENDIF*/
	
	PRINTLN(" TRIP SKIP - CLIENT - IS_SAFE_TO_TRIP_SKIP = TRUE")
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_DISABLING_CONTROLS_WHILE_ACTIVE_TRIP_SKIP()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
ENDPROC


//PURPOSE: Controls any Trip Skips that are available
PROC MAINTAIN_TRIP_SKIP_CLIENT(TRIP_SKIP_SERVER_BD &TSServerBD, TRIP_SKIP_PLAYER_BD &TSPlayerBD, TRIP_SKIP_LOCAL_DATA &TSLocalData, VECTOR vDestination, BOOL bTripSkipRuleActive, BOOL bPlayedBefore, BOOL &bHasTripSkipJustFinished, BOOL bWholeCrew = FALSE)
	
	//Don't process if player is SCTV
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR IS_PLAYER_SPECTATING(PLAYER_ID())
		EXIT
	ENDIF
	
	//Don't process if player is Dead
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			#IF IS_DEBUG_BUILD PRINTLN(" TRIP SKIP - CLIENT - EXIT - IS_NET_PLAYER_OK = FALSE") #ENDIF
			EXIT
		ENDIF
		IF IS_PLAYER_RESPAWNING(PLAYER_ID())
			#IF IS_DEBUG_BUILD PRINTLN(" TRIP SKIP - CLIENT - EXIT - IS_PLAYER_RESPAWNING") #ENDIF
			EXIT
		ENDIF
	ENDIF
	
	//DEBUG
	PRINTLN("TRIP SKIP - CLIENT - TSPlayerBD.iStage = ", TSPlayerBD.iStage)
	IF (GET_FRAME_COUNT() % 10) = 0
		IF g_FMMC_STRUCT.bPlayedAlready
			PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - g_FMMC_STRUCT.bPlayedAlready = TRUE")
		ENDIF
		IF g_FMMC_Current_Mission_Cloud_Data.iPlayed > 0
			PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - g_FMMC_Current_Mission_Cloud_Data.iPlayed = ", g_FMMC_Current_Playlist_Cloud_Data.iPlayed)
		ENDIF
		IF g_FMMC_Current_Mission_Cloud_Data.bPlayedBefore
			PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - g_FMMC_Current_Mission_Cloud_Data.bPlayedBefore = TRUE")
		ENDIF
	ENDIF
	
	IF g_bMissionEnding 
	OR g_bMissionOver
		IF TSPlayerBD.iStage < TSS_CLEANUP
			TSPlayerBD.iStage = TSS_CLEANUP
			PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - g_bMissionEnding OR g_bMissionOver - TSPlayerBD.iStage = X -> TSS_CLEANUP")
		ENDIF
	ENDIF
	
	IF TSPlayerBD.iStage > TSS_VOTE
	AND TSPlayerBD.iStage < TSS_FINISHED
		DISABLE_CINEMATIC_VEHICLE_IDLE_MODE_THIS_UPDATE()
		PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - Disabling cinematic vehicle cam - DISABLE_CINEMATIC_VEHICLE_IDLE_MODE_THIS_UPDATE")
	ENDIF
	
	//CLIENT
	SWITCH TSPlayerBD.iStage
		CASE TSS_INIT
			TSPlayerBD.iStage = TSS_WAIT
			PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_INIT -> TSS_WAIT")
		BREAK
		
		CASE TSS_WAIT
			IF bTripSkipRuleActive = TRUE
				IF TSServerBD.iStage > TSS_WAIT
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						TSPlayerBD.iStoredCost = GET_TRIP_SKIP_COST(TSServerBD)
						PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStoredCost = ", TSPlayerBD.iStoredCost)
						TSPlayerBD.iStage = TSS_VOTE
						PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_WAIT -> TSS_VOTE")
					ENDIF
				ELSE
					DO_TRIP_SKIP_UNAVAILABLE_HELP(TSServerBD, TSLocalData, bPlayedBefore)
				ENDIF
			ENDIF
		BREAK
		
		CASE TSS_VOTE
			IF IS_TRIP_SKIP_AVAILABLE(TSServerBD)
				PRINTLN(" TRIP SKIP - CLIENT - IS_TRIP_SKIP_AVAILABLE = TRUE")
				
				IF IS_SAFE_TO_TRIP_SKIP(TSServerBD, TSPlayerBD, vDestination)
				AND bTripSkipRuleActive = TRUE
					DISPLAY_TRIP_SKIP_HELP(TSServerBD, TSPlayerBD, TSLocalData, TSPlayerBD.iStoredCost)
					
					IF IS_PLAYER_IN_VALID_TRIP_SKIP_CAR(PLAYER_ID())
						IF NOT IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_IN_CAR)
							SET_BIT(TSPlayerBD.iBitSet, TSP_BI_IN_CAR)
							PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSP_BI_IN_CAR - SET")
							
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - CACHING THIS VEHICLE FOR USE LATER (contingency)")
								viTripSkipPrevVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_IN_CAR)
							CLEAR_BIT(TSPlayerBD.iBitSet, TSP_BI_IN_CAR)
							PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSP_BI_IN_CAR - CLEARED")
						ENDIF
					ENDIF
					
					//See if Player jumped out Last Second
					IF TSServerBD.iStage >= TSS_FADE_OUT
					AND NOT IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_IN_CAR)
						SET_BIT(TSPlayerBD.iBitSet, TSP_BI_IN_CAR)
						PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSP_BI_IN_CAR - SET - FORCED DUE TO JUMP OUT")
					ENDIF
					
					IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_IN_CAR)
					OR TSServerBD.iStage >= TSS_FADE_OUT
						
						PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSS_BI_ALL_IN_CAR")
						
						TRACK_AND_DISPLAY_VOTE(TSServerBD, TSPlayerBD, TSLocalData)
						
						IF IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_VOTED)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
						ENDIF
						
						IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_VOTE_PASSED)
						AND (IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR TSServerBD.iStage >= TSS_FADE_OUT)
							IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP", TSPlayerBD.iStoredCost)
							OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP4", TSPlayerBD.iStoredCost)
							OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP5", TSPlayerBD.iStoredCost)
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TS_HELP3")
								CLEAR_HELP()
							ENDIF
							
							// We only need to do this if we haven't been forced through with a >= FADE_OUT check.
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								LOCK_TRIP_SKIP_CAR()
							ENDIF
							
							PROCESS_DISABLING_CONTROLS_WHILE_ACTIVE_TRIP_SKIP()
							
							IF TSServerBD.iStage > TSS_VOTE
								TSPlayerBD.iCost = TSPlayerBD.iStoredCost
								PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerData.iCost = TSPlayerBD.iStoredCost $", TSPlayerBD.iStoredCost)
								
								SET_PLAYER_FOR_TRIP_SKIP()
								
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									
								/*IF IS_SCREEN_FADED_IN()
									DO_SCREEN_FADE_OUT(TRIP_SKIP_FADE_OUT_TIME)
									PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - DO SCREEN FADE OUT - A")
								ENDIF*/
								
								//TOGGLE_RENDERPHASES(FALSE)
								//PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TOGGLE_RENDERPHASES(FALSE) - PAUSE")
								
								DO_PULL_BACK_CAM()
								
								//DO EFFECT + SOUND
								ANIMPOSTFX_PLAY("MP_Celeb_Preload", 0, TRUE)	//MP_Celeb_Preload_Fade
								PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")			//PLAY_SOUND_FRONTEND(-1, "Mission_Pass_Notify", "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS")
								PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - ANIMPOSTFX_PLAY = MP_Celeb_Preload - SOUND = Mission_Pass_Notify")
								
								g_sSctvTickerQueue.bActive = TRUE
								Clear_Any_Objective_Text()
								Pause_Objective_Text()
								CLEAR_HELP()
								IS_DPADDOWN_DISABLED_THIS_FRAME()
								DONT_RENDER_IN_GAME_UI(TRUE)
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
								HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
								HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
								HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
								THEFEED_HIDE_THIS_FRAME()
						
								TSPlayerBD.iStage = TSS_FADE_OUT
								PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_VOTE -> TSS_FADE_OUT")
								
								SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_TRIP_SKIP_IN_PROGRESS)
								PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - iABI_TRIP_SKIP_IN_PROGRESS - SET")
								
								CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10.0)
								
							ELSE
								PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSS_BI_VOTE_PASSED = TRUE - WAITING FOR VOTE PASSED DELAY")
							ENDIF
						ELSE 							
							PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSS_BI_VOTE_PASSED = FALSE")
						ENDIF
						
					ELSE 
						PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSS_BI_ALL_IN_CAR = FALSE")
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP", TSPlayerBD.iStoredCost)
					OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP4", TSPlayerBD.iStoredCost)
					OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP5", TSPlayerBD.iStoredCost)
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TS_HELP3")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP", TSPlayerBD.iStoredCost)
				OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP4", TSPlayerBD.iStoredCost)
				OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("TS_HELP5", TSPlayerBD.iStoredCost)
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TS_HELP3")
					CLEAR_HELP()
				ENDIF
				TSPlayerBD.iStage = TSS_CLEANUP
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_VOTE -> TSS_CLEANUP")
			ENDIF
		BREAK
		
		CASE TSS_FADE_OUT
			
			//TRACK_AND_DISPLAY_VOTE(TSServerBD, TSPlayerBD, TSLocalData)
			
			IF HAS_NET_TIMER_EXPIRED(TSLocalData.FreezeEffectTimer, TRIP_SKIP_DELAY_BEFORE_FADE)
				IF IS_SCREEN_FADED_IN()
					DO_SCREEN_FADE_OUT(TRIP_SKIP_FADE_OUT_TIME)
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - DO SCREEN FADE OUT - B")
					//TOGGLE_RENDERPHASES(FALSE)
					//PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TOGGLE_RENDERPHASES(FALSE) - PAUSE - B")
				ELIF IS_SCREEN_FADED_OUT()
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_DONT_STOP_OTHER_CARS_AROUND_PLAYER)//, NSPC_CLEAR_TASKS|NSPC_FREEZE_POSITION)
					DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
					IS_PLAYER_IN_TRIP_SKIP_CAR(bWholeCrew)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					//CLEAR_PED_TASKS(PLAYER_PED_ID())
					INVALIDATE_IDLE_CAM()
					SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
					Pause_Objective_Text()
					SET_SCTV_TICKER_SCORE_OFF()
					SET_BIT(TSPlayerBD.iBitSet, TSP_BI_FADED_OUT)
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSP_BI_FADED_OUT - SET")
					CLEANUP_PULL_BACK_CAM()
					ANIMPOSTFX_STOP("MP_Celeb_Preload")
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - ANIMPOSTFX_STOP - MP_Celeb_Preload - A")
					g_sJobHeistInfo.m_usedTripSkip++
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - m_usedTripSkip++ VALUE = ", g_sJobHeistInfo.m_usedTripSkip)
					TSPlayerBD.iStage = TSS_WARP
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_FADE_OUT -> TSS_WARP")
				ENDIF
				
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
			ENDIF
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
			THEFEED_HIDE_THIS_FRAME()
			IS_DPADDOWN_DISABLED_THIS_FRAME()
			
			PROCESS_DISABLING_CONTROLS_WHILE_ACTIVE_TRIP_SKIP()
		BREAK
		
		CASE TSS_WARP
			PROCESS_DISABLING_CONTROLS_WHILE_ACTIVE_TRIP_SKIP()
			DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
			
			IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_FADED_OUT)
				
				IF NOT IS_BIT_SET(TSLocalData.iBitSet, TSL_BI_CLEAR_TASKS_DONE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					SET_BIT(TSLocalData.iBitSet, TSL_BI_CLEAR_TASKS_DONE)
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSL_BI_CLEAR_TASKS_DONE - SET")
				ENDIF
				
				IF IS_PLAYER_IN_TRIP_SKIP_CAR(bWholeCrew)
					IF CAR_READY_FOR_TRIP_SKIP_WARP()
					OR IS_FAKE_MULTIPLAYER_MODE_SET()
																
						PRINTLN(" TRIP SKIP - CLIENT - Warping to TSServerBD.vCarPos.x: ", TSServerBD.vCarPos.x, " TSServerBD.vCarPos.y: ", TSServerBD.vCarPos.y, " TSServerBD.vCarPos.z: ", TSServerBD.vCarPos.z)
						
						BOOL bPassenger
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							VEHICLE_INDEX thisCar
							thisCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())							
							IF DOES_ENTITY_EXIST(thisCar)
								IF IS_VEHICLE_DRIVEABLE(thisCar)
								AND (PLAYER_PED_ID() != GET_PED_IN_VEHICLE_SEAT(thisCar, VS_DRIVER))
									bPassenger = TRUE
									PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - setting this client as (PASSENGER).")
								ENDIF
							ENDIF	
						ENDIF
						
						IF NOT bPassenger
							CLEAR_AREA(TSServerBD.vCarPos, 10, TRUE)
							
							PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - (DRIVER).")
							
							IF NET_WARP_TO_COORD(TSServerBD.vCarPos, TSServerBD.fCarHeading, TRUE, FALSE)
								SET_BIT(TSPlayerBD.iBitSet, TSP_BI_WARP_DONE)
								PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSP_BI_WARP_DONE - SET")
								TSPlayerBD.iStage = TSS_SETUP_CAM
								PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_WARP -> TSS_SETUP_CAM")
								
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								AND NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - Vehicle somehow broke during tripskip. Fixing it up.")
									SET_VEHICLE_FIXED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								ENDIF
							ENDIF
						ELSE
							// Passengers skip the Warp as the Driver is the only one who needs to do this.
							SET_BIT(TSPlayerBD.iBitSet, TSP_BI_WARP_DONE)
							PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSP_BI_WARP_DONE - SET (PASSENGER)")
							TSPlayerBD.iStage = TSS_SETUP_CAM
							PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_WARP -> TSS_SETUP_CAM (PASSENGER)")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_SCREEN_FADED_IN()
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSS_WARP - SCREEN IS SOMEHOW FADED IN - FADE BACK OUT")
				DO_SCREEN_FADE_OUT(500)
				TOGGLE_RENDERPHASES(FALSE)
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TOGGLE_RENDERPHASES(FALSE) - PAUSE - FADE BACK OUT")
			ENDIF
			
			
			BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
			END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
		BREAK
		
		/*CASE TSS_CAR
			IF PLAYER_IN_TRIP_SKIP_CAR(TSServerBD, TSPlayerBD)
				TSPlayerBD.iStage = TSS_SETUP_CAM
				PRINTLN(" TRIP SKIP - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_CAR -> TSS_SETUP_CAM")
			ENDIF
		BREAK*/
		
		CASE TSS_SETUP_CAM
			PROCESS_DISABLING_CONTROLS_WHILE_ACTIVE_TRIP_SKIP()
			DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
			
			PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - PROCESSING TSS_SETUP_CAM")
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
			THEFEED_HIDE_THIS_FRAME()
			IS_DPADDOWN_DISABLED_THIS_FRAME()
			DISABLE_FIRST_PERSON_CAMS()
			
			CLEAR_AREA(TSServerBD.vCarPos, 10, TRUE)
			
			BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
			END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
			
			IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_WARPED)
			AND IS_SCREEN_FADED_OUT()
				IF IS_PLAYER_IN_TRIP_SKIP_CAR(bWholeCrew)
					IF CAR_READY_FOR_TRIP_SKIP_FADE_IN()
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF CREATE_TRIP_SKIP_CAM(TSServerBD, TSLocalData)
								MAINTAIN_TRIP_SKIP_CAR_DRIVING()
								
								IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 5.0
									IF IS_SCREEN_FADED_OUT()
										DO_SCREEN_FADE_IN(TRIP_SKIP_FADE_IN_TIME)
										PRINTLN(" TRIP SKIP - CLIENT - CREATE_TRIP_SKIP_CAM - FADE IN ", TRIP_SKIP_FADE_IN_TIME)
									ENDIF
									
									BUSYSPINNER_OFF()	
									TSPlayerBD.iStage = TSS_INTERP_CAM
									PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_SETUP_CAM -> TSS_INTERP_CAM")
								ELSE
									PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - Waiting for Vehicle to speed up first.")
								ENDIF
							ENDIF
						ELSE
							PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - Not in a vehicle.")
							
							IF DOES_ENTITY_EXIST(viTripSkipPrevVeh)
								PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSS_SETUP_CAM - We are somehow out of our vehicle during the TripSkip.")
								IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
									IF IS_VEHICLE_SEAT_FREE(viTripSKipPrevVeh, VS_DRIVER)
										PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSS_SETUP_CAM - TASKING TO ENTER DRIVER SEAT")
										TASK_ENTER_VEHICLE(PLAYER_PED_ID(), viTripSkipPrevVeh, -1, VS_DRIVER, DEFAULT,  ECF_WARP_PED)
									ELSE
										PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSS_SETUP_CAM - TASKING TO ENTER PASSENGER ANY SEAT")
										TASK_ENTER_VEHICLE(PLAYER_PED_ID(), viTripSkipPrevVeh, -1, VS_ANY_PASSENGER, DEFAULT,  ECF_WARP_PED)
									ENDIF
								ELSE
									PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSS_SETUP_CAM - We are waiting to perform enter veh task.")
								ENDIF
							ELSE
								SCRIPT_ASSERT("[LM] TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - We don't have a reference to our trip skip vehicle. something pretty bad here happend.")								
							ENDIF
						ENDIF							
					ENDIF
				ENDIF
			ELSE				
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - IS_SCREEN_FADED_OUT: ", IS_SCREEN_FADED_OUT(), " TSS_BI_ALL_WARPED: ", IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_WARPED))
				DO_SCREEN_FADE_OUT(500)
			ENDIF
			
			IF IS_SCREEN_FADED_IN()
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSS_WARP - SCREEN IS SOMEHOW FADED IN - FADE BACK OUT")
				DO_SCREEN_FADE_OUT(500)
				TOGGLE_RENDERPHASES(FALSE)
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TOGGLE_RENDERPHASES(FALSE) - PAUSE - FADE BACK OUT")
			ENDIF
			
		BREAK
				
		CASE TSS_INTERP_CAM
			PROCESS_DISABLING_CONTROLS_WHILE_ACTIVE_TRIP_SKIP()			
			
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND DOES_ENTITY_EXIST(viTripSkipPrevVeh)
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - We are somehow out of our vehicle during the TripSkip.")
				IF IS_VEHICLE_SEAT_FREE(viTripSKipPrevVeh, VS_DRIVER)
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TASKING TO ENTER DRIVER SEAT")
					TASK_ENTER_VEHICLE(PLAYER_PED_ID(), viTripSkipPrevVeh, -1, VS_DRIVER, DEFAULT,  ECF_WARP_PED)
				ELSE
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TASKING TO ENTER PASSENGER ANY SEAT")
					TASK_ENTER_VEHICLE(PLAYER_PED_ID(), viTripSkipPrevVeh, -1, VS_ANY_PASSENGER, DEFAULT,  ECF_WARP_PED)
				ENDIF
				
				EXIT
			ENDIF
						
			//Check if Cam is finished
			IF NOT IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_INTERP_DONE)
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - PROCESSING TSS_INTERP_CAM, not done")
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
				THEFEED_HIDE_THIS_FRAME()
				IS_DPADDOWN_DISABLED_THIS_FRAME()
				DISABLE_FIRST_PERSON_CAMS()
								
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(TSLocalData.InterpTimer, 3000)
					IF g_bMissionEnding = FALSE
					AND g_bMissionOver = FALSE
						IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())				
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							PRINTLN(" TRIP SKIP - CLIENT - CALLING NET_SET_PLAYER_CONTROL EARLY FOR SMOOTH TRANSITION.")
						ENDIF
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = PERFORMING_TASK
						OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = WAITING_TO_START_TASK
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ELSE
					MAINTAIN_TRIP_SKIP_CAR_DRIVING()
				ENDIF	
								
				IF HAS_TRIP_SKIP_CAM_FINISHED(TSLocalData)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - CLEAR_PED_TAKS - CAM HAS FINISHED")
										
					SET_PLAYER_FOR_TRIP_SKIP(TRUE)
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - PLAYER CONTROL BACK ON ")
					
					ANIMPOSTFX_STOP("HeistTripSkipFade")
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - ANIMPOSTFX_STOP - HeistTripSkipFade - A")
					g_sSctvTickerQueue.bActive = FALSE
					DONT_RENDER_IN_GAME_UI(FALSE)
					Unpause_Objective_Text()
					SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
					bHasTripSkipJustFinished = TRUE
					SET_SCTV_TICKER_SCORE_ON()
					
					SET_BIT(TSPlayerBD.iBitSet, TSP_BI_INTERP_DONE)
					PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSP_BI_INTERP_DONE - SET")
				ELSE
					// Only stop cam control while we're interping.
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				ENDIF
			ENDIF
			
			//Move on once everyone is done
			IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_INTERPED)
				TSPlayerBD.iStage = TSS_CLEANUP
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_INTERP_CAM -> TSS_CLEANUP")
			ENDIF
		BREAK
		
		CASE TSS_CLEANUP
			g_missionFile_ExternalTracker.TripSkipped = TRUE
			CLEANUP_TRIP_SKIP(TSLocalData, TSPlayerBD, bHasTripSkipJustFinished)
			TSPlayerBD.iStage = TSS_FINISHED
			PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_CLEANUP -> TSS_FINISHED")
		BREAK
		
		CASE TSS_FINISHED
			//Do Nothing
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
				TRIP_SKIP_PLAYER_BD EmptyPlayer
				TRIP_SKIP_LOCAL_DATA EmptyLocal
				TSPlayerBD = EmptyPlayer
				TSLocalData = EmptyLocal
				PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - DEBUG RESET CLIENT DATA")
			ENDIF
			#ENDIF
		BREAK
	ENDSWITCH
	
	//Check if we should end due to Spectator being at End
	IF TSServerBD.iStage = TSS_FINISHED
	AND TSPlayerBD.iStage < TSS_CLEANUP
		TSPlayerBD.iStage = TSS_CLEANUP
		PRINTLN(" TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - SERVER = TSS_FINISHED - TSPlayerBD.iStage -> TSS_CLEANUP")
	ENDIF
ENDPROC
	
//PURPOSE: Controls any Trip Skips that are available
PROC MAINTAIN_TRIP_SKIP_SERVER(TRIP_SKIP_SERVER_BD &TSServerBD, BOOL bPlayedFirstDialogue, INT iTeam)
	
	//Don't process if player is SCTV
	/*IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF*/
	
	//DEBUG
	PRINTLN(" TRIP SKIP - SERVER - TSServerBD.iStage = ", TSServerBD.iStage)
	
	IF TSServerBD.iStage < TSS_CLEANUP
		IF g_bMissionEnding 
		OR g_bMissionOver
			TSServerBD.iStage = TSS_CLEANUP
			PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - g_bMissionEnding OR g_bMissionOver - TSServerBD.iStage = X -> TSS_CLEANUP")
		ENDIF
	ENDIF
	//SERVER
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SWITCH TSServerBD.iStage
			CASE TSS_INIT
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSS_INIT")
				IF bPlayedFirstDialogue = TRUE 								OR IS_FAKE_MULTIPLAYER_MODE_SET()
				#IF IS_DEBUG_BUILD
				OR GET_COMMANDLINE_PARAM_EXISTS("sc_AllowTripSkipNoPlay")
				#ENDIF
					IF HAS_NET_TIMER_EXPIRED(TSServerBD.InitDelay, 5000)	OR IS_FAKE_MULTIPLAYER_MODE_SET()
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL)
							SET_SERVER_TRIP_SKIP_DATA(TSServerBD, 0)
						ELSE
							SET_SERVER_TRIP_SKIP_DATA(TSServerBD, iTeam)
						ENDIF
						
						TSServerBD.iStage = TSS_WAIT
						PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_INIT -> TSS_WAIT")
					ENDIF
				ENDIF
			BREAK
		
			CASE TSS_WAIT
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSS_WAIT")
				IF IS_TRIP_SKIP_AVAILABLE(TSServerBD)
					TSServerBD.iStage = TSS_VOTE
					PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_WAIT -> TSS_VOTE")
				ENDIF
			BREAK
			
			CASE TSS_VOTE
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSS_VOTE")
				IF IS_TRIP_SKIP_AVAILABLE(TSServerBD)
					IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_VOTE_PASSED)
					OR HAS_NET_TIMER_STARTED(TSServerBD.VotePassedDelay)
						IF HAS_NET_TIMER_EXPIRED(TSServerBD.VotePassedDelay, TRIP_SKIP_VOTE_PASSED_DELAY)
							//TSServerBD.iCost = TSServerBD.iStoredCost
							TSServerBD.iStage = TSS_FADE_OUT
							RESET_NET_TIMER(TSServerBD.TimeOut)
							SET_BIT(TSServerBD.iBitSet, TSS_BI_VOTE_PASSED)
							PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_VOTE -> TSS_FADE_OUT")
						ENDIF
					ENDIF
				ELSE
					TSServerBD.iStage = TSS_CLEANUP
					PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_VOTE -> TSS_CLEANUP")
				ENDIF
			BREAK
			
			CASE TSS_FADE_OUT
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSS_FADE_OUT")
				IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_FADED_OUT)
					TSServerBD.iStage = TSS_WARP
					RESET_NET_TIMER(TSServerBD.TimeOut)
					PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_FADE_OUT -> TSS_WARP")
				ELSE
					IF HAS_NET_TIMER_EXPIRED(TSServerBD.TimeOut, 20000)
						TSServerBD.iStage = TSS_CLEANUP
						PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TIMEOUT -  TSServerBD.iStage = TSS_FADE_OUT -> TSS_CLEANUP")
					ENDIF
				ENDIF
			BREAK
			
			CASE TSS_WARP
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSS_WARP")
				IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_WARPED)
					TSServerBD.iStage = TSS_SETUP_CAM
					RESET_NET_TIMER(TSServerBD.TimeOut)
					PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_WARP -> TSS_SETUP_CAM")
				ELSE
					IF HAS_NET_TIMER_EXPIRED(TSServerBD.TimeOut, 30000)
						TSServerBD.iStage = TSS_CLEANUP
						PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TIMEOUT -  TSServerBD.iStage = TSS_WARP -> TSS_CLEANUP")
					ENDIF
				ENDIF
			BREAK
			
			/*CASE TSS_CAR
				IF CREATE_TRIP_SKIP_CAR(TSServerBD)
					TSServerBD.iStage = TSS_SETUP_CAM
					PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_CAR -> TSS_SETUP_CAM")
				ENDIF
			BREAK*/
			
			CASE TSS_SETUP_CAM
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSS_SETUP_CAM")
				TSServerBD.iStage = TSS_INTERP_CAM
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_SETUP_CAM -> TSS_INTERP_CAM")
			BREAK
					
			CASE TSS_INTERP_CAM
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSS_INTERP_CAM")
				IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_INTERPED)
					TSServerBD.iStage = TSS_CLEANUP
					PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_INTERP_CAM -> TSS_CLEANUP")
				ELSE
					IF HAS_NET_TIMER_EXPIRED(TSServerBD.TimeOut, 15000)
						TSServerBD.iStage = TSS_CLEANUP
						PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TIMEOUT -  TSServerBD.iStage = TSS_INTERP_CAM -> TSS_CLEANUP")
					ENDIF
				ENDIF
			BREAK
			
			CASE TSS_CLEANUP
				TSServerBD.iStage = TSS_FINISHED
				PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_CLEANUP -> TSS_FINISHED")
			BREAK
			
			CASE TSS_FINISHED
				//Do Nothing
				
				#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
					TRIP_SKIP_SERVER_BD EmptyServer
					TSServerBD = EmptyServer
					PRINTLN(" TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - DEBUG RESET SERVER DATA")
				ENDIF
				#ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

// NeilF: i moved this to net_include.sch
//FUNC BOOL SHOULD_MOVE_TO_NON_APARTMENT_INTRO()
//	//if creator option checked
//	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciNEW_INTRO) AND NOT g_TransitionSessionNonResetVars.bHasQuickRestarted
//		PRINTLN("[MMacK] SHOULD_MOVE_TO_NON_APARTMENT_INTRO = TRUE")
//		RETURN TRUE
//	ELSE
//		PRINTLN("[MMacK] SHOULD_MOVE_TO_NON_APARTMENT_INTRO = FALSE")
//		RETURN FALSE
//	ENDIF
//ENDFUNC


////////////////////////////////////////////////////////////////////////////////////
///    							FORCED TRIP SKIP
////////////////////////////////////////////////////////////////////////////////////

//PURPOSE: Returns TRUE if the FOrce Trip Skip has Data setup
FUNC BOOL IS_FORCED_TRIP_SKIP_AVAILABLE()
	RETURN FALSE //this has been removed as per requests. 
	
	IF IS_VECTOR_ZERO(g_fmmc_struct.vAutoTripSkipCamPos)
		RETURN FALSE
	ENDIF
	
	//PRINTLN("FORCED TRIP SKIP - IS_FORCED_TRIP_SKIP_AVAILABLE - g_fmmc_struct.vAutoTripSkipCamPos = ", g_fmmc_struct.vAutoTripSkipCamPos)
	RETURN TRUE
ENDFUNC

//PURPOSE: Sets up the Server Data required
PROC SET_SERVER_FORCED_TRIP_SKIP_DATA(TRIP_SKIP_SERVER_BD &TSServerBD)	
	//Car
	TSServerBD.iCar			= g_fmmc_struct.iAutoTripSkipVehicle
	TSServerBD.vCarPos 		= g_fmmc_struct.vAutoTripSkipPos
	TSServerBD.fCarHeading 	= g_fmmc_struct.fAutoTripSkipHeading
	
	//Cam
	TSServerBD.CamPos		= g_fmmc_struct.vAutoTripSkipCamPos
	TSServerBD.CamRot		= g_fmmc_struct.vAutoTripSkipCamRot
	TSServerBD.CamFOV		= g_fmmc_struct.fAutoTripSkipCamFOV
	
	//TEMP FOR TESTING
	//Car
	/*TSServerBD.iCar 		= 0
	TSServerBD.vCarPos 		= <<-832.4045, 1666.5229, 194.0061>>
	TSServerBD.fCarHeading 	= 263.9138
	//Cam
	TSServerBD.CamPos		= <<-837.838196,1648.758301,199.446121>>
	TSServerBD.CamRot		= <<-4.281904,-0.000000,-28.199291>>
	TSServerBD.CamFOV		= 50.0*/
	
	PRINTLN(" FORCED TRIP SKIP - SERVER - SET_SERVER_FORCED_TRIP_SKIP_DATA - iCar = ", TSServerBD.iCar)
	PRINTLN(" FORCED TRIP SKIP - SERVER - SET_SERVER_FORCED_TRIP_SKIP_DATA - vCarPos = ", TSServerBD.vCarPos)
	PRINTLN(" FORCED TRIP SKIP - SERVER - SET_SERVER_FORCED_TRIP_SKIP_DATA - fCarHeading = ", TSServerBD.fCarHeading)
	PRINTLN(" FORCED TRIP SKIP - SERVER - SET_SERVER_FORCED_TRIP_SKIP_DATA - CamPos = ", TSServerBD.CamPos)
	PRINTLN(" FORCED TRIP SKIP - SERVER - SET_SERVER_FORCED_TRIP_SKIP_DATA - CamRot = ", TSServerBD.CamRot)
	PRINTLN(" FORCED TRIP SKIP - SERVER - SET_SERVER_FORCED_TRIP_SKIP_DATA - CamFOV = ", TSServerBD.CamFOV)
	PRINTLN(" FORCED TRIP SKIP - SERVER - SET_SERVER_FORCED_TRIP_SKIP_DATA - DONE")
ENDPROC

//PURPOSE: Controls any Trip Skips that are available
PROC MAINTAIN_FORCED_TRIP_SKIP_CLIENT(TRIP_SKIP_SERVER_BD &TSServerBD, TRIP_SKIP_PLAYER_BD &TSPlayerBD, TRIP_SKIP_LOCAL_DATA &TSLocalData, BOOL &bHasTripSkipJustFinished)
	
	//Don't process if player is SCTV
	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF
	
	//DEBUG
	#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9) PRINTLN(" FORCED TRIP SKIP - CLIENT - TSPlayerBD.iStage = ", TSPlayerBD.iStage) ENDIF #ENDIF
		
	//CLIENT
	SWITCH TSPlayerBD.iStage
		CASE TSS_INIT
			IF TSServerBD.iStage > TSS_WAIT
				TSPlayerBD.iStage = TSS_FADE_OUT
				PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_INIT -> TSS_FADE_OUT")
			ENDIF
		BREAK
				
		CASE TSS_FADE_OUT
			IF IS_SCREEN_FADED_IN()
				DO_SCREEN_FADE_OUT(TRIP_SKIP_FADE_OUT_TIME)
				PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - DO SCREEN FADE OUT")
			ELIF IS_SCREEN_FADED_OUT()
			
				// Clean up all the corona FX / Screen state (2212848)
				CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,FMMC_TYPE_MISSION,TRUE)

				ANIMPOSTFX_STOP("MP_job_load")
				CLEANUP_ALL_CORONA_FX(FALSE)
				SET_SKYFREEZE_CLEAR(TRUE)
							
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				Pause_Objective_Text()
				SET_SCTV_TICKER_SCORE_OFF()
				SET_BIT(TSPlayerBD.iBitSet, TSP_BI_FADED_OUT)
				PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSP_BI_FADED_OUT - SET")
				TSPlayerBD.iStage = TSS_WARP
				PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_FADE_OUT -> TSS_WARP")
			ENDIF
		BREAK
		
		CASE TSS_WARP
			IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_FADED_OUT)
				IF NET_WARP_TO_COORD(TSServerBD.vCarPos+<<0, -3, 2>>, TSServerBD.fCarHeading, FALSE, FALSE)
					IF IS_PLAYER_IS_IN_FORCED_TRIP_SKIP_CAR(TSServerBD)
						SET_BIT(TSPlayerBD.iBitSet, TSP_BI_WARP_DONE)
						PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSP_BI_WARP_DONE - SET")
						TSPlayerBD.iStage = TSS_SETUP_CAM
						PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_WARP -> TSS_SETUP_CAM")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE TSS_SETUP_CAM
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_WARPED)
				IF IS_PLAYER_IS_IN_FORCED_TRIP_SKIP_CAR(TSServerBD)
					IF CREATE_TRIP_SKIP_CAM(TSServerBD, TSLocalData)
						MAINTAIN_TRIP_SKIP_CAR_DRIVING()
						TSPlayerBD.iStage = TSS_INTERP_CAM
						PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_SETUP_CAM -> TSS_INTERP_CAM")
					ENDIF
				ENDIF
			ENDIF
		BREAK
				
		CASE TSS_INTERP_CAM
			
			//Check if Cam is finished
			IF NOT IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_INTERP_DONE)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				MAINTAIN_TRIP_SKIP_CAR_DRIVING()
				
				IF HAS_TRIP_SKIP_CAM_FINISHED(TSLocalData)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - CLEAR_PED_TAKS - CAM HAS FINISHED")
					
					SET_PLAYER_FOR_TRIP_SKIP(TRUE)
					PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - PLAYER CONTROL BACK ON ")
					
					Unpause_Objective_Text()
					bHasTripSkipJustFinished = TRUE
					SET_SCTV_TICKER_SCORE_ON()
					
					SET_BIT(TSPlayerBD.iBitSet, TSP_BI_INTERP_DONE)
					PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSP_BI_INTERP_DONE - SET")
				ENDIF
			ENDIF
			
			//Move on once everyone is done
			IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_INTERPED)
			AND IS_BIT_SET(TSPlayerBD.iBitSet, TSP_BI_INTERP_DONE)
				TSPlayerBD.iStage = TSS_CLEANUP
				PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_INTERP_CAM -> TSS_CLEANUP")
			ENDIF
		BREAK
		
		CASE TSS_CLEANUP
			CLEANUP_TRIP_SKIP(TSLocalData, TSPlayerBD, bHasTripSkipJustFinished)
			TSPlayerBD.iStage = TSS_FINISHED
			PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - TSPlayerBD.iStage = TSS_CLEANUP -> TSS_FINISHED")
		BREAK
		
		CASE TSS_FINISHED
			//Do Nothing
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
				TRIP_SKIP_PLAYER_BD EmptyPlayer
				TRIP_SKIP_LOCAL_DATA EmptyLocal
				TSPlayerBD = EmptyPlayer
				TSLocalData = EmptyLocal
				PRINTLN(" FORCED TRIP SKIP - CLIENT - MAINTAIN_TRIP_SKIP - DEBUG RESET CLIENT DATA")
			ENDIF
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC
	
//PURPOSE: Controls the Forced Trip Skip
PROC MAINTAIN_FORCED_TRIP_SKIP_SERVER(TRIP_SKIP_SERVER_BD &TSServerBD)
	
	//Don't process if player is SCTV
	IF IS_PLAYER_SCTV(PLAYER_ID())
		EXIT
	ENDIF
	
	//DEBUG
	#IF IS_DEBUG_BUILD IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9) PRINTLN(" FORCED TRIP SKIP - SERVER - TSServerBD.iStage = ", TSServerBD.iStage) ENDIF #ENDIF
	
	//SERVER
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SWITCH TSServerBD.iStage
			CASE TSS_INIT
				SET_SERVER_FORCED_TRIP_SKIP_DATA(TSServerBD)
				TSServerBD.iStage = TSS_FADE_OUT
				PRINTLN(" FORCED TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_INIT -> TSS_FADE_OUT")
			BREAK
						
			CASE TSS_FADE_OUT
				IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_FADED_OUT)
					TSServerBD.iStage = TSS_WARP
					PRINTLN(" FORCED TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_FADE_OUT -> TSS_WARP")
				ENDIF
			BREAK
			
			CASE TSS_WARP
				IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_WARPED)
					TSServerBD.iStage = TSS_SETUP_CAM
					PRINTLN(" FORCED TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_WARP -> TSS_SETUP_CAM")
				ENDIF
			BREAK
			
			CASE TSS_SETUP_CAM
				TSServerBD.iStage = TSS_INTERP_CAM
				PRINTLN(" FORCED TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_SETUP_CAM -> TSS_INTERP_CAM")
			BREAK
					
			CASE TSS_INTERP_CAM
				IF IS_BIT_SET(TSServerBD.iBitSet, TSS_BI_ALL_INTERPED)
					TSServerBD.iStage = TSS_CLEANUP
					PRINTLN(" FORCED TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_INTERP_CAM -> TSS_CLEANUP")
				ENDIF
			BREAK
			
			CASE TSS_CLEANUP
				TSServerBD.iStage = TSS_FINISHED
				PRINTLN(" FORCED TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - TSServerBD.iStage = TSS_CLEANUP -> TSS_FINISHED")
			BREAK
			
			CASE TSS_FINISHED
				//Do Nothing
				
				#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
					TRIP_SKIP_SERVER_BD EmptyServer
					TSServerBD = EmptyServer
					PRINTLN(" FORCED TRIP SKIP - SERVER - MAINTAIN_TRIP_SKIP - DEBUG RESET SERVER DATA")
				ENDIF
				#ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


////////////////////////////////////////////////////////////////////////////////////
///    							QUICK EQUIP MASK
////////////////////////////////////////////////////////////////////////////////////
//USES - INT iLocalQuickMaskBitSet
CONST_INT LBS_MASK_QUICK_EQUIPPED			0
CONST_INT LBS_MASK_QUICK_EQUIP_HELP_2		1
CONST_INT LBS_MASK_EXTEND_CONTROL_BLOCK		2
CONST_INT LBS_MASK_QUICK_EQUIP_HELP_1		3
CONST_INT LBS_MASK_QUICK_REMOVED			4
CONST_INT LBS_MASK_QUICK_REMOVE_HELP		5
CONST_INT LBS_MASK_QUICK_EQUIP_AVAILABLE	6
CONST_INT LBS_MASK_QUICK_UNEQUIP_AVAILABLE	7

CONST_INT ciMASK_FAILSAFE_TIMEOUT 	5000

FUNC BOOL SHOULD_CURRENT_BERD_AFFECT_MASKS()
	IF IS_THIS_LEGACY_FMMC_CONTENT()
		RETURN TRUE
	ENDIF
	
	INT iItemHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(COMP_TYPE_BERD), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD))
	IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iItemHash, DLC_RESTRICTION_TAG_EAR_PIECE, ENUM_TO_INT(SHOP_PED_COMPONENT))
	OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iItemHash, DLC_RESTRICTION_TAG_EAR_PIECE, ENUM_TO_INT(SHOP_PED_OUTFIT))
	OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iItemHash, DLC_RESTRICTION_TAG_EAR_PIECE, ENUM_TO_INT(SHOP_PED_PROP))
		PRINTLN("MASK - SHOULD_CURRENT_BERD_AFFECT_MASKS - The player's current BERD (hash ", iItemHash, ") is an earpiece")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CORRECT_MASK_STATE_FOR_QUICK_EQUIP_MASK(BOOL bUseRebreather, BOOL bRemoveMask)
	//QUICK EQUIP
	IF bRemoveMask = FALSE
		//NOT WEARING MASK CHECK
		IF IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0))
		OR NOT SHOULD_CURRENT_BERD_AFFECT_MASKS()
			RETURN TRUE
		ENDIF
		
		//NOT WEARING REBREATHER WHEN FLAG IS SET
		IF bUseRebreather = TRUE 
			IF NOT IS_MP_HEIST_GEAR_EQUIPPED(PLAYER_PED_ID(), HEIST_GEAR_REBREATHER)
				RETURN TRUE
			ENDIF
		ENDIF
		
	//QUICK REMOVE
	ELSE
		IF IS_PED_WEARING_A_MASK(PLAYER_PED_ID())
		OR NOT IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0))
		OR IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CORRECT_MASK_STATE_FOR_QUICK_EQUIP_HELMET(BOOL bUseRebreather, BOOL bRemoveMask)
	//QUICK EQUIP
	IF bRemoveMask = FALSE
		//IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
		IF NOT IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
			//NOT WEARING A HELMET
			//PRINTLN(" MASK - CORRECT_MASK_STATE_FOR_QUICK_EQUIP_HELMET = TRUE - A")
			RETURN TRUE
		ELSE
			//WEARING A HELMET
			IF NOT IS_PED_WEARING_A_FULL_FACE_HELMET(PLAYER_PED_ID())
				//PRINTLN(" MASK - CORRECT_MASK_STATE_FOR_QUICK_EQUIP_HELMET = TRUE - B")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF bUseRebreather = TRUE
			//PRINTLN(" MASK - CORRECT_MASK_STATE_FOR_QUICK_EQUIP_HELMET = TRUE - C")
			RETURN TRUE
		ENDIF
		
	//QUICK REMOVE
	ELSE
		//HELMET CHECK DONE WITH MASK CHECK
		//PRINTLN(" MASK - CORRECT_MASK_STATE_FOR_QUICK_EQUIP_HELMET = TRUE - D")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls the option to Quick Equip a Mask
PROC MAINTAIN_QUICK_EQUIP_MASK(MASK_ANIM_DATA& maskAnimData, BOOL& bAnimationPlaying, INT iTeam, BOOL bAvailable, INT &iDoneBitSet, BOOL bUseRebreather = FALSE, BOOL bRemoveMask = FALSE, BOOL bBlockRebreatherHelp = FALSE)

	BOOL bHasFailsafeTimerExpired = FALSE
	
	IF bAvailable
		
		// Load in mask anims
		REQUEST_MASK_ANIMS(maskAnimData)
		
		// Removed preload on mask as need headblend and hair preloaded also- currently with code.
//		IF NOT bPreloadedMaskOutfitData
//			IF NOT IS_BIT_SET(iDoneBitSet, LBS_MASK_QUICK_EQUIPPED)
//				PRINTLN("[RBJ] - MASK - net_mission_controler_public - MAINTAIN_QUICK_EQUIP_MASK - Preloading Heist mask for equip.")
//				sMPOutfitApplyData.eApplyStage 		= AOS_SET         
//			    sMPOutfitApplyData.pedID        	= PLAYER_PED_ID()
//			    sMPOutfitApplyData.eMask        	= INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_LOCAL_DEFAULT_MASK(iTeam))	
//				
//				bPreloadedMaskOutfitData 			= PRELOAD_MP_HEIST_MASK_DATA(sMPOutfitApplyData)
//				IF bPreloadedMaskOutfitData
//					PRINTLN("[RBJ] - MASK - net_mission_controler_public - MAINTAIN_QUICK_EQUIP_MASK - MP outfit for mask HAS been preloaded.")
//				ELSE
//					PRINTLN("[RBJ] - MASK - net_mission_controler_public - MAINTAIN_QUICK_EQUIP_MASK - MP outfit for mask has NOT been preloaded this frame.")
//				ENDIF
//			ENDIF
//		ENDIF
	
		IF (bRemoveMask = FALSE AND NOT IS_BIT_SET(iDoneBitSet, LBS_MASK_QUICK_EQUIPPED))
		OR (bRemoveMask = TRUE AND NOT IS_BIT_SET(iDoneBitSet, LBS_MASK_QUICK_REMOVED))
		
			IF READY_TO_PUT_MASK_ON(maskAnimData)
			AND bRemoveMask = FALSE
			
				//PRINTLN("[RBJ] - MASK - net_mission_controler_public - MAINTAIN_QUICK_EQUIP_MASK - DISABLE_CONTROL_ACTION - READY_TO_PUT_MASK_ON.")
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				
				IF NOT HAS_NET_TIMER_STARTED(maskAnimData.stMaskFailsafe)
					START_NET_TIMER(maskAnimData.stMaskFailsafe)
				ELSE
				
					bHasFailsafeTimerExpired = HAS_NET_TIMER_EXPIRED(maskAnimData.stMaskFailsafe, ciMASK_FAILSAFE_TIMEOUT)
					
					IF HAS_MASK_ANIM_STARTED(maskAnimData)
					OR DONT_DO_MASK_ANIM(maskAnimData)
					OR bHasFailsafeTimerExpired
					
						IF NOT DONT_DO_MASK_ANIM(maskAnimData)
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								CDEBUG3LN(DEBUG_MISSION, "[RBJ] - MASK - net_mission_controler_public - MAINTAIN_QUICK_EQUIP_MASK - Player in car so SET_PED_CAN_PLAY_AMBIENT_IDLES(PLAYER_PED_ID(), TRUE, TRUE).")
								SET_PED_CAN_PLAY_AMBIENT_IDLES(PLAYER_PED_ID(), TRUE, TRUE)
							ENDIF
						ENDIF
					
						IF HAS_MASK_EVENT_FIRED()
						OR DONT_DO_MASK_ANIM(maskAnimData)
						OR bHasFailsafeTimerExpired
						
							#IF IS_DEBUG_BUILD
								IF bHasFailsafeTimerExpired
									PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - bHasFailsafeTimerExpired")
									SCRIPT_ASSERT("MAINTAIN_QUICK_EQUIP_MASK, bHasFailsafeTimerExpired ")
								ENDIF
							#ENDIF
							
							IF bUseRebreather = FALSE
								PRINTLN("[RBJ] - MASK - net_mission_controler_public - MAINTAIN_QUICK_EQUIP_MASK - SET_PED_MP_OUTFIT.")
								MP_OUTFITS_APPLY_DATA   		sMPOutfitApplyData
								sMPOutfitApplyData.eApplyStage 	= AOS_SET         
							    sMPOutfitApplyData.pedID        = PLAYER_PED_ID()
							    sMPOutfitApplyData.eMask        = INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_LOCAL_DEFAULT_MASK(iTeam))
								CLEAR_PED_STORED_HAT_PROP(PLAYER_PED_ID())
								SET_PED_MP_OUTFIT(sMPOutfitApplyData, TRUE, TRUE, FALSE)
							ELSE
								PRINTLN("MASK - net_mission_controler_public - MAINTAIN_QUICK_EQUIP_MASK - SET_MP_HEIST_GEAR - HEIST_GEAR_REBREATHER")
								SET_MP_HEIST_GEAR(PLAYER_PED_ID(), HEIST_GEAR_REBREATHER)
							ENDIF
							
							FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
							//Start_Export_Player_Headshot_Timer() removed for bug 2230280
							BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
							
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK1")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK2")
								CLEAR_HELP()
							ENDIF
							
							IF NOT DONT_DO_MASK_ANIM(maskAnimData)
								bAnimationPlaying = TRUE
							ENDIF
							
							SET_BIT(iDoneBitSet, LBS_MASK_QUICK_EQUIPPED)
							PRINTLN("[RBJ] - MASK - net_mission_controler_public - MAINTAIN_QUICK_EQUIP_MASK - LBS_MASK_QUICK_EQUIPPED.")
				            PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - MASK HAS BEEN EQUIPPED - LBS_MASK_QUICK_EQUIPPED - SET")
							PRINTLN(" MASK - [MASK_ANIM] DONE ")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//IF IS_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0))	//NOT WEARING MASK CHECK
				//OR (bUseRebreather = TRUE AND NOT IS_MP_HEIST_GEAR_EQUIPPED(PLAYER_PED_ID(), HEIST_GEAR_REBREATHER))	//NOT WEARING REBREATHER WHEN FLAG IS SET
				IF CORRECT_MASK_STATE_FOR_QUICK_EQUIP_MASK(bUseRebreather, bRemoveMask)
					//IF (NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
					//  AND NOT IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD))))
					//OR bUseRebreather = TRUE																		//SKIP HELMET CHECK IF REBREATHER FLAG IS SET
					IF CORRECT_MASK_STATE_FOR_QUICK_EQUIP_HELMET(bUseRebreather, bRemoveMask)
						IF NOT IS_CUSTOM_MENU_ON_SCREEN()
						AND NOT IS_PAUSE_MENU_ACTIVE()
						AND NOT IS_BROWSER_OPEN()
						AND NOT g_bAtmShowing
						AND NOT GET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_PuttingOnHelmet)
						AND NOT IS_PED_TAKING_OFF_HELMET(PLAYER_PED_ID())
						AND NOT IS_PHONE_ONSCREEN()
						AND NOT IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
							//Help Text
							INT iStatInt
							iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT7)
							IF (NOT IS_BIT_SET(iStatInt, biNmh7_QuickEquipMask) OR (g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_Tutorial_Car))
							AND NOT IS_BIT_SET(iDoneBitSet, LBS_MASK_QUICK_EQUIP_HELP_1)
							AND bUseRebreather = FALSE
							AND bRemoveMask = FALSE
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP("HQE_MASK1", 15000)	//During a Heist, equip a mask to conceal your identity or to intimidate your enemies. Press ~INPUT_SCRIPT_PAD_LEFT~ to equip a mask.
									
									SET_BIT(iStatInt, biNmh7_QuickEquipMask)
									SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT7, iStatInt)
									PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - TUTORIAL HELP TEXT DONE - biNmh7_QuickEquipMask - SET")
									
									SET_BIT(iDoneBitSet, LBS_MASK_QUICK_EQUIP_HELP_1)
									PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - HELP TEXT DONE - LBS_MASK_QUICK_EQUIP_HELP_1 - SET")
								ENDIF
							ELIF (bRemoveMask = FALSE AND NOT IS_BIT_SET(iDoneBitSet, LBS_MASK_QUICK_EQUIP_HELP_2))
							OR 	 (bRemoveMask = TRUE AND NOT IS_BIT_SET(iDoneBitSet, LBS_MASK_QUICK_REMOVE_HELP))
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									IF bRemoveMask = TRUE
										PRINT_HELP("HQE_MASK3", 15000)	//Press ~INPUT_SCRIPT_PAD_LEFT~ to remove your mask.
										SET_BIT(iDoneBitSet, LBS_MASK_QUICK_REMOVE_HELP)
										PRINTLN(" MASK - REMOVE - MAINTAIN_QUICK_EQUIP_MASK - REMOVE MASK HELP TEXT DONE - LBS_MASK_QUICK_REMOVE_HELP - SET")
									ELIF bUseRebreather = FALSE
										PRINT_HELP("HQE_MASK", 15000)	//Press ~INPUT_SCRIPT_PAD_LEFT~ to equip a mask.
										SET_BIT(iDoneBitSet, LBS_MASK_QUICK_EQUIP_HELP_2)
										PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - EQUIP MASK HELP TEXT DONE - LBS_MASK_QUICK_EQUIP_HELP_2 - SET")
									ELSE
										IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS) 
										OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
										AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
										AND NOT bBlockRebreatherHelp //2020 Controller does this itself
											PRINT_HELP("HQE_MASK2", 15000)	//Press ~INPUT_SCRIPT_PAD_LEFT~ to equip the rebreather.
											SET_BIT(iDoneBitSet, LBS_MASK_QUICK_EQUIP_HELP_2)
											PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - REBREATHER HELP TEXT DONE - LBS_MASK_QUICK_EQUIP_HELP_2 - SET")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
							//Input Check	
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)	
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_THROW_GRENADE)
							
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
							
								IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_DETONATE)
								OR g_bAutoMaskEquip
									IF bRemoveMask = FALSE
										//PRINTLN("[RBJ] - MASK - net_mission_controler_public - MAINTAIN_QUICK_EQUIP_MASK - DISABLE_CONTROL_ACTION - NOT READY_TO_PUT_MASK_ON.")
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
										
										// READY_TO_PUT_MASK_ON
										SET_BIT(maskAnimData.iMaskBitSet, ciMASK_BITSET_PUT_ON_MASK)
									ELSE
										//Remove Masks and Helmets
										SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0))
										SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_HEAD_NONE)
										
										FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
										//Start_Export_Player_Headshot_Timer() removed for bug 2230280
										BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
										
										// removed for TODO 2227286 
//										INT iWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) 
//										IF iWantedLevel > 0
//											BOOL bGreyedOut
//											IF ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID())
//												bGreyedOut = TRUE
//											ENDIF
//											SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), (iWantedLevel-1))
//											SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//											IF bGreyedOut = TRUE
//												FORCE_START_HIDDEN_EVASION(PLAYER_ID())
//											ENDIF
//							           		PRINTLN(" MASK - REMOVE - MAINTAIN_QUICK_EQUIP_MASK - LOWER WANTED LEVEL TO ", (iWantedLevel-1))
//										ENDIF
										
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK3")
											CLEAR_HELP()
										ENDIF
										
										SET_BIT(iDoneBitSet, LBS_MASK_QUICK_REMOVED)
							            PRINTLN(" MASK - REMOVE - MAINTAIN_QUICK_EQUIP_MASK - LBS_MASK_QUICK_REMOVED - SET")
									ENDIF
									
									SET_BIT(iDoneBitSet, LBS_MASK_EXTEND_CONTROL_BLOCK)
						            PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - LBS_MASK_EXTEND_CONTROL_BLOCK - SET")
									
								ENDIF
							ENDIF
						ELSE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK1")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK2")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK3")
								CLEAR_HELP()
								CLEAR_BIT(iDoneBitSet, LBS_MASK_QUICK_EQUIP_HELP_2)
								CLEAR_BIT(iDoneBitSet, LBS_MASK_QUICK_REMOVE_HELP)
							ENDIF
						ENDIF
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK1")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK2")
							CLEAR_HELP()
							PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - ALREADY WEARING A HELMET")
						ENDIF
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
						AND NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet)
						AND NOT GET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_PuttingOnHelmet)
						AND NOT IS_PED_TAKING_OFF_HELMET(PLAYER_PED_ID())
							SET_BIT(iDoneBitSet, LBS_MASK_QUICK_EQUIPPED)
							PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - ALREADY WEARING A HELMET - NOT FROM A VEHICLE - LBS_MASK_QUICK_EQUIPPED - SET")
						ENDIF
					ENDIF
				ELSE
					IF bRemoveMask = FALSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK1")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK2")
							CLEAR_HELP()
						ENDIF
						SET_BIT(iDoneBitSet, LBS_MASK_QUICK_EQUIPPED)
						PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - ALREADY WEARING A MASK - LBS_MASK_QUICK_EQUIPPED - SET")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK3")
							CLEAR_HELP()
						ENDIF
						SET_BIT(iDoneBitSet, LBS_MASK_QUICK_REMOVED)
						PRINTLN(" MASK - REMOVE - MAINTAIN_QUICK_EQUIP_MASK - NOT WEARING A MASK - LBS_MASK_QUICK_REMOVED - SET")
					ENDIF
				ENDIF
			ENDIF
			
		
		ELSE
			IF bAnimationPlaying	//	This flag is needed at is a cleanup is done then the IS_ENTITY_PLAYING_ANIM will assert due to the maskAnimData being cleared.
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), GET_ANIM_DICT_MASK(maskAnimData), GET_ANIM_CLIP_NAME_MASK())
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						CDEBUG3LN(DEBUG_MISSION, "[RBJ] - MASK - net_mission_controler_public - MAINTAIN_QUICK_EQUIP_MASK - Player in car so SET_PED_CAN_PLAY_AMBIENT_IDLES(PLAYER_PED_ID(), TRUE, TRUE).")
						SET_PED_CAN_PLAY_AMBIENT_IDLES(PLAYER_PED_ID(), TRUE, TRUE)
					ENDIF
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				ELSE
					CLEANUP_MASK_ANIMS(maskAnimData)
					bAnimationPlaying = FALSE
				ENDIF
			ELSE
				//bPreloadedMaskOutfitData = FALSE
				CLEANUP_MASK_ANIMS(maskAnimData)
			ENDIF
		ENDIF
		
		//BLOCK CONTROLS
		IF IS_BIT_SET(iDoneBitSet, LBS_MASK_EXTEND_CONTROL_BLOCK)
			IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_DETONATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_THROW_GRENADE)
				PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - LBS_MASK_EXTEND_CONTROL_BLOCK - BLOCKING DPAD LEFT INPUTS")
			ELSE
				CLEAR_BIT(iDoneBitSet, LBS_MASK_EXTEND_CONTROL_BLOCK)
			    PRINTLN(" MASK - MAINTAIN_QUICK_EQUIP_MASK - LBS_MASK_EXTEND_CONTROL_BLOCK - CLEARED")
			ENDIF
		ENDIF
	ELSE
		IF bAnimationPlaying
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), GET_ANIM_DICT_MASK(maskAnimData), GET_ANIM_CLIP_NAME_MASK())
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					CDEBUG3LN(DEBUG_MISSION, "[RBJ] - MASK - net_mission_controler_public - MAINTAIN_QUICK_EQUIP_MASK - Player in car so SET_PED_CAN_PLAY_AMBIENT_IDLES(PLAYER_PED_ID(), TRUE, TRUE).")
					SET_PED_CAN_PLAY_AMBIENT_IDLES(PLAYER_PED_ID(), TRUE, TRUE)
				ENDIF
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			ELSE
				CLEANUP_MASK_ANIMS(maskAnimData)
				bAnimationPlaying = FALSE
			ENDIF
		ELSE
			//bPreloadedMaskOutfitData = FALSE
			CLEANUP_MASK_ANIMS(maskAnimData)
		ENDIF
//		bPreloadedMaskOutfitData = FALSE
//		CLEANUP_MASK_ANIMS(maskAnimData)
	ENDIF
ENDPROC

