///    net_arena_box_join.sch
///    
///    Info: 
///    Functionality for joining in arena wars missions as a box spectator via walk in

USING "globals.sch"
USING "script_maths.sch"
USING "script_network.sch"
USING "net_include.sch"
USING "net_transition_sessions_private.sch"
USING "net_corona_v2.sch"
USING "fmmc_corona_header.sch"


FUNC BOOL START_ARENA_BOX_SPECTATE_TRANSITION()
	BOOL bInWorkShop = IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
	IF LAUNCH_NEXT_ARENA_SERIES_RACE(DEFAULT, bInWorkShop, DEFAULT, FALSE)
		SET_TRANSITION_SESSIONS_LAUNCHING_ARENA_SPECTATE()
		IF bInWorkShop
			BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD")
			END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
			//Store_Heist_Data_Ready_For_Cross_Session_Transmission(Get_Players_Current_Apartment(), FALSE, FALSE, FALSE) 
			VECTOR vOnCallCamera
			BOOL bTemp
			INT iSetupBS
			SET_BIT(iSetupBS, ciCORONA_CAMERA_SETUP_BS_ARENA)
			SETUP_CORONA_CAMERA(g_sTransitionSessionData.ciCam, FMMC_TYPE_MISSION, 0, << 0.0, 0.0, 0.0 >>, vOnCallCamera, << 0.0, 0.0, 0.0 >>, TRUE, FALSE, vOnCallCamera, bTemp, bTemp, FALSE, iSetupBS)
			DISPLAY_RADAR(FALSE)
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL JOIN_ARENA_BOX_SPECTATE()
//	SET_LOCAL_PLAYER_STAYS_IN_ARENA_BOX_BETWEEN_ROUNDS(TRUE)
	RETURN START_ARENA_BOX_SPECTATE_TRANSITION()
ENDFUNC

FUNC BOOL JOIN_ARENA_BOX_SPECTATE_WITH_NEARBY()
//	SET_LOCAL_PLAYER_STAYS_IN_ARENA_BOX_BETWEEN_ROUNDS(TRUE)
	RETURN START_ARENA_BOX_SPECTATE_TRANSITION()
ENDFUNC

