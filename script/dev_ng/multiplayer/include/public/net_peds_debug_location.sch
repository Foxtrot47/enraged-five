//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_DEBUG_LOCATION.sch																				//
// Description: Functions to implement from net_peds_base.sch and look up tables to return debug location ped data.		//
//				This is the base debug ped data used when launching the ped system via the freemode debug widgets.		//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		09/03/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD
USING "net_peds_base.sch"
USING "website_public.sch"
USING "net_peds_data_common.sch"
USING "net_simple_cutscene_common.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_DEBUG_LOCATION_PED_DATA_LAYOUT_0(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
		// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= -1
					Data.iPackedDrawable[1]								= -1
					Data.iPackedDrawable[2]								= -1
					Data.iPackedTexture[0] 								= -1
					Data.iPackedTexture[1] 								= -1
					Data.iPackedTexture[2] 								= -1
					Data.vPosition 										= <<1560.0000, 400.0000, -50.0000>>
					Data.vRotation 										= <<0.0, 0.0, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTIONS TO IMPLEMENT ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_DEBUG_LOCATION_PED_SCRIPT_LAUNCH()
	RETURN TRUE
ENDFUNC

FUNC BOOL _IS_DEBUG_LOCATION_PARENT_A_SIMPLE_INTERIOR
	RETURN FALSE
ENDFUNC

PROC _SET_DEBUG_LOCATION_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
	UNUSED_PARAMETER(ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	SWITCH iLayout
		CASE 0
			_SET_DEBUG_LOCATION_PED_DATA_LAYOUT_0(Data, iPed, bSetPedArea)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT _GET_DEBUG_LOCATION_NETWORK_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	UNUSED_PARAMETER(ServerBD)
	RETURN 0
ENDFUNC

FUNC INT _GET_DEBUG_LOCATION_LOCAL_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	RETURN 0
ENDFUNC

FUNC INT _GET_DEBUG_LOCATION_SERVER_PED_LAYOUT_TOTAL()
	RETURN 1
ENDFUNC

FUNC INT _GET_DEBUG_LOCATION_SERVER_PED_LAYOUT()
	RETURN 0
ENDFUNC

FUNC INT _GET_DEBUG_LOCATION_SERVER_PED_LEVEL()
	RETURN 0
ENDFUNC

PROC _SET_DEBUG_LOCATION_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBD.iLayout 			= _GET_DEBUG_LOCATION_SERVER_PED_LAYOUT()
		ServerBD.iLevel 			= _GET_DEBUG_LOCATION_SERVER_PED_LEVEL()
		ServerBD.iMaxLocalPeds 		= _GET_DEBUG_LOCATION_LOCAL_PED_TOTAL(ServerBD)
		ServerBD.iMaxNetworkPeds	= _GET_DEBUG_LOCATION_NETWORK_PED_TOTAL(ServerBD)
	ENDIF
	g_bInitPedsCreated = TRUE
	PRINTLN("[AM_MP_PEDS] _SET_DEBUG_LOCATION_PED_SERVER_DATA - Layout: ", ServerBD.iLayout)
	PRINTLN("[AM_MP_PEDS] _SET_DEBUG_LOCATION_PED_SERVER_DATA - Level: ", ServerBD.iLevel)
	PRINTLN("[AM_MP_PEDS] _SET_DEBUG_LOCATION_PED_SERVER_DATA - Max Local Peds: ", ServerBD.iMaxLocalPeds)
	PRINTLN("[AM_MP_PEDS] _SET_DEBUG_LOCATION_PED_SERVER_DATA - Max Network Peds: ", ServerBD.iMaxNetworkPeds)
ENDPROC

FUNC BOOL _HAS_DEBUG_LOCATION_PED_BEEN_CREATED(PEDS_DATA &Data, INT iLevel)
	UNUSED_PARAMETER(iLevel)
	RETURN IS_ENTITY_ALIVE(Data.PedID)
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED MODELS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_DEBUG_LOCATION_LOCAL_PED_PROPERTIES(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)
	CLEAR_PED_TASKS(PedID)
ENDPROC

PROC _SET_DEBUG_LOCATION_NETWORK_PED_PROPERTIES(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_PED(NetworkPedID), TRUE)
	
	IF NOT IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, TRUE)
	ENDIF
ENDPROC

PROC _SET_DEBUG_LOCATION_PED_PROP_INDEXES(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(iLayout)
	SWITCH ePedModel
		CASE PED_MODEL_DAVE
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
		BREAK
		CASE PED_MODEL_SCOTT
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
		BREAK
		CASE PED_MODEL_ELRUBIO
			SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0)
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
		BREAK
		CASE PED_MODEL_KEINEMUSIK_RAMPA
			SET_PED_PROP_INDEX(PedID, ANCHOR_LEFT_WRIST, 0)
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
		BREAK
		CASE PED_MODEL_KEINEMUSIK_ME
			SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0)
		BREAK
		CASE PED_MODEL_KEINEMUSIK_ADAM
			SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0)
			SET_PED_PROP_INDEX(PedID, ANCHOR_LEFT_WRIST, 0)
			SET_PED_PROP_INDEX(PedID, ANCHOR_RIGHT_WRIST, 0)
		BREAK
		CASE PED_MODEL_PALMS_TRAX
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
		BREAK
		CASE PED_MODEL_MOODYMANN
			SET_PED_PROP_INDEX(PedID, ANCHOR_EARS, 0)
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
		BREAK
		CASE PED_MODEL_MOODYMANN_DANC_01
			SET_PED_PROP_INDEX(PedID, ANCHOR_LEFT_WRIST, 0)
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
			SET_PED_PROP_INDEX(PedID, ANCHOR_EARS, 0)
		BREAK
		CASE PED_MODEL_MOODYMANN_DANC_02
			SET_PED_PROP_INDEX(PedID, ANCHOR_RIGHT_WRIST, 0)
		BREAK
		CASE PED_MODEL_MIGUEL
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
		BREAK
		CASE PED_MODEL_PATRICIA
			SET_PED_PROP_INDEX(PedID, ANCHOR_LEFT_WRIST, 0)
		BREAK
		CASE PED_MODEL_PAVEL
			SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL _IS_PLAYER_IN_DEBUG_LOCATION_PARENT_PROPERTY(PLAYER_INDEX playerID)
	UNUSED_PARAMETER(playerID)
	RETURN TRUE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED SPEECH ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_DEBUG_LOCATION_PED_SPEECH_DATA_LAYOUT_0(SPEECH_DATA &SpeechData, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 0	
				SpeechData.fGreetSpeechDistance				= 3.5
				SpeechData.fByeSpeechDistance				= 4.7
				SpeechData.fListenDistance					= 7.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_DEBUG_LOCATION_PED_SPEECH_DATA(SPEECH_DATA &SpeechData, INT iLayout, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	SWITCH iLayout
		CASE 0
			_SET_DEBUG_LOCATION_PED_SPEECH_DATA_LAYOUT_0(SpeechData, iArrayID, iSpeechPedID, bNetworkPed)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL _CAN_DEBUG_LOCATION_PED_PLAY_SPEECH(PED_INDEX PedID, INT iPed, INT iLayout, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedSpeech)
	UNUSED_PARAMETER(iLayout)
	
	// Generic conditions
	IF NOT IS_ENTITY_ALIVE(PedID)
		PRINTLN("[AM_MP_PEDS] _CAN_DEBUG_LOCATION_PED_PLAY_SPEECH - Bail Reason: Ped is not alive")
		RETURN FALSE
	ENDIF
	
	IF NOT g_bInitPedsCreated
		PRINTLN("[AM_MP_PEDS] _CAN_DEBUG_LOCATION_PED_PLAY_SPEECH - Bail Reason: Waiting for all peds to be created first")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_DEBUG_LOCATION_PED_PLAY_SPEECH - Bail Reason: Player is walking in or out of interior")
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		PRINTLN("[AM_MP_PEDS] _CAN_DEBUG_LOCATION_PED_PLAY_SPEECH - Bail Reason: Screen is fading out")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[AM_MP_PEDS] _CAN_DEBUG_LOCATION_PED_PLAY_SPEECH - Bail Reason: Browser is open")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE() OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_DEBUG_LOCATION_PED_PLAY_SPEECH - Bail Reason: Cutscene is active")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() OR IS_TRANSITION_SESSION_LAUNCHING() OR IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN("[AM_MP_PEDS] _CAN_DEBUG_LOCATION_PED_PLAY_SPEECH - Bail Reason: Player in corona")
		RETURN FALSE
	ENDIF
	
	// Specific conditions
	SWITCH iPed
		CASE 0
			
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC PED_SPEECH _GET_DEBUG_LOCATION_PED_SPEECH_TYPE(INT iPed, PED_ACTIVITIES ePedActivity, INT iSpeech)
	UNUSED_PARAMETER(ePedActivity)
	PED_SPEECH eSpeech = PED_SPH_INVALID
	
	SWITCH iPed
		CASE 0
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_LOITER											BREAK
				CASE 3	eSpeech = PED_SPH_PT_WHERE_TO										BREAK
				CASE 4	eSpeech = PED_SPH_CT_WHATS_UP										BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN eSpeech
ENDFUNC

PROC _GET_DEBUG_LOCATION_PED_CONVO_DATA(PED_CONVO_DATA &convoData, INT iPed, PED_ACTIVITIES ePedActivity, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedActivity)
	RESET_PED_CONVO_DATA(convoData)
	
	SWITCH iPed
		CASE 0
			convoData.iSpeakerID						= 2
			convoData.sCharacterVoice					= "HS4_PAVEL"
			convoData.sSubtitleTextBlock				= "HS4PIAU"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					convoData.sRootName = "GENERIC_HI"
				BREAK
				CASE PED_SPH_PT_BYE
					convoData.sRootName = "GENERIC_BYE"
				BREAK
				CASE PED_SPH_PT_LOITER
					convoData.sRootName = "LOITER"
				BREAK
				CASE PED_SPH_PT_WHERE_TO
					convoData.sRootName = "TRAVEL_WHERE_TO"
				BREAK
				CASE PED_SPH_CT_WHATS_UP
					convoData.sRootName = "GENERIC_HOWSITGOING"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC FLOAT _GET_DEBUG_LOCATION_PED_SPEECH_TRIGGER_DISTANCE(INT iPed, PED_SPEECH ePedSpeech)
	FLOAT fDistance = 0.0
	
	SWITCH iPed
		CASE 0
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING		fDistance = 2.0		BREAK
				CASE PED_SPH_PT_BYE				fDistance = 4.0		BREAK
				CASE PED_SPH_LISTEN_DISTANCE	fDistance = 6.0		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN fDistance
ENDFUNC

FUNC PED_SPEECH _GET_DEBUG_LOCATION_PED_CONTROLLER_SPEECH(PED_SPEECH &eCurrentSpeech, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeech
	INT iAttempts = 0
	INT iMaxSpeech
	INT iRandSpeech
	PED_SPEECH eSpeech
	
	INT iMaxControllerSpeechTypes = 0
	INT iControllerSpeechTypes[PED_SPH_TOTAL]
	
	SWITCH iPed
		CASE 0
			
			// Description: Default simply selects a new speech to play that isn't the same as the previous speech.
			PED_CONVO_DATA convoData
			
			// Populate the iControllerSpeechTypes array with all the controller speech type IDs from _GET_GENREIC_PED_SPEECH_TYPE
			REPEAT PED_SPH_TOTAL iSpeech
				eSpeech = _GET_DEBUG_LOCATION_PED_SPEECH_TYPE(iPed, ePedActivity, iSpeech)
				IF (eSpeech > PED_SPH_PT_TOTAL AND eSpeech < PED_SPH_CT_TOTAL)
					RESET_PED_CONVO_DATA(convoData)
					_GET_DEBUG_LOCATION_PED_CONVO_DATA(convoData, iPed, ePedActivity, eSpeech)
					IF IS_CONVO_DATA_VALID(convoData)
						iControllerSpeechTypes[iMaxControllerSpeechTypes] = iSpeech
						iMaxControllerSpeechTypes++
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (iMaxControllerSpeechTypes > 1)
				iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
				eSpeech = _GET_DEBUG_LOCATION_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
				
				// Ensure speech type is different from previous
				WHILE (eSpeech = eCurrentSpeech AND iAttempts < 10)
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
					eSpeech = _GET_DEBUG_LOCATION_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
					iAttempts++
				ENDWHILE
				
				// Randomising failed to find new speech type. Manually set it.
				IF (iAttempts >= 10)
					REPEAT iMaxSpeech iSpeech
						eSpeech = _GET_DEBUG_LOCATION_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iSpeech])
						IF (eSpeech != eCurrentSpeech)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
				
			ELSE
				eSpeech = _GET_DEBUG_LOCATION_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[0])
			ENDIF
			
		BREAK
	ENDSWITCH
	
	eCurrentSpeech = eSpeech
	RETURN eSpeech
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED ANIM DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Note: Activities with multiple animations have their anim data populated in their own functions below.
//		 If an activity only has one animation associated with it, it is populated within _GET_DEBUG_LOCATION_PED_ANIM_DATA itself.
//
// Note: Each activity either has a _M_ or _F_ in its title to specify which ped gender should be using that activity.
//		 If an activity does not has either an _M_ or _F_ in its title; it is unisex, and can be used by either ped gender.
//
// Note: Some animations have been excluded from activities. 
//		 The excluded anims have Z axis starting positions that dont line up with the other anims in the same dictionary.
//		 This causes a snap that a blend cannot fix. Use the widget 'RAG/Script/AM_MP_PEDS/Animation/Output Initial Activity Anim Data' to see which Z axis anims are broken.

//╒═══════════════════════════════╕
//╞══════════╡ AMBIENT ╞══════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_DEBUG_LOCATION_AMBIENT_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "IDLE_B"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH PARTY STAND ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING GET_DEBUG_LOCATION_BEACH_PARTY_STAND_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_MALE_A_BASE"				BREAK
		CASE 1	sAnimClip = "STAND_MALE_A_IDLE_A"			BREAK
		CASE 2	sAnimClip = "STAND_MALE_A_IDLE_B"			BREAK
		CASE 3	sAnimClip = "STAND_MALE_A_IDLE_C"			BREAK
		CASE 4	sAnimClip = "STAND_MALE_A_IDLE_D"			BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_DEBUG_LOCATION_BEACH_PARTY_STAND_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_MALE_B_BASE"				BREAK
		CASE 1	sAnimClip = "STAND_MALE_B_IDLE_A"			BREAK
		CASE 2	sAnimClip = "STAND_MALE_B_IDLE_B"			BREAK
		CASE 3	sAnimClip = "STAND_MALE_B_IDLE_C"			BREAK
		CASE 4	sAnimClip = "STAND_MALE_B_IDLE_D"			BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═════════════════════════════════╕
//╞═╡ BEACH PARTY DRINK CUP STAND ╞═╡
//╘═════════════════════════════════╛

FUNC STRING GET_DEBUG_LOCATION_BEACH_PARTY_DRINK_CUP_STAND_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_DRINK_CUP_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_DEBUG_LOCATION_BEACH_PARTY_DRINK_CUP_STAND_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_DRINK_CUP_FEMALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════════╕
//╞═╡ BEACH PARTY CELL PHONE STAND ╞═╡
//╘══════════════════════════════════╛

FUNC STRING GET_DEBUG_LOCATION_BEACH_PARTY_CELL_PHONE_STAND_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_CELL_PHONE_FEMALE_A_BASE"	BREAK
		CASE 1	sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ MUSCLE FLEX ╞════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_DEBUG_LOCATION_MUSCLE_FLEX_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_MUSCLE_FLEX_ARMS_IN_FRONT_BASE"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════════╕
//╞═╡ BEACH PARTY SMOKE WEED STAND ╞═╡
//╘══════════════════════════════════╛

FUNC STRING GET_DEBUG_LOCATION_BEACH_PARTY_SMOKE_WEED_STAND_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_SMOKE_WEED_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

PROC _GET_DEBUG_LOCATION_PED_ANIM_DATA(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	UNUSED_PARAMETER(iClip)
	UNUSED_PARAMETER(iPedID)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(eTransitionState)
	UNUSED_PARAMETER(bPedTransition)
	RESET_PED_ANIM_DATA(pedAnimData)
	
	SWITCH eActivity
		CASE PED_ACT_AMBIENT_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@FEMALE_HOLD_ARM@IDLE_A"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_DEBUG_LOCATION_AMBIENT_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_DEBUG_LOCATION_BEACH_PARTY_STAND_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_DEBUG_LOCATION_BEACH_PARTY_DRINK_CUP_STAND_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_DEBUG_LOCATION_BEACH_PARTY_CELL_PHONE_STAND_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_DEBUG_LOCATION_BEACH_PARTY_STAND_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_DEBUG_LOCATION_BEACH_PARTY_DRINK_CUP_STAND_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_MUSCLE_FLEX
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_DEBUG_LOCATION_MUSCLE_FLEX_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_DEBUG_LOCATION_BEACH_PARTY_SMOKE_WEED_STAND_M_01_ANIM_CLIP(iClip)
		BREAK
	ENDSWITCH
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PROP ANIM DATA ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _GET_DEBUG_LOCATION_PED_PROP_ANIM_DATA(PED_ACTIVITIES eActivity, PED_PROP_ANIM_DATA &pedPropAnimData, INT iProp = 0, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bHeeledPed = FALSE)
	
	UNUSED_PARAMETER(eActivity)
	UNUSED_PARAMETER(pedPropAnimData)
	UNUSED_PARAMETER(iProp)
	UNUSED_PARAMETER(iClip)
	
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(bHeeledPed)
	RESET_PED_PROP_ANIM_DATA(pedPropAnimData)
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_DEBUG_LOCATION_LOOK_UP_TABLE(PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		// Script Launching
		CASE E_SHOULD_PED_SCRIPT_LAUNCH
			interface.returnShouldPedScriptLaunch = &_SHOULD_DEBUG_LOCATION_PED_SCRIPT_LAUNCH
		BREAK
		CASE E_IS_PARENT_A_SIMPLE_INTERIOR
			interface.returnIsParentASimpleInterior = &_IS_DEBUG_LOCATION_PARENT_A_SIMPLE_INTERIOR
		BREAK
		
		// Ped Data
		CASE E_GET_LOCAL_PED_TOTAL
			interface.returnGetLocalPedTotal = &_GET_DEBUG_LOCATION_LOCAL_PED_TOTAL
		BREAK
		CASE E_GET_NETWORK_PED_TOTAL
			interface.returnGetNetworkPedTotal = &_GET_DEBUG_LOCATION_NETWORK_PED_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT_TOTAL
			interface.returnGetServerPedLayoutTotal= &_GET_DEBUG_LOCATION_SERVER_PED_LAYOUT_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT
			interface.returnGetServerPedLayout = &_GET_DEBUG_LOCATION_SERVER_PED_LAYOUT
		BREAK
		CASE E_GET_SERVER_PED_LEVEL
			interface.returnGetServerPedLevel = &_GET_DEBUG_LOCATION_SERVER_PED_LEVEL
		BREAK
		CASE E_SET_PED_DATA
			interface.setPedData = &_SET_DEBUG_LOCATION_PED_DATA
		BREAK
		CASE E_SET_PED_SERVER_DATA
			interface.setPedServerData = &_SET_DEBUG_LOCATION_PED_SERVER_DATA
		BREAK
		CASE E_GET_PED_ANIM_DATA
			interface.getPedAnimData = &_GET_DEBUG_LOCATION_PED_ANIM_DATA
		BREAK
		CASE E_GET_PED_PROP_ANIM_DATA
			interface.getPedPropAnimData = &_GET_DEBUG_LOCATION_PED_PROP_ANIM_DATA
		BREAK
		CASE E_IS_PLAYER_IN_PARENT_PROPERTY
			interface.returnIsPlayerInParentProperty = &_IS_PLAYER_IN_DEBUG_LOCATION_PARENT_PROPERTY
		BREAK
		
		// Ped Creation
		CASE E_SET_LOCAL_PED_PROPERTIES
			interface.setLocalPedProperties = &_SET_DEBUG_LOCATION_LOCAL_PED_PROPERTIES
		BREAK
		CASE E_SET_NETWORK_PED_PROPERTIES
			interface.setNetworkPedProperties = &_SET_DEBUG_LOCATION_NETWORK_PED_PROPERTIES
		BREAK
		CASE E_SET_PED_PROP_INDEXES
			interface.setPedPropIndexes = &_SET_DEBUG_LOCATION_PED_PROP_INDEXES
		BREAK
		CASE E_HAS_PED_BEEN_CREATED
			interface.returnHasPedBeenCreated = &_HAS_DEBUG_LOCATION_PED_BEEN_CREATED
		BREAK
		
		// Ped Speech
		CASE E_SET_PED_SPEECH_DATA
			interface.setPedSpeechData = &_SET_DEBUG_LOCATION_PED_SPEECH_DATA
		BREAK
		CASE E_CAN_PED_PLAY_SPEECH
			interface.returnCanPedPlaySpeech = &_CAN_DEBUG_LOCATION_PED_PLAY_SPEECH
		BREAK
		CASE E_GET_PED_SPEECH_TYPE
			interface.returnGetPedSpeechType = &_GET_DEBUG_LOCATION_PED_SPEECH_TYPE
		BREAK
		CASE E_GET_PED_CONTROLLER_SPEECH
			interface.returnGetPedControllerSpeech = &_GET_DEBUG_LOCATION_PED_CONTROLLER_SPEECH
		BREAK
		CASE E_GET_PED_CONVO_DATA
			interface.getPedConvoData = &_GET_DEBUG_LOCATION_PED_CONVO_DATA
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF	// IS_DEBUG_BUILD
