USING "net_hacking_fingerprint_common.sch"

STRUCT FINGERPRINT_CLONE_FINGERPRINT
	INT iSprite_Id // sprite to be used
	INT iBitset // bitset for details
	INT iPositioning // which position
	
ENDSTRUCT

CONST_INT ciFINGERPRINT_SELECTED			0
CONST_INT ciFINGERPRINT_REQUIRED			1

CONST_INT ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS 8
CONST_INT ciFC_TARGET_NUMBER_OF_ELEMENTS 4
CONST_INT ciFC_NUMBER_OF_PRINTS_INGAME 4
CONST_INT ciFC_FINGEPRINT_VARAIATIONS 4

CONST_INT ciFC_FINGERPRINTS_SELECTED 0
CONST_INT ciFC_CHECKING_MATCH 1
CONST_INT ciFC_SHOWING_LOADING 2
CONST_INT ciFC_PASSING_FINGERPRINT 3
CONST_INT ciFC_FAILED_FINGERPRINT 4
CONST_INT ciFC_SCRAMBLING 5
CONST_INT ciFC_SCRAMBLED 6

CONST_INT ciFC_UI_ROW_LENGTH 2
CONST_INT ciFC_UI_COLUMN_LENGTH 4

CONST_INT ciFC_MAX_LIVES  6
CONST_INT ciFC_STARTUP_TIMER 4000
CONST_INT ciFC_HACK_TIME  300000
CONST_INT ciFC_CHECKING_TIME 4000
CONST_INT ciFC_PRE_CHECK_TIME 2000
CONST_INT ciFC_SCRAMBLE_TIME 30000
CONST_INT ciFC_END_DELAY 4000

TWEAK_FLOAT cfFC_FINGERPRINT_ELEMENT_COLUMN1 0.105
TWEAK_FLOAT cfFC_FINGERPRINT_ELEMENT_COLUMN2 0.239
TWEAK_FLOAT cfFC_FINGERPRINT_ELEMENT_ROW1 0.306
TWEAK_FLOAT cfFC_FINGERPRINT_ELEMENT_ROW2 0.439
TWEAK_FLOAT cfFC_FINGERPRINT_ELEMENT_ROW3 0.572
TWEAK_FLOAT cfFC_FINGERPRINT_ELEMENT_ROW4 0.706
TWEAK_FLOAT cfFC_FINGERPRINT_ELEMENT_WIDTH 128.0
TWEAK_FLOAT cfFC_FINGERPRINT_ELEMENT_HEIGHT 128.0
TWEAK_FLOAT cfFC_FINGERPRINT_ELEMENT_SELECTOR_WIDTH 180.0
TWEAK_FLOAT cfFC_FINGERPRINT_ELEMENT_SELECTOR_HEIGHT 180.0

CONST_FLOAT cfFC_FINGERPRINT_LARGE_POSITIONX 0.674
CONST_FLOAT cfFC_FINGERPRINT_LARGE_POSITIONY 0.379
CONST_FLOAT cfFC_FINGERPRINT_LARGE_WIDTH 400.0
CONST_FLOAT cfFC_FINGERPRINT_LARGE_HEIGHT 512.0

CONST_FLOAT cfFC_GRID_DETAILS_LEFTX 0.439
CONST_FLOAT cfFC_GRID_DETAILS_RIGHTX 0.902
CONST_FLOAT cfFC_GRID_DETAILS_WIDTH 16.0
CONST_FLOAT cfFC_GRID_DETAILS_HEIGHT 512.0
CONST_INT ciFC_GRID_DETAILS_FRAMES 4

CONST_FLOAT cfFC_GRID_NOISE_POSITIONX 0.653
CONST_FLOAT cfFC_GRID_NOISE_POSITIONY 0.379
CONST_FLOAT cfFC_GRID_NOISE_WIDTH 800.0
CONST_FLOAT cfFC_GRID_NOISE_HEIGHT 800.0
CONST_INT ciFC_GRID_NOISE_FRAMES 7

CONST_FLOAT cfFC_CIRCLEDECORATION_POSITIONX 0.983
CONST_FLOAT cfFC_CIRCLEDECORATION_POSITIONY 0.669
CONST_FLOAT cfFC_CIRCLEDECORATION_WIDTH 100.0
CONST_FLOAT cfFC_CIRCLEDECORATION_HEIGHT 100.0
CONST_INT ciFC_CIRCLEDECORATION_FRAMES 3

CONST_FLOAT cfFC_FINGERPRINT_TRACKER_WIDTH 128.0
CONST_FLOAT cfFC_FINGERPRINT_TRACKER_HEIGHT 128.0
CONST_FLOAT cfFC_FINGERPRINT_TRACKER_POSITIONY 0.832
CONST_FLOAT cfFC_FINGERPRINT_TRACKER1_POSITIONX 0.536
CONST_FLOAT cfFC_FINGERPRINT_TRACKER2_POSITIONX 0.662
CONST_FLOAT cfFC_FINGERPRINT_TRACKER3_POSITIONX 0.782
CONST_FLOAT cfFC_FINGERPRINT_TRACKER4_POSITIONX 0.905
CONST_FLOAT cfFC_FINGERPRINT_TRACKER_SELECTOR_WIDTH 180.0
CONST_FLOAT cfFC_FINGERPRINT_TRACKER_SELECTOR_HEIGHT 180.0

TWEAK_FLOAT cfFC_SCRAMBLE_POSITIONX 0.169
TWEAK_FLOAT cfFC_SCRAMBLE_POSITIONY 0.845
TWEAK_FLOAT cfFC_SCRAMBLE_LEFTPOINT 0.178

CONST_FLOAT cfFC_TIMER_POSITIONX 0.122
CONST_FLOAT cfFC_TIMER_POSITIONY 0.144
CONST_FLOAT cfFC_TIMER_DIGIT_DIFFERENCE 0.031

CONST_FLOAT cfFC_LIVES_POSITIONX 0.983
CONST_FLOAT cfFC_LIVES_POSITIONY 0.190
CONST_FLOAT	cfFC_LIVES_OFFSET 0.055

CONST_INT ciFC_DISABLED_COLOUR_MOD 4

// DATA
FUNC BOOL fc_IS_FINGERPRINT_REQUIRED(INT iFingerPrint, INT i)
	SWITCH iFingerPrint
		CASE 0
			SWITCH i
				CASE 0
				CASE 3
				CASE 5
				CASE 6
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH i
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH i
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH i
				CASE 0
				CASE 1
				CASE 2
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	
	// Also output stuff to complain about no existing
	RETURN FALSE
ENDFUNC

// DATA DONE


PROC GET_POSITION_FOR_FC_ELEMENT(INT iElement, FLOAT &fElementX, FLOAT &fElementY)
	
	SWITCH iElement
		CASE 0 
			fElementX = cfFC_FINGERPRINT_ELEMENT_COLUMN1 
			fElementY = cfFC_FINGERPRINT_ELEMENT_ROW1
		BREAK
		CASE 1 
			fElementX = cfFC_FINGERPRINT_ELEMENT_COLUMN2
			fElementY = cfFC_FINGERPRINT_ELEMENT_ROW1
		BREAK
		CASE 2 
			fElementX = cfFC_FINGERPRINT_ELEMENT_COLUMN1
			fElementY = cfFC_FINGERPRINT_ELEMENT_ROW2
		BREAK
		CASE 3
			fElementX = cfFC_FINGERPRINT_ELEMENT_COLUMN2
			fElementY = cfFC_FINGERPRINT_ELEMENT_ROW2
		BREAK
		CASE 4 
			fElementX = cfFC_FINGERPRINT_ELEMENT_COLUMN1
			fElementY = cfFC_FINGERPRINT_ELEMENT_ROW3
		BREAK
		CASE 5 
			fElementX = cfFC_FINGERPRINT_ELEMENT_COLUMN2
			fElementY = cfFC_FINGERPRINT_ELEMENT_ROW3
		BREAK
		CASE 6 
			fElementX = cfFC_FINGERPRINT_ELEMENT_COLUMN1
			fElementY = cfFC_FINGERPRINT_ELEMENT_ROW4
		BREAK
		CASE 7 
			fElementX = cfFC_FINGERPRINT_ELEMENT_COLUMN2
			fElementY = cfFC_FINGERPRINT_ELEMENT_ROW4
		BREAK
	ENDSWITCH
ENDPROC

// GAME PLAY FUNCTIONS
PROC SET_ELEMENT_SELECTED(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	
	INT i
	FOR i = 0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		IF sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iPositioning = iFingerPrint
			IF NOT IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFINGERPRINT_SELECTED)
				IF sGameplayData.iSelectedElementCount >= ciFC_TARGET_NUMBER_OF_ELEMENTS
					PRINTLN("[MC][FingerPrintClone] Too many elements selected")
					EXIT
				ENDIF
				PLAY_SOUND_FRONTEND(-1, "Select_Print_Tile", sGameplayData.sBaseStruct.sAudioSet)
				sGameplayData.iSelectedElementCount++
				SET_BIT(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFINGERPRINT_SELECTED)
				PRINTLN("[MC][FingerPrintClone] Selecting element ", iFingerPrint)
			ELSE
				PLAY_SOUND_FRONTEND(-1, "Deselect_Print_Tile", sGameplayData.sBaseStruct.sAudioSet)
				sGameplayData.iSelectedElementCount--
				PRINTLN("[MC][FingerPrintClone] Deselecting element ", iFingerPrint)
				CLEAR_BIT(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFINGERPRINT_SELECTED)
			ENDIF
	
			PRINTLN("[MC][FingerPrintClone] Element Location ", sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iPositioning)
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL CHECK_FINGERPRINT_SELECTED(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	RETURN IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iFingerPrint].iBitset, ciFINGERPRINT_SELECTED)
ENDFUNC

FUNC BOOL CHECK_FINGERPRINT_REQUIRED(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	RETURN IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iFingerPrint].iBitset, ciFINGERPRINT_REQUIRED)
ENDFUNC

FUNC BOOL CHECK_FINGERPRINT_MATCHED(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	RETURN CHECK_FINGERPRINT_REQUIRED(sGameplayData, iFingerPrint) AND CHECK_FINGERPRINT_SELECTED(sGameplayData, iFingerPrint)
ENDFUNC

FUNC BOOL FC_IS_PLAYER_ABLE_TO_INTERACT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	IF sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_PLAY
	AND sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_VISUAL_TEST
		//PRINTLN("[MC][FingerPrintClone] No interact - Wrong state")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
		PRINTLN("[MC][FingerPrintClone] No interact - checking match")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_PASSING_FINGERPRINT)
		PRINTLN("[MC][FingerPrintClone] No interact - passing fingerprint")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_FAILED_FINGERPRINT)
		PRINTLN("[MC][FingerPrintClone] No interact - failed fingerprint")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_SCRAMBLING)
		PRINTLN("[MC][FingerPrintClone] No interact - scrambling")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC START_ELEMENT_CHECK(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
	AND sGameplayData.iSelectedElementCount >= ciFC_TARGET_NUMBER_OF_ELEMENTS
		SET_BIT(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
		SET_BIT(sGameplayData.iF_Bs, ciFC_SHOWING_LOADING)
		// DO OTHER STUFF HERE
		// Audio/Visual effect
	ENDIF
ENDPROC

PROC ADJUST_SELECTOR(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, CONTROL_ACTION eControlAction)
	//INT iTempElement = sGameplayData.iSelectedElement
	PRINTLN("[MC][FingerPrintClone] Old Selector Position ", sGameplayData.iSelectedElement)
	//PRINTLN("[MC][FingerPrintClone] Action ", eControlAction)
	SWITCH eControlAction
		CASE INPUT_FRONTEND_UP
		PRINTLN("[MC][FingerPrintClone] Action Up")
			sGameplayData.iSelectedElement -= ciFC_UI_ROW_LENGTH
			
			IF sGameplayData.iSelectedElement < 0
				IF sGameplayData.iSelectedElement = -1
					sGameplayData.iSelectedElement = ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS - 1
				ELSE
					sGameplayData.iSelectedElement = ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS - ciFC_UI_ROW_LENGTH
				ENDIF
			ENDIF
		BREAK
		CASE INPUT_FRONTEND_DOWN
		PRINTLN("[MC][FingerPrintClone] Action Down")
			sGameplayData.iSelectedElement += ciFC_UI_ROW_LENGTH
			
			IF sGameplayData.iSelectedElement >= ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS
				IF sGameplayData.iSelectedElement = ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS
					sGameplayData.iSelectedElement = 0
				ELSE
					sGameplayData.iSelectedElement = 0 + (ciFC_UI_ROW_LENGTH/ciFC_UI_ROW_LENGTH)
				ENDIF
			ENDIF
		BREAK
		CASE INPUT_FRONTEND_LEFT
		PRINTLN("[MC][FingerPrintClone] Action Left")
			sGameplayData.iSelectedElement -= 1
			
			IF sGameplayData.iSelectedElement < 0
				sGameplayData.iSelectedElement = ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS - 1
			ENDIF
		BREAK
		CASE INPUT_FRONTEND_RIGHT
		PRINTLN("[MC][FingerPrintClone] Action Right")
			sGameplayData.iSelectedElement += 1
			
			IF sGameplayData.iSelectedElement >= ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS
				sGameplayData.iSelectedElement = 0
			ENDIF
		BREAK
	
	ENDSWITCH
	
	PLAY_SOUND_FRONTEND(-1, "Cursor_Move", sGameplayData.sBaseStruct.sAudioSet)
	PRINTLN("[MC][FingerPrintClone] New Selector Position ", sGameplayData.iSelectedElement)
ENDPROC

PROC PROCESS_FINGERPRINT_CLONE_INPUT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	CONTROL_ACTION eLeftStick = HG_GET_LEFT_STICK_ACTION(sGameplayData.sBaseStruct)
	
	// Move around the selection
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR eLeftStick = INPUT_FRONTEND_UP
		ADJUST_SELECTOR(sGameplayData, INPUT_FRONTEND_UP)
	ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	OR eLeftStick = INPUT_FRONTEND_DOWN
		ADJUST_SELECTOR(sGameplayData, INPUT_FRONTEND_DOWN)
	ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	OR eLeftStick = INPUT_FRONTEND_LEFT
		ADJUST_SELECTOR(sGameplayData, INPUT_FRONTEND_LEFT)
	ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	OR eLeftStick = INPUT_FRONTEND_RIGHT
		ADJUST_SELECTOR(sGameplayData, INPUT_FRONTEND_RIGHT)
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciHG_ABORT_WARNING)
			RESET_NET_TIMER(sGameplayData.sBaseStruct.tdCancelTimer)
			SET_BIT(sGameplayData.iF_Bs, ciHG_ABORT_WARNING)
		ENDIF
	ENDIF
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR (IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT))
		 sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
		 SET_ELEMENT_SELECTED(sGameplayData, sGameplayData.iSelectedElement)
	ENDIF
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
		sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
		START_ELEMENT_CHECK(sGameplayData)
	ENDIF
ENDPROC

PROC RUN_SCRAMBLE_PROCESS(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	INT newPositions[ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS]
	INT i
	INT j
	INT iRandomValue
	
	BOOL bWipeSelection
	
	iRandomValue = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	IF iRandomValue < sGameplayData.sBaseStruct.iScrambleWipeChance 
		sGameplayData.iSelectedElementCount = 0
		bWipeSelection = TRUE
		
	ENDIF
	
	FOR i = 0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		newPositions[i] = -1
	ENDFOR

	FOR i = 0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		newPositions[i] = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(newPositions, ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS,0,ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS )
	ENDFOR
	
	FOR i=0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		IF newPositions[i] = -1
			PRINTLN("[MC][FingerPrintClone] Scrambling - Element ", i, " is invalid - manually filling")
		
			FOR j = 0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
				IF NOT ARRAY_CONTAINS_INT(newPositions, j, ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS)
					newPositions[i] = j
					PRINTLN("[MC][FingerPrintClone] Scrambling - Element ", i, " manually filled with ", j)
					BREAKLOOP
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	
	// Apply positions
	FOR i = 0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		PRINTLN("[MC][FingerPrintClone] Scrambling - Element ", i)
		PRINTLN("[MC][FingerPrintClone] Scrambling - Old Position ", sGameplayData.sFingerPrintElements[iFingerPrint][i].iPositioning)
		PRINTLN("[MC][FingerPrintClone] Scrambling - New Position ", newPositions[i])
	
		sGameplayData.sFingerPrintElements[iFingerPrint][i].iPositioning = newPositions[i]
		
		IF bWipeSelection
			CLEAR_BIT(sGameplayData.sFingerPrintElements[iFingerPrint][i].iBitset, ciFINGERPRINT_SELECTED)
		ENDIF
	ENDFOR
	
ENDPROC

PROC SCRAMBLE_TILE_LOCATIONS(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	RUN_SCRAMBLE_PROCESS(sGameplayData, sGameplayData.iCurrentFingerprintIndex)
	
	sGameplayData.iSelectedElement = 0
	SET_BIT(sGameplayData.iF_Bs, ciFC_SCRAMBLED)
ENDPROC

PROC MAINTAIN_FINGERPRINT_CLONE_HELP(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, BOOL bShowFinalQuitWarning, HG_CONTROL_STRUCT sControllerStruct)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HG_FC_HELP_04a")
	AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		EXIT
	ENDIF
	
	// scramble
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciHG_ABORT_WARNING)
		IF NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_TEAM_HAS_AGGRO)
			IF NOT bShowFinalQuitWarning
				PRINT_HELP_NO_SOUND("HG_FC_HELP_04a", ciHG_FIRST_ABORT_TIME)
			ELSE
				PRINT_HELP_NO_SOUND("HG_FC_HELP_04")
			ENDIF
		ENDIF
			
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdCancelTimer, ciHG_ABORT_WARNING_TIME)
			CLEAR_BIT(sGameplayData.iF_Bs, ciHG_ABORT_WARNING)
		ENDIF

		EXIT
	ENDIF
	
	// scramble
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_SCRAMBLING)
		PRINT_HELP_NO_SOUND("HG_FC_HELP_03")
		EXIT
	ENDIF
	
	
	// check
	IF sGameplayData.iSelectedElementCount >= ciFC_TARGET_NUMBER_OF_ELEMENTS
	AND FC_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		PRINT_HELP_NO_SOUND("HG_FC_HELP_02")
		EXIT
	ENDIF
	
	// Select
	IF FC_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		PRINT_HELP_NO_SOUND("HG_FC_HELP_01")
	ENDIF
ENDPROC

PROC MAINTAIN_FINGERPRINT_CLONE_CONTEXT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	IF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_PLAY
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
			PRINTLN("[MC][FingerprintClone] Drawing cancel prompt")
		ENDIF
	
		SPRITE_PLACEMENT thisSpritePlacement = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
		
		IF sGameplayData.sBaseStruct.bUpdatePrompts
			REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sGameplayData.sBaseStruct.scaleformInstructionalButtons)
			
			// Exit
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sGameplayData.sBaseStruct.tdCancelTimer), ci_HG_QUIT_TIME, "HG_INT_04a")
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
			ENDIF
			// Select
			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HG_INT_01", sGameplayData.sBaseStruct.scaleformInstructionalButtons)
			
			IF sGameplayData.iSelectedElementCount >= ciFC_TARGET_NUMBER_OF_ELEMENTS
			// Check
			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "HG_INT_02", sGameplayData.sBaseStruct.scaleformInstructionalButtons)
			ENDIF
			
			// Change Selector
			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_ALL), "HG_INT_03", sGameplayData.sBaseStruct.scaleformInstructionalButtons)			
			
			sGameplayData.sBaseStruct.bUpdatePrompts = FALSE
		ENDIF
		
		//SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sGameplayData.sBaseStruct.scaleformInstructionalButtonsIndex, thisSpritePlacement, sGameplayData.sBaseStruct.scaleformInstructionalButtons)
	ELSE
		RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(sGameplayData.sBaseStruct.scaleformInstructionalButtons)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sGameplayData.sBaseStruct.scaleformInstructionalButtonsIndex)
	ENDIF
ENDPROC

PROC PROCESS_FINGERPRINT_CLONE_GAME(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, BOOL bShowFinalQuitWarning)
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
	AND NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFC_PASSING_FINGERPRINT)
	AND NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFC_FAILED_FINGERPRINT)
	// Do the check procedure here
		INT iCurrentlyMatched = 0
		INT i
		PRINTLN("[MC][FingerPrintClone] Fingerprint Check Started")
		FOR i = 0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
			IF CHECK_FINGERPRINT_SELECTED(sGameplayData, i)
				PRINTLN("[MC][FingerPrintClone] Fingerprint Selected: ", i)
			ENDIF
		
			IF CHECK_FINGERPRINT_MATCHED(sGameplayData, i)
				iCurrentlyMatched += 1
				PRINTLN("[MC][FingerPrintClone] Fingerprint Matched: ", i)
			ENDIF
		ENDFOR
		
		IF iCurrentlyMatched >= ciFC_TARGET_NUMBER_OF_ELEMENTS
			PRINTLN("[MC][FingerPrintClone] Fingerprint Passed")
			SET_BIT(sGameplayData.iF_Bs, ciFC_PASSING_FINGERPRINT)
		ELSE
			PRINTLN("[MC][FingerPrintClone] Fingerprint failed - matched: ", iCurrentlyMatched)
			SET_BIT(sGameplayData.iF_Bs, ciFC_FAILED_FINGERPRINT )
	
			sGameplayData.iNumberOfLastMatchedElements = iCurrentlyMatched
		ENDIF
			RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
		PLAY_SOUND_FRONTEND(-1, "Window_Draw", sGameplayData.sBaseStruct.sAudioSet)
		PLAY_SOUND_FRONTEND(sGameplayData.iProcessingSoundID, "Processing", sGameplayData.sBaseStruct.sAudioSet)
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
	AND IS_BIT_SET(sGameplayData.iF_Bs, ciFC_SHOWING_LOADING)
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciFC_PRE_CHECK_TIME)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_SHOWING_LOADING)
			STOP_SOUND(sGameplayData.iProcessingSoundID)
			
			IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_PASSING_FINGERPRINT)
				PLAY_SOUND_FRONTEND(sGameplayData.iProcessingSoundID, "Target_Match", sGameplayData.sBaseStruct.sAudioSet)
			ELSE
				PLAY_SOUND_FRONTEND(sGameplayData.iProcessingSoundID, "No_Match", sGameplayData.sBaseStruct.sAudioSet)
			ENDIF
		ENDIF
	ENDIF
	
	// Help Text
	MAINTAIN_FINGERPRINT_CLONE_HELP(sGameplayData, bShowFinalQuitWarning, sControllerStruct)
	
	IF NOT IS_BIT_SET(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_PLAYSTART)
		PLAY_SOUND_FRONTEND(-1, "Print_Appears", sGameplayData.sBaseStruct.sAudioSet)
		SET_BIT(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_PLAYSTART)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdScrambleTimer, sGameplayData.sBaseStruct.iScrambleTime)
	AND FC_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		SET_BIT(sGameplayData.iF_Bs, ciFC_SCRAMBLING)
		CLEAR_BIT(sGameplayData.iF_Bs, ciFC_SCRAMBLED)
		PRINTLN("[MC][FingerPrintClone] Scrambling")
		PLAY_SOUND_FRONTEND(-1, "Print_Shuffle", sGameplayData.sBaseStruct.sAudioSet)
		RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
	ELIF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_SCRAMBLING)
		IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFC_SCRAMBLED)
			SCRAMBLE_TILE_LOCATIONS(sGameplayData)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciFC_CHECKING_TIME)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_SCRAMBLING)
			RESET_NET_TIMER(sGameplayData.sBaseStruct.tdScrambleTimer)
			PRINTLN("[MC][FingerPrintClone] Finishing Scrambling")
		ENDIF
	ENDIF
	
	// RESET ANIMATION
	IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciFC_CHECKING_TIME)
	AND NOT FC_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
	PRINTLN("[MC][FingerPrintClone] Timer Expired")
		IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
			PRINTLN("[MC][FingerPrintClone] Checking Match")
			IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_FAILED_FINGERPRINT)
				CLEAR_BIT(sGameplayData.iF_Bs, ciFC_FAILED_FINGERPRINT)
				CLEAR_BIT(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
				sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
				RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
				RESET_NET_TIMER(sGameplayData.sBaseStruct.tdScrambleTimer)
				sGameplayData.sBaseStruct.iCurrentLives--
				sGameplayData.iSelectedElement = 0
				IF sGameplayData.sBaseStruct.iCurrentLives <= 0
					PRINTLN("[MC][FingerPrintClone] Out of Lives")
					UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_FAIL)
				ELSE
					PLAY_SOUND_FRONTEND(-1, "Window_Clear", sGameplayData.sBaseStruct.sAudioSet)
				
					IF NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_LOST_LIFE)
						SET_BIT(sControllerStruct.iBS, ciHGBS_LOST_LIFE)
					ENDIF
				ENDIF
				PRINTLN("[MC][FingerPrintClone] Finishing Failed")
			ELIF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_PASSING_FINGERPRINT)
				CLEAR_BIT(sGameplayData.iF_Bs, ciFC_PASSING_FINGERPRINT)
				CLEAR_BIT(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
				RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
				RESET_NET_TIMER(sGameplayData.sBaseStruct.tdScrambleTimer)
				sGameplayData.iCurrentFingerprintIndex++
				sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
				sGameplayData.iSelectedElement = 0
				sGameplayData.iSelectedElementCount = 0
				PRINTLN("[MC][FingerPrintClone] Finishing Passed")
				IF sGameplayData.iCurrentFingerprintIndex >= sGameplayData.sBaseStruct.iNumberOfStages
					PRINTLN("[MC][FingerPrintClone] Passing Game")
					sGameplayData.iCurrentFingerprintIndex--
					UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PASS)
				ELSE
					PLAY_SOUND_FRONTEND(-1, "Print_Appears", sGameplayData.sBaseStruct.sAudioSet)
					PLAY_SOUND_FRONTEND(-1, "Window_Clear", sGameplayData.sBaseStruct.sAudioSet)
					
					IF NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED_SECTION)
						SET_BIT(sControllerStruct.iBS, ciHGBS_PASSED_SECTION)
					ENDIF
					
					PRINTLN("[MC][FingerPrintClone] New Fingerprint is ", sGameplayData.iFingerPrintIds[sGameplayData.iCurrentFingerprintIndex])
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC DRAW_FINGERPRINT_ELEMENT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, TEXT_LABEL_23 tFingerPrint,INT iElement, TEXT_LABEL_23 tlDictionary)
	//PRINTLN("[MC][FingerprintClone] Drawing element ", iElement)
	// draw basic fingerprint
	FLOAT elementX
	FLOAT elementY
	
	GET_POSITION_FOR_FC_ELEMENT(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iPositioning, elementX, elementY)
	
	TEXT_LABEL_23 tlElementSprite = tFingerPrint
	tlElementSprite += "Comp_"
	//tlElementSprite += sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iSprite_Id + 1
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_SCRAMBLING)
		IF sGameplayData.sBaseStruct.iDrawCounter <= 0
			sGameplayData.iScrambledValues[iElement] = 0
		ELIF sGameplayData.sBaseStruct.iDrawCounter+iElement = 10
			sGameplayData.iScrambledValues[iElement] =GET_RANDOM_INT_IN_RANGE(1, ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS)
		ENDIF
		IF sGameplayData.iScrambledValues[iElement] = 0
			DRAW_HACKING_SPRITE("mpfclone_common", "comp_blank", elementX, elementY, cfFC_FINGERPRINT_ELEMENT_WIDTH, cfFC_FINGERPRINT_ELEMENT_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA) 
		ELSE
			tlElementSprite += sGameplayData.iScrambledValues[iElement]
			PRINTLN("[MC][FingerprintClone] Drawing scramble sprite ", tlElementSprite)
			DRAW_HACKING_SPRITE(tlDictionary, tlElementSprite, elementX, elementY, cfFC_FINGERPRINT_ELEMENT_WIDTH, cfFC_FINGERPRINT_ELEMENT_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR / ciFC_DISABLED_COLOUR_MOD, sGameplayData.rgbaTintColour.iG / ciFC_DISABLED_COLOUR_MOD, sGameplayData.rgbaTintColour.iB /ciFC_DISABLED_COLOUR_MOD, sGameplayData.rgbaTintColour.iA) 
		ENDIF
	ELSE
		tlElementSprite += sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iSprite_Id + 1
		//PRINTLN("[MC][FingerprintClone] Drawing sprite ", tlElementSprite)
		IF IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iBitset, ciFINGERPRINT_SELECTED)
			// Draw selected
			DRAW_HACKING_SPRITE(tlDictionary, tlElementSprite, elementX, elementY, cfFC_FINGERPRINT_ELEMENT_WIDTH, cfFC_FINGERPRINT_ELEMENT_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA) 
		ELSE
			DRAW_HACKING_SPRITE(tlDictionary, tlElementSprite, elementX, elementY, cfFC_FINGERPRINT_ELEMENT_WIDTH, cfFC_FINGERPRINT_ELEMENT_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR / ciFC_DISABLED_COLOUR_MOD, sGameplayData.rgbaTintColour.iG / ciFC_DISABLED_COLOUR_MOD, sGameplayData.rgbaTintColour.iB / ciFC_DISABLED_COLOUR_MOD, sGameplayData.rgbaTintColour.iA) 
		ENDIF
			
		IF sGameplayData.iSelectedElement = sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iPositioning
			// Draw selector
			DRAW_HACKING_SPRITE("MPFClone_Common", "selectorFrame", elementX, elementY, cfFC_FINGERPRINT_ELEMENT_SELECTOR_WIDTH, cfFC_FINGERPRINT_ELEMENT_SELECTOR_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA) 
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF sGameplayData.sBaseStruct.bDebugShowGuide
				IF IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iBitset, ciFINGERPRINT_REQUIRED)
					DRAW_HACKING_SPRITE("mphackinggame", "Comp_Blank", elementX, elementY, cfFC_FINGERPRINT_ELEMENT_WIDTH / 4, cfFC_FINGERPRINT_ELEMENT_HEIGHT / 4, 0.0, sGameplayData.rgbaTintColour.iR, 0, 0, sGameplayData.rgbaTintColour.iA / 4)
				ENDIF
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC DRAW_LARGE_FINGERPRINT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, TEXT_LABEL_23 tFingerPrint, TEXT_LABEL_23 tlDictionary)
	INT i
	TEXT_LABEL_23 tlElementSprite 
	
	//PRINTLN("[MC][FingerprintClone] Drawing Dictionary ", tlDictionary)

	FOR i = 0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		tlElementSprite = tFingerPrint
		tlElementSprite += i+1
		
		//PRINTLN("[MC][FingerprintClone] Drawing big sprite ", tlElementSprite)
		
		IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
			IF sGameplayData.sBaseStruct.iDrawCounter+i = 0
			OR sGameplayData.sBaseStruct.iDrawCounter-i = 5
			OR sGameplayData.sBaseStruct.iDrawCounter+i = 10
			OR sGameplayData.sBaseStruct.iDrawCounter-i = 15
			OR sGameplayData.sBaseStruct.iDrawCounter+i = 20
				sGameplayData.iCheckingAlphaValue[i] = GET_RANDOM_INT_IN_RANGE(127, 255)
			ENDIF
		
			DRAW_HACKING_SPRITE(tlDictionary, tlElementSprite, cfFC_FINGERPRINT_LARGE_POSITIONX, cfFC_FINGERPRINT_LARGE_POSITIONY, cfFC_FINGERPRINT_LARGE_WIDTH, cfFC_FINGERPRINT_LARGE_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.iCheckingAlphaValue[i])
		ELSE
			DRAW_HACKING_SPRITE(tlDictionary, tlElementSprite, cfFC_FINGERPRINT_LARGE_POSITIONX, cfFC_FINGERPRINT_LARGE_POSITIONY, cfFC_FINGERPRINT_LARGE_WIDTH, cfFC_FINGERPRINT_LARGE_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR/ ciFC_DISABLED_COLOUR_MOD, sGameplayData.rgbaTintColour.iG/ ciFC_DISABLED_COLOUR_MOD, sGameplayData.rgbaTintColour.iB / ciFC_DISABLED_COLOUR_MOD, sGameplayData.rgbaTintColour.iA)
		ENDIF
	ENDFOR
ENDPROC

FUNC FLOAT GET_TRACKER_POSITION(INT iElement)
	FLOAT position
	
	SWITCH iElement
		CASE 1
			position = cfFC_FINGERPRINT_TRACKER1_POSITIONX
		BREAK
		CASE 2
			position = cfFC_FINGERPRINT_TRACKER2_POSITIONX
		BREAK
		CASE 3
			position = cfFC_FINGERPRINT_TRACKER3_POSITIONX
		BREAK
		CASE 4
			position = cfFC_FINGERPRINT_TRACKER4_POSITIONX
		BREAK
	ENDSWITCH
	  
	RETURN position
ENDFUNC
	
PROC DRAW_FINGERPRINT_TRACKER(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iElement)
	TEXT_LABEL_23 tlElementSprite = "Decypher_"
	tlElementSprite += iElement
	
	FLOAT positionX = GET_TRACKER_POSITION(iElement)
	
	IF iElement <= sGameplayData.sBaseStruct.iNumberOfStages
		DRAW_HACKING_SPRITE("MPFClone_Common", tlElementSprite, positionX, cfFC_FINGERPRINT_TRACKER_POSITIONY, cfFC_FINGERPRINT_TRACKER_WIDTH, cfFC_FINGERPRINT_TRACKER_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA/2)
		IF iElement = sGameplayData.iCurrentFingerprintIndex+1
			DRAW_HACKING_SPRITE("MPFClone_Common", "Decyphered_Selector", positionX, cfFC_FINGERPRINT_TRACKER_POSITIONY, cfFC_FINGERPRINT_TRACKER_SELECTOR_WIDTH, cfFC_FINGERPRINT_TRACKER_SELECTOR_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
		ENDIF
	ELSE
		DRAW_HACKING_SPRITE("MPFClone_Common", "Disabled_Signal", positionX, cfFC_FINGERPRINT_TRACKER_POSITIONY, cfFC_FINGERPRINT_TRACKER_WIDTH, cfFC_FINGERPRINT_TRACKER_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA/2)
	ENDIF
ENDPROC

PROC DRAW_FINGERPRINT_CLONE_GRID_NOISE(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HACKING_GAME_STRUCT &sGameStruct,HG_RGBA_COLOUR_STRUCT sColourStruct)
		// DO processing for animated sprites
	
	sGameplayData.iGridNoiseCounter += sGameStruct.iDefaultUpdateFrames
	
	IF sGameplayData.iGridNoiseCounter >= ciFC_GRID_NOISE_FRAMES
		sGameplayData.iGridNoiseCounter = 0
	ENDIF
	
	// assemble sprite name
	TEXT_LABEL_23 tl23Sprite = "Grid_Noise_"
	tl23Sprite += sGameplayData.iGridNoiseCounter
	
	TEXT_LABEL_23 tl23Dict = "MPFClone_Grid"
	
	IF sGameplayData.iGridNoiseCounter > 4
		tl23Dict += "2"
	ELIF sGameplayData.iGridNoiseCounter > 2
		tl23Dict += "1"
	ENDIF
	
	// Draw it
	DRAW_HACKING_SPRITE(tl23Dict, tl23Sprite,  cfFC_GRID_NOISE_POSITIONX, cfFC_GRID_NOISE_POSITIONY, cfFC_GRID_NOISE_WIDTH, cfFC_GRID_NOISE_HEIGHT, 0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA)

ENDPROC

PROC DRAW_FINGERPRINT_CLONE_GRID_DETAILS(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HACKING_GAME_STRUCT &sGameStruct,HG_RGBA_COLOUR_STRUCT sColourStruct)

	UNUSED_PARAMETER(sGameStruct)
	// Draw Noise
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
		DRAW_FINGERPRINT_CLONE_GRID_NOISE(sGameplayData, sGameStruct, sColourStruct)
	ENDIF
	
	// Draw Grid details
	DRAW_HACKING_ANIMATED_SPRITE(sGameStruct, sGameplayData.iGridDetails1Counter, ciFC_GRID_DETAILS_FRAMES, HG_ANIMATION_DEFAULT,"MPFClone_GridDetails", "GridDetails_", cfFC_GRID_DETAILS_LEFTX, cfFC_GRID_NOISE_POSITIONY, cfFC_GRID_DETAILS_WIDTH, cfFC_GRID_DETAILS_HEIGHT, 0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA)
	DRAW_HACKING_ANIMATED_SPRITE(sGameStruct, sGameplayData.iGridDetails2Counter, ciFC_GRID_DETAILS_FRAMES, HG_ANIMATION_DEFAULT,"MPFClone_GridDetails", "GridDetails_", cfFC_GRID_DETAILS_RIGHTX, cfFC_GRID_NOISE_POSITIONY, cfFC_GRID_DETAILS_WIDTH, cfFC_GRID_DETAILS_HEIGHT, 0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA)
ENDPROC

PROC DRAW_FINGERPRINT_CLONE_GAME(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)

	// show normal game
	INT i
	
	TEXT_LABEL_23 tlFingerprintDictionary = "MPFClone_Print"
	tlFingerprintDictionary += sGameplayData.iFingerPrintIds[sGameplayData.iCurrentFingerprintIndex]
	//PRINTLN("[MC][FingerprintClone] Drawing fingerprint ", tlFingerprintDictionary)
	
	TEXT_LABEL_23 tlFingerPrint = "FP"
	tlFingerPrint += sGameplayData.iFingerPrintIds[sGameplayData.iCurrentFingerprintIndex]+1
	tlFingerPrint += "_"
	
	// Fingerprints
	FOR i=0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		DRAW_FINGERPRINT_ELEMENT(sGameplayData, tlFingerPrint, i, tlFingerprintDictionary )
	ENDFOR
	
	// Large FingerPrint
	DRAW_LARGE_FINGERPRINT(sGameplayData, tlFingerPrint, tlFingerprintDictionary )
	
	// DRAW TRACKER
	FOR i = 1 TO ciFC_NUMBER_OF_PRINTS_INGAME
		DRAW_FINGERPRINT_TRACKER(sGameplayData, i)
	ENDFOR
	
	DRAW_FINGERPRINT_CLONE_GRID_DETAILS(sGameplayData, sGameplayData.sBaseStruct, sGameplayData.rgbaTintColour )
	
	// Warning Messages
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH )
		// show common
		IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_SHOWING_LOADING)
		// show checking in progress
			DRAW_HACKING_SPRITE("mphackinggame", "Loading_Window", cfHG_STATUS_MESSAGE_POSITIONX, cfHG_STATUS_MESSAGE_POSITIONY, cfHG_STATUS_MESSAGE_LOADING_WIDTH, cfHG_STATUS_MESSAGE_LOADING_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
			
			// DRAW LOADING BAR
			int progress = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sGameplayData.sBaseStruct.tdAnimationTimer)
			
			IF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
				progress = ciFC_PRE_CHECK_TIME
			ENDIF
			
			FLOAT percentageOfSegments = TO_FLOAT(progress) / TO_FLOAT(ciFC_PRE_CHECK_TIME)
			percentageOfSegments = percentageOfSegments * 100
			//PRINTLN("[HACKINGGAME] Percentage of segments is ", percentageOfSegments)
			
			FLOAT numberOfSegments = (TO_FLOAT(ciHG_RESULTS_LOADING_MAX_SEGMENTS) / 100) * percentageOfSegments
			
			//PRINTLN("[HACKINGGAME] Number of segments is ", numberOfSegments)
			FLOAT posX
			FOR i = 1 TO FLOOR(numberOfSegments)
				posX = cfHG_RESULTS_LOADING_INITIALX + cfHG_RESULTS_LOADING_XDIFF*i
				DRAW_HACKING_SPRITE("mphackinggame", "Loading_Bar_Segment", posX, cfHG_RESULTS_LOADING_POSITIONY, cfHG_RESULTS_LOADING_WIDTH, cfHG_RESULTS_LOADING_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
			ENDFOR
		
		ELIF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_FAILED_FINGERPRINT )
			// Show Failure w/ number
			DRAW_HACKING_ANIMATED_SPRITE(sGameplayData.sBaseStruct, sGameplayData.sBaseStruct.iResultCounter, ciHG_RESULT_FRAME_COUNT, HG_ANIMATION_SLOW, "mphackinggame", "Incorrect_", cfHG_STATUS_MESSAGE_POSITIONX, cfHG_STATUS_MESSAGE_POSITIONY, cfHG_STATUS_MESSAGE_WIDTH, cfHG_STATUS_MESSAGE_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
		ELIF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_PASSING_FINGERPRINT )
			// Show Passing Fingerprint
			DRAW_HACKING_ANIMATED_SPRITE(sGameplayData.sBaseStruct, sGameplayData.sBaseStruct.iResultCounter, ciHG_RESULT_FRAME_COUNT, HG_ANIMATION_SLOW,"mphackinggame", "Correct_", cfHG_STATUS_MESSAGE_POSITIONX, cfHG_STATUS_MESSAGE_POSITIONY, cfHG_STATUS_MESSAGE_WIDTH, cfHG_STATUS_MESSAGE_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
		ENDIF
	ENDIF
	
	
ENDPROC
// END FUNCTION

PROC FILL_FINGERPRINT_STRUCT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	INT i
	
	FOR i = 0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		sGameplayData.sFingerPrintElements[iFingerPrint][i].iPositioning = i
		sGameplayData.sFingerPrintElements[iFingerPrint][i].iSprite_Id = i
		sGameplayData.sFingerPrintElements[iFingerPrint][i].iBitset = 0
		
		IF fc_IS_FINGERPRINT_REQUIRED(sGameplayData.iFingerPrintIds[iFingerPrint], i)
			SET_BIT(sGameplayData.sFingerPrintElements[iFingerPrint][i].iBitset, ciFINGERPRINT_REQUIRED)
		ENDIF
		
		PRINTLN("[MC][FingerprintClone][SETUP] Fingerprint Element ", i)
		PRINTLN("[MC][FingerprintClone][SETUP] Element Position ", sGameplayData.sFingerPrintElements[iFingerPrint][i].iPositioning)
		PRINTLN("[MC][FingerprintClone][SETUP] Element Sprite ID ", sGameplayData.sFingerPrintElements[iFingerPrint][i].iSprite_Id)
		PRINTLN("[MC][FingerprintClone][SETUP] Element Bitset ", sGameplayData.sFingerPrintElements[iFingerPrint][i].iBitset)
		PRINTLN("[MC][FingerprintClone][SETUP] Element End")
	ENDFOR
	
	
ENDPROC

PROC SETUP_FINGERPRINTS(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
UNUSED_PARAMETER(sControllerStruct)
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFC_FINGERPRINTS_SELECTED)
		EXIT
	ENDIF
	
	// Set the finger print IDs
	INT i
	
	FOR i = 0 TO ciFC_NUMBER_OF_PRINTS_INGAME - 1
		sGameplayData.iFingerPrintIds[i] = -1
	ENDFOR
	
	FOR i = 0 TO ciFC_NUMBER_OF_PRINTS_INGAME - 1
		PRINTLN("[MC][FingerprintClone][SETUP] Setting up Fingerprint ", i)
		
		sGameplayData.iFingerPrintIds[i] = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(sGameplayData.iFingerPrintIds, ciFC_NUMBER_OF_PRINTS_INGAME, 0, ciFC_FINGEPRINT_VARAIATIONS)
		
		// Fill in the data
		FILL_FINGERPRINT_STRUCT(sGameplayData, i)
		RUN_SCRAMBLE_PROCESS(sGameplayData, i)
		PRINTLN("[MC][FingerprintClone][SETUP] Finishing setup Fingerprint ", i)
		PRINTLN("[MC][FingerprintClone][SETUP] ID is ", sGameplayData.iFingerPrintIds[i])
	ENDFOR
	
	
	
	SET_BIT(sGameplayData.iF_Bs, ciFC_FINGERPRINTS_SELECTED)
ENDPROC

FUNC TEXT_LABEL_23 GET_DICTIONARY_FOR_FINGERPRINT_ID(INT i)
	TEXT_LABEL_23 t123
	
	t123 = "mpfclone_print"
	t123 += i
	
	return t123
ENDFUNC

FUNC BOOL LOAD_FINGERPRINTS(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
UNUSED_PARAMETER(sControllerStruct)
	IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFC_FINGERPRINTS_SELECTED)
		// Complain loudly to the game's manager
		RETURN FALSE
	ENDIF
	INT i
	TEXT_LABEL_23 tl23
	
	FOR i = 0 TO ciFC_NUMBER_OF_PRINTS_INGAME - 1
		tl23 = GET_DICTIONARY_FOR_FINGERPRINT_ID(sGameplayData.iFingerPrintIds[i])
	
		// Load
		REQUEST_STREAMED_TEXTURE_DICT(tl23)
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
			PRINTLN("[MC][FingerprintClone] Not Loaded ", tl23)
			RETURN FALSE
		ENDIF
	
	ENDFOR

	RETURN TRUE
ENDFUNC

PROC FC_RESET_GAMEPLAY_DATA(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	sGameplayData.iF_Bs = 0
	sGameplayData.sBaseStruct.iCurrentLives = ciFC_MAX_LIVES
	sGameplayData.iCurrentFingerprintIndex = 0
	
	sGameplayData.rgbaTintColour.iR = 255
	sGameplayData.rgbaTintColour.iG = 255
	sGameplayData.rgbaTintColour.iB = 255
	sGameplayData.rgbaTintColour.iA = 250
	
	INT i
	
	FOR i = 0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		sGameplayData.iScrambledValues[i] = 1
	ENDFOR
	
	FOR i = 0 TO ciFC_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		sGameplayData.iCheckingAlphaValue[i] = 255
	ENDFOR
	
	sGameplayData.iGridNoiseCounter = 0
	sGameplayData.iGridDetails1Counter = GET_RANDOM_INT_IN_RANGE(0, ciFC_GRID_DETAILS_FRAMES)
	sGameplayData.iGridDetails2Counter = GET_RANDOM_INT_IN_RANGE(0, ciFC_GRID_DETAILS_FRAMES)
	
	sGameplayData.sBaseStruct.iMaxTime = ciFC_HACK_TIME
	sGameplayData.sBaseStruct.iScrambleTime = ciFC_SCRAMBLE_TIME
	sGameplayData.sBaseStruct.iScrambleWipeChance = 0
	
	sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
	
	#IF IS_DEBUG_BUILD
		sGameplayData.sBaseStruct.fDebugTimeToSet = TO_FLOAT(ciFC_HACK_TIME/60000)
		sGameplayData.sBaseStruct.iDebugLivesToSet = ciFC_MAX_LIVES
		sGameplayData.sBaseStruct.fDebugScrambleTimeToSet = TO_FLOAT(ciFC_SCRAMBLE_TIME/1000)
		sGameplayData.sBaseStruct.iScrambleWipeChanceToSet = 0
	#ENDIF
ENDPROC

PROC DRAW_FINGERPRINT_CLONE_PLAY_AREA(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	DRAW_PLAY_AREA_COMMON(sGameplayData.sBaseStruct)
	
	// Draw Mode Specific play area here
	// Background frames
	DRAW_HACKING_SPRITE("MPFClone_Common", "background_layout", cfHG_BG_XPOS, cfHG_BG_YPOS, cfHG_PLAYAREA_WIDTH, cfHG_PLAYAREA_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
	// Draw scramble timer
	DRAW_HACKING_GAME_SCRAMBLER(sGameplayData.sBaseStruct, sGameplayData.sBaseStruct.iScrambleTime, cfFC_SCRAMBLE_POSITIONX, cfFC_SCRAMBLE_POSITIONY, cfHG_SCRAMBLE_WIDTH, cfHG_SCRAMBLE_HEIGHT, sGameplayData.rgbaTintColour, cfFC_SCRAMBLE_LEFTPOINT, cfHG_SCRAMBLE_DIFFBETWEENSEGMENT, cfHG_SCRAMBLE_ELEMENT_WIDTH, cfHG_SCRAMBLE_ELEMENT_HEIGHT, FC_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData), cfHG_SCRAMBLE_Y_OFFSET, IS_BIT_SET(sGameplayData.iF_Bs, ciFC_SCRAMBLING))
	
	// Timer
	DRAW_HACKING_GAME_TIMER(sGameplayData.sBaseStruct, sGameplayData.sBaseStruct.iMaxTime, cfFC_TIMER_POSITIONX, cfFC_TIMER_POSITIONY, cfHG_TIMER_WIDTH, cfHG_TIMER_HEIGHT, sGameplayData.rgbaTintColour, cfFC_TIMER_DIGIT_DIFFERENCE)
	
	// Draw Lives
	DRAW_HACKING_GAME_LIVES(sGameplayData.sBaseStruct, TRUE, cfFC_LIVES_POSITIONX, cfFC_LIVES_POSITIONY, cfHG_LIVES_WIDTH, cfHG_LIVES_HEIGHT, sGameplayData.rgbaTintColour, cfFC_LIVES_OFFSET)
	
	DRAW_HACKING_ANIMATED_SPRITE(sGameplayData.sBaseStruct, sGameplayData.iCircleDecoration[0], ciFC_CIRCLEDECORATION_FRAMES, HG_ANIMATION_DEFAULT, "mpfclone_common", "disc_A", cfFC_CIRCLEDECORATION_POSITIONX, cfFC_CIRCLEDECORATION_POSITIONY, cfFC_CIRCLEDECORATION_WIDTH, cfFC_CIRCLEDECORATION_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
	DRAW_HACKING_ANIMATED_SPRITE(sGameplayData.sBaseStruct, sGameplayData.iCircleDecoration[1], ciFC_CIRCLEDECORATION_FRAMES, HG_ANIMATION_DEFAULT, "mpfclone_common", "disc_B", cfFC_CIRCLEDECORATION_POSITIONX, cfFC_CIRCLEDECORATION_POSITIONY, cfFC_CIRCLEDECORATION_WIDTH, cfFC_CIRCLEDECORATION_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
	DRAW_HACKING_ANIMATED_SPRITE(sGameplayData.sBaseStruct, sGameplayData.iCircleDecoration[2], ciFC_CIRCLEDECORATION_FRAMES, HG_ANIMATION_DEFAULT, "mpfclone_common", "disc_C", cfFC_CIRCLEDECORATION_POSITIONX, cfFC_CIRCLEDECORATION_POSITIONY, cfFC_CIRCLEDECORATION_WIDTH, cfFC_CIRCLEDECORATION_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
	
	// Draw Techoration
	DRAW_HACKING_GAME_DECOR(sGameplayData.sBaseStruct, "MPFClone_Decor", cfHG_BG_XPOS, cfHG_BG_YPOS, cfHG_TECHARATION_WIDTH, cfHG_TECHARATION_HEIGHT, sGameplayData.rgbaTintColour )
ENDPROC

PROC CLEAN_UP_FINGERPRINT_CLONE(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, BOOL bFinalCleanup = FALSE, INT iType = HASH("practice"), INT iPlaythroughId = 0)
	IF NOT bFinalCleanup
		HACKING_GAME_UPDATE_TELEMETRY(sControllerStruct, sGameplayData.sBaseStruct, iType, iPlaythroughId, HASH("fingerprint clone"), sGameplayData.iCurrentFingerprintIndex+1)
	ENDIF
	
	HACKING_GAME_CLOSE_LOOPING_SOUNDS(sGameplayData.sBaseStruct, "DLC_H3_Fingerprint_Hack_Scene")
	
	HACKING_COMMON_CLEANUP(sControllerStruct, sGameplayData.sBaseStruct, bFinalCleanup)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Common")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Grid")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Grid1")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Grid2")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_GridDetails")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Decor")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Decor1")
	
	INT i
	TEXT_LABEL_23 tl23
	FOR i = 0 TO ciFC_NUMBER_OF_PRINTS_INGAME -1
		tl23 = "MPFClone_Print"
		tl23 += i
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(tl23)
	ENDFOR
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEIST3/Fingerprint_Match")
ENDPROC

PROC PROCESS_FINGERPRINT_CLONE_EVERY_FRAME_MISC(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	HACKING_GAME_HANDLE_LOOPING_SOUNDS(sGameplayData.sBaseStruct, "DLC_H3_Fingerprint_Hack_Scene")
	
	PROCESS_COMMON_HACKING_EVERY_FRAME_MISC(sGameplayData.sBaseStruct, sControllerStruct)
ENDPROC

// PROCESS STATE FUNCTIONS
PROC PROCESS_FINGERPRINT_CLONE_INIT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_FAILED) // Don't re-enter Init when the game is cleaning up
		EXIT
	ENDIF
	
	FINGERPRINT_MINIGAME_GAMEPLAY_DATA sEmpty
	COPY_SCRIPT_STRUCT(sGameplayData, sEmpty, SIZE_OF(FINGERPRINT_MINIGAME_GAMEPLAY_DATA))
	
	// setup common data
	IF NOT PROCESS_HACKING_GAME_INIT(sControllerStruct, sGameplayData.sBaseStruct)
		EXIT
	ENDIF
	
	FC_RESET_GAMEPLAY_DATA(sGameplayData)
	// Load Common textures for fingerprint
	TEXT_LABEL_23 tl23 = "MPFClone_Common"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][FingerPrintClone] Not Loaded ", tl23)
		EXIT
	ENDIF
	
	tl23 = "MPFClone_Grid"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][FingerPrintClone] Not Loaded ", tl23)
		EXIT
	ENDIF
	
	tl23 = "MPFClone_Grid1"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][FingerPrintClone] Not Loaded ", tl23)
		EXIT
	ENDIF
	
	tl23 = "MPFClone_Grid2"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][FingerPrintClone] Not Loaded ", tl23)
		EXIT
	ENDIF
	
	tl23 = "MPFClone_GridDetails"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][FingerPrintClone] Not Loaded ", tl23)
		EXIT
	ENDIF
	
	tl23 = "MPFClone_Decor"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][FingerPrintClone] Not Loaded ", tl23)
		EXIT
	ENDIF
	
	tl23 = "MPFClone_Decor1"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][FingerPrintClone] Not Loaded ", tl23)
		EXIT
	ENDIF
	 
	// Audio
	sGameplayData.sBaseStruct.sAudioSet = "DLC_H3_Cas_Finger_Minigame_Sounds"
	
	// We only need to grab and assign a sound id if we do not currently have one.
	IF sGameplayData.iProcessingSoundID = -1
		sGameplayData.iProcessingSoundID = GET_SOUND_ID()
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_HEIST3/Fingerprint_Match")
		PRINTLN("[MC][FingerprintClone] Not Loaded DLC_HEIST3/Fingerprint_Match")
		EXIT
	ENDIF
	
	UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_LOAD_LEVEL)
ENDPROC

PROC FINGERPRINT_CLONE_SETUP_HACKER_DETAILS(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	CASINO_HEIST_HACKERS eHacker = GET_PLAYER_CASINO_HEIST_HACKER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		eHacker = CASINO_HEIST_HACKER__AVI_SCHWARTZMAN
		PRINTLN("[MC][FingerprintClone] Hacker for practise session = Seting to Avi ")
	ENDIF
	
	SWITCH eHacker
		CASE CASINO_HEIST_HACKER__NONE
		CASE CASINO_HEIST_HACKER__MAX
			sGameplayData.sBaseStruct.iMaxTime = ciFC_HACK_TIME
			sGameplayData.sBaseStruct.iScrambleTime = ciFC_SCRAMBLE_TIME
			
			sGameplayData.sBaseStruct.iMaxLives = ciFC_MAX_LIVES
			sGameplayData.sBaseStruct.iCurrentLives = ciFC_MAX_LIVES
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 0
			PRINTLN("[MC][FingerprintClone] Setting Hacker to DEFAULT")
		BREAK
		CASE CASINO_HEIST_HACKER__RICKIE_LUKENS
			sGameplayData.sBaseStruct.iMaxTime = 120000
			sGameplayData.sBaseStruct.iScrambleTime = 40000

			sGameplayData.sBaseStruct.iMaxLives = 3
			sGameplayData.sBaseStruct.iCurrentLives = 3
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 40
			PRINTLN("[MC][FingerprintClone] Setting Hacker to Rickie")
		BREAK
		CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ
			sGameplayData.sBaseStruct.iMaxTime = 180000
			sGameplayData.sBaseStruct.iScrambleTime = 70000
			
			sGameplayData.sBaseStruct.iMaxLives = 4
			sGameplayData.sBaseStruct.iCurrentLives = 4
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 25
			PRINTLN("[MC][FingerprintClone] Setting Hacker to Christian")
		BREAK
		CASE CASINO_HEIST_HACKER__YOHAN
			sGameplayData.sBaseStruct.iMaxTime = 180000
			sGameplayData.sBaseStruct.iScrambleTime = 60000
			
			sGameplayData.sBaseStruct.iMaxLives = 5
			sGameplayData.sBaseStruct.iCurrentLives = 5
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 20
			PRINTLN("[MC][FingerprintClone] Setting Hacker to Yohan")
		BREAK
		CASE CASINO_HEIST_HACKER__AVI_SCHWARTZMAN
			sGameplayData.sBaseStruct.iMaxTime = 240000
			sGameplayData.sBaseStruct.iScrambleTime = 110000
			
			sGameplayData.sBaseStruct.iMaxLives = 6
			sGameplayData.sBaseStruct.iCurrentLives = 6
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 5
			PRINTLN("[MC][FingerprintClone] Setting Hacker to Avi")
		BREAK
		CASE CASINO_HEIST_HACKER__PAIGE_HARRIS
			sGameplayData.sBaseStruct.iMaxTime = 240000
			sGameplayData.sBaseStruct.iScrambleTime = 100000
			
			sGameplayData.sBaseStruct.iMaxLives = 6
			sGameplayData.sBaseStruct.iCurrentLives = 6
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 5
			PRINTLN("[MC][FingerprintClone] Setting Hacker to Paige")
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_FINGERPRINT_CLONE_LOAD_LEVEL(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, INT iNumberOfStages)
	HACKING_GAME_SET_NUMBER_OF_PATTERNS(sGameplayData.sBaseStruct, iNumberOfStages)

	PROCESS_HACKING_GAME_LOAD_LEVEL(sControllerStruct)
	
	SETUP_FINGERPRINTS(sGameplayData, sControllerStruct)
	
	// Load textures for finger prints
	IF NOT LOAD_FINGERPRINTS(sGameplayData, sControllerStruct)
		EXIT
	ENDIF
	
	LOAD_BINK_MOVIE(sGameplayData.sBaseStruct, "Intro_FC")
	
	// Start animation time
	REINIT_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
	
	FINGERPRINT_CLONE_SETUP_HACKER_DETAILS(sGameplayData)
	
	UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_STARTUP)
ENDPROC

PROC PROCESS_FINGERPRINT_CLONE_STARTUP(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	// Run the startup process
	PROCESS_HACKING_GAME_STARTUP(sControllerStruct, sGameplayData.sBaseStruct)
	
	IF MAINTAIN_BINK_MOVIE(sGameplayData.sBaseStruct)
	OR HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciFC_STARTUP_TIMER)
		RELEASE_BINK_MOVIE(sGameplayData.sBaseStruct.movieId)
		UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PLAY)
		//UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_VISUAL_TEST)
	ENDIF
ENDPROC

PROC PROCESS_FINGERPRINT_CLONE_PLAY(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, INT iNumberOfPlays, BOOL bShowFinalQuitWarning)
	BOOL bCanInteract = FC_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
	
	PROCESS_HACKING_GAME_PLAY(sControllerStruct, sGameplayData.sBaseStruct, iNumberOfPlays, bCanInteract)
	
	IF bCanInteract
		PROCESS_FINGERPRINT_CLONE_INPUT(sGameplayData)
	ENDIF
	
	PROCESS_FINGERPRINT_CLONE_GAME(sGameplayData, sControllerStruct, bShowFinalQuitWarning)
	
	DRAW_FINGERPRINT_CLONE_GAME(sGameplayData)
	
	MAINTAIN_FINGERPRINT_CLONE_CONTEXT(sGameplayData)
ENDPROC

PROC PROCESS_FINGERPRINT_CLONE_PASS(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	PROCESS_HACKING_GAME_PASS(sControllerStruct, sGameplayData.sBaseStruct, ciFC_END_DELAY, "Success_FC")

ENDPROC

PROC PROCESS_FINGERPRINT_CLONE_FAIL(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	PROCESS_HACKING_GAME_FAIL(sControllerStruct, sGameplayData.sBaseStruct, ciFC_END_DELAY, "Fail_FC")

ENDPROC

PROC PROCESS_FINGERPRINT_CLONE_VISUAL_TEST(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	PROCESS_HACKING_GAME_VISUAL_TEST(sControllerStruct, sGameplayData.sBaseStruct)
	
	IF FC_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		PROCESS_FINGERPRINT_CLONE_INPUT(sGameplayData)
	ENDIF
	
	DRAW_FINGERPRINT_CLONE_GAME(sGameplayData)
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_FINGERPINT_CLONE_WIDGETS(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	IF NOT sGameplayData.sBaseStruct.bDebugCreatedWidgets
		PRINTLN("[MC][FingerprintCloneDebug] - Widgets not made ")
		
		
		sGameplayData.sBaseStruct.widgetFingerprintClone = GET_ID_OF_TOP_LEVEL_WIDGET_GROUP()
		SET_CURRENT_WIDGET_GROUP(sGameplayData.sBaseStruct.widgetFingerprintClone)
		START_WIDGET_GROUP("FingerPrintClone")
			
			// Game Values
			ADD_WIDGET_STRING("Game Values")
			
			ADD_WIDGET_INT_SLIDER("Lives:", sGameplayData.sBaseStruct.iDebugLivesToSet, 1, 6, 1 )
			
			ADD_WIDGET_FLOAT_SLIDER("Time:", sGameplayData.sBaseStruct.fDebugTimeToSet, 1.0, 10.0, 0.5 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble Time:", sGameplayData.sBaseStruct.fDebugScrambleTimeToSet, 10.0, 500.0, 1.0 )
			ADD_WIDGET_INT_SLIDER("Scramble Wipe Chance:", sGameplayData.sBaseStruct.iScrambleWipeChanceToSet, 0, 100, 1 )
			
			ADD_WIDGET_BOOL("RESET GAME", sGameplayData.sBaseStruct.bDebugReset)
			// Positioning Values
//			ADD_WIDGET_STRING("Debug Values")
//			ADD_WIDGET_FLOAT_SLIDER("Background X", cfHG_BG_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Background Y", cfHG_BG_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Background Width", cfHG_BG_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Background Height", cfHG_BG_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Play Area Width", cfHG_PLAYAREA_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Play Area Height", cfHG_PLAYAREA_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Techaration Width", cfHG_TECHARATION_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Techaration Height", cfHG_TECHARATION_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 X", cfHG_tech1_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 Y", cfHG_tech1_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 Width", cfHG_tech1_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 Height", cfHG_tech1_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 X", cfHG_tech2_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 Y", cfHG_tech2_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 Width", cfHG_tech2_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 Height", cfHG_tech2_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 X", cfHG_tech3_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 Y", cfHG_tech3_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 Width", cfHG_tech3_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 Height", cfHG_tech3_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 X", cfHG_tech4_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 Y", cfHG_tech4_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 Width", cfHG_tech4_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 Height", cfHG_tech4_HEIGHT, 0.0, 2000.0, 10.0 )
//			
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintColumn 1", cfFC_FINGERPRINT_ELEMENT_COLUMN1, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintColumn 2", cfFC_FINGERPRINT_ELEMENT_COLUMN2, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 1", cfFC_FINGERPRINT_ELEMENT_ROW1, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 2", cfFC_FINGERPRINT_ELEMENT_ROW2, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 3", cfFC_FINGERPRINT_ELEMENT_ROW3, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 4", cfFC_FINGERPRINT_ELEMENT_ROW4, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintElement Width", cfFC_FINGERPRINT_ELEMENT_WIDTH, 0.0, 2000.0, 10.0 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintElement Height", cfFC_FINGERPRINT_ELEMENT_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge X", cfFC_FINGERPRINT_LARGE_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge Y", cfFC_FINGERPRINT_LARGE_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge Width", cfFC_FINGERPRINT_LARGE_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge Height", cfFC_FINGERPRINT_LARGE_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Left X", cfFC_GRID_DETAILS_LEFTX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Right X", cfFC_GRID_DETAILS_RIGHTX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Width", cfFC_GRID_DETAILS_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Height", cfFC_GRID_DETAILS_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise X", cfFC_GRID_NOISE_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise Y", cfFC_GRID_NOISE_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise Width", cfFC_GRID_NOISE_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise Height", cfFC_GRID_NOISE_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage X", cfHG_STATUS_MESSAGE_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage Y", cfHG_STATUS_MESSAGE_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage Width", cfHG_STATUS_MESSAGE_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage Height", cfHG_STATUS_MESSAGE_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Results Loading Initial X", cfHG_RESULTS_LOADING_INITIALX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Results Loading Diff X", cfHG_RESULTS_LOADING_XDIFF, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Results Loading Y", cfHG_RESULTS_LOADING_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Results Loading Height", cfHG_RESULTS_LOADING_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Results Loading Width", cfHG_RESULTS_LOADING_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_INT_SLIDER("Results Loading Max Segments", ciHG_RESULTS_LOADING_MAX_SEGMENTS, 0, 100, 1)
//			
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Width", cfFC_FINGERPRINT_TRACKER_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Height", cfFC_FINGERPRINT_TRACKER_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Width", cfFC_FINGERPRINT_TRACKER_SELECTOR_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Height", cfFC_FINGERPRINT_TRACKER_SELECTOR_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Y", cfFC_FINGERPRINT_TRACKER_POSITIONY, 0.0, 2000.0, 0.01 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker1 X", cfFC_FINGERPRINT_TRACKER1_POSITIONX, 0.0, 2000.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker2 X", cfFC_FINGERPRINT_TRACKER2_POSITIONX, 0.0, 2000.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker3 X", cfFC_FINGERPRINT_TRACKER3_POSITIONX, 0.0, 2000.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker4 X", cfFC_FINGERPRINT_TRACKER4_POSITIONX, 0.0, 2000.0, 0.01 )
			
//			ADD_WIDGET_FLOAT_SLIDER("Timer X", cfFC_TIMER_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Timer Y", cfFC_TIMER_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Timer Width", cfHG_TIMER_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Timer Height", cfHG_TIMER_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Timer Digit Difference", cfFC_TIMER_DIGIT_DIFFERENCE, 0.0, 2000.0, 0.01 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Lives X", cfFC_LIVES_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Lives Y", cfFC_LIVES_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Lives Width", cfHG_LIVES_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Lives Height", cfHG_LIVES_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Lives Offset", cfFC_LIVES_OFFSET, 0.0, 2000.0, 0.01 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration X", cfFC_CIRCLEDECORATION_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration Y", cfFC_CIRCLEDECORATION_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration Width", cfFC_CIRCLEDECORATION_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration Height", cfFC_CIRCLEDECORATION_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Scramble X", cfFC_SCRAMBLE_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble Y", cfFC_SCRAMBLE_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble BG Width", cfHG_SCRAMBLE_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble BG Height", cfHG_SCRAMBLE_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble Element Width", cfHG_SCRAMBLE_ELEMENT_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble Element Height", cfHG_SCRAMBLE_ELEMENT_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble Element Y Offset", cfHG_SCRAMBLE_Y_OFFSET, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble Left Most", cfFC_SCRAMBLE_LEFTPOINT, -1.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble Difference", cfHG_SCRAMBLE_DIFFBETWEENSEGMENT, 0.0, 2000.0, 0.01 )
			
//			ADD_WIDGET_FLOAT_SLIDER("Default Anim Speed", cfHG_DEFAULT_ANIM_FRAME_TIME, 0.0, 500.0, 10.00 )
//			ADD_WIDGET_FLOAT_SLIDER("Slow Anim Speed", cfHG_SLOW_ANIM_FRAME_TIME, 0.0, 500.0, 10.00 )
//			ADD_WIDGET_FLOAT_SLIDER("Fast Anim Speed", cfHG_FAST_ANIM_FRAME_TIME, 0.0, 500.0, 10.00 )

			ADD_WIDGET_INT_SLIDER("Overlay Alpha", ciHG_OVERLAY_ALPHA, 0, 255, 10)
			ADD_WIDGET_INT_SLIDER("Overlay R", ciHG_OVERLAY_R, 0, 255, 10)
			ADD_WIDGET_INT_SLIDER("Overlay G", ciHG_OVERLAY_G, 0, 255, 10)
			ADD_WIDGET_INT_SLIDER("Overlay B", ciHG_OVERLAY_B, 0, 255, 10)
			
			ADD_WIDGET_INT_SLIDER("Shadow Alpha", ciHG_SHADOW_ALPHA, 0, 255, 10)
			
			ADD_WIDGET_BOOL("Hide Overlays", sGameplayData.sBaseStruct.bDebugHideOverlay)
			ADD_WIDGET_BOOL("Hide Shadow Layer", sGameplayData.sBaseStruct.bDebugHideShadow)
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(sGameplayData.sBaseStruct.widgetFingerprintClone)
		sGameplayData.sBaseStruct.bDebugCreatedWidgets = TRUE
	ENDIF
ENDPROC


/// DEBUG
PROC PROCESS_FINGERPINT_CLONE_DEBUG(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	
	IF NOT sGameplayData.sBaseStruct.bDebugCreatedWidgets
		PROCESS_FINGERPINT_CLONE_WIDGETS(sGameplayData)
	ENDIF
	
	IF NOT FC_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		DRAW_DEBUG_TEXT_2D("INTERACTION LOCKED", NULL_VECTOR(), 255, 0, 0)
	ENDIF
	
	IF sGameplayData.sBaseStruct.bDebugReset
		sGameplayData.sBaseStruct.iMaxTime = FLOOR(sGameplayData.sBaseStruct.fDebugTimeToSet * 60000)
		sGameplayData.sBaseStruct.iScrambleTime = FLOOR(sGameplayData.sBaseStruct.fDebugScrambleTimeToSet * 1000)
		sGameplayData.sBaseStruct.iCurrentLives = sGameplayData.sBaseStruct.iDebugLivesToSet
		RESET_NET_TIMER(sGameplayData.sBaseStruct.tdTimer)
		sGameplayData.sBaseStruct.iScrambleWipeChance = sGameplayData.sBaseStruct.iScrambleWipeChanceToSet
		sGameplayData.sBaseStruct.bDebugReset = FALSE
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_SHIFT, "")
		PRINTLN("[MC][FingerprintClone] - PROCESS_FINGERPINT_CLONE_DEBUG - J SKIP - Going to HACKING_GAME_PASS")
		UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PASS)
	ENDIF
	
	IF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_PLAY
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			PRINTLN("[MC][FingerprintClone] - PROCESS_FINGERPINT_CLONE_DEBUG - J SKIP - Going to HACKING_GAME_VISUAL_TEST")
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_FAILED_FINGERPRINT)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_PASSING_FINGERPRINT)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_SCRAMBLING)
			UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_VISUAL_TEST)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			sGameplayData.sBaseStruct.bDebugShowGuide = !sGameplayData.sBaseStruct.bDebugShowGuide
		ENDIF
	ELIF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			PRINTLN("[MC][FingerprintClone] - PROCESS_FINGERPINT_CLONE_DEBUG - J SKIP - Going to HACKING_GAME_PLAY")
			sGameplayData.iCurrentFingerprintIndex = 0
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_FAILED_FINGERPRINT)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_PASSING_FINGERPRINT)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_SCRAMBLING)
			UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PLAY)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_CTRL, "")
			SET_BIT(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
			SET_BIT(sGameplayData.iF_Bs, ciFC_PASSING_FINGERPRINT)
		ENDIF

		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_SHIFT, "")
			SET_BIT(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
			SET_BIT(sGameplayData.iF_Bs, ciFC_FAILED_FINGERPRINT)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_ALT, "")
			SET_BIT(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
			SET_BIT(sGameplayData.iF_Bs, ciFC_SHOWING_LOADING)
		ENDIF

		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_CHECKING_MATCH)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_FAILED_FINGERPRINT)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFC_PASSING_FINGERPRINT)
			SET_BIT(sGameplayData.iF_Bs, ciFC_SHOWING_LOADING)
		ENDIF
	ENDIF
ENDPROC
#ENDIF
///
