USING "globals.sch"
USING "net_include.sch"
USING "net_ambience.sch"
USING "net_cutscene.sch"
USING "heist_island_travel.sch"

#IF FEATURE_HEIST_ISLAND

FUNC STRING GET_BEACH_SCENE_ANIM_DICT()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
		RETURN "anim@scripted@heist@ig25_beach@heeled@"
	ENDIF
	
	RETURN "anim@scripted@heist@ig25_beach@male@"
ENDFUNC

PROC GET_BEACH_SPAWN_POSITION_AND_ROTATION(VECTOR &vScenePosition, VECTOR &vSceneRotation)
	SWITCH NATIVE_TO_INT(PLAYER_ID())
		CASE 0
			vScenePosition = <<-1354.9652, -1673.7905, 1.1368>>
			vSceneRotation = <<0.0, 0.0, 306.0548>>
		BREAK
		
		CASE 1
			vScenePosition = <<-1409.1841, -1574.7614, 1.1402>>
			vSceneRotation = <<0.0, 0.0, 289.8647>>
		BREAK
		
		CASE 2
			vScenePosition = <<-1559.3306, -1230.8384, 0.6728>>
			vSceneRotation = <<0.0, 0.0, 322.3736>>
		BREAK
		
		CASE 3
			vScenePosition = <<-1545.2552, -1251.8491, 1.0106>>
			vSceneRotation = <<0.0, 0.0, 297.9889>>
		BREAK
		
		CASE 4
			vScenePosition = <<-1516.4100, -1325.6504, 1.0904>>
			vSceneRotation = <<0.0, 0.0, 277.6357>>
		BREAK
		
		CASE 5
			vScenePosition = <<-1499.7025, -1390.0171, 0.9719>>
			vSceneRotation = <<0.0, 0.0, 279.5217>>
		BREAK
		
		CASE 6
			vScenePosition = <<-1358.9540, -1672.3572, 1.0133>>
			vSceneRotation = <<0.0, 0.0, 309.5682>>
		BREAK
		
		CASE 7
			vScenePosition = <<-1410.6760, -1569.9869, 1.1454>>
			vSceneRotation = <<0.0, 0.0, 297.4637>>
		BREAK
		
		CASE 8
			vScenePosition = <<-1554.6614, -1231.7534, 0.8390>>
			vSceneRotation = <<0.0, 0.0, 304.6690>>
		BREAK
		
		CASE 9
			vScenePosition = <<-1540.6963, -1252.5114, 1.1390>>
			vSceneRotation = <<0.0, 0.0, 300.8615>>
		BREAK
		
		CASE 10
			vScenePosition = <<-1516.4111, -1330.9752, 1.0092>>
			vSceneRotation = <<0.0, 0.0, 281.3881>>
		BREAK
		
		CASE 11
			vScenePosition = <<-1495.7513, -1393.8546, 1.0815>>
			vSceneRotation = <<0.0, 0.0, 281.7297>>
		BREAK
		
		CASE 12
			vScenePosition = <<-1359.6281, -1677.5006, 0.8238>>
			vSceneRotation = <<0.0, 0.0, 318.1155>>
		BREAK
		
		CASE 13
			vScenePosition = <<-1405.8538, -1575.2864, 1.1261>>
			vSceneRotation = <<0.0, 0.0, 288.6711>>
		BREAK
		
		CASE 14
			vScenePosition = <<-1562.0005, -1223.6470, 0.5650>>
			vSceneRotation = <<0.0, 0.0, 320.5948>>
		BREAK
		
		CASE 15
			vScenePosition = <<-1539.2661, -1255.4816, 1.1242>>
			vSceneRotation = <<0.0, 0.0, 303.7138>>
		BREAK
		
		CASE 16
			vScenePosition = <<-1511.4783, -1327.9312, 1.1332>>
			vSceneRotation = <<0.0, 0.0, 286.6746>>
		BREAK
		
		CASE 17
			vScenePosition = <<-1498.9115, -1378.2701, 1.1446>>
			vSceneRotation = <<0.0, 0.0, 283.8162>>
		BREAK
		
		CASE 18
			vScenePosition = <<-1353.5332, -1682.5919, 0.9084>>
			vSceneRotation = <<0.0, 0.0, 300.5188>>
		BREAK
		
		CASE 19
			vScenePosition = <<-1402.4167, -1579.1233, 1.1274>>
			vSceneRotation = <<0.0, 0.0, 294.6224>>
		BREAK
		
		CASE 20
			vScenePosition = <<-1558.3270, -1215.4817, 0.4664>>
			vSceneRotation = <<0.0, 0.0, 319.2206>>
		BREAK
		
		CASE 21
			vScenePosition = <<-1542.0851, -1261.2416, 0.7790>>
			vSceneRotation = <<0.0, 0.0, 303.8786>>
		BREAK
		
		CASE 22
			vScenePosition = <<-1514.1035, -1334.8950, 1.0671>>
			vSceneRotation = <<0.0, 0.0, 281.1109>>
		BREAK
		
		CASE 23
			vScenePosition = <<-1490.7827, -1379.2446, 1.1513>>
			vSceneRotation = <<0.0, 0.0, 285.7036>>
		BREAK
		
		CASE 24
			vScenePosition = <<-1348.1721, -1681.5348, 1.1446>>
			vSceneRotation = <<0.0, 0.0, 304.7736>>
		BREAK
		
		CASE 25
			vScenePosition = <<-1398.2063, -1575.2065, 1.1370>>
			vSceneRotation = <<0.0, 0.0, 302.1246>>
		BREAK
		
		CASE 26
			vScenePosition = <<-1547.6256, -1227.2959, 0.9341>>
			vSceneRotation = <<0.0, 0.0, 288.4829>>
		BREAK
		
		CASE 27
			vScenePosition = <<-1547.8676, -1253.2101, 0.8150>>
			vSceneRotation = <<0.0, 0.0, 298.5717>>
		BREAK
		
		CASE 28
			vScenePosition = <<-1517.5437, -1315.8308, 1.0904>>
			vSceneRotation = <<0.0, 0.0, 289.7935>>
		BREAK
		
		CASE 29
			vScenePosition = <<-1487.7046, -1384.9109, 1.1528>>
			vSceneRotation = <<0.0, 0.0, 286.6581>>
		BREAK
		
		CASE 30
			vScenePosition = <<-1488.6250, -1391.9922, 1.1493>>
			vSceneRotation = <<0.0, 0.0, 288.3503>>
		BREAK
		
		CASE 31
			vScenePosition = <<-1486.9122, -1403.3689, 1.0622>>
			vSceneRotation = <<0.0, 0.0, 263.2072>>
		BREAK
		
		DEFAULT
			vScenePosition = <<-1354.9652, -1673.7905, 1.1368>>
			vSceneRotation = <<0.0, 0.0, 306.0548>>
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEANUP_ISLAND_HEIST_CUTSCENE(BOOL bFadeScreenOut = FALSE)
	IF bFadeScreenOut
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	
	IF IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
	ENDIF
	
	g_sIslandHeistCutsceneData.eCutscene = ISLAND_HEIST_CUTSCENE_INVALID
	
	g_sIslandHeistCutsceneData.iBS = 0
	g_sIslandHeistCutsceneData.iSyncSceneID = -1
	g_sIslandHeistCutsceneData.iCutsceneState = 0
	
	REMOVE_ANIM_DICT(GET_BEACH_SCENE_ANIM_DICT())
	
	IF DOES_CAM_EXIST(g_sIslandHeistCutsceneData.cutsceneCam)
		DESTROY_CAM(g_sIslandHeistCutsceneData.cutsceneCam)
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		CLEANUP_MP_CUTSCENE(FALSE, FALSE)
		
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()
	ENDIF
ENDPROC

PROC ISLAND_HEIST_CUTSCENE_HIDE_UI()
	SET_DISABLE_DECAL_RENDERING_THIS_FRAME()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_HELP_TEXT_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	
	IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		CLEAR_ALL_BIG_MESSAGES()
	ENDIF
ENDPROC

PROC GET_ISLAND_HEIST_CUTSCENE_COORDS_AND_HEADING(INT iIndex, VECTOR &vCoords, FLOAT &fHeading)
	SWITCH iIndex
		CASE 0
	        vCoords = <<4361.5820, -4559.3950, 4.1317>>
	        fHeading = 19.5993
	    BREAK
		
	    CASE 1
	        vCoords = <<4475.2813, -4520.8789, 3.9448>>
	        fHeading = 192.7989
	    BREAK
		
	    CASE 2
	        vCoords = <<4646.3984, -4473.2271, 4.5552>>
	        fHeading = 281.7984
	    BREAK
		
	    CASE 3
	        vCoords = <<4878.0645, -4459.6138, 7.9854>>
	        fHeading = 348.3983
	    BREAK
		
	    CASE 4
	        vCoords = <<5115.5566, -5284.3604, 6.2727>>
	        fHeading = 91.5980
	    BREAK
		
	    CASE 5
	        vCoords = <<5060.5845, -4627.2954, 2.5101>>
	        fHeading = 61.0849
	    BREAK
		
	    CASE 6
	        vCoords = <<5161.6699, -4631.6069, 2.5816>>
	        fHeading = 338.4838
	    BREAK
		
	    CASE 7
	        vCoords = <<4992.8223, -4801.9136, 13.9063>>
	        fHeading = 11.8838
	    BREAK
		
	    CASE 8
	        vCoords = <<5080.1499, -4899.5654, 17.0082>>
	        fHeading = 59.8834
	    BREAK
		
	    CASE 9
	        vCoords = <<5164.0400, -4969.7314, 13.7783>>
	        fHeading = 19.8700
	    BREAK
		
	    CASE 10
	        vCoords = <<5175.7339, -5113.2412, 3.0140>>
	        fHeading = 319.8700
	    BREAK
		
	    CASE 11
	        vCoords = <<5321.4717, -5248.9561, 32.5054>>
	        fHeading = 31.2697
	    BREAK
		
	    CASE 12
	        vCoords = <<5553.6343, -5232.3237, 13.7967>>
	        fHeading = 114.8693
	    BREAK
		
	    CASE 13
	        vCoords = <<5341.9097, -5488.3457, 53.1917>>
	        fHeading = 4.6692
	    BREAK
		
	    CASE 14
	        vCoords = <<5476.3662, -5829.7993, 18.9210>>
	        fHeading = 104.4690
	    BREAK
		
	    CASE 15
	        vCoords = <<4980.4785, -5590.7114, 24.1616>>
	        fHeading = 91.7456
	    BREAK
		
	    CASE 16
	        vCoords = <<4942.0298, -5284.7407, 4.4804>>
	        fHeading = 354.2434
	    BREAK
		
	    CASE 17
	        vCoords = <<4991.8325, -5178.8687, 2.4199>>
	        fHeading = 249.2431
	    BREAK
		
	    CASE 18
	        vCoords = <<5165.1416, -5492.8540, 49.5470>>
	        fHeading = 63.8426
	    BREAK
		
	    CASE 19
	        vCoords = <<4614.9214, -4785.2109, 11.6582>>
	        fHeading = 166.2186
	    BREAK
	ENDSWITCH
ENDPROC

PROC GET_CLOSEST_SAFE_CUTSCENE_COORDS_AND_HEADING(VECTOR &vCoords, FLOAT &fHeading)
	INT iCurrentClosest
	
	FLOAT fDistance = 10000.0
	FLOAT fTempHeading
	
	VECTOR vTempCoords
	VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
	
	INT i
	REPEAT 20 i
		GET_ISLAND_HEIST_CUTSCENE_COORDS_AND_HEADING(i, vTempCoords, fTempHeading)
		
		FLOAT fTempDistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vTempCoords)
		
		IF fTempDistance < fDistance
			fDistance = fTempDistance
			
			iCurrentClosest = i
		ENDIF
	ENDREPEAT
	
	GET_ISLAND_HEIST_CUTSCENE_COORDS_AND_HEADING(iCurrentClosest, vCoords, fHeading)
ENDPROC

PROC MAINTAIN_KICKED_FROM_BEACH_SCENE()
	INT iPedCount
	
	PED_INDEX PedArray[5]
	
	SWITCH g_sIslandHeistCutsceneData.iCutsceneState
		CASE 0
			REQUEST_CUTSCENE("HS4_SCP_KNCK")
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
				
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("Island_Guard", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 1
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_OUT()
	            DO_SCREEN_FADE_OUT(0)
			ELIF IS_SCREEN_FADED_OUT()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					iPedCount = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), PedArray, PEDTYPE_PLAYER_NETWORK)
					
					INT i
					REPEAT iPedCount i
						IF DOES_ENTITY_EXIST(PedArray[i])
							IF GET_ENTITY_MODEL(PedArray[i]) = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_m_cartelguards_01"))
							OR GET_ENTITY_MODEL(PedArray[i]) = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_m_cartelguards_02"))
								g_sIslandHeistCutsceneData.pedGuard = CLONE_PED(PedArray[i], FALSE, FALSE, FALSE)
								
								i = iPedCount
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 2
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_OUT()
	            DO_SCREEN_FADE_OUT(0)
			ELIF HAS_CUTSCENE_LOADED()
			AND IS_SCREEN_FADED_OUT()
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					
					IF IS_ENTITY_ALIVE(g_sIslandHeistCutsceneData.pedGuard)
						REGISTER_ENTITY_FOR_CUTSCENE(g_sIslandHeistCutsceneData.pedGuard, "Island_Guard", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					ELSE
						IF NOT HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GB_GET_LOCAL_PLAYER_MISSION_HOST(), IHIT_ARMOUR_SHIPMENTS)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard", PED_COMP_SPECIAL2, 1, 0)
						ELSE
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard", PED_COMP_SPECIAL2, 0, 0)
						ENDIF
					ENDIF
					
					SET_CUTSCENE_ORIGIN(<<5049.548, -4854.188, 3.8519>>, 135.0, 0)
					SET_CUTSCENE_ORIGIN(<<5049.548, -4854.188, 3.8519>>, 135.0, 1)
					
					START_CUTSCENE()
					START_MP_CUTSCENE()
					
					NETWORK_SET_VOICE_ACTIVE(FALSE)
					
					SET_ENTITY_VISIBLE_IN_CUTSCENE(PLAYER_PED_ID(), TRUE)
					
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF IS_CUTSCENE_PLAYING()
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 4
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			AND (GET_CUTSCENE_TOTAL_DURATION() - GET_CUTSCENE_TIME()) <= 600
				DO_SCREEN_FADE_OUT(500)
				
				SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				CLEANUP_MP_CUTSCENE(FALSE, FALSE)
				
				STOP_CUTSCENE_IMMEDIATELY()
				REMOVE_CUTSCENE()
				
				IF DOES_ENTITY_EXIST(g_sIslandHeistCutsceneData.pedGuard)
					DELETE_PED(g_sIslandHeistCutsceneData.pedGuard)
				ENDIF
				
				HEIST_ISLAND_TRAVEL__LEAVE_ISLAND(HEIST_ISLAND_LEAVE_FADE_OUT)
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 5
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF HEIST_ISLAND_TRAVEL__IS_TRANSITION_FINISHED()
					g_sIslandHeistCutsceneData.iSpawnLocation = GET_RANDOM_INT_IN_RANGE(0, 6)
					
					VECTOR vSpawnCoords
					VECTOR vSpawnRotation
					
					GET_BEACH_SPAWN_POSITION_AND_ROTATION(vSpawnCoords, vSpawnRotation)
					
					SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vSpawnCoords)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), vSpawnRotation.z)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
				IF NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0)
					SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
					
					REINIT_NET_TIMER(g_sIslandHeistCutsceneData.stLoadSceneTimer)
				ENDIF
			ELSE
				REQUEST_ANIM_DICT(GET_BEACH_SCENE_ANIM_DICT())
				
				IF (NOT IS_NEW_LOAD_SCENE_ACTIVE()
				OR IS_NEW_LOAD_SCENE_LOADED()
				OR HAS_NET_TIMER_EXPIRED(g_sIslandHeistCutsceneData.stLoadSceneTimer, 15000))
				AND HAS_ANIM_DICT_LOADED(GET_BEACH_SCENE_ANIM_DICT())
					CLEAR_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
					
					RESET_NET_TIMER(g_sIslandHeistCutsceneData.stLoadSceneTimer)
					
					NEW_LOAD_SCENE_STOP()
					
					VECTOR vScenePosition
					VECTOR vSceneRotation
					
					GET_BEACH_SPAWN_POSITION_AND_ROTATION(vScenePosition, vSceneRotation)
					
					g_sIslandHeistCutsceneData.iSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePosition, vSceneRotation, DEFAULT, TRUE)
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), g_sIslandHeistCutsceneData.iSyncSceneID, GET_BEACH_SCENE_ANIM_DICT(), "action", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, REALLY_SLOW_BLEND_IN)
					
					NETWORK_START_SYNCHRONISED_SCENE(g_sIslandHeistCutsceneData.iSyncSceneID)
					
					g_sIslandHeistCutsceneData.cutsceneCam = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
					
					PLAY_CAM_ANIM(g_sIslandHeistCutsceneData.cutsceneCam, "action_camera", GET_BEACH_SCENE_ANIM_DICT(), vScenePosition, vSceneRotation)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(g_sIslandHeistCutsceneData.iSyncSceneID) = -1
			OR GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(g_sIslandHeistCutsceneData.iSyncSceneID)) >= 0.1
				DO_SCREEN_FADE_IN(500)
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 8
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			VECTOR vLeftInput
			vLeftInput = <<GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y), 0.0>>
			
			IF NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(g_sIslandHeistCutsceneData.iSyncSceneID) = -1
			OR GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(g_sIslandHeistCutsceneData.iSyncSceneID)) >= 0.99
			OR ((VMAG(vLeftInput) >= 0.24) AND HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BREAKOUT")))
				NETWORK_SET_VOICE_ACTIVE(TRUE)
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				IF DOES_CAM_EXIST(g_sIslandHeistCutsceneData.cutsceneCam)
				AND IS_CAM_ACTIVE(g_sIslandHeistCutsceneData.cutsceneCam)
					RENDER_SCRIPT_CAMS(FALSE, TRUE, 1000)
				ENDIF
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ELIF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BLENDOUT_CAM"))
			AND DOES_CAM_EXIST(g_sIslandHeistCutsceneData.cutsceneCam)
			AND IS_CAM_ACTIVE(g_sIslandHeistCutsceneData.cutsceneCam)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				RENDER_SCRIPT_CAMS(FALSE, TRUE, 1000)
			ENDIF
		BREAK
		
		CASE 9
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			CLEANUP_ISLAND_HEIST_CUTSCENE()
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_CAUGHT_SCOPING_DJ_SCENE()
	IF HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
		CLEANUP_ISLAND_HEIST_CUTSCENE(TRUE)
		EXIT
	ENDIF
	
	INT iPedCount
	
	PED_INDEX PedArray[5]
	
	SWITCH g_sIslandHeistCutsceneData.iCutsceneState
		CASE 0
			REQUEST_CUTSCENE("HS4_SCP_KNCK")
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
				
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("Island_Guard", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 0 - cutscene requested and assets registered")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 1
			IF IS_SCREEN_FADED_OUT()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					iPedCount = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), PedArray, PEDTYPE_PLAYER_NETWORK)
					
					INT i
					REPEAT iPedCount i
						IF DOES_ENTITY_EXIST(PedArray[i])
							IF GET_ENTITY_MODEL(PedArray[i]) = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_m_cartelguards_01"))
							OR GET_ENTITY_MODEL(PedArray[i]) = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_m_cartelguards_02"))
								g_sIslandHeistCutsceneData.pedGuard = CLONE_PED(PedArray[i], FALSE, FALSE, FALSE)
								
								i = iPedCount
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 1 - found guard ped")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 2
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 2 - cutscene loading...")
			
			IF HAS_CUTSCENE_LOADED()
			AND IS_SCREEN_FADED_OUT()
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					FLOAT fSceneHeading
					
					VECTOR vScenePos
					
					GET_CLOSEST_SAFE_CUTSCENE_COORDS_AND_HEADING(vScenePos, fSceneHeading)
					
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					
					IF IS_ENTITY_ALIVE(g_sIslandHeistCutsceneData.pedGuard)
						REGISTER_ENTITY_FOR_CUTSCENE(g_sIslandHeistCutsceneData.pedGuard, "Island_Guard", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					ELSE
						IF NOT HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GB_GET_LOCAL_PLAYER_MISSION_HOST(), IHIT_ARMOUR_SHIPMENTS)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard", PED_COMP_SPECIAL2, 1, 0)
						ELSE
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard", PED_COMP_SPECIAL2, 0, 0)
						ENDIF
					ENDIF
					
					SET_CUTSCENE_ORIGIN(vScenePos + ROTATE_VECTOR_ABOUT_Z(<<-55.3191, -101.9515, -1.1358>>, fSceneHeading), fSceneHeading, 0)
					SET_CUTSCENE_ORIGIN(vScenePos + ROTATE_VECTOR_ABOUT_Z(<<-55.3191, -101.9515, -1.1358>>, fSceneHeading), fSceneHeading, 1)
					
					START_CUTSCENE()
					START_MP_CUTSCENE()
					
					NETWORK_SET_VOICE_ACTIVE(FALSE)
					
					SET_ENTITY_VISIBLE_IN_CUTSCENE(PLAYER_PED_ID(), TRUE)
					
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 2 - cutscene loaded, starting")
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF IS_CUTSCENE_PLAYING()
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 3 - cutscene playing")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 4
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			AND (GET_CUTSCENE_TOTAL_DURATION() - GET_CUTSCENE_TIME()) <= 600
				DO_SCREEN_FADE_OUT(500)
				
				SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				CLEANUP_MP_CUTSCENE(FALSE, FALSE)
				
				IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				STOP_CUTSCENE_IMMEDIATELY()
				REMOVE_CUTSCENE()
				
				IF DOES_ENTITY_EXIST(g_sIslandHeistCutsceneData.pedGuard)
					DELETE_PED(g_sIslandHeistCutsceneData.pedGuard)
				ENDIF
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 4 - cutscene finished")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 5
			REQUEST_CUTSCENE("HS4_SCP_PUSH")
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				
				IF NOT HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GB_GET_LOCAL_PLAYER_MISSION_HOST(), IHIT_ARMOUR_SHIPMENTS)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard1", PED_COMP_SPECIAL2, 0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard2", PED_COMP_SPECIAL2, 0, 0)
				ELSE
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard1", PED_COMP_SPECIAL2, 1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard2", PED_COMP_SPECIAL2, 1, 0)
				ENDIF
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 5 - cutscene requested")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 6
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 6 - cutscene loading...")
			
			IF HAS_CUTSCENE_LOADED()
				IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
					IF NEW_LOAD_SCENE_START_SPHERE(<<5009.5947, -4923.6978, 8.9134>>, 50.0)
						SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
						
						REINIT_NET_TIMER(g_sIslandHeistCutsceneData.stLoadSceneTimer)
					ENDIF
				ELSE
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					OR IS_NEW_LOAD_SCENE_LOADED()
					OR HAS_NET_TIMER_EXPIRED(g_sIslandHeistCutsceneData.stLoadSceneTimer, 15000)
						NEW_LOAD_SCENE_STOP()
						
						RESET_NET_TIMER(g_sIslandHeistCutsceneData.stLoadSceneTimer)
						
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						AND NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
							
							IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
							ENDIF
							
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
							
							SET_CUTSCENE_ORIGIN(<<5025.9997, -4839.2012, 6.5534>>, 155.0, 0)
							
							START_CUTSCENE()
							START_MP_CUTSCENE()
							
							NETWORK_SET_VOICE_ACTIVE(FALSE)
							
							SET_ENTITY_VISIBLE_IN_CUTSCENE(PLAYER_PED_ID(), TRUE)
							
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							
							PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 6 - cutscene started")
							g_sIslandHeistCutsceneData.iCutsceneState++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF IS_CUTSCENE_PLAYING()
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
					
					CLEAR_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
				ENDIF
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 7 - cutscene playing")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 8
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			AND (GET_CUTSCENE_TOTAL_DURATION() - GET_CUTSCENE_TIME()) <= 600
				DO_SCREEN_FADE_OUT(500)
				
				SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				CLEANUP_MP_CUTSCENE(FALSE, FALSE)
				
				STOP_CUTSCENE_IMMEDIATELY()
				REMOVE_CUTSCENE()
				
				CLEAR_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 8 - cutscene finished")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 9
			IF g_SpawnData.bUseCustomSpawnPoints
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
					DO_SCREEN_FADE_IN(500)
					
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_WARPED_TO_SAFE_LOCATION)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<5012.318, -4922.874, 9.189>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 306.400)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_WARPED_TO_SAFE_LOCATION)
				ELSE
					DO_SCREEN_FADE_IN(500)
					
					PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 9 - player moved")
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 10
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			CLEANUP_ISLAND_HEIST_CUTSCENE()
			PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_DJ_SCENE - 10 - cutscene finished")
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE()
	IF HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
		CLEANUP_ISLAND_HEIST_CUTSCENE(TRUE)
		EXIT
	ENDIF
	
	INT iPedCount
	
	PED_INDEX PedArray[5]
	
	SWITCH g_sIslandHeistCutsceneData.iCutsceneState
		CASE 0
			REQUEST_CUTSCENE("HS4_SCP_KNCK")
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
				
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("Island_Guard", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE - 0 - cutscene requested")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 1
			IF IS_SCREEN_FADED_OUT()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					iPedCount = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), PedArray, PEDTYPE_PLAYER_NETWORK)
					
					INT i
					REPEAT iPedCount i
						IF DOES_ENTITY_EXIST(PedArray[i])
							IF GET_ENTITY_MODEL(PedArray[i]) = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_m_cartelguards_01"))
							OR GET_ENTITY_MODEL(PedArray[i]) = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_m_cartelguards_02"))
								g_sIslandHeistCutsceneData.pedGuard = CLONE_PED(PedArray[i], FALSE, FALSE, FALSE)
								
								i = iPedCount
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE - 1 - grabbed gaurd ped")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 2
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF HAS_CUTSCENE_LOADED()
			AND IS_SCREEN_FADED_OUT()
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					FLOAT fSceneHeading
					
					VECTOR vScenePos
					
					GET_CLOSEST_SAFE_CUTSCENE_COORDS_AND_HEADING(vScenePos, fSceneHeading)
					
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					
					IF IS_ENTITY_ALIVE(g_sIslandHeistCutsceneData.pedGuard)
						REGISTER_ENTITY_FOR_CUTSCENE(g_sIslandHeistCutsceneData.pedGuard, "Island_Guard", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					ELSE
						IF NOT HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GB_GET_LOCAL_PLAYER_MISSION_HOST(), IHIT_ARMOUR_SHIPMENTS)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard", PED_COMP_SPECIAL2, 1, 0)
						ELSE
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard", PED_COMP_SPECIAL2, 0, 0)
						ENDIF
					ENDIF
					
					SET_CUTSCENE_ORIGIN(vScenePos + ROTATE_VECTOR_ABOUT_Z(<<-55.3191, -101.9515, -1.1358>>, fSceneHeading), fSceneHeading, 0)
					SET_CUTSCENE_ORIGIN(vScenePos + ROTATE_VECTOR_ABOUT_Z(<<-55.3191, -101.9515, -1.1358>>, fSceneHeading), fSceneHeading, 1)
					
					START_CUTSCENE()
					START_MP_CUTSCENE()
					
					NETWORK_SET_VOICE_ACTIVE(FALSE)
					
					SET_ENTITY_VISIBLE_IN_CUTSCENE(PLAYER_PED_ID(), TRUE)
					
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE - 2 - started cutscene")
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF IS_CUTSCENE_PLAYING()
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE - 3 - faded in")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 4
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			AND (GET_CUTSCENE_TOTAL_DURATION() - GET_CUTSCENE_TIME()) <= 600
				DO_SCREEN_FADE_OUT(500)
				
				SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				CLEANUP_MP_CUTSCENE(FALSE, FALSE)
				
				IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				STOP_CUTSCENE_IMMEDIATELY()
				REMOVE_CUTSCENE()
				
				IF DOES_ENTITY_EXIST(g_sIslandHeistCutsceneData.pedGuard)
					DELETE_PED(g_sIslandHeistCutsceneData.pedGuard)
				ENDIF
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE - 4 - cutscene finished")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 5
			REQUEST_CUTSCENE("HS4_SCP_PUSH")
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				
				IF NOT HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GB_GET_LOCAL_PLAYER_MISSION_HOST(), IHIT_ARMOUR_SHIPMENTS)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard1", PED_COMP_SPECIAL2, 0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard2", PED_COMP_SPECIAL2, 0, 0)
				ELSE
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard1", PED_COMP_SPECIAL2, 1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard2", PED_COMP_SPECIAL2, 1, 0)
				ENDIF
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE - 5 - 2nd cutscene requested")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 6
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF HAS_CUTSCENE_LOADED()
				IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
					IF NEW_LOAD_SCENE_START_SPHERE(<<4545.5234, -4483.2749, 2.4571>>, 50.0)
						SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
						
						REINIT_NET_TIMER(g_sIslandHeistCutsceneData.stLoadSceneTimer)
					ENDIF
				ELSE
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					OR IS_NEW_LOAD_SCENE_LOADED()
					OR HAS_NET_TIMER_EXPIRED(g_sIslandHeistCutsceneData.stLoadSceneTimer, 15000)
						NEW_LOAD_SCENE_STOP()
						
						RESET_NET_TIMER(g_sIslandHeistCutsceneData.stLoadSceneTimer)
						
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						AND NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
							
							IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
							ENDIF
							
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
							
							SET_CUTSCENE_ORIGIN(<<4532.467, -4549.819, 3.4564>>, 70.0, 0)
							
							START_CUTSCENE()
							START_MP_CUTSCENE()
							
							NETWORK_SET_VOICE_ACTIVE(FALSE)
							
							SET_ENTITY_VISIBLE_IN_CUTSCENE(PLAYER_PED_ID(), TRUE)
							
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							
							PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE - 6 - cutscene started")
							g_sIslandHeistCutsceneData.iCutsceneState++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF IS_CUTSCENE_PLAYING()
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
					
					CLEAR_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
				ENDIF
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE - 7 - faded in")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 8
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			AND (GET_CUTSCENE_TOTAL_DURATION() - GET_CUTSCENE_TIME()) <= 600
				DO_SCREEN_FADE_OUT(500)
				
				SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				CLEANUP_MP_CUTSCENE(FALSE, FALSE)
				
				STOP_CUTSCENE_IMMEDIATELY()
				REMOVE_CUTSCENE()
				
				CLEAR_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
				
				PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE - 8 - cutscene finished")
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 9
			IF g_SpawnData.bUseCustomSpawnPoints 
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
					DO_SCREEN_FADE_IN(500)
					
					PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE - 9 - warped custom")
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_WARPED_TO_SAFE_LOCATION)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<5012.318, -4922.874, 9.189>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 306.400)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_WARPED_TO_SAFE_LOCATION)
				ELSE
					DO_SCREEN_FADE_IN(500)
					PRINTLN("[HEIST_ISLAND][CAUGHT] MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE - 9 - warped")
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 10
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			CLEANUP_ISLAND_HEIST_CUTSCENE()
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_CAUGHT_SCOPING_SMUGGLER_3_TIMES_SCENE()
	INT iPedCount
	
	PED_INDEX PedArray[5]
	
	SWITCH g_sIslandHeistCutsceneData.iCutsceneState
		CASE 0
			REQUEST_CUTSCENE("HS4_SCP_KNCK")
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
				
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("Island_Guard", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 1
			IF IS_SCREEN_FADED_OUT()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					iPedCount = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), PedArray, PEDTYPE_PLAYER_NETWORK)
					
					INT i
					REPEAT iPedCount i
						IF DOES_ENTITY_EXIST(PedArray[i])
							IF GET_ENTITY_MODEL(PedArray[i]) = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_m_cartelguards_01"))
							OR GET_ENTITY_MODEL(PedArray[i]) = INT_TO_ENUM(MODEL_NAMES, HASH("g_m_m_cartelguards_02"))
								g_sIslandHeistCutsceneData.pedGuard = CLONE_PED(PedArray[i], FALSE, FALSE, FALSE)
								
								i = iPedCount
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 2
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF HAS_CUTSCENE_LOADED()
			AND IS_SCREEN_FADED_OUT()
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					FLOAT fSceneHeading
					
					VECTOR vScenePos
					
					GET_CLOSEST_SAFE_CUTSCENE_COORDS_AND_HEADING(vScenePos, fSceneHeading)
					
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					
					IF IS_ENTITY_ALIVE(g_sIslandHeistCutsceneData.pedGuard)
						REGISTER_ENTITY_FOR_CUTSCENE(g_sIslandHeistCutsceneData.pedGuard, "Island_Guard", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					ELSE
						IF NOT HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GB_GET_LOCAL_PLAYER_MISSION_HOST(), IHIT_ARMOUR_SHIPMENTS)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard", PED_COMP_SPECIAL2, 1, 0)
						ELSE
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Island_Guard", PED_COMP_SPECIAL2, 0, 0)
						ENDIF
					ENDIF
					
					SET_CUTSCENE_ORIGIN(vScenePos + ROTATE_VECTOR_ABOUT_Z(<<-55.3191, -101.9515, -1.1358>>, fSceneHeading), fSceneHeading, 0)
					SET_CUTSCENE_ORIGIN(vScenePos + ROTATE_VECTOR_ABOUT_Z(<<-55.3191, -101.9515, -1.1358>>, fSceneHeading), fSceneHeading, 1)
					
					START_CUTSCENE()
					START_MP_CUTSCENE()
					
					NETWORK_SET_VOICE_ACTIVE(FALSE)
					
					SET_ENTITY_VISIBLE_IN_CUTSCENE(PLAYER_PED_ID(), TRUE)
					
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF IS_CUTSCENE_PLAYING()
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 4
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			AND (GET_CUTSCENE_TOTAL_DURATION() - GET_CUTSCENE_TIME()) <= 600
				DO_SCREEN_FADE_OUT(500)
				
				SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_FADE_OUT)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				CLEANUP_MP_CUTSCENE(FALSE, FALSE)
				
				STOP_CUTSCENE_IMMEDIATELY()
				REMOVE_CUTSCENE()
				
				IF DOES_ENTITY_EXIST(g_sIslandHeistCutsceneData.pedGuard)
					DELETE_PED(g_sIslandHeistCutsceneData.pedGuard)
				ENDIF
				
				HEIST_ISLAND_TRAVEL__LEAVE_ISLAND(HEIST_ISLAND_LEAVE_FADE_OUT)
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 5
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF HEIST_ISLAND_TRAVEL__IS_TRANSITION_FINISHED()
					g_sIslandHeistCutsceneData.iSpawnLocation = GET_RANDOM_INT_IN_RANGE(0, 6)
					
					VECTOR vSpawnCoords
					VECTOR vSpawnRotation
					
					GET_BEACH_SPAWN_POSITION_AND_ROTATION(vSpawnCoords, vSpawnRotation)
					
					SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vSpawnCoords)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), vSpawnRotation.z)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF NOT IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
				IF NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0)
					SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
					
					REINIT_NET_TIMER(g_sIslandHeistCutsceneData.stLoadSceneTimer)
				ENDIF
			ELSE
				REQUEST_ANIM_DICT(GET_BEACH_SCENE_ANIM_DICT())
				
				IF (NOT IS_NEW_LOAD_SCENE_ACTIVE()
				OR IS_NEW_LOAD_SCENE_LOADED()
				OR HAS_NET_TIMER_EXPIRED(g_sIslandHeistCutsceneData.stLoadSceneTimer, 15000))
				AND HAS_ANIM_DICT_LOADED(GET_BEACH_SCENE_ANIM_DICT())
					CLEAR_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_START_LOAD_SCENE)
					
					RESET_NET_TIMER(g_sIslandHeistCutsceneData.stLoadSceneTimer)
					
					NEW_LOAD_SCENE_STOP()
					
					VECTOR vScenePosition
					VECTOR vSceneRotation
					
					GET_BEACH_SPAWN_POSITION_AND_ROTATION(vScenePosition, vSceneRotation)
					
					g_sIslandHeistCutsceneData.iSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vScenePosition, vSceneRotation, DEFAULT, TRUE)
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), g_sIslandHeistCutsceneData.iSyncSceneID, GET_BEACH_SCENE_ANIM_DICT(), "action", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, REALLY_SLOW_BLEND_IN)
					
					NETWORK_START_SYNCHRONISED_SCENE(g_sIslandHeistCutsceneData.iSyncSceneID)
					
					g_sIslandHeistCutsceneData.cutsceneCam = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
					
					PLAY_CAM_ANIM(g_sIslandHeistCutsceneData.cutsceneCam, "action_camera", GET_BEACH_SCENE_ANIM_DICT(), vScenePosition, vSceneRotation)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					g_sIslandHeistCutsceneData.iCutsceneState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			IF NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(g_sIslandHeistCutsceneData.iSyncSceneID) = -1
			OR GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(g_sIslandHeistCutsceneData.iSyncSceneID)) >= 0.1
				DO_SCREEN_FADE_IN(500)
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ENDIF
		BREAK
		
		CASE 8
			ISLAND_HEIST_CUTSCENE_HIDE_UI()
			
			VECTOR vLeftInput
			vLeftInput = <<GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y), 0.0>>
			
			IF NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(g_sIslandHeistCutsceneData.iSyncSceneID) = -1
			OR GET_SYNCHRONIZED_SCENE_PHASE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(g_sIslandHeistCutsceneData.iSyncSceneID)) >= 0.99
			OR ((VMAG(vLeftInput) >= 0.24) AND HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BREAKOUT")))
				NETWORK_SET_VOICE_ACTIVE(TRUE)
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				IF DOES_CAM_EXIST(g_sIslandHeistCutsceneData.cutsceneCam)
				AND IS_CAM_ACTIVE(g_sIslandHeistCutsceneData.cutsceneCam)
					RENDER_SCRIPT_CAMS(FALSE, TRUE, 1000)
				ENDIF
				
				g_sIslandHeistCutsceneData.iCutsceneState++
			ELIF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BLENDOUT_CAM"))
			AND DOES_CAM_EXIST(g_sIslandHeistCutsceneData.cutsceneCam)
			AND IS_CAM_ACTIVE(g_sIslandHeistCutsceneData.cutsceneCam)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				RENDER_SCRIPT_CAMS(FALSE, TRUE, 1000)
			ENDIF
		BREAK
		
		CASE 9
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			CLEANUP_ISLAND_HEIST_CUTSCENE()
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_ISLAND_HEIST_CUTSCENES()
	IF g_sIslandHeistCutsceneData.iCutsceneState > 0
		IF IS_TRANSITION_ACTIVE()
		OR NOT IS_SKYSWOOP_AT_GROUND()
			CLEANUP_ISLAND_HEIST_CUTSCENE()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_RUN_SCENE)
		SWITCH g_sIslandHeistCutsceneData.eCutscene
			CASE ISLAND_HEIST_CUTSCENE_KICKED_FROM_BEACH
				MAINTAIN_KICKED_FROM_BEACH_SCENE()
			BREAK
			
			CASE ISLAND_HEIST_CUTSCENE_CAUGHT_SCOPING_DJ
				MAINTAIN_CAUGHT_SCOPING_DJ_SCENE()
			BREAK
			
			CASE ISLAND_HEIST_CUTSCENE_CAUGHT_SCOPING_SMUGGLER
				MAINTAIN_CAUGHT_SCOPING_SMUGGLER_SCENE()
			BREAK
			
			CASE ISLAND_HEIST_CUTSCENE_CAUGHT_SCOPING_SMUGGLER_3_TIMES
				MAINTAIN_CAUGHT_SCOPING_SMUGGLER_3_TIMES_SCENE()
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC START_ISLAND_HEIST_CUTSCENE(ISLAND_HEIST_CUTSCENES eCutscene)
	SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_RUN_SCENE)
	
	g_sIslandHeistCutsceneData.eCutscene = eCutscene
ENDPROC

FUNC BOOL IS_ISLAND_HEIST_CUTSCENE_RUNNING()
	RETURN IS_BIT_SET(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_RUN_SCENE)
ENDFUNC

#ENDIF
