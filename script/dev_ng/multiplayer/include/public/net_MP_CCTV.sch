// ====================================================================================
// ====================================================================================
//
// Name:        net_MP_CCTV.sch
// Description: Header for multiplayer CCTV functionality for simple interiors
//				Based off of net_MP_TV.sch, only using cctv functionality
// Written By:  Alex Murphy
//
// ====================================================================================
// ====================================================================================

USING "freemode_header.sch"
USING "context_control_public.sch"
USING "net_simple_interior.sch"

CONST_INT MP_CCTV_CHANNEL_CHANGE_WAIT_TIME		1000
CONST_INT MP_CCTV_FADE_TIME_POWER_CHANGE		500
CONST_INT MP_CCTV_LOAD_SCENE_TIMEOUT			10000

// Local iBitset
CONST_INT MP_CCTV_LOCAL_BS_UPDATE_BUTTONS						0
CONST_INT MP_CCTV_LOCAL_BS_FOCUS_SET							1
CONST_INT MP_CCTV_LOCAL_BS_DISABLED_SUICIDE						2
CONST_INT MP_CCTV_LOCAL_BS_DISABLED_INTERACTION					3
CONST_INT MP_CCTV_LOCAL_BS_WATCH_TV_CLEANUP						4
CONST_INT MP_CCTV_LOCAL_BS_CLIENT_TIMER_CHANNEL_CHANGE_SET		5
CONST_INT MP_CCTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR				6
CONST_INT MP_CCTV_LOCAL_BS_DISABLE_EXTERIOR_FOCUS				7
CONST_INT MP_CCTV_LOCAL_BS_STATIC_OUTSIDE_CAM_ON				8
CONST_INT MP_CCTV_LOCAL_BS_AUDIO_FRONTEND						9
CONST_INT MP_CCTV_LOCAL_BS_FORCE_SCALEFORM_DRAW					10
CONST_INT MP_CCTV_LOCAL_BS_REQUEST_APARTMENT_AUDIO_SCENE		11
CONST_INT MP_CCTV_LOCAL_BS_DRAW_LOADING_ICON					12
CONST_INT MP_CCTV_LOCAL_BS_SET_WATCHING_TV						13
CONST_INT MP_CCTV_LOCAL_BS_PAUSED_RENDERPHASE					14
CONST_INT MP_CCTV_LOCAL_BS_STARTED_TIMECYCLE					15
CONST_INT MP_CCTV_LOCAL_BS_BLOCK_USE_OF_CCTV					16
CONST_INT MP_CCTV_LOCAL_BS_DISPLAYED_BLOCK_HELP					17
CONST_INT MP_CCTV_LOCAL_BS_SET_CONTROLS							18
CONST_INT MP_CCTV_LOCAL_BS_ALTERNATE_VIEW						19
CONST_INT MP_CCTV_LOCAL_BS_CLOSING_CAM							20
CONST_INT MP_CCTV_LOCAL_BS_AUDIO_FADE							21
CONST_INT MP_CCTV_LOCAL_BS_DISABLE_THERMAL						22
CONST_INT MP_CCTV_LOCAL_BS_BANNER_LOADED						23

// Local iSwitchBitset
CONST_INT MP_CCTV_SWITCH_BS_LOAD_SCENE_REQUIRED					0
CONST_INT MP_CCTV_SWITCH_BS_LOAD_SCENE_RUNNING					1
CONST_INT MP_CCTV_SWITCH_BS_FADE_REQUIRED						2
CONST_INT MP_CCTV_SWITCH_BS_ACTIVATING							3
CONST_INT MP_CCTV_SWITCH_BS_DEACTIVATING						4

// Client iInputBitset
CONST_INT MP_CCTV_INPUT_NEXT_CHANNEL		0
CONST_INT MP_CCTV_INPUT_PREVIOUS_CHANNEL	1

/// PURPOSE: List of client stage IDs
ENUM MP_CCTV_CLIENT_STAGE
	MP_CCTV_CLIENT_STAGE_INIT = 0,	// The client is setting up the cctv
	MP_CCTV_CLIENT_STAGE_WALKING,	// The client is in the property but not using the cctv
	MP_CCTV_CLIENT_STAGE_ACTIVATED	// The client has activated the cctv, can now change channels or deactivate controlling the cctv
ENDENUM

/// PURPOSE: List of cctv channel IDs
ENUM MP_CCTV_CHANNEL
	MP_CCTV_CHANNEL_0 = 0,	// The cctv channels are fullscreen, fixed camera channels with a filter applied
	MP_CCTV_CHANNEL_1,
	MP_CCTV_CHANNEL_2,
	MP_CCTV_CHANNEL_3,
	MP_CCTV_CHANNEL_4,
	MP_CCTV_CHANNEL_5,
	MP_CCTV_CHANNEL_6,
	MP_CCTV_CHANNEL_7,
	MP_CCTV_CHANNEL_8,
	MP_CCTV_CHANNEL_9,
	MP_CCTV_CHANNEL_10,
	MP_CCTV_CHANNEL_11,
	MP_CCTV_CHANNEL_12,
	MP_CCTV_CHANNEL_13,
	MP_CCTV_CHANNEL_14
ENDENUM

/// PURPOSE: List of cctv channel changing switch state IDs
ENUM MP_CCTV_SWITCH_STATE
	MP_CCTV_SWITCH_STATE_SETUP_PROCESS = 0,
	MP_CCTV_SWITCH_STATE_FADE_OUT,
	MP_CCTV_SWITCH_STATE_WAIT_FOR_FADE_OUT,
	MP_CCTV_SWITCH_STATE_CLEANUP_CURRENT,
	MP_CCTV_SWITCH_STATE_SETUP_DESIRED,
	MP_CCTV_SWITCH_STATE_SETUP_LOAD_SCENE,
	MP_CCTV_SWITCH_STATE_WAIT_FOR_LOAD_SCENE,
	MP_CCTV_SWITCH_STATE_CLEANUP_LOAD_SCENE,
	MP_CCTV_SWITCH_STATE_WAIT_FOR_FILTER_TIME,
	MP_CCTV_SWITCH_STATE_WAIT_FOR_FADE_IN,
	MP_CCTV_SWITCH_STATE_FADE_IN,
	MP_CCTV_SWITCH_STATE_CLEANUP_PROCESS,
	MP_CCTV_SWITCH_STATE_WATCH_CHANNEL
ENDENUM

/// PURPOSE: Local data struct
STRUCT MP_CCTV_LOCAL_DATA_STRUCT
	BOOL bUsedCCTV = FALSE
	BOOL bConcealed = FALSE
	BOOL bSpectatorFadesDisabled = FALSE
	
	CAMERA_INDEX camCCTV
	
	FLOAT fCameraAngle = 0.0
	FLOAT fAngledAreaWidth
	FLOAT fAngledAreaEnterHeading
	FLOAT fMaxHeadingDifference = 45.0 // Max acceptable heading difference between ped and fAngledAreaEnterHeading
	
	INT iBitset
	INT iSwitchBitset = 0
	INT iNumberOfChannels = 3
	INT iSoundCCTVMoveCam = -1
	INT iChannelSwitchTimer = 0
	INT iSoundCCTVChangeCam = -1
	INT iSoundCCTVBackground = -1
	INT iCCTVContextIntention = NEW_CONTEXT_INTENTION
	
	MP_CCTV_CHANNEL eCurrentChannel = MP_CCTV_CHANNEL_0
	MP_CCTV_CHANNEL eSwitchChannelTo = MP_CCTV_CHANNEL_0
	MP_CCTV_CHANNEL eSwitchChannelFrom = MP_CCTV_CHANNEL_0
	
	MP_CCTV_SWITCH_STATE eSwitchState = MP_CCTV_SWITCH_STATE_WATCH_CHANNEL
	
	SCALEFORM_INDEX sfCCTV
	SCALEFORM_INDEX sfButton
	
	SCALEFORM_INSTRUCTIONAL_BUTTONS scaleformInstructionalButtons
	
	SCALEFORM_LOADING_ICON loadingIcon
	
	SCRIPT_TIMER stFadeInDelay
	
	SIMPLE_INTERIORS eSimpleInteriorType
	
	TIME_DATATYPE timeIncrement
	TIME_DATATYPE timeClientChannelChange
	
	TVCHANNELTYPE tvChannelTypeCurrent = TVCHANNELTYPE_CHANNEL_NONE
	
	VECTOR vLoadSceneRot
	VECTOR vAngledAreaPos1
	VECTOR vAngledAreaPos2
	VECTOR vLoadSceneLocation
	
	VEHICLE_INDEX vehToAttachCCTVto = NULL
ENDSTRUCT

/// PURPOSE: Client broadcast data struct
STRUCT MP_CCTV_CLIENT_DATA_STRUCT
	INT iInputBitset
	
	MP_CCTV_CHANNEL eCurrentChannel = MP_CCTV_CHANNEL_0
	MP_CCTV_CHANNEL eTimeOfInputChannel = MP_CCTV_CHANNEL_0
	
	MP_CCTV_CLIENT_STAGE eStage = MP_CCTV_CLIENT_STAGE_INIT
ENDSTRUCT

#IF IS_DEBUG_BUILD
/// PURPOSE: Debug Data struct
STRUCT MP_CCTV_DEBUG
	BOOL bUpdateCam
	BOOL bInitialised
	
	CCTV_DETAILS camOffsetFromDefault
ENDSTRUCT
MP_CCTV_DEBUG MPCCTVWidgetData

/// PURPOSE:
///    Call during set up of script that uses mpcctv to add this widget.
PROC CREATE_MPCCTV_WIDGETS()
	IF NOT MPCCTVWidgetData.bInitialised
		MPCCTVWidgetData.camOffsetFromDefault.vPos = <<0.0, 0.0, 0.0>>
		MPCCTVWidgetData.camOffsetFromDefault.vRot = <<0.0, 0.0, 0.0>>
		MPCCTVWidgetData.camOffsetFromDefault.fFov = 0.0
		
		START_WIDGET_GROUP("net_mp_cctv")
			ADD_WIDGET_BOOL("Update cam", MPCCTVWidgetData.bUpdateCam)
			
			ADD_WIDGET_VECTOR_SLIDER("Pos offset", MPCCTVWidgetData.camOffsetFromDefault.vPos, -5000.0, 5000.0, 0.05)
			
			ADD_WIDGET_VECTOR_SLIDER("Rot offset", MPCCTVWidgetData.camOffsetFromDefault.vRot, 0.0, 360.0, 1.0)
			
			ADD_WIDGET_FLOAT_SLIDER("FOV offset", MPCCTVWidgetData.camOffsetFromDefault.fFOV, 0.0, 180.0, 1.0)
		STOP_WIDGET_GROUP()
		
		MPCCTVWidgetData.bInitialised = TRUE
	ENDIF
ENDPROC
#ENDIF

/// PURPOSE:
///    Sets the cctv client stage
/// PARAMS:
///    MPCCTVClient - Client broadcast data
///    eStage - Desired stage
PROC SET_MP_CCTV_CLIENT_STAGE(MP_CCTV_CLIENT_DATA_STRUCT &MPCCTVClient, MP_CCTV_CLIENT_STAGE eStage)
	MPCCTVClient.eStage = eStage
ENDPROC

/// PURPOSE:
///    Sets the cctv channel switch state for the local player
/// PARAMS:
///    MPCCTVLocal - Local data struct
///    eState - Desired cctv channel switch state
PROC SET_MP_CCTV_SWITCH_STATE(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, MP_CCTV_SWITCH_STATE eState)
	MPCCTVLocal.eSwitchState = eState
ENDPROC

/// PURPOSE:
///    Gets current local cctv channel switch state
/// PARAMS:
///    MPCCTVLocal - Local data struct
/// RETURNS:
///    Current cctv channel switch state
FUNC MP_CCTV_SWITCH_STATE GET_MP_CCTV_SWITCH_STATE(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	RETURN MPCCTVLocal.eSwitchState
ENDFUNC

/// PURPOSE:
///    Starts sound effect for changing to a cctv channel
/// PARAMS:
///    MPCCTVLocal - Local data struct
PROC MP_CCTV_SOUND_START_PAN_CAM(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	IF MPCCTVLocal.iSoundCCTVMoveCam = -1
		MPCCTVLocal.iSoundCCTVMoveCam = GET_SOUND_ID()
		
		IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
			PLAY_SOUND_FRONTEND(MPCCTVLocal.iSoundCCTVMoveCam, "Pan", "DLC_Arena_CCTV_SOUNDSET")
		ELSE
			PLAY_SOUND_FRONTEND(MPCCTVLocal.iSoundCCTVMoveCam, "Pan", "MP_CCTV_SOUNDSET")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops sound effect for changing to a cctv channel
/// PARAMS:
///    MPCCTVLocal - Local data struct
PROC MP_CCTV_SOUND_STOP_PAN_CAM(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	IF MPCCTVLocal.iSoundCCTVMoveCam <> -1
		STOP_SOUND(MPCCTVLocal.iSoundCCTVMoveCam)
		
		RELEASE_SOUND_ID(MPCCTVLocal.iSoundCCTVMoveCam)
		
		MPCCTVLocal.iSoundCCTVMoveCam = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Starts sound effect for changing to a cctv channel
/// PARAMS:
///    MPCCTVLocal - Local data struct
PROC MP_CCTV_SOUND_START_CHANGE_CAM(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	IF MPCCTVLocal.iSoundCCTVChangeCam = -1
		MPCCTVLocal.iSoundCCTVChangeCam = GET_SOUND_ID()
		
		IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
			PLAY_SOUND_FRONTEND(MPCCTVLocal.iSoundCCTVChangeCam, "Change_Cam", "DLC_Arena_CCTV_SOUNDSET")
		ELSE
			PLAY_SOUND_FRONTEND(MPCCTVLocal.iSoundCCTVChangeCam, "Change_Cam", "MP_CCTV_SOUNDSET")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops sound effect for changing to a cctv channel
/// PARAMS:
///    MPCCTVLocal - Local data struct
PROC MP_CCTV_SOUND_STOP_CHANGE_CAM(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	IF MPCCTVLocal.iSoundCCTVChangeCam <> -1
		STOP_SOUND(MPCCTVLocal.iSoundCCTVChangeCam)
		
		RELEASE_SOUND_ID(MPCCTVLocal.iSoundCCTVChangeCam)
		
		MPCCTVLocal.iSoundCCTVChangeCam = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Starts sound effect for background of a cctv channel
/// PARAMS:
///    MPCCTVLocal - Local data struct
PROC MP_CCTV_SOUND_START_BACKGROUND(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	IF MPCCTVLocal.iSoundCCTVBackground = -1
		MPCCTVLocal.iSoundCCTVBackground = GET_SOUND_ID()
		
		IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
			PLAY_SOUND_FRONTEND(MPCCTVLocal.iSoundCCTVBackground, "Background", "DLC_Arena_CCTV_SOUNDSET")
		ELSE
			PLAY_SOUND_FRONTEND(MPCCTVLocal.iSoundCCTVBackground, "Background", "MP_CCTV_SOUNDSET")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops sound effect for background of a cctv channel
/// PARAMS:
///    MPCCTVLocal - Local data struct
PROC MP_CCTV_SOUND_STOP_BACKGROUND(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	IF MPCCTVLocal.iSoundCCTVBackground <> -1
		STOP_SOUND(MPCCTVLocal.iSoundCCTVBackground)
		
		RELEASE_SOUND_ID(MPCCTVLocal.iSoundCCTVBackground)
		
		MPCCTVLocal.iSoundCCTVBackground = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Performs a screen fade in for the cctv
/// PARAMS:
///    MPCCTVLocal - Local data struct
///    iTime - Fade in time
PROC MP_CCTV_FADE_IN(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, INT iTime)
	DO_SCREEN_FADE_IN(iTime)
	
	CLEAR_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_FADE_REQUIRED)
ENDPROC

/// PURPOSE:
///    Performs a screen fade out for the cctv
/// PARAMS:
///    MPCCTVLocal - Local data struct
///    iTime - Fade out time
PROC MP_CCTV_FADE_OUT(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, INT iTime)
	DO_SCREEN_FADE_OUT(iTime)
	
	SET_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_FADE_REQUIRED)
ENDPROC

/// PURPOSE:
///    Returns if the local player can switch to a specified channel
/// PARAMS:
///    thisChannel - Desired channel
/// RETURNS:
///    True if the local player can switch to the channel
FUNC BOOL CAN_SWITCH_TO_THIS_MP_CCTV_CHANNEL(MP_CCTV_CHANNEL thisChannel)
	SWITCH thisChannel
		CASE MP_CCTV_CHANNEL_0 
		CASE MP_CCTV_CHANNEL_1
		CASE MP_CCTV_CHANNEL_2
		CASE MP_CCTV_CHANNEL_3
		CASE MP_CCTV_CHANNEL_4
		CASE MP_CCTV_CHANNEL_5
		CASE MP_CCTV_CHANNEL_6
		CASE MP_CCTV_CHANNEL_7
		CASE MP_CCTV_CHANNEL_8
		CASE MP_CCTV_CHANNEL_9
		CASE MP_CCTV_CHANNEL_10
		CASE MP_CCTV_CHANNEL_11
		CASE MP_CCTV_CHANNEL_12
		CASE MP_CCTV_CHANNEL_13
		CASE MP_CCTV_CHANNEL_14
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
			AND NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
				RETURN FALSE
			ENDIF
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Update the cached vehicle_index vehToAttachCCTVto.
///    While vehToAttachCCTVto <> NULL, camera pos & rot are relative
///    to the vehicle they are attached to.
PROC UPDATE_VEHICLE_TO_ATTACH_TO(MP_CCTV_LOCAL_DATA_STRUCT& localCCTV)
	SWITCH localCCTV.eSimpleInteriorType
		CASE SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1
			localCCTV.vehToAttachCCTVto = GET_AVENGER_VEHICLE_INDEX_THAT_I_AM_IN()
		BREAK
		
		//Add any other SIMPLE_INTERIORS that attach cctvs to vehicles here (filter by channel if required)
		
		DEFAULT
			localCCTV.vehToAttachCCTVto = NULL
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_CHANNEL_EXTERIOR(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, MP_CCTV_CHANNEL eChannel, SIMPLE_INTERIORS ePropertyID)
	SWITCH ePropertyID
		CASE SIMPLE_INTERIOR_BUNKER_1
		CASE SIMPLE_INTERIOR_BUNKER_2
		CASE SIMPLE_INTERIOR_BUNKER_3
		CASE SIMPLE_INTERIOR_BUNKER_4
		CASE SIMPLE_INTERIOR_BUNKER_5
		CASE SIMPLE_INTERIOR_BUNKER_6
		CASE SIMPLE_INTERIOR_BUNKER_7
		CASE SIMPLE_INTERIOR_BUNKER_9
		CASE SIMPLE_INTERIOR_BUNKER_10
		CASE SIMPLE_INTERIOR_BUNKER_11
		CASE SIMPLE_INTERIOR_BUNKER_12
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HANGAR_1
		CASE SIMPLE_INTERIOR_HANGAR_2
		CASE SIMPLE_INTERIOR_HANGAR_3
		CASE SIMPLE_INTERIOR_HANGAR_4
		CASE SIMPLE_INTERIOR_HANGAR_5
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_10
				CASE MP_CCTV_CHANNEL_11
				CASE MP_CCTV_CHANNEL_12
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_12
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0
					IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_ARENA_GARAGE_1
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0
				CASE MP_CCTV_CHANNEL_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
		CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
		CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON
		CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
		CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_4
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		#IF FEATURE_MUSIC_STUDIO
		CASE SIMPLE_INTERIOR_MUSIC_STUDIO
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_4
				CASE MP_CCTV_CHANNEL_5
				CASE MP_CCTV_CHANNEL_6
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		
		#IF FEATURE_FIXER
		CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
		CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
		CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
		CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_4
				CASE MP_CCTV_CHANNEL_5
				CASE MP_CCTV_CHANNEL_6
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF	
		
		#IF FEATURE_DLC_2_2022
		CASE SIMPLE_INTERIOR_JUGGALO_HIDEOUT
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0
					RETURN TRUE
				BREAK
				CASE MP_CCTV_CHANNEL_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_MULTISTOREY_GARAGE
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0
					RETURN TRUE
				BREAK
				CASE MP_CCTV_CHANNEL_1
					RETURN TRUE
				BREAK
				CASE MP_CCTV_CHANNEL_2
					RETURN TRUE
				BREAK
				CASE MP_CCTV_CHANNEL_3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		
		CASE SIMPLE_INTERIOR_INVALID
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Client sets the channel that the property cctv is watching
/// PARAMS:
///    MPCCTVClient - Client data struct
///    thisChannel - Desired channel
PROC SET_CLIENT_MP_CCTV_CHANNEL(MP_CCTV_CLIENT_DATA_STRUCT &MPCCTVClient, MP_CCTV_CHANNEL thisChannel)
	MPCCTVClient.eCurrentChannel = thisChannel
ENDPROC

/// PURPOSE:
///    Client switches to next valid channel
/// PARAMS:
///    MPCCTVClient - Client data struct
PROC SWITCH_TO_NEXT_MP_CCTV_CHANNEL(MP_CCTV_CLIENT_DATA_STRUCT &MPCCTVClient, MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, SIMPLE_INTERIORS propertyID, INT iBlockerBitSet = 0)
	BOOL bFinished = FALSE
	
	INT iCurrentChannelID  = ENUM_TO_INT(MPCCTVClient.eCurrentChannel)
	INT iProcess = iCurrentChannelID
	INT iSelection = -1
	
	WHILE bFinished = FALSE
		iProcess += 1
		
		IF iProcess >= MPCCTVLocal.iNumberOfChannels
			iProcess = 0
		ENDIF
		
		IF iProcess = iCurrentChannelID
			bFinished = TRUE
		ELSE
			IF CAN_SWITCH_TO_THIS_MP_CCTV_CHANNEL(INT_TO_ENUM(MP_CCTV_CHANNEL, iProcess))
				iSelection = iProcess
				bFinished = TRUE
			ENDIF
		ENDIF
	ENDWHILE
	
	IF propertyID != SIMPLE_INTERIOR_INVALID
		IF GET_SIMPLE_INTERIOR_TYPE(propertyID) = SIMPLE_INTERIOR_TYPE_BUNKER
			IF INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_5
			AND IS_BIT_SET(iBlockerBitSet, 0)
				iSelection++
			ENDIF
		ENDIF
		
		IF GET_SIMPLE_INTERIOR_TYPE(propertyID) = SIMPLE_INTERIOR_TYPE_HANGAR
			IF INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_5
			AND IS_BIT_SET(iBlockerBitSet, 0)
				iSelection++
			ENDIF
		ENDIF
		
		IF GET_SIMPLE_INTERIOR_TYPE(propertyID) = SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE
			IF INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_0
			AND bBlockCCTVDuringGangOpsCutscene
				iSelection++
			ENDIF
			
			IF (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_1
			AND IS_BIT_SET(iBlockerBitSet, 0))
				iSelection++
			ENDIF
			
			IF (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_2
			AND IS_BIT_SET(iBlockerBitSet, 1))
				iSelection++
			ENDIF
		ENDIF
		
		IF GET_SIMPLE_INTERIOR_TYPE(propertyID) = SIMPLE_INTERIOR_TYPE_BUSINESS_HUB
		AND GET_INTERIOR_FLOOR_INDEX() != 0
			IF (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_4
			AND IS_BIT_SET(iBlockerBitSet, 0))
				iSelection++
			ENDIF
			
			IF (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_5
			AND IS_BIT_SET(iBlockerBitSet, 0))
				iSelection++
			ENDIF
			
			IF (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_6
			AND IS_BIT_SET(iBlockerBitSet, 0))
				iSelection = 0
			ENDIF
		ENDIF
		
		#IF FEATURE_FIXER			
			IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())	
			AND (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_2)
			AND NOT HAS_PLAYER_PURCHASED_FIXER_HQ_UPGRADE_PERSONAL_QUARTERS(PLAYER_ID())			
				iSelection = 3
			ENDIF
		#ENDIF			
		
	ENDIF
	
	IF iSelection <> -1
		UPDATE_VEHICLE_TO_ATTACH_TO(MPCCTVLocal)
		
		SET_CLIENT_MP_CCTV_CHANNEL(MPCCTVClient, INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection))
	ENDIF
ENDPROC

/// PURPOSE:
///    Client switches to previous valid channel
/// PARAMS:
///    MPCCTVClient - Client data struct
PROC SWITCH_TO_PREVIOUS_MP_CCTV_CHANNEL(MP_CCTV_CLIENT_DATA_STRUCT &MPCCTVClient, MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, SIMPLE_INTERIORS propertyID, INT iBlockerBitSet = 0)
	BOOL bFinished = FALSE
	
	INT iCurrentChannelID = ENUM_TO_INT(MPCCTVClient.eCurrentChannel)
	INT iProcess = iCurrentChannelID
	INT iSelection = -1
	
	WHILE bFinished = FALSE
		iProcess -= 1
		
		IF iProcess < 0
			iProcess = MPCCTVLocal.iNumberOfChannels - 1
		ENDIF
		
		IF iProcess = iCurrentChannelID
			bFinished = TRUE
		ELSE
			IF CAN_SWITCH_TO_THIS_MP_CCTV_CHANNEL(INT_TO_ENUM(MP_CCTV_CHANNEL, iProcess))
				iSelection = iProcess
				bFinished = TRUE
			ENDIF
		ENDIF
	ENDWHILE
	
	IF propertyID != SIMPLE_INTERIOR_INVALID
		IF GET_SIMPLE_INTERIOR_TYPE(propertyID) = SIMPLE_INTERIOR_TYPE_BUNKER
			IF INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_5
			AND IS_BIT_SET(iBlockerBitSet, 0)
				iSelection--
			ENDIF
		ENDIF
		
		IF GET_SIMPLE_INTERIOR_TYPE(propertyID) = SIMPLE_INTERIOR_TYPE_HANGAR
			IF INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_5
			AND IS_BIT_SET(iBlockerBitSet, 0)
				iSelection--
			ENDIF
		ENDIF
		
		IF GET_SIMPLE_INTERIOR_TYPE(propertyID) = SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE
			IF (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_2
			AND IS_BIT_SET(iBlockerBitSet, 1))
				iSelection--
			ENDIF
			
			IF (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_1
			AND IS_BIT_SET(iBlockerBitSet, 0))
				iSelection--
			ENDIF
			
			IF INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_0
			AND bBlockCCTVDuringGangOpsCutscene
				iSelection = MPCCTVLocal.iNumberOfChannels - 1
			ENDIF
		ENDIF
		
		IF GET_SIMPLE_INTERIOR_TYPE(propertyID) = SIMPLE_INTERIOR_TYPE_BUSINESS_HUB
		AND GET_INTERIOR_FLOOR_INDEX() != 0
			IF (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_6
			AND IS_BIT_SET(iBlockerBitSet, 0))
				iSelection--
			ENDIF
			
			IF (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_5
			AND IS_BIT_SET(iBlockerBitSet, 0))
				iSelection--
			ENDIF
			
			IF (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_4
			AND IS_BIT_SET(iBlockerBitSet, 0))
				iSelection--
			ENDIF
		ENDIF
		
		#IF FEATURE_FIXER			
			IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())	
			AND (INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection) = MP_CCTV_CHANNEL_2)
			AND NOT HAS_PLAYER_PURCHASED_FIXER_HQ_UPGRADE_PERSONAL_QUARTERS(PLAYER_ID())			
				iSelection = 1
			ENDIF
		#ENDIF	
	ENDIF
	
	IF iSelection <> -1
		SET_CLIENT_MP_CCTV_CHANNEL(MPCCTVClient, INT_TO_ENUM(MP_CCTV_CHANNEL, iSelection))
	ENDIF
ENDPROC

/// PURPOSE:
///    Client sets flags on the frame it recognises the channel has changed
/// PARAMS:
///    MPCCTVLocal - Local data struct
PROC HANDLE_MP_CCTV_CHANNEL_CHANGE_THIS_FRAME(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	MPCCTVLocal.timeClientChannelChange = GET_NETWORK_TIME()
	
	SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_CLIENT_TIMER_CHANNEL_CHANGE_SET)
ENDPROC

/// PURPOSE:
///    Client checks if the current channel is no longer valid to watch
/// PARAMS:
///    MPCCTVClient - Client data struct
/// RETURNS:
///    True if channel no longer sutiable
FUNC BOOL HAS_CURRENT_MP_CCTV_CHANNEL_BECOME_UNACCEPTABLE(MP_CCTV_CLIENT_DATA_STRUCT &MPCCTVClient, SIMPLE_INTERIORS propertyID)
	MP_CCTV_CHANNEL thisCurrentClientChannel = MPCCTVClient.eCurrentChannel
	
	IF propertyID != SIMPLE_INTERIOR_INVALID
	AND GET_SIMPLE_INTERIOR_TYPE(propertyID) = SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE
		IF thisCurrentClientChannel = MP_CCTV_CHANNEL_0
		AND bBlockCCTVDuringGangOpsCutscene
			SET_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_NEXT_CHANNEL)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_0)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_1)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_2)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_3)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_4)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_5)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_6)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_7)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_8)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_9)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_10)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_11)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_12)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_13)
	OR NOT (thisCurrentClientChannel = MP_CCTV_CHANNEL_14)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Client keeps track of the current channel and switches to the correct on if necessary
/// PARAMS:
///    MPTVClient - Client data struct
///    MPTVLocal - Local data struct
PROC CLIENT_MAINTAIN_CURRENT_MP_CCTV_CHANNEL(MP_CCTV_CLIENT_DATA_STRUCT &MPCCTVClient, MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, SIMPLE_INTERIORS propertyID, INT iBlockerBitSet = 0)
	BOOL bReadyForUpdate = FALSE
	BOOL bChangedChannelThisFrame = FALSE
	BOOL bIsClientSuitableForChange = FALSE
	
	MP_CCTV_CHANNEL thisCurrentClientChannel = MPCCTVClient.eCurrentChannel
	
	IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_CLIENT_TIMER_CHANNEL_CHANGE_SET)
	OR GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPCCTVLocal.timeClientChannelChange) > MP_CCTV_CHANNEL_CHANGE_WAIT_TIME
		bReadyForUpdate = TRUE
		
		IF MPCCTVClient.eTimeOfInputChannel = thisCurrentClientChannel
			bIsClientSuitableForChange = TRUE
		ENDIF
	ENDIF
	
	IF bReadyForUpdate
		IF bIsClientSuitableForChange
			IF IS_BIT_SET(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_NEXT_CHANNEL)
				SWITCH_TO_NEXT_MP_CCTV_CHANNEL(MPCCTVClient, MPCCTVLocal, propertyID, iBlockerBitSet)
				
				bChangedChannelThisFrame = TRUE
			ELIF IS_BIT_SET(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_PREVIOUS_CHANNEL)
				SWITCH_TO_PREVIOUS_MP_CCTV_CHANNEL(MPCCTVClient, MPCCTVLocal, propertyID, iBlockerBitSet)
				
				bChangedChannelThisFrame = TRUE
			ENDIF
		ENDIF
		
		IF NOT bChangedChannelThisFrame
			IF HAS_CURRENT_MP_CCTV_CHANNEL_BECOME_UNACCEPTABLE(MPCCTVClient, propertyID)
				bChangedChannelThisFrame = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bChangedChannelThisFrame
		HANDLE_MP_CCTV_CHANNEL_CHANGE_THIS_FRAME(MPCCTVLocal)
	ENDIF
ENDPROC

/// PURPOSE:
///    Tidies the cctv ready to midly reset the data
/// PARAMS:
///    MPCCTVLocal - Local data struct
PROC TIDYUP_MP_CCTV(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, BOOl bReturnControl)
	PRINTLN("[CCTV] - TIDYUP_MP_CCTV - Starting")
	
	IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_AUDIO_FADE)
		SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
	ENDIF
	
	g_SimpleInteriorData.bSuppressConcealCheck = FALSE
	
	IF MPCCTVLocal.bUsedCCTV
		FORCE_SIMPLE_INTERIOR_CONCEAL_PLAYERS_CHECK()
		MPCCTVLocal.bUsedCCTV = FALSE
	ENDIF
	
	MPCCTVLocal.bConcealed = FALSE
	
	IF (NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
	OR  IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()  )
		MP_CCTV_SOUND_STOP_PAN_CAM(MPCCTVLocal)
		MP_CCTV_SOUND_STOP_CHANGE_CAM(MPCCTVLocal)
		MP_CCTV_SOUND_STOP_BACKGROUND(MPCCTVLocal)
	ENDIF
	
	g_bRequestingLiveStream = FALSE
	
	CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLE_EXTERIOR_FOCUS)
	CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_STATIC_OUTSIDE_CAM_ON)
	CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
	CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_CLOSING_CAM)
	
	CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_IS_SPECTATING)
	
	ENABLE_WEAPON_SWAP()
	
	Enable_MP_Comms()
	
	UNLOCK_MINIMAP_ANGLE()
	UNLOCK_MINIMAP_POSITION()
	
	SET_NOISEOVERIDE(FALSE)
	SET_NOISINESSOVERIDE(0.0)
	
	DISPLAY_CASH(TRUE)
	
	IF DOES_CAM_EXIST(MPCCTVLocal.camCCTV)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	
	IF DOES_CAM_EXIST(MPCCTVLocal.camCCTV)
		DESTROY_CAM(MPCCTVLocal.camCCTV)
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("WATCHING_SAFEHOUSE_TV")
		STOP_AUDIO_SCENE("WATCHING_SAFEHOUSE_TV")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_CCTV_SCENE")
		STOP_AUDIO_SCENE("MP_CCTV_SCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("dlc_aw_arena_cctv_scene")
		STOP_AUDIO_SCENE("dlc_aw_arena_cctv_scene")
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, FALSE)
	ENDIF
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(MPCCTVLocal.sfCCTV)
	
	IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
	AND IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_BANNER_LOADED)
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("AW_CAM_BANNER")
		
		CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_BANNER_LOADED)
	ENDIF
	
	CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR)
	
	IF bReturnControl
		IF NOT IS_SCREEN_FADED_IN()
			IF NOT IS_SCREEN_FADING_IN()
				IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_FADE_REQUIRED)
					MP_CCTV_FADE_IN(MPCCTVLocal, MP_CCTV_FADE_TIME_POWER_CHANGE)
					
					IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLE_THERMAL)
						ENABLE_NIGHTVISION(VISUALAID_THERMAL)
						
						CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLE_THERMAL)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF g_bMPTVplayerWatchingTV
		AND IS_NET_PLAYER_OK(PLAYER_ID())
		AND IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_SET_CONTROLS)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_PREVENT_VISIBILITY_CHANGES)
			CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_SET_CONTROLS)
		ENDIF
	ENDIF
	
	g_bMPTVplayerWatchingTV = FALSE
	g_bUsingCCTV = FALSE
	g_bUsingCCTVExternal = FALSE
	
	IF MPCCTVLocal.bSpectatorFadesDisabled
		ENABLE_SPECTATOR_FADES()
		
		MPCCTVLocal.bSpectatorFadesDisabled = FALSE
	ENDIF
	
	MPCCTVLocal.fCameraAngle = 0.0
	
	CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LAUNCH_LIVE_STREAM)
	
	PRINTLN("[CCTV] - TIDYUP_MP_CCTV - Finished")
ENDPROC

/// PURPOSE:
///    Completely cleans up the client data cctv as part of cleanup of the simple interior
/// PARAMS:
///    MPCCTVClient - Client broadcast data
///    MPCCTVLocal - Local data struct
PROC CLEANUP_MP_CCTV_CLIENT(MP_CCTV_CLIENT_DATA_STRUCT &MPCCTVClient, MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, BOOl bReturnControl, BOOL bClearTvChannel = TRUE)
	PRINTLN("[CCTV] - CLEANUP_MP_CCTV_CLIENT - Starting")
	
	IF bClearTvChannel
		PRINTLN("[CCTV] - SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)")
		SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
	ENDIF
	
	TIDYUP_MP_CCTV(MPCCTVLocal, bReturnControl)
	
	IF NOT Is_Ped_Drunk(PLAYER_PED_ID())
	AND IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_STARTED_TIMECYCLE)
	AND NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
		CLEAR_TIMECYCLE_MODIFIER()
	ENDIF
	
	RESET_NET_TIMER(MPCCTVLocal.stFadeInDelay)
	
	CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_STARTED_TIMECYCLE)
	
	IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLED_SUICIDE)
		ENABLE_KILL_YOURSELF_OPTION()
		
		CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLED_SUICIDE)
	ENDIF
	
	IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLED_INTERACTION)
		ENABLE_INTERACTION_MENU()
		
		CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLED_INTERACTION)
	ENDIF
	
	IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FOCUS_SET)
		CLEAR_FOCUS()
		
		NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
		
		CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FOCUS_SET)
	ENDIF
	
	IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_LOAD_SCENE_RUNNING)
		NEW_LOAD_SCENE_STOP()
		
		CLEAR_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_LOAD_SCENE_RUNNING)
	ENDIF
	
	IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_PAUSED_RENDERPHASE)
		IF NOT g_bMissionEnding
			TOGGLE_RENDERPHASES(TRUE)
		ENDIF
		
		CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_PAUSED_RENDERPHASE)
	ENDIF
	
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)
	
	MPCCTVLocal.eCurrentChannel = MP_CCTV_CHANNEL_0
	
	SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_WATCH_CHANNEL)
	
	MPCCTVLocal.tvChannelTypeCurrent = TVCHANNELTYPE_CHANNEL_NONE
	
	g_iOverheadNamesState = -1
	
	IF MPCCTVLocal.iCCTVContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(MPCCTVLocal.iCCTVContextIntention)
	ENDIF
	
	MPCCTVLocal.iBitset = 0
	MPCCTVLocal.iSwitchBitset = 0
	
	SET_MP_CCTV_CLIENT_STAGE(MPCCTVClient, MP_CCTV_CLIENT_STAGE_INIT)
	
	MPCCTVClient.iInputBitset = 0
	
	CLEAR_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_STOP_MPTV_SPECTATE)
	
	MPCCTVClient.eCurrentChannel = MP_CCTV_CHANNEL_0
	
	PRINTLN("[CCTV] - CLEANUP_MP_CCTV_CLIENT - Finished")
ENDPROC

/// PURPOSE:
///    Gets the channel type (scaleform, fullscreen, etc.) of a specified channel
/// PARAMS:
///    thisChannel - Channel ID of channel to get channel type of
/// RETURNS:
///    Channel type
FUNC TVCHANNELTYPE GET_MP_CCTV_CHANNEL_TVCHANNELTYPE(MP_CCTV_CHANNEL thisChannel)
	SWITCH thisChannel
		CASE MP_CCTV_CHANNEL_0
		CASE MP_CCTV_CHANNEL_1
		CASE MP_CCTV_CHANNEL_2
		CASE MP_CCTV_CHANNEL_3
		CASE MP_CCTV_CHANNEL_4
		CASE MP_CCTV_CHANNEL_5
		CASE MP_CCTV_CHANNEL_6
		CASE MP_CCTV_CHANNEL_7
		CASE MP_CCTV_CHANNEL_8
		CASE MP_CCTV_CHANNEL_9
		CASE MP_CCTV_CHANNEL_10
		CASE MP_CCTV_CHANNEL_11
		CASE MP_CCTV_CHANNEL_12
		CASE MP_CCTV_CHANNEL_13
		CASE MP_CCTV_CHANNEL_14
			RETURN TVCHANNELTYPE_CHANNEL_SPECIAL
		BREAK
	ENDSWITCH
	
	RETURN TVCHANNELTYPE_CHANNEL_NONE
ENDFUNC

/// PURPOSE:
///    Prepares the local player to change channel
/// PARAMS:
///    MPCCTVLocal - Local data struct
///    channelFrom - Channel player is leaving
///    channelTo - Channel player is going to
PROC CLIENT_SETUP_SWITCH_CCTV_CHANNEL(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, MP_CCTV_CHANNEL channelFrom, MP_CCTV_CHANNEL channelTo)
	MPCCTVLocal.iChannelSwitchTimer = 0
	
	SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_SETUP_PROCESS)
	
	MPCCTVLocal.eSwitchChannelFrom = channelFrom
	MPCCTVLocal.eSwitchChannelTo = channelTo
	
	CLEAR_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_ACTIVATING)
	CLEAR_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_DEACTIVATING)
	
	SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_UPDATE_BUTTONS)
ENDPROC

/// PURPOSE:
///    Sets the channel type and playlist
/// PARAMS:
///    thisChannel - The channel the cctv is currently on
PROC SET_CCTV_CHANNEL_AND_PLAYLIST(MP_CCTV_CHANNEL thisChannel)
	TVCHANNELTYPE thisChannelType = GET_MP_CCTV_CHANNEL_TVCHANNELTYPE(thisChannel)
	
	IF thisChannelType = TVCHANNELTYPE_CHANNEL_SPECIAL
		SWITCH thisChannel
			CASE MP_CCTV_CHANNEL_0
			CASE MP_CCTV_CHANNEL_1
			CASE MP_CCTV_CHANNEL_2
			CASE MP_CCTV_CHANNEL_3
			CASE MP_CCTV_CHANNEL_4
			CASE MP_CCTV_CHANNEL_5
			CASE MP_CCTV_CHANNEL_6
			CASE MP_CCTV_CHANNEL_7
			CASE MP_CCTV_CHANNEL_8
			CASE MP_CCTV_CHANNEL_9
			CASE MP_CCTV_CHANNEL_10
			CASE MP_CCTV_CHANNEL_11
			CASE MP_CCTV_CHANNEL_12
			CASE MP_CCTV_CHANNEL_13
			CASE MP_CCTV_CHANNEL_14
				SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_SPECIAL, GET_XML_PLAYLIST_FOR_TV_PLAYLIST(TV_PLAYLIST_SPECIAL_MP_CCTV))
			BREAK
		ENDSWITCH
	ENDIF
	
	SET_TV_CHANNEL(thisChannelType)
ENDPROC

/// PURPOSE:
///    Maintains the cctv channel in the local player's current property
/// PARAMS:
///    MPCCTVClient - Client data struct
///    MPCCTVLocal - Local data struct
PROC CLIENT_MAINTAIN_AMBIENT_CCTV(MP_CCTV_CLIENT_DATA_STRUCT &MPCCTVClient, MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_WATCH_TV_CLEANUP)
		IF MPCCTVLocal.eCurrentChannel <> MPCCTVClient.eCurrentChannel
			CLIENT_SETUP_SWITCH_CCTV_CHANNEL(MPCCTVLocal, MPCCTVLocal.eCurrentChannel, MPCCTVClient.eCurrentChannel)
			
			MPCCTVLocal.tvChannelTypeCurrent = GET_MP_CCTV_CHANNEL_TVCHANNELTYPE(MPCCTVClient.eCurrentChannel)
			MPCCTVLocal.eCurrentChannel = MPCCTVClient.eCurrentChannel
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player is facing the specified cctv system
/// PARAMS:
///    MPCCTVLocal - Local data struct
/// RETURNS:
///    True when the player is facing the cctv
FUNC BOOL IS_PLAYER_FACING_CCTV(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	FLOAT fAngleDifference = (GET_ENTITY_HEADING(PLAYER_PED_ID()) - MPCCTVLocal.fAngledAreaEnterHeading + 180.0 + 360.0) % 360.0 - 180.0
	
	IF fAngleDifference <= MPCCTVLocal.fMaxHeadingDifference
	AND fAngleDifference >= -MPCCTVLocal.fMaxHeadingDifference
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    If bBlocked = TRUE then the activity will be blocked and help text will be
///    shown to explain why if it has been added to GET_BLOCK_HELP_FOR_INTERIOR().
PROC SET_CCTV_ACTIVITY_BLOCKED(MP_CCTV_LOCAL_DATA_STRUCT& MPCCTVLocal, BOOL bBlocked = TRUE)
	IF bBlocked
		SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_BLOCK_USE_OF_CCTV)
	ELSE
		CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_BLOCK_USE_OF_CCTV)
	ENDIF
ENDPROC

/// PURPOSE:
///    Some interiors may have a reason to block the use of the CCTV.
///    This function returns the text_label for the help text explaining
///    why the the cctv cannot be used.
FUNC BOOL GET_BLOCK_HELP_FOR_INTERIOR(MP_CCTV_LOCAL_DATA_STRUCT& MPCCTVLocal, TEXT_LABEL_15& txtBlock)
	SWITCH MPCCTVLocal.eSimpleInteriorType
		CASE SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1
			txtBlock = "AOC_CCTV_B"
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the angled area to activate the cctv
/// PARAMS:
///    MPCCTVLocal - Local data struct
///    propertyID - Simple interior property ID
PROC GET_CCTV_LOCATION(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, SIMPLE_INTERIORS propertyID)
	SWITCH propertyID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			MPCCTVLocal.vAngledAreaPos1 = <<1002.6198, -3194.9094, -39.9932>>
			MPCCTVLocal.vAngledAreaPos2 = <<1003.4985, -3194.8752, -37.9932>>
			MPCCTVLocal.fAngledAreaWidth = 1.0
			MPCCTVLocal.fAngledAreaEnterHeading = 0.0
			MPCCTVLocal.iNumberOfChannels = 3
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			MPCCTVLocal.vAngledAreaPos1 = <<1044.5847, -3197.5376, -39.1576>>
			MPCCTVLocal.vAngledAreaPos2 = <<1044.5686, -3195.8821, -37.1576>>
			MPCCTVLocal.fAngledAreaWidth = 1.0
			MPCCTVLocal.fAngledAreaEnterHeading = 270.0
			MPCCTVLocal.iNumberOfChannels = 3
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			MPCCTVLocal.vAngledAreaPos1 = <<1087.1406, -3198.7212, -39.9935>>
			MPCCTVLocal.vAngledAreaPos2 = <<1087.1588, -3197.5728, -37.9935>>
			MPCCTVLocal.fAngledAreaWidth = 1.0
			MPCCTVLocal.fAngledAreaEnterHeading = 90.0
			MPCCTVLocal.iNumberOfChannels = 3
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			MPCCTVLocal.vAngledAreaPos1 = <<1134.1075, -3194.2183, -41.3969>>
			MPCCTVLocal.vAngledAreaPos2 = <<1135.8831, -3194.2239, -39.3970>>
			MPCCTVLocal.fAngledAreaWidth = 1.0
			MPCCTVLocal.fAngledAreaEnterHeading = 0.0
			MPCCTVLocal.iNumberOfChannels = 3
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			MPCCTVLocal.vAngledAreaPos1 = <<1156.5852, -3194.7195, -40.0080>>
			MPCCTVLocal.vAngledAreaPos2 = <<1156.536377,-3193.427490,-38.0080>>
			MPCCTVLocal.fAngledAreaWidth = 1.0
			MPCCTVLocal.fAngledAreaEnterHeading = 90.0
			MPCCTVLocal.iNumberOfChannels = 3
		BREAK
		
		CASE SIMPLE_INTERIOR_BUNKER_1
		CASE SIMPLE_INTERIOR_BUNKER_2
		CASE SIMPLE_INTERIOR_BUNKER_3
		CASE SIMPLE_INTERIOR_BUNKER_4
		CASE SIMPLE_INTERIOR_BUNKER_5
		CASE SIMPLE_INTERIOR_BUNKER_6
		CASE SIMPLE_INTERIOR_BUNKER_7
		CASE SIMPLE_INTERIOR_BUNKER_9
		CASE SIMPLE_INTERIOR_BUNKER_10
		CASE SIMPLE_INTERIOR_BUNKER_11
		CASE SIMPLE_INTERIOR_BUNKER_12
			MPCCTVLocal.vAngledAreaPos1 = <<904.811401, -3209.336426, -98.187920>>
			MPCCTVLocal.vAngledAreaPos2 = <<906.539795, -3208.556396, -96.187920>>
			MPCCTVLocal.fAngledAreaWidth = 1.0
			MPCCTVLocal.fAngledAreaEnterHeading = 203.0
			MPCCTVLocal.iNumberOfChannels = 8
		BREAK
		
		CASE SIMPLE_INTERIOR_HANGAR_1
		CASE SIMPLE_INTERIOR_HANGAR_2
		CASE SIMPLE_INTERIOR_HANGAR_3
		CASE SIMPLE_INTERIOR_HANGAR_4
		CASE SIMPLE_INTERIOR_HANGAR_5
			MPCCTVLocal.vAngledAreaPos1 = <<-1235.615479, -3004.170654, -43.866962>>
			MPCCTVLocal.vAngledAreaPos2 = <<-1237.277832, -3004.178223, -41.866959>>
			MPCCTVLocal.fAngledAreaWidth = 1.5
			MPCCTVLocal.fAngledAreaEnterHeading = 180.0
			MPCCTVLocal.iNumberOfChannels = 13
		BREAK
		
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10
			MPCCTVLocal.vAngledAreaPos1 = <<413.489960, 4810.461426, -59.998138>>
			MPCCTVLocal.vAngledAreaPos2 = <<413.495911, 4811.547852, -57.998138>>
			MPCCTVLocal.fAngledAreaWidth = 1.0
			MPCCTVLocal.fAngledAreaEnterHeading = 90.0
			MPCCTVLocal.iNumberOfChannels = 13
		BREAK
			
		CASE SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1
			MPCCTVLocal.vAngledAreaPos1 = <<514.96, 4748.312012, -67.824974>>
			MPCCTVLocal.vAngledAreaPos2 = <<514.96, 4749.322754, -69.996033>>
			MPCCTVLocal.fAngledAreaWidth = 1.05
			MPCCTVLocal.fAngledAreaEnterHeading = GET_HEADING_FROM_COORDS_LA(MPCCTVLocal.vAngledAreaPos2, MPCCTVLocal.vAngledAreaPos1)
			MPCCTVLocal.fMaxHeadingDifference = 90.0
			MPCCTVLocal.iNumberOfChannels = 1
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
				MPCCTVLocal.vAngledAreaPos1 = <<-1617.427246, -3013.810791, -76.205040>>
				MPCCTVLocal.vAngledAreaPos2 = <<-1618.722656, -3014.107666, -74.205040>>
				MPCCTVLocal.fAngledAreaWidth = 0.75
				MPCCTVLocal.fAngledAreaEnterHeading = 190.0
				MPCCTVLocal.iNumberOfChannels = 4
			ELIF GET_INTERIOR_FLOOR_INDEX() = 0
				MPCCTVLocal.vAngledAreaPos1 = <<-1617.427246, -3013.810791, -76.205040>>
				MPCCTVLocal.vAngledAreaPos2 = <<-1618.722656, -3014.107666, -74.205040>>
				MPCCTVLocal.fAngledAreaWidth = 0.75
				MPCCTVLocal.fAngledAreaEnterHeading = 190.0
				MPCCTVLocal.iNumberOfChannels = 10
			ELIF GET_INTERIOR_FLOOR_INDEX() = 1
				MPCCTVLocal.vAngledAreaPos1 = <<-1507.160767,-3032.958984,-80.242226>>
				MPCCTVLocal.vAngledAreaPos2 = <<-1508.490723,-3032.815918,-78.242226>>
				MPCCTVLocal.fAngledAreaWidth = 1.0
				MPCCTVLocal.fAngledAreaEnterHeading = 160.0
				MPCCTVLocal.iNumberOfChannels = 7
			ELSE
				MPCCTVLocal.vAngledAreaPos1 = <<-1516.970703,-3038.768311,-80.242226>>
				MPCCTVLocal.vAngledAreaPos2 = <<-1515.877563,-3039.484863,-78.242226>>
				MPCCTVLocal.fAngledAreaWidth = 1.0
				MPCCTVLocal.fAngledAreaEnterHeading = 320.0
				MPCCTVLocal.iNumberOfChannels = 7
			ENDIF
		BREAK
		
		CASE SIMPLE_INTERIOR_ARENA_GARAGE_1
			IF GET_INTERIOR_FLOOR_INDEX() = 0
				MPCCTVLocal.vAngledAreaPos1 = <<164.368088,5185.633789,-89.675415>>
				MPCCTVLocal.vAngledAreaPos2 = <<164.368225,5187.065918,-87.675415>>
				MPCCTVLocal.fAngledAreaWidth = 1.4375
				MPCCTVLocal.fAngledAreaEnterHeading = 90.0
				MPCCTVLocal.iNumberOfChannels = 2
			ELSE
				MPCCTVLocal.vAngledAreaPos1 = <<167.377426,5192.402344,10.224731>>
				MPCCTVLocal.vAngledAreaPos2 = <<167.371536,5193.817871,12.224731>>
				MPCCTVLocal.fAngledAreaWidth = 1.4375
				MPCCTVLocal.fAngledAreaEnterHeading = 90.0
				MPCCTVLocal.iNumberOfChannels = 2
			ENDIF
		BREAK
		
		CASE SIMPLE_INTERIOR_INVALID
			IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
				MPCCTVLocal.vAngledAreaPos1 = <<-1617.427246, -3013.810791, -76.205040>>
				MPCCTVLocal.vAngledAreaPos2 = <<-1618.722656, -3014.107666, -74.205040>>
				MPCCTVLocal.fAngledAreaWidth = 0.75
				MPCCTVLocal.fAngledAreaEnterHeading = 190.0
				MPCCTVLocal.iNumberOfChannels = 15
			ENDIF
		BREAK
		
		CASE SIMPLE_INTERIOR_SUBMARINE
			MPCCTVLocal.vAngledAreaPos1 = <<1562.299072,384.797974,-57.087540>>
			MPCCTVLocal.vAngledAreaPos2 = <<1562.253174,382.725800,-55.087887>>
			MPCCTVLocal.fAngledAreaWidth = 1.0
			MPCCTVLocal.fAngledAreaEnterHeading = 270.0
			MPCCTVLocal.iNumberOfChannels = 7
		BREAK		
		
		CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
		CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
		CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON
		CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
		CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
			MPCCTVLocal.vAngledAreaPos1 = <<-1358.994751,141.712738,-96.109337>>
			MPCCTVLocal.vAngledAreaPos2 = <<-1359.009888,143.277985,-95.109337>>
			MPCCTVLocal.fAngledAreaWidth = 1.0
			MPCCTVLocal.fAngledAreaEnterHeading = 90.0
			MPCCTVLocal.iNumberOfChannels = 5
		BREAK

		#IF FEATURE_MUSIC_STUDIO
		CASE SIMPLE_INTERIOR_MUSIC_STUDIO
			MPCCTVLocal.vAngledAreaPos1 = <<-1016.206482,-83.438690,-100.403091>>
			MPCCTVLocal.vAngledAreaPos2 = <<-1017.486328,-83.446747,-98.403091>>
			MPCCTVLocal.fAngledAreaWidth = 1.0
			MPCCTVLocal.fAngledAreaEnterHeading = 180.0
			MPCCTVLocal.iNumberOfChannels = 7
		BREAK
		#ENDIF

		#IF FEATURE_FIXER
		CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
		CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
		CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
		CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
			MPCCTVLocal.vAngledAreaPos1 = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< -9.12275, 1.80637, 8.60001 >>, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
			MPCCTVLocal.vAngledAreaPos2 = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< -7.47049, 1.80492, 10.6 >>, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
			MPCCTVLocal.fAngledAreaWidth = 1.25
			MPCCTVLocal.fAngledAreaEnterHeading = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(0.000000, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
			MPCCTVLocal.iNumberOfChannels = 7
		BREAK
		#ENDIF
		
		#IF FEATURE_DLC_2_2022
		CASE SIMPLE_INTERIOR_JUGGALO_HIDEOUT
			MPCCTVLocal.vAngledAreaPos1 = <<578.175232,-2605.696289,-50.647034>>
			MPCCTVLocal.vAngledAreaPos2 = <<578.186218,-2609.228516,-48.397034>>
			MPCCTVLocal.fAngledAreaWidth = 1.5
			MPCCTVLocal.fAngledAreaEnterHeading = 180.0
			MPCCTVLocal.iNumberOfChannels = 2
		BREAK
		
		CASE SIMPLE_INTERIOR_MULTISTOREY_GARAGE
			MPCCTVLocal.vAngledAreaPos1 = <<519.622498,-2606.876221,-49.999920>>
			MPCCTVLocal.vAngledAreaPos2 = <<519.617798,-2605.421631,-47.749920>>
			MPCCTVLocal.fAngledAreaWidth = 1.5
			MPCCTVLocal.fAngledAreaEnterHeading = 180.0
			MPCCTVLocal.iNumberOfChannels = 4
		BREAK
		#ENDIF
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL IS_PROPERTY_CCTV_PURCHASED_OR_UNLOCKED(SIMPLE_INTERIORS propertyID)
	IF propertyID != SIMPLE_INTERIOR_INVALID
		SWITCH GET_SIMPLE_INTERIOR_TYPE(propertyID)
			CASE SIMPLE_INTERIOR_TYPE_BUSINESS_HUB
				RETURN IS_PLAYER_NIGHTCLUB_SECURITY_PURCHASED(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Maintains player declaring that they want to use the cctv
/// PARAMS:
///    MPCCTVLocal - Local data struct
/// RETURNS:
///    True when completed activating
FUNC BOOL CLIENT_MAINTAIN_ACTIVATING_CCTV(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, BOOL bAlternateButtonPress = FALSE, SIMPLE_INTERIORS propertyID = SIMPLE_INTERIOR_INVALID)
	BOOL bReadyForContext = FALSE
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
	AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
	AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
	AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND IS_PROPERTY_CCTV_PURCHASED_OR_UNLOCKED(propertyID)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), MPCCTVLocal.vAngledAreaPos1, MPCCTVLocal.vAngledAreaPos2, MPCCTVLocal.fAngledAreaWidth)
		AND IS_PLAYER_FACING_CCTV(MPCCTVLocal)
		AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
			IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_BLOCK_USE_OF_CCTV)
				IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISPLAYED_BLOCK_HELP)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					TEXT_LABEL_15 txtBlock
					
					IF GET_BLOCK_HELP_FOR_INTERIOR(MPCCTVLocal, txtBlock)
						SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISPLAYED_BLOCK_HELP)
						
						PRINT_HELP(txtBlock)
					ENDIF
				ENDIF
			ELSE
				bReadyForContext = TRUE
			ENDIF
		ELSE
			TEXT_LABEL_15 txtBlock
			
			IF GET_BLOCK_HELP_FOR_INTERIOR(MPCCTVLocal, txtBlock)
			AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(txtBlock)
				CLEAR_HELP()
			ENDIF
			
			CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISPLAYED_BLOCK_HELP)
		ENDIF
	ENDIF
	
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		RELEASE_CONTEXT_INTENTION(MPCCTVLocal.iCCTVContextIntention)
		
		MPCCTVLocal.iCCTVContextIntention = NEW_CONTEXT_INTENTION
	ENDIF
	
	IF bReadyForContext
	AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
		IF bInPropertyOwnerNotPaidLastUtilityBill
			IF MPCCTVLocal.iCCTVContextIntention = NEW_CONTEXT_INTENTION
				REGISTER_CONTEXT_INTENTION(MPCCTVLocal.iCCTVContextIntention, CP_HIGH_PRIORITY, "MPCCTV_BILL")
			ENDIF
		ELSE
			IF MPCCTVLocal.iCCTVContextIntention = NEW_CONTEXT_INTENTION
			AND propertyID != SIMPLE_INTERIOR_ARENA_GARAGE_1
				IF MPCCTVLocal.iNumberOfChannels = 1
					REGISTER_CONTEXT_INTENTION(MPCCTVLocal.iCCTVContextIntention, CP_HIGH_PRIORITY, "MPCCTV_START_1")
				ELSE
					REGISTER_CONTEXT_INTENTION(MPCCTVLocal.iCCTVContextIntention, CP_HIGH_PRIORITY, "MPCCTV_START")
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPCCTV_START")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPCCTV_START_1")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_MANAGE")
					IF propertyID != SIMPLE_INTERIOR_ARENA_GARAGE_1
						IF HAS_CONTEXT_BUTTON_TRIGGERED(MPCCTVLocal.iCCTVContextIntention)
							RELEASE_CONTEXT_INTENTION(MPCCTVLocal.iCCTVContextIntention)
							
							GET_CCTV_LOCATION(MPCCTVLocal, propertyID)
							
							RETURN TRUE
						ENDIF
					ELSE
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
						AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
							RELEASE_CONTEXT_INTENTION(MPCCTVLocal.iCCTVContextIntention)
							
							GET_CCTV_LOCATION(MPCCTVLocal, propertyID)
							
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RELEASE_CONTEXT_INTENTION(MPCCTVLocal.iCCTVContextIntention)
	ENDIF
	
	IF bAlternateButtonPress
		SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
		
		GET_CCTV_LOCATION(MPCCTVLocal, propertyID)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets up the client activating the cctv
/// PARAMS:
///    MPCCTVLocal - Local data struct
///    channelTo - Channel to begin with
PROC CLIENT_SETUP_CCTV_ACTIVATING(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, MP_CCTV_CHANNEL channelTo)
	SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_UPDATE_BUTTONS)
	
	CLIENT_SETUP_SWITCH_CCTV_CHANNEL(MPCCTVLocal, channelTo, channelTo)
	
	SET_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_ACTIVATING)
	
	g_bMPTVplayerWatchingTV = TRUE
	g_bUsingCCTV = TRUE
ENDPROC

/// PURPOSE:
///    Destroy and make inactive the camera used when activated the cctv
/// PARAMS:
///    MPCCTVLocal - Local data struct
/// RETURNS:
///    True when complete
FUNC BOOL CLIENT_CLEANUP_CCTV_CAMERA(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FOCUS_SET)
		CLEAR_FOCUS()
		
		NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
		
		CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FOCUS_SET)
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("WATCHING_SAFEHOUSE_TV")
		STOP_AUDIO_SCENE("WATCHING_SAFEHOUSE_TV")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_CCTV_SCENE")
		STOP_AUDIO_SCENE("MP_CCTV_SCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("dlc_aw_arena_cctv_scene")
		STOP_AUDIO_SCENE("dlc_aw_arena_cctv_scene")
	ENDIF
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_UPDATE_BUTTONS)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns if the specified channel is a cctv channel
/// PARAMS:
///    thisChannel - channel ID to check
/// RETURNS:
///    True if the channel is a cctv channel
FUNC BOOL IS_MP_CCTV_CHANNEL_CCTV(MP_CCTV_CHANNEL thisChannel)
	IF thisChannel = MP_CCTV_CHANNEL_0
	OR thisChannel = MP_CCTV_CHANNEL_1
	OR thisChannel = MP_CCTV_CHANNEL_2
	OR thisChannel = MP_CCTV_CHANNEL_3
	OR thisChannel = MP_CCTV_CHANNEL_4
	OR thisChannel = MP_CCTV_CHANNEL_5
	OR thisChannel = MP_CCTV_CHANNEL_6
	OR thisChannel = MP_CCTV_CHANNEL_7
	OR thisChannel = MP_CCTV_CHANNEL_8
	OR thisChannel = MP_CCTV_CHANNEL_9
	OR thisChannel = MP_CCTV_CHANNEL_10
	OR thisChannel = MP_CCTV_CHANNEL_11
	OR thisChannel = MP_CCTV_CHANNEL_12
	OR thisChannel = MP_CCTV_CHANNEL_13
	OR thisChannel = MP_CCTV_CHANNEL_14
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if the specified channel is fullscreen
/// PARAMS:
///    thisChannel - channel ID to check
/// RETURNS:
///    True if the channel is fullscreen
FUNC BOOL IS_MP_CCTV_CHANNEL_FULLSCREEN(MP_CCTV_CHANNEL thisChannel)
	IF IS_MP_CCTV_CHANNEL_CCTV(thisChannel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Set up CCTV_DETAILS with default values
PROC INIT_CCTV_DETAILS(CCTV_DETAILS& cctvDetails)
	cctvDetails.vPos = <<0.0, 0.0, 0.0>>
	cctvDetails.vRot = <<0.0, 0.0, 0.0>>
	cctvDetails.fFov = 50.0
	cctvDetails.fMaxAngleRight = 35.0
	cctvDetails.fMaxAngleLeft = 35.0
ENDPROC

/// PURPOSE:
///    Returns if the specified channel is fullscreen
/// PARAMS:
///    propertyID - Simple interior ID
///    thisChannel - channel ID to check
FUNC CCTV_DETAILS GET_CCTV_DETAILS(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, SIMPLE_INTERIORS propertyID, INT iChannel)
	CCTV_DETAILS tempCCTVDetails
	
	INIT_CCTV_DETAILS(tempCCTVDetails)
	
	SWITCH propertyID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<53.044537, 6338.279785, 35.576405>>
					tempCCTVDetails.vRot = <<-20.291430, -0.000000, 29.026625>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<64.660690, 6323.345215, 36.444351>>
					tempCCTVDetails.vRot = <<-12.315643, -0.000000, -63.485329>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<26.355196, 6292.989746, 36.464302>>
					tempCCTVDetails.vRot = <<-22.365654, 0.000001, -150.058121>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<416.021362, 6520.855469, 31.951921>>
					tempCCTVDetails.vRot = <<-20.848019, -0.000002, -96.632568>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<408.311798, 6498.621094, 31.899664>>
					tempCCTVDetails.vRot = <<-16.912912, -0.000002, 173.769547>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<404.053284, 6510.458496, 30.462669>>
					tempCCTVDetails.vRot = <<-26.527725, -0.000002, 83.864922>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<51.506763, 6485.769043, 33.954098>>
					tempCCTVDetails.vRot = <<-14.934117, -0.000001, -44.504852>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<29.087170, 6493.841309, 35.320789>>
					tempCCTVDetails.vRot = <<-24.809351, -0.000000, 45.122974>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<50.186249, 6466.695801, 36.465118>>
					tempCCTVDetails.vRot = <<-12.809641, -0.000001, -134.553497>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<-415.270538, 6172.916504, 34.035480>>
					tempCCTVDetails.vRot = <<-12.367702, -0.000000, -45.178719>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<-420.364258, 6169.537598, 34.505043>>
					tempCCTVDetails.vRot = <<-13.415112, -0.000000, 45.588474>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<-432.143951, 6142.139648, 35.027729>>
					tempCCTVDetails.vRot = <<-13.103918, -0.000000, 135.526367>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<-163.597382, 6334.842773, 33.279037>>
					tempCCTVDetails.vRot = <<-10.390980, -0.000000, -44.580807>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<-179.246689, 6336.390625, 34.745560>>
					tempCCTVDetails.vRot = <<-15.393289, -0.000000, 44.813339>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<-177.565979, 6322.649414, 34.591667>>
					tempCCTVDetails.vRot = <<-15.976838, -0.000001, 135.771606>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<1454.895386, -1652.358032, 68.715477>>
					tempCCTVDetails.vRot = <<-16.378693, -0.000000, 25.275003>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<1440.651245, -1667.659546, 68.727356>>
					tempCCTVDetails.vRot = <<-20.347452, -0.000000, 115.770973>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<1453.828613, -1681.166016, 67.857277>>
					tempCCTVDetails.vRot = <<-10.715320, -0.000000, -156.097610>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<104.073586, 175.171570, 109.181511>>
					tempCCTVDetails.vRot = <<-13.633097, -0.000000, 159.659744>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<141.933090, 173.649460, 110.917458>>
					tempCCTVDetails.vRot = <<-15.013286, 0.000000, -110.529655>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<91.711914, 186.421783, 110.211533>>
					tempCCTVDetails.vRot = <<-14.092422, 0.000000, 160.467545>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<-1462.816284, -381.767914, 40.904587>>
					tempCCTVDetails.vRot = <<-10.794255, 0.000003, -133.724838>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<-1463.831177, -375.137909, 41.898960>>
					tempCCTVDetails.vRot = <<-15.232119, 0.000003, -45.097332>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<-1481.394409, -395.464752, 42.518887>>
					tempCCTVDetails.vRot = <<-10.884719, 0.000003, 136.147141>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<-1172.235107, -1374.359985, 9.462887>>
					tempCCTVDetails.vRot = <<-20.665766, -0.000000, 114.435753>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<-1165.741943, -1392.398682, 9.954999>>
					tempCCTVDetails.vRot = <<-21.275417, 0.000001, 124.640594>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<-1152.495239, -1394.404297, 9.979074>>
					tempCCTVDetails.vRot = <<-15.943830, -0.000000, -147.265854>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<299.279419, -759.965942, 32.101238>>
					tempCCTVDetails.vRot = <<-12.645959, 0.000000, -109.818398>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<306.632355, -735.047791, 35.795044>>
					tempCCTVDetails.vRot = <<-16.035807, 0.000001, -110.149384>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<288.350708, -788.325195, 38.857380>>
					tempCCTVDetails.vRot = <<-13.646616, 0.000001, -108.914078>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<201.595963, 2462.414795, 60.471066>>
					tempCCTVDetails.vRot = <<-15.729035, -0.000001, -159.424728>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<206.824173, 2470.720947, 59.743488>>
					tempCCTVDetails.vRot = <<-11.786305, -0.000000, -69.749527>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<192.132950, 2472.516602, 61.324230>>
					tempCCTVDetails.vRot = <<-13.728151, -0.000001, 20.188499>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<2848.786377, 4450.369141, 52.108841>>
					tempCCTVDetails.vRot = <<-13.006576, -0.000000, 107.267784>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<2855.093994, 4446.513184, 52.117870>>
					tempCCTVDetails.vRot = <<-6.434129, -0.000000, -161.662598>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<2870.139160, 4462.503906, 51.984428>>
					tempCCTVDetails.vRot = <<-6.523989, -0.000000, 16.625057>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<387.553192, 3584.505371, 35.755802>>
					tempCCTVDetails.vRot = <<-13.181582, -0.000000, -9.595942>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<393.706696, 3575.107666, 35.679443>>
					tempCCTVDetails.vRot = <<-11.988330, -0.000000, -99.717102>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<385.075958, 3565.494629, 36.159134>>
					tempCCTVDetails.vRot = <<-11.839277, -0.000000, 170.156143>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<634.488159, 2778.419678, 45.218670>>
					tempCCTVDetails.vRot = <<-10.052352, -0.000000, -86.241074>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<644.718628, 2756.314209, 45.951244>>
					tempCCTVDetails.vRot = <<-13.886223, -0.000000, -86.165863>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<628.885803, 2806.359619, 46.090553>>
					tempCCTVDetails.vRot = <<-11.964497, -0.000000, 4.249068>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<1657.381958, 4852.822266, 47.648918>>
					tempCCTVDetails.vRot = <<-26.832968, -0.000000, -82.379791>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<1641.624634, 4855.610840, 45.677814>>
					tempCCTVDetails.vRot = <<-20.788713, -0.000000, 101.852287>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<1656.013306, 4830.787109, 46.266159>>
					tempCCTVDetails.vRot = <<-13.982306, -0.000000, -171.552322>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<1181.652344, -3113.746094, 9.460971>>
					tempCCTVDetails.vRot = <<-8.852775, 0.000000, 90.217949>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<1197.159058, -3110.294678, 10.403809>>
					tempCCTVDetails.vRot = <<-9.161101, -0.000000, 0.242041>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<1242.043213, -3127.195068, 11.115586>>
					tempCCTVDetails.vRot = <<-11.686866, -0.000000, -89.523148>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<135.154724, -2473.972168, 8.920205>>
					tempCCTVDetails.vRot = <<-13.883119, -0.000000, -124.695915>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<126.134537, -2474.714600, 8.451329>>
					tempCCTVDetails.vRot = <<-8.696476, 0.000000, 144.686722>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<124.327095, -2464.508789, 8.597489>>
					tempCCTVDetails.vRot = <<-12.341438, 0.000000, 55.391891>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<-252.639114, -2586.016113, 9.861881>>
					tempCCTVDetails.vRot = <<-8.431856, 0.000000, 90.272736>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<-241.574844, -2597.194824, 9.264318>>
					tempCCTVDetails.vRot = <<-7.870702, 0.000000, 179.669846>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<-240.240158, -2575.400635, 8.882662>>
					tempCCTVDetails.vRot = <<-11.562041, -0.000000, 0.252795>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<671.804321, -2665.776855, 10.368782>>
					tempCCTVDetails.vRot = <<-13.092704, -0.000000, 89.807472>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<666.900513, -2700.553467, 10.408920>>
					tempCCTVDetails.vRot = <<-11.828203, -0.000000, 62.939022>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<684.716003, -2692.752686, 11.222744>>
					tempCCTVDetails.vRot = <<-18.115387, -0.000000, -179.863464>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<-331.288879, -2778.939941, 8.419658>>
					tempCCTVDetails.vRot = <<-11.729095, -0.000000, 90.222351>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<-324.012268, -2768.337646, 8.039400>>
					tempCCTVDetails.vRot = <<-17.160858, -0.000000, -0.141336>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<-296.165405, -2772.214600, 8.997739>>
					tempCCTVDetails.vRot = <<-9.678322, -0.000000, 0.103397>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_BUNKER_1
		CASE SIMPLE_INTERIOR_BUNKER_2
		CASE SIMPLE_INTERIOR_BUNKER_3
		CASE SIMPLE_INTERIOR_BUNKER_4
		CASE SIMPLE_INTERIOR_BUNKER_5
		CASE SIMPLE_INTERIOR_BUNKER_6
		CASE SIMPLE_INTERIOR_BUNKER_7
		CASE SIMPLE_INTERIOR_BUNKER_9
		CASE SIMPLE_INTERIOR_BUNKER_10
		CASE SIMPLE_INTERIOR_BUNKER_11
		CASE SIMPLE_INTERIOR_BUNKER_12
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<890.2434, -3225.9353, -95.9247>>
					tempCCTVDetails.vRot = <<-21.6518, 0.0000, -69.0302>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<894.8196, -3196.8809, -96.2448>>
					tempCCTVDetails.vRot = <<-20.7705, 0.0000, 153.3051>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<865.0455, -3243.0938, -94.8266>>
					tempCCTVDetails.vRot = <<-15.1147, -0.0000, -93.1612>>
				BREAK
				
				CASE 3
					tempCCTVDetails.vPos = <<948.0815, -3238.6194, -97.0305>>
					tempCCTVDetails.vRot = <<-14.6354, -0.0000, 45.9784>>
				BREAK
				
				CASE 4
					tempCCTVDetails.vPos = <<853.6488, -3246.5701, -94.0656>>
					tempCCTVDetails.vRot = <<-14.8117, 0.0000, 51.5520>>
				BREAK
				
				CASE 5
					tempCCTVDetails.vPos = <<896.8334, -3166.6987, -95.6694>>
					tempCCTVDetails.vRot = <<-20.6148, -0.0000, 170.5047>>
				BREAK
				
				CASE 6
					tempCCTVDetails.vPos = <<868.5811, -3227.4724, -95.2059>>
					tempCCTVDetails.vRot = <<-13.2581, 0.0000, -11.0253>>
				BREAK
				
				CASE 7
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_BUNKER_1
							tempCCTVDetails.vPos = <<525.4482, 3017.8984, 50.7942>>
							tempCCTVDetails.vRot = <<-13.3451, -0.0000, 108.5488>>
						BREAK
						
						CASE SIMPLE_INTERIOR_BUNKER_2
							tempCCTVDetails.vPos = <<869.1528, 3098.1272, 53.4888>>
							tempCCTVDetails.vRot = <<-7.7394, -0.0000, 164.5715>>
						BREAK
						
						CASE SIMPLE_INTERIOR_BUNKER_3
							tempCCTVDetails.vPos = <<18.8320, 2894.5935, 68.1748>>
							tempCCTVDetails.vRot = <<-13.5967, -0.0000, -19.9631>>
						BREAK
						
						CASE SIMPLE_INTERIOR_BUNKER_4
							tempCCTVDetails.vPos = <<1567.7930, 2198.8367, 89.2940>>
							tempCCTVDetails.vRot = <<-16.7729, -0.0000, -8.5119>>
						BREAK
						
						CASE SIMPLE_INTERIOR_BUNKER_5
							tempCCTVDetails.vPos = <<2091.0491, 3290.1860, 56.1720>>
							tempCCTVDetails.vRot = <<-10.8610, -0.0000, -34.2396>>
						BREAK
						
						CASE SIMPLE_INTERIOR_BUNKER_6
							tempCCTVDetails.vPos = <<2501.7180, 3205.5344, 60.0577>>
							tempCCTVDetails.vRot = <<-11.8937, -0.0000, 168.1174>>
						BREAK
						
						CASE SIMPLE_INTERIOR_BUNKER_7
							tempCCTVDetails.vPos = <<1778.6304, 4722.6060, 50.4819>>
							tempCCTVDetails.vRot = <<-18.7885, -0.0000, -119.7170>>
						BREAK
						
						CASE SIMPLE_INTERIOR_BUNKER_9
							tempCCTVDetails.vPos = <<-725.9142, 5959.9624, 32.9627>>
							tempCCTVDetails.vRot = <<-16.6971, 0.0000, 116.3382>>
						BREAK
						
						CASE SIMPLE_INTERIOR_BUNKER_10
							tempCCTVDetails.vPos = <<-348.1686, 4333.4780, 69.2909>>
							tempCCTVDetails.vRot = <<-17.2101, -0.0000, 67.2697>>
						BREAK
						
						CASE SIMPLE_INTERIOR_BUNKER_11
							tempCCTVDetails.vPos = <<-3000.8818, 3351.3923, 22.1161>>
							tempCCTVDetails.vRot = <<-14.6195, -0.0000, 113.1823>>
						BREAK
						
						CASE SIMPLE_INTERIOR_BUNKER_12
							tempCCTVDetails.vPos = <<-3126.7839, 1327.5712, 30.4150>>
							tempCCTVDetails.vRot = <<-11.6873, -0.0000, 44.9398>>
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HANGAR_1
		CASE SIMPLE_INTERIOR_HANGAR_2
		CASE SIMPLE_INTERIOR_HANGAR_3
		CASE SIMPLE_INTERIOR_HANGAR_4
		CASE SIMPLE_INTERIOR_HANGAR_5
			SWITCH iChannel
				CASE 0 // Main Hangar
					tempCCTVDetails.vPos = <<-1266.7141, -2960.6045, -32.3648>>
					tempCCTVDetails.vRot = <<-21.7804, 0.0000, 179.9788>>
				BREAK
				
				CASE 1 // Main Hangar
					tempCCTVDetails.vPos = <<-1247.1694, -3039.6184, -31.5676>>
					tempCCTVDetails.vRot = <<-23.8032, -0.0000, 58.3744>>
				BREAK
				
				CASE 2 // Main Hangar
					tempCCTVDetails.vPos = <<-1243.6936, -3025.4272, -41.7906>>
					tempCCTVDetails.vRot = <<-17.9761, -0.0000, 60.1254>>
				BREAK
				
				CASE 3 // Office
					tempCCTVDetails.vPos = <<-1234.5718, -3004.6858, -41.6271>>
					tempCCTVDetails.vRot = <<-3.7283, -0.0000, 52.3780>>
				BREAK
				
				CASE 4 // Office
					tempCCTVDetails.vPos = <<-1234.6686, -3009.8311, -41.3027>>
					tempCCTVDetails.vRot = <<-13.7233, -0.0000, 131.1660>>
				BREAK
				
				CASE 5 // Living Quarters
					tempCCTVDetails.vPos = <<-1241.4517, -2986.4043, -39.2859>>
					tempCCTVDetails.vRot = <<-24.4980, -0.0000, -47.5178>>
				BREAK
				
				CASE 6 // Cargo Storage
					tempCCTVDetails.vPos = <<-1244.8625, -2994.2810, -44.4609>>
					tempCCTVDetails.vRot = <<-24.2161, -0.0000, -131.0951>>
				BREAK
				
				CASE 7 // Aircraft Workshop
					tempCCTVDetails.vPos = <<-1299.6282, -3029.0010, -46.6098>>
					tempCCTVDetails.vRot = <<-25.0273, 0.0000, -140.8786>>
				BREAK
				
				CASE 8 // Aircraft Workshop
					tempCCTVDetails.vPos = <<-1299.6519, -3023.3657, -46.6202>>
					tempCCTVDetails.vRot = <<-18.7785, 0.0000, -39.3453>>
				BREAK
				
				CASE 9 // Aircraft Workshop
					tempCCTVDetails.vPos = <<-1291.1545, -2999.7117, -45.8598>>
					tempCCTVDetails.vRot = <<-22.3195, 0.0000, 142.7513>>
				BREAK
				
				CASE 10 // Exterior
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_HANGAR_1
							tempCCTVDetails.vPos = <<-1152.9023, -3412.2849, 28.4437>>
							tempCCTVDetails.vRot = <<-37.4905, -0.0000, -29.7840>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_2
							tempCCTVDetails.vPos = <<-1396.7550, -3268.9482, 24.6771>>
							tempCCTVDetails.vRot = <<-16.4052, -0.0000, -29.9211>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_3
							tempCCTVDetails.vPos = <<-2021.4349, 3157.5059, 51.4010>>
							tempCCTVDetails.vRot = <<-24.0335, -0.0000, 149.4360>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_4
							tempCCTVDetails.vPos = <<-1877.0586, 3110.5024, 42.5054>>
							tempCCTVDetails.vRot = <<-17.5001, -0.5679, 149.0176>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_5
							tempCCTVDetails.vPos = <<-2468.6094, 3277.6565, 42.3081>>
							tempCCTVDetails.vRot = <<-14.5116, -0.0000, 150.5079>>
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 11 // Exterior
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_HANGAR_1
							tempCCTVDetails.vPos = <<-1174.1511, -3317.9897, 38.5468>>
							tempCCTVDetails.vRot = <<-15.3540, -0.0000, -179.4439>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_2
							tempCCTVDetails.vPos = <<-1299.0039, -3232.2813, 29.2827>>
							tempCCTVDetails.vRot = <<-7.7357, -0.0000, 113.3930>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_3
							tempCCTVDetails.vPos = <<-2062.5352, 3135.2490, 50.5203>>
							tempCCTVDetails.vRot = <<-13.1370, -0.0000, -55.9709>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_4
							tempCCTVDetails.vPos = <<-1933.7417, 3060.6985, 50.5271>>
							tempCCTVDetails.vRot = <<-7.5039, -0.5679, -48.8860>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_5
							tempCCTVDetails.vPos = <<-2503.3589, 3207.9578, 45.3524>>
							tempCCTVDetails.vRot = <<-4.5594, -0.0000, -27.2466>>
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 12 // Exterior
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_HANGAR_1
							tempCCTVDetails.vPos = <<-1198.8156, -3383.6743, 35.2730>>
							tempCCTVDetails.vRot = <<-20.8371, -0.0000, 23.6413>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_2
							tempCCTVDetails.vPos = <<-1434.6611, -3272.3689, 28.9208>>
							tempCCTVDetails.vRot = <<-38.6760, -0.0000, 60.4474>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_3
							tempCCTVDetails.vPos = <<-1979.5597, 3229.5740, 55.7301>>
							tempCCTVDetails.vRot = <<-19.8524, 0.0000, -30.7328>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_4
							tempCCTVDetails.vPos = <<-1850.4392, 3156.6677, 47.8863>>
							tempCCTVDetails.vRot = <<-7.1437, -0.5679, -29.9480>>
						BREAK
						
						CASE SIMPLE_INTERIOR_HANGAR_5
							tempCCTVDetails.vPos = <<-2428.2756, 3315.3081, 42.3780>>
							tempCCTVDetails.vRot = <<-9.8226, -0.0000, -67.5922>>
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10
			SWITCH iChannel
				CASE 0 // Heist Planning Room
					tempCCTVDetails.vPos = <<344.3917, 4865.6978, -56.3980>>
					tempCCTVDetails.vRot = <<-18.0056, -0.0000, -42.3426>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 1 // Personal Quarters
					tempCCTVDetails.vPos = <<368.1584, 4819.4253, -56.5899>>
					tempCCTVDetails.vRot = <<-21.2136, 0.0000, -0.3729>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 2 // War Room
					tempCCTVDetails.vPos = <<321.3082, 4823.8413, -57.3565>>
					tempCCTVDetails.vRot = <<-18.0600, -0.0000, -55.2825>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 3 // Security Room
					tempCCTVDetails.vPos = <<424.5839, 4812.8320, -56.7062>>
					tempCCTVDetails.vRot = <<-20.3526, -0.0000, 88.6615>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 4 // Lobby
					tempCCTVDetails.vPos = <<336.6777, 4840.6030, -54.0224>>
					tempCCTVDetails.vRot = <<-26.2232, -0.0000, -108.6284>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 5 // Lobby
					tempCCTVDetails.vPos = <<377.3731, 4830.1611, -54.2200>>
					tempCCTVDetails.vRot = <<-19.7126, -0.0000, -93.1280>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 6 // Lobby
					tempCCTVDetails.vPos = <<407.7065, 4819.7725, -55.1168>>
					tempCCTVDetails.vRot = <<-20.5935, -0.0000, -2.4167>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 7 // Office
					tempCCTVDetails.vPos = <<375.3512, 4836.3203, -60.7220>>
					tempCCTVDetails.vRot = <<-10.6738, 0.0000, -9.8860>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 8 // Office
					tempCCTVDetails.vPos = <<340.9841, 4849.2451, -60.5910>>
					tempCCTVDetails.vRot = <<-17.6706, -0.0000, -30.4436>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 9 //Lounge
					tempCCTVDetails.vPos = <<369.7408, 4837.7773, -56.6573>>
					tempCCTVDetails.vRot = <<-22.3600, -0.0000, 24.2148>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 10 // Garage
					tempCCTVDetails.vPos = <<473.1380, 4848.0083, -48.9174>>
					tempCCTVDetails.vRot = <<-9.5226, -0.0000, -165.4840>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 11 // Garage
					tempCCTVDetails.vPos = <<496.1463, 4760.9927, -47.1770>>
					tempCCTVDetails.vRot = <<-17.8471, 0.0000, 15.4202>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 12 // Exterior
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1
							tempCCTVDetails.vPos = <<1321.9092, 2767.9348, 58.6325>>
							tempCCTVDetails.vRot = <<-5.4056, -0.0000, 19.9110>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2
							tempCCTVDetails.vPos = <<-44.2979, 2626.0750, 93.5399>>
							tempCCTVDetails.vRot = <<-3.4063, -0.0000, -107.7361>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3
							tempCCTVDetails.vPos = <<2722.2693, 3922.4773, 51.1632>>
							tempCCTVDetails.vRot = <<-4.9664, 0.0000, -103.7161>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4
							tempCCTVDetails.vPos = <<3445.6399, 5509.9253, 31.5132>>
							tempCCTVDetails.vRot = <<-8.3273, -0.0000, 122.9756>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6
							tempCCTVDetails.vPos = <<12.5605, 6803.4839, 30.4596>>
							tempCCTVDetails.vRot = <<-23.3582, 0.0000, 34.7273>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7
							tempCCTVDetails.vPos = <<-2364.3711, 2471.2854, 13.7183>>
							tempCCTVDetails.vRot = <<-6.7226, -0.0000, -84.7840>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8
							tempCCTVDetails.vPos = <<7.1126, 3309.6191, 50.6407>>
							tempCCTVDetails.vRot = <<-12.3649, -0.0000, -53.0013>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9
							tempCCTVDetails.vPos = <<2122.2544, 1750.5140, 117.7228>>
							tempCCTVDetails.vRot = <<-20.4740, -0.0000, 88.3105>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10
							tempCCTVDetails.vPos = <<1809.9996, 279.4279, 179.9132>>
							tempCCTVDetails.vRot = <<-13.1372, 0.0000, -90.2361>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1
			tempCCTVDetails.vPos = <<0.0, 6.2, 0.85>>
			tempCCTVDetails.vRot = <<340.0, 0.0, 0.0>>
			tempCCTVDetails.fMaxAngleLeft = 10.0
			tempCCTVDetails.fMaxAngleRight = 10.0
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
				SWITCH iChannel
					CASE 0 // DJ Front
						tempCCTVDetails.vPos = <<-1601.0083, -3012.6379, -77.0830>>
						tempCCTVDetails.vRot = <<-14.0325, -0.0000, 88.6520>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 1 // DJ Left
						tempCCTVDetails.vPos = <<-1602.1608, -3014.3564, -77.6091>>
						tempCCTVDetails.vRot = <<3.3017, 0.0000, 25.3270>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 2 // DJ Right
						tempCCTVDetails.vPos = <<-1603.3336, -3010.8794, -76.6271>>
						tempCCTVDetails.vRot = <<-23.6145, -0.0000, -148.6305>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 3 // DJ Top Down
						tempCCTVDetails.vPos = <<-1604.3833, -3012.6130, -76.1612>>
						tempCCTVDetails.vRot = <<-36.6228, -0.0000, -89.6699>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
				ENDSWITCH
			ELIF GET_INTERIOR_FLOOR_INDEX() = 0
				SWITCH iChannel
					CASE 0 // Exterior
						SWITCH propertyID
							CASE SIMPLE_INTERIOR_HUB_LA_MESA
								tempCCTVDetails.vPos = <<754.8915, -1332.4741, 29.6108>>
								tempCCTVDetails.vRot = <<-1.6485, -0.0000, -129.3100>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
								tempCCTVDetails.vPos = <<345.4364, -985.8168, 32.0621>>
								tempCCTVDetails.vRot = <<-1.4536, -0.0000, -37.2926>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
								tempCCTVDetails.vPos = <<-121.0578, -1256.3619, 31.9582>>
								tempCCTVDetails.vRot = <<-16.4409, -0.0000, -85.1727>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
								tempCCTVDetails.vPos = <<3.2740, 217.3130, 109.7653>>
								tempCCTVDetails.vRot = <<-12.0311, -0.0000, -113.0151>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
								tempCCTVDetails.vPos = <<870.9982, -2103.5308, 33.1736>>
								tempCCTVDetails.vRot = <<-20.0017, 0.0000, 81.5805>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
								tempCCTVDetails.vPos = <<-671.1730, -2461.2136, 17.9679>>
								tempCCTVDetails.vRot = <<-32.3471, 0.0000, 152.0553>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
								tempCCTVDetails.vPos = <<195.6193, -3178.7661, 8.4317>>
								tempCCTVDetails.vRot = <<-7.5870, -0.0000, 90.5970>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
								tempCCTVDetails.vPos = <<364.8457, 256.2720, 105.5142>>
								tempCCTVDetails.vRot = <<-5.4263, 0.0000, -19.9736>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
								tempCCTVDetails.vPos = <<-1278.8357, -645.9271, 30.2541>>
								tempCCTVDetails.vRot = <<-11.6321, 0.0000, 69.1304>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
								tempCCTVDetails.vPos = <<-1177.1744, -1143.8691, 9.5191>>
								tempCCTVDetails.vRot = <<0.5421, -0.0000, -82.4883>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
						ENDSWITCH
					BREAK
					
					CASE 1 // Lower Bar
						tempCCTVDetails.vPos = <<-1574.9905, -3011.0537, -77.6291>>
						tempCCTVDetails.vRot = <<-17.0175, 0.0000, 134.0558>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 2 // Upper Bar
						tempCCTVDetails.vPos = <<-1583.6477, -3015.4976, -74.5473>>
						tempCCTVDetails.vRot = <<-14.0434, -0.0000, 40.6984>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 3 // VIP Area
						tempCCTVDetails.vPos = <<-1602.2249, -3003.8098, -74.4226>>
						tempCCTVDetails.vRot = <<-16.6589, -0.0000, -69.5079>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 4 // Dance Floor
						tempCCTVDetails.vPos = <<-1591.6570, -3008.4016, -77.0773>>
						tempCCTVDetails.vRot = <<-18.4232, 0.0000, 129.7397>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 5 // DJ Booth
						tempCCTVDetails.vPos = <<-1606.4279, -3019.3293, -76.1958>>
						tempCCTVDetails.vRot = <<-24.9896, -0.0000, -39.2612>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 6 // Office Entrance
						tempCCTVDetails.vPos = <<-1606.9618, -3005.0830, -74.4389>>
						tempCCTVDetails.vRot = <<-17.1557, -0.0000, -131.0556>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 7 // Office
						tempCCTVDetails.vPos = <<-1620.2773, -3016.5281, -72.5882>>
						tempCCTVDetails.vRot = <<-25.7315, 0.0000, -50.9279>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 8 // Loading Bay Entrance
						tempCCTVDetails.vPos = <<-1625.3652, -2987.3716, -72.9585>>
						tempCCTVDetails.vRot = <<-16.8793, 0.0000, 109.5323>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 9 // Loading Bay
						tempCCTVDetails.vPos = <<-1634.1117, -3017.8928, -72.9314>>
						tempCCTVDetails.vRot = <<-20.5978, -0.0000, -51.5432>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
				ENDSWITCH
			ELSE
				SWITCH iChannel
					CASE 0 // Exterior
						SWITCH propertyID
							CASE SIMPLE_INTERIOR_HUB_LA_MESA
								tempCCTVDetails.vPos = <<733.8809, -1280.5146, 32.0762>>
								tempCCTVDetails.vRot = <<-23.0718, 0.0000, 149.2030>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
								tempCCTVDetails.vPos = <<338.7397, -994.5673, 34.6883>>
								tempCCTVDetails.vRot = <<-12.1295, -0.0000, 142.2404>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
								tempCCTVDetails.vPos = <<-168.6846, -1293.7164, 37.9058>>
								tempCCTVDetails.vRot = <<-19.6339, -0.0000, -116.5054>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
								tempCCTVDetails.vPos = <<-33.0446, 221.4452, 112.4069>>
								tempCCTVDetails.vRot = <<-15.0838, -0.0000, -131.4109>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
								tempCCTVDetails.vPos = <<917.0828, -2097.6853, 37.4752>>
								tempCCTVDetails.vRot = <<-14.7894, -0.0000, 50.8232>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
								tempCCTVDetails.vPos = <<-660.9339, -2374.1973, 20.6886>>
								tempCCTVDetails.vRot = <<-8.2275, -0.0000, 116.2672>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
								tempCCTVDetails.vPos = <<239.3804, -3136.7573, 14.0605>>
								tempCCTVDetails.vRot = <<-17.0212, -0.0000, 54.6928>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
								tempCCTVDetails.vPos = <<385.2475, 247.8413, 111.4846>>
								tempCCTVDetails.vRot = <<-12.8256, -0.0000, -172.5079>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
								tempCCTVDetails.vPos = <<-1230.5659, -702.7560, 29.9881>>
								tempCCTVDetails.vRot = <<-12.2916, -0.0000, 17.3399>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
							
							CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
								tempCCTVDetails.vPos = <<-1179.4352, -1175.1666, 12.3252>>
								tempCCTVDetails.vRot = <<-18.0320, -0.0000, -125.2228>>
								tempCCTVDetails.fFOV = 50.0
							BREAK
						ENDSWITCH
					BREAK
					
					CASE 1 // Storage Elevator
						tempCCTVDetails.vPos = <<-1504.0208, -3032.5730, -76.7028>>
						tempCCTVDetails.vRot = <<-32.6628, -0.0000, 73.1932>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 2 // Workstations
						tempCCTVDetails.vPos = <<-1519.9474, -3044.8657, -76.3923>>
						tempCCTVDetails.vRot = <<-32.0560, -0.0000, -32.7239>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 3 // Storage Room
						tempCCTVDetails.vPos = <<-1521.8236, -3031.2068, -76.4678>>
						tempCCTVDetails.vRot = <<-23.4451, -0.0000, -89.7034>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 4 // Garage Elevator
						tempCCTVDetails.vPos = <<-1515.1058, -3009.4922, -76.8222>>
						tempCCTVDetails.vRot = <<-16.1399, -0.0000, -120.1883>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 5 // Garage Workshop
						tempCCTVDetails.vPos = <<-1493.0902, -3003.5164, -77.6506>>
						tempCCTVDetails.vRot = <<-17.9957, -0.0000, 48.5046>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
					
					CASE 6 // Garage Entrance
						tempCCTVDetails.vPos = <<-1493.8805, -2976.1790, -77.7439>>
						tempCCTVDetails.vRot = <<-14.7672, -0.0000, 111.6384>>
						tempCCTVDetails.fFOV = 50.0
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE SIMPLE_INTERIOR_INVALID
			IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
				IF g_FMMC_STRUCT.sArenaInfo.iArena_Theme = ARENA_THEME_CONSUMERWASTELAND
					SWITCH iChannel
						CASE 0
							tempCCTVDetails.vPos = <<2883.3850, -3892.5667, 158.1912>>
							tempCCTVDetails.vRot = <<-21.0830, -0.0000, 4.4271>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 1
							tempCCTVDetails.vPos = <<2982.5925, -3800.0310, 176.0083>>
							tempCCTVDetails.vRot = <<-19.6673, -0.0000, 89.4214>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 2
							tempCCTVDetails.vPos = <<2883.3896, -3707.4775, 157.7621>>
							tempCCTVDetails.vRot = <<-20.2027, -0.0000, 167.3753>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 3
							tempCCTVDetails.vPos = <<2716.6880, -3707.3738, 157.5248>>
							tempCCTVDetails.vRot = <<-22.0318, -0.0000, -176.2512>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 4
							tempCCTVDetails.vPos = <<2617.3203, -3799.9268, 175.8861>>
							tempCCTVDetails.vRot = <<-24.1640, 0.0000, -90.4211>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 5
							tempCCTVDetails.vPos = <<2716.3696, -3892.4351, 157.3737>>
							tempCCTVDetails.vRot = <<-10.9516, -0.0000, -9.3239>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 6
							tempCCTVDetails.vPos = <<2785.8337, -3816.1772, 192.3848>>
							tempCCTVDetails.vRot = <<-32.6698, -0.0000, 119.6144>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 7
							tempCCTVDetails.vPos = <<2785.6895, -3787.6526, 192.3848>>
							tempCCTVDetails.vRot = <<-32.8077, -0.0000, 58.2836>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 8
							tempCCTVDetails.vPos = <<2814.0256, -3787.6536, 192.3848>>
							tempCCTVDetails.vRot = <<-35.2116, -0.0000, -60.6846>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 9
							tempCCTVDetails.vPos = <<2814.0256, -3815.3733, 192.3848>>
							tempCCTVDetails.vRot = <<-34.3910, 0.0000, -135.1473>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 10
							tempCCTVDetails.vPos = <<2741.4084, -3900.4243, 143.8768>>
							tempCCTVDetails.vRot = <<-10.7689, -0.0000, -50.9915>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 11
							tempCCTVDetails.vPos = <<2858.2661, -3699.9709, 143.8768>>
							tempCCTVDetails.vRot = <<-9.3653, 0.0000, 128.6076>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 12
							tempCCTVDetails.vPos = <<2869.7217, -3916.8552, 174.3559>>
							tempCCTVDetails.vRot = <<-31.6777, 0.0000, 0.0575>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 13
							tempCCTVDetails.vPos = <<2729.9746, -3917.0962, 174.3559>>
							tempCCTVDetails.vRot = <<-31.6777, 0.0000, 0.0575>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 14
							tempCCTVDetails.vPos = <<2799.9375, -3911.8022, 207.8903>>
							tempCCTVDetails.vRot = <<-35.5304, -0.0000, -1.1016>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
					ENDSWITCH
				ELSE
					SWITCH iChannel
						CASE 0
							tempCCTVDetails.vPos = <<2883.3826, -3892.5408, 155.2299>>
							tempCCTVDetails.vRot = <<-16.9571, -0.0000, 4.9242>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 1
							tempCCTVDetails.vPos = <<2982.4553, -3800.0310, 160.3440>>
							tempCCTVDetails.vRot = <<-18.4801, 0.0000, 89.4214>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 2
							tempCCTVDetails.vPos = <<2883.3850, -3707.4990, 155.1997>>
							tempCCTVDetails.vRot = <<-13.1762, -0.0000, 167.3753>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 3
							tempCCTVDetails.vPos = <<2716.6897, -3707.3943, 155.1997>>
							tempCCTVDetails.vRot = <<-13.3510, 0.0000, -176.2512>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 4
							tempCCTVDetails.vPos = <<2617.4724, -3799.9268, 158.4785>>
							tempCCTVDetails.vRot = <<-15.4044, -0.0000, -90.4211>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 5
							tempCCTVDetails.vPos = <<2716.3728, -3892.4153, 155.0615>>
							tempCCTVDetails.vRot = <<-19.3231, -0.0000, -9.3239>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 6
							tempCCTVDetails.vPos = <<2785.8337, -3816.1772, 192.3848>>
							tempCCTVDetails.vRot = <<-32.6698, -0.0000, 119.6144>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 7
							tempCCTVDetails.vPos = <<2785.6895, -3787.6526, 192.3848>>
							tempCCTVDetails.vRot = <<-32.8077, -0.0000, 58.2836>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 8
							tempCCTVDetails.vPos = <<2814.0256, -3787.6536, 192.3848>>
							tempCCTVDetails.vRot = <<-35.2116, -0.0000, -60.6846>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 9
							tempCCTVDetails.vPos = <<2814.0256, -3815.3733, 192.3848>>
							tempCCTVDetails.vRot = <<-34.3910, 0.0000, -135.1473>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 10
							tempCCTVDetails.vPos = <<2741.4084, -3900.4243, 143.8768>>
							tempCCTVDetails.vRot = <<-10.7689, -0.0000, -50.9915>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 11
							tempCCTVDetails.vPos = <<2858.2661, -3699.9709, 143.8768>>
							tempCCTVDetails.vRot = <<-9.3653, 0.0000, 128.6076>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 12
							tempCCTVDetails.vPos = <<2869.7217, -3916.8552, 174.3559>>
							tempCCTVDetails.vRot = <<-31.6777, 0.0000, 0.0575>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 13
							tempCCTVDetails.vPos = <<2729.9746, -3917.0962, 174.3559>>
							tempCCTVDetails.vRot = <<-31.6777, 0.0000, 0.0575>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
						
						CASE 14
							tempCCTVDetails.vPos = <<2799.9375, -3911.8022, 207.8903>>
							tempCCTVDetails.vRot = <<-35.5304, -0.0000, -1.1016>>
							tempCCTVDetails.fFOV = 50.0
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		BREAK
		
		CASE SIMPLE_INTERIOR_ARENA_GARAGE_1
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<-404.7897, -1877.8501, 24.5915>>
					tempCCTVDetails.vRot = <<-20.4745, -0.0000, -97.3189>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<-360.1998, -1859.7180, 24.5799>>
					tempCCTVDetails.vRot = <<-14.8981, -0.0000, 100.3293>>
					tempCCTVDetails.fFOV = 50.0
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_SUBMARINE
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<1558.5658, 380.5564, -48.3036>>
					tempCCTVDetails.vRot = <<-20.6230, -0.0000, -32.1963>>
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<1563.9287, 419.4655, -47.9852>>
					tempCCTVDetails.vRot = <<-18.2436, -0.0000, 146.8553>>
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<1564.2688, 392.2163, -51.7485>>
					tempCCTVDetails.vRot = <<-28.2570, -0.0000, 144.0053>>
				BREAK
				
				CASE 3
					tempCCTVDetails.vPos = <<1556.9882, 385.1603, -51.6520>>
					tempCCTVDetails.vRot = <<-26.3123, -0.0000, -141.0038>>
				BREAK
				
				CASE 4
					tempCCTVDetails.vPos = <<1565.0630, 443.9686, -51.3995>>
					tempCCTVDetails.vRot = <<-19.2951, 0.0000, 154.0811>>
				BREAK
				
				CASE 5
					tempCCTVDetails.vPos = <<1557.5686, 403.8779, -51.6788>>
					tempCCTVDetails.vRot = <<-16.8128, -0.0000, -33.9478>>
				BREAK
				
				CASE 6
					tempCCTVDetails.vPos = <<1561.4457, 364.5545, -54.7375>>
					tempCCTVDetails.vRot = <<-17.9534, -0.0000, -1.2037>>
				BREAK
			ENDSWITCH
		BREAK
			
		CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
		CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
		CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON
		CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
		CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
			SWITCH iChannel
				CASE 0	// Mezzanine
					tempCCTVDetails.vPos = <<-1357.1044, 136.7786, -93.3085>>
					tempCCTVDetails.vRot = <<-21.7370, -0.0000, -73.71313>>
					tempCCTVDetails.fMaxAngleLeft = 45.0
					tempCCTVDetails.fMaxAngleRight = 15.0
				BREAK
				
				CASE 1	// Garage
					tempCCTVDetails.vPos = <<-1345.3680, 136.7298, -95.7131>>
					tempCCTVDetails.vRot = <<-19.7992, -0.0000, -71.2920>>
					tempCCTVDetails.fMaxAngleLeft = 35.0
					tempCCTVDetails.fMaxAngleRight = 15.0
				BREAK
				
				CASE 2	// Mod Floor
					tempCCTVDetails.vPos = <<-1355.3149, 168.4263, -94.6094>>
					tempCCTVDetails.vRot = <<-25.1322, 0.0000, 172.7889>>
					tempCCTVDetails.fMaxAngleLeft = 55.0
					tempCCTVDetails.fMaxAngleRight = 35.0
				BREAK
				
				CASE 3	// Workshop
					tempCCTVDetails.vPos = <<-1361.3556, 165.6528, -95.8796>>
					tempCCTVDetails.vRot = <<-30.9150, -0.0000, 115.8260>>
					tempCCTVDetails.fMaxAngleLeft = 40.0
					tempCCTVDetails.fMaxAngleRight = 20.0
				BREAK
				
				CASE 4	// Entrance
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
							tempCCTVDetails.vPos = <<758.9403, -675.1568, 31.3249>>
							tempCCTVDetails.vRot = <<-27.0887, -0.0000, -152.6683>>
							tempCCTVDetails.fMaxAngleLeft = 45.0
							tempCCTVDetails.fMaxAngleRight = 29.0
						BREAK
						CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
							tempCCTVDetails.vPos = <<-150.0963, -1341.0437, 32.9251>>
							tempCCTVDetails.vRot = <<-26.6640, -0.0000, -120.4227>>
							tempCCTVDetails.fMaxAngleLeft = 10.0
							tempCCTVDetails.fMaxAngleRight = 45.0
						BREAK
						CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON
							tempCCTVDetails.vPos = <<-165.0954, -26.4049, 54.4457>>
							tempCCTVDetails.vRot = <<-16.5064, 0.0000, 99.5003>>
							tempCCTVDetails.fMaxAngleLeft = 50.0
							tempCCTVDetails.fMaxAngleRight = 25.0
						BREAK
						CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
							tempCCTVDetails.vPos = <<235.6586, -1870.5111, 28.7865>>
							tempCCTVDetails.vRot = <<-28.6045, 0.0000, 162.9770>>
							tempCCTVDetails.fMaxAngleLeft = 45.0
							tempCCTVDetails.fMaxAngleRight = 25.0
						BREAK
						CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
							tempCCTVDetails.vPos = <<488.7841, -892.4971, 28.0695>>
							tempCCTVDetails.vRot = <<-21.5103, -0.0000, -158.6043>>
							tempCCTVDetails.fMaxAngleLeft = 45.0
							tempCCTVDetails.fMaxAngleRight = 20.0
						BREAK
					ENDSWITCH
					
					
				BREAK				
			ENDSWITCH
		BREAK
		
		#IF FEATURE_MUSIC_STUDIO
		CASE SIMPLE_INTERIOR_MUSIC_STUDIO
			SWITCH iChannel
				CASE 0	// Control Room
					tempCCTVDetails.vPos = <<-1000.5214, -58.6640, -96.9038>>
					tempCCTVDetails.vRot = <<-20.6881, -0.0000, -153.8054>>
					tempCCTVDetails.fMaxAngleLeft = 20.0
					tempCCTVDetails.fMaxAngleRight = 60.0
				BREAK
				
				CASE 1	// Isolation Booth
					tempCCTVDetails.vPos = <<-1001.3336, -72.6089, -96.6629>>
					tempCCTVDetails.vRot = <<-29.1617, -0.0000, -119.6589>>
					tempCCTVDetails.fMaxAngleLeft = 20.0
					tempCCTVDetails.fMaxAngleRight = 40.0
				BREAK
				
				CASE 2	// Tracking Room
					tempCCTVDetails.vPos = <<-990.5572, -67.7290, -96.8221>>
					tempCCTVDetails.vRot = <<-19.5735, -0.0000, -103.5133>>
					tempCCTVDetails.fMaxAngleLeft = 75.0
					tempCCTVDetails.fMaxAngleRight = 80.0
				BREAK
				
				CASE 3	// Front Desk
					tempCCTVDetails.vPos = <<-1023.3707, -92.3678, -97.3480>>
					tempCCTVDetails.vRot = <<-24.9340, -0.0000, -15.1209>>
					tempCCTVDetails.fMaxAngleLeft = 10.0
					tempCCTVDetails.fMaxAngleRight = 50.0
				BREAK
				
				CASE 4	// Exterior - Entrance
					tempCCTVDetails.vPos = <<-837.8903, -234.8408, 40.4029>>
					tempCCTVDetails.vRot = <<-26.0319, -0.0000, 32.1612>>
					tempCCTVDetails.fMaxAngleLeft = 20.0
					tempCCTVDetails.fMaxAngleRight = 60.0							
				BREAK	
				
				CASE 5	// Exterior - Rear
					tempCCTVDetails.vPos = <<-854.1523, -250.4407, 41.6111>>
					tempCCTVDetails.vRot = <<-19.2571, -0.0000, -138.0315>>
					tempCCTVDetails.fMaxAngleLeft = 25.0
					tempCCTVDetails.fMaxAngleRight = 55.0							
				BREAK
				
				CASE 6	// Exterior - Roof
					tempCCTVDetails.vPos = <<-857.1470, -230.6671, 63.1336>>
					tempCCTVDetails.vRot = <<-22.9251, 0.0000, 0.0>>
					tempCCTVDetails.fMaxAngleLeft = 88.0
					tempCCTVDetails.fMaxAngleRight = 62.0						
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		
		#IF FEATURE_FIXER
		CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
		CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
		CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
		CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
			SWITCH iChannel
				CASE 0	// Entrance										
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
							tempCCTVDetails.vPos = <<387.7643, -65.4237, 105.5542>>
							tempCCTVDetails.vRot = <<-24.8520, 0.0000, 136.6439>>	
							tempCCTVDetails.fMaxAngleLeft = 50.0
							tempCCTVDetails.fMaxAngleRight = 60.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
							tempCCTVDetails.vPos = <<-1014.6932, -428.4726, 65.8640>>
							tempCCTVDetails.vRot = <<-20.8271, -0.0000, -173.7528>>
							tempCCTVDetails.fMaxAngleLeft = 40.0
							tempCCTVDetails.fMaxAngleRight = 60.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
							tempCCTVDetails.vPos = <<-593.5703, -713.2729, 115.0083>>
							tempCCTVDetails.vRot = <<-22.0819, -0.0000, -11.1561>>
							tempCCTVDetails.fMaxAngleLeft = 40.0
							tempCCTVDetails.fMaxAngleRight = 70.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
							tempCCTVDetails.vPos = <<-1000.5919, -755.5334, 64.0613>>
							tempCCTVDetails.vRot = <<-25.8898, 0.0000, -101.3953>>
							tempCCTVDetails.fMaxAngleLeft = 40.0
							tempCCTVDetails.fMaxAngleRight = 70.0
						BREAK
					ENDSWITCH
				BREAK
			
				CASE 1	// Office Level										
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
							tempCCTVDetails.vPos = <<393.1200, -73.5801, 109.6234>>
							tempCCTVDetails.vRot = <<-21.6381, 0.0000, 43.5879>>										
							tempCCTVDetails.fMaxAngleLeft = 50.0
							tempCCTVDetails.fMaxAngleRight = 50.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
							tempCCTVDetails.vPos = <<-1004.7451, -430.5146, 70.1265>>
							tempCCTVDetails.vRot = <<-18.7761, -0.0000, 81.5176>>
							tempCCTVDetails.fMaxAngleLeft = 50.0
							tempCCTVDetails.fMaxAngleRight = 50.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
							tempCCTVDetails.vPos = <<-601.6116, -707.0442, 119.1735>>
							tempCCTVDetails.vRot = <<-20.0217, -0.0000, -127.3222>>
							tempCCTVDetails.fMaxAngleLeft = 50.0
							tempCCTVDetails.fMaxAngleRight = 40.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
							tempCCTVDetails.vPos = <<-994.1427, -747.3150, 68.0368>>
							tempCCTVDetails.vRot = <<-21.8328, -0.0000, 145.3017>>
							tempCCTVDetails.fMaxAngleLeft = 50.0
							tempCCTVDetails.fMaxAngleRight = 40.0
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 2	// Personal Quarters (if owned)										
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
							tempCCTVDetails.vPos = <<374.8470, -60.6373, 114.2766>>
							tempCCTVDetails.vRot = <<-23.1520, -0.0000, -150.0937>>															
							tempCCTVDetails.fMaxAngleLeft = 30.0
							tempCCTVDetails.fMaxAngleRight = 40.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
							tempCCTVDetails.vPos = <<-1027.2012, -434.5600, 74.5607>>
							tempCCTVDetails.vRot = <<-22.9777, -0.0000, -101.1259>>
							tempCCTVDetails.fMaxAngleLeft = 30.0
							tempCCTVDetails.fMaxAngleRight = 50.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
							tempCCTVDetails.vPos = <<-580.1683, -712.8673, 123.6430>>
							tempCCTVDetails.vRot = <<-26.8597, -0.0000, 43.6406>>
							tempCCTVDetails.fMaxAngleLeft = 40.0
							tempCCTVDetails.fMaxAngleRight = 50.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
							tempCCTVDetails.vPos = <<-1000.0593, -768.9911, 72.4062>>
							tempCCTVDetails.vRot = <<-26.7584, 0.0000, -37.0187>>
							tempCCTVDetails.fMaxAngleLeft = 40.0
							tempCCTVDetails.fMaxAngleRight = 50.0
						BREAK
					ENDSWITCH
				BREAK
												
				CASE 3	// Operations Level				
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
							tempCCTVDetails.vPos = <<390.0664, -61.9516, 113.7487>>
							tempCCTVDetails.vRot = <<-18.3203, 0.0000, 18.6734>>
							tempCCTVDetails.fMaxAngleLeft = 40.0
							tempCCTVDetails.fMaxAngleRight = 60.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
							tempCCTVDetails.vPos = <<-1016.1967, -424.1888, 74.1926>>
							tempCCTVDetails.vRot = <<-18.7918, -0.0000, 74.8795>>
							tempCCTVDetails.fMaxAngleLeft = 40.0
							tempCCTVDetails.fMaxAngleRight = 60.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
							tempCCTVDetails.vPos = <<-594.6627, -717.2415, 123.3245>>
							tempCCTVDetails.vRot = <<-19.6735, -0.0000, -140.0594>>
							tempCCTVDetails.fMaxAngleLeft = 40.0
							tempCCTVDetails.fMaxAngleRight = 60.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
							tempCCTVDetails.vPos = <<-1004.4207, -754.4242, 72.3037>>
							tempCCTVDetails.vRot = <<-17.3176, -0.0000, 121.6154>>
							tempCCTVDetails.fMaxAngleLeft = 40.0
							tempCCTVDetails.fMaxAngleRight = 60.0
						BREAK
					ENDSWITCH									
				BREAK
																				
				CASE 4	// Exterior - Entrance											
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
							tempCCTVDetails.vPos = <<384.1058, -72.7906, 69.9670>>
							tempCCTVDetails.vRot = <<-21.0514, -0.0000, -150.8777>>				
							tempCCTVDetails.fMaxAngleLeft = 20.0
							tempCCTVDetails.fMaxAngleRight = 60.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD						
							tempCCTVDetails.vPos = <<-1013.6431, -411.9763, 41.7839>>
							tempCCTVDetails.vRot =  <<-28.8601, -0.0000, 113.0899>>
							tempCCTVDetails.fMaxAngleLeft = 25.0
							tempCCTVDetails.fMaxAngleRight = 160.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL						
							tempCCTVDetails.vPos = <<-586.2364, -706.7939, 38.8996>>
							tempCCTVDetails.vRot = <<-35.2676, -0.0000, 105.5353>>
							tempCCTVDetails.fMaxAngleLeft = 20.0
							tempCCTVDetails.fMaxAngleRight = 90.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI						
							tempCCTVDetails.vPos = <<-1039.0875, -752.3752, 21.9285>>
							tempCCTVDetails.vRot = <<-25.4793, -0.0000, -173.3071>>
							tempCCTVDetails.fMaxAngleLeft = 20.0
							tempCCTVDetails.fMaxAngleRight = 140.0
						BREAK
					ENDSWITCH
				BREAK	
				
				CASE 5	// Exterior - Garage
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK						
							tempCCTVDetails.vPos = <<356.6470, -81.6619, 69.8369>>
							tempCCTVDetails.vRot = <<-25.7706, -0.0000, -27.1376>>
							tempCCTVDetails.fMaxAngleLeft = 20.0
							tempCCTVDetails.fMaxAngleRight = 140.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD						
							tempCCTVDetails.vPos = <<-1029.0420, -410.1802, 34.8137>>
							tempCCTVDetails.vRot = <<-23.6780, -0.0000, 104.3431>>
							tempCCTVDetails.fMaxAngleLeft = 20.0
							tempCCTVDetails.fMaxAngleRight = 60.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL					
							tempCCTVDetails.vPos = <<-619.8470, -730.4952, 29.5439>>
							tempCCTVDetails.vRot = <<-17.5503, -0.0000, -161.1668>>
							tempCCTVDetails.fMaxAngleLeft = 20.0
							tempCCTVDetails.fMaxAngleRight = 90.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI						
							tempCCTVDetails.vPos = <<-991.7785, -769.0563, 19.0050>>
							tempCCTVDetails.vRot = <<-24.1672, -0.0000, -40.6725>>
							tempCCTVDetails.fMaxAngleLeft = 3.0
							tempCCTVDetails.fMaxAngleRight = 100.0
						BREAK
					ENDSWITCH
				BREAK
				
				CASE 6	// Exterior - Roof
					SWITCH propertyID
						CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK														
							tempCCTVDetails.vPos = <<382.0028, -52.5512, 124.3039>>
							tempCCTVDetails.vRot = <<-39.9730, -0.0000, -74.5614>>												
							tempCCTVDetails.fMaxAngleLeft = 20.0
							tempCCTVDetails.fMaxAngleRight = 60.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD							
							tempCCTVDetails.vPos = <<-1024.1444, -431.4286, 78.6751>>
							tempCCTVDetails.vRot = <<-29.8365, 0.0000, 178.6140>>
							tempCCTVDetails.fMaxAngleLeft = 20.0
							tempCCTVDetails.fMaxAngleRight = 60.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
							tempCCTVDetails.vPos = <<-579.4983, -712.7164, 131.5239>>
							tempCCTVDetails.vRot = <<-20.5221, -0.0000, 162.2622>>
							tempCCTVDetails.fMaxAngleLeft = 20.0
							tempCCTVDetails.fMaxAngleRight = 60.0
						BREAK
						CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI																					
							tempCCTVDetails.vPos = <<-1021.3446, -764.4041, 78.0854>>
							tempCCTVDetails.vRot = <<-20.5305, -0.0000, -121.8755>>
							tempCCTVDetails.fMaxAngleLeft = 20.0
							tempCCTVDetails.fMaxAngleRight = 100.0
						BREAK
					ENDSWITCH
				BREAK																
			ENDSWITCH
		BREAK
		#ENDIF
		
		#IF FEATURE_DLC_2_2022
		CASE SIMPLE_INTERIOR_JUGGALO_HIDEOUT
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<593.9539, -407.6195, 28.6475>>
					tempCCTVDetails.vRot = <<-22.6819, -0.0000, 0.0>> 
					tempCCTVDetails.fMaxAngleLeft = 43.9860
					tempCCTVDetails.fMaxAngleRight = 65.7696
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<596.4029, -431.1552, 27.6108>> 
					tempCCTVDetails.vRot = <<-17.9543, -0.0000, -96.0519>>
					tempCCTVDetails.fMaxAngleLeft = 68.4197
					tempCCTVDetails.fMaxAngleRight = 27.6322
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_MULTISTOREY_GARAGE
			SWITCH iChannel
				CASE 0
					tempCCTVDetails.vPos = <<-283.3875, 281.7849, 92.2637>>
					tempCCTVDetails.vRot = <<-24.7885, 0.0000, 168.749>>
					tempCCTVDetails.fMaxAngleLeft = 66.7640
					tempCCTVDetails.fMaxAngleRight = 55.513
				BREAK
				
				CASE 1
					tempCCTVDetails.vPos = <<-267.0724, 281.4863, 93.4096>>
					tempCCTVDetails.vRot = <<-25.4451, -0.0000, -1.7852+180.0>>
					tempCCTVDetails.fMaxAngleLeft = -1.7852 + 180.0 - 116.0922
					tempCCTVDetails.fMaxAngleRight = -1.7852 + 180.0 - 117.8774
				BREAK
				
				CASE 2
					tempCCTVDetails.vPos = <<-269.5697, 289.5145, 106.9647>>
					tempCCTVDetails.vRot = <<-25.8174, -0.0000, -39.1523+180.0>> 
					tempCCTVDetails.fMaxAngleLeft = 180.0 - 110.4399
					tempCCTVDetails.fMaxAngleRight = 180.0 - 149.5922
				BREAK
				
				CASE 3
					tempCCTVDetails.vPos = <<-298.4210, 303.5463, 92.4243>>
					tempCCTVDetails.vRot = <<-16.1354, -0.0000, -54.946>>
					tempCCTVDetails.fMaxAngleLeft = -54.946 + 68.5138 + 27.8093
					tempCCTVDetails.fMaxAngleRight = 54.946 - 13.5678 - 27.8093
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
	ENDSWITCH							
	
	#IF IS_DEBUG_BUILD
	tempCCTVDetails.vPos += MPCCTVWidgetData.camOffsetFromDefault.vPos
	tempCCTVDetails.vRot += MPCCTVWidgetData.camOffsetFromDefault.vRot
	tempCCTVDetails.fFOV += MPCCTVWidgetData.camOffsetFromDefault.fFOV
	#ENDIF
	
	RETURN tempCCTVDetails
ENDFUNC

FUNC STRING GET_CHANNEL_DESC_STRING(MP_CCTV_CHANNEL eChannel, SIMPLE_INTERIORS ePropertyID)
	STRING sChannelName = "MPTV_CCTV1"
	
	SWITCH ePropertyID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "MPTV_CCTV1"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "MPTV_CCTV1"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "MPTV_CCTV1"	BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_SUBMARINE
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "SUB_CCTV_1D"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "SUB_CCTV_2D"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "SUB_CCTV_3D"	BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "SUB_CCTV_4D"	BREAK
				CASE MP_CCTV_CHANNEL_4	sChannelName = "SUB_CCTV_5D"	BREAK
				CASE MP_CCTV_CHANNEL_5	sChannelName = "SUB_CCTV_6D"	BREAK
				CASE MP_CCTV_CHANNEL_6	sChannelName = "SUB_CCTV_7D"	BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
		CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
		CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON
		CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
		CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "AS_CCTV_D"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "AS_CCTV_D"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "AS_CCTV_D"	BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "AS_CCTV_D2"	BREAK
				CASE MP_CCTV_CHANNEL_4	sChannelName = "AS_CCTV_D2"	BREAK
			ENDSWITCH
		BREAK
		
		#IF FEATURE_FIXER
		CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
		CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
		CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
		CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
		#ENDIF
		#IF FEATURE_MUSIC_STUDIO
		CASE SIMPLE_INTERIOR_MUSIC_STUDIO
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "MS_CCTV_D"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "MS_CCTV_D2"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "MS_CCTV_D3"	BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "MS_CCTV_D4"	BREAK
				CASE MP_CCTV_CHANNEL_4	sChannelName = "MS_CCTV_D5"	BREAK
				CASE MP_CCTV_CHANNEL_5	sChannelName = "MS_CCTV_D6"	BREAK
				CASE MP_CCTV_CHANNEL_6	sChannelName = "MS_CCTV_D7"	BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		
		#IF FEATURE_DLC_2_2022
		CASE SIMPLE_INTERIOR_JUGGALO_HIDEOUT
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "JH_CCTV_D"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "JH_CCTV_D2"	BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_MULTISTOREY_GARAGE
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "MSG_CCTV_D"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "MSG_CCTV_D2"BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "MSG_CCTV_D3"BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "MSG_CCTV_D4"BREAK
			ENDSWITCH
		BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN sChannelName
ENDFUNC

FUNC STRING GET_CHANNEL_STRING(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, MP_CCTV_CHANNEL eChannel, SIMPLE_INTERIORS ePropertyID)
	STRING sChannelName = ""
	
	SWITCH ePropertyID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "MPCCTV_CAM_1"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "MPCCTV_CAM_2"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "MPCCTV_CAM_3"	BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_BUNKER_1
		CASE SIMPLE_INTERIOR_BUNKER_2
		CASE SIMPLE_INTERIOR_BUNKER_3
		CASE SIMPLE_INTERIOR_BUNKER_4
		CASE SIMPLE_INTERIOR_BUNKER_5
		CASE SIMPLE_INTERIOR_BUNKER_6
		CASE SIMPLE_INTERIOR_BUNKER_7
		CASE SIMPLE_INTERIOR_BUNKER_9
		CASE SIMPLE_INTERIOR_BUNKER_10
		CASE SIMPLE_INTERIOR_BUNKER_11
		CASE SIMPLE_INTERIOR_BUNKER_12
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "BUNK_CCTV_1"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "BUNK_CCTV_2"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "BUNK_CCTV_3"	BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "BUNK_CCTV_4"	BREAK
				CASE MP_CCTV_CHANNEL_4	sChannelName = "BUNK_CCTV_5"	BREAK
				CASE MP_CCTV_CHANNEL_5	sChannelName = "BUNK_CCTV_6"	BREAK
				CASE MP_CCTV_CHANNEL_6	sChannelName = "BUNK_CCTV_7"	BREAK
				CASE MP_CCTV_CHANNEL_7	sChannelName = "BUNK_CCTV_8"	BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HANGAR_1
		CASE SIMPLE_INTERIOR_HANGAR_2
		CASE SIMPLE_INTERIOR_HANGAR_3
		CASE SIMPLE_INTERIOR_HANGAR_4
		CASE SIMPLE_INTERIOR_HANGAR_5
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "HANGAR_CCTV_1"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "HANGAR_CCTV_2"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "HANGAR_CCTV_3"	BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "HANGAR_CCTV_4"	BREAK
				CASE MP_CCTV_CHANNEL_4	sChannelName = "HANGAR_CCTV_5"	BREAK
				CASE MP_CCTV_CHANNEL_5	sChannelName = "HANGAR_CCTV_6"	BREAK
				CASE MP_CCTV_CHANNEL_6	sChannelName = "HANGAR_CCTV_7"	BREAK
				CASE MP_CCTV_CHANNEL_7	sChannelName = "HANGAR_CCTV_8"	BREAK
				CASE MP_CCTV_CHANNEL_8	sChannelName = "HANGAR_CCTV_9"	BREAK
				CASE MP_CCTV_CHANNEL_9	sChannelName = "HANGAR_CCTV_10"	BREAK
				CASE MP_CCTV_CHANNEL_10	sChannelName = "HANGAR_CCTV_11"	BREAK
				CASE MP_CCTV_CHANNEL_11	sChannelName = "HANGAR_CCTV_12"	BREAK
				CASE MP_CCTV_CHANNEL_12	sChannelName = "HANGAR_CCTV_13"	BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "DEFUNCT_CCTV_1"		BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "DEFUNCT_CCTV_2"		BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "DEFUNCT_CCTV_3"		BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "DEFUNCT_CCTV_4"		BREAK
				CASE MP_CCTV_CHANNEL_4	sChannelName = "DEFUNCT_CCTV_5"		BREAK
				CASE MP_CCTV_CHANNEL_5	sChannelName = "DEFUNCT_CCTV_6"		BREAK
				CASE MP_CCTV_CHANNEL_6	sChannelName = "DEFUNCT_CCTV_7"		BREAK
				CASE MP_CCTV_CHANNEL_7	sChannelName = "DEFUNCT_CCTV_8"		BREAK
				CASE MP_CCTV_CHANNEL_8	sChannelName = "DEFUNCT_CCTV_9"		BREAK
				CASE MP_CCTV_CHANNEL_9	sChannelName = "DEFUNCT_CCTV_10"	BREAK
				CASE MP_CCTV_CHANNEL_10	sChannelName = "DEFUNCT_CCTV_11"	BREAK
				CASE MP_CCTV_CHANNEL_11	sChannelName = "DEFUNCT_CCTV_12"	BREAK
				CASE MP_CCTV_CHANNEL_12 sChannelName = "DEFUNCT_CCTV_13"	BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1
			sChannelName = "AOC_CCTV" BREAK
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
				SWITCH eChannel
					CASE MP_CCTV_CHANNEL_0	sChannelName = "CLUB_CCTV_6"	BREAK
					CASE MP_CCTV_CHANNEL_1	sChannelName = "CLUB_CCTV_6"	BREAK
					CASE MP_CCTV_CHANNEL_2	sChannelName = "CLUB_CCTV_6"	BREAK
					CASE MP_CCTV_CHANNEL_3	sChannelName = "CLUB_CCTV_6"	BREAK
				ENDSWITCH
			ELIF GET_INTERIOR_FLOOR_INDEX() = 0
				SWITCH eChannel
					CASE MP_CCTV_CHANNEL_0	sChannelName = "CLUB_CCTV_1"	BREAK
					CASE MP_CCTV_CHANNEL_1	sChannelName = "CLUB_CCTV_2"	BREAK
					CASE MP_CCTV_CHANNEL_2	sChannelName = "CLUB_CCTV_3"	BREAK
					CASE MP_CCTV_CHANNEL_3	sChannelName = "CLUB_CCTV_4"	BREAK
					CASE MP_CCTV_CHANNEL_4	sChannelName = "CLUB_CCTV_5"	BREAK
					CASE MP_CCTV_CHANNEL_5	sChannelName = "CLUB_CCTV_6"	BREAK
					CASE MP_CCTV_CHANNEL_6	sChannelName = "CLUB_CCTV_7"	BREAK
					CASE MP_CCTV_CHANNEL_7	sChannelName = "CLUB_CCTV_8"	BREAK
					CASE MP_CCTV_CHANNEL_8	sChannelName = "CLUB_CCTV_9"	BREAK
					CASE MP_CCTV_CHANNEL_9	sChannelName = "CLUB_CCTV_9"	BREAK
				ENDSWITCH
			ELSE
				SWITCH eChannel
					CASE MP_CCTV_CHANNEL_0	sChannelName = "HUB_CCTV_1"		BREAK
					CASE MP_CCTV_CHANNEL_1	sChannelName = "HUB_CCTV_2"		BREAK
					CASE MP_CCTV_CHANNEL_2	sChannelName = "HUB_CCTV_3"		BREAK
					CASE MP_CCTV_CHANNEL_3	sChannelName = "HUB_CCTV_4"		BREAK
					CASE MP_CCTV_CHANNEL_4	sChannelName = "HUB_CCTV_5"		BREAK
					CASE MP_CCTV_CHANNEL_6	sChannelName = "HUB_CCTV_7"		BREAK
					
					CASE MP_CCTV_CHANNEL_5
						IF GET_INTERIOR_FLOOR_INDEX() = 1
							sChannelName = "HUB_CCTV_6"
						ELSE
							sChannelName = "HUB_CCTV_6B"
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE SIMPLE_INTERIOR_ARENA_GARAGE_1
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "MPCCTV_CAM_1"		BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "MPCCTV_CAM_2"		BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_SUBMARINE
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "SUB_CCTV_1"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "SUB_CCTV_2"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "SUB_CCTV_3"	BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "SUB_CCTV_4"	BREAK
				CASE MP_CCTV_CHANNEL_4	sChannelName = "SUB_CCTV_5"	BREAK
				CASE MP_CCTV_CHANNEL_5	sChannelName = "SUB_CCTV_6"	BREAK
				CASE MP_CCTV_CHANNEL_6	sChannelName = "SUB_CCTV_7"	BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
		CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
		CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON
		CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
		CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "AS_CCTV_1"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "AS_CCTV_2"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "AS_CCTV_3"	BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "AS_CCTV_4"	BREAK
				CASE MP_CCTV_CHANNEL_4	sChannelName = "AS_CCTV_5"	BREAK
			ENDSWITCH
		BREAK
		
		#IF FEATURE_MUSIC_STUDIO
		CASE SIMPLE_INTERIOR_MUSIC_STUDIO
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "MS_CCTV_1"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "MS_CCTV_2"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "MS_CCTV_3"	BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "MS_CCTV_4"	BREAK
				CASE MP_CCTV_CHANNEL_4	sChannelName = "MS_CCTV_5"	BREAK
				CASE MP_CCTV_CHANNEL_5	sChannelName = "MS_CCTV_6"	BREAK
				CASE MP_CCTV_CHANNEL_6	sChannelName = "MS_CCTV_7"	BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		
		#IF FEATURE_FIXER
		CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
		CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
		CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
		CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "FHQ_CCTV_1"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "FHQ_CCTV_2"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "FHQ_CCTV_3"	BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "FHQ_CCTV_4"	BREAK
				CASE MP_CCTV_CHANNEL_4	sChannelName = "FHQ_CCTV_5"	BREAK
				CASE MP_CCTV_CHANNEL_5	sChannelName = "FHQ_CCTV_6"	BREAK
				CASE MP_CCTV_CHANNEL_6	sChannelName = "FHQ_CCTV_7"	BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		
		#IF FEATURE_DLC_2_2022
		CASE SIMPLE_INTERIOR_JUGGALO_HIDEOUT
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "JH_CCTV_1"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "JH_CCTV_2"	BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_MULTISTOREY_GARAGE
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0	sChannelName = "MSG_CCTV_1"	BREAK
				CASE MP_CCTV_CHANNEL_1	sChannelName = "MSG_CCTV_2"	BREAK
				CASE MP_CCTV_CHANNEL_2	sChannelName = "MSG_CCTV_3"	BREAK
				CASE MP_CCTV_CHANNEL_3	sChannelName = "MSG_CCTV_4"	BREAK
			ENDSWITCH
		BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN sChannelName
ENDFUNC

FUNC BOOL SHOULD_CHANNEL_CONCEAL_PLAYERS(MP_CCTV_CHANNEL eChannel, SIMPLE_INTERIORS ePropertyID)
	SWITCH ePropertyID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			RETURN TRUE
		BREAK
		
		CASE SIMPLE_INTERIOR_BUNKER_1
		CASE SIMPLE_INTERIOR_BUNKER_2
		CASE SIMPLE_INTERIOR_BUNKER_3
		CASE SIMPLE_INTERIOR_BUNKER_4
		CASE SIMPLE_INTERIOR_BUNKER_5
		CASE SIMPLE_INTERIOR_BUNKER_6
		CASE SIMPLE_INTERIOR_BUNKER_7
		CASE SIMPLE_INTERIOR_BUNKER_9
		CASE SIMPLE_INTERIOR_BUNKER_10
		CASE SIMPLE_INTERIOR_BUNKER_11
		CASE SIMPLE_INTERIOR_BUNKER_12
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0
				CASE MP_CCTV_CHANNEL_1
				CASE MP_CCTV_CHANNEL_2
				CASE MP_CCTV_CHANNEL_3
				CASE MP_CCTV_CHANNEL_4
				CASE MP_CCTV_CHANNEL_5
				CASE MP_CCTV_CHANNEL_6
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HANGAR_1
		CASE SIMPLE_INTERIOR_HANGAR_2
		CASE SIMPLE_INTERIOR_HANGAR_3
		CASE SIMPLE_INTERIOR_HANGAR_4
		CASE SIMPLE_INTERIOR_HANGAR_5
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0
				CASE MP_CCTV_CHANNEL_1
				CASE MP_CCTV_CHANNEL_2
				CASE MP_CCTV_CHANNEL_3
				CASE MP_CCTV_CHANNEL_4
				CASE MP_CCTV_CHANNEL_5
				CASE MP_CCTV_CHANNEL_6
				CASE MP_CCTV_CHANNEL_7
				CASE MP_CCTV_CHANNEL_8
				CASE MP_CCTV_CHANNEL_9
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0
				CASE MP_CCTV_CHANNEL_1
				CASE MP_CCTV_CHANNEL_2
				CASE MP_CCTV_CHANNEL_3
				CASE MP_CCTV_CHANNEL_4
				CASE MP_CCTV_CHANNEL_5
				CASE MP_CCTV_CHANNEL_6
				CASE MP_CCTV_CHANNEL_7
				CASE MP_CCTV_CHANNEL_8
				CASE MP_CCTV_CHANNEL_9
				CASE MP_CCTV_CHANNEL_10
				CASE MP_CCTV_CHANNEL_11
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			IF GET_INTERIOR_FLOOR_INDEX() = 0
				SWITCH eChannel
					CASE MP_CCTV_CHANNEL_1
					CASE MP_CCTV_CHANNEL_2
					CASE MP_CCTV_CHANNEL_3
					CASE MP_CCTV_CHANNEL_4
					CASE MP_CCTV_CHANNEL_5
					CASE MP_CCTV_CHANNEL_6
					CASE MP_CCTV_CHANNEL_7
					CASE MP_CCTV_CHANNEL_8
					CASE MP_CCTV_CHANNEL_9
						RETURN TRUE
					BREAK
				ENDSWITCH
			ELSE
				SWITCH eChannel
					CASE MP_CCTV_CHANNEL_1
					CASE MP_CCTV_CHANNEL_2
					CASE MP_CCTV_CHANNEL_3
					CASE MP_CCTV_CHANNEL_4
					CASE MP_CCTV_CHANNEL_5
					CASE MP_CCTV_CHANNEL_6
						RETURN TRUE
					BREAK
				ENDSWITCH
			ENDIF
		BREAK

		CASE SIMPLE_INTERIOR_SUBMARINE
			RETURN TRUE
		BREAK
		
		CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
		CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
		CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON
		CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
		CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0
				CASE MP_CCTV_CHANNEL_1
				CASE MP_CCTV_CHANNEL_2
				CASE MP_CCTV_CHANNEL_3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		#IF FEATURE_MUSIC_STUDIO
		CASE SIMPLE_INTERIOR_MUSIC_STUDIO
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0
				CASE MP_CCTV_CHANNEL_1
				CASE MP_CCTV_CHANNEL_2
				CASE MP_CCTV_CHANNEL_3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		
		#IF FEATURE_FIXER
		CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
		CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
		CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
		CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
			SWITCH eChannel
				CASE MP_CCTV_CHANNEL_0
				CASE MP_CCTV_CHANNEL_1
				CASE MP_CCTV_CHANNEL_2
				CASE MP_CCTV_CHANNEL_3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
								
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_STATIC_VALUE(SIMPLE_INTERIORS propertyID)
	IF propertyID != SIMPLE_INTERIOR_INVALID
		IF GET_SIMPLE_INTERIOR_TYPE(propertyID) = SIMPLE_INTERIOR_TYPE_BUSINESS_HUB
		AND GET_INTERIOR_FLOOR_INDEX() = 0
			RETURN 2.0
		ENDIF
	ENDIF
	
	RETURN 0.1
ENDFUNC

/// PURPOSE:
///    Processes switching between the local machines currently displayed channel, and the channel that the local player should be watching
/// PARAMS:
///    MPTVLocal - Local data struct
///    propertyID - Simple interior ID
/// RETURNS:
///    Returns true if channel is changed
FUNC BOOL CLIENT_MAINTAIN_CURRENT_WATCHED_CCTV_CHANNEL(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, SIMPLE_INTERIORS propertyID, BOOL bResetTimecycleModifier = FALSE, BOOL bWeed = FALSE)
	MPCCTVLocal.eSimpleInteriorType = propertyID
	
	BOOL bFinished = FALSE
	BOOL bCCTVToCCTV = FALSE
	BOOL bReturnTrue = FALSE
	BOOL bFullscreenToFullscreen = FALSE
	
	CCTV_DETAILS thisCCTV
	
	bFullscreenToFullscreen = FALSE
	
	IF NOT IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_ACTIVATING)
	AND NOT IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_DEACTIVATING)
	AND IS_MP_CCTV_CHANNEL_FULLSCREEN(MPCCTVLocal.eSwitchChannelFrom)
	AND IS_MP_CCTV_CHANNEL_FULLSCREEN(MPCCTVLocal.eSwitchChannelTo)
		bFullscreenToFullscreen = TRUE
		
		IF IS_MP_CCTV_CHANNEL_CCTV(MPCCTVLocal.eSwitchChannelFrom)
		AND IS_MP_CCTV_CHANNEL_CCTV(MPCCTVLocal.eSwitchChannelTo)
			bCCTVToCCTV = TRUE
		ENDIF
	ENDIF
	
	SWITCH GET_MP_CCTV_SWITCH_STATE(MPCCTVLocal)
		CASE MP_CCTV_SWITCH_STATE_SETUP_PROCESS
			bFinished = FALSE
			
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_DEACTIVATING)
				IF NOT IS_MP_CCTV_CHANNEL_FULLSCREEN(MPCCTVLocal.eSwitchChannelFrom)
					bFinished = TRUE
				ENDIF
			ELIF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_ACTIVATING)
				IF NOT IS_MP_CCTV_CHANNEL_FULLSCREEN(MPCCTVLocal.eSwitchChannelTo)
					bFinished = TRUE
				ENDIF
			ELSE
				IF NOT IS_MP_CCTV_CHANNEL_FULLSCREEN(MPCCTVLocal.eSwitchChannelFrom)
				AND NOT IS_MP_CCTV_CHANNEL_FULLSCREEN(MPCCTVLocal.eSwitchChannelTo)
					bFinished = TRUE
				ENDIF
			ENDIF
			
			IF bFinished
				SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_UPDATE_BUTTONS)
				
				SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_CLEANUP_PROCESS)
			ELSE
				IF bCCTVToCCTV
					SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
				ENDIF
				
				SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_FADE_OUT)
			ENDIF
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_FADE_OUT
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT(TRUE)
			
			MPCCTVLocal.bSpectatorFadesDisabled = TRUE
			
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT bFullscreenToFullscreen
				IF NOT IS_SCREEN_FADING_OUT()
					IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_ACTIVATING)
						MP_CCTV_FADE_OUT(MPCCTVLocal, MP_CCTV_FADE_TIME_POWER_CHANGE)
					ELSE
						MP_CCTV_FADE_OUT(MPCCTVLocal, MP_CCTV_FADE_TIME_POWER_CHANGE)
					ENDIF
				ENDIF
			ELSE
				IF bFullscreenToFullscreen
					IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
						SET_TIMECYCLE_MODIFIER("CAMERA_secuirity_FUZZ")
						
						SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_STARTED_TIMECYCLE)
					ELSE
						SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_AUDIO_FADE)
						
						SET_AUDIO_FLAG ("AllowRadioOverScreenFade", TRUE)
					ENDIF
					
					SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DRAW_LOADING_ICON)
					
					REFRESH_SCALEFORM_LOADING_ICON(MPCCTVLocal.loadingIcon)
					
					IF NOT g_LastPlayerAliveTinyRacers
					AND (NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
					OR  IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()  )
						MP_CCTV_SOUND_START_CHANGE_CAM(MPCCTVLocal)
					ENDIF
					
					IF NOT bCCTVToCCTV
						CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_ACTIVATING)
				AND IS_BIT_SET(GlobalPlayerBD_SCTV[NATIVE_TO_INT(PLAYER_ID())].iGeneralBitSet, GPBDS_GBI_USING_THERMAL)
					SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLE_THERMAL)
					
					ENABLE_NIGHTVISION(VISUALAID_OFF)
				ENDIF
				
				MPCCTVLocal.iChannelSwitchTimer = GET_GAME_TIMER()
				
				MP_CCTV_FADE_OUT(MPCCTVLocal, MP_CCTV_FADE_TIME_POWER_CHANGE)
				
				SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_WAIT_FOR_FADE_OUT)
			ENDIF
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_WAIT_FOR_FADE_OUT
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			IF IS_SCREEN_FADED_OUT()
				IF bFullscreenToFullscreen
					IF NOT g_bMissionEnding
						TOGGLE_RENDERPHASES(FALSE)
					ENDIF
					
					SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_PAUSED_RENDERPHASE)
				ENDIF
				
				// Set flag if this is an external channel once the screen has faded out
				SET_LOCAL_PLAYER_VIEWING_CCTV_EXTERNAL(IS_CHANNEL_EXTERIOR(MPCCTVLocal, MPCCTVLocal.eCurrentChannel, propertyID))
				
				SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_CLEANUP_CURRENT)
			ENDIF
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_CLEANUP_CURRENT
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			SWITCH MPCCTVLocal.eSwitchChannelFrom
				CASE MP_CCTV_CHANNEL_0
				CASE MP_CCTV_CHANNEL_1
				CASE MP_CCTV_CHANNEL_2
				CASE MP_CCTV_CHANNEL_3
				CASE MP_CCTV_CHANNEL_4
				CASE MP_CCTV_CHANNEL_5
				CASE MP_CCTV_CHANNEL_6
				CASE MP_CCTV_CHANNEL_7
				CASE MP_CCTV_CHANNEL_8
				CASE MP_CCTV_CHANNEL_9
				CASE MP_CCTV_CHANNEL_10
				CASE MP_CCTV_CHANNEL_11
				CASE MP_CCTV_CHANNEL_12
				CASE MP_CCTV_CHANNEL_13
				CASE MP_CCTV_CHANNEL_14
					IF NOT bFullscreenToFullscreen
						IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
							CLEAR_TIMECYCLE_MODIFIER()
						ENDIF
						
						IF NOT IS_STRING_NULL_OR_EMPTY(g_drunkCameraTimeCycleName)
							SET_TIMECYCLE_MODIFIER(g_drunkCameraTimeCycleName)
						ENDIF
					
						IF bResetTimecycleModifier
							IF bWeed
								SET_TIMECYCLE_MODIFIER("mp_bkr_ware02_upgrade")
							ELSE
								SET_TIMECYCLE_MODIFIER("mp_bkr_ware03_upgrade")
							ENDIF
							
							SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_STARTED_TIMECYCLE)
						ENDIF
					ENDIF
					
					IF NOT bCCTVToCCTV
					OR IS_CHANNEL_EXTERIOR(MPCCTVLocal, MPCCTVLocal.eSwitchChannelFrom, propertyID)
						g_SimpleInteriorData.bSuppressConcealCheck = FALSE
						
						FORCE_SIMPLE_INTERIOR_CONCEAL_PLAYERS_CHECK()
						
						MPCCTVLocal.bConcealed = FALSE
						
						IF NOT bCCTVToCCTV
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							
							IF DOES_CAM_EXIST(MPCCTVLocal.camCCTV)
								DESTROY_CAM(MPCCTVLocal.camCCTV)
							ENDIF
						ENDIF
					ENDIF
					
					IF (NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
					OR  IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()  )
						MP_CCTV_SOUND_STOP_BACKGROUND(MPCCTVLocal)
					ENDIF
					
					IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FOCUS_SET)
						NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
						
						CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FOCUS_SET)
					ENDIF
					
					IF IS_AUDIO_SCENE_ACTIVE("MP_CCTV_SCENE")
						STOP_AUDIO_SCENE("MP_CCTV_SCENE")
					ENDIF
					
					IF IS_AUDIO_SCENE_ACTIVE("dlc_aw_arena_cctv_scene")
						STOP_AUDIO_SCENE("dlc_aw_arena_cctv_scene")
					ENDIF
					
					CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR)
					
					bFinished = TRUE
				BREAK
			ENDSWITCH
			
			IF bFinished
				SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_SETUP_DESIRED)
			ENDIF
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_SETUP_DESIRED
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_DEACTIVATING)
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					SET_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_LOAD_SCENE_REQUIRED)
				ENDIF
				
				SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_REQUEST_APARTMENT_AUDIO_SCENE)
				
				MPCCTVLocal.vLoadSceneLocation = GET_ENTITY_COORDS(PLAYER_PED_ID())
				MPCCTVLocal.vLoadSceneRot = GET_GAMEPLAY_CAM_ROT()
				
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(MPCCTVLocal.sfCCTV)
				
				IF propertyID = SIMPLE_INTERIOR_INVALID
				AND IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_BANNER_LOADED)
					SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("AW_CAM_BANNER")
					
					CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_BANNER_LOADED)
				ENDIF
				
				CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
				
				bFinished = TRUE
			ELSE
				SWITCH MPCCTVLocal.eSwitchChannelTo
					CASE MP_CCTV_CHANNEL_0
					CASE MP_CCTV_CHANNEL_1
					CASE MP_CCTV_CHANNEL_2
					CASE MP_CCTV_CHANNEL_3
					CASE MP_CCTV_CHANNEL_4
					CASE MP_CCTV_CHANNEL_5
					CASE MP_CCTV_CHANNEL_6
					CASE MP_CCTV_CHANNEL_7
					CASE MP_CCTV_CHANNEL_8
					CASE MP_CCTV_CHANNEL_9
					CASE MP_CCTV_CHANNEL_10
					CASE MP_CCTV_CHANNEL_11
					CASE MP_CCTV_CHANNEL_12
					CASE MP_CCTV_CHANNEL_13
					CASE MP_CCTV_CHANNEL_14
						ENABLE_MOVIE_SUBTITLES(FALSE)
						
						MPCCTVLocal.sfCCTV = REQUEST_SCALEFORM_MOVIE("SECURITY_CAM")
						
						IF propertyID = SIMPLE_INTERIOR_INVALID
							REQUEST_STREAMED_TEXTURE_DICT("AW_CAM_BANNER")
						ENDIF
						
						IF HAS_SCALEFORM_MOVIE_LOADED(MPCCTVLocal.sfCCTV)
						AND (propertyID != SIMPLE_INTERIOR_INVALID OR HAS_STREAMED_TEXTURE_DICT_LOADED("AW_CAM_BANNER"))
							IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
								SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_BANNER_LOADED)
							ENDIF
							
							BEGIN_SCALEFORM_MOVIE_METHOD(MPCCTVLocal.sfCCTV, "SET_LOCATION")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_CHANNEL_STRING(MPCCTVLocal, MPCCTVLocal.eSwitchChannelTo, propertyID))
							END_SCALEFORM_MOVIE_METHOD()
							
							BEGIN_SCALEFORM_MOVIE_METHOD(MPCCTVLocal.sfCCTV, "SET_DETAILS")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_CHANNEL_DESC_STRING(MPCCTVLocal.eSwitchChannelTo, propertyID))
							END_SCALEFORM_MOVIE_METHOD()
							
							BEGIN_SCALEFORM_MOVIE_METHOD(MPCCTVLocal.sfCCTV, "SET_TIME")
							END_SCALEFORM_MOVIE_METHOD()
							
							thisCCTV = GET_CCTV_DETAILS(MPCCTVLocal, propertyID, ENUM_TO_INT(MPCCTVLocal.eSwitchChannelTo))
							
							IF NOT (thisCCTV.vPos.x = 0.0
								AND thisCCTV.vPos.y = 0.0
								AND thisCCTV.vPos.z = 0.0)
							AND thisCCTV.fFOV <> 0.0
								IF DOES_CAM_EXIST(MPCCTVLocal.camCCTV)
									DESTROY_CAM(MPCCTVLocal.camCCTV)
								ENDIF
								
								IF NOT bFullscreenToFullscreen
								AND NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
									SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
									
									SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_STARTED_TIMECYCLE)
								ENDIF
								
								SET_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_LOAD_SCENE_REQUIRED)
								
								IF MPCCTVLocal.vehToAttachCCTVTo <> NULL
								AND DOES_ENTITY_EXIST(MPCCTVLocal.vehToAttachCCTVTo)
								AND NOT IS_ENTITY_DEAD(MPCCTVLocal.vehToAttachCCTVTo)
									MPCCTVLocal.vLoadSceneLocation = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(MPCCTVLocal.vehToAttachCCTVto, thisCCTV.vPos)
								ELSE
									MPCCTVLocal.vLoadSceneLocation = thisCCTV.vPos
								ENDIF
								
								MPCCTVLocal.camCCTV = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
								
								// Set the position of the camera, or attach it to an entity
								IF MPCCTVLocal.vehToAttachCCTVTo <> NULL
								AND DOES_ENTITY_EXIST(MPCCTVLocal.vehToAttachCCTVTo)
								AND NOT IS_ENTITY_DEAD(MPCCTVLocal.vehToAttachCCTVTo)
									PRINTLN("Attaching cctv cam to entity")
									
									SET_CAM_FOV(MPCCTVLocal.camCCTV, thisCCTV.fFOV)
									
									ATTACH_CAM_TO_VEHICLE_BONE(MPCCTVLocal.camCCTV, MPCCTVLocal.vehToAttachCCTVTo, 0, TRUE, thisCCTV.vRot, thisCCTV.vPos, TRUE)
								ELSE
									PRINTLN("Setting cctv cam static position")
									
									SET_CAM_COORD(MPCCTVLocal.camCCTV, thisCCTV.vPos)
									SET_CAM_ROT(MPCCTVLocal.camCCTV, thisCCTV.vRot)
									SET_CAM_FOV(MPCCTVLocal.camCCTV, thisCCTV.fFOV)
								ENDIF
								
								IF NOT MPCCTVLocal.bConcealed
								AND NOT SHOULD_CHANNEL_CONCEAL_PLAYERS(MPCCTVLocal.eSwitchChannelTo, propertyID)
								AND NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
									g_SimpleInteriorData.bSuppressConcealCheck = TRUE
									
									FORCE_SIMPLE_INTERIOR_CONCEAL_PLAYERS_CHECK()
									
									MPCCTVLocal.bConcealed = TRUE
								ENDIF
								
								SET_CAM_ACTIVE(MPCCTVLocal.camCCTV, TRUE)
								
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								
								SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLE_EXTERIOR_FOCUS)
								SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_STATIC_OUTSIDE_CAM_ON)
								SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR)
								SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
								
								IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
									IF IS_AUDIO_SCENE_ACTIVE("MP_APARTMENT_SCENE")
										STOP_AUDIO_SCENE("MP_APARTMENT_SCENE")
									ENDIF
									
									IF IS_AUDIO_SCENE_ACTIVE("MP_GARAGE_SCENE")
										STOP_AUDIO_SCENE("MP_GARAGE_SCENE")
									ENDIF
									
									START_AUDIO_SCENE("MP_CCTV_SCENE")
								ENDIF
								
								IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
									START_AUDIO_SCENE("dlc_aw_arena_cctv_scene")
								ENDIF
								
								MPCCTVLocal.fCameraAngle = 0.0
								
								bFinished = TRUE
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			IF bFinished
				SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_WAIT_FOR_FILTER_TIME)
			ENDIF
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_WAIT_FOR_FILTER_TIME
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			IF GET_GAME_TIMER() - MPCCTVLocal.iChannelSwitchTimer > SPEC_HUD_FILTER_CHANGE_TIME
				IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_LOAD_SCENE_REQUIRED)
					SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_SETUP_LOAD_SCENE)
				ELSE
					IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
					OR propertyID = SIMPLE_INTERIOR_INVALID
						IF NOT IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_DEACTIVATING)
							SET_NOISINESSOVERIDE(GET_STATIC_VALUE(propertyID))
							SET_NOISEOVERIDE(TRUE)
						ELSE
							SET_NOISINESSOVERIDE(0.0)
							SET_NOISEOVERIDE(FALSE)
						ENDIF
					ENDIF
					
					IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
					AND NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
						SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_WAIT_FOR_FADE_IN)
					ELSE
						SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_FADE_IN)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_SETUP_LOAD_SCENE
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_LOAD_SCENE_RUNNING)
				NEW_LOAD_SCENE_STOP()
			ENDIF
			
			IF MPCCTVLocal.vLoadSceneRot.x = 0.0
			AND MPCCTVLocal.vLoadSceneRot.y = 0.0
			AND MPCCTVLocal.vLoadSceneRot.z = 0.0
				NEW_LOAD_SCENE_START_SPHERE(MPCCTVLocal.vLoadSceneLocation, 50.0)
			ELSE
				NEW_LOAD_SCENE_START(MPCCTVLocal.vLoadSceneLocation, MPCCTVLocal.vLoadSceneRot, 300)
			ENDIF
			
			SET_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_LOAD_SCENE_RUNNING)
			
			SET_FOCUS_POS_AND_VEL(MPCCTVLocal.vLoadSceneLocation, <<0.0, 0.0, 0.0>>)
			
			NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
			
			SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FOCUS_SET)
			
			MPCCTVLocal.iChannelSwitchTimer = GET_GAME_TIMER()
			
			SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_WAIT_FOR_LOAD_SCENE)
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_WAIT_FOR_LOAD_SCENE
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			IF IS_NEW_LOAD_SCENE_LOADED()
			OR GET_GAME_TIMER() - MPCCTVLocal.iChannelSwitchTimer > MP_CCTV_LOAD_SCENE_TIMEOUT
				IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_LOAD_SCENE_RUNNING)
					NEW_LOAD_SCENE_STOP()
					CLEAR_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_LOAD_SCENE_RUNNING)
				ENDIF
				
				SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_CLEANUP_LOAD_SCENE)
			ENDIF
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_CLEANUP_LOAD_SCENE
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			bFinished = FALSE
			
			IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_DEACTIVATING)
				CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLE_EXTERIOR_FOCUS)
				CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_STATIC_OUTSIDE_CAM_ON)
				
				SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_REQUEST_APARTMENT_AUDIO_SCENE)
				
				bFinished = TRUE
			ELSE
				bFinished = TRUE
			ENDIF
			
			IF bFinished = TRUE
				CLEAR_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_LOAD_SCENE_REQUIRED)
				
				MPCCTVLocal.vLoadSceneLocation = <<0.0, 0.0, 0.0>>
				MPCCTVLocal.vLoadSceneRot = <<0.0, 0.0, 0.0>>
				
				RESET_ADAPTATION()
				
				IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
				OR propertyID = SIMPLE_INTERIOR_INVALID
					IF NOT IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_DEACTIVATING)
						SET_NOISINESSOVERIDE(GET_STATIC_VALUE(propertyID))
						SET_NOISEOVERIDE(TRUE)
					ELSE
						SET_NOISINESSOVERIDE(0.0)
						SET_NOISEOVERIDE(FALSE)
					ENDIF
				ENDIF
				
				IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
				AND NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
					SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_WAIT_FOR_FADE_IN)
				ELSE
					SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_FADE_IN)
				ENDIF
			ENDIF
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_WAIT_FOR_FADE_IN
			IF NOT HAS_NET_TIMER_STARTED(MPCCTVLocal.stFadeInDelay)
				START_NET_TIMER(MPCCTVLocal.stFadeInDelay)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(MPCCTVLocal.stFadeInDelay, 1000)
					RESET_NET_TIMER(MPCCTVLocal.stFadeInDelay)
					
					SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_FADE_IN)
				ENDIF
			ENDIF
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_FADE_IN
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			IF g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
			OR propertyID = SIMPLE_INTERIOR_INVALID
				IF NOT IS_SCREEN_FADED_IN()
					IF NOT IS_SCREEN_FADING_IN()
						IF NOT g_bMissionEnding
							TOGGLE_RENDERPHASES(TRUE)
						ENDIF
						
						IF bFullscreenToFullscreen
							MP_CCTV_FADE_IN(MPCCTVLocal, MP_CCTV_FADE_TIME_POWER_CHANGE)
						ELSE
							MP_CCTV_FADE_IN(MPCCTVLocal, MP_CCTV_FADE_TIME_POWER_CHANGE)
						ENDIF
						
						IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_DEACTIVATING)
							IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLE_THERMAL)
								ENABLE_NIGHTVISION(VISUALAID_THERMAL)
								
								CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLE_THERMAL)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_CLEANUP_PROCESS)
				ENDIF
			ENDIF
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_CLEANUP_PROCESS
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			IF (NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
			OR  IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()  )
				MP_CCTV_SOUND_STOP_CHANGE_CAM(MPCCTVLocal)
			ENDIF
			
			IF IS_MP_CCTV_CHANNEL_CCTV(MPCCTVLocal.eSwitchChannelTo)
			AND NOT IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_DEACTIVATING)
				IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
				OR propertyID = SIMPLE_INTERIOR_INVALID
					IF propertyID != SIMPLE_INTERIOR_INVALID
						SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
						
						SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_STARTED_TIMECYCLE)
					ENDIF
					
					SET_NOISINESSOVERIDE(0.1)
					SET_NOISEOVERIDE(TRUE)
					
					IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
					OR IS_PLAYER_USING_ARENA_BOX_SPECTATORS_TABLE()
						SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LAUNCH_LIVE_STREAM)
					ENDIF
					
					MP_CCTV_SOUND_START_BACKGROUND(MPCCTVLocal)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
					CLEAR_TIMECYCLE_MODIFIER()
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(g_drunkCameraTimeCycleName)
					SET_TIMECYCLE_MODIFIER(g_drunkCameraTimeCycleName)
				ENDIF
				
				SET_NOISINESSOVERIDE(0.0)
				SET_NOISEOVERIDE(FALSE)
				
				IF bResetTimecycleModifier
					IF bWeed
						SET_TIMECYCLE_MODIFIER("mp_bkr_ware02_upgrade")
					ELSE
						SET_TIMECYCLE_MODIFIER("mp_bkr_ware03_upgrade")
					ENDIF
					
					SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_STARTED_TIMECYCLE)
				ENDIF
			ENDIF
			
			CLEAR_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_LOAD_SCENE_REQUIRED)
			
			MPCCTVLocal.vLoadSceneLocation = <<0.0, 0.0, 0.0>>
			MPCCTVLocal.vLoadSceneRot = <<0.0, 0.0, 0.0>>
			
			SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_UPDATE_BUTTONS)
			
			CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DRAW_LOADING_ICON)
			
			REFRESH_SCALEFORM_LOADING_ICON(MPCCTVLocal.loadingIcon, TRUE)
			
			ENABLE_SPECTATOR_FADES()
			
			MPCCTVLocal.bSpectatorFadesDisabled = FALSE
			
			SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_UPDATE_BUTTONS)
			
			SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_WATCH_CHANNEL)
		BREAK
		
		CASE MP_CCTV_SWITCH_STATE_WATCH_CHANNEL
			IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FULLSCREEN_TV_TEXT)
				IF IS_MP_CCTV_CHANNEL_FULLSCREEN(MPCCTVLocal.eCurrentChannel)
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FULLSCREEN_TV_TEXT, TRUE)
				ENDIF
			ENDIF
			
			IF IS_SCREEN_FADED_IN()
				IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
					SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
					
					CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_AUDIO_FADE)
				ENDIF
			ENDIF
			
			// Widget update
			#IF IS_DEBUG_BUILD
			IF MPCCTVWidgetData.bUpdateCam
				thisCCTV = GET_CCTV_DETAILS(MPCCTVLocal, propertyID, ENUM_TO_INT(MPCCTVLocal.eSwitchChannelTo))
				
				IF MPCCTVLocal.vehToAttachCCTVTo <> NULL
				AND DOES_ENTITY_EXIST(MPCCTVLocal.vehToAttachCCTVTo)
				AND NOT IS_ENTITY_DEAD(MPCCTVLocal.vehToAttachCCTVTo)
					DETACH_CAM(MPCCTVLocal.camCCTV)
					
					ATTACH_CAM_TO_VEHICLE_BONE(MPCCTVLocal.camCCTV, MPCCTVLocal.vehToAttachCCTVTo, 0, TRUE, thisCCTV.vRot, thisCCTV.vPos, TRUE)
				ELSE
					SET_CAM_COORD(MPCCTVLocal.camCCTV, thisCCTV.vPos)
					SET_CAM_ROT(MPCCTVLocal.camCCTV,thisCCTV.vRot)
				ENDIF
				
				SET_CAM_FOV(MPCCTVLocal.camCCTV, thisCCTV.fFov)
			ENDIF
			#ENDIF
			
			SWITCH MPCCTVLocal.eCurrentChannel
				CASE MP_CCTV_CHANNEL_0
				CASE MP_CCTV_CHANNEL_1
				CASE MP_CCTV_CHANNEL_2
				CASE MP_CCTV_CHANNEL_3
				CASE MP_CCTV_CHANNEL_4
				CASE MP_CCTV_CHANNEL_5
				CASE MP_CCTV_CHANNEL_6
				CASE MP_CCTV_CHANNEL_7
				CASE MP_CCTV_CHANNEL_8
				CASE MP_CCTV_CHANNEL_9
				CASE MP_CCTV_CHANNEL_10
				CASE MP_CCTV_CHANNEL_11
				CASE MP_CCTV_CHANNEL_12
				CASE MP_CCTV_CHANNEL_13
				CASE MP_CCTV_CHANNEL_14
					IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
						SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
					ENDIF
				BREAK
			ENDSWITCH
			
			IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_ACTIVATING)
				IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
					SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_DEACTIVATING)
				IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
					CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
				ENDIF
			ENDIF
			
			bReturnTrue = TRUE
		BREAK
	ENDSWITCH
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DRAW_LOADING_ICON)
			RUN_SCALEFORM_LOADING_ICON(MPCCTVLocal.loadingIcon, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(MPCCTVLocal.loadingIcon))
		ENDIF
		
		IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
			SWITCH MPCCTVLocal.eCurrentChannel
				CASE MP_CCTV_CHANNEL_0
				CASE MP_CCTV_CHANNEL_1
				CASE MP_CCTV_CHANNEL_2
				CASE MP_CCTV_CHANNEL_3
				CASE MP_CCTV_CHANNEL_4
				CASE MP_CCTV_CHANNEL_5
				CASE MP_CCTV_CHANNEL_6
				CASE MP_CCTV_CHANNEL_7
				CASE MP_CCTV_CHANNEL_8
				CASE MP_CCTV_CHANNEL_9
				CASE MP_CCTV_CHANNEL_10
				CASE MP_CCTV_CHANNEL_11
				CASE MP_CCTV_CHANNEL_12
				CASE MP_CCTV_CHANNEL_13
				CASE MP_CCTV_CHANNEL_14
					IF HAS_SCALEFORM_MOVIE_LOADED(MPCCTVLocal.sfCCTV)
					AND NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
						SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD_PRIORITY_LOW)
						
						DRAW_SCALEFORM_MOVIE_FULLSCREEN(MPCCTVLocal.sfCCTV, 255, 255, 255, 0)
						
						RESET_SCRIPT_GFX_ALIGN()
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN bReturnTrue
ENDFUNC

/// PURPOSE:
///    Updates each frame that the cctv is active to pan the camera left and right
/// PARAMS:
///    MPCCTVLocal - Local data struct
PROC MAINTAIN_CAMERA_ROTATION(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, BOOL bTurnLeft, CCTV_DETAILS& camDetails)
	IF DOES_CAM_EXIST(MPCCTVLocal.camCCTV)
		VECTOR vNewCamRot = GET_CAM_ROT(MPCCTVLocal.camCCTV)
		
		FLOAT fStep = (GET_FRAME_TIME() * 25)
		
		IF bTurnLeft
			IF MPCCTVLocal.fCameraAngle < camDetails.fMaxAngleLeft
				vNewCamRot.z += fStep
				
				MPCCTVLocal.fCameraAngle += fStep
				
				IF MPCCTVLocal.fCameraAngle > camDetails.fMaxAngleLeft
					vNewCamRot.z -= fStep
					
					MPCCTVLocal.fCameraAngle -= fStep
					
					vNewCamRot.z += (camDetails.fMaxAngleLeft - MPCCTVLocal.fCameraAngle)
					
					MPCCTVLocal.fCameraAngle = camDetails.fMaxAngleLeft
					
					MP_CCTV_SOUND_STOP_PAN_CAM(MPCCTVLocal)
				ELSE
					MP_CCTV_SOUND_START_PAN_CAM(MPCCTVLocal)
				ENDIF
				
				IF vNewCamRot.z > 360.0
					vNewCamRot.z -= 360.0
				ENDIF
				
				IF DOES_ENTITY_EXIST(MPCCTVLocal.vehToAttachCCTVto)
					DETACH_CAM(MPCCTVLocal.camCCTV)
					
					ATTACH_CAM_TO_VEHICLE_BONE(MPCCTVLocal.camCCTV, MPCCTVLocal.vehToAttachCCTVto, 0, TRUE, camDetails.vRot + <<0,0,MPCCTVLocal.fCameraAngle>>, camDetails.vPos, true)
				ELSE
					SET_CAM_ROT(MPCCTVLocal.camCCTV, vNewCamRot)
				ENDIF
			ELSE
				MP_CCTV_SOUND_STOP_PAN_CAM(MPCCTVLocal)
			ENDIF
		ELSE
			IF MPCCTVLocal.fCameraAngle > -camDetails.fMaxAngleRight
				vNewCamRot.z -= fStep
				
				MPCCTVLocal.fCameraAngle -= fStep
				
				IF MPCCTVLocal.fCameraAngle < -camDetails.fMaxAngleRight
					vNewCamRot.z += fStep
					
					MPCCTVLocal.fCameraAngle += fStep
					
					vNewCamRot.z -= (camDetails.fMaxAngleRight + MPCCTVLocal.fCameraAngle)
					
					MPCCTVLocal.fCameraAngle = -camDetails.fMaxAngleRight
					
					MP_CCTV_SOUND_STOP_PAN_CAM(MPCCTVLocal)
				ELSE
					MP_CCTV_SOUND_START_PAN_CAM(MPCCTVLocal)
				ENDIF
				
				IF vNewCamRot.z < -360.0
					vNewCamRot.z += 360.0
				ENDIF
				
				IF DOES_ENTITY_EXIST(MPCCTVLocal.vehToAttachCCTVto)
					DETACH_CAM(MPCCTVLocal.camCCTV)
					
					ATTACH_CAM_TO_VEHICLE_BONE(MPCCTVLocal.camCCTV, MPCCTVLocal.vehToAttachCCTVto, 0, TRUE, camDetails.vRot + <<0,0,MPCCTVLocal.fCameraAngle>>, camDetails.vPos, true)
				ELSE
					SET_CAM_ROT(MPCCTVLocal.camCCTV, vNewCamRot)
				ENDIF
			ELSE
				MP_CCTV_SOUND_STOP_PAN_CAM(MPCCTVLocal)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Processes the player cctv input
/// PARAMS:
///    MPTVClient - Client broadcast data
///    MPTVLocal - Local data struct
/// RETURNS:
///    True if player declares they no longer want to interact with the cctv
FUNC BOOL CLIENT_MAINTAIN_MP_CCTV_INPUT(MP_CCTV_CLIENT_DATA_STRUCT &MPCCTVClient, MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	AND NOT IS_BROWSER_OPEN()
	AND MPCCTVLocal.eSwitchState = MP_CCTV_SWITCH_STATE_WATCH_CHANNEL
		IF MPCCTVClient.eTimeOfInputChannel <> MPCCTVClient.eCurrentChannel
			CLEAR_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_NEXT_CHANNEL)
			CLEAR_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_PREVIOUS_CHANNEL)
			
			MPCCTVClient.eTimeOfInputChannel = MPCCTVClient.eCurrentChannel
		ENDIF
		
		CCTV_DETAILS cctvDetails = GET_CCTV_DETAILS(MPCCTVLocal, MPCCTVLocal.eSimpleInteriorType, ENUM_TO_INT(MPCCTVLocal.eCurrentChannel))
		
		ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		
		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE))
			IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_UPDATE_BUTTONS)
			SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_CLOSING_CAM)
			
			MP_CCTV_SOUND_STOP_PAN_CAM(MPCCTVLocal)
			
			RETURN TRUE
		ELSE
			INT iVertIncrement = 0
			
			IF NOT (cctvDetails.fMaxAngleLeft = 0 AND cctvDetails.fMaxAngleRight = 0)
			AND (NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
			 OR IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()  )
				IF (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_MOVE_LR) > 0.3)
					MAINTAIN_CAMERA_ROTATION(MPCCTVLocal, FALSE, cctvDetails)
				ELIF (GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_MOVE_LR) < -0.3)
					MAINTAIN_CAMERA_ROTATION(MPCCTVLocal, TRUE, cctvDetails)
				ELSE
					MP_CCTV_SOUND_STOP_PAN_CAM(MPCCTVLocal)
				ENDIF
			ENDIF
			
			IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
				CONTROL_ACTION eLeftCamControl = INPUT_SCRIPT_LB
				CONTROL_ACTION eRightCamControl = INPUT_SCRIPT_RB
				
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					eLeftCamControl = INPUT_COVER
					eRightCamControl = INPUT_CONTEXT
				ENDIF
				
				IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, eLeftCamControl)
					CLEAR_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_NEXT_CHANNEL)
					CLEAR_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_PREVIOUS_CHANNEL)
					
					SET_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_PREVIOUS_CHANNEL)
					
					SETTIMERA(0)
				ELIF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, eRightCamControl)
					CLEAR_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_NEXT_CHANNEL)
					CLEAR_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_PREVIOUS_CHANNEL)
					
					SET_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_NEXT_CHANNEL)
					
					SETTIMERA(0)
				ENDIF
			ELSE
				IF SHOULD_ANALOGUE_SELECTION_BE_INCREMENTED(MPCCTVLocal.timeIncrement, iVertIncrement, FALSE, -1, FALSE)
					CLEAR_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_NEXT_CHANNEL)
					CLEAR_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_PREVIOUS_CHANNEL)
					
					IF iVertIncrement = 1
					OR iVertIncrement = -1
						IF TIMERA() > 300
							IF iVertIncrement = 1
								SET_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_NEXT_CHANNEL)
							ELIF iVertIncrement = -1
								SET_BIT(MPCCTVClient.iInputBitset, MP_CCTV_INPUT_PREVIOUS_CHANNEL)
							ENDIF
							
							SETTIMERA(0)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets up client deactivating interacting with the cctv
/// PARAMS:
///    MPCCTVLocal - Local data struct
///    channelFrom - Final channel watched
PROC CLIENT_SETUP_CCTV_DEACTIVATING(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, MP_CCTV_CHANNEL channelFrom)
	CLIENT_SETUP_SWITCH_CCTV_CHANNEL(MPCCTVLocal, channelFrom, channelFrom)
	
	ENABLE_MOVIE_SUBTITLES(FALSE)
	
	SET_BIT(MPCCTVLocal.iSwitchBitset, MP_CCTV_SWITCH_BS_DEACTIVATING)
	
	CLEAR_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_STOP_MPTV_SPECTATE)
ENDPROC

/// PURPOSE:
///    Processes player in full-interaction mode with the cctv
/// PARAMS:
///    MPCCTVClient - Client data struct
///    MPCCTVLocal - Local data struct
///    propertyID - Simple interior ID
/// RETURNS:
///    True when player wants to stop interacting with cctv
FUNC BOOL CLIENT_MAINTAIN_WATCHING_CCTV(MP_CCTV_CLIENT_DATA_STRUCT &MPCCTVClient, MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, SIMPLE_INTERIORS propertyID, BOOL bResetModifier = FALSE, BOOL bWeed = FALSE)
	DISABLE_DPADDOWN_THIS_FRAME()
	
	DISPLAY_AMMO_THIS_FRAME(FALSE)
	HUD_FORCE_WEAPON_WHEEL(FALSE)
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	
	DISABLE_SELECTOR_THIS_FRAME()
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_WATCH_TV_CLEANUP)
		IF CLIENT_MAINTAIN_CURRENT_WATCHED_CCTV_CHANNEL(MPCCTVLocal, propertyID)
			IF CLIENT_MAINTAIN_MP_CCTV_INPUT(MPCCTVClient, MPCCTVLocal)
			OR bInPropertyOwnerNotPaidLastUtilityBill
			OR g_bCelebrationScreenIsActive
			OR IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_STOP_MPTV_SPECTATE)
			OR (IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_BLOCK_USE_OF_CCTV)
			AND GET_LOCAL_PLAYER_USING_OFFICE_SEATID() = -1)
				
				#IF IS_DEBUG_BUILD
				IF bInPropertyOwnerNotPaidLastUtilityBill
					PRINTLN("[CCTV] - DEACTIVATING - bInPropertyOwnerNotPaidLastUtilityBill")
				ELIF g_bCelebrationScreenIsActive
					PRINTLN("[CCTV] - DEACTIVATING - g_bCelebrationScreenIsActive")
				ELIF IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_STOP_MPTV_SPECTATE)
					PRINTLN("[CCTV] - DEACTIVATING - IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_STOP_MPTV_SPECTATE)")
				ELIF (IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_BLOCK_USE_OF_CCTV)
				AND GET_LOCAL_PLAYER_USING_OFFICE_SEATID() = -1)
					PRINTLN("[CCTV] - DEACTIVATING - (IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_BLOCK_USE_OF_CCTV) AND GET_LOCAL_PLAYER_USING_OFFICE_SEATID() = -1)")
				ELSE
					PRINTLN("[CCTV] - DEACTIVATING - CLIENT_MAINTAIN_MP_CCTV_INPUT(MPCCTVClient, MPCCTVLocal) = TRUE")
				ENDIF
				#ENDIF
				
				CLIENT_SETUP_CCTV_DEACTIVATING(MPCCTVLocal, MPCCTVLocal.eCurrentChannel)
				
				SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_WATCH_TV_CLEANUP)
			ENDIF
		ELSE
			IF MPCCTVClient.iInputBitset <> 0
				MPCCTVClient.iInputBitset = 0
			ENDIF
		ENDIF
	ELSE
		IF CLIENT_MAINTAIN_CURRENT_WATCHED_CCTV_CHANNEL(MPCCTVLocal, propertyID,  bResetModifier, bWeed)
			CLIENT_CLEANUP_CCTV_CAMERA(MPCCTVLocal)
			
			CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_WATCH_TV_CLEANUP)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(MPCCTVLocal.camCCTV)
	AND IS_CAM_RENDERING(MPCCTVLocal.camCCTV)
		CCTV_DETAILS cctvDetails = GET_CCTV_DETAILS(MPCCTVLocal, MPCCTVLocal.eSimpleInteriorType, ENUM_TO_INT(MPCCTVLocal.eCurrentChannel))
		
		IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_UPDATE_BUTTONS)
		OR HAVE_CONTROLS_CHANGED(PLAYER_CONTROL)
			REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPCCTVLocal.scaleformInstructionalButtons)
			
			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE)), "HUD_INPUT3", MPCCTVLocal.scaleformInstructionalButtons)
			
			IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
				CONTROL_ACTION eLeftCamControl = INPUT_SCRIPT_LB
				CONTROL_ACTION eRightCamControl = INPUT_SCRIPT_RB
				
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					eLeftCamControl = INPUT_COVER
					eRightCamControl = INPUT_CONTEXT
				ENDIF
				
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eRightCamControl), "CAM_CYCLE_R", MPCCTVLocal.scaleformInstructionalButtons)
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eLeftCamControl), "CAM_CYCLE_L", MPCCTVLocal.scaleformInstructionalButtons)
			ELSE
				IF MPCCTVLocal.iNumberOfChannels > 1
					STRING strTitle = "HUD_INPUT75"
					
					IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
						strTitle = "SELECT_VIEW_DJ"
					ENDIF
					
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_MOVE_UD), strTitle, MPCCTVLocal.scaleformInstructionalButtons)
				ENDIF
			ENDIF
			
			IF NOT (cctvDetails.fMaxAngleLeft = 0 AND cctvDetails.fMaxAngleRight = 0)
			AND (NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
			 OR IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX() )
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_MOVE_LR), "HUD_INPUT101", MPCCTVLocal.scaleformInstructionalButtons)
			ENDIF
			
			CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_UPDATE_BUTTONS)
		ENDIF
		
		SPRITE_PLACEMENT thisSpritePlacement = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
		
		SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(MPCCTVLocal.scaleformInstructionalButtons, 1.0)
		
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPCCTVLocal.sfButton, thisSpritePlacement, MPCCTVLocal.scaleformInstructionalButtons, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPCCTVLocal.scaleformInstructionalButtons))
		
		SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
		
		IF IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
			SPRITE_PLACEMENT sprArena
			
			sprArena.x = 0.85
			sprArena.y = 0.15
			
			sprArena.w = 0.25
			sprArena.h = 0.25
			
			sprArena.r = 255
			sprArena.g = 255
			sprArena.b = 255
			sprArena.a = 255
			
			DRAW_2D_SPRITE("AW_CAM_BANNER", "AW_CAM_BANNER", sprArena)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CAM_STREAMING_FOCUS(MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal)
	IF DOES_ENTITY_EXIST(MPCCTVLocal.vehToAttachCCTVto)
	AND NOT IS_ENTITY_DEAD(MPCCTVLocal.vehToAttachCCTVto)
		SET_FOCUS_POS_AND_VEL(GET_ENTITY_COORDS(MPCCTVLocal.vehToAttachCCTVto), GET_ENTITY_VELOCITY(MPCCTVLocal.vehToAttachCCTVto))
	ENDIF
ENDPROC

/// PURPOSE:
///    Client every frame update to be called by all players in the simple interior to handle displaying and interacting with cctv
/// PARAMS:
///    MPCCTVClient - Client data struct
///    MPCCTVLocal - Local data struct
///    propertyID - Simple interior property ID
PROC CLIENT_MAINTAIN_MP_CCTV(MP_CCTV_CLIENT_DATA_STRUCT &MPCCTVClient, MP_CCTV_LOCAL_DATA_STRUCT &MPCCTVLocal, SIMPLE_INTERIORS propertyID, BOOL bResetTimecycleModifier = FALSE, BOOL bWeed = FALSE, INT iBlockerBitSet = 0, BOOL bAlternateButtonPress = FALSE)
	IF MPCCTVClient.eStage = MP_CCTV_CLIENT_STAGE_ACTIVATED
		CLIENT_MAINTAIN_AMBIENT_CCTV(MPCCTVClient, MPCCTVLocal)
		CLIENT_MAINTAIN_CURRENT_MP_CCTV_CHANNEL(MPCCTVClient, MPCCTVLocal, propertyID, iBlockerBitSet)
		UPDATE_VEHICLE_TO_ATTACH_TO(MPCCTVLocal)
		MAINTAIN_CAM_STREAMING_FOCUS(MPCCTVLocal)
	ENDIF
	
	IF MPCCTVClient.eStage <> MP_CCTV_CLIENT_STAGE_ACTIVATED
		IF MPCCTVClient.iInputBitset <> 0
			MPCCTVClient.iInputBitset = 0
		ENDIF
	ENDIF
	
	SWITCH MPCCTVClient.eStage
		CASE MP_CCTV_CLIENT_STAGE_INIT
			IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBlockLoadingTvForHeistCleanup
				SET_TV_AUDIO_FRONTEND(FALSE)
				
				GET_CCTV_LOCATION(MPCCTVLocal, propertyID)
				
				SET_MP_CCTV_CLIENT_STAGE(MPCCTVClient, MP_CCTV_CLIENT_STAGE_WALKING)
			ENDIF
		BREAK
		
		CASE MP_CCTV_CLIENT_STAGE_WALKING
			IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLED_SUICIDE)
				ENABLE_KILL_YOURSELF_OPTION()
				
				CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLED_SUICIDE)
			ENDIF
			
			IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLED_INTERACTION)
				ENABLE_INTERACTION_MENU()
				
				CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLED_INTERACTION)
			ENDIF
			
			IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_AUDIO_FRONTEND)
				CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_AUDIO_FRONTEND)
			ENDIF
			
			SET_TV_AUDIO_FRONTEND(FALSE)
			
			MPCCTVLocal.eSimpleInteriorType = propertyID
			
			IF CLIENT_MAINTAIN_ACTIVATING_CCTV(MPCCTVLocal, bAlternateButtonPress, propertyID)
				MPCCTVLocal.bUsedCCTV = TRUE
				
				CLIENT_SETUP_CCTV_ACTIVATING(MPCCTVLocal, MPCCTVLocal.eCurrentChannel)
				
				DISPLAY_CASH(FALSE)
				
				SET_MP_CCTV_CLIENT_STAGE(MPCCTVClient, MP_CCTV_CLIENT_STAGE_ACTIVATED)
			ENDIF
		BREAK
		
		CASE MP_CCTV_CLIENT_STAGE_ACTIVATED
			SET_IDLE_KICK_DISABLED_THIS_FRAME()
			
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				AND NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_ALTERNATE_VIEW)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				ENDIF
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES)
				
				SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_SET_CONTROLS)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_KILL_YOURSELF_OPTION_DISABLED()
				DISABLE_KILL_YOURSELF_OPTION()
				
				SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLED_SUICIDE)
			ENDIF
			
			IF NOT IS_INTERACTION_MENU_DISABLED()
				DISABLE_INTERACTION_MENU()
				
				SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_DISABLED_INTERACTION)
			ENDIF
			
			IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_AUDIO_FRONTEND)
				SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_AUDIO_FRONTEND)
			ENDIF
			
			IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_SET_WATCHING_TV)
				g_bMPTVplayerWatchingTV = TRUE
				g_bUsingCCTV = TRUE
				
				SET_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_SET_WATCHING_TV)
			ENDIF
			
			IF CLEAN_UP_ON_CALL_WITH_OUT_WAIT()
				IF CLIENT_MAINTAIN_WATCHING_CCTV(MPCCTVClient, MPCCTVLocal, propertyID, bResetTimecycleModifier, bWeed)
					MPCCTVClient.eCurrentChannel = MP_CCTV_CHANNEL_0
					
					MPCCTVLocal.eCurrentChannel = MP_CCTV_CHANNEL_0
					
					SET_MP_CCTV_SWITCH_STATE(MPCCTVLocal, MP_CCTV_SWITCH_STATE_WATCH_CHANNEL)
					
					MPCCTVLocal.tvChannelTypeCurrent = TVCHANNELTYPE_CHANNEL_NONE
					
					TIDYUP_MP_CCTV(MPCCTVLocal, TRUE)
					
					g_bMPTVplayerWatchingTV = FALSE
					g_bUsingCCTV = FALSE
					g_bUsingCCTVExternal = FALSE
					
					IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_SET_WATCHING_TV)
						CLEAR_BIT(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_SET_WATCHING_TV)
					ENDIF
					
					SET_MP_CCTV_CLIENT_STAGE(MPCCTVClient, MP_CCTV_CLIENT_STAGE_WALKING)
				ENDIF
			ENDIF
			
			SET_CLEAR_ON_CALL_HUD_THIS_FRAME(TRUE)
		BREAK
	ENDSWITCH
	
	IF MPCCTVClient.eStage = MP_CCTV_CLIENT_STAGE_ACTIVATED
		IF NOT IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR)
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)
		ELSE
			IF IS_BIT_SET(MPCCTVLocal.iBitset, MP_CCTV_LOCAL_BS_STATIC_OUTSIDE_CAM_ON)
				USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
				
				SET_SCRIPTED_CONVERSION_COORD_THIS_FRAME(GET_FINAL_RENDERED_CAM_COORD())
				
				THEFEED_HIDE_THIS_FRAME()
			ENDIF
		ENDIF
		
		DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
		
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	ENDIF
ENDPROC
