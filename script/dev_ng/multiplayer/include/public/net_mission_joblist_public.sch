USING "rage_builtins.sch"
USING "globals.sch"

USING "COMMANDS_NETWORK.sch"




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_JobList_Public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all functions that access only Joblist Variables - to avoid cyclic includes.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************

// PURPOSE:	This function returns the Mission Context Text label.
//
// INPUT PARAMS:		missionType					The mission Type as an INT
//						missionSubType				The mission SubType as an INT
//						contextID					
//						tunable
//
// RETURNS:	Returns TEXT_LABEL_63 containing the Mission Context Text label that this mission belongs to
//
// NOTES:	Added for 1905690 and jobslist equivalent
// 			This function returns the Mission Context Text label. This is used to get the information from the tunables.
// 			It works by first passing in the name of the value you are looking for, then based on the mission type and other
// 			information, will get the relevent information, first looking in CONTENT_MODIFIER_X lists and then by subtype and 
// 			finally main types. Currently does not support bike and cycle races as there is no way to distinguish between these
//			Tunables will be used from the main race type.
FUNC TEXT_LABEL_63 GET_MISSION_CONTEXT_STRING(INT missionType, INT missionSubType, INT contextID, TEXT_LABEL_63 tunable, BOOL isPushBikeOnly = FALSE, BOOL isAdverMode = FALSE, BOOL bMember = FALSE)
	TEXT_LABEL_63 label = ""
	
	// If this mission is found within the content modifier check if it has the correct tunables
	IF  contextID > -1	
		label = "CONTENT_MODIFIER_"
		
		
		#IF NOT FEATURE_GTAO_MEMBERSHIP
		UNUSED_PARAMETER(bMember)
		#ENDIF
		
		#IF FEATURE_GTAO_MEMBERSHIP
		IF bMember
			label = "CONTENT_MODIFIER_MEMBERSHIP_"
		ENDIF
		#ENDIF
		
		label += contextID
		PRINTLN("[MMM] Concatenated String is : ", label)
		
		// If it has the tunables, return the label
		IF NETWORK_DOES_TUNABLE_EXIST(label, tunable)
			RETURN label
		ELSE
			PRINTLN("[MMM] Tunable not found in context : ", label, ", searching for new context label ...")
		ENDIF		
		// otherwise continue and look for a different level
	ENDIF
	
	SWITCH missionType
		CASE FMMC_TYPE_RACE
			label = ("MP_FM_RACES")	// Should be overidden by the following cases but if not it shall use default
			SWITCH missionSubType
				CASE FMMC_RACE_TYPE_STANDARD
				CASE FMMC_RACE_TYPE_P2P
					label = ("MP_FM_RACES_CAR")
				BREAK
				CASE FMMC_RACE_TYPE_BOAT
				CASE FMMC_RACE_TYPE_BOAT_P2P
					label = ("MP_FM_RACES_SEA")
				BREAK
				CASE FMMC_RACE_TYPE_STUNT
				CASE FMMC_RACE_TYPE_STUNT_P2P
				CASE FMMC_RACE_TYPE_TARGET
				CASE FMMC_RACE_TYPE_TARGET_P2P
					label = ("MP_FM_RACES_STUNT")
				BREAK
				CASE FMMC_RACE_TYPE_AIR	
				CASE FMMC_RACE_TYPE_AIR_P2P
					label = ("MP_FM_RACES_AIR")
				BREAK
				CASE FMMC_RACE_TYPE_BASEJUMP
				CASE FMMC_RACE_TYPE_BASEJUMP_P2P
					label = ("MP_FM_BASEJUMP")
				BREAK
				// Currently Bikes and Cycles are grouped as one, as such rather than applying 
				// the bonus of one on the other unpredictably, we are doing neither and using 
				// the default race tunables.
				// KGM 5/9/14: Uncommented Mark's changes now that we can differentiate between bike and cycle races
				CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE
				CASE FMMC_RACE_TYPE_BIKE_AND_CYCLE_P2P
					IF isPushBikeOnly
						label = ("MP_FM_RACES_CYCLE")
					ELSE						
						label = ("MP_FM_RACES_BIKE")
					ENDIF
				BREAK
			ENDSWITCH
			IF NOT NETWORK_DOES_TUNABLE_EXIST(label, tunable)
				PRINTLN("[MMM] ", tunable, " not found in Race Sub Type : ", label, ", using default Race Label")
				label = ("MP_FM_RACES")	
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_MISSION
			label =  ("MP_FM_MISSIONS")
			IF isAdverMode
				label =  ("MP_FMADVERSARY")
			ELSE
				SWITCH missionSubType		
					CASE FMMC_MISSION_TYPE_STANDARD
						label =  ("MP_FM_MISSIONS")
					BREAK
					CASE FMMC_MISSION_TYPE_CONTACT
						label = ("MP_FM_CONTACT")
					BREAK
					CASE FMMC_MISSION_TYPE_RANDOM
						label = ("MP_FM_RANDOM")
					BREAK
					CASE FMMC_MISSION_TYPE_VERSUS
						label = ("MP_FM_VERSUS")
					BREAK
					CASE FMMC_MISSION_TYPE_COOP
						label = ("FMMC_RSTAR_MVS")
					BREAK
					CASE FMMC_MISSION_TYPE_LTS
						label = ("MP_FM_LTS")
					BREAK
					CASE FMMC_MISSION_TYPE_CTF
						label = ("MP_FM_CAPTURE")
					BREAK
				ENDSWITCH
			ENDIF
			
			IF NOT NETWORK_DOES_TUNABLE_EXIST(label, tunable)
				PRINTLN("[MMM] ", tunable, " not found in Mission Sub Type : ", label, ", using default Mission Label")
				label = ("MP_FM_MISSIONS")	
			ENDIF
 		BREAK	 
		CASE FMMC_TYPE_DEATHMATCH 
			label = ("MP_FM_DM")
		BREAK
		CASE FMMC_TYPE_SURVIVAL   
			label = ("MP_FM_SURVIVAL")
		BREAK
		CASE FMMC_TYPE_GANGHIDEOUT
			label = ("MP_FM_GANG_ATTACK")
		BREAK
		CASE FMMC_TYPE_BASE_JUMP  
			label = ("MP_FM_BASEJUMP")
		BREAK
	ENDSWITCH
		
	PRINTLN("[MMM] Label identified is : ", label)		
	
	RETURN label
ENDFUNC

// PURPOSE:	Gets the RP and Cash values for a specific mission provided the content ID Hash, Mission Type and SubType.
//
// INPUT PARAMS:		xpMultiplier				XP Multiplier (by reference)
//						cashMultiplier				Cash Multiplier (by reference)
//						contentIdHash				The missions content ID Hash value. Used to find associated modifier ID list
//						missionType					The mission Type as an INT
//						missionSubType				The mission SubType as an INT
//
// RETURNS:	Returns the xp and cash multipliers by reference using the associated variables	
//
// NOTES:	Added for 1905690 and jobslist equivalent.  Used by joblist and coronas to get the RP and Cash values from the tunables files.
PROC Get_RP_And_Cash_Values(FLOAT& xpMultiplier, FLOAT& cashMultiplier, FLOAT& apMultiplier, INT contentIdHash, INT missionType, INT missionSubType, BOOL isPushBikeOnly = FALSE, BOOL isAdverMode = FALSE, BOOL bIsGangOps = FALSE, BOOL bIsArenaWar = FALSE, BOOL bMember = FALSE)
	TEXT_LABEL_63 contextString = ""
	PRINTLN("[MMM] Mission Type : ", missionType)
	PRINTLN("[MMM] Mission Subtype : ", missionSubType)
	PRINTLN("[MMM] Pushbike Only : ", isPushBikeOnly)
	PRINTLN("[MMM] DescHash : ", contentIdHash)	
	
//	IF missionType = FMMC_TYPE_MISSION
//	AND missionSubType = FMMC_MISSION_TYPE_FLOW_MISSION
//		PRINTLN("[MMM][JobList] Get_RP_And_Cash_Values No Badge Displayed on Feed - Displaying of badges has been overridden by FMMC_MISSION_TYPE_FLOW_MISSION")
//		EXIT
//	ENDIF
	
	INT contextID = NETWORK_GET_CONTENT_MODIFIER_LIST_ID(contentIdHash)
	PRINTLN("[MMM] Context ID based on hash is : ", contextID)
	
	TEXT_LABEL_63 tunableName = "XP_MULTIPLIER"
	contextString = GET_MISSION_CONTEXT_STRING(missionType, missionSubType, contextID, tunableName, isPushBikeOnly, isAdverMode, bMember)
	
	IF missionType = FMMC_TYPE_MISSION
		IF missionSubType = FMMC_MISSION_TYPE_HEIST
		OR missionSubType = FMMC_MISSION_TYPE_PLANNING
			PRINTLN("[MMM] missionType = FMMC_TYPE_MISSION - missionSubType = FMMC_MISSION_TYPE_HEIST OR FMMC_MISSION_TYPE_PLANNING")
			IF g_sMPTunables.fHeistBadgeXpMultiplier >= 1.0
				xpMultiplier = g_sMPTunables.fHeistBadgeXpMultiplier
				PRINTLN("[MMM] missionType = FMMC_TYPE_MISSION - xpMultiplier = ", g_sMPTunables.fHeistBadgeXpMultiplier)
			ENDIF
			IF g_sMPTunables.fHeistBadgeCashMultiplier >= 1.0
				cashMultiplier = g_sMPTunables.fHeistBadgeCashMultiplier 
				PRINTLN("[MMM] missionType = FMMC_TYPE_MISSION - cashMultiplier = ", g_sMPTunables.fHeistBadgeCashMultiplier)
			ENDIF
			EXIT
		ENDIF
		
		IF bIsGangOps
			PRINTLN("[MMM] missionType = FMMC_TYPE_MISSION - IsGangOps")
			IF g_sMPTunables.fH2_BADGE_XP_MULTIPLIER >= 1.0
				xpMultiplier = g_sMPTunables.fH2_BADGE_XP_MULTIPLIER
				PRINTLN("[MMM] missionType = FMMC_TYPE_MISSION - xpMultiplier = ", xpMultiplier)
			ENDIF
			IF g_sMPTunables.fH2_BADGE_CASH_MULTIPLIER >= 1.0
				cashMultiplier = g_sMPTunables.fH2_BADGE_CASH_MULTIPLIER
				PRINTLN("[MMM] missionType = FMMC_TYPE_MISSION - cashMultiplier = ", cashMultiplier)
			ENDIF
			EXIT
		ENDIF
	ENDIF
	
	// KGM 5/1/15 [BUG 2184413]: Cater for a NULL or EMPTY context string
	IF NOT (IS_STRING_NULL_OR_EMPTY(contextString))
		IF NETWORK_ACCESS_TUNABLE_FLOAT(contextString, tunableName, xpMultiplier)
			PRINTLN("[MMM] XP_MULTIPLIER tunable found in context ", contextString ,", value is : ", xpMultiplier)
		ELSE
			PRINTLN("[MMM] XP_MULTIPLIER tunable doesn't exist in context : ", contextString)	
		ENDIF
		
		FLOAT xpFakeMultiplier
		IF NETWORK_ACCESS_TUNABLE_FLOAT(contextString, "FAKE_XP_MULTIPLIER", xpFakeMultiplier)
			PRINTLN("[MMM] FAKE_XP_MULTIPLIER tunable found in context ", contextString ,", value is : ", xpFakeMultiplier)
		ENDIF
		
		IF xpFakeMultiplier > 0.0
			xpMultiplier = xpFakeMultiplier 
			PRINTLN("[MMM] xpMultiplier = xpFakeMultiplier ")
		ENDIF
		
	ELSE
		PRINTLN("[MMM] XP_MULTIPLIER tunable - but contenxt is NULL (ie: may be for Darts, etc). IGNORE TUNABLE.")	
	ENDIF
	
	tunableName = "CASH_MULTIPLIER"
	contextString = GET_MISSION_CONTEXT_STRING(missionType, missionSubType, contextID, tunableName, isPushBikeOnly, isAdverMode, bMember)
			
	// KGM 5/1/15 [BUG 2184413]: Cater for a NULL or EMPTY context string
	IF NOT (IS_STRING_NULL_OR_EMPTY(contextString))
		IF NETWORK_ACCESS_TUNABLE_FLOAT(contextString, tunableName, cashMultiplier) 
			PRINTLN("[MMM] Cash Multiplier tunable found in context ", contextString ,", value is : ", cashMultiplier)
		ELSE
			PRINTLN("[MMM] Cash Multiplier tunable doesn't exist in context : ", contextString)	
		ENDIF
		
		FLOAT cashFakeMultiplier
		IF NETWORK_ACCESS_TUNABLE_FLOAT(contextString, "FAKE_XP_MULTIPLIER", cashFakeMultiplier) 
			PRINTLN("[MMM] Fake Cash Multiplier tunable found in context ", contextString ,", value is : ", cashFakeMultiplier)
		ENDIF
		
		IF cashFakeMultiplier > 0.0
			cashMultiplier = cashFakeMultiplier 
			PRINTLN("[MMM] cashMultiplier = cashFakeMultiplier ")
		ENDIF
		
	ELSE
		PRINTLN("[MMM] Cash Multiplier tunable - but contenxt is NULL (ie: may be for Darts, etc). IGNORE TUNABLE.")	
	ENDIF
	
	IF bIsArenaWar
		tunableName = "AP_MULTIPLIER"
		contextString = GET_MISSION_CONTEXT_STRING(missionType, missionSubType, contextID, tunableName, isPushBikeOnly, isAdverMode, bMember)
				
		// KGM 5/1/15 [BUG 2184413]: Cater for a NULL or EMPTY context string
		IF NOT (IS_STRING_NULL_OR_EMPTY(contextString))
			IF NETWORK_ACCESS_TUNABLE_FLOAT(contextString, tunableName, apMultiplier) 
				PRINTLN("[MMM] AP Multiplier tunable found in context ", contextString ,", value is : ", apMultiplier)
			ELSE
				PRINTLN("[MMM] AP Multiplier tunable doesn't exist in context : ", contextString)	
			ENDIF
		ELSE
			PRINTLN("[MMM] AP Multiplier tunable - but contenxt is NULL (ie: may be for Darts, etc). IGNORE TUNABLE.")	
		ENDIF
	ENDIF
	
	#IF FEATURE_COPS_N_CROOKS
	IF missionType = FMMC_TYPE_CNC_INVITE
		PRINTLN("[MMM] missionType = FMMC_TYPE_CNC_INVITE")
		IF g_sMPTunables.iCNC_XP_MULTIPLIER >= 1.0
			xpMultiplier = TO_FLOAT(g_sMPTunables.iCNC_XP_MULTIPLIER)
			PRINTLN("[MMM] missionType = FMMC_TYPE_CNC_INVITE - xpMultiplier = ", xpMultiplier)
		ENDIF
		IF g_sMPTunables.iCNC_MODE_CASH_MULTIPLIER >= 1.0
			cashMultiplier = TO_FLOAT(g_sMPTunables.iCNC_MODE_CASH_MULTIPLIER)
			PRINTLN("[MMM] missionType = FMMC_TYPE_CNC_INVITE - cashMultiplier = ", cashMultiplier)
		ENDIF
		EXIT
	
	ENDIF
	#ENDIF
	
ENDPROC


// ===========================================================================================================
//      Recent Joblist Invites Data Access Functions
// ===========================================================================================================

// PURPOSE:	Allows the game to check if there has been a recent Invite
//
// RETURN VALUE:			BOOL					TRUE if there has been a recent invite, otherwise FALSE
//
// NOTES:	'Recent' is based on the RECENT_INVITE_TIME_msec global const
FUNC BOOL Is_There_A_Recent_Invite()
	RETURN (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_inviteRecentlyReceivedTimeout))
ENDFUNC





// ===========================================================================================================
//      Joblist Invites ON HOURS access routines
// ===========================================================================================================

// PURPOSE: Check if the invite should be displayed at this hour of the day
//
// INPUT PARAMS:        paramOnHours        The bitfield of OnHours
// RETURN VALUE:        BOOL                TRUE if the invite can be displayed at this hour, otherwise FALSE
FUNC BOOL Can_Invite_Be_Displayed_At_This_Hour(INT paramOnHours)
    INT timeOfDayHour = GET_CLOCK_HOURS()
    RETURN (IS_BIT_SET(paramOnHours, timeOfDayHour))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the invite was ok to be displayed in the previous hour
//
// INPUT PARAMS:        paramOnHours        The bitfield of OnHours
// RETURN VALUE:        BOOL                TRUE if the invite was ok to display in the previous hour, otherwise FALSE
FUNC BOOL Was_Invite_Ok_To_Display_Last_Hour(INT paramOnHours)

    INT timeOfDayHour = GET_CLOCK_HOURS()
    timeOfDayHour--
    
    IF (timeOfDayHour < 0)
        timeOfDayHour = 23
    ENDIF
    
    RETURN (IS_BIT_SET(paramOnHours, timeOfDayHour))
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the invite will be ok to display in the next hour
//
// INPUT PARAMS:        paramOnHours        The bitfield of OnHours
// RETURN VALUE:        BOOL                TRUE if the invite will be ok to display in the next hour, otherwise FALSE
FUNC BOOL Will_Invite_Be_Ok_To_Display_Next_Hour(INT paramOnHours)

    INT timeOfDayHour = GET_CLOCK_HOURS()
    timeOfDayHour++
    
    IF (timeOfDayHour >= 24)
        timeOfDayHour = 0
    ENDIF
    
    RETURN (IS_BIT_SET(paramOnHours, timeOfDayHour))
    
ENDFUNC



