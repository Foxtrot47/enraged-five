USING "globals.sch"
USING "script_network.sch"
USING "net_include.sch"

CONST_FLOAT NEAR_PLAYER_MAX_DIST2 250000.0

#IF IS_DEBUG_BUILD
PROC CreateNearPlayersWidget()
	INT i
	TEXT_LABEL_63 strName
	
	START_WIDGET_GROUP("Near Players")
		
		ADD_WIDGET_INT_SLIDER("g_iStaggeredParticipant", g_iStaggeredParticipant, -999, NUM_NETWORK_PLAYERS, 1)
		ADD_WIDGET_INT_SLIDER("g_iStaggeredParticipant_LastFrame", g_iStaggeredParticipant_LastFrame, -999, NUM_NETWORK_PLAYERS, 1)
	
		ADD_WIDGET_INT_SLIDER("MAX_NUM_NEAR_PLAYERS", MAX_NUM_NEAR_PLAYERS, 0, NUM_NETWORK_PLAYERS, 1)
		REPEAT NUM_NETWORK_PLAYERS i
			strName = "NearPlayers["
			strName += i
			strName += "]"
			ADD_WIDGET_INT_SLIDER(strName, NearPlayers[i], -1, NUM_NETWORK_PLAYERS, 1)	
		ENDREPEAT						
	STOP_WIDGET_GROUP()
	
ENDPROC
#ENDIF

PROC Init_Near_Players_Array()
	INT i
	REPEAT MAX_NUM_NEAR_PLAYERS i	
		NearPlayers[i] = -1	
	ENDREPEAT
ENDPROC

PROC Update_Near_Players_Array_For_Player(PLAYER_INDEX PlayerID)

	INT i
	FLOAT fMaxDist2 = 0.0
	FLOAT fThisDist2 
	INT iEmptySlot = -1
	INT iMaxDistSlot = -1
	
	INT iPlayer = NATIVE_TO_INT(PlayerID)
	PLAYER_INDEX ThisNearPlayer
	
	VECTOR vLocalPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
	
	// clean the current array and store the max dist
	REPEAT MAX_NUM_NEAR_PLAYERS i		
		IF NOT (NearPlayers[i] = -1)
			
			ThisNearPlayer = INT_TO_NATIVE(PLAYER_INDEX, NearPlayers[i])
		
			// remove this player from the array if he exists in it
			IF (NearPlayers[i] = iPlayer)
				NearPlayers[i] = -1
				iEmptySlot = i
			ELSE
				IF NOT IS_NET_PLAYER_OK(ThisNearPlayer, FALSE, TRUE)
				OR IS_PLAYER_SCTV(ThisNearPlayer)
					NearPlayers[i] = -1
					iEmptySlot = i
				ELSE
					fThisDist2 = VDIST2(GET_PLAYER_COORDS(ThisNearPlayer), vLocalPlayerCoords)
					IF (fThisDist2 > fMaxDist2)
						fMaxDist2 = fThisDist2
						iMaxDistSlot = i
					ENDIF
				ENDIF
			ENDIF
		ELSE
			iEmptySlot = i
		ENDIF	
	ENDREPEAT

	
	// put this player into the array
	IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
		fThisDist2 = VDIST2(GET_PLAYER_COORDS(PlayerID), vLocalPlayerCoords)
		
		IF NOT (iEmptySlot = -1)
			IF (fThisDist2 < NEAR_PLAYER_MAX_DIST2)
				NearPlayers[iEmptySlot] = iPlayer
			ENDIF
		ELSE
			// is this player closer than the max? if so, put him into the array
			IF NOT (iMaxDistSlot = -1)
				IF (fThisDist2 < fMaxDist2)
				AND (fThisDist2 < NEAR_PLAYER_MAX_DIST2)
					NearPlayers[iMaxDistSlot] = iPlayer
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


