// Name:        NET_CASINO_PEDS_DEBUG.sch
// Description: Debug functions and widgets for the Casino peds.

#IF FEATURE_CASINO
USING "net_casino_peds_header.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ DEBUG FUNCTIONS ╞═══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC INITIALISE_CASINO_PED_WIDGETS(CASINO_STRUCT &casinoStruct, SERVER_BROADCAST_DATA &serverBD)
	INITIALISE_CASINO_DEBUG_GLOBALS()
	
	INT i
	INT iIndex
	CASINO_AREA eArea = CASINO_AREA_MAIN_FLOOR
	IF IS_CASINO_PED_BIT_SET(casinoStruct, BS_CASINO_PED_MISSION_LAYOUT_ACTIVE)
		eArea = CASINO_AREA_TABLE_GAMES
	ENDIF
	
	REPEAT g_ciMAX_CASINO_AREAS i
		g_iCasinoPedLayouts[i] = serverBD.iPedLayout[i]
	ENDREPEAT
	
	CASINO_PED_TYPES ePedType
	CASINO_ACTIVITY_SLOTS eActivity
	
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA i
		// Permanent Area
		eDebugPedEditorLayoutOne[i].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 1, CASINO_AREA_PERMANENT))
		eDebugPedEditorLayoutOne[i].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 1, CASINO_AREA_PERMANENT))
		eDebugPedEditorLayoutOne[i].iAssignedArea = ENUM_TO_INT(CASINO_AREA_PERMANENT)
		
		ePedType = INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutOne[i].iPedType)
		eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutOne[i].iActivity)
		
		g_vDebugPedEditorLayoutOnePedCoords[i] = GET_CASINO_PED_POSITION(i, 1, CASINO_AREA_PERMANENT, ePedType, eActivity)
		g_fDebugPedEditorLayoutOnePedHeading[i] = GET_CASINO_PED_HEADING(i, 1, CASINO_AREA_PERMANENT, eActivity)
		eDebugPedEditorLayoutTwo[i].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 2, CASINO_AREA_PERMANENT))
		eDebugPedEditorLayoutTwo[i].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 2, CASINO_AREA_PERMANENT))
		eDebugPedEditorLayoutTwo[i].iAssignedArea = ENUM_TO_INT(CASINO_AREA_PERMANENT)
		
		ePedType = INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutTwo[i].iPedType)
		eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutTwo[i].iActivity)
		
		g_vDebugPedEditorLayoutTwoPedCoords[i] = GET_CASINO_PED_POSITION(i, 2, CASINO_AREA_PERMANENT, ePedType, eActivity)
		g_fDebugPedEditorLayoutTwoPedHeading[i] = GET_CASINO_PED_HEADING(i, 2, CASINO_AREA_PERMANENT, eActivity)
		eDebugPedEditorLayoutThree[i].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 3, CASINO_AREA_PERMANENT))
		eDebugPedEditorLayoutThree[i].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 3, CASINO_AREA_PERMANENT))
		eDebugPedEditorLayoutThree[i].iAssignedArea = ENUM_TO_INT(CASINO_AREA_PERMANENT)
		
		ePedType = INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutThree[i].iPedType)
		eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutThree[i].iActivity)
		
		g_vDebugPedEditorLayoutThreePedCoords[i] = GET_CASINO_PED_POSITION(i, 3, CASINO_AREA_PERMANENT, ePedType, eActivity)
		g_fDebugPedEditorLayoutThreePedHeading[i] = GET_CASINO_PED_HEADING(i, 3, CASINO_AREA_PERMANENT, eActivity)
		// Global data
		g_iCasinoPedType[i] = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, serverBD.iPedLayout[CASINO_AREA_MAIN_FLOOR], CASINO_AREA_PERMANENT))
		g_iCasinoActivitySlot[i] = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, serverBD.iPedLayout[CASINO_AREA_MAIN_FLOOR], CASINO_AREA_PERMANENT))
		
		ePedType = INT_TO_ENUM(CASINO_PED_TYPES, g_iCasinoPedType[i])
		eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iCasinoActivitySlot[i])
		
		g_vCasinoPedPos[i] = GET_CASINO_PED_POSITION(i, serverBD.iPedLayout[CASINO_AREA_MAIN_FLOOR], CASINO_AREA_PERMANENT, ePedType, eActivity)
		g_fCasinoPedHeading[i] = GET_CASINO_PED_HEADING(i, serverBD.iPedLayout[CASINO_AREA_MAIN_FLOOR], CASINO_AREA_PERMANENT, eActivity)
	ENDREPEAT
	
	FOR i = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		// Main Floor Area
		eDebugPedEditorLayoutOne[i].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 1, CASINO_AREA_MAIN_FLOOR))
		eDebugPedEditorLayoutOne[i].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 1, CASINO_AREA_MAIN_FLOOR))
		eDebugPedEditorLayoutOne[i].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MAIN_FLOOR)
		
		ePedType = INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutOne[i].iPedType)
		eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutOne[i].iActivity)
		
		g_vDebugPedEditorLayoutOnePedCoords[i] = GET_CASINO_PED_POSITION(i, 1, CASINO_AREA_MAIN_FLOOR, ePedType, eActivity)
		g_fDebugPedEditorLayoutOnePedHeading[i] = GET_CASINO_PED_HEADING(i, 1, CASINO_AREA_MAIN_FLOOR, eActivity)
		eDebugPedEditorLayoutTwo[i].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 2, CASINO_AREA_MAIN_FLOOR))
		eDebugPedEditorLayoutTwo[i].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 2, CASINO_AREA_MAIN_FLOOR))
		eDebugPedEditorLayoutTwo[i].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MAIN_FLOOR)
		
		ePedType = INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutTwo[i].iPedType)
		eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutTwo[i].iActivity)
		
		g_vDebugPedEditorLayoutTwoPedCoords[i] = GET_CASINO_PED_POSITION(i, 2, CASINO_AREA_MAIN_FLOOR, ePedType, eActivity)
		g_fDebugPedEditorLayoutTwoPedHeading[i] = GET_CASINO_PED_HEADING(i, 2, CASINO_AREA_MAIN_FLOOR, eActivity)
		eDebugPedEditorLayoutThree[i].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 3, CASINO_AREA_MAIN_FLOOR))
		eDebugPedEditorLayoutThree[i].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 3, CASINO_AREA_MAIN_FLOOR))
		eDebugPedEditorLayoutThree[i].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MAIN_FLOOR)
		
		ePedType = INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutThree[i].iPedType)
		eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutThree[i].iActivity)
		
		g_vDebugPedEditorLayoutThreePedCoords[i] = GET_CASINO_PED_POSITION(i, 3, CASINO_AREA_MAIN_FLOOR, ePedType, eActivity)
		g_fDebugPedEditorLayoutThreePedHeading[i] = GET_CASINO_PED_HEADING(i, 3, CASINO_AREA_MAIN_FLOOR, eActivity)
		// Table Games Area
		eDebugPedEditorLayoutOne[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 1, CASINO_AREA_TABLE_GAMES))
		eDebugPedEditorLayoutOne[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 1, CASINO_AREA_TABLE_GAMES))
		eDebugPedEditorLayoutOne[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iAssignedArea = ENUM_TO_INT(CASINO_AREA_TABLE_GAMES)
		
		ePedType = INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutOne[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iPedType)
		eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutOne[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iActivity)
		
		g_vDebugPedEditorLayoutOnePedCoords[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = GET_CASINO_PED_POSITION(i, 1, CASINO_AREA_TABLE_GAMES, ePedType, eActivity)
		g_fDebugPedEditorLayoutOnePedHeading[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = GET_CASINO_PED_HEADING(i, 1, CASINO_AREA_TABLE_GAMES, eActivity)
		eDebugPedEditorLayoutTwo[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 2, CASINO_AREA_TABLE_GAMES))
		eDebugPedEditorLayoutTwo[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 2, CASINO_AREA_TABLE_GAMES))
		eDebugPedEditorLayoutTwo[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iAssignedArea = ENUM_TO_INT(CASINO_AREA_TABLE_GAMES)
		
		ePedType = INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutTwo[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iPedType)
		eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutTwo[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iActivity)
		
		g_vDebugPedEditorLayoutTwoPedCoords[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = GET_CASINO_PED_POSITION(i, 2, CASINO_AREA_TABLE_GAMES, ePedType, eActivity)
		g_fDebugPedEditorLayoutTwoPedHeading[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = GET_CASINO_PED_HEADING(i, 2, CASINO_AREA_TABLE_GAMES, eActivity)
		eDebugPedEditorLayoutThree[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 3, CASINO_AREA_TABLE_GAMES))
		eDebugPedEditorLayoutThree[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 3, CASINO_AREA_TABLE_GAMES))
		eDebugPedEditorLayoutThree[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iAssignedArea = ENUM_TO_INT(CASINO_AREA_TABLE_GAMES)
		
		ePedType = INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutThree[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iPedType)
		eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutThree[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iActivity)
		
		g_vDebugPedEditorLayoutThreePedCoords[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = GET_CASINO_PED_POSITION(i, 3, CASINO_AREA_TABLE_GAMES, ePedType, eActivity)
		g_fDebugPedEditorLayoutThreePedHeading[(i+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = GET_CASINO_PED_HEADING(i, 3, CASINO_AREA_TABLE_GAMES, eActivity)
		// Sports Betting Area
		IF (i < (MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA+MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA))
			iIndex = (i+(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2))
			eDebugPedEditorLayoutOne[iIndex].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 1, CASINO_AREA_SPORTS_BETTING))
			eDebugPedEditorLayoutOne[iIndex].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 1, CASINO_AREA_SPORTS_BETTING))
			eDebugPedEditorLayoutOne[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_SPORTS_BETTING)
			
			ePedType = INT_TO_ENUM(CASINO_PED_TYPES,eDebugPedEditorLayoutOne[iIndex].iPedType)
			eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutOne[iIndex].iActivity)
			
			g_vDebugPedEditorLayoutOnePedCoords[iIndex] = GET_CASINO_PED_POSITION(i, 1, CASINO_AREA_SPORTS_BETTING, ePedType, eActivity)
			g_fDebugPedEditorLayoutOnePedHeading[iIndex] = GET_CASINO_PED_HEADING(i, 1, CASINO_AREA_SPORTS_BETTING, eActivity)
			eDebugPedEditorLayoutTwo[iIndex].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 2, CASINO_AREA_SPORTS_BETTING))
			eDebugPedEditorLayoutTwo[iIndex].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 2, CASINO_AREA_SPORTS_BETTING))
			eDebugPedEditorLayoutTwo[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_SPORTS_BETTING)
			
			ePedType = INT_TO_ENUM(CASINO_PED_TYPES,eDebugPedEditorLayoutTwo[iIndex].iPedType)
			eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutTwo[iIndex].iActivity)
			
			g_vDebugPedEditorLayoutTwoPedCoords[iIndex] = GET_CASINO_PED_POSITION(i, 2, CASINO_AREA_SPORTS_BETTING, ePedType, eActivity)
			g_fDebugPedEditorLayoutTwoPedHeading[iIndex] = GET_CASINO_PED_HEADING(i, 2, CASINO_AREA_SPORTS_BETTING, eActivity)
			eDebugPedEditorLayoutThree[iIndex].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 3, CASINO_AREA_SPORTS_BETTING))
			eDebugPedEditorLayoutThree[iIndex].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 3, CASINO_AREA_SPORTS_BETTING))
			eDebugPedEditorLayoutThree[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_SPORTS_BETTING)
			
			ePedType = INT_TO_ENUM(CASINO_PED_TYPES,eDebugPedEditorLayoutThree[iIndex].iPedType)
			eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutThree[iIndex].iActivity)
			
			g_vDebugPedEditorLayoutThreePedCoords[iIndex] = GET_CASINO_PED_POSITION(i, 3, CASINO_AREA_SPORTS_BETTING, ePedType, eActivity)
			g_fDebugPedEditorLayoutThreePedHeading[iIndex] = GET_CASINO_PED_HEADING(i, 3, CASINO_AREA_SPORTS_BETTING, eActivity)
		ENDIF
		// Managers Office Area
		IF (i = 42 OR i = 43)
			iIndex = (i+(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)+MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-(MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE+1))
			eDebugPedEditorLayoutOne[iIndex].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 1, CASINO_AREA_MANAGERS_OFFICE))
			eDebugPedEditorLayoutOne[iIndex].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 1, CASINO_AREA_MANAGERS_OFFICE))
			eDebugPedEditorLayoutOne[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MANAGERS_OFFICE)
			
			ePedType = INT_TO_ENUM(CASINO_PED_TYPES,eDebugPedEditorLayoutOne[iIndex].iPedType)
			eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutOne[iIndex].iActivity)
			
			g_vDebugPedEditorLayoutOnePedCoords[iIndex] = GET_CASINO_PED_POSITION(i, 1, CASINO_AREA_MANAGERS_OFFICE, ePedType, eActivity)
			g_fDebugPedEditorLayoutOnePedHeading[iIndex] = GET_CASINO_PED_HEADING(i, 1, CASINO_AREA_MANAGERS_OFFICE, eActivity)
			eDebugPedEditorLayoutTwo[iIndex].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 2, CASINO_AREA_MANAGERS_OFFICE))
			eDebugPedEditorLayoutTwo[iIndex].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 2, CASINO_AREA_MANAGERS_OFFICE))
			eDebugPedEditorLayoutTwo[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MANAGERS_OFFICE)
			
			ePedType = INT_TO_ENUM(CASINO_PED_TYPES,eDebugPedEditorLayoutTwo[iIndex].iPedType)
			eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutTwo[iIndex].iActivity)
			
			g_vDebugPedEditorLayoutTwoPedCoords[iIndex] = GET_CASINO_PED_POSITION(i, 2, CASINO_AREA_MANAGERS_OFFICE, ePedType, eActivity)
			g_fDebugPedEditorLayoutTwoPedHeading[iIndex] = GET_CASINO_PED_HEADING(i, 2, CASINO_AREA_MANAGERS_OFFICE, eActivity)
			eDebugPedEditorLayoutThree[iIndex].iActivity = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, 3, CASINO_AREA_MANAGERS_OFFICE))
			eDebugPedEditorLayoutThree[iIndex].iPedType = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, 3, CASINO_AREA_MANAGERS_OFFICE))
			eDebugPedEditorLayoutThree[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MANAGERS_OFFICE)
			
			ePedType = INT_TO_ENUM(CASINO_PED_TYPES,eDebugPedEditorLayoutThree[iIndex].iPedType)
			eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutThree[iIndex].iActivity)
			
			g_vDebugPedEditorLayoutThreePedCoords[iIndex] = GET_CASINO_PED_POSITION(i, 3, CASINO_AREA_MANAGERS_OFFICE, ePedType, eActivity)
			g_fDebugPedEditorLayoutThreePedHeading[iIndex] = GET_CASINO_PED_HEADING(i, 3, CASINO_AREA_MANAGERS_OFFICE, eActivity)
		ENDIF
		// Global data
		g_iCasinoPedType[i] = ENUM_TO_INT(GET_CASINO_PED_TYPE(i, serverBD.iPedLayout[CASINO_AREA_MAIN_FLOOR], eArea))
		g_iCasinoActivitySlot[i] = ENUM_TO_INT(GET_CASINO_PED_ACTIVITY_SLOT(i, serverBD.iPedLayout[CASINO_AREA_MAIN_FLOOR], eArea))
		
		ePedType = INT_TO_ENUM(CASINO_PED_TYPES,g_iCasinoPedType[i])
		eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iCasinoPedType[i])
		
		g_vCasinoPedPos[i] = GET_CASINO_PED_POSITION(i, serverBD.iPedLayout[CASINO_AREA_MAIN_FLOOR], eArea, ePedType, eActivity)
		g_fCasinoPedHeading[i] = GET_CASINO_PED_HEADING(i, serverBD.iPedLayout[CASINO_AREA_MAIN_FLOOR], eArea, eActivity)
	ENDFOR
	
	PRINTLN("[CASINO_PEDS] INITIALISE_CASINO_PED_WIDGETS called")
ENDPROC

FUNC THREADID GET_THREAD_OF_AM_CASINO_SCRIPT()
	SCRIPT_THREAD_ITERATOR_RESET() 
	THREADID thID = SCRIPT_THREAD_ITERATOR_GET_NEXT_THREAD_ID()
	WHILE (thID != NULL)
		
		IF (GET_HASH_KEY(GET_NAME_OF_SCRIPT_WITH_THIS_ID(thID)) = HASH("am_mp_casino"))
			RETURN thID
		ENDIF
		
		thID = SCRIPT_THREAD_ITERATOR_GET_NEXT_THREAD_ID()
	ENDWHILE
	RETURN thID
ENDFUNC

PROC OUTPUT_CREATED_PED_DETAILS()
	INT iResPeds
	INT iResVehicles
	INT iResObjects
	INT iCreatedPeds
	INT iCreatedVehicles
	INT iCreatedObjects
	
	INT iTotalReserved = GET_NUM_RESERVED_MISSION_PEDS(TRUE, RESERVATION_ALL)
	PRINTLN("[CASINO_PEDS] OUTPUT_CREATED_PED_DETAILS - RESERVATION_ALL iTotalReserved = ", iTotalReserved)
	
	iTotalReserved = GET_NUM_RESERVED_MISSION_PEDS(TRUE, RESERVATION_LOCAL_ONLY)
	PRINTLN("[CASINO_PEDS] OUTPUT_CREATED_PED_DETAILS - RESERVATION_LOCAL_ONLY iTotalReserved = ", iTotalReserved)
	
	iTotalReserved = GET_NUM_RESERVED_MISSION_PEDS(TRUE, RESERVATION_GLOBAL_ONLY)
	PRINTLN("[CASINO_PEDS] OUTPUT_CREATED_PED_DETAILS - RESERVATION_GLOBAL_ONLY iTotalReserved = ", iTotalReserved)
	
	GET_RESERVED_MISSION_ENTITIES_FOR_THREAD(GET_ID_OF_THIS_THREAD(), iResPeds, iResVehicles, iResObjects, iCreatedPeds, iCreatedVehicles, iCreatedObjects)
	PRINTLN("[CASINO_PEDS] OUTPUT_CREATED_PED_DETAILS - this thread iResPeds = ", iResPeds, 
													" iResVehicles = ", iResVehicles, 
													" iResObjects = ", iResObjects,
													" iCreatedPeds = ", iCreatedPeds,
													" iCreatedVehicles = ", iCreatedVehicles,
													" iCreatedObjects = ", iCreatedObjects)
	
	THREADID casinothreadid = GET_THREAD_OF_AM_CASINO_SCRIPT()
	IF (casinothreadid != NULL)
		GET_RESERVED_MISSION_ENTITIES_FOR_THREAD(GET_ID_OF_THIS_THREAD(), iResPeds, iResVehicles, iResObjects, iCreatedPeds, iCreatedVehicles, iCreatedObjects)	
		PRINTLN("[CASINO_PEDS] OUTPUT_CREATED_PED_DETAILS - casino thread iResPeds = ", iResPeds, 
												" iResVehicles = ", iResVehicles, 
												" iResObjects = ", iResObjects,
												" iCreatedPeds = ", iCreatedPeds,
												" iCreatedVehicles = ", iCreatedVehicles,
												" iCreatedObjects = ", iCreatedObjects)	
	ENDIF
	
ENDPROC

PROC SHOW_PED_IDS(SERVER_BROADCAST_DATA serverBD, CASINO_STRUCT &casinoStruct, INT iCasinoPedHighlight)
	
	INT i
	INT j
	INT iRow
	INT iRed
	INT iGreen
	INT iBlue
	INT iAlpha = 255
	FLOAT fRowZ = 0.06
	TEXT_LABEL_63 str
	
	VECTOR vCamCoords = GET_FINAL_RENDERED_CAM_COORD()
	
	IF NOT (g_SpawnData.bDebugLinesActive)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
		g_SpawnData.bDebugLinesActive = TRUE
	ENDIF
	
	BOOL bDoRender
	BOOL bDoHighlight
	HUD_COLOURS HudColour
	VECTOR vDrawPos
	INT iRemain
	
	VECTOR vPedPos
	VECTOR vActualPedPos
	FLOAT fPedHeading
	
	REPEAT MAX_NUMBER_OF_CASINO_PEDS i
		IF DOES_ENTITY_EXIST(casinoStruct.activityPeds[i].PedID)
			
			bDoRender = FALSE				
			bDoHighlight = FALSE		
			iRow = 0
			
			vPedPos = GET_CASINO_PED_POSITION(i, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[i].eAssignedArea)], casinoStruct.activityPeds[i].eAssignedArea, INT_TO_ENUM(CASINO_PED_TYPES, casinoStruct.activityPeds[i].iCasinoPedType), casinoStruct.activityPeds[i].eActiveActivity)
			vActualPedPos = GET_ENTITY_COORDS(casinoStruct.activityPeds[i].PedID, FALSE)
			fPedHeading = GET_CASINO_PED_HEADING(i, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[i].eAssignedArea)], casinoStruct.activityPeds[i].eAssignedArea, casinoStruct.activityPeds[i].eActiveActivity)
			
			bDoRender = TRUE
			
			IF IS_ENTITY_ON_SCREEN(casinoStruct.activityPeds[i].PedID)
			AND (VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vPedPos) <= 8.0 OR VDIST(vCamCoords, vPedPos) <= 8.0)
				
				IF (i = iCasinoPedHighlight)
					bDoHighlight = TRUE
				ENDIF
				
				IF (bDoRender)
				
					str = "activityPeds["
					str += GET_STRING_FROM_INT(i)
					str += "]: CasinoPedID "
					str += GET_STRING_FROM_INT(i)
					
					IF (bDoHighlight)
						HudColour = HUD_COLOUR_ORANGE
					ELSE
						HudColour = HUD_COLOUR_PURE_WHITE
					ENDIF
					GET_HUD_COLOUR(HudColour, iRed, iGreen, iBlue, iAlpha)
					
					vDrawPos = vActualPedPos
					
					DRAW_DEBUG_SPAWN_POINT(vPedPos,fPedHeading, HudColour, 0, 0.5)
					DRAW_DEBUG_TEXT(str, vDrawPos, iRed, iGreen, iBlue, iAlpha)
					
					IF (VMAG(vDrawPos) <= 0.0)
						vDrawPos = GET_ENTITY_COORDS(casinoStruct.activityPeds[i].PedID, FALSE)
						DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
					ENDIF					
					
					//IF VDIST2(vActualPedPos, vPedPos) > 10
						str = ""iRow++					
						str += "PED POS: "
						str += FLOAT_TO_STRING(vActualPedPos.x, 1)
						str += ", "
						str += FLOAT_TO_STRING(vActualPedPos.y, 1)
						str += ", "
						str += FLOAT_TO_STRING(vActualPedPos.z, 1)
						str += ", "
						DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
					//ENDIF
					
					str = ""
					
					IF INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iCasinoActivitySlot[i]) != CASINO_AC_SLOT_NULL
						iRow++
						str += "Act: "
						str += GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iCasinoActivitySlot[i]))
						DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
						
						iRow++
						str = "iCurrentSequence: "
						str += casinoStruct.activityPeds[i].iCurrentSequence
						IF IS_CASINO_SPECIAL_PED(i)
						AND IS_CASINO_SPECIAL_PED_ACTIVITY(casinoStruct.activityPeds[i].eActiveActivity)
							IF (GET_CASINO_SPECIAL_PED_NAME(INT_TO_ENUM(CASINO_PED_TYPES, g_iCasinoPedType[i])) < MAX_CASINO_SPECIAL_PED_NAME)
								str += " iConversation: "
								str += g_sSpecialPeds[GET_CASINO_SPECIAL_PED_NAME(INT_TO_ENUM(CASINO_PED_TYPES, g_iCasinoPedType[i]))].iConversation
							ENDIF
						ENDIF
						DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
					ENDIF
					
					// If not started then display as red
					IF NOT casinoStruct.activityPeds[i].bHasPedAnimStarted
						GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
					ENDIF
					
					str = ""
					iRow++
					SET_TEXT_SCALE(0.25, 0.25)
					GET_HUD_COLOUR(HudColour, iRed, iGreen, iBlue, iAlpha)
					SET_TEXT_COLOUR(iRed, iGreen, iBlue, iAlpha)
					str = "AnimClip: "
					str += casinoStruct.activityPeds[i].tlDebugAnimClipName
					DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)

					str = ""
					iRow++
					SET_TEXT_SCALE(0.25, 0.25)
					GET_HUD_COLOUR(HudColour, iRed, iGreen, iBlue, iAlpha)
					SET_TEXT_COLOUR(iRed, iGreen, iBlue, iAlpha)
					str += "fPhaseRate: "
					str += FLOAT_TO_STRING(casinoStruct.activityPeds[i].fPhaseRate)
					str += " iAnimDuration: "
					str += casinoStruct.activityPeds[i].iAnimDuration
					DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
					
					// Anim info
					iRow++
					str = "dur:"
					str += casinoStruct.activityPeds[i].iAnimDuration
					str += " remain:"
					iRemain = ANIM_TIME_REMAINING(casinoStruct.activityPeds[i])
					str += iRemain
					str += " frames:"
					iRemain = EXPECTED_FRAMES_UNTIL_END_OF_ANIM(casinoStruct.activityPeds[i])
					str += iRemain
					DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
					
					// Headtrack
					iRow++
					str = "headtrack: "
					
					IF casinoStruct.activityPeds[i].iPedToHeadTrack != -1
						str += "ped"
					ELIF NOT IS_VECTOR_ZERO(casinoStruct.activityPeds[i].vCoordToHeadTrack)
						str += "position"
					ELSE
						str += "none"
					ENDIF
					
					DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)

					// Casino ped type
					iRow++
					str = "casino ped type: "
					str += GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, g_iCasinoPedType[i]))
					DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)	
					
					// Casino ped ambient speech
					IF SHOULD_CASINO_PED_PLAY_SPEECH(i, casinoStruct, INT_TO_ENUM(CASINO_PED_TYPES, g_iCasinoPedType[i]))
						
						j = 0
						REPEAT CASINO_SPEECH_MAX j
							CASINO_PED_SPEECH_ENUM eCasinoPedSpeech = INT_TO_ENUM(CASINO_PED_SPEECH_ENUM, j)
							
							IF SHOULD_CASINO_PED_PLAY_SPEECH(i, casinoStruct, INT_TO_ENUM(CASINO_PED_TYPES, g_iCasinoPedType[i]), eCasinoPedSpeech)
								iRow++
								str = "speech:"
								str += GET_CASINO_PED_SPEECH_NAME(eCasinoPedSpeech)
								DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)	
							ENDIF
						ENDREPEAT
						
					ENDIF
				ENDIF		
				
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_DISPLAY_PED_ROULETTE_DEBUG(CASINO_STRUCT &casinoStruct)
	IF casinoStruct.ePlayerArea != CASINO_AREA_TABLE_GAMES
		RETURN FALSE
	ENDIF
	
	IF IS_CASINO_PED_BIT_SET(casinoStruct, BS_CASINO_PED_CULLING_PED_THIS_FRAME)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SHOW_PED_ROULETTE_INFO(SERVER_BROADCAST_DATA serverBD, CASINO_STRUCT &casinoStruct, INT iCasinoPedHighlight)
	
	IF NOT SHOULD_DISPLAY_PED_ROULETTE_DEBUG(casinoStruct)
		EXIT
	ENDIF
	
	INT iPedID
	INT iSeatID
	INT iRow
	
	INT iBetCounter = 0
	
	INT iRed
	INT iGreen
	INT iBlue
	INT iAlpha = 255
	
	FLOAT fRowZ = 0.06
	
	BOOL bHighStakes
	
	VECTOR vPedCoords
	VECTOR vCamCoords = GET_FINAL_RENDERED_CAM_COORD()
	
	TEXT_LABEL_63 tlLabel
	
	HUD_COLOURS eHudColour
	
	REPEAT MAX_CASINO_SEATS_PER_TABLE_GAME iSeatID
		iPedID = GET_ROULETTE_PED_ID(iSeatID)
		IF DOES_ENTITY_EXIST(casinoStruct.activityPeds[iPedID].PedID)
			
			iRow 		= 0
			vPedCoords 	= GET_ENTITY_COORDS(casinoStruct.activityPeds[iPedID].PedID, FALSE)
			
			IF IS_ENTITY_ON_SCREEN(casinoStruct.activityPeds[iPedID].PedID)
			AND (VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vPedCoords) <= 8.0 OR VDIST(vCamCoords, vPedCoords) <= 8.0)
				
				bHighStakes	= FALSE
				IF g_iPedReservedRouletteTable > 1
					bHighStakes = TRUE
				ENDIF
				
				eHudColour = HUD_COLOUR_PURE_WHITE
				IF (iPedID = iCasinoPedHighlight)
					eHudColour = HUD_COLOUR_ORANGE
				ENDIF
				
				iBetCounter = GET_ROULETTE_PED_BET_COUNTER(iPedID, casinoStruct)
				GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
				
				tlLabel = "Roulette Preset: "
				tlLabel += GET_STRING_FROM_INT(casinoStruct.iRoulettePreset)
				DRAW_DEBUG_TEXT(tlLabel, <<vPedCoords.x, vPedCoords.y, vPedCoords.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
				
				iRow++
				tlLabel = "Roulette State: "
				tlLabel += GET_STRING_FROM_INT(casinoStruct.iRouletteAnimFlow)
				DRAW_DEBUG_TEXT(tlLabel, <<vPedCoords.x, vPedCoords.y, vPedCoords.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
				
				
				iRow += 2
				SET_TEXT_SCALE(0.25, 0.25)
				GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
				SET_TEXT_COLOUR(iRed, iGreen, iBlue, iAlpha)
				tlLabel = "Anim Clip: "
				tlLabel += casinoStruct.activityPeds[iPedID].tlDebugAnimClipName
				DRAW_DEBUG_TEXT(tlLabel, <<vPedCoords.x, vPedCoords.y, vPedCoords.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
				
				iRow++
				tlLabel = "Bet Counter: "
				tlLabel += GET_STRING_FROM_INT(iBetCounter)
				DRAW_DEBUG_TEXT(tlLabel, <<vPedCoords.x, vPedCoords.y, vPedCoords.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
				
				iRow++
				tlLabel = "Bet Amount: "
				tlLabel += GET_STRING_FROM_INT(GET_PED_ROULETTE_BET_VALUE(iPedID, iBetCounter, casinoStruct.iRoulettePreset, bHighStakes))
				DRAW_DEBUG_TEXT(tlLabel, <<vPedCoords.x, vPedCoords.y, vPedCoords.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
				
				iRow++
				tlLabel = "Bet Placement: "
				IF casinoStruct.iCasinoPedToHighlight > -1
					tlLabel += GET_STRING_FROM_INT(GET_ROULETTE_BET_PLACEMENT(iPedID, casinoStruct.activityPeds[iPedID].iCurrentSequence, IS_CASINO_PED_FEMALE(iPedID, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].eAssignedArea)], casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].eAssignedArea), casinoStruct.activityPeds[iPedID].eActiveActivity))
					DRAW_DEBUG_TEXT(tlLabel, <<vPedCoords.x, vPedCoords.y, vPedCoords.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
				ENDIF
				
				iRow += 2
				tlLabel = "Seat ID: "
				tlLabel += GET_STRING_FROM_INT(iSeatID)
				DRAW_DEBUG_TEXT(tlLabel, <<vPedCoords.x, vPedCoords.y, vPedCoords.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
				
				iRow++
				tlLabel = "Ped ID: "
				tlLabel += GET_STRING_FROM_INT(iPedID)
				DRAW_DEBUG_TEXT(tlLabel, <<vPedCoords.x, vPedCoords.y, vPedCoords.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
				
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC SHOW_PED_SPEECH(SERVER_BROADCAST_DATA serverBD, CASINO_STRUCT &casinoStruct)
	TEXT_LABEL_63 str = ""
	INT iOffset_x = 0, iOffset_y = 0
	INT i, iRed, iGreen, iBlue, iAlpha
	HUD_COLOURS eHudColour = HUD_COLOUR_YELLOW
	
	IF NOT (g_SpawnData.bDebugLinesActive)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
		g_SpawnData.bDebugLinesActive = TRUE
	ENDIF
	
	REPEAT MAX_NUMBER_OF_CASINO_PEDS i
		iOffset_x = 0
		iOffset_y = 0
		
		CASINO_PED_TYPES ePeds = GET_CASINO_PED_TYPE(i, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[i].eAssignedArea)], casinoStruct.activityPeds[i].eAssignedArea)
		CASINO_ACTIVITY_SLOTS eActivity = GET_CASINO_PED_ACTIVITY_SLOT(i, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[i].eAssignedArea)], casinoStruct.activityPeds[i].eAssignedArea)
		IF NOT SHOULD_CASINO_PED_PLAY_SPEECH(i, casinoStruct, ePeds)
			//
		ELIF NOT DOES_ENTITY_EXIST(casinoStruct.activityPeds[i].PedID)
		OR IS_ENTITY_DEAD(casinoStruct.activityPeds[i].PedID)
			//
		ELSE
			VECTOR vScenePedCoord = GET_ENTITY_COORDS(casinoStruct.activityPeds[i].PedID, FALSE)
			
			INT iSpeechTimerDiffMS = -1
			IF HAS_NET_TIMER_STARTED(casinoStruct.activityPeds[i].stIdleSpeechTimer)
				iSpeechTimerDiffMS = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(casinoStruct.activityPeds[i].stIdleSpeechTimer)
			ENDIF
		
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, iRed, iGreen, iBlue, iAlpha)
			
			str  = "  ["
			str += i
			str += "] "
			str += GET_CASINO_PED_TYPE_STRING(ePeds)
			str += " timer: "
			IF (iSpeechTimerDiffMS = -1)
				str += "NULL"
			ELSE
				str += GET_STRING_FROM_FLOAT(TO_FLOAT(iSpeechTimerDiffMS)/1000.0)
			ENDIF
			GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
			DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)	iOffset_y++
			
			
			IF (ePeds = CASINO_BETH)
			OR (ePeds = CASINO_BLANE)
			OR (ePeds = CASINO_CALEB)
			OR (ePeds = CASINO_CURTIS)
			OR (ePeds = CASINO_DEAN)
			OR (ePeds = CASINO_CAROL)
			OR (ePeds = CASINO_EILEEN)
			OR (ePeds = CASINO_GABRIEL)
			OR (ePeds = CASINO_LAUREN)
			OR (ePeds = CASINO_TAYLOR)
			OR (ePeds = CASINO_USHI)
			OR (ePeds = CASINO_VINCE)
				str  = "  ["
				str += i
				str += "] "
				str += GET_CASINO_PED_ACTIVITY_STRING(eActivity)
				GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
				DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)	iOffset_y++
			ENDIF
			
			IF NOT (casinoStruct.activityPeds[i].eLastSpeech[0] = CASINO_SPEECH_INVALID)
			OR NOT (casinoStruct.activityPeds[i].eLastSpeech[1] = CASINO_SPEECH_INVALID)
				str  = "  "
				IF NOT (casinoStruct.activityPeds[i].eLastSpeech[0] = CASINO_SPEECH_INVALID)
					str += "eLastSpeech[0] "
					str += GET_CASINO_PED_SPEECH_NAME(casinoStruct.activityPeds[i].eLastSpeech[0])
				ENDIF
				IF NOT (casinoStruct.activityPeds[i].eLastSpeech[1] = CASINO_SPEECH_INVALID)
					IF NOT (casinoStruct.activityPeds[i].eLastSpeech[0] = CASINO_SPEECH_INVALID)
						str += ", [1] "
					ELSE
						str += "eLastSpeech[1] "
					ENDIF
					str += GET_CASINO_PED_SPEECH_NAME(casinoStruct.activityPeds[i].eLastSpeech[1])
				ENDIF
				eHudColour = HUD_COLOUR_YELLOW
				GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
				DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)	
				iOffset_y++
			ENDIF
			
			//
//			TEXT_LABEL_63 tl63Reason = ""
//			IF IS_CASINO_PEDS_SPEECH_BLOCKED_FOR_ACTIVITY(i, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[i].eAssignedArea)], casinoStruct.activityPeds[i], tl63Reason, casinoStruct.activityPeds[i].eAssignedArea)
//				str  = tl63Reason
//				eHudColour = HUD_COLOUR_RED
//				GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
//				DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)	iOffset_y++
//			ELIF NOT IS_STRING_NULL_OR_EMPTY(tl63Reason)
//				str  = tl63Reason
//				eHudColour = HUD_COLOUR_RED
//				GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
//				iAlpha = ROUND(TO_FLOAT(iAlpha)*0.5)
//				DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)	iOffset_y++
//			ENDIF
			
			CASINO_PED_SPEECH_ENUM eCasinoPedSpeech
			REPEAT CASINO_SPEECH_MAX eCasinoPedSpeech
				IF (eCasinoPedSpeech != CASINO_SPEECH_INVALID)
				AND SHOULD_CASINO_PED_PLAY_SPEECH(i, casinoStruct, ePeds, eCasinoPedSpeech)
				
					str = GET_CASINO_PED_SPEECH_NAME(eCasinoPedSpeech)
					eHudColour = HUD_COLOUR_YELLOW
					GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
					iOffset_x = 0
					DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
					iOffset_x += 15
			
//					str = "t:"
//					IF GET_CASINO_PED_SPEECH_TIMEDELAY(eCasinoPedSpeech) = HIGHEST_INT
//						str+= "ERROR"
//					ELIF GET_CASINO_PED_SPEECH_TIMEDELAY(eCasinoPedSpeech) = 0
//						str+= "ZERO"
//					ELSE
//						str+= ROUND(TO_FLOAT(GET_CASINO_PED_SPEECH_TIMEDELAY(eCasinoPedSpeech))/1000.0)
//						str+= "s"
//					ENDIF
//					eHudColour = HUD_COLOUR_REDLIGHT
//					GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
//					DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
//					iOffset_x += 5
					
					IF IS_BIT_SET_CASINO_SPEECH(casinoStruct.activityPeds[i], eCasinoPedSpeech)
						str = "SET"
						eHudColour = HUD_COLOUR_WHITE
						GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
						DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
					ELSE
						str = "unset"
						eHudColour = HUD_COLOUR_GREYDARK
						GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
						iAlpha = ROUND(TO_FLOAT(iAlpha)*0.5)
						DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
					ENDIF
					iOffset_x += 5
					
					IF IS_BIT_SET(g_iBSCasinoSpeech[i], ENUM_TO_INT(eCasinoPedSpeech))
						str = "BS_SET"
						eHudColour = HUD_COLOUR_WHITE
						GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
						DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
					ELSE
						str = "BS_unset"
						eHudColour = HUD_COLOUR_GREYDARK
						GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
						iAlpha = ROUND(TO_FLOAT(iAlpha)*0.5)
						DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
					ENDIF
					
					str  = "Context:\""
					str += g_tl63BSCasinoSpeechContext[i]
					str += "\""
					IF IS_STRING_NULL_OR_EMPTY(g_tl63BSCasinoSpeechContext[i])
						eHudColour = HUD_COLOUR_GREYDARK
						GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
						iAlpha = ROUND(TO_FLOAT(iAlpha)*0.5)
					ELSE
						eHudColour = HUD_COLOUR_WHITE
						GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
					ENDIF
					DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*20, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
					
//					IF (eCasinoPedSpeech = CASINO_SPEECH_GREET_FIRST)
//						iOffset_y++
//						IF IS_BIT_SET_CASINO_SPEECH(casinoStruct.activityPeds[i], CASINO_SPEECHCHECK_PLAYED_GREET_FIRST)
//							str = "  GREET_FIRST played"
//							eHudColour = HUD_COLOUR_YELLOW
//							GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
//							DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, 0*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
//						ELSE
//							str = "  GREET_FIRST not played"
//							eHudColour = HUD_COLOUR_YELLOW
//							GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
//							iAlpha = ROUND(TO_FLOAT(iAlpha)*0.5)
//							DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, 0*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
//						ENDIF
//					ELIF (eCasinoPedSpeech = CASINO_SPEECH_GREET)
//						iOffset_y++
//						IF IS_BIT_SET_CASINO_SPEECH(casinoStruct.activityPeds[i], CASINO_SPEECHCHECK_PLAYED_GREET)
//							str = "  GREET played"
//							eHudColour = HUD_COLOUR_YELLOW
//							GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
//							DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, 0*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
//						ELSE
//							str = "  GREET not played"
//							eHudColour = HUD_COLOUR_YELLOW
//							GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
//							iAlpha = ROUND(TO_FLOAT(iAlpha)*0.5)
//							DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, 0*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
//						ENDIF
//					ENDIF
					
					iOffset_y++
				ENDIF
			ENDREPEAT
			
			// Greet / Bye / Howsitgoing
			IF SHOULD_CASINO_PED_PLAY_SPEECH(i, casinoStruct, ePeds, CASINO_SPEECH_GREET_FIRST)
			OR SHOULD_CASINO_PED_PLAY_SPEECH(i, casinoStruct, ePeds, CASINO_SPEECH_GREET)
			OR SHOULD_CASINO_PED_PLAY_SPEECH(i, casinoStruct, ePeds, CASINO_SPEECH_BYE)
			OR SHOULD_CASINO_PED_PLAY_SPEECH(i, casinoStruct, ePeds, CASINO_SPEECH_CUSTOM)
				BOOL bIsPlayerInGreetArea = FALSE
				IF (ePeds = CASINO_MANAGER)
				ELSE
					VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
					VECTOR vScenePedPos = GET_ENTITY_COORDS(casinoStruct.activityPeds[i].PedID, FALSE)
					bIsPlayerInGreetArea = (GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vScenePedPos) < CASINO_PED_DEFAULT_GREET_AREA(ePeds))
					
					eHudColour = HUD_COLOUR_BLACK
					
					IF eHudColour = HUD_COLOUR_BLACK
						IF ABSF(vPlayerPos.z - vScenePedPos.z) > 1.0
							bIsPlayerInGreetArea = FALSE
							eHudColour = HUD_COLOUR_GREEN
						ENDIF
					ENDIF
					
					IF eHudColour = HUD_COLOUR_BLACK
						IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							INT iPlayerRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
							INT iScenePedRoomKey = GET_ROOM_KEY_FROM_ENTITY(casinoStruct.activityPeds[i].PedID)
							
							IF (iPlayerRoomKey != iScenePedRoomKey)
								bIsPlayerInGreetArea = FALSE
								eHudColour = HUD_COLOUR_YELLOW
							ENDIF
						ENDIF
					ENDIF
					
					IF bIsPlayerInGreetArea
						eHudColour = HUD_COLOUR_PURE_WHITE
					ENDIF
					
					GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
					iAlpha = ROUND(TO_FLOAT(iAlpha)*0.5)
					DRAW_DEBUG_SPHERE(vScenePedCoord, CASINO_PED_DEFAULT_GREET_AREA(ePeds), 255-iRed, 255-iGreen, 255-iBlue, iAlpha)
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	VECTOR vScenePedCoord = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)+<<0,0,0.5>>
	
	//
	iOffset_y = -1
	iOffset_x = -10
	str  = "stTime: "
	IF NOT HAS_NET_TIMER_STARTED(g_stCasinoSpeechTime)
		str += "NULL"
		eHudColour = HUD_COLOUR_BLACK
		GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
		iAlpha = ROUND(TO_FLOAT(iAlpha)*0.5)
	ELSE
		str += GET_STRING_FROM_FLOAT(TO_FLOAT(GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_stCasinoSpeechTime))/1000.0)
		eHudColour = HUD_COLOUR_BLACK
		GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
	ENDIF
	str += ", stGreetTime: "
	IF NOT HAS_NET_TIMER_STARTED(g_stCasinoSpeechGreetTime)
		str += "NULL"
		eHudColour = HUD_COLOUR_BLACK
		GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
		iAlpha = ROUND(TO_FLOAT(iAlpha)*0.5)
	ELSE
		str += GET_STRING_FROM_FLOAT(TO_FLOAT(GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(g_stCasinoSpeechGreetTime))/1000.0)
		eHudColour = HUD_COLOUR_BLACK
		GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
	ENDIF
		
	DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
	
	iOffset_y = 0
	REPEAT COUNT_OF(casinoStruct.sDebugSpeech.tl63Context) iOffset_y
		IF NOT IS_STRING_NULL_OR_EMPTY(casinoStruct.sDebugSpeech.tl63Context[iOffset_y])
			iOffset_x = -20
			
			str  = GET_CASINO_PED_TYPE_STRING(casinoStruct.sDebugSpeech.ePeds[iOffset_y])
			eHudColour = HUD_COLOUR_PURE_WHITE
			GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
			DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
			iOffset_x += 15
			
			str  = "\""
			str += casinoStruct.sDebugSpeech.tl63Context[iOffset_y]
			str += "\""
			eHudColour = HUD_COLOUR_PURE_WHITE
			GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
			DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
			iOffset_x += 20
			
			str  = ""
			str += GET_STRING_FROM_FLOAT(TO_FLOAT(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), casinoStruct.sDebugSpeech.Timer[iOffset_y])))/1000.0)
			eHudColour = HUD_COLOUR_PURE_WHITE
			GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
			DRAW_DEBUG_TEXT_WITH_OFFSET(str, vScenePedCoord, iOffset_x*10, iOffset_y*10, iRed, iGreen, iBlue, iAlpha)
			iOffset_x += 20
		ENDIF
	ENDREPEAT
ENDPROC

CONST_FLOAT cfELEMENT_HEIGHT	 	0.04	
CONST_FLOAT cfELEMENT_WIDTH 		0.82

FUNC STRING GET_STRING_CASINO_AUDIO_TAG_FROM_AUDIO_TRACK_HASH(INT iHashedTrackName)
	STRING sReturn
	SWITCH iHashedTrackName
		CASE HASH("DLC_BATTLE_MUSIC_BATTLE_MIX1_CLUB_01")
			sReturn = "DLC_BATTLE_MUSIC_BATTLE_MIX1_CLUB_01"
		BREAK
		CASE HASH("DLC_BATTLE_MUSIC_BATTLE_MIX1_CLUB_02")
			sReturn = "DLC_BATTLE_MUSIC_BATTLE_MIX1_CLUB_02"
		BREAK
		
		CASE HASH("DLC_BATTLE_MUSIC_BATTLE_MIX2_CLUB_01")
			sReturn = "DLC_BATTLE_MUSIC_BATTLE_MIX2_CLUB_01"
		BREAK
		CASE HASH("DLC_BATTLE_MUSIC_BATTLE_MIX2_CLUB_02")
			sReturn = "DLC_BATTLE_MUSIC_BATTLE_MIX2_CLUB_02"
		BREAK
		
		CASE HASH("DLC_BATTLE_MUSIC_BATTLE_MIX3_CLUB_01")
			sReturn = "DLC_BATTLE_MUSIC_BATTLE_MIX3_CLUB_01"
		BREAK
		CASE HASH("DLC_BATTLE_MUSIC_BATTLE_MIX3_CLUB_02")
			sReturn = "DLC_BATTLE_MUSIC_BATTLE_MIX3_CLUB_02"
		BREAK
		DEFAULT
			sReturn = GET_STRING_FROM_INT(iHashedTrackName)
		BREAK
	ENDSWITCH
	RETURN sReturn 
ENDFUNC

PROC MAINTAIN_VISUAL_DEBUG_BLOCK_DISPLAY(CASINO_STRUCT &casinoStruct)

	TEXT_LABEL_63 tlBeatTemp
	DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
	
	INT i
	REPEAT g_VisualDisplayStruct.iBlockCount i
		IF NOT IS_STRING_NULL_OR_EMPTY(g_VisualDisplayStruct.VisualBlockArray[i].tlBlockHeading)
			g_VisualDisplayStruct.VisualBlockArray[i].fCentreX = 0.8
			g_VisualDisplayStruct.VisualBlockArray[i].fCentreY = 0.5 //0.2*i + (0.07)
			g_VisualDisplayStruct.VisualBlockArray[i].fWidth = 0.4
			g_VisualDisplayStruct.VisualBlockArray[i].fHeight = 0.09	
		ENDIF
	ENDREPEAT
	
	i = 0
	
	DRAW_RECT(g_VisualDisplayStruct.VisualBlockArray[0].fCentreX, 0.75, g_VisualDisplayStruct.VisualBlockArray[0].fWidth, 0.5, 0, 0, 0, 128)
	
	FLOAT fTextScale = 0.25
	FLOAT fYLineSpacer = 0.015
	FLOAT fYPos
	
	REPEAT MAX_NUMBER_OF_VISIBLE_BLOCKS i
		IF NOT IS_STRING_NULL_OR_EMPTY(g_VisualDisplayStruct.VisualBlockArray[i].tlBlockHeading)
		AND (i = 0)
			
			fYPos = g_VisualDisplayStruct.VisualBlockArray[i].fCentreY
			
			fYPos += fYLineSpacer
			tlBeatTemp = "g_iCasinoPedsHallwayLayout = "
			tlBeatTemp += GET_STRING_FROM_INT(g_iCasinoPedsHallwayLayout)
			SET_TEXT_SCALE(fTextScale, fTextScale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(g_VisualDisplayStruct.VisualBlockArray[i].fCentreX - (g_VisualDisplayStruct.VisualBlockArray[i].fWidth/2), fYPos, "STRING", tlBeatTemp)//5th arg must be "STRING" for the string literal in next argument to be printed
			
			fYPos += fYLineSpacer
			tlBeatTemp = "g_iCasinoPedsToiletLayout = "
			tlBeatTemp += GET_STRING_FROM_INT(g_iCasinoPedsToiletLayout)
			SET_TEXT_SCALE(fTextScale, fTextScale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(g_VisualDisplayStruct.VisualBlockArray[i].fCentreX - (g_VisualDisplayStruct.VisualBlockArray[i].fWidth/2), fYPos, "STRING", tlBeatTemp)//5th arg must be "STRING" for the string literal in next argument to be printed
			
			fYPos += fYLineSpacer
			tlBeatTemp = "g_iCasinoPedsHallwayLayout = "
			tlBeatTemp += GET_STRING_FROM_INT(g_iCasinoPedsHallwayLayout)
			SET_TEXT_SCALE(fTextScale, fTextScale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(g_VisualDisplayStruct.VisualBlockArray[i].fCentreX - (g_VisualDisplayStruct.VisualBlockArray[i].fWidth/2), fYPos, "STRING", tlBeatTemp)//5th arg must be "STRING" for the string literal in next argument to be printed
			
			fYPos += fYLineSpacer
			tlBeatTemp = "g_iCasinoPedsMainAreaLayout = "
			tlBeatTemp += GET_STRING_FROM_INT(g_iCasinoPedsMainAreaLayout)
			SET_TEXT_SCALE(fTextScale, fTextScale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(g_VisualDisplayStruct.VisualBlockArray[i].fCentreX - (g_VisualDisplayStruct.VisualBlockArray[i].fWidth/2), fYPos, "STRING", tlBeatTemp)//5th arg must be "STRING" for the string literal in next argument to be printed
			
//			fYPos += fYLineSpacer
//			tlBeatTemp = "GET_CASINO_ACTUAL_PED_COUNT = "
//			tlBeatTemp += GET_CASINO_ACTUAL_PED_COUNT(casinoStruct)
//			SET_TEXT_SCALE(fTextScale, fTextScale)
//			DISPLAY_TEXT_WITH_LITERAL_STRING(g_VisualDisplayStruct.VisualBlockArray[i].fCentreX - (g_VisualDisplayStruct.VisualBlockArray[i].fWidth/2), fYPos, "STRING", tlBeatTemp)//5th arg must be "STRING" for the string literal in next argument to be printed
			
//			fYPos += fYLineSpacer
//			tlBeatTemp = "With no toilet/hallways = "
//			tlBeatTemp += GET_CASINO_ACTUAL_PED_COUNT(casinoStruct, FALSE)
//			SET_TEXT_SCALE(fTextScale, fTextScale)
//			DISPLAY_TEXT_WITH_LITERAL_STRING(g_VisualDisplayStruct.VisualBlockArray[i].fCentreX - (g_VisualDisplayStruct.VisualBlockArray[i].fWidth/2), fYPos, "STRING", tlBeatTemp)//5th arg must be "STRING" for the string literal in next argument to be printed
		ENDIF
	ENDREPEAT
	
	FLOAT fLeftMostCoord
	
	INT j 
	REPEAT MAX_NUMBER_OF_CASINO_PEDS j
		IF INT_TO_ENUM(CASINO_PED_TYPES, g_iCasinoPedType[j]) = CASINO_NULL_PED
			#IF IS_DEBUG_BUILD
			g_VisualDisplayStruct.VisualPedBlockArray[j].iR = 0 
			g_VisualDisplayStruct.VisualPedBlockArray[j].iG = 0
			g_VisualDisplayStruct.VisualPedBlockArray[j].iB = 255
			#ENDIF
		ELSE
			IF NOT DOES_ENTITY_EXIST(casinoStruct.activityPeds[j].PedID)
				#IF IS_DEBUG_BUILD
				g_VisualDisplayStruct.VisualPedBlockArray[j].iR = 255
				g_VisualDisplayStruct.VisualPedBlockArray[j].iG = 0
				g_VisualDisplayStruct.VisualPedBlockArray[j].iB = 0
				#ENDIF
			ELSE
				g_VisualDisplayStruct.VisualPedBlockArray[j].iR = 0 
				g_VisualDisplayStruct.VisualPedBlockArray[j].iG = 255
				g_VisualDisplayStruct.VisualPedBlockArray[j].iB = 0
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(casinoStruct.activityPeds[j].PedID)
			IF NOT IS_ENTITY_DEAD(casinoStruct.activityPeds[j].PedID)
				IF HAS_ANIM_EVENT_FIRED(casinoStruct.activityPeds[j].PedID, HASH("BEAT_LOOP_MARKER"))
					g_VisualDisplayStruct.VisualPedBlockArray[j].iR = 255 
					g_VisualDisplayStruct.VisualPedBlockArray[j].iG = 255
					g_VisualDisplayStruct.VisualPedBlockArray[j].iB = 0		
				ENDIF
			ENDIF
		ENDIF
		
		g_VisualDisplayStruct.VisualPedBlockArray[j].fWidth = g_VisualDisplayStruct.VisualBlockArray[0].fWidth/MAX_NUMBER_OF_CASINO_PEDS
		g_VisualDisplayStruct.VisualPedBlockArray[j].fHeight = 0.09
		
		fLeftMostCoord = g_VisualDisplayStruct.VisualBlockArray[1].fCentreX - (g_VisualDisplayStruct.VisualBlockArray[0].fWidth/2) // left most coord of block
		g_VisualDisplayStruct.VisualPedBlockArray[j].fCentreX = fLeftMostCoord + (g_VisualDisplayStruct.VisualPedBlockArray[j].fWidth/2) + (g_VisualDisplayStruct.VisualPedBlockArray[j].fWidth*j)		
		g_VisualDisplayStruct.VisualPedBlockArray[j].fCentreY = g_VisualDisplayStruct.VisualBlockArray[1].fCentreY
	ENDREPEAT
	
	j = 0
	MODEL_NAMES ePropModel
	REPEAT MAX_NUMBER_OF_CASINO_PEDS j
		GET_CASINO_PROP_DATA_FOR_ACTIVITY(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iCasinoActivitySlot[i]), 0, ePropModel)
		
		IF IS_MODEL_IN_CDIMAGE(ePropModel)
			IF NOT DOES_ENTITY_EXIST(casinoStruct.activityPeds[j].objectId[0])
				#IF IS_DEBUG_BUILD
				g_VisualDisplayStruct.VisualPropBlockArray[j].iR = 255 
				g_VisualDisplayStruct.VisualPropBlockArray[j].iG = 0
				g_VisualDisplayStruct.VisualPropBlockArray[j].iB = 0
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				g_VisualDisplayStruct.VisualPropBlockArray[j].iR = 0 
				g_VisualDisplayStruct.VisualPropBlockArray[j].iG = 255
				g_VisualDisplayStruct.VisualPropBlockArray[j].iB = 0
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			g_VisualDisplayStruct.VisualPropBlockArray[j].iR = 0 
			g_VisualDisplayStruct.VisualPropBlockArray[j].iG = 0
			g_VisualDisplayStruct.VisualPropBlockArray[j].iB = 255
			#ENDIF
		ENDIF
		
		g_VisualDisplayStruct.VisualPropBlockArray[j].fWidth = g_VisualDisplayStruct.VisualBlockArray[0].fWidth/MAX_NUMBER_OF_CASINO_PEDS
		g_VisualDisplayStruct.VisualPropBlockArray[j].fHeight = 0.09
		
		fLeftMostCoord = g_VisualDisplayStruct.VisualBlockArray[1].fCentreX - (g_VisualDisplayStruct.VisualBlockArray[3].fWidth/2) // left most coord of block
		g_VisualDisplayStruct.VisualPropBlockArray[j].fCentreX = fLeftMostCoord + (g_VisualDisplayStruct.VisualPropBlockArray[j].fWidth/2) + (g_VisualDisplayStruct.VisualPropBlockArray[j].fWidth*j)		
		g_VisualDisplayStruct.VisualPropBlockArray[j].fCentreY = g_VisualDisplayStruct.VisualBlockArray[3].fCentreY
	ENDREPEAT
	
ENDPROC

PROC DEBUG_DISPLAY_CASINO_PED_DEBUG(SERVER_BROADCAST_DATA &serverBD, CASINO_STRUCT &casinoStruct)
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_RETURN, KEYBOARD_MODIFIER_CTRL_SHIFT, "Casino Peds Debug Widget")
		casinoStruct.iDrawStateDebug++
		IF casinoStruct.iDrawStateDebug > 3
			casinoStruct.iDrawStateDebug = 0
		ENDIF
	ENDIF
	
	SWITCH casinoStruct.iDrawStateDebug
		CASE 0
			// Off
		BREAK
		CASE 1
			// Ped Info
			SHOW_PED_IDS(serverBD, casinoStruct, casinoStruct.iCasinoPedToHighlight)
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Editor: Peds", 0, 1)
		BREAK
		CASE 2
			// Speech Info
			SHOW_PED_SPEECH(serverBD, casinoStruct)
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Editor: Ped Speech", 0, 1)
		BREAK
		CASE 3
			// Roulette Info
			SHOW_PED_ROULETTE_INFO(serverBD, casinoStruct, casinoStruct.iCasinoPedToHighlight)
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Editor: Ped Roulette", 0, 1)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC CLEANUP_VISUAL_DEBUG_DATA()
	INT i
	REPEAT g_VisualDisplayStruct.iBlockCount i
		g_VisualDisplayStruct.VisualBlockArray[i].fCentreX = 0
		g_VisualDisplayStruct.VisualBlockArray[i].fCentreY = 0
		g_VisualDisplayStruct.VisualBlockArray[i].fWidth = 0
		g_VisualDisplayStruct.VisualBlockArray[i].fHeight = 0
		g_VisualDisplayStruct.VisualBlockArray[i].tlBlockHeading = ""
	ENDREPEAT
	
	INT j = 0
	REPEAT MAX_NUMBER_OF_CASINO_PEDS	j
		g_VisualDisplayStruct.VisualPropBlockArray[j].iB = 0
		g_VisualDisplayStruct.VisualPropBlockArray[j].iG = 0
		g_VisualDisplayStruct.VisualPropBlockArray[j].iR = 0
		
		g_VisualDisplayStruct.VisualPedBlockArray[j].iB = 0
		g_VisualDisplayStruct.VisualPedBlockArray[j].iR = 0
		g_VisualDisplayStruct.VisualPedBlockArray[j].iG = 0
	ENDREPEAT
	
	g_VisualDisplayStruct.iBlockCount = 0
ENDPROC

PROC CREATE_ALL_PEDS_FOR_COMPONENTS(SERVER_BROADCAST_DATA serverBD, CASINO_STRUCT &casinoStruct, MODEL_NAMES ClienteleModel, STRING sPath, STRING sFile)
	
	REQUEST_MODEL(ClienteleModel)
	WHILE NOT HAS_MODEL_LOADED(ClienteleModel)
		WAIT(0)
	ENDWHILE
	
	INT j
	INT m
	PED_INDEX PedID[MAX_NUMBER_OF_CASINO_PEDS]
	INT iPackedDrawable1
	INT iPackedTexture1
	INT iPackedDrawable2
	INT iPackedTexture2
	INT iDrawable
	INT iTexture
	PED_COMPONENT_CONFIG ComponentConfig
	VECTOR vPos
	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("						//  ",sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(ClienteleModel),sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
	
	// Create first 100 peds
	REPEAT 100 j
		vPos = GET_CASINO_PED_POSITION(j, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[j].eAssignedArea)], casinoStruct.activityPeds[j].eAssignedArea, INT_TO_ENUM(CASINO_PED_TYPES, casinoStruct.activityPeds[j].iCasinoPedType), casinoStruct.activityPeds[j].eActiveActivity)
		IF (VMAG(vPos) > 0.0)
			pedID[j] = CREATE_PED(PEDTYPE_MISSION, ClienteleModel, vPos, GET_CASINO_PED_HEADING(j, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[j].eAssignedArea)], casinoStruct.activityPeds[j].eAssignedArea, casinoStruct.activityPeds[j].eActiveActivity), FALSE, FALSE)			
			IF DOES_ENTITY_EXIST(pedID[j])
			AND NOT IS_ENTITY_DEAD(pedID[j])																			
				REPEAT NUM_PED_COMPONENTS m
					iDrawable = GET_PED_DRAWABLE_VARIATION(pedID[j], INT_TO_ENUM(PED_COMPONENT, m))
					iTexture = GET_PED_TEXTURE_VARIATION(pedID[j], INT_TO_ENUM(PED_COMPONENT, m))
					IF (m < MAX_NUM_OF_SETTABLE_COMPONENTS)																				
						ComponentConfig.ComponentData[m].iDrawable = iDrawable
						ComponentConfig.ComponentData[m].iTexture = iTexture
					ENDIF									
				ENDREPEAT
				PACK_PED_COMPONENT_CONFIG(ComponentConfig, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("						CASE ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(j,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("							iPackedDrawable1 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedDrawable1,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("							iPackedDrawable2 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedDrawable2,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("							iPackedTexture1 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedTexture1,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("							iPackedTexture2 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedTexture2,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("						BREAK",sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Delete
	REPEAT MAX_NUMBER_OF_CASINO_PEDS j	
		IF DOES_ENTITY_EXIST(pedID[j])
			DELETE_PED(pedid[j])
		ENDIF
	ENDREPEAT
	
	// Create remaining peds
	REPEAT MAX_NUMBER_OF_CASINO_PEDS j	
		IF (j < 100)
		 	j = 100
		ENDIF
		vPos = GET_CASINO_PED_POSITION(j, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[j].eAssignedArea)], casinoStruct.activityPeds[j].eAssignedArea, INT_TO_ENUM(CASINO_PED_TYPES, casinoStruct.activityPeds[j].iCasinoPedType), casinoStruct.activityPeds[j].eActiveActivity)
		IF (VMAG(vPos) > 0.0)
			pedID[j] = CREATE_PED(PEDTYPE_MISSION, ClienteleModel, vPos, GET_CASINO_PED_HEADING(j, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[j].eAssignedArea)], casinoStruct.activityPeds[j].eAssignedArea, casinoStruct.activityPeds[j].eActiveActivity), FALSE, FALSE)
			IF DOES_ENTITY_EXIST(pedID[j])
			AND NOT IS_ENTITY_DEAD(pedID[j])																			
				REPEAT NUM_PED_COMPONENTS m
					iDrawable = GET_PED_DRAWABLE_VARIATION(pedID[j], INT_TO_ENUM(PED_COMPONENT, m))
					iTexture = GET_PED_TEXTURE_VARIATION(pedID[j], INT_TO_ENUM(PED_COMPONENT, m))
					IF (m < MAX_NUM_OF_SETTABLE_COMPONENTS)																				
						ComponentConfig.ComponentData[m].iDrawable = iDrawable
						ComponentConfig.ComponentData[m].iTexture = iTexture
					ENDIF									
				ENDREPEAT
				PACK_PED_COMPONENT_CONFIG(ComponentConfig, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("						CASE ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(j,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("							iPackedDrawable1 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedDrawable1,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("							iPackedDrawable2 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedDrawable2,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("							iPackedTexture1 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedTexture1,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("							iPackedTexture2 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedTexture2,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("						BREAK",sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Delete
	REPEAT MAX_NUMBER_OF_CASINO_PEDS j	
		IF DOES_ENTITY_EXIST(pedID[j])
			DELETE_PED(pedid[j])
		ENDIF
	ENDREPEAT
ENDPROC

PROC OUTPUT_DEBUG_CASINO_PED_COMPONENT_VARIATIONS(CASINO_STRUCT &casinoStruct, SERVER_BROADCAST_DATA serverBD)
	
	IF g_bOutputCasinoPedClothingSettings
		STRING sPath = "X:/gta5/titleupdate/dev_ng/"
		
		INT iYear, iMonth, iDay, iHour, iMins, iSec
		GET_LOCAL_TIME(iYear, iMonth, iDay, iHour, iMins, iSec)
		
		TEXT_LABEL_63 sFile = "casino_component_data_"
		IF (iHour < 10)
			sFile += 0
		ENDIF
		sFile += iHour
		sFile += "."
		IF (iMins < 10)
			sFile += 0
		ENDIF
		sFile += iMins
		sFile += "."
		IF (iSec < 10)
			sFile += 0
		ENDIF
		sFile += iSec
		sFile += ".txt"
		
		CLEAR_NAMED_DEBUG_FILE(sPath, sFile)
		OPEN_NAMED_DEBUG_FILE(sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
		
		
		INT iPedID
		INT iTexture
		INT iDrawable
		INT iComponents
		INT iPackedTexture1
		INT iPackedDrawable1
		INT iPackedTexture2
		INT iPackedDrawable2
		PED_COMPONENT_CONFIG ComponentConfig
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("Current Layout: ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.ePlayerArea)], sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("Current Area: ",sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(casinoStruct.ePlayerArea),sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("SWITCH iPedID",sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
		
		REPEAT MAX_NUMBER_OF_CASINO_PEDS iPedID
			IF IS_ENTITY_ALIVE(casinoStruct.activityPeds[iPedID].PedID)
				
				REPEAT NUM_PED_COMPONENTS iComponents
					iDrawable = GET_PED_DRAWABLE_VARIATION(casinoStruct.activityPeds[iPedID].PedID, INT_TO_ENUM(PED_COMPONENT, iComponents))
					iTexture = GET_PED_TEXTURE_VARIATION(casinoStruct.activityPeds[iPedID].PedID, INT_TO_ENUM(PED_COMPONENT, iComponents))
					IF (iComponents < MAX_NUM_OF_SETTABLE_COMPONENTS)																				
						ComponentConfig.ComponentData[iComponents].iDrawable = iDrawable
						ComponentConfig.ComponentData[iComponents].iTexture = iTexture
					ENDIF									
				ENDREPEAT
				
				PACK_PED_COMPONENT_CONFIG(ComponentConfig, iPackedDrawable1, iPackedDrawable2, iPackedTexture1, iPackedTexture2)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE ", sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	//Ped type: ", sPath, sFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, casinoStruct.activityPeds[iPedID].iCasinoPedType)), sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)	
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("		iPackedDrawable1 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedDrawable1,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("		iPackedDrawable2 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedDrawable2,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("		iPackedTexture1 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedTexture1,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("		iPackedTexture2 = ",sPath, sFile)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iPackedTexture2,sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK ", sPath, sFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
			ENDIF
		ENDREPEAT
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDSWITCH",sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
		
		CLOSE_DEBUG_FILE()
		g_bOutputCasinoPedClothingSettings = FALSE
	ENDIF
	
ENDPROC

PROC UPDATE_DEBUG_CASINO_PED_DATA(CASINO_STRUCT &casinoStruct)
	
	INT iPedID
	INT iIndex
	REPEAT MAX_NUMBER_OF_CASINO_PEDS iPedID
		CASINO_AREA eArea = casinoStruct.activityPeds[iPedID].eAssignedArea
		
		SWITCH eArea
			CASE CASINO_AREA_PERMANENT
				// 0 - 38
				IF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_PERMANENT)] = 1
					g_vDebugPedEditorLayoutOnePedCoords[iPedID] = g_vCasinoPedPos[iPedID]
					g_fDebugPedEditorLayoutOnePedHeading[iPedID] = g_fCasinoPedHeading[iPedID]
					eDebugPedEditorLayoutOne[iPedID].iActivity = g_iCasinoActivitySlot[iPedID]
					eDebugPedEditorLayoutOne[iPedID].iPedType = g_iCasinoPedType[iPedID]
					eDebugPedEditorLayoutOne[iPedID].iAssignedArea = ENUM_TO_INT(CASINO_AREA_PERMANENT)
				ELIF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_PERMANENT)] = 2
					g_vDebugPedEditorLayoutTwoPedCoords[iPedID] = g_vCasinoPedPos[iPedID]
					g_fDebugPedEditorLayoutTwoPedHeading[iPedID] = g_fCasinoPedHeading[iPedID]
					eDebugPedEditorLayoutTwo[iPedID].iActivity = g_iCasinoActivitySlot[iPedID]
					eDebugPedEditorLayoutTwo[iPedID].iPedType = g_iCasinoPedType[iPedID]
					eDebugPedEditorLayoutTwo[iPedID].iAssignedArea = ENUM_TO_INT(CASINO_AREA_PERMANENT)
				ELIF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_PERMANENT)] = 3
					g_vDebugPedEditorLayoutThreePedCoords[iPedID] = g_vCasinoPedPos[iPedID]
					g_fDebugPedEditorLayoutThreePedHeading[iPedID] = g_fCasinoPedHeading[iPedID]
					eDebugPedEditorLayoutThree[iPedID].iActivity = g_iCasinoActivitySlot[iPedID]
					eDebugPedEditorLayoutThree[iPedID].iPedType = g_iCasinoPedType[iPedID]
					eDebugPedEditorLayoutThree[iPedID].iAssignedArea = ENUM_TO_INT(CASINO_AREA_PERMANENT)
				ENDIF
			BREAK
			CASE CASINO_AREA_MAIN_FLOOR
				// 39 - 124
				IF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_MAIN_FLOOR)] = 1
					g_vDebugPedEditorLayoutOnePedCoords[iPedID] = g_vCasinoPedPos[iPedID]
					g_fDebugPedEditorLayoutOnePedHeading[iPedID] = g_fCasinoPedHeading[iPedID]
					eDebugPedEditorLayoutOne[iPedID].iActivity = g_iCasinoActivitySlot[iPedID]
					eDebugPedEditorLayoutOne[iPedID].iPedType = g_iCasinoPedType[iPedID]
					eDebugPedEditorLayoutOne[iPedID].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MAIN_FLOOR)
				ELIF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_MAIN_FLOOR)] = 2
					g_vDebugPedEditorLayoutTwoPedCoords[iPedID] = g_vCasinoPedPos[iPedID]
					g_fDebugPedEditorLayoutTwoPedHeading[iPedID] = g_fCasinoPedHeading[iPedID]
					eDebugPedEditorLayoutTwo[iPedID].iActivity = g_iCasinoActivitySlot[iPedID]
					eDebugPedEditorLayoutTwo[iPedID].iPedType = g_iCasinoPedType[iPedID]
					eDebugPedEditorLayoutTwo[iPedID].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MAIN_FLOOR)
				ELIF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_MAIN_FLOOR)] = 3
					g_vDebugPedEditorLayoutThreePedCoords[iPedID] = g_vCasinoPedPos[iPedID]
					g_fDebugPedEditorLayoutThreePedHeading[iPedID] = g_fCasinoPedHeading[iPedID]
					eDebugPedEditorLayoutThree[iPedID].iActivity = g_iCasinoActivitySlot[iPedID]
					eDebugPedEditorLayoutThree[iPedID].iPedType = g_iCasinoPedType[iPedID]
					eDebugPedEditorLayoutThree[iPedID].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MAIN_FLOOR)
				ENDIF
			BREAK
			CASE CASINO_AREA_TABLE_GAMES
				// 125 - 210
				IF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_TABLE_GAMES)] = 1
					g_vDebugPedEditorLayoutOnePedCoords[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = g_vCasinoPedPos[iPedID]
					g_fDebugPedEditorLayoutOnePedHeading[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = g_fCasinoPedHeading[iPedID]
					eDebugPedEditorLayoutOne[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iActivity = g_iCasinoActivitySlot[iPedID]
					eDebugPedEditorLayoutOne[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iPedType = g_iCasinoPedType[iPedID]
					eDebugPedEditorLayoutOne[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iAssignedArea = ENUM_TO_INT(CASINO_AREA_TABLE_GAMES)
				ELIF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_TABLE_GAMES)] = 2
					g_vDebugPedEditorLayoutTwoPedCoords[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = g_vCasinoPedPos[iPedID]
					g_fDebugPedEditorLayoutTwoPedHeading[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = g_fCasinoPedHeading[iPedID]
					eDebugPedEditorLayoutTwo[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iActivity = g_iCasinoActivitySlot[iPedID]
					eDebugPedEditorLayoutTwo[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iPedType = g_iCasinoPedType[iPedID]
					eDebugPedEditorLayoutTwo[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iAssignedArea = ENUM_TO_INT(CASINO_AREA_TABLE_GAMES)
				ELIF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_TABLE_GAMES)] = 3
					g_vDebugPedEditorLayoutThreePedCoords[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = g_vCasinoPedPos[iPedID]
					g_fDebugPedEditorLayoutThreePedHeading[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)] = g_fCasinoPedHeading[iPedID]
					eDebugPedEditorLayoutThree[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iActivity = g_iCasinoActivitySlot[iPedID]
					eDebugPedEditorLayoutThree[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iPedType = g_iCasinoPedType[iPedID]
					eDebugPedEditorLayoutThree[(iPedID+MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA)].iAssignedArea = ENUM_TO_INT(CASINO_AREA_TABLE_GAMES)
				ENDIF
			BREAK
			CASE CASINO_AREA_SPORTS_BETTING
				// 211 - 225
				IF (iPedID >= MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA 
				AND iPedID < MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA+MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA)
					iIndex = (iPedID+(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2))
					IF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_SPORTS_BETTING)] = 1
						g_vDebugPedEditorLayoutOnePedCoords[iIndex] = g_vCasinoPedPos[iPedID]
						g_fDebugPedEditorLayoutOnePedHeading[iIndex] = g_fCasinoPedHeading[iPedID]
						eDebugPedEditorLayoutOne[iIndex].iActivity = g_iCasinoActivitySlot[iPedID]
						eDebugPedEditorLayoutOne[iIndex].iPedType = g_iCasinoPedType[iPedID]
						eDebugPedEditorLayoutOne[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_SPORTS_BETTING)
					ELIF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_SPORTS_BETTING)] = 2
						g_vDebugPedEditorLayoutTwoPedCoords[iIndex] = g_vCasinoPedPos[iPedID]
						g_fDebugPedEditorLayoutTwoPedHeading[iIndex] = g_fCasinoPedHeading[iPedID]
						eDebugPedEditorLayoutTwo[iIndex].iActivity = ENUM_TO_INT(g_iCasinoActivitySlot[iPedID])
						eDebugPedEditorLayoutTwo[iIndex].iPedType = ENUM_TO_INT(g_iCasinoPedType[iPedID])
						eDebugPedEditorLayoutTwo[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_SPORTS_BETTING)
					ELIF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_SPORTS_BETTING)] = 3
						g_vDebugPedEditorLayoutThreePedCoords[iIndex] = g_vCasinoPedPos[iPedID]
						g_fDebugPedEditorLayoutThreePedHeading[iIndex] = g_fCasinoPedHeading[iPedID]
						eDebugPedEditorLayoutThree[iIndex].iActivity = g_iCasinoActivitySlot[iPedID]
						eDebugPedEditorLayoutThree[iIndex].iPedType = g_iCasinoPedType[iPedID]
						eDebugPedEditorLayoutThree[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_SPORTS_BETTING)
					ENDIF
				ENDIF
			BREAK
			CASE CASINO_AREA_MANAGERS_OFFICE
				// 226 - 228
				IF (iPedID = 42 OR iPedID = 43)
					iIndex = (iPedID+(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)+MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-(MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE+1))
					IF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_MANAGERS_OFFICE)] = 1
						g_vDebugPedEditorLayoutOnePedCoords[iIndex] = g_vCasinoPedPos[iPedID]
						g_fDebugPedEditorLayoutOnePedHeading[iIndex] = g_fCasinoPedHeading[iPedID]
						eDebugPedEditorLayoutOne[iIndex].iActivity = g_iCasinoActivitySlot[iPedID]
						eDebugPedEditorLayoutOne[iIndex].iPedType = g_iCasinoPedType[iPedID]
						eDebugPedEditorLayoutOne[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MANAGERS_OFFICE)
					ELIF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_MANAGERS_OFFICE)] = 2
						g_vDebugPedEditorLayoutTwoPedCoords[iIndex] = g_vCasinoPedPos[iPedID]
						g_fDebugPedEditorLayoutTwoPedHeading[iIndex] = g_fCasinoPedHeading[iPedID]
						eDebugPedEditorLayoutTwo[iIndex].iActivity = g_iCasinoActivitySlot[iPedID]
						eDebugPedEditorLayoutTwo[iIndex].iPedType = g_iCasinoPedType[iPedID]
						eDebugPedEditorLayoutTwo[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MANAGERS_OFFICE)
					ELIF g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_MANAGERS_OFFICE)] = 3
						g_vDebugPedEditorLayoutThreePedCoords[iIndex] = g_vCasinoPedPos[iPedID]
						g_fDebugPedEditorLayoutThreePedHeading[iIndex] = g_fCasinoPedHeading[iPedID]
						eDebugPedEditorLayoutThree[iIndex].iActivity = g_iCasinoActivitySlot[iPedID]
						eDebugPedEditorLayoutThree[iIndex].iPedType = g_iCasinoPedType[iPedID]
						eDebugPedEditorLayoutThree[iIndex].iAssignedArea = ENUM_TO_INT(CASINO_AREA_MANAGERS_OFFICE)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
	
ENDPROC

PROC OUTPUT_DEBUG_CASINO_PED_FILE_HEADING(STRING sPath, STRING sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("/////////////////////////////", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("//     CASINO PED DATA     //", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("/////////////////////////////", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
ENDPROC

PROC OUTPUT_DEBUG_CASINO_PED_SPAWN_COORDS(STRING sPath, STRING sFile)
	INT iPedID
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Spawn Coords: ", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("SWITCH eCasinoArea", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Transition Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_PERMANENT", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	vSpawnCoords = ", sPath, sFile)
		ENDIF
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutOnePedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	vSpawnCoords = ", sPath, sFile)
		ENDIF
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutTwoPedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	vSpawnCoords = ", sPath, sFile)
		ENDIF
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutThreePedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Main Floor Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_MAIN_FLOOR", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	vSpawnCoords = ", sPath, sFile)
		ENDIF
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutOnePedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	vSpawnCoords = ", sPath, sFile)
		ENDIF
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutTwoPedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	vSpawnCoords = ", sPath, sFile)
		ENDIF
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutThreePedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Table Games Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_TABLE_GAMES", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO ((DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA)-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	vSpawnCoords = ", sPath, sFile)
		ENDIF
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutOnePedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO ((DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA)-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	vSpawnCoords = ", sPath, sFile)
		ENDIF
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutTwoPedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO ((DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA)-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	vSpawnCoords = ", sPath, sFile)
		ENDIF
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutThreePedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Sports Betting Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_SPORTS_BETTING", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutOnePedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutTwoPedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutThreePedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Managers Office Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_MANAGERS_OFFICE", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutOnePedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutTwoPedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		vSpawnCoords = ", sPath, sFile)
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(g_vDebugPedEditorLayoutThreePedCoords[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
ENDPROC

PROC OUTPUT_DEBUG_CASINO_PED_HEADINGS(STRING sPath, STRING sFile)
	INT iPedID
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Headings: ", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("SWITCH eCasinoArea", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Transition Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_PERMANENT", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	fHeading = ", sPath, sFile)
		ENDIF
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutOnePedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	fHeading = ", sPath, sFile)
		ENDIF
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutTwoPedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	fHeading = ", sPath, sFile)
		ENDIF
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutThreePedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Main Floor Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_MAIN_FLOOR", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	fHeading = ", sPath, sFile)
		ENDIF
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutOnePedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	fHeading = ", sPath, sFile)
		ENDIF
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutTwoPedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	fHeading = ", sPath, sFile)
		ENDIF
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutThreePedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Table Games Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_TABLE_GAMES", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	fHeading = ", sPath, sFile)
		ENDIF
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutOnePedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	fHeading = ", sPath, sFile)
		ENDIF
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutTwoPedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	fHeading = ", sPath, sFile)
		ENDIF
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutThreePedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Sports Betting Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_SPORTS_BETTING", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutOnePedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutTwoPedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutThreePedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Managers Office Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_MANAGERS_OFFICE", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutOnePedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutTwoPedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		fHeading = ", sPath, sFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(g_fDebugPedEditorLayoutThreePedHeading[iPedID], sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
ENDPROC

PROC OUTPUT_DEBUG_CASINO_PED_ACTIVITIES(STRING sPath, STRING sFile)
	INT iPedID
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Activities: ", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("SWITCH eCasinoArea", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Transition Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_PERMANENT", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutOne[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutTwo[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutThree[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Main Floor Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_MAIN_FLOOR", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutOne[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutTwo[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutThree[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Table Games Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_TABLE_GAMES", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutOne[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutTwo[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutThree[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Sports Betting Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_SPORTS_BETTING", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutOne[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutTwo[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutThree[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Managers Office Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_MANAGERS_OFFICE", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutOne[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutTwo[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, eDebugPedEditorLayoutThree[iPedID].iActivity)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
ENDPROC

PROC OUTPUT_DEBUG_CASINO_PED_TYPES(STRING sPath, STRING sFile)
	INT iPedID
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Ped Types: ", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("SWITCH eCasinoArea", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Transition Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_PERMANENT", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutOne[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutTwo[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutThree[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Main Floor Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_MAIN_FLOOR", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutOne[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutTwo[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutThree[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Table Games Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_TABLE_GAMES", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutOne[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutTwo[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutThree[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Sports Betting Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_SPORTS_BETTING", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutOne[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutTwo[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutThree[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Managers Office Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_MANAGERS_OFFICE", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutOne[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutTwo[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, eDebugPedEditorLayoutThree[iPedID].iPedType)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
ENDPROC

PROC OUTPUT_DEBUG_CASINO_PED_ASSIGNED_AREAS(STRING sPath, STRING sFile)
	INT iPedID
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Assigned Areas (No lookup for this; just for reference): ", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("SWITCH eCasinoArea", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Transition Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_PERMANENT", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutOne[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutTwo[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	REPEAT MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA iPedID
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutThree[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Main Floor Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_MAIN_FLOOR", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutOne[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutTwo[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS_PERMANENT_AREA TO (MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPedID, sPath, sFile)
		// Spacing
		IF iPedID < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutThree[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Table Games Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_TABLE_GAMES", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutOne[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutTwo[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = MAX_NUMBER_OF_CASINO_PEDS TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA), sPath, sFile)
		// Spacing
		IF (iPedID-MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA) < 100
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	RETURN ", sPath, sFile)
		ENDIF
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutThree[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	
	// Sports Betting Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_SPORTS_BETTING", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutOne[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutTwo[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutThree[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Managers Office Area
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE CASINO_AREA_MANAGERS_OFFICE", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH iLayout", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 1", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 1
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutOne[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 2", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 2
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutTwo[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			CASE 3", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				SWITCH iPedID", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	// Layout 3
	FOR iPedID = (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE) TO (DEBUG_MAX_NUMBER_OF_CASINO_PEDS-1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("					CASE ", sPath, sFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE((iPedID-(MAX_NUMBER_OF_CASINO_PEDS_MAIN_FLOOR_AREA*2)-MAX_NUMBER_OF_CASINO_PEDS_SPORTS_BETTING_AREA+MAX_NUMBER_OF_CASINO_PEDS_MANAGERS_OFFICE), sPath, sFile)
		// Spacing
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		RETURN ", sPath, sFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_CASINO_AREA_STRING(INT_TO_ENUM(CASINO_AREA, eDebugPedEditorLayoutThree[iPedID].iAssignedArea)), sPath, sFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	ENDFOR
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("				ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDSWITCH", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
ENDPROC

PROC OUTPUT_DEBUG_CASINO_PED_FILE_FOOTER(STRING sPath, STRING sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("/////////////////////////", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("//     END OF FILE     //", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("/////////////////////////", sPath, sFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, sFile)
ENDPROC

PROC OUTPUT_DEBUG_CASINO_PED_DATA()
	
	IF g_bOutputCasinoPedSettings
		g_bOutputCasinoPedChangesOnly  = FALSE // Always false
		
		STRING sPath = "X:/gta5/titleupdate/dev_ng/"
		
		INT iYear, iMonth, iDay, iHour, iMins, iSec
		GET_LOCAL_TIME(iYear, iMonth, iDay, iHour, iMins, iSec)
		
		TEXT_LABEL_63 sFile
		sFile = "casino_ped_data_"
		IF (iHour < 10)
			sFile += 0
		ENDIF
		sFile += iHour
		sFile += "."
		IF (iMins < 10)
			sFile += 0
		ENDIF
		sFile += iMins
		sFile += "."
		IF (iSec < 10)
			sFile += 0
		ENDIF
		sFile += iSec
		sFile += ".txt"

		CLEAR_NAMED_DEBUG_FILE(sPath, sFile)
		OPEN_NAMED_DEBUG_FILE(sPath, sFile)
		
		OUTPUT_DEBUG_CASINO_PED_FILE_HEADING(sPath, sFile)
		OUTPUT_DEBUG_CASINO_PED_SPAWN_COORDS( sPath, sFile)
		OUTPUT_DEBUG_CASINO_PED_HEADINGS(sPath, sFile)
		OUTPUT_DEBUG_CASINO_PED_ACTIVITIES(sPath, sFile)
		OUTPUT_DEBUG_CASINO_PED_TYPES(sPath, sFile)
		//OUTPUT_DEBUG_CASINO_PED_ASSIGNED_AREAS(sPath, sFile)
		OUTPUT_DEBUG_CASINO_PED_FILE_FOOTER(sPath, sFile)
		
		CLOSE_DEBUG_FILE()
		g_bOutputCasinoPedSettings = FALSE
	ENDIF
ENDPROC

PROC OUTPUT_PED_VARIATIONS(INT iPedID, PED_INDEX PedID)
	
	IF NOT IS_ENTITY_ALIVE(PedID)
		EXIT
	ENDIF
	
	PRINTLN("[CASINO_PEDS] OUTPUT_PED_VARIATIONS")
	PRINTLN("[CASINO_PEDS] OUTPUT_PED_VARIATIONS - iPedID: ", iPedID)
	
	INT iDrawables
	INT iTextures
	IF DOES_ENTITY_EXIST(PedID)
		
		STRING str = GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(PedID))
		PRINTLN("[CASINO_PEDS] OUTPUT_PED_VARIATIONS - For Model ", str)
		
		INT j, k
		REPEAT NUM_PED_COMPONENTS j
			PRINTLN("[CASINO_PEDS] OUTPUT_PED_VARIATIONS - Component ", j)
			iDrawables = GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PedID, INT_TO_ENUM(PED_COMPONENT, j))
			PRINTLN("[CASINO_PEDS] OUTPUT_PED_VARIATIONS - Drawables ", iDrawables)
			REPEAT iDrawables k
				iTextures = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PedID, INT_TO_ENUM(PED_COMPONENT, j), k)
				PRINTLN("[CASINO_PEDS] OUTPUT_PED_VARIATIONS - Drawable ", k, " Textures ", iTextures)
			ENDREPEAT
		ENDREPEAT
	ENDIF
	
ENDPROC

//PROC ASSIGN_ACTIVITY_TO_PED(CASINO_STRUCT &casinoStruct)
//	IF casinoStruct.iCasinoPedToHighlight >= 0
//		IF g_iCasinoPedActivity != ENUM_TO_INT(casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].eActiveActivity)
//			PRINTLN("[CASINO_PEDS] *Debug Widget* Changed activity to: ", g_iCasinoPedActivity)
//			casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].eActiveActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iCasinoPedActivity)
//			g_iCasinoActivitySlot[casinoStruct.iCasinoPedToHighlight] = g_iCasinoPedActivity
//			//DEBUG_DISPLAY_CASINO_PED_DEBUG(serverBD, casinoStruct)
//		ENDIF
//	ENDIF
//ENDPROC


PROC DEBUG_LOOP_THROUGH_PED_ACTIVITIES(CASINO_STRUCT &casinoStruct, SERVER_BROADCAST_DATA &serverBD)

	IF casinoStruct.iCasinoPedToHighlight >= 0
	
		g_bKillCasinoPedLogic = TRUE
		PRINTLN("[CASINO_PEDS] *Debug Widget* STARTED LOOPING WIDGET")
		
		//g_iCasinoActivitySlot[casinoStruct.iCasinoPedToHighlight] = g_iValidateHighlightedPedAnimID
				
		CASINO_AREA eArea = casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].eAssignedArea
		INT iLayout = serverBD.iPedLayout[ENUM_TO_INT(eArea)]
		//CASINO_ACTIVITY_SLOTS eActivity = GET_CASINO_PED_ACTIVITY_SLOT(casinoStruct.iCasinoPedToHighlight, iLayout, eArea)
		
		//PRINTLN("[CASINO_PEDS] *Debug Widget* Activity: ", ENUM_TO_INT(casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].eActiveActivity))			
				
		//STRING sPedAnimDict
		TEXT_LABEL_63 tlPedAnimClip
		TEXT_LABEL_63 tlPropAnimDict
		TEXT_LABEL_63 tlPropAnimClip
		FLOAT fAnimPhase = 0

		GET_CASINO_SEQUENCE_ANIM_DATA(casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].eActiveActivity
		, casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence
		, IS_CASINO_PED_FEMALE(casinoStruct.iCasinoPedToHighlight, iLayout, eArea)
		, casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight]
		, tlPedAnimClip
		, tlPropAnimDict
		, tlPropAnimClip)
		

		
		PRINTLN(
		"[CASINO_PEDS] *Debug Widget* Highlighted ped Activity: ", ENUM_TO_INT(casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].eActiveActivity)
		, " Ped sequence: ", casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence
		, " Is ped female: ", IS_CASINO_PED_FEMALE(casinoStruct.iCasinoPedToHighlight, iLayout, eArea)
		, " Casino activityPed: ", casinoStruct.iCasinoPedToHighlight
		, " Anim clip: ", tlPedAnimClip
		, " Prop anim:  ", tlPropAnimDict
		, " Prop anim clip: ", tlPropAnimClip)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tlPedAnimClip)
		
			fAnimPhase = GET_PED_ANIM_PHASE(casinoStruct.iCasinoPedToHighlight, casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence, casinoStruct)			
			PRINTLN("[CASINO_PEDS] *Debug Widget* Anim phase: ", fAnimPhase)
			IF fAnimPhase >= 0.96 OR g_iAnimationSequence = -1 OR casinoStruct.bRequestingPedAnim
				
				IF casinoStruct.bRequestedPedAnimFinished
					IF g_iAnimationSequence = -1			
						g_iAnimationSequence = 0
						casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence = g_iAnimationSequence				
					ELSE
						IF casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence = 0
							g_iAnimationSequence++		
										
							casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence = g_iAnimationSequence
						ELSE
							casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence = 0
						ENDIF
					ENDIF
				ENDIF
				
				PRINTLN("[CASINO_PEDS] *Debug Widget* Playing anim: ", casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence)
				
				casinoStruct.bRequestingPedAnim = TRUE
				casinoStruct.bRequestedPedAnimFinished = PERFORM_PED_ANIM(casinoStruct.iCasinoPedToHighlight
				, casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence, iLayout, casinoStruct)
				IF casinoStruct.bRequestedPedAnimFinished
					casinoStruct.bRequestingPedAnim = FALSE
				ENDIF
			ENDIF
			
			
			PRINTLN("[CASINO_PEDS] *Debug Widget* Ped sequence: ", casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence)
			PRINTLN("[CASINO_PEDS] *Debug Widget* Activity sequence: ", g_iAnimationSequence)	
		ELSE
			g_iAnimationSequence = -1
			g_bActivityTests = FALSE
			casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence = 0
		ENDIF
		
		PRINTLN("[CASINO_PEDS] *Debug Widget* FINISHED LOOPING WIDGET")		
	ELSE
		g_bActivityTests = FALSE
		g_iAnimationSequence = -1
	ENDIF	
ENDPROC

FUNC CASINO_ACTIVITY_SLOTS DEBUG_GET_ROULETTE_ACTIVITY_FROM_WIDGET()
	SWITCH g_iCasinoRouletteActivity
		CASE 0	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_1_REGULAR_01A
		CASE 1	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_1_REGULAR_01B
		CASE 2	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_1_M_SLOUCHY_F_ENGAGED_01A
		CASE 3	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_1_M_SLOUCHY_F_ENGAGED_01B
		CASE 4	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_2_REGULAR_02A
		CASE 5	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_2_REGULAR_02B
		CASE 6	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_2_M_SLOUCHY_WITH_DRINK_F_REGULAR_WITH_DRINK_02A
		CASE 7	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_2_M_SLOUCHY_WITH_DRINK_F_REGULAR_WITH_DRINK_02B
		CASE 8	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_3_REGULAR_03A
		CASE 9	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_3_REGULAR_03B
		CASE 10	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_3_M_ENGAGED_F_SLOUCHY_03A
		CASE 11	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_3_M_ENGAGED_F_SLOUCHY_03B
		CASE 12	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_4_REGULAR_04A
		CASE 13	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_4_REGULAR_04B
		CASE 14	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_4_M_REGULAR_WITH_DRINK_F_SLOUCHY_WITH_DRINK_04A
		CASE 15	RETURN CASINO_AC_SLOT_ROULETTE_SEAT_4_M_REGULAR_WITH_DRINK_F_SLOUCHY_WITH_DRINK_04B
	ENDSWITCH
	RETURN CASINO_AC_SLOT_NULL
ENDFUNC

PROC UPDATE_CASINO_WIDGETS(SERVER_BROADCAST_DATA& serverBD, CASINO_STRUCT &casinoStruct)
	
	IF g_bKillCasinoPedScript
		IF IS_THREAD_ACTIVE(g_CasinoPedsThreadID)
			#IF IS_DEBUG_BUILD
			PRINTLN("[CASINO_PEDS] *Debug Widget* Terminating script: ", GET_NAME_OF_SCRIPT_WITH_THIS_ID(g_CasinoPedsThreadID))
			#ENDIF
			g_bTurnOnCasinoPeds = FALSE // Don't terminate the thread directly or it won't run its cleanup procedure
			g_CasinoPedsThreadID = INT_TO_NATIVE(THREADID, -1)
		ENDIF
		g_bKillCasinoPedScript = FALSE
	ENDIF
	
	IF g_bCasinoDisplayActivePedTotal
		// Background
		DRAW_RECT(0.4785, 0.058, 0.127, 0.050, 0, 0, 0, 255)
		// Active Peds
		SET_TEXT_SCALE(0.5, 0.5)
		SET_TEXT_COLOUR(0, 255, 20, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.420, 0.035, "STRING", "Active Peds: ")
		SET_TEXT_SCALE(0.5, 0.5)
		SET_TEXT_COLOUR(0, 255, 20, 255)
		DISPLAY_TEXT_WITH_NUMBER(0.513, 0.036, "NUMBER", g_iCasinoActivePedTotal)
	ENDIF
	
	IF g_bActivityTests
		DEBUG_LOOP_THROUGH_PED_ACTIVITIES(casinoStruct, serverBD)
	ELSE
		g_iAnimationSequence = -1
	ENDIF
	
	IF g_bOutputSlotMachineChairCoords
		INT iSlotMachine
		INT iCurrentMachine
		REPEAT MAX_CASINO_SLOT_MACHINES iSlotMachine
			iCurrentMachine = iSlotMachine
			OBJECT_INDEX objSlotMachine = GET_CLOSEST_OBJECT_OF_TYPE(GET_SLOT_MACHINE_COORDS(iSlotMachine), 1.0, GET_SLOT_MACHINE_MODEL(iSlotMachine), FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(objSlotMachine)
			AND DOES_ENTITY_HAVE_DRAWABLE(objSlotMachine)
				VECTOR vBaseCoords = GET_ENTITY_BONE_POSTION(objSlotMachine, GET_ENTITY_BONE_INDEX_BY_NAME(objSlotMachine, "Chair_Base_01"))
				PRINTLN("[CHAIR_BONE] Slot Machine: ", iSlotMachine, " Base Coords: ", vBaseCoords)
			ELSE
				iSlotMachine = iCurrentMachine
			ENDIF
		ENDREPEAT
		g_bOutputSlotMachineChairCoords = FALSE
	ENDIF
	
	IF g_bOutputSlotMachineChairHeading
		INT iSlotMachine
		INT iCurrentMachine
		REPEAT MAX_CASINO_SLOT_MACHINES iSlotMachine
			iCurrentMachine = iSlotMachine
			OBJECT_INDEX objSlotMachine = GET_CLOSEST_OBJECT_OF_TYPE(GET_SLOT_MACHINE_COORDS(iSlotMachine), 1.0, GET_SLOT_MACHINE_MODEL(iSlotMachine), FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(objSlotMachine)
			AND DOES_ENTITY_HAVE_DRAWABLE(objSlotMachine)
				VECTOR vChairRotation = GET_ENTITY_BONE_ROTATION(objSlotMachine, GET_ENTITY_BONE_INDEX_BY_NAME(objSlotMachine, "Chair_Base_01"))
				PRINTLN("[CHAIR_BONE] Slot Machine: ", iSlotMachine, " Seat Heading: ", (vChairRotation.z-90.0))
			ELSE
				iSlotMachine = iCurrentMachine
			ENDIF
		ENDREPEAT
		g_bOutputSlotMachineChairHeading = FALSE
	ENDIF
	
	IF g_bOutputInsideTrackChairCoords
		INT iSeat
		INT iCurrentChair
		REPEAT MAX_CASINO_INSIDE_TRACK_SEATS iSeat
			iCurrentChair = iSeat
			OBJECT_INDEX objSeat = GET_CLOSEST_OBJECT_OF_TYPE(GET_INSIDE_TRACK_SEAT_COORDS(iSeat), 1.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("vw_prop_casino_track_chair_01")), FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(objSeat)
			AND DOES_ENTITY_HAVE_DRAWABLE(objSeat)
				VECTOR vSeatCoords = GET_ENTITY_BONE_POSTION(objSeat, GET_ENTITY_BONE_INDEX_BY_NAME(objSeat, "Chair_Base_01"))
				PRINTLN("[CHAIR_BONE] Inside Track Seat: ", iSeat, " Seat Coords: ", vSeatCoords)
			ELSE
				iSeat = iCurrentChair
			ENDIF
		ENDREPEAT
		g_bOutputInsideTrackChairCoords = FALSE
	ENDIF
	
	IF g_bOutputInsideTrackChairHeading
		INT iSeat
		INT iCurrentChair
		REPEAT MAX_CASINO_INSIDE_TRACK_SEATS iSeat
			iCurrentChair = iSeat
			OBJECT_INDEX objSeat = GET_CLOSEST_OBJECT_OF_TYPE(GET_INSIDE_TRACK_SEAT_COORDS(iSeat), 1.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("vw_prop_casino_track_chair_01")), FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(objSeat)
			AND DOES_ENTITY_HAVE_DRAWABLE(objSeat)
				VECTOR vSeatRotation = GET_ENTITY_BONE_ROTATION(objSeat, GET_ENTITY_BONE_INDEX_BY_NAME(objSeat, "Chair_Base_01"))
				PRINTLN("[CHAIR_BONE] Inside Track Seat: ", iSeat, " Seat Heading: ", (vSeatRotation-90.0))
			ELSE
				iSeat = iCurrentChair
			ENDIF
		ENDREPEAT
		g_bOutputInsideTrackChairHeading = FALSE
	ENDIF
	
	IF g_bOutputBlackjackChairCoords
		INT iSeat
		INT iTable
		INT iCurrentTable
		REPEAT MAX_CASINO_BLACKJACK_TABLES iTable
			iCurrentTable = iTable
			OBJECT_INDEX objTable = GET_CLOSEST_OBJECT_OF_TYPE(GET_BLACKJACK_TABLE_COORDS(iTable), 1.0, INT_TO_ENUM(MODEL_NAMES, GET_BLACKJACK_TABLE_HASH(iTable)), FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(objTable)
			AND DOES_ENTITY_HAVE_DRAWABLE(objTable)
				VECTOR vSeatCoords
				REPEAT MAX_CASINO_SEATS_PER_TABLE_GAME iSeat
					vSeatCoords = GET_ENTITY_BONE_POSTION(objTable, GET_ENTITY_BONE_INDEX_BY_NAME(objTable, GET_SEAT_BONE_NAME(iSeat)))
					PRINTLN("[CHAIR_BONE] Blackjack Table: ", iTable, " Seat: ", iSeat, " Seat Name: ", GET_SEAT_BONE_NAME(iSeat), " Seat Coords: ", vSeatCoords)
				ENDREPEAT
			ELSE
				iTable = iCurrentTable
			ENDIF
		ENDREPEAT
		g_bOutputBlackjackChairCoords = FALSE
	ENDIF
	
	IF g_bOutputBlackjackChairHeading
		INT iSeat
		INT iTable
		INT iCurrentTable
		REPEAT MAX_CASINO_BLACKJACK_TABLES iTable
			iCurrentTable = iTable
			OBJECT_INDEX objTable = GET_CLOSEST_OBJECT_OF_TYPE(GET_BLACKJACK_TABLE_COORDS(iTable), 1.0, INT_TO_ENUM(MODEL_NAMES, GET_BLACKJACK_TABLE_HASH(iTable)), FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(objTable)
			AND DOES_ENTITY_HAVE_DRAWABLE(objTable)
				VECTOR vSeatRotation
				REPEAT MAX_CASINO_SEATS_PER_TABLE_GAME iSeat
					vSeatRotation = GET_ENTITY_BONE_ROTATION(objTable, GET_ENTITY_BONE_INDEX_BY_NAME(objTable, GET_SEAT_BONE_NAME(iSeat)))
					PRINTLN("[CHAIR_BONE] Blackjack Table: ", iTable, " Seat: ", iSeat, " Seat Name: ", GET_SEAT_BONE_NAME(iSeat), " Seat Heading: ", (vSeatRotation.z-90.0))
				ENDREPEAT
			ELSE
				iTable = iCurrentTable
			ENDIF
		ENDREPEAT
		g_bOutputBlackjackChairHeading = FALSE
	ENDIF
	
	IF g_bOutputThreeCardPokerChairCoords
		INT iSeat
		INT iTable
		INT iCurrentTable
		REPEAT MAX_CASINO_THREE_CARD_POKER_TABLES iTable
			iCurrentTable = iTable
			OBJECT_INDEX objTable = GET_CLOSEST_OBJECT_OF_TYPE(GET_THREE_CARD_POKER_TABLE_COORDS(iTable), 1.0, INT_TO_ENUM(MODEL_NAMES, GET_THREE_CARD_POKER_TABLE_HASH(iTable)), FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(objTable)
			AND DOES_ENTITY_HAVE_DRAWABLE(objTable)
				VECTOR vSeatCoords
				REPEAT MAX_CASINO_SEATS_PER_TABLE_GAME iSeat
					vSeatCoords = GET_ENTITY_BONE_POSTION(objTable, GET_ENTITY_BONE_INDEX_BY_NAME(objTable, GET_SEAT_BONE_NAME(iSeat)))
					PRINTLN("[CHAIR_BONE] Three Card Poker Table: ", iTable, " Seat: ", iSeat, " Seat Name: ", GET_SEAT_BONE_NAME(iSeat), " Seat Coords: ", vSeatCoords)
				ENDREPEAT
			ELSE
				iTable = iCurrentTable
			ENDIF
		ENDREPEAT
		g_bOutputThreeCardPokerChairCoords = FALSE
	ENDIF
	
	IF g_bOutputThreeCardPokerChairHeading
		INT iSeat
		INT iTable
		INT iCurrentTable
		REPEAT MAX_CASINO_THREE_CARD_POKER_TABLES iTable
			iCurrentTable = iTable
			OBJECT_INDEX objTable = GET_CLOSEST_OBJECT_OF_TYPE(GET_THREE_CARD_POKER_TABLE_COORDS(iTable), 1.0, INT_TO_ENUM(MODEL_NAMES, GET_THREE_CARD_POKER_TABLE_HASH(iTable)), FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(objTable)
			AND DOES_ENTITY_HAVE_DRAWABLE(objTable)
				VECTOR vSeatRotation
				REPEAT MAX_CASINO_SEATS_PER_TABLE_GAME iSeat
					vSeatRotation = GET_ENTITY_BONE_ROTATION(objTable, GET_ENTITY_BONE_INDEX_BY_NAME(objTable, GET_SEAT_BONE_NAME(iSeat)))
					PRINTLN("[CHAIR_BONE] Three Card Poker Table: ", iTable, " Seat: ", iSeat, " Seat Name: ", GET_SEAT_BONE_NAME(iSeat), " Seat Heading: ", (vSeatRotation.z-90.0))
				ENDREPEAT
			ELSE
				iTable = iCurrentTable
			ENDIF
		ENDREPEAT
		g_bOutputThreeCardPokerChairHeading = FALSE
	ENDIF
	
	IF g_bOutputRouletteChairCoords
		INT iSeat
		INT iTable
		INT iCurrentTable
		REPEAT MAX_CASINO_ROULETTE_TABLES iTable
			iCurrentTable = iTable
			OBJECT_INDEX objTable = GET_CLOSEST_OBJECT_OF_TYPE(GET_ROULETTE_TABLE_COORDS(iTable), 1.0, INT_TO_ENUM(MODEL_NAMES, GET_ROULETTE_TABLE_HASH(g_iPedReservedRouletteTable)), FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(objTable)
			AND DOES_ENTITY_HAVE_DRAWABLE(objTable)
				VECTOR vSeatCoords
				REPEAT MAX_CASINO_SEATS_PER_TABLE_GAME iSeat
					vSeatCoords = GET_ENTITY_BONE_POSTION(objTable, GET_ENTITY_BONE_INDEX_BY_NAME(objTable, GET_SEAT_BONE_NAME(iSeat)))
					PRINTLN("[CHAIR_BONE] Roulette Table: ", iTable, " Seat: ", iSeat, " Seat Name: ", GET_SEAT_BONE_NAME(iSeat), " Seat Coords: ", vSeatCoords)
				ENDREPEAT
			ELSE
				iTable = iCurrentTable
			ENDIF
		ENDREPEAT
		g_bOutputRouletteChairCoords = FALSE
	ENDIF
	
	IF g_bOutputRouletteChairHeading
		INT iSeat
		INT iTable
		INT iCurrentTable
		REPEAT MAX_CASINO_ROULETTE_TABLES iTable
			iCurrentTable = iTable
			OBJECT_INDEX objTable = GET_CLOSEST_OBJECT_OF_TYPE(GET_ROULETTE_TABLE_COORDS(iTable), 1.0, INT_TO_ENUM(MODEL_NAMES, GET_ROULETTE_TABLE_HASH(g_iPedReservedRouletteTable)), FALSE, FALSE, FALSE)
			
			IF DOES_ENTITY_EXIST(objTable)
			AND DOES_ENTITY_HAVE_DRAWABLE(objTable)
				VECTOR vSeatRotation
				REPEAT MAX_CASINO_SEATS_PER_TABLE_GAME iSeat
					vSeatRotation = GET_ENTITY_BONE_ROTATION(objTable, GET_ENTITY_BONE_INDEX_BY_NAME(objTable, GET_SEAT_BONE_NAME(iSeat)))
					PRINTLN("[CHAIR_BONE] Roulette Table: ", iTable, " Seat: ", iSeat, " Seat Name: ", GET_SEAT_BONE_NAME(iSeat), " Seat Heading: ", (vSeatRotation.z-90.0))
				ENDREPEAT
			ELSE
				iTable = iCurrentTable
			ENDIF
		ENDREPEAT
		g_bOutputRouletteChairHeading = FALSE
	ENDIF
	
	INT iPedID
	FOR iPedID = 39 TO 41
		INT iInsideTrackPedID = GET_INSIDE_TRACK_PED_PRIORITY_ID(iPedID)
		IF IS_INSIDE_TRACK_PED_PRIORITY_ID_VALID(iInsideTrackPedID)
			IF IS_BIT_SET(g_iInsideTrackPedsBS[iInsideTrackPedID], ciINSIDE_TRACK_PEDS_RACE_IS_UPCOMING)
				IF NOT g_bCasinoInsideTrackRaceUpComing[iInsideTrackPedID]
					g_bCasinoInsideTrackRaceUpComing[iInsideTrackPedID] = TRUE
				ENDIF
			ELSE
				IF g_bCasinoInsideTrackRaceUpComing[iInsideTrackPedID]
					g_bCasinoInsideTrackRaceUpComing[iInsideTrackPedID] = FALSE
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_iInsideTrackPedsBS[iInsideTrackPedID], ciINSIDE_TRACK_PEDS_RACE_IN_PROGRESS)
				IF NOT g_bCasinoInsideTrackRaceInProgress[iInsideTrackPedID]
					g_bCasinoInsideTrackRaceInProgress[iInsideTrackPedID] = TRUE
				ENDIF
			ELSE
				IF g_bCasinoInsideTrackRaceInProgress[iInsideTrackPedID]
					g_bCasinoInsideTrackRaceInProgress[iInsideTrackPedID] = FALSE
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_iInsideTrackPedsBS[iInsideTrackPedID], ciINSIDE_TRACK_PEDS_RACE_COMPLETE)
				IF NOT g_bCasinoInsideTrackRaceFinished[iInsideTrackPedID]
					g_bCasinoInsideTrackRaceFinished[iInsideTrackPedID] = TRUE
				ENDIF
			ELSE
				IF g_bCasinoInsideTrackRaceFinished[iInsideTrackPedID]
					g_bCasinoInsideTrackRaceFinished[iInsideTrackPedID] = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF g_bValidateHighlightedPedAnims
		IF casinoStruct.iCasinoPedToHighlight >= 0
			g_iCasinoActivitySlot[casinoStruct.iCasinoPedToHighlight] = g_iValidateHighlightedPedAnimID
			
			CASINO_AREA eArea = casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].eAssignedArea
			INT iLayout = serverBD.iPedLayout[ENUM_TO_INT(eArea)]
			CASINO_ACTIVITY_SLOTS eActivity = GET_CASINO_PED_ACTIVITY_SLOT(casinoStruct.iCasinoPedToHighlight, iLayout, eArea)
			
			STRING sPedAnimDict
			TEXT_LABEL_63 tlPedAnimClip
			TEXT_LABEL_63 tlPropAnimDict
			TEXT_LABEL_63 tlPropAnimClip
			
			GET_CASINO_SEQUENCE_ANIM_DATA(eActivity, casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence, IS_CASINO_PED_FEMALE(casinoStruct.iCasinoPedToHighlight, iLayout, eArea), casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight], tlPedAnimClip, tlPropAnimDict, tlPropAnimClip)
			sPedAnimDict = casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].sPedAnimDict
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sPedAnimDict)
				REQUEST_ANIM_DICT(sPedAnimDict)
				IF HAS_ANIM_DICT_LOADED(sPedAnimDict)
					TASK_PLAY_ANIM_ADVANCED(casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].PedID, sPedAnimDict, tlPedAnimClip, casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].vSpawnCoords, <<0,0,casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].fHeading>>, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, DEFAULT, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_TURN_OFF_COLLISION, 0.0, DEFAULT, AIK_DISABLE_ARM_IK | AIK_DISABLE_LEG_IK | AIK_DISABLE_HEAD_IK | AIK_DISABLE_TORSO_IK | AIK_DISABLE_TORSO_REACT_IK)
					REMOVE_ANIM_DICT(sPedAnimDict)
					IF g_iValidateHighlightedPedAnimID+1 != ENUM_TO_INT(CASINO_AC_SLOT_TOTAL)
						g_iValidateHighlightedPedAnimID++
					ELSE
						g_iValidateHighlightedPedAnimID = 0
						g_bValidateHighlightedPedAnims = FALSE
					ENDIF
				ENDIF
			ELSE
				IF g_iValidateHighlightedPedAnimID+1 != ENUM_TO_INT(CASINO_AC_SLOT_TOTAL)
					g_iValidateHighlightedPedAnimID++
				ELSE
					g_iValidateHighlightedPedAnimID = 0
					g_bValidateHighlightedPedAnims = FALSE
				ENDIF
			ENDIF
		ELSE
			g_bValidateHighlightedPedAnims = FALSE
		ENDIF
	ENDIF
	
	IF g_bSetHighlightedPedAnimValidation
		g_iValidateHighlightedPedAnimID = g_iValidateHighlightedPedStartingIndex
		g_bValidateHighlightedPedAnims = FALSE
		g_bSetHighlightedPedAnimValidation = FALSE
	ENDIF
	
	IF g_bCreateCasinoChip
		IF NOT DOES_ENTITY_EXIST(g_objCasinoChipDebug)
			g_objCasinoChipDebug = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("vw_prop_chip_100dollar_x1")), GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ROULETTE_TABLE_COORDS(g_iPedReservedRouletteTable), GET_ROULETTE_TABLE_HEADING(g_iPedReservedRouletteTable), g_vCasinoChipOffset), FALSE, FALSE)
		ENDIF
		g_bCreateCasinoChip = FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_objCasinoChipDebug)
		SET_ENTITY_COORDS(g_objCasinoChipDebug, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ROULETTE_TABLE_COORDS(g_iPedReservedRouletteTable), GET_ROULETTE_TABLE_HEADING(g_iPedReservedRouletteTable), g_vCasinoChipOffset))
	ENDIF
	
	IF g_bDeleteCasinoChip
		SAFE_DELETE_OBJECT(g_objCasinoChipDebug)
		g_bDeleteCasinoChip = FALSE
	ENDIF
	
	IF g_bBroadcast_casino_peds_speech_data
		BROADCAST_CASINO_PEDS_SPEECH_DATA(
				INT_TO_ENUM(CASINO_PED_SPEECH_ENUM, g_iBroadcast_casino_peds_speech_enum),
				GET_CASINO_PED_SPEECH_CONTEXT(
						GET_CASINO_PED_TYPE(g_iBroadcast_casino_peds_speech_ped, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[g_iBroadcast_casino_peds_speech_ped].eAssignedArea)], casinoStruct.activityPeds[g_iBroadcast_casino_peds_speech_ped].eAssignedArea),
						INT_TO_ENUM(CASINO_PED_SPEECH_ENUM, g_iBroadcast_casino_peds_speech_enum),
						GET_CASINO_PED_ACTIVITY_SLOT(g_iBroadcast_casino_peds_speech_ped, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[g_iBroadcast_casino_peds_speech_ped].eAssignedArea)], casinoStruct.activityPeds[g_iBroadcast_casino_peds_speech_ped].eAssignedArea)),
				g_iBroadcast_casino_peds_speech_ped,
				ENUM_TO_INT(casinoStruct.activityPeds[g_iBroadcast_casino_peds_speech_ped].eAssignedArea))
		g_bBroadcast_casino_peds_speech_data = FALSE
	ENDIF
	
	IF g_bSetRoulettePreset
		CLEANUP_ROULETTE_FROM_CULLING(casinoStruct)
		casinoStruct.iRoulettePreset = g_iRoulettePreset
		g_bSetRoulettePreset = FALSE
	ENDIF
	
	IF g_bSetBlackjackPreset
		CLEANUP_BLACKJACK_FROM_CULLING(casinoStruct)
		SET_CASINO_PED_BJACK_STATE(casinoStruct, CASINO_PED_BJACK_STATE_INIT)
		casinoStruct.iBlackjackPreset = g_iBlackjackPreset
		g_bSetBlackjackPreset = FALSE
	ENDIF
	
	IF g_bCasinoRouletteCycleAnims
		IF casinoStruct.iCasinoPedToHighlight >= 0
		AND IS_ENTITY_ALIVE(casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].PedID)
			
			FLOAT fAnimPhase = 0.0
			INT iLayout = serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].eAssignedArea)]
			
			// Init activity & clip
			IF NOT g_bCasinoRouletteSetInitClip
				casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].eActiveActivity = DEBUG_GET_ROULETTE_ACTIVITY_FROM_WIDGET()
				g_iCasinoActivitySlot[casinoStruct.iCasinoPedToHighlight] = g_iCasinoRouletteActivity
				
				g_iCasinoRouletteBettingClip = ENUM_TO_INT(ROULETTE_PED_ANIM_STATE_PLACE_BET_ZONE_1_V1)
				g_bCasinoRouletteSetInitClip = TRUE
			ELSE
				// Clip phase
				IF g_bCasinoRouletteInitAnimPlaying
					fAnimPhase = GET_PED_ANIM_PHASE(casinoStruct.iCasinoPedToHighlight, casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence, casinoStruct)
				ENDIF
			ENDIF
			
			// Update if first anim or end of current anim
			IF NOT g_bCasinoRouletteInitAnimPlaying
			OR (g_bCasinoRouletteInitAnimPlaying AND fAnimPhase > 0.96)
			OR g_bCasinoRouletteProcessingAnim
				g_bCasinoRouletteProcessingAnim = TRUE
				
				casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].iCurrentSequence = g_iCasinoRouletteBettingClip
				casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].bHasPedAnimStarted = FALSE
				
				BOOL bFemalePed = FALSE
				IF g_iCasinoRoulettePedGender = 1
					bFemalePed = TRUE
				ENDIF
				
				// Play anim
				IF RUN_CASINO_PED_ANIM(casinoStruct.iCasinoPedToHighlight, iLayout, casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight] #IF IS_DEBUG_BUILD, TRUE, bFemalePed #ENDIF)
					g_bCasinoRouletteInitAnimPlaying = TRUE
					g_bCasinoRouletteProcessingAnim = FALSE
					
					g_iCasinoRouletteBettingClip++
					IF g_iCasinoRouletteBettingClip = ENUM_TO_INT(ROULETTE_PED_ANIM_STATE_COLLECT_CHIPS_SCOOP_V2)
						g_iCasinoRouletteBettingClip = ENUM_TO_INT(ROULETTE_PED_ANIM_STATE_PLACE_BET_ZONE_1_V2)
					ELIF g_iCasinoRouletteBettingClip > ENUM_TO_INT(ROULETTE_PED_ANIM_STATE_PLACE_BET_ZONE_3_V2)
						g_bCasinoRouletteCycleAnims = FALSE
						g_bCasinoRouletteSetInitClip = FALSE
					ENDIF
					
					IF g_iCasinoRouletteBettingClip >= ENUM_TO_INT(ROULETTE_PED_ANIM_STATE_PLACE_BET_ZONE_2_V2)
						casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].tlDebugAnimClipName += "_V2"
					ELSE
						casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].tlDebugAnimClipName += "_V1"
					ENDIF
				ENDIF
			ENDIF
		ELSE
			g_bCasinoRouletteCycleAnims = FALSE
			g_bCasinoRouletteSetInitClip = FALSE
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Ensures no conflicts exist between special ped scenarios
///    E.g. Using the widget to update Caleb to play blackjack when blackjack is already in use by another special ped
PROC DEBUG_REMOVE_SPECIAL_PED_CONFLICTS()
	
	INT iScenario
	INT iSpecialPedLoop
	CASINO_SPECIAL_PED_AREA eScenario = CASINO_SPECIAL_PED_AREA_INVALID
	CASINO_SPECIAL_PED_NAME eSpecialPedID = CASINO_SPECIAL_PED_NAME_INVALID
	
	// Get special ped scenario selected from widget
	REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
		CASINO_SPECIAL_PED_NAME eSpecialPed = DEBUG_GET_CASINO_SPECIAL_PED_TYPE(iSpecialPedLoop)
		IF DEBUG_PROCESS_SPECIAL_PED_WIDGET(eSpecialPed)
			iScenario = -1
			IF g_bCasinoSpecialPedScenarios[iSpecialPedLoop*3]
				iScenario = 0
			ELIF g_bCasinoSpecialPedScenarios[(iSpecialPedLoop*3)+1]
				iScenario = 1
			ELIF g_bCasinoSpecialPedScenarios[(iSpecialPedLoop*3)+2]
				iScenario = 2
			ENDIF
			
			IF iScenario != -1
				eScenario = GET_CASINO_SPECIAL_PED_SCENARIO(iScenario)
				eSpecialPedID = eSpecialPed
				BREAKLOOP
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Make sure no other special ped have the selected scenario set
	IF (eScenario != CASINO_SPECIAL_PED_AREA_INVALID)
		REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
			CASINO_SPECIAL_PED_NAME eSpecialPed = DEBUG_GET_CASINO_SPECIAL_PED_TYPE(iSpecialPedLoop)
			IF DEBUG_PROCESS_SPECIAL_PED_WIDGET(eSpecialPed)
			AND eSpecialPed != eSpecialPedID
				IF GET_CASINO_SPECIAL_PED_SCENARIO(iSpecialPedLoop*3) = eScenario
					g_bCasinoSpecialPedScenarios[(iSpecialPedLoop*3)] = FALSE
					g_bCasinoSpecialPedScenarios[(iSpecialPedLoop*3)+1] = TRUE
					g_bCasinoSpecialPedScenarios[(iSpecialPedLoop*3)+2] = FALSE
					
				ELIF GET_CASINO_SPECIAL_PED_SCENARIO((iSpecialPedLoop*3)+1) = eScenario
					g_bCasinoSpecialPedScenarios[(iSpecialPedLoop*3)] = FALSE
					g_bCasinoSpecialPedScenarios[(iSpecialPedLoop*3)+1] = FALSE
					g_bCasinoSpecialPedScenarios[(iSpecialPedLoop*3)+2] = TRUE
				
				ELIF GET_CASINO_SPECIAL_PED_SCENARIO((iSpecialPedLoop*3)+2) = eScenario
					g_bCasinoSpecialPedScenarios[(iSpecialPedLoop*3)] = TRUE
					g_bCasinoSpecialPedScenarios[(iSpecialPedLoop*3)+1] = FALSE
					g_bCasinoSpecialPedScenarios[(iSpecialPedLoop*3)+2] = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC UPDATE_DEBUG_CASINO_SPECIAL_PED_DATA(SERVER_BROADCAST_DATA &serverBD, CASINO_STRUCT &casinoStruct)
	
	INT iSpecialPedLoop
	REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
		CASINO_SPECIAL_PED_NAME eSpecialPed = DEBUG_GET_CASINO_SPECIAL_PED_TYPE(iSpecialPedLoop)
		IF eSpecialPed != CASINO_SPECIAL_PED_NAME_INVALID
			g_bCasinoSpecialPedAlive[iSpecialPedLoop] = IS_ENTITY_ALIVE(casinoStruct.activityPeds[g_sSpecialPeds[eSpecialPed].iPedID].PedID)
			IF DOES_TEXT_WIDGET_EXIST(g_twCasinoSpecialPedAssignedArea[iSpecialPedLoop])
				SET_CONTENTS_OF_TEXT_WIDGET(g_twCasinoSpecialPedAssignedArea[iSpecialPedLoop], GET_CASINO_AREA_NAME(casinoStruct.activityPeds[g_sSpecialPeds[eSpecialPed].iPedID].eAssignedArea))
			ENDIF
			IF DOES_TEXT_WIDGET_EXIST(g_twCasinoSpecialPedActiveScenario[iSpecialPedLoop])
				SET_CONTENTS_OF_TEXT_WIDGET(g_twCasinoSpecialPedActiveScenario[iSpecialPedLoop], g_tlCasinoSpecialPedScenario[iSpecialPedLoop])
			ENDIF
			
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			AND g_bCasinoSpecialPedUpdateScenario[eSpecialPed]
				RECREATE_CASINO_PEDS()
				serverBD.bServerDataInitialised = FALSE
				CLEAR_CASINO_PED_BIT(casinoStruct, BS_CASINO_PED_GLOBAL_PED_DATA_SET)
				DEBUG_REMOVE_SPECIAL_PED_CONFLICTS()
				g_bCasinoSpecialPedUpdateScenario[eSpecialPed] = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC UPDATE_CASINO_PED_EDITOR_WIDGET(CASINO_STRUCT &casinoStruct, SERVER_BROADCAST_DATA serverBD)
	INT iPedID
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_O, KEYBOARD_MODIFIER_CTRL, "Recreate Casino Peds")
	OR (g_bRecreateCasinoPeds)
		RECREATE_CASINO_PEDS()
		g_bRecreateCasinoPeds = FALSE
	ENDIF
	
	IF g_bWarpPlayerToHighlightedPed
		IF (casinoStruct.iCasinoPedToHighlight >= 0 AND casinoStruct.iCasinoPedToHighlight < MAX_NUMBER_OF_CASINO_PEDS)
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_ENTITY_ALIVE(casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].PedID)
				VECTOR vHighlightedPedCoords = GET_ENTITY_COORDS(casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].PedID)
				FLOAT vHighlightedPedHeading = GET_ENTITY_HEADING(casinoStruct.activityPeds[casinoStruct.iCasinoPedToHighlight].PedID)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vHighlightedPedCoords)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), vHighlightedPedHeading)
			ENDIF
		ENDIF
		g_bWarpPlayerToHighlightedPed = FALSE
	ENDIF
	
	IF g_bSetCasinoPedLayout
		g_iCasinoPedLayouts[g_iCasinoPedLayoutArea] = (g_iCasinoPedLayoutType+1)
		REPEAT MAX_NUMBER_OF_CASINO_PEDS iPedID
			// Clear current data and then set
			// Layout 3 doesn't have as many peds so data from layout 1 and 2 will remain if not cleared
			CLEAR_CASINO_PED_DATA(iPedID)
			DEBUG_SET_CASINO_PED_DATA(iPedID, casinoStruct)
		ENDREPEAT
		RECREATE_CASINO_PEDS()
		g_bSetCasinoPedLayout = FALSE
	ENDIF
	
	TEXT_LABEL_63 tlPedAnimClip
	TEXT_LABEL_63 tlPropAnimDict
	TEXT_LABEL_63 tlPropAnimClip
	
	REPEAT MAX_NUMBER_OF_CASINO_PEDS iPedID
		IF DOES_TEXT_WIDGET_EXIST(casinoStruct.twAssignedArea[iPedID])
			SET_CONTENTS_OF_TEXT_WIDGET(casinoStruct.twAssignedArea[iPedID], GET_CASINO_AREA_NAME(casinoStruct.activityPeds[iPedID].eAssignedArea))
		ENDIF
		IF DOES_TEXT_WIDGET_EXIST(tdCasinoPedAnimName[iPedID])
			BOOL bFemalePed = IS_CASINO_PED_FEMALE(iPedID, serverBD.iPedLayout[ENUM_TO_INT(casinoStruct.activityPeds[iPedID].eAssignedArea)], casinoStruct.activityPeds[iPedID].eAssignedArea)
			GET_CASINO_SEQUENCE_ANIM_DATA(casinoStruct.activityPeds[iPedID].eActiveActivity, casinoStruct.activityPeds[iPedID].iCurrentSequence, bFemalePed, casinoStruct.activityPeds[iPedID], tlPedAnimClip, tlPropAnimDict, tlPropAnimClip)
			SET_CONTENTS_OF_TEXT_WIDGET(tdCasinoPedAnimName[iPedID], tlPedAnimClip)
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC MAINTAIN_CASINO_PED_WIDGETS(CASINO_STRUCT &casinoStruct, SERVER_BROADCAST_DATA &serverBD)
	IF NOT casinoStruct.bDebugWidgetsCreated
		EXIT
	ENDIF
		
	UPDATE_CASINO_WIDGETS(serverBD, casinoStruct)
	UPDATE_DEBUG_CASINO_SPECIAL_PED_DATA(serverBD, casinoStruct)
	DEBUG_DISPLAY_CASINO_PED_DEBUG(serverBD, casinoStruct)
	
	IF IS_CASINO_PED_DEBUG_EDITOR_ACTIVE()
		UPDATE_CASINO_PED_EDITOR_WIDGET(casinoStruct, serverBD)
		UPDATE_DEBUG_CASINO_PED_DATA(casinoStruct)
		
		OUTPUT_DEBUG_CASINO_PED_DATA()
		OUTPUT_DEBUG_CASINO_PED_COMPONENT_VARIATIONS(casinoStruct, serverBD)
		
		IF g_bOutputCreatedPedDetails
			OUTPUT_CREATED_PED_DETAILS()
			g_bOutputCreatedPedDetails = FALSE
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ DEBUG WIDGETS ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_CASINO_PED_WIDGETS(CASINO_STRUCT &casinoStruct, SERVER_BROADCAST_DATA &serverBD)
	
	INT iPedID
	INT iCasinoArea
	TEXT_LABEL_63 str
	
	START_WIDGET_GROUP("AM_CASINO_PEDS")
		ADD_WIDGET_BOOL("Kill Script", g_bKillCasinoPedScript)
		START_WIDGET_GROUP("AM_CASINO_PEDS - EDITOR")
			ADD_WIDGET_BOOL("Activate Editor", g_bCasinoPedEditorActive)
			ADD_WIDGET_BOOL("Reposition Peds", g_bCasinoPedWidgetRepositionPeds)
			ADD_WIDGET_BOOL("Kill Ped Logic", g_bKillCasinoPedLogic)
			ADD_WIDGET_BOOL("Output Data", g_bOutputCasinoPedSettings)
			ADD_WIDGET_BOOL("Output Clothing Data For Current Area", g_bOutputCasinoPedClothingSettings)
			ADD_WIDGET_BOOL("Recreate Peds", g_bRecreateCasinoPeds)
			ADD_WIDGET_STRING("Recreate Peds Debug Keys: CTRL+O")
			
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Off")
				ADD_TO_WIDGET_COMBO("Ped Info")
				ADD_TO_WIDGET_COMBO("Speech Info")
				ADD_TO_WIDGET_COMBO("Ped Roulette Info")
			STOP_WIDGET_COMBO("Editor State", casinoStruct.iDrawStateDebug)
			ADD_WIDGET_STRING("Editor State Debug Keys: CTRL+SHIFT+ENTER")
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Highlight Ped", casinoStruct.iCasinoPedToHighlight, -2, MAX_NUMBER_OF_CASINO_PEDS, 1)
			ADD_WIDGET_BOOL("Warp Player To Highlighted Ped", g_bWarpPlayerToHighlightedPed)
			ADD_WIDGET_STRING("Warp will only succeed if hightlighted player is alive")
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Validation Starting Index", g_iValidateHighlightedPedStartingIndex, 0, ENUM_TO_INT(CASINO_AC_SLOT_TOTAL), 1)
			ADD_WIDGET_BOOL("Set Highlighted Ped Anim Starting Index", g_bSetHighlightedPedAnimValidation)
			ADD_WIDGET_BOOL("Validate Highlighted Ped Anims", g_bValidateHighlightedPedAnims)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Display Active Ped Total ", g_bCasinoDisplayActivePedTotal)
			ADD_WIDGET_STRING("")
			
			ADD_WIDGET_BOOL("Activate Animation looping ", g_bActivityTests)
			
			START_WIDGET_GROUP("Layouts")
				START_NEW_WIDGET_COMBO()
					REPEAT MAX_CASINO_AREAS iCasinoArea
						ADD_TO_WIDGET_COMBO(GET_CASINO_AREA_NAME(INT_TO_ENUM(CASINO_AREA, iCasinoArea)))
					ENDREPEAT
				STOP_WIDGET_COMBO("Layout Area", g_iCasinoPedLayoutArea)
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("1")
					ADD_TO_WIDGET_COMBO("2")
					ADD_TO_WIDGET_COMBO("3")
				STOP_WIDGET_COMBO("Layout", g_iCasinoPedLayoutType)
				ADD_WIDGET_BOOL("Set Layout", g_bSetCasinoPedLayout)
				ADD_WIDGET_STRING("")
				
				ADD_WIDGET_INT_READ_ONLY("Transition Layout: ", g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_PERMANENT)])
				ADD_WIDGET_INT_READ_ONLY("Main Floor Layout: ", g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_MAIN_FLOOR)])
				ADD_WIDGET_INT_READ_ONLY("Table Games Layout: ", g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_TABLE_GAMES)])
				ADD_WIDGET_INT_READ_ONLY("Sports Betting Layout: ", g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_SPORTS_BETTING)])
				ADD_WIDGET_INT_READ_ONLY("Managers Office Layout: ", g_iCasinoPedLayouts[ENUM_TO_INT(CASINO_AREA_MANAGERS_OFFICE)])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Chair Bone Indexes")
				ADD_WIDGET_STRING("Search for [CHAIR_BONE] in logs.")
				ADD_WIDGET_STRING("If object has not been streamed in yet nothing will output.")
				START_WIDGET_GROUP("Coords")
					ADD_WIDGET_BOOL("Output Slot Machine Chair Coords", g_bOutputSlotMachineChairCoords)
					ADD_WIDGET_BOOL("Output Inside Track Chair Coords", g_bOutputInsideTrackChairCoords)
					ADD_WIDGET_BOOL("Output Blackjack Chair Coords", g_bOutputBlackjackChairCoords)
					ADD_WIDGET_BOOL("Output Three Card Poker Chair Coords", g_bOutputThreeCardPokerChairCoords)
					ADD_WIDGET_BOOL("Output Roulette Chair Coords", g_bOutputRouletteChairCoords)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Heading")
					ADD_WIDGET_BOOL("Output Slot Machine Chair Heading", g_bOutputSlotMachineChairHeading)
					ADD_WIDGET_BOOL("Output Inside Track Chair Heading", g_bOutputInsideTrackChairHeading)
					ADD_WIDGET_BOOL("Output Blackjack Chair Heading", g_bOutputBlackjackChairHeading)
					ADD_WIDGET_BOOL("Output Three Card Poker Chair Heading", g_bOutputThreeCardPokerChairHeading)
					ADD_WIDGET_BOOL("Output Roulette Chair Heading", g_bOutputRouletteChairHeading)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Inside Track")
				FOR iPedID = 39 TO 41
					str = "Ped ID: "
					str += iPedID
					START_WIDGET_GROUP(str)
						INT iInsideTrackPedID = GET_INSIDE_TRACK_PED_PRIORITY_ID(iPedID)
						IF IS_INSIDE_TRACK_PED_PRIORITY_ID_VALID(iInsideTrackPedID)
							ADD_WIDGET_BOOL("Race Up Coming", g_bCasinoInsideTrackRaceUpComing[iInsideTrackPedID])
							ADD_WIDGET_BOOL("Race In Progress", g_bCasinoInsideTrackRaceInProgress[iInsideTrackPedID])
							ADD_WIDGET_BOOL("Race Finished", g_bCasinoInsideTrackRaceFinished[iInsideTrackPedID])
						ENDIF
					STOP_WIDGET_GROUP()
					str = ""
				ENDFOR
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Roulette")
				START_WIDGET_GROUP("Casino Chips")
					ADD_WIDGET_BOOL("Create Casino Chip", g_bCreateCasinoChip)
					ADD_WIDGET_BOOL("Delete Casino Chip", g_bDeleteCasinoChip)
					ADD_WIDGET_VECTOR_SLIDER("Offset", g_vCasinoChipOffset, -10000.0, 10000.0, 0.01)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Preset")
					ADD_WIDGET_INT_SLIDER("Preset", g_iRoulettePreset, 0, 2, 1)
					ADD_WIDGET_STRING("Setting a different preset will restart ped roulette")
					ADD_WIDGET_BOOL("Set Preset", g_bSetRoulettePreset)
					ADD_WIDGET_INT_READ_ONLY("Current Preset: ", casinoStruct.iRoulettePreset)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Betting Anims")
					START_NEW_WIDGET_COMBO()
						ADD_TO_WIDGET_COMBO("S1_REG_01A")
						ADD_TO_WIDGET_COMBO("S1_REG_01B")
						ADD_TO_WIDGET_COMBO("S1_M_SLO_F_ENG_01A")
						ADD_TO_WIDGET_COMBO("S1_M_SLO_F_ENG_01B")
						ADD_TO_WIDGET_COMBO("S2_REG_02A")
						ADD_TO_WIDGET_COMBO("S2_REG_02B")
						ADD_TO_WIDGET_COMBO("S2_M_SLOU_W_DRK_F_REG_W_DRK_02A")
						ADD_TO_WIDGET_COMBO("S2_M_SLOU_W_DRK_F_REG_W_DRK_02B")
						ADD_TO_WIDGET_COMBO("S3_REG_03A")
						ADD_TO_WIDGET_COMBO("S3_REG_03B")
						ADD_TO_WIDGET_COMBO("S3_M_ENG_F_SLO_03A")
						ADD_TO_WIDGET_COMBO("S3_M_ENG_F_SLO_03B")
						ADD_TO_WIDGET_COMBO("S4_REG_04A")
						ADD_TO_WIDGET_COMBO("S4_REG_04B")
						ADD_TO_WIDGET_COMBO("S4_M_REG_W_DRK_F_SLO_W_DRK_04A")
						ADD_TO_WIDGET_COMBO("S4_M_REG_W_DRK_F_SLO_W_DRK_04B")
					STOP_WIDGET_COMBO("Activity", g_iCasinoRouletteActivity)
					START_NEW_WIDGET_COMBO()
						ADD_TO_WIDGET_COMBO("Male")
						ADD_TO_WIDGET_COMBO("Female")
					STOP_WIDGET_COMBO("Gender", g_iCasinoRoulettePedGender)
					ADD_WIDGET_BOOL("Cycle Anims", g_bCasinoRouletteCycleAnims)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Blackjack")
				START_WIDGET_GROUP("Preset")
					ADD_WIDGET_INT_SLIDER("Preset", g_iBlackjackPreset, 0, 2, 1)
					ADD_WIDGET_STRING("Setting a different preset will restart ped roulette")
					ADD_WIDGET_BOOL("Set Preset", g_bSetBlackjackPreset)
					ADD_WIDGET_INT_READ_ONLY("Current Preset: ", casinoStruct.iBlackjackPreset)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Active Peds")	
				REPEAT MAX_NUMBER_OF_CASINO_PEDS iPedID
					str = "ActivityPed["
					str += iPedID
					str += "]"
					
					START_WIDGET_GROUP(str)
						ADD_WIDGET_VECTOR_SLIDER("Position", g_vCasinoPedPos[iPedID], -9999.9, 9999.9, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("Heading", g_fCasinoPedHeading[iPedID], -360.0, 360.0, 0.1)
						ADD_WIDGET_STRING("")
						IF GET_COMMANDLINE_PARAM_EXISTS("sc_CasinoPedEditorAmbientActivities")
							ADD_WIDGET_INT_SLIDER("Activity", g_iCasinoActivitySlot[iPedID], -1, ENUM_TO_INT(CASINO_AC_SLOT_AMBIENT_TOTAL) -1, 1)
						ELSE
							ADD_WIDGET_INT_SLIDER("Activity", g_iCasinoActivitySlot[iPedID], -1, ENUM_TO_INT(CASINO_AC_SLOT_TOTAL) -1, 1)
							tdCasinoPedAnimName[iPedID] = ADD_TEXT_WIDGET("Anim Name: ")
							ADD_WIDGET_STRING("")
						ENDIF
						ADD_WIDGET_INT_SLIDER("Ped Type", g_iCasinoPedType[iPedID], -1, ENUM_TO_INT(MAX_NUMBER_OF_CASINO_PED_TYPES) -1, 1)
						casinoStruct.twAssignedArea[iPedID] = ADD_TEXT_WIDGET("Assigned Area: ")
					STOP_WIDGET_GROUP()
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Anim Dict Management")
				ADD_WIDGET_INT_READ_ONLY("Anim Dicts Requested", g_iAnimDictsRequestedPerFrame)
			STOP_WIDGET_GROUP()
			
			CREATE_CASINO_SPECIAL_PED_WIDGETS()
		STOP_WIDGET_GROUP()
			
		START_WIDGET_GROUP("Ped Speech")
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Off")
				ADD_TO_WIDGET_COMBO("Ped Info")
				ADD_TO_WIDGET_COMBO("Speech Info")
			STOP_WIDGET_COMBO("Editor State", casinoStruct.iDrawStateDebug)
			
			ADD_WIDGET_INT_READ_ONLY("Updated Total", casinoStruct.iPedsSpeechUpdatedTotal)
			ADD_WIDGET_INT_READ_ONLY("Updated This Frame", casinoStruct.iPedsSpeechUpdatedThisFrame)
			
			BOOL bStartedWidget = FALSE
			REPEAT COUNT_OF(g_iBSCasinoSpeech) iPedID
				IF SHOULD_CASINO_PED_PLAY_SPEECH(iPedID, casinoStruct, INT_TO_ENUM(CASINO_PED_TYPES, casinoStruct.activityPeds[iPedID].iCasinoPedType))
					IF NOT bStartedWidget
						START_WIDGET_GROUP("g_iBSCasinoSpeech[]")
						bStartedWidget = TRUE
					ENDIF
					ADD_WIDGET_INT_READ_ONLY(GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, casinoStruct.activityPeds[iPedID].iCasinoPedType)), g_iBSCasinoSpeech[iPedID])
				ENDIF
			ENDREPEAT
			IF bStartedWidget
				STOP_WIDGET_GROUP()
			ENDIF
			
			START_NEW_WIDGET_COMBO()
				REPEAT CASINO_SPEECH_MAX iPedID
					IF (INT_TO_ENUM(CASINO_PED_SPEECH_ENUM, iPedID) = CASINO_SPEECH_INVALID)
						ADD_TO_WIDGET_COMBO("invalid")
					ELSE
						ADD_TO_WIDGET_COMBO(GET_CASINO_PED_SPEECH_NAME(INT_TO_ENUM(CASINO_PED_SPEECH_ENUM, iPedID)))
					ENDIF
				ENDREPEAT
			STOP_WIDGET_COMBO("Broadcast Enum", g_iBroadcast_casino_peds_speech_enum)
			ADD_WIDGET_INT_SLIDER("Broadcast Ped", g_iBroadcast_casino_peds_speech_ped, 0, 125, 1)
			ADD_WIDGET_BOOL("Broadcast Speech", g_bBroadcast_casino_peds_speech_data)
			
			START_WIDGET_GROUP("Casino ped greet areas")
				ADD_WIDGET_FLOAT_SLIDER("DEFAULT greet area", tfCASINO_PED_DEFAULT_GREET_AREA, 0.0, 10.0, 0.1)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		CREATE_PATROL_PED_WIDGET(serverBD.sPatrolPedsBDData, casinoStruct.PatrolPedsLocalData)		
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC CREATE_AND_INITIALISE_CASINO_PED_WIDGETS(CASINO_STRUCT &casinoStruct, SERVER_BROADCAST_DATA &serverBD)
	IF casinoStruct.bCreateDebugWidgets
		INITIALISE_CASINO_PED_WIDGETS(casinoStruct, serverBD)
		CREATE_CASINO_PED_WIDGETS(casinoStruct, serverBD)
		casinoStruct.bCreateDebugWidgets = FALSE
		casinoStruct.bDebugWidgetsCreated = TRUE
	ENDIF
ENDPROC

#ENDIF // IS_DEBUG_BUILD
#ENDIF // FEATURE_CASINO
