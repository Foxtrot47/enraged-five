USING "globals.sch"
USING "net_include.sch"




//Purpose: Sets the bit on the MP decorator bitset on the ped passed
PROC SET_MP_DECORATOR_BIT(PLAYER_INDEX thePlayer, INT thisBit)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== Decorator === SET_MP_DECORATOR_BIT(", thisBit, ")")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	INT iBitSet = 0
	IF DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(thePlayer), "MPBitset")
		iBitSet = DECOR_GET_INT(GET_PLAYER_PED_SCRIPT_INDEX(thePlayer), "MPBitset")
	ENDIF
	SET_BIT(iBitSet, thisBit)
	DECOR_SET_INT(GET_PLAYER_PED_SCRIPT_INDEX(thePlayer), "MPBitset", iBitSet)
ENDPROC

//Purpose: Clears the bit on the MP decorator bitset on the ped passed
PROC CLEAR_MP_DECORATOR_BIT(PLAYER_INDEX thePlayer, INT thisBit)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== Decorator === CLEAR_MP_DECORATOR_BIT(", thisBit, ")")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	INT iBitSet = 0
	IF DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(thePlayer), "MPBitset")
		iBitSet = DECOR_GET_INT(GET_PLAYER_PED_SCRIPT_INDEX(thePlayer), "MPBitset")
	ENDIF
	CLEAR_BIT(iBitSet, thisBit)
	DECOR_SET_INT(GET_PLAYER_PED_SCRIPT_INDEX(thePlayer), "MPBitset", iBitSet)
ENDPROC

//Purpose: Returns if the bit passed has been set on the MP decorator of the ped passed
FUNC BOOL IS_MP_DECORATOR_BIT_SET(PLAYER_INDEX thePlayer, INT thisBit)
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(thePlayer), "MPBitset")
			IF IS_BIT_SET(DECOR_GET_INT(GET_PLAYER_PED_SCRIPT_INDEX(thePlayer), "MPBitset"), thisBit)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// Ped index copies of the above functions //////////////////////////////////////////////////////////////////

//Purpose: Sets the bit on the MP decorator bitset on the ped passed
PROC SET_PED_MP_DECORATOR_BIT(PED_INDEX thePed, INT thisBit)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "[PED_DEC] === Decorator === SET_PED_MP_DECORATOR_BIT(", thisBit, ")")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	INT iBitSet = 0
	IF DECOR_EXIST_ON(thePed, "MPBitset")
		iBitSet = DECOR_GET_INT(thePed, "MPBitset")
	ENDIF
	SET_BIT(iBitSet, thisBit)
	DECOR_SET_INT(thePed, "MPBitset", iBitSet)
ENDPROC

//Purpose: Clears the bit on the MP decorator bitset on the ped passed
PROC CLEAR_PED_MP_DECORATOR_BIT(PED_INDEX thePed, INT thisBit)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "[PED_DEC] === Decorator === CLEAR_PED_MP_DECORATOR_BIT(", thisBit, ")")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	INT iBitSet = 0
	IF DECOR_EXIST_ON(thePed, "MPBitset")
		iBitSet = DECOR_GET_INT(thePed, "MPBitset")
	ENDIF
	CLEAR_BIT(iBitSet, thisBit)
	DECOR_SET_INT(thePed, "MPBitset", iBitSet)
ENDPROC

//Purpose: Returns if the bit passed has been set on the MP decorator of the ped passed
FUNC BOOL IS_PED_MP_DECORATOR_BIT_SET(PED_INDEX thePed, INT thisBit)
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(thePed, "MPBitset")
			IF IS_BIT_SET(DECOR_GET_INT(thePed, "MPBitset"), thisBit)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////


//Purpose: Sets the bit on the attribute damage decorator bitset on the ped passed
PROC SET_ATTRIBUTE_DAMAGE_DECORATOR_BIT(ENTITY_INDEX thisEnt, INT thisBit)
	INT iBitSet = 0
	IF DECOR_EXIST_ON(thisEnt, "AttributeDamage")
		iBitSet = DECOR_GET_INT(thisEnt, "AttributeDamage")
	ENDIF
	SET_BIT(iBitSet, thisBit)
	DECOR_SET_INT(thisEnt, "AttributeDamage", iBitSet)
ENDPROC

//Purpose: Clears the bit on the attribute damage decorator bitset on the ped passed
PROC CLEAR_ATTRIBUTE_DAMAGE_DECORATOR_BIT(ENTITY_INDEX thisEnt, INT thisBit)
	INT iBitSet = 0
	IF DECOR_EXIST_ON(thisEnt, "AttributeDamage")
		iBitSet = DECOR_GET_INT(thisEnt, "AttributeDamage")
	ENDIF
	CLEAR_BIT(iBitSet, thisBit)
	DECOR_SET_INT(thisEnt, "AttributeDamage", iBitSet)
ENDPROC

//Purpose: Returns if the bit passed has been set on the attribute damage decorator of the ped passed
FUNC BOOL IS_ATTRIBUTE_DAMAGE_DECORATOR_BIT_SET(ENTITY_INDEX thisEnt, INT thisBit)
	IF DECOR_IS_REGISTERED_AS_TYPE("AttributeDamage", DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(thisEnt, "AttributeDamage")
			IF IS_BIT_SET(DECOR_GET_INT(thisEnt, "AttributeDamage"), thisBit)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

