USING "rage_builtins.sch"
USING "globals.sch"

USING "fmmc_cloud_loader.sch"	// POSSIBLY TEMP (6/6/13): For checking if another mission download is in progress

USING "net_heists_public.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Cloud_Mission_Loader_Public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains the public interface for the cloud_mission_loader script.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Calling Script Access Functions
// ===========================================================================================================

// PURPOSE:	Clear the global struct
PROC Clear_Cloud_Loaded_Mission_Data()

	g_structCloudMissionLoader emptyStruct
	g_sCloudMissionLoaderMP = emptyStruct
	
	g_sCloudMissionLoaderMP.cmlReturnMissionName = ""
	g_sCloudMissionLoaderMP.cmlReturnDescription = ""
	
	g_sCloudMissionLoaderMP.cmlOfflineID		= ""
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Relaunch the standalone loader script if it isn't in memory when required
//
// RETURN VALUE:		BOOL		TRUE if the loader script is available for use, FALSE if it is still being reloaded
//
// NOTES:	KGM 15/1/15: It now gets killed to save memory when on a Heist.
FUNC BOOL Is_Standalone_Loader_Script_Now_Available()

	IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("net_cloud_mission_loader")) > 0)
		RETURN TRUE
	ENDIF
	
	REQUEST_SCRIPT("net_cloud_mission_loader")
	IF (HAS_SCRIPT_LOADED("net_cloud_mission_loader"))
	    START_NEW_SCRIPT("net_cloud_mission_loader", FRIEND_STACK_SIZE)
	    SET_SCRIPT_AS_NO_LONGER_NEEDED("net_cloud_mission_loader")
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the download has just finished
//
// RETURN VALUE:			BOOL						TRUE if the cloud data has finished, otherwise FALSE
FUNC BOOL Check_For_Download_Now_Finished()

	IF (g_sCloudMissionLoaderMP.cmlDataLoadFinished)
		// Make a note of the 'successfully finished' flag so it can be set again after the data is cleared
		BOOL			loadedSuccessfully	= g_sCloudMissionLoaderMP.cmlDataLoadSuccessful
		INT				returnType			= g_sCloudMissionLoaderMP.cmlReturnType
		INT				returnSubtype		= g_sCloudMissionLoaderMP.cmlReturnSubtype
		TEXT_LABEL_63	returnMissionName	= g_sCloudMissionLoaderMP.cmlReturnMissionName
		TEXT_LABEL_63	returnDescription	= g_sCloudMissionLoaderMP.cmlReturnDescription
	
		// Clear the global struct
		Clear_Cloud_Loaded_Mission_Data()
		
		// Ensure the loaded flag gets set again and setup the 'load successful' flag so it can still be checked
		g_sCloudMissionLoaderMP.cmlDataLoadFinished		= FALSE
		g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= loadedSuccessfully
		
		// Return Data if relevant
		g_sCloudMissionLoaderMP.cmlReturnType			= returnType
		g_sCloudMissionLoaderMP.cmlReturnSubtype		= returnSubtype
		g_sCloudMissionLoaderMP.cmlReturnMissionName	= returnMissionName
		g_sCloudMissionLoaderMP.cmlReturnDescription	= returnDescription
		
		#IF IS_DEBUG_BUILD
			IF (g_sCloudMissionLoaderMP.cmlDataLoadSuccessful)
				PRINTSTRING("...KGM MP [CloudLoader]: Requester Script informed that the cloud-loaded data is now available")	PRINTNL()
				PRINTSTRING("      RETURN VALUES (if relevant): ")																PRINTNL()
				PRINTSTRING("         Type        : ") PRINTINT(g_sCloudMissionLoaderMP.cmlReturnType)							PRINTNL()
				PRINTSTRING("         Subtype     : ") PRINTINT(g_sCloudMissionLoaderMP.cmlReturnSubtype)						PRINTNL()
				PRINTSTRING("         Mission Name: ") PRINTSTRING(g_sCloudMissionLoaderMP.cmlReturnMissionName)				PRINTNL()
				PRINTSTRING("         Description : ") PRINTSTRING(g_sCloudMissionLoaderMP.cmlReturnDescription)				PRINTNL()
			ELSE
				PRINTSTRING("...KGM MP [CloudLoader]: Requester Script informed that the cloud-loaded data load has ended. Download failed. Requester needs to check for this.") PRINTNL()
			ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF

	// Not finished
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a new download request can start
//
// RETURN VALUE:			BOOL						TRUE if a new one can start, FALSE if there is some other download in progress
FUNC BOOL Can_A_New_Download_Request_Start()
	
	// KGM TEMP 6/6/13: Delay if there is another cloud-load in progress (TEMP because when we get concurrent downloads this won;t be a problem)
	IF (IS_CORONA_DOWNLOADING_FROM_UGC_SERVER())
		#IF IS_DEBUG_BUILD
			PRINTSTRING("...KGM MP [CloudLoader]: DELAY DOWNLOAD - A download is already in progress (TEMP Until we get DUAL DOWNLOAD).")
			PRINTNL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// KGM TEMP 10/7/13: Delay if there is a cloud refresh download in progress - this seems to cause issues
	IF (Is_Mission_At_Coords_Cloud_Loaded_Data_Being_Downloaded())
		#IF IS_DEBUG_BUILD
			PRINTSTRING("...KGM MP [CloudLoader]: DELAY DOWNLOAD - A Refresh Mission Download is still in progress (TEMP Until we get DUAL DOWNLOAD).")
			PRINTNL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Delay this we are getting by content ID
	IF (ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID())
		#IF IS_DEBUG_BUILD
			PRINTSTRING("      - DELAY REQUEST: Mission Data Download Request Still Active. - ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID")
			PRINTNL()		
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// New download can start
	RETURN TRUE
		
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Mission Data Download functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Allows a request for the full mission data to be downloaded form the cloud (originally added for mission triggerer)
//
// INPUT PARAMS:			paramMissionIdData			The MissionID data
//							paramSpecificOfflineID		Fill this with an offline contentID if a specific piece of offline content is needed as an alternative to the cloud contentID
//							paramUseSpecificOfflineID	TRUE if a specific piece of offline content should be used and the string should be filled, FALSE to treat the string as empty
//							paramClearPrevData			[DEFAULT = TRUE] TRUE if the previous UGC data should get cleared, FALSE if it should be retained to prevent a duplicate download
// RETURN VALUE:			BOOL						TRUE if the cloud data is loaded, otherwise FALSE
FUNC BOOL Load_Cloud_Loaded_Mission_Data(MP_MISSION_ID_DATA paramMissionIdData, TEXT_LABEL_31 paramSpecificOfflineID, BOOL paramUseSpecificOfflineID, BOOL paramClearPrevData = TRUE)

	// Ensure the offlineID variables is consistent
	IF (paramUseSpecificOfflineID)
		// ...specific offline content should be used
		IF (IS_STRING_NULL_OR_EMPTY(paramSpecificOfflineID))
			paramUseSpecificOfflineID = FALSE
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [CloudLoader]: UseSpecificOfflineID = TRUE, but filename is EMPTY. Setting flag to FALSE") NET_NL()
			#ENDIF
		ENDIF
	ELSE
		// ...specific offline content is not needed
		IF NOT (IS_STRING_NULL_OR_EMPTY(paramSpecificOfflineID))
			paramSpecificOfflineID = ""
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [CloudLoader]: UseSpecificOfflineID = FALSE, but filename not EMPTY. Setting filename to EMPTY") NET_NL()
			#ENDIF
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
		PRINTSTRING("...KGM MP [CloudLoader]: Request Received To Load Cloud Mission Data (FileName: ")
		PRINTSTRING(paramMissionIdData.idCloudFilename)
		PRINTSTRING(")")
		PRINTNL()
		IF (paramUseSpecificOfflineID)
			PRINTSTRING("      BUT SPECIFIC OFFLINE CONTENT IS REQUIRED: ")
			PRINTSTRING(paramSpecificOfflineID)
			PRINTNL()
		ENDIF
	#ENDIF

	// Delay this if there is a mission header load in progress
	IF (g_sCloudMissionLoaderMP.cmlHeaderRequired)
		PRINTSTRING("      - DELAY REQUEST: Header Download Request Still Active.")
		PRINTNL()
		
		RETURN FALSE
	ENDIF

	// Delay this if there is a Heist Planning Header load in progress
	IF (g_sCloudMissionLoaderMP.cmlHeistPlanningHeadersRequired)
		PRINTSTRING("      - DELAY REQUEST: Heist Planning Header Download Request Still Active.")
		PRINTNL()
		
		RETURN FALSE
	ENDIF
	
	// Relaunch the standalone script if not available
	IF NOT (Is_Standalone_Loader_Script_Now_Available())
		PRINTSTRING("      - DELAY REQUEST: Relaunching Cloud Loader script (Load_Cloud_Loaded_Mission_Data).")
		PRINTNL()
		
		RETURN FALSE
	ENDIF

	// Check if the data has already been loaded (this flag will get set by the standalone script)
	IF (Check_For_Download_Now_Finished())
		RETURN TRUE
	ENDIF
	
	// If this is a new request then clear any one-off variables
	IF NOT (g_sCloudMissionLoaderMP.cmlLoadRequired)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("...KGM MP [CloudLoader]: One-Off data clearout before new mission load request (FileName: ")
			PRINTSTRING(paramMissionIdData.idCloudFilename)
			PRINTSTRING(")")
			IF (paramUseSpecificOfflineID)
				PRINTSTRING(" (")
				PRINTSTRING(paramSpecificOfflineID)
				PRINTSTRING(")")
			ENDIF
			IF (g_sCloudMissionLoaderMP.cmlClearDataPreload)
				PRINTSTRING(" - also clear previous UGC data")
			ENDIF
			PRINTNL()
		#ENDIF
	
		IF NOT (Can_A_New_Download_Request_Start())
			RETURN FALSE
		ENDIF
		
		g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= FALSE
		g_sCloudMissionLoaderMP.cmlNumLoadAttempts		= 0
		g_sCloudMissionLoaderMP.cmlReportingFrames		= -1
		g_sCloudMissionLoaderMP.cmlCancelLoad			= FALSE
		
		// Clear the UGC Data preload if requested
		IF (paramClearPrevData)
			g_sCloudMissionLoaderMP.cmlClearDataPreload	= TRUE
		ELSE
			g_sCloudMissionLoaderMP.cmlClearDataPreload	= FALSE
		ENDIF
	ENDIF
	
	// The data hasn't yet loaded, so store the details in the globals for the standalone script to read
	g_sCloudMissionLoaderMP.cmlLoadRequired					= TRUE
	g_sCloudMissionLoaderMP.cmlHeaderRequired				= FALSE
	g_sCloudMissionLoaderMP.cmlHeistPlanningHeadersRequired	= FALSE
	g_sCloudMissionLoaderMP.cmlDataLoadFinished				= FALSE
	
	Clear_MP_MISSION_ID_DATA_Struct(g_sCloudMissionLoaderMP.cmlMissionIdData)
	g_sCloudMissionLoaderMP.cmlMissionIdData		= paramMissionIdData
	
	// Store offlineID if required
	g_sCloudMissionLoaderMP.cmlUseOfflineID			= paramUseSpecificOfflineID
	IF (paramUseSpecificOfflineID)
		g_sCloudMissionLoaderMP.cmlOfflineID 		= paramSpecificOfflineID
	ENDIF
		
	#IF IS_DEBUG_BUILD
		PRINTSTRING("...KGM MP [CloudLoader]: Request To Load Cloud Mission Data (FileName: ")
		PRINTSTRING(paramMissionIdData.idCloudFilename)
		PRINTSTRING(") - NOW ACTIVE")
		IF (paramUseSpecificOfflineID)
			PRINTSTRING("  (Using OfflineID instead: ")
			PRINTSTRING(paramSpecificOfflineID)
			PRINTSTRING(")")
		ENDIF
		PRINTNL()
	#ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Cancel an ongoing mission data load
PROC Cancel_Ongoing_Cloud_Loaded_Mission_Data_Download()

	IF NOT (g_sCloudMissionLoaderMP.cmlLoadRequired)
		PRINTLN(".KGM [CloudLoader]: Cancel_Ongoing_Cloud_Loaded_Mission_Data_Load: IGNORING. Mission Data Load is not in progress.")
		g_sCloudMissionLoaderMP.cmlCancelLoad = FALSE
		EXIT
	ENDIF
	
	PRINTLN(".KGM [CloudLoader]: Cancel_Ongoing_Cloud_Loaded_Mission_Data_Load: Data Load in Progress. Cancelling.")
	g_sCloudMissionLoaderMP.cmlCancelLoad = TRUE
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Did the previous mission load attempt end successfully??
//
// RETURN VALUE:		BOOL			TRUE if the previous mission load attempt ended successfully, FALSE if not
FUNC BOOL Did_Cloud_Loaded_Mission_Data_Load_Successfully()

	IF (g_sCloudMissionLoaderMP.cmlLoadRequired)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("...KGM MP [CloudLoader]: Did_Cloud_Loaded_Mission_Data_Load_Successfully: Called while a load is in progress. Returning FALSE.") PRINTNL()
			SCRIPT_ASSERT("Did_Cloud_Loaded_Mission_Data_Load_Successfully() - ERROR: Called while a load is in progress. Returning FALSE. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	BOOL wasSuccessful = g_sCloudMissionLoaderMP.cmlDataLoadSuccessful
	
	g_sCloudMissionLoaderMP.cmlDataLoadSuccessful = FALSE
	
	#IF IS_DEBUG_BUILD
		IF (wasSuccessful)
			PRINTSTRING("...KGM MP [CloudLoader]: Did_Cloud_Loaded_Mission_Data_Load_Successfully: Requesting Script now told load was successful.") PRINTNL()
		ELSE
			PRINTSTRING("...KGM MP [CloudLoader]: Did_Cloud_Loaded_Mission_Data_Load_Successfully: Requesting Script now told load FAILED.") PRINTNL()
		ENDIF
		
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	RETURN (wasSuccessful)
	
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Header Data Download functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Allows a request to download the header data for a contentID (added for Joblist Cross-session invites)
//
// INPUT PARAMS:			paramContentID				The ContentID (CloudFilename)
// RETURN VALUE:			BOOL						TRUE if the header data is loaded, otherwise FALSE
FUNC BOOL Load_Cloud_Loaded_Header_Data(TEXT_LABEL_23 paramContentID)
		
	#IF IS_DEBUG_BUILD
		PRINTSTRING("...KGM MP [CloudLoader]: Request Received To Load Cloud Header Data (FileName: ")
		PRINTSTRING(paramContentID)
		PRINTSTRING(")")
		PRINTNL()
	#ENDIF

	// Delay this if there is a full mission data download in progress
	IF (g_sCloudMissionLoaderMP.cmlLoadRequired)
		PRINTSTRING("      - DELAY REQUEST: Mission Data Download Request Still Active.")
		PRINTNL()
		
		RETURN FALSE
	ENDIF

	// Delay this if there is a Heist Planning Header load in progress
	IF (g_sCloudMissionLoaderMP.cmlHeistPlanningHeadersRequired)
		PRINTSTRING("      - DELAY REQUEST (Header Load): Heist Planning Header Download Request Still Active.")
		PRINTNL()
		
		RETURN FALSE
	ENDIF
	
	// Delay this if there is a Heist Planning Header load in progress
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF !GET_FM_UGC_INITIAL_HAS_FINISHED()
			PRINTSTRING("      - DELAY REQUEST (Header Load): GET_FM_UGC_INITIAL_HAS_FINISHED")
			PRINTNL()
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Relaunch the standalone script if not available
	IF NOT (Is_Standalone_Loader_Script_Now_Available())
		PRINTSTRING("      - DELAY REQUEST: Relaunching Cloud Loader script (Load_Cloud_Loaded_Header_Data).")
		PRINTNL()
		
		RETURN FALSE
	ENDIF
	
	// Check if the data has already been loaded (this flag will get set by the standalone script)
	IF (Check_For_Download_Now_Finished())
		RETURN TRUE
	ENDIF
	
	// If this is a new request then clear any one-off variables
	IF NOT (g_sCloudMissionLoaderMP.cmlHeaderRequired)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("...KGM MP [CloudLoader]: One-Off data clearout before new header load request (FileName: ")
			PRINTSTRING(paramContentID)
			PRINTSTRING(")")
			PRINTNL()
		#ENDIF
	
		IF NOT (Can_A_New_Download_Request_Start())
			RETURN FALSE
		ENDIF
		
		g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= FALSE
		g_sCloudMissionLoaderMP.cmlNumLoadAttempts		= 0
		g_sCloudMissionLoaderMP.cmlReportingFrames		= -1
		// KGM 27/3/14: Also request that the data struct gets cleared (same as a mission load)
		g_sCloudMissionLoaderMP.cmlClearDataPreload		= TRUE
	ENDIF
	
	// The data hasn't yet loaded, so store the details in the globals for the standalone script to read
	g_sCloudMissionLoaderMP.cmlLoadRequired						= FALSE
	g_sCloudMissionLoaderMP.cmlHeaderRequired					= TRUE
	g_sCloudMissionLoaderMP.cmlHeistPlanningHeadersRequired		= FALSE
	g_sCloudMissionLoaderMP.cmlDataLoadFinished					= FALSE
	
	Clear_MP_MISSION_ID_DATA_Struct(g_sCloudMissionLoaderMP.cmlMissionIdData)
	TEXT_LABEL_23 TEMP_ContentID = paramContentID
	g_sCloudMissionLoaderMP.cmlMissionIdData.idCloudFilename	= TEMP_ContentID
		
	#IF IS_DEBUG_BUILD
		PRINTSTRING("...KGM MP [CloudLoader]: Request Received To Load Cloud Header Data (FileName: ")
		PRINTSTRING(paramContentID)
		PRINTSTRING(") - NOW ACTIVE")
		PRINTNL()
	#ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Did the previous header load attempt end successfully??
//
// RETURN VALUE:		BOOL			TRUE if the previous header load attempt ended successfully, FALSE if not
FUNC BOOL Did_Cloud_Loaded_Header_Data_Load_Successfully()

	IF (g_sCloudMissionLoaderMP.cmlHeaderRequired)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("...KGM MP [CloudLoader]: Did_Cloud_Loaded_Header_Data_Load_Successfully: Called while a load is in progress. Returning FALSE.") PRINTNL()
			SCRIPT_ASSERT("Did_Cloud_Loaded_Header_Data_Load_Successfully() - ERROR: Called while a load is in progress. Returning FALSE. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	BOOL wasSuccessful = g_sCloudMissionLoaderMP.cmlDataLoadSuccessful
	
	g_sCloudMissionLoaderMP.cmlDataLoadSuccessful = FALSE
	
	#IF IS_DEBUG_BUILD
		IF (wasSuccessful)
			PRINTSTRING("...KGM MP [CloudLoader]: Did_Cloud_Loaded_Header_Data_Load_Successfully: Requesting Script now told load was successful.") PRINTNL()
		ELSE
			PRINTSTRING("...KGM MP [CloudLoader]: Did_Cloud_Loaded_Header_Data_Load_Successfully: Requesting Script now told load FAILED.") PRINTNL()
		ENDIF
		
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	RETURN (wasSuccessful)
	
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Heist Planning Header Data Download functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Heist Planning rootContentID string from the relevant stat
//
// INPUT PARAMS:		paramArrayPos		The stat to retrieve the data from
// RETURN VALUE:		TEXT_LABEL_23		The stored rootContentID
//
// NOTES:	Ignore STAT 0 - this holds the Finale RootContentID, so all stats are offset by one from the passed in arrayPos
FUNC TEXT_LABEL_23 Get_Heist_Planning_RootContentID_From_Stats(INT paramArrayPos)

	TEXT_LABEL_23 theReturn = ""

	IF (paramArrayPos >= MAX_HEIST_PLANNING_MISSIONS)
		PRINTLN(".KGM [Heist]: ERROR - Get_Heist_Planning_RootContentID_From_Stats: Passed in arrayPos (", paramArrayPos, ") > MAX_HEIST_PLANNING_MISSIONS (", MAX_HEIST_PLANNING_MISSIONS, ")")
		SCRIPT_ASSERT("Get_Heist_Planning_RootContentID_From_Stats: ERROR: Passed in arrayPos > MAX_HEIST_PLANNING_MISSIONS. Tell Keith.")
		RETURN (theReturn)
	ENDIF
	
	SWITCH (paramArrayPos)
		CASE 0		theReturn = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_1)		BREAK
		CASE 1		theReturn = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_2)		BREAK
		CASE 2		theReturn = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_3)		BREAK
		CASE 3		theReturn = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_4)		BREAK
		CASE 4		theReturn = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_5)		BREAK
		CASE 5		theReturn = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_6)		BREAK
		CASE 6		theReturn = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_7)		BREAK
		
		DEFAULT
			PRINTLN(".KGM [Heist]: ERROR - Get_Heist_Planning_RootContentID_From_Stats: Passed in arrayPos is unsupported. Add to Switch: ", paramArrayPos)
			SCRIPT_ASSERT("Get_Heist_Planning_RootContentID_From_Stats: ERROR: Passed in arrayPos is unsupported. Add to Switch. Tell Keith.")
			RETURN (theReturn)
	ENDSWITCH
	
	RETURN (theReturn)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Prefill the array of ContentIDs that need Heist Planning header data downloaded
//
// INPUT PARAMS:			paramPlayerID		The Player_Index of the Heist Leader
// RETURN VALUE:			BOOL				TRUE if there is header data to download, FALSE if there is none
FUNC BOOL Prefill_Heist_Planning_Required_Header_Data(PLAYER_INDEX paramPlayerID)

	// Clear the contentIDs array
	INT tempLoop = 0
	REPEAT MAX_CONTENT_RESULTS tempLoop
		g_heistPlanningCloudRCIDsMP.szContentID[tempLoop] = ""
	ENDREPEAT
	g_numHeistPlanningRequired = 0
			
	BOOL detailsAdded	= FALSE
	BOOL readFromStats	= FALSE
	
	// Check if the details should be read from Stats or Player Broadcast Data
	IF (PLAYER_ID() = paramPlayerID)
		// Heist Leader
		PRINTLN(".KGM [Heist][CloudLoader]: I am Heist Leader. Attempting to fill Heist Planning Header Details required from STATS")
		
		readFromStats = TRUE
		
		// Has the STATS been setup (this is usually the case except when the data is needed for the intro cutscene)
		TEXT_LABEL_23 tlHeistFinaleRootContID = GET_HEIST_FINALE_STAT_DATA()
		
		IF ARE_STRINGS_EQUAL(tlHeistFinaleRootContID, ".")
		OR ARE_STRINGS_EQUAL(tlHeistFinaleRootContID, "")
			readFromStats = FALSE
			PRINTLN(".KGM [Heist][CloudLoader]: BUT: the STATS aren't setup yet, so fill them from my Player Broadcast Data instead")
		ENDIF
	ELSE
		// Heist Crew, so always read from Player Broadcast Data, not STATS
		readFromStats = FALSE
		
		// Another player is Heist Leader so grab the data from player broadcast data
		IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
			PRINTLN(".KGM [Heist][CloudLoader]: Another player is Heist Leader. Attempting to fill Heist Planning Header Details BUT PLAYER IS NOT OK")
			
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [Heist][CloudLoader]: Another player is Heist Leader. Filling Heist Planning Header Details required from Player Broadcast Data. Leader: ", GET_PLAYER_NAME(paramPlayerID))
		#ENDIF
	ENDIF
	
	IF (readFromStats)
		// Grab the data from the STATS
		REPEAT MAX_HEIST_PLANNING_MISSIONS tempLoop
			detailsAdded = TRUE
			// This check is 'just in case' we ever get more Planning Missions per heist than are allowed to be downloaded in a batch (MAX_CREATOR_RESULTS)
			// And also 'just in case' we ever get more Planning Missions per heist than the UGC_CONTENT_ID_QUERY array is designed to hold (MAX_CONTENT_RESULTS)
			IF (tempLoop < MAX_CREATOR_RESULTS)
			AND (tempLoop < MAX_CONTENT_RESULTS)
				// Retrieve the next Planning Mission RootContentID from the STATS
				g_heistPlanningCloudRCIDsMP.szContentID[g_numHeistPlanningRequired] = Get_Heist_Planning_RootContentID_From_Stats(tempLoop)
				IF (IS_STRING_NULL_OR_EMPTY(g_heistPlanningCloudRCIDsMP.szContentID[g_numHeistPlanningRequired]))
				OR (ARE_STRINGS_EQUAL(g_heistPlanningCloudRCIDsMP.szContentID[g_numHeistPlanningRequired], "."))
					g_heistPlanningCloudRCIDsMP.szContentID[g_numHeistPlanningRequired] = ""
					detailsAdded = FALSE
					PRINTLN(".KGM [Heist][CloudLoader]: ...", tempLoop, "  - NO ROOTCONTENTID STAT STORED. TotalCount remains: ", g_numHeistPlanningRequired)
				ENDIF
				
				IF (detailsAdded)
					PRINTLN(".KGM [Heist][CloudLoader]: ...g_heistPlanningCloudRCIDsMP.szContentID[", g_numHeistPlanningRequired, "] = ", g_heistPlanningCloudRCIDsMP.szContentID[g_numHeistPlanningRequired])
					g_numHeistPlanningRequired++
				ENDIF
			ENDIF
		ENDREPEAT
			
		IF (g_numHeistPlanningRequired = 0)
			PRINTLN(".KGM [Heist][CloudLoader]: There is no content in STATS to download")
			
			RETURN FALSE
		ENDIF
	ELSE
		INT playerAsInt = NATIVE_TO_INT(paramPlayerID)
		
		REPEAT MAX_HEIST_PLANNING_MISSIONS tempLoop
			detailsAdded = TRUE
			// This check is 'just in case' we ever get more Planning Missions per heist than are allowed to be downloaded in a batch (MAX_CREATOR_RESULTS)
			// And also 'just in case' we ever get more Planning Missions per heist than the UGC_CONTENT_ID_QUERY array is designed to hold (MAX_CONTENT_RESULTS)
			IF (tempLoop < MAX_CREATOR_RESULTS)
			AND (tempLoop < MAX_CONTENT_RESULTS)
				// Retrieve the next Planning Mission RootContentID from Player Broadcast Data
				g_heistPlanningCloudRCIDsMP.szContentID[g_numHeistPlanningRequired] = GlobalplayerBD_FM_HeistPlanning[playerAsInt].sSetupMissionData[tempLoop].tl23MissionRootContID
				IF (IS_STRING_NULL_OR_EMPTY(g_heistPlanningCloudRCIDsMP.szContentID[g_numHeistPlanningRequired]))
				OR (ARE_STRINGS_EQUAL(g_heistPlanningCloudRCIDsMP.szContentID[g_numHeistPlanningRequired], "."))
					g_heistPlanningCloudRCIDsMP.szContentID[g_numHeistPlanningRequired] = ""
					detailsAdded = FALSE
					PRINTLN(".KGM [Heist][CloudLoader]: ...", tempLoop, "  - NO ROOTCONTENTID STAT STORED. TotalCount remains: ", g_numHeistPlanningRequired)
				ENDIF
				
				IF (detailsAdded)
					PRINTLN(".KGM [Heist][CloudLoader]: ...g_heistPlanningCloudRCIDsMP.szContentID[", g_numHeistPlanningRequired, "] = ", g_heistPlanningCloudRCIDsMP.szContentID[g_numHeistPlanningRequired])
					g_numHeistPlanningRequired++
				ENDIF
			ENDIF
		ENDREPEAT
			
		IF (g_numHeistPlanningRequired = 0)
			PRINTLN(".KGM [Heist][CloudLoader]: There is no content in Player Broadcast Data to download")
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Allows a request to download the header data for all Heist Planning missions associated with the rootContentID of a Heist Finale mission
//
// INPUT PARAMS:			paramPlayerID				The PlayerID where the Heist Planning Mission details need to be retrieved from
// RETURN VALUE:			BOOL						TRUE if the Heist Planning Header data is loaded, otherwise FALSE
FUNC BOOL Load_Cloud_Loaded_Heist_Planning_Header_Data(PLAYER_INDEX paramPlayerID)
		
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist][CloudLoader]: Request Received To Load Cloud Heist Planning Header Data (Heist Leader: ", GET_PLAYER_NAME(paramPlayerID), ")")
	#ENDIF

	// Delay this if there is a full mission data download in progress
	IF (g_sCloudMissionLoaderMP.cmlLoadRequired)
		PRINTSTRING("      - DELAY REQUEST: Mission Data Download Request Still Active.")
		PRINTNL()
		
		RETURN FALSE
	ENDIF

	// Delay this if there is a header data download in progress
	IF (g_sCloudMissionLoaderMP.cmlHeaderRequired)
		PRINTSTRING("      - DELAY REQUEST: Mission Data Download Request Still Active.")
		PRINTNL()
		
		RETURN FALSE
	ENDIF
	
	// Relaunch the standalone script if not available
	IF NOT (Is_Standalone_Loader_Script_Now_Available())
		PRINTSTRING("      - DELAY REQUEST: Relaunching Cloud Loader script (Load_Cloud_Loaded_Heist_Planning_Header_Data).")
		PRINTNL()
		
		RETURN FALSE
	ENDIF
	
	// Check if the data has already been loaded (this flag will get set by the standalone script)
	IF (Check_For_Download_Now_Finished())
		RETURN TRUE
	ENDIF
	
	// If this is a new request then clear any one-off variables
	IF NOT (g_sCloudMissionLoaderMP.cmlHeistPlanningHeadersRequired)
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [Heist][CloudLoader]: One-Off data clearout before new Heist Planning Header Data load request (Heist Leader: ", GET_PLAYER_NAME(paramPlayerID), ")")
		#ENDIF
	
		IF NOT (Can_A_New_Download_Request_Start())
			RETURN FALSE
		ENDIF
		
		// KGM 2/8/14: Pre-fill the ContentIDs array with the Heist Planning missions we need to download
		IF NOT (Prefill_Heist_Planning_Required_Header_Data(paramPlayerID))
			RETURN FALSE
		ENDIF
		
		g_sCloudMissionLoaderMP.cmlDataLoadSuccessful	= FALSE
		g_sCloudMissionLoaderMP.cmlNumLoadAttempts		= 0
		g_sCloudMissionLoaderMP.cmlReportingFrames		= -1
		// KGM 27/3/14: Also request that the data struct gets cleared (same as a mission load)
		g_sCloudMissionLoaderMP.cmlClearDataPreload		= TRUE
	ENDIF
	
	// The data hasn't yet loaded, so store the details in the globals for the standalone script to read
	g_sCloudMissionLoaderMP.cmlLoadRequired						= FALSE
	g_sCloudMissionLoaderMP.cmlHeaderRequired					= FALSE
	g_sCloudMissionLoaderMP.cmlHeistPlanningHeadersRequired		= TRUE
	g_sCloudMissionLoaderMP.cmlDataLoadFinished					= FALSE
	
	// ...no details need passed to the loader script - the details will be taken from either STATS (Leader) or the Leader's Player Broadcast data (Crew)
	Clear_MP_MISSION_ID_DATA_Struct(g_sCloudMissionLoaderMP.cmlMissionIdData)
		
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist][CloudLoader]: ...Actively downloading Cloud Heist Planning Header Data (Heist Leader: ", GET_PLAYER_NAME(paramPlayerID), ") - ONGOING")
	#ENDIF
	
	RETURN FALSE

ENDFUNC
	// FEATURE_HEIST_PLANNING


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Did the previous Heist Planning Header load attempt end successfully??
//
// RETURN VALUE:		BOOL			TRUE if the previous Heist Planning Header load attempt ended successfully, FALSE if not
FUNC BOOL Did_Cloud_Loaded_Heist_Planning_Header_Data_Load_Successfully()

	IF (g_sCloudMissionLoaderMP.cmlHeistPlanningHeadersRequired)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("...KGM MP [CloudLoader]: Did_Cloud_Loaded_Heist_Planning_Header_Data_Load_Successfully: Called while a load is in progress. Returning FALSE.") PRINTNL()
			SCRIPT_ASSERT("Did_Cloud_Loaded_Heist_Planning_Header_Data_Load_Successfully() - ERROR: Called while a load is in progress. Returning FALSE. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	BOOL wasSuccessful = g_sCloudMissionLoaderMP.cmlDataLoadSuccessful
	
	g_sCloudMissionLoaderMP.cmlDataLoadSuccessful = FALSE
	
	#IF IS_DEBUG_BUILD
		IF (wasSuccessful)
			PRINTSTRING("...KGM MP [CloudLoader]: Did_Cloud_Loaded_Heist_Planning_Header_Data_Load_Successfully: Requesting Script now told load was successful.") PRINTNL()
		ELSE
			PRINTSTRING("...KGM MP [CloudLoader]: Did_Cloud_Loaded_Heist_Planning_Header_Data_Load_Successfully: Requesting Script now told load FAILED.") PRINTNL()
		ENDIF
		
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	RETURN (wasSuccessful)
	
ENDFUNC
	// FEATURE_HEIST_PLANNING




// ===========================================================================================================
//      Standalone Script Access Functions
// ===========================================================================================================

// PURPOSE:	Is cloud-loaded mission data required?
//
// RETURN VALUE:		BOOL			TRUE if mission data is required to be loaded, FALSE if not
FUNC BOOL Is_Cloud_Loaded_Mission_Data_Required_To_Be_Loaded()
	RETURN (g_sCloudMissionLoaderMP.cmlLoadRequired)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Is cloud-loaded header data required?
//
// RETURN VALUE:		BOOL			TRUE if header data is required to be loaded, FALSE if not
FUNC BOOL Is_Cloud_Loaded_Header_Data_Required_To_Be_Loaded()
	RETURN (g_sCloudMissionLoaderMP.cmlHeaderRequired)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Is cloud-loaded Heist Planning Header data required?
//
// RETURN VALUE:		BOOL			TRUE if Heist Planning Header data is required to be loaded, FALSE if not
FUNC BOOL Is_Cloud_Loaded_Heist_Planning_Header_Data_Required_To_Be_Loaded()
	RETURN (g_sCloudMissionLoaderMP.cmlHeistPlanningHeadersRequired)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Is cloud-loaded mission data ongoing download to be cancelled?
//
// RETURN VALUE:		BOOL			TRUE if ongoing mission download is to be cancelled, FALSE if not
FUNC BOOL Is_Cloud_Loaded_Mission_Data_Load_To_Be_Cancelled()
	RETURN (g_sCloudMissionLoaderMP.cmlCancelLoad)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Is a cloud mission download in progress?
//
// RETURN VALUE:		BOOL			TRUE if a download is ongoing, FALSE if not
FUNC BOOL Is_Cloud_Loaded_Mission_Data_Download_In_Progress()

	// Check a download is required but is not finished
	IF NOT (g_sCloudMissionLoaderMP.cmlLoadRequired)
	AND NOT (g_sCloudMissionLoaderMP.cmlHeaderRequired)
	AND NOT (g_sCloudMissionLoaderMP.cmlHeistPlanningHeadersRequired)
		RETURN FALSE
	ENDIF
	
	// A Mission or Header or Heist Planning Header download is required, so check if it has finished
	IF NOT (g_sCloudMissionLoaderMP.cmlDataLoadFinished)
		// ...in progress
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	 
ENDFUNC




