//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_drone_controller.sch																				//
// Description: This is data and function helpers needed for starting drone script from freemode(PI Menu) or arcade mode//
//																														//
// Written by:  Ata																										//
// Date:  		20/08/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


USING "net_drone.sch"

#IF FEATURE_CASINO_HEIST

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡    ENUMS    ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

ENUM DRONE_CONTROLLER_STATE
	DRONE_CONTROLLER_STATE_IDLE =	0,
	DRONE_CONTROLLER_STATE_UPDATE,
	DRONE_CONTROLLER_STATE_START_DRONE_SCRIPT,
	DRONE_CONTROLLER_STATE_CLEANUP
ENDENUM

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    STRUCT    ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

STRUCT DRONE_CONTROLLER
	INT iDroneStartShapeTest, iDroneStartHitSomthing
	
	DRONE_CONTROLLER_STATE eDroneControllerState
	SHAPETEST_INDEX droneStartShapeTest	
	
ENDSTRUCT

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡    FUNC / PROC    ╞═══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING GET_DRONE_CONTROLLER_STATE_NAME(DRONE_CONTROLLER_STATE eState)
	SWITCH eState
		CASE DRONE_CONTROLLER_STATE_IDLE				RETURN 		"DRONE_CONTROLLER_STATE_IDLE"
		CASE DRONE_CONTROLLER_STATE_UPDATE				RETURN 		"DRONE_CONTROLLER_STATE_UPDATE"
		CASE DRONE_CONTROLLER_STATE_START_DRONE_SCRIPT	RETURN 		"DRONE_CONTROLLER_STATE_START_DRONE_SCRIPT"
		CASE DRONE_CONTROLLER_STATE_CLEANUP				RETURN 		"DRONE_CONTROLLER_STATE_CLEANUP" 
	ENDSWITCH 
	
	RETURN "INVALID DRONE CONTROLLER STATE"
ENDFUNC
#ENDIF

PROC SET_DRONE_CONTROLLER_STATE(DRONE_CONTROLLER &eDroneController, DRONE_CONTROLLER_STATE eState)
	IF eDroneController.eDroneControllerState != eState
		eDroneController.eDroneControllerState = eState
		PRINTLN("[AM_MP_DRONE] - SET_DRONE_CONTROLLER_STATE: ", GET_DRONE_CONTROLLER_STATE_NAME(eState))
	ENDIF	
ENDPROC

FUNC BOOL SHOULD_START_SHAPE_TEST_PROCESS()
	RETURN TRUE
ENDFUNC 

PROC MAINTAIN_DRONE_CONTROLLER_IDLE_STATE(DRONE_CONTROLLER &eDroneController)
	IF SHOULD_START_SHAPE_TEST_PROCESS()
		SET_DRONE_CONTROLLER_STATE(eDroneController, DRONE_CONTROLLER_STATE_UPDATE)
	ENDIF
ENDPROC

PROC MAINTAIN_DRONE_START_SHAPE_TEST(DRONE_CONTROLLER &eDroneController)

	SWITCH eDroneController.idroneStartShapeTest
	    CASE DRONE_SHAPETEST_INIT
	        
			// See if we can collide with anything around drone
			FLOAT fRadius
			VECTOR vPlayerCoords, vEndCoord, vOffset
			FLOAT fPlayerHeading			
			
			fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
			fRadius = 1.1
			vOffset = <<0, 0.5, 0.23>>
			
			#IF IS_DEBUG_BUILD
			IF g_fDroneShapeTestRadius != 0 
				fRadius = g_fDroneShapeTestRadius
			ENDIF
			IF !IS_VECTOR_ZERO(g_vDroneShapeTestOffset)
				vOffset = g_vDroneShapeTestOffset
			ENDIF	
			#ENDIF
			
			vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			vEndCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPlayerCoords, fPlayerHeading, vOffset)
			
			eDroneController.droneStartShapeTest = START_SHAPE_TEST_CAPSULE(vEndCoord, vEndCoord, fRadius, SCRIPT_INCLUDE_ALL, PLAYER_PED_ID())
			
			#IF IS_DEBUG_BUILD
				IF g_bDisplayDroneShapeTest
					DRAW_DEBUG_SPHERE(vEndCoord, fRadius, 0, 255, 0, 64)
				ENDIF	
			#ENDIF
			
			IF NATIVE_TO_INT(eDroneController.droneStartShapeTest) != 0
				eDroneController.idroneStartShapeTest 		= DRONE_SHAPETEST_PROCESS
				//PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_START_SHAPE_TEST eDroneController.idroneStartShapeTest INIT to PROCESS ")
			ENDIF	
				
	    BREAK
	    CASE DRONE_SHAPETEST_PROCESS
	     
	        INT iHitSomething
	        VECTOR vNormalAtPosHit, vDroneCollisionHitPos
	        ENTITY_INDEX entityHit
			 
	        SHAPETEST_STATUS shapetestStatus 
			shapetestStatus = GET_SHAPE_TEST_RESULT(eDroneController.droneStartShapeTest, iHitSomething, vDroneCollisionHitPos, vNormalAtPosHit, entityHit)
			
			// result is ready
	         IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
	             #IF IS_DEBUG_BUILD
				//PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_START_SHAPE_TEST - We got shapetest results!")
				#ENDIF
				
				IF iHitSomething = 0
					
					eDroneController.iDroneStartHitSomthing = DRONE_CAM_AREA_CHECK_NOT_HIT_ANYTHING
					SET_DRONE_AREA_SAFE_TO_SPAWN(TRUE)
					
					vDroneCollisionHitPos = <<0,0,0>> 
					#IF IS_DEBUG_BUILD
					IF g_bDisplayDroneShapeTest
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "Drone collision hit somthing = FALSE", 1,0,255,0)		
					ENDIF	
					#ENDIF
					
	             ELSE

	                eDroneController.iDroneStartHitSomthing = DRONE_CAM_AREA_CHECK_HIT_SOMETHING
					SET_DRONE_AREA_SAFE_TO_SPAWN(FALSE)
					
					#IF IS_DEBUG_BUILD
						IF g_bDisplayDroneShapeTest
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "Drone collision hit somthing = TRUE", 1)
						ENDIF	
						PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_START_SHAPE_TEST  DRONE_CAM_AREA_CHECK_HIT_SOMETHING ")
					#ENDIF
					
					eDroneController.droneStartShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
					eDroneController.idroneStartShapeTest 	= DRONE_SHAPETEST_INIT  
					
	             ENDIF
				 
			ELIF shapetestStatus	= SHAPETEST_STATUS_NONEXISTENT
				eDroneController.idroneStartShapeTest 	= DRONE_SHAPETEST_INIT    
	         ENDIF

	    BREAK
	 ENDSWITCH

ENDPROC

FUNC BOOL SHOULD_ALLOW_DRONE_SHAPE_TEST()
	IF IS_CUSTOM_MENU_ON_SCREEN()
	AND ARE_STRINGS_EQUAL(g_sMenuData.tl15Title, "PIM_TITLE2")
		RETURN TRUE
	ENDIF
	
	IF !IS_PLAYER_USING_DRONE(PLAYER_ID())
	AND SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SAFE_TO_START_DRONE_SHAPETEST()
	IF !IS_NET_PLAYER_OK(PLAYER_ID())
		PRINTLN("[AM_MP_DRONE] - IS_SAFE_TO_START_DRONE_SHAPETEST FALSE player not okay")
		RETURN FALSE
	ENDIF
	
	IF !IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[AM_MP_DRONE] - IS_SAFE_TO_START_DRONE_SHAPETEST FALSE sky cam not in ground")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE) OR IS_PLAYER_IN_CASINO(PLAYER_ID()))
	AND !IS_DRONE_USE_FROM_PI_MENU_ENABLED_IN_MISSION()
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 120 = 0
			PRINTLN("[AM_MP_DRONE] - IS_SAFE_TO_START_DRONE_SHAPETEST FALSE player in property")
		ENDIF	
		#ENDIF
		RETURN FALSE
	ENDIF	
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		//IF !IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableDroneDeployment)
		IF !IS_DRONE_USE_FROM_PI_MENU_ENABLED_IN_MISSION()
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 120 = 0
				PRINTLN("[AM_MP_DRONE] - IS_SAFE_TO_START_DRONE_SHAPETEST player in activity session need to enable drone in missions")
			ENDIF	
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	#IF FEATURE_FREEMODE_ARCADE
	IF !IS_FREEMODE_ARCADE()
	#ENDIF
		IF !IS_DRONE_USE_FROM_PI_MENU_ENABLED_IN_MISSION()
			IF !DOES_PLAYER_OWN_AN_ARCADE_PROPERTY(PLAYER_ID())
				RETURN FALSE
			ENDIF
			
			IF !HAS_PLAYER_PURCHASED_ARCADE_DRONE_STATION(PLAYER_ID())
				RETURN FALSE
			ENDIF	
		ENDIF	
		
		IF !SHOULD_ALLOW_DRONE_SHAPE_TEST()
			RETURN FALSE
		ENDIF	
		
	#IF FEATURE_FREEMODE_ARCADE
	ELSE
		#IF FEATURE_COPS_N_CROOKS
			IF GET_ARCADE_MODE() = ARC_COPS_CROOKS 
			AND !IS_COPS_CROOK_DRONE_TYPE_COP()
				RETURN FALSE
			ENDIF
		#ENDIF
	ENDIF
	#ENDIF

	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
	AND !IS_DRONE_USE_FROM_PI_MENU_ENABLED_IN_MISSION()
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 120 = 0
			PRINTLN("[AM_MP_DRONE] - IS_SAFE_TO_START_DRONE_SHAPETEST FALSE player critical to event")
		ENDIF	
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_USING_DRONE()
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 120 = 0
			PRINTLN("[AM_MP_DRONE] - IS_SAFE_TO_START_DRONE_SHAPETEST FALSE player in using drone")
		ENDIF	
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_DRONE_CLEANING_UP()
		PRINTLN("[AM_MP_DRONE] - IS_SAFE_TO_START_DRONE_SHAPETEST FALSE player is cleaning up")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF

	IF IS_PLAYER_RIDING_TRAIN(PLAYER_ID())
		RETURN FALSE
	ENDIF	
	
	IF IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF	
	
	IF IS_PED_FALLING(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF	
	
	IF GET_ENTITY_SUBMERGED_LEVEL(PLAYER_PED_ID()) > 0.3
		PRINTLN("[AM_MP_DRONE] - IS_SAFE_TO_START_DRONE_SHAPETEST FALSE player is in water")
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
		PRINTLN("[AM_MP_DRONE] - IS_SAFE_TO_START_DRONE_SHAPETEST FALSE player is in air")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_ON_VEHICLE(PLAYER_PED_ID())
		PRINTLN("[AM_MP_DRONE] - IS_SAFE_TO_START_DRONE_SHAPETEST FALSE player is on vehicle")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC 

FUNC BOOL IS_SAFE_TO_LAUNCH_DRONE_FROM_INTERACTION_MENU()
	IF !IS_DRONE_AREA_SAFE_TO_SPAWN()
		RETURN FALSE
	ENDIF 
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_DRONE_IN_COOL_DOWN()
	AND !IS_DRONE_USE_FROM_PI_MENU_ENABLED_IN_MISSION()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableDroneDeployment)
	AND !IS_DRONE_USE_FROM_PI_MENU_ENABLED_IN_MISSION()
		RETURN FALSE
	ENDIF
	
	IF !IS_SAFE_TO_START_DRONE_SHAPETEST()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC 

FUNC BOOL IS_SAFE_TO_START_DRONE()
	IF !IS_DRONE_AREA_SAFE_TO_SPAWN()
		//PRINTLN("[AM_MP_DRONE] - IS_SAFE_TO_START_DRONE FALSE IS_DRONE_AREA_SAFE_TO_SPAWN false")
		RETURN FALSE
	ENDIF
	
	IF !SHOULD_START_DRONE_FROM_PLAYER_PHONE()
		RETURN FALSE
	ENDIF	
	
	IF IS_LOCAL_PLAYER_USING_DRONE()
		RETURN FALSE
	ENDIF
	
	IF IS_DRONE_CLEANING_UP()
		RETURN FALSE
	ENDIF
	
	IF g_SimpleInteriorData.bEventAutowarpActive 
	OR IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		SET_START_DRONE_FROM_PLAYER_PHONE(FALSE)
		RETURN FALSE
	ENDIF	
	
	RETURN TRUE
ENDFUNC 

PROC MAINTAIN_DRONE_CONTROLLER_UPDATE_STATE(DRONE_CONTROLLER &eDroneController)
	IF IS_SAFE_TO_START_DRONE_SHAPETEST()
		MAINTAIN_DRONE_START_SHAPE_TEST(eDroneController)
	ENDIF	
	
	IF IS_SAFE_TO_START_DRONE()
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("AM_MP_DRONE")) > 0 
			SET_TERMINATE_REMOTE_DRONE_SCRIPT(TRUE)
		ELSE
			SET_DRONE_CONTROLLER_STATE(eDroneController, DRONE_CONTROLLER_STATE_START_DRONE_SCRIPT)
		ENDIF
	ELSE
		IF SHOULD_START_DRONE_FROM_PLAYER_PHONE()
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("AM_MP_DRONE")) < 1 
				CLEAR_HELP()
				PRINT_HELP("PIM_DRONAMOS")
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_START_DRONE_FROM_PLAYER_PHONE(FALSE)
			ELSE
				PRINTLN("[AM_MP_DRONE] - MAINTAIN_DRONE_CONTROLLER_UPDATE_STATE drone script is running it should clean up please state ", GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("AM_MP_DRONE")))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DRONE_CONTROLLER_START_SCRIPT_STATE(DRONE_CONTROLLER &eDroneController)
	#IF FEATURE_FREEMODE_ARCADE
	IF !IS_FREEMODE_ARCADE()
	#ENDIF
		IF IS_DRONE_USE_FROM_PI_MENU_ENABLED_IN_MISSION()
			IF START_DRONE(DRONE_TYPE_CASINO_HEIST_MISSION, <<0, 0, 0>>, <<0, 0, 0>>, <<0, 0, 0>>)
				SET_DRONE_CONTROLLER_STATE(eDroneController, DRONE_CONTROLLER_STATE_CLEANUP)
			ENDIF
		ELSE
			IF START_DRONE(DRONE_TYPE_HEIST_FREEMODE, <<0, 0, 0>>, <<0, 0, 0>>, <<0, 0, 0>>)
				SET_DRONE_CONTROLLER_STATE(eDroneController, DRONE_CONTROLLER_STATE_CLEANUP)
			ENDIF
		ENDIF	
	#IF FEATURE_FREEMODE_ARCADE
	ELSE
		#IF FEATURE_COPS_N_CROOKS
		IF START_DRONE(DRONE_TYPE_ARCADE_COP, <<0, 0, 0>>, <<0, 0, 0>>, <<0, 0, 0>>)
			SET_DRONE_CONTROLLER_STATE(eDroneController, DRONE_CONTROLLER_STATE_CLEANUP)
		ENDIF
		#ENDIF
	ENDIF
	#ENDIF
ENDPROC

PROC MAINTAIN_DRONE_CONTROLLER_CLEANUP_STATE(DRONE_CONTROLLER &eDroneController)
	eDroneController.droneStartShapeTest 	= INT_TO_NATIVE(SHAPETEST_INDEX, 0)
	eDroneController.idroneStartShapeTest 	= DRONE_SHAPETEST_INIT 
	eDroneController.iDroneStartHitSomthing = -1
	SET_DRONE_CONTROLLER_STATE(eDroneController, DRONE_CONTROLLER_STATE_IDLE)
ENDPROC
	
/// PURPOSE:
///    State machine to process drone controller logic to star the drone script
PROC PROCESS_DRONE_CONTROLLER(DRONE_CONTROLLER &eDroneController)
	SWITCH eDroneController.eDroneControllerState
		CASE DRONE_CONTROLLER_STATE_IDLE
			MAINTAIN_DRONE_CONTROLLER_IDLE_STATE(eDroneController)
		BREAK
		CASE DRONE_CONTROLLER_STATE_UPDATE
			MAINTAIN_DRONE_CONTROLLER_UPDATE_STATE(eDroneController)
		BREAK
		CASE DRONE_CONTROLLER_STATE_START_DRONE_SCRIPT
			MAINTAIN_DRONE_CONTROLLER_START_SCRIPT_STATE(eDroneController)
		BREAK
		CASE DRONE_CONTROLLER_STATE_CLEANUP
			MAINTAIN_DRONE_CONTROLLER_CLEANUP_STATE(eDroneController)
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF


