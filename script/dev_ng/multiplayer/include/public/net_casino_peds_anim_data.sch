// Name:        NET_CASINO_PEDS_ANIM_DATA.sch
// Description: Lookup for Casino peds animation data.

#IF FEATURE_CASINO
USING "net_casino_peds_anim_data_lookup.sch"

#IF IS_DEBUG_BUILD
FUNC BOOL IS_CASINO_PED_DEBUG_EDITOR_ACTIVE()
	RETURN g_bCasinoPedEditorActive
ENDFUNC

FUNC BOOL IS_CASINO_PED_DEBUG_REPOSITION_PEDS_WIDGET_ACTIVE()
	RETURN g_bCasinoPedWidgetRepositionPeds
ENDFUNC

FUNC BOOL IS_CASINO_PED_DEBUG_KILL_LOGIC_WIDGET_ACTIVE()
	RETURN g_bKillCasinoPedLogic
ENDFUNC
#ENDIF

FUNC STRING GET_CASINO_AREA_STRING(CASINO_AREA eCasinoArea)
	SWITCH eCasinoArea
		CASE CASINO_AREA_MAIN_FLOOR			RETURN "CASINO_AREA_MAIN_FLOOR"
		CASE CASINO_AREA_PERMANENT			RETURN "CASINO_AREA_PERMANENT"
		CASE CASINO_AREA_TABLE_GAMES		RETURN "CASINO_AREA_TABLE_GAMES"
		CASE CASINO_AREA_SPORTS_BETTING		RETURN "CASINO_AREA_SPORTS_BETTING"
		CASE CASINO_AREA_MANAGERS_OFFICE	RETURN "CASINO_AREA_MANAGERS_OFFICE"
	ENDSWITCH
	RETURN "NULL"
ENDFUNC

FUNC STRING BUILD_ANIM_DICT_STRING(CASINO_PED_ANIM_DICT eDict, CASINO_AUDIO_TAGS eIntensityTag)
	SWITCH eDict
		CASE CASINO_ANIM_DICT_SINGLE_PROPS
			SWITCH eIntensityTag
				CASE CASINO_AUDIO_TAG_NULL
				CASE CASINO_AUDIO_TAG_LOW
					RETURN "anim@AMB@NIGHTCLUB@dancers@crowddance_single_props@low_intensity"	
				CASE CASINO_AUDIO_TAG_MEDIUM
					RETURN "anim@AMB@NIGHTCLUB@dancers@crowddance_single_props@med_intensity"
				CASE CASINO_AUDIO_TAG_HIGH
				CASE CASINO_AUDIO_TAG_HIGH_HANDS
					RETURN "anim@AMB@NIGHTCLUB@dancers@crowddance_single_props@hi_intensity"
			ENDSWITCH
		BREAK
		CASE CASINO_ANIM_DICT_AMBIENT_PEDS
			SWITCH eIntensityTag
				CASE CASINO_AUDIO_TAG_NULL
				CASE CASINO_AUDIO_TAG_LOW
				CASE CASINO_AUDIO_TAG_MEDIUM
					RETURN "anim@AMB@NIGHTCLUB@dancers@club_ambientpeds@low-med_intensity"	
				CASE CASINO_AUDIO_TAG_HIGH
				CASE CASINO_AUDIO_TAG_HIGH_HANDS
					RETURN "anim@AMB@NIGHTCLUB@dancers@club_ambientpeds@med-hi_intensity"
			ENDSWITCH		
		BREAK
	ENDSWITCH

	RETURN ""
ENDFUNC

FUNC STRING BUILD_ANIM_DICT_TRANSITION_STRING(CASINO_PED_ANIM_DICT eDict)
	SWITCH eDict
		CASE CASINO_ANIM_DICT_SINGLE_PROPS
		CASE CASINO_ANIM_DICT_AMBIENT_PEDS
			RETURN "anim@AMB@NIGHTCLUB@dancers@club_ambientpeds_transitions@"
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC TEXT_LABEL_63 SINGLE_PROPS_BUILD_ANIM_CLIP_STRING(CASINO_AUDIO_TAGS eIntensityTag, INT iClipDuration, INT iVersion, BOOL bIsFemale, INT iEndNumber)
	TEXT_LABEL_63 str
	
	SWITCH eIntensityTag
		CASE CASINO_AUDIO_TAG_NULL
		CASE CASINO_AUDIO_TAG_LOW
			str = "li_dance_prop_"
		BREAK
		CASE CASINO_AUDIO_TAG_MEDIUM							
			str = "mi_dance_prop_"
		BREAK
		CASE CASINO_AUDIO_TAG_HIGH	
		CASE CASINO_AUDIO_TAG_HIGH_HANDS						
			str = "hi_dance_prop_"
		BREAK		
	ENDSWITCH
	
	// duration
	IF (iClipDuration < 10)
		str += 0
	ENDIF
	str += iClipDuration
	
	// version
	str += "_v"
	str += iVersion
	
	// male / female
	IF NOT (bIsFemale)
		str += "_male"
	ELSE
		str += "_female"
	ENDIF
	
	// end number
	str += "^"
	str += iEndNumber
	
	RETURN str
ENDFUNC

FUNC TEXT_LABEL_63 SINGLE_PROPS_BUILD_ANIM_CLIP_TRANSITION_STRING(CASINO_AUDIO_TAGS eIntensityTagStart, CASINO_AUDIO_TAGS eIntensityTagEnd, INT iClipDuration, INT iVersion, BOOL bIsFemale, INT iEndNumber)
	TEXT_LABEL_63 str
	
	SWITCH eIntensityTagStart
		CASE CASINO_AUDIO_TAG_NULL
		CASE CASINO_AUDIO_TAG_LOW
			SWITCH eIntensityTagEnd
				CASE CASINO_AUDIO_TAG_MEDIUM							
					str = "trans_dance_prop_li_to_mi_"
				BREAK
				CASE CASINO_AUDIO_TAG_HIGH	
				CASE CASINO_AUDIO_TAG_HIGH_HANDS						
					str = "trans_dance_prop_li_to_hi_"
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AUDIO_TAG_MEDIUM	
			SWITCH eIntensityTagEnd
				CASE CASINO_AUDIO_TAG_NULL
				CASE CASINO_AUDIO_TAG_LOW
					str = "trans_dance_prop_mi_to_li_"
				BREAK
				CASE CASINO_AUDIO_TAG_HIGH	
				CASE CASINO_AUDIO_TAG_HIGH_HANDS						
					str = "trans_dance_prop_mi_to_hi_"
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AUDIO_TAG_HIGH		
		CASE CASINO_AUDIO_TAG_HIGH_HANDS
			SWITCH eIntensityTagEnd
				CASE CASINO_AUDIO_TAG_NULL
				CASE CASINO_AUDIO_TAG_LOW
					str = "trans_dance_prop_hi_to_li_"
				BREAK
				CASE CASINO_AUDIO_TAG_MEDIUM							
					str = "trans_dance_prop_hi_to_mi_"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF IS_STRING_NULL_OR_EMPTY(str) // Transition is sometimes invalid, do not build a string for it
		RETURN str
	ENDIF
	
	// duration
	IF (iClipDuration < 10)
		str += 0
	ENDIF
	str += iClipDuration
	
	// version
	str += "_v"
	str += iVersion
	
	// male / female
	IF NOT (bIsFemale)
		str += "_male"
	ELSE
		str += "_female"
	ENDIF
	
	// end number
	str += "^"
	str += iEndNumber
	
	RETURN str
ENDFUNC

FUNC TEXT_LABEL_63 AMBIENT_PEDS_BUILD_ANIM_CLIP_STRING(CASINO_AUDIO_TAGS eIntensityTag, INT iClipDuration, INT iVersion, BOOL bIsFemale, INT iEndNumber)
	TEXT_LABEL_63 str
	
	SWITCH eIntensityTag
		CASE CASINO_AUDIO_TAG_NULL
		CASE CASINO_AUDIO_TAG_LOW
		CASE CASINO_AUDIO_TAG_MEDIUM
			str = "li-mi_amb_club_"
		BREAK
		CASE CASINO_AUDIO_TAG_HIGH	
		CASE CASINO_AUDIO_TAG_HIGH_HANDS						
			str = "mi-hi_amb_club_"
		BREAK

	ENDSWITCH
	
	// duration
	IF (iClipDuration < 10)
		str += 0
	ENDIF
	
	str += iClipDuration
	
	IF (iClipDuration = 6)
		str += "_base"
	ELSE
		// version
		str += "_v"
		str += iVersion
	ENDIF
	
	// male / female
	IF NOT (bIsFemale)
		str += "_male"
	ELSE
		str += "_female"
	ENDIF
	
	// end number
	str += "^"
	str += iEndNumber
	
	RETURN str
ENDFUNC

FUNC TEXT_LABEL_63 AMBIENT_PEDS_BUILD_ANIM_CLIP_TRANSITION_STRING(CASINO_AUDIO_TAGS eIntensityTagStart, CASINO_AUDIO_TAGS eIntensityTagEnd, INT iClipDuration, INT iVersion, BOOL bIsFemale, INT iEndNumber)
	TEXT_LABEL_63 str
	
	SWITCH eIntensityTagStart
		CASE CASINO_AUDIO_TAG_NULL
		CASE CASINO_AUDIO_TAG_LOW
		CASE CASINO_AUDIO_TAG_MEDIUM	
			SWITCH eIntensityTagEnd
				CASE CASINO_AUDIO_TAG_HIGH	
				CASE CASINO_AUDIO_TAG_HIGH_HANDS						
					str = "trans_li-mi_to_mi-hi_"
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AUDIO_TAG_HIGH		
		CASE CASINO_AUDIO_TAG_HIGH_HANDS
			SWITCH eIntensityTagEnd
				CASE CASINO_AUDIO_TAG_NULL
				CASE CASINO_AUDIO_TAG_LOW
				CASE CASINO_AUDIO_TAG_MEDIUM							
					str = "trans_mi-hi_to_li-mi_"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF IS_STRING_NULL_OR_EMPTY(str) // Transition is sometimes invalid, do not build a string for it
		RETURN str
	ENDIF
	
	// duration
	IF (iClipDuration < 10)
		str += 0
	ENDIF
	str += iClipDuration
	
	// version
	str += "_v"
	str += iVersion
	
	// male / female
	IF NOT (bIsFemale)
		str += "_male"
	ELSE
		str += "_female"
	ENDIF
	
	// end number
	str += "^"
	str += iEndNumber
	
	RETURN str
ENDFUNC

FUNC TEXT_LABEL_63 BUILD_ANIM_CLIP_TRANSITION_STRING(CASINO_PED_ANIM_DICT eDict, CASINO_AUDIO_TAGS eIntensityTagStart, CASINO_AUDIO_TAGS eIntensityTagEnd, INT iClipDuration, INT iVersion, BOOL bIsFemale, INT iEndNumber)
	SWITCH eDict
		CASE CASINO_ANIM_DICT_SINGLE_PROPS	RETURN SINGLE_PROPS_BUILD_ANIM_CLIP_TRANSITION_STRING(eIntensityTagStart, eIntensityTagEnd, iClipDuration, iVersion, bIsFemale, iEndNumber)
		CASE CASINO_ANIM_DICT_AMBIENT_PEDS	RETURN AMBIENT_PEDS_BUILD_ANIM_CLIP_TRANSITION_STRING(eIntensityTagStart, eIntensityTagEnd, iClipDuration, iVersion, bIsFemale, iEndNumber)
	ENDSWITCH	
	TEXT_LABEL_63 str
	RETURN str
ENDFUNC

FUNC TEXT_LABEL_63 BUILD_ANIM_CLIP_STRING(CASINO_PED_ANIM_DICT eDict, CASINO_AUDIO_TAGS eIntensityTag, INT iClipDuration, INT iVersion, BOOL bIsFemale, INT iEndNumber)
	SWITCH eDict
		CASE CASINO_ANIM_DICT_SINGLE_PROPS	RETURN SINGLE_PROPS_BUILD_ANIM_CLIP_STRING(eIntensityTag, iClipDuration, iVersion, bIsFemale, iEndNumber)
		CASE CASINO_ANIM_DICT_AMBIENT_PEDS	RETURN AMBIENT_PEDS_BUILD_ANIM_CLIP_STRING(eIntensityTag, iClipDuration, iVersion, bIsFemale, iEndNumber)
	ENDSWITCH
	TEXT_LABEL_63 str
	RETURN str
ENDFUNC

FUNC STRING GET_CASINO_PED_TYPE_STRING(CASINO_PED_TYPES ncPedType)
	SWITCH ncPedType
		CASE CASINO_NULL_PED					RETURN "CASINO_NULL_PED"
		CASE CASINO_GENERIC_MALE				RETURN "CASINO_GENERIC_MALE"
		CASE CASINO_SMART_MALE					RETURN "CASINO_SMART_MALE"
		CASE CASINO_GENERIC_FEMALE				RETURN "CASINO_GENERIC_FEMALE"
		CASE CASINO_SMART_FEMALE				RETURN "CASINO_SMART_FEMALE"
		CASE CASINO_MAITRED						RETURN "CASINO_MAITRED"
		CASE CASINO_CASHIER						RETURN "CASINO_CASHIER"
		CASE CASINO_MANAGER						RETURN "CASINO_MANAGER"
		CASE CASINO_SECURITY_GUARD_1			RETURN "CASINO_SECURITY_GUARD_1"
		CASE CASINO_SECURITY_GUARD_2			RETURN "CASINO_SECURITY_GUARD_2"
		CASE CASINO_SECURITY_GUARD_3			RETURN "CASINO_SECURITY_GUARD_3"
		CASE CASINO_SECURITY_GUARD_4			RETURN "CASINO_SECURITY_GUARD_4"
		CASE CASINO_SECURITY_GUARD_5			RETURN "CASINO_SECURITY_GUARD_5"
		CASE CASINO_SECURITY_GUARD_6			RETURN "CASINO_SECURITY_GUARD_6"
		CASE CASINO_FRONT_DESK_GUARD			RETURN "CASINO_FRONT_DESK_GUARD"
		CASE CASINO_DEALER_MALE_1				RETURN "CASINO_DEALER_MALE_1"
		CASE CASINO_DEALER_MALE_2				RETURN "CASINO_DEALER_MALE_2"
		CASE CASINO_DEALER_MALE_3				RETURN "CASINO_DEALER_MALE_3"
		CASE CASINO_DEALER_MALE_4				RETURN "CASINO_DEALER_MALE_4"
		CASE CASINO_DEALER_MALE_5				RETURN "CASINO_DEALER_MALE_5"
		CASE CASINO_DEALER_MALE_6				RETURN "CASINO_DEALER_MALE_6"
		CASE CASINO_DEALER_MALE_7				RETURN "CASINO_DEALER_MALE_7"
		CASE CASINO_DEALER_FEMALE_1				RETURN "CASINO_DEALER_FEMALE_1"
		CASE CASINO_DEALER_FEMALE_2				RETURN "CASINO_DEALER_FEMALE_2"
		CASE CASINO_DEALER_FEMALE_3				RETURN "CASINO_DEALER_FEMALE_3"
		CASE CASINO_DEALER_FEMALE_4				RETURN "CASINO_DEALER_FEMALE_4"
		CASE CASINO_DEALER_FEMALE_5				RETURN "CASINO_DEALER_FEMALE_5"
		CASE CASINO_DEALER_FEMALE_6				RETURN "CASINO_DEALER_FEMALE_6"
		CASE CASINO_DEALER_FEMALE_7				RETURN "CASINO_DEALER_FEMALE_7"
		CASE CASINO_CALEB						RETURN "CASINO_CALEB"
		CASE CASINO_USHI						RETURN "CASINO_USHI"
		CASE CASINO_GABRIEL						RETURN "CASINO_GABRIEL"
		CASE CASINO_VINCE						RETURN "CASINO_VINCE"
		CASE CASINO_DEAN						RETURN "CASINO_DEAN"
		CASE CASINO_CAROL						RETURN "CASINO_CAROL"
		CASE CASINO_BETH						RETURN "CASINO_BETH"
		CASE CASINO_LAUREN						RETURN "CASINO_LAUREN"
		CASE CASINO_TAYLOR						RETURN "CASINO_TAYLOR"
		CASE CASINO_BLANE						RETURN "CASINO_BLANE"
		CASE CASINO_EILEEN						RETURN "CASINO_EILEEN"
		CASE CASINO_CURTIS						RETURN "CASINO_CURTIS"
		CASE MAX_NUMBER_OF_CASINO_PED_TYPES		RETURN "MAX_NUMBER_OF_CASINO_PED_TYPES"
	ENDSWITCH
	RETURN " "
ENDFUNC

FUNC CASINO_PED_TYPES GET_CASINO_BLACKJACK_PED_TYPE(BOOL bFemale = FALSE)
	CASINO_PED_TYPES ePedType
	IF bFemale
		ePedType = CASINO_GENERIC_FEMALE
		IF g_iPedReservedBlackjackTable > 1
			ePedType = CASINO_SMART_FEMALE
		ENDIF
	ELSE
		ePedType = CASINO_GENERIC_MALE
		IF g_iPedReservedBlackjackTable > 1
			ePedType = CASINO_SMART_MALE
		ENDIF
	ENDIF
	RETURN ePedType
ENDFUNC

FUNC CASINO_PED_TYPES GET_CASINO_THREE_CARD_POKER_PED_TYPE(BOOL bFemale = FALSE)
	CASINO_PED_TYPES ePedType
	IF bFemale
		ePedType = CASINO_GENERIC_FEMALE
		IF g_iPedReservedThreeCardPokerTable > 1
			ePedType = CASINO_SMART_FEMALE
		ENDIF
	ELSE
		ePedType = CASINO_GENERIC_MALE
		IF g_iPedReservedThreeCardPokerTable > 1
			ePedType = CASINO_SMART_MALE
		ENDIF
	ENDIF
	RETURN ePedType
ENDFUNC

FUNC CASINO_PED_TYPES GET_CASINO_ROULETTE_PED_TYPE(BOOL bFemale = FALSE)
	CASINO_PED_TYPES ePedType
	IF bFemale
		ePedType = CASINO_GENERIC_FEMALE
		IF g_iPedReservedRouletteTable > 1
			ePedType = CASINO_SMART_FEMALE
		ENDIF
	ELSE
		ePedType = CASINO_GENERIC_MALE
		IF g_iPedReservedRouletteTable > 1
			ePedType = CASINO_SMART_MALE
		ENDIF
	ENDIF
	RETURN ePedType
ENDFUNC

FUNC INT GET_SLOT_MACHINE_PED_PRIORITY(INT iPedID)
	INT iPriority = -1
	SWITCH iPedID
		CASE 79	iPriority = 1	BREAK
		CASE 5	iPriority = 2	BREAK
		CASE 4	iPriority = 3	BREAK
		CASE 6	iPriority = 4	BREAK
		CASE 77	iPriority = 5	BREAK
		CASE 78	iPriority = 6	BREAK
		CASE 80	iPriority = 7	BREAK
		CASE 7	iPriority = 8	BREAK
		CASE 36	iPriority = 9	BREAK
		CASE 35	iPriority = 10	BREAK
	ENDSWITCH
	RETURN iPriority
ENDFUNC

FUNC INT GET_SLOT_MACHINE_ID(INT iPedID)
	INT iPriority = GET_SLOT_MACHINE_PED_PRIORITY(iPedID)
	
	INT iSlotMachine
	INT iCounter = 1
	REPEAT MAX_CASINO_SLOT_MACHINES iSlotMachine
		IF iSlotMachine < 32
			IF IS_BIT_SET(g_iPedReservedSlotMachineBS, iSlotMachine)
				IF iCounter = iPriority
					RETURN iSlotMachine
				ELSE
					iCounter++
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(g_iPedReservedSlotMachineBS2, (iSlotMachine-32))
				IF iCounter = iPriority
					RETURN iSlotMachine
				ELSE
					iCounter++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

FUNC MODEL_NAMES GET_SLOT_MACHINE_MODEL(INT iSlotMachine)
	TEXT_LABEL_31 tlMachineModel = "vw_prop_casino_slot_0"
	SWITCH iSlotMachine
		//main left
		CASE 0 tlMachineModel += "4a"	BREAK
		CASE 1 tlMachineModel += "5a"	BREAK
		CASE 2 tlMachineModel += "6a"	BREAK
		CASE 3 tlMachineModel += "7a"	BREAK
		CASE 4 tlMachineModel += "8a"	BREAK
		
		//main top
		CASE 5 tlMachineModel += "1a"	BREAK
		CASE 6 tlMachineModel += "2a"	BREAK
		CASE 7 tlMachineModel += "3a"	BREAK
		CASE 8 tlMachineModel += "4a"	BREAK
		CASE 9 tlMachineModel += "5a"	BREAK
		CASE 10 tlMachineModel += "6a"	BREAK
		CASE 11 tlMachineModel += "7a"	BREAK
		
		//main right
		CASE 12 tlMachineModel += "1a"	BREAK
		CASE 13 tlMachineModel += "2a"	BREAK
		CASE 14 tlMachineModel += "3a"	BREAK
		CASE 15 tlMachineModel += "4a"	BREAK
		CASE 16 tlMachineModel += "5a"	BREAK
		
		//left front pole
		CASE 17 tlMachineModel += "4a"	BREAK
		CASE 18 tlMachineModel += "5a"	BREAK
		CASE 19 tlMachineModel += "1a"	BREAK
		CASE 20 tlMachineModel += "2a"	BREAK
		CASE 21 tlMachineModel += "3a"	BREAK
		
		//left back pole
		CASE 22 tlMachineModel += "7a"	BREAK
		CASE 23 tlMachineModel += "8a"	BREAK
		CASE 24 tlMachineModel += "4a"	BREAK
		CASE 25 tlMachineModel += "5a"	BREAK
		CASE 26 tlMachineModel += "6a"	BREAK
		
		//right back pole
		CASE 27 tlMachineModel += "4a"	BREAK
		CASE 28 tlMachineModel += "5a"	BREAK
		CASE 29 tlMachineModel += "1a"	BREAK
		CASE 30 tlMachineModel += "2a"	BREAK
		CASE 31 tlMachineModel += "3a"	BREAK
		
		//right front pole
		CASE 32 tlMachineModel += "7a"	BREAK
		CASE 33 tlMachineModel += "8a"	BREAK
		CASE 34 tlMachineModel += "4a"	BREAK
		CASE 35 tlMachineModel += "5a"	BREAK
		CASE 36 tlMachineModel += "6a"	BREAK
		
		//back room bottom wall
		CASE 37 tlMachineModel += "8a"	BREAK
		CASE 38 tlMachineModel += "7a"	BREAK
		CASE 39 tlMachineModel += "6a"	BREAK
		CASE 40 tlMachineModel += "5a"	BREAK
		CASE 41 tlMachineModel += "4a"	BREAK
		CASE 42 tlMachineModel += "3a"	BREAK
		CASE 43 tlMachineModel += "2a"	BREAK
		
		//back room left pole
		CASE 44 tlMachineModel += "3a"	BREAK
		CASE 45 tlMachineModel += "4a"	BREAK
		CASE 46 tlMachineModel += "5a"	BREAK
		CASE 47 tlMachineModel += "1a"	BREAK
		CASE 48 tlMachineModel += "2a"	BREAK
		
		//back room right pole
		CASE 49 tlMachineModel += "6a"	BREAK
		CASE 50	tlMachineModel += "7a"	BREAK
		CASE 51	tlMachineModel += "8a"	BREAK
		CASE 52	tlMachineModel += "4a"	BREAK
		CASE 53	tlMachineModel += "5a"	BREAK
	ENDSWITCH
	RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(tlMachineModel))
ENDFUNC

FUNC CASINO_PED_TYPES GET_CASINO_SLOT_MACHINE_PED_TYPE(INT iPedID, BOOL bFemalePed = FALSE)
	INT iSlotID = GET_SLOT_MACHINE_ID(iPedID)
	MODEL_NAMES eSlotModel = GET_SLOT_MACHINE_MODEL(iSlotID)
	
	CASINO_PED_TYPES ePedType = CASINO_GENERIC_MALE
	IF bFemalePed
		ePedType = CASINO_GENERIC_FEMALE
	ENDIF
	
	IF eSlotModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("VW_PROP_CASINO_SLOT_01A"))	// 'Angel and the Knight'
	OR eSlotModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("VW_PROP_CASINO_SLOT_05A"))	// 'Twilight Knife'
	OR eSlotModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("VW_PROP_CASINO_SLOT_06A"))	// 'Deity of the Sun'
	OR eSlotModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("VW_PROP_CASINO_SLOT_07A"))	// 'Diamond Miner'
		ePedType = CASINO_SMART_MALE
		IF bFemalePed
			ePedType = CASINO_GENERIC_FEMALE
		ENDIF
	ENDIF
	
	RETURN ePedType
ENDFUNC

FUNC CASINO_PED_TYPES GET_CASINO_SLOT_MACHINE_PED_TYPE_FROM_MACHINE_ID(INT iSlotMachine, BOOL bFemalePed = FALSE)
	MODEL_NAMES eSlotModel = GET_SLOT_MACHINE_MODEL(iSlotMachine)
	
	CASINO_PED_TYPES ePedType = CASINO_GENERIC_MALE
	IF bFemalePed
		ePedType = CASINO_GENERIC_FEMALE
	ENDIF
	
	IF eSlotModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("VW_PROP_CASINO_SLOT_01A"))	// 'Angel and the Knight'
	OR eSlotModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("VW_PROP_CASINO_SLOT_05A"))	// 'Twilight Knife'
	OR eSlotModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("VW_PROP_CASINO_SLOT_06A"))	// 'Deity of the Sun'
	OR eSlotModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("VW_PROP_CASINO_SLOT_07A"))	// 'Diamond Miner'
		ePedType = CASINO_SMART_MALE
		IF bFemalePed
			ePedType = CASINO_GENERIC_FEMALE
		ENDIF
	ENDIF
	
	RETURN ePedType
ENDFUNC

FUNC CASINO_PED_TYPES GET_CASINO_DEALER_TYPE_FROM_SERVER_BD(CASINO_DEALER_PED_TYPES eDealerType)
	SWITCH eDealerType
		CASE CASINO_DEALER_MALE_VAR1		RETURN CASINO_DEALER_MALE_1
		CASE CASINO_DEALER_MALE_VAR2		RETURN CASINO_DEALER_MALE_2
		CASE CASINO_DEALER_MALE_VAR3		RETURN CASINO_DEALER_MALE_3
		CASE CASINO_DEALER_MALE_VAR4		RETURN CASINO_DEALER_MALE_4
		CASE CASINO_DEALER_MALE_VAR5		RETURN CASINO_DEALER_MALE_5
		CASE CASINO_DEALER_MALE_VAR6		RETURN CASINO_DEALER_MALE_6
		CASE CASINO_DEALER_MALE_VAR7		RETURN CASINO_DEALER_MALE_7
		CASE CASINO_DEALER_FEMALE_VAR1		RETURN CASINO_DEALER_FEMALE_1
		CASE CASINO_DEALER_FEMALE_VAR2		RETURN CASINO_DEALER_FEMALE_2
		CASE CASINO_DEALER_FEMALE_VAR3		RETURN CASINO_DEALER_FEMALE_3
		CASE CASINO_DEALER_FEMALE_VAR4		RETURN CASINO_DEALER_FEMALE_4
		CASE CASINO_DEALER_FEMALE_VAR5		RETURN CASINO_DEALER_FEMALE_5
		CASE CASINO_DEALER_FEMALE_VAR6		RETURN CASINO_DEALER_FEMALE_6
		CASE CASINO_DEALER_FEMALE_VAR7		RETURN CASINO_DEALER_FEMALE_7
	ENDSWITCH
	RETURN CASINO_NULL_PED
ENDFUNC

FUNC CASINO_PED_TYPES GET_CASINO_BLACKJACK_DEALER_PED_TYPE()
	RETURN GET_CASINO_DEALER_TYPE_FROM_SERVER_BD(g_eBlackjackDealerPeds[g_iPedReservedBlackjackTable])
ENDFUNC

FUNC CASINO_PED_TYPES GET_CASINO_THREE_CARD_POKER_DEALER_PED_TYPE()
	RETURN GET_CASINO_DEALER_TYPE_FROM_SERVER_BD(g_eThreeCardPokerDealerPeds[g_iPedReservedThreeCardPokerTable])
ENDFUNC

FUNC CASINO_PED_TYPES GET_CASINO_ROULETTE_DEALER_PED_TYPE()
	RETURN GET_CASINO_DEALER_TYPE_FROM_SERVER_BD(g_eRouletteDealerPeds[g_iPedReservedRouletteTable])
ENDFUNC

FUNC CASINO_PED_TYPES GET_SECURITY_GUARD_PED_TYPE_FROM_SERVER_BD(CASINO_SECURITY_GUARD_PED_TYPES eGuardType)
	SWITCH eGuardType
		CASE CASINO_SECURITY_GUARD_V1	RETURN CASINO_SECURITY_GUARD_1
		CASE CASINO_SECURITY_GUARD_V2	RETURN CASINO_SECURITY_GUARD_2
		CASE CASINO_SECURITY_GUARD_V3	RETURN CASINO_SECURITY_GUARD_3
		CASE CASINO_SECURITY_GUARD_V4	RETURN CASINO_SECURITY_GUARD_4
		CASE CASINO_SECURITY_GUARD_V5	RETURN CASINO_SECURITY_GUARD_5
		CASE CASINO_SECURITY_GUARD_V6	RETURN CASINO_SECURITY_GUARD_6
	ENDSWITCH
	RETURN CASINO_NULL_PED
ENDFUNC

FUNC INT GET_CASINO_SECURITY_GUARD_ARRAY_INDEX(INT iPedID)
	SWITCH iPedID
		CASE 2 		RETURN 0
		CASE 17		RETURN 1
		CASE 20		RETURN 2
		CASE 123	RETURN 3
		CASE 124	RETURN 4
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC CASINO_PED_TYPES GET_CASINO_SECURITY_GUARD_PED_TYPE(INT iPedID)
	INT iArrayIndex = GET_CASINO_SECURITY_GUARD_ARRAY_INDEX(iPedID)
	RETURN GET_SECURITY_GUARD_PED_TYPE_FROM_SERVER_BD(g_eSecurityGuardPeds[iArrayIndex])
ENDFUNC


FUNC CASINO_PED_TYPES GET_CASINO_SLOT_MACHINE_SPECTATOR_PED_TYPE()
	RETURN INT_TO_ENUM(CASINO_PED_TYPES, g_iSpectatingSlotMachinePedType)
ENDFUNC

FUNC CASINO_PED_TYPES GET_CASINO_PED_TYPE(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea #IF IS_DEBUG_BUILD, BOOL bDebugPrints = FALSE #ENDIF)
	
	#IF IS_DEBUG_BUILD
	IF bDebugPrints
		PRINTLN("[CASINO_PEDS] GET_CASINO_PED_TYPE - Ped: ", iPedID, " Layout: ", iLayout, " Casino Area: ", GET_CASINO_AREA_STRING(eCasinoArea))
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_CASINO_PED_DEBUG_EDITOR_ACTIVE()
		IF g_iCasinoPedType[iPedID] != -1
			#IF IS_DEBUG_BUILD
			IF bDebugPrints
				PRINTLN("[CASINO_PEDS] GET_CASINO_PED_TYPE - Debug Ped Type: ", GET_CASINO_PED_TYPE_STRING(INT_TO_ENUM(CASINO_PED_TYPES, g_iCasinoPedType[iPedID])))
			ENDIF
			#ENDIF
			RETURN INT_TO_ENUM(CASINO_PED_TYPES, g_iCasinoPedType[iPedID])
		ENDIF
	ENDIF
	#ENDIF
	
	CASINO_PED_TYPES pedType = CASINO_NULL_PED
	IF OVERRIDE_CASINO_SPECIAL_PED_TYPE(iPedID, iLayout, eCasinoArea,pedType)
	AND NOT g_bCasinoIntroCutsceneRequired
		#IF IS_DEBUG_BUILD
		IF bDebugPrints
			PRINTLN("[CASINO_PEDS] GET_CASINO_PED_TYPE - Special Ped Type: ", GET_CASINO_PED_TYPE_STRING(pedType))
		ENDIF
		#ENDIF
		RETURN pedType
	ENDIF
	
	SWITCH eCasinoArea
		CASE CASINO_AREA_PERMANENT
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						CASE 0		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 1		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 2		pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)					BREAK
						CASE 3		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 4		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID, TRUE)				BREAK
						CASE 5		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID, TRUE)				BREAK
						CASE 6		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID, TRUE)				BREAK
						CASE 7		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID, TRUE)				BREAK
						CASE 8		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 9		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 10		pedType = CASINO_NULL_PED												BREAK
						CASE 11		pedType = CASINO_SMART_MALE												BREAK
						CASE 12		pedType = GET_CASINO_SLOT_MACHINE_SPECTATOR_PED_TYPE()					BREAK
						CASE 13		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 14		pedType = CASINO_SMART_MALE												BREAK
						CASE 15		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 16		pedType = CASINO_SMART_MALE												BREAK
						CASE 17		pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)					BREAK
						CASE 18		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 19		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 20		pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)					BREAK
						CASE 21		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 22		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 23		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 24		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 25		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 26		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 27		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 28		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 29		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 30		pedType = CASINO_SMART_MALE												BREAK
						CASE 31		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 32		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 33		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 34		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 35		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 36		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 37		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 38		pedType = CASINO_GENERIC_MALE											BREAK
					ENDSWITCH																				
				BREAK																						
				CASE 2																						
					SWITCH iPedID																			
						CASE 0		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 1		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 2		pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)					BREAK
						CASE 3		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 4		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID, TRUE)				BREAK
						CASE 5		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID, TRUE)				BREAK
						CASE 6		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID, TRUE)				BREAK
						CASE 7		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID, TRUE)				BREAK
						CASE 8		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 9		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 10		pedType = CASINO_NULL_PED												BREAK
						CASE 11		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 12		pedType = GET_CASINO_SLOT_MACHINE_SPECTATOR_PED_TYPE()					BREAK
						CASE 13		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 14		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 15		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 16		pedType = CASINO_SMART_MALE												BREAK
						CASE 17		pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)					BREAK
						CASE 18		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 19		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 20		pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)					BREAK
						CASE 21		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 22		pedType = CASINO_SMART_MALE												BREAK
						CASE 23		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 24		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 25		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 26		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 27		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 28		pedType = CASINO_SMART_MALE												BREAK
						CASE 29		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 30		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 31		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 32		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 33		pedType = CASINO_SMART_MALE												BREAK
						CASE 34		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 35		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 36		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 37		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 38		pedType = CASINO_GENERIC_MALE											BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPedID
						CASE 0		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 1		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 2		pedType =  CASINO_GENERIC_MALE				BREAK
						CASE 3		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 4		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 5		pedType =  CASINO_GENERIC_MALE				BREAK
						CASE 6		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 7		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 8		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 9		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 10		pedType =  CASINO_NULL_PED					BREAK
						CASE 11		pedType =  CASINO_GENERIC_MALE				BREAK
						CASE 12		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 13		pedType =  CASINO_GENERIC_MALE				BREAK
						CASE 14		pedType =  CASINO_GENERIC_MALE				BREAK
						CASE 15		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 16		pedType =  CASINO_GENERIC_MALE				BREAK
						CASE 17		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 18		pedType =  CASINO_GENERIC_MALE				BREAK
						CASE 19		pedType =  CASINO_GENERIC_FEMALE			BREAK
						CASE 20		pedType =  CASINO_GENERIC_MALE				BREAK
						CASE 21		pedType =  CASINO_GENERIC_MALE				BREAK
						CASE 22		pedType =  CASINO_GENERIC_MALE				BREAK
					ENDSWITCH
				BREAK
				CASE 4
					SWITCH iPedID
						// Permnent
						CASE 0	pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID, TRUE)				BREAK
						CASE 1	pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID, TRUE)				BREAK
						// Table Games
						CASE 2	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 3	pedType = CASINO_DEALER_FEMALE_5										BREAK
						CASE 4	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 5	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 6	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 7	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 8	pedType = CASINO_DEALER_FEMALE_3										BREAK
						CASE 9	pedType = CASINO_DEALER_MALE_2											BREAK
						CASE 10	pedType = CASINO_DEALER_MALE_5											BREAK
						CASE 11	pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 12	pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 13	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 14	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 15	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 16	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 17	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 18	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 19	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 20	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 21	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 22	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 23	pedType = CASINO_SMART_MALE												BREAK
						CASE 24	pedType = CASINO_SMART_MALE												BREAK
						CASE 25	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 26	pedType = CASINO_SMART_MALE												BREAK
						CASE 27	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 28	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 29	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 30	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 31	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 32	pedType = CASINO_SECURITY_GUARD_1										BREAK
						CASE 33	pedType = CASINO_DEALER_MALE_4											BREAK
						CASE 34	pedType = CASINO_DEALER_FEMALE_1										BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_MAIN_FLOOR
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Entrance (15)
						CASE 39		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 40		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 41		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 42		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 43		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 44		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 45		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 46		pedType = CASINO_SMART_MALE												BREAK
						CASE 47		pedType = CASINO_SMART_MALE												BREAK
						CASE 48		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 49		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 50		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 51		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 52		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 53		pedType = CASINO_GENERIC_MALE											BREAK
						// Behind Bar (13)																	
						CASE 54		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 55		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 56		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 57		pedType = CASINO_SMART_MALE												BREAK
						CASE 58		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 59		pedType = CASINO_FRONT_DESK_GUARD										BREAK
						CASE 60		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 61		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 62		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 63		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 64		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 65		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 66		pedType = CASINO_GENERIC_MALE											BREAK
						// All the rest (53)																
						CASE 67		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 68		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 69		pedType = CASINO_SMART_MALE												BREAK
						CASE 70		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 71		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 72		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 73		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 74		pedType = CASINO_SMART_MALE												BREAK
						CASE 75		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 76		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 77		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 78		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 79		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 80		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
//						CASE 81		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 82		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 83		pedType = CASINO_SMART_MALE												BREAK
						CASE 84		pedType = CASINO_SMART_MALE												BREAK
						CASE 85		pedType = CASINO_SMART_MALE												BREAK
						CASE 86		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 87		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 88		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 89		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 90		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 91		pedType = CASINO_SMART_MALE												BREAK
						CASE 92		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 93		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 94		pedType = CASINO_SMART_MALE												BREAK
						CASE 95		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 96		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 97		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 98		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 99		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 100	pedType = CASINO_SMART_MALE												BREAK
						CASE 101	pedType = CASINO_SMART_MALE												BREAK
						CASE 102	pedType = CASINO_SMART_MALE												BREAK
						CASE 103	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 104	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 105	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 106	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 107	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 108	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 109	pedType = CASINO_SMART_MALE												BREAK
						CASE 110	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 111	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 112	pedType = CASINO_SMART_MALE												BREAK
						CASE 113	pedType = CASINO_SMART_MALE												BREAK
						CASE 114	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 115	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 116	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 117	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 118	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 119	pedType = CASINO_SMART_MALE												BREAK
						// Special peds (5)																	
						CASE 120	pedType = CASINO_CASHIER												BREAK
						CASE 121	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 122	pedType = CASINO_MAITRED												BREAK
						CASE 123	pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)					BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						CASE 39		pedType = CASINO_SMART_MALE												BREAK
						CASE 40		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 41		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 42		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 43		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 44		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 45		pedType = CASINO_SMART_MALE												BREAK
						CASE 46		pedType = CASINO_SMART_MALE												BREAK
						CASE 47		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 48		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 49		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 50		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 51		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 52		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 53		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 54		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 55		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 56		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 57		pedType = CASINO_SMART_MALE												BREAK
						CASE 58		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 59		pedType = CASINO_FRONT_DESK_GUARD										BREAK
						CASE 60		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 61		pedType = CASINO_SMART_MALE												BREAK
						CASE 62		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 63		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 64		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 65		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 66		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 67		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 68		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 69		pedType = CASINO_SMART_MALE												BREAK
						CASE 70		pedType = CASINO_SMART_MALE												BREAK
						CASE 71		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 72		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 73		pedType = CASINO_SMART_MALE												BREAK
						CASE 74		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 75		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 76		pedType = CASINO_SMART_MALE												BREAK
						CASE 77		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 78		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 79		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 80		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
//						CASE 81		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 82		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 83		pedType = CASINO_SMART_MALE												BREAK
						CASE 84		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 85		pedType = CASINO_SMART_MALE												BREAK
						CASE 86		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 87		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 88		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 89		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 90		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 91		pedType = CASINO_SMART_MALE												BREAK
						CASE 92		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 93		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 94		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 95		pedType = CASINO_SMART_MALE												BREAK
						CASE 96		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 97		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 98		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 99		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 100	pedType = CASINO_SMART_MALE												BREAK
						CASE 101	pedType = CASINO_SMART_MALE												BREAK
						CASE 102	pedType = CASINO_SMART_MALE												BREAK
						CASE 103	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 104	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 105	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 106	pedType = CASINO_SMART_MALE												BREAK
						CASE 107	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 108	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 109	pedType = CASINO_SMART_MALE												BREAK
						CASE 110	pedType = CASINO_SMART_MALE												BREAK
						CASE 111	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 112	pedType = CASINO_SMART_MALE												BREAK
						CASE 113	pedType = CASINO_SMART_MALE												BREAK
						CASE 114	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 115	pedType = CASINO_SMART_MALE												BREAK
						CASE 116	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 117	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 118	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 119	pedType = CASINO_GENERIC_MALE											BREAK
						// Special peds (5)																	
						CASE 120	pedType = CASINO_CASHIER												BREAK
						CASE 121	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 122	pedType = CASINO_MAITRED												BREAK
						CASE 123	pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)					BREAK
					ENDSWITCH																				
				BREAK																						
				CASE 3
					SWITCH iPedID
						CASE 39		pedType =  CASINO_NULL_PED												BREAK
						CASE 40		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 41		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 42		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 43		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 44		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 45		pedType =  CASINO_CURTIS												BREAK
						CASE 46		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 47		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 48		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 49		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 50		pedType =  CASINO_SMART_MALE											BREAK
						CASE 51		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 52		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 53		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 54		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 55		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 56		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 57		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 58		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 59		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 60		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 61		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 62		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 63		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 64		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 65		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 66		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 67		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 68		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 69		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 70		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 71		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 72		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 73		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 74		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 75		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 76		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 77		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 78		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 79		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 80		pedType =  CASINO_SMART_MALE											BREAK
						CASE 81		pedType =  CASINO_SMART_MALE											BREAK
						CASE 82		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 83		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 84		pedType =  CASINO_SMART_FEMALE											BREAK
						CASE 85		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 86		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 87		pedType =  CASINO_SMART_MALE											BREAK
						CASE 88		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 89		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 90		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 91		pedType =  CASINO_SMART_MALE											BREAK
						CASE 92		pedType =  CASINO_GENERIC_MALE											BREAK
						CASE 93		pedType =  CASINO_GENERIC_FEMALE										BREAK
						CASE 94		pedType =  CASINO_GENERIC_FEMALE										BREAK
					ENDSWITCH																						
				BREAK
				CASE 4
					SWITCH iPedID
						// Entrance (15)
						CASE 39		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 40		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 41		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 42		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 43		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 44		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 45		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 46		pedType = CASINO_SMART_MALE												BREAK
						CASE 47		pedType = CASINO_SMART_MALE												BREAK
						CASE 48		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 49		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 50		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 51		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 52		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 53		pedType = CASINO_GENERIC_MALE											BREAK
						// Behind Bar (13)																	
						CASE 54		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 55		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 56		pedType = CASINO_DEALER_MALE_1											BREAK
						CASE 57		pedType = CASINO_SMART_MALE												BREAK
						CASE 58		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 59		pedType = CASINO_DEALER_FEMALE_6										BREAK
						CASE 60		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 61		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 62		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 63		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 64		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 65		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 66		pedType = CASINO_GENERIC_MALE											BREAK
						// All the rest (53)																
						CASE 67		pedType = CASINO_DEALER_MALE_6											BREAK
						CASE 68		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 69		pedType = CASINO_SMART_MALE												BREAK
						CASE 70		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 71		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 72		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 73		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 74		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 75		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 76		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 77		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 78		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 79		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 80		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)						BREAK
						CASE 81		pedType = CASINO_DEALER_FEMALE_2										BREAK		
						CASE 82		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 83		pedType = CASINO_SMART_MALE												BREAK
						CASE 84		pedType = CASINO_SMART_MALE												BREAK
						CASE 85		pedType = CASINO_SMART_MALE												BREAK
						CASE 86		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 87		pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 90		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 91		pedType = CASINO_SMART_MALE												BREAK
						CASE 92		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 93		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 94		pedType = CASINO_SMART_MALE												BREAK
						CASE 95		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 96		pedType = CASINO_SMART_FEMALE											BREAK
						CASE 98		pedType = CASINO_GENERIC_MALE											BREAK
						CASE 100	pedType = CASINO_SMART_MALE												BREAK
						CASE 101	pedType = CASINO_SMART_MALE												BREAK
						CASE 102	pedType = CASINO_SMART_MALE												BREAK
						CASE 103	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 104	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 105	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 106	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 107	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 108	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 109	pedType = CASINO_SMART_MALE												BREAK
						CASE 110	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 111	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 112	pedType = CASINO_SMART_MALE												BREAK
						CASE 113	pedType = CASINO_SMART_MALE												BREAK
						CASE 114	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 115	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 116	pedType = CASINO_GENERIC_MALE											BREAK
						CASE 117	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 118	pedType = CASINO_SMART_FEMALE											BREAK
						CASE 119	pedType = CASINO_SMART_MALE												BREAK
						// Special peds (5)																	
						CASE 120	pedType = CASINO_CASHIER												BREAK
						CASE 121	pedType = CASINO_GENERIC_FEMALE											BREAK
						CASE 122	pedType = CASINO_MAITRED												BREAK
						CASE 123	pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)					BREAK
						
					ENDSWITCH
				BREAK
			ENDSWITCH																								
		BREAK																										
		CASE CASINO_AREA_TABLE_GAMES																				
			SWITCH iLayout																							
				CASE 1																								
					SWITCH iPedID																					
						// Need to include Sports Betting area here for culling from the Main Floor
						// Sports Betting
						CASE 39		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 40		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 41		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 42		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 43		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 44		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 45		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 46		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 47		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 48		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 49		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 50		pedType = CASINO_SMART_MALE														BREAK
						CASE 51		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 52		pedType = CASINO_SMART_MALE														BREAK
						CASE 53		pedType = CASINO_SMART_MALE														BREAK
						// Lounge																					BREAK
//						CASE 54		pedType = CASINO_GENERIC_FEMALE													BREAK
//						CASE 55		pedType = CASINO_GENERIC_FEMALE													BREAK
//						CASE 56		pedType = CASINO_GENERIC_FEMALE													BREAK
//						CASE 57		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 58		pedType = CASINO_SMART_MALE														BREAK
						CASE 59		pedType = CASINO_SMART_MALE														BREAK
						CASE 60		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 61		pedType = CASINO_SMART_FEMALE													BREAK
//						CASE 62		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 63		pedType = CASINO_SMART_MALE														BREAK
//						CASE 64		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 65		pedType = CASINO_SMART_MALE														BREAK
						CASE 66		pedType = CASINO_GENERIC_MALE													BREAK
						// Table Games																				
						// Dealers																					
						CASE 67		pedType = GET_CASINO_BLACKJACK_DEALER_PED_TYPE()								BREAK
						CASE 68		pedType = GET_CASINO_THREE_CARD_POKER_DEALER_PED_TYPE()							BREAK
						CASE 69		pedType = GET_CASINO_ROULETTE_DEALER_PED_TYPE()									BREAK
//						CASE 70		pedType = CASINO_GENERIC_FEMALE													BREAK
//						CASE 71		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 72		pedType = GET_CASINO_BLACKJACK_PED_TYPE(TRUE)									BREAK
						CASE 73		pedType = GET_CASINO_BLACKJACK_PED_TYPE(TRUE)									BREAK
						CASE 74		pedType = GET_CASINO_BLACKJACK_PED_TYPE()										BREAK
						CASE 75		pedType = GET_CASINO_BLACKJACK_PED_TYPE(TRUE)									BREAK
						CASE 76		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 77		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 78		pedType = GET_CASINO_ROULETTE_PED_TYPE(TRUE)									BREAK
						CASE 79		pedType = GET_CASINO_ROULETTE_PED_TYPE()										BREAK
						CASE 80		pedType = GET_CASINO_ROULETTE_PED_TYPE()										BREAK
						CASE 81		pedType = GET_CASINO_ROULETTE_PED_TYPE()										BREAK
						CASE 82		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 83		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 84		pedType = GET_CASINO_THREE_CARD_POKER_PED_TYPE()								BREAK
						CASE 85		pedType = GET_CASINO_THREE_CARD_POKER_PED_TYPE()								BREAK
						CASE 86		pedType = GET_CASINO_THREE_CARD_POKER_PED_TYPE(TRUE)							BREAK
						CASE 87		pedType = GET_CASINO_THREE_CARD_POKER_PED_TYPE(TRUE)							BREAK
						CASE 88		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 89		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 90		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 91		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 92		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 93		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 94		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 95		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 96		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 97		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 98		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 99		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 100	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 101	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 102	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 103	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 104	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 105	pedType = CASINO_SMART_MALE														BREAK
						CASE 106	pedType = CASINO_SMART_MALE														BREAK
						CASE 107	pedType = CASINO_SMART_MALE														BREAK
						CASE 108	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 109	pedType = CASINO_SMART_MALE														BREAK
						CASE 110	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 111	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 112	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 113	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 114	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 121	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 123	pedType = CASINO_SMART_FEMALE													BREAK
						// Bouncer
						CASE 124	pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)							BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Need to include Sports Betting area here for culling from the Main Floor
						// Sports Betting
						CASE 39		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 40		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 41		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 42		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 43		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 44		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 45		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 46		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 47		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 48		pedType = CASINO_SMART_MALE														BREAK
						CASE 49		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 50		pedType = CASINO_SMART_MALE														BREAK
						CASE 51		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 52		pedType = CASINO_SMART_MALE														BREAK
						CASE 53		pedType = CASINO_GENERIC_MALE													BREAK
						// Lounge																					
//						CASE 54		pedType = CASINO_GENERIC_FEMALE													BREAK
//						CASE 55		pedType = CASINO_GENERIC_FEMALE													BREAK
//						CASE 56		pedType = CASINO_GENERIC_FEMALE													BREAK
//						CASE 57		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 58		pedType = CASINO_SMART_MALE														BREAK
						CASE 59		pedType = CASINO_SMART_MALE														BREAK
						CASE 60		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 61		pedType = CASINO_SMART_FEMALE													BREAK
//						CASE 62		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 63		pedType = CASINO_SMART_MALE														BREAK
//						CASE 64		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 65		pedType = CASINO_SMART_MALE														BREAK
						CASE 66		pedType = CASINO_GENERIC_MALE													BREAK
						// Table Games																				
						// Dealers																					
						CASE 67		pedType = GET_CASINO_BLACKJACK_DEALER_PED_TYPE()								BREAK
						CASE 68		pedType = GET_CASINO_THREE_CARD_POKER_DEALER_PED_TYPE()							BREAK
						CASE 69		pedType = GET_CASINO_ROULETTE_DEALER_PED_TYPE()									BREAK
																													
//						CASE 67		pedType = CASINO_GENERIC_FEMALE													BREAK
//						CASE 68		pedType = CASINO_GENERIC_FEMALE													BREAK
//						CASE 69		pedType = CASINO_GENERIC_MALE													BREAK
//						CASE 70		pedType = CASINO_GENERIC_FEMALE													BREAK
//						CASE 71		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 72		pedType = GET_CASINO_BLACKJACK_PED_TYPE(TRUE)									BREAK
						CASE 73		pedType = GET_CASINO_BLACKJACK_PED_TYPE(TRUE)									BREAK
						CASE 74		pedType = GET_CASINO_BLACKJACK_PED_TYPE()										BREAK
						CASE 75		pedType = GET_CASINO_BLACKJACK_PED_TYPE(TRUE)									BREAK
						CASE 76		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 77		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 78		pedType = GET_CASINO_ROULETTE_PED_TYPE(TRUE)									BREAK
						CASE 79		pedType = GET_CASINO_ROULETTE_PED_TYPE()										BREAK
						CASE 80		pedType = GET_CASINO_ROULETTE_PED_TYPE()										BREAK
						CASE 81		pedType = GET_CASINO_ROULETTE_PED_TYPE()										BREAK
						CASE 82		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 83		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 84		pedType = GET_CASINO_THREE_CARD_POKER_PED_TYPE()								BREAK
						CASE 85		pedType = GET_CASINO_THREE_CARD_POKER_PED_TYPE()								BREAK
						CASE 86		pedType = GET_CASINO_THREE_CARD_POKER_PED_TYPE(TRUE)							BREAK
						CASE 87		pedType = GET_CASINO_THREE_CARD_POKER_PED_TYPE(TRUE)							BREAK
						CASE 88		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 89		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 90		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 91		pedType = CASINO_SMART_MALE														BREAK
						CASE 92		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 93		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 94		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 95		pedType = CASINO_SMART_MALE														BREAK
						CASE 96		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 97		pedType = CASINO_SMART_MALE														BREAK
						CASE 98		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 99		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 100	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 101	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 102	pedType = CASINO_SMART_MALE														BREAK
						CASE 103	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 104	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 105	pedType = CASINO_SMART_MALE														BREAK
						CASE 106	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 107	pedType = CASINO_SMART_MALE														BREAK
						CASE 108	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 109	pedType = CASINO_SMART_MALE														BREAK
						CASE 110	pedType = CASINO_SMART_MALE														BREAK
						CASE 111	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 112	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 113	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 114	pedType = CASINO_SMART_MALE														BREAK
						CASE 121	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 123	pedType = CASINO_SMART_FEMALE													BREAK
						// Bouncer
						CASE 124	pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)							BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPedID
						CASE 39		pedType =  CASINO_DEALER_MALE_1													BREAK
						CASE 40		pedType =  CASINO_DEALER_FEMALE_1												BREAK
						CASE 41		pedType =  CASINO_DEALER_MALE_2													BREAK
						CASE 42		pedType =  CASINO_SMART_MALE													BREAK
						CASE 43		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 44		pedType =  CASINO_DEALER_FEMALE_2												BREAK
						CASE 45		pedType =  CASINO_SMART_MALE													BREAK
						CASE 46		pedType =  CASINO_SMART_MALE													BREAK
						CASE 47		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 48		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 49		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 50		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 51		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 52		pedType =  CASINO_DEALER_MALE_3													BREAK
						CASE 53		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 54		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 55		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 56		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 57		pedType =  CASINO_DEALER_MALE_4													BREAK
						CASE 58		pedType =  CASINO_DEALER_MALE_5													BREAK
						CASE 59		pedType =  CASINO_DEALER_FEMALE_3												BREAK
						CASE 60		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 61		pedType =  CASINO_SMART_MALE													BREAK
						CASE 62		pedType =  CASINO_SMART_MALE													BREAK
						CASE 63		pedType =  CASINO_DEALER_FEMALE_4												BREAK
						CASE 64		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 65		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 66		pedType =  CASINO_NULL_PED														BREAK
						CASE 67		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 68		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 69		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 70		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 71		pedType =  CASINO_SMART_MALE													BREAK
						CASE 72		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 73		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 74		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 75		pedType =  CASINO_SMART_MALE													BREAK
						CASE 76		pedType =  CASINO_NULL_PED														BREAK
						CASE 77		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 78		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 79		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 80		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 81		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 82		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 83		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 84		pedType =  CASINO_SMART_MALE													BREAK
						CASE 85		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 86		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 87		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 88		pedType =  CASINO_GENERIC_MALE													BREAK
						//CASE 89		pedType =  CASINO_GENERIC_MALE												BREAK
						CASE 90		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 91		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 92		pedType =  CASINO_GENERIC_MALE													BREAK
						CASE 93		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 94		pedType =  CASINO_GENERIC_FEMALE												BREAK
						CASE 95		pedType =  CASINO_DEALER_FEMALE_5												BREAK
						CASE 96		pedType =  CASINO_DEALER_MALE_6													BREAK
						CASE 97		pedType =  CASINO_DEALER_FEMALE_6												BREAK
						CASE 98		pedType =  CASINO_DEALER_MALE_7													BREAK
						CASE 99		pedType =  CASINO_DEALER_FEMALE_7												BREAK
					ENDSWITCH																						
				BREAK																								
			ENDSWITCH																								
		BREAK																										
		CASE CASINO_AREA_SPORTS_BETTING																				
			SWITCH iLayout																							
				CASE 1																								
					SWITCH iPedID																					
						// Sports Betting
						CASE 39		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 40		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 41		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 42		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 43		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 44		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 45		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 46		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 47		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 48		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 49		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 50		pedType = CASINO_SMART_MALE														BREAK
						CASE 51		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 52		pedType = CASINO_SMART_MALE														BREAK
						CASE 53		pedType = CASINO_SMART_MALE														BREAK
						// Need to include most main floor area here for culling from the Table Games				
						// Behind Bar (13)																			
						CASE 54		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 55		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 56		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 57		pedType = CASINO_SMART_MALE														BREAK
						CASE 58		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 59		pedType = CASINO_FRONT_DESK_GUARD												BREAK
						CASE 60		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 61		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 62		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 63		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 64		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 65		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 66		pedType = CASINO_GENERIC_MALE													BREAK
						// All the rest (53)																		
						CASE 67		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 68		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 69		pedType = CASINO_SMART_MALE														BREAK
						CASE 70		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 71		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 72		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 73		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 74		pedType = CASINO_SMART_MALE														BREAK
						CASE 75		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 76		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 77		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 78		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 79		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 80		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
//						CASE 81		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 82		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 83		pedType = CASINO_SMART_MALE														BREAK
						CASE 84		pedType = CASINO_SMART_MALE														BREAK
						CASE 85		pedType = CASINO_SMART_MALE														BREAK
						CASE 86		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 87		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 88		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 89		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 90		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 91		pedType = CASINO_SMART_MALE														BREAK
						CASE 92		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 93		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 94		pedType = CASINO_SMART_MALE														BREAK
						CASE 95		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 96		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 97		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 98		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 99		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 100	pedType = CASINO_SMART_MALE														BREAK
						CASE 101	pedType = CASINO_SMART_MALE														BREAK
						CASE 102	pedType = CASINO_SMART_MALE														BREAK
						CASE 103	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 104	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 105	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 106	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 107	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 108	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 109	pedType = CASINO_SMART_MALE														BREAK
						CASE 110	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 111	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 112	pedType = CASINO_SMART_MALE														BREAK
						CASE 113	pedType = CASINO_SMART_MALE														BREAK
						CASE 114	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 115	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 116	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 117	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 118	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 119	pedType = CASINO_SMART_MALE														BREAK
						// Special peds (5)
						CASE 120	pedType = CASINO_CASHIER														BREAK
						CASE 121	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 122	pedType = CASINO_MAITRED														BREAK
						CASE 123	pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)							BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Sports Betting
						CASE 39		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 40		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 41		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 42		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 43		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 44		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 45		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 46		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 47		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 48		pedType = CASINO_SMART_MALE														BREAK
						CASE 49		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 50		pedType = CASINO_SMART_MALE														BREAK
						CASE 51		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 52		pedType = CASINO_SMART_MALE														BREAK
						CASE 53		pedType = CASINO_GENERIC_MALE													BREAK
						// Need to include most main floor area here for culling from the Table Games				
						// Behind Bar (13)																			
						CASE 54		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 55		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 56		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 57		pedType = CASINO_SMART_MALE														BREAK
						CASE 58		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 59		pedType = CASINO_FRONT_DESK_GUARD												BREAK
						CASE 60		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 61		pedType = CASINO_SMART_MALE														BREAK
						CASE 62		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 63		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 64		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 65		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 66		pedType = CASINO_GENERIC_MALE													BREAK
						// All the rest (53)																		
						CASE 67		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 68		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 69		pedType = CASINO_SMART_MALE														BREAK
						CASE 70		pedType = CASINO_SMART_MALE														BREAK
						CASE 71		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 72		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 73		pedType = CASINO_SMART_MALE														BREAK
						CASE 74		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 75		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 76		pedType = CASINO_SMART_MALE														BREAK
						CASE 77		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 78		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 79		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 80		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
//						CASE 81		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 82		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 83		pedType = CASINO_SMART_MALE														BREAK
						CASE 84		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 85		pedType = CASINO_SMART_MALE														BREAK
						CASE 86		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 87		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 88		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 89		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 90		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 91		pedType = CASINO_SMART_MALE														BREAK
						CASE 92		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 93		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 94		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 95		pedType = CASINO_SMART_MALE														BREAK
						CASE 96		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 97		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 98		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 99		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 100	pedType = CASINO_SMART_MALE														BREAK
						CASE 101	pedType = CASINO_SMART_MALE														BREAK
						CASE 102	pedType = CASINO_SMART_MALE														BREAK
						CASE 103	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 104	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 105	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 106	pedType = CASINO_SMART_MALE														BREAK
						CASE 107	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 108	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 109	pedType = CASINO_SMART_MALE														BREAK
						CASE 110	pedType = CASINO_SMART_MALE														BREAK
						CASE 111	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 112	pedType = CASINO_SMART_MALE														BREAK
						CASE 113	pedType = CASINO_SMART_MALE														BREAK
						CASE 114	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 115	pedType = CASINO_SMART_MALE														BREAK
						CASE 116	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 117	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 118	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 119	pedType = CASINO_GENERIC_MALE													BREAK
						// Special peds (5)																			
						CASE 120	pedType = CASINO_CASHIER														BREAK
						CASE 121	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 122	pedType = CASINO_MAITRED														BREAK
						CASE 123	pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)							BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_MANAGERS_OFFICE
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Entrance (15)
						CASE 39		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 40		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 41		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 42		pedType = CASINO_GENERIC_FEMALE													BREAK
						// Manager																					
						CASE 43		pedType = CASINO_MANAGER														BREAK
						CASE 44		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 45		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 46		pedType = CASINO_SMART_MALE														BREAK
						CASE 47		pedType = CASINO_SMART_MALE														BREAK
						CASE 48		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 49		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 50		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 51		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 52		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 53		pedType = CASINO_GENERIC_MALE													BREAK
						// Behind Bar (13)																			
						CASE 54		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 55		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 56		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 57		pedType = CASINO_SMART_MALE														BREAK
						CASE 58		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 59		pedType = CASINO_FRONT_DESK_GUARD												BREAK
						CASE 60		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 61		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 62		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 63		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 64		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 65		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 66		pedType = CASINO_GENERIC_MALE													BREAK
						// All the rest (53)																		
						CASE 67		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 68		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 69		pedType = CASINO_SMART_MALE														BREAK
						CASE 70		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 71		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 72		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 73		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 74		pedType = CASINO_SMART_MALE														BREAK
						CASE 75		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 76		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 77		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 78		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 79		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 80		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
//						CASE 81		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 82		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 83		pedType = CASINO_SMART_MALE														BREAK
						CASE 84		pedType = CASINO_SMART_MALE														BREAK
						CASE 85		pedType = CASINO_SMART_MALE														BREAK
						CASE 86		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 87		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 88		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 89		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 90		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 91		pedType = CASINO_SMART_MALE														BREAK
						CASE 92		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 93		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 94		pedType = CASINO_SMART_MALE														BREAK
						CASE 95		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 96		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 97		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 98		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 99		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 100	pedType = CASINO_SMART_MALE														BREAK
						CASE 101	pedType = CASINO_SMART_MALE														BREAK
						CASE 102	pedType = CASINO_SMART_MALE														BREAK
						CASE 103	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 104	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 105	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 106	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 107	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 108	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 109	pedType = CASINO_SMART_MALE														BREAK
						CASE 110	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 111	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 112	pedType = CASINO_SMART_MALE														BREAK
						CASE 113	pedType = CASINO_SMART_MALE														BREAK
						CASE 114	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 115	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 116	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 117	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 118	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 119	pedType = CASINO_SMART_MALE														BREAK
						// Special peds (5)																			
						CASE 120	pedType = CASINO_CASHIER														BREAK
						CASE 121	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 122	pedType = CASINO_MAITRED														BREAK
						CASE 123	pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)							BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						CASE 39		pedType = CASINO_SMART_MALE														BREAK
						CASE 40		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 41		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 42		pedType = CASINO_GENERIC_MALE													BREAK
						// Manager																					
						CASE 43		pedType = CASINO_MANAGER														BREAK
						CASE 44		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 45		pedType = CASINO_SMART_MALE														BREAK
						CASE 46		pedType = CASINO_SMART_MALE														BREAK
						CASE 47		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 48		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 49		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 50		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 51		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 52		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 53		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 54		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 55		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 56		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 57		pedType = CASINO_SMART_MALE														BREAK
						CASE 58		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 59		pedType = CASINO_FRONT_DESK_GUARD												BREAK
						CASE 60		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 61		pedType = CASINO_SMART_MALE														BREAK
						CASE 62		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 63		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 64		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 65		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 66		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 67		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 68		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 69		pedType = CASINO_SMART_MALE														BREAK
						CASE 70		pedType = CASINO_SMART_MALE														BREAK
						CASE 71		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 72		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 73		pedType = CASINO_SMART_MALE														BREAK
						CASE 74		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 75		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 76		pedType = CASINO_SMART_MALE														BREAK
						CASE 77		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 78		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 79		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
						CASE 80		pedType = GET_CASINO_SLOT_MACHINE_PED_TYPE(iPedID)								BREAK
//						CASE 81		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 82		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 83		pedType = CASINO_SMART_MALE														BREAK
						CASE 84		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 85		pedType = CASINO_SMART_MALE														BREAK
						CASE 86		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 87		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 88		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 89		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 90		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 91		pedType = CASINO_SMART_MALE														BREAK
						CASE 92		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 93		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 94		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 95		pedType = CASINO_SMART_MALE														BREAK
						CASE 96		pedType = CASINO_SMART_FEMALE													BREAK
						CASE 97		pedType = CASINO_GENERIC_MALE													BREAK
						CASE 98		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 99		pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 100	pedType = CASINO_SMART_MALE														BREAK
						CASE 101	pedType = CASINO_SMART_MALE														BREAK
						CASE 102	pedType = CASINO_SMART_MALE														BREAK
						CASE 103	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 104	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 105	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 106	pedType = CASINO_SMART_MALE														BREAK
						CASE 107	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 108	pedType = CASINO_GENERIC_MALE													BREAK
						CASE 109	pedType = CASINO_SMART_MALE														BREAK
						CASE 110	pedType = CASINO_SMART_MALE														BREAK
						CASE 111	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 112	pedType = CASINO_SMART_MALE														BREAK
						CASE 113	pedType = CASINO_SMART_MALE														BREAK
						CASE 114	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 115	pedType = CASINO_SMART_MALE														BREAK
						CASE 116	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 117	pedType = CASINO_SMART_FEMALE													BREAK
						CASE 118	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 119	pedType = CASINO_GENERIC_MALE													BREAK
						// Special peds (5)																			
						CASE 120	pedType = CASINO_CASHIER														BREAK
						CASE 121	pedType = CASINO_GENERIC_FEMALE													BREAK
						CASE 122	pedType = CASINO_MAITRED														BREAK
						CASE 123	pedType = GET_CASINO_SECURITY_GUARD_PED_TYPE(iPedID)							BREAK
					ENDSWITCH																						
				BREAK																								
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugPrints
		PRINTLN("[CASINO_PEDS] GET_CASINO_PED_TYPE - Ped Type: ", GET_CASINO_PED_TYPE_STRING(pedType))
	ENDIF
	#ENDIF
	
	RETURN pedType
ENDFUNC

FUNC CASINO_ACTIVITY_SLOTS GET_MANAGERS_ACTIVITY()
	SWITCH g_iManagerPedVariation
		CASE 0 	RETURN CASINO_AC_SLOT_MANAGER_PHONE
		CASE 1 	RETURN CASINO_AC_SLOT_MANAGER_LEAN
		CASE 2 	RETURN CASINO_AC_SLOT_MANAGER_SIT
		CASE 3 	RETURN CASINO_AC_SLOT_MANAGER_BEHIND_DESK
		CASE 4	RETURN CASINO_AC_SLOT_MANAGER_DRINK
	ENDSWITCH
	RETURN CASINO_AC_SLOT_NULL
ENDFUNC

FUNC CASINO_ACTIVITY_SLOTS GET_BLACKJACK_PED_ACTIVITY(INT iPedID)
	CASINO_ACTIVITY_SLOTS eBjackActivity
	SWITCH iPedID
		CASE 72	eBjackActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iBlackjackPedActivitiesID[0])	BREAK
		CASE 73	eBjackActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iBlackjackPedActivitiesID[1])	BREAK
		CASE 74	eBjackActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iBlackjackPedActivitiesID[2])	BREAK
		CASE 75	eBjackActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iBlackjackPedActivitiesID[3])	BREAK
	ENDSWITCH
	RETURN eBjackActivity
ENDFUNC

FUNC CASINO_ACTIVITY_SLOTS GET_INSIDE_TRACK_PED_ACTIVITY(INT iPedID)
	CASINO_ACTIVITY_SLOTS eInsideTrackActivity
	SWITCH iPedID
		CASE 39	eInsideTrackActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iInsideTrackPedActivitiesID[0])	BREAK
		CASE 40	eInsideTrackActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iInsideTrackPedActivitiesID[1])	BREAK
		CASE 41	eInsideTrackActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iInsideTrackPedActivitiesID[2])	BREAK
	ENDSWITCH
	RETURN eInsideTrackActivity
ENDFUNC

FUNC CASINO_ACTIVITY_SLOTS GET_POKER_PED_ACTIVITY(INT iPedID)
	CASINO_ACTIVITY_SLOTS ePokerActivity
	SWITCH iPedID
		CASE 84	ePokerActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iPokerPedActivitiesID[0])	BREAK
		CASE 85	ePokerActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iPokerPedActivitiesID[1])	BREAK
		CASE 86	ePokerActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iPokerPedActivitiesID[2])	BREAK
		CASE 87	ePokerActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iPokerPedActivitiesID[3])	BREAK
	ENDSWITCH
	RETURN ePokerActivity
ENDFUNC

FUNC CASINO_ACTIVITY_SLOTS GET_SLOT_MACHINE_PED_ACTIVITY(INT iPedID)
	CASINO_ACTIVITY_SLOTS eSlotMachineActivity
	SWITCH iPedID
		CASE 79	eSlotMachineActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachinePedActivitiesID[0])		BREAK
		CASE 5	eSlotMachineActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachinePedActivitiesID[1])		BREAK
		CASE 4	eSlotMachineActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachinePedActivitiesID[2])		BREAK
		CASE 6	eSlotMachineActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachinePedActivitiesID[3])		BREAK
		CASE 77	eSlotMachineActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachinePedActivitiesID[4])		BREAK
		CASE 78	eSlotMachineActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachinePedActivitiesID[5])		BREAK
		CASE 80	eSlotMachineActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachinePedActivitiesID[6])		BREAK
		CASE 7	eSlotMachineActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachinePedActivitiesID[7])		BREAK
		CASE 36	eSlotMachineActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachinePedActivitiesID[8])		BREAK
		CASE 35	eSlotMachineActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachinePedActivitiesID[9])		BREAK
		CASE 37	eSlotMachineActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachinePedActivitiesID[10])	BREAK
	ENDSWITCH
	RETURN eSlotMachineActivity
ENDFUNC

FUNC CASINO_ACTIVITY_SLOTS GET_SLOT_MACHINE_SPECTATE_ACTIVITY()
	RETURN INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachineSpectatorActivity)
ENDFUNC

FUNC INT GET_ROULETTE_PED_ID(INT iSeat)
	SWITCH iSeat
		CASE 0 	RETURN 81
		CASE 1	RETURN 80
		CASE 2	RETURN 79
		CASE 3	RETURN 78
		case 4	return 69 // dealer
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT GET_ROULETTE_SEAT_ID(INT iPedID)
	SWITCH iPedID
		CASE 81	RETURN 0
		CASE 80	RETURN 1
		CASE 79 RETURN 2
		CASE 78	RETURN 3
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC CASINO_ACTIVITY_SLOTS GET_ROULETTE_PED_ACTIVITY(INT iPedID)
	INT iSeatID = GET_ROULETTE_SEAT_ID(iPedID)
	IF (iSeatID != -1)
		RETURN INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iRoulettePedActivitiesID[iSeatID])
	ENDIF
	RETURN CASINO_AC_SLOT_NULL
ENDFUNC

FUNC INT GET_TABLE_GAME_SPECTATOR_PED_ID(INT iPedID)
	SWITCH iPedID
		CASE 0	RETURN 88
		CASE 1	RETURN 89
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC CASINO_ACTIVITY_SLOTS GET_TABLE_GAME_SPECTATOR_PED_ACTIVITY(INT iPedID)
	SWITCH iPedID
		CASE 88	RETURN INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iTableGameSpectatorActivities[0])
		CASE 89	RETURN INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iTableGameSpectatorActivities[1])
	ENDSWITCH
	RETURN CASINO_AC_SLOT_NULL
ENDFUNC

FUNC CASINO_ACTIVITY_SLOTS GET_CASINO_PED_ACTIVITY_SLOT(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea #IF IS_DEBUG_BUILD, BOOL bDebugPrints = FALSE #ENDIF)
	
	#IF IS_DEBUG_BUILD
	IF bDebugPrints
		PRINTLN("[CASINO_PEDS] GET_CASINO_PED_ACTIVITY_SLOT - Ped: ", iPedID, " Layout: ", iLayout, " Casino Area: ", GET_CASINO_AREA_STRING(eCasinoArea))
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_CASINO_PED_DEBUG_EDITOR_ACTIVE()
		IF g_iCasinoActivitySlot[iPedID] != -1
			#IF IS_DEBUG_BUILD
			IF bDebugPrints
				PRINTLN("[CASINO_PEDS] GET_CASINO_PED_ACTIVITY_SLOT - Debug Activity: ", GET_CASINO_PED_ACTIVITY_STRING(INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iCasinoActivitySlot[iPedID])))
			ENDIF
			#ENDIF
			RETURN INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iCasinoActivitySlot[iPedID])
		ENDIF
	ENDIF
	#ENDIF
	
	CASINO_ACTIVITY_SLOTS activitySlot = CASINO_AC_SLOT_NULL
	IF OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT(iPedID, iLayout, eCasinoArea,activitySlot)
	AND NOT g_bCasinoIntroCutsceneRequired
		#IF IS_DEBUG_BUILD
		IF bDebugPrints
			PRINTLN("[CASINO_PEDS] GET_CASINO_PED_ACTIVITY_SLOT - Special Ped Activity: ", GET_CASINO_PED_ACTIVITY_STRING(activitySlot))
		ENDIF
		#ENDIF
		RETURN activitySlot
	ENDIF
	
	SWITCH eCasinoArea
		CASE CASINO_AREA_PERMANENT
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						CASE 0		activitySlot = CASINO_AC_SLOT_AMBIENT_F_1						BREAK
						CASE 1		activitySlot = CASINO_AC_SLOT_AMBIENT_F_1						BREAK
						CASE 2		activitySlot = CASINO_AC_SLOT_GUARDING							BREAK
						CASE 3		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A				BREAK
						CASE 4		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 5		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 6		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 7		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 8		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 9		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3A				BREAK
						CASE 10		activitySlot = CASINO_AC_SLOT_GUARDING							BREAK
						CASE 11		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B				BREAK
						CASE 12		activitySlot = GET_SLOT_MACHINE_SPECTATE_ACTIVITY()				BREAK
						CASE 13		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 14		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 15		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B				BREAK
						CASE 16		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B						BREAK
						CASE 17		activitySlot = CASINO_AC_SLOT_BOUNCER							BREAK
						CASE 18		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B						BREAK
						CASE 19		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 20		activitySlot = CASINO_AC_SLOT_BOUNCER							BREAK
						CASE 21		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 22		activitySlot = CASINO_AC_SLOT_HANGOUT							BREAK
						CASE 23		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING					BREAK
						CASE 24		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING					BREAK
						CASE 25		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 26		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 27		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1A						BREAK
						CASE 28		activitySlot = CASINO_AC_SLOT_HANGOUT							BREAK
						CASE 29		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2A						BREAK
						CASE 30		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A				BREAK
						CASE 31		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B				BREAK
						CASE 32		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A				BREAK
						CASE 33		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 34		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B						BREAK
						CASE 35		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 36		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 37		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 38		activitySlot = CASINO_AC_SLOT_TEXT_IDLE							BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						CASE 0		activitySlot = CASINO_AC_SLOT_AMBIENT_F_1						BREAK
						CASE 1		activitySlot = CASINO_AC_SLOT_AMBIENT_F_1						BREAK
						CASE 2		activitySlot = CASINO_AC_SLOT_GUARDING							BREAK
						CASE 3		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING					BREAK
						CASE 4		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 5		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 6		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 7		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 8		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B						BREAK
						CASE 9		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A				BREAK
						CASE 10		activitySlot = CASINO_AC_SLOT_GUARDING							BREAK
						CASE 11		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B				BREAK
						CASE 12		activitySlot = GET_SLOT_MACHINE_SPECTATE_ACTIVITY()				BREAK
						CASE 13		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1A				BREAK
						CASE 14		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A						BREAK
						CASE 15		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3A				BREAK
						CASE 16		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B						BREAK
						CASE 17		activitySlot = CASINO_AC_SLOT_BOUNCER							BREAK
						CASE 18		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B						BREAK
						CASE 19		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A				BREAK
						CASE 20		activitySlot = CASINO_AC_SLOT_BOUNCER							BREAK
						CASE 21		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3A						BREAK
						CASE 22		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 23		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING					BREAK
						CASE 24		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B						BREAK
						CASE 25		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A						BREAK
						CASE 26		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A		BREAK
						CASE 27		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 28		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1A						BREAK
						CASE 29		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3A				BREAK
						CASE 30		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 31		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B			BREAK
						CASE 32		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A						BREAK
						CASE 33		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1A				BREAK
						CASE 34		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B						BREAK
						CASE 35		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 36		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 37		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 38		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1A				BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPedID
						CASE 0		activitySlot = CASINO_AC_SLOT_AMBIENT_F_1						BREAK
						CASE 1		activitySlot = CASINO_AC_SLOT_AMBIENT_F_1						BREAK	
						CASE 2		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 3		activitySlot = CASINO_AC_SLOT_SIT_DRINKING						BREAK
						CASE 4		activitySlot = CASINO_AC_SLOT_SIT_DRINKING						BREAK
						CASE 5		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 6		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 7		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 8		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 9		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3A				BREAK
						CASE 10		activitySlot = CASINO_AC_SLOT_NULL								BREAK
						CASE 11		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B				BREAK
						CASE 12		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 13		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 14		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 15		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B				BREAK
						CASE 16		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B						BREAK
						CASE 17		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 18		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 19		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 20		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 21		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 22		activitySlot = CASINO_AC_SLOT_HANGOUT							BREAK
					ENDSWITCH																	
				BREAK
				CASE 4
					SWITCH iPedID
						// Permnent
						CASE 0	activitySlot = CASINO_AC_SLOT_MACHINE_REGULAR_01A				BREAK
						CASE 1	activitySlot = CASINO_AC_SLOT_MACHINE_ENGAGED_01B				BREAK
						// Table Games		
						CASE 2	activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 3	activitySlot = CASINO_AC_SLOT_BLACKJACK_DEALER					BREAK
						CASE 4	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A				BREAK
						CASE 5	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 6	activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A						BREAK
						CASE 7	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1A				BREAK
						CASE 8	activitySlot = CASINO_AC_SLOT_ROULETTE_DEALER					BREAK
						CASE 9	activitySlot = CASINO_AC_SLOT_ROULETTE_DEALER					BREAK
						CASE 10	activitySlot = CASINO_AC_SLOT_BLACKJACK_DEALER					BREAK
						CASE 11	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 12	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B						BREAK
						CASE 13	activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 14	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B				BREAK
						CASE 15	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 16	activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B						BREAK
						CASE 17	activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B						BREAK
						CASE 18	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 19	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A				BREAK
						CASE 20	activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A						BREAK
						CASE 21	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B				BREAK
						CASE 22	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1B				BREAK
						CASE 23	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B				BREAK
						CASE 24	activitySlot = CASINO_AC_SLOT_HANGOUT_M_1A						BREAK
						CASE 25	activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B						BREAK
						CASE 26	activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B						BREAK
						CASE 27	activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A						BREAK
						CASE 28	activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B						BREAK
						CASE 29	activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B						BREAK
						CASE 30	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B				BREAK
						CASE 31	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B				BREAK																
						CASE 32	activitySlot = CASINO_AC_SLOT_BOUNCER							BREAK
						CASE 33	activitySlot = CASINO_AC_SLOT_BLACKJACK_DEALER					BREAK
						CASE 34	activitySlot = CASINO_AC_SLOT_BLACKJACK_DEALER					BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH																			
		BREAK																					
		CASE CASINO_AREA_MAIN_FLOOR																
			SWITCH iLayout																		
				CASE 1
					SWITCH iPedID
						// Entrance (15)
						CASE 39		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B					BREAK
						CASE 40		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 41		activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A			BREAK
						CASE 42		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B					BREAK
						CASE 43		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 44		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1B					BREAK
						CASE 45		activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 46		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A					BREAK
						CASE 47		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B							BREAK
						CASE 48		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 49		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1A					BREAK
						CASE 50		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B							BREAK
						CASE 51		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B							BREAK
						CASE 52		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B							BREAK
						CASE 53		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1A					BREAK
						// Behind Bar (13)																
						CASE 54		activitySlot = CASINO_AC_SLOT_SIT_DRINKING							BREAK
						CASE 55		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1				BREAK
						CASE 56		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1				BREAK
						CASE 57		activitySlot = CASINO_AC_SLOT_SIT_DRINKING							BREAK
						CASE 58		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A				BREAK
						CASE 59		activitySlot = CASINO_AC_SLOT_SECURITY_GUARD						BREAK
						CASE 60		activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 61		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B					BREAK
						CASE 62		activitySlot = CASINO_AC_SLOT_OUTOFMONEY_M_1A						BREAK
						CASE 63		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B					BREAK
						CASE 64		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A					BREAK
						CASE 65		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A					BREAK
						CASE 66		activitySlot = CASINO_AC_SLOT_SIT_DRINKING							BREAK
						// All the rest (53)															
						CASE 67		activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 68		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A							BREAK
						CASE 69		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 70		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B				BREAK
						CASE 71		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 72		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B							BREAK
						CASE 73		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A							BREAK
						CASE 74		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1A							BREAK
						CASE 75		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A					BREAK
						CASE 76		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B					BREAK
						CASE 77		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)				BREAK
						CASE 78		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)				BREAK
						CASE 79		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)				BREAK
						CASE 80		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)				BREAK
//						CASE 81		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A					BREAK
						CASE 82		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B							BREAK
						CASE 83		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A							BREAK
						CASE 84		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 85		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A					BREAK
						CASE 86		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A							BREAK
						CASE 87		activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 88		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B				BREAK
						CASE 89		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A			BREAK
						CASE 90		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A			BREAK
						CASE 91		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B					BREAK
						CASE 92		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A							BREAK
						CASE 93		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B							BREAK
						CASE 94		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 95		activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A			BREAK
						CASE 96		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1				BREAK
						CASE 97		activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 98		activitySlot = CASINO_AC_SLOT_SHOP_MALE_01A							BREAK
						CASE 99		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A					BREAK
						CASE 100	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 101	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 102	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 103	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A				BREAK
						CASE 104	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B			BREAK
						CASE 105	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B			BREAK
						CASE 106	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A					BREAK
						CASE 107	activitySlot = CASINO_AC_SLOT_HANGOUT								BREAK
						CASE 108	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 109	activitySlot = CASINO_AC_SLOT_BAR_MALE_SIT_WITH_DRINK_01A			BREAK
						CASE 110	activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 111	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B					BREAK
						CASE 112	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 113	activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B							BREAK
						CASE 114	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B					BREAK
						CASE 115	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 116	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A					BREAK
						CASE 117	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A					BREAK
						CASE 118	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A					BREAK
						CASE 119	activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B							BREAK
						// Special peds (5)																
						CASE 120	activitySlot = CASINO_AC_SLOT_CASHIER								BREAK
						CASE 121	activitySlot = CASINO_AC_SLOT_AMBIENT_F_1							BREAK
						CASE 122	activitySlot = CASINO_AC_SLOT_MAITRED								BREAK
						CASE 123	activitySlot = CASINO_AC_SLOT_GUARDING								BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						CASE 39		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B				BREAK
						CASE 40		activitySlot = CASINO_AC_SLOT_OUTOFMONEY_M_2B					BREAK
						CASE 41		activitySlot = CASINO_AC_SLOT_BAR_MALE_SIT_WITH_DRINK_01A		BREAK
						CASE 42		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A				BREAK
						CASE 43		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING					BREAK
						CASE 44		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A				BREAK
						CASE 45		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B				BREAK
						CASE 46		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A				BREAK
						CASE 47		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B						BREAK
						CASE 48		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1A						BREAK
						CASE 49		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING					BREAK
						CASE 50		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B				BREAK
						CASE 51		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A				BREAK
						CASE 52		activitySlot = CASINO_AC_SLOT_SHOP_MALE_01A						BREAK
						CASE 53		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B						BREAK
						CASE 54		activitySlot = CASINO_AC_SLOT_SIT_DRINKING						BREAK
						CASE 55		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1			BREAK
						CASE 56		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1			BREAK
						CASE 57		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B						BREAK
						CASE 58		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A			BREAK
						CASE 59		activitySlot = CASINO_AC_SLOT_SECURITY_GUARD					BREAK
						CASE 60		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A			BREAK
						CASE 61		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B				BREAK
						CASE 62		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B			BREAK
						CASE 63		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B				BREAK
						CASE 64		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B				BREAK
						CASE 65		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A				BREAK
						CASE 66		activitySlot = CASINO_AC_SLOT_SIT_DRINKING						BREAK
						CASE 67		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B			BREAK
						CASE 68		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B						BREAK
						CASE 69		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 70		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B						BREAK
						CASE 71		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A			BREAK
						CASE 72		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A			BREAK
						CASE 73		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A				BREAK
						CASE 74		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B						BREAK
						CASE 75		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B			BREAK
						CASE 76		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 77		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 78		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 79		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 80		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
//						CASE 81		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A				BREAK
						CASE 82		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A				BREAK
						CASE 83		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A						BREAK
						CASE 84		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2A						BREAK
						CASE 85		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A				BREAK
						CASE 86		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 87		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 88		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B			BREAK
						CASE 89		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A		BREAK
						CASE 90		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A		BREAK
						CASE 91		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B				BREAK
						CASE 92		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 93		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B				BREAK
						CASE 94		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 95		activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A		BREAK
						CASE 96		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1			BREAK
						CASE 97		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A						BREAK
						CASE 98		activitySlot = CASINO_AC_SLOT_SHOP_FEMALE_01A					BREAK
						CASE 99		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A				BREAK
						CASE 100	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 101	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 102	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 103	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A			BREAK
						CASE 104	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B		BREAK
						CASE 105	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B			BREAK
						CASE 106	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 107	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A				BREAK
						CASE 108	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 109	activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A		BREAK
						CASE 110	activitySlot = CASINO_AC_SLOT_LEANING_TEXTING					BREAK
						CASE 111	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B				BREAK
						CASE 112	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 113	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 114	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B		BREAK
						CASE 115	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 116	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 117	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B				BREAK
						CASE 118	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1B				BREAK
						CASE 119	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01B		BREAK
						// Special peds (5)															
						CASE 120	activitySlot = CASINO_AC_SLOT_CASHIER							BREAK
						CASE 121	activitySlot = CASINO_AC_SLOT_AMBIENT_F_1						BREAK
						CASE 122	activitySlot = CASINO_AC_SLOT_MAITRED							BREAK
						CASE 123	activitySlot = CASINO_AC_SLOT_GUARDING							BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPedID
						CASE 39		activitySlot =  CASINO_AC_SLOT_NULL								BREAK
						CASE 40		activitySlot =  CASINO_AC_SLOT_SIT_DRINKING						BREAK
						CASE 41		activitySlot =  CASINO_AC_SLOT_SIT_DRINKING						BREAK
						CASE 42		activitySlot =  CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1			BREAK
						CASE 43		activitySlot =  CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1			BREAK
						CASE 44		activitySlot =  CASINO_AC_SLOT_SIT_DRINKING						BREAK
						CASE 45		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_02			BREAK
						CASE 46		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_01			BREAK
						CASE 47		activitySlot =  CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_M_1			BREAK
						CASE 48		activitySlot =  CASINO_AC_SLOT_BAR_FEMALE_GAWK_01				BREAK
						CASE 49		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_02			BREAK
						CASE 50		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_03			BREAK
						CASE 51		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_02			BREAK
						CASE 52		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_03			BREAK
						CASE 53		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_02			BREAK
						CASE 54		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_04			BREAK
						CASE 55		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_03			BREAK
						CASE 56		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_03			BREAK
						CASE 57		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_01			BREAK
						CASE 58		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_03			BREAK
						CASE 59		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_02			BREAK
						CASE 60		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_04			BREAK
						CASE 61		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_03			BREAK
						CASE 62		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_01			BREAK
						CASE 63		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_02			BREAK
						CASE 64		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_04			BREAK
						CASE 65		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_04			BREAK
						CASE 66		activitySlot =  CASINO_AC_SLOT_NULL								BREAK
						CASE 67		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_03			BREAK
						CASE 68		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_03			BREAK
						CASE 69		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_01			BREAK
						CASE 70		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_02			BREAK
						CASE 71		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_05			BREAK
						CASE 72		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_04			BREAK
						CASE 73		activitySlot =  CASINO_AC_SLOT_STANDING_FEMALE_GAWK_02			BREAK
						CASE 74		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_01			BREAK
						CASE 75		activitySlot =  CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_M_2			BREAK
						CASE 76		activitySlot =  CASINO_AC_SLOT_NULL								BREAK
						CASE 77		activitySlot =  CASINO_AC_SLOT_HANGOUT_CONVO_M_1B				BREAK
						CASE 78		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_07			BREAK
						CASE 79		activitySlot =  CASINO_AC_SLOT_BAR_MALE_GAWK_01					BREAK
						CASE 80		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_06			BREAK
						CASE 81		activitySlot =  CASINO_AC_SLOT_STANDING_MALE_GAWK_04			BREAK
						CASE 82		activitySlot =  CASINO_AC_SLOT_MACHINE_FEMALE_GAWK_02			BREAK
						CASE 83		activitySlot =  CASINO_AC_SLOT_MACHINE_FEMALE_GAWK_01			BREAK
						CASE 84		activitySlot =  CASINO_AC_SLOT_MACHINE_FEMALE_GAWK_02			BREAK
						CASE 85		activitySlot =  CASINO_AC_SLOT_MACHINE_FEMALE_GAWK_05			BREAK
						CASE 86		activitySlot =  CASINO_AC_SLOT_MACHINE_REGULAR_02B				BREAK
						CASE 87		activitySlot =  CASINO_AC_SLOT_MACHINE_REGULAR_02B				BREAK
						CASE 88		activitySlot =  CASINO_AC_SLOT_DRINK							BREAK
						CASE 89		activitySlot =  CASINO_AC_SLOT_DRINK							BREAK
						CASE 90		activitySlot =  CASINO_AC_SLOT_DRINK							BREAK
						CASE 91		activitySlot =  CASINO_AC_SLOT_MACHINE_FEMALE_GAWK_02			BREAK
						CASE 92		activitySlot =  CASINO_AC_SLOT_HANGOUT_CONVO_M_1B				BREAK
						CASE 93		activitySlot =  CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 94		activitySlot =  CASINO_AC_SLOT_HANGOUT_F_2B						BREAK
					ENDSWITCH																		
				BREAK
				CASE 4
					SWITCH iPedID
						// Entrance (15)
						CASE 39		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B					BREAK
						CASE 40		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 41		activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A			BREAK
						CASE 42		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B					BREAK
						CASE 43		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 44		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1B					BREAK
						CASE 45		activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 46		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A					BREAK
						CASE 47		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B							BREAK
						CASE 48		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 49		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1A					BREAK
						CASE 50		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B							BREAK
						CASE 51		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B							BREAK
						CASE 52		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B							BREAK
						CASE 53		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1A					BREAK
						// Behind Bar (13)																
						CASE 54		activitySlot = CASINO_AC_SLOT_SIT_DRINKING							BREAK
						CASE 55		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1				BREAK
						CASE 56		activitySlot = CASINO_AC_SLOT_ROULETTE_DEALER						BREAK
						CASE 57		activitySlot = CASINO_AC_SLOT_SIT_DRINKING							BREAK
						CASE 58		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A				BREAK
						CASE 59		activitySlot = CASINO_AC_SLOT_ROULETTE_DEALER						BREAK
						CASE 60		activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 61		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B					BREAK
						CASE 62		activitySlot = CASINO_AC_SLOT_OUTOFMONEY_M_1A						BREAK
						CASE 63		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B					BREAK
						CASE 64		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A					BREAK
						CASE 65		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A					BREAK
						CASE 66		activitySlot = CASINO_AC_SLOT_SIT_DRINKING							BREAK
						// All the rest (53)															
						CASE 67		activitySlot = CASINO_AC_SLOT_BLACKJACK_DEALER						BREAK
						CASE 68		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A							BREAK
						CASE 69		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 70		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B				BREAK
						CASE 71		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 72		activitySlot = CASINO_AC_SLOT_BLACKJACK_ENGAGED_01B					BREAK
						CASE 73		activitySlot = CASINO_AC_SLOT_BLACKJACK_REGULAR_01B					BREAK
						CASE 74		activitySlot = CASINO_AC_SLOT_BLACKJACK_SLOUCHY_01A					BREAK
						CASE 75		activitySlot = CASINO_AC_SLOT_BLACKJACK_REGULAR_01A					BREAK
						CASE 76		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B					BREAK
						CASE 77		activitySlot = CASINO_AC_SLOT_MACHINE_SLOUCHY_01A					BREAK
						CASE 78		activitySlot = CASINO_AC_SLOT_MACHINE_REGULAR_02B					BREAK
						CASE 79		activitySlot = CASINO_AC_SLOT_MACHINE_REGULAR_02A					BREAK
						CASE 80		activitySlot = CASINO_AC_SLOT_MACHINE_ENGAGED_01A					BREAK
						CASE 81		activitySlot = CASINO_AC_SLOT_BLACKJACK_DEALER						BREAK
						CASE 82		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B							BREAK
						CASE 83		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A							BREAK
						CASE 84		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 85		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A					BREAK
						CASE 86		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A							BREAK
						CASE 87		activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 90		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A			BREAK
						CASE 91		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B					BREAK
						CASE 92		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A							BREAK
						CASE 93		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B							BREAK
						CASE 94		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 95		activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A			BREAK
						CASE 96		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1				BREAK
						CASE 98		activitySlot = CASINO_AC_SLOT_SHOP_MALE_01A							BREAK
						CASE 100	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 101	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 102	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 103	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A				BREAK
						CASE 104	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B			BREAK
						CASE 105	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B			BREAK
						CASE 106	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A					BREAK
						CASE 107	activitySlot = CASINO_AC_SLOT_HANGOUT								BREAK
						CASE 108	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 109	activitySlot = CASINO_AC_SLOT_BAR_MALE_SIT_WITH_DRINK_01A			BREAK
						CASE 110	activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 111	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B					BREAK
						CASE 112	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 113	activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B							BREAK
						CASE 114	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B					BREAK
						CASE 115	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 116	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A					BREAK
						CASE 117	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A					BREAK
						CASE 118	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A					BREAK
						CASE 119	activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B							BREAK
						// Special peds (5)																
						CASE 120	activitySlot = CASINO_AC_SLOT_CASHIER								BREAK
						CASE 121	activitySlot = CASINO_AC_SLOT_AMBIENT_F_1							BREAK
						CASE 122	activitySlot = CASINO_AC_SLOT_MAITRED								BREAK
						CASE 123	activitySlot = CASINO_AC_SLOT_GUARDING								BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH																				
		BREAK																						
		CASE CASINO_AREA_TABLE_GAMES
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Need to include Sports Betting area here for culling from the Main Floor
						// Sports Betting
						CASE 39	activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)				BREAK
						CASE 40	activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)				BREAK
						CASE 41	activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)				BREAK
						CASE 42	activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B							BREAK
						CASE 43	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 44	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 45	activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 46	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 47	activitySlot = CASINO_AC_SLOT_HANGOUT								BREAK
						CASE 48	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B				BREAK
						CASE 49	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 50	activitySlot = CASINO_AC_SLOT_HANGOUT_M_3A							BREAK
						CASE 51	activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A							BREAK
						CASE 52	activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B							BREAK
						CASE 53	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B					BREAK
						// Lounge																	
//						CASE 54	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
//						CASE 55	activitySlot = CASINO_AC_SLOT_SMOKE									BREAK
//						CASE 56	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
//						CASE 57	activitySlot = CASINO_AC_SLOT_SMOKE									BREAK
						CASE 58	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 59	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 60	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A				BREAK
						CASE 61	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B			BREAK
//						CASE 62	activitySlot = CASINO_AC_SLOT_SMOKE									BREAK
						CASE 63	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
//						CASE 64	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 65	activitySlot = CASINO_AC_SLOT_SIT_DRINKING							BREAK
						CASE 66	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B					BREAK
						// Table Games																
						// Dealers																	
						CASE 67	activitySlot = CASINO_AC_SLOT_BLACKJACK_DEALER						BREAK
						CASE 68	activitySlot = CASINO_AC_SLOT_POKER_DEALER							BREAK
						CASE 69	activitySlot = CASINO_AC_SLOT_ROULETTE_DEALER						BREAK
//						CASE 70	activitySlot = CASINO_AC_SLOT_SIT_DRINKING							BREAK
//						CASE 71	activitySlot = CASINO_AC_SLOT_SIT_DRINKING							BREAK
						CASE 72	activitySlot = GET_BLACKJACK_PED_ACTIVITY(iPedID)					BREAK
						CASE 73	activitySlot = GET_BLACKJACK_PED_ACTIVITY(iPedID)					BREAK
						CASE 74	activitySlot = GET_BLACKJACK_PED_ACTIVITY(iPedID)					BREAK
						CASE 75	activitySlot = GET_BLACKJACK_PED_ACTIVITY(iPedID)					BREAK
						CASE 76	activitySlot = GET_TABLE_GAME_SPECTATOR_PED_ACTIVITY(iPedID)		BREAK
						CASE 77	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 78	activitySlot = GET_ROULETTE_PED_ACTIVITY(iPedID)					BREAK
						CASE 79	activitySlot = GET_ROULETTE_PED_ACTIVITY(iPedID)					BREAK
						CASE 80	activitySlot = GET_ROULETTE_PED_ACTIVITY(iPedID)					BREAK
						CASE 81	activitySlot = GET_ROULETTE_PED_ACTIVITY(iPedID)					BREAK
						CASE 82	activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A							BREAK
						CASE 83	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1A					BREAK
						CASE 84	activitySlot = GET_POKER_PED_ACTIVITY(iPedID)						BREAK
						CASE 85	activitySlot = GET_POKER_PED_ACTIVITY(iPedID)						BREAK
						CASE 86	activitySlot = GET_POKER_PED_ACTIVITY(iPedID)						BREAK
						CASE 87	activitySlot = GET_POKER_PED_ACTIVITY(iPedID)						BREAK
						CASE 88	activitySlot = GET_TABLE_GAME_SPECTATOR_PED_ACTIVITY(iPedID)		BREAK
						CASE 89	activitySlot = GET_TABLE_GAME_SPECTATOR_PED_ACTIVITY(iPedID)		BREAK
						CASE 90	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 91	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B					BREAK
						CASE 92	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A							BREAK
						CASE 93	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A							BREAK
						CASE 94	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B							BREAK
						CASE 95	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B					BREAK
						CASE 96	activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B							BREAK
						CASE 97	activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B							BREAK
						CASE 98	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 99	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A					BREAK
						CASE 100	activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A						BREAK
						CASE 101	activitySlot = CASINO_AC_SLOT_HANGOUT_F_2A						BREAK
						CASE 102	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B				BREAK
						CASE 103	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B				BREAK
						CASE 104	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1B				BREAK
						CASE 105	activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B						BREAK
						CASE 106	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B				BREAK
						CASE 107	activitySlot = CASINO_AC_SLOT_HANGOUT_M_1A						BREAK
						CASE 108	activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B						BREAK
						CASE 109	activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B						BREAK
						CASE 110	activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A						BREAK
						CASE 111	activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B						BREAK
						CASE 112	activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B						BREAK
						CASE 113	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B				BREAK
						CASE 114	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B				BREAK
						CASE 121	activitySlot = CASINO_AC_SLOT_TOILET_CHECK_OUT_MIRROR			BREAK
						CASE 123	activitySlot = CASINO_AC_SLOT_LEANING_TEXTING					BREAK
						// Bouncer																	
						CASE 124	activitySlot = CASINO_AC_SLOT_BOUNCER							BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Need to include Sports Betting area here for culling from the Main Floor
						// Sports Betting
						CASE 39		activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)				BREAK
						CASE 40		activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)				BREAK
						CASE 41		activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)				BREAK
						CASE 42		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B				BREAK
						CASE 43		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A			BREAK
						CASE 44		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 45		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B							BREAK
						CASE 46		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B							BREAK
						CASE 47		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A							BREAK
						CASE 48		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B							BREAK
						CASE 49		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A							BREAK
						CASE 50		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B							BREAK
						CASE 51		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B							BREAK
						CASE 52		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3A							BREAK
						CASE 53		activitySlot = CASINO_AC_SLOT_OUTOFMONEY_M_1B						BREAK
						// Lounge																		
//						CASE 54	activitySlot = CASINO_AC_SLOT_DRINK										BREAK	
//						CASE 55	activitySlot = CASINO_AC_SLOT_SMOKE										BREAK
//						CASE 56	activitySlot = CASINO_AC_SLOT_DRINK										BREAK
//						CASE 57	activitySlot = CASINO_AC_SLOT_SMOKE										BREAK
						CASE 58	activitySlot = CASINO_AC_SLOT_SIT_DRINKING								BREAK
						CASE 59	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B						BREAK
						CASE 60	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B						BREAK
						CASE 61	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B				BREAK
//						CASE 62	activitySlot = CASINO_AC_SLOT_SMOKE										BREAK
						CASE 63	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
//						CASE 64	activitySlot = CASINO_AC_SLOT_DRINK										BREAK
						CASE 65	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 66	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B						BREAK
						// Table Games																	
						// Dealers																		
						CASE 67	activitySlot = CASINO_AC_SLOT_BLACKJACK_DEALER							BREAK
						CASE 68	activitySlot = CASINO_AC_SLOT_POKER_DEALER								BREAK
						CASE 69	activitySlot = CASINO_AC_SLOT_ROULETTE_DEALER							BREAK
//						CASE 70	activitySlot = CASINO_AC_SLOT_SIT_DRINKING								BREAK
//						CASE 71	activitySlot = CASINO_AC_SLOT_SIT_DRINKING								BREAK
						CASE 72	activitySlot = GET_BLACKJACK_PED_ACTIVITY(iPedID)						BREAK
						CASE 73	activitySlot = GET_BLACKJACK_PED_ACTIVITY(iPedID)						BREAK
						CASE 74	activitySlot = GET_BLACKJACK_PED_ACTIVITY(iPedID)						BREAK
						CASE 75	activitySlot = GET_BLACKJACK_PED_ACTIVITY(iPedID)						BREAK
						CASE 76	activitySlot = GET_TABLE_GAME_SPECTATOR_PED_ACTIVITY(iPedID)			BREAK
						CASE 77	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A				BREAK
						CASE 78	activitySlot = GET_ROULETTE_PED_ACTIVITY(iPedID)						BREAK
						CASE 79	activitySlot = GET_ROULETTE_PED_ACTIVITY(iPedID)						BREAK
						CASE 80	activitySlot = GET_ROULETTE_PED_ACTIVITY(iPedID)						BREAK
						CASE 81	activitySlot = GET_ROULETTE_PED_ACTIVITY(iPedID)						BREAK
						CASE 82	activitySlot = CASINO_AC_SLOT_OUTOFMONEY_M_2B							BREAK
						CASE 83	activitySlot = CASINO_AC_SLOT_OUTOFMONEY_M_1A							BREAK
						CASE 84	activitySlot = GET_POKER_PED_ACTIVITY(iPedID)							BREAK
						CASE 85	activitySlot = GET_POKER_PED_ACTIVITY(iPedID)							BREAK
						CASE 86	activitySlot = GET_POKER_PED_ACTIVITY(iPedID)							BREAK
						CASE 87	activitySlot = GET_POKER_PED_ACTIVITY(iPedID)							BREAK
						CASE 88	activitySlot = GET_TABLE_GAME_SPECTATOR_PED_ACTIVITY(iPedID)			BREAK
						CASE 89	activitySlot = GET_TABLE_GAME_SPECTATOR_PED_ACTIVITY(iPedID)			BREAK
						CASE 90	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B								BREAK
						CASE 91	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B						BREAK
						CASE 92	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A								BREAK
						CASE 93	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A						BREAK
						CASE 94	activitySlot = CASINO_AC_SLOT_HANGOUT_F_2A								BREAK
						CASE 95	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A						BREAK
						CASE 96	activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B								BREAK
						CASE 97	activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B								BREAK
						CASE 98	activitySlot = CASINO_AC_SLOT_OUTOFMONEY_M_1B							BREAK
						CASE 99	activitySlot = CASINO_AC_SLOT_TOILET_CHECK_OUT_MIRROR					BREAK
						CASE 100	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B							BREAK
						CASE 101	activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 102	activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A							BREAK
						CASE 103	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B					BREAK
						CASE 104	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 105	activitySlot = CASINO_AC_SLOT_HANGOUT_M_3A							BREAK
						CASE 106	activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B							BREAK
						CASE 107	activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 108	activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B							BREAK
						CASE 109	activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B							BREAK
						CASE 110	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 111	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1B					BREAK
						CASE 112	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A					BREAK
						CASE 113	activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A							BREAK
						CASE 114	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 121	activitySlot = CASINO_AC_SLOT_TOILET_CHECK_OUT_MIRROR				BREAK
						CASE 123	activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						// Bouncer																		
						CASE 124	activitySlot = CASINO_AC_SLOT_BOUNCER								BREAK
					ENDSWITCH																			
				BREAK																					
				CASE 3																					
					SWITCH iPedID																			
						CASE 39	activitySlot =  CASINO_AC_SLOT_BLACKJACK_DEALER							BREAK
						CASE 40	activitySlot =  CASINO_AC_SLOT_ROULETTE_DEALER							BREAK
						CASE 41	activitySlot =  CASINO_AC_SLOT_BLACKJACK_DEALER							BREAK
						CASE 42	activitySlot =  CASINO_AC_SLOT_BLACKJACK_SLOUCHY_01A					BREAK
						CASE 43	activitySlot =  CASINO_AC_SLOT_BLACKJACK_SLOUCHY_WITH_DRINK_01B			BREAK
						CASE 44	activitySlot =  CASINO_AC_SLOT_ROULETTE_DEALER							BREAK
						CASE 45	activitySlot =  CASINO_AC_SLOT_ROULETTE_SEAT_1_REGULAR_01B				BREAK
						CASE 46	activitySlot =  CASINO_AC_SLOT_ROULETTE_SEAT_1_REGULAR_01A				BREAK
						CASE 47	activitySlot =  CASINO_AC_SLOT_ROULETTE_SEAT_1_M_SLOUCHY_F_ENGAGED_01A  BREAK
						CASE 48	activitySlot =  CASINO_AC_SLOT_DRINK									BREAK
						CASE 49	activitySlot =  CASINO_AC_SLOT_BLACKJACK_SLOUCHY_WITH_DRINK_01A			BREAK
						CASE 50	activitySlot =  CASINO_AC_SLOT_HANGOUT_M_3A								BREAK
						CASE 51	activitySlot =  CASINO_AC_SLOT_HANGOUT_F_3A								BREAK
						CASE 52	activitySlot =  CASINO_AC_SLOT_BLACKJACK_DEALER							BREAK
						CASE 53	activitySlot =  CASINO_AC_SLOT_HANGOUT_CONVO_M_3B						BREAK
						CASE 54	activitySlot =  CASINO_AC_SLOT_DRINK									BREAK
						CASE 55	activitySlot =  CASINO_AC_SLOT_DRINK									BREAK
						CASE 56	activitySlot =  CASINO_AC_SLOT_HANGOUT									BREAK
						CASE 57	activitySlot =  CASINO_AC_SLOT_DEALER									BREAK
						CASE 58	activitySlot =  CASINO_AC_SLOT_BLACKJACK_DEALER							BREAK
						CASE 59	activitySlot =  CASINO_AC_SLOT_ROULETTE_DEALER							BREAK
						CASE 60	activitySlot =  CASINO_AC_SLOT_BLACKJACK_SLOUCHY_WITH_DRINK_01A			BREAK
						CASE 61	activitySlot =  CASINO_AC_SLOT_BLACKJACK_REGULAR_02B					BREAK
						CASE 62	activitySlot =  CASINO_AC_SLOT_HANGOUT_CONVO_M_3A						BREAK
						CASE 63	activitySlot =  CASINO_AC_SLOT_BLACKJACK_DEALER							BREAK
						CASE 64	activitySlot =  CASINO_AC_SLOT_DRINK									BREAK
						CASE 65	activitySlot =  CASINO_AC_SLOT_DRINK									BREAK
						CASE 66	activitySlot =  CASINO_AC_SLOT_NULL										BREAK
						CASE 67	activitySlot =  CASINO_AC_SLOT_HANGOUT_CONVO_M_3B						BREAK
						CASE 68	activitySlot =  CASINO_AC_SLOT_DRINK									BREAK
						CASE 69	activitySlot =  CASINO_AC_SLOT_DRINK									BREAK
						CASE 70	activitySlot =  CASINO_AC_SLOT_DRINK									BREAK
						CASE 71	activitySlot =  CASINO_AC_SLOT_DRINK									BREAK
						CASE 72	activitySlot =  CASINO_AC_SLOT_HANGOUT_F_2A								BREAK
						CASE 73	activitySlot =  CASINO_AC_SLOT_MACHINE_REGULAR_01A						BREAK
						CASE 74	activitySlot =  CASINO_AC_SLOT_MACHINE_REGULAR_01B						BREAK
						CASE 75	activitySlot =  CASINO_AC_SLOT_MACHINE_SLOUCHY_01A						BREAK
						CASE 76	activitySlot =  CASINO_AC_SLOT_NULL										BREAK
						CASE 77	activitySlot =  CASINO_AC_SLOT_MACHINE_ENGAGED_01B						BREAK
						CASE 78	activitySlot =  CASINO_AC_SLOT_MACHINE_REGULAR_02B						BREAK
						CASE 79	activitySlot =  CASINO_AC_SLOT_BLACKJACK_ENGAGED_01A					BREAK
						CASE 80	activitySlot =  CASINO_AC_SLOT_BLACKJACK_REGULAR_WITH_DRINK_01A			BREAK
						CASE 81	activitySlot =  CASINO_AC_SLOT_SIT_DRINKING								BREAK
						CASE 82	activitySlot =  CASINO_AC_SLOT_SIT_DRINKING								BREAK
						CASE 83	activitySlot =  CASINO_AC_SLOT_HANGOUT_F_1A								BREAK
						CASE 84	activitySlot =  CASINO_AC_SLOT_HANGOUT_CONVO_M_2B						BREAK
						CASE 85	activitySlot =  CASINO_AC_SLOT_HANGOUT_CONVO_M_1B						BREAK
						CASE 86	activitySlot =  CASINO_AC_SLOT_ROULETTE_SEAT_4_REGULAR_04A				BREAK
						CASE 87	activitySlot =  CASINO_AC_SLOT_ROULETTE_SEAT_3_REGULAR_03B				BREAK
						CASE 88	activitySlot =  CASINO_AC_SLOT_ROULETTE_SEAT_2_M_SLOUCHY_WITH_DRINK_F_REGULAR_WITH_DRINK_02A	BREAK
						//CASE 89	activitySlot =  CASINO_AC_SLOT_DRINK								BREAK
						CASE 90	activitySlot =  CASINO_AC_SLOT_DRINK									BREAK
						CASE 91	activitySlot =  CASINO_AC_SLOT_DRINK									BREAK
						CASE 92	activitySlot =  CASINO_AC_SLOT_HANGOUT_CONVO_M_2B						BREAK
						CASE 93	activitySlot =  CASINO_AC_SLOT_HANGOUT_F_1A								BREAK
						CASE 94	activitySlot =  CASINO_AC_SLOT_HANGOUT_F_1A								BREAK
						CASE 95	activitySlot =  CASINO_AC_SLOT_ROULETTE_DEALER							BREAK
						CASE 96	activitySlot =  CASINO_AC_SLOT_ROULETTE_DEALER							BREAK
						CASE 97	activitySlot =  CASINO_AC_SLOT_ROULETTE_DEALER							BREAK
						CASE 98	activitySlot =  CASINO_AC_SLOT_BLACKJACK_DEALER							BREAK
						CASE 99	activitySlot =  CASINO_AC_SLOT_BLACKJACK_DEALER							BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_SPORTS_BETTING
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Sports Betting
						CASE 39		activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)					BREAK
						CASE 40		activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)					BREAK
						CASE 41		activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)					BREAK
						CASE 42		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B								BREAK
						CASE 43		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A				BREAK
						CASE 44		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B				BREAK
						CASE 45		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING							BREAK
						CASE 46		activitySlot = CASINO_AC_SLOT_DRINK										BREAK
						CASE 47		activitySlot = CASINO_AC_SLOT_HANGOUT									BREAK
						CASE 48		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B					BREAK
						CASE 49		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 50		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3A								BREAK
						CASE 51		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A								BREAK
						CASE 52		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B								BREAK
						CASE 53		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B						BREAK
						// Need to include most main floor area here for culling from the Table Games		
						// Behind Bar (13)																	
						CASE 54		activitySlot = CASINO_AC_SLOT_SIT_DRINKING								BREAK
						CASE 55		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1					BREAK
						CASE 56		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1					BREAK
						CASE 57		activitySlot = CASINO_AC_SLOT_SIT_DRINKING								BREAK
						CASE 58		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A					BREAK
						CASE 59		activitySlot = CASINO_AC_SLOT_SECURITY_GUARD							BREAK
						CASE 60		activitySlot = CASINO_AC_SLOT_DRINK										BREAK
						CASE 61		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B						BREAK
						CASE 62		activitySlot = CASINO_AC_SLOT_OUTOFMONEY_M_1A							BREAK
						CASE 63		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B						BREAK
						CASE 64		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A						BREAK
						CASE 65		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A						BREAK
						CASE 66		activitySlot = CASINO_AC_SLOT_SIT_DRINKING								BREAK
						// All the rest (53)																
						CASE 67		activitySlot = CASINO_AC_SLOT_DRINK										BREAK
						CASE 68		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A								BREAK
						CASE 69		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 70		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B					BREAK
						CASE 71		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 72		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B								BREAK
						CASE 73		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A								BREAK
						CASE 74		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1A								BREAK
						CASE 75		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A						BREAK
						CASE 76		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B						BREAK
						CASE 77		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)					BREAK
						CASE 78		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)					BREAK
						CASE 79		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)					BREAK
						CASE 80		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)					BREAK
//						CASE 81		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A						BREAK
						CASE 82		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B								BREAK
						CASE 83		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A								BREAK
						CASE 84		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A				BREAK
						CASE 85		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A						BREAK
						CASE 86		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A								BREAK
						CASE 87		activitySlot = CASINO_AC_SLOT_DRINK										BREAK
						CASE 88		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B					BREAK
						CASE 89		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A				BREAK
						CASE 90		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A				BREAK
						CASE 91		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B						BREAK
						CASE 92		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A								BREAK
						CASE 93		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B								BREAK
						CASE 94		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A				BREAK
						CASE 95		activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A				BREAK
						CASE 96		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1					BREAK
						CASE 97		activitySlot = CASINO_AC_SLOT_DRINK										BREAK
						CASE 98		activitySlot = CASINO_AC_SLOT_SHOP_MALE_01A								BREAK
						CASE 99		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A						BREAK
						CASE 100	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 101	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A				BREAK
						CASE 102	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A				BREAK
						CASE 103	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A					BREAK
						CASE 104	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B				BREAK
						CASE 105	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B				BREAK
						CASE 106	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A						BREAK
						CASE 107	activitySlot = CASINO_AC_SLOT_HANGOUT									BREAK
						CASE 108	activitySlot = CASINO_AC_SLOT_DRINK										BREAK
						CASE 109	activitySlot = CASINO_AC_SLOT_BAR_MALE_SIT_WITH_DRINK_01A				BREAK
						CASE 110	activitySlot = CASINO_AC_SLOT_LEANING_TEXTING							BREAK
						CASE 111	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B						BREAK
						CASE 112	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 113	activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B								BREAK
						CASE 114	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B						BREAK
						CASE 115	activitySlot = CASINO_AC_SLOT_DRINK										BREAK
						CASE 116	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A						BREAK
						CASE 117	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A						BREAK
						CASE 118	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A						BREAK
						CASE 119	activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B								BREAK
						// Special peds (5)																	
						CASE 120	activitySlot = CASINO_AC_SLOT_CLOAKROOM_STOOL							BREAK
						CASE 121	activitySlot = CASINO_AC_SLOT_AMBIENT_F_1								BREAK
						CASE 122	activitySlot = CASINO_AC_SLOT_MAITRED									BREAK
						CASE 123	activitySlot = CASINO_AC_SLOT_GUARDING									BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Sports Betting
						CASE 39		activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)					BREAK
						CASE 40		activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)					BREAK
						CASE 41		activitySlot = GET_INSIDE_TRACK_PED_ACTIVITY(iPedID)					BREAK
						CASE 42		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B					BREAK
						CASE 43		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A				BREAK
						CASE 44		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING							BREAK
						CASE 45		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B								BREAK
						CASE 46		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B								BREAK
						CASE 47		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A								BREAK
						CASE 48		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B								BREAK
						CASE 49		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A								BREAK
						CASE 50		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B								BREAK
						CASE 51		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B								BREAK
						CASE 52		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3A								BREAK
						CASE 53		activitySlot = CASINO_AC_SLOT_OUTOFMONEY_M_1B							BREAK
						// Need to include most main floor area here for culling from the Table Games		
						// Behind Bar (13)																	
						CASE 54		activitySlot = CASINO_AC_SLOT_SIT_DRINKING								BREAK
						CASE 55		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1					BREAK
						CASE 56		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1					BREAK
						CASE 57		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B								BREAK
						CASE 58		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A					BREAK
						CASE 59		activitySlot = CASINO_AC_SLOT_SECURITY_GUARD							BREAK
						CASE 60		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A					BREAK
						CASE 61		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B						BREAK
						CASE 62		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B					BREAK
						CASE 63		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B						BREAK
						CASE 64		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B						BREAK
						CASE 65		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A						BREAK
						CASE 66		activitySlot = CASINO_AC_SLOT_SIT_DRINKING								BREAK
						CASE 67		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B					BREAK
						CASE 68		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B								BREAK
						CASE 69		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 70		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B								BREAK
						CASE 71		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A					BREAK
						CASE 72		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A					BREAK
						CASE 73		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A						BREAK
						CASE 74		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B								BREAK
						CASE 75		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B					BREAK
						CASE 76		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A				BREAK
						CASE 77		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)					BREAK
						CASE 78		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)					BREAK
						CASE 79		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)					BREAK
						CASE 80		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)					BREAK
//						CASE 81		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A						BREAK
						CASE 82		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A						BREAK
						CASE 83		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A								BREAK
						CASE 84		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2A								BREAK
						CASE 85		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A						BREAK
						CASE 86		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A								BREAK
						CASE 87		activitySlot = CASINO_AC_SLOT_DRINK										BREAK
						CASE 88		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B					BREAK
						CASE 89		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A				BREAK
						CASE 90		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A				BREAK
						CASE 91		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B						BREAK
						CASE 92		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 93		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B						BREAK
						CASE 94		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A				BREAK
						CASE 95		activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A				BREAK
						CASE 96		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1					BREAK
						CASE 97		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A								BREAK
						CASE 98		activitySlot = CASINO_AC_SLOT_SHOP_FEMALE_01A							BREAK
						CASE 99		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A						BREAK
						CASE 100	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 101	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A				BREAK
						CASE 102	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A				BREAK
						CASE 103	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A					BREAK
						CASE 104	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B				BREAK
						CASE 105	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B					BREAK
						CASE 106	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A				BREAK
						CASE 107	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A						BREAK
						CASE 108	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 109	activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A				BREAK
						CASE 110	activitySlot = CASINO_AC_SLOT_LEANING_TEXTING							BREAK
						CASE 111	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B						BREAK
						CASE 112	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 113	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A					BREAK
						CASE 114	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B				BREAK
						CASE 115	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A				BREAK
						CASE 116	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A								BREAK
						CASE 117	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B						BREAK
						CASE 118	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1B						BREAK
						CASE 119	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01B				BREAK
						// Special peds (5)																	
						CASE 120	activitySlot = CASINO_AC_SLOT_CLOAKROOM_STOOL							BREAK
						CASE 121	activitySlot = CASINO_AC_SLOT_AMBIENT_F_1								BREAK
						CASE 122	activitySlot = CASINO_AC_SLOT_MAITRED									BREAK
						CASE 123	activitySlot = CASINO_AC_SLOT_GUARDING									BREAK
					ENDSWITCH																				BREAK
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_MANAGERS_OFFICE
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Entrance (15)
						CASE 39		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B				BREAK
						CASE 40		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING					BREAK
						CASE 41		activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A		BREAK
						CASE 42		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B				BREAK
						// Manager																	
						CASE 43		activitySlot = GET_MANAGERS_ACTIVITY()							BREAK
						CASE 44		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1B				BREAK
						CASE 45		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 46		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A				BREAK
						CASE 47		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B						BREAK
						CASE 48		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING					BREAK
						CASE 49		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1A				BREAK
						CASE 50		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B						BREAK
						CASE 51		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B						BREAK
						CASE 52		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B						BREAK
						CASE 53		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1A				BREAK
						// Behind Bar (13)															
						CASE 54		activitySlot = CASINO_AC_SLOT_SIT_DRINKING						BREAK
						CASE 55		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1			BREAK
						CASE 56		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1			BREAK
						CASE 57		activitySlot = CASINO_AC_SLOT_SIT_DRINKING						BREAK
						CASE 58		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A			BREAK
						CASE 59		activitySlot = CASINO_AC_SLOT_SECURITY_GUARD					BREAK
						CASE 60		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 61		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B				BREAK
						CASE 62		activitySlot = CASINO_AC_SLOT_OUTOFMONEY_M_1A					BREAK
						CASE 63		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B				BREAK
						CASE 64		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A				BREAK
						CASE 65		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A				BREAK
						CASE 66		activitySlot = CASINO_AC_SLOT_SIT_DRINKING						BREAK
						// All the rest (53)														
						CASE 67		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 68		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3A						BREAK
						CASE 69		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 70		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B			BREAK
						CASE 71		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 72		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B						BREAK
						CASE 73		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 74		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1A						BREAK
						CASE 75		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A				BREAK
						CASE 76		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B				BREAK
						CASE 77		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 78		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 79		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
						CASE 80		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)			BREAK
//						CASE 81		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A				BREAK
						CASE 82		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B						BREAK
						CASE 83		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A						BREAK
						CASE 84		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 85		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A				BREAK
						CASE 86		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 87		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 88		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B			BREAK
						CASE 89		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A		BREAK
						CASE 90		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A		BREAK
						CASE 91		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B				BREAK
						CASE 92		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A						BREAK
						CASE 93		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2B						BREAK
						CASE 94		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 95		activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A		BREAK
						CASE 96		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1			BREAK
						CASE 97		activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 98		activitySlot = CASINO_AC_SLOT_SHOP_MALE_01A						BREAK
						CASE 99		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A				BREAK
						CASE 100	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 101	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 102	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A		BREAK
						CASE 103	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A			BREAK
						CASE 104	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B		BREAK
						CASE 105	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B		BREAK
						CASE 106	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A				BREAK
						CASE 107	activitySlot = CASINO_AC_SLOT_HANGOUT							BREAK
						CASE 108	activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 109	activitySlot = CASINO_AC_SLOT_BAR_MALE_SIT_WITH_DRINK_01A		BREAK
						CASE 110	activitySlot = CASINO_AC_SLOT_LEANING_TEXTING					BREAK
						CASE 111	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B				BREAK
						CASE 112	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A			BREAK
						CASE 113	activitySlot = CASINO_AC_SLOT_HANGOUT_M_2B						BREAK
						CASE 114	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B				BREAK
						CASE 115	activitySlot = CASINO_AC_SLOT_DRINK								BREAK
						CASE 116	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A				BREAK
						CASE 117	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A				BREAK
						CASE 118	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A				BREAK
						CASE 119	activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B						BREAK
						// Special peds (5)															
						CASE 120	activitySlot = CASINO_AC_SLOT_CLOAKROOM_STOOL					BREAK
						CASE 121	activitySlot = CASINO_AC_SLOT_AMBIENT_F_1						BREAK
						CASE 122	activitySlot = CASINO_AC_SLOT_MAITRED							BREAK
						CASE 123	activitySlot = CASINO_AC_SLOT_GUARDING							BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						CASE 39		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B					BREAK
						CASE 40		activitySlot = CASINO_AC_SLOT_OUTOFMONEY_M_2B						BREAK
						CASE 41		activitySlot = CASINO_AC_SLOT_BAR_MALE_SIT_WITH_DRINK_01A			BREAK
						CASE 42		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A					BREAK
						// Manager
						CASE 43		activitySlot = GET_MANAGERS_ACTIVITY()								BREAK
						CASE 44		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A					BREAK
						CASE 45		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B					BREAK
						CASE 46		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A					BREAK
						CASE 47		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1B							BREAK
						CASE 48		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1A							BREAK
						CASE 49		activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 50		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B					BREAK
						CASE 51		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A					BREAK
						CASE 52		activitySlot = CASINO_AC_SLOT_SHOP_MALE_01A							BREAK
						CASE 53		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B							BREAK
						CASE 54		activitySlot = CASINO_AC_SLOT_SIT_DRINKING							BREAK
						CASE 55		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1				BREAK
						CASE 56		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1				BREAK
						CASE 57		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B							BREAK
						CASE 58		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A				BREAK
						CASE 59		activitySlot = CASINO_AC_SLOT_SECURITY_GUARD						BREAK
						CASE 60		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A				BREAK
						CASE 61		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2B					BREAK
						CASE 62		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B				BREAK
						CASE 63		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B					BREAK
						CASE 64		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B					BREAK
						CASE 65		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A					BREAK
						CASE 66		activitySlot = CASINO_AC_SLOT_SIT_DRINKING							BREAK
						CASE 67		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B				BREAK
						CASE 68		activitySlot = CASINO_AC_SLOT_HANGOUT_M_1B							BREAK
						CASE 69		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 70		activitySlot = CASINO_AC_SLOT_HANGOUT_M_3B							BREAK
						CASE 71		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A				BREAK
						CASE 72		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A				BREAK
						CASE 73		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_2A					BREAK
						CASE 74		activitySlot = CASINO_AC_SLOT_HANGOUT_F_3B							BREAK
						CASE 75		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B				BREAK
						CASE 76		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 77		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)				BREAK
						CASE 78		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)				BREAK
						CASE 79		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)				BREAK
						CASE 80		activitySlot = GET_SLOT_MACHINE_PED_ACTIVITY(iPedID)				BREAK
//						CASE 81		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A					BREAK
						CASE 82		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A					BREAK
						CASE 83		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A							BREAK
						CASE 84		activitySlot = CASINO_AC_SLOT_HANGOUT_F_2A							BREAK
						CASE 85		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3A					BREAK
						CASE 86		activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A							BREAK
						CASE 87		activitySlot = CASINO_AC_SLOT_DRINK									BREAK
						CASE 88		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B				BREAK
						CASE 89		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A			BREAK
						CASE 90		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01A			BREAK
						CASE 91		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_1B					BREAK
						CASE 92		activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 93		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_M_3B					BREAK
						CASE 94		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 95		activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A			BREAK
						CASE 96		activitySlot = CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_F_1				BREAK
						CASE 97		activitySlot = CASINO_AC_SLOT_HANGOUT_M_2A							BREAK
						CASE 98		activitySlot = CASINO_AC_SLOT_SHOP_FEMALE_01A						BREAK
						CASE 99		activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2A					BREAK
						CASE 100	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 101	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 102	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 103	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01A				BREAK
						CASE 104	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B			BREAK
						CASE 105	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_F_01B				BREAK
						CASE 106	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 107	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1A					BREAK
						CASE 108	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 109	activitySlot = CASINO_AC_SLOT_BAR_MALE_STAND_WITH_DRINK_01A			BREAK
						CASE 110	activitySlot = CASINO_AC_SLOT_LEANING_TEXTING						BREAK
						CASE 111	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_3B					BREAK
						CASE 112	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 113	activitySlot = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A				BREAK
						CASE 114	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_F_01B			BREAK
						CASE 115	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01A			BREAK
						CASE 116	activitySlot = CASINO_AC_SLOT_HANGOUT_F_1A							BREAK
						CASE 117	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_2B					BREAK
						CASE 118	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_F_1B					BREAK
						CASE 119	activitySlot = CASINO_AC_SLOT_HANGOUT_CONVO_WITHDRINK_M_01B			BREAK
						// Special peds (5)
						CASE 120	activitySlot = CASINO_AC_SLOT_CLOAKROOM_STOOL						BREAK
						CASE 121	activitySlot = CASINO_AC_SLOT_AMBIENT_F_1							BREAK
						CASE 122	activitySlot = CASINO_AC_SLOT_MAITRED								BREAK
						CASE 123	activitySlot = CASINO_AC_SLOT_GUARDING								BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugPrints
		PRINTLN("[CASINO_PEDS] GET_CASINO_PED_ACTIVITY_SLOT - Ped Activity: ", GET_CASINO_PED_ACTIVITY_STRING(activitySlot))
	ENDIF
	#ENDIF
	
	RETURN activitySlot
ENDFUNC

FUNC VECTOR GET_BLACKJACK_TABLE_COORDS(INT iTable)
	SWITCH iTable
		CASE 0	RETURN <<1148.8368, 269.7470, -52.8409>>
		CASE 1	RETURN <<1151.8400, 266.7470, -52.8409>>
		CASE 2	RETURN <<1129.4065, 262.3578, -52.0410>>
		CASE 3	RETURN <<1144.4291, 247.3352, -52.0410>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_THREE_CARD_POKER_TABLE_COORDS(INT iTable)
	SWITCH iTable
		CASE 0	RETURN <<1143.3379, 264.2453, -52.8409>>
		CASE 1	RETURN <<1146.3290, 261.2543, -52.8409>>
		CASE 2	RETURN <<1133.7400, 266.6947, -52.0409>>
		CASE 3	RETURN <<1148.7400, 251.6947, -52.0409>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_ROULETTE_TABLE_COORDS(INT iTable)
	SWITCH iTable
		CASE 0	RETURN <<1144.8137, 268.2634, -52.8409>>
		CASE 1	RETURN <<1150.3547, 262.7224, -52.8409>>
		CASE 2	RETURN <<1133.9583, 262.1071, -52.0409>>
		CASE 3	RETURN <<1129.5952, 267.2637, -52.0409>>
		CASE 4	RETURN <<1144.6178, 252.2411, -52.0409>>
		CASE 5	RETURN <<1148.9808, 247.0846, -52.0409>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_INSIDE_TRACK_SEAT_COORDS(INT iSeat)
	SWITCH iSeat
		CASE 0	RETURN <<1091.4180, 257.5440, -52.2409>>
		CASE 1	RETURN <<1092.0721, 258.1984, -52.2409>>
		CASE 2	RETURN <<1094.4731, 260.5995, -52.2409>>
		CASE 3	RETURN <<1095.1270, 261.2540, -52.2409>>
		CASE 4	RETURN <<1095.8520, 261.9780, -52.2409>>
		CASE 5	RETURN <<1096.5057, 262.6327, -52.2409>>
		CASE 6	RETURN <<1098.9023, 265.0287, -52.2409>>
		CASE 7	RETURN <<1099.5560, 265.6830, -52.2409>>
		CASE 8	RETURN <<1092.9740, 255.2110, -52.2409>>
		CASE 9	RETURN <<1093.6277, 255.8650, -52.2409>>
		CASE 10	RETURN <<1096.4174, 258.6552, -52.2409>>
		CASE 11	RETURN <<1097.0720, 259.3090, -52.2409>>
		CASE 12	RETURN <<1097.7970, 260.0340, -52.2409>>
		CASE 13	RETURN <<1098.4507, 260.6878, -52.2409>>
		CASE 14	RETURN <<1101.2358, 263.4730, -52.2409>>
		CASE 15	RETURN <<1101.8900, 264.1270, -52.2409>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_SLOT_MACHINE_COORDS(INT iSlotMachine)
	SWITCH iSlotMachine
		//main left
		CASE 0 RETURN <<1100.4828,230.4082,-50.8409>>
		CASE 1 RETURN <<1100.9390, 231.0017, -50.8409>>
		CASE 2 RETURN <<1101.2209,231.6943, -50.8409>>
		CASE 3 RETURN <<1101.3228,232.4321, -50.8409>>
		CASE 4 RETURN <<1101.2295,233.1719, -50.8409>>
		
		//main top
		CASE 5 RETURN <<1108.9385,239.4797,-50.8409>>
		CASE 6 RETURN <<1109.5358,239.0278, -50.8409>>
		CASE 7 RETURN <<1110.2292,238.7428, -50.8409>>
		CASE 8 RETURN <<1110.9741,238.6420, -50.8409>>
		CASE 9 RETURN <<1111.7162,238.7384, -50.8409>>
		CASE 10 RETURN <<1112.4067,239.0216, -50.8409>>
		CASE 11 RETURN <<1112.9991,239.4742, -50.8409>>
		
		//main right
		CASE 12 RETURN <<1120.8530,233.1621, -50.8409>>
		CASE 13 RETURN <<1120.7528,232.4272, -50.8409>>
		CASE 14 RETURN <<1120.8533,231.6886, -50.8409>>
		CASE 15 RETURN <<1121.1350,230.9999, -50.8409>>
		CASE 16 RETURN <<1121.5918,230.4106, -50.8409>>
		
		//left front pole
		CASE 17 RETURN <<1104.5742,229.4451, -50.8409>>
		CASE 18 RETURN <<1104.3022,230.3183, -50.8409>>
		CASE 19 RETURN <<1105.0492,230.8450, -50.8409>>
		CASE 20 RETURN <<1105.7809,230.2973, -50.8409>>
		CASE 21 RETURN <<1105.4862,229.4322, -50.8409>>
		
		//left back pole
		CASE 22 RETURN <<1108.0054,233.9177, -50.8409>>
		CASE 23 RETURN <<1107.7354,234.7909, -50.8409>>
		CASE 24 RETURN <<1108.4823,235.3176, -50.8409>>
		CASE 25 RETURN <<1109.2140,234.7699, -50.8409>>
		CASE 26 RETURN <<1108.9193,233.9084, -50.8409>>
		
		//right back pole
		CASE 27 RETURN <<1113.6398,233.6755, -50.8409>>
		CASE 28 RETURN <<1113.3696,234.5486, -50.8409>>
		CASE 29 RETURN <<1114.1166,235.0753, -50.8409>>
		CASE 30 RETURN <<1114.8484,234.5277, -50.8409>>
		CASE 31 RETURN <<1114.5536,233.6625, -50.8409>>
		
		//right front pole
		CASE 32 RETURN <<1116.6624,228.8896, -50.8409>>
		CASE 33 RETURN <<1116.3922,229.7682, -50.8409>>
		CASE 34 RETURN <<1117.1392,230.2895, -50.8409>>
		CASE 35 RETURN <<1117.8710,229.7419, -50.8409>>
		CASE 36 RETURN <<1117.5762,228.8767, -50.8409>>
		
		//back room bottom wall
		CASE 37 RETURN <<1129.6400,250.4510, -52.0409>>
		CASE 38 RETURN <<1130.3765,250.3577, -52.0409>>
		CASE 39 RETURN <<1131.0624,250.0776, -52.0409>>
		CASE 40 RETURN <<1131.6548,249.6264, -52.0409>>
		CASE 41 RETURN <<1132.1093,249.0355, -52.0409>>
		CASE 42 RETURN <<1132.3961,248.3382, -52.0409>>
		CASE 43 RETURN <<1132.4922,247.5984, -52.0409>>
		
		//back room left pole
		CASE 44 RETURN <<1133.9524,256.1037, -52.0409>>
		CASE 45 RETURN <<1133.8274,256.9098, -52.0409>>
		CASE 46 RETURN <<1134.5560,257.2778, -52.0409>>
		CASE 47 RETURN <<1135.1316,256.6990, -52.0409>>
		CASE 48 RETURN <<1134.7590,255.9734, -52.0409>>
		
		//back room right pole
		CASE 49 RETURN <<1138.1951,251.8611, -52.0409>>
		CASE 50	RETURN <<1138.0703,252.6677, -52.0409>>
		CASE 51	RETURN <<1138.7986,253.0363, -52.0409>>
		CASE 52	RETURN <<1139.3722,252.4563, -52.0409>>
		CASE 53	RETURN <<1138.9995,251.7306, -52.0409>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL IS_CASINO_PED_TYPE_FEMALE(CASINO_PED_TYPES ePedType)
	SWITCH ePedType
		CASE CASINO_SMART_FEMALE
		CASE CASINO_GENERIC_FEMALE
		CASE CASINO_DEALER_FEMALE_1
		CASE CASINO_DEALER_FEMALE_2
		CASE CASINO_DEALER_FEMALE_3
		CASE CASINO_DEALER_FEMALE_4
		CASE CASINO_DEALER_FEMALE_5
		CASE CASINO_DEALER_FEMALE_6
		CASE CASINO_DEALER_FEMALE_7
		CASE CASINO_CAROL
		CASE CASINO_BETH
		CASE CASINO_LAUREN
		CASE CASINO_TAYLOR
		CASE CASINO_EILEEN
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CASINO_PED_FEMALE(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea)
	RETURN IS_CASINO_PED_TYPE_FEMALE(GET_CASINO_PED_TYPE(iPedID, iLayout, eCasinoArea))
ENDFUNC

/// PURPOSE: This is for smart, generic and special females peds (no dealers)
FUNC BOOL IS_CASINO_AMBIENT_PED_FEMALE(CASINO_PED_TYPES ePedType)
	SWITCH ePedType
		CASE CASINO_SMART_FEMALE
		CASE CASINO_GENERIC_FEMALE
		CASE CASINO_CAROL
		CASE CASINO_BETH
		CASE CASINO_LAUREN
		CASE CASINO_TAYLOR
		CASE CASINO_EILEEN
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING GET_SLOT_MACHINE_SEAT_BONE_INDEX_NAME(MODEL_NAMES eSlotMachineModel)
	STRING sBone = "Chair_Seat_01"
	IF (eSlotMachineModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("vw_prop_casino_slot_08a")))
		sBone = "Chair_Seat_002"
	ENDIF
	RETURN sBone
ENDFUNC

FUNC STRING GET_SEAT_BONE_NAME(INT iSeat)
	SWITCH iSeat
		CASE 0	RETURN "Chair_Base_01"
		CASE 1	RETURN "Chair_Base_02"
		CASE 2	RETURN "Chair_Base_03"
		CASE 3	RETURN "Chair_Base_04"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT GET_BLACKJACK_TABLE_HASH(INT iTable)
	SWITCH iTable
		CASE 0	RETURN GET_HASH_KEY("vw_prop_casino_blckjack_01")
		CASE 1	RETURN GET_HASH_KEY("vw_prop_casino_blckjack_01")
		CASE 2	RETURN GET_HASH_KEY("vw_prop_casino_blckjack_01b")
		CASE 3	RETURN GET_HASH_KEY("vw_prop_casino_blckjack_01b")
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT GET_THREE_CARD_POKER_TABLE_HASH(INT iTable)
	SWITCH iTable
		CASE 0	RETURN GET_HASH_KEY("vw_prop_casino_3cardpoker_01")
		CASE 1	RETURN GET_HASH_KEY("vw_prop_casino_3cardpoker_01")
		CASE 2	RETURN GET_HASH_KEY("vw_prop_casino_3cardpoker_01b")
		CASE 3	RETURN GET_HASH_KEY("vw_prop_casino_3cardpoker_01b")
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT GET_ROULETTE_TABLE_HASH(INT iTable)
	SWITCH iTable
		CASE 0	RETURN GET_HASH_KEY("vw_prop_casino_roulette_01")
		CASE 1	RETURN GET_HASH_KEY("vw_prop_casino_roulette_01")
		CASE 2	RETURN GET_HASH_KEY("vw_prop_casino_roulette_01b")
		CASE 3	RETURN GET_HASH_KEY("vw_prop_casino_roulette_01b")
		CASE 4	RETURN GET_HASH_KEY("vw_prop_casino_roulette_01b")
		CASE 5	RETURN GET_HASH_KEY("vw_prop_casino_roulette_01b")
	ENDSWITCH
	RETURN -1
ENDFUNC


FUNC BOOL IS_CASINO_PED_PLAYING_BLACKJACK(INT iPedID, CASINO_AREA ePlayerCurrentArea)
	SWITCH iPedID
		CASE 67 // Dealer
		CASE 72
		CASE 73
		CASE 74
		CASE 75
			RETURN (ePlayerCurrentArea = CASINO_AREA_TABLE_GAMES)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CASINO_PED_PLAYING_ROULETTE(INT iPedID, CASINO_AREA ePlayerCurrentArea)
	SWITCH iPedID
		CASE 69 // Dealer
		CASE 78
		CASE 79
		CASE 80
		CASE 81
			RETURN (ePlayerCurrentArea = CASINO_AREA_TABLE_GAMES)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_BLACKJACK_CHAIR_COORDS(INT iTable, INT iChair, CASINO_PED_TYPES ePedType)
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	SWITCH iTable
		CASE 0
			SWITCH iChair
				CASE 0	vCoords = << 1149.65, 270.502, -52.8409 >>	BREAK
				CASE 1	vCoords = << 1148.71, 270.505, -52.8409 >>	BREAK
				CASE 2	vCoords = << 1148.08, 269.867, -52.8409 >>	BREAK
				CASE 3	vCoords = << 1148.08, 268.935, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iChair
				CASE 0	vCoords = << 1151.03, 265.992, -52.8409 >>	BREAK
				CASE 1	vCoords = << 1151.97, 265.989, -52.8409 >>	BREAK
				CASE 2	vCoords = << 1152.6, 266.627, -52.8409 >>	BREAK
				CASE 3	vCoords = << 1152.6, 267.559, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iChair
				CASE 0	vCoords = << 1130.16, 261.544, -52.041 >>	BREAK
				CASE 1	vCoords = << 1130.16, 262.486, -52.041 >>	BREAK
				CASE 2	vCoords = << 1129.53, 263.119, -52.041 >>	BREAK
				CASE 3	vCoords = << 1128.59, 263.114, -52.041 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iChair
				CASE 0	vCoords = << 1145.18, 246.521, -52.041 >>	BREAK
				CASE 1	vCoords = << 1145.19, 247.464, -52.041 >>	BREAK
				CASE 2	vCoords = << 1144.55, 248.097, -52.041 >>	BREAK
				CASE 3	vCoords = << 1143.62, 248.092, -52.041 >>	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT ARE_VECTORS_EQUAL(vCoords, <<0.0, 0.0, 0.0>>)
		// Chair offset by 1m
		vCoords += <<0.0, 0.0, 1.0>>
	
		// Female ped offset by 1.91 cm
		IF IS_CASINO_AMBIENT_PED_FEMALE(ePedType)
			vCoords += <<0.0, 0.0, 0.0191>>
		ENDIF
	ENDIF
	
	RETURN vCoords
ENDFUNC

FUNC VECTOR GET_BLACKJACK_PED_COORDS(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea, CASINO_PED_TYPES ePedType)
	UNUSED_PARAMETER(iLayout)
	UNUSED_PARAMETER(eCasinoArea)
	
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	SWITCH g_iPedReservedBlackjackTable
		CASE 0
			SWITCH iPedID
				CASE 72	vCoords = << 1149.65, 270.502, -52.8409 >>	BREAK
				CASE 73	vCoords = << 1148.71, 270.505, -52.8409 >>	BREAK
				CASE 74	vCoords = << 1148.08, 269.867, -52.8409 >>	BREAK
				CASE 75	vCoords = << 1148.08, 268.935, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iPedID
				CASE 72	vCoords = << 1151.03, 265.992, -52.8409 >>	BREAK
				CASE 73	vCoords = << 1151.97, 265.989, -52.8409 >>	BREAK
				CASE 74	vCoords = << 1152.6, 266.627, -52.8409 >>	BREAK
				CASE 75	vCoords = << 1152.6, 267.559, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iPedID
				CASE 72	vCoords = << 1130.16, 261.544, -52.041 >>	BREAK
				CASE 73	vCoords = << 1130.16, 262.486, -52.041 >>	BREAK
				CASE 74	vCoords = << 1129.53, 263.119, -52.041 >>	BREAK
				CASE 75	vCoords = << 1128.59, 263.114, -52.041 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iPedID
				CASE 72	vCoords = << 1145.18, 246.521, -52.041 >>	BREAK
				CASE 73	vCoords = << 1145.19, 247.464, -52.041 >>	BREAK
				CASE 74	vCoords = << 1144.55, 248.097, -52.041 >>	BREAK
				CASE 75	vCoords = << 1143.62, 248.092, -52.041 >>	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT ARE_VECTORS_EQUAL(vCoords, <<0.0, 0.0, 0.0>>)
		// Chair offset by 1m
		vCoords += <<0.0, 0.0, 1.0>>
		
		// Female ped offset by 1.91 cm
		IF IS_CASINO_AMBIENT_PED_FEMALE(ePedType)
			vCoords += <<0.0, 0.0, 0.0191>>
		ENDIF
	ENDIF
	
	RETURN vCoords
ENDFUNC

FUNC VECTOR GET_THREE_CARD_POKER_CHAIR_COORDS(INT iTable, INT iChair, CASINO_PED_TYPES ePedType, BOOL bIncludeFemaleOffset = TRUE)
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	SWITCH iTable
		CASE 0
			SWITCH iChair
				CASE 0	vCoords = << 1144.16, 264.996, -52.8409 >>	BREAK
				CASE 1	vCoords = << 1143.21, 265.004, -52.8409 >>	BREAK
				CASE 2	vCoords = << 1142.58, 264.369, -52.8409 >>	BREAK
				CASE 3	vCoords = << 1142.58, 263.438, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iChair
				CASE 0	vCoords = << 1145.51, 260.504, -52.8409 >>	BREAK
				CASE 1	vCoords = << 1146.45, 260.496, -52.8409 >>	BREAK
				CASE 2	vCoords = << 1147.09, 261.13, -52.8409 >>	BREAK
				CASE 3	vCoords = << 1147.09, 262.062, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iChair
				CASE 0	vCoords = << 1132.99, 267.513, -52.0409 >>	BREAK
				CASE 1	vCoords = << 1132.98, 266.57, -52.0409 >>	BREAK
				CASE 2	vCoords = << 1133.62, 265.934, -52.0409 >>	BREAK
				CASE 3	vCoords = << 1134.55, 265.934, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iChair
				CASE 0	vCoords = << 1147.99, 252.513, -52.0409 >>	BREAK
				CASE 1	vCoords = << 1147.98, 251.57, -52.0409 >>	BREAK
				CASE 2	vCoords = << 1148.62, 250.934, -52.0409 >>	BREAK
				CASE 3	vCoords = << 1149.55, 250.934, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT ARE_VECTORS_EQUAL(vCoords, <<0.0, 0.0, 0.0>>)
		// Chair offset by 1m
		vCoords += <<0.0, 0.0, 1.0>>
		
		// Female ped offset by 1.91 cm
		IF bIncludeFemaleOffset
			IF IS_CASINO_AMBIENT_PED_FEMALE(ePedType)
				vCoords += <<0.0, 0.0, 0.0191>>
			ENDIF
		ENDIF
	ENDIF
	
	RETURN vCoords
ENDFUNC

FUNC VECTOR GET_THREE_CARD_POKER_PED_COORDS(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea, CASINO_PED_TYPES ePedType)
	UNUSED_PARAMETER(iLayout)
	UNUSED_PARAMETER(eCasinoArea)
	
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	SWITCH g_iPedReservedThreeCardPokerTable
		CASE 0
			SWITCH iPedID
				CASE 84	vCoords = << 1144.16, 264.996, -52.8409 >>	BREAK
				CASE 85	vCoords = << 1143.21, 265.004, -52.8409 >>	BREAK
				CASE 86	vCoords = << 1142.58, 264.369, -52.8409 >>	BREAK
				CASE 87	vCoords = << 1142.58, 263.438, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iPedID
				CASE 84	vCoords = << 1145.51, 260.504, -52.8409 >>	BREAK
				CASE 85	vCoords = << 1146.45, 260.496, -52.8409 >>	BREAK
				CASE 86	vCoords = << 1147.09, 261.13, -52.8409 >>	BREAK
				CASE 87	vCoords = << 1147.09, 262.062, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iPedID
				CASE 84	vCoords = << 1132.99, 267.513, -52.0409 >>	BREAK
				CASE 85	vCoords = << 1132.98, 266.57, -52.0409 >>	BREAK
				CASE 86	vCoords = << 1133.62, 265.934, -52.0409 >>	BREAK
				CASE 87	vCoords = << 1134.55, 265.934, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iPedID
				CASE 84	vCoords = << 1147.99, 252.513, -52.0409 >>	BREAK
				CASE 85	vCoords = << 1147.98, 251.57, -52.0409 >>	BREAK
				CASE 86	vCoords = << 1148.62, 250.934, -52.0409 >>	BREAK
				CASE 87	vCoords = << 1149.55, 250.934, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT ARE_VECTORS_EQUAL(vCoords, <<0.0, 0.0, 0.0>>)
		// Chair offset by 1m
		vCoords += <<0.0, 0.0, 1.0>>
		
		// Female ped offset by 1.91 cm
		IF IS_CASINO_AMBIENT_PED_FEMALE(ePedType)
			vCoords += <<0.0, 0.0, 0.0191>>
		ENDIF
	ENDIF
	
	RETURN vCoords
ENDFUNC

FUNC VECTOR GET_ROULETTE_CHAIR_COORDS(INT iTable, INT iSeat, CASINO_PED_TYPES ePedType)
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	SWITCH iTable
		CASE 0
			SWITCH iSeat
				CASE 0	vCoords = << 1144.34, 269.022, -52.8409 >>	BREAK
				CASE 1	vCoords = << 1143.66, 268.335, -52.8409 >>	BREAK
				CASE 2	vCoords = << 1143.75, 267.377, -52.8409 >>	BREAK
				CASE 3	vCoords = << 1144.72, 267.277, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iSeat
				CASE 0	vCoords = << 1150.83, 261.963, -52.8409 >>	BREAK
				CASE 1	vCoords = << 1151.51, 262.651, -52.8409 >>	BREAK
				CASE 2	vCoords = << 1151.42, 263.608, -52.8409 >>	BREAK
				CASE 3	vCoords = << 1150.45, 263.708, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iSeat
				CASE 0	vCoords = << 1134.03, 263.266, -52.0409 >>	BREAK
				CASE 1	vCoords = << 1133.07, 263.172, -52.0409 >>	BREAK
				CASE 2	vCoords = << 1132.97, 262.204, -52.0409 >>	BREAK
				CASE 3	vCoords = << 1134.72, 262.578, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iSeat
				CASE 0	vCoords = << 1129.52, 266.105, -52.0409 >>	BREAK
				CASE 1	vCoords = << 1130.48, 266.199, -52.0409 >>	BREAK
				CASE 2	vCoords = << 1130.58, 267.167, -52.0409 >>	BREAK
				CASE 3	vCoords = << 1128.84, 266.792, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH iSeat
				CASE 0	vCoords = << 1144.55, 251.083, -52.0409 >>	BREAK
				CASE 1	vCoords = << 1145.5, 251.176, -52.0409 >>	BREAK
				CASE 2	vCoords = << 1145.6, 252.144, -52.0409 >>	BREAK
				CASE 3	vCoords = << 1143.86, 251.77, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH iSeat
				CASE 0	vCoords = << 1149.05, 248.243, -52.0409 >>	BREAK
				CASE 1	vCoords = << 1148.09, 248.15, -52.0409 >>	BREAK
				CASE 2	vCoords = << 1147.99, 247.182, -52.0409 >>	BREAK
				CASE 3	vCoords = << 1149.74, 247.556, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT ARE_VECTORS_EQUAL(vCoords, <<0.0, 0.0, 0.0>>)
		// Chair offset by 1m
		vCoords += <<0.0, 0.0, 1.0>>
		
		// Female ped offset by 1.91 cm
		IF IS_CASINO_AMBIENT_PED_FEMALE(ePedType)
			vCoords += <<0.0, 0.0, 0.0191>>
		ENDIF
	ENDIF
	
	RETURN vCoords
ENDFUNC

FUNC VECTOR GET_ROULETTE_PED_COORDS(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea, CASINO_PED_TYPES ePedType)
	UNUSED_PARAMETER(iLayout)
	UNUSED_PARAMETER(eCasinoArea)
	
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	SWITCH g_iPedReservedRouletteTable
		CASE 0
			SWITCH iPedID
				CASE 81	vCoords = << 1144.34, 269.022, -52.8409 >>	BREAK
				CASE 80	vCoords = << 1143.66, 268.335, -52.8409 >>	BREAK
				CASE 79	vCoords = << 1143.75, 267.377, -52.8409 >>	BREAK
				CASE 78	vCoords = << 1144.72, 267.277, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iPedID
				CASE 81	vCoords = << 1150.83, 261.963, -52.8409 >>	BREAK
				CASE 80	vCoords = << 1151.51, 262.651, -52.8409 >>	BREAK
				CASE 79	vCoords = << 1151.42, 263.608, -52.8409 >>	BREAK
				CASE 78	vCoords = << 1150.45, 263.708, -52.8409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iPedID
				CASE 81	vCoords = << 1134.72, 262.578, -52.0409 >>	BREAK
				CASE 80	vCoords = << 1134.03, 263.266, -52.0409 >>	BREAK
				CASE 79	vCoords = << 1133.07, 263.172, -52.0409 >>	BREAK
				CASE 78	vCoords = << 1132.97, 262.204, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iPedID
				CASE 81	vCoords = << 1128.84, 266.792, -52.0409 >>	BREAK
				CASE 80	vCoords = << 1129.52, 266.105, -52.0409 >>	BREAK
				CASE 79	vCoords = << 1130.48, 266.199, -52.0409 >>	BREAK
				CASE 78	vCoords = << 1130.58, 267.167, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH iPedID
				CASE 81	vCoords = << 1143.86, 251.77, -52.0409 >>	BREAK
				CASE 80	vCoords = << 1144.55, 251.083, -52.0409 >>	BREAK
				CASE 79	vCoords = << 1145.5, 251.176, -52.0409 >>	BREAK
				CASE 78	vCoords = << 1145.6, 252.144, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH iPedID
				CASE 81	vCoords = << 1149.74, 247.556, -52.0409 >>	BREAK
				CASE 80	vCoords = << 1149.05, 248.243, -52.0409 >>	BREAK
				CASE 79	vCoords = << 1148.09, 248.15, -52.0409 >>	BREAK
				CASE 78	vCoords = << 1147.99, 247.182, -52.0409 >>	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT ARE_VECTORS_EQUAL(vCoords, <<0.0, 0.0, 0.0>>)
		// Chair offset by 1m
		vCoords += <<0.0, 0.0, 1.0>>
		
		// Female ped offset by 1.91 cm
		IF IS_CASINO_AMBIENT_PED_FEMALE(ePedType)
			vCoords += <<0.0, 0.0, 0.0191>>
		ENDIF
	ENDIF
	
	RETURN vCoords
ENDFUNC

FUNC BOOL IS_CASINO_PED_PLAYING_INSIDE_TRACK(INT iPedID, CASINO_AREA ePlayerCurrentArea)
	SWITCH iPedID
		CASE 39
		CASE 41
		CASE 40
			RETURN (ePlayerCurrentArea = CASINO_AREA_SPORTS_BETTING)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC INT GET_INSIDE_TRACK_SEAT_ID(INT iPedID)
	INT iPriority
	SWITCH iPedID
		CASE 39	iPriority = 1	BREAK
		CASE 41	iPriority = 2	BREAK
		CASE 40	iPriority = 3	BREAK
	ENDSWITCH
	
	INT iSeat
	INT iCounter = 1
	REPEAT MAX_CASINO_INSIDE_TRACK_SEATS iSeat
		IF IS_BIT_SET(g_iPedReservedInsideTrackSeatBS, iSeat)
			IF iCounter = iPriority
				RETURN iSeat
			ELSE
				iCounter++
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

FUNC VECTOR GET_INSIDE_TRACK_CHAIR_COORDS(INT iPedID, CASINO_PED_TYPES ePedType)
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	INT iSeat = GET_INSIDE_TRACK_SEAT_ID(iPedID)
	SWITCH iSeat
		CASE 0 vCoords = << 1091.42, 257.544, -52.2409 >>	BREAK
		CASE 1 vCoords = << 1092.07, 258.198, -52.2409 >>	BREAK
		CASE 2 vCoords = << 1094.47, 260.599, -52.2409 >>	BREAK
		CASE 3 vCoords = << 1095.13, 261.254, -52.2409 >>	BREAK
		CASE 4 vCoords = << 1095.85, 261.978, -52.2409 >>	BREAK
		CASE 5 vCoords = << 1096.51, 262.633, -52.2409 >>	BREAK
		CASE 6 vCoords = << 1098.9, 265.029, -52.2409 >>	BREAK
		CASE 7 vCoords = << 1099.56, 265.683, -52.2409 >>	BREAK
		CASE 8 vCoords = << 1092.97, 255.211, -52.2409 >>	BREAK
		CASE 9 vCoords = << 1093.63, 255.865, -52.2409 >>	BREAK
		CASE 10 vCoords = << 1096.42, 258.655, -52.2411 >>	BREAK
		CASE 11 vCoords = << 1097.07, 259.309, -52.2409 >>	BREAK
		CASE 12 vCoords = << 1097.8, 260.034, -52.2409 >>	BREAK
		CASE 13 vCoords = << 1098.45, 260.688, -52.2409 >>	BREAK
		CASE 14 vCoords = << 1101.24, 263.473, -52.2409 >>	BREAK
		CASE 15 vCoords = << 1101.89, 264.127, -52.2409 >>	BREAK
	ENDSWITCH
	
	IF NOT ARE_VECTORS_EQUAL(vCoords, <<0.0, 0.0, 0.0>>)
		// Chair offset by 1m
		vCoords += <<0.0, 0.0, 1.0>>
		
		// Female ped offset by 1.91 cm
		IF IS_CASINO_AMBIENT_PED_FEMALE(ePedType)
			vCoords += <<0.0, 0.0, 0.0191>>
		ENDIF
	ENDIF
	RETURN vCoords
ENDFUNC

FUNC VECTOR GET_INSIDE_TRACK_RACE_START_POSITION()
	RETURN << 1089.34, 259.86, -49.51 >>
ENDFUNC

FUNC VECTOR GET_INSIDE_TRACK_RACE_END_POSITION()
	RETURN << 1097.08, 267.71, -49.49 >>
ENDFUNC

FUNC VECTOR GET_SLOT_MACHINE_CHAIR_COORDS(INT iSlotMachine, CASINO_PED_TYPES ePedType)
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	SWITCH iSlotMachine
		//main left
		CASE 0 vCoords = << 1101.01, 229.876, -50.8409 >> BREAK
		CASE 1 vCoords = << 1101.59, 230.626, -50.8409 >> BREAK
		CASE 2 vCoords = << 1101.95, 231.501, -50.8409 >> BREAK
		CASE 3 vCoords = << 1102.08, 232.433, -50.8409 >> BREAK
		CASE 4 vCoords = << 1101.96, 233.367, -50.8409 >> BREAK
		
		//main top
		CASE 5 vCoords = << 1108.41, 238.946, -50.8409 >> 	BREAK
		CASE 6 vCoords = << 1109.16, 238.376, -50.8409 >> 	BREAK
		CASE 7 vCoords = << 1110.03, 238.016, -50.8409 >> 	BREAK
		CASE 8 vCoords = << 1110.97, 237.89, -50.8409 >>	BREAK
		CASE 9 vCoords = << 1111.91, 238.012, -50.8409 >> 	BREAK
		CASE 10 vCoords = << 1112.78, 238.371, -50.8409 >> 	BREAK
		CASE 11 vCoords = << 1113.53, 238.943, -50.8409 >> 	BREAK
		
		//main right
		CASE 12 vCoords = << 1120.13, 233.355, -50.8409 >> 	BREAK
		CASE 13 vCoords = << 1120, 232.427, -50.8409 >>		BREAK
		CASE 14 vCoords = << 1120.13, 231.494, -50.8409 >> 	BREAK
		CASE 15 vCoords = << 1120.48, 230.624, -50.8409 >> 	BREAK
		CASE 16 vCoords = << 1121.06, 229.878, -50.8409 >> 	BREAK
		
		//left front pole
		CASE 17 vCoords = << 1104.13, 228.836, -50.8409 >>	BREAK
		CASE 18 vCoords = << 1103.59, 230.55, -50.8409 >>	BREAK
		CASE 19 vCoords = << 1105.05, 231.597, -50.8409 >>	BREAK
		CASE 20 vCoords = << 1106.5, 230.53, -50.8409 >>	BREAK
		CASE 21 vCoords = << 1105.93, 228.823, -50.8409 >>	BREAK
		
		//left back pole
		CASE 22 vCoords = << 1107.56, 233.308, -50.8409 >> 	BREAK
		CASE 23 vCoords = << 1107.02, 235.023, -50.8409 >> 	BREAK
		CASE 24 vCoords = << 1108.48, 236.07, -50.8409 >> 	BREAK
		CASE 25 vCoords = << 1109.93, 235.003, -50.8409 >>	BREAK
		CASE 26 vCoords = << 1109.36, 233.297, -50.8409 >> 	BREAK
		
		//right back pole
		CASE 27 vCoords = << 1113.2, 233.067, -50.8409 >>	BREAK
		CASE 28 vCoords = << 1112.65, 234.78, -50.8409 >>	BREAK
		CASE 29 vCoords = << 1114.11, 235.828, -50.8409 >>	BREAK
		CASE 30 vCoords = << 1115.56, 234.76, -50.8409 >>	BREAK
		CASE 31 vCoords = << 1115, 233.054, -50.8409 >>		BREAK
		
		//right front pole
		CASE 32 vCoords = << 1116.22, 228.28, -50.8409 >>	BREAK
		CASE 33 vCoords = << 1115.68, 229.995, -50.8409 >> 	BREAK
		CASE 34 vCoords = << 1117.14, 231.042, -50.8409 >> 	BREAK
		CASE 35 vCoords = << 1118.59, 229.975, -50.8409 >> 	BREAK
		CASE 36 vCoords = << 1118.02, 228.269, -50.8409 >>	BREAK
		
		//back room bottom wall
		CASE 37 vCoords = << 1129.64, 251.203, -52.0409 >>	BREAK
		CASE 38 vCoords = << 1130.57, 251.085, -52.0409 >> 	BREAK
		CASE 39 vCoords = << 1131.44, 250.73, -52.0409 >>	BREAK
		CASE 40 vCoords = << 1132.19, 250.159, -52.0409 >> 	BREAK
		CASE 41 vCoords = << 1132.76, 249.412, -52.0409 >> 	BREAK
		CASE 42 vCoords = << 1133.12, 248.533, -52.0409 >> 	BREAK
		CASE 43 vCoords = << 1133.24, 247.598, -52.0409 >> 	BREAK
		
		//back room left pole
		CASE 44 vCoords = << 1133.42, 255.572, -52.0409 >>	BREAK
		CASE 45 vCoords = << 1133.16, 257.251, -52.0409 >>	BREAK
		CASE 46 vCoords = << 1134.67, 258.021, -52.0409 >>	BREAK
		CASE 47 vCoords = << 1135.87, 256.819, -52.0409 >>	BREAK
		CASE 48 vCoords = << 1135.1, 255.303, -52.0409 >>	BREAK 
		
		//back room right pole
		CASE 49 vCoords = << 1137.66, 251.328, -52.0409 >> 	BREAK
		CASE 50	vCoords = << 1137.4, 253.008, -52.0409 >>	BREAK
		CASE 51	vCoords = << 1138.92, 253.779, -52.0409 >> 	BREAK
		CASE 52	vCoords = << 1140.12, 252.574, -52.0409 >> 	BREAK
		CASE 53	vCoords = << 1139.34, 251.061, -52.0409 >> 	BREAK
	ENDSWITCH
	
	IF NOT ARE_VECTORS_EQUAL(vCoords, <<0.0, 0.0, 0.0>>)
		// Chair offset by 1m
		vCoords += <<0.0, 0.0, 1.0>>
		
		// Female ped offset by 1.91 cm
		IF IS_CASINO_AMBIENT_PED_FEMALE(ePedType)
		AND (ePedType != CASINO_EILEEN) // B*5971962 Eileen using no heel anims now
			vCoords += <<0.0, 0.0, 0.0191>>
		ENDIF
	ENDIF
	RETURN vCoords
ENDFUNC

FUNC VECTOR GET_SLOT_MACHINE_PED_COORDS(INT iPedID, CASINO_PED_TYPES ePedType = CASINO_GENERIC_MALE)
	INT iSlotMachine = GET_SLOT_MACHINE_ID(iPedID)
	RETURN GET_SLOT_MACHINE_CHAIR_COORDS(iSlotMachine, ePedType)
ENDFUNC

FUNC FLOAT GET_SLOT_MACHINE_HEADING(INT iSlotMachine)
	SWITCH iSlotMachine
		//main left
		CASE 0 RETURN 45.0
		CASE 1 RETURN 60.0
		CASE 2 RETURN 75.0
		CASE 3 RETURN 90.0
		CASE 4 RETURN 105.0
		
		//main top
		CASE 5 RETURN -45.0
		CASE 6 RETURN -30.0
		CASE 7 RETURN -15.0
		CASE 8 RETURN 0.0
		CASE 9 RETURN 15.0
		CASE 10 RETURN 30.0
		CASE 11 RETURN 45.0
		
		//main right
		CASE 12 RETURN -105.0
		CASE 13 RETURN -90.0
		CASE 14 RETURN -75.0
		CASE 15 RETURN -60.0
		CASE 16 RETURN -45.0
		
		//left front pole
		CASE 17 RETURN -36.0
		CASE 18 RETURN -108.0
		CASE 19 RETURN 180.0
		CASE 20 RETURN 108.0
		CASE 21 RETURN 36.0
		
		//left back pole
		CASE 22 RETURN -36.0
		CASE 23 RETURN -108.0
		CASE 24 RETURN 180.0
		CASE 25 RETURN 108.0
		CASE 26 RETURN 36.0
		
		//right back pole
		CASE 27 RETURN -36.0
		CASE 28 RETURN -108.0
		CASE 29 RETURN 180.0
		CASE 30 RETURN 108.0
		CASE 31 RETURN 36.0
		
		//right front pole
		CASE 32 RETURN -36.0
		CASE 33 RETURN -108.0
		CASE 34 RETURN 180.0
		CASE 35 RETURN 108.0
		CASE 36 RETURN 36.0
		
		//back room bottom wall
		CASE 37 RETURN 180.0
		CASE 38 RETURN 165.0
		CASE 39 RETURN 150.0
		CASE 40 RETURN 135.0
		CASE 41 RETURN 120.0
		CASE 42 RETURN 105.0
		CASE 43 RETURN 90.0
		
		//back room left pole
		CASE 44 RETURN -45.0
		CASE 45 RETURN -117.0
		CASE 46 RETURN 171.0
		CASE 47 RETURN 99.0
		CASE 48 RETURN 27.0
		
		//back room right pole
		CASE 49 RETURN -45.0
		CASE 50 RETURN -117.0
		CASE 51 RETURN 171.0
		CASE 52 RETURN 99.0
		CASE 53	RETURN 27.0
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_SLOT_MACHINE_PED_HEADING(INT iPedID)
	INT iSlotMachine = GET_SLOT_MACHINE_ID(iPedID)
	RETURN GET_SLOT_MACHINE_HEADING(iSlotMachine)
ENDFUNC


FUNC VECTOR GET_SLOT_MACHINE_SPECTATOR_COORDS()
	VECTOR vOffset = <<0.0, 0.0, 0.0>>
	CASINO_ACTIVITY_SLOTS eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachineSpectatorActivity)
	CASINO_PED_TYPES ePedType = INT_TO_ENUM(CASINO_PED_TYPES, g_iSpectatingSlotMachinePedType)
	BOOL bFemalePed = IS_CASINO_PED_TYPE_FEMALE(ePedType)
	
	IF bFemalePed
		SWITCH eActivity
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_01A				vOffset = <<0.3, -0.85, 0.0>>		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_01B				vOffset = <<0.1, -0.9, 0.0>>		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_02A				vOffset = <<0.1, -0.9, 0.0>>		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_02B				vOffset = <<0.45, -0.4, 0.0>>		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_WITH_DRINK_01A	vOffset = <<-0.55, -0.54, 0.0>>		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_WITH_DRINK_01B	vOffset = <<-0.15, -0.6, 0.0>>		BREAK
			CASE CASINO_SPECIAL_PED_CAROL_SLOTS							vOffset = <<-0.5, -0.65, 0.0>>		BREAK
		ENDSWITCH
	ELSE
		SWITCH eActivity
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_01A				vOffset = <<-0.3, -0.65, 0.0>>		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_01B				vOffset = <<-0.3, -0.65, 0.0>>		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_02A				vOffset = <<0.6, -0.6, 0.0>>		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_02B				vOffset = <<0.0, -0.8, 0.0>>		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_WITH_DRINK_01A	vOffset = <<0.5, -0.45, 0.0>>		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_WITH_DRINK_01B	vOffset = <<-0.65, -0.45, 0.0>>		BREAK
		ENDSWITCH
	ENDIF
	
	RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_SLOT_MACHINE_PED_COORDS(g_iSpectatingSlotMachinePedID), GET_SLOT_MACHINE_PED_HEADING(g_iSpectatingSlotMachinePedID), vOffset)
ENDFUNC

FUNC VECTOR GET_TABLE_SPECTATOR_COORDS(INT iPedID, CASINO_ACTIVITY_SLOTS eActivity)
	
	CASINO_TABLE_GAME_TYPE eGameType
	IF (g_iPedReservedBlackjackTable = 0 OR g_iPedReservedBlackjackTable = 1)
		eGameType = CASINO_TABLE_GAME_BLACKJACK
	ELIF (g_iPedReservedRouletteTable = 0 OR g_iPedReservedRouletteTable = 1)
		eGameType = CASINO_TABLE_GAME_ROULETTE
	ENDIF
	
	SWITCH eGameType
		CASE CASINO_TABLE_GAME_BLACKJACK
			IF g_iPedReservedBlackjackTable = 0
				SWITCH eActivity
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_01A
						SWITCH iPedID
							CASE 88	RETURN <<1149.160,  271.201,  -51.815>>
							CASE 89 RETURN <<1147.460,  269.311,  -51.815>>
						ENDSWITCH
					BREAK
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_02A
						SWITCH iPedID
							CASE 88	RETURN <<1149.160,  271.201,  -51.815>>
							CASE 89 RETURN <<1147.380,  269.431,  -51.815>>
						ENDSWITCH
					BREAK
				ENDSWITCH
			ELIF g_iPedReservedBlackjackTable = 1
				SWITCH eActivity
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_01A
						SWITCH iPedID
							CASE 88	RETURN <<1151.670,  265.241,  -51.815>>
							CASE 89 RETURN <<1153.410,  267.061,  -51.815>>
						ENDSWITCH
					BREAK
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_02A
						SWITCH iPedID
							CASE 88	RETURN <<1151.670,  265.241,  -51.815>>
							CASE 89 RETURN <<1153.410,  267.061,  -51.815>>
						ENDSWITCH
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CASINO_TABLE_GAME_ROULETTE
			IF g_iPedReservedRouletteTable = 0
				SWITCH eActivity
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_01A
						SWITCH iPedID
							CASE 88	RETURN <<1143.509,  269.220,  -51.815>>
							CASE 89 RETURN <<1142.869,  267.760,  -51.815>>
						ENDSWITCH
					BREAK
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_02A
						SWITCH iPedID
							CASE 88	RETURN <<1143.509,  269.220,  -51.815>>
							CASE 89 RETURN <<1142.869,  267.760,  -51.815>>
						ENDSWITCH
					BREAK
				ENDSWITCH
			ELIF g_iPedReservedRouletteTable = 1
				SWITCH eActivity
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_01A
						SWITCH iPedID
							CASE 88	RETURN <<1151.692,  261.839,  -51.825>>
							CASE 89 RETURN <<1152.219,  263.310,  -51.815>>
						ENDSWITCH
					BREAK
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_02A
						SWITCH iPedID
							CASE 88	RETURN <<1151.692,  261.839,  -51.825>>
							CASE 89 RETURN <<1152.219,  263.310,  -51.815>>
						ENDSWITCH
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_BLACKJACK_DEALER_COORDS(INT iTable)
	SWITCH iTable
		CASE 0	RETURN <<1149.3910, 269.1934, -51.8409>>
		CASE 1	RETURN <<1151.2858, 267.3007, -51.8409>>
		CASE 2	RETURN <<1128.8528, 261.8036, -51.0410>>
		CASE 3	RETURN <<1143.8754, 246.7810, -51.0410>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_THREE_CARD_POKER_DEALER_COORDS(INT iTable)
	SWITCH iTable
		CASE 0	RETURN <<1143.8734, 263.7043, -51.8409>>
		CASE 1	RETURN <<1145.7935, 261.7953, -51.8409>>
		CASE 2	RETURN <<1134.2810, 267.2302, -51.0409>>
		CASE 3	RETURN <<1149.2810, 252.2302, -51.0409>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_ROULETTE_DEALER_COORDS(INT iTable)
	SWITCH iTable
		CASE 0	RETURN <<1145.3370, 267.7967, -51.8409>>
		CASE 1	RETURN <<1149.7910, 263.1628, -51.8409>>
		CASE 2	RETURN <<1133.4916, 261.5838, -51.0409>>
		CASE 3	RETURN <<1130.0356, 267.8274, -51.0409>>
		CASE 4	RETURN <<1145.0582, 252.8048, -51.0409>>
		CASE 5	RETURN <<1148.5404, 246.5209, -51.0409>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_MANAGERS_COORDS()
	SWITCH g_iManagerPedVariation
		CASE 0 	RETURN <<1109.251, 241.0657, -45.85349>>
		CASE 1 	RETURN <<1114.522, 245.4032, -45.85349>>
		CASE 2 	RETURN <<1107.124, 247.0657, -46.44099>>
		CASE 3 	RETURN <<1111.056, 242.696, -45.737>>
		CASE 4	RETURN <<1114.021, 240.8032, -45.85349>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_CASINO_PED_POSITION(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea, CASINO_PED_TYPES ePedType, CASINO_ACTIVITY_SLOTS eActivity #IF IS_DEBUG_BUILD, BOOL bDebugPrints = FALSE #ENDIF)
	
	#IF IS_DEBUG_BUILD
	IF bDebugPrints
		PRINTLN("[CASINO_PEDS] GET_CASINO_PED_POSITION - Ped: ", iPedID, " Layout: ", iLayout, " Casino Area: ", GET_CASINO_AREA_STRING(eCasinoArea))
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_CASINO_PED_DEBUG_EDITOR_ACTIVE()
	AND IS_CASINO_PED_DEBUG_REPOSITION_PEDS_WIDGET_ACTIVE()
		IF NOT IS_VECTOR_ZERO(g_vCasinoPedPos[iPedID])
			IF bDebugPrints
				PRINTLN("[CASINO_PEDS] GET_CASINO_PED_POSITION - Debug Ped Coords: ", g_vCasinoPedPos[iPedID])
			ENDIF
			RETURN g_vCasinoPedPos[iPedID]
		ENDIF
	ENDIF
	#ENDIF
	
	VECTOR vSpawnCoords = <<0,0,0>>	
	IF OVERRIDE_CASINO_SPECIAL_PED_POSITION(iPedID,iLayout,eCasinoArea,vSpawnCoords)
	AND NOT g_bCasinoIntroCutsceneRequired
		#IF IS_DEBUG_BUILD
		IF bDebugPrints
			PRINTLN("[CASINO_PEDS] GET_CASINO_PED_POSITION - Special Ped Coords: ", vSpawnCoords)
		ENDIF
		#ENDIF
		RETURN vSpawnCoords
	ENDIF
	
	SWITCH eCasinoArea
		CASE CASINO_AREA_PERMANENT
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						CASE 0		vSpawnCoords = <<1113.9120, 209.0580, -49.5160>>	BREAK
						CASE 1		vSpawnCoords = <<1109.9170, 207.6400, -49.5160>>	BREAK
						CASE 2		vSpawnCoords = <<1101.2720, 253.2630, -50.8430>>	BREAK
						CASE 3		vSpawnCoords = <<1116.6935, 237.6115, -49.8400>>	BREAK
						CASE 4		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 5		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 6		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 7		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 8		vSpawnCoords = <<1117.2855, 237.0365, -49.8400>>	BREAK
						CASE 9		vSpawnCoords = <<1118.0215, 232.8820, -49.8200>>	BREAK
						CASE 10		vSpawnCoords = <<1145.8165, 257.6440, -51.4370>>	BREAK
						CASE 11		vSpawnCoords = <<1117.9675, 233.7870, -49.8400>>	BREAK
						CASE 12		vSpawnCoords = GET_SLOT_MACHINE_SPECTATOR_COORDS()	BREAK
						CASE 13		vSpawnCoords = <<1112.9745, 225.3240, -49.8400>>	BREAK
						CASE 14		vSpawnCoords = <<1123.6324, 241.5535, -50.4350>>	BREAK
						CASE 15		vSpawnCoords = <<1123.4814, 240.6735, -50.4350>>	BREAK
						CASE 16		vSpawnCoords = <<1122.7645, 241.3840, -50.4350>>	BREAK
						CASE 17		vSpawnCoords = <<1122.3765, 252.3175, -50.0500>>	BREAK
						CASE 18		vSpawnCoords = <<1133.7690, 233.9530, -50.4350>>	BREAK
						CASE 19		vSpawnCoords = <<1133.3685, 233.1855, -50.4350>>	BREAK
						CASE 20		vSpawnCoords = <<1134.3685, 240.3585, -50.0500>>	BREAK
						CASE 21		vSpawnCoords = <<1139.6105, 238.5480, -50.4350>>	BREAK
						CASE 22		vSpawnCoords = <<1139.3555, 237.6185, -50.4350>>	BREAK
						CASE 23		vSpawnCoords = <<1126.8936, 236.2065, -50.4350>>	BREAK
						CASE 24		vSpawnCoords = <<1138.4146, 245.6525, -50.4350>>	BREAK
						CASE 25		vSpawnCoords = <<1115.0775, 251.6440, -50.4350>>	BREAK
						CASE 26		vSpawnCoords = <<1115.6265, 252.3380, -50.4350>>	BREAK
						CASE 27		vSpawnCoords = <<1115.9344, 251.5260, -50.4350>>	BREAK
						CASE 28		vSpawnCoords = <<1119.8635, 258.3320, -50.4350>>	BREAK
						CASE 29		vSpawnCoords = <<1120.8375, 258.3415, -50.4350>>	BREAK
						CASE 30		vSpawnCoords = <<1136.3160, 250.5375, -51.0430>>	BREAK
						CASE 31		vSpawnCoords = <<1135.5890, 251.0910, -51.0230>>	BREAK
						CASE 32		vSpawnCoords = <<1135.4265, 250.2435, -51.0230>>	BREAK
						CASE 33		vSpawnCoords = <<1132.4510, 253.3590, -51.0430>>	BREAK
						CASE 34		vSpawnCoords = <<1133.4590, 253.4960, -51.0430>>	BREAK
						CASE 35		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 36		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 37		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 38		vSpawnCoords = <<1126.0145, 254.1719, -50.4350>>	BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						CASE 0		vSpawnCoords = <<1113.9120, 209.0580, -49.5160>>	BREAK
						CASE 1		vSpawnCoords = <<1109.9170, 207.6400, -49.5160>>	BREAK
						CASE 2		vSpawnCoords = <<1101.2720, 253.2630, -50.8430>>	BREAK
						CASE 3		vSpawnCoords = <<1117.3149, 237.8640, -49.8400>>	BREAK
						CASE 4		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 5		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 6		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 7		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 8		vSpawnCoords = <<1117.5715, 233.6090, -49.8400>>	BREAK
						CASE 9		vSpawnCoords = <<1117.2745, 232.5995, -49.8200>>	BREAK
						CASE 10		vSpawnCoords = <<1145.8165, 257.6440, -51.4370>>	BREAK
						CASE 11		vSpawnCoords = <<1118.1345, 232.8295, -49.8400>>	BREAK
						CASE 12		vSpawnCoords = GET_SLOT_MACHINE_SPECTATOR_COORDS()	BREAK
						CASE 13		vSpawnCoords = <<1112.9504, 222.9415, -49.8400>>	BREAK
						CASE 14		vSpawnCoords = <<1122.4365, 242.0110, -50.4350>>	BREAK
						CASE 15		vSpawnCoords = <<1124.9825, 238.8210, -50.4350>>	BREAK
						CASE 16		vSpawnCoords = <<1121.6885, 242.2140, -50.4350>>	BREAK
						CASE 17		vSpawnCoords = <<1122.3765, 252.3175, -50.0500>>	BREAK
						CASE 18		vSpawnCoords = <<1135.5165, 234.3655, -50.4350>>	BREAK
						CASE 19		vSpawnCoords = <<1134.7275, 233.8855, -50.4350>>	BREAK
						CASE 20		vSpawnCoords = <<1134.3685, 240.3585, -50.0500>>	BREAK
						CASE 21		vSpawnCoords = <<1125.0615, 239.7465, -50.4450>>	BREAK
						CASE 22		vSpawnCoords = <<1134.6475, 234.7610, -50.4350>>	BREAK
						CASE 23		vSpawnCoords = <<1140.9095, 238.3730, -50.4350>>	BREAK
						CASE 24		vSpawnCoords = <<1138.2855, 244.8850, -50.4350>>	BREAK
						CASE 25		vSpawnCoords = <<1115.0775, 251.6440, -50.4350>>	BREAK
						CASE 26		vSpawnCoords = <<1115.6265, 252.3380, -50.4350>>	BREAK
						CASE 27		vSpawnCoords = <<1120.3145, 257.4560, -50.4350>>	BREAK
						CASE 28		vSpawnCoords = <<1119.4315, 258.0345, -50.4350>>	BREAK
						CASE 29		vSpawnCoords = <<1120.4355, 258.3415, -50.4350>>	BREAK
						CASE 30		vSpawnCoords = <<1136.3160, 250.5375, -51.0430>>	BREAK
						CASE 31		vSpawnCoords = <<1135.5890, 251.0910, -51.0230>>	BREAK
						CASE 32		vSpawnCoords = <<1132.9755, 252.6185, -51.0230>>	BREAK
						CASE 33		vSpawnCoords = <<1132.4510, 253.3590, -51.0430>>	BREAK
						CASE 34		vSpawnCoords = <<1133.3215, 253.6585, -50.9930>>	BREAK
						CASE 35		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 36		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 37		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 38		vSpawnCoords = <<1138.3365, 244.0069, -50.4350>>	BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPedID
						CASE 0		vSpawnCoords = <<1113.9120, 209.0580, -49.5160>>	BREAK
						CASE 1		vSpawnCoords = <<1109.9170, 207.6400, -49.5160>>	BREAK
						CASE 2		vSpawnCoords = <<1140.3177, 243.0237, -50.4350>>	BREAK
						CASE 3		vSpawnCoords = <<1130.1677, 239.2526, -50.5900>>	BREAK
						CASE 4		vSpawnCoords = <<1129.3060, 240.1123, -50.5200>>	BREAK
						CASE 5		vSpawnCoords = <<1130.8550, 243.8468, -50.0500>>	BREAK
						CASE 6		vSpawnCoords = <<1130.4205, 242.9311, -50.0500>>	BREAK
						CASE 7		vSpawnCoords = <<1131.2085, 242.9690, -50.0500>>	BREAK
						CASE 8		vSpawnCoords = <<1134.2313, 232.3333, -50.4350>>	BREAK
						CASE 9		vSpawnCoords = <<1123.9095, 243.3287, -50.4350>>	BREAK
						CASE 10		vSpawnCoords = <<0.0000, 0.0000, 0.0000>>	BREAK
						CASE 11		vSpawnCoords = <<1123.4164, 244.1383, -50.4350>>	BREAK
						CASE 12		vSpawnCoords = <<1120.7438, 242.6565, -50.4350>>	BREAK
						CASE 13		vSpawnCoords = <<1121.6808, 242.4619, -50.4350>>	BREAK
						CASE 14		vSpawnCoords = <<1115.6172, 250.5871, -50.4350>>	BREAK
						CASE 15		vSpawnCoords = <<1116.5774, 250.6152, -50.4350>>	BREAK
						CASE 16		vSpawnCoords = <<1124.2587, 250.8920, -50.0500>>	BREAK
						CASE 17		vSpawnCoords = <<1124.2521, 249.7859, -50.0500>>	BREAK
						CASE 18		vSpawnCoords = <<1123.0688, 249.8564, -50.0500>>	BREAK
						CASE 19		vSpawnCoords = <<1122.8199, 251.1461, -50.0500>>	BREAK
						CASE 20		vSpawnCoords = <<1133.4613, 243.5503, -50.0500>>	BREAK
						CASE 21		vSpawnCoords = <<1133.0098, 242.9285, -50.0500>>	BREAK
						CASE 22		vSpawnCoords = <<1121.5393, 259.5484, -50.4350>>	BREAK
					ENDSWITCH
				BREAK
				CASE 4
					SWITCH iPedID
						// Permanent
						CASE 0		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(0, ePedType)	BREAK
						CASE 1		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(10, ePedType)	BREAK
						// Table Games
						CASE 2		vSpawnCoords = <<1101.0031, 214.9490, -48.9860>>		BREAK
						CASE 3		vSpawnCoords = GET_THREE_CARD_POKER_DEALER_COORDS(2)	BREAK
						CASE 4		vSpawnCoords = <<1113.8585, 225.1640, -49.8400>>	BREAK
						CASE 5		vSpawnCoords = <<1137.0334, 260.3040, -51.4370>>	BREAK
						CASE 6		vSpawnCoords = <<1151.1475, 258.0975, -51.3970>>	BREAK
						CASE 7		vSpawnCoords = <<1150.5715, 257.7925, -51.4370>>	BREAK
						CASE 8		vSpawnCoords = GET_ROULETTE_DEALER_COORDS(2)			BREAK
						CASE 9		vSpawnCoords = GET_ROULETTE_DEALER_COORDS(3)			BREAK
						CASE 10		vSpawnCoords = GET_BLACKJACK_DEALER_COORDS(2)			BREAK
						CASE 11		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(22, ePedType)	BREAK
						CASE 12		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(27, ePedType)	BREAK
						CASE 13		vSpawnCoords = <<1142.5455, 254.9255, -51.4370>>	BREAK
						CASE 14		vSpawnCoords = <<1141.8354, 254.5215, -51.4370>>	BREAK
						CASE 15		vSpawnCoords = <<1141.8555, 255.2525, -51.4370>>	BREAK
						CASE 16		vSpawnCoords = <<1136.3180, 260.1810, -51.3970>>	BREAK
						CASE 17		vSpawnCoords = <<1136.7695, 259.5200, -51.4370>>	BREAK
						CASE 18		vSpawnCoords = <<1139.2515, 267.7785, -51.4370>>	BREAK
						CASE 19		vSpawnCoords = <<1139.3475, 268.6940, -51.3970>>	BREAK
						CASE 20		vSpawnCoords = <<1140.0045, 268.1645, -51.3970>>	BREAK
						CASE 21		vSpawnCoords = <<1143.6949, 257.9110, -51.3970>>	BREAK
						CASE 22		vSpawnCoords = <<1144.4720, 257.9370, -51.3970>>	BREAK
						CASE 23		vSpawnCoords = <<1154.0455, 264.2510, -51.8480>>	BREAK
						CASE 24		vSpawnCoords = <<1153.7845, 263.3367, -51.8480>>	BREAK
						CASE 25		vSpawnCoords = <<1140.3605, 256.9013, -51.3770>>	BREAK
						CASE 26		vSpawnCoords = <<1141.1385, 257.0070, -51.4370>>	BREAK
						CASE 27		vSpawnCoords = <<1145.2375, 271.7720, -51.8080>>	BREAK
						CASE 28		vSpawnCoords = <<1145.9745, 271.9293, -51.8080>>	BREAK
						CASE 29		vSpawnCoords = <<1148.3575, 266.7345, -51.8480>>	BREAK
						CASE 30		vSpawnCoords = <<1147.5095, 266.5113, -51.8480>>	BREAK
						CASE 31		vSpawnCoords = <<1148.1105, 265.8579, -51.8080>>	BREAK
						CASE 32		vSpawnCoords = <<1122.8245, 263.0550, -51.0550>>	BREAK
						CASE 33		vSpawnCoords = <<1143.8734, 263.7043, -51.8409>>	BREAK 	// Poker Dealer 0
						CASE 34		vSpawnCoords = <<1145.7935, 261.7953, -51.8409>>	BREAK	// Poker Dealer 1
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_MAIN_FLOOR
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Entrance (15)
						CASE 39		vSpawnCoords = <<1088.9565, 212.4370, -48.9660>>	BREAK
						CASE 40		vSpawnCoords = <<1086.9716, 206.0560, -48.9860>>	BREAK
						CASE 41		vSpawnCoords = <<1113.2015, 204.7450, -49.4485>>	BREAK
						CASE 42		vSpawnCoords = <<1091.5186, 217.2425, -49.1740>>	BREAK
						CASE 43		vSpawnCoords = <<1099.2595, 226.4900, -48.9860>>	BREAK
						CASE 44		vSpawnCoords = <<1094.4196, 223.0400, -48.9860>>	BREAK
						CASE 45		vSpawnCoords = <<1094.0095, 222.1700, -48.9860>>	BREAK
						CASE 46		vSpawnCoords = <<1116.8665, 213.2050, -49.4440>>	BREAK
						CASE 47		vSpawnCoords = <<1088.3475, 212.0230, -48.9860>>	BREAK
						CASE 48		vSpawnCoords = <<1084.5905, 217.9915, -49.2240>>	BREAK
						CASE 49		vSpawnCoords = <<1092.5024, 217.1670, -49.2040>>	BREAK
						CASE 50		vSpawnCoords = <<1092.0255, 216.4510, -49.2040>>	BREAK
						CASE 51		vSpawnCoords = <<1095.8625, 208.0835, -48.9860>>	BREAK
						CASE 52		vSpawnCoords = <<1096.3405, 208.6385, -48.9860>>	BREAK
						CASE 53		vSpawnCoords = <<1095.5605, 208.8155, -48.9860>>	BREAK
						// Behind Bar (13)
						CASE 54		vSpawnCoords = <<1119.8550, 210.2220, -49.9460>>	BREAK
						CASE 55		vSpawnCoords = <<1115.0405, 206.1520, -49.7310>>	BREAK
						CASE 56		vSpawnCoords = <<1113.1495, 211.5060, -49.7235>>	BREAK
						CASE 57		vSpawnCoords = <<1113.8254, 200.7025, -49.9415>>	BREAK
						CASE 58		vSpawnCoords = <<1113.3975, 201.9220, -49.4040>>	BREAK
						CASE 59		vSpawnCoords = <<1090.8900, 192.0700, -49.5200>>	BREAK
						CASE 60		vSpawnCoords = <<1113.2930, 214.4700, -49.4440>>	BREAK
						CASE 61		vSpawnCoords = <<1104.0455, 234.7050, -49.8400>>	BREAK
						CASE 62		vSpawnCoords = <<1105.7760, 224.4380, -49.2080>>	BREAK
						CASE 63		vSpawnCoords = <<1113.9965, 214.1345, -49.4440>>	BREAK
						CASE 64		vSpawnCoords = <<1104.4764, 235.4725, -49.8400>>	BREAK
						CASE 65		vSpawnCoords = <<1104.9875, 234.8050, -49.8400>>	BREAK
						CASE 66		vSpawnCoords = <<1119.5430, 208.7070, -49.9410>>	BREAK
						// All the rest (53)
						CASE 67		vSpawnCoords = <<1101.0031, 214.9490, -48.9860>>	BREAK
						CASE 68		vSpawnCoords = <<1105.8015, 223.1215, -48.9660>>	BREAK
						CASE 69		vSpawnCoords = <<1106.1595, 222.4235, -48.9860>>	BREAK
						CASE 70		vSpawnCoords = <<1108.3050, 218.2440, -49.4235>>	BREAK
						CASE 71		vSpawnCoords = <<1107.5485, 217.6190, -49.4440>>	BREAK
						CASE 72		vSpawnCoords = <<1107.4625, 224.9230, -49.8200>>	BREAK
						CASE 73		vSpawnCoords = <<1108.1135, 225.4100, -49.8200>>	BREAK
						CASE 74		vSpawnCoords = <<1108.2015, 224.4080, -49.8400>>	BREAK
						CASE 75		vSpawnCoords = <<1113.8585, 225.1640, -49.8400>>	BREAK
						CASE 76		vSpawnCoords = <<1113.3055, 224.4100, -49.8400>>	BREAK
						CASE 77		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 78		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 79		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 80		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
//						CASE 81		vSpawnCoords = <<1116.6935, 237.6115, -49.8400>>	BREAK
						CASE 82		vSpawnCoords = <<1111.6075, 219.0760, -49.4440>>	BREAK
						CASE 83		vSpawnCoords = <<1112.5680, 219.0310, -49.4440>>	BREAK
						CASE 84		vSpawnCoords = <<1112.0175, 218.4295, -49.4440>>	BREAK
						CASE 85		vSpawnCoords = <<1102.4525, 211.3055, -49.4440>>	BREAK
						CASE 86		vSpawnCoords = <<1102.2875, 210.4355, -49.4040>>	BREAK
						CASE 87		vSpawnCoords = <<1101.6980, 211.0780, -49.4040>>	BREAK
						CASE 88		vSpawnCoords = <<1103.7715, 207.8645, -49.4040>>	BREAK
						CASE 89		vSpawnCoords = <<1103.0045, 207.7775, -49.4040>>	BREAK
						CASE 90		vSpawnCoords = <<1105.5515, 210.9350, -49.4240>>	BREAK
						CASE 91		vSpawnCoords = <<1106.3730, 211.4370, -49.4340>>	BREAK
						CASE 92		vSpawnCoords = <<1109.8680, 214.1860, -49.4040>>	BREAK
						CASE 93		vSpawnCoords = <<1109.0295, 214.5840, -49.4040>>	BREAK
						CASE 94		vSpawnCoords = <<1106.0850, 210.2550, -49.4240>>	BREAK
						CASE 95		vSpawnCoords = <<1110.6906, 211.4035, -49.4435>>	BREAK
						CASE 96		vSpawnCoords = <<1109.324, 205.450, -49.7260>>		BREAK
						CASE 97		vSpawnCoords = <<1108.3575, 199.9490, -49.4040>>	BREAK
						CASE 98		vSpawnCoords = <<1103.4835, 197.9990, -49.4040>>	BREAK
						CASE 99		vSpawnCoords = <<1108.4675, 200.6430, -49.4040>>	BREAK
						CASE 100	vSpawnCoords = <<1106.1545, 202.6385, -49.4240>>	BREAK
						CASE 101	vSpawnCoords = <<1105.2305, 202.3530, -49.4240>>	BREAK
						CASE 102	vSpawnCoords = <<1105.2125, 205.4240, -49.4440>>	BREAK
						CASE 103	vSpawnCoords = <<1105.9755, 205.8145, -49.4240>>	BREAK
						CASE 104	vSpawnCoords = <<1105.7896, 204.9860, -49.4240>>	BREAK
						CASE 105	vSpawnCoords = <<1099.8810, 206.6930, -49.4240>>	BREAK
						CASE 106	vSpawnCoords = <<1100.1625, 207.4905, -49.4440>>	BREAK
						CASE 107	vSpawnCoords = <<1105.6420, 214.2970, -49.4440>>	BREAK
						CASE 108	vSpawnCoords = <<1106.2325, 213.7785, -49.4440>>	BREAK
						CASE 109	vSpawnCoords = <<1108.5795, 206.8625, -49.4485>>	BREAK
						CASE 110	vSpawnCoords = <<1101.2375, 241.8610, -50.4370>>	BREAK
						CASE 111	vSpawnCoords = <<1109.056,  213.736, -49.4040>>		BREAK
						CASE 112	vSpawnCoords = <<1116.0815, 212.6625, -49.4440>>	BREAK
						CASE 113	vSpawnCoords = <<1100.0790, 214.9300, -48.9860>>	BREAK
						CASE 114	vSpawnCoords = <<1108.3400, 217.4060, -49.4040>>	BREAK
						CASE 115	vSpawnCoords = <<1096.5480, 241.7300, -50.4370>>	BREAK
						CASE 116	vSpawnCoords = <<1097.3690, 241.3750, -50.4370>>	BREAK
						CASE 117	vSpawnCoords = <<1097.7784, 245.9935, -50.4170>>	BREAK
						CASE 118	vSpawnCoords = <<1098.5804, 245.6090, -50.4170>>	BREAK
						CASE 119	vSpawnCoords = <<1097.7665, 245.1425, -50.4370>>	BREAK
						// Special peds (5)
						CASE 120	vSpawnCoords = <<1117.4315, 219.9995, -49.6525>>	BREAK
						CASE 121	vSpawnCoords = <<1094.5100, 197.5900, -50.5160>>	BREAK
						CASE 122	vSpawnCoords = <<1088.940, 221.124, -49.178>>		BREAK
						CASE 123	vSpawnCoords = <<1084.2850, 215.3040, -49.2240>>	BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Entrance (15)
						CASE 39		vSpawnCoords = <<1089.5535, 212.9495, -48.9860>>	BREAK
						CASE 40		vSpawnCoords = <<1092.7775, 206.8735, -48.9860>>	BREAK
						CASE 41		vSpawnCoords = <<1113.8685, 205.0000, -49.4460>>	BREAK
						CASE 42		vSpawnCoords = <<1085.5275, 218.6250, -49.2040>>	BREAK
						CASE 43		vSpawnCoords = <<1093.6060, 221.2350, -48.9860>>	BREAK
						CASE 44		vSpawnCoords = <<1098.0496, 225.7375, -48.9860>>	BREAK
						CASE 45		vSpawnCoords = <<1097.1235, 225.3325, -48.9860>>	BREAK
						CASE 46		vSpawnCoords = <<1114.6705, 214.0225, -49.4440>>	BREAK
						CASE 47		vSpawnCoords = <<1088.7325, 212.2180, -48.9860>>	BREAK
						CASE 48		vSpawnCoords = <<1085.0804, 217.7390, -49.2040>>	BREAK
						CASE 49		vSpawnCoords = <<1092.5024, 217.1670, -49.2040>>	BREAK
						CASE 50		vSpawnCoords = <<1089.9135, 212.1360, -48.9640>>	BREAK
						CASE 51		vSpawnCoords = <<1095.5916, 208.0585, -48.9860>>	BREAK
						CASE 52		vSpawnCoords = <<1104.9905, 199.5510, -49.4335>>	BREAK
						CASE 53		vSpawnCoords = <<1094.6635, 208.2830, -48.9860>>	BREAK
						// Behind Bar (13)
						CASE 54		vSpawnCoords = <<1118.2725, 203.9920, -49.9460>>	BREAK
						CASE 55		vSpawnCoords = <<1115.0405, 206.1520, -49.7310>>	BREAK
						CASE 56		vSpawnCoords = <<1113.1495, 211.5060, -49.7235>>	BREAK
						CASE 57		vSpawnCoords = <<1111.4084, 202.2675, -49.4040>>	BREAK
						CASE 58		vSpawnCoords = <<1111.0754, 201.4445, -49.4040>>	BREAK
						CASE 59		vSpawnCoords = <<1090.8900, 192.0700, -49.5200>>	BREAK
						CASE 60		vSpawnCoords = <<1113.9045, 213.5275, -49.4440>>	BREAK
						CASE 61		vSpawnCoords = <<1104.9135, 234.7050, -49.8400>>	BREAK
						CASE 62		vSpawnCoords = <<1108.3165, 227.7855, -49.8405>>	BREAK
						CASE 63		vSpawnCoords = <<1106.6155, 214.3220, -49.4440>>	BREAK
						CASE 64		vSpawnCoords = <<1105.3365, 235.4725, -49.8400>>	BREAK
						CASE 65		vSpawnCoords = <<1114.5945, 225.1575, -49.8400>>	BREAK
						CASE 66		vSpawnCoords = <<1119.5430, 208.7070, -49.9410>>	BREAK
						// All the rest (53)
						CASE 67		vSpawnCoords = <<1104.8365, 215.7115, -48.9860>>	BREAK
						CASE 68		vSpawnCoords = <<1104.5045, 222.0990, -48.9660>>	BREAK
						CASE 69		vSpawnCoords = <<1105.4565, 216.4410, -48.9860>>	BREAK
						CASE 70		vSpawnCoords = <<1108.3050, 218.2440, -49.449>>		BREAK
						CASE 71		vSpawnCoords = <<1108.468, 217.397, -49.421>>		BREAK
						CASE 72		vSpawnCoords = <<1108.7896, 226.9555, -49.8200>>	BREAK
						CASE 73		vSpawnCoords = <<1108.1075, 223.9700, -49.8200>>	BREAK
						CASE 74		vSpawnCoords = <<1108.7015, 223.2505, -49.8400>>	BREAK
						CASE 75		vSpawnCoords = <<1115.2355, 225.9940, -49.8400>>	BREAK
						CASE 76		vSpawnCoords = <<1114.2545, 226.1975, -49.8400>>	BREAK
						CASE 77		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 78		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 79		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 80		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
//						CASE 81		vSpawnCoords = <<1116.6935, 237.6115, -49.8400>>	BREAK
						CASE 82		vSpawnCoords = <<1112.0925, 223.1410, -49.8415>>	BREAK
						CASE 83		vSpawnCoords = <<1112.4495, 219.0310, -49.4440>>	BREAK
						CASE 84		vSpawnCoords = <<1112.1775, 218.2170, -49.4440>>	BREAK
						CASE 85		vSpawnCoords = <<1102.4525, 211.3055, -49.4440>>	BREAK
						CASE 86		vSpawnCoords = <<1102.2875, 210.4355, -49.4040>>	BREAK
						CASE 87		vSpawnCoords = <<1101.6980, 211.0780, -49.4040>>	BREAK
						CASE 88		vSpawnCoords = <<1105.2465, 207.8645, -49.4040>>	BREAK
						CASE 89		vSpawnCoords = <<1104.5895, 207.1975, -49.4040>>	BREAK
						CASE 90		vSpawnCoords = <<1105.7625, 210.6025, -49.4240>>	BREAK
						CASE 91		vSpawnCoords = <<1105.7795, 211.5770, -49.4340>>	BREAK
						CASE 92		vSpawnCoords = <<1109.3655, 213.1860, -49.4040>>	BREAK
						CASE 93		vSpawnCoords = <<1108.6844, 213.7740, -49.4040>>	BREAK
						CASE 94		vSpawnCoords = <<1104.4435, 208.0675, -49.4240>>	BREAK
						CASE 95		vSpawnCoords = <<1109.4305, 210.5910, -49.4460>>	BREAK
						CASE 96		vSpawnCoords = <<1109.324, 205.450, -49.7260>>		BREAK
						CASE 97		vSpawnCoords = <<1108.3575, 199.9490, -49.4040>>	BREAK
						CASE 98		vSpawnCoords = <<1100.3060, 199.1615, -49.4440>>	BREAK
						CASE 99		vSpawnCoords = <<1108.4675, 200.6430, -49.4040>>	BREAK
						CASE 100	vSpawnCoords = <<1106.8275, 203.8910, -49.4240>>	BREAK
						CASE 101	vSpawnCoords = <<1106.4504, 203.0380, -49.4240>>	BREAK
						CASE 102	vSpawnCoords = <<1103.0985, 204.7215, -49.4440>>	BREAK
						CASE 103	vSpawnCoords = <<1102.9926, 205.6595, -49.4240>>	BREAK
						CASE 104	vSpawnCoords = <<1105.7455, 203.9335, -49.4240>>	BREAK
						CASE 105	vSpawnCoords = <<1099.3936, 207.5955, -49.4240>>	BREAK
						CASE 106	vSpawnCoords = <<1100.3044, 208.1305, -49.4440>>	BREAK
						CASE 107	vSpawnCoords = <<1106.0045, 214.7595, -49.4440>>	BREAK
						CASE 108	vSpawnCoords = <<1105.7996, 213.8635, -49.4440>>	BREAK
						CASE 109	vSpawnCoords = <<1108.9655, 206.1975, -49.4535>>	BREAK
						CASE 110	vSpawnCoords = <<1095.2290, 246.3460, -50.5000>>	BREAK
						CASE 111	vSpawnCoords = <<1109.7125, 214.0760, -49.4040>>	BREAK
						CASE 112	vSpawnCoords = <<1114.5804, 212.9925, -49.4200>>	BREAK
						CASE 113	vSpawnCoords = <<1099.4545, 208.4350, -49.4210>>	BREAK
						CASE 114	vSpawnCoords = <<1109.296, 217.7610, -49.4040>>		BREAK
						CASE 115	vSpawnCoords = <<1097.0125, 240.8275, -50.4370>>	BREAK
						CASE 116	vSpawnCoords = <<1097.8815, 240.3825, -50.4370>>	BREAK
						CASE 117	vSpawnCoords = <<1097.7335, 241.2910, -50.4170>>	BREAK
						CASE 118	vSpawnCoords = <<1098.6746, 244.8215, -50.4170>>	BREAK
						CASE 119	vSpawnCoords = <<1099.5435, 244.5075, -50.4370>>	BREAK
						// Special peds (5)
						CASE 120	vSpawnCoords = <<1117.4315, 219.9995, -49.6525>>	BREAK
						CASE 121	vSpawnCoords = <<1094.5100, 197.5900, -50.5160>>	BREAK
						CASE 122	vSpawnCoords = <<1088.940, 221.124, -49.178>>		BREAK
						CASE 123	vSpawnCoords = <<1084.2850, 215.3040, -49.2240>>	BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPedID
						CASE 39		vSpawnCoords = <<0.0000, 0.0000, 0.0000>>	BREAK
						CASE 40		vSpawnCoords = <<1120.0280, 210.9683, -49.9295>>	BREAK
						CASE 41		vSpawnCoords = <<1119.6095, 209.6720, -49.9300>>	BREAK
						CASE 42		vSpawnCoords = <<1115.5670, 207.6465, -49.7310>>	BREAK
						CASE 43		vSpawnCoords = <<1115.0410, 206.1520, -49.7310>>	BREAK
						CASE 44		vSpawnCoords = <<1118.5260, 204.2499, -50.0000>>	BREAK
						CASE 45		vSpawnCoords = <<1108.6538, 198.8187, -49.4502>>	BREAK
						CASE 46		vSpawnCoords = <<1107.6600, 199.2600, -49.4000>>	BREAK
						CASE 47		vSpawnCoords = <<1112.3270, 204.4314, -49.6610>>	BREAK
						CASE 48		vSpawnCoords = <<1110.7600, 204.7800, -49.6000>>	BREAK
						CASE 49		vSpawnCoords = <<1113.7000, 201.7000, -49.4440>>	BREAK
						CASE 50		vSpawnCoords = <<1109.3600, 198.4200, -49.4440>>	BREAK
						CASE 51		vSpawnCoords = <<1110.1100, 199.4000, -49.4000>>	BREAK
						CASE 52		vSpawnCoords = <<1095.9180, 224.6652, -48.9699>>	BREAK
						CASE 53		vSpawnCoords = <<1095.6720, 223.6090, -48.9700>>	BREAK
						CASE 54		vSpawnCoords = <<1121.0411, 225.2831, -49.8400>>	BREAK
						CASE 55		vSpawnCoords = <<1121.2520, 226.3615, -49.8400>>	BREAK
						CASE 56		vSpawnCoords = <<1110.6300, 199.7994, -49.4340>>	BREAK
						CASE 57		vSpawnCoords = <<1111.2780, 200.4258, -49.4440>>	BREAK
						CASE 58		vSpawnCoords = <<1093.6870, 218.7197, -48.9700>>	BREAK
						CASE 59		vSpawnCoords = <<1094.1781, 218.1716, -48.9700>>	BREAK
						CASE 60		vSpawnCoords = <<1094.2090, 217.4156, -48.9700>>	BREAK
						CASE 61		vSpawnCoords = <<1115.6908, 208.7173, -49.4440>>	BREAK
						CASE 62		vSpawnCoords = <<1096.6675, 225.2607, -49.0000>>	BREAK
						CASE 63		vSpawnCoords = <<1119.8199, 213.2200, -49.4440>>	BREAK
						CASE 64		vSpawnCoords = <<1101.5048, 229.2193, -49.8400>>	BREAK
						CASE 65		vSpawnCoords = <<1119.8000, 212.5000, -49.4000>>	BREAK
						CASE 66		vSpawnCoords = <<0.0000, 0.0000, 0.0000>>	BREAK
						CASE 67		vSpawnCoords = <<1095.2200, 223.1100, -49.0000>>	BREAK
						CASE 68		vSpawnCoords = <<1117.8103, 216.0665, -49.4440>>	BREAK
						CASE 69		vSpawnCoords = <<1117.5890, 215.4655, -49.4440>>	BREAK
						CASE 70		vSpawnCoords = <<1113.1000, 203.3000, -49.4000>>	BREAK
						CASE 71		vSpawnCoords = <<1111.4700, 202.1200, -49.4000>>	BREAK
						CASE 72		vSpawnCoords = <<1111.9301, 202.7000, -49.4000>>	BREAK
						CASE 73		vSpawnCoords = <<1121.2096, 228.9851, -49.8400>>	BREAK
						CASE 74		vSpawnCoords = <<1120.8011, 228.2520, -49.8400>>	BREAK
						CASE 75		vSpawnCoords = <<1113.8690, 205.0000, -49.6510>>	BREAK
						CASE 76		vSpawnCoords = <<0.0000, 0.0000, 0.0000>>	BREAK
						CASE 77		vSpawnCoords = <<1113.0640, 204.7166, -49.4440>>	BREAK
						CASE 78		vSpawnCoords = <<1114.6111, 205.5242, -49.4440>>	BREAK
						CASE 79		vSpawnCoords = <<1115.2540, 207.0005, -49.4440>>	BREAK
						CASE 80		vSpawnCoords = <<1115.6691, 204.8346, -49.4440>>	BREAK
						CASE 81		vSpawnCoords = <<1109.6400, 200.7800, -49.4000>>	BREAK
						CASE 82		vSpawnCoords = <<1109.3445, 205.5100, -49.4000>>	BREAK
						CASE 83		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(21, ePedType)	BREAK
						CASE 84		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(2, ePedType)	BREAK
						CASE 85		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(23, ePedType)	BREAK
						CASE 86		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(6, ePedType)	BREAK
						CASE 87		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(18, ePedType)	BREAK
						CASE 88		vSpawnCoords = <<1106.0569, 238.9053, -49.8400>>	BREAK
						CASE 89		vSpawnCoords = <<1106.8779, 238.8721, -49.8400>>	BREAK
						CASE 90		vSpawnCoords = <<1105.5015, 238.3354, -49.8400>>	BREAK
						CASE 91		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(36, ePedType)	BREAK
						CASE 92		vSpawnCoords = <<1099.3591, 237.8142, -50.4370>>	BREAK
						CASE 93		vSpawnCoords = <<1099.5201, 238.7642, -50.4370>>	BREAK
						CASE 94		vSpawnCoords = <<1100.3624, 238.1020, -50.4370>>	BREAK
					ENDSWITCH
				BREAK
				CASE 4
					SWITCH iPedID
						// Entrance (15)
						CASE 39		vSpawnCoords = <<1088.9565, 212.4370, -48.9660>>	BREAK
						CASE 40		vSpawnCoords = <<1086.9716, 206.0560, -48.9860>>	BREAK
						CASE 41		vSpawnCoords = <<1113.2015, 204.7450, -49.4485>>	BREAK
						CASE 42		vSpawnCoords = <<1091.5186, 217.2425, -49.1740>>	BREAK
						CASE 43		vSpawnCoords = <<1099.2595, 226.4900, -48.9860>>	BREAK
						CASE 44		vSpawnCoords = <<1094.4196, 223.0400, -48.9860>>	BREAK
						CASE 45		vSpawnCoords = <<1094.0095, 222.1700, -48.9860>>	BREAK
						CASE 46		vSpawnCoords = <<1116.8665, 213.2050, -49.4440>>	BREAK
						CASE 47		vSpawnCoords = <<1088.3475, 212.0230, -48.9860>>	BREAK
						CASE 48		vSpawnCoords = <<1084.5905, 217.9915, -49.2240>>	BREAK
						CASE 49		vSpawnCoords = <<1092.5024, 217.1670, -49.2040>>	BREAK
						CASE 50		vSpawnCoords = <<1092.0255, 216.4510, -49.2040>>	BREAK
						CASE 51		vSpawnCoords = <<1095.8625, 208.0835, -48.9860>>	BREAK
						CASE 52		vSpawnCoords = <<1096.3405, 208.6385, -48.9860>>	BREAK
						CASE 53		vSpawnCoords = <<1095.5605, 208.8155, -48.9860>>	BREAK
						// Behind Bar (13)
						CASE 54		vSpawnCoords = <<1119.8550, 210.2220, -49.9460>>	BREAK
						CASE 55		vSpawnCoords = <<1115.0405, 206.1520, -49.7310>>	BREAK
						CASE 56		vSpawnCoords = GET_ROULETTE_DEALER_COORDS(0)		BREAK
						CASE 57		vSpawnCoords = <<1113.8254, 200.7025, -49.9415>>	BREAK
						CASE 58		vSpawnCoords = <<1113.3975, 201.9220, -49.4040>>	BREAK
						CASE 59		vSpawnCoords = GET_ROULETTE_DEALER_COORDS(1)		BREAK
						CASE 60		vSpawnCoords = <<1113.2930, 214.4700, -49.4440>>	BREAK
						CASE 61		vSpawnCoords = <<1104.0455, 234.7050, -49.8400>>	BREAK
						CASE 62		vSpawnCoords = <<1105.7760, 224.4380, -49.2080>>	BREAK
						CASE 63		vSpawnCoords = <<1113.9965, 214.1345, -49.4440>>	BREAK
						CASE 64		vSpawnCoords = <<1104.4764, 235.4725, -49.8400>>	BREAK
						CASE 65		vSpawnCoords = <<1104.9875, 234.8050, -49.8400>>	BREAK
						CASE 66		vSpawnCoords = <<1119.5430, 208.7070, -49.9410>>	BREAK
						// All the rest (53)
						CASE 67		vSpawnCoords = GET_BLACKJACK_DEALER_COORDS(0)		BREAK
						CASE 68		vSpawnCoords = <<1105.8015, 223.1215, -48.9660>>	BREAK
						CASE 69		vSpawnCoords = <<1106.1595, 222.4235, -48.9860>>	BREAK
						CASE 70		vSpawnCoords = <<1108.3050, 218.2440, -49.4235>>	BREAK
						CASE 71		vSpawnCoords = <<1107.5485, 217.6190, -49.4440>>	BREAK
						CASE 72		vSpawnCoords = GET_BLACKJACK_CHAIR_COORDS(0, 0, ePedType)		BREAK
						CASE 73		vSpawnCoords = GET_BLACKJACK_CHAIR_COORDS(0, 1, ePedType)		BREAK
						CASE 74		vSpawnCoords = GET_BLACKJACK_CHAIR_COORDS(0, 2, ePedType)		BREAK
						CASE 75		vSpawnCoords = GET_BLACKJACK_CHAIR_COORDS(0, 3, ePedType)		BREAK
						CASE 76		vSpawnCoords = <<1113.3055, 224.4100, -49.8400>>	BREAK
						CASE 77		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(27, ePedType)	BREAK
						CASE 78		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(32, ePedType)	BREAK
						CASE 79		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(51, ePedType)	BREAK
						CASE 80		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(41, ePedType)	BREAK
						CASE 81		vSpawnCoords = GET_BLACKJACK_DEALER_COORDS(1)		BREAK
						CASE 82		vSpawnCoords = <<1111.6075, 219.0760, -49.4440>>	BREAK
						CASE 83		vSpawnCoords = <<1112.5680, 219.0310, -49.4440>>	BREAK
						CASE 84		vSpawnCoords = <<1112.0175, 218.4295, -49.4440>>	BREAK
						CASE 85		vSpawnCoords = <<1102.4525, 211.3055, -49.4440>>	BREAK
						CASE 86		vSpawnCoords = <<1102.2875, 210.4355, -49.4040>>	BREAK
						CASE 87		vSpawnCoords = <<1101.6980, 211.0780, -49.4040>>	BREAK
						CASE 90		vSpawnCoords = <<1105.5515, 210.9350, -49.4240>>	BREAK
						CASE 91		vSpawnCoords = <<1106.3730, 211.4370, -49.4340>>	BREAK
						CASE 92		vSpawnCoords = <<1109.8680, 214.1860, -49.4040>>	BREAK
						CASE 93		vSpawnCoords = <<1109.0295, 214.5840, -49.4040>>	BREAK
						CASE 94		vSpawnCoords = <<1106.0850, 210.2550, -49.4240>>	BREAK
						CASE 95		vSpawnCoords = <<1110.6906, 211.4035, -49.4435>>	BREAK
						CASE 96		vSpawnCoords = <<1109.324, 205.450, -49.7260>>		BREAK
						CASE 98		vSpawnCoords = <<1103.4835, 197.9990, -49.4040>>	BREAK
						CASE 100	vSpawnCoords = <<1106.1545, 202.6385, -49.4240>>	BREAK
						CASE 101	vSpawnCoords = <<1105.2305, 202.3530, -49.4240>>	BREAK
						CASE 102	vSpawnCoords = <<1105.2125, 205.4240, -49.4440>>	BREAK
						CASE 103	vSpawnCoords = <<1105.9755, 205.8145, -49.4240>>	BREAK
						CASE 104	vSpawnCoords = <<1105.7896, 204.9860, -49.4240>>	BREAK
						CASE 105	vSpawnCoords = <<1099.8810, 206.6930, -49.4240>>	BREAK
						CASE 106	vSpawnCoords = <<1100.1625, 207.4905, -49.4440>>	BREAK
						CASE 107	vSpawnCoords = <<1105.6420, 214.2970, -49.4440>>	BREAK
						CASE 108	vSpawnCoords = <<1106.2325, 213.7785, -49.4440>>	BREAK
						CASE 109	vSpawnCoords = <<1108.5795, 206.8625, -49.4485>>	BREAK
						CASE 110	vSpawnCoords = <<1101.2375, 241.8610, -50.4370>>	BREAK
						CASE 111	vSpawnCoords = <<1109.1165, 213.5960, -49.4040>>	BREAK
						CASE 112	vSpawnCoords = <<1116.0815, 212.6625, -49.4440>>	BREAK
						CASE 113	vSpawnCoords = <<1100.0790, 214.9300, -48.9860>>	BREAK
						CASE 114	vSpawnCoords = <<1108.3400, 217.4060, -49.4040>>	BREAK
						CASE 115	vSpawnCoords = <<1096.5480, 241.7300, -50.4370>>	BREAK
						CASE 116	vSpawnCoords = <<1097.3690, 241.3750, -50.4370>>	BREAK
						CASE 117	vSpawnCoords = <<1097.7784, 245.9935, -50.4170>>	BREAK
						CASE 118	vSpawnCoords = <<1098.5804, 245.6090, -50.4170>>	BREAK
						CASE 119	vSpawnCoords = <<1097.7665, 245.1425, -50.4370>>	BREAK
						// Special peds (5)
						CASE 120	vSpawnCoords = <<1117.4315, 219.9995, -49.6525>>	BREAK
						CASE 121	vSpawnCoords = <<1094.5100, 197.5900, -50.5160>>	BREAK
						CASE 122	vSpawnCoords = <<1088.9800, 221.4940, -49.5940>>	BREAK
						CASE 123	vSpawnCoords = <<1084.2850, 215.3040, -49.2240>>	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_TABLE_GAMES
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Need to include Sports Betting area here for culling from the Main Floor
						// Sports Betting
						CASE 39		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)	BREAK
						CASE 40		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)	BREAK
						CASE 41		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)	BREAK
						CASE 42		vSpawnCoords = <<1105.7216, 250.8935, -50.3970>>	BREAK
						CASE 43		vSpawnCoords = <<1105.6486, 251.8110, -50.4370>>	BREAK
						CASE 44		vSpawnCoords = <<1104.9175, 251.3800, -50.4370>>	BREAK
						CASE 45		vSpawnCoords = <<1107.4976, 255.4655, -50.4370>>	BREAK
						CASE 46		vSpawnCoords = <<1111.5295, 260.4365, -50.4370>>	BREAK
						CASE 47		vSpawnCoords = <<1111.4775, 259.5290, -50.4370>>	BREAK
						CASE 48		vSpawnCoords = <<1099.0005, 254.1600, -51.2130>>	BREAK
						CASE 49		vSpawnCoords = <<1102.8514, 257.0135, -51.2430>>	BREAK
						CASE 50		vSpawnCoords = <<1104.4554, 256.9015, -50.8430>>	BREAK
						CASE 51		vSpawnCoords = <<1104.1324, 256.1140, -50.8430>>	BREAK
						CASE 52		vSpawnCoords = <<1108.6735, 249.2515, -50.4370>>	BREAK
						CASE 53		vSpawnCoords = <<1109.6685, 249.5380, -50.4370>>	BREAK
						// Lounge
//						CASE 54		vSpawnCoords = <<1130.8879, 239.5530, -50.9600>>	BREAK
//						CASE 55		vSpawnCoords = <<1131.5010, 240.0800, -50.9600>>	BREAK
//						CASE 56		vSpawnCoords = <<1129.9750, 244.0500, -50.9600>>	BREAK
//						CASE 57		vSpawnCoords = <<1130.5470, 243.5900, -50.9600>>	BREAK
						CASE 58		vSpawnCoords = <<1132.6805, 241.9000, -50.0500>>	BREAK
						CASE 59		vSpawnCoords = <<1131.8986, 242.1980, -50.0500>>	BREAK
						CASE 60		vSpawnCoords = <<1131.9995, 241.3975, -50.0500>>	BREAK
						CASE 61		vSpawnCoords = <<1123.9415, 250.1050, -50.0000>>	BREAK
//						CASE 62		vSpawnCoords = <<1122.6130, 251.0530, -50.9600>>	BREAK
						CASE 63		vSpawnCoords = <<1124.4294, 250.8060, -50.0500>>	BREAK
//						CASE 64		vSpawnCoords = <<1122.6720, 251.9360, -50.9600>>	BREAK
						CASE 65		vSpawnCoords = <<1121.7205, 247.6985, -50.5400>>	BREAK
						CASE 66		vSpawnCoords = <<1122.5435, 248.4910, -50.0500>>	BREAK
						// Table Games
						// Dealers
						CASE 67	vSpawnCoords = GET_BLACKJACK_DEALER_COORDS(g_iPedReservedBlackjackTable)					BREAK
						CASE 68	vSpawnCoords = GET_THREE_CARD_POKER_DEALER_COORDS(g_iPedReservedThreeCardPokerTable)		BREAK
						CASE 69 vSpawnCoords = GET_ROULETTE_DEALER_COORDS(g_iPedReservedRouletteTable)						BREAK
						
//						CASE 70		vSpawnCoords = <<1114.6215, 253.5430, -46.3945>>			BREAK
//						CASE 71		vSpawnCoords = <<1110.9835, 242.0060, -46.3645>>			BREAK
						CASE 72		vSpawnCoords = GET_BLACKJACK_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 73		vSpawnCoords = GET_BLACKJACK_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 74		vSpawnCoords = GET_BLACKJACK_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 75		vSpawnCoords = GET_BLACKJACK_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 76		vSpawnCoords = GET_TABLE_SPECTATOR_COORDS(iPedID, eActivity)	BREAK
						CASE 77		vSpawnCoords = <<1137.0334, 260.3040, -51.4370>>	BREAK
						CASE 78		vSpawnCoords = GET_ROULETTE_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 79		vSpawnCoords = GET_ROULETTE_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 80		vSpawnCoords = GET_ROULETTE_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 81		vSpawnCoords = GET_ROULETTE_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 82		vSpawnCoords = <<1151.1475, 258.0975, -51.3970>>	BREAK
						CASE 83		vSpawnCoords = <<1150.5715, 257.7925, -51.4370>>	BREAK
						CASE 84		vSpawnCoords = GET_THREE_CARD_POKER_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)	BREAK
						CASE 85		vSpawnCoords = GET_THREE_CARD_POKER_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)	BREAK
						CASE 86		vSpawnCoords = GET_THREE_CARD_POKER_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)	BREAK
						CASE 87		vSpawnCoords = GET_THREE_CARD_POKER_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)	BREAK
						CASE 88		vSpawnCoords = GET_TABLE_SPECTATOR_COORDS(iPedID, eActivity)	BREAK
						CASE 89		vSpawnCoords = GET_TABLE_SPECTATOR_COORDS(iPedID, eActivity)	BREAK
						CASE 90		vSpawnCoords = <<1142.5455, 254.9255, -51.4370>>	BREAK
						CASE 91		vSpawnCoords = <<1141.8354, 254.5215, -51.4370>>	BREAK
						CASE 92		vSpawnCoords = <<1141.8555, 255.2525, -51.4370>>	BREAK
						CASE 93		vSpawnCoords = <<1140.5645, 261.3650, -51.3970>>	BREAK
						CASE 94		vSpawnCoords = <<1150.5660, 254.6370, -51.3970>>	BREAK
						CASE 95		vSpawnCoords = <<1149.8960, 254.9820, -51.4370>>	BREAK
						CASE 96		vSpawnCoords = <<1136.3180, 260.1810, -51.3970>>	BREAK
						CASE 97		vSpawnCoords = <<1136.7695, 259.5200, -51.4370>>	BREAK
						CASE 98		vSpawnCoords = <<1139.2515, 267.7785, -51.4370>>	BREAK
						CASE 99		vSpawnCoords = <<1139.3475, 268.6940, -51.3970>>	BREAK
						CASE 100	vSpawnCoords = <<1140.0045, 268.1645, -51.3970>>	BREAK
						CASE 101	vSpawnCoords = <<1148.6895, 255.4060, -51.3970>>	BREAK
						CASE 102	vSpawnCoords = <<1148.0765, 255.9365, -51.4370>>	BREAK
						CASE 103	vSpawnCoords = <<1143.6949, 257.9110, -51.3970>>	BREAK
						CASE 104	vSpawnCoords = <<1144.4720, 257.9370, -51.3970>>	BREAK
						CASE 105	vSpawnCoords = <<1139.7825, 261.2410, -51.4370>>	BREAK
						CASE 106	vSpawnCoords = <<1154.0455, 264.2510, -51.8480>>	BREAK
						CASE 107	vSpawnCoords = <<1153.7845, 263.3367, -51.8480>>	BREAK
						CASE 108	vSpawnCoords = <<1140.3605, 256.9013, -51.3770>>	BREAK
						CASE 109	vSpawnCoords = <<1141.1385, 257.0070, -51.4370>>	BREAK
						CASE 110	vSpawnCoords = <<1145.2375, 271.7720, -51.8080>>	BREAK
						CASE 111	vSpawnCoords = <<1145.9745, 271.9293, -51.8080>>	BREAK
						CASE 112	vSpawnCoords = <<1148.3575, 266.7345, -51.8480>>	BREAK
						CASE 113	vSpawnCoords = <<1147.5095, 266.5113, -51.8480>>	BREAK
						CASE 114	vSpawnCoords = <<1148.1105, 265.8579, -51.8080>>	BREAK
						CASE 121	vSpawnCoords = <<1133.1755, 276.8320, -51.050>>		BREAK
						CASE 123	vSpawnCoords = <<1130.2305, 277.6775, -51.027>>		BREAK
						// Bouncer
						CASE 124	vSpawnCoords = <<1122.8245, 263.0550, -51.0550>>		BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Need to include Sports Betting area here for culling from the Main Floor
						// Sports Betting
						CASE 39		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)		BREAK
						CASE 40		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)		BREAK
						CASE 41		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)		BREAK
						CASE 42		vSpawnCoords = <<1107.6995, 254.5535, -50.3970>>	BREAK
						CASE 43		vSpawnCoords = <<1112.3774, 259.7500, -50.3970>>	BREAK
						CASE 44		vSpawnCoords = <<1101.6696, 249.4925, -50.3970>>	BREAK
						CASE 45		vSpawnCoords = <<1108.5544, 254.6330, -50.4370>>	BREAK
						CASE 46		vSpawnCoords = <<1111.4955, 260.3865, -50.4370>>	BREAK
						CASE 47		vSpawnCoords = <<1111.3065, 259.5290, -50.4370>>	BREAK
						CASE 48		vSpawnCoords = <<1098.2225, 253.7425, -51.2130>>	BREAK
						CASE 49		vSpawnCoords = <<1102.6515, 258.2035, -51.2430>>	BREAK
						CASE 50		vSpawnCoords = <<1105.7775, 249.1115, -50.4205>>	BREAK
						CASE 51		vSpawnCoords = <<1105.2996, 249.9440, -50.3955>>	BREAK
						CASE 52		vSpawnCoords = <<1104.8094, 249.2515, -50.4370>>	BREAK
						CASE 53		vSpawnCoords = <<1110.7075, 249.7605, -50.4095>>	BREAK
						// Lounge
//						CASE 54		vSpawnCoords = <<1130.8879, 239.5530, -50.9600>>			BREAK
//						CASE 55		vSpawnCoords = <<1131.5010, 240.0800, -50.9600>>			BREAK
//						CASE 56		vSpawnCoords = <<1129.9750, 244.0500, -50.9600>>			BREAK
//						CASE 57		vSpawnCoords = <<1130.5470, 243.5900, -50.9600>>			BREAK
						CASE 58		vSpawnCoords = <<1130.2371, 239.3400, -50.5525>>			BREAK
						CASE 59		vSpawnCoords = <<1132.3555, 242.6180, -50.0500>>			BREAK
						CASE 60		vSpawnCoords = <<1131.9155, 241.9450, -50.0000>>			BREAK
						CASE 61		vSpawnCoords = <<1123.1324, 249.6175, -50.0000>>			BREAK
//						CASE 62		vSpawnCoords = <<1122.6130, 251.0530, -50.9600>>			BREAK
						CASE 63		vSpawnCoords = <<1123.6075, 250.4735, -50.0500>>			BREAK
//						CASE 64		vSpawnCoords = <<1122.6720, 251.9360, -50.9600>>			BREAK
						CASE 65		vSpawnCoords = <<1122.6436, 250.2535, -50.0200>>			BREAK
						CASE 66		vSpawnCoords = <<1122.5435, 248.4910, -50.0500>>			BREAK
						// Table Games
						// Dealers
						CASE 67	vSpawnCoords = GET_BLACKJACK_DEALER_COORDS(g_iPedReservedBlackjackTable)					BREAK
						CASE 68	vSpawnCoords = GET_THREE_CARD_POKER_DEALER_COORDS(g_iPedReservedThreeCardPokerTable)		BREAK
						CASE 69 vSpawnCoords = GET_ROULETTE_DEALER_COORDS(g_iPedReservedRouletteTable)						BREAK
						
//						CASE 67		vSpawnCoords = <<1122.8405, 263.2595, -51.1170>>			BREAK
//						CASE 68		vSpawnCoords = <<1122.7686, 265.8750, -51.1170>>			BREAK
//						CASE 69		vSpawnCoords = <<1145.8165, 257.6440, -51.5170>>			BREAK
//						CASE 70		vSpawnCoords = <<1114.6215, 253.5430, -46.3945>>			BREAK
//						CASE 71		vSpawnCoords = <<1110.9835, 242.0060, -46.3645>>			BREAK
						CASE 72		vSpawnCoords = GET_BLACKJACK_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)			BREAK
						CASE 73		vSpawnCoords = GET_BLACKJACK_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)			BREAK
						CASE 74		vSpawnCoords = GET_BLACKJACK_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)			BREAK
						CASE 75		vSpawnCoords = GET_BLACKJACK_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)			BREAK
						CASE 76		vSpawnCoords = GET_TABLE_SPECTATOR_COORDS(iPedID, eActivity)			BREAK
						CASE 77		vSpawnCoords = <<1136.8774, 259.3890, -51.4370>>			BREAK
						CASE 78		vSpawnCoords = GET_ROULETTE_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 79		vSpawnCoords = GET_ROULETTE_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 80		vSpawnCoords = GET_ROULETTE_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 81		vSpawnCoords = GET_ROULETTE_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)		BREAK
						CASE 82		vSpawnCoords = <<1155.3774, 248.2700, -51.0320>>			BREAK
						CASE 83		vSpawnCoords = <<1151.1965, 258.9200, -51.6295>>			BREAK
						CASE 84		vSpawnCoords = GET_THREE_CARD_POKER_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)	BREAK
						CASE 85		vSpawnCoords = GET_THREE_CARD_POKER_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)	BREAK
						CASE 86		vSpawnCoords = GET_THREE_CARD_POKER_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)	BREAK
						CASE 87		vSpawnCoords = GET_THREE_CARD_POKER_PED_COORDS(iPedID, iLayout, eCasinoArea, ePedType)	BREAK
						CASE 88		vSpawnCoords = GET_TABLE_SPECTATOR_COORDS(iPedID, eActivity)			BREAK
						CASE 89		vSpawnCoords = GET_TABLE_SPECTATOR_COORDS(iPedID, eActivity)			BREAK
						CASE 90		vSpawnCoords = <<1142.7784, 255.3680, -51.4370>>			BREAK
						CASE 91		vSpawnCoords = <<1142.0925, 254.7840, -51.4370>>			BREAK
						CASE 92		vSpawnCoords = <<1140.9604, 257.7150, -51.4370>>			BREAK
						CASE 93		vSpawnCoords = <<1139.8735, 260.8600, -51.3970>>			BREAK
						CASE 94		vSpawnCoords = <<1152.0685, 255.0620, -51.3970>>			BREAK
						CASE 95		vSpawnCoords = <<1152.0665, 255.7370, -51.4370>>			BREAK
						CASE 96		vSpawnCoords = <<1135.9755, 259.5385, -51.3470>>			BREAK
						CASE 97		vSpawnCoords = <<1139.0985, 261.0875, -51.4370>>			BREAK
						CASE 98		vSpawnCoords = <<1138.0845, 270.7735, -51.4370>>			BREAK
						CASE 99		vSpawnCoords = <<1139.7426, 266.2540, -51.3970>>			BREAK
						CASE 100	vSpawnCoords = <<1139.8635, 267.1145, -51.3970>>			BREAK
						CASE 101	vSpawnCoords = <<1166.6200, 249.2050, -51.0000>>			BREAK
						CASE 102	vSpawnCoords = <<1158.0525, 251.0390, -51.0420>>			BREAK
						CASE 103	vSpawnCoords = <<1144.3936, 257.6585, -51.3970>>			BREAK
						CASE 104	vSpawnCoords = <<1143.6005, 258.0770, -51.4000>>			BREAK
						CASE 105	vSpawnCoords = <<1139.3405, 260.1435, -51.4370>>			BREAK
						CASE 106	vSpawnCoords = <<1147.4415, 264.4110, -51.8480>>			BREAK
						CASE 107	vSpawnCoords = <<1153.7896, 262.9042, -51.8480>>			BREAK
						CASE 108	vSpawnCoords = <<1140.5645, 256.8413, -51.3770>>			BREAK
						CASE 109	vSpawnCoords = <<1141.4625, 256.8945, -51.4370>>			BREAK
						CASE 110	vSpawnCoords = <<1144.8965, 271.7720, -51.8080>>			BREAK
						CASE 111	vSpawnCoords = <<1145.7775, 271.9618, -51.8080>>			BREAK
						CASE 112	vSpawnCoords = <<1147.4915, 266.7345, -51.8280>>			BREAK
						CASE 113	vSpawnCoords = <<1146.6345, 266.5113, -51.7680>>			BREAK
						CASE 114	vSpawnCoords = <<1147.6755, 263.6429, -51.8080>>			BREAK
						CASE 121	vSpawnCoords = <<1133.1755, 275.9570, -51.050>>				BREAK
						CASE 123	vSpawnCoords = <<1133.1365, 279.6350, -51.027>>				BREAK
						// Bouncer
						CASE 124	vSpawnCoords = <<1122.8245, 263.0550, -51.0550>>			BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPedID
						CASE 39		vSpawnCoords = GET_THREE_CARD_POKER_DEALER_COORDS(2)	BREAK
						CASE 40		vSpawnCoords = GET_ROULETTE_DEALER_COORDS(2)		BREAK
						CASE 41		vSpawnCoords = GET_BLACKJACK_DEALER_COORDS(2)		BREAK
						CASE 42		vSpawnCoords = GET_BLACKJACK_CHAIR_COORDS(2, 0, ePedType)		BREAK
						CASE 43		vSpawnCoords = GET_BLACKJACK_CHAIR_COORDS(2, 1, ePedType)		BREAK
						CASE 44		vSpawnCoords = GET_ROULETTE_DEALER_COORDS(3)		BREAK
						CASE 45		vSpawnCoords = GET_ROULETTE_CHAIR_COORDS(3, 0, ePedType)		BREAK
						CASE 46		vSpawnCoords = GET_ROULETTE_CHAIR_COORDS(3, 3, ePedType)		BREAK
						CASE 47		vSpawnCoords = GET_ROULETTE_CHAIR_COORDS(3, 2, ePedType)		BREAK
						CASE 48		vSpawnCoords = <<1148.0369, 272.1950, -51.803>>		BREAK
						CASE 49		vSpawnCoords = <<1148.0500, 269.8500, -51.8030>>	BREAK
						CASE 50		vSpawnCoords = <<1148.6573, 272.0535, -51.8030>>	BREAK
						CASE 51		vSpawnCoords = <<1154.4392, 265.0449, -51.8030>>	BREAK
						CASE 52		vSpawnCoords = GET_BLACKJACK_DEALER_COORDS(0)		BREAK
						CASE 53		vSpawnCoords = <<1153.6521, 265.2047, -51.8030>>	BREAK
						CASE 54		vSpawnCoords = <<1152.6388, 258.2251, -51.4370>>	BREAK
						CASE 55		vSpawnCoords = <<1152.0778, 257.5877, -51.4370>>	BREAK
						CASE 56		vSpawnCoords = <<1152.6862, 257.1098, -51.4370>>	BREAK
						CASE 57		vSpawnCoords = GET_THREE_CARD_POKER_DEALER_COORDS(3)	BREAK
						CASE 58		vSpawnCoords = GET_BLACKJACK_DEALER_COORDS(3)			BREAK
						CASE 59		vSpawnCoords = GET_ROULETTE_DEALER_COORDS(5)			BREAK
						CASE 60		vSpawnCoords = GET_THREE_CARD_POKER_CHAIR_COORDS(3, 1, ePedType) 	BREAK
						CASE 61		vSpawnCoords = GET_THREE_CARD_POKER_CHAIR_COORDS(3, 2, ePedType) 	BREAK
						CASE 62		vSpawnCoords = <<1142.1628, 250.3171, -51.0370>>		BREAK
						CASE 63		vSpawnCoords = GET_THREE_CARD_POKER_DEALER_COORDS(0)	BREAK
						CASE 64		vSpawnCoords = <<1135.9951, 259.6327, -51.4370>>	BREAK
						CASE 65		vSpawnCoords = <<1136.4047, 260.3572, -51.4370>>	BREAK
						CASE 66		vSpawnCoords = <<0.0000, 0.0000, 0.0000>>	BREAK
						CASE 67		vSpawnCoords = <<1151.8674, 270.8652, -51.8030>>	BREAK
						CASE 68		vSpawnCoords = <<1152.3495, 270.3676, -51.8030>>	BREAK
						CASE 69		vSpawnCoords = <<1141.8900, 263.7700, -51.8000>>	BREAK
						CASE 70		vSpawnCoords = <<1142.7537, 254.5275, -51.4370>>	BREAK
						CASE 71		vSpawnCoords = <<1141.8284, 255.0904, -51.4370>>	BREAK
						CASE 72		vSpawnCoords = <<1129.4998, 251.8942, -51.0270>>	BREAK
						CASE 73		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(37, ePedType)	BREAK
						CASE 74		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(41, ePedType)	BREAK
						CASE 75		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(46, ePedType)	BREAK
						CASE 76		vSpawnCoords = <<0.0000, 0.0000, 0.0000>>	BREAK
						CASE 77		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(51, ePedType)	BREAK
						CASE 78		vSpawnCoords = GET_SLOT_MACHINE_CHAIR_COORDS(52, ePedType)	BREAK
						CASE 79		vSpawnCoords = <<1142.6000, 264.3800, -51.8100>>	BREAK
						CASE 80		vSpawnCoords = <<1142.5601, 263.4400, -51.8000>>	BREAK
						CASE 81		vSpawnCoords = <<1151.8459, 259.4124, -52.1030>>	BREAK
						CASE 82		vSpawnCoords = <<1152.6121, 260.0365, -52.1030>>	BREAK
						CASE 83		vSpawnCoords = <<1130.7175, 260.2205, -51.0130>>	BREAK
						CASE 84		vSpawnCoords = <<1131.1708, 260.9451, -51.0130>>	BREAK
						CASE 85		vSpawnCoords = <<1139.5149, 254.1232, -51.0270>>	BREAK
						CASE 86		vSpawnCoords = GET_ROULETTE_CHAIR_COORDS(1, 3, ePedType)		BREAK
						CASE 87		vSpawnCoords = GET_ROULETTE_CHAIR_COORDS(1, 2, ePedType)		BREAK
						CASE 88		vSpawnCoords = GET_ROULETTE_CHAIR_COORDS(1, 1, ePedType)		BREAK
						//CASE 89		vSpawnCoords = <<1126.3160, 257.2570, -50.4350>>	BREAK
						CASE 90		vSpawnCoords = <<1116.1345, 261.2075, -50.4350>>	BREAK
						CASE 91		vSpawnCoords = <<1116.8860, 260.0966, -50.4350>>	BREAK
						CASE 92		vSpawnCoords = <<1116.0099, 260.3257, -50.4350>>	BREAK
						CASE 93		vSpawnCoords = <<1109.0923, 257.2992, -50.4350>>	BREAK
						CASE 94		vSpawnCoords = <<1110.1456, 257.2798, -50.4350>>	BREAK
						CASE 95		vSpawnCoords = GET_ROULETTE_DEALER_COORDS(0)		BREAK
						CASE 96		vSpawnCoords = GET_ROULETTE_DEALER_COORDS(1)		BREAK
						CASE 97		vSpawnCoords = GET_ROULETTE_DEALER_COORDS(4)		BREAK
						CASE 98		vSpawnCoords = GET_THREE_CARD_POKER_DEALER_COORDS(1)	BREAK
						CASE 99		vSpawnCoords = GET_BLACKJACK_DEALER_COORDS(1)		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_SPORTS_BETTING
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Sports Betting
						CASE 39		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)	BREAK
						CASE 40		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)	BREAK
						CASE 41		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)	BREAK
						CASE 42		vSpawnCoords = <<1105.7216, 250.8935, -50.3970>>	BREAK
						CASE 43		vSpawnCoords = <<1105.6486, 251.8110, -50.3970>>	BREAK
						CASE 44		vSpawnCoords = <<1104.9175, 251.3800, -50.3970>>	BREAK
						CASE 45		vSpawnCoords = <<1107.4976, 255.4655, -50.4370>>	BREAK
						CASE 46		vSpawnCoords = <<1111.5295, 260.4365, -50.4370>>	BREAK
						CASE 47		vSpawnCoords = <<1111.4775, 259.5290, -50.4370>>	BREAK
						CASE 48		vSpawnCoords = <<1099.0005, 254.1600, -51.2130>>	BREAK
						CASE 49		vSpawnCoords = <<1102.8514, 257.0135, -51.2430>>	BREAK
						CASE 50		vSpawnCoords = <<1104.4554, 256.9015, -50.8430>>	BREAK
						CASE 51		vSpawnCoords = <<1104.1324, 256.1140, -50.8430>>	BREAK
						CASE 52		vSpawnCoords = <<1108.6735, 249.2515, -50.4370>>	BREAK
						CASE 53		vSpawnCoords = <<1109.6685, 249.5380, -50.4370>>	BREAK
						// Need to include most main floor area here for culling from the Table Games
						// Behind Bar (13)
						CASE 54		vSpawnCoords = <<1119.8550, 210.2220, -49.9460>>	BREAK
						CASE 55		vSpawnCoords = <<1115.0405, 206.1520, -49.7310>>	BREAK
						CASE 56		vSpawnCoords = <<1113.1495, 211.5060, -49.7235>>	BREAK
						CASE 57		vSpawnCoords = <<1113.8254, 200.7025, -49.9415>>	BREAK
						CASE 58		vSpawnCoords = <<1113.3975, 201.9220, -49.4040>>	BREAK
						CASE 59		vSpawnCoords = <<1090.8900, 192.0700, -49.5200>>	BREAK
						CASE 60		vSpawnCoords = <<1113.2930, 214.4700, -49.4440>>	BREAK
						CASE 61		vSpawnCoords = <<1104.0455, 234.7050, -49.8400>>	BREAK
						CASE 62		vSpawnCoords = <<1105.7760, 224.4380, -49.2080>>	BREAK
						CASE 63		vSpawnCoords = <<1113.9965, 214.1345, -49.4440>>	BREAK
						CASE 64		vSpawnCoords = <<1104.4764, 235.4725, -49.8400>>	BREAK
						CASE 65		vSpawnCoords = <<1104.9875, 234.8050, -49.8400>>	BREAK
						CASE 66		vSpawnCoords = <<1119.5430, 208.7070, -49.9410>>	BREAK
						// All the rest (53)
						CASE 67		vSpawnCoords = <<1101.0031, 214.9490, -48.9860>>	BREAK
						CASE 68		vSpawnCoords = <<1105.8015, 223.1215, -48.9660>>	BREAK
						CASE 69		vSpawnCoords = <<1106.1595, 222.4235, -48.9860>>	BREAK
						CASE 70		vSpawnCoords = <<1108.3750, 218.5040, -49.4235>>	BREAK
						CASE 71		vSpawnCoords = <<1107.5485, 217.6190, -49.4440>>	BREAK
						CASE 72		vSpawnCoords = <<1107.4625, 224.9230, -49.8200>>	BREAK
						CASE 73		vSpawnCoords = <<1108.1135, 225.4100, -49.8200>>	BREAK
						CASE 74		vSpawnCoords = <<1108.2015, 224.4080, -49.8400>>	BREAK
						CASE 75		vSpawnCoords = <<1113.8585, 225.1640, -49.8400>>	BREAK
						CASE 76		vSpawnCoords = <<1113.3055, 224.4100, -49.8400>>	BREAK
						CASE 77		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 78		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 79		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 80		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
//						CASE 81		vSpawnCoords = <<1116.6935, 237.6115, -49.8400>>	BREAK
						CASE 82		vSpawnCoords = <<1111.6075, 219.0760, -49.4440>>	BREAK
						CASE 83		vSpawnCoords = <<1112.5680, 219.0310, -49.4440>>	BREAK
						CASE 84		vSpawnCoords = <<1112.0175, 218.4295, -49.4440>>	BREAK
						CASE 85		vSpawnCoords = <<1102.4525, 211.3055, -49.4440>>	BREAK
						CASE 86		vSpawnCoords = <<1102.2875, 210.4355, -49.4040>>	BREAK
						CASE 87		vSpawnCoords = <<1101.6980, 211.0780, -49.4040>>	BREAK
						CASE 88		vSpawnCoords = <<1103.7715, 207.8645, -49.4040>>	BREAK
						CASE 89		vSpawnCoords = <<1103.0045, 207.7775, -49.4040>>	BREAK
						CASE 90		vSpawnCoords = <<1105.5515, 210.9350, -49.4240>>	BREAK
						CASE 91		vSpawnCoords = <<1106.3730, 211.4370, -49.4340>>	BREAK
						CASE 92		vSpawnCoords = <<1109.8680, 214.1860, -49.4040>>	BREAK
						CASE 93		vSpawnCoords = <<1109.0295, 214.5840, -49.4040>>	BREAK
						CASE 94		vSpawnCoords = <<1106.0850, 210.2550, -49.4240>>	BREAK
						CASE 95		vSpawnCoords = <<1110.6906, 211.4035, -49.4435>>	BREAK
						CASE 96		vSpawnCoords = <<1109.324, 205.450, -49.7260>>		BREAK
						CASE 97		vSpawnCoords = <<1108.3575, 199.9490, -49.4040>>	BREAK
						CASE 98		vSpawnCoords = <<1103.4835, 197.9990, -49.4040>>	BREAK
						CASE 99		vSpawnCoords = <<1108.4675, 200.6430, -49.4040>>	BREAK
						CASE 100	vSpawnCoords = <<1106.1545, 202.6385, -49.4240>>	BREAK
						CASE 101	vSpawnCoords = <<1105.2305, 202.3530, -49.4240>>	BREAK
						CASE 102	vSpawnCoords = <<1105.2125, 205.4240, -49.4440>>	BREAK
						CASE 103	vSpawnCoords = <<1105.9755, 205.8145, -49.4240>>	BREAK
						CASE 104	vSpawnCoords = <<1105.7896, 204.9860, -49.4240>>	BREAK
						CASE 105	vSpawnCoords = <<1099.8810, 206.6930, -49.4240>>	BREAK
						CASE 106	vSpawnCoords = <<1100.1625, 207.4905, -49.4440>>	BREAK
						CASE 107	vSpawnCoords = <<1105.6420, 214.2970, -49.4440>>	BREAK
						CASE 108	vSpawnCoords = <<1106.2325, 213.7785, -49.4440>>	BREAK
						CASE 109	vSpawnCoords = <<1108.5795, 206.8625, -49.4485>>	BREAK
						CASE 110	vSpawnCoords = <<1101.2375, 241.8610, -50.4370>>	BREAK
						CASE 111	vSpawnCoords = <<1109.1165, 213.5960, -49.4040>>	BREAK
						CASE 112	vSpawnCoords = <<1116.0815, 212.6625, -49.4440>>	BREAK
						CASE 113	vSpawnCoords = <<1100.0790, 214.9300, -48.9860>>	BREAK
						CASE 114	vSpawnCoords = <<1108.3400, 217.4060, -49.4040>>	BREAK
						CASE 115	vSpawnCoords = <<1096.5480, 241.7300, -50.4370>>	BREAK
						CASE 116	vSpawnCoords = <<1097.3690, 241.3750, -50.4370>>	BREAK
						CASE 117	vSpawnCoords = <<1097.7784, 245.9935, -50.4170>>	BREAK
						CASE 118	vSpawnCoords = <<1098.5804, 245.6090, -50.4170>>	BREAK
						CASE 119	vSpawnCoords = <<1097.7665, 245.1425, -50.4370>>	BREAK
						// Special peds (5)
						CASE 120	vSpawnCoords = <<1117.4315, 219.9995, -49.6525>>	BREAK
						CASE 121	vSpawnCoords = <<1094.5100, 197.5900, -50.5160>>	BREAK
						CASE 122	vSpawnCoords = <<1088.940, 221.124, -49.178>>		BREAK
						CASE 123	vSpawnCoords = <<1084.2850, 215.3040, -49.2240>>	BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Sports Betting
						CASE 39		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)		BREAK
						CASE 40		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)		BREAK
						CASE 41		vSpawnCoords = GET_INSIDE_TRACK_CHAIR_COORDS(iPedID, ePedType)		BREAK
						CASE 42		vSpawnCoords = <<1107.6995, 254.5535, -50.3970>>	BREAK
						CASE 43		vSpawnCoords = <<1112.3774, 259.7500, -50.3970>>	BREAK
						CASE 44		vSpawnCoords = <<1101.6696, 249.4925, -50.3970>>	BREAK
						CASE 45		vSpawnCoords = <<1108.5544, 254.6330, -50.4370>>	BREAK
						CASE 46		vSpawnCoords = <<1111.4955, 260.3865, -50.4370>>	BREAK
						CASE 47		vSpawnCoords = <<1111.3065, 259.5290, -50.4370>>	BREAK
						CASE 48		vSpawnCoords = <<1098.2225, 253.7425, -51.2130>>	BREAK
						CASE 49		vSpawnCoords = <<1102.6515, 258.2035, -51.2430>>	BREAK
						CASE 50		vSpawnCoords = <<1105.7775, 249.1115, -50.4205>>	BREAK
						CASE 51		vSpawnCoords = <<1105.2996, 249.9440, -50.3955>>	BREAK
						CASE 52		vSpawnCoords = <<1104.8094, 249.2515, -50.4370>>	BREAK
						CASE 53		vSpawnCoords = <<1110.7075, 249.7605, -50.4095>>	BREAK
						// Need to include most main floor area here for culling from the Table Games
						// Behind Bar (13)
						CASE 54		vSpawnCoords = <<1118.2725, 203.9920, -49.9460>>	BREAK
						CASE 55		vSpawnCoords = <<1115.0405, 206.1520, -49.7310>>	BREAK
						CASE 56		vSpawnCoords = <<1113.1495, 211.5060, -49.7235>>	BREAK
						CASE 57		vSpawnCoords = <<1111.4084, 202.2675, -49.4040>>	BREAK
						CASE 58		vSpawnCoords = <<1111.0754, 201.4445, -49.4040>>	BREAK
						CASE 59		vSpawnCoords = <<1090.8900, 192.0700, -49.5200>>	BREAK
						CASE 60		vSpawnCoords = <<1113.9045, 213.5275, -49.4440>>	BREAK
						CASE 61		vSpawnCoords = <<1104.9135, 234.7050, -49.8400>>	BREAK
						CASE 62		vSpawnCoords = <<1108.3165, 227.7855, -49.8405>>	BREAK
						CASE 63		vSpawnCoords = <<1106.6155, 214.3220, -49.4440>>	BREAK
						CASE 64		vSpawnCoords = <<1105.3365, 235.4725, -49.8400>>	BREAK
						CASE 65		vSpawnCoords = <<1114.5945, 225.1575, -49.8400>>	BREAK
						CASE 66		vSpawnCoords = <<1119.5430, 208.7070, -49.9410>>	BREAK
						// All the rest (53)
						CASE 67		vSpawnCoords = <<1104.8365, 215.7115, -48.9860>>	BREAK
						CASE 68		vSpawnCoords = <<1104.5045, 222.0990, -48.9660>>	BREAK
						CASE 69		vSpawnCoords = <<1105.4565, 216.4410, -48.9860>>	BREAK
						CASE 70		vSpawnCoords = <<1108.3750, 218.5040, -49.449>>		BREAK
						CASE 71		vSpawnCoords = <<1108.4680, 217.3970, -49.421>>		BREAK
						CASE 72		vSpawnCoords = <<1108.7896, 226.9555, -49.8200>>	BREAK
						CASE 73		vSpawnCoords = <<1108.1075, 223.9700, -49.8200>>	BREAK
						CASE 74		vSpawnCoords = <<1108.7015, 223.2505, -49.8400>>	BREAK
						CASE 75		vSpawnCoords = <<1115.2355, 225.9940, -49.8400>>	BREAK
						CASE 76		vSpawnCoords = <<1114.2545, 226.1975, -49.8400>>	BREAK
						CASE 77		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 78		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 79		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 80		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
//						CASE 81		vSpawnCoords = <<1116.6935, 237.6115, -49.8400>>	BREAK
						CASE 82		vSpawnCoords = <<1112.0925, 223.1410, -49.8415>>	BREAK
						CASE 83		vSpawnCoords = <<1112.4495, 219.0310, -49.4440>>	BREAK
						CASE 84		vSpawnCoords = <<1112.1775, 218.2170, -49.4440>>	BREAK
						CASE 85		vSpawnCoords = <<1102.4525, 211.3055, -49.4440>>	BREAK
						CASE 86		vSpawnCoords = <<1102.2875, 210.4355, -49.4040>>	BREAK
						CASE 87		vSpawnCoords = <<1101.6980, 211.0780, -49.4040>>	BREAK
						CASE 88		vSpawnCoords = <<1105.2465, 207.8645, -49.4040>>	BREAK
						CASE 89		vSpawnCoords = <<1104.5895, 207.1975, -49.4040>>	BREAK
						CASE 90		vSpawnCoords = <<1105.7625, 210.6025, -49.4240>>	BREAK
						CASE 91		vSpawnCoords = <<1105.7795, 211.5770, -49.4340>>	BREAK
						CASE 92		vSpawnCoords = <<1109.3655, 213.1860, -49.4040>>	BREAK
						CASE 93		vSpawnCoords = <<1108.6844, 213.7740, -49.4040>>	BREAK
						CASE 94		vSpawnCoords = <<1104.4435, 208.0675, -49.4240>>	BREAK
						CASE 95		vSpawnCoords = <<1109.4305, 210.5910, -49.4460>>	BREAK
						CASE 96		vSpawnCoords = <<1109.324, 205.450, -49.7260>>		BREAK
						CASE 97		vSpawnCoords = <<1108.3575, 199.9490, -49.4040>>	BREAK
						CASE 98		vSpawnCoords = <<1100.3060, 199.1615, -49.4440>>	BREAK
						CASE 99		vSpawnCoords = <<1108.4675, 200.6430, -49.4040>>	BREAK
						CASE 100	vSpawnCoords = <<1106.8275, 203.8910, -49.4240>>	BREAK
						CASE 101	vSpawnCoords = <<1106.4504, 203.0380, -49.4240>>	BREAK
						CASE 102	vSpawnCoords = <<1103.0985, 204.7215, -49.4440>>	BREAK
						CASE 103	vSpawnCoords = <<1102.9926, 205.6595, -49.4240>>	BREAK
						CASE 104	vSpawnCoords = <<1105.7455, 203.9335, -49.4240>>	BREAK
						CASE 105	vSpawnCoords = <<1099.3936, 207.5955, -49.4240>>	BREAK
						CASE 106	vSpawnCoords = <<1100.3044, 208.1305, -49.4440>>	BREAK
						CASE 107	vSpawnCoords = <<1106.0045, 214.7595, -49.4440>>	BREAK
						CASE 108	vSpawnCoords = <<1105.7996, 213.8635, -49.4440>>	BREAK
						CASE 109	vSpawnCoords = <<1108.9655, 206.1975, -49.4535>>	BREAK
						CASE 110	vSpawnCoords = <<1095.2290, 246.3460, -50.5000>>	BREAK
						CASE 111	vSpawnCoords = <<1109.7125, 214.0760, -49.4040>>	BREAK
						CASE 112	vSpawnCoords = <<1114.5804, 212.9925, -49.4200>>	BREAK
						CASE 113	vSpawnCoords = <<1099.4545, 208.4350, -49.4210>>	BREAK
						CASE 114	vSpawnCoords = <<1109.296, 217.7610, -49.4040>>		BREAK
						CASE 115	vSpawnCoords = <<1097.0125, 240.8275, -50.4370>>	BREAK
						CASE 116	vSpawnCoords = <<1097.8815, 240.3825, -50.4370>>	BREAK
						CASE 117	vSpawnCoords = <<1097.7335, 241.2910, -50.4170>>	BREAK
						CASE 118	vSpawnCoords = <<1098.6746, 244.8215, -50.4170>>	BREAK
						CASE 119	vSpawnCoords = <<1099.5435, 244.5075, -50.4370>>	BREAK
						// Special peds (5)
						CASE 120	vSpawnCoords = <<1117.4315, 219.9995, -49.6525>>	BREAK
						CASE 121	vSpawnCoords = <<1094.5100, 197.5900, -50.5160>>	BREAK
						CASE 122	vSpawnCoords = <<1088.940, 221.124, -49.178>>		BREAK
						CASE 123	vSpawnCoords = <<1084.2850, 215.3040, -49.2240>>	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_MANAGERS_OFFICE
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Entrance (15)
						CASE 39		vSpawnCoords = <<1088.9565, 212.4370, -48.9660>>	BREAK
						CASE 40		vSpawnCoords = <<1086.9716, 206.0560, -48.9860>>	BREAK
						CASE 41		vSpawnCoords = <<1113.2015, 204.7450, -49.4485>>	BREAK
						CASE 42		vSpawnCoords = <<1091.5186, 217.2425, -49.1740>>	BREAK
						// Manager
						CASE 43		vSpawnCoords = GET_MANAGERS_COORDS()				BREAK
						CASE 44		vSpawnCoords = <<1094.4196, 223.0400, -48.9860>>	BREAK
						CASE 45		vSpawnCoords = <<1094.0095, 222.1700, -48.9860>>	BREAK
						CASE 46		vSpawnCoords = <<1116.8665, 213.2050, -49.4440>>	BREAK
						CASE 47		vSpawnCoords = <<1088.3475, 212.0230, -48.9860>>	BREAK
						CASE 48		vSpawnCoords = <<1084.5905, 217.9915, -49.2240>>	BREAK
						CASE 49		vSpawnCoords = <<1092.5024, 217.1670, -49.2040>>	BREAK
						CASE 50		vSpawnCoords = <<1092.0255, 216.4510, -49.2040>>	BREAK
						CASE 51		vSpawnCoords = <<1095.8625, 208.0835, -48.9860>>	BREAK
						CASE 52		vSpawnCoords = <<1096.3405, 208.6385, -48.9860>>	BREAK
						CASE 53		vSpawnCoords = <<1095.5605, 208.8155, -48.9860>>	BREAK
						// Behind Bar (13)
						CASE 54		vSpawnCoords = <<1119.8550, 210.2220, -49.9460>>	BREAK
						CASE 55		vSpawnCoords = <<1115.0405, 206.1520, -49.7310>>	BREAK
						CASE 56		vSpawnCoords = <<1113.1495, 211.5060, -49.7235>>	BREAK
						CASE 57		vSpawnCoords = <<1113.8254, 200.7025, -49.9415>>	BREAK
						CASE 58		vSpawnCoords = <<1113.3975, 201.9220, -49.4040>>	BREAK
						CASE 59		vSpawnCoords = <<1090.8900, 192.0700, -49.5200>>	BREAK
						CASE 60		vSpawnCoords = <<1113.2930, 214.4700, -49.4440>>	BREAK
						CASE 61		vSpawnCoords = <<1104.0455, 234.7050, -49.8400>>	BREAK
						CASE 62		vSpawnCoords = <<1105.7760, 224.4380, -49.2080>>	BREAK
						CASE 63		vSpawnCoords = <<1113.9965, 214.1345, -49.4440>>	BREAK
						CASE 64		vSpawnCoords = <<1104.4764, 235.4725, -49.8400>>	BREAK
						CASE 65		vSpawnCoords = <<1104.9875, 234.8050, -49.8400>>	BREAK
						CASE 66		vSpawnCoords = <<1119.5430, 208.7070, -49.9410>>	BREAK
						// All the rest (53)
						CASE 67		vSpawnCoords = <<1101.0031, 214.9490, -48.9860>>	BREAK
						CASE 68		vSpawnCoords = <<1105.8015, 223.1215, -48.9660>>	BREAK
						CASE 69		vSpawnCoords = <<1106.1595, 222.4235, -48.9860>>	BREAK
						CASE 70		vSpawnCoords = <<1108.3750, 218.5040, -49.4235>>	BREAK
						CASE 71		vSpawnCoords = <<1107.5485, 217.6190, -49.4440>>	BREAK
						CASE 72		vSpawnCoords = <<1107.4625, 224.9230, -49.8200>>	BREAK
						CASE 73		vSpawnCoords = <<1108.1135, 225.4100, -49.8200>>	BREAK
						CASE 74		vSpawnCoords = <<1108.2015, 224.4080, -49.8400>>	BREAK
						CASE 75		vSpawnCoords = <<1113.8585, 225.1640, -49.8400>>	BREAK
						CASE 76		vSpawnCoords = <<1113.3055, 224.4100, -49.8400>>	BREAK
						CASE 77		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 78		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 79		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 80		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
//						CASE 81		vSpawnCoords = <<1116.6935, 237.6115, -49.8400>>	BREAK
						CASE 82		vSpawnCoords = <<1111.6075, 219.0760, -49.4440>>	BREAK
						CASE 83		vSpawnCoords = <<1112.5680, 219.0310, -49.4440>>	BREAK
						CASE 84		vSpawnCoords = <<1112.0175, 218.4295, -49.4440>>	BREAK
						CASE 85		vSpawnCoords = <<1102.4525, 211.3055, -49.4440>>	BREAK
						CASE 86		vSpawnCoords = <<1102.2875, 210.4355, -49.4040>>	BREAK
						CASE 87		vSpawnCoords = <<1101.6980, 211.0780, -49.4040>>	BREAK
						CASE 88		vSpawnCoords = <<1103.7715, 207.8645, -49.4040>>	BREAK
						CASE 89		vSpawnCoords = <<1103.0045, 207.7775, -49.4040>>	BREAK
						CASE 90		vSpawnCoords = <<1105.5515, 210.9350, -49.4240>>	BREAK
						CASE 91		vSpawnCoords = <<1106.3730, 211.4370, -49.4340>>	BREAK
						CASE 92		vSpawnCoords = <<1109.8680, 214.1860, -49.4040>>	BREAK
						CASE 93		vSpawnCoords = <<1109.0295, 214.5840, -49.4040>>	BREAK
						CASE 94		vSpawnCoords = <<1106.0850, 210.2550, -49.4240>>	BREAK
						CASE 95		vSpawnCoords = <<1110.6906, 211.4035, -49.4435>>	BREAK
						CASE 96		vSpawnCoords = <<1109.324, 205.450, -49.7260>>		BREAK
						CASE 97		vSpawnCoords = <<1108.3575, 199.9490, -49.4040>>	BREAK
						CASE 98		vSpawnCoords = <<1103.4835, 197.9990, -49.4040>>	BREAK
						CASE 99		vSpawnCoords = <<1108.4675, 200.6430, -49.4040>>	BREAK
						CASE 100	vSpawnCoords = <<1106.1545, 202.6385, -49.4240>>	BREAK
						CASE 101	vSpawnCoords = <<1105.2305, 202.3530, -49.4240>>	BREAK
						CASE 102	vSpawnCoords = <<1105.2125, 205.4240, -49.4440>>	BREAK
						CASE 103	vSpawnCoords = <<1105.9755, 205.8145, -49.4240>>	BREAK
						CASE 104	vSpawnCoords = <<1105.7896, 204.9860, -49.4240>>	BREAK
						CASE 105	vSpawnCoords = <<1099.8810, 206.6930, -49.4240>>	BREAK
						CASE 106	vSpawnCoords = <<1100.1625, 207.4905, -49.4440>>	BREAK
						CASE 107	vSpawnCoords = <<1105.6420, 214.2970, -49.4440>>	BREAK
						CASE 108	vSpawnCoords = <<1106.2325, 213.7785, -49.4440>>	BREAK
						CASE 109	vSpawnCoords = <<1108.5795, 206.8625, -49.4485>>	BREAK
						CASE 110	vSpawnCoords = <<1101.2375, 241.8610, -50.4370>>	BREAK
						CASE 111	vSpawnCoords = <<1109.1165, 213.5960, -49.4040>>	BREAK
						CASE 112	vSpawnCoords = <<1116.0815, 212.6625, -49.4440>>	BREAK
						CASE 113	vSpawnCoords = <<1100.0790, 214.9300, -48.9860>>	BREAK
						CASE 114	vSpawnCoords = <<1108.3400, 217.4060, -49.4040>>	BREAK
						CASE 115	vSpawnCoords = <<1096.5480, 241.7300, -50.4370>>	BREAK
						CASE 116	vSpawnCoords = <<1097.3690, 241.3750, -50.4370>>	BREAK
						CASE 117	vSpawnCoords = <<1097.7784, 245.9935, -50.4170>>	BREAK
						CASE 118	vSpawnCoords = <<1098.5804, 245.6090, -50.4170>>	BREAK
						CASE 119	vSpawnCoords = <<1097.7665, 245.1425, -50.4370>>	BREAK
						// Special peds (5)
						CASE 120	vSpawnCoords = <<1117.4315, 219.9995, -49.6525>>	BREAK
						CASE 121	vSpawnCoords = <<1094.5100, 197.5900, -50.5160>>	BREAK
						CASE 122	vSpawnCoords = <<1088.940, 221.124, -49.178>>		BREAK
						CASE 123	vSpawnCoords = <<1084.2850, 215.3040, -49.2240>>	BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Entrance (15)
						CASE 39		vSpawnCoords = <<1089.5535, 212.9495, -48.9860>>	BREAK
						CASE 40		vSpawnCoords = <<1092.7775, 206.8735, -48.9860>>	BREAK
						CASE 41		vSpawnCoords = <<1113.8685, 205.0000, -49.4460>>	BREAK
						CASE 42		vSpawnCoords = <<1085.5275, 218.6250, -49.2040>>	BREAK
						// Manager
						CASE 43		vSpawnCoords = GET_MANAGERS_COORDS()				BREAK
						CASE 44		vSpawnCoords = <<1098.0496, 225.7375, -48.9860>>	BREAK
						CASE 45		vSpawnCoords = <<1097.1235, 225.3325, -48.9860>>	BREAK
						CASE 46		vSpawnCoords = <<1114.6705, 214.0225, -49.4440>>	BREAK
						CASE 47		vSpawnCoords = <<1088.7325, 212.2180, -48.9860>>	BREAK
						CASE 48		vSpawnCoords = <<1085.0804, 217.7390, -49.2040>>	BREAK
						CASE 49		vSpawnCoords = <<1092.5024, 217.1670, -49.2040>>	BREAK
						CASE 50		vSpawnCoords = <<1089.9135, 212.1360, -48.9640>>	BREAK
						CASE 51		vSpawnCoords = <<1095.5916, 208.0585, -48.9860>>	BREAK
						CASE 52		vSpawnCoords = <<1104.9905, 199.5510, -49.4335>>	BREAK
						CASE 53		vSpawnCoords = <<1094.6635, 208.2830, -48.9860>>	BREAK
						// Behind Bar (13)
						CASE 54		vSpawnCoords = <<1118.2725, 203.9920, -49.9460>>	BREAK
						CASE 55		vSpawnCoords = <<1115.0405, 206.1520, -49.7310>>	BREAK
						CASE 56		vSpawnCoords = <<1113.1495, 211.5060, -49.7235>>	BREAK
						CASE 57		vSpawnCoords = <<1111.4084, 202.2675, -49.4040>>	BREAK
						CASE 58		vSpawnCoords = <<1111.0754, 201.4445, -49.4040>>	BREAK
						CASE 59		vSpawnCoords = <<1090.8900, 192.0700, -49.5200>>	BREAK
						CASE 60		vSpawnCoords = <<1113.9045, 213.5275, -49.4440>>	BREAK
						CASE 61		vSpawnCoords = <<1104.9135, 234.7050, -49.8400>>	BREAK
						CASE 62		vSpawnCoords = <<1108.3165, 227.7855, -49.8405>>	BREAK
						CASE 63		vSpawnCoords = <<1106.6155, 214.3220, -49.4440>>	BREAK
						CASE 64		vSpawnCoords = <<1105.3365, 235.4725, -49.8400>>	BREAK
						CASE 65		vSpawnCoords = <<1114.5945, 225.1575, -49.8400>>	BREAK
						CASE 66		vSpawnCoords = <<1119.5430, 208.7070, -49.9410>>	BREAK
						// All the rest (53)
						CASE 67		vSpawnCoords = <<1104.8365, 215.7115, -48.9860>>	BREAK
						CASE 68		vSpawnCoords = <<1104.5045, 222.0990, -48.9660>>	BREAK
						CASE 69		vSpawnCoords = <<1105.4565, 216.4410, -48.9860>>	BREAK
						CASE 70		vSpawnCoords = <<1108.3750, 218.5040, -49.449>>		BREAK
						CASE 71		vSpawnCoords = <<1108.4680, 217.3970, -49.421>>		BREAK
						CASE 72		vSpawnCoords = <<1108.7896, 226.9555, -49.8200>>	BREAK
						CASE 73		vSpawnCoords = <<1108.1075, 223.9700, -49.8200>>	BREAK
						CASE 74		vSpawnCoords = <<1108.7015, 223.2505, -49.8400>>	BREAK
						CASE 75		vSpawnCoords = <<1115.2355, 225.9940, -49.8400>>	BREAK
						CASE 76		vSpawnCoords = <<1114.2545, 226.1975, -49.8400>>	BREAK
						CASE 77		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 78		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 79		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
						CASE 80		vSpawnCoords = GET_SLOT_MACHINE_PED_COORDS(iPedID, ePedType)	BREAK
//						CASE 81		vSpawnCoords = <<1116.6935, 237.6115, -49.8400>>	BREAK
						CASE 82		vSpawnCoords = <<1112.0925, 223.1410, -49.8415>>	BREAK
						CASE 83		vSpawnCoords = <<1112.4495, 219.0310, -49.4440>>	BREAK
						CASE 84		vSpawnCoords = <<1112.1775, 218.2170, -49.4440>>	BREAK
						CASE 85		vSpawnCoords = <<1102.4525, 211.3055, -49.4440>>	BREAK
						CASE 86		vSpawnCoords = <<1102.2875, 210.4355, -49.4040>>	BREAK
						CASE 87		vSpawnCoords = <<1101.6980, 211.0780, -49.4040>>	BREAK
						CASE 88		vSpawnCoords = <<1105.2465, 207.8645, -49.4040>>	BREAK
						CASE 89		vSpawnCoords = <<1104.5895, 207.1975, -49.4040>>	BREAK
						CASE 90		vSpawnCoords = <<1105.7625, 210.6025, -49.4240>>	BREAK
						CASE 91		vSpawnCoords = <<1105.7795, 211.5770, -49.4340>>	BREAK
						CASE 92		vSpawnCoords = <<1109.3655, 213.1860, -49.4040>>	BREAK
						CASE 93		vSpawnCoords = <<1108.6844, 213.7740, -49.4040>>	BREAK
						CASE 94		vSpawnCoords = <<1104.4435, 208.0675, -49.4240>>	BREAK
						CASE 95		vSpawnCoords = <<1109.4305, 210.5910, -49.4460>>	BREAK
						CASE 96		vSpawnCoords = <<1109.324, 205.450, -49.7260>>		BREAK
						CASE 97		vSpawnCoords = <<1108.3575, 199.9490, -49.4040>>	BREAK
						CASE 98		vSpawnCoords = <<1100.3060, 199.1615, -49.4440>>	BREAK
						CASE 99		vSpawnCoords = <<1108.4675, 200.6430, -49.4040>>	BREAK
						CASE 100	vSpawnCoords = <<1106.8275, 203.8910, -49.4240>>	BREAK
						CASE 101	vSpawnCoords = <<1106.4504, 203.0380, -49.4240>>	BREAK
						CASE 102	vSpawnCoords = <<1103.0985, 204.7215, -49.4440>>	BREAK
						CASE 103	vSpawnCoords = <<1102.9926, 205.6595, -49.4240>>	BREAK
						CASE 104	vSpawnCoords = <<1105.7455, 203.9335, -49.4240>>	BREAK
						CASE 105	vSpawnCoords = <<1099.3936, 207.5955, -49.4240>>	BREAK
						CASE 106	vSpawnCoords = <<1100.3044, 208.1305, -49.4440>>	BREAK
						CASE 107	vSpawnCoords = <<1106.0045, 214.7595, -49.4440>>	BREAK
						CASE 108	vSpawnCoords = <<1105.7996, 213.8635, -49.4440>>	BREAK
						CASE 109	vSpawnCoords = <<1108.9655, 206.1975, -49.4535>>	BREAK
						CASE 110	vSpawnCoords = <<1095.2290, 246.3460, -50.5000>>	BREAK
						CASE 111	vSpawnCoords = <<1109.7125, 214.0760, -49.4040>>	BREAK
						CASE 112	vSpawnCoords = <<1114.5804, 212.9925, -49.4200>>	BREAK
						CASE 113	vSpawnCoords = <<1099.4545, 208.4350, -49.4210>>	BREAK
						CASE 114	vSpawnCoords = <<1109.296, 217.7610, -49.4040>>		BREAK
						CASE 115	vSpawnCoords = <<1097.0125, 240.8275, -50.4370>>	BREAK
						CASE 116	vSpawnCoords = <<1097.8815, 240.3825, -50.4370>>	BREAK
						CASE 117	vSpawnCoords = <<1097.7335, 241.2910, -50.4170>>	BREAK
						CASE 118	vSpawnCoords = <<1098.6746, 244.8215, -50.4170>>	BREAK
						CASE 119	vSpawnCoords = <<1099.5435, 244.5075, -50.4370>>	BREAK
						// Special peds (5)
						CASE 120	vSpawnCoords = <<1117.4315, 219.9995, -49.6525>>	BREAK
						CASE 121	vSpawnCoords = <<1094.5100, 197.5900, -50.5160>>	BREAK
						CASE 122	vSpawnCoords = <<1088.940, 221.124, -49.178>>		BREAK
						CASE 123	vSpawnCoords = <<1084.2850, 215.3040, -49.2240>>	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugPrints
		PRINTLN("[CASINO_PEDS] GET_CASINO_PED_POSITION - Ped Coords: ", vSpawnCoords)
	ENDIF
	#ENDIF
	
	RETURN vSpawnCoords
ENDFUNC

FUNC FLOAT GET_BLACKJACK_TABLE_HEADING(INT iTableID)
	SWITCH iTableID
		CASE 0	RETURN -134.69
		CASE 1	RETURN 45.31
		CASE 2	RETURN 135.31
		CASE 3	RETURN 135.31
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_BLACKJACK_CHAIR_HEADING(INT iTable, INT iSeat)
	FLOAT fHeading = 0.0
	SWITCH iTable
		CASE 0
			SWITCH iSeat
				CASE 0	fHeading = -204.690643	BREAK
				CASE 1	fHeading = -154.690628	BREAK
				CASE 2	fHeading = -114.690666	BREAK
				CASE 3	fHeading = -64.690651	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iSeat
				CASE 0	fHeading = -24.690651	BREAK
				CASE 1	fHeading = 25.309364	BREAK
				CASE 2	fHeading = 65.309326	BREAK
				CASE 3	fHeading = -244.690628	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iSeat
				CASE 0	fHeading = 65.309433	BREAK
				CASE 1	fHeading = -244.690521	BREAK
				CASE 2	fHeading = -204.690552	BREAK
				CASE 3	fHeading = -154.690552	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iSeat
				CASE 0	fHeading = 65.309433	BREAK
				CASE 1	fHeading = -244.690521	BREAK
				CASE 2	fHeading = -204.690552	BREAK
				CASE 3	fHeading = -154.690552	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN fHeading
ENDFUNC

FUNC FLOAT GET_BLACKJACK_PED_HEADING(INT iPedID)
	FLOAT fHeading = 0.0
	SWITCH g_iPedReservedBlackjackTable
		CASE 0
			SWITCH iPedID
				CASE 72	fHeading = -204.690643	BREAK
				CASE 73	fHeading = -154.690628	BREAK
				CASE 74	fHeading = -114.690666	BREAK
				CASE 75	fHeading = -64.690651	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iPedID
				CASE 72	fHeading = -24.690651	BREAK
				CASE 73	fHeading = 25.309364	BREAK
				CASE 74	fHeading = 65.309326	BREAK
				CASE 75	fHeading = -244.690628	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iPedID
				CASE 72	fHeading = 65.309433	BREAK
				CASE 73	fHeading = -244.690521	BREAK
				CASE 74	fHeading = -204.690552	BREAK
				CASE 75	fHeading = -154.690552	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iPedID
				CASE 72	fHeading = 65.309433	BREAK
				CASE 73	fHeading = -244.690521	BREAK
				CASE 74	fHeading = -204.690552	BREAK
				CASE 75	fHeading = -154.690552	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN fHeading
ENDFUNC

FUNC FLOAT GET_THREE_CARD_POKER_TABLE_HEADING(INT iTableID)
	SWITCH iTableID
		CASE 0	RETURN -135.0
		CASE 1	RETURN 45.0
		CASE 2	RETURN -45.0
		CASE 3	RETURN -45.0
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_THREE_CARD_POKER_CHAIR_HEADING(INT iTable, INT iSeat)
	FLOAT fHeading = 0.0
	SWITCH iTable
		CASE 0
			SWITCH iSeat
				CASE 0	fHeading = -204.999939	BREAK
				CASE 1	fHeading = -154.999939	BREAK
				CASE 2	fHeading = -114.999962	BREAK
				CASE 3	fHeading = -64.999954	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iSeat
				CASE 0	fHeading = -25.000061	BREAK
				CASE 1	fHeading = 24.999954	BREAK
				CASE 2	fHeading = 64.999924	BREAK
				CASE 3	fHeading = -245.000046	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iSeat
				CASE 0	fHeading = -114.999931	BREAK
				CASE 1	fHeading = -64.999908	BREAK
				CASE 2	fHeading = -24.999947	BREAK
				CASE 3	fHeading = 25.000069 	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iSeat
				CASE 0	fHeading = -114.999931	BREAK
				CASE 1	fHeading = -64.999908	BREAK
				CASE 2	fHeading = -24.999947	BREAK
				CASE 3	fHeading = 25.000069 	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN fHeading
ENDFUNC

FUNC FLOAT GET_THREE_CARD_POKER_PED_HEADING(INT iPedID)
	FLOAT fHeading = 0.0
	SWITCH g_iPedReservedThreeCardPokerTable
		CASE 0
			SWITCH iPedID
				CASE 84	fHeading = -204.999939	BREAK
				CASE 85	fHeading = -154.999939	BREAK
				CASE 86	fHeading = -114.999962	BREAK
				CASE 87	fHeading = -64.999954	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iPedID
				CASE 84	fHeading = -25.000061	BREAK
				CASE 85	fHeading = 24.999954	BREAK
				CASE 86	fHeading = 64.999924	BREAK
				CASE 87	fHeading = -245.000046	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iPedID
				CASE 84	fHeading = -114.999931	BREAK
				CASE 85	fHeading = -64.999908	BREAK
				CASE 86	fHeading = -24.999947	BREAK
				CASE 87	fHeading = 25.000069 	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iPedID
				CASE 84	fHeading = -114.999931	BREAK
				CASE 85	fHeading = -64.999908	BREAK
				CASE 86	fHeading = -24.999947	BREAK
				CASE 87	fHeading = 25.000069 	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN fHeading
ENDFUNC

FUNC FLOAT GET_ROULETTE_TABLE_HEADING(INT iTable)
	SWITCH iTable
		CASE 0	RETURN -135.0
		CASE 1	RETURN 45.0
		CASE 2	RETURN 135.0
		CASE 3	RETURN -45.0
		CASE 4	RETURN -45.0
		CASE 5	RETURN 135.0
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_ROULETTE_CHAIR_HEADING(INT iTable, INT iChair)
	FLOAT fHeading = 0.0
	SWITCH iTable
		CASE 0
			SWITCH iChair
				CASE 0	fHeading = -135.000092	BREAK
				CASE 1	fHeading = -135.000092	BREAK
				CASE 2	fHeading = -45.000103	BREAK
				CASE 3	fHeading = 44.999939	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iChair
				CASE 0	fHeading = 44.999908	BREAK
				CASE 1	fHeading = 44.999908	BREAK
				CASE 2	fHeading = -225.000092	BREAK
				CASE 3	fHeading = -135.000031	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iChair
				CASE 0	fHeading = -224.999954	BREAK
				CASE 1	fHeading = -134.999969	BREAK
				CASE 2	fHeading = -44.999928	BREAK
				CASE 3	fHeading = -224.999954	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iChair
				CASE 0	fHeading = -44.999962	BREAK
				CASE 1	fHeading = 45.000015	BREAK
				CASE 2	fHeading = -224.999908	BREAK
				CASE 3	fHeading = -44.999962 	BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH iChair
				CASE 0	fHeading = -44.999962	BREAK
				CASE 1	fHeading = 45.000015	BREAK
				CASE 2	fHeading = -224.999908	BREAK
				CASE 3	fHeading = -44.999962 	BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH iChair
				CASE 0	fHeading = -224.999954	BREAK
				CASE 1	fHeading = -134.999969	BREAK
				CASE 2	fHeading = -44.999928	BREAK
				CASE 3	fHeading = -224.999954	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN fHeading
ENDFUNC

FUNC FLOAT GET_ROULETTE_PED_HEADING(INT iPedID)
	FLOAT fHeading = 0.0
	SWITCH g_iPedReservedRouletteTable
		CASE 0
			SWITCH iPedID
				CASE 81	fHeading = -135.000092	BREAK
				CASE 80	fHeading = -135.000092	BREAK
				CASE 79	fHeading = -45.000103	BREAK
				CASE 78	fHeading = 44.999939	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iPedID
				CASE 81	fHeading = 44.999908	BREAK
				CASE 80	fHeading = 44.999908	BREAK
				CASE 79	fHeading = -225.000092	BREAK
				CASE 78	fHeading = -135.000031	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iPedID
				CASE 81	fHeading = -224.999954	BREAK
				CASE 80	fHeading = -224.999954	BREAK
				CASE 79	fHeading = -134.999969	BREAK
				CASE 78	fHeading = -44.999928	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iPedID
				CASE 81	fHeading = -44.999962	BREAK
				CASE 80	fHeading = -44.999962	BREAK
				CASE 79	fHeading = 45.000015	BREAK
				CASE 78	fHeading = -224.999908	BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH iPedID
				CASE 81	fHeading = -44.999962	BREAK
				CASE 80	fHeading = -44.999962	BREAK
				CASE 79	fHeading = 45.000015	BREAK
				CASE 78	fHeading = -224.999908	BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH iPedID
				CASE 81	fHeading = -224.999954	BREAK
				CASE 80	fHeading = -224.999954	BREAK
				CASE 79	fHeading = -134.999969	BREAK
				CASE 78	fHeading = -44.999928	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN fHeading
ENDFUNC

FUNC FLOAT GET_INSIDE_TRACK_CHAIR_HEADING()
	RETURN 45.0
ENDFUNC

FUNC FLOAT GET_SLOT_MACHINE_SPECTATOR_HEADING()
	FLOAT fOffset = 0.0
	CASINO_ACTIVITY_SLOTS eActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSlotMachineSpectatorActivity)
	CASINO_PED_TYPES ePedType = INT_TO_ENUM(CASINO_PED_TYPES, g_iSpectatingSlotMachinePedType)
	BOOL bFemalePed = IS_CASINO_PED_TYPE_FEMALE(ePedType)
	
	IF bFemalePed
		SWITCH eActivity
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_01A				fOffset = 15.0		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_01B				fOffset = 0.0		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_02A				fOffset = 5.0		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_02B				fOffset = 0.0		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_WITH_DRINK_01A	fOffset = 20.0		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_WITH_DRINK_01B	fOffset = 10.0		BREAK
		ENDSWITCH
	ELSE
		SWITCH eActivity
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_01A				fOffset = -10.0		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_01B				fOffset = -10.0		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_02A				fOffset = 10.0		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_02B				fOffset = 0.0		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_WITH_DRINK_01A	fOffset = 20.0		BREAK
			CASE CASINO_AC_SLOT_MACHINE_SPECTATE_STAND_WITH_DRINK_01B	fOffset = 0.0		BREAK
		ENDSWITCH
	ENDIF
	
	FLOAT fSlotMachineHeading = GET_SLOT_MACHINE_PED_HEADING(g_iSpectatingSlotMachinePedID)
	fSlotMachineHeading += fOffset
	
	RETURN fSlotMachineHeading
ENDFUNC

FUNC FLOAT GET_TABLE_SPECTATOR_HEADING(INT iPedID, CASINO_ACTIVITY_SLOTS eActivity)
	
	CASINO_TABLE_GAME_TYPE eGameType
	IF (g_iPedReservedBlackjackTable = 0 OR g_iPedReservedBlackjackTable = 1)
		eGameType = CASINO_TABLE_GAME_BLACKJACK
	ELIF (g_iPedReservedRouletteTable = 0 OR g_iPedReservedRouletteTable = 1)
		eGameType = CASINO_TABLE_GAME_ROULETTE
	ENDIF
	
	SWITCH eGameType
		CASE CASINO_TABLE_GAME_BLACKJACK
			IF g_iPedReservedBlackjackTable = 0
				SWITCH eActivity
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_01A
						SWITCH iPedID
							CASE 88	RETURN 202.320
							CASE 89 RETURN 298.800
						ENDSWITCH
					BREAK
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_02A
						SWITCH iPedID
							CASE 88	RETURN 185.000
							CASE 89 RETURN 274.320
						ENDSWITCH
					BREAK
				ENDSWITCH
			ELIF g_iPedReservedBlackjackTable = 1
				SWITCH eActivity
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_01A
						SWITCH iPedID
							CASE 88	RETURN 28.000
							CASE 89 RETURN 113.040
						ENDSWITCH
					BREAK
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_02A
						SWITCH iPedID
							CASE 88	RETURN 15.000
							CASE 89 RETURN 100.000
						ENDSWITCH
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CASINO_TABLE_GAME_ROULETTE
			IF g_iPedReservedRouletteTable = 0
				SWITCH eActivity
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_01A
						SWITCH iPedID
							CASE 88	RETURN 250.000
							CASE 89 RETURN 298.800
						ENDSWITCH
					BREAK
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_02A
						SWITCH iPedID
							CASE 88	RETURN 234.720
							CASE 89 RETURN 281.520
						ENDSWITCH
					BREAK
				ENDSWITCH
			ELIF g_iPedReservedRouletteTable = 1
				SWITCH eActivity
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_01A
						SWITCH iPedID
							CASE 88	RETURN 70.240
							CASE 89 RETURN 120.000
						ENDSWITCH
					BREAK
					CASE CASINO_AC_SLOT_TABLE_GAME_SPECTATE_STAND_02A
						SWITCH iPedID
							CASE 88	RETURN 50.000
							CASE 89 RETURN 100.000
						ENDSWITCH
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_BLACKJACK_DEALER_HEADING(INT iTableID)
	SWITCH iTableID
		CASE 0	RETURN 45.31
		CASE 1	RETURN -134.69
		CASE 2	RETURN -44.69
		CASE 3	RETURN -44.69
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_THREE_CARD_POKER_DEALER_HEADING(INT iTableID)
	SWITCH iTableID
		CASE 0	RETURN 45.00
		CASE 1	RETURN -135.00
		CASE 2	RETURN 135.00
		CASE 3	RETURN 135.00
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_ROULETTE_DEALER_HEADING(INT iTableID)
	SWITCH iTableID
		CASE 0	RETURN 45.00
		CASE 1	RETURN -135.00
		CASE 2	RETURN -45.00
		CASE 3	RETURN 135.00
		CASE 4	RETURN 135.00
		CASE 5	RETURN -45.00
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_MANAGERS_HEADING()
	SWITCH g_iManagerPedVariation
		CASE 0 	RETURN -157.750
		CASE 1 	RETURN 90.0
		CASE 2 	RETURN -90.0
		CASE 3 	RETURN 0.0
		CASE 4	RETURN 153.5
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_CASINO_PED_HEADING(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea, CASINO_ACTIVITY_SLOTS eActivity #IF IS_DEBUG_BUILD, BOOL bDebugPrints = FALSE #ENDIF)
	
	#IF IS_DEBUG_BUILD
	IF bDebugPrints
		PRINTLN("[CASINO_PEDS] GET_CASINO_PED_HEADING - Ped: ", iPedID, " Layout: ", iLayout, " Casino Area: ", GET_CASINO_AREA_STRING(eCasinoArea))
	ENDIF
	#ENDIF	
	
	#IF IS_DEBUG_BUILD
	IF IS_CASINO_PED_DEBUG_EDITOR_ACTIVE()
	AND IS_CASINO_PED_DEBUG_REPOSITION_PEDS_WIDGET_ACTIVE()
		IF g_fCasinoPedHeading[iPedID] != -1
			#IF IS_DEBUG_BUILD
			IF bDebugPrints
				PRINTLN("[CASINO_PEDS] GET_CASINO_PED_HEADING - Debug Ped Coords: ", g_fCasinoPedHeading[iPedID])
			ENDIF
			#ENDIF
			RETURN g_fCasinoPedHeading[iPedID]
		ENDIF
	ENDIF
	#ENDIF
	
	FLOAT fHeading = 0.0
	IF OVERRIDE_CASINO_SPECIAL_PED_HEADING(iPedID,iLayout,eCasinoArea,fHeading)
	AND NOT g_bCasinoIntroCutsceneRequired
		#IF IS_DEBUG_BUILD
		IF bDebugPrints
			PRINTLN("[CASINO_PEDS] GET_CASINO_PED_HEADING - Special Ped Coords: ", fHeading)
		ENDIF
		#ENDIF
		RETURN fHeading
	ENDIF
	
	SWITCH eCasinoArea
		CASE CASINO_AREA_PERMANENT
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						CASE 0		fHeading = 300.7990	BREAK
						CASE 1		fHeading = 95.9990	BREAK
						CASE 2		fHeading = 239.7980	BREAK
						CASE 3		fHeading = 225.5460	BREAK
						CASE 4		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 5		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 6		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 7		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 8		fHeading = 49.0710	BREAK
						CASE 9		fHeading = 3.2210	BREAK
						CASE 10		fHeading = -7.9770	BREAK
						CASE 11		fHeading = 185.7600	BREAK
						CASE 12		fHeading = GET_SLOT_MACHINE_SPECTATOR_HEADING()		BREAK
						CASE 13		fHeading = 222.5200	BREAK
						CASE 14		fHeading = 138.9440	BREAK
						CASE 15		fHeading = 11.6690	BREAK
						CASE 16		fHeading = 273.9440	BREAK
						CASE 17		fHeading = 83.5200	BREAK
						CASE 18		fHeading = 163.5430	BREAK
						CASE 19		fHeading = -30.2400	BREAK
						CASE 20		fHeading = 193.3300	BREAK
						CASE 21		fHeading = 132.7930	BREAK
						CASE 22		fHeading = 337.8430	BREAK
						CASE 23		fHeading = 339.7670	BREAK
						CASE 24		fHeading = 122.1670	BREAK
						CASE 25		fHeading = 295.4660	BREAK
						CASE 26		fHeading = 172.7160	BREAK
						CASE 27		fHeading = 44.8410	BREAK
						CASE 28		fHeading = 278.7910	BREAK
						CASE 29		fHeading = 82.2660	BREAK
						CASE 30		fHeading = 84.2200	BREAK
						CASE 31		fHeading = 205.1950	BREAK
						CASE 32		fHeading = 308.1950	BREAK
						CASE 33		fHeading = 281.3940	BREAK
						CASE 34		fHeading = 91.9940	BREAK
						CASE 35		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 36		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 37		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 38		fHeading = -22.4500	BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						CASE 0		fHeading = 300.7990	BREAK
						CASE 1		fHeading = 95.9990	BREAK
						CASE 2		fHeading = 239.7980	BREAK
						CASE 3		fHeading = 143.0210	BREAK
						CASE 4		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 5		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 6		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 7		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 8		fHeading = -162.0000	BREAK
						CASE 9		fHeading = -48.7540	BREAK
						CASE 10		fHeading = -7.9770	BREAK
						CASE 11		fHeading = 92.0600	BREAK
						CASE 12		fHeading = GET_SLOT_MACHINE_SPECTATOR_HEADING()		BREAK
						CASE 13		fHeading = 93.5400	BREAK
						CASE 14		fHeading = 76.4690	BREAK
						CASE 15		fHeading = 11.6690	BREAK
						CASE 16		fHeading = 254.9690	BREAK
						CASE 17		fHeading = 83.5200	BREAK
						CASE 18		fHeading = 81.3600	BREAK
						CASE 19		fHeading = -30.2400	BREAK
						CASE 20		fHeading = 193.3300	BREAK
						CASE 21		fHeading = 200.8800	BREAK
						CASE 22		fHeading = 221.6680	BREAK
						CASE 23		fHeading = 45.3600	BREAK
						CASE 24		fHeading = -188.6400	BREAK
						CASE 25		fHeading = 295.4660	BREAK
						CASE 26		fHeading = 141.4160	BREAK
						CASE 27		fHeading = 16.1160	BREAK
						CASE 28		fHeading = 235.5910	BREAK
						CASE 29		fHeading = 131.7600	BREAK
						CASE 30		fHeading = 55.4200	BREAK
						CASE 31		fHeading = 218.4450	BREAK
						CASE 32		fHeading = 7.2000	BREAK
						CASE 33		fHeading = 281.3940	BREAK
						CASE 34		fHeading = 127.1190	BREAK
						CASE 35		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 36		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 37		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 38		fHeading = 3.5750	BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPedID
						CASE 0		fHeading = 300.7990	BREAK
						CASE 1		fHeading = 95.9990	BREAK
						CASE 2		fHeading = 140.7982	BREAK
						CASE 3		fHeading = 331.9975	BREAK
						CASE 4		fHeading = 297.7972	BREAK
						CASE 5		fHeading = 180.5969	BREAK
						CASE 6		fHeading = 333.7965	BREAK
						CASE 7		fHeading = 51.5963	BREAK
						CASE 8		fHeading = 7.9963	BREAK
						CASE 9		fHeading = 155.3959	BREAK
						CASE 10		fHeading = 0.0000	BREAK
						CASE 11		fHeading = 142.9957	BREAK
						CASE 12		fHeading = 177.1955	BREAK
						CASE 13		fHeading = 97.3952	BREAK
						CASE 14		fHeading = 268.1949	BREAK
						CASE 15		fHeading = 93.9945	BREAK
						CASE 16		fHeading = 116.1944	BREAK
						CASE 17		fHeading = 40.7942	BREAK
						CASE 18		fHeading = 328.5939	BREAK
						CASE 19		fHeading = 222.9935	BREAK
						CASE 20		fHeading = 141.7933	BREAK
						CASE 21		fHeading = 321.1930	BREAK
						CASE 22		fHeading = 171.9928	BREAK
					ENDSWITCH
				BREAK
				CASE 4
					SWITCH iPedID
						// Permanent
						CASE 0		fHeading = GET_SLOT_MACHINE_HEADING(0)	BREAK
						CASE 1		fHeading = GET_SLOT_MACHINE_HEADING(10)	BREAK
						// Table Games
						CASE 2		fHeading = 21.9920			BREAK
						CASE 3		fHeading = GET_THREE_CARD_POKER_DEALER_HEADING(2)	BREAK
						CASE 4		fHeading = 117.9910		BREAK
						CASE 5		fHeading = 129.2880	BREAK
						CASE 6		fHeading = 31.8850	BREAK
						CASE 7		fHeading = 14.4000	BREAK
						CASE 8		fHeading = GET_ROULETTE_DEALER_HEADING(2)			BREAK
						CASE 9		fHeading = GET_ROULETTE_DEALER_HEADING(3)			BREAK
						CASE 10		fHeading = GET_BLACKJACK_DEALER_HEADING(2)			BREAK
						CASE 11		fHeading = GET_SLOT_MACHINE_HEADING(22)	BREAK
						CASE 12		fHeading = GET_SLOT_MACHINE_HEADING(27)	BREAK
						CASE 13		fHeading = 79.7870	BREAK
						CASE 14		fHeading = 319.1370	BREAK
						CASE 15		fHeading = 215.3370	BREAK
						CASE 16		fHeading = 251.3860	BREAK
						CASE 17		fHeading = 43.5360	BREAK
						CASE 18		fHeading = 324.1870	BREAK
						CASE 19		fHeading = 198.2360	BREAK
						CASE 20		fHeading = 72.3110	BREAK
						CASE 21		fHeading = 272.9860	BREAK
						CASE 22		fHeading = 87.3860	BREAK
						CASE 23		fHeading = 156.6880	BREAK
						CASE 24		fHeading = -18.0000	BREAK
						CASE 25		fHeading = 259.2000	BREAK
						CASE 26		fHeading = 85.6800	BREAK
						CASE 27		fHeading = 253.7910	BREAK
						CASE 28		fHeading = 107.6410	BREAK
						CASE 29		fHeading = 128.1600	BREAK
						CASE 30		fHeading = -88.5600	BREAK
						CASE 31		fHeading = 9.3600	BREAK
						CASE 32		fHeading = 275.0000	BREAK
						CASE 33		fHeading = 45.00	BREAK	// Poker dealer 0
						CASE 34		fHeading = -135.000	BREAK	// Poker dealer 1
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_MAIN_FLOOR
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Entrance (15)
						CASE 39		fHeading = 128.6750	BREAK
						CASE 40		fHeading = -90.0000	BREAK
						CASE 41		fHeading = 17.0750	BREAK
						CASE 42		fHeading = 251.9150	BREAK
						CASE 43		fHeading = 187.8900	BREAK
						CASE 44		fHeading = 177.5690	BREAK
						CASE 45		fHeading = 320.0690	BREAK
						CASE 46		fHeading = 128.1600	BREAK
						CASE 47		fHeading = -44.6400	BREAK
						CASE 48		fHeading = 258.1180	BREAK
						CASE 49		fHeading = 113.5670	BREAK
						CASE 50		fHeading = 326.9420	BREAK
						CASE 51		fHeading = 340.7690	BREAK
						CASE 52		fHeading = 120.5930	BREAK
						CASE 53		fHeading = 207.5930	BREAK
						// Behind Bar (13)
						CASE 54		fHeading = 78.5230	BREAK
						CASE 55		fHeading = 57.3410	BREAK
						CASE 56		fHeading = 160.0160	BREAK
						CASE 57		fHeading = 14.1160	BREAK
						CASE 58		fHeading = 200.0160	BREAK
						CASE 59		fHeading = 31.4660	BREAK
						CASE 60		fHeading = 232.3880	BREAK
						CASE 61		fHeading = 323.2800	BREAK
						CASE 62		fHeading = 37.8800	BREAK
						CASE 63		fHeading = 74.3880	BREAK
						CASE 64		fHeading = 189.9620	BREAK
						CASE 65		fHeading = 63.7620	BREAK
						CASE 66		fHeading = 85.0000	BREAK
						// All the rest (53)
						CASE 67		fHeading = 21.9920	BREAK
						CASE 68		fHeading = 139.6800	BREAK
						CASE 69		fHeading = 99.0920	BREAK
						CASE 70		fHeading = 175.0670	BREAK
						CASE 71		fHeading = 284.1920	BREAK
						CASE 72		fHeading = 272.1920	BREAK
						CASE 73		fHeading = 156.2910	BREAK
						CASE 74		fHeading = 18.7910	BREAK
						CASE 75		fHeading = 117.9910	BREAK
						CASE 76		fHeading = 360.0000	BREAK
						CASE 77		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 78		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 79		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 80		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
//						CASE 81		fHeading = 225.5460	BREAK
						CASE 82		fHeading = 239.4200	BREAK
						CASE 83		fHeading = 116.9940	BREAK
						CASE 84		fHeading = 353.9940	BREAK
						CASE 85		fHeading = 136.1930	BREAK
						CASE 86		fHeading = 3.1930	BREAK
						CASE 87		fHeading = 242.5930	BREAK
						CASE 88		fHeading = 90.3920	BREAK
						CASE 89		fHeading = 278.7670	BREAK
						CASE 90		fHeading = 261.9170	BREAK
						CASE 91		fHeading = 148.5920	BREAK
						CASE 92		fHeading = 83.7920	BREAK
						CASE 93		fHeading = 218.7910	BREAK
						CASE 94		fHeading = 23.5920	BREAK
						CASE 95		fHeading = 199.9920	BREAK
						CASE 96		fHeading = 315.9910	BREAK
						CASE 97		fHeading = -10.4850	BREAK
						CASE 98		fHeading = -46.8000	BREAK
						CASE 99		fHeading = 167.4640	BREAK
						CASE 100	fHeading = 111.9890	BREAK
						CASE 101	fHeading = 287.0390	BREAK
						CASE 102	fHeading = 266.2640	BREAK
						CASE 103	fHeading = 134.8380	BREAK
						CASE 104	fHeading = 19.7130	BREAK
						CASE 105	fHeading = 350.3880	BREAK
						CASE 106	fHeading = 156.6880	BREAK
						CASE 107	fHeading = 231.3880	BREAK
						CASE 108	fHeading = 56.5880	BREAK
						CASE 109	fHeading = 288.3170	BREAK
						CASE 110	fHeading = 136.5150	BREAK
						CASE 111	fHeading = 318.991	BREAK
						CASE 112	fHeading = 305.2800	BREAK
						CASE 113	fHeading = 6.7000	BREAK
						CASE 114	fHeading = 68.3170	BREAK
						CASE 115	fHeading = 253.7910	BREAK
						CASE 116	fHeading = 66.7910	BREAK
						CASE 117	fHeading = 212.0660	BREAK
						CASE 118	fHeading = 93.7910	BREAK
						CASE 119	fHeading = 344.7910	BREAK
						// Special peds (5)
						CASE 120	fHeading = 89.0000	BREAK
						CASE 121	fHeading = 268.9990	BREAK
						CASE 122	fHeading = 178.7980	BREAK
						CASE 123	fHeading = 314.0230	BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Entrance (15)
						CASE 39		fHeading = 168.6750	BREAK
						CASE 40		fHeading = 52.1500	BREAK
						CASE 41		fHeading = 31.6800	BREAK
						CASE 42		fHeading = 159.1200	BREAK
						CASE 43		fHeading = -95.7600	BREAK
						CASE 44		fHeading = 177.5690	BREAK
						CASE 45		fHeading = 210.2400	BREAK
						CASE 46		fHeading = 150.1850	BREAK
						CASE 47		fHeading = -44.6400	BREAK
						CASE 48		fHeading = -42.6400	BREAK
						CASE 49		fHeading = 113.5670	BREAK
						CASE 50		fHeading = 50.5600	BREAK
						CASE 51		fHeading = 69.1200	BREAK
						CASE 52		fHeading = 134.3680	BREAK
						CASE 53		fHeading = 257.6180	BREAK
						// Behind Bar (13)
						CASE 54		fHeading = 48.1230	BREAK
						CASE 55		fHeading = 57.3410	BREAK
						CASE 56		fHeading = 160.0160	BREAK
						CASE 57		fHeading = -206.6400	BREAK
						CASE 58		fHeading = -22.7150	BREAK
						CASE 59		fHeading = 31.4660	BREAK
						CASE 60		fHeading = 288.3630	BREAK
						CASE 61		fHeading = 341.5550	BREAK
						CASE 62		fHeading = 192.2400	BREAK
						CASE 63		fHeading = 87.8630	BREAK
						CASE 64		fHeading = 138.2370	BREAK
						CASE 65		fHeading = -18.5630	BREAK
						CASE 66		fHeading = 85.0000	BREAK
						// All the rest (53)
						CASE 67		fHeading = 38.8800	BREAK
						CASE 68		fHeading = 117.3600	BREAK
						CASE 69		fHeading = 56.8800	BREAK
						CASE 70		fHeading = 215.000	BREAK
						CASE 71		fHeading = 316.800	BREAK
						CASE 72		fHeading = 27.3600	BREAK
						CASE 73		fHeading = 211.5410	BREAK
						CASE 74		fHeading = 42.8660	BREAK
						CASE 75		fHeading = 74.8800	BREAK
						CASE 76		fHeading = -92.8800	BREAK
						CASE 77		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 78		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 79		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 80		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
//						CASE 81		fHeading = 225.5460	BREAK
						CASE 82		fHeading = 271.6200	BREAK
						CASE 83		fHeading = 164.8800	BREAK
						CASE 84		fHeading = 339.9940	BREAK
						CASE 85		fHeading = 136.1930	BREAK
						CASE 86		fHeading = 3.1930	BREAK
						CASE 87		fHeading = 242.5930	BREAK
						CASE 88		fHeading = 90.3920	BREAK
						CASE 89		fHeading = -18.0000	BREAK
						CASE 90		fHeading = 2.8800	BREAK
						CASE 91		fHeading = 188.9920	BREAK
						CASE 92		fHeading = 23.7600	BREAK
						CASE 93		fHeading = 269.2660	BREAK
						CASE 94		fHeading = -141.1200	BREAK
						CASE 95		fHeading = 223.9920	BREAK
						CASE 96		fHeading = 315.9910	BREAK
						CASE 97		fHeading = -10.4850	BREAK
						CASE 98		fHeading = -44.7500	BREAK
						CASE 99		fHeading = 167.4640	BREAK
						CASE 100	fHeading = 111.9890	BREAK
						CASE 101	fHeading = 9.3600	BREAK
						CASE 102	fHeading = -8.6400	BREAK
						CASE 103	fHeading = -164.8800	BREAK
						CASE 104	fHeading = -105.1200	BREAK
						CASE 105	fHeading = 324.8380	BREAK
						CASE 106	fHeading = 125.8880	BREAK
						CASE 107	fHeading = 213.1380	BREAK
						CASE 108	fHeading = -34.5620	BREAK
						CASE 109	fHeading = 287.8920	BREAK
						CASE 110	fHeading = -121.8700	BREAK
						CASE 111	fHeading = 144.0000		BREAK
						CASE 112	fHeading = 6.5750		BREAK
						CASE 113	fHeading = -138.2400	BREAK
						CASE 114	fHeading = 82.0420		BREAK
						CASE 115	fHeading = 253.7910		BREAK
						CASE 116	fHeading = 39.3160		BREAK
						CASE 117	fHeading = 155.2160		BREAK
						CASE 118	fHeading = -100.8000	BREAK
						CASE 119	fHeading = 72.7200		BREAK
						// Special peds (5)
						CASE 120	fHeading = 89.0000	BREAK
						CASE 121	fHeading = 268.9990	BREAK
						CASE 122	fHeading = 178.7980	BREAK
						CASE 123	fHeading = 314.0230	BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPedID
						CASE 39		fHeading = 0.0000	BREAK
						CASE 40		fHeading = 75.0000	BREAK
						CASE 41		fHeading = 75.0000	BREAK
						CASE 42		fHeading = 80.0000	BREAK
						CASE 43		fHeading = 57.3410	BREAK
						CASE 44		fHeading = 50.0000	BREAK
						CASE 45		fHeading = 3.7923	BREAK
						CASE 46		fHeading = 316.8000	BREAK
						CASE 47		fHeading = 5.0000	BREAK
						CASE 48		fHeading = -20.0000	BREAK
						CASE 49		fHeading = 34.5600	BREAK
						CASE 50		fHeading = 0.0000	BREAK
						CASE 51		fHeading = 28.0800	BREAK
						CASE 52		fHeading = 263.3905	BREAK
						CASE 53		fHeading = 263.3905	BREAK
						CASE 54		fHeading = 81.3600	BREAK
						CASE 55		fHeading = 81.3600	BREAK
						CASE 56		fHeading = 31.6800	BREAK
						CASE 57		fHeading = 34.5600	BREAK
						CASE 58		fHeading = 306.7200	BREAK
						CASE 59		fHeading = 288.7881	BREAK
						CASE 60		fHeading = 273.6000	BREAK
						CASE 61		fHeading = 44.6400	BREAK
						CASE 62		fHeading = 170.6400	BREAK
						CASE 63		fHeading = 97.9200	BREAK
						CASE 64		fHeading = 213.8400	BREAK
						CASE 65		fHeading = 73.4400	BREAK
						CASE 66		fHeading = 0.0000	BREAK
						CASE 67		fHeading = 267.1200	BREAK
						CASE 68		fHeading = 93.3861	BREAK
						CASE 69		fHeading = 17.5859	BREAK
						CASE 70		fHeading = 71.2800	BREAK
						CASE 71		fHeading = 54.7200	BREAK
						CASE 72		fHeading = 50.0000	BREAK
						CASE 73		fHeading = 108.0000	BREAK
						CASE 74		fHeading = 58.3200	BREAK
						CASE 75		fHeading = 33.3500	BREAK
						CASE 76		fHeading = 0.0000	BREAK
						CASE 77		fHeading = 23.5844	BREAK
						CASE 78		fHeading = 23.5844	BREAK
						CASE 79		fHeading = 339.8400	BREAK
						CASE 80		fHeading = 351.5843	BREAK
						CASE 81		fHeading = 48.2400	BREAK
						CASE 82		fHeading = 315.9910	BREAK
						CASE 83		fHeading = GET_SLOT_MACHINE_HEADING(21)	BREAK
						CASE 84		fHeading = GET_SLOT_MACHINE_HEADING(2)	BREAK
						CASE 85		fHeading = GET_SLOT_MACHINE_HEADING(23)	BREAK
						CASE 86		fHeading = GET_SLOT_MACHINE_HEADING(6)	BREAK
						CASE 87		fHeading = GET_SLOT_MACHINE_HEADING(18)	BREAK
						CASE 88		fHeading = 201.1828	BREAK
						CASE 89		fHeading = 150.5828	BREAK
						CASE 90		fHeading = 261.7824	BREAK
						CASE 91		fHeading = GET_SLOT_MACHINE_HEADING(36)	BREAK
						CASE 92		fHeading = 297.9820	BREAK
						CASE 93		fHeading = 191.7816	BREAK
						CASE 94		fHeading = 92.5815	BREAK
					ENDSWITCH
				BREAK
				CASE 4
					SWITCH iPedID
						// Entrance (15)
						CASE 39		fHeading = 128.6750	BREAK
						CASE 40		fHeading = -90.0000	BREAK
						CASE 41		fHeading = 17.0750	BREAK
						CASE 42		fHeading = 251.9150	BREAK
						CASE 43		fHeading = 187.8900	BREAK
						CASE 44		fHeading = 177.5690	BREAK
						CASE 45		fHeading = 320.0690	BREAK
						CASE 46		fHeading = 128.1600	BREAK
						CASE 47		fHeading = -44.6400	BREAK
						CASE 48		fHeading = 258.1180	BREAK
						CASE 49		fHeading = 113.5670	BREAK
						CASE 50		fHeading = 326.9420	BREAK
						CASE 51		fHeading = 340.7690	BREAK
						CASE 52		fHeading = 120.5930	BREAK
						CASE 53		fHeading = 207.5930	BREAK
						// Behind Bar (13)
						CASE 54		fHeading = 78.5230	BREAK
						CASE 55		fHeading = 57.3410	BREAK
						CASE 56		fHeading = GET_ROULETTE_DEALER_HEADING(0)	BREAK
						CASE 57		fHeading = 14.1160	BREAK
						CASE 58		fHeading = 200.0160	BREAK
						CASE 59		fHeading = GET_ROULETTE_DEALER_HEADING(1)	BREAK
						CASE 60		fHeading = 232.3880	BREAK
						CASE 61		fHeading = 323.2800	BREAK
						CASE 62		fHeading = 37.8800	BREAK
						CASE 63		fHeading = 74.3880	BREAK
						CASE 64		fHeading = 189.9620	BREAK
						CASE 65		fHeading = 63.7620	BREAK
						CASE 66		fHeading = 85.0000	BREAK
						// All the rest (53)
						CASE 67		fHeading = GET_BLACKJACK_DEALER_HEADING(0)	BREAK
						CASE 68		fHeading = 139.6800	BREAK
						CASE 69		fHeading = 99.0920	BREAK
						CASE 70		fHeading = 175.0670	BREAK
						CASE 71		fHeading = 284.1920	BREAK
						CASE 72		fHeading = GET_BLACKJACK_CHAIR_HEADING(0, 0)	BREAK
						CASE 73		fHeading = GET_BLACKJACK_CHAIR_HEADING(0, 1)	BREAK
						CASE 74		fHeading = GET_BLACKJACK_CHAIR_HEADING(0, 2)	BREAK
						CASE 75		fHeading = GET_BLACKJACK_CHAIR_HEADING(0, 3)	BREAK
						CASE 76		fHeading = 360.0000	BREAK
						CASE 77		fHeading = GET_SLOT_MACHINE_HEADING(27)	BREAK
						CASE 78		fHeading = GET_SLOT_MACHINE_HEADING(32)	BREAK
						CASE 79		fHeading = GET_SLOT_MACHINE_HEADING(51)	BREAK
						CASE 80		fHeading = GET_SLOT_MACHINE_HEADING(41)	BREAK
						CASE 81		fHeading = GET_BLACKJACK_DEALER_HEADING(1)	BREAK
						CASE 82		fHeading = 239.4200	BREAK
						CASE 83		fHeading = 116.9940	BREAK
						CASE 84		fHeading = 353.9940	BREAK
						CASE 85		fHeading = 136.1930	BREAK
						CASE 86		fHeading = 3.1930	BREAK
						CASE 87		fHeading = 242.5930	BREAK
						CASE 90		fHeading = 261.9170	BREAK
						CASE 91		fHeading = 148.5920	BREAK
						CASE 92		fHeading = 83.7920	BREAK
						CASE 93		fHeading = 218.7910	BREAK
						CASE 94		fHeading = 23.5920	BREAK
						CASE 95		fHeading = 199.9920	BREAK
						CASE 96		fHeading = 315.9910	BREAK
						CASE 98		fHeading = -46.8000	BREAK
						CASE 100	fHeading = 111.9890	BREAK
						CASE 103	fHeading = 134.8380	BREAK
						CASE 104	fHeading = 19.7130	BREAK
						CASE 105	fHeading = 350.3880	BREAK
						CASE 106	fHeading = 156.6880	BREAK
						CASE 107	fHeading = 231.3880	BREAK
						CASE 108	fHeading = 56.5880	BREAK
						CASE 109	fHeading = 288.3170	BREAK
						CASE 110	fHeading = 136.5150	BREAK
						CASE 111	fHeading = 324.9910	BREAK
						CASE 112	fHeading = 305.2800	BREAK
						CASE 113	fHeading = 6.7000	BREAK
						CASE 114	fHeading = 68.3170	BREAK
						CASE 115	fHeading = 253.7910	BREAK
						CASE 116	fHeading = 66.7910	BREAK
						CASE 117	fHeading = 212.0660	BREAK
						CASE 118	fHeading = 93.7910	BREAK
						CASE 119	fHeading = 344.7910	BREAK
						// Special peds (5)
						CASE 120	fHeading = 89.0000	BREAK
						CASE 121	fHeading = 268.9990	BREAK
						CASE 122	fHeading = 178.7980	BREAK
						CASE 123	fHeading = 314.0230	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_TABLE_GAMES
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Need to include Sports Betting area here for culling from the Main Floor
						// Sports Betting
						CASE 39		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()				BREAK
						CASE 40		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()				BREAK
						CASE 41		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()				BREAK
						CASE 42		fHeading = 33.8400	BREAK
						CASE 43		fHeading = 150.5870	BREAK
						CASE 44		fHeading = 266.1870	BREAK
						CASE 45		fHeading = 131.8050	BREAK
						CASE 46		fHeading = 170.0610	BREAK
						CASE 47		fHeading = 352.7360	BREAK
						CASE 48		fHeading = 25.3860	BREAK
						CASE 49		fHeading = 58.0360	BREAK
						CASE 50		fHeading = 156.6850	BREAK
						CASE 51		fHeading = -19.4400	BREAK
						CASE 52		fHeading = 281.9850	BREAK
						CASE 53		fHeading = 103.9840	BREAK	
						// Lounge
//						CASE 54		fHeading = 335.1900	BREAK
//						CASE 55		fHeading = 125.9900	BREAK
//						CASE 56		fHeading = 225.1900	BREAK
//						CASE 57		fHeading = 31.3900	BREAK
						CASE 58		fHeading = 96.9890	BREAK
						CASE 59		fHeading = 214.7890	BREAK
						CASE 60		fHeading = 340.7890	BREAK
						CASE 61		fHeading = -23.0400	BREAK
//						CASE 62		fHeading = 280.1890	BREAK
						CASE 63		fHeading = 161.6880	BREAK
//						CASE 64		fHeading = 198.7880	BREAK
						CASE 65		fHeading = -48.0370	BREAK
						CASE 66		fHeading = 136.7880	BREAK
						// Table Games
						// Dealers
						CASE 67		fHeading = GET_BLACKJACK_DEALER_HEADING(g_iPedReservedBlackjackTable)				BREAK
						CASE 68		fHeading = GET_THREE_CARD_POKER_DEALER_HEADING(g_iPedReservedThreeCardPokerTable)	BREAK
						CASE 69		fHeading = GET_ROULETTE_DEALER_HEADING(g_iPedReservedRouletteTable)					BREAK
//						CASE 70		fHeading = 264.4980	BREAK
//						CASE 71		fHeading = 359.5970	BREAK
						CASE 72		fHeading = GET_BLACKJACK_PED_HEADING(iPedID)	BREAK
						CASE 73		fHeading = GET_BLACKJACK_PED_HEADING(iPedID)	BREAK
						CASE 74		fHeading = GET_BLACKJACK_PED_HEADING(iPedID)	BREAK
						CASE 75		fHeading = GET_BLACKJACK_PED_HEADING(iPedID)	BREAK
						CASE 76		fHeading = GET_TABLE_SPECTATOR_HEADING(iPedID, eActivity)		BREAK
						CASE 77		fHeading = 129.2880	BREAK
						CASE 78		fHeading = GET_ROULETTE_PED_HEADING(iPedID)		BREAK
						CASE 79		fHeading = GET_ROULETTE_PED_HEADING(iPedID)		BREAK
						CASE 80		fHeading = GET_ROULETTE_PED_HEADING(iPedID)		BREAK
						CASE 81		fHeading = GET_ROULETTE_PED_HEADING(iPedID)		BREAK
						CASE 82		fHeading = 31.8850	BREAK
						CASE 83		fHeading = 14.4000	BREAK
						CASE 84		fHeading = GET_THREE_CARD_POKER_PED_HEADING(iPedID)	BREAK
						CASE 85		fHeading = GET_THREE_CARD_POKER_PED_HEADING(iPedID)	BREAK
						CASE 86		fHeading = GET_THREE_CARD_POKER_PED_HEADING(iPedID)	BREAK
						CASE 87		fHeading = GET_THREE_CARD_POKER_PED_HEADING(iPedID)	BREAK
						CASE 88		fHeading = GET_TABLE_SPECTATOR_HEADING(iPedID, eActivity)		BREAK
						CASE 89		fHeading = GET_TABLE_SPECTATOR_HEADING(iPedID, eActivity)		BREAK
						CASE 90		fHeading = 79.7870	BREAK
						CASE 91		fHeading = 319.1370	BREAK
						CASE 92		fHeading = 215.3370	BREAK
						CASE 93		fHeading = 85.7360	BREAK
						CASE 94		fHeading = 52.7870	BREAK
						CASE 95		fHeading = 239.7860	BREAK
						CASE 96		fHeading = 251.3860	BREAK
						CASE 97		fHeading = 43.5360	BREAK
						CASE 98		fHeading = 324.1870	BREAK
						CASE 99		fHeading = 198.2360	BREAK
						CASE 100	fHeading = 72.3110	BREAK
						CASE 101	fHeading = 108.0620	BREAK
						CASE 102	fHeading = 153.9230	BREAK
						CASE 103	fHeading = 272.9860	BREAK
						CASE 104	fHeading = 87.3860	BREAK
						CASE 105	fHeading = 287.8860	BREAK
						CASE 106	fHeading = 156.6880	BREAK
						CASE 107	fHeading = -18.0000	BREAK
						CASE 108	fHeading = 259.2000	BREAK
						CASE 109	fHeading = 85.6800	BREAK
						CASE 110	fHeading = 253.7910	BREAK
						CASE 111	fHeading = 107.6410	BREAK
						CASE 112	fHeading = 128.1600	BREAK
						CASE 113	fHeading = -88.5600	BREAK
						CASE 114	fHeading = 9.3600	BREAK
						CASE 121	fHeading = -89.9300	BREAK
						CASE 123	fHeading = 270.9980	BREAK
						// Bouncer
						CASE 124	fHeading = 275.0000	BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Need to include Sports Betting area here for culling from the Main Floor
						// Sports Betting
						CASE 39		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()		BREAK
						CASE 40		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()		BREAK
						CASE 41		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()		BREAK
						CASE 42		fHeading = -88.5600	BREAK
						CASE 43		fHeading = 77.0400	BREAK
						CASE 44		fHeading = -49.6800	BREAK
						CASE 45		fHeading = 97.9200	BREAK
						CASE 46		fHeading = 209.5200	BREAK
						CASE 47		fHeading = 278.6400	BREAK
						CASE 48		fHeading = 25.3860	BREAK
						CASE 49		fHeading = 58.0360	BREAK
						CASE 50		fHeading = 55.4400	BREAK
						CASE 51		fHeading = 171.3600	BREAK
						CASE 52		fHeading = 281.9850	BREAK
						CASE 53		fHeading = 59.0400	BREAK
						// Lounge
//						CASE 54		fHeading = 335.1900	BREAK
//						CASE 55		fHeading = 125.9900	BREAK
//						CASE 56		fHeading = 225.1900	BREAK
//						CASE 57		fHeading = 31.3900	BREAK
						CASE 58		fHeading = -28.2150	BREAK
						CASE 59		fHeading = 151.9200	BREAK
						CASE 60		fHeading = 340.7890	BREAK
						CASE 61		fHeading = 10.8000	BREAK
//						CASE 62		fHeading = 280.1890	BREAK
						CASE 63		fHeading = 140.4000	BREAK
//						CASE 64		fHeading = 198.7880	BREAK
						CASE 65		fHeading = -91.3870	BREAK
						CASE 66		fHeading = 136.7880	BREAK
						// Table Games
						// Dealers
						CASE 67		fHeading = GET_BLACKJACK_DEALER_HEADING(g_iPedReservedBlackjackTable)				BREAK
						CASE 68		fHeading = GET_THREE_CARD_POKER_DEALER_HEADING(g_iPedReservedThreeCardPokerTable)	BREAK
						CASE 69		fHeading = GET_ROULETTE_DEALER_HEADING(g_iPedReservedRouletteTable)					BREAK
						
//						CASE 67		fHeading = 270.9980										BREAK
//						CASE 68		fHeading = 271.0730										BREAK
//						CASE 69		fHeading = -7.9770										BREAK
//						CASE 70		fHeading = 264.4980										BREAK
//						CASE 71		fHeading = 359.5970										BREAK
						CASE 72		fHeading = GET_BLACKJACK_PED_HEADING(iPedID)			BREAK
						CASE 73		fHeading = GET_BLACKJACK_PED_HEADING(iPedID)			BREAK
						CASE 74		fHeading = GET_BLACKJACK_PED_HEADING(iPedID)			BREAK
						CASE 75		fHeading = GET_BLACKJACK_PED_HEADING(iPedID)			BREAK
						CASE 76		fHeading = GET_TABLE_SPECTATOR_HEADING(iPedID, eActivity)		BREAK
						CASE 77		fHeading = 77.0400	BREAK
						CASE 78		fHeading = GET_ROULETTE_PED_HEADING(iPedID)			BREAK
						CASE 79		fHeading = GET_ROULETTE_PED_HEADING(iPedID)			BREAK
						CASE 80		fHeading = GET_ROULETTE_PED_HEADING(iPedID)			BREAK
						CASE 81		fHeading = GET_ROULETTE_PED_HEADING(iPedID)			BREAK
						CASE 82		fHeading = 81.3350	BREAK
						CASE 83		fHeading = 119.9300	BREAK
						CASE 84		fHeading = GET_THREE_CARD_POKER_PED_HEADING(iPedID)	BREAK
						CASE 85		fHeading = GET_THREE_CARD_POKER_PED_HEADING(iPedID)	BREAK
						CASE 86		fHeading = GET_THREE_CARD_POKER_PED_HEADING(iPedID)	BREAK
						CASE 87		fHeading = GET_THREE_CARD_POKER_PED_HEADING(iPedID)	BREAK
						CASE 88		fHeading = GET_TABLE_SPECTATOR_HEADING(iPedID, eActivity)		BREAK
						CASE 89		fHeading = GET_TABLE_SPECTATOR_HEADING(iPedID, eActivity)		BREAK
						CASE 90		fHeading = 124.9120	BREAK
						CASE 91		fHeading = 319.1370	BREAK
						CASE 92		fHeading = 193.7870	BREAK
						CASE 93		fHeading = 85.7360	BREAK
						CASE 94		fHeading = 3.8870	BREAK
						CASE 95		fHeading = 203.3360	BREAK
						CASE 96		fHeading = 251.3860	BREAK
						CASE 97		fHeading = -128.1600	BREAK
						CASE 98		fHeading = 197.2800	BREAK
						CASE 99		fHeading = 269.2800	BREAK
						CASE 100	fHeading = 263.5200	BREAK
						CASE 101	fHeading = 89.5620	BREAK
						CASE 102	fHeading = 0.9250	BREAK
						CASE 103	fHeading = 337.7360	BREAK
						CASE 104	fHeading = -28.8000	BREAK
						CASE 105	fHeading = 10.8000	BREAK
						CASE 106	fHeading = 197.2800	BREAK
						CASE 107	fHeading = 67.6800	BREAK
						CASE 108	fHeading = 302.6500	BREAK
						CASE 109	fHeading = 73.5550	BREAK
						CASE 110	fHeading = -150.4500	BREAK
						CASE 111	fHeading = 180.0000	BREAK
						CASE 112	fHeading = 100.7100	BREAK
						CASE 113	fHeading = -70.3350	BREAK
						CASE 114	fHeading = 22.3200	BREAK
						CASE 121	fHeading = -89.9300	BREAK
						CASE 123	fHeading = 90.0000	BREAK
						// Bouncer
						CASE 124	fHeading = 275.0000	BREAK
					ENDSWITCH
				BREAK
				CASE 3
					SWITCH iPedID
						CASE 39		fHeading = GET_THREE_CARD_POKER_DEALER_HEADING(2)	BREAK
						CASE 40		fHeading = GET_ROULETTE_DEALER_HEADING(2)		BREAK
						CASE 41		fHeading = GET_BLACKJACK_DEALER_HEADING(2)		BREAK
						CASE 42		fHeading = GET_BLACKJACK_CHAIR_HEADING(2, 0)	BREAK
						CASE 43		fHeading = GET_BLACKJACK_CHAIR_HEADING(2, 1)	BREAK
						CASE 44		fHeading = GET_ROULETTE_DEALER_HEADING(3)		BREAK
						CASE 45		fHeading = GET_ROULETTE_CHAIR_HEADING(3, 0)		BREAK
						CASE 46		fHeading = GET_ROULETTE_CHAIR_HEADING(3, 3)		BREAK
						CASE 47		fHeading = GET_ROULETTE_CHAIR_HEADING(3, 2)		BREAK
						CASE 48		fHeading = 172.1778	BREAK
						CASE 49		fHeading = 246.9600	BREAK
						CASE 50		fHeading = 124.7772	BREAK
						CASE 51		fHeading = 82.7771	BREAK
						CASE 52		fHeading = GET_BLACKJACK_DEALER_HEADING(0)	BREAK
						CASE 53		fHeading = 253.9768	BREAK
						CASE 54		fHeading = 183.7767	BREAK
						CASE 55		fHeading = 262.9767	BREAK
						CASE 56		fHeading = 23.1763	BREAK
						CASE 57		fHeading = GET_THREE_CARD_POKER_DEALER_HEADING(3)	BREAK
						CASE 58		fHeading = GET_BLACKJACK_DEALER_HEADING(3)			BREAK
						CASE 59		fHeading = GET_ROULETTE_DEALER_HEADING(5)			BREAK
						CASE 60		fHeading = GET_THREE_CARD_POKER_CHAIR_HEADING(3, 1)	BREAK
						CASE 61		fHeading = GET_THREE_CARD_POKER_CHAIR_HEADING(3, 2)	BREAK
						CASE 62		fHeading = 215.7747	BREAK
						CASE 63		fHeading = GET_THREE_CARD_POKER_DEALER_HEADING(0)	BREAK
						CASE 64		fHeading = 280.5742	BREAK
						CASE 65		fHeading = 147.3738	BREAK
						CASE 66		fHeading = 0.0000	BREAK
						CASE 67		fHeading = 147.3738	BREAK
						CASE 68		fHeading = 60.3735	BREAK
						CASE 69		fHeading = 263.5200	BREAK
						CASE 70		fHeading = 65.3734	BREAK
						CASE 71		fHeading = 206.9730	BREAK
						CASE 72		fHeading = 198.5730	BREAK
						CASE 73		fHeading = GET_SLOT_MACHINE_HEADING(37)	BREAK
						CASE 74		fHeading = GET_SLOT_MACHINE_HEADING(41)	BREAK
						CASE 75		fHeading = GET_SLOT_MACHINE_HEADING(46)	BREAK
						CASE 76		fHeading = 0.0000	BREAK
						CASE 77		fHeading = GET_SLOT_MACHINE_HEADING(51)	BREAK
						CASE 78		fHeading = GET_SLOT_MACHINE_HEADING(52)	BREAK
						CASE 79		fHeading = 246.9600	BREAK
						CASE 80		fHeading = 296.6400	BREAK
						CASE 81		fHeading = 36.1716	BREAK
						CASE 82		fHeading = 57.1715	BREAK
						CASE 83		fHeading = 57.1715	BREAK
						CASE 84		fHeading = 87.8400	BREAK
						CASE 85		fHeading = 156.5711	BREAK
						CASE 86		fHeading = GET_ROULETTE_CHAIR_HEADING(1, 3)		BREAK
						CASE 87		fHeading = GET_ROULETTE_CHAIR_HEADING(1, 2)		BREAK
						CASE 88		fHeading = GET_ROULETTE_CHAIR_HEADING(1, 1)		BREAK
						//CASE 89		fHeading = 140.9710	BREAK
						CASE 90		fHeading = 147.1702	BREAK
						CASE 91		fHeading = 20.5698	BREAK
						CASE 92		fHeading = 281.3696	BREAK
						CASE 93		fHeading = 263.1694	BREAK
						CASE 94		fHeading = 99.1693	BREAK
						CASE 95		fHeading = GET_ROULETTE_DEALER_HEADING(0)		BREAK
						CASE 96		fHeading = GET_ROULETTE_DEALER_HEADING(1)		BREAK
						CASE 97		fHeading = GET_ROULETTE_DEALER_HEADING(4)		BREAK
						CASE 98		fHeading = GET_THREE_CARD_POKER_DEALER_HEADING(1)	BREAK
						CASE 99		fHeading = GET_BLACKJACK_DEALER_HEADING(1)		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_SPORTS_BETTING
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Sports Betting
						CASE 39		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()		BREAK
						CASE 40		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()		BREAK
						CASE 41		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()		BREAK
						CASE 42		fHeading = 33.8400	BREAK
						CASE 43		fHeading = 150.5870	BREAK
						CASE 44		fHeading = 266.1870	BREAK
						CASE 45		fHeading = 131.8050	BREAK
						CASE 46		fHeading = 170.0610	BREAK
						CASE 47		fHeading = 352.7360	BREAK
						CASE 48		fHeading = 25.3860	BREAK
						CASE 49		fHeading = 58.0360	BREAK
						CASE 50		fHeading = 156.6850	BREAK
						CASE 51		fHeading = -19.4400	BREAK
						CASE 52		fHeading = 281.9850	BREAK
						CASE 53		fHeading = 103.9840	BREAK
						// Behind Bar (13)
						CASE 54		fHeading = 78.5230	BREAK
						CASE 55		fHeading = 57.3410	BREAK
						CASE 56		fHeading = 160.0160	BREAK
						CASE 57		fHeading = 14.1160	BREAK
						CASE 58		fHeading = 200.0160	BREAK
						CASE 59		fHeading = 31.4660	BREAK
						CASE 60		fHeading = 232.3880	BREAK
						CASE 61		fHeading = 323.2800	BREAK
						CASE 62		fHeading = 37.8800	BREAK
						CASE 63		fHeading = 74.3880	BREAK
						CASE 64		fHeading = 189.9620	BREAK
						CASE 65		fHeading = 63.7620	BREAK
						CASE 66		fHeading = 85.0000	BREAK
						// All the rest (53)
						CASE 67		fHeading = 21.9920	BREAK
						CASE 68		fHeading = 139.6800	BREAK
						CASE 69		fHeading = 99.0920	BREAK
						CASE 70		fHeading = 175.0670	BREAK
						CASE 71		fHeading = 284.1920	BREAK
						CASE 72		fHeading = 272.1920	BREAK
						CASE 73		fHeading = 156.2910	BREAK
						CASE 74		fHeading = 18.7910	BREAK
						CASE 75		fHeading = 117.9910	BREAK
						CASE 76		fHeading = 360.0000	BREAK
						CASE 77		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 78		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 79		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 80		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
//						CASE 81		fHeading = 225.5460	BREAK
						CASE 82		fHeading = 239.4200	BREAK
						CASE 83		fHeading = 116.9940	BREAK
						CASE 84		fHeading = 353.9940	BREAK
						CASE 85		fHeading = 136.1930	BREAK
						CASE 86		fHeading = 3.1930	BREAK
						CASE 87		fHeading = 242.5930	BREAK
						CASE 88		fHeading = 90.3920	BREAK
						CASE 89		fHeading = 278.7670	BREAK
						CASE 90		fHeading = 261.9170	BREAK
						CASE 91		fHeading = 148.5920	BREAK
						CASE 92		fHeading = 83.7920	BREAK
						CASE 93		fHeading = 218.7910	BREAK
						CASE 94		fHeading = 23.5920	BREAK
						CASE 95		fHeading = 199.9920	BREAK
						CASE 96		fHeading = 315.9910	BREAK
						CASE 97		fHeading = -10.4850	BREAK
						CASE 98		fHeading = -46.8000	BREAK
						CASE 99		fHeading = 167.4640	BREAK
						CASE 100	fHeading = 111.9890	BREAK
						CASE 101	fHeading = 287.0390	BREAK
						CASE 102	fHeading = 266.2640	BREAK
						CASE 103	fHeading = 134.8380	BREAK
						CASE 104	fHeading = 19.7130	BREAK
						CASE 105	fHeading = 350.3880	BREAK
						CASE 106	fHeading = 156.6880	BREAK
						CASE 107	fHeading = 231.3880	BREAK
						CASE 108	fHeading = 56.5880	BREAK
						CASE 109	fHeading = 288.3170	BREAK
						CASE 110	fHeading = 136.5150	BREAK
						CASE 111	fHeading = 324.9910	BREAK
						CASE 112	fHeading = 305.2800	BREAK
						CASE 113	fHeading = 6.7000	BREAK
						CASE 114	fHeading = 68.3170	BREAK
						CASE 115	fHeading = 253.7910	BREAK
						CASE 116	fHeading = 66.7910	BREAK
						CASE 117	fHeading = 212.0660	BREAK
						CASE 118	fHeading = 93.7910	BREAK
						CASE 119	fHeading = 344.7910	BREAK
						// Special peds (5)
						CASE 120	fHeading = 89.0000	BREAK
						CASE 121	fHeading = 268.9990	BREAK
						CASE 122	fHeading = 178.7980	BREAK
						CASE 123	fHeading = 314.0230	BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Sports Betting
						CASE 39		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()		BREAK
						CASE 40		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()		BREAK
						CASE 41		fHeading = GET_INSIDE_TRACK_CHAIR_HEADING()		BREAK
						CASE 42		fHeading = -88.5600	BREAK
						CASE 43		fHeading = 77.0400	BREAK
						CASE 44		fHeading = -49.6800	BREAK
						CASE 45		fHeading = 97.9200	BREAK
						CASE 46		fHeading = 209.5200	BREAK
						CASE 47		fHeading = 278.6400	BREAK
						CASE 48		fHeading = 25.3860	BREAK
						CASE 49		fHeading = 58.0360	BREAK
						CASE 50		fHeading = 55.4400	BREAK
						CASE 51		fHeading = 171.3600	BREAK
						CASE 52		fHeading = 281.9850	BREAK
						CASE 53		fHeading = 59.0400	BREAK
						// Behind Bar (13)
						CASE 54		fHeading = 48.1230	BREAK
						CASE 55		fHeading = 57.3410	BREAK
						CASE 56		fHeading = 160.0160	BREAK
						CASE 57		fHeading = -206.6400	BREAK
						CASE 58		fHeading = -22.7150	BREAK
						CASE 59		fHeading = 31.4660	BREAK
						CASE 60		fHeading = 288.3630	BREAK
						CASE 61		fHeading = 341.5550	BREAK
						CASE 62		fHeading = 192.2400	BREAK
						CASE 63		fHeading = 87.8630	BREAK
						CASE 64		fHeading = 138.2370	BREAK
						CASE 65		fHeading = -18.5630	BREAK
						CASE 66		fHeading = 85.0000	BREAK
						// All the rest (53)
						CASE 67		fHeading = 38.8800	BREAK
						CASE 68		fHeading = 117.3600	BREAK
						CASE 69		fHeading = 56.8800	BREAK
						CASE 70		fHeading = 215.000	BREAK
						CASE 71		fHeading = 316.800	BREAK
						CASE 72		fHeading = 27.3600	BREAK
						CASE 73		fHeading = 211.5410	BREAK
						CASE 74		fHeading = 42.8660	BREAK
						CASE 75		fHeading = 74.8800	BREAK
						CASE 76		fHeading = -92.8800	BREAK
						CASE 77		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 78		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 79		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 80		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
//						CASE 81		fHeading = 225.5460	BREAK
						CASE 82		fHeading = 271.6200	BREAK
						CASE 83		fHeading = 164.8800	BREAK
						CASE 84		fHeading = 339.9940	BREAK
						CASE 85		fHeading = 136.1930	BREAK
						CASE 86		fHeading = 3.1930	BREAK
						CASE 87		fHeading = 242.5930	BREAK
						CASE 88		fHeading = 90.3920	BREAK
						CASE 89		fHeading = -18.0000	BREAK
						CASE 90		fHeading = 2.8800	BREAK
						CASE 91		fHeading = 188.9920	BREAK
						CASE 92		fHeading = 23.7600	BREAK
						CASE 93		fHeading = 269.2660	BREAK
						CASE 94		fHeading = -141.1200	BREAK
						CASE 95		fHeading = 223.9920	BREAK
						CASE 96		fHeading = 315.9910	BREAK
						CASE 97		fHeading = -10.4850	BREAK
						CASE 98		fHeading = -44.7500	BREAK
						CASE 99		fHeading = 167.4640	BREAK
						CASE 100	fHeading = 111.9890	BREAK
						CASE 101	fHeading = 9.3600	BREAK
						CASE 102	fHeading = -8.6400	BREAK
						CASE 103	fHeading = -164.8800	BREAK
						CASE 104	fHeading = -105.1200	BREAK
						CASE 105	fHeading = 324.8380	BREAK
						CASE 106	fHeading = 125.8880	BREAK
						CASE 107	fHeading = 213.1380	BREAK
						CASE 108	fHeading = -34.5620	BREAK
						CASE 109	fHeading = 287.8920	BREAK
						CASE 110	fHeading = -121.8700	BREAK
						CASE 111	fHeading = 144.0000		BREAK
						CASE 112	fHeading = 6.5750		BREAK
						CASE 113	fHeading = -138.2400	BREAK
						CASE 114	fHeading = 82.0420		BREAK
						CASE 115	fHeading = 253.7910		BREAK
						CASE 116	fHeading = 39.3160		BREAK
						CASE 117	fHeading = 155.2160		BREAK
						CASE 118	fHeading = -100.8000	BREAK
						CASE 119	fHeading = 72.7200		BREAK
						// Special peds (5)
						CASE 120	fHeading = 89.0000	BREAK
						CASE 121	fHeading = 268.9990	BREAK
						CASE 122	fHeading = 178.7980	BREAK
						CASE 123	fHeading = 314.0230	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_AREA_MANAGERS_OFFICE
			SWITCH iLayout
				CASE 1
					SWITCH iPedID
						// Entrance (15)
						CASE 39		fHeading = 128.6750	BREAK
						CASE 40		fHeading = -90.0000	BREAK
						CASE 41		fHeading = 17.0750	BREAK
						CASE 42		fHeading = 251.9150	BREAK
						// Managers
						CASE 43		fHeading = GET_MANAGERS_HEADING()	BREAK
						CASE 44		fHeading = 177.5690	BREAK
						CASE 45		fHeading = 320.0690	BREAK
						CASE 46		fHeading = 128.1600	BREAK
						CASE 47		fHeading = -44.6400	BREAK
						CASE 48		fHeading = 258.1180	BREAK
						CASE 49		fHeading = 113.5670	BREAK
						CASE 50		fHeading = 326.9420	BREAK
						CASE 51		fHeading = 340.7690	BREAK
						CASE 52		fHeading = 120.5930	BREAK
						CASE 53		fHeading = 207.5930	BREAK
						// Behind Bar (13)
						CASE 54		fHeading = 78.5230	BREAK
						CASE 55		fHeading = 57.3410	BREAK
						CASE 56		fHeading = 160.0160	BREAK
						CASE 57		fHeading = 14.1160	BREAK
						CASE 58		fHeading = 200.0160	BREAK
						CASE 59		fHeading = 31.4660	BREAK
						CASE 60		fHeading = 232.3880	BREAK
						CASE 61		fHeading = 323.2800	BREAK
						CASE 62		fHeading = 37.8800	BREAK
						CASE 63		fHeading = 74.3880	BREAK
						CASE 64		fHeading = 189.9620	BREAK
						CASE 65		fHeading = 63.7620	BREAK
						CASE 66		fHeading = 85.0000	BREAK
						// All the rest (53)
						CASE 67		fHeading = 21.9920	BREAK
						CASE 68		fHeading = 139.6800	BREAK
						CASE 69		fHeading = 99.0920	BREAK
						CASE 70		fHeading = 175.0670	BREAK
						CASE 71		fHeading = 284.1920	BREAK
						CASE 72		fHeading = 272.1920	BREAK
						CASE 73		fHeading = 156.2910	BREAK
						CASE 74		fHeading = 18.7910	BREAK
						CASE 75		fHeading = 117.9910	BREAK
						CASE 76		fHeading = 360.0000	BREAK
						CASE 77		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 78		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 79		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 80		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
//						CASE 81		fHeading = 225.5460	BREAK
						CASE 82		fHeading = 239.4200	BREAK
						CASE 83		fHeading = 116.9940	BREAK
						CASE 84		fHeading = 353.9940	BREAK
						CASE 85		fHeading = 136.1930	BREAK
						CASE 86		fHeading = 3.1930	BREAK
						CASE 87		fHeading = 242.5930	BREAK
						CASE 88		fHeading = 90.3920	BREAK
						CASE 89		fHeading = 278.7670	BREAK
						CASE 90		fHeading = 261.9170	BREAK
						CASE 91		fHeading = 148.5920	BREAK
						CASE 92		fHeading = 83.7920	BREAK
						CASE 93		fHeading = 218.7910	BREAK
						CASE 94		fHeading = 23.5920	BREAK
						CASE 95		fHeading = 199.9920	BREAK
						CASE 96		fHeading = 315.9910	BREAK
						CASE 97		fHeading = -10.4850	BREAK
						CASE 98		fHeading = -46.8000	BREAK
						CASE 99		fHeading = 167.4640	BREAK
						CASE 100	fHeading = 111.9890	BREAK
						CASE 101	fHeading = 287.0390	BREAK
						CASE 102	fHeading = 266.2640	BREAK
						CASE 103	fHeading = 134.8380	BREAK
						CASE 104	fHeading = 19.7130	BREAK
						CASE 105	fHeading = 350.3880	BREAK
						CASE 106	fHeading = 156.6880	BREAK
						CASE 107	fHeading = 231.3880	BREAK
						CASE 108	fHeading = 56.5880	BREAK
						CASE 109	fHeading = 288.3170	BREAK
						CASE 110	fHeading = 136.5150	BREAK
						CASE 111	fHeading = 324.9910	BREAK
						CASE 112	fHeading = 305.2800	BREAK
						CASE 113	fHeading = 6.7000	BREAK
						CASE 114	fHeading = 68.3170	BREAK
						CASE 115	fHeading = 253.7910	BREAK
						CASE 116	fHeading = 66.7910	BREAK
						CASE 117	fHeading = 212.0660	BREAK
						CASE 118	fHeading = 93.7910	BREAK
						CASE 119	fHeading = 344.7910	BREAK
						// Special peds (5)
						CASE 120	fHeading = 89.0000	BREAK
						CASE 121	fHeading = 268.9990	BREAK
						CASE 122	fHeading = 178.7980	BREAK
						CASE 123	fHeading = 314.0230	BREAK
					ENDSWITCH
				BREAK
				CASE 2
					SWITCH iPedID
						// Entrance (15)
						CASE 39		fHeading = 168.6750	BREAK
						CASE 40		fHeading = 52.1500	BREAK
						CASE 41		fHeading = 31.6800	BREAK
						CASE 42		fHeading = 159.1200	BREAK
						// Managers
						CASE 43		fHeading = GET_MANAGERS_HEADING()	BREAK
						CASE 44		fHeading = 177.5690	BREAK
						CASE 45		fHeading = 210.2400	BREAK
						CASE 46		fHeading = 150.1850	BREAK
						CASE 47		fHeading = -44.6400	BREAK
						CASE 48		fHeading = -42.6400	BREAK
						CASE 49		fHeading = 113.5670	BREAK
						CASE 50		fHeading = 50.5600	BREAK
						CASE 51		fHeading = 69.1200	BREAK
						CASE 52		fHeading = 134.3680	BREAK
						CASE 53		fHeading = 257.6180	BREAK
						// Behind Bar (13)
						CASE 54		fHeading = 48.1230	BREAK
						CASE 55		fHeading = 57.3410	BREAK
						CASE 56		fHeading = 160.0160	BREAK
						CASE 57		fHeading = -206.6400	BREAK
						CASE 58		fHeading = -22.7150	BREAK
						CASE 59		fHeading = 31.4660	BREAK
						CASE 60		fHeading = 288.3630	BREAK
						CASE 61		fHeading = 341.5550	BREAK
						CASE 62		fHeading = 192.2400	BREAK
						CASE 63		fHeading = 87.8630	BREAK
						CASE 64		fHeading = 138.2370	BREAK
						CASE 65		fHeading = -18.5630	BREAK
						CASE 66		fHeading = 85.0000	BREAK
						// All the rest (53)
						CASE 67		fHeading = 38.8800	BREAK
						CASE 68		fHeading = 117.3600	BREAK
						CASE 69		fHeading = 56.8800	BREAK
						CASE 70		fHeading = 215.000	BREAK
						CASE 71		fHeading = 316.800	BREAK
						CASE 72		fHeading = 27.3600	BREAK
						CASE 73		fHeading = 211.5410	BREAK
						CASE 74		fHeading = 42.8660	BREAK
						CASE 75		fHeading = 74.8800	BREAK
						CASE 76		fHeading = -92.8800	BREAK
						CASE 77		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 78		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 79		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
						CASE 80		fHeading = GET_SLOT_MACHINE_PED_HEADING(iPedID)	BREAK
//						CASE 81		fHeading = 225.5460	BREAK
						CASE 82		fHeading = 271.6200	BREAK
						CASE 83		fHeading = 164.8800	BREAK
						CASE 84		fHeading = 339.9940	BREAK
						CASE 85		fHeading = 136.1930	BREAK
						CASE 86		fHeading = 3.1930	BREAK
						CASE 87		fHeading = 242.5930	BREAK
						CASE 88		fHeading = 90.3920	BREAK
						CASE 89		fHeading = -18.0000	BREAK
						CASE 90		fHeading = 2.8800	BREAK
						CASE 91		fHeading = 188.9920	BREAK
						CASE 92		fHeading = 23.7600	BREAK
						CASE 93		fHeading = 269.2660	BREAK
						CASE 94		fHeading = -141.1200	BREAK
						CASE 95		fHeading = 223.9920	BREAK
						CASE 96		fHeading = 315.9910	BREAK
						CASE 97		fHeading = -10.4850	BREAK
						CASE 98		fHeading = -44.7500	BREAK
						CASE 99		fHeading = 167.4640	BREAK
						CASE 100	fHeading = 111.9890	BREAK
						CASE 101	fHeading = 9.3600	BREAK
						CASE 102	fHeading = -8.6400	BREAK
						CASE 103	fHeading = -164.8800	BREAK
						CASE 104	fHeading = -105.1200	BREAK
						CASE 105	fHeading = 324.8380	BREAK
						CASE 106	fHeading = 125.8880	BREAK
						CASE 107	fHeading = 213.1380	BREAK
						CASE 108	fHeading = -34.5620	BREAK
						CASE 109	fHeading = 287.8920	BREAK
						CASE 110	fHeading = -121.8700	BREAK
						CASE 111	fHeading = 144.0000		BREAK
						CASE 112	fHeading = 6.5750		BREAK
						CASE 113	fHeading = -138.2400	BREAK
						CASE 114	fHeading = 82.0420		BREAK
						CASE 115	fHeading = 253.7910		BREAK
						CASE 116	fHeading = 39.3160		BREAK
						CASE 117	fHeading = 155.2160		BREAK
						CASE 118	fHeading = -100.8000	BREAK
						CASE 119	fHeading = 72.7200		BREAK
						// Special peds (5)
						CASE 120	fHeading = 89.0000	BREAK
						CASE 121	fHeading = 268.9990	BREAK
						CASE 122	fHeading = 178.7980	BREAK
						CASE 123	fHeading = 314.0230	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDebugPrints
		PRINTLN("[CASINO_PEDS] GET_CASINO_PED_HEADING - Ped Coords: ", fHeading)
	ENDIF
	#ENDIF
	
	RETURN fHeading
ENDFUNC


FUNC BOOL GET_CASINO_PED_VOICE_NAME(CASINO_PED_TYPES ePeds, TEXT_LABEL_63 &tl63PVGName)
	tl63PVGName = ""
	SWITCH ePeds
		CASE CASINO_SECURITY_GUARD_1	tl63PVGName = "S_M_M_BOUNCER_01_BLACK_FULL_01" RETURN TRUE BREAK
		CASE CASINO_SECURITY_GUARD_2	tl63PVGName = "S_M_M_GENERICSECURITY_01_WHITE_MINI_01" RETURN TRUE BREAK
		CASE CASINO_SECURITY_GUARD_3	tl63PVGName = "S_M_M_BOUNCER_01_LATINO_FULL_01" RETURN TRUE BREAK
		CASE CASINO_SECURITY_GUARD_4	tl63PVGName = "S_M_M_GENERICSECURITY_01_WHITE_MINI_01" RETURN TRUE BREAK
		CASE CASINO_SECURITY_GUARD_5	tl63PVGName = "S_M_M_GENERICSECURITY_01_WHITE_MINI_01" RETURN TRUE BREAK
		CASE CASINO_SECURITY_GUARD_6	tl63PVGName = "S_M_M_BOUNCER_01_LATINO_FULL_01" RETURN TRUE BREAK
		CASE CASINO_DEALER_MALE_1		tl63PVGName = "S_M_Y_Casino_01_WHITE_01" RETURN TRUE BREAK
		CASE CASINO_DEALER_MALE_2		tl63PVGName = "S_M_Y_Casino_01_ASIAN_01" RETURN TRUE BREAK
		CASE CASINO_DEALER_MALE_3		tl63PVGName = "S_M_Y_Casino_01_ASIAN_02" RETURN TRUE BREAK
		CASE CASINO_DEALER_MALE_4		tl63PVGName = "S_M_Y_Casino_01_ASIAN_01" RETURN TRUE BREAK
		CASE CASINO_DEALER_MALE_5		tl63PVGName = "S_M_Y_Casino_01_WHITE_01" RETURN TRUE BREAK
		CASE CASINO_DEALER_MALE_6		tl63PVGName = "S_M_Y_Casino_01_WHITE_02" RETURN TRUE BREAK
		CASE CASINO_DEALER_MALE_7		tl63PVGName = "S_M_Y_Casino_01_WHITE_01" RETURN TRUE BREAK
		CASE CASINO_DEALER_FEMALE_1		tl63PVGName = "S_F_Y_Casino_01_ASIAN_01" RETURN TRUE BREAK
		CASE CASINO_DEALER_FEMALE_2		tl63PVGName = "S_F_Y_Casino_01_ASIAN_02" RETURN TRUE BREAK
		CASE CASINO_DEALER_FEMALE_3		tl63PVGName = "S_F_Y_Casino_01_ASIAN_01" RETURN TRUE BREAK
		CASE CASINO_DEALER_FEMALE_4		tl63PVGName = "S_F_Y_Casino_01_ASIAN_02" RETURN TRUE BREAK
		CASE CASINO_DEALER_FEMALE_5		tl63PVGName = "S_F_Y_Casino_01_LATINA_01" RETURN TRUE BREAK
		CASE CASINO_DEALER_FEMALE_6		tl63PVGName = "S_F_Y_Casino_01_LATINA_02" RETURN TRUE BREAK
		CASE CASINO_DEALER_FEMALE_7		tl63PVGName = "S_F_Y_Casino_01_LATINA_01" RETURN TRUE BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

#ENDIF // FEATURE_CASINO
