USING "rage_builtins.sch" 
USING "globals.sch"
USING "net_prints.sch"
USING "net_stat_system.sch"
USING "commands_debug.sch"
USING "script_clock.sch"

CONST_INT NUMBER_OF_TIMESTAMPS_STORED_FOR_CAR_SALES 20

CONST_INT MAX_EXPLOIT_LEVEL 7

#IF IS_DEBUG_BUILD
STRUCT PV_LIMITER_DEBUG

	// player stats
	INT iExploitLevel
	INT iPeakExploitLevel
	INT iCurrentExploitSeason
	INT iCurrentTUSeason
	INT iPreviousSeasonExploitLevel
	INT iPreviousSeason2ExploitLevel
	INT iPreviousSeason3ExploitLevel
	INT iPreviousSeason4ExploitLevel
	INT iNumCarsSoldToday
	
	// character stats
	INT iThresholdLevel
	INT iCarsSoldInCurrentThreshold
	INT iLastCarSoldIndex
	
	// server authorative
	INT iSeasonToClear
	BOOL bOptOutOfSystem
	BOOL bOptInLowRepeatOffender
	BOOL bOptInHighRepeatOffender
	
	BOOL bPretendTryToSellACar

ENDSTRUCT

PROC FORCE_PV_LIMITER_DEBUG_UPDATE_NOW()
	MPGlobalsAmbience.bForcePVLimterDebugUpdateNow = TRUE
ENDPROC
#ENDIF

FUNC INT GET_CAP_FOR_EXPLOIT_LEVEL(INT iExploitLevel)

	IF (MPGlobalsAmbience.bGET_CAP_FOR_EXPLOIT_LEVEL_BGOverride)
		PRINTLN("[Rate_Limiter] GET_CAP_FOR_EXPLOIT_LEVEL - iGET_CAP_FOR_EXPLOIT_LEVEL_BGValue  = ", MPGlobalsAmbience.iGET_CAP_FOR_EXPLOIT_LEVEL_BGValue)
		RETURN MPGlobalsAmbience.iGET_CAP_FOR_EXPLOIT_LEVEL_BGValue
	ENDIF

	SWITCH iExploitLevel
		CASE 0 RETURN g_sMPTunables.iCap_for_exploit_level_0
		CASE 1 RETURN g_sMPTunables.iCap_for_exploit_level_1
		CASE 2 RETURN g_sMPTunables.iCap_for_exploit_level_2
		CASE 3 RETURN g_sMPTunables.iCap_for_exploit_level_3
		CASE 4 RETURN g_sMPTunables.iCap_for_exploit_level_4
		CASE 5 RETURN g_sMPTunables.iCap_for_exploit_level_5
		CASE 6 RETURN g_sMPTunables.iCap_for_exploit_level_6
		CASE 7 RETURN g_sMPTunables.iCap_for_exploit_level_7
	ENDSWITCH
	RETURN 0
ENDFUNC


FUNC BOOL IS_REPEAT_OFFENDER_1()

	IF (MPGlobalsAmbience.bIS_REPEAT_OFFENDER_1_BGOverride)
		PRINTLN("[Rate_Limiter] IS_REPEAT_OFFENDER_1 - bIS_REPEAT_OFFENDER_1_BGValue  = ", MPGlobalsAmbience.bIS_REPEAT_OFFENDER_1_BGValue)
		RETURN MPGlobalsAmbience.bIS_REPEAT_OFFENDER_1_BGValue
	ENDIF

	INT iCurrentPeak = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL)
	
	IF (iCurrentPeak <  g_sMPTunables.iRepeatOffenderMinExploitLevel1)
		RETURN(FALSE)
	ENDIF
	
	INT iPrevious[4]
	INT i
	
	iPrevious[0] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASONEXPLOITLEVEL)
	iPrevious[1] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON2EXPLOITLEVEL)
	iPrevious[2] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON3EXPLOITLEVEL)
	iPrevious[3] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON4EXPLOITLEVEL)

	REPEAT (g_sMPTunables.iRepeatOffenderNumSeasons1-1) i
		IF (iPrevious[i] < g_sMPTunables.iRepeatOffenderMinExploitLevel1)
			RETURN(FALSE)
		ENDIF
	ENDREPEAT
	
	RETURN(TRUE)
	
ENDFUNC

FUNC BOOL IS_REPEAT_OFFENDER_2()
	
	IF (MPGlobalsAmbience.bIS_REPEAT_OFFENDER_2_BGOverride)
		PRINTLN("[Rate_Limiter] IS_REPEAT_OFFENDER_2 - bIS_REPEAT_OFFENDER_1_BGValue  = ", MPGlobalsAmbience.bIS_REPEAT_OFFENDER_2_BGValue)
		RETURN MPGlobalsAmbience.bIS_REPEAT_OFFENDER_2_BGValue
	ENDIF	
	
	INT iCurrentPeak = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL)
	
	IF (iCurrentPeak <  g_sMPTunables.iRepeatOffenderMinExploitLevel2)
		RETURN(FALSE)
	ENDIF
		
	
	INT iPrevious[4]
	INT i
	
	iPrevious[0] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASONEXPLOITLEVEL)
	iPrevious[1] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON2EXPLOITLEVEL)
	iPrevious[2] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON3EXPLOITLEVEL)
	iPrevious[3] = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON4EXPLOITLEVEL)

	REPEAT (g_sMPTunables.iRepeatOffenderNumSeasons2-1) i
		IF (iPrevious[i] < g_sMPTunables.iRepeatOffenderMinExploitLevel2)
			RETURN(FALSE)
		ENDIF
	ENDREPEAT
	
	RETURN(TRUE)
	
ENDFUNC

PROC SET_PLAYERS_EXPLOIT_LEVEL(INT iNewValue)

	IF (MPGlobalsAmbience.bSET_PLAYERS_EXPLOIT_LEVEL_BGOverride)
		PRINTLN("[Rate_Limiter] SET_PLAYERS_EXPLOIT_LEVEL - iSET_PLAYERS_EXPLOIT_LEVEL_BGValue  = ", MPGlobalsAmbience.iSET_PLAYERS_EXPLOIT_LEVEL_BGValue)
		iNewValue = MPGlobalsAmbience.iSET_PLAYERS_EXPLOIT_LEVEL_BGValue
	ENDIF

	PRINTLN("[Rate_Limiter] SET_PLAYERS_EXPLOIT_LEVEL - ", iNewValue)
	
	INT iOldValue = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_EXPLOIT_LEVEL)

	PRINTLN("[Rate_Limiter] SET_PLAYERS_EXPLOIT_LEVEL - old level ", iOldValue)

	SET_MP_INT_PLAYER_STAT(MPPLY_CHAR_EXPLOIT_LEVEL, iNewValue)
	
	// is new value greater than the current peak value?
	INT iPeakValue = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL)
	IF (iNewValue > iPeakValue)
	AND (iOldValue < iNewValue) // only store peak if we are going up an exploit level, ignore decays.
		PRINTLN("[Rate_Limiter] SET_PLAYERS_EXPLOIT_LEVEL - setting new peak exploit level ", iNewValue)
		SET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL, iNewValue)
	ENDIF
	
ENDPROC

PROC SET_PLAYERS_THRESHOLD_LEVEL(INT iNewValue)

	IF (MPGlobalsAmbience.bSET_PLAYERS_THRESHOLD_LEVEL_BGOverride)
		PRINTLN("[Rate_Limiter] SET_PLAYERS_THRESHOLD_LEVEL - iSET_PLAYERS_THRESHOLD_LEVEL_BGValue  = ", MPGlobalsAmbience.iSET_PLAYERS_THRESHOLD_LEVEL_BGValue)
		iNewValue = MPGlobalsAmbience.iSET_PLAYERS_THRESHOLD_LEVEL_BGValue
	ENDIF

	INT iSlot = GET_SLOT_NUMBER(-1)
	
	PRINTLN("[Rate_Limiter] SET_PLAYERS_THRESHOLD_LEVEL - ", iNewValue, ", iSlot = ", iSlot)	
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_NUM_THRESH_CROSSED_TODAY, iNewValue)
	
	IF (iSlot = 0)
		iNewValue += 1000 
	ENDIF
	
	PRINTLN("[Rate_Limiter] SET_PLAYERS_THRESHOLD_LEVEL - setting reset to ", iNewValue)	
	
	SET_MP_INT_PLAYER_STAT(MPPLY_THRESHOLD_RESET, iNewValue+1)
	
ENDPROC


#IF IS_DEBUG_BUILD
PROC NET_PRINT_STATDATE(STRUCT_STAT_DATE &DateToPrint)
	
	// date
	NET_PRINT_INT(DateToPrint.Day)
	NET_PRINT("/")	
	NET_PRINT_INT(DateToPrint.Month)
	NET_PRINT("/")
	NET_PRINT_INT(DateToPrint.Year)
	
	NET_PRINT(" ")
	
	// time
	IF (DateToPrint.Hour < 10)
		NET_PRINT_INT(0)
	ENDIF
	NET_PRINT_INT(DateToPrint.Hour)
	NET_PRINT(":")
	IF (DateToPrint.Minute < 10)
		NET_PRINT_INT(0)
	ENDIF
	NET_PRINT_INT(DateToPrint.Minute)
	NET_PRINT(":")
	IF (DateToPrint.Seconds < 10)
		NET_PRINT_INT(0)
	ENDIF
	NET_PRINT_INT(DateToPrint.Seconds)
//	NET_PRINT(".")	
//	NET_PRINT_INT(DateToPrint.Milliseconds)
	NET_PRINT(" ")	

ENDPROC
#ENDIF 

PROC SET_LAST_TIME_THRESHOLD_CROSSED(STRUCT_STAT_DATE &DateValue)

	IF (MPGlobalsAmbience.bSET_LAST_TIME_THRESHOLD_CROSSED_BGOverride)
		#IF IS_DEBUG_BUILD
		NET_PRINT("[Rate_Limiter] SET_LAST_TIME_THRESHOLD_CROSSED - DateSET_LAST_TIME_THRESHOLD_CROSSED  = ")
		NET_PRINT_STATDATE(MPGlobalsAmbience.DateSET_LAST_TIME_THRESHOLD_CROSSED)
		NET_NL()
		#ENDIF
		DateValue = MPGlobalsAmbience.DateSET_LAST_TIME_THRESHOLD_CROSSED
	ENDIF

	#IF IS_DEBUG_BUILD
	NET_PRINT("[Rate_Limiter] SET_LAST_TIME_THRESHOLD_CROSSED - ")
	NET_PRINT_STATDATE(DateValue)
	NET_NL()
	#ENDIF
	SET_MP_DATE_PLAYER_STAT(MPPLY_LAST_TIME_THRESH_CROSS, DateValue)
	
	#IF IS_DEBUG_BUILD
		FORCE_PV_LIMITER_DEBUG_UPDATE_NOW()
	#ENDIF	
ENDPROC

FUNC BOOL IS_LEAP_YEAR(INT iYear)
	IF (iYear%4 = 0)
		IF (iYear%100=0)
			IF (iYear%400=0)
				RETURN(TRUE)
			ELSE
				RETURN(FALSE)
			ENDIF
		ELSE
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC INT DAYS_IN_YEAR(INT iYear)
	IF IS_LEAP_YEAR(iYear)
		RETURN(366)
	ENDIF
	RETURN(365)
ENDFUNC

PROC CONVERT_MINS_SINCE_2000_TO_STATDATE(INT iTotalMins, STRUCT_STAT_DATE &ReturnDate)

	// firstly convert to days, hours, mins
	INT iDays = FLOOR(TO_FLOAT(iTotalMins)/ 1440.0)
	INT iHours = FLOOR(TO_FLOAT(iTotalMins%1440) / 60.0)
	INT iMins = (iTotalMins%1440)%60
	
	// now work out how many years to add to 2000
	INT iYearsToAdd
	WHILE (iDays >= DAYS_IN_YEAR(2000 + iYearsToAdd))
		IF IS_LEAP_YEAR(2000 + iYearsToAdd)	
			iDays -= 366
		ELSE
			iDays -= 365
		ENDIF
		iYearsToAdd += 1
	ENDWHILE
	
	
	// work out how many months to add ( to january, so iMonthToAdd = 1 would be feburary)
	INT iMonthsToAdd
	WHILE (iDays >= GET_NUMBER_OF_DAYS_IN_MONTH(INT_TO_ENUM(MONTH_OF_YEAR, iMonthsToAdd), (2000 + iYearsToAdd)))
		iDays -= GET_NUMBER_OF_DAYS_IN_MONTH(INT_TO_ENUM(MONTH_OF_YEAR, iMonthsToAdd), (2000 + iYearsToAdd))
		iMonthsToAdd += 1
	ENDWHILE
	
	// we should now have the final date
	ReturnDate.Year = (2000 + iYearsToAdd)
	ReturnDate.Month = 1 + iMonthsToAdd
	ReturnDate.Day = 1 + iDays
	ReturnDate.Hour = iHours
	ReturnDate.Minute = iMins
	
//	#IF IS_DEBUG_BUILD
//	NET_PRINT("[Rate_Limiter] CONVERT_MINS_SINCE_2000_TO_STATDATE - iTotalMins = ")
//	NET_PRINT_INT(iTotalMins)
//	NET_PRINT(", Returning date:  ")
//	NET_PRINT_STATDATE(ReturnDate)
//	NET_NL()
//	#ENDIF	
	
ENDPROC


FUNC INT CONVERT_STATDATE_TO_MINS_SINCE_2000(STRUCT_STAT_DATE &Date)
	
	INT iYear = Date.Year - 2000
	INT iTotalMins = 0
	INT i
	
	// add previous years
	IF (iYear > 0)
		REPEAT iYear i
			IF IS_LEAP_YEAR(i)
				iTotalMins += (60*24*366)
			ELSE
				iTotalMins += (60*24*365)	
			ENDIF
		ENDREPEAT
	ENDIF
	
	// add previous months of this year 
	IF (Date.Month > 0)
		REPEAT (Date.Month-1) i
			iTotalMins += GET_NUMBER_OF_DAYS_IN_MONTH(INT_TO_ENUM(MONTH_OF_YEAR, i), iYear) * 60 * 24
		ENDREPEAT
	ENDIF
	
	// add the previous days
	IF (Date.Day > 0)
		iTotalMins += (Date.Day-1) * 60 *24
	ENDIF
	
	// add the current hours
	iTotalMins += Date.Hour * 60

	// add the current mins
	iTotalMins += Date.Minute

	RETURN(iTotalMins)

ENDFUNC


PROC SET_REPEAT_OFFENDER_TIME_1(STRUCT_STAT_DATE &DateValue)

	IF (MPGlobalsAmbience.bSET_REPEAT_OFFENDER_TIME_1_BGOverride)
		#IF IS_DEBUG_BUILD
		NET_PRINT("[Rate_Limiter] SET_REPEAT_OFFENDER_TIME_1 - DateSET_REPEAT_OFFENDER_TIME_1  = ")
		NET_PRINT_STATDATE(MPGlobalsAmbience.DateSET_REPEAT_OFFENDER_TIME_1)
		NET_NL()
		#ENDIF
		DateValue = MPGlobalsAmbience.DateSET_REPEAT_OFFENDER_TIME_1
	ENDIF

	#IF IS_DEBUG_BUILD
	NET_PRINT("[Rate_Limiter] SET_REPEAT_OFFENDER_TIME_1 - ")
	NET_PRINT_STATDATE(DateValue)
	NET_NL()
	#ENDIF
	SET_MP_DATE_PLAYER_STAT(MPPLY_REPEAT_OFFENDER_1_TIME, DateValue)
	
	#IF IS_DEBUG_BUILD
	STRUCT_STAT_DATE LiftedDate
	NET_PRINT("[Rate_Limiter] SET_REPEAT_OFFENDER_TIME_1 - Ban will be lifted on: ")
	CONVERT_MINS_SINCE_2000_TO_STATDATE((CONVERT_STATDATE_TO_MINS_SINCE_2000(DateValue) + g_sMPTunables.iPermanentCapRepeatOffender1), LiftedDate)
	NET_PRINT_STATDATE(LiftedDate)
	NET_NL()
	#ENDIF
ENDPROC

PROC SET_REPEAT_OFFENDER_TIME_2(STRUCT_STAT_DATE &DateValue)

	IF (MPGlobalsAmbience.bSET_REPEAT_OFFENDER_TIME_2_BGOverride)
		#IF IS_DEBUG_BUILD
		NET_PRINT("[Rate_Limiter] SET_REPEAT_OFFENDER_TIME_2 - DateSET_REPEAT_OFFENDER_TIME_2  = ")
		NET_PRINT_STATDATE(MPGlobalsAmbience.DateSET_REPEAT_OFFENDER_TIME_2)
		NET_NL()
		#ENDIF
		DateValue = MPGlobalsAmbience.DateSET_REPEAT_OFFENDER_TIME_2
	ENDIF

	#IF IS_DEBUG_BUILD
	NET_PRINT("[Rate_Limiter] SET_REPEAT_OFFENDER_TIME_2 - ")
	NET_PRINT_STATDATE(DateValue)
	NET_NL()
	#ENDIF
	SET_MP_DATE_PLAYER_STAT(MPPLY_REPEAT_OFFENDER_2_TIME, DateValue)
	
	#IF IS_DEBUG_BUILD
	STRUCT_STAT_DATE LiftedDate
	NET_PRINT("[Rate_Limiter] SET_REPEAT_OFFENDER_TIME_2 - Ban will be lifted on: ")
	CONVERT_MINS_SINCE_2000_TO_STATDATE((CONVERT_STATDATE_TO_MINS_SINCE_2000(DateValue) + g_sMPTunables.iPermanentCapRepeatOffender2), LiftedDate)
	NET_PRINT_STATDATE(LiftedDate)
	NET_NL()
	#ENDIF	
ENDPROC




FUNC INT GET_MINS_BETWEEN_STATDATES(STRUCT_STAT_DATE &EarlierDate, STRUCT_STAT_DATE &LaterDate)

	INT iEarlierDateMins = CONVERT_STATDATE_TO_MINS_SINCE_2000(EarlierDate)
	INT iLaterDateMins = CONVERT_STATDATE_TO_MINS_SINCE_2000(LaterDate)
	
//	// test
//	#IF IS_DEBUG_BUILD
//	STRUCT_STAT_DATE testEarlierDate
//	STRUCT_STAT_DATE testLaterDate
//	CONVERT_MINS_SINCE_2000_TO_STATDATE(iEarlierDateMins, testEarlierDate)
//	CONVERT_MINS_SINCE_2000_TO_STATDATE(iLaterDateMins, testLaterDate)
//	
//	NET_PRINT("[Rate_Limiter] GET_MINS_BETWEEN_STATDATES - EarlierDate:")
//	NET_PRINT_STATDATE(EarlierDate)
//	NET_PRINT(" testEarlierDate :")
//	NET_PRINT_STATDATE(testEarlierDate)
//	NET_PRINT(" LaterDate :")
//	NET_PRINT_STATDATE(LaterDate)
//	NET_PRINT(" testLaterDate :")
//	NET_PRINT_STATDATE(testLaterDate)
//	NET_NL()
//	#ENDIF

	RETURN iLaterDateMins - iEarlierDateMins

ENDFUNC

FUNC INT GET_DAYS_BETWEEN_STATDATES(STRUCT_STAT_DATE &EarlierDate, STRUCT_STAT_DATE &LaterDate)

	INT iMins = GET_MINS_BETWEEN_STATDATES(EarlierDate, LaterDate)

	RETURN FLOOR(TO_FLOAT(iMins)/(60*24))
ENDFUNC


FUNC BOOL IS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE()

	IF (MPGlobalsAmbience.bIS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE_BGOverride)
		PRINTLN("[Rate_Limiter] IS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE - bIS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE_BGValue  = ", MPGlobalsAmbience.bIS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE_BGValue)
		RETURN MPGlobalsAmbience.bIS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE_BGValue
	ENDIF	

	STRUCT_STAT_DATE currentdate
	GET_UTC_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)	
		
	STRUCT_STAT_DATE storeddate
	storeddate = GET_MP_DATE_PLAYER_STAT(MPPLY_TIME_FIRST_CAR_SOLD_TODAY)

	#IF IS_DEBUG_BUILD
		NET_PRINT("[Rate_Limiter] IS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE - currentdate = ") NET_PRINT_STATDATE(currentdate) NET_NL()
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[Rate_Limiter] IS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE - storeddate = ") NET_PRINT_STATDATE(storeddate) NET_NL() 
		PRINTLN("[Rate_Limiter] IS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE - MPPLY_NUM_CARS_SOLD_TODAY = ", GET_MP_INT_PLAYER_STAT(MPPLY_NUM_CARS_SOLD_TODAY))
	#ENDIF

	INT iMinsPassed = GET_MINS_BETWEEN_STATDATES(storeddate, currentdate)
	
	PRINTLN("[Rate_Limiter] IS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE - iMinsPassed = ", iMinsPassed) 
	
	IF (iMinsPassed < g_sMPTunables.iTime_Window_For_PV_Sales_Limiter)
		PRINTLN("[Rate_Limiter] IS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE - same day returning TRUE") 
		RETURN(TRUE)
	ENDIF
	
	PRINTLN("[Rate_Limiter] IS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE - same day returning FALSE")
	RETURN(FALSE)
	
ENDFUNC

PROC CLEAR_PREVIOUS_EXPLOIT_LEVELS(INT iSeason)

	IF (MPGlobalsAmbience.bCLEAR_PREVIOUS_EXPLOIT_LEVELS_BGOverride)
		PRINTLN("[Rate_Limiter] CLEAR_PREVIOUS_EXPLOIT_LEVELS - bg override ")
		EXIT
	ENDIF	

	PRINTLN("[Rate_Limiter] CLEAR_PREVIOUS_EXPLOIT_LEVELS called with ", iSeason)
	
	SWITCH iSeason
		CASE 1
			SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASONEXPLOITLEVEL, 0)
		BREAK
		CASE 2
			SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON2EXPLOITLEVEL, 0)
		BREAK
		CASE 3
			SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON3EXPLOITLEVEL, 0)
		BREAK
		CASE 4
			SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON4EXPLOITLEVEL, 0)
		BREAK
		DEFAULT
			SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASONEXPLOITLEVEL, 0)
			SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON2EXPLOITLEVEL, 0)
			SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON3EXPLOITLEVEL, 0)
			SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON4EXPLOITLEVEL, 0)		
		BREAK
	ENDSWITCH
	
ENDPROC

PROC STORE_PREVIOUS_EXPLOIT_LEVEL(INT iValueToStore)
	
	IF (MPGlobalsAmbience.bSTORE_PREVIOUS_EXPLOIT_LEVEL_BGOverride)
		PRINTLN("[Rate_Limiter] STORE_PREVIOUS_EXPLOIT_LEVEL - bg override ")
		EXIT
	ENDIF	

	INT iPreviousValue1 = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASONEXPLOITLEVEL)
	INT iPreviousValue2 = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON2EXPLOITLEVEL)
	INT iPreviousValue3 = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON3EXPLOITLEVEL)

	PRINTLN("[Rate_Limiter] STORE_PREVIOUS_EXPLOIT_LEVEL - ", iValueToStore, ",", iPreviousValue1, ",", iPreviousValue2, ",", iPreviousValue3)

	SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASONEXPLOITLEVEL, iValueToStore)
	SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON2EXPLOITLEVEL, iPreviousValue1)
	SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON3EXPLOITLEVEL, iPreviousValue2)
	SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON4EXPLOITLEVEL, iPreviousValue3)
	
ENDPROC

FUNC BOOL IS_STAT_DATE_VALID(STRUCT_STAT_DATE &InDate)
	IF (InDate.Year != 0)
	OR (InDate.Month != 0)
	OR (InDate.Day != 0)
	OR (InDate.Hour != 0)
	OR (InDate.Minute != 0)
	OR (InDate.Seconds != 0)
	OR (InDate.Milliseconds != 0)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC CHECK_REPEAT_OFFENDER_RESTRICTIONS(BOOL & bHasJustAlteredPeak)

	IF (MPGlobalsAmbience.bCHECK_REPEAT_OFFENDER_RESTRICTIONS_BGOverride)
		PRINTLN("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - bg override ")
		EXIT
	ENDIF	

	// store if they are are repeat defender (so we dont keep calling the commands)
	BOOL bIsRepeatOffender1 = IS_REPEAT_OFFENDER_1()
	BOOL bIsRepeatOffender2 = IS_REPEAT_OFFENDER_2()

	// is it time to lift repeat offender restrictions?
	IF (bIsRepeatOffender1)
	OR (bIsRepeatOffender2)
	
		STRUCT_STAT_DATE currentdate
		GET_UTC_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)	
	
		INT iMins
	
		// has punishment time elapsed?
		BOOL bHasServedPunishment1
		BOOL bHasServedPunishment2
		
		STRUCT_STAT_DATE offenderDate1
		STRUCT_STAT_DATE offenderDate2
		STRUCT_STAT_DATE DateOfLiftedBan
		
		IF (bIsRepeatOffender1)
			offenderDate1 = GET_MP_DATE_PLAYER_STAT(MPPLY_REPEAT_OFFENDER_1_TIME)
			IF IS_STAT_DATE_VALID(offenderDate1)
				iMins = GET_MINS_BETWEEN_STATDATES(	offenderDate1, currentdate)
				PRINTLN("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - time spent as repeat offender 1 = ", iMins, " minutes")
				IF (iMins >= g_sMPTunables.iPermanentCapRepeatOffender1)
					bHasServedPunishment1 = TRUE
				ENDIF
			ELSE
				bHasServedPunishment1 = TRUE
				PRINTLN("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - repeat offender 1 time not valid.")
			ENDIF
		ENDIF
		
		IF (bIsRepeatOffender2)
			offenderDate2 = GET_MP_DATE_PLAYER_STAT(MPPLY_REPEAT_OFFENDER_2_TIME)
			IF IS_STAT_DATE_VALID(offenderDate2)
				iMins = GET_MINS_BETWEEN_STATDATES(	offenderDate2, currentdate)
				PRINTLN("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - time spent as repeat offender 2 = ", iMins, " minutes")
				IF (iMins >= g_sMPTunables.iPermanentCapRepeatOffender2)
					bHasServedPunishment2 = TRUE	
				ENDIF
			ELSE
				bHasServedPunishment2 = TRUE
				PRINTLN("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - repeat offender 2 time not valid.")
			ENDIF
		ENDIF
		
		IF (bIsRepeatOffender1) AND (bHasServedPunishment1)
			PRINTLN("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - served punishment for offence 1.")
			// check we're still not serving time for offence 2
			IF NOT (bIsRepeatOffender2)
			OR ((bIsRepeatOffender2) AND (bHasServedPunishment2))
				PRINTLN("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - served punishment for offence 1 - clearing current peak.")
				SET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL, 0)
				bHasJustAlteredPeak = TRUE
			ELSE
				// still serving time for offence 2
				PRINTLN("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - served punishment for offence 1 - still serving for offence 2.")
				SET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL, g_sMPTunables.iRepeatOffenderMinExploitLevel2)
				bHasJustAlteredPeak = TRUE
			ENDIF
			
			// when was the date the punishment expired?
			CONVERT_MINS_SINCE_2000_TO_STATDATE(CONVERT_STATDATE_TO_MINS_SINCE_2000(offenderDate1) + g_sMPTunables.iPermanentCapRepeatOffender1, DateOfLiftedBan)
			
			#IF IS_DEBUG_BUILD
			NET_PRINT("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - ban was lifted on: ")
			NET_PRINT_STATDATE(DateOfLiftedBan)
			NET_NL()
			#ENDIF
			
			SET_LAST_TIME_THRESHOLD_CROSSED(DateOfLiftedBan) // store this time as last threshold crossed so current exploit level doesn't clear immediately.
			#IF IS_DEBUG_BUILD
				FORCE_PV_LIMITER_DEBUG_UPDATE_NOW()
			#ENDIF
		ENDIF
		
		IF (bIsRepeatOffender2) AND (bHasServedPunishment2)
			PRINTLN("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - served punishment for offence 2.")
			// check we're still not serving time for offence 1
			IF NOT (bIsRepeatOffender1)
			OR ((bIsRepeatOffender1) AND (bHasServedPunishment1))
				PRINTLN("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - served punishment for offence 2 - clearing current peak.")
				SET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL, 0)
				bHasJustAlteredPeak = TRUE
			ELSE
				// still serving time for offence 1
				PRINTLN("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - served punishment for offence 2 - still serving for offence 1.")	
				SET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL, g_sMPTunables.iRepeatOffenderMinExploitLevel1)	
				bHasJustAlteredPeak = TRUE
			ENDIF
			
			// when was the date the punishment expired?
			CONVERT_MINS_SINCE_2000_TO_STATDATE(CONVERT_STATDATE_TO_MINS_SINCE_2000(offenderDate2) + g_sMPTunables.iPermanentCapRepeatOffender2, DateOfLiftedBan)
			
			#IF IS_DEBUG_BUILD
			NET_PRINT("[Rate_Limiter] CHECK_REPEAT_OFFENDER_RESTRICTIONS - ban was lifted on: ")
			NET_PRINT_STATDATE(DateOfLiftedBan)
			NET_NL()
			#ENDIF			
			
			SET_LAST_TIME_THRESHOLD_CROSSED(DateOfLiftedBan) // store this time as last threshold crossed so current exploit level doesn't clear immediately.
			#IF IS_DEBUG_BUILD
				FORCE_PV_LIMITER_DEBUG_UPDATE_NOW()
			#ENDIF
		ENDIF

	ENDIF
	
ENDPROC

PROC CHECK_CURRENT_SALES_SEASONS(INT iCurrentExploitSeason, INT iCurrentTUSeason, INT iSeasonToClear, BOOL bHasJustAlteredPeak)
	
	IF (MPGlobalsAmbience.bCHECK_CURRENT_SALES_SEASONS_BGOverride)
		PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - bg override ")
		EXIT
	ENDIF		
	
	IF (iSeasonToClear > 0)
		PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - iSeasonToClear = ", iSeasonToClear)
		CLEAR_PREVIOUS_EXPLOIT_LEVELS(iSeasonToClear)
		SET_MP_INT_PLAYER_STAT(MPPLY_SEASONTOCLEAR, 0)		
	ENDIF
	
	INT iCurrentExploitLevel
	IF NOT (iCurrentExploitSeason = g_sMPTunables.iCurrentVehicleSalesSeason)
	OR NOT (iCurrentTUSeason = g_sMPTunables.iCurrentVehicleSalesTUSeason)
		iCurrentExploitLevel = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_EXPLOIT_LEVEL)
		PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - iCurrentExploitLevel = ", iCurrentExploitLevel)
	ENDIF
	

	// exploit season
	IF NOT (iCurrentExploitSeason = g_sMPTunables.iCurrentVehicleSalesSeason)
	
		PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - different Exploit season. iCurrentExploitSeason = ", iCurrentExploitSeason, ", g_sMPTunables.iCurrentVehicleSalesSeason = ", g_sMPTunables.iCurrentVehicleSalesSeason)
	
		IF (iCurrentExploitSeason < g_sMPTunables.iCurrentVehicleSalesSeason)
			
			
			INT iCurrentExploitLevelPeak
			IF NOT (bHasJustAlteredPeak)
				iCurrentExploitLevelPeak = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL)
				PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - iCurrentExploitLevelPeak = ", iCurrentExploitLevelPeak)
			ELSE
				iCurrentExploitLevelPeak = iCurrentExploitLevel
				PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - bHasJustAlteredPeak = TRUE, iCurrentExploitLevelPeak = ", iCurrentExploitLevelPeak)
			ENDIF
			
			BOOL bIsReapeatOffender1 = IS_REPEAT_OFFENDER_1()
			BOOL bIsReapeatOffender2 = IS_REPEAT_OFFENDER_2()
			
			// store the current 'peak' level in the previous array.
			IF ((iCurrentExploitSeason+1) = g_sMPTunables.iCurrentVehicleSalesSeason)
			OR (bIsReapeatOffender1)
			OR (bIsReapeatOffender2)
				STORE_PREVIOUS_EXPLOIT_LEVEL(iCurrentExploitLevelPeak)		
			ELSE
				// we've missed some season updates, store 0 for the seasons we weren't logged in.
				PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - storing missed season exploit level as 0")
				STORE_PREVIOUS_EXPLOIT_LEVEL(0)
			ENDIF
			
			// drop current exploit level and clear peak level if we're not a repeat offender
			IF NOT bIsReapeatOffender1
			AND NOT bIsReapeatOffender2
			
				SET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL, 0)
				
				IF (iCurrentExploitLevel > g_sMPTunables.iNumExploitLevelsToDrop )
					iCurrentExploitLevel -= g_sMPTunables.iNumExploitLevelsToDrop	
					PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - dropping exploit level to ", iCurrentExploitLevel)
					SET_PLAYERS_EXPLOIT_LEVEL(iCurrentExploitLevel)
				ELIF ((iCurrentExploitLevel > 0) AND (iCurrentExploitLevel <= g_sMPTunables.iNumExploitLevelsToDrop))
					// clear exploit level and set threshold to 8 (for both chars)
					SET_PLAYERS_EXPLOIT_LEVEL(0)
					SET_PLAYERS_THRESHOLD_LEVEL(g_sMPTunables.iThresholdLevelToDropTo)
					PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - clearing exploit and setthing threshold level to ", g_sMPTunables.iThresholdLevelToDropTo)
				ENDIF
				
			ENDIF
		
			SET_MP_INT_PLAYER_STAT(MPPLY_NUM_CARS_SOLD_TODAY, 0)
			
			INCREMENT_MP_INT_PLAYER_STAT(MPPLY_CURRENTVEHSALESSEASON)
		
			// do we need to run again?
			IF ((iCurrentExploitSeason+1) != g_sMPTunables.iCurrentVehicleSalesSeason)
				PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - new seasons still don't match calling recursively.")
				CHECK_CURRENT_SALES_SEASONS((iCurrentExploitSeason+1), iCurrentTUSeason, iSeasonToClear, bHasJustAlteredPeak)
				EXIT
			ENDIF
		
		ELSE
			PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - current season is ahead, resetting.")
			SET_MP_INT_PLAYER_STAT(MPPLY_CURRENTVEHSALESSEASON, g_sMPTunables.iCurrentVehicleSalesSeason)
		ENDIF
	
	ELSE
		PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - season up to date.")	
	ENDIF
	
	// TU Season has changed
	IF NOT (iCurrentTUSeason = g_sMPTunables.iCurrentVehicleSalesTUSeason)
		PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - different TU Season. iCurrentTUSeason = ", iCurrentTUSeason, ", g_sMPTunables.iCurrentVehicleSalesTUSeason = ", g_sMPTunables.iCurrentVehicleSalesTUSeason)	

		IF (iCurrentTUSeason < g_sMPTunables.iCurrentVehicleSalesTUSeason)
			IF (iCurrentExploitLevel = 0)
				SET_PLAYERS_THRESHOLD_LEVEL(0)
				PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - resetting thresholds to zero. Different TU season.")	
			ENDIF
			SET_MP_INT_PLAYER_STAT(MPPLY_NUM_CARS_SOLD_TODAY, 0)
			INCREMENT_MP_INT_PLAYER_STAT(MPPLY_CURRENTVEHSALESTUSEASON)
			IF ((iCurrentTUSeason+1) != g_sMPTunables.iCurrentVehicleSalesTUSeason)
				PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - new TU seasons still don't match calling recursively.")
				CHECK_CURRENT_SALES_SEASONS(iCurrentExploitSeason, (iCurrentTUSeason+1), iSeasonToClear, bHasJustAlteredPeak)
				EXIT	
			ENDIF
		ELSE	
			PRINTLN("[Rate_Limiter] CHECK_CURRENT_SALES_SEASONS - current TU season is ahead, resetting.")
			SET_MP_INT_PLAYER_STAT(MPPLY_CURRENTVEHSALESTUSEASON, g_sMPTunables.iCurrentVehicleSalesTUSeason)
		ENDIF
	ENDIF
		
			
ENDPROC

PROC CHECK_THRESHOLD_LEVEL_SYNC()

	IF (MPGlobalsAmbience.bCHECK_THRESHOLD_LEVEL_SYNC_BGOverride)
		PRINTLN("[Rate_Limiter] CHECK_THRESHOLD_LEVEL_SYNC - bg override ")
		EXIT
	ENDIF	

	INT iStoredReset = GET_MP_INT_PLAYER_STAT(MPPLY_THRESHOLD_RESET)
	iStoredReset += -1
	INT iSlot = GET_SLOT_NUMBER(-1)
	INT iThisCharThreshold = GET_MP_INT_CHARACTER_STAT(MP_STAT_NUM_THRESH_CROSSED_TODAY)
	PRINTLN("[Rate_Limiter] CHECK_THRESHOLD_LEVEL_SYNC - initial iStoredReset = ", iStoredReset, ", iSlot = ", iSlot, ", iThisCharThreshold = ", iThisCharThreshold)
	IF (iStoredReset > -1)
		IF (iSlot > 0)
			iStoredReset -= 1000	
			PRINTLN("[Rate_Limiter] CHECK_THRESHOLD_LEVEL_SYNC - adjusted iStoredReset = ", iStoredReset)
		ENDIF	
		IF ((iStoredReset > -1) AND (iStoredReset < 1000))
		AND NOT (iStoredReset = iThisCharThreshold)
			PRINTLN("[Rate_Limiter] CHECK_THRESHOLD_LEVEL_SYNC setting to ", iStoredReset)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUM_THRESH_CROSSED_TODAY, iStoredReset)
		ENDIF
	ENDIF
ENDPROC

PROC RUN_PV_STATS_SYNC_UPDATE()

	IF (MPGlobalsAmbience.bRUN_PV_STATS_SYNC_UPDATE_BGOverride)
		PRINTLN("[Rate_Limiter] RUN_PV_STATS_SYNC_UPDATE - bg override ")
		EXIT
	ENDIF	
		
	PRINTLN("[Rate_Limiter] RUN_PV_STATS_SYNC_UPDATE called.")
	DEBUG_PRINTCALLSTACK()

	IF NOT (g_bHasCopiedAllTunableValuesToScriptGlobals)
		PRINTLN("[Rate_Limiter] RUN_PV_STATS_SYNC_UPDATE - g_bHasCopiedAllTunableValuesToScriptGlobals = FALSE ")
		EXIT	
	ENDIF
	
	CHECK_THRESHOLD_LEVEL_SYNC()

	BOOL bHasJustAlteredPeak

	CHECK_REPEAT_OFFENDER_RESTRICTIONS(bHasJustAlteredPeak)

	CHECK_CURRENT_SALES_SEASONS(GET_MP_INT_PLAYER_STAT(MPPLY_CURRENTVEHSALESSEASON), GET_MP_INT_PLAYER_STAT(MPPLY_CURRENTVEHSALESTUSEASON), GET_MP_INT_PLAYER_STAT(MPPLY_SEASONTOCLEAR), bHasJustAlteredPeak)
	
	#IF IS_DEBUG_BUILD
		MPGlobalsAmbience.bUpdateGetPVLimiterStats = TRUE
	#ENDIF	
ENDPROC

PROC INIT_PV_LIMITER()
	
	IF NOT (MPGlobalsAmbience.bHasInitialisedPVLimiter)
		
		IF (g_bHasCopiedAllTunableValuesToScriptGlobals)
			PRINTLN("[Rate_Limiter] INIT_PV_LIMITER - intialising... ")
			RUN_PV_STATS_SYNC_UPDATE()
			MPGlobalsAmbience.bHasInitialisedPVLimiter = TRUE			
		ELSE
			PRINTLN("[Rate_Limiter] INIT_PV_LIMITER - waiting for tunables...")
		ENDIF
	
	ENDIF
ENDPROC

FUNC BOOL CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK()

	IF (MPGlobalsAmbience.bCAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK_BGOverride)
		PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK  = ", MPGlobalsAmbience.bCAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK_BGValue)
		RETURN MPGlobalsAmbience.bCAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK_BGValue
	ENDIF

	RUN_PV_STATS_SYNC_UPDATE()

	// are caps enforced?
	IF IS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE()
		
		PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - Less than 24 hours have passed")
		
		INT iCarsSoldToday = GET_MP_INT_PLAYER_STAT(MPPLY_NUM_CARS_SOLD_TODAY)
		PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - iCarsSoldToday = ", iCarsSoldToday)

		PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - bRepeat_Offenders_Caps_Are_Enforced = ", g_sMPTunables.bRepeat_Offenders_Caps_Are_Enforced)

	
		IF (GET_MP_BOOL_PLAYER_STAT(MPPLY_OPT_OUT_OF_SYSTEM)=FALSE)

			IF (g_sMPTunables.bRepeat_Offenders_Caps_Are_Enforced) 
			AND ((IS_REPEAT_OFFENDER_1() AND (iCarsSoldToday >= g_sMPTunables.iRepeatOffenderCap1) AND (GET_MP_BOOL_PLAYER_STAT(MPPLY_LOW_REPEAT_OFFENDER_CAP)=TRUE))
				  OR (IS_REPEAT_OFFENDER_2() AND (iCarsSoldToday >= g_sMPTunables.iRepeatOffenderCap2) AND (GET_MP_BOOL_PLAYER_STAT(MPPLY_HIGH_REPEAT_OFFENDER_CAP)=TRUE) ))
				
				#IF IS_DEBUG_BUILD
					IF IS_REPEAT_OFFENDER_1()
						PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - returning FALSE - repeat offender 1")	
					ENDIF
					IF IS_REPEAT_OFFENDER_2()
						PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - returning FALSE - repeat offender 2")	
					ENDIF
				#ENDIF	
				RETURN(FALSE)	
			
			ELIF (g_sMPTunables.bCaps_are_enfored)
			
				PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - caps are enforced")
		
				INT iExploitLevel = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_EXPLOIT_LEVEL)
				PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - iExploitLevel = ", iExploitLevel)
								
				INT iCap = GET_CAP_FOR_EXPLOIT_LEVEL(iExploitLevel)
				PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - iCap = ", iCap)			
			
				IF (iCarsSoldToday >= iCap)
					PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - returning FALSE")
					RETURN(FALSE)
				ENDIF
						
			ELSE
				PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - caps not enforced")
			ENDIF
		
		ELSE
			PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - opt out of system server authorative stat set.")
		ENDIF
				
	ELSE
		PRINTLN("[Rate_Limiter] CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK - 24 hours have passed")
	ENDIF
			
	// return true by default
	RETURN(TRUE)
	
ENDFUNC




FUNC MP_DATE_STATS GET_STAT_FOR_SOLD_INDEX(INT iIndex)
	//PRINTLN("[Rate_Limiter] GET_STAT_FOR_SOLD_INDEX - iIndex ", iIndex) 
	SWITCH iIndex
		CASE 1 RETURN  MP_STAT_CAR_SOLD_TIME_1
		CASE 2 RETURN  MP_STAT_CAR_SOLD_TIME_2
		CASE 3 RETURN  MP_STAT_CAR_SOLD_TIME_3
		CASE 4 RETURN  MP_STAT_CAR_SOLD_TIME_4
		CASE 5 RETURN  MP_STAT_CAR_SOLD_TIME_5
		CASE 6 RETURN  MP_STAT_CAR_SOLD_TIME_6
		CASE 7 RETURN  MP_STAT_CAR_SOLD_TIME_7
		CASE 8 RETURN  MP_STAT_CAR_SOLD_TIME_8
		CASE 9 RETURN  MP_STAT_CAR_SOLD_TIME_9
		CASE 10 RETURN MP_STAT_CAR_SOLD_TIME_10
		CASE 11 RETURN MP_STAT_CAR_SOLD_TIME_11
		CASE 12 RETURN MP_STAT_CAR_SOLD_TIME_12
		CASE 13 RETURN MP_STAT_CAR_SOLD_TIME_13
		CASE 14 RETURN MP_STAT_CAR_SOLD_TIME_14
		CASE 15 RETURN MP_STAT_CAR_SOLD_TIME_15
		CASE 16 RETURN MP_STAT_CAR_SOLD_TIME_16
		CASE 17 RETURN MP_STAT_CAR_SOLD_TIME_17
		CASE 18 RETURN MP_STAT_CAR_SOLD_TIME_18
		CASE 19 RETURN MP_STAT_CAR_SOLD_TIME_19
		CASE 20 RETURN MP_STAT_CAR_SOLD_TIME_20
	ENDSWITCH	
	SCRIPT_ASSERT("GET_STAT_FOR_SOLD_INDEX - invalid index!")
	RETURN(MP_STAT_CAR_SOLD_TIME_1)
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_THRESHOLD_DATA()
	NET_PRINT("[Rate_Limiter] THRESHOLD DATA: ") NET_NL() 
	NET_PRINT("[Rate_Limiter] ---------------- ") NET_NL()
	STRUCT_STAT_DATE DateData
	INT i
	REPEAT NUMBER_OF_TIMESTAMPS_STORED_FOR_CAR_SALES i
		DateData = GET_MP_DATE_CHARACTER_STAT(GET_STAT_FOR_SOLD_INDEX(i+1))
		NET_PRINT("[Rate_Limiter] MP_STAT_CAR_SOLD_TIME_") NET_PRINT_INT(i+1) NET_PRINT(" ") NET_PRINT_STATDATE(DateData) NET_NL()
	ENDREPEAT
	DateData = GET_MP_DATE_PLAYER_STAT(MPPLY_LAST_TIME_THRESH_CROSS)
	NET_PRINT("[Rate_Limiter] MPPLY_LAST_TIME_THRESH_CROSS ") NET_PRINT(" ") NET_PRINT_STATDATE(DateData) NET_NL()
	NET_PRINT("[Rate_Limiter] MP_STAT_LAST_CAR_SOLD_NUMBER = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_CAR_SOLD_NUMBER)) NET_NL()
	
ENDPROC
#ENDIF


PROC RESET_THRESHOLD_STATS()

	IF (MPGlobalsAmbience.bRESET_THRESHOLD_STATS_BGOverride)
		PRINTLN("[Rate_Limiter] RESET_THRESHOLD_STATS - bRESET_THRESHOLD_STATS_BGOverride  = ", MPGlobalsAmbience.bRESET_THRESHOLD_STATS_BGOverride)
		EXIT
	ENDIF


	STRUCT_STAT_DATE EmptyDate
	INT i
	REPEAT NUMBER_OF_TIMESTAMPS_STORED_FOR_CAR_SALES i
		SET_MP_DATE_CHARACTER_STAT(GET_STAT_FOR_SOLD_INDEX(i+1), EmptyDate)	
	ENDREPEAT
	SET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_CAR_SOLD_NUMBER, 0)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_VEH_SOLD_CURR_THRESH, 0)
	PRINTLN("[Rate_Limiter] RESET_THRESHOLD_STATS - called ") 
ENDPROC

FUNC STRUCT_STAT_DATE GET_TIME_OF_FIRST_SALE_IN_THRESHOLD(INT iThisSoldCarIndex)

	IF (MPGlobalsAmbience.bGET_TIME_OF_FIRST_SALE_IN_THRESHOLD_BGOverride)
		#IF IS_DEBUG_BUILD
		NET_PRINT("[Rate_Limiter] GET_TIME_OF_FIRST_SALE_IN_THRESHOLD - DateSET_LAST_TIME_THRESHOLD_CROSSED  = ")
		NET_PRINT_STATDATE(MPGlobalsAmbience.DateGET_TIME_OF_FIRST_SALE_IN_THRESHOLD)
		NET_NL()
		#ENDIF
		RETURN MPGlobalsAmbience.DateGET_TIME_OF_FIRST_SALE_IN_THRESHOLD
	ENDIF

	// find timestamp of 1st car in threshold window
	
	INT iFirstCarIndex = iThisSoldCarIndex - g_sMPTunables.iThreshold_no_of_cars
	iFirstCarIndex += 1
	
	IF (iFirstCarIndex < 1)
		iFirstCarIndex += NUMBER_OF_TIMESTAMPS_STORED_FOR_CAR_SALES	
	ENDIF
	
	RETURN GET_MP_DATE_CHARACTER_STAT(GET_STAT_FOR_SOLD_INDEX(iFirstCarIndex))		
	
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC INT GET_VEHICLES_SOLD_IN_CURRENT_THRESHOLD_WINDOW()
	
	STRUCT_STAT_DATE currentdate
	GET_UTC_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)	
	
	INT iLastCarSold = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_CAR_SOLD_NUMBER)
	INT i
	INT iCount
	INT iMins
	
	STRUCT_STAT_DATE storeddate
	
	IF (iLastCarSold > 0)
		REPEAT iLastCarSold i
			storeddate = GET_MP_DATE_CHARACTER_STAT(GET_STAT_FOR_SOLD_INDEX(i+1))
			iMins = GET_MINS_BETWEEN_STATDATES(storeddate, currentdate)
			IF (iMins < g_sMPTunables.iThreshold_no_of_minutes)
				iCount++	
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN iCount
	
ENDFUNC

PROC PV_LIMITER_DEBUG_UPDATE()

	IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobalsAmbience.timePVLimiterUpdate)) > MPGlobalsAmbience.iTimeToWaitForDebugUpdate)
	OR (MPGlobalsAmbience.bForcePVLimterDebugUpdateNow)

		// vehicle sold in threshold
		INT iCurrentValue = GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_VEH_SOLD_CURR_THRESH)
		INT iCarsSoldInThreshold = GET_VEHICLES_SOLD_IN_CURRENT_THRESHOLD_WINDOW()		
		IF (iCarsSoldInThreshold != iCurrentValue)			
			PRINTLN("[Rate_Limiter] PV_LIMITER_DEBUG_UPDATE, updating vehicles sold in threshold from ",iCurrentValue, " to ",  iCarsSoldInThreshold)		
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_VEH_SOLD_CURR_THRESH, iCarsSoldInThreshold)
		ENDIF
		
		// repeat offender info
		MPGlobalsAmbience.bAmIRepeatOffender1 = IS_REPEAT_OFFENDER_1()
		MPGlobalsAmbience.bAmIRepeatOffender2 = IS_REPEAT_OFFENDER_2()
	
	
		STRUCT_STAT_DATE currentdate
		GET_UTC_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)
		STRUCT_STAT_DATE offenderDate
		INT iMins
		
		IF (MPGlobalsAmbience.bAmIRepeatOffender1)
			offenderDate = GET_MP_DATE_PLAYER_STAT(MPPLY_REPEAT_OFFENDER_1_TIME)
			iMins = GET_MINS_BETWEEN_STATDATES(	offenderDate, currentdate)
			MPGlobalsAmbience.iRemainingPunishment1 = g_sMPTunables.iPermanentCapRepeatOffender1 - iMins
		ELSE
			MPGlobalsAmbience.iRemainingPunishment1 = 0
		ENDIF
		IF (MPGlobalsAmbience.iRemainingPunishment1 < 0)
			MPGlobalsAmbience.iRemainingPunishment1 = 0
		ENDIF
		
		IF (MPGlobalsAmbience.bAmIRepeatOffender2)
			offenderDate = GET_MP_DATE_PLAYER_STAT(MPPLY_REPEAT_OFFENDER_2_TIME)
			iMins = GET_MINS_BETWEEN_STATDATES(	offenderDate, currentdate)
			MPGlobalsAmbience.iRemainingPunishment2 = g_sMPTunables.iPermanentCapRepeatOffender2 - iMins
		ELSE
			MPGlobalsAmbience.iRemainingPunishment2 = 0
		ENDIF
		IF (MPGlobalsAmbience.iRemainingPunishment2 < 0)
			MPGlobalsAmbience.iRemainingPunishment2 = 0
		ENDIF
		
		MPGlobalsAmbience.bForcePVLimterDebugUpdateNow = FALSE
		MPGlobalsAmbience.timePVLimiterUpdate = GET_NETWORK_TIME()
	ENDIF
ENDPROC

#ENDIF


FUNC BOOL DOES_THIS_SALE_BREAK_THRESHOLD(INT iThisSoldCarIndex)

	IF (MPGlobalsAmbience.bDOES_THIS_SALE_BREAK_THRESHOLD_BGOverride)
		PRINTLN("[Rate_Limiter] DOES_THIS_SALE_BREAK_THRESHOLD - bDOES_THIS_SALE_BREAK_THRESHOLD_BGValue  = ", MPGlobalsAmbience.bDOES_THIS_SALE_BREAK_THRESHOLD_BGValue)
		RETURN MPGlobalsAmbience.bDOES_THIS_SALE_BREAK_THRESHOLD_BGValue
	ENDIF	

	STRUCT_STAT_DATE currentdate
	GET_UTC_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)	

	STRUCT_STAT_DATE storeddate
	storeddate = GET_TIME_OF_FIRST_SALE_IN_THRESHOLD(iThisSoldCarIndex)	

	#IF IS_DEBUG_BUILD
		NET_PRINT("[Rate_Limiter] DOES_THIS_SALE_BREAK_THRESHOLD - storeddate = ") NET_PRINT_STATDATE(storeddate) NET_NL() 
	#ENDIF


	IF (storeddate.Year != 0)
		// check if we're still in the threshold window
		INT iMins = GET_MINS_BETWEEN_STATDATES(storeddate, currentdate)
		PRINTLN("[Rate_Limiter] DOES_THIS_SALE_BREAK_THRESHOLD - mins between dates = ", iMins)
		IF (iMins < g_sMPTunables.iThreshold_no_of_minutes )
			PRINTLN("[Rate_Limiter] DOES_THIS_SALE_BREAK_THRESHOLD - Returning TRUE ")
			RETURN(TRUE)
		ENDIF	
	ENDIF
	
	PRINTLN("[Rate_Limiter] DOES_THIS_SALE_BREAK_THRESHOLD - Returning FALSE")
	RETURN(FALSE)

ENDFUNC

PROC INCREMENT_CARS_SOLD_FOR_CAPS_CHECK(INT iForcedExploitLevelIncrease=0, BOOL bForceMaxCapHitToday = FALSE)

	MPGlobalsAmbience.bINCREMENT_CARS_SOLD_FOR_CAPS_CHECK_Called = TRUE
	MPGlobalsAmbience.iINCREMENT_CARS_SOLD_FOR_CAPS_CHECK_iForcedExploitLevelIncrease = iForcedExploitLevelIncrease
	MPGlobalsAmbience.iINCREMENT_CARS_SOLD_FOR_CAPS_CHECK_bForceMaxCapHitToday = bForceMaxCapHitToday

	IF (MPGlobalsAmbience.bINCREMENT_CARS_SOLD_FOR_CAPS_CHECK_BGOverride)
		PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - bINCREMENT_CARS_SOLD_FOR_CAPS_CHECK  = ", MPGlobalsAmbience.bINCREMENT_CARS_SOLD_FOR_CAPS_CHECK_BGOverride)
		EXIT
	ENDIF

	STRUCT_STAT_DATE currentdate
	GET_UTC_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)	

	#IF IS_DEBUG_BUILD
		NET_PRINT("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - currentdate = ") NET_PRINT_STATDATE(currentdate) NET_NL() 
		PRINT_THRESHOLD_DATA()
	#ENDIF
	
	// increment sales timestamping
	INT iLastSoldCar = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_CAR_SOLD_NUMBER)
	INT iThisSoldCarIndex = iLastSoldCar + 1
	IF (iThisSoldCarIndex > NUMBER_OF_TIMESTAMPS_STORED_FOR_CAR_SALES)
		iThisSoldCarIndex = 1	
	ENDIF
	SET_MP_DATE_CHARACTER_STAT(GET_STAT_FOR_SOLD_INDEX(iThisSoldCarIndex), currentdate)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_CAR_SOLD_NUMBER, iThisSoldCarIndex)
	PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - iLastSoldCar ", iLastSoldCar)
	PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - setting iThisSoldCarIndex ", iThisSoldCarIndex)
	
	INT iExploitLevel = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_EXPLOIT_LEVEL)	
	PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - iExploitLevel ", iExploitLevel)	
	
	#IF IS_DEBUG_BUILD
		INT iCarAlreadySoldToday = GET_MP_INT_PLAYER_STAT(MPPLY_NUM_CARS_SOLD_TODAY)
		PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - iCarAlreadySoldToday ", iCarAlreadySoldToday)
	#ENDIF	
	
	// store if they are are repeat defender (so we dont keep calling the commands)
	BOOL bIsRepeatOffender1 = IS_REPEAT_OFFENDER_1()
	BOOL bIsRepeatOffender2 = IS_REPEAT_OFFENDER_2()	
	
	// should we reduce cap?
	IF (iExploitLevel > 0)
		IF (bIsRepeatOffender1 OR bIsRepeatOffender2)	
			
			#IF IS_DEBUG_BUILD
				IF IS_REPEAT_OFFENDER_1()
					PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - not reducing exploit level as player is repeat offender 1")
				ENDIF
				IF IS_REPEAT_OFFENDER_2()
					PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - not reducing exploit level as player is repeat offender 2")
				ENDIF
			#ENDIF		
			
		ELSE
		
			STRUCT_STAT_DATE storeddate
			storeddate = GET_MP_DATE_PLAYER_STAT(MPPLY_LAST_TIME_THRESH_CROSS)	
			#IF IS_DEBUG_BUILD
				NET_PRINT("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - storeddate = ") NET_PRINT_STATDATE(storeddate) NET_NL()
			#ENDIF
			
			INT iMinsSinceLastThresholdCross = GET_MINS_BETWEEN_STATDATES(storeddate, currentdate)
			PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - iMinsSinceLastThresholdCross = ", iMinsSinceLastThresholdCross)
			BOOL bExploitLevelHasBeenReduced = FALSE		
			
			IF IS_STAT_DATE_VALID(storeddate)							
				WHILE ((iMinsSinceLastThresholdCross > g_sMPTunables.iNumber_of_minutes_for_cap_recovery) AND (iExploitLevel > 0))
					iExploitLevel--
					iMinsSinceLastThresholdCross -= g_sMPTunables.iNumber_of_minutes_for_cap_recovery
					bExploitLevelHasBeenReduced = TRUE
				ENDWHILE 
			ELSE
				PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - stored date is not valid, not reducing exploit level.")
			ENDIF
			
			IF (bExploitLevelHasBeenReduced)
				
				// have we cleared the exploit level? if so set the threshold level to a few below
				IF (iExploitLevel = 0)
					PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - exploit level cleared, setting threshold level to  = ", g_sMPTunables.iThresholdLevelToDropTo)
					SET_PLAYERS_THRESHOLD_LEVEL(g_sMPTunables.iThresholdLevelToDropTo)
				ENDIF
			
				PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK bringing forward time of last threshold crossed so we dont reduce again next check.  ")
				SET_LAST_TIME_THRESHOLD_CROSSED(currentdate)
				
				PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - reducing exploit level to ", iExploitLevel)
				SET_PLAYERS_EXPLOIT_LEVEL(iExploitLevel)
			ENDIF
			
		ENDIF
	ENDIF	
	
	IF NOT IS_LESS_THAN_24_HOURS_SINCE_FIRST_SALE()
		PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - More than 24 hours since first sale, reset MPPLY_NUM_CARS_SOLD_TODAY.")
		// initialise overall stat
		SET_MP_DATE_PLAYER_STAT(MPPLY_TIME_FIRST_CAR_SOLD_TODAY, currentdate)
		SET_MP_INT_PLAYER_STAT(MPPLY_NUM_CARS_SOLD_TODAY, 0)
	ENDIF	
		
	PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - Less than 24 hours since first sale.")
	
	// increment overall stat
	INCREMENT_MP_INT_PLAYER_STAT(MPPLY_NUM_CARS_SOLD_TODAY)
	INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NO_VEH_SOLD_CURR_THRESH)
	
	// if we want to force hit the cap limit for vehicles sold today.
	IF (bForceMaxCapHitToday)
		PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - bForceMaxCapHitToday = TRUE ")
		SET_MP_INT_PLAYER_STAT(MPPLY_NUM_CARS_SOLD_TODAY, 99)	
	ENDIF
	
	
	// threshold stats
	INT iThresholdLevel = GET_MP_INT_CHARACTER_STAT(MP_STAT_NUM_THRESH_CROSSED_TODAY)	
	PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - iThresholdLevel ", iThresholdLevel)		
	
	// if caps are not enforced and exploit level is zero, then monitor when we should start increasing their exploit level.
	IF DOES_THIS_SALE_BREAK_THRESHOLD(iThisSoldCarIndex)
	OR (iForcedExploitLevelIncrease > 0)
	
		PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - This breaks threshold. ")
		PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - iForcedExploitLevelIncrease = ", iForcedExploitLevelIncrease)
	
		// increase number of thresholds broken today
		iThresholdLevel += 1
		IF (iThresholdLevel < g_sMPTunables.iNo_of_initial_thresholds_to_cross)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUM_THRESH_CROSSED_TODAY, iThresholdLevel)
			SET_MP_INT_PLAYER_STAT(MPPLY_THRESHOLD_RESET, 0)
		ENDIF
		
		IF (iExploitLevel = 0)		
		AND NOT (iForcedExploitLevelIncrease > 0)
			IF NOT (g_sMPTunables.bCaps_are_enfored)
				IF (iThresholdLevel >= g_sMPTunables.iNo_of_initial_thresholds_to_cross)
					// yes, reset threshold first car, increment counter to get on first ladder.
					SET_PLAYERS_EXPLOIT_LEVEL(1)
				ENDIF	
			ELSE
				IF (iThresholdLevel >= g_sMPTunables.iNo_of_initial_thresholds_to_cross_CAPS_ON)
					// yes, reset threshold first car, increment counter to get on first ladder.
					SET_PLAYERS_EXPLOIT_LEVEL(1)
				ENDIF	
			ENDIF
		ELIF (iExploitLevel <= MAX_EXPLOIT_LEVEL)
			// increase exploit level
			IF (iForcedExploitLevelIncrease > 0)
				iExploitLevel += iForcedExploitLevelIncrease
			ELSE
				iExploitLevel++
			ENDIF
			
			IF (iExploitLevel > MAX_EXPLOIT_LEVEL)
				iExploitLevel = MAX_EXPLOIT_LEVEL
			ENDIF
			SET_PLAYERS_EXPLOIT_LEVEL(iExploitLevel)
			
			// does this make the player a repeat offender? if so store the time.
			IF NOT (bIsRepeatOffender1) 
			AND IS_REPEAT_OFFENDER_1()
				PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - player has just become repeat offender 1. ")
				SET_REPEAT_OFFENDER_TIME_1(currentdate)
			ENDIF
			IF NOT (bIsRepeatOffender2) 
			AND IS_REPEAT_OFFENDER_2()
				PRINTLN("[Rate_Limiter] INCREMENT_CARS_SOLD_FOR_CAPS_CHECK - player has become repeat offender 2. ")
				SET_REPEAT_OFFENDER_TIME_2(currentdate)
			ENDIF
			
		ENDIF
			
		// store time of this threshold cross
		SET_LAST_TIME_THRESHOLD_CROSSED(currentdate)
		
		// reset threshold check
		RESET_THRESHOLD_STATS()
	ENDIF
		
	
	#IF IS_DEBUG_BUILD
		FORCE_PV_LIMITER_DEBUG_UPDATE_NOW()
		MPGlobalsAmbience.bUpdateGetPVLimiterStats = TRUE
	#ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

PROC PRINT_PV_LIMITER_DEBUG_DATA(PV_LIMITER_DEBUG &Data)
	
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA ---- PLAYER STATS ----")
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iExploitLevel = ", Data.iExploitLevel)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iPeakExploitLevel = ", Data.iPeakExploitLevel)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iCurrentExploitSeason = ", Data.iCurrentExploitSeason)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iCurrentTUSeason = ", Data.iCurrentTUSeason)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iPreviousSeasonExploitLevel = ", Data.iPreviousSeasonExploitLevel)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iPreviousSeason2ExploitLevel = ", Data.iPreviousSeason2ExploitLevel)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iPreviousSeason3ExploitLevel = ", Data.iPreviousSeason3ExploitLevel)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iPreviousSeason4ExploitLevel = ", Data.iPreviousSeason4ExploitLevel)		
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iNumCarsSoldToday = ", Data.iNumCarsSoldToday)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA ---- CHARACTER STATS ----")	
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iThresholdLevel = ", Data.iThresholdLevel)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iCarsSoldInCurrentThreshold = ", Data.iCarsSoldInCurrentThreshold)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iLastCarSoldIndex = ", Data.iLastCarSoldIndex)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA ---- SERVER STATS ----")	
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA iSeasonToClear = ", Data.iSeasonToClear)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA bOptOutOfSystem = ", Data.bOptOutOfSystem)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA bOptInLowRepeatOffender = ", Data.bOptInLowRepeatOffender)
	PRINTLN("[Rate_Limiter] PRINT_PV_LIMITER_DEBUG_DATA bOptInHighRepeatOffender = ", Data.bOptInHighRepeatOffender)
	
ENDPROC

PROC GET_PV_LIMITER_STATS(PV_LIMITER_DEBUG &Data)
	PRINTLN("[Rate_Limiter] GET_PV_LIMITER_STATS called.")
	
	// player stats
	Data.iExploitLevel = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_EXPLOIT_LEVEL)
	Data.iPeakExploitLevel = GET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL)
	Data.iCurrentExploitSeason = GET_MP_INT_PLAYER_STAT(MPPLY_CURRENTVEHSALESSEASON)
	Data.iCurrentTUSeason = GET_MP_INT_PLAYER_STAT(MPPLY_CURRENTVEHSALESTUSEASON)
	Data.iPreviousSeasonExploitLevel = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASONEXPLOITLEVEL)
	Data.iPreviousSeason2ExploitLevel = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON2EXPLOITLEVEL)
	Data.iPreviousSeason3ExploitLevel = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON3EXPLOITLEVEL)
	Data.iPreviousSeason4ExploitLevel = GET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON4EXPLOITLEVEL)	
	Data.iNumCarsSoldToday = GET_MP_INT_PLAYER_STAT(MPPLY_NUM_CARS_SOLD_TODAY)
	
	// char stats
	Data.iThresholdLevel = GET_MP_INT_CHARACTER_STAT(MP_STAT_NUM_THRESH_CROSSED_TODAY)
	Data.iCarsSoldInCurrentThreshold = GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_VEH_SOLD_CURR_THRESH)
	Data.iLastCarSoldIndex = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_CAR_SOLD_NUMBER)
	
	// server profile stats
	Data.iSeasonToClear = GET_MP_INT_PLAYER_STAT(MPPLY_SEASONTOCLEAR)
	Data.bOptOutOfSystem = GET_MP_BOOL_PLAYER_STAT(MPPLY_OPT_OUT_OF_SYSTEM)
	Data.bOptInLowRepeatOffender = GET_MP_BOOL_PLAYER_STAT(MPPLY_LOW_REPEAT_OFFENDER_CAP)
	Data.bOptInHighRepeatOffender = GET_MP_BOOL_PLAYER_STAT(MPPLY_HIGH_REPEAT_OFFENDER_CAP)

	 
	
	PRINT_PV_LIMITER_DEBUG_DATA(Data)
	
ENDPROC

PROC SET_PV_LIMITER_STATS(PV_LIMITER_DEBUG &Data)
	PRINTLN("[Rate_Limiter] SET_PV_LIMITER_STATS called.")
	
	// player stats
	SET_PLAYERS_EXPLOIT_LEVEL(Data.iExploitLevel)
	SET_MP_INT_PLAYER_STAT(MPPLY_CHAR_PEAK_EXPLOIT_LEVEL, Data.iPeakExploitLevel)
	SET_MP_INT_PLAYER_STAT(MPPLY_CURRENTVEHSALESSEASON, Data.iCurrentExploitSeason)
	SET_MP_INT_PLAYER_STAT(MPPLY_CURRENTVEHSALESTUSEASON, Data.iCurrentTUSeason)
	SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASONEXPLOITLEVEL, Data.iPreviousSeasonExploitLevel)
	SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON2EXPLOITLEVEL, Data.iPreviousSeason2ExploitLevel)
	SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON3EXPLOITLEVEL, Data.iPreviousSeason3ExploitLevel)
	SET_MP_INT_PLAYER_STAT(MPPLY_PREVSEASON4EXPLOITLEVEL, Data.iPreviousSeason4ExploitLevel)
	SET_MP_INT_PLAYER_STAT(MPPLY_NUM_CARS_SOLD_TODAY , Data.iNumCarsSoldToday)
	
	// character stats
	SET_MP_INT_CHARACTER_STAT(MP_STAT_NUM_THRESH_CROSSED_TODAY, Data.iThresholdLevel)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_VEH_SOLD_CURR_THRESH, Data.iCarsSoldInCurrentThreshold)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_CAR_SOLD_NUMBER, Data.iLastCarSoldIndex)
	
	// server authorative
	SET_MP_INT_PLAYER_STAT(MPPLY_SEASONTOCLEAR, Data.iSeasonToClear)
	SET_MP_BOOL_PLAYER_STAT(MPPLY_OPT_OUT_OF_SYSTEM, Data.bOptOutOfSystem)
	SET_MP_BOOL_PLAYER_STAT(MPPLY_LOW_REPEAT_OFFENDER_CAP, Data.bOptInLowRepeatOffender)
	SET_MP_BOOL_PLAYER_STAT(MPPLY_HIGH_REPEAT_OFFENDER_CAP, Data.bOptInHighRepeatOffender)
	
ENDPROC

PROC ADD_WIDGET_FOR_PV_LIMITER(PV_LIMITER_DEBUG &Data)
	
	START_WIDGET_GROUP("PV Sales - Rate Limiter")
	
		// tunables
		START_WIDGET_GROUP("Tunables")
		
			ADD_WIDGET_BOOL("bCaps_are_enfored", g_sMPTunables.bCaps_are_enfored)
			ADD_WIDGET_BOOL("bRepeat_Offenders_Caps_Are_Enforced", g_sMPTunables.bRepeat_Offenders_Caps_Are_Enforced)
			
			START_WIDGET_GROUP("Exploit Level Caps")				
				ADD_WIDGET_INT_SLIDER("iCap_for_exploit_level_0", g_sMPTunables.iCap_for_exploit_level_0, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iCap_for_exploit_level_1", g_sMPTunables.iCap_for_exploit_level_1, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iCap_for_exploit_level_2", g_sMPTunables.iCap_for_exploit_level_2, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iCap_for_exploit_level_3", g_sMPTunables.iCap_for_exploit_level_3, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iCap_for_exploit_level_4", g_sMPTunables.iCap_for_exploit_level_4, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iCap_for_exploit_level_5", g_sMPTunables.iCap_for_exploit_level_5, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iCap_for_exploit_level_6", g_sMPTunables.iCap_for_exploit_level_6, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iCap_for_exploit_level_7", g_sMPTunables.iCap_for_exploit_level_7, 0, 99, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Threshold Limits")
				ADD_WIDGET_INT_SLIDER("iThreshold_no_of_cars", g_sMPTunables.iThreshold_no_of_cars, 0, 20, 1)
				ADD_WIDGET_INT_SLIDER("iThreshold_no_of_minutes", g_sMPTunables.iThreshold_no_of_minutes, 0, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iNo_of_initial_thresholds_to_cross", g_sMPTunables.iNo_of_initial_thresholds_to_cross, 0, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iNo_of_initial_thresholds_to_cross_CAPS_ON", g_sMPTunables.iNo_of_initial_thresholds_to_cross_CAPS_ON, 0, HIGHEST_INT, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Recovery")
				ADD_WIDGET_INT_SLIDER("iNumber_of_minutes_for_cap_recovery", g_sMPTunables.iNumber_of_minutes_for_cap_recovery, 0, HIGHEST_INT, 1)	
				ADD_WIDGET_INT_SLIDER("iNumExploitLevelsToDrop", g_sMPTunables.iNumExploitLevelsToDrop, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iThresholdLevelToDropTo", g_sMPTunables.iThresholdLevelToDropTo, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iTime_Window_For_PV_Sales_Limiter", g_sMPTunables.iTime_Window_For_PV_Sales_Limiter, 0, HIGHEST_INT, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Seasons")
				ADD_WIDGET_INT_SLIDER("iCurrentVehicleSalesSeason", g_sMPTunables.iCurrentVehicleSalesSeason, 0, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iCurrentVehicleSalesTUSeason", g_sMPTunables.iCurrentVehicleSalesTUSeason, 0, HIGHEST_INT, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Repeat Offenders")								
				ADD_WIDGET_INT_SLIDER("iRepeatOffenderNumSeasons1", g_sMPTunables.iRepeatOffenderNumSeasons1, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iRepeatOffenderNumSeasons2", g_sMPTunables.iRepeatOffenderNumSeasons2, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iRepeatOffenderCap1", g_sMPTunables.iRepeatOffenderCap1, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iRepeatOffenderCap2", g_sMPTunables.iRepeatOffenderCap2, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iRepeatOffenderMinExploitLevel1", g_sMPTunables.iRepeatOffenderMinExploitLevel1, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iRepeatOffenderMinExploitLevel2", g_sMPTunables.iRepeatOffenderMinExploitLevel2, 0, 99, 1)	
				ADD_WIDGET_INT_SLIDER("iPermanentCapRepeatOffender1", g_sMPTunables.iPermanentCapRepeatOffender1, 0, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("iPermanentCapRepeatOffender2", g_sMPTunables.iPermanentCapRepeatOffender2, 0, HIGHEST_INT, 1)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	
		// stats
		START_WIDGET_GROUP("Stats")
			//ADD_WIDGET_BOOL("Get Latest Stats", MPGlobalsAmbience.bUpdateGetPVLimiterStats)
		
			START_WIDGET_GROUP("Player Stats")
				ADD_WIDGET_INT_SLIDER("EXPLOIT_LEVEL", Data.iExploitLevel, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("PEAK_EXPLOIT_LEVEL", Data.iPeakExploitLevel, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("CURRENTVEHICLESALESSEASON", Data.iCurrentExploitSeason, 0, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("CURRENTVEHICLESALESTUSEASON", Data.iCurrentTUSeason, 0, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("PREVIOUSSEASONEXPLOITLEVEL", Data.iPreviousSeasonExploitLevel, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("PREVIOUSSEASON2EXPLOITLEVEL", Data.iPreviousSeason2ExploitLevel, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("PREVIOUSSEASON3EXPLOITLEVEL", Data.iPreviousSeason3ExploitLevel, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("PREVIOUSSEASON4EXPLOITLEVEL", Data.iPreviousSeason4ExploitLevel, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("NUM_CARS_SOLD_TODAY", Data.iNumCarsSoldToday, 0, HIGHEST_INT, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Character Stats")
				ADD_WIDGET_INT_SLIDER("THRESHOLD_LEVEL", Data.iThresholdLevel, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("NO_VEH_SOLD_CURR_THRESH", Data.iCarsSoldInCurrentThreshold, 0, 99, 1)
			STOP_WIDGET_GROUP()
		
			START_WIDGET_GROUP("Server Authorative Stats")
				ADD_WIDGET_INT_SLIDER("SEASONTOCLEAR", Data.iSeasonToClear, 0, 99, 1)
				ADD_WIDGET_BOOL("OPT_OUT_OF_SYSTEM", Data.bOptOutOfSystem)
				ADD_WIDGET_BOOL("LOW_REPEAT_OFFENDER_CAP", Data.bOptInLowRepeatOffender)
				ADD_WIDGET_BOOL("HIGH_REPEAT_OFFENDER_CAP", Data.bOptInHighRepeatOffender)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL("Save These Stats", MPGlobalsAmbience.bUpdateSetPVLimiterStats)
		STOP_WIDGET_GROUP()
		
		// testing
		START_WIDGET_GROUP("Debugging / QA Info")
			ADD_WIDGET_BOOL("Simulate Car Sale", Data.bPretendTryToSellACar)
			//ADD_WIDGET_INT_SLIDER("LAST_CAR_SOLD_NUMBER", Data.iLastCarSoldIndex, 0, 99, 1)
			//ADD_WIDGET_INT_SLIDER("iTimeToWaitForDebugUpdate", MPGlobalsAmbience.iTimeToWaitForDebugUpdate, 0, HIGHEST_INT, 1)
			//ADD_WIDGET_INT_SLIDER("Length of a day in minutes", MPGlobalsAmbience.iDayInMinutes, 0, HIGHEST_INT, 1)
			
			START_WIDGET_GROUP("Repeat Offender Info")
				ADD_WIDGET_BOOL("Is Repeat Offender 1", MPGlobalsAmbience.bAmIRepeatOffender1)
				ADD_WIDGET_BOOL("Is Repeat Offender 2", MPGlobalsAmbience.bAmIRepeatOffender2)
				ADD_WIDGET_INT_READ_ONLY("Remaining minutes for Offender 1", MPGlobalsAmbience.iRemainingPunishment1)
				ADD_WIDGET_INT_READ_ONLY("Remaining minutes for Offender 2", MPGlobalsAmbience.iRemainingPunishment2)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
	MPGlobalsAmbience.bUpdateGetPVLimiterStats = TRUE // do an initial grab of stats

ENDPROC

PROC UPDATE_WIDGETS_FOR_PV_LIMITER(PV_LIMITER_DEBUG &Data)

	IF (MPGlobalsAmbience.bUpdateGetPVLimiterStats)
		GET_PV_LIMITER_STATS(Data)
		MPGlobalsAmbience.bUpdateGetPVLimiterStats = FALSE
		MPGlobalsAmbience.bForcePVLimterDebugUpdateNow = TRUE
	ENDIF
	
	IF (MPGlobalsAmbience.bUpdateSetPVLimiterStats)
		SET_PV_LIMITER_STATS(Data)
		MPGlobalsAmbience.bUpdateSetPVLimiterStats = FALSE
		MPGlobalsAmbience.bUpdateGetPVLimiterStats = TRUE
		MPGlobalsAmbience.bForcePVLimterDebugUpdateNow = TRUE
	ENDIF
	
	IF (Data.bPretendTryToSellACar)
		PRINTLN("[Rate_Limiter] DEBUG - PRETENDING TO TRY AND SELL A CAR ****")
		IF CAN_PLAYER_SELL_PERSONAL_VEHICLE_CAPS_CHECK()
			INCREMENT_CARS_SOLD_FOR_CAPS_CHECK()
		ENDIF		
		Data.bPretendTryToSellACar = FALSE
	ENDIF
	
	PV_LIMITER_DEBUG_UPDATE()

ENDPROC

#ENDIF

// eof


