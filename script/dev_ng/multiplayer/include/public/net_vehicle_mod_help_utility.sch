USING "globals.sch"
USING "net_include.sch"
USING "net_ambience.sch"
USING "net_help_text_appender.sch"


CONST_INT MAX_HELP_DISPLAYS 3


/// PURPOSE:
///    Gets the players current vehicle model
/// RETURNS:
///    The players current vehicle model
FUNC MODEL_NAMES GET_PLAYERS_CURRENT_VEHICLE_MODEL()
	
	VEHICLE_INDEX viVehicle = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	
	IF NOT DOES_ENTITY_EXIST(viVehicle)
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDIF

	RETURN GET_ENTITY_MODEL(viVehicle)
	
ENDFUNC


/// PURPOSE:
///    Gets the players current vehicle
/// RETURNS:
///    The players current vehicle
FUNC VEHICLE_INDEX GET_PLAYERS_CURRENT_VEHICLE()
	RETURN GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
ENDFUNC


/// PURPOSE:
///    Checks if mod help text can be printed for the given mod
/// PARAMS:
///    viVehicle - the vehicle to check for mods
///    eModType - the mod slot to check
///    iModID - the id of the mod
///    iPrintedFlag - the index to increment the display count
///   eRequiredSeat - the seat the player must be in
/// RETURNS:
///    TRUE if can print help for this mod
FUNC BOOL CAN_PRINT_MOD_HELP_TEXT(INT& iModHelpDisplayCounts[], MOD_TYPE eModType, INT iModID,
INT iPrintedFlag, VEHICLE_SEAT eRequiredSeat = VS_DRIVER)
	
	IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != eRequiredSeat
		RETURN FALSE
	ENDIF
	
	// If message has already been displayed 
	IF iModHelpDisplayCounts[iPrintedFlag] >= MAX_HELP_DISPLAYS
		RETURN FALSE
	ENDIF
	
	// If vehicle doesn't have this mod leave
	IF GET_VEHICLE_MOD(GET_PLAYERS_CURRENT_VEHICLE(), eModType) != iModID
		RETURN FALSE
	ENDIF

	++iModHelpDisplayCounts[iPrintedFlag]
	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///	   Checks if the current vehicle can print help text for a number of mods greater than a starting mod id
/// PARAMS:
///    iModHelpDisplayCounts - array keeping track of display counts
///    eModType - the mod slot type
///    iModStartID - checks all mods with an id greater than or equal to this
///    iPrintedFlag - the index to increment the display count
///    eRequiredSeat - the seat the player must be in
/// RETURNS:
///    TRUE if can print help for this mod
FUNC BOOL CAN_PRINT_MODS_HELP_TEXT(INT& iModHelpDisplayCounts[], MOD_TYPE eModType, INT iModStartID,
INT iPrintedFlag, VEHICLE_SEAT eRequiredSeat = VS_DRIVER)
	
	IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != eRequiredSeat
		RETURN FALSE
	ENDIF
	
	// If message has already been displayed 
	IF iModHelpDisplayCounts[iPrintedFlag] >= MAX_HELP_DISPLAYS
		RETURN FALSE
	ENDIF
	
	// If vehicle doesn't have this mod leave
	IF GET_VEHICLE_MOD(GET_PLAYERS_CURRENT_VEHICLE(), eModType) < iModStartID
		RETURN FALSE
	ENDIF

	++iModHelpDisplayCounts[iPrintedFlag]
	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///    Checks if the current vehicle can print help text for a mod
///    that is installed by default on a specific vehicle
/// PARAMS:
///    sInstance - arena vehicle help instance
///    eVehicleRequiringHelpText - vehicle to check for default mod
///    iPrintedFlag - the index to increment the display count
///    eRequiredSeat - the seat the player must be in
/// RETURNS:
///    TRUE if can print help for this mod
FUNC BOOL CAN_PRINT_VEHICLE_DEFAULT_MOD_HELP_TEXT(INT& iModHelpDisplayCounts[],
MODEL_NAMES eVehicleRequiringHelpText, INT iPrintedFlag, VEHICLE_SEAT eRequiredSeat = VS_DRIVER)
	
	IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != eRequiredSeat
		RETURN FALSE
	ENDIF
	
	IF GET_PLAYERS_CURRENT_VEHICLE_MODEL() != eVehicleRequiringHelpText
		RETURN FALSE
	ENDIF
	
	// If message has already been displayed three times
	IF iModHelpDisplayCounts[iPrintedFlag] >= MAX_HELP_DISPLAYS
		RETURN FALSE
	ENDIF
	
	++iModHelpDisplayCounts[iPrintedFlag]
	
	RETURN TRUE
	
ENDFUNC

