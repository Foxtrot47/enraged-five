//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        net_club_dj_battle_screen_typedefs.sch														
/// Description: Contains the struct and getters and setters for the dj battle screen found in 
///    			 net_club_dj_battle_screen.sch
/// Written by:  Online Technical Team: Tom Turner														
/// Date:  		23/09/2020																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "mp_globals.sch"
USING "rgeneral_include.sch"

/// PURPOSE: 
///    States used for a DJ battle screen
ENUM CLUB_DJ_BATTLE_SCREEN_STATE
	CDJBWS__INVALID = -1
	,CDJBWS__LINK_RENDER_TARGET
	,CDJBWS__DRAW
	,CDJBWS__OFF
ENDENUM

DEBUGONLY FUNC STRING DEBUG_GET_CLUB_DJ_BATTLE_SCREEN_STATE_AS_STRING(CLUB_DJ_BATTLE_SCREEN_STATE eEnum)
	SWITCH eEnum
		CASE CDJBWS__INVALID				RETURN	"CDJBWS__INVALID"
		CASE CDJBWS__LINK_RENDER_TARGET		RETURN	"CDJBWS__LINK_RENDER_TARGET"
		CASE CDJBWS__DRAW					RETURN	"CDJBWS__DRAW"
		CASE CDJBWS__OFF					RETURN	"CDJBWS__OFF"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_CLUB_DJ_BATTLE_SCREEN_STATE_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC
 
TYPEDEF FUNC BOOL T_GET_CLUB_DJ_BATTLE_SCREEN_COLOR_VARIATION(INT iIndex, COLOR_STRUCT &sColorOut)

/// PURPOSE: 
///    Struct for running a dj battle screen
STRUCT CLUB_DJ_BATTLE_SCREEN
	INT iID = -1
	INT iRenderTargetID = -1
	CLUB_DJ_BATTLE_SCREEN_STATE eState
	
	MODEL_NAMES eScreenProp	
	STRING strRenderTextureName
	STRING strBinkPlaylist
	
	CLUB_MUSIC_INTENSITY eAudioIntensity
	T_GET_CLUB_DJ_BATTLE_SCREEN_COLOR_VARIATION fpColorVariantLookup
	COLOR_STRUCT sCurrentColor
	INT iCurrentColorVariation = 0
	INT iCurrentBeat = 0
ENDSTRUCT

TYPEDEF FUNC CLUB_DJ_BATTLE_SCREEN T_CLUB_DJ_BATTLE_SCREEN_LOOKUP(INT iIndex, INT &iVariation[])

////////////////////////////////////////////////////////////////////////////////
///	GETTERS
////////////////////////////////////////////////////////////////////////////////

FUNC CLUB_DJ_BATTLE_SCREEN_STATE CLUB_DJ_BATTLE_SCREEN__GET_STATE(CLUB_DJ_BATTLE_SCREEN &sInst)
	RETURN sInst.eState
ENDFUNC

FUNC MODEL_NAMES CLUB_DJ_BATTLE_SCREEN__GET_WALL_PROP(CLUB_DJ_BATTLE_SCREEN &sInst)
	RETURN sInst.eScreenProp
ENDFUNC

FUNC STRING CLUB_DJ_BATTLE_SCREEN__GET_RENDER_TEXTURE_NAME(CLUB_DJ_BATTLE_SCREEN &sInst)
	RETURN sInst.strRenderTextureName
ENDFUNC

FUNC STRING CLUB_DJ_BATTLE_SCREEN__GET_BINK_PLAYLIST(CLUB_DJ_BATTLE_SCREEN &sInst)
	RETURN sInst.strBinkPlaylist
ENDFUNC

FUNC INT CLUB_DJ_BATTLE_SCREEN__GET_ID(CLUB_DJ_BATTLE_SCREEN &sInst)
	RETURN sInst.iID
ENDFUNC

FUNC T_GET_CLUB_DJ_BATTLE_SCREEN_COLOR_VARIATION CLUB_DJ_BATTLE_SCREEN__GET_COLOR_VARIANT_LOOKUP(CLUB_DJ_BATTLE_SCREEN &sInst)
	RETURN sInst.fpColorVariantLookup
ENDFUNC

FUNC INT CLUB_DJ_BATTLE_SCREEN__GET_RENDER_TARGET_ID(CLUB_DJ_BATTLE_SCREEN &sInst)
	RETURN sInst.iRenderTargetID
ENDFUNC

////////////////////////////////////////////////////////////////////////////////
///	SETTERS
////////////////////////////////////////////////////////////////////////////////

PROC _CLUB_DJ_BATTLE_SCREEN__SET_STATE(CLUB_DJ_BATTLE_SCREEN &sInst, CLUB_DJ_BATTLE_SCREEN_STATE eState)
	PRINTLN("[CLUB_DJ_BATTLE_SCREEN] SET_STATE - ", DEBUG_GET_CLUB_DJ_BATTLE_SCREEN_STATE_AS_STRING(sInst.eState), 
		"->", DEBUG_GET_CLUB_DJ_BATTLE_SCREEN_STATE_AS_STRING(eState))
	sInst.eState = eState
ENDPROC

PROC CLUB_DJ_BATTLE_SCREEN__SET_WALL_PROP(CLUB_DJ_BATTLE_SCREEN &sInst, MODEL_NAMES eScreenProp)
	sInst.eScreenProp = eScreenProp
ENDPROC

PROC CLUB_DJ_BATTLE_SCREEN__SET_RENDER_TEXTURE_NAME(CLUB_DJ_BATTLE_SCREEN &sInst, STRING strRenderTextureName)
	sInst.strRenderTextureName = strRenderTextureName
ENDPROC

PROC CLUB_DJ_BATTLE_SCREEN__SET_BINK_PLAYLIST(CLUB_DJ_BATTLE_SCREEN &sInst, STRING strBinkPlaylist)
	sInst.strBinkPlaylist = strBinkPlaylist
ENDPROC

PROC _CLUB_DJ_BATTLE_SCREEN__SET_ID(CLUB_DJ_BATTLE_SCREEN &sInst, INT iID)
	sInst.iID = iID
ENDPROC

PROC CLUB_DJ_BATTLE_SCREEN__SET_COLOUR_VARIANT_LOOKUP(CLUB_DJ_BATTLE_SCREEN &sInst, T_GET_CLUB_DJ_BATTLE_SCREEN_COLOR_VARIATION fpLookup)
	sInst.fpColorVariantLookup = fpLookup
ENDPROC

PROC _CLUB_DJ_BATTLE_SCREEN__SET_RENDER_TARGET_ID(CLUB_DJ_BATTLE_SCREEN &sInst, INT iID)
	sInst.iRenderTargetID = iID
ENDPROC
