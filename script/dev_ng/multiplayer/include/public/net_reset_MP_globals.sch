USING "globals.sch"
USING "MP_Globals.sch"
USING "MP_Globals_FM.sch"
USING "title_update_globals.sch"

//#IF IS_DEBUG_BUILD
//USING "mp_globals_block_cnc.sch"
//#ENDIF



// These routines used to be in MP_Globals.sch - extracted out into their own header as we move the MP_Globals into their own globals block


PROC RESET_SCRIPT_TIMER(SCRIPT_TIMER &timer)
	 timer.Timer = NULL
	 timer.bInitialisedTimer = FALSE
ENDPROC

PROC RESET_MP_GLOBALS_AMBIENCE_VARS_0()
	
	NPC_BOUNTY_LOCAL_STRUCT npcBountyRequestsTemp
	MPGlobalsAmbience.npcBountyRequests = npcBountyRequestsTemp
	PLAYER_BOUNTY_STRUCT playerBountyTemp
	MPGlobalsAmbience.playerBounty = playerBountyTemp
	BOUNTY_STRUCT global_BountyTemp
	MPGlobalsAmbience.global_Bounty = global_BountyTemp
	BOUNTY_TEXTS_CONTROL_STRUCT BountyTextsTemp
	MPGlobalsAmbience.BountyTexts = BountyTextsTemp
	BOUNTY_CALL_STRUCT BountyCallsTemp
	MPGlobalsAmbience.BountyCalls = BountyCallsTemp
	BOUNTY_SAVE_CHECK_STRUCT BountySaveCheckTemp
	MPGlobalsAmbience.BountySaveCheck = BountySaveCheckTemp
	REQUESTED_STRIPPER_STORED_STRUCT stripperRequestTemp
	MPGlobalsAmbience.stripperRequest = stripperRequestTemp
	GARAGE_REQUESTS garageRequestsTemp
	MPGlobalsAmbience.garageRequests = garageRequestsTemp
	g_structCopDispatch prevCopDispatchTemp
	MPGlobalsAmbience.prevCopDispatch = prevCopDispatchTemp
	AWL_CHANGED_LOOK_STRUCT AWLChangedLookDataTemp
	MPGlobalsAmbience.AWLChangedLookData = AWLChangedLookDataTemp
	structPedsForConversation scannerConvStructTemp
	MPGlobalsAmbience.scannerConvStruct = scannerConvStructTemp
	IMPORT_EXPORT_LIST_OF_CARS impExpGLobalListTemp
	MPGlobalsAmbience.impExpGLobalList = impExpGLobalListTemp

ENDPROC
PROC RESET_MP_GLOBALS_AMBIENCE_VARS_1()
	MP_VEHICLE_TEXTS_CONTROL_STRUCT mpVehTextsTemp
	MPGlobalsAmbience.mpVehTexts = mpVehTextsTemp
	PLAYER_WANTED_LEVEL_STATE playerWantedStateTemp
	MPGlobalsAmbience.playerWantedState = playerWantedStateTemp
ENDPROC
PROC RESET_MP_GLOBALS_AMBIENCE_VARS_2()
	INT i
	TEXT_MESSAGE_LITERAL_STRUCT TMLdataTemp
	FOR i = 0 TO MAX_NUM_TEXT_MESSAGE_LITERAL - 1
		MPGlobalsAmbience.TMLdata[i] = TMLdataTemp
	ENDFOR
	
	NEW_GARAGE_DOOR_DATA_SERVER globalCopyOfGarageDataTemp
	FOR i = 0 TO TOTAL_NUM_MP_GARAGES - 1
		MPGlobalsAmbience.globalCopyOfGarageData[i] = globalCopyOfGarageDataTemp
	ENDFOR
	
	g_structCopDispatch copDispatchAmbientTemp
	g_structCopDispatch copDispatchMissionTemp
	FOR i = 0 TO TOTAL_NUM_MP_GARAGES - 1
		MPGlobalsAmbience.copDispatchAmbient[i] = copDispatchAmbientTemp
		MPGlobalsAmbience.copDispatchMission[i] = copDispatchMissionTemp
	ENDFOR
	
	MP_MISSION_DATA patrolMissionTemp
	FOR i = 0 TO MAX_NUM_MISSIONS_ALLOWED - 1
		MPGlobalsAmbience.patrolMission[i] = patrolMissionTemp
	ENDFOR
ENDPROC
PROC RESET_MP_GLOBALS_AMBIENCE_VARS_3()
	INT i
	AVAIL_ACT_DATA availableActTemp
	FOR i = 0 TO MP_ACT_MAX - 1
		MPGlobalsAmbience.availableAct[i] = availableActTemp
	ENDFOR
ENDPROC
PROC RESET_MP_GLOBALS_AMBIENCE_VARS_4()
	INT iLoop
	
	// Create empty struct to wipe the old struct
	MAGNATE_GANG_BOSS_LOCAL_GLOBALS sGangBoss
	
	// Store out things we want to keep
	eSELL_VARIATION eSellMissionHistoryList[ciMAX_SELL_HISTORY_LIST_SIZE]
	BOOL bInitialisedSellHistory
	bInitialisedSellHistory = MPGlobalsAmbience.sMagnateGangBossData.bSellMissionHistoryListInitialised
	REPEAT ciMAX_SELL_HISTORY_LIST_SIZE iLoop
		eSellMissionHistoryList[iLoop] = MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[iLoop]
	ENDREPEAT
	
	// Wipe the old struct
	MPGlobalsAmbience.sMagnateGangBossData = sGangBoss
	
	// Repopulate new struct with the old data we want to keep
	MPGlobalsAmbience.sMagnateGangBossData.bSellMissionHistoryListInitialised = bInitialisedSellHistory
	REPEAT ciMAX_SELL_HISTORY_LIST_SIZE iLoop
		MPGlobalsAmbience.sMagnateGangBossData.eSellMissionHistoryList[iLoop] = eSellMissionHistoryList[iLoop]
	ENDREPEAT
ENDPROC
PROC RESET_MP_GLOBALS_AMBIENCE_VARS_5()

	DM_BRU_BOX_DATA BruBoxDataDMTemp
	MPGlobalsAmbience.BruBoxDataDM = BruBoxDataDMTemp
	RACE_TO_POINT_STRUCT R2PdataTemp
	MPGlobalsAmbience.R2Pdata = R2PdataTemp
	
	BUDDIES_STRUCT BuddiesDataTemp
	MPGlobalsAmbience.BuddiesData = BuddiesDataTemp
	CRATE_DROP_LAUNCH_STRUCT CrateDropLaunchDataTemp
	MPGlobalsAmbience.CrateDropLaunchData = CrateDropLaunchDataTemp
	HIDEOUT_STRUCT HideoutDataTemp
	MPGlobalsAmbience.HideoutData = HideoutDataTemp
	INVITED_TO_APARTMENT_STRUCT	InvitedToApartmentDataTemp
	MPGlobalsAmbience.InvitedToApartmentData = InvitedToApartmentDataTemp
	CAR_THEFT_STAGE eCarTheftStageTemp
	MPGlobalsAmbience.eCarTheftStage = eCarTheftStageTemp
	COP_PATROL_STAGE copPatrolStateTemp
	MPGlobalsAmbience.copPatrolState = copPatrolStateTemp
	
	FMC_INVITES_STRUCT sFMCInvitesTemp
	COPY_SCRIPT_STRUCT(MPGlobalsAmbience.sFMCInvites, sFMCInvitesTemp, SIZE_OF(sFMCInvitesTemp))
ENDPROC

PROC RESET_MP_GLOBALS_AMBIENCE()
//	MPGlobalsAmbienceStruct EmptyStruct
//	IF (g_bUseStackCopy)
//		COPY_SCRIPT_STRUCT(	MPGlobalsAmbience, EmptyStruct, SIZE_OF(MPGlobalsAmbienceStruct))
//	ELSE
//		MPGlobalsAmbience = EmptyStruct
//	ENDIF	

	//PD rewriting this as creating an instance of MPGlobalsAmbienceStruct is now blowing the stack since CL 6274140
	
	INT i, i2

	#IF IS_DEBUG_BUILD
	
		RESET_SCRIPT_TIMER(MPGlobalsAmbience.g_DebugWarpOutDisabledPrintTimer)
		
		MPGlobalsAmbience.iDropOffHookerDebugLaunchID = -1
		MPGlobalsAmbience.iHoldUp_DebugLaunchBitset = 0
		MPGlobalsAmbience.g_iPlaneDropVariation = 0
		MPGlobalsAmbience.g_iShowroomvariation = 0
		MPGlobalsAmbience.g_iShipMentStealvariation	= 0
		MPGlobalsAmbience.g_iDispOfVeh_BodyVariation = 0
		MPGlobalsAmbience.g_iWitnessToCourt = 0
		MPGlobalsAmbience.g_iPackageGrab = 0
		MPGlobalsAmbience.iWarpToPropertyNum = 0
		
		MPGlobalsAmbience.g_bDebugMenuAndy = FALSE
		MPGlobalsAmbience.g_bDebugMenuLeslie  = FALSE
		MPGlobalsAmbience.g_bDebugMenuRoss = FALSE
		MPGlobalsAmbience.g_bDebugMenuSean = FALSE
		REPEAT MAX_OWNED_PROPERTIES i
			MPGlobalsAmbience.warpToCurrentProperty[i] = FALSE
		ENDREPEAT
		MPGlobalsAmbience.bReplacePropertySlot1 = FALSE
		MPGlobalsAmbience.bReplacePropertySlot2 = FALSE
		MPGlobalsAmbience.bReplacePropertySlot3 = FALSE
		MPGlobalsAmbience.bMenuWarp = FALSE
		MPGlobalsAmbience.bKeepVeh  = FALSE
		
		MPGlobalsAmbience.vMenuWarpCoord = <<0,0,0>>
		
		MPGlobalsAmbience.fMenuWarpHeading = 0.0
		
	#ENDIF
	
	MPGlobalsAmbience.piHoldUpTriggerPlayer = NULL
	MPGlobalsAmbience.piHeistSpectateTarget = NULL
	
	MPGlobalsAmbience.vehPersonalVehicle = NULL
	MPGlobalsAmbience.vehPegasusVehicle = NULL
	MPGlobalsAmbience.vehTruckVehicle[0] = NULL
	MPGlobalsAmbience.vehTruckVehicle[1] = NULL
	MPGlobalsAmbience.vehAvengerVehicle = NULL
	MPGlobalsAmbience.vehHackerTruck = NULL
	MPGlobalsAmbience.lastValkyrieForStraightIntoFpMode = NULL
	MPGlobalsAmbience.viHeliILastShowedHeliGunCamHelp = NULL
	
	MPGlobalsAmbience.biCarTheftDocks = NULL
	
	MPGlobalsAmbience.iImpoundVehicleCheckTimer = NULL
	MPGlobalsAmbience.iImpoundYardCheckTimer = NULL
	MPGlobalsAmbience.iPVCleanupTime = NULL
	MPGlobalsAmbience.g_nextBuddyInCarTimeout = NULL
	MPGlobalsAmbience.g_nextSirensOnMissionTimeout = NULL
	MPGlobalsAmbience.ForceOutTimer = NULL
	MPGlobalsAmbience.iSpottedAsFugitiveTimer = NULL
	MPGlobalsAmbience.iPrevDispatchTimer = NULL
	MPGlobalsAmbience.time_ImportExportTextMessage = NULL
	MPGlobalsAmbience.time_LaunchImportExport = NULL
	MPGlobalsAmbience.time_LaunchTaxi = NULL
	MPGlobalsAmbience.time5WantedLevel = NULL
	
	MPGlobalsAmbience.tlQuickGPS = ""
	MPGlobalsAmbience.tlQuickGPS_Offrule = ""
	MPGlobalsAmbience.sAML_CurrentlyRunning = ""
	FOR i = 0 TO ENUM_TO_INT(AML_SCRIPT_MAX) - 1
		MPGlobalsAmbience.sAML_EventHistory[i] = "" //Tunable
	ENDFOR
	FOR i = 0 TO MAX_NUM_OF_CARS_ON_IMPORT_EXPORT_LIST -1
		MPGlobalsAmbience.ImportExportWantedCars[i] = ""
	ENDFOR
	FOR i = 0 TO MAX_NUM_CRATE_DROPS -1
		MPGlobalsAmbience.vCrateDropLocations[i] = <<0,0,0>>
	ENDFOR
	
	MPGlobalsAmbience.vQuickGPS = <<0,0,0>>
	MPGlobalsAmbience.vQuickGPS2 = <<0,0,0>>
	MPGlobalsAmbience.vLastBuddyInCarUpdateLocation = <<0,0,0>>
	MPGlobalsAmbience.vLocationWantedLevel = <<0,0,0>>
	MPGlobalsAmbience.vLocation1minXpRewardWantedLevel = <<0,0,0>>
	
	MPGlobalsAmbience.g_fStopDist = 0.0
	MPGlobalsAmbience.iTimeTrialCoronaFMMC = -1
	MPGlobalsAmbience.iTimeTrialVariation = -1
	#IF FEATURE_GEN9_EXCLUSIVE
	MPGlobalsAmbience.sHSWTimeTrials.eSubvariation = HTTS_INVALID
	#ENDIF
	MPGlobalsAmbience.iAmbientBitset = 0
	MPGlobalsAmbience.iAmbientCrimeBitset = 0
	MPGlobalsAmbience.iAMProstituteLastInstance = 0
	MPGlobalsAmbience.iHoldUpActiveBitSet = 0
	MPGlobalsAmbience.iHoldUpGeneralBitSet = 0
	MPGlobalsAmbience.iLastShop = -1
	MPGlobalsAmbience.iTMLQueueSize = 0
	MPGlobalsAmbience.iQuickGPSTeam = -1
	MPGlobalsAmbience.iQuickGPSPriority = -1
	MPGlobalsAmbience.bQuickGPSUsedBitSet = 0
	MPGlobalsAmbience.iLastJobID = LAST_JOB_NONE
	MPGlobalsAmbience.iLastJobCashTotal = 0
	MPGlobalsAmbience.iLastJobCashGivenOut = 0
	MPGlobalsAmbience.icurrent_xp_reward_total = 0
	MPGlobalsAmbience.iStoredAllowIntoPV = APV_NOONE
	MPGlobalsAmbience.iStoredAllowIntoTruck = APV_NOONE
	MPGlobalsAmbience.iStoredAllowIntoAvenger = APV_NOONE
	MPGlobalsAmbience.iStoredAllowIntoHackerTruck = APV_NOONE
	#IF FEATURE_HEIST_ISLAND
	MPGlobalsAmbience.iStoredAllowIntoSubmarine = APV_NOONE
	#ENDIF
	MPGlobalsAmbience.iMaintainCanPickupItemCheckCounter = 0
	MPGlobalsAmbience.iCarTheftBitfield = 0
	MPGlobalsAmbience.copPatrolNewMission = 0
	MPGlobalsAmbience.iCopPatrolCancelledBitSet = 0
	MPGlobalsAmbience.bsCopPatrolLeaveTown = 0
	MPGlobalsAmbience.g_currentSirensOnMissionPts = 0
	MPGlobalsAmbience.g_currentBuddyInCarPts = 0
	MPGlobalsAmbience.iTaxiBitSet = 0
	MPGlobalsAmbience.g_iStopTime = 0
	MPGlobalsAmbience.iAMDrugDealingLastInstance = 0
	MPGlobalsAmbience.iTriggerAlarmBitset = 0
	MPGlobalsAmbience.iPoliceCellDoorBitSet = 0
	MPGlobalsAmbience.iJailCellDoorStateBitset = 0
	MPGlobalsAmbience.iPlayerInLhsCellBitset = 0
	MPGlobalsAmbience.iPlayerInMidCellBitset = 0
	MPGlobalsAmbience.iPlayerInRhsCellBitset = 0
	MPGlobalsAmbience.iPlayerBehindPoliceGate = 0
	MPGlobalsAmbience.iPlayerWantsPoliceGateOpen = 0
	MPGlobalsAmbience.iHeistLocationLockedBitSet = 0
	MPGlobalsAmbience.iPlayerNeedHeistLocOpen = 0
	MPGlobalsAmbience.iServerRequestedLockHeistlocBitset = 0
	MPGlobalsAmbience.iLockHeistDoorsBitset = 0
	MPGlobalsAmbience.iUsingPhoneRequestBitSet = 0
	MPGlobalsAmbience.iVDPersonalVehicleSlot = -1
	MPGlobalsAmbience.iVehicleDropVariation = 0
	MPGlobalsAmbience.iVehicleDropModel = 0
	MPGlobalsAmbience.iTimesWantedForStolenVehicle = 0
	MPGlobalsAmbience.g_iWarpType = 0
	MPGlobalsAmbience.iPrevDispatchHelp = 0
	MPGlobalsAmbience.iAMDoorsLastInstance = 0
	MPGlobalsAmbience.iAmFmImportExportLastInstance = 0
	MPGlobalsAmbience.iNumberOfImportExportWantedCars = 0
	MPGlobalsAmbience.iHpvStuckBitset = 0
	MPGlobalsAmbience.iHoldUpTutLastInstance = 0
	MPGlobalsAmbience.iCarModTutLastInstance = 0
	MPGlobalsAmbience.iAmMissionLastInstance = 0
	MPGlobalsAmbience.iNpcInviteLastInstance = 0
	MPGlobalsAmbience.iNpcInviteBitset = 0
	MPGlobalsAmbience.iPlayerNpcInviteBitset = 0
	MPGlobalsAmbience.iLesterCutLastInstance = 0
	MPGlobalsAmbience.iTrevorCutLastInstance = 0
	MPGlobalsAmbience.iHeistCutLastInstance = 0
	MPGlobalsAmbience.iFmNmhBitSet = 0
	MPGlobalsAmbience.iFmNmhBitSet2 = 0
	MPGlobalsAmbience.iFmNmhBitSet3 = 0
	MPGlobalsAmbience.iFmNmhBitSet4 = 0
	MPGlobalsAmbience.iFmNmhBitSet5 = 0
	MPGlobalsAmbience.iFmNmhBitSet6 = 0
	MPGlobalsAmbience.iFmNmhBitSet7 = 0
	MPGlobalsAmbience.iFmNmhBitSet9 = 0
	MPGlobalsAmbience.iFmNmhBitSet10 = 0
	MPGlobalsAmbience.iFmNmhBitSet11 = 0
	MPGlobalsAmbience.iFmGbHelpBitSet = 0
	MPGlobalsAmbience.iFmActivityHelp = 0
	MPGlobalsAmbience.iWarpingOutOfPropertyWithID = 0
	MPGlobalsAmbience.iPMLaunchCrewSessionNum = PAUSE_MENU_MIN_SESSION_CREWS
	MPGlobalsAmbience.iXPWantedLevel = 0
	MPGlobalsAmbience.iAvailActSlowLoop = 0
	MPGlobalsAmbience.iGlobImpExpBitset = 0
	MPGlobalsAmbience.iPassiveModeCoolDownDelay = -1
	MPGlobalsAmbience.iPassiveModeDisableDelay = -1
	MPGlobalsAmbience.NEW_g_iGarageWarpingOut = 0
	MPGlobalsAmbience.NEW_g_iGaragePlayerIsIn = 0
	MPGlobalsAmbience.g_iGarageCamStage = 0
	MPGlobalsAmbience.g_iWarpOutOfGarageStage = 0
	MPGlobalsAmbience.iDemoMenuBitset = 0
	MPGlobalsAmbience.iSlotToUseForVeh = -1
	MPGlobalsAmbience.iLesterDisableCopsBitset = 0
	MPGlobalsAmbience.iLesterDisableCopsProg = 0
	MPGlobalsAmbience.iMyMaxWantedLevel = 0
	MPGlobalsAmbience.iOffRadarBitset = 0
	MPGlobalsAmbience.iThiefBitSet = 0
	MPGlobalsAmbience.iAmbBitSet = 0
	MPGlobalsAmbience.iAML_ServerBitSet = 0
	
	FOR i = 0 TO 2
		FOR i2 = 0 TO 2	
			MPGlobalsAmbience.iGangBackupVehicleDamage[i][i2] = 0 
		ENDFOR
	ENDFOR
	FOR i = 0 TO MAX_NUM_MISSIONS_ALLOWED -1
		MPGlobalsAmbience.iCopPatrolCrooksArrested[i] = 0
		MPGlobalsAmbience.iCopPatrolCrooksKilled[i] = 0
	ENDFOR
	FOR i = 0 TO ENUM_TO_INT(MAX_NUM_HEIST_LOCATION) - 1
		MPGlobalsAmbience.iRespondedToLockRequest[i] = 0
	ENDFOR
	MPGlobalsAmbience.bInitLowriderCutLaunch = FALSE
	MPGlobalsAmbience.bJustAcceptedCSInvite_ForGarageScript = FALSE
	MPGlobalsAmbience.bIsDPADDOWNActive = TRUE
	MPGlobalsAmbience.bIsDPADDOWNDEACTIVEThisFrame = TRUE
	MPGlobalsAmbience.bUpdatePlayerMoodNormal = FALSE
	MPGlobalsAmbience.bUpdatePlayerMoodDM = FALSE
	MPGlobalsAmbience.bUpdatePlayerMoodRace = FALSE
	MPGlobalsAmbience.bInitedValuesForSC_Collector = FALSE
	MPGlobalsAmbience.bUsingTelescope = FALSE
	MPGlobalsAmbience.bShowingArmoredVanBlip = FALSE
	MPGlobalsAmbience.bBlockWantedLevelRP = FALSE
	MPGlobalsAmbience.bPrintPegasusReclaimTicker = FALSE
	MPGlobalsAmbience.bScannerConvInit = FALSE
	MPGlobalsAmbience.iPrevDispatchTimerInit = FALSE
	MPGlobalsAmbience.bPrevDispatchPlayed = FALSE
	MPGlobalsAmbience.bInitAMDoorsLaunch = FALSE
	MPGlobalsAmbience.bInitFmImportExportLaunch = FALSE
	MPGlobalsAmbience.bPlayerOnImportExportHpv = FALSE
	MPGlobalsAmbience.bPlayerFailedHPVChecks = FALSE
	MPGlobalsAmbience.bLaunchImportExportHpv = FALSE
	MPGlobalsAmbience.bCleanupImportExportHpv = FALSE
	MPGlobalsAmbience.bSendImportExportTextMessage = FALSE
	MPGlobalsAmbience.bHasImportExportList = FALSE
	MPGlobalsAmbience.bIsInImportExportCar = FALSE
	MPGlobalsAmbience.bGangBackupCheckPlayerWarpedOutGarage = FALSE
	MPGlobalsAmbience.bHasBeenDamagedByMercenaries = FALSE
	MPGlobalsAmbience.bInitMpTaxiLaunch = FALSE
	MPGlobalsAmbience.bInitHoldUpTutLaunch = FALSE
	MPGlobalsAmbience.bInitCarModTutLaunch = FALSE
	MPGlobalsAmbience.bLaunchSmallMission = FALSE
	MPGlobalsAmbience.bInitNpcInviteLaunch = FALSE
	MPGlobalsAmbience.bInitLesterCutLaunch = FALSE
	MPGlobalsAmbience.bInitTrevorCutLaunch = FALSE
	MPGlobalsAmbience.bInitHeistCutLaunch = FALSE
	MPGlobalsAmbience.bSomeoneWantsToStartHoldUpTut = FALSE
	MPGlobalsAmbience.bRunningFmIntroCut = FALSE
	MPGlobalsAmbience.bFMIntroCutDemandingDpadDownActive = FALSE
	MPGlobalsAmbience.bDoBountyReminderHelp = FALSE
	MPGlobalsAmbience.b5StartWLTimerInit = FALSE
	MPGlobalsAmbience.bPlayerViewingLeaderboard = FALSE
	MPGlobalsAmbience.bSpawnVehicle = FALSE
	MPGlobalsAmbience.bLaunchBackUpHeli = FALSE
	MPGlobalsAmbience.bLaunchAirstrike = FALSE
	MPGlobalsAmbience.bAirstrikeExplosionsInProgress = FALSE
	MPGlobalsAmbience.bGoneStraightIntoValkyrieFpModeforThisHeli = FALSE
	MPGlobalsAmbience.bPassiveModeInVeh = FALSE
	MPGlobalsAmbience.bPlayerBlipHighlighted = FALSE
	MPGlobalsAmbience.g_bMP_SwapObjects_RunPROC = FALSE
	MPGlobalsAmbience.NEW_g_bPlayerInAGarage = FALSE
	MPGlobalsAmbience.g_bDoorsInited = FALSE
	MPGlobalsAmbience.g_bDisableGarageCam = FALSE
	MPGlobalsAmbience.g_bDisableWarpOutOfLockedGarage = FALSE
	MPGlobalsAmbience.g_bDisableWarpOutOfLockedGarageFade = FALSE
	MPGlobalsAmbience.bInitAMProstituteLaunch = FALSE
	MPGlobalsAmbience.bHoldUpTriggeredEmptyReg = FALSE
	MPGlobalsAmbience.bDisableMissionHintCam = FALSE
	MPGlobalsAmbience.bIsGPSDropOff = FALSE
	MPGlobalsAmbience.bDisableTakeOffChute = FALSE
	MPGlobalsAmbience.g_bPropertyReset = FALSE
	MPGlobalsAmbience.bRappellingDisabled = FALSE
	MPGlobalsAmbience.g_bForceFromVehicle = FALSE
	MPGlobalsAmbience.g_bDisableForceFromVehicle = FALSE
	MPGlobalsAmbience.g_bStopVehicle = FALSE
	MPGlobalsAmbience.bInitAMDrugDealingLaunch = FALSE
	MPGlobalsAmbience.bRunAlarmProc = FALSE
	MPGlobalsAmbience.bRunPoliceCellDoorProc = FALSE
	MPGlobalsAmbience.bInitPoliceCellDoors = FALSE
	MPGlobalsAmbience.g_bmeetingwarp = FALSE
	MPGlobalsAmbience.bLaunchAmmoDrop = FALSE
	MPGlobalsAmbience.bLaunchBoatPickup = FALSE
	MPGlobalsAmbience.bLaunchHeliPickup = FALSE
	MPGlobalsAmbience.bLaunchBruBox = FALSE
	MPGlobalsAmbience.bLaunchVehicleDrop = FALSE
	MPGlobalsAmbience.bLaunchVehicleDropDrive = FALSE
	MPGlobalsAmbience.bLaunchVehicleDropPersonal = FALSE
	MPGlobalsAmbience.bLaunchVehicleDropPersonalTrailer2 = FALSE
	
	MPGlobalsAmbience.bEquipBallisticsDrop  = FALSE
	MPGlobalsAmbience.bRemoveBallisticsDrop   = FALSE 
	MPGlobalsAmbience.bEquipBallisticsIsEquiped  = FALSE
	MPGlobalsAmbience.bLaunchBallisticsDrop  = FALSE
	MPGlobalsAmbience.bLaunchVehicleDropTruck = FALSE
	MPGlobalsAmbience.bLaunchVehicleDropHackerTruck = FALSE
	MPGlobalsAmbience.bLaunchVehicleDropSubmarine = FALSE
	MPGlobalsAmbience.bLaunchVehicleDropSubmarineDinghy = FALSE
	#IF FEATURE_DLC_2_2022
	MPGlobalsAmbience.bLaunchVehicleDropSupportBike = FALSE
	MPGlobalsAmbience.bLaunchVehicleDropAcidLab = FALSE
	#ENDIF
	#IF FEATURE_GEN9_EXCLUSIVE
	MPGlobalsAmbience.bSubSpawnForHeistDeeplink = FALSE
	#ENDIF
	MPGlobalsAmbience.iBallisticsHelp = 0
	MPGlobalsAmbience.bOppressorHelp = FALSE
	MPGlobalsAmbience.beventawardtshirtHelp = FALSE
	MPGlobalsAmbience.beventawardtvehHelp = FALSE
	MPGlobalsAmbience.bheavysniperheavymk2Help = FALSE
	MPGlobalsAmbience.bINSURGTECHMODHELP1Help = FALSE
	MPGlobalsAmbience.bINSURGTECHMODHELP2Help = FALSE
	MPGlobalsAmbience.bINSURGTECHMODHELP3Help = FALSE
	MPGlobalsAmbience.bProximityMineVehicleHelp = FALSE
	///----------------------------------------
	
	MPGlobalsAmbience.bLaunchVehicleDropAvenger  = FALSE
	
	MPGlobalsAmbience.bLaunchVehicleDropPersonalAircraft = FALSE
	MPGlobalsAmbience.bHasInitialisedPVLimiter = FALSE
	
	MPGlobalsAmbience.iRequestedHeliByPA = -1	
	MPGlobalsAmbience.bRequestedImpoundByPA = FALSE
	MPGlobalsAmbience.bRequestedByOfficePA = FALSE
	MPGlobalsAmbience.bForceOppressor2CDOnCreate = FALSE
	
	//MPGlobalsAmbience.bTriggerPropertyExitHeli = FALSE
	//MPGlobalsAmbience.iPropertyPegasusOption = -1
	//MPGlobalsAmbience.iPropertyExitHeliType = -1
	//MPGlobalsAmbience.iPropertyExitHeliDriver = -1
	
	MPGlobalsAmbience.bDestroyedOwnPegasus = FALSE
	
	MPGlobalsAmbience.bForceDeletePV = FALSE
	
	MPGlobalsAmbience.bRequestingVehicleDropPersonal = FALSE
	MPGlobalsAmbience.g_bWarpOutOfGarageTookTooLongFading = FALSE
	MPGlobalsAmbience.g_bWarpOutOfGarageMadePlayerInvisible = FALSE
	MPGlobalsAmbience.g_bWarpOutOfGarageFadeStarted = FALSE
	MPGlobalsAmbience.g_bAllowGetOutOfVehicle = FALSE
	
	MPGlobalsAmbience.bLaunchRCBandito = FALSE
	#IF FEATURE_CASINO_HEIST
	MPGlobalsAmbience.bLaunchRCTank = FALSE
	#ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iHackerTruckModBS, PROPERTY_BROADCAST_HACKER_TRUCK_RC_BANDITO_INITIALISING)
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iHackerTruckModBS, PROPERTY_BROADCAST_HACKER_TRUCK_RC_BANDITO_INITIALISING)
		PRINTLN("SET_PLAYER_INITIALISING_RC_VEHICLE - FALSE")
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sArcadePropertyData.iBS2, PROPERTY_BROADCAST_BS2_ARCADE_RC_TANK_INITIALISING)
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sArcadePropertyData.iBS2, PROPERTY_BROADCAST_BS2_ARCADE_RC_TANK_INITIALISING)
		
		PRINTLN("SET_PLAYER_INITIALISING_RC_TANK - FALSE")
	ENDIF
	#ENDIF
	
	#IF FEATURE_FIXER
	MPGlobalsAmbience.iPersonalVehicleRCHash = -1
	#ENDIF
	
	MPGlobalsAmbience.TaxiCarModel = TAXI
	MPGlobalsAmbience.TaxiDriverModel = A_M_Y_StLat_01
	MPGlobalsAmbience.VDPersonalVehicleModel = DUMMY_MODEL_FOR_SCRIPT
	
	MPGlobalsAmbience.gCarModelOnList = DUMMY_MODEL_FOR_SCRIPT
	
	RESET_SCRIPT_TIMER(MPGlobalsAmbience.iSoldOutTimer)
	RESET_SCRIPT_TIMER(MPGlobalsAmbience.iMaintainCanPickupItemCheckTimer)
	RESET_SCRIPT_TIMER(MPGlobalsAmbience.iLaunchCheckDelay)
	RESET_SCRIPT_TIMER(MPGlobalsAmbience.iHeliLaunchCheckDelay)
	RESET_SCRIPT_TIMER(MPGlobalsAmbience.AssociatedWantedLevelTimer)
	RESET_SCRIPT_TIMER(MPGlobalsAmbience.stAsinFailsafeTimer)
	RESET_SCRIPT_TIMER(MPGlobalsAmbience.g_WarpOutOfGarageFailSafeTimer)
	RESET_SCRIPT_TIMER(MPGlobalsAmbience.g_AllowGetOutOfVehicleTimer)
	RESET_SCRIPT_TIMER(MPGlobalsAmbience.timeCopsDisabled)
	RESET_SCRIPT_TIMER(MPGlobalsAmbience.iPassiveModeCoolDownTimer)
	RESET_SCRIPT_TIMER(MPGlobalsAmbience.iPassiveModeDisableDelayTimer)
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1
		RESET_SCRIPT_TIMER(MPGlobalsAmbience.iSendMessageToPlayerTimer[i])
		RESET_SCRIPT_TIMER(MPGlobalsAmbience.requestPickupDelay[i])
	ENDFOR
	FOR i = 0 TO 6
		RESET_SCRIPT_TIMER(MPGlobalsAmbience.iItemSoldOutTimer[i])
	ENDFOR
	
	RESET_MP_GLOBALS_AMBIENCE_VARS_0()
	RESET_MP_GLOBALS_AMBIENCE_VARS_1()
	RESET_MP_GLOBALS_AMBIENCE_VARS_2()
	RESET_MP_GLOBALS_AMBIENCE_VARS_3()
	RESET_MP_GLOBALS_AMBIENCE_VARS_4()
	RESET_MP_GLOBALS_AMBIENCE_VARS_5()
	
	MPGlobalsAmbience.bPlateDupeDataInitialied = FALSE
	
	REPEAT MAX_OWNED_PROPERTIES i
		REPEAT MAX_SAVED_VEHS_IN_PROPERTY_UPPER_LIMIT i2
			MPGlobalsAmbience.bHasThisGarageVehicleBeenCreatedThisSession[i][i2] = FALSE
		ENDREPEAT
	ENDREPEAT
ENDPROC
PROC RESET_MP_GLOBALS_COMMS()
	g_structMPGlobalsComms EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	g_sCommsMP, EmptyStruct, SIZE_OF(g_structMPGlobalsComms))
	ELSE
		g_sCommsMP = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_MP_GLOBALS_MISSION_TRIGGER()
	g_structMissionTriggeringMP EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	g_sTriggerMP, EmptyStruct, SIZE_OF(g_structMissionTriggeringMP))
	ELSE
		g_sTriggerMP = EmptyStruct
	ENDIF	
ENDPROC
PROC RESET_MP_GLOBALS_MISSION_DETAILS()
	g_structMissionDetailsOverlayMP EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	g_sMDOverlayMP, EmptyStruct, SIZE_OF(g_structMissionDetailsOverlayMP))
	ELSE
		g_sMDOverlayMP = EmptyStruct
	ENDIF	
ENDPROC
PROC RESET_MP_GLOBALS_HUD()
	MPGlobalsHudStruct EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsHud, EmptyStruct, SIZE_OF(MPGlobalsHudStruct))
	ELSE
		MPGlobalsHud = EmptyStruct
	ENDIF	
ENDPROC
PROC RESET_MP_TU_GLOBALS_HUD()
	MPGlobalsHudStruct_TITLEUPDATE EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsHud_TitleUpdate, EmptyStruct, SIZE_OF(MPGlobalsHudStruct_TITLEUPDATE))
	ELSE
		MPGlobalsHud_TitleUpdate = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_MP_GLOBALS_POSITION_HUD()
	MPGlobalsHudPositionStruct EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsHudPosition, EmptyStruct, SIZE_OF(MPGlobalsHudPositionStruct))
	ELSE
		MPGlobalsHudPosition = EmptyStruct
	ENDIF		
ENDPROC

PROC RESET_HUDELEMENT_METER_STUCT()
	HUDELEMENT_METER_STUCT EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsScoreHud.ElementHud_METER, EmptyStruct, SIZE_OF(HUDELEMENT_METER_STUCT))
	ELSE
		MPGlobalsScoreHud.ElementHud_METER = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_HUDELEMENT_CHECKPOINT_STUCT()
	HUDELEMENT_CHECKPOINT_STUCT EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsScoreHud.ElementHud_CHECKPOINT, EmptyStruct, SIZE_OF(HUDELEMENT_CHECKPOINT_STUCT))
	ELSE
		MPGlobalsScoreHud.ElementHud_CHECKPOINT = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_HUDELEMENT_ELIMINATION_STUCT()
	HUDELEMENT_ELIMINATION_STUCT EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsScoreHud.ElementHud_ELIMINATION, EmptyStruct, SIZE_OF(HUDELEMENT_ELIMINATION_STUCT))
	ELSE
		MPGlobalsScoreHud.ElementHud_ELIMINATION = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_HUDELEMENT_SINGLE_NUMBER_STUCT()
	HUDELEMENT_SINGLE_NUMBER_STUCT EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsScoreHud.ElementHud_SINGLENUMBER, EmptyStruct, SIZE_OF(HUDELEMENT_SINGLE_NUMBER_STUCT))
	ELSE
		MPGlobalsScoreHud.ElementHud_SINGLENUMBER = EmptyStruct
	ENDIF	
ENDPROC
PROC RESET_HUDELEMENT_DOUBLE_NUMBER_STUCT()
	HUDELEMENT_DOUBLE_NUMBER_STUCT EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsScoreHud.ElementHud_DOUBLENUMBER, EmptyStruct, SIZE_OF(HUDELEMENT_DOUBLE_NUMBER_STUCT))
	ELSE
		MPGlobalsScoreHud.ElementHud_DOUBLENUMBER = EmptyStruct
	ENDIF	
ENDPROC
PROC RESET_HUDELEMENT_DOUBLE_NUMBER_PLACE_STUCT()
	HUDELEMENT_DOUBLE_NUMBER_PLACE_STUCT EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE, EmptyStruct, SIZE_OF(HUDELEMENT_DOUBLE_NUMBER_PLACE_STUCT))
	ELSE
		MPGlobalsScoreHud.ElementHud_DOUBLENUMPLACE = EmptyStruct
	ENDIF	
ENDPROC
PROC RESET_HUDELEMENT_SCORE_STUCT()
	HUDELEMENT_SCORE_STUCT EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsScoreHud.ElementHud_SCORE, EmptyStruct, SIZE_OF(HUDELEMENT_SCORE_STUCT))
	ELSE
		MPGlobalsScoreHud.ElementHud_SCORE = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_HUDELEMENT_TIMER_STUCT()
	HUDELEMENT_TIMER_STUCT EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsScoreHud.ElementHud_TIMER, EmptyStruct, SIZE_OF(HUDELEMENT_TIMER_STUCT))
	ELSE
		MPGlobalsScoreHud.ElementHud_TIMER = EmptyStruct
	ENDIF	
ENDPROC
PROC RESET_HUDELEMENT_WINDMETER_STRUCT()
	HUDELEMENT_WINDMETER_STRUCT EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsScoreHud.ElementHud_WIND, EmptyStruct, SIZE_OF(HUDELEMENT_WINDMETER_STRUCT))
	ELSE
		MPGlobalsScoreHud.ElementHud_WIND = EmptyStruct
	ENDIF		
ENDPROC

PROC RESET_MP_GLOBALS_SCORE_HUD()
	RESET_HUDELEMENT_METER_STUCT()
	RESET_HUDELEMENT_CHECKPOINT_STUCT()
	RESET_HUDELEMENT_ELIMINATION_STUCT()
	RESET_HUDELEMENT_SINGLE_NUMBER_STUCT()
	RESET_HUDELEMENT_DOUBLE_NUMBER_STUCT()
	RESET_HUDELEMENT_DOUBLE_NUMBER_PLACE_STUCT()
	RESET_HUDELEMENT_SCORE_STUCT()
	RESET_HUDELEMENT_TIMER_STUCT()
	RESET_HUDELEMENT_WINDMETER_STRUCT()
	
	MPGlobalsScoreHud.iHowManyDisplays = 0
	MPGlobalsScoreHud.isSomethingDisplaying = FALSE
	INT iLoop
	SCRIPT_TIMER stTemp
	REPEAT MAX_NUMBER_HUD_ELEMENT iLoop
		MPGlobalsScoreHud.iFlashing_GenericDoubleNumberPlace_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericDoubleNumberPlace_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericDoubleNumber_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericDoubleNumber_MiniHud[iLoop] = stTemp 
		MPGlobalsScoreHud.iFlashing_GenericScore_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericScore_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericNumber_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericNumber_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericTimer_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericTimer_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericElimination_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericElimination_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericCheckpoint_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericCheckpoint_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericMeter_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericMeter_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericFourIconBar_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iFlashing_GenericFourIconBar_MiniHud[iLoop] = stTemp
		
		MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumberPlace_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumberPlace_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumber_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericDoubleNumber_MiniHud[iLoop] = stTemp 
		MPGlobalsScoreHud.iColourFlashing_GenericScore_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericScore_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericNumber_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericNumber_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericTimer_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericTimer_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericElimination_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericElimination_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericCheckpoint_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericCheckpoint_MiniHud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericMeter_Hud[iLoop] = stTemp
		MPGlobalsScoreHud.iColourFlashing_GenericMeter_MiniHud[iLoop] = stTemp


		MPGlobalsScoreHud.iGoalMetFlashing_GenericMeter[iLoop] = stTemp
		MPGlobalsScoreHud.iGoalMetFlashing_GenericScore[iLoop] = stTemp
		MPGlobalsScoreHud.iGoalMetFlashing_GenericDoubleNumberPlace[iLoop] = stTemp
		MPGlobalsScoreHud.iGoalMetFlashing_GenericDoubleNumber[iLoop] = stTemp
		MPGlobalsScoreHud.iGoalMetFlashing_GenericNumber[iLoop] = stTemp
		MPGlobalsScoreHud.iGoalMetFlashing_GenericTimer[iLoop] = stTemp
		MPGlobalsScoreHud.iGoalMetFlashing_GenericElimination[iLoop] = stTemp
		MPGlobalsScoreHud.iGoalMetFlashing_GenericCheckpoint[iLoop] = stTemp
		
		MPGlobalsScoreHud.iGoalFadeFlashing_GenericMeter[iLoop] = 0
		MPGlobalsScoreHud.iGoalFadeFlashing_GenericDoubleNumberPlace[iLoop] = 0
		MPGlobalsScoreHud.iGoalFadeFlashing_GenericDoubleNumber[iLoop] = 0
		MPGlobalsScoreHud.iGoalFadeFlashing_GenericNumber[iLoop] = 0
		MPGlobalsScoreHud.iGoalFadeFlashing_GenericTimer[iLoop] = 0
		MPGlobalsScoreHud.iGoalFadeFlashing_GenericElimination[iLoop] = 0
		MPGlobalsScoreHud.iGoalFadeFlashing_GenericCheckpoint[iLoop] = 0
		MPGlobalsScoreHud.iGoalFadeFlashing_GenericScore[iLoop] = 0
	ENDREPEAT
	
	
	REPEAT NUMBER_OF_PROGRESSHUD_ELEMENTS iLoop
		MPGlobalsScoreHud.ProgressHud_ActivationBitset[iLoop] = 0
		MPGlobalsScoreHud.ProgressHud_LastFrameBitset[iLoop] = 0
		
		MPGlobalsScoreHud.ProgressHud_InitBitset[iLoop] = 0
		
		MPGlobalsScoreHud.ProgressHud_ExtendDisplayBitset[iLoop] = 0
	ENDREPEAT

	MPGlobalsScoreHud.ProgressHud_ForceReset = FALSE
	
	MPGlobalsScoreHud.TopOfTimers = 0.0
ENDPROC
PROC RESET_MP_GLOBALS_EVENTS()
	MPGlobalsEventsStruct EmptyStruct
	MPGlobalsEvents = EmptyStruct
ENDPROC
PROC RESET_MP_PLAYER_BLIP_DATA()
	PLAYER_BLIP_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	g_PlayerBlipsData, EmptyStruct, SIZE_OF(PLAYER_BLIP_DATA))
	ELSE
		g_PlayerBlipsData = EmptyStruct
	ENDIF	
ENDPROC
//#IF FEATURE_GANG_BOSS
//PROC RESET_GANG_BOSS_DATA()
//	GANG_BOSS_DATA EmptyStruct
//	g_GangBossData = EmptyStruct
//ENDPROC
//#ENDIF
PROC  RESET_STRUCT_MP_BETTING()
	STRUCT_MP_BETTING EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobals.g_MPBettingData, EmptyStruct, SIZE_OF(STRUCT_MP_BETTING))
	ELSE
		MPGlobals.g_MPBettingData = EmptyStruct
	ENDIF	
ENDPROC
PROC  RESET_VEHICLE_APP_DATA()
	SAVED_VEHICLE_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobals.VehicleData, EmptyStruct, SIZE_OF(SAVED_VEHICLE_DATA))
	ELSE
		MPGlobals.VehicleData = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_GLOBAL_SPAWN_DATA()
	SPAWN_INFO EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	g_SpawnData, EmptyStruct, SIZE_OF(SPAWN_INFO))
	ELSE
		g_SpawnData = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_KILL_STRIP_DATA()
	KILL_STRIP_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobals.g_KillStrip, EmptyStruct, SIZE_OF(KILL_STRIP_DATA))
	ELSE
		MPGlobals.g_KillStrip = EmptyStruct
	ENDIF		
ENDPROC
PROC  RESET_KILL_STRIP_VOICE_DATA()
	KILL_STRIP_VOICE_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobals.g_KillStripVoiceData, EmptyStruct, SIZE_OF(KILL_STRIP_VOICE_DATA))
	ELSE
		MPGlobals.g_KillStripVoiceData = EmptyStruct
	ENDIF		
ENDPROC
PROC  RESET_BIG_MESSAGE()
	INT i
	BIG_MESSAGE EmptyStruct
	FOR i = 0 TO (NUM_BIG_MESSAGE - 1)
		IF (g_bUseStackCopy)
			COPY_SCRIPT_STRUCT(	MPGlobals.g_BigMessage[i], EmptyStruct, SIZE_OF(BIG_MESSAGE))
		ELSE
			MPGlobals.g_BigMessage[i] = EmptyStruct
		ENDIF		
	ENDFOR
ENDPROC

PROC  RESET_STRUCT_SECURITY_VAN_GLOBAL_AMBIENCE_LOCAL_DATA()
	STRUCT_SECURITY_VAN_GLOBAL_AMBIENCE_LOCAL_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobals.sSecVanAmbdata, EmptyStruct, SIZE_OF(STRUCT_SECURITY_VAN_GLOBAL_AMBIENCE_LOCAL_DATA))
	ELSE
		MPGlobals.sSecVanAmbdata = EmptyStruct
	ENDIF		
ENDPROC
PROC  RESET_MINIGAME_MP_GLOBALS()
	MINIGAME_MP_GLOBALS EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobals.sMinigameMPGlobals, EmptyStruct, SIZE_OF(MINIGAME_MP_GLOBALS))
	ELSE
		MPGlobals.sMinigameMPGlobals = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_CORONA_POS_DATA() 
	CORONA_POS_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobals.coronaPosData, EmptyStruct, SIZE_OF(CORONA_POS_DATA))
	ELSE
		MPGlobals.coronaPosData = EmptyStruct
	ENDIF		
ENDPROC
PROC  RESET_STRUCT_ARREST_EVENT_DATA()
	INT i
	STRUCT_ARREST_EVENT_DATA EmptyStruct
	FOR i = 0 TO (ARREST_EVENT_ARRAY_SIZE - 1)
		IF (g_bUseStackCopy)
			COPY_SCRIPT_STRUCT(	MPGlobals.g_structArrestEventArray[i], EmptyStruct, SIZE_OF(STRUCT_ARREST_EVENT_DATA))
		ELSE
			MPGlobals.g_structArrestEventArray[i] = EmptyStruct
		ENDIF	
	ENDFOR
ENDPROC

PROC  RESET_XP_ANIM()
	INT i
	XP_ANIM EmptyStruct
	FOR i = 0 TO (NUM_XP_ANIMS - 1)
		IF (g_bUseStackCopy)
			COPY_SCRIPT_STRUCT(	MPGlobals.XpAnims[i], EmptyStruct, SIZE_OF(XP_ANIM))
		ELSE
			MPGlobals.XpAnims[i] = EmptyStruct
		ENDIF		
	ENDFOR
ENDPROC
PROC RESET_INTERATIONS_GLOBALS()
	MPGlobalsInteractionsStruct EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(MPGlobalsInteractions, EmptyStruct, SIZE_OF(MPGlobalsInteractionsStruct))
	ELSE
		MPGlobalsInteractions = EmptyStruct
	ENDIF	
ENDPROC
PROC RESET_MP_GLOBALS_FW()
	MPGlobalsFWStruct EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(MPGlobalsFW, EmptyStruct, SIZE_OF(MPGlobalsFWStruct))
	ELSE
		MPGlobalsFW = EmptyStruct
	ENDIF	
ENDPROC
PROC RESET_PRIVATE_YACHT_GLOBALS()
	MPGlobalsPrivateYachtStruct EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(MPGlobalsPrivateYacht, EmptyStruct, SIZE_OF(MPGlobalsPrivateYachtStruct))
	ELSE
		MPGlobalsPrivateYacht = EmptyStruct
	ENDIF		
ENDPROC

PROC RESET_OFFICE_HELI_DOCK_GLOBALS()
	OFFICE_HELI_DOCK_GLOBAL_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(g_OfficeHeliDockData, EmptyStruct, SIZE_OF(OFFICE_HELI_DOCK_GLOBAL_DATA))
	ELSE
		g_OfficeHeliDockData = EmptyStruct
	ENDIF
ENDPROC
PROC RESET_HELI_TAXI_QUICK_TRAVEL_GLOBALS()
	HELI_TAXI_QUICK_TRAVEL_GLOBAL_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(g_HeliTaxiQuickTravelData, EmptyStruct, SIZE_OF(HELI_TAXI_QUICK_TRAVEL_GLOBAL_DATA))
	ELSE
		g_HeliTaxiQuickTravelData = EmptyStruct
	ENDIF	
ENDPROC
PROC RESET_SIMPLE_INTERIOR_GLOBALS()
	SIMPLE_INTERIOR_GLOBAL_DATA EmptyStruct	
	SIMPLE_INTERIORS eIntScriptKilled = g_SimpleInteriorData.eKilledScriptForInteriorID // Keep the script that was killed
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(g_SimpleInteriorData, EmptyStruct, SIZE_OF(SIMPLE_INTERIOR_GLOBAL_DATA))
	ELSE
		g_SimpleInteriorData = EmptyStruct
	ENDIF		
	g_SimpleInteriorData.eKilledScriptForInteriorID = eIntScriptKilled

ENDPROC

PROC RESET_IE_DELIVERY_GLOBALS()
	IE_DELIVERY_GLOBAL_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(g_IEDeliveryData, EmptyStruct, SIZE_OF(IE_DELIVERY_GLOBAL_DATA))
	ELSE
		g_IEDeliveryData = EmptyStruct
	ENDIF		
ENDPROC

PROC RESET_MP_RESTRICTED_INTERIOR_ACCESS_GLOBALS()
	MP_RESTRICTED_INTERIOR_ACCESS_GLOBAL_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(g_MPRestrictedInteriorAccessData, EmptyStruct, SIZE_OF(MP_RESTRICTED_INTERIOR_ACCESS_GLOBAL_DATA))
	ELSE
		g_MPRestrictedInteriorAccessData = EmptyStruct
	ENDIF	
ENDPROC

PROC RESET_ASYNC_GRID_SPAWN_GLOBALS()
	ASYNC_GRID_SPAWN_GLOBAL_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(g_AsyncGridSpawnData, EmptyStruct, SIZE_OF(ASYNC_GRID_SPAWN_GLOBAL_DATA))
	ELSE
		g_AsyncGridSpawnData = EmptyStruct
	ENDIF
ENDPROC

PROC RESET_FREEMODE_DELIVERY_GLOBALS()
	FREEMODE_DELIVERY_GLOBAL_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(g_FreemodeDeliveryData, EmptyStruct, SIZE_OF(FREEMODE_DELIVERY_GLOBAL_DATA))
	ELSE
		g_FreemodeDeliveryData = EmptyStruct
	ENDIF	
ENDPROC

// *****************************************************
//  These don't get called anywhere, should they??
// ****************************************************
PROC RESET_MP_GLOBALS()
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobals, MPGlobalsEmptyCopy, SIZE_OF(MPGlobalsStruct))
	ELSE
		MPGlobals = MPGlobalsEmptyCopy
	ENDIF
ENDPROC
PROC RESET_MP_GLOBALS_STATS()
	MPGlobalsStatsStruct EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsStats, EmptyStruct, SIZE_OF(MPGlobalsStatsStruct))
	ELSE	
		MPGlobalsStats = EmptyStruct
	ENDIF
ENDPROC
PROC RESET_MP_GLOBALS_REWARDS()
	#IF IS_DEBUG_BUILD
	
		PRINTLN("Flushing stats1 RESET_MP_GLOBALS_REWARDS ")
	#ENDIF
	MPGlobalsHudRewardsStruct EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPGlobalsHudRewards, EmptyStruct, SIZE_OF(MPGlobalsHudRewardsStruct))
	ELSE
		MPGlobalsHudRewards = EmptyStruct
	ENDIF
	
	
ENDPROC

PROC RESET_MP_GLOBALS_SPECTATOR()
	MPSpectatorGlobalStruct EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	MPSpecGlobals, EmptyStruct, SIZE_OF(MPSpectatorGlobalStruct))
	ELSE
		MPSpecGlobals = EmptyStruct
	ENDIF
ENDPROC

PROC RESET_MP_GLOBALS_HOLD_UP()
	GlobalServerBroadcastDataHoldUp EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	GlobalServerBD_HoldUp, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataHoldUp))
	ELSE
		GlobalServerBD_HoldUp = EmptyStruct
	ENDIF	
ENDPROC

PROC RESET_MP_GLOBALS_MISSION_REQUEST()
	GlobalServerBroadcastDataMissionRequest EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	GlobalServerBD_MissionRequest, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataMissionRequest))
	ELSE
		GlobalServerBD_MissionRequest = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_MP_GLOBALS_MISSION_LIST()
	GlobalServerBroadcastDataMissionList EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	GlobalServerBD_MissionList, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataMissionList))
	ELSE
		GlobalServerBD_MissionList = EmptyStruct
	ENDIF	
ENDPROC
PROC RESET_MP_GLOBALS_EXCLUSION_AREAS()
	GlobalServerBroadcastDataExclusionArea EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	GlobalServerBD_ExclusionAreas, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataExclusionArea))
	ELSE
		GlobalServerBD_ExclusionAreas = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_MP_GLOBALS_ACTIVITY_SELECTOR()
	GlobalServerBroadcastDataActivitySelector EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	GlobalServerBD_ActivitySelector, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataActivitySelector))
	ELSE
		GlobalServerBD_ActivitySelector = EmptyStruct
	ENDIF		
ENDPROC
PROC RESET_MP_GLOBALS_BETTING()
	GlobalServerBroadcastDataBetting EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(	GlobalServerBD_Betting, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataBetting))
	ELSE
		GlobalServerBD_Betting = EmptyStruct
	ENDIF		
ENDPROC


PROC RESET_HANGAR_GIFT_PLANE_STRUCT()
	hangarGiftPlane.bAttemptedToAward = FALSE
	hangarGiftPlane.bTransactionCompleted = FALSE
	hangarGiftPlane.iTransactionResult = 0
ENDPROC

PROC RESET_NIGHTCLUB_GIFT_DELIVERY_VEHICLE_STRUCT()
	nightclubGiftDeliveryVehicle.bAttemptedToAward = FALSE
	nightclubGiftDeliveryVehicle.bTransactionCompleted = FALSE
	nightclubGiftDeliveryVehicle.iTransactionResult = 0
ENDPROC

//PROC RESET_HACKER_TRUCK_GIFT_DELIVERY_VEHICLE_STRUCT()
//	hackerTruckGiftDeliveryVehicle.bAttemptedToAward = FALSE
//	hackerTruckGiftDeliveryVehicle.bTransactionCompleted = FALSE
//	hackerTruckGiftDeliveryVehicle.iTransactionResult = 0
//ENDPROC

// ********* Reset all of the above **********
PROC RESET_ALL_GLOBAL_STRUCTS()

	#IF IS_DEBUG_BUILD
	INT iStackSizeStart = GET_CURRENT_STACK_SIZE()
	PRINTLN("RESET_MP_GLOBALS_AMBIENCE - BEFORE - GET_CURRENT_STACK_SIZE() = ", iStackSizeStart)
	#ENDIF
	
	RESET_MP_GLOBALS_AMBIENCE()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_GLOBALS_AMBIENCE - GET_ALLOCATED_STACK_SIZE = ") PRINTINT(GET_ALLOCATED_STACK_SIZE())PRINTSTRING(" GET_CURRENT_STACK_SIZE = ")PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF	
			
	RESET_MP_GLOBALS_COMMS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_GLOBALS_COMMS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF	
	
	RESET_MP_GLOBALS_MISSION_TRIGGER()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_GLOBALS_MISSION_TRIGGER - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF	
	
	RESET_MP_GLOBALS_MISSION_DETAILS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_GLOBALS_MISSION_DETAILS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF	
	
	RESET_MP_GLOBALS_HUD()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_GLOBALS_HUD - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF	
	
	RESET_MP_TU_GLOBALS_HUD()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_TU_GLOBALS_HUD - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF		
	

	RESET_MP_GLOBALS_POSITION_HUD()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_GLOBALS_POSITION_HUD - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF	
	
	PRINTSTRING("[STACK] SIZE_OF(MPGlobalsScoreHudStruct) = ")PRINTINT(SIZE_OF(MPGlobalsScoreHudStruct))PRINTNL()
	
	//Done in timershud.sc now
//	RESET_MP_GLOBALS_SCORE_HUD()
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING("[STACK] RESET_MP_GLOBALS_SCORE_HUD - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
//	#ENDIF	
	
	RESET_MP_GLOBALS_SPECTATOR()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_GLOBALS_SPECTATOR - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF

//	RESET_MP_GLOBALS_STATS() 
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING("RESET_MP_GLOBALS_STATS - GET_ALLOCATED_STACK_SIZE = ") PRINTINT(GET_ALLOCATED_STACK_SIZE()) PRINTNL()
//	#ENDIF	
	
	RESET_MP_GLOBALS_EVENTS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_GLOBALS_EVENTS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF		
	
	RESET_MP_PLAYER_BLIP_DATA()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_PLAYER_BLIP_DATA - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF	
	
//	#IF FEATURE_GANG_BOSS
//	RESET_GANG_BOSS_DATA()
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING("[STACK] RESET_GANG_BOSS_DATA - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
//	#ENDIF	
//	#ENDIF
	
	RESET_GLOBAL_SPAWN_DATA()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_GLOBAL_SPAWN_DATA - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF	
	
	RESET_MP_GLOBALS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_GLOBALS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	
//	RESET_PROPERTY_MAINTAIN_GLOBALS()
//	#IF IS_DEBUG_BUILD
//		PRINTSTRING("RESET_PROPERTY_MAINTAIN_GLOBALS - GET_ALLOCATED_STACK_SIZE = ") PRINTINT(GET_ALLOCATED_STACK_SIZE()) PRINTNL()
//	#ENDIF
	
	RESET_INTERATIONS_GLOBALS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_INTERATIONS_GLOBALS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	
	RESET_MP_GLOBALS_HOLD_UP()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_GLOBALS_HOLD_UP - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	
	RESET_MP_GLOBALS_MISSION_REQUEST()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[STACK] RESET_MP_GLOBALS_MISSION_REQUEST - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	RESET_MP_GLOBALS_MISSION_LIST()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_MP_GLOBALS_MISSION_LIST - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	RESET_MP_GLOBALS_EXCLUSION_AREAS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_MP_GLOBALS_EXCLUSION_AREAS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	RESET_MP_GLOBALS_ACTIVITY_SELECTOR()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_MP_GLOBALS_ACTIVITY_SELECTOR - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	RESET_MP_GLOBALS_BETTING()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_MP_GLOBALS_BETTING - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	RESET_MP_GLOBALS_FW()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_MP_GLOBALS_FW - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	RESET_PRIVATE_YACHT_GLOBALS()	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_PRIVATE_YACHT_GLOBALS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	
	
	RESET_OFFICE_HELI_DOCK_GLOBALS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_OFFICE_HELI_DOCK_GLOBALS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	RESET_HELI_TAXI_QUICK_TRAVEL_GLOBALS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_HELI_TAXI_QUICK_TRAVEL_GLOBALS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	RESET_SIMPLE_INTERIOR_GLOBALS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_SIMPLE_INTERIOR_GLOBALS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	
	RESET_IE_DELIVERY_GLOBALS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_IE_DELIVERY_GLOBALS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	
	RESET_MP_RESTRICTED_INTERIOR_ACCESS_GLOBALS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_MP_RESTRICTED_INTERIOR_ACCESS_GLOBALS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	
	RESET_ASYNC_GRID_SPAWN_GLOBALS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_ASYNC_GRID_SPAWN_GLOBALS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	
	RESET_FREEMODE_DELIVERY_GLOBALS()
	#IF IS_DEBUG_BUILD
		PRINTSTRING("RESET_FREEMODE_DELIVERY_GLOBALS - GET_CURRENT_STACK_SIZE = ") PRINTINT(GET_CURRENT_STACK_SIZE()) PRINTNL()
	#ENDIF
	
	RESET_HANGAR_GIFT_PLANE_STRUCT()
	
	RESET_NIGHTCLUB_GIFT_DELIVERY_VEHICLE_STRUCT()
	
	g_bAvengerRemovalRUN = FALSE
	#IF FEATURE_GEN9_EXCLUSIVE
	g_bRunGEN9PatchLicencePlates = FALSE
	#ENDIF
	RESET_NET_TIMER(g_stAllowAvengersRemovalTimer)
	#IF IS_DEBUG_BUILD
	INT iStackSizeEnd = GET_CURRENT_STACK_SIZE()
	INT iStackSizeDiff = iStackSizeEnd - iStackSizeStart
	PRINTLN("RESET_MP_GLOBALS_AMBIENCE - AFTER - GET_CURRENT_STACK_SIZE() = ", iStackSizeEnd, " diff = ", iStackSizeDiff)
	#ENDIF
	
ENDPROC






// *****  GLOBAL PLAYER BROADCAST DATA *****
PROC CLEAR_GlobalplayerBD(INT iPlayer)
	GlobalPlayerBroadcastData EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalplayerBD[iPlayer], EmptyStruct, SIZE_OF(GlobalPlayerBroadcastData))
	ELSE
		GlobalplayerBD[iPlayer] = EmptyStruct
	ENDIF		
ENDPROC	

PROC CLEAR_GlobalplayerBD_FM(INT iPlayer)
	GlobalPlayerBroadcastDataFM EmptyPlayerDataFM
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalplayerBD_FM[iPlayer], EmptyPlayerDataFM, SIZE_OF(GlobalPlayerBroadcastDataFM))
	ELSE
		GlobalplayerBD_FM[iPlayer] = EmptyPlayerDataFM
	ENDIF		
	
	// KGM 9/10/13: Added this in here because it is a TL63 mission name replacement for a now-obsolete TL31 version in the above struct
	GlobalPlayerBroadcastDataMN EmptyPlayerDataMissionName
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalplayerBD_MissionName[iPlayer], EmptyPlayerDataMissionName, SIZE_OF(GlobalPlayerBroadcastDataMN))
	ELSE
		GlobalplayerBD_MissionName[iPlayer] = EmptyPlayerDataMissionName
	ENDIF		

ENDPROC

PROC CLEAR_GlobalplayerBD_DM(INT iPlayer)
	GlobalPlayerBroadcastDataDM EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalplayerBD_DM[iPlayer], EmptyStruct, SIZE_OF(GlobalPlayerBroadcastDataDM))
	ELSE
		GlobalplayerBD_DM[iPlayer] = EmptyStruct
	ENDIF		
ENDPROC
PROC CLEAR_GlobalplayerBD_Races(INT iPlayer)
	GlobalPlayerBroadcastDataRaces EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalplayerBD_Races[iPlayer], EmptyStruct, SIZE_OF(GlobalPlayerBroadcastDataRaces))
	ELSE
		GlobalplayerBD_Races[iPlayer] = EmptyStruct
	ENDIF		
ENDPROC

PROC CLEAR_GlobalplayerBD_FM_2(INT iPlayer)
	GlobalPlayerBroadcastDataFM_2 EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalplayerBD_FM_2[iPlayer], EmptyStruct, SIZE_OF(GlobalPlayerBroadcastDataFM_2))
	ELSE
		GlobalplayerBD_FM_2[iPlayer] = EmptyStruct
	ENDIF	
ENDPROC

PROC CLEAR_GlobalplayerBD_Kicking(INT iPlayer)
	GlobalplayerBroadcastDataKicking EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalplayerBD_Kicking[iPlayer], EmptyStruct, SIZE_OF(GlobalplayerBroadcastDataKicking))
	ELSE
		GlobalplayerBD_Kicking[iPlayer] = EmptyStruct
	ENDIF	
ENDPROC

PROC CLEAR_GlobalplayerBD_SCTV(INT iPlayer)
	GlobalPlayerBroadcastDataSCTV EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalPlayerBD_SCTV[iPlayer], EmptyStruct, SIZE_OF(GlobalPlayerBroadcastDataSCTV))
	ELSE
		GlobalPlayerBD_SCTV[iPlayer] = EmptyStruct
	ENDIF	
ENDPROC

// AMEC HEISTS
PROC CLEAR_GlobalplayerBD_FM_HeistPlanning(INT iPlayer)
	GlobalPlayerBroadcastDataFM_HeistPlanning EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalplayerBD_FM_HeistPlanning[iPlayer], EmptyStruct, SIZE_OF(GlobalPlayerBroadcastDataFM_HeistPlanning))
	ELSE
		GlobalplayerBD_FM_HeistPlanning[iPlayer] = EmptyStruct
	ENDIF	
ENDPROC

PROC CLEAR_GlobalplayerBD_Tattoos(INT iPlayer)
	GlobalPlayerBroadcastDataTattoos EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalplayerBD_Tattoos[iPlayer], EmptyStruct, SIZE_OF(GlobalPlayerBroadcastDataTattoos))
	ELSE
		GlobalplayerBD_Tattoos[iPlayer] = EmptyStruct
	ENDIF	
ENDPROC

PROC CLEAR_GlobalplayerBD_Stats(INT iPlayer)
	GlobalPlayerBroadcastDataStats EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalplayerBD_Stats[iPlayer], EmptyStruct, SIZE_OF(GlobalPlayerBroadcastDataStats))
	ELSE
		GlobalplayerBD_Stats[iPlayer] = EmptyStruct
	ENDIF		
ENDPROC


// ***** GLOBAL SERVER BROADCAST DATA *****
PROC CLEAR_GlobalServerBD()
	GlobalServerBroadcastData EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD, EmptyStruct, SIZE_OF(GlobalServerBroadcastData))
	ELSE
		GlobalServerBD= EmptyStruct
	ENDIF		
ENDPROC

// KGM: 6/5/14: Clearing out server broadcast data registered with Freemode that hasn't been getting cleared
PROC CLEAR_GlobalServerBD_BlockB()
	GlobalServerBroadcastDataB EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_BlockB, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataB))
	ELSE
		GlobalServerBD_BlockB= EmptyStruct
	ENDIF	
ENDPROC

PROC CLEAR_GlobalServerBD_BlockC()
	GlobalServerBroadcastDataC EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_BlockC, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataC))
	ELSE
		GlobalServerBD_BlockC = EmptyStruct
	ENDIF
ENDPROC

// KGM: 6/5/14: Clearing out server broadcast data registered with Freemode that hasn't been getting cleared
PROC CLEAR_GlobalServerBD_MissionRequest()
	GlobalServerBroadcastDataMissionRequest EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_MissionRequest, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataMissionRequest))
	ELSE
		GlobalServerBD_MissionRequest= EmptyStruct
	ENDIF	
ENDPROC

// KGM: 6/5/14: Clearing out server broadcast data registered with Freemode that hasn't been getting cleared
PROC CLEAR_GlobalServerBD_MissionList()
	GlobalServerBroadcastDataMissionList EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_MissionList, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataMissionList))
	ELSE
		GlobalServerBD_MissionList = EmptyStruct
	ENDIF	
ENDPROC

// KGM: 6/5/14: Clearing out server broadcast data registered with Freemode that hasn't been getting cleared
PROC CLEAR_GlobalServerBD_ExclusionAreas()
	GlobalServerBroadcastDataExclusionArea EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_ExclusionAreas, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataExclusionArea))
	ELSE
		GlobalServerBD_ExclusionAreas = EmptyStruct
	ENDIF		
ENDPROC

// KGM: 6/5/14: Clearing out server broadcast data registered with Freemode that hasn't been getting cleared
PROC CLEAR_GlobalServerBD_ActivitySelector()
	GlobalServerBroadcastDataActivitySelector EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_ActivitySelector, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataActivitySelector))
	ELSE
		GlobalServerBD_ActivitySelector = EmptyStruct
	ENDIF	
ENDPROC

// KGM: 6/5/14: Clearing out server broadcast data registered with Freemode that hasn't been getting cleared
PROC CLEAR_GlobalServerBD_Betting()
	GlobalServerBroadcastDataBetting EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_Betting, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataBetting))
	ELSE
		GlobalServerBD_Betting = EmptyStruct
	ENDIF	
ENDPROC

// KGM: 6/5/14: Clearing out server broadcast data registered with Freemode that hasn't been getting cleared
PROC CLEAR_GlobalServerBD_HoldUp()
	GlobalServerBroadcastDataHoldUp EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_HoldUp, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataHoldUp))
	ELSE
		GlobalServerBD_HoldUp = EmptyStruct
	ENDIF	
ENDPROC

PROC CLEAR_GlobalServerBD_SyncedInteractions()
	SERVER_INTERACTION_BROADCAST_DATA EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_SyncedInteractions, EmptyStruct, SIZE_OF(SERVER_INTERACTION_BROADCAST_DATA))
	ELSE
		GlobalServerBD_SyncedInteractions = EmptyStruct
	ENDIF		
ENDPROC

// KGM: 6/5/14: Clearing out server broadcast data registered with Freemode that hasn't been getting cleared
PROC CLEAR_GlobalServerBD_MissionsShared()
	g_structMissionsSharedServerBD EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_MissionsShared, EmptyStruct, SIZE_OF(g_structMissionsSharedServerBD))
	ELSE
		GlobalServerBD_MissionsShared = EmptyStruct
	ENDIF		
ENDPROC


PROC CLEAR_GlobalServerBD_FM()
	INT i
	GlobalServerBroadcastDataFM EmptyStruct
	REPEAT GANG_CALL_TYPE_TOTAL_NUMBER i
		EmptyStruct.iGangCallCurrentOwnerPlayer[i] = -1
		EmptyStruct.iGangCallCurrentTargetPlayer[i] = -1
	ENDREPEAT
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_FM, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataFM))
	ELSE
		GlobalServerBD_FM = EmptyStruct
	ENDIF
ENDPROC
PROC CLEAR_GlobalServerBD_DM()
	GlobalServerBroadcastDataDM EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_DM, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataDM))
	ELSE
		GlobalServerBD_DM = EmptyStruct
	ENDIF	
ENDPROC
PROC CLEAR_GlobalServerBD_Races()
	GlobalServerBroadcastDataRaces EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_Races, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataRaces))
	ELSE
		GlobalServerBD_Races = EmptyStruct
	ENDIF		
ENDPROC

PROC CLEAR_GlobalServerBD_Kicking()
	GlobalServerBroadcastDataKicking EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_Kicking, EmptyStruct, SIZE_OF(GlobalServerBroadcastDataKicking))
	ELSE
		GlobalServerBD_Kicking = EmptyStruct
	ENDIF	
ENDPROC

// AMEC HEISTS

PROC CLEAR_GlobalServerBD_HeistPlanning
	HEIST_SERVER_PLANNING_BD EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_HeistPlanning, EmptyStruct, SIZE_OF(HEIST_SERVER_PLANNING_BD))
	ELSE
		GlobalServerBD_HeistPlanning = EmptyStruct
	ENDIF		
ENDPROC

PROC CLEAR_GlobalServerBD_PropertyInstances()
	PROPERTY_SERVER_INSTANCE_BD EmptyStruct
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(GlobalServerBD_PropertyInstances, EmptyStruct, SIZE_OF(PROPERTY_SERVER_INSTANCE_BD))
	ELSE
		GlobalServerBD_PropertyInstances = EmptyStruct
	ENDIF		
ENDPROC

//PROC CLEAR_GlobalServerBD_HeistPrePlanning
//	HEIST_SERVER_PRE_PLANNING_BD EmptyServerDataHeistPrePlanning
//	GlobalServerBD_HeistPrePlanning = EmptyServerDataHeistPrePlanning
//ENDPROC

// ****** CLEAR ALL PROCS ******
PROC CLEAR_ALL_GLOBAL_PLAYER_BROADCAST_DATA(INT iPlayer)
	CLEAR_GlobalplayerBD(iPlayer)
	CLEAR_GlobalplayerBD_FM(iPlayer)		// includes GlobalplayerBD_MissionName
	CLEAR_GlobalplayerBD_DM(iPlayer)
	CLEAR_GlobalplayerBD_Races(iPlayer)
	CLEAR_GlobalplayerBD_FM_2(iPlayer)
	CLEAR_GlobalplayerBD_Kicking(iPlayer)
	
	// KGM 6/5/14: These global player BD structs are not getting cleared when Freemode ends - they should.
	CLEAR_GlobalplayerBD_SCTV(iPlayer)
	// KGM 6/5/14: END NEW GLOBAL SERVER STRUCT CLEAROUT
	
	// AMEC HEISTS
		CLEAR_GlobalplayerBD_FM_HeistPlanning(iPlayer)
		CLEAR_GlobalplayerBD_Tattoos(iPlayer)
	
	CLEAR_GlobalplayerBD_Stats(iPlayer)
ENDPROC
PROC CLEAR_ALL_GLOBAL_SERVER_BROADCAST_DATA()
	CLEAR_GlobalServerBD()
	CLEAR_GlobalServerBD_FM()
	CLEAR_GlobalServerBD_DM()
	CLEAR_GlobalServerBD_Races()
	CLEAR_GlobalServerBD_Kicking()

	
	// KGM 6/5/14: These global server BD structs are not getting cleared when Freemode ends - they should.
	// NOTE:		These are the new structs that were split off from GlobalServerBD when it became too large
	CLEAR_GlobalServerBD_BlockB()
	CLEAR_GlobalServerBD_BlockC()
	CLEAR_GlobalServerBD_MissionRequest()
	CLEAR_GlobalServerBD_MissionList()
	CLEAR_GlobalServerBD_ExclusionAreas()
	CLEAR_GlobalServerBD_ActivitySelector()
	CLEAR_GlobalServerBD_Betting()
	CLEAR_GlobalServerBD_HoldUp()
	CLEAR_GlobalServerBD_MissionsShared()
	CLEAR_GlobalServerBD_SyncedInteractions()
	// KGM 6/5/14: END NEW GLOBAL SERVER STRUCT CLEAROUT
	
	// AMEC HEISTS
		CLEAR_GlobalServerBD_HeistPlanning()
//		CLEAR_GlobalServerBD_HeistPrePlanning()		
		CLEAR_GlobalServerBD_PropertyInstances()	
ENDPROC


PROC CLEAR_ALL_GLOBAL_BROADCAST_DATA(BOOL bStopBroadcastingIfInSession=FALSE)

	#IF IS_DEBUG_BUILD
		PRINTSTRING("CLEAR_ALL_GLOBAL_BROADCAST_DATA - called") PRINTNL()
	#ENDIF

	INT iPlayer
	
	IF (bStopBroadcastingIfInSession)
		IF (NETWORK_IS_IN_SESSION() AND NETWORK_IS_GAME_IN_PROGRESS())
			#IF IS_DEBUG_BUILD
				PRINTSTRING("CLEAR_ALL_GLOBAL_BROADCAST_DATA - Still In Network Session, so calling: NETWORK_FINISH_BROADCASTING_DATA()") PRINTNL()
			#ENDIF			
			NETWORK_FINISH_BROADCASTING_DATA()
		ENDIF
	ENDIF
	
	// players broadcast data
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		CLEAR_ALL_GLOBAL_PLAYER_BROADCAST_DATA(iPlayer)	
	ENDREPEAT	
	
	// server broadcast data
	CLEAR_ALL_GLOBAL_SERVER_BROADCAST_DATA()

ENDPROC

PROC CLEAR_ALL_MP_GLOBALS()

	#IF IS_DEBUG_BUILD
	INT iStackSizeStart = GET_CURRENT_STACK_SIZE()
	PRINTLN("CLEAR_ALL_MP_GLOBALS - BEFORE - GET_CURRENT_STACK_SIZE() = ", iStackSizeStart)
	#ENDIF

	g_bSyncedBroadcastFromFreemode = FALSE
	CLEAR_ALL_GLOBAL_BROADCAST_DATA()
	RESET_ALL_GLOBAL_STRUCTS()
	
	#IF IS_DEBUG_BUILD
	INT iStackSizeEnd = GET_CURRENT_STACK_SIZE()
	INT iStackSizeDiff = iStackSizeEnd - iStackSizeStart
	PRINTLN("CLEAR_ALL_MP_GLOBALS - AFTER - GET_CURRENT_STACK_SIZE() = ", iStackSizeEnd, " diff = ", iStackSizeDiff)
	#ENDIF	
	
ENDPROC





