USING "rage_builtins.sch"
USING "globals.sch"

USING "dialogue_public.sch"
USING "net_comms_public.sch"

#IF IS_DEBUG_BUILD
	USING "net_comms_debug.sch"
#ENDIF




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_comms_stored.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains MP Communication requests that can't be maintained by the requesting script
//								(eg: if the communication comes from a script event).
//		NOTES			:	A maintenance function will be called each frame from the MP Comms script.
//							This was added for the Assassination mission where the communications are sent to players that
//								are not on the mission.
//							The Global Control Variables were converted into an array to allow multiple types of Stored Communications
//								to be controlled through this script. Initially this allowed General Stored Comms (and an old CnC use).
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      General MP Stored Comms functions
// ===========================================================================================================

// PURPOSE:	Clear out the global Stored Comms variables
//
// INPUT PARAMS:	paramTypeAsInt				The array type for this stored communication
//					paramBeingInitialised		TRUE if this is being called during initialisation [default = FALSE]
PROC Reset_MP_Stored_Communication_Details(INT paramTypeAsInt, BOOL paramBeingInitialised = FALSE)

	g_sCommsMP.storedComms[paramTypeAsInt].scBitflags			= CLEAR_ALL_MP_EOM_MESSAGE_BITFLAGS
	g_sCommsMP.storedComms[paramTypeAsInt].scCharacter			= ""
	g_sCommsMP.storedComms[paramTypeAsInt].scSpeaker			= ""
	g_sCommsMP.storedComms[paramTypeAsInt].scCharSheetAsInt		= ENUM_TO_INT(NO_CHARACTER)
	g_sCommsMP.storedComms[paramTypeAsInt].scGroup				= ""
	g_sCommsMP.storedComms[paramTypeAsInt].scRoot				= ""
	g_sCommsMP.storedComms[paramTypeAsInt].scPostHelp			= ""
	g_sCommsMP.storedComms[paramTypeAsInt].scInitialTimeoutInitialised = FALSE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms]: Reset_MP_Stored_Communication_Details(): ")
		NET_PRINT(Convert_Stored_Comms_Type_As_Int_To_String(paramTypeAsInt))
		NET_PRINT(" - All details cleared") NET_NL()
	#ENDIF
	
	// Don't update the widgets if being initialised - the widgets don't yet exist
	IF (paramBeingInitialised)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// Clear out the text widgets
		g_WIDGET_StoredComms_ScriptName[paramTypeAsInt]		= ""
		g_WIDGET_StoredComms_CharSheetID[paramTypeAsInt]	= ""
		Update_MP_Stored_Communication_Text_Widgets(paramTypeAsInt)
		
		// Untick the 'initial delay expired' widget
		g_WIDGET_StoredComms_Delay_Expired[paramTypeAsInt]	= FALSE
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set a Stored Communication as complete
//
// INPUT PARAMS:	paramTypeAsInt				The array type for this stored communication
PROC MP_Stored_Communication_Has_Completed(INT paramTypeAsInt)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms]: The Stored Communication has ended: ")
		NET_PRINT(Convert_Stored_Comms_Type_As_Int_To_String(paramTypeAsInt))
		NET_NL()
	#ENDIF
	
	Reset_MP_Stored_Communication_Details(paramTypeAsInt)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set a Stored Communication as received
//
// INPUT PARAMS:	paramTypeAsInt				The array type for this stored communication
PROC Set_MP_Stored_Communication_Received(INT paramTypeAsInt)
	SET_BIT(g_sCommsMP.storedComms[paramTypeAsInt].scBitflags, MP_STORED_COMMS_RECEIVED)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if a Stored Communication has been received
//
// INPUT PARAMS:	paramTypeAsInt				The array type for this stored communication
// RETURN VALUE:	BOOL						TRUE if a Stored Communication has been received, otherwise FALSE
FUNC BOOL Has_An_MP_Stored_Communication_Been_Received(INT paramTypeAsInt)
	RETURN (IS_BIT_SET(g_sCommsMP.storedComms[paramTypeAsInt].scBitflags, MP_STORED_COMMS_RECEIVED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set a Stored Communication as active
//
// INPUT PARAMS:	paramTypeAsInt				The array type for this stored communication
PROC Set_MP_Stored_Communication_Active(INT paramTypeAsInt)
	SET_BIT(g_sCommsMP.storedComms[paramTypeAsInt].scBitflags, MP_STORED_COMMS_ACTIVE)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if there is an active Stored Communication
//
// INPUT PARAMS:	paramTypeAsInt				The array type for this stored communication
// RETURN VALUE:	BOOL						TRUE if there is an active Stored Communication, otherwise FALSE
FUNC BOOL Is_This_An_Active_MP_Stored_Communication(INT paramTypeAsInt)
	RETURN (IS_BIT_SET(g_sCommsMP.storedComms[paramTypeAsInt].scBitflags, MP_STORED_COMMS_ACTIVE))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Calculate and Store the Initial Timeout for the Communication
//
// INPUT PARAMS:	paramTypeAsInt				The array type for this stored communication
//					paramDelay					The delay in msec
PROC Calculate_MP_Stored_Communication_Initial_Timeout(INT paramTypeAsInt, INT paramDelay)

	g_sCommsMP.storedComms[paramTypeAsInt].scInitialTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), paramDelay)
	g_sCommsMP.storedComms[paramTypeAsInt].scInitialTimeoutInitialised = TRUE
	
ENDPROC





// ===========================================================================================================
//      The Main MP Stored Communication Control Routines
// ===========================================================================================================

// PURPOSE:	A one-off Stored Communication initialisation that loops through the array clearing out all variables.
PROC Initialise_MP_Stored_Communication()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms]: Initialise_MP_Stored_Communication") NET_NL()
	#ENDIF

	BOOL beingInitialised = TRUE
	
	// Clear out the whole array
	INT tempLoop = 0
	REPEAT MAX_STORED_COMMS_TYPES tempLoop
		Reset_MP_Stored_Communication_Details(tempLoop, beingInitialised)
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	This procedure should be called every frame to control access to the communications systems for Stored Communication Type.
//
// INPUT PARAMS:	paramTypeAsInt				The array type for this stored communication
PROC Maintain_One_MP_Stored_Communication(INT paramTypeAsInt)

	// If a Stored Communication hasn't been received then do nothing
	IF NOT (Has_An_MP_Stored_Communication_Been_Received(paramTypeAsInt))
		EXIT
	ENDIF
	
	// There is a Stored Communication that needs maintained
	structPedsForConversation conversationStruct
	
	// If the Stored Communication isn't active, then try to activate it
	IF NOT (Is_This_An_Active_MP_Stored_Communication(paramTypeAsInt))
		// Has the Initial Delay expired?
		IF NOT IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sCommsMP.storedComms[paramTypeAsInt].scInitialTimeout)
		AND (g_sCommsMP.storedComms[paramTypeAsInt].scInitialTimeoutInitialised)
			EXIT
		ENDIF
	
		// Add the speaker of the Stored Communication to the conversation struct
		ADD_PED_FOR_DIALOGUE(ConversationStruct, ConvertSingleCharacter(g_sCommsMP.storedComms[paramTypeAsInt].scSpeaker), NULL, g_sCommsMP.storedComms[paramTypeAsInt].scCharacter)
		
		// Request permission from the communications systems to play the Stored Communication
		enumCharacterList theCharSheetID = INT_TO_ENUM(enumCharacterList, g_sCommsMP.storedComms[paramTypeAsInt].scCharSheetAsInt)
		IF (Request_MP_Comms_Message(conversationStruct, theCharSheetID, g_sCommsMP.storedComms[paramTypeAsInt].scGroup, g_sCommsMP.storedComms[paramTypeAsInt].scRoot))
			// ...permission granted, Stored Communication now being played
			Set_MP_Stored_Communication_Active(paramTypeAsInt)
		ENDIF
		
		EXIT
	ENDIF
	
	// The Stored Communication must already be active, so check if it has ended
	IF NOT (Is_MP_Comms_Still_Playing())
		IF NOT ARE_STRINGS_EQUAL(g_sCommsMP.storedComms[paramTypeAsInt].scPostHelp,"")
			PRINT_HELP(g_sCommsMP.storedComms[paramTypeAsInt].scPostHelp)
		ENDIF
		MP_Stored_Communication_Has_Completed(paramTypeAsInt)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	This procedure should be called every frame to control access to the communications systems - needs to loop through array.
PROC Maintain_MP_Stored_Communication()

	INT tempLoop = 0
	
	// Perform any maintenance functionality that needs performed every frame
	// NOTE: Currently only DEBUG
	#IF IS_DEBUG_BUILD
		REPEAT MAX_STORED_COMMS_TYPES tempLoop
			Maintain_MP_Stored_Communication_Widgets(tempLoop)
		ENDREPEAT
	#ENDIF

	// Only maintain one communication if any communication is active - there is no need to process any other pending communication
	INT maxTypesAsInt = ENUM_TO_INT(MAX_STORED_COMMS_TYPES)
	INT activeCommunication = maxTypesAsInt
	REPEAT MAX_STORED_COMMS_TYPES tempLoop
		IF (Has_An_MP_Stored_Communication_Been_Received(tempLoop))
			IF (Is_This_An_Active_MP_Stored_Communication(tempLoop))
				// There should only be one active communication since it all goes through the MP Communication system - let things continue, it should resolve itself
				#IF IS_DEBUG_BUILD
					IF NOT (activeCommunication = maxTypesAsInt)
						SCRIPT_ASSERT("Maintain_MP_Stored_Communication(): ERROR - More than one Stored Communication Type is classed as Active - this should be impossible")
						NET_PRINT("...KGM MP [MPComms]: Maintain_MP_Stored_Communication() - More than one MP Stored Communication has been classed as active") NET_NL()
					ENDIF
				#ENDIF
			
				activeCommunication = tempLoop
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Maintain only the active communication if there is one
	IF NOT (activeCommunication = maxTypesAsInt)
		Maintain_One_MP_Stored_Communication(activeCommunication)
		
		EXIT
	ENDIF
		
	// No communications are active, so maintain all communications
	REPEAT MAX_STORED_COMMS_TYPES tempLoop
		Maintain_One_MP_Stored_Communication(tempLoop)
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Add a MP script to pass in the details for a Stored Communication.
//
// INPUT PARAMS:	paramStoredType		The type of Stored Communication being requested
//					paramCharacter		The Character ID string (from the 'Character' dropdown box in dialogueStar)
//					paramSpeakerID		The Speaker ID string (from the 'Speaker' value in dialogueStar)
//					paramCharSheetID	The CharSheetID for the character starting the call
//					paramGroupID		The Subtitle Group ID string for the Conversation (from the 'Subtitle Group ID' value in dialogueStar)
//					paramRootID			The Root ID string for the Conversation (from the 'Root' value in dialogueStar)
//					paramDelay			The initial delay before attempting to trigger the audio [default = 0]
//					postHelp			The help text that will display after conversation has finished
// RETURN VALUE:	BOOL				TRUE if the Stored Communication was accepted for output, otherwise FALSE
//
// NOTES:	The details will be stored and the Comms system will trigger this speech using the appropriate method (cellphone or radio).
//			KGM 1/9/11: There will now be an array of stored communications of different types (eg: general, 'kill or arrest').
FUNC BOOL Add_New_MP_Stored_Communication(g_eMPStoredCommsTypes paramStoredType, STRING paramCharacter, STRING paramSpeakerID, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID, STRING postHelp, INT paramDelay = 0)

	// Error Checking - NULL or Empty strings
	IF (Is_String_Null_Or_Empty(paramCharacter))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Add_New_MP_Stored_Communication(): The Character ID string is NULL or empty - This should match the 'Character' dropdown box in dialogueStar")
			NET_PRINT("...KGM MP [MPComms]: Stored Communication Error: Character String was NULL or Empty.") NET_NL()
			NET_PRINT("           Requested by script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			NET_PRINT("           Stored Comms Type  : ") NET_PRINT(Convert_Stored_Comms_Type_To_String(paramStoredType)) NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF

	IF (Is_String_Null_Or_Empty(paramSpeakerID))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Add_New_MP_Stored_Communication(): The Speaker ID string is NULL or empty - This should match the 'Speaker' value in dialogueStar.")
			NET_PRINT("...KGM MP [MPComms]: Stored Communication Error: Speaker ID String was NULL or Empty.") NET_NL()
			NET_PRINT("           Requested by script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			NET_PRINT("           Stored Comms Type  : ") NET_PRINT(Convert_Stored_Comms_Type_To_String(paramStoredType)) NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF

	IF (Is_String_Null_Or_Empty(paramGroupID))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Add_New_MP_Stored_Communication(): The Group ID string is NULL or empty - This should match the 'Subtitle Group ID' value in dialogueStar.")
			NET_PRINT("...KGM MP [MPComms]: Stored Communication Error: Group ID String was NULL or Empty.") NET_NL()
			NET_PRINT("           Requested by script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			NET_PRINT("           Stored Comms Type  : ") NET_PRINT(Convert_Stored_Comms_Type_To_String(paramStoredType)) NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF

	IF (Is_String_Null_Or_Empty(paramRootID))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Add_New_MP_Stored_Communication(): The Root ID string is NULL or empty - This should match the 'Root' value in dialogueStar.")
			NET_PRINT("...KGM MP [MPComms]: Stored Communication Error: Root ID String was NULL or Empty.") NET_NL()
			NET_PRINT("           Requested by script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			NET_PRINT("           Stored Comms Type  : ") NET_PRINT(Convert_Stored_Comms_Type_To_String(paramStoredType)) NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// Output details to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms]: Script requested MP Stored Communication: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("...........From Character: ") NET_PRINT(paramCharacter) NET_PRINT("   [speaker ID = ") NET_PRINT(paramSpeakerID) NET_PRINT("]") NET_NL()
		NET_PRINT("...........Group ID: ") NET_PRINT(paramGroupID) NET_PRINT("   Root ID: ") NET_PRINT(paramRootID) NET_NL()
		NET_PRINT("...........Cellphone Display Character Name: ") NET_PRINT(Convert_MP_CharSheetID_To_String(paramCharSheetID)) NET_NL()
		NET_PRINT("...........Delay: ") NET_PRINT_INT(paramDelay) NET_NL()
		NET_PRINT("...........Stored Comms Type  : ") NET_PRINT(Convert_Stored_Comms_Type_To_String(paramStoredType)) NET_NL()
	#ENDIF
	
	// Error Checks - Strings Too Long
	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramCharacter) > MAX_LENGTH_MP_COMMS_CHARACTER_ID_STRING)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Add_New_MP_Stored_Communication(): The Character ID string is longer than expected. Tell Keith.")
			NET_PRINT("...KGM MP [MPComms]: Stored Communication Error: Character String was too long.") NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramSpeakerID) > MAX_LENGTH_MP_COMMS_SPEAKER_ID_STRING)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Add_New_MP_Stored_Communication(): The Speaker ID string should have only one character in range 0-8 or A-Z - This should match the 'Speaker' value in dialogueStar.")
			NET_PRINT("...KGM MP [MPComms]: Stored Communication Error: SpeakerID String was too long.") NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramGroupID) > MAX_LENGTH_MP_COMMS_GROUP_ID_STRING)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Add_New_MP_Stored_Communication(): The Group ID string is longer than expected. Tell Keith.")
			NET_PRINT("...KGM MP [MPComms]: Stored Communication Error: GroupID String was too long.") NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramRootID) > MAX_LENGTH_MP_COMMS_ROOT_ID_STRING)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Add_New_MP_Stored_Communication(): The Root ID string is longer than expected. Tell Keith.")
			NET_PRINT("...KGM MP [MPComms]: Stored Communication Error: RootID String was too long.") NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (paramStoredType = MAX_STORED_COMMS_TYPES)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Add_New_MP_Stored_Communication(): The Stored Comms Type is illegal. Tell Keith.")
			NET_PRINT("...KGM MP [MPComms]: Stored Communication Error: StoredCommsType = MAX_STORED_COMMS_TYPES (should be a valid comms type).") NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Other Error Checks
	IF (paramDelay < 0)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Add_New_MP_Stored_Communication(): The Delay is less then 0. Tell Keith. Temp Fix: Setting to 0 so assert can be ignored.")
			NET_PRINT("...KGM MP [MPComms]: Stored Communication Error: Delay is less than 0. Temp Fix: Setting to 0.") NET_NL()
		#ENDIF
		
		paramDelay = 0
	ENDIF
	
	// Convert the Stored Communication Type enum to an INT
	INT storedCommsTypeAsInt = ENUM_TO_INT(paramStoredType)
	
	// Ditch this request if a Stored Communication is already active
	IF (Is_This_An_Active_MP_Stored_Communication(storedCommsTypeAsInt))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms]: There is already an active Stored Communication, so ditching this request.") NET_NL()
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	// Store the details ready for triggering
	// NOTE: This will overwrite an existing request that hasn't been activated.
	// ...first, clear out the globals
	Reset_MP_Stored_Communication_Details(storedCommsTypeAsInt)
	
	// ...then store the details of the new message and mark it as pending
	g_sCommsMP.storedComms[storedCommsTypeAsInt].scCharacter		= paramCharacter
	g_sCommsMP.storedComms[storedCommsTypeAsInt].scSpeaker			= paramSpeakerID
	g_sCommsMP.storedComms[storedCommsTypeAsInt].scCharSheetAsInt	= ENUM_TO_INT(paramCharSheetID)
	g_sCommsMP.storedComms[storedCommsTypeAsInt].scGroup			= paramGroupID
	g_sCommsMP.storedComms[storedCommsTypeAsInt].scRoot				= paramRootID
	g_sCommsMP.storedComms[storedCommsTypeAsInt].scPostHelp			= postHelp
	Calculate_MP_Stored_Communication_Initial_Timeout(storedCommsTypeAsInt, paramDelay)
	Set_MP_Stored_Communication_Received(storedCommsTypeAsInt)
	
	#IF IS_DEBUG_BUILD
		// Store the text widgets that display text that is otherwise non-persistent
		g_WIDGET_StoredComms_ScriptName[storedCommsTypeAsInt]	= GET_THIS_SCRIPT_NAME()
		g_WIDGET_StoredComms_CharSheetID[storedCommsTypeAsInt]	= Convert_MP_CharSheetID_To_String(paramCharSheetID)
		Update_MP_Stored_Communication_Text_Widgets(storedCommsTypeAsInt)
	#ENDIF
	
	// All okay
	RETURN TRUE
	
ENDFUNC
