//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_ARENA_BIG_SCREEN.sch																				//
// Description: Rendering bink videos to the arena big screen during game modes & inside the arena garage.				//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		11/10/18																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_include.sch"
USING "website_public.sch"
USING "net_ArenaAnnouncer_Header.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ ENUMS ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

ENUM ARENA_BIG_SCREEN_STATES
	ARENA_BIG_SCREEN_STATE_INVALID = -1,
	ARENA_BIG_SCREEN_STATE_RESET_DATA = 0,
	ARENA_BIG_SCREEN_STATE_LINK_RT,
	ARENA_BIG_SCREEN_STATE_SERVER_BD,
	ARENA_BIG_SCREEN_STATE_REQUEST_TXD,
	ARENA_BIG_SCREEN_STATE_GAMEMODE_INFO,
	ARENA_BIG_SCREEN_STATE_INTRO,
	ARENA_BIG_SCREEN_STATE_SPONSORS,
	ARENA_BIG_SCREEN_STATE_IMAGES,
	ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SERVER_BD,
	ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SET_CLIP,
	ARENA_BIG_SCREEN_STATE_CROWD_SERVER_BD,
	ARENA_BIG_SCREEN_STATE_CROWD_SET_CLIP,
	ARENA_BIG_SCREEN_STATE_NOISE_METRE_CLIP,
	ARENA_BIG_SCREEN_STATE_DRONE_IMAGES,
	ARENA_BIG_SCREEN_STATE_TRAP_IMAGES,
	ARENA_BIG_SCREEN_STATE_RC_CAR_IMAGES,
	ARENA_BIG_SCREEN_STATE_TURRET_IMAGES,
	ARENA_BIG_SCREEN_STATE_FADE_SCREENS,
	ARENA_BIG_SCREEN_STATE_POST_SPECIAL_CLIP,
	ARENA_BIG_SCREEN_STATE_WINNER_SERVER_BD,
	ARENA_BIG_SCREEN_STATE_WINNER_SET_CLIP,
	ARENA_BIG_SCREEN_STATE_DRAW
ENDENUM

ENUM ARENA_BIG_SCREEN_PLAYLIST_TYPE
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_INVALID = 0,
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_INTRO,
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS,
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES,
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_DEATH_MOMENTS,
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_CROWD_CLIPS,
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_NOISE_METRE,
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_DRONE_IMAGES,
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TRAP_IMAGES,
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_RCCAR_IMAGES,
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TURRET_IMAGES,
	ARENA_BIG_SCREEN_PLAYLIST_TYPE_WINNER
ENDENUM

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ STRUCTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

STRUCT ARENA_BIG_SCREEN_SERVER_STRUCT
	INT iArenaIntroPlaylistIndex = -1
	INT iArenaImagePlaylistIndex = 0
	INT iArenaDeathMomentPlaylistIndex = -1
	INT iArenaCrowdClipPlaylistIndex = -1
	INT iArenaWinnerPlaylistIndex = -1
	INT iArenaSponsorPlaylistArray[ciARENA_MAX_SPONSOR_PLAYLISTS]
	INT iArenaImagePlaylistArray[ciARENA_MAX_IMAGE_PLAYLISTS]
ENDSTRUCT

STRUCT ARENA_BIG_SCREEN_STRUCT
	INT iGameModeType = -1
	INT iRenderTargetID = -1
	INT iIntroPlaylistIndex = -1
	INT iSponsorPlaylistIndex = 0
	INT iImagePlaylistIndex = 0
	INT iImagePlaylistSubIndex = 0
	INT iDeathMomentPlaylistIndex = -1
	INT iCrowdClipPlaylistIndex = -1
	INT iWinnerPlaylistIndex = -1
	INT iScreenAlpha = 255
	BOOL bBinkVidPlaying = FALSE
	BOOL bCleanupAfterGameMode = FALSE
	BOOL bBigScreenFaded = FALSE
	BOOL bIncrementPlaylistIndex = FALSE
	BOOL bCelebrationScreenActive = FALSE
	BOOL bWinnerMainClipPlayed = FALSE
	BOOL bDisplayWinnerBink = FALSE
	BOOL bSpecialBinkPlaying = FALSE
	BOOL bCheckQueuedSpecialBinks = FALSE
	TEXT_LABEL_23 tlBinkPlaylist
	TEXT_LABEL_23 tlCurrentImage
	SCRIPT_TIMER stWinnerDelayTimer
	SCRIPT_TIMER stImageDisplayTimer
	ARENA_BIG_SCREEN_STATES eState = ARENA_BIG_SCREEN_STATE_RESET_DATA
	ARENA_BIG_SCREEN_PLAYLIST_TYPE ePlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_INVALID
	ARENA_BIG_SCREEN_PLAYLIST_TYPE ePreviousPlaylistType
	TIME_DATATYPE tdScreenFadeTimer
	
	#IF IS_DEBUG_BUILD
	BOOL bTriggerDeathMoment = FALSE
	BOOL bTriggerCrowdClip = FALSE
	BOOL bTriggerNoiseMetre = FALSE
	BOOL bTriggerDroneImage = FALSE
	BOOL bTriggerTrapImage = FALSE
	BOOL bTriggerRCCarImage = FALSE
	BOOL bTriggerTurretImage = FALSE
	BOOL bTriggerIntro = FALSE
	BOOL bTriggerWinner = FALSE
	
	BOOL bQueryDeathMoment = FALSE
	BOOL bQueryCrowdClip = FALSE
	BOOL bQueryNoiseMetre = FALSE
	BOOL bQueryDroneImage = FALSE
	BOOL bQueryTrapImage = FALSE
	BOOL bQueryRCCarImage = FALSE
	BOOL bQueryTurretImage = FALSE
	BOOL bQueryIntro = FALSE
	BOOL bQueryWinner = FALSE
	#ENDIF
ENDSTRUCT

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ DATA FUNCTIONS ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC STRING GET_ARENA_BIG_SCREEN_RENDER_TARGET_NAME()
	RETURN "bigscreen_01"
ENDFUNC

FUNC MODEL_NAMES GET_ARENA_BIG_SCREEN_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xs_prop_arena_bigscreen_01"))
ENDFUNC

FUNC STRING GET_ARENA_BIG_SCREEN_TXD()
	RETURN "Prop_Screen_Arena_Giant"
ENDFUNC

FUNC TEXT_LABEL_23 GET_ARENA_BIG_SCREEN_INTRO_PLAYLIST(INT iGameModeType, INT iIntroPlaylistIndex)
	TEXT_LABEL_23 tlIntroPlaylist = ""
	SWITCH iGameModeType
		CASE ciNEWVS_ARENA_TRIALS		tlIntroPlaylist = "ABS_MS_BZBT_PL_"		BREAK
		CASE ciNEWVS_BOMB_FOOTBALL		tlIntroPlaylist = "ABS_MS_BWALL_PL_"	BREAK
		CASE ciNEWVS_CARNAGE			tlIntroPlaylist = "ABS_MS_CRNG_PL_"		BREAK
		CASE ciNEWVS_DM_CARNAGE			tlIntroPlaylist = "ABS_MS_CRNG_PL_"		BREAK
		CASE ciNEWVS_DM_PASS_THE_BOMB	tlIntroPlaylist = "ABS_MS_HBMB_PL_"		BREAK
		CASE ciNEWVS_GAMES_MASTERS		tlIntroPlaylist = "ABS_MS_GMST_PL_"		BREAK
		CASE ciNEWVS_MONSTER_JAM		tlIntroPlaylist = "ABS_MS_HCTM_PL_"		BREAK
		CASE ciNEWVS_TAG_TEAM			tlIntroPlaylist = "ABS_MS_TGTM_PL_"		BREAK
		DEFAULT 						tlIntroPlaylist = "ABS_MS_GEN_PL_"		BREAK
	ENDSWITCH
	tlIntroPlaylist += iIntroPlaylistIndex
	PRINTLN("[ARENA_BIG_SCREEN] GET_ARENA_BIG_SCREEN_INTRO_PLAYLIST - tlIntroPlaylist: ", tlIntroPlaylist)
	RETURN tlIntroPlaylist
ENDFUNC

PROC INCREMENT_ARENA_BIG_SCEEN_SPONSOR_PLAYLIST_INDEX(INT &iSponsorPlaylistIndex)
	iSponsorPlaylistIndex++
	IF iSponsorPlaylistIndex > (ciARENA_MAX_SPONSOR_PLAYLISTS-1)
		iSponsorPlaylistIndex = 0
	ENDIF
	PRINTLN("[ARENA_BIG_SCREEN] INCREMENT_ARENA_BIG_SCEEN_SPONSOR_PLAYLIST_INDEX - iSponsorPlaylistIndex: ", iSponsorPlaylistIndex)
ENDPROC

FUNC TEXT_LABEL_23 GET_ARENA_BIG_SCREEN_SPONSOR_PLAYLIST(INT &iSponsorPlaylistArray[], INT &iSponsorPlaylistIndex)
	TEXT_LABEL_23 tlSponsorPlaylist = "ABS_SPON_PL_"
	tlSponsorPlaylist += iSponsorPlaylistArray[iSponsorPlaylistIndex]
	INCREMENT_ARENA_BIG_SCEEN_SPONSOR_PLAYLIST_INDEX(iSponsorPlaylistIndex)
	PRINTLN("[ARENA_BIG_SCREEN] GET_ARENA_BIG_SCREEN_SPONSOR_PLAYLIST - tlSponsorPlaylist: ", tlSponsorPlaylist)
	RETURN tlSponsorPlaylist
ENDFUNC

FUNC TEXT_LABEL_23 GET_ARENA_BIG_SCREEN_DEATH_MOMENT_PLAYLIST(INT &iDeathMomentPlaylistIndex)
	TEXT_LABEL_23 tlDeathMomentPlaylist = "ABS_DM_PL_"
	tlDeathMomentPlaylist += iDeathMomentPlaylistIndex
	PRINTLN("[ARENA_BIG_SCREEN] GET_ARENA_BIG_SCREEN_DEATH_MOMENT_PLAYLIST - tlDeathMomentPlaylist: ", tlDeathMomentPlaylist)
	RETURN tlDeathMomentPlaylist
ENDFUNC

FUNC TEXT_LABEL_23 GET_ARENA_BIG_SCREEN_CROWD_PLAYLIST(INT &iCrowdPlaylistIndex)
	TEXT_LABEL_23 tlCrowdPlaylist = "ABS_CC_PL_"
	tlCrowdPlaylist += iCrowdPlaylistIndex
	PRINTLN("[ARENA_BIG_SCREEN] GET_ARENA_BIG_SCREEN_CROWD_PLAYLIST - tlCrowdPlaylist: ", tlCrowdPlaylist)
	RETURN tlCrowdPlaylist
ENDFUNC

FUNC TEXT_LABEL_23 GET_ARENA_BIG_SCREEN_WINNER_PLAYLIST(INT &iWinnerPlaylistIndex)
	TEXT_LABEL_23 tlWinnerPlaylist = "ABS_WR_PL_"
	tlWinnerPlaylist += iWinnerPlaylistIndex
	PRINTLN("[ARENA_BIG_SCREEN] GET_ARENA_BIG_SCREEN_WINNER_PLAYLIST - tlWinnerPlaylist: ", tlWinnerPlaylist)
	RETURN tlWinnerPlaylist
ENDFUNC

FUNC TEXT_LABEL_23 GET_ARENA_BIG_SCREEN_CURRENT_IMAGE(INT &iImagePlaylistArray[], INT &iImagePlaylistIndex, INT &iImagePlaylistSubIndex)
	PRINTLN("[ARENA_BIG_SCREEN] GET_ARENA_BIG_SCREEN_CURRENT_IMAGE - iImagePlaylistArray[", iImagePlaylistIndex, "]: ", iImagePlaylistArray[iImagePlaylistIndex])
	PRINTLN("[ARENA_BIG_SCREEN] GET_ARENA_BIG_SCREEN_CURRENT_IMAGE - iImagePlaylistSubIndex: ", iImagePlaylistSubIndex)
	TEXT_LABEL_23 tlCurrentImage = ""
	SWITCH iImagePlaylistArray[iImagePlaylistIndex]
		CASE 0
			SWITCH iImagePlaylistSubIndex
				CASE 0	tlCurrentImage = "MESSAGE_11"	BREAK
				CASE 1	tlCurrentImage = "MESSAGE_20"	BREAK
				CASE 2	tlCurrentImage = "MESSAGE_14"	BREAK
				CASE 3	tlCurrentImage = "MESSAGE_13"	BREAK
				CASE 4	tlCurrentImage = "MESSAGE_09"	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iImagePlaylistSubIndex
				CASE 0	tlCurrentImage = "MESSAGE_08"	BREAK
				CASE 1	tlCurrentImage = "MESSAGE_19"	BREAK
				CASE 2	tlCurrentImage = "MESSAGE_16"	BREAK
				CASE 3	tlCurrentImage = "MESSAGE_12"	BREAK
				CASE 4	tlCurrentImage = "MESSAGE_18"	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iImagePlaylistSubIndex
				CASE 0	tlCurrentImage = "MESSAGE_22"	BREAK
				CASE 1	tlCurrentImage = "MESSAGE_17"	BREAK
				CASE 2	tlCurrentImage = "MESSAGE_15"	BREAK
				CASE 3	tlCurrentImage = "MESSAGE_24"	BREAK
				CASE 4	tlCurrentImage = "MESSAGE_04"	BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iImagePlaylistSubIndex
				CASE 0	tlCurrentImage = "MESSAGE_01"	BREAK
				CASE 1	tlCurrentImage = "MESSAGE_02"	BREAK
				CASE 2	tlCurrentImage = "MESSAGE_25"	BREAK
				CASE 3	tlCurrentImage = "MESSAGE_27"	BREAK
				CASE 4	tlCurrentImage = "MESSAGE_03"	BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH iImagePlaylistSubIndex
				CASE 0	tlCurrentImage = "MESSAGE_21"	BREAK
				CASE 1	tlCurrentImage = "MESSAGE_06"	BREAK
				CASE 2	tlCurrentImage = "MESSAGE_05"	BREAK
				CASE 3	tlCurrentImage = "MESSAGE_07"	BREAK
				CASE 4	tlCurrentImage = "MESSAGE_23"	BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH iImagePlaylistSubIndex
				CASE 0	tlCurrentImage = "MESSAGE_28"	BREAK
				CASE 1	tlCurrentImage = "MESSAGE_10"	BREAK
				CASE 2	tlCurrentImage = "MESSAGE_11"	BREAK
				CASE 3	tlCurrentImage = "MESSAGE_29"	BREAK
				CASE 4	tlCurrentImage = "MESSAGE_26"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	PRINTLN("[ARENA_BIG_SCREEN] GET_ARENA_BIG_SCREEN_CURRENT_IMAGE - Returning Image: ", tlCurrentImage)
	RETURN tlCurrentImage
ENDFUNC

FUNC STRING GET_ARENA_BIG_SCREEN_STATE_NAME(ARENA_BIG_SCREEN_STATES eState)
	SWITCH eState
		CASE ARENA_BIG_SCREEN_STATE_INVALID						RETURN "ARENA_BIG_SCREEN_STATE_INVALID"
		CASE ARENA_BIG_SCREEN_STATE_RESET_DATA					RETURN "ARENA_BIG_SCREEN_STATE_RESET_DATA"
		CASE ARENA_BIG_SCREEN_STATE_LINK_RT						RETURN "ARENA_BIG_SCREEN_STATE_LINK_RT"
		CASE ARENA_BIG_SCREEN_STATE_SERVER_BD					RETURN "ARENA_BIG_SCREEN_STATE_SERVER_BD"
		CASE ARENA_BIG_SCREEN_STATE_REQUEST_TXD					RETURN "ARENA_BIG_SCREEN_STATE_REQUEST_TXD"
		CASE ARENA_BIG_SCREEN_STATE_GAMEMODE_INFO				RETURN "ARENA_BIG_SCREEN_STATE_GAMEMODE_INFO"
		CASE ARENA_BIG_SCREEN_STATE_INTRO						RETURN "ARENA_BIG_SCREEN_STATE_INTRO"
		CASE ARENA_BIG_SCREEN_STATE_SPONSORS					RETURN "ARENA_BIG_SCREEN_STATE_SPONSORS"
		CASE ARENA_BIG_SCREEN_STATE_IMAGES						RETURN "ARENA_BIG_SCREEN_STATE_IMAGES"
		CASE ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SERVER_BD		RETURN "ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SERVER_BD"
		CASE ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SET_CLIP		RETURN "ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SET_CLIP"
		CASE ARENA_BIG_SCREEN_STATE_CROWD_SERVER_BD				RETURN "ARENA_BIG_SCREEN_STATE_CROWD_SERVER_BD"
		CASE ARENA_BIG_SCREEN_STATE_CROWD_SET_CLIP				RETURN "ARENA_BIG_SCREEN_STATE_CROWD_SET_CLIP"
		CASE ARENA_BIG_SCREEN_STATE_NOISE_METRE_CLIP			RETURN "ARENA_BIG_SCREEN_STATE_NOISE_METRE_CLIP"
		CASE ARENA_BIG_SCREEN_STATE_DRONE_IMAGES				RETURN "ARENA_BIG_SCREEN_STATE_DRONE_IMAGES"
		CASE ARENA_BIG_SCREEN_STATE_TRAP_IMAGES					RETURN "ARENA_BIG_SCREEN_STATE_TRAP_IMAGES"
		CASE ARENA_BIG_SCREEN_STATE_RC_CAR_IMAGES				RETURN "ARENA_BIG_SCREEN_STATE_RC_CAR_IMAGES"
		CASE ARENA_BIG_SCREEN_STATE_TURRET_IMAGES				RETURN "ARENA_BIG_SCREEN_STATE_TURRET_IMAGES"
		CASE ARENA_BIG_SCREEN_STATE_FADE_SCREENS				RETURN "ARENA_BIG_SCREEN_STATE_FADE_SCREENS"
		CASE ARENA_BIG_SCREEN_STATE_POST_SPECIAL_CLIP			RETURN "ARENA_BIG_SCREEN_STATE_POST_SPECIAL_CLIP"
		CASE ARENA_BIG_SCREEN_STATE_WINNER_SERVER_BD			RETURN "ARENA_BIG_SCREEN_STATE_WINNER_SERVER_BD"
		CASE ARENA_BIG_SCREEN_STATE_WINNER_SET_CLIP				RETURN "ARENA_BIG_SCREEN_STATE_WINNER_SET_CLIP"
		CASE ARENA_BIG_SCREEN_STATE_DRAW						RETURN "ARENA_BIG_SCREEN_STATE_DRAW"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_ARENA_BIG_SCREEN_PLAYLIST_TYPE_NAME(ARENA_BIG_SCREEN_PLAYLIST_TYPE ePlaylistType)
	SWITCH ePlaylistType
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_INTRO					RETURN "ARENA_BIG_SCREEN_PLAYLIST_TYPE_INTRO"
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS				RETURN "ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS"
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES					RETURN "ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES"
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_DEATH_MOMENTS			RETURN "ARENA_BIG_SCREEN_PLAYLIST_TYPE_DEATH_MOMENTS"
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_CROWD_CLIPS				RETURN "ARENA_BIG_SCREEN_PLAYLIST_TYPE_CROWD_CLIPS"
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_DRONE_IMAGES	RETURN "ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_DRONE_IMAGES"
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TRAP_IMAGES		RETURN "ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TRAP_IMAGES"
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_RCCAR_IMAGES	RETURN "ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_RCCAR_IMAGES"
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TURRET_IMAGES	RETURN "ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TURRET_IMAGES"
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_NOISE_METRE				RETURN "ARENA_BIG_SCREEN_PLAYLIST_TYPE_NOISE_METRE"
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_WINNER					RETURN "ARENA_BIG_SCREEN_PLAYLIST_TYPE_WINNER"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC SET_ARENA_BIG_SCREEN_STATE(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, ARENA_BIG_SCREEN_STATES eState)
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_STATE - Setting State: ", GET_ARENA_BIG_SCREEN_STATE_NAME(eState))
	sArenaBigScreenData.eState = eState
ENDPROC

PROC SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE ePlaylistType)
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE - Setting Playlist Type: ", GET_ARENA_BIG_SCREEN_PLAYLIST_TYPE_NAME(ePlaylistType))
	sArenaBigScreenData.ePlaylistType = ePlaylistType
ENDPROC

FUNC INT GET_ARENA_BIG_SCREEN_ANNOUNCER_CROWD_CLIP_BS(INT iClip)
	SWITCH iClip
		CASE 0	RETURN	g_iAA_PlaySound_Generic_BinkReactions_BeerSmash
		CASE 1	RETURN	g_iAA_PlaySound_Generic_BinkReactions_Birth
		CASE 2	RETURN	g_iAA_PlaySound_Generic_BinkReactions_Crazy
		CASE 3	RETURN	g_iAA_PlaySound_Generic_BinkReactions_Divorce
		CASE 4	RETURN	g_iAA_PlaySound_Generic_BinkReactions_EMT
		CASE 5	RETURN	g_iAA_PlaySound_Generic_BinkReactions_Fight
		CASE 6	RETURN	g_iAA_PlaySound_Generic_BinkReactions_Fight2
		CASE 7	RETURN	g_iAA_PlaySound_Generic_BinkReactions_Grind
		CASE 8	RETURN	g_iAA_PlaySound_Generic_BinkReactions_Guns
		CASE 9	RETURN	g_iAA_PlaySound_Generic_BinkReactions_Noise
		CASE 10	RETURN	g_iAA_PlaySound_Generic_BinkReactions_Rapper
		CASE 11	RETURN	g_iAA_PlaySound_Generic_BinkReactions_TXT
		CASE 12	RETURN	g_iAA_PlaySound_Generic_BinkReactions_Vomit
	ENDSWITCH
	RETURN -1
ENDFUNC

PROC PLAY_ARENA_BIG_SCREEN_ANNOUNCER_CROWD_CLIP(INT iClip)
	INT iAnnouncerBS = GET_ARENA_BIG_SCREEN_ANNOUNCER_CROWD_CLIP_BS(iClip)
	IF iAnnouncerBS != -1
		IF NOT IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_SET(iAnnouncerBS)
			PRINTLN("[ARENA_BIG_SCREEN] PLAY_ARENA_BIG_SCREEN_ANNOUNCER_CROWD_CLIP - Triggering Announcer For Crowd Clip: ", iClip, " Announcer BS: ", iAnnouncerBS)
			SET_ARENA_ANNOUNCER_BS_GENERAL(iAnnouncerBS, TRUE, DEFAULT, DEFAULT, TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC TEXT_LABEL_23 GET_ARENA_GARAGE_MOCAP_BIG_SCREEN_SPONSOR_PLAYLIST(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	TEXT_LABEL_23 tlSponsorPlaylist = "ABS_AG_SPON_PL_"
	tlSponsorPlaylist += sArenaBigScreenData.iSponsorPlaylistIndex
	PRINTLN("[ARENA_BIG_SCREEN] GET_ARENA_GARAGE_MOCAP_BIG_SCREEN_SPONSOR_PLAYLIST - tlSponsorPlaylist: ", tlSponsorPlaylist)
	RETURN tlSponsorPlaylist
ENDFUNC

FUNC TEXT_LABEL_23 GET_ARENA_GARAGE_BIG_SCREEN_SPONSOR_PLAYLIST(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	TEXT_LABEL_23 tlSponsorPlaylist = "ABS_SPON_PL_"
	tlSponsorPlaylist += sArenaBigScreenData.iSponsorPlaylistIndex
	PRINTLN("[ARENA_BIG_SCREEN] GET_ARENA_GARAGE_BIG_SCREEN_SPONSOR_PLAYLIST - tlSponsorPlaylist: ", tlSponsorPlaylist)
	RETURN tlSponsorPlaylist
ENDFUNC

FUNC TEXT_LABEL_23 GET_ARENA_GARAGE_BIG_SCREEN_CURRENT_IMAGE(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	TEXT_LABEL_23 tlCurrentImage = ""
	SWITCH sArenaBigScreenData.iImagePlaylistIndex
		CASE 0
			SWITCH sArenaBigScreenData.iImagePlaylistSubIndex
				CASE 0	tlCurrentImage = "MESSAGE_19"	BREAK
				CASE 1	tlCurrentImage = "MESSAGE_20"	BREAK
				CASE 2	tlCurrentImage = "MESSAGE_21"	BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH sArenaBigScreenData.iImagePlaylistSubIndex
				CASE 0	tlCurrentImage = "MESSAGE_22"	BREAK
				CASE 1	tlCurrentImage = "MESSAGE_23"	BREAK
				CASE 2	tlCurrentImage = "MESSAGE_24"	BREAK
				CASE 3	tlCurrentImage = "MESSAGE_25"	BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH sArenaBigScreenData.iImagePlaylistSubIndex
				CASE 0	tlCurrentImage = "MESSAGE_26"	BREAK
				CASE 1	tlCurrentImage = "MESSAGE_27"	BREAK
				CASE 2	tlCurrentImage = "MESSAGE_28"	BREAK
				CASE 3	tlCurrentImage = "MESSAGE_29"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	PRINTLN("[ARENA_BIG_SCREEN] GET_ARENA_GARAGE_BIG_SCREEN_CURRENT_IMAGE - Returning Image: ", tlCurrentImage)
	RETURN tlCurrentImage
ENDFUNC

FUNC BOOL HAS_ARENA_GARAGE_IMAGE_PLAYLIST_FINISHED(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	SWITCH sArenaBigScreenData.iImagePlaylistIndex
		CASE 0
			IF sArenaBigScreenData.iImagePlaylistSubIndex > 2
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
		CASE 2
			IF sArenaBigScreenData.iImagePlaylistSubIndex > 3
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC INCREMENT_ARENA_BIG_SCEEN_IMAGE_PLAYLIST_INDEX(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	sArenaBigScreenData.iImagePlaylistIndex++
	IF sArenaBigScreenData.iImagePlaylistIndex > (ciARENA_GARAGE_MAX_IMAGE_PLAYLIST_SIZE-1)
		sArenaBigScreenData.iImagePlaylistIndex = 0
		sArenaBigScreenData.iImagePlaylistSubIndex = 0
	ENDIF
	PRINTLN("[ARENA_BIG_SCREEN] INCREMENT_ARENA_BIG_SCEEN_IMAGE_PLAYLIST_INDEX - iImagePlaylistIndex: ", sArenaBigScreenData.iImagePlaylistIndex)
ENDPROC

FUNC INT GET_ARENA_BIG_SCENE_MOCAP_CLIP_LENGTH_MS(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	SWITCH sArenaBigScreenData.iSponsorPlaylistIndex
		CASE 0	RETURN 4000	//ABS_SPON_JUNK
		CASE 1	RETURN 4000	//ABS_SPON_REDW
		CASE 2	RETURN 5000	//ABS_SPON_CBKT
		CASE 3	RETURN 5000	//ABS_SPON_BEEF
		CASE 4	RETURN 4000	//ABS_SPON_BAVA
		CASE 5	RETURN 4000	//ABS_SPON_ATOM
		CASE 6	RETURN 5000	//ABS_SPON_MOLL
		CASE 7	RETURN 4000	//ABS_SPON_LOGG
		CASE 8	RETURN 5000	//ABS_SPON_DVTY
		CASE 9	RETURN 4000	//ABS_SPON_SPNK
		CASE 10	RETURN 4000	//ABS_SPON_RING
		CASE 11	RETURN 4000	//ABS_SPON_DOIL
		CASE 12	RETURN 4000	//ABS_SPON_LINV
		CASE 13	RETURN 5000	//ABS_SPON_PROL
		CASE 14	RETURN 4000	//ABS_SPON_YKON
	ENDSWITCH
	RETURN 0
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ SERVER BD ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_ARENA_BIG_SCREEN_SERVER_BD_DATA(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData)
	PRINTLN("[ARENA_BIG_SCREEN] DEBUG_PRINT_ARENA_BIG_SCREEN_SERVER_BD_DATA - Setting Intro Index: ", sArenaBigScreenServerData.iArenaIntroPlaylistIndex)
	INT i
	REPEAT ciARENA_MAX_SPONSOR_PLAYLISTS i
		PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_SERVER_BD_DATA - Sponsor Playlist Array [", i, "]: ", sArenaBigScreenServerData.iArenaSponsorPlaylistArray[i])
	ENDREPEAT
	REPEAT ciARENA_MAX_IMAGE_PLAYLISTS i
		PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_SERVER_BD_DATA - Image Playlist Array [", i, "]: ", sArenaBigScreenServerData.iArenaImagePlaylistArray[i])
	ENDREPEAT
ENDPROC
#ENDIF

PROC POPULATE_ARENA_BIG_SCREEN_SERVER_BD_SPONSOR_PLAYLIST_ARRAY(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData)
	INT i, j
	BOOL bUniqueNumberUsed[ciARENA_MAX_SPONSOR_PLAYLISTS]
	
	// Init sponsor playlist array to -1
	REPEAT ciARENA_MAX_SPONSOR_PLAYLISTS i
		sArenaBigScreenServerData.iArenaSponsorPlaylistArray[i] = -1
	ENDREPEAT
	
	// Populate sponsor playlist array with unique random number
	REPEAT ciARENA_MAX_SPONSOR_PLAYLISTS i
		INT iRand = GET_RANDOM_INT_IN_RANGE(0, ciARENA_MAX_SPONSOR_PLAYLISTS)
		// Check if random number already used
		IF bUniqueNumberUsed[iRand]
			iRand = i
		ENDIF
		// Check if current index already used
		IF bUniqueNumberUsed[iRand]
			REPEAT ciARENA_MAX_SPONSOR_PLAYLISTS j
				IF NOT bUniqueNumberUsed[j]
					iRand = j
				ENDIF
			ENDREPEAT
		ENDIF
		sArenaBigScreenServerData.iArenaSponsorPlaylistArray[i] = iRand
		bUniqueNumberUsed[iRand] = TRUE
	ENDREPEAT
ENDPROC

PROC POPULATE_ARENA_BIG_SCREEN_SERVER_BD_IMAGE_PLAYLIST_ARRAY(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData)
	INT i, j
	BOOL bUniqueNumberUsed[ciARENA_MAX_IMAGE_PLAYLISTS]
	
	// Init image playlist array to -1
	REPEAT ciARENA_MAX_IMAGE_PLAYLISTS i
		sArenaBigScreenServerData.iArenaImagePlaylistArray[i] = -1
	ENDREPEAT
	
	// Populate sponsor playlist array with unique random number
	REPEAT ciARENA_MAX_IMAGE_PLAYLISTS i
		INT iRand = GET_RANDOM_INT_IN_RANGE(0, ciARENA_MAX_IMAGE_PLAYLISTS)
		// Check if random number already used
		IF bUniqueNumberUsed[iRand]
			iRand = i
		ENDIF
		// Check if current index already used
		IF bUniqueNumberUsed[iRand]
			REPEAT ciARENA_MAX_IMAGE_PLAYLISTS j
				IF NOT bUniqueNumberUsed[j]
					iRand = j
				ENDIF
			ENDREPEAT
		ENDIF
		sArenaBigScreenServerData.iArenaImagePlaylistArray[i] = iRand
		bUniqueNumberUsed[iRand] = TRUE
	ENDREPEAT
ENDPROC

FUNC BOOL DOES_SERVER_BD_INTRO_PLAYLIST_NEED_SET(INT iIntroPlaylistIndex)
	RETURN (iIntroPlaylistIndex = -1)
ENDFUNC

FUNC BOOL DOES_SERVER_BD_SPONSOR_PLAYLIST_NEED_SET(INT &iSponsorPlaylistArray[])
	INT i
	REPEAT ciARENA_MAX_SPONSOR_PLAYLISTS i
		IF (iSponsorPlaylistArray[i] = -1)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_SERVER_BD_IMAGE_PLAYLIST_NEED_SET(INT &iImagePlaylistArray[])
	INT i
	REPEAT ciARENA_MAX_IMAGE_PLAYLISTS i
		IF (iImagePlaylistArray[i] = -1)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC CLEAR_ARENA_BIG_SCREEN_SERVER_BD_INTRO_PLAYLIST_INDEX(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF sArenaBigScreenServerData.iArenaIntroPlaylistIndex != -1
			sArenaBigScreenServerData.iArenaIntroPlaylistIndex = -1
		ENDIF
	ENDIF
ENDPROC

PROC SET_ARENA_BIG_SCREEN_SERVER_BD_INTRO_PLAYLIST_INDEX(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData)
	IF sArenaBigScreenServerData.iArenaIntroPlaylistIndex = -1
		sArenaBigScreenServerData.iArenaIntroPlaylistIndex = GET_RANDOM_INT_IN_RANGE(0, 5)
	ENDIF
ENDPROC

PROC SET_ARENA_BIG_SCREEN_DEATH_MOMENT_SERVER_BD_DATA(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData, BOOL bOverwriteValue = FALSE)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bOverwriteValue
			sArenaBigScreenServerData.iArenaDeathMomentPlaylistIndex = GET_RANDOM_INT_IN_RANGE(0, 9)
		ELSE
			IF sArenaBigScreenServerData.iArenaDeathMomentPlaylistIndex = -1
				sArenaBigScreenServerData.iArenaDeathMomentPlaylistIndex = GET_RANDOM_INT_IN_RANGE(0, 9)
			ENDIF
		ENDIF
	ENDIF
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_DEATH_MOMENT_SERVER_BD_DATA - bOverwriteValue: ", bOverwriteValue)
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_DEATH_MOMENT_SERVER_BD_DATA - Death Moment Playlist Index: ", sArenaBigScreenServerData.iArenaDeathMomentPlaylistIndex)
ENDPROC

PROC SET_ARENA_BIG_SCREEN_CROWD_SERVER_BD_DATA(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData, BOOL bOverwriteValue = FALSE)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bOverwriteValue
			sArenaBigScreenServerData.iArenaCrowdClipPlaylistIndex = GET_RANDOM_INT_IN_RANGE(0, 13)
		ELSE
			IF sArenaBigScreenServerData.iArenaCrowdClipPlaylistIndex = -1
				sArenaBigScreenServerData.iArenaCrowdClipPlaylistIndex = GET_RANDOM_INT_IN_RANGE(0, 13)
			ENDIF
		ENDIF
	ENDIF
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_CROWD_SERVER_BD_DATA - bOverwriteValue: ", bOverwriteValue)
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_CROWD_SERVER_BD_DATA - Crowd Playlist Index: ", sArenaBigScreenServerData.iArenaCrowdClipPlaylistIndex)
ENDPROC

PROC SET_ARENA_BIG_SCREEN_WINNER_SERVER_BD_DATA(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData, BOOL bOverwriteValue = FALSE)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bOverwriteValue
			sArenaBigScreenServerData.iArenaWinnerPlaylistIndex = GET_RANDOM_INT_IN_RANGE(0, 5)
		ELSE
			IF sArenaBigScreenServerData.iArenaWinnerPlaylistIndex = -1
				sArenaBigScreenServerData.iArenaWinnerPlaylistIndex = GET_RANDOM_INT_IN_RANGE(0, 5)
			ENDIF
		ENDIF
	ENDIF
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_WINNER_SERVER_BD_DATA - bOverwriteValue: ", bOverwriteValue)
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_WINNER_SERVER_BD_DATA - Winner Playlist Index: ", sArenaBigScreenServerData.iArenaWinnerPlaylistIndex)
ENDPROC

PROC SET_ARENA_BIG_SCREEN_SERVER_BD_DATA(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SET_ARENA_BIG_SCREEN_SERVER_BD_INTRO_PLAYLIST_INDEX(sArenaBigScreenServerData)
		POPULATE_ARENA_BIG_SCREEN_SERVER_BD_SPONSOR_PLAYLIST_ARRAY(sArenaBigScreenServerData)
		POPULATE_ARENA_BIG_SCREEN_SERVER_BD_IMAGE_PLAYLIST_ARRAY(sArenaBigScreenServerData)
		SET_ARENA_BIG_SCREEN_DEATH_MOMENT_SERVER_BD_DATA(sArenaBigScreenServerData)
		SET_ARENA_BIG_SCREEN_CROWD_SERVER_BD_DATA(sArenaBigScreenServerData)
		SET_ARENA_BIG_SCREEN_WINNER_SERVER_BD_DATA(sArenaBigScreenServerData)
	ENDIF
	#IF IS_DEBUG_BUILD
	DEBUG_PRINT_ARENA_BIG_SCREEN_SERVER_BD_DATA(sArenaBigScreenServerData)
	#ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ LOCATE CHECKS ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_LOCAL_PLAYER_IN_ARENA_BIG_SCREEN_RENDER_AREA()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<224.899445,5180.197266,-84.565102>>, <<211.515640,5180.269043,-89.997276>>, 21.0)	// Window
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<211.811310,5179.962402,-84.565102>>, <<197.598160,5180.011230,-89.997276>>, 15.0)	// Main area
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CLEANUP ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC RESET_ARENA_GARAGE_BIG_SCEEN_DATA(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	sArenaBigScreenData.iSponsorPlaylistIndex = 0
	sArenaBigScreenData.iImagePlaylistIndex = 0
	sArenaBigScreenData.iImagePlaylistSubIndex = 0
	sArenaBigScreenData.iRenderTargetID = -1
	sArenaBigScreenData.iScreenAlpha = 255
	sArenaBigScreenData.tlBinkPlaylist = ""
	sArenaBigScreenData.tlCurrentImage = ""
	RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
ENDPROC

PROC CLEANUP_ARENA_BIG_SCREEN_DATA(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	PRINTLN("[ARENA_BIG_SCREEN] CLEANUP_ARENA_BIG_SCREEN_DATA - Clearing data...")
	
	// Reset local data
	sArenaBigScreenData.iGameModeType = -1
	sArenaBigScreenData.iIntroPlaylistIndex = -1
	sArenaBigScreenData.iSponsorPlaylistIndex = 0
	sArenaBigScreenData.iImagePlaylistSubIndex = 0
	sArenaBigScreenData.iDeathMomentPlaylistIndex = -1
	sArenaBigScreenData.iCrowdClipPlaylistIndex = -1
	sArenaBigScreenData.iWinnerPlaylistIndex = -1
	sArenaBigScreenData.iRenderTargetID = -1
	sArenaBigScreenData.iScreenAlpha = 255
	sArenaBigScreenData.bBigScreenFaded = FALSE
	sArenaBigScreenData.bCelebrationScreenActive = FALSE
	sArenaBigScreenData.bWinnerMainClipPlayed = FALSE
	sArenaBigScreenData.bDisplayWinnerBink = FALSE
	sArenaBigScreenData.bSpecialBinkPlaying = FALSE
	sArenaBigScreenData.bCheckQueuedSpecialBinks = FALSE
	sArenaBigScreenData.tlBinkPlaylist = ""
	sArenaBigScreenData.tlCurrentImage = ""
	sArenaBigScreenData.eState = ARENA_BIG_SCREEN_STATE_RESET_DATA
	sArenaBigScreenData.ePlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_INVALID
	RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
	RESET_NET_TIMER(sArenaBigScreenData.stWinnerDelayTimer)
	
	PRINTLN("[ARENA_BIG_SCREEN] CLEANUP_ARENA_BIG_SCREEN_DATA - Data Cleared.")
ENDPROC

PROC RESET_ARENA_BIG_SCREEN_CLEANUP_FLAG()
	IF NOT g_bMissionClientGameStateRunning
		IF g_bCleanupArenaScreens != FALSE
			g_bCleanupArenaScreens = FALSE
			PRINTLN("[ARENA_BIG_SCREEN] RESET_ARENA_BIG_SCREEN_CLEANUP_FLAG - Setting g_bCleanupArenaScreens to false")
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_ARENA_BIG_SCREEN(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData, ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, BOOL bFullCleanup, BOOL bFromFmmcCleanUp = FALSE)
	
	IF sArenaBigScreenData.bCleanupAfterGameMode
		RESET_ARENA_BIG_SCREEN_CLEANUP_FLAG()
		EXIT
	ENDIF
	
	IF sArenaBigScreenData.bBinkVidPlaying
		PRINTLN("[ARENA_BIG_SCREEN] CLEANUP_ARENA_BIG_SCREEN - CLEAR_TV_CHANNEL_PLAYLIST")
		CLEAR_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_1)
		IF NOT HAS_ARENA_WARS_TV_LOBBY_PLAYLIST_STARTED()
			PRINTLN("[ARENA_BIG_SCREEN][AWTV] CLEANUP_ARENA_BIG_SCREEN - !!! SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)")
			SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
		ENDIF
		SET_ARENA_BIG_SCREEN_ACTIVE_RENDER_STATE(FALSE)
		sArenaBigScreenData.bBinkVidPlaying = FALSE
	ENDIF
	
	IF bFullCleanup
		IF IS_NAMED_RENDERTARGET_REGISTERED(GET_ARENA_BIG_SCREEN_RENDER_TARGET_NAME())
			RELEASE_NAMED_RENDERTARGET(GET_ARENA_BIG_SCREEN_RENDER_TARGET_NAME())
		ENDIF
		IF sArenaBigScreenData.eState != ARENA_BIG_SCREEN_STATE_RESET_DATA
			sArenaBigScreenData.eState = ARENA_BIG_SCREEN_STATE_RESET_DATA
		ENDIF
		iArenaBigScreenBinkBS = 0
		sArenaBigScreenData.iRenderTargetID = -1
		SET_ARENA_BIG_SCREEN_ACTIVE_RENDER_STATE(FALSE)
		IF NOT bFromFmmcCleanUp
			CLEAR_ARENA_BIG_SCREEN_SERVER_BD_INTRO_PLAYLIST_INDEX(sArenaBigScreenServerData)
			SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_ARENA_BIG_SCREEN_TXD())
		ENDIF
		sArenaBigScreenData.bCleanupAfterGameMode = TRUE
		PRINTLN("[ARENA_BIG_SCREEN] CLEANUP_ARENA_BIG_SCREEN - Cleanup complete")
	ENDIF
	
ENDPROC

PROC CLEANUP_ARENA_GARAGE_BIG_SCREEN(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, BOOL bFullCleanup)
	SET_ARENA_BIG_SCREEN_ACTIVE_RENDER_STATE(FALSE)
	
	IF sArenaBigScreenData.bBinkVidPlaying
		PRINTLN("[ARENA_BIG_SCREEN] CLEANUP_ARENA_GARAGE_BIG_SCREEN - Setting TV channel to: TVCHANNELTYPE_CHANNEL_NONE")
		CLEAR_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_1)
		SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
		sArenaBigScreenData.bBinkVidPlaying = FALSE
	ENDIF
	
	IF NOT sArenaBigScreenData.bIncrementPlaylistIndex
		PRINTLN("[ARENA_BIG_SCREEN] CLEANUP_ARENA_GARAGE_BIG_SCREEN - Incrementing playlist index's")
		RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
		INCREMENT_ARENA_BIG_SCEEN_IMAGE_PLAYLIST_INDEX(sArenaBigScreenData)
		INCREMENT_ARENA_BIG_SCEEN_SPONSOR_PLAYLIST_INDEX(sArenaBigScreenData.iSponsorPlaylistIndex)
		IF (sArenaBigScreenData.ePlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS)
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_SPONSORS)
		ELIF (sArenaBigScreenData.ePlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES)
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_IMAGES)
		ENDIF
		sArenaBigScreenData.bIncrementPlaylistIndex = TRUE
	ENDIF
	
	IF bFullCleanup
		IF IS_NAMED_RENDERTARGET_REGISTERED(GET_ARENA_BIG_SCREEN_RENDER_TARGET_NAME())
			RELEASE_NAMED_RENDERTARGET(GET_ARENA_BIG_SCREEN_RENDER_TARGET_NAME())
		ENDIF
		IF sArenaBigScreenData.iRenderTargetID != -1
			sArenaBigScreenData.iRenderTargetID = -1
		ENDIF
		IF sArenaBigScreenData.eState != ARENA_BIG_SCREEN_STATE_RESET_DATA
			sArenaBigScreenData.eState = ARENA_BIG_SCREEN_STATE_RESET_DATA
		ENDIF
		SET_ARENA_BIG_SCREEN_ACTIVE_RENDER_STATE(FALSE)
		PRINTLN("[ARENA_BIG_SCREEN] CLEANUP_ARENA_BIG_SCREEN - Cleanup complete")
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ RENDERING ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC MAINTAIN_ARENA_BIG_SCREEN_WIDGETS(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	IF sArenaBigScreenData.bTriggerDeathMoment
		IF NOT IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_DeathMoments)
			SET_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_DeathMoments)
		ENDIF
		sArenaBigScreenData.bTriggerDeathMoment = FALSE
	ENDIF
	IF sArenaBigScreenData.bTriggerCrowdClip
		IF NOT IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_CrowdClip)
			SET_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_CrowdClip)
		ENDIF
		sArenaBigScreenData.bTriggerCrowdClip = FALSE
	ENDIF
	IF sArenaBigScreenData.bTriggerNoiseMetre
		IF NOT IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_NoiseMetre)
			SET_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_NoiseMetre)
		ENDIF
		sArenaBigScreenData.bTriggerNoiseMetre = FALSE
	ENDIF
	IF sArenaBigScreenData.bTriggerDroneImage
		IF NOT IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupDrone)
			SET_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupDrone)
		ENDIF
		sArenaBigScreenData.bTriggerDroneImage = FALSE
	ENDIF
	IF sArenaBigScreenData.bTriggerTrapImage
		IF NOT IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTraps)
			SET_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTraps)
		ENDIF
		sArenaBigScreenData.bTriggerTrapImage = FALSE
	ENDIF
	IF sArenaBigScreenData.bTriggerRCCarImage
		IF NOT IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupRCCar)
			SET_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupRCCar)
		ENDIF
		sArenaBigScreenData.bTriggerRCCarImage = FALSE
	ENDIF
	IF sArenaBigScreenData.bTriggerTurretImage
		IF NOT IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTurrets)
			SET_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTurrets)
		ENDIF
		sArenaBigScreenData.bTriggerTurretImage = FALSE
	ENDIF
	IF sArenaBigScreenData.bTriggerIntro
		IF NOT IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Intro)
			SET_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Intro)
		ENDIF
		sArenaBigScreenData.bTriggerIntro = FALSE
	ENDIF
	IF sArenaBigScreenData.bTriggerWinner
		IF NOT IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Winner)
			SET_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Winner)
		ENDIF
		sArenaBigScreenData.bTriggerWinner = FALSE
	ENDIF
	
	sArenaBigScreenData.bQueryIntro			= IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Intro)
	sArenaBigScreenData.bQueryDeathMoment	= IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_DeathMoments)
	sArenaBigScreenData.bQueryCrowdClip		= IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_CrowdClip)
	sArenaBigScreenData.bQueryNoiseMetre	= IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_NoiseMetre)
	sArenaBigScreenData.bQueryDroneImage	= IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupDrone)
	sArenaBigScreenData.bQueryTrapImage		= IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTraps)
	sArenaBigScreenData.bQueryRCCarImage	= IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupRCCar)
	sArenaBigScreenData.bQueryTurretImage	= IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTurrets)
	sArenaBigScreenData.bQueryWinner		= IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Winner)
ENDPROC
#ENDIF

FUNC ARENA_BIG_SCREEN_STATES GET_ARENA_BIG_SCREEN_STATE_FROM_PLAYLIST_TYPE(ARENA_BIG_SCREEN_PLAYLIST_TYPE ePlaylistType)
	SWITCH ePlaylistType
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_INTRO					RETURN ARENA_BIG_SCREEN_STATE_LINK_RT
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_WINNER					RETURN ARENA_BIG_SCREEN_STATE_WINNER_SERVER_BD
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS				RETURN ARENA_BIG_SCREEN_STATE_SPONSORS
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES					RETURN ARENA_BIG_SCREEN_STATE_IMAGES
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_DEATH_MOMENTS			RETURN ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SERVER_BD
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_CROWD_CLIPS				RETURN ARENA_BIG_SCREEN_STATE_CROWD_SERVER_BD
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_NOISE_METRE
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_DRONE_IMAGES
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TRAP_IMAGES
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_RCCAR_IMAGES
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TURRET_IMAGES
			RETURN ARENA_BIG_SCREEN_STATE_FADE_SCREENS
		BREAK
	ENDSWITCH
	RETURN ARENA_BIG_SCREEN_STATE_INVALID
ENDFUNC

PROC SET_ARENA_BIG_SCREEN_FADE_TIMER(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	IF NOT sArenaBigScreenData.bBigScreenFaded
		sArenaBigScreenData.tdScreenFadeTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), ciARENA_BIG_SCREEN_FADE_TIME_MS)
		sArenaBigScreenData.bBigScreenFaded = TRUE
	ENDIF
ENDPROC

PROC UPDATE_ARENA_BIG_SCREEN_FADE(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, BOOL bFadeIn)
	INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sArenaBigScreenData.tdScreenFadeTimer))
	IF bFadeIn
		sArenaBigScreenData.iScreenAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / ciARENA_BIG_SCREEN_FADE_TIME_MS) * 255)))
	ELSE
		sArenaBigScreenData.iScreenAlpha = ROUND((TO_FLOAT(iTimeDifference) / ciARENA_BIG_SCREEN_FADE_TIME_MS) * 255)
	ENDIF
ENDPROC

FUNC BOOL PERFORM_ARENA_BIG_SCREEN_FADE(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, BOOL bFadeIn)
	SET_ARENA_BIG_SCREEN_FADE_TIMER(sArenaBigScreenData)
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), sArenaBigScreenData.tdScreenFadeTimer)
		UPDATE_ARENA_BIG_SCREEN_FADE(sArenaBigScreenData, bFadeIn)
	ELSE
		sArenaBigScreenData.bBigScreenFaded = FALSE
		IF bFadeIn
			sArenaBigScreenData.iScreenAlpha = 255
			RETURN TRUE
		ELSE
			sArenaBigScreenData.iScreenAlpha = 0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL BLOCK_ARENA_BIG_SCREEN_RENDERING(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, BOOL bArenaGarage = TRUE, BOOL bMocapSceneOne = FALSE, BOOL bMocapSceneTwo = FALSE)
	// General
	IF IS_BROWSER_OPEN()
	OR IS_PLAYER_IN_CORONA()
	OR sArenaBigScreenData.iRenderTargetID = -1
		RETURN TRUE
	ENDIF
	
	// Mocap
	IF bMocapSceneOne
		RETURN FALSE
	ELIF bMocapSceneTwo
		IF IS_ARENA_OFFICE_TV_RENDERING()
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Arena Garage
	IF bArenaGarage
	AND NOT bMocapSceneTwo
		IF IS_ARENA_OFFICE_TV_RENDERING()
		OR NOT IS_LOCAL_PLAYER_IN_ARENA_BIG_SCREEN_RENDER_AREA()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_ARENA_BIG_SCREEN_IMAGE_SETTINGS(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_IMAGE_SETTINGS - Current Image: ", sArenaBigScreenData.tlCurrentImage)
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
	sArenaBigScreenData.bBinkVidPlaying = FALSE
	sArenaBigScreenData.bCleanupAfterGameMode = FALSE
	START_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_IMAGE_SETTINGS - Setup complete")
ENDPROC

PROC SET_ARENA_BIG_SCREEN_TV_SETTINGS(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, BOOL bMocapSceneOne = FALSE, BOOL bMocapSceneTwo = FALSE)
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_TV_SETTINGS - Playlist: ", sArenaBigScreenData.tlBinkPlaylist)
	IF NOT IS_STRING_NULL_OR_EMPTY(sArenaBigScreenData.tlBinkPlaylist)
		SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_1, sArenaBigScreenData.tlBinkPlaylist, TRUE)
	ENDIF
	SET_TV_AUDIO_FRONTEND(TRUE)
	SET_TV_VOLUME(-100.0)
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_1)
	sArenaBigScreenData.bBinkVidPlaying = TRUE
	sArenaBigScreenData.bCleanupAfterGameMode = FALSE
	IF bMocapSceneOne
	OR bMocapSceneTwo
		START_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
	ENDIF
	PRINTLN("[ARENA_BIG_SCREEN] SET_ARENA_BIG_SCREEN_TV_SETTINGS - Setup complete")
ENDPROC

PROC RENDER_ARENA_BIG_SCREEN(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, BOOL bDrawState = FALSE)
	
	IF bDrawState
		IF sArenaBigScreenData.bIncrementPlaylistIndex
			sArenaBigScreenData.bIncrementPlaylistIndex = FALSE
		ENDIF
	ENDIF
	
	SWITCH sArenaBigScreenData.ePlaylistType
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_DRONE_IMAGES
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TRAP_IMAGES
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_RCCAR_IMAGES
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TURRET_IMAGES
			SET_ARENA_BIG_SCREEN_ACTIVE_RENDER_STATE(TRUE)
			SET_TEXT_RENDER_ID(sArenaBigScreenData.iRenderTargetID)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			IF NOT IS_STRING_NULL_OR_EMPTY(sArenaBigScreenData.tlCurrentImage)
				DRAW_SPRITE_NAMED_RENDERTARGET(GET_ARENA_BIG_SCREEN_TXD(), sArenaBigScreenData.tlCurrentImage, 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, sArenaBigScreenData.iScreenAlpha)
			ENDIF
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			g_bArenaBigScreenBinkPlaying = FALSE
		BREAK
		DEFAULT
			IF sArenaBigScreenData.bBinkVidPlaying
				IF GET_TV_CHANNEL() != TVCHANNELTYPE_CHANNEL_1
					SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_1)
				ENDIF
				SET_ARENA_BIG_SCREEN_ACTIVE_RENDER_STATE(TRUE)
				SET_TEXT_RENDER_ID(sArenaBigScreenData.iRenderTargetID)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				DRAW_TV_CHANNEL(0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, sArenaBigScreenData.iScreenAlpha)
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
				g_bArenaBigScreenBinkPlaying = TRUE
			ELSE
				SET_ARENA_BIG_SCREEN_TV_SETTINGS(sArenaBigScreenData)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC RENDER_ARENA_BIG_SCREEN_PREVIOUS(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	SWITCH sArenaBigScreenData.ePreviousPlaylistType
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES
			SET_ARENA_BIG_SCREEN_ACTIVE_RENDER_STATE(TRUE)
			SET_TEXT_RENDER_ID(sArenaBigScreenData.iRenderTargetID)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			IF NOT IS_STRING_NULL_OR_EMPTY(sArenaBigScreenData.tlCurrentImage)
				DRAW_SPRITE_NAMED_RENDERTARGET(GET_ARENA_BIG_SCREEN_TXD(), sArenaBigScreenData.tlCurrentImage, 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, sArenaBigScreenData.iScreenAlpha)
			ENDIF
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			g_bArenaBigScreenBinkPlaying = FALSE
		BREAK
		DEFAULT
			IF sArenaBigScreenData.bBinkVidPlaying
				SET_ARENA_BIG_SCREEN_ACTIVE_RENDER_STATE(TRUE)
				SET_TEXT_RENDER_ID(sArenaBigScreenData.iRenderTargetID)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				DRAW_TV_CHANNEL(0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, sArenaBigScreenData.iScreenAlpha)
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
				g_bArenaBigScreenBinkPlaying = TRUE
			ELSE
				SET_ARENA_BIG_SCREEN_TV_SETTINGS(sArenaBigScreenData)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL REGISTER_AND_LINK_ARENA_BIG_SCREEN_RENDER_TARGET(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	STRING sRenderTarget = GET_ARENA_BIG_SCREEN_RENDER_TARGET_NAME()
	MODEL_NAMES eRenderTargetModel = GET_ARENA_BIG_SCREEN_MODEL()
	
	IF IS_STRING_NULL_OR_EMPTY(sRenderTarget)
	OR eRenderTargetModel = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	// Register, Link & Set RT ID
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
		REGISTER_NAMED_RENDERTARGET(sRenderTarget)
		IF NOT IS_NAMED_RENDERTARGET_LINKED(eRenderTargetModel)
			LINK_NAMED_RENDERTARGET(eRenderTargetModel)
			IF sArenaBigScreenData.iRenderTargetID = -1
				sArenaBigScreenData.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
				PRINTLN("[ARENA_BIG_SCREEN] REGISTER_AND_LINK_ARENA_BIG_SCREEN_RENDER_TARGET - [0] Render Target ID: ", sArenaBigScreenData.iRenderTargetID)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		// Link & Set RT ID
		IF NOT IS_NAMED_RENDERTARGET_LINKED(eRenderTargetModel)
			LINK_NAMED_RENDERTARGET(eRenderTargetModel)
			IF sArenaBigScreenData.iRenderTargetID = -1
				sArenaBigScreenData.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
				PRINTLN("[ARENA_BIG_SCREEN] REGISTER_AND_LINK_ARENA_BIG_SCREEN_RENDER_TARGET - [1] Render Target ID: ", sArenaBigScreenData.iRenderTargetID)
				RETURN TRUE
			ENDIF
		ELSE
			// Set RT ID
			IF sArenaBigScreenData.iRenderTargetID = -1
				sArenaBigScreenData.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
				PRINTLN("[ARENA_BIG_SCREEN] REGISTER_AND_LINK_ARENA_BIG_SCREEN_RENDER_TARGET - [2] Render Target ID: ", sArenaBigScreenData.iRenderTargetID)
				RETURN TRUE
			ELSE
				// Everything already set up
				PRINTLN("[ARENA_BIG_SCREEN] REGISTER_AND_LINK_ARENA_BIG_SCREEN_RENDER_TARGET - [3] Render Target ID: ", sArenaBigScreenData.iRenderTargetID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Do not active a special playlist if one is currently playing
FUNC BOOL LISTEN_FOR_ARENA_BIG_SCREEN_SPECIAL_BINKS(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	IF sArenaBigScreenData.ePlaylistType != ARENA_BIG_SCREEN_PLAYLIST_TYPE_INTRO
	AND sArenaBigScreenData.ePlaylistType != ARENA_BIG_SCREEN_PLAYLIST_TYPE_DEATH_MOMENTS
	AND sArenaBigScreenData.ePlaylistType != ARENA_BIG_SCREEN_PLAYLIST_TYPE_CROWD_CLIPS
	AND sArenaBigScreenData.ePlaylistType != ARENA_BIG_SCREEN_PLAYLIST_TYPE_NOISE_METRE
	AND sArenaBigScreenData.ePlaylistType != ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_DRONE_IMAGES
	AND sArenaBigScreenData.ePlaylistType != ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TRAP_IMAGES
	AND sArenaBigScreenData.ePlaylistType != ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_RCCAR_IMAGES
	AND sArenaBigScreenData.ePlaylistType != ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TURRET_IMAGES
	AND sArenaBigScreenData.ePlaylistType != ARENA_BIG_SCREEN_PLAYLIST_TYPE_WINNER
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PREVIOUS_ARENA_BIG_SCREEN_PLAYLIST_TYPE_VALID(ARENA_BIG_SCREEN_PLAYLIST_TYPE ePreviousPlaylistType)
	IF ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_INTRO
	AND ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_DEATH_MOMENTS
	AND ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_CROWD_CLIPS
	AND ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_NOISE_METRE
	AND ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_DRONE_IMAGES
	AND ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TRAP_IMAGES
	AND ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_RCCAR_IMAGES
	AND ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TURRET_IMAGES
	AND ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_WINNER
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ARENA_BIG_SCREEN_DEATH_MOMENT_PLAYING()
	IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_MOMNT_DEAD"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_MOMNT_DECI"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_MOMNT_DEMO"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_MOMNT_EXTN"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_MOMNT_OBLT"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_MOMNT_SLHT"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_MOMNT_WSTD"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_MOMNT_WIDW"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_MOMNT_USUK"))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARENA_BIG_SCREEN_CROWD_CLIP_PLAYING()
	IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_BEER"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_BIRTH"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_CRAZY"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_DIVORCE"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_EMT"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_FIGHT1"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_FIGHT2"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_GRIND"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_GUNS"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_NOISE1"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_RAPPER"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_TXT"))
	OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_CROWD_VOMIT"))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARENA_BIG_SCREEN_NOISE_METRE_PLAYING()
	RETURN IS_TVSHOW_CURRENTLY_PLAYING(HASH("CLIP_NOISE_METR"))
ENDFUNC

/// PURPOSE: Returns TRUE is any special bink global bitset has been set   
/// PARAMS:  bOnlyIngameBinks - Excludes checking the intro and winner binks
FUNC BOOL HAS_ANY_ARENA_BIG_SCREEN_SPECIAL_BINK_BEEN_TRIGGERED(BOOL bOnlyIngameBinks = FALSE)
	INT iBit
	INT iMaxBit = (ciArenaBigScreenBS_PowerupTurrets+1)
	REPEAT iMaxBit iBit
		IF IS_BIT_SET(iArenaBigScreenBinkBS, iBit)
			IF bOnlyIngameBinks
				RETURN (iBit != ciArenaBigScreenBS_Intro AND iBit != ciArenaBigScreenBS_Winner)
			ELSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

/// PURPOSE: Maintains triggering the special bink videos (crowd clips, death moments etc).
PROC MAINTAIN_ARENA_BIG_SCREEN_SPECIAL_BINKS(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData, ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, BOOL bCheckQueuedSpecialBinks = FALSE)
	IF IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Intro)
		PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Intro has been triggered!")
		sArenaBigScreenData.ePreviousPlaylistType = sArenaBigScreenData.ePlaylistType
		IF NOT IS_PREVIOUS_ARENA_BIG_SCREEN_PLAYLIST_TYPE_VALID(sArenaBigScreenData.ePreviousPlaylistType)
			sArenaBigScreenData.ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
		ENDIF
		SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_INTRO)
		SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_LINK_RT)
		CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Intro)
		sArenaBigScreenData.iScreenAlpha = 255
		sArenaBigScreenData.bCelebrationScreenActive = FALSE
		
	ELIF IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Winner) 
	AND g_bcelebrationScreenIsActive  // Don't process winner until celebration screen active B*5564596
		PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Winner has been triggered!")
		
		IF IS_THIS_A_ROUNDS_MISSION()
			// Timeframe to determine if winner clip should be displayed
			IF NOT HAS_NET_TIMER_STARTED(sArenaBigScreenData.stWinnerDelayTimer)
				START_NET_TIMER(sArenaBigScreenData.stWinnerDelayTimer)
				PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - ROUNDS MISSION - Starting winner delay timer")
			ELIF NOT HAS_NET_TIMER_EXPIRED(sArenaBigScreenData.stWinnerDelayTimer, 3000)
				IF g_bCelebrationPodiumIsActive
				OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
					PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - WINNER - ROUNDS MISSION - Winner bink needs to be played so progress")
					sArenaBigScreenData.bDisplayWinnerBink = TRUE
				ENDIF
			ENDIF
			
			// Progress to show winner clip or cleanup
			IF sArenaBigScreenData.bDisplayWinnerBink
			OR (sArenaBigScreenData.bCelebrationScreenActive AND DID_I_JOIN_MISSION_AS_SPECTATOR())
			OR (HAS_NET_TIMER_STARTED(sArenaBigScreenData.stWinnerDelayTimer) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sArenaBigScreenData.stWinnerDelayTimer, 3000))
				IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
				AND SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
					PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - ROUNDS MISSION - Waiting for g_bCelebrationPodiumIsActive to be set...")
					IF g_bCelebrationPodiumIsActive
						PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - WINNER - ROUNDS MISSION - g_bCelebrationPodiumIsActive is set so display winner bink")
						SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_WINNER)
						SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_WINNER_SERVER_BD)
						CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Winner)
						sArenaBigScreenData.iScreenAlpha = 255
						sArenaBigScreenData.bDisplayWinnerBink = FALSE
					ENDIF
				ELSE
					IF DID_I_JOIN_MISSION_AS_SPECTATOR()
						PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - WINNER - ROUNDS MISSION - Player JIP'd so cleanup.")
					ELSE
						PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - WINNER - ROUNDS MISSION - This is end of the round, not the game so go straight to cleanup")
					ENDIF
					SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_INVALID)
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_INVALID)
					CLEANUP_ARENA_BIG_SCREEN(sArenaBigScreenServerData, sArenaBigScreenData, TRUE)
					CLEANUP_ARENA_BIG_SCREEN_DATA(sArenaBigScreenData)
					CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Winner)
					sArenaBigScreenData.bDisplayWinnerBink = FALSE
				ENDIF
				RESET_NET_TIMER(sArenaBigScreenData.stWinnerDelayTimer)
			ENDIF
		ELSE
			PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - NON ROUNDS MISSION - Waiting for g_bCelebrationPodiumIsActive to be set...")
			
			IF DID_I_JOIN_MISSION_AS_SPECTATOR()
				PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - WINNER - NON ROUNDS MISSION - Player JIP'd so cleanup. Don't wait for g_bCelebrationPodiumIsActive.")
				SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_INVALID)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_INVALID)
				CLEANUP_ARENA_BIG_SCREEN(sArenaBigScreenServerData, sArenaBigScreenData, TRUE)
				CLEANUP_ARENA_BIG_SCREEN_DATA(sArenaBigScreenData)
				CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Winner)
				sArenaBigScreenData.bDisplayWinnerBink = FALSE
				
			ELIF g_bCelebrationPodiumIsActive
				PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - WINNER - NON ROUNDS MISSION - g_bCelebrationPodiumIsActive is set so display winner bink")
				SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_WINNER)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_WINNER_SERVER_BD)
				CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Winner)
				sArenaBigScreenData.iScreenAlpha = 255
			ENDIF	
		ENDIF
		
	ELIF IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_DeathMoments)
		PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Death Moment has been triggered!")
		
		IF sArenaBigScreenData.bCelebrationScreenActive
			PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Celebration screen is active so don't play death moments")
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_DeathMoments)
		ELSE
			sArenaBigScreenData.ePreviousPlaylistType = sArenaBigScreenData.ePlaylistType
			IF NOT IS_PREVIOUS_ARENA_BIG_SCREEN_PLAYLIST_TYPE_VALID(sArenaBigScreenData.ePreviousPlaylistType)
				sArenaBigScreenData.ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
			ENDIF
			SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_DEATH_MOMENTS)
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SERVER_BD)
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_DeathMoments)
			sArenaBigScreenData.iScreenAlpha = 255
			
			sArenaBigScreenData.bCheckQueuedSpecialBinks = bCheckQueuedSpecialBinks
		ENDIF
		
	ELIF IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_CrowdClip)
		PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Crowd Clip has been triggered!")
		
		IF sArenaBigScreenData.bCelebrationScreenActive
			PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Celebration screen is active so don't play crowd clip")
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_CrowdClip)
		ELSE
			sArenaBigScreenData.ePreviousPlaylistType = sArenaBigScreenData.ePlaylistType
			IF NOT IS_PREVIOUS_ARENA_BIG_SCREEN_PLAYLIST_TYPE_VALID(sArenaBigScreenData.ePreviousPlaylistType)
				sArenaBigScreenData.ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
			ENDIF
			SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_CROWD_CLIPS)
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_CROWD_SERVER_BD)
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_CrowdClip)
			sArenaBigScreenData.iScreenAlpha = 255
			
			sArenaBigScreenData.bCheckQueuedSpecialBinks = bCheckQueuedSpecialBinks
		ENDIF
	
	ELIF IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_NoiseMetre)
		PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Noise Metre has been triggered!")
		
		IF sArenaBigScreenData.bCelebrationScreenActive
			PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Celebration screen is active so don't play noise metre")
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_NoiseMetre)
		ELSE
			sArenaBigScreenData.ePreviousPlaylistType = sArenaBigScreenData.ePlaylistType
			IF NOT IS_PREVIOUS_ARENA_BIG_SCREEN_PLAYLIST_TYPE_VALID(sArenaBigScreenData.ePreviousPlaylistType)
				sArenaBigScreenData.ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
			ENDIF
			
			SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_NOISE_METRE)
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_NoiseMetre)
			sArenaBigScreenData.iScreenAlpha = 255
			
			IF bCheckQueuedSpecialBinks
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_NOISE_METRE_CLIP)
			ELSE
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_FADE_SCREENS)
			ENDIF
		ENDIF
		
	ELIF IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupDrone)
		PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Powerup Drone Image has been triggered!")
		
		IF sArenaBigScreenData.bCelebrationScreenActive
			PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Celebration screen is active so don't play powerup drone")
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupDrone)
		ELSE
			sArenaBigScreenData.ePreviousPlaylistType = sArenaBigScreenData.ePlaylistType
			IF NOT IS_PREVIOUS_ARENA_BIG_SCREEN_PLAYLIST_TYPE_VALID(sArenaBigScreenData.ePreviousPlaylistType)
				sArenaBigScreenData.ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
			ENDIF
			
			SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_DRONE_IMAGES)
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupDrone)
			RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
			sArenaBigScreenData.iScreenAlpha = 255
			
			IF bCheckQueuedSpecialBinks
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRONE_IMAGES)
			ELSE
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_FADE_SCREENS)
			ENDIF
		ENDIF
		
	ELIF IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTraps)
		PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Powerup Traps Image has been triggered!")
		
		IF sArenaBigScreenData.bCelebrationScreenActive
			PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Celebration screen is active so don't play powerup traps")
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTraps)
		ELSE
			sArenaBigScreenData.ePreviousPlaylistType = sArenaBigScreenData.ePlaylistType
			IF NOT IS_PREVIOUS_ARENA_BIG_SCREEN_PLAYLIST_TYPE_VALID(sArenaBigScreenData.ePreviousPlaylistType)
				sArenaBigScreenData.ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
			ENDIF
			
			SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TRAP_IMAGES)
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTraps)
			RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
			sArenaBigScreenData.iScreenAlpha = 255
			
			IF bCheckQueuedSpecialBinks
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_TRAP_IMAGES)
			ELSE
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_FADE_SCREENS)
			ENDIF
		ENDIF
	
	ELIF IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupRCCar)
		PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Powerup RC Car Image has been triggered!")
		
		IF sArenaBigScreenData.bCelebrationScreenActive
			PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Celebration screen is active so don't play powerup rc car")
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupRCCar)
		ELSE
			sArenaBigScreenData.ePreviousPlaylistType = sArenaBigScreenData.ePlaylistType
			IF NOT IS_PREVIOUS_ARENA_BIG_SCREEN_PLAYLIST_TYPE_VALID(sArenaBigScreenData.ePreviousPlaylistType)
				sArenaBigScreenData.ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
			ENDIF
			
			SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_RCCAR_IMAGES)
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupRCCar)
			RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
			sArenaBigScreenData.iScreenAlpha = 255
			
			IF bCheckQueuedSpecialBinks
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_RC_CAR_IMAGES)
			ELSE
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_FADE_SCREENS)
			ENDIF
		ENDIF
		
	ELIF IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTurrets)
		PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Powerup Turrets Image has been triggered!")
		
		IF sArenaBigScreenData.bCelebrationScreenActive
			PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Celebration screen is active so don't play powerup turrets")
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTurrets)
		ELSE
			sArenaBigScreenData.ePreviousPlaylistType = sArenaBigScreenData.ePlaylistType
			IF NOT IS_PREVIOUS_ARENA_BIG_SCREEN_PLAYLIST_TYPE_VALID(sArenaBigScreenData.ePreviousPlaylistType)
				sArenaBigScreenData.ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
			ENDIF
			
			SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TURRET_IMAGES)
			CLEAR_BIT(iArenaBigScreenBinkBS, ciArenaBigScreenBS_PowerupTurrets)
			RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
			sArenaBigScreenData.iScreenAlpha = 255
			
			IF bCheckQueuedSpecialBinks
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_TURRET_IMAGES)
			ELSE
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_FADE_SCREENS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Handles the cleanup after a special bink video has played
PROC PROCESS_END_OF_ARENA_BIG_SCREEN_SPECIAL_BINK(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData, ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER2"))
		PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Special bink has finished - Next: ", GET_ARENA_BIG_SCREEN_PLAYLIST_TYPE_NAME(sArenaBigScreenData.ePreviousPlaylistType))
		
		// Reset any playlist type specific data
		IF sArenaBigScreenData.ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES
			RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
		ENDIF
		
		// Clear the special bink movie here B*5564596
		sArenaBigScreenData.tlBinkPlaylist = GET_ARENA_BIG_SCREEN_SPONSOR_PLAYLIST(sArenaBigScreenServerData.iArenaSponsorPlaylistArray, sArenaBigScreenData.iSponsorPlaylistIndex)
		
		// Check if another special bink has been set before returning to idle state
		IF HAS_ANY_ARENA_BIG_SCREEN_SPECIAL_BINK_BEEN_TRIGGERED(TRUE)
			sArenaBigScreenData.ePlaylistType = sArenaBigScreenData.ePreviousPlaylistType
			SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, sArenaBigScreenData.ePlaylistType)
			MAINTAIN_ARENA_BIG_SCREEN_SPECIAL_BINKS(sArenaBigScreenServerData, sArenaBigScreenData, TRUE)
			sArenaBigScreenData.bSpecialBinkPlaying = FALSE
		ELSE
			sArenaBigScreenData.iScreenAlpha = 0
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, GET_ARENA_BIG_SCREEN_STATE_FROM_PLAYLIST_TYPE(sArenaBigScreenData.ePreviousPlaylistType))
			sArenaBigScreenData.bSpecialBinkPlaying = FALSE
		ENDIF
	ELSE
		IF sArenaBigScreenData.iScreenAlpha != 255
			sArenaBigScreenData.iScreenAlpha = 255
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Gets big screen ready for winner bink if celebration screen becomes active
PROC LISTEN_FOR_CELEBRATION_SCREEN(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	IF g_bcelebrationScreenIsActive
		IF sArenaBigScreenData.iRenderTargetID != -1
		AND NOT sArenaBigScreenData.bCelebrationScreenActive
			PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Celebration screen active. Listen for winner state.")
			
			SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_INVALID)
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_INVALID)
			
			IF NOT IS_BIT_SET(iArenaBigScreenBinkBS, ciArenaBigScreenBS_Winner)
				PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Celebration screen active. Winner state should have been triggered. Triggering now.")
				SET_ARENA_BIG_SCREEN_BINK_STATE(ciArenaBigScreenBS_Winner)
			ENDIF
			
			sArenaBigScreenData.bCelebrationScreenActive = TRUE
			sArenaBigScreenData.iScreenAlpha = 255
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Returns TRUE if Arena big screens should update to render something else 
PROC UPDATE_ARENA_BIG_SCREEN(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData, ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	LISTEN_FOR_CELEBRATION_SCREEN(sArenaBigScreenData)
	
	SWITCH sArenaBigScreenData.ePlaylistType
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_INTRO
			IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER"))
				PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Intro playlist has finished - Next: Sponsors")
				SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_SPONSORS)
			ENDIF
		BREAK
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
			IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER"))
				PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Sponsor playlist has finished - Next: Images")
				SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_IMAGES)
			ENDIF
		BREAK
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES
			IF HAS_NET_TIMER_STARTED(sArenaBigScreenData.stImageDisplayTimer) 
			AND HAS_NET_TIMER_EXPIRED(sArenaBigScreenData.stImageDisplayTimer, ciARENA_IMAGE_PLAYLIST_DURATION_MS)
				PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Image ", sArenaBigScreenData.iImagePlaylistSubIndex, " has finished display")
				RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
				sArenaBigScreenData.iImagePlaylistSubIndex++
				IF sArenaBigScreenData.iImagePlaylistSubIndex > (ciARENA_MAX_IMAGE_PLAYLIST_SIZE-1)
					PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Next: Sponsors")
					sArenaBigScreenData.iImagePlaylistSubIndex = 0
					SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS)
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_SPONSORS)
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						sArenaBigScreenServerData.iArenaImagePlaylistIndex++
						IF sArenaBigScreenServerData.iArenaImagePlaylistIndex > (ciARENA_MAX_IMAGE_PLAYLISTS-1)
							sArenaBigScreenServerData.iArenaImagePlaylistIndex = 0
						ENDIF
						PRINTLN("[ARENA_BIG_SCREEN][HOST] UPDATE_ARENA_BIG_SCREEN - iArenaImagePlaylistIndex: ", sArenaBigScreenServerData.iArenaImagePlaylistIndex)
					ENDIF
				ELSE
					PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Next: Images")
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_IMAGES)
				ENDIF
			ENDIF
		BREAK
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_DEATH_MOMENTS
			IF IS_ARENA_BIG_SCREEN_DEATH_MOMENT_PLAYING()
				sArenaBigScreenData.bSpecialBinkPlaying = TRUE
			ENDIF
			IF sArenaBigScreenData.bSpecialBinkPlaying
				PROCESS_END_OF_ARENA_BIG_SCREEN_SPECIAL_BINK(sArenaBigScreenServerData, sArenaBigScreenData)
			ENDIF
		BREAK
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_CROWD_CLIPS
			IF IS_ARENA_BIG_SCREEN_CROWD_CLIP_PLAYING()
				sArenaBigScreenData.bSpecialBinkPlaying = TRUE
			ENDIF
			IF sArenaBigScreenData.bSpecialBinkPlaying
				PROCESS_END_OF_ARENA_BIG_SCREEN_SPECIAL_BINK(sArenaBigScreenServerData, sArenaBigScreenData)
			ENDIF
		BREAK
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_NOISE_METRE
			IF IS_ARENA_BIG_SCREEN_NOISE_METRE_PLAYING()
				sArenaBigScreenData.bSpecialBinkPlaying = TRUE
			ENDIF
			IF sArenaBigScreenData.bSpecialBinkPlaying
				PROCESS_END_OF_ARENA_BIG_SCREEN_SPECIAL_BINK(sArenaBigScreenServerData, sArenaBigScreenData)
			ENDIF
		BREAK
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_DRONE_IMAGES
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TRAP_IMAGES
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_RCCAR_IMAGES
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TURRET_IMAGES
			IF HAS_NET_TIMER_STARTED(sArenaBigScreenData.stImageDisplayTimer) 
				IF HAS_NET_TIMER_EXPIRED(sArenaBigScreenData.stImageDisplayTimer, ciARENA_IMAGE_PLAYLIST_DURATION_MS)
					PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Powerup Image ", sArenaBigScreenData.tlCurrentImage, " has finished display")
					RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
					
					IF HAS_ANY_ARENA_BIG_SCREEN_SPECIAL_BINK_BEEN_TRIGGERED(TRUE)
						sArenaBigScreenData.ePlaylistType = sArenaBigScreenData.ePreviousPlaylistType
						SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, sArenaBigScreenData.ePlaylistType)
						MAINTAIN_ARENA_BIG_SCREEN_SPECIAL_BINKS(sArenaBigScreenServerData, sArenaBigScreenData, TRUE)
					ELSE
						sArenaBigScreenData.iScreenAlpha = 0
						SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, GET_ARENA_BIG_SCREEN_STATE_FROM_PLAYLIST_TYPE(sArenaBigScreenData.ePreviousPlaylistType))
					ENDIF
				ELSE
					IF sArenaBigScreenData.iScreenAlpha != 255
						sArenaBigScreenData.iScreenAlpha = 255
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_WINNER
			IF g_bCelebrationPodiumIsActive
				IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER"))
					IF sArenaBigScreenData.iScreenAlpha != 255
						sArenaBigScreenData.iScreenAlpha = 255
					ENDIF
				ENDIF
				
				IF NOT sArenaBigScreenData.bWinnerMainClipPlayed
					IF NOT IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER"))
						sArenaBigScreenData.bWinnerMainClipPlayed = TRUE
					ENDIF
				ELSE
					IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER2"))
						PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_BIG_SCREEN - Winner playlist has finished - Next: Invalid")
						SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_INVALID)
						SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_INVALID)
						CLEANUP_ARENA_BIG_SCREEN(sArenaBigScreenServerData, sArenaBigScreenData, TRUE)
						CLEANUP_ARENA_BIG_SCREEN_DATA(sArenaBigScreenData)
						sArenaBigScreenData.bWinnerMainClipPlayed = FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF LISTEN_FOR_ARENA_BIG_SCREEN_SPECIAL_BINKS(sArenaBigScreenData)
		MAINTAIN_ARENA_BIG_SCREEN_SPECIAL_BINKS(sArenaBigScreenServerData, sArenaBigScreenData)
	ENDIF
ENDPROC

PROC MAINTAIN_ARENA_BIG_SCREEN_RENDERING(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData, ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	
	SWITCH sArenaBigScreenData.eState
		CASE ARENA_BIG_SCREEN_STATE_RESET_DATA
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_RESET_DATA")
			
			CLEANUP_ARENA_BIG_SCREEN_DATA(sArenaBigScreenData)
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_INVALID)
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_LINK_RT
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_LINK_RT")
			
			IF REGISTER_AND_LINK_ARENA_BIG_SCREEN_RENDER_TARGET(sArenaBigScreenData)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_SERVER_BD)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_SERVER_BD
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_SERVER_BD")
			
			SET_ARENA_BIG_SCREEN_SERVER_BD_DATA(sArenaBigScreenServerData)
			sArenaBigScreenData.iIntroPlaylistIndex = sArenaBigScreenServerData.iArenaIntroPlaylistIndex
			IF sArenaBigScreenData.iIntroPlaylistIndex != -1
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_REQUEST_TXD)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_REQUEST_TXD
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_REQUEST_TXD")
			
			REQUEST_STREAMED_TEXTURE_DICT(GET_ARENA_BIG_SCREEN_TXD())
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(GET_ARENA_BIG_SCREEN_TXD())
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_GAMEMODE_INFO)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_GAMEMODE_INFO
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_GAMEMODE_INFO")
			
			sArenaBigScreenData.iGameModeType = g_FMMC_STRUCT.iAdversaryModeType
			IF sArenaBigScreenData.iGameModeType != -1
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_INTRO)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_INTRO
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_INTRO")
			
			// Server BD should be set but adding safety check in case it hasn't
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF DOES_SERVER_BD_INTRO_PLAYLIST_NEED_SET(sArenaBigScreenServerData.iArenaIntroPlaylistIndex)
					PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_INTRO - Setting server BD!")
					SET_ARENA_BIG_SCREEN_SERVER_BD_DATA(sArenaBigScreenServerData)
				ENDIF
			ENDIF
			
			sArenaBigScreenData.tlBinkPlaylist = GET_ARENA_BIG_SCREEN_INTRO_PLAYLIST(sArenaBigScreenData.iGameModeType, sArenaBigScreenData.iIntroPlaylistIndex)
			IF NOT IS_STRING_NULL_OR_EMPTY(sArenaBigScreenData.tlBinkPlaylist)
				SET_ARENA_BIG_SCREEN_TV_SETTINGS(sArenaBigScreenData)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_SPONSORS
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_SPONSORS")
			
			// Server BD should be set but adding safety check in case it hasn't
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF DOES_SERVER_BD_SPONSOR_PLAYLIST_NEED_SET(sArenaBigScreenServerData.iArenaSponsorPlaylistArray)
					PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_SPONSORS - Setting server BD!")
					SET_ARENA_BIG_SCREEN_SERVER_BD_DATA(sArenaBigScreenServerData)
				ENDIF
			ENDIF
			
			sArenaBigScreenData.tlBinkPlaylist = GET_ARENA_BIG_SCREEN_SPONSOR_PLAYLIST(sArenaBigScreenServerData.iArenaSponsorPlaylistArray, sArenaBigScreenData.iSponsorPlaylistIndex)
			IF NOT IS_STRING_NULL_OR_EMPTY(sArenaBigScreenData.tlBinkPlaylist)
				SET_ARENA_BIG_SCREEN_TV_SETTINGS(sArenaBigScreenData)
				IF sArenaBigScreenData.iScreenAlpha = 0
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_POST_SPECIAL_CLIP)
				ELSE
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
				ENDIF
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_IMAGES
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_IMAGES")
			RENDER_ARENA_BIG_SCREEN(sArenaBigScreenData)
			
			// Server BD should be set but adding safety check in case it hasn't
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF DOES_SERVER_BD_IMAGE_PLAYLIST_NEED_SET(sArenaBigScreenServerData.iArenaImagePlaylistArray)
					PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_IMAGES - Setting server BD!")
					SET_ARENA_BIG_SCREEN_SERVER_BD_DATA(sArenaBigScreenServerData)
				ENDIF
			ENDIF
			
			sArenaBigScreenData.tlCurrentImage = GET_ARENA_BIG_SCREEN_CURRENT_IMAGE(sArenaBigScreenServerData.iArenaImagePlaylistArray, sArenaBigScreenServerData.iArenaImagePlaylistIndex, sArenaBigScreenData.iImagePlaylistSubIndex)
			IF NOT IS_STRING_NULL_OR_EMPTY(sArenaBigScreenData.tlCurrentImage)
				SET_ARENA_BIG_SCREEN_IMAGE_SETTINGS(sArenaBigScreenData)
				IF sArenaBigScreenData.iScreenAlpha = 0
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_POST_SPECIAL_CLIP)
				ELSE
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
				ENDIF
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SERVER_BD
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SERVER_BD")
			RENDER_ARENA_BIG_SCREEN(sArenaBigScreenData)
			
			SET_ARENA_BIG_SCREEN_DEATH_MOMENT_SERVER_BD_DATA(sArenaBigScreenServerData)
			sArenaBigScreenData.iDeathMomentPlaylistIndex = sArenaBigScreenServerData.iArenaDeathMomentPlaylistIndex
			IF sArenaBigScreenData.iDeathMomentPlaylistIndex != -1
				
				IF sArenaBigScreenData.bCheckQueuedSpecialBinks
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SET_CLIP)
				ELSE
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_FADE_SCREENS)
				ENDIF
				
				sArenaBigScreenData.bCheckQueuedSpecialBinks = FALSE
				
				// Set new value for next death moment
				SET_ARENA_BIG_SCREEN_DEATH_MOMENT_SERVER_BD_DATA(sArenaBigScreenServerData, TRUE)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SET_CLIP
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SET_CLIP")
			
			sArenaBigScreenData.tlBinkPlaylist = GET_ARENA_BIG_SCREEN_DEATH_MOMENT_PLAYLIST(sArenaBigScreenData.iDeathMomentPlaylistIndex)
			IF NOT IS_STRING_NULL_OR_EMPTY(sArenaBigScreenData.tlBinkPlaylist)
				SET_ARENA_BIG_SCREEN_TV_SETTINGS(sArenaBigScreenData)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_CROWD_SERVER_BD
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_CROWD_SERVER_BD")
			RENDER_ARENA_BIG_SCREEN(sArenaBigScreenData)
			
			SET_ARENA_BIG_SCREEN_CROWD_SERVER_BD_DATA(sArenaBigScreenServerData)
			sArenaBigScreenData.iCrowdClipPlaylistIndex = sArenaBigScreenServerData.iArenaCrowdClipPlaylistIndex
			IF sArenaBigScreenData.iCrowdClipPlaylistIndex != -1
				PLAY_ARENA_BIG_SCREEN_ANNOUNCER_CROWD_CLIP(sArenaBigScreenData.iCrowdClipPlaylistIndex)
				
				IF sArenaBigScreenData.bCheckQueuedSpecialBinks
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_CROWD_SET_CLIP)
				ELSE
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_FADE_SCREENS)
				ENDIF
				
				sArenaBigScreenData.bCheckQueuedSpecialBinks = FALSE
				
				// Set new value for next crowd clip
				SET_ARENA_BIG_SCREEN_CROWD_SERVER_BD_DATA(sArenaBigScreenServerData, TRUE)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_CROWD_SET_CLIP
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_CROWD_SET_CLIP")
			
			sArenaBigScreenData.tlBinkPlaylist = GET_ARENA_BIG_SCREEN_CROWD_PLAYLIST(sArenaBigScreenData.iCrowdClipPlaylistIndex)
			IF NOT IS_STRING_NULL_OR_EMPTY(sArenaBigScreenData.tlBinkPlaylist)
				SET_ARENA_BIG_SCREEN_TV_SETTINGS(sArenaBigScreenData)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_NOISE_METRE_CLIP
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_NOISE_METRE_CLIP")
			
			sArenaBigScreenData.tlBinkPlaylist = "ABS_NM_PL"
			SET_ARENA_BIG_SCREEN_TV_SETTINGS(sArenaBigScreenData)
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_DRONE_IMAGES
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_DRONE_IMAGES")
			
			sArenaBigScreenData.tlCurrentImage = "POWERUP_DRONE"
			SET_ARENA_BIG_SCREEN_IMAGE_SETTINGS(sArenaBigScreenData)
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_TRAP_IMAGES
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_TRAP_IMAGES")
			
			sArenaBigScreenData.tlCurrentImage = "POWERUP_TRAPS"
			SET_ARENA_BIG_SCREEN_IMAGE_SETTINGS(sArenaBigScreenData)
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_RC_CAR_IMAGES
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_RC_CAR_IMAGES")
			
			sArenaBigScreenData.tlCurrentImage = "POWERUP_RCCARS"
			SET_ARENA_BIG_SCREEN_IMAGE_SETTINGS(sArenaBigScreenData)
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_TURRET_IMAGES
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_TURRET_IMAGES")
			
			sArenaBigScreenData.tlCurrentImage = "POWERUP_TURRETS"
			SET_ARENA_BIG_SCREEN_IMAGE_SETTINGS(sArenaBigScreenData)
			SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_WINNER_SERVER_BD
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_WINNER_SERVER_BD")
			
			SET_ARENA_BIG_SCREEN_WINNER_SERVER_BD_DATA(sArenaBigScreenServerData)
			sArenaBigScreenData.iWinnerPlaylistIndex = sArenaBigScreenServerData.iArenaWinnerPlaylistIndex
			IF sArenaBigScreenData.iWinnerPlaylistIndex != -1
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_WINNER_SET_CLIP)
				
				// Set new value for next winner clip (safe guarding but probably not needed as server BD wiped when fmmc_launcher terminates)
				SET_ARENA_BIG_SCREEN_WINNER_SERVER_BD_DATA(sArenaBigScreenServerData, TRUE)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_WINNER_SET_CLIP
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_WINNER_SET_CLIP")
			
			sArenaBigScreenData.tlBinkPlaylist = GET_ARENA_BIG_SCREEN_WINNER_PLAYLIST(sArenaBigScreenData.iWinnerPlaylistIndex)
			IF NOT IS_STRING_NULL_OR_EMPTY(sArenaBigScreenData.tlBinkPlaylist)
				SET_ARENA_BIG_SCREEN_TV_SETTINGS(sArenaBigScreenData)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_FADE_SCREENS
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_FADE_SCREENS")
			RENDER_ARENA_BIG_SCREEN_PREVIOUS(sArenaBigScreenData)
			
			IF PERFORM_ARENA_BIG_SCREEN_FADE(sArenaBigScreenData, FALSE)
				SWITCH sArenaBigScreenData.ePlaylistType
					CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_DEATH_MOMENTS 
						SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DEATH_MOMENTS_SET_CLIP)
					BREAK
					CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_CROWD_CLIPS
						SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_CROWD_SET_CLIP)
					BREAK
					CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_NOISE_METRE
						SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_NOISE_METRE_CLIP)
					BREAK
					CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_DRONE_IMAGES
						SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRONE_IMAGES)
					BREAK
					CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TRAP_IMAGES
						SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_TRAP_IMAGES)
					BREAK
					CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_RCCAR_IMAGES
						SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_RC_CAR_IMAGES)
					BREAK
					CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_POWERUP_TURRET_IMAGES
						SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_TURRET_IMAGES)
					BREAK
				ENDSWITCH
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_POST_SPECIAL_CLIP
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_POST_SPECIAL_CLIP")
			RENDER_ARENA_BIG_SCREEN_PREVIOUS(sArenaBigScreenData)
			
			IF PERFORM_ARENA_BIG_SCREEN_FADE(sArenaBigScreenData, TRUE)
				SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, sArenaBigScreenData.ePreviousPlaylistType)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_DRAW
			
			IF BLOCK_ARENA_BIG_SCREEN_RENDERING(sArenaBigScreenData, FALSE)
				CLEANUP_ARENA_BIG_SCREEN(sArenaBigScreenServerData, sArenaBigScreenData, FALSE)
			ELSE
				RENDER_ARENA_BIG_SCREEN(sArenaBigScreenData)
			ENDIF
			
			UPDATE_ARENA_BIG_SCREEN(sArenaBigScreenServerData, sArenaBigScreenData)
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_INVALID
			UPDATE_ARENA_BIG_SCREEN(sArenaBigScreenServerData, sArenaBigScreenData)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC UPDATE_ARENA_GARAGE_BIG_SCREEN(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, BOOL bMocapSceneOne = FALSE, BOOL bMocapSceneTwo = FALSE)
	
	SWITCH sArenaBigScreenData.ePlaylistType
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
			IF bMocapSceneOne
			OR bMocapSceneTwo
				IF HAS_NET_TIMER_STARTED(sArenaBigScreenData.stImageDisplayTimer) 
				AND HAS_NET_TIMER_EXPIRED(sArenaBigScreenData.stImageDisplayTimer, GET_ARENA_BIG_SCENE_MOCAP_CLIP_LENGTH_MS(sArenaBigScreenData))
					PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_GARAGE_BIG_SCREEN - Sponsor clip has finished - Next: More sponsor clips")
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_SPONSORS)
					RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
					sArenaBigScreenData.iSponsorPlaylistIndex++
					IF sArenaBigScreenData.iSponsorPlaylistIndex > (ciARENA_GARAGE_MOCAL_MAX_SPONSOR_CLIPS-1)
						sArenaBigScreenData.iSponsorPlaylistIndex = 0
					ENDIF
				ENDIF
			ELSE
				IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER"))
					PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_GARAGE_BIG_SCREEN - Sponsor playlist has finished - Next: Images")
					SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES)
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_IMAGES)
					INCREMENT_ARENA_BIG_SCEEN_SPONSOR_PLAYLIST_INDEX(sArenaBigScreenData.iSponsorPlaylistIndex)
				ENDIF
			ENDIF
		BREAK
		CASE ARENA_BIG_SCREEN_PLAYLIST_TYPE_IMAGES
			IF HAS_NET_TIMER_STARTED(sArenaBigScreenData.stImageDisplayTimer) 
			AND HAS_NET_TIMER_EXPIRED(sArenaBigScreenData.stImageDisplayTimer, ciARENA_IMAGE_PLAYLIST_DURATION_MS)
				PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_GARAGE_BIG_SCREEN - Image Playlist Index: ", sArenaBigScreenData.iImagePlaylistIndex)
				PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_GARAGE_BIG_SCREEN - Image Playlist Sub Index: ", sArenaBigScreenData.iImagePlaylistSubIndex, " has finished display")
				RESET_NET_TIMER(sArenaBigScreenData.stImageDisplayTimer)
				sArenaBigScreenData.iImagePlaylistSubIndex++
				IF HAS_ARENA_GARAGE_IMAGE_PLAYLIST_FINISHED(sArenaBigScreenData)
					PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_GARAGE_BIG_SCREEN - Next: Sponsors")
					sArenaBigScreenData.iImagePlaylistSubIndex = 0
					SET_ARENA_BIG_SCREEN_PLAYLIST_TYPE(sArenaBigScreenData, ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS)
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_SPONSORS)
					INCREMENT_ARENA_BIG_SCEEN_IMAGE_PLAYLIST_INDEX(sArenaBigScreenData)
				ELSE
					PRINTLN("[ARENA_BIG_SCREEN] UPDATE_ARENA_GARAGE_BIG_SCREEN - Next: Images")
					SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_IMAGES)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC MAINTAIN_ARENA_GARAGE_BIG_SCREEN_RENDERING(ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData, BOOL bMocapSceneOne = FALSE, BOOL bMocapSceneTwo = FALSE)
	
	SWITCH sArenaBigScreenData.eState
		CASE ARENA_BIG_SCREEN_STATE_RESET_DATA
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_GARAGE_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_RESET_DATA")
			
			REQUEST_STREAMED_TEXTURE_DICT(GET_ARENA_BIG_SCREEN_TXD())
			IF HAS_STREAMED_TEXTURE_DICT_LOADED(GET_ARENA_BIG_SCREEN_TXD())
				RESET_ARENA_GARAGE_BIG_SCEEN_DATA(sArenaBigScreenData)
				sArenaBigScreenData.ePlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
				sArenaBigScreenData.ePreviousPlaylistType = ARENA_BIG_SCREEN_PLAYLIST_TYPE_SPONSORS
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_LINK_RT)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_LINK_RT
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_GARAGE_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_LINK_RT")
			
			IF REGISTER_AND_LINK_ARENA_BIG_SCREEN_RENDER_TARGET(sArenaBigScreenData)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_SPONSORS)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_SPONSORS
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_GARAGE_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_SPONSORS")
			RENDER_ARENA_BIG_SCREEN(sArenaBigScreenData)
			
			IF bMocapSceneOne
			OR bMocapSceneTwo
				sArenaBigScreenData.tlBinkPlaylist = GET_ARENA_GARAGE_MOCAP_BIG_SCREEN_SPONSOR_PLAYLIST(sArenaBigScreenData)
			ELSE
				sArenaBigScreenData.tlBinkPlaylist = GET_ARENA_GARAGE_BIG_SCREEN_SPONSOR_PLAYLIST(sArenaBigScreenData)
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sArenaBigScreenData.tlBinkPlaylist)
				SET_ARENA_BIG_SCREEN_TV_SETTINGS(sArenaBigScreenData, bMocapSceneOne, bMocapSceneTwo)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_IMAGES
			PRINTLN("[ARENA_BIG_SCREEN] MAINTAIN_ARENA_GARAGE_BIG_SCREEN_RENDERING - ARENA_BIG_SCREEN_STATE_IMAGES")
			RENDER_ARENA_BIG_SCREEN(sArenaBigScreenData)
			
			sArenaBigScreenData.tlCurrentImage = GET_ARENA_GARAGE_BIG_SCREEN_CURRENT_IMAGE(sArenaBigScreenData)
			IF NOT IS_STRING_NULL_OR_EMPTY(sArenaBigScreenData.tlCurrentImage)
				SET_ARENA_BIG_SCREEN_IMAGE_SETTINGS(sArenaBigScreenData)
				SET_ARENA_BIG_SCREEN_STATE(sArenaBigScreenData, ARENA_BIG_SCREEN_STATE_DRAW)
			ENDIF
			
		BREAK
		CASE ARENA_BIG_SCREEN_STATE_DRAW
			
			IF BLOCK_ARENA_BIG_SCREEN_RENDERING(sArenaBigScreenData, TRUE, bMocapSceneOne, bMocapSceneTwo)
				CLEANUP_ARENA_GARAGE_BIG_SCREEN(sArenaBigScreenData, FALSE)
			ELSE
				RENDER_ARENA_BIG_SCREEN(sArenaBigScreenData, TRUE)
				UPDATE_ARENA_GARAGE_BIG_SCREEN(sArenaBigScreenData, bMocapSceneOne, bMocapSceneTwo)
			ENDIF
			
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL SHOULD_ARENA_BIG_SCREEN_RENDER()
	IF IS_PLAYER_USING_ARENA()
	AND g_bMissionClientGameStateRunning
	AND NOT g_bCleanupArenaScreens
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Controls the arena big screen during game modes
FUNC BOOL MAINTAIN_ARENA_BIG_SCREEN(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData, ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	BOOL bRendering = FALSE
	IF SHOULD_ARENA_BIG_SCREEN_RENDER()
		bRendering = TRUE
		MAINTAIN_ARENA_BIG_SCREEN_RENDERING(sArenaBigScreenServerData, sArenaBigScreenData)
	ELSE
		CLEANUP_ARENA_BIG_SCREEN(sArenaBigScreenServerData, sArenaBigScreenData, TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	MAINTAIN_ARENA_BIG_SCREEN_WIDGETS(sArenaBigScreenData)
	#ENDIF
	
	RETURN bRendering
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ DEBUG ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC CREATE_ARENA_BIG_SCREEN_WIDGETS(ARENA_BIG_SCREEN_SERVER_STRUCT &sArenaBigScreenServerData, ARENA_BIG_SCREEN_STRUCT &sArenaBigScreenData)
	START_WIDGET_GROUP("Arena Big Screen")
		START_WIDGET_GROUP("Playlist IDs")
			ADD_WIDGET_INT_READ_ONLY("Intro Playlist ID:", sArenaBigScreenServerData.iArenaIntroPlaylistIndex)
			ADD_WIDGET_INT_READ_ONLY("Image Playlist ID:", sArenaBigScreenServerData.iArenaImagePlaylistIndex)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Playlist Order")
			START_WIDGET_GROUP("Sponsor")
				ADD_WIDGET_INT_READ_ONLY("0) ", sArenaBigScreenServerData.iArenaSponsorPlaylistArray[0])
				ADD_WIDGET_INT_READ_ONLY("1) ", sArenaBigScreenServerData.iArenaSponsorPlaylistArray[1])
				ADD_WIDGET_INT_READ_ONLY("2) ", sArenaBigScreenServerData.iArenaSponsorPlaylistArray[2])
				ADD_WIDGET_INT_READ_ONLY("3) ", sArenaBigScreenServerData.iArenaSponsorPlaylistArray[3])
				ADD_WIDGET_INT_READ_ONLY("4) ", sArenaBigScreenServerData.iArenaSponsorPlaylistArray[4])
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Images")
				ADD_WIDGET_INT_READ_ONLY("0) ", sArenaBigScreenServerData.iArenaImagePlaylistArray[0])
				ADD_WIDGET_INT_READ_ONLY("1) ", sArenaBigScreenServerData.iArenaImagePlaylistArray[1])
				ADD_WIDGET_INT_READ_ONLY("2) ", sArenaBigScreenServerData.iArenaImagePlaylistArray[2])
				ADD_WIDGET_INT_READ_ONLY("3) ", sArenaBigScreenServerData.iArenaImagePlaylistArray[3])
				ADD_WIDGET_INT_READ_ONLY("4) ", sArenaBigScreenServerData.iArenaImagePlaylistArray[4])
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Local Data")
			ADD_WIDGET_INT_READ_ONLY("Render Target ID: ", sArenaBigScreenData.iRenderTargetID)
			ADD_WIDGET_INT_READ_ONLY("Game Mode Type: ", sArenaBigScreenData.iGameModeType)
			ADD_WIDGET_INT_READ_ONLY("Screen Alpha: ", sArenaBigScreenData.iScreenAlpha)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_READ_ONLY("Intro Playlist ID: ", sArenaBigScreenData.iIntroPlaylistIndex)
			ADD_WIDGET_INT_READ_ONLY("Sponsor Playlist ID:", sArenaBigScreenData.iSponsorPlaylistIndex)
			ADD_WIDGET_INT_READ_ONLY("Image Playlist Sub ID: ", sArenaBigScreenData.iImagePlaylistSubIndex)
			ADD_WIDGET_INT_READ_ONLY("Crowd Playlist ID: ", sArenaBigScreenData.iCrowdClipPlaylistIndex)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Playing Bink Vid: ", sArenaBigScreenData.bBinkVidPlaying)
			ADD_WIDGET_BOOL("Cleanup After Gamemode: ", sArenaBigScreenData.bCleanupAfterGameMode)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Server Data")
			ADD_WIDGET_INT_READ_ONLY("Crowd Playlist Index: ", sArenaBigScreenServerData.iArenaCrowdClipPlaylistIndex)
			ADD_WIDGET_INT_READ_ONLY("Death Moments Playlist Index: ", sArenaBigScreenServerData.iArenaDeathMomentPlaylistIndex)
			ADD_WIDGET_INT_READ_ONLY("Winner Playlist Index: ", sArenaBigScreenServerData.iArenaWinnerPlaylistIndex)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Global Bitset")
			ADD_WIDGET_BOOL("Intro", sArenaBigScreenData.bQueryIntro)
			ADD_WIDGET_BOOL("Death Moment", sArenaBigScreenData.bQueryDeathMoment)
			ADD_WIDGET_BOOL("Crowd Clip", sArenaBigScreenData.bQueryCrowdClip)
			ADD_WIDGET_BOOL("Noise Metre", sArenaBigScreenData.bQueryNoiseMetre)
			ADD_WIDGET_BOOL("Drone Image", sArenaBigScreenData.bQueryDroneImage)
			ADD_WIDGET_BOOL("Trap Image", sArenaBigScreenData.bQueryTrapImage)
			ADD_WIDGET_BOOL("RC Car Image", sArenaBigScreenData.bQueryRCCarImage)
			ADD_WIDGET_BOOL("Turret Image", sArenaBigScreenData.bQueryTurretImage)
			ADD_WIDGET_BOOL("Winner", sArenaBigScreenData.bQueryWinner)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Debug Testing")
			ADD_WIDGET_BOOL("Trigger Intro", sArenaBigScreenData.bTriggerIntro)
			ADD_WIDGET_BOOL("Trigger Death Moment", sArenaBigScreenData.bTriggerDeathMoment)
			ADD_WIDGET_BOOL("Trigger Crowd Clip", sArenaBigScreenData.bTriggerCrowdClip)
			ADD_WIDGET_BOOL("Trigger Noise Metre", sArenaBigScreenData.bTriggerNoiseMetre)
			ADD_WIDGET_BOOL("Trigger Drone Image", sArenaBigScreenData.bTriggerDroneImage)
			ADD_WIDGET_BOOL("Trigger Trap Image", sArenaBigScreenData.bTriggerTrapImage)
			ADD_WIDGET_BOOL("Trigger RC Car Image", sArenaBigScreenData.bTriggerRCCarImage)
			ADD_WIDGET_BOOL("Trigger Turret Image", sArenaBigScreenData.bTriggerTurretImage)
			ADD_WIDGET_BOOL("Trigger Winner", sArenaBigScreenData.bTriggerWinner)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

