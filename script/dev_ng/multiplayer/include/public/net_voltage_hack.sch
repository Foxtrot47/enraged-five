USING "net_voltage_hack_functions.sch"
USING "mp_scaleform_functions.sch"

ENUM VOLTAGE_STATES

	VS_INIT = 0,
	VS_REQUEST_ASSETS,
	VS_LOAD_CHECK,
	VS_PICK_LINK,
	VS_MAIN_LOOP,
	VS_SLEEP

ENDENUM

VOLTAGE_STATES vs_current_state = VS_INIT

// Register the links made between each node
STRUCT VOLTAGE_LINK

	VOLTAGE_NODES startNode = TOP_VOLTAGE
	VOLTAGE_NODES endNode = TOP_VOLTAGE
	
	VOLTAGE_LINE_COLOURS eLinkColour = RED_VOLT
	
	INT iLinkValue = -1
	
ENDSTRUCT

VOLTAGE_LINK voltageLinks[3]

PROC VOLTAGE_RESET_LINKS(HG_CONTROL_STRUCT &sControllerStruct)

	VOLTAGE_CALCULATE_TARGET_VALUE(sControllerStruct)
	
	voltageLinks[0].startNode = TOP_VOLTAGE
	voltageLinks[1].startNode = MIDDLE_VOLTAGE
	voltageLinks[2].startNode = BOTTOM_VOLTAGE
	
	voltageLinks[0].endNode = BOTTOM_VOLTAGE
	voltageLinks[1].endNode = MIDDLE_VOLTAGE
	voltageLinks[2].endNode = TOP_VOLTAGE
	
	voltageLinks[0].eLinkColour = YELLOW_VOLT
	voltageLinks[1].eLinkColour = RED_VOLT
	voltageLinks[2].eLinkColour = BLUE_VOLT
	
	voltageLinks[0].iLinkValue = 0
	voltageLinks[1].iLinkValue = 0
	voltageLinks[2].iLinkValue = 0
	
	iCurrentValue = 0
	iLinkCount = 0
	iActiveLink = 0
	
	IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_TOP_MOD_LINKED)
		CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_TOP_MOD_LINKED)
	ENDIF

	IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_MIDDLE_MOD_LINKED)
		CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_MIDDLE_MOD_LINKED)
	ENDIF

	IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_BOTTOM_MOD_LINKED)
		CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_BOTTOM_MOD_LINKED)
	ENDIF	
	
	// Number link
	IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_TOP_NUM_LINKED)
		CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_TOP_NUM_LINKED)
	ENDIF

	IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_MIDDLE_NUM_LINKED)
		CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_MIDDLE_NUM_LINKED)
	ENDIF

	IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_BOTTOM_NUM_LINKED)
		CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_BOTTOM_NUM_LINKED)
	ENDIF	
	
	IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_PLAY_FAIL_SOUND)
		CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_PLAY_FAIL_SOUND)
	ENDIF

	IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_PLAY_SUCCESS_SOUND)
		CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_PLAY_SUCCESS_SOUND)
	ENDIF

ENDPROC

PROC VG_INIT_VARIABLES(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sGameStruct)

	rgbaBase.iR = 255
	rgbaBase.iG = 255
	rgbaBase.iB = 255
	rgbaBase.iA = 0
	
	rgbaBackground.iR = 255
	rgbaBackground.iG = 255
	rgbaBackground.iB = 255
	rgbaBackground.iA = 255

	rgbaOverlay.iR = 255
	rgbaOverlay.iG = 255
	rgbaOverlay.iB = 255
	rgbaOverlay.iA = 190
	
	fTimer = 0
	iGridSelectionX = 12
	iGridSelectionY = 5
	fCountValue = 0
	
	voltageHackStickNavData.bLeftStickMovedUp = FALSE
	voltageHackStickNavData.bLeftStickMovedDown = FALSE
	
	CLEAR_BIT(sControllerStruct.iBS, ciHGBS_FIRST_PLAYED)
		
	// TODO: Get a better check to see if the first scoping mission is active.
	IF  GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_VOLTAGE) = 0
	AND NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_FIRST_PLAYED)
		SET_BIT(sControllerStruct.iBS, ciHGBS_FIRST_PLAYED)
	ENDIF
	
	VOLTAGE_INITIALISE_GRAPH()
	VOLTAGE_RESET_LINKS(sControllerStruct)	
		
	CLEAR_BIT(sControllerStruct.iBS, ciHGBS_QUIT)
	CLEAR_BIT(sControllerStruct.iBS, ciHGBS_TIMED_OUT)
	CLEAR_BIT(sControllerStruct.iBS, ciHGBS_PASSED)
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD) // TESTING
	
	CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][VOLTAGE] Requesting bink video")
	LOAD_BINK_MOVIE(sGameStruct, "Voltage_Intro")
	
	// Restart / start the new timer if it hasn't been already
	START_NET_TIMER(sGameStruct.tdTimer) // Safe to use here as it doesnt run if already initialised.
	RESET_NET_TIMER(sGameStruct.tdEndDelay)
	RESET_NET_TIMER(sGameStruct.tdAnimationTimer)
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H4_Voltage_Hack_Scene")
		START_AUDIO_SCENE("DLC_H4_Voltage_Hack_Scene")
	ENDIF
	
ENDPROC

PROC RESET_VOLTAGE_SCALEFORM_CONTEXT(HACKING_GAME_STRUCT &sGameStruct)
	IF sGameStruct.scaleformInstructionalButtonsIndex != NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sGameStruct.scaleformInstructionalButtonsIndex)
		RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(sGameStruct.scaleformInstructionalButtons)
		sGameStruct.scaleformInstructionalButtonsIndex = NULL
		PRINTLN("[VOLTAGE] - MAINTAIN_VOLTAGE_SCALEFORM_CONTEXT - RESET_VOLTAGE_SCALEFORM_CONTEXT")
		sGameStruct.bUpdatePrompts = TRUE
		fAbortTimer = 0.0		
	ENDIF
	IF NOT sGameStruct.bUpdatePrompts
		sGameStruct.bUpdatePrompts = TRUE
		PRINTLN("[VOLTAGE] - MAINTAIN_VOLTAGE_SCALEFORM_CONTEXT - RESET_VOLTAGE_SCALEFORM_CONTEXT sGameStruct.bUpdatePrompts = TRUE")
	ENDIF
ENDPROC

PROC MAINTAIN_VOLTAGE_SCALEFORM_CONTEXT(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sGameStruct)
	UNUSED_PARAMETER(sControllerStruct)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	IF sGameStruct.eHackingGameState = HACKING_GAME_PLAY
		
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE)
			OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE)
				sGameStruct.bUpdatePrompts = TRUE
				PRINTLN("[MC][VoltageHack] Drawing cancel prompt PC")
			ENDIF
		ELSE //NOT IS_USING_KEYBOARD_AND_MOUSE
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				sGameStruct.bUpdatePrompts = TRUE
				PRINTLN("[MC][VoltageHack] Drawing cancel prompt")
			ENDIF
		ENDIF
	
		SPRITE_PLACEMENT thisSpritePlacement = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
		
		IF sGameStruct.bUpdatePrompts
			PRINTLN("[VOLTAGE] - MAINTAIN_VOLTAGE_SCALEFORM_CONTEXT - sGameStruct.bUpdatePrompts")
			REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sGameStruct.scaleformInstructionalButtons)
			
			// Exit
			IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					fAbortTimer += (0+@1000) // Add to the timer this frame
					DRAW_GENERIC_METER(ROUND(fAbortTimer), ci_HG_QUIT_TIME, "HG_INT_04a", DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 
									   DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(GFX_ORDER_BEFORE_HUD_PRIORITY_HIGH))
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameStruct.scaleformInstructionalButtons )
				ELSE
					fAbortTimer = 0.0
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameStruct.scaleformInstructionalButtons )
				ENDIF
			ELSE //IS_USING_KEYBOARD_AND_MOUSE
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE)
					fAbortTimer += (0+@1000) // Add to the timer this frame
					DRAW_GENERIC_METER(ROUND(fAbortTimer), ci_HG_QUIT_TIME, "HG_INT_04a", DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 
									   DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(GFX_ORDER_BEFORE_HUD_PRIORITY_HIGH))
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE, "HG_INT_04",sGameStruct.scaleformInstructionalButtons )
				ELSE
					fAbortTimer = 0.0
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE, "HG_INT_04",sGameStruct.scaleformInstructionalButtons )
				ENDIF
			ENDIF
					
			// Exit
//			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//				DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sGameplayData.sBaseStruct.tdCancelTimer), ci_HG_QUIT_TIME, "HG_INT_04a")
//				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
//			ELSE
//				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
//			ENDIF
		
			// Tutorial instructions
			// Select
			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HG_INT_01", sGameStruct.scaleformInstructionalButtons)
			
			// Change Selector
			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_ALL), "HG_INT_03", sGameStruct.scaleformInstructionalButtons)
			
			sGameStruct.bUpdatePrompts = FALSE
		ENDIF
		
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sGameStruct.scaleformInstructionalButtonsIndex, thisSpritePlacement, sGameStruct.scaleformInstructionalButtons)
		
	ELSE
		RESET_VOLTAGE_SCALEFORM_CONTEXT(sGameStruct)
	ENDIF
ENDPROC

PROC VOLTAGE_DRAW_COMMON_SPRITES(HG_CONTROL_STRUCT &sControllerStruct)

	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED)
		EXIT
	ENDIF

	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage_BG", "PHONE_BACKGROUND", 
	INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, (cfBASE_SCREEN_HEIGHT/2.0)),
	INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT),
	0.0,
	rgbaBackground)
	
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage_BG", "target_text", 
	INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-4.0, 88.0),
	INIT_VECTOR_2D(76, 16),
	0.0,
	VOLTAGE_GET_TEXT_COLOUR(RED_TEXT))
	
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage", "Phone_Icons", 
	INIT_VECTOR_2D(0.99 * ((cfBASE_SCREEN_WIDTH/1.778)*VOLTAGE_GET_CUSTOM_SCREEN_ASPECT_RATIO()), 66.0),
	INIT_VECTOR_2D(132, 44),
	0.0,
	rgbaBase)
	
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage", "MainInterface_BG", 
	INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, (cfBASE_SCREEN_HEIGHT/2.0)),
	INIT_VECTOR_2D(1064, 880),
	0.0,
	rgbaBase)
	
//	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage", "MainInterface_BG_GRID", 
//	INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, (cfBASE_SCREEN_HEIGHT/2.0)),
//	INIT_VECTOR_2D(1064, 880),
//	0.0,
//	rgbaBase)
	
	
	// Plug in values
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage",
		VOLTAGE_CONVERT_INT_TO_STRING(iValues[0]),
		INIT_VECTOR_2D(512, 307),
		INIT_VECTOR_2D(64, 104),
		0.0,
		rgbaBase)
					
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage",
		VOLTAGE_CONVERT_INT_TO_STRING(iValues[1]),
		INIT_VECTOR_2D(512, 543),
		INIT_VECTOR_2D(64, 104),
		0.0,
		rgbaBase)
	
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage",
		VOLTAGE_CONVERT_INT_TO_STRING(iValues[2]),
		INIT_VECTOR_2D(512, 777),
		INIT_VECTOR_2D(64, 104),
		0.0,
		rgbaBase)
	
	// Function icons
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_voltage",
		VOLTAGE_GET_ICON_TEXTURE(eFunctions[0]),
		INIT_VECTOR_2D(1383, 307),
		INIT_VECTOR_2D(108, 96),
		0.0,
		rgbaBase)
	
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_voltage",
		VOLTAGE_GET_ICON_TEXTURE(eFunctions[1]),
		INIT_VECTOR_2D(1383, 543),
		INIT_VECTOR_2D(108, 96),
		0.0,
		rgbaBase)
	
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_voltage",
		VOLTAGE_GET_ICON_TEXTURE(eFunctions[2]),
		INIT_VECTOR_2D(1383, 777),
		INIT_VECTOR_2D(108, 96),
		0.0,
		rgbaBase)
		
				
	VOLTAGE_RENDER_BATTERY_DRAIN(sControllerStruct)
	
	VOLTAGE_DRAW_INT_AS_SPRITES(iTargetValue, INIT_VECTOR_2D(956, 158))
	
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage_BG", "result_text", 
		INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-4.0, cfBASE_SCREEN_HEIGHT - 88.0),
		INIT_VECTOR_2D(76, 16),
		0.0,
		VOLTAGE_GET_TEXT_COLOUR(GREY_TEXT))
ENDPROC


PROC VOLTAGE_CLEANUP(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sGameStruct)
	HG_STORE_END_TIME(sControllerStruct, sGameStruct.tdTimer, sGameStruct.iMaxTime)
	CDEBUG1LN(DEBUG_MINIGAME, "VOLTAGE_CLEANUP - Hit this frame")

	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPIsland_Voltage")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPIsland_Voltage_BG")
	
	// Iterate play count
	IF NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_FIRST_PLAYED)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_VOLTAGE, GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_VOLTAGE) + 1)
	ENDIF
	
	INT iPlaythroughId = g_sIsland_Heist_Prep_Telemetry_data.playthroughId
	PRINTLN("VOLTAGE_CLEANUP, iPlaythroughId = ", iPlaythroughId)
	HACKING_GAME_UPDATE_TELEMETRY_H4(sControllerStruct, sGameStruct, iPlaythroughId, HASH("voltage hack"), iAttemptCounter+1, ROUND(fTimer))
	
	fTimer = 0
	iVoltageBS = 0
	iAttemptCounter = 0
	vs_current_state = VS_SLEEP
	RESET_VOLTAGE_SCALEFORM_CONTEXT(sGameStruct)
	STOP_SOUND(sGameStruct.iHGSoundBitset)
	STOP_SOUND(sGameStruct.iHGBackgroundLoopSndId)
	
	sGameStruct.iHGSoundBitset = -1
	sGameStruct.iHGBackgroundLoopSndId = -1
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEI4/DLC_HEI4_V_MG")
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_H4_Voltage_Hack_Scene")
		STOP_AUDIO_SCENE("DLC_H4_Voltage_Hack_Scene")
	ENDIF
	
	CLEAR_HELP() // Clear any help message on screen
	HACKING_COMMON_CLEANUP(sControllerStruct, sGameStruct)
ENDPROC

PROC PROCESS_VOLTAGE_GAME_MAIN_LOOP(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sGameStruct)

	PROCESS_COMMON_HACKING_EVERY_FRAME_MISC(sGameStruct, sControllerStruct)
	
	IF IS_PC_VERSION()
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		
#IF IS_DEBUG_BUILD
	VOLTAGE_PROCESS_DEBUG_WIDGETS(sGameStruct, sControllerStruct)
#ENDIF
		
	SWITCH vs_current_state
			
		CASE VS_SLEEP
			PROCESS_HACKING_GAME_SLEEP(sGameStruct, sControllerStruct)
			vs_current_state = VS_INIT
		BREAK
			
		CASE VS_INIT
		
			VG_INIT_VARIABLES(sControllerStruct, sGameStruct)
			// Run the startup process
			IF sGameStruct.movieId != NULL
				PROCESS_HACKING_GAME_STARTUP(sControllerStruct, sGameStruct)
			ENDIF
			
			CDEBUG1LN(DEBUG_MINIGAME, "[VH][BAZ][Voltage Hack] Running Init")
			IF NETWORK_IS_GAME_IN_PROGRESS()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				DISABLE_INTERACTION_MENU() 
			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			//Turn on blinders for multihead displays.
			SET_MULTIHEAD_SAFE(TRUE, TRUE)
			g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
			
			//Prevent notifications from appearing during the minigame.
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = TRUE
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
			
			sGameStruct.sQuitTimer.action = INPUT_FRONTEND_CANCEL
			sGameStruct.sQuitTimer.control = FRONTEND_CONTROL
			
			sGameStruct.sQuitTimerPC.control = FRONTEND_CONTROL
			sGameStruct.sQuitTimerPC.action = INPUT_FRONTEND_DELETE

			CLEAR_BIT(sControllerStruct.iBS, ciHGBS_QUIT)
			
			RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(sGameStruct.scaleformInstructionalButtons)
			
			vs_current_state = VS_REQUEST_ASSETS
		
		BREAK
	
		CASE VS_REQUEST_ASSETS
		
			REQUEST_STREAMED_TEXTURE_DICT("MPIsland_Voltage")
			REQUEST_STREAMED_TEXTURE_DICT("MPIsland_Voltage_BG")
			
			// Audio
			sGameStruct.sAudioSet = "DLC_H4_Voltage_Minigame_Sounds"
			
			// We only need to grab and assign a sound id if we do not currently have one.
//			IF sGameStruct.iHGSoundBitset = -1
//				sGameStruct.iHGSoundBitset = GET_SOUND_ID()
//			ENDIF
			
			IF sGameStruct.iHGBackgroundLoopSndId = -1
				sGameStruct.iHGBackgroundLoopSndId = GET_SOUND_ID()
			ENDIF
			
			IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_HEI4/DLC_HEI4_V_MG")
				CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][Voltage_hack] Not Loaded DLC_HEI4/DLC_HEI4_V_MG")
				EXIT
			ENDIF
					
			PLAY_SOUND_FRONTEND(-1, "Loading_Bink", sGameStruct.sAudioSet)
				
			vs_current_state = VS_LOAD_CHECK
		BREAK

		CASE VS_LOAD_CHECK
		
			IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPIsland_Voltage")
				CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][PROCESS_VOLTAGE_GAME] VS_LOAD_CHECK - MPIsland_Voltage not loaded yet")
				BREAK
			ENDIF
		
			IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPIsland_Voltage_BG")
				CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][PROCESS_VOLTAGE_GAME] VS_LOAD_CHECK - MPIsland_Voltage_BG not loaded yet")
				BREAK
			ENDIF
			
			//IF NOT HACKING_IS_SAFE_TO_HACK()
				//CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][PROCESS_VOLTAGE_GAME] HACKING_IS_SAFE_TO_HACK - Returning false this frame. Cannot progress.")
				//BREAK
			//ENDIF

			IF MAINTAIN_BINK_MOVIE(sGameStruct)
			OR HAS_NET_TIMER_EXPIRED(sGameStruct.tdAnimationTimer, 4000)
				RELEASE_BINK_MOVIE(sGameStruct.movieId)
				UPDATE_HACKING_GAME_STATE(sGameStruct, HACKING_GAME_PLAY)
				PLAY_SOUND_FRONTEND(-1, "Main_Screen_Draw", sGameStruct.sAudioSet)
				vs_current_state = VS_PICK_LINK
			ENDIF
		
		BREAK
		
		CASE VS_PICK_LINK
			//CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][PROCESS_VOLTAGE_GAME] VS_PICK_LINK - Picking initial link node")
			IF sGameStruct.eHackingGameState != HACKING_GAME_PLAY
				BREAK
			ENDIF
			
			VOLTAGE_DRAW_COMMON_SPRITES(sControllerStruct)
							
			VOLTAGE_DRAW_INT_AS_SPRITES(ROUND(fCountValue), INIT_VECTOR_2D(956, 920), GREY_TEXT)

			VOLTAGE_CHECK_LEFT_STICK_NEUTRAL()
		
			IF VOLTAGE_INPUT_IS_UP_PRESSED(sGameStruct)
				VOLTAGE_SUB_ACTIVE_LINK()
				
				//TESTING Check for infinite
				WHILE VOLTAGE_IS_ACTIVE_NUMBER_LINKED(iActiveLink)
					VOLTAGE_SUB_ACTIVE_LINK()
				ENDWHILE

			ENDIF
			
			IF VOLTAGE_INPUT_IS_DOWN_PRESSED(sGameStruct)
				VOLTAGE_ITER_ACTIVE_LINK()

				//TESTING Check for infinite
				WHILE VOLTAGE_IS_ACTIVE_NUMBER_LINKED(iActiveLink)
					VOLTAGE_ITER_ACTIVE_LINK()
				ENDWHILE

			ENDIF
			
			// Don't draw rest of the assets if we've exceeded the available time.
			IF fTimer >= ciVH_MAX_TIME
				EXIT
			ENDIF
			
			SWITCH INT_TO_ENUM(VOLTAGE_NODES, iActiveLink)
				CASE TOP_VOLTAGE
					VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][1], YELLOW_VOLT)
					VOLTAGE_DRAW_NODE(gridNodes[0][1], YELLOW_VOLT)
//					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][1], gridNodes[4][1], YELLOW_VOLT)
				BREAK
			
				CASE MIDDLE_VOLTAGE
					VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][5], RED_VOLT)
					VOLTAGE_DRAW_NODE(gridNodes[0][5], RED_VOLT)
//					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][5], gridNodes[4][5], RED_VOLT)
				BREAK
			
				CASE BOTTOM_VOLTAGE
					VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][9], BLUE_VOLT)
					VOLTAGE_DRAW_NODE(gridNodes[0][9], BLUE_VOLT)
//					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][9], gridNodes[4][9], BLUE_VOLT)
				BREAK
			ENDSWITCH
			
			// Fade up on the assets
			IF fTimer < ciVH_MAX_TIME
			AND NOT VOLTAGE_HANDLE_ALPHA_FADE_UP()
				CDEBUG1LN(DEBUG_MINIGAME, "VOLTAGE_HANDLE_ALPHA_FADE_UP - Checking this frame")
				
				// Draw the overlay
				VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage_BG", "VOLTAGE_PIXEL_GRID", 
					INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, (cfBASE_SCREEN_HEIGHT/2.0)),
					INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT),
					0.0,
					rgbaOverlay)
				
				EXIT
			ENDIF
			
			// Advance when selected
			IF VOLTAGE_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				PLAY_SOUND_FRONTEND(-1, "OS_Draw", sGameStruct.sAudioSet)
				// Reset selection to be straight across
				iGridSelectionX = 12
				iGridSelectionY = GET_VOLTAGE_Y_COORD(INT_TO_ENUM(VOLTAGE_NODES, iActiveLink))
								
				CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_PLAY_FAIL_SOUND)
				CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_PLAY_SUCCESS_SOUND)
				
				iActiveCounter = iActiveLink
				
				WHILE VOLTAGE_IS_NODE_LINKED(INT_TO_ENUM(VOLTAGE_NODES, iActiveCounter))		
					iActiveCounter++
					
					IF iActiveCounter >= 3
						iActiveCounter = 0
					ENDIF
					
					iGridSelectionY = GET_VOLTAGE_Y_COORD(INT_TO_ENUM(VOLTAGE_NODES, iActiveCounter))
				ENDWHILE
				
				vs_current_state = VS_MAIN_LOOP
			ENDIF
				
			IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_TOP_NUM_LINKED)
				VOLTAGE_LINK_NODES_NEW(voltageLinks[0].startNode, voltageLinks[0].endNode, YELLOW_VOLT_SET)
			ENDIF
		
			IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_MIDDLE_NUM_LINKED)
				VOLTAGE_LINK_NODES_NEW(voltageLinks[1].startNode, voltageLinks[1].endNode, RED_VOLT_SET, 1)
			ENDIF
		
			IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_BOTTOM_NUM_LINKED)
				VOLTAGE_LINK_NODES_NEW(voltageLinks[2].startNode, voltageLinks[2].endNode, BLUE_VOLT_SET, 2)
			ENDIF
			
			// Count up or down when value changes
			IF fCountValue > iCurrentValue
				fCountValue -= (0.0+@50.0)
				
				IF fCountValue < iCurrentValue
					fCountValue = TO_FLOAT(iCurrentValue)
				ENDIF
			ENDIF
			
			IF fCountValue < iCurrentValue
				fCountValue += (0.0+@50.0)
				
				IF fCountValue > iCurrentValue
					fCountValue = TO_FLOAT(iCurrentValue)
				ENDIF
			ENDIF
			
			// You can step back by cancelling
			IF VOLTAGE_IS_MENU_DECLINE_BUTTON_JUST_PRESSED()
			AND iLinkCount > 0
				CDEBUG1LN(DEBUG_MINIGAME, "[BAZ]PROCESS_VOLTAGE_GAME_MAIN_LOOP - VS_PICK_LINK - Cancelling back a step while in selecting number.")
				
				iLinkCount--
				VOLTAGE_UNLINK(voltageLinks[iActiveLink].endNode, iActiveLink)
				voltageLinks[iActiveLink].iLinkValue = 0
				
				IF iLinkCount = 0
					IF iFirstLink >= 0
						iActiveLink = iFirstLink
						iFirstLink = -1
					ENDIF
				ENDIF
				
				IF iLinkCount = 1
					IF iSecondLink >= 0
						iActiveLink = iSecondLink
						iSecondLink = -1
					ENDIF
				ENDIF
				
				VOLTAGE_UNLINK(voltageLinks[iActiveLink].endNode, iActiveLink)
				
				SWITCH iActiveLink
					CASE 0
						voltageLinks[0].startNode = TOP_VOLTAGE
						voltageLinks[0].endNode = BOTTOM_VOLTAGE
						voltageLinks[0].eLinkColour = YELLOW_VOLT
						voltageLinks[0].iLinkValue = 0
					BREAK
					
					CASE 1
						voltageLinks[1].startNode = MIDDLE_VOLTAGE
						voltageLinks[1].endNode = MIDDLE_VOLTAGE
						voltageLinks[1].eLinkColour = RED_VOLT
						voltageLinks[1].iLinkValue = 0
					BREAK
					
					CASE 2
						voltageLinks[2].startNode = BOTTOM_VOLTAGE
						voltageLinks[2].endNode = TOP_VOLTAGE
						voltageLinks[2].eLinkColour = BLUE_VOLT
						voltageLinks[2].iLinkValue = 0
					BREAK
				ENDSWITCH
				
				//VOLTAGE_SUB_ACTIVE_LINK()
				//vs_current_state = VS_MAIN_LOOP
				PLAY_SOUND_FRONTEND(-1, "Disconnect_Wire", sGameStruct.sAudioSet)

				
				CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][PROCESS_VOLTAGE_GAME] PROCESS_VOLTAGE_GAME_MAIN_LOOP - Back up one step")
			ENDIF		
			
			
		BREAK
		
		CASE VS_MAIN_LOOP
		
			IF sGameStruct.eHackingGameState != HACKING_GAME_PLAY
				BREAK
			ENDIF
		
			iCurrentValue = 
				voltageLinks[0].iLinkValue +
				voltageLinks[1].iLinkValue +
				voltageLinks[2].iLinkValue 
		
			// Place all main loop logic here
			VOLTAGE_DRAW_COMMON_SPRITES(sControllerStruct)
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_SHOW_SET)
			AND iLinkCount < 3
			
				IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_TOP_NUM_LINKED)
					VOLTAGE_LINK_NODES_NEW(voltageLinks[0].startNode, voltageLinks[0].endNode, YELLOW_VOLT_SET)
				ENDIF
			
				IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_MIDDLE_NUM_LINKED)
					VOLTAGE_LINK_NODES_NEW(voltageLinks[1].startNode, voltageLinks[1].endNode, RED_VOLT_SET, 1)
				ENDIF
			
				IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_BOTTOM_NUM_LINKED)
					VOLTAGE_LINK_NODES_NEW(voltageLinks[2].startNode, voltageLinks[2].endNode, BLUE_VOLT_SET, 2)
				ENDIF
				
			ENDIF
			
			IF iLinkCount < 3
				
				voltageLinks[iActiveLink].endNode = GET_VOLTAGE_FROM_Y_COORD(iGridSelectionY)
				
				IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_SHOW_SET)
					VOLTAGE_LINK_DRAW_ACTIVE_LINE(sGameStruct, voltageLinks[iActiveLink].startNode, voltageLinks[iActiveLink].eLinkColour, iActiveLink)
				ENDIF
									
				// If the grid selection is clear then buffer the final link value
				// Used by the on-screen RESULT.
				IF iGridSelectionX >= 9				
					voltageLinks[iActiveLink].iLinkValue = VOLTAGE_APPLY_RANDOM_MOD(iValues[iActiveLink], eFunctions[ENUM_TO_INT(voltageLinks[iActiveLink].endNode)])
				ELSE
					voltageLinks[iActiveLink].iLinkValue = 0
				ENDIF
					
				IF fTimer >= ciVH_MAX_TIME
				AND NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_SHOW_SET)
					
					STOP_SOUND(sGameStruct.iHGSoundBitset)
					STOP_SOUND(sGameStruct.iHGBackgroundLoopSndId)
					PLAY_SOUND_FRONTEND(-1, "Minigame_Failure", sGameStruct.sAudioSet) // Fail sound if you didn't hook everything up
					iAttemptCounter += 1

					CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_PLAY_FAIL_SOUND)
					SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_SHOW_SET)
				ENDIF
											
				IF VOLTAGE_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
				AND NOT VOLTAGE_IS_NODE_LINKED(voltageLinks[iActiveLink].endNode)
				AND NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_SHOW_SET)
					VOLTAGE_SET_LINKED(voltageLinks[iActiveLink].endNode)
					VOLTAGE_GET_MOD_SET_SFX(eFunctions[iActiveLink], sGameStruct)
					SWITCH iActiveLink
					
						CASE 0
							SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_TOP_NUM_LINKED)
						BREAK
						
						CASE 1
							SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_MIDDLE_NUM_LINKED)
						BREAK
					
						CASE 2
							SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_BOTTOM_NUM_LINKED)
						BREAK
					
					ENDSWITCH
					
					IF iLinkCount >= 2
						IF iCurrentValue = iTargetValue
							PLAY_SOUND_FRONTEND(-1, "All_Connected_Correct", sGameStruct.sAudioSet)
						ELSE
							PLAY_SOUND_FRONTEND(-1, "All_Connected_Incorrect", sGameStruct.sAudioSet)
						ENDIF
					ENDIF 
					
					SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_SHOW_SET)
				ENDIF
				
				// Animation when setting a voltage link
				IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_SHOW_SET)
					// Add to the timer
					fAnimTimer += (0.0+@1000)
					
					IF fTimer < ciVH_MAX_TIME
					AND fAnimTimer < 1250.0

						IF (fAnimTimer % 500.0) <= 250
							VOLTAGE_LINK_NODES_NEW(voltageLinks[iActiveLink].startNode, voltageLinks[iActiveLink].endNode, voltageLinks[iActiveLink].eLinkColour, iActiveLink)
						ELSE
							VOLTAGE_LINK_NODES_NEW(voltageLinks[iActiveLink].startNode, voltageLinks[iActiveLink].endNode, INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[iActiveLink].eLinkColour) + 4)), iActiveLink)
						ENDIF
					ENDIF
					
					// Regular animation timer for flashing objects.
					IF fAnimTimer >= 1250.0
					//OR GET_BINK_MOVIE_TIME(bmID) >= 99.0
					
						IF iLinkCount >= 2
							IF fAnimTimer <= 2500.0
											
								VOLTAGE_TEXT_COLOURS colourToUse
								
								// Get the colour and SFX
								IF iCurrentValue = iTargetValue
									IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_PLAY_SUCCESS_SOUND)
										PLAY_SOUND_FRONTEND(-1, "Minigame_Success", sGameStruct.sAudioSet) // Fail sound if you didn't hook everything up
										SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_PLAY_SUCCESS_SOUND)
									ENDIF
									colourToUse = GREEN_TEXT
								ELSE
									IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_PLAY_FAIL_SOUND)
										PLAY_SOUND_FRONTEND(-1, "Minigame_Failure", sGameStruct.sAudioSet) // Fail sound if you didn't hook everything up
										iAttemptCounter += 1
										SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_PLAY_FAIL_SOUND)
									ENDIF
									colourToUse = RED_TEXT
								ENDIF
				
								// Colour of text determined above
								VOLTAGE_DRAW_INT_AS_SPRITES(ROUND(fCountValue), INIT_VECTOR_2D(956, 920), colourToUse)
				
								VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage_BG", "result_text", 
									INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-4.0, cfBASE_SCREEN_HEIGHT - 88.0),
									INIT_VECTOR_2D(76, 16),
									0.0,
									VOLTAGE_GET_TEXT_COLOUR(colourToUse))
								
								IF (fAnimTimer % 500.0) <= 250
									VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][1], voltageLinks[0].eLinkColour)
									VOLTAGE_DRAW_NODE(gridNodes[0][1], voltageLinks[0].eLinkColour)
									VOLTAGE_LINK_NODES_NEW(voltageLinks[0].startNode, voltageLinks[0].endNode, voltageLinks[0].eLinkColour, 0)
									VOLTAGE_DRAW_MOD_NODE(gridNodes[12][GET_VOLTAGE_Y_COORD(voltageLinks[0].endNode)], voltageLinks[0].eLinkColour)
									
									VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][5], voltageLinks[1].eLinkColour)
									VOLTAGE_DRAW_NODE(gridNodes[0][5], voltageLinks[1].eLinkColour)
									VOLTAGE_LINK_NODES_NEW(voltageLinks[1].startNode, voltageLinks[1].endNode, voltageLinks[1].eLinkColour, 1)
									VOLTAGE_DRAW_MOD_NODE(gridNodes[12][GET_VOLTAGE_Y_COORD(voltageLinks[1].endNode)], voltageLinks[1].eLinkColour)
									
									VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][9], voltageLinks[2].eLinkColour)
									VOLTAGE_DRAW_NODE(gridNodes[0][9], voltageLinks[2].eLinkColour)
									VOLTAGE_LINK_NODES_NEW(voltageLinks[2].startNode, voltageLinks[2].endNode, voltageLinks[2].eLinkColour, 2)
									VOLTAGE_DRAW_MOD_NODE(gridNodes[12][GET_VOLTAGE_Y_COORD(voltageLinks[2].endNode)], voltageLinks[2].eLinkColour)
								ELSE
									//VOLTAGE_LINK_NODES_NEW(voltageLinks[iActiveLink].startNode, voltageLinks[iActiveLink].endNode, INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[iActiveLink].eLinkColour) + 4)), iActiveLink)
									VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][1], INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[0].eLinkColour) + 4)))
									VOLTAGE_DRAW_NODE(gridNodes[0][1], INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[0].eLinkColour) + 4)))
									VOLTAGE_LINK_NODES_NEW(voltageLinks[0].startNode, voltageLinks[0].endNode, INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[0].eLinkColour) + 4)), 0)
									VOLTAGE_DRAW_MOD_NODE(gridNodes[12][GET_VOLTAGE_Y_COORD(voltageLinks[0].endNode)], INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[0].eLinkColour) + 4)))
									
									VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][5], INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[1].eLinkColour) + 4)))
									VOLTAGE_DRAW_NODE(gridNodes[0][5], INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[1].eLinkColour) + 4)))
									VOLTAGE_LINK_NODES_NEW(voltageLinks[1].startNode, voltageLinks[1].endNode, INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[1].eLinkColour) + 4)), 1)
									VOLTAGE_DRAW_MOD_NODE(gridNodes[12][GET_VOLTAGE_Y_COORD(voltageLinks[1].endNode)], INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[1].eLinkColour) + 4)))
									
									VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][9], INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[2].eLinkColour) + 4)))
									VOLTAGE_DRAW_NODE(gridNodes[0][9], INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[2].eLinkColour) + 4)))
									VOLTAGE_LINK_NODES_NEW(voltageLinks[2].startNode, voltageLinks[2].endNode, INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[2].eLinkColour) + 4)), 2)
									VOLTAGE_DRAW_MOD_NODE(gridNodes[12][GET_VOLTAGE_Y_COORD(voltageLinks[2].endNode)], INT_TO_ENUM(VOLTAGE_LINE_COLOURS, (ENUM_TO_INT(voltageLinks[2].eLinkColour) + 4)))
								ENDIF
								
								// Draw the overlay
								VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage_BG", "VOLTAGE_PIXEL_GRID", 
									INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, (cfBASE_SCREEN_HEIGHT/2.0)),
									INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT),
									0.0,
									rgbaOverlay)
								
								EXIT
							ENDIF
						ENDIF
						
						CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_SHOW_SET)
						fAnimTimer = 0
						
						IF iLinkCount = 0
							iFirstLink = iActiveLink
						ENDIF
						
						IF iLinkCount = 1
							iSecondLink = iActiveLink
						ENDIF
						
						iLinkCount++
						
						IF iLinkCount <= 2
							WHILE VOLTAGE_IS_ACTIVE_NUMBER_LINKED(iActiveLink)
								VOLTAGE_ITER_ACTIVE_LINK()
							ENDWHILE
						
							vs_current_state = VS_PICK_LINK // Testing. May break flow.
						ENDIF
					ENDIF

				ENDIF
				
			ELSE // iLinkCount >= 3
 				// Passed conditions
				IF iCurrentValue = iTargetValue
				
					VOLTAGE_DRAW_INT_AS_SPRITES(ROUND(fCountValue), INIT_VECTOR_2D(956, 920), GREEN_TEXT)
				
					VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage_BG", "result_text", 
						INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-4.0, cfBASE_SCREEN_HEIGHT - 88.0),
						INIT_VECTOR_2D(76, 16),
						0.0,
						VOLTAGE_GET_TEXT_COLOUR(GREEN_TEXT))
				
					// Fade up on the assets
					IF NOT VOLTAGE_HANDLE_ALPHA_FADE_OUT()
						BREAK
					ELSE
						IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_SUCCESS_BINK_SOUND)
							STOP_SOUND(sGameStruct.iHGBackgroundLoopSndId)
							PLAY_SOUND_FRONTEND(-1, "Success_Bink", sGameStruct.sAudioSet)
							CLEAR_HELP() // Stop all help
							SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_SUCCESS_BINK_SOUND)
						ENDIF
					ENDIF
				
					IF VOLTAGE_PROCESS_HACKING_GAME_BINK(sControllerStruct, sGameStruct, ciVH_END_DELAY, "Voltage_Success")
					
						SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPIsland_Voltage")
						SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPIsland_Voltage_BG")
						HACKING_COMMON_CLEANUP(sControllerStruct, sGameStruct)
						
						SET_BIT(sControllerStruct.iBS, ciHGBS_PASSED)
												
						// url:bugstar:6771224 - Prep - Scoping - DJ - Local player was unable to abort the hack whilst on the hacking mini game in the tower
						IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_FIRST_PLAYED)
							SET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_VOLTAGE, GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_VOLTAGE) + 1)
						ENDIF
						
						//HACKING_GAME_UPDATE_TELEMETRY_H4(sControllerStruct, sGameStruct, 0, HASH("voltage hack"), iAttemptCounter+1)
												
						STOP_SOUND(sGameStruct.iHGBackgroundLoopSndId)
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEI4/DLC_HEI4_V_MG")
					ENDIF

					BREAK
				ELSE // You did not hit the target so reset the minigame!
					// Timer penalty? Saying no as it doesn't make too much sense in context.
					//fTimer += 500					
					VOLTAGE_RESET_LINKS(sControllerStruct)
					BREAK
				ENDIF
				
			ENDIF
			
			// You can step back by cancelling
			IF VOLTAGE_IS_MENU_DECLINE_BUTTON_JUST_PRESSED()
				voltageLinks[iActiveLink].iLinkValue = 0
				
				VOLTAGE_UNLINK(voltageLinks[iActiveLink].endNode, iActiveLink)
				
				SWITCH iActiveLink
					CASE 0
						voltageLinks[0].startNode = TOP_VOLTAGE
						voltageLinks[0].endNode = BOTTOM_VOLTAGE
						voltageLinks[0].eLinkColour = YELLOW_VOLT
						voltageLinks[0].iLinkValue = 0
					BREAK
					
					CASE 1
						voltageLinks[1].startNode = MIDDLE_VOLTAGE
						voltageLinks[1].endNode = MIDDLE_VOLTAGE
						voltageLinks[1].eLinkColour = RED_VOLT
						voltageLinks[1].iLinkValue = 0
					BREAK
					
					CASE 2
						voltageLinks[2].startNode = BOTTOM_VOLTAGE
						voltageLinks[2].endNode = TOP_VOLTAGE
						voltageLinks[2].eLinkColour = BLUE_VOLT
						voltageLinks[2].iLinkValue = 0
					BREAK
				ENDSWITCH
				
				PLAY_SOUND_FRONTEND(-1, "Disconnect_Wire", sGameStruct.sAudioSet)
				vs_current_state = VS_PICK_LINK
				
				CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][PROCESS_VOLTAGE_GAME] PROCESS_VOLTAGE_GAME_MAIN_LOOP - Back up one step")
			ENDIF		

			// Count up or down when value changes
			IF fCountValue > iCurrentValue
				fCountValue -= (0.0+@50.0)
				
				IF fCountValue < iCurrentValue
					fCountValue = TO_FLOAT(iCurrentValue)
				ENDIF
			ENDIF
			
			IF fCountValue < iCurrentValue
				fCountValue += (0.0+@50.0)
				
				IF fCountValue > iCurrentValue
					fCountValue = TO_FLOAT(iCurrentValue)
				ENDIF
			ENDIF
										
			// Turn Red if we are above the limit
			// url:bugstar:6723016
			IF iCurrentValue < iTargetValue
				VOLTAGE_DRAW_INT_AS_SPRITES(ROUND(fCountValue), INIT_VECTOR_2D(956, 920), GREY_TEXT)
				
				VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage_BG", "result_text", 
					INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-4.0, cfBASE_SCREEN_HEIGHT - 88.0),
					INIT_VECTOR_2D(76, 16),
					0.0,
					VOLTAGE_GET_TEXT_COLOUR(GREY_TEXT))
					
			ELIF iCurrentValue = iTargetValue
				VOLTAGE_DRAW_INT_AS_SPRITES(ROUND(fCountValue), INIT_VECTOR_2D(956, 920), GREEN_TEXT)
				
				VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage_BG", "result_text", 
					INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-4.0, cfBASE_SCREEN_HEIGHT - 88.0),
					INIT_VECTOR_2D(76, 16),
					0.0,
					VOLTAGE_GET_TEXT_COLOUR(GREEN_TEXT))
					
			ELIF iCurrentValue > iTargetValue
				VOLTAGE_DRAW_INT_AS_SPRITES(ROUND(fCountValue), INIT_VECTOR_2D(956, 920), RED_TEXT)
				
				VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage_BG", "result_text", 
					INIT_VECTOR_2D((cfBASE_SCREEN_WIDTH/2.0)-4.0, cfBASE_SCREEN_HEIGHT - 88.0),
					INIT_VECTOR_2D(76, 16),
					0.0,
					VOLTAGE_GET_TEXT_COLOUR(RED_TEXT))
			ENDIF
						
			//VOLTAGE_RENDER_BATTERY_DRAIN()							
		BREAK
	
	ENDSWITCH
	
	// Handle the looping background noise
	IF vs_current_state = VS_PICK_LINK
	OR vs_current_state = VS_MAIN_LOOP
	
		// Draw the overlay
		IF NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED)
			VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage_BG", "VOLTAGE_PIXEL_GRID", 
				INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH/2.0, (cfBASE_SCREEN_HEIGHT/2.0)),
				INIT_VECTOR_2D(cfBASE_SCREEN_WIDTH, cfBASE_SCREEN_HEIGHT),
				0.0,
				rgbaOverlay)
		ENDIF
		
// Option to mute the sound at George's request
#IF IS_DEBUG_BUILD
		IF bDebugSoundMute
			IF NOT HAS_SOUND_FINISHED(sGameStruct.iHGBackgroundLoopSndId)
				STOP_SOUND(sGameStruct.iHGBackgroundLoopSndId)			
			ENDIF
			
			EXIT
		ENDIF
#ENDIF
		// Stop the sound if the timer has started
		IF HAS_NET_TIMER_STARTED(sGameStruct.tdEndDelay) // If this timer is active we can assume that a bink is active
			IF NOT HAS_SOUND_FINISHED(sGameStruct.iHGBackgroundLoopSndId)
				STOP_SOUND(sGameStruct.iHGBackgroundLoopSndId)			
			ENDIF
			EXIT		
		ENDIF
		
		IF HAS_SOUND_FINISHED(sGameStruct.iHGBackgroundLoopSndId)
			PLAY_SOUND_FRONTEND(sGameStruct.iHGBackgroundLoopSndId, "Background_loop", sGameStruct.sAudioSet)
		ENDIF
		
		BOOL bGlitch = ((iLinkCount >= 3) AND (iCurrentValue != iTargetValue)) // Glitch if hitting a wrong voltage
		
		IF (iCurrentValue > iTargetValue)
			bGlitch = TRUE // Also makes sense to do it if we overshoot
		ENDIF
		
		SET_VARIABLE_ON_SOUND(sGameStruct.iHGBackgroundLoopSndId, "Timebar", ((ciVH_MAX_TIME-fTimer)/10000.0))
		SET_VARIABLE_ON_SOUND(sGameStruct.iHGBackgroundLoopSndId, "Voltage_Current", TO_FLOAT(iCurrentValue))
		SET_VARIABLE_ON_SOUND(sGameStruct.iHGBackgroundLoopSndId, "Voltage_Target", TO_FLOAT(iTargetValue))
		SET_VARIABLE_ON_SOUND(sGameStruct.iHGBackgroundLoopSndId, "Glitch", TO_FLOAT(BOOL_TO_INT(bGlitch)))
		
	ENDIF

	UNUSED_PARAMETER(sGameStruct)
	UNUSED_PARAMETER(sControllerStruct)

#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		RESET_VOLTAGE_SCALEFORM_CONTEXT(sGameStruct)
		HACKING_COMMON_CLEANUP(sControllerStruct, sGameStruct)
	ENDIF
#ENDIF

ENDPROC

PROC VOLTAGE_SHOW_HELP_TEXT(HACKING_GAME_STRUCT &sGameStruct)

	IF sGameStruct.eHackingGameState != HACKING_GAME_PLAY
	OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		EXIT
	ENDIF

	IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_SHOWN_HELP)
		SWITCH (FLOOR(fTimer / 12000))
			CASE 0
				PRINT_HELP_NO_SOUND("HG_VH_HELP_00")
			BREAK
			
			CASE 1
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					PRINT_HELP_NO_SOUND("HG_VH_HELP_01_PC")
				ELSE
					PRINT_HELP_NO_SOUND("HG_VH_HELP_01")
				ENDIF
			BREAK
			
			CASE 2
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					PRINT_HELP_NO_SOUND("HG_VH_HELP_02_PC")
				ELSE
					PRINT_HELP_NO_SOUND("HG_VH_HELP_02")
				ENDIF
			BREAK
			
			CASE 3
				PRINT_HELP_FOREVER_NO_SOUND("HG_VH_HELP_03")
				SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_SHOWN_HELP)
			BREAK
		ENDSWITCH
	ENDIF

//	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HG_fr_HELP_04a")
//	AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//		EXIT
//	ENDIF
	
//	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//		IF NOT IS_BIT_SET(iVoltageBS, ciHG_ABORT_WARNING)
//			RESET_NET_TIMER(sGameStruct.tdCancelTimer)
//			SET_BIT(iVoltageBS, ciHG_ABORT_WARNING)		ENDIF
//	ENDIF
//	
//	IF NOT IS_CONTROL_HELD(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//	AND IS_BIT_SET(iVoltageBS, ciHG_ABORT_WARNING)
//		CLEAR_BIT(iVoltageBS, ciHG_ABORT_WARNING)
//		
//	ENDIF

//	IF IS_BIT_SET(iVoltageBS, ciHG_ABORT_WARNING)
//	
//		IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_QUIT_ONCE)
//			PRINT_HELP_NO_SOUND("HG_fr_HELP_04a", ciHG_FIRST_ABORT_TIME)
//		ELSE
//			PRINT_HELP_NO_SOUND("HG_fr_HELP_04")
//		ENDIF
//			
//		IF HAS_NET_TIMER_EXPIRED(sGameStruct.tdCancelTimer, ciHG_ABORT_WARNING_TIME)
//			CLEAR_BIT(iVoltageBS, ciHG_ABORT_WARNING)
//		ENDIF
//
//		EXIT
//	ENDIF
	
ENDPROC

PROC PROCESS_VOLTAGE_GAME(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sGameStruct, BOOL bTimeout = TRUE)

	PROCESS_VOLTAGE_GAME_MAIN_LOOP(sControllerStruct, sGameStruct)
	VOLTAGE_SHOW_HELP_TEXT(sGameStruct)
	MAINTAIN_VOLTAGE_SCALEFORM_CONTEXT(sControllerStruct, sGameStruct) // Abort prompt
	HIDE_HUD_AND_RADAR_THIS_FRAME() //url:bugstar:6779960 - Voltage Minigame - The minimap/radar appears on the hacking screen

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_HELD(sGameStruct.sQuitTimerPC, TRUE, ci_HG_QUIT_TIME)
			CDEBUG1LN(DEBUG_MINIGAME, "[HACKING_GAME] Player has backed out by pressing CANCEL PC")
			HG_STORE_END_TIME(sControllerStruct, sGameStruct.tdTimer, sGameStruct.iMaxTime)
			//SET_BIT(sControllerStruct.iBS, ciHGBS_FAILED)
			//UPDATE_HACKING_GAME_STATE(sCommonStruct, HACKING_GAME_FAIL)
			SET_BIT(sControllerStruct.iBS, ciHGBS_QUIT)
		ENDIF
	ELSE
		IF IS_CONTROL_HELD(sGameStruct.sQuitTimer, TRUE, ci_HG_QUIT_TIME)
			CDEBUG1LN(DEBUG_MINIGAME, "[HACKING_GAME] Player has backed out by pressing CANCEL")
			HG_STORE_END_TIME(sControllerStruct, sGameStruct.tdTimer, sGameStruct.iMaxTime)
			//SET_BIT(sControllerStruct.iBS, ciHGBS_FAILED)
			//UPDATE_HACKING_GAME_STATE(sCommonStruct, HACKING_GAME_FAIL)
			SET_BIT(sControllerStruct.iBS, ciHGBS_QUIT)
		ENDIF
	ENDIF
	
	// Game will not timeout if this is the first time the voltage game is played.
	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_FIRST_PLAYED) 
		EXIT
	ENDIF
	
	IF fTimer >= ciVH_MAX_TIME
	AND NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_SHOW_SET)// Don't time out on first attempt
	AND NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED)
	
		// Fade up on the assets
		IF bTimeout
		AND NOT VOLTAGE_HANDLE_ALPHA_FADE_OUT()
			CDEBUG1LN(DEBUG_MINIGAME, "VOLTAGE_HANDLE_ALPHA_FADE_OUT - Checking this frame")
			EXIT
		ENDIF
		
		IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_PLAY_FAIL_SOUND)
			STOP_SOUND(sGameStruct.iHGBackgroundLoopSndId)
			PLAY_SOUND_FRONTEND(-1, "Fail_Bink", sGameStruct.sAudioSet)
			CLEAR_HELP() // Stop all help
			SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_PLAY_FAIL_SOUND)
		ENDIF
	
		IF VOLTAGE_PROCESS_HACKING_GAME_BINK(sControllerStruct, sGameStruct, ciVH_END_DELAY, "Voltage_Fail")
			STOP_SOUND(sGameStruct.iHGBackgroundLoopSndId)
			IF bTimeout
				CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][VOLTAGE_HACK] Game has timed out")
				SET_BIT(sControllerStruct.iBS, ciHGBS_FAILED)
				//PROCESS_HACKING_GAME_FAIL(sControllerStruct, sGameStruct, ciVH_END_DELAY, "Success_FC")
				UPDATE_HACKING_GAME_STATE(sGameStruct, HACKING_GAME_FAIL)
			ELSE // Some implementations will require game to remain active.
				iAttemptCounter += 1
				VOLTAGE_RESET_LINKS(sControllerStruct) // Reset the board.
				fTimer = 0.0
				
				sControllerStruct.iTimeElapsed = ROUND(fTimer)
				//HACKING_GAME_UPDATE_TELEMETRY_H4(sControllerStruct, sGameStruct, 0, HASH("voltage hack"), iAttemptCounter+1)
				
				vs_current_state = VS_PICK_LINK
			ENDIF
		ENDIF
	ENDIF

ENDPROC
