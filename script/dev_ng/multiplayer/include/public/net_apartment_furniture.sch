USING "commands_interiors.sch"
USING "net_prints.sch"

#IF FEATURE_APARTMENT_FURNITURE

STRUCT FURNITURE_DETAILS
	INT iInteriorHandle = -1
	BOOL bLoadedInteriorInfo = FALSE
	sInteriorFile InteriorInfo
ENDSTRUCT


PROC PRINT_NODE(INT iNodeHandle)
	PRINTLN("[furniture] --------------- PRINT_NODE - ", iNodeHandle)
ENDPROC

PROC PRINT_RSREF_NODE(INT iRSNodeHandle)
	sInteriorRsRefNode RSNodeDetails
	sRsRefShopData ShopDetails
	IF LAYOUT_GET_RSREF_INFO(iRSNodeHandle,RSNodeDetails,ShopDetails)
		PRINTLN("[furniture] ------------ PRINT_RSREF_NODE NAMEHASH: ",RSNodeDetails.nameHash)
		PRINTLN("[furniture] ------------ PRINT_RSREF_NODE TRANSLATION: ",RSNodeDetails.translation)
		PRINTLN("[furniture] ------------ PRINT_RSREF_NODE ROTATION: ",RSNodeDetails.rotation)
		PRINTLN("[furniture] ------------ PRINT_RSREF_NODE layoutNodes: ",RSNodeDetails.numberOfLayoutNodes)	
		PRINTLN("[furniture] ------------ PRINT_RSREF_NODE Shop Name: ",ShopDetails.name)	
		PRINTLN("[furniture] ------------ PRINT_RSREF_NODE Shop Price: ",ShopDetails.price)	
		PRINTLN("[furniture] ------------ PRINT_RSREF_NODE Shop Description: ",ShopDetails.description)	
		INT i
		INT iNodeHandle
		REPEAT RSNodeDetails.numberOfLayoutNodes i 
			iNodeHandle = GET_LAYOUT_HANDLE_FROM_RSREF(iRSNodeHandle,i)	
			PRINT_NODE(iNodeHandle)
		ENDREPEAT
	ELSE
		PRINTLN("[furniture] ------------ PRINT_RSREF_NODE - LAYOUT_GET_RSREF_INFO = FALSE")	
	ENDIF
ENDPROC

PROC PRINT_GROUP(INT iGroupHandle)
	sInteriorGroupNode GroupDetails
	IF LAYOUT_GET_GROUP_INFO(iGroupHandle,GroupDetails)
		PRINTLN("[furniture] --------- PRINT_GROUP - No of RSRef nodes: ", GroupDetails.numberOfRsRefNodes)	
		INT i
		INT iRSNodeHandle
		REPEAT GroupDetails.numberOfRsRefNodes i
			iRSNodeHandle = GET_RSREF_HANDLE_FROM_GROUP(iGroupHandle,i)		
			PRINT_RSREF_NODE(iRSNodeHandle)
		ENDREPEAT		
	ELSE
		PRINTLN("[furniture] --------- PRINT_GROUP - LAYOUT_GET_GROUP_INFO = FALSE")
	ENDIF
ENDPROC

PROC PRINT_LAYOUT(INT iLayoutHandle)
	sInteriorLayoutNode LayoutDetails	
	IF LAYOUT_GET_LAYOUT_INFO(iLayoutHandle, LayoutDetails)
		PRINTLN("[furniture] ------ PRINT_LAYOUT - NAMEHASH: ", LayoutDetails.nameHash)
		PRINTLN("[furniture] ------ PRINT_LAYOUT - TRANSLATION: ", LayoutDetails.translation)
		PRINTLN("[furniture] ------ PRINT_LAYOUT - ROTATION: ", LayoutDetails.rotation)
		PRINTLN("[furniture] ------ PRINT_LAYOUT - PURCHASABLE: ", LayoutDetails.purchasable)
		PRINTLN("[furniture] ------ PRINT_LAYOUT - numberOfGroupNodes: ", LayoutDetails.numberOfGroupNodes)
		INT i
		INT iGroupHandle
		REPEAT LayoutDetails.numberOfGroupNodes i
			iGroupHandle = GET_GROUP_HANDLE_FROM_LAYOUT(iLayoutHandle,i)		
			PRINT_GROUP(iGroupHandle)
		ENDREPEAT
	ELSE
		PRINTLN("[furniture] ------ PRINT_LAYOUT - LAYOUT_GET_LAYOUT_INFO = FALSE")	
	ENDIF
ENDPROC

PROC PRINT_ROOM(INT iRoomHandle)
	sInteriorRoom RoomDetails	
	IF LAYOUT_GET_ROOM_INFO(iRoomHandle, RoomDetails)
		PRINTLN("[furniture] --- PRINT_ROOM - NAMEHASH: ",RoomDetails.nameHash)
		PRINTLN("[furniture] --- PRINT_ROOM - numberOfLayoutNodes: ",RoomDetails.numberOfLayoutNodes)
		INT i
		INT iLayoutNode		
		REPEAT RoomDetails.numberOfLayoutNodes i
			iLayoutNode = GET_LAYOUT_HANDLE_FROM_ROOM(iRoomHandle,i)			
			PRINT_LAYOUT(iLayoutNode)
		ENDREPEAT
	ELSE
		PRINTLN("[furniture] --- PRINT_ROOM - LAYOUT_GET_ROOM_INFO = FALSE")	
	ENDIF
ENDPROC

PROC PRINT_INTERIOR(FURNITURE_DETAILS &FurnitureDetails)
	INT i
	INT iRoomHandle
	PRINTLN("[furniture] PRINT_INTERIOR - NAMEHASH: ",FurnitureDetails.InteriorInfo.nameHash)
	PRINTLN("[furniture] PRINT_INTERIOR - No of room nodes: ",FurnitureDetails.InteriorInfo.numberOfRoomNodes)
	REPEAT FurnitureDetails.InteriorInfo.numberOfRoomNodes i
		iRoomHandle = GET_ROOM_HANDLE_FROM_INTERIOR(FurnitureDetails.iInteriorHandle, i)
		PRINT_ROOM(iRoomHandle)
	ENDREPEAT
ENDPROC

PROC PRINT_LAYOUT_INFO_FOR_INTERIOR(STRING strFileName, FURNITURE_DETAILS &FurnitureDetails)
	
	// get interior handle
	IF (FurnitureDetails.iInteriorHandle = -1)
		FurnitureDetails.iInteriorHandle = LAYOUT_LOAD_INTERIOR_INFO(strFileName)
		NET_PRINT("[furniture] PRINT_LAYOUT_INFO_FOR_INTERIOR - FurnitureDetails.iInteriorHandle = ") NET_PRINT_INT(FurnitureDetails.iInteriorHandle) NET_NL()
	ENDIF
	
	// load interior info
	IF NOT (FurnitureDetails.iInteriorHandle = -1)
	AND NOT (FurnitureDetails.bLoadedInteriorInfo)		
		IF LAYOUT_GET_INTERIOR_INFO(FurnitureDetails.iInteriorHandle, FurnitureDetails.InteriorInfo)
			FurnitureDetails.bLoadedInteriorInfo = TRUE
			PRINT_INTERIOR(FurnitureDetails)
			NET_PRINT("[furniture] PRINT_LAYOUT_INFO_FOR_INTERIOR - LAYOUT_GET_INTERIOR_INFO = TRUE ") NET_NL()		
		ELSE
			//NET_PRINT("[furniture] PRINT_LAYOUT_INFO_FOR_INTERIOR - waiting on LAYOUT_GET_INTERIOR_INFO... ") NET_NL()		
		ENDIF		
	ENDIF
	
	
ENDPROC


FUNC STRING GET_LAYOUT_FILE_FOR_PROPERTY(INT iProperty)
	SWITCH iProperty
		CASE 0 RETURN "int_mp_h_04"
	ENDSWITCH
	RETURN "int_mp_h_04"
ENDFUNC

PROC PRINT_LAYOUT_INFO_FOR_PROPERTY(INT iProperty, FURNITURE_DETAILS &FurnitureDetails)
	PRINT_LAYOUT_INFO_FOR_INTERIOR(GET_LAYOUT_FILE_FOR_PROPERTY(iProperty), FurnitureDetails)
ENDPROC

PROC RESET_FURNITURE_DETAILS(FURNITURE_DETAILS &FurnitureDetails)
	FURNITURE_DETAILS EmptyDetails
	FurnitureDetails = EmptyDetails
ENDPROC

#ENDIF


