//╔═════════════════════════════════════════════════════════════════════════════╗
//║		net_heli_sctv.sch														║
//║																				║
//║		A helicopter that follow racers, SCTV players can spectate from the 	║
//║		heli perspective (1217660)												║
//║																				║
//║		C Speirs, February 2014													║
//╚═════════════════════════════════════════════════════════════════════════════╝	

USING "globals.sch"
USING "net_include.sch"
USING "net_scoring_common.sch"
USING "net_mission.sch"

CONST_INT ciBIT_QUIT_ON 	0
CONST_INT ciBIT_QUIT_SOUND 	1
CONST_INT ciBIT_HAS_QUIT 	2

#IF IS_DEBUG_BUILD 
USING "net_debug.sch"

FUNC BOOL SHOULD_SCTV_DEBUG_START()
	RETURN g_bLaunchSCTVChopper
ENDFUNC
#ENDIF

STRUCT HELI_SCTV_STRUCT
	MODEL_NAMES mnHeli = FROGGER
	MODEL_NAMES mnPilot = A_M_Y_RoadCyc_01
	NETWORK_INDEX niHeli
	NETWORK_INDEX niPilot
	PED_INDEX pedStored
	INT iHeliProgress
	INT iHeliCamProgress
	PLAYER_INDEX playerToSpectate
	SCRIPT_TIMER stHeliTask
ENDSTRUCT

STRUCT HELI_SCTV_STRUCT_CLIENT
	CAMERA_INDEX camHeli
	ENTITY_INDEX eHeli
	VECTOR vPosFollow
//	VECTOR vPosStored
	INT iHeliCamProgress
	SCALEFORM_INDEX scaleformHelp
	SCALEFORM_INSTRUCTIONAL_BUTTONS scaleformStruct
	INT iButtonProgress
	INT iQuitProgress
	INT iBitSet
ENDSTRUCT

FUNC VECTOR GET_SCTV_CHOPPER_COORDS(VECTOR vStartPosOfTargetPed)
//	VECTOR vChopperPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[0].vPos 
	VECTOR vReturn
	
//	IF bFirstInit

//	ELSE
		vReturn = vStartPosOfTargetPed
		vReturn.z += 100.0
//	ENDIF

	RETURN vReturn
ENDFUNC

FUNC FLOAT GET_SCTV_CHOPPER_HEADING()
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[0].fHead
ENDFUNC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				STAGE FUNCTIONS													║

// SERVER
//═════════════════════════════════════════════════════════════════════════════
CONST_INT SCTV_SERVER_HELI_STAGE_INIT 		0
CONST_INT SCTV_SERVER_HELI_STAGE_CREATE 	1
CONST_INT SCTV_SERVER_HELI_STAGE_FLY 		2

FUNC INT GET_SCTV_SERVER_HELI_STAGE(HELI_SCTV_STRUCT& heliStruct)

	RETURN heliStruct.iHeliProgress
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_CURRENT_SCTV_HELI_STAGE(HELI_SCTV_STRUCT& heliStruct)
	SWITCH heliStruct.iHeliProgress
		CASE SCTV_SERVER_HELI_STAGE_INIT 	PRINTLN("[CS_HELI_SPEC]SCTV_SERVER_HELI_STAGE_INIT") BREAK
		CASE SCTV_SERVER_HELI_STAGE_CREATE PRINTLN("[CS_HELI_SPEC]SCTV_SERVER_HELI_STAGE_CREATE") BREAK	
		CASE SCTV_SERVER_HELI_STAGE_FLY  	PRINTLN("[CS_HELI_SPEC]SCTV_SERVER_HELI_STAGE_FLY") BREAK
	ENDSWITCH
ENDPROC

PROC PRINT_GOTO_SCTV_HELI_STAGE(INT iStageGoTo)
	SWITCH iStageGoTo
		CASE SCTV_SERVER_HELI_STAGE_INIT 	PRINTLN("[CS_HELI_SPEC]GOTO_SCTV_HELI_STAGE >>> SCTV_SERVER_HELI_STAGE_INIT") BREAK
		CASE SCTV_SERVER_HELI_STAGE_CREATE PRINTLN("[CS_HELI_SPEC]GOTO_SCTV_HELI_STAGE >>> SCTV_SERVER_HELI_STAGE_CREATE") BREAK	
		CASE SCTV_SERVER_HELI_STAGE_FLY  	PRINTLN("[CS_HELI_SPEC]GOTO_SCTV_HELI_STAGE >>> SCTV_SERVER_HELI_STAGE_FLY") BREAK
	ENDSWITCH
ENDPROC

#ENDIF

PROC GOTO_SCTV_HELI_STAGE(HELI_SCTV_STRUCT& heliStruct, INT iStageGoTo)

	#IF IS_DEBUG_BUILD
		PRINT_CURRENT_SCTV_HELI_STAGE(heliStruct)
		PRINT_GOTO_SCTV_HELI_STAGE(iStageGoTo)
	#ENDIF

	heliStruct.iHeliProgress = iStageGoTo
ENDPROC

// CLIENT
//═════════════════════════════════════════════════════════════════════════════
CONST_INT SCTV_CLIENT_STAGE_INIT 	0
CONST_INT SCTV_CLIENT_STAGE_CAMS 	1
CONST_INT SCTV_CLIENT_STAGE_POINT 	2

#IF IS_DEBUG_BUILD
PROC PRINT_CURRENT_CLIENT_SCTV_HELI_STAGE(HELI_SCTV_STRUCT_CLIENT& heliStructClient)
	SWITCH heliStructClient.iHeliCamProgress
		CASE SCTV_CLIENT_STAGE_INIT 	PRINTLN("[CS_HELI_SPEC]SCTV_CLIENT_STAGE_INIT") BREAK
		CASE SCTV_CLIENT_STAGE_CAMS 	PRINTLN("[CS_HELI_SPEC]SCTV_CLIENT_STAGE_CAMS") BREAK	
		CASE SCTV_CLIENT_STAGE_POINT  	PRINTLN("[CS_HELI_SPEC]SCTV_CLIENT_STAGE_POINT") BREAK
	ENDSWITCH
ENDPROC

PROC PRINT_GOTO_SCTV_CLIENT_HELI_STAGE(INT iStageGoTo)
	SWITCH iStageGoTo
		CASE SCTV_CLIENT_STAGE_INIT 	PRINTLN("[CS_HELI_SPEC]PRINT_GOTO_SCTV_CLIENT_HELI_STAGE >>> SCTV_CLIENT_STAGE_INIT") BREAK
		CASE SCTV_CLIENT_STAGE_CAMS 	PRINTLN("[CS_HELI_SPEC]PRINT_GOTO_SCTV_CLIENT_HELI_STAGE >>> SCTV_CLIENT_STAGE_CAMS") BREAK	
		CASE SCTV_CLIENT_STAGE_POINT  	PRINTLN("[CS_HELI_SPEC]PRINT_GOTO_SCTV_CLIENT_HELI_STAGE >>> SCTV_CLIENT_STAGE_POINT") BREAK
	ENDSWITCH
ENDPROC
#ENDIF

FUNC INT GET_SCTV_CLIENT_HELI_STAGE(HELI_SCTV_STRUCT_CLIENT& heliStructClient)

	RETURN heliStructClient.iHeliCamProgress
ENDFUNC

PROC GOTO_CLIENT_SCTV_HELI_STAGE(HELI_SCTV_STRUCT_CLIENT& heliStructClient, INT iStageGoTo)
	#IF IS_DEBUG_BUILD
		PRINT_CURRENT_CLIENT_SCTV_HELI_STAGE(heliStructClient)
		PRINT_GOTO_SCTV_CLIENT_HELI_STAGE(iStageGoTo)
	#ENDIF

	heliStructClient.iHeliCamProgress = iStageGoTo
ENDPROC

PROC DO_QUIT_HELI_BUTTONS(HELI_SCTV_STRUCT_CLIENT& heliStructClient)
	SPRITE_PLACEMENT aSprite 
	aSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	IF LOAD_MENU_ASSETS()
		SWITCH heliStructClient.iButtonProgress
			CASE 0
				REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(heliStructClient.scaleformStruct)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HUD_QUIT", heliStructClient.scaleformStruct, TRUE)	// Quit CIRCLE
				
				heliStructClient.iButtonProgress++
			BREAK
			CASE 1
				RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(heliStructClient.scaleformHelp, aSprite, heliStructClient.scaleformStruct)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL IS_HELI_QUIT_SCREEN_ON(HELI_SCTV_STRUCT_CLIENT& heliStructClient)
	RETURN IS_BIT_SET(heliStructClient.iBitSet, ciBIT_QUIT_ON)
ENDFUNC

PROC HELI_QUIT_SCREEN_TOGGLE_ON(HELI_SCTV_STRUCT_CLIENT& heliStructClient)

	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF

	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)	
		IF NOT IS_BIT_SET(heliStructClient.iBitSet, ciBIT_QUIT_ON)
			PRINTLN(" [CS_HELI_SPEC] HELI_QUIT_SCREEN_TOGGLE_ON - TRUE  ")
			SET_BIT(heliStructClient.iBitSet, ciBIT_QUIT_ON)
		ENDIF
		IF NOT IS_BIT_SET(heliStructClient.iBitSet, ciBIT_QUIT_SOUND)
			PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			SET_BIT(heliStructClient.iBitSet, ciBIT_QUIT_SOUND)
		ENDIF
	ENDIF
ENDPROC

PROC HELI_QUIT_SCREEN_TOGGLE_OFF(HELI_SCTV_STRUCT_CLIENT& heliStructClient)

	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF

	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF IS_BIT_SET(heliStructClient.iBitSet, ciBIT_QUIT_ON)
			PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			PRINTLN(" [CS_HELI_SPEC] HELI_QUIT_SCREEN_TOGGLE_OFF - FALSE  ")
			CLEAR_BIT(heliStructClient.iBitSet, ciBIT_QUIT_ON)
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_QUIT_HELI_SPECTATE(HELI_SCTV_STRUCT_CLIENT& heliStructClient)

	IF NOT IS_BIT_SET(heliStructClient.iBitSet, ciBIT_HAS_QUIT)
		SWITCH heliStructClient.iQuitProgress
			CASE 0
				IF NOT IS_PAUSE_MENU_ACTIVE()
					IF IS_HELI_QUIT_SCREEN_ON(heliStructClient)	
					
						HELI_QUIT_SCREEN_TOGGLE_OFF(heliStructClient)

						// "Are you sure you want to quit the voting screen?"
						SET_WARNING_MESSAGE_WITH_HEADER("SPEC_LEAVE", "SPEC_SURE", (FE_WARNING_YES | FE_WARNING_NO))

						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							PLAY_SOUND_FRONTEND(-1, "EXIT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							
							heliStructClient.iQuitProgress++
						ENDIF
					ELSE
						// Listen out for quit screen toggle
						HELI_QUIT_SCREEN_TOGGLE_ON(heliStructClient)
					ENDIF
				ENDIF
			BREAK
			CASE 1
				
				SET_BIT(heliStructClient.iBitSet, ciBIT_HAS_QUIT)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_QUIT_HELI_SPECTATE(HELI_SCTV_STRUCT_CLIENT& heliStructClient)
	RETURN IS_BIT_SET(heliStructClient.iBitSet, ciBIT_HAS_QUIT)
ENDFUNC

PROC HIDE_HUD_DURING_HELI_SPECTATE()
	DISABLE_INTERACTION_MENU()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	//THEFEED_HIDE_THIS_FRAME()
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
ENDPROC

//╚═════════════════════════════════════════════════════════════════════════════╝

PROC SET_SCTV_HELI_GLOBAL(BOOL bFlag)
	IF bFlag
		IF bSCTVHeliActive = FALSE
			bSCTVHeliActive = TRUE
			PRINTLN("[CS_HELI_SPEC] bSCTVHeliActive, TRUE ")
		ENDIF
	ELSE
		IF bSCTVHeliActive = TRUE
			bSCTVHeliActive = FALSE
			PRINTLN("[CS_HELI_SPEC] bSCTVHeliActive, FALSE ")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_HELI_AND_PILOT_NET_IDS_EXIST(HELI_SCTV_STRUCT& heliStruct)
	IF NETWORK_DOES_NETWORK_ID_EXIST(heliStruct.niHeli)
	AND NETWORK_DOES_NETWORK_ID_EXIST(heliStruct.niPilot)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_SCTV_HELI_CLEANUP(HELI_SCTV_STRUCT& heliStruct, BOOL bIsClient, BOOL bCleanupHeli = TRUE, BOOL bCleanupNow = TRUE)

	IF bIsClient
		PRINTLN("[CS_HELI_SPEC] DO_SCTV_HELI_CLEANUP, RENDER_SCRIPT_CAMS")
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	
	ENTITY_INDEX eiHeli
	ENTITY_INDEX eiPilot

	IF bCleanupHeli
	
		IF NETWORK_DOES_NETWORK_ID_EXIST(heliStruct.niHeli)
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(heliStruct.niHeli)
		
			eiHeli = NET_TO_ENT(heliStruct.niHeli)
			
			IF bCleanupNow
				DELETE_NET_ID(heliStruct.niHeli)
				PRINTLN("[CS_HELI_SPEC] DO_SCTV_HELI_CLEANUP, DELETE_NET_ID, niHeli")
			ELSE
				SET_ENTITY_AS_NO_LONGER_NEEDED(eiHeli)
				PRINTLN("[CS_HELI_SPEC] DO_SCTV_HELI_CLEANUP, SET_ENTITY_AS_NO_LONGER_NEEDED, niHeli")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(heliStruct.niPilot)
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(heliStruct.niPilot)
		
			eiPilot = NET_TO_ENT(heliStruct.niPilot)
			
			IF bCleanupNow
				DELETE_NET_ID(heliStruct.niPilot)
				PRINTLN("[CS_HELI_SPEC] DO_SCTV_HELI_CLEANUP, DELETE_NET_ID, niPilot")
			ELSE
				SET_ENTITY_AS_NO_LONGER_NEEDED(eiPilot)
				PRINTLN("[CS_HELI_SPEC] DO_SCTV_HELI_CLEANUP, SET_ENTITY_AS_NO_LONGER_NEEDED, niHeli")
			ENDIF
		ENDIF
	ENDIF
	CLEANUP_MENU_ASSETS()
	SET_SCTV_HELI_GLOBAL(FALSE)
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				HELI															║

FUNC BOOL CREATE_HELI_FOR_RACE(HELI_SCTV_STRUCT& heliStruct, VECTOR vHeliPos, FLOAT fHeliHeading)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(heliStruct.niHeli)
	
		RETURN TRUE
	ENDIF
	
	BOOL bScriptHostVeh 			= TRUE
	BOOL bRegisterAsNetworkObject 	= TRUE
	BOOL bSetExistOnAllMachines 	= TRUE 
	BOOL bSetAsStolen				= FALSE 
	BOOL bFreezeWaitingOnCollision 	= TRUE

	REQUEST_MODEL(heliStruct.mnHeli)
	IF HAS_MODEL_LOADED(heliStruct.mnHeli)
		IF NOT IS_VECTOR_ZERO(vHeliPos)
			IF CREATE_NET_VEHICLE(heliStruct.niHeli, heliStruct.mnHeli, vHeliPos, fHeliHeading, bScriptHostVeh, bRegisterAsNetworkObject, bSetExistOnAllMachines, bSetAsStolen, bFreezeWaitingOnCollision)

				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_PED_IN_HELI(HELI_SCTV_STRUCT& heliStruct)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(heliStruct.niPilot)
	
		RETURN TRUE
	ENDIF

	REQUEST_MODEL(heliStruct.mnPilot)
	ENTITY_INDEX eiHeli
	IF HAS_MODEL_LOADED(heliStruct.mnPilot)	
		IF IS_NET_VEHICLE_DRIVEABLE(heliStruct.niHeli)
			eiHeli = NET_TO_ENT(heliStruct.niHeli)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(eiHeli)
				IF CREATE_NET_PED_IN_VEHICLE(heliStruct.niPilot, heliStruct.niHeli, PEDTYPE_CIVMALE, heliStruct.mnPilot, VS_DRIVER)
				
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("[CS_HELI_SPEC] CREATE_PED_IN_HELI, NETWORK_REQUEST_CONTROL_OF_ENTITY ")
				NETWORK_REQUEST_CONTROL_OF_ENTITY(eiHeli)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCTV_HELI_PREP(HELI_SCTV_STRUCT& heliStruct)
	// Heli
	ENTITY_INDEX eiHeli = NET_TO_ENT(heliStruct.niHeli)
	SET_ENTITY_PROOFS(eiHeli, TRUE, TRUE, TRUE, TRUE, FALSE)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(eiHeli, FALSE)
	
	VEHICLE_INDEX vehHeli = NET_TO_VEH(heliStruct.niHeli)
	SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(vehHeli, FALSE) 
	SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(vehHeli, FALSE)
	
	// Pilot
	ENTITY_INDEX eiPilot = NET_TO_ENT(heliStruct.niPilot)
	SET_ENTITY_INVINCIBLE(eiPilot, TRUE)
	
	// Stop it falling out the sky
	SET_VEHICLE_ENGINE_ON(vehHeli, TRUE, TRUE)
	SET_HELI_TURBULENCE_SCALAR(vehHeli, 0.3)
	SET_HELI_BLADES_FULL_SPEED(vehHeli)
	ACTIVATE_PHYSICS(vehHeli)
	
	SET_SCTV_HELI_GLOBAL(TRUE)
	PRINTLN("[CS_HELI_SPEC] SCTV_HELI_PREP")
ENDPROC

CONST_FLOAT cfHELI_CRUISE_SPEED 			60.00
CONST_FLOAT cfHELI_CUSTOM_OFFSET			25.00
CONST_INT 	ciHELI_MIN_HEIGHT_ABOVE_TERRAIN	35

CONST_INT ciHELI_TIMER_TASK 5000

PROC TASK_FLY_SCTV_CHOPPER(HELI_SCTV_STRUCT& heliStruct, PED_INDEX pedPilot, VEHICLE_INDEX vehChopper, PED_INDEX pedFollow, FLOAT fSpeed = cfHELI_CRUISE_SPEED, FLOAT fOffset = cfHELI_CUSTOM_OFFSET, INT iMinhight = ciHELI_MIN_HEIGHT_ABOVE_TERRAIN, HELIMODE eHelimode = HF_ForceHeightMapAvoidance)
	
	BOOL bTimerExpired
	IF NOT HAS_NET_TIMER_STARTED(heliStruct.stHeliTask)
		START_NET_TIMER(heliStruct.stHeliTask)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(heliStruct.stHeliTask, ciHELI_TIMER_TASK)
			RESET_NET_TIMER(heliStruct.stHeliTask)
			PRINTLN("[CS_HELI_SPEC] TASK_FLY_SCTV_CHOPPER, RESET_NET_TIMER ")
			bTimerExpired = TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedPilot) AND NOT IS_ENTITY_DEAD(vehChopper) AND NOT IS_PED_INJURED(pedFollow)
		IF GET_SCRIPT_TASK_STATUS(pedPilot, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(pedPilot, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
		OR pedFollow <> heliStruct.pedStored // If the player to follow changes
		OR bTimerExpired
			IF pedFollow <> heliStruct.pedStored
				heliStruct.pedStored = pedFollow
				PRINTLN("[CS_HELI_SPEC] heliStruct.pedStored = pedFollow - calling CLEAR_PED_TASKS(pedPilot)")
				CLEAR_PED_TASKS(pedPilot)
			ENDIF
			TASK_VEHICLE_HELI_PROTECT(pedPilot, vehChopper, pedFollow, fSpeed, DF_SteerAroundObjects, fOffset, 
										iMinhight, eHeliMode)
			PRINTLN("[CS_HELI_SPEC] TASK_FLY_SCTV_CHOPPER ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(heliStruct.pedStored)))
		ENDIF
	ENDIF
ENDPROC

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				LOGIC															║

// The SCTV player will run this
PROC RUN_HELI_CAM_LOGIC_CLIENT(HELI_SCTV_STRUCT& heliStruct, HELI_SCTV_STRUCT_CLIENT& heliStructClient, ENTITY_INDEX eHeli, VECTOR vPosFollow, BOOL bNoButtonPress = FALSE, 
								BOOL bShowButtons = FALSE)

	SWITCH GET_SCTV_CLIENT_HELI_STAGE(heliStructClient)
	
		CASE SCTV_CLIENT_STAGE_INIT
			IF HAS_SCTV_HELI_BUTTON_BEEN_PRESSED()	
			OR bNoButtonPress
			
				GOTO_CLIENT_SCTV_HELI_STAGE(heliStructClient, SCTV_CLIENT_STAGE_CAMS)
			ENDIF
		BREAK
		
		CASE SCTV_CLIENT_STAGE_CAMS
			IF NOT DOES_CAM_EXIST(heliStructClient.camHeli)
				heliStructClient.camHeli = CREATE_CAM("default_scripted_camera", FALSE)
				PRINTLN("[CS_HELI_SPEC] RUN_HELI_CAM_LOGIC_CLIENT, CREATE_CAM ")
			ELSE
				// Ignore interiors see (1816218)
				CLEAR_ROOM_FOR_GAME_VIEWPORT()
				// Attach cam to heli and switch on
				ATTACH_CAM_TO_ENTITY(heliStructClient.camHeli, eHeli, <<0.0, 2.75, -1.25>> , TRUE) // <<0.0, 1.958, -0.468>>
				SET_CAM_ACTIVE(heliStructClient.camHeli, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_SCTV_HELI_GLOBAL(TRUE)

				GOTO_CLIENT_SCTV_HELI_STAGE(heliStructClient, SCTV_CLIENT_STAGE_POINT)
			ENDIF
		BREAK
		
		CASE SCTV_CLIENT_STAGE_POINT
			
			IF DOES_CAM_EXIST(heliStructClient.camHeli)
			
				IF NOT IS_CAM_ACTIVE(heliStructClient.camHeli)
					ATTACH_CAM_TO_ENTITY(heliStructClient.camHeli, eHeli, <<0.0, 2.75, -1.25>> , TRUE)
					SET_CAM_ACTIVE(heliStructClient.camHeli, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					PRINTLN("[CS_HELI_SPEC] RUN_HELI_CAM_LOGIC_CLIENT, RENDER_SCRIPT_CAMS ", vPosFollow)
				ELSE
					POINT_CAM_AT_COORD(heliStructClient.camHeli, vPosFollow)
				ENDIF
			
				IF bShowButtons
					DO_QUIT_HELI_BUTTONS(heliStructClient)
				ENDIF
				
				IF HAS_SCTV_HELI_BUTTON_BEEN_PRESSED()
				AND NOT bNoButtonPress
				
					DO_SCTV_HELI_CLEANUP(heliStruct, TRUE)
					
					GOTO_CLIENT_SCTV_HELI_STAGE(heliStructClient, SCTV_CLIENT_STAGE_INIT)
				ENDIF
			ELSE
				PRINTLN("[CS_HELI_SPEC] RUN_HELI_CAM_LOGIC_CLIENT, SCTV_CLIENT_STAGE_POINT, DOES_CAM_EXIST ")
				GOTO_CLIENT_SCTV_HELI_STAGE(heliStructClient, SCTV_CLIENT_STAGE_INIT)
			ENDIF
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				PRINTLN("[CS_HELI_SPEC] RUN_HELI_CAM_LOGIC_CLIENT, DEFAULT ")
				SCRIPT_ASSERT("[CS_HELI_SPEC] RUN_HELI_CAM_LOGIC_CLIENT, DEFAULT bug Speirs")
			#ENDIF
		BREAK
	
	ENDSWITCH
ENDPROC

// Script host will run this
// Create the SCTV heli, the ped and also runs the flying task
PROC RUN_SCTV_HELI_LOGIC_SERVER(HELI_SCTV_STRUCT& heliStruct, PED_INDEX pedToFollow, VECTOR vStartPosOfTargetPed, BOOL bFly, FLOAT fSpeed = cfHELI_CRUISE_SPEED, FLOAT fOffset = cfHELI_CUSTOM_OFFSET, INT iMinhight = ciHELI_MIN_HEIGHT_ABOVE_TERRAIN, HELIMODE eHeliMode = HF_ForceHeightMapAvoidance)
	PED_INDEX pedPilot
	VEHICLE_INDEX vehChopper
	SWITCH GET_SCTV_SERVER_HELI_STAGE(heliStruct)

		CASE SCTV_SERVER_HELI_STAGE_INIT
			IF GET_NUM_RESERVED_MISSION_PEDS() = 0
				PRINTLN("[CS_HELI_SPEC] RUN_SCTV_HELI_LOGIC_SERVER, RESERVE_NETWORK_MISSION_PEDS ")
				RESERVE_NETWORK_MISSION_PEDS(1) 
			ENDIF

			GOTO_SCTV_HELI_STAGE(heliStruct, SCTV_SERVER_HELI_STAGE_CREATE)		
		BREAK
		
		CASE SCTV_SERVER_HELI_STAGE_CREATE
			IF NOT IS_VECTOR_ZERO(vStartPosOfTargetPed)
				IF CREATE_HELI_FOR_RACE(heliStruct, GET_SCTV_CHOPPER_COORDS(vStartPosOfTargetPed), GET_SCTV_CHOPPER_HEADING())
					IF CREATE_PED_IN_HELI(heliStruct)
						SCTV_HELI_PREP(heliStruct)
						
						GOTO_SCTV_HELI_STAGE(heliStruct, SCTV_SERVER_HELI_STAGE_FLY)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SCTV_SERVER_HELI_STAGE_FLY
			IF bFly
				IF DO_HELI_AND_PILOT_NET_IDS_EXIST(heliStruct)
					pedPilot 	= NET_TO_PED(heliStruct.niPilot)
					vehChopper 	= NET_TO_VEH(heliStruct.niHeli)
					TASK_FLY_SCTV_CHOPPER(heliStruct, pedPilot, vehChopper, pedToFollow, fSpeed, fOffset, iMinhight, eHeliMode)
//					PRINTLN("[CS_HELI_SPEC] SCTV_SERVER_HELI_STAGE_FLY, OK ")
				ELSE
//					PRINTLN("[CS_HELI_SPEC] DO_HELI_AND_PILOT_NET_IDS_EXIST ")
				ENDIF
			ENDIF
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				PRINTLN("[CS_HELI_SPEC] RUN_SCTV_HELI_LOGIC_SERVER, DEFAULT ")
				SCRIPT_ASSERT("[CS_HELI_SPEC] RUN_SCTV_HELI_LOGIC_SERVER, DEFAULT bug Speirs")
			#ENDIF
		BREAK
	
	ENDSWITCH
ENDPROC

//╚═════════════════════════════════════════════════════════════════════════════╝





