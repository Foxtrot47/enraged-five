
USING "rage_builtins.sch" 
USING "globals.sch"
USING "net_prints.sch"
USING "net_stat_system.sch"

#IF IS_DEBUG_BUILD

PROC OUTPUT_ALL_DUPE_DETECT_VALUES()
	PRINTLN("[platedupes] ============= DUPE DETECTOR ALL VALUES ======= [START] ===============")
	
	INT i
	
	REPEAT TOTAL_STORED_STATS_PLATES_PERSONALISED i 
		IF NOT (g_iPersonalisedCarPlateStats[i] = 0)
			PRINTLN("[platedupes] g_iPersonalisedCarPlateStats[", i, "] = ", g_iPersonalisedCarPlateStats[i])
		ENDIF
	ENDREPEAT
	
	REPEAT TOTAL_STORED_GLOBAL_PLATES_PERSONALISED i
		IF NOT (g_iPersonalisedCarPlateGlobals[i] = 0)
			PRINTLN("[platedupes] g_iPersonalisedCarPlateGlobals[", i, "] = ", g_iPersonalisedCarPlateGlobals[i])
		ENDIF
	ENDREPEAT
	
	REPEAT TOTAL_STORED_STATS_PLATES_SOLD i
		IF NOT (g_iSoldCarPlateStats[i] = 0)
			PRINTLN("[platedupes] g_iSoldCarPlateStats[", i, "] = ", g_iSoldCarPlateStats[i])
		ENDIF
	ENDREPEAt
	
	REPEAT TOTAL_STORED_GLOBAL_PLATES_PERSONALISED i
		IF NOT (g_iSoldCarPlateGlobals[i] = 0)
			PRINTLN("[platedupes] g_iSoldCarPlateGlobals[", i, "] = ", g_iSoldCarPlateGlobals[i])
		ENDIF
	ENDREPEAT
	
	REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i 
		IF NOT (g_iKnownDupePlateStats[i] = 0)
			PRINTLN("[platedupes] g_iKnownDupePlateStats[", i, "] = ", g_iKnownDupePlateStats[i], " g_iKnownDupeSoldStats[", i, "] = ", g_iKnownDupeSoldStats[i])
		ENDIF
	ENDREPEAT
	
	REPEAT TOTAL_STORED_GLOBAL_PLATES_KNOWN_DUPES i
		IF NOT (g_iKnownDupePlateGlobals[i] = 0)
			PRINTLN("[platedupes] g_iKnownDupePlateGlobals[", i, "] = ", g_iKnownDupePlateGlobals[i], " g_iKnownDupeSoldGlobals[", i, "] = ", g_iKnownDupeSoldGlobals[i])
		ENDIF
	ENDREPEAT
	
	PRINTLN("[platedupes] ============= DUPE DETECTOR ALL VALUES ======= [END] ===============")
	
ENDPROC

#ENDIF


// ====== list handling functions ======
PROC ShiftGlobalArrayListUp(INT &iArray[], INT iIndex=0)
	IF (iIndex < (COUNT_OF(iArray)-1))
		iArray[iIndex] = iArray[iIndex+1]
		ShiftGlobalArrayListUp(iArray, iIndex + 1)
	ENDIF
ENDPROC
PROC ShiftGlobalArrayListDown(INT &iArray[], INT iIndex=0)
	IF (iIndex > 0)
		iArray[iIndex] = iArray[iIndex-1]
		ShiftGlobalArrayListDown(iArray, iIndex - 1)
	ELSE 
		IF (iIndex = 0)
			iArray[0] = 0 // becomes a free space
		ENDIF
	ENDIF
ENDPROC
FUNC BOOL IsInValueInGlobalArray(INT &iArray[], INT iValue)
	INT i
	REPEAT COUNT_OF(iArray) i
		IF (iArray[i] = iValue)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC
FUNC INT GetIndexOfValueInGlobalArray(INT &iArray[], INT iValue)
	INT i
	REPEAT COUNT_OF(iArray) i
		IF (iArray[i] = iValue)
			RETURN i
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC
FUNC INT GetNumberOfEntriesOfValueInGlobalArray(INT &iArray[], INT iValue)
	INT iCount = 0
	INT i
	REPEAT COUNT_OF(iArray) i
		IF (iArray[i] = iValue)
			iCount++
		ENDIF
	ENDREPEAT
	RETURN iCount	
ENDFUNC

PROC AddValueToGlobalArray(INT &iArray[], INT iValue)
	IF NOT IsInValueInGlobalArray(iArray, iValue)
		ShiftGlobalArrayListUp(iArray)	
		iArray[COUNT_OF(iArray)-1] = iValue
	ENDIF
ENDPROC

PROC DeleteSlotFromGlobalArray(INT &iArray[], INT iSlot)
	IF (iSlot > -1)
		ShiftGlobalArrayListDown(iArray, iSlot)
	ENDIF	
ENDPROC



// ====== KNOWN DUPES ======
FUNC INT GetKnownDupePlateStatValue(INT iIndex)
	SWITCH iIndex
		CASE 0 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA0) // g_iStat_KnownDupePlate0
		CASE 1 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA1) // g_iStat_KnownDupePlate1
		CASE 2 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA2) // g_iStat_KnownDupePlate2
		CASE 3 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA3) // g_iStat_KnownDupePlate3
		CASE 4 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA4) // g_iStat_KnownDupePlate4
		CASE 5 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA5) // g_iStat_KnownDupePlate5
		CASE 6 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA6) // g_iStat_KnownDupePlate6
		CASE 7 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA7) // g_iStat_KnownDupePlate7
		CASE 8 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA8) // g_iStat_KnownDupePlate8
		CASE 9 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA9) // g_iStat_KnownDupePlate9
	ENDSWITCH
	RETURN 0
ENDFUNC

PROC SetKnownDupePlateStatValue(INT iIndex, INT iValue)
	SWITCH iIndex
		CASE 0 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA0, iValue) BREAK // g_iStat_KnownDupePlate0 = iValue BREAK
		CASE 1 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA1, iValue) BREAK // g_iStat_KnownDupePlate1 = iValue BREAK
		CASE 2 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA2, iValue) BREAK // g_iStat_KnownDupePlate2 = iValue BREAK
		CASE 3 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA3, iValue) BREAK // g_iStat_KnownDupePlate3 = iValue BREAK
		CASE 4 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA4, iValue) BREAK // g_iStat_KnownDupePlate4 = iValue BREAK
		CASE 5 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA5, iValue) BREAK // g_iStat_KnownDupePlate5 = iValue BREAK
		CASE 6 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA6, iValue) BREAK // g_iStat_KnownDupePlate6 = iValue BREAK
		CASE 7 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA7, iValue) BREAK // g_iStat_KnownDupePlate7 = iValue BREAK
		CASE 8 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA8, iValue) BREAK // g_iStat_KnownDupePlate8 = iValue BREAK
		CASE 9 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERPUMA9, iValue) BREAK // g_iStat_KnownDupePlate9 = iValue BREAK
	ENDSWITCH
	g_iKnownDupePlateStats[iIndex] = iValue
ENDPROC

FUNC INT GetKnownDupeSoldStatValue(INT iIndex)
	SWITCH iIndex
		CASE 0 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD0) // g_iStat_KnownDupeSold0
		CASE 1 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD1) // g_iStat_KnownDupeSold1
		CASE 2 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD2) // g_iStat_KnownDupeSold2
		CASE 3 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD3) // g_iStat_KnownDupeSold3
		CASE 4 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD4) // g_iStat_KnownDupeSold4
		CASE 5 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD5) // g_iStat_KnownDupeSold5
		CASE 6 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD6) // g_iStat_KnownDupeSold6
		CASE 7 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD7) // g_iStat_KnownDupeSold7
		CASE 8 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD8) // g_iStat_KnownDupeSold8
		CASE 9 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD9) // g_iStat_KnownDupeSold9
	ENDSWITCH
	RETURN 0
ENDFUNC

PROC SetKnownDupeSoldStatValue(INT iIndex, INT iValue)
	SWITCH iIndex
		CASE 0 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD0, iValue) BREAK // g_iStat_KnownDupeSold0 = iValue BREAK
		CASE 1 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD1, iValue) BREAK // g_iStat_KnownDupeSold1 = iValue BREAK
		CASE 2 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD2, iValue) BREAK // g_iStat_KnownDupeSold2 = iValue BREAK
		CASE 3 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD3, iValue) BREAK // g_iStat_KnownDupeSold3 = iValue BREAK
		CASE 4 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD4, iValue) BREAK // g_iStat_KnownDupeSold4 = iValue BREAK
		CASE 5 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD5, iValue) BREAK // g_iStat_KnownDupeSold5 = iValue BREAK
		CASE 6 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD6, iValue) BREAK // g_iStat_KnownDupeSold6 = iValue BREAK
		CASE 7 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD7, iValue) BREAK // g_iStat_KnownDupeSold7 = iValue BREAK
		CASE 8 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD8, iValue) BREAK // g_iStat_KnownDupeSold8 = iValue BREAK
		CASE 9 SET_MP_INT_PLAYER_STAT(MPPLY_KNOWNDOZERSOLD9, iValue) BREAK // g_iStat_KnownDupeSold9 = iValue BREAK
	ENDSWITCH
	g_iKnownDupeSoldStats[iIndex] = iValue
ENDPROC

PROC ShiftKnownDupePlateListUp(INT iIndex=0)
	IF (iIndex < TOTAL_STORED_STATS_KNOWN_DUPES)
		SetKnownDupePlateStatValue(iIndex, GetKnownDupePlateStatValue(iIndex+1))
		ShiftKnownDupePlateListUp(iIndex + 1)
	ENDIF
ENDPROC
PROC ShiftKnownDupeSoldListUp(INT iIndex=0)
	IF (iIndex < TOTAL_STORED_STATS_KNOWN_DUPES)
		SetKnownDupeSoldStatValue(iIndex, GetKnownDupeSoldStatValue(iIndex+1))
		ShiftKnownDupeSoldListUp(iIndex + 1)
	ENDIF
ENDPROC
PROC ShiftKnownDupePlateListDown(INT iIndex=0)
	IF (iIndex > 0)
		SetKnownDupePlateStatValue(iIndex, GetKnownDupePlateStatValue(iIndex-1))
		ShiftKnownDupePlateListDown(iIndex - 1)
	ELSE
		IF (iIndex = 0)
			SetKnownDupePlateStatValue(0, 0) // becomes a free space
		ENDIF	
	ENDIF
ENDPROC
PROC ShiftKnownDupeSoldListDown(INT iIndex=0)
	IF (iIndex > 0)
		SetKnownDupeSoldStatValue(iIndex, GetKnownDupeSoldStatValue(iIndex-1))
		ShiftKnownDupeSoldListDown(iIndex - 1)
	ELSE
		IF (iIndex = 0)
			SetKnownDupeSoldStatValue(0, 0) // becomes a free space
		ENDIF
	ENDIF
ENDPROC
PROC RemoveHashPlateFromKnownDupeList(INT iHashPlate)
	INT iIndex
	iIndex = GetIndexOfValueInGlobalArray(g_iKnownDupePlateStats, iHashPlate)
	#IF IS_DEBUG_BUILD
		IF (iIndex > -1)
			PRINTLN("[platedupes] RemoveHashPlateFromKnownDupeList - in stats list, index ", iIndex)
		ENDIF	
	#ENDIF
	IF (iIndex > -1)
		ShiftKnownDupePlateListDown(iIndex)
		ShiftKnownDupeSoldListDown(iIndex)
	ELSE
		PRINTLN("[platedupes] RemoveHashPlateFromKnownDupeList - not in stats lists - hash ", iHashPlate)	
	ENDIF
	iIndex = GetIndexOfValueInGlobalArray(g_iKnownDupePlateGlobals, iHashPlate)
	#IF IS_DEBUG_BUILD
		IF (iIndex > -1)
			PRINTLN("[platedupes] RemoveHashPlateFromKnownDupeList - in global list, index ", iIndex)
		ENDIF	
	#ENDIF
	IF (iIndex > -1)
		DeleteSlotFromGlobalArray(g_iKnownDupePlateGlobals, iIndex)
		DeleteSlotFromGlobalArray(g_iKnownDupeSoldGlobals, iIndex)
	ELSE
		PRINTLN("[platedupes] RemoveHashPlateFromKnownDupeList - not in global lists - hash ", iHashPlate)	
	ENDIF
ENDPROC


FUNC INT GetStoredKnownDupeSoldForPlate(TEXT_LABEL_15 tlPlate)
	
	Pad_Plate_To_8_Characters(tlPlate)

	INT i
	INT iHashPlate = GET_HASH_KEY(tlPlate)
	
	// check stats
	REPEAT TOTAL_STORED_STATS_KNOWN_DUPES i
		IF (g_iKnownDupePlateStats[i] = iHashPlate)	
			RETURN g_iKnownDupeSoldStats[i]
		ENDIF
	ENDREPEAT
	
	// check global list
	REPEAT COUNT_OF(g_iKnownDupePlateGlobals) i
		IF (g_iKnownDupePlateGlobals[i] = iHashPlate)	
			RETURN g_iKnownDupeSoldGlobals[i]
		ENDIF
	ENDREPEAT
	
	RETURN 0
	
ENDFUNC

// ====== PERSONALISED PLATES ======

FUNC INT GetRecentlyUsedPersonalisedPlateStatValue(INT iIndex)
	SWITCH iIndex
		CASE 0 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_PERSZOLAPUMA0) // g_iStat_PersonalisedCarPlate0
		CASE 1 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_PERSZOLAPUMA1) // g_iStat_PersonalisedCarPlate1
		CASE 2 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_PERSZOLAPUMA2) // g_iStat_PersonalisedCarPlate2
		CASE 3 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_PERSZOLAPUMA3) // g_iStat_PersonalisedCarPlate3
		CASE 4 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_PERSZOLAPUMA4) // g_iStat_PersonalisedCarPlate4
	ENDSWITCH
	RETURN 0
ENDFUNC

PROC SetRecentlyUsedPersonalisedPlateStatValue(INT iIndex, INT iValue)
	SWITCH iIndex
		CASE 0 SET_MP_INT_PLAYER_STAT(MPPLY_PERSZOLAPUMA0, iValue) BREAK // g_iStat_PersonalisedCarPlate0 = iValue BREAK
		CASE 1 SET_MP_INT_PLAYER_STAT(MPPLY_PERSZOLAPUMA1, iValue) BREAK // g_iStat_PersonalisedCarPlate1 = iValue BREAK
		CASE 2 SET_MP_INT_PLAYER_STAT(MPPLY_PERSZOLAPUMA2, iValue) BREAK // g_iStat_PersonalisedCarPlate2 = iValue BREAK
		CASE 3 SET_MP_INT_PLAYER_STAT(MPPLY_PERSZOLAPUMA3, iValue) BREAK // g_iStat_PersonalisedCarPlate3 = iValue BREAK
		CASE 4 SET_MP_INT_PLAYER_STAT(MPPLY_PERSZOLAPUMA4, iValue) BREAK // g_iStat_PersonalisedCarPlate4 = iValue BREAK
	ENDSWITCH
	g_iPersonalisedCarPlateStats[iIndex] = iValue
ENDPROC

PROC ShiftRecentlyUsedPersonalisedPlateStatsListUp(INT iIndex=0)
	IF (iIndex < TOTAL_STORED_STATS_PLATES_PERSONALISED)
		SetRecentlyUsedPersonalisedPlateStatValue(iIndex, GetRecentlyUsedPersonalisedPlateStatValue(iIndex+1))
		ShiftRecentlyUsedPersonalisedPlateStatsListUp(iIndex + 1)
	ENDIF
ENDPROC
PROC ShiftRecentlyUsedPersonalisedPlateListDown(INT iIndex=0)
	IF (iIndex > 0)
		SetRecentlyUsedPersonalisedPlateStatValue(iIndex, GetRecentlyUsedPersonalisedPlateStatValue(iIndex-1))
		ShiftRecentlyUsedPersonalisedPlateListDown(iIndex - 1)
	ELSE
		IF (iIndex = 0)
			SetRecentlyUsedPersonalisedPlateStatValue(0, 0) // becomes a free space
		ENDIF	
	ENDIF
ENDPROC

PROC RemoveHashPlateFromRecentlyUsedPersonalisedList(INT iHashPlate)
	INT iIndex
	iIndex = GetIndexOfValueInGlobalArray(g_iPersonalisedCarPlateStats, iHashPlate)
	#IF IS_DEBUG_BUILD
		IF (iIndex > -1)
			PRINTLN("[platedupes] RemoveHashPlateFromRecentlyUsedPersonalisedList - in stats list, index ", iIndex)
		ENDIF	
	#ENDIF
	IF (iIndex > -1)
		ShiftRecentlyUsedPersonalisedPlateListDown(iIndex)
	ELSE
		PRINTLN("[platedupes] RemoveHashPlateFromRecentlyUsedPersonalisedList - not in stats lists - hash ", iHashPlate)
	ENDIF
	iIndex = GetIndexOfValueInGlobalArray(g_iPersonalisedCarPlateGlobals, iHashPlate)
	#IF IS_DEBUG_BUILD
		IF (iIndex > -1)
			PRINTLN("[platedupes] RemoveHashPlateFromRecentlyUsedPersonalisedList - in global list, index ", iIndex)
		ENDIF	
	#ENDIF
	IF (iIndex > -1)
		DeleteSlotFromGlobalArray(g_iPersonalisedCarPlateGlobals, iIndex)
	ELSE
		PRINTLN("[platedupes] RemoveHashPlateFromRecentlyUsedPersonalisedList - not in global lists - hash ", iHashPlate)	
	ENDIF
ENDPROC


// ====== SOLD PLATES ======
FUNC INT GetRecentlySoldPlateStatValue(INT iIndex)
	SWITCH iIndex
		CASE 0 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_SOLDZOLAPUMA0) // g_iStat_SoldCarPlate0
		CASE 1 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_SOLDZOLAPUMA1) // g_iStat_SoldCarPlate1
		CASE 2 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_SOLDZOLAPUMA2) // g_iStat_SoldCarPlate2
		CASE 3 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_SOLDZOLAPUMA3) // g_iStat_SoldCarPlate3
		CASE 4 RETURN GET_MP_INT_PLAYER_STAT(MPPLY_SOLDZOLAPUMA4) // g_iStat_SoldCarPlate4
	ENDSWITCH
	RETURN 0
ENDFUNC

PROC SetRecentlySoldPlateStatValue(INT iIndex, INT iValue)
	SWITCH iIndex
		CASE 0 SET_MP_INT_PLAYER_STAT(MPPLY_SOLDZOLAPUMA0, iValue) BREAK // g_iStat_SoldCarPlate0 = iValue BREAK
		CASE 1 SET_MP_INT_PLAYER_STAT(MPPLY_SOLDZOLAPUMA1, iValue) BREAK // g_iStat_SoldCarPlate1 = iValue BREAK
		CASE 2 SET_MP_INT_PLAYER_STAT(MPPLY_SOLDZOLAPUMA2, iValue) BREAK // g_iStat_SoldCarPlate2 = iValue BREAK
		CASE 3 SET_MP_INT_PLAYER_STAT(MPPLY_SOLDZOLAPUMA3, iValue) BREAK // g_iStat_SoldCarPlate3 = iValue BREAK
		CASE 4 SET_MP_INT_PLAYER_STAT(MPPLY_SOLDZOLAPUMA4, iValue) BREAK // g_iStat_SoldCarPlate4 = iValue BREAK
	ENDSWITCH
	g_iSoldCarPlateStats[iIndex] = iValue
ENDPROC
PROC ShiftRecentlySoldPlateListUp(INT iIndex=0)
	IF (iIndex < TOTAL_STORED_STATS_PLATES_SOLD)
		SetRecentlySoldPlateStatValue(iIndex, GetRecentlySoldPlateStatValue(iIndex+1))
		ShiftRecentlySoldPlateListUp(iIndex + 1)
	ENDIF
ENDPROC
PROC ShiftRecentlySoldPlateListDown(INT iIndex=0)
	IF (iIndex > 0)
		SetRecentlySoldPlateStatValue(iIndex, GetRecentlySoldPlateStatValue(iIndex-1))
		ShiftRecentlySoldPlateListDown(iIndex - 1)
	ELSE
		IF (iIndex = 0)
			SetRecentlySoldPlateStatValue(0, 0) // becomes a free space
		ENDIF	
	ENDIF
ENDPROC
PROC RemoveHashPlateFromRecentlySoldList(INT iHashPlate)
	INT iIndex
	iIndex = GetIndexOfValueInGlobalArray(g_iSoldCarPlateStats, iHashPlate)
	#IF IS_DEBUG_BUILD
		IF (iIndex > -1)
			PRINTLN("[platedupes] RemoveHashPlateFromRecentlySoldList - in stats list, index ", iIndex)
		ENDIF	
	#ENDIF
	IF (iIndex > -1)
		ShiftRecentlySoldPlateListDown(iIndex)
	ELSE
		PRINTLN("[platedupes] RemoveHashPlateFromRecentlySoldList - not in stats lists - hash ", iHashPlate)
	ENDIF
	iIndex = GetIndexOfValueInGlobalArray(g_iSoldCarPlateGlobals, iHashPlate)
	#IF IS_DEBUG_BUILD
		IF (iIndex > -1)
			PRINTLN("[platedupes] RemoveHashPlateFromRecentlySoldList - in global list, index ", iIndex)
		ENDIF	
	#ENDIF
	IF (iIndex > -1)
		DeleteSlotFromGlobalArray(g_iSoldCarPlateGlobals, iIndex)
	ELSE
		PRINTLN("[platedupes] RemoveHashPlateFromRecentlySoldList - not in global lists - hash ", iHashPlate)
	ENDIF
ENDPROC


PROC RemoveDuplicateHashPlateEntriesFromKnownLists()
	
	INT iIndex
	INT iEntries
	INT iHashPlate
	INT i
	
	// ---  known dupes ---
	
	// stats
	REPEAT COUNT_OF(g_iKnownDupePlateStats) i
		iHashPlate = g_iKnownDupePlateStats[i]
		IF (iHashPlate != 0)
			iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iKnownDupePlateStats, iHashPlate)
			WHILE iEntries > 1
				#IF IS_DEBUG_BUILD
					PRINTLN("[platedupes] RemoveDuplicateHashPlateEntriesFromKnownLists - iHashPlate ", iHashPlate, " has ", iEntries, " entries in g_iKnownDupePlateStats")	
				#ENDIF	
				iIndex = GetIndexOfValueInGlobalArray(g_iKnownDupePlateStats, iHashPlate)
				ShiftKnownDupePlateListDown(iIndex)
				ShiftKnownDupeSoldListDown(iIndex)
				iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iKnownDupePlateStats, iHashPlate)
			ENDWHILE
		ENDIF
	ENDREPEAT
	
	// globals
	REPEAT COUNT_OF(g_iKnownDupePlateGlobals) i
		iHashPlate = g_iKnownDupePlateGlobals[i]
		IF (iHashPlate != 0)
			iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iKnownDupePlateGlobals, iHashPlate)
			WHILE iEntries > 1
				#IF IS_DEBUG_BUILD
					PRINTLN("[platedupes] RemoveDuplicateHashPlateEntriesFromKnownLists - iHashPlate ", iHashPlate, " has ", iEntries, " entries in g_iKnownDupePlateGlobals")	
				#ENDIF	
				iIndex = GetIndexOfValueInGlobalArray(g_iKnownDupePlateGlobals, iHashPlate)
				DeleteSlotFromGlobalArray(g_iKnownDupePlateGlobals, iIndex)
				DeleteSlotFromGlobalArray(g_iKnownDupeSoldGlobals, iIndex)
				iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iKnownDupePlateGlobals, iHashPlate)
			ENDWHILE
		ENDIF
	ENDREPEAT
	
	// ---  recently sold ---
	
	// stats
	REPEAT COUNT_OF(g_iSoldCarPlateStats) i
		iHashPlate = g_iSoldCarPlateStats[i]
		IF (iHashPlate != 0)
			iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iSoldCarPlateStats, iHashPlate)
			WHILE iEntries > 1
				#IF IS_DEBUG_BUILD
					PRINTLN("[platedupes] RemoveDuplicateHashPlateEntriesFromKnownLists - iHashPlate ", iHashPlate, " has ", iEntries, " entries in g_iSoldCarPlateStats")	
				#ENDIF	
				iIndex = GetIndexOfValueInGlobalArray(g_iSoldCarPlateStats, iHashPlate)
				ShiftRecentlySoldPlateListDown(iIndex)
				iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iSoldCarPlateStats, iHashPlate)
			ENDWHILE
		ENDIF
	ENDREPEAT
	
	// globals
	REPEAT COUNT_OF(g_iSoldCarPlateGlobals) i
		iHashPlate = g_iSoldCarPlateGlobals[i]	
		IF (iHashPlate != 0)
			iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iSoldCarPlateGlobals, iHashPlate)
			WHILE iEntries > 1
				#IF IS_DEBUG_BUILD
					PRINTLN("[platedupes] RemoveDuplicateHashPlateEntriesFromKnownLists - iHashPlate ", iHashPlate, " has ", iEntries, " entries in g_iSoldCarPlateGlobals")	
				#ENDIF	
				iIndex = GetIndexOfValueInGlobalArray(g_iSoldCarPlateGlobals, iHashPlate)
				DeleteSlotFromGlobalArray(g_iSoldCarPlateGlobals, iIndex)
				iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iSoldCarPlateGlobals, iHashPlate)
			ENDWHILE
		ENDIF
	ENDREPEAT
	
	// ---  recently personalised ---
	
	// stats
	REPEAT COUNT_OF(g_iPersonalisedCarPlateStats) i
		iHashPlate = g_iPersonalisedCarPlateStats[i]		
		IF (iHashPlate != 0)
			iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iPersonalisedCarPlateStats, iHashPlate)
			WHILE iEntries > 1
				#IF IS_DEBUG_BUILD
					PRINTLN("[platedupes] RemoveDuplicateHashPlateEntriesFromKnownLists - iHashPlate ", iHashPlate, " has ", iEntries, " entries in g_iPersonalisedCarPlateStats")	
				#ENDIF	
				iIndex = GetIndexOfValueInGlobalArray(g_iPersonalisedCarPlateStats, iHashPlate)
				ShiftRecentlyUsedPersonalisedPlateListDown(iIndex)
				iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iPersonalisedCarPlateStats, iHashPlate)
			ENDWHILE
		ENDIF
	ENDREPEAT
	
	// globals
	REPEAT COUNT_OF(g_iPersonalisedCarPlateGlobals) i
		iHashPlate = g_iPersonalisedCarPlateGlobals[i]		
		IF (iHashPlate != 0)
			iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iPersonalisedCarPlateGlobals, iHashPlate)
			WHILE iEntries > 1
				#IF IS_DEBUG_BUILD
					PRINTLN("[platedupes] RemoveDuplicateHashPlateEntriesFromKnownLists - iHashPlate ", iHashPlate, " has ", iEntries, " entries in g_iPersonalisedCarPlateGlobals")	
				#ENDIF	
				iIndex = GetIndexOfValueInGlobalArray(g_iPersonalisedCarPlateGlobals, iHashPlate)
				DeleteSlotFromGlobalArray(g_iPersonalisedCarPlateGlobals, iIndex)
				iEntries = GetNumberOfEntriesOfValueInGlobalArray(g_iPersonalisedCarPlateGlobals, iHashPlate)
			ENDWHILE	
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[platedupes] ------ RemoveDuplicateHashPlateEntriesFromKnownLists --------")
	OUTPUT_ALL_DUPE_DETECT_VALUES()	
	#ENDIF	

ENDPROC

FUNC BOOL ArePlateHashesEqual(TEXT_LABEL_15 tlPlate1, TEXT_LABEL_15 tlPlate2)
	RETURN (GET_HASH_KEY(tlPlate1) = GET_HASH_KEY(tlPlate2))
ENDFUNC

PROC ShiftGlobalArrayListsUp(INT &iArray1[], INT &iArray2[], INT iIndex=0)
	#IF IS_DEBUG_BUILD
		IF (COUNT_OF(iArray1) != COUNT_OF(iArray2))
			SCRIPT_ASSERT("ShiftGlobalArrayListsUp - arrays must be same size!")
		ENDIF
	#ENDIF
	IF (iIndex < (COUNT_OF(iArray1)-1))
		iArray1[iIndex] = iArray1[iIndex+1]
		iArray2[iIndex] = iArray2[iIndex+1]
		ShiftGlobalArrayListsUp(iArray1, iArray2, iIndex + 1)
	ENDIF
ENDPROC

FUNC BOOL AreValuesInGlobalArrays(INT &iArray1[], INT iValue1, INT &iArray2[], INT iValue2)
	#IF IS_DEBUG_BUILD
		IF (COUNT_OF(iArray1) != COUNT_OF(iArray2))
			SCRIPT_ASSERT("AreValuesInGlobalArrays - arrays must be same size!")
		ENDIF
	#ENDIF
	
	//PRINTLN("AreValuesInGlobalArrays - value1 = ", iValue1, ", value2 = ", iValue2)
	
	INT i
	REPEAT COUNT_OF(iArray1) i
		
		//PRINTLN("AreValuesInGlobalArrays - iArray1[", i, "] = ", iArray1[i], ", iArray2[", i, "] = ", iArray2[1])	
	
		IF ((iArray1[i] = iValue1) AND (iArray2[i] = iValue2))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC AddValueToGlobalArrays(INT &iArray1[], INT iValue1, INT &iArray2[], INT iValue2)
	#IF IS_DEBUG_BUILD
		IF (COUNT_OF(iArray1) != COUNT_OF(iArray2))
			SCRIPT_ASSERT("AddValueToGlobalArrays - arrays must be same size!")
		ENDIF
	#ENDIF

	IF NOT AreValuesInGlobalArrays(iArray1, iValue1, iArray2, iValue2)
		ShiftGlobalArrayListsUp(iArray1, iArray2)	
		iArray1[COUNT_OF(iArray1)-1] = iValue1
		iArray2[COUNT_OF(iArray2)-1] = iValue2
	ENDIF
ENDPROC

FUNC BOOL HasVehicleHadMetricSent(VEHICLE_INDEX VehicleID, INT iLocation)
	RETURN AreValuesInGlobalArrays(g_iVehicleIDMetricSent, NATIVE_TO_INT(VehicleID), g_iLocationMetricSent, iLocation)
ENDFUNC

PROC AddVehicleToMetricSentList(VEHICLE_INDEX VehicleID, INT iLocation)
	AddValueToGlobalArrays(g_iVehicleIDMetricSent, NATIVE_TO_INT(VehicleID), g_iLocationMetricSent, iLocation)
ENDPROC





