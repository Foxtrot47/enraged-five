

// ----------------------------------------------------- \\
// ----------------------------------------------------- \\
//                                                       \\
// SCRIPT:                                               \\
//                                                       \\
// net_heli_gun_launching.sc                             \\
//                                                       \\
// ----------------------------------------------------- \\
//                                                       \\
// DESCRIPTION:                                          \\
//                                                       \\
// handles launching and terminating net_heli_gun.sc     \\
//                                                       \\
// ----------------------------------------------------- \\
//                                                       \\
// AUTHOR:                                               \\
//                                                       \\
// William.Kennedy@RockstarNorth.com                     \\
//                                                   	 \\
// ----------------------------------------------------- \\
// ----------------------------------------------------- \\

/// PURPOSE: Headers.
USING "globals.sch"
USING "net_include.sch"
USING "net_scoring_common.sch"
USING "net_mission.sch"
USING "menu_public.sch"
USINg "net_spawn.sch"

#IF IS_DEBUG_BUILD 
USING "net_debug.sch"
#ENDIF

USING "VehicleTurretHeader.sch"

CONST_INT	TRUCK_BS_IS_TRUCK_TURRET		0

/// PURPOSE: Heli gun launching states.
ENUM eHELI_GUN_LAUNCING_STATE
	eHELIGUNLAUNCHINGSTATE_OFF = 0,
	eHELIGUNLAUNCHINGSTATE_LAUNCHING,
	eHELIGUNLAUNCHINGSTATE_RUNNING,
	eHELIGUNLAUNCHINGSTATE_RESET
ENDENUM

ENUM HELI_LAUNCHING_GENERAL_BS
	HL_BS_HAVE_SET_RESET_TURRET_FALSE
	
ENDENUM

/// PURPOSE: Heli gun launching data.
STRUCT STRUCT_HELI_GUN_LAUNCHING_DATA
	eHELI_GUN_LAUNCING_STATE eState
	MP_MISSION_DATA scriptToLaunch
	BOOL bInitialisedScriptLaunch
	BOOL bIAmPilot
	BOOL bInBackSeatOfValkyrie
	BOOL bInFrontRightOfValkyrie
	BOOL bDoFirstPersonMode
	BOOL bShouldHaveAccessToHeliGun
	BOOL bShownPilotRestrictionsHelpText
	BOOL bShowRestrictionsHelpForPilot
	PLAYER_INDEX coPilotId
	VEHICLE_INDEX viHeli
	INT iNumThreadsRunningHeliGunScript
	BOOL bInValkyrie
	BOOL bInGunRunningTruck
	BOOL bInValidVehicle
	INT iBSGeneral  //Uses HELI_LAUNCHING_GENERAL_BS (prefix : HL_BS_)
	VEHICLE_INDEX viHeliSetTurretResetFalse //The heli that we set SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS false for
ENDSTRUCT

/// PURPOSE: Heli gun launching debug functions.
#IF IS_DEBUG_BUILD

BOOL bAllowDriverGunAccess
BOOL bCreatedWidgets
TEXT_WIDGET_ID twID_launchingstate

BOOL bCreateTempGunCamChopper
BOOL bCreateTempHeliCam
VEHICLE_INDEX viTempChopper
CAMERA_INDEX tempCam
VECTOR vTempgunCamCamOffset, vTempGunCamCoords, vTempGunCamCamRotation
BOOL bDrawDebugSphere, bDrawTempGunCam, bTurnedOnTempCam

FUNC STRING GET_HELI_LAUNCHING_STATE_NAME(eHELI_GUN_LAUNCING_STATE eState)
	SWITCH eState
		CASE eHELIGUNLAUNCHINGSTATE_OFF				RETURN "OFF"
		CASE eHELIGUNLAUNCHINGSTATE_LAUNCHING		RETURN "LAUNCHING"
		CASE eHELIGUNLAUNCHINGSTATE_RUNNING			RETURN "RUNNING"
		CASE eHELIGUNLAUNCHINGSTATE_RESET			RETURN "RESET"
	ENDSWITCH
	RETURN "NOT_IN_SWITCH"
ENDFUNC

PROC CREATE_HELI_GUN_WIDGETS() 
	
	IF NOT bCreatedWidgets
		START_WIDGET_GROUP("Heli Gun Launching")
			ADD_WIDGET_BOOL("Allow Driver Gun Access", bAllowDriverGunAccess)
			twID_launchingstate = ADD_TEXT_WIDGET("State")
			START_WIDGET_GROUP("Temp Camera")
				ADD_WIDGET_BOOL("Create Temp Valkyrie", bCreateTempGunCamChopper)
				ADD_WIDGET_BOOL("Create Temp Cam", bCreateTempHeliCam)
				ADD_WIDGET_VECTOR_SLIDER("Cam Offset", vTempgunCamCamOffset, -1000.0, 1000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Cam Rotation", vTempGunCamCamRotation, -1000.0, 1000.0, 0.01)
				ADD_WIDGET_BOOL("Highlight Cam", bDrawDebugSphere)
				ADD_WIDGET_BOOL("Render Cam", bDrawTempGunCam)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		bCreatedWidgets = TRUE
	ENDIF
	
ENDPROC

PROC MAINTAIN_HELI_GUN_LAUNCHING_WIDGETS(STRUCT_HELI_GUN_LAUNCHING_DATA sData)
	
	SET_CONTENTS_OF_TEXT_WIDGET(twID_launchingstate, GET_HELI_LAUNCHING_STATE_NAME(sData.eState))
	
ENDPROC

#ENDIF

FUNC BOOL SHOULD_ALLOW_PILOT_TO_USE_HELI_CAM()
	#IF FEATURE_TUNER
	IF g_heligunForceAllow
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determines if a passenger turret cam is in use.   
FUNC BOOL IS_PASSENGER_TURRET_CAM_ON(VEHICLE_INDEX veh, VEHICLE_SEAT eSeat)
	PED_INDEX passenger = GET_PED_IN_VEHICLE_SEAT(veh, eSeat)
	IF DOES_ENTITY_EXIST(passenger)
	AND NOT IS_ENTITY_DEAD(passenger)
		PLAYER_INDEX player = NETWORK_GET_PLAYER_INDEX_FROM_PED(passenger)
		INT iPlayer = NATIVE_TO_INT(player)
		IF iPlayer <> -1
		 	RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bInHeliGunCam) //GlobalplayerBD_FM[iPlayer].bInHeliGunCam
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///Converts a seat index to a main weapon index (non-modded) based on the vehicle we are currently in. 
FUNC INT SEAT_TO_PRIMARY_WEAPON_INDEX(VEHICLE_SEAT eSeatIndex, MODEL_NAMES model)
	SWITCH model
	CASE BOMBUSHKA
		SWITCH eSeatIndex
			CASE VS_FRONT_RIGHT RETURN 2	// Top Turret
			CASE VS_BACK_LEFT	RETURN 0	// Nose Turret (Stock)
			CASE VS_BACK_RIGHT 	RETURN 3	// Rear Turret (Stock)
		ENDSWITCH
	BREAK
	CASE VOLATOL
		SWITCH eSeatIndex
			CASE VS_BACK_LEFT 	RETURN 0
			CASE VS_BACK_RIGHT 	RETURN 1
		ENDSWITCH
	BREAK
	CASE AKULA
		SWITCH eSeatIndex
			CASE VS_FRONT_RIGHT RETURN 0	// Nose Turret (Stock)
		ENDSWITCH
	BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

/// PURPOSE:
///Converts a seat index to a secondary weapon index (modded) based on the vehicle we are currently in. 
FUNC INT SEAT_TO_SECONDARY_WEAPON_INDEX(VEHICLE_SEAT eSeatIndex, MODEL_NAMES model)
	SWITCH model
	CASE BOMBUSHKA
		SWITCH eSeatIndex
			CASE VS_BACK_LEFT	RETURN 1	// Nose Turret (Modded)
			CASE VS_BACK_RIGHT 	RETURN 4	// Rear Turret (Modded)
		ENDSWITCH
	BREAK
	CASE AKULA
		SWITCH eSeatIndex
			CASE VS_FRONT_RIGHT RETURN 2	// Nose Turret (Modded)
		ENDSWITCH
	BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

PROC MAINTAIN_RESTRICTED_AMMO_FOR_TURRET(VEHICLE_INDEX vehicle, MODEL_NAMES model, VEHICLE_SEAT eSeat)
	INT iAmmoCount = PICK_INT(IS_PASSENGER_TURRET_CAM_ON(vehicle, eSeat), -1, 0)
	
	// Set for stock weapon index
	INT iPrimaryWeaponIndex = SEAT_TO_PRIMARY_WEAPON_INDEX(eSeat, model)
	IF iPrimaryWeaponIndex = -1
		EXIT
	ENDIF
	IF GET_VEHICLE_WEAPON_RESTRICTED_AMMO( vehicle, iPrimaryWeaponIndex ) <> iAmmoCount
		SET_VEHICLE_WEAPON_RESTRICTED_AMMO( vehicle, iPrimaryWeaponIndex, iAmmoCount )
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SET_VEHICLE_WEAPON_RESTRICTED_AMMO(vehicle = ", NATIVE_TO_INT(vehicle), ", iPrimaryWeaponIndex = ", iPrimaryWeaponIndex,", iAmmoCount = ",iAmmoCount,")")
	ENDIF
	
	// Set for modded weapon index as well
	INT iSecondaryWeaponIndex = SEAT_TO_SECONDARY_WEAPON_INDEX(eSeat, model)
	IF iSecondaryWeaponIndex = -1
		EXIT
	ENDIF	
	IF GET_VEHICLE_WEAPON_RESTRICTED_AMMO( vehicle, iSecondaryWeaponIndex ) <> iAmmoCount
		SET_VEHICLE_WEAPON_RESTRICTED_AMMO( vehicle, iSecondaryWeaponIndex, iAmmoCount )
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "SET_VEHICLE_WEAPON_RESTRICTED_AMMO(vehicle = ", NATIVE_TO_INT(vehicle), ", iSecondaryWeaponIndex = ", iSecondaryWeaponIndex,", iAmmoCount = ",iAmmoCount,")")
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops the turrets rotating while any passengers are
///    not actually using the turrets. 
///    Should be called every frame by freemode.sc after a call to 
///	   MAINTAIN_HELI_PASSENGER_GUN_LAUNCHING().
PROC MAINTAIN_RESTRICTED_AMMO_TO_STOP_VEH_TURRET_ROTATION(STRUCT_HELI_GUN_LAUNCHING_DATA& data)
	BOOL bUsingValidVehicle = FALSE
	IF DOES_ENTITY_EXIST( data.viHeli )
	AND NOT IS_ENTITY_DEAD( data.viHeli )						
	AND NETWORK_HAS_CONTROL_OF_ENTITY( data.viHeli )
		MODEL_NAMES model = GET_ENTITY_MODEL( data.viHeli )
		IF model = VOLATOL
		OR model = BOMBUSHKA
		OR model = AKULA
			bUsingValidVehicle = TRUE
			//Ammo == 0  -> Turrets cannot rotate
			//Ammo == -1 -> turrets can rotate
			MAINTAIN_RESTRICTED_AMMO_FOR_TURRET( data.viHeli, model, VS_FRONT_RIGHT ) 
			MAINTAIN_RESTRICTED_AMMO_FOR_TURRET( data.viHeli, model, VS_BACK_LEFT	) 
			MAINTAIN_RESTRICTED_AMMO_FOR_TURRET( data.viHeli, model, VS_BACK_RIGHT 	) 
			//Set this so that the turrets do not try to reset themselves
			IF NOT IS_BIT_SET_ENUM(data.iBSGeneral, HL_BS_HAVE_SET_RESET_TURRET_FALSE)
				CDEBUG1LN( DEBUG_NET_GUN_TURRET, "SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS(",NATIVE_TO_INT(data.viHeli),", FALSE)" )
				SET_BIT_ENUM( data.iBSGeneral, HL_BS_HAVE_SET_RESET_TURRET_FALSE )
				data.viHeliSetTurretResetFalse = data.viHeli
				SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS( data.viHeli, FALSE )
			ENDIF
		ENDIF
	ENDIF
	
	//If we have set SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS to false but are now no 
	//longer in the vehicle, set to false
	IF NOT bUsingValidVehicle
	AND IS_BIT_SET_ENUM( data.iBSGeneral, HL_BS_HAVE_SET_RESET_TURRET_FALSE )
		IF DOES_ENTITY_EXIST( data.viHeliSetTurretResetFalse )
		AND NOT IS_ENTITY_DEAD( data.viHeliSetTurretResetFalse )
		AND NETWORK_HAS_CONTROL_OF_ENTITY( data.viHeliSetTurretResetFalse )
		AND NOT IS_PASSENGER_TURRET_CAM_ON( data.viHeliSetTurretResetFalse, VS_FRONT_RIGHT	) 
		AND NOT IS_PASSENGER_TURRET_CAM_ON( data.viHeliSetTurretResetFalse, VS_BACK_LEFT	) 						
		AND NOT IS_PASSENGER_TURRET_CAM_ON( data.viHeliSetTurretResetFalse, VS_BACK_RIGHT	) 	
			CDEBUG1LN( DEBUG_NET_GUN_TURRET, "SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS(",NATIVE_TO_INT(data.viHeliSetTurretResetFalse),", TRUE)" )
			SET_SHOULD_RESET_TURRET_IN_SCRIPTED_CAMERAS( data.viHeliSetTurretResetFalse, TRUE )
		ENDIF
		CLEAR_BIT_ENUM( data.iBSGeneral, HL_BS_HAVE_SET_RESET_TURRET_FALSE )
		data.viHeliSetTurretResetFalse = INT_TO_NATIVE(VEHICLE_INDEX, -1)	
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the heli gun launching state.
/// PARAMS:
///    sData - struct of heli gun launching data.
///    eState - state to set heli gun launching to.
PROC SET_HELI_LAUNCHING_STATE(STRUCT_HELI_GUN_LAUNCHING_DATA &sData, eHELI_GUN_LAUNCING_STATE eState)
	
	#IF IS_DEBUG_BUILD
		IF (sData.eState != eState)
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[WJK] - heli gun launching - setting launching state to ", GET_HELI_LAUNCHING_STATE_NAME(eState))NET_NL()
		ENDIF
	#ENDIF
	
	sData.eState = eState
	
ENDPROC

FUNC BOOL IS_PED_SITTING_IN_SCRIPTED_GUN_HELI_MODEL(STRUCT_HELI_GUN_LAUNCHING_DATA &sData, PED_INDEX pedId, VEHICLE_INDEX &heliId, BOOL &bInValkyrie)
	
	VEHICLE_INDEX blankVehicle
	sData.viHeli = blankVehicle
	
	IF IS_PED_SITTING_IN_ANY_VEHICLE(pedId)
		heliId = GET_VEHICLE_PED_IS_IN(pedId)
		IF IS_MODEL_VALID_HELI_GUN_MODEL(heliId, bInValkyrie)
			IF IS_VEHICLE_DRIVEABLE(heliId)
				sData.viHeli = heliId
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DISPLAY_ON_JOB_HELI_GUN_RESTRICTIONS_HELP()
	#IF IS_DEBUG_BUILD
	BOOL bDoGunHelp
	VEHICLE_INDEX heliId
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet,ciALLOW_HELI_CAM)
		RETURN TRUE
	ENDIF

	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_HELP_ROCKET_HEIST_TEXT) < 3
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					heliId = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF GET_ENTITY_MODEL(heliId) = VALKYRIE
						bDoGunHelp = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF bDoGunHelp
			PRINT_HELP("HUNTGUN_7b")
		ELSE
			PRINT_HELP("HUNTGUN_7")
		ENDIF
		INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_HELP_ROCKET_HEIST_TEXT)
		RETURN TRUE
	ELSE
		RETURN TRUE
	ENDIF
	#ENDIF
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_PILOT_ON_JOB_HELI_GUN_RESTRICTIONS_HELP(STRUCT_HELI_GUN_LAUNCHING_DATA &sData)
	
	IF NOT sData.bShownPilotRestrictionsHelpText
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			IF sData.bShowRestrictionsHelpForPilot
				IF DISPLAY_ON_JOB_HELI_GUN_RESTRICTIONS_HELP()
					sData.bShownPilotRestrictionsHelpText = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



/// PURPOSE:
///    Gets if the local player should be able to use the heli guin this frame.
/// RETURNS:
///    TRUE if sitting in the back left or right seats of a valid heli, FALSE if not.
PROC MAINTAIN_LAUNCHING_FLAGS(STRUCT_HELI_GUN_LAUNCHING_DATA &sData)
	
	INT i
	
	PED_INDEX pedId, coPilotPed,turretPilot1Ped, turretPilot2Ped
	VEHICLE_INDEX heliId
	
	pedId = GET_PLAYER_PED(PLAYER_ID())
	
	sData.bShouldHaveAccessToHeliGun = FALSE
	sData.bInBackSeatOfValkyrie = FALSE
	sData.bShowRestrictionsHelpForPilot = FALSE
	sData.bInValkyrie = FALSE
	sData.bInValidVehicle = FALSE
//	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_LAUNCHING_FLAGS")
	IF IS_PED_SITTING_IN_SCRIPTED_GUN_HELI_MODEL(sData, pedId, heliId, sData.bInValkyrie)
		sData.bInValidVehicle = TRUE
		BOOL bTemp
		coPilotPed = GET_PED_IN_VEHICLE_SEAT(heliId, VS_FRONT_RIGHT)
		turretPilot1Ped = GET_PED_IN_VEHICLE_SEAT(heliId, VS_BACK_LEFT)
		turretPilot2Ped = GET_PED_IN_VEHICLE_SEAT(heliId, VS_BACK_RIGHT)
		// If I am the copilot, I have access.
		IF coPilotPed = PLAYER_PED_ID()
		OR(IS_MODEL_VALID_HELI_GUN_MODEL(sData.viHeli, bTemp)
			AND (turretPilot1Ped = PLAYER_PED_ID() OR turretPilot2Ped = PLAYER_PED_ID()))
			sData.bIAmPilot = FALSE
			sData.coPilotId = INVALID_PLAYER_INDEX()
			sData.bShouldHaveAccessToHeliGun = TRUE
			IF sData.bInValkyrie
				sData.bInFrontRightOfValkyrie = TRUE
			ENDIF
			EXIT
		ELSE
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_LAUNCHING_FLAGS: player isn't co pilot, or turret operator on bombushka")
		ENDIF
		
		// If I am the pilot, I have access once the copilot is running the script.
		IF GET_PED_IN_VEHICLE_SEAT(heliId, VS_DRIVER) = PLAYER_PED_ID()
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet,ciALLOW_HELI_CAM)
				sData.bShowRestrictionsHelpForPilot = TRUE
			ENDIF
			IF DOES_ENTITY_EXIST(coPilotPed)
				IF NOT IS_ENTITY_DEAD(coPilotPed)
				
					IF IS_PED_A_PLAYER(coPilotPed)
						sData.coPilotId = NETWORK_GET_PLAYER_INDEX_FROM_PED(coPilotPed)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT_ON_SCRIPT(sData.coPilotId, "heli_gun", NATIVE_TO_INT(sData.coPilotId))
							sData.bIAmPilot = TRUE
							sData.bShouldHaveAccessToHeliGun = TRUE
							EXIT
						ENDIF
						
					ELIF SHOULD_ALLOW_PILOT_TO_USE_HELI_CAM()		// If a non-player ped is in co-pilot seat, run pilot enable cam checks
						sData.bShouldHaveAccessToHeliGun = TRUE
						EXIT
					ENDIF
					
				ELIF SHOULD_ALLOW_PILOT_TO_USE_HELI_CAM()		// If dead ped is in co-pilot seat, run pilot enable cam checks
					sData.bShouldHaveAccessToHeliGun = TRUE
					EXIT
				ENDIF
				
			ELIF SHOULD_ALLOW_PILOT_TO_USE_HELI_CAM()			// If no ped is in co-pilot seat, run pilot enable cam checks
				sData.bShouldHaveAccessToHeliGun = TRUE
				EXIT
			ENDIF
		ELSE
			IF sData.bInValkyrie
				REPEAT COUNT_OF(VEHICLE_SEAT) i
					IF (INT_TO_ENUM(VEHICLE_SEAT, i) != VS_ANY_PASSENGER)
						IF (INT_TO_ENUM(VEHICLE_SEAT, i) != VS_DRIVER)
							IF (INT_TO_ENUM(VEHICLE_SEAT, i) != VS_FRONT_RIGHT)
								IF GET_PED_IN_VEHICLE_SEAT(heliId, INT_TO_ENUM(VEHICLE_SEAT, i)) = PLAYER_PED_ID()
									sData.bInBackSeatOfValkyrie = TRUE
									EXIT
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
		
		// Debug widget so can test with one person.
		#IF IS_DEBUG_BUILD
		IF bAllowDriverGunAccess
			sData.bShouldHaveAccessToHeliGun = TRUE
			EXIT
		ENDIF
		#ENDIF
			
	ENDIF
	
	sData.bIAmPilot = FALSE
	sData.coPilotId = INVALID_PLAYER_INDEX()
	
	sData.bShouldHaveAccessToHeliGun = FALSE
ENDPROC

PROC MAINTAIN_STRAIGHT_INTO_VALKYRIE_FIRST_PEROSN_MODE_DATA(STRUCT_HELI_GUN_LAUNCHING_DATA &sData)
	
	IF sData.bInValkyrie
	AND sData.bShouldHaveAccessToHeliGun
		IF NOT DOES_ENTITY_EXIST(MPGlobalsAmbience.lastValkyrieForStraightIntoFpMode)
			MPGlobalsAmbience.lastValkyrieForStraightIntoFpMode = sData.viHeli
		ENDIF
	ELSE
		VEHICLE_INDEX tempVeh
		MPGlobalsAmbience.lastValkyrieForStraightIntoFpMode = tempVeh
		MPGlobalsAmbience.bGoneStraightIntoValkyrieFpModeforThisHeli = FALSE
	ENDIF
	
ENDPROC
FUNC BOOL IS_HELI_SEAT_TAKEN_BY_A_PLAYER( VEHICLE_INDEX viHeli, VEHICLE_SEAT vsSeat, PED_INDEX& pedInSeat )
	pedInSeat = GET_PED_IN_VEHICLE_SEAT(viHeli, vsSeat)
	IF DOES_ENTITY_EXIST(pedInSeat)
	AND NOT IS_ENTITY_DEAD(pedInSeat)
	AND IS_PED_A_PLAYER(pedInSeat)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL SHOULD_HELI_GUN_LAUNCHING_RESET(STRUCT_HELI_GUN_LAUNCHING_DATA &sData)
	IF NOT sData.bInValidVehicle
		PRINTLN("[OCH] SHOULD_HELI_GUN_LAUNCHING_RESET - TRUE: bsData.bInValidVehicle = ", sData.bInValidVehicle )
		RETURN TRUE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		PRINTLN("[OCH] SHOULD_HELI_GUN_LAUNCHING_RESET - TRUE: NOT IS_NET_PLAYER_OK()")
		RETURN TRUE
	ENDIF
	
	IF NOT sData.bShouldHaveAccessToHeliGun
		PRINTLN("[OCH] SHOULD_HELI_GUN_LAUNCHING_RESET - sData.bShouldHaveAccessToHeliGuN = FALSE, if next debug is not '[OCH] SHOULD_HELI_GUN_LAUNCHING_RESET...' then script doesn't need to reset" )
		IF DOES_ENTITY_EXIST( sData.viHeli )
		AND NOT NETWORK_HAS_CONTROL_OF_ENTITY( sData.viHeli )
			PRINTLN("[OCH] SHOULD_HELI_GUN_LAUNCHING_RESET - TRUE: bShouldHaveAccesToHeliGun = ", sData.bShouldHaveAccessToHeliGun," AND bIAmPilot = ", sData.bIAmPilot)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL FREEMODE_LAUNCH_HELI_GUN_SCRIPT(STRUCT_HELI_GUN_LAUNCHING_DATA &sData)
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
	OR NOT IS_SKYSWOOP_AT_GROUND()
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[net_heli_gun_launching][FREEMODE_LAUNCH_HELI_GUN_SCRIPT] FALSE : [SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE] TRUE")
		RETURN FALSE
	ENDIF
	
	INT iInstance
	
	IF IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(sData.viHeli)
		iInstance = NATIVE_TO_INT(PLAYER_ID())
	ELIF sData.bIAmPilot
		iInstance = NATIVE_TO_INT(sData.coPilotId)
	ELSE
		iInstance = NATIVE_TO_INT(PLAYER_ID())
	ENDIF
				
	sData.scriptToLaunch.mdID.idMission = eHELI_GUN
	sData.scriptToLaunch.iInstanceId = iInstance
	
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[OCH] FREEMODE_LAUNCH_HELI_GUN_SCRIPT - Instance ID = ", iInstance )						
	RETURN NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(sData.scriptToLaunch)
ENDFUNC

PROC TRACK_IF_HELI_GUN_SCRIPT_IS_RUNNING(STRUCT_HELI_GUN_LAUNCHING_DATA &sData)
	
	sData.iNumThreadsRunningHeliGunScript = 0
	
	IF DOES_SCRIPT_EXIST("heli_gun")
		sData.iNumThreadsRunningHeliGunScript = GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("heli_gun"))
	ENDIF
ENDPROC

PROC MAINAIN_BACK_SEAT_FIRST_PERSON_MODE(STRUCT_HELI_GUN_LAUNCHING_DATA &sData)
	
	BOOL bDoFirstPersonCam
	
	IF sData.bInBackSeatOfValkyrie
		bDoFirstPersonCam = TRUE
//		PRINTLN("heli gun launching - sData.bInBackSeatOfValkyrie = TRUE, setting bDoFirstPersonCam = TRUE.")
	ENDIF
	
//	IF sData.bInFrontRightOfValkyrie 
//		PRINTLN("heli gun launching - sData.bInFrontRightOfValkyrie  = TRUE.")
//		IF NOT GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInHeliGunCam
//			PRINTLN("heli gun launching - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInHeliGunCam = FALSE, setting bDoFirstPersonCam = TRUE.")
//			bDoFirstPersonCam = TRUE
//		ENDIF
//	ENDIF
	
	IF bDoFirstPersonCam
//		PRINTLN("heli gun launching - bDoFirstPersonCam = TRUE.")
		IF DOES_ENTITY_EXIST(sData.viHeli)
//			PRINTLN("heli gun launching - heli exists.")
			IF IS_VEHICLE_DRIVEABLE(sData.viHeli)
//				PRINTLN("heli gun launching - heli driveable, calling SET_ALLOW_CUSTOM_VEHICLE_DRIVE_BY_CAM_THIS_UPDATE(TRUE).")
				SET_ALLOW_CUSTOM_VEHICLE_DRIVE_BY_CAM_THIS_UPDATE(TRUE)
				IF IS_PHONE_ONSCREEN()
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UD)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
					CPRINTLN(DEBUG_PAUSE_MENU, "DISABLE_CONTROL_ACTION INPUT_VEH_PASSENGER_ATTACK 1 at frame ", GET_FRAME_COUNT())
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

PROC MAINTAIN_DEBUG_TEMP_CAM()
	
	IF bCreateTempGunCamChopper
	OR bCreateTempHeliCam
		IF NOT DOES_ENTITY_EXIST(viTempChopper)
			REQUEST_LOAD_MODEL(VALKYRIE)
			IF HAS_MODEL_LOADED(VALKYRIE)
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						viTempChopper = CREATE_VEHICLE(VALKYRIE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 10.0, 0.0 >>), 0.0, FALSE, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(viTempChopper)
				IF bCreateTempHeliCam
					IF NOT DOES_CAM_EXIST(tempCam)
						tempCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					ELSE
						vTempGunCamCamRotation = GET_ENTITY_ROTATION(viTempChopper)
						vTempGunCamCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viTempChopper, vTempgunCamCamOffset)
						SET_CAM_PARAMS(tempCam, vTempGunCamCoords, vTempGunCamCamRotation, 45.0, 0)
						IF bDrawDebugSphere
							VECTOR vCamRot = GET_CAM_ROT(tempCam)
							DRAW_DEBUG_SPAWN_POINT(GET_CAM_COORD(tempCam), vCamRot.z, HUD_COLOUR_GREENLIGHT)
						ENDIF
						IF bDrawTempGunCam
							IF NOT bTurnedOnTempCam
								SET_CAM_ACTIVE(tempCam, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, TRUE)
								bTurnedOnTempCam = TRUE
							ENDIF
						ELSE
							IF bTurnedOnTempCam
								SET_CAM_ACTIVE(tempCam, FALSE)
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								bTurnedOnTempCam = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

#ENDIF
PROC MAINTAIN_TRUCK_GUN_TURRET_LAUNCHING()
	IF (g_iShouldLaunchTruckTurret != -1
	OR g_iInteriorTurretSeat != -1)
	AND NOT IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	#IF FEATURE_HEIST_ISLAND
	AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	#ENDIF
//	AND NOT NETWORK_IS_SCRIPT_ACTIVE("heli_gun", NATIVE_TO_INT(PLAYER_ID()), TRUE)
		IF NOT IS_MP_PASSIVE_MODE_ENABLED()
		
			IF g_bCanPlayerUseTurretControls
				IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE() = FALSE
				AND IS_SKYSWOOP_AT_GROUND()
				
					IF NOT NETWORK_IS_SCRIPT_ACTIVE("heli_gun", NATIVE_TO_INT(PLAYER_ID()), TRUE)
						REQUEST_SCRIPT("heli_gun")
							
						IF HAS_SCRIPT_LOADED("heli_gun")
							MP_MISSION_DATA HordeMissionData 
							
			//				SET_BIT(HordeMissionData.iTruckBitSet, TRUCK_BS_IS_TRUCK_TURRET)
							
							HordeMissionData.mdID.idMission = eHELI_GUN
							HordeMissionData.iInstanceId = NATIVE_TO_INT(PLAYER_ID())
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[POD] - MAINTAIN_TRUCK_GUN_TURRET_LAUNCHING: Script hasn't launched yet, launching heli_gun instanceID: ", HordeMissionData.iInstanceId)NET_NL()
							START_NEW_SCRIPT_WITH_ARGS("heli_gun", HordeMissionData, SIZE_OF(HordeMissionData),  MULTIPLAYER_MISSION_STACK_SIZE) 
							SET_SCRIPT_AS_NO_LONGER_NEEDED("heli_gun")
						ENDIF
					ELSE
						CDEBUG2LN(DEBUG_NET_GUN_TURRET, "[POD] - MAINTAIN_TRUCK_GUN_TURRET_LAUNCHING: Script has already launched heli_gun instanceID: ", NATIVE_TO_INT(PLAYER_ID()))NET_NL()
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TRUCK_GUN_TURRET_LAUNCHING: g_iShouldLaunchTruckTurret resetting to -1")
					g_iShouldLaunchTruckTurret = -1
				ENDIF	
			ELSE
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TRUCK_GUN_TURRET_LAUNCHING: g_bCanPlayerUseTurretControls FALSE")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TRUCK_GUN_TURRET_LAUNCHING: g_iShouldLaunchTruckTurret resetting to -1 - IS_MP_PASSIVE_MODE_ENABLED")
			g_iShouldLaunchTruckTurret = -1
		ENDIF
	ELSE
//		IF IS_PLAYER_IN_CREATOR_AIRCRAFT(PLAYER_ID())
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TRUCK_GUN_TURRET_LAUNCHING: g_iInteriorTurretSeat: ", g_iInteriorTurretSeat , " g_iShouldLaunchTruckTurret: ", g_iShouldLaunchTruckTurret)
//		ENDIF
//		CDEBUG1LN(DEBUG_NET_GUN_TURRET, "MAINTAIN_TRUCK_GUN_TURRET_LAUNCHING: g_iInteriorTurretSeat: ", g_iInteriorTurretSeat , " g_iShouldLaunchTruckTurret: ", g_iShouldLaunchTruckTurret)
	ENDIF
ENDPROC

/// PURPOSE:
///   Keeps a bunch of player_control actions disabled while in a heli_gun vehicle and in a turret seat.
///   Runs all the time during freemode as a quick hack because heli_gun shuts down after the player
///   leaves the turret cam view (then restarts because they're in a seat still). This gives a ~1 second gap
///   where the player can do things they shouldn't. 
PROC DISABLE_PASSENGER_WEAPON_SELECT_WHILE_HELI_GUN_SCRIPT_NOT_RUNNING(STRUCT_HELI_GUN_LAUNCHING_DATA &sData)
	//If in valid vehlice 
		//AND that vehicle exists
		//AND ( ( in valk OR hunt ) AND in front seat ) 
		//OR ( in bombushka AND ( front seat OR turret1 OR turret2 ) ) 	
	IF sData.bInValidVehicle
	AND DOES_ENTITY_EXIST(sData.viHeli)
	AND (((GET_ENTITY_MODEL(sData.viHeli) = HUNTER 
		OR GET_ENTITY_MODEL(sData.viHeli) = VALKYRIE)
			AND GET_PED_IN_VEHICLE_SEAT(sData.viHeli, VS_FRONT_RIGHT)= PLAYER_PED_ID())
	OR (IS_VEHICLE_SUITABLE_FOR_VEHICLE_SEAT_TURRET(sData.viHeli)
		AND (GET_PED_IN_VEHICLE_SEAT(sData.viHeli, VS_BACK_LEFT)= PLAYER_PED_ID()
		OR GET_PED_IN_VEHICLE_SEAT(sData.viHeli, VS_BACK_RIGHT)= PLAYER_PED_ID() 
		OR GET_PED_IN_VEHICLE_SEAT(sData.viHeli, VS_FRONT_RIGHT)= PLAYER_PED_ID())))
		//Disable inputs.
		//The heli_gun script checks deisabled inputs for the ones it needs anyway.
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON) 
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	ENDIF
ENDPROC

PROC MAINTAIN_HELI_PASSENGER_GUN_LAUNCHING(STRUCT_HELI_GUN_LAUNCHING_DATA &sData)

	#IF IS_DEBUG_BUILD
	MAINTAIN_DEBUG_TEMP_CAM()
	#ENDIF
	
	// Set value of sData.iNumThreadsRunningHeliGunScript
	TRACK_IF_HELI_GUN_SCRIPT_IS_RUNNING(sData)
	MAINTAIN_LAUNCHING_FLAGS(sData)
	MAINAIN_BACK_SEAT_FIRST_PERSON_MODE(sData)
	MAINTAIN_PILOT_ON_JOB_HELI_GUN_RESTRICTIONS_HELP(sData)
	MAINTAIN_STRAIGHT_INTO_VALKYRIE_FIRST_PEROSN_MODE_DATA(sData)
	//Fix for: url:bugstar:3894569
	DISABLE_PASSENGER_WEAPON_SELECT_WHILE_HELI_GUN_SCRIPT_NOT_RUNNING(sData)
	// Main switch.
	SWITCH sData.eState
		
		CASE eHELIGUNLAUNCHINGSTATE_OFF
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF sData.bShouldHaveAccessToHeliGun
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[WJK] - heli gun launching - player should have access to heli gun.")NET_NL()
					SET_HELI_LAUNCHING_STATE(sData, eHELIGUNLAUNCHINGSTATE_LAUNCHING)
					//KILL_INTERACTION_MENU()
				ENDIF
			ENDIF
			
		BREAK
		
		CASE eHELIGUNLAUNCHINGSTATE_LAUNCHING
			
			IF NOT SHOULD_HELI_GUN_LAUNCHING_RESET(sData)
				
				IF sData.iNumThreadsRunningHeliGunScript = 0
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[WJK] - heli gun launching - not running heli gun script yet.")NET_NL()
					
					IF NOT sData.bInitialisedScriptLaunch
						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[WJK] - heli gun launching - not yet initialised heli gun script launch.")NET_NL()
						IF FREEMODE_LAUNCH_HELI_GUN_SCRIPT(sData)
							CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[WJK] - heli gun launching - initiated launch of heli gun script.")NET_NL()
							SET_HELI_LAUNCHING_STATE(sData, eHELIGUNLAUNCHINGSTATE_RUNNING)
							sData.bInitialisedScriptLaunch = TRUE
						ENDIF
					ENDIF
					
				ELSE
					
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[WJK] - heli gun launching - running heli gun script.")NET_NL()
					SET_HELI_LAUNCHING_STATE(sData, eHELIGUNLAUNCHINGSTATE_RUNNING)
					
				ENDIF
				
			ELSE
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[WJK] - heli gun launching - I should no longer have access to the heli gun.")NET_NL()
				SET_HELI_LAUNCHING_STATE(sData, eHELIGUNLAUNCHINGSTATE_RESET)
				
			ENDIF
			
		BREAK
		
		CASE eHELIGUNLAUNCHINGSTATE_RUNNING
		
			IF sData.iNumThreadsRunningHeliGunScript = 0
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[WJK] - heli gun launching - no longer running heli gun script.")NET_NL()
				g_bTerminateHeliGunScript = TRUE
				SET_HELI_LAUNCHING_STATE(sData, eHELIGUNLAUNCHINGSTATE_RESET)
			ENDIF
			
			IF SHOULD_HELI_GUN_LAUNCHING_RESET(sData)
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[WJK] - heli gun launching - I should no longer have access to the heli gun.")NET_NL()
				g_bTerminateHeliGunScript = TRUE
				SET_HELI_LAUNCHING_STATE(sData, eHELIGUNLAUNCHINGSTATE_RESET)
			ENDIF
			
		BREAK
		
		CASE eHELIGUNLAUNCHINGSTATE_RESET
			
			// Reset.
			IF sData.iNumThreadsRunningHeliGunScript = 0
				CDEBUG1LN(DEBUG_NET_GUN_TURRET, "[WJK] - heli gun launching - no longer running heli gun script.")NET_NL()
				sData.bInitialisedScriptLaunch = FALSE
				g_bTerminateHeliGunScript = FALSE
				sData.bShownPilotRestrictionsHelpText = FALSE
				SET_HELI_LAUNCHING_STATE(sData, eHELIGUNLAUNCHINGSTATE_OFF)
			ENDIF
			
		BREAK
		
	ENDSWITCH

ENDPROC





















