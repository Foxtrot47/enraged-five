USING "net_beam_hack_2d_vectors.sch"
USING "net_common_hacking_base.sch"

CONST_FLOAT cfBASE_SCREEN_HEIGHT				1080.0
CONST_FLOAT cfBASE_SCREEN_WIDTH					1920.0

CONST_INT ciVH_STARTUP_TIMER 4000
CONST_INT ciVH_MAX_TIME  120000
CONST_INT ciVH_CHECKING_TIME 4000
CONST_INT ciVH_PRE_CHECK_TIME 2000
CONST_INT ciVH_END_DELAY 4000

// BITSETS
INT iVoltageBS

ENUM VOLTAGE_HACK_FLAGS

	VH_BIT_USING_KEYS = BIT0,
	VH_BIT_TOP_MOD_LINKED = BIT1,
	VH_BIT_MIDDLE_MOD_LINKED = BIT2,
	VH_BIT_BOTTOM_MOD_LINKED = BIT3,
	VH_BIT_SHOWN_HELP = BIT4,
	VH_BIT_QUIT_ONCE = BIT5,
	
	VH_BIT_SHOW_SET = BIT6,
	
	VH_BIT_TOP_NUM_LINKED = BIT7,
	VH_BIT_MIDDLE_NUM_LINKED = BIT8,
	VH_BIT_BOTTOM_NUM_LINKED = BIT9,
	
	VH_BIT_PLAY_SUCCESS_SOUND = BIT10,
	VH_BIT_PLAY_FAIL_SOUND = BIT11,
	
	VH_BIT_SUCCESS_BINK_SOUND = BIT12

ENDENUM

// RGBA Colours
HG_RGBA_COLOUR_STRUCT rgbaBackground
HG_RGBA_COLOUR_STRUCT rgbaBase
HG_RGBA_COLOUR_STRUCT rgbaOverlay

// Grid nodes system
VECTOR_2D gridNodes[13][11]

// Integers
INT iAttemptCounter

// Floats
FLOAT fTimer
FLOAT fAnimTimer
FLOAT fAbortTimer

FUNC VECTOR_2D VOLTAGE_CONVERT_TO_VECTOR_2D_TO_SCREENSPACE(VECTOR_2D vPixelSpace)
	RETURN INIT_VECTOR_2D(vPixelSpace.x * (1 / cfBASE_SCREEN_WIDTH), vPixelSpace.y * (1 / cfBASE_SCREEN_HEIGHT))
ENDFUNC

FUNC VECTOR_2D VOLTAGE_CONVERT_TO_VECTOR_2D_TO_PIXELSPACE(VECTOR_2D vScreenSpace)
	RETURN INIT_VECTOR_2D(vScreenSpace.x * cfBASE_SCREEN_WIDTH, vScreenSpace.y * cfBASE_SCREEN_HEIGHT)
ENDFUNC

/// PURPOSE:
///    Calculates the screen aspect ratio based on the current resolution.
FUNC FLOAT VOLTAGE_GET_CUSTOM_SCREEN_ASPECT_RATIO()
	FLOAT fLocalAspectRatio = GET_ASPECT_RATIO(FALSE)
	
	//Fix for Eyefinity triple monitors: the resolution returns as the full three-screen width but the game is only drawn on one screen.
	IF IS_PC_VERSION()
		IF fLocalAspectRatio >= 4.0
			fLocalAspectRatio = fLocalAspectRatio / 3.0
		ENDIF
	ENDIF
	
	RETURN fLocalAspectRatio
	
ENDFUNC

PROC VOLTAGE_CONVERT_X_ASPECT(FLOAT &fXPos)	
	FLOAT fLocalAspectRatio = VOLTAGE_GET_CUSTOM_SCREEN_ASPECT_RATIO()
	
	IF fLocalAspectRatio < (cfBASE_SCREEN_WIDTH/cfBASE_SCREEN_HEIGHT)
		fXPos = (fXPos/(cfBASE_SCREEN_WIDTH/cfBASE_SCREEN_HEIGHT))*(fLocalAspectRatio)
	ENDIF
ENDPROC

FUNC FLOAT VOLTAGE_APPLY_ASPECT_MOD_TO_X(FLOAT fX)
	fX = ((fX * cfBASE_SCREEN_WIDTH) - ((cfBASE_SCREEN_WIDTH - cfBASE_SCREEN_HEIGHT) / 2)) / cfBASE_SCREEN_HEIGHT

	fX =  0.5 - ((0.5 - fX) / VOLTAGE_GET_CUSTOM_SCREEN_ASPECT_RATIO())
	RETURN fX
ENDFUNC

FUNC FLOAT VOLTAGE_GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO()
	RETURN ((cfBASE_SCREEN_WIDTH/cfBASE_SCREEN_HEIGHT) / VOLTAGE_GET_CUSTOM_SCREEN_ASPECT_RATIO())
ENDFUNC

PROC VOLTAGE_CONVERT_PIXELSPACE_TO_SCREENSPACE(VECTOR_2D &vPos, VECTOR_2D &vScale)

	vPos = INIT_VECTOR_2D(vPos.x * (1 / cfBASE_SCREEN_WIDTH), vPos.y * (1 / cfBASE_SCREEN_HEIGHT))
	vScale = INIT_VECTOR_2D(vScale.x * (1 / cfBASE_SCREEN_WIDTH), vScale.y * (1 / cfBASE_SCREEN_HEIGHT))
		
	vPos.x = VOLTAGE_APPLY_ASPECT_MOD_TO_X(vPos.x)
	vScale.X *= VOLTAGE_GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO()
	
ENDPROC


/// PURPOSE:
///    Draws a sprite from screenspace values
///    Note: Use ARCADE_CABINET_DRAW_SPRITE_ROTATED for non-square rotated sprites
PROC VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED(STRING stTextureDict, STRING stTexture, VECTOR_2D vCenter, VECTOR_2D vScale, FLOAT fRotation, HG_RGBA_COLOUR_STRUCT rgba)

	IF IS_STRING_NULL_OR_EMPTY(stTextureDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[VOLTAGE_HACK] [BAZ] Invalid Dictionary string passed. SPRITE will not display")
		EXIT
	ENDIF

	IF IS_STRING_NULL_OR_EMPTY(stTexture)	
		CDEBUG1LN(DEBUG_MINIGAME, "[VOLTAGE_HACK] [BAZ] Invalid Texture string passed. SPRITE will not display")
		EXIT
	ENDIF

	// Check that the streamed texture dictionary has actually loaded.	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(stTextureDict)
		CDEBUG1LN(DEBUG_MINIGAME, "[VOLTAGE_HACK] [BAZ] Invalid Dictionary string passed. SPRITE will not display")
		DEBUG_PRINTCALLSTACK()
		EXIT
	ENDIF	

#IF IS_DEBUG_BUILD

	IF g_bBlockArcadeCabinetDrawing
		EXIT
	ENDIF

	IF g_bMinimizeArcadeCabinetDrawing
		DRAW_SPRITE(stTextureDict, stTexture, 
			VOLTAGE_APPLY_ASPECT_MOD_TO_X(vCenter.X) * g_fMinimizeArcadeCabinetCentreX,
			vCenter.Y * g_fMinimizeArcadeCabinetCentreY, 
			vScale.X * (1.778 / VOLTAGE_GET_CUSTOM_SCREEN_ASPECT_RATIO()) * g_fMinimizeArcadeCabinetCentreScale, 
			vScale.Y * g_fMinimizeArcadeCabinetCentreScale, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
	ELSE
#ENDIF
		// DO REGARDLESS OF WHETHER BOOLEAN IS SET
		VOLTAGE_CONVERT_PIXELSPACE_TO_SCREENSPACE(vCenter, vScale)
		
		DRAW_SPRITE(stTextureDict, stTexture, vCenter.X, vCenter.Y, vScale.X, vScale.Y, fRotation, rgba.iR, rgba.iG, rgba.iB, rgba.iA)
		
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF
ENDPROC

/// PURPOSE:
///    Draws a rect from screenspace values
PROC VOLTAGE_DRAW_RECT_FROM_VECTOR_2D(VECTOR_2D vCenter, VECTOR_2D vScale, INT iR, INT iG, INT iB, INT iA)

#IF IS_DEBUG_BUILD 
	IF g_bBlockArcadeCabinetDrawing
		EXIT
	ENDIF
#ENDIF

#IF IS_DEBUG_BUILD
	IF g_bMinimizeArcadeCabinetDrawing
		DRAW_RECT(vCenter.x * g_fMinimizeArcadeCabinetCentreX, vCenter.y * g_fMinimizeArcadeCabinetCentreY, vScale.x * g_fMinimizeArcadeCabinetCentreScale, vScale.y * g_fMinimizeArcadeCabinetCentreScale, iR, iG, iB, iA)
	ELSE
#ENDIF
		DRAW_RECT(vCenter.x, vCenter.y, vScale.x, vScale.y, iR, iG, iB, iA)
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF

ENDPROC

PROC VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(VECTOR_2D vCenter, VECTOR_2D vScale, INT iR, INT iG, INT iB, INT iA)

#IF IS_DEBUG_BUILD 
	IF g_bBlockArcadeCabinetDrawing
		EXIT
	ENDIF
#ENDIF

	VOLTAGE_CONVERT_PIXELSPACE_TO_SCREENSPACE(vCenter, vScale)

#IF IS_DEBUG_BUILD
	IF g_bMinimizeArcadeCabinetDrawing
		DRAW_RECT(vCenter.x * g_fMinimizeArcadeCabinetCentreX, vCenter.y * g_fMinimizeArcadeCabinetCentreY, vScale.x * g_fMinimizeArcadeCabinetCentreScale, vScale.y * g_fMinimizeArcadeCabinetCentreScale, iR, iG, iB, iA)
	ELSE
#ENDIF
		DRAW_RECT(vCenter.x, vCenter.y, vScale.x, vScale.y, iR, iG, iB, iA)
#IF IS_DEBUG_BUILD
	ENDIF
#ENDIF

ENDPROC

PROC VOLTAGE_INITIALISE_GRAPH()
	INT i
	INT j
	
	FLOAT fXStart = (cfBASE_SCREEN_WIDTH/2.0) - 375.0 // take hald of grid size.
	FLOAT fXStep = (59.0+(1.0/3.0))
	
	FOR i = 0 TO 12
		FOR j = 0 TO 10
			gridNodes[i][j].x = (fXStart + (fXStep*i))
			gridNodes[i][j].y = (248.0 + ((58.0+(1.0/3.0))*j))
		ENDFOR
	ENDFOR

ENDPROC

ENUM VOLTAGE_LINE_COLOURS

	RED_VOLT,
	YELLOW_VOLT,
	BLUE_VOLT,
	COLOUR_NORMAL_COUNT,
//	ORANGE_VOLT,
//	PURPLE_VOLT,
	RED_VOLT_SET,
	YELLOW_VOLT_SET,
	BLUE_VOLT_SET

ENDENUM

ENUM VOLTAGE_TEXT_COLOURS

	GREY_TEXT,
	RED_TEXT,
	GREEN_TEXT,
	COLOUR_COUNT_TEXT

ENDENUM

PROC VOLTAGE_GET_COLOUR(VOLTAGE_LINE_COLOURS colourToGet, INT &red, INT &green, INT &blue)

	VOLTAGE_LINE_COLOURS finalColour = colourToGet

	IF colourToGet > COLOUR_NORMAL_COUNT
		finalColour = INT_TO_ENUM(VOLTAGE_LINE_COLOURS, ENUM_TO_INT(colourToGet) - (ENUM_TO_INT(COLOUR_NORMAL_COUNT) + 1))
	ENDIF

	SWITCH finalColour
		CASE RED_VOLT
			red = 255
			green = 10
			blue = 10
		BREAK
		
		CASE YELLOW_VOLT
			red = 255
			green = 171
			blue = 0
		BREAK
		
		CASE BLUE_VOLT
			red = 50
			green = 30
			blue = 255
		BREAK
				
//		CASE ORANGE_VOLT
//			red = 255
//			green = 165
//			blue = 10
//		BREAK
//		
//		CASE PURPLE_VOLT
//			red = 128
//			green = 10
//			blue = 128
//		BREAK
	ENDSWITCH

	// Darken if the Line has been set.
	IF colourToGet > COLOUR_NORMAL_COUNT
		red = ROUND(red/2.0)
		green = ROUND(green/2.0)
		blue = ROUND(blue/2.0)
	ENDIF

ENDPROC

PROC VOLTAGE_DRAW_LINE_BETWEEN_NODES(VECTOR_2D vStartPoint, VECTOR_2D vEndPoint, VOLTAGE_LINE_COLOURS lineColour, BOOL bEndRect = TRUE)

	// Vary over variables
	VECTOR_2D Center = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(vStartPoint, vEndPoint), 2)
	VECTOR_2D WH
	
	INT r
	INT g
	INT b
	VOLTAGE_GET_COLOUR(lineColour, r, g, b)

	IF vStartPoint.x = vEndPoint.x
		WH.x = 4
		WH.y = ABSF(vEndPoint.y - vStartPoint.y)
	ENDIF

	IF vStartPoint.y = vEndPoint.y
		WH.x = ABSF(vEndPoint.x - vStartPoint.x)
		WH.y = 4
	ENDIF
		
	// Mainline
	VOLTAGE_CONVERT_PIXELSPACE_TO_SCREENSPACE(Center, WH)
	VOLTAGE_DRAW_RECT_FROM_VECTOR_2D(Center, WH, R, G, B, rgbaBase.iA)
	
	// Ending Rect
	VECTOR_2D xy = INIT_VECTOR_2D(vEndPoint.X, vEndPoint.Y)
	WH = INIT_VECTOR_2D(20,20)
	
	VOLTAGE_CONVERT_PIXELSPACE_TO_SCREENSPACE(xy, WH)
	
	IF bEndRect
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D(xy, WH, R, G, B, rgbaBase.iA)
	ENDIF

ENDPROC

PROC VOLTAGE_DRAW_NODE(VECTOR_2D vStartPoint, VOLTAGE_LINE_COLOURS lineColour)
	VECTOR_2D scale = INIT_VECTOR_2D(20, 20)
	VECTOR_2D startP = vStartPoint
	
	VOLTAGE_CONVERT_PIXELSPACE_TO_SCREENSPACE(startP, scale)
	
	INT r
	INT g
	INT b
		
	VOLTAGE_GET_COLOUR(lineColour, r, g, b)	
	VOLTAGE_DRAW_RECT_FROM_VECTOR_2D(startP, scale, R, G, B, rgbaBase.iA)
ENDPROC

PROC VOLTAGE_DRAW_NUMBER_NODE(VECTOR_2D vStartPoint, VOLTAGE_LINE_COLOURS lineColour)

	// Get the required colour
	INT r
	INT g
	INT b
	VOLTAGE_GET_COLOUR(lineColour, r, g, b)	

	HG_RGBA_COLOUR_STRUCT colour
	colour.iR = r
	colour.iG = g
	colour.iB = b
	colour.iA = rgbaBase.iA
	// We assume you pass in the Start node poisition so offset accordingly.
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage", "NUMBER_NODE", INIT_VECTOR_2D(vStartPoint.x-54.0, vStartPoint.y), INIT_VECTOR_2D(128, 120), 0.0, colour)

ENDPROC


PROC VOLTAGE_DRAW_MOD_NODE(VECTOR_2D vStartPoint, VOLTAGE_LINE_COLOURS lineColour)

	// Get the required colour
	INT r
	INT g
	INT b
	VOLTAGE_GET_COLOUR(lineColour, r, g, b)	

	HG_RGBA_COLOUR_STRUCT colour
	colour.iR = r
	colour.iG = g
	colour.iB = b
	colour.iA = rgbaBase.iA
	// We assume you pass in the Start node poisition so offset accordingly.
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage", "CIRCLE_NODE", INIT_VECTOR_2D(vStartPoint.x+68.0, vStartPoint.y), INIT_VECTOR_2D(164, 152), 0.0, colour)

ENDPROC

ENUM VOLTAGE_NODES

	TOP_VOLTAGE,
	MIDDLE_VOLTAGE,
	BOTTOM_VOLTAGE

ENDENUM

//ENUM NUMBER_NODES
//
//	TOP_VOLTAGE,
//	MIDDLE_VOLTAGE,
//	BOTTOM_VOLTAGE
//
//ENDENUM

PROC VOLTAGE_LINK_NODES(VOLTAGE_NODES startV, VOLTAGE_NODES endV, VOLTAGE_LINE_COLOURS lineColour = RED_VOLT)

	// Straight across
	IF startV = endV
		SWITCH startV
	
			CASE TOP_VOLTAGE
				VOLTAGE_DRAW_NODE(gridNodes[0][1], lineColour)
				VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][1], gridNodes[12][1], lineColour)
			BREAK
			
			CASE MIDDLE_VOLTAGE
				VOLTAGE_DRAW_NODE(gridNodes[0][5], lineColour)
				VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][5], gridNodes[12][5], lineColour)
			BREAK
			
			CASE BOTTOM_VOLTAGE
				VOLTAGE_DRAW_NODE(gridNodes[0][9], lineColour)
				VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][9], gridNodes[12][9], lineColour)
			BREAK
			
		ENDSWITCH
		
		EXIT
	ENDIF

	SWITCH startV
	
		CASE TOP_VOLTAGE
			VOLTAGE_DRAW_NODE(gridNodes[0][1], lineColour)
			VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][1], gridNodes[5][1], lineColour)
		BREAK
	
		CASE MIDDLE_VOLTAGE
			VOLTAGE_DRAW_NODE(gridNodes[0][5], lineColour)
			VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][5], gridNodes[6][5], lineColour)
		BREAK
	
		CASE BOTTOM_VOLTAGE
			VOLTAGE_DRAW_NODE(gridNodes[0][9], lineColour)
			VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][9], gridNodes[7][9], lineColour)
		BREAK
	
	ENDSWITCH
	
	
	SWITCH endV
		CASE TOP_VOLTAGE
			SWITCH startV
				CASE MIDDLE_VOLTAGE
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[6][5], gridNodes[6][1], lineColour)
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[6][1], gridNodes[12][1], lineColour)
				BREAK
				
				CASE BOTTOM_VOLTAGE
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[7][9], gridNodes[7][1], lineColour)
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[7][1], gridNodes[12][1], lineColour)
				BREAK
			ENDSWITCH
		BREAK
	
		CASE MIDDLE_VOLTAGE
			SWITCH startV
				CASE TOP_VOLTAGE
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[5][1], gridNodes[5][6], lineColour)
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[5][6], gridNodes[12][6], lineColour)
				BREAK
				
				CASE BOTTOM_VOLTAGE
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[7][9], gridNodes[7][5], lineColour)
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[7][5], gridNodes[12][5], lineColour)
				BREAK
			ENDSWITCH
		BREAK
	
		CASE BOTTOM_VOLTAGE
			SWITCH startV
				CASE TOP_VOLTAGE
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[5][1], gridNodes[5][9], lineColour)
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[5][9], gridNodes[12][9], lineColour)
				BREAK
				
				CASE MIDDLE_VOLTAGE
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[6][5], gridNodes[6][9], lineColour)	
					VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[6][9], gridNodes[12][9], lineColour)
				BREAK
			ENDSWITCH
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC VOLTAGE_CALCULATE_Y_VALUE(INT startingPointY, INT endPointY, INT &iMiddleY, FLOAT fMidPoint = 0.5)
	
	INT iStartEndY_Difference = ABSI(endPointY - startingPointY) //+ 1
	iMiddleY = CEIL((iStartEndY_Difference * fMidPoint) + 0.9)


	IF endPointY = 0
		iMiddleY -= 1
		EXIT
	ENDIF

	IF startingPointY >= 5
	AND endPointY >= 5
		iMiddleY += 4
		EXIT
	ENDIF
	
	IF startingPointY >= 5
	AND endPointY >= 3
		iMiddleY += 2
		EXIT
	ENDIF
	
ENDPROC

PROC VOLTAGE_LINK_NODES_NEW(VOLTAGE_NODES startV, VOLTAGE_NODES endV, VOLTAGE_LINE_COLOURS lineColour = RED_VOLT, INT iLinkCount = 0)
	INT iBoardLength = 12
		
	// Straight across
	IF startV = endV
		SWITCH startV
	
			CASE TOP_VOLTAGE
				VOLTAGE_DRAW_NODE(gridNodes[0][1], lineColour)
				VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][1], gridNodes[12][1], lineColour)
			BREAK
			
			CASE MIDDLE_VOLTAGE
				VOLTAGE_DRAW_NODE(gridNodes[0][(iLinkCount*2)+3], lineColour)
				VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][(iLinkCount*2)+3], gridNodes[12][(iLinkCount*2)+3], lineColour)
			BREAK
			
			CASE BOTTOM_VOLTAGE
				VOLTAGE_DRAW_NODE(gridNodes[0][9], lineColour)
				VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][9], gridNodes[12][9], lineColour)
			BREAK
			
		ENDSWITCH
		
		EXIT
	ENDIF

	INT middleY

	INT startingPointY
	INT endPointY
	
	// Get start node location
	SWITCH startV
	
		CASE TOP_VOLTAGE
			startingPointY = 1
		BREAK
	
		CASE MIDDLE_VOLTAGE
			startingPointY = 5
		BREAK
	
		CASE BOTTOM_VOLTAGE
			startingPointY = 9
		BREAK
	
	ENDSWITCH
	
	// Get end point location
	SWITCH endV
	
		CASE TOP_VOLTAGE
			endPointY = 1
		BREAK
	
		CASE MIDDLE_VOLTAGE
			endPointY = 5
		BREAK
	
		CASE BOTTOM_VOLTAGE
			endPointY = 9
		BREAK
		
	ENDSWITCH

	FLOAT fFirstLink = 0.5
	
	SWITCH iLinkCount
	
		CASE 1
			fFirstLink = 0.3
		BREAK
	
		CASE 2
			fFirstLink = 0.7
		BREAK
	
	ENDSWITCH
	
	VOLTAGE_CALCULATE_Y_VALUE(startingPointY, endPointY, middleY, fFirstLink)
	
	IF endPointY > startingPointY
		IF ABSI(endPointY - startingPointY) <= 4
		OR iBoardLength <= 3
			VOLTAGE_DRAW_NODE(gridNodes[0][startingPointY], lineColour)
			VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][startingPointY], gridNodes[ROUND(iBoardLength*fFirstLink)][startingPointY], lineColour)
			VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[ROUND(iBoardLength*fFirstLink)][startingPointY], gridNodes[ROUND(iBoardLength*fFirstLink)][endPointY], lineColour)
			VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[ROUND(iBoardLength*fFirstLink)][endPointY], gridNodes[iBoardLength][endPointY], lineColour)
			EXIT
		ENDIF
	ENDIF	
	
	INT iFirstLink = 3
	INT iSecondLink = 6

	SWITCH iLinkCount
	
		CASE 1
			iFirstLink = 5
			iSecondLink = 8
		BREAK
	
		CASE 2
			iFirstLink = 7
			iSecondLink = 10
		BREAK
	
	ENDSWITCH
	
	VOLTAGE_DRAW_NODE(gridNodes[0][startingPointY], lineColour)
	VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][startingPointY], gridNodes[iFirstLink][startingPointY], lineColour)
	VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[iFirstLink][startingPointY], gridNodes[iFirstLink][middleY], lineColour)
	VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[iFirstLink][middleY], gridNodes[iSecondLink][middleY], lineColour)
	VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[iSecondLink][middleY], gridNodes[iSecondLink][endPointY], lineColour)
	VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[iSecondLink][endPointY], gridNodes[iBoardLength][endPointY], lineColour)
	
ENDPROC

FLOAT fMouseX_Voltage
FLOAT fMouseY_Voltage

PROC GET_NEAREST_GRID_COORD_TO_MOUSE(INT &XCoord, INT &YCoord)

	IF fMouseX_Voltage != GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
		fMouseX_Voltage= GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
		CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_USING_KEYS)
	ENDIF
	
	IF fMouseY_Voltage != GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
		fMouseY_Voltage= GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
		CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_USING_KEYS)
	ENDIF

	// Is the mouse within the bounds of the screen?
	
	FLOAT minX = gridNodes[0][0].x * (1 / cfBASE_SCREEN_WIDTH)
	FLOAT minY = gridNodes[0][0].y * (1 / cfBASE_SCREEN_HEIGHT)
	FLOAT maxX = gridNodes[12][10].x * (1 / cfBASE_SCREEN_WIDTH)
	FLOAT maxY = gridNodes[12][10].y * (1 / cfBASE_SCREEN_HEIGHT)
		
	// How far offset into the grid is the Mouse?
	IF fMouseX_Voltage <= maxX
		FLOAT fMouseOffsetX =  fMouseX_Voltage - minX
		FLOAT fXStepScreenspace = ((maxX - minX)/12.0)

		FLOAT fXDifference = (fMouseOffsetX % fXStepScreenspace)
		FLOAT fHalfXScreenSpace = (fXStepScreenspace/2.0)
			
		IF (fXDifference <= fHalfXScreenSpace)

			IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_USING_KEYS)	
				XCoord = FLOOR(fMouseOffsetX / fXStepScreenspace)
			ENDIF
		ELSE

			IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_USING_KEYS)	
				XCoord = CEIL(fMouseOffsetX / fXStepScreenspace)
			ENDIF
		ENDIF
	ELSE
		XCoord = 12
	ENDIF
	
	IF fMouseY_Voltage <= maxY
		FLOAT fMouseOffsetY =  fMouseY_Voltage - minY
		FLOAT fYStepScreenspace = 58.5 * (1 / cfBASE_SCREEN_HEIGHT)
		
		FLOAT fYDifference = (fMouseOffsetY % fYStepScreenspace)
		FLOAT fHalfYScreenSpace = (fYStepScreenspace/2.0)

		IF (fYDifference <= fHalfYScreenSpace)

			
			IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_USING_KEYS)	
				YCoord = FLOOR(fMouseOffsetY / fYStepScreenspace)
			ENDIF
		ELSE

			
			IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_USING_KEYS)	
				YCoord = CEIL(fMouseOffsetY / fYStepScreenspace)
			ENDIF
		ENDIF
	ELSE
		YCoord = 9
	ENDIF
	
	// Anti overrun
	IF XCoord < 0
		XCoord = 0
	ENDIF
	
	IF XCoord > 12
		XCoord = 12
	ENDIF
	
	IF YCoord < 1
		YCoord = 1
	ENDIF
	
	IF YCoord > 9
		YCoord = 9
	ENDIF
		
ENDPROC

FUNC INT GET_VOLTAGE_Y_COORD(VOLTAGE_NODES node)

	SWITCH node
	
		CASE TOP_VOLTAGE
			RETURN 1
		BREAK 
	
		CASE MIDDLE_VOLTAGE
			RETURN 5
		BREAK
		
		CASE BOTTOM_VOLTAGE
			RETURN 9
		BREAK
	
	ENDSWITCH
	
	RETURN 0

ENDFUNC

FUNC VOLTAGE_NODES GET_VOLTAGE_FROM_Y_COORD(INT iYCoord)

	SWITCH iYCoord
	
		CASE 0
		CASE 1
		CASE 2
			RETURN TOP_VOLTAGE
		BREAK 
	
		CASE 3
		CASE 4
		CASE 5
		CASE 6
		CASE 7
			RETURN MIDDLE_VOLTAGE
		BREAK
		
		CASE 8
		CASE 9
		CASE 10
			RETURN BOTTOM_VOLTAGE
		BREAK
	
	ENDSWITCH
	
	RETURN MIDDLE_VOLTAGE

ENDFUNC


// Input Helpers
STRUCT VOLTAGE_ANALOGUE_STICK_MENU_NAV

	BOOL bLeftStickMovedUp
	BOOL bLeftStickMovedDown

ENDSTRUCT

VOLTAGE_ANALOGUE_STICK_MENU_NAV voltageHackStickNavData

PROC VOLTAGE_CHECK_LEFT_STICK_NEUTRAL()
	
	IF voltageHackStickNavData.bLeftStickMovedUp
	AND GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) >= -0.1
		voltageHackStickNavData.bLeftStickMovedUp = FALSE
	ENDIF
	
	IF voltageHackStickNavData.bLeftStickMovedDown
	AND GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) <= 0.1
		voltageHackStickNavData.bLeftStickMovedDown = FALSE
	ENDIF
	
ENDPROC

FUNC BOOL VOLTAGE_INPUT_IS_UP_PRESSED(HACKING_GAME_STRUCT &sGameStruct, BOOL bSound = TRUE)

	Bool bReturn = FALSE
	
	bReturn = IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_UP)

	// Stick moved so toggle boolean
	IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < -0.1
	AND !voltageHackStickNavData.bLeftStickMovedUp
		voltageHackStickNavData.bLeftStickMovedUp = TRUE
		bReturn = true
	ENDIF

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_UP_ONLY)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
			bReturn = true
		ENDIF
		
	ENDIF
	
	IF bReturn
	AND bSound
		// Move up sfx
		PLAY_SOUND_FRONTEND(-1, "Nav_up", sGameStruct.sAudioSet)
	ENDIF
	
	return bReturn
	
ENDFUNC

FUNC BOOL VOLTAGE_INPUT_IS_DOWN_PRESSED(HACKING_GAME_STRUCT &sGameStruct, BOOL bSound = TRUE)

	BOOL bReturn = FALSE

	bReturn = IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_DOWN)
	
	// Stick moved so toggle boolean
	IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > 0.1
	AND !voltageHackStickNavData.bLeftStickMovedDown
		voltageHackStickNavData.bLeftStickMovedDown = TRUE
		bReturn = true
	ENDIF
	
	IF !bReturn
	AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_MOVE_DOWN_ONLY)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF bReturn
	AND bSound
		// Move up sfx
		PLAY_SOUND_FRONTEND(-1, "Nav_down", sGameStruct.sAudioSet)
	ENDIF

	RETURN bReturn
	
ENDFUNC

/// PURPOSE: Checks to see if player has just pressed the menu accept button
/// RETURNS: TRUE if the player has just pressed the button, FALSE if they have not
FUNC BOOL VOLTAGE_IS_MENU_ACCEPT_BUTTON_JUST_PRESSED()
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
	//OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE: Checks to see if player has just pressed the menu decline button
/// RETURNS: TRUE if the player has just pressed the button, FALSE if they have not
FUNC BOOL VOLTAGE_IS_MENU_DECLINE_BUTTON_JUST_PRESSED()
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_AIM)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL VOLTAGE_IS_NODE_LINKED(VOLTAGE_NODES voltage)

	SWITCH voltage
		CASE TOP_VOLTAGE
			IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_TOP_MOD_LINKED)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE MIDDLE_VOLTAGE
			IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_MIDDLE_MOD_LINKED)
				RETURN TRUE			
			ENDIF
		BREAK
		
		CASE BOTTOM_VOLTAGE
			IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_BOTTOM_MOD_LINKED)
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

PROC VOLTAGE_SELECT_NODE_FROM_DIRECTIONAL_INPUT(HACKING_GAME_STRUCT &sGameStruct, INT &iXCoord, INT &iYCoord)

	VOLTAGE_NODES vnNodes = GET_VOLTAGE_FROM_Y_COORD(iYCoord)
	BOOL bChanged = FALSE
	
	VOLTAGE_CHECK_LEFT_STICK_NEUTRAL()
	
	SWITCH vnNodes
		
		CASE TOP_VOLTAGE
		
			IF VOLTAGE_INPUT_IS_UP_PRESSED(sGameStruct)
				vnNodes = BOTTOM_VOLTAGE
			ENDIF
			
			IF VOLTAGE_INPUT_IS_DOWN_PRESSED(sGameStruct)
				vnNodes = MIDDLE_VOLTAGE
			ENDIF
			
			bChanged = TRUE
		BREAK
		
		CASE MIDDLE_VOLTAGE
		
			IF VOLTAGE_INPUT_IS_UP_PRESSED(sGameStruct)
				vnNodes = TOP_VOLTAGE
			ENDIF
			
			IF VOLTAGE_INPUT_IS_DOWN_PRESSED(sGameStruct)
				vnNodes = BOTTOM_VOLTAGE
			ENDIF
		
			bChanged = TRUE
		BREAK
		
		CASE BOTTOM_VOLTAGE
		
			IF VOLTAGE_INPUT_IS_UP_PRESSED(sGameStruct)
				vnNodes = MIDDLE_VOLTAGE
			ENDIF
			
			IF VOLTAGE_INPUT_IS_DOWN_PRESSED(sGameStruct)
				vnNodes = TOP_VOLTAGE
			ENDIF
		
			bChanged = TRUE
		BREAK
		
	ENDSWITCH
	
	
		
	IF VOLTAGE_INPUT_IS_UP_PRESSED(sGameStruct, FALSE)
		WHILE VOLTAGE_IS_NODE_LINKED(vnNodes)
			vnNodes = (INT_TO_ENUM(VOLTAGE_NODES, ENUM_TO_INT(vnNodes) - 1))
			
			IF ENUM_TO_INT(vnNodes) < 0
				vnNodes = BOTTOM_VOLTAGE
			ENDIF
		ENDWHILE
	ENDIF
		
	IF VOLTAGE_INPUT_IS_DOWN_PRESSED(sGameStruct, FALSE)
		WHILE VOLTAGE_IS_NODE_LINKED(vnNodes)
			vnNodes = (INT_TO_ENUM(VOLTAGE_NODES, ENUM_TO_INT(vnNodes) + 1))
			
			IF ENUM_TO_INT(vnNodes) >= 3
				vnNodes = TOP_VOLTAGE
			ENDIF
		ENDWHILE
	ENDIF
	
	
	IF bChanged
		IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_USING_KEYS)
			CDEBUG1LN(DEBUG_MINIGAME, "VOLTAGE_SELECT_NODE_FROM_DIRECTIONAL_INPUT - SETTING BITMASK VH_BIT_USING_KEYS")
			SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_USING_KEYS)
		ENDIF
	ENDIF
	
	// Only update if a button is actually pressed.
	IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_USING_KEYS)
		iXCoord = 12
		iYCoord = GET_VOLTAGE_Y_COORD(vnNodes) // Pass back into the Y coord
	ENDIF
		
ENDPROC

INT iGridSelectionX
INT iGridSelectionY

PROC VOLTAGE_LINK_DRAW_ACTIVE_LINE(HACKING_GAME_STRUCT& sGameStruct, VOLTAGE_NODES startPosition, VOLTAGE_LINE_COLOURS lineColour = RED_VOLT, INT iLinkCount = 0)

	INT iBoardLength = 12

	// PC only flair
	// Leaving unused for now
//	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
//		GET_NEAREST_GRID_COORD_TO_MOUSE(iGridSelectionX, iGridSelectionY)
//	ENDIF
		
	UNUSED_PARAMETER(startPosition)
	UNUSED_PARAMETER(iBoardLength)
		
	VOLTAGE_SELECT_NODE_FROM_DIRECTIONAL_INPUT(sGameStruct, iGridSelectionX, iGridSelectionY)
	
	VOLTAGE_DRAW_NODE(gridNodes[iGridSelectionX][iGridSelectionY], lineColour)
	
	// Straight across
	IF GET_VOLTAGE_Y_COORD(startPosition) = iGridSelectionY
		VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][iGridSelectionY], lineColour)
		VOLTAGE_DRAW_NODE(gridNodes[0][iGridSelectionY], lineColour)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][iGridSelectionY], gridNodes[iGridSelectionX][iGridSelectionY], lineColour)
		VOLTAGE_DRAW_MOD_NODE(gridNodes[iGridSelectionX][iGridSelectionY], lineColour)
		EXIT
	ENDIF
		
	//INT startY
	INT middleY
	//INT endY
		
	INT startingPointY = GET_VOLTAGE_Y_COORD(startPosition)	
	INT endPointY = iGridSelectionY
	
	FLOAT fFirstLink = 0.5
	
	SWITCH iLinkCount
	
		CASE 1
			fFirstLink = 0.3
		BREAK
	
		CASE 2
			fFirstLink = 0.7
		BREAK
	
	ENDSWITCH
		
	VOLTAGE_CALCULATE_Y_VALUE(startingPointY, iGridSelectionY, middleY, fFirstLink)
		
	IF endPointY > startingPointY
		IF ABSI(iGridSelectionY - startingPointY) <= 4
		OR iGridSelectionX <= 3
			VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][startingPointY], lineColour)
			VOLTAGE_DRAW_NODE(gridNodes[0][startingPointY], lineColour)
			VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][startingPointY], gridNodes[ROUND(iGridSelectionX*fFirstLink)][startingPointY], lineColour)
			VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[ROUND(iGridSelectionX*fFirstLink)][startingPointY], gridNodes[ROUND(iGridSelectionX*fFirstLink)][endPointY], lineColour)
			VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[ROUND(iGridSelectionX*fFirstLink)][endPointY], gridNodes[iGridSelectionX][endPointY], lineColour)
			VOLTAGE_DRAW_MOD_NODE(gridNodes[iGridSelectionX][endPointY], lineColour)
			EXIT
		ENDIF
	ENDIF
		
	INT iFirstLink = 3
	INT iSecondLink = 6

	SWITCH iLinkCount
	
		CASE 1
			iFirstLink = 5
			iSecondLink = 8
		BREAK
	
		CASE 2
			iFirstLink = 7
			iSecondLink = 10
		BREAK
	
	ENDSWITCH
	
	VOLTAGE_DRAW_NODE(gridNodes[0][startingPointY], lineColour)
	VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[0][startingPointY], gridNodes[iFirstLink][startingPointY], lineColour)
	VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[iFirstLink][startingPointY], gridNodes[iFirstLink][middleY], lineColour)
	VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[iFirstLink][middleY], gridNodes[iSecondLink][middleY], lineColour)
	VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[iSecondLink][middleY], gridNodes[iSecondLink][endPointY], lineColour)
	VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[iSecondLink][endPointY], gridNodes[iBoardLength][endPointY], lineColour)
	
	// Draw decorative nodes
	VOLTAGE_DRAW_NUMBER_NODE(gridNodes[0][startingPointY], lineColour)
	VOLTAGE_DRAW_MOD_NODE(gridNodes[iBoardLength][endPointY], lineColour)
	
ENDPROC

FUNC STRING VOLTAGE_CONVERT_INT_TO_STRING(INT iInt)
	SWITCH iInt
	
		CASE 0
			RETURN "0"
		BREAK
		
		CASE 1
			RETURN "1"
		BREAK
		
		CASE 2
			RETURN "2"
		BREAK
		
		CASE 3
			RETURN "3"
		BREAK
		
		CASE 4
			RETURN "4"
		BREAK
		
		CASE 5
			RETURN "5"
		BREAK
		
		CASE 6
			RETURN "6"
		BREAK
		
		CASE 7
			RETURN "7"
		BREAK
		
		CASE 8
			RETURN "8"
		BREAK
		
		CASE 9
			RETURN "9"
		BREAK
		
	ENDSWITCH
	
	RETURN "0"
ENDFUNC

FUNC HG_RGBA_COLOUR_STRUCT VOLTAGE_GET_TEXT_COLOUR(VOLTAGE_TEXT_COLOURS colour)

	HG_RGBA_COLOUR_STRUCT colourToUse = rgbaBase
	
	// Red or green text
	SWITCH colour
	
		CASE GREY_TEXT
			colourToUse.iA = rgbaBase.iA
			colourToUse.iR = 160
			colourToUse.iG = 160
			colourToUse.iB = 160
		BREAK
	
		CASE RED_TEXT
			colourToUse.iA = rgbaBase.iA
			colourToUse.iR = 231
			colourToUse.iG = 99
			colourToUse.iB = 99
		BREAK
		
		CASE GREEN_TEXT
			colourToUse.iA = rgbaBase.iA
			colourToUse.iR = 93
			colourToUse.iG = 234
			colourToUse.iB = 154
		BREAK
	
	ENDSWITCH
	
	RETURN colourToUse

ENDFUNC

PROC VOLTAGE_DRAW_INT_AS_SPRITES(INT iNumberToDraw, VECTOR_2D position, VOLTAGE_TEXT_COLOURS colour = RED_TEXT)
	
	VECTOR_2D vLeftNumber = position	
	vLeftNumber.x -= 90.0
	
	VECTOR_2D vRightNumber = position	
	vRightNumber.x += 90.0
	
	IF iNumberToDraw < 0
		VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage", VOLTAGE_CONVERT_INT_TO_STRING(0), vLeftNumber, INIT_VECTOR_2D(64, 104), 0.0, VOLTAGE_GET_TEXT_COLOUR(colour))
		VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage", VOLTAGE_CONVERT_INT_TO_STRING(0), position, INIT_VECTOR_2D(64, 104), 0.0, VOLTAGE_GET_TEXT_COLOUR(colour))
		VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage", VOLTAGE_CONVERT_INT_TO_STRING(0), vRightNumber, INIT_VECTOR_2D(64, 104), 0.0, VOLTAGE_GET_TEXT_COLOUR(colour))
		EXIT	
	ENDIF
			
	INT iHundreds = FLOOR(iNumberToDraw/100.0)
	INT iTens = FLOOR((iNumberToDraw - (iHundreds*100.0))/10.0) // Remove the hundreds	
	INT iSingles = iNumberToDraw - ((iHundreds*100) + (iTens*10))
	
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage", VOLTAGE_CONVERT_INT_TO_STRING(iHundreds), vLeftNumber, INIT_VECTOR_2D(64, 104), 0.0, VOLTAGE_GET_TEXT_COLOUR(colour))
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage", VOLTAGE_CONVERT_INT_TO_STRING(iTens), position, INIT_VECTOR_2D(64, 104), 0.0, VOLTAGE_GET_TEXT_COLOUR(colour))
	VOLTAGE_DRAW_PIXELSPACE_SPRITE_CORRECTED("MPIsland_Voltage", VOLTAGE_CONVERT_INT_TO_STRING(iSingles), vRightNumber, INIT_VECTOR_2D(64, 104), 0.0, VOLTAGE_GET_TEXT_COLOUR(colour))
	
ENDPROC


PROC VOLTAGE_RENDER_BATTERY_DRAIN(HG_CONTROL_STRUCT &sControllerStruct)
	// Render a dark block over battery display to show units counting down.
	
	// Just render a full battery if this is the first time through.
	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_FIRST_PLAYED)
	
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(602, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
	
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(624, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
	
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(646, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
											
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(668, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
										
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(690, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
		
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(712, 919),
										INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
	
		EXIT
	ENDIF
	
	IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_SHOW_SET)// Don't time out on first attempt
	AND NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED) // Stop the timer if you're already passing
		fTimer += (0+@1000) // Add to the timer this frame
	ENDIF
		
	IF fTimer < 20000
	
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(602, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
	
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(624, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
	
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(646, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
											
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(668, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
										
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(690, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
		
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(712, 919),
										INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
	ELIF fTimer < 40000
	
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(624, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
	
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(646, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
											
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(668, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
										
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(690, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
		
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(712, 919),
										INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
	ELIF fTimer < 60000
	
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(646, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
											
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(668, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
										
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(690, 919),
											INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)
		
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(712, 919),
										INIT_VECTOR_2D(16, 38),
											93,
											234,
											154,
											rgbaBase.iA)

											
	ELIF fTimer < 80000
	
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(668, 919),
											INIT_VECTOR_2D(16, 38),
											240,
											220,
											60,
											rgbaBase.iA)
											
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(690, 919),
											INIT_VECTOR_2D(16, 38),
											240,
											220,
											60,
											rgbaBase.iA)
			
		VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(712, 919),
											INIT_VECTOR_2D(16, 38),
											240,
											220,
											60,
											rgbaBase.iA)
	
	ELIF fTimer < 100000
											
		IF (fTimer%1000) <= 500
		
			VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(690, 919),
											INIT_VECTOR_2D(16, 38),
											240,
											10,
											10,
											rgbaBase.iA)
			
			VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(712, 919),
											INIT_VECTOR_2D(16, 38),
											240,
											10,
											10,
											rgbaBase.iA)
											
		ENDIF
											
	ELIF fTimer < ciVH_MAX_TIME
	
													
		IF (fTimer%500) <= 250
		
			VOLTAGE_DRAW_RECT_FROM_VECTOR_2D_PIXEL_CORRECTED(INIT_VECTOR_2D(712, 919),
											INIT_VECTOR_2D(16, 38),
											240,
											10,
											10,
											rgbaBase.iA)
											
		ENDIF
	ENDIF

ENDPROC

PROC VOLTAGE_SET_LINKED(VOLTAGE_NODES voltage)

	SWITCH voltage
		CASE TOP_VOLTAGE
			IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_TOP_MOD_LINKED)
				SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_TOP_MOD_LINKED)
			ENDIF
		BREAK
		
		CASE MIDDLE_VOLTAGE
			IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_MIDDLE_MOD_LINKED)
				SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_MIDDLE_MOD_LINKED)
			ENDIF
		BREAK
		
		CASE BOTTOM_VOLTAGE
			IF NOT IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_BOTTOM_MOD_LINKED)
				SET_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_BOTTOM_MOD_LINKED)
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

PROC VOLTAGE_UNLINK(VOLTAGE_NODES voltage, INT iActiveLink)

	SWITCH voltage
		CASE TOP_VOLTAGE
			IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_TOP_MOD_LINKED)
				CDEBUG1LN(DEBUG_MINIGAME, "[BAZ] VOLTAGE_UNLINK - Remove top link: VH_BIT_TOP_MOD_LINKED cleared")
				CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_TOP_MOD_LINKED)
			ENDIF
		BREAK
		
		CASE MIDDLE_VOLTAGE
			IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_MIDDLE_MOD_LINKED)
				CDEBUG1LN(DEBUG_MINIGAME, "[BAZ] VOLTAGE_UNLINK - Remove middle link: VH_BIT_MIDDLE_MOD_LINKED cleared")
				CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_MIDDLE_MOD_LINKED)
			ENDIF
		BREAK
		
		CASE BOTTOM_VOLTAGE
			IF IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_BOTTOM_MOD_LINKED)
				CDEBUG1LN(DEBUG_MINIGAME, "[BAZ] VOLTAGE_UNLINK - Remove bottom link: VH_BIT_BOTTOM_MOD_LINKED cleared")
				CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_BOTTOM_MOD_LINKED)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	SWITCH iActiveLink
					
		CASE 0
			CDEBUG1LN(DEBUG_MINIGAME, "[BAZ] VOLTAGE_UNLINK - Remove top link: VH_BIT_TOP_NUM_LINKED cleared")
			CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_TOP_NUM_LINKED)
		BREAK
		
		CASE 1
			CDEBUG1LN(DEBUG_MINIGAME, "[BAZ] VOLTAGE_UNLINK - Remove middle link: VH_BIT_MIDDLE_NUM_LINKED cleared")
			CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_MIDDLE_NUM_LINKED)
		BREAK
	
		CASE 2
			CDEBUG1LN(DEBUG_MINIGAME, "[BAZ] VOLTAGE_UNLINK - Remove bottom link: VH_BIT_BOTTOM_NUM_LINKED cleared")
			CLEAR_BITMASK_AS_ENUM(iVoltageBS, VH_BIT_BOTTOM_NUM_LINKED)
		BREAK
	
	ENDSWITCH

ENDPROC

PROC VOLTAGE_VICTORY_ANIM()

	IF (fAnimTimer % 500.0) <= 250
		//FIRST FRAME
		
		// Eyes
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[3][1], gridNodes[3][4],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[9][1], gridNodes[9][4],YELLOW_VOLT)
		
		// Mouth
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[1][6], gridNodes[1][8],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[11][6], gridNodes[11][8],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[1][8], gridNodes[11][8],YELLOW_VOLT)
	ELSE
		//SECOND FRAME

		// Eyes
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[2][1], gridNodes[2][4],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[4][1], gridNodes[4][4],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[2][1], gridNodes[4][1],YELLOW_VOLT)
		
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[8][1], gridNodes[8][4],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[10][1], gridNodes[10][4],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[8][1], gridNodes[10][1],YELLOW_VOLT)	
		
		// Mouth
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[1][6], gridNodes[11][6],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[1][6], gridNodes[1][7],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[11][6], gridNodes[11][7],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[1][7], gridNodes[2][7],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[11][7], gridNodes[10][7],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[2][7], gridNodes[2][8],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[10][7], gridNodes[10][8],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[2][8], gridNodes[3][8],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[10][8], gridNodes[9][8],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[3][8], gridNodes[3][9],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[9][8], gridNodes[9][9],YELLOW_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[3][9], gridNodes[9][9],YELLOW_VOLT)
	ENDIF
	
ENDPROC

PROC VOLTAGE_FAIL_ANIM()

	IF (fAnimTimer % 500.0) <= 250
		//FIRST FRAME
		
		// Eyes
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[3][1], gridNodes[3][4],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[9][1], gridNodes[9][4],RED_VOLT)
		
		// Mouth
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[1][6], gridNodes[1][8],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[11][6], gridNodes[11][8],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[1][6], gridNodes[11][6],RED_VOLT)
	ELSE
		//SECOND FRAME

		// Left eye
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[2][1], gridNodes[2][2],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[2][2], gridNodes[3][2],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[3][2], gridNodes[3][3],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[3][3], gridNodes[4][3],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[4][3], gridNodes[4][4],RED_VOLT)
		
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[4][1], gridNodes[4][2],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[4][2], gridNodes[3][2],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[3][2], gridNodes[3][3],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[3][3], gridNodes[2][3],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[2][3], gridNodes[2][4],RED_VOLT)
		
		// Right eye
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[8][1], gridNodes[8][2],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[8][2], gridNodes[9][2],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[9][2], gridNodes[9][3],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[9][3], gridNodes[10][3],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[10][3], gridNodes[10][4],RED_VOLT)
		
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[10][1], gridNodes[10][2],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[10][2], gridNodes[9][2],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[9][2], gridNodes[9][3],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[9][3], gridNodes[8][3],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[8][3], gridNodes[8][4],RED_VOLT)
		
//		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[2][1], gridNodes[2][2],RED_VOLT)
//		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[2][2], gridNodes[3][2],RED_VOLT)
//		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[3][2], gridNodes[3][3],RED_VOLT)
//		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[3][3], gridNodes[4][3],RED_VOLT)
//		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[4][3], gridNodes[4][4],RED_VOLT)
		
		// Mouth
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[5][6], gridNodes[7][6],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[5][6], gridNodes[5][8],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[7][6], gridNodes[7][8],RED_VOLT)
		VOLTAGE_DRAW_LINE_BETWEEN_NODES(gridNodes[5][8], gridNodes[7][8],RED_VOLT)

	ENDIF
	
ENDPROC

FUNC BOOL VOLTAGE_IS_ACTIVE_NUMBER_LINKED(INT iActiveLink)

	SWITCH iActiveLink
					
		CASE 0
			RETURN IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_TOP_NUM_LINKED)
		BREAK
		
		CASE 1
			RETURN IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_MIDDLE_NUM_LINKED)
		BREAK
	
		CASE 2
			RETURN IS_BITMASK_AS_ENUM_SET(iVoltageBS, VH_BIT_BOTTOM_NUM_LINKED)
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC BOOL VOLTAGE_PROCESS_HACKING_GAME_BINK(HG_CONTROL_STRUCT &sControllerStruct, HACKING_GAME_STRUCT &sCommonStruct, INT iDelayTime, STRING sClipName)
	UNUSED_PARAMETER(sControllerStruct)
	IF NOT HAS_NET_TIMER_STARTED(sCommonStruct.tdEndDelay)
		REINIT_NET_TIMER(sCommonStruct.tdEndDelay)
		LOAD_BINK_MOVIE(sCommonStruct, sClipName)
	ELSE
		IF MAINTAIN_BINK_MOVIE(sCommonStruct)
		OR HAS_NET_TIMER_EXPIRED(sCommonStruct.tdEndDelay, iDelayTime)
			PRINTLN("[HACKING_GAME] Fail = Moving to cleanup")
			RELEASE_BINK_MOVIE(sCommonStruct.movieId)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Handle the fades
FUNC BOOL VOLTAGE_HANDLE_ALPHA_FADE_UP()

	IF rgbaBase.iA < 255
		rgbaBase.iA	+= ROUND((0+@100)*2.0) // Double speed fade up. Should take just over a second.
	ELSE
		rgbaBase.iA	= 255
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL VOLTAGE_HANDLE_ALPHA_FADE_OUT()

	IF rgbaBase.iA > 0
		rgbaBase.iA	-= ROUND((0+@100)*2.0) // Double speed fade out. Should take just over a second.
	ELSE
		rgbaBase.iA	= 0
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
BOOL bDebugSoundMute
BOOL bFirstTime

PROC VOLTAGE_PROCESS_DEBUG_WIDGETS(HACKING_GAME_STRUCT& sGameStruct, HG_CONTROL_STRUCT &sControllerStruct)
	UNUSED_PARAMETER(sControllerStruct)
	IF NOT sGameStruct.bDebugCreatedWidgets
		PRINTLN("[MC][FingerprintCloneDebug] - Widgets not made ")

		sGameStruct.widgetFingerprintClone = GET_ID_OF_TOP_LEVEL_WIDGET_GROUP()
		SET_CURRENT_WIDGET_GROUP(sGameStruct.widgetFingerprintClone)
		START_WIDGET_GROUP("VoltageHackMinigame")
			
			// Game Values
			//ADD_WIDGET_STRING("Game Values")

			ADD_WIDGET_BOOL("STOP LOOP", bDebugSoundMute)
			ADD_WIDGET_BOOL("FIRST TIME EMULATION", bFirstTime)
		
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(sGameStruct.widgetFingerprintClone)
		sGameStruct.bDebugCreatedWidgets = TRUE
	ENDIF

	IF bFirstTime
		SET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_VOLTAGE, 0)
		bFirstTime = FALSE
	ENDIF
	
ENDPROC
#ENDIF
