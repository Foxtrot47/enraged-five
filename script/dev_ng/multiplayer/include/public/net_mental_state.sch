USING "net_stat_system.sch"
USING "net_ticker_logic.sch"

CONST_FLOAT NORMAL_MENTAL_FLOAT 20.0
CONST_FLOAT PSYCHO_MENTAL_FLOAT 80.0

PROC INCREASE_PLAYER_MENTAL_STATE(FLOAT fIncrease, BOOL bIsPlayerKill=FALSE)

	// apply the multiplier
	IF (fIncrease > 0.0)
		IF IS_THREAD_ACTIVE(MPGlobalsAmbience.MentalStateIncreaseThreadID)
			NET_PRINT("[mental_state] INCREASE_PLAYER_MENTAL_STATE - MPGlobalsAmbience.fMentalStateIncreaseMultiplier = ") NET_PRINT_FLOAT(MPGlobalsAmbience.fMentalStateIncreaseMultiplier) NET_NL()
			fIncrease *= MPGlobalsAmbience.fMentalStateIncreaseMultiplier	
		ELSE
			NET_PRINT("[mental_state] INCREASE_PLAYER_MENTAL_STATE - mental state increase thread ID no longer active.") NET_NL()
			MPGlobalsAmbience.MentalStateIncreaseThreadID = INT_TO_NATIVE(THREADID, -1)
			MPGlobalsAmbience.fMentalStateIncreaseMultiplier = 1.0
		ENDIF
	ENDIF

	// don't do anything if increase is 0
	IF (fIncrease = 0.0)
		NET_PRINT("[mental_state] INCREASE_PLAYER_MENTAL_STATE - increase is 0.0, exiting.") NET_NL()
		EXIT
	ENDIF

	// don't increase if a spectator
	IF IS_PLAYER_SCTV(PLAYER_ID())
	AND (fIncrease > 0.0)
		NET_PRINT("[mental_state] INCREASE_PLAYER_MENTAL_STATE - player is sctv, not changing.") NET_NL()
		EXIT
	ENDIF
	
	// don't increase if on ryan's 'Distract Cops' activity
	IF (fIncrease > 0.0)
	AND IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_PLAYER_IN_DISTRACT_COPS_AREA)
		NET_PRINT("[mental_state] INCREASE_PLAYER_MENTAL_STATE - player is on 'Distract Cops' activity, not increasing.") NET_NL()
		EXIT	
	ENDIF
	
	// don't increase or decrease if testing creator
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		NET_PRINT("[mental_state] INCREASE_PLAYER_MENTAL_STATE - player is using creator, not changing.") NET_NL()
		EXIT
	ENDIF	

	// only increase mental stat if off mission
	IF (fIncrease > 0.0)
	AND IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()) 
		NET_PRINT("[mental_state] INCREASE_PLAYER_MENTAL_STATE - player on mission, not incrementing.") NET_NL()
		EXIT
	ENDIF
	
//	IF IS_PLAYER_PED_BIGFOOT()
//		NET_PRINT("[mental_state] INCREASE_PLAYER_MENTAL_STATE - player is bigfoot, not incrementing.") NET_NL()
//		EXIT
//	ENDIF
	
	//FLOAT fOldTotal = GET_MP_FLOAT_PLAYER_STAT(MPPLY_PLAYER_FMENTAL_STATE)
	FLOAT fOldTotal = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PLAYER_MENTAL_STATE)
	FLOAT fNewTotal = fOldTotal + fIncrease

	IF (fNewTotal > 100.0)
		fNewTotal = 100.0
	ENDIF
	IF (fNewTotal < 0.0)
		fNewTotal = 0.0
	ENDIF
	
	INT iHighestTotal
	iHighestTotal = GET_MP_INT_CHARACTER_STAT(MP_STAT_HIGHEST_MENTAL_STATE)
	// is this the highest we've reached recently?
	IF (fNewTotal > fOldTotal)	
		IF (FLOOR(fNewTotal) > iHighestTotal)
			iHighestTotal = FLOOR(fNewTotal)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_HIGHEST_MENTAL_STATE, iHighestTotal)
			NET_PRINT("[mental_state] INCREASE_PLAYER_MENTAL_STATE - highest total increased to ") NET_PRINT_INT(iHighestTotal) NET_NL()
		ENDIF
	ENDIF
	
	// have we just returned back to zero?
	IF (fNewTotal < NORMAL_MENTAL_FLOAT)
	AND (fOldTotal >= NORMAL_MENTAL_FLOAT)
	AND (iHighestTotal >= PSYCHO_MENTAL_FLOAT)
		NET_PRINT("[mental_state] INCREASE_PLAYER_MENTAL_STATE - awarding return to normal. ") NET_NL()
		INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_MENTALSTATE_TO_NORMAL,1)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_HIGHEST_MENTAL_STATE, 0) // reset the highest stored value back to zero.
	ENDIF
	
	// should we play warning ticker?
	
	// every 1/10th increment
	IF (fIncrease > 0.0)
		IF ((fOldTotal = 0.0) AND (fNewTotal > 0.0))
		OR ((fOldTotal < 10.0) AND (fNewTotal > 10.0))
		OR ((fOldTotal < 20.0) AND (fNewTotal > 20.0))
		OR ((fOldTotal < 30.0) AND (fNewTotal > 30.0))
		OR ((fOldTotal < 40.0) AND (fNewTotal > 40.0))
		OR ((fOldTotal < 50.0) AND (fNewTotal > 50.0))
		OR ((fOldTotal < 60.0) AND (fNewTotal > 60.0))
		OR ((fOldTotal < 70.0) AND (fNewTotal > 70.0))
		OR ((fOldTotal < 80.0) AND (fNewTotal > 80.0))
		OR ((fOldTotal < 90.0) AND (fNewTotal > 90.0))
		
			PRINT_TICKER("PSYCHO_WARN1")
		
		ENDIF
		
		// send event to everyone to say player is turning psycho?
		IF ((fOldTotal < PSYCHO_MENTAL_FLOAT) AND (fNewTotal > PSYCHO_MENTAL_FLOAT))
			SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerMessage
			TickerMessage.TickerEvent = TICKER_EVENT_GOING_PSYCHO
			BROADCAST_TICKER_EVENT(TickerMessage, ALL_PLAYERS())
		ENDIF
		
	ENDIF
	
	// player kills under 12.0
	IF (bIsPlayerKill)
		IF (fOldTotal < 12.0)
		AND (fIncrease > 0.0)
			INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT6)
			IF NOT IS_BIT_SET(iStatInt, biNmh6_PsychoWarning1)				
				PRINT_TICKER("PSYCHO_WARN1")				
				// dont need to display this ticker anymore
				IF (fNewTotal > 12.0)
					SET_BIT(iStatInt, biNmh6_PsychoWarning1)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	//SET_MP_FLOAT_PLAYER_STAT(MPPLY_PLAYER_FMENTAL_STATE, fNewTotal)
	SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PLAYER_MENTAL_STATE, fNewTotal)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].fMentalState = fNewTotal
	
	NET_PRINT("[mental_state] INCREASE_PLAYER_MENTAL_STATE increased by ") NET_PRINT_FLOAT(fIncrease) NET_PRINT(" to ") NET_PRINT_FLOAT(fNewTotal) NET_NL()

ENDPROC

PROC SET_MENTAL_STATE_MULTIPLIER(FLOAT fMultiplier)
	
	IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()	
		DEBUG_PRINTCALLSTACK()
		NET_PRINT("[mental_state] SET_MENTAL_STATE_MULTIPLIER - called from within launch script. I do not like this. Neilf") NET_NL()
		SCRIPT_ASSERT("SET_MENTAL_STATE_MULTIPLIER - called from within launch script.")
		EXIT	
	ENDIF
	
	FLOAT fDiff = MPGlobalsAmbience.fMentalStateIncreaseMultiplier - fMultiplier
	
	IF IS_THREAD_ACTIVE(MPGlobalsAmbience.MentalStateIncreaseThreadID)
		IF NOT (MPGlobalsAmbience.MentalStateIncreaseThreadID = GET_ID_OF_THIS_THREAD())
		AND (ABSF(fDiff) > 0.001)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[mental_state] SET_MENTAL_STATE_MULTIPLIER - in use by another script and values differ! old value = ", MPGlobalsAmbience.fMentalStateIncreaseMultiplier, ", new value = ", fMultiplier)
			SCRIPT_ASSERT("SET_MENTAL_STATE_MULTIPLIER - in use by another script!")
			EXIT
		ENDIF
	ENDIF
	
	MPGlobalsAmbience.fMentalStateIncreaseMultiplier = fMultiplier
	MPGlobalsAmbience.MentalStateIncreaseThreadID = GET_ID_OF_THIS_THREAD()
	
	NET_PRINT("[mental_state] SET_MENTAL_STATE_MULTIPLIER - setup by ") 
	NET_PRINT(GET_THIS_SCRIPT_NAME()) 
	NET_PRINT(" fMultiplier  = ") NET_PRINT_FLOAT(fMultiplier) 
	NET_NL()	
	DEBUG_PRINTCALLSTACK()

ENDPROC

PROC RESET_MENTAL_STATE_MULTIPLIER()

	IF IS_THREAD_ACTIVE(MPGlobalsAmbience.MentalStateIncreaseThreadID)
		IF NOT (MPGlobalsAmbience.MentalStateIncreaseThreadID = GET_ID_OF_THIS_THREAD())
		AND (MPGlobalsAmbience.fMentalStateIncreaseMultiplier < 1.0)
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("[mental_state] RESET_MENTAL_STATE_MULTIPLIER - in use by another script and value is not 1.0!") NET_NL()
			//SCRIPT_ASSERT("RESET_MENTAL_STATE_MULTIPLIER - in use by another script!")
			EXIT
		ENDIF
	ENDIF
	
	MPGlobalsAmbience.MentalStateIncreaseThreadID = INT_TO_NATIVE(THREADID, -1)
	MPGlobalsAmbience.fMentalStateIncreaseMultiplier = 1.0
	
	NET_PRINT("[mental_state] RESET_MENTAL_STATE_MULTIPLIER - called by ") 
	NET_PRINT(GET_THIS_SCRIPT_NAME()) 
	NET_NL()	
	DEBUG_PRINTCALLSTACK()
	
ENDPROC

PROC SET_MENTAL_STATE_WILL_IGNORE_THIS_VEHICLE(VEHICLE_INDEX VehicleID, BOOL bSet)
	
	NET_PRINT("[mental_state] SET_MENTAL_STATE_WILL_IGNORE_THIS_VEHICLE - called by ") 
	NET_PRINT(GET_THIS_SCRIPT_NAME()) 
	NET_PRINT(" vehicle ID = ") NET_PRINT_INT(NATIVE_TO_INT(VehicleID))
	NET_PRINT(", bSet = ") NET_PRINT_BOOL(bSet)
	NET_NL()	
	DEBUG_PRINTCALLSTACK()
	
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		INT iDecoratorValue
		IF DECOR_EXIST_ON(VehicleID, "MPBitset")
			iDecoratorValue = DECOR_GET_INT(VehicleID, "MPBitset")
		ENDIF
	
		IF (bSet)
			SET_BIT(iDecoratorValue, MP_DECORATOR_BS_IGNORE_FOR_MENTAL_STATE)	
		ELSE
			CLEAR_BIT(iDecoratorValue, MP_DECORATOR_BS_IGNORE_FOR_MENTAL_STATE)	
		ENDIF
		
		DECOR_SET_INT(VehicleID, "MPBitset", iDecoratorValue)	
	ENDIF

ENDPROC

FUNC BOOL SHOULD_MENTAL_STATE_IGNORE_THIS_VEHICLE(VEHICLE_INDEX VehicleID)
	
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
	
		INT iDecoratorValue
		IF DECOR_EXIST_ON(VehicleID, "MPBitset")
			iDecoratorValue = DECOR_GET_INT(VehicleID, "MPBitset")
		ENDIF		
	
		RETURN IS_BIT_SET(iDecoratorValue, MP_DECORATOR_BS_IGNORE_FOR_MENTAL_STATE)
	
	ENDIF
	
	RETURN(FALSE)

ENDFUNC





