USING "globals.sch"
USING "net_simple_interior_common.sch"
USING "net_simple_cutscene.sch"
USING "Cutscene_help.sch"
USING "net_gang_boss.sch"

#IF FEATURE_GEN9_EXCLUSIVE
USING "net_mp_intro.sch"
#ENDIF

CONST_INT BASIC_SIMPLE_INTERIOR_ENTRY_STAGE_INIT		0
CONST_INT BASIC_SIMPLE_INTERIOR_ENTRY_RUNNING			1
CONST_INT BASIC_SIMPLE_INTERIOR_ENTRY_FINISHED			2

CONST_INT SMPL_INT_VEH_ENTRY_CAM_SHOT_ONE_TIME			5000
CONST_INT SMPL_INT_VEH_ENTRY_CAM_SHOT_TWO_TIME			3500

STRUCT SIMPLE_INTERIOR_VEH_ENTRY_SCENE_DATA
	FLOAT fStartHeading
	VECTOR vVehStartPosition
	VECTOR vVehEndPosition
	
	//Moveforward limit area
	VECTOR vLimitAreaA
	VECTOR vLimitAreaB
	FLOAT fLimitAreaWidth
	
	BOOL bVehAboveLengthLimit
	BOOL bSingleShotBikeScene
	
	//Indexes	
	INT iVecArrayIndexPrimaryVehModelMax
	INT iVecArrayIndexSavedVehCoords
	
	INT iFloatArrayIndexSavedVehSpeed
	INT iFloatArrayIndexVehMoveDistance
	INT iFloatArrayIndexVehZCoord
	
	//BS Ints
	INT iTaskedVeh
	INT iFrozenVehClone
	INT iFinishedFirstShot
ENDSTRUCT

PROC ARE_PLAYERS_NEAR_PLAYER(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR vEntryLocate, BOOL& ref_bFriends, BOOL& ref_bGang, BOOL bPlayerCanInviteFriends = TRUE, BOOL bInSameIntInstance = FALSE, BOOL bLimitZDiff = FALSE)
	INT iLocalCrewID = -1
	IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
		iLocalCrewID = GET_LOCAL_PLAYER_CLAN_ID()
	ENDIF
	
	SIMPLE_INTERIOR_TYPE eInteriorType = GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID)
	FLOAT fMaxDist2 = POW(TO_FLOAT(GET_SIMPLE_INTERIOR_INVITE_RADIUS(eSimpleInteriorID)), 2.0)
	PLAYER_INDEX piLocal = PLAYER_ID()
	VECTOR vPlayerCoords
	
	INT iLocalInstance = GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX,i)
		
		IF piLocal = piPlayer
		OR NOT NETWORK_IS_PLAYER_ACTIVE(piPlayer)
			RELOOP
		ENDIF
		
		PED_INDEX ped = GET_PLAYER_PED(piPlayer)
		IF IS_ENTITY_DEAD(ped)
			RELOOP
		ELSE
			vPlayerCoords = GET_ENTITY_COORDS(ped)
		ENDIF
		
		IF bInSameIntInstance
			IF iLocalInstance != GlobalPlayerBD[NATIVE_TO_INT(piPlayer)].SimpleInteriorBD.iInstance
				RELOOP
			ENDIF
		ENDIF
		
		IF bLimitZDiff
			IF GET_SIMPLE_INTEIROR_INVITE_NERABY_MAX_Z_DIFF(eInteriorType) != 0.0
				IF ABSF(vEntryLocate.z - vPlayerCoords.Z) > GET_SIMPLE_INTEIROR_INVITE_NERABY_MAX_Z_DIFF(eInteriorType)
					PRINTLN("[SIMPLE_INTERIOR][AUTOWARP] ARE_PLAYERS_NEAR_PLAYER - Z Diff is too great for this interior.")
					RELOOP
				ENDIF
			ENDIF
		ENDIF
			
		IF VDIST2(GET_ENTITY_COORDS(ped), vEntryLocate) <= fMaxDist2
			GAMER_HANDLE playerHandle = GET_GAMER_HANDLE_PLAYER(piPlayer)
			
			BOOL bSittingOnBlockedSeat = FALSE
			
			IF GET_PLAYER_USING_OFFICE_SEATID(piPlayer) > -1 
				IF eSimpleInteriorID = SIMPLE_INTERIOR_CASINO
				OR eSimpleInteriorID = SIMPLE_INTERIOR_CASINO_APT
					bSittingOnBlockedSeat = TRUE
				ENDIF
			ENDIF
			
			IF bPlayerCanInviteFriends
			AND NOT ref_bFriends
				IF iLocalCrewID <> -1
				AND iLocalCrewID = GET_MP_PLAYER_CLAN_ID(playerHandle)
				OR NETWORK_IS_FRIEND(playerHandle)
					IF NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(piPlayer, TRUE)
					AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(piPlayer)
					AND NOT IS_PED_IN_ANY_VEHICLE(ped, TRUE)
					AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(ped)
					AND NOT IS_PLAYER_SPECTATING(piPlayer)
					AND NOT IS_PLAYER_ON_ANY_FM_JOB(piPlayer)
					AND NOT (GB_IS_PLAYER_MEMBER_OF_A_GANG(piPlayer) AND NOT GB_ARE_PLAYERS_IN_SAME_GANG(piLocal, piPlayer) AND GET_INTERIOR_FLOOR_INDEX() != ciBUSINESS_HUB_FLOOR_NIGHTCLUB)
					AND NOT IS_PLAYER_AN_ANIMAL(piPlayer)
					AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(piPlayer)
					AND NOT bSittingOnBlockedSeat
						ref_bFriends = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT ref_bGang
			AND GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), piPlayer)
			AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(piPlayer)
			AND NOT IS_PED_IN_ANY_VEHICLE(ped, TRUE)
			AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(ped)
			AND NOT IS_PLAYER_SPECTATING(piPlayer)
			AND NOT IS_PLAYER_ON_ANY_FM_JOB(piPlayer)
			AND GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			AND NOT IS_PLAYER_AN_ANIMAL(piPlayer)
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(piPlayer)
			AND NOT bSittingOnBlockedSeat
				ref_bGang = TRUE
			ENDIF
		ENDIF
		
		// If we've found both gang members & friends close enough we don't need to search anymore.
		IF ref_bGang 
		AND ref_bFriends
			BREAKLOOP
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL ARE_ANY_PLAYERS_NEARBY(SIMPLE_INTERIORS eSimpleInteriorID, BOOL bGangOnly = FALSE, BOOL bInSameIntInstance = FALSE)
	// Check to see if the player is alone
	BOOL bFriendsNearby, bGangNearby
	ARE_PLAYERS_NEAR_PLAYER(eSimpleInteriorID, GET_ENTITY_COORDS(PLAYER_PED_ID()), bFriendsNearby, bGangNearby, TRUE, bInSameIntInstance)
	
	IF bGangOnly
		RETURN bGangNearby
	ENDIF
	
	RETURN bFriendsNearby OR bGangNearby
ENDFUNC

PROC EXT_CUTSCENE_SIMPLE_INTERIOR_CONCEAL_ENTRY_VEHICLE(BOOL bExcludeDriver = FALSE, BOOL bPreventAutoShuffle = FALSE, BOOL bConceal = TRUE)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(veh)
			IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), veh) != VS_DRIVER
			OR NOT bExcludeDriver
				IF bConceal 
					IF NOT NETWORK_IS_ENTITY_CONCEALED(veh)
						NETWORK_CONCEAL_ENTITY(veh, bConceal)
					ENDIF
				ELSE
					IF NETWORK_IS_ENTITY_CONCEALED(veh)
						NETWORK_CONCEAL_ENTITY(veh, bConceal)
					ENDIF
				ENDIF
			ENDIF
			IF bPreventAutoShuffle
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SIMPLE_INTERIOR_VEH_ENTRY_CONTROL_VEH_CLONE(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim, SIMPLE_INTERIOR_VEH_ENTRY_SCENE_DATA sEntrySceneData, INT iFadeOutTime = 500)
	IF NOT IS_ENTITY_ALIVE(entryAnim.sCutsceneVeh.vehicle)
		EXIT
	ENDIF

	FLOAT fTimeFactor, fCurrentSpeed
	
	INT iTotalSceneTime 			= g_SimpleInteriorData.sCustomCutsceneExt.sScenes[g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene].iDuration
	INT iElapsedTime 				= g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime
	INT iMaxSceneToMoveForward 		= g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount - 1
	INT iMaxTotalElapsedMoveForward = -1
		
	FLOAT fInitialSpeed 	= 1.75
	FLOAT fMaxForwardSpeed 	= 3.75
	
	IF sEntrySceneData.bVehAboveLengthLimit
		fInitialSpeed 		= 0.8
		fMaxForwardSpeed 	= 2.0
		iMaxSceneToMoveForward = g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount	
	ENDIF
	
	VECTOR vCarFront = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entryAnim.sCutsceneVeh.vehicle, entryAnim.vVectorArray[sEntrySceneData.iVecArrayIndexPrimaryVehModelMax])
		
	IF g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene < (g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount - 1)
	OR sEntrySceneData.bSingleShotBikeScene	
		IF (((g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene) < iMaxSceneToMoveForward)		
		AND IS_BIT_SET(entryAnim.iSceneBS, g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene))
			INT i
			
			iTotalSceneTime = 0
			REPEAT iMaxSceneToMoveForward i
				iTotalSceneTime += g_SimpleInteriorData.sCustomCutsceneExt.sScenes[i].iDuration
				IF i < g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene
					iElapsedTime += g_SimpleInteriorData.sCustomCutsceneExt.sScenes[i].iDuration
				ENDIF
			ENDREPEAT
			
			IF iMaxTotalElapsedMoveForward > -1
			AND iTotalSceneTime > iMaxTotalElapsedMoveForward
				iTotalSceneTime = iMaxTotalElapsedMoveForward
			ENDIF
		ENDIF
		
		IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle))
		
			PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(entryAnim.sCutsceneVeh.vehicle)
			
			IF pedDriver = NULL
				CDEBUG1LN(DEBUG_SMPL_INTERIOR, "SIMPLE_INTERIOR_VEH_ENTRY_CONTROL_VEH_CLONE - TASK_GO_TO_COORD_ANY_MEANS ped is null")
			ELIF NOT DOES_ENTITY_EXIST(pedDriver)
				CDEBUG1LN(DEBUG_SMPL_INTERIOR, "SIMPLE_INTERIOR_VEH_ENTRY_CONTROL_VEH_CLONE - TASK_GO_TO_COORD_ANY_MEANS ped is non-existent")
			ENDIF
			
			IF NOT IS_BIT_SET(entryAnim.iSceneBS, sEntrySceneData.iTaskedVeh)
				SET_VEHICLE_MAX_SPEED(entryAnim.sCutsceneVeh.vehicle, 2.0)				
				TASK_GO_TO_COORD_ANY_MEANS(pedDriver, sEntrySceneData.vVehEndPosition, 1.0, entryAnim.sCutsceneVeh.vehicle, FALSE, DRIVINGMODE_PLOUGHTHROUGH)
				SET_BIT(entryAnim.iSceneBS, sEntrySceneData.iTaskedVeh) 
			ENDIF
			
			fCurrentSpeed = GET_ENTITY_SPEED(entryAnim.sCutsceneVeh.vehicle)
		ELSE
			//Move the car forward
			fTimeFactor = FMIN(TO_FLOAT(iElapsedTime) / TO_FLOAT(iTotalSceneTime), 1.0)
			fCurrentSpeed = fInitialSpeed + GET_GRAPH_TYPE_ACCEL(fTimeFactor) * (fMaxForwardSpeed - fInitialSpeed)
			SET_VEHICLE_HANDBRAKE(entryAnim.sCutsceneVeh.vehicle, FALSE)
			SET_VEHICLE_BRAKE(entryAnim.sCutsceneVeh.vehicle, 0.0)
			SET_VEHICLE_FORWARD_SPEED(entryAnim.sCutsceneVeh.vehicle, fCurrentSpeed)
		ENDIF
		
		// Monitor if front of the car is about to hit the garage, if so move to the last scene, save the speed and position so we can maintain it
		IF IS_POINT_IN_ANGLED_AREA(vCarFront, sEntrySceneData.vLimitAreaA, sEntrySceneData.vLimitAreaB, sEntrySceneData.fLimitAreaWidth)
			IF g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene != (g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount - 1)
				PRINTLN("[CUTSCENE] SIMPLE_INTERIOR_VEH_ENTRY_CONTROL_VEH_CLONE - Front of the car was about to hit the door, moving to last scene with ID: ", (g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount - 1))		
									
				IF GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle) = MONSTER
				OR GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle) = MONSTER3
				OR GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle) = MONSTER4
				OR GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle) = MONSTER5
					IF NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(500)
						EXIT
					ENDIF
				ENDIF
				
				g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene 					= g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount - 1
				g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime 		= 0
				
				entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexSavedVehSpeed] 	= fCurrentSpeed
				entryAnim.vVectorArray[sEntrySceneData.iVecArrayIndexSavedVehCoords] 	= GET_ENTITY_COORDS(entryAnim.sCutsceneVeh.vehicle)
				
				IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle))
					CLEAR_PED_TASKS(GET_PED_IN_VEHICLE_SEAT(entryAnim.sCutsceneVeh.vehicle))
				ENDIF
				
				PRINTLN("[CUTSCENE] SIMPLE_INTERIOR_VEH_ENTRY_CONTROL_VEH_CLONE - Current speed = ", entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexSavedVehSpeed])
				PRINTLN("[CUTSCENE] SIMPLE_INTERIOR_VEH_ENTRY_CONTROL_VEH_CLONE - Current position = ", entryAnim.vVectorArray[sEntrySceneData.iVecArrayIndexSavedVehCoords])
			ELSE
				DO_SCREEN_FADE_OUT(iFadeOutTime)
			ENDIF
		ENDIF
			
	ELSE
				
		VECTOR vStartPos, vEndPos
		VECTOR vDir 				= GET_HEADING_AS_VECTOR(GET_ENTITY_HEADING(entryAnim.sCutsceneVeh.vehicle))
		FLOAT fLastRecordedSpeed 	= fMaxForwardSpeed
		
		IF entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexSavedVehSpeed] > 0.0
			vStartPos 			= entryAnim.vVectorArray[sEntrySceneData.iVecArrayIndexSavedVehCoords]
			fLastRecordedSpeed 	= entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexSavedVehSpeed]
			
			FLOAT fDistToMoveCar = (entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexVehMoveDistance] * 1.48)
			vEndPos = vStartPos + vDir * fDistToMoveCar
			
			FLOAT fTimeToMove = (fDistToMoveCar / fLastRecordedSpeed) * 1000.0
			fTimeFactor = FMIN(TO_FLOAT(g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime) / fTimeToMove, 1.0)
		ELSE
			fTimeFactor = FMIN(TO_FLOAT(g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime) / TO_FLOAT(iTotalSceneTime), 1.0)
			// Calculate endpos to maintain last speed
			sEntrySceneData.vVehEndPosition = sEntrySceneData.vVehStartPosition + vDir * (TO_FLOAT(iTotalSceneTime) / 1000.0) * fLastRecordedSpeed
		ENDIF
		
		VECTOR vTotalOffset = vEndPos - vStartPos
		VECTOR vCurrentPos = vStartPos + vTotalOffset * GET_GRAPH_TYPE_DECEL(fTimeFactor)
		
		IF NOT IS_BIT_SET(entryAnim.iSceneBS, sEntrySceneData.iFrozenVehClone)							
			SET_ENTITY_COLLISION(entryAnim.sCutsceneVeh.vehicle, FALSE)
			SET_ENTITY_COORDS(entryAnim.sCutsceneVeh.vehicle, vCurrentPos) // This will set it on ground properly
			SET_ENTITY_ROTATION(entryAnim.sCutsceneVeh.vehicle, <<0.0, 0.0, sEntrySceneData.fStartHeading>>)
			FREEZE_ENTITY_POSITION(entryAnim.sCutsceneVeh.vehicle, TRUE)
			
			VECTOR vCurrentCoords = GET_ENTITY_COORDS(entryAnim.sCutsceneVeh.vehicle)
			entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexVehZCoord] = vCurrentCoords.Z
			SET_BIT(entryAnim.iSceneBS, sEntrySceneData.iFrozenVehClone)
		ENDIF
		
		SET_ENTITY_COORDS_NO_OFFSET(entryAnim.sCutsceneVeh.vehicle, <<vCurrentPos.X, vCurrentPos.Y, entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexVehZCoord]>>)

	ENDIF
ENDPROC

PROC SIMPLE_INTERIOR_VEH_ENTRY_SINGLE_SHOT_CONTROL_VEH_CLONE(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim, SIMPLE_INTERIOR_VEH_ENTRY_SCENE_DATA sEntrySceneData)
	IF NOT IS_ENTITY_ALIVE(entryAnim.sCutsceneVeh.vehicle)
		EXIT
	ENDIF

	FLOAT fTimeFactor, fCurrentSpeed
	
	INT iTotalSceneTime 			= g_SimpleInteriorData.sCustomCutsceneExt.sScenes[g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene].iDuration
	INT iElapsedTime 				= g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime
	INT iMaxSceneToMoveForward 		= g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount
	INT iMaxTotalElapsedMoveForward = -1
		
	FLOAT fInitialSpeed 	= 1.75
	FLOAT fMaxForwardSpeed 	= 3.75
	
	IF sEntrySceneData.bVehAboveLengthLimit
		fInitialSpeed 		= 0.8
		fMaxForwardSpeed 	= 2.0
		iMaxSceneToMoveForward = g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount	
	ENDIF
	
	VECTOR vCarFront = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entryAnim.sCutsceneVeh.vehicle, entryAnim.vVectorArray[sEntrySceneData.iVecArrayIndexPrimaryVehModelMax])
		
	IF NOT IS_BIT_SET(entryAnim.iSceneBS, sEntrySceneData.iFinishedFirstShot)
		IF (((g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene) < iMaxSceneToMoveForward)		
		AND IS_BIT_SET(entryAnim.iSceneBS, g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene))
			INT i
			
			iTotalSceneTime = 0
			REPEAT iMaxSceneToMoveForward i
				iTotalSceneTime += g_SimpleInteriorData.sCustomCutsceneExt.sScenes[i].iDuration
				IF i < g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene
					iElapsedTime += g_SimpleInteriorData.sCustomCutsceneExt.sScenes[i].iDuration
				ENDIF
			ENDREPEAT
			
			IF iMaxTotalElapsedMoveForward > -1
			AND iTotalSceneTime > iMaxTotalElapsedMoveForward
				iTotalSceneTime = iMaxTotalElapsedMoveForward
			ENDIF
		ENDIF
		
		IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle))
		
			PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(entryAnim.sCutsceneVeh.vehicle)
			
			IF pedDriver = NULL
				CDEBUG1LN(DEBUG_SMPL_INTERIOR, "SIMPLE_INTERIOR_VEH_ENTRY_SINGLE_SHOT_CONTROL_VEH_CLONE - TASK_GO_TO_COORD_ANY_MEANS ped is null")
			ELIF NOT DOES_ENTITY_EXIST(pedDriver)
				CDEBUG1LN(DEBUG_SMPL_INTERIOR, "SIMPLE_INTERIOR_VEH_ENTRY_SINGLE_SHOT_CONTROL_VEH_CLONE - TASK_GO_TO_COORD_ANY_MEANS ped is non-existent")
			ENDIF
			
			IF NOT IS_BIT_SET(entryAnim.iSceneBS, sEntrySceneData.iTaskedVeh)
				SET_VEHICLE_MAX_SPEED(entryAnim.sCutsceneVeh.vehicle, 2.0)				
				TASK_GO_TO_COORD_ANY_MEANS(pedDriver, sEntrySceneData.vVehEndPosition, 1.0, entryAnim.sCutsceneVeh.vehicle, FALSE, DRIVINGMODE_PLOUGHTHROUGH)
				SET_BIT(entryAnim.iSceneBS, sEntrySceneData.iTaskedVeh) 
			ENDIF
			
			fCurrentSpeed = GET_ENTITY_SPEED(entryAnim.sCutsceneVeh.vehicle)
		ELSE
			//Move the car forward
			fTimeFactor = FMIN(TO_FLOAT(iElapsedTime) / TO_FLOAT(iTotalSceneTime), 1.0)
			fCurrentSpeed = fInitialSpeed + GET_GRAPH_TYPE_ACCEL(fTimeFactor) * (fMaxForwardSpeed - fInitialSpeed)
			SET_VEHICLE_HANDBRAKE(entryAnim.sCutsceneVeh.vehicle, FALSE)
			SET_VEHICLE_BRAKE(entryAnim.sCutsceneVeh.vehicle, 0.0)
			SET_VEHICLE_FORWARD_SPEED(entryAnim.sCutsceneVeh.vehicle, fCurrentSpeed)
		ENDIF
		
		// Monitor if front of the car is about to hit the garage, if so move to the last scene, save the speed and position so we can maintain it
		IF IS_POINT_IN_ANGLED_AREA(vCarFront, sEntrySceneData.vLimitAreaA, sEntrySceneData.vLimitAreaB, sEntrySceneData.fLimitAreaWidth)
			PRINTLN("[CUTSCENE] SIMPLE_INTERIOR_VEH_ENTRY_SINGLE_SHOT_CONTROL_VEH_CLONE - Front of the car was about to hit the door, moving to last scene with ID: ", (g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount - 1))		
			
			entryAnim.iSceneTimeAtCut												= g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime
			entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexSavedVehSpeed] 	= fCurrentSpeed
			entryAnim.vVectorArray[sEntrySceneData.iVecArrayIndexSavedVehCoords] 	= GET_ENTITY_COORDS(entryAnim.sCutsceneVeh.vehicle)
			
			IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle))
				CLEAR_PED_TASKS(GET_PED_IN_VEHICLE_SEAT(entryAnim.sCutsceneVeh.vehicle))
			ENDIF
			
			SET_BIT(entryAnim.iSceneBS, sEntrySceneData.iFinishedFirstShot)
			
			PRINTLN("[CUTSCENE][MGR_DBG] SIMPLE_INTERIOR_VEH_ENTRY_SINGLE_SHOT_CONTROL_VEH_CLONE - Current time = ", entryAnim.iSceneTimeAtCut)
			PRINTLN("[CUTSCENE][MGR_DBG] SIMPLE_INTERIOR_VEH_ENTRY_SINGLE_SHOT_CONTROL_VEH_CLONE - Current speed = ", entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexSavedVehSpeed])
			PRINTLN("[CUTSCENE][MGR_DBG] SIMPLE_INTERIOR_VEH_ENTRY_SINGLE_SHOT_CONTROL_VEH_CLONE - Current position = ", entryAnim.vVectorArray[sEntrySceneData.iVecArrayIndexSavedVehCoords])
		ENDIF
			
	ELSE
		VECTOR vStartPos, vEndPos
		VECTOR vDir 				= GET_HEADING_AS_VECTOR(GET_ENTITY_HEADING(entryAnim.sCutsceneVeh.vehicle))
		FLOAT fLastRecordedSpeed 	= fMaxForwardSpeed
		
		IF entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexSavedVehSpeed] > 0.0
			vStartPos 			= entryAnim.vVectorArray[sEntrySceneData.iVecArrayIndexSavedVehCoords]
			fLastRecordedSpeed 	= entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexSavedVehSpeed]
			
			FLOAT fDistToMoveCar = (entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexVehMoveDistance] * 1.48)
			vEndPos = vStartPos + vDir * fDistToMoveCar
					
			FLOAT fTimeToMove = (fDistToMoveCar / fLastRecordedSpeed) * 1000.0
			INT iSecondStageElapsedTime = (g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime - entryAnim.iSceneTimeAtCut)
			fTimeFactor = FMIN(TO_FLOAT(iSecondStageElapsedTime) / fTimeToMove, 1.0)
		ELSE
			fTimeFactor = FMIN(TO_FLOAT(g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime) / TO_FLOAT(iTotalSceneTime), 1.0)
			// Calculate endpos to maintain last speed
			sEntrySceneData.vVehEndPosition = sEntrySceneData.vVehStartPosition + vDir * (TO_FLOAT(iTotalSceneTime) / 1000.0) * fLastRecordedSpeed
		ENDIF
		
		VECTOR vTotalOffset = vEndPos - vStartPos
		VECTOR vCurrentPos = vStartPos + vTotalOffset * GET_GRAPH_TYPE_DECEL(fTimeFactor)
		
		IF NOT IS_BIT_SET(entryAnim.iSceneBS, sEntrySceneData.iFrozenVehClone)
			SET_ENTITY_COLLISION(entryAnim.sCutsceneVeh.vehicle, FALSE)
			SET_ENTITY_COORDS(entryAnim.sCutsceneVeh.vehicle, vCurrentPos) // This will set it on ground properly
			SET_ENTITY_ROTATION(entryAnim.sCutsceneVeh.vehicle, <<0.0, 0.0, sEntrySceneData.fStartHeading>>)
			FREEZE_ENTITY_POSITION(entryAnim.sCutsceneVeh.vehicle, TRUE)
			
			VECTOR vCurrentCoords = GET_ENTITY_COORDS(entryAnim.sCutsceneVeh.vehicle)
			entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexVehZCoord] = vCurrentCoords.Z
			SET_BIT(entryAnim.iSceneBS, sEntrySceneData.iFrozenVehClone)
		ENDIF
		
		SET_ENTITY_COORDS_NO_OFFSET(entryAnim.sCutsceneVeh.vehicle, <<vCurrentPos.X, vCurrentPos.Y, entryAnim.fStateArray[sEntrySceneData.iFloatArrayIndexVehZCoord]>>)

	ENDIF
ENDPROC

FUNC BOOL SIMPLE_INTERIOR_VEH_ENTRY_SCENE_CREATE_SCENE_NO_ZOOM(SIMPLE_CUTSCENE &cutscene, VECTOR vEstablishingShot, VECTOR vEstablishingRot, FLOAT fEstablishingFOV, VECTOR vEstablishingEndShot, VECTOR vEstablishingEndRot, FLOAT fEstablishingEndFOV, BOOL bSkipSecondShot, VECTOR vTDShot, VECTOR vTDRot, FLOAT fTDFOV, VECTOR vTDEndShot, VECTOR vTDEndRot, FLOAT fTDEndFOV, BOOL bShortenSecondCut, FLOAT fEstablishingCamShake = 0.5, FLOAT fTDCamShake = 0.1)
	CDEBUG2LN(DEBUG_PROPERTY, "SIMPLE_INTERIOR_VEH_ENTRY_SCENE_CREATE_SCENE - Setup scenes.")	
	INT iShotOneTime = SMPL_INT_VEH_ENTRY_CAM_SHOT_ONE_TIME
	INT iShotTwoTime = SMPL_INT_VEH_ENTRY_CAM_SHOT_TWO_TIME
	
	IF bShortenSecondCut
		iShotTwoTime = 2350
	ENDIF
	
	IF bSkipSecondShot
		iShotOneTime = 3750
	ENDIF
	
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, iShotOneTime, "zoom_sustain", vEstablishingShot, vEstablishingRot, fEstablishingFOV, vEstablishingEndShot, vEstablishingEndRot, fEstablishingEndFOV, fEstablishingCamShake)
	
	IF NOT bSkipSecondShot
		SIMPLE_CUTSCENE_ADD_SCENE(cutscene, iShotTwoTime, "above", vTDShot, vTDRot, fTDFOV, vTDEndShot, vTDEndRot, fTDEndFOV, fTDCamShake, 0, 1000)
	ENDIF	
			
	RETURN TRUE
ENDFUNC

FUNC BOOL SIMPLE_INTERIOR_VEH_ENTRY_SCENE_CREATE_SCENE(SIMPLE_CUTSCENE &cutscene, VECTOR vEstablishingShot, VECTOR vEstablishingRot, FLOAT fEstablishingFOV, FLOAT fZoomFOV, BOOL bSkipSecondShot, VECTOR vTDShot, VECTOR vTDRot, FLOAT fTDFOV)
	CDEBUG2LN(DEBUG_PROPERTY, "SIMPLE_INTERIOR_VEH_ENTRY_SCENE_CREATE_SCENE - Setup scenes.")	
	INT iZoomSustainTime = 4000
	
	IF bSkipSecondShot
		iZoomSustainTime = 3000
	ENDIF
	
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 100, "est", vEstablishingShot, vEstablishingRot, fEstablishingFOV, vEstablishingShot, vEstablishingRot, fEstablishingFOV, 1.0)
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 900, "zoom", vEstablishingShot, vEstablishingRot, fEstablishingFOV, vEstablishingShot, vEstablishingRot, fZoomFOV, 1.0)
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, iZoomSustainTime, "zoom_sustain", vEstablishingShot, vEstablishingRot, fZoomFOV, vEstablishingShot, vEstablishingRot, fZoomFOV, 1.0)
	
	IF NOT bSkipSecondShot
		SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 3500, "above", vTDShot, vTDRot, fTDFOV, vTDShot, vTDRot, fTDFOV, 0.1, 0, 1000)
	ENDIF	
			
	RETURN TRUE
ENDFUNC

FUNC BOOL SIMPLE_INTERIOR_ALT_VEH_ENTRY_SCENE_CREATE_SCENE(SIMPLE_CUTSCENE &cutscene, VECTOR vEstablishingShot, VECTOR vEstablishingRot, FLOAT fEstablishingFOV, FLOAT fZoomFOV, VECTOR vSecondShotStart, VECTOR vSecondShotStartRot, FLOAT vSecondShotStartFOV, VECTOR vSecondShotEnd, VECTOR vSecondShotEndRot, FLOAT vSecondShotEndFOV)
	CDEBUG2LN(DEBUG_PROPERTY, "SIMPLE_INTERIOR_ALT_VEH_ENTRY_SCENE_CREATE_SCENE - Setup scenes.")
	
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 100, "est", vEstablishingShot, vEstablishingRot, fEstablishingFOV, vEstablishingShot, vEstablishingRot, fEstablishingFOV, 0.0)
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 900, "zoom", vEstablishingShot, vEstablishingRot, fEstablishingFOV, vEstablishingShot, vEstablishingRot, fZoomFOV, 0.0, 0, 0, GRAPH_TYPE_VERY_SLOW_OUT, GRAPH_TYPE_VERY_SLOW_OUT)
	SIMPLE_CUTSCENE_ADD_SCENE(cutscene, 3500, "pan", vSecondShotStart, vSecondShotStartRot, vSecondShotStartFOV, vSecondShotEnd, vSecondShotEndRot, vSecondShotEndFOV, 0.35, 0, 1000, GRAPH_TYPE_SLOW_IN, GRAPH_TYPE_SLOW_IN)
			
	RETURN TRUE
ENDFUNC

PROC SIMPLE_INTERIOR_BUZZER_MENU_UPDATE()
	IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_USING_BUZZER_MENU)
		SET_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_BUZZER_MENU_UPDATE_FOR_NEW_REQUEST)
	ENDIF
ENDPROC

//========================== INTERIOR BUZZER HELPER FUNCTIONS ==========================

/// PURPOSE:
///    Sends an event to the specified player requesting entry to their simple interior
PROC SIMPLE_INTERIOR_BUZZER_REQUEST_ENTRY(SIMPLE_INTERIORS eInteriorID, PLAYER_INDEX pOwner)
	SCRIPT_EVENT_REQUEST_PLAYER_DATA_ARGS eArgs
	eArgs.iArray[0] = ENUM_TO_INT(eInteriorID)
	g_SimpleInteriorData.eBuzzerData.pOwner = pOwner
	g_SimpleInteriorData.eBuzzerData.bAwaitingPlayerResponse = FALSE
	PRINTLN("[SIMPLE_INTERIOR][BUZZER] SIMPLE_INTERIOR_BUZZER_REQUEST_ENTRY - Sending to player ", NATIVE_TO_INT(pOwner), " in interior ", eInteriorID)
	BROADCAST_REQUEST_PLAYER_DATA_ARGS(pOwner, PRDT_REQUEST_ENTRY_TO_SIMPLE_INTERIOR, eArgs)
ENDPROC

/// PURPOSE:
///    Tell a player that we are no longer requesting entry to their property
PROC SIMPLE_INTERIOR_BUZZER_RELEASE_ENTRY_REQUEST(PLAYER_INDEX pOwner)
	IF pOwner = INVALID_PLAYER_INDEX()
		PRINTLN("[SIMPLE_INTERIOR][BUZZER] SIMPLE_INTERIOR_BUZZER_RELEASE_ENTRY_REQUEST invalid player - Ignoring.")
		EXIT
	ENDIF
	
	SCRIPT_EVENT_REQUEST_PLAYER_DATA_ARGS eArgs
	PRINTLN("[SIMPLE_INTERIOR][BUZZER] SIMPLE_INTERIOR_BUZZER_RELEASE_ENTRY_REQUEST - Sending to player ", NATIVE_TO_INT(pOwner))
	BROADCAST_REQUEST_PLAYER_DATA_ARGS(pOwner, PRDT_RELEASE_SIMPLE_INTERIOR_ENTRY_REQUEST, eArgs)
ENDPROC

/// PURPOSE:
///    Sends an event replying to a players request for entry to their property
PROC SIMPLE_INTERIOR_BUZZER_SEND_REPLY(PLAYER_INDEX pPlayer, BOOL bAllowEntry)
	
	INT iPlayer = NATIVE_TO_INT(pPlayer)
	
	IF iPlayer <= -1
		EXIT
	ENDIF
	
	SCRIPT_EVENT_REQUEST_PLAYER_DATA_ARGS eArgs
	
	IF bAllowEntry
		eArgs.iArray[0] = RR_SIMPLE_INTERIOR_BUZZER_REPLY_ALLOW_ENTRY
		g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iReplayStatus = SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_REQUEST_ACCEPTED
	ELSE
		eArgs.iArray[0] = RR_SIMPLE_INTERIOR_BUZZER_REPLY_REJECT_ENTRY
		g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iReplayStatus = SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_REQUEST_REJECTED
	ENDIF
	
	g_SimpleInteriorData.eBuzzerReplyData[iPlayer].responseTime = GET_NETWORK_TIME()
	
	PRINTLN("[SIMPLE_INTERIOR][BUZZER] SIMPLE_INTERIOR_BUZZER_SEND_REPLY - Sending to player ", NATIVE_TO_INT(pPlayer), " reply: ", eArgs.iArray[0])
	BROADCAST_REQUEST_PLAYER_DATA_ARGS(pPlayer, PRDT_SIMPLE_INTERIOR_BUZZER_ENTRY_REPLY, eArgs)
ENDPROC

/// PURPOSE:
///    Checks the hash of the player to see if it is the same player that requested entry
///    Returns TRUE if the player is valid and the hash is correct
FUNC BOOL SIMPLE_INTERIOR_BUZZER_CHECK_STORED_PLAYER_NAME(PLAYER_INDEX player)
	INT iPlayer = NATIVE_TO_INT(player)
	
	IF iPlayer > -1
		RETURN g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iPlayerHash = GET_HASH_KEY(GET_PLAYER_NAME(player))
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the loacl player accepted the entry request of the given player if that player is valid
FUNC BOOL DID_LOCAL_PLAYER_ALLOW_BUZZER_ENTRY_FOR_PLAYER(PLAYER_INDEX pPlayer)
	IF SIMPLE_INTERIOR_BUZZER_CHECK_STORED_PLAYER_NAME(pPlayer)
		RETURN (g_SimpleInteriorData.eBuzzerReplyData[NATIVE_TO_INT(pPlayer)].iReplayStatus = SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_REQUEST_ACCEPTED)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Stores a request by another player for entry to the simple interior we are in now
FUNC BOOL SIMPLE_INTERIOR_BUZZER_STORE_BUZZER_REQUEST(PLAYER_INDEX player)
	INT iPlayer = NATIVE_TO_INT(player)
	
	IF iPlayer < -1
	OR iPlayer >= COUNT_OF(g_SimpleInteriorData.eBuzzerReplyData)
		PRINTLN("SIMPLE_INTERIOR_BUZZER_STORE_BUZZER_REQUEST [VALIDATE] - invalid player index in iPlayer: ", iPlayer)
		SCRIPT_ASSERT("SIMPLE_INTERIOR_BUZZER_STORE_BUZZER_REQUEST [VALIDATE] - invalid player index in iPlayer")
		RETURN FALSE
	ENDIF
	
	IF iPlayer != -1
		IF NETWORK_IS_PLAYER_ACTIVE(player)
			IF g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iReplayStatus = SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_NO_REQUEST
			AND NOT SIMPLE_INTERIOR_BUZZER_CHECK_STORED_PLAYER_NAME(player)
				g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iPlayerHash 		= GET_HASH_KEY(GET_PLAYER_NAME(player))
				g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iReplayStatus	= SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_AWAITING_RESPONSE
				RETURN TRUE
			ELSE
				PRINTLN("[SIMPLE_INTERIOR][BUZZER] SIMPLE_INTERIOR_BUZZER_STORE_BUZZER_REQUEST - Request from player ", iPlayer, " already stored. Response status = ", g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iReplayStatus)
			ENDIF
		ELSE
			SCRIPT_ASSERT("SIMPLE_INTERIOR_BUZZER_STORE_BUZZER_REQUEST - Request from unavailable player!")
		ENDIF
	ELSE
		SCRIPT_ASSERT("SIMPLE_INTERIOR_BUZZER_STORE_BUZZER_REQUEST - Request from invalid player!")
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if there are any players buzzing our property
FUNC BOOL ARE_ANY_SIMPLE_INTERIOR_BUZZER_REQUESTS_AWAITING_A_RESPONSE()
	INT i
	
	REPEAT NUM_NETWORK_REAL_PLAYERS() i
		IF g_SimpleInteriorData.eBuzzerReplyData[i].iReplayStatus = SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_AWAITING_RESPONSE
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Validates that the given player is okay to be stored as a buzzer request
FUNC BOOL SHOULD_PLAYER_BE_ADDED_TO_SIMPLE_INTERIOR_BUZZER_RESPONSE_MENU(PLAYER_INDEX player)
	INT iPlayer = NATIVE_TO_INT(player)
	
	IF iPlayer > -1
		IF NETWORK_IS_PLAYER_ACTIVE(player)
			IF g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iReplayStatus = SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_AWAITING_RESPONSE
			AND SIMPLE_INTERIOR_BUZZER_CHECK_STORED_PLAYER_NAME(player)
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("[SIMPLE_INTERIOR][BUZZER] SHOULD_PLAYER_BE_ADDED_TO_SIMPLE_INTERIOR_BUZZER_RESPONSE_MENU - Player ", iPlayer, " is inactive")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Clears the stored buzzer request/reply data for the specified player
PROC CLEANUP_SIMPLE_INTERIOR_BUZZER_RESPONSE_DATA(PLAYER_INDEX player)
	INT iPlayer = NATIVE_TO_INT(player)
	
	IF iPlayer > -1
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iSimpleInteriorBuzzerAcceptBS, iPlayer)
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iSimpleInteriorBuzzerRejectBS, iPlayer)
		g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iReplayStatus 	= SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_NO_REQUEST
		g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iPlayerHash		= -1
		SIMPLE_INTERIOR_BUZZER_MENU_UPDATE()
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the stored buzzer request/reply data for all players
PROC CLEANUP_ALL_SIMPLE_INTERIOR_BUZZER_RESPONSE_DATA()
	INT i
	
	REPEAT NUM_NETWORK_REAL_PLAYERS() i
		CLEANUP_SIMPLE_INTERIOR_BUZZER_RESPONSE_DATA(INT_TO_PLAYERINDEX(i))
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Replys to the given players request for entry. Send an event if the sored request is valid
PROC DENY_SIMPLE_INTERRIOR_BUZZER_ENTRY_REQUESTS(PLAYER_INDEX player)
	INT iPlayer = NATIVE_TO_INT(player)
	
	IF iPlayer > -1
		//We have a response waiting at this index
		IF g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iReplayStatus = SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_AWAITING_RESPONSE
		
			//Is this still the same player who made the original request
			IF SIMPLE_INTERIOR_BUZZER_CHECK_STORED_PLAYER_NAME(player)
				SIMPLE_INTERIOR_BUZZER_SEND_REPLY(player, FALSE)
			ELSE
				g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iReplayStatus 	= SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_NO_REQUEST
				g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iPlayerHash		= -1
			ENDIF
			
		ELIF NOT SIMPLE_INTERIOR_BUZZER_CHECK_STORED_PLAYER_NAME(player)
			g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iReplayStatus 	= SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_NO_REQUEST
			g_SimpleInteriorData.eBuzzerReplyData[iPlayer].iPlayerHash		= -1
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Denys any request for entry to the local players simple interior property
PROC DENY_ALL_SIMPLE_INTERRIOR_BUZZER_ENTRY_REQUESTS()
	INT i
	
	REPEAT NUM_NETWORK_REAL_PLAYERS() i
		PLAYER_INDEX player = INT_TO_PLAYERINDEX(i)
		DENY_SIMPLE_INTERRIOR_BUZZER_ENTRY_REQUESTS(player)
	ENDREPEAT
	
	CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_BUZZER_REQUEST_RECEIVED)
ENDPROC

//========================== INTERIOR BUZZER REQUEST HELPER FUNCTIONS ==========================

/// PURPOSE:
///    Returns TRUE if the buzzed player has responded to the local players request for entry
FUNC BOOL HAS_LOCAL_PLAYER_RECEIVED_SI_ENTRY_REQUEST_RESPONSE()
	RETURN g_SimpleInteriorData.eBuzzerData.iResponse != -1
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the player buzzed has accepted the local players request for entry
FUNC BOOL WAS_LOCAL_PLAYER_SI_ENTRY_REQUEST_ACCEPTED()
	RETURN g_SimpleInteriorData.eBuzzerData.iResponse = RR_SIMPLE_INTERIOR_BUZZER_REPLY_ALLOW_ENTRY
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the player buzzed has rejected the local players request for entry
FUNC BOOL WAS_LOCAL_PLAYER_SI_ENTRY_REQUEST_REJECTED()
	RETURN g_SimpleInteriorData.eBuzzerData.iResponse = RR_SIMPLE_INTERIOR_BUZZER_REPLY_REJECT_ENTRY
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the given player has rejected the local players request for entry
FUNC BOOL WAS_LOCAL_PLAYER_SI_ENTRY_REQUEST_REJECTED_BY_PLAYER(PLAYER_INDEX playerToCheck)
	IF WAS_LOCAL_PLAYER_SI_ENTRY_REQUEST_REJECTED()
		RETURN playerToCheck = g_SimpleInteriorData.eBuzzerData.pOwner
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Clears all buzzer entry request data
PROC CLEAR_SIMPLE_INTERIOR_BUZZER_ENTRY_REQUEST_DATA()
	SIMPLE_INTERIOR_BUZZER_REQUEST_DATA sNewStruct
	g_SimpleInteriorData.eBuzzerData = sNewStruct
	g_SimpleInteriorData.eBuzzerData.pOwner = INVALID_PLAYER_INDEX()
ENDPROC

/// PURPOSE:
///    Returns TRUE if the local player has sent a buzzer request
FUNC BOOL HAS_LOCAL_PLAYER_SENT_SIMPLE_INTERIOR_BUZZER_REQUEST()
	RETURN g_SimpleInteriorData.eBuzzerData.pOwner != INVALID_PLAYER_INDEX()
ENDFUNC

/// PURPOSE:
///    Returns the player whom we requested entry from
FUNC PLAYER_INDEX GET_PLAYER_BUZZED_BY_LOCAL_PLAYER_SIMPLE_INTERIOR()
	RETURN g_SimpleInteriorData.eBuzzerData.pOwner
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the local player has sent a buzzer request to the given player
FUNC BOOL HAS_LOCAL_PLAYER_SENT_SIMPLE_INTERIOR_BUZZER_REQUEST_TO_PLAYER(PLAYER_INDEX pOwner)
	IF pOwner = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	RETURN g_SimpleInteriorData.eBuzzerData.pOwner = pOwner
ENDFUNC

PROC SET_SIMPLE_INTERIOR_TO_TRANSITION_TO_VIA_ELEVATOR(SIMPLE_INTERIORS eInteriorID)
	IF eInteriorID != SIMPLE_INTERIOR_INVALID
		IF g_SimpleInteriorData.eInteriorToTransitionToViaElevator = SIMPLE_INTERIOR_INVALID
			PRINTLN("SET_SIMPLE_INTERIOR_TO_TRANSITION_TO_VIA_ELEVATOR - Setting transition to interior: ", eInteriorID)
			g_SimpleInteriorData.eInteriorToTransitionToViaElevator = eInteriorID
		ELSE
			SCRIPT_ASSERT("SET_SIMPLE_INTERIOR_TO_TRANSITION_TO_VIA_ELEVATOR - Interior ID already set.")
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_SIMPLE_INTERIOR_TO_TRANSITION_TO_VIA_ELEVATOR()
	PRINTLN("CLEAR_SIMPLE_INTERIOR_TO_TRANSITION_TO_VIA_ELEVATOR - Called")
	g_SimpleInteriorData.eInteriorToTransitionToViaElevator = SIMPLE_INTERIOR_INVALID
ENDPROC

FUNC BOOL SHOULD_SIMPLE_INTERIOR_TRANSITION_VIA_ELEVATOR()
	RETURN g_SimpleInteriorData.eInteriorToTransitionToViaElevator != SIMPLE_INTERIOR_INVALID
ENDFUNC

FUNC SIMPLE_INTERIORS GET_SIMPLE_INTERIOR_TO_TRANSITION_TO_VIA_ELEVATOR()
	RETURN g_SimpleInteriorData.eInteriorToTransitionToViaElevator
ENDFUNC

PROC SET_SIMPLE_INTERIOR_TO_TRANSITION_TO_FROM_EXT_MENU(SIMPLE_INTERIORS eInteriorRunningMenu, SIMPLE_INTERIORS eInteriorID)
	IF eInteriorID != SIMPLE_INTERIOR_INVALID
		IF g_SimpleInteriorData.eIntToEnterFromSeperateInteriorMenu = SIMPLE_INTERIOR_INVALID
			IF NETWORK_IS_SCRIPT_ACTIVE("AM_MP_SMPL_INTERIOR_EXT", ENUM_TO_INT(eInteriorID), TRUE)
				PRINTLN("SET_SIMPLE_INTERIOR_TO_TRANSITION_TO_FROM_EXT_MENU - Setting transition to interior: ", eInteriorID, " from interior: ", eInteriorRunningMenu)
				g_SimpleInteriorData.eIntToEnterFromSeperateInteriorMenu = eInteriorID
				g_SimpleInteriorData.eIntFromWhichSeperateIntSelected = eInteriorRunningMenu
			ELSE
				SCRIPT_ASSERT("SET_SIMPLE_INTERIOR_TO_TRANSITION_TO_FROM_EXT_MENU - Script is not running for the target interior.")
			ENDIF
		ELSE
			SCRIPT_ASSERT("SET_SIMPLE_INTERIOR_TO_TRANSITION_TO_FROM_EXT_MENU - Interior ID already set.")
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_SIMPLE_INTERIOR_TO_TRANSITION_TO_FROM_EXT_MENU()
	PRINTLN("CLEAR_SIMPLE_INTERIOR_TO_TRANSITION_TO_FROM_EXT_MENU - Called")
	g_SimpleInteriorData.eIntToEnterFromSeperateInteriorMenu = SIMPLE_INTERIOR_INVALID
ENDPROC

/// PURPOSE:
///    Allows simple interiors to support moving to an inteirior from an entirley seperate exterior script e.g. the casino and acsino apartment
FUNC BOOL SHOULD_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU(SIMPLE_INTERIORS eInteriorID)
	RETURN g_SimpleInteriorData.eIntToEnterFromSeperateInteriorMenu = eInteriorID
ENDFUNC

FUNC BOOL SHOULD_TRANSITION_TO_ANY_INTERIOR_FROM_EXT_MENU()
	RETURN g_SimpleInteriorData.eIntToEnterFromSeperateInteriorMenu != SIMPLE_INTERIOR_INVALID
ENDFUNC

FUNC SIMPLE_INTERIORS GET_SIMPLE_INTERIOR_TO_TRANSITION_TO_FROM_EXT_MENU()
	RETURN g_SimpleInteriorData.eIntToEnterFromSeperateInteriorMenu
ENDFUNC

PROC SET_LOCAL_PLAYER_SHOULD_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU_IN_VEH(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_SHOULD_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU_IN_VEH)
			SET_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_SHOULD_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU_IN_VEH)
			PRINTLN("SET_LOCAL_PLAYER_SHOULD_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU_IN_VEH - TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_SHOULD_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU_IN_VEH)
			CLEAR_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_SHOULD_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU_IN_VEH)
			PRINTLN("SET_LOCAL_PLAYER_SHOULD_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU_IN_VEH - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_LOCAL_PLAYER_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU_IN_VEH()
	RETURN IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_SHOULD_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU_IN_VEH)
ENDFUNC

PROC SET_EXT_SCRIPT_SHOULD_CLEANUP_ESTABLISING_SHOT(SIMPLE_INTERIORS eInterior, BOOL bSet)
	IF eInterior != g_SimpleInteriorData.eIntFromWhichSeperateIntSelected
	OR g_SimpleInteriorData.eIntFromWhichSeperateIntSelected = SIMPLE_INTERIOR_INVALID
		PRINTLN("SET_EXT_SCRIPT_SHOULD_CLEANUP_ESTABLISING_SHOT - eInterior = ", eInterior, " eIntFromWhichSeperateIntSelected = ", g_SimpleInteriorData.eIntFromWhichSeperateIntSelected)
		SCRIPT_ASSERT("SET_EXT_SCRIPT_SHOULD_CLEANUP_ESTABLISING_SHOT - No or invalid interior set!")
		EXIT
	ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_CLEANUP_ESTABLISHING_SHOT_FROM_EXT)
			SET_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_CLEANUP_ESTABLISHING_SHOT_FROM_EXT)
			PRINTLN("SET_EXT_SCRIPT_SHOULD_CLEANUP_ESTABLISING_SHOT - TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_CLEANUP_ESTABLISHING_SHOT_FROM_EXT)
			CLEAR_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_CLEANUP_ESTABLISHING_SHOT_FROM_EXT)
			PRINTLN("SET_EXT_SCRIPT_SHOULD_CLEANUP_ESTABLISING_SHOT - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_EXT_SCRIPT_CLEANUP_ESTABLISING_SHOT(SIMPLE_INTERIORS eInterior)
	
	IF eInterior != g_SimpleInteriorData.eIntFromWhichSeperateIntSelected
	OR eInterior = SIMPLE_INTERIOR_INVALID
		RETURN FALSE
	ENDIF
	
	RETURN IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_CLEANUP_ESTABLISHING_SHOT_FROM_EXT)
ENDFUNC

PROC SIMPLE_INTERIOR_INT_SCRIPT_MOCAP_REQUEST_EXT_ESTABLISING_SHOT_CLEANUP()
	DEBUG_PRINTCALLSTACK()
	SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_INT_MOCAP_REQUEST_EXT_CAMERA_CLEANUP)
ENDPROC

FUNC SIMPLE_INTERIORS GET_SIMPLE_INTERIOR_THAT_TRIGGERED_ENTRY_TO_THIS_INTERIOR(SIMPLE_INTERIORS eInteriorID)
	IF eInteriorID != g_SimpleInteriorData.eIntToEnterFromSeperateInteriorMenu
		RETURN SIMPLE_INTERIOR_INVALID
	ENDIF
	
	RETURN g_SimpleInteriorData.eIntFromWhichSeperateIntSelected
ENDFUNC

PROC SET_LOCAL_PLAYER_SKIP_RS_TELEPORTING_WARP(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_LP_SKIP_RS_TELEPORTING_WARP)
			SET_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_LP_SKIP_RS_TELEPORTING_WARP)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("SET_LOCAL_PLAYER_SKIP_RS_TELEPORTING_WARP - TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_LP_SKIP_RS_TELEPORTING_WARP)
			CLEAR_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_LP_SKIP_RS_TELEPORTING_WARP)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("SET_LOCAL_PLAYER_SKIP_RS_TELEPORTING_WARP - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_LOCAL_PLAYER_SKIP_RS_TELEPORTING_WARP()
	RETURN IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_LP_SKIP_RS_TELEPORTING_WARP)
ENDFUNC

FUNC BOOL LP_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU_NOT_IN_VEH(SIMPLE_INTERIORS eInteriorID)
	IF SHOULD_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU(eInteriorID)
	AND NOT SHOULD_LOCAL_PLAYER_TRANSITION_TO_THIS_INTERIOR_FROM_EXT_MENU_IN_VEH()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_SIMPLE_INTERIOR_ENTER_WITH_NEARBY_LABEL(INT iMenuIndex)
	SWITCH iMenuIndex
		CASE ciSIMPLE_INT_ENTER_ALONE 			RETURN "BWH_ENTRM_ALONE"
		CASE ciSIMPLE_INT_ENTER_WITH_ORG
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
				RETURN "BWH_ENTRM_GANG"
			ELSE
				RETURN "PROP_HEI_E_3"
			ENDIF
		BREAK
		CASE ciSIMPLE_INT_ENTER_WITH_FRIENDS 	RETURN "PROP_HEI_E_2"
	ENDSWITCH
	
	RETURN "UNKNOWN"
ENDFUNC

FUNC BOOL IS_SIMPLE_INT_ENTER_WITH_NEARBY_MENU_OPTION_SELECTABLE(INT iMenuIndex, BOOL bFriendsNearby, BOOL bGangNearby)
	SWITCH iMenuIndex
		CASE ciSIMPLE_INT_ENTER_ALONE 			RETURN TRUE
		CASE ciSIMPLE_INT_ENTER_WITH_ORG		RETURN bGangNearby
		CASE ciSIMPLE_INT_ENTER_WITH_FRIENDS 	RETURN bFriendsNearby
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_CASINO_ROOF_EXTERIOR_SPAWN_POINT_ON_FOOT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<964.2870, 58.9797, 111.5530>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 1	vSpawnPoint 	= <<964.0721, 60.4364, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 2	vSpawnPoint 	= <<963.3423, 57.6183, 111.5529>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 3	vSpawnPoint 	= <<964.8136, 61.2717, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 4	vSpawnPoint 	= <<962.4945, 56.4102, 111.5529>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 5	vSpawnPoint 	= <<961.5035, 60.6570, 111.5530>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 6	vSpawnPoint 	= <<962.7609, 59.3626, 111.5530>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 7	vSpawnPoint 	= <<962.7224, 61.2828, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 8	vSpawnPoint 	= <<961.4890, 58.8472, 111.5529>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 9	vSpawnPoint 	= <<963.8465, 62.6046, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 10	vSpawnPoint 	= <<961.3293, 57.5069, 111.5529>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 11	vSpawnPoint 	= <<963.5379, 64.0844, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 12	vSpawnPoint 	= <<959.9266, 61.5612, 111.5530>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 13	vSpawnPoint 	= <<961.0929, 62.1820, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 14	vSpawnPoint 	= <<959.7714, 59.0508, 111.5529>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 15	vSpawnPoint 	= <<962.4349, 63.0316, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 16	vSpawnPoint 	= <<959.7065, 57.4739, 111.5528>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 17	vSpawnPoint 	= <<963.9102, 65.7979, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 18	vSpawnPoint 	= <<957.4850, 63.3180, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 19	vSpawnPoint 	= <<959.2108, 63.6873, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 20	vSpawnPoint 	= <<958.4797, 61.6645, 111.5530>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 21	vSpawnPoint 	= <<961.2648, 63.5400, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 22	vSpawnPoint 	= <<958.0678, 58.2828, 111.5528>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 23	vSpawnPoint 	= <<962.3433, 65.3979, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 24	vSpawnPoint 	= <<957.6443, 59.5736, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 25	vSpawnPoint 	= <<956.7937, 60.8593, 111.5528>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 26	vSpawnPoint 	= <<958.7524, 56.8070, 111.5528>>	fSpawnHeading 	= 146.5252	RETURN TRUE	BREAK
		CASE 27	vSpawnPoint 	= <<958.6348, 60.0536, 111.5530>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 28	vSpawnPoint 	= <<957.5490, 55.1038, 111.5526>>	fSpawnHeading 	= 146.3119	RETURN TRUE	BREAK
		CASE 29	vSpawnPoint 	= <<960.8799, 67.3903, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 30	vSpawnPoint 	= <<955.5912, 64.5573, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 31	vSpawnPoint 	= <<957.2928, 65.6837, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 32	vSpawnPoint 	= <<956.0101, 62.1252, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 33	vSpawnPoint 	= <<957.9618, 66.9349, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 34	vSpawnPoint 	= <<956.1443, 58.3010, 111.5526>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 35	vSpawnPoint 	= <<958.2112, 68.6648, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 36	vSpawnPoint 	= <<953.5358, 64.8645, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 37	vSpawnPoint 	= <<956.1406, 69.1216, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 38	vSpawnPoint 	= <<955.8687, 67.8361, 111.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 39	vSpawnPoint 	= <<955.2102, 66.5096, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 40	vSpawnPoint 	= <<954.9066, 59.8987, 111.5527>>	fSpawnHeading 	= 147.9058	RETURN TRUE	BREAK
		CASE 41	vSpawnPoint 	= <<955.2508, 70.5964, 111.5532>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 42	vSpawnPoint 	= <<949.4545, 63.4966, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 43	vSpawnPoint 	= <<951.0095, 65.5424, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 44	vSpawnPoint 	= <<950.2859, 61.3399, 111.5529>>	fSpawnHeading 	= 152.9990	RETURN TRUE	BREAK
		CASE 45	vSpawnPoint 	= <<951.9775, 67.1016, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 46	vSpawnPoint 	= <<948.4278, 60.0821, 111.5529>>	fSpawnHeading 	= 155.1920	RETURN TRUE	BREAK
		CASE 47	vSpawnPoint 	= <<952.2723, 63.6054, 111.5527>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 48	vSpawnPoint 	= <<948.7949, 70.1563, 112.5510>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 49	vSpawnPoint 	= <<945.9695, 65.8314, 112.5512>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 50	vSpawnPoint 	= <<949.2806, 68.6292, 112.5537>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 51	vSpawnPoint 	= <<948.2241, 57.6969, 111.5527>>	fSpawnHeading 	= 151.4906	RETURN TRUE	BREAK
		CASE 52	vSpawnPoint 	= <<948.2211, 67.5482, 112.5530>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 53	vSpawnPoint 	= <<952.9069, 56.6578, 111.5524>>	fSpawnHeading 	= 146.5040	RETURN TRUE	BREAK
		CASE 54	vSpawnPoint 	= <<943.2110, 64.1246, 112.5499>>	fSpawnHeading 	= 146.0803	RETURN TRUE	BREAK
		CASE 55	vSpawnPoint 	= <<946.6950, 64.3657, 112.5531>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		CASE 56	vSpawnPoint 	= <<950.0883, 52.2144, 111.5526>>	fSpawnHeading 	= 152.2559	RETURN TRUE	BREAK
		CASE 57	vSpawnPoint 	= <<954.5706, 50.9862, 111.5525>>	fSpawnHeading 	= 144.6391	RETURN TRUE	BREAK
		CASE 58	vSpawnPoint 	= <<946.4670, 56.8877, 111.5529>>	fSpawnHeading 	= 154.3817	RETURN TRUE	BREAK
		CASE 59	vSpawnPoint 	= <<947.4856, 66.2685, 112.5530>>	fSpawnHeading 	= 56.1900	RETURN TRUE	BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	// NOTE : default cases of the switch statements return FALSE
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_CASINO_FRONT_DOOR_EXTERIOR_SPAWN_POINT_ON_FOOT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<924.0214, 45.9810, 80.0959>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 1	vSpawnPoint 	= <<925.0367, 47.5067, 80.0959>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 2	vSpawnPoint 	= <<922.7727, 44.3121, 80.0959>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 3	vSpawnPoint 	= <<925.6932, 49.6108, 80.0959>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 4	vSpawnPoint 	= <<921.4413, 43.2125, 80.0959>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 5	vSpawnPoint 	= <<926.1833, 51.5652, 80.0959>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 6	vSpawnPoint 	= <<922.1965, 48.3703, 80.0959>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 7	vSpawnPoint 	= <<923.5712, 49.6449, 80.0959>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 8	vSpawnPoint 	= <<921.5172, 46.3417, 80.0959>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 9	vSpawnPoint 	= <<924.0030, 51.6185, 79.8963>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 10	vSpawnPoint 	= <<920.2224, 44.8788, 80.0959>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 11	vSpawnPoint 	= <<925.1751, 52.5721, 80.0959>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 12	vSpawnPoint 	= <<919.7559, 49.6290, 79.8961>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 13	vSpawnPoint 	= <<921.3201, 51.0796, 79.8963>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 14	vSpawnPoint 	= <<919.9817, 47.7266, 79.8963>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 15	vSpawnPoint 	= <<922.5366, 53.5411, 79.8963>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 16	vSpawnPoint 	= <<917.2034, 47.0593, 79.8961>>	fSpawnHeading 	= 57.6000	RETURN TRUE	BREAK
		CASE 17	vSpawnPoint 	= <<926.5344, 54.0223, 80.0959>>	fSpawnHeading 	= 328.5122	RETURN TRUE	BREAK
		CASE 18	vSpawnPoint 	= <<910.5871, 49.3540, 79.8961>>	fSpawnHeading 	= 151.3175	RETURN TRUE	BREAK
		CASE 19	vSpawnPoint 	= <<924.6784, 54.9418, 79.8963>>	fSpawnHeading 	= 321.8538	RETURN TRUE	BREAK
		CASE 20	vSpawnPoint 	= <<912.7980, 47.3785, 79.8961>>	fSpawnHeading 	= 153.6041	RETURN TRUE	BREAK
		CASE 21	vSpawnPoint 	= <<922.3555, 56.2383, 79.8963>>	fSpawnHeading 	= 323.4198	RETURN TRUE	BREAK
		CASE 22	vSpawnPoint 	= <<916.6665, 45.3245, 79.8961>>	fSpawnHeading 	= 152.7008	RETURN TRUE	BREAK
		CASE 23	vSpawnPoint 	= <<920.2661, 57.8243, 79.8963>>	fSpawnHeading 	= 327.1542	RETURN TRUE	BREAK
		CASE 24	vSpawnPoint 	= <<926.2833, 56.9722, 79.8963>>	fSpawnHeading 	= 330.8231	RETURN TRUE	BREAK
		CASE 25	vSpawnPoint 	= <<923.6321, 57.3753, 79.8963>>	fSpawnHeading 	= 328.2698	RETURN TRUE	BREAK
		CASE 26	vSpawnPoint 	= <<928.2498, 56.0584, 80.0959>>	fSpawnHeading 	= 332.2401	RETURN TRUE	BREAK
		CASE 27	vSpawnPoint 	= <<922.7319, 59.6312, 79.8963>>	fSpawnHeading 	= 322.5325	RETURN TRUE	BREAK
		CASE 28	vSpawnPoint 	= <<918.3226, 60.3755, 79.8961>>	fSpawnHeading 	= 317.9934	RETURN TRUE	BREAK
		CASE 29	vSpawnPoint 	= <<920.8060, 60.7145, 79.8961>>	fSpawnHeading 	= 327.9775	RETURN TRUE	BREAK
		CASE 30	vSpawnPoint 	= <<915.0543, 44.9063, 79.8961>>	fSpawnHeading 	= 150.4646	RETURN TRUE	BREAK
		CASE 31	vSpawnPoint 	= <<912.9003, 45.4279, 79.8961>>	fSpawnHeading 	= 155.0189	RETURN TRUE	BREAK
		CASE 32	vSpawnPoint 	= <<916.0872, 42.9597, 79.8961>>	fSpawnHeading 	= 143.9354	RETURN TRUE	BREAK
		CASE 33	vSpawnPoint 	= <<911.2024, 47.7720, 79.8964>>	fSpawnHeading 	= 155.3554	RETURN TRUE	BREAK
		CASE 34	vSpawnPoint 	= <<918.6417, 41.6886, 80.0963>>	fSpawnHeading 	= 142.4540	RETURN TRUE	BREAK
		CASE 35	vSpawnPoint 	= <<918.5939, 43.7490, 79.8961>>	fSpawnHeading 	= 135.7811	RETURN TRUE	BREAK
		CASE 36	vSpawnPoint 	= <<925.2930, 62.4365, 79.5737>>	fSpawnHeading 	= 331.5186	RETURN TRUE	BREAK
		CASE 37	vSpawnPoint 	= <<925.4841, 60.2395, 79.8356>>	fSpawnHeading 	= 329.6761	RETURN TRUE	BREAK
		CASE 38	vSpawnPoint 	= <<923.4069, 63.1084, 79.6376>>	fSpawnHeading 	= 325.0549	RETURN TRUE	BREAK
		CASE 39	vSpawnPoint 	= <<928.2126, 60.5921, 79.5766>>	fSpawnHeading 	= 332.8898	RETURN TRUE	BREAK
		CASE 40	vSpawnPoint 	= <<921.6520, 64.8097, 79.5628>>	fSpawnHeading 	= 325.7750	RETURN TRUE	BREAK
		CASE 41	vSpawnPoint 	= <<929.6193, 58.5147, 79.9291>>	fSpawnHeading 	= 331.7317	RETURN TRUE	BREAK
		CASE 42	vSpawnPoint 	= <<913.1684, 40.8677, 79.8961>>	fSpawnHeading 	= 141.4518	RETURN TRUE	BREAK
		CASE 43	vSpawnPoint 	= <<912.3802, 43.0171, 79.8961>>	fSpawnHeading 	= 144.9305	RETURN TRUE	BREAK
		CASE 44	vSpawnPoint 	= <<914.5126, 38.9252, 79.8563>>	fSpawnHeading 	= 137.5591	RETURN TRUE	BREAK
		CASE 45	vSpawnPoint 	= <<910.4434, 42.8315, 79.8963>>	fSpawnHeading 	= 144.0880	RETURN TRUE	BREAK
		CASE 46	vSpawnPoint 	= <<917.1274, 38.2923, 80.0963>>	fSpawnHeading 	= 144.7904	RETURN TRUE	BREAK
		CASE 47	vSpawnPoint 	= <<909.9447, 45.3507, 79.8974>>	fSpawnHeading 	= 142.9947	RETURN TRUE	BREAK
		CASE 48	vSpawnPoint 	= <<909.9600, 36.8224, 79.5372>>	fSpawnHeading 	= 146.7280	RETURN TRUE	BREAK
		CASE 49	vSpawnPoint 	= <<909.5275, 39.2422, 79.6758>>	fSpawnHeading 	= 162.0746	RETURN TRUE	BREAK
		CASE 50	vSpawnPoint 	= <<910.8312, 33.7939, 79.3770>>	fSpawnHeading 	= 146.3022	RETURN TRUE	BREAK
		CASE 51	vSpawnPoint 	= <<907.1073, 38.2415, 79.5141>>	fSpawnHeading 	= 144.2117	RETURN TRUE	BREAK
		CASE 52	vSpawnPoint 	= <<913.5198, 33.5385, 79.6689>>	fSpawnHeading 	= 134.6851	RETURN TRUE	BREAK
		CASE 53	vSpawnPoint 	= <<906.9109, 41.0300, 79.6877>>	fSpawnHeading 	= 149.4363	RETURN TRUE	BREAK
		CASE 54	vSpawnPoint 	= <<926.5994, 66.2424, 78.9909>>	fSpawnHeading 	= 325.0552	RETURN TRUE	BREAK
		CASE 55	vSpawnPoint 	= <<927.9213, 64.5270, 79.1030>>	fSpawnHeading 	= 323.2315	RETURN TRUE	BREAK
		CASE 56	vSpawnPoint 	= <<924.4518, 66.6107, 79.1143>>	fSpawnHeading 	= 329.3598	RETURN TRUE	BREAK
		CASE 57	vSpawnPoint 	= <<929.8293, 63.4097, 79.0939>>	fSpawnHeading 	= 328.6109	RETURN TRUE	BREAK
		CASE 58	vSpawnPoint 	= <<933.1887, 64.5893, 78.9224>>	fSpawnHeading 	= 328.6626	RETURN TRUE	BREAK
		CASE 59	vSpawnPoint 	= <<931.6279, 61.3492, 79.4262>>	fSpawnHeading 	= 329.7645	RETURN TRUE	BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	// NOTE : default cases of the switch statements return FALSE
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_CASINO_GARAGE_DOOR_EXTERIOR_SPAWN_POINT_ON_FOOT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<935.7125, 0.7710, 77.7644>>		fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 1	vSpawnPoint 	= <<934.2730, 0.6677, 77.7644>>		fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 2	vSpawnPoint 	= <<936.9580, -0.0769, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 3	vSpawnPoint 	= <<933.6446, 1.8578, 77.7644>>		fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 4	vSpawnPoint 	= <<937.3975, -1.4726, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 5	vSpawnPoint 	= <<933.1326, -2.0737, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 6	vSpawnPoint 	= <<934.7734, -1.2830, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 7	vSpawnPoint 	= <<934.5783, -2.6454, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 8	vSpawnPoint 	= <<932.5691, -0.1218, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 9	vSpawnPoint 	= <<935.9344, -2.5826, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 10	vSpawnPoint 	= <<932.7231, -3.9223, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 11	vSpawnPoint 	= <<931.9151, -2.9457, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 12	vSpawnPoint 	= <<934.1415, -4.1674, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 13	vSpawnPoint 	= <<931.9166, -1.0247, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 14	vSpawnPoint 	= <<935.6564, -4.1572, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 15	vSpawnPoint 	= <<931.8552, -5.5122, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 16	vSpawnPoint 	= <<930.3812, -4.4871, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 17	vSpawnPoint 	= <<933.3884, -5.9307, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 18	vSpawnPoint 	= <<930.6068, -2.5251, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 19	vSpawnPoint 	= <<934.9482, -6.1534, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 20	vSpawnPoint 	= <<932.0659, -7.2615, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 21	vSpawnPoint 	= <<930.1447, -6.2657, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 22	vSpawnPoint 	= <<934.0477, -8.6760, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 23	vSpawnPoint 	= <<928.1719, -4.3595, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 24	vSpawnPoint 	= <<936.4449, -9.2537, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 25	vSpawnPoint 	= <<927.2251, -7.2683, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 26	vSpawnPoint 	= <<925.9214, -5.4871, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 27	vSpawnPoint 	= <<929.0580, -8.5637, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 28	vSpawnPoint 	= <<936.0305, -11.4261, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 29	vSpawnPoint 	= <<931.5402, -9.9470, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 30	vSpawnPoint 	= <<928.1652, -10.2493, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 31	vSpawnPoint 	= <<925.4908, -8.5805, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 32	vSpawnPoint 	= <<930.8953, -11.9554, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 33	vSpawnPoint 	= <<933.6464, -11.0328, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 34	vSpawnPoint 	= <<933.4933, -13.3052, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 35	vSpawnPoint 	= <<927.1553, -13.0672, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 36	vSpawnPoint 	= <<926.1167, -11.5367, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 37	vSpawnPoint 	= <<928.8705, -14.8647, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 38	vSpawnPoint 	= <<923.7054, -11.1116, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 39	vSpawnPoint 	= <<931.1321, -16.1201, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 40	vSpawnPoint 	= <<921.8865, -10.1871, 77.7644>>	fSpawnHeading 	= 61.1220	RETURN TRUE	BREAK
		CASE 41	vSpawnPoint 	= <<923.0490, -8.6886, 77.7644>>	fSpawnHeading 	= 48.8630	RETURN TRUE	BREAK
		CASE 42	vSpawnPoint 	= <<934.8026, -16.9447, 77.7644>>	fSpawnHeading 	= 245.8883	RETURN TRUE	BREAK
		CASE 43	vSpawnPoint 	= <<924.6136, -14.5658, 77.7644>>	fSpawnHeading 	= 151.9227	RETURN TRUE	BREAK
		CASE 44	vSpawnPoint 	= <<933.7977, -18.5146, 77.7644>>	fSpawnHeading 	= 246.2589	RETURN TRUE	BREAK
		CASE 45	vSpawnPoint 	= <<924.7784, -16.9379, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 46	vSpawnPoint 	= <<921.8671, -13.9380, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		CASE 47	vSpawnPoint 	= <<928.1649, -18.8884, 77.7644>>	fSpawnHeading 	= 149.4000	RETURN TRUE	BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	// NOTE : default cases of the switch statements return FALSE
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_CASINO_HELI_ROOF_EXTERIOR_SPAWN_POINT_ON_FOOT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<977.7286, 61.4553, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 1	vSpawnPoint 	= <<976.6138, 62.1240, 119.2261>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 2	vSpawnPoint 	= <<978.8434, 60.7865, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 3	vSpawnPoint 	= <<974.8250, 63.1637, 119.2261>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 4	vSpawnPoint 	= <<979.9583, 60.1177, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 5	vSpawnPoint 	= <<973.1671, 63.9873, 119.2261>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 6	vSpawnPoint 	= <<981.0731, 59.4490, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 7	vSpawnPoint 	= <<971.5465, 65.3677, 119.2259>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 8	vSpawnPoint 	= <<977.0599, 60.3405, 119.2261>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 9	vSpawnPoint 	= <<975.9451, 61.0093, 119.2261>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 10	vSpawnPoint 	= <<978.1747, 59.6717, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 11	vSpawnPoint 	= <<974.1289, 62.1058, 119.2261>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 12	vSpawnPoint 	= <<979.2895, 59.0029, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 13	vSpawnPoint 	= <<969.6888, 62.1277, 119.2259>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 14	vSpawnPoint 	= <<983.0764, 58.3915, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 15	vSpawnPoint 	= <<970.5819, 63.6996, 119.2259>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 16	vSpawnPoint 	= <<974.6773, 55.0764, 119.2261>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 17	vSpawnPoint 	= <<975.2763, 59.8945, 119.2261>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 18	vSpawnPoint 	= <<977.5059, 58.5569, 119.2261>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 19	vSpawnPoint 	= <<973.7545, 60.2713, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 20	vSpawnPoint 	= <<978.6972, 56.3623, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 21	vSpawnPoint 	= <<972.3776, 60.8114, 119.2408>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 22	vSpawnPoint 	= <<981.9325, 55.7656, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 23	vSpawnPoint 	= <<967.3163, 59.0168, 119.2259>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 24	vSpawnPoint 	= <<973.1139, 55.9958, 119.2261>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 25	vSpawnPoint 	= <<970.9693, 59.9194, 119.2259>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 26	vSpawnPoint 	= <<977.3831, 56.9464, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 27	vSpawnPoint 	= <<968.8290, 59.7325, 119.2259>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 28	vSpawnPoint 	= <<981.9651, 53.9143, 119.2257>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 29	vSpawnPoint 	= <<971.2009, 57.0126, 119.2259>>	fSpawnHeading 	= 149.0400	RETURN TRUE	BREAK
		CASE 30	vSpawnPoint 	= <<959.4390, 32.1849, 119.2260>>	fSpawnHeading 	= 147.0743	RETURN TRUE	BREAK
		CASE 31	vSpawnPoint 	= <<957.7834, 33.3199, 119.2260>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 32	vSpawnPoint 	= <<960.4030, 31.0250, 119.2260>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 33	vSpawnPoint 	= <<953.8661, 35.6682, 119.2258>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 34	vSpawnPoint 	= <<961.8635, 30.7777, 119.2260>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 35	vSpawnPoint 	= <<955.4436, 33.7557, 119.2258>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 36	vSpawnPoint 	= <<956.9995, 31.7663, 119.2259>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 37	vSpawnPoint 	= <<960.4615, 29.7253, 119.2260>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 38	vSpawnPoint 	= <<952.8369, 34.4785, 119.2259>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 39	vSpawnPoint 	= <<964.4356, 27.3626, 119.2260>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 40	vSpawnPoint 	= <<958.9203, 28.3665, 119.2260>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 41	vSpawnPoint 	= <<954.9124, 31.0344, 119.2258>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 42	vSpawnPoint 	= <<960.4018, 26.7007, 119.2260>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 43	vSpawnPoint 	= <<951.7661, 32.4964, 119.2259>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 44	vSpawnPoint 	= <<963.2364, 25.7707, 119.2260>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 45	vSpawnPoint 	= <<956.8428, 28.1320, 119.2259>>	fSpawnHeading 	= 149.7600	RETURN TRUE	BREAK
		CASE 46	vSpawnPoint 	= <<964.2562, 59.0137, 111.5530>>	fSpawnHeading 	= 58.6800	RETURN TRUE	BREAK
		CASE 47	vSpawnPoint 	= <<965.2100, 59.9826, 111.5531>>	fSpawnHeading 	= 58.6800	RETURN TRUE	BREAK
		CASE 48	vSpawnPoint 	= <<963.6317, 57.9828, 111.5529>>	fSpawnHeading 	= 58.6800	RETURN TRUE	BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	// NOTE : default cases of the switch statements return FALSE
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_CASINO_TEMP_EXTERIOR_SPAWN_POINT_FOR_LIMO_SERVICE(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<931.9263, 42.1522, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 1	vSpawnPoint 	= <<932.8716, 43.6840, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 2	vSpawnPoint 	= <<930.9810, 40.6205, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 3	vSpawnPoint 	= <<933.8170, 45.2158, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 4	vSpawnPoint 	= <<930.0357, 39.0887, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 5	vSpawnPoint 	= <<934.7623, 46.7476, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 6	vSpawnPoint 	= <<929.0904, 37.5569, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 7	vSpawnPoint 	= <<930.3945, 43.0976, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 8	vSpawnPoint 	= <<931.3398, 44.6293, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 9	vSpawnPoint 	= <<929.4492, 41.5658, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 10	vSpawnPoint 	= <<932.2852, 46.1611, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 11	vSpawnPoint 	= <<928.5039, 40.0340, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 12	vSpawnPoint 	= <<933.2305, 47.6929, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 13	vSpawnPoint 	= <<927.5586, 38.5022, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 14	vSpawnPoint 	= <<928.8627, 44.0429, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 15	vSpawnPoint 	= <<929.8080, 45.5747, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 16	vSpawnPoint 	= <<927.9174, 42.5111, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 17	vSpawnPoint 	= <<930.7534, 47.1065, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 18	vSpawnPoint 	= <<926.9721, 40.9793, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 19	vSpawnPoint 	= <<931.6987, 48.6382, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 20	vSpawnPoint 	= <<926.0268, 39.4475, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 21	vSpawnPoint 	= <<927.3309, 44.9882, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 22	vSpawnPoint 	= <<928.2762, 46.5200, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 23	vSpawnPoint 	= <<926.3856, 43.4564, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 24	vSpawnPoint 	= <<929.2216, 48.0518, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 25	vSpawnPoint 	= <<925.4403, 41.9246, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 26	vSpawnPoint 	= <<930.1669, 49.5836, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 27	vSpawnPoint 	= <<924.4950, 40.3928, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 28	vSpawnPoint 	= <<932.0979, 39.8551, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		CASE 29	vSpawnPoint 	= <<933.5530, 42.2353, 69.8997>>	fSpawnHeading 	= 58.3200	RETURN TRUE	BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	// NOTE : default cases of the switch statements return FALSE
	RETURN TRUE
ENDFUNC

FUNC INT GET_NUMBER_OF_LIMO_SERVICE_SEATS_FOR_CASINO(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN 5
ENDFUNC

PROC CASINO_SEND_VEH_SERVICE_EXIT_REQUEST(SIMPLE_INTERIORS eSimpleInteriorID, INT iDestination)
	UNUSED_PARAMETER(eSimpleInteriorID)
	CASINO_LIMO_WARP_DESTINATION eDestination = INT_TO_ENUM(CASINO_LIMO_WARP_DESTINATION, iDestination)
	REQUEST_CASINO_LIMO_WARP(eDestination)
ENDPROC

PROC CASINO_SET_LOCAL_PLAYER_USING_LIMO_SERVICE_EXIT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SET_LOCAL_PLAYER_USING_CASINO_LIMO_WARP()
ENDPROC

FUNC BOOL GET_CASINO_NIGHTCLUB_DOOR_SPAWN_POINT_ON_FOOT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<987.6998, 79.6174, 79.9906>>	fSpawnHeading 	= 325.8119	RETURN TRUE	BREAK
		CASE 1	vSpawnPoint 	= <<988.8380, 79.1121, 79.9906>>	fSpawnHeading 	= 330.0994	RETURN TRUE	BREAK
		CASE 2	vSpawnPoint 	= <<985.5862, 84.4155, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 3	vSpawnPoint 	= <<986.7773, 80.4501, 79.9906>>	fSpawnHeading 	= 334.2740	RETURN TRUE	BREAK
		CASE 4	vSpawnPoint 	= <<985.0731, 83.1928, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 5	vSpawnPoint 	= <<983.9196, 82.9472, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 6	vSpawnPoint 	= <<983.9998, 84.5737, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 7	vSpawnPoint 	= <<982.5625, 82.4819, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 8	vSpawnPoint 	= <<985.6527, 82.2686, 79.9906>>	fSpawnHeading 	= 350.7229	RETURN TRUE	BREAK
		CASE 9	vSpawnPoint 	= <<982.5352, 80.6484, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 10	vSpawnPoint 	= <<980.6038, 81.2832, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 11	vSpawnPoint 	= <<981.9153, 84.7201, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 12	vSpawnPoint 	= <<981.0470, 82.9033, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 13	vSpawnPoint 	= <<983.1270, 85.9735, 79.9907>>	fSpawnHeading 	= 64.3531	RETURN TRUE	BREAK
		CASE 14	vSpawnPoint 	= <<979.0856, 82.2783, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 15	vSpawnPoint 	= <<980.5722, 84.4286, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 16	vSpawnPoint 	= <<980.5979, 86.0491, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 17	vSpawnPoint 	= <<979.4668, 84.0393, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 18	vSpawnPoint 	= <<981.4636, 87.2133, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 19	vSpawnPoint 	= <<977.7943, 83.2559, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 20	vSpawnPoint 	= <<978.3337, 87.0449, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 21	vSpawnPoint 	= <<979.3495, 85.6261, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 22	vSpawnPoint 	= <<975.6777, 86.7260, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 23	vSpawnPoint 	= <<980.0270, 88.0974, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 24	vSpawnPoint 	= <<977.4577, 85.2751, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 25	vSpawnPoint 	= <<974.8323, 89.2286, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 26	vSpawnPoint 	= <<976.7056, 88.7798, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 27	vSpawnPoint 	= <<973.9108, 87.7250, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 28	vSpawnPoint 	= <<978.2062, 89.4353, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 29	vSpawnPoint 	= <<975.3220, 84.7579, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 30	vSpawnPoint 	= <<972.6411, 91.2214, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 31	vSpawnPoint 	= <<973.9681, 92.0315, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 32	vSpawnPoint 	= <<971.3626, 89.7511, 79.9906>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 33	vSpawnPoint 	= <<976.3371, 90.3089, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 34	vSpawnPoint 	= <<973.2603, 86.0688, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 35	vSpawnPoint 	= <<991.7577, 78.5492, 79.9906>>	fSpawnHeading 	= 237.7406	RETURN TRUE	BREAK
		CASE 36	vSpawnPoint 	= <<992.4178, 79.9915, 79.9906>>	fSpawnHeading 	= 251.9052	RETURN TRUE	BREAK
		CASE 37	vSpawnPoint 	= <<970.2983, 86.4567, 79.9902>>	fSpawnHeading 	= 148.0900	RETURN TRUE	BREAK
		CASE 38	vSpawnPoint 	= <<969.8869, 92.5477, 79.9907>>	fSpawnHeading 	= 56.1600	RETURN TRUE	BREAK
		CASE 39	vSpawnPoint 	= <<969.7424, 88.2620, 79.9907>>	fSpawnHeading 	= 150.6809	RETURN TRUE	BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	// NOTE : default cases of the switch statements return FALSE
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_EXIT_CASINO_TO_ROOF()
	RETURN IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_LOCATION_CASINO_ROOF)
ENDFUNC

PROC CLEAR_PLAYER_EXIT_CASINO_TO_ROOF()
	PRINTLN("[SIMPLE_INTERIOR] CLEAR_PLAYER_EXIT_CASINO_TO_ROOF")
	CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_LOCATION_CASINO_ROOF)
ENDPROC

FUNC BOOL WAS_SIMPLE_INTERIOR_VEH_ENTRY_TRIGGERED_BY_DRIVER_FROM_REMOTE_EXT_SC()
	RETURN IS_BIT_SET(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXTERIOR_TRIGGERED_VEH_ENTRY_FROM_REMOTE_EXT_SC)
ENDFUNC

FUNC BOOL SHOULD_PLAYER_EXIT_CASINO_TO_HELIPAD()
	RETURN IS_BIT_SET(g_simpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_TO_CASINO_HELIPAD)
ENDFUNC

PROC CLEAR_PLAYER_EXIT_CASINO_TO_HELIPAD()
	PRINTLN("[SIMPLE_INTERIOR] CLEAR_PLAYER_EXIT_CASINO_TO_ROOF")
	CLEAR_BIT(g_simpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_TO_CASINO_HELIPAD)
ENDPROC

/// PURPOSE:
///    BS used for broadcasts
/// PARAMS:
///    excludePlayer - 
/// RETURNS:
///    
FUNC INT EVERYONE_RUNNING_THIS_SIMPLE_INTERIOR_INT_SCRIPT(SIMPLE_INTERIORS eInteriorID, PLAYER_INDEX excludePlayer, INT iFloorIndexToMatch = -1)
	INT iPlayersBS
	INT iParticipant
	PARTICIPANT_INDEX participantIndex
	PLAYER_INDEX playerIndex
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		participantIndex = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
			
			IF playerIndex != excludePlayer
			AND IS_NET_PLAYER_OK(playerIndex)
			AND GET_SIMPLE_INTERIOR_PLAYER_IS_IN(playerIndex) = eInteriorID
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(playerIndex)
			AND (iFloorIndexToMatch = -1 OR GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(playerIndex) = iFloorIndexToMatch)
				SET_BIT(iPlayersBS, NATIVE_TO_INT(playerIndex))
				//PRINTLN("EVERYONE_RUNNING_THIS_SIMPLE_INTERIOR_INT_SCRIPT - Player ", GET_PLAYER_NAME(playerIndex), " (", NATIVE_TO_INT(playerIndex), ") is in!")
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iPlayersBS
ENDFUNC

FUNC BOOL IS_CASINO_PROPERTY_ACCESIBLE(INT iMenuIndex)
	SWITCH iMenuIndex
		CASE ciCASINO_MAIN_ENTRANCE 			RETURN TRUE
		CASE ciCASINO_APARTMENT_ENTRANCE 		RETURN DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
		CASE ciCASINO_APARTMENT_GARAGE_ENTRANCE	RETURN (HAS_PLAYER_PURCHASED_CASINO_APARTMENT_GARAGE(PLAYER_ID()) AND !g_sMPTunables.bVC_PENTHOUSE_DISABLE_PARKING)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/////////////////////////////////////////////////////////////////////////////////////////////////////////
///    Casino veh cutscene shared data & functionality
/////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC VECTOR GET_CASINO_BUILDING_HELIPAD_COORDS()
	RETURN <<965.8581, 42.3771, 123.1199>>
ENDFUNC

//Cutscene BS
CONST_INT CUTSCENE_BS_CAS_APT_SETUP_FIRST_VEHICLE_SCENE		0
CONST_INT CUTSCENE_BS_CAS_APT_ONE_SHOT_BIKE_SCENE			1
CONST_INT CUTSCENE_BS_CAS_APT_TALL_VEH_SCENE				2
CONST_INT CUTSCENE_BS_CAS_APT_VEH_ABOVE_LENGTH_LIMIT		3
CONST_INT CUTSCENE_BS_CAS_APT_TASKED_MOTORBIKE				4
CONST_INT CUTSCENE_BS_CAS_APT_FROZEN_CLONE					5
CONST_INT CUTSCENE_BS_CAS_APT_SET_VEH_WARP_FLAG				6

//Valet exit cutscene BS
CONST_INT CUTSCENE_BS_CAS_VAL_EXIT_SETUP_RENDERING_CAM			0
CONST_INT CUTSCENE_BS_CAS_VAL_EXIT_SELECTED_A_SCENE_VARIAITON	1
CONST_INT CUTSCENE_BS_CAS_VAL_EXIT_SELECTED_VAR_1				2
CONST_INT CUTSCENE_BS_CAS_VAL_EXIT_SELECTED_VAR_2				3
CONST_INT CUTSCENE_BS_CAS_VAL_EXIT_USE_MALE_ANIMS				4
CONST_INT CUTSCENE_BS_CAS_VAL_EXIT_SETUP_MODEL_HIDES			5

//Cutscene array indexes
CONST_INT CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MIN		1
CONST_INT CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MAX		0
CONST_INT CUTSCENE_VEC_ARRAY_CAS_SAVED_VEH_COORD			2

CONST_INT CUTSCENE_FLOAT_ARRAY_CAS_VEH_Z_COORD				0
CONST_INT CUTSCENE_FLOAT_CAS_MOVE_VEH_DISTANCE				1
CONST_INT CUTSCENE_FLOAT_ARRAY_CAS_VEH_SPEED				2

//Objects
CONST_INT CUTSCENE_CAS_OBJ_CAR_KEYS							0

//Peds
CONST_INT CUTSCENE_CAS_PED_DRIVER							0
CONST_INT CUTSCENE_CAS_PED_PLAYER							0

//General consts
CONST_FLOAT CASINO_APT_SCENE_VEH_MAX_HEIGHT 				4.9
CONST_FLOAT CASINO_APT_SCENE_VEH_MAX_LENGTH 				7.0

CONST_FLOAT CASINO_VALET_SCENE_VEH_TINY_LENGTH				2.1
CONST_FLOAT CASINO_VALET_SCENE_VEH_USHORT_LENGTH 			2.75
CONST_FLOAT CASINO_VALET_SCENE_VEH_SHORT_LENGTH 			3.5
CONST_FLOAT CASINO_VALET_SCENE_VEH_LARGE_LENGTH 			4.875
CONST_FLOAT CASINO_VALET_SCENE_VEH_XL_LENGTH 				6.5

ENUM CAS_VALET_ENTRY_EXIT_ANIM_COMPONENT
	CVE_DRIVER,
	CVE_DRIVER_FACE,
	CVE_KEYS,
	CVE_VALET,
	CVE_VALET_FACE,
	CVE_VALET_PROP,
	CVE_CAMERA,
	CVE_DOOR
ENDENUM

FUNC BOOL SHOULD_CASINO_GAR_ENTRY_VEH_SCENE_BE_SKIPPED()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(veh)
				//The vehicle is visibly too tall for the garrage door so skip the scene
				VECTOR vModelMin, vModelMax
				SAFE_GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), vModelMin, vModelMax)
			
				//Check the height of the vehicle
				IF (vModelMax.z - vModelMin.z) > CASINO_APT_SCENE_VEH_MAX_HEIGHT	
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_CASINO_GARAGE_VEH_SCENE_ESTABLISHING_SHOT(VECTOR &vEstablishingPos, VECTOR &vEstablishingRot, FLOAT &fEstablishingFOV, BOOL bStart)
	UNUSED_PARAMETER(vEstablishingPos)
	UNUSED_PARAMETER(vEstablishingRot)
	UNUSED_PARAMETER(fEstablishingFOV)
	IF bStart
		vEstablishingPos 	= <<937.2059, -3.9669, 78.4233>>
		vEstablishingRot	= <<2.4913, -0.0000, 84.4146>>
		fEstablishingFOV	= 49.2248
	ELSE
		vEstablishingPos 	= <<937.5319, -3.2264, 78.4233>>
		vEstablishingRot	= <<2.4913, -0.0000, 67.1525>>
		fEstablishingFOV	= 49.2248
	ENDIF
ENDPROC

PROC GET_CASINO_GARAGE_VEH_SCENE_TOP_DOWN_SHOT(VECTOR &vTDShot, VECTOR &vTDRot, FLOAT &fTDFOV, BOOL bStart)
	UNUSED_PARAMETER(vTDShot)
	UNUSED_PARAMETER(vTDRot)
	UNUSED_PARAMETER(fTDFOV)
	IF bStart
		vTDShot = <<936.4088, 1.3674, 85.4383>>
		vTDRot	= <<-68.3827, -0.0000, 147.8355>>
		fTDFOV	= 50.0
	ELSE
		vTDShot = <<936.4088, 1.3674, 85.4383>>
		vTDRot	= <<-58.3432, 0.0000, 147.8355>>
		fTDFOV	= 50.0
	ENDIF
ENDPROC

FUNC BOOL EXT_CUTSCENE_CASINO_GARAGE_ENTRY_CREATE_ASSETS(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
		
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[CASINO_APT] - EXT_CUTSCENE_ARENA_GARAGE_CREATE_ASSETS: ped is in a vehicle. Entrance used = ", GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED())
		
		VEHICLE_INDEX vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF NOT DOES_ENTITY_EXIST(vehPlayerIsIn)
			ASSERTLN("[CASINO_APT] - EXT_CUTSCENE_ARENA_GARAGE_CREATE_ASSETS - ped is in a vehicle, but can not grab vehicle reference...?")
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(entryAnim.sCutsceneVeh.vehicle)
			CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[CASINO_APT] - EXT_CUTSCENE_ARENA_GARAGE_CREATE_ASSETS - created clone of the player vehicle.")
			CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS(vehPlayerIsIn, entryAnim.sCutsceneVeh, chVEHICLE_CLONE_TRAILER | chVEHICLE_CLONE_FIX_TIRES | chVEHICLE_CLONE_ONLY_PLAYER_PASSENGERS | chVEHICLE_CLONE_DO_NOT_CLONE_PASSENGERS)
			CUTSCENE_HELP_DISABLE_VEHICLE_AUTOMATIC_CRASH(entryAnim.sCutsceneVeh.vehicle)
			SET_DISABLE_MAP_COLLISION(entryAnim.sCutsceneVeh.vehicle)
			
			RETURN FALSE
		ELSE
			IF CUTSCENE_HELP_VEH_PASSENGERS_CLONE(vehPlayerIsIn, entryAnim.sCutsceneVeh.vehicle, entryAnim.sCutsceneVeh.vehPassengers, TRUE, TRUE, DEFAULT, TRUE)
				CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[CASINO_APT] - EXT_CUTSCENE_ARENA_GARAGE_CREATE_ASSETS - Placed ped clones inside the vehicle on entry.")
				SET_DISABLE_MAP_COLLISION(entryAnim.sCutsceneVeh.vehicle)
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
		
		//Grab the model min and max for use later
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle), entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MIN], entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MAX])
		
		VECTOR vEstablishingShot, vEstablishingShotEnd, vEstablishingRot, vEstablishingEndRot, vTDShot, vTDEndShot, vTDRot, vTDEndRot
		FLOAT fEstablishingFOV, fEstablishingEndFOV, fTDFOV, fTDEndFOV
		
		//Scene 1
		GET_CASINO_GARAGE_VEH_SCENE_ESTABLISHING_SHOT(vEstablishingShot, vEstablishingRot, fEstablishingFOV, TRUE)
		GET_CASINO_GARAGE_VEH_SCENE_ESTABLISHING_SHOT(vEstablishingShotEnd, vEstablishingEndRot, fEstablishingEndFOV, FALSE)
		
		//Scene 2
		GET_CASINO_GARAGE_VEH_SCENE_TOP_DOWN_SHOT(vTDShot, vTDRot, fTDFOV, TRUE)
		GET_CASINO_GARAGE_VEH_SCENE_TOP_DOWN_SHOT(vTDEndShot, vTDEndRot, fTDEndFOV, FALSE)
		
		BOOL bSkipSecondCut 			= IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehPlayerIsIn))
		//If the vehicle does not qualify as extra long then use a shorter second cut
		BOOL bShortenSecondCut 			= !((entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MAX].y - entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MIN].y) > CASINO_APT_SCENE_VEH_MAX_LENGTH)
		
		IF bSkipSecondCut
			SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_APT_ONE_SHOT_BIKE_SCENE)
		ELSE
			CLEAR_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_APT_ONE_SHOT_BIKE_SCENE)
		ENDIF		
		
		SIMPLE_INTERIOR_VEH_ENTRY_SCENE_CREATE_SCENE_NO_ZOOM(g_SimpleInteriorData.sCustomCutsceneExt, vEstablishingShot, vEstablishingRot, fEstablishingFOV, vEstablishingShotEnd, vEstablishingEndRot, fEstablishingEndFOV, bSkipSecondCut, vTDShot, vTDRot, fTDFOV, vTDEndShot, vTDEndRot, fTDEndFOV, bShortenSecondCut)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC VECTOR CASINO_GARAGE_VEH_ENTRY_VEHICLE_START_POSITION()
	RETURN <<932.9553, -4.2065, 77.7649>>
ENDFUNC

FUNC FLOAT CASINO_GARAGE_VEH_ENTRY_VEHICLE_START_HEADING()
	RETURN 328.1944
ENDFUNC

FUNC VECTOR CASINO_GARAGE_VEH_ENTRY_VEHICLE_END_POSITION()
	RETURN <<940.2556, 6.9082, 77.7649>>
ENDFUNC

PROC CASINO_GARAGE_VEH_ENTRY_GET_VEHICLE_DRIVE_FORWARD_LIMIT_AREA(VECTOR &vecA, VECTOR &vecB, FLOAT &fWidth)
	
	vecA = <<934.148499,3.296672,77.764908>>
	vecB = <<938.994263,0.124347,81.139908>>
	fWidth = 1.43750

ENDPROC

PROC CASINO_GARAGE_VEH_ENTRY_CONTROL_VEH_CLONE(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	SIMPLE_INTERIOR_VEH_ENTRY_SCENE_DATA sEntrySceneData
	
	sEntrySceneData.bVehAboveLengthLimit 	= IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_APT_VEH_ABOVE_LENGTH_LIMIT)
	sEntrySceneData.fStartHeading 			= CASINO_GARAGE_VEH_ENTRY_VEHICLE_START_HEADING()
	sEntrySceneData.vVehStartPosition		= CASINO_GARAGE_VEH_ENTRY_VEHICLE_START_POSITION()
	sEntrySceneData.vVehEndPosition			= CASINO_GARAGE_VEH_ENTRY_VEHICLE_END_POSITION()
	
	//Indexes
	sEntrySceneData.iVecArrayIndexPrimaryVehModelMax 	= CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MAX
	sEntrySceneData.iVecArrayIndexSavedVehCoords		= CUTSCENE_VEC_ARRAY_CAS_SAVED_VEH_COORD
	
	sEntrySceneData.iFloatArrayIndexSavedVehSpeed		= CUTSCENE_FLOAT_ARRAY_CAS_VEH_SPEED
	sEntrySceneData.iFloatArrayIndexVehMoveDistance		= CUTSCENE_FLOAT_CAS_MOVE_VEH_DISTANCE
	sEntrySceneData.iFloatArrayIndexVehZCoord			= CUTSCENE_FLOAT_ARRAY_CAS_VEH_Z_COORD
	
	//BS Ints
	sEntrySceneData.iTaskedVeh							= CUTSCENE_BS_CAS_APT_TASKED_MOTORBIKE
	sEntrySceneData.iFrozenVehClone						= CUTSCENE_BS_CAS_APT_FROZEN_CLONE
	sEntrySceneData.bSingleShotBikeScene 				= IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_APT_ONE_SHOT_BIKE_SCENE)
	
	CASINO_GARAGE_VEH_ENTRY_GET_VEHICLE_DRIVE_FORWARD_LIMIT_AREA(sEntrySceneData.vLimitAreaA, sEntrySceneData.vLimitAreaB, sEntrySceneData.fLimitAreaWidth)
	
	SIMPLE_INTERIOR_VEH_ENTRY_CONTROL_VEH_CLONE(entryAnim, sEntrySceneData)
ENDPROC

FUNC BOOL EXT_CUTSCENE_CASINO_GAR_MAINTAIN(SIMPLE_INTERIORS eInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	
	IF SHOULD_CASINO_GAR_ENTRY_VEH_SCENE_BE_SKIPPED()
		RETURN TRUE
	ENDIF
	
	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(g_SimpleInteriorData.sCustomCutsceneExt, 0)
	OR SIMPLE_CUTSCENE_IS_SCENE_RUNNING(g_SimpleInteriorData.sCustomCutsceneExt, 1)
	
		//FIRST FRAME
		IF NOT IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_APT_SETUP_FIRST_VEHICLE_SCENE)
			CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(entryAnim.sCutsceneVeh.vehicle, TRUE)
			
			IF IS_ENTITY_ALIVE(entryAnim.sCutsceneVeh.vehicle)
				
				//Locally conceal the primary delivery vehicle
				//EXT_CUTSCENE_SIMPLE_INTERIOR_CONCEAL_ENTRY_VEHICLE()
				
				//Check the height of the vehicle
				IF (entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MAX].z - entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MIN].z) > CASINO_APT_SCENE_VEH_MAX_HEIGHT	
					SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_APT_TALL_VEH_SCENE)
				ENDIF
				
				VECTOR vVehiclePosition
				IF GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle) = MONSTER
				OR GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle) = MONSTER3
				OR GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle) = MONSTER4
				OR GET_ENTITY_MODEL(entryAnim.sCutsceneVeh.vehicle) = MONSTER5
					vVehiclePosition = <<929.8, -8.265, 77.7649>>
				ELSE
					vVehiclePosition = <<932.9553, -4.2065, 77.7649>>
				ENDIF
				
				SET_ENTITY_COORDS_NO_OFFSET(entryAnim.sCutsceneVeh.vehicle, vVehiclePosition)
				SET_ENTITY_HEADING(entryAnim.sCutsceneVeh.vehicle, 328.1944)
				
				FREEZE_ENTITY_POSITION(entryAnim.sCutsceneVeh.vehicle, FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(entryAnim.sCutsceneVeh.vehicle)
				SET_ENTITY_COLLISION(entryAnim.sCutsceneVeh.vehicle, TRUE)
				SET_ENTITY_VISIBLE(entryAnim.sCutsceneVeh.vehicle, TRUE)
				SET_VEHICLE_ENGINE_ON(entryAnim.sCutsceneVeh.vehicle, TRUE, TRUE)
				
				SET_FOCUS_POS_AND_VEL(vVehiclePosition, <<0.0, 0.0, 0.0>>)
				
				entryAnim.fStateArray[CUTSCENE_FLOAT_CAS_MOVE_VEH_DISTANCE] = (entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MAX].y - entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MIN].y)
				entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MAX].x = 0.0
				entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_CAS_PRIMARY_VEH_MODEL_MAX].z = 0.0
				
				entryAnim.bPlayGarageDoorSoundOnExit = TRUE
				PLAY_SOUND_FRONTEND(-1, "Garage_Door_Open", "GTAO_Script_Doors_Faded_Screen_Sounds")
				
				//Setup complete
				SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_APT_SETUP_FIRST_VEHICLE_SCENE)				
			ENDIF
		ELSE
			IF eInteriorID = SIMPLE_INTERIOR_CASINO_VAL_GARAGE
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_APT_SET_VEH_WARP_FLAG)
				VEHICLE_INDEX theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF PLAYER_PED_ID() = GET_PED_IN_VEHICLE_SEAT( theVeh)
//					INIT_WARP_INTO_CASINO_VALET_CAR_PARK(FALSE)
					IF NOT HAS_PLAYER_STARTED_WARP_INTO_CASINO_VALET_CAR_PARK()
						SET_ENTITY_COORDS(theVeh,<<932.9553+(3*NATIVE_TO_INT(PLAYER_ID())), -4.2065, 67.7649>>)
						FREEZE_ENTITY_POSITION(theVeh,TRUE)
						CDEBUG1LN(DEBUG_PROPERTY, "[CASINO_APT] - EXT_CUTSCENE_CASINO_APT_MAINTAIN - moving beneath map and freezing")
					ENDIF
					SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_APT_SET_VEH_WARP_FLAG)
				ENDIF
			ENDIF
		ENDIF
		
		CASINO_GARAGE_VEH_ENTRY_CONTROL_VEH_CLONE(entryAnim)
	ENDIF
	
	//Start fading out
	IF (g_SimpleInteriorData.sCustomCutsceneExt.bPlaying
	AND g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount > 0
	AND g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene = g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount - 1
	AND (g_SimpleInteriorData.sCustomCutsceneExt.sScenes[g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene].iDuration - g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime) <= 400)
		OR NOT g_SimpleInteriorData.sCustomCutsceneExt.bPlaying
		
		IF eInteriorID = SIMPLE_INTERIOR_CASINO_VAL_GARAGE						
			//Locally conceal the primary delivery vehicle
			//EXT_CUTSCENE_SIMPLE_INTERIOR_CONCEAL_ENTRY_VEHICLE(DEFAULT,DEFAULT,FALSE)
			SET_LOCAL_PLAYER_SKIP_RS_TELEPORTING_WARP(TRUE)
		ENDIF
		
		IF (GET_FRAME_COUNT() % 60) = 0
			CDEBUG1LN(DEBUG_PROPERTY, "[CASINO_APT] - EXT_CUTSCENE_CASINO_APT_MAINTAIN - NOT PLAYING")
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//////////////////////////////////////////////
///Casino valet exit

FUNC MODEL_NAMES GET_CASINO_VALET_PED_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("S_M_Y_Casino_01"))
ENDFUNC

FUNC MODEL_NAMES GET_CASINO_VALET_CARKEY_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("lr_prop_carkey_fob"))
ENDFUNC

FUNC MODEL_NAMES GET_CASINO_VALET_STAND_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_Valet_01a"))
ENDFUNC

PROC SET_CASINO_VALET_PED_VARIATIONS(PED_INDEX &pedValet)
	IF IS_ENTITY_ALIVE(pedValet)
		SET_PED_DEFAULT_COMPONENT_VARIATION(pedValet)
		SET_PED_COMPONENT_VARIATION(pedValet, PED_COMP_HEAD, 5, 0)
		SET_PED_COMPONENT_VARIATION(pedValet, PED_COMP_BERD, 1, 0)
		SET_PED_COMPONENT_VARIATION(pedValet, PED_COMP_HAIR, 5, 0)
		SET_PED_COMPONENT_VARIATION(pedValet, PED_COMP_TORSO, 1, 4)
		SET_PED_COMPONENT_VARIATION(pedValet, PED_COMP_LEG, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedValet, PED_COMP_FEET, 1, 0)
		SET_PED_COMPONENT_VARIATION(pedValet, PED_COMP_TEETH, 2, 0)
		SET_PED_COMPONENT_VARIATION(pedValet, PED_COMP_SPECIAL, 3, 0)
		SET_PED_COMPONENT_VARIATION(pedValet, PED_COMP_DECL, 1, 0)
		SET_PED_COMPONENT_VARIATION(pedValet, PED_COMP_JBIB, 1, 1)
	ENDIF
ENDPROC

FUNC STRING GET_ANIM_DICT_FOR_CASINO_VALET_EXIT(INT iVariation)
	SWITCH iVariation
		CASE 0 RETURN "anim@amb@casino@valet@exit@var01@"	BREAK
		CASE 1 RETURN "anim@amb@casino@valet@exit@var02@"	BREAK
	ENDSWITCH
	
	RETURN "**UNKNOWN**"
ENDFUNC

FUNC INT GET_SELECTED_CASINO_VALET_EXIT_ANIM_VARIATION(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	INT iVariation = 0
	
	IF IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_SELECTED_VAR_2)
		iVariation = 1
	ENDIF
	
	RETURN iVariation
ENDFUNC

FUNC STRING GET_ANIM_FOR_CASINO_VALET_EXIT(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim, CAS_VALET_ENTRY_EXIT_ANIM_COMPONENT eComponent)
	SWITCH GET_SELECTED_CASINO_VALET_EXIT_ANIM_VARIATION(entryAnim)
		CASE 0
			SWITCH eComponent
				CASE CVE_DRIVER
					IF IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_USE_MALE_ANIMS)
						RETURN "exit_var01_mp_m_freemode_01"
					ELSE
						RETURN "exit_var01_mp_f_freemode_01"
					ENDIF
				BREAK
				CASE CVE_DRIVER_FACE
					IF IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_USE_MALE_ANIMS)
						RETURN "exit_var01_mp_m_freemode_01_facial"
					ELSE
						RETURN "exit_var01_mp_f_freemode_01_facial"
					ENDIF
				BREAK
				CASE CVE_KEYS
					IF IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_USE_MALE_ANIMS)
						RETURN "exit_var01_mp_m_lr_prop_carkey_fob"
					ELSE
						RETURN "exit_var01_mp_f_lr_prop_carkey_fob"
					ENDIF
				BREAK
				CASE CVE_VALET		RETURN "exit_var01_valet_male"				BREAK
				CASE CVE_VALET_PROP	RETURN "exit_var01_vw_prop_vw_valet_01a"	BREAK
				CASE CVE_CAMERA		RETURN "exit_var01_cam"						BREAK
				CASE CVE_VALET_FACE	RETURN "exit_var01_valet_male_facial"		BREAK
			ENDSWITCH
		BREAK
		CASE 1			
			SWITCH eComponent
				CASE CVE_DRIVER
					IF IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_USE_MALE_ANIMS)
						RETURN "exit_var02_mp_m_freemode_01"
					ELSE
						RETURN "exit_var02_mp_f_freemode_01"
					ENDIF
				BREAK
				CASE CVE_DRIVER_FACE
					IF IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_USE_MALE_ANIMS)
						RETURN "exit_var02_mp_m_freemode_01_facial"
					ELSE
						RETURN "exit_var02_mp_f_freemode_01_facial"
					ENDIF
				BREAK
				CASE CVE_KEYS
					IF IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_USE_MALE_ANIMS)
						RETURN "exit_var02_mp_m_lr_prop_carkey_fob"
					ELSE
						RETURN "exit_var02_mp_f_lr_prop_carkey_fob"
					ENDIF
				BREAK
				CASE CVE_VALET		RETURN "exit_var02_valet_male"				BREAK
				CASE CVE_VALET_PROP	RETURN "exit_var02_vw_prop_vw_valet_01a"	BREAK
				CASE CVE_CAMERA		RETURN "exit_var02_cam"						BREAK
				CASE CVE_VALET_FACE	RETURN "exit_var02_valet_male_facial"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN "**UNKNOWN**"
ENDFUNC

FUNC VECTOR GET_CASINO_VALET_ENTRY_SCENE_VEH_POSITION(FLOAT fVehLength)
	IF fVehLength >= CASINO_VALET_SCENE_VEH_XL_LENGTH
		RETURN <<923.6102, 56.4650, 79.8982>>
	ELIF fVehLength >= CASINO_VALET_SCENE_VEH_LARGE_LENGTH
		RETURN <<923.7418, 56.4470, 79.8982>>
	ELIF fVehLength <= CASINO_VALET_SCENE_VEH_TINY_LENGTH
		RETURN <<921.7211, 54.8358, 79.8982>>
	ELIF fVehLength <= CASINO_VALET_SCENE_VEH_USHORT_LENGTH
		RETURN <<922.2322, 55.1365, 79.8982>>
	ELIF fVehLength <= CASINO_VALET_SCENE_VEH_SHORT_LENGTH
		RETURN <<922.7203, 55.2826, 79.8982>>
	ENDIF
	
	RETURN <<923.1972, 55.3635, 79.8982>>
ENDFUNC

PROC SELECT_CASINO_VALET_EXIT_VARIATION(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	CLEAR_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_SELECTED_VAR_1)
	CLEAR_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_SELECTED_VAR_2)
	
	INT iVariation = GET_RANDOM_INT_IN_RANGE(0, 2)
	
	SWITCH iVariation
		CASE 0	SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_SELECTED_VAR_1)	BREAK
		CASE 1	SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_SELECTED_VAR_2)	BREAK
	ENDSWITCH
	
	PRINTLN("[CASINO] - SELECT_CASINO_VALET_EXIT_VARIATION - Selected variation ", iVariation)
	entryAnim.dictionary = GET_ANIM_DICT_FOR_CASINO_VALET_EXIT(iVariation)
ENDPROC

FUNC BOOL INT_CUTSCENE_CASINO_VALET_EXIT_ASSETS_LOADED(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF NOT IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_SELECTED_A_SCENE_VARIAITON)
		SELECT_CASINO_VALET_EXIT_VARIATION(entryAnim)
		SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_SELECTED_A_SCENE_VARIAITON)
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(g_SimpleInteriorData.eValetExit.ownerPed)
		SCRIPT_ASSERT("INT_CUTSCENE_CASINO_VALET_EXIT_ASSETS_LOADED - Can't use drivers ped!")
		g_SimpleInteriorData.eValetExit.ownerPed = PLAYER_PED_ID()
	ENDIF
		
	MODEL_NAMES eValetModel = GET_CASINO_VALET_PED_MODEL()
	MODEL_NAMES eKeyModel	= GET_CASINO_VALET_CARKEY_MODEL()
	REQUEST_MODEL(eValetModel)
	REQUEST_MODEL(eKeyModel)
	REQUEST_MODEL(GET_CASINO_ENTRANCE_DOOR_MODEL(CED_LEFT_SIDE_LEFT_DOOR))
	REQUEST_MODEL(GET_CASINO_ENTRANCE_DOOR_MODEL(CED_LEFT_SIDE_RIGHT_DOOR))
	REQUEST_MODEL(GET_ENTITY_MODEL(g_SimpleInteriorData.eValetExit.ownerPed))
	REQUEST_ANIM_DICT(entryAnim.dictionary)
	
	IF NOT HAS_MODEL_LOADED(eValetModel)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(eKeyModel)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED((GET_CASINO_ENTRANCE_DOOR_MODEL(CED_LEFT_SIDE_LEFT_DOOR)))
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED((GET_CASINO_ENTRANCE_DOOR_MODEL(CED_LEFT_SIDE_RIGHT_DOOR)))
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(entryAnim.dictionary)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(GET_ENTITY_MODEL(g_SimpleInteriorData.eValetExit.ownerPed))
		RETURN FALSE
	ENDIF
	
	SET_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_SCENE_USES_CUSTOM_CUTSCENE)
	SET_BIT(g_SimpleInteriorData.iSixthBS, BS6_SIMPLE_INTERIOR_EXIT_SCENE_CEDE_SCREEN_FADE_CONTROL_TO_CS_HEADER)
	
	RETURN TRUE
ENDFUNC

PROC CASINO_VALET_EXIT_SCENE_DO_DOOR_MODEL_HIDE(VECTOR vDoorCoords, MODEL_NAMES eModel)
	CREATE_MODEL_HIDE(vDoorCoords, 0.1, eModel, FALSE)
ENDPROC

FUNC BOOL CASINO_VALET_EXIT_SCENE_CREATE_DOOR(OBJECT_INDEX &objDoor, VECTOR vDoorCoords, FLOAT fDoorRotation, MODEL_NAMES eModel)
	
	objDoor = CREATE_OBJECT(eModel, vDoorCoords, FALSE, FALSE, TRUE)
	
	IF NOT DOES_ENTITY_EXIST(objDoor)
		RETURN FALSE
	ELSE
		SET_ENTITY_COORDS_NO_OFFSET(objDoor, vDoorCoords, FALSE, FALSE, FALSE)
		SET_ENTITY_ROTATION(objDoor, <<0.0, 0.0, fDoorRotation>>)
		FREEZE_ENTITY_POSITION(objDoor, TRUE)
		SET_ENTITY_COLLISION(objDoor, FALSE)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_VALET_PED_FOR_CASINO(PED_INDEX &pedValet, VECTOR vCoords, BOOL bReleaseModel = TRUE)
	//Create the valet ped
	MODEL_NAMES eValetModel = GET_CASINO_VALET_PED_MODEL()
	pedValet = CREATE_PED(PEDTYPE_CIVMALE, eValetModel, vCoords, 0.0, FALSE, FALSE)
	
	IF bReleaseModel
		SET_MODEL_AS_NO_LONGER_NEEDED(eValetModel)
	ENDIF
	
	IF IS_ENTITY_ALIVE(pedValet)
		FREEZE_ENTITY_POSITION(pedValet, TRUE)
		SET_CASINO_VALET_PED_VARIATIONS(pedValet)
		SET_PED_CONFIG_FLAG(pedValet, PCF_CanBeAgitated, FALSE)
		SET_PED_CONFIG_FLAG(pedValet, PCF_PedIgnoresAnimInterruptEvents, TRUE)
		SET_PED_CONFIG_FLAG(pedValet, PCF_DisableExplosionReactions, TRUE)
		SET_PED_CONFIG_FLAG(pedValet, PCF_DisableShockingEvents, TRUE)
		SET_PED_CONFIG_FLAG(pedValet, PCF_IgnoreBeingOnFire, TRUE)
	ELSE
		CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[CASINO] - EXT_CUTSCENE_CASINO_CREATE_ASSETS - Creating valet ped.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL INT_CUTSCENE_CASINO_VALET_EXIT_ASSETS_CREATED(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_PED_IN_ANY_VEHICLE(g_SimpleInteriorData.eValetExit.ownerPed)
		CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[CASINO] - INT_CUTSCENE_CASINO_VALET_EXIT_ASSETS_LOADED: ped is in a vehicle. Entrance used = ", GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED())
		
		VEHICLE_INDEX vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(g_SimpleInteriorData.eValetExit.ownerPed)
		
		IF NOT DOES_ENTITY_EXIST(vehPlayerIsIn)
			ASSERTLN("[CASINO] - INT_CUTSCENE_CASINO_VALET_EXIT_ASSETS_LOADED - ped is in a vehicle, but can not grab vehicle reference...?")
		ENDIF
		
		//Create a clone of the driver ped
		PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(vehPlayerIsIn)
		
		IF CUTSCENE_HELP_CLONE_PED(entryAnim.sCutsceneVeh.vehPassengers[CUTSCENE_CAS_PED_DRIVER], pedDriver, TRUE, TRUE)
		AND IS_ENTITY_ALIVE(entryAnim.sCutsceneVeh.vehPassengers[CUTSCENE_CAS_PED_DRIVER])
			CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[CASINO] - INT_CUTSCENE_CASINO_VALET_EXIT_ASSETS_CREATED - Cloned the driver player.")
			
			IF IS_ENTITY_ALIVE(entryAnim.sCutsceneVeh.vehicle)
				SET_DISABLE_MAP_COLLISION(entryAnim.sCutsceneVeh.vehicle)
			ENDIF
			
			IF IS_PED_MALE(entryAnim.sCutsceneVeh.vehPassengers[CUTSCENE_CAS_PED_DRIVER])
				SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_USE_MALE_ANIMS)
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
		
		//Create the valet ped
		IF NOT CREATE_VALET_PED_FOR_CASINO(entryAnim.pedActor, <<926.6437, 50.7593, 70.0961>>)
			RETURN FALSE
		ENDIF
		
		//Create the keys
		entryAnim.objectArray[CUTSCENE_CAS_OBJ_CAR_KEYS] = CREATE_OBJECT(GET_CASINO_VALET_CARKEY_MODEL(), <<926.6437, 50.7593, 75.0961>>, FALSE, FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_CASINO_VALET_CARKEY_MODEL())
		
		IF IS_ENTITY_ALIVE(entryAnim.objectArray[CUTSCENE_CAS_OBJ_CAR_KEYS])
			FREEZE_ENTITY_POSITION(entryAnim.objectArray[CUTSCENE_CAS_OBJ_CAR_KEYS], TRUE)
		ELSE
			CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[CASINO] - INT_CUTSCENE_CASINO_VALET_EXIT_ASSETS_CREATED - Creating car keys.")
			RETURN FALSE
		ENDIF
		
		MODEL_NAMES eLeftDoorModel 	= GET_CASINO_ENTRANCE_DOOR_MODEL(CED_LEFT_SIDE_LEFT_DOOR)
		MODEL_NAMES eRightDoorModel = GET_CASINO_ENTRANCE_DOOR_MODEL(CED_LEFT_SIDE_RIGHT_DOOR)
		
		IF NOT IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_SETUP_MODEL_HIDES)
			CASINO_VALET_EXIT_SCENE_DO_DOOR_MODEL_HIDE(GET_CASINO_ENTRANCE_DOOR_COORDS(CED_LEFT_SIDE_LEFT_DOOR), eLeftDoorModel)
			CASINO_VALET_EXIT_SCENE_DO_DOOR_MODEL_HIDE(GET_CASINO_ENTRANCE_DOOR_COORDS(CED_LEFT_SIDE_RIGHT_DOOR), eRightDoorModel)
			SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_SETUP_MODEL_HIDES)
		ENDIF
		
		//Create the open doors
		IF NOT CASINO_VALET_EXIT_SCENE_CREATE_DOOR(entryAnim.objFakeDoor, GET_CASINO_ENTRANCE_DOOR_COORDS(CED_LEFT_SIDE_LEFT_DOOR), -48.0, eLeftDoorModel)
			RETURN FALSE
		ENDIF
		
		IF NOT CASINO_VALET_EXIT_SCENE_CREATE_DOOR(entryAnim.objFakeFrame, GET_CASINO_ENTRANCE_DOOR_COORDS(CED_LEFT_SIDE_RIGHT_DOOR), 157.0, eRightDoorModel)
			RETURN FALSE
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(eLeftDoorModel)
		SET_MODEL_AS_NO_LONGER_NEEDED(eRightDoorModel)
		
		VECTOR scenePosition 	= << 925.9088, 51.2420, 80.0950 >>
		VECTOR sceneRotation 	= << 0.000, 0.000, 58.0 >>
		STRING sCameraAnim		= GET_ANIM_FOR_CASINO_VALET_EXIT(entryAnim, CVE_CAMERA)
		
		//Scene 1
		INT iSceneDuration = 3500
		SIMPLE_CUTSCENE_ADD_ANIMATED_CAM_SCENE(g_SimpleInteriorData.sCustomCutsceneInt, iSceneDuration, "valet exit", scenePosition, sceneRotation, entryAnim.dictionary, sCameraAnim, TRUE)
		CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[CASINO] - INT_CUTSCENE_CASINO_VALET_EXIT_ASSETS_CREATED - Setup scene data.")
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC INT_CUTSCENE_CASINO_VALET_EXIT_MAINTAIN(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	SET_ENTITY_VISIBLE_IN_CUTSCENE(PLAYER_PED_ID(), FALSE)

	IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(g_SimpleInteriorData.sCustomCutsceneInt, 0)
		
		IF NOT IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_SETUP_RENDERING_CAM)			
			DO_SCREEN_FADE_IN(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
			
			IF IS_ENTITY_ALIVE(entryAnim.pedActor)
			AND IS_ENTITY_ALIVE(entryAnim.sCutsceneVeh.vehPassengers[CUTSCENE_CAS_PED_DRIVER])
			AND DOES_ENTITY_EXIST(entryAnim.objectArray[CUTSCENE_CAS_OBJ_CAR_KEYS])
				
				IF IS_BIT_SET(g_SimpleInteriorData.sCustomCutsceneInt.iBS, SCS_BS_CREATED_SYNC_SCENE)
					STRING sPlayerAnim 	= GET_ANIM_FOR_CASINO_VALET_EXIT(entryAnim, CVE_DRIVER)
					STRING sValetAnim 	= GET_ANIM_FOR_CASINO_VALET_EXIT(entryAnim, CVE_VALET)
					STRING sKeysAnim 	= GET_ANIM_FOR_CASINO_VALET_EXIT(entryAnim, CVE_KEYS)
					STRING sPlayerFace	= GET_ANIM_FOR_CASINO_VALET_EXIT(entryAnim, CVE_DRIVER_FACE)
					STRING sValetFace	= GET_ANIM_FOR_CASINO_VALET_EXIT(entryAnim, CVE_VALET_FACE)
					
					TASK_SYNCHRONIZED_SCENE(entryAnim.sCutsceneVeh.vehPassengers[CUTSCENE_CAS_PED_DRIVER], g_SimpleInteriorData.sCustomCutsceneInt.iSyncSceneID, entryAnim.dictionary, sPlayerAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(entryAnim.pedActor, g_SimpleInteriorData.sCustomCutsceneInt.iSyncSceneID, entryAnim.dictionary, sValetAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(entryAnim.objectArray[CUTSCENE_CAS_OBJ_CAR_KEYS], g_SimpleInteriorData.sCustomCutsceneInt.iSyncSceneID, sKeysAnim, entryAnim.dictionary, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					
					PLAY_FACIAL_ANIM(entryAnim.sCutsceneVeh.vehPassengers[CUTSCENE_CAS_PED_DRIVER], sPlayerFace, entryAnim.dictionary)
					PLAY_FACIAL_ANIM(entryAnim.pedActor, sValetFace, entryAnim.dictionary)
				ELSE
					SCRIPT_ASSERT("INT_CUTSCENE_CASINO_VALET_EXIT_MAINTAIN - Failed to add elements to sync scene!")
				ENDIF
			ENDIF
			
			SET_BIT(entryAnim.iSceneBS, CUTSCENE_BS_CAS_VAL_EXIT_SETUP_RENDERING_CAM)
		ENDIF
			
		IF GET_SYNCHRONIZED_SCENE_PHASE(g_SimpleInteriorData.sCustomCutsceneInt.iSyncSceneID) > 0.0
			FLOAT fPhaseFade = 0.57
			
			IF GET_SELECTED_CASINO_VALET_EXIT_ANIM_VARIATION(entryAnim) = 1
				fPhaseFade = 0.48
			ENDIF
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(g_SimpleInteriorData.sCustomCutsceneInt.iSyncSceneID) >= fPhaseFade
				DO_SCREEN_FADE_OUT(600)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INT_CUTSCENE_CASINO_VALET_EXIT_CLEANUP(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF DOES_ENTITY_EXIST(entryAnim.pedActor)
		DELETE_PED(entryAnim.pedActor)
	ENDIF
	
	IF DOES_ENTITY_EXIST(entryAnim.objFakeDoor)
		DELETE_OBJECT(entryAnim.objFakeDoor)
	ENDIF
	
	IF DOES_ENTITY_EXIST(entryAnim.objFakeFrame)
		DELETE_OBJECT(entryAnim.objFakeFrame)
	ENDIF
	
	IF DOES_ENTITY_EXIST(entryAnim.objectArray[CUTSCENE_CAS_OBJ_CAR_KEYS])
		DELETE_OBJECT(entryAnim.objectArray[CUTSCENE_CAS_OBJ_CAR_KEYS])
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(entryAnim.dictionary)
		REMOVE_ANIM_DICT(entryAnim.dictionary)
	ENDIF
	
	IF DOES_ENTITY_EXIST(entryAnim.sCutsceneVeh.vehicle)
		DELETE_VEHICLE(entryAnim.sCutsceneVeh.vehicle)
	ENDIF
	
	entryAnim.iSceneBS = 0
ENDPROC

FUNC BOOL GET_CASINO_VALET_SERVICE_FINAL_SPAWN_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID, INT iIndex, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF iIndex = 0
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_ENTITY_ALIVE(veh)
				MODEL_NAMES model = GET_ENTITY_MODEL(veh)
				VECTOR vMin, vMax
				FLOAT fVehLength
				GET_MODEL_DIMENSIONS(model, vMin, vMax)
				fVehLength = (vMax.y - vMin.y)
				
				vSpawnPoint = GET_CASINO_VALET_ENTRY_SCENE_VEH_POSITION(fVehLength)
				fSpawnHeading = 328.6163
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iIndex
		CASE 0			vSpawnPoint = <<925.5957, 65.0351, 79.2222>> fSpawnHeading = 328.2000	RETURN TRUE BREAK
		CASE 1			vSpawnPoint = <<925.5957, 65.0351, 79.2222>> fSpawnHeading = 328.2000	RETURN TRUE BREAK
		CASE 2			vSpawnPoint = <<909.0912, 38.6726, 79.6215>> fSpawnHeading = 327.0000	RETURN TRUE BREAK
		CASE 3			vSpawnPoint = <<931.2994, 74.4908, 77.9336>> fSpawnHeading = 329.0000	RETURN TRUE BREAK
		CASE 4			vSpawnPoint = <<902.8402, 28.3589, 78.7044>> fSpawnHeading = 327.0000	RETURN TRUE BREAK
		CASE 5			vSpawnPoint = <<932.5010, 82.4904, 77.9181>> fSpawnHeading = 54.3999	RETURN TRUE BREAK
		CASE 6			vSpawnPoint = <<898.0199, 20.3194, 77.9913>> fSpawnHeading = 328.6000	RETURN TRUE BREAK
		CASE 7			vSpawnPoint = <<913.5620, 77.4460, 78.0300>> fSpawnHeading = 320.7990	RETURN TRUE BREAK
		CASE 8			vSpawnPoint = <<907.2730, 69.9110, 77.9580>> fSpawnHeading = 320.1990	RETURN TRUE BREAK
		CASE 9			vSpawnPoint = <<900.2680, 61.3750, 77.8810>> fSpawnHeading = 319.9990	RETURN TRUE BREAK
		CASE 10			vSpawnPoint = <<894.3190, 54.1570, 77.8220>> fSpawnHeading = 319.9990	RETURN TRUE BREAK
		CASE 11			vSpawnPoint = <<888.4190, 47.2480, 77.5710>> fSpawnHeading = 317.1990	RETURN TRUE BREAK
		CASE 12			vSpawnPoint = <<882.2600, 40.5070, 77.4810>> fSpawnHeading = 317.5990	RETURN TRUE BREAK
		CASE 13			vSpawnPoint = <<871.4950, 42.0860, 77.3620>> fSpawnHeading = 135.1990	RETURN TRUE BREAK
		CASE 14			vSpawnPoint = <<878.7260, 49.6890, 77.3730>> fSpawnHeading = 136.3990	RETURN TRUE BREAK
		CASE 15			vSpawnPoint = <<885.7720, 57.9520, 77.6030>> fSpawnHeading = 139.5990	RETURN TRUE BREAK
		CASE 16			vSpawnPoint = <<892.4330, 65.7680, 77.7470>> fSpawnHeading = 139.5990	RETURN TRUE BREAK
		CASE 17			vSpawnPoint = <<898.4750, 73.1150, 77.7910>> fSpawnHeading = 139.5990	RETURN TRUE BREAK
		CASE 18			vSpawnPoint = <<904.6320, 80.7170, 77.8610>> fSpawnHeading = 139.5990	RETURN TRUE BREAK
		CASE 19			vSpawnPoint = <<910.2800, 87.5130, 77.9230>> fSpawnHeading = 139.5990	RETURN TRUE BREAK
		CASE 20			vSpawnPoint = <<916.9310, 95.3610, 78.0150>> fSpawnHeading = 139.5990	RETURN TRUE BREAK
		CASE 21			vSpawnPoint = <<864.8210, 35.1780, 77.4990>> fSpawnHeading = 134.9990	RETURN TRUE BREAK
		CASE 22			vSpawnPoint = <<923.6880, 103.4760, 78.0730>> fSpawnHeading = 141.2000	RETURN TRUE BREAK
		CASE 23			vSpawnPoint = <<929.4290, 110.6700, 78.5380>> fSpawnHeading = 141.2000	RETURN TRUE BREAK
		CASE 24			vSpawnPoint = <<935.7090, 118.3410, 79.1820>> fSpawnHeading = 141.2000	RETURN TRUE BREAK
		CASE 25			vSpawnPoint = <<935.1770, 104.1110, 78.4560>> fSpawnHeading = 320.5990	RETURN TRUE BREAK
		CASE 26			vSpawnPoint = <<941.6120, 111.5400, 79.0790>> fSpawnHeading = 320.5990	RETURN TRUE BREAK
		CASE 27			vSpawnPoint = <<947.4310, 118.6620, 79.5770>> fSpawnHeading = 320.5990	RETURN TRUE BREAK
		CASE 28			vSpawnPoint = <<942.2983, 126.0638, 79.6741>> fSpawnHeading = 140.1996	RETURN TRUE BREAK
		CASE 29			vSpawnPoint = <<857.9811, 29.1198, 77.7929>> fSpawnHeading = 129.9995	RETURN TRUE BREAK
		CASE 30			vSpawnPoint = <<953.7520, 126.3746, 79.8556>> fSpawnHeading = 320.1992	RETURN TRUE BREAK
		CASE 31			vSpawnPoint = <<858.9122, 17.1410, 78.0824>> fSpawnHeading = 316.3988	RETURN TRUE BREAK
		CASE 32			vSpawnPoint = <<851.7980, 9.9069, 78.4125>> fSpawnHeading = 316.3988	RETURN TRUE BREAK
		CASE 33			vSpawnPoint = <<960.2880, 134.6161, 79.8594>> fSpawnHeading = 329.7802	RETURN TRUE BREAK
		CASE 34			vSpawnPoint = <<966.6393, 142.3445, 79.8597>> fSpawnHeading = 329.7802	RETURN TRUE BREAK
		CASE 35			vSpawnPoint = <<844.4919, 2.6800, 78.7837>> fSpawnHeading = 316.6493	RETURN TRUE BREAK
		CASE 36			vSpawnPoint = <<837.6433, -4.9222, 79.1079>> fSpawnHeading = 316.6493	RETURN TRUE BREAK
		CASE 37			vSpawnPoint = <<973.0681, 150.2437, 79.8614>> fSpawnHeading = 329.7802	RETURN TRUE BREAK
		CASE 38			vSpawnPoint = <<831.2288, -12.6651, 79.3907>> fSpawnHeading = 328.1106	RETURN TRUE BREAK
		CASE 39			vSpawnPoint = <<824.9086, -21.2094, 79.5774>> fSpawnHeading = 328.1106	RETURN TRUE BREAK
		CASE 40			vSpawnPoint = <<948.7646, 133.9688, 79.8556>> fSpawnHeading = 138.7283	RETURN TRUE BREAK

	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_CASINO_PROPERTY_EXT_MENU_GRAPHIC()
	RETURN "ShopUI_Title_Casino"
ENDFUNC

PROC SET_SIMPLE_INTERIOR_PLAY_FAKE_ELE_ARIVAL_SOUND()
	IF NOT IS_BIT_SET(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_PLAY_ELEVATOR_ARRIVAL_SOUND)
		DEBUG_PRINTCALLSTACK()
		SET_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_PLAY_ELEVATOR_ARRIVAL_SOUND)
	ENDIF	
ENDPROC

PROC CLEAR_SIMPLE_INTERIOR_PLAY_FAKE_ELE_ARIVAL_SOUND()
	IF IS_BIT_SET(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_PLAY_ELEVATOR_ARRIVAL_SOUND)
		DEBUG_PRINTCALLSTACK()
		CLEAR_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_PLAY_ELEVATOR_ARRIVAL_SOUND)
	ENDIF	
ENDPROC

FUNC BOOL SHOULD_SIMPLE_INTERIOR_PLAY_FAKE_ELE_ARIVAL_SOUND()
	RETURN IS_BIT_SET(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_PLAY_ELEVATOR_ARRIVAL_SOUND)
ENDFUNC

PROC SET_SIMPLE_INTERIOR_VEHICLE_FLAGS_WHEN_LEAVING(VEHICLE_INDEX vehiID,BOOL bDisableStuff)
	//SET_VEH_RADIO_STATION(vehiID,radioStation)
	
	IF bDisableStuff
		SET_ENTITY_VISIBLE(vehiID,FALSE) //hidden for test
		FREEZE_ENTITY_POSITION(vehiID,TRUE)
		SET_ENTITY_COLLISION(vehiID,FALSE)
		PRINTLN("SET_SIMPLE_INTERIOR_VEHICLE_FLAGS_WHEN_LEAVING - disabling everything")
	ELSE
		SET_ENTITY_COLLISION(vehiID,TRUE)
		SET_ENTITY_VISIBLE(vehiID,TRUE) //hidden for test
		FREEZE_ENTITY_POSITION(vehiID,FALSE)
		SET_VEHICLE_LIGHTS(vehiID,NO_VEHICLE_LIGHT_OVERRIDE)
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehiID, FALSE)  
		SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehiID,FALSE)
		SET_VEHICLE_IS_STOLEN(vehiID, FALSE) 
		SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vehiID,FALSE)
		SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehiID,FALSE)		
		SET_ENTITY_CAN_BE_DAMAGED(vehiID,TRUE)
		SET_CAN_USE_HYDRAULICS(vehiID,TRUE)
		MODIFY_VEHICLE_TOP_SPEED(vehiID,0)
		SET_VEHICLE_MAX_SPEED(vehiID,-1)
		SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehiID, TRUE)
		IF GET_ENTITY_MODEL(vehiID) = DELUXO
		OR GET_ENTITY_MODEL(vehiID) = OPPRESSOR2
			SET_DISABLE_HOVER_MODE_FLIGHT(vehiID,FALSE)
			SET_SPECIAL_FLIGHT_MODE_ALLOWED(vehiID,TRUE)
		ENDIF
		STOP_BRINGING_VEHICLE_TO_HALT(vehiID)
		IF GET_VEHICLE_MOD(vehiID, MOD_ENGINE) != -1
			SET_VEHICLE_MOD(vehiID, MOD_ENGINE, GET_VEHICLE_MOD(vehiID, MOD_ENGINE))
		ENDIF
		IF GET_VEHICLE_MOD(vehiID, MOD_GEARBOX) != -1
			SET_VEHICLE_MOD(vehiID, MOD_GEARBOX, GET_VEHICLE_MOD(vehiID, MOD_GEARBOX))
		ENDIF	
		IF GET_VEHICLE_MOD(vehiID, MOD_BRAKES) != -1
			SET_VEHICLE_MOD(vehiID, MOD_BRAKES, GET_VEHICLE_MOD(vehiID, MOD_BRAKES))
		ENDIF
		TOGGLE_VEHICLE_MOD(vehiID , MOD_TOGGLE_TURBO,IS_TOGGLE_MOD_ON(vehiID, MOD_TOGGLE_TURBO))
		PRINTLN("SET_SIMPLE_INTERIOR_VEHICLE_FLAGS_WHEN_LEAVING - enabling everything")
	ENDIF
ENDPROC

PROC SET_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER(INT iFloorIndex, SIMPLE_INTERIORS eInterior)
	PRINTLN("SET_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER - Setting to Floor: ", iFloorIndex, " of simple interior: ", eInterior)
	g_SimpleInteriorData.iFloorNumberForPropertyVehEntry = iFloorIndex
	g_SimpleInteriorData.eInteriorFloorNumberForPropertyVehEntry = eInterior
ENDPROC

FUNC INT GET_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER()
	RETURN g_SimpleInteriorData.iFloorNumberForPropertyVehEntry
ENDFUNC

FUNC BOOL IS_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER_SET()
	RETURN g_SimpleInteriorData.iFloorNumberForPropertyVehEntry > -1
ENDFUNC

FUNC SIMPLE_INTERIORS GET_SMPL_INT_ID_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER()
	RETURN g_SimpleInteriorData.eInteriorFloorNumberForPropertyVehEntry
ENDFUNC

PROC SMPL_INT_ANIM_TOGGLE_ASSETS_READY(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim, BOOL bReady)
	INT index, arraySize
	
	arraySize = COUNT_OF(entryAnim.pedArray)
	REPEAT arraySize index
		IF DOES_ENTITY_EXIST(entryAnim.pedArray[index])
			SET_ENTITY_COLLISION(entryAnim.pedArray[index], bReady)
			SET_ENTITY_VISIBLE(entryAnim.pedArray[index], bReady)
			FREEZE_ENTITY_POSITION(entryAnim.pedArray[index], NOT bReady)
		ENDIF
	ENDREPEAT
	
	arraySize = COUNT_OF(entryAnim.objectArray)
	REPEAT arraySize index
		IF DOES_ENTITY_EXIST(entryAnim.objectArray[index])
			SET_ENTITY_COLLISION(entryAnim.objectArray[index], bReady)
			SET_ENTITY_VISIBLE(entryAnim.objectArray[index], bReady)
			IF NOT IS_ENTITY_STATIC(entryAnim.objectArray[index])
				FREEZE_ENTITY_POSITION(entryAnim.objectArray[index], NOT bReady)
			ENDIF
		ENDIF
	ENDREPEAT
	
	CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_TOGGLE_READY(entryAnim.sCutsceneVeh, bReady)
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_RIVAL_OR_RIVAL_PASSENGER(SIMPLE_INTERIORS eSimpleInteriorID)
	IF GET_SIMPLE_INTERIOR_FROM_FM_GANGOPS_DROPOFF(g_FreemodeDeliveryData.sLocalDropoff.eDropoff) = eSimpleInteriorID
	OR GET_SIMPLE_INTERIOR_FROM_CSH_DROPOFF(g_FreemodeDeliveryData.sLocalDropoff.eDropoff) = eSimpleInteriorID
	OR GB_IS_GLOBAL_CLIENT_BIT2_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_2_PASSENGER_TO_RIVAL_BUSINESS_PLAYER)
		RETURN TRUE
	ENDIF
	
	IF FMC_DROPOFF_IS_FIXER_HQ(g_FreemodeDeliveryData.sLocalDropoff.eDropoff)
	AND FMC_GET_DROPOFF_PROPERTY_ID(g_FreemodeDeliveryData.sLocalDropoff.eDropoff) = eSimpleInteriorID
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_BASIC_SIMPLE_INTERIOR_ENTRY_ANIM_STAGE(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim, INT iStage)
	PRINTLN("SET_BASIC_SIMPLE_INTERIOR_ENTRY_ANIM_STAGE: setting stage from ",entryAnim.basicEntry.iStage, " to ",iStage)
	entryAnim.basicEntry.iStage = iStage
ENDPROC

PROC SETUP_BASIC_SIMPLE_INTERIOR_ENTRY_ANIM_CAM(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	IF DOES_CAM_EXIST(entryAnim.camera)
		DESTROY_CAM(entryAnim.camera)
	ENDIF
	
	entryAnim.camera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
	SET_CAM_PARAMS(entryAnim.camera, entryAnim.cameraPos, entryAnim.cameraRot, entryAnim.cameraFov)
	SET_CAM_FAR_CLIP(entryAnim.camera, 1000)
	SHAKE_CAM(entryAnim.camera, "HAND_SHAKE", entryAnim.fCamShake)
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_PROPERTY, "SETUP_BASIC_SIMPLE_INTERIOR_ENTRY_ANIM_CAM - Setup done.")
	#ENDIF
ENDPROC

PROC CLEANUP_BASIC_SIMPLE_INTERIOR_ENTRY(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	IF entryAnim.basicEntry.iStage > BASIC_SIMPLE_INTERIOR_ENTRY_STAGE_INIT
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(entryAnim.syncSceneID)
			DETACH_SYNCHRONIZED_SCENE(entryAnim.syncSceneID)
		ENDIF
		
		IF DOES_ENTITY_EXIST(entryAnim.pedActor)
			DELETE_PED(entryAnim.pedActor)
		ENDIF
		
		//SET_SIMPLE_INTERIOR_FAKE_DOOR_TO_OPEN(FALSE, details)
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		IF DOES_CAM_EXIST(entryAnim.camera)
			DESTROY_CAM(entryAnim.camera)
		ENDIF
		CLEANUP_MP_CUTSCENE(TRUE, FALSE)
		SET_MULTIHEAD_SAFE(FALSE, TRUE)

		g_SimpleInteriorData.bEntryAnimPlaying = FALSE
		entryAnim.basicEntry.iStage = BASIC_SIMPLE_INTERIOR_ENTRY_STAGE_INIT
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "CLEANUP_BASIC_SIMPLE_INTERIOR_ENTRY - Stopping...")
		#ENDIF
	ENDIF
ENDPROC

PROC BASIC_SIMPLE_INTERIOR_ENTRY_NEARBY_PED_VISIBLE()
	PED_INDEX tmpArray[10]
			
	INT npeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), tmpArray)
	
	INT i
	REPEAT npeds i
		IF DOES_ENTITY_EXIST(tmpArray[i])
		AND IS_ENTITY_ALIVE(tmpArray[i])
		AND IS_PED_ACTIVE_IN_SCENARIO(tmpArray[i])
			SET_ENTITY_VISIBLE_IN_CUTSCENE(tmpArray[i], TRUE, TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL RUN_BASIC_SIMPLE_INTERIOR_ENTRY_ANIM(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	SWITCH entryAnim.basicEntry.iStage
		CASE BASIC_SIMPLE_INTERIOR_ENTRY_STAGE_INIT
			IF DOES_ENTITY_EXIST(entryAnim.pedActor)
			AND NOT IS_PED_INJURED(entryAnim.pedActor)
				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, TRUE)
				START_MP_CUTSCENE(TRUE)
				SET_MULTIHEAD_SAFE(TRUE, TRUE)
				
				REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(entryAnim.pedActor, FALSE), 100.0)
				
				SETUP_BASIC_SIMPLE_INTERIOR_ENTRY_ANIM_CAM(entryAnim)

				
				FINALIZE_HEAD_BLEND(entryAnim.pedActor)
				FREEZE_ENTITY_POSITION(entryAnim.pedActor, FALSE)
				entryAnim.syncSceneID = CREATE_SYNCHRONIZED_SCENE(entryAnim.syncScenePos, entryAnim.syncSceneRot)
				
				TASK_SYNCHRONIZED_SCENE(
					entryAnim.pedActor, 
					entryAnim.syncSceneID,
					entryAnim.dictionary, 
					entryAnim.clip, 
					INSTANT_BLEND_IN,
					INSTANT_BLEND_OUT,
					SYNCED_SCENE_DONT_INTERRUPT,
					RBF_NONE
				)
				
				SET_SYNCHRONIZED_SCENE_PHASE(entryAnim.syncSceneID, entryAnim.startingPhase)
				
				IF NOT IS_VECTOR_ZERO(entryAnim.cameraPos2)
					CLEAR_BIT(entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_BS_UPDATE_CAM_PARAMS)
				ELSE
					SET_BIT(entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_BS_UPDATE_CAM_PARAMS)
				ENDIF
				
				IF DOES_CAM_EXIST(entryAnim.camera)
				AND NOT IS_BIT_SET(entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_BS_UPDATE_CAM_PARAMS)
					SET_CAM_PARAMS(entryAnim.camera, entryAnim.cameraPos2, entryAnim.cameraRot2, entryAnim.cameraFov2, 3000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
					
					SET_BIT(entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_BS_UPDATE_CAM_PARAMS)
				ENDIF
				
				REMOVE_ANIM_DICT(entryAnim.dictionary)
				
				REINIT_NET_TIMER(entryAnim.stPlayTime, TRUE, TRUE)
				SET_BASIC_SIMPLE_INTERIOR_ENTRY_ANIM_STAGE(entryAnim,BASIC_SIMPLE_INTERIOR_ENTRY_RUNNING)
			ELSE
				PRINTLN("RUN_BASIC_SIMPLE_INTERIOR_ENTRY_ANIM ped not ok")
			ENDIF
		BREAK
		CASE BASIC_SIMPLE_INTERIOR_ENTRY_RUNNING
			IF IS_SYNCHRONIZED_SCENE_RUNNING(entryAnim.syncSceneID)
				
				entryAnim.currentPhase = GET_SYNCHRONIZED_SCENE_PHASE(entryAnim.syncSceneID)
				
				IF IS_ENTITY_ALIVE(entryAnim.pedActor)
					REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(entryAnim.pedActor, FALSE), 100.0)
				ENDIF
			
				// Play sound of opening door
				IF NOT IS_STRING_NULL_OR_EMPTY(entryAnim.strOnEnterPhaseSoundSet)
					INT i
					REPEAT 2 i
						IF NOT IS_BIT_SET(entryAnim.iOnEnterSoundsPlayedBS, i) 
						AND NOT IS_STRING_NULL_OR_EMPTY(entryAnim.strOnEnterSoundNames[i])
						AND entryAnim.currentPhase >= entryAnim.fOnEnterSoundPhases[i]
							VECTOR vSoundPos
							IF NOT IS_ENTITY_DEAD(entryAnim.pedActor)
								vSoundPos = GET_ENTITY_COORDS(entryAnim.pedActor)
							ENDIF
							
	//						iWarehouseEntrySoundID[i] = GET_SOUND_ID()
							PLAY_SOUND_FROM_COORD(-1, entryAnim.strOnEnterSoundNames[i], vSoundPos, entryAnim.strOnEnterPhaseSoundSet)
							//PLAY_SOUND_FRONTEND(-1, entryAnim.strOnEnterSoundNames[i], entryAnim.strOnEnterPhaseSoundSet)
							
							SET_BIT(entryAnim.iOnEnterSoundsPlayedBS, i)
							#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][ENTRY_ANIM] RUN_SIMPLE_INTERIOR_ENTRY_ANIM - Playing sound ", entryAnim.strOnEnterSoundNames[i])
							#ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				IF NOT IS_BIT_SET(entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_BS_FADED_SCREEN)
					IF (entryAnim.basicEntry.fFadeInPhase > 0
					AND entryAnim.currentPhase > entryAnim.basicEntry.fFadeInPhase)
					OR (entryAnim.basicEntry.iFadeInTime > 0
					AND HAS_NET_TIMER_EXPIRED(entryAnim.stPlayTime, entryAnim.basicEntry.iFadeInTime, TRUE))
						DO_SCREEN_FADE_OUT(500)
						SET_BIT(entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_BS_FADED_SCREEN)
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][ENTRY_ANIM] RUN_SIMPLE_INTERIOR_ENTRY_ANIM - fading out screen ")
						#ENDIF
					ENDIF
				ENDIF

				IF (entryAnim.currentPhase > entryAnim.endingPhase
				AND entryAnim.iDuration	<= 0)
				OR (entryAnim.iDuration	> 0
				AND HAS_NET_TIMER_EXPIRED_ONE_FRAME(entryAnim.stPlayTime, entryAnim.iDuration, TRUE))
				OR entryAnim.currentPhase > 0.99
					IF DOES_ENTITY_EXIST(entryAnim.pedActor)
					AND NOT IS_PED_INJURED(entryAnim.pedActor)
					AND NOT IS_STRING_NULL_OR_EMPTY(entryAnim.dictionaryIdle)
					AND NOT IS_STRING_NULL_OR_EMPTY(entryAnim.clipIdle)
						TASK_PLAY_ANIM(entryAnim.pedActor, entryAnim.dictionaryIdle, entryAnim.clipIdle, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, DEFAULT, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)
					ENDIF
					
					SET_BASIC_SIMPLE_INTERIOR_ENTRY_ANIM_STAGE(entryAnim,BASIC_SIMPLE_INTERIOR_ENTRY_FINISHED)
				ENDIF
			ENDIF
		BREAK
		CASE BASIC_SIMPLE_INTERIOR_ENTRY_FINISHED
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_THIS_SIMPLE_INTERIOR_ENTRY_LOCATE(SIMPLE_INTERIORS eInterior)
	RETURN g_SimpleInteriorData.eInLocateInteriorID = eInterior
ENDFUNC

FUNC BOOL IS_FM_JOB_ENTRY_OF_TYPE_THAT_SHOULD_BLOCK_ACCESS()
	IF GET_FM_JOB_ENTERY_TYPE() != ciMISSION_ENTERY_TYPE_INVALID
	AND GET_FM_JOB_ENTERY_TYPE() != ciMISSION_ENTERY_TYPE_HEIST_QUICK_INVITE
	AND GET_FM_JOB_ENTERY_TYPE() != ciMISSION_ENTERY_TYPE_WALK_IN
	AND GET_FM_JOB_ENTERY_TYPE() != ciMISSION_ENTERY_TYPE_PHONE_RANDOM
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PASSANGERS_PLAYERS()
	INT iVehSeat
	VEHICLE_SEAT vSeat
	PED_INDEX pedPassanger
	VEHICLE_INDEX playerVeh =  GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	FOR iVehSeat = VS_FRONT_RIGHT TO VS_EXTRA_RIGHT_3
		vSeat = INT_TO_ENUM(VEHICLE_SEAT, iVehSeat)
		pedPassanger = GET_PED_IN_VEHICLE_SEAT(playerVeh, vSeat)
		IF DOES_ENTITY_EXIST(pedPassanger)
		AND NOT IS_ENTITY_DEAD(pedPassanger)
			IF NOT IS_PED_A_PLAYER(pedPassanger)
				RETURN FALSE
			ENDIF	
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_JOB_LIST_INVITE_BEEN_ACCEPTED()
	INT i
	
	IF g_numMPRVInvites > 0
		REPEAT g_numMPRVInvites i
			IF i < MAX_MP_JOBLIST_ENTRIES
			AND g_sJLRVInvitesMP[i].jrviAccepted
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_JOB_ENTRY_TYPE_BLOCK_SIMPLE_INTERIOR_TRANSITION()
	IF IS_FM_JOB_ENTRY_OF_TYPE_THAT_SHOULD_BLOCK_ACCESS()
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
	AND NOT SHOULD_ALLOW_CASINO_ENTRY_DURING_MISSION()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_SIMPLE_INTERIOR_ENTRY_FOR_ON_CALL()
	IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
	OR AM_I_A_TRANSITION_SESSION_PLAYER_ON_CALL()
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CASINO_NIGHTCLUB_ENTRY_BLOCKED_FOR_BEING_KICKED()
	IF HAS_NET_TIMER_STARTED(g_stCasinoNightClubKickTime) 
	AND NOT HAS_NET_TIMER_EXPIRED(g_stCasinoNightClubKickTime, ciNIGHTCLUB_KICK_DURATION_MS)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_USE_ELEVATOR_ENTRY_TO_CASINO_CLUB()
	IF IS_CASINO_NIGHTCLUB_ENTRY_BLOCKED_FOR_BEING_KICKED()
	OR NOT DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
	OR IS_BIT_SET(g_SimpleInteriorData.sGlobalController.iBS2, SIMPLE_INTERIOR_CONTROL_BS2_DISABLE_CASINO_CLUB_ENTRY_FOR_PC)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC
