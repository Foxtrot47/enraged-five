USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "script_network.sch"




// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_Trigger_Overview.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Access to some Game Overview information used during Mission Triggering decision making.
//
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************





// =============================================================================================================================================================
//      Mission Triggering Overview Consts
// =============================================================================================================================================================

CONST_INT	MT_OVERVIEW_BLOCK_MISSIONS_TIMER_msec	3000


// -----------------------------------------------------------------------------------------------------------

// MP Mission Triggering Overview bitflags - describes various aspects of this player's game that influences mission triggering decision making
CONST_INT	MT_OVERVIEW_TEMP_BLOCK_MISSIONS			0	// TRUE if the timer is still active that blocks missions because an ambient activity is launching, otherwise FALSE





// =============================================================================================================================================================
//      Some access functions for Mission Triggering use
// =============================================================================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Clear Mission View Overview Variables
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear the player global broadcast and local variables - this is done on initialisation of the mission triggering system
PROC Clear_This_Players_Mission_Overview_Variables()

	IF NOT (IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID()))
		NET_PRINT("...KGM MP [Ambient]: ERROR: Mission Overview Variables weren't cleared because GBD ARRAY not set")
		EXIT
	ENDIF
	
	// Player Broadcast Data
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sOverviewMP.mtoBitflags	= CLEAR_ALL_MP_MT_OVERVIEW_BITFLAGS
	
	// Local Data
	g_sMissionOverviewLocalMP.mtolTempBlockTimeout					= GET_NETWORK_TIME()
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Temporarily Block Missions
//		Useful if a player is launching an ambient activity as a temporary crossover blockage
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if player is temporarily blocked from launching missions
//
// INPUT PARAMS:		paramPlayerID		The Player Index being checked
// RETURN VALUE:		BOOL				TRUE if the 'Temp Block Missions' flag is set, otherwise FALSE
FUNC BOOL Is_Player_Using_Temporary_Mission_Block(PLAYER_INDEX paramPlayerID)

	IF NOT (IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(paramPlayerID))
		RETURN FALSE
	ENDIF

	RETURN (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].sOverviewMP.mtoBitflags, MT_OVERVIEW_TEMP_BLOCK_MISSIONS))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if this player is temporarily blocked from launching missions
//
// RETURN VALUE:		BOOL			TRUE if the 'Temp Block Missions' flag is set, otherwise FALSE
FUNC BOOL Is_This_Player_Using_Temporary_Mission_Block()
	RETURN (Is_Player_Using_Temporary_Mission_Block(PLAYER_ID()))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Indicates that the player should temporarily not be interrupted by Mission Triggering requests
PROC Set_Temporary_Mission_Block_For_This_Player()

	#IF IS_DEBUG_BUILD
		IF NOT (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sOverviewMP.mtoBitflags, MT_OVERVIEW_TEMP_BLOCK_MISSIONS))
			NET_PRINT("...KGM MP [Ambient]: Info For Mission Triggering: Setting temporary block of mission triggering (it will automatically timeout) [From: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_PRINT("]")
			NET_NL()
		ENDIF
	#ENDIF
	
	// Set the 'Temp Block Missions' flag
	SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sOverviewMP.mtoBitflags, MT_OVERVIEW_TEMP_BLOCK_MISSIONS)
	
	// Set the timeout timer
	g_sMissionOverviewLocalMP.mtolTempBlockTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), MT_OVERVIEW_BLOCK_MISSIONS_TIMER_msec)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Automatically unblock the temporary mission launching block whent he timer expires
PROC Maintain_Temporary_Mission_Block()

	IF NOT (IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID()))
		EXIT
	ENDIF

	IF NOT (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sOverviewMP.mtoBitflags, MT_OVERVIEW_TEMP_BLOCK_MISSIONS))
		EXIT
	ENDIF
	
	// Check for timer expiry
	IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sMissionOverviewLocalMP.mtolTempBlockTimeout))
		EXIT
	ENDIF

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [Ambient]: Info For Mission Triggering: Temporary Mission Block has timed out - Removing Block")
		NET_NL()
	#ENDIF
	
	// Clear the 'on ambient activity flag' and the local timer
	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].sOverviewMP.mtoBitflags, MT_OVERVIEW_TEMP_BLOCK_MISSIONS)
	g_sMissionOverviewLocalMP.mtolTempBlockTimeout = GET_NETWORK_TIME()

ENDPROC

// ===========================================================================================================
//      Mission At Coords Option Bitflag: Invite Accepted
// ===========================================================================================================

// PURPOSE:	Check if an Invite to the mission has been accepted
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
// RETURN VALUE:			BOOL				TRUE if an Invite to the mission has been accepted, otherwise FALSE
FUNC BOOL Has_MissionsAtCoords_Mission_Invite_Been_Accepted(INT paramSlot)
	RETURN (IS_BIT_SET(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_INVITE_ACCEPTED))
ENDFUNC


// PURPOSE:	Check if there is a ContentID stored for an Invite Acceptance
//
// RETURN VALUE:			BOOL				TRUE if there is a stored ContentID, otherwise FALSE
FUNC BOOL Is_There_A_Most_Recent_Invite_Accepted_ContentID()
	RETURN (g_sAtCoordsControlsMP_AddTU.matccInviteAcceptContentIdHash != 0)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the ContentID Hash of the mission where the Invite Accepted flag has just been set
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
//
// NOTES:	Always overwrites existing value
PROC Set_MissionsAtCoords_Most_Recent_Invite_Accepted_ContentID(INT paramSlot)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(".KGM [At Coords]: ")
		NET_PRINT_TIME()
		PRINTSTRING("Invite Accepted ContentID - Set. Slot: ")
		PRINTINT(paramSlot)
		PRINTSTRING(" -")
	#ENDIF

	INT slotContentIdHash = g_sAtCoordsMP[paramSlot].matcIdCloudnameHash
	
	// Debug Output details of the value already stored (if there is one)
	#IF IS_DEBUG_BUILD
		IF (Is_There_A_Most_Recent_Invite_Accepted_ContentID())
			PRINTSTRING(" REPLACE EXISTING HASH: ")
			PRINTINT(g_sAtCoordsControlsMP_AddTU.matccInviteAcceptContentIdHash)
		ENDIF
	#ENDIF
	
	// Store the new details and the safety timeout
	g_sAtCoordsControlsMP_AddTU.matccInviteAcceptContentIdHash	= slotContentIdHash
	g_sAtCoordsControlsMP_AddTU.matccInviteAcceptTimeout		= GET_GAME_TIMER() + MATC_INVITE_ACCEPTED_SAFETY_TIMEOUT_msec
	
	#IF IS_DEBUG_BUILD
		g_sAtCoordsControlsMP_AddTU.matccDebugInviteAcceptContentID = g_sAtCoordsMP[paramSlot].matcMissionIdData.idCloudFilename
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING(" NEW HASH: ")
		PRINTINT(g_sAtCoordsControlsMP_AddTU.matccInviteAcceptContentIdHash)
		PRINTSTRING(" - ")
		PRINTSTRING(g_sAtCoordsMP[paramSlot].matcMissionIdData.idCloudFilename)
		PRINTSTRING(" [safety Timeout: ")
		PRINTINT(MATC_INVITE_ACCEPTED_SAFETY_TIMEOUT_msec)
		PRINTSTRING(" msec]")
		PRINTNL()
	#ENDIF
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that an invite to the mission has been accepted
//
// INPUT PARAMS:			paramSlot			The array position for the mission within the Mission At Coords array
PROC Set_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(INT paramSlot)

	IF (Has_MissionsAtCoords_Mission_Invite_Been_Accepted(paramSlot))
		// Set the contentID again, just in case some other contentID replaced this one as the most recently accepted contentID
		Set_MissionsAtCoords_Most_Recent_Invite_Accepted_ContentID(paramSlot)
		
		EXIT
	ENDIF

	SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_INVITE_ACCEPTED)

	// Store the ContentID
	Set_MissionsAtCoords_Most_Recent_Invite_Accepted_ContentID(paramSlot)
	
ENDPROC

