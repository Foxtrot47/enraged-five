//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_DEBUG.sch																						//
// Description: Header file containing debug functionality for all ped aspects.				 							//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_animation.sch"
USING "net_peds_head_tracking.sch"
USING "net_peds_speech.sch"
USING "net_peds_script_management.sch"

#IF IS_DEBUG_BUILD

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════╡ DEBUG WIDGETS - INITIALISE ╞══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC INT GET_PED_CULLING_AREA_ARRAY_ID(PED_AREAS ePedArea)
	RETURN (ENUM_TO_INT(ePedArea)-1)
ENDFUNC

PROC INITIALISE_PED_DEBUG_DATA(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	REPEAT ServerBD.iMaxLocalPeds iPed
		LocalData.PedData[iPed].PedDebugData.iCullingArea						= ENUM_TO_INT(LocalData.PedData[iPed].cullingData.ePedArea)-1
		LocalData.PedData[iPed].PedDebugData.bHideCutscenePed					= IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		LocalData.PedData[iPed].PedDebugData.bHeeledPed							= IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_HEELED_PED)
	ENDREPEAT
	
	REPEAT ServerBD.iMaxNetworkPeds iPed
		ServerBD.NetworkPedData[iPed].Data.PedDebugData.iCullingArea			= ENUM_TO_INT(ServerBD.NetworkPedData[iPed].Data.cullingData.ePedArea)-1
		ServerBD.NetworkPedData[iPed].Data.PedDebugData.bHideCutscenePed		= IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPed].Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		ServerBD.NetworkPedData[iPed].Data.PedDebugData.bHeeledPed				= IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPed].Data.iBS, BS_PED_DATA_HEELED_PED)
	ENDREPEAT
	
	DebugData.ServerData.iMaxLocalPeds 	= ServerBD.iMaxLocalPeds
	DebugData.ServerData.iLayout		= ServerBD.iLayout
	DebugData.ServerData.iLevel			= ServerBD.iLevel
	DebugData.GeneralData.vBasePosition	= <<0.0, 0.0, 0.0>>
	
ENDPROC

PROC INITIALISE_PED_PARENT_DEBUG_DATA(PED_DEBUG_PARENT_PED_DATA &DebugParentPedData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iChildPedID
	INT iParentPedID
	REPEAT MAX_NUM_LOCAL_PARENT_PEDS iParentPedID
		IF (LocalData.ParentPedData[iParentPedID].iParentID != -1)
			DebugParentPedData.TempLocalParentPedData[iParentPedID].iParentID = LocalData.ParentPedData[iParentPedID].iParentID
		ENDIF
		REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPedID
			DebugParentPedData.TempLocalParentPedData[iParentPedID].iChildID[iChildPedID] = -1
			IF (LocalData.ParentPedData[iParentPedID].iChildID[iChildPedID] != -1)
				DebugParentPedData.TempLocalParentPedData[iParentPedID].iChildID[iChildPedID] = LocalData.ParentPedData[iParentPedID].iChildID[iChildPedID]
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	REPEAT MAX_NUM_NETWORK_PARENT_PEDS iParentPedID
		IF (ServerBD.ParentPedData[iParentPedID].iParentID != -1)
			DebugParentPedData.TempNetworkParentPedData[iParentPedID].iParentID = ServerBD.ParentPedData[iParentPedID].iParentID
		ENDIF
		REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPedID
			DebugParentPedData.TempNetworkParentPedData[iParentPedID].iChildID[iChildPedID] = -1
			IF (ServerBD.ParentPedData[iParentPedID].iChildID[iChildPedID] != -1)
				DebugParentPedData.TempNetworkParentPedData[iParentPedID].iChildID[iChildPedID] = ServerBD.ParentPedData[iParentPedID].iChildID[iChildPedID]
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

PROC INITIALISE_PED_DANCING_ANIM_PERCENTAGE_DEBUG_DATA()
	
	INT iActivity 			= 0
	INT iActivityArrayID 	= 0
	INT iPedIntensity 		= 0
	INT iPercentage 		= 0
	CLUB_MUSIC_INTENSITY ePedIntensity
	
	FOR iActivity = ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_01) TO ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_03)
		iActivityArrayID = (iActivity-ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_01))
		IF (iActivityArrayID >= 0 AND iActivityArrayID < MAX_PED_ACTIVITIES_FOR_VARIATION_PERCENTAGES)
			
			FOR iPedIntensity = ENUM_TO_INT(CLUB_MUSIC_INTENSITY_LOW) TO ENUM_TO_INT(CLUB_MUSIC_INTENSITY_TRANCE)
				IF (iPedIntensity != ENUM_TO_INT(CLUB_MUSIC_INTENSITY_HIGH_HANDS))
					ePedIntensity = INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iPedIntensity)
					iPercentage = GET_ANIM_VARIATION_CLIP_PERCENTAGE(INT_TO_ENUM(PED_ACTIVITIES, iActivity), ePedIntensity, FALSE)
					
					SWITCH ePedIntensity
						CASE CLUB_MUSIC_INTENSITY_TRANCE	g_DancingIntensityPercentages[iActivityArrayID].iTrancePercentage 	= iPercentage	BREAK
						CASE CLUB_MUSIC_INTENSITY_LOW		g_DancingIntensityPercentages[iActivityArrayID].iLowPercentage 		= iPercentage	BREAK
						CASE CLUB_MUSIC_INTENSITY_MEDIUM	g_DancingIntensityPercentages[iActivityArrayID].iMediumPercentage 	= iPercentage	BREAK
						CASE CLUB_MUSIC_INTENSITY_HIGH		g_DancingIntensityPercentages[iActivityArrayID].iHighPercentage 	= iPercentage	BREAK
					ENDSWITCH
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ EVENTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC DEBUG_DELETE_LOCAL_SCRIPT_PEDS(PEDS_DATA &Data[], INT iMaxPedsToDelete)
	
	INT iPed
	IF (iMaxPedsToDelete > 0)
		REPEAT iMaxPedsToDelete iPed
			DELETE_LOCAL_SCRIPT_PED(Data[iPed])
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC PROCESS_PED_DEBUG_EVENT_MAX_LOCAL_PEDS(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, INT iMaxLocalPeds)
	DebugData.PerformanceData.vCoords 	= <<-2216.329, 1164.305, -24.255>>
	DebugData.PerformanceData.fHeading 	= 270.0
	
	SWITCH iMaxLocalPeds
		CASE 100
			DebugData.PerformanceData.iGridRows 			= 20
			DebugData.PerformanceData.iGridColumns 			= 5
			DebugData.PerformanceData.bCreatePeds 			= TRUE
		BREAK
		CASE 75
			DebugData.PerformanceData.iGridRows 			= 15
			DebugData.PerformanceData.iGridColumns 			= 5
			DebugData.PerformanceData.bCreatePeds 			= TRUE
		BREAK
		CASE 50
			DebugData.PerformanceData.iGridRows 			= 10
			DebugData.PerformanceData.iGridColumns 			= 5
			DebugData.PerformanceData.bCreatePeds 			= TRUE
		BREAK
		CASE 25
			DebugData.PerformanceData.iGridRows 			= 5
			DebugData.PerformanceData.iGridColumns 			= 5
			DebugData.PerformanceData.bCreatePeds 			= TRUE
		BREAK
		CASE 12
			DebugData.PerformanceData.iGridRows 			= 4
			DebugData.PerformanceData.iGridColumns 			= 3
			DebugData.PerformanceData.bCreatePeds 			= TRUE
		BREAK
		CASE -1
			DEBUG_DELETE_LOCAL_SCRIPT_PEDS(LocalData.PedData, 100)
			DebugData.ServerData.iMaxLocalPeds				= 0
			ServerBD.iMaxLocalPeds 							= 0
			DebugData.PerformanceData.iGridRows				= 0
			DebugData.PerformanceData.iGridColumns			= 0
			DebugData.PerformanceData.iCachedGridRows		= 0
			DebugData.PerformanceData.iCachedGridColumns	= 0
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_PED_DEBUG_EVENTS(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, SCRIPTED_EVENT_TYPES eEventType, INT iEventID)
	
	SWITCH eEventType
		CASE SCRIPT_EVENT_SET_MAX_LOCAL_PEDS
			SCRIPT_EVENT_DATA_SET_MAX_LOCAL_PEDS maxLocalPedsEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, maxLocalPedsEvent, SIZE_OF(maxLocalPedsEvent))
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT() AND NETWORK_IS_PLAYER_A_PARTICIPANT(maxLocalPedsEvent.Details.FromPlayerIndex)
					IF (maxLocalPedsEvent.bDebugPedLocation)
						
						IF (ePedLocation != PED_LOCATION_DEBUG)
							PedDebugScriptManagementData.bRelaunchPedScript = TRUE
							PedDebugScriptManagementData.iMmenuMaxLocalPeds = maxLocalPedsEvent.iMaxLocalPeds
							EXIT
						ENDIF
						
						PROCESS_PED_DEBUG_EVENT_MAX_LOCAL_PEDS(DebugData, ServerBD, LocalData, maxLocalPedsEvent.iMaxLocalPeds)
					ELSE
						IF (ServerBD.iMaxLocalPeds != maxLocalPedsEvent.iMaxLocalPeds)
							ServerBD.iMaxLocalPeds = maxLocalPedsEvent.iMaxLocalPeds
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPT_EVENT_DEBUG_CLUB_MUSIC_RESET_TRACK
			DEBUG_SCRIPT_EVENT_DATA_CLUB_MUSIC_RESET_TRACK debugResetTrackEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, debugResetTrackEvent, SIZE_OF(debugResetTrackEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(debugResetTrackEvent.Details.FromPlayerIndex)
					INT iPedID
					REPEAT MAX_PEDS_TO_UPDATE_DEBUG_UPDATE_PED_DATA iPedID
						IF IS_PEDS_BIT_SET(LocalData.PedData[iPedID].iBS, BS_PED_DATA_DJ_PED)
							CLEAR_PEDS_BIT(LocalData.PedData[iPedID].iBS, BS_PED_DATA_DJ_START_PHASE)
							IF IS_ENTITY_ALIVE(LocalData.PedData[iPedID].PedID)
								CLEAR_PED_TASKS(LocalData.PedData[iPedID].PedID)
							ENDIF
							LocalData.PedData[iPedID].animData.iSyncSceneID = -1
							LocalData.PedData[iPedID].animData.iSumAnimDurationMS = 0
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡ DEBUG WIDGETS - GENERAL ╞════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC RENDER_PED_INFO(PED_DEBUG_DATA &DebugData, PEDS_DATA Data, INT iPed, BOOL bNetworkPed = FALSE)
	
	HUD_COLOURS HudColour
	
	INT iRow = 0
	INT iAlpha = 255
	INT iRed, iGreen, iBlue
	FLOAT fRowZ = 0.06
	VECTOR vActualPedPos, vDrawPos
	TEXT_LABEL_63 str
	
	INT iHighlightedPed = DebugData.HighlightData.iHighlightedLocalPed
	
	IF (bNetworkPed)
		iHighlightedPed = GET_PED_NETWORK_ID(DebugData.HighlightData.iHighlightedNetworkPed)
	ENDIF
	
	IF (iHighlightedPed = iPed)
		HudColour = HUD_COLOUR_PURPLELIGHT
	ELSE
		HudColour = HUD_COLOUR_PURE_WHITE
	ENDIF
	GET_HUD_COLOUR(HudColour, iRed, iGreen, iBlue, iAlpha)
	
	vActualPedPos = Data.vPosition
	IF DOES_ENTITY_EXIST(Data.PedID)
		vActualPedPos = GET_ENTITY_COORDS(Data.PedID, FALSE)
	ENDIF
	vDrawPos = vActualPedPos
	
	IF (VDIST(GET_PLAYER_COORDS(PLAYER_ID()), Data.vPosition) <= MAX_PED_DRAW_DEBUG_DATA_DISTANCE OR VDIST(GET_FINAL_RENDERED_CAM_COORD(), Data.vPosition) <= MAX_PED_DRAW_DEBUG_DATA_DISTANCE)
	
		iRow++
		str = "Coords: "
		str += VECTOR_TO_STRING(Data.vPosition)
		DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "Rotation: "
		str += VECTOR_TO_STRING(Data.vRotation)
		DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "Culling Area: "
		str += DEBUG_GET_PED_AREA_STRING(Data.cullingData.ePedArea)
		DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "Level: "
		str += Data.iLevel
		DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "Activity: "
		str += DEBUG_GET_PED_ACTIVITY_STRING(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
		DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "Model: "
		str += DEBUG_GET_PED_MODEL_STRING(INT_TO_ENUM(PED_MODELS, Data.iPedModel))
		DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
		iRow++
		str = "Ped: "
		str += iPed
		
		IF (bNetworkPed)
			str += " (Network Ped: "
			str += GET_PED_NETWORK_ARRAY_ID(iPed)
			str += ")"
		ENDIF
		
		DRAW_DEBUG_SPAWN_POINT(Data.vPosition, Data.vRotation.z, HudColour, 0.0, 0.5)
		DRAW_DEBUG_TEXT(str, <<vDrawPos.x, vDrawPos.y, vDrawPos.z + (fRowZ* TO_FLOAT(iRow))>>, iRed, iGreen, iBlue, iAlpha)
		
	ENDIF
	
ENDPROC

PROC WARP_TO_PED(PEDS_DATA &Data)
	IF (IS_ENTITY_ALIVE(Data.PedID) AND IS_ENTITY_ALIVE(PLAYER_PED_ID()))
		SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(Data.PedID))
		SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_ENTITY_HEADING(Data.PedID))
	ENDIF
ENDPROC

PROC WARP_TO_HIGHLIGHTED_PED(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, BOOL bNetworkPed = FALSE)
	INT iHighlightedPed = DebugData.HighlightData.iHighlightedLocalPed
	
	IF (bNetworkPed)
		iHighlightedPed = DebugData.HighlightData.iHighlightedNetworkPed
	ENDIF
	
	IF (bNetworkPed)
		IF (iHighlightedPed >= 0 AND iHighlightedPed < MAX_NUM_TOTAL_NETWORK_PEDS)
			WARP_TO_PED(ServerBD.NetworkPedData[DebugData.HighlightData.iHighlightedNetworkPed].Data)
		ENDIF
	ELSE
		IF (iHighlightedPed >= 0 AND iHighlightedPed < MAX_NUM_TOTAL_LOCAL_PEDS)
			WARP_TO_PED(LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed])
		ENDIF
	ENDIF
ENDPROC

FUNC TEXT_LABEL_63 GET_DEBUG_FILE_NAME(PED_LOCATIONS ePedLocation, INT iLayout)
	
	TEXT_LABEL_63 tlFile = "ped_data_"
	INT iYear, iMonth, iDay, iHour, iMins, iSec
	
	SWITCH ePedLocation
		CASE PED_LOCATION_ISLAND
			tlFile += "island"
		BREAK
		CASE PED_LOCATION_SUBMARINE
			tlFile += "submarine"
		BREAK
		#IF FEATURE_CASINO_NIGHTCLUB
		CASE PED_LOCATION_CASINO_NIGHTCLUB
			tlFile += "casino_nightclub"
		BREAK
		#ENDIF
		#IF FEATURE_TUNER
		CASE PED_LOCATION_AUTO_SHOP
			tlFile += "auto_shop"
		BREAK
		CASE PED_LOCATION_CAR_MEET
			tlFile += "car_meet"
		BREAK
		CASE PED_LOCATION_CAR_MEET_SANDBOX
			tlFile += "car_meet_sandbox"
		BREAK
		CASE PED_LOCATION_FIXER_HQ_HAWICK
		CASE PED_LOCATION_FIXER_HQ_ROCKFORD
		CASE PED_LOCATION_FIXER_HQ_SEOUL
		CASE PED_LOCATION_FIXER_HQ_VESPUCCI
			tlFile += "fixer_hq"
		BREAK
		CASE PED_LOCATION_MUSIC_STUDIO
			tlFile += "music_studio"
		BREAK
		#ENDIF
		#IF FEATURE_DLC_2_2022
		CASE PED_LOCATION_JUGGALO_HIDEOUT
			tlFile += "juggalo_hideout"
		BREAK
		#ENDIF
	ENDSWITCH
	
	tlFile += "/"
	tlFile += "layout"
	tlFile += iLayout
	tlFile += "_"
	
	GET_LOCAL_TIME(iYear, iMonth, iDay, iHour, iMins, iSec)
	
	IF (iHour < 10)
		tlFile += 0
	ENDIF
	tlFile += iHour
	tlFile += "."
	IF (iMins < 10)
		tlFile += 0
	ENDIF
	tlFile += iMins
	tlFile += "."
	IF (iSec < 10)
		tlFile += 0
	ENDIF
	tlFile += iSec
	tlFile += "_"
	
	tlFile += iDay
	tlFile += "."
	tlFile += iMonth
	tlFile += "."
	tlFile += iYear
	tlFile += ".txt"
	
	RETURN tlFile
ENDFUNC

PROC OUTPUT_PED_LAYOUT_AREA_DATA(PEDS_DATA &Data, STRING sPath, TEXT_LABEL_63 tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		IF bSetPedArea", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("			Data.cullingData.ePedArea 								= ", sPath, tlFile)
	
	TEXT_LABEL_63 tlPedArea = DEBUG_GET_PED_AREA_STRING(Data.cullingData.ePedArea)
	SWITCH Data.cullingData.ePedArea
		CASE PED_AREA_INVALID
		CASE PED_AREA_TOTAL
		CASE PED_AREA_TWO
			tlPedArea = DEBUG_GET_PED_AREA_STRING(PED_AREA_ONE)
		BREAK
	ENDSWITCH
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlPedArea, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDIF", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
ENDPROC

PROC OUTPUT_PED_LAYOUT_PARENT_DATA(SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, INT iPed, TEXT_LABEL_63 tlSpacing, STRING sPath, TEXT_LABEL_63 tlFile, BOOL bNetworkPed = FALSE)
	
	INT iChildPedID
	INT iParentPedID
	
	IF (bNetworkPed)
		// Check if current ped is a parent ped
		IF (ServerBD.iParentPedID[iPed] != -1)
			REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPedID
				IF (ServerBD.ParentPedData[ServerBD.iParentPedID[iPed]].iChildID[iChildPedID] != -1)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
					SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)", sPath, tlFile)
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
					BREAKLOOP
				ENDIF
			ENDREPEAT
		ELSE
			// Check if current ped is a child ped
			REPEAT MAX_NUM_NETWORK_PARENT_PEDS iParentPedID
				IF (ServerBD.ParentPedData[iParentPedID].iParentID != -1)
					REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPedID
						IF (ServerBD.ParentPedData[iParentPedID].iChildID[iChildPedID] = iPed)
							SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
							SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)", sPath, tlFile)
							SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
			ENDREPEAT
		ENDIF
	ELSE
		// Check if current ped is a parent ped
		IF (LocalData.iParentPedID[iPed] != -1)
			REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPedID
				IF (LocalData.ParentPedData[LocalData.iParentPedID[iPed]].iChildID[iChildPedID] != -1)
					SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
					SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)", sPath, tlFile)
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
					BREAKLOOP
				ENDIF
			ENDREPEAT
		ELSE
			// Check if current ped is a child ped
			REPEAT MAX_NUM_LOCAL_PARENT_PEDS iParentPedID
				IF (LocalData.ParentPedData[iParentPedID].iParentID != -1)
					REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPedID
						IF (LocalData.ParentPedData[iParentPedID].iChildID[iChildPedID] = iPed)
							SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
							SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)", sPath, tlFile)
							SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL DEBUG_IS_PED_HIDDEN_IN_CUTSCENE_WITH_CONDITION(PED_LOCATIONS ePedLocation, INT iPedID, TEXT_LABEL_63 &tlConditionStringOne, TEXT_LABEL_63 &tlConditionStringTwo)
	
	SWITCH ePedLocation
		CASE PED_LOCATION_ISLAND
			SWITCH iPedID
				CASE 0
				CASE 1
				CASE 2
				CASE 3
				CASE 4
				CASE 6
				CASE 7
				CASE 20
				CASE 25
				CASE 45
				CASE 47
				CASE 56
				CASE 60
					tlConditionStringOne = "	IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION"
					tlConditionStringTwo = "(PLAYER_ID())"
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DEBUG_IS_PED_SKIPPABLE_WITH_CONDITION(PED_LOCATIONS ePedLocation, INT iPedID, TEXT_LABEL_63 &tlConditionStringOne, TEXT_LABEL_63 &tlConditionStringTwo)
	
	SWITCH ePedLocation
		CASE PED_LOCATION_ISLAND
			SWITCH iPedID
				CASE 119
				CASE 120
				CASE 121
				CASE 122
					tlConditionStringOne = "	IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION"
					tlConditionStringTwo = "(PLAYER_ID())"
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC OUTPUT_PED_LAYOUT_MAIN_DATA(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, INT iArea, INT iPed, STRING sPath, TEXT_LABEL_63 tlFile, BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(iArea)
	
	VECTOR vBasePosition = GET_PED_LOCAL_COORDS_BASE_POSITION(ePedLocation)
	FLOAT fBaseHeading = GET_PED_LOCAL_HEADING_BASE_HEADING(ePedLocation)
	
	TEXT_LABEL_63 tlSpacing = "			"
	IF (bNetworkPed)
		tlSpacing = "		"
	ENDIF
	
	TEXT_LABEL_63 tlEqualSpacing = " 										"
	IF (bNetworkPed)
		tlEqualSpacing = " 											"
	ENDIF
	
	TEXT_LABEL_63 tlClothingDrawableSpacing = "								"
	IF (bNetworkPed)
		tlClothingDrawableSpacing = " 								"
	ENDIF
	
	TEXT_LABEL_63 tlClothingTextureSpacing = " 								"
	IF (bNetworkPed)
		tlClothingTextureSpacing = " 									"
	ENDIF
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("CASE ", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(DEBUG_GET_PED_AREA_STRING(PED_AREA_PERMANENT), sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("CASE ", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(DEBUG_GET_PED_AREA_STRING(PED_AREA_ONE), sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("CASE ", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(DEBUG_GET_PED_AREA_STRING(PED_AREA_TWO), sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPedModel", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlEqualSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("= ENUM_TO_INT(", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(DEBUG_GET_PED_MODEL_STRING(INT_TO_ENUM(PED_MODELS, Data.iPedModel)), sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(")", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iActivity", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlEqualSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("= ENUM_TO_INT(", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(DEBUG_GET_PED_ACTIVITY_STRING(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity)), sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(")", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iLevel", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlEqualSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("= ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iLevel, sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPackedDrawable[0]", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlClothingDrawableSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("= ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPackedDrawable[0], sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPackedDrawable[1]", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlClothingDrawableSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("= ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPackedDrawable[1], sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPackedDrawable[2]", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlClothingDrawableSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("= ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPackedDrawable[2], sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPackedTexture[0]", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlClothingTextureSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("= ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPackedTexture[0], sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPackedTexture[1]", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlClothingTextureSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("= ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPackedTexture[1], sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.iPackedTexture[2]", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlClothingTextureSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("= ", sPath, tlFile)
	SAVE_INT_TO_NAMED_DEBUG_FILE(Data.iPackedTexture[2], sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.vPosition", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlEqualSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("= ", sPath, tlFile)
	
	IF NOT IS_VECTOR_ZERO(vBasePosition)
		// Output the peds local coords
		VECTOR vLocalCoords = TRANSFORM_WORLD_COORDS_TO_LOCAL_COORDS(vBasePosition, fBaseHeading, Data.vPosition)
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(vLocalCoords, sPath, tlFile)
	ELSE
		// Output the peds world coords
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(Data.vPosition, sPath, tlFile)
	ENDIF
	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	Data.vRotation", sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlEqualSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("= ", sPath, tlFile)
	
	IF (fBaseHeading != -1.0)
		// Ouput the peds local heading
		FLOAT fLocalHeading = TRANSFORM_WORLD_HEADING_TO_LOCAL_HEADING(fBaseHeading, Data.vRotation.z)
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(<<Data.vRotation.x, Data.vRotation.y, fLocalHeading>>, sPath, tlFile)
	ELSE
		// Output the peds world heading
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(Data.vRotation, sPath, tlFile)
	ENDIF
	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	CLEAR_PEDS_BITSET(Data.iBS)", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	TEXT_LABEL_63 tlConditionStringOne = ""
	TEXT_LABEL_63 tlConditionStringTwo = ""
	IF DEBUG_IS_PED_SKIPPABLE_WITH_CONDITION(ePedLocation, iPed, tlConditionStringOne, tlConditionStringTwo)
		IF NOT IS_STRING_NULL_OR_EMPTY(tlConditionStringOne)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(tlConditionStringOne, sPath, tlFile)
			IF NOT IS_STRING_NULL_OR_EMPTY(tlConditionStringTwo)
				SAVE_STRING_TO_NAMED_DEBUG_FILE(tlConditionStringTwo, sPath, tlFile)
			ENDIF
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)", sPath, tlFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDIF", sPath, tlFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)", sPath, tlFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		ENDIF
		
	ELIF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF (bNetworkPed)
		IF (ServerBD.iHeadTrackingPedID[iPed] != -1)
			IF (DebugData.HeadTrackingData.NetworkHeadTrackingData[ServerBD.iHeadTrackingPedID[iPed]].bHeadTrackCoords)
				SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)", sPath, tlFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
				
			ELIF (DebugData.HeadTrackingData.NetworkHeadTrackingData[ServerBD.iHeadTrackingPedID[iPed]].bHeadTrackingNearestPlayer)
				SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)", sPath, tlFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
			ENDIF
		ENDIF
	ELSE
		IF (LocalData.iHeadTrackingPedID[iPed] != -1)
			IF (DebugData.HeadTrackingData.LocalHeadTrackingData[LocalData.iHeadTrackingPedID[iPed]].bHeadTrackCoords)
				SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)", sPath, tlFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
				
			ELIF (DebugData.HeadTrackingData.LocalHeadTrackingData[LocalData.iHeadTrackingPedID[iPed]].bHeadTrackingNearestPlayer)
				SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)", sPath, tlFile)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_SPEECH)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF DEBUG_IS_PED_HIDDEN_IN_CUTSCENE_WITH_CONDITION(ePedLocation, iPed, tlConditionStringOne, tlConditionStringTwo)
		IF NOT IS_STRING_NULL_OR_EMPTY(tlConditionStringOne)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(tlConditionStringOne, sPath, tlFile)
			IF NOT IS_STRING_NULL_OR_EMPTY(tlConditionStringTwo)
				SAVE_STRING_TO_NAMED_DEBUG_FILE(tlConditionStringTwo, sPath, tlFile)
			ENDIF
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)", sPath, tlFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDIF", sPath, tlFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)", sPath, tlFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		ENDIF
		
	ELIF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HEELED_PED)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PARTNER_PED)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARTNER_PED)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_NO_ANIMATION)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_NO_ANIMATION)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_PATROL)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_PATROL)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_DJ_PED)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDIF", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_FREEZE_PED)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FREEZE_PED)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_VFX)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHANGE_CAPSULE_SIZE)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHANGE_CAPSULE_SIZE)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	IF (bNetworkPed)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_NETWORK_ANIMS)", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
	
	OUTPUT_PED_LAYOUT_PARENT_DATA(ServerBD, LocalData, iPed, tlSpacing, sPath, tlFile, bNetworkPed)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE(tlSpacing, sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("BREAK", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
ENDPROC

PROC SET_PED_DEBUG_COUNT_LEVEL_DATA(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	REPEAT MAX_NUM_PED_LEVELS iPed
		DebugData.ServerData.iLevelCount[iPed] = 0
	ENDREPEAT
	
	REPEAT ServerBD.iMaxLocalPeds iPed
		IF (LocalData.PedData[iPed].iLevel >= 0 AND LocalData.PedData[iPed].iLevel < MAX_NUM_PED_LEVELS)
			IF NOT IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_SKIP_PED)
				DebugData.ServerData.iLevelCount[LocalData.PedData[iPed].iLevel]++
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT ServerBD.iMaxNetworkPeds iPed
		IF (ServerBD.NetworkPedData[iPed].Data.iLevel >= 0 AND ServerBD.NetworkPedData[iPed].Data.iLevel < MAX_NUM_PED_LEVELS)
			IF NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPed].Data.iBS, BS_PED_DATA_SKIP_PED)
				DebugData.ServerData.iLevelCount[ServerBD.NetworkPedData[iPed].Data.iLevel]++
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC UPDATE_ACTIVE_LOCAL_PED_DEBUG_VALUES(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, INT iPed)
	
	// Reset anim data as activity has changed
	ANIM_PED_DATA blankAnimPedData
	LocalData.PedData[iPed].animData = blankAnimPedData
	
	// Level
	// SET_PED_DEBUG_COUNT_LEVEL_DATA(DebugData, ServerBD, LocalData)
	
	// Culling Area
	IF (LocalData.PedData[iPed].PedDebugData.iCullingArea != GET_PED_CULLING_AREA_ARRAY_ID(LocalData.PedData[iPed].cullingData.ePedArea))
		
		// Reset ped data for this ped
		RESET_PED_DATA(LocalData.PedData[iPed])
		
		// Update ped culling area
		LocalData.PedData[iPed].cullingData.ePedArea = INT_TO_ENUM(PED_AREAS, LocalData.PedData[iPed].PedDebugData.iCullingArea+1)
		LocalData.eCullingArea = INT_TO_ENUM(PED_AREAS, LocalData.PedData[iPed].PedDebugData.iCullingArea+1)
		
		// Regrab lost data
		SET_PED_DATA(ePedLocation, ServerBD, LocalData.PedData[iPed], iPed, ServerBD.iLayout, FALSE)
		
	ENDIF
	
	// Cutscene Ped
	IF (LocalData.PedData[iPed].PedDebugData.bHideCutscenePed)
		IF NOT IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
			SET_PEDS_BIT(LocalData.PedData[iPed].iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		ENDIF
	ELSE
		IF IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
			CLEAR_PEDS_BIT(LocalData.PedData[iPed].iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		ENDIF
	ENDIF
	
	// Heeled Ped
	IF (LocalData.PedData[iPed].PedDebugData.bHeeledPed)
		IF NOT IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_HEELED_PED)
			SET_PEDS_BIT(LocalData.PedData[iPed].iBS, BS_PED_DATA_HEELED_PED)
		ENDIF
	ELSE
		IF IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_HEELED_PED)
			CLEAR_PEDS_BIT(LocalData.PedData[iPed].iBS, BS_PED_DATA_HEELED_PED)
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_ACTIVE_NETWORK_PED_DEBUG_VALUES(SERVER_PED_DATA &ServerBD, INT iPedID)
	
	// Reset anim data reguardless if the activity has changed
	ANIM_PED_DATA blankAnimPedData
	ServerBD.NetworkPedData[iPedID].Data.animData = blankAnimPedData
	
	// Cutscene Ped
	IF (ServerBD.NetworkPedData[iPedID].Data.PedDebugData.bHideCutscenePed)
		IF NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
			SET_PEDS_BIT(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		ENDIF
	ELSE
		IF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
			CLEAR_PEDS_BIT(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		ENDIF
	ENDIF
	
	// Heeled Ped
	IF (ServerBD.NetworkPedData[iPedID].Data.PedDebugData.bHeeledPed)
		IF NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_HEELED_PED)
			SET_PEDS_BIT(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_HEELED_PED)
		ENDIF
	ELSE
		IF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_HEELED_PED)
			CLEAR_PEDS_BIT(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_HEELED_PED)
		ENDIF
	ENDIF
	
ENDPROC

PROC OUTPUT_PED_LAYOUT_DATA(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, STRING sPath, TEXT_LABEL_63 tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("SWITCH iPed", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	// Local Peds", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	INT iPed
	INT iArea
	REPEAT ServerBD.iMaxLocalPeds iPed
		
		// Make sure we have the latest ped debug values before outputting
		UPDATE_ACTIVE_LOCAL_PED_DEBUG_VALUES(ePedLocation, ServerBD, LocalData, iPed)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPed, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
			
			OUTPUT_PED_LAYOUT_AREA_DATA(LocalData.PedData[iPed], sPath, tlFile)
			
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		SWITCH Data.cullingData.ePedArea", sPath, tlFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
			
			OUTPUT_PED_LAYOUT_MAIN_DATA(ePedLocation, DebugData, ServerBD, LocalData, LocalData.PedData[iPed], iArea, iPed, sPath, tlFile)
			
			SAVE_STRING_TO_NAMED_DEBUG_FILE("		ENDSWITCH", sPath, tlFile)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
			
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDREPEAT
	
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	// Networked Peds", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	TEXT_LABEL_23 tlNetworkPed
	REPEAT ServerBD.iMaxNetworkPeds iPed
		
		// Make sure we have the latest ped debug values before outputting
		UPDATE_ACTIVE_NETWORK_PED_DEBUG_VALUES(ServerBD, iPed)
		
		tlNetworkPed = "NETWORK_PED_ID_"
		tlNetworkPed += iPed
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	CASE ", sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(tlNetworkPed, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
			
			OUTPUT_PED_LAYOUT_AREA_DATA(ServerBD.NetworkPedData[iPed].Data, sPath, tlFile)
			OUTPUT_PED_LAYOUT_MAIN_DATA(ePedLocation, DebugData, ServerBD, LocalData, ServerBD.NetworkPedData[iPed].Data, 0, iPed, sPath, tlFile, TRUE)
			
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	BREAK", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDSWITCH", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
ENDPROC

PROC OUTPUT_PED_HEAD_TRACKING_DATA_MAIN(HEAD_TRACKING_DATA &HeadTrackingData, INT iPed, STRING sPath, TEXT_LABEL_63 tlFile)
	IF (HeadTrackingData.iPedID != -1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		CASE ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPed, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			HeadtrackingData.iPedID							= ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(HeadTrackingData.iPedID, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			HeadtrackingData.iHeadTrackingTime				= ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(HeadTrackingData.iHeadTrackingTime, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			HeadtrackingData.fHeadTrackingRange				= ", sPath, tlFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(HeadTrackingData.fHeadTrackingRange, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			HeadtrackingData.vHeadTrackingCoords			= ", sPath, tlFile)
		SAVE_VECTOR_TO_NAMED_DEBUG_FILE(HeadTrackingData.vHeadTrackingCoords, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_MEDIUM", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		BREAK", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
ENDPROC

PROC OUTPUT_PED_HEAD_TRACKING_DATA(SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, STRING sPath, TEXT_LABEL_63 tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Head Tracking Data:", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("IF (bNetworkPed)", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	SWITCH iArrayID", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	INT iPed
	REPEAT MAX_NUM_NETWORK_HEAD_TRACKING_PEDS iPed
		OUTPUT_PED_HEAD_TRACKING_DATA_MAIN(ServerBD.HeadTrackingData[iPed], iPed, sPath, tlFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDSWITCH", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ELSE", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	SWITCH iArrayID", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	REPEAT MAX_NUM_LOCAL_HEAD_TRACKING_PEDS iPed
		OUTPUT_PED_HEAD_TRACKING_DATA_MAIN(LocalData.HeadTrackingData[iPed], iPed, sPath, tlFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDSWITCH", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDIF", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
ENDPROC

PROC OUTPUT_PED_SPEECH_DATA_MAIN(SPEECH_DATA &SpeechData, INT iPed, STRING sPath, TEXT_LABEL_63 tlFile)
	IF (SpeechData.iPedID != -1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		CASE ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPed, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			SpeechData.iPedID								= ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(SpeechData.iPedID, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			SpeechData.fGreetSpeechDistance					= ", sPath, tlFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(SpeechData.fGreetSpeechDistance, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			SpeechData.fByeSpeechDistance					= ", sPath, tlFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(SpeechData.fByeSpeechDistance, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			SpeechData.fListenDistance						= ", sPath, tlFile)
		SAVE_FLOAT_TO_NAMED_DEBUG_FILE(SpeechData.fListenDistance, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			iSpeechPedID[SpeechData.iPedID]					= iArrayID", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		BREAK", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
ENDPROC

PROC OUTPUT_PED_SPEECH_DATA(SCRIPT_PED_DATA &LocalData, STRING sPath, TEXT_LABEL_63 tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Speech Data:", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("IF (bNetworkPed)", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	SWITCH iArrayID", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	INT iPed
	REPEAT MAX_NUM_NETWORK_PLAYER_SPEECH_PEDS iPed
		OUTPUT_PED_SPEECH_DATA_MAIN(LocalData.NetworkSpeechData[iPed], iPed, sPath, tlFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDSWITCH", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ELSE", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	SWITCH iArrayID", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	REPEAT MAX_NUM_LOCAL_PLAYER_SPEECH_PEDS iPed
		OUTPUT_PED_SPEECH_DATA_MAIN(LocalData.LocalSpeechData[iPed], iPed, sPath, tlFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDSWITCH", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDIF", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
ENDPROC

PROC OUTPUT_PED_PARENT_DATA_MAIN(PARENT_PED_DATA &ParentPedData, INT iPed, STRING sPath, TEXT_LABEL_63 tlFile)
	IF (ParentPedData.iParentID != -1)
	AND (ParentPedData.iChildID[0] != -1 OR ParentPedData.iChildID[1] != -1 OR ParentPedData.iChildID[2] != -1)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		CASE ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(iPed, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			ParentPedData.iParentID							= ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(ParentPedData.iParentID, sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			ParentPedData.iChildID[0]						= ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(ParentPedData.iChildID[0], sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			ParentPedData.iChildID[1]						= ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(ParentPedData.iChildID[1], sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			ParentPedData.iChildID[2]						= ", sPath, tlFile)
		SAVE_INT_TO_NAMED_DEBUG_FILE(ParentPedData.iChildID[2], sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("			iParentPedID[ParentPedData.iParentID]			= iArrayID", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("		BREAK", sPath, tlFile)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	ENDIF
ENDPROC

PROC OUTPUT_PED_PARENT_DATA(SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, STRING sPath, TEXT_LABEL_63 tlFile)
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("Parent Ped Data:", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("IF (bNetworkPed)", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	SWITCH iArrayID", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	INT iPed
	REPEAT MAX_NUM_NETWORK_PARENT_PEDS iPed
		OUTPUT_PED_PARENT_DATA_MAIN(ServerBD.ParentPedData[iPed], iPed, sPath, tlFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDSWITCH", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ELSE", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	SWITCH iArrayID", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
	REPEAT MAX_NUM_LOCAL_PARENT_PEDS iPed
		OUTPUT_PED_PARENT_DATA_MAIN(LocalData.ParentPedData[iPed], iPed, sPath, tlFile)
	ENDREPEAT
	
	SAVE_STRING_TO_NAMED_DEBUG_FILE("	ENDSWITCH", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("ENDIF", sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(sPath, tlFile)
	
ENDPROC

PROC OUTPUT_PED_DATA(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	STRING sPath = "X:/gta5/titleupdate/dev_ng/"
	TEXT_LABEL_63 tlFile = GET_DEBUG_FILE_NAME(ePedLocation, ServerBD.iLayout)
	
	CLEAR_NAMED_DEBUG_FILE(sPath, tlFile)
	OPEN_NAMED_DEBUG_FILE(sPath, tlFile)
		
		OUTPUT_PED_LAYOUT_DATA(ePedLocation, DebugData, ServerBD, LocalData, sPath, tlFile)
		OUTPUT_PED_HEAD_TRACKING_DATA(ServerBD, LocalData, sPath, tlFile)
		OUTPUT_PED_SPEECH_DATA(LocalData, sPath, tlFile)
		OUTPUT_PED_PARENT_DATA(ServerBD, LocalData, sPath, tlFile)
		
	CLOSE_DEBUG_FILE()
	
ENDPROC

PROC UPDATE_PED_DEBUG_SERVER_BROADCAST_DATA(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	ServerBD.iLayout 			= DebugData.ServerData.iLayout
	ServerBD.iMaxLocalPeds 		= GET_LOCAL_PED_TOTAL(ePedLocation, ServerBD)
	ServerBD.iMaxNetworkPeds 	= GET_NETWORK_PED_TOTAL(ePedLocation, ServerBD)
	
	INT iPed
	REPEAT ServerBD.iMaxLocalPeds iPed
		RESET_PED_DATA(LocalData.PedData[iPed])
		SET_PED_DATA(ePedLocation, ServerBD, LocalData.PedData[iPed], iPed, ServerBD.iLayout)
	ENDREPEAT
	REPEAT ServerBD.iMaxNetworkPeds iPed
		RESET_PED_DATA(ServerBD.NetworkPedData[iPed].Data)
		SET_PED_DATA(ePedLocation, ServerBD, ServerBD.NetworkPedData[iPed].Data, GET_PED_NETWORK_ID(iPed), ServerBD.iLayout)
	ENDREPEAT
	
	SET_ALL_PED_PATROL_DATA(ePedLocation, ServerBD, LocalData)
	SET_ALL_PED_HEAD_TRACKING_DATA(ePedLocation, ServerBD, LocalData)
	SET_PED_CONTROLLER_SPEECH_DATA(ePedLocation, ServerBD, LocalData)
	SET_ALL_PED_SPEECH_DATA(ePedLocation, ServerBD, LocalData)
	SET_ALL_PED_PARENT_DATA(ePedLocation, ServerBD, LocalData)
	SET_ALL_PED_VFX_DATA(ServerBD, LocalData)
	
	LocalData.iLocalSpeechPed 			= 0
	LocalData.iLocalSpeechTimeOffsetMS 	= 0
	RESET_NET_TIMER(LocalData.stLocalSpeechTimer)
	
	INITIALISE_PED_DEBUG_DATA(DebugData, ServerBD, LocalData)
	INITIALISE_PED_PARENT_DEBUG_DATA(DebugData.ParentPedData, ServerBD, LocalData)
	
	IF (ServerBD.iLevel != DebugData.ServerData.iLevel)
		ServerBD.iLevel = DebugData.ServerData.iLevel
	ENDIF
	
ENDPROC

PROC UPDATE_PED_GENERAL_DEBUG_WIDGETS(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, INT iPed, BOOL bNetworkPed = FALSE)
	
	IF DOES_TEXT_WIDGET_EXIST(DebugData.GeneralData.twLogSearch)
		SET_CONTENTS_OF_TEXT_WIDGET(DebugData.GeneralData.twLogSearch, "UPDATE_PED_GENERAL_DEBUG_WIDGETS")
	ENDIF
	
	IF (DebugData.GeneralData.bShowDebugData)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		IF (bNetworkPed)
			RENDER_PED_INFO(DebugData, ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(iPed)].Data, iPed, TRUE)
		ELSE
			RENDER_PED_INFO(DebugData, LocalData.PedData[iPed], iPed)
		ENDIF
	ENDIF
	
	IF (DebugData.HighlightData.bWarpToHighlightedLocalPed)
		DebugData.HighlightData.bWarpToHighlightedLocalPed = FALSE
		WARP_TO_HIGHLIGHTED_PED(DebugData, ServerBD, LocalData)
	ENDIF
	
	IF (DebugData.HighlightData.bWarpToHighlightedNetworkPed)
		DebugData.HighlightData.bWarpToHighlightedNetworkPed = FALSE
		WARP_TO_HIGHLIGHTED_PED(DebugData, ServerBD, LocalData, TRUE)
	ENDIF
	
	IF (DebugData.GeneralData.bRecreatePeds)
	OR IS_DEBUG_KEY_JUST_PRESSED(KEY_O, KEYBOARD_MODIFIER_CTRL, "Recreating Peds")
		IF DELETE_ALL_SCRIPT_PEDS(ServerBD, LocalData.PedData)
			DebugData.GeneralData.bRecreatePeds = FALSE
		ENDIF
		UPDATE_PED_DEBUG_SERVER_BROADCAST_DATA(ePedLocation, DebugData, ServerBD, LocalData)
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF (ServerBD.iLevel != DebugData.ServerData.iLevel)
			ServerBD.iLevel = DebugData.ServerData.iLevel
		ENDIF
	ENDIF
	
	IF (DebugData.GeneralData.bOutputData)
		OUTPUT_PED_DATA(ePedLocation, DebugData, ServerBD, LocalData)
		DebugData.GeneralData.bOutputData = FALSE
	ENDIF
	
	IF (DebugData.GeneralData.bOutputAllPedLocalCoords)
		DebugData.GeneralData.bOutputAllPedLocalCoords = FALSE
		
		INT iPedID
		IF (ServerBD.iMaxLocalPeds > 0)
			REPEAT ServerBD.iMaxLocalPeds iPedID
				IF DOES_ENTITY_EXIST(LocalData.PedData[iPedID].PedID)
					VECTOR vLocalCoords = TRANSFORM_WORLD_COORDS_TO_LOCAL_COORDS(DebugData.GeneralData.vBasePosition, DebugData.GeneralData.fBaseHeading, LocalData.PedData[iPedID].vPosition)
					FLOAT fLocalHeading = TRANSFORM_WORLD_HEADING_TO_LOCAL_HEADING(DebugData.GeneralData.fBaseHeading, LocalData.PedData[iPedID].vRotation.z)
					
					VECTOR vWorldCoords = TRANSFORM_LOCAL_COORDS_TO_WORLD_COORDS(DebugData.GeneralData.vBasePosition, DebugData.GeneralData.fBaseHeading, vLocalCoords)
					FLOAT fWorldHeading = TRANSFORM_LOCAL_HEADING_TO_WORLD_HEADING(DebugData.GeneralData.fBaseHeading, fLocalHeading)
					
					BOOL bCoordsValidated = FALSE
					IF ARE_VECTORS_ALMOST_EQUAL(LocalData.PedData[iPedID].vPosition, vWorldCoords)
						bCoordsValidated = TRUE
					ENDIF
					
					BOOL bHeadingValidated = FALSE
					IF (fWorldHeading = LocalData.PedData[iPedID].vRotation.z)
					OR (fWorldHeading > LocalData.PedData[iPedID].vRotation.z - 0.1)
					OR (fWorldHeading < LocalData.PedData[iPedID].vRotation.z + 0.1)
						bHeadingValidated = TRUE
					ENDIF
					
					PRINTLN("[AM_MP_PEDS] UPDATE_PED_GENERAL_DEBUG_WIDGETS - Ped ID: ", iPedID, " Local Coords: ", vLocalCoords, " Local Heading: ", fLocalHeading, " Coords Validated: ", bCoordsValidated, " Heading Validated: ", bHeadingValidated)
				ENDIF
			ENDREPEAT
		ENDIF
		
		PRINTLN("[AM_MP_PEDS] UPDATE_PED_GENERAL_DEBUG_WIDGETS")
	ENDIF
	
	IF (DebugData.GeneralData.bPlacePedsOnGroundProperly)
		DebugData.GeneralData.bPlacePedsOnGroundProperly = FALSE
		
		INT iPeds
		FLOAT fEntityWorldHeight = 0.0
		IF (ServerBD.iMaxLocalPeds > 0)
			REPEAT ServerBD.iMaxLocalPeds iPeds
				IF DOES_ENTITY_EXIST(LocalData.PedData[iPeds].PedID)
					fEntityWorldHeight = GET_ENTITY_HEIGHT(LocalData.PedData[iPeds].PedID, LocalData.PedData[iPeds].vPosition, TRUE, TRUE)
					
					IF IS_PEDS_BIT_SET(LocalData.PedData[iPeds].iBS, BS_PED_DATA_HEELED_PED)
						LocalData.PedData[iPeds].vPosition.z = fEntityWorldHeight-MAX_PED_Z_AXIS_OFFSET_HEELS
					ELSE
						LocalData.PedData[iPeds].vPosition.z = fEntityWorldHeight-MAX_PED_Z_AXIS_OFFSET
					ENDIF
					
					LocalData.PedData[iPeds].PedDebugData.bApplyChanges = TRUE
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
ENDPROC

PROC CREATE_PED_GENERAL_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD)
	
	ADD_WIDGET_BOOL("Enable Debug", DebugData.GeneralData.bEnableDebug)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ADD_WIDGET_STRING("Warning: You are not the host of AM_MP_PEDS! Not all debug will be available to you!")
	ENDIF
	
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP("General")
		ADD_WIDGET_BOOL("Show Debug Data", DebugData.GeneralData.bShowDebugData)
		ADD_WIDGET_BOOL("Recreate Peds (CTRL+O)", DebugData.GeneralData.bRecreatePeds)
		ADD_WIDGET_BOOL("Output Ped Data", DebugData.GeneralData.bOutputData)
		ADD_WIDGET_STRING("Note: Attempts to place the ped on the ground properly. Origin will be the peds waist.")
		ADD_WIDGET_BOOL("Place Peds On Ground Properly", DebugData.GeneralData.bPlacePedsOnGroundProperly)
		ADD_WIDGET_STRING("Note: Data is outputted in a folder called 'ped_data_x' at: X:/gta5/titleupdate/dev_ng/")
		
		START_WIDGET_GROUP("Highlighted Local Ped")
			ADD_WIDGET_INT_SLIDER("Highlighted Local Ped", DebugData.HighlightData.iHighlightedLocalPed, -1, (ServerBD.iMaxLocalPeds-1), 1)
			ADD_WIDGET_BOOL("Warp to Highlighted Local Ped", DebugData.HighlightData.bWarpToHighlightedLocalPed)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Highlighted Network Ped")
			ADD_WIDGET_INT_SLIDER("Highlighted Network Ped", DebugData.HighlightData.iHighlightedNetworkPed, -1, (ServerBD.iMaxNetworkPeds-1), 1)
			ADD_WIDGET_BOOL("Warp to Highlighted Network Ped", DebugData.HighlightData.bWarpToHighlightedNetworkPed)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Convert To Local Coords")
			ADD_WIDGET_VECTOR_SLIDER("Base Position", DebugData.GeneralData.vBasePosition, -99999.9, 99999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Base Heading", DebugData.GeneralData.fBaseHeading, -360.0, 360.0, 0.1)
			ADD_WIDGET_BOOL("Output All Peds Local Coords", DebugData.GeneralData.bOutputAllPedLocalCoords)
			DebugData.GeneralData.twLogSearch = ADD_TEXT_WIDGET("Search in Logs: ")
			ADD_WIDGET_STRING("Tick 'Output Ped Data' after updating base coords and heading in script to output ped data with local coords.")
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════╡ DEBUG WIDGETS - SERVER BROADCAST DATA ╞═════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_PED_SERVER_BROADCAST_DATA_DEBUG_WIDGETS(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP("Server Broadcast Data")
		ADD_WIDGET_INT_READ_ONLY("Max Local Peds", ServerBD.iMaxLocalPeds)
		ADD_WIDGET_INT_READ_ONLY("Max Network Peds", ServerBD.iMaxNetworkPeds)
		
		ADD_WIDGET_STRING("Warning: Changing Layout will lose any changes to the ped data!")
		DebugData.ServerData.iLayout = CLAMP_INT(DebugData.ServerData.iLayout, 0, GET_SERVER_PED_LAYOUT_TOTAL(ePedLocation)-1)
		ADD_WIDGET_INT_SLIDER("Layout", DebugData.ServerData.iLayout, 0, GET_SERVER_PED_LAYOUT_TOTAL(ePedLocation)-1, 1)
		ADD_WIDGET_INT_SLIDER("Level", DebugData.ServerData.iLevel, 0, MAX_NUM_PED_LEVELS-1, 1)
		ADD_WIDGET_BOOL("Apply Changes", DebugData.GeneralData.bRecreatePeds)
		
		START_WIDGET_GROUP("Ped Level Count")
			SET_PED_DEBUG_COUNT_LEVEL_DATA(DebugData, ServerBD, LocalData)
			
			INT iLevels
			TEXT_LABEL_15 tlWidgetName
			REPEAT MAX_NUM_PED_LEVELS iLevels
				tlWidgetName = "Count Level "
				tlWidgetName += iLevels
				ADD_WIDGET_INT_READ_ONLY(tlWidgetName, DebugData.ServerData.iLevelCount[iLevels])
				tlWidgetName = ""
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ DEBUG WIDGETS - ACTIVE PEDS ╞══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_ACTIVE_PED_WIDGETS(PEDS_DATA &Data, INT iPed, BOOL bNetworkedPed = FALSE)
	
	TEXT_LABEL_63 str
	str = "Ped: "
	IF (bNetworkedPed)
		str += GET_PED_NETWORK_ID(iPed)
	ELSE
		str += iPed
	ENDIF
	
	IF (bNetworkedPed)
		str += "  (Network Ped: "
		str += iPed
		str += ")"
	ENDIF
	
	START_WIDGET_GROUP(str)
		IF NOT (bNetworkedPed)
			START_NEW_WIDGET_COMBO()
				
				INT iArea
				REPEAT PED_AREA_TOTAL iArea
					PED_AREAS ePedArea = INT_TO_ENUM(PED_AREAS, iArea)
					IF IS_PED_CULLING_AREA_VALID(ePedArea)
						ADD_TO_WIDGET_COMBO(DEBUG_GET_PED_AREA_STRING(ePedArea))
					ENDIF
				ENDREPEAT
				
			STOP_WIDGET_COMBO("Culling Area", Data.PedDebugData.iCullingArea)
			ADD_WIDGET_STRING("")
		ENDIF
		
		ADD_WIDGET_INT_SLIDER("Ped Model", Data.iPedModel, 0, ENUM_TO_INT(PED_MODEL_TOTAL)-1, 1)
		Data.PedDebugData.twPedModel = ADD_TEXT_WIDGET("Ped Model: ")
		ADD_WIDGET_STRING("")
		
		ADD_WIDGET_VECTOR_SLIDER("Position", Data.vPosition, -99999.9, 99999.9, 0.01)
		ADD_WIDGET_STRING("")
		ADD_WIDGET_VECTOR_SLIDER("Rotation", Data.vRotation, -360.0, 360.0, 0.1)
		ADD_WIDGET_STRING("")
		
		ADD_WIDGET_INT_SLIDER("Activity", Data.iActivity, 0, ENUM_TO_INT(PED_ACT_TOTAL)-1, 1)
		Data.PedDebugData.twActivity = ADD_TEXT_WIDGET("Activity: ")
		ADD_WIDGET_BOOL("Is Anim Data Populated?", Data.PedDebugData.bActivityDataPopulated)
		ADD_WIDGET_STRING("")
		
		ADD_WIDGET_INT_SLIDER("Packed Drawable[0]", Data.iPackedDrawable[0], -HIGHEST_INT, HIGHEST_INT, 1)
		ADD_WIDGET_INT_SLIDER("Packed Drawable[1]", Data.iPackedDrawable[1], -HIGHEST_INT, HIGHEST_INT, 1)
		ADD_WIDGET_INT_SLIDER("Packed Drawable[2]", Data.iPackedDrawable[2], -HIGHEST_INT, HIGHEST_INT, 1)
		ADD_WIDGET_INT_SLIDER("Packed Texture[0]", Data.iPackedTexture[0], -HIGHEST_INT, HIGHEST_INT, 1)
		ADD_WIDGET_INT_SLIDER("Packed Texture[1]", Data.iPackedTexture[1], -HIGHEST_INT, HIGHEST_INT, 1)
		ADD_WIDGET_INT_SLIDER("Packed Texture[2]", Data.iPackedTexture[2], -HIGHEST_INT, HIGHEST_INT, 1)
		ADD_WIDGET_STRING("")
		
		ADD_WIDGET_INT_SLIDER("Level", Data.iLevel, 0, (MAX_NUM_PED_LEVELS-1), 1)
		ADD_WIDGET_BOOL("Hide Ped in Cutscene", Data.PedDebugData.bHideCutscenePed)
		ADD_WIDGET_BOOL("Heeled Ped", Data.PedDebugData.bHeeledPed)
		ADD_WIDGET_STRING("")
		
		ADD_WIDGET_STRING("Note: Must tick Apply Changes if ANY of the above has changed.")
		ADD_WIDGET_BOOL("Apply Changes", Data.PedDebugData.bApplyChanges)
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC CREATE_ALL_ACTIVE_PEDS_DEBUG_WIDGETS(SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	ADD_WIDGET_STRING("")
	
	START_WIDGET_GROUP("Active Peds")
		START_WIDGET_GROUP("Local Peds")
			
			REPEAT ServerBD.iMaxLocalPeds iPed
				CREATE_ACTIVE_PED_WIDGETS(LocalData.PedData[iPed], iPed)
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Network Peds")
			
			REPEAT ServerBD.iMaxNetworkPeds iPed
				CREATE_ACTIVE_PED_WIDGETS(ServerBD.NetworkPedData[iPed].Data, iPed, TRUE)
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_ACTIVE_LOCAL_PEDS_DEBUG_WIDGETS(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, SCRIPT_PED_LOITER_DATA &LoiterData, INT iPed)
	
	// Descriptions
	IF DOES_TEXT_WIDGET_EXIST(LocalData.PedData[iPed].PedDebugData.twPedModel)
		STRING sPedModel = DEBUG_GET_PED_MODEL_STRING(INT_TO_ENUM(PED_MODELS, LocalData.PedData[iPed].iPedModel))
		IF NOT IS_STRING_NULL_OR_EMPTY(sPedModel)
			SET_CONTENTS_OF_TEXT_WIDGET(LocalData.PedData[iPed].PedDebugData.twPedModel, sPedModel)
		ENDIF
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(LocalData.PedData[iPed].PedDebugData.twActivity)
		STRING sActivity = DEBUG_GET_PED_ACTIVITY_STRING(INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iPed].iActivity))
		IF NOT IS_STRING_NULL_OR_EMPTY(sActivity)
			SET_CONTENTS_OF_TEXT_WIDGET(LocalData.PedData[iPed].PedDebugData.twActivity, sActivity)
		ENDIF
	ENDIF
	
	IF (LocalData.PedData[iPed].PedDebugData.iCachedActivity != LocalData.PedData[iPed].iActivity)
		LocalData.PedData[iPed].PedDebugData.iCachedActivity = LocalData.PedData[iPed].iActivity
		
		PED_ANIM_DATA PedAnimData
		GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iPed].iActivity), pedAnimData)
		
		IF IS_STRING_NULL_OR_EMPTY(PedAnimData.sAnimClip)
			LocalData.PedData[iPed].PedDebugData.bActivityDataPopulated = FALSE
		ELSE
			LocalData.PedData[iPed].PedDebugData.bActivityDataPopulated = TRUE
		ENDIF
	ENDIF
	
	IF (LocalData.PedData[iPed].PedDebugData.bApplyChanges)
		UPDATE_ACTIVE_LOCAL_PED_DEBUG_VALUES(ePedLocation, ServerBD, LocalData, iPed)
		SET_PED_SPEECH_LOITER_DEBUG_DATA(DebugData, -1, 0)
		RESET_NET_TIMER(LoiterData.stLoiterTimer)
		DELETE_LOCAL_SCRIPT_PED(LocalData.PedData[iPed])
		LocalData.PedData[iPed].PedDebugData.bApplyChanges = FALSE
	ENDIF
	
ENDPROC

PROC UPDATE_ACTIVE_NETWORK_PEDS_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_LOITER_DATA &LoiterData, INT iPed)
	
	INT iPedID = GET_PED_NETWORK_ARRAY_ID(iPed)
	
	// Descriptions
	IF DOES_TEXT_WIDGET_EXIST(ServerBD.NetworkPedData[iPedID].Data.PedDebugData.twPedModel)
		STRING sPedModel = DEBUG_GET_PED_MODEL_STRING(INT_TO_ENUM(PED_MODELS, ServerBD.NetworkPedData[iPedID].Data.iPedModel))
		IF NOT IS_STRING_NULL_OR_EMPTY(sPedModel)
			SET_CONTENTS_OF_TEXT_WIDGET(ServerBD.NetworkPedData[iPedID].Data.PedDebugData.twPedModel, DEBUG_GET_PED_MODEL_STRING(INT_TO_ENUM(PED_MODELS, ServerBD.NetworkPedData[iPedID].Data.iPedModel)))
		ENDIF
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(ServerBD.NetworkPedData[iPedID].Data.PedDebugData.twActivity)
		STRING sActivity = DEBUG_GET_PED_ACTIVITY_STRING(INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[iPedID].Data.iActivity))
		IF NOT IS_STRING_NULL_OR_EMPTY(sActivity)
			SET_CONTENTS_OF_TEXT_WIDGET(ServerBD.NetworkPedData[iPedID].Data.PedDebugData.twActivity, sActivity)
		ENDIF
	ENDIF
	
	IF (ServerBD.NetworkPedData[iPedID].Data.PedDebugData.bApplyChanges)
		
		UPDATE_ACTIVE_NETWORK_PED_DEBUG_VALUES(ServerBD, iPedID)
		SET_PED_SPEECH_LOITER_DEBUG_DATA(DebugData, -1, 0)
		RESET_NET_TIMER(LoiterData.stLoiterTimer)
		
		// Reserve max network props for debug
		IF NOT IS_PEDS_BIT_SET(DebugData.iBS, BS_PED_DEBUG_DATA_RESERVE_MAX_NETWORK_PROPS)
			SET_PEDS_BIT(DebugData.iBS, BS_PED_DEBUG_DATA_RESERVE_MAX_NETWORK_PROPS)
			
			INT iNetworkObjectsToReserve = (MAX_NUM_RESERVED_NETWORK_OBJECTS - GET_NUM_RESERVED_MISSION_OBJECTS(FALSE))
			
			IF (iNetworkObjectsToReserve > (MAX_NUM_PED_PROPS*MAX_NUM_TOTAL_NETWORK_PEDS))
				iNetworkObjectsToReserve = (MAX_NUM_PED_PROPS*MAX_NUM_TOTAL_NETWORK_PEDS)
			ENDIF
			
			IF (iNetworkObjectsToReserve > 0)
				RESERVE_NETWORK_MISSION_OBJECTS(iNetworkObjectsToReserve)
			ENDIF
		ENDIF
		
		IF DELETE_NETWORK_SCRIPT_PED(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(iPed)], GET_PED_NETWORK_ARRAY_ID(iPed))
			ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(iPed)].Data.PedDebugData.bApplyChanges = FALSE
		ENDIF
		
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ DEBUG WIDGETS - SPEECH ╞════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_PED_SPEECH_WIDGETS(PED_DEBUG_SPEECH_DATA &DebugGeneralSpeechData, PED_DEBUG_SPEECH_DATA_MAIN &DebugSpeechData, SPEECH_DATA &SpeechData, INT iPed)
	
	TEXT_LABEL_15 str = "Ped: "
	str += iPed
	
	START_WIDGET_GROUP(str)
		ADD_WIDGET_INT_SLIDER("Ped ID", SpeechData.iPedID, -1, MAX_NUM_TOTAL_LOCAL_PEDS, 1)
		ADD_WIDGET_FLOAT_SLIDER("Greet (Green)", SpeechData.fGreetSpeechDistance, 0.0, 20.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Bye (Red)", SpeechData.fByeSpeechDistance, 0.0, 20.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Listen Distance (White)", SpeechData.fListenDistance, 0.0, 20.0, 0.1)
		ADD_WIDGET_BOOL("Set Ped As Speakable", DebugGeneralSpeechData.bSpeakablePed)
		ADD_WIDGET_BOOL("Draw", DebugSpeechData.bDrawSpeechTriggers)
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC CREATE_PED_SPEECH_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	ADD_WIDGET_STRING("")
	
	START_WIDGET_GROUP("Speech")
		START_WIDGET_GROUP("Local Controller Speech")
			ADD_WIDGET_INT_READ_ONLY("Next Speech Ped", LocalData.iLocalSpeechPed)
			ADD_WIDGET_INT_READ_ONLY("Time Until Speech", DebugData.SpeechData.iLocalTimeUntilSpeech)
			DebugData.SpeechData.twLocalSpeechToPlay = ADD_TEXT_WIDGET("Local Speech To Play")
			ADD_WIDGET_BOOL("Trigger speech (CLICK ONLY ONCE. Speech will trigger after current animation) ", DebugData.SpeechData.bTriggerControlledSpeech)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Network Controller Speech")
			ADD_WIDGET_INT_READ_ONLY("Next Speech Ped", LocalData.iNetworkSpeechPed)
			ADD_WIDGET_INT_READ_ONLY("Time Until Speech", DebugData.SpeechData.iNetworkTimeUntilSpeech)
			DebugData.SpeechData.twNetworkSpeechToPlay = ADD_TEXT_WIDGET("Network Speech To Play")
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Distance Triggers")
			ADD_WIDGET_BOOL("Draw All", DebugData.SpeechData.bDrawAllSpeechTriggers)
			ADD_WIDGET_BOOL("Clear All", DebugData.SpeechData.bClearAllSpeechTriggers)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_STRING("Speech Data is outputted in the same file when pressing 'Output Ped Data' in the General Tab")
		START_WIDGET_GROUP("Local Peds")
			
			REPEAT MAX_NUM_LOCAL_PLAYER_SPEECH_PEDS iPed
				CREATE_PED_SPEECH_WIDGETS(DebugData.SpeechData, DebugData.SpeechData.LocalSpeechData[iPed], LocalData.LocalSpeechData[iPed], iPed)
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Network Peds")
			
			REPEAT MAX_NUM_NETWORK_PLAYER_SPEECH_PEDS iPed
				CREATE_PED_SPEECH_WIDGETS(DebugData.SpeechData, DebugData.SpeechData.NetworkSpeechData[iPed], LocalData.NetworkSpeechData[iPed], iPed)
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Loitering")
			ADD_WIDGET_INT_READ_ONLY("Ped", DebugData.SpeechData.iLoiterPedID)
			ADD_WIDGET_INT_READ_ONLY("Timer", DebugData.SpeechData.iTimeUntilLoiterSpeech)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC DEBUG_GET_PED_SPEECH_RGB(FLOAT &fCircleOffset, INT &iR, INT &iG, INT &iB, PED_SPEECH eSpeech)
	SWITCH eSpeech
		CASE PED_SPH_PT_GREETING
			fCircleOffset = 0.8
			iR = 0
			iG = 255
			iB = 0
		BREAK
		CASE PED_SPH_PT_BYE
			fCircleOffset = 0.9
			iR = 255
			iG = 0
			iB = 0
		BREAK
		CASE PED_SPH_LISTEN_DISTANCE
			fCircleOffset = 1.0
			iR = 255
			iG = 255
			iB = 255
		BREAK
	ENDSWITCH
ENDPROC

PROC DRAW_PED_SPEECH_TRIGGERS(PED_DEBUG_SPEECH_DATA_MAIN DebugSpeechData, PEDS_DATA Data, SPEECH_DATA SpeechData)
	
	IF NOT DebugSpeechData.bDrawSpeechTriggers
		EXIT
	ENDIF
	
	VECTOR vActualPedPos = Data.vPosition
	IF IS_ENTITY_ALIVE(Data.PedID)
		vActualPedPos = GET_ENTITY_COORDS(Data.PedID, FALSE)
	ENDIF
	
	IF IS_ENTITY_ALIVE(Data.PedID)
	AND IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_SPEECH)
	AND (VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vActualPedPos) <= MAX_PED_DRAW_DEBUG_DATA_DISTANCE OR VDIST(GET_FINAL_RENDERED_CAM_COORD(), vActualPedPos) <= MAX_PED_DRAW_DEBUG_DATA_DISTANCE)
		
		INT iR = 0
		INT iG = 0
		INT iB = 0
		INT iSpeechType
		FLOAT fCircleOffset = 0.0
		VECTOR vCircleCoords = <<0.0, 0.0, 0.0>>
		
		REPEAT PED_SPH_TOTAL iSpeechType
			PED_SPEECH eSpeech = INT_TO_ENUM(PED_SPEECH, iSpeechType)
			
			IF IS_PED_SPEECH_VALID(eSpeech)
			AND (GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, eSpeech) != 0.0)
				
				DEBUG_GET_PED_SPEECH_RGB(fCircleOffset, iR, iG, iB, eSpeech)
				IF IS_ENTITY_ALIVE(Data.PedID)
					vCircleCoords = vActualPedPos
					vCircleCoords.z -= fCircleOffset
				ENDIF
				
				DRAW_DEBUG_FLOAT_CIRCLE(vCircleCoords, GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, eSpeech), iR, iG, iB, 255)
			ENDIF
		ENDREPEAT
		
		DEBUG_GET_PED_SPEECH_RGB(fCircleOffset, iR, iG, iB, PED_SPH_LISTEN_DISTANCE)
		IF IS_ENTITY_ALIVE(Data.PedID)
			vCircleCoords = vActualPedPos
			vCircleCoords.z -= fCircleOffset
		ENDIF
		
		DRAW_DEBUG_FLOAT_CIRCLE(vCircleCoords, GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, PED_SPH_LISTEN_DISTANCE), iR, iG, iB, 255)
	ENDIF
	
ENDPROC

PROC UPDATE_PED_SPEECH_DEBUG_WIDGETS(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, INT iPed, BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(ePedLocation)
	INT iPedID
	
	// Local Time Until Speech
	DebugData.SpeechData.iLocalTimeUntilSpeech = LocalData.iLocalSpeechTimeOffsetMS - (GET_GAME_TIMER() - DebugData.SpeechData.iLocalGameTimeSpeechSet)
	IF (DebugData.SpeechData.iLocalTimeUntilSpeech < 0)
		DebugData.SpeechData.iLocalTimeUntilSpeech = 0
	ENDIF
	
	// Network Time Until Speech
	DebugData.SpeechData.iNetworkTimeUntilSpeech = LocalData.iNetworkSpeechTimeOffsetMS - (GET_GAME_TIMER() - DebugData.SpeechData.iNetworkGameTimeSpeechSet)
	IF (DebugData.SpeechData.iNetworkTimeUntilSpeech < 0)
		DebugData.SpeechData.iNetworkTimeUntilSpeech = 0
	ENDIF
	
	// Local Speech To Play
	IF DOES_TEXT_WIDGET_EXIST(DebugData.SpeechData.twLocalSpeechToPlay)
		SET_CONTENTS_OF_TEXT_WIDGET(DebugData.SpeechData.twLocalSpeechToPlay, DEBUG_GET_PED_SPEECH_STRING(LocalData.eLocalPedSpeech))
	ENDIF
	
	// Network Speech To Play
	IF DOES_TEXT_WIDGET_EXIST(DebugData.SpeechData.twNetworkSpeechToPlay)
		SET_CONTENTS_OF_TEXT_WIDGET(DebugData.SpeechData.twNetworkSpeechToPlay, DEBUG_GET_PED_SPEECH_STRING(LocalData.eNetworkPedSpeech))
	ENDIF
	
	// Draw All Speech Triggers
	IF (DebugData.SpeechData.bDrawAllSpeechTriggers)
		DebugData.SpeechData.bDrawAllSpeechTriggers = FALSE
		
		REPEAT MAX_NUM_LOCAL_PLAYER_SPEECH_PEDS iPedID
			DebugData.SpeechData.LocalSpeechData[iPedID].bDrawSpeechTriggers = TRUE
		ENDREPEAT
		
		REPEAT MAX_NUM_NETWORK_PLAYER_SPEECH_PEDS iPedID
			DebugData.SpeechData.NetworkSpeechData[iPedID].bDrawSpeechTriggers = TRUE
		ENDREPEAT
	ENDIF
	
	// Clear All Speech Triggers
	IF (DebugData.SpeechData.bClearAllSpeechTriggers)
		DebugData.SpeechData.bClearAllSpeechTriggers = FALSE
		
		REPEAT MAX_NUM_LOCAL_PLAYER_SPEECH_PEDS iPedID
			DebugData.SpeechData.LocalSpeechData[iPedID].bDrawSpeechTriggers = FALSE
		ENDREPEAT
		
		REPEAT MAX_NUM_NETWORK_PLAYER_SPEECH_PEDS iPedID
			DebugData.SpeechData.NetworkSpeechData[iPedID].bDrawSpeechTriggers = FALSE
		ENDREPEAT
	ENDIF
	
	// Draw Speech Triggers
	IF (bNetworkPed)
		IF (LocalData.iNetworkSpeechPedID[GET_PED_NETWORK_ARRAY_ID(iPed)] != -1)
			DRAW_PED_SPEECH_TRIGGERS(DebugData.SpeechData.NetworkSpeechData[LocalData.iNetworkSpeechPedID[GET_PED_NETWORK_ARRAY_ID(iPed)]], ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(iPed)].Data, LocalData.NetworkSpeechData[LocalData.iNetworkSpeechPedID[GET_PED_NETWORK_ARRAY_ID(iPed)]])
		ENDIF
	ELSE
		IF (LocalData.iLocalSpeechPedID[iPed] != -1)
			DRAW_PED_SPEECH_TRIGGERS(DebugData.SpeechData.LocalSpeechData[LocalData.iLocalSpeechPedID[iPed]], LocalData.PedData[iPed], LocalData.LocalSpeechData[LocalData.iLocalSpeechPedID[iPed]])
		ENDIF
	ENDIF
	
	// Loiter Speech
	DebugData.SpeechData.iTimeUntilLoiterSpeech = LocalData.LoiterData.iLoiterDelay - (GET_GAME_TIMER() - DebugData.SpeechData.iGameTimeLoiterSpeechSet)
	IF (DebugData.SpeechData.iTimeUntilLoiterSpeech < 0)
		DebugData.SpeechData.iTimeUntilLoiterSpeech = 0
	ENDIF
	
	// Is ped speakable
	IF (DebugData.SpeechData.bSpeakablePed)
		DebugData.SpeechData.bSpeakablePed = FALSE
		
		INT iSpeechPed
		REPEAT MAX_NUM_LOCAL_PLAYER_SPEECH_PEDS iSpeechPed
			IF (LocalData.LocalSpeechData[iSpeechPed].iPedID != -1)
				SET_PEDS_BIT(LocalData.PedData[LocalData.LocalSpeechData[iSpeechPed].iPedID].iBS, BS_PED_DATA_USE_SPEECH)
				LocalData.iLocalSpeechPedID[LocalData.LocalSpeechData[iSpeechPed].iPedID] = iSpeechPed
			ENDIF
		ENDREPEAT
		
		REPEAT MAX_NUM_NETWORK_PLAYER_SPEECH_PEDS iSpeechPed
			IF (LocalData.NetworkSpeechData[iSpeechPed].iPedID != -1)
				SET_PEDS_BIT(ServerBD.NetworkPedData[LocalData.NetworkSpeechData[iSpeechPed].iPedID].Data.iBS, BS_PED_DATA_USE_SPEECH)
				LocalData.iNetworkSpeechPedID[LocalData.NetworkSpeechData[iSpeechPed].iPedID] = iSpeechPed
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════╡ DEBUG WIDGETS - HEAD TRACKING ╞═════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_PED_HEAD_TRACKING_WIDGETS(PED_DEBUG_HEAD_TRACKING_DATA &DebugGeneralHeadTrackingData, PED_DEBUG_HEAD_TRACKING_DATA_MAIN &DebugHeadTrackingData, HEAD_TRACKING_DATA &HeadTrackingData, INT iPed, BOOL bNetworkPed = FALSE)
	
	TEXT_LABEL_15 str = "Ped: "
	str += iPed
	
	START_WIDGET_GROUP(str)
		ADD_WIDGET_INT_SLIDER("Ped ID", HeadTrackingData.iPedID, -1, MAX_NUM_TOTAL_LOCAL_PEDS, 1)
		ADD_WIDGET_FLOAT_SLIDER("Head Tracking Range", HeadTrackingData.fHeadTrackingRange, 0.0, 20.0, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("Head Tracking Coords", HeadTrackingData.vHeadTrackingCoords, -99999.9, 99999.9, 0.01)
		ADD_WIDGET_BOOL("Head Track Coords", DebugHeadTrackingData.bHeadTrackCoords)
		ADD_WIDGET_BOOL("Head Track Nearest Player", DebugHeadTrackingData.bHeadTrackingNearestPlayer)
		ADD_WIDGET_STRING("")
		
		IF (bNetworkPed)
			ADD_WIDGET_BOOL("Apply Changes", DebugGeneralHeadTrackingData.bNetworkApplyChanges)
		ELSE
			ADD_WIDGET_BOOL("Apply Changes", DebugGeneralHeadTrackingData.bLocalApplyChanges)
		ENDIF
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC CREATE_PED_HEAD_TRACKING_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	ADD_WIDGET_STRING("")
	
	START_WIDGET_GROUP("Head Tracking")
		ADD_WIDGET_STRING("Head Tracking Data is outputted in the same file when pressing 'Output Ped Data' in the General Tab")
		
		START_WIDGET_GROUP("Local Peds")
			
			REPEAT MAX_NUM_LOCAL_HEAD_TRACKING_PEDS iPed
				CREATE_PED_HEAD_TRACKING_WIDGETS(DebugData.HeadTrackingData, DebugData.HeadTrackingData.LocalHeadTrackingData[iPed], LocalData.HeadTrackingData[iPed], iPed)
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Network Peds")
			
			REPEAT MAX_NUM_NETWORK_HEAD_TRACKING_PEDS iPed
				CREATE_PED_HEAD_TRACKING_WIDGETS(DebugData.HeadTrackingData, DebugData.HeadTrackingData.NetworkHeadTrackingData[iPed], ServerBD.HeadTrackingData[iPed], iPed, TRUE)
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_LOCAL_PED_HEAD_TRACKING_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData)
	
	IF (DebugData.HeadTrackingData.bLocalApplyChanges)
		DebugData.HeadTrackingData.bLocalApplyChanges = FALSE
		
		INT iHeadTrackingPedID
		REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iHeadTrackingPedID
			CLEAR_PEDS_BIT(LocalData.PedData[iHeadTrackingPedID].iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)
			CLEAR_PEDS_BIT(LocalData.PedData[iHeadTrackingPedID].iBS, BS_PED_DATA_SET_HEAD_TRACKING_COORDS)
			CLEAR_PEDS_BIT(LocalData.PedData[iHeadTrackingPedID].iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
		ENDREPEAT
		
		REPEAT MAX_NUM_LOCAL_HEAD_TRACKING_PEDS iHeadTrackingPedID
			IF (LocalData.HeadTrackingData[iHeadTrackingPedID].iPedID != -1)
				
				// Head Tracking Coords
				IF (DebugData.HeadTrackingData.LocalHeadTrackingData[iHeadTrackingPedID].bHeadTrackCoords)
					SET_PEDS_BIT(LocalData.PedData[LocalData.HeadTrackingData[iHeadTrackingPedID].iPedID].iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)
					CLEAR_PEDS_BIT(LocalData.PedData[LocalData.HeadTrackingData[iHeadTrackingPedID].iPedID].iBS, BS_PED_DATA_SET_HEAD_TRACKING_COORDS)
					CLEAR_PEDS_BIT(LocalData.PedData[LocalData.HeadTrackingData[iHeadTrackingPedID].iPedID].iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
				
				// Head Tracking Nearest Player
				ELIF (DebugData.HeadTrackingData.LocalHeadTrackingData[iHeadTrackingPedID].bHeadTrackingNearestPlayer)
					SET_PEDS_BIT(LocalData.PedData[LocalData.HeadTrackingData[iHeadTrackingPedID].iPedID].iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					CLEAR_PEDS_BIT(LocalData.PedData[LocalData.HeadTrackingData[iHeadTrackingPedID].iPedID].iBS, BS_PED_DATA_SET_HEAD_TRACKING_COORDS)
					CLEAR_PEDS_BIT(LocalData.PedData[LocalData.HeadTrackingData[iHeadTrackingPedID].iPedID].iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)
				ENDIF
				
				LocalData.iHeadTrackingPedID[LocalData.HeadTrackingData[iHeadTrackingPedID].iPedID] = iHeadTrackingPedID
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC UPDATE_NETWORK_PED_HEAD_TRACKING_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD)
	
	IF (DebugData.HeadTrackingData.bNetworkApplyChanges)
		DebugData.HeadTrackingData.bNetworkApplyChanges = FALSE
		
		INT iHeadTrackingPedID
		REPEAT MAX_NUM_TOTAL_NETWORK_PEDS iHeadTrackingPedID
			CLEAR_PEDS_BIT(ServerBD.NetworkPedData[iHeadTrackingPedID].Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)
			CLEAR_PEDS_BIT(ServerBD.NetworkPedData[iHeadTrackingPedID].Data.iBS, BS_PED_DATA_SET_HEAD_TRACKING_COORDS)
			CLEAR_PEDS_BIT(ServerBD.NetworkPedData[iHeadTrackingPedID].Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
		ENDREPEAT
		
		REPEAT MAX_NUM_NETWORK_HEAD_TRACKING_PEDS iHeadTrackingPedID
			IF (ServerBD.HeadTrackingData[iHeadTrackingPedID].iPedID != -1)
				
				// Head Tracking Coords
				IF (DebugData.HeadTrackingData.NetworkHeadTrackingData[iHeadTrackingPedID].bHeadTrackCoords)
					SET_PEDS_BIT(ServerBD.NetworkPedData[ServerBD.HeadTrackingData[iHeadTrackingPedID].iPedID].Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)
					CLEAR_PEDS_BIT(ServerBD.NetworkPedData[ServerBD.HeadTrackingData[iHeadTrackingPedID].iPedID].Data.iBS, BS_PED_DATA_SET_HEAD_TRACKING_COORDS)
					CLEAR_PEDS_BIT(ServerBD.NetworkPedData[ServerBD.HeadTrackingData[iHeadTrackingPedID].iPedID].Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
				
				// Head Tracking Nearest Player
				ELIF (DebugData.HeadTrackingData.NetworkHeadTrackingData[iHeadTrackingPedID].bHeadTrackingNearestPlayer)
					SET_PEDS_BIT(ServerBD.NetworkPedData[ServerBD.HeadTrackingData[iHeadTrackingPedID].iPedID].Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					CLEAR_PEDS_BIT(ServerBD.NetworkPedData[ServerBD.HeadTrackingData[iHeadTrackingPedID].iPedID].Data.iBS, BS_PED_DATA_SET_HEAD_TRACKING_COORDS)
					CLEAR_PEDS_BIT(ServerBD.NetworkPedData[ServerBD.HeadTrackingData[iHeadTrackingPedID].iPedID].Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)
				ENDIF
				
				ServerBD.iHeadTrackingPedID[ServerBD.HeadTrackingData[iHeadTrackingPedID].iPedID] = iHeadTrackingPedID
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ DEBUG WIDGETS - CULLING ╞═══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_PED_CULLING_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData)
	
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP("Culling")
		ADD_WIDGET_STRING("Note: Individual ped culling widgets are in Active Peds")
		ADD_WIDGET_STRING("Changing culling areas will regrab ped data! Only do so after outputting any current changes")
		START_NEW_WIDGET_COMBO()
			
			INT iArea
			REPEAT PED_AREA_TOTAL iArea
				PED_AREAS ePedArea = INT_TO_ENUM(PED_AREAS, iArea)
				IF IS_PED_CULLING_AREA_VALID(ePedArea)
					ADD_TO_WIDGET_COMBO(DEBUG_GET_PED_AREA_STRING(ePedArea))
				ENDIF
			ENDREPEAT
			
		STOP_WIDGET_COMBO("Change All Peds Culling Area", DebugData.CullingData.iCullingArea)
		ADD_WIDGET_BOOL("Apply Changes", DebugData.CullingData.bApplyChanges)
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_PED_CULLING_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	IF (DebugData.CullingData.bApplyChanges)
		DebugData.CullingData.bApplyChanges = FALSE
		
		INT iPed
		REPEAT ServerBD.iMaxLocalPeds iPed
			LocalData.PedData[iPed].PedDebugData.iCullingArea	= DebugData.CullingData.iCullingArea
			LocalData.PedData[iPed].PedDebugData.bApplyChanges 	= TRUE
		ENDREPEAT
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡ DEBUG WIDGETS - ANIMATION ╞══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC STRING GET_PED_ACTIVITY_VARIATION_PERCENTAGE_NAME(INT iActivity)
	STRING sActivity = ""
	SWITCH iActivity
		CASE 0	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_M_01)		BREAK
		CASE 1	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_M_02)		BREAK
		CASE 2	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_M_03)		BREAK
		CASE 3	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_M_04)		BREAK
		CASE 4	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_M_05)		BREAK
		CASE 5	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_F_01)		BREAK
		CASE 6	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_F_02)		BREAK
		CASE 7	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_PROP_M_01)	BREAK
		CASE 8	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_PROP_M_02)	BREAK
		CASE 9	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_PROP_M_03)	BREAK
		CASE 10	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_PROP_M_04)	BREAK
		CASE 11	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_PROP_F_01)	BREAK
		CASE 12	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_BEACH_PROP_F_02)	BREAK
		CASE 13	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_CLUB_M_01)		BREAK
		CASE 14	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_CLUB_M_02)		BREAK
		CASE 15	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_CLUB_M_03)		BREAK
		CASE 16	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_CLUB_F_01)		BREAK
		CASE 17	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_CLUB_F_02)		BREAK
		CASE 18	sActivity = DEBUG_GET_PED_ACTIVITY_STRING(PED_ACT_DANCING_CLUB_F_03)		BREAK
	ENDSWITCH
	RETURN sActivity
ENDFUNC

PROC CREATE_PED_ANIMATION_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData)
	
	INITIALISE_PED_DANCING_ANIM_PERCENTAGE_DEBUG_DATA()
	
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP("Animation")
		
		START_WIDGET_GROUP("Play Animations")
			ADD_WIDGET_BOOL("Enable Anim Debug", DebugData.AnimData.bEnableAnimDebug)
			
			Add_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Ped ID", DebugData.AnimData.iAnimPed, -1, NETWORK_PED_ID_9, 1)
			ADD_WIDGET_INT_SLIDER("Activity", DebugData.AnimData.iActivity, 0, ENUM_TO_INT(PED_ACT_TOTAL)-1, 1)
			DebugData.AnimData.twActivity = ADD_TEXT_WIDGET("Activity: ")
			
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Clip ID", DebugData.AnimData.iClip, 0, MAX_NUM_ANIMS_IN_ACTIVITY, 1)
			DebugData.AnimData.twAnimClip = ADD_TEXT_WIDGET("Anim Clip: ")
			DebugData.AnimData.twAnimDict = ADD_TEXT_WIDGET("Anim Dict: ")
			ADD_WIDGET_BOOL("Play Animation", DebugData.AnimData.bPlayAnim)
			
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Cycle Animations", DebugData.AnimData.bCycleAnims)
			
			ADD_WIDGET_STRING("")
			START_WIDGET_GROUP("Child Peds")
				
				INT iChildPed
				TEXT_LABEL_15 tlLabel = ""
				REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPed
					tlLabel = ""
					tlLabel += "Child Ped ID "
					tlLabel += iChildPed
					START_WIDGET_GROUP(tlLabel)
						ADD_WIDGET_STRING("The Ped ID above is the parent ped to these child peds. Clip ID above selects which clips both the parent and child play.")
						ADD_WIDGET_INT_SLIDER("Child Ped ID", DebugData.AnimData.ChildPedData[iChildPed].iChildPedID, -1, MAX_NUM_TOTAL_LOCAL_PEDS, 1)
						ADD_WIDGET_INT_SLIDER("Child Ped Activity", DebugData.AnimData.ChildPedData[iChildPed].iActivity, 0, ENUM_TO_INT(PED_ACT_TOTAL)-1, 1)
						DebugData.AnimData.ChildPedData[iChildPed].twActivity = ADD_TEXT_WIDGET("Child Ped Activity: ")
						ADD_WIDGET_STRING("")
						DebugData.AnimData.ChildPedData[iChildPed].twAnimClip = ADD_TEXT_WIDGET("Anim Clip: ")
						DebugData.AnimData.ChildPedData[iChildPed].twAnimDict = ADD_TEXT_WIDGET("Anim Dict: ")
						ADD_WIDGET_BOOL("Apply Changes", DebugData.AnimData.bApplyChildPedChanges)
					STOP_WIDGET_GROUP()
				ENDREPEAT
				
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Variation Percentages")
			
			INT iPedActivity
			REPEAT MAX_PED_ACTIVITIES_FOR_VARIATION_PERCENTAGES iPedActivity
				START_WIDGET_GROUP(GET_PED_ACTIVITY_VARIATION_PERCENTAGE_NAME(iPedActivity))
					START_WIDGET_GROUP("Low Intensity")
						ADD_WIDGET_INT_SLIDER("Percentage", g_DancingIntensityPercentages[iPedActivity].iLowPercentage, 1, 100, 1)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Medium Intensity")
						ADD_WIDGET_INT_SLIDER("Percentage", g_DancingIntensityPercentages[iPedActivity].iMediumPercentage, 1, 100, 1)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("High Intensity")
						ADD_WIDGET_INT_SLIDER("Percentage", g_DancingIntensityPercentages[iPedActivity].iHighPercentage, 1, 100, 1)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Trance Intensity")
						ADD_WIDGET_INT_SLIDER("Percentage", g_DancingIntensityPercentages[iPedActivity].iTrancePercentage, 1, 100, 1)
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Transition Percentage (Non Dancing)")
			ADD_WIDGET_INT_SLIDER("Percentage", g_PedTransitionPercentage, 1, 100, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Activity Data")
			ADD_WIDGET_STRING("Note: Select activity from Play Animations Activity slider.")
			ADD_WIDGET_BOOL("Output Initial Activity Anim Data", DebugData.AnimData.bOutputInitialActivityAnimData)
			DebugData.AnimData.twLogSearch = ADD_TEXT_WIDGET("Search in Logs: ")
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC

FUNC BOOL DEBUG_HAVE_PED_PROPS_BEEN_CREATED(PEDS_DATA &PedData, PED_LOCATIONS ePedLocation)
	
	INT iPropID
	INT iValidProps
	INT iCreatedProps
	PED_PROP_DATA pedPropData
	
	REPEAT MAX_NUM_PED_PROPS iPropID
		GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, PedData.iActivity), iPropID, pedPropData)
		IF (IS_MODEL_VALID(pedPropData.ePropModel) AND pedPropData.ePropModel != DUMMY_MODEL_FOR_SCRIPT)
			iValidProps++
			
			IF DOES_ENTITY_EXIST(PedData.PropID[iPropID])
			AND DOES_ENTITY_HAVE_DRAWABLE(PedData.PropID[iPropID])
				iCreatedProps++
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN (iValidProps = iCreatedProps)
ENDFUNC

PROC DEBUG_PLAY_TASK_ANIM(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, NETWORK_PED_DATA &NetworkPedData, PEDS_DATA &PedData, PED_ANIM_DATA &highlightedPedAnimData, INT iAnimPed, BOOL bNetworkPed = FALSE)
	
	IF IS_ENTITY_ALIVE(PedData.PedID)
		IF REQUEST_AND_LOAD_ANIMATION_DICT(highlightedPedAnimData.sAnimDict)
			CLEAR_PED_TASKS(PedData.PedID)
			
			IF (bNetworkPed)
				CREATE_NETWORK_SCRIPT_PED_PROPS(ePedLocation, NetworkPedData, iAnimPed)
			ELSE
				CREATE_LOCAL_SCRIPT_PED_PROPS(ePedLocation, PedData, iAnimPed)
			ENDIF
			
			IF DEBUG_HAVE_PED_PROPS_BEEN_CREATED(PedData, ePedLocation)
				IF NOT IS_STRING_NULL_OR_EMPTY(highlightedPedAnimData.sAnimDict)
				AND NOT IS_STRING_NULL_OR_EMPTY(highlightedPedAnimData.sAnimClip)
					highlightedPedAnimData.animFlags = AF_LOOPING
					TASK_PLAY_ANIM_ADVANCED(PedData.PedID, highlightedPedAnimData.sAnimDict, highlightedPedAnimData.sAnimClip, PedData.vPosition, PedData.vRotation, highlightedPedAnimData.fBlendInDelta, highlightedPedAnimData.fBlendOutDelta, highlightedPedAnimData.iTimeToPlay, highlightedPedAnimData.animFlags, highlightedPedAnimData.fStartPhase, highlightedPedAnimData.rotOrder, highlightedPedAnimData.ikFlags)
					PedData.animData.iTaskAnimStartTimeMS 	= GET_GAME_TIMER()
					PedData.animData.iTaskAnimDurationMS 	= GET_ANIM_DURATION_TIME_MS(highlightedPedAnimData.sAnimDict, highlightedPedAnimData.sAnimClip, highlightedPedAnimData.fStartPhase, highlightedPedAnimData.iTimeToPlay)
				ENDIF
			ELSE
				DebugData.AnimData.bPlayAnim = TRUE
			ENDIF
		ELSE
			DebugData.AnimData.bPlayAnim = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC DEBUG_CYCLE_TASK_ANIM(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, NETWORK_PED_DATA &NetworkPedData, PEDS_DATA &PedData, PED_ANIM_DATA &highlightedPedAnimData, INT iAnimPed, BOOL bNetworkPed = FALSE)
	
	IF IS_ENTITY_ALIVE(PedData.PedID)
		IF REQUEST_AND_LOAD_ANIMATION_DICT(highlightedPedAnimData.sAnimDict)
			
			IF (bNetworkPed)
				CREATE_NETWORK_SCRIPT_PED_PROPS(ePedLocation, NetworkPedData, iAnimPed)
			ELSE
				CREATE_LOCAL_SCRIPT_PED_PROPS(ePedLocation, PedData, iAnimPed)
			ENDIF
			
			IF DEBUG_HAVE_PED_PROPS_BEEN_CREATED(PedData, ePedLocation)
				IF (NOT DebugData.AnimData.bCycleAnimsActive)
				OR (PedData.animData.iTaskAnimStartTimeMS = -1)
				OR (GET_CURRENT_TASK_ANIM_PHASE(PedData) >= MAX_PED_ANIM_PHASE)
					IF NOT IS_STRING_NULL_OR_EMPTY(highlightedPedAnimData.sAnimDict)
					AND NOT IS_STRING_NULL_OR_EMPTY(highlightedPedAnimData.sAnimClip)
						TASK_PLAY_ANIM_ADVANCED(PedData.PedID, highlightedPedAnimData.sAnimDict, highlightedPedAnimData.sAnimClip, PedData.vPosition, PedData.vRotation, highlightedPedAnimData.fBlendInDelta, highlightedPedAnimData.fBlendOutDelta, highlightedPedAnimData.iTimeToPlay, highlightedPedAnimData.animFlags, highlightedPedAnimData.fStartPhase, highlightedPedAnimData.rotOrder, highlightedPedAnimData.ikFlags)
						PedData.animData.iTaskAnimStartTimeMS 	= GET_GAME_TIMER()
						PedData.animData.iTaskAnimDurationMS 	= GET_ANIM_DURATION_TIME_MS(highlightedPedAnimData.sAnimDict, highlightedPedAnimData.sAnimClip, highlightedPedAnimData.fStartPhase, highlightedPedAnimData.iTimeToPlay)
						DebugData.AnimData.bIncrementCycleAnim 	= FALSE
						DebugData.AnimData.bCycleAnimsActive 	= TRUE
					ELSE
						DebugData.AnimData.iClip = 0
					ENDIF
				ELSE
					// Grab the next animation data just before it ends - Can't increment clip ID after triggering current anim as clip name in widget will be the next clip
					IF (DebugData.AnimData.bCycleAnimsActive AND GET_CURRENT_TASK_ANIM_PHASE(PedData) >= MAX_PED_ANIM_PHASE-0.01)
						IF (NOT DebugData.AnimData.bIncrementCycleAnim)
							DebugData.AnimData.bIncrementCycleAnim = TRUE
							DebugData.AnimData.iClip++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC DEBUG_PLAY_SYNC_SCENE(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData, PEDS_DATA &PedData, PED_ANIM_DATA &highlightedPedAnimData, PED_ANIM_DATA &childPedAnimData[], INT iClip, INT iAnimPed, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	IF IS_ENTITY_ALIVE(PedData.PedID)
	AND (LocalData.ParentPedData[0].iChildID[0] = -1 OR (LocalData.ParentPedData[0].iChildID[0] != -1 AND IS_ENTITY_ALIVE(LocalData.PedData[LocalData.ParentPedData[0].iChildID[0]].PedID)))
	AND (LocalData.ParentPedData[0].iChildID[1] = -1 OR (LocalData.ParentPedData[0].iChildID[1] != -1 AND IS_ENTITY_ALIVE(LocalData.PedData[LocalData.ParentPedData[0].iChildID[1]].PedID)))
	AND (LocalData.ParentPedData[0].iChildID[2] = -1 OR (LocalData.ParentPedData[0].iChildID[2] != -1 AND IS_ENTITY_ALIVE(LocalData.PedData[LocalData.ParentPedData[0].iChildID[2]].PedID)))
	
		IF REQUEST_AND_LOAD_ANIMATION_DICT(highlightedPedAnimData.sAnimDict)
		AND (IS_STRING_NULL_OR_EMPTY(childPedAnimData[0].sAnimDict) OR (NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData[0].sAnimDict) AND REQUEST_AND_LOAD_ANIMATION_DICT(childPedAnimData[0].sAnimDict)))
		AND (IS_STRING_NULL_OR_EMPTY(childPedAnimData[1].sAnimDict) OR (NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData[1].sAnimDict) AND REQUEST_AND_LOAD_ANIMATION_DICT(childPedAnimData[1].sAnimDict)))
		AND (IS_STRING_NULL_OR_EMPTY(childPedAnimData[2].sAnimDict) OR (NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData[2].sAnimDict) AND REQUEST_AND_LOAD_ANIMATION_DICT(childPedAnimData[2].sAnimDict)))
			
			CLEAR_PED_TASKS(PedData.PedID)
			CREATE_LOCAL_SCRIPT_PED_PROPS(ePedLocation, PedData, iAnimPed, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
			IF DEBUG_HAVE_PED_PROPS_BEEN_CREATED(PedData, ePedLocation)
			
				// Create the synced scene
				PedData.animData.iSyncSceneID = CREATE_SYNCHRONIZED_SCENE(PedData.vPosition, PedData.vRotation)
				
				IF DOES_ENTITY_EXIST(PedData.PedID)
				AND NOT IS_STRING_NULL_OR_EMPTY(highlightedPedAnimData.sAnimDict)
				AND NOT IS_STRING_NULL_OR_EMPTY(highlightedPedAnimData.sAnimClip)
					TASK_SYNCHRONIZED_SCENE(PedData.PedID, PedData.animData.iSyncSceneID, highlightedPedAnimData.sAnimDict, highlightedPedAnimData.sAnimClip, highlightedPedAnimData.fBlendInDelta, highlightedPedAnimData.fBlendOutDelta, highlightedPedAnimData.syncSceneFlags, highlightedPedAnimData.ragdollFlags, highlightedPedAnimData.fMoverBlendInDelta, highlightedPedAnimData.ikFlags)
					PedData.animData.iCurrentClip = iClip
					
					// Add any child peds to the synced scene
					BOOL bChildPedUsed = FALSE
					ADD_CHILD_PEDS_TO_LOCAL_SYNCED_SCENE(ePedLocation, LocalData, PedData, iAnimPed, iClip, bChildPedUsed, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
					
					// Add any props to the synced scene
					ADD_PED_LOCAL_PROPS_TO_SYNCED_SCENE(ePedLocation, PedData, highlightedPedAnimData, PedData.animData.iSyncSceneID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(PedData.iBS, BS_PED_DATA_HEELED_PED))
					
					// Hide group props on init
					INT iProp
					REPEAT MAX_NUM_PED_PROPS iProp
						IF DOES_ENTITY_EXIST(PedData.PropID[iProp])
							IF IS_PEDS_BIT_SET(highlightedPedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
								SET_ENTITY_ALPHA(PedData.PropID[iProp], 0, FALSE)
							ENDIF
						ELSE
							BREAKLOOP
						ENDIF
					ENDREPEAT
					
					// Clear prop visible bits for new anim
					CLEAR_PEDS_BIT(PedData.iBS, BS_PED_DATA_SHOW_PROP)
					CLEAR_PEDS_BIT(PedData.iBS, BS_PED_DATA_HIDE_PROP)
					
					SET_SYNCHRONIZED_SCENE_LOOPED(PedData.animData.iSyncSceneID, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedData.PedID, TRUE)
					SET_PED_KEEP_TASK(PedData.PedID, TRUE)
				ENDIF
			ELSE
				DebugData.AnimData.bPlayAnim = TRUE
			ENDIF
		ELSE
			DebugData.AnimData.bPlayAnim = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC DEBUG_CYCLE_SYNC_SCENE(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData, PEDS_DATA &PedData, PED_ANIM_DATA &highlightedPedAnimData, PED_ANIM_DATA &childPedAnimData[], INT iClip, INT iAnimPed, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	IF IS_ENTITY_ALIVE(PedData.PedID)
	AND (LocalData.ParentPedData[0].iChildID[0] = -1 OR (LocalData.ParentPedData[0].iChildID[0] != -1 AND IS_ENTITY_ALIVE(LocalData.PedData[LocalData.ParentPedData[0].iChildID[0]].PedID)))
	AND (LocalData.ParentPedData[0].iChildID[1] = -1 OR (LocalData.ParentPedData[0].iChildID[1] != -1 AND IS_ENTITY_ALIVE(LocalData.PedData[LocalData.ParentPedData[0].iChildID[1]].PedID)))
	AND (LocalData.ParentPedData[0].iChildID[2] = -1 OR (LocalData.ParentPedData[0].iChildID[2] != -1 AND IS_ENTITY_ALIVE(LocalData.PedData[LocalData.ParentPedData[0].iChildID[2]].PedID)))
	
		IF REQUEST_AND_LOAD_ANIMATION_DICT(highlightedPedAnimData.sAnimDict)
		AND (IS_STRING_NULL_OR_EMPTY(childPedAnimData[0].sAnimDict) OR (NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData[0].sAnimDict) AND REQUEST_AND_LOAD_ANIMATION_DICT(childPedAnimData[0].sAnimDict)))
		AND (IS_STRING_NULL_OR_EMPTY(childPedAnimData[1].sAnimDict) OR (NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData[1].sAnimDict) AND REQUEST_AND_LOAD_ANIMATION_DICT(childPedAnimData[1].sAnimDict)))
		AND (IS_STRING_NULL_OR_EMPTY(childPedAnimData[2].sAnimDict) OR (NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData[2].sAnimDict) AND REQUEST_AND_LOAD_ANIMATION_DICT(childPedAnimData[2].sAnimDict)))
			
			CREATE_LOCAL_SCRIPT_PED_PROPS(ePedLocation, PedData, iAnimPed, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
			IF DEBUG_HAVE_PED_PROPS_BEEN_CREATED(PedData, ePedLocation)
				IF (PedData.animData.iSyncSceneID = -1)
				OR (PedData.animData.iSyncSceneID != -1 AND GET_SYNCHRONIZED_SCENE_PHASE(PedData.animData.iSyncSceneID) >= MAX_PED_ANIM_PHASE)
				
					// Create the synced scene
					PedData.animData.iSyncSceneID = CREATE_SYNCHRONIZED_SCENE(PedData.vPosition, PedData.vRotation)
					
					IF DOES_ENTITY_EXIST(PedData.PedID)
					AND NOT IS_STRING_NULL_OR_EMPTY(highlightedPedAnimData.sAnimDict)
					AND NOT IS_STRING_NULL_OR_EMPTY(highlightedPedAnimData.sAnimClip)
						TASK_SYNCHRONIZED_SCENE(PedData.PedID, PedData.animData.iSyncSceneID, highlightedPedAnimData.sAnimDict, highlightedPedAnimData.sAnimClip, highlightedPedAnimData.fBlendInDelta, highlightedPedAnimData.fBlendOutDelta, highlightedPedAnimData.syncSceneFlags, highlightedPedAnimData.ragdollFlags, highlightedPedAnimData.fMoverBlendInDelta, highlightedPedAnimData.ikFlags)
						PedData.animData.iCurrentClip = iClip
						DebugData.AnimData.bIncrementCycleAnim 	= FALSE
						DebugData.AnimData.bCycleAnimsActive 	= TRUE
						
						// Add any child peds to the synced scene
						BOOL bChildPedUsed = FALSE
						ADD_CHILD_PEDS_TO_LOCAL_SYNCED_SCENE(ePedLocation, LocalData, PedData, iAnimPed, iClip, bChildPedUsed, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
						
						// Add any props to the synced scene
						ADD_PED_LOCAL_PROPS_TO_SYNCED_SCENE(ePedLocation, PedData, highlightedPedAnimData, PedData.animData.iSyncSceneID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(PedData.iBS, BS_PED_DATA_HEELED_PED))
						
						// Hide group props on init
						INT iProp
						REPEAT MAX_NUM_PED_PROPS iProp
							IF DOES_ENTITY_EXIST(PedData.PropID[iProp])
								IF IS_PEDS_BIT_SET(highlightedPedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
									SET_ENTITY_ALPHA(PedData.PropID[iProp], 0, FALSE)
								ENDIF
							ELSE
								BREAKLOOP
							ENDIF
						ENDREPEAT
						
						// Clear prop visible bits for new anim
						CLEAR_PEDS_BIT(PedData.iBS, BS_PED_DATA_SHOW_PROP)
						CLEAR_PEDS_BIT(PedData.iBS, BS_PED_DATA_HIDE_PROP)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedData.PedID, TRUE)
						SET_PED_KEEP_TASK(PedData.PedID, TRUE)
					ELSE
						DebugData.AnimData.iClip = 0
						PedData.animData.iSyncSceneID = -1
					ENDIF
				ELSE
					// Grab the next animation data just before it ends - Can't increment clip ID after triggering current anim as clip name in widget will be the next clip
					IF (DebugData.AnimData.bCycleAnimsActive AND (PedData.animData.iSyncSceneID != -1 AND GET_SYNCHRONIZED_SCENE_PHASE(PedData.animData.iSyncSceneID) >= MAX_PED_ANIM_PHASE-0.01))
						IF (NOT DebugData.AnimData.bIncrementCycleAnim)
							DebugData.AnimData.bIncrementCycleAnim = TRUE
							DebugData.AnimData.iClip++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			DebugData.AnimData.bPlayAnim = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC DEBUG_MAINTAIN_SYNC_SCENE_PROP_VISIBILITY(PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iSyncSceneID, INT iClip, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	IF NOT IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
		EXIT
	ENDIF
	
	IF (iSyncSceneID = -1)
		EXIT
	ENDIF
	
	INT iProp
	FLOAT fPhaseTime
	FLOAT fVisibleEndPhase = -1.0
	FLOAT fVisibleStartPhase = -1.0
	REPEAT MAX_NUM_PED_PROPS iProp
		IF DOES_ENTITY_EXIST(Data.PropID[iProp])
			GET_ANIM_PROP_VISIBLE_PHASES(fVisibleStartPhase, fVisibleEndPhase, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), iProp, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
			IF (fVisibleStartPhase != -1.0)
			AND (fVisibleEndPhase != -1.0)
				fPhaseTime = GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneID)
				
				IF (fPhaseTime <= 0.02)
					CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_SHOW_PROP)
					CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PROP)
				ENDIF
				
				IF (fPhaseTime >= fVisibleEndPhase)
					IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HIDE_PROP)
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PROP)
						SET_ENTITY_ALPHA(Data.PropID[iProp], 0, FALSE)
					ENDIF
				ELIF (fPhaseTime >= fVisibleStartPhase)
					IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SHOW_PROP)
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SHOW_PROP)
						SET_ENTITY_ALPHA(Data.PropID[iProp], 255, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC UPDATE_PED_ANIMATION_DEBUG_WIDGETS(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	IF NOT DebugData.AnimData.bEnableAnimDebug
		EXIT
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(DebugData.AnimData.twActivity)
		STRING sActivity = DEBUG_GET_PED_ACTIVITY_STRING(INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity))
		IF NOT IS_STRING_NULL_OR_EMPTY(sActivity)
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.AnimData.twActivity, sActivity)
		ENDIF
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(DebugData.AnimData.twLogSearch)
		SET_CONTENTS_OF_TEXT_WIDGET(DebugData.AnimData.twLogSearch, "UPDATE_PED_ANIMATION_DEBUG_WIDGETS")
	ENDIF
	
	INT iChildPed
	REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPed
		STRING sChildActivity = DEBUG_GET_PED_ACTIVITY_STRING(INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.ChildPedData[iChildPed].iActivity))
		IF NOT IS_STRING_NULL_OR_EMPTY(sChildActivity)
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.AnimData.ChildPedData[iChildPed].twActivity, sChildActivity)
		ENDIF
	ENDREPEAT
	
	// Child ped anim testing
	IF (DebugData.AnimData.bApplyChildPedChanges)
		DebugData.AnimData.bApplyChildPedChanges = FALSE
		
		// Override parent slot 0 for debug
		IF (DebugData.AnimData.iAnimPed > -1 AND DebugData.AnimData.iAnimPed < MAX_NUM_TOTAL_LOCAL_PEDS)
			LocalData.ParentPedData[0].iParentID = DebugData.AnimData.iAnimPed
			
			REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPed
				IF (DebugData.AnimData.ChildPedData[iChildPed].iChildPedID != -1)
					LocalData.ParentPedData[0].iChildID[iChildPed] = DebugData.AnimData.ChildPedData[iChildPed].iChildPedID
					LocalData.PedData[DebugData.AnimData.ChildPedData[iChildPed].iChildPedID].iActivity = DebugData.AnimData.ChildPedData[iChildPed].iActivity
					DELETE_LOCAL_SCRIPT_PED_PROPS(LocalData.PedData[DebugData.AnimData.ChildPedData[iChildPed].iChildPedID])
				ENDIF
			ENDREPEAT
			
			DebugData.ParentPedData.bLocalApplyChanges = TRUE
		ENDIF
	ENDIF
	
	INT iClipID = DebugData.AnimData.iClip
	PED_ANIM_DATA highlightedPedAnimData
	PED_ANIM_DATA childPedAnimData[MAX_NUM_TOTAL_CHILD_PEDS]
	
	IF (DebugData.AnimData.iCachedActivity != DebugData.AnimData.iActivity)
		DebugData.AnimData.iCachedActivity = DebugData.AnimData.iActivity
		
		IF (DebugData.AnimData.iAnimPed != -1)
			IF (DebugData.AnimData.iAnimPed >= MAX_NUM_TOTAL_LOCAL_PEDS)
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					ServerBD.NetworkPedData[(DebugData.AnimData.iAnimPed-MAX_NUM_TOTAL_LOCAL_PEDS)].Data.iActivity = DebugData.AnimData.iActivity
					DELETE_NETWORK_SCRIPT_PED_PROPS(ServerBD.NetworkPedData[(DebugData.AnimData.iAnimPed-MAX_NUM_TOTAL_LOCAL_PEDS)])
				ENDIF
			ELSE
				LocalData.PedData[DebugData.AnimData.iAnimPed].iActivity = DebugData.AnimData.iActivity
				DELETE_LOCAL_SCRIPT_PED_PROPS(LocalData.PedData[DebugData.AnimData.iAnimPed])
			ENDIF
		ENDIF
		
		INT iMaxClips
		DebugData.AnimData.iMaxLowIntensityClips				= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_LOW, 			CLUB_MUSIC_INTENSITY_LOW, 			FALSE, FALSE)
		DebugData.AnimData.iMaxMediumIntensityClips				= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_MEDIUM, 		CLUB_MUSIC_INTENSITY_MEDIUM, 		FALSE, FALSE)+DebugData.AnimData.iMaxLowIntensityClips
		DebugData.AnimData.iMaxHighIntensityClips				= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_HIGH, 		CLUB_MUSIC_INTENSITY_HIGH, 			FALSE, FALSE)+DebugData.AnimData.iMaxMediumIntensityClips
		DebugData.AnimData.iMaxHighHandsIntensityClips			= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_HIGH_HANDS, 	CLUB_MUSIC_INTENSITY_HIGH_HANDS, 	FALSE, FALSE)+DebugData.AnimData.iMaxHighIntensityClips
		DebugData.AnimData.iMaxTranceIntensityClips				= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_TRANCE, 		CLUB_MUSIC_INTENSITY_TRANCE, 		FALSE, FALSE)+DebugData.AnimData.iMaxHighHandsIntensityClips
		
		DebugData.AnimData.iMaxLowMedTransIntensityClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_MEDIUM, 		CLUB_MUSIC_INTENSITY_LOW, 			TRUE, FALSE)+DebugData.AnimData.iMaxTranceIntensityClips
		DebugData.AnimData.iMaxLowHighTransIntensityClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_HIGH, 		CLUB_MUSIC_INTENSITY_LOW, 			TRUE, FALSE)+DebugData.AnimData.iMaxLowMedTransIntensityClips
		DebugData.AnimData.iMaxLowTranceTransIntensityClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_TRANCE, 		CLUB_MUSIC_INTENSITY_LOW, 			TRUE, FALSE)+DebugData.AnimData.iMaxLowHighTransIntensityClips
		
		DebugData.AnimData.iMaxMedLowTransIntensityClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_LOW, 			CLUB_MUSIC_INTENSITY_MEDIUM,		TRUE, FALSE)+DebugData.AnimData.iMaxLowTranceTransIntensityClips
		DebugData.AnimData.iMaxMedHighTransIntensityClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_HIGH, 		CLUB_MUSIC_INTENSITY_MEDIUM,		TRUE, FALSE)+DebugData.AnimData.iMaxMedLowTransIntensityClips
		DebugData.AnimData.iMaxMedTranceTransIntensityClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_TRANCE, 		CLUB_MUSIC_INTENSITY_MEDIUM,		TRUE, FALSE)+DebugData.AnimData.iMaxMedHighTransIntensityClips
		
		DebugData.AnimData.iMaxHighLowTransIntensityClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_LOW, 			CLUB_MUSIC_INTENSITY_HIGH, 			TRUE, FALSE)+DebugData.AnimData.iMaxMedTranceTransIntensityClips
		DebugData.AnimData.iMaxHighMedTransIntensityClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_MEDIUM, 		CLUB_MUSIC_INTENSITY_HIGH, 			TRUE, FALSE)+DebugData.AnimData.iMaxHighLowTransIntensityClips
		DebugData.AnimData.iMaxHighTranceTransIntensityClips	= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_TRANCE, 		CLUB_MUSIC_INTENSITY_HIGH, 			TRUE, FALSE)+DebugData.AnimData.iMaxHighMedTransIntensityClips
		
		DebugData.AnimData.iMaxTranceLowTransIntensityClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_LOW, 			CLUB_MUSIC_INTENSITY_TRANCE, 		TRUE, FALSE)+DebugData.AnimData.iMaxHighTranceTransIntensityClips
		DebugData.AnimData.iMaxTranceMedTransIntensityClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_MEDIUM, 		CLUB_MUSIC_INTENSITY_TRANCE, 		TRUE, FALSE)+DebugData.AnimData.iMaxTranceLowTransIntensityClips
		DebugData.AnimData.iMaxTranceHighTransIntensityClips	= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, CLUB_MUSIC_INTENSITY_HIGH, 		CLUB_MUSIC_INTENSITY_TRANCE, 		TRUE, FALSE)+DebugData.AnimData.iMaxTranceMedTransIntensityClips
		
		DebugData.AnimData.iMaxTransitionActivityOneClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, DEFAULT, DEFAULT, DEFAULT, FALSE, PED_TRANS_STATE_ACTIVITY_ONE)
		DebugData.AnimData.iMaxTransitionActivityTwoClips		= GET_MAX_ACTIVITY_ANIMS(ePedLocation, iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, DEFAULT, DEFAULT, DEFAULT, FALSE, PED_TRANS_STATE_ACTIVITY_TWO)+DebugData.AnimData.iMaxTransitionActivityOneClips
	ENDIF
	
	GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, iClipID, DebugData.AnimData.eMusicIntensity, DebugData.AnimData.ePedMusicIntensity, DebugData.AnimData.bDancingTransition)
	
	REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPed
		IF (DebugData.AnimData.ChildPedData[iChildPed].iChildPedID != -1)
			GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.ChildPedData[iChildPed].iActivity), childPedAnimData[iChildPed], iClipID, DebugData.AnimData.eMusicIntensity, DebugData.AnimData.ePedMusicIntensity, DebugData.AnimData.bDancingTransition)
		ENDIF
	ENDREPEAT
	
	IF IS_PEDS_BIT_SET(highlightedPedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
		DebugData.AnimData.bDancingTransition = FALSE
		
		IF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxTranceMedTransIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxTranceHighTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxTranceMedTransIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_HIGH
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_TRANCE
			DebugData.AnimData.bDancingTransition 	= TRUE
		
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxTranceLowTransIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxTranceMedTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxTranceLowTransIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_MEDIUM
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_TRANCE
			DebugData.AnimData.bDancingTransition 	= TRUE
		
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxHighTranceTransIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxTranceLowTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxHighTranceTransIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_LOW
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_TRANCE
			DebugData.AnimData.bDancingTransition 	= TRUE
			
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxHighMedTransIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxHighTranceTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxHighMedTransIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_TRANCE
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_HIGH
			DebugData.AnimData.bDancingTransition 	= TRUE
			
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxHighLowTransIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxHighMedTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxHighLowTransIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_MEDIUM
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_HIGH
			DebugData.AnimData.bDancingTransition 	= TRUE
		
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxMedTranceTransIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxHighLowTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxMedTranceTransIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_LOW
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_HIGH
			DebugData.AnimData.bDancingTransition 	= TRUE
		
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxMedHighTransIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxMedTranceTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxMedHighTransIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_TRANCE
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_MEDIUM 
			DebugData.AnimData.bDancingTransition 	= TRUE
		
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxMedLowTransIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxMedHighTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxMedLowTransIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_HIGH
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_MEDIUM 
			DebugData.AnimData.bDancingTransition 	= TRUE
		
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxLowTranceTransIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxMedLowTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxLowTranceTransIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_LOW
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_MEDIUM
			DebugData.AnimData.bDancingTransition 	= TRUE
			
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxLowHighTransIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxLowTranceTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxLowHighTransIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_TRANCE
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_LOW
			DebugData.AnimData.bDancingTransition 	= TRUE
		
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxLowMedTransIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxLowHighTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxLowMedTransIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_HIGH
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_LOW
			DebugData.AnimData.bDancingTransition 	= TRUE
		
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxTranceIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxLowMedTransIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxTranceIntensityClips
			DebugData.AnimData.eMusicIntensity 		= CLUB_MUSIC_INTENSITY_MEDIUM
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_LOW
			DebugData.AnimData.bDancingTransition 	= TRUE
		
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxHighHandsIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxTranceIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxHighHandsIntensityClips
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_TRANCE
		
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxHighIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxHighHandsIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxHighIntensityClips
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_HIGH_HANDS
		
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxMediumIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxHighIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxMediumIntensityClips
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_HIGH
			
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxLowIntensityClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxMediumIntensityClips-1)
			iClipID -= DebugData.AnimData.iMaxLowIntensityClips
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_MEDIUM
			
		ELSE
			DebugData.AnimData.ePedMusicIntensity 	= CLUB_MUSIC_INTENSITY_LOW
		ENDIF
		
		// Regrab data with music intensity changes
		GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, iClipID, DebugData.AnimData.eMusicIntensity, DebugData.AnimData.ePedMusicIntensity, DebugData.AnimData.bDancingTransition)
		
		REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPed
			IF (DebugData.AnimData.ChildPedData[iChildPed].iChildPedID != -1)
				GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.ChildPedData[iChildPed].iActivity), childPedAnimData[iChildPed], iClipID, DebugData.AnimData.eMusicIntensity, DebugData.AnimData.ePedMusicIntensity, DebugData.AnimData.bDancingTransition)
			ENDIF
		ENDREPEAT
	
	ELIF IS_PEDS_BIT_SET(highlightedPedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
		IF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxTransitionActivityTwoClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxTransitionActivityTwoClips)
			DebugData.AnimData.eTransitionState = PED_TRANS_STATE_ACTIVITY_TWO
			DebugData.AnimData.bPedTransition	= TRUE
			iClipID 							= 0
			
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxTransitionActivityOneClips AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxTransitionActivityTwoClips-1)
			DebugData.AnimData.eTransitionState = PED_TRANS_STATE_ACTIVITY_TWO
			DebugData.AnimData.bPedTransition	= FALSE
			iClipID -= DebugData.AnimData.iMaxTransitionActivityOneClips+1
			
		ELIF (DebugData.AnimData.iClip > DebugData.AnimData.iMaxTransitionActivityOneClips-1 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxTransitionActivityOneClips)
			DebugData.AnimData.eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE
			DebugData.AnimData.bPedTransition	= TRUE
			iClipID 							= 0
			
		ELIF (DebugData.AnimData.iClip >= 0 AND DebugData.AnimData.iClip <= DebugData.AnimData.iMaxTransitionActivityOneClips-1)
			DebugData.AnimData.eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE
			DebugData.AnimData.bPedTransition	= FALSE
		ENDIF
		
		// Regrab data with changes
		GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), highlightedPedAnimData, iClipID, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DebugData.AnimData.eTransitionState, DebugData.AnimData.bPedTransition)
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(DebugData.AnimData.twAnimClip)
		IF NOT IS_STRING_NULL_OR_EMPTY(highlightedPedAnimData.sAnimClip)
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.AnimData.twAnimClip, highlightedPedAnimData.sAnimClip)
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.AnimData.twAnimClip, "-")
		ENDIF
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(DebugData.AnimData.twAnimDict)
		IF NOT IS_STRING_NULL_OR_EMPTY(highlightedPedAnimData.sAnimDict)
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.AnimData.twAnimDict, highlightedPedAnimData.sAnimDict)
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.AnimData.twAnimDict, "Animation data not populated for this location.")
		ENDIF
	ENDIF
	
	REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPed
		IF DOES_TEXT_WIDGET_EXIST(DebugData.AnimData.ChildPedData[iChildPed].twAnimClip)
			IF NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData[iChildPed].sAnimClip)
				SET_CONTENTS_OF_TEXT_WIDGET(DebugData.AnimData.ChildPedData[iChildPed].twAnimClip, childPedAnimData[iChildPed].sAnimClip)
			ELSE
				SET_CONTENTS_OF_TEXT_WIDGET(DebugData.AnimData.ChildPedData[iChildPed].twAnimClip, "-")
			ENDIF
		ENDIF
		IF DOES_TEXT_WIDGET_EXIST(DebugData.AnimData.ChildPedData[iChildPed].twAnimDict)
			IF NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData[iChildPed].sAnimDict)
				SET_CONTENTS_OF_TEXT_WIDGET(DebugData.AnimData.ChildPedData[iChildPed].twAnimDict, childPedAnimData[iChildPed].sAnimDict)
			ELSE
				SET_CONTENTS_OF_TEXT_WIDGET(DebugData.AnimData.ChildPedData[iChildPed].twAnimDict, "Animation data not populated for this location.")
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Maintain sync scene props
	IF (DebugData.AnimData.iAnimPed != -1)
		DEBUG_MAINTAIN_SYNC_SCENE_PROP_VISIBILITY(LocalData.PedData[DebugData.AnimData.iAnimPed], highlightedPedAnimData, LocalData.PedData[DebugData.AnimData.iAnimPed].animData.iSyncSceneID, iClipID, DebugData.AnimData.eMusicIntensity, DebugData.AnimData.ePedMusicIntensity, DebugData.AnimData.bDancingTransition)
	ENDIF
	
	// Repeatedly play an animation 
	IF (DebugData.AnimData.bPlayAnim)
		DebugData.AnimData.bPlayAnim = FALSE
		IF (DebugData.AnimData.iAnimPed != -1)
			IF IS_PEDS_BIT_SET(highlightedPedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
				IF (DebugData.AnimData.iAnimPed >= MAX_NUM_TOTAL_LOCAL_PEDS)
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						DEBUG_PLAY_TASK_ANIM(ePedLocation, DebugData, ServerBD.NetworkPedData[(DebugData.AnimData.iAnimPed-MAX_NUM_TOTAL_LOCAL_PEDS)], ServerBD.NetworkPedData[(DebugData.AnimData.iAnimPed-MAX_NUM_TOTAL_LOCAL_PEDS)].Data, highlightedPedAnimData, DebugData.AnimData.iAnimPed, TRUE)
					ENDIF
				ELSE
					DEBUG_PLAY_TASK_ANIM(ePedLocation, DebugData, ServerBD.NetworkPedData[0], LocalData.PedData[DebugData.AnimData.iAnimPed], highlightedPedAnimData, DebugData.AnimData.iAnimPed)
				ENDIF
			ELIF IS_PEDS_BIT_SET(highlightedPedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
				IF (DebugData.AnimData.iAnimPed < MAX_NUM_TOTAL_LOCAL_PEDS)
					DEBUG_PLAY_SYNC_SCENE(ePedLocation, DebugData, LocalData, LocalData.PedData[DebugData.AnimData.iAnimPed], highlightedPedAnimData, childPedAnimData, iClipID, DebugData.AnimData.iAnimPed, DebugData.AnimData.eMusicIntensity, DebugData.AnimData.ePedMusicIntensity, DebugData.AnimData.bDancingTransition)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Cycle through all animation in an activity
	IF (DebugData.AnimData.bCycleAnims)
		IF (DebugData.AnimData.iAnimPed != -1)
			IF IS_PEDS_BIT_SET(highlightedPedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
				IF (DebugData.AnimData.iAnimPed >= MAX_NUM_TOTAL_LOCAL_PEDS)
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						DEBUG_CYCLE_TASK_ANIM(ePedLocation, DebugData, ServerBD.NetworkPedData[(DebugData.AnimData.iAnimPed-MAX_NUM_TOTAL_LOCAL_PEDS)], ServerBD.NetworkPedData[(DebugData.AnimData.iAnimPed-MAX_NUM_TOTAL_LOCAL_PEDS)].Data, highlightedPedAnimData, DebugData.AnimData.iAnimPed, TRUE)
					ENDIF
				ELSE
					DEBUG_CYCLE_TASK_ANIM(ePedLocation, DebugData, ServerBD.NetworkPedData[0], LocalData.PedData[DebugData.AnimData.iAnimPed], highlightedPedAnimData, DebugData.AnimData.iAnimPed)
				ENDIF
			ELIF IS_PEDS_BIT_SET(highlightedPedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
				IF (DebugData.AnimData.iAnimPed < MAX_NUM_TOTAL_LOCAL_PEDS)
					DEBUG_CYCLE_SYNC_SCENE(ePedLocation, DebugData, LocalData, LocalData.PedData[DebugData.AnimData.iAnimPed], highlightedPedAnimData, childPedAnimData, iClipID, DebugData.AnimData.iAnimPed, DebugData.AnimData.eMusicIntensity, DebugData.AnimData.ePedMusicIntensity, DebugData.AnimData.bDancingTransition)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		DebugData.AnimData.bCycleAnimsActive = FALSE
	ENDIF
	
	IF (DebugData.AnimData.bOutputInitialActivityAnimData)
		
		INT iClip
		INT iIntensity
		INT iPedIntensity
		INT iAnimDictsLoaded
		VECTOR vInitialAnimCoords
		VECTOR vInitialAnimRotation
		PED_ANIM_DATA pedAnimData
		
		SWITCH DebugData.iState
			CASE 0
				
				// The anim dictionary must be loaded in order to query the inital coords and rotations of the anim.
				
				REPEAT ENUM_TO_INT(CLUB_MUSIC_INTENSITY_TOTAL)-1 iIntensity
					RESET_PED_ANIM_DATA(pedAnimData)
					GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), pedAnimData, iClip, INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity), INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity))
					
					IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
						IF REQUEST_AND_LOAD_ANIMATION_DICT(pedAnimData.sAnimDict)
							iAnimDictsLoaded++
						ENDIF
					ENDIF
					
				ENDREPEAT
				
				REPEAT ENUM_TO_INT(CLUB_MUSIC_INTENSITY_TOTAL)-1 iIntensity
					RESET_PED_ANIM_DATA(pedAnimData)
					GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), pedAnimData, iClip, INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity), INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity), TRUE)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
						IF REQUEST_AND_LOAD_ANIMATION_DICT(pedAnimData.sAnimDict)
							iAnimDictsLoaded++
						ENDIF
					ENDIF
					
				ENDREPEAT
				
				IF (iAnimDictsLoaded = (ENUM_TO_INT(CLUB_MUSIC_INTENSITY_TOTAL)-1)*2)
					DebugData.iState++
				ENDIF
				
			BREAK
			CASE 1
				
				// Output all the anim initial data
				
				PRINTLN("[AM_MP_PEDS] UPDATE_PED_ANIMATION_DEBUG_WIDGETS")
				PRINTLN("[AM_MP_PEDS] UPDATE_PED_ANIMATION_DEBUG_WIDGETS")
				PRINTLN("[AM_MP_PEDS] UPDATE_PED_ANIMATION_DEBUG_WIDGETS - Anims: ")
				
				REPEAT ENUM_TO_INT(CLUB_MUSIC_INTENSITY_TOTAL)-1 iIntensity
					PRINTLN("[AM_MP_PEDS] UPDATE_PED_ANIMATION_DEBUG_WIDGETS")
					
					REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
						RESET_PED_ANIM_DATA(pedAnimData)
						GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), pedAnimData, iClip, INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity), INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity))
						
						IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
						AND NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
							vInitialAnimCoords		= GET_ANIM_INITIAL_OFFSET_POSITION(pedAnimData.sAnimDict, pedAnimData.sAnimClip, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							vInitialAnimRotation	= GET_ANIM_INITIAL_OFFSET_ROTATION(pedAnimData.sAnimDict, pedAnimData.sAnimClip, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							PRINTLN("[AM_MP_PEDS] UPDATE_PED_ANIMATION_DEBUG_WIDGETS - Intensity: ", GET_CLUB_MUSIC_INTENSITY_NAME(INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity)), " Clip: ", iClip, " Clip Name: ", pedAnimData.sAnimClip, " Initial Anim Coords: ", vInitialAnimCoords, " Initial Anim Rotation: ", vInitialAnimRotation)
						ELSE
							BREAKLOOP
						ENDIF
						
					ENDREPEAT
				ENDREPEAT
				
				PRINTLN("[AM_MP_PEDS] UPDATE_PED_ANIMATION_DEBUG_WIDGETS")
				PRINTLN("[AM_MP_PEDS] UPDATE_PED_ANIMATION_DEBUG_WIDGETS - Transition Anims: ")
				
				REPEAT ENUM_TO_INT(CLUB_MUSIC_INTENSITY_TOTAL)-1 iPedIntensity
					PRINTLN("[AM_MP_PEDS] UPDATE_PED_ANIMATION_DEBUG_WIDGETS")
					
					REPEAT ENUM_TO_INT(CLUB_MUSIC_INTENSITY_TOTAL)-1 iIntensity
						REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
							RESET_PED_ANIM_DATA(pedAnimData)
							GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, DebugData.AnimData.iActivity), pedAnimData, iClip, INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity), INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iPedIntensity), TRUE)
							
							IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
							AND NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
								vInitialAnimCoords		= GET_ANIM_INITIAL_OFFSET_POSITION(pedAnimData.sAnimDict, pedAnimData.sAnimClip, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								vInitialAnimRotation	= GET_ANIM_INITIAL_OFFSET_ROTATION(pedAnimData.sAnimDict, pedAnimData.sAnimClip, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								PRINTLN("[AM_MP_PEDS] UPDATE_PED_ANIMATION_DEBUG_WIDGETS - From Ped Intensity: ", GET_CLUB_MUSIC_INTENSITY_NAME(INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iPedIntensity)), " To Intensity: ", GET_CLUB_MUSIC_INTENSITY_NAME(INT_TO_ENUM(CLUB_MUSIC_INTENSITY, iIntensity)), " Clip: ", iClip, " Clip Name: ", pedAnimData.sAnimClip, " Initial Anim Coords: ", vInitialAnimCoords, " Initial Anim Rotation: ", vInitialAnimRotation)
							ELSE
								PRINTLN("[AM_MP_PEDS] ")
								BREAKLOOP
							ENDIF
						ENDREPEAT
					ENDREPEAT
				ENDREPEAT
				
				DebugData.iState = 0
				DebugData.AnimData.bOutputInitialActivityAnimData = FALSE
				
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════╡ DEBUG WIDGETS - PARENT PEDS ╞═════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_PED_PARENT_WIDGETS(PED_DEBUG_PARENT_PED_DATA &DebugParentPedData, INT iPed, BOOL bNetworkPed = FALSE)
	
	TEXT_LABEL_15 str = "Ped: "
	str += iPed
	
	START_WIDGET_GROUP(str)
		
		IF (bNetworkPed)
			ADD_WIDGET_INT_SLIDER("Parent Ped ID", DebugParentPedData.TempNetworkParentPedData[iPed].iParentID, -1, MAX_NUM_TOTAL_NETWORK_PEDS, 1)
		ELSE
			ADD_WIDGET_INT_SLIDER("Parent Ped ID", DebugParentPedData.TempLocalParentPedData[iPed].iParentID, -1, MAX_NUM_TOTAL_LOCAL_PEDS, 1)
		ENDIF
		
		INT iChildPed
		TEXT_LABEL_15 tlLabel = ""
		REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPed
			tlLabel = ""
			tlLabel += "Child Ped ID "
			tlLabel += iChildPed
			IF (bNetworkPed)
				ADD_WIDGET_INT_SLIDER(tlLabel, DebugParentPedData.TempNetworkParentPedData[iPed].iChildID[iChildPed], -1, MAX_NUM_TOTAL_LOCAL_PEDS, 1)
			ELSE
				ADD_WIDGET_INT_SLIDER(tlLabel, DebugParentPedData.TempLocalParentPedData[iPed].iChildID[iChildPed], -1, MAX_NUM_TOTAL_LOCAL_PEDS, 1)
			ENDIF
		ENDREPEAT
		
		ADD_WIDGET_STRING("")
		
		IF (bNetworkPed)
			ADD_WIDGET_BOOL("Apply Changes", DebugParentPedData.bNetworkApplyChanges)
		ELSE
			ADD_WIDGET_BOOL("Apply Changes", DebugParentPedData.bLocalApplyChanges)
		ENDIF
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC CREATE_PED_PARENT_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INITIALISE_PED_PARENT_DEBUG_DATA(DebugData.ParentPedData, ServerBD, LocalData)
	
	INT iPed
	ADD_WIDGET_STRING("")
	
	START_WIDGET_GROUP("Child Peds")
		ADD_WIDGET_STRING("Child Ped Data is outputted in the same file when pressing 'Output Ped Data' in the General Tab")
		
		START_WIDGET_GROUP("Local Peds")
			
			REPEAT MAX_NUM_LOCAL_PARENT_PEDS iPed
				CREATE_PED_PARENT_WIDGETS(DebugData.ParentPedData, iPed)
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Network Peds")
			
			REPEAT MAX_NUM_NETWORK_PARENT_PEDS iPed
				CREATE_PED_PARENT_WIDGETS(DebugData.ParentPedData, iPed, TRUE)
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_LOCAL_PED_PARENT_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData)
	
	INT iChildPedID
	INT iParentPedID
	
	IF (DebugData.ParentPedData.bLocalApplyChanges)
		DebugData.ParentPedData.bLocalApplyChanges = FALSE
		
		REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iParentPedID
			CLEAR_PEDS_BIT(LocalData.PedData[iParentPedID].iBS, BS_PED_DATA_CHILD_PED)
			CLEAR_PEDS_BIT(LocalData.PedData[iParentPedID].iBS, BS_PED_DATA_PARENT_PED)
		ENDREPEAT
		
		REPEAT MAX_NUM_LOCAL_PARENT_PEDS iParentPedID
			IF (DebugData.ParentPedData.TempLocalParentPedData[iParentPedID].iParentID != -1)
				LocalData.ParentPedData[iParentPedID].iParentID = DebugData.ParentPedData.TempLocalParentPedData[iParentPedID].iParentID
				
				CLEAR_PEDS_BIT(LocalData.PedData[LocalData.ParentPedData[iParentPedID].iParentID].iBS, BS_PED_DATA_PARENT_PED)
				
				INT iValidChildPeds = 0
				REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPedID
					IF (DebugData.ParentPedData.TempLocalParentPedData[iParentPedID].iChildID[iChildPedID] != -1)
						LocalData.ParentPedData[iParentPedID].iChildID[iChildPedID] = DebugData.ParentPedData.TempLocalParentPedData[iParentPedID].iChildID[iChildPedID]
						IS_PEDS_BIT_SET(LocalData.PedData[LocalData.ParentPedData[iParentPedID].iChildID[iChildPedID]].iBS, BS_PED_DATA_CHILD_PED)
						iValidChildPeds++
					ENDIF
				ENDREPEAT
				
				IF (iValidChildPeds > 0)
					SET_PEDS_BIT(LocalData.PedData[LocalData.ParentPedData[iParentPedID].iParentID].iBS, BS_PED_DATA_PARENT_PED)
					LocalData.iParentPedID[LocalData.ParentPedData[iParentPedID].iParentID] = iParentPedID
				ENDIF
				
			ENDIF
		ENDREPEAT
		
		// Update child peds coords/heading to match parents
		REPEAT MAX_NUM_LOCAL_PARENT_PEDS iParentPedID
			IF (LocalData.ParentPedData[iParentPedID].iParentID != -1)
				REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPedID
					IF (LocalData.ParentPedData[iParentPedID].iChildID[iChildPedID] != -1)
						LocalData.PedData[LocalData.ParentPedData[iParentPedID].iChildID[iChildPedID]].vRotation 	= LocalData.PedData[LocalData.ParentPedData[iParentPedID].iParentID].vRotation
						LocalData.PedData[LocalData.ParentPedData[iParentPedID].iChildID[iChildPedID]].vPosition 	= LocalData.PedData[LocalData.ParentPedData[iParentPedID].iParentID].vPosition
					ENDIF
				ENDREPEAT
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC UPDATE_NETWORK_PED_PARENT_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD)
	
	INT iChildPedID
	INT iParentPedID
	
	IF (DebugData.ParentPedData.bNetworkApplyChanges)
		DebugData.ParentPedData.bNetworkApplyChanges = FALSE
		
		REPEAT MAX_NUM_TOTAL_NETWORK_PEDS iParentPedID
			CLEAR_PEDS_BIT(ServerBD.NetworkPedData[iParentPedID].Data.iBS, BS_PED_DATA_CHILD_PED)
			CLEAR_PEDS_BIT(ServerBD.NetworkPedData[iParentPedID].Data.iBS, BS_PED_DATA_PARENT_PED)
		ENDREPEAT
		
		REPEAT MAX_NUM_NETWORK_PARENT_PEDS iParentPedID
			IF (DebugData.ParentPedData.TempNetworkParentPedData[iParentPedID].iParentID != -1)
				ServerBD.ParentPedData[iParentPedID].iParentID = DebugData.ParentPedData.TempNetworkParentPedData[iParentPedID].iParentID
				
				CLEAR_PEDS_BIT(ServerBD.NetworkPedData[ServerBD.ParentPedData[iParentPedID].iParentID].Data.iBS, BS_PED_DATA_PARENT_PED)
				
				INT iValidChildPeds = 0
				REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPedID
					IF (DebugData.ParentPedData.TempNetworkParentPedData[iParentPedID].iChildID[iChildPedID] != -1)
						ServerBD.ParentPedData[iParentPedID].iChildID[iChildPedID] = DebugData.ParentPedData.TempNetworkParentPedData[iParentPedID].iChildID[iChildPedID]
						IS_PEDS_BIT_SET(ServerBD.NetworkPedData[ServerBD.ParentPedData[iParentPedID].iChildID[iChildPedID]].Data.iBS, BS_PED_DATA_CHILD_PED)
						iValidChildPeds++
					ENDIF
				ENDREPEAT
				
				IF (iValidChildPeds > 0)
					SET_PEDS_BIT(ServerBD.NetworkPedData[ServerBD.ParentPedData[iParentPedID].iParentID].Data.iBS, BS_PED_DATA_PARENT_PED)
					ServerBD.iParentPedID[ServerBD.ParentPedData[iParentPedID].iParentID] = iParentPedID
				ENDIF
				
			ENDIF
		ENDREPEAT
		
		// Update child peds coords/heading to match parents
		REPEAT MAX_NUM_NETWORK_PARENT_PEDS iParentPedID
			IF (ServerBD.ParentPedData[iParentPedID].iParentID != -1)
				REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChildPedID
					IF (ServerBD.ParentPedData[iParentPedID].iChildID[iChildPedID] != -1)
						ServerBD.NetworkPedData[ServerBD.ParentPedData[iParentPedID].iChildID[iChildPedID]].Data.vRotation 	= ServerBD.NetworkPedData[ServerBD.ParentPedData[iParentPedID].iParentID].Data.vRotation
						ServerBD.NetworkPedData[ServerBD.ParentPedData[iParentPedID].iChildID[iChildPedID]].Data.vPosition 	= ServerBD.NetworkPedData[ServerBD.ParentPedData[iParentPedID].iParentID].Data.vPosition
					ENDIF
				ENDREPEAT
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ DEBUG WIDGETS - PROPS ╞═════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_PED_PROP_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData)
	
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP("Props")
		START_WIDGET_GROUP("Local Peds")
			
			ADD_WIDGET_INT_SLIDER("Ped ID", DebugData.HighlightData.iHighlightedLocalPed, -1, MAX_NUM_TOTAL_LOCAL_PEDS, 1)
			ADD_WIDGET_INT_SLIDER("Prop ID", DebugData.PropData.iHighlightedLocalPedProp, 0, MAX_NUM_PED_PROPS, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_VECTOR_SLIDER("Prop Coords", DebugData.PropData.vLocalPropCoords, -99999.9, 99999.9, 0.01)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_VECTOR_SLIDER("Prop Rotation", DebugData.PropData.vLocalPropRotation, -360.0, 360.0, 0.1)
			
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Network Peds")
			
			ADD_WIDGET_INT_SLIDER("Ped ID", DebugData.HighlightData.iHighlightedNetworkPed, -1, MAX_NUM_TOTAL_LOCAL_PEDS, 1)
			ADD_WIDGET_INT_SLIDER("Prop ID", DebugData.PropData.iHighlightedNetworkPedProp, 0, MAX_NUM_PED_PROPS, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_VECTOR_SLIDER("Prop Coords", DebugData.PropData.vNetworkPropCoords, -99999.9, 99999.9, 0.01)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_VECTOR_SLIDER("Prop Rotation", DebugData.PropData.vNetworkPropRotation, -360.0, 360.0, 0.1)
			
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_LOCAL_PED_PROP_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData, PED_LOCATIONS ePedLocation)
	
	IF (DebugData.HighlightData.iHighlightedLocalPed = -1)
		EXIT
	ENDIF
	
	PED_PROP_DATA pedPropData
	
	IF (DebugData.PropData.iCachedHighlightedLocalPed != DebugData.HighlightData.iHighlightedLocalPed)
	OR (DebugData.PropData.iCachedHighlightedLocalPedProp != DebugData.PropData.iHighlightedLocalPedProp)
		
		DebugData.PropData.iCachedHighlightedLocalPed		= DebugData.HighlightData.iHighlightedLocalPed
		DebugData.PropData.iCachedHighlightedLocalPedProp 	= DebugData.PropData.iHighlightedLocalPedProp
		
		GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].iActivity), DebugData.PropData.iHighlightedLocalPedProp, pedPropData)
		
		DebugData.PropData.vLocalPropCoords					= pedPropData.vBoneOffsetPosition
		DebugData.PropData.vLocalPropRotation				= pedPropData.vBoneOffsetRotation
		
		DebugData.PropData.vCachedLocalPropCoords			= DebugData.PropData.vLocalPropCoords
		DebugData.PropData.vCachedLocalPropRotation			= DebugData.PropData.vLocalPropRotation
		
	ENDIF
	
	IF IS_ENTITY_ALIVE(LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PedID)
	AND DOES_ENTITY_EXIST(LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PropID[DebugData.PropData.iHighlightedLocalPedProp])
		IF NOT ARE_VECTORS_EQUAL(DebugData.PropData.vCachedLocalPropCoords, DebugData.PropData.vLocalPropCoords)
		OR NOT ARE_VECTORS_EQUAL(DebugData.PropData.vCachedLocalPropRotation, DebugData.PropData.vLocalPropRotation)
			
			DebugData.PropData.vCachedLocalPropCoords 		= DebugData.PropData.vLocalPropCoords
			DebugData.PropData.vCachedLocalPropRotation		= DebugData.PropData.vLocalPropRotation
			
			GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].iActivity), DebugData.PropData.iHighlightedLocalPedProp, pedPropData)
			
			DETACH_ENTITY(LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PropID[DebugData.PropData.iHighlightedLocalPedProp])
			ATTACH_ENTITY_TO_ENTITY(LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PropID[DebugData.PropData.iHighlightedLocalPedProp], LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PedID, GET_PED_BONE_INDEX(LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PedID, pedPropData.ePedBone), DebugData.PropData.vLocalPropCoords, DebugData.PropData.vLocalPropRotation, TRUE, TRUE)
			
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_NETWORK_PED_PROP_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation)
	
	IF (DebugData.HighlightData.iHighlightedNetworkPed = -1)
		EXIT
	ENDIF
	
	PED_PROP_DATA pedPropData
	
	IF (DebugData.PropData.iCachedHighlightedNetworkPed != DebugData.HighlightData.iHighlightedNetworkPed)
	OR (DebugData.PropData.iCachedHighlightedNetworkPedProp != DebugData.PropData.iHighlightedNetworkPedProp)
		
		DebugData.PropData.iCachedHighlightedNetworkPed		= DebugData.HighlightData.iHighlightedNetworkPed
		DebugData.PropData.iCachedHighlightedNetworkPedProp = DebugData.PropData.iHighlightedNetworkPedProp
		
		GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[DebugData.HighlightData.iHighlightedNetworkPed].Data.iActivity), DebugData.PropData.iHighlightedNetworkPedProp, pedPropData)
		
		DebugData.PropData.vNetworkPropCoords				= pedPropData.vBoneOffsetPosition
		DebugData.PropData.vNetworkPropRotation				= pedPropData.vBoneOffsetRotation
		
		DebugData.PropData.vCachedNetworkPropCoords			= DebugData.PropData.vNetworkPropCoords
		DebugData.PropData.vCachedNetworkPropRotation		= DebugData.PropData.vNetworkPropRotation
		
	ENDIF
	
	IF IS_ENTITY_ALIVE(ServerBD.NetworkPedData[DebugData.HighlightData.iHighlightedNetworkPed].Data.PedID)
	AND DOES_ENTITY_EXIST(ServerBD.NetworkPedData[DebugData.HighlightData.iHighlightedNetworkPed].Data.PropID[DebugData.PropData.iHighlightedNetworkPedProp])
		IF NOT ARE_VECTORS_EQUAL(DebugData.PropData.vCachedNetworkPropCoords, DebugData.PropData.vNetworkPropCoords)
		OR NOT ARE_VECTORS_EQUAL(DebugData.PropData.vCachedNetworkPropRotation, DebugData.PropData.vNetworkPropRotation)
			
			DebugData.PropData.vCachedNetworkPropCoords 	= DebugData.PropData.vNetworkPropCoords
			DebugData.PropData.vCachedNetworkPropRotation	= DebugData.PropData.vNetworkPropRotation
			
			GET_PED_PROP_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[DebugData.HighlightData.iHighlightedNetworkPed].Data.iActivity), DebugData.PropData.iHighlightedNetworkPedProp, pedPropData)
			
			DETACH_ENTITY(ServerBD.NetworkPedData[DebugData.HighlightData.iHighlightedNetworkPed].Data.PropID[DebugData.PropData.iHighlightedNetworkPedProp])
			ATTACH_ENTITY_TO_ENTITY(ServerBD.NetworkPedData[DebugData.HighlightData.iHighlightedNetworkPed].Data.PropID[DebugData.PropData.iHighlightedNetworkPedProp], ServerBD.NetworkPedData[DebugData.HighlightData.iHighlightedNetworkPed].Data.PedID, GET_PED_BONE_INDEX(ServerBD.NetworkPedData[DebugData.HighlightData.iHighlightedNetworkPed].Data.PedID, pedPropData.ePedBone), DebugData.PropData.vNetworkPropCoords, DebugData.PropData.vNetworkPropRotation, TRUE, TRUE)
			
		ENDIF
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ DEBUG WIDGETS - PED MODELS ╞═══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC STRING GET_PED_MODEL_COMP_STRING(INT iComp)
	STRING sComp = ""
	SWITCH iComp
		CASE 0	sComp = "PED_COMP_HEAD"			BREAK
		CASE 1	sComp = "PED_COMP_BERD"			BREAK
		CASE 2	sComp = "PED_COMP_HAIR"			BREAK
		CASE 3	sComp = "PED_COMP_TORSO"		BREAK
		CASE 4	sComp = "PED_COMP_LEG"			BREAK
		CASE 5	sComp = "PED_COMP_HAND"			BREAK
		CASE 6	sComp = "PED_COMP_FEET"			BREAK
		CASE 7	sComp = "PED_COMP_TEETH"		BREAK
		CASE 8	sComp = "PED_COMP_SPECIAL"		BREAK
		CASE 9	sComp = "PED_COMP_SPECIAL2"		BREAK
		CASE 10	sComp = "PED_COMP_DECL"			BREAK 
		CASE 11 sComp = "PED_COMP_JBIB"			BREAK
	ENDSWITCH
	RETURN sComp
ENDFUNC

PROC CREATE_PED_MODEL_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData)
	
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP("Ped Models & Clothing")
		ADD_WIDGET_INT_SLIDER("Highlighted Local Ped", DebugData.HighlightData.iHighlightedLocalPed, -1, MAX_NUM_TOTAL_LOCAL_PEDS, 1)
		ADD_WIDGET_INT_SLIDER("Ped Model", DebugData.ModelData.iPedModel, 0, ENUM_TO_INT(PED_MODEL_TOTAL)-1, 1)
		DebugData.ModelData.twPedModel = ADD_TEXT_WIDGET("Ped Model: ")
		
		ADD_WIDGET_STRING("")
		ADD_WIDGET_BOOL("Reset Comp Values", DebugData.ModelData.bResetCompValues)
		
		INT iComp
		REPEAT NUM_PED_COMPONENTS iComp
			START_WIDGET_GROUP(GET_PED_MODEL_COMP_STRING(iComp))
				ADD_WIDGET_INT_SLIDER("Drawable: ", DebugData.ModelData.ComponentConfig.ComponentData[iComp].iDrawable, 0, MAX_NUM_PED_CLOTHING_COMPONENT_VALUE, 1)
				ADD_WIDGET_INT_SLIDER("Texture: ", DebugData.ModelData.ComponentConfig.ComponentData[iComp].iTexture, 0, MAX_NUM_PED_CLOTHING_COMPONENT_VALUE, 1)
			STOP_WIDGET_GROUP()
		ENDREPEAT
		
		ADD_WIDGET_BOOL("Apply Changes", DebugData.ModelData.bApplyChanges)
		ADD_WIDGET_STRING("")
		
		ADD_WIDGET_BOOL("Create Packed Comp Vars", DebugData.ModelData.bCreatePackedCompVars)
		
		INT iVar
		TEXT_LABEL_63 tlDrawableWidgetName = ""
		REPEAT MAX_NUM_PED_CLOTHING_COMPONENT_VARS iVar
			tlDrawableWidgetName = "Packed Drawable Comps ["
			tlDrawableWidgetName += iVar
			tlDrawableWidgetName += "]: "
			ADD_WIDGET_INT_SLIDER(tlDrawableWidgetName, DebugData.ModelData.iPackedDrawable[iVar], -HIGHEST_INT, HIGHEST_INT, 1)
			tlDrawableWidgetName = ""
		ENDREPEAT
		
		ADD_WIDGET_STRING("")
		TEXT_LABEL_63 tlTextureWidgetName = ""
		REPEAT MAX_NUM_PED_CLOTHING_COMPONENT_VARS iVar
			tlTextureWidgetName = "Packed Texture Comps ["
			tlTextureWidgetName += iVar
			tlTextureWidgetName += "]: "
			ADD_WIDGET_INT_SLIDER(tlTextureWidgetName, DebugData.ModelData.iPackedTexture[iVar], -HIGHEST_INT, HIGHEST_INT, 1)
			tlTextureWidgetName = ""
		ENDREPEAT
		
		ADD_WIDGET_BOOL("Unpack Comp Vars to PED_COMPS above", DebugData.ModelData.bUnpackCompVars)
		
		ADD_WIDGET_STRING("")
		ADD_WIDGET_STRING("Outputs all ped data with their current packed drawable and texture values")
		ADD_WIDGET_BOOL("Output All Peds Clothing", DebugData.ModelData.bOutputAllPedClothingValues)
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC RESET_LOCAL_PED_MODEL_CLOTHING_COMPONENT_DEBUG_WIDGET_VALUES(PED_DEBUG_DATA &DebugData)
	
	INT iComps
	REPEAT NUM_PED_COMPONENTS iComps
		DebugData.ModelData.ComponentConfig.ComponentData[iComps].iDrawable = 0
		DebugData.ModelData.ComponentConfig.ComponentData[iComps].iTexture = 0
	ENDREPEAT
	
ENDPROC

PROC APPLY_PED_CLOTHING_RESTRICTIONS(PED_INDEX &PedID, PED_CLOTHING_COMPONENT_CONFIG &ComponentConfig, PED_COMPONENT ePedComponent, PED_MODELS ePedModel)
	
	SWITCH ePedModel
		CASE PED_MODEL_BEACH_MALE_01
			SWITCH ePedComponent
				CASE PED_COMP_TEETH
					// Remove chains
					ComponentConfig.ComponentData[ENUM_TO_INT(ePedComponent)].iDrawable = 0
					ComponentConfig.ComponentData[ENUM_TO_INT(ePedComponent)].iTexture = 0
					SET_PED_COMPONENT_VARIATION(PedID, ePedComponent, ComponentConfig.ComponentData[ENUM_TO_INT(ePedComponent)].iDrawable, ComponentConfig.ComponentData[ENUM_TO_INT(ePedComponent)].iTexture)
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_MODEL_ISLAND_GUARDS
			SWITCH ePedComponent
				CASE PED_COMP_SPECIAL2
					// Remove body armor
					ComponentConfig.ComponentData[ENUM_TO_INT(ePedComponent)].iDrawable = 0
					ComponentConfig.ComponentData[ENUM_TO_INT(ePedComponent)].iTexture = 0
					SET_PED_COMPONENT_VARIATION(PedID, ePedComponent, ComponentConfig.ComponentData[ENUM_TO_INT(ePedComponent)].iDrawable, ComponentConfig.ComponentData[ENUM_TO_INT(ePedComponent)].iTexture)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

PROC RESET_PED_CLOTHING_COMPONENT_CONFIG(PED_CLOTHING_COMPONENT_CONFIG &ComponentConfig)
	
	INT iComp
	REPEAT NUM_PED_COMPONENTS iComp
		ComponentConfig.ComponentData[iComp].iDrawable = 0
		ComponentConfig.ComponentData[iComp].iTexture = 0
	ENDREPEAT
	
ENDPROC

PROC UPDATE_LOCAL_PED_MODEL_DEBUG_WIDGETS(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	IF DOES_TEXT_WIDGET_EXIST(DebugData.ModelData.twPedModel)
		STRING sPedModel = DEBUG_GET_PED_MODEL_STRING(INT_TO_ENUM(PED_MODELS, DebugData.ModelData.iPedModel))
		IF NOT IS_STRING_NULL_OR_EMPTY(sPedModel)
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.ModelData.twPedModel, sPedModel)
		ENDIF
	ENDIF
	
	IF (DebugData.ModelData.bResetCompValues)
		DebugData.ModelData.bResetCompValues = FALSE
		RESET_LOCAL_PED_MODEL_CLOTHING_COMPONENT_DEBUG_WIDGET_VALUES(DebugData)
	ENDIF
	
	IF (DebugData.HighlightData.iHighlightedLocalPed != -1)
		IF (DebugData.ModelData.bApplyChanges)
			SWITCH DebugData.ModelData.iDebugFlow
				CASE 0
					LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].iPedModel = DebugData.ModelData.iPedModel
					DELETE_LOCAL_SCRIPT_PED(LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed])
					DebugData.ModelData.iDebugFlow = 1
				BREAK
				CASE 1
					IF IS_ENTITY_ALIVE(LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PedID)
						INT iComp
						REPEAT NUM_PED_COMPONENTS iComp
							SET_PED_COMPONENT_VARIATION(LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PedID, INT_TO_ENUM(PED_COMPONENT, iComp), DebugData.ModelData.ComponentConfig.ComponentData[iComp].iDrawable, DebugData.ModelData.ComponentConfig.ComponentData[iComp].iTexture)
						ENDREPEAT
						DebugData.ModelData.iDebugFlow = 0
						DebugData.ModelData.bApplyChanges = FALSE
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	IF (DebugData.ModelData.bCreatePackedCompVars)
		DebugData.ModelData.bCreatePackedCompVars = FALSE
		
		INT iComp
		PED_CLOTHING_COMPONENT_CONFIG ComponentConfig
		
		REPEAT NUM_PED_COMPONENTS iComp
			ComponentConfig.ComponentData[iComp].iDrawable = DebugData.ModelData.ComponentConfig.ComponentData[iComp].iDrawable
			ComponentConfig.ComponentData[iComp].iTexture = DebugData.ModelData.ComponentConfig.ComponentData[iComp].iTexture
		ENDREPEAT
		
		PACK_PED_CLOTHING_COMPONENT_CONFIG(DebugData.ModelData.iPackedDrawable, DebugData.ModelData.iPackedTexture, ComponentConfig)
	ENDIF
	
	IF (DebugData.ModelData.bUnpackCompVars)
		DebugData.ModelData.bUnpackCompVars = FALSE
		RESET_LOCAL_PED_MODEL_CLOTHING_COMPONENT_DEBUG_WIDGET_VALUES(DebugData)
		UNPACK_PED_CLOTHING_COMPONENT_CONFIG(DebugData.ModelData.iPackedDrawable, DebugData.ModelData.iPackedTexture, DebugData.ModelData.ComponentConfig)
	ENDIF
	
	IF (DebugData.ModelData.bOutputAllPedClothingValues)
		DebugData.ModelData.bOutputAllPedClothingValues = FALSE
		
		INT iPed
		INT iComp
		PED_CLOTHING_COMPONENT_CONFIG ComponentConfig
		
		// Get the drawable and texture from each ped and pack
		REPEAT ServerBD.iMaxLocalPeds iPed
			IF IS_ENTITY_ALIVE(LocalData.PedData[iPed].PedID)
				RESET_PED_CLOTHING_COMPONENT_CONFIG(ComponentConfig)
				
				REPEAT NUM_PED_COMPONENTS iComp
					ComponentConfig.ComponentData[iComp].iDrawable = GET_PED_DRAWABLE_VARIATION(LocalData.PedData[iPed].PedID, INT_TO_ENUM(PED_COMPONENT, iComp))
					ComponentConfig.ComponentData[iComp].iTexture = GET_PED_TEXTURE_VARIATION(LocalData.PedData[iPed].PedID, INT_TO_ENUM(PED_COMPONENT, iComp))
					APPLY_PED_CLOTHING_RESTRICTIONS(LocalData.PedData[iPed].PedID, ComponentConfig, INT_TO_ENUM(PED_COMPONENT, iComp), INT_TO_ENUM(PED_MODELS, LocalData.PedData[iPed].iPedModel))
				ENDREPEAT
				
				PACK_PED_CLOTHING_COMPONENT_CONFIG(LocalData.PedData[iPed].iPackedDrawable, LocalData.PedData[iPed].iPackedTexture, ComponentConfig)
			ENDIF
		ENDREPEAT
		
		// Output ped data now the clothing values have been packed and stored
		OUTPUT_PED_DATA(ePedLocation, DebugData, ServerBD, LocalData)
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════╡ DEBUG WIDGETS - DJ SWITCH SCENE ╞═══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Debug: Gets the DJs name.
/// PARAMS:
///    eClubDJ - The DJ to get the name of.
/// RETURNS: The DJs name as a string.
FUNC STRING DEBUG_GET_CLUB_DJ_NAME(CLUB_DJS eClubDJ)
	STRING sDJName = ""
	SWITCH eClubDJ
		CASE CLUB_DJ_SOLOMUN					sDJName = "CLUB_DJ_SOLOMUN"						BREAK
		CASE CLUB_DJ_TALE_OF_US					sDJName = "CLUB_DJ_TALE_OF_US"					BREAK
		CASE CLUB_DJ_DIXON						sDJName = "CLUB_DJ_DIXON"						BREAK
		CASE CLUB_DJ_BLACK_MADONNA				sDJName = "CLUB_DJ_BLACK_MADONNA"				BREAK
		CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB		sDJName = "CLUB_DJ_KEINEMUSIK_NIGHTCLUB"		BREAK
		CASE CLUB_DJ_KEINEMUSIK_BEACH_PARTY		sDJName = "CLUB_DJ_KEINEMUSIK_BEACH_PARTY"		BREAK
		CASE CLUB_DJ_PALMS_TRAX					sDJName = "CLUB_DJ_PALMS_TRAX"					BREAK
		CASE CLUB_DJ_MOODYMANN					sDJName = "CLUB_DJ_MOODYMANN"					BREAK
	ENDSWITCH	
	RETURN sDJName
ENDFUNC

PROC CREATE_PED_DJ_SWITCH_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData)
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP("DJ switch scene")

			ADD_WIDGET_INT_SLIDER("Set From DJ", DebugData.DJSwitchData.iFromDJ, 0, (ENUM_TO_INT(CLUB_DJ_TOTAL)-1), 1)
			DebugData.DJSwitchData.twFromDJ = ADD_TEXT_WIDGET("Set From DJ:")
			
			ADD_WIDGET_INT_SLIDER("Set To DJ", DebugData.DJSwitchData.iToDJ, 0, (ENUM_TO_INT(CLUB_DJ_TOTAL)-1), 1)
			DebugData.DJSwitchData.twToDJ = ADD_TEXT_WIDGET("Set To DJ:")
			
			ADD_WIDGET_BOOL("Trigger DJ switch cutscene", DebugData.DJSwitchData.bTriggerSwitchScene)
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_PED_DJ_SWITCH_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData)
	
	IF DOES_TEXT_WIDGET_EXIST(DebugData.DJSwitchData.twFromDJ)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.DJSwitchData.twFromDJ, DEBUG_GET_CLUB_DJ_NAME(INT_TO_ENUM(CLUB_DJS, DebugData.DJSwitchData.iFromDJ)))
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.DJSwitchData.twFromDJ, "-")
		ENDIF
	ENDIF

	IF DOES_TEXT_WIDGET_EXIST(DebugData.DJSwitchData.twToDJ)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.DJSwitchData.twToDJ, DEBUG_GET_CLUB_DJ_NAME(INT_TO_ENUM(CLUB_DJS, DebugData.DJSwitchData.iToDJ)))
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.DJSwitchData.twToDJ, "-")
		ENDIF
	ENDIF
	
	IF DebugData.DJSwitchData.bTriggerSwitchScene
		IF NOT IS_PEDS_BIT_SET(LocalData.iBS, BS_PED_LOCAL_DATA_TRIGGER_DJ_SWITCH)
			SET_PEDS_BIT(LocalData.iBS, BS_PED_LOCAL_DATA_TRIGGER_DJ_SWITCH)
		ENDIF
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ DEBUG WIDGETS - VFX ╞══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC PED_BONETAG DEBUG_GET_PED_BONE_TAG(INT iBoneID)
	SWITCH iBoneID
		CASE -1	RETURN BONETAG_NULL
		CASE 0	RETURN BONETAG_ROOT
		CASE 1 	RETURN BONETAG_PELVIS
		CASE 2 	RETURN BONETAG_SPINE
		CASE 3 	RETURN BONETAG_SPINE1
		CASE 4 	RETURN BONETAG_SPINE2
		CASE 5 	RETURN BONETAG_SPINE3
		CASE 6 	RETURN BONETAG_NECK
		CASE 7 	RETURN BONETAG_HEAD
		CASE 8 	RETURN BONETAG_R_CLAVICLE
		CASE 9 	RETURN BONETAG_R_UPPERARM
		CASE 10 RETURN BONETAG_R_FOREARM
		CASE 11 RETURN BONETAG_R_HAND
		CASE 12 RETURN BONETAG_R_FINGER0
		CASE 13 RETURN BONETAG_R_FINGER01
		CASE 14 RETURN BONETAG_R_FINGER02
		CASE 15 RETURN BONETAG_R_FINGER1
		CASE 16 RETURN BONETAG_R_FINGER11
		CASE 17 RETURN BONETAG_R_FINGER12
		CASE 18 RETURN BONETAG_R_FINGER2
		CASE 19 RETURN BONETAG_R_FINGER21
		CASE 20 RETURN BONETAG_R_FINGER22
		CASE 21 RETURN BONETAG_R_FINGER3
		CASE 22 RETURN BONETAG_R_FINGER31
		CASE 23 RETURN BONETAG_R_FINGER32
		CASE 24 RETURN BONETAG_R_FINGER4
		CASE 25 RETURN BONETAG_R_FINGER41
		CASE 26 RETURN BONETAG_R_FINGER42
		CASE 27 RETURN BONETAG_L_CLAVICLE
		CASE 28 RETURN BONETAG_L_UPPERARM
		CASE 29 RETURN BONETAG_L_FOREARM
		CASE 30 RETURN BONETAG_L_HAND
		CASE 31 RETURN BONETAG_L_FINGER0
		CASE 32 RETURN BONETAG_L_FINGER01
		CASE 33 RETURN BONETAG_L_FINGER02
		CASE 34 RETURN BONETAG_L_FINGER1
		CASE 35 RETURN BONETAG_L_FINGER11
		CASE 36 RETURN BONETAG_L_FINGER12
		CASE 37 RETURN BONETAG_L_FINGER2
		CASE 38 RETURN BONETAG_L_FINGER21
		CASE 39 RETURN BONETAG_L_FINGER22
		CASE 40 RETURN BONETAG_L_FINGER3
		CASE 41 RETURN BONETAG_L_FINGER31
		CASE 42 RETURN BONETAG_L_FINGER32
		CASE 43 RETURN BONETAG_L_FINGER4
		CASE 44 RETURN BONETAG_L_FINGER41
		CASE 45 RETURN BONETAG_L_FINGER42
		CASE 46 RETURN BONETAG_L_THIGH
		CASE 47 RETURN BONETAG_L_CALF
		CASE 48 RETURN BONETAG_L_FOOT
		CASE 49 RETURN BONETAG_L_TOE
		CASE 50 RETURN BONETAG_R_THIGH
		CASE 51 RETURN BONETAG_R_CALF
		CASE 52 RETURN BONETAG_R_FOOT
		CASE 53 RETURN BONETAG_R_TOE
		CASE 54 RETURN BONETAG_PH_L_HAND
		CASE 55 RETURN BONETAG_PH_R_HAND
	ENDSWITCH
	RETURN BONETAG_NULL
ENDFUNC

PROC CREATE_PED_VFX_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD)
	
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP("VFX")
		
		ADD_WIDGET_INT_SLIDER("Highlighted Local Ped", DebugData.HighlightData.iHighlightedLocalPed, -1, (ServerBD.iMaxLocalPeds-1), 1)
		DebugData.VFXData.twPTXFAssetName = ADD_TEXT_WIDGET("PTFX Asset Name: ")
		DebugData.VFXData.twPTXFEffectName = ADD_TEXT_WIDGET("PTFX Effect Name: ")
		ADD_WIDGET_STRING("")
		ADD_WIDGET_VECTOR_SLIDER("Offset Coords", DebugData.VFXData.vCoordsOffset, -99999.9, 99999.9, 0.01)
		ADD_WIDGET_STRING("")
		ADD_WIDGET_VECTOR_SLIDER("Offset Rotation", DebugData.VFXData.vRotationOffset, -360.0, 360.0, 0.1)
		ADD_WIDGET_STRING("")
		ADD_WIDGET_FLOAT_SLIDER("Scale", DebugData.VFXData.fScale, 0.0, 10.0, 0.01)
		ADD_WIDGET_STRING("")
		ADD_WIDGET_INT_SLIDER("Prop ID", DebugData.VFXData.iPropID, -1, MAX_NUM_PED_PROPS, 1)
		ADD_WIDGET_BOOL("Play on Prop", DebugData.VFXData.bPlayOnProp)
		ADD_WIDGET_STRING("")
		ADD_WIDGET_INT_SLIDER("Ped Bone ID", DebugData.VFXData.iBoneID, -1, 55, 1)
		ADD_WIDGET_BOOL("Play on Ped Bone", DebugData.VFXData.bPlayOnPedBone)
		DebugData.VFXData.twPedBoneName = ADD_TEXT_WIDGET("Ped Bone: ")
		ADD_WIDGET_STRING("")
		ADD_WIDGET_BOOL("Looped VFX", DebugData.VFXData.bLoopedVFX)
		ADD_WIDGET_BOOL("Play VFX", DebugData.VFXData.bPlayVFX)
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_LOCAL_PED_VFX_DEBUG_WIDGETS(PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData)
	
	IF (DebugData.HighlightData.iHighlightedLocalPed = -1)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PedID)
		EXIT
	ENDIF
	
	STRING sPTFXAssetName = ""
	IF DOES_TEXT_WIDGET_EXIST(DebugData.VFXData.twPTXFAssetName)
		sPTFXAssetName = GET_CONTENTS_OF_TEXT_WIDGET(DebugData.VFXData.twPTXFAssetName)
	ENDIF
	
	STRING sPTFXEffectName = ""
	IF DOES_TEXT_WIDGET_EXIST(DebugData.VFXData.twPTXFEffectName)
		sPTFXEffectName = GET_CONTENTS_OF_TEXT_WIDGET(DebugData.VFXData.twPTXFEffectName)
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(DebugData.VFXData.twPedBoneName)
		SET_CONTENTS_OF_TEXT_WIDGET(DebugData.VFXData.twPedBoneName, GET_BONETAG_NAME_FOR_DEBUG(DEBUG_GET_PED_BONE_TAG(DebugData.VFXData.iBoneID)))
	ENDIF
	
	IF (DebugData.VFXData.bPlayVFX)
		
		IF IS_STRING_NULL_OR_EMPTY(sPTFXAssetName)
		OR IS_STRING_NULL_OR_EMPTY(sPTFXEffectName)
		OR ARE_STRINGS_EQUAL(sPTFXAssetName, "New text widget")
		OR ARE_STRINGS_EQUAL(sPTFXEffectName, "New text widget")
			DebugData.VFXData.bPlayVFX = FALSE
			EXIT
		ENDIF
		
		IF (DebugData.VFXData.fCachedScale != DebugData.VFXData.fScale)
		OR NOT ARE_VECTORS_EQUAL(DebugData.VFXData.vCoordsOffset, DebugData.VFXData.vCachedCoordsOffset)
		OR NOT ARE_VECTORS_EQUAL(DebugData.VFXData.vCachedRotationOffset, DebugData.VFXData.vCachedRotationOffset)
			
			IF IS_PEDS_BIT_SET(DebugData.iBS, BS_PED_DEBUG_DATA_PLAYING_PROP_VFX)
				DebugData.VFXData.bPlayOnProp 		= TRUE
			ELIF IS_PEDS_BIT_SET(DebugData.iBS, BS_PED_DEBUG_DATA_PLAYING_PED_BONE_VFX)
				DebugData.VFXData.bPlayOnPedBone 	= TRUE
			ENDIF
			
		ENDIF
			
		IF (DebugData.VFXData.bPlayOnProp)
			
			IF (DebugData.VFXData.iPropID = -1)
				DebugData.VFXData.bPlayVFX 			= FALSE
				DebugData.VFXData.bPlayOnProp 		= FALSE
				EXIT
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PropID[DebugData.VFXData.iPropID])
				DebugData.VFXData.bPlayVFX 			= FALSE
				DebugData.VFXData.bPlayOnProp 		= FALSE
				EXIT
			ENDIF
			
			CLEANUP_PED_VFX(DebugData.VFXData.PTFXEffect)
			USE_PARTICLE_FX_ASSET(sPTFXAssetName)
			
			IF (DebugData.VFXData.bLoopedVFX)
				DebugData.VFXData.PTFXEffect = START_PARTICLE_FX_LOOPED_ON_ENTITY(sPTFXEffectName, LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PropID[DebugData.VFXData.iPropID], DebugData.VFXData.vCoordsOffset, DebugData.VFXData.vRotationOffset, DebugData.VFXData.fScale)
			ELSE
				START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(sPTFXEffectName, LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PropID[DebugData.VFXData.iPropID], DebugData.VFXData.vCoordsOffset, DebugData.VFXData.vRotationOffset, DebugData.VFXData.fScale)
			ENDIF
			
			DebugData.VFXData.vCachedCoordsOffset 	= DebugData.VFXData.vCoordsOffset
			DebugData.VFXData.vCachedRotationOffset	= DebugData.VFXData.vCachedRotationOffset
			DebugData.VFXData.fCachedScale 			= DebugData.VFXData.fScale
			DebugData.VFXData.bPlayOnProp 			= FALSE
			SET_PEDS_BIT(DebugData.iBS, BS_PED_DEBUG_DATA_PLAYING_PROP_VFX)
			
		ELIF (DebugData.VFXData.bPlayOnPedBone)
			
			IF (DebugData.VFXData.iBoneID = -1)
				DebugData.VFXData.bPlayVFX 			= FALSE
				DebugData.VFXData.bPlayOnPedBone 	= FALSE
				EXIT
			ENDIF
			
			CLEANUP_PED_VFX(DebugData.VFXData.PTFXEffect)
			USE_PARTICLE_FX_ASSET(sPTFXAssetName)
			
			IF (DebugData.VFXData.bLoopedVFX)
				DebugData.VFXData.PTFXEffect = START_PARTICLE_FX_LOOPED_ON_PED_BONE(sPTFXEffectName, LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PedID, DebugData.VFXData.vCoordsOffset, DebugData.VFXData.vRotationOffset, DEBUG_GET_PED_BONE_TAG(DebugData.VFXData.iBoneID), DebugData.VFXData.fScale)
			ELSE
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(sPTFXEffectName, LocalData.PedData[DebugData.HighlightData.iHighlightedLocalPed].PedID, DebugData.VFXData.vCoordsOffset, DebugData.VFXData.vRotationOffset, DEBUG_GET_PED_BONE_TAG(DebugData.VFXData.iBoneID), DebugData.VFXData.fScale)
			ENDIF
			
			DebugData.VFXData.vCachedCoordsOffset 	= DebugData.VFXData.vCoordsOffset
			DebugData.VFXData.vCachedRotationOffset	= DebugData.VFXData.vCachedRotationOffset
			DebugData.VFXData.fCachedScale 			= DebugData.VFXData.fScale
			DebugData.VFXData.bPlayOnPedBone 		= FALSE
			SET_PEDS_BIT(DebugData.iBS, BS_PED_DEBUG_DATA_PLAYING_PED_BONE_VFX)
			
		ENDIF
	ELSE
		CLEANUP_PED_VFX(DebugData.VFXData.PTFXEffect)
		
		IF IS_PEDS_BIT_SET(DebugData.iBS, BS_PED_DEBUG_DATA_PLAYING_PROP_VFX)
			CLEAR_PEDS_BIT(DebugData.iBS, BS_PED_DEBUG_DATA_PLAYING_PROP_VFX)
		ENDIF
		IF IS_PEDS_BIT_SET(DebugData.iBS, BS_PED_DEBUG_DATA_PLAYING_PED_BONE_VFX)
			CLEAR_PEDS_BIT(DebugData.iBS, BS_PED_DEBUG_DATA_PLAYING_PED_BONE_VFX)
		ENDIF
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ DEBUG WIDGETS - PERFORMANCE ╞══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_PED_PERFORMANCE_DEBUG_WIDGETS(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData)
	
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP("Performance")
		
		ADD_WIDGET_STRING("Do NOT tick Enable Widget at the top when running Performance debug widgets.")
		ADD_WIDGET_STRING("This will enable debug functionality and inaccurately depict the update time and performance of the script.")
		ADD_WIDGET_STRING("")
		DebugData.PerformanceData.twWarningName = ADD_TEXT_WIDGET("Note: ")
		ADD_WIDGET_BOOL("Relaunch Ped Script", PedDebugScriptManagementData.bRelaunchPedScript)
		
		IF (ePedLocation = PED_LOCATION_DEBUG)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Debug arrows are created at the corners of the grid.")
			ADD_WIDGET_VECTOR_SLIDER("Coords", DebugData.PerformanceData.vCoords, -99999.9, 99999.9, 0.01)
			ADD_WIDGET_BOOL("Use Player Coords", DebugData.PerformanceData.bUsePlayerCoords)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_FLOAT_SLIDER("Heading", DebugData.PerformanceData.fHeading, -360.0, 360.0, 0.1)
			ADD_WIDGET_BOOL("Use Player Heading", DebugData.PerformanceData.bUsePlayerHeading)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Grid Rows", DebugData.PerformanceData.iGridRows, 0, 20, 1)
			ADD_WIDGET_INT_SLIDER("Grid Columns", DebugData.PerformanceData.iGridColumns, 0, 20, 1)
			ADD_WIDGET_INT_READ_ONLY("Total Peds:", DebugData.PerformanceData.iTotalPeds)
			DebugData.PerformanceData.twTotalPedsName = ADD_TEXT_WIDGET("Note: ")
			ADD_WIDGET_STRING("")
			ADD_WIDGET_FLOAT_SLIDER("Row Offset", DebugData.PerformanceData.fRowOffset, -5.0, 5.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Column Offset", DebugData.PerformanceData.fColumnOffset, -5.0, 5.0, 0.01)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Unique Peds", DebugData.PerformanceData.iUniquePeds, 0, 20, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Create Peds", DebugData.PerformanceData.bCreatePeds)
			ADD_WIDGET_BOOL("Delete Peds", DebugData.PerformanceData.bDeletePeds)
			ADD_WIDGET_STRING("")
			START_WIDGET_GROUP("Animations")
				ADD_WIDGET_BOOL("Play Task Anims", DebugData.PerformanceData.bPlayTaskAnims)
				ADD_WIDGET_BOOL("Play Task Anims With Props", DebugData.PerformanceData.bPlayPropTaskAnims)
				ADD_WIDGET_STRING("Note: There is a code limit of 50 sync scenes. The rest will be Task Anims")
				ADD_WIDGET_BOOL("Play Sync Scenes", DebugData.PerformanceData.bPlaySyncScenes)
				ADD_WIDGET_BOOL("Play Sync Scenes With Props", DebugData.PerformanceData.bPlayPropSyncScenes)
				ADD_WIDGET_BOOL("Random", DebugData.PerformanceData.bPlayRandomAnimTypes)
				ADD_WIDGET_BOOL("Remove Anims", DebugData.PerformanceData.bRemoveAnims)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Head Tracking")
				ADD_WIDGET_STRING("Note: There are 10 max head tracking peds. They will track the player.")
				ADD_WIDGET_BOOL("Enable Head Tracking Peds", DebugData.PerformanceData.bEnableHeadTrackingPeds)
				ADD_WIDGET_BOOL("Disable Head Tracking Peds", DebugData.PerformanceData.bDisableHeadTrackingPeds)
			STOP_WIDGET_GROUP()
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Terminate Script", PedDebugScriptManagementData.bTerminatePedScript)
		ENDIF
		
	STOP_WIDGET_GROUP()
	
ENDPROC

FUNC INT DEBUG_CALCULATE_PERFORMANCE_TOTAL_PEDS(PED_DEBUG_DATA &DebugData, BOOL &bOverPedLimit)
	
	IF (DebugData.PerformanceData.iGridRows = 0 OR DebugData.PerformanceData.iGridColumns = 0)
		RETURN 0
	ENDIF
	
	INT iPeds = (DebugData.PerformanceData.iGridRows*DebugData.PerformanceData.iGridColumns)
	IF (iPeds > 100)
		iPeds = 100
		bOverPedLimit = TRUE
	ENDIF
	
	RETURN iPeds
ENDFUNC

FUNC VECTOR DEBUG_GET_PERFORMANCE_PED_COORDS(PED_DEBUG_DATA &DebugData, INT iRow, INT iColumn)
	VECTOR vPedOffset = <<DebugData.PerformanceData.fRowOffset*TO_FLOAT(iRow), DebugData.PerformanceData.fColumnOffset*TO_FLOAT(iColumn), 0.0>>
	RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DebugData.PerformanceData.vCoords, DebugData.PerformanceData.fHeading, vPedOffset)
ENDFUNC

PROC DEBUG_SET_PERFORMANCE_PED_ANIMATION_DATA(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData, PED_ACTIVITIES eMalePedActivity, PED_ACTIVITIES eFemalePedActivity, BOOL bCreatePedProps = FALSE, PED_ACTIVITIES eSecondaryMalePedActivity = PED_ACT_INVALID, PED_ACTIVITIES eSecondaryFemalePedActivity = PED_ACT_INVALID, BOOL bPlaySyncScenes = FALSE)
	
	IF (DebugData.PerformanceData.iGridRows = 0 OR DebugData.PerformanceData.iGridColumns = 0)
		EXIT
	ENDIF
	
	INT iPed 	= 0
	INT iRow	= 0
	INT iColumn	= 0
	
	REPEAT DebugData.PerformanceData.iCachedGridRows iRow
		REPEAT DebugData.PerformanceData.iCachedGridColumns iColumn
			IF IS_ENTITY_ALIVE(LocalData.PedData[iPed].PedID)
				
				// Animation origins are at ped waist height
				IF IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_NO_ANIMATION)
					LocalData.PedData[iPed].vPosition.z += 1.0
				ENDIF
				
				CLEAR_PED_TASKS(LocalData.PedData[iPed].PedID)
				DELETE_LOCAL_SCRIPT_PED_PROPS(LocalData.PedData[iPed])
				CLEAR_PEDS_BIT(LocalData.PedData[iPed].iBS, BS_PED_DATA_NO_ANIMATION)
				LocalData.PedData[iPed].animData.iTaskAnimStartTimeMS = -1
				
				LocalData.PedData[iPed].iActivity = ENUM_TO_INT(eMalePedActivity)
				IF (LocalData.PedData[iPed].iPedModel = ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04))
					LocalData.PedData[iPed].iActivity = ENUM_TO_INT(eFemalePedActivity)
				ENDIF
				
				IF (bPlaySyncScenes)
					STOP_PED_SYNCED_SCENE(LocalData.PedData[iPed])
					
					// Code limit
					IF (iPed >= MAX_NUM_ANIM_SYNC_SCENES-20)
						LocalData.PedData[iPed].iActivity = ENUM_TO_INT(eSecondaryMalePedActivity)
						IF (LocalData.PedData[iPed].iPedModel = ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04))
							LocalData.PedData[iPed].iActivity = ENUM_TO_INT(eSecondaryFemalePedActivity)
						ENDIF
					ENDIF
				ENDIF
				
				IF (bCreatePedProps)
					CREATE_LOCAL_SCRIPT_PED_PROPS(ePedLocation, LocalData.PedData[iPed], iPed)
				ENDIF
			ENDIF
			iPed++
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

FUNC PED_ACTIVITIES DEBUG_GET_PERFORMANCE_PED_ACTIVITY(INT iPedActivity)
	PED_ACTIVITIES ePedActivity = PED_ACT_AMBIENT_F_01
	SWITCH iPedActivity
		CASE 0	ePedActivity = PED_ACT_AMBIENT_F_01							BREAK
		CASE 1	ePedActivity = PED_ACT_BEACH_PARTY_STAND_M_01				BREAK
		CASE 2	ePedActivity = PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01		BREAK
		CASE 3	ePedActivity = PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01	BREAK
		CASE 4	ePedActivity = PED_ACT_BEACH_PARTY_STAND_M_02				BREAK
		CASE 5	ePedActivity = PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01		BREAK
		CASE 6	ePedActivity = PED_ACT_MUSCLE_FLEX							BREAK
		CASE 7	ePedActivity = PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01	BREAK
	ENDSWITCH
	RETURN ePedActivity
ENDFUNC

PROC DEBUG_CLEAR_PERFORMANCE_PED_ANIMATION_DATA(PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData)
	
	IF (DebugData.PerformanceData.iGridRows = 0 OR DebugData.PerformanceData.iGridColumns = 0)
		EXIT
	ENDIF
	
	INT iPed 	= 0
	INT iRow	= 0
	INT iColumn	= 0
	
	REPEAT DebugData.PerformanceData.iCachedGridRows iRow
		REPEAT DebugData.PerformanceData.iCachedGridColumns iColumn
			IF IS_ENTITY_ALIVE(LocalData.PedData[iPed].PedID)
				CLEAR_PED_TASKS(LocalData.PedData[iPed].PedID)
				DELETE_LOCAL_SCRIPT_PED_PROPS(LocalData.PedData[iPed])
				SET_PEDS_BIT(LocalData.PedData[iPed].iBS, BS_PED_DATA_NO_ANIMATION)
			ENDIF
			iPed++
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

PROC DEBUG_SET_PERFORMANCE_PED_ANIMATION_DATA_RANDOM(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData)
	
	IF (DebugData.PerformanceData.iGridRows = 0 OR DebugData.PerformanceData.iGridColumns = 0)
		EXIT
	ENDIF
	
	INT iPed 				= 0
	INT iRow				= 0
	INT iColumn				= 0
	INT iSyncScenes			= 0
	INT iRandPedActivity 	= 0
	
	REPEAT DebugData.PerformanceData.iCachedGridRows iRow
		REPEAT DebugData.PerformanceData.iCachedGridColumns iColumn
			IF IS_ENTITY_ALIVE(LocalData.PedData[iPed].PedID)
				
				// Animation origins are at ped waist height
				IF IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_NO_ANIMATION)
					LocalData.PedData[iPed].vPosition.z += 1.0
				ENDIF
				
				CLEAR_PED_TASKS(LocalData.PedData[iPed].PedID)
				DELETE_LOCAL_SCRIPT_PED_PROPS(LocalData.PedData[iPed])
				CLEAR_PEDS_BIT(LocalData.PedData[iPed].iBS, BS_PED_DATA_NO_ANIMATION)
				LocalData.PedData[iPed].animData.iTaskAnimStartTimeMS = -1
				
				iRandPedActivity = GET_RANDOM_INT_IN_RANGE(0, 8)
				LocalData.PedData[iPed].iActivity = ENUM_TO_INT(DEBUG_GET_PERFORMANCE_PED_ACTIVITY(iRandPedActivity))
				
				IF (LocalData.PedData[iPed].iActivity = ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_M_02))
				OR (LocalData.PedData[iPed].iActivity = ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01))
				OR (LocalData.PedData[iPed].iActivity = ENUM_TO_INT(PED_ACT_MUSCLE_FLEX))
				OR (LocalData.PedData[iPed].iActivity = ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01))
					STOP_PED_SYNCED_SCENE(LocalData.PedData[iPed])
					iSyncScenes++
					
					IF (iSyncScenes >= MAX_NUM_ANIM_SYNC_SCENES-20)
						iRandPedActivity = GET_RANDOM_INT_IN_RANGE(0, 4)
						LocalData.PedData[iPed].iActivity = ENUM_TO_INT(DEBUG_GET_PERFORMANCE_PED_ACTIVITY(iRandPedActivity))
					ENDIF
				ENDIF
				
				IF (LocalData.PedData[iPed].iActivity = ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01))
				OR (LocalData.PedData[iPed].iActivity = ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01))
				OR (LocalData.PedData[iPed].iActivity = ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01))
				OR (LocalData.PedData[iPed].iActivity = ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01))
					CREATE_LOCAL_SCRIPT_PED_PROPS(ePedLocation, LocalData.PedData[iPed], iPed)
				ENDIF
			ENDIF
			iPed++
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

PROC DEBUG_SET_PERFORMANCE_PED_HEAD_TRACKING_DATA(PED_DEBUG_DATA &DebugData, SCRIPT_PED_DATA &LocalData, BOOL bEnableHeadTracking = TRUE)
	
	IF (DebugData.PerformanceData.iGridRows = 0 OR DebugData.PerformanceData.iGridColumns = 0)
		EXIT
	ENDIF
	
	INT iPed 				= 0
	INT iRow				= 0
	INT iColumn				= 0
	INT iHeadTrackingPeds 	= 0
	
	REPEAT DebugData.PerformanceData.iCachedGridRows iRow
		REPEAT DebugData.PerformanceData.iCachedGridColumns iColumn
			IF IS_ENTITY_ALIVE(LocalData.PedData[iPed].PedID)
				IF (iHeadTrackingPeds < MAX_NUM_LOCAL_HEAD_TRACKING_PEDS)
					RESET_PED_HEAD_TRACKING_DATA(LocalData.HeadTrackingData[iHeadTrackingPeds])
					IF (bEnableHeadTracking)
						SET_PEDS_BIT(LocalData.PedData[iPed].iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
						LocalData.HeadTrackingData[iHeadTrackingPeds].iPedID						= iPed
						LocalData.HeadTrackingData[iHeadTrackingPeds].iHeadTrackingTime				= -1
						LocalData.HeadTrackingData[iHeadTrackingPeds].fHeadTrackingRange			= 7.0000
						LocalData.HeadTrackingData[iHeadTrackingPeds].vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
						LocalData.HeadTrackingData[iHeadTrackingPeds].eHeadTrackingLookFlag 		= SLF_DEFAULT
						LocalData.HeadTrackingData[iHeadTrackingPeds].eHeadTrackingLookPriority 	= SLF_LOOKAT_HIGH
						LocalData.iHeadTrackingPedID[iPed]											= iHeadTrackingPeds
					ELSE
						CLEAR_PEDS_BIT(LocalData.PedData[iPed].iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
						LocalData.iHeadTrackingPedID[iPed]											= -1
						IF IS_PED_HEADTRACKING_ENTITY(LocalData.PedData[iPed].PedID, PLAYER_PED_ID())
							TASK_CLEAR_LOOK_AT(LocalData.PedData[iPed].PedID)
						ENDIF
					ENDIF
				ELSE
					BREAKLOOP
				ENDIF
				iHeadTrackingPeds++
			ENDIF
			iPed++
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

FUNC PED_MODELS DEBUG_GET_PED_PERFORMANCE_MODEL(PED_DEBUG_DATA &DebugData, INT iPedID, BOOL bFemalePed = FALSE)
	
	PED_MODELS ePedModel = PED_MODEL_HIPSTER_MALE_04
	IF (bFemalePed)
		ePedModel = PED_MODEL_HIPSTER_FEMALE_04
	ENDIF
	
	IF (iPedID >= DebugData.PerformanceData.iUniquePeds)
		RETURN ePedModel
	ENDIF
	
	SWITCH iPedID
		CASE 0	ePedModel = PED_MODEL_HAO				BREAK
		CASE 1	ePedModel = PED_MODEL_DAVE				BREAK
		CASE 2	ePedModel = PED_MODEL_SCOTT				BREAK
		CASE 3	ePedModel = PED_MODEL_ELRUBIO			BREAK
		CASE 4	ePedModel = PED_MODEL_KEINEMUSIK_RAMPA	BREAK
		CASE 5	ePedModel = PED_MODEL_KEINEMUSIK_ME		BREAK
		CASE 6	ePedModel = PED_MODEL_KEINEMUSIK_ADAM	BREAK
		CASE 7	ePedModel = PED_MODEL_PALMS_TRAX		BREAK
		CASE 8	ePedModel = PED_MODEL_MOODYMANN			BREAK
		CASE 9	ePedModel = PED_MODEL_MOODYMANN_DANC_01	BREAK
		CASE 10	ePedModel = PED_MODEL_MOODYMANN_DANC_02	BREAK
		CASE 11	ePedModel = PED_MODEL_MOODYMANN_DANC_03	BREAK
		CASE 12	ePedModel = PED_MODEL_JACKIE			BREAK
		CASE 13	ePedModel = PED_MODEL_MIGUEL			BREAK
		CASE 14	ePedModel = PED_MODEL_PATRICIA			BREAK
		CASE 15	ePedModel = PED_MODEL_KAYLEE			BREAK
		CASE 16	ePedModel = PED_MODEL_BODYGUARD			BREAK
		CASE 17	ePedModel = PED_MODEL_PAVEL				BREAK
		CASE 18	ePedModel = PED_MODEL_RECEPTIONIST		BREAK
		CASE 19	ePedModel = PED_MODEL_HIGH_SECURITY_01	BREAK
	ENDSWITCH
	
	RETURN ePedModel
ENDFUNC

PROC DEBUG_SET_PED_PERFORMANCE_CLOTHING_VARIATIONS(PEDS_DATA &Data, PED_MODELS ePedModel)
	SWITCH ePedModel
		CASE PED_MODEL_KEINEMUSIK_RAMPA
			Data.iPackedDrawable[0]						= 16777216
			Data.iPackedDrawable[1]						= 16777473
			Data.iPackedDrawable[2]						= 2
			Data.iPackedTexture[0] 						= 0
			Data.iPackedTexture[1] 						= 0
			Data.iPackedTexture[2] 						= 0
		BREAK
		CASE PED_MODEL_KEINEMUSIK_ME
			Data.iPackedDrawable[0]						= 16777216
			Data.iPackedDrawable[1]						= 256
			Data.iPackedDrawable[2]						= 16842752
			Data.iPackedTexture[0] 						= 0
			Data.iPackedTexture[1] 						= 0
			Data.iPackedTexture[2] 						= 0
		BREAK
		CASE PED_MODEL_KEINEMUSIK_ADAM
			Data.iPackedDrawable[0]						= 16777216
			Data.iPackedDrawable[1]						= 65792
			Data.iPackedDrawable[2]						= 16777217
			Data.iPackedTexture[0] 						= 0
			Data.iPackedTexture[1] 						= 0
			Data.iPackedTexture[2] 						= 0
		BREAK
		CASE PED_MODEL_DAVE
			Data.iPackedDrawable[0]						= 0
			Data.iPackedDrawable[1]						= 16842753
			Data.iPackedDrawable[2]						= 65536
			Data.iPackedTexture[0] 						= 0
			Data.iPackedTexture[1] 						= 0
			Data.iPackedTexture[2] 						= 0
		BREAK
		CASE PED_MODEL_ELRUBIO
			Data.iPackedDrawable[0]						= 65536
			Data.iPackedDrawable[1]						= 0
			Data.iPackedDrawable[2]						= 65536
			Data.iPackedTexture[0] 						= 0
			Data.iPackedTexture[1] 						= 0
			Data.iPackedTexture[2] 						= 0
		BREAK
		CASE PED_MODEL_PALMS_TRAX
		CASE PED_MODEL_MOODYMANN
		CASE PED_MODEL_MOODYMANN_DANC_01
		CASE PED_MODEL_MOODYMANN_DANC_02
		CASE PED_MODEL_MOODYMANN_DANC_03
		CASE PED_MODEL_RECEPTIONIST
		CASE PED_MODEL_HIGH_SECURITY_01
		CASE PED_MODEL_SCOTT
			Data.iPackedDrawable[0]						= 0
			Data.iPackedDrawable[1]						= 0
			Data.iPackedDrawable[2]						= 0
			Data.iPackedTexture[0] 						= 0
			Data.iPackedTexture[1] 						= 0
			Data.iPackedTexture[2] 						= 0
		BREAK
		DEFAULT
			Data.iPackedDrawable[0]						= -1
			Data.iPackedDrawable[1]						= -1
			Data.iPackedDrawable[2]						= -1
			Data.iPackedTexture[0] 						= -1
			Data.iPackedTexture[1] 						= -1
			Data.iPackedTexture[2] 						= -1
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_PED_PERFORMANCE_DEBUG_WIDGETS(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	// Used when relaunching script if not in debug ped location and M menu event sent
	IF (ePedLocation = PED_LOCATION_DEBUG AND PedDebugScriptManagementData.iMmenuMaxLocalPeds != -2)
		PROCESS_PED_DEBUG_EVENT_MAX_LOCAL_PEDS(DebugData, ServerBD, LocalData, PedDebugScriptManagementData.iMmenuMaxLocalPeds)
		PedDebugScriptManagementData.iMmenuMaxLocalPeds = -2
	ENDIF
	
	BOOL bOverPedLimit = FALSE
	DebugData.PerformanceData.iTotalPeds = DEBUG_CALCULATE_PERFORMANCE_TOTAL_PEDS(DebugData, bOverPedLimit)
	
	IF DOES_TEXT_WIDGET_EXIST(DebugData.PerformanceData.twWarningName)
		IF (ePedLocation = PED_LOCATION_DEBUG)
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.PerformanceData.twWarningName, "You are running the debug instance of the ped script.")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.PerformanceData.twWarningName, "WARNING: Not running debug instance of ped script. Relaunch.")
		ENDIF
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(DebugData.PerformanceData.twTotalPedsName)
		IF (bOverPedLimit)
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.PerformanceData.twTotalPedsName, "Grid Size Over Limit. Reduce Size. Max Peds: 100")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(DebugData.PerformanceData.twTotalPedsName, "Grid Size Under Limit.")
		ENDIF
	ENDIF
	
	IF (DebugData.PerformanceData.bUsePlayerCoords)
		DebugData.PerformanceData.bUsePlayerCoords = FALSE
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			DebugData.PerformanceData.vCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			DebugData.PerformanceData.vCoords.z -= 1.0
		ENDIF
	ENDIF
	
	IF (DebugData.PerformanceData.bUsePlayerHeading)
		DebugData.PerformanceData.bUsePlayerHeading = FALSE
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			DebugData.PerformanceData.fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	// Valid Grid 
	IF (DebugData.PerformanceData.iGridRows > 0 AND DebugData.PerformanceData.iGridColumns > 0)
		
		// Draw debug arrows at corners of grid to help with placement
		// Corner 4 - Back Right
		DRAW_DEBUG_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DebugData.PerformanceData.vCoords, DebugData.PerformanceData.fHeading, <<DebugData.PerformanceData.fRowOffset*TO_FLOAT(DebugData.PerformanceData.iGridRows-1), DebugData.PerformanceData.fColumnOffset*TO_FLOAT(DebugData.PerformanceData.iGridColumns-1), 0.0>>), DebugData.PerformanceData.fHeading, HUD_COLOUR_PINK, 0.0, 0.5)
		// Corner 3 - Back Left
		DRAW_DEBUG_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DebugData.PerformanceData.vCoords, DebugData.PerformanceData.fHeading, <<0.0, DebugData.PerformanceData.fColumnOffset*TO_FLOAT(DebugData.PerformanceData.iGridColumns-1), 0.0>>), DebugData.PerformanceData.fHeading, HUD_COLOUR_PINK, 0.0, 0.5)
		// Corner 2 - Front Right
		DRAW_DEBUG_SPAWN_POINT(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(DebugData.PerformanceData.vCoords, DebugData.PerformanceData.fHeading, <<DebugData.PerformanceData.fRowOffset*TO_FLOAT(DebugData.PerformanceData.iGridRows-1), 0.0, 0.0>>), DebugData.PerformanceData.fHeading, HUD_COLOUR_PINK, 0.0, 0.5)
		// Corner 1 - Front Left
		DRAW_DEBUG_SPAWN_POINT(DebugData.PerformanceData.vCoords, DebugData.PerformanceData.fHeading, HUD_COLOUR_GREENDARK, 0.0, 0.5)
		
	ENDIF
	
	IF (DebugData.PerformanceData.bCreatePeds)
		DebugData.PerformanceData.bCreatePeds 							= FALSE
		
		IF (DebugData.PerformanceData.iGridRows = 0 OR DebugData.PerformanceData.iGridColumns = 0)
			EXIT
		ENDIF
			
		DebugData.PerformanceData.bRunningDebugPedLocation				= TRUE
		DEBUG_DELETE_LOCAL_SCRIPT_PEDS(LocalData.PedData, DebugData.ServerData.iMaxLocalPeds)
		
		DebugData.PerformanceData.vCachedCoords							= DebugData.PerformanceData.vCoords
		DebugData.PerformanceData.fCachedHeading						= DebugData.PerformanceData.fHeading
		
		DebugData.ServerData.iMaxLocalPeds								= DebugData.PerformanceData.iTotalPeds
		ServerBD.iMaxLocalPeds 											= DebugData.PerformanceData.iTotalPeds
		
		INT iPed 		= 0
		INT iRow		= 0
		INT iColumn		= 0
		INT iPedGender	= 0
		
		// Validate rows/columns
		IF (bOverPedLimit)
			FOR iColumn = DebugData.PerformanceData.iGridColumns TO 0 STEP -1
				IF (iColumn*DebugData.PerformanceData.iGridRows <= 100)
					DebugData.PerformanceData.iGridColumns = iColumn
					BREAKLOOP
				ENDIF
			ENDFOR
			iColumn = 0
		ENDIF
		
		DebugData.PerformanceData.iCachedGridRows						= DebugData.PerformanceData.iGridRows
		DebugData.PerformanceData.iCachedGridColumns					= DebugData.PerformanceData.iGridColumns
		
		REPEAT DebugData.PerformanceData.iGridRows iRow
			REPEAT DebugData.PerformanceData.iGridColumns iColumn
				LocalData.PedData[iPed].cullingData.ePedArea 			= PED_AREA_PERMANENT
				LocalData.PedData[iPed].iPedModel 						= ENUM_TO_INT(DEBUG_GET_PED_PERFORMANCE_MODEL(DebugData, iPed))
				
				iPedGender = GET_RANDOM_INT_IN_RANGE(0, 2)
				IF (iPedGender = 1)
					LocalData.PedData[iPed].iPedModel 					= ENUM_TO_INT(DEBUG_GET_PED_PERFORMANCE_MODEL(DebugData, iPed, TRUE))
				ENDIF
				
				LocalData.PedData[iPed].iActivity 						= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_M_01)
				LocalData.PedData[iPed].iLevel 							= 0
				
				DEBUG_SET_PED_PERFORMANCE_CLOTHING_VARIATIONS(LocalData.PedData[iPed], INT_TO_ENUM(PED_MODELS, LocalData.PedData[iPed].iPedModel))
				
				LocalData.PedData[iPed].vPosition 						= DEBUG_GET_PERFORMANCE_PED_COORDS(DebugData, iRow, iColumn)
				LocalData.PedData[iPed].vRotation 						= <<0.0, 0.0, DebugData.PerformanceData.fHeading>>
				
				CLEAR_PEDS_BITSET(LocalData.PedData[iPed].iBS)
				SET_PEDS_BIT(LocalData.PedData[iPed].iBS, BS_PED_DATA_NO_ANIMATION)
				
				iPed++
			ENDREPEAT
		ENDREPEAT
	ENDIF
	
	IF (DebugData.PerformanceData.bDeletePeds)
		DebugData.PerformanceData.bDeletePeds = FALSE
		PROCESS_PED_DEBUG_EVENT_MAX_LOCAL_PEDS(DebugData, ServerBD, LocalData, -1)
	ENDIF
	
	// Update ped coords/heading
	IF NOT ARE_VECTORS_EQUAL(DebugData.PerformanceData.vCachedCoords, DebugData.PerformanceData.vCoords, TRUE)
	OR (DebugData.PerformanceData.fCachedHeading != DebugData.PerformanceData.fHeading)
		
		DebugData.PerformanceData.vCachedCoords 	= DebugData.PerformanceData.vCoords
		DebugData.PerformanceData.fCachedHeading 	= DebugData.PerformanceData.fHeading
		
		IF (ServerBD.iMaxLocalPeds > 0)
			
			INT iPed 	= 0
			INT iRow	= 0
			INT iColumn	= 0
			
			REPEAT DebugData.PerformanceData.iCachedGridRows iRow
				REPEAT DebugData.PerformanceData.iCachedGridColumns iColumn
					IF IS_ENTITY_ALIVE(LocalData.PedData[iPed].PedID)
						
						LocalData.PedData[iPed].vPosition = DEBUG_GET_PERFORMANCE_PED_COORDS(DebugData, iRow, iColumn)
						LocalData.PedData[iPed].vRotation = <<0.0, 0.0, DebugData.PerformanceData.fHeading>>
						
						// Animation origins are at ped waist height
						IF NOT IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_NO_ANIMATION)
							LocalData.PedData[iPed].vPosition.z += 1.0
						ENDIF
						
						SET_ENTITY_COORDS(LocalData.PedData[iPed].PedID, LocalData.PedData[iPed].vPosition)
						SET_ENTITY_ROTATION(LocalData.PedData[iPed].PedID, LocalData.PedData[iPed].vRotation)
						
					ENDIF
					iPed++
				ENDREPEAT
			ENDREPEAT
			
		ENDIF
	ENDIF
	
	IF (DebugData.PerformanceData.bPlayTaskAnims)
		DebugData.PerformanceData.bPlayTaskAnims = FALSE
		DEBUG_SET_PERFORMANCE_PED_ANIMATION_DATA(ePedLocation, DebugData, LocalData, PED_ACT_BEACH_PARTY_STAND_M_01, PED_ACT_AMBIENT_F_01)
	ENDIF
	
	IF (DebugData.PerformanceData.bPlayPropTaskAnims)
		DebugData.PerformanceData.bPlayPropTaskAnims = FALSE
		DEBUG_SET_PERFORMANCE_PED_ANIMATION_DATA(ePedLocation, DebugData, LocalData, PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01, PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01, TRUE)
	ENDIF
	
	IF (DebugData.PerformanceData.bPlaySyncScenes)
		DebugData.PerformanceData.bPlaySyncScenes = FALSE
		DEBUG_SET_PERFORMANCE_PED_ANIMATION_DATA(ePedLocation, DebugData, LocalData, PED_ACT_BEACH_PARTY_STAND_M_02, PED_ACT_MUSCLE_FLEX, DEFAULT, PED_ACT_BEACH_PARTY_STAND_M_01, PED_ACT_AMBIENT_F_01, TRUE)
	ENDIF
	
	IF (DebugData.PerformanceData.bPlayPropSyncScenes)
		DebugData.PerformanceData.bPlayPropSyncScenes = FALSE
		DEBUG_SET_PERFORMANCE_PED_ANIMATION_DATA(ePedLocation, DebugData, LocalData, PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01, PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01, TRUE, PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01, PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01, TRUE)
	ENDIF
	
	IF (DebugData.PerformanceData.bPlayRandomAnimTypes)
		DebugData.PerformanceData.bPlayRandomAnimTypes = FALSE
		DEBUG_SET_PERFORMANCE_PED_ANIMATION_DATA_RANDOM(ePedLocation, DebugData, LocalData)
	ENDIF
	
	IF (DebugData.PerformanceData.bRemoveAnims)
		DebugData.PerformanceData.bRemoveAnims = FALSE
		DEBUG_CLEAR_PERFORMANCE_PED_ANIMATION_DATA(DebugData, LocalData)
	ENDIF
	
	IF (DebugData.PerformanceData.bEnableHeadTrackingPeds)
		DebugData.PerformanceData.bEnableHeadTrackingPeds = FALSE
		DEBUG_SET_PERFORMANCE_PED_HEAD_TRACKING_DATA(DebugData, LocalData)
	ENDIF
	
	IF (DebugData.PerformanceData.bDisableHeadTrackingPeds)
		DebugData.PerformanceData.bDisableHeadTrackingPeds = FALSE
		DEBUG_SET_PERFORMANCE_PED_HEAD_TRACKING_DATA(DebugData, LocalData, FALSE)
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ DEBUG WIDGETS - FADING PEDS ╞══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_PED_FADE_DEBUG_WIDGETS(SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP("Ped Fading")
		
		INT iPeds
		TEXT_LABEL_63 tlLocalPed = ""
		REPEAT ServerBD.iMaxLocalPeds iPeds
			tlLocalPed = "Local Ped: "
			tlLocalPed += iPeds
			START_WIDGET_GROUP(tlLocalPed)
				ADD_WIDGET_BOOL("Fade Ped Out", LocalData.PedData[iPeds].PedDebugData.bFadePedOut)
			STOP_WIDGET_GROUP()
			tlLocalPed = ""
		ENDREPEAT
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_LOCAL_PED_FADE_DEBUG_WIDGETS(SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iPeds
	REPEAT ServerBD.iMaxLocalPeds iPeds
		IF (LocalData.PedData[iPeds].PedDebugData.bFadePedOut)
			LocalData.PedData[iPeds].PedDebugData.bFadePedOut = FALSE
			SET_PEDS_BIT(LocalData.PedData[iPeds].iBS, BS_PED_DATA_FADE_PED)
		ENDIF
	ENDREPEAT
		
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ DEBUG WIDGETS - PED LOOK	  ╞══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_PED_LOOK_WIDGETS()
	
	ADD_WIDGET_STRING("")
	START_WIDGET_GROUP("Ped Look")		
		ADD_WIDGET_INT_SLIDER("g_iVIPOutfit ", g_iVIPOutfit, -1, 5, 1)		
		ADD_WIDGET_INT_SLIDER("g_iNightClubClientele ", g_iNightClubClientele, 0, 3, 1)	
	STOP_WIDGET_GROUP()
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ M MENU ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC UPDATE_PED_M_MENU_DEBUG(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	// Refresh peds as max local peds has changed
	IF (DebugData.ServerData.iMaxLocalPeds != ServerBD.iMaxLocalPeds)
		DEBUG_DELETE_LOCAL_SCRIPT_PEDS(LocalData.PedData, DebugData.ServerData.iMaxLocalPeds)
		UPDATE_PED_DEBUG_SERVER_BROADCAST_DATA(ePedLocation, DebugData, ServerBD, LocalData)
		DebugData.ServerData.iMaxLocalPeds = ServerBD.iMaxLocalPeds
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ FREEMODE WIDGETS ╞═══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_PED_SCRIPT_MANAGEMENT_WIDGETS(PED_DEBUG_SCRIPT_MANAGEMENT_DATA &ScriptManagementData)
	
	START_WIDGET_GROUP("Interior Ped System")
		ADD_WIDGET_BOOL("Launch Ped Script", ScriptManagementData.bLaunchPedScript)
		ADD_WIDGET_BOOL("Terminate Ped Script", ScriptManagementData.bTerminatePedScript)
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_PED_SCRIPT_MANAGEMENT_WIDGETS(PED_DEBUG_SCRIPT_MANAGEMENT_DATA &ScriptManagementData)
	
	IF (ScriptManagementData.bLaunchPedScript)
		MAINTAIN_PED_SCRIPT_LAUNCHING(PED_LOCATION_DEBUG, ScriptManagementData.bPedScriptRunning, PED_SCRIPT_INSTANCE_DEBUG, FALSE)
		IF (ScriptManagementData.bPedScriptRunning)
			ScriptManagementData.bLaunchPedScript = FALSE
		ENDIF
	ENDIF
	
	IF (ScriptManagementData.bTerminatePedScript)
		ScriptManagementData.bTerminatePedScript = FALSE
		ScriptManagementData.bPedScriptRunning = FALSE
		g_bTurnOnPeds = FALSE
	ENDIF
	
	IF (ScriptManagementData.bRelaunchPedScript)
		SWITCH ScriptManagementData.iRelaunchScriptFlow
			CASE 0
				ScriptManagementData.bTerminatePedScript = TRUE
				ScriptManagementData.iRelaunchScriptFlow++
			BREAK
			CASE 1
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("AM_MP_PEDS")) < 1
					ScriptManagementData.iRelaunchScriptFlow = 0
					ScriptManagementData.bLaunchPedScript = TRUE
					ScriptManagementData.bRelaunchPedScript	= FALSE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ UPDATE WIDGETS ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC UPDATE_PED_DEBUG_WIDGETS(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	UPDATE_PED_M_MENU_DEBUG(ePedLocation, DebugData, ServerBD, LocalData)
	UPDATE_PED_DJ_SWITCH_DEBUG_WIDGETS(DebugData, LocalData)
	UPDATE_PED_PERFORMANCE_DEBUG_WIDGETS(ePedLocation, DebugData, ServerBD, LocalData)
	
	IF NOT DebugData.GeneralData.bEnableDebug
		EXIT
	ENDIF
	
	INT iPed
	INT iNetworkPed
	
	IF (ServerBD.iMaxLocalPeds > 0)
		REPEAT ServerBD.iMaxLocalPeds iPed
			UPDATE_PED_GENERAL_DEBUG_WIDGETS(ePedLocation, DebugData, ServerBD, LocalData, iPed)
			UPDATE_PED_SPEECH_DEBUG_WIDGETS(ePedLocation, DebugData, ServerBD, LocalData, iPed)
			UPDATE_PED_CULLING_DEBUG_WIDGETS(DebugData, ServerBD, LocalData)
			UPDATE_ACTIVE_LOCAL_PEDS_DEBUG_WIDGETS(ePedLocation, DebugData, ServerBD, LocalData, LocalData.LoiterData, iPed)
		ENDREPEAT
	ENDIF
	
	UPDATE_PED_ANIMATION_DEBUG_WIDGETS(ePedLocation, DebugData, ServerBD, LocalData)
	UPDATE_LOCAL_PED_HEAD_TRACKING_DEBUG_WIDGETS(DebugData, LocalData)
	UPDATE_LOCAL_PED_PARENT_DEBUG_WIDGETS(DebugData, LocalData)
	UPDATE_LOCAL_PED_PROP_DEBUG_WIDGETS(DebugData, LocalData, ePedLocation)
	UPDATE_LOCAL_PED_MODEL_DEBUG_WIDGETS(ePedLocation, DebugData, ServerBD, LocalData)
	UPDATE_LOCAL_PED_VFX_DEBUG_WIDGETS(DebugData, LocalData)
	UPDATE_LOCAL_PED_FADE_DEBUG_WIDGETS(ServerBD, LocalData)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	IF (ServerBD.iMaxNetworkPeds > 0)
		REPEAT ServerBD.iMaxNetworkPeds iPed
			iNetworkPed = GET_PED_NETWORK_ID(iPed)
			UPDATE_PED_GENERAL_DEBUG_WIDGETS(ePedLocation, DebugData, ServerBD, LocalData, iNetworkPed, TRUE)
			UPDATE_PED_SPEECH_DEBUG_WIDGETS(ePedLocation, DebugData, ServerBD, LocalData, iNetworkPed, TRUE)
			UPDATE_PED_CULLING_DEBUG_WIDGETS(DebugData, ServerBD, LocalData)
			UPDATE_ACTIVE_NETWORK_PEDS_DEBUG_WIDGETS(DebugData, ServerBD, LocalData.LoiterData, iNetworkPed)
		ENDREPEAT
	ENDIF
	
	UPDATE_NETWORK_PED_HEAD_TRACKING_DEBUG_WIDGETS(DebugData, ServerBD)
	UPDATE_NETWORK_PED_PARENT_DEBUG_WIDGETS(DebugData, ServerBD)
	UPDATE_NETWORK_PED_PROP_DEBUG_WIDGETS(DebugData, ServerBD, ePedLocation)
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ CREATE WIDGETS ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC CREATE_PED_DEBUG_WIDGETS(PED_LOCATIONS ePedLocation, PED_DEBUG_DATA &DebugData, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	CREATE_PED_GENERAL_DEBUG_WIDGETS(DebugData, ServerBD)
	CREATE_PED_SERVER_BROADCAST_DATA_DEBUG_WIDGETS(ePedLocation, DebugData, ServerBD, LocalData)
	CREATE_ALL_ACTIVE_PEDS_DEBUG_WIDGETS(ServerBD, LocalData)
	CREATE_PED_SPEECH_DEBUG_WIDGETS(DebugData, LocalData)
	CREATE_PED_HEAD_TRACKING_DEBUG_WIDGETS(DebugData, ServerBD, LocalData)
	CREATE_PED_CULLING_DEBUG_WIDGETS(DebugData)
	CREATE_PED_ANIMATION_DEBUG_WIDGETS(DebugData)
	CREATE_PED_PARENT_DEBUG_WIDGETS(DebugData, ServerBD, LocalData)
	CREATE_PED_PROP_DEBUG_WIDGETS(DebugData)
	CREATE_PED_MODEL_DEBUG_WIDGETS(DebugData)
	CREATE_PED_DJ_SWITCH_DEBUG_WIDGETS(DebugData)
	CREATE_PED_VFX_DEBUG_WIDGETS(DebugData, ServerBD)
	CREATE_PED_PERFORMANCE_DEBUG_WIDGETS(ePedLocation, DebugData)
	CREATE_PED_FADE_DEBUG_WIDGETS(ServerBD, LocalData)
	CREATE_PED_LOOK_WIDGETS()
	
ENDPROC
#ENDIF	// IS_DEBUG_BUILD
#ENDIF	// FEATURE_HEIST_ISLAND
