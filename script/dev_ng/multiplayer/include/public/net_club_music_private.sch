//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_CLUB_MUSIC_PRIVATE.sch																				//
// Description: Private header file containing wrapper functions for the table look ups.								//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		30/03/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_CASINO_NIGHTCLUB
USING "net_club_music_casino_nightclub.sch"
#ENDIF

#IF FEATURE_FIXER
USING "net_club_music_studio.sch"
#ENDIF

#IF FEATURE_HEIST_ISLAND
USING "net_club_music_island.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_CLUB_MUSIC_LOOK_UP_TABLE(CLUB_LOCATIONS eClubLocation, CLUB_MUSIC_INTERFACE &interface, CLUB_MUSIC_PROCEDURES eProc)
	SWITCH eClubLocation
		CASE CLUB_LOCATION_ISLAND
			BUILD_CLUB_MUSIC_ISLAND_LOOK_UP_TABLE(interface, eProc)
		BREAK
		#IF FEATURE_CASINO_NIGHTCLUB
		CASE CLUB_LOCATION_CASINO_NIGHTCLUB
			BUILD_CLUB_MUSIC_CASINO_NIGHTCLUB_LOOK_UP_TABLE(interface, eProc)
		BREAK
		#ENDIF	
		#IF FEATURE_FIXER
		CASE CLUB_LOCATION_MUSIC_STUDIO
			BUILD_CLUB_MUSIC_STUDIO_LOOK_UP_TABLE(interface, eProc)
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ WRAPPER FUNCTIONS ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Gets the current DJ for the club location.
/// PARAMS:
///    eClubLocation - Club location.
///    iDJ - Current DJ index.
/// RETURNS: DJ for the clubs location.
FUNC CLUB_DJS GET_CLUB_DJ(DJ_SERVER_DATA &DJServerData, CLUB_LOCATIONS eClubLocation, INT iDJ, BOOL bNextDJ = FALSE, BOOL bUsingPathB = FALSE)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_DJ)
	IF (lookUpTable.returnGetClubDJ != NULL)
		RETURN CALL lookUpTable.returnGetClubDJ(DJServerData, iDJ, bNextDJ, bUsingPathB)
	ENDIF
	RETURN CLUB_DJ_NULL
ENDFUNC

/// PURPOSE:
///    Gets the max DJs for the club location.
/// PARAMS:
///    eClubLocation - Club location.
/// RETURNS: Max DJs in the clubs location.
FUNC INT GET_CLUB_MAX_DJS(CLUB_LOCATIONS eClubLocation)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_MAX_DJS)
	IF (lookUpTable.returnGetClubMaxDJS != NULL)
		RETURN CALL lookUpTable.returnGetClubMaxDJS()
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the clubs radio station.
/// PARAMS:
///    eClubLocation - Club location.
///    eClubDJ - DJ to query. 
///    bPrivateMode - If Private mode is active - Private mode has different radio stations.
/// RETURNS: The radio station enum associated with the DJ.
FUNC CLUB_RADIO_STATIONS GET_CLUB_RADIO_STATION(CLUB_LOCATIONS eClubLocation, CLUB_DJS eClubDJ, BOOL bPrivateMode = FALSE)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_RADIO_STATION)
	IF (lookUpTable.returnGetClubRadioStation != NULL)
		RETURN CALL lookUpTable.returnGetClubRadioStation(eClubDJ, bPrivateMode)
	ENDIF
	RETURN CLUB_RADIO_STATION_NULL
ENDFUNC

/// PURPOSE:
///    Gets the DJ radio station name as a string.
/// PARAMS:
///    eClubLocation - Club location.
///    eRadioStation - Radio station to get the name of.
/// RETURNS: Radio station name as a string.
FUNC STRING GET_CLUB_RADIO_STATION_NAME(CLUB_LOCATIONS eClubLocation, CLUB_RADIO_STATIONS eRadioStation)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_RADIO_STATION_NAME)
	IF (lookUpTable.returnGetClubRadioStationName != NULL)
		RETURN CALL lookUpTable.returnGetClubRadioStationName(eRadioStation)
	ENDIF
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the max tracks the passed in radio station plays.
/// PARAMS:
///    eClubLocation - Club location.
///    eRadioStation - Radio station to query.
/// RETURNS: The max tracks a radio station can play.
FUNC INT GET_CLUB_RADIO_STATION_MAX_TRACKS(CLUB_LOCATIONS eClubLocation, CLUB_RADIO_STATIONS eRadioStation)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_RADIO_STATION_MAX_TRACKS)
	IF (lookUpTable.returnGetClubRadioStationMaxTracks != NULL)
		RETURN CALL lookUpTable.returnGetClubRadioStationMaxTracks(eRadioStation)
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the track name of a radio station.
/// PARAMS:
///    eClubLocation - Club location.
///    eRadioStation - Radio station to query.
///    iTrackID - Music track to get name of.
/// RETURNS: The name of a radio station track as a string.
FUNC STRING GET_CLUB_RADIO_STATION_TRACK_NAME(CLUB_LOCATIONS eClubLocation, CLUB_RADIO_STATIONS eRadioStation, INT iTrackID)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_RADIO_STATION_TRACK_NAME)
	IF (lookUpTable.returnGetClubRadioStationTrackName != NULL)
		RETURN CALL lookUpTable.returnGetClubRadioStationTrackName(eRadioStation, iTrackID)
	ENDIF
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the duration of a music track in milliseconds.
/// PARAMS:
///    eClubLocation - Club location.
///    eRadioStation - Radio station to query.
///    iTrackID - Music track to query.
/// RETURNS: The duration of a music track in milliseconds.
FUNC INT GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(CLUB_LOCATIONS eClubLocation, CLUB_RADIO_STATIONS eRadioStation, INT iTrackID)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_RADIO_STATION_TRACK_DURATION_MS)
	IF (lookUpTable.returnGetClubRadioStationTrackDurationMS != NULL)
		RETURN CALL lookUpTable.returnGetClubRadioStationTrackDurationMS(eRadioStation, iTrackID)
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets sum of tracks according up to current track
FUNC INT GET_SUM_CLUB_RADIO_STATION_TRACK_TIME_MS(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, INT iCurrentTrack)
	INT i, iSum
	FOR i = 1 TO iCurrentTrack
		iSum += GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, i)
	ENDFOR
	PRINTLN("[CLUB_MUSIC] GET_SUM_CLUB_RADIO_STATION_TRACK_TIME iSum: ", iSum)
	RETURN iSum
ENDFUNC

/// PURPOSE:
///    Gets the current intensity of a radio station track. Also gets the intensity time MS that the intensity changes at during the track.
/// PARAMS:
///    eClubLocation - Club location.
///    eRadioStation - Radio station to query.
///    iTrackID - Music track to query.
///    iTrackIntensityID - The track intensity ID. This is based on the play time of the current track.
///    iTrackIntensityTimeMS - Reference that holds the track time (MS) that the intensity changes, based on the iTrackIntensityID.
///    eClubMusicIntensity - Reference that holds the track intensity based on the iTrackIntensityID.
PROC GET_CLUB_RADIO_STATION_TRACK_INTENSITY(CLUB_LOCATIONS eClubLocation, CLUB_RADIO_STATIONS eRadioStation, INT iTrackID, INT iTrackIntensityID, INT &iTrackIntensityTimeMS, CLUB_MUSIC_INTENSITY &eClubMusicIntensity)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_RADIO_STATION_TRACK_INTENSITY)
	IF (lookUpTable.getClubRadioStationTrackIntensity != NULL)
		CALL lookUpTable.getClubRadioStationTrackIntensity(eRadioStation, iTrackID, iTrackIntensityID, iTrackIntensityTimeMS, eClubMusicIntensity)
	ENDIF
ENDPROC

/// PURPOSE:
///    Implemented for music studio to bypass the intensity related checks
/// PARAMS:
///    eClubLocation - Club location.
///    eClubDJ 		 - DJ to query.
FUNC BOOL DOES_CLUB_USE_MUSIC_INTENSITY(CLUB_LOCATIONS eClubLocation)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_DOES_CLUB_USE_MUSIC_INTENSITY)
	IF (lookUpTable.returnDoesClubUseMusicIntensity != NULL)
		RETURN CALL lookUpTable.returnDoesClubUseMusicIntensity()
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the max static emitters inside a club.
/// PARAMS:
///    eClubLocation - Club location.
/// RETURNS: The max number of static emitters inside a club.
FUNC INT GET_CLUB_MAX_STATIC_EMITTERS(CLUB_LOCATIONS eClubLocation)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_MAX_STATIC_EMITTERS)
	IF (lookUpTable.returnGetClubMaxStaticEmitters != NULL)
		RETURN CALL lookUpTable.returnGetClubMaxStaticEmitters()
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the name of the static emitter based on its ID.
/// PARAMS:
///    eClubLocation - Club location.
///    iEmitterID - The ID of the static emitter.
/// RETURNS: Static emitter name as a string.
FUNC STRING GET_CLUB_STATIC_EMITTER_NAME(CLUB_LOCATIONS eClubLocation, INT iEmitterID)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_STATIC_EMITTER_NAME)
	IF (lookUpTable.returnGetClubStaticEmitterName != NULL)
		RETURN CALL lookUpTable.returnGetClubStaticEmitterName(iEmitterID)
	ENDIF
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the name of the clubs audio scene.
/// PARAMS:
///    eClubLocation - Club location.
/// RETURNS: Audio scene name as a string.
FUNC STRING GET_CLUB_AUDIO_SCENE_NAME(CLUB_LOCATIONS eClubLocation)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_AUDIO_SCENE_NAME)
	IF (lookUpTable.returnGetClubAudioSceneName != NULL)
		RETURN CALL lookUpTable.returnGetClubAudioSceneName()
	ENDIF
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the name of the zone. this will boost the audio when player get close to zone
/// PARAMS:
///    eClubLocation - Club location.
/// RETURNS: Audio scene name as a string.
FUNC STRING GET_CLUB_AUDIO_ZONE_NAME(CLUB_LOCATIONS eClubLocation)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_GET_CLUB_ZONE_NAME)
	IF (lookUpTable.returnGetClubZoneName != NULL)
		RETURN CALL lookUpTable.returnGetClubZoneName()
	ENDIF
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Determines if we use UGC file to pull ped data
/// PARAMS:
///    eClubLocation -  Club location.
///    sDataFileName - TRUE if we use UGC file and return valid file name
/// RETURNS:
///    
FUNC BOOL DOES_CLUB_LOCATION_USE_DATAFILE(CLUB_LOCATIONS eClubLocation, CLUB_DJS eClubDJ, TEXT_LABEL_63 &sDataFileName)
	CLUB_MUSIC_INTERFACE lookUpTable
	BUILD_CLUB_MUSIC_LOOK_UP_TABLE(eClubLocation, lookUpTable, E_DOES_DJ_DATA_LOAD_FROM_DATAFILE)
	RETURN CALL lookUpTable.returnDoesDJDataLoadFromDataFile(eClubDJ, sDataFileName)
ENDFUNC
#ENDIF //FEATURE_HEIST_ISLAND
