USING "rage_builtins.sch"
USING "globals.sch"

USING "net_activity_selector_groups.sch"

USING "net_prints.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_activity_selector.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Treats a number of activities as a group and selects one as the 'current' mission from the group.
//		NOTES			:	This is intended to be used as follows:
//								- In Freemode to place a number of missions on the map from a specific in-game contact
//								- In Freemode to place a choice of missions at a savehouse
//								- In CnC to replace the current mission selector
//								- In CnC to potentially integrate the grouped mission routines
//							Missions that start at blips will be registered with the Mission At Coords system.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      ACTIVITY SELECTOR WIDGETS
// ===========================================================================================================

// PURPOSE:	Set up any Activity Selector Widgets
#IF IS_DEBUG_BUILD
PROC Create_MP_Activity_Selector_Widgets()
	
	// Widgets
	START_WIDGET_GROUP("Activity Selector")
		Create_Activity_Selector_Group_Widgets()
	STOP_WIDGET_GROUP()
	
ENDPROC
#ENDIF




// ===========================================================================================================
//      GAME-SPECIFIC FUNCTIONS
//		(May be moved to a different header sometime)
// ===========================================================================================================

// PURPOSE:	Get generic safehouse coords that can later be identified and swapped on a per-player basis for the player's actual safehouse coords
//
// RETURN VALUE:		VECTOR				Coords for the generic savehouse coords
//
// NOTES:	KGM 5/2/13: This is Freemode-specific.
FUNC VECTOR Get_MP_Heist_Generic_Safehouse_Coords()
	RETURN (MP_HEIST_GENERIC_SAFEHOUSE_COORDS)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get generic contact mission coords that allows all contact missions to be grouped together for the server to control as a group
//
// RETURN VALUE:		VECTOR				Coords for the generic contact mission coords
//
// NOTES:	KGM 17/4/13: This is Freemode-specific.
FUNC VECTOR Get_MP_Contact_Mission_Generic_Mission_Coords()
	RETURN (MP_CONTACT_MISSION_GENERIC_COORDS)
ENDFUNC




// ===========================================================================================================
//      Public Access Functions
// ===========================================================================================================

// PURPOSE:	Some data may need cleared out as a cloud refresh is ongoing
PROC Prepare_Local_Activity_Selector_For_Cloud_Data_Refresh()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect]: Prepare Local Activity Selector Data that needs to know about a Cloud Refresh") NET_NL()
	#ENDIF
	
	// Contact Mission storage data is local and should get cleared out ready for fresh data
	Clear_Local_Contact_Missions()
	
	// Heist Strand storage data is local and should get cleared out ready for fresh data
	Clear_Local_Heist_Strands()
		// FEATURE_HEIST_PLANNING

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Add Details of an activity to be maintained by the Activity Selector Server
//
// INPUT PARAMS:			paramCoords				The activity's coords
//							paramContact			The activity's Contact (or NO_CHARACTER)
//							paramRank				The activity's rank
// REFERENCE PARAMS:		refMissionIdData		Mission Identification Data
PROC Add_Activity_To_Activity_Selector_Server(MP_MISSION_ID_DATA &refMissionIdData, VECTOR paramCoords, enumCharacterList paramContact, INT paramRank)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect]: Adding Activity To Activity Selector by Broadcasting to Pending Activities") NET_NL()
	#ENDIF
	
	// KGM_HEIST_DLC: 3/9/13: Remove this blocking code when we want Heists to work again
//	IF (Is_FM_Cloud_Loaded_Activity_A_Heist(refMissionIdData))
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [ActSelect]: KGM 3/9/13: IGNORING MISSION TAGGED AS A HEIST. ContentID: ") NET_PRINT(refMissionIdData.idCloudFilename) NET_NL()
//			SCRIPT_ASSERT("Add_Activity_To_Activity_Selector_Server() - IGNORING MISSION TAGGED AS A HEIST")
//		#ENDIF
//		
//		EXIT
//	ENDIF
	
	// Contact Missions should now use local registration
	IF (ARE_VECTORS_ALMOST_EQUAL(paramCoords, MP_CONTACT_MISSION_GENERIC_COORDS))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect]: ERROR - Contact Mission should not store data with the server") NET_NL()
			SCRIPT_ASSERT("Add_Activity_To_Activity_Selector_Server() - ERROR: Contact Mission should not store data with the server")
		#ENDIF
	
		EXIT
	ENDIF
	
	// Error Checking - ensure only appropriate mission types get registered as Pending Activities
	// NOTE: If other activity types can be setup in safehouses then the transition session INT we use to broadcast the safehouseID will need to change as it will clash with a Races use
	#IF IS_DEBUG_BUILD
		IF NOT (Are_Details_Consistent_For_Activity_Selector(refMissionIdData, paramContact, paramRank))
			EXIT
		ENDIF
	#ENDIF

	// Store the data on the Pending Activities array
	Broadcast_Add_Activity_To_Pending_Activities(refMissionIdData, paramCoords, paramContact, paramRank)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Add Details of an activity to be maintained locally by the Activity Selector
//
// INPUT PARAMS:			paramCoords				The activity's coords
//							paramContact			The activity's Contact (or NO_CHARACTER)
//							paramRank				The activity's rank
// REFERENCE PARAMS:		refMissionIdData		Mission Identification Data
PROC Add_Activity_To_Local_Activity_Selector(MP_MISSION_ID_DATA &refMissionIdData, VECTOR paramCoords, enumCharacterList paramContact, INT paramRank)
	
	// Error Checking - ensure only appropriate mission types get registered as Pending Activities
	// NOTE: If other activity types can be setup in safehouses then the transition session INT we use to broadcast the safehouseID will need to change as it will clash with a Races use
	#IF IS_DEBUG_BUILD
		IF NOT (Are_Details_Consistent_For_Activity_Selector(refMissionIdData, paramContact, paramRank))
			EXIT
		ENDIF
	#ENDIF
	
	// KGM 14/4/14: Allow a CnC/Not CnC test and filter Contact Missions from being setup as appropriate
	BOOL isCNC = FALSE
	
	// A CnC Mission will have a minimum Gang Rank, so we'll use that as the decision maker for now
	INT	gangRank = Get_Gang_Rank_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	BOOL isMissionForCNC = (gangRank > 0)
	
	IF (isCNC)
	AND NOT (isMissionForCNC)
		// ...this is CnC but the mission is not a CnC Mission, so ditch it
		PRINTLN(".KGM [ActSelect]: Ditch Contact Mission. CnC Active, but CM is not for CnC: ", refMissionIdData.idCloudFilename)
		EXIT
	ENDIF
	
	IF NOT (isCNC)
	AND (isMissionForCNC)
		// ...this is not CnC but the mission is a CnC Mission, so ditch it
		PRINTLN(".KGM [ActSelect]: Ditch Contact Mission. CnC is not Active, but CM is for CnC: ", refMissionIdData.idCloudFilename)
		EXIT
	ENDIF
	// KGM: END Cnc/Not CnC test and filtering-----------------------------------------------------------

	INT theMissionRankToUse = paramRank
	IF (isCNC)
		theMissionRankToUse = gangRank
	ENDIF
	
	// Contact Missions should use local registration
	IF (ARE_VECTORS_ALMOST_EQUAL(paramCoords, MP_CONTACT_MISSION_GENERIC_COORDS))
		Add_Contact_Mission_To_Local_Activity_Selector(refMissionIdData, paramContact, theMissionRankToUse)
		
		EXIT
	ENDIF
	
	// Heist Strands (using Heist Finale data) should use local registration
	IF (ARE_VECTORS_ALMOST_EQUAL(paramCoords, MP_HEIST_GENERIC_SAFEHOUSE_COORDS))
		Add_Heist_Strand_To_Local_Activity_Selector(refMissionIdData, paramContact, theMissionRankToUse)
		
		EXIT
	ENDIF
		// FEATURE_HEIST_PLANNING
	
	// All other registrations should register with the server
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect]: ERROR - This Activity should not store local data") NET_NL()
		SCRIPT_ASSERT("Add_Activity_To_Local_Activity_Selector() - ERROR: This Activity should not store local data")
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Server Initialisation and Maintenance Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//     Server Initialisation
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Initialise the Activity Selector Server into a default starting state
PROC Initialise_MP_Activity_Selector_Server()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect]: Initialise_MP_Activity_Selector_Server") NET_NL()
	#ENDIF
	
	// Initialise the Group Server variables
	Initialise_MP_Activity_Selector_Group_Server()

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//     Server Maintenance
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Maintain the Activity Selector Server
PROC Maintain_MP_Activity_Selector_Server()
	
	// Maintain Group server scheduled maintenance
	Maintain_MP_Activity_Selector_Group_Server()

ENDPROC




// ===========================================================================================================
//      Client Initialisation and Maintenance Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//     Client Initialisation
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Initialise the Activity Selector Client into a default starting state
PROC Initialise_MP_Activity_Selector_Client()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect]: Initialise_MP_Activity_Selector_Client") NET_NL()
	#ENDIF
	
	// Initialise the Group Client variables
	Initialise_MP_Activity_Selector_Group_Client()
	
	// Setup the Heist Safehouse Vector (used for identification purposes only) with an unrealistic vector to ensure it only matches with itself
	MP_HEIST_GENERIC_SAFEHOUSE_COORDS = << -9999.9, -9999.9, -9999.9 >>
	
	// Setup the Contact Mission Group Vector (used for identification purposes only) with an unrealistic vector to ensure it only matches with itself
	MP_CONTACT_MISSION_GENERIC_COORDS = << -8888.8, -8888.8, -8888.8 >>

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//     Client Maintenance
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Maintain the Activity Selector Client
PROC Maintain_MP_Activity_Selector_Client()
	
	// Maintain Group client scheduled maintenance
	Maintain_MP_Activity_Selector_Group_Client()

ENDPROC




// ===========================================================================================================
//      GAME-SPECIFIC ACCESS FUNCTIONS
//		(May be moved to a different header sometime)
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//     	FM Contact Mission-terminology functions
//		(Functions setup to use Contact Mission terminology as an interface into generic functions)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The game allows a Contact Mission triggering communication to take place
PROC Allow_FM_Contact_Mission_Triggering_Communication()
	#IF IS_DEBUG_BUILD
	IF (GET_COMMANDLINE_PARAM_EXISTS("sc_ForcedRankInviteDebug"))
		CPRINTLN(DEBUG_AMBIENT, "[2703037] [Allow_FM_Contact_Mission_Triggering_Communication] Calling Allow_FM_Heist_Triggering_Communication...")
	ENDIF
	#ENDIF
	
	// Ignore if this player is already processing a Contact Mission
	IF (Get_Player_Contact_Mission_Stage(PLAYER_ID()) != CONTACT_MISSION_CLIENT_STAGE_NO_MISSION)
		#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_ForcedRankInviteDebug"))
			CPRINTLN(DEBUG_AMBIENT, "[2703037] [Allow_FM_Contact_Mission_Triggering_Communication] Get_Player_Contact_Mission_Stage(PLAYER_ID()) != CONTACT_MISSION_CLIENT_STAGE_NO_MISSION")
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	// Ignore if player timeout hasn't expired
	IF NOT (Has_Client_Contact_Mission_Player_Timeout_Expired(PLAYER_ID()))
		#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_ForcedRankInviteDebug"))
			CPRINTLN(DEBUG_AMBIENT, "[2703037] [Allow_FM_Contact_Mission_Triggering_Communication] NOT (Has_Client_Contact_Mission_Player_Timeout_Expired(PLAYER_ID()))")
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	// Try to find a suitable mission to offer to the player
	Try_To_Find_Contact_Mission_To_Offer_To_Player()
ENDPROC





// -----------------------------------------------------------------------------------------------------------
//     	FM Heist-terminology functions
//		(Functions setup to use Heist terminology as an interface into generic functions)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The game allows a Heist triggering communication to take place
PROC Allow_FM_Heist_Triggering_Communication()

	// Until we get Contact Mission specific triggering, forward the call to the Contact Mission function
	// then allow the Heist routine below to be processed.
	Allow_FM_Contact_Mission_Triggering_Communication()
	
	// Ignore for now if the player is on a mission
	IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		EXIT
	ENDIF
	
	// KGM 19/5/14: The new method which will replace the obsolete commented out method below
	Allow_MP_Heist_Comms_If_Available()
	Allow_MP_Heist_Completion_Phonecall_If_Available()
		// FEATURE_HEIST_PLANNING

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gather the details for the next Heist mission
//
// INPUT PARAMS:		paramStartSlot			The slot marking the start of the search for the next Heist mission
//						paramPlayerBits			[DEFAULT = ALL_PLAYER_BITS_CLEAR] A Bitfield of players whose Heist missions are to be considered
//						paramWrap				[DEFAULT = TRUE] Allow Wrap
// RETURN VALUE:		g_structASFMHeistMP		A struct containing the details needed to identify a mission
FUNC g_structASFMHeistMP Get_Next_FM_Heist_Mission_Details(INT paramStartSlot, INT paramPlayerBits = ALL_PLAYER_BITS_CLEAR, BOOL paramWrap = TRUE)

	g_structASFMHeistMP returnDetails
	Clear_MP_MISSION_ID_DATA_Struct(returnDetails.fmhPendingActivitiesMissionIdData)
	
	INT nextSlot = Get_Next_Missions_Shared_Slot_For_These_Details(paramStartSlot, paramPlayerBits, paramWrap)
	
	// Are there any details?
	IF (nextSlot = ILLEGAL_PENDING_ACTIVITIES_SLOT)
		// ...no, so just return the struct with the default values
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect]: Get_Next_FM_Heist_Mission_Details() - FAILED TO FIND MATCHING HEIST DETAILS") NET_NL()
		#ENDIF
	
		RETURN returnDetails
	ENDIF
	
	// Found details, so fill the struct
	returnDetails.fmhPendingActivitiesArrayPos			= nextSlot
	returnDetails.fmhPendingActivitiesMissionIdData		= Get_Pending_Activities_MissionID_Data_For_Slot(nextSlot)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect]: Passing Next Heist Details: Slot ")
		NET_PRINT_INT(nextSlot)
		NET_PRINT(" [Creator: ")
		NET_PRINT_INT(returnDetails.fmhPendingActivitiesMissionIdData.idCreator)
		NET_PRINT(" (Var: ")
		NET_PRINT_INT(returnDetails.fmhPendingActivitiesMissionIdData.idVariation)
		NET_PRINT(")] - ")
		NET_PRINT(GET_MP_MISSION_NAME(returnDetails.fmhPendingActivitiesMissionIdData.idMission))
		NET_NL()
	#ENDIF
	
	RETURN (returnDetails)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gather the details for the previous Heist mission
//
// INPUT PARAMS:		paramStartSlot			The slot marking the start of the search for the next Heist mission
//						paramPlayerBits			[DEFAULT = ALL_PLAYER_BITS_CLEAR] A Bitfield of players whose Heist missions are to be considered
//						paramWrap				[DEFAULT = TRUE] Allow Wrap
// RETURN VALUE:		g_structASFMHeistMP		A struct containing the details needed to identify a mission
FUNC g_structASFMHeistMP Get_Previous_FM_Heist_Mission_Details(INT paramStartSlot, INT paramPlayerBits = ALL_PLAYER_BITS_CLEAR, BOOL paramWrap = TRUE)

	g_structASFMHeistMP returnDetails
	Clear_MP_MISSION_ID_DATA_Struct(returnDetails.fmhPendingActivitiesMissionIdData)
	
	INT previousSlot = Get_Previous_Missions_Shared_Slot_For_These_Details(paramStartSlot, paramPlayerBits, paramWrap)
	
	// Are there any details?
	IF (previousSlot = ILLEGAL_PENDING_ACTIVITIES_SLOT)
		// ...no, so just return the struct with the default values
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect]: Get_Previous_FM_Heist_Mission_Details() - FAILED TO FIND MATCHING HEIST DETAILS") NET_NL()
		#ENDIF
	
		RETURN returnDetails
	ENDIF
	
	// Found details, so fill the struct
	returnDetails.fmhPendingActivitiesArrayPos			= previousSlot
	returnDetails.fmhPendingActivitiesMissionIdData		= Get_Pending_Activities_MissionID_Data_For_Slot(previousSlot)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect]: Passing Previous Heist Details: Slot ")
		NET_PRINT_INT(previousSlot)
		NET_PRINT(" [Creator: ")
		NET_PRINT_INT(returnDetails.fmhPendingActivitiesMissionIdData.idCreator)
		NET_PRINT(" (Var: ")
		NET_PRINT_INT(returnDetails.fmhPendingActivitiesMissionIdData.idVariation)
		NET_PRINT(")] - ")
		NET_PRINT(GET_MP_MISSION_NAME(returnDetails.fmhPendingActivitiesMissionIdData.idMission))
		NET_NL()
	#ENDIF
	
	RETURN (returnDetails)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Setup the chosen heist mission
//
// INPUT PARAMS:		paramChosenHeist		A struct containing the details needed to identify the mission
PROC Setup_Chosen_FM_Heist(g_structASFMHeistMP paramChosenHeist)

	INT					theSlot					= paramChosenHeist.fmhPendingActivitiesArrayPos
	MP_MISSION_ID_DATA	updatedMissionIdData	= paramChosenHeist.fmhPendingActivitiesMissionIdData

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect]: Setup_Chosen_FM_Heist() - ACCEPTED THESE DETAILS:") NET_NL()
		NET_PRINT("       Slot: ")
		NET_PRINT_INT(theSlot)
		NET_PRINT(" [Creator: ")
		NET_PRINT_INT(updatedMissionIdData.idCreator)
		NET_PRINT(" (Var: ")
		NET_PRINT_INT(updatedMissionIdData.idVariation)
		NET_PRINT(")] - ")
		NET_PRINT(GET_MP_MISSION_NAME(updatedMissionIdData.idMission))
		NET_NL()
	#ENDIF
	
	// Ensure the slot is still valid
	IF NOT (Is_Pending_Activities_Slot_In_Use(theSlot))
		// ..the slot isn't in use - this should become impossible once all the consistency checks are in (the slot shouldn't get cleared until no one is accessing it)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect]: Setup_Chosen_FM_Heist() - ERROR: Slot is no longer in use") NET_NL()
			SCRIPT_ASSERT("Setup_Chosen_FM_Heist(): ERROR - Slot is no longer in use. Ditching this request. This still needs to be correctly handled, but should never happen. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// This heist is controlled by this player, so let the Mission Controller know about the change in details
	INT missionControllerUniqueID = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
	IF (missionControllerUniqueID = NO_UNIQUE_ID)
		// ...this player is no longer on a mission with the Mission Controller, so nothing to do - it should all have cleaned up correctly elsewhere
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [ActSelect]: Setup_Chosen_FM_Heist() - WARNING: Player is not allocated to a mission on the Mission Controller. Ignoring this request, the mission must have cleaned up") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Broadcast the new mission details to the Mission Controller
	BOOL actingAsScriptHost	= FALSE
	Broadcast_Change_Reserved_Mission_Details(missionControllerUniqueID, updatedMissionIdData, actingAsScriptHost)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [ActSelect]: Changed Mission Details for Mission Controller UniqueID: ") NET_PRINT_INT(missionControllerUniqueID) NET_NL()
	#ENDIF
		
// Still needed? Probably not - KGM 29/3/13
//	// TO DO: Update the details of the chosen mission on the 'Shared Mission Details' system (to be implemented)
//	
//	// Broadcast the chosen details to all players
//	Broadcast_Setup_Player_Choice_Group_Mission(MSGID_FM_HEIST, paramChosenHeist.fmhPendingActivitiesArrayPos)

ENDPROC





