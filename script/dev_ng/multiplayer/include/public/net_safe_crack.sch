//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_SAFE_CRACK.sch																			//
// Description: Controls the Safe Cracking mini-game														//
// Written by:  Ryan Baker																					//
// Date: 15/11/2011																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "help_at_location.sch"
USING "shared_hud_displays.sch"
//**USING "context_control_public.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "screen_placements.sch"

// CnC Headers

USING "net_hud_activating.sch"

//Constants
//CONST_INT MAX_DRAWN_DIAL_NUMBERS		5
CONST_INT MAX_DIAL_NUMBERS				360
CONST_INT MAX_NUM_LOCKS					3
CONST_INT DIAL_MOVEMENT_DELAY_MAX		70		//500
CONST_INT DIAL_MOVEMENT_DELAY_MIN		7		//100
CONST_INT SAFE_CRACK_UNLOCK_TIME		1500	//2000
CONST_FLOAT DIAL_MOVEMENT_AMOUNT		3.6		//4		//3
TWEAK_INT DIAL_SHAKE_TIME				20
TWEAK_FLOAT DIAL_SHAKE_RANGE			0.0007

CONST_INT SC_ANIM_SUCCEED	0
CONST_INT SC_ANIM_FAIL		1

//Safe Cracking BitSet
CONST_INT SC_BS_IS_PLAYER_IN_POSITION			0
CONST_INT SC_BS_IS_PLAYER_SAFE_CRACKING			1
CONST_INT SC_BS_HAS_PLAYER_PASSED_SAFE_CRACK	2
CONST_INT SC_BS_HAS_PLAYER_FAILED_SAFE_CRACK	3
CONST_INT SC_BS_HAS_PLAYER_QUIT_SAFE_CRACK		4
CONST_INT SC_BS_STICK_PRESSED_LEFT				5
CONST_INT SC_BS_STICK_PRESSED_RIGHT				6
CONST_INT SC_BS_DIAL_IN_CORRECT_POSITION		7
CONST_INT SC_BS_HIDE_WEAPON_ON_OPEN_START		8
CONST_INT SC_BS_HIDE_WEAPON_ON_OPEN_DONE		9
CONST_INT SC_START_HELP_DISPLAYED				10
CONST_INT SC_LAST_TURNED_LEFT					11

VECTOR		SC_ANIM_OFFSET_FROM_SAFE			= <<-0.2152,-0.8895,0.2479>>

//Safe Cracking Variables
STRUCT SAFE_CRACK_STRUCT
	INT iBitSet
	//INT iNumbers[MAX_DRAWN_DIAL_NUMBERS]
	INT iLockPosition[MAX_NUM_LOCKS]
	INT iSafeCrackStage = 0
	SCRIPT_TIMER iMoveDialTimer
	INT iMoveDialTimerDelay
	SCRIPT_TIMER iUnlockTimer
	VECTOR vSafeCoord
	FLOAT fSafeHeading
	VECTOR vPlayerSafeCrackCoord
	FLOAT fPlayerSafeCrackHeading
	VECTOR vFloatingHelpMin
	VECTOR vFloatingHelpMax
	FLOAT fFloatingHelpWidth
	SPRITE_PLACEMENT DialBase
	SPRITE_PLACEMENT DialMain
	INT iSoundID[6]
	//**INT iStartContextID = NEW_CONTEXT_INTENTION
	//**INT iEndContextID = NEW_CONTEXT_INTENTION
	CAMERA_INDEX CameraIndex
	VECTOR vCameraPos
	VECTOR vCameraRot
	FLOAT fCameraFOV
	VECTOR vSafeDoorProp
	SEQUENCE_INDEX seqGetIntoPos
	SEQUENCE_INDEX seqIdleAnim
	SEQUENCE_INDEX seqLockSucceed
	SEQUENCE_INDEX seqLockFail
	SCRIPT_TIMER iDialShakeTimer
	INT iSafeDoorEnumHash
	INT iResetOnLeft = 1
	INT iResetOnRight = 2
	BOOL bForceExitSafe
	FLOAT iLockRange = DIAL_MOVEMENT_AMOUNT		// Can be increased to make it easier (large sweet spot to unlock)
	INT iSafeCrackUnlockTime = SAFE_CRACK_UNLOCK_TIME
	#IF IS_DEBUG_BUILD
	BOOL bWidgetsCreated
	#ENDIF
ENDSTRUCT


//PURPOSE: Returns TRUE when the assets for the Safe Crack MiniGame assets are loaded
FUNC BOOL HAVE_SAFE_CRACK_ASSETS_LOADED()
	REQUEST_STREAMED_TEXTURE_DICT("MPSafeCracking")
	REQUEST_ANIM_DICT("mini@safe_cracking")
	
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPSafeCracking")
	AND REQUEST_AMBIENT_AUDIO_BANK("SAFE_CRACK")
	AND HAS_ANIM_DICT_LOADED("mini@safe_cracking")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_SAFE_CRACK_UNLOCK_TIME(SAFE_CRACK_STRUCT &SafeCrackData)
	RETURN SafeCrackData.iSafeCrackUnlockTime
ENDFUNC

////*** PLAYER ANIMATIONS ***////

//PURPOSE: Get Random Anim
FUNC STRING GET_RANDOM_SAFE_CRACK_ANIM(INT iAnimType)
	SWITCH iAnimType
		//SUCCEED
		CASE 0
			SWITCH GET_RANDOM_INT_IN_RANGE(1, 5)
				CASE 1	RETURN "dial_turn_succeed_1"
				CASE 2	RETURN "dial_turn_succeed_2"
				CASE 3	RETURN "dial_turn_succeed_3"
				CASE 4	RETURN "dial_turn_succeed_4"
			ENDSWITCH
		BREAK
		//FAIL
		CASE 1
			SWITCH GET_RANDOM_INT_IN_RANGE(1, 5)
				CASE 1	RETURN "dial_turn_fail_1"
				CASE 2	RETURN "dial_turn_fail_2"
				CASE 3	RETURN "dial_turn_fail_3"
				CASE 4	RETURN "dial_turn_fail_4"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN "idle_base"
ENDFUNC

//PURPOSE: Creates all the sequences used for the Safe Cracking. Also needs to load the Safe Crack animations
FUNC BOOL CREATE_SAFE_CRACK_SEQUENCES(SAFE_CRACK_STRUCT &SafeCrackData, OBJECT_INDEX objSafe)
	REQUEST_ANIM_DICT("mini@safe_cracking")
	IF HAS_ANIM_DICT_LOADED("mini@safe_cracking")
	
		VECTOR vCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objSafe, SC_ANIM_OFFSET_FROM_SAFE)
		VECTOR vRot = <<0,0,GET_ENTITY_HEADING(objSafe)>>
		//Get Into Position
		OPEN_SEQUENCE_TASK(SafeCrackData.seqGetIntoPos)
			//TASK_TURN_PED_TO_FACE_COORD(NULL, SafeCrackData.vSafeCoord)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "step_into", vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_base", vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			//SET_SEQUENCE_TO_REPEAT(thisSeq, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK(SafeCrackData.seqGetIntoPos)
		
		//Idle
		OPEN_SEQUENCE_TASK(SafeCrackData.seqIdleAnim)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_base", vCoord, vRot, SLOW_BLEND_IN, 0.1, GET_RANDOM_INT_IN_RANGE(8000, 20000), AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_heavy_breathe", vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_base", vCoord, vRot, SLOW_BLEND_IN, 0.1, GET_RANDOM_INT_IN_RANGE(8000, 20000), AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_look_around", vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			SET_SEQUENCE_TO_REPEAT(SafeCrackData.seqIdleAnim, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK(SafeCrackData.seqIdleAnim)
		
		//Lock Succeed
		OPEN_SEQUENCE_TASK(SafeCrackData.seqLockSucceed)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", GET_RANDOM_SAFE_CRACK_ANIM(SC_ANIM_SUCCEED), vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_base", vCoord, vRot, SLOW_BLEND_IN, 0.1, GET_RANDOM_INT_IN_RANGE(8000, 20000), AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_heavy_breathe", vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_base", vCoord, vRot, SLOW_BLEND_IN, 0.1, GET_RANDOM_INT_IN_RANGE(8000, 20000), AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_look_around", vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON)
			SET_SEQUENCE_TO_REPEAT(SafeCrackData.seqLockSucceed, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK(SafeCrackData.seqLockSucceed)
		
		//Lock Fail
		OPEN_SEQUENCE_TASK(SafeCrackData.seqLockFail)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", GET_RANDOM_SAFE_CRACK_ANIM(SC_ANIM_FAIL), vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_base", vCoord, vRot, SLOW_BLEND_IN, 0.1, GET_RANDOM_INT_IN_RANGE(8000, 20000), AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_heavy_breathe", vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_base", vCoord, vRot, SLOW_BLEND_IN, 0.1, GET_RANDOM_INT_IN_RANGE(8000, 20000), AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, "mini@safe_cracking", "idle_look_around", vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
			SET_SEQUENCE_TO_REPEAT(SafeCrackData.seqLockFail, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK(SafeCrackData.seqLockFail)
		
		NET_PRINT("   --->>>   SAFE CRACK - CREATE_SAFE_CRACK_SEQUENCES - DONE")NET_NL()
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Clears all the Safe Crack Sequences
PROC CLEAR_SAFE_CRACK_SEQUENCES(SAFE_CRACK_STRUCT &SafeCrackData)
	CLEAR_SEQUENCE_TASK(SafeCrackData.seqGetIntoPos)
	CLEAR_SEQUENCE_TASK(SafeCrackData.seqIdleAnim)
	CLEAR_SEQUENCE_TASK(SafeCrackData.seqLockSucceed)
	CLEAR_SEQUENCE_TASK(SafeCrackData.seqLockFail)
ENDPROC

//PURPOSE: Returns TRUE if the player is playing a Safe Crack idle animation
FUNC BOOL IS_PLAYER_PLAYING_IDLE_SAFE_CRACK_ANIM()
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mini@safe_cracking", "idle_base")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mini@safe_cracking", "idle_heavy_breathe")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mini@safe_cracking", "idle_look_around")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Get player into safe cracking position
PROC GET_INTO_SAFE_CRACK_POSITION(SAFE_CRACK_STRUCT &SafeCrackData, OBJECT_INDEX objSafe)
	//VECTOR vTemp
	//VECTOR vSceneHeading
	//vSceneHeading.z = (SafeCrackData.fSafeHeading+90)
	
	//SafeCrackData.vPlayerSafeCrackCoord = GET_ANIM_INITIAL_OFFSET_POSITION("MINI@SAFE_CRACKING", "DOOR_OPEN_SUCCEED_STAND", SafeCrackData.vSafeCoord, vSceneHeading)
	//vTemp = GET_ANIM_INITIAL_OFFSET_ROTATION("MINI@SAFE_CRACKING", "DOOR_OPEN_SUCCEED_STAND", SafeCrackData.vSafeCoord, vSceneHeading) 
	//SafeCrackData.fPlayerSafeCrackHeading = vTemp.z
	
	SET_ENTITY_HEADING(PLAYER_PED_ID(), SafeCrackData.fPlayerSafeCrackHeading)
	SET_ENTITY_COORDS(PLAYER_PED_ID(), SafeCrackData.vPlayerSafeCrackCoord)
	ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), objSafe, -1, SC_ANIM_OFFSET_FROM_SAFE, <<0,0,0>>, DEFAULT, DEFAULT, TRUE)
	
	IF SafeCrackData.seqGetIntoPos != NULL
		TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SafeCrackData.seqGetIntoPos)
	ELSE
		NET_PRINT("   --->>>   SAFE CRACK - SafeCrackData.seqGetIntoPos is not valid, so not performing that sequence right now")NET_NL()
	ENDIF
	
	NET_PRINT("   --->>>   SAFE CRACK - GET_INTO_SAFE_CRACK_POSITION")NET_NL()
ENDPROC

//PURPOSE: Puts the player into a slightly random Idle state
PROC SAFE_CRACK_IDLE_ANIM(SAFE_CRACK_STRUCT &SafeCrackData)
	TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SafeCrackData.seqIdleAnim)
	NET_PRINT("   --->>>   SAFE CRACK - SAFE_CRACK_IDLE_ANIM")NET_NL()
ENDPROC

//PURPOSE: Play a succeed anim (when a lock is cracked)
PROC SAFE_CRACK_LOCK_SUCCEED_ANIM(SAFE_CRACK_STRUCT &SafeCrackData)
	TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SafeCrackData.seqLockSucceed)
	NET_PRINT("   --->>>   SAFE CRACK - SAFE_CRACK_LOCK_SUCCEED_ANIM")NET_NL()
ENDPROC

//PURPOSE: Play a fail anim (when a locks are reset)
PROC SAFE_CRACK_LOCK_FAIL_ANIM(SAFE_CRACK_STRUCT &SafeCrackData)
	TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SafeCrackData.seqLockFail)
	NET_PRINT("   --->>>   SAFE CRACK - SAFE_CRACK_LOCK_FAIL_ANIM")NET_NL()
ENDPROC

//PURPOSE: Play a turn dial anti-clockwise anim
PROC TURN_DIAL_ANTI_SAFE_CRACK_ANIM(OBJECT_INDEX objSafe)
	IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mini@safe_cracking", "dial_turn_anti_fast")
	
		VECTOR vCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objSafe, SC_ANIM_OFFSET_FROM_SAFE)
		VECTOR vRot = <<0,0,GET_ENTITY_HEADING(objSafe)>>
		
		TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), "mini@safe_cracking", "dial_turn_anti_fast", vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
		NET_PRINT("   --->>>   SAFE CRACK - TURN_DIAL_ANTI_SAFE_CRACK_ANIM")NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Play a turn dial clockwise anim
PROC TURN_DIAL_CLOCK_SAFE_CRACK_ANIM(OBJECT_INDEX objSafe)
	IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mini@safe_cracking", "dial_turn_clock_fast")
	
		VECTOR vCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objSafe, SC_ANIM_OFFSET_FROM_SAFE)
		VECTOR vRot = <<0,0,GET_ENTITY_HEADING(objSafe)>>
	
		TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), "mini@safe_cracking", "dial_turn_clock_fast", vCoord, vRot, SLOW_BLEND_IN, 0.1, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
	ENDIF
	NET_PRINT("   --->>>   SAFE CRACK - TURN_DIAL_CLOCK_SAFE_CRACK_ANIM")NET_NL()
ENDPROC

//PURPOSE: Play an anim to exit the Safe Cracking
PROC EXIT_SAFE_CRACK_POSITION_ANIM()
	TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@safe_cracking", "step_out", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
	NET_PRINT("   --->>>   SAFE CRACK - EXIT_SAFE_CRACK_POSITION_ANIM")NET_NL()
ENDPROC

//PURPOSE: Play an anim to pass and exit the Safe Cracking
PROC PASSED_SAFE_CRACK_ANIM(SAFE_CRACK_STRUCT &SafeCrackData, OBJECT_INDEX objSafe, INT &iSceneID)

	IF safeCrackData.iBitSet = 0
	OR iSceneID = 0
	ENDIF
	
	VECTOR vCoord
	VECTOR vRot
	
	IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
		DETACH_ENTITY(PLAYER_PED_ID(), FALSE, FALSE)
	ENDIF

//	vCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objSafe, SC_ANIM_OFFSET_FROM_SAFE)
//	vRot = <<0,0,GET_ENTITY_HEADING(objSafe)>>
//
//	TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), "mini@safe_cracking", "DOOR_OPEN_SUCCEED_STAND", vCoord, vRot, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_HIDE_WEAPON | AF_USE_MOVER_EXTRACTION)
//	PLAY_ENTITY_ANIM(objSafe, "DOOR_OPEN_SUCCEED_STAND_SAFE", "mini@safe_cracking", SLOW_BLEND_IN, FALSE, TRUE, DEFAULT, 0, enum_to_int(AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION))
	
	vRot = <<0,0,GET_ENTITY_HEADING(objSafe)+90>>
	vCoord = GET_ENTITY_COORDS(objSafe) - GET_ANIM_INITIAL_OFFSET_POSITION("mini@safe_cracking", "DOOR_OPEN_SUCCEED_STAND_SAFE", <<0,0,0>>, vRot)
	iSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoord, vRot, EULER_YXZ)	//, TRUE)
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iSceneID, "mini@safe_cracking", "DOOR_OPEN_SUCCEED_STAND", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_PRESERVE_VELOCITY | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_HIDE_WEAPON, RBF_PLAYER_IMPACT)
	IF DOES_ENTITY_EXIST(objSafe)
		NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(objSafe, iSceneID, "mini@safe_cracking", "DOOR_OPEN_SUCCEED_STAND_SAFE", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_PRESERVE_VELOCITY)
	ENDIF
	NETWORK_START_SYNCHRONISED_SCENE(iSceneID)				
	
	NET_PRINT("   --->>>   SAFE CRACK - PASSED_SAFE_CRACK_ANIM")NET_NL()
ENDPROC

//PURPOSE: Maintain Playing an anim to pass and exit the Safe Cracking
PROC MAINTAIN_PASSED_SAFE_CRACK_ANIM(OBJECT_INDEX objSafe, INT iSceneID)
		
	INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iSceneID)
	IF iLocalSceneID != -1
		REQUEST_ANIM_DICT("mini@safe_cracking")
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(objSafe)
			OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(OBJ_TO_NET(objSafe)) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 0.95	//1.0	//
					IF DOES_ENTITY_EXIST(objSafe)
					AND HAS_ANIM_DICT_LOADED("mini@safe_cracking")
						PLAY_ENTITY_ANIM(objSafe, "DOOR_OPEN_SUCCEED_STAND_SAFE", "mini@safe_cracking", NORMAL_BLEND_IN, FALSE, TRUE, FALSE, 0.96, ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))
						NET_PRINT("   --->>>   SAFE CRACK - MAINTAIN_PASSED_SAFE_CRACK_ANIM - ANIM SET") NET_NL()	
					ENDIF
					NETWORK_STOP_SYNCHRONISED_SCENE(iLocalSceneID)
					NET_PRINT("   --->>>   SAFE CRACK - MAINTAIN_PASSED_SAFE_CRACK_ANIM - DONE")NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PROC HIDE_WEAPON_DURING_SAFE_CRACK_OPEN_DOOR_ANIM(SAFE_CRACK_STRUCT &SafeCrackData)
//	IF NOT IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_HIDE_WEAPON_ON_OPEN_DONE)
//		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mini@safe_cracking", "door_open_succeed_stand")
//			SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE)
//			SET_BIT(SafeCrackData.iBitSet, SC_BS_HIDE_WEAPON_ON_OPEN_START)
//			NET_PRINT("   --->>>   SAFE CRACK - HIDE_WEAPON_DURING_SAFE_CRACK_OPEN_DOOR_ANIM - START")NET_NL()
//		ELIF IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_HIDE_WEAPON_ON_OPEN_START)
//			SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
//			SET_BIT(SafeCrackData.iBitSet, SC_BS_HIDE_WEAPON_ON_OPEN_DONE)
//			NET_PRINT("   --->>>   SAFE CRACK - HIDE_WEAPON_DURING_SAFE_CRACK_OPEN_DOOR_ANIM - DONE")NET_NL()
//		ENDIF
//	ENDIF
//ENDPROC

//PURPOSE: Locks the Safe Door into the Open position
FUNC BOOL OPEN_SAFE_CRACK_DOOR(SAFE_CRACK_STRUCT &SafeCrackData)
	//SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_gangsafedoor, SafeCrackData.vSafeDoorProp, TRUE, 0, 0, 1.0)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(SafeCrackData.iSafeDoorEnumHash)
		IF NETWORK_HAS_CONTROL_OF_DOOR(SafeCrackData.iSafeDoorEnumHash)
			DOOR_SYSTEM_SET_DOOR_STATE(SafeCrackData.iSafeDoorEnumHash, DOORSTATE_FORCE_OPEN_THIS_FRAME)
			NET_PRINT("   --->>>   SAFE CRACK - CLOSE_SAFE_CRACK_DOOR - DOOR OPEN")NET_NL()
			RETURN TRUE
		ELSE
			NETWORK_REQUEST_CONTROL_OF_DOOR(SafeCrackData.iSafeDoorEnumHash)
		ENDIF
	ELSE
		NET_PRINT("   --->>>   SAFE CRACK - INITIALISE_SAFE_CRACK - CALLED")NET_NL()
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Locks the Safe Door into the Closed position
FUNC BOOL CLOSE_SAFE_CRACK_DOOR(SAFE_CRACK_STRUCT &SafeCrackData)
	//SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_gangsafedoor, SafeCrackData.vSafeDoorProp, TRUE, 0, 0, 0.0)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(SafeCrackData.iSafeDoorEnumHash)
		IF NETWORK_HAS_CONTROL_OF_DOOR(SafeCrackData.iSafeDoorEnumHash)
			DOOR_SYSTEM_SET_DOOR_STATE(SafeCrackData.iSafeDoorEnumHash, DOORSTATE_FORCE_CLOSED_THIS_FRAME)
			NET_PRINT("   --->>>   SAFE CRACK - CLOSE_SAFE_CRACK_DOOR - DOOR CLOSED")NET_NL()
			RETURN TRUE
		ELSE
			NETWORK_REQUEST_CONTROL_OF_DOOR(SafeCrackData.iSafeDoorEnumHash)
		ENDIF
	ELSE
		NET_PRINT("   --->>>   SAFE CRACK - CLOSE_SAFE_CRACK_DOOR - DOOR NOT REGISTERED")NET_NL()
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns the range at which the dial position can be above/below the Lock Position
FUNC FLOAT GET_SAFE_CRACK_LOCK_RANGE(SAFE_CRACK_STRUCT &SafeCrackData)
	RETURN SafeCrackData.iLockRange
ENDFUNC

//PURPOSE: Returns the minimum range for unlocking
FUNC FLOAT GET_MIN_UNLOCK_RANGE(SAFE_CRACK_STRUCT &SafeCrackData, INT iStage = 0)
	
	//Skip Check
	IF iStage != 0
		IF SafeCrackData.iSafeCrackStage = SafeCrackData.iResetOnLeft
			RETURN TO_FLOAT(SafeCrackData.iLockPosition[SafeCrackData.iSafeCrackStage])
		ENDIF
	ENDIF
	
	FLOAT iRange1 = (SafeCrackData.iLockPosition[SafeCrackData.iSafeCrackStage] - GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData))
	IF iRange1 < 0
		iRange1 = (MAX_DIAL_NUMBERS + iRange1)
	ENDIF
	
	FLOAT iRange2 = (SafeCrackData.iLockPosition[SafeCrackData.iSafeCrackStage] + GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData))
	IF iRange2 >= MAX_DIAL_NUMBERS
		iRange2 = (0 + (iRange2-MAX_DIAL_NUMBERS))
	ENDIF
		
	IF iRange1 < iRange2
		RETURN iRange1 - (DIAL_MOVEMENT_AMOUNT*iStage)
	ELSE
		RETURN iRange2 - (DIAL_MOVEMENT_AMOUNT*iStage)
	ENDIF
ENDFUNC

//PURPOSE: Returns the maximum range for unlocking
FUNC FLOAT GET_MAX_UNLOCK_RANGE(SAFE_CRACK_STRUCT &SafeCrackData, INT iStage = 0)
	
	//Skip Check
	IF iStage != 0
		IF SafeCrackData.iSafeCrackStage = SafeCrackData.iResetOnRight
			RETURN TO_FLOAT(SafeCrackData.iLockPosition[SafeCrackData.iSafeCrackStage])
		ENDIF
	ENDIF
	
	FLOAT iRange1 = (SafeCrackData.iLockPosition[SafeCrackData.iSafeCrackStage] - GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData))
	IF iRange1 < 0
		iRange1 = (MAX_DIAL_NUMBERS + iRange1)
	ENDIF
	
	FLOAT iRange2 = (SafeCrackData.iLockPosition[SafeCrackData.iSafeCrackStage] + GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData))
	IF iRange2 >= MAX_DIAL_NUMBERS
		iRange2 = (0 + (iRange2-MAX_DIAL_NUMBERS))
	ENDIF
	
	IF iRange1 > iRange2
		RETURN iRange1 + (DIAL_MOVEMENT_AMOUNT*iStage)
	ELSE
		RETURN iRange2 + (DIAL_MOVEMENT_AMOUNT*iStage)
	ENDIF
ENDFUNC

////*** SAFE CRACK CAMERA ***////
//PURPOSE: Cleans up the Safe Cracking Camera
PROC CLEAR_SAFE_CRACK_CAMERA(SAFE_CRACK_STRUCT &SafeCrackData)
	IF DOES_CAM_EXIST(SafeCrackData.CameraIndex)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_CAM_ACTIVE(SafeCrackData.CameraIndex, FALSE)
		DESTROY_ALL_CAMS()
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		NET_PRINT("   --->>>   SAFE CRACK - CLEAR_SAFE_CRACK_CAMERA")NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Creates the Safe Cracking Camera
PROC CREATE_SAFE_CRACK_CAMERA(SAFE_CRACK_STRUCT &SafeCrackData)
	IF NOT DOES_CAM_EXIST(SafeCrackData.CameraIndex)
		SafeCrackData.CameraIndex = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
		SET_CAM_ACTIVE(SafeCrackData.CameraIndex, TRUE)
		SET_CAM_PARAMS(SafeCrackData.CameraIndex, SafeCrackData.vCameraPos, SafeCrackData.vCameraRot, SafeCrackData.fCameraFOV)
		POINT_CAM_AT_ENTITY(SafeCrackData.CameraIndex, PLAYER_PED_ID(), <<0.7, 0, 0.15>>)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		NET_PRINT("   --->>>   SAFE CRACK - CREATE_SAFE_CRACK_CAMERA")NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Sets the player to have passed the Safe Crack and resets the Camera, control, etc.
PROC SET_PLAYER_HAS_PASSED_SAFE_CRACK(SAFE_CRACK_STRUCT &SafeCrackData)
	IF IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
		Enable_MP_Comms()
		//OPEN_SAFE_CRACK_DOOR(SafeCrackData)	//NOW DONE BY THE MISSION SCRIPT AS IT HAS TO BE CALLED REPEATEDLY UNTIL OPEN
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_SAFE_CRACK_CAMERA(SafeCrackData)
	ENDIF
	SafeCrackData.iSafeCrackStage = MAX_NUM_LOCKS
	SET_BIT(SafeCrackData.iBitSet, SC_BS_HAS_PLAYER_PASSED_SAFE_CRACK)
	NET_PRINT("   --->>>   SAFE CRACK - SC_BS_HAS_PLAYER_PASSED_SAFE_CRACK - SET")NET_NL()
ENDPROC

//PURPOSE: Clears the Floating Help above the Safe (should be used if somebody else is cracking the safe to make sure other players don't think they can do it)
PROC CLEAR_SAFE_CRACK_FLOATING_HELP()
	//IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("SC_START")
	//	CLEAR_THIS_FLOATING_HELP("SC_START")
	//ENDIF
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SC_START")
		CLEAR_HELP()
	ENDIF
	//CLEAR_BIT(SafeCrackData.iBitSet, SC_START_HELP_DISPLAYED)
ENDPROC

//PURPOSE: Clears Help Text and Context Controller data
PROC CLEAR_SAFE_CRACK_PERSISTING_DATA(SAFE_CRACK_STRUCT &SafeCrackData)
	CLEAR_SAFE_CRACK_FLOATING_HELP()
	//**RELEASE_CONTEXT_INTENTION(SafeCrackData.iStartContextID)
	//**RELEASE_CONTEXT_INTENTION(SafeCrackData.iEndContextID)
	CLEAR_SAFE_CRACK_SEQUENCES(SafeCrackData)
ENDPROC

//PURPOSE: Set Dial Shake Value
PROC SET_DIAL_SHAKE(SAFE_CRACK_STRUCT &SafeCrackData, INT iShakeAmount)
	IF SafeCrackData.iDialShakeTimer.bInitialisedTimer = FALSE	// = 0
		IF SafeCrackData.DialBase.x != 0.500
		OR SafeCrackData.DialBase.y != 0.500
			SafeCrackData.DialBase.x = 0.500
			SafeCrackData.DialBase.y = 0.500
			SafeCrackData.DialMain.x = 0.499
			SafeCrackData.DialMain.y = 0.499
		ELSE
			SafeCrackData.DialBase.x = (0.500 + GET_RANDOM_FLOAT_IN_RANGE(-DIAL_SHAKE_RANGE*iShakeAmount, DIAL_SHAKE_RANGE*iShakeAmount))
			SafeCrackData.DialBase.y = (0.500 + GET_RANDOM_FLOAT_IN_RANGE(-DIAL_SHAKE_RANGE*iShakeAmount, DIAL_SHAKE_RANGE*iShakeAmount))
			SafeCrackData.DialMain.x = (SafeCrackData.DialBase.x-0.001)
			SafeCrackData.DialMain.y = (SafeCrackData.DialBase.y-0.001)
		ENDIF
		START_NET_TIMER(SafeCrackData.iDialShakeTimer)	//SafeCrackData.iDialShakeTimer = (GET_THE_NETWORK_TIMER() + ((DIAL_SHAKE_TIME*5) - (DIAL_SHAKE_TIME*iShakeAmount)))
	ELIF HAS_NET_TIMER_EXPIRED(SafeCrackData.iDialShakeTimer, ((DIAL_SHAKE_TIME*5) - (DIAL_SHAKE_TIME*iShakeAmount)))	//SafeCrackData.iDialShakeTimer < GET_THE_NETWORK_TIMER()
		RESET_NET_TIMER(SafeCrackData.iDialShakeTimer)	//SafeCrackData.iDialShakeTimer = 0
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	BOOL bDebugDraw
#ENDIF

//PURPOSE: Draws the Safe Cracking minigame
PROC DRAW_SAFE_CRACK(SAFE_CRACK_STRUCT &SafeCrackData)

	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_PRESSED(KEY_HOME)
			bDebugDraw = !bDebugDraw
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bDebugDraw)
		ENDIF
		
	#ENDIF


	IF SafeCrackData.iSafeCrackStage < MAX_NUM_LOCKS
		
//		//Set Dial Shake
//		IF SafeCrackData.DialMain.fRotation > GET_MIN_UNLOCK_RANGE(SafeCrackData)
//		AND SafeCrackData.DialMain.fRotation < GET_MAX_UNLOCK_RANGE(SafeCrackData)
//			SET_DIAL_SHAKE(SafeCrackData, 5)
//		ELIF SafeCrackData.DialMain.fRotation > GET_MIN_UNLOCK_RANGE(SafeCrackData, 1) 	//(GET_MIN_UNLOCK_RANGE(SafeCrackData) - DIAL_MOVEMENT_AMOUNT)
//		AND SafeCrackData.DialMain.fRotation < GET_MAX_UNLOCK_RANGE(SafeCrackData, 1)	//(GET_MAX_UNLOCK_RANGE(SafeCrackData) + DIAL_MOVEMENT_AMOUNT)
//			SET_DIAL_SHAKE(SafeCrackData, 4)
//		ELIF SafeCrackData.DialMain.fRotation > GET_MIN_UNLOCK_RANGE(SafeCrackData, 2) 	//(GET_MIN_UNLOCK_RANGE(SafeCrackData) - (DIAL_MOVEMENT_AMOUNT*2))
//		AND SafeCrackData.DialMain.fRotation < GET_MAX_UNLOCK_RANGE(SafeCrackData, 2)	//(GET_MAX_UNLOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT*2))
//			SET_DIAL_SHAKE(SafeCrackData, 3)
//		ELIF SafeCrackData.DialMain.fRotation > GET_MIN_UNLOCK_RANGE(SafeCrackData, 3) 	//(GET_MIN_UNLOCK_RANGE(SafeCrackData) - (DIAL_MOVEMENT_AMOUNT*3))
//		AND SafeCrackData.DialMain.fRotation < GET_MAX_UNLOCK_RANGE(SafeCrackData, 3)	//(GET_MAX_UNLOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT*3))
//			SET_DIAL_SHAKE(SafeCrackData, 2)
//		ELIF SafeCrackData.DialMain.fRotation > GET_MIN_UNLOCK_RANGE(SafeCrackData, 4) 	//(GET_MIN_UNLOCK_RANGE(SafeCrackData) - (DIAL_MOVEMENT_AMOUNT*4))
//		AND SafeCrackData.DialMain.fRotation < GET_MAX_UNLOCK_RANGE(SafeCrackData, 4)	//(GET_MAX_UNLOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT*4))
//			SET_DIAL_SHAKE(SafeCrackData, 1)
		
		//Set Dial Shake
		FLOAT fDiff = ABSF(GET_ANGULAR_DIFFERENCE(SafeCrackData.DialMain.fRotation, TO_FLOAT(SafeCrackData.iLockPosition[SafeCrackData.iSafeCrackStage])))
		//NET_PRINT("   --->>>	DRAW_SAFE_CRACK - absolute angular difference = ") NET_PRINT_FLOAT(fDiff) NET_NL()
		IF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData)
			SET_DIAL_SHAKE(SafeCrackData, 5)
		ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 1)
			SET_DIAL_SHAKE(SafeCrackData, 4)
		ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 2)
			SET_DIAL_SHAKE(SafeCrackData, 3)
		ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 3)
			SET_DIAL_SHAKE(SafeCrackData, 2)
		ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 4)
			SET_DIAL_SHAKE(SafeCrackData, 1)
		//Reset Shake
		ELSE
			IF SafeCrackData.DialBase.x != 0.500
				SafeCrackData.DialBase.x = 0.500
				SafeCrackData.DialMain.x = 0.499
			ENDIF
			IF SafeCrackData.DialBase.y != 0.500
				SafeCrackData.DialBase.y = 0.500
				SafeCrackData.DialMain.y = 0.499
			ENDIF
			IF SafeCrackData.iDialShakeTimer.bInitialisedTimer = TRUE	// != 0
				RESET_NET_TIMER(SafeCrackData.iDialShakeTimer)	//SafeCrackData.iDialShakeTimer = 0
			ENDIF
		ENDIF
		
		DRAW_2D_SPRITE("MPSafeCracking", "Dial_BG", SafeCrackData.DialBase)
		DRAW_2D_SPRITE("MPSafeCracking", "Dial", SafeCrackData.DialMain)
				
		//Checkpoint Counter
		DRAW_ELIMINATION_HUD(MAX_NUM_LOCKS, "SC_CHECK", (SafeCrackData.iSafeCrackStage>0), (SafeCrackData.iSafeCrackStage>1), (SafeCrackData.iSafeCrackStage>2))
		//DRAW_CHECKPOINT_HUD(SafeCrackData.iSafeCrackStage, MAX_NUM_LOCKS, "SC_CHECK", HUD_COLOUR_YELLOW)	//~s~Positions Unlocked
		//DRAW_CHECKPOINT_HUD_AT_POSITION(SafeCrackData.iSafeCrackStage, MAX_NUM_LOCKS, 0.462+0.038, 0.290+0.04)
	ENDIF
ENDPROC

//PURPOSE: Returns the Pitch for the Tumbler sound
FUNC FLOAT GET_SAFE_CRACK_TUMBLER_PITCH(SAFE_CRACK_STRUCT &SafeCrackData)

	FLOAT fDiff = ABSF(GET_ANGULAR_DIFFERENCE(SafeCrackData.DialMain.fRotation, TO_FLOAT(SafeCrackData.iLockPosition[SafeCrackData.iSafeCrackStage])))
	//NET_PRINT("   --->>>	GET_SAFE_CRACK_TUMBLER_PITCH - absolute angular difference = ") NET_PRINT_FLOAT(fDiff) NET_NL()

//	IF SafeCrackData.DialMain.fRotation > GET_MIN_UNLOCK_RANGE(SafeCrackData)
//	AND SafeCrackData.DialMain.fRotation < GET_MAX_UNLOCK_RANGE(SafeCrackData)
	IF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData)
		NET_PRINT("   --->>>   SAFE CRACK - PITCH 5")NET_NL()
		RETURN 1.0
//	ELIF SafeCrackData.DialMain.fRotation > (GET_MIN_UNLOCK_RANGE(SafeCrackData) - DIAL_MOVEMENT_AMOUNT)
//	AND SafeCrackData.DialMain.fRotation < (GET_MAX_UNLOCK_RANGE(SafeCrackData) + DIAL_MOVEMENT_AMOUNT)
	ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 1)
		NET_PRINT("   --->>>   SAFE CRACK - PITCH 4")NET_NL()
		RETURN 0.8
//	ELIF SafeCrackData.DialMain.fRotation > (GET_MIN_UNLOCK_RANGE(SafeCrackData) - (DIAL_MOVEMENT_AMOUNT*2))
//	AND SafeCrackData.DialMain.fRotation < (GET_MAX_UNLOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT*2))
	ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 2)
		NET_PRINT("   --->>>   SAFE CRACK - PITCH 3")NET_NL()
		RETURN 0.6
//	ELIF SafeCrackData.DialMain.fRotation > (GET_MIN_UNLOCK_RANGE(SafeCrackData) - (DIAL_MOVEMENT_AMOUNT*3))
//	AND SafeCrackData.DialMain.fRotation < (GET_MAX_UNLOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT*3))
	ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 3)
		NET_PRINT("   --->>>   SAFE CRACK - PITCH 2")NET_NL()
		RETURN 0.4
//	ELIF SafeCrackData.DialMain.fRotation > (GET_MIN_UNLOCK_RANGE(SafeCrackData) - (DIAL_MOVEMENT_AMOUNT*4))
//	AND SafeCrackData.DialMain.fRotation < (GET_MAX_UNLOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT*4))
	ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 4)
		NET_PRINT("   --->>>   SAFE CRACK - PITCH 1")NET_NL()
		RETURN 0.2
	ENDIF
	
	//NET_PRINT("   --->>>   SAFE CRACK - PITCH 0 - DEFAULT")NET_NL()
	RETURN 0.0
ENDFUNC

//PURPOSE: Plays the passed in sound effect
PROC PLAY_SAFE_CRACK_SOUND(SAFE_CRACK_STRUCT &SafeCrackData, INT iSound)
	SWITCH iSound
		CASE 0
			IF SafeCrackData.iSoundID[iSound] = -1
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ELSE
				STOP_SOUND(SafeCrackData.iSoundID[iSound])
				RELEASE_SOUND_ID(SafeCrackData.iSoundID[iSound])
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ENDIF
			PLAY_SOUND_FRONTEND(SafeCrackData.iSoundID[iSound], "TUMBLER_TURN", "SAFE_CRACK_SOUNDSET")	//SAFE_CRACK_TUMBLER_TURN_MASTER")
			SET_VARIABLE_ON_SOUND(SafeCrackData.iSoundID[iSound], "TUMBLER_IN_ZONE", GET_SAFE_CRACK_TUMBLER_PITCH(SafeCrackData))
			NET_PRINT("   --->>>   SAFE CRACK - PLAY_SAFE_CRACK_SOUND - TUMBLER_TURN")NET_NL()
		BREAK
		
		CASE 1
			IF SafeCrackData.iSoundID[iSound] = -1
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ELSE
				STOP_SOUND(SafeCrackData.iSoundID[iSound])
				RELEASE_SOUND_ID(SafeCrackData.iSoundID[iSound])
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ENDIF
			PLAY_SOUND_FRONTEND(SafeCrackData.iSoundID[iSound], "TUMBLER_RESET", "SAFE_CRACK_SOUNDSET")	//SAFE_CRACK_TUMBLER_RESET_MASTER")
			NET_PRINT("   --->>>   SAFE CRACK - PLAY_SAFE_CRACK_SOUND - TUMBLER_RESET")NET_NL()
		BREAK
		
		CASE 2
			IF SafeCrackData.iSoundID[iSound] = -1
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ELSE
				STOP_SOUND(SafeCrackData.iSoundID[iSound])
				RELEASE_SOUND_ID(SafeCrackData.iSoundID[iSound])
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ENDIF
			PLAY_SOUND_FRONTEND(SafeCrackData.iSoundID[iSound], "TUMBLER_PIN_FALL", "SAFE_CRACK_SOUNDSET")	//SAFE_CRACK_PIN_FALL_MASTER")
			NET_PRINT("   --->>>   SAFE CRACK - PLAY_SAFE_CRACK_SOUND - TUMBLER_PIN_FALL")NET_NL()
		BREAK
		
		CASE 3
			IF SafeCrackData.iSoundID[iSound] = -1
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ELSE
				STOP_SOUND(SafeCrackData.iSoundID[iSound])
				RELEASE_SOUND_ID(SafeCrackData.iSoundID[iSound])
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ENDIF
			PLAY_SOUND_FRONTEND(SafeCrackData.iSoundID[iSound], "TUMBLER_PIN_FALL_FINAL", "SAFE_CRACK_SOUNDSET")	//SAFE_CRACK_FINAL_PIN_FALL_MASTER")
			NET_PRINT("   --->>>   SAFE CRACK - PLAY_SAFE_CRACK_SOUND - TUMBLER_PIN_FALL_FINAL")NET_NL()
		BREAK
		
		CASE 4
			IF SafeCrackData.iSoundID[iSound] = -1
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ELSE
				STOP_SOUND(SafeCrackData.iSoundID[iSound])
				RELEASE_SOUND_ID(SafeCrackData.iSoundID[iSound])
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ENDIF
			PLAY_SOUND_FRONTEND(SafeCrackData.iSoundID[iSound], "SAFE_DOOR_OPEN", "SAFE_CRACK_SOUNDSET")	//SAFE_CRACK_DOOR_OPEN_MASTER")
			NET_PRINT("   --->>>   SAFE CRACK - PLAY_SAFE_CRACK_SOUND - SAFE_DOOR_OPEN")NET_NL()
		BREAK
		
		CASE 5
			IF SafeCrackData.iSoundID[iSound] = -1
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ELSE
				STOP_SOUND(SafeCrackData.iSoundID[iSound])
				RELEASE_SOUND_ID(SafeCrackData.iSoundID[iSound])
				SafeCrackData.iSoundID[iSound] = GET_SOUND_ID()
			ENDIF
			PLAY_SOUND_FRONTEND(SafeCrackData.iSoundID[iSound], "SAFE_DOOR_CLOSE", "SAFE_CRACK_SOUNDSET")	//SAFE_CRACK_DOOR_CLOSE_MASTER")
			NET_PRINT("   --->>>   SAFE CRACK - PLAY_SAFE_CRACK_SOUND - SAFE_DOOR_CLOSE")NET_NL()
		BREAK
		
	ENDSWITCH
ENDPROC

//PURPOSE: Gets random positions for the Safe Locks if needed
PROC SET_RANDOM_SAFE_CRACK_LOCK_POSITIONS(SAFE_CRACK_STRUCT &SafeCrackData)
	//Lock 1
	SafeCrackData.iLockPosition[0] = (GET_RANDOM_INT_IN_RANGE(60, (MAX_DIAL_NUMBERS-60)))
	NET_PRINT("   --->>>   SAFE CRACK - SET_RANDOM_SAFE_CRACK_LOCK_POSITION - RANDOM POSITION FOUND - SafeCrackData.iLockPosition[0] = ")NET_PRINT_INT(SafeCrackData.iLockPosition[0])NET_NL()
	
	//Lock 2 and 3
	IF GET_RANDOM_BOOL()
		SafeCrackData.iLockPosition[1] = (GET_RANDOM_INT_IN_RANGE(SafeCrackData.iLockPosition[0]+45, (SafeCrackData.iLockPosition[0]+155+1)))
		SafeCrackData.iLockPosition[2] = (GET_RANDOM_INT_IN_RANGE(SafeCrackData.iLockPosition[0]-155, (SafeCrackData.iLockPosition[0]-45+1)))
	ELSE
		SafeCrackData.iLockPosition[2] = (GET_RANDOM_INT_IN_RANGE(SafeCrackData.iLockPosition[0]+45, (SafeCrackData.iLockPosition[0]+155+1)))
		SafeCrackData.iLockPosition[1] = (GET_RANDOM_INT_IN_RANGE(SafeCrackData.iLockPosition[0]-155, (SafeCrackData.iLockPosition[0]-45+1)))
	ENDIF
	
	IF SafeCrackData.iLockPosition[1] < 0
		SafeCrackData.iLockPosition[1] = (MAX_DIAL_NUMBERS + SafeCrackData.iLockPosition[1])
	ELIF SafeCrackData.iLockPosition[1] >= MAX_DIAL_NUMBERS
		SafeCrackData.iLockPosition[1] = (SafeCrackData.iLockPosition[1] - MAX_DIAL_NUMBERS)
	ENDIF
	IF SafeCrackData.iLockPosition[2] < 0
		SafeCrackData.iLockPosition[2] = (MAX_DIAL_NUMBERS + SafeCrackData.iLockPosition[2])
	ELIF SafeCrackData.iLockPosition[2] >= MAX_DIAL_NUMBERS
		SafeCrackData.iLockPosition[2] = (SafeCrackData.iLockPosition[2] - MAX_DIAL_NUMBERS)
	ENDIF

	// TEMP TEST - values used to cause the wrap calculation issue in bug 1532316
//	SafeCrackData.iLockPosition[0] = 67
//	SafeCrackData.iLockPosition[1] = 143
//	SafeCrackData.iLockPosition[2] = 358

	NET_PRINT("   --->>>   SAFE CRACK - SET_RANDOM_SAFE_CRACK_LOCK_POSITION - RANDOM POSITION FOUND - SafeCrackData.iLockPosition[1] = ")NET_PRINT_INT(SafeCrackData.iLockPosition[1])NET_NL()
	NET_PRINT("   --->>>   SAFE CRACK - SET_RANDOM_SAFE_CRACK_LOCK_POSITION - RANDOM POSITION FOUND - SafeCrackData.iLockPosition[2] = ")NET_PRINT_INT(SafeCrackData.iLockPosition[2])NET_NL()

//	INT i
//	INT iLockPos
//	REPEAT MAX_NUM_LOCKS i
//		IF SafeCrackData.iLockPosition[i] = 0
//			
//			//IF i = 0
//				iLockPos += (GET_RANDOM_INT_IN_RANGE(60, (MAX_DIAL_NUMBERS-60)))
//			//ELSE
//			//	iLockPos += (SafeCrackData.iLockPosition[i-1] + (GET_RANDOM_INT_IN_RANGE(60, (MAX_DIAL_NUMBERS-60))))
//			//ENDIF
//			
//			NET_PRINT("   --->>>   SAFE CRACK - SET_RANDOM_SAFE_CRACK_LOCK_POSITION - RANDOM POSITION FOUND - A iLockPos = ")NET_PRINT_INT(iLockPos)NET_NL()
//			IF iLockPos < 0
//				iLockPos = (MAX_DIAL_NUMBERS + iLockPos)
//				NET_PRINT("   --->>>   SAFE CRACK - SET_RANDOM_SAFE_CRACK_LOCK_POSITION - RANDOM POSITION FOUND - B iLockPos = ")NET_PRINT_INT(iLockPos)NET_NL()
//			ELIF iLockPos >= MAX_DIAL_NUMBERS
//				iLockPos = (iLockPos - MAX_DIAL_NUMBERS)
//				NET_PRINT("   --->>>   SAFE CRACK - SET_RANDOM_SAFE_CRACK_LOCK_POSITION - RANDOM POSITION FOUND - C iLockPos = ")NET_PRINT_INT(iLockPos)NET_NL()
//			ENDIF
//			
//			//Check Lock 3 isn't near Lock 1
//			IF i = 2
//				IF iLockPos < (SafeCrackData.iLockPosition[0] + 20)
//				AND	iLockPos > (SafeCrackData.iLockPosition[0] - 20)
//					IF iLockPos < SafeCrackData.iLockPosition[0]
//						iLockPos -= 40
//					ELSE
//						iLockPos += 40
//					ENDIF
//					NET_PRINT("   --->>>   SAFE CRACK - SET_RANDOM_SAFE_CRACK_LOCK_POSITION - LOCK 3 NEAR LOCK 1 - NOW iLockPos = ")NET_PRINT_INT(iLockPos)NET_NL()
//				ENDIF
//			ENDIF
//			
//			SafeCrackData.iLockPosition[i] = iLockPos
//			NET_PRINT("   --->>>   SAFE CRACK - SET_RANDOM_SAFE_CRACK_LOCK_POSITION - RANDOM POSITION FOUND - iLockPos = ")NET_PRINT_INT(iLockPos)NET_NL()
//		ELSE
//			iLockPos = SafeCrackData.iLockPosition[i]
//			NET_PRINT("   --->>>   SAFE CRACK - SET_RANDOM_SAFE_CRACK_LOCK_POSITION - POSITION ALREADY SET - iLockPos = ")NET_PRINT_INT(iLockPos)NET_NL()
//		ENDIF
//	ENDREPEAT
ENDPROC

//PURPOSE: Resets the Safe Cracking Data
PROC RESET_SAFE_CRACK( SAFE_CRACK_STRUCT &SafeCrackData, BOOL bSetPlayerControl )
	SafeCrackData.iBitSet = 0
	//SafeCrackData.iNumbers[0] = 19
	//SafeCrackData.iNumbers[1] = 20
	//SafeCrackData.iNumbers[2] = 1
	//SafeCrackData.iNumbers[3] = 2
	//SafeCrackData.iNumbers[4] = 3
	SafeCrackData.iSafeCrackStage = 0
	//SafeCrackData.iLockPosition[0] = 10
	//SafeCrackData.iLockPosition[1] = 5
	//SafeCrackData.iLockPosition[2] = 15
	/*IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("SC_START")
		CLEAR_THIS_FLOATING_HELP("SC_START")
	ENDIF*/
	IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK1", 1)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK2", 2)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK3", 2)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK2", 3)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK3", 3)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK11", 1)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK21", 2)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK31", 2)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK21", 3)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK31", 3)
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SC_START")
		CLEAR_HELP()
	ENDIF
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPEntry")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SAFE_CRACK")
	REMOVE_ANIM_DICT("mini@safe_cracking")
	
	//**RELEASE_CONTEXT_INTENTION(SafeCrackData.iStartContextID)
	//**RELEASE_CONTEXT_INTENTION(SafeCrackData.iEndContextID)
		
	Enable_MP_Comms()
	
	IF bSetPlayerControl
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE, DEFAULT, FALSE)
	ENDIF
	MPGlobals.bPlayerIsSafeCracking = FALSE
	CLEAR_SAFE_CRACK_CAMERA(SafeCrackData)
	
	NET_PRINT("   --->>>   SAFE CRACK - RESET_SAFE_CRACK - CALLED")NET_NL()
ENDPROC

#IF IS_DEBUG_BUILD
//PURPOSE: Creates the Texture Widgets
PROC CREATE_SAFE_CRACK_TEXTURE_WIDGETS(SAFE_CRACK_STRUCT &SafeCrackData)
	IF SafeCrackData.bWidgetsCreated = FALSE
		START_WIDGET_GROUP("Safe Cracking - Textures")
			ADD_WIDGET_INT_SLIDER("DIAL_SHAKE_TIME", DIAL_SHAKE_TIME, 0, 5000, 5)
			ADD_WIDGET_FLOAT_SLIDER("DIAL_SHAKE_RANGE", DIAL_SHAKE_RANGE, 0, 1.0, 0.0001)
			CREATE_A_SPRITE_PLACEMENT_WIDGET(SafeCrackData.DialBase, "Dial Base")
			CREATE_A_SPRITE_PLACEMENT_WIDGET(SafeCrackData.DialMain, "Dial Main")
		STOP_WIDGET_GROUP()
		SafeCrackData.bWidgetsCreated = TRUE
		NET_PRINT("   --->>>   SAFE CRACK - WIDGETS CREATED")NET_NL()
	ENDIF
ENDPROC
#ENDIF

//PURPOSE: Fills in the structs and creates widgets for the Textures
PROC SETUP_SAFE_CRACK_TEXTURE_DATA(SAFE_CRACK_STRUCT &SafeCrackData)
	//Dial Base
	SafeCrackData.DialBase.x 			= 0.500
	SafeCrackData.DialBase.y 			= 0.500
	SafeCrackData.DialBase.w 			= 0.298	//0.320
	SafeCrackData.DialBase.h 			= 0.495	//0.500
	SafeCrackData.DialBase.r			= 255
	SafeCrackData.DialBase.g			= 255
	SafeCrackData.DialBase.b			= 255
	SafeCrackData.DialBase.a			= 255
	SafeCrackData.DialBase.fRotation 	= 0.0
	
	//Dial Main
	SafeCrackData.DialMain.x 			= 0.499
	SafeCrackData.DialMain.y 			= 0.499	//0.498
	SafeCrackData.DialMain.w 			= 0.150	//0.163
	SafeCrackData.DialMain.h 			= 0.250	//0.255
	SafeCrackData.DialMain.r			= 255
	SafeCrackData.DialMain.g			= 255
	SafeCrackData.DialMain.b			= 255
	SafeCrackData.DialMain.a			= 255
	SafeCrackData.DialMain.fRotation 	= 0.0
	
	//#IF IS_DEBUG_BUILD
	//Widgets
	//CREATE_SAFE_CRACK_TEXTURE_WIDGETS(SafeCrackData)
	//#ENDIF
ENDPROC

//PURPOSE: Initialises the Safe Cracking data
PROC INITIALISE_SAFE_CRACK(SAFE_CRACK_STRUCT &SafeCrackData)
	//SafeCrackData.iNumbers[0] = 19
	//SafeCrackData.iNumbers[1] = 20
	//SafeCrackData.iNumbers[2] = 1
	//SafeCrackData.iNumbers[3] = 2
	//SafeCrackData.iNumbers[4] = 3
	SafeCrackData.iSafeCrackStage = 0
	SET_RANDOM_SAFE_CRACK_LOCK_POSITIONS(SafeCrackData)
	IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK1", 1)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK2", 2)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK3", 2)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK2", 3)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK3", 3)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK11", 1)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK21", 2)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK31", 2)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK21", 3)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("SC_LOCK31", 3)
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SC_START")
		CLEAR_HELP()
	ENDIF
	SETUP_SAFE_CRACK_TEXTURE_DATA(SafeCrackData)
	//Clear Sounds
	INT i
	REPEAT 6 i
		IF SafeCrackData.iSoundID[i] > 0
			RELEASE_SOUND_ID(SafeCrackData.iSoundID[i])
		ENDIF
		SafeCrackData.iSoundID[i] = -1
	ENDREPEAT
	NET_PRINT("   --->>>   SAFE CRACK - INITIALISE_SAFE_CRACK - CALLED")NET_NL()
ENDPROC

//PURPOSE: Returns TRUE if the player is in Safe Cracking minigame
FUNC BOOL IS_PLAYER_SAFE_CRACKING(SAFE_CRACK_STRUCT &SafeCrackData)
	IF IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if the player has passed the Safe Cracking minigame
FUNC BOOL HAS_PLAYER_CRACKED_SAFE(SAFE_CRACK_STRUCT &SafeCrackData)
	IF IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_HAS_PLAYER_PASSED_SAFE_CRACK)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: 
FUNC BOOL CAN_PLAYER_START_SAFE_CRACKING()
	//IF IS_POSITION_OCCUPIED(SafeCrackData.vPlayerSafeCrackCoord, 0.75, FALSE, FALSE, TRUE, FALSE, FALSE, PLAYER_PED_ID())
	IF IS_PED_RAGDOLL(PLAYER_PED_ID())
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR IS_PAUSE_MENU_ACTIVE()
	OR IS_PED_IN_COVER(PLAYER_PED_ID())
	OR IS_CUSTOM_MENU_ON_SCREEN()
	OR g_bBrowserVisible
	OR IS_PHONE_ONSCREEN()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Returns True if the player should leave the Safe Cracking
FUNC BOOL HAS_PLAYER_LEFT_SAFE_CRACKING(SAFE_CRACK_STRUCT &SafeCrackData)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON)
	
	//**REGISTER_CONTEXT_INTENTION(SafeCrackData.iEndContextID, CP_MAXIMUM_PRIORITY, "", TRUE)
	
	IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), SafeCrackData.vPlayerSafeCrackCoord, <<0.75, 0.75, 1.5>>, FALSE, TRUE, TM_ON_FOOT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)	//(PAD1, LEFTSHOULDER1)
	//**OR HAS_CONTEXT_BUTTON_TRIGGERED(SafeCrackData.iEndContextID)
	OR IS_PED_RAGDOLL(PLAYER_PED_ID())
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR IS_PAUSE_MENU_ACTIVE()
	OR IS_PED_IN_COVER(PLAYER_PED_ID())
	OR SafeCrackData.bForceExitSafe
	
		IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			DETACH_ENTITY(PLAYER_PED_ID(), FALSE, FALSE)
		ENDIF
	
		//**RELEASE_CONTEXT_INTENTION(SafeCrackData.iEndContextID)
		NET_PRINT("   --->>>   SAFE CRACK - HAS_PLAYER_LEFT_SAFE_CRACKING - TRUE")NET_NL()
		RETURN TRUE	
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Get's the Dial Movement dealy based on the player's Hacking Stat
FUNC INT GET_SAFE_CRACK_DIAL_MOVEMENT_DELAY()
	INT iMovementDelay = (DIAL_MOVEMENT_DELAY_MAX - (ROUND((DIAL_MOVEMENT_DELAY_MAX-DIAL_MOVEMENT_DELAY_MIN)/100)))
	
	IF iMovementDelay > DIAL_MOVEMENT_DELAY_MAX
		iMovementDelay = DIAL_MOVEMENT_DELAY_MAX
	ELIF iMovementDelay < DIAL_MOVEMENT_DELAY_MIN
		iMovementDelay = DIAL_MOVEMENT_DELAY_MIN
	ENDIF
	
	RETURN iMovementDelay
ENDFUNC

//PURPOSE: Moves the Dial by the passed in amount
PROC MOVE_SAFE_CRACK_DIAL(SAFE_CRACK_STRUCT &SafeCrackData, FLOAT iMoveAmount, BOOL bPlaySound = TRUE)
	//INT i
	//INT iNumber
	/*REPEAT MAX_DRAWN_DIAL_NUMBERS i
		iNumber = SafeCrackData.iNumbers[i]
		iNumber += iMoveAmount
		IF iNumber < 1
			iNumber = (MAX_DIAL_NUMBERS - iNumber)
		ELIF iNumber > MAX_DIAL_NUMBERS
			iNumber = (iNumber - MAX_DIAL_NUMBERS)
		ENDIF
		SafeCrackData.iNumbers[i] = iNumber
	ENDREPEAT*/
	
	SafeCrackData.DialMain.fRotation += iMoveAmount
	
	//Cap The Rotation
	/*IF SafeCrackData.DialMain.fRotation < 0
		SafeCrackData.DialMain.fRotation = MAX_DIAL_NUMBERS
	ELIF SafeCrackData.DialMain.fRotation > MAX_DIAL_NUMBERS
		SafeCrackData.DialMain.fRotation = 0
	ENDIF*/
	IF SafeCrackData.DialMain.fRotation < 0
		SafeCrackData.DialMain.fRotation = (MAX_DIAL_NUMBERS + SafeCrackData.DialMain.fRotation)
	ELIF SafeCrackData.DialMain.fRotation >= MAX_DIAL_NUMBERS
		SafeCrackData.DialMain.fRotation = (SafeCrackData.DialMain.fRotation - MAX_DIAL_NUMBERS)
	ENDIF
	
	IF bPlaySound = TRUE
		PLAY_SAFE_CRACK_SOUND(SafeCrackData, 0)
	ENDIF
ENDPROC

//PURPOSE: Controls the unlocking of the current Lock Stage (also performs the pas vibration)
PROC PROCESS_SAFE_CRACK_UNLOCKING(SAFE_CRACK_STRUCT &SafeCrackData)
	IF SafeCrackData.iSafeCrackStage < MAX_NUM_LOCKS
		//IF SafeCrackData.iNumbers[2] = SafeCrackData.iLockPosition[SafeCrackData.iSafeCrackStage]
		
		BOOL bResetUnlockTimer = FALSE
//		IF SafeCrackData.DialMain.fRotation > GET_MIN_UNLOCK_RANGE(SafeCrackData)
//		AND SafeCrackData.DialMain.fRotation < GET_MAX_UNLOCK_RANGE(SafeCrackData)
		FLOAT fDiff = ABSF(GET_ANGULAR_DIFFERENCE(SafeCrackData.DialMain.fRotation, TO_FLOAT(SafeCrackData.iLockPosition[SafeCrackData.iSafeCrackStage])))
		//NET_PRINT("   --->>>	PROCESS_SAFE_CRACK_UNLOCKING - absolute angular difference = ") NET_PRINT_FLOAT(fDiff) NET_NL()
		IF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData)
			IF SafeCrackData.iUnlockTimer.bInitialisedTimer = FALSE	// = 0
				START_NET_TIMER(SafeCrackData.iUnlockTimer)	//SafeCrackData.iUnlockTimer = (GET_THE_NETWORK_TIMER() + SAFE_CRACK_UNLOCK_TIME)//(GET_SAFE_CRACK_DIAL_MOVEMENT_DELAY()*2))
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 180)
				SET_BIT(SafeCrackData.iBitSet, SC_BS_DIAL_IN_CORRECT_POSITION)
				NET_PRINT("   --->>>   SAFE CRACK - Shake 5A")NET_NL()
			ELIF HAS_NET_TIMER_EXPIRED(SafeCrackData.iUnlockTimer, GET_SAFE_CRACK_UNLOCK_TIME(SafeCrackData))	//SafeCrackData.iUnlockTimer < GET_THE_NETWORK_TIMER()
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 200, 250)
				NET_PRINT("   --->>>   SAFE CRACK - Shake 6")NET_NL()
				CLEAR_HELP()
				NET_PRINT("   --->>>   SAFE CRACK - POSITION UNLOCKED - ")NET_PRINT_INT(SafeCrackData.iSafeCrackStage)NET_NL()
				
				//Set Next Directions
				IF SafeCrackData.iSafeCrackStage = 0
					IF IS_BIT_SET(SafeCrackData.iBitSet, SC_LAST_TURNED_LEFT)
						SafeCrackData.iResetOnLeft = 1
						SafeCrackData.iResetOnRight = 2
						PRINTLN("   --->>>   SAFE CRACK - iResetOnLeft = ", SafeCrackData.iResetOnLeft, " iResetOnRight = ", SafeCrackData.iResetOnRight)
					ELSE
						SafeCrackData.iResetOnLeft = 2
						SafeCrackData.iResetOnRight = 1
						PRINTLN("   --->>>   SAFE CRACK - iResetOnLeft = ", SafeCrackData.iResetOnLeft, " iResetOnRight = ", SafeCrackData.iResetOnRight)
					ENDIF
				ENDIF
				
				SafeCrackData.iSafeCrackStage++
				bResetUnlockTimer = TRUE
				//Play Pinfall Sound
				IF SafeCrackData.iSafeCrackStage < MAX_NUM_LOCKS
					PLAY_SAFE_CRACK_SOUND(SafeCrackData, 2)
					SAFE_CRACK_LOCK_SUCCEED_ANIM(SafeCrackData)
				ELSE
					PLAY_SAFE_CRACK_SOUND(SafeCrackData, 3)
					PLAY_SAFE_CRACK_SOUND(SafeCrackData, 4)
				ENDIF
			ELSE
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 180)
				NET_PRINT("   --->>>   SAFE CRACK - Shake 5")NET_NL()
			ENDIF
		ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 1)
//		ELIF SafeCrackData.DialMain.fRotation > GET_MIN_UNLOCK_RANGE(SafeCrackData, 1)	//(GET_MIN_UNLOCK_RANGE(SafeCrackData) - DIAL_MOVEMENT_AMOUNT)
//		AND SafeCrackData.DialMain.fRotation < GET_MAX_UNLOCK_RANGE(SafeCrackData, 1)	//(GET_MAX_UNLOCK_RANGE(SafeCrackData) + DIAL_MOVEMENT_AMOUNT)
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 130)
			bResetUnlockTimer = TRUE
			NET_PRINT("   --->>>   SAFE CRACK - Shake 4")NET_NL()
		ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 2)
//		ELIF SafeCrackData.DialMain.fRotation > GET_MIN_UNLOCK_RANGE(SafeCrackData, 2)	//(GET_MIN_UNLOCK_RANGE(SafeCrackData) - (DIAL_MOVEMENT_AMOUNT*2))
//		AND SafeCrackData.DialMain.fRotation < GET_MAX_UNLOCK_RANGE(SafeCrackData, 2)	//(GET_MAX_UNLOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT*2))
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 112)
			bResetUnlockTimer = TRUE
			NET_PRINT("   --->>>   SAFE CRACK - Shake 3")NET_NL()
		ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 3)
//		ELIF SafeCrackData.DialMain.fRotation > GET_MIN_UNLOCK_RANGE(SafeCrackData, 3)	//(GET_MIN_UNLOCK_RANGE(SafeCrackData) - (DIAL_MOVEMENT_AMOUNT*3))
//		AND SafeCrackData.DialMain.fRotation < GET_MAX_UNLOCK_RANGE(SafeCrackData, 3)	//(GET_MAX_UNLOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT*3))
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 100)
			bResetUnlockTimer = TRUE
			NET_PRINT("   --->>>   SAFE CRACK - Shake 2")NET_NL()
		ELIF fDiff < GET_SAFE_CRACK_LOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT * 4)
//		ELIF SafeCrackData.DialMain.fRotation > GET_MIN_UNLOCK_RANGE(SafeCrackData, 4)	//(GET_MIN_UNLOCK_RANGE(SafeCrackData) - (DIAL_MOVEMENT_AMOUNT*4))
//		AND SafeCrackData.DialMain.fRotation < GET_MAX_UNLOCK_RANGE(SafeCrackData, 4)	//(GET_MAX_UNLOCK_RANGE(SafeCrackData) + (DIAL_MOVEMENT_AMOUNT*4))
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 90)
			bResetUnlockTimer = TRUE
			NET_PRINT("   --->>>   SAFE CRACK - Shake 1")NET_NL()
		ENDIF
		
		IF SafeCrackData.iUnlockTimer.bInitialisedTimer = TRUE	// > 0
			IF bResetUnlockTimer = TRUE
				RESET_NET_TIMER(SafeCrackData.iUnlockTimer)	//SafeCrackData.iUnlockTimer = 0
				CLEAR_BIT(SafeCrackData.iBitSet, SC_BS_DIAL_IN_CORRECT_POSITION)
				NET_PRINT("   --->>>   SAFE CRACK - iUnlockTimer = 0")NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Processes the player using the Safe Cracking Controls
PROC PROCESS_SAFE_CRACK_CONTROLS(SAFE_CRACK_STRUCT &SafeCrackData, OBJECT_INDEX objSafe)
	IF SafeCrackData.iSafeCrackStage < MAX_NUM_LOCKS
		//Display help text
		TEXT_LABEL_15 tlHelpText
		//tlHelpText = "SC_LOCK"
		//tlHelpText += (SafeCrackData.iSafeCrackStage+1)
		IF SafeCrackData.iSafeCrackStage = 0
			tlHelpText = "SC_LOCK1"
		ELIF SafeCrackData.iSafeCrackStage = SafeCrackData.iResetOnLeft
			tlHelpText = "SC_LOCK2"
		ELSE SafeCrackData.iSafeCrackStage = SafeCrackData.iResetOnRight
			tlHelpText = "SC_LOCK3"
		ENDIF
		
		IF GET_PROFILE_SETTING(PROFILE_CONTROLLER_VIBRATION) = 0
			tlHelpText += 1
		ENDIF
		IF NOT IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED(tlHelpText, (SafeCrackData.iSafeCrackStage+1))
			PRINT_HELP_FOREVER_WITH_NUMBER(tlHelpText, (SafeCrackData.iSafeCrackStage+1))	//~p~Position 1~s~ ~n~Use ~PAD_LSTICK_LEFT~ to turn the dial left.~n~Stop when you reach the correct number.~n~Turning the dial the wrong way will reset the safe.
			PRINTLN("   --->>>   SAFE CRACK - HELP TEXT = ", tlHelpText)
		ENDIF
		
		//Get Stick Position
		INT i_left_x	//, i_left_y, i_right_x, i_right_y
		//i_left_x = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) 	//(PAD1, i_left_x, i_left_y, i_right_x, i_right_y)
		//i_left_x = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		i_left_x = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_MOVE_LR)
		
		//CONTROL PRESSED LEFT
		IF i_left_x < 100	//0
			//Restart if on Stage 1
			IF SafeCrackData.iSafeCrackStage = SafeCrackData.iResetOnLeft	//1
				SafeCrackData.iSafeCrackStage = 0
				MOVE_SAFE_CRACK_DIAL(SafeCrackData, DIAL_MOVEMENT_AMOUNT, FALSE)
				RESET_NET_TIMER(SafeCrackData.iMoveDialTimer)	// = 0
				SafeCrackData.iMoveDialTimerDelay = GET_SAFE_CRACK_DIAL_MOVEMENT_DELAY()
				PLAY_SAFE_CRACK_SOUND(SafeCrackData, 1)
				SAFE_CRACK_LOCK_FAIL_ANIM(SafeCrackData)
				RESET_NET_TIMER(SafeCrackData.iUnlockTimer)	//SafeCrackData.iUnlockTimer = 0
				SET_BIT(SafeCrackData.iBitSet, SC_LAST_TURNED_LEFT)
				CLEAR_BIT(SafeCrackData.iBitSet, SC_BS_DIAL_IN_CORRECT_POSITION)
				NET_PRINT("   --->>>   SAFE CRACK - RESTART SAFE - 1")NET_NL()
			//Move The Dial
			ELSE
				//IF SafeCrackData.iMoveDialTimer = 0
				//	SafeCrackData.iMoveDialTimer = (GET_THE_NETWORK_TIMER() + GET_SAFE_CRACK_DIAL_MOVEMENT_DELAY())
				IF HAS_NET_TIMER_EXPIRED(SafeCrackData.iMoveDialTimer, SafeCrackData.iMoveDialTimerDelay) //< GET_THE_NETWORK_TIMER()
					TURN_DIAL_CLOCK_SAFE_CRACK_ANIM(objSafe)
					MOVE_SAFE_CRACK_DIAL(SafeCrackData, DIAL_MOVEMENT_AMOUNT)
					RESET_NET_TIMER(SafeCrackData.iMoveDialTimer)	//SafeCrackData.iMoveDialTimer = 0
					SafeCrackData.iMoveDialTimerDelay = GET_SAFE_CRACK_DIAL_MOVEMENT_DELAY()
					SET_BIT(SafeCrackData.iBitSet, SC_LAST_TURNED_LEFT)
					NET_PRINT("   --->>>   SAFE CRACK - Dial Moved Left")NET_NL()
				ENDIF
			ENDIF
			
		//CONTROL PRESSED RIGHT
		ELIF i_left_x > 160	//0
			//Restart if on Stage 2
			IF SafeCrackData.iSafeCrackStage = SafeCrackData.iResetOnRight	//2
				SafeCrackData.iSafeCrackStage = 0
				MOVE_SAFE_CRACK_DIAL(SafeCrackData, DIAL_MOVEMENT_AMOUNT, FALSE)
				RESET_NET_TIMER(SafeCrackData.iMoveDialTimer)	//SafeCrackData.iMoveDialTimer = 0
				SafeCrackData.iMoveDialTimerDelay = GET_SAFE_CRACK_DIAL_MOVEMENT_DELAY()
				PLAY_SAFE_CRACK_SOUND(SafeCrackData, 1)
				SAFE_CRACK_LOCK_FAIL_ANIM(SafeCrackData)
				RESET_NET_TIMER(SafeCrackData.iUnlockTimer)	//SafeCrackData.iUnlockTimer = 0
				CLEAR_BIT(SafeCrackData.iBitSet, SC_LAST_TURNED_LEFT)
				CLEAR_BIT(SafeCrackData.iBitSet, SC_BS_DIAL_IN_CORRECT_POSITION)
				NET_PRINT("   --->>>   SAFE CRACK - RESTART SAFE - 2")NET_NL()
			//Move The Dial
			ELSE
				//IF SafeCrackData.iMoveDialTimer = 0
				//	SafeCrackData.iMoveDialTimer = (GET_THE_NETWORK_TIMER() + GET_SAFE_CRACK_DIAL_MOVEMENT_DELAY())
				IF HAS_NET_TIMER_EXPIRED(SafeCrackData.iMoveDialTimer, SafeCrackData.iMoveDialTimerDelay) //SafeCrackData.iMoveDialTimer < GET_THE_NETWORK_TIMER()
					TURN_DIAL_ANTI_SAFE_CRACK_ANIM(objSafe)
					MOVE_SAFE_CRACK_DIAL(SafeCrackData, -DIAL_MOVEMENT_AMOUNT)
					RESET_NET_TIMER(SafeCrackData.iMoveDialTimer)	//SafeCrackData.iMoveDialTimer = 0
					SafeCrackData.iMoveDialTimerDelay = GET_SAFE_CRACK_DIAL_MOVEMENT_DELAY()
					CLEAR_BIT(SafeCrackData.iBitSet, SC_LAST_TURNED_LEFT)
					NET_PRINT("   --->>>   SAFE CRACK - Dial Moved Right")NET_NL()
				ENDIF
			ENDIF
			
		//CONTROL NOT PRESSED
		ELSE
			//Reset Move Dial Timer
			//IF SafeCrackData.iMoveDialTimer > 0
			//	SafeCrackData.iMoveDialTimer = 0
			//	NET_PRINT("   --->>>   SAFE CRACK - iMoveDialTimer = 0")NET_NL()
			//ENDIF
			IF NOT IS_PLAYER_PLAYING_IDLE_SAFE_CRACK_ANIM()
				SAFE_CRACK_IDLE_ANIM(SafeCrackData)
			ENDIF
			IF SafeCrackData.iMoveDialTimerDelay != -1//SafeCrackData.iMoveDialTimer != 9999
				SafeCrackData.iMoveDialTimerDelay = -1	//9999
				NET_PRINT("   --->>>   SAFE CRACK - iMoveDialTimer = -1")NET_NL()
			ENDIF
		ENDIF
		
		//Check if player has passed this stage
		PROCESS_SAFE_CRACK_UNLOCKING(SafeCrackData)
	ENDIF
ENDPROC

//PURPOSE: Processes the Safe Cracking minigame
PROC RUN_SAFE_CRACK_MINIGAME(SAFE_CRACK_STRUCT &SafeCrackData, OBJECT_INDEX objSafe)
	IF IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
		IF NOT IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_IN_POSITION)
			GET_INTO_SAFE_CRACK_POSITION(SafeCrackData, objSafe)
			
			INITIALISE_SAFE_CRACK(SafeCrackData)
			
			Disable_MP_Comms()
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_ALLOW_PLAYER_DAMAGE|SPC_ALLOW_PAD_SHAKE|SPC_PREVENT_EVERYBODY_BACKOFF)
			SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE, DEFAULT, FALSE)
			CLEAR_AREA_OF_OBJECTS(GET_PLAYER_COORDS(PLAYER_ID()), 1.0)//, CLEAROBJ_FLAG_BROADCAST)
			
			CREATE_SAFE_CRACK_CAMERA(SafeCrackData)
			DISABLE_SELECTOR_THIS_FRAME()
			
			MPGlobals.bPlayerIsSafeCracking = TRUE
			SET_BIT(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_IN_POSITION)
			NET_PRINT("   --->>>   SAFE CRACK - PLAYER GIVEN GET IN POSITION SEQUENCE")NET_NL()
		ELSE
			
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_UseKinematicPhysics, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ExpandPedCapsuleFromSkeleton, TRUE)
			
			DISABLE_SELECTOR_THIS_FRAME()
			
			PROCESS_SAFE_CRACK_CONTROLS(SafeCrackData, objSafe)
			
			DRAW_SAFE_CRACK(SafeCrackData)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Forces the player to end Safe Cracking if they are flagged as doing it
PROC FORCE_EXIT_SAFE_CRACKING(SAFE_CRACK_STRUCT &SafeCrackData)
	IF IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
		EXIT_SAFE_CRACK_POSITION_ANIM()
		RESET_SAFE_CRACK( SafeCrackData, TRUE )
		NET_PRINT("   --->>>   SAFE CRACK - FORCE_EXIT_SAFE_CRACKING - CALLED")NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Processes the Safe Cracking minigame
PROC PROCESS_PLAYER_SAFE_CRACKING(SAFE_CRACK_STRUCT &SafeCrackData, OBJECT_INDEX objSafe, INT &iSceneID)
	//Check if Player is near Safe
	//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), SafeCrackData.vSafeCoord, <<4, 4, 1.5>>, FALSE, TRUE, TM_ON_FOOT)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), SafeCrackData.vFloatingHelpMin, SafeCrackData.vFloatingHelpMax, SafeCrackData.fFloatingHelpWidth, FALSE, TRUE, TM_ON_FOOT)
		REQUEST_STREAMED_TEXTURE_DICT("MPSafeCracking")
		REQUEST_ANIM_DICT("mini@safe_cracking")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPSafeCracking")
		AND REQUEST_AMBIENT_AUDIO_BANK("SAFE_CRACK")
		AND HAS_ANIM_DICT_LOADED("mini@safe_cracking")
			//Player is not yet Safe Cracking
			IF NOT IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
				IF CAN_PLAYER_START_SAFE_CRACKING()
					
					//**REGISTER_CONTEXT_INTENTION(SafeCrackData.iStartContextID, CP_HIGH_PRIORITY, "", TRUE)
					
					//Check if Player is at the Safe

					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), SafeCrackData.vPlayerSafeCrackCoord, <<0.75, 0.75, 1.5>>, FALSE, TRUE, TM_ON_FOOT)
						
						//IF NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("SC_START")
						IF NOT IS_BIT_SET(SafeCrackData.iBitSet, SC_START_HELP_DISPLAYED)
							IF NOT IS_PHONE_ONSCREEN()
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									//HELP_AT_LOCATION("SC_START", GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<-0.50, 0, 1.75>>), HELP_TEXT_SOUTH, -1, FLOATING_HELP_MISSION_SLOT)	//~s~Press ~PAD_LB~ to start safe cracking.
									PRINT_HELP("SC_START")
									SET_BIT(SafeCrackData.iBitSet, SC_START_HELP_DISPLAYED)
								ENDIF
							ENDIF
						ELSE
							IF IS_PHONE_ONSCREEN()
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SC_START")
									CLEAR_HELP()
									CLEAR_BIT(SafeCrackData.iBitSet, SC_START_HELP_DISPLAYED)
									NET_PRINT("   --->>>   SAFE CRACK - PHONE ONSCREEN - CLEARING HELP")NET_NL()
								ENDIF
							ENDIF
						ENDIF
						
						//DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON)
						IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)	//(PAD1, LEFTSHOULDER1)
						//**IF HAS_CONTEXT_BUTTON_TRIGGERED(SafeCrackData.iStartContextID)
							
							//IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("SC_START")
							//	CLEAR_THIS_FLOATING_HELP("SC_START")
							//ENDIF
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SC_START")
								CLEAR_HELP()
							ENDIF
							CLEAR_BIT(SafeCrackData.iBitSet, SC_START_HELP_DISPLAYED)
							//**RELEASE_CONTEXT_INTENTION(SafeCrackData.iStartContextID)
							SET_BIT(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
							//CLOSE_SAFE_CRACK_DOOR(SafeCrackData)
							NET_PRINT("   --->>>   SAFE CRACK - SC_BS_IS_PLAYER_SAFE_CRACKING - SET")NET_NL()
						ENDIF
					ELSE
						//IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("SC_START")
						//	CLEAR_THIS_FLOATING_HELP("SC_START")
						//ENDIF
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SC_START")
							CLEAR_HELP()
						ENDIF
						CLEAR_BIT(SafeCrackData.iBitSet, SC_START_HELP_DISPLAYED)
						//**RELEASE_CONTEXT_INTENTION(SafeCrackData.iStartContextID)
						//**RELEASE_CONTEXT_INTENTION(SafeCrackData.iEndContextID)
					ENDIF
				ELSE
					//IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("SC_START")
					//	CLEAR_THIS_FLOATING_HELP("SC_START")
					//ENDIF
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SC_START")
						CLEAR_HELP()
					ENDIF
					CLEAR_BIT(SafeCrackData.iBitSet, SC_START_HELP_DISPLAYED)
					//**RELEASE_CONTEXT_INTENTION(SafeCrackData.iStartContextID)
					//**RELEASE_CONTEXT_INTENTION(SafeCrackData.iEndContextID)
				ENDIF
				
			//Player is Safe Cracking
			ELSE
				
				IF SafeCrackData.iSafeCrackStage < MAX_NUM_LOCKS
					//Check that player hasn't left the Safe
					IF HAS_PLAYER_LEFT_SAFE_CRACKING(SafeCrackData)
						EXIT_SAFE_CRACK_POSITION_ANIM()
						RESET_SAFE_CRACK( SafeCrackData, TRUE )
						IF SafeCrackData.bForceExitSafe
							SafeCrackData.bForceExitSafe = FALSE
						ENDIF
					ENDIF
					
					RUN_SAFE_CRACK_MINIGAME(SafeCrackData, objSafe)
				ELSE
					IF NOT IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_HAS_PLAYER_PASSED_SAFE_CRACK)
						/*Enable_MP_Comms()
						PASSED_SAFE_CRACK_ANIM()
						OPEN_SAFE_CRACK_DOOR(SafeCrackData)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
						CLEAR_SAFE_CRACK_CAMERA(SafeCrackData)
						SET_BIT(SafeCrackData.iBitSet, SC_BS_HAS_PLAYER_PASSED_SAFE_CRACK)
						NET_PRINT("   --->>>   SAFE CRACK - SC_BS_HAS_PLAYER_PASSED_SAFE_CRACK - SET")NET_NL()*/
						PASSED_SAFE_CRACK_ANIM(SafeCrackData, objSafe, iSceneID)
						SET_PLAYER_HAS_PASSED_SAFE_CRACK(SafeCrackData)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			NET_PRINT("   --->>>   SAFE CRACK - ANIMS, TEXTURES AND SOUNDS NOT LOADED - MPSafeCracking")NET_NL()
		ENDIF
	ELSE
		IF IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
			RESET_SAFE_CRACK(SafeCrackData, TRUE )
		ENDIF
	ENDIF
ENDPROC
