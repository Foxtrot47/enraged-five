//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        mp_submarine_periscope.sch																			//
// Description: This is implementation of the submarines' periscope. Consists of both private and public API		//
//				for other scripts to use if a periscope is needed to be implemented.								//
// Written by:  Joe Davis																							//
// Date:  		22/07/2020																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "mp_globals.sch"
USING "script_network.sch"
USING "commands_camera.sch"
USING "commands_hud.sch"
USING "net_blips.sch"
USING "context_control_public.sch"
USING "mp_submarine_weapons.sch"
USING "rc_helper_functions.sch"
USING "net_simple_interior_submarine.sch"

// ***************************************************************************************
//
// 					CONSTANTS
//
// ***************************************************************************************

// sPeriscopeData.iBS
CONST_INT BS_LOCAL_PLAYER_IS_USING_PERISCOPE		0
CONST_INT BS_REMOTE_PLAYER_IS_USING_PERISCOPE		1
CONST_INT BS_ENABLE_PERISCOPE_CAM					2
CONST_INT BS_SET_PERISCOPE_UNAVAILABLE				3
CONST_INT BS_IMMEDIATELY_ENTERED_PERISCOPE			4
CONST_INT BS_KICKED_OUT_OF_PERISCOPE				5
CONST_INT BS_IMMEDIATELY_ENTERING_PERISCOPE			6
CONST_INT BS_PLAYER_CAN_BE_KICKED_OUT				7

CONST_INT PERISCOPE_SHOW_TIP_COUNT					3

CONST_INT PERISCOPE_MAX_TARGETS						1
CONST_INT PERISCOPE_LOCK_ON_TIME					1000
CONST_INT PERISCOPE_LOCK_COOLDOWN_TIME				1500
CONST_FLOAT PERISCOPE_MAX_LOCK_DISTANCE_WS			500.0 	//Max world space distance (from camera)
CONST_FLOAT PERISCOPE_MAX_LOCK_RADIUS_SS			0.1		//Max screen space distance (from center)
CONST_FLOAT PERISCOPE_LOCK_BREAK_RADIUS_SS			0.3		//Max screen space distance (from center)

CONST_INT PERISCOPE_SCOPE_ALPHA 					200

// ***************************************************************************************
// 					ENUMS
// ***************************************************************************************

ENUM PERISCOPE_TRIGGER_STAGE
	PTS_PLAYER_OUT_OF_RANGE,
	PTS_INIT_FOR_USE,
	PTS_WAIT_FOR_PLAYER_ENTER,
	PTS_START_ANIM_ENTRY,
	PTS_ENTER_SCOPE_CAM,
	PTS_UPDATE_SCOPE_CAM,
	PTS_EXIT_SCOPE_CAM,
	PTS_START_ANIM_EXIT,
	PTS_BAIL_OUT,
	PTS_CLEANUP
ENDENUM

ENUM PERISCOPE_ANIM_STAGE
	PAS_DEFAULT,
	PAS_START_WALKING,
	PAS_WALKING,
	PAS_ENTER,
	PAS_IDLE,
	PAS_IDLING,
	PAS_EXIT,
	PAS_EXITING
ENDENUM

ENUM PERISCOPE_LOCK_STATE
	LS_DEFAULT,
	LS_LOCKING,
	LS_LOCKED
ENDENUM

FUNC STRING LOCK_STATE_AS_STRING(PERISCOPE_LOCK_STATE State)
	SWITCH State
		CASE LS_DEFAULT RETURN "LS_DEFAULT"
		CASE LS_LOCKING RETURN "LS_LOCKING"
		CASE LS_LOCKED RETURN "LS_LOCKED"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

ENUM PERISCOPE_KICK_OUT_REASON
	KICK_DEFAULT,
	KICK_SUB_TOO_DEEP,
	KICK_USED_BY_REMOTE_PLAYER,
	KICK_SMPL_INT_REQUEST
ENDENUM

FUNC STRING KICK_OUT_REASON_AS_STRING(PERISCOPE_KICK_OUT_REASON State)
	SWITCH State
		CASE KICK_DEFAULT RETURN "KICK_DEFAULT"
		CASE KICK_SUB_TOO_DEEP RETURN "KICK_SUB_TOO_DEEP"
		CASE KICK_USED_BY_REMOTE_PLAYER RETURN "KICK_USED_BY_REMOTE_PLAYER"
		CASE KICK_SMPL_INT_REQUEST	RETURN "KICK_SMPL_INT_REQUEST"
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC


// ***************************************************************************************
// 					STRUCTS
// ***************************************************************************************

STRUCT PERISCOPE_CAM_DATA

	CAMERA_INDEX cameraID
	
	VECTOR vPrevRot
	VECTOR vRot
	FLOAT fFOV
	
	FLOAT fStickSpeed
	FLOAT fCursorSpeed
	
	FLOAT fMinPitch
	FLOAT fMaxPitch
	
	FLOAT fCurrentRotSpeed // 0 - 1
	
	FLOAT fMaxDepth

ENDSTRUCT

STRUCT PERISCOPE_TEXTURE_DATA
	
	//Dictionary the Textures are stored in
	TEXT_LABEL_31 Dictionary
	
	TEXT_LABEL_31 ScopeOverlay
	TEXT_LABEL_31 CrosshairLine
	TEXT_LABEL_31 Bars
	TEXT_LABEL_31 Numbers
	TEXT_LABEL_31 Target_Default
	TEXT_LABEL_31 Target_LockingOn
	TEXT_LABEL_31 Target_LockedOn
	
	VECTOR ScopeOverlay_Resolution
	VECTOR CrosshairLine_Resolution
	VECTOR Bars_Resolution
	VECTOR Numbers_Resolution
	VECTOR Target_Default_Resolution
	VECTOR Target_LockingOn_Resolution
	VECTOR Target_LockedOn_Resolution
	
	INT Numbers_SpritesX = 6
	INT Numbers_SpritesY = 6
	
ENDSTRUCT

STRUCT PERISCOPE_LOCK_TARGET_DATA
	ENTITY_INDEX Entity = null
	VECTOR Offset
	PERISCOPE_LOCK_STATE LockState = LS_DEFAULT
	FLOAT FlashTick
ENDSTRUCT

STRUCT PERISCOPE_TARGETING_DATA
	
	PERISCOPE_LOCK_TARGET_DATA PendingLockTarget
	
	SCRIPT_TIMER LockOnTimer
	SCRIPT_TIMER LockCooldownTimer
	
	BOOL bLockOnRequiresControlRelease
	BOOL bFireRequiresControlRelease
	
	BOOL bIsFiring
	
	PERISCOPE_LOCK_TARGET_DATA LockedTargets[PERISCOPE_MAX_TARGETS]
	INT iLockTargetCount = 0
	
	INT iLockingSoundId = -1
	INT iLockedSoundId = -1

ENDSTRUCT

STRUCT PERISCOPE_ANIM_DATA
	VECTOR vPos
	VECTOR vRot
	
	TEXT_LABEL_63 tlDictionary
	
	TEXT_LABEL_23 tlClipPed
	TEXT_LABEL_23 tlClipPeriscope
	
	INT iSyncSceneID
	INT iPropSceneID = -1
	INT iPeriscopeRestingAnimState
ENDSTRUCT

STRUCT PERISCOPE_INSTRUCTIONAL_BUTTONS

	SCALEFORM_INDEX ScaleformIndex
	SCALEFORM_INSTRUCTIONAL_BUTTONS sButtons
	SPRITE_PLACEMENT sPlacement

ENDSTRUCT

STRUCT SERVER_PERISCOPE_BD_DATA
	NETWORK_INDEX niPeriscopeProp
	INT iplayerUsingPeriscope = -1
ENDSTRUCT

STRUCT PLAYER_PERISCOPE_BD_DATA
	INT iBS
ENDSTRUCT

STRUCT PERISCOPE_DATA

	// Enums
	PERISCOPE_TRIGGER_STAGE eStage 				= PTS_PLAYER_OUT_OF_RANGE
	PERISCOPE_ANIM_STAGE eAnimStage 			= PAS_DEFAULT
	PERISCOPE_KICK_OUT_REASON eKickOutReason 	= KICK_DEFAULT
	
	// Structs
	PERISCOPE_CAM_DATA sCamData
	PERISCOPE_TEXTURE_DATA sTextures
	PERISCOPE_TARGETING_DATA sTargetingData
	PERISCOPE_ANIM_DATA sAnimData
	PERISCOPE_INSTRUCTIONAL_BUTTONS sInstructButtons
	SCRIPT_TIMER sTimer
	
	SERVER_PERISCOPE_BD_DATA serverBD
	PLAYER_PERISCOPE_BD_DATA playerBD[NUM_NETWORK_PLAYERS]
	
	// Props
	MODEL_NAMES ePeriscopeModel
	OBJECT_INDEX oiPeriscope
	
	// Bit Sets
	INT iBS
	
	// Button Context
	INT iContextButtonID = NEW_CONTEXT_INTENTION
	
	// Audio
	INT iBGLoopSoundID = -1
	
	// Prompt / Help Text
	VECTOR vPromptPos
	VECTOR vPromptDimentions
	
	//Texture data
	VECTOR fScopeOverlayResolution
	
	// Submarine
	VEHICLE_INDEX vehSubmarineIAmIn
	
ENDSTRUCT

// ***************************************************************************************
//
// 					PRIVATE DEBUG
//
// ***************************************************************************************

#IF IS_DEBUG_BUILD

BOOL bDebugDisplayCamValues
BOOL bDebugDisplayTargetingValues
BOOL bDebugEnterScope
BOOL bDebugTestBailOut
BOOL bDebugDisablePeriscope
BOOL bDebugHideInputPrompts
BOOL bDebugDisplayPeriscopeDepthValues
BOOL bDebugDisplayAnimValues

BOOL bDebugDontRenderPeriscopeCam = FALSE
BOOL bDebugHideScope = FALSE
BOOL bDebugHideScopeCrosshair = FALSE
BOOL bDebugHideScopeBars = FALSE
BOOL bDebugHideScopeBarNumbers = FALSE
BOOL bDebugImmediatelyEnterPeriscope = FALSE

BOOL bDebugUpdateAnimPos

INT iDebugScopeAlpha = PERISCOPE_SCOPE_ALPHA

FLOAT fDebugTargetSmoothDampSpeed
FLOAT fDebugTargetErrorRadius

VECTOR vDebugDepthTextPos 		= <<0.01, 0.60, 0.0>>
VECTOR vDebugDepthTextOffset 	= <<0.0, 0.04, 0.0>>
FLOAT fDebugDepthTextScale		= 0.6

/// PURPOSE:
///    Convert the constants value to it's name.
/// PARAMS:
///    iBit - The bit / const_int
/// RETURNS:
///    Name of bit.
FUNC STRING _DEBUG_PERISCOPE__GET_BIT_NAME(INT iBit)
	SWITCH iBit
		CASE 0		RETURN "BS_LOCAL_PLAYER_IS_USING_PERISCOPE"
		CASE 1		RETURN "BS_REMOTE_PLAYER_IS_USING_PERISCOPE"
		CASE 2		RETURN "BS_ENABLE_PERISCOPE_CAM"
		CASE 3		RETURN "BS_SET_PERISCOPE_UNAVAILABLE"
		CASE 4		RETURN "BS_IMMEDIATELY_ENTERED_PERISCOPE"
		CASE 5		RETURN "BS_KICKED_OUT_OF_PERISCOPE"
		CASE 6		RETURN "BS_IMMEDIATELY_ENTERING_PERISCOPE"
		CASE 7		RETURN "BS_PLAYER_CAN_BE_KICKED_OUT"
	ENDSWITCH
	
	RETURN "NULL"
ENDFUNC

/// PURPOSE:
///    Displays camera stats on screen. Useful for fine tweaking movement.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _DEBUG_PERISCOPE__DRAW_CAM_VALUES(PERISCOPE_DATA &sPeriscopeData)
	
	IF DOES_CAM_EXIST(sPeriscopeData.sCamData.cameraID)
	AND IS_CAM_ACTIVE(sPeriscopeData.sCamData.cameraID)
	
		// Debug values for movement.
		IF (bDebugDisplayCamValues)
			TEXT_LABEL_63 tlText
			VECTOR vPos = <<0.1, 0.1, 0.0>>
			VECTOR vOffset = << 0.0, 0.02, 0.0>>
			
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
			tlText = "Cam Pos: "
			tlText += GET_STRING_FROM_VECTOR(GET_CAM_COORD(sPeriscopeData.sCamData.cameraID))
			DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
			
			vPos += vOffset
			tlText = "Cam Rot: "
			tlText += GET_STRING_FROM_VECTOR(sPeriscopeData.sCamData.vRot)
			DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
			
			vPos += vOffset
			tlText = "Cam FOV: "
			tlText += GET_STRING_FROM_FLOAT(sPeriscopeData.sCamData.fFOV)
			DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
			
			vPos += vOffset
			tlText = "Stick Speed: "
			tlText += GET_STRING_FROM_FLOAT(sPeriscopeData.sCamData.fStickSpeed)
			DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
			
			vPos += vOffset
			tlText = "Cursor Speed: "
			tlText += GET_STRING_FROM_FLOAT(sPeriscopeData.sCamData.fCursorSpeed)
			DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
			
			vPos += vOffset
			tlText = "Pitch Range: "
			tlText += GET_STRING_FROM_FLOAT(sPeriscopeData.sCamData.fMinPitch)
			tlText += " -> "
			tlText += GET_STRING_FROM_FLOAT(sPeriscopeData.sCamData.fMaxPitch)
			DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
			
			vPos += vOffset
			tlText = "g_fMenuCursorXMoveDistance: "
			tlText += GET_STRING_FROM_FLOAT(g_fMenuCursorXMoveDistance)
			DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
			
			vPos += vOffset
			tlText = "g_fMenuCursorYMoveDistance: "
			tlText += GET_STRING_FROM_FLOAT(g_fMenuCursorYMoveDistance)
			DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
			
			vPos += vOffset
			tlText = "Turning Sound Value (0-1): "
			tlText += GET_STRING_FROM_FLOAT(sPeriscopeData.sCamData.fCurrentRotSpeed)
			DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
			
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				FLOAT fMouseY = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD)
				FLOAT fMouseX = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR)
	
				vPos += vOffset
				tlText = "fMouseX: "
				tlText += GET_STRING_FROM_FLOAT(fMouseX)
				DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
				
				vPos += vOffset
				tlText = "fMouseY: "
				tlText += GET_STRING_FROM_FLOAT(fMouseY)
				DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
			ENDIF
		ENDIF
		
	ENDIF
	
	// Debug text for production to determine periscope depth.
	IF (bDebugDisplayPeriscopeDepthValues)
		VECTOR _vDebugDepthTextPos 		= vDebugDepthTextPos
		VECTOR _vDebugDepthTextOffset 	= vDebugDepthTextOffset
		
		TEXT_LABEL_31 tlPeriscope = "Periscope Depth"
		SET_TEXT_COLOUR(0, 255, 0, 255)
		SET_TEXT_SCALE(fDebugDepthTextScale, fDebugDepthTextScale)
		DISPLAY_TEXT_WITH_LITERAL_STRING(_vDebugDepthTextPos.x, _vDebugDepthTextPos.y, "STRING", tlPeriscope)
		
		_vDebugDepthTextPos += _vDebugDepthTextOffset
		
		IF IS_ENTITY_ALIVE(sPeriscopeData.vehSubmarineIAmIn)
			VECTOR vSubPos = GET_ENTITY_COORDS(sPeriscopeData.vehSubmarineIAmIn)
			
			TEXT_LABEL_31 tlCurrentDepth = "Current Depth: "
			
			IF (vSubPos.z >= 0.0)
				tlCurrentDepth += GET_STRING_FROM_INT(0)
				tlCurrentDepth += "m"
			ELSE
				FLOAT fTempDepthValue = vSubPos.z
				fTempDepthValue *= -1 // Treating depth as a positive value below the water surface.

				tlCurrentDepth += GET_STRING_FROM_FLOAT(fTempDepthValue, 2)
				tlCurrentDepth += "m"
			ENDIF
			
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_SCALE(fDebugDepthTextScale, fDebugDepthTextScale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(_vDebugDepthTextPos.x, _vDebugDepthTextPos.y, "STRING", tlCurrentDepth)
		ENDIF
		
		_vDebugDepthTextPos += _vDebugDepthTextOffset
		
		TEXT_LABEL_63 tlMaxDepth = "Max Depth: "
		tlMaxDepth += GET_STRING_FROM_FLOAT(sPeriscopeData.sCamData.fMaxDepth, 2)
		tlMaxDepth += "m"
		
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_SCALE(fDebugDepthTextScale, fDebugDepthTextScale)
		DISPLAY_TEXT_WITH_LITERAL_STRING(_vDebugDepthTextPos.x, _vDebugDepthTextPos.y, "STRING", tlMaxDepth)
		
	ENDIF

ENDPROC

PROC _DEBUG_PERISCOPE__DRAW_TARGETING_VALUES(PERISCOPE_DATA &sPeriscopeData)

	IF NOT (bDebugDisplayTargetingValues)
		EXIT
	ENDIF
	
	IF DOES_CAM_EXIST(sPeriscopeData.sCamData.cameraID)
	AND IS_CAM_ACTIVE(sPeriscopeData.sCamData.cameraID)
	
		TEXT_LABEL_63 tlText
		VECTOR vPos = <<0.1, 0.1, 0.0>>
		VECTOR vOffset = << 0.0, 0.02, 0.0>>
		
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		tlText = "iLockTargetCount = "
		tlText += GET_STRING_FROM_INT(sPeriscopeData.sTargetingData.iLockTargetCount)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
		vPos += vOffset
		
		tlText = "bIsFiring = "
		tlText += GET_STRING_FROM_BOOL(sPeriscopeData.sTargetingData.bIsFiring)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
		vPos += vOffset
		
		tlText = "bLockOnRequiresControlRelease = "
		tlText += GET_STRING_FROM_BOOL(sPeriscopeData.sTargetingData.bLockOnRequiresControlRelease)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
		vPos += vOffset
		
		tlText = "bFireRequiresControlRelease = "
		tlText += GET_STRING_FROM_BOOL(sPeriscopeData.sTargetingData.bFireRequiresControlRelease)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
		vPos += vOffset		
		
		tlText = "PendingLockTarget.Entity = "
		tlText += GET_STRING_FROM_INT(NATIVE_TO_INT(sPeriscopeData.sTargetingData.PendingLockTarget.Entity))
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
		vPos += vOffset		

		tlText = "PendingLockTarget.LockState = "
		tlText += LOCK_STATE_AS_STRING(sPeriscopeData.sTargetingData.PendingLockTarget.LockState)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
		vPos += vOffset
		
	ENDIF

ENDPROC

PROC _DEBUG_PERISCOPE__UPDATE_ANIMATION(PERISCOPE_DATA &sPeriscopeData)
	
	IF bDebugUpdateAnimPos
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sPeriscopeData.sAnimData.iSyncSceneID)
			SET_SYNCHRONIZED_SCENE_ORIGIN(sPeriscopeData.sAnimData.iSyncSceneID, sPeriscopeData.sAnimData.vPos, sPeriscopeData.sAnimData.vRot)
		ENDIF
	ENDIF
	
	IF bDebugDisplayAnimValues
		TEXT_LABEL_63 tlText
		VECTOR vPos = <<0.1, 0.1, 0.0>>
		VECTOR vOffset = << 0.0, 0.02, 0.0>>
		
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		tlText = "iPropSceneID: "
		tlText += GET_STRING_FROM_INT(sPeriscopeData.sAnimData.iPropSceneID)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos, 0, 255, 0)
		vPos += vOffset	
		
		tlText = "iPeriscopeRestingAnimState: "
		tlText += GET_STRING_FROM_INT(sPeriscopeData.sAnimData.iPeriscopeRestingAnimState)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos, 0, 255, 0)
		vPos += vOffset
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Main function for adding debug widgets for the periscope.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _DEBUG_PERISCOPE__ADD_WIDGETS(PERISCOPE_DATA &sPeriscopeData)

	PRINTLN("[PERISCOPE][_DEBUG_PERISCOPE__ADD_WIDGETS] Adding debug widgets.")
	
	START_WIDGET_GROUP("Periscope")
		START_WIDGET_GROUP("General")
			ADD_WIDGET_BOOL("Enter Scope", bDebugEnterScope)
			ADD_WIDGET_BOOL("Test Bail out", bDebugTestBailOut)
			ADD_WIDGET_BOOL("Set Periscope Unavailable", bDebugDisablePeriscope)
			ADD_WIDGET_BOOL("Immediately Enter Periscope Test", bDebugImmediatelyEnterPeriscope)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Depth Settings")
			ADD_WIDGET_BOOL("Display Depth Values On Screen", bDebugDisplayPeriscopeDepthValues)
			ADD_WIDGET_FLOAT_SLIDER("Values Text Pos x", vDebugDepthTextPos.x, 0, 1.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Values Text Pos y", vDebugDepthTextPos.y, 0, 1.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Values Text Offset", vDebugDepthTextOffset.y, 0, 1.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Values Text Scale", fDebugDepthTextScale, 0, 1.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Max Depth", sPeriscopeData.sCamData.fMaxDepth, 0, 200.0, 1.0)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Camera")
			ADD_WIDGET_BOOL("Dont Render Periscope Cam", bDebugDontRenderPeriscopeCam)
			ADD_WIDGET_BOOL("Show Cam Debug", bDebugDisplayCamValues)
			ADD_WIDGET_FLOAT_SLIDER("Stick Speed", sPeriscopeData.sCamData.fStickSpeed, 0, 1, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Cursor Speed", sPeriscopeData.sCamData.fCursorSpeed, 0, 1000, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("FOV", sPeriscopeData.sCamData.fFOV, 0, 200, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Min Pitch", sPeriscopeData.sCamData.fMinPitch, -90, 90, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Max Pitch", sPeriscopeData.sCamData.fMaxPitch, -90, 90, 0.1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Scope")
			ADD_WIDGET_BOOL("Hide Scope", bDebugHideScope)
			ADD_WIDGET_BOOL("Hide Scope Crosshair", bDebugHideScopeCrosshair)
			ADD_WIDGET_BOOL("Hide Scope Bars", bDebugHideScopeBars)
			ADD_WIDGET_BOOL("Hide Scope Bar Numbers", bDebugHideScopeBarNumbers)
			ADD_WIDGET_INT_SLIDER("Scope Alpha", iDebugScopeAlpha, 0, 255, 1)
			ADD_WIDGET_BOOL("Hide Input Prompts", bDebugHideInputPrompts)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Targeting")
			ADD_WIDGET_BOOL("Show Targeting Debug", bDebugDisplayTargetingValues)
			ADD_WIDGET_FLOAT_SLIDER("Target SmoothDamp Speed", fDebugTargetSmoothDampSpeed, 0, 20, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Target Error Radius", fDebugTargetErrorRadius, 0, 1000, 0.1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Animation")
			ADD_WIDGET_BOOL("Update Anim Pos", bDebugUpdateAnimPos)
			ADD_WIDGET_BOOL("bDebugDisplayAnimValues", bDebugDisplayAnimValues)
			ADD_WIDGET_VECTOR_SLIDER("Scene Origin Pos", sPeriscopeData.sAnimData.vPos, -2000, 2000, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("Scene Origin Rot", sPeriscopeData.sAnimData.vRot, -2000, 2000, 0.1)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()

ENDPROC

/// PURPOSE:
///    Main debug update
/// PARAMS:
///    sPeriscope - Periscope
PROC _DEBUG_PERISCOPE__UPDATE(PERISCOPE_DATA &sPeriscope)

	_DEBUG_PERISCOPE__DRAW_CAM_VALUES(sPeriscope)
	_DEBUG_PERISCOPE__DRAW_TARGETING_VALUES(sPeriscope)
	_DEBUG_PERISCOPE__UPDATE_ANIMATION(sPeriscope)

ENDPROC

#ENDIF // #IF IS_DEBUG_BUILD

// ***************************************************************************************
//
// 					PRIVATE API
//
// ***************************************************************************************

// *******************************************
// 					HELPERS
// *******************************************

PROC _PERISCOPE__SET_PED_CAPSULE()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		FLOAT fCapsuleSize = 0.40
		SET_PED_CAPSULE(PLAYER_PED_ID(), fCapsuleSize)
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the name of the current anim stage.
/// PARAMS:
///    eStage - The anim stage of the psynced scene.
/// RETURNS:
///    Name of current anim stage.
FUNC STRING _PERISCOPE__GET_STRING_FROM_ANIM_STAGE(PERISCOPE_ANIM_STAGE eStage)
	SWITCH eStage
		CASE PAS_DEFAULT			RETURN "PAS_DEFAULT"
		CASE PAS_START_WALKING		RETURN "PAS_START_WALKING"
		CASE PAS_WALKING			RETURN "PAS_WALKING"
		CASE PAS_ENTER				RETURN "PAS_ENTER"
		CASE PAS_IDLE				RETURN "PAS_IDLE"
		CASE PAS_IDLING				RETURN "PAS_IDLING"
		CASE PAS_EXIT				RETURN "PAS_EXIT"
		CASE PAS_EXITING			RETURN "PAS_EXITING"
	ENDSWITCH
	
	RETURN "NULL"
ENDFUNC

/// PURPOSE:
///    Set the anim stage of the synced scene.
/// PARAMS:
///    sPeriscope - Periscope.
///    eNewStage - New anim stage.
PROC _PERISCOPE__SET_ANIM_STAGE(PERISCOPE_DATA &sPeriscope, PERISCOPE_ANIM_STAGE eNewStage)
	PRINTLN("[PERISCOPE][_PERISCOPE__SET_ANIM_STAGE] Setting stage from ", _PERISCOPE__GET_STRING_FROM_ANIM_STAGE(sPeriscope.eAnimStage), " to ", _PERISCOPE__GET_STRING_FROM_ANIM_STAGE(eNewStage))
	sPeriscope.eAnimStage = eNewStage
ENDPROC

/// PURPOSE:
///    Returns the name of the current stage.
/// PARAMS:
///    eStage - The stage of the periscope.
/// RETURNS:
///    Name of current stage.
FUNC STRING _PERISCOPE__GET_STRING_FROM_TRIGGER_STAGE(PERISCOPE_TRIGGER_STAGE eStage)
	SWITCH eStage
		CASE PTS_PLAYER_OUT_OF_RANGE	RETURN "PTS_PLAYER_OUT_OF_RANGE"
		CASE PTS_INIT_FOR_USE			RETURN "PTS_INIT_FOR_USE"
		CASE PTS_WAIT_FOR_PLAYER_ENTER	RETURN "PTS_WAIT_FOR_PLAYER_ENTER"
		CASE PTS_START_ANIM_ENTRY		RETURN "PTS_START_ANIM_ENTRY"
		CASE PTS_ENTER_SCOPE_CAM		RETURN "PTS_ENTER_SCOPE_CAM"
		CASE PTS_UPDATE_SCOPE_CAM		RETURN "PTS_UPDATE_SCOPE_CAM"
		CASE PTS_EXIT_SCOPE_CAM			RETURN "PTS_EXIT_SCOPE_CAM"
		CASE PTS_START_ANIM_EXIT		RETURN "PTS_START_ANIM_EXIT"
		CASE PTS_BAIL_OUT				RETURN "PTS_BAIL_OUT"
		CASE PTS_CLEANUP				RETURN "PTS_CLEANUP"
	ENDSWITCH
	
	RETURN "NULL"
ENDFUNC

/// PURPOSE:
///    Set the stage of the periscope.
/// PARAMS:
///    sPeriscope - Periscope.
///    eNewStage - New periscope stage.
PROC _PERISCOPE__SET_TRIGGER_STAGE(PERISCOPE_DATA &sPeriscope, PERISCOPE_TRIGGER_STAGE eNewStage)
	PRINTLN("[PERISCOPE][_PERISCOPE__SET_TRIGGER_STAGE] Setting stage from ", _PERISCOPE__GET_STRING_FROM_TRIGGER_STAGE(sPeriscope.eStage), " to ", _PERISCOPE__GET_STRING_FROM_TRIGGER_STAGE(eNewStage))
	sPeriscope.eStage = eNewStage
ENDPROC

PROC _PERISCOPE__SET_KICK_OUT_REASON(PERISCOPE_DATA &sPeriscope, PERISCOPE_KICK_OUT_REASON eReason)
	PRINTLN("[PERISCOPE][_PERISCOPE__SET_KICK_OUT_REASON] Setting kick out reason ", KICK_OUT_REASON_AS_STRING(sPeriscope.eKickOutReason), " to ", KICK_OUT_REASON_AS_STRING(eReason))
	sPeriscope.eKickOutReason = eReason
ENDPROC

/// PURPOSE:
///    Sets the periscope bit set.
/// PARAMS:
///    sPeriscope - Periscope.
///    iBit - The it.
///    bSet - TRUE for setting the bit. FALSE for clearing the bit.
PROC _PERISCOPE__SET_BIT(PERISCOPE_DATA &sPeriscope, INT iBit, BOOL bSet)
	IF (bSet)
		IF NOT IS_BIT_SET(sPeriscope.iBS, iBit)
			PRINTLN("[PERISCOPE][_PERISCOPE__SET_BIT] Setting bit ", _DEBUG_PERISCOPE__GET_BIT_NAME(iBit))
			SET_BIT(sPeriscope.iBS, iBit)
		ENDIF
	ELSE
		IF IS_BIT_SET(sPeriscope.iBS, iBit)	
			PRINTLN("[PERISCOPE][_PERISCOPE__SET_BIT] Clearing bit ", _DEBUG_PERISCOPE__GET_BIT_NAME(iBit))
			CLEAR_BIT(sPeriscope.iBS, iBit)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Get the text label of the periscope help text.
/// RETURNS:
///    Help text message.
FUNC STRING _PERISCOPE__GET_USE_HELP_TEXT_LABEL()
	RETURN "SUB_PERI_USE"
ENDFUNC

/// PURPOSE:
///    Get the text label of the periscope in use help text.
/// RETURNS:
///    Help text message.
FUNC STRING _PERISCOPE__GET_IN_USE_HELP_TEXT_LABEL()
	RETURN "SUB_PERI_IN_USE"
ENDFUNC

/// PURPOSE:
///    Get the text label of the perisope bail help text.
/// RETURNS:
///    Help text message. 
FUNC STRING _PERISCOPE__GET_BAIL_HELP_TEXT_LABEL()
	RETURN "SUB_PERI_BAIL"
ENDFUNC

FUNC STRING _PERISCOPE__GET_TIP_HELP_TEXT_LABEL()
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN "SUB_PERI_TIPPC"
	ELSE
		RETURN "SUB_PERI_TIP"
	ENDIF
	
	RETURN "SUB_PERI_TIP"
ENDFUNC

/// PURPOSE:
///    Safely display help text if needed.
/// PARAMS:
///    strHelpTextLabel - Text label.
PROC _PERISCOPE__SAFE_DISPLAY_HELP_TEXT(STRING strHelpTextLabel)
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(strHelpTextLabel)
	AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	AND NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
	AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
	AND NOT IS_PED_SPRINTING(PLAYER_PED_ID())
	AND NOT NETWORK_IS_IN_MP_CUTSCENE()
		PRINT_HELP_FOREVER(strHelpTextLabel)
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely remove help text.
/// PARAMS:
///    strHelpTextLabel - Text label.
PROC _PERISCOPE__SAFE_REMOVE_HELP_TEXT(STRING strHelpTextLabel)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(strHelpTextLabel)
		CLEAR_HELP()
	ENDIF
ENDPROC

/// PURPOSE:
///    Detect when a player enters the periscope prompt range.
/// PARAMS:
///    playerID - Player
///    sPeriscopeData - Periscope
/// RETURNS:
///    TRUE if player is within range.
FUNC BOOL _PERISCOPE__IS_PLAYER_WITHIN_RANGE(PLAYER_INDEX playerID, PERISCOPE_DATA &sPeriscopeData)
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
	OR NOT IS_SKYSWOOP_AT_GROUND()
	OR g_iSimpleInteriorState != SIMPLE_INT_STATE_IDLE
		
		#IF IS_DEBUG_BUILD
			IF (GET_FRAME_COUNT() % 30) = 0
				PRINTLN("[PERISCOPE][_PERISCOPE__IS_PLAYER_WITHIN_RANGE] Early Exit - Walking in or out: ", IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID()), " skyswoop: ", IS_SKYSWOOP_AT_GROUND(), " simple interior state: ", g_iSimpleInteriorState)
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF playerID != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(playerID)
		PED_INDEX playerPed = GET_PLAYER_PED(playerID)
		
		IF IS_ENTITY_AT_COORD(playerPed, sPeriscopeData.vPromptPos, sPeriscopeData.vPromptDimentions, DEFAULT, DEFAULT, TM_ON_FOOT)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VECTOR _PERISCOPE__GET_CAM_OFFSET_FROM_SUBMARINE()
	RETURN <<0.6057, 25.3831, 13.2303>>
ENDFUNC

FUNC VECTOR _PERISCOPE__GET_CAM_ORIGIN_POINT(PERISCOPE_DATA &sPeriscopeData)
	IF IS_ENTITY_ALIVE(sPeriscopeData.vehSubmarineIAmIn)
		VECTOR vOffset = _PERISCOPE__GET_CAM_OFFSET_FROM_SUBMARINE()
		VECTOR vReturn = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sPeriscopeData.vehSubmarineIAmIn, vOffset)
		
		PRINTLN("[PERISCOPE][_PERISCOPE__GET_CAM_ATTACHED_TO_SUBMARINE_COORD] Returning: ", vReturn)
		RETURN vReturn
	ENDIF
	
	PRINTLN("[PERISCOPE][_PERISCOPE__GET_CAM_ATTACHED_TO_SUBMARINE_COORD] Submarine isn't alive. Can't get coord. Returning 0.")
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC BOOL _PERISCOPE__IS_CAM_ABOVE_WATER_HEIGHT(PERISCOPE_DATA &sPeriscopeData, BOOL bIncludeWaves = TRUE)
	FLOAT fWaterHeight
	VECTOR vCamPos = _PERISCOPE__GET_CAM_ORIGIN_POINT(sPeriscopeData)
	
	IF (bIncludeWaves)
		GET_WATER_HEIGHT(vCamPos, fWaterHeight)
	ELSE
		GET_WATER_HEIGHT_NO_WAVES(vCamPos, fWaterHeight)
	ENDIF
	
	/*
	TEXT_LABEL_63 tlText
	VECTOR vPos = <<0.1, 0.1, 0.0>>
	VECTOR vOffset = << 0.0, 0.02, 0.0>>

	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	tlText = "vCamPos.z: "
	tlText += GET_STRING_FROM_FLOAT(vCamPos.z)
	DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
	
	vPos += vOffset
	tlText = "fWaterHeight: "
	tlText += GET_STRING_FROM_FLOAT(fWaterHeight)
	DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos)
	*/
	
	IF vCamPos.z > fWaterHeight
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if player is alone submarine periscope area
FUNC BOOL _PERISCOPE__IS_PLAYER_ALONE_IN_SUBMARINE_PERISCOPE_AREA(PERISCOPE_DATA &sPeriscopeData)
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))

			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			IF IS_NET_PLAYER_OK(playerID)
			AND NOT NETWORK_IS_PLAYER_CONCEALED(playerID)
				IF playerID != PLAYER_ID()
					IF _PERISCOPE__IS_PLAYER_WITHIN_RANGE(playerID, sPeriscopeData)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDREPEAT

	RETURN TRUE
ENDFUNC

PROC _PERISCOPE_DISABLE_MOVEMENT_CONTROL_ACTION_THIS_FRAME()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_TURN_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_PITCH_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_THROTTLE_UP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_THROTTLE_DOWN)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_ASCEND)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_DESCEND)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_TURN_HARD_LEFT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_TURN_HARD_RIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_MOUSE_CONTROL_OVERRIDE)
		ENDIF
	ENDIF
ENDPROC

// *******************************************
// 				PROPS & ANIMATION
// *******************************************

PROC _PERISCOPE__GET_ANIM_DATA(PERISCOPE_DATA &sPeriscopeData, PERISCOPE_ANIM_STAGE eStage)
	
	// ========== PED ANIMS ==========
	IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
		sPeriscopeData.sAnimData.tlDictionary = "ANIM@SCRIPTED@SUBMARINE@IG17_SUB_PERISCOPE@HEELED@"
		
		SWITCH eStage
			CASE PAS_ENTER
				sPeriscopeData.sAnimData.tlClipPed = "ENTER_HEELED"
			BREAK
			CASE PAS_IDLE
				sPeriscopeData.sAnimData.tlClipPed = "IDLE_HEELED"
			BREAK
			CASE PAS_EXIT
				sPeriscopeData.sAnimData.tlClipPed = "EXIT_HEELED"
			BREAK
			CASE PAS_DEFAULT
				sPeriscopeData.sAnimData.tlClipPed = ""
			BREAK
		ENDSWITCH
	ELSE
		sPeriscopeData.sAnimData.tlDictionary = "ANIM@SCRIPTED@SUBMARINE@IG17_SUB_PERISCOPE@MALE@"
		
		SWITCH eStage
			CASE PAS_ENTER
				sPeriscopeData.sAnimData.tlClipPed = "ENTER_MALE"
			BREAK
			CASE PAS_IDLE
				sPeriscopeData.sAnimData.tlClipPed = "IDLE_MALE"
			BREAK
			CASE PAS_EXIT
				sPeriscopeData.sAnimData.tlClipPed = "EXIT_MALE"
			BREAK
			CASE PAS_DEFAULT
				sPeriscopeData.sAnimData.tlClipPed = ""
			BREAK
		ENDSWITCH
	ENDIF
	
	// ========== PROP ANIMS ==========

	SWITCH eStage
		CASE PAS_ENTER
			sPeriscopeData.sAnimData.tlClipPeriscope = "ENTER_PERISCOPE"
		BREAK
		CASE PAS_IDLE
			sPeriscopeData.sAnimData.tlClipPeriscope = "IDLE_PERISCOPE"
		BREAK
		CASE PAS_EXIT
			sPeriscopeData.sAnimData.tlClipPeriscope = "EXIT_PERISCOPE"
		BREAK
		CASE PAS_DEFAULT
			sPeriscopeData.sAnimData.tlClipPeriscope = ""
		BREAK
	ENDSWITCH
	
	/*
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[PERISCOPE][_PERISCOPE__GET_ANIM_DATA] Getting anim data for stage: ", _PERISCOPE__GET_STRING_FROM_ANIM_STAGE(eStage))
	PRINTLN("[PERISCOPE][_PERISCOPE__GET_ANIM_DATA] sPeriscopeData.sAnimData.tlDictionary: ", Get_String_From_TextLabel(sPeriscopeData.sAnimData.tlDictionary))
	PRINTLN("[PERISCOPE][_PERISCOPE__GET_ANIM_DATA] sPeriscopeData.sAnimData.tlClipPed: ", Get_String_From_TextLabel(sPeriscopeData.sAnimData.tlClipPed))
	PRINTLN("[PERISCOPE][_PERISCOPE__GET_ANIM_DATA] sPeriscopeData.sAnimData.tlClipPeriscope: ", Get_String_From_TextLabel(sPeriscopeData.sAnimData.tlClipPeriscope))
	*/
ENDPROC

FUNC BOOL _PERISCOPE__CREATE_PERISCOPE_PROP(PERISCOPE_DATA &sPeriscopeData)
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sPeriscopeData.serverBD.niPeriscopeProp)
			
		// ========== Model ==========
		sPeriscopeData.ePeriscopeModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_int_lev_Sub_Periscope"))
		
		IF NOT IS_MODEL_VALID(sPeriscopeData.ePeriscopeModel)
			PRINTLN("[PERISCOPE][_PERISCOPE__CREATE_PERISCOPE_PROP] Not a valid model!")
			RETURN FALSE
		ENDIF
	
		REQUEST_MODEL(sPeriscopeData.ePeriscopeModel)
		
		IF NOT HAS_MODEL_LOADED(sPeriscopeData.ePeriscopeModel)
			PRINTLN("[PERISCOPE][_PERISCOPE__CREATE_PERISCOPE_PROP] Loading model...")
			RETURN FALSE
		ENDIF
		
		IF IS_STRING_NULL_OR_EMPTY(sPeriscopeData.sAnimData.tlDictionary)
		OR IS_STRING_NULL_OR_EMPTY(sPeriscopeData.sAnimData.tlClipPeriscope)
			_PERISCOPE__GET_ANIM_DATA(sPeriscopeData, PAS_ENTER)
		ENDIF
		
		REQUEST_ANIM_DICT(sPeriscopeData.sAnimData.tlDictionary)
		
		IF NOT HAS_ANIM_DICT_LOADED(sPeriscopeData.sAnimData.tlDictionary)
			PRINTLN("[PERISCOPE][_PERISCOPE__CREATE_PERISCOPE_PROP] Requesting anim dict...: ", Get_String_From_TextLabel(sPeriscopeData.sAnimData.tlDictionary))
			RETURN FALSE
		ENDIF
		
		IF NOT CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
			PRINTLN("[PERISCOPE][_PERISCOPE__CREATE_PERISCOPE_PROP] CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT false")
			RETURN FALSE
		ENDIF
		
		RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
		
		IF CAN_REGISTER_MISSION_OBJECTS(1)
			// ========== Object ==========
			OBJECT_INDEX objTempPeriscope
			PRINTLN("[PERISCOPE][_PERISCOPE__CREATE_PERISCOPE_PROP] Creating persicope prop!")
			
			VECTOR vPeriscopePos, vPeriscopeRot
			vPeriscopePos = GET_ANIM_INITIAL_OFFSET_POSITION(sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPeriscope, sPeriscopeData.sAnimData.vPos, sPeriscopeData.sAnimData.vRot)
			vPeriscopeRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPeriscope, sPeriscopeData.sAnimData.vPos, sPeriscopeData.sAnimData.vRot)
			
			objTempPeriscope = CREATE_OBJECT(sPeriscopeData.ePeriscopeModel, vPeriscopePos)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(sPeriscopeData.ePeriscopeModel)
			SET_ENTITY_CAN_BE_DAMAGED(objTempPeriscope, FALSE)
			NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(objTempPeriscope, TRUE)
			SET_ENTITY_COORDS_NO_OFFSET(objTempPeriscope, vPeriscopePos)
			SET_ENTITY_ROTATION(objTempPeriscope, vPeriscopeRot)
			FREEZE_ENTITY_POSITION(objTempPeriscope, TRUE)
			sPeriscopeData.serverBD.niPeriscopeProp = OBJ_TO_NET(objTempPeriscope)
		ENDIF
	ELSE	
		IF NOT IS_BIT_SET(sPeriscopeData.iBS, BS_REMOTE_PLAYER_IS_USING_PERISCOPE)
		AND NOT IS_BIT_SET(sPeriscopeData.iBS, BS_LOCAL_PLAYER_IS_USING_PERISCOPE)
			
			INT iLocalSceneID = -1
			
			IF TAKE_CONTROL_OF_ENTITY(sPeriscopeData.oiPeriscope)
				SWITCH sPeriscopeData.sAnimData.iPeriscopeRestingAnimState
					CASE 0
						_PERISCOPE__GET_ANIM_DATA(sPeriscopeData, PAS_ENTER)
					
						REQUEST_ANIM_DICT(sPeriscopeData.sAnimData.tlDictionary)
						
						IF HAS_ANIM_DICT_LOADED(sPeriscopeData.sAnimData.tlDictionary)
							PRINTLN("[PERISCOPE][_PERISCOPE__CREATE_PERISCOPE_PROP] Playing initial anim...")
							
							sPeriscopeData.sAnimData.iPropSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(sPeriscopeData.sAnimData.vPos, sPeriscopeData.sAnimData.vRot)
						
							NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(sPeriscopeData.oiPeriscope, sPeriscopeData.sAnimData.iPropSceneID, sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPeriscope, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
							
							NETWORK_START_SYNCHRONISED_SCENE(sPeriscopeData.sAnimData.iPropSceneID)
							sPeriscopeData.sAnimData.iPeriscopeRestingAnimState = 1
						ENDIF
					BREAK
					CASE 1
						iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sPeriscopeData.sAnimData.iPropSceneID)
						
						IF iLocalSceneID != -1
							IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) != 0.01
								PRINTLN("[PERISCOPE][_PERISCOPE__CREATE_PERISCOPE_PROP] Setting phase and rate.")
								SET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID, 0.01)
								SET_SYNCHRONIZED_SCENE_RATE(iLocalSceneID, 0.0)
								sPeriscopeData.sAnimData.iPeriscopeRestingAnimState = 2 
							ENDIF
						ELSE
							sPeriscopeData.sAnimData.iPeriscopeRestingAnimState = 2 
						ENDIF
					BREAK
					CASE 2
						// Do nothing
					BREAK
				ENDSWITCH
			ENDIF
			
		ELSE
			IF sPeriscopeData.sAnimData.iPeriscopeRestingAnimState != 0
				sPeriscopeData.sAnimData.iPeriscopeRestingAnimState = 0
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Convert periscope network objects to local
PROC _PERISCOPE__MAINTAIN_LOCAL_OBJECTS(PERISCOPE_DATA &sPeriscopeData)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sPeriscopeData.serverBD.niPeriscopeProp)
		sPeriscopeData.oiPeriscope = NET_TO_OBJ(sPeriscopeData.serverBD.niPeriscopeProp)
	ENDIF
ENDPROC

// *******************************************
// 					AUDIO
// *******************************************

FUNC STRING _PERISCOPE__GET_SOUNDSET()
	RETURN "DLC_H4_Submarine_Periscope_Sounds"
ENDFUNC

FUNC STRING _PERISCOPE__GET_AUDIO_SCENE()
	RETURN "dlc_hei4_submarine_Periscope_View_Scene"
ENDFUNC

/// PURPOSE:
///    Start the SFX and mixer scene when entering the camera.
PROC _PERISCOPE__START_AUDIO(PERISCOPE_DATA &sPeriscopeData)
	PRINTLN("[PERISCOPE][_PERISCOPE__START_AUDIO] Called.")
	
	sPeriscopeData.iBGLoopSoundID = GET_SOUND_ID()
	PLAY_SOUND_FRONTEND(sPeriscopeData.iBGLoopSoundID, "Background_Loop", _PERISCOPE__GET_SOUNDSET())

	IF NOT IS_AUDIO_SCENE_ACTIVE(_PERISCOPE__GET_AUDIO_SCENE())
		START_AUDIO_SCENE(_PERISCOPE__GET_AUDIO_SCENE())
		PRINTLN("[PERISCOPE][_PERISCOPE__START_AUDIO] Playing Audio Scene: ", _PERISCOPE__GET_AUDIO_SCENE())
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops the SFX and mixer scene when exiting the camera.
PROC _PERISCOPE__STOP_AUDIO(PERISCOPE_DATA &sPeriscopeData)
	PRINTLN("[PERISCOPE][_PERISCOPE__STOP_AUDIO] Called.")
	
	STOP_SOUND(sPeriscopeData.iBGLoopSoundID)
	RELEASE_SOUND_ID(sPeriscopeData.iBGLoopSoundID)
	
	IF IS_AUDIO_SCENE_ACTIVE(_PERISCOPE__GET_AUDIO_SCENE())
		STOP_AUDIO_SCENE(_PERISCOPE__GET_AUDIO_SCENE())
		PRINTLN("[PERISCOPE][_PERISCOPE__STOP_AUDIO] Stopping Audio Scene: ", _PERISCOPE__GET_AUDIO_SCENE())
	ENDIF
ENDPROC

PROC PERISCOPE__UPDATE_TURNING_SOUND(PERISCOPE_DATA &sPeriscopeData)
	
	IF (sPeriscopeData.iBGLoopSoundID != -1)
	AND (sPeriscopeData.sCamData.fCurrentRotSpeed >= 0)
	AND (sPeriscopeData.sCamData.fCurrentRotSpeed <= 1)
		SET_VARIABLE_ON_SOUND(sPeriscopeData.iBGLoopSoundID, "TurnSpeed", sPeriscopeData.sCamData.fCurrentRotSpeed)
	ENDIF
	
ENDPROC

// *******************************************
// 					CAMERA
// *******************************************

/// PURPOSE:
///    Reset the values for the cams' rotational difference when leaving the periscope. 
PROC _PERISCOPE__RESET_CAM_ROT_SPEED(PERISCOPE_DATA &sPeriscopeData)
	sPeriscopeData.sCamData.fCurrentRotSpeed = 0
ENDPROC

/// PURPOSE:
///    Enable to TC Mod used for the periscope.
PROC _PERISCOPE__APPLY_SCOPE_TC_MOD()

	TEXT_LABEL_23 strMod = "IslandPeriscope"
	
	CLEAR_TIMECYCLE_MODIFIER()
	SET_TIMECYCLE_MODIFIER(strMod)
	PUSH_TIMECYCLE_MODIFIER()
	POP_TIMECYCLE_MODIFIER()
	
	PRINTLN("[PERISCOPE][_PERISCOPE__APPLY_SCOPE_TC_MOD] Timecycle mod is ACTIVE: ", strMod)
	
ENDPROC

/// PURPOSE:
///    Clear the TC Mod used for the periscope.
PROC _PERISCOPE__CLEAR_SCOPE_TC_MOD()
	
	CLEAR_TIMECYCLE_MODIFIER()
	
	PRINTLN("[PERISCOPE][_PERISCOPE__CLEAR_SCOPE_TC_MOD] Timecycle mod is CLEARED")
	
ENDPROC

/// PURPOSE:
///    Called for one frame to set the internal data for the scope camera. 
/// PARAMS:
///    sPeriscopeData - Periscope instance.
PROC _PERISCOPE__CAMERA_INIT(PERISCOPE_DATA &sPeriscopeData)

	// FOV
	IF (sPeriscopeData.sCamData.fFOV = 0.0)
		sPeriscopeData.sCamData.fFOV = 50.0
	ENDIF

	// Stick speed
	IF (sPeriscopeData.sCamData.fStickSpeed = 0.0)
		sPeriscopeData.sCamData.fStickSpeed = 0.03
	ENDIF
	
	// Cursor speed
	IF (sPeriscopeData.sCamData.fCursorSpeed = 0.0)
		sPeriscopeData.sCamData.fCursorSpeed = 50.0
	ENDIF
	// Pitch Limits
	IF (sPeriscopeData.sCamData.fMinPitch = 0.0)
		sPeriscopeData.sCamData.fMinPitch = -36.0
	ENDIF
	IF (sPeriscopeData.sCamData.fMaxPitch = 0.0)
		sPeriscopeData.sCamData.fMaxPitch = 85.0
	ENDIF

ENDPROC

/// PURPOSE:
///    Enable the periscope camera.
/// PARAMS:
///    sPeriscopeData - Periscope
///    bEnable - TRUE enters the periscope camers. FALSE exits the camera and returns to player cam.
PROC _PERISCOPE__ENABLE_CAMERA(PERISCOPE_DATA &sPeriscopeData, BOOL bEnable)
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_ENABLE_PERISCOPE_CAM, bEnable)
ENDPROC

/// PURPOSE:
///    Check if the periscope camera is enabled.
/// PARAMS:
///    sPeriscopeData - periscope
/// RETURNS:
///    TRUE if is enabled.
FUNC BOOL _PERISCOPE__IS_CAMERA_ENABLED(PERISCOPE_DATA &sPeriscopeData)
	RETURN IS_BIT_SET(sPeriscopeData.iBS, BS_ENABLE_PERISCOPE_CAM)
ENDFUNC

/// PURPOSE:
///    Clean up the periscope camera.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _PERISCOPE__CLEANUP_CAMERA(PERISCOPE_DATA &sPeriscopeData)

	IF _PERISCOPE__IS_CAMERA_ENABLED(sPeriscopeData)
		_PERISCOPE__ENABLE_CAMERA(sPeriscopeData, FALSE)
	ENDIF

	IF DOES_CAM_EXIST(sPeriscopeData.sCamData.cameraID)
		PRINTLN("[PERISCOPE][_PERISCOPE__CLEANUP_CAMERA] Cleaning up camera.")
		DESTROY_CAM(sPeriscopeData.sCamData.cameraID)
		SET_WIDESCREEN_BORDERS(FALSE, 0)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Detects when the current cam values are different to the target values.
/// PARAMS:
///    sPeriscopeData - Periscope.
/// RETURNS:
///    TRUE if values are different.
FUNC BOOL _PERISCOPE__HAS_CAMERA_VALUES_CHANGED(PERISCOPE_DATA &sPeriscopeData)

	VECTOR vCurrentRot
	FLOAT fCurrentFOV
	
	IF DOES_CAM_EXIST(sPeriscopeData.sCamData.cameraID)
	AND IS_CAM_ACTIVE(sPeriscopeData.sCamData.cameraID)
	
		vCurrentRot = GET_CAM_ROT(sPeriscopeData.sCamData.cameraID)
		fCurrentFOV = GET_CAM_FOV(sPeriscopeData.sCamData.cameraID)
		
		IF (VDIST2(sPeriscopeData.sCamData.vRot, vCurrentRot) > 0.01)
		OR (ABSF(sPeriscopeData.sCamData.fFOV - fCurrentFOV) > 0.01)
			RETURN TRUE
		ENDIF
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    A magic way of returning the percentage of a point between 2 values
/// PARAMS:
///    fA - Min Value
///    fB - Max Value
///    fValue - Given Value
FUNC FLOAT _PERISCOPE_INVERSE_LERP(FLOAT fA, FLOAT fB, FLOAT fValue)
	IF fA = fB
		RETURN 0.0
	ENDIF

	RETURN CLAMP(((fValue - fA) / (fB - fA)),0,1)
ENDFUNC

/// PURPOSE:
///    Update the rotational difference values in order to calculate the speed of the cam rotation. 
PROC _PERICOPE__UPDATE_CAM_ROTATION_SPEED(PERISCOPE_CAM_DATA &sPeriscopeCamData, FLOAT fRightX, FLOAT fRightY, FLOAT fMaxStickValue, FLOAT fFrameRateModifier, FLOAT fSpeedReduction, FLOAT &fReturnZ, FLOAT &fReturnX, FLOAT &fReturnSpeed)
	FLOAT fMaxRotDiffZ
	FLOAT fMaxRotDiffX
	FLOAT fSpeed 
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		fSpeed = sPeriscopeCamData.fCursorSpeed
	ELSE
		fSpeed = sPeriscopeCamData.fStickSpeed
	ENDIF
	
	// First we calculate how far the camera will rotate round on the next frame. 
	fReturnZ = (fRightX * fSpeed * fFrameRateModifier)
	fReturnX = (fRightY * fSpeed * fFrameRateModifier) / fSpeedReduction
	
	// Then, we calculate the max value it could've rotated round at if at max stick speed. 
	IF (fRightX >= 0)
		fMaxRotDiffZ = (fMaxStickValue * fSpeed * fFrameRateModifier)
	ELSE
		fMaxRotDiffZ = (-fMaxStickValue * fSpeed * fFrameRateModifier)
	ENDIF
	
	IF (fRightY >= 0)
		fMaxRotDiffX = (fMaxStickValue * fSpeed * fFrameRateModifier) / fSpeedReduction
	ELSE
		fMaxRotDiffX = (-fMaxStickValue * fSpeed * fFrameRateModifier) / fSpeedReduction
	ENDIF
	
	// Why do we do this? To calculate the camera rotation speed of course!
	FLOAT fCurrentRotSpeedZ = _PERISCOPE_INVERSE_LERP(0, fMaxRotDiffZ, fReturnZ)
	FLOAT fCurrentRotSpeedX = _PERISCOPE_INVERSE_LERP(0, fMaxRotDiffX, fReturnX) / fSpeedReduction
	
	// Pick the fastest roation speed out of both directions. 
	BOOL bIsRotatingFasterOnZAxis = (fCurrentRotSpeedZ > fCurrentRotSpeedX)
	FLOAT fNewFloat = PICK_FLOAT(bIsRotatingFasterOnZAxis, fCurrentRotSpeedZ, fCurrentRotSpeedX)
	
	fReturnSpeed = LERP_FLOAT(sPeriscopeCamData.fCurrentRotSpeed, fNewFloat, 0.6)
ENDPROC

/// PURPOSE:
///    Main procedure for handling the camera controls.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _PERISCOPE__HANDLE_CAMERA_CONTROLS(PERISCOPE_DATA &sPeriscopeData)

	IF DOES_CAM_EXIST(sPeriscopeData.sCamData.cameraID)
	AND IS_CAM_ACTIVE(sPeriscopeData.sCamData.cameraID)
	
		FLOAT fSpeedReduction = 3.0
		FLOAT fMouseSpeedReduction = 7.0
		FLOAT fMaxStickValue = 127.0
		FLOAT fMaxCursorValue = 0.01
		FLOAT fCurrentSpeed
		
		FLOAT fNewRotZ
		FLOAT fNewRotX
		
		// Speed (framerate independant)
		FLOAT fFrameRateModifier = 30.0 * TIMESTEP()
		
		ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
		
		// Ensure players aren't controlling a vehicle while opering the periscope.
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				DISABLE_PLAYER_VEHICLE_CONTROLS_THIS_FRAME()
			ENDIF
		ENDIF
		
		// -------------Controller-------------
		IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
			INT iLeftX, iLeftY, iRightX, iRightY
			GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
			
			IF IS_LOOK_INVERTED()
				iRightY *= -1
			ENDIF
			
			_PERICOPE__UPDATE_CAM_ROTATION_SPEED(sPeriscopeData.sCamData, TO_FLOAT(iRightX), TO_FLOAT(iRightY), fMaxStickValue, fFrameRateModifier, fSpeedReduction, fNewRotZ, fNewRotX, fCurrentSpeed)
			
			sPeriscopeData.sCamData.vRot.z -= fNewRotZ
			sPeriscopeData.sCamData.vRot.x -= fNewRotX
		ENDIF
		
		
		// -------------Keyboard & Mouse-------------
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			
			FLOAT fMouseY = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD)
			FLOAT fMouseX = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR)
			
			// Apparently TIMESTEP() break mouse movement. So pass in 1.
			_PERICOPE__UPDATE_CAM_ROTATION_SPEED(sPeriscopeData.sCamData, fMouseX, fMouseY, fMaxCursorValue, 1, fMouseSpeedReduction, fNewRotZ, fNewRotX, fCurrentSpeed)
			
			sPeriscopeData.sCamData.vRot.z -= fNewRotZ
			
			IF IS_MOUSE_LOOK_INVERTED()
				sPeriscopeData.sCamData.vRot.x += fNewRotX
			ELSE
				sPeriscopeData.sCamData.vRot.x -= fNewRotX
			ENDIF
			
		ENDIF
		
		// Ensure rot values dont exceed 360 degrees.
		IF	(sPeriscopeData.sCamData.vRot.z > 360)
		OR (sPeriscopeData.sCamData.vRot.z < -360)
			sPeriscopeData.sCamData.vRot.z = 0
		ENDIF
		
		// Clamp pitch
		sPeriscopeData.sCamData.vRot.x = CLAMP(	sPeriscopeData.sCamData.vRot.x, 
											sPeriscopeData.sCamData.fMinPitch,
											sPeriscopeData.sCamData.fMaxPitch)
		
		
		// Only update speed if camera is moving.
		IF NOT ARE_VECTORS_EQUAL(sPeriscopeData.sCamData.vPrevRot, sPeriscopeData.sCamData.vRot)
			sPeriscopeData.sCamData.fCurrentRotSpeed = fCurrentSpeed
			sPeriscopeData.sCamData.vPrevRot = sPeriscopeData.sCamData.vRot
		ELSE
			sPeriscopeData.sCamData.fCurrentRotSpeed = 0
		ENDIF
	
	ENDIF

ENDPROC

/// PURPOSE:
///    Main update for the periscope camera.
/// PARAMS:
///    sPeriscopeData - Periscope
PROC _PERISCOPE__UPDATE_CAMERA(PERISCOPE_DATA &sPeriscopeData)

	// -----Debug-----
	#IF IS_DEBUG_BUILD
	IF bDebugDontRenderPeriscopeCam
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		DRAW_DEBUG_TEXT_2D("Debug 'bDebugDontRenderPeriscopeCam' ACTIVE", <<0.1,0.1,0.0>>)
		EXIT
	ENDIF
	#ENDIF

	// -------------Disable camera if not enabled by player-------------
	IF NOT _PERISCOPE__IS_CAMERA_ENABLED(sPeriscopeData)
		_PERISCOPE__CLEANUP_CAMERA(sPeriscopeData)
		EXIT
	ENDIF
	
	// -------------Error Management-------------
	IF IS_VECTOR_ZERO(sPeriscopeData.sCamData.vRot)
	AND (sPeriscopeData.sCamData.fFOV = 0)
		PRINTLN("[PERISCOPE][_PERISCOPE__UPDATE_CAMERA] Camera data not initialised.")
	ENDIF
	
	// -------------Setting up Camera-------------
	IF NOT DOES_CAM_EXIST(sPeriscopeData.sCamData.cameraID)
		PRINTLN("[PERISCOPE][_PERISCOPE__UPDATE_CAMERA] Creating camera.")
		sPeriscopeData.sCamData.cameraID = CREATE_CAMERA()
		SET_CAM_ROT(sPeriscopeData.sCamData.cameraID, sPeriscopeData.sCamData.vRot)
		SET_CAM_FOV(sPeriscopeData.sCamData.cameraID, sPeriscopeData.sCamData.fFOV)
		SET_WIDESCREEN_BORDERS(TRUE, 0)
		
		IF DOES_ENTITY_EXIST(sPeriscopeData.vehSubmarineIAmIn)
		AND IS_ENTITY_ALIVE(sPeriscopeData.vehSubmarineIAmIn)
			VECTOR vOffset = _PERISCOPE__GET_CAM_OFFSET_FROM_SUBMARINE()
			ATTACH_CAM_TO_ENTITY(sPeriscopeData.sCamData.cameraID, sPeriscopeData.vehSubmarineIAmIn, vOffset)
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(sPeriscopeData.sCamData.cameraID)
	AND NOT IS_CAM_ACTIVE(sPeriscopeData.sCamData.cameraID)
		PRINTLN("[PERISCOPE][_PERISCOPE__UPDATE_CAMERA] Setting camera active.")
		SET_CAM_ACTIVE(sPeriscopeData.sCamData.cameraID, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ENDIF
	
	// -------------Updating Camera-------------
	IF DOES_CAM_EXIST(sPeriscopeData.sCamData.cameraID)
		
		IF _PERISCOPE__HAS_CAMERA_VALUES_CHANGED(sPeriscopeData)
			SET_CAM_ROT(sPeriscopeData.sCamData.cameraID, sPeriscopeData.sCamData.vRot)
			SET_CAM_FOV(sPeriscopeData.sCamData.cameraID, sPeriscopeData.sCamData.fFOV)
			
			SET_CAM_ACTIVE(sPeriscopeData.sCamData.cameraID, TRUE)
			PERISCOPE__UPDATE_TURNING_SOUND(sPeriscopeData)
		ELSE
			IF (sPeriscopeData.iBGLoopSoundID != -1)
				SET_VARIABLE_ON_SOUND(sPeriscopeData.iBGLoopSoundID, "TurnSpeed", 0)
			ENDIF
		ENDIF
		
		//Maintain ambient cam shake - can be interupted
		IF NOT IS_CAM_SHAKING(sPeriscopeData.sCamData.cameraID)
			SHAKE_CAM(sPeriscopeData.sCamData.cameraID, "HAND_SHAKE", 0.5)
		ENDIF
		
	ENDIF
	
ENDPROC

// *******************************************
// 					Targeting
// *******************************************

FUNC FLOAT VECTOR_LENGTH_SQ(VECTOR V)
	RETURN (V.x * V.x) + (V.y * V.y) + (V.z * V.z)
ENDFUNC

FUNC VECTOR SMOOTH_DAMP_VECTOR(VECTOR Current, VECTOR Target, FLOAT InterpSpeed)
	IF InterpSpeed <= 0.0
		RETURN Target
	ENDIF
	
	VECTOR Delta = Target - Current
	
	IF VECTOR_LENGTH_SQ(Delta) < 0.0001
		RETURN Target
	ENDIF

	Delta *= CLAMP(InterpSpeed * TIMESTEP(), 0.0, 1.0)
	
	RETURN Current + Delta
	
ENDFUNC

PROC _PERISCOPE_SET_LOCKEDON_STATE(PERISCOPE_LOCK_TARGET_DATA& sLockTarget, HOMING_LOCKON_STATE State)
	IF IS_ENTITY_ALIVE(sLockTarget.Entity) AND IS_ENTITY_A_VEHICLE(sLockTarget.Entity)
		SET_VEHICLE_HOMING_LOCKEDONTO_STATE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sLockTarget.Entity), State)
	ENDIF
ENDPROC

FUNC BOOL _PERISCOPE__CAN_TARGET_ENTITY(PERISCOPE_DATA& sPeriscopeData, ENTITY_INDEX entIndex)
	IF DOES_ENTITY_EXIST(entIndex)
	AND NOT IS_ENTITY_DEAD(entIndex)
		IF IS_ENTITY_A_VEHICLE(entIndex)
			VEHICLE_INDEX Veh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entIndex)
			IF Veh != sPeriscopeData.vehSubmarineIAmIn
				RETURN TRUE
			ENDIF
		ELIF IS_ENTITY_A_PED(entIndex)
			PED_INDEX playerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(entIndex)
			IF IS_PED_A_PLAYER(playerPed)
				IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(NETWORK_GET_PLAYER_INDEX_FROM_PED(playerPed), PLAYER_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC 

FUNC BOOL _PERISCOPE__HAS_TARGETED_ENTITY(PERISCOPE_TARGETING_DATA& sTargetingData, ENTITY_INDEX entIndex)
	INT iLoop
	REPEAT PERISCOPE_MAX_TARGETS iLoop
		IF sTargetingData.LockedTargets[iLoop].Entity = entIndex
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL _PERISCOPE__IS_TARGET_VALID(PERISCOPE_LOCK_TARGET_DATA &sLockTarget)
	IF sLockTarget.Entity != NULL AND DOES_ENTITY_EXIST(sLockTarget.Entity) AND NOT IS_ENTITY_DEAD(sLockTarget.Entity)
		FLOAT sx, sy
		IF GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(sLockTarget.Entity), sx, sy)
			VECTOR vScreenCenterRelativeCoord = <<sx, sy, 0>> - <<0.5, 0.5, 0>>
			RETURN GET_LENGTH_OF_VECTOR(vScreenCenterRelativeCoord) <= PERISCOPE_LOCK_BREAK_RADIUS_SS
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC PERISCOPE_LOCK_TARGET_DATA _PERISCOPE__FIND_BEST_TARGET(PERISCOPE_DATA& sPeriscopeData)

	INT iLoop
	FLOAT sx, sy
	FLOAT fBestPriority = 999999
	PERISCOPE_LOCK_TARGET_DATA BestTarget
	
	VECTOR CameraPosition
	FLOAT fMaxLockDistanceSq = PERISCOPE_MAX_LOCK_DISTANCE_WS * PERISCOPE_MAX_LOCK_DISTANCE_WS
	
	IF DOES_CAM_EXIST(sPeriscopeData.sCamData.cameraID)
	AND IS_CAM_ACTIVE(sPeriscopeData.sCamData.cameraID)
		CameraPosition = GET_CAM_COORD(sPeriscopeData.sCamData.cameraID)
	ELSE
		ASSERTLN("[PERISCOPE][_PERISCOPE__FIND_BEST_TARGET] Unable to get camera coords. ")
		// Fallback on sub coords if the cam somehow doesn't exist
		IF IS_ENTITY_ALIVE(sPeriscopeData.vehSubmarineIAmIn)
			CameraPosition = GET_ENTITY_COORDS(sPeriscopeData.vehSubmarineIAmIn)
		ENDIF
	ENDIF

	// Vehicle search
	VEHICLE_INDEX nearbyVehs[100]
	INT iNearbyVehs = GET_ALL_VEHICLES(nearbyVehs)
	REPEAT iNearbyVehs iLoop
		IF _PERISCOPE__CAN_TARGET_ENTITY(sPeriscopeData, nearbyVehs[iLoop]) AND NOT _PERISCOPE__HAS_TARGETED_ENTITY(sPeriscopeData.sTargetingData, nearbyVehs[iLoop])
			
			VECTOR EntityPosition = GET_ENTITY_COORDS(nearbyVehs[iLoop])
			FLOAT fWorldDistanceSq = GET_DISTANCE_BETWEEN_COORDS_SQUARED(CameraPosition, EntityPosition)
			
			//Ignore targets out of range
			IF fWorldDistanceSq > fMaxLockDistanceSq
				RELOOP
			ENDIF
			
			IF GET_SCREEN_COORD_FROM_WORLD_COORD(EntityPosition, sx, sy)
				VECTOR vScreenCenterRelativeCoord = <<sx, sy, 0>> - <<0.5, 0.5, 0>>
				FLOAT fDistanceFromScreenCenter = GET_LENGTH_OF_VECTOR(vScreenCenterRelativeCoord)
				IF fDistanceFromScreenCenter < PERISCOPE_MAX_LOCK_RADIUS_SS
					FLOAT fPriority = fWorldDistanceSq * fDistanceFromScreenCenter
					IF fPriority < fBestPriority
						fBestPriority = fPriority
						BestTarget.Entity = nearbyVehs[iLoop]
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
	ENDREPEAT	
	
	RETURN BestTarget
	
ENDFUNC

PROC _PERISCOPE__TARGETING_INIT(PERISCOPE_DATA& sPeriscopeData)
	PRINTLN("[PERISCOPE][_PERISCOPE__TARGETING_INIT]")
	REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")	
	sPeriscopeData.sTargetingData.PendingLockTarget.Entity = NULL
	sPeriscopeData.sTargetingData.iLockTargetCount = 0
	INT iLoop
	REPEAT PERISCOPE_MAX_TARGETS iLoop
		sPeriscopeData.sTargetingData.LockedTargets[iLoop].Entity = NULL
	ENDREPEAT
	sPeriscopeData.sTargetingData.iLockingSoundId = GET_SOUND_ID()
	sPeriscopeData.sTargetingData.iLockedSoundId = GET_SOUND_ID()
ENDPROC

PROC _PERISCOPE__TARGETING_CLEANUP(PERISCOPE_DATA& sPeriscopeData)
	PRINTLN("[PERISCOPE][_PERISCOPE__TARGETING_CLEANUP]")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("helicopterhud")
	STOP_AND_RELEASE_SOUND(sPeriscopeData.sTargetingData.iLockingSoundId)
	STOP_AND_RELEASE_SOUND(sPeriscopeData.sTargetingData.iLockedSoundId)
ENDPROC

PROC _PERISCOPE__AQUIRE_BEST_TARGET(PERISCOPE_DATA& sPeriscopeData)
	PERISCOPE_LOCK_TARGET_DATA BestTarget = _PERISCOPE__FIND_BEST_TARGET(sPeriscopeData)
	IF BestTarget.Entity != sPeriscopeData.sTargetingData.PendingLockTarget.Entity
		PRINTLN("[PERISCOPE][_PERISCOPE__AQUIRE_BEST_TARGET] - New Target=", NATIVE_TO_INT(BestTarget.Entity))
		sPeriscopeData.sTargetingData.PendingLockTarget = BestTarget
	ENDIF
ENDPROC

PROC _PERISCOPE__START_PENDING_LOCK(PERISCOPE_TARGETING_DATA& sTargetingData)
	IF sTargetingData.PendingLockTarget.Entity != NULL
		PRINTLN("[PERISCOPE][_PERISCOPE__START_PENDING_LOCK] - Entity=", NATIVE_TO_INT(sTargetingData.PendingLockTarget.Entity))
		sTargetingData.PendingLockTarget.LockState = LS_LOCKING
		RESET_NET_TIMER(sTargetingData.LockOnTimer)
	ELSE
		PRINTLN("[PERISCOPE][_PERISCOPE__START_PENDING_LOCK] - Failed!")
	ENDIF
ENDPROC

PROC _PERISCOPE__CANCEL_PENDING_LOCK(PERISCOPE_TARGETING_DATA &sTargetingData)

	IF sTargetingData.PendingLockTarget.Entity != NULL OR sTargetingData.PendingLockTarget.LockState != LS_DEFAULT
		PRINTLN("[PERISCOPE][_PERISCOPE__CANCEL_PENDING_LOCK]")
		sTargetingData.PendingLockTarget.Entity = NULL
		sTargetingData.PendingLockTarget.LockState = LS_DEFAULT
	ENDIF
ENDPROC

PROC _PERISCOPE__ADD_LOCK_TARGET(PERISCOPE_TARGETING_DATA &sTargetingData, PERISCOPE_LOCK_TARGET_DATA sLockTarget)
	IF sTargetingData.iLockTargetCount < PERISCOPE_MAX_TARGETS AND sLockTarget.Entity != NULL
		INT FreeIndex
		REPEAT PERISCOPE_MAX_TARGETS FreeIndex
			IF sTargetingData.LockedTargets[FreeIndex].Entity = NULL
				PRINTLN("[PERISCOPE][_PERISCOPE__ADD_LOCK_TARGET] Target=", FreeIndex, ", Entity=", NATIVE_TO_INT(sLockTarget.Entity))
				sTargetingData.LockedTargets[FreeIndex] = sLockTarget
				sTargetingData.LockedTargets[FreeIndex].LockState = LS_LOCKED
				sTargetingData.iLockTargetCount++
				EXIT
			ENDIF
		ENDREPEAT
		ASSERTLN("Failed to lock target - no free indices")
	ENDIF
ENDPROC

PROC _PERISCOPE__CLEAR_TARGETS(PERISCOPE_TARGETING_DATA &sTargetingData, BOOL bForce = FALSE)
	IF sTargetingData.PendingLockTarget.Entity != NULL OR sTargetingData.iLockTargetCount > 0 OR bForce
		PRINTLN("[PERISCOPE][_PERISCOPE__CLEAR_TARGETS] bForce=", bForce)
		INT Index
		REPEAT PERISCOPE_MAX_TARGETS Index
			sTargetingData.LockedTargets[Index].Entity = NULL
		ENDREPEAT
		_PERISCOPE__CANCEL_PENDING_LOCK(sTargetingData)
		sTargetingData.iLockTargetCount = 0
	ENDIF
ENDPROC

PROC _PERISCOPE__VALIDATE_TARGETS(PERISCOPE_DATA &sPeriscopeData)

	PERISCOPE_LOCK_TARGET_DATA Target = sPeriscopeData.sTargetingData.PendingLockTarget
	
	IF Target.Entity != NULL AND NOT _PERISCOPE__IS_TARGET_VALID(Target)
		_PERISCOPE__CANCEL_PENDING_LOCK(sPeriscopeData.sTargetingData)
	ENDIF
	
	INT iLoop
	REPEAT PERISCOPE_MAX_TARGETS iLoop
		Target = sPeriscopeData.sTargetingData.LockedTargets[iLoop]
		IF Target.Entity != NULL AND NOT _PERISCOPE__IS_TARGET_VALID(Target)
			sPeriscopeData.sTargetingData.LockedTargets[iLoop].Entity = NULL
			sPeriscopeData.sTargetingData.iLockTargetCount--
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_BLOCK_PERISCOPE_MISSILE()
	IF IS_PLAYER_IN_PASSIVE_MODE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC CONTROL_ACTION _PERISCOPE__GET_AIM_CONTROL()
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN INPUT_CURSOR_CANCEL
	ENDIF
	
	RETURN INPUT_FRONTEND_LT
ENDFUNC

FUNC CONTROL_ACTION _PERISCOPE__GET_FIRE_CONTROL()
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN INPUT_CURSOR_ACCEPT
	ENDIF
	
	RETURN INPUT_FRONTEND_RT
ENDFUNC

PROC _PERISCOPE__UPDATE_TARGETING(PERISCOPE_DATA &sPeriscopeData)
	
	IF SHOULD_BLOCK_PERISCOPE_MISSILE()
		EXIT
	ENDIF
	
	//Validate Targets
	_PERISCOPE__VALIDATE_TARGETS(sPeriscopeData)
	
	//Process lock-on
	IF NOT sPeriscopeData.sTargetingData.bIsFiring
	
		IF 	HAS_NET_TIMER_EXPIRED(sPeriscopeData.sTargetingData.LockCooldownTimer, PERISCOPE_LOCK_COOLDOWN_TIME)
			AND sPeriscopeData.sTargetingData.iLockTargetCount < PERISCOPE_MAX_TARGETS
			
			SWITCH sPeriscopeData.sTargetingData.PendingLockTarget.LockState
				CASE LS_DEFAULT
					_PERISCOPE__AQUIRE_BEST_TARGET(sPeriscopeData)	
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, _PERISCOPE__GET_AIM_CONTROL()) AND _PERISCOPE__IS_TARGET_VALID(sPeriscopeData.sTargetingData.PendingLockTarget)
						_PERISCOPE__START_PENDING_LOCK(sPeriscopeData.sTargetingData)
					ENDIF
				BREAK
				CASE LS_LOCKING
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, _PERISCOPE__GET_AIM_CONTROL())
					
						//Tell pending target we're locking them
						_PERISCOPE_SET_LOCKEDON_STATE(sPeriscopeData.sTargetingData.PendingLockTarget, HLOS_ACQUIRING)
					
						//Lock pending target after duration
						IF HAS_NET_TIMER_EXPIRED(sPeriscopeData.sTargetingData.LockOnTimer, PERISCOPE_LOCK_ON_TIME)
							_PERISCOPE__ADD_LOCK_TARGET(sPeriscopeData.sTargetingData, sPeriscopeData.sTargetingData.PendingLockTarget)
							_PERISCOPE__CANCEL_PENDING_LOCK(sPeriscopeData.sTargetingData)
						ENDIF
					ELSE 
						_PERISCOPE__CANCEL_PENDING_LOCK(sPeriscopeData.sTargetingData)
					ENDIF
				BREAK
			ENDSWITCH
		
		//Clear all locks when the button is released
		ELIF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, _PERISCOPE__GET_AIM_CONTROL())
			_PERISCOPE__CLEAR_TARGETS(sPeriscopeData.sTargetingData)	
		ENDIF
		
	ENDIF	
	
	//Tell targets we've locked them
	INT iLoop
	REPEAT PERISCOPE_MAX_TARGETS iLoop
		_PERISCOPE_SET_LOCKEDON_STATE(sPeriscopeData.sTargetingData.LockedTargets[iLoop], HLOS_ACQUIRED)
	ENDREPEAT

ENDPROC

PROC _PERISCOPE__UPDATE_AUDIO(PERISCOPE_DATA &sPeriscopeData)

	//Default stop all audio
	BOOL StopLockingAudio = TRUE
	BOOL StopLockedAudio = TRUE
	
	//Disable audio if paused or fading out
	IF IS_PAUSE_MENU_ACTIVE() OR IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		StopLockingAudio = TRUE
		StopLockedAudio = TRUE
	//Play Locking audio if there's a pending target
	ELIF sPeriscopeData.sTargetingData.PendingLockTarget.Entity != NULL
	AND sPeriscopeData.sTargetingData.PendingLockTarget.LockState = LS_LOCKING
		StopLockingAudio = FALSE
		StopLockedAudio = TRUE
		IF HAS_SOUND_FINISHED(sPeriscopeData.sTargetingData.iLockingSoundId)
			PLAY_SOUND_FRONTEND(sPeriscopeData.sTargetingData.iLockingSoundId, "VULKAN_LOCK_ON_AMBER")
		ENDIF
	//Play Locked audio when we have move than one target
	ELIF sPeriscopeData.sTargetingData.iLockTargetCount > 0
		StopLockingAudio = TRUE
		StopLockedAudio = FALSE
		IF HAS_SOUND_FINISHED(sPeriscopeData.sTargetingData.iLockedSoundId)
			PLAY_SOUND_FRONTEND(sPeriscopeData.sTargetingData.iLockedSoundId, "VULKAN_LOCK_ON_RED")
		ENDIF
	ENDIF	
	
	//Stop if desired
	IF StopLockingAudio AND NOT HAS_SOUND_FINISHED(sPeriscopeData.sTargetingData.iLockingSoundId)
		STOP_SOUND(sPeriscopeData.sTargetingData.iLockingSoundId)
	ENDIF
	IF StopLockedAudio AND NOT HAS_SOUND_FINISHED(sPeriscopeData.sTargetingData.iLockedSoundId)
		STOP_SOUND(sPeriscopeData.sTargetingData.iLockedSoundId)
	ENDIF
	
ENDPROC

FUNC BOOL _PERISCOPE_CAN_ARM_MISSILE(PERISCOPE_DATA &sPeriscopeData)
	IF SHOULD_BLOCK_PERISCOPE_MISSILE()
		RETURN FALSE
	ENDIF
	
	IF NOT sPeriscopeData.sTargetingData.bIsFiring
	AND HAS_NET_TIMER_EXPIRED(sPeriscopeData.sTargetingData.LockCooldownTimer, PERISCOPE_LOCK_COOLDOWN_TIME)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL _PERISCOPE_CAN_FIRE(PERISCOPE_DATA &sPeriscopeData)
	IF SHOULD_BLOCK_PERISCOPE_MISSILE()
		RETURN FALSE
	ENDIF
	
	IF NOT sPeriscopeData.sTargetingData.bIsFiring
	AND sPeriscopeData.sTargetingData.iLockTargetCount > 0
	AND NOT sPeriscopeData.sTargetingData.bFireRequiresControlRelease
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Main procedure for handling periscope weapon controls.
/// PARAMS:
///    sPeriscopeData - Periscope
PROC _PERISCOPE__HANDLE_WEAPON_CONTROLS(PERISCOPE_DATA &sPeriscopeData)
	
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, _PERISCOPE__GET_FIRE_CONTROL())
		IF _PERISCOPE_CAN_FIRE(sPeriscopeData)
			PRINTLN("[PERISCOPE][_PERISCOPE__HANDLE_WEAPON_CONTROLS] - Firing started")
			sPeriscopeData.sTargetingData.bFireRequiresControlRelease = TRUE
			sPeriscopeData.sTargetingData.bIsFiring = TRUE
		ENDIF
	ELSE
		sPeriscopeData.sTargetingData.bFireRequiresControlRelease = FALSE
	ENDIF
	
	IF sPeriscopeData.sTargetingData.bIsFiring
		INT iLoop
		REPEAT PERISCOPE_MAX_TARGETS iLoop
			ENTITY_INDEX TargetEntity = sPeriscopeData.sTargetingData.LockedTargets[iLoop].Entity
			IF TargetEntity != NULL AND SUBMARINE_FIRE_HOMING_MISSILE(sPeriscopeData.vehSubmarineIAmIn, TargetEntity)
				sPeriscopeData.sTargetingData.LockedTargets[iLoop].Entity = NULL
				sPeriscopeData.sTargetingData.iLockTargetCount--
				SHAKE_CAM(sPeriscopeData.sCamData.cameraID,"MEDIUM_EXPLOSION_SHAKE",0.1)
				INCREMENT_SUBMARINE_PROJECTILE_FIRED()
				EXIT
			ENDIF
		ENDREPEAT
		IF sPeriscopeData.sTargetingData.iLockTargetCount <= 0
			PRINTLN("[PERISCOPE][_PERISCOPE__HANDLE_WEAPON_CONTROLS] - Firing ended")
			sPeriscopeData.sTargetingData.bIsFiring = FALSE
			RESET_NET_TIMER(sPeriscopeData.sTargetingData.LockCooldownTimer)
			_PERISCOPE__CLEAR_TARGETS(sPeriscopeData.sTargetingData, TRUE)
		ENDIF
	ENDIF
ENDPROC

// *******************************************
// 					DRAW
// *******************************************

CONST_FLOAT cfDEFAULT_ASPECT_RATIO 1.7778
CONST_INT ciTILE_COUNT 36  //one for every 10 degrees
FLOAT fAspectRatio
FLOAT fAspectMod
INT iScreenX
INT iScreenY
CONST_FLOAT fDesignResolutionX 1920.0
CONST_FLOAT fDesignResolutionY 1080.0

FUNC INT GET_SCOPE_ALPHA()
	INT Result = PERISCOPE_SCOPE_ALPHA 
	#IF IS_DEBUG_BUILD
		Result = iDebugScopeAlpha
	#ENDIF
	RETURN Result
ENDFUNC

/// PURPOSE:
///    Request the dds texture for the scope.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _PERISCOPE__DRAW_INIT(PERISCOPE_DATA &sPeriscopeData)

	PRINTLN("[PERISCOPE][_PERISCOPE__DRAW_INIT]")
	sPeriscopeData.sTextures.Dictionary =			"mpsubmarine_periscope"
	sPeriscopeData.sTextures.ScopeOverlay = 		"ScopeOverlay"
	sPeriscopeData.sTextures.CrosshairLine = 		"CrosshairLine"
	sPeriscopeData.sTextures.Bars = 				"Bars"
	sPeriscopeData.sTextures.Numbers = 				"Numbers"
	sPeriscopeData.sTextures.Target_Default = 		"Target_Default"
	sPeriscopeData.sTextures.Target_LockingOn = 	"Target_LockingOn"
	sPeriscopeData.sTextures.Target_LockedOn = 		"Target_LockedOn"
	
	REQUEST_STREAMED_TEXTURE_DICT(sPeriscopeData.sTextures.Dictionary)

	IF HAS_STREAMED_TEXTURE_DICT_LOADED(sPeriscopeData.sTextures.Dictionary)
		sPeriscopeData.sTextures.ScopeOverlay_Resolution = GET_TEXTURE_RESOLUTION(sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.ScopeOverlay)
		sPeriscopeData.sTextures.CrosshairLine_Resolution = GET_TEXTURE_RESOLUTION(sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.CrosshairLine)
		sPeriscopeData.sTextures.Bars_Resolution = GET_TEXTURE_RESOLUTION(sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.Bars)
		sPeriscopeData.sTextures.Numbers_Resolution = GET_TEXTURE_RESOLUTION(sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.Numbers)
		sPeriscopeData.sTextures.Target_Default_Resolution = GET_TEXTURE_RESOLUTION(sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.Target_Default)
		sPeriscopeData.sTextures.Target_LockingOn_Resolution = GET_TEXTURE_RESOLUTION(sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.Target_LockingOn)
		sPeriscopeData.sTextures.Target_LockedOn_Resolution = GET_TEXTURE_RESOLUTION(sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.Target_LockedOn)
	ENDIF
	
ENDPROC

FUNC FLOAT FLOOR_PIXEL(FLOAT Value, INT Resolution)	
	RETURN Value - (Value % (1.0/TO_FLOAT(Resolution)))
ENDFUNC

PROC _PERISCOPE__ADJUST_SPRITE_TRANSFORM(FLOAT& CenterX, FLOAT& CenterY, FLOAT& Width, FLOAT& Height)

	//Normalise design resolution
	CenterX /= fDesignResolutionX
	CenterY /= fDesignResolutionY
	Width /= fDesignResolutionX
	Height /= fDesignResolutionY

	//Correct for aspect - center aligned
	CenterX = 0.5 - ((0.5 - CenterX) * fAspectMod)
	Width *= fAspectMod

	//align to pixel actual boundary
	Width = FLOOR_PIXEL(Width, iScreenX)
	Height = FLOOR_PIXEL(Height, iScreenY)
	FLOAT hw = Width * 0.5
	FLOAT hh = Height * 0.5
	CenterX = FLOOR_PIXEL(CenterX - hw, iScreenX) + hw
	CenterY = FLOOR_PIXEL(CenterY - hh, iScreenY) + hh
	
ENDPROC

PROC _PERISCOPE__DRAW_SPRITE_UV(STRING DictionaryionaryName, STRING TextureName, FLOAT CenterX, FLOAT CenterY, FLOAT Width, FLOAT Height, FLOAT uMin, FLOAT uMax, FLOAT vMin,FLOAT vMax, FLOAT Rotation, INT R, INT G, INT B, INT A)
	
	_PERISCOPE__ADJUST_SPRITE_TRANSFORM(CenterX, CenterY, Width, Height)
	
	DRAW_SPRITE_ARX_WITH_UV(DictionaryionaryName, TextureName, 
							CenterX, CenterY, Width, Height,
							uMin, vMin, uMax, vMax,
							Rotation,
							R, G, B, A)
ENDPROC

PROC _PERISCOPE__DRAW_SPRITE(STRING DictionaryionaryName, STRING TextureName, FLOAT CenterX, FLOAT CenterY, FLOAT Width, FLOAT Height, FLOAT Rotation, INT R, INT G, INT B, INT A, BOOL DoStereorize = FALSE)

	_PERISCOPE__ADJUST_SPRITE_TRANSFORM(CenterX, CenterY, Width, Height)
	
	DRAW_SPRITE_ARX(DictionaryionaryName, TextureName, 
					CenterX, CenterY, Width, Height,
					Rotation,
					R, G, B, A,
					DoStereorize)
ENDPROC

PROC _PERISCOPE_DRAW_NO_TARGET_BOX(PERISCOPE_DATA& sPeriscopeData)
	TEXT_LABEL_31 Texture = sPeriscopeData.sTextures.Target_Default
	FLOAT Width = sPeriscopeData.sTextures.Target_Default_Resolution.x
	FLOAT Height = sPeriscopeData.sTextures.Target_Default_Resolution.y
	HUD_COLOURS Colour = HUD_COLOUR_GREEN
	
	INT R, G, B, A
	GET_HUD_COLOUR(Colour, r, g, b, a)
	
	_PERISCOPE__DRAW_SPRITE(sPeriscopeData.sTextures.Dictionary, Texture,
							fDesignResolutionX/2.0, fDesignResolutionY/2.0, Width, Height,
							0,
							R, G, B, GET_SCOPE_ALPHA(),
							TRUE)
ENDPROC

PROC _PERISCOPE_DRAW_TARGET_BOX(PERISCOPE_DATA& sPeriscopeData, PERISCOPE_LOCK_TARGET_DATA& sLockTarget)

	IF _PERISCOPE__IS_TARGET_VALID(sLockTarget)
		
		//------Determine visualisation------
		TEXT_LABEL_31 Texture = ""
		FLOAT Width = 0
		FLOAT Height = 0
		HUD_COLOURS Colour = HUD_COLOUR_WHITE
		FLOAT LockAlpha = 0.0
		
		SWITCH sLockTarget.LockState
			CASE LS_DEFAULT
				EXIT
			BREAK
			CASE LS_LOCKING
				LockAlpha = TO_FLOAT(GET_TIME_USED_BY_SCRIPT_TIMER_IN_MILLISECONDS(sPeriscopeData.sTargetingData.LockOnTimer)) / TO_FLOAT(PERISCOPE_LOCK_ON_TIME)
				IF LockAlpha < 0.5
					Texture = sPeriscopeData.sTextures.Target_Default
					Width = sPeriscopeData.sTextures.Target_Default_Resolution.x
					Height = sPeriscopeData.sTextures.Target_Default_Resolution.y
					Colour = HUD_COLOUR_GREEN
				ELSE
					Texture = sPeriscopeData.sTextures.Target_LockingOn
					Width = sPeriscopeData.sTextures.Target_LockingOn_Resolution.x
					Height = sPeriscopeData.sTextures.Target_LockingOn_Resolution.y
					Colour = HUD_COLOUR_ORANGE
				ENDIF
			BREAK
			CASE LS_LOCKED
				Texture = sPeriscopeData.sTextures.Target_LockedOn
				Width = sPeriscopeData.sTextures.Target_LockedOn_Resolution.x
				Height = sPeriscopeData.sTextures.Target_LockedOn_Resolution.y
				Colour = HUD_COLOUR_RED
				LockAlpha = 1.0
			BREAK
		ENDSWITCH
		
		FLOAT InvLockAlpha = CLAMP(1.0 - LockAlpha, 0.0, 1.0)
		
		INT R, G, B, A
		GET_HUD_COLOUR(Colour, r, g, b, a)
		
		//------Error Offset------		
		VECTOR ErrorOffset
		IF sLockTarget.LockState != LS_LOCKED
			FLOAT ErrorRadius = 3
#IF IS_DEBUG_BUILD 
			IF fDebugTargetErrorRadius > 0 
				ErrorRadius = fDebugTargetErrorRadius 
			ENDIF 
#ENDIF
			ErrorOffset = GET_RANDOM_POINT_ON_CIRCUMFERENCE(<<0,0,0>>, ErrorRadius * InvLockAlpha)
			
			FLOAT fSmoothDampSpeed = 10.0
#IF IS_DEBUG_BUILD 
			IF fDebugTargetSmoothDampSpeed > 0 
				fSmoothDampSpeed = fDebugTargetSmoothDampSpeed 
			ENDIF 
#ENDIF
			
			sLockTarget.Offset = SMOOTH_DAMP_VECTOR(sLockTarget.Offset, ErrorOffset, fSmoothDampSpeed)
			
		ELSE //sLockTarget.LockState = LS_LOCKED
			sLockTarget.Offset = <<0,0,0>>
		ENDIF
		
		//------Flash------
		BOOL VisibleThisFrame = TRUE
		IF sLockTarget.LockState != LS_LOCKED
			FLOAT FlashPeriod = 0.1 + 0.4 * InvLockAlpha
			sLockTarget.FlashTick = (sLockTarget.FlashTick + TIMESTEP()) % FlashPeriod
			VisibleThisFrame = sLockTarget.FlashTick >= FlashPeriod/2
		ELSE
			sLockTarget.FlashTick = 0
		ENDIF
		
		//------Draw------
		IF VisibleThisFrame
			VECTOR Center = sLockTarget.Offset
			
			SET_DRAW_ORIGIN(GET_ENTITY_COORDS(sLockTarget.Entity))
			
			Center.x *= fAspectMod / fDesignResolutionX
			Center.y /= fDesignResolutionY
			Width *= fAspectMod / fDesignResolutionX
			Height /= fDesignResolutionY
			
			DRAW_SPRITE(sPeriscopeData.sTextures.Dictionary, Texture,
						Center.x, Center.y, Width, Height,
						0,
						R, G, B, GET_SCOPE_ALPHA(),
						TRUE)
									
			CLEAR_DRAW_ORIGIN()
		ENDIF
					
	ENDIF	

ENDPROC

PROC _PERISCOPE__DRAW_CROSSHAIR(PERISCOPE_DATA &sPeriscopeData, HUD_COLOURS Colour)

	#IF IS_DEBUG_BUILD
	IF bDebugHideScopeCrosshair
		EXIT
	ENDIF
	#ENDIF

	FLOAT fCrosshairSize = sPeriscopeData.sTextures.CrosshairLine_Resolution.y
	
	INT R, G, B, A
	GET_HUD_COLOUR(Colour, R, G, B, A)
	
	//Horizontal
	_PERISCOPE__DRAW_SPRITE(sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.CrosshairLine, 
							0.5 * fDesignResolutionX, 0.5 * fDesignResolutionY, fDesignResolutionX, fCrosshairSize, 
							0, 
							R, G, B, GET_SCOPE_ALPHA())	
	//Vertical			
	_PERISCOPE__DRAW_SPRITE(sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.CrosshairLine, 
							0.5 * fDesignResolutionX, 0.5 * fDesignResolutionY, fDesignResolutionX, fCrosshairSize, 
							90, 
							R, G, B, GET_SCOPE_ALPHA())	
ENDPROC

PROC _PERISCOPE__DRAW_SCOPE_BAR_NUMBERS(PERISCOPE_DATA &sPeriscopeData, FLOAT BarOffsetX, FLOAT BarLength, FLOAT CenterX, FLOAT CenterY, FLOAT Rotation, HUD_COLOURS Color, BOOL Reversed = FALSE)

	#IF IS_DEBUG_BUILD
	IF bDebugHideScopeBarNumbers
		EXIT
	ENDIF
	#ENDIF

	FLOAT SpriteWidth = 1.0 / TO_FLOAT(sPeriscopeData.sTextures.Numbers_SpritesX)
	FLOAT SpriteHeight = 1.0 / TO_FLOAT(sPeriscopeData.sTextures.Numbers_SpritesY)
	
	VECTOR NumberCenter = <<CenterX, CenterY, 0.0>>
	
	FLOAT NumberWidth = sPeriscopeData.sTextures.Numbers_Resolution.x / sPeriscopeData.sTextures.Numbers_SpritesX
	FLOAT NumberHeight = sPeriscopeData.sTextures.Numbers_Resolution.y / sPeriscopeData.sTextures.Numbers_SpritesY
	
	FLOAT NumberStepX = sPeriscopeData.sTextures.Bars_Resolution.x
	IF Reversed
		NumberStepX = -NumberStepX
	ENDIF
	
	//Offset numbers to the top of the bar
	FLOAT NumberOffsetY = -sPeriscopeData.sTextures.Numbers_Resolution.y * 0.5
	
	INT R, G, B, A
	GET_HUD_COLOUR(Color, R, G, B, A)
	
	INT iTile
	REPEAT ciTILE_COUNT iTile
		
		FLOAT TileX = WRAP((NumberStepX * TO_FLOAT(iTile)) + BarOffsetX, -BarLength/2.0, BarLength/2.0)
		
		VECTOR NumberPosition = NumberCenter + ROTATE_VECTOR_ABOUT_Z(<<TileX, NumberOffsetY, 0.0>>, Rotation)
		
		INT GridX = iTile % sPeriscopeData.sTextures.Numbers_SpritesX
		INT GridY = iTile / sPeriscopeData.sTextures.Numbers_SpritesY
		
		FLOAT uMin = TO_FLOAT(GridX) * SpriteWidth
		FLOAT uMax = TO_FLOAT(GridX+1) * SpriteWidth
		FLOAT vMin = TO_FLOAT(GridY) * SpriteHeight
		FLOAT vMax = TO_FLOAT(GridY+1) * SpriteHeight
		
		_PERISCOPE__DRAW_SPRITE_UV(	sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.Numbers,
									NumberPosition.x, NumberPosition.y, NumberWidth, NumberHeight,
									uMin, uMax, vMin, vMax,
									0,
									R, G, B, GET_SCOPE_ALPHA())
									
	ENDREPEAT
	
	
ENDPROC

//Draw scope navigation bar - in pixels/degrees
PROC _PERISCOPE__DRAW_SCOPE_BAR(PERISCOPE_DATA &sPeriscopeData, FLOAT NavHeadingDeg, FLOAT CenterX, FLOAT CenterY, FLOAT Rotation, HUD_COLOURS BarColour, HUD_COLOURS NumbersColour, BOOL ReverseNumbers = FALSE)
	
	#IF IS_DEBUG_BUILD
	IF bDebugHideScopeBars
		EXIT
	ENDIF
	#ENDIF

	FLOAT Offset = -WRAP(NavHeadingDeg, 0, 360) / 360
		
	FLOAT TileWidth = sPeriscopeData.sTextures.Bars_Resolution.x
	FLOAT TileHeight = sPeriscopeData.sTextures.Bars_Resolution.y
		
	FLOAT BarLength = ciTILE_COUNT * TileWidth
	
	FLOAT uMin = ciTILE_COUNT * (0.0 + Offset) + 0.5
	FLOAT uMax = ciTILE_COUNT * (1.0 + Offset) + 0.5
	FLOAT vMin = 0
	FLOAT vMax = 1
	
	INT R, G, B, A
	GET_HUD_COLOUR(BarColour, R, G, B, A)
	
	_PERISCOPE__DRAW_SPRITE_UV(	sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.Bars, 
								CenterX, CenterY, BarLength, TileHeight, //Center and size
								uMin, uMax, vMin, vMax, //UVs
								Rotation, //Rotation
								R, G, B, GET_SCOPE_ALPHA())
	
	_PERISCOPE__DRAW_SCOPE_BAR_NUMBERS(sPeriscopeData, -Offset * BarLength, BarLength, CenterX, CenterY, Rotation, NumbersColour, ReverseNumbers)
	
ENDPROC

/// PURPOSE:
///    Draw the scope texture on screen.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _PERISCOPE__DRAW(PERISCOPE_DATA &sPeriscopeData)

	#IF IS_DEBUG_BUILD
	IF bDebugHideScope
	OR bDebugDontRenderPeriscopeCam
		EXIT
	ENDIF
	#ENDIF
	
	GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
	fAspectRatio = GET_ASPECT_RATIO(FALSE)
	//Fix for Eyefinity triple monitors: the resolution returns as the full three-screen width but the game is only drawn on one screen.
	IF IS_PC_VERSION()
		IF fAspectRatio >= 4.0
			fAspectRatio = fAspectRatio / 3.0
		ENDIF
	ENDIF
	fAspectMod = cfDEFAULT_ASPECT_RATIO / fAspectRatio
		
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(sPeriscopeData.sTextures.Dictionary)
	AND DOES_CAM_EXIST(sPeriscopeData.sCamData.cameraID)
	AND IS_CAM_ACTIVE(sPeriscopeData.sCamData.cameraID)
		
		FLOAT fDesignCenterX = fDesignResolutionX * 0.5
		FLOAT fDesignCenterY = fDesignResolutionY * 0.5
		
		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
		SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
		
		//Scope bars
		VECTOR CameraRot = GET_CAM_ROT(sPeriscopeData.sCamData.cameraID, EULER_XYZ)
		FLOAT fPitch = CameraRot.x ////sPeriscopeData.sCamData.vRot.x
		FLOAT fYaw = CameraRot.z //sPeriscopeData.sCamData.vRot.z
		CONST_FLOAT SCOPE_CIRCLE_SIZE 660.0
		FLOAT fBarOffset = (SCOPE_CIRCLE_SIZE - (sPeriscopeData.sTextures.Bars_Resolution.y * 0.5))
		
		_PERISCOPE__DRAW_SCOPE_BAR(sPeriscopeData, fPitch, 	fDesignCenterX-fBarOffset, fDesignCenterY,	90, HUD_COLOUR_GREEN, HUD_COLOUR_GREEN, TRUE)
		_PERISCOPE__DRAW_SCOPE_BAR(sPeriscopeData, fYaw, 	fDesignCenterX, fDesignCenterY+fBarOffset, 	0 , HUD_COLOUR_GREEN, HUD_COLOUR_GREEN)
		
		//Crosshair
		_PERISCOPE__DRAW_CROSSHAIR(sPeriscopeData, HUD_COLOUR_GREEN)
		
		//Targets
		IF _PERISCOPE_CAN_ARM_MISSILE(sPeriscopeData)
		AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, _PERISCOPE__GET_AIM_CONTROL())
		AND sPeriscopeData.sTargetingData.PendingLockTarget.Entity = NULL 
		AND sPeriscopeData.sTargetingData.iLockTargetCount <= 0
			_PERISCOPE_DRAW_NO_TARGET_BOX(sPeriscopeData)
		ELSE
			_PERISCOPE_DRAW_TARGET_BOX(sPeriscopeData, sPeriscopeData.sTargetingData.PendingLockTarget)
		ENDIF
		
		int iLoop
		REPEAT PERISCOPE_MAX_TARGETS iLoop
			_PERISCOPE_DRAW_TARGET_BOX(sPeriscopeData, sPeriscopeData.sTargetingData.LockedTargets[iLoop])
		ENDREPEAT
		
		//Overlay
		_PERISCOPE__DRAW_SPRITE(sPeriscopeData.sTextures.Dictionary, sPeriscopeData.sTextures.ScopeOverlay, 
								fDesignCenterX, fDesignCenterY, sPeriscopeData.sTextures.ScopeOverlay_Resolution.x, sPeriscopeData.sTextures.ScopeOverlay_Resolution.y, 
								0, 
								255, 255, 255, 255)
								
		//Side black bars (if necessary)
		FLOAT OverlayScreenWidth = ((sPeriscopeData.sTextures.ScopeOverlay_Resolution.x - 5.0) / fDesignResolutionX) * fAspectMod
		IF OverlayScreenWidth < 1.0
			FLOAT BarWidth = (1.0 - OverlayScreenWidth) / 2.0
			DRAW_RECT(BarWidth/2.0, 		0.5, BarWidth, 1.0, 0, 0, 0, 255)
			DRAW_RECT(1.0 - BarWidth/2.0, 	0.5, BarWidth, 1.0, 0, 0, 0, 255)
		ENDIF
	ENDIF
	
ENDPROC

// *******************************************
// 			INSTUCTIONAL BUTTONS
// *******************************************

/// PURPOSE:
///    Adds instructional buttons for when the player is looking through the scope.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _PERISCOPE__UPDATE_INSTRUCTIONAL_BUTTONS(PERISCOPE_DATA &sPeriscopeData)
	
	RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(sPeriscopeData.sInstructButtons.sButtons)
	
	// Exit Scope
	ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "PERISCOPE_EXIT", sPeriscopeData.sInstructButtons.sButtons)
	
	// Fire
	IF _PERISCOPE_CAN_FIRE(sPeriscopeData)
		ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, _PERISCOPE__GET_FIRE_CONTROL()), "PERISCOPE_FIRE", sPeriscopeData.sInstructButtons.sButtons)
	ENDIF
	
	// Arm missiles
	IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, _PERISCOPE__GET_AIM_CONTROL()) AND _PERISCOPE_CAN_ARM_MISSILE(sPeriscopeData)
		ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, _PERISCOPE__GET_AIM_CONTROL()), "PERISCOPE_ARM", sPeriscopeData.sInstructButtons.sButtons)			
	ENDIF
ENDPROC

/// PURPOSE:
///    Draw the instructional buttons while in the scope camera
/// PARAMS:
///    sPeriscopeData - 
PROC _PERISCOPE__DRAW_INSTRUCTIONAL_BUTTONS(PERISCOPE_DATA &sPeriscopeData)

	#IF IS_DEBUG_BUILD
	IF bDebugHideInputPrompts
		EXIT
	ENDIF
	#ENDIF
	sPeriscopeData.sInstructButtons.sPlacement = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD_PRIORITY_HIGH)
	
	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sPeriscopeData.sInstructButtons.ScaleformIndex, sPeriscopeData.sInstructButtons.sPlacement, sPeriscopeData.sInstructButtons.sButtons)
	
ENDPROC

/// PURPOSE:
///    Resets the scope instruictional buttons.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _PERISCOPE__CLEANUP_INSTRUCTIONAL_BUTTONS(PERISCOPE_DATA &sPeriscopeData)

	PRINTLN("[PERISCOPE][_PERISCOPE__CLEANUP_INSTRUCTIONAL_BUTTONS] Cleaning up instructional buttons.")

	IF HAS_SCALEFORM_MOVIE_LOADED(sPeriscopeData.sInstructButtons.ScaleformIndex)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sPeriscopeData.sInstructButtons.ScaleformIndex)
	ENDIF

ENDPROC

// *******************************************
// 			BAIL OUT
// *******************************************

/// PURPOSE:
///    Completely exit the periscope if something is not right.
/// PARAMS:
///    sPeriscopeData - sPeriscope
PROC _PERISCOPE__BAIL_OUT(PERISCOPE_DATA &sPeriscopeData)
	
	PRINTLN("[PERISCOPE][_PERISCOPE__BAIL_OUT] Bailing out of periscope...")
	
	IF IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_IN()
	AND NOT IS_PLAYER_LEAVING_SUBMARINE_DRIVER_SEAT()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
	ENDIF
	
	IF _PERISCOPE__IS_CAMERA_ENABLED(sPeriscopeData)
		_PERISCOPE__ENABLE_CAMERA(sPeriscopeData, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	
	PRINT_HELP(_PERISCOPE__GET_BAIL_HELP_TEXT_LABEL(), DEFAULT_HELP_TEXT_TIME)
	
	IF IS_PLAYER_LEAVING_SUBMARINE_DRIVER_SEAT()
	   _PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_CLEANUP)
	ELSE
	   _PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_START_ANIM_EXIT)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Add conditions for when the persicope access should be blocked. 
/// PARAMS:
///    sPeriscopeData - Periscope
FUNC BOOL _PERISCOPE__SHOULD_PERISCOPE_ACCESS_BE_BLOCKED(PERISCOPE_DATA &sPeriscopeData)

	BOOL bBlockPeriscope = FALSE
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
	OR NOT IS_GAMEPLAY_CAM_RENDERING()
	OR NOT IS_SKYSWOOP_AT_GROUND()
		bBlockPeriscope = TRUE
	ENDIF
	
	// Somebody else is using it.
	IF IS_BIT_SET(sPeriscopeData.iBS, BS_REMOTE_PLAYER_IS_USING_PERISCOPE)
	AND NOT IS_BIT_SET(sPeriscopeData.iBS, BS_LOCAL_PLAYER_IS_USING_PERISCOPE)
		DISPLAY_HELP_TEXT_THIS_FRAME(_PERISCOPE__GET_IN_USE_HELP_TEXT_LABEL(), FALSE)
		_PERISCOPE__SET_PED_CAPSULE()
		bBlockPeriscope = TRUE	
	ENDIF
	
	// Something has disabled it.
	IF IS_BIT_SET(sPeriscopeData.iBS, BS_SET_PERISCOPE_UNAVAILABLE)
	#IF IS_DEBUG_BUILD
	OR (bDebugDisablePeriscope)
	#ENDIF
		DISPLAY_HELP_TEXT_THIS_FRAME(_PERISCOPE__GET_BAIL_HELP_TEXT_LABEL(), FALSE)
		bBlockPeriscope = TRUE	
	ENDIF
	
	/*
	// Submarine is too deep
	IF IS_ENTITY_ALIVE(sPeriscopeData.vehSubmarineIAmIn)
		VECTOR vSubCoords = GET_ENTITY_COORDS(sPeriscopeData.vehSubmarineIAmIn)
		FLOAT fTempMaxDepth = sPeriscopeData.sCamData.fMaxDepth
		
		IF (g_sMPTunables.fPERISCOPE_MAX_DEPTH > 0.0)
			fTempMaxDepth = g_sMPTunables.fPERISCOPE_MAX_DEPTH
		ENDIF
		
		IF (vSubCoords.z < -fTempMaxDepth)
		AND NOT (fTempMaxDepth = 0)
			DISPLAY_HELP_TEXT_THIS_FRAME("SUB_PERI_DEPTH", FALSE)
			bBlockPeriscope = TRUE
		ENDIF
	ENDIF
	*/
	
	// Periscope cam is under water
	IF NOT _PERISCOPE__IS_CAM_ABOVE_WATER_HEIGHT(sPeriscopeData, FALSE)
	AND NOT bBlockPeriscope
		DISPLAY_HELP_TEXT_THIS_FRAME("SUB_PERI_DEPTH", FALSE)
		bBlockPeriscope = TRUE
	ENDIF
	
	// Player is not alone in the periscope area.
	IF NOT _PERISCOPE__IS_PLAYER_ALONE_IN_SUBMARINE_PERISCOPE_AREA(sPeriscopeData)
	AND NOT bBlockPeriscope
		DISPLAY_HELP_TEXT_THIS_FRAME("PERI_TOO_MANY", FALSE)
		bBlockPeriscope = TRUE
	ENDIF
	
	// Block Access
	IF (bBlockPeriscope)
		IF (sPeriscopeData.iContextButtonID != NEW_CONTEXT_INTENTION)
			RELEASE_CONTEXT_INTENTION(sPeriscopeData.iContextButtonID)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL _PERISCOPE__SHOULD_PLAYER_BE_KICKED_OUT(PERISCOPE_DATA &sPeriscopeData)

	IF _PERISCOPE__IS_CAMERA_ENABLED(sPeriscopeData)
		BOOL bKickPlayerOut = FALSE
		
		IF IS_BIT_SET(sPeriscopeData.iBS, BS_KICKED_OUT_OF_PERISCOPE)
			RETURN FALSE
		ENDIF
		
		IF NETWORK_IS_IN_MP_CUTSCENE()
		OR NOT IS_SKYSWOOP_AT_GROUND()
			bKickPlayerOut = TRUE
		ENDIF
		
		// If players are able to be kicked out via the bit, add conditions here.
		IF IS_BIT_SET(sPeriscopeData.iBS, BS_PLAYER_CAN_BE_KICKED_OUT)
		
			// Somebody else is using it.
			IF IS_BIT_SET(sPeriscopeData.iBS, BS_REMOTE_PLAYER_IS_USING_PERISCOPE)
			AND NOT IS_BIT_SET(sPeriscopeData.iBS, BS_LOCAL_PLAYER_IS_USING_PERISCOPE)
				PRINTLN("[PERISCOPE][_PERISCOPE__SHOULD_PLAYER_BE_KICKED_OUT] Yes. Bit is set and anohter player wants to use it.")
				_PERISCOPE__SET_KICK_OUT_REASON(sPeriscopeData, KICK_USED_BY_REMOTE_PLAYER)
				bKickPlayerOut = TRUE	
			ENDIF
			
		ENDIF
		
		/*
		// Submarine is too deep
		IF IS_ENTITY_ALIVE(sPeriscopeData.vehSubmarineIAmIn)
			VECTOR vSubCoords = GET_ENTITY_COORDS(sPeriscopeData.vehSubmarineIAmIn)
			FLOAT fTempMaxDepth = sPeriscopeData.sCamData.fMaxDepth
			
			IF (g_sMPTunables.fPERISCOPE_MAX_DEPTH > 0.0)
				fTempMaxDepth = g_sMPTunables.fPERISCOPE_MAX_DEPTH
			ENDIF
			
			IF (vSubCoords.z < -fTempMaxDepth)
			AND NOT (fTempMaxDepth = 0)
				_PERISCOPE__SET_KICK_OUT_REASON(sPeriscopeData, KICK_SUB_TOO_DEEP)
				bKickPlayerOut = TRUE
			ENDIF
		ENDIF
		*/
		
		// Periscope cam is under water
		IF NOT _PERISCOPE__IS_CAM_ABOVE_WATER_HEIGHT(sPeriscopeData, TRUE)
			_PERISCOPE__SET_KICK_OUT_REASON(sPeriscopeData, KICK_SUB_TOO_DEEP)
			bKickPlayerOut = TRUE
		ENDIF
		
		//Simple interior needs to kick the player
		IF SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_SUBMARINE(SIMPLE_INTERIOR_SUBMARINE, 0)
			_PERISCOPE__SET_KICK_OUT_REASON(sPeriscopeData, KICK_SMPL_INT_REQUEST)
			bKickPlayerOut = TRUE
		ENDIF
		
		// Block Access
		IF (bKickPlayerOut)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Display help text based off the reason.
/// PARAMS:
PROC _PERISCOPE__PRINT_KICKED_OUT_HELP_TEXT(PERISCOPE_DATA &sPeriscopeData)
	
	STRING strReason
	
	SWITCH sPeriscopeData.eKickOutReason
		CASE KICK_SUB_TOO_DEEP
			strReason = "SUB_PERI_DEPTH"
		BREAK
		CASE KICK_USED_BY_REMOTE_PLAYER
			strReason = _PERISCOPE__GET_IN_USE_HELP_TEXT_LABEL()
		BREAK
		DEFAULT
			strReason = _PERISCOPE__GET_BAIL_HELP_TEXT_LABEL()
		BREAK
	ENDSWITCH
	
	PRINTLN("[PERISCOPE][_PERISCOPE__PRINT_KICKED_OUT_HELP_TEXT] Printing help text: ", strReason)
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_HELP_MESSAGE_ON_SCREEN()	
		CLEAR_HELP()
	ENDIF
	
	PRINT_HELP(strReason)
	
ENDPROC

/// PURPOSE:
///    Kicks the player out of the periscope.
/// PARAMS:
///    sPeriscopeData - 
PROC _PERISCOPE__KICK_PLAYER_OUT(PERISCOPE_DATA &sPeriscopeData)
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_KICKED_OUT_OF_PERISCOPE, TRUE)
	PRINTLN("[PERISCOPE][_PERISCOPE__KICK_PLAYER_OUT] Kicked out of cam, fading screen out")
ENDPROC

// *******************************************
// 					STAGES
// *******************************************

/// PURPOSE:
///    Called every frame while the player is out of range of the periscope.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _STAGE_PLAYER_OUT_OF_RANGE(PERISCOPE_DATA &sPeriscopeData)

	// Release input for accessing the periscope.
	IF (sPeriscopeData.iContextButtonID != NEW_CONTEXT_INTENTION)
		RELEASE_CONTEXT_INTENTION(sPeriscopeData.iContextButtonID)
	ENDIF
	
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_LOCAL_PLAYER_IS_USING_PERISCOPE, FALSE)
	SET_SUBMARINE_USING_PERISCOPE(FALSE)
	
	// Wait for input if player enters area.
	IF _PERISCOPE__IS_PLAYER_WITHIN_RANGE(PLAYER_ID(), sPeriscopeData)
		PRINTLN("[PERISCOPE][_STAGE_PLAYER_OUT_OF_RANGE] Player is now in range: ", GET_PLAYER_NAME(PLAYER_ID()))
		_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_INIT_FOR_USE)
	ENDIF
	
ENDPROC

PROC _STATE_INIT_FOR_USE(PERISCOPE_DATA &sPeriscopeData)

	_PERISCOPE__DRAW_INIT(sPeriscopeData)
	_PERISCOPE__CAMERA_INIT(sPeriscopeData)
	_PERISCOPE__TARGETING_INIT(sPeriscopeData)
	_PERISCOPE__UPDATE_INSTRUCTIONAL_BUTTONS(sPeriscopeData)
	
	_PERISCOPE__GET_ANIM_DATA(sPeriscopeData, PAS_ENTER)
	REQUEST_ANIM_DICT(sPeriscopeData.sAnimData.tlDictionary)
	
	IF IS_BIT_SET(sPeriscopeData.iBS, BS_LOCAL_PLAYER_IS_USING_PERISCOPE)
	OR IS_BIT_SET(sPeriscopeData.iBS, BS_IMMEDIATELY_ENTERED_PERISCOPE)
		_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_ENTER_SCOPE_CAM)
	ELSE
		_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_WAIT_FOR_PLAYER_ENTER)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Called every frame while the player is within range of the periscope.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _STAGE_WAIT_FOR_PLAYER_ENTER(PERISCOPE_DATA &sPeriscopeData)

	// Player is out of range.
	IF NOT _PERISCOPE__IS_PLAYER_WITHIN_RANGE(PLAYER_ID(), sPeriscopeData)
		PRINTLN("[PERISCOPE][_STAGE_WAIT_FOR_PLAYER_ENTER] Player is now out of range: ", GET_PLAYER_NAME(PLAYER_ID()))
		_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_CLEANUP)
		EXIT
	ENDIF
	
	// Display help text for accessing the periscope.
	IF (sPeriscopeData.iContextButtonID = NEW_CONTEXT_INTENTION)
		REGISTER_CONTEXT_INTENTION(sPeriscopeData.iContextButtonID, CP_MEDIUM_PRIORITY, _PERISCOPE__GET_USE_HELP_TEXT_LABEL())
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CONTEXT)
	ENDIF
	
	// Player is entering periscope
	IF HAS_CONTEXT_BUTTON_TRIGGERED(sPeriscopeData.iContextButtonID)
		PRINTLN("[PERISCOPE][_STAGE_WAIT_FOR_PLAYER_ENTER] Player triggered prompt. Entering periscope.")
		_PERISCOPE__SET_BIT(sPeriscopeData, BS_LOCAL_PLAYER_IS_USING_PERISCOPE, TRUE)
		SET_SUBMARINE_USING_PERISCOPE(TRUE)
		_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_START_ANIM_ENTRY)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Called every frame when the player has confirmed they want to enter the scope camera.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _STAGE_START_ANIM_ENTRY(PERISCOPE_DATA &sPeriscopeData)

	BOOL bEnterScopeCam
	INT iLocalSceneID

	IF (sPeriscopeData.iContextButtonID != NEW_CONTEXT_INTENTION)
		RELEASE_CONTEXT_INTENTION(sPeriscopeData.iContextButtonID)
	ENDIF

	_PERISCOPE__SAFE_REMOVE_HELP_TEXT(_PERISCOPE__GET_USE_HELP_TEXT_LABEL())
	
	IF NOT IS_CELLPHONE_DISABLED()
		DISABLE_CELLPHONE(TRUE)
	ENDIF
	
	IF NOT IS_INTERACTION_MENU_DISABLED()
		DISABLE_INTERACTION_MENU()
	ENDIF
	
	// --------------------Start Anim--------------------
	SWITCH sPeriscopeData.eAnimStage
		CASE PAS_DEFAULT
			_PERISCOPE__GET_ANIM_DATA(sPeriscopeData, PAS_ENTER)
			
			REQUEST_ANIM_DICT(sPeriscopeData.sAnimData.tlDictionary)
			
			IF HAS_ANIM_DICT_LOADED(sPeriscopeData.sAnimData.tlDictionary)

				PRINTLN("[PERISCOPE][_STAGE_START_ANIM_ENTRY] Anims loaded!")
				PRINTLN("[PERISCOPE][_STAGE_START_ANIM_ENTRY] sPeriscopeData.sAnimData.tlDictionary: ", Get_String_From_TextLabel(sPeriscopeData.sAnimData.tlDictionary))
				//RESET_NET_TIMER(sPeriscopeData.sTimer)
				_PERISCOPE__SET_ANIM_STAGE(sPeriscopeData, PAS_START_WALKING)
			ENDIF
		BREAK
		CASE PAS_START_WALKING
			VECTOR vPlayerPos, vPlayerRot
			vPlayerPos = GET_ANIM_INITIAL_OFFSET_POSITION(sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPed, sPeriscopeData.sAnimData.vPos, sPeriscopeData.sAnimData.vRot)
			vPlayerRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPed, sPeriscopeData.sAnimData.vPos, sPeriscopeData.sAnimData.vRot)
			
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vPlayerPos, PEDMOVEBLENDRATIO_WALK, DEFAULT, vPlayerRot.z, 0.05)
			
			PRINTLN("[PERISCOPE][_STAGE_START_ANIM_ENTRY] sPeriscopeData.sAnimData.vPos: ", sPeriscopeData.sAnimData.vPos)
			PRINTLN("[PERISCOPE][_STAGE_START_ANIM_ENTRY] sPeriscopeData.sAnimData.vRot: ", sPeriscopeData.sAnimData.vRot)
			PRINTLN("[PERISCOPE][_STAGE_START_ANIM_ENTRY] Walking to coords: ", vPlayerPos)
			
			_PERISCOPE__SET_ANIM_STAGE(sPeriscopeData, PAS_WALKING)
		BREAK
		CASE PAS_WALKING
			IF TAKE_CONTROL_OF_ENTITY(sPeriscopeData.oiPeriscope)
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
				
				sPeriscopeData.sAnimData.iSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(sPeriscopeData.sAnimData.vPos, sPeriscopeData.sAnimData.vRot, DEFAULT, TRUE)
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sPeriscopeData.sAnimData.iSyncSceneID, sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPed, WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(sPeriscopeData.oiPeriscope, sPeriscopeData.sAnimData.iSyncSceneID, sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPeriscope, WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
				
				NETWORK_START_SYNCHRONISED_SCENE(sPeriscopeData.sAnimData.iSyncSceneID)
				
				_PERISCOPE__SET_ANIM_STAGE(sPeriscopeData, PAS_ENTER)
			ENDIF
		BREAK
		CASE PAS_ENTER
			iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sPeriscopeData.sAnimData.iSyncSceneID)
			
			IF TAKE_CONTROL_OF_ENTITY(sPeriscopeData.oiPeriscope)
				IF iLocalSceneID = -1
				OR GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 0.95
					bEnterScopeCam = TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	/*
	// If for whatever reason the player takes too long to enter, exit this state.
	IF (sPeriscopeData.eAnimStage = PAS_WALKING)
	OR (sPeriscopeData.eAnimStage = PAS_ENTER)
		IF NOT HAS_NET_TIMER_STARTED(sPeriscopeData.sTimer)
			START_NET_TIMER(sPeriscopeData.sTimer)
			PRINTLN("[PERISCOPE][_STAGE_START_ANIM_ENTRY] Started walking to periscope, starting timer...")
		ENDIF
		IF HAS_NET_TIMER_STARTED(sPeriscopeData.sTimer)
			IF HAS_NET_TIMER_EXPIRED(sPeriscopeData.sTimer, 10000)
				SCRIPT_ASSERT("[PERISCOPE][_STAGE_START_ANIM_ENTRY] Taking too long to enter, bailing out.")
				PRINTLN("[PERISCOPE][_STAGE_START_ANIM_ENTRY] Taking too long to enter, bailing out.")
				RESET_NET_TIMER(sPeriscopeData.sTimer)
				_PERISCOPE__SET_ANIM_STAGE(sPeriscopeData, PAS_IDLE)
				_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_BAIL_OUT)
			ENDIF
		ENDIF
	ENDIF
	*/
	
	// ----------Enter Scope Cam----------
	IF bEnterScopeCam
		// Fade the screen out.
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			PRINTLN("[PERISCOPE][_STAGE_START_ANIM_ENTRY] Entering scope cam. Fading screen.")
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_IN()
			RESET_NET_TIMER(sPeriscopeData.sTimer)
			_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_ENTER_SCOPE_CAM)
			_PERISCOPE__SET_ANIM_STAGE(sPeriscopeData, PAS_IDLE)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Called every frame when the entry anim is completed.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _STAGE_ENTER_SCOPE_CAM(PERISCOPE_DATA &sPeriscopeData)

	// Local player plays idle anim whilst looking through the scope.
	IF NOT IS_BIT_SET(sPeriscopeData.iBS, BS_IMMEDIATELY_ENTERED_PERISCOPE)
		SWITCH sPeriscopeData.eAnimStage
			CASE PAS_DEFAULT
			CASE PAS_ENTER
			CASE PAS_WALKING
			CASE PAS_START_WALKING
				_PERISCOPE__SET_ANIM_STAGE(sPeriscopeData, PAS_IDLE)
			BREAK
			CASE PAS_IDLE
				
				_PERISCOPE__GET_ANIM_DATA(sPeriscopeData, PAS_IDLE)
				
				IF TAKE_CONTROL_OF_ENTITY(sPeriscopeData.oiPeriscope)
					sPeriscopeData.sAnimData.iSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(sPeriscopeData.sAnimData.vPos, sPeriscopeData.sAnimData.vRot, DEFAULT, DEFAULT, TRUE)
							
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sPeriscopeData.sAnimData.iSyncSceneID, sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPed, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(sPeriscopeData.oiPeriscope, sPeriscopeData.sAnimData.iSyncSceneID, sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPeriscope, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
					
					NETWORK_START_SYNCHRONISED_SCENE(sPeriscopeData.sAnimData.iSyncSceneID)
					_PERISCOPE__SET_ANIM_STAGE(sPeriscopeData, PAS_IDLING)
				ENDIF
				
			BREAK
			CASE PAS_IDLING
				// Anything else while idling?
			BREAK
			
		ENDSWITCH
	ENDIF

	// What we need to enter the scope.
	_PERISCOPE__ENABLE_CAMERA(sPeriscopeData, TRUE)
	_PERISCOPE__DRAW_INIT(sPeriscopeData)
	_PERISCOPE_DISABLE_MOVEMENT_CONTROL_ACTION_THIS_FRAME()
	
	//Update the focus to start streaming in the world at the sub's location
	IF DOES_ENTITY_EXIST(sPeriscopeData.vehSubmarineIAmIn)
	AND IS_ENTITY_ALIVE(sPeriscopeData.vehSubmarineIAmIn)
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		SET_FOCUS_POS_AND_VEL(GET_ENTITY_COORDS(sPeriscopeData.vehSubmarineIAmIn), GET_ENTITY_VELOCITY(sPeriscopeData.vehSubmarineIAmIn))
		PRINTLN("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM] Setting Focus")
	ENDIF
	
	BOOL bEnterScopeCam = TRUE
	
	// Dont enter cam if we're not ready.
	IF NOT _PERISCOPE__IS_CAMERA_ENABLED(sPeriscopeData)
		PRINTLN("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM] Waiting for scope cam to be enabled...")
		bEnterScopeCam = FALSE
	ENDIF
	
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sPeriscopeData.sTextures.Dictionary)
		PRINTLN("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM] Waiting for scope texture dict to load...")
		bEnterScopeCam = FALSE
	ENDIF
	
	IF IS_PLAYER_LEAVING_SUBMARINE_DRIVER_SEAT()
		PRINTLN("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM]player leaving submarine drive seat")
		_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_BAIL_OUT)
		EXIT
	ENDIF
	
	// Debug test
	#IF IS_DEBUG_BUILD
	IF (bDebugTestBailOut)
		bEnterScopeCam = FALSE
	ENDIF
	#ENDIF
	
	// If we're not ready, start a timer so we're not stuck on a black screen.
	IF NOT (bEnterScopeCam)
	AND NOT HAS_NET_TIMER_STARTED(sPeriscopeData.sTimer)
		START_NET_TIMER(sPeriscopeData.sTimer)
		PRINTLN("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM] Scope cam not ready, starting timer...")
	ENDIF
	
	// Throw an assert after 3 seconds.
	IF HAS_NET_TIMER_STARTED(sPeriscopeData.sTimer)
	AND HAS_NET_TIMER_EXPIRED(sPeriscopeData.sTimer, 3000)
	
		SCRIPT_ASSERT("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM] Failed to properly load scope cam. Submit a bug to *Design (Online Technical)*")
		PRINTLN("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM] Failed to properly load scope cam.")
		PRINTLN("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM] Was bDebugTestBailOut TRUE? - ", bDebugTestBailOut)
		PRINTLN("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM] Was the camera enabled? - ", _PERISCOPE__IS_CAMERA_ENABLED(sPeriscopeData))
		PRINTLN("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM] Was the texture dict loaded? - ", HAS_STREAMED_TEXTURE_DICT_LOADED(sPeriscopeData.sTextures.Dictionary))
		
		// Better to use a cam without a texture than not use it at all...
		IF _PERISCOPE__IS_CAMERA_ENABLED(sPeriscopeData)
		AND NOT HAS_STREAMED_TEXTURE_DICT_LOADED(sPeriscopeData.sTextures.Dictionary)
			bEnterScopeCam = TRUE
		ELSE
			_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_BAIL_OUT)
		ENDIF
		
	ENDIF
	
	// Enter cam once we're ready.
	IF (bEnterScopeCam)
	
		IF NOT IS_CELLPHONE_DISABLED()
			DISABLE_CELLPHONE(TRUE)
		ENDIF
		
		IF NOT IS_INTERACTION_MENU_DISABLED()
			DISABLE_INTERACTION_MENU()
		ENDIF
		
		_PERISCOPE__APPLY_SCOPE_TC_MOD()
		
		// Fade the screen in.
		IF NOT IS_SCREEN_FADED_IN()
		AND NOT IS_SCREEN_FADING_IN()
			PRINTLN("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM] Entering scope cam. Fading screen.")
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
		ENDIF
		
		IF (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN())
		AND NOT IS_SCREEN_FADING_OUT()
			
			//Clear all messages as we may want to display our own immediately and don't want others shown
			CLEAR_ALL_HELP_MESSAGES()
			
			_PERISCOPE__START_AUDIO(sPeriscopeData)
			
			IF NOT g_bHasShownPeriscopeHelpTip
				g_bHasShownPeriscopeHelpTip = TRUE //Only show help once per boot
				INT iPeriTipStat = GET_PACKED_STAT_INT(PACKED_MP_INT_KOSATKA_PERISCOPE_TIP)
				IF iPeriTipStat < PERISCOPE_SHOW_TIP_COUNT //Limit total of X times
					SET_PACKED_STAT_INT(PACKED_MP_INT_KOSATKA_PERISCOPE_TIP, iPeriTipStat + 1)
					PRINT_HELP(_PERISCOPE__GET_TIP_HELP_TEXT_LABEL(), DEFAULT_HELP_TEXT_TIME)
				ENDIF
			ENDIF
		
			_PERISCOPE__DRAW(sPeriscopeData)
			
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_FiresDummyRockets, TRUE)
			CLEAR_SUBMARINE_USED_WEAPON_DATA()
			
			_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_UPDATE_SCOPE_CAM)
		ENDIF
		
	ELSE
		PRINTLN("[PERISCOPE][_STAGE_ENTER_SCOPE_CAM] Waiting on scope assets...")
	ENDIF 

ENDPROC

/// PURPOSE:
///    Called every frame while the player is in the periscope camera view.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _STAGE_UPDATE_SCOPE_CAM(PERISCOPE_DATA &sPeriscopeData)

	BOOL bExitScopeCam = FALSE
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_SCREEN_FADING_OUT()
	AND NOT IS_SCREEN_FADED_OUT()
		
		_PERISCOPE__HANDLE_CAMERA_CONTROLS(sPeriscopeData)
		_PERISCOPE__HANDLE_WEAPON_CONTROLS(sPeriscopeData)
		
		_PERISCOPE__DRAW_INSTRUCTIONAL_BUTTONS(sPeriscopeData)
		
		_PERISCOPE__UPDATE_TARGETING(sPeriscopeData)
		
		// Begin exiting the camera if exit prompt is pressed.
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)	
			PRINTLN("[PERISCOPE][_STAGE_UPDATE_SCOPE_CAM] Player pressed input control. Exiting camera.")
			bExitScopeCam = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sPeriscopeData.iBS, BS_KICKED_OUT_OF_PERISCOPE)
		bExitScopeCam = TRUE
	ENDIF
	
	_PERISCOPE__DRAW(sPeriscopeData)
	
	_PERISCOPE__UPDATE_AUDIO(sPeriscopeData)
	_PERISCOPE_DISABLE_MOVEMENT_CONTROL_ACTION_THIS_FRAME()
	
	// Update focus
	IF DOES_ENTITY_EXIST(sPeriscopeData.vehSubmarineIAmIn)
	AND IS_ENTITY_ALIVE(sPeriscopeData.vehSubmarineIAmIn)
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		SET_FOCUS_POS_AND_VEL(GET_ENTITY_COORDS(sPeriscopeData.vehSubmarineIAmIn), GET_ENTITY_VELOCITY(sPeriscopeData.vehSubmarineIAmIn))
		PRINTLN("[PERISCOPE][_STAGE_UPDATE_SCOPE_CAM] Setting Focus")
	ENDIF
	
	IF (bExitScopeCam)
		// Fade the screen out.
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
		AND NOT IS_SCREEN_FADING_IN()
			PRINTLN("[PERISCOPE][_STAGE_UPDATE_SCOPE_CAM] Exiting scope cam. Fading screen.")
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
		ENDIF
	ENDIF
	
	// Proceed once the screen is faded.
	IF IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_IN()
		_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_EXIT_SCOPE_CAM)
	ENDIF

ENDPROC

/// PURPOSE:
///    Called every frame when the player wants to exit the periscope camera.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _STAGE_EXIT_SCOPE_CAM(PERISCOPE_DATA &sPeriscopeData)

	_PERISCOPE__ENABLE_CAMERA(sPeriscopeData, FALSE)
	_PERISCOPE_DISABLE_MOVEMENT_CONTROL_ACTION_THIS_FRAME()
	
	IF NOT _PERISCOPE__IS_CAMERA_ENABLED(sPeriscopeData)
	AND NOT DOES_CAM_EXIST(sPeriscopeData.sCamData.cameraID)
	
		IF IS_CELLPHONE_DISABLED()
			DISABLE_CELLPHONE(FALSE)
		ENDIF
		
		IF IS_INTERACTION_MENU_DISABLED()
			ENABLE_INTERACTION_MENU()
		ENDIF
		
		ENABLE_ALL_MP_HUD()
		_PERISCOPE__CLEAR_SCOPE_TC_MOD()
		_PERISCOPE__STOP_AUDIO(sPeriscopeData)
		_PERISCOPE__RESET_CAM_ROT_SPEED(sPeriscopeData)
	
		// Fade the screen in.
		IF NOT IS_SCREEN_FADED_IN()
		AND NOT IS_SCREEN_FADING_IN()
			PRINTLN("[PERISCOPE][_STAGE_EXIT_SCOPE_CAM] Exiting scope cam. Fading screen.")
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
		ENDIF
		
		IF (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN())
		AND NOT IS_SCREEN_FADING_OUT()
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
			
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CLEAR_FOCUS()
				PRINTLN("[PERISCOPE][_STAGE_UPDATE_SCOPE_CAM] Clearing Focus")
			ENDIF
			
			IF IS_BIT_SET(sPeriscopeData.iBS, BS_IMMEDIATELY_ENTERED_PERISCOPE)		
				PRINTLN("[PERISCOPE][_STAGE_EXIT_SCOPE_CAM] Skipping anim.")
				
				_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_CLEANUP)
			ELSE
				_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_START_ANIM_EXIT)
				_PERISCOPE__SET_ANIM_STAGE(sPeriscopeData, PAS_EXIT)
			ENDIF
				
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///     Called every frame when the player is playing the exit animation.
/// PARAMS:
///    sPeriscopeData - Periscope.
PROC _STAGE_START_ANIM_EXIT(PERISCOPE_DATA &sPeriscopeData)

	BOOL bStoppedUsingPeriscope
	
	// Play exit anim
	// Local player plays idle anim whilst looking through the scope.
	SWITCH sPeriscopeData.eAnimStage
		CASE PAS_ENTER
		CASE PAS_WALKING
		CASE PAS_START_WALKING
		CASE PAS_IDLE
		CASE PAS_IDLING
			_PERISCOPE__SET_ANIM_STAGE(sPeriscopeData, PAS_EXIT)
		BREAK
		CASE PAS_EXIT
			_PERISCOPE__GET_ANIM_DATA(sPeriscopeData, PAS_EXIT)
			IF TAKE_CONTROL_OF_ENTITY(sPeriscopeData.oiPeriscope)
				sPeriscopeData.sAnimData.iSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(sPeriscopeData.sAnimData.vPos, sPeriscopeData.sAnimData.vRot)
						
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sPeriscopeData.sAnimData.iSyncSceneID, sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPed, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(sPeriscopeData.oiPeriscope, sPeriscopeData.sAnimData.iSyncSceneID, sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPeriscope, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
				
				NETWORK_START_SYNCHRONISED_SCENE(sPeriscopeData.sAnimData.iSyncSceneID)
				
				RESET_NET_TIMER(sPeriscopeData.sTimer)
				_PERISCOPE__SET_ANIM_STAGE(sPeriscopeData, PAS_EXITING)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	// Proceed once the anim is done.
	IF sPeriscopeData.eAnimStage = PAS_EXITING
		INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sPeriscopeData.sAnimData.iSyncSceneID)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 0.97
				NETWORK_STOP_SYNCHRONISED_SCENE(sPeriscopeData.sAnimData.iSyncSceneID)
				bStoppedUsingPeriscope = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	// If for whatever reason the player takes too long to enter, exit this state.
	IF (sPeriscopeData.eAnimStage = PAS_EXITING)
		IF NOT HAS_NET_TIMER_STARTED(sPeriscopeData.sTimer)
			START_NET_TIMER(sPeriscopeData.sTimer)
			PRINTLN("[PERISCOPE][_STAGE_START_ANIM_EXIT] Started exiting periscope, starting timer...")
		ENDIF
		IF HAS_NET_TIMER_STARTED(sPeriscopeData.sTimer)
			IF HAS_NET_TIMER_EXPIRED(sPeriscopeData.sTimer, 8000)
				PRINTLN("[PERISCOPE][_STAGE_START_ANIM_EXIT] Taking too long to exit, stopping scene.")
				INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sPeriscopeData.sAnimData.iSyncSceneID)
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
					NETWORK_STOP_SYNCHRONISED_SCENE(sPeriscopeData.sAnimData.iSyncSceneID)
				ENDIF
				bStoppedUsingPeriscope = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bStoppedUsingPeriscope
		_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_CLEANUP)
	ENDIF
	
ENDPROC

// ***************************************************************************************
//
// 					PUBLIC API
//
// ***************************************************************************************

/// PURPOSE:
///    Register submarine helm player broadcast data
/// PARAMS:
///    ref_local - submarine helm player broadcast data struct
PROC PERISCOPE_REGISTER_PLAYER_BROADCAST_DATA(PERISCOPE_DATA &sPeriscopeData)
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(sPeriscopeData.playerBD, SIZE_OF(sPeriscopeData.playerBD))
	PRINTLN("[PERISCOPE][PERISCOPE_REGISTER_PLAYER_BROADCAST_DATA] - Registered broadcast data")
ENDPROC

/// PURPOSE:
///    Register submarine helm server broadcast data
/// PARAMS:
///    ref_local - submarine helm server broadcast data struct
PROC PERISCOPE_REGISTER_SERVER_BROADCAST_DATA(PERISCOPE_DATA &sPeriscopeData)
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(sPeriscopeData.serverBD, SIZE_OF(sPeriscopeData.serverBD))
	PRINTLN("[PERISCOPE][PERISCOPE_REGISTER_SERVER_BROADCAST_DATA] - Registered broadcast data")
ENDPROC

/// PURPOSE:
///    Sometime the submarine isn't initiailised at the start, so update it here.
PROC UPDATE_PERISCOPE_SUBMARINE_VEHICLE_INDEX(PERISCOPE_DATA &sPeriscopeData, VEHICLE_INDEX vehSubmarine)
	IF IS_ENTITY_ALIVE(vehSubmarine)
	AND NOT DOES_ENTITY_EXIST(sPeriscopeData.vehSubmarineIAmIn)
		PRINTLN("[PERISCOPE][UPDATE_PERISCOPE_SUBMARINE_VEHICLE_INDEX] - Assigned Submarine vehicle index.")
		sPeriscopeData.vehSubmarineIAmIn = vehSubmarine
	ENDIF
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_USING_PERISCOPE(PERISCOPE_DATA &sPeriscopeData)
	RETURN IS_BIT_SET(sPeriscopeData.iBS, BS_LOCAL_PLAYER_IS_USING_PERISCOPE)
ENDFUNC

FUNC BOOL IS_REMOTE_PLAYER_USING_PERISCOPE(PERISCOPE_DATA &sPeriscopeData)
	RETURN IS_BIT_SET(sPeriscopeData.iBS, BS_REMOTE_PLAYER_IS_USING_PERISCOPE)
ENDFUNC

PROC SET_REMOTE_PLAYER_IS_USING_PERISCOPE(PERISCOPE_DATA &sPeriscopeData, BOOL bIsUsing)
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_REMOTE_PLAYER_IS_USING_PERISCOPE, bIsUsing)
ENDPROC

PROC DISABLE_PERISCOPE(PERISCOPE_DATA &sPeriscopeData, BOOL bDisable)
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_SET_PERISCOPE_UNAVAILABLE, bDisable)
ENDPROC

FUNC BOOL IS_PERISCOPE_DISABLED(PERISCOPE_DATA &sPeriscopeData)
	RETURN IS_BIT_SET(sPeriscopeData.iBS, BS_SET_PERISCOPE_UNAVAILABLE)
ENDFUNC

// *******************************************
// 					CAMERA
// *******************************************

PROC SET_PERISCOPE_CAM_MIN_PITCH(PERISCOPE_DATA &sPeriscopeData, FLOAT fMinPitch)
	PRINTLN("[PERISCOPE][SET_PERISCOPE_CAM_MIN_PITCH] Setting new cam min pitch: ", GET_STRING_FROM_FLOAT(fMinPitch))
	sPeriscopeData.sCamData.fMinPitch = fMinPitch
ENDPROC

PROC SET_PERISCOPE_CAM_MAX_PITCH(PERISCOPE_DATA &sPeriscopeData, FLOAT fMaxPitch)
	PRINTLN("[PERISCOPE][SET_PERISCOPE_CAM_MAX_PITCH] Setting new cam max pitch: ", GET_STRING_FROM_FLOAT(fMaxPitch))
	sPeriscopeData.sCamData.fMaxPitch = fMaxPitch
ENDPROC

PROC SET_PERISCOPE_CAM_ROT(PERISCOPE_DATA &sPeriscopeData, VECTOR vNewRot)
	sPeriscopeData.sCamData.vRot = vNewRot
ENDPROC

PROC SET_PERISCOPE_CAM_FOV(PERISCOPE_DATA &sPeriscopeData, FLOAT fNewFOV)
	sPeriscopeData.sCamData.fFOV = fNewFOV
ENDPROC

/// PARAMS:
///    sPeriscopeData - Periscope
///    fNewSpeed - Value must be between 0 and 0.5.
PROC SET_PERISCOPE_CAM_CONTROLLER_SPEED(PERISCOPE_DATA &sPeriscopeData, FLOAT fNewSpeed)

	IF (fNewSpeed < 0)
	OR (fNewSpeed > 0.5)
		SCRIPT_ASSERT("[PERISCOPE][SET_PERISCOPE_CAM_CONTROLLER_SPEED] Value must be between 0 and 0.5. Your value: ", GET_STRING_FROM_FLOAT(fNewSpeed))
		EXIT
	ENDIF

	sPeriscopeData.sCamData.fStickSpeed = fNewSpeed
ENDPROC

PROC SET_PERISCOPE_CAM_CURSOR_SPEED(PERISCOPE_DATA &sPeriscopeData, FLOAT fNewSpeed)
	sPeriscopeData.sCamData.fCursorSpeed = fNewSpeed
ENDPROC

/// PURPOSE:
///    Used by external scripts to indicate that the periscope has been entered via something else.
///    This will skip all anims and immediately enter the periscope.
///    CALL ONE FRAME ONLY
/// PARAMS:
///    sPeriscopeData - 
PROC IMMEDIATELY_ENTER_PERISCOPE(PERISCOPE_DATA &sPeriscopeData, BOOL bAllowPlayerToBeKickedOut = FALSE)
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[PERISCOPE][IMMEDIATELY_ENTER_PERISCOPE] Trying to enter periscope immediately...")
	
	IF IS_BIT_SET(sPeriscopeData.iBS, BS_IMMEDIATELY_ENTERED_PERISCOPE)
	OR IS_BIT_SET(sPeriscopeData.iBS, BS_LOCAL_PLAYER_IS_USING_PERISCOPE)
		PRINTLN("[PERISCOPE][IMMEDIATELY_ENTER_PERISCOPE] Already in use by local player")
		EXIT
	ENDIF

	// Somebody else is using it.
	IF IS_BIT_SET(sPeriscopeData.iBS, BS_REMOTE_PLAYER_IS_USING_PERISCOPE)
	AND NOT IS_BIT_SET(sPeriscopeData.iBS, BS_LOCAL_PLAYER_IS_USING_PERISCOPE)
		PRINTLN("[PERISCOPE][IMMEDIATELY_ENTER_PERISCOPE] Someone else is using it.")
		
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(_PERISCOPE__GET_IN_USE_HELP_TEXT_LABEL())
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_ON_SCREEN()
				PRINT_HELP(_PERISCOPE__GET_IN_USE_HELP_TEXT_LABEL())
			ENDIF
		ENDIF
		EXIT
	ENDIF
	
	// Periscope cam is under water
	IF NOT _PERISCOPE__IS_CAM_ABOVE_WATER_HEIGHT(sPeriscopeData, TRUE)
		PRINTLN("[PERISCOPE][IMMEDIATELY_ENTER_PERISCOPE] Cam is underwater.")
		
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SUB_PERI_DEPTH")
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_ON_SCREEN()
				PRINT_HELP("SUB_PERI_DEPTH")
			ENDIF	
		ENDIF
		EXIT
	ENDIF
	
	// Dont set as using periscope, so other players who actually use the periscope prop have prority
	//_PERISCOPE__SET_BIT(sPeriscopeData, BS_LOCAL_PLAYER_IS_USING_PERISCOPE, TRUE)
	
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_IMMEDIATELY_ENTERED_PERISCOPE, TRUE)
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_IMMEDIATELY_ENTERING_PERISCOPE, TRUE)
	
	IF (bAllowPlayerToBeKickedOut)
		_PERISCOPE__SET_BIT(sPeriscopeData, BS_PLAYER_CAN_BE_KICKED_OUT, TRUE)
	ENDIF
	
ENDPROC

PROC SET_PERISCOPE_MAX_DEPTH(PERISCOPE_DATA &sPeriscopeData, FLOAT fMaxDepth)
	sPeriscopeData.sCamData.fMaxDepth = fMaxDepth
ENDPROC

// *******************************************
// 					ANIMATION
// *******************************************

PROC SET_PERISCOPE_ANIM_COORDS(PERISCOPE_DATA &sPeriscopeData, VECTOR vNewCoords)
	sPeriscopeData.sAnimData.vPos = vNewCoords
ENDPROC

PROC SET_PERISCOPE_ANIM_ROT(PERISCOPE_DATA &sPeriscopeData, VECTOR vNewRot)
	sPeriscopeData.sAnimData.vRot = vNewRot
ENDPROC

// *******************************************
// 					MAIN
// *******************************************

/// PURPOSE:
///    Used by other scipts to cleanup the periscope they have implemented.
/// PARAMS:
///    sPeriscopeData - Periscope instance.
PROC PERISCOPE_CLEANUP(PERISCOPE_DATA &sPeriscopeData)
	
	PRINTLN("[PERISCOPE][PERISCOPE_CLEANUP] Cleaning up!")
	
	// ----------Ensure the periscope prop doesn't move if the player using it leaves----------
	IF sPeriscopeData.eStage > PTS_ENTER_SCOPE_CAM
	AND NOT IS_BIT_SET(sPeriscopeData.iBS, BS_IMMEDIATELY_ENTERED_PERISCOPE)
	AND NOT (sPeriscopeData.sAnimData.iPropSceneID > -1)
		NETWORK_STOP_SYNCHRONISED_SCENE(sPeriscopeData.sAnimData.iSyncSceneID)
		
		IF TAKE_CONTROL_OF_ENTITY(sPeriscopeData.oiPeriscope)
		
			VECTOR vPeriscopePos, vPeriscopeRot
			vPeriscopePos = GET_ANIM_INITIAL_OFFSET_POSITION(sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPeriscope, sPeriscopeData.sAnimData.vPos, sPeriscopeData.sAnimData.vRot)
			vPeriscopeRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sPeriscopeData.sAnimData.tlDictionary, sPeriscopeData.sAnimData.tlClipPeriscope, sPeriscopeData.sAnimData.vPos, sPeriscopeData.sAnimData.vRot)
			
			SET_ENTITY_COORDS_NO_OFFSET(sPeriscopeData.oiPeriscope, vPeriscopePos)
			SET_ENTITY_ROTATION(sPeriscopeData.oiPeriscope, vPeriscopeRot)
			FREEZE_ENTITY_POSITION(sPeriscopeData.oiPeriscope, TRUE)
			
		ENDIF
	ENDIF
	
	ENABLE_ALL_MP_HUD()
	
	IF IS_BIT_SET(sPeriscopeData.iBS, BS_KICKED_OUT_OF_PERISCOPE)
		_PERISCOPE__PRINT_KICKED_OUT_HELP_TEXT(sPeriscopeData)
	ENDIF
	
	IF (sPeriscopeData.iContextButtonID != NEW_CONTEXT_INTENTION)
		RELEASE_CONTEXT_INTENTION(sPeriscopeData.iContextButtonID)
	ENDIF
	
	_PERISCOPE__CLEANUP_CAMERA(sPeriscopeData)
	_PERISCOPE__TARGETING_CLEANUP(sPeriscopeData)
	_PERISCOPE__CLEANUP_INSTRUCTIONAL_BUTTONS(sPeriscopeData)
	
	IF IS_CELLPHONE_DISABLED()
		DISABLE_CELLPHONE(FALSE)
	ENDIF
	
	IF IS_INTERACTION_MENU_DISABLED()
		ENABLE_INTERACTION_MENU()
	ENDIF
	
	RESET_NET_TIMER(sPeriscopeData.sTimer)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sPeriscopeData.sTextures.Dictionary)
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sPeriscopeData.sTextures.Dictionary)
	ENDIF
	
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_LOCAL_PLAYER_IS_USING_PERISCOPE, FALSE)
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_IMMEDIATELY_ENTERED_PERISCOPE, FALSE)
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_KICKED_OUT_OF_PERISCOPE, FALSE)
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_IMMEDIATELY_ENTERING_PERISCOPE, FALSE)
	_PERISCOPE__SET_BIT(sPeriscopeData, BS_PLAYER_CAN_BE_KICKED_OUT, FALSE)
	SET_SUBMARINE_USING_PERISCOPE(FALSE)
	DISABLE_PERISCOPE(sPeriscopeData, FALSE)
	
	IF (sPeriscopeData.eStage != PTS_PLAYER_OUT_OF_RANGE)
		_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_PLAYER_OUT_OF_RANGE)
	ENDIF
	
	IF (sPeriscopeData.eAnimStage != PAS_DEFAULT)
		_PERISCOPE__SET_ANIM_STAGE(sPeriscopeData, PAS_DEFAULT)
	ENDIF
	
	IF (sPeriscopeData.eKickOutReason != KICK_DEFAULT)
		_PERISCOPE__SET_KICK_OUT_REASON(sPeriscopeData, KICK_DEFAULT)
	ENDIF
	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_FiresDummyRockets, FALSE)
	SEND_SUBMARINE_WEAPONS_TELEMETRICS(SWT_HOMING_MISSILE, IS_OWNER_OF_THIS_SUB())
ENDPROC

PROC REMOVE_PERISCOPE_ANIM_DICT(PERISCOPE_DATA &sPeriscopeData)
	IF NOT IS_STRING_NULL_OR_EMPTY(sPeriscopeData.sAnimData.tlDictionary)
		IF DOES_ANIM_DICT_EXIST(sPeriscopeData.sAnimData.tlDictionary)
			REMOVE_ANIM_DICT(sPeriscopeData.sAnimData.tlDictionary)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Used by other script to initialise the persicope.
/// PARAMS:
///    sPeriscopeData - Periscope instance.
PROC PERISCOPE_INITIALISE(PERISCOPE_DATA &sPeriscopeData)

	PRINTLN("[PERISCOPE][PERISCOPE_INITIALISE] Initialising!")
	
	_PERISCOPE__GET_ANIM_DATA(sPeriscopeData, PAS_ENTER)
	IF DOES_ANIM_DICT_EXIST(sPeriscopeData.sAnimData.tlDictionary)
		REQUEST_ANIM_DICT(sPeriscopeData.sAnimData.tlDictionary)
	ENDIF

	_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_PLAYER_OUT_OF_RANGE)
	
ENDPROC

/// PURPOSE:
///    Used by other scipts to update the periscope they have implemented.
/// PARAMS:
///    sPeriscopeData - Periscope instance.
PROC PERISCOPE_UPDATE(PERISCOPE_DATA &sPeriscopeData)

	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
		_PERISCOPE__CREATE_PERISCOPE_PROP(sPeriscopeData)
	ENDIF
	
	_PERISCOPE__MAINTAIN_LOCAL_OBJECTS(sPeriscopeData)
	
	// ---------- Kick Player out of periscope ----------
	IF _PERISCOPE__SHOULD_PLAYER_BE_KICKED_OUT(sPeriscopeData)
		_PERISCOPE__KICK_PLAYER_OUT(sPeriscopeData)
	ENDIF

	// ---------- Block periscope access is unavailable ----------
	IF _PERISCOPE__IS_PLAYER_WITHIN_RANGE(PLAYER_ID(), sPeriscopeData)
	AND NOT IS_BIT_SET(sPeriscopeData.iBS, BS_KICKED_OUT_OF_PERISCOPE)
	AND (sPeriscopeData.eStage <= PTS_WAIT_FOR_PLAYER_ENTER)
		IF _PERISCOPE__SHOULD_PERISCOPE_ACCESS_BE_BLOCKED(sPeriscopeData)
			EXIT
		ENDIF
	ENDIF
	
	// ---------- Immediately enter if set. ----------
	IF IS_BIT_SET(sPeriscopeData.iBS, BS_IMMEDIATELY_ENTERING_PERISCOPE)
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			PRINTLN("[PERISCOPE][IMMEDIATELY_ENTER_PERISCOPE] Entering scope cam. Fading screen.")
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_IN()
			RESET_NET_TIMER(sPeriscopeData.sTimer)
			_PERISCOPE__SET_BIT(sPeriscopeData, BS_IMMEDIATELY_ENTERED_PERISCOPE, TRUE)
			_PERISCOPE__SET_BIT(sPeriscopeData, BS_IMMEDIATELY_ENTERING_PERISCOPE, FALSE)
			_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_INIT_FOR_USE)
		ENDIF
	ENDIF

	// ---------- Main State Machine ----------
	SWITCH sPeriscopeData.eStage
		CASE PTS_PLAYER_OUT_OF_RANGE
			_STAGE_PLAYER_OUT_OF_RANGE(sPeriscopeData)
		BREAK
		CASE PTS_INIT_FOR_USE
			_STATE_INIT_FOR_USE(sPeriscopeData)
		BREAK
		CASE PTS_WAIT_FOR_PLAYER_ENTER
			_STAGE_WAIT_FOR_PLAYER_ENTER(sPeriscopeData)
		BREAK
		CASE PTS_START_ANIM_ENTRY
			_STAGE_START_ANIM_ENTRY(sPeriscopeData)
		BREAK
		CASE PTS_ENTER_SCOPE_CAM
			_STAGE_ENTER_SCOPE_CAM(sPeriscopeData)
		BREAK
		CASE PTS_UPDATE_SCOPE_CAM
			_STAGE_UPDATE_SCOPE_CAM(sPeriscopeData)
		BREAK
		CASE PTS_EXIT_SCOPE_CAM
			_STAGE_EXIT_SCOPE_CAM(sPeriscopeData)
		BREAK
		CASE PTS_START_ANIM_EXIT
			_STAGE_START_ANIM_EXIT(sPeriscopeData)
		BREAK
		CASE PTS_BAIL_OUT
			_PERISCOPE__BAIL_OUT(sPeriscopeData)
		BREAK
		CASE PTS_CLEANUP
			PERISCOPE_CLEANUP(sPeriscopeData)
		BREAK
	ENDSWITCH
	
	_PERISCOPE__UPDATE_CAMERA(sPeriscopeData)
	
	_PERISCOPE__UPDATE_INSTRUCTIONAL_BUTTONS(sPeriscopeData)
	
	#IF IS_DEBUG_BUILD
	_DEBUG_PERISCOPE__UPDATE(sPeriscopeData)
	IF bDebugEnterScope
		PRINTLN("[PERISCOPE][PERISCOPE_UPDATE] Debug. Entering periscope.")
		_PERISCOPE__SET_BIT(sPeriscopeData, BS_LOCAL_PLAYER_IS_USING_PERISCOPE, TRUE)
		SET_SUBMARINE_USING_PERISCOPE(TRUE)
		_PERISCOPE__SET_TRIGGER_STAGE(sPeriscopeData, PTS_ENTER_SCOPE_CAM)
		bDebugEnterScope = FALSE
	ENDIF
	
	IF (bDebugImmediatelyEnterPeriscope)
		IMMEDIATELY_ENTER_PERISCOPE(sPeriscopeData)
		bDebugImmediatelyEnterPeriscope = FALSE
	ENDIF
	#ENDIF
	
ENDPROC





