//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        heist_island_travel.sch																				
/// Description: Header for maintaining all island warp trigger locations and maintaining a background
///    			 island request service for quickly transitioning to and from the island with a single function
///    			 call
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		16/07/2020																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "warp_transition_trigger_location.sch"
USING "heist_island_warp_data.sch"
USING "net_mission_joblist.sch"
USING "script_maths.sch"
USING "net_ambience.sch"

CONST_INT ciBEACH_PARTY_MAX_RESTRICTED_AREA_KICKS_BEFORE_REMOVAL 8
CONST_FLOAT ciBEACH_PARTY_RESTRICTION_RADIUS 120.0
CONST_FLOAT cfBEACH_PARTY_RESTRICTION_RETURN_DISTANCE 5.0

STRUCT HEIST_ISLAND_TRAVEL_BEACH_PARTY
	BOOL bPlayerGettingKickedFromArea
	INT iPlayerKickCount
	SCRIPT_TIMER sKickBailTimer
ENDSTRUCT

ENUM BEACH_PARTY_UNLOCK_STATE
	BPUS__WAIT_FOR_UNLOCK
	,BPUS__WAIT_FOR_COUNTDOWN
	,BPUS__UPDATE_TEXT_MESSAGE
	,BPUS__WAIT_FOR_BLIP_FLASH
ENDENUM

STRUCT HEIST_ISLAND_TRAVEL_STRUCT
	SCRIPT_TIMER sLSIAUnlockCountdown
	SCRIPT_TIMER sNoMissionBailTimer
	BEACH_PARTY_UNLOCK_STATE eLSIAUnlockState
	WARP_TRANSITION_TRIGGER_LOCATION_STRUCT sLSIAToBeachPartyTrigger
	WARP_TRANSITION_TRIGGER_LOCATION_STRUCT sBeachPartyToLSIATrigger
	VEHICLE_INDEX viLeaveBeachVehicleProp
	
	// Used for background warp system
	WARP_TRANSITION_STRUCT sQuickIslandWarp
	INT iStaggeredPlayerCheck
	
	BOOL bLaunchBackupHeli
	MP_MISSION_DATA sHeliLaunch	
	
	#IF IS_DEBUG_BUILD
		BOOL bTriggerLSIAUnlock
		BOOL bSkipCountdown
		BOOL bResetLSIAUnlock
		BOOL bShowGlobals
		BOOL bResetVibesAward
	#ENDIF
ENDSTRUCT

/// PURPOSE:
///    Gets the vehicle setup for the vehicle that appears at los santos international airport
/// PARAMS:
///    iVehicleSetup - the index of the vehicle setup 0 - 9
///    sData - the returned vehicle data 
PROC HEIST_ISLAND_DATA__GET_LSIA_ARRIVAL_VEHICLE_SETUP_DATA(INT iVehicleSetup, VEHICLE_SETUP_STRUCT_MP &sData)
	SWITCH iVehicleSetup
		// Vehicle 0
		CASE 0
			sData.VehicleSetup.eModel = COGNOSCENTI
			sData.VehicleSetup.tlPlateText = "46JJP410"
			sData.VehicleSetup.iColourExtra1 = 156
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK

		// Vehicle 1
		CASE 1
			sData.VehicleSetup.eModel = COGNOSCENTI
			sData.VehicleSetup.tlPlateText = "21OUZ426"
			sData.VehicleSetup.iColourExtra1 = 156
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK

		// Vehicle 2
		CASE 2
			sData.VehicleSetup.eModel = COGNOSCENTI
			sData.VehicleSetup.tlPlateText = "20CFH317"
			sData.VehicleSetup.iColourExtra1 = 156
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK

		// Vehicle 3
		CASE 3
			sData.VehicleSetup.eModel = COGNOSCENTI
			sData.VehicleSetup.tlPlateText = "29WZZ231"
			sData.VehicleSetup.iColourExtra1 = 156
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK

		// Vehicle 4
		CASE 4
			sData.VehicleSetup.eModel = COGNOSCENTI
			sData.VehicleSetup.tlPlateText = "47QLW646"
			sData.VehicleSetup.iColourExtra1 = 156
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK

		// Vehicle 5
		CASE 5
			sData.VehicleSetup.eModel = COGNOSCENTI
			sData.VehicleSetup.tlPlateText = "29EXA220"
			sData.VehicleSetup.iColourExtra1 = 156
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK

		// Vehicle 6
		CASE 6
			sData.VehicleSetup.eModel = COGNOSCENTI
			sData.VehicleSetup.tlPlateText = "23KBM033"
			sData.VehicleSetup.iColourExtra1 = 156
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK

		// Vehicle 7
		CASE 7
			sData.VehicleSetup.eModel = COGNOSCENTI
			sData.VehicleSetup.tlPlateText = "86YQB679"
			sData.VehicleSetup.iColourExtra1 = 156
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK

		// Vehicle 8
		CASE 8
			sData.VehicleSetup.eModel = COGNOSCENTI
			sData.VehicleSetup.tlPlateText = "00OSA318"
			sData.VehicleSetup.iColourExtra1 = 156
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK

		// Vehicle 9
		CASE 9
			sData.VehicleSetup.eModel = COGNOSCENTI
			sData.VehicleSetup.tlPlateText = "89RGN556"
			sData.VehicleSetup.iColourExtra1 = 156
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Gets the los santos international airport arrive vehicles locattion and heading for the given spawn slot
/// PARAMS:
///    iSpawnSlot - the spawn index used 0 - 10
///    sTransform - the returned transform of the vehicle
PROC HEIST_ISLAND_DATA__GET_LSIA_ARRIVAL_VEHICLE_LOCATION(INT iSpawnSlot, TRANSFORM_STRUCT &sTransform)
	SWITCH iSpawnSlot
		CASE 0
	        sTransform.Position =       <<-1145.0330, -2739.3459, 12.9530>>
	        sTransform.Rotation =      <<0.0, 0.0, 330.0000>>
	    BREAK
	    CASE 1
	        sTransform.Position =       <<-1150.1281, -2748.1179, 12.9510>>
	        sTransform.Rotation =      <<0.0, 0.0, 330.0000>>
	    BREAK
	    CASE 2
	        sTransform.Position =       <<-1153.9280, -2754.9241, 12.9490>>
	        sTransform.Rotation =      <<0.0, 0.0, 330.0000>>
	    BREAK
	    CASE 3
	        sTransform.Position =       <<-1157.8190, -2762.0630, 12.9490>>
	        sTransform.Rotation =      <<0.0, 0.0, 330.8000>>
	    BREAK
	    CASE 4
	        sTransform.Position =       <<-1163.0699, -2772.0100, 12.9480>>
	        sTransform.Rotation =      <<0.0, 0.0, 332.8000>>
	    BREAK
	    CASE 5
	        sTransform.Position =       <<-1156.7310, -2730.2471, 12.9530>>
	        sTransform.Rotation =      <<0.0, 0.0, 315.6000>>
	    BREAK
	    CASE 6
	        sTransform.Position =       <<-1160.3770, -2736.9490, 12.9530>>
	        sTransform.Rotation =      <<0.0, 0.0, 313.6000>>
	    BREAK
	    CASE 7
	        sTransform.Position =       <<-1163.9600, -2743.3831, 12.9520>>
	        sTransform.Rotation =      <<0.0, 0.0, 312.4000>>
	    BREAK
	    CASE 8
	        sTransform.Position =       <<-1176.0270, -2771.1689, 12.9440>>
	        sTransform.Rotation =      <<0.0, 0.0, 333.4000>>
	    BREAK
	    CASE 9
	        sTransform.Position =       <<-1166.8979, -2780.3230, 12.9470>>
	        sTransform.Rotation =      <<0.0, 0.0, 334.2000>>
	    BREAK
	    CASE 10
	        sTransform.Position =       <<-1152.9180, -2744.2981, 12.9520>>
	        sTransform.Rotation =      <<0.0, 0.0, 330.0000>>
	    BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Spawns an arrival vehicle at los santos if g_bSpawnLSIAArrivalGangVehicle is true
///    TODO: This was added last minute for a review play, it would be better to use an event instead of a global
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_SPAWN_LSIA_SCOPING_MISSION_ARRIVAL_VEHICLE()
	IF NOT g_bSpawnLSIAArrivalGangVehicle
		EXIT
	ENDIF
	
	IF HEIST_ISLAND_LOADING__ARE_IPLS_ACTIVE()
		EXIT
	ENDIF
	
	// Only boss should spawn
	IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		g_bSpawnLSIAArrivalGangVehicle = FALSE
		EXIT
	ENDIF
	
	INT iSlot = GB_GET_GANG_ID_FROM_BOSS(PLAYER_ID())
	
	VEHICLE_SETUP_STRUCT_MP sVehData
	HEIST_ISLAND_DATA__GET_LSIA_ARRIVAL_VEHICLE_SETUP_DATA(iSlot, sVehData)
	
	REQUEST_MODEL(sVehData.VehicleSetup.eModel)
	
	IF NOT HAS_MODEL_LOADED(sVehData.VehicleSetup.eModel)
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_SPAWN_LSIA_SCOPING_MISSION_ARRIVAL_VEHICLE - Loading model...")
		EXIT
	ENDIF
	
	IF NOT CAN_REGISTER_MISSION_VEHICLES(GET_NUM_CREATED_MISSION_VEHICLES() + 1)
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_SPAWN_LSIA_SCOPING_MISSION_ARRIVAL_VEHICLE - Waiting for vehicle creation to be allowed...")	
		EXIT
	ENDIF

	TRANSFORM_STRUCT sTransform
	HEIST_ISLAND_DATA__GET_LSIA_ARRIVAL_VEHICLE_LOCATION(iSlot, sTransform)
	REQUEST_COLLISION_AT_COORD(sTransform.Position)
	VEHICLE_INDEX Veh = CREATE_VEHICLE(sVehData.VehicleSetup.eModel, sTransform.Position, sTransform.Rotation.z, TRUE, FALSE)
	SET_VEHICLE_SETUP_MP(Veh, sVehData)
	SET_MODEL_AS_NO_LONGER_NEEDED(sVehData.VehicleSetup.eModel)
	PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_SPAWN_LSIA_SCOPING_MISSION_ARRIVAL_VEHICLE - Created vehicle at (", iSlot ,") ", sTransform.Position)
	g_bSpawnLSIAArrivalGangVehicle = FALSE
ENDPROC

/// PURPOSE:
///    Maintains the flow for unlocking the beach party. It will wait for the heist to be completed as a leader, wait for g_sMPTunables.iHEIST_ISLAND_BEACH_PARTY_UNLOCK_COUNTDOWN_MS,
///    send a text message, and then set the party as unlocked
/// PARAMS:
///    sInst - the heist island travel instance
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_BEACH_PARTY_TRIGGER_UNLOCK(HEIST_ISLAND_TRAVEL_STRUCT &sInst)

	// Use a staggered player index to perform these checks
	IF (PLAYER_ID() != INT_TO_NATIVE(PLAYER_INDEX, sINST.iStaggeredPlayerCheck))	
		EXIT
	ENDIF

	// TODO: Refactor this
	SWITCH sInst.eLSIAUnlockState
		CASE BPUS__WAIT_FOR_UNLOCK
			// Already unlocked
			IF GET_PACKED_STAT_BOOL(PACKED_MP_STAT_SHOWN_BEACH_PARTY_UNLOCK_HELP)
				EXIT
			ENDIF
			
			// Should unlock on heist completion
			IF NOT HAS_PLAYER_COMPLETED_ISLAND_HEIST_AS_LEADER(PLAYER_ID())
				EXIT
			ENDIF
			
			IF IS_LOCAL_PLAYER_ON_ANY_FM_MISSION()
				EXIT
			ENDIF
			
			IF NETWORK_IS_ACTIVITY_SESSION() 
				EXIT
			ENDIF
			
			IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
				EXIT	
			ENDIF
			
			IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION_TRIGGER()
				EXIT
			ENDIF
			
			IF IS_SKYSWOOP_IN_SKY()
				EXIT
			ENDIF
			
			sInst.eLSIAUnlockState = BPUS__WAIT_FOR_COUNTDOWN
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BEACH_PARTY_TRIGGER_UNLOCK - Going to WAIT_FOR_COUNTDOWN")
		BREAK
		CASE BPUS__WAIT_FOR_COUNTDOWN
			IF IS_LOCAL_PLAYER_ON_ANY_FM_MISSION()
				EXIT
			ENDIF
			
			IF NETWORK_IS_ACTIVITY_SESSION() 
				EXIT
			ENDIF
			
			IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
				EXIT	
			ENDIF
			
			IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION_TRIGGER()
				EXIT
			ENDIF
			
			IF IS_SKYSWOOP_IN_SKY()
				EXIT
			ENDIF
			
			IF NOT HAS_NET_TIMER_EXPIRED(sInst.sLSIAUnlockCountdown, g_sMPTunables.iHEIST_ISLAND_BEACH_PARTY_UNLOCK_COUNTDOWN_MS, TRUE)
			#IF IS_DEBUG_BUILD AND NOT sInst.bSkipCountdown #ENDIF
				EXIT
			ENDIF
			
			sInst.eLSIAUnlockState = BPUS__UPDATE_TEXT_MESSAGE
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BEACH_PARTY_TRIGGER_UNLOCK - Going to UPDATE_TEXT_MESSAGE")
		BREAK
		CASE BPUS__UPDATE_TEXT_MESSAGE
			IF NETWORK_IS_ACTIVITY_SESSION() 
				EXIT
			ENDIF
			
			IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
				EXIT	
			ENDIF
			
			IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION_TRIGGER()
				EXIT
			ENDIF
			
			IF IS_LOCAL_PLAYER_ON_ANY_FM_MISSION()
				EXIT
			ENDIF
			
			IF NOT SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_NCLUBE, "BP_TXT_MESSAGE", TXTMSG_UNLOCKED)
				EXIT
			ENDIF
			
			PRINT_HELP_WITH_COLOURED_STRING("BP_UNLOCK", "BP_BLIP", HUD_COLOUR_WHITE)
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_SHOWN_BEACH_PARTY_UNLOCK_HELP, TRUE)
			sInst.eLSIAUnlockState = BPUS__WAIT_FOR_BLIP_FLASH
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BEACH_PARTY_TRIGGER_UNLOCK - Going to WAIT_FOR_BLIP_FLASH")
		BREAK
		CASE BPUS__WAIT_FOR_BLIP_FLASH
			// Wait for blip to create then flash it and mark as done
			IF NOT DOES_BLIP_EXIST(sInst.sLSIAToBeachPartyTrigger.biBlip)
				EXIT
			ENDIF
			
			SET_BLIP_FLASH_TIMER(sInst.sLSIAToBeachPartyTrigger.biBlip, 5000)
			
			RESET_NET_TIMER(sInst.sLSIAUnlockCountdown)
			sInst.eLSIAUnlockState = BPUS__WAIT_FOR_UNLOCK
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BEACH_PARTY_TRIGGER_UNLOCK - Going to WAIT_FOR_UNLOCK")
		BREAK
	ENDSWITCH
ENDPROC

///------------------------------------------
///    Warp Assets
///------------------------------------------    

/// PURPOSE:
///    Safely deletes the vehicle prop at the beach party that is used as the warp back to los santos
/// PARAMS:
///    sInst - the heist island travel instance
PROC _HEIST_ISLAND_TRAVEL__CLEANUP_LEAVE_BEACH_VEHICLE_PROP(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	SAFE_DELETE_VEHICLE(sInst.viLeaveBeachVehicleProp)
	PRINTLN("[HEIST_ISLAND_TRAVEL] CLEANUP_LEAVE_BEACH_VEHICLE_PROP - Cleaned up vehicle")
ENDPROC

/// PURPOSE:
///    The position of the beach party leave vehicle
/// RETURNS:
///    The position of the beach party leave vehicle
FUNC VECTOR _HEIST_ISLAND_TRAVEL__GET_LEAVE_BEACH_VEHICLE_PROP_POSITION()
	RETURN <<4921.079,-4905.775,2.874>>
ENDFUNC

/// PURPOSE:
///    Maintains creating the beach party leave vehicle prop, it will be created at _HEIST_ISLAND_TRAVEL__GET_LEAVE_BEACH_VEHICLE_PROP_POSITION
/// PARAMS:
///    sInst - the heist island travel instance
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_CREATING_LEAVE_BEACH_VEHICLE_PROP(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	IF DOES_ENTITY_EXIST(sInst.viLeaveBeachVehicleProp)
		EXIT
	ENDIF
	
	MODEL_NAMES eVehicleModel = INT_TO_ENUM(MODEL_NAMES, HASH("winky"))
	REQUEST_MODEL(eVehicleModel)
	
	IF GET_FRAME_COUNT() % 180 = 0
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_CREATING_LEAVE_BEACH_VEHICLE_PROP - Loading Vehicle model...")
	ENDIF
	
	VECTOR vVehiclePos = _HEIST_ISLAND_TRAVEL__GET_LEAVE_BEACH_VEHICLE_PROP_POSITION()
	REQUEST_COLLISION_AT_COORD(vVehiclePos)
	
	IF HAS_MODEL_LOADED(eVehicleModel)
		sInst.viLeaveBeachVehicleProp = CREATE_VEHICLE(eVehicleModel, vVehiclePos, DEFAULT, FALSE)
		SET_ENTITY_INVINCIBLE(sInst.viLeaveBeachVehicleProp, TRUE)
		SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(sInst.viLeaveBeachVehicleProp, FALSE)
		SET_VEHICLE_FULLBEAM(sInst.viLeaveBeachVehicleProp, FALSE)
		SET_VEHICLE_LIGHTS(sInst.viLeaveBeachVehicleProp, FORCE_VEHICLE_LIGHTS_OFF)
		SET_VEHICLE_DOORS_LOCKED(sInst.viLeaveBeachVehicleProp, VEHICLELOCK_LOCKED)
		SET_VEHICLE_FIXED(sInst.viLeaveBeachVehicleProp)
        SET_ENTITY_HEALTH(sInst.viLeaveBeachVehicleProp, 1000)
        SET_VEHICLE_ENGINE_HEALTH(sInst.viLeaveBeachVehicleProp, 1000)
        SET_VEHICLE_PETROL_TANK_HEALTH(sInst.viLeaveBeachVehicleProp, 1000)
        SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(sInst.viLeaveBeachVehicleProp, TRUE)
		SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(sInst.viLeaveBeachVehicleProp, TRUE)
		SET_ENTITY_CAN_BE_DAMAGED(sInst.viLeaveBeachVehicleProp, FALSE)
		SET_VEHICLE_RADIO_ENABLED(sInst.viLeaveBeachVehicleProp, FALSE)
		SET_VEHICLE_DOORS_LOCKED(sInst.viLeaveBeachVehicleProp, VEHICLELOCK_CANNOT_ENTER)
		SET_VEHICLE_DIRT_LEVEL(sInst.viLeaveBeachVehicleProp, 14.9)	
		SET_ENTITY_HEADING(sInst.viLeaveBeachVehicleProp, 50.4620)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(eVehicleModel)	
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_CREATING_LEAVE_BEACH_VEHICLE_PROP - Created leave beach vehicle")
	ENDIF
ENDPROC

ENUM BEACH_LEAVE_VEHICLE_BLOCK_RESULT
	BLVBR__AVAILABLE
	,BLVBR__NOT_ON_ISLAND
	,BLVBR__BEACH_PARTY_INACTIVE
	,BLVBR__NOT_AT_BEACH_PARTY
	,BLVBR__CORONA_BLOCKED
ENDENUM

DEBUGONLY FUNC STRING DEBUG_GET_BEACH_LEAVE_VEHICLE_BLOCK_RESULT_AS_STRING(BEACH_LEAVE_VEHICLE_BLOCK_RESULT eEnum)
	SWITCH eEnum
		CASE BLVBR__AVAILABLE				RETURN	"BLVBR__AVAILABLE"
		CASE BLVBR__NOT_ON_ISLAND			RETURN	"BLVBR__NOT_ON_ISLAND"
		CASE BLVBR__BEACH_PARTY_INACTIVE	RETURN	"BLVBR__BEACH_PARTY_INACTIVE"
		CASE BLVBR__NOT_AT_BEACH_PARTY		RETURN	"BLVBR__NOT_AT_BEACH_PARTY"
		CASE BLVBR__CORONA_BLOCKED			RETURN	"BLVBR__CORONA_BLOCKED"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_BEACH_LEAVE_VEHICLE_BLOCK_RESULT_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets if the beach party leave vehicle should be shown reason
/// PARAMS:
///    sInst - the heist island travel instance
/// RETURNS:
///    BLVBR__AVAILABLE if the vehicle should be shown
FUNC BEACH_LEAVE_VEHICLE_BLOCK_RESULT _HEIST_ISLAND_TRAVEL__SHOULD_SHOW_BEACH_LEAVE_VEHICLE(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	// Should only appear when the player is on the island
	IF NOT IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
		RETURN BLVBR__NOT_ON_ISLAND
	ENDIF
	
	// If the party is forced to be inactive it should not show
	IF NOT IS_BEACH_PARTY_ACTIVE()
		RETURN BLVBR__BEACH_PARTY_INACTIVE
	ENDIF
	
	// If the player is not at the beach party it should not show
	IF NOT IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY()
		RETURN BLVBR__NOT_AT_BEACH_PARTY
	ENDIF
	
	// Only show beach vehicle if Corona is visible, unless they are on the DJ scoping mission
	IF GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) != IHV_SCOPING_PARTY
		IF WARP_TRANSITION_TRIGGER_LOCATION__GET_IS_TRIGGER_BLOCKED(sInst.sBeachPartyToLSIATrigger)
			RETURN BLVBR__CORONA_BLOCKED
		ENDIF 
	ENDIF
	
	// Vehicle should show
	RETURN BLVBR__AVAILABLE
ENDFUNC

/// PURPOSE:
///    Maintains creating the beach party leave vehicle if it should be shown
/// PARAMS:
///    sInst - the heist island travel instance
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_LEAVE_BEACH_VEHICLE_PROP(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	BOOL bVehicleExists = DOES_ENTITY_EXIST(sInst.viLeaveBeachVehicleProp)
	BEACH_LEAVE_VEHICLE_BLOCK_RESULT eResult = _HEIST_ISLAND_TRAVEL__SHOULD_SHOW_BEACH_LEAVE_VEHICLE(sInst)
	
	IF eResult = BLVBR__AVAILABLE
		IF NOT bVehicleExists
			_HEIST_ISLAND_TRAVEL__MAINTAIN_CREATING_LEAVE_BEACH_VEHICLE_PROP(sInst)
		ENDIF
	ELSE
		IF bVehicleExists
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_LEAVE_BEACH_VEHICLE_PROP - Vehicle spawn blocked: ", DEBUG_GET_BEACH_LEAVE_VEHICLE_BLOCK_RESULT_AS_STRING(eResult))
			_HEIST_ISLAND_TRAVEL__CLEANUP_LEAVE_BEACH_VEHICLE_PROP(sInst)
		ENDIF
	ENDIF
ENDPROC

///------------------------------------------
///    Background Warp System
///------------------------------------------    

/// PURPOSE:
///    Initialises all of the heist island travel warp locations and maintained transitions
/// PARAMS:
///    sInst - the heist island travel instance
PROC INIT_HEIST_ISLAND_TRAVEL(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	HEIST_ISLAND_DATA__GET_LOS_SANTOS_AIRPORT_WARP_TRIGGER(sInst.sLSIAToBeachPartyTrigger)
	HEIST_ISLAND_DATA__GET_HEIST_ISLAND_BEACH_WARP_TRIGGER(sInst.sBeachPartyToLSIATrigger)
ENDPROC

/// PURPOSE:
///    Sets the state of the heist island travel background warps
/// PARAMS:
///    eNewState - the state to switch to
PROC _HEIST_ISLAND_TRAVEL__SET_GLOBAL_TRANSITION_STATE(HEIST_ISLAND_TRANSITION_STATE eNewState)
	DEBUG_PRINTCALLSTACK()
	g_sHeistIslandTravel.eState = eNewState
	PRINTLN("[HEIST_ISLAND_TRAVEL] SET_GLOBAL_TRANSITION_STATE - eState: ", eNewState)
ENDPROC

/// PURPOSE:
///    Checks if the island background warp has finished, this will return false if a transition was never started
/// RETURNS:
///    TRUE if the island background warp has failed or succeded
FUNC BOOL HEIST_ISLAND_TRAVEL__HAS_ISLAND_TRANSITION_FINISHED()
	RETURN g_sHeistIslandTravel.eState = HEIST_ISLAND_TRANSITION_STATE_SUCCESS
		OR g_sHeistIslandTravel.eState = HEIST_ISLAND_TRANSITION_STATE_FAILED
ENDFUNC

/// PURPOSE:
///    Requests a transition to the island, it will only work if not already on the island and a leave island request is not in progress
/// PARAMS:
///    eTransitionType - Which transition to use, this will play a differnet cinematic and may do things such as restrict player to the beach area
///    iInstanceID - Sets the tutorial instance the player will be put into on the island, if left as -1 it will use the default instance for the transition type
///    				 e.g. beach party transitions will put them in the beach party tutorial instance
/// RETURNS:
///    TRUE if successful
FUNC BOOL HEIST_ISLAND_TRAVEL__JOIN_ISLAND(HEIST_ISLAND_JOIN_TRANSITIONS eTransitionType = HEIST_ISLAND_JOIN_FADE_OUT_AND_IN)
	IF HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
		PRINTLN("[HEIST_ISLAND_TRAVEL] JOIN_ISLAND - Failed! transition already in progress")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
		PRINTLN("[HEIST_ISLAND_TRAVEL] JOIN_ISLAND - Failed! an unmanaged transition is in progress!")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	WARP_TRANSITION_CONFIG_STRUCT sWarp
	HEIST_ISLAND_DATA__GET_JOIN_TRANSITION_CONFIG(eTransitionType, sWarp)
	
	IF WARP_TRANSITION_CONFIG__IS_WARP_BLOCKED(sWarp)
		PRINTLN("[HEIST_ISLAND_TRAVEL] JOIN_ISLAND - Failed! transition is blocked")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	// Setup fresh transition
	HEIST_ISLAND_TRANSITION sTransition
	sTransition.iTransitionType = ENUM_TO_INT(eTransitionType)
	COPY_SCRIPT_STRUCT(g_sHeistIslandTravel.sTransition, sTransition, SIZE_OF(sTransition))
	
	// Start the process
	g_sHeistIslandTravel.bIsJoinTransition = TRUE
	_HEIST_ISLAND_TRAVEL__SET_GLOBAL_TRANSITION_STATE(HEIST_ISLAND_TRANSITION_STATE_IN_PROGRESS)
	
	g_bDelayShardStart = TRUE

	PRINTLN("[HEIST_ISLAND_TRAVEL] JOIN_ISLAND - Started with transition: ", DEBUG_GET_HEIST_ISLAND_JOIN_TRANSITIONS_AS_STRING(eTransitionType))
	DEBUG_PRINTCALLSTACK()
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Requests a transition to the island and places the player at the specified position, it will only work if not already on the island and a leave island request is not in progress
/// PARAMS:
///    vPosition - Sets the position the player will arrive at, this overrides the transition types default position. WILL NOT OVERRIDE IF SET TO <<0.0, 0.0, 0.0>> 
///    fHeading - Sets the heading the player will face when they arrive, this overrides the transition types default heading
///    eTransitionType - Which transition to use, this will play a different cinematic and may do things such as restrict player to the beach area
///    iInstanceID -  Sets the tutorial instance the player will be put into on the island, if left as -1 it will use the default instance for the transition type
///    				 e.g. beach party transitions will put them in the beach party tutorial instance
/// RETURNS:
///    TRUE if successful
FUNC BOOL HEIST_ISLAND_TRAVEL__JOIN_ISLAND_WITH_OVERRIDES(HEIST_ISLAND_JOIN_TRANSITIONS eTransitionType, HEIST_ISLAND_TRANSITION &sOverrides)
	IF HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
		PRINTLN("[HEIST_ISLAND_TRAVEL] JOIN_ISLAND_WITH_OVERRIDES - Failed! transition already in progress")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
		PRINTLN("[HEIST_ISLAND_TRAVEL] JOIN_ISLAND_WITH_OVERRIDES - Failed! an unmanaged transition is in progress!")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	WARP_TRANSITION_CONFIG_STRUCT sWarp
	HEIST_ISLAND_DATA__GET_JOIN_TRANSITION_CONFIG(eTransitionType, sWarp)
	
	IF WARP_TRANSITION_CONFIG__IS_WARP_BLOCKED(sWarp)
		PRINTLN("[HEIST_ISLAND_TRAVEL] JOIN_ISLAND_WITH_OVERRIDES - Failed! transition is blocked")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	sOverrides.iTransitionType = ENUM_TO_INT(eTransitionType)
	COPY_SCRIPT_STRUCT(g_sHeistIslandTravel.sTransition, sOverrides, SIZE_OF(sOverrides))
	
	g_sHeistIslandTravel.bIsJoinTransition = TRUE
	_HEIST_ISLAND_TRAVEL__SET_GLOBAL_TRANSITION_STATE(HEIST_ISLAND_TRANSITION_STATE_IN_PROGRESS)
	
	g_bDelayShardStart = TRUE
	
	PRINTLN("[HEIST_ISLAND_TRAVEL] JOIN_ISLAND_WITH_OVERRIDES - Started with transition: ", DEBUG_GET_HEIST_ISLAND_JOIN_TRANSITIONS_AS_STRING(eTransitionType))
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Requests a transition to leave the island, it will only work if on the island and a join island request is not in progress
/// PARAMS:
///    eTransitionType - Which transition to use, this will play a different cinematic and may set various flags
/// RETURNS:
///    TRUE if successful
FUNC BOOL HEIST_ISLAND_TRAVEL__LEAVE_ISLAND(HEIST_ISLAND_LEAVE_TRANSITIONS eTransitionType = HEIST_ISLAND_LEAVE_FADE_OUT_AND_IN)
	IF HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
		PRINTLN("[HEIST_ISLAND_TRAVEL] LEAVE_ISLAND - Failed! transition already in progress")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
		PRINTLN("[HEIST_ISLAND_TRAVEL] LEAVE_ISLAND - Failed! an unmanaged transition is in progress!")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	WARP_TRANSITION_CONFIG_STRUCT sWarp
	HEIST_ISLAND_DATA__GET_LEAVE_TRANSITION_CONFIG(eTransitionType, sWarp)
	
	IF WARP_TRANSITION_CONFIG__IS_WARP_BLOCKED(sWarp)
		PRINTLN("[HEIST_ISLAND_TRAVEL] LEAVE_ISLAND - Failed! transition is blocked")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	HEIST_ISLAND_TRANSITION sOverrides
	sOverrides.iTransitionType = ENUM_TO_INT(eTransitionType)
	COPY_SCRIPT_STRUCT(g_sHeistIslandTravel.sTransition, sOverrides, SIZE_OF(sOverrides))
	
	g_sHeistIslandTravel.bIsJoinTransition = FALSE
	_HEIST_ISLAND_TRAVEL__SET_GLOBAL_TRANSITION_STATE(HEIST_ISLAND_TRANSITION_STATE_IN_PROGRESS)
	
	g_bDelayShardStart = TRUE
	
	PRINTLN("[HEIST_ISLAND_TRAVEL] LEAVE_ISLAND - Started with transition: ", DEBUG_GET_HEIST_ISLAND_LEAVE_TRANSITIONS_AS_STRING(eTransitionType))
	DEBUG_PRINTCALLSTACK()
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Requests a transition to leave the island placing the player at the specified position, it will only work if on the island and a join island request is not in progress
/// PARAMS:
///    vPosition - Sets the position the player will arrive at, this overrides the transition types default position, WILL NOT OVERRIDE IF SET TO <<0.0, 0.0, 0.0>> 
///    fHeading - Sets the heading the player will face when they arrive, this overrides the transition types default heading
///    eTransitionType - Which transition to use, this will play a different cinematic and may set various flags
/// RETURNS:
///    TRUE if successful
FUNC BOOL HEIST_ISLAND_TRAVEL__LEAVE_ISLAND_WITH_OVERRIDES(HEIST_ISLAND_LEAVE_TRANSITIONS eTransitionType, HEIST_ISLAND_TRANSITION &sOverrides)
	IF HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
		PRINTLN("[HEIST_ISLAND_TRAVEL] LEAVE_ISLAND_WITH_OVERRIDES - Failed! transition already in progress")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
		PRINTLN("[HEIST_ISLAND_TRAVEL] LEAVE_ISLAND_WITH_OVERRIDES - Failed! an unmanaged transition is in progress!")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	WARP_TRANSITION_CONFIG_STRUCT sWarp
	HEIST_ISLAND_DATA__GET_LEAVE_TRANSITION_CONFIG(eTransitionType, sWarp)
	
	IF WARP_TRANSITION_CONFIG__IS_WARP_BLOCKED(sWarp)
		PRINTLN("[HEIST_ISLAND_TRAVEL] LEAVE_ISLAND_WITH_OVERRIDES - Failed! transition is blocked")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	sOverrides.iTransitionType = ENUM_TO_INT(eTransitionType)
	COPY_SCRIPT_STRUCT(g_sHeistIslandTravel.sTransition, sOverrides, SIZE_OF(sOverrides))
	
	g_sHeistIslandTravel.bIsJoinTransition = FALSE
	_HEIST_ISLAND_TRAVEL__SET_GLOBAL_TRANSITION_STATE(HEIST_ISLAND_TRANSITION_STATE_IN_PROGRESS)
	
	g_bDelayShardStart = TRUE
	PRINTLN("[HEIST_ISLAND_TRAVEL] LEAVE_ISLAND_WITH_OVERRIDES - Started with overrides")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates and returns a warp config struct according to the global background warp data overrides
/// RETURNS:
///    A warp transition config struct configured with the global background systems overrides
FUNC WARP_TRANSITION_CONFIG_STRUCT _HEIST_ISLAND_TRAVEL__GET_WARP_CONFIG_FOR_CURRENT_GLOBAL_TRANSITION()
	WARP_TRANSITION_CONFIG_STRUCT sTransition
	INT iTransitionID = g_sHeistIslandTravel.sTransition.iTransitionType
	
	IF g_sHeistIslandTravel.bIsJoinTransition
		HEIST_ISLAND_JOIN_TRANSITIONS eJoinTransition = INT_TO_ENUM(HEIST_ISLAND_JOIN_TRANSITIONS, iTransitionID)
		HEIST_ISLAND_DATA__GET_JOIN_TRANSITION_CONFIG(eJoinTransition, sTransition)
	ELSE
		HEIST_ISLAND_LEAVE_TRANSITIONS eLeaveTransition = INT_TO_ENUM(HEIST_ISLAND_LEAVE_TRANSITIONS, iTransitionID)
		HEIST_ISLAND_DATA__GET_LEAVE_TRANSITION_CONFIG(eLeaveTransition, sTransition)
	ENDIF
	
	// We may want to overide the arrive position on this transition for convenience
	VECTOR vPos = g_sHeistIslandTravel.sTransition.vPositionOverride
	
	IF NOT ARE_VECTORS_EQUAL(vPos, <<0.0, 0.0, 0.0>>)
		WARP_TRANSITION_CONFIG__SET_ARRIVE_LOCATION(sTransition, vPos)
		sTransition.bHasBackupPositions = FALSE
		PRINTLN("[HEIST_ISLAND_TRAVEL] GET_WARP_CONFIG_FOR_CURRENT_GLOBAL_TRANSITION - Position override set, ignoring backup positions: ", vPos)
	ENDIF
	
	FLOAT fHeading = g_sHeistIslandTravel.sTransition.fHeadingOverride
	
	IF fHeading != cfHEIST_ISLAND_TRAVEL_HEADING_UNASSIGNED
		WARP_TRANSITION_CONFIG__SET_ARRIVE_HEADING(sTransition, fHeading)
		PRINTLN("[HEIST_ISLAND_TRAVEL] GET_WARP_CONFIG_FOR_CURRENT_GLOBAL_TRANSITION - Heading override set: ", fHeading)
	ENDIF
	
	// Missions may define a tutorial instance they want to use
	INT iTutorialInstance =  g_sHeistIslandTravel.sTransition.iInstanceID
	
	IF iTutorialInstance != -1
		WARP_TRANSITION_CONFIG__SET_TUTORIAL_INSTANCE_ID(sTransition, iTutorialInstance)
		PRINTLN("[HEIST_ISLAND_TRAVEL] GET_WARP_CONFIG_FOR_CURRENT_GLOBAL_TRANSITION - Tutorial instance override set: ", iTutorialInstance)
	ENDIF
	
	sTransition.iBS = g_sHeistIslandTravel.sTransition.iConfigBS
	PRINTLN("[HEIST_ISLAND_TRAVEL] GET_WARP_CONFIG_FOR_CURRENT_GLOBAL_TRANSITION - iBS set: ", sTransition.iBS)	
	
	HEIST_ISLAND_TRANSITION_CUTSCENE eLeave = g_sHeistIslandTravel.sTransition.eLeaveCutscene
	
	IF eLeave != HEIST_ISLAND_TRANSITION_CUTSCENE_INVALID
		HEIST_ISLAND_DATA__SET_WARP_CONFIG_LEAVE_CINEMATIC(eLeave, sTransition)
		PRINTLN("[HEIST_ISLAND_TRAVEL] GET_WARP_CONFIG_FOR_CURRENT_GLOBAL_TRANSITION - Using leave cutscene override: ", DEBUG_GET_HEIST_ISLAND_TRANSITION_CUTSCENE_AS_STRING(eLeave))
	ENDIF
	
	HEIST_ISLAND_TRANSITION_CUTSCENE eArrive = g_sHeistIslandTravel.sTransition.eArriveCutscene
	
	IF eArrive != HEIST_ISLAND_TRANSITION_CUTSCENE_INVALID
		HEIST_ISLAND_DATA__SET_WARP_CONFIG_ARRIVE_CINEMATIC(eArrive, sTransition)
		PRINTLN("[HEIST_ISLAND_TRAVEL] GET_WARP_CONFIG_FOR_CURRENT_GLOBAL_TRANSITION - Using arrive cutscene override: ", DEBUG_GET_HEIST_ISLAND_TRANSITION_CUTSCENE_AS_STRING(eArrive))
	ENDIF	
	
	sTransition.eArriveOutfit = g_sHeistIslandTravel.sTransition.eArriveOutfit
	sTransition.eArriveBossOutfit = g_sHeistIslandTravel.sTransition.eArriveBossOutfit
	
	RETURN sTransition
ENDFUNC

/// PURPOSE:
///    Cleans up the warp transition by setting the player as no longer invincible
PROC _HEIST_ISLAND_TRAVEL__CLEANUP_WARP_TRANSITION()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the last background warp by clearing the warp config struct
PROC _HEIST_ISLAND_TRAVEL__CLEAR_LAST_TRANSITION()
	g_sHeistIslandTravel.bIsJoinTransition = FALSE
	HEIST_ISLAND_TRANSITION sTransition
	COPY_SCRIPT_STRUCT(g_sHeistIslandTravel.sTransition, sTransition, SIZE_OF(sTransition))
ENDPROC

/// PURPOSE:
///    Maintains the service for fade warping back from the island to Los Santos
///    this is so people can quickly warp leave the island without having to configure a transition themselves
/// PARAMS:
///    sInst - the heist island travel instance
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_BACKGROUND_WARPS(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	IF NOT HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
		EXIT
	ENDIF
	
	IF NOT WARP_TRANSITION__IS_INITIALIZED(sInst.sQuickIslandWarp)
		WARP_TRANSITION_CONFIG_STRUCT sTransition = _HEIST_ISLAND_TRAVEL__GET_WARP_CONFIG_FOR_CURRENT_GLOBAL_TRANSITION()
		INIT_WARP_TRANSITION(sInst.sQuickIslandWarp, sTransition)	
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BACKGROUND_WARPS - Starting transition")
	ENDIF
		
	IF NOT IS_WARP_TRANSITION_IN_PROGRESS(sInst.sQuickIslandWarp) 
		IF NOT START_WARP_TRANSITION(sInst.sQuickIslandWarp)
			_HEIST_ISLAND_TRAVEL__SET_GLOBAL_TRANSITION_STATE(HEIST_ISLAND_TRANSITION_STATE_FAILED)
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BACKGROUND_WARPS - FAILED - Warp transition is blocked")
			EXIT
		ENDIF
	ENDIF
	
	IF MAINTAIN_WARP_TRANSITION(sInst.sQuickIslandWarp)
		_HEIST_ISLAND_TRAVEL__SET_GLOBAL_TRANSITION_STATE(HEIST_ISLAND_TRANSITION_STATE_SUCCESS)
		_HEIST_ISLAND_TRAVEL__CLEAR_LAST_TRANSITION()
		_HEIST_ISLAND_TRAVEL__CLEANUP_WARP_TRANSITION()
		CLEANUP_WARP_TRANSITION(sInst.sQuickIslandWarp)
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BACKGROUND_WARPS - SUCCESS - Left the island")
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
DEBUGONLY PROC _HEIST_ISLAND_TRAVEL_DEBUG__DISPLAY_TRAVEL_GLOBALS(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	UNUSED_PARAMETER(sInst)

	SET_TEXT_SCALE(0.5, 0.5)
	VECTOR vOrigin = <<0.1, 0.1, 0.1>>
	VECTOR vOffset = <<0.0, GET_RENDERED_CHARACTER_HEIGHT(), 0.0>>
	TEXT_LABEL_63 strText = "Local player on island: "
	strText += GET_STRING_FROM_BOOL(IS_LOCAL_PLAYER_ON_HEIST_ISLAND())
	DISPLAY_TEXT_WITH_LITERAL_STRING(vOrigin.x, vOrigin.y, "STRING", GET_STRING_FROM_TL(strText))
	
	SET_TEXT_SCALE(0.5, 0.5)
	vOrigin += vOffset
	strText = "At beach party: " 
	strText += GET_STRING_FROM_BOOL(IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY())
	DISPLAY_TEXT_WITH_LITERAL_STRING(vOrigin.x, vOrigin.y, "STRING",  GET_STRING_FROM_TL(strText))
	
	SET_TEXT_SCALE(0.5, 0.5)
	vOrigin += vOffset
	strText = "Restricted to party: " 
	strText += GET_STRING_FROM_BOOL(IS_LOCAL_PLAYER_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY())
	DISPLAY_TEXT_WITH_LITERAL_STRING(vOrigin.x, vOrigin.y, "STRING",  GET_STRING_FROM_TL(strText))
	
	SET_TEXT_SCALE(0.5, 0.5)
	vOrigin += vOffset
	strText = "In Warp Transition Global: " 
	strText += GET_STRING_FROM_BOOL(IS_LOCAL_PLAYER_IN_WARP_TRANSITION())
	DISPLAY_TEXT_WITH_LITERAL_STRING(vOrigin.x, vOrigin.y, "STRING",  GET_STRING_FROM_TL(strText))
	
	SET_TEXT_SCALE(0.5, 0.5)
	vOrigin += vOffset
	strText = "In Warp Trigger Global: " 
	strText += GET_STRING_FROM_BOOL(IS_LOCAL_PLAYER_IN_WARP_TRANSITION_TRIGGER())
	DISPLAY_TEXT_WITH_LITERAL_STRING(vOrigin.x, vOrigin.y, "STRING",  GET_STRING_FROM_TL(strText))
ENDPROC

DEBUGONLY PROC HEIST_ISLAND_TRAVEL__ADD_DEBUG_WIDGETS(HEIST_ISLAND_TRAVEL_STRUCT &sInst)	
	sInst.bShowGlobals = GET_COMMANDLINE_PARAM_EXISTS("sc_HITravelGlobalsDebug")
	
	START_WIDGET_GROUP("Heist Island Travel")
		ADD_WIDGET_BOOL("Draw Travel Globals", sInst.bShowGlobals)
		ADD_WIDGET_BOOL("Force warp trigger bail", g_bWarpTriggerDebugBailTest)
		START_WIDGET_GROUP("Beach Party Unlocking")
			ADD_WIDGET_BOOL("Trigger Beach Party Unlock Sequence", sInst.bTriggerLSIAUnlock)
			ADD_WIDGET_BOOL("Skip Countdown", sInst.bSkipCountdown)
			ADD_WIDGET_INT_SLIDER("Countdown duration (ms): ", g_sMPTunables.iHEIST_ISLAND_BEACH_PARTY_UNLOCK_COUNTDOWN_MS, 0, 60 * 1000 * 30, 1000)
			ADD_WIDGET_BOOL("Reset",  sInst.bResetLSIAUnlock)
			ADD_WIDGET_BOOL("Reset Vibes Award", sInst.bResetVibesAward)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_DEBUG(HEIST_ISLAND_TRAVEL_STRUCT &sInst)	
	IF sInst.bTriggerLSIAUnlock
		sInst.bTriggerLSIAUnlock = FALSE
		
		IF sInst.eLSIAUnlockState = BPUS__WAIT_FOR_UNLOCK
			sInst.eLSIAUnlockState = BPUS__WAIT_FOR_COUNTDOWN
			PRINT_DEBUG_TICKER("Starting beach party unlock")
		ELSE
			PRINT_DEBUG_TICKER("Beach party unlock already in progress, use Skip Countdown")
		ENDIF
	ENDIF
	
	IF sInst.bResetVibesAward
		sInst.bResetVibesAward = FALSE
		SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_PARTY_VIBES, FALSE)
		PRINT_DEBUG_TICKER("Reset vibes award")
	ENDIF
	
	IF sInst.bResetLSIAUnlock
		sInst.bResetLSIAUnlock = FALSE
		
		IF sInst.eLSIAUnlockState = BPUS__WAIT_FOR_UNLOCK
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_SHOWN_BEACH_PARTY_UNLOCK_HELP, FALSE)
			PRINT_DEBUG_TICKER("Beach party unlock reset")
		ELSE
			PRINT_DEBUG_TICKER("Beach party unlock in progress, wait for it to end before resetting")
		ENDIF
	ENDIF
	
	IF sInst.bShowGlobals
		_HEIST_ISLAND_TRAVEL_DEBUG__DISPLAY_TRAVEL_GLOBALS(sInst)
	ENDIF

ENDPROC
#ENDIF

/// PURPOSE:
///    Maintains updating the island travel warp trigger coronas at LSIA and the beach party
///    these will only show if they are not blocked by their assigned block conditions
/// PARAMS:
///    sInst - the heist island travle instance
/// RETURNS:
///    TRUE is a warp trigger corona is warping the player
FUNC BOOL _HEIST_ISLAND_TRAVEL__MAINTAIN_WARP_TRIGGERS(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	
	// Only the relevant blip every other frame
	IF GET_FRAME_COUNT() % 2 = 0
		IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()		
			WARP_TRANSITION_TRIGGER_LOCATION__MAINTAIN_BLIP(sInst.sBeachPartyToLSIATrigger)	
		ELSE
			WARP_TRANSITION_TRIGGER_LOCATION__MAINTAIN_BLIP(sInst.sLSIAToBeachPartyTrigger)
		ENDIF
	ENDIF
		
	VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())		
	BOOL bTransitionLISAToBeach = IS_WARP_TRANSITION_TRIGGER_RUNNING_TRANSITION(sInst.sLSIAToBeachPartyTrigger)
	BOOL bTransitionBeachToLISA = IS_WARP_TRANSITION_TRIGGER_RUNNING_TRANSITION(sInst.sBeachPartyToLSIATrigger)
		
	// We only want to call maintain for the warp trigger if it's already been triggered, or close enough for the player to trigger
	IF bTransitionLISAToBeach
	OR GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, sInst.sLSIAToBeachPartyTrigger.vLeaveMarkerLocation) < 30
		MAINTAIN_WARP_TRANSITION_TRIGGER_LOCATION(sInst.sLSIAToBeachPartyTrigger)
		IF bTransitionLISAToBeach
			RETURN TRUE
		ENDIF
	ENDIF
		
	IF bTransitionBeachToLISA
	OR GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, sInst.sBeachPartyToLSIATrigger.vLeaveMarkerLocation) < 30
		MAINTAIN_WARP_TRANSITION_TRIGGER_LOCATION(sInst.sBeachPartyToLSIATrigger)
		IF bTransitionBeachToLISA
			RETURN TRUE
		ENDIF
	ENDIF
					
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintains warping the player to the island if they accept a beach party invite. If they are on a mission or already on the island
///    the invite will be cleared
/// PARAMS:
///    sInst - the heist island travel instance
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_ISLAND_INVITES(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	IF HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
		EXIT
	ENDIF
	
	PLAYER_INDEX piInvitingPlayer = INT_TO_NATIVE(PLAYER_INDEX, sInst.iStaggeredPlayerCheck)	
	
	IF Does_Basic_Invite_From_Player_Exist(piInvitingPlayer, FMMC_TYPE_HEIST_ISLAND_INVITE)
		IF Has_Player_Accepted_Basic_Invite_From_Player(piInvitingPlayer, FMMC_TYPE_HEIST_ISLAND_INVITE)
			IF NOT g_bg_blockAcceptingHeistIslandInvite
				HEIST_ISLAND_TRANSITION sOverrides
				SET_BIT_ENUM(sOverrides.iConfigBS, WTCB_INVITED_WARP)
				HEIST_ISLAND_TRAVEL__JOIN_ISLAND_WITH_OVERRIDES(HEIST_ISLAND_JOIN_BEACH_PARTY_FADE, sOverrides)
			ENDIF
				
			Cancel_Basic_Invite_From_Player(piInvitingPlayer, FMMC_TYPE_HEIST_ISLAND_INVITE)
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_ISLAND_INVITES - Invite accepted, joining island - g_bg_blockAcceptingHeistIslandInvite = ", g_bg_blockAcceptingHeistIslandInvite)
		ENDIF
		
		IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
		OR IS_LOCAL_PLAYER_ON_ANY_FM_MISSION()
			Cancel_Basic_Invite_From_Player(piInvitingPlayer, FMMC_TYPE_HEIST_ISLAND_INVITE)
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_ISLAND_INVITES - Invite no longer valid, Cancelling")
		ENDIF
	ENDIF
ENDPROC

PROC _HEIST_ISLAND_TRAVEL__TRIGGER_WARP_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_HEIST_ISLAND_WARP_TRANSITION sEvent
	
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, sEvent, SIZE_OF(sEvent))
		EXIT
	ENDIF
	
	IF sEvent.Details.FromPlayerIndex != INVALID_PLAYER_INDEX()
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_MASS_WARP_EVENTS - Mass warp event recieved from player: ", GET_PLAYER_NAME(sEvent.Details.FromPlayerIndex))
	ELSE
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_MASS_WARP_EVENTS - Mass warp event recieved from invalid player")
	ENDIF
	
	IF sEvent.bIsIslandJoin
		HEIST_ISLAND_TRAVEL__JOIN_ISLAND(INT_TO_ENUM(HEIST_ISLAND_JOIN_TRANSITIONS, sEvent.iTransitionType))
	ELSE
		HEIST_ISLAND_TRAVEL__LEAVE_ISLAND(INT_TO_ENUM(HEIST_ISLAND_LEAVE_TRANSITIONS, sEvent.iTransitionType))
	ENDIF	
ENDPROC

PROC _HEIST_ISLAND_TRAVEL__TRIGGER_ISLAND_HELI_LAUNCH(HEIST_ISLAND_TRAVEL_STRUCT &sHeistIslandTravel, INT iEventID)
	IF sHeistIslandTravel.bLaunchBackupHeli
		PRINTLN("[HEIST_ISLAND_TRAVEL][HELI] TRIGGER_ISLAND_HELI_LAUNCH - Already launching heli")
		EXIT
	ENDIF
	
	SCRIPT_EVENT_DATA_ISLAND_BACKUP_HELI_LAUNCH sEvent
	
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, sEvent, SIZE_OF(sEvent))
		EXIT
	ENDIF
	
	PRINTLN("[HEIST_ISLAND_TRAVEL][HELI] TRIGGER_ISLAND_HELI_LAUNCH - instance: ", sEvent.iScriptInstance)
	
	sHeistIslandTravel.sHeliLaunch.mdID.idMission = eAM_ISLAND_BACKUP_HELI
	sHeistIslandTravel.sHeliLaunch.iInstanceId = sEvent.iScriptInstance
	sHeistIslandTravel.bLaunchBackupHeli = TRUE
	PRINTLN("[HEIST_ISLAND_TRAVEL][HELI] TRIGGER_ISLAND_HELI_LAUNCH - starting heli launch")
ENDPROC

/// PURPOSE:
///    Maintins cleaning up the island flags if the player is somehow pulled away from the island. This ensures the flags will always be valid
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_FLAG_CLEANUP_FAILSAFES()
	IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
		IF NOT g_bDelayShardStart
			g_bDelayShardStart = TRUE
			PRINTLN("[HEIST_ISLAND_TRAVEL] FLAG_CLEANUP_FAILSAFES - In transition so blocking shard")
		ENDIF
		
		EXIT
	ENDIF
	
	IF g_bDelayShardStart
		g_bDelayShardStart = FALSE
		PRINTLN("[HEIST_ISLAND_TRAVEL] FLAG_CLEANUP_FAILSAFES - Clearing block shard")
	ENDIF
	
	BOOL bOnIsland = IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
	
	IF NOT bOnIsland AND IS_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY()
		SET_LOCAL_PLAYER_AT_HEIST_ISLAND_BEACH_PARTY(FALSE)
		PRINTLN("[HEIST_ISLAND_TRAVEL] FLAG_CLEANUP_FAILSAFES - Player was at beach party but not at island!")
	ENDIF
	
	IF bOnIsland AND NOT HEIST_ISLAND_LOADING__ARE_IPLS_ACTIVE()
		_SET_LOCAL_PLAYER_IS_ON_HEIST_ISLAND(FALSE)
		PRINTLN("[HEIST_ISLAND_TRAVEL] FLAG_CLEANUP_FAILSAFES - Player was on island but ipls are not loaded!")
	ENDIF
	
	IF NOT bOnIsland AND IS_LOCAL_PLAYER_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY()
		SET_LOCAL_PLAYER_IS_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY(FALSE)
		PRINTLN("[HEIST_ISLAND_TRAVEL] FLAG_CLEANUP_FAILSAFES - Player was not on the island but was restricted to the beach party!")
	ENDIF
	
	IF NOT bOnIsland AND NOT ARE_ALL_BEACH_PARTY_RESTRICTED_AREAS_ACTIVE_FOR_LOCAL_PLAYER()
		ENABLE_ALL_BEACH_PARTY_RESTRICTED_AREAS_FOR_LOCAL_PLAYER(TRUE)
		PRINTLN("AM_MP_ISLAND - FLAG_CLEANUP_FAILSAFES - layer was not on the island and the restricted areas weren't reset")
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains enabling/disabling the waterplanes for the new island. These planes fill the gap underneath los santos which causes interiors to be flooded, so this
///    maintenance function makes sure they are never enabled when in an interior
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_WATER_PLANES()
	BOOL bWaterBlocked = HEIST_ISLAND_LOADING__ARE_WATER_IPLS_BLOCKED()
	bool bWaterActive = HEIST_ISLAND_LOADING__ARE_WATER_IPLS_ACTIVE()
	
	// The island loads new water IPLs under los santos, meaning we can't have the island loaded when in interiors because they are under the map
	IF bWaterBlocked AND bWaterActive
		HEIST_ISLAND_LOADING__ENABLE_WATER_IPLS(FALSE)
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_HEIST_ISLAND_TRAVEL_WATER_IPLS - Water is blocked, unloading island IPLs to avoid water flooded interior")
		EXIT
	ENDIF
	
	// If not iblocked and the island is loaded make sure the water is on
	IF NOT bWaterBlocked AND NOT bWaterActive AND HEIST_ISLAND_LOADING__ARE_IPLS_ACTIVE()
		HEIST_ISLAND_LOADING__ENABLE_WATER_IPLS(TRUE)
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_HEIST_ISLAND_TRAVEL_WATER_IPLS - Island is loading but water IPLs aren't, loading water IPLs")
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a skeleton should be shown in the panther cage on the island.
/// RETURNS:
///    TRUE is the player is on the finale and has completed the heist as a leader or if they are on the smuggler mission
FUNC BOOL _HEIST_ISLAND_TRAVEL__SHOULD_SHOW_SKELETON()
	IF NOT IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
		RETURN FALSE
	ENDIF
	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_MISSION_FINALE()
		IF HAS_PLAYER_COMPLETED_ISLAND_HEIST_AS_LEADER(GB_GET_LOCAL_PLAYER_GANG_BOSS()) 
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = IHV_SCOPING_SMUGGLER
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintains enabling the panther cage skeleton if the conditions are met
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_CAGE_SKELETON()
	STRING strIPL = "h4_Mansion_Remains_Cage"

	IF _HEIST_ISLAND_TRAVEL__SHOULD_SHOW_SKELETON()
		IF NOT IS_IPL_ACTIVE(strIPL) 
			REQUEST_IPL(strIPL)
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_CAGE_SKELETON - Loading skeleton doot...")
		ENDIF
	ELSE
		IF IS_IPL_ACTIVE(strIPL) 
			REMOVE_IPL(strIPL)
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_CAGE_SKELETON - Removing skeleton...")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains various bail conditions for kicking the player from the island
///    each bail condition is behing a tuneable so we can turn it off from a bacground switch
///    be careful when adding bails, this will process whenever the island runs
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_ISLAND_BAILS(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	IF g_bBlockAllIslandBails
		EXIT
	ENDIF
	
	// Shouldn't do anything if we are transitioning
	IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
		EXIT
	ENDIF
	
	// Do not process any bails if a transition is requested
	IF HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
		EXIT
	ENDIF
	
	// Don't check in activity session (i.e. on mission)
	IF NETWORK_IS_ACTIVITY_SESSION()
		EXIT
	ENDIF
	
	// If the player somehow escapes the boundary of the island unload it
	IF NOT g_bBlockIslandOutOfBoundsBail
	AND NOT g_SpawnData.bPassedOutDrunk // Passing out can spawn player off island, need to wait for respawn to initiate travel
		IF HEIST_ISLAND_LOADING__ARE_IPLS_ACTIVE() AND NOT GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
			// Squared distance so square the radius
			IF GET_DISTANCE_BETWEEN_COORDS_SQUARED(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<5017.422363, -5130.447754, 2.199280>>) > g_fIslandOutOfBoundsRadius * g_fIslandOutOfBoundsRadius
				HEIST_ISLAND_LOADING__UNLOAD_ISLAND()
			ENDIF
		ENDIF
	ENDIF
	
	// Can only bail if on island
	IF NOT IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
		EXIT
	ENDIF
	
	// If we are on a scoping mission and in the beach party session we should get kicked
	IF NOT g_bBlockIslandScopingMissionBail
		IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
			IF NETWORK_IS_IN_TUTORIAL_SESSION()
				IF NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(PLAYER_ID()) = ciHEIST_ISLAND_BEACH_PARTY_TUTORIAL_INSTANCE_ID
					PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_ISLAND_BAILS - Player is on scoping mission but is at the post finale beach party, leaving island")
					HEIST_ISLAND_TRAVEL__LEAVE_ISLAND()
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CONST_INT ciNO_MISSION_BAIL_TIMER 20000
	
	IF NOT g_bBlockIslandNoMissionBail
		BOOL bSwitchingMission = FALSE
		
		// On heist prep missions there is a brief period where the mission flag is invalid so we don't want to kick
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
			IF IS_PLAYER_ON_HEIST_ISLAND(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION_TO_LAUNCH(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = FMMC_TYPE_ISLAND_HEIST_PREP
				bSwitchingMission = TRUE
			ENDIF
		ENDIF
	
		IF NOT bSwitchingMission AND NOT GB_IS_PLAYER_ON_ISLAND_HEIST_PREP_MISSION(PLAYER_ID())
			IF NETWORK_IS_IN_TUTORIAL_SESSION()
				IF NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(PLAYER_ID()) != ciHEIST_ISLAND_BEACH_PARTY_TUTORIAL_INSTANCE_ID
					IF HAS_NET_TIMER_EXPIRED(sInst.sNoMissionBailTimer, ciNO_MISSION_BAIL_TIMER)
						PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_ISLAND_BAILS - Player on island but not at party and no mission is running! leaving island")
						RESET_NET_TIMER(sInst.sNoMissionBailTimer)
						HEIST_ISLAND_TRAVEL__LEAVE_ISLAND()
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ELIF HAS_NET_TIMER_STARTED(sInst.sNoMissionBailTimer)
			RESET_NET_TIMER(sInst.sNoMissionBailTimer)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sInst.sNoMissionBailTimer)
			RESET_NET_TIMER(sInst.sNoMissionBailTimer)
		ENDIF
	ENDIF
	
	// If we are critical to a freemode event we should not be allowed on the island
	IF NOT g_bBlockIslandCriticalEventBail
		IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
			IF NOT GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
				PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_ISLAND_BAILS - Player is critical to event and is needed in los santos, leaving island")
				HEIST_ISLAND_TRAVEL__LEAVE_ISLAND()
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	// We should always be in a tutorial session when on the island so if we are not we may have been in a disconnected state and should bail
	IF NOT g_bBlockInvalidTutorialSessionBail
		IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_ISLAND_BAILS - Player is not in any tutorial session, leaving island")
			HEIST_ISLAND_TRAVEL__LEAVE_ISLAND()
			EXIT
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Maintains supressing the wanted level on the island
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_SUPRESS_WANTED_LEVEL()
	IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains launching the island backup heli if it has been triggered by a netework event
/// PARAMS:
///    sInst - the heit island travel instance
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_LAUNCHING_BACKUP_HELI(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	IF sInst.bLaunchBackupHeli
		PRINTLN("[HEIST_ISLAND_TRAVEL][HELI] MAINTAIN_LAUNCHING_BACKUP_HELI - Loading script...")
		IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(sInst.sHeliLaunch)
			sInst.bLaunchBackupHeli = FALSE
			PRINTLN("[HEIST_ISLAND_TRAVEL][HELI] MAINTAIN_LAUNCHING_BACKUP_HELI - Heli script launched")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Mainatins updating all the island warp locations and maintained transition requests to and from the island
/// PARAMS:
///    sInst - the heist island travel instance
PROC MAINTAIN_HEIST_ISLAND_TRAVEL(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	#IF IS_DEBUG_BUILD
		_HEIST_ISLAND_TRAVEL__MAINTAIN_DEBUG(sInst)
	#ENDIF
	
	_HEIST_ISLAND_TRAVEL__MAINTAIN_WATER_PLANES()
	_HEIST_ISLAND_TRAVEL__MAINTAIN_ISLAND_INVITES(sInst)
	_HEIST_ISLAND_TRAVEL__MAINTAIN_LAUNCHING_BACKUP_HELI(sInst)
	_HEIST_ISLAND_TRAVEL__MAINTAIN_BEACH_PARTY_TRIGGER_UNLOCK(sInst)
	_HEIST_ISLAND_TRAVEL__MAINTAIN_SPAWN_LSIA_SCOPING_MISSION_ARRIVAL_VEHICLE()
	_HEIST_ISLAND_TRAVEL__MAINTAIN_CAGE_SKELETON()
	_HEIST_ISLAND_TRAVEL__MAINTAIN_SUPRESS_WANTED_LEVEL()
	
	sInst.iStaggeredPlayerCheck = IWRAP_INDEX(sInst.iStaggeredPlayerCheck + 1, NUM_NETWORK_PLAYERS)
	
	// Don't run warp transition triggers if a background transition is in progress
	IF NOT HEIST_ISLAND_TRAVEL__IS_TRANSITION_IN_PROGRESS()
		IF _HEIST_ISLAND_TRAVEL__MAINTAIN_WARP_TRIGGERS(sInst)
			// We don't want to process background transitions if a warp location has triggered
			EXIT
		ENDIF
	ENDIF
	
	// Maintain these default transitions so that people can transition with a single function call
	_HEIST_ISLAND_TRAVEL__MAINTAIN_BACKGROUND_WARPS(sInst)
	
	// Need to show a jeep next to the beach warp
	_HEIST_ISLAND_TRAVEL__MAINTAIN_LEAVE_BEACH_VEHICLE_PROP(sInst)
	
	/// Any flags or bail cleanups should be done after we update background warps
	_HEIST_ISLAND_TRAVEL__MAINTAIN_ISLAND_BAILS(sInst)
	_HEIST_ISLAND_TRAVEL__MAINTAIN_FLAG_CLEANUP_FAILSAFES()
ENDPROC

/// PURPOSE:
///    Cleans up all the heist island warp locations and the island warp request transitions
/// PARAMS:
///    sInst - the heist island travel instance
PROC CLEANUP_HEIST_ISLAND_TRAVEL(HEIST_ISLAND_TRAVEL_STRUCT &sInst)
	BOOL bCleanupClothes = FALSE
	
	CLEAN_UP_WARP_TRANSITION_TRIGGER_LOCATION(sInst.sLSIAToBeachPartyTrigger)
	CLEAN_UP_WARP_TRANSITION_TRIGGER_LOCATION(sInst.sBeachPartyToLSIATrigger)
	CLEANUP_WARP_TRANSITION(sInst.sQuickIslandWarp)
	
	IF HEIST_ISLAND_LOADING__ARE_IPLS_ACTIVE()
		IF g_bStopLoadScenesBeforeIslandIPLCleanup
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
				PRINTLN("[HEIST_ISLAND_TRAVEL] CLEANUP - Load scene was active, stopping load scene so we can unload ipls")
				DEBUG_PRINTCALLSTACK()
			ENDIF
		ENDIF
		HEIST_ISLAND_LOADING__UNLOAD_ISLAND()
		
		bCleanupClothes = TRUE
	ENDIF
	
	HEIST_ISLAND_LOADING__ON_UNLOAD_ISLAND(bCleanupClothes)// Always clear flags
	_HEIST_ISLAND_TRAVEL__CLEANUP_LEAVE_BEACH_VEHICLE_PROP(sInst)
	_HEIST_ISLAND_TRAVEL__CLEAR_LAST_TRANSITION()
	_HEIST_ISLAND_TRAVEL__SET_GLOBAL_TRANSITION_STATE(HEIST_ISLAND_TRANSITION_STATE_IDLE)
	g_bDelayShardStart = FALSE
ENDPROC

///------------------------------------------------------------------------
///    Beach Party
///    Split out from heist island travel so it can be run separately
///    from the island script
///------------------------------------------------------------------------    

/// PURPOSE:
///    Gets the centre position of the beach party that will use for distance checks
/// RETURNS:
///    The centre position of the beach party that will use for distance checks
FUNC VECTOR _HEIST_ISLAND_TRAVEL__GET_BEACH_PARTY_RADIUS_ORIGIN()
	RETURN <<4897.607910, -4942.010254, 3.336017>> 
ENDFUNC

/// PURPOSE:
///    Checks if the player is within the radius we consider to be the beach party. If the player is dead it will use their cam coord instead
/// PARAMS:
///    fRadius - ciBEACH_PARTY_RESTRICTION_RADIUS, can be overriden if a custom radius needs to be checked
/// RETURNS:
///    TRUE is the player is in the beach party radius
FUNC BOOL _HEIST_ISLAND_TRAVEL__IS_LOCAL_PLAYER_WITHIN_BEACH_PARTY_RADIUS(FLOAT fRadius = ciBEACH_PARTY_RESTRICTION_RADIUS)
	VECTOR vPartyOrigin = _HEIST_ISLAND_TRAVEL__GET_BEACH_PARTY_RADIUS_ORIGIN()
	VECTOR vPlayerPosition
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ELSE
		// If they just died go off the gameplay camera position instead
		vPlayerPosition = GET_GAMEPLAY_CAM_COORD()
	ENDIF
	
	FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS_SQUARED(vPartyOrigin, vPlayerPosition)
	RETURN fDistance <= fRadius * fRadius
ENDFUNC

/// PURPOSE:
///    Checks if the player is in the given beach party restricted area
/// PARAMS:
///    eArea - the restricted area to check
/// RETURNS:
///    TRUE if the player ped is in the restructed area
FUNC BOOL _HEIST_ISLAND_TRAVEL__IS_PLAYER_IN_RESTRICTED_AREA(HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS eArea)
	PED_INDEX PedIndex = PLAYER_PED_ID()
	
	SWITCH eArea
		CASE HIBPRA_SEA						RETURN IS_ENTITY_IN_ANGLED_AREA(PedIndex, <<4791.022949,-4958.360352,6.490686>>, <<4826.247070,-4896.496094,-5.389570>>, 6.000000)
		CASE HIBPRA_LEFT_GATE				RETURN IS_ENTITY_IN_ANGLED_AREA(PedIndex, << 4949.43, -4873.04, 0.379 >>, << 4955.6, -4844.63, 10.72 >>, 24.000000)
		CASE HIBPRA_RIGHT_GATE				RETURN IS_ENTITY_IN_ANGLED_AREA(PedIndex, << 4956.94, -4917.65, -0.938 >>, << 4977.68, -4934.75, 9.985 >>, 29.500000)
		CASE HIBPRA_OUTSIDE_PARTY_RADIUS 	RETURN NOT _HEIST_ISLAND_TRAVEL__IS_LOCAL_PLAYER_WITHIN_BEACH_PARTY_RADIUS()
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player should be walked back to the beach party area
/// PARAMS:
///    eReturnArea - the restricted area the player was caught in
/// RETURNS:
///    TRUE if the player was in an enabled restricted area and should be walked back to the party
FUNC BOOL _HEIST_ISLAND_TRAVEL__SHOULD_RETURN_PLAYER_TO_PARTY(HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS &eReturnAreaOut)
	INT i
	
	REPEAT HIBPRA_COUNT i
		eReturnAreaOut = INT_TO_ENUM(HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS, i)
		
		IF NOT IS_BEACH_PARTY_RESTRICTED_AREA_ENABLED_FOR_LOCAL_PLAYER(eReturnAreaOut)
			RELOOP
		ENDIF
		
		IF _HEIST_ISLAND_TRAVEL__IS_PLAYER_IN_RESTRICTED_AREA(eReturnAreaOut)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Calculates the players walk to target when being kicked from a restricted area, this is calculated as a vector towards the centre of the party
///    except for the gate restricted areas which have custom points defined
/// PARAMS:
///    eArea - the restricted are to kick the player from
/// RETURNS:
///    The location the player must walk to after being kicked from the given area
FUNC VECTOR _HEIST_ISLAND_TRAVEL__CALCULATE_BEACH_PARTY_KICK_COORD(HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS eArea)
	SWITCH eArea
		//Due to the curved nature of the path to each gate and the chosen locate position, calculating a point between the player and the party returns a vector inside map geometry (url:bugstar:6728437)
		CASE HIBPRA_LEFT_GATE	RETURN <<4946.8164, -4877.6006, 2.8446>>
		CASE HIBPRA_RIGHT_GATE	RETURN <<4953.3071, -4916.2822, 3.7098>>
	ENDSWITCH
	
	VECTOR vPartyOrigin = _HEIST_ISLAND_TRAVEL__GET_BEACH_PARTY_RADIUS_ORIGIN()
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vDirToCentre = vPartyOrigin - vPlayerPos
	VECTOR vKickLocation = vPlayerPos + (NORMALISE_VECTOR(vDirToCentre) * cfBEACH_PARTY_RESTRICTION_RETURN_DISTANCE)
	PRINTLN("[HEIST_ISLAND_TRAVEL] CALCULATE_BEACH_PARTY_KICK_COORD - kick location: ", vKickLocation)
	RETURN vKickLocation
ENDFUNC

/// PURPOSE:
///    Maintains kicking the player from beach party restricted areas if they are enabled. 
///    After enough violations the player will be removed from the island entirely with a transition cutscene
/// PARAMS:
///    sInst - the heist island travel instance
PROC _HEIST_ISLAND_TRAVEL__MAINTAIN_BEACH_RESTRICTION(HEIST_ISLAND_TRAVEL_BEACH_PARTY &sInst)	
	BOOL bSwimBailTimeExpired = HAS_NET_TIMER_STARTED_AND_EXPIRED(sInst.sKickBailTimer, 10000) AND IS_PED_SWIMMING(PLAYER_PED_ID())	
	
	// Return to area complete, run regardless of flag so we don't get stuck halfway through
	IF sInst.bPlayerGettingKickedFromArea 
	AND (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK OR bSwimBailTimeExpired)
		IF bSwimBailTimeExpired
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BEACH_RESTRICTION - Swim bail timer expired, clearing task")
		ENDIF
		
		IF NOT IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID())
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BEACH_RESTRICTION - Player has returned to party, controls enabled")
		ENDIF
		
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BEACH_RESTRICTION - Player has returned to party, getting kicked cleared")
		sInst.bPlayerGettingKickedFromArea = FALSE
		CLEAR_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_BEACH_GUARD_TURNAWAY)
		RESET_NET_TIMER(sInst.sKickBailTimer)
	ENDIF
	
	// Flag not set so reset and leave
	IF NOT IS_LOCAL_PLAYER_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY()	
		sInst.iPlayerKickCount = 0
		RESET_NET_TIMER(sInst.sKickBailTimer)
		EXIT
	ENDIF
	
	// Not out of bounds
	HEIST_ISLAND_BEACH_PARTY_RESTRICTED_AREAS eReturnArea
	IF NOT _HEIST_ISLAND_TRAVEL__SHOULD_RETURN_PLAYER_TO_PARTY(eReturnArea)
		EXIT
	ENDIF
	
	IF g_SpawnData.bPassedOutDrunk
		EXIT
	ENDIF
	
	IF g_sIslandHeistCutsceneData.iBS != 0
		EXIT
	ENDIF
	
	IF IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID())
		EXIT
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
	OR NOT IS_SKYSWOOP_AT_GROUND()
	OR IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
		sInst.iPlayerKickCount = 0
		EXIT
	ENDIF
	
	IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
		sInst.iPlayerKickCount = 0 // Don't want kicked from beach scene to play on mission
		RESET_NET_TIMER(sInst.sKickBailTimer)
	ENDIF
	
	// Start return to area
	IF NOT sInst.bPlayerGettingKickedFromArea
		sInst.iPlayerKickCount++
		
		IF sInst.iPlayerKickCount >= ciBEACH_PARTY_MAX_RESTRICTED_AREA_KICKS_BEFORE_REMOVAL
			sInst.iPlayerKickCount = 0
			
			SET_BIT(g_sIslandHeistCutsceneData.iBS, ISLAND_HEIST_CUTSCENE_BS_RUN_SCENE)
			g_sIslandHeistCutsceneData.eCutscene = ISLAND_HEIST_CUTSCENE_KICKED_FROM_BEACH
			PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BEACH_RESTRICTION - Kicking from party with cutscene")
			EXIT
		ENDIF
		
		FLOAT fReturnRadius = PICK_FLOAT(eReturnArea = HIBPRA_SEA, 2.0, 1.0)
		TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), _HEIST_ISLAND_TRAVEL__CALCULATE_BEACH_PARTY_KICK_COORD(eReturnArea), PEDMOVE_WALK, DEFAULT, fReturnRadius)
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)		
		sInst.bPlayerGettingKickedFromArea = TRUE
		SET_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_BEACH_GUARD_TURNAWAY)
		RESET_NET_TIMER(sInst.sKickBailTimer)
		START_NET_TIMER(sInst.sKickBailTimer)
		IF NOT GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
			PRINT_HELP("BP_RESTRICTION", DEFAULT_HELP_TEXT_TIME)
		ENDIF
		PRINTLN("[HEIST_ISLAND_TRAVEL] MAINTAIN_BEACH_RESTRICTION - Out of bounds returning player to party, kick count: ", sInst.iPlayerKickCount)
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains island travel systems relating to the beach party
///    Restricts player to party area and places leave warp transition jeep
/// PARAMS:
///    sInst - the beach party travel instance
PROC HEIST_ISLAND_TRAVEL_BEACH_PARTY__MAINTAIN(HEIST_ISLAND_TRAVEL_BEACH_PARTY &sInst)
	_HEIST_ISLAND_TRAVEL__MAINTAIN_BEACH_RESTRICTION(sInst)
	// Add any beach party only updates here
ENDPROC

/// PURPOSE:
///    Cleans up the beach party travel ensuring the vehicles are removed and the 
///    restriction to the party is cleaned up
/// PARAMS:
///    sInst -  the beach party travel instance
PROC HEIST_ISLAND_TRAVEL_BEACH_PARTY__CLEANUP(HEIST_ISLAND_TRAVEL_BEACH_PARTY &sInst)
	IF sInst.bPlayerGettingKickedFromArea 
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
		PRINTLN("[HEIST_ISLAND_TRAVEL] HEIST_ISLAND_TRAVEL_BEACH_PARTY__CLEANUP - Restoring Player Control and clearing tasks, was in middle of restricted area kick")
	ENDIF
	
	CLEAR_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_BEACH_GUARD_TURNAWAY)
	sInst.bPlayerGettingKickedFromArea = FALSE
	sInst.iPlayerKickCount = 0
	RESET_NET_TIMER(sInst.sKickBailTimer)
	
	SET_MAX_WANTED_LEVEL(5)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)

ENDPROC
#ENDIF // FEATURE_HEIST_ISLAND
