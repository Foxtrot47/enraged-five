//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        mp_submarine_weapons.sch																			//
// Description: This file implements the submarine's weapon systems          										//
// Written by:  Aaron Baumbach																						//
// Date:  		22/08/2020																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "mp_globals.sch"
USING "script_network.sch"
USING "net_vehicle_drop.sch"
USING "mp_submarine_helm.sch"
USING "mp_submarine_coords.sch"

#IF FEATURE_HEIST_ISLAND

CONST_FLOAT MAX_SPREAD_DIST_OF_SUB_SPAWNS 425.0


FUNC STRING SWS_STRING(SUBMARINE_WARP_STATE State)
	SWITCH State
		CASE SWS_NONE 	RETURN "SWS_NONE"			BREAK
		CASE SWS_START	RETURN "SWS_START"			BREAK
		CASE SWS_WARP	RETURN "SWS_WARP"			BREAK
		CASE SWS_END	RETURN "SWS_END"			BREAK
	ENDSWITCH
	RETURN "UNKNOWN"
ENDFUNC


TWEAK_FLOAT SUB_WARP_BLIP_SCALE 1.5
TWEAK_INT SUB_WARP_BLIP_ALPHA 255
//TWEAK_FLOAT SUB_WARP_MAP_ZOOM_SPEED 1500.0
TWEAK_FLOAT SUB_WARP_MAP_ZOOM_MIN 400.0
TWEAK_FLOAT SUB_WARP_MAP_ZOOM_MAX 2400.0





PROC _SUBMARINE_WARP_SET_STATE(SUBMARINE_WARP_DATA &Data, SUBMARINE_WARP_STATE NewState)
	IF Data.State != NewState
		PRINTLN("[SUB_WARP] _SUBMARINE_WARP_SET_STATE: ", SWS_STRING(Data.State), " -> ", SWS_STRING(NewState))
		Data.State = NewState
		REINIT_NET_TIMER(Data.StateTimer)
	ENDIF
ENDPROC




FUNC VECTOR GetAreaMidPoint(INT iArea)

	SUBMARINE_AREA_WARP_DATA Data
	GET_SUBMARINE_AREA_WARP_POSITIONS(Data, iArea)

	INT i
	INT iCount = 0
	VECTOR vCumPos = <<0,0,0>> // heh... 'cumulative'
	REPEAT NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS i
		IF (VMAG(Data.Slots[i].Coords) > 0.0)
			vCumPos += Data.Slots[i].Coords
			iCount++
		ENDIF
	ENDREPEAT
	
	IF (iCount > 0)
		RETURN (vCumPos / TO_FLOAT(iCount))
	ENDIF

	RETURN <<0,0,0>>
	
ENDFUNC

FUNC INT GetNearestAreaToCoord(VECTOR vCoord, FLOAT fMaxRadius = 0.0)

	INT iNearest = -1
	FLOAT fNearestDist = 9999999999.9
	INT i
	FLOAT fDist
	VECTOR vMid
	FLOAT fMaxDistSquared
	IF (fMaxRadius > 0.0)
		fMaxDistSquared = fMaxRadius * fMaxRadius
	ELSE
		fMaxDistSquared = fNearestDist
	ENDIF
	
	REPEAT NUM_SUBMARINE_PRESET_WARP_AREAS i
		vMid = GetAreaMidPoint(i)
		vMid.z = 0.0
		vCoord.z = 0.0
		fDist = VDIST2(vCoord, vMid)
		
		PRINTLN("[SUB_WARP] GetNearestAreaToCoord fDist ", fDist, " vMid ", vMid, ", vCoord ", vCoord, ", fMaxDistSquared = ", fMaxDistSquared)
		
		IF (fDist < fMaxDistSquared) 
		AND (fDist < fNearestDist)
			iNearest = i
			fNearestDist = fDist
		ENDIF		
	ENDREPEAT

	PRINTLN("[SUB_WARP] GetNearestAreaToCoord returning area ", iNearest, " from coord ", vCoord)
	RETURN iNearest

ENDFUNC

FUNC INT GET_SUBMARINE_CURRENT_AREA()
	PLAYER_INDEX LocalPlayerId = PLAYER_ID()
	VEHICLE_INDEX SubVeh = GET_SUBMARINE_VEHICLE(LocalPlayerId)	
	IF DOES_ENTITY_EXIST(SubVeh)
		RETURN GetNearestAreaToCoord(GET_ENTITY_COORDS(SubVeh, FALSE), MAX_SPREAD_DIST_OF_SUB_SPAWNS + 200.0 )
	ENDIF
	RETURN -1
ENDFUNC

PROC GET_SUBMARINE_CURRENT_POSITION_AND_HEADING(VECTOR &vReturnPos, FLOAT &fReturnHeading)
	PLAYER_INDEX LocalPlayerId = PLAYER_ID()
	VEHICLE_INDEX SubVeh = GET_SUBMARINE_VEHICLE(LocalPlayerId)	
	IF DOES_ENTITY_EXIST(SubVeh)
	AND NOT IS_ENTITY_DEAD(SubVeh)
		vReturnPos = GET_ENTITY_COORDS(SubVeh)
		fReturnHeading = GET_ENTITY_HEADING(SubVeh)
	ENDIF
ENDPROC

FUNC BOOL SUBMARINE_WARP_TO_AREA(SUBMARINE_WARP_DATA &Data, INT iArea)
	PRINTLN("[SUB_WARP] SUBMARINE_WARP_TO - iArea=", iArea)
	
	IF DOES_ENTITY_EXIST(GET_SUBMARINE_VEHICLE(PLAYER_ID()))
		_SUBMARINE_WARP_SET_STATE(Data, SWS_START)
		Data.iTargetArea = iArea
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		VECTOR vStart = GET_ENTITY_COORDS(GET_SUBMARINE_VEHICLE(PLAYER_ID()), FALSE)
		VECTOR vEnd = GetAreaMidPoint(Data.iTargetArea)
		PLAYSTATS_FAST_TRVL(GetNearestAreaToCoord(vStart), vStart.x, vStart.y, vStart.z, Data.iTargetArea, vEnd.x, vEnd.y, vEnd.z, ENUM_TO_INT(FT_TYPE_SUBMARINE))
		PRINTLN("[AM_CASINO_LIMO][PLAYSTATS_FAST_TRVL] START: ",vStart.x, ", ", vStart.y,", ", vStart.z)
		PRINTLN("[AM_CASINO_LIMO][PLAYSTATS_FAST_TRVL] DESTINATION: ",vEnd.x, ", ", vEnd.y,", ", vEnd.z)
		PRINTLN("[AM_CASINO_LIMO][PLAYSTATS_FAST_TRVL] TYPE: ", ENUM_TO_INT(FT_TYPE_SUBMARINE))
		PRINTLN("[AM_CASINO_LIMO][PLAYSTATS_FAST_TRVL] endLoc: ", Data.iTargetArea)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC _SUBMARINE_CLEAR_FAST_TRAVEL_MENU(SUBMARINE_FAST_TRAVEL_MENU &FTMData)
	
	IF NOT FTMData.bIsDiplaying
		EXIT
	ENDIF
	
	PRINTLN("[SUB_WARP] _SUBMARINE_CLEAR_FAST_TRAVEL_MENU")
	
	FTMData.bIsDiplaying = FALSE
	
	IF FTMData.bTriggerPavelDialogue
		FTMData.bTriggerPavelDialogue = FALSE
		CLEAR_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_PAVEL_WHERE_TO_DIALOGUE)
	ENDIF
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	
	
	//===MENU===
	CLEAR_MENU_DATA()
	CLEANUP_MENU_ASSETS()
	
	//===MAP==
	SAFE_REMOVE_BLIP(FTMData.MapSubBlip)
	DISABLE_RADAR_MAP(FALSE)
	g_PlayerBlipsData.bBigMapIsActive = FALSE
	SET_RADAR_ZOOM(0)
	UNLOCK_MINIMAP_ANGLE()
	UNLOCK_MINIMAP_POSITION()
	SET_BIGMAP_ACTIVE(FALSE)
	THEFEED_SHOW()
	REMOVE_MULTIPLAYER_BANK_CASH()
	REMOVE_MULTIPLAYER_WALLET_CASH()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
	g_bForceUpdateActivityBlips = TRUE
	SET_HIDE_ALL_JOB_BLIPS_FOR_YACHT_STATUS(FALSE)
	UPDATE_ALL_SHOP_BLIPS()
	
	
ENDPROC

//Build the Fast Travel menu, returns true when done
FUNC BOOL _SUBMARINE_BUILD_FAST_TRAVEL_MENU(SUBMARINE_FAST_TRAVEL_MENU &FTMData, SUBMARINE_WARP_DATA &Data)

	IF FTMData.bIsDiplaying
		RETURN TRUE
	ENDIF

	PRINTLN("[SUB_WARP] _SUBMARINE_BUILD_FAST_TRAVEL_MENU")
	
	IF NOT LOAD_MENU_ASSETS()
		PRINTLN("[SUB_WARP] Loading menu assets")
		RETURN FALSE
	ENDIF
	
	FTMData.bIsDiplaying = TRUE
	FTMData.fMapZoom = SUB_WARP_MAP_ZOOM_MAX
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
	

	
	//===MENU===
	CLEAR_MENU_DATA()
	SET_MENU_TITLE("SUB_MOVE_T")
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)	
	
	INT iArea
	REPEAT (NUM_SUBMARINE_PRESET_WARP_AREAS) iArea
		
		ADD_MENU_ITEM_TEXT(iArea, Data.Areas[iArea].Name)
						
		IF GET_SUBMARINE_CURRENT_AREA() = iArea
			ADD_MENU_ITEM_TEXT(iArea, "", 1)
			ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_YACHT)
		ELSE
			IF (Data.Areas[iArea].Cost = 0)
				ADD_MENU_ITEM_TEXT(iArea, "PIM_DFREE", 0)
			ELSE
				ADD_MENU_ITEM_TEXT(iArea, "ITEM_COST", 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_INT(Data.Areas[iArea].Cost)
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	SET_CURRENT_MENU_ITEM(FTMData.iCurrentItem)
	
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
	ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_FRONTEND_TRIGGERS, "IB_ZOOM")
	
	FTMData.bConfirmedMove = FALSE
	
	IF NOT FTMData.bTriggerPavelDialogue
		FTMData.bTriggerPavelDialogue = TRUE
		SET_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_PAVEL_WHERE_TO_DIALOGUE)
	ENDIF
	
//	//===MAP===
//	THEFEED_HIDE()
//	SET_MULTIPLAYER_BANK_CASH()
//	SET_MULTIPLAYER_WALLET_CASH()	
//	DISPLAY_RADAR(TRUE)
//	DISABLE_RADAR_MAP(TRUE)
//	SET_BIGMAP_ACTIVE(TRUE, FALSE)
//	g_PlayerBlipsData.bBigMapIsActive = TRUE
	
	RETURN TRUE
	
ENDFUNC



PROC _SUBMARINE_WARP_INIT_AREAS(SUBMARINE_WARP_DATA& Data)
	
	INT i
	
	// cost
	REPEAT NUM_SUBMARINE_PRESET_WARP_AREAS i
		IF HAS_LOCAL_PLAYER_COMPLETED_ISLAND_HEIST_AS_LEADER(TRUE)
			Data.Areas[i].Cost = g_sMPTunables.iIH_SUBMARINE_FAST_TRAVEL_UNLOCKED // 2000
		ELSE
			Data.Areas[i].Cost = g_sMPTunables.iIH_SUBMARINE_FAST_TRAVEL_DEFAULT // 10000
		ENDIF
	ENDREPEAT

	
	// name
	Data.Areas[0].Name = "SUB_FT_0"
	Data.Areas[1].Name = "SUB_FT_1"
	Data.Areas[2].Name = "SUB_FT_2"
	Data.Areas[3].Name = "SUB_FT_3"
	Data.Areas[4].Name = "SUB_FT_4"
	Data.Areas[5].Name = "SUB_FT_5"
	Data.Areas[6].Name = "SUB_FT_6"
	Data.Areas[7].Name = "SUB_FT_7"
	Data.Areas[8].Name = "SUB_FT_8"
	Data.Areas[9].Name = "SUB_FT_9"
	Data.Areas[10].Name = "SUB_FT_10"
	Data.Areas[11].Name = "SUB_FT_11"
	
	

ENDPROC


PROC _SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU(SUBMARINE_FAST_TRAVEL_MENU &FTMData, SUBMARINE_WARP_DATA &Data)

	// Update transaction state
	IF USE_SERVER_TRANSACTIONS()
		IF Data.iCashTransactionState = SUB_MOVE_TRANSACTION_FAILED
			DELETE_CASH_TRANSACTION(Data.iCashTransactionId)
			Data.iCashTransactionState = SUB_MOVE_TRANSACTION_INVALID
		ENDIF
		
		IF Data.iCashTransactionState != SUB_MOVE_TRANSACTION_INVALID
			IF IS_CASH_TRANSACTION_COMPLETE(Data.iCashTransactionId)
				IF GET_CASH_TRANSACTION_STATUS(Data.iCashTransactionId) = CASH_TRANSACTION_STATUS_SUCCESS
					Data.iCashTransactionState = SUB_MOVE_TRANSACTION_SUCCESS
				ELSE
					Data.iCashTransactionState = SUB_MOVE_TRANSACTION_FAILED
					DELETE_CASH_TRANSACTION(Data.iCashTransactionId)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Handle Input
	IF FTMData.bIsDiplaying
	AND Data.State = SWS_NONE
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	AND NOT g_sShopSettings.bProcessStoreAlert		
	AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
	
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
		ENDIF
		
		// Grab the current analogue stick positions
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
			
		// Set the input flag states
		BOOL bAccept		 = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
		BOOL bBack			 = (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
		BOOL bPrevious	     = ((iLeftY < -64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)) 
		BOOL bNext		     = ((iLeftY > 64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))
		
		IF IS_PC_VERSION()
			IF IS_USING_CURSOR(FRONTEND_CONTROL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)					
				HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
				HANDLE_MENU_CURSOR(FALSE)
			ENDIF
			
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
				bAccept = TRUE										
			ELIF IS_MENU_CURSOR_CANCEL_PRESSED()
				bBack = TRUE
			ELIF IS_MENU_CURSOR_SCROLL_UP_PRESSED()
				bPrevious = TRUE
			ELIF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
				bNext = TRUE
			ENDIF
		ENDIF			
		
		IF (bAccept
			AND (FTMData.iCurrentItem > -1)
			AND (FTMData.iCurrentItem < NUM_SUBMARINE_PRESET_WARP_AREAS))
		OR (Data.iCashTransactionState != SUB_MOVE_TRANSACTION_INVALID)
		
			//TODO: Validation
			//1) Is near area already?
			//2) Is there a slot free in this area?
			//3) Blocked by tunable
			//4) Cannot afford
			
			// Transaction checks
			IF Data.iCashTransactionState = SUB_MOVE_TRANSACTION_PENDING
				// wait
				PRINTLN("_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU - waiting for transaction")
				
			ELIF Data.iCashTransactionState = SUB_MOVE_TRANSACTION_FAILED
				SET_CURRENT_MENU_ITEM_DESCRIPTION("SUB_FAIL_2", 4000) // There was an error processing this transaction.
				PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				FTMData.bConfirmedMove = FALSE
				PRINTLN("_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU - an error processing this transaction")
						
			// are we at that location already?
			ELIF (GET_SUBMARINE_CURRENT_AREA() = FTMData.iCurrentItem)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("SUB_FAIL_0", 4000) // Your Yacht is already located here.
				PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				FTMData.bConfirmedMove = FALSE
				PRINTLN("_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU - Yacht is already located here")
				
			// is blocked?
			ELIF (MPGlobalsAmbience.bSubFastTravelAreaDisabled[FTMData.iCurrentItem])
				SET_CURRENT_MENU_ITEM_DESCRIPTION("SUB_FAIL_4", 4000) // cant move here.
				PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				FTMData.bConfirmedMove = FALSE
				PRINTLN("_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU - blocked")
				
			// Can't afford
			ELIF (NOT CAN_PLAYER_AFFORD_ITEM_COST(Data.Areas[FTMData.iCurrentItem].Cost))
				SET_CURRENT_MENU_ITEM_DESCRIPTION("SUB_FAIL_3", 4000) // You can't afford to move your Yacht here.
				PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				FTMData.bConfirmedMove = FALSE
				PRINTLN("_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU - cant afford")
				
			// confirm
			ELIF (FTMData.bConfirmedMove = FALSE)
				
				// are you sure?
				SET_CURRENT_MENU_ITEM_DESCRIPTION("SUB_MOVE_CONF")
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(Data.Areas[FTMData.iCurrentItem].Name)
				PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				FTMData.bConfirmedMove = TRUE
				PRINTLN("_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU - set confirm")
				
			ELSE

				IF (Data.Areas[FTMData.iCurrentItem].Cost = 0)
					PRINTLN("_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU - free move")
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SUB_PASS_0", 4000) // Destination confirmed.
					SUBMARINE_WARP_TO_AREA(Data, FTMData.iCurrentItem)
				ELSE
					IF USE_SERVER_TRANSACTIONS()
						IF Data.iCashTransactionState = SUB_MOVE_TRANSACTION_INVALID
							PRINTLN("_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU - Requesting cash transaction")
							IF NETWORK_REQUEST_CASH_TRANSACTION(Data.iCashTransactionState, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_SPEND, CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_SPEND_SUBMARINE_RELOCATION, Data.Areas[FTMData.iCurrentItem].Cost, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
								PRINTLN("_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU - Cash transaction started")
								Data.iCashTransactionState = SUB_MOVE_TRANSACTION_PENDING
								PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							ELSE
								PRINTLN("_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU - Cash transaction failed")
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SUB_FAIL_2", 4000) // There was an error processing this transaction.
								PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							ENDIF
						ELSE
							PRINTLN("MAINTAIN_YACHT_MOVE - Cash transaction complete")
							NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(Data.iCashTransactionId))
							NETWORK_SPENT_MOVE_SUBMARINE(Data.Areas[FTMData.iCurrentItem].Cost, FALSE, TRUE)
							DELETE_CASH_TRANSACTION(Data.iCashTransactionId)
							Data.iCashTransactionState = SUB_MOVE_TRANSACTION_INVALID
							SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_PASS_0", 4000) // Destination confirmed.
							SUBMARINE_WARP_TO_AREA(Data, FTMData.iCurrentItem)
						ENDIF
					ELSE
						NETWORK_SPENT_MOVE_SUBMARINE(Data.Areas[FTMData.iCurrentItem].Cost, FALSE, TRUE)
						PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						SET_CURRENT_MENU_ITEM_DESCRIPTION("YACHT_PASS_0", 4000) // Destination confirmed.
						SUBMARINE_WARP_TO_AREA(Data, FTMData.iCurrentItem)
					ENDIF	
				ENDIF
					
				
				//There's a state desync so sync first
				IF g_iMenuCursorItem = FTMData.iCurrentItem
					FTMData.iCurrentItem = g_iMenuCursorItem
					SET_CURRENT_MENU_ITEM(FTMData.iCurrentItem)
				ENDIF
			
			ENDIF

			
		ELIF bBack		
		
			IF (FTMData.bConfirmedMove)
				FTMData.bConfirmedMove = FALSE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("")
			ELSE
				FTMData.bWantsToDisplay = FALSE
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FREEMODE_SOUNDSET")
			
		ELIF bPrevious OR bNext
		
			IF NOT (FTMData.bConfirmedMove)
				INT iInputMove
				IF ALLOW_ANALOGUE_MOVEMENT(FTMData.timeScrollDelay,iInputMove)
				
					INT iNewIndex = IWRAP_INDEX(FTMData.iCurrentItem - iInputMove, NUM_SUBMARINE_PRESET_WARP_AREAS)
					
					IF iNewIndex != FTMData.iCurrentItem
						FTMData.iCurrentItem = iNewIndex
						SET_CURRENT_MENU_ITEM(iNewIndex)
						PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					ENDIF
				
				ENDIF
			ENDIF
			
		ENDIF
		
		FLOAT fZoomDelta = 0
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LT)
			//fZoomDelta = SUB_WARP_MAP_ZOOM_SPEED *  TIMESTEP()
			fZoomDelta = 50
		ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RT)
			//fZoomDelta = -SUB_WARP_MAP_ZOOM_SPEED *  TIMESTEP()
			fZoomDelta = -50
		ENDIF
		FTMData.fMapZoom = CLAMP(FTMData.fMapZoom + fZoomDelta, SUB_WARP_MAP_ZOOM_MIN, SUB_WARP_MAP_ZOOM_MAX)
					
		
	ENDIF
	
	
	//Build/Clear Menu
	IF Data.State = SWS_NONE
		IF FTMData.bWantsToDisplay
			_SUBMARINE_BUILD_FAST_TRAVEL_MENU(FTMData, Data)
		ELSE
			_SUBMARINE_CLEAR_FAST_TRAVEL_MENU(FTMData)
		ENDIF
	ENDIF
	
	//Draw Menu
	IF FTMData.bIsDiplaying

		//===MAP===
		SUBMARINE_AREA_WARP_DATA AreaWarpData
		GET_SUBMARINE_AREA_WARP_POSITIONS(AreaWarpData, FTMData.iCurrentItem)
		SUBMARINE_WARP_AREA_SLOT_DATA SelectedSlot = AreaWarpData.Slots[0]
		
		// if we are looking at our current area, then align blip to our current positions
		IF (FTMData.iCurrentItem = GET_SUBMARINE_CURRENT_AREA())
			GET_SUBMARINE_CURRENT_POSITION_AND_HEADING(SelectedSlot.Coords, SelectedSlot.Heading)
			PRINTLN("_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU Aligning sub blip location to player, at ", SelectedSlot.Coords)
		ENDIF	
	
		DRAW_MENU()
		SET_MULTIPLAYER_BANK_CASH()
		SET_MULTIPLAYER_WALLET_CASH()
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
		THEFEED_HIDE()
		DISPLAY_RADAR(TRUE)
		SET_BIGMAP_ACTIVE(TRUE, FALSE)
		SET_RADAR_ZOOM_TO_DISTANCE(FTMData.fMapZoom)
		SET_RADAR_AS_EXTERIOR_THIS_FRAME()
		LOCK_MINIMAP_ANGLE(0)
		LOCK_MINIMAP_POSITION(SelectedSlot.Coords.x, SelectedSlot.Coords.y)
		DISABLE_RADAR_MAP(TRUE)
		g_PlayerBlipsData.bBigMapIsActive = TRUE
		

		IF NOT DOES_BLIP_EXIST(FTMData.MapSubBlip)
			FTMData.MapSubBlip = CREATE_BLIP_FOR_COORD(SelectedSlot.Coords)
		ENDIF
		
		SET_BLIP_SPRITE(FTMData.MapSubBlip, RADAR_TRACE_SUB2)
		SET_BLIP_DISPLAY(FTMData.MapSubBlip, DISPLAY_MINIMAP_OR_BIGMAP)
		SET_BLIP_SCALE(FTMData.MapSubBlip, SUB_WARP_BLIP_SCALE)
		SET_BLIP_ALPHA(FTMData.MapSubBlip, SUB_WARP_BLIP_ALPHA)		
		SET_BLIP_COORDS(FTMData.MapSubBlip, SelectedSlot.Coords)
		SET_BLIP_ROTATION_WITH_FLOAT(FTMData.MapSubBlip, SelectedSlot.Heading)
		
	ENDIF


ENDPROC

PROC UPDATE_LIGHTING_FOR_CAMERA(SUBMARINE_CUTSCENE_DATA &Data)

	INT i
	VECTOR vCamPos
	VECTOR vCamRot
	VECTOR vLightPos
	VECTOR vLightDirection
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	#ENDIF
	
	IF DOES_CAM_EXIST(Data.CamId)	
	
		vCamPos = GET_CAM_COORD(Data.CamId)
		vCamRot = GET_CAM_ROT(Data.CamId)
		
		#IF IS_DEBUG_BUILD
		IF (Data.bRenderLights)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)									
			DRAW_DEBUG_CROSS(vCamPos, 0.5)					
		ENDIF
		#ENDIF		
	
		REPEAT NUM_SUB_CUTSCENE_LIGHTS i
			IF (Data.SubLight[i].bActive)
			
				// get light position and directions in world coords
				vLightPos = Data.SubLight[i].vOffset
				RotateVec(vLightPos, vCamRot)
				vLightPos += vCamPos 
				
				vLightDirection = Data.SubLight[i].vDirection
				RotateVec(vLightDirection, vCamRot)
				
				#IF IS_DEBUG_BUILD
				IF (Data.bRenderLights)				
					str = "Light "
					str += i
					DRAW_DEBUG_TEXT(str, vLightPos, 200, 25, 100, 255)
					DRAW_DEBUG_SPHERE(vLightPos, 0.1, Data.SubLight[i].r, Data.SubLight[i].g, Data.SubLight[i].b, 255)	
					
					IF (Data.SubLight[i].iType > 1)
						DRAW_DEBUG_LINE(vLightPos, vLightPos+(vLightDirection * 0.4), Data.SubLight[i].r, Data.SubLight[i].g, Data.SubLight[i].b)					
					ENDIF
				ENDIF
				#ENDIF
			
				SWITCH Data.SubLight[i].iType
					CASE 0
						DRAW_LIGHT_WITH_RANGE(vLightPos, Data.SubLight[i].r, Data.SubLight[i].g, Data.SubLight[i].b, Data.SubLight[i].Range, Data.SubLight[i].Intensity)
					BREAK
					CASE 1
						DRAW_LIGHT_WITH_RANGEEX(vLightPos, Data.SubLight[i].r, Data.SubLight[i].g, Data.SubLight[i].b, Data.SubLight[i].Range, Data.SubLight[i].Intensity, Data.SubLight[i].Falloff)
					BREAK
					CASE 2
						DRAW_SPOT_LIGHT(vLightPos, Data.SubLight[i].vDirection, Data.SubLight[i].r, Data.SubLight[i].g, Data.SubLight[i].b, Data.SubLight[i].Falloff, Data.SubLight[i].Intensity, Data.SubLight[i].InnerAngle, Data.SubLight[i].OuterAngle, Data.SubLight[i].Exp)
					BREAK
					CASE 3
						DRAW_SHADOWED_SPOT_LIGHT(vLightPos, Data.SubLight[i].vDirection, Data.SubLight[i].r, Data.SubLight[i].g, Data.SubLight[i].b, Data.SubLight[i].Falloff, Data.SubLight[i].Intensity, Data.SubLight[i].InnerAngle, Data.SubLight[i].OuterAngle, Data.SubLight[i].Exp, i)
					BREAK
				ENDSWITCH
			ENDIF
		ENDREPEAT	
	ENDIF

ENDPROC

PROC INIT_CUTSCENE_DATA(SUBMARINE_CUTSCENE_DATA &Data)

	IF NOT (Data.bInitialised)
		
		// set lighting
Data.SubLight[0].bActive = TRUE
Data.SubLight[0].iType = 2
Data.SubLight[0].vOffset = <<-13.8075, -12.4000, 37.2000>>
Data.SubLight[0].vDirection = <<0.0000, -65.4000, -0.7350>>
Data.SubLight[0].r = 64
Data.SubLight[0].g = 163
Data.SubLight[0].b = 255
Data.SubLight[0].Range = 1209.9879
Data.SubLight[0].Intensity = 25.0000
Data.SubLight[0].Falloff = 150.0000
Data.SubLight[0].InnerAngle = 87.0000
Data.SubLight[0].OuterAngle = 208.0800
Data.SubLight[0].Exp = 3589.9641

Data.SubLight[1].bActive = TRUE
Data.SubLight[1].iType = 2
Data.SubLight[1].vOffset = <<19.6880, 48.6280, 7.6975>>
Data.SubLight[1].vDirection = <<47.3500, 37.6000, -0.7350>>
Data.SubLight[1].r = 64
Data.SubLight[1].g = 163
Data.SubLight[1].b = 255
Data.SubLight[1].Range = 0.0000
Data.SubLight[1].Intensity = 0.4000
Data.SubLight[1].Falloff = 100.0000
Data.SubLight[1].InnerAngle = 0.0000
Data.SubLight[1].OuterAngle = 150.0000
Data.SubLight[1].Exp = 0.0000

Data.SubLight[2].bActive = TRUE
Data.SubLight[2].iType = 2
Data.SubLight[2].vOffset = <<34.2000, 64.4000, 51.0000>>
Data.SubLight[2].vDirection = <<-57.0000, 3.4000, -100.0000>>
Data.SubLight[2].r = 64
Data.SubLight[2].g = 163
Data.SubLight[2].b = 255
Data.SubLight[2].Range = 0.0000
Data.SubLight[2].Intensity = 25.0000
Data.SubLight[2].Falloff = 100.0000
Data.SubLight[2].InnerAngle = 0.0000
Data.SubLight[2].OuterAngle = 180.0000
Data.SubLight[2].Exp = 20.0000

Data.SubLight[3].bActive = TRUE
Data.SubLight[3].iType = 2
Data.SubLight[3].vOffset = <<17.4000, -43.0000, 51.0000>>
Data.SubLight[3].vDirection = <<-20.2000, 3.4000, 6.0000>>
Data.SubLight[3].r = 64
Data.SubLight[3].g = 163
Data.SubLight[3].b = 255
Data.SubLight[3].Range = 0.0000
Data.SubLight[3].Intensity = 150.0000
Data.SubLight[3].Falloff = 100.0000
Data.SubLight[3].InnerAngle = 0.0000
Data.SubLight[3].OuterAngle = 150.0000
Data.SubLight[3].Exp = 20.0000
		
		// set ptfx
		Data.PtFx[0].Id = INT_TO_NATIVE(PTFX_ID, 0)
		Data.PtFx[0].vOffset = <<0.0000, 0.0000, 0.0000>>
		Data.PtFx[0].vRotation = <<0.0000, 0.0000, 0.0000>>		
		Data.PtFx[0].strName = "scr_ih_sub_propeller"
		Data.PtFx[0].strBoneName = "prop_2_slow"
		Data.PtFx[0].iSceneId = -1
		Data.PtFx[0].bLooped = TRUE
		
		Data.PtFx[1].Id = INT_TO_NATIVE(PTFX_ID, 0)
		Data.PtFx[1].vOffset = <<0.0000, 0.0000, 0.0000>>
		Data.PtFx[1].vRotation = <<0.0000, 0.0000, 0.0000>>
		Data.PtFx[1].strName = "scr_ih_sub_propeller"
		Data.PtFx[1].strBoneName = "prop_1_fast"
		Data.PtFx[1].iSceneId = -1
		Data.PtFx[1].bLooped = TRUE
		
		Data.PtFx[2].Id = INT_TO_NATIVE(PTFX_ID, 0)
		Data.PtFx[2].vOffset = <<0.0000, 0.0000, 0.0000>>
		Data.PtFx[2].vRotation = <<0.0000, 0.0000, 0.0000>>
		Data.PtFx[2].strName = "scr_ih_sub_surface"		
		Data.PtFx[2].strBoneName = "headlight_r"
		Data.PtFx[2].iSceneId = 1 // arriving only
		Data.PtFx[2].bLooped = FALSE
		
		Data.bInitialised = TRUE
	ENDIF

ENDPROC

PROC UPDATE_SUB_PARTICLE_FX(SUBMARINE_CUTSCENE_DATA &Data, STRING strPtFxAsset, BOOL bIsDeparting)

	INT i
	
	#IF IS_DEBUG_BUILD
	VECTOR vPos
	TEXT_LABEL_63 str
	#ENDIF
	
	IF DOES_ENTITY_EXIST(Data.CloneId)
	AND NOT IS_ENTITY_DEAD(Data.CloneId)
		REPEAT NUM_SUB_PTFX i
		
			IF (Data.PtFx[i].iSceneId = -1)
			OR ((Data.PtFx[i].iSceneId = 0) AND (bIsDeparting))
			OR ((Data.PtFx[i].iSceneId = 1) AND (bIsDeparting = FALSE))
			
			
				#IF IS_DEBUG_BUILD
					IF (Data.bRenderPtFx)
						vPos = GET_ENTITY_BONE_POSTION(Data.CloneId, GET_ENTITY_BONE_INDEX_BY_NAME(Data.CloneId, Data.PtFx[i].strBoneName))
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						str = Data.PtFx[i].strName
						DRAW_DEBUG_TEXT(str, vPos, 200, 25, 100, 255)
						DRAW_DEBUG_SPHERE(vPos, 0.1)	
					ENDIF
				#ENDIF
			
				
			
				IF (Data.PtFx[i].Id = INT_TO_NATIVE(PTFX_ID, 0))
					USE_PARTICLE_FX_ASSET(strPtFxAsset)
					IF (Data.PtFx[i].bLooped)
						Data.PtFx[i].Id = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(Data.PtFx[i].strName, Data.CloneId, Data.PtFx[i].vOffset, Data.PtFx[i].vRotation, GET_ENTITY_BONE_INDEX_BY_NAME(Data.CloneId, Data.PtFx[i].strBoneName)) 
					ELSE
						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY_BONE(Data.PtFx[i].strName, Data.CloneId, Data.PtFx[i].vOffset, Data.PtFx[i].vRotation, GET_ENTITY_BONE_INDEX_BY_NAME(Data.CloneId, Data.PtFx[i].strBoneName)) 					
						Data.PtFx[i].Id = INT_TO_NATIVE(PTFX_ID, 1)
					ENDIF
					PRINTLN("[SUB_WARP] UPDATE_SUB_PARTICLE_FX - starting ptfx ", i)	
				ELSE
					#IF IS_DEBUG_BUILD
						IF (Data.PtFx[i].bRecreate)
							IF DOES_PARTICLE_FX_LOOPED_EXIST(Data.PtFx[i].Id)
								STOP_PARTICLE_FX_LOOPED(Data.PtFx[i].Id)								
								PRINTLN("[SUB_WARP] UPDATE_SUB_PARTICLE_FX - stopping ptfx ", i)
							ENDIF
							Data.PtFx[i].Id = INT_TO_NATIVE(PTFX_ID, 0)
							Data.PtFx[i].bRecreate = FALSE
						ENDIF
					#ENDIF
				ENDIF
				
			ENDIF
		ENDREPEAT
	ENDIF

ENDPROC

PROC CLEANUP_SUBMARINE_CUTSCENE(SUBMARINE_CUTSCENE_DATA &Data, STRING strAnimDict, STRING strPtFxAsset, STRING strAudioScene)

	SET_MODEL_AS_NO_LONGER_NEEDED(KOSATKA)
	REMOVE_ANIM_DICT(strAnimDict)
	REMOVE_NAMED_PTFX_ASSET(strPtFxAsset)
	STOP_STREAM()

	IF IS_AUDIO_SCENE_ACTIVE(strAudioScene)
		STOP_AUDIO_SCENE(strAudioScene)	
	ENDIF
	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP() 
	ENDIF	
			
	IF Data.iCalmId != -1
		REMOVE_EXTRA_CALMING_QUAD(Data.iCalmId)
		SET_CALMED_WAVE_HEIGHT_SCALER(1)
		PRINTLN("[SUB_WARP][DEPART_SCENE] REMOVE_EXTRA_CALMING_QUAD maintain ID #",Data.iCalmId)
		Data.iCalmId = -1
	ENDIF
	
	CLEAR_FOCUS()
	IF DOES_ENTITY_EXIST(Data.CloneId)
		DELETE_VEHICLE(Data.CloneId)
	ENDIF
	IF DOES_CAM_EXIST(Data.CamId)
		DESTROY_CAM(Data.CamId)
	ENDIF
	CLEANUP_MP_CUTSCENE(DEFAULT, FALSE, TRUE)

	RENDER_SCRIPT_CAMS(FALSE, FALSE, 0)	
	DESTROY_ALL_CAMS(TRUE)
	
	INT i
	REPEAT NUM_SUB_PTFX i
		IF  DOES_PARTICLE_FX_LOOPED_EXIST(Data.PtFx[i].Id)
			STOP_PARTICLE_FX_LOOPED(Data.PtFx[i].Id)
			PRINTLN("[SUB_WARP] CLEANUP_SUBMARINE_CUTSCENE - stopping ptfx ", i)
		ENDIF	
		Data.PtFx[i].Id = INT_TO_NATIVE(PTFX_ID, 0)
	ENDREPEAT
	
	//DO_SCREEN_FADE_IN(500)
	Data.iState = 0
	Data.iSceneId = 0
ENDPROC

PROC GetNearestStartSafeStartCoordsForSubCutscene(VECTOR vInCoords, FLOAT fInHeading, VECTOR &vReturnCoords, FLOAT &fReturnHeading)

	// first get nearest area to coord:
	INT iNearestArea = GetNearestAreaToCoord(vInCoords)
	
	FLOAT fNearestDist = 99999999999.9
	INT iNearestPoint = -1
	
	// now go through each of the points in that location and choose the nearest one
	SUBMARINE_AREA_WARP_DATA WarpData
	GET_SUBMARINE_AREA_WARP_POSITIONS(WarpData, iNearestArea)
	
	INT i
	FLOAT fThisDist2
	REPEAT NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS i
		fThisDist2 = VDIST2(WarpData.Slots[i].Coords, vInCoords)
		IF (fThisDist2 < fNearestDist)
			fNearestDist = fThisDist2
			iNearestPoint = i
		ENDIF
	ENDREPEAT
	
	IF (iNearestPoint > -1)
		vReturnCoords =  WarpData.Slots[iNearestPoint].Coords
		fReturnHeading = WarpData.Slots[iNearestPoint].Heading
		
		PRINTLN("GetNearestStartSafeStartCoordsForSubCutscene - returning nearest point ", iNearestPoint, " in area ", iNearestArea, " at ", vReturnCoords)
	ELSE
		vReturnCoords =  vInCoords
		fReturnHeading = fInHeading	
		
		PRINTLN("GetNearestStartSafeStartCoordsForSubCutscene - returning original coords ", vReturnCoords)
	ENDIF	
	
ENDPROC

FUNC BOOL SUBMARINE_CUTSCENE(SUBMARINE_CUTSCENE_DATA &Data, VEHICLE_INDEX vehSub, VECTOR vCoords, FLOAT fHeading, BOOL bDeparting)

	STRING strAnimDict = "ANIM@SCRIPTED@SUBMARINE@SUB_FAST_TRAVEL@"
	STRING strAnimNameEntity
	STRING strAnimNameCamera
	
	STRING strPtFxAsset = "scr_ih_sub"

	STRING strStreamSet = "DLC_H4_Anims_Submarine_Fast_Travel"
	STRING strStreamName
	
	STRING strAudioScene = "DLC_H4_Sub_Fast_Travel_Mix_Scene"
	
	IF (bDeparting)
		strAnimNameEntity = "SUB_FAST_TRAVEL_START"
		strAnimNameCamera = "SUB_FAST_TRAVEL_START_CAM"
		strStreamName = "Start"
	ELSE
		strAnimNameEntity = "SUB_FAST_TRAVEL_END"
		strAnimNameCamera = "SUB_FAST_TRAVEL_END_CAM"
		strStreamName = "Stop"
	ENDIF

	SWITCH Data.iState
	
		// trigger
		CASE 0						
			PRINTLN("[SUB_WARP][DEPART_SCENE] launching...")
			
			INIT_CUTSCENE_DATA(Data)
			
			IF DOES_ENTITY_EXIST(vehSub)
			AND NOT IS_ENTITY_DEAD(vehSub)

			ELSE
				PRINTLN("[SUB_WARP][DEPART_SCENE] sub is dead")	
			ENDIF
			
//			IF (VMAG(Data.vWaterOffset) = 0.0)
//				Data.vWaterOffset = <<0.0, 20.0, 3.0>>
//			ENDIF
			
			Data.iState++	
		BREAK
	
		// request and setup for cutscene
		CASE 1
		
			// make sure we are on a black screen
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(0)
			ENDIF
			
			// store data about the sub
			IF DOES_ENTITY_EXIST(vehSub)
			AND NOT IS_ENTITY_DEAD(vehSub)
				GET_VEHICLE_SETUP_MP(vehSub, Data.vehicleSetup)
			ENDIF
			
			IF (bDeparting)
				GetNearestStartSafeStartCoordsForSubCutscene(vCoords, fHeading, vCoords, fHeading)
			ENDIF
			
			// store the location, but we might need to use a safe fixed location nearby
			Data.vLocation = vCoords				
			Data.vLocation.z = Data.fZHeight				
			Data.fHeading = fHeading	
			
			PRINTLN("[SUB_WARP][DEPART_SCENE] - Data.vLocation = ", Data.vLocation, ", heading = ", Data.fHeading)
			
			START_MP_CUTSCENE(TRUE) 

			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION)
			
			Data.iCalmId = ADD_EXTRA_CALMING_QUAD(Data.vLocation.x - Data.fCalmingQuadSize, Data.vLocation.y - Data.fCalmingQuadSize, Data.vLocation.x + Data.fCalmingQuadSize, Data.vLocation.y + Data.fCalmingQuadSize, Data.fScalarWaveValue)
			SET_CALMED_WAVE_HEIGHT_SCALER(Data.fScalarWaveValue)			
			PRINTLN("[SUB_WARP][DEPART_SCENE] setting calming quad, wave value = ", Data.fScalarWaveValue)

			DESTROY_ALL_CAMS(TRUE)			
			IF DOES_CAM_EXIST(Data.CamId)
				DESTROY_CAM(Data.CamId)
			ENDIF
			
			REQUEST_MODEL(KOSATKA)
			REQUEST_ANIM_DICT(strAnimDict)
			REQUEST_NAMED_PTFX_ASSET(strPtFxAsset)
			
			Data.iState++	
		BREAK
		
		// wait for stuff to load, then create sub and start camera
		CASE 2
			IF LOAD_STREAM(strStreamName, strStreamSet)
			AND HAS_MODEL_LOADED(KOSATKA)
			AND HAS_ANIM_DICT_LOADED(strAnimDict)
			AND HAS_NAMED_PTFX_ASSET_LOADED(strPtFxAsset)
			
				// create a clone of the submarine for use in the cutscene
				Data.CloneId = CREATE_VEHICLE(KOSATKA, Data.vLocation, Data.fHeading, FALSE, FALSE, FALSE)	
				SET_ENTITY_COORDS_NO_OFFSET(Data.CloneId, Data.vLocation)
				SET_ENTITY_VISIBLE(Data.CloneId, TRUE)
				SET_ENTITY_DYNAMIC(Data.CloneId, TRUE)
				
			

				IF GET_NUM_MOD_KITS(Data.CloneId) > 0
					SET_VEHICLE_MOD_KIT(Data.CloneId, 0)
					//SET_VEHICLE_MOD_KIT(Data.CloneId2, 0)
				ENDIF
				
				SET_SUBMARINE_PROPERTY_COLOURS(Data.CloneId, Data.iColour)
				SET_SUBMARINE_PROPERTY_FLAG(Data.CloneId, Data.iFlag)
				//SET_SUBMARINE_PROPERTY_COLOURS(Data.CloneId2, Data.iColour)
				//SET_SUBMARINE_PROPERTY_FLAG(Data.CloneId2, Data.iFlag)
				
				SET_ENTITY_INVINCIBLE(Data.CloneId, TRUE)
				//SET_ENTITY_INVINCIBLE(Data.CloneId2, TRUE)
				
				SET_VEHICLE_LIGHTS(Data.CloneId, FORCE_VEHICLE_LIGHTS_OFF)
				//SET_VEHICLE_LIGHTS(Data.CloneId2, FORCE_VEHICLE_LIGHTS_OFF)
	
				PRINTLN("[SUB_WARP][DEPART_SCENE] created clone")
				
				SET_FOCUS_POS_AND_VEL(Data.vLocation, <<0,0,0>>)
				NEW_LOAD_SCENE_START_SPHERE(Data.vLocation, 200.0)
				
				Data.iState++
				Data.iGameTimer = GET_GAME_TIMER()
				
			ELSE
				PRINTLN("[SUB_WARP][DEPART_SCENE] waiting for load")
			ENDIF
		BREAK
	
		// wait for load scene
		CASE 3
			IF (IS_NEW_LOAD_SCENE_ACTIVE() AND IS_NEW_LOAD_SCENE_LOADED())
			OR ((GET_GAME_TIMER() - Data.iGameTimer) > 7000)
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP() 
				ENDIF
				
				Data.iSceneId = CREATE_SYNCHRONIZED_SCENE(Data.vLocation, <<0.0, 0.0, Data.fHeading>>)
				IF NOT IS_ENTITY_DEAD(Data.CloneId)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(Data.CloneId, Data.iSceneId, strAnimNameEntity, strAnimDict, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
				ENDIF
				
				Data.CamId = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				PLAY_SYNCHRONIZED_CAM_ANIM(Data.CamId, Data.iSceneId, strAnimNameCamera, strAnimDict)
				
				START_AUDIO_SCENE(strAudioScene)

				IF IS_STREAM_PLAYING()
					STOP_STREAM()
				ENDIF
				
				PLAY_STREAM_FRONTEND()
				
				PRINTLN("[SUB_WARP][DEPART_SCENE] starting ")
				
				Data.iState++
				Data.iGameTimer = GET_GAME_TIMER()
					

			ENDIF
		BREAK
		
		// fade in
		CASE 4		
			IF ((GET_GAME_TIMER() - Data.iGameTimer) > 250) // hides pops
			AND IS_SYNCHRONIZED_SCENE_RUNNING(Data.iSceneId)
				DO_SCREEN_FADE_IN(0)			
				Data.iState++
				Data.iGameTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
	
		// wait for cutscene to finish playing
		CASE 5
			//IF ((GET_GAME_TIMER() - Data.iGameTimer) > 10000)
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(Data.iSceneId)
			OR (GET_SYNCHRONIZED_SCENE_PHASE(Data.iSceneId) >= Data.fEndScenePhase)
				IF NOT IS_SCREEN_FADING_OUT()
					IF (bDeparting)
						DO_SCREEN_FADE_OUT(0)
					ELSE
						DO_SCREEN_FADE_OUT(500)
					ENDIF
				ENDIF
				IF IS_SCREEN_FADED_OUT()			
					Data.iState++
					Data.iGameTimer = GET_GAME_TIMER()
					PRINTLN("[SUB_WARP][DEPART_SCENE] cutscene faded out")
				ENDIF
			ELSE
			
			IF (bDeparting)
				UPDATE_LIGHTING_FOR_CAMERA(Data)
			ENDIF
			UPDATE_SUB_PARTICLE_FX(Data, strPtFxAsset, bDeparting)
			
			#IF IS_DEBUG_BUILD
			IF (Data.bPause)
				IF NOT (Data.bIsPaused)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(Data.iSceneId)
						SET_SYNCHRONIZED_SCENE_RATE(Data.iSceneId, 0.0)
						Data.bIsPaused = TRUE
					ENDIF
				ENDIF
			ELSE
				IF (Data.bIsPaused)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(Data.iSceneId)
						SET_SYNCHRONIZED_SCENE_RATE(Data.iSceneId, 1.0)
						Data.bIsPaused = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_STREAM_PLAYING()
				PRINTLN("[SUB_WARP][DEPART_SCENE] stream is playing ")
			ELSE
				PRINTLN("[SUB_WARP][DEPART_SCENE] stream is not playing. ")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE(strAudioScene)
				PRINTLN("[SUB_WARP][DEPART_SCENE] audio scene is playing ")
			ELSE
				PRINTLN("[SUB_WARP][DEPART_SCENE] audio scene is NOT playing ")
			ENDIF
			
			IF DOES_ENTITY_EXIST(Data.CloneId)
				
				VECTOR vec
				
				IF DOES_ENTITY_HAVE_PHYSICS(Data.CloneId)
					PRINTLN("[SUB_WARP][DEPART_SCENE] sub has physics ")
					vec = GET_ENTITY_VELOCITY(Data.CloneId)
					PRINTLN("[SUB_WARP][DEPART_SCENE] vel =  ", vec)
				ELSE
					PRINTLN("[SUB_WARP][DEPART_SCENE] sub does not have physics ")
				ENDIF
				
				
			ENDIF
			
			#ENDIF
			

				
			ENDIF
		BREAK
		
		// wait a second for the audio to finish before cleaning up.
		CASE 6
			IF (bDeparting)
			AND ((GET_GAME_TIMER() - Data.iGameTimer) < 1000)
				PRINTLN("[SUB_WARP][DEPART_SCENE] waiting for audio to cleanup ")	
			ELSE
				CLEANUP_SUBMARINE_CUTSCENE(Data, strAnimDict, strPtFxAsset, strAudioScene)
				RETURN TRUE	
			ENDIF
		BREAK
		
	ENDSWITCH
	
	// the sub has been destroyed mid cutscene
	IF HAS_OWNER_EMPTIED_DESTROYED_SMPL_INTERIOR_SUBMARINE(g_OwnerOfSubmarinePropertyIAmIn)
		PRINTLN("[SUB_WARP][DEPART_SCENE] has been destroyed mid cutscene")	
		CLEANUP_SUBMARINE_CUTSCENE(Data, strAnimDict, strPtFxAsset, strAudioScene)
		RETURN TRUE
	ENDIF
	
	

	RETURN FALSE

ENDFUNC

FUNC BOOL FindSafeWarpPoint(SUBMARINE_WARP_DATA &Data)

	INT iSlot
	
	#IF IS_DEBUG_BUILD
	FLOAT fMaxDist = -1.0
	#ENDIF

	// we are going to use the custom car node search functions, this has the host authoritive stuff already 
	// built in and is production ready.

	SWITCH Data.iFindPointState
	
		// add all the points as custom car nodes
		CASE 0
		
			CLEAR_CUSTOM_VEHICLE_NODES() // clear any custom car nodes left dangling
		
			// get the point data
			SUBMARINE_AREA_WARP_DATA AreaWarpData
			GET_SUBMARINE_AREA_WARP_POSITIONS(AreaWarpData, Data.iTargetArea)
		
			REPEAT 	NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS iSlot
				ADD_CUSTOM_VEHICLE_NODE(<<AreaWarpData.Slots[iSlot].Coords.x, AreaWarpData.Slots[iSlot].Coords.y, SUBMARINE_SPAWN_DEPTH>>, AreaWarpData.Slots[iSlot].Heading)
			ENDREPEAT
			
			Data.TargetMidpoint = GetAreaMidPoint(Data.iTargetArea)
			
			#IF IS_DEBUG_BUILD
			// what is the furthest point from the midpoint??			
			REPEAT 	NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS iSlot
				IF (VDIST(Data.TargetMidpoint, AreaWarpData.Slots[iSlot].Coords) > fMaxDist)
					fMaxDist = VDIST(Data.TargetMidpoint, AreaWarpData.Slots[iSlot].Coords)
				ENDIF
			ENDREPEAT
			PRINTLN("[SUB_WARP] - max point from centre is ", fMaxDist)
			#ENDIF
			
			
			PRINTLN("[SUB_WARP] - FindSafeWarpPoint - initialised ")
			Data.iFindPointState++
		BREAK
		
		CASE 1
		
			VEHICLE_SPAWN_LOCATION_PARAMS params
			params.bAllowFallbackToInactiveNodes = FALSE
			params.bAvoidSpawningInExclusionZones = FALSE
			params.bCheckEntityArea = TRUE
			params.bCheckOwnVisibility = FALSE
			params.bCheckThisPlayerVehicle = FALSE
			params.bEnforceMinDistForCustomNodes = FALSE
			params.bIgnoreCustomNodesForArea = TRUE
			params.bIgnoreCustomNodesForMissionLaunch = TRUE
			params.fMaxDistance = MAX_SPREAD_DIST_OF_SUB_SPAWNS
			params.bReturnNearestGoodNode = FALSE
			params.bReturnRandomGoodNode = FALSE
			params.bUseRandomNodeAfter1stAttempt = TRUE
		
			IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(Data.TargetMidpoint, <<0,0,0>>, KOSATKA, FALSE, Data.TargetSlotData.Coords, Data.TargetSlotData.Heading, params)
				PRINTLN("[SUB_WARP] - FindSafeWarpPoint - got point, returning TRUE. coords ", Data.TargetSlotData.Coords)	
				Data.iFindPointState = 0
				RETURN TRUE
			ELSE
				PRINTLN("[SUB_WARP] - FindSafeWarpPoint - waiting on  HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS")	
			ENDIF
			
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC




PROC _MAINTAIN_CUTSCENE_FOR_NON_OWNER(SUBMARINE_WARP_DATA &Data, SUBMARINE_HELM_DATA &HelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)

	SUBMARINE_AREA_WARP_DATA AreaWarpData


	
		//PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - g_OwnerOfSubmarinePropertyIAmIn  = ", NATIVE_TO_INT(g_OwnerOfSubmarinePropertyIAmIn))
	
		IF (g_OwnerOfSubmarinePropertyIAmIn != INVALID_PLAYER_INDEX())

			VEHICLE_INDEX SubVeh = GET_SUBMARINE_VEHICLE(g_OwnerOfSubmarinePropertyIAmIn)
			
			//IF DOES_ENTITY_EXIST(SubVeh)
			//AND NOT IS_ENTITY_DEAD(SubVeh)
			
				SWITCH Data.State
					CASE SWS_NONE
						
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						AND (g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE)
						
							// if there is a player using the helm, have they triggered a fast warp?
							IF (serverBD.iplayerUsingHelm > -1)
							AND IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET(HelmData, INT_TO_NATIVE(PLAYER_INDEX, serverBD.iplayerUsingHelm), PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_START_CS)
										
								PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - fast travel has been triggered.")
											
								IF NOT IS_SCREEN_FADING_OUT()
									DO_SCREEN_FADE_OUT(500)
								ENDIF
								
								IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								ENDIF
			
								Data.TargetSlotData.Coords = HelmData.playerBD[serverBD.iplayerUsingHelm].vFastTravelCoordsStart
								Data.TargetSlotData.Heading = HelmData.playerBD[serverBD.iplayerUsingHelm].fFastTravelHeadingStart
									
								Data.CutsceneData.iFlag = HelmData.playerBD[serverBD.iplayerUsingHelm].iSubFlag
								Data.CutsceneData.iColour = HelmData.playerBD[serverBD.iplayerUsingHelm].iSubColour
									
								PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - set Data.TargetSlotData.Coords = ", Data.TargetSlotData.Coords)
				
								Data.CutsceneData.InteriorId = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
								Data.CutsceneData.iRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
				
								REINIT_NET_TIMER(Data.StateTimer)
								Data.State = SWS_START
								
							ENDIF
						
						ELSE
							PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - players control is switched off for some reason, we dont want to interfere.")
						ENDIF
						
					BREAK
					
					CASE SWS_START
						//Proceed when ready and faded out
						IF IS_SCREEN_FADED_OUT()
					
							IF (VMAG(Data.TargetSlotData.Coords) = 0.0)
								GET_SUBMARINE_AREA_WARP_POSITIONS(AreaWarpData, 0)
								Data.TargetSlotData = AreaWarpData.Slots[0]
								PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - didn't have valid start coords, using default .")
							ENDIF
						
							PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - set Data.TargetSlotData.Coords = ", Data.TargetSlotData.Coords)
						
							REINIT_NET_TIMER(Data.StateTimer)
							Data.State = SWS_CUTSCENE_DEPART
							PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - going to SWS_CUTSCENE_DEPART .")
						ELSE
							IF NOT IS_SCREEN_FADING_OUT()
								DO_SCREEN_FADE_OUT(500)	
							ENDIF
						ENDIF
					BREAK
					
					CASE SWS_CUTSCENE_DEPART
						IF SUBMARINE_CUTSCENE(Data.CutsceneData, SubVeh, Data.TargetSlotData.Coords, Data.TargetSlotData.Heading, TRUE)
							Data.State = SWS_WARP
						ENDIF
					BREAK
					
					CASE SWS_WARP
					
						// did submaine die?
						IF HAS_NET_TIMER_EXPIRED(Data.StateTimer, 20000)
						OR HAS_OWNER_EMPTIED_DESTROYED_SMPL_INTERIOR_SUBMARINE(g_OwnerOfSubmarinePropertyIAmIn)
							PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - SWS_WARP - Timed out / dead - go to end")
							RESET_NET_TIMER(Data.StateTimer)
							Data.State = SWS_END
							EXIT
						ENDIF						
					
						// wait for them to trigger the end cutscene
						IF ((serverBD.iplayerUsingHelm > -1) AND IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET(HelmData, INT_TO_NATIVE(PLAYER_INDEX, serverBD.iplayerUsingHelm), PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_END_CS))
							REINIT_NET_TIMER(Data.StateTimer)
							
							IF (serverBD.iplayerUsingHelm > -1)
								Data.TargetSlotData.Coords = HelmData.playerBD[serverBD.iplayerUsingHelm].vFastTravelCoordsEnd
								Data.TargetSlotData.Heading = HelmData.playerBD[serverBD.iplayerUsingHelm].fFastTravelHeadingEnd
							ENDIF
							
							IF (VMAG(Data.TargetSlotData.Coords) = 0.0)
								GET_SUBMARINE_AREA_WARP_POSITIONS(AreaWarpData, 0)
								Data.TargetSlotData = AreaWarpData.Slots[0]
								PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - didn't have valid end coords, using default .")
							ENDIF							
							
							Data.State = SWS_CUTSCENE_ARRIVE
							PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - triggering arrival scene.")
						ELSE
							PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - waiting to do arrival.")
						ENDIF
					BREAK
					
					CASE SWS_CUTSCENE_ARRIVE
						IF SUBMARINE_CUTSCENE(Data.CutsceneData, SubVeh, Data.TargetSlotData.Coords, Data.TargetSlotData.Heading, FALSE)
							REINIT_NET_TIMER(Data.StateTimer)
							RESET_NET_TIMER(Data.StateTimer)
							Data.State = SWS_END
						ENDIF
					BREAK
					
					CASE SWS_END
					
						// Add a timeout so we dont get stuck on black screen.
						IF NOT HAS_NET_TIMER_STARTED(Data.StateTimer)
							START_NET_TIMER(Data.StateTimer)
						ENDIF

						//Fade back in
						IF NOT IS_SCREEN_FADING_IN() AND NOT IS_SCREEN_FADED_IN()
							
							// make sure we are back in the correct room
							IF IS_VALID_INTERIOR(Data.CutsceneData.InteriorId)
								IF IS_INTERIOR_READY(Data.CutsceneData.InteriorId)
									FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), Data.CutsceneData.InteriorId, Data.CutsceneData.iRoomKey)
								ELSE
									IF NOT HAS_NET_TIMER_EXPIRED(Data.StateTimer, 5000)
										PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - FORCE_ROOM_FOR_ENTITY - Waiting for interior to be ready...")
										EXIT
									ENDIF
									
									ASSERTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - FORCE_ROOM_FOR_ENTITY - Interior is not ready and timer has expired. Fading in without forcing player into room...")
								ENDIF
								
								PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - FORCE_ROOM_FOR_ENTITY - interior id = ", NATIVE_TO_INT(Data.CutsceneData.InteriorId), ", roomkey = ", Data.CutsceneData.iRoomKey)
							ELSE
								PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - FORCE_ROOM_FOR_ENTITY - interior not valid")
							ENDIF

	
						
							DO_SCREEN_FADE_IN(500)
						ELIF IS_SCREEN_FADED_IN()
							
							PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - finished. successful")
							
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							IF HAS_NET_TIMER_STARTED(Data.StateTimer)
								RESET_NET_TIMER(Data.StateTimer)
							ENDIF
							Data.State = SWS_NONE
							
						ENDIF
						
					BREAK					
					
				ENDSWITCH
			
//			ELSE
//				
//				IF NOT (Data.State = SWS_NONE)
//					IF NOT IS_SCREEN_FADING_IN() AND NOT IS_SCREEN_FADED_IN()
//						DO_SCREEN_FADE_IN(500)
//					ELIF IS_SCREEN_FADED_IN()
//						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						Data.State = SWS_NONE
//					ENDIF
//				ENDIF
//			
//			ENDIF
			
		ENDIF


	IF (Data.State != SWS_NONE)
	OR ((serverBD.iplayerUsingHelm > -1) AND IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET(HelmData, INT_TO_NATIVE(PLAYER_INDEX, serverBD.iplayerUsingHelm), PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_START_CS))								
		IF (MPGlobalsAmbience.bSubFastTravelActive = FALSE)
			PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - setting MPGlobalsAmbience.bSubFastTravelActive = TRUE")
			MPGlobalsAmbience.bSubFastTravelActive = TRUE
		ENDIF
	ELSE
		IF (MPGlobalsAmbience.bSubFastTravelActive = TRUE)
			PRINTLN("[SUB_WARP] _MAINTAIN_CUTSCENE_FOR_NON_OWNER - setting MPGlobalsAmbience.bSubFastTravelActive = FALSE")
			MPGlobalsAmbience.bSubFastTravelActive = FALSE
		ENDIF
	ENDIF	


ENDPROC

PROC _SUBMARINE_MAINTAIN_WARP(SUBMARINE_FAST_TRAVEL_MENU &FTMData, SUBMARINE_WARP_DATA &Data, SUBMARINE_HELM_DATA &HelmData)	
	PLAYER_INDEX LocalPlayerId = PLAYER_ID()
	VEHICLE_INDEX SubVeh = GET_SUBMARINE_VEHICLE(LocalPlayerId)

	SWITCH Data.State
		CASE SWS_START
			
			IF HAS_NET_TIMER_EXPIRED(Data.StateTimer, 1600)
				IF HAS_NET_TIMER_EXPIRED(Data.StateTimer, 30000)
					ASSERTLN("[SUB_WARP] _SUBMARINE_MAINTAIN_WARP - 0 - Timed out")
					_SUBMARINE_WARP_SET_STATE(Data, SWS_END)
					EXIT
				ENDIF
				
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(500)
				ENDIF
				
				//Proceed when ready and faded out
				IF IS_SCREEN_FADED_OUT()
					Data.bWasDisplayingMenu = FTMData.bIsDiplaying
					_SUBMARINE_CLEAR_FAST_TRAVEL_MENU(FTMData)
					_SUBMARINE_WARP_SET_STATE(Data, SWS_CUTSCENE_DEPART)
				ENDIF
				
				IF DOES_ENTITY_EXIST(SubVeh)
				AND NOT IS_ENTITY_DEAD(SubVeh)
				
					HelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].vFastTravelCoordsStart = GET_ENTITY_COORDS(SubVeh)
					HelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].fFastTravelHeadingStart = GET_ENTITY_HEADING(SubVeh)				
								
					Data.CutsceneData.iColour = GET_LOCAL_PLAYER_SUBMARINE_COLOUR()
					Data.CutsceneData.iFlag = GET_LOCAL_PLAYER_SUBMARINE_FLAG()
					
					HelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iSubColour = Data.CutsceneData.iColour
					HelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iSubFlag = Data.CutsceneData.iFlag				
												
					SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(HelmData, PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_START_CS, TRUE)
				ENDIF
				
				// initialise any variables that need to be
				Data.iFindPointState = 0
			ELSE
				PRINTLN("[SUB_WARP] _SUBMARINE_MAINTAIN_WARP - SWS_WARP - waiting on start delay")
			ENDIF
			
		BREAK
		
		CASE SWS_CUTSCENE_DEPART
			
			FORCE_DIVE_SUBMARINE(-30.0)
		
			IF SUBMARINE_CUTSCENE(Data.CutsceneData, SubVeh, HelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].vFastTravelCoordsStart, HelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].fFastTravelHeadingStart, TRUE)
				_SUBMARINE_WARP_SET_STATE(Data, SWS_WARP)
				SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(HelmData, PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_START_CS, FALSE)
			ENDIF
		BREAK
		
		CASE SWS_WARP
		
			FORCE_DIVE_SUBMARINE(-30.0)
			
			IF HAS_NET_TIMER_EXPIRED(Data.StateTimer, 20000)
			OR NOT IS_ENTITY_ALIVE(SubVeh)
				PRINTLN("[SUB_WARP] _SUBMARINE_MAINTAIN_WARP - SWS_WARP - Timed out")
				_SUBMARINE_WARP_SET_STATE(Data, SWS_END)
				EXIT
			ENDIF
		
			IF IS_ENTITY_ALIVE(SubVeh)
			
				IF FindSafeWarpPoint(Data)
					
					REQUEST_WARP_SUBMARINE(Data.TargetSlotData.Coords, Data.TargetSlotData.Heading)
					
					
					IF DOES_ENTITY_EXIST(SubVeh)
					AND NOT IS_ENTITY_DEAD(SubVeh)
						HelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].vFastTravelCoordsEnd = Data.TargetSlotData.Coords
						HelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].fFastTravelHeadingEnd = Data.TargetSlotData.Heading
						SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(HelmData, PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_END_CS, TRUE)
					ENDIF					
					
					_SUBMARINE_WARP_SET_STATE(Data, SWS_CUTSCENE_ARRIVE)
				ELSE
					PRINTLN("[SUB_WARP] _SUBMARINE_MAINTAIN_WARP - SWS_WARP - waiting on FindSafeWarpPoint")
				ENDIF

			ENDIF
			
			
		BREAK
		
		CASE SWS_CUTSCENE_ARRIVE
			IF NOT IS_SUBMARINE_WARPING()
				IF SUBMARINE_CUTSCENE(Data.CutsceneData, SubVeh, Data.TargetSlotData.Coords, Data.TargetSlotData.Heading, FALSE)
					_SUBMARINE_WARP_SET_STATE(Data, SWS_END)
					SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(HelmData, PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_END_CS, FALSE)
				ENDIF	
			ELSE
				PRINTLN("[SUB_WARP] _SUBMARINE_MAINTAIN_WARP - SWS_CUTSCENE_ARRIVE - waiting on IS_SUBMARINE_WARPING")
			ENDIF
		BREAK
		
		CASE SWS_END
			
			// come out of menu
			FTMData.bWantsToDisplay = FALSE
			
			//Fade back in
			IF NOT IS_SCREEN_FADING_IN() AND NOT IS_SCREEN_FADED_IN()
				DO_SCREEN_FADE_IN(500)
			ELIF IS_SCREEN_FADED_IN()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				_SUBMARINE_WARP_SET_STATE(Data, SWS_NONE)
			ENDIF
			
		BREAK
	ENDSWITCH
	
	IF (MPGlobalsAmbience.bSubFastTravelActive = TRUE)
		IF (Data.State = SWS_NONE)
			PRINTLN("[SUB_WARP] _SUBMARINE_MAINTAIN_WARP - setting MPGlobalsAmbience.bSubFastTravelActive = FALSE")
			MPGlobalsAmbience.bSubFastTravelActive = FALSE
		ENDIF
	ELSE
		IF (Data.State != SWS_NONE)
			PRINTLN("[SUB_WARP] _SUBMARINE_MAINTAIN_WARP - setting MPGlobalsAmbience.bSubFastTravelActive = TRUE")
			MPGlobalsAmbience.bSubFastTravelActive = TRUE
		ENDIF	
	ENDIF	
	
ENDPROC




#IF IS_DEBUG_BUILD




PROC _SUBMARINE_WARP_ADD_WIDGETS(SUBMARINE_FAST_TRAVEL_MENU &FTMData, SUBMARINE_WARP_DATA &Data)
	
	INT i
	TEXT_LABEL_63 str	

	PRINTLN("[SUB_WARP] Adding debug widgets.")
	START_WIDGET_GROUP("Submarine Fast Travel")
	
		START_WIDGET_GROUP("transaction")
			ADD_WIDGET_INT_SLIDER("iCashTransactionId", Data.iCashTransactionId, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iCashTransactionState", Data.iCashTransactionState, -1, HIGHEST_INT, 1)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Warp Data")
			ADD_WIDGET_VECTOR_SLIDER("TargetMidpoint", Data.TargetMidpoint, -9999.9, 9999.9, 0.01)	
			ADD_WIDGET_VECTOR_SLIDER("TargetSlotData.Coords", Data.TargetSlotData.Coords, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("TargetSlotData.Heading", Data.TargetSlotData.Heading, -9999.9, 9999.9, 0.01)

			//ADD_WIDGET_FLOAT_SLIDER("fDiveForce", Data.fDiveForce, -9999.9, 9999.9, 0.01)

			ADD_WIDGET_BOOL("bWasDisplayingMenu", Data.bWasDisplayingMenu)
			
			
			ADD_WIDGET_BOOL("MPGlobalsAmbience.bSubFastTravelActive", MPGlobalsAmbience.bSubFastTravelActive)
			
			START_WIDGET_GROUP("Disable Stuff")
				ADD_WIDGET_BOOL("bSubFastTravelDisabled", MPGlobalsAmbience.bSubFastTravelDisabled)	
				REPEAT NUM_SUBMARINE_PRESET_WARP_AREAS i
					str = "bSubFastTravelAreaDisabled["
					str += i
					str += "]"
					ADD_WIDGET_BOOL(str, MPGlobalsAmbience.bSubFastTravelAreaDisabled[i])	
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Cutscene")
				ADD_WIDGET_BOOL("Cutscene is Departing", Data.bIsDepartingCutscene)
				ADD_WIDGET_BOOL("Launch Cutscene", Data.bLaunchCutscene)
				ADD_WIDGET_BOOL("Pause", Data.CutsceneData.bPause)				
				
				START_WIDGET_GROUP("Lighting")
					
					ADD_WIDGET_BOOL("Debug Render", Data.CutsceneData.bRenderLights)
					ADD_WIDGET_BOOL("Output Data", Data.CutsceneData.bOutputLightData)
				
					REPEAT NUM_SUB_CUTSCENE_LIGHTS i
						str = "Light "
						str += i						
						START_WIDGET_GROUP(str)
							ADD_WIDGET_BOOL("Active", Data.CutsceneData.SubLight[i].bActive)														
							START_NEW_WIDGET_COMBO()
								ADD_TO_WIDGET_COMBO("DRAW_LIGHT_WITH_RANGE")
								ADD_TO_WIDGET_COMBO("DRAW_LIGHT_WITH_RANGEEX")								
								ADD_TO_WIDGET_COMBO("DRAW_SPOT_LIGHT")
								ADD_TO_WIDGET_COMBO("DRAW_SHADOWED_SPOT_LIGHT")
							STOP_WIDGET_COMBO("Type", Data.CutsceneData.SubLight[i].iType )
							ADD_WIDGET_VECTOR_SLIDER("Offset", Data.CutsceneData.SubLight[i].vOffset, -500.0, 500.0, 0.01)			
							ADD_WIDGET_VECTOR_SLIDER("Direction", Data.CutsceneData.SubLight[i].vDirection, -500.0, 500.0, 0.01)
							ADD_WIDGET_INT_SLIDER("R", Data.CutsceneData.SubLight[i].r, 0, 255, 1)
							ADD_WIDGET_INT_SLIDER("G", Data.CutsceneData.SubLight[i].g, 0, 255, 1)
							ADD_WIDGET_INT_SLIDER("B", Data.CutsceneData.SubLight[i].b, 0, 255, 1)
							ADD_WIDGET_FLOAT_SLIDER("Range", Data.CutsceneData.SubLight[i].Range, 0.0, 9999.9, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("Intensity", Data.CutsceneData.SubLight[i].Intensity, 0.0, 9999.9, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("Falloff", Data.CutsceneData.SubLight[i].Falloff, 0.0, 9999.9, 0.01)						
							ADD_WIDGET_FLOAT_SLIDER("InnerAngle", Data.CutsceneData.SubLight[i].InnerAngle, 0.0, 360.0, 1.0)
							ADD_WIDGET_FLOAT_SLIDER("OuterAngle", Data.CutsceneData.SubLight[i].OuterAngle, 0.0, 360.0, 1.0)							
							ADD_WIDGET_FLOAT_SLIDER("Exp", Data.CutsceneData.SubLight[i].Exp, 0.0, 9999.9, 0.01)
						STOP_WIDGET_GROUP()
					ENDREPEAT
				STOP_WIDGET_GROUP()
					
				START_WIDGET_GROUP("PTFX")
					ADD_WIDGET_BOOL("Output Data", Data.CutsceneData.bOutputPtFxData)
					ADD_WIDGET_BOOL("Render Bones", Data.CutsceneData.bRenderPtFx)
					REPEAT NUM_SUB_PTFX i
						str = "PtFx "
						str += i	
						START_WIDGET_GROUP(str)
							ADD_WIDGET_BOOL("Recreate", Data.CutsceneData.PtFx[i].bRecreate)
							ADD_WIDGET_VECTOR_SLIDER("vOffset", Data.CutsceneData.PtFx[i].vOffset, -100.0, 100.0, 0.01)	
							ADD_WIDGET_VECTOR_SLIDER("vRotation", Data.CutsceneData.PtFx[i].vRotation, -100.0, 100.0, 0.01)								
							ADD_WIDGET_INT_SLIDER("iSceneId", Data.CutsceneData.PtFx[i].iSceneId, -1, 1, 1)
							ADD_WIDGET_BOOL("bLooped", Data.CutsceneData.PtFx[i].bLooped)
						STOP_WIDGET_GROUP()
					ENDREPEAT
				STOP_WIDGET_GROUP()
				
				
				START_WIDGET_GROUP("Debug")
					ADD_WIDGET_INT_SLIDER("iState", Data.CutsceneData.iState,-1,999,1)
					ADD_WIDGET_INT_SLIDER("iSceneId", Data.CutsceneData.iSceneId,-1,999,1)
					ADD_WIDGET_VECTOR_SLIDER("vLocation", Data.CutsceneData.vLocation, -9999.9, 9999.9, 0.01)	
					ADD_WIDGET_FLOAT_SLIDER("fZHeight", Data.CutsceneData.fZHeight, -9999.9, 9999.9, 0.01)			
					ADD_WIDGET_FLOAT_SLIDER("fHeading", Data.CutsceneData.fHeading, -9999.9, 9999.9, 0.01)
					ADD_WIDGET_INT_SLIDER("iCalmId", Data.CutsceneData.iCalmId, -1, HIGHEST_INT, 1)
					ADD_WIDGET_FLOAT_SLIDER("fCalmingQuadSize", Data.CutsceneData.fCalmingQuadSize, 0.0, 9999.9, 1.0)	
					ADD_WIDGET_FLOAT_SLIDER("fScalarWaveValue", Data.CutsceneData.fScalarWaveValue, 0.0, 2.0, 0.01)	
					ADD_WIDGET_FLOAT_SLIDER("fEndScenePhase", Data.CutsceneData.fEndScenePhase, 0.0, 1.0, 0.01)
					ADD_WIDGET_INT_SLIDER("iColour", Data.CutsceneData.iColour, 0, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("iFlag", Data.CutsceneData.iFlag, 0, HIGHEST_INT, 1)
				STOP_WIDGET_GROUP()
//				ADD_WIDGET_VECTOR_SLIDER("vWaterOffset", Data.CutsceneData.vWaterOffset, -500.0, 500.0, 0.1)
//				ADD_WIDGET_FLOAT_SLIDER("fWaterSpeed", Data.CutsceneData.fWaterSpeed, 0.0, 5.0, 0.1)
//				ADD_WIDGET_FLOAT_SLIDER("fWaterRate", Data.CutsceneData.fWaterRate, 0.0, 1.0, 0.01)
//				ADD_WIDGET_BOOL("bDrawWaterSphere", Data.CutsceneData.bDrawWaterSphere)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Areas")
				REPEAT NUM_SUBMARINE_PRESET_WARP_AREAS i
					str = "Area "
					str += i
					ADD_WIDGET_STRING(str)
					ADD_WIDGET_STRING(Data.Areas[i].Name)
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Menu Data")
			ADD_WIDGET_BOOL("bWantsToDisplay", FTMData.bWantsToDisplay)
			ADD_WIDGET_BOOL("bIsDiplaying", FTMData.bIsDiplaying)
			ADD_WIDGET_INT_SLIDER("iCurrentItem", FTMData.iCurrentItem, -1, 99, 1)
			ADD_WIDGET_FLOAT_SLIDER("fMapZoom", FTMData.fMapZoom, -999.0, 9999.0, 0.01)
			ADD_WIDGET_BOOL("bConfirmedMove", FTMData.bConfirmedMove)
		STOP_WIDGET_GROUP()
		
		
		
	STOP_WIDGET_GROUP()
	
	START_WIDGET_GROUP("Submarine weapon tracking")
		ADD_WIDGET_INT_SLIDER("number of kills", MPGlobalsAmbience.SubWeaponData.iNumberOfKills, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("number of projectiles", MPGlobalsAmbience.SubWeaponData.iNumberOfProjectiles, -1, 99, 1)
	STOP_WIDGET_GROUP()
	
	
ENDPROC


STRUCT SUB_WARP_PLACEMENT_DATA
	
	VEHICLE_INDEX VehicleID[60]

	BOOL bIsActive
	BOOL bInitialised
	BOOL bOutputAll
	BOOL bOutputSelected
	BOOL bWarpToLeadSub

	BOOL bTestWarp
	BOOL bTestAllWarpLocations
	BOOL bTestAllWarpLocationsStarted
	INT iTestWarpArea
	INT iTestWarpSlot
	
	VECTOR vTestCoords
	FLOAT fTestHeading

	INT iSelectedArea = 0
	
	VECTOR vWorldPos[NUM_SUBMARINE_PRESET_WARP_AREAS][NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS]
	FLOAT fWorldHeading[NUM_SUBMARINE_PRESET_WARP_AREAS][NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS]
	BOOL bGrabFromZero[NUM_SUBMARINE_PRESET_WARP_AREAS][NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS]
	
	VECTOR vInitialSpawnPos[NUMBER_OF_SUBMARINE_SPAWNS]
	FLOAT fInitialSpawnHeading[NUMBER_OF_SUBMARINE_SPAWNS]
	
	BOOL bCalculateOffsets
	BOOL bLoadOffsets
	VECTOR vOffsetPos[NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS]
	FLOAT fOffsetHeading[NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS]
	
	FLOAT fFrontOffset  = 52.0
	FLOAT fBackOffset = -70.0
	FLOAT fMinDepth = -40.0
	
	BOOL bDoInitalPositions

ENDSTRUCT

PROC SUBMARINE_WARP_PLACEMENT_WIDGET(SUB_WARP_PLACEMENT_DATA &Data)

	INT iArea, iSlot, i
	TEXT_LABEL_63 str

	START_WIDGET_GROUP("Submarine Warp Placement")
		ADD_WIDGET_BOOL("bIsActive", Data.bIsActive)
		ADD_WIDGET_BOOL("bInitialised", Data.bInitialised)
		ADD_WIDGET_BOOL("bCalculateOffsets", Data.bCalculateOffsets)
		ADD_WIDGET_BOOL("bLoadOffsets", Data.bLoadOffsets)		
		ADD_WIDGET_BOOL("bOutputAll", Data.bOutputAll)
		ADD_WIDGET_BOOL("bOutputSelected", Data.bOutputSelected)
		ADD_WIDGET_BOOL("bWarpToLeadSub", Data.bWarpToLeadSub)
		ADD_WIDGET_BOOL("bTestWarp", Data.bTestWarp)
		ADD_WIDGET_BOOL("bTestAllWarpLocations", Data.bTestAllWarpLocations)
		ADD_WIDGET_VECTOR_SLIDER("Test Coords", Data.vTestCoords, -99999.9, 99999.9, 0.5)
		ADD_WIDGET_FLOAT_SLIDER("Test Heading", Data.fTestHeading, 0.0, 360.0, 1.0)
		
		ADD_WIDGET_INT_SLIDER("iSelectedArea", Data.iSelectedArea, 0, NUM_SUBMARINE_PRESET_WARP_AREAS-1, 1)
		
		ADD_WIDGET_FLOAT_SLIDER("fFrontOffset", Data.fFrontOffset, 0.0, 200.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("fBackOffset", Data.fBackOffset, -200, 0.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("fMinDepth", Data.fMinDepth, -100.0, 0.0, 0.1)
		
		ADD_WIDGET_BOOL("bDoInitalPositions", Data.bDoInitalPositions)
		
		//ADD_WIDGET_FLOAT_SLIDER("SUBMARINE_SPAWN_DEPTH", SUBMARINE_SPAWN_DEPTH, -30.0, 0.0, 0.1)
		
		START_WIDGET_GROUP("Fast Warp Positions")
			REPEAT NUM_SUBMARINE_PRESET_WARP_AREAS iArea
				str = "Area "
				str += iArea
				START_WIDGET_GROUP(str)
					REPEAT NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS iSlot
						str = "["
						str += iSlot
						str += "] Position"
						ADD_WIDGET_VECTOR_SLIDER(str, Data.vWorldPos[iArea][iSlot], -99999.9, 99999.9, 0.5)	
						str = "["
						str += iSlot
						str += "] Heading"
						ADD_WIDGET_FLOAT_SLIDER(str, Data.fWorldHeading[iArea][iSlot], 0,360.0, 1.0)
						str = "["
						str += iSlot
						str += "] Grab from 0"
						ADD_WIDGET_BOOL(str, Data.bGrabFromZero[iArea][iSlot])
					ENDREPEAT
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Offset Positions")
			REPEAT NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS iSlot
				str = "slot "
				str += iSlot
				START_WIDGET_GROUP(str)
					ADD_WIDGET_VECTOR_SLIDER("Offset Position", Data.vOffsetPos[iSlot], -99999.9, 99999.9, 0.01)						
					ADD_WIDGET_FLOAT_SLIDER("Offset Heading", Data.fOffsetHeading[iSlot], 0,360.0, 1.0)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Initial Positions")
			REPEAT NUMBER_OF_SUBMARINE_SPAWNS i
				str = "position "
				str += i
				ADD_WIDGET_VECTOR_SLIDER(str, Data.vInitialSpawnPos[i], -99999.9, 99999.9, 0.5)	
				str = "heading "
				str += i
				ADD_WIDGET_FLOAT_SLIDER(str, Data.fInitialSpawnHeading[i], 0,360.0, 1.0)	
			ENDREPEAT
		STOP_WIDGET_GROUP()
	
	STOP_WIDGET_GROUP()

ENDPROC

PROC UPDATE_SUBMARINE_WARP_PLACEMENT_WIDGET(SUB_WARP_PLACEMENT_DATA &Data)

	INT iArea, iSlot, i
	SUBMARINE_AREA_WARP_DATA WarpData
	TEXT_LABEL_63 str
	VECTOR vCoords
	FLOAT fGroundZ
	VECTOR vPos
	FLOAT fHeading

	IF (Data.bIsActive)
		
		// grab data
		IF NOT (Data.bInitialised)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			REQUEST_MODEL(KOSATKA)			
			// grab all currently saved points
			REPEAT NUM_SUBMARINE_PRESET_WARP_AREAS iArea
				REPEAT NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS iSlot				
					GET_SUBMARINE_AREA_WARP_POSITIONS(WarpData, iArea, TRUE)									
					Data.vWorldPos[iArea][iSlot] = WarpData.Slots[iSlot].Coords
					Data.fWorldHeading[iArea][iSlot] = WarpData.Slots[iSlot].Heading
				ENDREPEAT
			ENDREPEAT					
			// grab all initial positions
			REPEAT NUMBER_OF_SUBMARINE_SPAWNS i
				GET_SUBMARINE_VEHICLE_SPAWN_LOCATIONS(i, Data.vInitialSpawnPos[i], Data.fInitialSpawnHeading[i]	)
			ENDREPEAT
			Data.bInitialised = TRUE
		ENDIF
		
		
		// calculate offests
		IF (Data.bCalculateOffsets)
			IF DOES_ENTITY_EXIST(Data.VehicleID[0])
			AND NOT IS_ENTITY_DEAD(Data.VehicleID[0])
			
				REPEAT NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS iSlot
					Data.vOffsetPos[iSlot] = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(Data.VehicleID[0], Data.vWorldPos[Data.iSelectedArea][iSlot])
					Data.fOffsetHeading[iSlot] = Data.fWorldHeading[Data.iSelectedArea][iSlot] - Data.fWorldHeading[Data.iSelectedArea][0] 
				ENDREPEAT
			ENDIF					
			Data.bCalculateOffsets = FALSE
		ENDIF
		
		// load offsets
		IF (Data.bLoadOffsets)			
			REPEAT NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS iSlot
				Data.vWorldPos[Data.iSelectedArea][iSlot] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(Data.vWorldPos[Data.iSelectedArea][0], Data.fWorldHeading[Data.iSelectedArea][0], Data.vOffsetPos[iSlot] )
				Data.fWorldHeading[Data.iSelectedArea][iSlot] = Data.fWorldHeading[Data.iSelectedArea][0] + Data.fOffsetHeading[iSlot]
			ENDREPEAT
			Data.bLoadOffsets = FALSE
		ENDIF
			
			
		// grab from zero
		REPEAT NUM_SUBMARINE_PRESET_WARP_AREAS iArea
			REPEAT NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS iSlot
				IF (Data.bGrabFromZero[iArea][iSlot])
					Data.vWorldPos[iArea][iSlot] = Data.vWorldPos[iArea][0]
					Data.fWorldHeading[iArea][iSlot] = Data.fWorldHeading[iArea][0]
					Data.bGrabFromZero[iArea][iSlot] = FALSE
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
		
		// create vehicles and set positions
		REPEAT NUMBER_OF_SUBMARINE_SPAWNS iSlot
		
			IF (Data.bDoInitalPositions)
				vPos = Data.vInitialSpawnPos[iSlot]
				fHeading = Data.fInitialSpawnHeading[iSlot]
			ELSE
				IF (iSlot < NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS)
					vPos = Data.vWorldPos[Data.iSelectedArea][iSlot]
					fHeading = Data.fWorldHeading[Data.iSelectedArea][iSlot]
				ELSE
					IF DOES_ENTITY_EXIST(Data.VehicleID[iSlot])	
						DELETE_VEHICLE(Data.VehicleID[iSlot])
					ENDIF
				ENDIF
			ENDIF
		
			IF HAS_MODEL_LOADED(KOSATKA)
				IF NOT DOES_ENTITY_EXIST(Data.VehicleID[iSlot])	
					IF (Data.bDoInitalPositions)
					OR (iSlot < NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS)
						Data.VehicleID[iSlot]  = CREATE_VEHICLE(KOSATKA, vPos, fHeading, FALSE)
						SET_ENTITY_COLLISION(Data.VehicleID[iSlot], FALSE)
						FREEZE_ENTITY_POSITION(Data.VehicleID[iSlot], TRUE)
						SET_ENTITY_INVINCIBLE(Data.VehicleID[iSlot], TRUE)
					ENDIF
				ELSE
					IF NOT IS_ENTITY_DEAD(Data.VehicleID[iSlot])	
						SET_ENTITY_COORDS_NO_OFFSET(Data.VehicleID[iSlot],vPos)
						SET_ENTITY_HEADING(Data.VehicleID[iSlot],fHeading)
					ENDIF
					
					// front offset
					vCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Data.VehicleID[iSlot], <<0.0, Data.fFrontOffset, 0.0>>)
					
					IF GET_GROUND_Z_FOR_3D_COORD(vCoords, fGroundZ)
						str = "Depth = "
						str += ROUND(fGroundZ)						
					ENDIF
					IF (fGroundZ < 0.0)
					AND (fGroundZ > Data.fMinDepth)
						DRAW_DEBUG_TEXT(str, <<vCoords.x, vCoords.y, vCoords.z + 1.0>>, 255)
					ELSE
						DRAW_DEBUG_TEXT(str, <<vCoords.x, vCoords.y, vCoords.z + 1.0>>)
					ENDIF
					
					// back offset
					vCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Data.VehicleID[iSlot], <<0.0, Data.fBackOffset, 0.0>>)
					IF GET_GROUND_Z_FOR_3D_COORD(vCoords, fGroundZ)
						str = "Depth = "
						str += ROUND(fGroundZ)						
					ENDIF
					IF (fGroundZ < 0.0)
					AND (fGroundZ > Data.fMinDepth)
						DRAW_DEBUG_TEXT(str, <<vCoords.x, vCoords.y, vCoords.z + 1.0>>, 255)
					ELSE
						DRAW_DEBUG_TEXT(str, <<vCoords.x, vCoords.y, vCoords.z + 1.0>>)
					ENDIF
					
					
					// middle
					vCoords = vPos
					IF (Data.bDoInitalPositions)
						str = "Initial Position "
						str += iSlot
					ELSE
						str = "Area "
						str += Data.iSelectedArea
						str += ", Slot "
						str += iSlot
					ENDIF
					IF GET_GROUND_Z_FOR_3D_COORD(vCoords, fGroundZ)
						str += " Depth = "
						str += ROUND(fGroundZ)						
					ENDIF
					IF (fGroundZ < 0.0)
					AND (fGroundZ > Data.fMinDepth)
						DRAW_DEBUG_TEXT(str, <<vCoords.x, vCoords.y, vCoords.z + 1.0>>, 255)
					ELSE
						DRAW_DEBUG_TEXT(str, <<vCoords.x, vCoords.y, vCoords.z + 1.0>>)
					ENDIF

					
				ENDIF
			ENDIF
		ENDREPEAT
		
		// test warp
		IF (Data.bTestWarp)
			IF DOES_ENTITY_EXIST(GET_SUBMARINE_VEHICLE(PLAYER_ID()))
			AND NOT IS_ENTITY_DEAD(GET_SUBMARINE_VEHICLE(PLAYER_ID()))
				REQUEST_WARP_SUBMARINE(Data.vTestCoords, Data.fTestHeading)
				Data.bTestWarp = FALSE
			ENDIF
		ENDIF
		
		// test all warp locations
		IF (Data.bTestAllWarpLocations)
		
			IF NOT (Data.bTestAllWarpLocationsStarted)
				Data.iTestWarpArea = 0
				Data.iTestWarpSlot = 0
				Data.bTestAllWarpLocationsStarted = TRUE
				PRINTLN("[SUB_WARP][TESTING] starting to test all warp locations")
			ENDIF
		
			IF (Data.bTestAllWarpLocationsStarted)
				IF (Data.iTestWarpArea < NUM_SUBMARINE_PRESET_WARP_AREAS)
					IF NOT IS_SUBMARINE_WARPING()
																
						GET_SUBMARINE_AREA_WARP_POSITIONS(WarpData, Data.iTestWarpArea)
						Data.vTestCoords = WarpData.Slots[Data.iTestWarpSlot].Coords
						Data.fTestHeading = WarpData.Slots[Data.iTestWarpSlot].Heading	
						
						Data.vTestCoords.z = SUBMARINE_SPAWN_DEPTH
						
						// warp the player there first
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<Data.vTestCoords.x, Data.vTestCoords.y, Data.vTestCoords.z + 50.0>>, FALSE, FALSE, FALSE)

						
						// make sure collision is loaded						
						IF HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
						
							PRINTLN("[SUB_WARP][TESTING] testing area ", Data.iTestWarpArea, " slot ", Data.iTestWarpSlot, " at coords ", Data.vTestCoords)
							REQUEST_WARP_SUBMARINE(Data.vTestCoords, Data.fTestHeading)
														
							// next test
							Data.iTestWarpSlot++
							IF (Data.iTestWarpSlot >= NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS)
								Data.iTestWarpSlot = 0
								Data.iTestWarpArea++
							ENDIF
							
						ELSE
							PRINTLN("[SUB_WARP][TESTING] waiting on collision loading...")
						
						ENDIF
					ELSE
						PRINTLN("[SUB_WARP][TESTING] waiting on warp...")
						
						IF DOES_ENTITY_EXIST(GET_SUBMARINE_VEHICLE(PLAYER_ID()))
							vCoords = GET_ENTITY_COORDS(GET_SUBMARINE_VEHICLE(PLAYER_ID()), FALSE)
							PRINTLN("[SUB_WARP][TESTING] sub current coords = ", vCoords)
							vCoords.z = 10.0
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vCoords, FALSE, FALSE, FALSE)
						ENDIF
						
						IF (MPGlobalsAmbience.bWarpSubFailed)
							PRINTLN("[SUB_WARP][TESTING] FAILED area ", Data.iTestWarpArea, " slot ", Data.iTestWarpSlot, " at coords ", Data.vTestCoords)
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[SUB_WARP][TESTING] finished testing")
					Data.bTestAllWarpLocationsStarted = FALSE
					Data.bTestAllWarpLocations = FALSE
				ENDIF
			ENDIF
		
		ELSE
			IF (Data.bTestAllWarpLocationsStarted)
				Data.bTestAllWarpLocationsStarted = FALSE	
			ENDIF
		ENDIF
		
		// output data
		IF (Data.bOutputAll)
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("----- SUBMARINE WARP POSITIONS -------")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			REPEAT NUM_SUBMARINE_PRESET_WARP_AREAS iArea
				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				str = "CASE "
				str += iArea
				SAVE_STRING_TO_DEBUG_FILE(str)
				
			
				REPEAT NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS iSlot
					SAVE_NEWLINE_TO_DEBUG_FILE()
					str = "_SUBMARINE_WARP_INIT_AREA_SLOT(Data.Slots["
					str += iSlot
					str += "], "
					SAVE_STRING_TO_DEBUG_FILE(str)
					SAVE_VECTOR_TO_DEBUG_FILE(Data.vWorldPos[iArea][iSlot])
					SAVE_STRING_TO_DEBUG_FILE(", ")
					SAVE_FLOAT_TO_DEBUG_FILE(Data.fWorldHeading[iArea][iSlot])
					SAVE_STRING_TO_DEBUG_FILE(")")
				ENDREPEAT
				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				str = "BREAK"
				SAVE_STRING_TO_DEBUG_FILE(str)
			ENDREPEAT
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("----- END -------")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("----- SUBMARINE INITIAL POSITIONS -------")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			REPEAT NUMBER_OF_SUBMARINE_SPAWNS i
				
				// CASE 0     vCoords = <<-1914.2683, -1985.8696, 0.0000>>     fHeading = 316.3999     BREAK
				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				str = "CASE "
				str += i
				str += "     "
				SAVE_STRING_TO_DEBUG_FILE(str)				
				SAVE_STRING_TO_DEBUG_FILE("vCoords = ")
				SAVE_VECTOR_TO_DEBUG_FILE(Data.vInitialSpawnPos[i])
				SAVE_STRING_TO_DEBUG_FILE("     fHeading = ")
				SAVE_FLOAT_TO_DEBUG_FILE(Data.fInitialSpawnHeading[i])
				SAVE_STRING_TO_DEBUG_FILE("     BREAK")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				

			ENDREPEAT
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("----- END -------")
			SAVE_NEWLINE_TO_DEBUG_FILE()			
			
			Data.bOutputAll = FALSE			
		ENDIF
		
		// output data
		IF (Data.bOutputSelected)
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("----- SUBMARINE WARP POSITIONS -------")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
				
			SAVE_NEWLINE_TO_DEBUG_FILE()
			str = "CASE "
			str += Data.iSelectedArea
			SAVE_STRING_TO_DEBUG_FILE(str)
			
		
			REPEAT NUM_SUBMARINE_PRESET_WARP_AREA_SLOTS iSlot
				SAVE_NEWLINE_TO_DEBUG_FILE()
				str = "_SUBMARINE_WARP_INIT_AREA_SLOT(Data.Slots["
				str += iSlot
				str += "], "
				SAVE_STRING_TO_DEBUG_FILE(str)
				SAVE_VECTOR_TO_DEBUG_FILE(Data.vWorldPos[Data.iSelectedArea][iSlot])
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(Data.fWorldHeading[Data.iSelectedArea][iSlot])
				SAVE_STRING_TO_DEBUG_FILE(")")
			ENDREPEAT
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			str = "BREAK"
			SAVE_STRING_TO_DEBUG_FILE(str)

			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("----- END -------")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			Data.bOutputSelected = FALSE			
		ENDIF		
	
		// warp player to lead sub
		IF (Data.bWarpToLeadSub)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), Data.vWorldPos[Data.iSelectedArea][0], FALSE)
		ENDIF
	
	ELSE
		// cleanup 
		IF (Data.bInitialised)
			REPEAT NUMBER_OF_SUBMARINE_SPAWNS iSlot	
				IF DOES_ENTITY_EXIST(Data.VehicleID[iSlot])
					DELETE_VEHICLE(Data.VehicleID[iSlot])
				ENDIF
			ENDREPEAT			
			Data.bInitialised = FALSE	
		ENDIF
	ENDIF

ENDPROC

PROC OUTPUT_PTFX_DATA(SUBMARINE_CUTSCENE_DATA &Data)

	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("----- SUBMARINE FAST TRAVEL PTFX DATA -------")
	SAVE_NEWLINE_TO_DEBUG_FILE()	

	INT i
	TEXT_LABEL_63 str
	REPEAT NUM_SUB_PTFX i
	
		// vOffset
		SAVE_NEWLINE_TO_DEBUG_FILE()
		str = "Data.PtFx["
		str += i
		str += "].vOffset = "
		SAVE_STRING_TO_DEBUG_FILE(str)
		SAVE_VECTOR_TO_DEBUG_FILE(Data.PtFx[i].vOffset)		
		
		// vRotation
		SAVE_NEWLINE_TO_DEBUG_FILE()
		str = "Data.PtFx["
		str += i
		str += "].vRotation = "
		SAVE_STRING_TO_DEBUG_FILE(str)
		SAVE_VECTOR_TO_DEBUG_FILE(Data.PtFx[i].vRotation)			
		
	
	ENDREPEAT
	
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("----- END -- SUBMARINE FAST TRAVEL LIGHT DATA -------")
	SAVE_NEWLINE_TO_DEBUG_FILE()

ENDPROC

PROC OUTPUT_LIGHT_DATA(SUBMARINE_CUTSCENE_DATA &Data)

	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("----- SUBMARINE FAST TRAVEL LIGHT DATA -------")
	SAVE_NEWLINE_TO_DEBUG_FILE()

	INT i
	TEXT_LABEL_63 str
	REPEAT NUM_SUB_CUTSCENE_LIGHTS i
	
		// bActive
		SAVE_NEWLINE_TO_DEBUG_FILE()
		str = "Data.SubLight["
		str += i
		str += "].bActive = "
		IF (Data.SubLight[i].bActive)
			str += "TRUE"
		ELSE
			str += "FALSE"
		ENDIF
		SAVE_STRING_TO_DEBUG_FILE(str)
		
		// iType
		SAVE_NEWLINE_TO_DEBUG_FILE()
		str = "Data.SubLight["
		str += i
		str += "].iType = "
		SAVE_STRING_TO_DEBUG_FILE(str)
		SAVE_INT_TO_DEBUG_FILE(Data.SubLight[i].iType)
		
		// vOffset
		SAVE_NEWLINE_TO_DEBUG_FILE()
		str = "Data.SubLight["
		str += i
		str += "].vOffset = "
		SAVE_STRING_TO_DEBUG_FILE(str)
		SAVE_VECTOR_TO_DEBUG_FILE(Data.SubLight[i].vOffset)
		
		// vDirection
		SAVE_NEWLINE_TO_DEBUG_FILE()
		str = "Data.SubLight["
		str += i
		str += "].vDirection = "
		SAVE_STRING_TO_DEBUG_FILE(str)
		SAVE_VECTOR_TO_DEBUG_FILE(Data.SubLight[i].vDirection)		
		
		// r
		SAVE_NEWLINE_TO_DEBUG_FILE()
		str = "Data.SubLight["
		str += i
		str += "].r = "
		SAVE_STRING_TO_DEBUG_FILE(str)
		SAVE_INT_TO_DEBUG_FILE(Data.SubLight[i].r)			
		
		// g
		SAVE_NEWLINE_TO_DEBUG_FILE()
		str = "Data.SubLight["
		str += i
		str += "].g = "
		SAVE_STRING_TO_DEBUG_FILE(str)
		SAVE_INT_TO_DEBUG_FILE(Data.SubLight[i].g)			
		
		// b
		SAVE_NEWLINE_TO_DEBUG_FILE()
		str = "Data.SubLight["
		str += i
		str += "].b = "
		SAVE_STRING_TO_DEBUG_FILE(str)
		SAVE_INT_TO_DEBUG_FILE(Data.SubLight[i].b)			
		
		// Range
		SAVE_NEWLINE_TO_DEBUG_FILE()
		str = "Data.SubLight["
		str += i
		str += "].Range = "
		SAVE_STRING_TO_DEBUG_FILE(str)
		SAVE_FLOAT_TO_DEBUG_FILE(Data.SubLight[i].Range)			
		
		// Intensity
		SAVE_NEWLINE_TO_DEBUG_FILE()
		str = "Data.SubLight["
		str += i
		str += "].Intensity = "
		SAVE_STRING_TO_DEBUG_FILE(str)
		SAVE_FLOAT_TO_DEBUG_FILE(Data.SubLight[i].Intensity)			
		
		IF (Data.SubLight[i].iType > 0)
		
			// Falloff
			SAVE_NEWLINE_TO_DEBUG_FILE()
			str = "Data.SubLight["
			str += i
			str += "].Falloff = "
			SAVE_STRING_TO_DEBUG_FILE(str)
			SAVE_FLOAT_TO_DEBUG_FILE(Data.SubLight[i].Falloff)			
			
			IF (Data.SubLight[i].iType > 1)
				// InnerAngle
				SAVE_NEWLINE_TO_DEBUG_FILE()
				str = "Data.SubLight["
				str += i
				str += "].InnerAngle = "
				SAVE_STRING_TO_DEBUG_FILE(str)
				SAVE_FLOAT_TO_DEBUG_FILE(Data.SubLight[i].InnerAngle)		
				
				// OuterAngle
				SAVE_NEWLINE_TO_DEBUG_FILE()
				str = "Data.SubLight["
				str += i
				str += "].OuterAngle = "
				SAVE_STRING_TO_DEBUG_FILE(str)
				SAVE_FLOAT_TO_DEBUG_FILE(Data.SubLight[i].OuterAngle)		
				
				// Exp
				SAVE_NEWLINE_TO_DEBUG_FILE()
				str = "Data.SubLight["
				str += i
				str += "].Exp = "
				SAVE_STRING_TO_DEBUG_FILE(str)
				SAVE_FLOAT_TO_DEBUG_FILE(Data.SubLight[i].Exp)		
			ENDIF
			
		ENDIF
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
	
	ENDREPEAT
	
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("----- END -- SUBMARINE FAST TRAVEL LIGHT DATA -------")
	SAVE_NEWLINE_TO_DEBUG_FILE()	

ENDPROC

PROC _SUBMARINE_WARP_UPDATE_DEBUG(SUBMARINE_WARP_DATA &Data)


	IF (Data.bLaunchCutscene)
		VEHICLE_INDEX subId = GET_SUBMARINE_VEHICLE(PLAYER_ID())
		VECTOR vPos = GET_ENTITY_COORDS(subId)
		FLOAT fHeading = GET_ENTITY_HEADING(subId)
		IF SUBMARINE_CUTSCENE(Data.CutsceneData, subId, vPos, fHeading, Data.bIsDepartingCutscene)
			DO_SCREEN_FADE_IN(0)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			Data.bLaunchCutscene = FALSE	
		ENDIF
	ENDIF
	
	IF (Data.CutsceneData.bOutputLightData)
		OUTPUT_LIGHT_DATA(Data.CutsceneData)
		Data.CutsceneData.bOutputLightData = FALSE
	ENDIF

	IF (Data.CutsceneData.bOutputPtFxData)
		OUTPUT_PTFX_DATA(Data.CutsceneData)
		Data.CutsceneData.bOutputPtFxData = FALSE
	ENDIF
	
ENDPROC

#ENDIF


PROC SUBMARINE_WARP_INIT(SUBMARINE_WARP_DATA &Data)
	
	_SUBMARINE_WARP_INIT_AREAS(Data)
	
ENDPROC

PROC SUBMARINE_WARP_UPDATE(SUBMARINE_FAST_TRAVEL_MENU &FTMData, SUBMARINE_WARP_DATA &Data, SUBMARINE_HELM_DATA &HelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	
//	VEHICLE_INDEX subId = GET_SUBMARINE_VEHICLE(PLAYER_ID())
//	IF DOES_ENTITY_EXIST(subID)
//		VECTOR vec = GET_ENTITY_VELOCITY(subID)
//		PRINTLN("[SUB_WARP] vel =  ", vec, " vmag = ", VMAG(vec))
//	ENDIF
	
	_SUBMARINE_MAINTAIN_FAST_TRAVEL_MENU(FTMData, Data)
	IF IS_OWNER_OF_THIS_SUB()
		_SUBMARINE_MAINTAIN_WARP(FTMData, Data, HelmData)
	ELSE
		_MAINTAIN_CUTSCENE_FOR_NON_OWNER(Data, HelmData, serverBD)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		_SUBMARINE_WARP_UPDATE_DEBUG(Data)
	#ENDIF
ENDPROC

PROC SUBMARINE_WARP_CLEANUP(SUBMARINE_FAST_TRAVEL_MENU &FTMData, SUBMARINE_WARP_DATA &Data)
	// Cleanup transaction
	IF Data.iCashTransactionState != SUB_MOVE_TRANSACTION_INVALID
		IF IS_CASH_TRANSACTION_COMPLETE(Data.iCashTransactionId)
			DELETE_CASH_TRANSACTION(Data.iCashTransactionId)
		ELSE
			UPDATE_CASH_TRANSACTION_FLAGS(Data.iCashTransactionId, CTPF_AUTO_PROCESS_REPLY)
		ENDIF
	ENDIF
	
	_SUBMARINE_CLEAR_FAST_TRAVEL_MENU(FTMData)
ENDPROC

#ENDIF //FEATURE_HEIST_ISLAND
