//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        mp_submarine_weapons.sch																			//
// Description: This file implements the submarine's weapon systems          										//
// Written by:  Aaron Baumbach																						//
// Date:  		22/08/2020																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "mp_globals.sch"
USING "script_network.sch"
USING "context_control_public.sch"

#IF FEATURE_HEIST_ISLAND

CONST_INT SUB_HOMING_MISSILE_COOLDOWN		1000

STRUCT SUBMARINE_WEAPONS_DATA

	SCRIPT_TIMER homingMissileCooldownTimer
	MODEL_NAMES homingMissileModel

ENDSTRUCT

SUBMARINE_WEAPONS_DATA m_submarineWeaponsData

FUNC VECTOR GET_SUBMARINE_MISSILE_LAUNCHER_COORD()
	//Fire from camera location - TODO: Fire from the sides of sub 
	RETURN GET_FINAL_RENDERED_CAM_COORD() + <<0,0,1>>
ENDFUNC

FUNC VECTOR GET_SUBMARINE_MISSILE_LAUNCHER_DIR()
	//Fire straight up - TODO: Fire from the sides of sub 
	RETURN <<0,0,1>>
ENDFUNC

FUNC INT GET_SUBMARINE_MISSILE_DAMAGE()
	RETURN 100
ENDFUNC

//Returns true if weapon was fired successfully
FUNC BOOL SUBMARINE_FIRE_HOMING_MISSILE(VEHICLE_INDEX SubmarineVeh, ENTITY_INDEX TargetIndex)
	
	IF NOT IS_ENTITY_ALIVE(SubmarineVeh)
		RETURN FALSE
	ENDIF	
	
	IF NOT IS_ENTITY_ALIVE(TargetIndex)
		RETURN FALSE
	ENDIF	
	
	IF NOT HAS_MODEL_LOADED(m_submarineWeaponsData.homingMissileModel)
		PRINTLN("[SUB_WEAPONS][SUBMARINE_FIRE_HOMING_MISSILE] Waiting for model to load")
		RETURN FALSE
	ENDIF
	
	IF 	NOT HAS_NET_TIMER_STARTED(m_submarineWeaponsData.homingMissileCooldownTimer)
		OR HAS_NET_TIMER_EXPIRED(m_submarineWeaponsData.homingMissileCooldownTimer, SUB_HOMING_MISSILE_COOLDOWN)
		
		PRINTLN("[SUB_WEAPONS][SUBMARINE_FIRE_HOMING_MISSILE] TargetIndex = ", NATIVE_TO_INT(TargetIndex))
		
		//Kick off the cool down timer
		RESET_NET_TIMER(m_submarineWeaponsData.homingMissileCooldownTimer)
		START_NET_TIMER(m_submarineWeaponsData.homingMissileCooldownTimer)
		
		//Fire the missile!
		VECTOR vStartCoord 				= GET_SUBMARINE_MISSILE_LAUNCHER_COORD()
		VECTOR vEndPoint 				= vStartCoord + GET_SUBMARINE_MISSILE_LAUNCHER_DIR()
		INT iDamage 					= GET_SUBMARINE_MISSILE_DAMAGE()
		BOOL bPerfectAccuracy			= TRUE
		WEAPON_TYPE eWeaponType			= WEAPONTYPE_DLC_VEHICLE_SUB_MISSILE_HOMING
		BOOL bIgnoreCollisionResetNoBB 	= TRUE
		
		SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartCoord, vEndPoint, iDamage, bPerfectAccuracy, eWeaponType, 
																PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, SubmarineVeh, DEFAULT, DEFAULT, 
																TargetIndex, DEFAULT, DEFAULT, DEFAULT, bIgnoreCollisionResetNoBB)
		
		PLAY_SOUND_FROM_COORD(-1, "Fire", vStartCoord, "DLC_BTL_Terrobyte_Turret_Sounds", TRUE, 120, TRUE)
		
		RETURN TRUE
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SUBMARINE_WEAPONS_INIT()
	m_submarineWeaponsData.homingMissileModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_ex_vehiclemissile_1"))
	REQUEST_MODEL(m_submarineWeaponsData.homingMissileModel)
ENDPROC

PROC SUBMARINE_WEAPONS_UPDATE()

ENDPROC

PROC SUBMARINE_WEAPONS_CLEANUP()
	SET_MODEL_AS_NO_LONGER_NEEDED(m_submarineWeaponsData.homingMissileModel)
ENDPROC

#ENDIF //FEATURE_HEIST_ISLAND
