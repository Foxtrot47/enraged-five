//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        mp_submarine_helm.sch																			
/// Description: Header for manage helm functionality							
/// Written by:  Ata																						
/// Date:  		02/10/2020																							
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "mp_globals.sch"
USING "lineactivation.sch"
USING "script_debug.sch"
USING "context_control_public.sch"
USING "net_include.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
//USING "mp_submarine_warp.sch"

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════════╡ CONSTS ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
CONST_FLOAT cfSUBMARINE_HELM_HEADING_LEEWAY 										100.0
CONST_INT 	ciSUBMARINE_HELM_MAX_NUM_ANIM_CLIPS										6
CONST_INT 	ciSUBMARINE_HELM_PROMPT_REASON_TOO_MANY_PLAYERS							1
CONST_INT 	ciSUBMARINE_HELM_PROMPT_REASON_NOT_IN_LOCATE_AREA						2
CONST_INT	ciSUBMARINE_HELM_PROMPT_REASON_DONT_HAVE_ACCESS							3

// PLAYER_SUBMARINE_HELM_BD_DATA.iBS
CONST_INT PLAYER_SUBMARINE_HELM_BD_DATA_ENTERED_HELM_SEAT							0
CONST_INT PLAYER_SUBMARINE_HELM_BD_DATA_ENTERING_SUBMARINE_DRIVER_SEAT				1
CONST_INT PLAYER_SUBMARINE_HELM_BD_DATA_FINALIZE_CLONE_PED_HEAD_BLEND				2
CONST_INT PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_START_CS				3
CONST_INT PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_END_CS				4

// SUBMARINE_HELM_DATA.iBS
CONST_INT SUBMARINE_HELM_DATA_BS_TASK_GO_TO_HELM_COORD								0
CONST_INT SUBMARINE_HELM_DATA_BS_ENTERED_FROM_LEFT_SIDE								1
CONST_INT SUBMARINE_HELM_DATA_BS_ACTIVATE_AUTO_PILOT								2
CONST_INT SUBMARINE_HELM_DATA_BS_ENTERED_PERISCOPE									3
CONST_INT SUBMARINE_HELM_DATA_BS_TOO_MANY_PLAYER_AROUND_HELM_TEXT					4
CONST_INT SUBMARINE_HELM_DATA_BS_DISPLAYED_DRIVING_CONTROL_HELP						5
CONST_INT SUBMARINE_HELM_DATA_BS_DONT_HAVE_ACCESS_TO_HELM_TEXT						6
CONST_INT SUBMARINE_HELM_DATA_BS_DISPLAY_AUTO_PILOT_HELP							7
CONST_INT SUBMARINE_HELM_DATA_BS_TOGGLED_AUTO_PILOT									8
CONST_INT SUBMARINE_HELM_DATA_BS_IN_HELM_WITH_PC_CONTROLS							9


CONST_INT NUM_SUB_CUTSCENE_LIGHTS 4
CONST_INT NUM_SUB_PTFX 3

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════════╡ ENUMS ╞═════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
/// PURPOSE: 
///    States for the submarine helm
ENUM SUBMARINE_HELM_STATE
	SHS_UNINTITALIZED
	,SHS_INIT_NETWORK_OBJECTS
	,SHS_IDLE
	,SHS_PROMPT
	,SHS_GO_TO_ANIM_COORD
	,SHS_ON_ENTER
	,SHS_ON_UPDATE
	,SHS_ON_EXIT
ENDENUM

/// PURPOSE: Submarine helm options given to player when in helm seat
ENUM SUBMARINE_HELM_OPTION
	SHO_IDLE
	,SHO_DRIVE
	,SHO_FAST_TRAVEL
	,SHO_EXIT
ENDENUM

/// PURPOSE: Submarine helm drive option state
ENUM SUBMARINE_HELM_DRIVE_STATE
	SHDS_ON_ENTER_WARP_TO_VEH
	,SHDS_WARP_TO_VEH
	,SHDS_IN_VEH
	,SHDS_ON_ENTER_WARP_TO_HELM
	,SHDS_WARP_TO_HELM
ENDENUM

/// PURPOSE: Enum to keep track of helm animation clips
ENUM SUBMARIN_HELM_ANIM_CLIPS
	SHAC_ENTER_SEAT = 0
	,SHAC_BASE_SEAT
	,SHAC_EXIT_SEAT
	,SHAC_ENTER_COMPUTER
	,SHAC_IDLE_COMPUTER
	,SHAC_EXIT_COMPUTER
	
	// Update ciSUBMARINE_HELM_MAX_NUM_ANIM_CLIPS
ENDENUM

ENUM SUBMARINE_FAST_TRAVEL_MENU_STATE
	REBUILD,
	DISPLAY,
	HIDE
ENDENUM


ENUM SUBMARINE_WARP_STATE
	SWS_NONE,
	SWS_START,
	SWS_CUTSCENE_DEPART,
	SWS_WARP,
	SWS_CUTSCENE_ARRIVE,
	SWS_END
ENDENUM

/// PURPOSE:
///    Gets the debug name of the given submarine helm state  
DEBUGONLY FUNC STRING DEBUG_GET_SUBMARINE_HELM_STATE_AS_STRING(SUBMARINE_HELM_STATE eEnum)
	SWITCH eEnum
		CASE SHS_UNINTITALIZED			RETURN 	"SHS_UNINTITALIZED"
		CASE SHS_INIT_NETWORK_OBJECTS	RETURN 	"SHS_INIT_NETWORK_OBJECTS"
		CASE SHS_IDLE					RETURN	"SHS_IDLE"
		CASE SHS_PROMPT					RETURN	"SHS_PROMPT"
		CASE SHS_GO_TO_ANIM_COORD		RETURN 	"SHS_GO_TO_ANIM_COORD"
		CASE SHS_ON_ENTER				RETURN 	"SHS_ON_ENTER"
		CASE SHS_ON_UPDATE 				RETURN 	"SHS_ON_UPDATE"
		CASE SHS_ON_EXIT				RETURN 	"SHS_ON_EXIT"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_SUBMARINE_HELM_STATE_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the debug name of the given submarine helm option  
DEBUGONLY FUNC STRING DEBUG_GET_SUBMARINE_HELM_OPTION_AS_STRING(SUBMARINE_HELM_OPTION eEnum)
	SWITCH eEnum
		CASE SHO_IDLE			RETURN  "SHO_IDLE"
		CASE SHO_DRIVE			RETURN 	"SHO_DRIVE"
		CASE SHO_FAST_TRAVEL	RETURN	"SHO_FAST_TRAVEL"
		CASE SHO_EXIT			RETURN  "SHO_EXIT"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_SUBMARINE_HELM_OPTION_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the debug name of the given submarine helm drive state
DEBUGONLY FUNC STRING DEBUG_GET_SUBMARINE_HELM_DRIVE_STATE_AS_STRING(SUBMARINE_HELM_DRIVE_STATE eEnum)
	SWITCH eEnum
		CASE SHDS_ON_ENTER_WARP_TO_VEH		RETURN "SHDS_ON_ENTER_WARP_TO_VEH"
		CASE SHDS_WARP_TO_VEH				RETURN "SHDS_WARP_TO_VEH"
		CASE SHDS_IN_VEH					RETURN "SHDS_IN_VEH"	
		CASE SHDS_ON_ENTER_WARP_TO_HELM		RETURN "SHDS_ON_ENTER_WARP_TO_HELM"
		CASE SHDS_WARP_TO_HELM				RETURN "SHDS_WARP_TO_HELM"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_SUBMARINE_HELM_DRIVE_STATE_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the debug name submarine helm player broadcast data bit set as string 
DEBUGONLY FUNC STRING DEBUG_GET_SUBMARINE_PLAYER_BD_BS_AS_STRING(INT iBS)
	SWITCH iBS
		CASE PLAYER_SUBMARINE_HELM_BD_DATA_ENTERED_HELM_SEAT					RETURN "PLAYER_SUBMARINE_HELM_BD_DATA_ENTERED_HELM_SEAT"
		CASE PLAYER_SUBMARINE_HELM_BD_DATA_ENTERING_SUBMARINE_DRIVER_SEAT		RETURN "PLAYER_SUBMARINE_HELM_BD_DATA_ENTERING_SUBMARINE_DRIVER_SEAT"
		CASE PLAYER_SUBMARINE_HELM_BD_DATA_FINALIZE_CLONE_PED_HEAD_BLEND		RETURN "PLAYER_SUBMARINE_HELM_BD_DATA_FINALIZE_CLONE_PED_HEAD_BLEND"
		CASE PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_START_CS		RETURN "PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_START_CS"
		CASE PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_END_CS			RETURN "PLAYER_SUBMARINE_HELM_BD_DATA_TRIGGERED_FAST_TRAVEL_END_CS"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_SUBMARINE_PLAYER_BD_BS_AS_STRING - Missing name from lookup: ", iBS)
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the debug name submarine helm server broadcast data bit set as string 
DEBUGONLY FUNC STRING DEBUG_GET_SUBMARINE_SERVER_BD_BS_AS_STRING(INT iBS)
	SWITCH iBS
		CASE 0					RETURN "INVALID"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_SUBMARINE_SERVER_BD_BS_AS_STRING - Missing name from lookup: ", iBS)
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the debug name submarine helm local data bit set as string 
DEBUGONLY FUNC STRING DEBUG_GET_SUBMARINE_LOCAL_BS_AS_STRING(INT iBS)
	SWITCH iBS
		CASE SUBMARINE_HELM_DATA_BS_TASK_GO_TO_HELM_COORD				RETURN "SUBMARINE_HELM_DATA_BS_TASK_GO_TO_HELM_COORD"
		CASE SUBMARINE_HELM_DATA_BS_ENTERED_FROM_LEFT_SIDE				RETURN "SUBMARINE_HELM_DATA_BS_ENTERED_FROM_LEFT_SIDE"
		CASE SUBMARINE_HELM_DATA_BS_ACTIVATE_AUTO_PILOT					RETURN "SUBMARINE_HELM_DATA_BS_ACTIVATE_AUTO_PILOT"
		CASE SUBMARINE_HELM_DATA_BS_ENTERED_PERISCOPE					RETURN "SUBMARINE_HELM_DATA_BS_ENTERED_PERISCOPE"
		CASE SUBMARINE_HELM_DATA_BS_TOO_MANY_PLAYER_AROUND_HELM_TEXT	RETURN "SUBMARINE_HELM_DATA_BS_TOO_MANY_PLAYER_AROUND_HELM_TEXT"
		CASE SUBMARINE_HELM_DATA_BS_DISPLAYED_DRIVING_CONTROL_HELP		RETURN "SUBMARINE_HELM_DATA_BS_DISPLAYED_DRIVING_CONTROL_HELP"
		CASE SUBMARINE_HELM_DATA_BS_DONT_HAVE_ACCESS_TO_HELM_TEXT		RETURN "SUBMARINE_HELM_DATA_BS_DONT_HAVE_ACCESS_TO_HELM_TEXT"
		CASE SUBMARINE_HELM_DATA_BS_DISPLAY_AUTO_PILOT_HELP				RETURN "SUBMARINE_HELM_DATA_BS_DISPLAY_AUTO_PILOT_HELP"
		CASE SUBMARINE_HELM_DATA_BS_TOGGLED_AUTO_PILOT					RETURN "SUBMARINE_HELM_DATA_BS_TOGGLED_AUTO_PILOT"
		CASE SUBMARINE_HELM_DATA_BS_IN_HELM_WITH_PC_CONTROLS			RETURN "SUBMARINE_HELM_DATA_BS_IN_HELM_WITH_PC_CONTROLS"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_SUBMARINE_LOCAL_BS_AS_STRING - Missing name from lookup: ", iBS)
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Area check function pointer for checking if in the helm trigger area
TYPEDEF FUNC BOOL T_HELM_AREA_CHECK(PED_INDEX piPed, BOOL bLeftOnly)
/// PURPOSE:
///    Get submarine helm seat anim dic   
TYPEDEF FUNC STRING T_HELM_ANIM_DIC(BOOL bFemale)
/// PURPOSE:
///    Get submarine helm seat anim clips   
TYPEDEF PROC T_HELM_ANIM_CLIPS(TEXT_LABEL_23 &animClips[])
/// PURPOSE:
///    Get submarine vehicle index that we want to control   
TYPEDEF FUNC VEHICLE_INDEX T_SUBMARINE_HELM_VEHICLE_INDEX()

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DATA STRUCTURES ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
STRUCT SERVER_SUBMARINE_HELM_BD_DATA
	INT iplayerUsingHelm = -1
	INT iBS
	NETWORK_INDEX niChair
	NETWORK_INDEX niController
	NETWORK_INDEX niClonePed
ENDSTRUCT

STRUCT PLAYER_SUBMARINE_HELM_BD_DATA
	INT iBS
	
	VECTOR vFastTravelCoordsStart
	FLOAT fFastTravelHeadingStart
	
	VECTOR vFastTravelCoordsEnd
	FLOAT fFastTravelHeadingEnd	
	
	INT iSubColour
	INT iSubFlag
ENDSTRUCT

STRUCT SUBMARINE_HELM_DATA
	INT iContextIntention = NEW_CONTEXT_INTENTION
	INT iBS, iPlayerStagger 
	INT iSyncScene = -1
	INT iLeakSoundID = -1
	INT iLeakSoundState 
	
	VECTOR vHelmCoords
	FLOAT fHelmHeading
	
	// Enums
	SUBMARINE_HELM_STATE eState
	SUBMARINE_HELM_OPTION eHelmOption
	SUBMARINE_HELM_DRIVE_STATE eDriveState
	
	// Struct
	PLAYER_SUBMARINE_HELM_BD_DATA playerBD[NUM_NETWORK_PLAYERS]
	
	// Function pointers
	T_HELM_AREA_CHECK fpAreaCheck
	T_HELM_ANIM_DIC fpAnimDic
	T_HELM_ANIM_CLIPS fpAnimClips
	T_SUBMARINE_HELM_VEHICLE_INDEX fpVehicleIndex
	
	OBJECT_INDEX oiChair
	OBJECT_INDEX oiController
	PED_INDEX piClonePed
	
	SCRIPT_TIMER stWarpBackAnimTimer
	SCRIPT_TIMER stSafeTimer
	SCRIPT_CONTROL_HOLD_TIMER schtAutoPilotControl
ENDSTRUCT

STRUCT SUBMARINE_WARP_AREA_DATA
	TEXT_LABEL_15 Name
	INT Cost
ENDSTRUCT


STRUCT LIGHT_DATA
	VECTOR vOffset
	VECTOR vDirection
	INT r, g, b
	FLOAT Range
	FLOAT Intensity
	FLOAT InnerAngle
	FLOAT OuterAngle
	FLOAT Falloff
	FLOAT Exp
	INT iType
	BOOL bActive
ENDSTRUCT

STRUCT PTFX_DATA
	PTFX_ID Id
	VECTOR vOffset
	VECTOR vRotation
	//INT iBone
	TEXT_LABEL_63 strName
	TEXT_LABEL_63 strBoneName
	INT iSceneId = -1
	BOOL bLooped
	
	#IF IS_DEBUG_BUILD
	BOOL bRecreate
	#ENDIF

ENDSTRUCT

STRUCT SUBMARINE_CUTSCENE_DATA 
	
	BOOL bInitialised
	INT iSceneId
	INT iState
	INT iCalmId = -1
	FLOAT fCalmingQuadSize = 200.0
	FLOAT fScalarWaveValue = 0.02
	FLOAT fEndScenePhase = 1.0
	
	VEHICLE_SETUP_STRUCT_MP vehicleSetup
	
	VECTOR vLocation
	FLOAT fHeading
	FLOAT fZHeight = 0.0 
	
	CAMERA_INDEX CamId
	VEHICLE_INDEX CloneId
	
	INT iFlag = 0
	INT iColour = 0
	INTERIOR_INSTANCE_INDEX InteriorId
	INT iRoomKey
	
	LIGHT_DATA SubLight[NUM_SUB_CUTSCENE_LIGHTS]
	#IF IS_DEBUG_BUILD
	BOOL bRenderLights
	BOOL bPause
	BOOL bIsPaused
	BOOL bOutputLightData
	
	BOOL bRenderPtFx
	BOOL bOutputPtFxData
	#ENDIF
	
	PTFX_DATA PtFx[NUM_SUB_PTFX]
	
//	VECTOR vWaterOffset
//	FLOAT fWaterSpeed = 2.5
//	FLOAT fWaterRate = 0.05	
	
	INT iGameTimer
	
//	#IF IS_DEBUG_BUILD
//	BOOL bDrawWaterSphere
//	#ENDIF
		
ENDSTRUCT

STRUCT SUBMARINE_WARP_DATA
	
	SUBMARINE_WARP_AREA_DATA Areas[NUM_SUBMARINE_PRESET_WARP_AREAS]
	
	SUBMARINE_WARP_STATE State
	
	INT iFindPointState
	INT iTargetArea
	VECTOR TargetMidpoint
	SUBMARINE_WARP_AREA_SLOT_DATA TargetSlotData
	
	
	INT iCashTransactionState = SUB_MOVE_TRANSACTION_INVALID
	INT iCashTransactionId
	
	//FLOAT fDiveForce = -1.0
	
	SCRIPT_TIMER StateTimer
	
	SUBMARINE_CUTSCENE_DATA CutsceneData

	
	BOOL bWasDisplayingMenu
	
	#IF IS_DEBUG_BUILD
	BOOL bLaunchCutscene
	BOOL bIsDepartingCutscene
	#ENDIF
	
ENDSTRUCT

STRUCT SUBMARINE_FAST_TRAVEL_MENU
	BOOL bWantsToDisplay = FALSE
	BOOL bIsDiplaying = FALSE
	INT iCurrentItem
	TIME_DATATYPE timeScrollDelay
	FLOAT fMapZoom
	BLIP_INDEX MapSubBlip
	BOOL bConfirmedMove =FALSE
	BOOL bTriggerPavelDialogue = FALSE
ENDSTRUCT

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ GETTER / SETTER ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
/// PURPOSE:
///    Gets the current state of the submarine helm
FUNC SUBMARINE_HELM_STATE GET_SUBMARINE_HELM_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN sSubmarineHelmData.eState
ENDFUNC

/// PURPOSE:
///    Sets submarine helm main state
PROC SET_SUBMARINE_HELM_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SUBMARINE_HELM_STATE eState)
	PRINTLN("[SUBMARINE_HELM] SET_SUBMARINE_HELM_STATE - ", DEBUG_GET_SUBMARINE_HELM_STATE_AS_STRING(sSubmarineHelmData.eState), 
		" -> ", DEBUG_GET_SUBMARINE_HELM_STATE_AS_STRING(eState))
	sSubmarineHelmData.eState = eState
ENDPROC

/// PURPOSE:
///    Gets the current option of the submarine helm
FUNC SUBMARINE_HELM_OPTION GET_SUBMARINE_HELM_OPTION(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN sSubmarineHelmData.eHelmOption
ENDFUNC

/// PURPOSE:
///    Sets submarine helm option state e.g. drive, fast travel
PROC SET_SUBMARINE_HELM_OPTION(SUBMARINE_HELM_DATA &sSubmarineHelmData, SUBMARINE_HELM_OPTION eOption)
	PRINTLN("[SUBMARINE_HELM] SET_SUBMARINE_HELM_OPTION - ", DEBUG_GET_SUBMARINE_HELM_OPTION_AS_STRING(sSubmarineHelmData.eHelmOption), 
		" -> ", DEBUG_GET_SUBMARINE_HELM_OPTION_AS_STRING(eOption))
	sSubmarineHelmData.eHelmOption = eOption
ENDPROC

/// PURPOSE:
///    Gets the current drive state of the submarine helm
FUNC SUBMARINE_HELM_DRIVE_STATE GET_SUBMARINE_HELM_DRIVE_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN sSubmarineHelmData.eDriveState
ENDFUNC

/// PURPOSE:
///    Sets submarine helm drive state
PROC SET_SUBMARINE_HELM_DRIVE_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SUBMARINE_HELM_DRIVE_STATE eState)
	PRINTLN("[SUBMARINE_HELM] SET_SUBMARINE_HELM_DRIVE_STATE - ", DEBUG_GET_SUBMARINE_HELM_DRIVE_STATE_AS_STRING(sSubmarineHelmData.eDriveState), 
		" -> ", DEBUG_GET_SUBMARINE_HELM_DRIVE_STATE_AS_STRING(eState))
	sSubmarineHelmData.eDriveState = eState
ENDPROC

/// PURPOSE:
///    Sets the area check function pointer that is used to check if the player is in the helm activation area
PROC SET_SUBMARINE_HELM_AREA_CHECK(SUBMARINE_HELM_DATA &sSubmarineHelmData, T_HELM_AREA_CHECK fpAreaCheck)
	sSubmarineHelmData.fpAreaCheck = fpAreaCheck
ENDPROC

/// PURPOSE:
///    Sets the helm seat anim dic function pointer that is used to play seat animation
PROC SET_SUBMARINE_HELM_ANIM_DIC(SUBMARINE_HELM_DATA &sSubmarineHelmData, T_HELM_ANIM_DIC fpAnimDic)
	sSubmarineHelmData.fpAnimDic = fpAnimDic
ENDPROC

/// PURPOSE:
///    Sets the helm seat anim clips function pointer that is used to play seat animation
PROC SET_SUBMARINE_HELM_ANIM_CLIPS(SUBMARINE_HELM_DATA &sSubmarineHelmData, T_HELM_ANIM_CLIPS fpAnimClips)
	sSubmarineHelmData.fpAnimClips = fpAnimClips
ENDPROC

/// PURPOSE:
///    Sets the submarine helm vehicle index
PROC SET_SUBMARINE_HELM_VEHICLE_INDEX(SUBMARINE_HELM_DATA &sSubmarineHelmData, T_SUBMARINE_HELM_VEHICLE_INDEX fpVehicleIndex)
	sSubmarineHelmData.fpVehicleIndex = fpVehicleIndex
ENDPROC

/// PURPOSE:
///    Sets the helm position that will be used for heading checks
/// PARAMS:
///    vHelmCoords - the position of the helm the player must face to activate it
PROC SET_SUBMARINE_HELM_COORDS(SUBMARINE_HELM_DATA &sSubmarineHelmData, VECTOR vHelmCoords)
	sSubmarineHelmData.vHelmCoords = vHelmCoords
ENDPROC

/// PURPOSE:
///    Sets the helm heading that will be use to set prop headings
/// PARAMS:
///    fHelmHeading - the heading of the helm
PROC SET_SUBMARINE_HELM_HEADING(SUBMARINE_HELM_DATA &sSubmarineHelmData, FLOAT fHelmHeading)
	sSubmarineHelmData.fHelmHeading = fHelmHeading
ENDPROC

/// PURPOSE:
///    Sets submarine helm clone ped visibility
PROC SET_SUBMARINE_HELM_CLONE_PED_VISIBILITY_LOCALLY(SUBMARINE_HELM_DATA &sSubmarineHelmData, BOOL bVisible)
	IF NOT DOES_ENTITY_EXIST(sSubmarineHelmData.piClonePed)
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(sSubmarineHelmData.piClonePed)
		EXIT
	ENDIF	
	
	IF bVisible
		SET_ENTITY_LOCALLY_VISIBLE(sSubmarineHelmData.piClonePed)
	ELSE
		SET_ENTITY_LOCALLY_INVISIBLE(sSubmarineHelmData.piClonePed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets submarine helm user player visibility
PROC SET_SUBMARINE_HELM_USER_PLAYER_VISIBILITY_LOCALLY(PLAYER_INDEX playerTocheck, BOOL bVisible)
	IF NOT IS_NET_PLAYER_OK(playerTocheck)
		EXIT
	ENDIF
	
	IF bVisible
		SET_PLAYER_VISIBLE_LOCALLY(playerTocheck)
	ELSE
		SET_PLAYER_INVISIBLE_LOCALLY(playerTocheck)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets submarine helm player broadcast data bit set
/// PARAMS:
///    iBit - bit to set
///    bSet - true to set bit or false to clear
PROC SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(SUBMARINE_HELM_DATA &sSubmarineHelmData, INT iBit, BOOL bSet)
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		PRINTLN("[SUBMARINE_HELM] SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS invalid player id")
		EXIT
	ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(sSubmarineHelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			SET_BIT(sSubmarineHelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			PRINTLN("[SUBMARINE_HELM] SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS - ", DEBUG_GET_SUBMARINE_PLAYER_BD_BS_AS_STRING(iBit), " TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(sSubmarineHelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			CLEAR_BIT(sSubmarineHelmData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			PRINTLN("[SUBMARINE_HELM] SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS - ", DEBUG_GET_SUBMARINE_PLAYER_BD_BS_AS_STRING(iBit), " FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if submarine helm player broadcast data bit is set
/// PARAMS:
///    iBit - bit to set
FUNC BOOL IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET(SUBMARINE_HELM_DATA &sSubmarineHelmData, PLAYER_INDEX playerTocheck, INT iBit)
	IF playerTocheck = INVALID_PLAYER_INDEX()
		PRINTLN("[SUBMARINE_HELM] IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET invalid playerTocheck")
		RETURN FALSE
	ENDIF
	
	RETURN IS_BIT_SET(sSubmarineHelmData.playerBD[NATIVE_TO_INT(playerTocheck)].iBS, iBit)
ENDFUNC

/// PURPOSE:
///    Sets submarine helm local bit set
/// PARAMS:
///    iBit - bit to set
///    bSet - true to set bit or false to clear
PROC SET_SUBMARINE_HELM_LOCAL_DATA_BS(SUBMARINE_HELM_DATA &sSubmarineHelmData, INT iBit, BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(sSubmarineHelmData.iBS, iBit)
			SET_BIT(sSubmarineHelmData.iBS, iBit)
			PRINTLN("[SUBMARINE_HELM] SET_SUBMARINE_HELM_LOCAL_DATA_BS - ", DEBUG_GET_SUBMARINE_LOCAL_BS_AS_STRING(iBit), " TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(sSubmarineHelmData.iBS, iBit)
			CLEAR_BIT(sSubmarineHelmData.iBS, iBit)
			PRINTLN("[SUBMARINE_HELM] SET_SUBMARINE_HELM_LOCAL_DATA_BS - ", DEBUG_GET_SUBMARINE_LOCAL_BS_AS_STRING(iBit), " FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears all submarine helm bit sets
PROC CLEAR_ALL_SUBMARINE_HELM_LOCAL_DATA_BS(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	sSubmarineHelmData.iBS = 0
	PRINTLN("[SUBMARINE_HELM] CLEAR_ALL_SUBMARINE_HELM_LOCAL_DATA_BS")
ENDPROC

/// PURPOSE:
///    Check if submarine helm player broadcast data bit is set
/// PARAMS:
///    iBit - bit to set
FUNC BOOL IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(SUBMARINE_HELM_DATA &sSubmarineHelmData, INT iBit)
	RETURN IS_BIT_SET(sSubmarineHelmData.iBS, iBit)
ENDFUNC

PROC SET_SUBMARINE_HELM_PLAYER_CONTROL(BOOL bHasControl, NET_SET_PLAYER_CONTROL_FLAGS eFlags = 0, BOOL bForceSettings = FALSE)
	IF NOT IS_SKYSWOOP_AT_GROUND()
		EXIT
	ENDIF	
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), bHasControl, eFlags, bForceSettings)
ENDPROC

/// PURPOSE:
///    Sets the context intention ID that is used for the prompt
/// PARAMS:
///    iContextIntention - the context intention ID to use
PROC SET_SUBMARINE_HELM_CONTEXT_INTENTION(SUBMARINE_HELM_DATA &sSubmarineHelmData, INT iContextIntention)
	sSubmarineHelmData.iContextIntention = iContextIntention
ENDPROC

/// PURPOSE:
///    Gets the submarine helm trigger area check function pointer that is used to determine if a ped is in the activation area
FUNC T_HELM_AREA_CHECK GET_SUBMARINE_HELM_AREA_CHECK_FP(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN sSubmarineHelmData.fpAreaCheck
ENDFUNC

/// PURPOSE:
///    Gets the submarine helm seat anim dic
FUNC T_HELM_ANIM_DIC GET_SUBMARINE_HELM_ANIM_DIC_FP(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN sSubmarineHelmData.fpAnimDic
ENDFUNC

/// PURPOSE:
///    Gets the submarine helm seat anim clips
FUNC T_HELM_ANIM_CLIPS GET_SUBMARINE_HELM_ANIM_CLIPS_FP(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN sSubmarineHelmData.fpAnimClips
ENDFUNC

/// PURPOSE:
///    Gets the submarine helm vehicle index
FUNC T_SUBMARINE_HELM_VEHICLE_INDEX GET_SUBMARINE_HELM_VEHICLE_INDEX_FP(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN sSubmarineHelmData.fpVehicleIndex
ENDFUNC

/// PURPOSE:
///    Gets the submarine helm position which is used for heading checks
FUNC VECTOR GET_SUBMARINE_HELM_COORDS(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN sSubmarineHelmData.vHelmCoords
ENDFUNC

/// PURPOSE:
///    Gets the submarine helm heading 
FUNC FLOAT GET_SUBMARINE_HELM_HEADING(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN sSubmarineHelmData.fHelmHeading
ENDFUNC

/// PURPOSE:
///    Gets the submarine helm context intention ID that is used for displaying the activation prompt
FUNC INT GET_SUBMARINE_HELM_CONTEXT_INTENTION(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN sSubmarineHelmData.iContextIntention
ENDFUNC

/// PURPOSE:
///    Gets Submarin helm chair model name enum   
FUNC MODEL_NAMES GET_SUBMARINE_HELM_CHAIR_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("h4_int_lev_sub_chair_02"))
ENDFUNC

FUNC BOOL IS_THIS_SUBMARINE_HELM_CHAIR_MODEL(MODEL_NAMES modelToCheck)
	RETURN GET_SUBMARINE_HELM_CHAIR_MODEL() = modelToCheck
ENDFUNC

/// PURPOSE:
///    Gets Submarin helm controller model name enum   
FUNC MODEL_NAMES GET_SUBMARINE_HELM_CONTROLLER_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Console_01a"))
ENDFUNC

FUNC BOOL IS_THIS_SUBMARINE_HELM_CONTROLLER_MODEL(MODEL_NAMES modelToCheck)
	RETURN GET_SUBMARINE_HELM_CONTROLLER_MODEL() = modelToCheck
ENDFUNC

/// PURPOSE:
///    Checks if the submarine helm is currently showing its prompt
FUNC BOOL IS_SUBMARINE_HELM_PROMPT_ACTIVE(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN GET_SUBMARINE_HELM_CONTEXT_INTENTION(sSubmarineHelmData) != NEW_CONTEXT_INTENTION
ENDFUNC 


/// PURPOSE:
///    Submarine helm option help. Displayed when player is in helm seat.
FUNC STRING GET_SUBMARINE_HELM_OPTION_HELP()
	IF IS_OWNER_OF_THIS_SUB()
		RETURN "MP_SUBSEAT_1"
	ELSE
		RETURN "MP_SUBSEAT_1B"
	ENDIF
ENDFUNC	

/// PURPOSE:
///    Get helm seat anim dic according to the assigned anim dic function pointer
FUNC STRING GET_SUBMARINE_HELM_ANIM_DIC(SUBMARINE_HELM_DATA &sSubmarineHelmData, BOOL bFemale)
	T_HELM_ANIM_DIC fpAnimDic = GET_SUBMARINE_HELM_ANIM_DIC_FP(sSubmarineHelmData)
	
	IF fpAnimDic = NULL
		PRINTLN("[SUBMARINE_HELM] GET_SUBMARINE_HELM_ANIM_DIC - helm anim dic null")
		RETURN ""
	ENDIF

	RETURN CALL fpAnimDic(bFemale)
ENDFUNC

/// PURPOSE:
///    Get helm all seat anim clips according to the assigned anim clips function pointer
PROC GET_SUBMARINE_HELM_ANIM_CLIPS(SUBMARINE_HELM_DATA &sSubmarineHelmData, TEXT_LABEL_23 &animClips[])
	T_HELM_ANIM_CLIPS fpAnimClips = GET_SUBMARINE_HELM_ANIM_CLIPS_FP(sSubmarineHelmData)
	
	IF fpAnimClips = NULL
		PRINTLN("[SUBMARINE_HELM] GET_SUBMARINE_HELM_ANIM_DIC - helm anim clips null")
		EXIT
	ENDIF
	
	CALL fpAnimClips(animClips)
ENDPROC

/// PURPOSE:
///    Gets submarine helm anim clip using given enum
///    eAnimClip - Submarine helm anim clip enum 
FUNC TEXT_LABEL_23 GET_SUBMARINE_HELM_ANIM_CLIP_NAME(SUBMARINE_HELM_DATA &sSubmarineHelmData, SUBMARIN_HELM_ANIM_CLIPS eAnimClip)
	TEXT_LABEL_23 tlAnimClips[ciSUBMARINE_HELM_MAX_NUM_ANIM_CLIPS]
	GET_SUBMARINE_HELM_ANIM_CLIPS(sSubmarineHelmData, tlAnimClips)
	
	RETURN tlAnimClips[ENUM_TO_INT(eAnimClip)]
ENDFUNC	

/// PURPOSE:
///    Get helm seat anim clips according to the assigned anim dic function pointer
FUNC VEHICLE_INDEX GET_SUBMARINE_HELM_VEHICLE_INDEX(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	T_SUBMARINE_HELM_VEHICLE_INDEX fpVehicleIndex = GET_SUBMARINE_HELM_VEHICLE_INDEX_FP(sSubmarineHelmData)
	
	IF fpVehicleIndex = NULL
		PRINTLN("[SUBMARINE_HELM] GET_SUBMARINE_HELM_VEHICLE_INDEX - helm vehicle index null")
		RETURN NULL
	ENDIF
	
	RETURN CALL fpVehicleIndex()
ENDFUNC

/// PURPOSE:
///    Check if ped is running specific submarine helm anim 
/// PARAMS:
///    pedToCheck - Passing in player ped or clone ped
FUNC BOOL IS_SUBMARINE_HELM_PED_RUNNING_SEAT_ANIM(SUBMARINE_HELM_DATA &sSubmarineHelmData, PED_INDEX pedToCheck, SUBMARIN_HELM_ANIM_CLIPS eAnimClip)
	TEXT_LABEL_23 tlClip = GET_SUBMARINE_HELM_ANIM_CLIP_NAME(sSubmarineHelmData, eAnimClip)
	RETURN IS_ENTITY_PLAYING_ANIM(pedToCheck, GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, IS_PED_WEARING_HIGH_HEELS(pedToCheck)), tlClip)
ENDFUNC

/// PURPOSE:
///    Gets submarine help text
FUNC STRING GET_SUBMARINE_DRIVER_HELP(BOOL bDrivingControl = FALSE, BOOL bAutoPilot = FALSE, BOOL bSonar = FALSE)
	IF bDrivingControl
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			IF IS_PLAYER_SUBMARINE_SONAR_PURCHASED(g_OwnerOfSubmarinePropertyIAmIn)
			AND g_OwnerOfSubmarinePropertyIAmIn = PLAYER_ID() //only owner otherwise we need to update all other script to support it
				RETURN "BB_SUB_D_H3SPC"
			ELSE
				RETURN "BB_SUB_DRI_H3PC"
			ENDIF
		ELSE
			IF IS_PLAYER_SUBMARINE_SONAR_PURCHASED(g_OwnerOfSubmarinePropertyIAmIn)
			AND g_OwnerOfSubmarinePropertyIAmIn = PLAYER_ID()
				#IF IS_JAPANESE_BUILD
				IF IS_PLAYSTATION_PLATFORM()
					RETURN "BB_SUB_D_H3SJ"
				ENDIF
				#ENDIF
				
				RETURN "BB_SUB_D_H3S"
			ELSE
				#IF IS_JAPANESE_BUILD
				IF IS_PLAYSTATION_PLATFORM()
					RETURN "BB_SUB_DRI_H3J"
				ENDIF
				#ENDIF
				
				RETURN "BB_SUB_DRI_H3"
			ENDIF
		ENDIF
	ELIF bAutoPilot
		RETURN "BB_SUB_DRI_H4"
	ELIF bSonar
		RETURN "BB_SUB_DRI_H6"
	ENDIF

	IF NOT IS_PLAYER_SUBMARINE_SONAR_PURCHASED(g_OwnerOfSubmarinePropertyIAmIn)
		RETURN "BB_SUB_DRI_H1"
	ELIF g_OwnerOfSubmarinePropertyIAmIn = PLAYER_ID()
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			RETURN "BB_SUB_D_PC_H2"
		ELSE
			RETURN "BB_SUB_DRI_H2"
		ENDIF
	ENDIF

	RETURN ""
ENDFUNC

/// PURPOSE:
///    Check if local player has control off all submarine helm entities
/// PARAMS:
///    bIncludeClonePed - True also check clone ped 
FUNC BOOL HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES(SUBMARINE_HELM_DATA &sSubmarineHelmData, BOOL bIncludeClonePed)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sSubmarineHelmData.oiChair)
		PRINTLN("[SUBMARINE_HELM] HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES - oiChair doesn't exist")
		RETURN FALSE
	ENDIF
	
	IF NOT TAKE_CONTROL_OF_ENTITY(sSubmarineHelmData.oiChair)
		PRINTLN("[SUBMARINE_HELM] HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES - can't take control of chair")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sSubmarineHelmData.oiController)
		PRINTLN("[SUBMARINE_HELM] HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES - oiController doesn't exist")
		RETURN FALSE
	ENDIF
	
	IF NOT TAKE_CONTROL_OF_ENTITY(sSubmarineHelmData.oiController)
		PRINTLN("[SUBMARINE_HELM] HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES - can't takle control of controller")
		RETURN FALSE
	ENDIF
	
	IF bIncludeClonePed
		IF NOT DOES_ENTITY_EXIST(sSubmarineHelmData.piClonePed)
			PRINTLN("[SUBMARINE_HELM] HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES - clone ped doesn't exist")
			RETURN FALSE
		ENDIF
		
		IF NOT TAKE_CONTROL_OF_ENTITY(sSubmarineHelmData.piClonePed)
			PRINTLN("[SUBMARINE_HELM] HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES - can't take control of ped")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Set submarine helm prop can migrate state
///    Need to call every frame until return true
FUNC BOOL SET_SUBMARINE_HELM_PROP_CAN_MIGRATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD, BOOL canMigarte)
	IF HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES(sSubmarineHelmData, TRUE)
		SET_NETWORK_ID_CAN_MIGRATE(serverBD.niChair, canMigarte)
		SET_NETWORK_ID_CAN_MIGRATE(serverBD.niController, canMigarte)
		SET_NETWORK_ID_CAN_MIGRATE(serverBD.niClonePed, canMigarte)
		PRINTLN("[SUBMARINE_HELM] SET_SUBMARINE_HELM_PROP_CAN_MIGRATE - ", GET_STRING_FROM_BOOL(canMigarte))
		RETURN TRUE
	ENDIF	
		
	RETURN FALSE	
ENDFUNC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ INITIALISE ╞═══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
/// PURPOSE:
///    Initilize submarine helm data. This must be called in init state of script in order to subarmine helm to work
/// PARAMS:
///    sSubmarineHelmData - submarine helm data
///    fpAreaCheck - are check function point to determine if player is in helm area
///    fpAnimDic - seat animation dictionary
///    fpAnimClips - seat animation clips
///    fpVehicleIndex - vehicle index of submarine we want to control
///    vHelmCoords - helm coords
///    fHeading - helm heading
PROC SUBMARINE_HELM_INIT(SUBMARINE_HELM_DATA &sSubmarineHelmData
						, T_HELM_AREA_CHECK fpAreaCheck
						, T_HELM_ANIM_DIC fpAnimDic
						, T_HELM_ANIM_CLIPS fpAnimClips
						, T_SUBMARINE_HELM_VEHICLE_INDEX fpVehicleIndex
						, VECTOR vHelmCoords
						, FLOAT fHeading)
						
	SET_SUBMARINE_HELM_AREA_CHECK(sSubmarineHelmData, fpAreaCheck)
	SET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, fpAnimDic)
	SET_SUBMARINE_HELM_ANIM_CLIPS(sSubmarineHelmData, fpAnimClips)
	SET_SUBMARINE_HELM_VEHICLE_INDEX(sSubmarineHelmData, fpVehicleIndex)
	SET_SUBMARINE_HELM_COORDS(sSubmarineHelmData, vHelmCoords)
	SET_SUBMARINE_HELM_HEADING(sSubmarineHelmData, fHeading)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SET_SUBMARINE_HELM_STATE(sSubmarineHelmData, SHS_INIT_NETWORK_OBJECTS)
	ELSE
		SET_SUBMARINE_HELM_STATE(sSubmarineHelmData, SHS_IDLE)
	ENDIF	
	
	SUBMARINE_HELM_ACCESS_INIT()
	PRINTLN("[SUBMARINE_HELM] INIT - Finished")
ENDPROC

/// PURPOSE:
///    Register submarine helm player broadcast data
/// PARAMS:
///    ref_local - submarine helm player broadcast data struct
PROC SUBMARINE_HELM_REGISTER_PLAYER_BROADCAST_DATA(SUBMARINE_HELM_DATA &ref_local)
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(ref_local.playerBD, SIZE_OF(ref_local.playerBD))
	PRINTLN("[SUBMARINE_HELM] SUBMARINE_HELM_REGISTER_PLAYER_BROADCAST_DATA - Registered broadcast data")
ENDPROC

/// PURPOSE:
///    Register submarine helm server broadcast data
/// PARAMS:
///    ref_local - submarine helm server broadcast data struct
PROC SUBMARINE_HELM_REGISTER_SERVER_BROADCAST_DATA(SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	PRINTLN("[SUBMARINE_HELM] SUBMARINE_HELM_REGISTER_PLAYER_BROADCAST_DATA - Registered broadcast data")
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ MAIN LOGIC PROC ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛


/// PURPOSE:
///    Manages the access restrictions for the submarine helm, set by the submarine owner in the interaction menu. 
FUNC BOOL CAN_PLAYER_ACCESS_SUBMARINE_HELM(PLAYER_INDEX PlayerID)
	IF IS_NET_PLAYER_OK(PlayerID)
	AND NOT IS_PLAYER_DEAD(PlayerID)
		// ----- Owner -----
		IF (PlayerID = g_OwnerOfSubmarinePropertyIAmIn)
			PRINTLN("[CAN_PLAYER_ACCESS_SUBMARINE_HELM] I am the owner! Returning TRUE")
			RETURN TRUE
		ENDIF
		
		// ----- Everyone -----
		IF IS_SUBMARINE_HELM_ACCESS_EVERYONE(g_OwnerOfSubmarinePropertyIAmIn)
			PRINTLN("[CAN_PLAYER_ACCESS_SUBMARINE_HELM] Everyone can enter! Returning TRUE")
			RETURN TRUE
		ENDIF
		
		// ----- Crew -----
		IF IS_SUBMARINE_HELM_ACCESS_CREW_ONLY(g_OwnerOfSubmarinePropertyIAmIn)
			IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(PlayerID, g_OwnerOfSubmarinePropertyIAmIn)
				PRINTLN("[CAN_PLAYER_ACCESS_SUBMARINE_HELM] I am not in the owners active clan. Returning FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
		
		// ----- Friend -----
		IF IS_SUBMARINE_HELM_ACCESS_FRIENDS_ONLY(g_OwnerOfSubmarinePropertyIAmIn)
			IF NOT IS_PLAYER_FRIEND(g_OwnerOfSubmarinePropertyIAmIn)
				PRINTLN("[CAN_PLAYER_ACCESS_SUBMARINE_HELM] I am not friends with the owner. Returning FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
		
		// ----- Crew + Friends -----
		IF IS_SUBMARINE_HELM_ACCESS_CREW_AND_FRIENDS_ONLY(g_OwnerOfSubmarinePropertyIAmIn)
			IF NOT IS_PLAYER_FRIEND(g_OwnerOfSubmarinePropertyIAmIn)
			AND NOT GB_ARE_PLAYERS_IN_SAME_GANG(PlayerID, g_OwnerOfSubmarinePropertyIAmIn)
				PRINTLN("[CAN_PLAYER_ACCESS_SUBMARINE_HELM] I'm not friends or in a crew with the owner. Returning FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
		
		// ----- No one shall pass -----
		IF IS_SUBMARINE_HELM_ACCESS_NO_ONE(g_OwnerOfSubmarinePropertyIAmIn)
			PRINTLN("[CAN_PLAYER_ACCESS_SUBMARINE_HELM] No one can enter! Returning FALSE")
			RETURN FALSE
		ENDIF
		
		PRINTLN("[CAN_PLAYER_ACCESS_SUBMARINE_HELM] I have access! Returning TRUE!")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[CAN_PLAYER_ACCESS_SUBMARINE_HELM] Returning FALSE for player: ", NATIVE_TO_INT(PlayerID))
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Creates the submarine helm help prompt that asks the player to press a button to sit in helm seat
///    this will also clear any active help text so we instantly get the prompt
PROC CREATE_SUBMARINE_HELM_PROMPT(SUBMARINE_HELM_DATA &sSubmarineHelmData, STRING strLabel)	
	CLEAR_HELP()
	REGISTER_CONTEXT_INTENTION(sSubmarineHelmData.iContextIntention, CP_MAXIMUM_PRIORITY, strLabel)
	PRINTLN("[SUBMARINE_HELM] CREATE_PROMPT - Prompt created")
ENDPROC

/// PURPOSE:
///    Removes the submarine helm help text prompt
PROC CLEAR_SUBMARINE_HELM_PROMPT(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RELEASE_CONTEXT_INTENTION(sSubmarineHelmData.iContextIntention)
	SET_SUBMARINE_HELM_CONTEXT_INTENTION(sSubmarineHelmData, NEW_CONTEXT_INTENTION)
	PRINTLN("[SUBMARINE_HELM] CLEAR_PROMPT - Prompt cleared")
ENDPROC

/// PURPOSE:
///    Checks if the current activation prompt has been activated signalling that we should sit in helm seat
/// RETURNS:
///    TRUE if the prompt has been accepted
FUNC BOOL  HAS_SUBMARINE_HELM_PROMPT_BEEN_ACCEPTED(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	RETURN HAS_CONTEXT_BUTTON_TRIGGERED(sSubmarineHelmData.iContextIntention)
ENDFUNC

/// PURPOSE:
///    Checks if the given ped is in the trigger area according to the assigned area check function pointer
/// RETURNS:
///    TRUE if in the area OR FALSE if not in the area or the area check is not assigned
FUNC BOOL IS_PED_IN_SUBMARINE_HELM_TRIGGER_AREA(SUBMARINE_HELM_DATA &sSubmarineHelmData, PED_INDEX piPed, BOOL bLeftOnly)
	T_HELM_AREA_CHECK fpAreaCheck = GET_SUBMARINE_HELM_AREA_CHECK_FP(sSubmarineHelmData)
	
	IF fpAreaCheck = NULL
		PRINTLN("[SUBMARINE_HELM] IS_PED_IN_TRIGGER_AREA - No trigger area set")
		RETURN FALSE
	ENDIF
	
	RETURN CALL fpAreaCheck(piPed, bLeftOnly)
ENDFUNC

/// PURPOSE:
///    Checks to see if the given ped is facing the direction of the submarine helm seat according to its assigned helm position
///    this has a leeway defined by cfSUBMARINE_HELM_HEADING_LEEWAY
/// RETURNS:
///    TRUE if the given ped is facing the rack
FUNC BOOL IS_PED_FACING_SUBMARINE_HELM(SUBMARINE_HELM_DATA &sSubmarineHelmData, PED_INDEX piPed)
	VECTOR vTargetHeadingCoord = GET_ENTITY_COORDS(sSubmarineHelmData.oiController)
	VECTOR vPedCoord = GET_ENTITY_COORDS(piPed)
	FLOAT fPedHeading = GET_ENTITY_HEADING(piPed)
	FLOAT fDirHeading = GET_HEADING_BETWEEN_VECTORS_2D(vPedCoord, vTargetHeadingCoord)
	RETURN IS_HEADING_ACCEPTABLE_CORRECTED(fDirHeading, fPedHeading, cfSUBMARINE_HELM_HEADING_LEEWAY)
ENDFUNC

/// PURPOSE:
///    Check if player is alone submarine helm area
FUNC BOOL IS_PLAYER_ALONE_IN_SUBMARINE_HELM_AREA(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))

			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			IF IS_NET_PLAYER_OK(playerID)
			AND NOT NETWORK_IS_PLAYER_CONCEALED(playerID)
				IF playerID <> PLAYER_ID()
					IF IS_PED_IN_SUBMARINE_HELM_TRIGGER_AREA(sSubmarineHelmData, GET_PLAYER_PED(playerID), FALSE)
					OR IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET(sSubmarineHelmData, playerID, PLAYER_SUBMARINE_HELM_BD_DATA_ENTERED_HELM_SEAT)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDREPEAT

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Check if player is in the helm seat
FUNC BOOL IS_SUBMARINE_BEING_DRIVEN_BY_ANY_REMOTE_PLAYER(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))

			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			IF IS_NET_PLAYER_OK(playerID)
			AND NOT NETWORK_IS_PLAYER_CONCEALED(playerID)
				IF playerID != PLAYER_ID()
					IF IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET(sSubmarineHelmData, playerID, PLAYER_SUBMARINE_HELM_BD_DATA_ENTERED_HELM_SEAT)
					AND IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID), TRUE)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDREPEAT

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the local player should recevie the prompt, based on if they are in the trigger area
///    and facing the helm seat
/// RETURNS:
///    TRUE if the local player is facing the helm and in its trigger area
FUNC BOOL SHOULD_SHOW_SUBMARINE_HELM_PROMPT(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD, INT &iReason)
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	
	IF IS_PHONE_ONSCREEN()
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		RETURN FALSE
	ENDIF
	
	IF IS_WARNING_MESSAGE_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_SYSTEM_UI_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	IF serverBD.iplayerUsingHelm != -1
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_SUBMARINE_HELM_TRIGGER_AREA(sSubmarineHelmData, piPlayerPed, FALSE) 
		iReason = ciSUBMARINE_HELM_PROMPT_REASON_NOT_IN_LOCATE_AREA
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_FACING_SUBMARINE_HELM(sSubmarineHelmData, piPlayerPed)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_ALONE_IN_SUBMARINE_HELM_AREA(sSubmarineHelmData)
		iReason = ciSUBMARINE_HELM_PROMPT_REASON_TOO_MANY_PLAYERS
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		RETURN FALSE
	ENDIF
	
	IF HAS_OWNER_EMPTIED_SUBMARINE_HELM(g_OwnerOfSubmarinePropertyIAmIn)
		RETURN FALSE
	ENDIF
	
	IF g_bBustedWarpInProgress
		PRINTLN("[SUBMARINE_HELM] SHOULD_SHOW_SUBMARINE_HELM_PROMPT g_bBustedWarpInProgress true")
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_PLAYER_ACCESS_SUBMARINE_HELM(PLAYER_ID())
		PRINTLN("[SUBMARINE_HELM] SHOULD_SHOW_SUBMARINE_HELM_PROMPT Dont have access by owner. Returning FALSE")
		iReason = ciSUBMARINE_HELM_PROMPT_REASON_DONT_HAVE_ACCESS
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Create Submarine helm objects for player interaction
/// PARAMS:
///    objectModel - prop model name
PROC CREATE_SUBMARINE_HELM_OBJECTS(SUBMARINE_HELM_DATA &sSubmarineHelmData, NETWORK_INDEX &niObject, MODEL_NAMES objectModel)
	
	REQUEST_ANIM_DICT(GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, FALSE))
	IF NOT HAS_ANIM_DICT_LOADED(GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, FALSE))
		EXIT
	ENDIF
	
	IF NOT REQUEST_LOAD_MODEL(objectModel)
		EXIT
	ENDIF
	
	IF NOT CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
		PRINTLN("[SUBMARINE_HELM] CREATE_SUBMARINE_HELM_OBJECTS CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT false")
		EXIT
	ENDIF
	
	RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
	
	IF NOT CAN_REGISTER_MISSION_OBJECTS(1)
		PRINTLN("[SUBMARINE_HELM] CREATE_SUBMARINE_HELM_OBJECTS CAN_REGISTER_MISSION_OBJECTS false")
		EXIT
	ENDIF

	TEXT_LABEL_23 tlEnterClip = GET_SUBMARINE_HELM_ANIM_CLIP_NAME(sSubmarineHelmData, SHAC_ENTER_SEAT)
	TEXT_LABEL_23 tlClipSufix
	
	IF IS_THIS_SUBMARINE_HELM_CHAIR_MODEL(objectModel)
		tlClipSufix = "_chair"
	ELSE
		tlClipSufix = "_console"
	ENDIF
	
	tlEnterClip += tlClipSufix
	
	FLOAT fHelmHeading = GET_SUBMARINE_HELM_HEADING(sSubmarineHelmData)
	VECTOR vHelmCoords = GET_SUBMARINE_HELM_COORDS(sSubmarineHelmData)
	VECTOR tempPosition = GET_ANIM_INITIAL_OFFSET_POSITION(GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, FALSE), tlEnterClip, vHelmCoords, <<0, 0, fHelmHeading>>)
	VECTOR tempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, FALSE), tlEnterClip, vHelmCoords, <<0, 0, fHelmHeading>>)

	niObject = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(objectModel, tempPosition, DEFAULT, TRUE))									
	SET_ENTITY_ROTATION(NET_TO_OBJ(niObject), tempRotation)
	SET_ENTITY_INVINCIBLE(NET_TO_OBJ(niObject), TRUE)
	FREEZE_ENTITY_POSITION(NET_TO_OBJ(niObject), TRUE)
	SET_ENTITY_MIRROR_REFLECTION_FLAG(NET_TO_OBJ(niObject), FALSE)
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(niObject), TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(objectModel)
	PRINTLN("[SUBMARINE_HELM] CREATE_SUBMARINE_HELM_OBJECTS: created prop = ", GET_MODEL_NAME_FOR_DEBUG(objectModel), ", position = ", tempPosition)
ENDPROC

/// PURPOSE:
///    Gets the submarine helm animation clip end phase 
FUNC FLOAT GET_SUBMARINE_HELM_SYNCHRONISED_SCENE_END_PHASE(SUBMARIN_HELM_ANIM_CLIPS eAnimClip)
	SWITCH eAnimClip
		CASE SHAC_EXIT_COMPUTER		RETURN  0.85
	ENDSWITCH

	RETURN 0.95
ENDFUNC 

/// PURPOSE:
///    Check is usbmarine helm synchronised scene is finished
FUNC BOOL HAS_SUBMARINE_HELM_SYNCHRONISED_SCENE_FINISHED(SUBMARINE_HELM_DATA &sSubmarineHelmData, SUBMARIN_HELM_ANIM_CLIPS eAnimClip)
	INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSubmarineHelmData.iSyncScene)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
	AND GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= GET_SUBMARINE_HELM_SYNCHRONISED_SCENE_END_PHASE(eAnimClip)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check is usbmarine helm synchronised scene phase
FUNC FLOAT GET_SUBMARINE_HELM_SYNCHRONIZED_SCENE_PHASE(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sSubmarineHelmData.iSyncScene)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
		RETURN GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID)
	ENDIF
	RETURN 0.0
ENDFUNC

/// PURPOSE:
///    Start submarine helm seat synchronised scene depending on helm state
PROC PERFORM_SUBMARINE_HELM_SYNCHRONISED_SCENE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SUBMARIN_HELM_ANIM_CLIPS eAnimClip, SERVER_SUBMARINE_HELM_BD_DATA &serverBD, BOOL bLoopAnim = FALSE, BOOL bHoldLastFrame = FALSE)
	FLOAT fStartPhase = 0.0

	TEXT_LABEL_23 tlPedClip
	TEXT_LABEL_23 tlControlerClip
	TEXT_LABEL_23 tlChairClip

	TEXT_LABEL_23 tlClip = GET_SUBMARINE_HELM_ANIM_CLIP_NAME(sSubmarineHelmData, eAnimClip)
	
	tlPedClip 		= tlClip
	tlControlerClip = tlClip
	tlChairClip 	= tlClip
	
	IF IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_ENTERED_FROM_LEFT_SIDE)
		IF eAnimClip = SHAC_ENTER_SEAT
		OR eAnimClip = SHAC_EXIT_SEAT
			tlPedClip 		+= "_left"
			tlControlerClip += "_left"
			tlChairClip 	+= "_left"
		ENDIF	
	ENDIF
	
	SYNCED_SCENE_PLAYBACK_FLAGS eFlag = SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT
	
	IF bLoopAnim
		eFlag = SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_DONT_INTERRUPT
	ENDIF
	
	FLOAT fBlendIn 		= SLOW_BLEND_IN
	FLOAT fBlendOut 	= SLOW_BLEND_OUT
	
	sSubmarineHelmData.iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_SUBMARINE_HELM_COORDS(sSubmarineHelmData) , <<0, 0, GET_SUBMARINE_HELM_HEADING(sSubmarineHelmData)>>
																	, DEFAULT, bHoldLastFrame, bLoopAnim, DEFAULT, fStartPhase)
	
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sSubmarineHelmData.iSyncScene, GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())), tlPedClip, fBlendIn, fBlendOut, eFlag)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niClonePed)
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(sSubmarineHelmData.piClonePed, sSubmarineHelmData.iSyncScene
											, GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, IS_PED_WEARING_HIGH_HEELS(sSubmarineHelmData.piClonePed))
											, tlPedClip, fBlendIn, fBlendOut, eFlag)
	ENDIF
	
	tlChairClip += "_chair"
	NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(sSubmarineHelmData.oiChair, sSubmarineHelmData.iSyncScene, GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID()))
											, tlChairClip, fBlendIn, fBlendOut, eFlag)
	
	tlControlerClip += "_console"
	NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(sSubmarineHelmData.oiController, sSubmarineHelmData.iSyncScene, GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID()))
											, tlControlerClip, fBlendIn, fBlendOut, eFlag)
	
	NETWORK_START_SYNCHRONISED_SCENE(sSubmarineHelmData.iSyncScene)
	PRINTLN("[SUBMARINE_HELM] - PERFORM_SUBMARINE_HELM_SYNCHRONISED_SCENE tlPedClip: ", tlPedClip, " tlChairClip: ", tlChairClip, " tlControlelrClip: ", tlControlerClip)
ENDPROC

/// PURPOSE:
///    Cleans up the submarine helm releasing all assets
PROC SUBMARINE_SUBMARINE_HELM_CLEANUP(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)	
	// Cleanup context intention if active
	IF IS_SUBMARINE_HELM_PROMPT_ACTIVE(sSubmarineHelmData)
		CLEAR_SUBMARINE_HELM_PROMPT(sSubmarineHelmData)
	ENDIF
	
	SET_SUBMARINE_HELM_PROP_CAN_MIGRATE(sSubmarineHelmData, serverBD, TRUE)
	SET_INTERIOR_STATE_TO_PILOT_VEHICLE_OUTSIDE_INTERIOR(FALSE)
	SET_PLAYER_IN_SUBMARINE_DRIVER_SEAT(FALSE)
	FORCE_SUBMARINE_DRIVER_TO_HELM_SEAT(FALSE)
	SET_PLAYER_LEAVING_SUBMARINE_DRIVER_SEAT(FALSE)
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	REMOVE_ANIM_DICT(GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, TRUE))
	REMOVE_ANIM_DICT(GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, FALSE))
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_SUBMARINE_HELM_OPTION_HELP())
		CLEAR_HELP()
	ENDIF
	
	// Do complete struct clear
	SUBMARINE_HELM_DATA sEmpty
	sSubmarineHelmData = sEmpty
	PRINTLN("[SUBMARINE_HELM] CLEANUP - Finished")
ENDPROC

/// PURPOSE:
///    Maintain submarine helm idle state
PROC SUBMARINE_HELM_INIT_NETWORK_OBJECTS_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SET_SUBMARINE_HELM_STATE(sSubmarineHelmData, SHS_IDLE)
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niChair)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niController)
		SET_SUBMARINE_HELM_STATE(sSubmarineHelmData, SHS_IDLE)
		EXIT
	ENDIF	
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niChair)
		CREATE_SUBMARINE_HELM_OBJECTS(sSubmarineHelmData, serverBD.niChair, GET_SUBMARINE_HELM_CHAIR_MODEL())
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niController)
		CREATE_SUBMARINE_HELM_OBJECTS(sSubmarineHelmData, serverBD.niController, GET_SUBMARINE_HELM_CONTROLLER_MODEL())
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if we should display help text when subamrine helm prompt is blocked
PROC MAINTAIN_SUBMARINE_HELM_PROMPT_BLOCK_REASON(SUBMARINE_HELM_DATA &sSubmarineHelmData, INT iReason)
	IF iReason = ciSUBMARINE_HELM_PROMPT_REASON_TOO_MANY_PLAYERS
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_TOO_MANY")
			CLEAR_HELP()
			PRINT_HELP("POD_TOO_MANY")
			SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_TOO_MANY_PLAYER_AROUND_HELM_TEXT, TRUE)
		ENDIF
	ELIF iReason = ciSUBMARINE_HELM_PROMPT_REASON_DONT_HAVE_ACCESS
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELM_NO_ACCESS")
			CLEAR_HELP()
			PRINT_HELP("HELM_NO_ACCESS")
			SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_DONT_HAVE_ACCESS_TO_HELM_TEXT, TRUE)
		ENDIF
	ELSE
		IF iReason != ciSUBMARINE_HELM_PROMPT_REASON_NOT_IN_LOCATE_AREA
			IF IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_TOO_MANY_PLAYER_AROUND_HELM_TEXT)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_TOO_MANY")
					CLEAR_HELP()
				ENDIF
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_TOO_MANY_PLAYER_AROUND_HELM_TEXT, FALSE)
			ENDIF	
		ENDIF
		
		IF iReason != ciSUBMARINE_HELM_PROMPT_REASON_DONT_HAVE_ACCESS
			IF IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_DONT_HAVE_ACCESS_TO_HELM_TEXT)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELM_NO_ACCESS")
					CLEAR_HELP()
				ENDIF
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_DONT_HAVE_ACCESS_TO_HELM_TEXT, FALSE)
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain submarine helm idle state
PROC SUBMARINE_HELM_IDLE_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	INT iReason
	IF NOT SHOULD_SHOW_SUBMARINE_HELM_PROMPT(sSubmarineHelmData, serverBD, iReason)
		MAINTAIN_SUBMARINE_HELM_PROMPT_BLOCK_REASON(sSubmarineHelmData, iReason)
		EXIT
	ENDIF
	
	CLEAR_HELP()
	CREATE_SUBMARINE_HELM_PROMPT(sSubmarineHelmData, "MPJAC_SIT")
	SET_SUBMARINE_HELM_STATE(sSubmarineHelmData, SHS_PROMPT)
ENDPROC

/// PURPOSE:
///    Maintain submarine helm prompt state
PROC SUBMARINE_HELM_PROMPT_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	INT iReason
	IF NOT SHOULD_SHOW_SUBMARINE_HELM_PROMPT(sSubmarineHelmData, serverBD, iReason)
		CLEAR_SUBMARINE_HELM_PROMPT(sSubmarineHelmData)
		SET_SUBMARINE_HELM_STATE(sSubmarineHelmData, SHS_IDLE)
		EXIT
	ENDIF
	
	IF HAS_SUBMARINE_HELM_PROMPT_BEEN_ACCEPTED(sSubmarineHelmData)
		SET_SUBMARINE_HELM_PLAYER_CONTROL(FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		CLEAR_SUBMARINE_HELM_PROMPT(sSubmarineHelmData)
		SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(sSubmarineHelmData, PLAYER_SUBMARINE_HELM_BD_DATA_ENTERED_HELM_SEAT, TRUE)
		
		IF IS_PED_IN_SUBMARINE_HELM_TRIGGER_AREA(sSubmarineHelmData, PLAYER_PED_ID(), TRUE) 
			SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_ENTERED_FROM_LEFT_SIDE, TRUE)
		ENDIF
		
		DISABLE_INTERACTION_MENU()
		REQUEST_ANIM_DICT(GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, TRUE))
		REQUEST_ANIM_DICT(GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, FALSE))
		
		SET_SUBMARINE_HELM_STATE(sSubmarineHelmData, SHS_GO_TO_ANIM_COORD)
	ENDIF
ENDPROC

/// PURPOSE:
///    Disable player collision with submarine helm entities while sync scene is active
PROC DISBALE_PLAYER_COLLISION_WITH_SUBMARINE_HELM_ENTITES(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	IF DOES_ENTITY_EXIST(sSubmarineHelmData.oiChair)
		SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), sSubmarineHelmData.oiChair, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSubmarineHelmData.oiController)
		SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), sSubmarineHelmData.oiController, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSubmarineHelmData.piClonePed)
		SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), sSubmarineHelmData.piClonePed, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Disable gameplay camera collision with submarine helm entities while sync scene is active
PROC DISBALE_GAMEPLAY_CAM_COLLISION_WITH_SUBMARINE_HELM_ENTITES(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	IF DOES_ENTITY_EXIST(sSubmarineHelmData.oiChair)
		SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(sSubmarineHelmData.oiChair)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSubmarineHelmData.oiController)
		SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(sSubmarineHelmData.oiController)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSubmarineHelmData.piClonePed)
		SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(sSubmarineHelmData.piClonePed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Invalidate cinematice idle camera when submarine helm is active
PROC INVALIDATE_IDLE_CAM_WHILE_SUBMARINE_HELM_ACTIVE()
	INVALIDATE_IDLE_CAM()
ENDPROC	

/// PURPOSE:
///    All functionality which needs switched off should added in this function
///    This function called every frame when state > SHS_PROMPT
PROC DISABLE_UNNECESSARY_PLAYER_FUNCTINALITY_THIS_FRAME(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	IF NOT IS_PLAYER_IN_SUBMARINE_DRIVER_SEAT()
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		DISABLE_SELECTOR_THIS_FRAME()
		HIDE_PLAYER_HEAD_THIS_FRAME(PLAYER_PED_ID(), TRUE)
		HIDE_PLAYER_HEAD_THIS_FRAME(sSubmarineHelmData.piClonePed, TRUE)
	ENDIF
	
	DISBALE_PLAYER_COLLISION_WITH_SUBMARINE_HELM_ENTITES(sSubmarineHelmData)
	DISBALE_GAMEPLAY_CAM_COLLISION_WITH_SUBMARINE_HELM_ENTITES(sSubmarineHelmData)
	INVALIDATE_IDLE_CAM_WHILE_SUBMARINE_HELM_ACTIVE()
	SET_PED_CAPSULE(PLAYER_PED_ID(), 0.15)

	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE) = CAM_VIEW_MODE_FIRST_PERSON
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	ENDIF	
ENDPROC

/// PURPOSE:
///    Maintain task player go to enter animation coord
PROC SUBMARINE_HELM_GO_TO_ANIM_COORD_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF NOT HAS_ANIM_DICT_LOADED(GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())))
		EXIT
	ENDIF	
	
	TEXT_LABEL_23 tlEnterClip = GET_SUBMARINE_HELM_ANIM_CLIP_NAME(sSubmarineHelmData, SHAC_ENTER_SEAT)
	
	IF IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_ENTERED_FROM_LEFT_SIDE)
		tlEnterClip += "_left"
	ENDIF	
	
	FLOAT fHelmHeading = GET_SUBMARINE_HELM_HEADING(sSubmarineHelmData)
	VECTOR vHelmCoords = GET_SUBMARINE_HELM_COORDS(sSubmarineHelmData)
	VECTOR vHelmTriggerPos 	= GET_ANIM_INITIAL_OFFSET_POSITION(GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())), tlEnterClip, vHelmCoords, <<0, 0, fHelmHeading>>, 0.0)  
	VECTOR vHelmTriggerRot	= GET_ANIM_INITIAL_OFFSET_ROTATION(GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())), tlEnterClip, vHelmCoords, <<0, 0, fHelmHeading>>, 0.0)  
	
	DISABLE_UNNECESSARY_PLAYER_FUNCTINALITY_THIS_FRAME(sSubmarineHelmData)
	HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES(sSubmarineHelmData, FALSE)
	
	IF IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_TASK_GO_TO_HELM_COORD)
		IF HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES(sSubmarineHelmData, FALSE)
			IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK)
			OR ARE_VECTORS_ALMOST_EQUAL(GET_PLAYER_COORDS(PLAYER_ID()), vHelmTriggerPos, 0.01)
				PERFORM_SUBMARINE_HELM_SYNCHRONISED_SCENE(sSubmarineHelmData, SHAC_ENTER_SEAT, serverBD, FALSE, TRUE)
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_TASK_GO_TO_HELM_COORD, FALSE)
				SET_SUBMARINE_HELM_STATE(sSubmarineHelmData, SHS_ON_ENTER)
			ENDIF	
		ENDIF
		EXIT
	ENDIF
	
	VECTOR vPlayerCoord = GET_PLAYER_COORDS(PLAYER_ID())
	
	IF !ARE_VECTORS_ALMOST_EQUAL(GET_PLAYER_COORDS(PLAYER_ID()), vHelmTriggerPos, 0.2)
		TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), <<vHelmTriggerPos.x, vHelmTriggerPos.y, vPlayerCoord.z>>, PEDMOVEBLENDRATIO_WALK, 4000, vHelmTriggerRot.z, 0.2)
	ELSE
		TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), vHelmTriggerRot.z)
	ENDIF	
	
	SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_TASK_GO_TO_HELM_COORD, TRUE)
ENDPROC

/// PURPOSE:
///    Submarine helm on enter state. Checks if enter animation is done and starts the idle animation.
PROC SUBMARINE_HELM_ON_ENTER_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	DISABLE_UNNECESSARY_PLAYER_FUNCTINALITY_THIS_FRAME(sSubmarineHelmData)
	// Calling every frame to make sure we grab all entity control before next anim starts
	HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES(sSubmarineHelmData, TRUE)
	
	IF HAS_SUBMARINE_HELM_SYNCHRONISED_SCENE_FINISHED(sSubmarineHelmData, SHAC_ENTER_SEAT)
	AND HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES(sSubmarineHelmData, TRUE)
		SET_SUBMARINE_HELM_PLAYER_CONTROL(TRUE)
		PERFORM_SUBMARINE_HELM_SYNCHRONISED_SCENE(sSubmarineHelmData, SHAC_BASE_SEAT, serverBD, TRUE)
		SET_SUBMARINE_HELM_STATE(sSubmarineHelmData, SHS_ON_UPDATE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if it is safe to display helm options
FUNC BOOL IS_SAFE_TO_DISPLAY_SUBMARINE_HELM_IDLE_OPTIONS()
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_SYSTEM_UI_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF	
	
	IF IS_WARNING_MESSAGE_ACTIVE()
		RETURN FALSE
	ENDIF	
	
	IF IS_BROWSER_OPEN()
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		RETURN FALSE
	ENDIF	
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		RETURN FALSE
	ENDIF	
	
	IF g_bBustedWarpInProgress
		PRINTLN("[SUBMARINE_HELM] IS_SAFE_TO_DISPLAY_SUBMARINE_HELM_IDLE_OPTIONS g_bBustedWarpInProgress true")
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		RETURN FALSE
	ENDIF	
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Check if player should get kicked out of helm seat
FUNC BOOL SHOULD_KICK_PLAYER_FROM_SUBMARINE_HELM_SEAT()
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
		RETURN TRUE
	ENDIF	
	
	IF HAS_OWNER_EMPTIED_SUBMARINE_HELM(g_OwnerOfSubmarinePropertyIAmIn)
		RETURN TRUE
	ENDIF
	
	IF NOT CAN_PLAYER_ACCESS_SUBMARINE_HELM(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if we should bail submarine helm drive mode 
///    bCheckPlayerControl - check if player press exit button to leave driver seat
FUNC BOOL SHOULD_BAIL_SUBMARINE_HELM_DRIVE(SUBMARINE_HELM_DATA &sSubmarineHelmData, BOOL bCheckPlayerControl)
	IF NOT IS_ENTITY_ALIVE(GET_SUBMARINE_HELM_VEHICLE_INDEX(sSubmarineHelmData))
		PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - submarine not alive")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - IS_PLAYER_IN_CORONA")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - IS_SKYSWOOP_AT_GROUND false")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_KICK_PILOT_OUT_OF_INTERIOR()
		PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - SHOULD_KICK_PILOT_OUT_OF_INTERIOR")
		RETURN TRUE
	ENDIF	

	IF HAS_OWNER_EMPTIED_SUBMARINE_HELM(g_OwnerOfSubmarinePropertyIAmIn)
		PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - HAS_OWNER_EMPTIED_SUBMARINE_HELM")
		RETURN TRUE
	ENDIF
	
	IF NOT CAN_PLAYER_ACCESS_SUBMARINE_HELM(PLAYER_ID())
		PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - CAN_PLAYER_ACCESS_SUBMARINE_HELM")
		RETURN TRUE
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
		PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - IS_TRANSITION_ACTIVE")
		RETURN TRUE
	ENDIF
	
	IF IS_DOING_SUBMARINE_FAST_TRAVEL()
		PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - IS_DOING_SUBMARINE_FAST_TRAVEL")
		RETURN TRUE
	ENDIF
	
	IF IS_CUTSCENE_ACTIVE() 
		PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - IS_CUTSCENE_ACTIVE")
		RETURN TRUE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - IS_CUTSCENE_PLAYING")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_FORCE_SUBMARINE_DRIVER_TO_HELM_SEAT()
		PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - SHOULD_FORCE_SUBMARINE_DRIVER_TO_HELM_SEAT")
		RETURN TRUE
	ENDIF
	
	// Keep this at the end of this function
	IF bCheckPlayerControl
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
			PRINTLN("[SUBMARINE_HELM] SHOULD_BAIL_SUBMARINE_HELM_DRIVE - INPUT_VEH_EXIT pressed")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Submarine helm idle option state. Displays help and checks which option playre chosen
PROC MAINTAIN_SUBMARINE_HELM_IDLE_OPTION(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD, SUBMARINE_FAST_TRAVEL_MENU &FTMData)
	IF NOT IS_SAFE_TO_DISPLAY_SUBMARINE_HELM_IDLE_OPTIONS()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_SUBMARINE_HELM_OPTION_HELP())
			CLEAR_HELP()
		ENDIF
		EXIT
	ENDIF	
	
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_SUBMARINE_HELM_OPTION_HELP())
		CLEAR_HELP()
		PRINT_HELP_FOREVER(GET_SUBMARINE_HELM_OPTION_HELP())
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_IN_HELM_WITH_PC_CONTROLS, TRUE)
		ENDIF
	ELSE
		IF IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_IN_HELM_WITH_PC_CONTROLS)
			IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_IN_HELM_WITH_PC_CONTROLS, FALSE)
				CLEAR_HELP()
			ENDIF
		ELSE	
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_IN_HELM_WITH_PC_CONTROLS, TRUE)
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_KICK_PLAYER_FROM_SUBMARINE_HELM_SEAT()
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF	
		SET_SUBMARINE_HELM_OPTION(sSubmarineHelmData, SHO_EXIT)
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) 
	AND NOT SHOULD_BAIL_SUBMARINE_HELM_DRIVE(sSubmarineHelmData, FALSE)
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF	
		
		DO_SCREEN_FADE_OUT(500)
		SET_SUBMARINE_HELM_PLAYER_CONTROL(FALSE)
		PERFORM_SUBMARINE_HELM_SYNCHRONISED_SCENE(sSubmarineHelmData, SHAC_ENTER_COMPUTER, serverBD, FALSE, TRUE)
		SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(sSubmarineHelmData, PLAYER_SUBMARINE_HELM_BD_DATA_ENTERING_SUBMARINE_DRIVER_SEAT, TRUE)
		SET_SUBMARINE_HELM_OPTION(sSubmarineHelmData, SHO_DRIVE)
	ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X) AND IS_OWNER_OF_THIS_SUB())
		IF NOT (MPGlobalsAmbience.bSubFastTravelDisabled)
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF	
			FTMData.bWantsToDisplay = TRUE
			SET_SUBMARINE_HELM_OPTION(sSubmarineHelmData, SHO_FAST_TRAVEL)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Submarine helm exit option state. If exit input pressed, play exit animation and kick player out of seat.
PROC MAINTAIN_SUBMARINE_HELM_EXIT_OPTION(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	PERFORM_SUBMARINE_HELM_SYNCHRONISED_SCENE(sSubmarineHelmData, SHAC_EXIT_SEAT, serverBD)
	SET_SUBMARINE_HELM_OPTION(sSubmarineHelmData, SHO_IDLE)
	SET_SUBMARINE_HELM_STATE(sSubmarineHelmData, SHS_ON_EXIT)
ENDPROC

/// PURPOSE:
///    Submarine helm drive init state. Initilize flags, interior state before warpping to drive seat
PROC MAINTAIN_SUBMARINE_DRIVE_ON_ENTER_WARP_TO_VEH_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	SET_SUBMARINE_HELM_PROP_CAN_MIGRATE(sSubmarineHelmData, serverBD, FALSE)

	IF NOT HAS_SUBMARINE_HELM_SYNCHRONISED_SCENE_FINISHED(sSubmarineHelmData, SHAC_ENTER_COMPUTER)
		EXIT
	ENDIF

	PERFORM_SUBMARINE_HELM_SYNCHRONISED_SCENE(sSubmarineHelmData, SHAC_IDLE_COMPUTER, serverBD, TRUE)
	SET_INTERIOR_STATE_TO_PILOT_VEHICLE_OUTSIDE_INTERIOR(TRUE)
	SET_SUBMARINE_HELM_DRIVE_STATE(sSubmarineHelmData, SHDS_WARP_TO_VEH)
ENDPROC

/// PURPOSE:
///    Displays submarine halm driver help text every time we enter the drive seat
PROC DISPLAY_SUBMARINE_HELM_DRIVE_HELP_TEXT(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_HELP_MESSAGE_ON_SCREEN()	
		CLEAR_HELP()
	ENDIF

	BOOL bDrivingControl = FALSE
	IF NOT IS_BIT_SET(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SUBMARINE_HELM_CONTROL_HELP)
		INT iHintHelpCount = GET_PACKED_STAT_INT(PACKED_MP_INT_KOSATKA_CONTROL_HELP)
		IF iHintHelpCount < 4
			iHintHelpCount++
			bDrivingControl = TRUE
			SET_PACKED_STAT_INT(PACKED_MP_INT_KOSATKA_CONTROL_HELP, iHintHelpCount)
			SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SUBMARINE_HELM_CONTROL_HELP)
			SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_DISPLAYED_DRIVING_CONTROL_HELP, TRUE)
		ELSE
			SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SUBMARINE_HELM_CONTROL_HELP)
		ENDIF			
	ENDIF

	PRINT_HELP(GET_SUBMARINE_DRIVER_HELP(bDrivingControl))	
ENDPROC

/// PURPOSE:
///    Maintain submarine helm drive, warp to vehicle state. Check if player is ready and warp to driver seat
PROC MAINTAIN_SUBMARINE_HELM_DRIVE_WARP_TO_VEH_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF NOT SET_SUBMARINE_HELM_PROP_CAN_MIGRATE(sSubmarineHelmData, serverBD, FALSE)
		EXIT
	ENDIF
	
	IF SHOULD_BAIL_SUBMARINE_HELM_DRIVE(sSubmarineHelmData, FALSE)
	AND NOT IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID())
		FORCE_SIMPLE_INTERIOR_BLIPS_UPDATE()
		IF NOT SHOULD_KICK_PILOT_OUT_OF_INTERIOR()
			DO_SCREEN_FADE_IN(500)
		ENDIF	
		SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(sSubmarineHelmData, PLAYER_SUBMARINE_HELM_BD_DATA_ENTERING_SUBMARINE_DRIVER_SEAT, FALSE)
		SET_SUBMARINE_HELM_PLAYER_CONTROL(TRUE)
		SET_PLAYER_IN_SUBMARINE_DRIVER_SEAT(FALSE)
		SET_MINIMAP_BLOCK_WAYPOINT(TRUE)
		IF IS_WAYPOINT_ACTIVE()
			SET_WAYPOINT_OFF()
		ENDIF
		FORCE_SUBMARINE_DRIVER_TO_HELM_SEAT(FALSE)
		SET_INTERIOR_STATE_TO_PILOT_VEHICLE_OUTSIDE_INTERIOR(FALSE)
		SET_SUBMARINE_HELM_PROP_CAN_MIGRATE(sSubmarineHelmData, serverBD, TRUE)
		PERFORM_SUBMARINE_HELM_SYNCHRONISED_SCENE(sSubmarineHelmData, SHAC_EXIT_COMPUTER, serverBD, FALSE, TRUE)
		SET_SUBMARINE_HELM_DRIVE_STATE(sSubmarineHelmData, SHDS_ON_ENTER_WARP_TO_VEH)
		SET_SUBMARINE_HELM_OPTION(sSubmarineHelmData, SHO_IDLE)
		EXIT
	ENDIF

	IF NOT IS_SCREEN_FADED_OUT()
		EXIT
	ENDIF
	
	VEHICLE_INDEX submarineVeh = GET_SUBMARINE_HELM_VEHICLE_INDEX(sSubmarineHelmData)
	
	IF NOT TAKE_CONTROL_OF_ENTITY(submarineVeh)
		EXIT
	ENDIF

	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), submarineVeh)
		SET_PLAYER_IN_SUBMARINE_DRIVER_SEAT(TRUE)
		SET_MINIMAP_BLOCK_WAYPOINT(FALSE)
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)

		IF g_sMPTunables.IH_RADIO_ENABLE_KULT_FM
			START_VEHICLE_AND_TURN_ON_RADIO(submarineVeh, "RADIO_34_DLC_HEI4_KULT")
		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
		DO_SCREEN_FADE_IN(500)
		ENABLE_INTERACTION_MENU()
		SET_SUBMARINE_HELM_PLAYER_CONTROL(TRUE)
		DISPLAY_SUBMARINE_HELM_DRIVE_HELP_TEXT(sSubmarineHelmData)
		SET_SUBMARINE_HELM_DRIVE_STATE(sSubmarineHelmData, SHDS_IN_VEH)
		EXIT
	ENDIF
	
	CLEAR_SUBMARINE_USED_WEAPON_DATA()
	WarpPlayerIntoCar(PLAYER_PED_ID(), GET_SUBMARINE_HELM_VEHICLE_INDEX(sSubmarineHelmData), VS_DRIVER)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, FALSE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_FiresDummyRockets, TRUE)
	RESET_NET_TIMER(sSubmarineHelmData.stSafeTimer)
ENDPROC

FUNC BOOL HAS_PLAYER_PRESSED_SUBAMRINE_AUTOPILOT(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	sSubmarineHelmData.schtAutoPilotControl.control = FRONTEND_CONTROL
	sSubmarineHelmData.schtAutoPilotControl.action = INPUT_CONTEXT
	RETURN IS_CONTROL_HELD(sSubmarineHelmData.schtAutoPilotControl)
ENDFUNC

FUNC BOOL HAS_PLAYER_PRESSED_ENTER_PERISCOPE()
	RETURN IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CONTEXT)
ENDFUNC

/// PURPOSE:
///    Activate submarine auto pilot mode
PROC ACTIVATE_SUBMARINE_HELM_AUTOPILOT(VEHICLE_INDEX submarineVeh, BOOL bPrintTicker)
	VECTOR vTargetCoord = GET_ENTITY_COORDS(submarineVeh)

	// Max value 2147483647 = 24H
	FORCE_SUBMARINE_NEUTRAL_BUOYANCY(submarineVeh, 2147483647)
	TASK_SUBMARINE_GOTO_AND_STOP(NULL , submarineVeh, vTargetCoord, TRUE)
	SET_VEHICLE_DONT_TERMINATE_TASK_WHEN_ACHIEVED(submarineVeh)
	SET_DISABLE_SUPERDUMMY(submarineVeh, TRUE)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(submarineVeh, TRUE)
	
	IF bPrintTicker
		PRINT_TICKER("SUB_AUTO_AC")
	ENDIF
	
	PRINTLN("[SUBMARINE_HELM] ACTIVATE_SUBMARINE_HELM_AUTOPILOT")
ENDPROC

/// PURPOSE:
///    Dectivate submarine auto pilot mode
PROC DEACTIVATE_SUBMARINE_HELM_AUTOPILOT(VEHICLE_INDEX submarineVeh)
	CLEAR_PRIMARY_VEHICLE_TASK(submarineVeh)
	CLEAR_DEFAULT_PRIMARY_TASK(PLAYER_PED_ID())
	SET_DISABLE_SUPERDUMMY(submarineVeh, FALSE)
	FORCE_SUBMARINE_NEUTRAL_BUOYANCY(submarineVeh, 0)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(submarineVeh, FALSE)
	PRINT_TICKER("SUB_AUTO_DEAC")
	PRINTLN("[SUBMARINE_HELM] DEACTIVATE_SUBMARINE_HELM_AUTOPILOT")
ENDPROC

/// PURPOSE:
///    Check if we should enable / disbale autopilot 
PROC MAINTAIN_SUBMARINE_HELM_DRIVE_AUTO_PILOT(SUBMARINE_HELM_DATA &sSubmarineHelmData, VEHICLE_INDEX submarineVeh)
	IF HAS_PLAYER_PRESSED_SUBAMRINE_AUTOPILOT(sSubmarineHelmData)
		IF NOT IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_TOGGLED_AUTO_PILOT)
			IF NOT IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_ACTIVATE_AUTO_PILOT)
				ACTIVATE_SUBMARINE_HELM_AUTOPILOT(submarineVeh, TRUE)
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_TOGGLED_AUTO_PILOT, TRUE)
				EXIT
			ELSE
				DEACTIVATE_SUBMARINE_HELM_AUTOPILOT(submarineVeh)
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_ACTIVATE_AUTO_PILOT, FALSE)
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_DISPLAY_AUTO_PILOT_HELP, FALSE)
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_TOGGLED_AUTO_PILOT, TRUE)
				EXIT
			ENDIF
		ENDIF
	ELSE
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
			SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_TOGGLED_AUTO_PILOT, FALSE)	
		ENDIF
	ENDIF
	
	IF GET_ACTIVE_VEHICLE_MISSION_TYPE(submarineVeh) = MISSION_GOTO
		SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_ACTIVATE_AUTO_PILOT, TRUE)
		IF NOT IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_DISPLAY_AUTO_PILOT_HELP)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_ON_SCREEN()
				PRINT_HELP("BB_SUB_DRI_H5")
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_DISPLAY_AUTO_PILOT_HELP, TRUE)
			ENDIF	
		ENDIF	
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BB_SUB_DRI_H5")
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Manage entering & Exiting the periscope while driving the submarine.
PROC MAINTAIN_SUBMARINE_HELM_PERISCOPE(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	IF HAS_PLAYER_PRESSED_ENTER_PERISCOPE()
	AND NOT IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_TOGGLED_AUTO_PILOT)
		SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_ENTERED_PERISCOPE, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if we need to display submarine persicope help
///    This should only display if we display driving control first and skiped persicope help
PROC MAINTAIN_SUBMARINE_PERSICOPE_HELP(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	IF IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_DISPLAYED_DRIVING_CONTROL_HELP)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_HELP_MESSAGE_ON_SCREEN()	
			PRINT_HELP(GET_SUBMARINE_DRIVER_HELP(FALSE))	
			SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_DISPLAYED_DRIVING_CONTROL_HELP, FALSE)
		ENDIF
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_SUBMARINE_DRIVER_HELP(FALSE))
		IF IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_IN_HELM_WITH_PC_CONTROLS)
			IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_IN_HELM_WITH_PC_CONTROLS, FALSE)
				CLEAR_HELP()
				PRINT_HELP(GET_SUBMARINE_DRIVER_HELP(FALSE))	
			ENDIF
		ELSE	
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				SET_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_IN_HELM_WITH_PC_CONTROLS, TRUE)
				CLEAR_HELP()
				PRINT_HELP(GET_SUBMARINE_DRIVER_HELP(FALSE))	
			ENDIF
		ENDIF
	ENDIF	
ENDPROC


/// PURPOSE:
///    Check if we need to display submarine auto pilot help
PROC MAINTAIN_SUBMARINE_AUTOPILOT_HELP(SUBMARINE_HELM_DATA &sSubmarineHelmData, VEHICLE_INDEX submarineVeh)
	IF NOT IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_DISPLAYED_DRIVING_CONTROL_HELP)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_HELP_MESSAGE_ON_SCREEN()	
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SUBMARINE_HELM_AUTOPILOT_HELP)
			AND IS_BIT_SET(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SUBMARINE_SONAR_AUTOPILOT_HELP)
				IF NOT IS_ENTITY_SUBMERGED_IN_WATER(submarineVeh, TRUE, 0.9)
					EXIT
				ENDIF
				
				INT iHintHelpCount = GET_PACKED_STAT_INT(PACKED_MP_INT_KOSATKA_AUTOPILOT_HELP)
				IF iHintHelpCount < 4
					iHintHelpCount++
					SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SUBMARINE_HELM_AUTOPILOT_HELP)
					SET_PACKED_STAT_INT(PACKED_MP_INT_KOSATKA_AUTOPILOT_HELP, iHintHelpCount)
					PRINT_HELP(GET_SUBMARINE_DRIVER_HELP(FALSE, TRUE))	
				ELSE
					SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SUBMARINE_HELM_AUTOPILOT_HELP)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Display sonar help under water if we havn't already
PROC MAINTAIN_SUBMARINE_SONAR_HELP(SUBMARINE_HELM_DATA &sSubmarineHelmData, VEHICLE_INDEX submarineVeh)
	IF NOT IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_DISPLAYED_DRIVING_CONTROL_HELP)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_HELP_MESSAGE_ON_SCREEN()	
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SUBMARINE_HELM_AUTOPILOT_HELP)
			AND NOT IS_BIT_SET(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SUBMARINE_SONAR_AUTOPILOT_HELP)
				IF NOT IS_ENTITY_SUBMERGED_IN_WATER(submarineVeh, TRUE, 0.9)
					EXIT
				ENDIF
				
				INT iHintHelpCount = GET_PACKED_STAT_INT(PACKED_MP_INT_KOSATKA_SONAR_HELP)
				IF iHintHelpCount < 4
					iHintHelpCount++
					SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SUBMARINE_SONAR_AUTOPILOT_HELP)
					SET_PACKED_STAT_INT(PACKED_MP_INT_KOSATKA_SONAR_HELP, iHintHelpCount)
					PRINT_HELP(GET_SUBMARINE_DRIVER_HELP(FALSE, FALSE, TRUE))	
				ELSE
					SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SUBMARINE_SONAR_AUTOPILOT_HELP)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Stop submarine leak loop sound
/// PARAMS:
///    sSubmarineHelmData - 
PROC STOP_SUBMARINE_LEAK_LOOP_SOUND(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	IF !HAS_SOUND_FINISHED(sSubmarineHelmData.iLeakSoundID)
		STOP_SOUND(sSubmarineHelmData.iLeakSoundID)
		RELEASE_SOUND_ID(sSubmarineHelmData.iLeakSoundID)
		sSubmarineHelmData.iLeakSoundID = -1
		PRINTLN("[SUBMARINE_HELM] STOP_SUBMARINE_LEAK_LOOP_SOUND sSubmarineHelmData.iLeakSoundID = -1")
	ENDIF
ENDPROC

/// PURPOSE:
///    Reaching Crush Depth in the Kosatka could use some sfx to warn and make this more dramatic
PROC MAINTAIN_SUBMARINE_AIR_LEAK_SOUND(SUBMARINE_HELM_DATA &sSubmarineHelmData, VEHICLE_INDEX submarineVeh)
	INT iAirLeak = GET_SUBMARINE_NUMBER_OF_AIR_LEAKS(submarineVeh)
	
	IF iAirLeak < 1
	AND GET_SUBMARINE_IS_UNDER_DESIGN_DEPTH(submarineVeh)
		iAirLeak = 1
	ENDIF	
	
	IF iAirLeak >= SUBMARINE_AIR_LEAK_LIMIT
		IF sSubmarineHelmData.iLeakSoundState != 3
			STOP_SUBMARINE_LEAK_LOOP_SOUND(sSubmarineHelmData)
			PLAY_SOUND_FRONTEND(-1, "Crush", "DLC_H4_Submarine_Crush_Depth_Sounds")	
			sSubmarineHelmData.iLeakSoundState = 3
			PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_AIR_LEAK_SOUND play Crush")
		ENDIF	
	ELSE
		SWITCH iAirLeak
			CASE 1
				IF sSubmarineHelmData.iLeakSoundState != 1
					STOP_SUBMARINE_LEAK_LOOP_SOUND(sSubmarineHelmData)
					IF HAS_SOUND_FINISHED(sSubmarineHelmData.iLeakSoundID)
						sSubmarineHelmData.iLeakSoundID = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(sSubmarineHelmData.iLeakSoundID, "Creaking_Loop", "DLC_H4_Submarine_Crush_Depth_Sounds")	
						sSubmarineHelmData.iLeakSoundState = 1
						PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_AIR_LEAK_SOUND play Creaking_Loop")
					ENDIF	
				ENDIF	
			BREAK
			CASE 2
			CASE 3
				IF sSubmarineHelmData.iLeakSoundState != 2
					STOP_SUBMARINE_LEAK_LOOP_SOUND(sSubmarineHelmData)
					IF HAS_SOUND_FINISHED(sSubmarineHelmData.iLeakSoundID)
						sSubmarineHelmData.iLeakSoundID = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(sSubmarineHelmData.iLeakSoundID, "Warning_Alarm_Loop", "DLC_H4_Submarine_Crush_Depth_Sounds")	
						sSubmarineHelmData.iLeakSoundState = 2
						PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_AIR_LEAK_SOUND play Warning_Alarm_Loop")
					ENDIF	
				ENDIF	
			BREAK
			DEFAULT
				IF sSubmarineHelmData.iLeakSoundState != 0
					STOP_SUBMARINE_LEAK_LOOP_SOUND(sSubmarineHelmData)
					sSubmarineHelmData.iLeakSoundState = 0
					PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_AIR_LEAK_SOUND iAirLeak: ", iAirLeak, " STOP_SUBMARINE_LEAK_LOOP_SOUND")
				ENDIF	
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Display necessary help text when in submarine driver seat
PROC DISPLAY_SUBMARINE_HELM_HELP(SUBMARINE_HELM_DATA &sSubmarineHelmData, VEHICLE_INDEX submarineVeh)
	MAINTAIN_SUBMARINE_PERSICOPE_HELP(sSubmarineHelmData)
	MAINTAIN_SUBMARINE_AUTOPILOT_HELP(sSubmarineHelmData, submarineVeh)
	MAINTAIN_SUBMARINE_SONAR_HELP(sSubmarineHelmData, submarineVeh)
ENDPROC

/// PURPOSE:
///    Maintain submarine helm drive state, this will check if player is in vehicle. Disable/unable vehicle functionality. Wait for player control to return player to helm seat.
PROC MAINTAIN_SUBMARINE_HELM_DRIVE_IN_VEHICLE_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	VEHICLE_INDEX submarineVeh = GET_SUBMARINE_HELM_VEHICLE_INDEX(sSubmarineHelmData)
	
	SET_RADAR_AS_EXTERIOR_THIS_FRAME()
	FORCE_CINEMATIC_RENDERING_THIS_UPDATE(FALSE)
	
	IF NOT SHOULD_BAIL_SUBMARINE_HELM_DRIVE(sSubmarineHelmData, TRUE)	
	AND IS_ENTITY_ALIVE(submarineVeh)
		MAINTAIN_SUBMARINE_HELM_DRIVE_AUTO_PILOT(sSubmarineHelmData, submarineVeh)
		MAINTAIN_SUBMARINE_HELM_PERISCOPE(sSubmarineHelmData)
		DISPLAY_SUBMARINE_HELM_HELP(sSubmarineHelmData, submarineVeh)
		MAINTAIN_SUBMARINE_AIR_LEAK_SOUND(sSubmarineHelmData, submarineVeh)
		EXIT
	ENDIF

	IF IS_ENTITY_ALIVE(submarineVeh)
	AND TAKE_CONTROL_OF_ENTITY(submarineVeh)
		SET_VEHICLE_KEEP_ENGINE_ON_WHEN_ABANDONED(submarineVeh, TRUE)
	ENDIF
	
	DISABLE_INTERACTION_MENU()
	STOP_SUBMARINE_LEAK_LOOP_SOUND(sSubmarineHelmData)
	SET_PLAYER_LEAVING_SUBMARINE_DRIVER_SEAT(TRUE)
	DO_SCREEN_FADE_OUT(500)
	SET_SUBMARINE_HELM_PLAYER_CONTROL(FALSE, NSPC_CLEAR_TASKS | NSPC_FREEZE_POSITION)
	SET_SUBMARINE_HELM_DRIVE_STATE(sSubmarineHelmData, SHDS_ON_ENTER_WARP_TO_HELM)
ENDPROC

/// PURPOSE:
///    Maintain submarine helm drive, on enter warp to helm. Get ready to move player to helm
PROC MAINTAIN_SUBMARINE_HELM_ON_ENTER_WARP_TO_HELM_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF NOT IS_SCREEN_FADED_OUT()
		EXIT
	ENDIF
	
	IF NOT HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES(sSubmarineHelmData, TRUE)
		EXIT
	ENDIF
	
	IF NOT NET_WARP_TO_COORD(GET_SUBMARINE_HELM_COORDS(sSubmarineHelmData), 0.0)
		PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_HELM_ON_ENTER_WARP_TO_HELM_STATE waiting on NET_WARP_TO_COORD")
		EXIT
	ENDIF

	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_FiresDummyRockets, FALSE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableStartEngine, TRUE)
	SEND_SUBMARINE_WEAPONS_TELEMETRICS(SWT_TORPEDO, IS_OWNER_OF_THIS_SUB())

	IF IS_SUBMARINE_HELM_LOCAL_DATA_BIT_SET(sSubmarineHelmData, SUBMARINE_HELM_DATA_BS_ACTIVATE_AUTO_PILOT)
		VEHICLE_INDEX submarineVeh = GET_SUBMARINE_HELM_VEHICLE_INDEX(sSubmarineHelmData)
	
		IF IS_ENTITY_ALIVE(submarineVeh)
			ACTIVATE_SUBMARINE_HELM_AUTOPILOT(submarineVeh, FALSE)
		ENDIF	
	ENDIF
	
	SET_SUBMARINE_HELM_PROP_CAN_MIGRATE(sSubmarineHelmData, serverBD, TRUE)
	PERFORM_SUBMARINE_HELM_SYNCHRONISED_SCENE(sSubmarineHelmData, SHAC_EXIT_COMPUTER, serverBD, FALSE, TRUE)
	SET_SUBMARINE_HELM_DRIVE_STATE(sSubmarineHelmData, SHDS_WARP_TO_HELM)	
ENDPROC

/// PURPOSE:
///    Check if player is back in helm seat and clean up flags
PROC MAINTAIN_SUBMARINE_HELM_WARP_TO_HELM_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF NOT HAS_PLAYER_CONTROL_OF_ALL_SUBMARINE_HELM_ENTITIES(sSubmarineHelmData, TRUE)
		EXIT
	ENDIF
	
	IF HAS_SUBMARINE_HELM_SYNCHRONISED_SCENE_FINISHED(sSubmarineHelmData, SHAC_EXIT_COMPUTER)
		IF NOT HAS_NET_TIMER_STARTED(sSubmarineHelmData.stWarpBackAnimTimer)
			PERFORM_SUBMARINE_HELM_SYNCHRONISED_SCENE(sSubmarineHelmData, SHAC_BASE_SEAT, serverBD, TRUE)
			START_NET_TIMER(sSubmarineHelmData.stWarpBackAnimTimer)
		ENDIF	
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(sSubmarineHelmData.stWarpBackAnimTimer)
		EXIT
	ELSE
		//IF NOT IS_SUBMARINE_HELM_PED_RUNNING_SEAT_ANIM(sSubmarineHelmData, PLAYER_PED_ID(), SHAC_BASE_SEAT)
		IF NOT HAS_NET_TIMER_EXPIRED(sSubmarineHelmData.stWarpBackAnimTimer, 2500)
			EXIT
		ENDIF
	ENDIF	

	FORCE_SIMPLE_INTERIOR_BLIPS_UPDATE()
	SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(sSubmarineHelmData, PLAYER_SUBMARINE_HELM_BD_DATA_ENTERING_SUBMARINE_DRIVER_SEAT, FALSE)
	SET_PLAYER_IN_SUBMARINE_DRIVER_SEAT(FALSE)
	SET_MINIMAP_BLOCK_WAYPOINT(TRUE)
	IF IS_WAYPOINT_ACTIVE()
		SET_WAYPOINT_OFF()
	ENDIF
	FORCE_SUBMARINE_DRIVER_TO_HELM_SEAT(FALSE)
	SET_PLAYER_LEAVING_SUBMARINE_DRIVER_SEAT(FALSE)
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	IF IS_USING_PED_SCUBA_GEAR_VARIATION(PLAYER_PED_ID())
		CLEAR_PED_SCUBA_GEAR_VARIATION(PLAYER_PED_ID())
	ENDIF
	
	// Interiro script will handle screen fade and player controls
	IF NOT SHOULD_KICK_PILOT_OUT_OF_INTERIOR()
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		SET_SUBMARINE_HELM_PLAYER_CONTROL(TRUE)
		DO_SCREEN_FADE_IN(500)
	ENDIF
	
	RESET_NET_TIMER(sSubmarineHelmData.schtAutoPilotControl.sTimer)
	RESET_NET_TIMER(sSubmarineHelmData.stWarpBackAnimTimer)
	SET_INTERIOR_STATE_TO_PILOT_VEHICLE_OUTSIDE_INTERIOR(FALSE)
	SET_SUBMARINE_HELM_DRIVE_STATE(sSubmarineHelmData, SHDS_ON_ENTER_WARP_TO_VEH)
	SET_SUBMARINE_HELM_OPTION(sSubmarineHelmData, SHO_IDLE)
ENDPROC

FUNC BOOL SHOULD_CLEANUP_SUBMARINE_HELP_TEXT()
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		RETURN TRUE
	ENDIF	
	
	IF g_bBustedWarpInProgress
		PRINTLN("[SUBMARINE_HELM] SHOULD_CLEANUP_SUBMARINE_HELP_TEXT g_bBustedWarpInProgress true")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SUBMARINE_HELM_HELP_TEXT()
	IF NOT SHOULD_CLEANUP_SUBMARINE_HELP_TEXT()
		EXIT
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_SUBMARINE_HELM_OPTION_HELP())
		CLEAR_HELP()
	ENDIF
ENDPROC

/// PURPOSE:
///    Submarine helm drive option state. 
PROC MAINTAIN_SUBMARINE_HELM_DRIVE_OPTION(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	MAINTAIN_SUBMARINE_HELM_HELP_TEXT()
	
	SWITCH GET_SUBMARINE_HELM_DRIVE_STATE(sSubmarineHelmData)
		CASE SHDS_ON_ENTER_WARP_TO_VEH
			MAINTAIN_SUBMARINE_DRIVE_ON_ENTER_WARP_TO_VEH_STATE(sSubmarineHelmData, serverBD)
		BREAK
		CASE SHDS_WARP_TO_VEH
			MAINTAIN_SUBMARINE_HELM_DRIVE_WARP_TO_VEH_STATE(sSubmarineHelmData, serverBD)
		BREAK
		CASE SHDS_IN_VEH
			MAINTAIN_SUBMARINE_HELM_DRIVE_IN_VEHICLE_STATE(sSubmarineHelmData)
		BREAK
		CASE SHDS_ON_ENTER_WARP_TO_HELM
			MAINTAIN_SUBMARINE_HELM_ON_ENTER_WARP_TO_HELM_STATE(sSubmarineHelmData, serverBD)
		BREAK
		CASE SHDS_WARP_TO_HELM
			MAINTAIN_SUBMARINE_HELM_WARP_TO_HELM_STATE(sSubmarineHelmData, serverBD)
		BREAK
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Submarine fast travel option state. This is where fast travel menu gets launched.
PROC MAINTAIN_SUBMARINE_HELM_FAST_TRAVEL_OPTION(SUBMARINE_HELM_DATA &sSubmarineHelmData, SUBMARINE_FAST_TRAVEL_MENU &FTMData)
	// Call SET_SUBMARINE_HELM_OPTION(sSubmarineHelmData, SHO_IDLE) to get back to main(where we display button press help).
	// If you want to kick player out of seat call SET_SUBMARINE_HELM_OPTION(sSubmarineHelmData, SHO_EXIT)
	IF NOT FTMData.bWantsToDisplay AND NOT FTMData.bIsDiplaying
		SET_SUBMARINE_HELM_OPTION(sSubmarineHelmData, SHO_IDLE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Submarine helm on update state. Player is in seat and looping animation is playing. Player will get help text to choose helm option.
PROC SUBMARINE_HELM_ON_UPDATE_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD, SUBMARINE_FAST_TRAVEL_MENU &FTMData)
	DISABLE_UNNECESSARY_PLAYER_FUNCTINALITY_THIS_FRAME(sSubmarineHelmData)
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
	OR IS_TRANSITION_ACTIVE()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			ENDIF
		ENDIF	
		SUBMARINE_SUBMARINE_HELM_CLEANUP(sSubmarineHelmData, serverBD)
		EXIT
	ENDIF
	
	SWITCH GET_SUBMARINE_HELM_OPTION(sSubmarineHelmData)
		CASE SHO_IDLE
			MAINTAIN_SUBMARINE_HELM_IDLE_OPTION(sSubmarineHelmData, serverBD, FTMData)
		BREAK
		CASE SHO_DRIVE
			MAINTAIN_SUBMARINE_HELM_DRIVE_OPTION(sSubmarineHelmData, serverBD)
		BREAK
		CASE SHO_FAST_TRAVEL
			MAINTAIN_SUBMARINE_HELM_FAST_TRAVEL_OPTION(sSubmarineHelmData, FTMData)
		BREAK
		CASE SHO_EXIT
			MAINTAIN_SUBMARINE_HELM_EXIT_OPTION(sSubmarineHelmData, serverBD)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Submarine helm exit seat state where we check if exit animation is done and cleanup
PROC SUBMARINE_HELM_ON_EXIT_STATE(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	DISABLE_UNNECESSARY_PLAYER_FUNCTINALITY_THIS_FRAME(sSubmarineHelmData)
	
	IF HAS_SUBMARINE_HELM_SYNCHRONISED_SCENE_FINISHED(sSubmarineHelmData, SHAC_EXIT_SEAT)
		ENABLE_INTERACTION_MENU()
		SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(sSubmarineHelmData, PLAYER_SUBMARINE_HELM_BD_DATA_ENTERED_HELM_SEAT, FALSE)
		CLEAR_ALL_SUBMARINE_HELM_LOCAL_DATA_BS(sSubmarineHelmData)
		SET_SUBMARINE_HELM_STATE(sSubmarineHelmData, SHS_IDLE)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Update submarine helm player stagger
PROC UPDATE_SUBMARINE_HELM_STAGGER(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	sSubmarineHelmData.iPlayerStagger = (sSubmarineHelmData.iPlayerStagger + 1) % NETWORK_GET_MAX_NUM_PARTICIPANTS()
ENDPROC

/// PURPOSE:
///    Maintain submarin helm clone ped creation. Create a clone ped to replace player ped when warped to submarine driver seat.
PROC MAINTAIN_SUBMARINE_HELM_CLONE_PED_CREATION(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF serverBD.iplayerUsingHelm = -1
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niClonePed)
		EXIT
	ENDIF	
	
	PLAYER_INDEX piHelmUser = INT_TO_NATIVE(PLAYER_INDEX, serverBD.iplayerUsingHelm) 
	
	IF NOT IS_NET_PLAYER_OK(piHelmUser)
		EXIT
	ENDIF
	
	PED_INDEX piHelmUserPed = GET_PLAYER_PED(piHelmUser)
	
	IF NOT IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET(sSubmarineHelmData, piHelmUser, PLAYER_SUBMARINE_HELM_BD_DATA_ENTERED_HELM_SEAT)
		EXIT
	ENDIF
	
	STRING sAnimClip = GET_SUBMARINE_HELM_ANIM_DIC(sSubmarineHelmData, IS_PED_WEARING_HIGH_HEELS(piHelmUserPed))
	REQUEST_ANIM_DICT(sAnimClip)
	IF NOT HAS_ANIM_DICT_LOADED(sAnimClip)
		EXIT
	ENDIF	
	
	IF NOT CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_PEDS(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
		PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_HELM_CLONE_PED_CREATION CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT false")
		EXIT
	ENDIF
	
	RESERVE_LOCAL_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
	
	IF NOT CAN_REGISTER_MISSION_PEDS(1)
		PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_HELM_CLONE_PED_CREATION CAN_REGISTER_MISSION_PEDS false")
		EXIT
	ENDIF
	
	MODEL_NAMES playerModel = GET_ENTITY_MODEL(piHelmUserPed)
	
	IF NOT REQUEST_LOAD_MODEL(playerModel)
		EXIT
	ENDIF
	
	IF NOT HAS_PED_HEAD_BLEND_FINISHED(piHelmUserPed)
	OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(piHelmUserPed)
		EXIT
	ENDIF

	TEXT_LABEL_23 tlBaseClip = GET_SUBMARINE_HELM_ANIM_CLIP_NAME(sSubmarineHelmData, SHAC_BASE_SEAT)

	FLOAT fHelmHeading = GET_SUBMARINE_HELM_HEADING(sSubmarineHelmData)
	VECTOR vHelmCoords = GET_SUBMARINE_HELM_COORDS(sSubmarineHelmData)
	VECTOR tempPosition = GET_ANIM_INITIAL_OFFSET_POSITION(sAnimClip, tlBaseClip, vHelmCoords, <<0, 0, fHelmHeading>>)
	VECTOR tempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimClip, tlBaseClip, vHelmCoords, <<0, 0, fHelmHeading>>)

	PED_INDEX clonePed = CLONE_PED(GET_PLAYER_PED(piHelmUser))
	SET_ENTITY_COORDS(clonePed, tempPosition)
	SET_ENTITY_HEADING(clonePed, tempRotation.z)
	serverBD.niClonePed = PED_TO_NET(clonePed)
	
	SET_ENTITY_VISIBLE(NET_TO_PED(serverBD.niClonePed), FALSE)
	SET_ENTITY_INVINCIBLE(NET_TO_PED(serverBD.niClonePed), TRUE)
	FREEZE_ENTITY_POSITION(NET_TO_PED(serverBD.niClonePed), TRUE)
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_PED(serverBD.niClonePed), TRUE)
	
	FINALIZE_HEAD_BLEND(clonePed)
	NETWORK_APPLY_CACHED_PLAYER_HEAD_BLEND_DATA(NET_TO_PED(serverBD.niClonePed), PLAYER_ID())

	PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_HELM_CLONE_PED_CREATION")
ENDPROC

/// PURPOSE:
///    Remove clone ped since no one is using submarine helm
PROC REMOVE_SUBMARINE_HELM_CLONE_PED(SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niClonePed)
		IF TAKE_CONTROL_OF_NET_ID(serverBD.niClonePed)
			DELETE_NET_ID(serverBD.niClonePed)
			RESERVE_LOCAL_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS(FALSE, RESERVATION_LOCAL_ONLY) - 1)
			PRINTLN("[SUBMARINE_HELM] REMOVE_SUBMARINE_HELM_CLONE_PED")
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///    This will keep track of player index of submarine helm user
PROC MAINTAIN_SUBMARINE_HELM_IN_USE_PLAYER(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF serverBD.iplayerUsingHelm = -1
		IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, sSubmarineHelmData.iPlayerStagger))
			IF IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET(sSubmarineHelmData, INT_TO_NATIVE(PLAYER_INDEX, sSubmarineHelmData.iPlayerStagger), PLAYER_SUBMARINE_HELM_BD_DATA_ENTERED_HELM_SEAT)
				serverBD.iplayerUsingHelm = sSubmarineHelmData.iPlayerStagger
				PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_HELM_IN_USE_PLAYER: player = ", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, sSubmarineHelmData.iPlayerStagger)), " using submarine helm")
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, serverBD.iplayerUsingHelm))
		OR NOT IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET(sSubmarineHelmData, INT_TO_NATIVE(PLAYER_INDEX, serverBD.iplayerUsingHelm), PLAYER_SUBMARINE_HELM_BD_DATA_ENTERED_HELM_SEAT)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niClonePed)
				REMOVE_SUBMARINE_HELM_CLONE_PED(serverBD)
			ELSE	
				serverBD.iplayerUsingHelm = -1
				PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_HELM_IN_USE_PLAYER: player = ", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, sSubmarineHelmData.iPlayerStagger)), " no longer using submarine helm")
			ENDIF	
		ENDIF	
	ENDIF	
ENDPROC

/// PURPOSE:
///    Toggles clone ped and helm user player visibility locally depending on helm user state
PROC MAINTAIN_SUBMARINE_PED_VISIBILITY_LOCALLY(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niClonePed)
		EXIT
	ENDIF
	
	IF serverBD.iplayerUsingHelm != -1
		IF IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET(sSubmarineHelmData, INT_TO_NATIVE(PLAYER_INDEX, serverBD.iplayerUsingHelm), PLAYER_SUBMARINE_HELM_BD_DATA_ENTERING_SUBMARINE_DRIVER_SEAT)
			SET_SUBMARINE_HELM_CLONE_PED_VISIBILITY_LOCALLY(sSubmarineHelmData, TRUE)
			IF IS_ENTITY_ALIVE(sSubmarineHelmData.piClonePed)
			AND IS_ENTITY_VISIBLE(sSubmarineHelmData.piClonePed)
				SET_SUBMARINE_HELM_USER_PLAYER_VISIBILITY_LOCALLY(INT_TO_NATIVE(PLAYER_INDEX, serverBD.iplayerUsingHelm), FALSE)
			ENDIF	
		ELSE
			SET_SUBMARINE_HELM_USER_PLAYER_VISIBILITY_LOCALLY(INT_TO_NATIVE(PLAYER_INDEX, serverBD.iplayerUsingHelm), TRUE)
			IF IS_ENTITY_ALIVE(GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, serverBD.iplayerUsingHelm)))
			AND IS_ENTITY_VISIBLE(GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, serverBD.iplayerUsingHelm)))
				SET_SUBMARINE_HELM_CLONE_PED_VISIBILITY_LOCALLY(sSubmarineHelmData, FALSE)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Catch submarine helm network objects to local
/// PARAMS:
///    sSubmarineHelmData - 
PROC MAINTAIN_SUBMARINE_HELM_LOCAL_OBJECTS(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niChair)
		sSubmarineHelmData.oiChair = NET_TO_OBJ(serverBD.niChair)
	ENDIF
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niController)
		sSubmarineHelmData.oiController = NET_TO_OBJ(serverBD.niController)
	ENDIF
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niClonePed)
		sSubmarineHelmData.piClonePed = NET_TO_PED(serverBD.niClonePed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain submarine helm server funtionality
PROC MAINTAIN_SUBMARINE_HELM_SERVER(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	MAINTAIN_SUBMARINE_HELM_IN_USE_PLAYER(sSubmarineHelmData, serverBD)
	MAINTAIN_SUBMARINE_HELM_CLONE_PED_CREATION(sSubmarineHelmData, serverBD)
ENDPROC

/// PURPOSE:
///    Check if clone ped exisit and finalize head blend
PROC MAITNAIN_SUBMARINE_HELM_CLONE_PED_HEAD_BLEND(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD)
	IF NOT IS_ENTITY_ALIVE(sSubmarineHelmData.piClonePed)
		SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(sSubmarineHelmData, PLAYER_SUBMARINE_HELM_BD_DATA_FINALIZE_CLONE_PED_HEAD_BLEND, FALSE)
		EXIT
	ENDIF
	
	IF NOT IS_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BIT_SET(sSubmarineHelmData, PLAYER_ID(), PLAYER_SUBMARINE_HELM_BD_DATA_FINALIZE_CLONE_PED_HEAD_BLEND)	
		IF serverBD.iplayerUsingHelm != -1
			IF HAS_PED_HEAD_BLEND_FINISHED(sSubmarineHelmData.piClonePed)
			AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSubmarineHelmData.piClonePed)
				IF NETWORK_HAS_CACHED_PLAYER_HEAD_BLEND_DATA(INT_TO_NATIVE(PLAYER_INDEX , serverBD.iplayerUsingHelm))
					IF NETWORK_APPLY_CACHED_PLAYER_HEAD_BLEND_DATA(sSubmarineHelmData.piClonePed, INT_TO_NATIVE(PLAYER_INDEX , serverBD.iplayerUsingHelm))
						SET_SUBMARINE_HELM_PLAYER_BROADCAST_DATA_BS(sSubmarineHelmData, PLAYER_SUBMARINE_HELM_BD_DATA_FINALIZE_CLONE_PED_HEAD_BLEND, TRUE)
					ENDIF
				ENDIF	
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if we are stuck in pilot vehicle outside interior state
PROC MAINTAIN_SUBMARINE_HELM_SAFE_TIMER(SUBMARINE_HELM_DATA &sSubmarineHelmData)
	IF SHOULD_SET_INTERIOR_STATE_TO_PILOT_VEHICLE_OUTSIDE_INTERIOR()
		VEHICLE_INDEX submarineVeh = GET_SUBMARINE_HELM_VEHICLE_INDEX(sSubmarineHelmData)
		
		IF IS_ENTITY_ALIVE(submarineVeh)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), submarineVeh)
				IF HAS_NET_TIMER_EXPIRED(sSubmarineHelmData.stSafeTimer, g_SimpleInteriorData.iSubmarineHelmSafeTimer)
					SET_INTERIOR_STATE_TO_PILOT_VEHICLE_OUTSIDE_INTERIOR(FALSE)
					RESET_NET_TIMER(sSubmarineHelmData.stSafeTimer)
					PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_HELM_SAFE_TIMER - player is not in submarine driver seat but interior state is in SIMPLE_INT_STATE_PILOT_VEHICLE_OUTSIDE_INTERIOR")
				ENDIF
			ELSE
				RESET_NET_TIMER(sSubmarineHelmData.stSafeTimer)	
			ENDIF
		ENDIF
	ELSE	
		RESET_NET_TIMER(sSubmarineHelmData.stSafeTimer)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Updates the submarine helm that allows the player to select drive or fast travel option
///    This will not update unless SUBMARINE_HELM_INIT is called
PROC MAINTAIN_SUBMARINE_HELM(SUBMARINE_HELM_DATA &sSubmarineHelmData, SERVER_SUBMARINE_HELM_BD_DATA &serverBD, SUBMARINE_FAST_TRAVEL_MENU &FTMData)	
	MAINTAIN_SUBMARINE_HELM_SERVER(sSubmarineHelmData, serverBD)
	MAINTAIN_SUBMARINE_PED_VISIBILITY_LOCALLY(sSubmarineHelmData, serverBD)
	MAITNAIN_SUBMARINE_HELM_CLONE_PED_HEAD_BLEND(sSubmarineHelmData, serverBD)
	MAINTAIN_SUBMARINE_HELM_LOCAL_OBJECTS(sSubmarineHelmData, serverBD)
	MAINTAIN_SUBMARINE_HELM_SAFE_TIMER(sSubmarineHelmData)
	
	SWITCH GET_SUBMARINE_HELM_STATE(sSubmarineHelmData)
		CASE SHS_UNINTITALIZED
			#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 120 = 0
					PRINTLN("[SUBMARINE_HELM] MAINTAIN_SUBMARINE_HELM - Not initialized")
				ENDIF
			#ENDIF
		BREAK
		CASE SHS_INIT_NETWORK_OBJECTS
			SUBMARINE_HELM_INIT_NETWORK_OBJECTS_STATE(sSubmarineHelmData, serverBD)
		BREAK
		CASE SHS_IDLE
			SUBMARINE_HELM_IDLE_STATE(sSubmarineHelmData, serverBD)
		BREAK
		CASE SHS_PROMPT
			SUBMARINE_HELM_PROMPT_STATE(sSubmarineHelmData, serverBD)
		BREAK
		CASE SHS_GO_TO_ANIM_COORD
			SUBMARINE_HELM_GO_TO_ANIM_COORD_STATE(sSubmarineHelmData, serverBD)
		BREAK
		CASE SHS_ON_ENTER
			SUBMARINE_HELM_ON_ENTER_STATE(sSubmarineHelmData, serverBD)
		BREAK
		CASE SHS_ON_UPDATE
			SUBMARINE_HELM_ON_UPDATE_STATE(sSubmarineHelmData, serverBD, FTMData)
		BREAK
		CASE SHS_ON_EXIT
			SUBMARINE_HELM_ON_EXIT_STATE(sSubmarineHelmData)
		BREAK
	ENDSWITCH	
	
	UPDATE_SUBMARINE_HELM_STAGGER(sSubmarineHelmData)
ENDPROC



