//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        mp_submarine_scuba_rack.sch																			
/// Description: Header for equiping scuba gear from the submarine scuba racks.	Runs a maximum of 20 racks at a time.							
/// Written by:  Tom Turner																						
/// Date:  		28/09/2020																							
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "mp_globals.sch"
USING "script_debug.sch"
USING "lineactivation.sch"
USING "context_control_public.sch"

#IF FEATURE_HEIST_ISLAND
/// PURPOSE: 
///    States for the submarine scuba rack
ENUM SUBMARINE_SCUBA_RACK_STATE
	SSRS_UNINTITALIZED
	,SSRS_IDLE
	,SSRS_PROMPT_EQUIP
	,SSRS_PROMPT_REMOVE
	,SSRS_PROMPT_NO_OUTFIT
	,SSRS_LOAD_AND_EQUIP
	,SSRS_LOAD_AND_REMOVE
	,SSRS_WAIT_FOR_TRIGGER_EXIT
ENDENUM

/// PURPOSE:
///    Gets the debug name of the given submarine scuba rack state, Works in debug only
/// PARAMS:
///    eEnum - Scuba rack enum to get the name of
/// RETURNS:
///    Gets the debug name of the given submarine scuba rack state
DEBUGONLY FUNC STRING DEBUG_GET_SUBMARINE_SCUBA_RACK_STATE_AS_STRING(SUBMARINE_SCUBA_RACK_STATE eEnum)
	SWITCH eEnum
		CASE SSRS_UNINTITALIZED			RETURN 	"SSRS_UNINTITALIZED"
		CASE SSRS_IDLE					RETURN	"SSRS_IDLE"
		CASE SSRS_PROMPT_EQUIP			RETURN	"SSRS_PROMPT_EQUIP"
		CASE SSRS_PROMPT_REMOVE			RETURN 	"SSRS_PROMPT_REMOVE"
		CASE SSRS_PROMPT_NO_OUTFIT		RETURN	"SSRS_PROMPT_NO_OUTFIT"
		CASE SSRS_LOAD_AND_EQUIP 		RETURN 	"SSRS_LOAD_AND_EQUIP"
		CASE SSRS_LOAD_AND_REMOVE		RETURN 	"SSRS_LOAD_AND_REMOVE"
		CASE SSRS_WAIT_FOR_TRIGGER_EXIT	RETURN 	"SSRS_WAIT_FOR_TRIGGER_EXIT"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_SUBMARINE_SCUBA_RACK_STATE_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Area check function pointer for checking if in the scuba rack volume 
TYPEDEF FUNC BOOL T_SCUBA_RACK_AREA_CHECK(PED_INDEX piPed)

CONST_FLOAT cfSUBMARINE_SCUBA_RACK_HEADING_LEEWAY 60.0
CONST_INT ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS 22

/// PURPOSE: 
///    Struct for running a scuba rack
STRUCT SUBMARINE_SCUBA_RACK
	SUBMARINE_SCUBA_RACK_STATE eState
	T_SCUBA_RACK_AREA_CHECK fpAreaCheck
	PED_COMP_NAME_ENUM eScubaOutfitComp
	VECTOR vRackCoords
	INT iContextIntention = NEW_CONTEXT_INTENTION
	INT iBS // SUBMARINE_SCUBA_RACK_BIT_SET
ENDSTRUCT

/// PURPOSE:
///    Gets the ped componant for the given scuba index, the index being 0 - ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS-1
/// PARAMS:
///    iScubaOutfitIndex - index of a scuba outfit between 0 - ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS-1, should match outfit suffix in shop label
/// RETURNS:
///    The ped component for the given index OR DUMMY_PED_COMP if index is out of range
FUNC PED_COMP_NAME_ENUM SUBMARINE_SCUBA_RACK__GET_SCUBA_OUTFIT_PED_COMPONENT_AT_INDEX(INT iScubaOutfitIndex)
	IF iScubaOutfitIndex < 0 OR iScubaOutfitIndex >= ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS
		RETURN DUMMY_PED_COMP
	ENDIF
	
	CONST_INT ciFEMALE_OFFSET 314
	CONST_INT ciMALE_OFFSET 345
	
	// These dlc outfits don't have an actual enum so we have to calculate based on an offset
	INT iOutfitEnum = PICK_INT(IS_PLAYER_FEMALE(), ENUM_TO_INT(OUTFIT_FMF_DLC) + ciFEMALE_OFFSET, ENUM_TO_INT(OUTFIT_FMM_DLC) + ciMALE_OFFSET)
	iOutfitEnum += iScubaOutfitIndex
	RETURN INT_TO_ENUM(PED_COMP_NAME_ENUM, iOutfitEnum)
ENDFUNC

/// PURPOSE:
///    Checks if the given scuba outfit has been purchased by the player
///    0 will always return true because the player should have the black scuba item unlocked by default
/// PARAMS:
///    iScubaOutfitIndex - index of a scuba outfit between 0 - ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS-1, should match outfit suffix in shop label
/// RETURNS:
///    TRUE if the scuba outfit has been purchased or is the default scuba outfit
FUNC BOOL SUBMARINE_SCUBA_RACK__IS_SCUBA_OUTFIT_PURCHASED(INT iScubaOutfitIndex)
	// We always have access to the black scuba outfit
	IF iScubaOutfitIndex = 0
		RETURN TRUE
	ENDIF
	
	PED_COMP_NAME_ENUM ePedCompName = SUBMARINE_SCUBA_RACK__GET_SCUBA_OUTFIT_PED_COMPONENT_AT_INDEX(iScubaOutfitIndex)
	
	// Failed to get component
	IF ePedCompName = DUMMY_PED_COMP
		RETURN FALSE
	ENDIF
	
	PED_COMP_ITEM_DATA_STRUCT sMainItemData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_OUTFIT, ePedCompName)
	RETURN IS_BIT_SET(sMainItemData.iProperties, PED_COMPONENT_ACQUIRED_BIT)
ENDFUNC

/// PURPOSE:
///    Gets the count of how many scuba suits the player has purchased
///    there will always be at least 1 because the default scuba gear is gifted by default
/// RETURNS:
///    The cout of unlocked scuba outfits
FUNC INT SUBMARINE_SCUBA_RACK__GET_COUNT_OF_PURCHASED_SCUBA_OUTFITS()
	INT iScubaOutfit
	INT iCount
	
	REPEAT ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS iScubaOutfit
		IF SUBMARINE_SCUBA_RACK__IS_SCUBA_OUTFIT_PURCHASED(iScubaOutfit)
			++iCount
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

/// PURPOSE:
///    This converts the selection in the PI menu into the index of a purchased scuba outfit,
///    this is so we can exclude unpurchased scuba items
/// PARAMS:
///    iIndex - the PI menu selection index, should be between 0 - ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS-1
/// RETURNS:
///    The purchased scuba outfit at the selection index, If the given index is out of range it will return 0
FUNC INT SUBMARINE_SCUBA_RACK__GET_PIM_INDEX_AS_SCUBA_INDEX(INT iIndex)	
	// Out of range
	IF iIndex < 0 OR iIndex >= ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS
		RETURN 0
	ENDIF
	
	INT iScubaOutfit
	INT iScubaOutfitList[ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS]
	INT iInsert
	
	// Create list of purchased scuba gear
	REPEAT ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS iScubaOutfit
		IF SUBMARINE_SCUBA_RACK__IS_SCUBA_OUTFIT_PURCHASED(iScubaOutfit)
			iScubaOutfitList[iInsert] = iScubaOutfit
			++iInsert
		ENDIF
	ENDREPEAT
	
	RETURN iScubaOutfitList[iIndex]
ENDFUNC

/// PURPOSE:
///    Gets the scuba outfit index as an index for the PI menu selection so that we exclude unpurchased
///    outfits
/// PARAMS:
///    iScubaIndex - should be between 0 - ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS-1
/// RETURNS:
///    The scuba outfit index as an index for the PI menu selection
FUNC INT SUBMARINE_SCUBA_RACK__GET_SCUBA_INDEX_AS_PIM_INDEX(INT iScubaIndex)
	INT iScubaOutfit
	INT iScubaOutfitList[ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS]
	INT iInsert
	
	// Create list of purchased scuba gear
	REPEAT ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS iScubaOutfit
		IF SUBMARINE_SCUBA_RACK__IS_SCUBA_OUTFIT_PURCHASED(iScubaOutfit)
			iScubaOutfitList[iScubaOutfit] = iInsert
			++iInsert
		ENDIF
	ENDREPEAT
	
	RETURN iScubaOutfitList[iScubaIndex]
ENDFUNC

/// PURPOSE:
///    Gets the JBIB (torso) ped component of the given scuba outfit index   
/// PARAMS:
///    iScubaOutfitIndex - index of the scuba outift between 0 - ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS-1
/// RETURNS:
///    The JBIB ped component of the outfit OR DUMMY_PED_COMP if the index is out of range
FUNC PED_COMP_NAME_ENUM SUBMARINE_SCUBA_RACK__GET_SCUBA_OUTFITS_JBIB(INT iScubaOutfitIndex)
	IF iScubaOutfitIndex < 0 OR iScubaOutfitIndex >= ciSUBMARINE_SCUBA_RACK_MAX_SCUBA_OUTFITS
		RETURN DUMMY_PED_COMP
	ENDIF
	
	TEXT_LABEL_63 tlHash
	
	// Female
	IF IS_PLAYER_FEMALE()
		tlHash = "DLC_MP_X17_F_JBIB_2_"
		tlHash += iScubaOutfitIndex
		RETURN GET_PED_COMP_ITEM_FROM_NAME_HASH(MP_F_FREEMODE_01, GET_HASH_KEY(tlHash), COMP_TYPE_JBIB, 4)
	ENDIF
	
	// Male
	tlHash = "DLC_MP_X17_M_JBIB_2_"
	tlHash += iScubaOutfitIndex
	RETURN GET_PED_COMP_ITEM_FROM_NAME_HASH(MP_M_FREEMODE_01, GET_HASH_KEY(tlHash), COMP_TYPE_JBIB, 3)
ENDFUNC

/// PURPOSE:
///    Checks if the player is wearing their chosen scuba gear according to the selection in the pi menu
/// RETURNS:
///    TRUE if the palyer is wearing the scuba gear they chose in the PI menu
FUNC BOOL SUBMARINE_SCUBA_RACK__DOES_LOCAL_PLAYER_HAVE_SELECTED_SCUBA_GEAR_EQUIPPED()
	INT iSelectedScubaGear = GET_PACKED_STAT_INT(PACKED_MP_INT_SUBMARINE_SCUBA_GEAR_SELECTION)
	PED_COMP_NAME_ENUM eScubaJBIB = SUBMARINE_SCUBA_RACK__GET_SCUBA_OUTFITS_JBIB(iSelectedScubaGear)
	
	IF eScubaJBIB = DUMMY_PED_COMP
		PRINTLN("[SUBMARINE_SCUBA_RACK] DOES_LOCAL_PLAYER_HAVE_SELECTED_SCUBA_GEAR_EQUIPPED - Failed to get scuba JBIB")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[SUBMARINE_SCUBA_RACK] DOES_LOCAL_PLAYER_HAVE_SELECTED_SCUBA_GEAR_EQUIPPED - Scuba JBIB: ", ENUM_TO_INT(eScubaJBIB))
	
	PED_COMP_NAME_ENUM eCurrentJBIB = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB)
	PRINTLN("[SUBMARINE_SCUBA_RACK] DOES_LOCAL_PLAYER_HAVE_SELECTED_SCUBA_GEAR_EQUIPPED - Equipped JBIB: ", ENUM_TO_INT(eCurrentJBIB))
	
	RETURN eScubaJBIB = eCurrentJBIB
ENDFUNC

/// ***************************************************************************************
///    
/// 					GETTERS AND SETTERS
///    
/// *************************************************************************************** 

// *******************************************
// 					GETTERS
// *******************************************

/// PURPOSE:
///    Gets the current state of the scuba rack
/// PARAMS:
///    sScubaRackData - the scuba rack instance
/// RETURNS:
///    The current state of the scuba rack
FUNC SUBMARINE_SCUBA_RACK_STATE SUBMARINE_SCUBA_RACK__GET_STATE(SUBMARINE_SCUBA_RACK &sScubaRackData)
	RETURN sScubaRackData.eState
ENDFUNC

/// PURPOSE:
///    Gets the scuba rack trigger area check function pointer that is used to determine if a ped is in the activation area
/// PARAMS:
///    sScubaRackData - the scuba rack instance
/// RETURNS:
///    The scuba rack trigger area check function pointer
FUNC T_SCUBA_RACK_AREA_CHECK SUBMARINE_SCUBA_RACK__GET_AREA_CHECK(SUBMARINE_SCUBA_RACK &sScubaRackData)
	RETURN sScubaRackData.fpAreaCheck
ENDFUNC

/// PURPOSE:
///    Gets the scuba racks position which is used for heading checks
/// PARAMS:
///    sInst -  the scuba rack instance
/// RETURNS:
///    The scuba racks position
FUNC VECTOR SUBMARINE_SCUBA_RACK__GET_RACK_COORDS(SUBMARINE_SCUBA_RACK &sInst)
	RETURN sInst.vRackCoords
ENDFUNC

/// PURPOSE:
///    Gets the scuba racks context intention ID that is used for displaying the activation prompt
/// PARAMS:
///    sScubaRackData - the scuba rack instance
/// RETURNS:
///    The scuba racks context intention ID
FUNC INT _SUBMARINE_SCUBA_RACK__GET_CONTEXT_INTENTION(SUBMARINE_SCUBA_RACK &sScubaRackData)
	RETURN sScubaRackData.iContextIntention
ENDFUNC

// *******************************************
// 					SETTERS
// *******************************************

/// PURPOSE:
///    Sets the current state of the scuba rack
/// PARAMS:
///    sScubaRackData - the scuba rack instance
///    eState - the state to change to
PROC _SUBMARINE_SCUBA_RACK__SET_STATE(SUBMARINE_SCUBA_RACK &sScubaRackData, SUBMARINE_SCUBA_RACK_STATE eState)
	PRINTLN("[SUBMARINE_SCUBA_RACK] SET_STATE - ", DEBUG_GET_SUBMARINE_SCUBA_RACK_STATE_AS_STRING(sScubaRackData.eState), 
		" -> ", DEBUG_GET_SUBMARINE_SCUBA_RACK_STATE_AS_STRING(eState))
	sScubaRackData.eState = eState
ENDPROC

/// PURPOSE:
///    Sets the area check function pointer that is used to check if the player is in the scuba racks activation area
/// PARAMS:
///    sScubaRackData - the scuba rack instance
///    fpAreaCheck - a function with the signature BOOL T_SCUBA_RACK_AREA_CHECK(PED_INDEX piPed) that returns true when the given ped is in
///    				 the scuba racks trigger volume
PROC SUBMARINE_SCUBA_RACK__SET_AREA_CHECK(SUBMARINE_SCUBA_RACK &sScubaRackData, T_SCUBA_RACK_AREA_CHECK fpAreaCheck)
	sScubaRackData.fpAreaCheck = fpAreaCheck
ENDPROC

/// PURPOSE:
///    Sets the racks position that will be used for heading checks
/// PARAMS:
///    sInst - the scuba rack instance
///    vRackCoords - the position of the rack the player must face to activate it
PROC SUBMARINE_SCUBA_RACK__SET_RACK_COORDS(SUBMARINE_SCUBA_RACK &sScubaRackData, VECTOR vRackCoords)
	sScubaRackData.vRackCoords = vRackCoords
ENDPROC

/// PURPOSE:
///    Sets the context intention ID that is used for the prompt
/// PARAMS:
///    sScubaRackData - the scuba rack instance
///    iContextIntention - the context intention ID to use
PROC _SUBMARINE_SCUBA_RACK__SET_CONTEXT_INTENTION(SUBMARINE_SCUBA_RACK &sScubaRackData, INT iContextIntention)
	sScubaRackData.iContextIntention = iContextIntention
ENDPROC

/// ***************************************************************************************
///    
/// 					PRIVATE
///    
/// ***************************************************************************************  

// *******************************************
// 					PROMPT
// *******************************************

/// PURPOSE:
///    Creates the scuba rack help prompt that asks the player to press a button to equip the scuba suit
///    this will also clear any active help text so we instantly get the prompt
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC _SUBMARINE_SCUBA_RACK__CREATE_PROMPT(SUBMARINE_SCUBA_RACK &sScubaRackData, STRING strLabel)	
	CLEAR_HELP()
	REGISTER_CONTEXT_INTENTION(sScubaRackData.iContextIntention, CP_MAXIMUM_PRIORITY, strLabel)
	PRINTLN("[SUBMARINE_SCUBA_RACK] CREATE_PROMPT - Prompt created")
ENDPROC

/// PURPOSE:
///    Removes the scuba racks help text prompt
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC  _SUBMARINE_SCUBA_RACK__CLEAR_PROMPT(SUBMARINE_SCUBA_RACK &sScubaRackData)
	RELEASE_CONTEXT_INTENTION(sScubaRackData.iContextIntention)
	_SUBMARINE_SCUBA_RACK__SET_CONTEXT_INTENTION(sScubaRackData, NEW_CONTEXT_INTENTION)
	PRINTLN("[SUBMARINE_SCUBA_RACK] CLEAR_PROMPT - Prompt cleared")
ENDPROC

/// PURPOSE:
///    Checks if the current activation prompt has been activated signalling that we should equip the scuba suit
/// PARAMS:
///    sScubaRackData - the scuba rack instance
/// RETURNS:
///    TRUE if the prompt has been accepted
FUNC BOOL  _SUBMARINE_SCUBA_RACK__HAS_PROMPT_BEEN_ACCEPTED(SUBMARINE_SCUBA_RACK &sScubaRackData)
	RETURN HAS_CONTEXT_BUTTON_TRIGGERED(sScubaRackData.iContextIntention)
ENDFUNC

// *******************************************
// 					COORD CHECKS
// *******************************************

/// PURPOSE:
///    Checks if the given ped is in the trigger area according to the assigned area check function pointer
/// PARAMS:
///    sScubaRackData - the scuba rack instance
///    piPed - the ped to check
/// RETURNS:
///    TRUE if in the area OR FALSE if not in the area or the area check is not assigned
FUNC BOOL _SUBMARINE_SCUBA_RACK__IS_PED_IN_TRIGGER_AREA(SUBMARINE_SCUBA_RACK &sScubaRackData, PED_INDEX piPed)
	T_SCUBA_RACK_AREA_CHECK fpAreaCheck = SUBMARINE_SCUBA_RACK__GET_AREA_CHECK(sScubaRackData)
	
	IF fpAreaCheck = NULL
		PRINTLN("[SUBMARINE_SCUBA_RACK] IS_PED_IN_TRIGGER_AREA - No trigger area set")
		RETURN FALSE
	ENDIF
	
	RETURN CALL fpAreaCheck(piPed)
ENDFUNC

/// PURPOSE:
///    Checks to see if the given ped is facing the direction of the scuba rack according to its assigned rack position
///    this has a leeway defined by cfSUBMARINE_SCUBA_RACK_HEADING_LEEWAY
/// PARAMS:
///    sScubaRackData - the scuba rack instance
///    piPed - the ped to check
/// RETURNS:
///    TRUE if the given ped is facing the rack
FUNC BOOL _SUBMARINE_SCUBA_RACK__IS_PED_FACING_RACK(SUBMARINE_SCUBA_RACK &sScubaRackData, PED_INDEX piPed)
	VECTOR vRackCoord = SUBMARINE_SCUBA_RACK__GET_RACK_COORDS(sScubaRackData)
	VECTOR vPedCoord = GET_ENTITY_COORDS(piPed)
	FLOAT fPedHeading = GET_ENTITY_HEADING(piPed)
	FLOAT fDirHeading = GET_HEADING_BETWEEN_VECTORS_2D(vPedCoord, vRackCoord)
	RETURN IS_HEADING_ACCEPTABLE_CORRECTED(fDirHeading, fPedHeading, cfSUBMARINE_SCUBA_RACK_HEADING_LEEWAY)
ENDFUNC

/// PURPOSE:
///    Checks if the local player should recevie the prompt, based on if they are in the trigger area
///    and facing the rack
/// PARAMS:
///    sScubaRackData - the scuba rack instance
/// RETURNS:
///    TRUE if the local player is facing the rack and in its trigger area
FUNC BOOL _SUBMARINE_SCUBA_RACK__SHOULD_SHOW_PROMPT_TO_LOCAL_PLAYER(SUBMARINE_SCUBA_RACK &sScubaRackData)
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	RETURN _SUBMARINE_SCUBA_RACK__IS_PED_IN_TRIGGER_AREA(sScubaRackData, piPlayerPed) 
		AND _SUBMARINE_SCUBA_RACK__IS_PED_FACING_RACK(sScubaRackData, piPlayerPed)
ENDFUNC

/// PURPOSE:
///    Stores the players current outfit so that we can restore it later if we want to remove the scuba gear
PROC _SUBMARINE_SCUBA_RACK__STORE_CURRENT_OUTFIT_BEFORE_SETTING_SCUBA_GEAR()
	STORE_PED_CLOTHES_IN_MP_OUTFIT_SLOT(PLAYER_PED_ID(), SPARE_CUSTOM_OUTFIT_SLOT_INDEX, TRUE, TRUE)
ENDPROC

/// PURPOSE:
///    Sets the scuba outfit that will be equipped according to the stat that is altered by the 
///    submarine scuba gear selection in the pi menu
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC _SUBMARINE_SCUBA_RACK__SET_OUTFIT_TO_CHOSEN_SCUBA_GEAR(SUBMARINE_SCUBA_RACK &sScubaRackData)
	INT iSelectedScubaSuit = GET_PACKED_STAT_INT(PACKED_MP_INT_SUBMARINE_SCUBA_GEAR_SELECTION)
	sScubaRackData.eScubaOutfitComp = SUBMARINE_SCUBA_RACK__GET_SCUBA_OUTFIT_PED_COMPONENT_AT_INDEX(iSelectedScubaSuit)
ENDPROC

// *******************************************
// 					STATES
// *******************************************

/// PURPOSE:
///    Updates the scuba racks idle state where it waits until it should show the prompt to the player
///    transitions to the prompt state when the player is in the trigger area and facing the rack
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC _SUBMARINE_SCUBA_RACK__UPDATE_IDLE_STATE(SUBMARINE_SCUBA_RACK &sScubaRackData)	
	IF NOT _SUBMARINE_SCUBA_RACK__SHOULD_SHOW_PROMPT_TO_LOCAL_PLAYER(sScubaRackData)
		EXIT
	ENDIF
	
	// Show a prompt to add or remove the scuba gear
	IF	SUBMARINE_SCUBA_RACK__DOES_LOCAL_PLAYER_HAVE_SELECTED_SCUBA_GEAR_EQUIPPED()
		IF IS_MP_OUTFIT_STORED_IN_SLOT(SPARE_CUSTOM_OUTFIT_SLOT_INDEX, TRUE)
			_SUBMARINE_SCUBA_RACK__CREATE_PROMPT(sScubaRackData, "SCUBA_RACK_R")
			_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_PROMPT_REMOVE)
		ELSE
			_SUBMARINE_SCUBA_RACK__CREATE_PROMPT(sScubaRackData, "SCUBA_RACK_N")
			_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_PROMPT_NO_OUTFIT)
		ENDIF
	ELSE
		_SUBMARINE_SCUBA_RACK__CREATE_PROMPT(sScubaRackData, "SCUBA_RACK")
		_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_PROMPT_EQUIP)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the state for showing the player the equip scuba gear prompt
///    transitions back to idle if the player is not in the trigger area or not facing the rack
///    transitions to load and equip state if the player accepts the prompt
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC _SUBMARINE_SCUBA_RACK__UPDATE_PROMPT_EQUIP_STATE(SUBMARINE_SCUBA_RACK &sScubaRackData)	
	IF NOT _SUBMARINE_SCUBA_RACK__SHOULD_SHOW_PROMPT_TO_LOCAL_PLAYER(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__CLEAR_PROMPT(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_IDLE)
		EXIT
	ENDIF
	
	IF _SUBMARINE_SCUBA_RACK__HAS_PROMPT_BEEN_ACCEPTED(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__STORE_CURRENT_OUTFIT_BEFORE_SETTING_SCUBA_GEAR()
		_SUBMARINE_SCUBA_RACK__SET_OUTFIT_TO_CHOSEN_SCUBA_GEAR(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_LOAD_AND_EQUIP)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the state for showing the player the remove scuba gear prompt
///    transitions back to idle if the player is not in the trigger area or not facing the rack
///    transitions to load and remove state if the player accepts the prompt
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC _SUBMARINE_SCUBA_RACK__UPDATE_PROMPT_REMOVE_STATE(SUBMARINE_SCUBA_RACK &sScubaRackData)	
	IF NOT _SUBMARINE_SCUBA_RACK__SHOULD_SHOW_PROMPT_TO_LOCAL_PLAYER(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__CLEAR_PROMPT(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_IDLE)
		EXIT
	ENDIF
	
	IF _SUBMARINE_SCUBA_RACK__HAS_PROMPT_BEEN_ACCEPTED(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_LOAD_AND_REMOVE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the state for showing help text telling the player they have no outfit available
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC _SUBMARINE_SCUBA_RACK__UPDATE_PROMPT_NO_OUTFIT(SUBMARINE_SCUBA_RACK &sScubaRackData)	
	IF NOT _SUBMARINE_SCUBA_RACK__SHOULD_SHOW_PROMPT_TO_LOCAL_PLAYER(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__CLEAR_PROMPT(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_IDLE)
		EXIT
	ENDIF
	
	// In trigger but player changed scuba selection in PI menu so go to normal prompt
	IF NOT SUBMARINE_SCUBA_RACK__DOES_LOCAL_PLAYER_HAVE_SELECTED_SCUBA_GEAR_EQUIPPED()
		_SUBMARINE_SCUBA_RACK__CLEAR_PROMPT(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__CREATE_PROMPT(sScubaRackData, "SCUBA_RACK")
		_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_PROMPT_EQUIP)
	ELIF IS_MP_OUTFIT_STORED_IN_SLOT(SPARE_CUSTOM_OUTFIT_SLOT_INDEX, TRUE)
		_SUBMARINE_SCUBA_RACK__CLEAR_PROMPT(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__CREATE_PROMPT(sScubaRackData, "SCUBA_RACK_R")
		_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_PROMPT_REMOVE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the tshirt of the local player, this is required after setting a scuba outfit
PROC  _SUBMARINE_SCUBA_RACK__CLEAR_TSHIRT()
	IF IS_PLAYER_FEMALE()
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0)
		CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EYES)
	ELSE	
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 15, 0)
		CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EYES)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the load and equip state for the scuba rack that processes equipping the players scuba gear
///    transitions to the idle state when the outfit is equipped
///    will not run if the players ped is injured
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC _SUBMARINE_SCUBA_RACK__UPDATE_LOAD_AND_EQUIP_STATE(SUBMARINE_SCUBA_RACK &sScubaRackData)	
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	
	IF IS_PED_INJURED(piPlayerPed)
		EXIT
	ENDIF
	
	// Wait for outfit to equip
	IF SET_PED_COMP_ITEM_CURRENT_MP(piPlayerPed, COMP_TYPE_OUTFIT, sScubaRackData.eScubaOutfitComp, FALSE)
		_SUBMARINE_SCUBA_RACK__CLEAR_TSHIRT()
		UPDATE_TATOOS_MP(piPlayerPed)
		_SUBMARINE_SCUBA_RACK__CLEAR_PROMPT(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__CREATE_PROMPT(sScubaRackData, "SCUBA_RACK_R")
		_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_PROMPT_REMOVE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the load and remove state for the scuba rack that processes removing the players scuba gear
///    transitions to the idle state when the previous outfit has been equipped
///    will not run if the players ped is injured
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC _SUBMARINE_SCUBA_RACK__UPDATE_LOAD_AND_REMOVE_STATE(SUBMARINE_SCUBA_RACK &sScubaRackData)	
	PED_INDEX piPlayerPed = PLAYER_PED_ID()
	
	IF IS_PED_INJURED(piPlayerPed)
		EXIT
	ENDIF
	
	// Preload then apply and clear stored outfit
	IF PRELOAD_MP_OUTFIT_STORED_IN_SLOT(PLAYER_PED_ID(), SPARE_CUSTOM_OUTFIT_SLOT_INDEX, TRUE)
		SET_MP_OUTFIT_STORED_IN_SLOT(PLAYER_PED_ID(), SPARE_CUSTOM_OUTFIT_SLOT_INDEX, FALSE, TRUE)
		REMOVE_MP_OUTFIT_IN_SLOT(SPARE_CUSTOM_OUTFIT_SLOT_INDEX, TRUE)
		_SUBMARINE_SCUBA_RACK__CLEAR_PROMPT(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__CREATE_PROMPT(sScubaRackData, "SCUBA_RACK")
		_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_PROMPT_EQUIP)
	ENDIF
ENDPROC

/// PURPOSE:
///    Update state for waiting for the player to exit the trigger area before transitioning to idle
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC _SUBMARINE_SCUBA_RACK__WAIT_FOR_TRIGGER_EXIT_STATE(SUBMARINE_SCUBA_RACK &sScubaRackData)	
	// Wait until we exit the trigger before showing any further prompts
	IF NOT _SUBMARINE_SCUBA_RACK__IS_PED_IN_TRIGGER_AREA(sScubaRackData, PLAYER_PED_ID())
		_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_IDLE)
	ENDIF
ENDPROC

/// ***************************************************************************************
///    
/// 					PUBLIC
///    
/// ***************************************************************************************   

/// PURPOSE:
///    Checks if the scuba rack is currently showing its prompt
/// PARAMS:
///    sScubaRackData - the scuba rack instance
/// RETURNS:
///    TRUE if the scuba rack is showing its prompt
FUNC BOOL SUBMARINE_SCUBA_RACK__IS_PROMPT_ACTIVE(SUBMARINE_SCUBA_RACK &sScubaRackData)
	RETURN _SUBMARINE_SCUBA_RACK__GET_CONTEXT_INTENTION(sScubaRackData) != NEW_CONTEXT_INTENTION
ENDFUNC 

/// PURPOSE:
///    Initializes the scuba rack with its trigger volume and position, this must be called before attempting to use 
///    SUBMARINE_SCUBA_RACK__MAINTAIN
/// PARAMS:
///    sScubaRackData - the scuba rack instance
///    fpAreaCheck - a function with the signature BOOL T_SCUBA_RACK_AREA_CHECK(PED_INDEX piPed) that returns true when the given ped is in
///    				 the scuba racks trigger volume
///    vRackCoords - the position of the rack the player must face to activate it
PROC SUBMARINE_SCUBA_RACK__INIT(SUBMARINE_SCUBA_RACK &sScubaRackData, T_SCUBA_RACK_AREA_CHECK fpAreaCheck, VECTOR vRackCoords)
	SUBMARINE_SCUBA_RACK__SET_AREA_CHECK(sScubaRackData, fpAreaCheck)
	SUBMARINE_SCUBA_RACK__SET_RACK_COORDS(sScubaRackData, vRackCoords)
	_SUBMARINE_SCUBA_RACK__SET_STATE(sScubaRackData, SSRS_IDLE)
	PRINTLN("[SUBMARINE_SCUBA_RACK] INIT - Finished")
ENDPROC

/// PURPOSE:
///    Updates the scuba rack that allows the player to equip their chosen scuba gear in the PI menu
///    the black scuba gear is unlocked by default. This will not update unless SUBMARINE_SCUBA_RACK__INIT is called
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC SUBMARINE_SCUBA_RACK__MAINTAIN(SUBMARINE_SCUBA_RACK &sScubaRackData)	
	SWITCH SUBMARINE_SCUBA_RACK__GET_STATE(sScubaRackData)
		CASE SSRS_UNINTITALIZED
			#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 120 = 0
					PRINTLN("[SUBMARINE_SCUBA_RACK] MAINTAIN - Not initialized")
				ENDIF
			#ENDIF
		BREAK
		CASE SSRS_IDLE
			_SUBMARINE_SCUBA_RACK__UPDATE_IDLE_STATE(sScubaRackData)
		BREAK
		CASE SSRS_PROMPT_EQUIP
			_SUBMARINE_SCUBA_RACK__UPDATE_PROMPT_EQUIP_STATE(sScubaRackData)
		BREAK
		CASE SSRS_PROMPT_REMOVE
			_SUBMARINE_SCUBA_RACK__UPDATE_PROMPT_REMOVE_STATE(sScubaRackData)
		BREAK
		CASE SSRS_PROMPT_NO_OUTFIT
			_SUBMARINE_SCUBA_RACK__UPDATE_PROMPT_NO_OUTFIT(sScubaRackData)
		BREAK
		CASE SSRS_LOAD_AND_EQUIP
			_SUBMARINE_SCUBA_RACK__UPDATE_LOAD_AND_EQUIP_STATE(sScubaRackData)
		BREAK
		CASE SSRS_LOAD_AND_REMOVE
			_SUBMARINE_SCUBA_RACK__UPDATE_LOAD_AND_REMOVE_STATE(sScubaRackData)
		BREAK
		CASE SSRS_WAIT_FOR_TRIGGER_EXIT
			_SUBMARINE_SCUBA_RACK__WAIT_FOR_TRIGGER_EXIT_STATE(sScubaRackData)
		BREAK
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Cleans up the scuba rack releasing all assets, after cleanup the rack needs to be
///    reinitialized with SUBMARINE_SCUBA_RACK__INIT if you need to reuse it
/// PARAMS:
///    sScubaRackData - the scuba rack instance
PROC SUBMARINE_SCUBA_RACK__CLEANUP(SUBMARINE_SCUBA_RACK &sScubaRackData)	
	// Cleanup context intention if active
	IF SUBMARINE_SCUBA_RACK__IS_PROMPT_ACTIVE(sScubaRackData)
		_SUBMARINE_SCUBA_RACK__CLEAR_PROMPT(sScubaRackData)
	ENDIF
	
	// Do complete struct clear
	SUBMARINE_SCUBA_RACK sEmpty
	sScubaRackData = sEmpty
	PRINTLN("[SUBMARINE_SCUBA_RACK] CLEANUP - Finished")
ENDPROC

#ENDIF // FEATURE_HEIST_ISLAND
