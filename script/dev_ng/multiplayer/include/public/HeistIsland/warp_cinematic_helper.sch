#IF FEATURE_HEIST_ISLAND
USING "lsia_take_off_cinematic_typedefs.sch"
USING "rc_helper_functions.sch"
USING "globals.sch"
USING "net_cutscene.sch"
USING "net_freemode_cut.sch"
USING "cutscene_help.sch"

ENUM WARP_CINEMATIC_STATE
	WCS_INVALID = -1
	,WCS_INIT
	,WCS_REGISTER_PLAYER_AND_LOAD_CUTSCENE
	,WCS_FADE_IN
	,WCS_WAIT_FOR_CINEMATIC_END
	,WCS_FADE_OUT
ENDENUM

DEBUGONLY FUNC STRING DEBUG_GET_WARP_CINEMATIC_STATE_AS_STRING(WARP_CINEMATIC_STATE eEnum)
	SWITCH eEnum
		CASE WCS_INVALID				RETURN	"WCS_INVALID"
		CASE WCS_INIT					RETURN	"WCS_INIT"
		CASE WCS_REGISTER_PLAYER_AND_LOAD_CUTSCENE	RETURN "WCS_REGISTER_PLAYER_AND_LOAD_CUTSCENE"
		CASE WCS_FADE_IN				RETURN  "WCS_FADE_IN"
		CASE WCS_WAIT_FOR_CINEMATIC_END	RETURN	"WCS_WAIT_FOR_CINEMATIC_END"
		CASE WCS_FADE_OUT				RETURN	"WCS_FADE_OUT"
	ENDSWITCH

	ASSERTLN("GET_WARP_CINEMATIC_STATE_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

STRUCT WARP_CINEMATIC_STRUCT
	WARP_CINEMATIC_STATE eState
	STRING strCutscene
	
	// TODO: swap for bitset
	BOOL bHasSections
	BOOL bRenablePlayerOnFadeout = FALSE
	BOOL bSetPlayerInCutscene
	BOOL bSceneContainsPlayer = TRUE
	BOOL bMuteRadio = FALSE
	BOOL bSkippable = FALSE
	BOOL bShouldPedBeVisibleByDefault = TRUE
	
	VECTOR vLoadScene
	VECTOR vLoadSceneRot
	BOOL bLoadSceneStarted = FALSE
	BOOL bCutsceneRequested = FALSE
	CUTSCENE_SECTION eCutsceneSections
	SCRIPT_TIMER sLoadBail
	
	MODEL_NAMES eVehicleModel = DUMMY_MODEL_FOR_SCRIPT
	STRING sVehicleHandle
	PED_INDEX piScriptPedForVeh
	PED_INDEX piPlayerPed
	VEHICLE_INDEX viScriptVeh
ENDSTRUCT

///----------------------------------------------------------------------------
///    SETTERS
///----------------------------------------------------------------------------    

PROC WARP_CINEMATIC__SET_STATE(WARP_CINEMATIC_STRUCT &sInst, WARP_CINEMATIC_STATE eNewState)
	PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "] SET_STATE - ", sInst.eState, " -> ", eNewState)
	sInst.eState = eNewState
ENDPROC


///----------------------------------------------------------------------------
///    GETTERS
///---------------------------------------------------------------------------- 

FUNC WARP_CINEMATIC_STATE WARP_CINEMATIC__GET_STATE(WARP_CINEMATIC_STRUCT &sInst)
	RETURN sInst.eState
ENDFUNC

FUNC PED_INDEX WARP_CINEMATIC__GET_PLAYER_PED_FOR_CUTSCENE()	
	PED_INDEX pedId = PLAYER_PED_ID()
	
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		pedId = GET_PLAYER_PED(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	ENDIF
	
	RETURN pedId
ENDFUNC

FUNC PLAYER_INDEX WARP_CINEMATIC__GET_PLAYER_INDEX_FOR_CUTSCENE()
	
	PLAYER_INDEX playerIndex = PLAYER_ID()
	
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		playerIndex = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	ENDIF
	
	RETURN playerIndex
ENDFUNC

FUNC BOOL WARP_CINEMATIC__CREATE_PLAYER_PED(WARP_CINEMATIC_STRUCT &sInst)
	PRINTLN("[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_PLAYER_PED")
	IF NOT sInst.bSceneContainsPlayer
		PRINTLN("[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_PLAYER_PED - No Player in this scene")
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(sInst.piPlayerPed)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(WARP_CINEMATIC__GET_PLAYER_PED_FOR_CUTSCENE())
		PRINTLN("[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_PLAYER_PED - Ped for cutscene is dead or missing!")
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX piPlayerIndex = WARP_CINEMATIC__GET_PLAYER_INDEX_FOR_CUTSCENE()
	IF piPlayerIndex = INVALID_PLAYER_INDEX()
		PRINTLN("[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_PLAYER_PED - Player index is invalid!")
		RETURN FALSE
	ENDIF
	
	IF piPlayerIndex = PLAYER_ID()
		sInst.piPlayerPed = CLONE_PED_ALT(WARP_CINEMATIC__GET_PLAYER_PED_FOR_CUTSCENE(), FALSE, FALSE, TRUE, TRUE)
		IF NOT DOES_ENTITY_EXIST(sInst.piPlayerPed)
			CASSERTLN(DEBUG_SYSTEM, "[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_PLAYER_PED - Failed to clone local player ped for cutscene.")
			RETURN FALSE
		ENDIF
	ELSE
		sInst.piPlayerPed = CLONE_PED_ALT(WARP_CINEMATIC__GET_PLAYER_PED_FOR_CUTSCENE(), FALSE, FALSE, FALSE, FALSE)
		IF NOT DOES_ENTITY_EXIST(sInst.piPlayerPed)
			CASSERTLN(DEBUG_SYSTEM, "[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_PLAYER_PED - Failed to clone remlote boss ped for cutscene.")
			RETURN FALSE
		ENDIF
		// If we are cloning the gang boss we should apply their cached tattoos since we don't have linked blends and compressed damages turned on as we need to apply outfits with overlays later.
		SET_MP_TATTOOS_FROM_DATA(sInst.piPlayerPed, GlobalplayerBD_Tattoos[NATIVE_TO_INT(piPlayerIndex)].iPlayerTattooData)
	ENDIF
	
	SET_ENTITY_VISIBLE(sInst.piPlayerPed, sInst.bShouldPedBeVisibleByDefault)
	SET_ENTITY_INVINCIBLE(sInst.piPlayerPed, TRUE)
	SET_ENTITY_COLLISION(sInst.piPlayerPed, FALSE)
	SET_PED_HELMET(sInst.piPlayerPed, FALSE)
	REMOVE_PED_HELMET(sInst.piPlayerPed, TRUE)
	RETURN TRUE
ENDFUNC

FUNC BOOL WARP_CINEMATIC__CREATE_VEHICLE(WARP_CINEMATIC_STRUCT &sInst)
	IF DOES_ENTITY_EXIST(sInst.viScriptVeh)
		RETURN TRUE
	ENDIF
	
	IF sInst.eVehicleModel = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_VEHICLE - No vehicle in this scene")
		RETURN TRUE
	ENDIF
	
	REQUEST_MODEL(sInst.eVehicleModel)
	IF NOT HAS_MODEL_LOADED(sInst.eVehicleModel)
		PRINTLN("[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_VEHICLE - Loading vehicle model ", GET_MODEL_NAME_FOR_DEBUG(sInst.eVehicleModel))
		RETURN FALSE
	ENDIF
	
	sInst.viScriptVeh = CREATE_VEHICLE(sInst.eVehicleModel, sInst.vLoadScene, 0.0, FALSE, FALSE)
	SET_ENTITY_INVINCIBLE(sInst.viScriptVeh, TRUE)
	SET_ENTITY_COLLISION(sInst.viScriptVeh, FALSE)
	FREEZE_ENTITY_POSITION(sInst.viScriptVeh, TRUE)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(sInst.eVehicleModel)
	
	PRINTLN("[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_VEHICLE - Vehicle created")			
	RETURN TRUE
ENDFUNC

FUNC BOOL WARP_CINEMATIC__CREATE_PED_FOR_VEHICLE(WARP_CINEMATIC_STRUCT &sInst)
	IF sInst.eVehicleModel = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_PED_FOR_VEHICLE - No vehicle in this scene")
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(sInst.piScriptPedForVeh)
		RETURN TRUE
	ENDIF
	
	REQUEST_MODEL(GET_PLAYER_MODEL())
	IF NOT HAS_MODEL_LOADED(GET_PLAYER_MODEL())
		PRINTLN("[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_PED_FOR_VEHICLE - Loading player model ", GET_MODEL_NAME_FOR_DEBUG(GET_PLAYER_MODEL()))
		RETURN FALSE
	ENDIF
	
	sInst.piScriptPedForVeh = CREATE_PED(PEDTYPE_CIVMALE, GET_PLAYER_MODEL(), sInst.vLoadScene, 0.0, FALSE, FALSE)
	SET_ENTITY_INVINCIBLE(sInst.piScriptPedForVeh, TRUE)
	SET_ENTITY_VISIBLE(sInst.piScriptPedForVeh, FALSE)
	SET_ENTITY_COLLISION(sInst.piScriptPedForVeh, FALSE)
	FREEZE_ENTITY_POSITION(sInst.piScriptPedForVeh, TRUE)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_MODEL())
	
	PRINTLN("[WARP_CINEMATIC] WARP_CINEMATIC__CREATE_PED_FOR_VEHICLE - Ped created")
	RETURN TRUE
ENDFUNC

PROC _WARP_CINEMATIC__REQUEST_CUTSCENE(WARP_CINEMATIC_STRUCT &sInst)
	
	IF NOT sInst.bCutsceneRequested
		IF IS_STRING_NULL_OR_EMPTY(sInst.strCutscene)
			ASSERTLN("[WARP_CINEMATIC] REQUEST_CUTSCENE - Cutscene string is null or empty, skipping cutscene")
			WARP_CINEMATIC__SET_STATE(sInst, WCS_FADE_OUT)
			EXIT
		ENDIF
	
		STRING strCutscene = sInst.strCutscene
		
		IF sInst.bHasSections
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(strCutscene, sInst.eCutsceneSections,CUTSCENE_REQUESTED_IN_MISSION|CUTSCENE_PLAYBACK_FORCE_LOAD_AUDIO_EVENT)
		ELSE
			REQUEST_CUTSCENE(strCutscene)
		ENDIF
		
		sInst.bCutsceneRequested = TRUE
	ENDIF

ENDPROC

PROC _WARP_CINEMATIC__MAINTAIN_STREAMING_AT_CURRENT_CAMERA_COORDS()
	SET_FOCUS_POS_AND_VEL(GET_FINAL_RENDERED_CAM_COORD(), <<0.0, 0.0, 0.0>>)
ENDPROC

PROC _WARP_CINEMATIC__SETUP_CUTSCENE_VEHICLE(WARP_CINEMATIC_STRUCT &sInst)
	IF sInst.eVehicleModel != DUMMY_MODEL_FOR_SCRIPT
		IF IS_ENTITY_ALIVE(sInst.viScriptVeh)
			VEHICLE_SETUP_STRUCT_MP sData
			
			SWITCH sInst.eVehicleModel
				CASE NIMBUS
					SET_VEHICLE_COLOURS(sInst.viScriptVeh, 111, 28)
					SET_VEHICLE_EXTRA_COLOURS(sInst.viScriptVeh, 1, 6)
					BREAK
				CASE VELUM2
					sData.VehicleSetup.eModel = VELUM2
					sData.VehicleSetup.tlPlateText = "24SGX816"
					sData.VehicleSetup.iColour1 = 111
					sData.VehicleSetup.iColour2 = 27
					sData.VehicleSetup.iColourExtra1 = 111
					sData.VehicleSetup.iColourExtra2 = 0
					sData.iColour5 = 1
					sData.iColour6 = 132
					sData.iLivery2 = 0
					sData.VehicleSetup.iLivery = 2
					sData.VehicleSetup.iTyreR = 255
					sData.VehicleSetup.iTyreG = 255
					sData.VehicleSetup.iTyreB = 255
					sData.VehicleSetup.iNeonR = 255
					sData.VehicleSetup.iNeonB = 255
					SET_VEHICLE_SETUP_MP(sInst.viScriptVeh, sData, FALSE)
					BREAK
			ENDSWITCH
			
			REGISTER_ENTITY_FOR_CUTSCENE(sInst.viScriptVeh, sInst.sVehicleHandle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
			PRINTLN("[WARP_CINEMATIC] _WARP_CINEMATIC__SETUP_CUTSCENE_VEHICLE - Set up ", GET_MODEL_NAME_FOR_DEBUG(sInst.eVehicleModel))
		ENDIF
	ENDIF
ENDPROC

PROC _WARP_CINEMATIC__MAINTAIN_CUTSCENE_VEHICLES(WARP_CINEMATIC_STRUCT &sInst)
	IF sInst.eVehicleModel = DUMMY_MODEL_FOR_SCRIPT
		EXIT
	ENDIF
	
	IF NOT IS_CUTSCENE_PLAYING()
		EXIT
	ENDIF
	
	STRING strJetHandle = PICK_STRING(sInst.eVehicleModel = NIMBUS, "Plane_1", "Plane")
	
	IF DOES_CUTSCENE_HANDLE_EXIST(strJetHandle) != CHSR_HANDLE_EXISTS
		EXIT
	ENDIF
	
	sInst.viScriptVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY(strJetHandle, sInst.eVehicleModel))
	
	IF NOT DOES_ENTITY_EXIST(sInst.viScriptVeh)
		EXIT
	ENDIF
		
	IF NOT IS_VEHICLE_DRIVEABLE(sInst.viScriptVeh)
		EXIT
	ENDIF
	
	// Create pilot if not done already
	WARP_CINEMATIC__CREATE_PED_FOR_VEHICLE(sInst)
	
	// Add invisible pilot and turn off the lights
	IF GET_VEHICLE_DOOR_ANGLE_RATIO(sInst.viScriptVeh, SC_DOOR_FRONT_LEFT) < 0.0001
		SET_VEHICLE_ENGINE_ON(sInst.viScriptVeh, TRUE, TRUE, FALSE)
		SET_VEHICLE_LIGHTS(sInst.viScriptVeh, FORCE_VEHICLE_LIGHTS_ON)
	
		IF DOES_ENTITY_EXIST(sInst.piScriptPedForVeh)
		AND NOT IS_PED_INJURED(sInst.piScriptPedForVeh)
		AND NOT IS_PED_IN_VEHICLE(sInst.piScriptPedForVeh, sInst.viScriptVeh)
			SET_ENTITY_COLLISION(sInst.piScriptPedForVeh, TRUE)
			FREEZE_ENTITY_POSITION(sInst.piScriptPedForVeh, FALSE)
			TASK_ENTER_VEHICLE(sInst.piScriptPedForVeh, sInst.viScriptVeh, -1, VS_DRIVER, PEDMOVE_SPRINT, ECF_WARP_PED)
		ENDIF
	ELSE
		SET_VEHICLE_ENGINE_ON(sInst.viScriptVeh, FALSE, TRUE, FALSE)
		SET_VEHICLE_LIGHTS(sInst.viScriptVeh, SET_VEHICLE_LIGHTS_OFF)
		
		IF DOES_ENTITY_EXIST(sInst.piScriptPedForVeh)
		AND NOT IS_PED_INJURED(sInst.piScriptPedForVeh)
		AND IS_PED_IN_VEHICLE(sInst.piScriptPedForVeh, sInst.viScriptVeh)
			TASK_LEAVE_VEHICLE(sInst.piScriptPedForVeh, sInst.viScriptVeh, ECF_WARP_PED)
		ENDIF
	ENDIF
	
	// Set tint
	SWITCH sInst.eVehicleModel
		CASE NIMBUS
			SET_VEHICLE_COLOURS(sInst.viScriptVeh, 111, 28)
			SET_VEHICLE_EXTRA_COLOURS(sInst.viScriptVeh, 1, 6)
			BREAK
		CASE VELUM2
			SET_VEHICLE_COLOURS(sInst.viScriptVeh, 111, 27)
			SET_VEHICLE_EXTRA_COLOURS(sInst.viScriptVeh, 111, 0)
			SET_VEHICLE_LIVERY(sInst.viScriptVeh, 2)
			BREAK
	ENDSWITCH
ENDPROC

PROC _WARP_CINEMATIC__MAINTAIN_PLAYER_VISIBILITY(WARP_CINEMATIC_STRUCT &sInst)
	
	IF NOT IS_CUTSCENE_PLAYING()
		EXIT
	ENDIF
	
	IF sInst.bSetPlayerInCutscene AND NOT IS_ENTITY_VISIBLE(WARP_CINEMATIC__GET_PLAYER_PED_FOR_CUTSCENE())
		SET_ENTITY_VISIBLE(WARP_CINEMATIC__GET_PLAYER_PED_FOR_CUTSCENE(), TRUE)
		SET_ENTITY_VISIBLE_IN_CUTSCENE(WARP_CINEMATIC__GET_PLAYER_PED_FOR_CUTSCENE(), TRUE)
	ENDIF
ENDPROC

PROC _WARP_CINEMATIC__UPDATE_INIT_STATE(WARP_CINEMATIC_STRUCT &sInst)
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_SkipWarpTransitionCinematic")
			WARP_CINEMATIC__SET_STATE(sInst, WCS_FADE_OUT)
			EXIT
		ENDIF
	#ENDIF
	
	IF IS_PLAYER_RESPAWNING(PLAYER_ID())
		PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "] UPDATE_INIT_STATE - Player dead, waiting for respawn...")
		EXIT
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF
	
	IF NOT WARP_CINEMATIC__CREATE_PLAYER_PED(sInst)
		PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "] UPDATE_REGISTER_PLAYER_AND_LOAD_CUTSCENE_STATE - Creating player...")
		EXIT
	ENDIF
	
	
	IF NOT WARP_CINEMATIC__CREATE_PED_FOR_VEHICLE(sInst)
		PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "] UPDATE_REGISTER_PLAYER_AND_LOAD_CUTSCENE_STATE - Creating pilot ped...")
		EXIT
	ENDIF

	IF NOT sInst.bLoadSceneStarted
		IF NOT NEW_LOAD_SCENE_START(sInst.vLoadScene,  CONVERT_ROTATION_TO_DIRECTION_VECTOR(sInst.vLoadSceneRot), 4000)
			PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "] UPDATE_INIT_STATE - Load scene starting...")
			EXIT
		ELSE
			sInst.bLoadSceneStarted = TRUE
			PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "] UPDATE_INIT_STATE - Load scene started at ", sInst.vLoadScene, ", rot = ", sInst.vLoadSceneRot)
		ENDIF
	ENDIF
	
	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
	OR NOT IS_NEW_LOAD_SCENE_LOADED() 
		PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "]  UPDATE_INIT_STATE - Load scene loading...")
		
		IF NOT HAS_NET_TIMER_EXPIRED(sInst.sLoadBail, g_sMPTunables.iWARP_CINEMATIC_LOAD_TIMEOUT)
			EXIT
		ELSE
			ASSERTLN("[WARP_CINEMATIC][", sInst.strCutscene, "]  UPDATE_INIT_STATE - Exceeded load time, skipping through load scene")
		ENDIF
	ELSE
		NEW_LOAD_SCENE_STOP()
		PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "]  UPDATE_INIT_STATE - Load scene complete!")
	ENDIF

	RESET_NET_TIMER(sInst.sLoadBail)
	WARP_CINEMATIC__SET_STATE(sInst, WCS_REGISTER_PLAYER_AND_LOAD_CUTSCENE)
	PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "] UPDATE_INIT_STATE - Finished, cutscene duration: ", GET_CUTSCENE_TOTAL_DURATION())
ENDPROC

PROC _WARP_CINEMATIC__UPDATE_REGISTER_PLAYER_AND_LOAD_CUTSCENE_STATE(WARP_CINEMATIC_STRUCT &sInst)
	_WARP_CINEMATIC__REQUEST_CUTSCENE(sInst)
	
	IF sInst.bSceneContainsPlayer	
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
		ENDIF
	ENDIF
	
	IF NOT HAS_THIS_CUTSCENE_LOADED(sInst.strCutscene)
		PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "]  UPDATE_REGISTER_PLAYER_AND_LOAD_CUTSCENE_STATE - Cutscene loading...")
		
		IF HAS_NET_TIMER_EXPIRED(sInst.sLoadBail, g_sMPTunables.iWARP_CINEMATIC_LOAD_TIMEOUT)
			ASSERTLN("[WARP_CINEMATIC][", sInst.strCutscene, "]  UPDATE_REGISTER_PLAYER_AND_LOAD_CUTSCENE_STATE - Exceeded load time loading cutscene, skipping through cutscene")
		ENDIF
		
		EXIT
	ENDIF	
	
	IF sInst.bSceneContainsPlayer	
		IF NOT IS_ENTITY_ALIVE(sInst.piPlayerPed)
			RESURRECT_PED(sInst.piPlayerPed)
			PRINTLN("[WARP_CINEMATIC] MAINTAIN_SETTING_PLAYER - Player ped was dead, resurrecting")
		ENDIF
			
		REGISTER_ENTITY_FOR_CUTSCENE(sInst.piPlayerPed, "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
	ENDIF
	
	_WARP_CINEMATIC__SETUP_CUTSCENE_VEHICLE(sInst)
	START_MP_CUTSCENE()
	START_CUTSCENE()
	RESET_NET_TIMER(sInst.sLoadBail)
	CLEAR_AREA_OF_PROJECTILES(sInst.vLoadScene, 100)
	_WARP_CINEMATIC__MAINTAIN_STREAMING_AT_CURRENT_CAMERA_COORDS()
	WARP_CINEMATIC__SET_STATE(sInst, WCS_FADE_IN)
	PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "] UPDATE_REGISTER_PLAYER_AND_LOAD_CUTSCENE_STATE - Finished, cutscene duration: ", GET_CUTSCENE_TOTAL_DURATION())
ENDPROC

PROC _WARP_CINEMATIC__UPDATE_FADE_IN_STATE(WARP_CINEMATIC_STRUCT &sInst)
	_WARP_CINEMATIC__MAINTAIN_STREAMING_AT_CURRENT_CAMERA_COORDS()
	DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
	WARP_CINEMATIC__SET_STATE(sInst, WCS_WAIT_FOR_CINEMATIC_END)
	PRINTLN("_WARP_CINEMATIC__UPDATE_FADE_IN_STATE - fading in")
ENDPROC

PROC _WARP_CINEMATIC__UPDATE_PLANE_LEAVE_STATE(WARP_CINEMATIC_STRUCT &sInst)
	
	_WARP_CINEMATIC__MAINTAIN_STREAMING_AT_CURRENT_CAMERA_COORDS()
	_WARP_CINEMATIC__MAINTAIN_CUTSCENE_VEHICLES(sInst)
	
	BOOL bCanSkip = sInst.bSkippable
	
//	// Can always skip in debug mode
	#IF IS_DEBUG_BUILD
		bCanSkip = TRUE
	#ENDIF

	IF bCanSkip
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
			WARP_CINEMATIC__SET_STATE(sInst, WCS_FADE_OUT)
			PRINTLN("_WARP_CINEMATIC__UPDATE_PLANE_LEAVE_STATE - Cutscene skip pressed, skipping...")
		ENDIF
	ENDIF
	
	IF NOT IS_CUTSCENE_PLAYING() 
	OR GET_CUTSCENE_TIME() > GET_CUTSCENE_END_TIME() - DEFAULT_FADE_TIME
		IF DOES_ENTITY_EXIST(sInst.viScriptVeh)
			IF NOT IS_VEHICLE_EMPTY(sInst.viScriptVeh)
				EMPTY_VEHICLE(sInst.viScriptVeh)
			ENDIF
		ENDIF
		
		WARP_CINEMATIC__SET_STATE(sInst, WCS_FADE_OUT)
	ELSE
		PRINTLN("_WARP_CINEMATIC__UPDATE_PLANE_LEAVE_STATE - waiting for cutscene to be completed")
	ENDIF
ENDPROC

FUNC BOOL _WARP_CINEMATIC__UPDATE_FADE_OUT_STATE(WARP_CINEMATIC_STRUCT &sInst)
	_WARP_CINEMATIC__MAINTAIN_STREAMING_AT_CURRENT_CAMERA_COORDS()
	
	IF IS_SCREEN_FADED_OUT()
	
		IF sInst.bRenablePlayerOnFadeout
			CLEANUP_MP_CUTSCENE(TRUE,FALSE)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		ENDIF		
		
		STOP_CUTSCENE()
		RETURN TRUE
	ENDIF
		
	IF NOT IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

PROC WARP_CINEMATIC__INIT_CINEMATIC(WARP_CINEMATIC_STRUCT &sInst, STRING strCutsceneName, BOOL bEnablePlayerOnEnd = FALSE, BOOL bHasSections = FALSE, CUTSCENE_SECTION eSections = CS_SECTION_1)
	sInst.strCutscene = strCutsceneName
	sInst.bHasSections = bHasSections
	sInst.eCutsceneSections = eSections
	sInst.bRenablePlayerOnFadeout = bEnablePlayerOnEnd	
	sInst.bCutsceneRequested = FALSE
	RESET_NET_TIMER(sInst.sLoadBail)
	PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "] INIT_CINEMATIC - Initialized")
ENDPROC

PROC WARP_CINEMATIC__INIT_CINEMATIC_WITH_LOAD_SCENE(WARP_CINEMATIC_STRUCT &sInst, VECTOR vLoadPos, VECTOR vLoadRot, STRING strCutsceneName,
BOOL bEnablePlayerOnEnd = FALSE, BOOL bHasSections = FALSE, CUTSCENE_SECTION eSections = CS_SECTION_1, BOOL bSkippable = FALSE)
	
	WARP_CINEMATIC__INIT_CINEMATIC(sInst, strCutsceneName, bEnablePlayerOnEnd, bHasSections, eSections)
	sInst.vLoadScene = vLoadPos
	sInst.vLoadSceneRot = vLoadRot
	sInst.bSkippable = bSkippable
ENDPROC

PROC WARP_CINEMATIC__SET_MUTE_RADIO(WARP_CINEMATIC_STRUCT &sInst, BOOL bMute)
	sInst.bMuteRadio = bMute
	
	IF bMute
		IF NOT IS_AUDIO_SCENE_ACTIVE("MUTES_RADIO_SCENE")
			START_AUDIO_SCENE("MUTES_RADIO_SCENE")
		ENDIF
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE("MUTES_RADIO_SCENE")
			STOP_AUDIO_SCENE("MUTES_RADIO_SCENE")
		ENDIF
	ENDIF
ENDPROC

PROC WARP_CINEMATIC__SET_CINEMATIC_VEHICLE_DATA(WARP_CINEMATIC_STRUCT &sInst, MODEL_NAMES eVehicleModel, STRING sVehicleHandle)
	sInst.eVehicleModel = eVehicleModel
	sInst.sVehicleHandle = sVehicleHandle
ENDPROC

PROC WARP_CINEMATIC__CLEANUP_CINEMATIC(WARP_CINEMATIC_STRUCT &sInst)	
	
	IF IS_CUTSCENE_ACTIVE()
		STOP_CUTSCENE_IMMEDIATELY()
	ENDIF
	
	IF DOES_ENTITY_EXIST(sInst.piScriptPedForVeh)
		DELETE_PED(sInst.piScriptPedForVeh)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sInst.piPlayerPed)
		DELETE_PED(sInst.piPlayerPed)
	ENDIF
	
	WARP_CINEMATIC__SET_MUTE_RADIO(sInst, FALSE)
	
	REMOVE_CUTSCENE()	
	
	NEW_LOAD_SCENE_STOP()
	CLEAR_FOCUS()
	
	IF THEFEED_IS_PAUSED()
		// Enable the ticker feed.
		THEFEED_RESUME()
	ENDIF
	
	SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	
	RESET_NET_TIMER(sInst.sLoadBail)
	CLEANUP_MP_CUTSCENE(TRUE, FALSE)
	WARP_CINEMATIC_STRUCT sEmpty
	sInst = sEmpty
	PRINTLN("[WARP_CINEMATIC][", sInst.strCutscene, "] CLEANUP_CINEMATIC - Cleaned up")
ENDPROC

PROC WARP_CINEMATIC__SUPRESS_SYSTEMS()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	DISABLE_ALL_MP_HUD_THIS_FRAME()
	DISABLE_FRONTEND_THIS_FRAME()
	
	IF IS_PAUSE_MENU_ACTIVE()
		SET_PAUSE_MENU_ACTIVE(FALSE)
		SET_FRONTEND_ACTIVE(FALSE)
	ENDIF
	
	DISABLE_ALL_MP_HUD_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
	
	IF NOT THEFEED_IS_PAUSED()
		// Pause the ticker feed.
		THEFEED_PAUSE()
	ENDIF
	
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	SET_DISABLE_RANK_UP_MESSAGE(TRUE)
ENDPROC

FUNC BOOL WARP_CINEMATIC__MAINTAIN_CINEMATIC(WARP_CINEMATIC_STRUCT &sInst)	
	
	WARP_CINEMATIC__SUPRESS_SYSTEMS()
	
	SWITCH WARP_CINEMATIC__GET_STATE(sInst)
		CASE WCS_INIT
			_WARP_CINEMATIC__UPDATE_INIT_STATE(sInst)
			BREAK
		CASE WCS_REGISTER_PLAYER_AND_LOAD_CUTSCENE
			_WARP_CINEMATIC__UPDATE_REGISTER_PLAYER_AND_LOAD_CUTSCENE_STATE(sInst)		
			BREAK
		CASE WCS_FADE_IN
			_WARP_CINEMATIC__UPDATE_FADE_IN_STATE(sInst)
			BREAK
		CASE WCS_WAIT_FOR_CINEMATIC_END
			_WARP_CINEMATIC__UPDATE_PLANE_LEAVE_STATE(sInst)
			BREAK
		CASE WCS_FADE_OUT		
			RETURN _WARP_CINEMATIC__UPDATE_FADE_OUT_STATE(sInst)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

#ENDIF // FEATURE_HEIST_ISLAND
