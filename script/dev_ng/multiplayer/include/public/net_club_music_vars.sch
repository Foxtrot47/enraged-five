//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_CLUB_MUSIC_VARS.sch																					//
// Description: Header file containing constants, structs and enums for club music.										//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		30/03/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "mp_globals_new_features_TU.sch"

#IF FEATURE_HEIST_ISLAND
//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CONSTANTS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

CONST_INT MAX_CLUB_END_OF_MIX_FADE_TIME_MS							1000
CONST_INT MAX_NUM_PUBLIC_CLUB										3		// public club data is stored in freemode serverBD 

// DJ_SERVER_DATA.iBS
CONST_INT CLUB_MUSIC_SERVER_BS_FREEMODE_SELECTED_DJ					0
CONST_INT CLUB_MUSIC_SERVER_BS_START_FROM_BEGINING_OF_SET			1
CONST_INT CLUB_MUSIC_SERVER_BS_START_DJ_SWITCH_SCENE				2
CONST_INT CLUB_MUSIC_SERVER_BS_USING_PATH_B							3
CONST_INT CLUB_MUSIC_SERVER_BS_MUSIC_STUDIO_TRACK_SELECTED			4

#IF IS_DEBUG_BUILD
CONST_INT DEBUG_MAX_CLUB_RADIO_STATION_TRACKS						3
CONST_INT DEBUG_MAX_CLUB_RADIO_STATION_TRACK_DURATION_MINS			40
CONST_INT DEBUG_MAX_SERVER_BD_UPDATE_TIME_MS						1000
#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ ENUMS ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

ENUM CLUB_LOCATIONS
	CLUB_LOCATION_INVALID = -1,
	CLUB_LOCATION_ISLAND = 0,
	#IF FEATURE_CASINO_NIGHTCLUB
	CLUB_LOCATION_CASINO_NIGHTCLUB=1,
	#ENDIF
	#IF FEATURE_FIXER
	CLUB_LOCATION_MUSIC_STUDIO=2,
	#ENDIF
	CLUB_LOCATION_TOTAL
ENDENUM

ENUM CLUB_MUSIC_INTENSITY
	CLUB_MUSIC_INTENSITY_NULL = -1,
	CLUB_MUSIC_INTENSITY_LOW = 0,
	CLUB_MUSIC_INTENSITY_MEDIUM,
	CLUB_MUSIC_INTENSITY_HIGH,
	CLUB_MUSIC_INTENSITY_HIGH_HANDS,
	CLUB_MUSIC_INTENSITY_TRANCE,
	CLUB_MUSIC_INTENSITY_TOTAL
ENDENUM

ENUM CLUB_DJS
	CLUB_DJ_NULL = -1,
	CLUB_DJ_SOLOMUN = 0,
	CLUB_DJ_DIXON,
	CLUB_DJ_TALE_OF_US,
	CLUB_DJ_BLACK_MADONNA,
	CLUB_DJ_KEINEMUSIK_NIGHTCLUB,
	CLUB_DJ_KEINEMUSIK_BEACH_PARTY,
	CLUB_DJ_PALMS_TRAX,
	CLUB_DJ_MOODYMANN,
	CLUB_DJ_DR_DRE_ABSENT,
	CLUB_DJ_DR_DRE_MIX,
	CLUB_DJ_DR_DRE_MIX_VOCALS,
	CLUB_DJ_DR_DRE_MUSICIAN_DRUMS,
	CLUB_DJ_DR_DRE_MUSICIAN_VOCALS,
	CLUB_DJ_DR_DRE_MIX_2,
	// Keep the DJ order as it is otherwise it will mess with telemtry data
	CLUB_DJ_TOTAL
ENDENUM

ENUM CLUB_RADIO_STATIONS
	CLUB_RADIO_STATION_NULL = -1,
	CLUB_RADIO_STATION_SOLOMUN = 0,
	CLUB_RADIO_STATION_TALE_OF_US,
	CLUB_RADIO_STATION_DIXON,
	CLUB_RADIO_STATION_BLACK_MADONNA,
	CLUB_RADIO_STATION_SOLOMUN_PRIVATE,
	CLUB_RADIO_STATION_TALE_OF_US_PRIVATE,
	CLUB_RADIO_STATION_DIXON_PRIVATE,
	CLUB_RADIO_STATION_BLACK_MADONNA_PRIVATE,
	CLUB_RADIO_STATION_KEINEMUSIK_NIGHTCLUB,
	CLUB_RADIO_STATION_KEINEMUSIK_BEACH_PARTY,
	CLUB_RADIO_STATION_PALMS_TRAX,
	CLUB_RADIO_STATION_MOODYMANN,
	CLUB_RADIO_STATION_DR_DRE_ABSENT,
	CLUB_RADIO_STATION_DR_DRE_MIX,
	CLUB_RADIO_STATION_DR_DRE_MIX_VOCALS,
	CLUB_RADIO_STATION_DR_DRE_MUSICIAN_DRUMS,
	CLUB_RADIO_STATION_DR_DRE_MUSICIAN_VOCALS,
	CLUB_RADIO_STATION_DR_DRE_MIX_2,
	CLUB_RADIO_STATION_TOTAL
ENDENUM

ENUM CLUB_DJ_STATE
	CLUB_DJ_INIT_DATAFILE_STATE,
	CLUB_DJ_INIT_STATE,
	CLUB_DJ_UPDATE_STATE,
	CLUB_DJ_SET_END_STATE,
	CLUB_DJ_SET_PAUSE_MUSIC_STATE,
	CLUB_DJ_CLEANUP
ENDENUM

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ STRUCTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
STRUCT DJ_DEBUG_DATA
	INT iDJ										= -1
	INT iMusicTrack								= 1
	INT iCutStage 
	FLOAT fTrackStartingMin						= 0.0 
	BOOL bResetMusicTrack						= FALSE
	BOOL bDisplayDebug							= FALSE
	BOOL bDisplaySlimmedDebug					= FALSE
	BOOL bMusicPlaying 							= FALSE
	TEXT_WIDGET_ID twDJ
	TEXT_WIDGET_ID twNextDJ
	TEXT_WIDGET_ID twHostMessage
	SCRIPT_TIMER stHostUpdateMusicData
	SCRIPT_TIMER CutTimer
ENDSTRUCT
#ENDIF

STRUCT DJ_SERVER_DATA
	INT iMusicTrack								= 1
	INT iMusicTrackNameHash						= 0
	INT iStartingMixTimeMS						= 0
	INT iTrackDurationMixTimeMS					= 0
	INT iTrackIntensityID						= 0
	INT iNextTrackIntensityTimeMS				= 0
	INT iBS
	BOOL bInitialisedData						= FALSE
	CLUB_DJS eDJ								= CLUB_DJ_NULL
	CLUB_DJS eNextDJ							= CLUB_DJ_NULL
	CLUB_RADIO_STATIONS eRadioStation 			= CLUB_RADIO_STATION_NULL
	CLUB_MUSIC_INTENSITY eMusicIntensity		= CLUB_MUSIC_INTENSITY_NULL
	CLUB_MUSIC_INTENSITY eNextMusicIntensity	= CLUB_MUSIC_INTENSITY_NULL	// Only set when intensity is about to change. CLUB_MUSIC_INTENSITY_NULL otherwise
	SCRIPT_TIMER stMusicTimer												// Timer never started, only used to offset time and diff against
	
	#IF FEATURE_FIXER
	BOOL bUsingPathB							= FALSE
	#ENDIF
ENDSTRUCT

STRUCT DJ_LOCAL_DATA
	INT iBS
	BOOL bMusicPlaying 							= FALSE 					// Using bool - Don't want to call native IS_BIT_SET each frame
	STRING sRadioStationName
	CLUB_DJS eDJ								= CLUB_DJ_NULL
	CLUB_MUSIC_INTENSITY eMusicIntensity		= CLUB_MUSIC_INTENSITY_NULL
	CLUB_MUSIC_INTENSITY eNextMusicIntensity	= CLUB_MUSIC_INTENSITY_NULL
	CLUB_DJ_STATE 	eDJstate
	
	#IF FEATURE_FIXER
	BOOL bUsingPathB							= FALSE
	INT iMusicStudioWorkState 					= 0
	#ENDIF
ENDSTRUCT
#ENDIF //FEATURE_HEIST_ISLAND
