USING "net_include.sch"
USING "net_hud_activating.sch"
USING "net_objective_text.sch"
USING "net_rank_ui.sch"
USING "cutscene_public.sch"
USING "selector_public.sch"
USING "net_transition_sessions.sch"
USING "net_vehicle_setup.sch"

/// PURPOSE:
///    To check if it is safe to start a mp cutscene
FUNC BOOL IS_PLAYER_OK_TO_START_MP_CUTSCENE()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT IS_SCREEN_FADING_IN()
		AND NOT IS_SCREEN_FADING_OUT()
			//IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING
			IF NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

/// PURPOSE: Set the female ped feet to flats
PROC SET_FEMALE_PED_FEET_TO_FLATS(PED_INDEX pedID)
	
	scrShopPedComponent componentItem
	INT iTempLegDraw
	INT iTempLegTex
	
	// If we are female, reset to no heels
	IF DOES_ENTITY_EXIST(pedID)
	AND NOT IS_ENTITY_DEAD(pedID)
		IF GET_ENTITY_MODEL(pedID) = MP_F_FREEMODE_01
	
			iTempLegDraw = GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_LEG)
			iTempLegTex = GET_PED_TEXTURE_VARIATION(pedID, PED_COMP_LEG)
	
			GET_SHOP_PED_COMPONENT(HASH("DLC_MP_HEIST_F_LEGS_0_0"), componentItem)
		
			if iTempLegDraw != componentItem.m_drawableIndex
			AND iTempLegTex != componentItem.m_textureIndex
				//Not on exception list giving default flats
				#IF IS_DEBUG_BUILD
					PRINTLN("EXCEPTION CHECK PASSED SETTING STANDARD VARIATION")
				#ENDIF
				
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_FEET, 4, 0)
			ELSE
				//Set of boots
				#IF IS_DEBUG_BUILD
					PRINTLN("EXCEPTION LEGS! NEED TO SET PED INTO BOOTS")
				#ENDIF
				
				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_FEET, 2, 1)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    To clone a local version of a vehicle including any damage for a cutscene

FUNC BOOL CREATE_CUTSCENE_VEHICLE_CLONE(VEHICLE_INDEX &NewVehicleId,VEHICLE_INDEX VehToClone,VECTOR vPos, FLOAT fHeading) 
	
	model_names vehModel
	INT irepeat
	
	RESET_VEHICLE_SETUP_STRUCT_MP(fakeGarageVehicleSetupStruct)
	
	IF IS_VEHICLE_DRIVEABLE(VehToClone)
		vehModel = GET_ENTITY_MODEL(VehToClone)
		GET_VEHICLE_SETUP_MP(VehToClone, fakeGarageVehicleSetupStruct)
	ELSE
		#IF IS_DEBUG_BUILD	
			DEBUG_PRINTCALLSTACK()			
			NET_SCRIPT_ASSERT("CREATE_CUTSCENE_VEHICLE_CLONE: IS_VEHICLE_DRIVEABLE  = FALSE")		
			NET_PRINT("CREATE_CUTSCENE_VEHICLE_CLONE - called by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
		#ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD	
		IF NOT REQUEST_LOAD_MODEL(vehModel)
			DEBUG_PRINTCALLSTACK()			
			NET_SCRIPT_ASSERT("CREATE_CUTSCENE_VEHICLE_CLONE: LOAD_MODEL()  = FALSE")		
		ENDIF	
		NET_PRINT("CREATE_CUTSCENE_VEHICLE_CLONE - called by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
		NET_PRINT(", Model = ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG(vehModel)) NET_NL()
	#ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(NewVehicleId)
		NewVehicleId = CREATE_VEHICLE(vehModel, vPos, fHeading, FALSE, FALSE)
		
		SET_VEHICLE_SETUP_MP(NewVehicleId, fakeGarageVehicleSetupStruct)
		
		IF NOT IS_THIS_MODEL_A_BIKE(vehModel)
			FOR irepeat = ENUM_TO_INT(SC_DOOR_FRONT_LEFT) TO ENUM_TO_INT(SC_DOOR_BOOT)
				IF IS_VEHICLE_DOOR_DAMAGED(VehToClone,INT_TO_ENUM(SC_DOOR_LIST,irepeat))
					SET_VEHICLE_DOOR_BROKEN(NewVehicleId,INT_TO_ENUM(SC_DOOR_LIST,irepeat),TRUE)
				ENDIF
			ENDFOR
		ENDIF
		FOR irepeat = ENUM_TO_INT(SC_WHEEL_CAR_FRONT_LEFT) TO ENUM_TO_INT(SC_WHEEL_BIKE_REAR)
			IF IS_VEHICLE_TYRE_BURST(VehToClone,INT_TO_ENUM(SC_WHEEL_LIST,irepeat),TRUE)
				SET_VEHICLE_TYRE_BURST(NewVehicleId,INT_TO_ENUM(SC_WHEEL_LIST,irepeat),TRUE)
			ELIF IS_VEHICLE_TYRE_BURST(VehToClone,INT_TO_ENUM(SC_WHEEL_LIST,irepeat))
				SET_VEHICLE_TYRE_BURST(NewVehicleId,INT_TO_ENUM(SC_WHEEL_LIST,irepeat))
			ENDIF
		ENDFOR
		IF NOT IS_THIS_MODEL_A_BIKE(vehModel)
			FOR irepeat = ENUM_TO_INT(SC_WINDOW_FRONT_LEFT) TO ENUM_TO_INT(SC_WINDOW_REAR_RIGHT)
				IF NOT IS_VEHICLE_WINDOW_INTACT(VehToClone,INT_TO_ENUM(SC_WINDOW_LIST,irepeat))
					REMOVE_VEHICLE_WINDOW(NewVehicleId,INT_TO_ENUM(SC_WINDOW_LIST,irepeat))
				ENDIF
			ENDFOR
		ENDIF
		
		COPY_VEHICLE_DAMAGES(VehToClone, NewVehicleId)
		
		IF vehModel = DOMINATOR4
		OR vehModel = DOMINATOR5
		OR vehModel = DOMINATOR6
			IF NOT GET_DOES_VEHICLE_HAVE_TOMBSTONE(VehToClone)
				HIDE_TOMBSTONE(NewVehicleId, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(NewVehicleId)
		
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Disables the players control and makes him invincible before starting a cutscene.
PROC MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(BOOL bLeavePedCopyBehind=TRUE, BOOL bPlayerInvisible = TRUE, BOOL bPlayerHasCollision = TRUE, BOOL bPlayerFrozen = FALSE, BOOL bClearTasks = TRUE, BOOL bStopDucking = TRUE, BOOL bKeepPortableObjects = FALSE)
	
	#IF IS_DEBUG_BUILD
	NET_NL()
	NET_PRINT("*****")NET_NL()
	NET_PRINT("[cutscene] MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE called by ")NET_PRINT(GET_THIS_SCRIPT_NAME())NET_NL()
	NET_PRINT("*****")NET_NL()
	NET_NL()
	#ENDIF
	
	Disable_MP_Comms()
	DISABLE_CELLPHONE(TRUE)
//	DISABLE_MISSION_OVERLAY()	

	SET_DPADDOWN_ACTIVE(TRUE)
	
	DISABLE_SCRIPT_HUD(HUDPART_AWARDS, TRUE)
	DISABLE_SELECTOR()
	SET_USER_RADIO_CONTROL_ENABLED(FALSE)
	
	IF (bLeavePedCopyBehind)
		NETWORK_LEAVE_PED_BEHIND_BEFORE_CUTSCENE(PLAYER_ID(), bKeepPortableObjects)
	ENDIF
	
	NET_SET_PLAYER_CONTROL_FLAGS eFlags
	IF (bPlayerInvisible)
		eFlags = eFlags | NSPC_SET_INVISIBLE 
	ENDIF
	IF (bClearTasks)
		eFlags = eFlags | NSPC_CLEAR_TASKS 
	ENDIF
	IF NOT (bPlayerHasCollision)
		eFlags = eFlags | NSPC_NO_COLLISION 
	ENDIF
	IF (bPlayerFrozen)
		eFlags = eFlags | NSPC_FREEZE_POSITION 
	ENDIF
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, eFlags)
	
	IF bStopDucking
		IF IS_PED_DUCKING(PLAYER_PED_ID())
			SET_PED_DUCKING(PLAYER_PED_ID(), FALSE)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Clears the screen and sets all the required options for playing a cutscene
PROC START_MP_CUTSCENE(BOOL bSetInMpCutscene = TRUE,BOOL bMakePlayerInvincible = TRUE, BOOL bAllowCallsOverScene = FALSE)

	DEBUG_PRINTCALLSTACK()

	#IF IS_DEBUG_BUILD
	NET_NL()
	NET_PRINT("*****")NET_NL()
	NET_PRINT("[cutscene] START_MP_CUTSCENE called by ")NET_PRINT(GET_THIS_SCRIPT_NAME())NET_NL()
	NET_PRINT("*****")NET_NL()
	NET_NL()
	#ENDIF
	
	Pause_Objective_Text()
	CLEAR_ALL_FLOATING_HELP()
	CLEAR_HELP()
	CLEAR_PRINTS()
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bAllowCallsOverScene)		
	DISABLE_ALL_MP_HUD()
	DISABLE_SCRIPT_HUD(HUDPART_AWARDS, TRUE)
	SET_DPADDOWN_ACTIVE(FALSE)
	SET_WIDESCREEN_BORDERS(TRUE, -1)
	DISPLAY_RADAR(FALSE)
	DISPLAY_HUD(FALSE)
	CLEAR_ALL_FLOATING_HELP()
	HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(TRUE)
	MPGlobals.bStartedMPCutscene = TRUE //used by am safehouse to determine if help needs to be displayed again after player exits cutscene
	IF bSetInMpCutscene	//This is sometimes set to false for tutorials so that the ambient population will exist
		IF NOT NETWORK_IS_IN_MP_CUTSCENE() // Wrapped in check to prevent John gurney's assert that fires if NETWORK_SET_IN_MP_CUTSCENE with the same value for it's agrument more than once.
			PRINTLN("[TS] [MSRAND][net_cutscne] - NETWORK_SET_IN_MP_CUTSCENE(TRUE, ", bMakePlayerInvincible, ")")
			NETWORK_SET_IN_MP_CUTSCENE(TRUE,bMakePlayerInvincible)
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEANUP_MP_CUTSCENE(BOOL bClearHelp = TRUE,BOOL bReturnPlayerControl = TRUE, BOOL bPreventInvincibilityChanges = FALSE, BOOL bCleanupNetworkCutscene = TRUE)
	
	#IF IS_DEBUG_BUILD
	NET_NL()
	NET_PRINT("*****")NET_NL()
	NET_PRINT("[cutscene] CLEANUP_MP_CUTSCENE called by ")NET_PRINT(GET_THIS_SCRIPT_NAME())NET_NL()
	NET_PRINT("*****")NET_NL()
	NET_NL()
	#ENDIF
	
	Unpause_Objective_Text()
	
	IF bClearHelp
		CLEAR_ALL_FLOATING_HELP()
		CLEAR_HELP()
	ENDIF
	CLEAR_PRINTS()
	Enable_MP_Comms()
	
	SET_PARTICLE_FX_CAM_INSIDE_VEHICLE(FALSE)

	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bPreventInvincibilityChanges)
	ENABLE_ALL_MP_HUD()
	DISABLE_SCRIPT_HUD(HUDPART_AWARDS, FALSE)
	SET_DPADDOWN_ACTIVE(TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, -1)
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	ENABLE_SELECTOR()
	SET_USER_RADIO_CONTROL_ENABLED(TRUE)
	IF NETWORK_IS_GAME_IN_PROGRESS() // John G handles cleaning up if Network game isn't running. See 892498
		IF bCleanupNetworkCutscene
			IF NETWORK_IS_IN_MP_CUTSCENE() // Wrapped in check to prevent John gurney's assert that fires if NETWORK_SET_IN_MP_CUTSCENE with the same value for it's agrument more than once.
				PRINTLN("[TS] [MSRAND] - NETWORK_SET_IN_MP_CUTSCENE(FALSE,FALSE)")
				NETWORK_SET_IN_MP_CUTSCENE(FALSE,FALSE)
			ENDIF
		ELSE
			PRINTLN("CLEANUP_MP_CUTSCENE - bCleanupNetworkCutscene = FALSE")
		ENDIF
	ENDIF
	
	HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(FALSE)
	
	IF IS_TRANSITION_ACTIVE() = FALSE
	AND DOES_TRANSITION_SESSIONS_NEED_TO_WARP_TO_START_SKYCAM() = FALSE
	AND bReturnPlayerControl
	AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
	AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
	AND IS_SKYSWOOP_AT_GROUND()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	MPGlobals.bStartedMPCutscene = FALSE //used by am safehouse to determine if help needs to be displayed again after player exits cutscene
	
	MAINTAIN_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
ENDPROC



// EOF



