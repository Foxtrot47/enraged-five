// Name: net_corona_position.sch
// Author: James Adwick
// Date: 26/10/2012
// Purpose: Commands called within FM to position player on edge of corona and facing camera.

USING "globals.sch"
USING "net_include.sch"
USING "net_include.sch"
USING "net_betting_client.sch"
USING "Net_Entity_Icons.sch"
//TWEAK_FLOAT tfMOVE_LEFT_RIGHT_CORONA 0.3

PROC CLEANUP_SERVER_PLAYER_CORONA_DATA(INT iPlayerID)
	
	//...fetch the player current position details
	INT iVoteID = GlobalServerBD_BlockB.g_PlayerCoronaPos[iPlayerID].iVoteIndex
	INT iCoronaPos = GlobalServerBD_BlockB.g_PlayerCoronaPos[iPlayerID].iCoronaPos

	// Do we have valid values for vote ID and position on edge
	IF iVoteID != -1
	AND iCoronaPos != -1
	
		//...clear the bit on server BD
		CLEAR_BIT(GlobalServerBD_BlockB.g_BSVoteCoronaPos[iVoteID], iCoronaPos)
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[JA@CORONA] CLEANUP_SERVER_PLAYER_CORONA_DATA -")
			PRINTLN("			iPlayerID: ", iPlayerID) 
			
			IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayerID))
				PRINTLN(" Name: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayerID)))
			ENDIF
			
			PRINTLN("			iVoteID: ", iVoteID)
			PRINTLN("			iCoronaPos: ", iCoronaPos)
			PRINTLN("			GlobalServerBD_BlockB.g_BSVoteCoronaPos[iVoteID]: ", GlobalServerBD_BlockB.g_BSVoteCoronaPos[iVoteID])
		#ENDIF
	ENDIF

	// reset values for this player
	GlobalServerBD_BlockB.g_PlayerCoronaPos[iPlayerID].iVoteIndex = -1
	GlobalServerBD_BlockB.g_PlayerCoronaPos[iPlayerID].iCoronaPos = -1
ENDPROC

// *********************************************
//		BROADCASTING & PROCESSING OF EVENTS
// *********************************************

/// PURPOSE: Inform server I have left a vote and to clear up corona positioning
PROC BROADCAST_PLAYER_CLEAR_CORONA_SLOT()
	
	// Set up event to notify server of player leaving corona (slot now available)
	EVENT_STRUCT_PLAYER_CLEAR_CORONA_SLOT Event
	Event.Details.Type = SCRIPT_EVENT_PLAYER_CLEAR_CORONA_SLOT
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	INT iSendTo = ALL_PLAYERS()
	IF NOT (iSendTo = 0)		
		
		PRINTLN("[JA@CORONA] BROADCAST_PLAYER_CLEAR_CORONA_SLOT - SENT TO SERVER")
		
		// ...broadcast to all players
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		PRINTLN("[JA@CORONA] BROADCAST_PLAYER_CLEAR_CORONA_SLOT - playerflags = 0 so not broadcasting")
	ENDIF
	
ENDPROC

FUNC INT GET_PLAYER_CORONA_VARIATION(PLAYER_INDEX playerID)
	IF (Is_Player_Allocated_For_Any_Mission(playerID))
		INT playerIndexAsInt = NATIVE_TO_INT(playerID)
	
		IF (GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpState != MP_PLAYER_STATE_NONE)
			
			INT missionSlot = GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpSlot
			IF (missionSlot != MPMCP_NO_MISSION_SLOT)
				
				RETURN Get_MP_Mission_Request_MissionVariation(missionSlot)
			
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[JA@CORONA] GET_PLAYER_CORONA_VARIATION - missionSlot != MPMCP_NO_MISSION_SLOT, ", GET_PLAYER_NAME(playerID), " missionSlot = ", missionSlot)
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[JA@CORONA] GET_PLAYER_CORONA_VARIATION - GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpState != MP_PLAYER_STATE_NONE, ", GET_PLAYER_NAME(playerID), " State: ", GlobalServerBD_BlockB.missionPlayerData[playerIndexAsInt].mcpState)
		#ENDIF
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[JA@CORONA] GET_PLAYER_CORONA_VARIATION - Is_Player_Allocated_For_Any_Mission(thePlayerIndex) = FALSE, ", GET_PLAYER_NAME(playerID))
	#ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC



FUNC BOOL IS_SLOT_VALID_FOR_MISSION(INT iMissionType, INT iSlot, PLAYER_INDEX playerID)

	// Special slots for shooting range
	IF iMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
	
		INT iMissionVar = GET_PLAYER_CORONA_VARIATION(playerID)
	
		IF iMissionVar = -1
			RETURN FALSE
		ENDIF
	
		IF iMissionVar = 0
			IF iSlot = 6
			OR iSlot = 7
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF iMissionVar = 1
			IF iSlot = 1
			OR iSlot = 2
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Special slots for darts
	IF iMissionType = FMMC_TYPE_MG_DARTS
		IF iSlot = 4
		OR iSlot = 6
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC


/// PURPOSE: Find an available slot on edge of corona for player
PROC PROCESS_PLAYER_REQUESTING_CORONA_POS(PLAYER_INDEX playerID)

	PRINTLN("[JA@CORONA] PROCESS_PLAYER_REQUESTING_CORONA_POS - called...")
	
	INT i
	INT iPlayerInt = NATIVE_TO_INT(playerID)
	INT iPlayerVote = GlobalplayerBD_FM_2[iPlayerInt].iVoteToUse
	INT iMissionType = GlobalplayerBD_FM_2[iPlayerInt].missionType
	
	IF iPlayerVote != -1
		//...find an available position on corona edge
		REPEAT NUM_NETWORK_PLAYERS i
		
			IF NOT IS_BIT_SET(GlobalServerBD_BlockB.g_BSVoteCoronaPos[iPlayerVote], i)
				IF IS_SLOT_VALID_FOR_MISSION(iMissionType, i, playerID)
				
					PRINTLN("[JA@CORONA] PROCESS_PLAYER_REQUESTING_CORONA_POS - player ", GET_PLAYER_NAME(playerID), " assigned slot: ", i)
					PRINTLN("			iVoteID: ", iPlayerVote)
					PRINTLN("			iCoronaPos: ", i)
					PRINTLN("			GlobalServerBD_BlockB.g_BSVoteCoronaPos[iVoteID]: ", GlobalServerBD_BlockB.g_BSVoteCoronaPos[iPlayerVote])
					
					SET_BIT(GlobalServerBD_BlockB.g_BSVoteCoronaPos[iPlayerVote], i)				// Store our vote bitset
					GlobalServerBD_BlockB.g_PlayerCoronaPos[iPlayerInt].iCoronaPos = i				// Store our slot number for player to pick up
					GlobalServerBD_BlockB.g_PlayerCoronaPos[iPlayerInt].iVoteIndex = iPlayerVote	// Store our vote index for server to use to clean up
					
					i = NUM_NETWORK_PLAYERS	// EXIT
				ENDIF
			ENDIF
		
		ENDREPEAT
	ENDIF
	
ENDPROC


/// PURPOSE: Clear up a player from the corona positioning bitset
PROC PROCESS_PLAYER_CLEAR_CORONA_POS(INT iEventID)
	
	PRINTLN("[JA@CORONA] PROCESS_PLAYER_CLEAR_CORONA_POS - called...")
	
	EVENT_STRUCT_PLAYER_CLEAR_CORONA_SLOT Event
	
	// Retrieve details of event received from player to clear corona slot
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		IF IS_NET_PLAYER_OK(Event.Details.FromPlayerIndex, FALSE)
			
			CLEANUP_SERVER_PLAYER_CORONA_DATA(NATIVE_TO_INT(Event.Details.FromPlayerIndex))
		ENDIF
	ELSE
		PRINTLN("PROCESS_PLAYER_CLEAR_CORONA_POS - could not retrieve data")
	ENDIF	
	
ENDPROC

// **************************
//		UTILITY FUNCTIONS
// **************************

/// PURPOSE: Update the state of the player
PROC GOTO_CORONA_POS_STATE(INT iNewState)
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("[JA@CORONA] Moving to state: ")
	
		SWITCH iNewState
			CASE CORONA_POS_STATE_NULL			PRINTSTRING("CORONA_POS_STATE_NULL")			BREAK
			CASE CORONA_POS_STATE_HAS_SLOT		PRINTSTRING("CORONA_POS_STATE_HAS_SLOT")		BREAK
			CASE CORONA_POS_STATE_SET_COLLISION	PRINTSTRING("CORONA_POS_STATE_SET_COLLISION")	BREAK
			CASE CORONA_POS_STATE_IN_POSITION	PRINTSTRING("CORONA_POS_STATE_IN_POSITION")		BREAK
			CASE CORONA_POS_STATE_REQUESTING	PRINTSTRING("CORONA_POS_STATE_REQUESTING")		BREAK
			CASE CORONA_POS_STATE_LEAVING		PRINTSTRING("CORONA_POS_STATE_LEAVING")			BREAK
		ENDSWITCH
		
		PRINTNL()
	#ENDIF
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCoronaPosState = iNewState
ENDPROC

/// PURPOSE: Cleans up local data and broadcasts event to server to clean up slot info
PROC CLEANUP_LOCAL_CORONA_DETAILS(BOOL bReturnControl = FALSE, BOOL bCalledFromSCRIPT_CLEANUP = FALSE)
	PRINTLN("CLEANUP_LOCAL_CORONA_DETAILS")
	IF NETWORK_IS_GAME_IN_PROGRESS()
		BROADCAST_PLAYER_CLEAR_CORONA_SLOT()
	ENDIF
	MPGlobals.coronaPosData.iCoronaPosition = -1
	MPGlobals.coronaPosData.iSlotSelected = 0
	
	IF NOT bCalledFromSCRIPT_CLEANUP
		// Turn on control if we haven't already.
		IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
			
			// Reset player IK mode and control
			SET_PED_LEG_IK_MODE(PLAYER_PED_ID(), LEG_IK_FULL)
			
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				IF NOT IS_PLAYER_IN_CORONA()
				OR bReturnControl
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	GOTO_CORONA_POS_STATE(CORONA_POS_STATE_NULL)
ENDPROC

/// PURPOSE: Checks voting bitset to know when we require position
FUNC BOOL DOES_PLAYER_REQUIRE_CORONA_SLOT()
	IF GET_CORONA_STATUS() != CORONA_STATUS_WALK_OUT
	AND GET_CORONA_STATUS() != CORONA_STATUS_QUIT
		INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
		RETURN IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIwantToVote)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Return the next available player to look at on edge of corona
FUNC INT GET_NEXT_PLAYER_IN_CORONA(INT iIncrement, PLAYER_INDEX &playerID)

	INT iCurrentSelection = MPGlobals.coronaPosData.iSlotSelected
	INT iMyVoteID = GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iVoteToUse
	BOOL bFoundNextPlayer = FALSE
	BOOL bWrap = (SUM_BITS(GlobalServerBD_BlockB.g_BSVoteCoronaPos[iMyVoteID]) = 16)
	PLAYER_INDEX tempPlayerID
	
	WHILE NOT bFoundNextPlayer
	
		iCurrentSelection += iIncrement
		
		IF iCurrentSelection >= NUM_NETWORK_PLAYERS
		
			IF bWrap		
				iCurrentSelection = 0
			ELSE
				RETURN MPGlobals.coronaPosData.iSlotSelected
			ENDIF
		ENDIF
		
		IF iCurrentSelection < 0
			IF bWrap
				iCurrentSelection = NUM_NETWORK_PLAYERS-1
			ELSE
				RETURN MPGlobals.coronaPosData.iSlotSelected
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalServerBD_BlockB.g_BSVoteCoronaPos[iMyVoteID], iCurrentSelection)
			tempPlayerID = GET_PLAYER_IN_CORONA_SLOT(iCurrentSelection)
			
			IF tempPlayerID != INVALID_PLAYER_INDEX()
			AND GET_PLAYER_CORONA_POS_STATE(tempPlayerID) = CORONA_POS_STATE_IN_POSITION
				playerID = tempPlayerID
				bFoundNextPlayer = TRUE
			ENDIF
		ENDIF
		
	ENDWHILE

	PRINTLN("[JA@CORONA] GET_NEXT_PLAYER_IN_CORONA - Next player is at slot: ", iCurrentSelection)
	
	RETURN iCurrentSelection
ENDFUNC


// *****************************************************
// 			MAINTAIN FUNCTIONS - SERVER & CLIENT
// *****************************************************

/// PURPOSE: Monitor any players leaving the game and cleaning up any corona positioning we have
PROC MAINTAIN_SERVER_CORONA_POS(INT iPosToCheck, PLAYER_INDEX playerID, BOOL bActive)

	IF playerID = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF
	
	BOOL bPlayerHasSlotDetails = FALSE
	INT index = iPosToCheck
		
	// Does player have a corona position
	IF GlobalServerBD_BlockB.g_PlayerCoronaPos[index].iVoteIndex != -1
	OR GlobalServerBD_BlockB.g_PlayerCoronaPos[index].iCoronaPos != -1
		bPlayerHasSlotDetails = TRUE
	ENDIF
	
	IF bPlayerHasSlotDetails
		// Check to see if player is no longer active
		IF NOT bActive
		OR NOT IS_NET_PLAYER_OK(playerID, TRUE)
			PRINTLN("[JA@CORONA] (S) Clearing player who is no longer in game: ", index)
		
			CLEANUP_SERVER_PLAYER_CORONA_DATA(index)
			EXIT
		ENDIF
	ENDIF
	
	// Check client state and act accordingly
	SWITCH GET_PLAYER_CORONA_POS_STATE(playerID)
	
		//...player not in corona flow
		CASE CORONA_POS_STATE_NULL
			
			//...does he have slots though
			IF bPlayerHasSlotDetails
			
				//...yes, clear bit
				PRINTLN("[JA@CORONA] (S) Clearing player ", GET_PLAYER_NAME(playerID), " who is no longer in corona")
		
				CLEANUP_SERVER_PLAYER_CORONA_DATA(index)
			ENDIF
		BREAK
		
		//...player needs a slot
		CASE CORONA_POS_STATE_REQUESTING
		
			//...have we given them a slot?
			IF NOT bPlayerHasSlotDetails
							
				IF IS_NET_PLAYER_OK(playerID)
					//...no, find the next available slot and broadcast
					PRINTLN("[JA@CORONA] (S) Player needs corona slot: ", GET_PLAYER_NAME(playerID))
					
					PROCESS_PLAYER_REQUESTING_CORONA_POS(playerID)			
				ELSE
					PRINTLN("[JA@CORONA] (S) CORONA_POS_STATE_REQUESTING - IS_NET_PLAYER_OK = FLASE")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

	
ENDPROC

// Monitor our requesting / clearing of corona position based on voting state (Perhaps make it a switch state)
PROC MAINTAIN_REQUEST_CORONA_POS()

	SWITCH GET_LOCAL_PLAYER_CORONA_POS_STATE()
	
		CASE CORONA_POS_STATE_NULL
		
			// Check if we are involved in a voting corona
			IF DOES_PLAYER_REQUIRE_CORONA_SLOT()
				INT iMyGBD
				iMyGBD = NATIVE_TO_INT(PLAYER_ID())
				
				//...yes, make request for a slot position
				IF GlobalplayerBD_FM_2[iMyGBD].iVoteToUse != -1
				
					PRINTLN("[JA@CORONA] MAINTAIN_REQUEST_CORONA_POS - requesting a slot. VoteID: ", GlobalplayerBD_FM_2[iMyGBD].iVoteToUse)
				
					GOTO_CORONA_POS_STATE(CORONA_POS_STATE_REQUESTING)
				ENDIF
			ENDIF
		BREAK
	
		// Wait until the server has assigned us a position on the edge of the corona
		CASE CORONA_POS_STATE_REQUESTING
		
			IF GlobalServerBD_BlockB.g_PlayerCoronaPos[NATIVE_TO_INT(PLAYER_ID())].iCoronaPos != -1
				//...update our local knowledge of our position and update state
				MPGlobals.coronaPosData.iCoronaPosition = GlobalServerBD_BlockB.g_PlayerCoronaPos[NATIVE_TO_INT(PLAYER_ID())].iCoronaPos
				GOTO_CORONA_POS_STATE(CORONA_POS_STATE_HAS_SLOT)
				
				PRINTLN("[JA@CORONA] MAINTAIN_REQUEST_CORONA_POS - I now have a slot: ", MPGlobals.coronaPosData.iCoronaPosition)
			ENDIF
			
			// clean up if we no longer need slot
			IF NOT DOES_PLAYER_REQUIRE_CORONA_SLOT()
				CLEANUP_LOCAL_CORONA_DETAILS()
			ENDIF
			
		BREAK
	
		// Reset if we leave the voting corona.
		CASE CORONA_POS_STATE_HAS_SLOT
		CASE CORONA_POS_STATE_SET_COLLISION
		CASE CORONA_POS_STATE_IN_POSITION
			
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableActionMode, TRUE)
		
			IF NOT DOES_PLAYER_REQUIRE_CORONA_SLOT()
				CLEANUP_LOCAL_CORONA_DETAILS()
			ENDIF
			
		BREAK
	
		CASE CORONA_POS_STATE_LEAVING
		
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE: Display new version of player stat card (filter stats based on mission type (only deathmatch and races at the moment))
PROC DISPLAY_PLAYER_STATS_CARD(SCALEFORM_PLAYER_CARD &ScaleformPlayerCardStruct, SCALEFORM_INDEX &ButtonMovie, PLAYER_INDEX piPassed, INT iMissionType = FMMC_TYPE_MISSION, HUD_COLOURS hcPassed =  HUD_COLOUR_FREEMODE)
	IF NETWORK_IS_SIGNED_ONLINE()
		
		IF IS_NET_PLAYER_OK(piPassed, FALSE, TRUE)
			SPRITE_PLACEMENT ScaleformSprite = GET_SCALEFORM_PLAYER_CARD_POSITION()
			//Adjust it
			ScaleformSprite.x = 0.162
			ScaleformSprite.y = 0.350
		
			IF SHOULD_REFRESH_SCALEFORM_PLAYER_CARD(ScaleformPlayerCardStruct)
				
				//Get the playersgamer handel
				GAMER_HANDLE gamerHandle 
				gamerHandle= GET_GAMER_HANDLE_PLAYER(piPassed)     
				//Get the gamer name
				STRING GamerTag = GET_PLAYER_NAME(piPassed)
				//Player slot and rank
				INT iPlayerSlot = NATIVE_TO_INT(piPassed)
				INT iRank 		= GET_PLAYER_RANK(piPassed)
				//Rank name
				TEXT_LABEL_31 RankTitle  = GET_RANK_TEXTLABEL(iRank, GET_PLAYER_GLOBAL_TEAM(piPassed))
				TEXT_LABEL_15 tlCrewTag = ""
				//Clan details
				NETWORK_CLAN_DESC PlayerClan
				IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
					NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
				ENDIF
				NETWORK_CLAN_GET_UI_FORMATTED_TAG_EZ(PlayerClan, tlCrewTag)
								
				//Set up the card details
				PRIVATE_SET_SCALEFORM_PLAYER_CARD_HUD_COLOUR				(hcPassed,  										ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_NAME						(GamerTag,  										ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_RANK_AND_TITLE			(iRank, 	RankTitle,								ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_XP						(GlobalplayerBD_FM[iPlayerSlot].scoreData.iXp, 			ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_KILLSDEATH				(GlobalplayerBD_FM[iPlayerSlot].scoreData.fKDRatio, 			ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_IS_FREEMODE				(TRUE, 												ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_MISSION_TYPE			(iMissionType, 										ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_KD_DETAILS			(GlobalplayerBD_FM[iPlayerSlot].scoreData.iKills, GlobalplayerBD_FM[iPlayerSlot].scoreData.iDeaths,	ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_DEATHMATCH_DETAILS	(GlobalplayerBD_FM[iPlayerSlot].scoreData.iDMWon, GlobalplayerBD_FM[iPlayerSlot].scoreData.iDMLost, 	ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_RACE_DETAILS			(GlobalplayerBD_FM[iPlayerSlot].scoreData.iRacesWon, GlobalplayerBD_FM[iPlayerSlot].scoreData.iRacesLost, 			ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_ACCURACY_DETAILS		(GlobalplayerBD_FM[iPlayerSlot].scoreData.iFired,GlobalplayerBD_FM[iPlayerSlot].scoreData.iHit,ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_MISSION_CREATOR_DETAILS(GlobalplayerBD_FM[iPlayerSlot].scoreData.iMissionsCreated, 				ScaleformPlayerCardStruct)
				PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_CREW_DETAILS(PlayerClan.ClanName, tlCrewTag, ScaleformPlayerCardStruct)
			ENDIF	

			RUN_MP_PLAYER_CARD(ButtonMovie, ScaleformSprite, ScaleformPlayerCardStruct, SHOULD_REFRESH_SCALEFORM_PLAYER_CARD(ScaleformPlayerCardStruct))
		ENDIF
	ENDIF
ENDPROC

///Display a players gamer card in the top corner like (TO BE REPLACED BY ABOVE COMMAND)
PROC FMMC_DISPLAY_PLAYER_STATS_CARD(SCALEFORM_PLAYER_CARD &ScaleformPlayerCardStruct, SCALEFORM_INDEX &ButtonMovie, PLAYER_INDEX piPassed, HUD_COLOURS hcPassed =  HUD_COLOUR_FREEMODE)
	
	IF NETWORK_IS_SIGNED_ONLINE()
		//Get the placement
		SPRITE_PLACEMENT ScaleformSprite = GET_SCALEFORM_PLAYER_CARD_POSITION()
		//Adjust it
		ScaleformSprite.x = 0.162
		ScaleformSprite.y = 0.350

		//Get the playersgamer handel
		GAMER_HANDLE gamerHandle 
		gamerHandle= GET_GAMER_HANDLE_PLAYER(piPassed)     
		//Get the gamer name
		STRING GamerTag = GET_PLAYER_NAME(piPassed)
		//Player slot and rank
		INT iPlayerSlot = NATIVE_TO_INT(piPassed)
		INT iRank 		= GET_PLAYER_RANK(piPassed)
		//Rank name
		TEXT_LABEL_31 RankTitle  = GET_RANK_TEXTLABEL(iRank, GET_PLAYER_GLOBAL_TEAM(piPassed))
		//Clan details
		NETWORK_CLAN_DESC PlayerClan
		PlayerClan.ClanTag = ""
		IF NETWORK_CLAN_SERVICE_IS_VALID() AND NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
			NETWORK_CLAN_PLAYER_GET_DESC(PlayerClan, SIZE_OF(PlayerClan), gamerHandle)
		ENDIF
		
		//Set up the card details
		PRIVATE_SET_SCALEFORM_PLAYER_CARD_HUD_COLOUR				(hcPassed,  										ScaleformPlayerCardStruct)
		PRIVATE_ADD_SCALEFORM_PLAYER_CARD_NAME						(GamerTag,  										ScaleformPlayerCardStruct)
		PRIVATE_ADD_SCALEFORM_PLAYER_CARD_RANK_AND_TITLE			(iRank, 	RankTitle,								ScaleformPlayerCardStruct)
		PRIVATE_ADD_SCALEFORM_PLAYER_CARD_XP						(GlobalplayerBD_FM[iPlayerSlot].scoreData.iXp, 			ScaleformPlayerCardStruct)
		PRIVATE_ADD_SCALEFORM_PLAYER_CARD_KILLSDEATH				(GlobalplayerBD_FM[iPlayerSlot].scoreData.fKDRatio, 			ScaleformPlayerCardStruct)
		PRIVATE_ADD_SCALEFORM_PLAYER_CARD_IS_FREEMODE				(TRUE, 												ScaleformPlayerCardStruct)
		PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_KD_DETAILS			(GlobalplayerBD_FM[iPlayerSlot].scoreData.iKills, GlobalplayerBD_FM[iPlayerSlot].scoreData.iDeaths,	ScaleformPlayerCardStruct)
		PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_DEATHMATCH_DETAILS	(GlobalplayerBD_FM[iPlayerSlot].scoreData.iDMWon, GlobalplayerBD_FM[iPlayerSlot].scoreData.iDMLost, 	ScaleformPlayerCardStruct)
		PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_RACE_DETAILS			(GlobalplayerBD_FM[iPlayerSlot].scoreData.iRacesWon, GlobalplayerBD_FM[iPlayerSlot].scoreData.iRacesLost, 			ScaleformPlayerCardStruct)
		PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_ACCURACY_DETAILS		(GlobalplayerBD_FM[iPlayerSlot].scoreData.iFired ,GlobalplayerBD_FM[iPlayerSlot].scoreData.iHit,ScaleformPlayerCardStruct)
		PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_MISSION_CREATOR_DETAILS(GlobalplayerBD_FM[iPlayerSlot].scoreData.iMissionsCreated, 				ScaleformPlayerCardStruct)
		PRIVATE_ADD_SCALEFORM_PLAYER_CARD_ADD_CREW_DETAILS			(PlayerClan.ClanName, "", ScaleformPlayerCardStruct)
		
		//Draw it
		RUN_SCALEFORM_PLAYER_CARDS(ButtonMovie, ScaleformSprite, ScaleformPlayerCardStruct, SHOULD_REFRESH_SCALEFORM_PLAYER_CARD(ScaleformPlayerCardStruct))
	ENDIF
ENDPROC

/// PURPOSE: Returns FALSE if the slot we are looking at is no longer active
FUNC BOOL IS_CORONA_SLOT_STILL_ACTIVE()

	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	INT iMyVoteID = GlobalplayerBD_FM_2[iMyGBD].iVoteToUse
	
	IF NOT IS_BIT_SET(GlobalServerBD_BlockB.g_BSVoteCoronaPos[iMyVoteID], MPGlobals.coronaPosData.iSlotSelected)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns the radius for the mission type corona
FUNC FLOAT GET_RADIUS_OF_CORONA(INT iMissionType)
	IF iMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
	OR iMissionType = FMMC_TYPE_MG_DARTS
	OR iMissionType = FMMC_TYPE_MG_ARM_WRESTLING
		RETURN (ciIN_LOCATE_DISTANCE_INSIDE - ciIN_LOCATE_DISTANCE_REDUCTION)
	ELSE
		RETURN (ciIN_LOCATE_DISTANCE - ciIN_LOCATE_DISTANCE_REDUCTION)
	ENDIF
ENDFUNC


FUNC FLOAT GET_CORONA_SLOT_ANGLE(INT iSlot, INT iNumSlots, BOOL bAdjustValue = TRUE)
	
	IF bAdjustValue
		ADJUST_CORONA_PED_SLOT_DATA(iSlot, iNumSlots)
	ENDIF

	RETURN (-iSlot * (360.0 / iNumSlots))
ENDFUNC

/// PURPOSE: Returns the position vector of the corona
FUNC VECTOR GET_PLAYER_POSITION_IN_CORONA(INT iSlot, VECTOR vCentrePosition, INT iMissionType, INT iNumPlayers, FLOAT &fGroundZ, FLOAT fBackupZ)

	FLOAT fAngleDelta = 0.0 + GET_CORONA_SLOT_ANGLE(iSlot, iNumPlayers, FALSE)
	
// KEITH 3/5/13: For positioning players we now always want to use the smaller radius (players are invisible and collision free) - other corona related stuff can still use the larger radius
//				 This is to ensure smaller mission coronas can be used without problems (ie: Heists use a smaller radius but GET_RADIUS_OF_CORONA() doesn't know if this activity is a Heist or not)
//				 This is a bit of a hack. If this causes problems the fix would be for GET_RADIUS_OF_CORONA() to use same decision making as Get_Corona_Trigger_Radius_From_MissionIdData()
//	FLOAT fRadius = GET_RADIUS_OF_CORONA(iMissionType)
	FLOAT fRadius = (ciIN_LOCATE_DISTANCE_INSIDE - ciIN_LOCATE_DISTANCE_REDUCTION)
	// Keep this param for now, but avoid unreferenced variable compiler error
	iMissionType = iMissionType
	
	VECTOR vPlayerPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCentrePosition, fAngleDelta, <<0.0, fRadius, 2.0>>)
	
	// For uneven grounds
	IF GET_GROUND_Z_FOR_3D_COORD(vPlayerPos, fGroundZ)
		PRINTLN("PROCESS_PLAYER_CORONA_POSITION - Found ground Z: ", fGroundZ)
		vPlayerPos.z = fGroundZ
		MPGlobals.coronaPosData.bFallBackZ = FALSE
	ELSE
		PRINTLN("PROCESS_PLAYER_CORONA_POSITION - Can't find ground Z, fall back: ", fBackupZ)
		vPlayerPos.z = fBackupZ
		MPGlobals.coronaPosData.bFallBackZ = TRUE
	ENDIF
	
	RETURN vPlayerPos
ENDFUNC

// 1849349
FUNC FLOAT POSITION_HEIST_PLAYERS_TO_FACE_BOARD(VECTOR vPlayerPos, INT iProperty = -1)
	VECTOR vHeistBoardPos
	VECTOR vReturnCoords
	
	INT iInsideProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
	IF iProperty <> -1
		iInsideProperty = iProperty
	ENDIF
	
	// Grab Heist Board coords
	GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistBoardPos, MP_PROP_ELEMENT_HEIST_BOARD_LOC, iInsideProperty)

	// Grab the coords from board to player
	vReturnCoords = (vHeistBoardPos - vPlayerPos)
	
	FLOAT fNewHeading = GET_HEADING_BETWEEN_VECTORS_2D(vPlayerPos, vHeistBoardPos)
	
	PRINTLN("POSITION_HEIST_PLAYERS_TO_FACE_BOARD, vHeistBoardPos   = ", vHeistBoardPos)
	PRINTLN("POSITION_HEIST_PLAYERS_TO_FACE_BOARD, vPlayerPos       = ", vPlayerPos)
	PRINTLN("POSITION_HEIST_PLAYERS_TO_FACE_BOARD, vReturnCoords    = ", vReturnCoords)
	PRINTLN("POSITION_HEIST_PLAYERS_TO_FACE_BOARD, fNewHeading    = ", fNewHeading)	
	
	RETURN fNewHeading
ENDFUNC

/// PURPOSE: Positions player at edge of corona (KGM 9/6/13: Except for Contact Missions).
PROC PROCESS_PLAYER_CORONA_POSITION(	BOOL 					bNewCorona,
										VECTOR 					vCentrePosition, 			//Corona position
										BOOL 					bReadyToMoveOn,				//Move on
										INT 					iNumPlayers, 				//Number of players
										INT						iMissionType,				// Used for betting ID
										BOOL					bIsContactMission,			// TRUE for Contact Missions
										BOOL					bIsHeistMission,			// TRUE for Heist Missions
										FLOAT					fBackUpZ)			
										
	//get out if it's a strand mission!
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		EXIT
	ENDIF
	
	BOOL bIsRaceTutorial = FALSE
	
	SWITCH GET_LOCAL_PLAYER_CORONA_POS_STATE()
	
		// Get the player into their position on corona edge
		CASE CORONA_POS_STATE_HAS_SLOT
			IF NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_PLAYER_TELEPORT_ACTIVE()
					IF bReadyToMoveOn
						IF IS_SKYSWOOP_AT_GROUND()
						OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
						OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
					
							// KGM 9/6/13: Don't position player in corona for Contact Missions
							IF NOT (bIsContactMission)
							OR IS_ARENA_WARS_JOB() 

								PRINTLN("PROCESS_PLAYER_CORONA_POSITION - Moving player into slot position")
								
								IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
									IF NOT HAS_FM_RACE_TUT_BEEN_DONE()
										bIsRaceTutorial = TRUE
										PRINTLN("PROCESS_PLAYER_CORONA_POSITION - bIsRaceTutorial = TRUE")
									ENDIF
								ENDIF
								
								FLOAT fPlayerHeading
								VECTOR vPlayerPos

								// Retrieve position of player in corona
								FLOAT fGroundZ
								
								vPlayerPos = GET_PLAYER_POSITION_IN_CORONA(MPGlobals.coronaPosData.iCoronaPosition, vCentrePosition, iMissionType, iNumPlayers, fGroundZ, fBackUpZ)
								
								// If this is a heist, ignore our newly calculated Z.
								IF bIsHeistMission
								OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
								OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
									PRINTLN("[CORONA] PROCESS_PLAYER_CORONA_POSITION - Mission is a HEIST / Q.RESTART / ROUNDS, reset z to: ", vCentrePosition.z)
									vPlayerPos.z = vCentrePosition.z
									MPGlobals.coronaPosData.bFallBackZ = FALSE
								ENDIF
								
								//...set player position and clear any tasks
								SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)					// Disable players action mode
								
								IF NOT bIsRaceTutorial
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())						// Clear all running tasks
								ENDIF
								
								SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)					// Make ped stand up fully
								SET_PED_LEG_IK_MODE(PLAYER_PED_ID(), LEG_IK_OFF)					// Prevent player crouching from interaction
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)	// Remove weapon / action mode
								
								PRINTLN("[CORONA] PROCESS_PLAYER_CORONA_POSITION - Player POSITION: ", vPlayerPos)
								
								//...disable player control so they are fixed in position
								IF NOT bIsRaceTutorial
									IF bNewCorona
									
										IF GET_HEIST_CORONA_FROM_PLANNING_BOARD()
											NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_FREEZE_POSITION)
										ELSE
									
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
									
										ENDIF
									
									ELSE
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
									ENDIF
								ELSE
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								ENDIF
								
								
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								IF NOT bIsRaceTutorial
								AND GET_FM_JOB_ENTERY_TYPE() != ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA_WALL 
									SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerPos, FALSE)
										
									IF bIsHeistMission
										fPlayerHeading = POSITION_HEIST_PLAYERS_TO_FACE_BOARD(vPlayerPos)
									ELSE
									
										fPlayerHeading = GET_HEADING_FROM_VECTOR_2D(vCentrePosition.x - vPlayerPos.x, vCentrePosition.y - vPlayerPos.y)
									
									ENDIF
									
									SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerHeading)
								ENDIF
								MPGlobals.coronaPosData.iSlotSelected = MPGlobals.coronaPosData.iCoronaPosition
								//MPGLobals.coronaPosData.playerID = PLAYER_ID()
							ELSE
								// KGM 9/6/13: This must be a Contact Mission
								PRINTLN("PROCESS_PLAYER_CORONA_POSITION - CONTACT MISSION, so not moving player into slot position")
								
								// For now, still remove player control for Contact Missions because the screen is blurry - may need to change this to passive mode instead
								//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, FALSE, FALSE, FALSE, TRUE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
								
								IF IS_PLAYER_LAUNCHING_FIXER_SHORT_TRIP_IN_SMOKING_ROOM(PLAYER_ID())
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION)
								ELSE
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION | NSPC_ALLOW_DURING_TRANSITION)
								ENDIF
							ENDIF
							
							// Update player state
							GOTO_CORONA_POS_STATE(CORONA_POS_STATE_SET_COLLISION)
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("PROCESS_PLAYER_CORONA_POSITION - FAILING ON IS_SKYSWOOP_AT_GROUND()")
						#ENDIF
						
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("PROCESS_PLAYER_CORONA_POSITION - FAILING ON bReadyToMoveOn")
					#ENDIF
					
					ENDIF
				ELSE
					PRINTLN("PROCESS_PLAYER_CORONA_POSITION - FAILING ON IS_PLAYER_TELEPORT_ACTIVE")
				ENDIF
			ENDIF
		BREAK
		
		CASE CORONA_POS_STATE_SET_COLLISION
			
			//SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
			
			GOTO_CORONA_POS_STATE(CORONA_POS_STATE_IN_POSITION)

//			IF IS_SCREEN_FADED_OUT()
//			AND NOT IS_SCREEN_FADING_IN()	
//				PRINTLN("CORONA_POS_STATE_SET_COLLISION - DO_SCREEN_FADE_IN(1000)")
//				DO_SCREEN_FADE_IN(1000)
//			ENDIF

		BREAK
		
		CASE CORONA_POS_STATE_IN_POSITION
			
			// If we have had to use a failsafe Z coord, try and correct ourselves
			IF MPGlobals.coronaPosData.bFallBackZ
				
				FLOAT fGroundZ
				VECTOR vPlayerCurrentPos
				vPlayerCurrentPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				
				IF GET_GROUND_Z_FOR_3D_COORD(vPlayerCurrentPos, fGroundZ)
					
					vPlayerCurrentPos.z = fGroundZ
					PRINTLN("CORONA_POS_STATE_IN_POSITION - CORRECT THE PLAYERS POSITION: ", vPlayerCurrentPos)
					
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerCurrentPos, FALSE)
					MPGlobals.coronaPosData.bFallBackZ = FALSE
				ENDIF
			ENDIF
			
//			// If the slot we are looking at is no longer active, reset
//			IF MPGlobals.coronaPosData.iSlotSelected != MPGlobals.coronaPosData.iCoronaPosition
//				IF NOT IS_CORONA_SLOT_STILL_ACTIVE()
//					NET_PRINT("[JA@CORONA] PROCESS_PLAYER_CORONA_POSITION - slot is not active so look back at us.") NET_NL()
//				
//					RESET_CORONA_POSITION_TO_LOCAL_PLAYER(vCentrePosition, iNumPlayers, iMissionType, ScaleformPlayerCardStruct)
//				ENDIF
//			ENDIF
				
		BREAK
	
	ENDSWITCH
ENDPROC

// *************************
//		DEBUG FUNCTIONS
// *************************

#IF IS_DEBUG_BUILD

/// PURPOSE: Returns TRUE if corona positioning is turned on.
FUNC BOOL IS_CORONA_POSITIONING_ENABLED()
	RETURN GlobalServerBD.g_bDebugEnableCoronaPos
ENDFUNC

/// PURPOSE: Client broadcast to turn on / off corona positioning
PROC BROADCAST_DEBUG_TOGGLE_CORONA_POSITION()
	EVENT_STRUCT_DEBUG_TOGGLE_CORONA_POS Event
	Event.Details.Type = SCRIPT_EVENT_TOGGLE_CORONA_POS
	Event.Details.FromPlayerIndex = PLAYER_ID()
	
	INT iSendTo = ALL_PLAYERS()
	IF NOT (iSendTo = 0)		
		
		PRINTLN("[JA@CORONA] BROADCAST_DEBUG_TOGGLE_CORONA_POSITIONING - SENT TO SERVER")
		
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		PRINTLN("[JA@CORONA] BROADCAST_DEBUG_TOGGLE_CORONA_POSITIONING - playerflags = 0 so not broadcasting")
	ENDIF
ENDPROC

/// PURPOSE: Server updates value of corona positioning bool based event from client
PROC PROCESS_CORONA_POSITION_TOGGLED()
	GlobalServerBD.g_bDebugEnableCoronaPos = !GlobalServerBD.g_bDebugEnableCoronaPos
	
	IF GlobalServerBD.g_bDebugEnableCoronaPos
		PRINTLN("[JA@CORONA] SERVER: Corona Position System now turned ON")
	ELSE
		PRINTLN("[JA@CORONA] SERVER: Corona Position System now turned OFF")
	ENDIF
ENDPROC

// Maintain Corona positioning widgets
PROC MAINTAIN_CORONA_POS_WIDGETS()
	
	IF g_bDebugToggleCoronaPositions
	
		IF GlobalServerBD.g_bDebugEnableCoronaPos
			PRINTLN("[JA@CORONA] Corona Position System turned OFF")
		ELSE
			PRINTLN("[JA@CORONA] Corona Position System turned ON")
		ENDIF
	
		BROADCAST_DEBUG_TOGGLE_CORONA_POSITION()
	
		g_bDebugToggleCoronaPositions = FALSE
	ENDIF
	
	IF iDebugCoronaPosition != -1
		
		MPGlobals.coronaPosData.iCoronaPosition = iDebugCoronaPosition
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCoronaPosState = CORONA_POS_STATE_HAS_SLOT
		
		iDebugCoronaPosition = -1
	ENDIF
	
ENDPROC

// Create Corona positioning widgets
PROC CREATE_CORONA_POS_WIDGETS()
	START_WIDGET_GROUP("Corona Positions")
	
		ADD_WIDGET_BOOL("Toggle Corona Positioning", g_bDebugToggleCoronaPositions)
		ADD_WIDGET_INT_SLIDER("Debug corona position", iDebugCoronaPosition, -1, 32, 1)

		START_WIDGET_GROUP("Server Corona Positioning Bitset")
			INT i
			TEXT_LABEL_15 tlCorona
			REPEAT 10 i
				tlCorona = "Corona "
				tlCorona += i
				ADD_WIDGET_INT_READ_ONLY(tlCorona, GlobalServerBD_BlockB.g_BSVoteCoronaPos[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()

		ADD_WIDGET_INT_READ_ONLY("Player Corona Position: ", MPGlobals.coronaPosData.iCoronaPosition)
		ADD_WIDGET_INT_READ_ONLY("Player State: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCoronaPosState)
	
	STOP_WIDGET_GROUP()
ENDPROC

#ENDIF

