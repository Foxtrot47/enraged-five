//////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_joyrider.sch														//
// Description: Mainly deals with the launching of the Joyrider script.					//
// Written by:  Ryan Baker																//
// Date: 09/01/2013																		//
//////////////////////////////////////////////////////////////////////////////////////////

// MODE INDEPENDENT INCLUDES
USING "globals.sch"
USING "script_ped.sch"
USING "commands_player.sch"
USING "script_network.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "net_script_timers.sch"
USING "net_mission.sch"

USING "net_include.sch"
USING "fm_unlocks_header.sch"
USING "net_freemode_cut.sch"

// CONST INTS
CONST_INT JOYRIDER_SPAWN_TIMER_LIMIT	5000

// ENUMS
ENUM JOYRIDER_LAUNCHING_STATE
	eJOYRIDERLAUNCHINGSTATE_INIT = 0,
	eJOYRIDERLAUNCHINGSTATE_WAIT,
	eJOYRIDERLAUNCHINGSTATE_LAUNCHING,
	eJOYRIDERLAUNCHINGSTATE_RUNNING
ENDENUM

ENUM JOYRIDER_WAIT_STATE
	JOYRIDERWAITSTATE_ONE_DAY_SESSION = 0,
	JOYRIDERWAITSTATE_ONE_DAY_SINCE_LAST_SPAWN,
	JOYRIDERWAITSTATE_OTHER_CHECKS
ENDENUM

// STRUCTS
STRUCT STRUCT_JOYRIDER_LAUNCHING_DATA
	JOYRIDER_LAUNCHING_STATE eLaunchingState
	JOYRIDER_WAIT_STATE eLaunchingWaitState
	SCRIPT_TIMER sJoyriderSpawnTimer
	MP_MISSION_DATA JoyriderToLaunch
	INT iAttemptsCounter
ENDSTRUCT

// FUNCTIONS
#IF IS_DEBUG_BUILD

BOOL bDebugLaunchJoyrider

FUNC STRING GET_JOYRIDER_LAUNCH_STATE_NAME(JOYRIDER_LAUNCHING_STATE eState)
	SWITCH eState
		CASE eJOYRIDERLAUNCHINGSTATE_INIT		RETURN "INIT"
		CASE eJOYRIDERLAUNCHINGSTATE_WAIT		RETURN "WAIT"
		CASE eJOYRIDERLAUNCHINGSTATE_LAUNCHING	RETURN "LAUNCHING"
		CASE eJOYRIDERLAUNCHINGSTATE_RUNNING	RETURN "RUNNING	"
	ENDSWITCH
	
	RETURN "NOT_IN_SWITCH"
ENDFUNC	

FUNC STRING GET_JOYRIDER_WAIT_STATE_NAME(JOYRIDER_WAIT_STATE eLaunchingWaitState)
	SWITCH eLaunchingWaitState
		CASE JOYRIDERWAITSTATE_ONE_DAY_SESSION			RETURN "ONE_DAY_SESSION"
		CASE JOYRIDERWAITSTATE_ONE_DAY_SINCE_LAST_SPAWN	RETURN "ONE_DAY_SINCE_LAST_SPAWN"
		CASE JOYRIDERWAITSTATE_OTHER_CHECKS				RETURN "OTHER_CHECKS"
	ENDSWITCH
	
	RETURN "INVALID STATE"
ENDFUNC
	
#ENDIF

/// PURPOSE:
///    Sets the Joyrider launch state.
/// PARAMS:
///    eState - sate to set to.
PROC SET_JOYRIDER_LAUNCH_STATE(STRUCT_JOYRIDER_LAUNCHING_DATA &structData, JOYRIDER_LAUNCHING_STATE eState)
	#IF IS_DEBUG_BUILD
		IF (eState != structData.eLaunchingState)
			NET_PRINT("*RLB - Joyrider: going to state ")NET_PRINT(GET_JOYRIDER_LAUNCH_STATE_NAME(eState))NET_NL()
		ENDIF
	#ENDIF
	structData.eLaunchingState = eState
ENDPROC

/// PURPOSE:
///    Gets the Joyrider launch state.
/// RETURNS:
///    Launch state.
FUNC JOYRIDER_LAUNCHING_STATE GET_JOYRIDER_LAUNCH_STATE(STRUCT_JOYRIDER_LAUNCHING_DATA &structData)
	RETURN structData.eLaunchingState
ENDFUNC

/// PURPOSE:
///    Sets the Joyrider launch wait state.
/// PARAMS:
///    eState - sate to set to.
PROC SET_JOYRIDER_LAUNCH_WAIT_STATE(STRUCT_JOYRIDER_LAUNCHING_DATA &structData, JOYRIDER_WAIT_STATE eState)
	#IF IS_DEBUG_BUILD
		IF (eState != structData.eLaunchingWaitState)
			NET_PRINT("*RLB - Joyrider: going to wait state ")NET_PRINT(GET_JOYRIDER_WAIT_STATE_NAME(eState))NET_NL()
		ENDIF
	#ENDIF
	structData.eLaunchingWaitState = eState
ENDPROC

/// PURPOSE:
///    Gets the Joyrider launch wait state.
/// RETURNS:
///    Launch state.
FUNC JOYRIDER_WAIT_STATE GET_JOYRIDER_LAUNCH_WAIT_STATE(STRUCT_JOYRIDER_LAUNCHING_DATA &structData)
	RETURN structData.eLaunchingWaitState
ENDFUNC

/// PURPOSE:
///    Resets the Joyrider spawn timer that checks the spawn conditions have stayed true for long enough.
/// PARAMS:
///    structData - struct of Joyrider data.
PROC RESET_JOYRIDER_SPAWN_TIMER(STRUCT_JOYRIDER_LAUNCHING_DATA &structData)
	
	RESET_NET_TIMER(structData.sJoyriderSpawnTimer)
	SCRIPT_TIMER stTemp
	structData.sJoyriderSpawnTimer = stTemp
	
ENDPROC

/// PURPOSE:
///    Cleans up Joyrider launcher data.
PROC CLEANUP_JOYRIDER(STRUCT_JOYRIDER_LAUNCHING_DATA &structData)
	
	// Temp structs.
	STRUCT_JOYRIDER_LAUNCHING_DATA sTemp0
	
	// Wipe Joyrider data.
	structData = sTemp0
	
	// Set state back to init.
	SET_JOYRIDER_LAUNCH_STATE(structData, eJOYRIDERLAUNCHINGSTATE_INIT)
	
	// Reset spawn timer.
	RESET_JOYRIDER_SPAWN_TIMER(structData)
											
	// Do debug info.
	#IF IS_DEBUG_BUILD
		CLEAR_BIT(ib_DebugMenuLaunchingBitSet,DBM_START_JOYRIDER)
		CLEAR_BIT(ib_DebugMenuLaunchingBitSet,DBM_WARP_TO_JOYRIDER)
		bDebugLaunchJoyrider = FALSE
		NET_PRINT("*RLB - Joyrider: cleaned up.")NET_NL()
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Performs free mode only checks to get if the local player is ok to run the Joyrider script.
/// RETURNS:
///    TRUE if ok to run Joyrider script in free mode, FALSE if not.
FUNC BOOL CAN_RUN_JOYRIDER_FREEMODE_CHECKS(BOOL bDoDebugPrints = FALSE)
	
	IF bDoDebugPrints = bDoDebugPrints
		bDoDebugPrints = bDoDebugPrints
	ENDIF
	
	// Is player playng a freemode mode/mission? (i.e. playing a deathmatch)
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		IF bDoDebugPrints
			NET_PRINT("[RLB] - Joyrider - CAN_RUN_JOYRIDER_FREEMODE_CHECKS - IS_PLAYER_ON_ANY_FM_MISSION = TRUE.")NET_NL()
		ENDIF
		RETURN FALSE
	ENDIF
	
	// Is player launching a freemode mode/mission? (i.e. launching a deathmatch)
    IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		IF bDoDebugPrints
			NET_PRINT("[RLB] - Joyrider - CAN_RUN_JOYRIDER_FREEMODE_CHECKS - IS_FM_MISSION_LAUNCH_IN_PROGRESS = TRUE.")NET_NL()
		ENDIF
		RETURN FALSE
	ENDIF
	
	// Is player running an activity session?
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF bDoDebugPrints
			NET_PRINT("[RLB] - Joyrider - CAN_RUN_JOYRIDER_FREEMODE_CHECKS - NETWORK_IS_ACTIVITY_SESSION = TRUE.")NET_NL()
		ENDIF
		RETURN FALSE
	ENDIF
	
	// Is player doing a tutorial?
    IF NETWORK_IS_IN_TUTORIAL_SESSION()
		IF bDoDebugPrints
			NET_PRINT("[RLB] - Joyrider - CAN_RUN_JOYRIDER_FREEMODE_CHECKS - NETWORK_IS_IN_TUTORIAL_SESSION = TRUE.")NET_NL()
		ENDIF
		RETURN FALSE
	ENDIF
	
	// Has player unlocked Import Export?
	IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_IMPORT_EXPORT)
		IF bDoDebugPrints
			NET_PRINT("[RLB] - Joyrider - CAN_RUN_JOYRIDER_FREEMODE_CHECKS - IS_FM_TYPE_UNLOCKED(FMMC_TYPE_JOYRIDER) = FALSE.")NET_NL()
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_MP_AMBIENT_SCRIPT_BLOCKED(MPAM_TYPE_JOYRIDER)
		IF bDoDebugPrints
			NET_PRINT("[RLB] - Joyrider - CAN_RUN_JOYRIDER_FREEMODE_CHECKS - IS_MP_AMBIENT_SCRIPT_BLOCKED(MPAM_TYPE_JOYRIDER) = FALSE.")NET_NL()
		ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets if player is ok to launch Joyrider script.
/// RETURNS:
///    TRUE if ok, FALSE if not.
FUNC BOOL CAN_I_LAUNCH_JOYRIDER()
	
	// Am I dead?
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	// Free mode checks.
	IF NOT CAN_RUN_JOYRIDER_FREEMODE_CHECKS()
		RETURN FALSE
	ENDIF
	
//	// Is a Joyrider schedule active?
//	IF NOT ( (GET_CLOCK_HOURS() >= 1) AND (GET_CLOCK_HOURS() < 11) )
//	AND NOT ( (GET_CLOCK_HOURS() >= 12) AND (GET_CLOCK_HOURS() < 22) )
//		RETURN FALSE
//	ENDIF

//	// Is any player on a mission?
//	IF GET_IS_ANY_ACTIVE_PLAYER_ON_ANY_MISSION()
//		RETURN FALSE
//	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Runs Joyrider spawn timer.
/// RETURNS:
///    True if timer limit reached, false if not.
FUNC BOOL RUN_JOYRIDER_SPAWN_TIMER(STRUCT_JOYRIDER_LAUNCHING_DATA &structData)
	
	IF NOT HAS_NET_TIMER_STARTED(structData.sJoyriderSpawnTimer)
		START_NET_TIMER(structData.sJoyriderSpawnTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(structData.sJoyriderSpawnTimer, JOYRIDER_SPAWN_TIMER_LIMIT)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE:
FUNC INT GET_CLOCK_HOUR_WITH_ADDED_HOURS(INT iHoursToAdd)
	NET_PRINT("*RLB - Joyrider: GET_CLOCK_HOUR_WITH_ADDED_HOURS - CURRENT HOUR = ") NET_PRINT_INT(GET_CLOCK_HOURS()) NET_NL()
	
	IF iHoursToAdd >= 24
		iHoursToAdd = (iHoursToAdd-24)
		NET_PRINT("*RLB - Joyrider: GET_CLOCK_HOUR_WITH_ADDED_HOURS - iHoursToAdd >= 24") NET_NL()
	ENDIF
	NET_PRINT("*RLB - Joyrider: GET_CLOCK_HOUR_WITH_ADDED_HOURS - iHoursToAdd = ") NET_PRINT_INT(iHoursToAdd) NET_NL()
	
	INT iHour = (GET_CLOCK_HOURS() + iHoursToAdd)
	
	IF iHour >= 24
		iHour = (iHour-24)
		NET_PRINT("*RLB - Joyrider: GET_CLOCK_HOUR_WITH_ADDED_HOURS - iHour >= 24") NET_NL()
	ENDIF
	NET_PRINT("*RLB - Joyrider: GET_CLOCK_HOUR_WITH_ADDED_HOURS - iHour = ") NET_PRINT_INT(iHour) NET_NL()
	
	RETURN iHour
ENDFUNC

/// PURPOSE:
///    Processes flag that indicates if the session has been runing for >= one in game day (24 hours on the in-game clock).
///    Should only be processed by the session host.
PROC MAINTAIN_ENOUGH_TIME_PASSED_SINCE_SESSION_START_FLAG()

	IF (GlobalServerBD_BlockB.sJoyriderClockData.iHourSessionStarted = (-1))
		IF HAS_FREEMODE_INTRO_BEEN_DONE()
			GlobalServerBD_BlockB.sJoyriderClockData.iHourSessionStarted = GET_CLOCK_HOUR_WITH_ADDED_HOURS(GET_RANDOM_INT_IN_RANGE(12, 19))
			
			NET_PRINT("*RLB - Joyrider: MAINTAIN_ENOUGH_TIME_PASSED_SINCE_SESSION_START_FLAG - Hour of first Joyrider = ")
			NET_PRINT_INT(GlobalServerBD_BlockB.sJoyriderClockData.iHourSessionStarted) NET_NL()
		ENDIF
	ELSE
	
		SWITCH GlobalServerBD_BlockB.sJoyriderClockData.iSessionRunningOneInGameDayCheckStage
			
			CASE 0
				//IF (GET_CLOCK_HOURS() != GlobalServerBD_BlockB.sJoyriderClockData.iHourSessionStarted)
				//	NET_PRINT("*RLB - Joyrider: MAINTAIN_ENOUGH_TIME_PASSED_SINCE_SESSION_START_FLAG - clock hour not on hour first player entered game, iSessionRunningOneInGameDayCheckStage = 1.")NET_NL()
					GlobalServerBD_BlockB.sJoyriderClockData.iSessionRunningOneInGameDayCheckStage++
				//ENDIF
			BREAK
			
			CASE 1
				IF (GET_CLOCK_HOURS() = GlobalServerBD_BlockB.sJoyriderClockData.iHourSessionStarted)
				AND ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH(1, 1, 0, TRUE, FALSE)
					NET_PRINT("*RLB - Joyrider: MAINTAIN_ENOUGH_TIME_PASSED_SINCE_SESSION_START_FLAG - been one in game day since first player entered session, bSessionBeenRunningOneInGameDay = TRUE, iSessionRunningOneInGameDayCheckStage = 1.")NET_NL()
					GlobalServerBD_BlockB.sJoyriderClockData.bSessionBeenRunningOneInGameDay = TRUE
					GlobalServerBD_BlockB.sJoyriderClockData.iSessionRunningOneInGameDayCheckStage++
				ENDIF
			BREAK
			
			CASE 2
				// Done! Been running for >= one in game day!
			BREAK
			
		ENDSWITCH
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Runs checks that it has been one day since the last spawn of the Joyrider.
//PROC MAINTAIN_HOUR_JOYRIDER_LAST_SPAWNED()
//	
//	SWITCH GlobalServerBD_BlockB.sJoyriderClockData.iMaintainHourSpawnedStage
//		
//		CASE 0
//			IF DOES_SCRIPT_EXIST("AM_JOYRIDER")
//				IF NETWORK_IS_SCRIPT_ACTIVE("AM_JOYRIDER")
//				
//					GlobalServerBD_BlockB.sJoyriderClockData.iMaintainHourSpawnedStage++
//					NET_PRINT("*RLB - Joyrider: MAINTAIN_HOUR_JOYRIDER_LAST_SPAWNED - Joyrider script is running, going to iMaintainHourSpawnedStage = 1")NET_NL()
//					
//				ENDIF
//			ENDIF
//		BREAK
//		
//		CASE 1
//			IF DOES_SCRIPT_EXIST("AM_JOYRIDER")
//				IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_JOYRIDER")
//				
//					GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded = GET_CLOCK_HOUR_WITH_ADDED_HOURS(GET_RANDOM_INT_IN_RANGE(8, 17))	//GET_CLOCK_HOURS()
//					GlobalServerBD_BlockB.sJoyriderClockData.iMaintainHourSpawnedStage = 0
//					
//					NET_PRINT("*RLB - Joyrider: MAINTAIN_HOUR_JOYRIDER_LAST_SPAWNED - Hour next Joyrider starts = ")
//					NET_PRINT_INT(GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded)
//					NET_NL()
//					
//				ENDIF
//			ENDIF
//		BREAK
//		
//	ENDSWITCH
//	
//ENDPROC

PROC MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG()
	
	SWITCH GlobalServerBD_BlockB.sJoyriderClockData.iCheckStage
	
		CASE 0
			IF GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded != (-1)
			
				//IF (GET_CLOCK_HOURS() != GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded)
										
					GlobalServerBD_BlockB.sJoyriderClockData.iCheckStage++
					
				//	NET_PRINT("*RLB - Joyrider: MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG - clock hour Joyrider ended = ") 
				//	NET_PRINT_INT(GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded) NET_NL()
					
				//	NET_PRINT("*RLB - Joyrider: MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG - GET_CLOCK_HOURS() = ")
				//	NET_PRINT_INT(GET_CLOCK_HOURS()) NET_NL()
					
				//	NET_PRINT("*RLB - Joyrider: MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG - clock hour not on hour Joyrider ended, iCheckStage = 1.") NET_NL()
				//ENDIF
				NET_PRINT("*RLB - Joyrider: MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG - GlobalServerBD_BlockB.sJoyriderClockData.iCheckStage = 1")
			ELSE
				GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded = GET_CLOCK_HOUR_WITH_ADDED_HOURS(GET_RANDOM_INT_IN_RANGE(8, 17))	//GET_CLOCK_HOURS()
				GlobalServerBD_BlockB.sJoyriderClockData.iMaintainHourSpawnedStage = 0
				
				NET_PRINT("*RLB - Joyrider: MAINTAIN_HOUR_JOYRIDER_LAST_SPAWNED - BBB - Hour next Joyrider starts = ")
				NET_PRINT_INT(GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded)
				NET_NL()
			ENDIF
		BREAK
		
		CASE 1
			IF (GET_CLOCK_HOURS() = GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded)
			AND ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH(1, 1, 0, TRUE, FALSE)
				
				NET_PRINT("*RLB - Joyrider: MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG - clock hour Joyrider ended = ")
				NET_PRINT_INT(GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded) NET_NL()
				
				NET_PRINT("*RLB - Joyrider: MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG - GET_CLOCK_HOURS() = ")
				NET_PRINT_INT(GET_CLOCK_HOURS()) NET_NL()
				
				NET_PRINT("*RLB - Joyrider: been one in game day since Joyrider last spawned, bClockCheckAllowSpawn = TRUE, iCheckStage = 2") NET_NL()
				
				GlobalServerBD_BlockB.sJoyriderClockData.bClockCheckAllowSpawn = TRUE
				GlobalServerBD_BlockB.sJoyriderClockData.iCheckStage++
				
			ENDIF
		BREAK
		
		CASE 2
			IF DOES_SCRIPT_EXIST("AM_JOYRIDER")
				IF NETWORK_IS_SCRIPT_ACTIVE("AM_JOYRIDER")
					
					GlobalServerBD_BlockB.sJoyriderClockData.iMaintainHourSpawnedStage++
					NET_PRINT("*RLB - Joyrider: MAINTAIN_HOUR_JOYRIDER_LAST_SPAWNED - Joyrider script is running, going to iMaintainHourSpawnedStage = 1")NET_NL()
					
					//From MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG
					NET_PRINT("*RLB - Joyrider: MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG - Joyrider script running, iCheckStage = 3") NET_NL()
					GlobalServerBD_BlockB.sJoyriderClockData.iCheckStage++
					
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF DOES_SCRIPT_EXIST("AM_JOYRIDER")
				IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_JOYRIDER")
				
					NET_PRINT("*RLB - Joyrider: MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG - Joyrider script stopped running, resetting check data, iCheckStage = 0") NET_NL()
				
					GlobalServerBD_BlockB.sJoyriderClockData.iCheckStage = 0
					GlobalServerBD_BlockB.sJoyriderClockData.bClockCheckAllowSpawn = FALSE
					
					//From MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG
					GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded = GET_CLOCK_HOUR_WITH_ADDED_HOURS(GET_RANDOM_INT_IN_RANGE(8, 17))	//GET_CLOCK_HOURS()
					GlobalServerBD_BlockB.sJoyriderClockData.iMaintainHourSpawnedStage = 0
					
					NET_PRINT("*RLB - Joyrider: MAINTAIN_HOUR_JOYRIDER_LAST_SPAWNED - Hour next Joyrider starts = ")
					NET_PRINT_INT(GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded)
					NET_NL()
					
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL FREEMODE_LAUNCH_JOYRIDER_SCRIPT(STRUCT_JOYRIDER_LAUNCHING_DATA &structData)
	
	structData.JoyriderToLaunch.mdID.idMission = eAM_JOYRIDER
	structData.JoyriderToLaunch.iInstanceId = (-1)
							
	RETURN NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(structData.JoyriderToLaunch)
ENDFUNC

FUNC BOOL IS_PLAYER_AT_DOCKS_JR()

	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1216.994995,-3001.905762,4.865235>>, <<835.374695,-3013.846436,54.763802>>, 250.000000)
		RETURN TRUE
	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1284.885010,-3184.985840,4.906164>>, <<833.896362,-3182.306396,54.901245>>, 250.000000)
		RETURN TRUE
	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1284.792847,-3270.264648,4.902858>>, <<838.359741,-3268.247559,55.074493>>, 250.000000)
		RETURN TRUE
	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<864.664185,-2900.036865,4.900886>>, <<864.003662,-3345.968750,54.900665>>, 250.000000)
		RETURN TRUE
	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<447.005920,-2983.689453,5.008680>>, <<750.049988,-2981.952881,54.885017>>, 250.000000)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if the player is in range of a road node
FUNC BOOL IS_PLAYER_CLOSE_TO_VEHICLE_NODE(VECTOR vCoords, VECTOR vDimensions)
	VECTOR vNode
	IF GET_CLOSEST_VEHICLE_NODE(vCoords, vNode, NF_NONE)
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vNode, vDimensions)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD

TEXT_WIDGET_ID twIDJoyriderLaunchState
TEXT_WIDGET_ID twIDWaitStateJR
INT iJoyriderSpawntimerValueDebug
BOOL bHasJoyriderSpawnTimerStartedDebug
BOOL bOnAmbientScriptJR
PROC ADD_JOYRIDER_LAUNCHING_WIDGETS()
	
	START_WIDGET_GROUP("Joyrider Launching Widgets")
		
		ADD_WIDGET_BOOL("bDebugLaunchJoyrider", bDebugLaunchJoyrider)
		ADD_WIDGET_BOOL("On Amb Script", bOnAmbientScriptJR)
		
		START_WIDGET_GROUP("Globals")
			ADD_WIDGET_INT_READ_ONLY("Session Start Hour", GlobalServerBD_BlockB.sJoyriderClockData.iHourSessionStarted)
			ADD_WIDGET_BOOL("Session Been running One Day", GlobalServerBD_BlockB.sJoyriderClockData.bSessionBeenRunningOneInGameDay)
			ADD_WIDGET_INT_READ_ONLY("Session Running One Day Check Stage", GlobalServerBD_BlockB.sJoyriderClockData.iSessionRunningOneInGameDayCheckStage)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Launch State")
			twIDJoyriderLaunchState = ADD_TEXT_WIDGET("Launch State")
			twIDWaitStateJR = ADD_TEXT_WIDGET("Wait State")
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("In Game Days Check")
			ADD_WIDGET_INT_READ_ONLY("Hour Last Spawned", GlobalServerBD_BlockB.sJoyriderClockData.iClockHoursJoyriderEnded)
			ADD_WIDGET_INT_READ_ONLY("Clock Check Stage", GlobalServerBD_BlockB.sJoyriderClockData.iCheckStage)
			ADD_WIDGET_BOOL("Clock Check Says Allow Spawn", GlobalServerBD_BlockB.sJoyriderClockData.bClockCheckAllowSpawn)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Spawn Timer")
			ADD_WIDGET_BOOL("Timer Started", bHasJoyriderSpawnTimerStartedDebug)
			ADD_WIDGET_INT_READ_ONLY("Timer Value", iJoyriderSpawntimerValueDebug)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_JOYRIDER_WIDGETS(STRUCT_JOYRIDER_LAUNCHING_DATA &structData)
	
	SET_CONTENTS_OF_TEXT_WIDGET(twIDJoyriderLaunchState, GET_JOYRIDER_LAUNCH_STATE_NAME(GET_JOYRIDER_LAUNCH_STATE(structData)))
	SET_CONTENTS_OF_TEXT_WIDGET(twIDWaitStateJR, GET_JOYRIDER_WAIT_STATE_NAME(GET_JOYRIDER_LAUNCH_WAIT_STATE(structData)))
	
	bOnAmbientScriptJR = IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_JOYRIDER)
	
	IF NOT HAS_NET_TIMER_STARTED(structData.sJoyriderSpawnTimer)
		bHasJoyriderSpawnTimerStartedDebug = FALSE
		iJoyriderSpawntimerValueDebug = 0
	ELSE
		bHasJoyriderSpawnTimerStartedDebug = TRUE
		iJoyriderSpawntimerValueDebug = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(structData.sJoyriderSpawnTimer)
	ENDIF
	
ENDPROC

#ENDIF

PROC MAINTAIN_JOYRIDER_CLIENT(STRUCT_JOYRIDER_LAUNCHING_DATA &structData)
	
	BOOL bGoToOtherChecks
	BOOL bResetSpawnTimer
	BOOL bCleanup
	
	// Maintain clock data.
	IF NETWORK_IS_HOST()
		MAINTAIN_ENOUGH_TIME_PASSED_SINCE_SESSION_START_FLAG()
		//MAINTAIN_HOUR_JOYRIDER_LAST_SPAWNED()
		MAINTAIN_HAS_BEEN_ONE_DAY_SINCE_LAST_JOYRIDER_SPAWN_FLAG()
	ENDIF
	
	// Main state switch.
	SWITCH GET_JOYRIDER_LAUNCH_STATE(structData)
		
		CASE eJOYRIDERLAUNCHINGSTATE_INIT
			
			// If mission is not disabled, go to wait stage.
			IF NOT IS_MP_MISSION_DISABLED(eAM_JOYRIDER)	
				NET_PRINT("*RLB - Joyrider: Joyrider script not disabled.")NET_NL()
				SET_JOYRIDER_LAUNCH_STATE(structData, eJOYRIDERLAUNCHINGSTATE_WAIT)
			ENDIF
			
		BREAK
		
		CASE eJOYRIDERLAUNCHINGSTATE_WAIT
			
			// Wait sub states switch.
			SWITCH GET_JOYRIDER_LAUNCH_WAIT_STATE(structData)
			
				CASE JOYRIDERWAITSTATE_ONE_DAY_SESSION
					
					// If some one is running the script, jump to the other checks.
					/*IF DOES_SCRIPT_EXIST("AM_JOYRIDER")
						IF NETWORK_IS_SCRIPT_ACTIVE("AM_JOYRIDER")
							NET_PRINT("*RLB - Joyrider: eJOYRIDERLAUNCHINGSTATE_WAIT - script is running on network.")NET_NL()
							bGoToOtherChecks = TRUE
						ENDIF
					ENDIF*/
					
					// Once the session has been running for one day or more, allow other checks to be processed.
					IF GlobalServerBD_BlockB.sJoyriderClockData.bSessionBeenRunningOneInGameDay
						NET_PRINT("*RLB - Joyrider: eJOYRIDERLAUNCHINGSTATE_WAIT - session has been running one day.")NET_NL()
						bGoToOtherChecks = TRUE
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(ib_DebugMenuLaunchingBitSet,DBM_START_JOYRIDER)
					OR bDebugLaunchJoyrider
						NET_PRINT("*RLB - Joyrider: eJOYRIDERLAUNCHINGSTATE_WAIT - Joyrider has been debug started.")NET_NL()
						bGoToOtherChecks = TRUE
					ENDIF
					#ENDIF
					
					IF bGoToOtherChecks
					AND ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH(1, 1, 0, TRUE, FALSE)
						NET_PRINT("*RLB - Joyrider: eJOYRIDERLAUNCHINGSTATE_WAIT -  going to other checks stage in prep for first launch.")NET_NL()
						SET_JOYRIDER_LAUNCH_WAIT_STATE(structData, JOYRIDERWAITSTATE_OTHER_CHECKS)
					ENDIF
					
				BREAK
				
				CASE JOYRIDERWAITSTATE_ONE_DAY_SINCE_LAST_SPAWN
					
					// If some one is running the script, jump to the other checks.
					/*IF DOES_SCRIPT_EXIST("AM_JOYRIDER")
						IF NETWORK_IS_SCRIPT_ACTIVE("AM_JOYRIDER")
							NET_PRINT("*RLB - Joyrider: JOYRIDERWAITSTATE_ONE_DAY_SINCE_LAST_SPAWN - script is running on network.")NET_NL()
							bGoToOtherChecks = TRUE
						ENDIF
					ENDIF*/
					
					// If server says it's been one day since the last Joyrider spawn, go to the other checks stage.
					IF GlobalServerBD_BlockB.sJoyriderClockData.bClockCheckAllowSpawn
						IF DOES_SCRIPT_EXIST("AM_JOYRIDER")
							IF NETWORK_IS_SCRIPT_ACTIVE("AM_JOYRIDER")
								// RESET IF SOMEBODY ELSE IS ALREADY RUNNING THE SCRIPT
								//IF bCleanup
									CLEANUP_JOYRIDER(structData)
									IF GlobalServerBD_BlockB.sJoyriderClockData.bSessionBeenRunningOneInGameDay
										SET_JOYRIDER_LAUNCH_WAIT_STATE(structData, JOYRIDERWAITSTATE_ONE_DAY_SINCE_LAST_SPAWN)
									ELSE
										SET_JOYRIDER_LAUNCH_WAIT_STATE(structData, JOYRIDERWAITSTATE_ONE_DAY_SESSION)
									ENDIF
									SET_JOYRIDER_LAUNCH_STATE(structData, eJOYRIDERLAUNCHINGSTATE_INIT)
									NET_PRINT("*RLB - Joyrider: RESET - A")NET_NL()
								//ENDIF
							ELSE
								NET_PRINT("*RLB - Joyrider: JOYRIDERWAITSTATE_ONE_DAY_SINCE_LAST_SPAWN - been one day since Joyrider last existed.")NET_NL()
								bGoToOtherChecks = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(ib_DebugMenuLaunchingBitSet,DBM_START_JOYRIDER)
					OR bDebugLaunchJoyrider
						NET_PRINT("*RLB - Joyrider: JOYRIDERWAITSTATE_ONE_DAY_SINCE_LAST_SPAWN - Joyrider has been debug started.")NET_NL()
						bGoToOtherChecks = TRUE
					ENDIF
					#ENDIF
					
					IF bGoToOtherChecks
					AND ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH(1, 1, 0, TRUE, FALSE)
						NET_PRINT("*RLB - Joyrider: JOYRIDERWAITSTATE_ONE_DAY_SINCE_LAST_SPAWN - going to other checks stage in prep for launch.")NET_NL()
						SET_JOYRIDER_LAUNCH_WAIT_STATE(structData, JOYRIDERWAITSTATE_OTHER_CHECKS)
					ENDIF
					
				BREAK
				
				CASE JOYRIDERWAITSTATE_OTHER_CHECKS
					
					// If script is already running.
					IF DOES_SCRIPT_EXIST("AM_JOYRIDER")
						IF NETWORK_IS_SCRIPT_ACTIVE("AM_JOYRIDER")
							
							// RESET IF SOMEBODY ELSE IS ALREADY RUNNING THE SCRIPT
							//IF bCleanup
								CLEANUP_JOYRIDER(structData)
								IF GlobalServerBD_BlockB.sJoyriderClockData.bSessionBeenRunningOneInGameDay
									SET_JOYRIDER_LAUNCH_WAIT_STATE(structData, JOYRIDERWAITSTATE_ONE_DAY_SINCE_LAST_SPAWN)
								ELSE
									SET_JOYRIDER_LAUNCH_WAIT_STATE(structData, JOYRIDERWAITSTATE_ONE_DAY_SESSION)
								ENDIF
								SET_JOYRIDER_LAUNCH_STATE(structData, eJOYRIDERLAUNCHINGSTATE_INIT)
								NET_PRINT("*RLB - Joyrider: RESET - B")NET_NL()
							//ENDIF
							// If we pass the free mode run Joyrider checks, go to the launching state.
							//IF CAN_RUN_JOYRIDER_FREEMODE_CHECKS()
							//	NET_PRINT("*RLB - Joyrider: JOYRIDERWAITSTATE_OTHER_CHECKS - CAN_RUN_JOYRIDER_FREEMODE_CHECKS() = TRUE.")NET_NL()
							//	SET_JOYRIDER_LAUNCH_STATE(structData, eJOYRIDERLAUNCHINGSTATE_LAUNCHING)
							//ENDIF
							
						// If the script is not running.
						ELSE
							IF RUN_JOYRIDER_SPAWN_TIMER(structData)	
							#IF IS_DEBUG_BUILD
							OR IS_BIT_SET(ib_DebugMenuLaunchingBitSet,DBM_START_JOYRIDER)
							OR bDebugLaunchJoyrider
							#ENDIF
								//NET_PRINT("						- RUN_JOYRIDER_SPAWN_TIMER()")					NET_NL()
								
								// If I pass the launch Joyrider checks for 30 seconds and I am not at the docks.
								IF CAN_I_LAUNCH_JOYRIDER()
								#IF IS_DEBUG_BUILD
								OR IS_BIT_SET(ib_DebugMenuLaunchingBitSet,DBM_START_JOYRIDER)
								OR bDebugLaunchJoyrider
								#ENDIF
									//NET_PRINT("						- CAN_I_LAUNCH_JOYRIDER()")						NET_NL()
								
									IF NOT IS_PLAYER_AT_DOCKS_JR()
									#IF IS_DEBUG_BUILD
									OR IS_BIT_SET(ib_DebugMenuLaunchingBitSet,DBM_START_JOYRIDER)
									OR bDebugLaunchJoyrider
									#ENDIF
										//NET_PRINT("						- IS_PLAYER_AT_DOCKS_JR()")						NET_NL()
										
										IF IS_PLAYER_CLOSE_TO_VEHICLE_NODE(GET_PLAYER_COORDS(PLAYER_ID()), <<40, 40, 40>>)
										AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 10
										#IF IS_DEBUG_BUILD
										OR IS_BIT_SET(ib_DebugMenuLaunchingBitSet,DBM_START_JOYRIDER)
										OR bDebugLaunchJoyrider
										#ENDIF
											//NET_PRINT("						- IS_PLAYER_CLOSE_TO_VEHICLE_NODE()")			NET_NL()
											
											NET_PRINT("*RLB - Joyrider: JOYRIDERWAITSTATE_OTHER_CHECKS - passed checks")	NET_NL()
																					
											#IF IS_DEBUG_BUILD
												CLEAR_BIT(ib_DebugMenuLaunchingBitSet,DBM_START_JOYRIDER)
												bDebugLaunchJoyrider = FALSE
											#ENDIF
											
											// Go to launching state.
											SET_JOYRIDER_LAUNCH_STATE(structData, eJOYRIDERLAUNCHINGSTATE_LAUNCHING)
										
										// Reset timer.
										ELSE
											bResetSpawnTimer = TRUE
										ENDIF
										
									// If at the docks, reset timer.
									ELSE
										bResetSpawnTimer = TRUE
									ENDIF
								// If I am not allowed to launch the Joyrider, reset timer.
								ELSE
									bResetSpawnTimer = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					// If I should reset timer, do it.
					IF bResetSpawnTimer
						IF HAS_NET_TIMER_STARTED(structData.sJoyriderSpawnTimer)
							structData.iAttemptsCounter++
							IF structData.iAttemptsCounter < 12
								RESET_JOYRIDER_SPAWN_TIMER(structData)
							ELSE
								CLEANUP_JOYRIDER(structData)
								IF GlobalServerBD_BlockB.sJoyriderClockData.bSessionBeenRunningOneInGameDay
									SET_JOYRIDER_LAUNCH_WAIT_STATE(structData, JOYRIDERWAITSTATE_ONE_DAY_SINCE_LAST_SPAWN)
								ELSE
									SET_JOYRIDER_LAUNCH_WAIT_STATE(structData, JOYRIDERWAITSTATE_ONE_DAY_SESSION)
								ENDIF
								SET_JOYRIDER_LAUNCH_STATE(structData, eJOYRIDERLAUNCHINGSTATE_INIT)
								NET_PRINT("*RLB - Joyrider: RESET - C")NET_NL()
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
			ENDSWITCH
			
		BREAK
		
		CASE eJOYRIDERLAUNCHINGSTATE_LAUNCHING
			
			// If the script is running on my machine, go to the running state.
			IF DOES_SCRIPT_EXIST("AM_JOYRIDER")
				IF NETWORK_IS_SCRIPT_ACTIVE("AM_JOYRIDER", -1, TRUE)		//GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_JOYRIDER")) >= 1
					
					NET_PRINT("*RLB - Joyrider: eJOYRIDERLAUNCHINGSTATE_LAUNCHING - Joyrider script is now running.")NET_NL()
					SET_JOYRIDER_LAUNCH_STATE(structData, eJOYRIDERLAUNCHINGSTATE_RUNNING)
					
				// If I am in free mode and I am not running the script, try to launch it.
				ELSE
					
					//NET_PRINT("*RLB - Joyrider: eJOYRIDERLAUNCHINGSTATE_LAUNCHING - Joyrider script is not running yet.")NET_NL()
					
					//NET_PRINT("*RLB - Joyrider: eJOYRIDERLAUNCHINGSTATE_LAUNCHING - in freemode.")NET_NL()
					IF FREEMODE_LAUNCH_JOYRIDER_SCRIPT(structData)
						NET_PRINT("*RLB - Joyrider: eJOYRIDERLAUNCHINGSTATE_LAUNCHING - FREEMODE_LAUNCH_JOYRIDER_SCRIPT = TRUE.")NET_NL()
						SET_JOYRIDER_LAUNCH_STATE(structData, eJOYRIDERLAUNCHINGSTATE_RUNNING)
						#IF IS_DEBUG_BUILD
							CLEAR_BIT(ib_DebugMenuLaunchingBitSet, DBM_START_JOYRIDER)
							bDebugLaunchJoyrider = FALSE
						#ENDIF
					ELSE
						NET_PRINT("*RLB - Joyrider: eJOYRIDERLAUNCHINGSTATE_LAUNCHING - FREEMODE_LAUNCH_JOYRIDER_SCRIPT = FALSE.")NET_NL()
					ENDIF
					
				ENDIF
			ENDIF
			
		BREAK
		
		CASE eJOYRIDERLAUNCHINGSTATE_RUNNING
			
			// If I am still allowed to run the script.
			IF CAN_RUN_JOYRIDER_FREEMODE_CHECKS(TRUE)
			
				// If the script has stopped running, reset.
				IF DOES_SCRIPT_EXIST("AM_JOYRIDER")
					IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_JOYRIDER")
						NET_PRINT("*RLB - Joyrider: eJOYRIDERLAUNCHINGSTATE_RUNNING - Joyrider no longer running on any machine.")NET_NL()
						bCleanup = TRUE
					ENDIF
				ENDIF
				
			// If I am not allowed to run the script, immediately stop running it.
			ELSE
				NET_PRINT("*RLB - Joyrider: eJOYRIDERLAUNCHINGSTATE_RUNNING - CAN_RUN_JOYRIDER_FREEMODE_CHECKS() = FALSE, cleaning up and exiting Joyrider script.")NET_NL()
				bCleanup = TRUE
			ENDIF
			
			// If I am to stop running the script, cleanup and go to the init stage.
			IF bCleanup
				CLEANUP_JOYRIDER(structData)
				IF GlobalServerBD_BlockB.sJoyriderClockData.bSessionBeenRunningOneInGameDay
					SET_JOYRIDER_LAUNCH_WAIT_STATE(structData, JOYRIDERWAITSTATE_ONE_DAY_SINCE_LAST_SPAWN)
				ELSE
					SET_JOYRIDER_LAUNCH_WAIT_STATE(structData, JOYRIDERWAITSTATE_ONE_DAY_SESSION)
				ENDIF
				SET_JOYRIDER_LAUNCH_STATE(structData, eJOYRIDERLAUNCHINGSTATE_INIT)
			ENDIF
		
		BREAK
		
	ENDSWITCH
	
ENDPROC
