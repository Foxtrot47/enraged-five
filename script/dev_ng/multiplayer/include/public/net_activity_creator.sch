//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_activity_creator.sch																	//
// Description: functionality for creating data driven acitivties											//
// Written by:  Philip O'Duffy																				//
// Date: 2016-06-01 (ISO 8601) 																				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    
USING "commands_object.sch"
USING "net_realty_details.sch"
USING "net_activity_creator_components.sch"
USING "net_property_sections_armory_truck.sch"
USING "freemode_header.sch"


//FUNC BOOL CREATE_ACTIVITY_PROPS(ACTIVITY_MAIN &ActivityMain)
//	INT l
//	REPEAT iMaxSyncSceneEntityInSequence l
//		REQUEST_ANIM_DICT(ActivityMain.animState[0].activityAnimSequence[l].activitySyncSceneEntity[l].sAnimDict)
//		IF HAS_ANIM_DICT_LOADED(ActivityMain.animState[0].activityAnimSequence[l].activitySyncSceneEntity[l].sAnimDict)
//			IF IS_MODEL_IN_CDIMAGE(ActivityMain.objectModelName[l])
//				
//				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ActivityMain.niSceneObjects[l])
//					IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
//						RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
//						IF CAN_REGISTER_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY))
//						
//							VECTOR tempPosition 
//							tempPosition = GET_ANIM_INITIAL_OFFSET_POSITION(ActivityMain.animState[0].activityAnimSequence[l].activitySyncSceneEntity[l].sAnimDict, ActivityMain.animState[0].activityAnimSequence[l].activitySyncSceneEntity[l].sAnimClip, ActivityMain.animState[0].activityAnimSequence[l].scenePosition, ActivityMain.animState[0].activityAnimSequence[l].sceneOrientation)
//							
//							VECTOR tempRotation 
//							tempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(ActivityMain.animState[0].activityAnimSequence[l].activitySyncSceneEntity[l].sAnimDict, ActivityMain.animState[0].activityAnimSequence[l].activitySyncSceneEntity[l].sAnimClip, ActivityMain.animState[0].activityAnimSequence[l].scenePosition, ActivityMain.animState[0].activityAnimSequence[l].sceneOrientation)
//
//						
//							OBJECT_INDEX tempObj = CREATE_OBJECT_NO_OFFSET(ActivityMain.objectModelName[l], tempPosition)
//							SET_ENTITY_ROTATION(tempObj, tempRotation)
//							SET_ENTITY_INVINCIBLE(tempObj, TRUE)
//							ActivityMain.niSceneObjects[l][0] = OBJ_TO_NET(tempObj)
//							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_PROPS: created prop [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]))
//							NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(ActivityMain.niSceneObjects[l]), TRUE)
//						ELSE
//							RETURN FALSE
//						ENDIF
//					ELSE
//						RETURN FALSE
//					ENDIF
//				ENDIF
//			ELSE
////				CWARNINGLN(DEBUG_NET_ACTIVITY_CREATOR, "HAVE_ALL_PROPS_LOADED: widgetData.sModelNames[", l, "] not a valid CDImage  = ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]))
//			ENDIF	
//		ENDIF
//	ENDREPEAT
//	
//	RETURN TRUE
//ENDFUNC
FUNC BOOL HAVE_ALL_LOCAL_ACTIVITY_PEDS_BEEN_CREATED(ACTIVITY_MAIN &ActivityMain, CREATOR_ACTIVITY_PEDS &activityPeds)
	INT l
	IF ActivityMain.bIsLocalScene = TRUE
		REPEAT iMaxSyncScenePedInSequence l
		
			IF IS_MODEL_IN_CDIMAGE(ActivityMain.pedData[l].mnPedModel)
				IF NOT DOES_ENTITY_EXIST(activityPeds.piScenePeds[l])
					//CDEBUG2LN(DEBUG_NET_ACTIVITY_CREATOR, "HAVE_ALL_ACTIVITY_PROPS_BEEN_CREATED: prop [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.pedData[l].mnPedModel), ", not yet created for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
					RETURN FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_ALL_LOCAL_STAND_ALONE_PROPS_BEEN_CREATED(STAND_ALONE_PROP_STRUCT &standAloneProps[iMaxStandAloneProps])
	INT l
	
	REPEAT iMaxStandAloneProps l
	
//		IF IS_MODEL_IN_CDIMAGE(standAloneProps[l].propName)
		IF standAloneProps[l].propName != DUMMY_MODEL_FOR_SCRIPT
		AND NOT standAloneProps[l].bHide
			IF NOT DOES_ENTITY_EXIST(standAloneProps[l].eProp)
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "HAVE_ALL_LOCAL_STAND_ALONE_PROPS_BEEN_CREATED: prop [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(standAloneProps[l].propName))
				RETURN FALSE
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "HAVE_ALL_LOCAL_STAND_ALONE_PROPS_BEEN_CREATED: [", l, "] prop is null ")
		ENDIF
	ENDREPEAT
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "HAVE_ALL_LOCAL_STAND_ALONE_PROPS_BEEN_CREATED: TRUE ")
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_ALL_LOCAL_ACTIVITY_PROPS_BEEN_CREATED(ACTIVITY_MAIN &ActivityMain, CREATOR_ACTIVITY_PROPS &activityLocalProps)
	INT l
	INT m
	IF ActivityMain.bIsLocalScene = TRUE
		REPEAT iMaxSyncSceneEntityInSequence l
			REPEAT iMaxNumberOfSwappableEntities m
				IF IS_MODEL_IN_CDIMAGE(ActivityMain.objectModelName[l])
					IF NOT DOES_ENTITY_EXIST(activityLocalProps.oiSceneObjects[l])
						//CDEBUG2LN(DEBUG_NET_ACTIVITY_CREATOR, "HAVE_ALL_ACTIVITY_PROPS_BEEN_CREATED: prop [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]), ", not yet created for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
						RETURN FALSE
					ENDIF
				ELSE
	//				ActivityMain.objectModelName[l][m]
//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "HAVE_ALL_ACTIVITY_PROPS_BEEN_CREATED: IS_MODEL_IN_CDIMAGE = FALSE, prop [", l, "][",m,"] for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_ALL_ACTIVITY_PEDS_BEEN_CREATED(ACTIVITY_MAIN &ActivityMain, SERVER_CREATOR_ACTIVITY_PEDS &activityPeds)
	INT l
	
	REPEAT iMaxSyncScenePedInSequence l
		IF ActivityMain.bIsLocalScene = FALSE
			IF IS_MODEL_IN_CDIMAGE(ActivityMain.pedData[l].mnPedModel)
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activityPeds.niScenePeds[l])
					//CDEBUG2LN(DEBUG_NET_ACTIVITY_CREATOR, "HAVE_ALL_ACTIVITY_PROPS_BEEN_CREATED: prop [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.pedData[l].mnPedModel), ", not yet created for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_ALL_ACTIVITY_PEDS_BEEN_CREATED_FOR_CREATOR_UI(ACTIVITY_MAIN &ActivityMain)
	INT l
	
	REPEAT iMaxSyncScenePedInSequence l
		IF ActivityMain.bIsLocalScene = FALSE
			IF IS_MODEL_IN_CDIMAGE(ActivityMain.pedData[l].mnPedModel)
				IF NOT DOES_ENTITY_EXIST(ActivityMain.piScenePeds[l])
//					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(PED_TO_NET(ActivityMain.piScenePeds[l]))
						//CDEBUG2LN(DEBUG_NET_ACTIVITY_CREATOR, "HAVE_ALL_ACTIVITY_PROPS_BEEN_CREATED: prop [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.pedData[l].mnPedModel), ", not yet created for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
						RETURN FALSE
//					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC


FUNC BOOL HAVE_ALL_ACTIVITY_PROPS_BEEN_CREATED(ACTIVITY_MAIN &ActivityMain, SERVER_CREATOR_ACTIVITY_PROPS &activityProps)
	INT l
//	INT m
	IF ActivityMain.bIsLocalScene = FALSE
		REPEAT iMaxSyncSceneEntityInSequence l
//			REPEAT iMaxNumberOfSwappableEntities m
				IF IS_MODEL_IN_CDIMAGE(ActivityMain.objectModelName[l])
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activityProps.niSceneObjects[l])
						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "HAVE_ALL_ACTIVITY_PROPS_BEEN_CREATED: prop [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]), ", not yet created for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
						RETURN FALSE
					ENDIF
				ELSE
	//				ActivityMain.objectModelName[l][m]
//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "HAVE_ALL_ACTIVITY_PROPS_BEEN_CREATED: IS_MODEL_IN_CDIMAGE = FALSE, prop [", l, "][",m,"] for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
				ENDIF
//			ENDREPEAT
		ENDREPEAT
	ENDIF
	RETURN TRUE
ENDFUNC



FUNC BOOL HAS_PED_ANIM_LOADED(PED_DATA &pedData)
	IF NOT IS_STRING_NULL_OR_EMPTY(pedData.sAnimDict)
		REQUEST_ANIM_DICT(pedData.sAnimDict)
		IF HAS_ANIM_DICT_LOADED(pedData.sAnimDict)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		CWARNINGLN(DEBUG_NET_ACTIVITY_CREATOR, "HAS_PED_ANIM_LOADED: sAnimDict = NULL")
		RETURN TRUE
	ENDIF 
ENDFUNC

FUNC BOOL HAS_ANIM_LOADED(STRING sAnimDict)
	IF NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
		REQUEST_ANIM_DICT(sAnimDict)
		IF HAS_ANIM_DICT_LOADED(sAnimDict)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		CWARNINGLN(DEBUG_NET_ACTIVITY_CREATOR, "HAS_PED_ANIM_LOADED: sAnimDict = NULL")
		RETURN TRUE
	ENDIF 
ENDFUNC


FUNC STRING GET_PED_COMPONENT_NAME(PED_COMPONENT pedComponent)
	SWITCH pedComponent
		CASE   PED_COMP_HEAD 	RETURN "PED_COMP_HEAD"
		CASE   PED_COMP_BERD 	RETURN "PED_COMP_BERD"
		CASE   PED_COMP_HAIR 	RETURN "PED_COMP_HAIR"
		CASE   PED_COMP_TORSO 	RETURN "PED_COMP_TORSO"
		CASE   PED_COMP_LEG 	RETURN "PED_COMP_LEG"
		CASE   PED_COMP_HAND 	RETURN "PED_COMP_HAND"
		CASE   PED_COMP_FEET 	RETURN "PED_COMP_FEET"
		CASE   PED_COMP_TEETH 	RETURN "PED_COMP_TEETH"
		CASE   PED_COMP_SPECIAL RETURN "PED_COMP_ACCS"
		CASE   PED_COMP_SPECIAL2 RETURN "PED_COMP_TASK"
		CASE   PED_COMP_DECL     RETURN "PED_COMP_DECL"
		CASE   PED_COMP_JBIB  	RETURN "PED_COMP_JBIB"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC VECTOR GET_BOX_POSITION_FROM_SCENE_POSITION(VECTOR vScenePos)
	// Research table near whiteboard
	IF GET_DISTANCE_BETWEEN_COORDS(vScenePos, <<891.96808, -3202.10571, -98.40501>>, FALSE) < 0.5
		RETURN <<893.16779, -3201.64551, -99.19627>>
	ENDIF
	
	// Whiteboard by research table
	IF GET_DISTANCE_BETWEEN_COORDS(vScenePos, <<889.37683, -3202.70703, -97.33437>>, FALSE) < 0.5
		RETURN <<890.06171, -3201.74170, -99.19627>>
	ENDIF
	
	// Out back near machine
	IF GET_DISTANCE_BETWEEN_COORDS(vScenePos, <<884.01233, -3207.51196, -99.19627>>, FALSE) < 0.5
		RETURN <<883.68506, -3207.57837, -99.19627>>
	ENDIF
	
	// Security guard by main entrance
	IF GET_DISTANCE_BETWEEN_COORDS(vScenePos, <<895.47986, -3241.60571, -99.16917>>, FALSE) < 0.5
		RETURN vScenePos - <<0.0,0.0,1.0>>
	ENDIF
	
	// Security guard by supply gate
	IF GET_DISTANCE_BETWEEN_COORDS(vScenePos, <<916.06946, -3236.08545, -99.29433>>, FALSE) < 0.5
		RETURN vScenePos - <<0.0,0.0,1.0>>
	ENDIF
	
	// Computer 1 out back
	IF GET_DISTANCE_BETWEEN_COORDS(vScenePos, <<891.99292, -3212.16040, -99.20737>>, FALSE) < 0.5
		RETURN <<892.18530, -3211.94116, -99.20605>>
	ENDIF
	
	// Computer 2 out back
	IF GET_DISTANCE_BETWEEN_COORDS(vScenePos, <<888.23413, -3213.90210, -98.39187>>, FALSE) < 0.5
		RETURN <<888.78003, -3212.88623, -99.19757>>
	ENDIF
	
	// Research table chair south
	IF GET_DISTANCE_BETWEEN_COORDS(vScenePos, <<892.00604, -3203.12476, -98.40501>>, FALSE) < 0.5
		RETURN <<891.67102, -3203.15918, -99.19627>>
	ENDIF
	
	// Research table chair north
	IF GET_DISTANCE_BETWEEN_COORDS(vScenePos, <<891.91650, -3201.09424, -98.40501>>, FALSE) < 0.5
		RETURN <<892.44336, -3200.83618, -99.19627>>
	ENDIF
	
	RETURN vScenePos + <<0.0, 0.0, -1.0>>
ENDFUNC

FUNC BOOL CREATE_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK(ACTIVITY_MAIN &ActivityMain, CREATOR_ACTIVITY_PEDS &activityPeds)
	
	GET_SEQ_ANIM_DATA(ActivityMain.eActivity, 0, 0, ActivityMain.activityAnimSequence)
	
	INT i
	REPEAT iMaxSyncScenePedInSequence i 
		IF IS_MODEL_IN_CDIMAGE(ActivityMain.pedData[i].mnPedModel)
			IF REQUEST_LOAD_MODEL(ActivityMain.pedData[i].mnPedModel)
			AND REQUEST_LOAD_MODEL(prop_box_wood06a)		// prop_barrel_02b
			AND HAS_ANIM_LOADED(ActivityMain.activityAnimSequence.activitySyncScenePed[i].sAnimDict)
				IF NOT DOES_ENTITY_EXIST(activityPeds.piScenePeds[i])
							
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: created ped [", i, "], mnPedModel = ", ActivityMain.pedData[i].mnPedModel, ", activity = ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity), ", vPos = ", ActivityMain.pedData[i].vPos)
					
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: created prop [", i, "] = sAnimDict = ", ActivityMain.activityAnimSequence.activitySyncScenePed[i].sAnimDict, ", sAnimClip: ", ActivityMain.activityAnimSequence.activitySyncScenePed[i].sAnimClip)
					
					
					
					VECTOR tempPosition 
					tempPosition = GET_ANIM_INITIAL_OFFSET_POSITION(ActivityMain.activityAnimSequence.activitySyncScenePed[i].sAnimDict, ActivityMain.activityAnimSequence.activitySyncScenePed[i].sAnimClip, ActivityMain.activityAnimSequence.scenePosition, ActivityMain.activityAnimSequence.sceneOrientation)
					
					VECTOR tempRotation 
					tempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(ActivityMain.activityAnimSequence.activitySyncScenePed[i].sAnimDict, ActivityMain.activityAnimSequence.activitySyncScenePed[i].sAnimClip, ActivityMain.activityAnimSequence.scenePosition, ActivityMain.activityAnimSequence.sceneOrientation)
					ActivityMain.pedData[i].vPos		= tempPosition				
					ActivityMain.pedData[i].vRot.Z    	= tempRotation.Z
					
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: created ped [", i, "], mnPedModel = ", ActivityMain.pedData[i].mnPedModel, ", activity = ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity), ", vPos = ", ActivityMain.pedData[i].vPos)

								
					activityPeds.piScenePeds[i] = CREATE_PED(PEDTYPE_MISSION, ActivityMain.pedData[i].mnPedModel, ActivityMain.pedData[i].vPos, ActivityMain.pedData[i].vRot.Z, FALSE)
					ActivityMain.piScenePeds[i] = activityPeds.piScenePeds[i]
					INT j
					REPEAT NUM_PED_COMPONENTS j
						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: [", j, "] ", GET_PED_COMPONENT_NAME(INT_TO_ENUM(PED_COMPONENT,j)), ", NewDrawableNumber = ", ActivityMain.pedData[i].pcsComponents[j].NewDrawableNumber, ", NewTextureNumber = ", ActivityMain.pedData[i].pcsComponents[j].NewTextureNumber, ", NewPaletteNumber = ", ActivityMain.pedData[i].pcsComponents[j].NewPaletteNumber)
						SET_PED_COMPONENT_VARIATION(activityPeds.piScenePeds[i], INT_TO_ENUM(PED_COMPONENT,j), ActivityMain.pedData[i].pcsComponents[j].NewDrawableNumber, ActivityMain.pedData[i].pcsComponents[j].NewTextureNumber, ActivityMain.pedData[i].pcsComponents[j].NewPaletteNumber) 
					ENDREPEAT
					
					IF ActivityMain.pedData[i].weaponType != WEAPONTYPE_UNARMED
						GIVE_WEAPON_TO_PED(activityPeds.piScenePeds[i], ActivityMain.pedData[i].weaponType, 1, TRUE)
					ENDIF
					
					INT k
					REPEAT NUM_PED_PROPS k
						IF ActivityMain.pedData[i].ppsProps[k].iPropIndex != -1
							SET_PED_PROP_INDEX(activityPeds.piScenePeds[i], INT_TO_ENUM(PED_PROP_POSITION, k), ActivityMain.pedData[i].ppsProps[k].iPropIndex, ActivityMain.pedData[i].ppsProps[k].iTexIndex)
						ENDIF
					ENDREPEAT
					
					SET_MODEL_AS_NO_LONGER_NEEDED(ActivityMain.pedData[i].mnPedModel)
					SET_ENTITY_CAN_BE_DAMAGED(activityPeds.piScenePeds[i], FALSE)
					SET_PED_AS_ENEMY(activityPeds.piScenePeds[i], FALSE)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(activityPeds.piScenePeds[i], TRUE)
					SET_PED_RESET_FLAG(activityPeds.piScenePeds[i],PRF_DisablePotentialBlastReactions, TRUE)
					SET_PED_CONFIG_FLAG(activityPeds.piScenePeds[i],PCF_UseKinematicModeWhenStationary,TRUE)
					SET_PED_CONFIG_FLAG(activityPeds.piScenePeds[i],PCF_DontActivateRagdollFromExplosions,TRUE)
					SET_PED_CONFIG_FLAG(activityPeds.piScenePeds[i],PCF_DontActivateRagdollFromVehicleImpact,TRUE)

					
					SET_PED_CAN_EVASIVE_DIVE(activityPeds.piScenePeds[i], FALSE)
					SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(activityPeds.piScenePeds[i], TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(activityPeds.piScenePeds[i], FALSE)
					SET_PED_CONFIG_FLAG(activityPeds.piScenePeds[i],PCF_DisableExplosionReactions,TRUE)
					
					
					IF NOT IS_STRING_NULL_OR_EMPTY(ActivityMain.pedData[i].sAnimDict)
						TASK_PLAY_ANIM(activityPeds.piScenePeds[i], ActivityMain.pedData[i].sAnimDict, ActivityMain.pedData[i].sAnimClip, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(activityPeds.piScenePeds[i], TRUE)
						SET_PED_KEEP_TASK(activityPeds.piScenePeds[i], TRUE)
					ENDIF
					
					
					IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
						// Create collision objects for ped
						IF NOT DOES_ENTITY_EXIST(activityPeds.objScenePedCollisionObj[i])
						
							PRINTLN("CREATE_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK - ActivityMain.activityAnimSequence.activitySyncSceneEntity[i].sAnimDict = ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[i].sAnimDict)
							PRINTLN("CREATE_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK - ActivityMain.activityAnimSequence.activitySyncSceneEntity[i].sAnimClip = ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[i].sAnimClip)
							PRINTLN("CREATE_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK - ActivityMain.activityAnimSequence.scenePosition = ", ActivityMain.activityAnimSequence.scenePosition)
							PRINTLN("CREATE_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK - ActivityMain.activityAnimSequence.sceneOrientation = ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[i].sAnimDict)
						
							VECTOR vTemp = GET_BOX_POSITION_FROM_SCENE_POSITION(ActivityMain.activityAnimSequence.scenePosition)
							vTemp = vTemp - <<0.0, 0.0, -1.0>>
							activityPeds.objScenePedCollisionObj[i] = CREATE_OBJECT(prop_box_wood06a, vTemp, FALSE, FALSE)
							
							SET_ENTITY_COLLISION(activityPeds.objScenePedCollisionObj[i], TRUE)
							SET_ENTITY_VISIBLE(activityPeds.objScenePedCollisionObj[i], FALSE)
							FREEZE_ENTITY_POSITION(activityPeds.objScenePedCollisionObj[i], TRUE)
							SET_ENTITY_INVINCIBLE(activityPeds.objScenePedCollisionObj[i], TRUE)
							
							SET_MODEL_AS_NO_LONGER_NEEDED(prop_box_wood06a)
							#IF IS_DEBUG_BUILD
							VECTOR vCoords = GET_ENTITY_COORDS(activityPeds.objScenePedCollisionObj[i])
							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: collision object has been made for ped = ", i, " at coords ", vCoords)
							#ENDIF
						ENDIF
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: ped already exists for i = ", i)
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: anims and/or ped model not loaded yet")
				RETURN FALSE
			ENDIF
		ELSE
//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: anims and/or ped model not loaded yet")
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK(ACTIVITY_MAIN &ActivityMain, SERVER_CREATOR_ACTIVITY_PEDS &activityPeds)
	INT i
	REPEAT iMaxSyncScenePedInSequence i 
		IF IS_MODEL_IN_CDIMAGE(ActivityMain.pedData[i].mnPedModel)
			REQUEST_MODEL(ActivityMain.pedData[i].mnPedModel)
			IF HAS_MODEL_LOADED(ActivityMain.pedData[i].mnPedModel)
			AND HAS_PED_ANIM_LOADED(ActivityMain.pedData[i])
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activityPeds.niScenePeds[i])
					IF CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_PEDS()+ 1,false,true)
						RESERVE_LOCAL_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
						
						IF CAN_REGISTER_MISSION_PEDS(1)
							
							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: created network ped [", i, "], mnPedModel = ", ActivityMain.pedData[i].mnPedModel)
										

							PED_INDEX tempPed
							tempPed = CREATE_PED(PEDTYPE_MISSION, ActivityMain.pedData[i].mnPedModel, ActivityMain.pedData[i].vPos, ActivityMain.pedData[i].vRot.Z)
							INT j
							REPEAT NUM_PED_COMPONENTS j
								SET_PED_COMPONENT_VARIATION(tempPed, INT_TO_ENUM(PED_COMPONENT,j), ActivityMain.pedData[i].pcsComponents[j].NewDrawableNumber, ActivityMain.pedData[i].pcsComponents[j].NewTextureNumber, ActivityMain.pedData[i].pcsComponents[j].NewPaletteNumber) 
							ENDREPEAT
							
							IF ActivityMain.pedData[i].weaponType != WEAPONTYPE_UNARMED
								GIVE_WEAPON_TO_PED(tempPed, ActivityMain.pedData[i].weaponType, 1, TRUE)
							ENDIF

							
							SET_MODEL_AS_NO_LONGER_NEEDED(ActivityMain.pedData[i].mnPedModel)
							SET_ENTITY_CAN_BE_DAMAGED(tempPed, FALSE)
							SET_PED_AS_ENEMY(tempPed, FALSE)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
							SET_PED_RESET_FLAG(tempPed,PRF_DisablePotentialBlastReactions, TRUE)
							SET_PED_CONFIG_FLAG(tempPed,PCF_UseKinematicModeWhenStationary,TRUE)
							SET_PED_CONFIG_FLAG(tempPed,PCF_DontActivateRagdollFromExplosions,TRUE)
							NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(tempPed, TRUE)

							
							SET_PED_CAN_EVASIVE_DIVE(tempPed, FALSE)
							SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(tempPed, TRUE)
							SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(tempPed, FALSE)
							SET_PED_CONFIG_FLAG(tempPed,PCF_DisableExplosionReactions,TRUE)
							activityPeds.niScenePeds[i] = PED_TO_NET(tempPed)
							
//							IF NOT IS_STRING_NULL_OR_EMPTY(ActivityMain.pedData[i].sAnimDict)
//								TASK_PLAY_ANIM(NET_TO_PED(activityPeds.niScenePeds[i]), ActivityMain.pedData[i].sAnimDict, ActivityMain.pedData[i].sAnimClip, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
//								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(activityPeds.niScenePeds[i]), TRUE)
//								SET_PED_KEEP_TASK(NET_TO_PED(activityPeds.niScenePeds[i]), TRUE)
//							ENDIF
						ELSE
							CWARNINGLN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: CAN_REGISTER_MISSION_PEDS = FALSE")
							RETURN FALSE
							
						ENDIF
					ELSE
						CWARNINGLN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT = FALSE")
						RETURN FALSE
					ENDIF
					
					
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF NOT HAS_MODEL_LOADED(ActivityMain.pedData[i].mnPedModel)
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: mnPedModel not loaded yet: ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.pedData[i].mnPedModel))
				ENDIF
				
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CREATOR_UI(ACTIVITY_MAIN &ActivityMain)
	INT i
	REPEAT iMaxSyncScenePedInSequence i 
		IF IS_MODEL_IN_CDIMAGE(ActivityMain.pedData[i].mnPedModel)
			REQUEST_MODEL(ActivityMain.pedData[i].mnPedModel)
			IF HAS_MODEL_LOADED(ActivityMain.pedData[i].mnPedModel)
			AND HAS_PED_ANIM_LOADED(ActivityMain.pedData[i])
				IF NOT DOES_ENTITY_EXIST(ActivityMain.piScenePeds[i])
					
					IF CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_PEDS()+ 1,false,true)
						RESERVE_LOCAL_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
						IF CAN_REGISTER_MISSION_PEDS(1)
							
							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: created network ped [", i, "], mnPedModel = ", ActivityMain.pedData[i].mnPedModel)
										
							ActivityMain.piScenePeds[i] = CREATE_PED(PEDTYPE_MISSION, ActivityMain.pedData[i].mnPedModel, ActivityMain.pedData[i].vPos, ActivityMain.pedData[i].vRot.Z)
							INT j
							REPEAT NUM_PED_COMPONENTS j
								SET_PED_COMPONENT_VARIATION(ActivityMain.piScenePeds[i], INT_TO_ENUM(PED_COMPONENT,j), ActivityMain.pedData[i].pcsComponents[j].NewDrawableNumber, ActivityMain.pedData[i].pcsComponents[j].NewTextureNumber, ActivityMain.pedData[i].pcsComponents[j].NewPaletteNumber) 
							ENDREPEAT
							
							SET_MODEL_AS_NO_LONGER_NEEDED(ActivityMain.pedData[i].mnPedModel)
							SET_ENTITY_CAN_BE_DAMAGED(ActivityMain.piScenePeds[i], FALSE)
							SET_PED_AS_ENEMY(ActivityMain.piScenePeds[i], FALSE)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ActivityMain.piScenePeds[i], TRUE)
							SET_PED_RESET_FLAG(ActivityMain.piScenePeds[i],PRF_DisablePotentialBlastReactions, TRUE)
							SET_PED_CONFIG_FLAG(ActivityMain.piScenePeds[i],PCF_UseKinematicModeWhenStationary,TRUE)
							SET_PED_CONFIG_FLAG(ActivityMain.piScenePeds[i],PCF_DontActivateRagdollFromExplosions,TRUE)
							NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(ActivityMain.piScenePeds[i], TRUE)

							
							SET_PED_CAN_EVASIVE_DIVE(ActivityMain.piScenePeds[i], FALSE)
							SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(ActivityMain.piScenePeds[i], TRUE)
							SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(ActivityMain.piScenePeds[i], FALSE)
							SET_PED_CONFIG_FLAG(ActivityMain.piScenePeds[i],PCF_DisableExplosionReactions,TRUE)

						ELSE
							CWARNINGLN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: CAN_REGISTER_MISSION_PEDS = FALSE")
							RETURN FALSE
							
						ENDIF
					ELSE
						CWARNINGLN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT = FALSE")
						RETURN FALSE
					ENDIF
						
						
					
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: anims and/or ped model not loaded yet")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK(ACTIVITY_MAIN &ActivityMain, CREATOR_ACTIVITY_PROPS &activityLocalProps)
	INT l
//	INT m
	
	IF DOES_ENTITY_EXIST(ActivityMain.piScenePeds[0])
		GET_SEQ_ANIM_DATA(ActivityMain.eActivity, 0, 0, ActivityMain.activityAnimSequence, IS_PED_FEMALE(ActivityMain.piScenePeds[0]))
	ELSE
		GET_SEQ_ANIM_DATA(ActivityMain.eActivity, 0, 0, ActivityMain.activityAnimSequence)
	ENDIF
	
	
	REPEAT iMaxSyncSceneEntityInSequence l
//		REPEAT iMaxNumberOfSwappableEntities m
			IF IS_MODEL_IN_CDIMAGE(ActivityMain.objectModelName[l])
				
				IF NOT IS_STRING_NULL_OR_EMPTY(ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict)
					REQUEST_ANIM_DICT(ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict)
//					IF IS_MODEL_A_VEHICLE(ActivityMain.objectModelName[l])
//						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: REQUEST_VEHICLE_ASSET: ",  GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]))
//						REQUEST_VEHICLE_ASSET(ActivityMain.objectModelName[l])
//					ENDIF 
					IF HAS_ANIM_DICT_LOADED(ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict)
					AND REQUEST_LOAD_MODEL(ActivityMain.objectModelName[l])
//						( 
//							(IS_MODEL_A_VEHICLE(ActivityMain.objectModelName[l]) AND HAS_VEHICLE_ASSET_LOADED(ActivityMain.objectModelName[l]) )
//						OR (IS_MODEL_A_VEHICLE(ActivityMain.objectModelName[l]) = FALSE)  
//						)
						
						IF NOT DOES_ENTITY_EXIST(activityLocalProps.oiSceneObjects[l])
//							IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
//								RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
//								IF CAN_REGISTER_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY))
									
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: created prop [", l, "] = sAnimDict = ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict, ", sAnimClip: ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimClip, ", MODEL_NAME: ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]))
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: created prop [", l, "] = scenePosition = ", ActivityMain.activityAnimSequence.scenePosition, ", sceneOrientation: ",   ActivityMain.activityAnimSequence.sceneOrientation)
									
									VECTOR tempPosition 
									tempPosition = GET_ANIM_INITIAL_OFFSET_POSITION(ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict, ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimClip, ActivityMain.activityAnimSequence.scenePosition, ActivityMain.activityAnimSequence.sceneOrientation)
									
									VECTOR tempRotation 
									tempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict, ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimClip, ActivityMain.activityAnimSequence.scenePosition, ActivityMain.activityAnimSequence.sceneOrientation)
									
									IF IS_MODEL_A_VEHICLE(ActivityMain.objectModelName[l])
										IF NOT HAS_VEHICLE_ASSET_LOADED(ActivityMain.objectModelName[l])
											CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: HAS_VEHICLE_ASSET_LOADED!!: FALSE ",  GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]))
										ELSE
											CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: HAS_VEHICLE_ASSET_LOADED!!: TRUE ",  GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]))
										ENDIF 
									ENDIF
									
									IF NOT IS_MODEL_A_VEHICLE(ActivityMain.objectModelName[l])
										CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: Creating using CREATE_OBJECT_NO_OFFSET")
										activityLocalProps.oiSceneObjects[l] = CREATE_OBJECT_NO_OFFSET(ActivityMain.objectModelName[l], tempPosition, FALSE)
										SET_ENTITY_ROTATION(activityLocalProps.oiSceneObjects[l], tempRotation)
									ELIF IS_MODEL_A_VEHICLE(ActivityMain.objectModelName[l])
										CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: Creating using CREATE_VEHICLE")
										activityLocalProps.oiSceneObjects[l] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(CREATE_VEHICLE(ActivityMain.objectModelName[l], tempPosition, tempRotation.Z, FALSE, FALSE))
									ENDIF
									SET_ENTITY_INVINCIBLE(activityLocalProps.oiSceneObjects[l], TRUE)
									FREEZE_ENTITY_POSITION(activityLocalProps.oiSceneObjects[l], TRUE)
									
									
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: created prop [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]), ", position = ", tempPosition)
//								ELSE
//									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: can't register entity")
//									RETURN FALSE
//								ENDIF
//							ELSE
//								RETURN FALSE
//							ENDIF
						ENDIF
					ELSE
//						IF IS_MODEL_A_VEHICLE(ActivityMain.objectModelName[l]) 
//						AND NOT HAS_VEHICLE_ASSET_LOADED(ActivityMain.objectModelName[l])
						IF NOT REQUEST_LOAD_MODEL(ActivityMain.objectModelName[l])
							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: REQUEST_LOAD_MODEL NOT LOADED: ",  GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]))
							
						ENDIF 
						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: loading: ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict, " for entity [", l, "] for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
						RETURN FALSE
					ENDIF
				ELSE
					IF NOT IS_VECTOR_ZERO(ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].vEntityPos)
						IF NOT DOES_ENTITY_EXIST(activityLocalProps.oiSceneObjects[l])

//							IF CAN_REGISTER_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY))
								
								
								CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: created prop [", l, "] vEntityPosOffset = ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].vEntityPos, ", vEntityHeadingOffset = ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].vEntityHeading)
															
								VECTOR vTempPos = ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].vEntityPos + ActivityMain.activityAnimSequence.scenePosition
								
								VECTOR vTempRot = ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].vEntityHeading + ActivityMain.activityAnimSequence.sceneOrientation
								
								CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: created prop [", l, "] vEntityPos = ", vTempPos, ", vEntityHeading = ", vTempRot)
								
								activityLocalProps.oiSceneObjects[l] = CREATE_OBJECT_NO_OFFSET(ActivityMain.objectModelName[l],  vTempPos, FALSE)
								SET_ENTITY_ROTATION(activityLocalProps.oiSceneObjects[l], vTempRot)
								SET_ENTITY_INVINCIBLE(activityLocalProps.oiSceneObjects[l], TRUE)
								FREEZE_ENTITY_POSITION(activityLocalProps.oiSceneObjects[l], TRUE)
								
								
								CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: created prop [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]))
//							ELSE
//								CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: can't register entity")
//								RETURN FALSE
//							ENDIF

						ENDIF
					ELSE
					ENDIF
					
					CWARNINGLN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: anim is null for entitiy [", l, "] for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
				ENDIF
						
			ELSE
				
//						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: widgetData.sModelNames[", l, "][",m,"] not a valid CDImage ")
			ENDIF	
			
//		ENDREPEAT
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC



FUNC BOOL CREATE_LOCAL_STAND_ALONE_PROPS_FOR_ACTIVITY(STAND_ALONE_PROP_STRUCT &propStruct[iMaxStandAloneProps])
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_STAND_ALONE_PROPS_FOR_ACTIVITY")
	
	INT l
	REPEAT iMaxStandAloneProps l 
		IF propStruct[l].bHide = FALSE
			IF NOT DOES_ENTITY_EXIST(propStruct[l].eProp)
				IF propStruct[l].propName != DUMMY_MODEL_FOR_SCRIPT
					IF REQUEST_LOAD_MODEL(propStruct[l].propName)

						
//						IF CAN_REGISTER_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY))
							
							IF IS_MODEL_A_VEHICLE(propStruct[l].propName)
								IF NOT HAS_VEHICLE_ASSET_LOADED(propStruct[l].propName)
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_STAND_ALONE_PROPS_FOR_ACTIVITY: HAS_VEHICLE_ASSET_LOADED!!: FALSE ",  GET_MODEL_NAME_FOR_DEBUG(propStruct[l].propName))
								ELSE
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_STAND_ALONE_PROPS_FOR_ACTIVITY: HAS_VEHICLE_ASSET_LOADED!!: TRUE ",  GET_MODEL_NAME_FOR_DEBUG(propStruct[l].propName))
								ENDIF 
							ENDIF
							
							IF NOT IS_MODEL_A_VEHICLE(propStruct[l].propName)
								CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_STAND_ALONE_PROPS_FOR_ACTIVITY: creating propStruct[", l,"].propName = ", propStruct[l].propName)
								propStruct[l].eProp = CREATE_OBJECT_NO_OFFSET(propStruct[l].propName, propStruct[l].vPos, FALSE)
								SET_ENTITY_ROTATION(propStruct[l].eProp, propStruct[l].vRot)
							ELIF IS_MODEL_A_VEHICLE(propStruct[l].propName)
								CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_STAND_ALONE_PROPS_FOR_ACTIVITY: Creating using CREATE_VEHICLE")
								propStruct[l].eProp = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(CREATE_VEHICLE(propStruct[l].propName, propStruct[l].vPos, propStruct[l].vRot.Z, FALSE, FALSE))
							ENDIF
							SET_ENTITY_INVINCIBLE(propStruct[l].eProp, TRUE)
							FREEZE_ENTITY_POSITION(propStruct[l].eProp, TRUE)
							
							
							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_STAND_ALONE_PROPS_FOR_ACTIVITY: created prop [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(propStruct[l].propName), ", position = ", propStruct[l].vPos)
//						ELSE
//							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_STAND_ALONE_PROPS_FOR_ACTIVITY: can't register entity")
//							RETURN FALSE
//						ENDIF
						
					ELSE

						IF NOT REQUEST_LOAD_MODEL(propStruct[l].propName)
							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_STAND_ALONE_PROPS_FOR_ACTIVITY: REQUEST_LOAD_MODEL NOT LOADED: ",  GET_MODEL_NAME_FOR_DEBUG(propStruct[l].propName))
						ENDIF 
						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_STAND_ALONE_PROPS_FOR_ACTIVITY: loading prop" )
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
			
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_LOCAL_STAND_ALONE_MODEL_HIDE_PROPS_FOR_ACTIVITY(STAND_ALONE_PROP_STRUCT &propStruct[iMaxStandAloneProps])
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_STAND_ALONE_PROPS_FOR_ACTIVITY")
	
	INT l
	REPEAT iMaxStandAloneProps l 
		IF propStruct[l].bHide = TRUE
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_LOCAL_STAND_ALONE_MODEL_HIDE_PROPS_FOR_ACTIVITY: creating model[", l, "] hide for model ", GET_MODEL_NAME_FOR_DEBUG(propStruct[l].propName))
			CREATE_MODEL_HIDE(propStruct[l].vPos, 1.0, propStruct[l].propName, TRUE)
		ENDIF	
			
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK(ACTIVITY_MAIN &ActivityMain, SERVER_CREATOR_ACTIVITY_PROPS &activityProps)
	INT l
//	INT m
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: GET_SEQ_ANIM_DATA for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
	GET_SEQ_ANIM_DATA(ActivityMain.eActivity, 0, 0, ActivityMain.activityAnimSequence)
	
	REPEAT iMaxSyncSceneEntityInSequence l
//		REPEAT iMaxNumberOfSwappableEntities m
			IF NOT IS_STRING_NULL_OR_EMPTY(ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict)
				REQUEST_ANIM_DICT(ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict)
				IF HAS_ANIM_DICT_LOADED(ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict)

					IF IS_MODEL_IN_CDIMAGE(ActivityMain.objectModelName[l])
						
						IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activityProps.niSceneObjects[l])
							IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
								RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
								
								IF CAN_REGISTER_MISSION_OBJECTS(1)
									IF NETWORK_IS_IN_MP_CUTSCENE() 
										SET_NETWORK_CUTSCENE_ENTITIES(TRUE)	
										CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK, in cutscene therefore setting SET_NETWORK_CUTSCENE_ENTITIES")
									ENDIF
									
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: created prop [", l, "] = sAnimDict = ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict, ", sAnimClip: ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimClip)
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: created prop [", l, "] = scenePosition = ", ActivityMain.activityAnimSequence.scenePosition, ", sceneOrientation: ",   ActivityMain.activityAnimSequence.sceneOrientation)
									
									VECTOR tempPosition 
									tempPosition = GET_ANIM_INITIAL_OFFSET_POSITION(ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict, ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimClip, ActivityMain.activityAnimSequence.scenePosition, ActivityMain.activityAnimSequence.sceneOrientation)
									
									VECTOR tempRotation 
									tempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict, ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimClip, ActivityMain.activityAnimSequence.scenePosition, ActivityMain.activityAnimSequence.sceneOrientation)
									
									
									activityProps.niSceneObjects[l] = OBJ_TO_NET( CREATE_OBJECT_NO_OFFSET(ActivityMain.objectModelName[l], tempPosition, DEFAULT, TRUE))									
									SET_ENTITY_ROTATION(NET_TO_OBJ(activityProps.niSceneObjects[l]), tempRotation)
									SET_ENTITY_INVINCIBLE(NET_TO_OBJ(activityProps.niSceneObjects[l]), TRUE)
									FREEZE_ENTITY_POSITION(NET_TO_OBJ(activityProps.niSceneObjects[l]), TRUE)
									
									SET_ENTITY_MIRROR_REFLECTION_FLAG(NET_TO_OBJ(activityProps.niSceneObjects[l]), FALSE)
									
									IF ActivityMain.bPropVisibilitySwap
										SET_ENTITY_VISIBLE(NET_TO_OBJ(activityProps.niSceneObjects[l]), NOT ActivityMain.bObjectHiddenNotInUse[l])
										CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: Setting prop visibility ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]), " to ", NOT ActivityMain.bObjectHiddenInUse[l])	
									ENDIF
																		
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: created prop [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(ActivityMain.objectModelName[l]), ", position = ", tempPosition)
									NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_OBJ(activityProps.niSceneObjects[l]), TRUE)
									
									IF ActivityMain.activityCheck.iRoomKey != -1
										IF NOT IS_PED_INJURED(PLAYER_PED_ID())
										AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
										AND IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
											CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: setting room for prop [", l, "] = ", ActivityMain.activityCheck.iRoomKey)
											FORCE_ROOM_FOR_ENTITY(NET_TO_OBJ(activityProps.niSceneObjects[l]), GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), ActivityMain.activityCheck.iRoomKey)
										ENDIF
									ENDIF
									
									IF NETWORK_IS_IN_MP_CUTSCENE() 
										SET_NETWORK_CUTSCENE_ENTITIES(FALSE)	
									ENDIF
								ELSE
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: CAN_REGISTER_MISSION_OBJECTS failing for activity:  ",DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity) )
									RETURN FALSE
								ENDIF
							ELSE
								RETURN FALSE
							ENDIF
						ENDIF
					ELSE
						//CDEBUG2LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: widgetData.sModelNames[", l, "] not a valid CDImage ")
					ENDIF	
				ELSE
					//CDEBUG2LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: loading: ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[l].sAnimDict, " for entity [", l, "] for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
					RETURN FALSE
				ENDIF
			ELSE
				//CDEBUG2LN(DEBUG_NET_ACTIVITY_CREATOR, "CREATE_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: anim is null for entitiy [", l, "] for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
			ENDIF
//		ENDREPEAT
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DESTROY_ACTIVITY_PROPS(SERVER_CREATOR_ACTIVITY_PROPS &activityProps)
	INT l
	REPEAT iMaxSyncSceneEntityInSequence l
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activityProps.niSceneObjects[l])
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activityProps.niSceneObjects[l])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "DESTROY_ACTIVITY_PROPS: sModelNames[", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(NET_TO_ENT(activityProps.niSceneObjects[l]))))
				
				DELETE_NET_ID(activityProps.niSceneObjects[l])
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) - 1)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(activityProps.niSceneObjects[l])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "DESTROY_ACTIVITY_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of sModelNames[", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(NET_TO_ENT(activityProps.niSceneObjects[l]))))
				RETURN FALSE
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DESTROY_ACTIVITY_PEDS(SERVER_CREATOR_ACTIVITY_PEDS &activityPeds)
	INT l
	REPEAT iMaxSyncScenePedInSequence l
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activityPeds.niScenePeds[l])
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activityPeds.niScenePeds[l])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "DESTROY_ACTIVITY_PEDS: sModelNames[", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(NET_TO_ENT(activityPeds.niScenePeds[l]))))
				
				DELETE_NET_ID(activityPeds.niScenePeds[l])
				RESERVE_LOCAL_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS(FALSE, RESERVATION_LOCAL_ONLY) - 1)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(activityPeds.niScenePeds[l])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "DESTROY_ACTIVITY_PEDS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of sModelNames[", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(NET_TO_ENT(activityPeds.niScenePeds[l]))))
				RETURN FALSE
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DESTROY_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK(CREATOR_ACTIVITY_PROPS &activityLocalProps)
	INT l

	
	
	
	REPEAT iMaxSyncSceneEntityInSequence l
		IF  DOES_ENTITY_EXIST(activityLocalProps.oiSceneObjects[l])
			
			
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "DESTROY_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " destroying [", l, "] = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityLocalProps.oiSceneObjects[l])))
			
			DELETE_OBJECT(activityLocalProps.oiSceneObjects[l])
				
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC DESTROY_ALL_LOCAL_ACTIVITY_PROPS(CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck[iMaxCreatorActivities])
	
	INT i = 0
//	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		REPEAT iMaxCreatorActivities i
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "DESTROY_ALL_LOCAL_ACTIVITY_PROPS: i = ", i, ", activity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[i].eActivity), ", is local = ", activityCheck[i].bLocalScript)
			IF activityCheck[i].bLocalScript = TRUE
				DESTROY_LOCAL_ACTIVITY_PROPS_FOR_ACTIVITY_CHECK(activityCheck[i].activityLocalProps)
			ELSE
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "DESTROY_ALL_LOCAL_ACTIVITY_PROPS: not destroying ")
			ENDIF
		ENDREPEAT
//	ENDIF
ENDPROC

FUNC BOOL DESTROY_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK(CREATOR_ACTIVITY_PEDS &activityLocalPeds)
	INT l

	
	
	
	REPEAT iMaxSyncScenePedInSequence l
		IF  DOES_ENTITY_EXIST(activityLocalPeds.piScenePeds[l])

			
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "DESTROY_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " destroying [", l, "]")// = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYER_PED(activityLocalPeds.piScenePeds))))
			DELETE_PED(activityLocalPeds.piScenePeds[l])
				
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC DESTROY_ALL_LOCAL_ACTIVITY_PEDS(CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck[iMaxCreatorActivities])
	
	INT i = 0
//	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		REPEAT iMaxCreatorActivities i
			DESTROY_LOCAL_ACTIVITY_PEDS_FOR_ACTIVITY_CHECK(activityCheck[i].activityLocalPeds)
		ENDREPEAT
//	ENDIF
ENDPROC

PROC DESTROY_ALL_NETWORK_ACTIVITY_PROPS(SERVER_CREATOR_ACTIVITY_PROPS &activityProps[iMaxCreatorActivities])
	
	INT i = 0
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		REPEAT iMaxCreatorActivities i
			DESTROY_ACTIVITY_PROPS(activityProps[i])
		ENDREPEAT
	ENDIF
ENDPROC

PROC DESTROY_ALL_NETWORK_ACTIVITY_PEDS(SERVER_CREATOR_ACTIVITY_PEDS &activityPeds[iMaxCreatorActivities])
	
	INT i = 0
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		REPEAT iMaxCreatorActivities i
			DESTROY_ACTIVITY_PEDS(activityPeds[i])
		ENDREPEAT
	ENDIF
ENDPROC


//FUNC BOOL DESTROY_SERVER_ACTIVITY_PROPS(SERVER_CREATOR_ACTIVITY_PROPS &activityProps[iMaxCreatorActivities])
//	INT l, m, n
//	REPEAT iMaxCreatorActivities l
//		REPEAT iMaxSyncSceneEntityInSequence m
//			REPEAT iMaxNumberOfSwappableEntities n
//		
//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(activityProps[l].niSceneObjects[m][n])
//					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(activityProps[l].niSceneObjects[m][n])
//						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "DESTROY_SERVER_ACTIVITY_PROPS: activityProps[", l, "].niSceneObjects[", m,"][", n,"] ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(NET_TO_OBJ( activityProps[l].niSceneObjects[m][n]))))
//						
//						DELETE_NET_ID(activityProps[l].niSceneObjects[m][n])
//						RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) - 1)
//					ELSE
//						TAKE_CONTROL_OF_NET_ID(activityProps[l].niSceneObjects[m][n])
//						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "DESTROY_SERVER_ACTIVITY_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of activityProps[", l, "].niSceneObjects[", m,"][", n,"] = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(NET_TO_OBJ( activityProps[l].niSceneObjects[m][n]))))
//						RETURN FALSE
//					ENDIF
//				ENDIF
//			ENDREPEAT
//		ENDREPEAT
//	ENDREPEAT
//	
//	RETURN TRUE
//ENDFUNC

//PROC INIT_ACTIVITY(ACTIVITY_MAIN &ActivityMain)
//	SWITCH ActivityMain.actStage
//		CASE ACTIVITY_STAGE_INIT
//			MP_PROP_OFFSET_STRUCT mpOffsetBoundBox1
//			mpOffsetBoundBox1.vLoc = ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct.vAngledAreaA 
//			
//			MP_PROP_OFFSET_STRUCT mpBaseInteriorLoc 
//			mpBaseInteriorLoc = GET_BASE_INTERIOR_LOCATION(GET_BASE_PROPERTY_FROM_PROPERTY(ActivityMain.iCurrentProperty))
//			MP_PROP_OFFSET_STRUCT mpInteriorLoc 
//			mpInteriorLoc = GET_BASE_INTERIOR_LOCATION(ActivityMain.iCurrentProperty)
//			GET_RELATIVE_COORDS_FROM_BASE_COORDS(mpBaseInteriorLoc.vLoc, mpInteriorLoc.vLoc, mpOffsetBoundBox1)
//			
//			ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct.vAngledAreaA = mpOffsetBoundBox1.vLoc 
//			
//			MP_PROP_OFFSET_STRUCT mpOffsetBoundBox2
//			mpOffsetBoundBox2.vLoc = ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct.vAngledAreaB
//			GET_RELATIVE_COORDS_FROM_BASE_COORDS(mpBaseInteriorLoc.vLoc, mpInteriorLoc.vLoc, mpOffsetBoundBox2)
//			ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct.vAngledAreaB = mpOffsetBoundBox2.vLoc 
//			
//			SetActStage(ActivityMain, ACTIVITY_STAGE_SHOULD_LAUNCH)
//		BREAK
//	ENDSWITCH
//	
//ENDPROC


//PROC SET_GRABBED_OBJECTS_AS_SCENE_OBJECTS(OBJECT_INDEX &oiSceneObjects[iMaxSyncSceneEntityInSequence][iMaxNumberOfSwappableEntities], ACTIVITY_ANIM_SYNC_SCENE_COMPONENT_ENTITY &ActSyncSceneEntityStruct[iMaxSyncSceneEntityInSequence])
//	INT i
//	REPEAT iMaxSyncSceneEntityInSequence i
//		IF DOES_ENTITY_EXIST(oiSceneObjects[i][0])
//		AND NOT DOES_ENTITY_EXIST(ActSyncSceneEntityStruct[i].EntityIndex)
//			ActSyncSceneEntityStruct[i].EntityIndex = oiSceneObjects[i][0]
//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SET_GRABBED_OBJECTS_AS_SCENE_OBJECTS: i: ", i, ", EntityIndex = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiSceneObjects[i][0])))
//		ENDIF
//	ENDREPEAT
//
//ENDPROC

FUNC BOOL SHOULD_ACTIVITY_UI_SCRIPT_CLEANUP()	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_ADD, KEYBOARD_MODIFIER_CTRL_SHIFT, "Kill ActivityCreatorUI Script") 
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_UI_SCRIPT_CLEANUP: Ctrl Shift + pressed ")
		RETURN TRUE
	ENDIF
	#ENDIF
	//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_UI_SCRIPT_CLEANUP")
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_UI_SCRIPT_CLEANUP: NETWORK_IS_GAME_IN_PROGRESS = TRUE")
		RETURN TRUE
	ENDIF
	
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_UI_SCRIPT_CLEANUP: SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
		RETURN TRUE
	ENDIF



	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_RANGE_OF_ACTIVITY(CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck)
	
	INT iArea
	FLOAT fWidth
	REPEAT iMaxSyncSceneEntryAreas iArea
		IF NOT IS_VECTOR_ZERO(activityCheck.vBoundingBoxA[iArea])
		AND NOT IS_VECTOR_ZERO(activityCheck.vBoundingBoxB[iArea])
			fWidth = 1
			IF activityCheck.fBoundingBoxWidth[0] != 0
				fWidth = activityCheck.fBoundingBoxWidth[0]
			ENDIF
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), activityCheck.vBoundingBoxA[iArea], activityCheck.vBoundingBoxB[iArea], fWidth)
	//			INT iParticipant
	//			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
	//				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
	//					PLAYER_INDEX PlayerId 
	//					PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
	//					IF PlayerID <> PLAYER_ID()
	//						IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), activityCheck.vBoundingBoxA, activityCheck.vBoundingBoxB, 1)
	//							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "IS_PLAYER_IN_RANGE_OF_ACTIVITY: IS_POSITION_OCCUPIED: TRUE, Player: ", GET_PLAYER_NAME(PlayerID), " is also occupying space for ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck.eActivity), ", activityCheck.vBoundingBoxA: ", activityCheck.vBoundingBoxA, ", activityCheck.vBoundingBoxB: ", activityCheck.vBoundingBoxB)
	//							RETURN FALSE
	//						ENDIF
	//					ENDIF
	//				ENDIF
	//			ENDREPEAT
				
				//CDEBUG2LN(DEBUG_NET_ACTIVITY_CREATOR, "IS_PLAYER_IN_RANGE_OF_ACTIVITY: IS_POSITION_OCCUPIED: FALSE, for activity: ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck.eActivity))
				RETURN TRUE
			ELSE
				//CDEBUG2LN(DEBUG_NET_ACTIVITY_CREATOR, "IS_PLAYER_IN_RANGE_OF_ACTIVITY: player is not in angled area ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck.eActivity), ", activityCheck.vBoundingBoxA = ", activityCheck.vBoundingBoxA[iArea], ", activityCheck.vBoundingBoxB = ", activityCheck.vBoundingBoxB[iArea])
//				RETURN FALSE
			ENDIF
		ELSE
			//CDEBUG2LN(DEBUG_NET_ACTIVITY_CREATOR, "IS_PLAYER_IN_RANGE_OF_ACTIVITY: vBoundingBoxA and vBoundingBoxB = 0")
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_OUT_OF_RANGE(CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck)
		FLOAT fWidth = 1
		IF activityCheck.fBoundingBoxWidth[0] != 0
			fWidth = activityCheck.fBoundingBoxWidth[0]
		ENDIF

		IF NOT activityCheck.bLaunchImmediately
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), activityCheck.vBoundingBoxA[0], activityCheck.vBoundingBoxB[0], fWidth)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), activityCheck.vBoundingBoxA[1], activityCheck.vBoundingBoxB[1], fWidth)
//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "IS_PLAYER_OUT_OF_RANGE: FALSE, player is in angled area activityCheck.vBoundingBoxA = ", activityCheck.vBoundingBoxA[0], ", activityCheck.vBoundingBoxB = ", activityCheck.vBoundingBoxB[0], ", activityCheck.bLaunchImmediately = ", activityCheck.bLaunchImmediately)
				RETURN FALSE
			ENDIF
		ELSE
//			CDEBUG1LN(DEBUG_NET_GUN_TURRET, "IS_PLAYER_OUT_OF_RANGE: here ")	
			RETURN FALSE
		ENDIF

	RETURN TRUE
ENDFUNC


FUNC BOOL SHOULD_ACTIVITY_SCRIPT_CLEANUP(ACTIVITY_MAIN &ActivityMain)	
	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD0, KEYBOARD_MODIFIER_CTRL_SHIFT, "Kill ActivityCreatorUI Script") 
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_PROTOTYPE_ACTIVITY_SCRIPT_CLEANUP: Ctrl Shift 0 pressed ")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF HAVE_ARMORY_TRUCK_SECTIONS_UPDATED()
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_PROTOTYPE_ACTIVITY_SCRIPT_CLEANUP: HAVE_ARMORY_TRUCK_SECTIONS_UPDATED: TRUE ")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_OUT_OF_RANGE(ActivityMain.activityCheck)
		IF ActivityMain.actStage = ACTIVITY_STAGE_SHOULD_LAUNCH
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: IS_PLAYER_OUT_OF_RANGE = TRUE, && ActivityMain.actStage = ACTIVITY_STAGE_APPROACH")
			RETURN TRUE
		ELSE
//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: IS_PLAYER_OUT_OF_RANGE = TRUE, && ActivityMain.actStage = ", actStateString(ActivityMain.actStage) )
		ENDIF
	ENDIF

	//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP")
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: NETWORK_IS_GAME_IN_PROGRESS = TRUE")
		RETURN TRUE
	ENDIF
	
//	IF NOT IS_SKYSWOOP_AT_GROUND()
//		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: IS_SKYSWOOP_AT_GROUND = TRUE")
//		RETURN TRUE
//	ENDIF
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
		RETURN TRUE
	ENDIF


	IF IS_PLAYER_DEAD(PLAYER_ID())
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: IS_PLAYER_DEAD = TRUE")
		RETURN TRUE
	ENDIF
	
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: IS_PLAYER_SCTV = TRUE, cleaning up")
		RETURN TRUE
	ENDIF

	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), ActivityMain.activityCheck.vActivityPos) > ActivityMain.activityCheck.fActivityTriggerDistance +0.5
	AND NOT ActivityMain.activityCheck.bLaunchImmediately
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: GET_DISTANCE_BETWEEN_COORDS: ", GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), ActivityMain.activityCheck.vActivityPos),", can't be greater than: ", ActivityMain.activityCheck.fActivityTriggerDistance +0.5, " , cleaning up")
		RETURN TRUE
	ENDIF
	
	IF ActivityMain.activityCheck.bLaunchImmediately
	AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), ActivityMain.activityCheck.vActivityPos) > ActivityMain.activityCheck.fActivityTriggerDistance
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: bLaunchImmediately: GET_DISTANCE_BETWEEN_COORDS: ", GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), ActivityMain.activityCheck.vActivityPos),", can't be greater than ", ActivityMain.activityCheck.fActivityTriggerDistance, " , cleaning up")
		RETURN TRUE
	ENDIF
	
	
	IF IS_SCREEN_FADED_OUT()
	AND NOT g_bMPTVplayerWatchingTV
	AND NOT (ActivityMain.eActivity  = ACT_CREATOR_TURRET_SEAT_LEFT
			OR ActivityMain.eActivity  = ACT_CREATOR_TURRET_SEAT
			OR ActivityMain.eActivity  = ACT_CREATOR_OSPREY_TURRET_SEAT
			OR ActivityMain.eActivity  = ACT_CREATOR_OSPREY_TURRET_SEAT_LEFT
			OR ActivityMain.eActivity  = ACT_CREATOR_IAA_TURRET_SEAT
			OR ActivityMain.eActivity  = ACT_CREATOR_HACKER_TRUCK_SEAT_1
			OR ActivityMain.eActivity  = ACT_CREATOR_HACKER_TRUCK_SEAT_2
			#IF FEATURE_HEIST_ISLAND
			OR ActivityMain.eActivity  = ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_1
			OR ActivityMain.eActivity  = ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_2
			#ENDIF	)
	AND NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
	#IF FEATURE_CASINO_NIGHTCLUB
	AND NOT IS_PLAYER_IN_CASINO_NIGHTCLUB(PLAYER_ID())
	#ENDIF
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: IS_SCREEN_FADED_OUT: TRUE: ")
		RETURN TRUE
	ENDIF
	
	IF NOT ActivityMain.bIsLocalScene
	OR IS_PLAYER_IN_BUNKER(PLAYER_ID())
		IF g_iKillAllActivityCreatorScripts[ActivityMain.activityCheck.iScriptInstanceID] = 1
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: activityCheck.iScriptInstanceID = ", ActivityMain.activityCheck.iScriptInstanceID)
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: g_iKillAllActivityCreatorScripts[", ActivityMain.activityCheck.iScriptInstanceID,"] TRUE, ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity), ", killing ")
			g_iKillAllActivityCreatorScripts[ActivityMain.activityCheck.iScriptInstanceID] = 0
			SetActStage(ActivityMain, ACTIVITY_STAGE_BUNKER_CLEANUP)
		ELSE
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: activityCheck.iScriptInstanceID = ", ActivityMain.activityCheck.iScriptInstanceID, ", g_iKillAllActivityCreatorScripts[ActivityMain.activityCheck.iScriptInstanceID] = 0")
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_SCRIPT_CLEANUP: Player is using orbital cannon, clean up activity")
		RETURN TRUE
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PROTOTYPE_ACTIVITY_SCRIPT_CLEANUP()	
	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD0, KEYBOARD_MODIFIER_CTRL_SHIFT, "Kill ActivityCreatorUI Script") 
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_PROTOTYPE_ACTIVITY_SCRIPT_CLEANUP: Ctrl Shift 0 pressed ")
		RETURN TRUE
	ENDIF
	#ENDIF

	//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_PROTOTYPE_ACTIVITY_SCRIPT_CLEANUP")
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_PROTOTYPE_ACTIVITY_SCRIPT_CLEANUP: NETWORK_IS_GAME_IN_PROGRESS = TRUE")
		RETURN TRUE
	ENDIF
	
	
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_PROTOTYPE_ACTIVITY_SCRIPT_CLEANUP: SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
		RETURN TRUE
	ENDIF


	IF IS_PLAYER_DEAD(PLAYER_ID())
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_PROTOTYPE_ACTIVITY_SCRIPT_CLEANUP: IS_PLAYER_DEAD = TRUE")
		RETURN TRUE
	ENDIF


//	#IF IS_DEBUG_BUILD
//	IF luxeActStruct.bCleanupScriptDebug
//		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_PROTOTYPE_ACTIVITY_SCRIPT_CLEANUP: bCleanupScriptDebug = TRUE")
//		RETURN TRUE
//	ENDIF
//	#ENDIF



	
	RETURN FALSE
ENDFUNC

PROC TERMINATE_NET_ACTIVITY_SCRIPT()
	CLEAR_HELP()
	TERMINATE_THIS_THREAD()
ENDPROC

PROC GET_RELATIVE_COORDS_FOR_INTERIOR(ACTIVITY_MAIN &ActivityMain)
	INT iArea
	MP_PROP_OFFSET_STRUCT mpOffsetBoundBox1
	MP_PROP_OFFSET_STRUCT mpOffsetBoundBox2
	REPEAT iMaxSyncSceneEntryAreas iArea
		mpOffsetBoundBox1.vLoc = <<0,0,0>>
		mpOffsetBoundBox1.vRot = <<0,0,0>>
		mpOffsetBoundBox1.vLoc = ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[iArea].vAngledAreaA 

		MP_PROP_OFFSET_STRUCT mpBaseInteriorLoc 
		mpBaseInteriorLoc = GET_BASE_INTERIOR_LOCATION(GET_BASE_PROPERTY_FROM_PROPERTY(ActivityMain.iCurrentProperty))
		MP_PROP_OFFSET_STRUCT mpInteriorLoc 
		mpInteriorLoc = GET_BASE_INTERIOR_LOCATION(ActivityMain.iCurrentProperty)
		GET_RELATIVE_COORDS_FROM_BASE_COORDS(mpBaseInteriorLoc.vLoc, mpInteriorLoc.vLoc, mpOffsetBoundBox1)

		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[iArea].vAngledAreaA = mpOffsetBoundBox1.vLoc 

		mpOffsetBoundBox2.vLoc = <<0,0,0>>
		mpOffsetBoundBox2.vRot = <<0,0,0>>
		mpOffsetBoundBox2.vLoc = ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[iArea].vAngledAreaB
		GET_RELATIVE_COORDS_FROM_BASE_COORDS(mpBaseInteriorLoc.vLoc, mpInteriorLoc.vLoc, mpOffsetBoundBox2)
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[iArea].vAngledAreaB = mpOffsetBoundBox2.vLoc 
	ENDREPEAT
ENDPROC

PROC RUN_LAUNCHED_SCRIPT_CHECKS(ACTIVITY_MAIN &ActivityMain)

	IF SHOULD_ACTIVITY_SCRIPT_CLEANUP(ActivityMain)
		SetActStage(ActivityMain, ACTIVITY_STAGE_CLEANUP)
		//If the script should die becuse MP is going then we must kill it
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "RUN_LAUNCHED_SCRIPT_CHECKS: SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - Calling TERMINATE_NET_ACTIVITY_SCRIPT")
			#ENDIF
			TERMINATE_NET_ACTIVITY_SCRIPT()
		ENDIF
		PRINTNL()
	ENDIF
	
	IF NOT IS_SAFE_TO_SHOW_MESSAGE()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText)
			CLEAR_HELP_TEXT_GENERIC(ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct)
			
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "RUN_LAUNCHED_SCRIPT_CHECKS: IS_SAFE_TO_SHOW_MESSAGE - Clearing activity help text.")
			#ENDIF
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_TOO_MANY")
			CLEAR_HELP()
		ENDIF
	ENDIF		

ENDPROC

PROC PED_CAMERA_IGNORE_ANIMATED_ENTITY_COLLISION(ACTIVITY_MAIN &ActivityMain)
	INT i
//	INT m
	REPEAT iMaxSyncSceneEntityInSequence i
//		REPEAT iMaxNumberOfSwappableEntities m
			IF DOES_ENTITY_EXIST(ActivityMain.oiSceneObjects[i])
	//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE: oiSceneObjects ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ActivityMain.oiSceneObjects[i][0])))
				SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(ActivityMain.oiSceneObjects[i])
	//			SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), ActivityMain.oiSceneObjects[i][m], TRUE) // replacing collision between player and office chair.
			ENDIF
//		ENDREPEAT
	ENDREPEAT
	
ENDPROC

PROC ASSET_RENDER_TO_ROOM_LOGIC(ACTIVITY_MAIN &ActivityMain)
	IF NOT DOES_ENTITY_EXIST(GET_PLAYER_PED(PLAYER_ID()))
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ASSET_RENDER_TO_ROOM_LOGIC: ped does not exist")
		#ENDIF
		
		EXIT
	ENDIF
	
	IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())))
	AND IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID())))
		INT i
		REPEAT iMaxSyncScenePedInSequence i 
			IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(PLAYER_ID()))
			AND NOT IS_ENTITY_DEAD(ActivityMain.piScenePeds[i])
				IF DOES_ENTITY_EXIST(ActivityMain.piScenePeds[i])
				
					// InteriorTarget should be valid or 0. If it's 0, then we need to make sure to force the target ped into
					// the player's local interior.
					INTERIOR_INSTANCE_INDEX interiorTarget = GET_INTERIOR_FROM_ENTITY(ActivityMain.piScenePeds[i])
					INTERIOR_INSTANCE_INDEX interiorPlayer = GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID()))
					INT keyPlayer = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
				
					IF (IS_VALID_INTERIOR(interiorPlayer))
						IF (interiorPlayer != interiorTarget)
							IF (keyPlayer != 0)
						
								FORCE_ROOM_FOR_ENTITY(ActivityMain.piScenePeds[i], interiorPlayer, keyPlayer)					
								
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ASSET_RENDER_TO_ROOM_LOGIC: activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity), ", iScriptInstanceID:", ActivityMain.activityCheck.iScriptInstanceID, ", ActivityMain.piScenePeds[",i , "] forcing to render to room")
								//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "ASSET_RENDER_TO_ROOM_LOGIC: Player: ", NATIVE_TO_INT(interiorPlayer), ", Key: ", keyPlayer)
								#ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								// Invalid room key for the player makes this difficult as one must specify a place for the interior
								// occlusion portals to start processing entity visibility.
								//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "ASSET_RENDER_TO_ROOM_LOGIC: Player ped room key is invalid, cannot force target. PlInter: ", NATIVE_TO_INT(interiorPlayer) ,", PlKey: ", keyPlayer, ", PedIdx: ", NATIVE_TO_INT(ActivityMain.piScenePeds[i]))
							#ENDIF
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "ASSET_RENDER_TO_ROOM_LOGIC: Player ped interior is invalid, cannot force target. PlInter: ", NATIVE_TO_INT(interiorPlayer) ,", PlKey: ", keyPlayer, ", PedIdx: ", NATIVE_TO_INT(ActivityMain.piScenePeds[i]))					
					#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		INT j
		REPEAT iMaxSyncSceneEntityInSequence i 
		
			IF NOT IS_ENTITY_DEAD(ActivityMain.activityAnimSequence.activitySyncSceneEntity[j].EntityIndex)
				IF DOES_ENTITY_EXIST(ActivityMain.activityAnimSequence.activitySyncSceneEntity[j].EntityIndex)
				
					// InteriorTarget should be valid or 0. If it's 0, then we need to make sure to force the target ped into
					// the player's local interior.
					INTERIOR_INSTANCE_INDEX interiorTarget = GET_INTERIOR_FROM_ENTITY(ActivityMain.activityAnimSequence.activitySyncSceneEntity[j].EntityIndex)
					INTERIOR_INSTANCE_INDEX interiorPlayer = GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID()))
					INT keyPlayer = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
				
					IF (IS_VALID_INTERIOR(interiorPlayer))
						IF (interiorPlayer != interiorTarget)
							IF (keyPlayer != 0)
						
								FORCE_ROOM_FOR_ENTITY(ActivityMain.activityAnimSequence.activitySyncSceneEntity[j].EntityIndex, interiorPlayer, keyPlayer)					
								
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ASSET_RENDER_TO_ROOM_LOGIC: activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity), ", iScriptInstanceID:", ActivityMain.activityCheck.iScriptInstanceID, ", ActivityMain.activityAnimSequence.activitySyncSceneEntity[",j , "] forcing to render to room")
								
								//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "ASSET_RENDER_TO_ROOM_LOGIC: Player: ", NATIVE_TO_INT(interiorPlayer), ", Key: ", keyPlayer)
								#ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								// Invalid room key for the player makes this difficult as one must specify a place for the interior
								// occlusion portals to start processing entity visibility.
								//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "ASSET_RENDER_TO_ROOM_LOGIC: Player ped room key is invalid, cannot force target. PlInter: ", NATIVE_TO_INT(interiorPlayer) ,", PlKey: ", keyPlayer, ", PedIdx: ", NATIVE_TO_INT(ActivityMain.activityAnimSequence.activitySyncSceneEntity[j].EntityIndex))
							#ENDIF
							ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "ASSET_RENDER_TO_ROOM_LOGIC: Player ped interior is invalid, cannot force target. PlInter: ", NATIVE_TO_INT(interiorPlayer) ,", PlKey: ", keyPlayer, ", PedIdx: ", NATIVE_TO_INT(ActivityMain.activityAnimSequence.activitySyncSceneEntity[j].EntityIndex))					
					#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC CHECK_FOR_TAGGED_CLIPS(ACTIVITY_MAIN &ActivityMain)
		
	INT s
	INT j
	REPEAT iMaxNumberOfAnimStates s
		REPEAT iMaxNumberOfSequences j
			
//			GET_CFID_DESK_DOCS_ANIM_DATA(activityAnimSequence)
			GET_SEQ_ANIM_DATA(ActivityMain.eActivity, s, j, ActivityMain.activityAnimSequence)
			IF ActivityMain.activityAnimSequence.clipTypeDefData.bBaseVarClip
				 
				ActivityMain.animState[s].iBaseClipsArray[ActivityMain.animState[s].iBaseClipCount] = j
																																
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CHECK_FOR_TAGGED_CLIPS: ActivityMain.animState[", s, "].iBaseClipsArray[", ActivityMain.animState[s].iBaseClipCount, "] = ", j)
				ActivityMain.animState[s].iBaseClipCount++
				
				
				
				IF NOT ActivityMain.bActivityUsesAnimClipTags
					ActivityMain.bActivityUsesAnimClipTags = TRUE
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CHECK_FOR_TAGGED_CLIPS: ActivityMain.bActivityUsesAnimClipTags = TRUE ")
				ENDIF
			ENDIF
			IF ActivityMain.activityAnimSequence.clipTypeDefData.bIdleVarClip
				
				ActivityMain.animState[s].iIdleClipsArray[ActivityMain.animState[s].iIdleClipCount] = j
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CHECK_FOR_TAGGED_CLIPS: ActivityMain.animState[", s, "].iIdleClipsArray[", ActivityMain.animState[s].iIdleClipCount, "] = ", j)
				ActivityMain.animState[s].iIdleClipCount++
				
				
				
				IF NOT ActivityMain.bActivityUsesAnimClipTags
					ActivityMain.bActivityUsesAnimClipTags = TRUE
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CHECK_FOR_TAGGED_CLIPS: ActivityMain.bActivityUsesAnimClipTags = TRUE ")
				ENDIF
			ENDIF
			IF ActivityMain.activityAnimSequence.clipTypeDefData.bTransitionVarClip
			
				ActivityMain.animState[s].iTransClipsArray[ActivityMain.animState[s].iTransClipCount] = j
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CHECK_FOR_TAGGED_CLIPS: ActivityMain.animState[", s, "].iTransClipsArray[", ActivityMain.animState[s].iTransClipCount, "] = ", j)
				ActivityMain.animState[s].iTransClipCount++
				
				
				IF NOT ActivityMain.bActivityUsesAnimClipTags
					ActivityMain.bActivityUsesAnimClipTags = TRUE
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CHECK_FOR_TAGGED_CLIPS: ActivityMain.bActivityUsesAnimClipTags = TRUE ")
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

PROC BLOCKING_LOGIC(ACTIVITY_MAIN &ActivityMain)
	IF ActivityMain.actStage > ACTIVITY_STAGE_APPROACH
		IF ActivityMain.bBlockRecording
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		ENDIF
	ENDIF
ENDPROC

PROC SET_ACTIVITY_FLAGS(ACTIVITY_MAIN &ActivityMain)
	IF ActivityMain.actStage > ACTIVITY_STAGE_APPROACH
		// Set the peds capsule radius each frame. 
		IF ActivityMain.activityCheck.bUpdateActivityPhysics
			SET_PED_CAPSULE(PLAYER_PED_ID(), ActivityMain.activityCheck.fCapsuleRadius)
//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "RUN_ACTIVITY_UPDATE: SET_PED_CAPSULE - Setting ped capsule radius to ", ActivityMain.activityCheck.fCapsuleRadius)
		ENDIF
//		IF NOT ActivityMain.activityCheck.bRagDoll
//		AND NOT IS_ENTITY_DEAD(ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex)
////			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SET_ACTIVITY_FLAGS: SET_PED_CAN_RAGDOLL = FALSE")
//			SET_PED_CAN_RAGDOLL(ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex, FALSE)
//			SET_RAGDOLL_BLOCKING_FLAGS(ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex, RBF_VEHICLE_IMPACT)
//		ENDIF
	ENDIF
ENDPROC 

//PROC REMOVE_CONFLICTING_CLOTHING(ACTIVITY_MAIN &ActivityMain)
//	IF ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()
//		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "REMOVE_CONFLICTING_CLOTHING: PLAYER_PED_ID")
//		IF IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
//			CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
//		ENDIF
//	ELSE
//		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "REMOVE_CONFLICTING_CLOTHING: ")
//	ENDIF
//ENDPROC

FUNC BOOL SHOULD_ACTIVITY_REMOVE_HELMETS(ACTIVITY_MAIN &ActivityMain)
	SWITCH ActivityMain.eActivity
		CASE RADIO_ENUM_1
		CASE RADIO_ENUM_2
		CASE RADIO_ENUM_3
		CASE RADIO_ENUM_4
		CASE ACT_CREATOR_OSPREY_TURRET_SEAT_LEFT
		CASE ACT_CREATOR_OSPREY_TURRET_SEAT
		CASE ACT_CREATOR_IAA_TURRET_SEAT
		CASE ACT_CREATOR_TURRET_SEAT_LEFT
		CASE ACT_CREATOR_TURRET_SEAT
		CASE ACT_CREATOR_HACKER_TRUCK_SEAT_1
		CASE ACT_CREATOR_HACKER_TRUCK_SEAT_2
		#IF FEATURE_HEIST_ISLAND
		CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_1
		CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_2
		#ENDIF	
			RETURN FALSE
		BREAK
		DEFAULT
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC

PROC REMOVE_CONFLICTING_CLOTHING(ACTIVITY_MAIN &ActivityMain)
	IF SHOULD_ACTIVITY_REMOVE_HELMETS(ActivityMain)
		IF ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()
		AND ActivityMain.bClothingStored = FALSE
			PED_COMP_NAME_ENUM eThisPropHead = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD))
			IF IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, eThisPropHead)
				
				ActivityMain.eCachedPropHeadItem = eThisPropHead
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "REMOVE_CONFLICTING_CLOTHING: Ped is wearing helmet: ", ENUM_TO_INT(eThisPropHead), ", removing")
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
				
				ActivityMain.bClothingStored = TRUE
			ENDIF
			
			PED_COMP_NAME_ENUM eThisBerd = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD)
			IF IS_ITEM_NIGHTVISION(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_BERD, eThisBerd)
				ActivityMain.eCachedBerdItem = eThisBerd
				
				SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, GET_DUMMY_COMPONENT_FOR_SLOT(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_BERD, 0), FALSE)
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "REMOVE_CONFLICTING_CLOTHING: Ped is wearing Nightvision: ", ENUM_TO_INT(eThisBerd), ", removing")
				
				ActivityMain.bClothingStored = TRUE
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "REMOVE_CONFLICTING_CLOTHING: ")
		ENDIF
	ENDIF
ENDPROC  

PROC REAPPLY_CONFLICTING_CLOTHING(ACTIVITY_MAIN &ActivityMain)
	IF ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()
	AND ActivityMain.bClothingStored = TRUE
		
		IF ActivityMain.eCachedPropHeadItem != DUMMY_PED_COMP
		AND ActivityMain.eCachedPropHeadItem != OUTFIT_DEFAULT
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "REAPPLY_CONFLICTING_CLOTHING: Ped was wearing helmet: ", ENUM_TO_INT( ActivityMain.eCachedPropHeadItem), ", reapplying")
			SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ActivityMain.eCachedPropHeadItem, FALSE)
		ENDIF
		
		
		IF ActivityMain.eCachedBerdItem != DUMMY_PED_COMP
		AND ActivityMain.eCachedBerdItem != OUTFIT_DEFAULT
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "REAPPLY_CONFLICTING_CLOTHING: Ped was wearing NightVision: ", ENUM_TO_INT( ActivityMain.eCachedBerdItem), ", reapplying")
			SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, ActivityMain.eCachedBerdItem, FALSE)
		ENDIF
		ActivityMain.bClothingStored = FALSE
	ELSE
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "REMOVE_CONFLICTING_CLOTHING: ")
	ENDIF
ENDPROC  

PROC CLEANUP_ACTIVITY_PEDS_AND_PROPS(ACTIVITY_MAIN &ActivityMain)
	INT i
	REPEAT iMaxSyncScenePedInSequence i
		IF DOES_ENTITY_EXIST(ActivityMain.piScenePeds[i] )
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck ped[", i, "], to activityMain.piScenePed: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ActivityMain.piScenePeds[i])))
			DELETE_PED(ActivityMain.piScenePeds[i])
		ENDIF
	ENDREPEAT
	
	REPEAT iMaxSyncSceneEntityInSequence i
		IF DOES_ENTITY_EXIST(ActivityMain.getObjectsStruct[i].oiServerObject)
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_ACTIVITY_PEDS_AND_PROPS: deleting object i0: ", i, ", to ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ActivityMain.getObjectsStruct[i].oiServerObject)))
			DELETE_OBJECT(ActivityMain.getObjectsStruct[i].oiServerObject)
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEANUP_BUNKER_ACTIVITY_PEDS_AND_PROPS(CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck[iMaxCreatorActivities])
	INT iActivity
	REPEAT iMaxCreatorActivities iActivity
		INT i
		REPEAT iMaxSyncScenePedInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_BUNKER_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck ped[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])))
				
				DELETE_PED(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			ENDIF
			
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_BUNKER_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck objScenePedCollisionObj[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])))
				
				DELETE_OBJECT(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			ENDIF
		ENDREPEAT
		
			
		REPEAT iMaxSyncSceneEntityInSequence i
			
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_BUNKER_ACTIVITY_PEDS_AND_PROPS: deleting object i0 [", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])))
				DELETE_OBJECT(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			ENDIF
			
		ENDREPEAT
	ENDREPEAT
ENDPROC

PROC CLEANUP_NIGHTCLUB_ACTIVITY_PEDS_AND_PROPS(CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck[iMaxCreatorActivities])
	INT iActivity
	REPEAT iMaxCreatorActivities iActivity
		INT i
		REPEAT iMaxSyncScenePedInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_NIGHTCLUB_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck ped[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])))
				
				DELETE_PED(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			ENDIF
			
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_NIGHTCLUB_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck objScenePedCollisionObj[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])))
				
				DELETE_OBJECT(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			ENDIF
		ENDREPEAT
		
			
		REPEAT iMaxSyncSceneEntityInSequence i
			
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_NIGHTCLUB_ACTIVITY_PEDS_AND_PROPS: deleting object i0 [", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])))
				DELETE_OBJECT(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			ENDIF
			
		ENDREPEAT
	ENDREPEAT
ENDPROC

#IF FEATURE_CASINO_NIGHTCLUB
PROC CLEANUP_CASINO_NIGHTCLUB_ACTIVITY_PEDS_AND_PROPS(CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck[iMaxCreatorActivities])
	INT iActivity
	REPEAT iMaxCreatorActivities iActivity
		INT i
		REPEAT iMaxSyncScenePedInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
				DELETE_PED(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			ENDIF
			
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
				DELETE_OBJECT(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			ENDIF
		ENDREPEAT
		
		REPEAT iMaxSyncSceneEntityInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
				DELETE_OBJECT(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC
#ENDIF

PROC CLEANUP_ARENA_GARAGE_ACTIVITY_PEDS_AND_PROPS(CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck[iMaxCreatorActivities])
	INT iActivity
	REPEAT iMaxCreatorActivities iActivity
		INT i
		REPEAT iMaxSyncScenePedInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_ARENA_GARAGE_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck ped[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])))
				
				DELETE_PED(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			ENDIF
			
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_ARENA_GARAGE_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck objScenePedCollisionObj[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])))
				
				DELETE_OBJECT(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			ENDIF
		ENDREPEAT
		
		i = 0
		
		REPEAT iMaxSyncSceneEntityInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_ARENA_GARAGE_ACTIVITY_PEDS_AND_PROPS: deleting object i0 [", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])))
				
				DELETE_OBJECT(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

PROC CLEANUP_AUTO_SHOP_ACTIVITY_PEDS_AND_PROPS(CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck[iMaxCreatorActivities])
	INT iActivity
	REPEAT iMaxCreatorActivities iActivity
		INT i
		REPEAT iMaxSyncScenePedInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_AUTO_SHOP_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck ped[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])))
				
				DELETE_PED(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			ENDIF
			
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_AUTO_SHOP_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck objScenePedCollisionObj[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])))
				
				DELETE_OBJECT(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			ENDIF
		ENDREPEAT
		
		i = 0
		
		REPEAT iMaxSyncSceneEntityInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_AUTO_SHOP_ACTIVITY_PEDS_AND_PROPS: deleting object i0 [", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])))
				
				DELETE_OBJECT(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC

#IF FEATURE_FIXER
PROC CLEANUP_FIXER_HQ_ACTIVITY_PEDS_AND_PROPS(CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck[iMaxCreatorActivities])
	INT iActivity
	REPEAT iMaxCreatorActivities iActivity
		INT i
		REPEAT iMaxSyncScenePedInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_FIXER_HQ_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck ped[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])))
				
				DELETE_PED(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			ENDIF
			
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_FIXER_HQ_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck objScenePedCollisionObj[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])))
				
				DELETE_OBJECT(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			ENDIF
		ENDREPEAT
		
		i = 0
		
		REPEAT iMaxSyncSceneEntityInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_FIXER_HQ_ACTIVITY_PEDS_AND_PROPS: deleting object i0 [", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])))
				
				DELETE_OBJECT(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC
#ENDIF

#IF FEATURE_MUSIC_STUDIO
PROC CLEANUP_MUSIC_STUDIO_ACTIVITY_PEDS_AND_PROPS(CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck[iMaxCreatorActivities])
	INT iActivity
	REPEAT iMaxCreatorActivities iActivity
		INT i
		REPEAT iMaxSyncScenePedInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_MUSIC_STUDIO_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck ped[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])))
				
				DELETE_PED(activityCheck[iActivity].activityLocalPeds.piScenePeds[i])
			ENDIF
			
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_MUSIC_STUDIO_ACTIVITY_PEDS_AND_PROPS: deleting activityCheck objScenePedCollisionObj[", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])))
				
				DELETE_OBJECT(activityCheck[iActivity].activityLocalPeds.objScenePedCollisionObj[i])
			ENDIF
		ENDREPEAT
		
		i = 0
		
		REPEAT iMaxSyncSceneEntityInSequence i
			IF DOES_ENTITY_EXIST(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			AND NETWORK_HAS_CONTROL_OF_ENTITY(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "CLEANUP_MUSIC_STUDIO_ACTIVITY_PEDS_AND_PROPS: deleting object i0 [", i, "], eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(activityCheck[iActivity].eActivity), ", iInstanceID = ", activityCheck[iActivity].iScriptInstanceID, ", model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])))
				
				DELETE_OBJECT(activityCheck[iActivity].activityLocalProps.oiSceneObjects[i])
			ENDIF
		ENDREPEAT
	ENDREPEAT
ENDPROC
#ENDIF


FUNC BOOL SHOULD_ACTIVITY_SUPPRESS_PHONE(ACTIVITY_MAIN &ActivityMain)
	
	IF NOT IS_LOCAL_PLAYER_USING_DRONE()
		SWITCH ActivityMain.eActivity
			CASE ACT_CREATOR_TURRET_SEAT_LEFT
			CASE ACT_CREATOR_TURRET_SEAT
			CASE ACT_CREATOR_OSPREY_TURRET_SEAT
			CASE ACT_CREATOR_OSPREY_TURRET_SEAT_LEFT
			CASE ACT_CREATOR_IAA_TURRET_SEAT
			CASE ACT_CREATOR_HACKER_TRUCK_SEAT_1
			CASE ACT_CREATOR_HACKER_TRUCK_SEAT_2
			#IF FEATURE_HEIST_ISLAND
			CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_1
			CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_2
			#ENDIF	
				IF ActivityMain.actStage > ACTIVITY_STAGE_APPROACH
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SUPPRESS_PHONE_LOGIC(ACTIVITY_MAIN &ActivityMain)

	IF SHOULD_ACTIVITY_SUPPRESS_PHONE(ActivityMain)
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
ENDPROC

PROC MAINTAIN_SUPPRESS_RECORDING_LOGIC(ACTIVITY_MAIN &ActivityMain)
	IF SHOULD_ACTIVITY_SUPPRESS_PHONE(ActivityMain)
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	ENDIF
ENDPROC


FUNC BOOL SHOULD_ACTIVITY_SUPPRESS_IDLE_CAM(ACTIVITY_MAIN &ActivityMain)
	SWITCH ActivityMain.eActivity
		CASE ACT_CREATOR_TURRET_SEAT_LEFT
		CASE ACT_CREATOR_TURRET_SEAT
		CASE ACT_CREATOR_OSPREY_TURRET_SEAT
		CASE ACT_CREATOR_OSPREY_TURRET_SEAT_LEFT
		CASE ACT_CREATOR_IAA_TURRET_SEAT
		CASE ACT_CREATOR_HACKER_TRUCK_SEAT_1
		CASE ACT_CREATOR_HACKER_TRUCK_SEAT_2
		#IF FEATURE_HEIST_ISLAND
		CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_1
		CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_2
		#ENDIF	
			RETURN TRUE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ACTIVITY_SUPPRESS_INTERACTION_MENU(ACTIVITY_MAIN &ActivityMain)
	SWITCH ActivityMain.eActivity
		CASE ACT_CREATOR_TURRET_SEAT_LEFT
		CASE ACT_CREATOR_TURRET_SEAT
		CASE ACT_CREATOR_OSPREY_TURRET_SEAT
		CASE ACT_CREATOR_OSPREY_TURRET_SEAT_LEFT
		CASE ACT_CREATOR_IAA_TURRET_SEAT
		CASE ACT_CREATOR_HACKER_TRUCK_SEAT_1
		CASE ACT_CREATOR_HACKER_TRUCK_SEAT_2
		#IF FEATURE_HEIST_ISLAND
		CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_1
		CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_2
		#ENDIF	
			SWITCH ActivityMain.actStage
				CASE ACTIVITY_STAGE_APPROACH
				CASE ACTIVITY_STAGE_RUN_ENTER_ANIM
				CASE ACTIVITY_STAGE_CHECK_ENTER_OR_IDLE_ANIM
				CASE ACTIVITY_STAGE_CHECK_BASE_ANIM
				CASE ACTIVITY_STAGE_RUNANIM
				CASE ACTIVITY_STAGE_EXIT_TURN
				CASE ACTIVITY_STAGE_ENTER_TURN
				CASE ACTIVITY_STAGE_EXIT
				CASE ACTIVITY_STAGE_CLEANUP
				CASE ACTIVITY_STAGE_BUNKER_CLEANUP
				CASE ACTIVITY_STAGE_EXIT_INTERRUPT
					RETURN TRUE
				BREAK
				
			ENDSWITCH 

		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ACTIVITY_CAM_IGNORE_ENTITY_COLLISION(ACTIVITY_MAIN &ActivityMain, INT &iEntityID)

	BOOL bReturnBool = FALSE
	SWITCH ActivityMain.eActivity
		CASE ACT_CREATOR_TURRET_SEAT_LEFT
		CASE ACT_CREATOR_TURRET_SEAT
		CASE ACT_CREATOR_OSPREY_TURRET_SEAT
		CASE ACT_CREATOR_OSPREY_TURRET_SEAT_LEFT
		CASE ACT_CREATOR_IAA_TURRET_SEAT
		CASE ACT_CREATOR_HACKER_TRUCK_SEAT_1
		CASE ACT_CREATOR_HACKER_TRUCK_SEAT_2
		#IF FEATURE_HEIST_ISLAND
		CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_1
		CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_2
		#ENDIF	
			iEntityID = 0
			bReturnBool = TRUE
		BREAK
	ENDSWITCH

	
	IF bReturnBool
		//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_CAM_IGNORE_ENTITY_COLLISION: TRUE: ActivityMain.eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
	ENDIF 
	RETURN bReturnBool
ENDFUNC

FUNC BOOL SHOULD_ACTIVITY_PLAYER_IGNORE_ENTITY_COLLISION(ACTIVITY_MAIN &ActivityMain, INT &iEntityID)

	BOOL bReturnBool = FALSE
	SWITCH ActivityMain.eActivity
		CASE ACT_CREATOR_TURRET_SEAT_LEFT
		CASE ACT_CREATOR_TURRET_SEAT
		CASE ACT_CREATOR_OSPREY_TURRET_SEAT
		CASE ACT_CREATOR_OSPREY_TURRET_SEAT_LEFT
		CASE ACT_CREATOR_IAA_TURRET_SEAT
		CASE ACT_CREATOR_HACKER_TRUCK_SEAT_1
		CASE ACT_CREATOR_HACKER_TRUCK_SEAT_2
		#IF FEATURE_HEIST_ISLAND
		CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_1
		CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_2
		#ENDIF		
			iEntityID = 0
			bReturnBool = TRUE
		BREAK
		CASE ACT_CREATOR_DEFUNCT_RUM
			iEntityID = 1
			bReturnBool = TRUE
		BREAK
	ENDSWITCH

//	bReturnBool = FALSE
//	IF bReturnBool
//		//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_PLAYER_IGNORE_ENTITY_COLLISION: TRUE: ActivityMain.eActivity = ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
//	ENDIF 
	RETURN bReturnBool
ENDFUNC

PROC MAINTAIN_SUPPRESS_IDLE_CAM_LOGIC(ACTIVITY_MAIN &ActivityMain)

	IF SHOULD_ACTIVITY_SUPPRESS_IDLE_CAM(ActivityMain)
		INVALIDATE_IDLE_CAM()
	ENDIF
ENDPROC

PROC MAINTAIN_SUPPRESS_INTERACTION_MENU_LOGIC(ACTIVITY_MAIN &ActivityMain)

	IF SHOULD_ACTIVITY_SUPPRESS_INTERACTION_MENU(ActivityMain)
//		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_SUPPRESS_INTERACTION_MENU_LOGIC: DISABLE_INTERACTION_MENU ActivityMain.actStage = ", actStateString(ActivityMain.actStage))
		DISABLE_INTERACTION_MENU()
	ELSE
//		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_SUPPRESS_INTERACTION_MENU_LOGIC: ENABLE_INTERACTION_MENU ActivityMain.actStage = ", actStateString(ActivityMain.actStage))
		ENABLE_INTERACTION_MENU()
	ENDIF
ENDPROC

PROC MAINTAIN_CAMERA_COLLISION_IGNORE_LOGIC(ACTIVITY_MAIN &ActivityMain)
	INT iIgnoreEntityID
	IF SHOULD_ACTIVITY_CAM_IGNORE_ENTITY_COLLISION(ActivityMain, iIgnoreEntityID)
		IF NOT IS_ENTITY_DEAD(ActivityMain.activityAnimSequence.activitySyncSceneEntity[iIgnoreEntityID].EntityIndex)
			SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(ActivityMain.activityAnimSequence.activitySyncSceneEntity[iIgnoreEntityID].EntityIndex)
			//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_CAMERA_COLLISION_IGNORE_LOGIC: GamePlay cam ignorning collision of activitySyncSceneEntity[", iIgnoreEntityID, "].EntityIndex = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ActivityMain.activityAnimSequence.activitySyncSceneEntity[iIgnoreEntityID].EntityIndex)))
		ELSE
			//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_CAMERA_COLLISION_IGNORE_LOGIC: GamePlay cam ignorning collision of activitySyncSceneEntity[", iIgnoreEntityID, "].EntityIndex, however entity is dead")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PLAYER_COLLISION_IGNORE_LOGIC(ACTIVITY_MAIN &ActivityMain)
	INT iIgnoreEntityID
	IF SHOULD_ACTIVITY_PLAYER_IGNORE_ENTITY_COLLISION(ActivityMain, iIgnoreEntityID)
		IF NOT IS_ENTITY_DEAD(ActivityMain.activityAnimSequence.activitySyncSceneEntity[iIgnoreEntityID].EntityIndex)
			//CDEBUG3LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_PLAYER_COLLISION_IGNORE_LOGIC: Player ignorning collision of activitySyncSceneEntity[", iIgnoreEntityID, "].EntityIndex = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ActivityMain.activityAnimSequence.activitySyncSceneEntity[iIgnoreEntityID].EntityIndex)))
			SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), ActivityMain.activityAnimSequence.activitySyncSceneEntity[iIgnoreEntityID].EntityIndex, TRUE) // replacing collision between player and office chair.
		ELSE
			CWARNINGLN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_PLAYER_IGNORE_ENTITY_COLLISION: GamePlay cam ignorning collision of activitySyncSceneEntity[", iIgnoreEntityID, "].EntityIndex")
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_GLOBALS(ACTIVITY_MAIN &ActivityMain)
	IF g_bPlayerInActivityArea
		g_iPlayerIsInModTurretAngledArea = ActivityMain.activityCheck.iTurretInstance
	ELSE
		g_iPlayerIsInModTurretAngledArea = -1
	ENDIF
ENDPROC

PROC MAINTAIN_FADE(ACTIVITY_MAIN &ActivityMain)
	IF (g_iShouldLaunchTruckTurret != -1
	OR g_iInteriorTurretSeat != -1)
	AND g_bTurretFadeOut
	AND NOT IS_PLAYER_LEAVING_SIMPLE_INTERIOR()
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		IF NOT HAS_NET_TIMER_STARTED(ActivityMain.stFadeTimer)
			START_NET_TIMER(ActivityMain.stFadeTimer,TRUE)
			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_FADE: START_NET_TIMER ")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(ActivityMain.stFadeTimer, 500, TRUE)
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_FADE: screen fading in ")
				RESET_NET_TIMER(ActivityMain.stFadeTimer)
				g_bTurretFadeOut = FALSE
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ActivityCreatorMaintainFade")
			IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)	
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_FADE: g_iShouldLaunchTruckTurret = ", g_iShouldLaunchTruckTurret)
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_FADE: g_iInteriorTurretSeat = ", g_iInteriorTurretSeat)
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_FADE: g_bTurretFadeOut = ", g_bTurretFadeOut)
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_FADE: IS_PLAYER_LEAVING_SIMPLE_INTERIOR = ", IS_PLAYER_LEAVING_SIMPLE_INTERIOR())
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_FADE: HAS_NET_TIMER_STARTED(ActivityMain.stFadeTimer) = ", HAS_NET_TIMER_STARTED(ActivityMain.stFadeTimer))
			ENDIF
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_UPDATE_FUCKED_SCRIPT(ACTIVITY_MAIN &ActivityMain)
	IF ENUM_TO_INT(ActivityMain.eActivity) = -1
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_UPDATE_FUCKED_SCRIPT: ActivityMain.eActivity = -1, Setting to ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.activityCheck.eActivity))
		ActivityMain.eActivity = ActivityMain.activityCheck.eActivity
	ENDIF
	
ENDPROC

PROC MAINTAIN_SUPRESS_WEAPONS(ACTIVITY_MAIN &ActivityMain)
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
	AND ActivityMain.actStage > ACTIVITY_STAGE_APPROACH
		HUD_FORCE_WEAPON_WHEEL(FALSE)
		DISPLAY_AMMO_THIS_FRAME(FALSE)			
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
		WEAPON_TYPE wtWeapon
		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wtWeapon)
		AND wtWeapon != WEAPONTYPE_UNARMED
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_HEAD_TRACKING_LOGIC(ACTIVITY_MAIN &ActivityMain)
	IF ActivityMain.bHeadTracking = TRUE
		IF NOT IS_ENTITY_DEAD(ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex)
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF GET_DISTANCE_BETWEEN_ENTITIES(ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex, PLAYER_PED_ID()) < 15
				GET_SEQ_ANIM_DATA(ActivityMain.eActivity, ActivityMain.iCurrentAnimState, ActivityMain.iCurrentAnimSequenceID, ActivityMain.activityAnimSequence, IS_PED_FEMALE(ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex))
				
				IF NOT IS_PED_HEADTRACKING_ENTITY(ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex, PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex,  PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_BROADCAST_VARS()
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iTurretSeat != g_iInteriorTurretSeat
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iTurretSeat = g_iInteriorTurretSeat
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "MAINTAIN_BROADCAST_VARS: GlobalplayerBD_FM.iTurretSeat = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iTurretSeat)
	ENDIF
ENDPROC

PROC IGNORE_DOORS_LOGIC(ACTIVITY_MAIN &ActivityMain)
	SWITCH ActivityMain.eActivity
		CASE ACT_CREATOR_SECURITY_STAND_WITH_WEAPON
		CASE ACT_CREATOR_BUNKER_RESEARCH_COMPUTER
			IF NOT IS_ENTITY_DEAD(ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex)
				SET_PED_IS_IGNORED_BY_AUTO_OPEN_DOORS(ActivityMain.activityAnimSequence.activitySyncScenePed[0].PedIndex, TRUE)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_ACTIVITY_IN_USE_VARIABLE(BOOL bSet)
	IF IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
	AND NOT IS_PLAYER_IN_NIGHTCLUB_OFFICE_AREA(PLAYER_ID())
		g_bDrinkingActivityRunning = bSet
	ENDIF
ENDPROC

PROC BLOCK_RECORDING_ILLICIT_ACTIVITIES(ACTIVITY_ENUMS &eActivity)
	IF eActivity > ACTIVITY_NONE
	AND eActivity < MAX_NUMBER_ACTIVITIES
		SWITCH eActivity
			CASE OLD_BEER_DRINKING_ENUM
			CASE NEW_BEER_DRINKING_ENUM
			CASE WHISKY_DRINK_ENUM
			CASE NEW_BONG_ENUM
			CASE OLD_BONG_ENUM
			CASE NEW_WHISKY_ENUM
			CASE WINE_DRINK_ENUM
			CASE NEW_WINE_DRINK_ENUM
			CASE NEW_WHISKY_2_ENUM
			CASE NEW_WHISKY_3_ENUM
			CASE ACT_CREATOR_BEER_1
			CASE ACT_CREATOR_BEER_2
			CASE ACT_CREATOR_BEER_3
			CASE ACT_CREATOR_WHISKEY_VARA_1
			CASE ACT_CREATOR_IDLE_SMOKING
			CASE ACT_CREATOR_IDLE_SMOKING_JOINT
			CASE ACT_CREATOR_BUNKER_WHISKY
			CASE ACT_CREATOR_DEFUNCT_RUM
			CASE ACT_CREATOR_NIGHT_CLUB_RUM
			CASE ACT_CREATOR_NIGHT_CLUB_VIP_WHISKEY
			CASE ACT_CREATOR_NIGHT_CLUB_BEER
			CASE ACT_CREATOR_ARENA_GARAGE_BEER
			CASE ACT_CREATOR_ARENA_GARAGE_RUM
			CASE ACT_CREATOR_ARENA_GARAGE_WHISKEY
			CASE ACT_CREATOR_ARENA_GARAGE_WHISKEY_2
			CASE ACT_CREATOR_CASINO_BEER
			CASE ACT_CREATOR_CASINO_VODKA
			CASE ACT_CREATOR_CASINO_VODKA_2
			#IF FEATURE_FIXER
			CASE ACT_CREATOR_FIXER_HQ_NEW_BONG
			#ENDIF
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "BLOCK_RECORDING_ILLICIT_ACTIVITIES: Blocking recording when using activity ", DEBUG_GET_MP_PROP_ACT_NAME(eActivity))
			BREAK
		ENDSWITCH	
	ENDIF
ENDPROC

PROC RUN_ACTIVITY_UPDATE(ACTIVITY_MAIN &ActivityMain)

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		MAINTAIN_UPDATE_FUCKED_SCRIPT(ActivityMain)
		PED_CAMERA_IGNORE_ANIMATED_ENTITY_COLLISION(ActivityMain)
		ASSET_RENDER_TO_ROOM_LOGIC(ActivityMain)
		BLOCKING_LOGIC(ActivityMain)
//		SET_ACTIVITY_FLAGS(ActivityMain)
		MAINTAIN_TRUCK_TURRET_CONTROL_SUPPRESSION(ActivityMain)
		MAINTAIN_TRUCK_TURRET_USE_CONTROLS_GLOB(ActivityMain)
		MAINTAIN_SUPPRESS_PHONE_LOGIC(ActivityMain)
		MAINTAIN_SUPPRESS_IDLE_CAM_LOGIC(ActivityMain)
		MAINTAIN_CAMERA_COLLISION_IGNORE_LOGIC(ActivityMain)
		MAINTAIN_PLAYER_COLLISION_IGNORE_LOGIC(ActivityMain)
		MAINTAIN_SUPPRESS_RECORDING_LOGIC(ActivityMain)
		MAINTAIN_GLOBALS(ActivityMain)
		MAINTAIN_FADE(ActivityMain)
		MAINTAIN_SUPPRESS_INTERACTION_MENU_LOGIC(ActivityMain)
		MAINTAIN_SUPRESS_WEAPONS(ActivityMain)
		MAINTAIN_HEAD_TRACKING_LOGIC(ActivityMain)
		MAINTAIN_BROADCAST_VARS()
		IGNORE_DOORS_LOGIC(ActivityMain)
		
		IF ActivityMain.activityCheck.bScriptLaunch = TRUE
			RUN_LAUNCHED_SCRIPT_CHECKS(ActivityMain)
		ENDIF
//		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ActivityMain.actStage = ", actStateString(ActivityMain.actStage))



		IF ActivityMain.actStage > ACTIVITY_STAGE_SHOULD_LAUNCH
		AND ActivityMain.actStage < ACTIVITY_STAGE_EXIT
			BLOCK_RECORDING_ILLICIT_ACTIVITIES(ActivityMain.activityCheck.eActivity)
		ENDIF


		SWITCH ActivityMain.actStage
		
			CASE ACTIVITY_STAGE_INIT
				
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty != -1	
					GET_RELATIVE_COORDS_FOR_INTERIOR(ActivityMain)
				ENDIF
				CHECK_FOR_TAGGED_CLIPS(ActivityMain)
				ActivityMain.activityAnimSequence.scenePosition = ActivityMain.activityCheck.vScenePos
				ActivityMain.activityAnimSequence.sceneOrientation = ActivityMain.activityCheck.vSceneRot
				
				SetActStage(ActivityMain, ACTIVITY_STAGE_SHOULD_LAUNCH)
			BREAK
		
			CASE ACTIVITY_STAGE_SHOULD_LAUNCH
				
				IF (ActivityMain.eActivity = RADIO_ENUM_3)
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
						CPRINTLN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SHOULD_LAUNCH: INPUT_FRONTEND_LEFT pressed - swap RADIO_ENUM_3 for RADIO_ENUM_4")
						ActivityMain.eActivity = RADIO_ENUM_4
						
//						CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck
//						activityCheck.iPropertyID = ActivityMain.iCurrentProperty
//						SETUP_RADIO_ACTIVITY4(ActivityMain, activityCheck)
						
						ActivityMain.RUN_AUDIO_EFFECT_LOGIC = &RUN_AUDIO_LOGIC_FOR_OFFICE_LIGHT_NIGHTCLUB
						
						// Enter input logic
						ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC				= &HAS_PLAYER_JUST_PRESSED_CONTROL
						ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType		= FRONTEND_CONTROL
						ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction		= INPUT_CONTEXT_SECONDARY
						
						ActivityMain.activityCheck.vSceneRot												= <<0,0,180.0>>-<<0,0,-10>>
						ActivityMain.activityAnimSequence.sceneOrientation									= ActivityMain.activityCheck.vSceneRot
					ENDIF
				ENDIF
				IF (ActivityMain.eActivity = RADIO_ENUM_4)
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
						CPRINTLN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SHOULD_LAUNCH: INPUT_CONTEXT pressed - swap RADIO_ENUM_4 for RADIO_ENUM_3")
						ActivityMain.eActivity = RADIO_ENUM_3
						
//						CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck
//						activityCheck.iPropertyID = ActivityMain.iCurrentProperty
//						SETUP_RADIO_ACTIVITY3(ActivityMain, activityCheck)
						
						ActivityMain.RUN_AUDIO_EFFECT_LOGIC = &RUN_AUDIO_LOGIC_FOR_FROSTED_GLASS_NIGHTCLUB
						
						// Enter input logic
						ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC				= &HAS_PLAYER_JUST_PRESSED_CONTROL
						ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType		= FRONTEND_CONTROL
						ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction		= INPUT_CONTEXT
						
						ActivityMain.activityCheck.vSceneRot												= <<0,0,180.0>>+<<0,0,-10>>
						ActivityMain.activityAnimSequence.sceneOrientation									= ActivityMain.activityCheck.vSceneRot
					ENDIF
				ENDIF
				
//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SHOULD_ACTIVITY_LAUNCH: ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
				IF CALL ActivityMain.SHOULD_ACTIVITY_LAUNCH(ActivityMain.shouldActivityLaunchStruct, ActivityMain.loadForActivity, ActivityMain.activityAnimSequence)				
					IF ActivityMain.bShouldPlayerApproachActivity = TRUE
						SetActStage(ActivityMain, ACTIVITY_STAGE_APPROACH)
						SET_ACTIVITY_IN_USE_VARIABLE(TRUE)
					ELSE
						IF DOES_ENTITY_EXIST(ActivityMain.piScenePeds[0])
							IF NOT IS_ENTITY_DEAD(ActivityMain.piScenePeds[0])
								IF IS_PED_FEMALE(ActivityMain.piScenePeds[0])
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SHOULD_LAUNCH: ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity), ", ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.activityCheck.eActivity), ", Getting female anims, model name ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ActivityMain.piScenePeds[0])))
								ELSE
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SHOULD_LAUNCH: NOT Getting female anims", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ActivityMain.piScenePeds[0])))
								ENDIF
								CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SHOULD_LAUNCH: ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity), ", ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.activityCheck.eActivity), ", ")
								GET_SEQ_ANIM_DATA(ActivityMain.eActivity, ActivityMain.iCurrentAnimState, ActivityMain.iCurrentAnimSequenceID, ActivityMain.activityAnimSequence, IS_PED_FEMALE(ActivityMain.piScenePeds[0]))
								SET_ACTIVITY_IN_USE_VARIABLE(TRUE)
								SetActStage(ActivityMain, ACTIVITY_STAGE_RUN_ENTER_ANIM)	
							ELSE
								CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SHOULD_LAUNCH: ActivityMain.piScenePeds[0] is dead, not launching")
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SHOULD_LAUNCH: ActivityMain.piScenePeds[0] doesn't exist, not launching, ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			
			CASE ACTIVITY_STAGE_APPROACH							
				IF ActivityMain.activityCheck.eActivity = ACT_CREATOR_OSPREY_TURRET_SEAT
				
					ActivityMain.activityAnimSequence.iEnterArea = 0
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ActivityMain.activityAnimSequence.iEnterArea = 0")
				ELIF ActivityMain.activityCheck.eActivity = ACT_CREATOR_IAA_TURRET_SEAT
					ActivityMain.activityAnimSequence.iEnterArea = 1
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ActivityMain.activityAnimSequence.iEnterArea = 1")
				ENDIF
				GET_SEQ_ANIM_DATA(ActivityMain.eActivity, ActivityMain.iCurrentAnimState, ActivityMain.iStartAnimID, ActivityMain.activityAnimSequence, DEFAULT, ActivityMain.activityAnimSequence.iEnterArea)
				IF NOT LOAD_APPROACH_ANIM(ActivityMain.activityAnimSequence)
					EXIT
				ENDIF
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_RUN_ENTER_ANIM: ActivityMain.activityCheck.iTurretInstance = ", ActivityMain.activityCheck.iTurretInstance, ", activity = ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity))
				IF ActivityMain.activityCheck.iTurretInstance != -1
				
					IF ActivityMain.bCanUseTurrets
					AND NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
					AND NOT IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
						g_iInteriorTurretSeat = ActivityMain.activityCheck.iTurretInstance
					ENDIF
					IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
					OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
						g_iInteriorTurretSeat = ActivityMain.activityCheck.iTurretInstance
					ENDIF
					IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
					OR IS_PLAYER_IN_CREATOR_TRAILER(PLAYER_ID())
						g_iShouldLaunchTruckTurret = ActivityMain.activityCheck.iTurretInstance
						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_APPROACH: g_iShouldLaunchTruckTurret = ", g_iShouldLaunchTruckTurret)
					ENDIF
					IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
						g_iShouldLaunchTruckTurret = ActivityMain.activityCheck.iTurretInstance
						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_APPROACH: g_iShouldLaunchTruckTurret = ", g_iShouldLaunchTruckTurret)
					ENDIF
					#IF FEATURE_HEIST_ISLAND
					IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
						g_iShouldLaunchSubmarineSeat = ActivityMain.activityCheck.iTurretInstance
						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_APPROACH: g_iShouldLaunchTruckTurret = ", g_iShouldLaunchSubmarineSeat)
					ENDIF
					#ENDIF
					
//					IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
//						g_iInteriorTurretSeat = ActivityMain.activityCheck.iTurretInstance
//						CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ACTIVITY_STAGE_APPROACH: g_iInteriorTurretSeat= ", g_iInteriorTurretSeat)
//					ENDIF
					
//					g_SimpleInteriorData.bSuppressConcealCheck = TRUE // Fix for B*3508803
					
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_RUN_ENTER_ANIM: CAN_USE_TURRET = TRUE: g_iShouldLaunchTruckTurret = ", g_iShouldLaunchTruckTurret)
				ELSE
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_RUN_ENTER_ANIM: CAN_USE_TURRET = FALSE: g_iShouldLaunchTruckTurret = ", g_iShouldLaunchTruckTurret)
				ENDIF
				
				GET_APPROACH_COORDS(ActivityMain)
				IF CALL ActivityMain.RUN_APPROACH_COMPONENT(ActivityMain.approachComponentStruct)
					IF DOES_ENTITY_EXIST(ActivityMain.piScenePeds[0])
					AND NOT IS_ENTITY_DEAD(ActivityMain.piScenePeds[0])
						REMOVE_CONFLICTING_CLOTHING(ActivityMain)
						GET_SEQ_ANIM_DATA(ActivityMain.eActivity, ActivityMain.iCurrentAnimState, ActivityMain.iCurrentAnimSequenceID, ActivityMain.activityAnimSequence, IS_PED_FEMALE(ActivityMain.piScenePeds[0]), ActivityMain.activityAnimSequence.iEnterArea)
						SetActStage(ActivityMain, ACTIVITY_STAGE_RUN_ENTER_ANIM)	
					ELSE
						REMOVE_CONFLICTING_CLOTHING(ActivityMain)
						GET_SEQ_ANIM_DATA(ActivityMain.eActivity, ActivityMain.iCurrentAnimState, ActivityMain.iCurrentAnimSequenceID, ActivityMain.activityAnimSequence, FALSE, ActivityMain.activityAnimSequence.iEnterArea)
						SetActStage(ActivityMain, ACTIVITY_STAGE_RUN_ENTER_ANIM)	
					ENDIF
				ENDIF
			BREAK
			
			CASE ACTIVITY_STAGE_RUN_ENTER_ANIM
				
				
				ActivityMain.activityAnimSequence.scenePosition = ActivityMain.activityCheck.vScenePos
				ActivityMain.activityAnimSequence.sceneOrientation = ActivityMain.activityCheck.vSceneRot 
				
				IF ActivityMain.activityAnimSequence.bMaintainChairScenePosition
					IF DOES_ENTITY_EXIST(ActivityMain.activityAnimSequence.activitySyncSceneEntity[0].EntityIndex)
						ActivityMain.activityAnimSequence.scenePosition 	= GET_ENTITY_COORDS(ActivityMain.activityAnimSequence.activitySyncSceneEntity[0].EntityIndex)
						ActivityMain.activityAnimSequence.sceneOrientation  = GET_ENTITY_ROTATION(ActivityMain.activityAnimSequence.activitySyncSceneEntity[0].EntityIndex)

	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "RUN_CURRENT_ANIM_SEQUENCE: bMaintainChairScenePosition = TRUE: scenePosition: ", ActivityMain.activityAnimSequence.scenePosition)
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "RUN_CURRENT_ANIM_SEQUENCE: bMaintainChairScenePosition = TRUE: sceneOrientation: ", ActivityMain.activityAnimSequence.sceneOrientation)
					ELSE
						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "RUN_CURRENT_ANIM_SEQUENCE: DOES_ENTITY_EXIST = FALSE : activitySyncSceneEntity[0].EntityIndex")
					ENDIF
				ENDIF
				
				IF (ActivityMain.eActivity = RADIO_ENUM_3)
					CPRINTLN(DEBUG_NET_ACTIVITY_CREATOR, "RUN_CURRENT_ANIM_SEQUENCE: RADIO_ENUM_3 rot ", ActivityMain.activityAnimSequence.sceneOrientation)
//					ActivityMain.activityCheck.vSceneRot												= <<0,0,180.0>>-<<0,0,-10>>
//					ActivityMain.activityAnimSequence.sceneOrientation									= ActivityMain.activityCheck.vSceneRot
				ENDIF
				IF (ActivityMain.eActivity = RADIO_ENUM_4)
					CPRINTLN(DEBUG_NET_ACTIVITY_CREATOR, "RUN_CURRENT_ANIM_SEQUENCE: RADIO_ENUM_4 rot ", ActivityMain.activityAnimSequence.sceneOrientation)
//					ActivityMain.activityCheck.vSceneRot												= <<0,0,180.0>>+<<0,0,-10>>
//					ActivityMain.activityAnimSequence.sceneOrientation									= ActivityMain.activityCheck.vSceneRot
				ENDIF
				
				IF CALL ActivityMain.GET_ASSETS_FOR_ACTIVITY(ActivityMain.activityAnimSequence, ActivityMain.oiSceneObjects, ActivityMain.getObjectsStruct, ActivityMain.piScenePeds)
//					SET_GRABBED_OBJECTS_AS_SCENE_OBJECTS(ActivityMain.oiSceneObjects, ActivityMain.animState[ActivityMain.iCurrentAnimState].activityAnimSequence[ActivityMain.iCurrentAnimSequenceID].activitySyncSceneEntity)
					IF RUN_CURRENT_ANIM_SEQUENCE(ActivityMain)						
						SetActStage(ActivityMain, ACTIVITY_STAGE_CLEANUP)
					ENDIF
					// has to call after for cleanup
					MAINTAIN_PROPS_VISIBILITY_SWAP(ActivityMain)
				ELSE
				ENDIF
				
			BREAK
			
			CASE ACTIVITY_STAGE_CHECK_ENTER_OR_IDLE_ANIM
				
	//			IF DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
	//			AND HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("CREATE_REMOTE"))
	//			AND IS_ANIM_DICT_SEAT_GAME()
	//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
	//					SET_ENTITY_VISIBLE(NET_TO_ENT(niOfficeSetTopRemote), TRUE)
	//				ENDIF
	//			ENDIF
	//			IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("DESTROY_REMOTE"))
	//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CHECK_ENTER_OR_IDLE_ANIM: DESTROY_REMOTE fired, making remote invisible")	
	//					SET_ENTITY_VISIBLE(NET_TO_ENT(niOfficeSetTopRemote), FALSE)
	//				ENDIF
	//			ENDIF
	//			// the remote isn't create and destroyed for the clips, they are created while the player is sitting and destroyed when they leave
	////			IF NOT DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
	////			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
	////				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CHECK_ENTER_OR_IDLE_ANIM: DELETE_NET_ID(niOfficeSetTopRemote)")
	////				DELETE_NET_ID(niOfficeSetTopRemote)
	////			ENDIF
	//			
	//			INT localIdleScene
	//			localIdleScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
	//
	//			IF localIdleScene != -1
	//				
	//				IF GET_SYNCHRONIZED_SCENE_PHASE(localIdleScene) >= 1 // Has the idle or enter anim finished yet
	//						
	//					iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
	//					IF bOfficeSeatTurning
	//						bOfficeSeatTurning = FALSE
	//					ENDIF
	//					#IF IS_DEBUG_BUILD
	//					IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExecOfficeArmchairExpandPedCapsule")
	//						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "base", SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON,  RBF_PLAYER_IMPACT,DEFAULT)
	//					ELSE
	//					#ENDIF
	//						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "base", SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE,  RBF_PLAYER_IMPACT,DEFAULT)
	//					#IF IS_DEBUG_BUILD
	//					ENDIF
	//					#ENDIF
	//					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
	//					AND DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
	//					AND IS_ANIM_DICT_SEAT_GAME()
	//						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(niOfficeSetTopRemote), iJacuzziNetSceneid, sOfficeSeatAnimDict, "base_remote", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START)
	//					ENDIF
	//					
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "POD: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", ANIM_NAME: base")
	//
	//					NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
	//
	//					
	//					
	//					SetActStage(ActivityMain, ACTIVITY_STAGE_CHECK_BASE_ANIM)
	//					
	//				ENDIF
	//			ENDIF		
				
				
			BREAK
			
			CASE ACTIVITY_STAGE_CHECK_BASE_ANIM 
	//			INT localScene
	//			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
	//			IF localScene != -1
	//				
	//				
	//				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 1
	//				
	//										
	//					iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
	//					#IF IS_DEBUG_BUILD
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
	//					#ENDIF
	//					
	//					
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CHECK_BASE_ANIM: Going to play idle anim")
	//										
	//					
	//					iIdleOfficeSeatClipVar = GENERATE_RANDOM_ACTIVITY_STAGE_ARMCHAIR_IDLE_CLIP()
	//					
	//					#IF IS_DEBUG_BUILD
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "POD: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", ANIM_NAME: ", GET_ACTIVITY_STAGE_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar))
	//					#ENDIF					
	//					#IF IS_DEBUG_BUILD
	//					IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExecOfficeArmchairExpandPedCapsule")
	//						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_ACTIVITY_STAGE_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar), SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON,  RBF_PLAYER_IMPACT,DEFAULT)
	//					ELSE
	//					#ENDIF
	//						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_ACTIVITY_STAGE_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar), SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE,  RBF_PLAYER_IMPACT,DEFAULT)
	//					#IF IS_DEBUG_BUILD
	//					ENDIF
	//					#ENDIF
	//					
	//					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
	//					AND DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
	//					AND IS_ANIM_DICT_SEAT_GAME()
	//						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CHECK_BASE_ANIM: sOfficeSeatAnimDict = ", sOfficeSeatAnimDict, ", GET_OFFICE_REMOTE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT = ", GET_OFFICE_REMOTE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar), ", eAnimOfficeSeatVar = ", eAnimOfficeSeatVar )
	//						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(niOfficeSetTopRemote), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_OFFICE_REMOTE_SEAT_PED_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START)
	//					ENDIF
	//					
	//					
	////					IF IS_ACTIVITY_STAGE_BOARDROOM_BOSS(iPlayerChosenSeatSlot)
	////					AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_REVOLVER)
	////						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NETWORK_GET_ENTITY_FROM_NETWORK_ID(niOfficeSeatRevolver), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_ACTIVITY_STAGE_GUN_IDLE_CLIP_STRING_FROM_INT(iIdleOfficeSeatClipVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS)
	////					ENDIF
	//					
	//					NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
	//					
	//
	//					SetActStage(ActivityMain, ACTIVITY_STAGE_CHECK_ENTER_OR_IDLE_ANIM)
	//					
	//				ENDIF
	//				
	//			ENDIF
				
			BREAK
			
			
			
			CASE ACTIVITY_STAGE_PLAY_COMPUTER_EXIT
	//			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
	//			IF localScene != -1
	//			
	//
	//				
	//				#IF IS_DEBUG_BUILD
	//				VECTOR vRotation 
	//				vRotation = GET_ENTITY_ROTATION(objOfficeSeat)
	//				VECTOR vCoords 
	//				vCoords = GET_ENTITY_COORDS(objOfficeSeat)
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CHECK_BASE_ANIM: GET_ENTITY_COORDS(objOfficeSeat) = ", vCoords, ", GET_ENTITY_ROTATION(objOfficeSeat) = ", vRotation)
	//				#ENDIF
	//				
	//				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
	//				#IF IS_DEBUG_BUILD
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
	//				#ENDIF
	//				
	//				
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CHECK_BASE_ANIM: Going to play computer idle anim")			
	//				
	//				
	//				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "COMPUTER_EXIT", SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE ,  DEFAULT,DEFAULT,AIK_DISABLE_HEAD_IK)
	//				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(objOfficeSeat, iJacuzziNetSceneid, sOfficeSeatAnimDict, "COMPUTER_EXIT_CHAIR", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS)
	//				
	//				
	//				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
	//				#IF IS_DEBUG_BUILD
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "POD: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", ANIM_NAME: COMPUTER_EXIT")
	//				#ENDIF
	//				
	//				
	//				IF g_bSecuroQuickExitOfficeChair = TRUE
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CHECK_ENTER_OR_IDLE_ANIM: exiting because g_bSecuroQuickExitOfficeChair = TRUE")
	//					SetActStage(ActivityMain, ACTIVITY_STAGE_EXIT)
	//				ELSE
	//					SetActStage(ActivityMain, ACTIVITY_STAGE_CHECK_ENTER_OR_IDLE_ANIM)
	//				ENDIF
	//			ENDIF
					
				
			BREAK
			
			CASE ACTIVITY_STAGE_EXIT_TURN

	//			iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
	//			#IF IS_DEBUG_BUILD
	//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
	//			#ENDIF
	//																																															
	//			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE , RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
	//			
	//			NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
	////			IF IS_ACTIVITY_STAGE_EXIT_PROMPT_SHOWN()
	////			OR IS_ACTIVITY_STAGE_COMPUTER_EXIT_PROMPT_SHOWN()
	////				CLEAR_HELP()
	////			ENDIF
	//
	//			CLEAR_BIT(iOfficeSeatiBitSet, ACTIVITY_STAGE_BS_EXIT_HELP_TEXT_DISPLAYED)
	//			
	//				
	//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_PLAYER_INTPUT_LOGIC: Cancel was pressed by crew boardroom member, while turned")
	//			
	//			SetActStage(ActivityMain, ACTIVITY_STAGE_CHECK_ENTER_OR_IDLE_ANIM)
					
				
			BREAK
			
			CASE ACTIVITY_STAGE_SWITCH_TO_GAME_ANIM
	//			REQUEST_ANIM_DICT(sOfficeSeatAnimDict)
	//			
	//			
	//			
	//			IF HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
	//			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
	//				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
	//				#IF IS_DEBUG_BUILD
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
	//				#ENDIF
	//																																																
	//				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_SEAT_ENTER_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE , RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
	//				#IF IS_DEBUG_BUILD
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SWITCH_FROM_GAME_ANIM_TO_SEAT: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", GET_SEAT_ENTER_TO_GAME_ANIM_CLIP: ", GET_SEAT_ENTER_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), ", eAnimOfficeSeatVar = ", eAnimOfficeSeatVar)
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SWITCH_FROM_GAME_ANIM_TO_SEAT: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", GET_REMOTE_SEAT_ENTER_TO_GAME_ANIM_CLIP: ", GET_REMOTE_SEAT_ENTER_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), ", eAnimOfficeSeatVar = ", eAnimOfficeSeatVar)
	//				#ENDIF
	//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
	//				AND DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
	//				AND IS_ANIM_DICT_SEAT_GAME()
	//					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(niOfficeSetTopRemote), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_REMOTE_SEAT_ENTER_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START)
	//				ENDIF
	//				
	//				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
	//
	//
	//				CLEAR_BIT(iOfficeSeatiBitSet, ACTIVITY_STAGE_BS_EXIT_HELP_TEXT_DISPLAYED)
	//				
	//				// We now switch anim var, from seat specific D/E/A to game specific A/B/C 
	//				eAnimOfficeSeatVar = SWITCH_TO_GAME_ANIM_VAR(iPlayerChosenSeatSlot)
	//				
	//				
	//				SetActStage(ActivityMain, ACTIVITY_STAGE_CHECK_ENTER_OR_IDLE_ANIM)
	//			ELSE
	//				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SWITCH_TO_GAME_ANIM: niOfficeSetTopRemote not created")
	//				ENDIF
	//				IF HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SWITCH_TO_GAME_ANIM: sOfficeSeatAnimDict: ", sOfficeSeatAnimDict, ", not loaded")
	//				ENDIF
	//			ENDIF
				
			BREAK
			
			CASE ACTIVITY_STAGE_PLAY_GAME_STANDING_ENTER
	//			REQUEST_ANIM_DICT(sOfficeSeatAnimDict)
	//
	//			IF HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
	//			AND CREATE_GAME_CONTROLLER()
	//				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_ROTATION(PLAYER_PED_ID()), DEFAULT, TRUE, FALSE)
	//				#IF IS_DEBUG_BUILD
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
	//				#ENDIF
	//																																																
	//				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "ENTER", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_BLOCK_MOVER_UPDATE, RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
	//				#IF IS_DEBUG_BUILD
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SWITCH_FROM_GAME_ANIM_TO_SEAT: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", GET_SEAT_ENTER_TO_GAME_ANIM_CLIP: ENTER, eAnimOfficeSeatVar = ", eAnimOfficeSeatVar)
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SWITCH_FROM_GAME_ANIM_TO_SEAT: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", GET_REMOTE_SEAT_ENTER_TO_GAME_ANIM_CLIP: ENTER_REMOTE eAnimOfficeSeatVar = ", eAnimOfficeSeatVar)
	//				#ENDIF
	//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
	//				AND DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_ID())
	//					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(niOfficeSetTopRemote), iJacuzziNetSceneid, sOfficeSeatAnimDict, "ENTER_REMOTE", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START | SYNCED_SCENE_BLOCK_MOVER_UPDATE)
	//				ENDIF
	//				
	//				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
	//
	//
	//				CLEAR_BIT(iOfficeSeatiBitSet, ACTIVITY_STAGE_BS_EXIT_HELP_TEXT_DISPLAYED)
	//				
	//				eAnimOfficeSeatVar = VAR_STANDING
	//				
	//				
	//				SetActStage(ActivityMain, ACTIVITY_STAGE_CHECK_ENTER_OR_IDLE_ANIM)
	//			ELSE
	//				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SWITCH_TO_GAME_ANIM: niOfficeSetTopRemote not created")
	//				ENDIF
	//				IF HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SWITCH_TO_GAME_ANIM: sOfficeSeatAnimDict: ", sOfficeSeatAnimDict, ", not loaded")
	//				ENDIF
	//			ENDIF
			BREAK
			
			
			CASE ACTIVITY_STAGE_SWITCH_FROM_GAME_ANIM_TO_SEAT
	//			REQUEST_ANIM_DICT(sOfficeSeatAnimDict)
	//			
	//			
	//			
	//			IF HAS_ANIM_DICT_LOADED(sOfficeSeatAnimDict)
	//				iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
	//				#IF IS_DEBUG_BUILD
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid)
	//				#ENDIF
	//				
	//				#IF IS_DEBUG_BUILD
	//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_SWITCH_FROM_GAME_ANIM_TO_SEAT: PLAYER added to Net Sync Scene, ANIM_DICT: ", sOfficeSeatAnimDict,", GET_SEAT_EXIT_TO_GAME_ANIM_CLIP: ", GET_SEAT_EXIT_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), ", eAnimOfficeSeatVar = ", eAnimOfficeSeatVar)
	//				#ENDIF
	//				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_SEAT_EXIT_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE , RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
	//				
	//				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niOfficeSetTopRemote)
	//
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid, ", GET_REMOTE_SEAT_EXIT_TO_GAME_ANIM_CLIP() = ", GET_REMOTE_SEAT_EXIT_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar))
	//					
	//					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_ENT(niOfficeSetTopRemote), iJacuzziNetSceneid, sOfficeSeatAnimDict, GET_REMOTE_SEAT_EXIT_TO_GAME_ANIM_CLIP(eAnimOfficeSeatVar), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START)
	//				ENDIF
	//				
	//				NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
	//	//			IF IS_ACTIVITY_STAGE_EXIT_PROMPT_SHOWN()
	//	//			OR IS_ACTIVITY_STAGE_COMPUTER_EXIT_PROMPT_SHOWN()
	//	//				CLEAR_HELP()
	//	//			ENDIF
	//
	//				CLEAR_BIT(iOfficeSeatiBitSet, ACTIVITY_STAGE_BS_EXIT_HELP_TEXT_DISPLAYED)
	//				
	//					
	//				
	//				// now we update the anim dict, as the correct exit from the game anim has been performed and now we switch to the seated anim dict
	//				sOfficeSeatAnimDict = GET_ACTIVITY_STAGE_ARMCHAIR_ENTER_ANIM_DICT(eAnimOfficeSeatVar, IS_PLAYER_FEMALE()) // Must change the anim dict to the seat one now.
	//				SetActStage(ActivityMain, ACTIVITY_STAGE_CHECK_ENTER_OR_IDLE_ANIM)
	//			ENDIF
			BREAK
			
			CASE ACTIVITY_STAGE_EXIT
	////			INT localScene
	//			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
	//			IF localScene != -1
	//				
	//				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 1
	//				OR NOT HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BLOCK_INTERRUPT"))
	//					IF NOT HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BLOCK_INTERRUPT"))
	//						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_EXIT: BLOCK_INTERRUPT not found, exiting from anim early")
	//					ENDIF
	//					iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
	//					#IF IS_DEBUG_BUILD
	////					STRING 
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "POD: CREATE net sync scene, SCENE_ID: " , iJacuzziNetSceneid, ", GET_CENTER_BASE_ANIM() = ", GET_CENTER_BASE_ANIM())
	//					#ENDIF
	//																																																	
	//					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, GET_CENTER_BASE_ANIM(), "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE , DEFAULT,INSTANT_BLEND_IN)
	//					NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(objOfficeSeat, iJacuzziNetSceneid, sOfficeSeatAnimDict, "exit_CHAIR", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_NET_ON_EARLY_NON_PED_STOP_RETURN_TO_START| SYNCED_SCENE_USE_PHYSICS)
	//					
	//					NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
	//					
	//					//IF IS_OFFICE_ARMCHAIR_EXIT_PROMPT_SHOWN()
	//						//CLEAR_HELP()
	//						IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
	//							RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
	//						ENDIF
	//					//ENDIF
	//
	//					CLEAR_BIT(iOfficeSeatiBitSet, ACTIVITY_STAGE_BS_EXIT_HELP_TEXT_DISPLAYED)
	//					
	//					SetActStage(ActivityMain, ACTIVITY_STAGE_CLEANUP)	
	//				ENDIF
	//			ENDIF
			BREAK
			
			CASE ACTIVITY_STAGE_EXIT_INTERRUPT
	//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CLEANUP: NETWORK_STOP_SYNCHRONISED_SCENE")
	//			NETWORK_STOP_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
	//			SetActStage(ActivityMain, ACTIVITY_STAGE_PUT_ON_CLOTHES)	
	//			ACTIVITY_STAGE_ACTIVITY_CLEAN_UP()
			BREAK
			
			CASE ACTIVITY_STAGE_TV_EXIT_SEAT
	//			iJacuzziNetSceneid = NETWORK_CREATE_SYNCHRONISED_SCENE(mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vLoc, mpOffsetOfficeSeats[iPlayerChosenSeatSlot].vRot, DEFAULT, TRUE, FALSE)
	//					
	//				
	//			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iJacuzziNetSceneid, sOfficeSeatAnimDict, "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_USE_PHYSICS |SYNCED_SCENE_ON_ABORT_STOP_SCENE ,  RBF_PLAYER_IMPACT,INSTANT_BLEND_IN)
	//			
	//					
	//			NETWORK_START_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
	//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "iJacuzziNetSceneid = ", iJacuzziNetSceneid)
	//			
	//			//IF IS_OFFICE_ARMCHAIR_EXIT_PROMPT_SHOWN()
	////				CLEAR_HELP()
	//				IF iOfficeSeatContextIntention != NEW_CONTEXT_INTENTION
	//					RELEASE_CONTEXT_INTENTION(iOfficeSeatContextIntention)
	//				ENDIF
	//			//ENDIF
	//			CLEAR_BIT(iOfficeSeatiBitSet, ACTIVITY_STAGE_BS_EXIT_HELP_TEXT_DISPLAYED)
	//
	//			CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "exiting activity")
	//			SetActStage(ActivityMain, ACTIVITY_STAGE_CLEANUP)
			BREAK
			
			CASE ACTIVITY_STAGE_CLEANUP
				
				IF ARE_STRINGS_EQUAL(ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText, "MP_HTRUCK_T_1e")
					IF IS_HACKER_TRUCK_DRONE_COOLDOWN_TEXT_DISPLAYING()
						CLEAR_HELP()
					ENDIF
				#IF FEATURE_HEIST_ISLAND
				ELIF ARE_STRINGS_EQUAL(ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText, "MP_RGSUBM_T_1e")
					IF IS_SUBMARINE_GUIDED_MISSILE_COOLDOWN_TEXT_DISPLAYING(1)
					OR IS_SUBMARINE_GUIDED_MISSILE_COOLDOWN_TEXT_DISPLAYING(2)
						CLEAR_HELP()
					ENDIF
				#ENDIF
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText)
					AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText)
						CLEAR_HELP_TEXT_GENERIC(ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct)
					ENDIF
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(ActivityMain.activityAnimSequence.helpTextStruct.helpText)
				AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(ActivityMain.activityAnimSequence.helpTextStruct.helpText)
					CLEAR_HELP_TEXT_GENERIC(ActivityMain.activityAnimSequence.helpTextStruct)
				ENDIF
				
				IF g_bTurretFadeOut
					IF NOT IS_PLAYER_LEAVING_SIMPLE_INTERIOR()
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ENDIF
						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CLEANUP: g_bTurretFadeOut: TRUE, screen fading in ")
					ELSE	
						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CLEANUP: g_bTurretFadeOut: TRUE, But IS_PLAYER_LEAVING_SIMPLE_INTERIOR is true so skipping fadein")
					ENDIF
				ENDIF
				
				
				
				g_bPlayerInActivityArea = FALSE
				ActivityMain.iCurrentAnimSequenceID = 0
				
				IF g_bDrinkingActivityRunning
					g_bDrinkingActivityRunning = FALSE
				ELIF g_bWeedActivityRunning
					g_bWeedActivityRunning = FALSE
				ENDIF
				REAPPLY_CONFLICTING_CLOTHING(ActivityMain)
				
	//			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iJacuzziNetSceneid)
	//			IF localScene != -1
	//				
	//
	//				
	//				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 1
	//				OR HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAKOUT_FINISH"))
	//				OR HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAK_OUT"))
	//					#IF IS_DEBUG_BUILD
	//					IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAKOUT_FINISH"))
	//						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CLEANUP: Finishing anim early as BREAKOUT_FINISH anim event fired")
	//					ENDIF
	//					IF HAS_ANIM_EVENT_FIRED (PLAYER_PED_ID(), HASH("BREAK_OUT"))
	//						CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CLEANUP: Finishing anim early as BREAK_OUT anim event fired")
	//					ENDIF
	//					#ENDIF 
	//					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CLEANUP: NETWORK_STOP_SYNCHRONISED_SCENE")
	//					NETWORK_STOP_SYNCHRONISED_SCENE(iJacuzziNetSceneid)
	//					SetActStage(ActivityMain, ACTIVITY_STAGE_PUT_ON_CLOTHES)	
	//					ACTIVITY_STAGE_ACTIVITY_CLEAN_UP()
	//				ENDIF
	//			ELSE
	////				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CLEANUP: Scene isn't running cleanup anyway")
	////				SetActStage(ActivityMain, ACTIVITY_STAGE_LOCATECHECK)
	////				ACTIVITY_STAGE_ACTIVITY_CLEAN_UP()
	//			ENDIF
	//			
	////			playerBD[NATIVE_TO_INT(PLAYER_ID())].iJacuzziSeatUsed = -1

				SWITCH ActivityMain.eActivity
					CASE ACT_CREATOR_BEER_1
					CASE ACT_CREATOR_BEER_2
					CASE ACT_CREATOR_BEER_3
					CASE ACT_CREATOR_WHISKEY_VARA_1
						SEND_ACTIVITY_TELEMETRY_DATA(ActivityMain.eActivity, 2)
					BREAK
					CASE NEW_BONG_ENUM
						SEND_ACTIVITY_TELEMETRY_DATA(ActivityMain.eActivity, 3)
					BREAK
				ENDSWITCH
				
				IF g_iShouldLaunchTruckTurret != -1
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ACTIVITY_STAGE_CLEANUP: g_iShouldLaunchTruckTurret was ", g_iShouldLaunchTruckTurret, ", resetting")
					g_iShouldLaunchTruckTurret = -1
				ENDIF
				
				IF g_iInteriorTurretSeat != -1
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ACTIVITY_STAGE_CLEANUP: g_iInteriorTurretSeat  was ", g_iInteriorTurretSeat , ", resetting")
					g_iInteriorTurretSeat = -1
				ENDIF
				
				#IF FEATURE_HEIST_ISLAND
				IF g_iShouldLaunchSubmarineSeat != -1
					g_iShouldLaunchSubmarineSeat = -1
				ENDIF
				#ENDIF
				
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iTurretSeat != -1
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ACTIVITY_STAGE_CLEANUP: GlobalplayerBD_FM.iTurretSeat  was ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iTurretSeat, ", resetting")
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iTurretSeat = -1
				ENDIF
				
				gPlayerCreatorActivity = NULL_ACTIVITY
				
				ENABLE_INTERACTION_MENU()
				
//				CLEANUP_ACTIVITY_PEDS_AND_PROPS(ActivityMain)
				SetActStage(ActivityMain, ACTIVITY_STAGE_SHOULD_LAUNCH)
				IF ActivityMain.activityCheck.bScriptLaunch = TRUE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_TOO_MANY")
						CLEAR_HELP()
					ENDIF
					
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_CLEANUP: TERMINATE_THIS_THREAD")
					TERMINATE_THIS_THREAD()
				ENDIF
			BREAK
			
			CASE ACTIVITY_STAGE_BUNKER_CLEANUP
				
				IF NOT IS_STRING_NULL_OR_EMPTY(ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText)
				AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText)
					CLEAR_HELP_TEXT_GENERIC(ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct)
				ENDIF
				
				g_bPlayerInActivityArea = FALSE
				ActivityMain.iCurrentAnimSequenceID = 0
				
				IF g_bDrinkingActivityRunning
					g_bDrinkingActivityRunning = FALSE
				ELIF g_bWeedActivityRunning
					g_bWeedActivityRunning = FALSE
				ENDIF
				
				
				SWITCH ActivityMain.eActivity
					CASE ACT_CREATOR_BEER_1
					CASE ACT_CREATOR_BEER_2
					CASE ACT_CREATOR_BEER_3
					CASE ACT_CREATOR_WHISKEY_VARA_1
						SEND_ACTIVITY_TELEMETRY_DATA(ActivityMain.eActivity, 2)
					BREAK
					CASE NEW_BONG_ENUM
						SEND_ACTIVITY_TELEMETRY_DATA(ActivityMain.eActivity, 3)
					BREAK
				ENDSWITCH
				
				IF g_iShouldLaunchTruckTurret != -1
					CDEBUG1LN(DEBUG_NET_GUN_TURRET, "ACTIVITY_STAGE_BUNKER_CLEANUP: g_iShouldLaunchTruckTurret was ", g_iShouldLaunchTruckTurret, ", resetting")
					g_iShouldLaunchTruckTurret = -1
				ENDIF
				
				#IF FEATURE_HEIST_ISLAND
				IF g_iShouldLaunchSubmarineSeat != -1
					g_iShouldLaunchSubmarineSeat = -1
				ENDIF	
				#ENDIF
				
//				CLEANUP_ACTIVITY_PEDS_AND_PROPS(ActivityMain)
				SetActStage(ActivityMain, ACTIVITY_STAGE_SHOULD_LAUNCH)
				IF ActivityMain.activityCheck.bScriptLaunch = TRUE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("POD_TOO_MANY")
						CLEAR_HELP()
					ENDIF
					
					CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACTIVITY_STAGE_BUNKER_CLEANUP: TERMINATE_THIS_THREAD, for activity ", DEBUG_GET_MP_PROP_ACT_NAME(ActivityMain.eActivity)," id = ", ActivityMain.activityCheck.iScriptInstanceID, " we are running ", GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("net_apartment_activity")), " net_apartment_activity scripts")
					TERMINATE_THIS_THREAD()
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF

ENDPROC



