// Name: 		net_kill_strip_helper.sch
// Author: 		James Adwick
// Date: 		26/06/2012
// Purpose: 	Helper header file for scripts wanting detais of killer / victim

USING "globals.sch"
USING "NET_SCORING_Common.sch"
USING "net_stat_system.sch"
USING "transition_crews.sch"
USING "FMMC_MP_In_Game_Menu.sch"

// -----------------------------------------------------------
//               HELPER FUNCTIONS FOR KILL STRIPS
// Deathmatch, Freemode, Races etc should be able to use 
// these to gather data about kill and possibly use scaleform 
//                    display methods below.
// -----------------------------------------------------------

PROC GET_DETAILS_FOR_KILLER_OF_PLAYER(KILLER_DATA_STRUCT &killerData, PLAYER_INDEX killerID)

	killerData.sKillerName = GET_PLAYER_NAME(killerID)
	killerData.killerID = killerID
	INT iTeam = GET_PLAYER_GLOBAL_TEAM(killerID)
	killerData.iKillerTeam = iTeam
	killerData.iKillerRank = GET_PLAYER_GLOBAL_RANK(killerID)
	
	INT iAlpha
	GET_HUD_COLOUR(HUD_COLOUR_RED, killerData.iKillerColourR, killerData.iKillerColourG, killerData.iKillerColourB, iAlpha)
		
	killerData.iPlayerKilledKiller = MPGlobalsEvents.number_times_killed_by_player[NATIVE_TO_INT(killerID)]	// Confusing wording (player here is referring to local player)
	killerData.iKilledByKiller = MPGlobalsEvents.number_times_killed_player[NATIVE_TO_INT(killerID)]
ENDPROC

PROC REFRESH_KILL_V_DEATH_VALUES(KILLER_DATA_STRUCT &killerData)
	
	IF IS_NET_PLAYER_OK(killerData.killerID, FALSE)
	
		#IF IS_DEBUG_BUILD
			IF killerData.iPlayerKilledKiller != MPGlobalsEvents.number_times_killed_by_player[NATIVE_TO_INT(killerData.killerID)]
			OR killerData.iKilledByKiller != MPGlobalsEvents.number_times_killed_player[NATIVE_TO_INT(killerData.killerID)]
				NET_PRINT("[JA@KILLSTRIP] REFRESH_KILL_V_DEATH_VALUES - refreshing values as we have had an update") NET_NL()
			ENDIF
		#ENDIF
	
		killerData.iPlayerKilledKiller = MPGlobalsEvents.number_times_killed_by_player[NATIVE_TO_INT(killerData.killerID)]
		killerData.iKilledByKiller = MPGlobalsEvents.number_times_killed_player[NATIVE_TO_INT(killerData.killerID)]
	ENDIF	
	
ENDPROC



/// PURPOSE: Fill out crew info data for player passed
PROC GET_PLAYERS_CREW_INFO(PLAYER_CREW_DATA_STRUCT& crewData, PLAYER_INDEX playerID)
	
	GAMER_HANDLE aGamerHandle = GET_GAMER_HANDLE_PLAYER(playerID)
	IF IS_GAMER_HANDLE_VALID(aGamerHandle)
		IF IS_PLAYER_IN_ACTIVE_CLAN(aGamerHandle)
		
			#IF IS_DEBUG_BUILD
				NET_PRINT("[JA@KILLSTRIP] Player ") NET_PRINT(GET_PLAYER_NAME(playerID))
				NET_PRINT(" is in a crew") NET_NL()
			#ENDIF
		
			NETWORK_CLAN_DESC clanDesc
			NETWORK_CLAN_PLAYER_GET_DESC(clanDesc, SIZE_OF(clanDesc), aGamerHandle)
		
			crewData.bIsActive 	= TRUE
			crewData.crewTag	= GET_PLAYER_CLAN_TAG(aGamerHandle)
			crewData.crewName	= GET_PLAYER_CLAN_NAME(aGamerHandle)
			crewData.privateCrew = IS_PLAYER_CLAN_PRIVATE(aGamerHandle)
			crewData.rockstarCrew = IS_CREW_A_ROCKSTAR_CREW(clanDesc)
			crewData.crewFounder = IS_PLAYER_CLAN_FOUNDER(aGamerHandle)
			
			// Begin requesting the crew emblem
			NETWORK_CLAN_GET_EMBLEM_TXD_NAME(aGamerHandle, crewData.crewEmblemTxd)
		ENDIF
	ENDIF
	
ENDPROC

//  This should loop through the bones of the player and store them in hitPoints.
PROC GET_PLAYER_HIT_POINTS(INT& hitPoints[])
	
	// Null the entire array
	INT i
	REPEAT 5 i
		MPGlobals.g_KillStrip.hitPoints[i] = -1
	ENDREPEAT
	
	// Check we have some hit points
	IF NETWORK_GET_NUMBER_BODY_TRACKER_HITS() <= 0
		NET_PRINT("[JA@KILLSTRIP]: No hit points detected") NET_NL()
		EXIT
	ENDIF
	
	// Fatal hit should always be the first point passed
	hitPoints[0] = NETWORK_GET_BONE_ID_OF_FATAL_HIT()
	
	NET_PRINT("[JA@KILLSTRIP]: Fatal kill was: ") NET_PRINT_INT(hitPoints[0]) NET_NL()
	
	// Handle if we don't get a valid 
	IF hitPoints[0] = -1
		NET_PRINT("[JA@KILLSTRIP]: NETWORK_GET_NUMBER_BODY_TRACKER_HITS was greater than 0 but we didn't have a fatal hit") NET_NL()
		EXIT
	ENDIF
	
	IF hitPoints[0] = ENUM_TO_INT(RAGDOLL_NUM_COMPONENTS)
		NET_PRINT("[JA@KILLSTRIP]: GET_PLAYER_HIT_POINTS - Fatal hit was execution") NET_NL()
		EXIT
	ENDIF
	
	// Loop through the values of the body and see if bone has been hit
	i = 0
	INT iNumBones = ENUM_TO_INT(RAGDOLL_NUM_COMPONENTS)			// This should be kept in sync with generic.sch PED_RAGDOLL_COMPONENTS 
	INT index = 1

	REPEAT iNumBones i
		IF NETWORK_HAS_BONE_BEEN_HIT_BY_KILLER(i)
		
			IF i != hitPoints[0]
			
				NET_PRINT("[JA@KILLSTRIP]: Bone has been hit: ") NET_PRINT_INT(i) NET_NL()
			
				hitPoints[index] = i
				index++
			ENDIF
			
			IF index = 5
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

// -----------------------------------------------------------
//  				SCALEFORM HELPER FUNCTIONS
// -----------------------------------------------------------

/// PURPOSE: Add killer info for scaleform movie passed.
PROC ADD_CREW_INFO_FOR_KILLER(SCALEFORM_INDEX movieID, PLAYER_CREW_DATA_STRUCT crewInfo)
	BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_CREW_TAG")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(crewInfo.privateCrew)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(crewInfo.rockstarCrew)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(crewInfo.crewTag)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(crewInfo.crewFounder)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE: Sets the colours of the 2 players involved in the kill
PROC SET_KILLER_VICTIM_COLOURS(SCALEFORM_INDEX movieID, KILLER_DATA_STRUCT iKillerData)

	INT iAlpha
	INT iVictimR
	INT iVictimG
	INT iVictimB
	
	GET_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(PLAYER_ID()), iVictimR, iVictimG, iVictimB, iAlpha)

	BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_KILLER_AND_VICTIM_COLOURS")			
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iKillerData.iKillerColourR)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iKillerData.iKillerColourG)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iKillerData.iKillerColourB)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVictimR)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVictimG)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVictimB)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SET_KILLER_INFO(SCALEFORM_INDEX movieID, TEXT_LABEL_63 tlKillerName, INT iKillerRank, INT iKillerScore, INT iVictimScore, INT iWeapon, INT iHudColour)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYER_INFO")
	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlKillerName)

		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iKillerRank)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iKillerScore)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVictimScore)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWeapon)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHudColour)
		
	END_SCALEFORM_MOVIE_METHOD()

	
ENDPROC

/// PURPOSE: Set up the ped headshot of a player
PROC SET_PLAYERS_HEADSHOT(SCALEFORM_INDEX movieID, STRING txdName)

	PRINTLN("SET_PLAYERS_HEADSHOT called with txdName: ", txdName)

	BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYERS_HEADSHOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(txdName)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE: Set up the ped headshot of a player
PROC SET_PLAYERS_CREW_EMBLEM(SCALEFORM_INDEX movieID, STRING txdName)

	PRINTLN("SET_PLAYERS_CREW_EMBLEM called...")

	BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_KILLERS_CREW_EMBLEM")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(txdName)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

// PURPOSE: Loop through kill strip hit points passing to scaleform
PROC SET_PLAYERS_BODY_HIT_POINTS(SCALEFORM_INDEX movieID, INT& hitPoints[])
	INT i
	BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "ADD_DAMAGE_TO_VICTIM")

		REPEAT 5 i
			IF hitPoints[i] != -1
			
				PRINTLN("[JA@KILLSTRIP] SET_PLAYERS_BODY_HIT_POINTS - passing hit point ", hitPoints[i])
			
				IF hitPoints[i] = ENUM_TO_INT(RAGDOLL_NUM_COMPONENTS)
					PRINTLN("[JA@KILLSTRIP] SET_PLAYERS_BODY_HIT_POINTS - overriding max component to -1")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(hitPoints[i])
				ENDIF
			ENDIF
		ENDREPEAT

	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE: Make passed in instructional button flash
PROC MAKE_INSTRUCTIONAL_BUTTON_FLASH(SCALEFORM_INDEX movieID, INT iButton, INT iAlpha = 30, FLOAT fTime = 0.2)
	IF HAS_SCALEFORM_MOVIE_LOADED(MPGlobals.g_KillStrip.buttonMovie)
		BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "FLASH_BUTTON_BY_ID")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iButton)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTime)
		END_SCALEFORM_MOVIE_METHOD()
		PRINTLN(" MAKE_INSTRUCTIONAL_BUTTON_FLASH - iButton = ", iButton, " iAlpha = ", iAlpha, " fTime = ", fTime)
	ELSE
		PRINTLN(" MAKE_INSTRUCTIONAL_BUTTON_FLASH - Button Movie hasn't yet loaded.")
	ENDIF
ENDPROC
