//////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_taxi_job_request.sch												//
// Description: Handles requesting Taxi jobs in freemode.								//
// Written by:  David Trenholme															//
// Date:        26/08/2022																//
//////////////////////////////////////////////////////////////////////////////////////////  

//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"

USING "net_gang_boss.sch"
USING "net_include.sch"

//----------------------
//	CONSTANTS
//----------------------
CONST_INT	TJR_REQUEST_COMPLETE_DELAY_MS		2500
CONST_INT	TJR_REQUEST_INPUT_DURATION_MAX_MS	250

//----------------------
//	ENUMS
//----------------------
ENUM TJR_STATE
	eTJRSTATE_INIT,
	eTJRSTATE_WAIT_FOR_REQUEST,
	eTJRSTATE_REQUEST_COMPLETE
ENDENUM

ENUM TJR_AVAILABILITY
	eTJRAVAILABILITY_AVAILABLE,
	eTJRAVAILABILITY_UNAVAILABLE_GANG_MEMBER,
	eTJRAVAILABILITY_UNAVAILABLE_ON_MISSION
ENDENUM

//----------------------
//	BITSETS
//----------------------
ENUM TJR_HELP_TEXT
	eTJRHELPTEXT_REQUEST_JOB,
	eTJRHELPTEXT_GANG_MEMBER,
	eTJRHELPTEXT_ON_MISSION
ENDENUM
CONST_INT	TJR_HELP_TEXT_BITSET_ARRAY_SIZE	(COUNT_OF(TJR_HELP_TEXT) + 31) / 32

ENUM TJR_LOCAL_BITSET
	eTJRLOCALBITSET_SHOULD_ENABLE_REQUEST,
	
	eTJRLOCALBITSET_END
ENDENUM
CONST_INT	TJR_LOCAL_BITSET_ARRAY_SIZE	(COUNT_OF(TJR_LOCAL_BITSET) + 30) / 32

//----------------------
//	STRUCTS
//----------------------
#IF IS_DEBUG_BUILD
STRUCT TJR_DEBUG_VARS_STRUCT
	BOOL bIgnoreIsValidTaxi
	BOOL bClearRequestHelpTextDone
ENDSTRUCT
#ENDIF

STRUCT TJR_VARS_STRUCT
	#IF IS_DEBUG_BUILD
	TJR_DEBUG_VARS_STRUCT sDebug
	#ENDIF
	
	TJR_STATE eState
	SCRIPT_TIMER timerRequest
	
	INT iBitSet[TJR_LOCAL_BITSET_ARRAY_SIZE]
	INT iHelpTextTriggeredBitSet[TJR_HELP_TEXT_BITSET_ARRAY_SIZE]
ENDSTRUCT

//----------------------
//	HELPER FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD
FUNC STRING TJR_GET_STATE_NAME(TJR_STATE eState)
	SWITCH eState
		CASE eTJRSTATE_INIT				RETURN "eTJRSTATE_INIT"
		CASE eTJRSTATE_WAIT_FOR_REQUEST	RETURN "eTJRSTATE_WAIT_FOR_REQUEST"
		CASE eTJRSTATE_REQUEST_COMPLETE	RETURN "eTJRSTATE_REQUEST_COMPLETE"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING TJR_GET_AVAILABILITY_NAME(TJR_AVAILABILITY eAvailability)
	SWITCH eAvailability
		CASE eTJRAVAILABILITY_AVAILABLE					RETURN "eTJRAVAILABILITY_AVAILABLE"
		CASE eTJRAVAILABILITY_UNAVAILABLE_GANG_MEMBER	RETURN "eTJRAVAILABILITY_UNAVAILABLE_GANG_MEMBER"
		CASE eTJRAVAILABILITY_UNAVAILABLE_ON_MISSION	RETURN "eTJRAVAILABILITY_UNAVAILABLE_ON_MISSION"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING TJR_GET_HELP_TEXT_NAME(TJR_HELP_TEXT eHelp)
	SWITCH eHelp
		CASE eTJRHELPTEXT_REQUEST_JOB	RETURN "eTJRHELPTEXT_REQUEST_JOB"
		CASE eTJRHELPTEXT_GANG_MEMBER	RETURN "eTJRHELPTEXT_GANG_MEMBER"
		CASE eTJRHELPTEXT_ON_MISSION	RETURN "eTJRHELPTEXT_ON_MISSION"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING TJR_GET_LOCAL_BIT_NAME(TJR_LOCAL_BITSET eBit)
	SWITCH eBit
		CASE eTJRLOCALBITSET_SHOULD_ENABLE_REQUEST	RETURN "eTJRLOCALBITSET_SHOULD_ENABLE_REQUEST"
		
		CASE eTJRLOCALBITSET_END					RETURN "eTJRLOCALBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC
#ENDIF

FUNC BOOL TJR_IS_LOCAL_BIT_SET(TJR_VARS_STRUCT& sTJRVarsStruct, TJR_LOCAL_BITSET eBit)
	RETURN IS_ARRAYED_BIT_ENUM_SET(sTJRVarsStruct.iBitSet, eBit)
ENDFUNC

PROC TJR_SET_LOCAL_BIT(TJR_VARS_STRUCT& sTJRVarsStruct, TJR_LOCAL_BITSET eBit #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF )
	IF SET_ARRAYED_BIT_ENUM(sTJRVarsStruct.iBitSet, eBit)
		#IF IS_DEBUG_BUILD
		IF NOT bBlockLogPrints
			PRINTLN("[TAXI_JOB_REQUEST] TJR_SET_LOCAL_BIT - ", TJR_GET_LOCAL_BIT_NAME(eBit))
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC TJR_CLEAR_LOCAL_BIT(TJR_VARS_STRUCT& sTJRVarsStruct, TJR_LOCAL_BITSET eBit #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF )
	IF CLEAR_ARRAYED_BIT_ENUM(sTJRVarsStruct.iBitSet, eBit)
		#IF IS_DEBUG_BUILD
		IF NOT bBlockLogPrints
			PRINTLN("[TAXI_JOB_REQUEST] TJR_CLEAR_LOCAL_BIT - ", TJR_GET_LOCAL_BIT_NAME(eBit))
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL TJR_IS_HELP_TEXT_TRIGGERED(TJR_VARS_STRUCT& sTJRVarsStruct, TJR_HELP_TEXT eHelp)
	RETURN IS_ARRAYED_BIT_ENUM_SET(sTJRVarsStruct.iHelpTextTriggeredBitSet, eHelp)
ENDFUNC

PROC TJR_TRIGGER_HELP_TEXT(TJR_VARS_STRUCT& sTJRVarsStruct, TJR_HELP_TEXT eHelp #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF )
	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
	AND NOT IS_ARRAYED_BIT_ENUM_SET(sTJRVarsStruct.iHelpTextTriggeredBitSet, eHelp)
		PRINTLN("[TAXI_JOB_REQUEST] TJR_TRIGGER_HELP_TEXT - ", TJR_GET_HELP_TEXT_NAME(eHelp))
	ENDIF
	#ENDIF
	
	SET_ARRAYED_BIT_ENUM(sTJRVarsStruct.iHelpTextTriggeredBitSet, eHelp)
ENDPROC

PROC TJR_CLEAR_HELP_TEXT_TRIGGERED(TJR_VARS_STRUCT& sTJRVarsStruct, TJR_HELP_TEXT eHelp #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF )
	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
	AND IS_ARRAYED_BIT_ENUM_SET(sTJRVarsStruct.iHelpTextTriggeredBitSet, eHelp)
		PRINTLN("[TAXI_JOB_REQUEST] TJR_CLEAR_HELP_TEXT_TRIGGERED - ", TJR_GET_HELP_TEXT_NAME(eHelp))
	ENDIF
	#ENDIF
	
	CLEAR_ARRAYED_BIT_ENUM(sTJRVarsStruct.iHelpTextTriggeredBitSet, eHelp)
ENDPROC

PROC TJR_RESET_HELP_TEXT_TRIGGERED(TJR_VARS_STRUCT& sTJRVarsStruct #IF IS_DEBUG_BUILD , BOOL bBlockLogPrints = FALSE #ENDIF )
	#IF IS_DEBUG_BUILD
	IF NOT bBlockLogPrints
	AND IS_ANY_ARRAYED_BIT_SET(sTJRVarsStruct.iHelpTextTriggeredBitSet)
		PRINTLN("[TAXI_JOB_REQUEST] TJR_CLEAR_ALL_HELP_TEXT_TRIGGERED")
	ENDIF
	#ENDIF
	
	RESET_ARRAYED_BIT_SET(sTJRVarsStruct.iHelpTextTriggeredBitSet)
ENDPROC

FUNC TJR_STATE TJR_GET_STATE(TJR_VARS_STRUCT& sTJRVarsStruct)
	RETURN sTJRVarsStruct.eState
ENDFUNC

PROC TJR_SET_STATE(TJR_VARS_STRUCT& sTJRVarsStruct, TJR_STATE eState)
	PRINTLN("[TAXI_JOB_REQUEST] TJR_SET_STATE - ", TJR_GET_STATE_NAME(TJR_GET_STATE(sTJRVarsStruct)), " -> ", TJR_GET_STATE_NAME(eState))
	
	sTJRVarsStruct.eState = eState
ENDPROC

FUNC BOOL TJR_IS_REQUEST_HELP_TEXT_DONE()
	RETURN g_TransitionSessionNonResetVars.sMagnateGangBossData.bTJRRequestHelpTextDone
ENDFUNC

PROC TJR_SET_REQUEST_HELP_TEXT_DONE()
	PRINTLN("[TAXI_JOB_REQUEST] TJR_SET_REQUEST_HELP_TEXT_DONE")
	DEBUG_PRINTCALLSTACK()
	
	g_TransitionSessionNonResetVars.sMagnateGangBossData.bTJRRequestHelpTextDone = TRUE
ENDPROC

PROC TJR_SET_TAXI_VEHICLE(VEHICLE_INDEX vehTaxi)
	IF MPGlobalsAmbience.sTaxiDriver.vehTaxi = vehTaxi
		EXIT
	ENDIF
	
	PRINTLN("[TAXI_JOB_REQUEST] TJR_SET_TAXI_VEHICLE - vehTaxi = ", NATIVE_TO_INT(vehTaxi))
	DEBUG_PRINTCALLSTACK()
	
	MPGlobalsAmbience.sTaxiDriver.vehTaxi = vehTaxi
ENDPROC

//----------------------
//	CLEANUP FUNCTIONS
//----------------------
FUNC BOOL TJR_SHOULD_CLEANUP(TJR_VARS_STRUCT& sTJRVarsStruct)
	SWITCH TJR_GET_STATE(sTJRVarsStruct)
		CASE eTJRSTATE_INIT
			RETURN FALSE
	ENDSWITCH
	
	IF NOT TJR_IS_LOCAL_BIT_SET(sTJRVarsStruct, eTJRLOCALBITSET_SHOULD_ENABLE_REQUEST)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC TJR_CLEANUP(TJR_VARS_STRUCT& sTJRVarsStruct)
	PRINTLN("[TAXI_JOB_REQUEST] TJR_CLEANUP")
	DEBUG_PRINTCALLSTACK()
	
	TJR_RESET_HELP_TEXT_TRIGGERED(sTJRVarsStruct)
	RESET_NET_TIMER(sTJRVarsStruct.timerRequest)
	
	TJR_SET_STATE(sTJRVarsStruct, eTJRSTATE_INIT)
ENDPROC

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD
PROC TJR_DEBUG_CLEAR_REQUEST_HELP_TEXT_DONE()
	PRINTLN("[TAXI_JOB_REQUEST] TJR_CLEAR_REQUEST_HELP_TEXT_DONE")
	DEBUG_PRINTCALLSTACK()
	
	g_TransitionSessionNonResetVars.sMagnateGangBossData.bTJRRequestHelpTextDone = FALSE
ENDPROC

PROC TJR_DEBUG_CREATE_WIDGETS(TJR_VARS_STRUCT& sTJRVarsStruct)
	PRINTLN("[TAXI_JOB_REQUEST] TJR_DEBUG_CREATE_WIDGETS")
	
	START_WIDGET_GROUP("Taxi Job Request")
		ADD_WIDGET_BOOL("Ignore Is Valid Taxi", sTJRVarsStruct.sDebug.bIgnoreIsValidTaxi)
		ADD_WIDGET_BOOL("Clear request help text done", sTJRVarsStruct.sDebug.bClearRequestHelpTextDone)
	STOP_WIDGET_GROUP()
ENDPROC

PROC TJR_DEBUG_MAINTAIN_DEBUG(TJR_VARS_STRUCT& sTJRVarsStruct)
	IF sTJRVarsStruct.sDebug.bClearRequestHelpTextDone
		PRINTLN("[TAXI_JOB_REQUEST] TJR_DEBUG_MAINTAIN_DEBUG - sTJRVarsStruct.sDebug.bClearRequestHelpTextDone = TRUE")
		
		sTJRVarsStruct.sDebug.bClearRequestHelpTextDone = FALSE
		
		TJR_DEBUG_CLEAR_REQUEST_HELP_TEXT_DONE()
	ENDIF
ENDPROC
#ENDIF

//----------------------
//	MAIN FUNCTIONS
//----------------------
FUNC BOOL TJR_IS_LOCAL_PLAYER_IN_VALID_TAXI(TJR_VARS_STRUCT& sTJRVarsStruct)
	UNUSED_PARAMETER(sTJRVarsStruct)
	
	IF NOT MPGlobals.sFreemodeCache.bIsPlayerInAnyVehicle
		RETURN FALSE
	ENDIF
	
	IF NOT MPGlobals.sFreemodeCache.bIsPlayerVehicleDrivable
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF sTJRVarsStruct.sDebug.bIgnoreIsValidTaxi
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL TJR_SHOULD_ENABLE_REQUEST(TJR_VARS_STRUCT& sTJRVarsStruct)
	IF NOT MPGlobals.sFreemodeCache.bLocalPlayerOk
		RETURN FALSE
	ENDIF
	
	IF NOT TJR_IS_LOCAL_PLAYER_IN_VALID_TAXI(sTJRVarsStruct)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(MPGlobals.sFreemodeCache.PlayerIndex)
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_LAUNCHING_ANY_GANG_BOSS_MISSION(MPGlobals.sFreemodeCache.PlayerIndex)
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_PLAYER_ACTIVE_IN_CORONA_PAST_TRANSITION_SESSION(MPGlobals.sFreemodeCache.PlayerIndex)
		RETURN FALSE
	ENDIF
	
	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(MPGlobals.sFreemodeCache.PlayerIndex, MPAM_TYPE_TIME_TRIAL)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_MP_PROPERTY(MPGlobals.sFreemodeCache.PlayerIndex, FALSE)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(MPGlobals.sFreemodeCache.PlayerIndex)
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
		RETURN FALSE
	ENDIF
	
	IF IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(MPGlobals.sFreemodeCache.PlayerIndex)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_SESSION_LAUNCHING()
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		RETURN FALSE
	ENDIF
	
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(MPGlobals.sFreemodeCache.PlayerIndex)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC TJR_MAINTAIN_SHOULD_ENABLE_REQUEST(TJR_VARS_STRUCT& sTJRVarsStruct)
	IF TJR_SHOULD_ENABLE_REQUEST(sTJRVarsStruct)
		TJR_SET_LOCAL_BIT(sTJRVarsStruct, eTJRLOCALBITSET_SHOULD_ENABLE_REQUEST)
	ELSE
		TJR_CLEAR_LOCAL_BIT(sTJRVarsStruct, eTJRLOCALBITSET_SHOULD_ENABLE_REQUEST)
	ENDIF
ENDPROC

FUNC BOOL TJR_SHOULD_MAINTAIN_HELP_TEXT(TJR_VARS_STRUCT& sTJRVarsStruct)
	SWITCH TJR_GET_STATE(sTJRVarsStruct)
		CASE eTJRSTATE_INIT
			RETURN FALSE
	ENDSWITCH
	
	IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	IF IS_RADAR_HIDDEN()
		RETURN FALSE
	ENDIF
	
	IF g_Show_Lap_Dpad
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC STRING TJR_GET_HELP_TEXT_LABEL(TJR_HELP_TEXT eHelp)
	SWITCH eHelp
		CASE eTJRHELPTEXT_REQUEST_JOB	RETURN "TAXI_H_REQ_JOB"
		CASE eTJRHELPTEXT_GANG_MEMBER	RETURN "TAXI_H_GANG_MEM"
		CASE eTJRHELPTEXT_ON_MISSION	RETURN "TAXI_H_ON_MISS"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC TJR_PRINT_HELP_TEXT(TJR_HELP_TEXT eHelp, INT iOverrideTime = -1)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(TJR_GET_HELP_TEXT_LABEL(eHelp))
		EXIT
	ENDIF
	
	PRINTLN("[TAXI_JOB_REQUEST] TJR_PRINT_HELP_TEXT - Printing help text eHelp = ", TJR_GET_HELP_TEXT_NAME(eHelp), " with label = ", TJR_GET_HELP_TEXT_LABEL(eHelp))
	
	SWITCH iOverrideTime
		CASE -2
			PRINT_HELP_FOREVER(TJR_GET_HELP_TEXT_LABEL(eHelp))
		BREAK
		
		DEFAULT
			PRINT_HELP(TJR_GET_HELP_TEXT_LABEL(eHelp), iOverrideTime)
		BREAK
	ENDSWITCH
ENDPROC

PROC TJR_CLEAR_HELP_TEXT()
	PRINTLN("[TAXI_JOB_REQUEST] TJR_CLEAR_HELP_TEXT")
	
	INT i
	REPEAT COUNT_OF(TJR_HELP_TEXT) i
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(TJR_GET_HELP_TEXT_LABEL(INT_TO_ENUM(TJR_HELP_TEXT, i)))
			CLEAR_HELP()
		ENDIF
	ENDREPEAT
ENDPROC

PROC TJR_MAINTAIN_HELP_TEXT(TJR_VARS_STRUCT& sTJRVarsStruct)
	IF NOT TJR_SHOULD_MAINTAIN_HELP_TEXT(sTJRVarsStruct)
		EXIT
	ENDIF
	
	INT i
	REPEAT COUNT_OF(TJR_HELP_TEXT) i
		TJR_HELP_TEXT eHelp = INT_TO_ENUM(TJR_HELP_TEXT, i)
		
		IF TJR_IS_HELP_TEXT_TRIGGERED(sTJRVarsStruct, eHelp)
			TJR_PRINT_HELP_TEXT(eHelp)
			TJR_CLEAR_HELP_TEXT_TRIGGERED(sTJRVarsStruct, eHelp)
			
			BREAKLOOP
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL TJR_HAS_LOCAL_PLAYER_TRIGGERED_REQUEST(TJR_VARS_STRUCT& sTJRVarsStruct)
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
		REINIT_NET_TIMER(sTJRVarsStruct.timerRequest)
	ELSE
		IF HAS_NET_TIMER_STARTED(sTJRVarsStruct.timerRequest)
		AND NOT HAS_NET_TIMER_EXPIRED(sTJRVarsStruct.timerRequest, TJR_REQUEST_INPUT_DURATION_MAX_MS)
		AND IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC TJR_AVAILABILITY TJR_GET_AVAILABILITY()
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(MPGlobals.sFreemodeCache.PlayerIndex)
		RETURN eTJRAVAILABILITY_UNAVAILABLE_GANG_MEMBER
	ENDIF
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(MPGlobals.sFreemodeCache.PlayerIndex)
		RETURN eTJRAVAILABILITY_UNAVAILABLE_ON_MISSION
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(MPGlobals.sFreemodeCache.PlayerIndex)
		RETURN eTJRAVAILABILITY_UNAVAILABLE_ON_MISSION
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(MPGlobals.sFreemodeCache.PlayerIndex)
		RETURN eTJRAVAILABILITY_UNAVAILABLE_ON_MISSION
	ENDIF
	
	RETURN eTJRAVAILABILITY_AVAILABLE
ENDFUNC

PROC TJR_PROCESS_REQUEST(TJR_VARS_STRUCT& sTJRVarsStruct)
	TJR_AVAILABILITY eAvailability = TJR_GET_AVAILABILITY()
	
	PRINTLN("[TAXI_JOB_REQUEST] TJR_PROCESS_REQUEST - eAvailability = ", TJR_GET_AVAILABILITY_NAME(eAvailability))
	
	SWITCH eAvailability
		CASE eTJRAVAILABILITY_AVAILABLE
			TJR_CLEAR_HELP_TEXT()
			TJR_SET_TAXI_VEHICLE(GET_VEHICLE_PED_IS_IN(MPGlobals.sFreemodeCache.PlayerPedIndex))
			REQUEST_LAUNCH_GB_MISSION(FMMC_TYPE_TAXI_DRIVER)
		BREAK
		
		CASE eTJRAVAILABILITY_UNAVAILABLE_GANG_MEMBER
			TJR_CLEAR_HELP_TEXT()
			TJR_TRIGGER_HELP_TEXT(sTJRVarsStruct, eTJRHELPTEXT_GANG_MEMBER)
		BREAK
		
		CASE eTJRAVAILABILITY_UNAVAILABLE_ON_MISSION
			TJR_CLEAR_HELP_TEXT()
			TJR_TRIGGER_HELP_TEXT(sTJRVarsStruct, eTJRHELPTEXT_ON_MISSION)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL TJR_SHOULD_USE_EVERYFRAME_UPDATE(TJR_VARS_STRUCT& sTJRVarsStruct)
	SWITCH TJR_GET_STATE(sTJRVarsStruct)
		CASE eTJRSTATE_WAIT_FOR_REQUEST
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_TAXI_JOB_REQUEST(TJR_VARS_STRUCT& sTJRVarsStruct)
	#IF IS_DEBUG_BUILD
	TJR_DEBUG_MAINTAIN_DEBUG(sTJRVarsStruct)
	#ENDIF
	
	TJR_MAINTAIN_SHOULD_ENABLE_REQUEST(sTJRVarsStruct)
	TJR_MAINTAIN_HELP_TEXT(sTJRVarsStruct)
	
	IF TJR_SHOULD_CLEANUP(sTJRVarsStruct)
		TJR_CLEANUP(sTJRVarsStruct)
	ENDIF
	
	SWITCH TJR_GET_STATE(sTJRVarsStruct)
		CASE eTJRSTATE_INIT
			IF TJR_IS_LOCAL_BIT_SET(sTJRVarsStruct, eTJRLOCALBITSET_SHOULD_ENABLE_REQUEST)
				TJR_SET_STATE(sTJRVarsStruct, eTJRSTATE_WAIT_FOR_REQUEST)
			ENDIF
		BREAK
		
		CASE eTJRSTATE_WAIT_FOR_REQUEST
			IF NOT TJR_IS_REQUEST_HELP_TEXT_DONE()
				TJR_TRIGGER_HELP_TEXT(sTJRVarsStruct, eTJRHELPTEXT_REQUEST_JOB)
				TJR_SET_REQUEST_HELP_TEXT_DONE()
			ENDIF
			
			IF TJR_HAS_LOCAL_PLAYER_TRIGGERED_REQUEST(sTJRVarsStruct)
				TJR_PROCESS_REQUEST(sTJRVarsStruct)
				TJR_SET_STATE(sTJRVarsStruct, eTJRSTATE_REQUEST_COMPLETE)
			ENDIF
		BREAK
		
		CASE eTJRSTATE_REQUEST_COMPLETE
			IF HAS_NET_TIMER_EXPIRED(sTJRVarsStruct.timerRequest, TJR_REQUEST_COMPLETE_DELAY_MS)
				RESET_NET_TIMER(sTJRVarsStruct.timerRequest)
				
				TJR_SET_STATE(sTJRVarsStruct, eTJRSTATE_WAIT_FOR_REQUEST)
			ENDIF
		BREAK
	ENDSWITCH
	
	IF TJR_SHOULD_USE_EVERYFRAME_UPDATE(sTJRVarsStruct)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
