USING "globals.sch"
USING "mp_globals.sch"

ENUM COMBINED_MODELS
	CM_NULL,
	CM_ARMOUR_TRUCK_HAULER,
	CM_AMROUR_TRUCK_PHANTOM
	, CM_NIGHTSHARK_AA
ENDENUM


FUNC MODEL_NAMES COMBINED_MODEL_ID(COMBINED_MODELS CombinedModel)
	INT iHash
	iHash = 1000 + ENUM_TO_INT(CombinedModel)
	RETURN INT_TO_ENUM(MODEL_NAMES, iHash)
ENDFUNC

FUNC COMBINED_MODELS GET_COMBINED_MODEL_ID_FROM_MODEL_NAME(MODEL_NAMES ModelName)
	INT i
	REPEAT COUNT_OF(COMBINED_MODELS) i
		COMBINED_MODELS CombinedModel
		CombinedModel = INT_TO_ENUM(COMBINED_MODELS, i)
		IF (COMBINED_MODEL_ID(CombinedModel) = ModelName)
			RETURN CombinedModel
		ENDIF
	ENDREPEAT
	RETURN CM_NULL
ENDFUNC

#IF IS_DEBUG_BUILD

FUNC STRING CombinedModelName(COMBINED_MODELS CombinedModel)
	SWITCH CombinedModel
		CASE CM_NULL					RETURN "CM_NULL"
		CASE CM_ARMOUR_TRUCK_HAULER 	RETURN "CM_ARMOUR_TRUCK_HAULER"
		CASE CM_AMROUR_TRUCK_PHANTOM	RETURN "CM_AMROUR_TRUCK_PHANTOM"
		CASE CM_NIGHTSHARK_AA			RETURN "CM_NIGHTSHARK_AA"
	ENDSWITCH
	RETURN "CM UNKNOWN!"
ENDFUNC

FUNC STRING GET_SAFE_MODEL_NAME_FOR_DEBUG(MODEL_NAMES ModelName)
	COMBINED_MODELS CombinedModel
	CombinedModel  = GET_COMBINED_MODEL_ID_FROM_MODEL_NAME(ModelName)
	IF (CombinedModel = CM_NULL)
		RETURN GET_MODEL_NAME_FOR_DEBUG(ModelName)
	ENDIF
	RETURN CombinedModelName(CombinedModel)
ENDFUNC
#ENDIF

PROC FillCombinedModelStruct(COMBINED_MODELS CombinedModel, COMBINED_MODEL_DATA &CombinedModelData)
	
	SWITCH CombinedModel
		CASE CM_ARMOUR_TRUCK_HAULER
			CombinedModelData.Model[0] = HAULER2	
			CombinedModelData.Model[1] = TRAILERLARGE
			CombinedModelData.fOverLap = -2.6
		BREAK
		CASE CM_AMROUR_TRUCK_PHANTOM
			CombinedModelData.Model[0] = PHANTOM3	
			CombinedModelData.Model[1] = TRAILERLARGE
			CombinedModelData.fOverLap = -2.6
		BREAK
		CASE CM_NIGHTSHARK_AA
			CombinedModelData.Model[0] = NIGHTSHARK	
			CombinedModelData.Model[1] = TRAILERSMALL2 
			CombinedModelData.fOverLap = 0.5
		BREAK 
	ENDSWITCH
	
ENDPROC



PROC SAFE_GET_MODEL_DIMENSIONS_COMBINDED_MODEL(COMBINED_MODELS CombinedModel, VECTOR &vReturnMin, VECTOR &vReturnMax, FLOAT fMinLength=6.0, FLOAT fMinWidth=3.5, FLOAT fMinHeight=3.0)
	
	FillCombinedModelStruct(CombinedModel, g_SafeCombinedModel_Data)
	
	INT i
	REPEAT 2 i
		IF IS_MODEL_VALID(g_SafeCombinedModel_Data.Model[i])
			GET_MODEL_DIMENSIONS(g_SafeCombinedModel_Data.Model[i], g_SafeCombinedModel_vMin[i], g_SafeCombinedModel_vMax[i])
		ENDIF
		IF (VMAG(g_SafeCombinedModel_vMin[i]) <= 0.01)
		OR (VMAG(g_SafeCombinedModel_vMax[i]) <= 0.01)
			g_SafeCombinedModel_vMin[i].x = 0.0 - (fMinWidth * 0.5)
			g_SafeCombinedModel_vMax[i].x = 0.0 + (fMinWidth * 0.5)
			g_SafeCombinedModel_vMin[i].y = 0.0 - (fMinLength * 0.5)
			g_SafeCombinedModel_vMax[i].y = 0.0 + (fMinLength * 0.5)
			g_SafeCombinedModel_vMin[i].z = 0.0 - (fMinHeight * 0.5)
			g_SafeCombinedModel_vMax[i].z = 0.0 + (fMinHeight * 0.5)
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
			PRINTLN("SAFE_GET_MODEL_DIMENSIONS_COMBINDED_MODEL - g_SafeCombinedModel_vMin[", i, "] = ", g_SafeCombinedModel_vMin[i], " g_SafeCombinedModel_vMax[", i, "] = ", g_SafeCombinedModel_vMax[i])
		ENDIF		
		#ENDIF
		
		g_SafeCombinedModel_fWidth[i] 	= g_SafeCombinedModel_vMax[i].x - g_SafeCombinedModel_vMin[i].x
		g_SafeCombinedModel_fLength[i] 	= g_SafeCombinedModel_vMax[i].y - g_SafeCombinedModel_vMin[i].y
		g_SafeCombinedModel_fHeight[i] 	= g_SafeCombinedModel_vMax[i].z - g_SafeCombinedModel_vMin[i].z
		
		IF (g_SafeCombinedModel_fWidth[i] > g_SafeCombinedModel_fMaxWidth)
			g_SafeCombinedModel_fMaxWidth = g_SafeCombinedModel_fWidth[i]
		ENDIF
	
		IF (g_SafeCombinedModel_fHeight[i] > g_SafeCombinedModel_fMaxHeight)
			g_SafeCombinedModel_fMaxHeight = g_SafeCombinedModel_fHeight[i]
		ENDIF
	
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	IF (g_SpawnData.bShowAdvancedSpew)
		PRINTLN("SAFE_GET_MODEL_DIMENSIONS_COMBINDED_MODEL - fMaxWidth = ", g_SafeCombinedModel_fMaxWidth, ", fMaxHeight = ", g_SafeCombinedModel_fMaxHeight)
	ENDIF		
	#ENDIF	
	 
	g_SafeCombinedModel_vCombinedMin.x = g_SafeCombinedModel_fMaxWidth * -0.5
	g_SafeCombinedModel_vCombinedMax.x = g_SafeCombinedModel_fMaxWidth * 0.5

	g_SafeCombinedModel_vCombinedMin.y = ((0.5 * g_SafeCombinedModel_fLength[0]) + g_SafeCombinedModel_fLength[1] + g_SafeCombinedModel_Data.fOverLap) * -1.0
	g_SafeCombinedModel_vCombinedMax.y = 0.5 * g_SafeCombinedModel_fLength[0]
	
	g_SafeCombinedModel_vCombinedMin.z = g_SafeCombinedModel_fHeight[0] * -0.5
	g_SafeCombinedModel_vCombinedMax.z = g_SafeCombinedModel_fHeight[0] * 0.5	
	
	vReturnMin = g_SafeCombinedModel_vCombinedMin
	vReturnMax = g_SafeCombinedModel_vCombinedMax
	
	#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
			PRINTLN("SAFE_GET_MODEL_DIMENSIONS_COMBINDED_MODEL - CombinedModel ", ENUM_TO_INT(CombinedModel), " vReturnMin = ", vReturnMin, " vReturnMax = ", vReturnMax)
		ENDIF
	#ENDIF	
	
ENDPROC


