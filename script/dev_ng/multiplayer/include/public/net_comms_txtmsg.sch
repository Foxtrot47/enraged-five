USING "rage_builtins.sch"
USING "globals.sch"

USING "net_comms_public.sch"

#IF IS_DEBUG_BUILD
	USING "net_comms_debug.sch"
#ENDIF




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Comms_Txtmsg.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Allows fire-and-forget txtmsgs to be sent in MP from either Players or CharSheetIDs.
//		NOTES			:	Accepts a number of requests to send a txtmsg, stores the details, and tries to send the highest priority
//								message.
//							This communicates directly with the Cellphone, but takes the MP Comms state into account.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      The MP Txtmsg Storage and Retrieval Routines
// ===========================================================================================================

// PURPOSE:	Reset all the variables associated with one MP Txtmsg
//
// INPUT PARAMS:		paramArrayPos			The array position of these Txtmsg details
PROC Reset_One_MP_Txtmsg_Details(INT paramArrayPos)

	g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags			= CLEAR_ALL_MP_TXTMSG_BITFLAGS
	g_sCommsMP.mpTxtmsgs[paramArrayPos].tmType				= NO_MP_TXTMSG_TYPE
	g_sCommsMP.mpTxtmsgs[paramArrayPos].tmSenderAsInt		= INVALID_MP_TXTMSG_SENDER
	g_sCommsMP.mpTxtmsgs[paramArrayPos].tmTextMessage		= ""
	g_sCommsMP.mpTxtmsgs[paramArrayPos].tmComponentString	= ""
	g_sCommsMP.mpTxtmsgs[paramArrayPos].tmComponentInt		= NO_INT_SUBSTRING_COMPONENT_VALUE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms]: MP Txtmsg - Resetting all stored details at array position: ")
		NET_PRINT_INT(paramArrayPos)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets an MP txtmsg array position as 'txt received'
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
PROC Set_MP_Txtmsg_Received(INT paramArrayPos)

	SET_BIT(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_RECEIVED)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........MP Txtmsg Array Position: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" - set as TXTMSG RECEIVED")
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a txtmsg has been received in the specified array position
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
// RETURN VALUE:		BOOL					TRUE if a txtmsg has been received, otherwise FALSE
FUNC BOOL Has_MP_Txtmsg_Been_Received(INT paramArrayPos)
	RETURN (IS_BIT_SET(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_RECEIVED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets an MP txtmsg array position as 'active'
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
PROC Set_MP_Txtmsg_As_Active(INT paramArrayPos)

	SET_BIT(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_ACTIVE)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........MP Txtmsg Array Position: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" - set as TXTMSG ACTIVE")
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the txtmsg in the specified array position is active
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
// RETURN VALUE:		BOOL					TRUE if the txtmsg is active, otherwise FALSE
FUNC BOOL Is_MP_Txtmsg_Active(INT paramArrayPos)
	RETURN (IS_BIT_SET(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_ACTIVE))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets an MP txtmsg type for the specified array position
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
//						paramType				The type of this txtmsg request (also used for prioritising)
PROC Set_MP_Txtmsg_Type(INT paramArrayPos, INT paramType)

	g_sCommsMP.mpTxtmsgs[paramArrayPos].tmType = paramType
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........MP Txtmsg Array Position: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" - setting txtmsg type as: ")
		NET_PRINT(Convert_MP_Txtmsg_Type_To_String(paramType))
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gets the MP txtmsg type for the specified array position
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
// RETURN VALUE:		INT						The tyupe of the txtmsg in this array position
FUNC INT Get_MP_Txtmsg_Type(INT paramArrayPos)
	RETURN (g_sCommsMP.mpTxtmsgs[paramArrayPos].tmType)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets the sender of an MP txtmsg for the specified array position
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
//						paramSenderAsInt		The Sender ID as an INT (Sender is either a Player Index as an INT, or a CharSheetID as an INT)
//						paramSenderIsPlayer		TRUE if the Sender INT is a Player Index, FALSE if the Sender INT is a CharSheet ID
//
// NOTES:	The sender can either be the Player Index as an INT or the CharSheetID as an INT, so also need to store which one it is
PROC Set_MP_Txtmsg_Sender_As_Int(INT paramArrayPos, INT paramSenderAsInt, BOOL paramSenderIsPlayer)

	// Store the Sender ID
	g_sCommsMP.mpTxtmsgs[paramArrayPos].tmSenderAsInt = paramSenderAsInt
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........MP Txtmsg Array Position: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" - setting txtmsg sender: ")
	#ENDIF
	
	IF (paramSenderIsPlayer)
		// Sender is a Player Index
		SET_BIT(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_SENT_BY_PLAYERINDEX)
		CLEAR_BIT(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_SENT_BY_CHARSHEETID)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("PLAYER with index = ")
			NET_PRINT_INT(paramSenderAsInt)
			NET_PRINT(" (")
			NET_PRINT(GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, paramSenderAsInt)))
			NET_PRINT(")")
			NET_NL()
		#ENDIF
	ELSE
		// Sender is a CharSheet ID
		SET_BIT(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_SENT_BY_CHARSHEETID)
		CLEAR_BIT(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_SENT_BY_PLAYERINDEX)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("CharSheetID INT = ")
			NET_PRINT_INT(paramSenderAsInt)
			NET_PRINT(" (")
			NET_PRINT(Convert_MP_CharSheetID_To_String(INT_TO_ENUM(enumCharacterList, paramSenderAsInt)))
			NET_PRINT(")")
			NET_NL()
		#ENDIF
	ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gets the sender (as an INT) of an MP txtmsg for the specified array position
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot
// RETURN VALUE:		INT						The Sender ID as an INT (Sender is either a Player Index as an INT, or a CharSheetID as an INT)
FUNC INT Get_MP_Txtmsg_Sender_As_Int(INT paramArrayPos)
	RETURN (g_sCommsMP.mpTxtmsgs[paramArrayPos].tmSenderAsInt)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets an MP txtmsg text for the specified array position
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
//						paramText				The text label of the text for this text message
PROC Set_MP_Txtmsg_Text(INT paramArrayPos, STRING paramText)

	TEXT_LABEL_15 theText = paramText
	g_sCommsMP.mpTxtmsgs[paramArrayPos].tmTextMessage = theText
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........MP Txtmsg Array Position: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" - setting txtmsg Text as: ")
		NET_PRINT(paramText)
		NET_PRINT(" (")
		NET_PRINT(GET_STRING_FROM_TEXT_FILE(paramText))
		NET_PRINT(")")
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gets an MP txtmsg text for the specified array position
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
// RETURN VALUE:		TEXT_LABEL_15			Text txtmsg text label
FUNC TEXT_LABEL_15 Get_MP_Txtmsg_Text(INT paramArrayPos)
	RETURN (g_sCommsMP.mpTxtmsgs[paramArrayPos].tmTextMessage)
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Component Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets whether the text message contains any components
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
PROC Set_MP_Txtmsg_Has_Components(INT paramArrayPos)

	SET_BIT(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_COMPONENTS_IN_USE)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........MP Txtmsg Array Position: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" - set as COMPONENTS_IN_USE")
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a txtmsg in the specified array position has components
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to check
// RETURN VALUE:		BOOL					TRUE if a txtmsg has components, otherwise FALSE
FUNC BOOL Does_MP_Txtmsg_Have_Components(INT paramArrayPos)
	RETURN (IS_BIT_SET(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_COMPONENTS_IN_USE))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets whether the string component of a text message is a player name
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
PROC Set_MP_Txtmsg_String_Component_Is_Player(INT paramArrayPos)

	SET_BIT(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_COMPONENT_IS_PLAYER)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........MP Txtmsg Array Position: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" - set as STRING_COMPONENT_IS_PLAYER")
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a txtmsg string component in the specified array position is a player
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to check
// RETURN VALUE:		BOOL					TRUE if a txtmsg string component is a player, otherwise FALSE
FUNC BOOL Is_MP_Txtmsg_String_Component_A_Player(INT paramArrayPos)
	RETURN (IS_BIT_SET(g_sCommsMP.mpTxtmsgs[paramArrayPos].tmBitflags, MP_TXTMSG_COMPONENT_IS_PLAYER))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Stores a text message string component
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
//						paramStringComponent	The String Component (usually as a text label)
//						paramIsPlayer			TRUE if the string component is a player name, otherwise FALSE for a textLabel
//
// NOTES:	Assumes all consistency checking has been done prior to calling this function
PROC Set_MP_Txtmsg_Component_String(INT paramArrayPos, STRING paramStringComponent, BOOL paramIsPlayer)

	g_sCommsMP.mpTxtmsgs[paramArrayPos].tmComponentString = paramStringComponent
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........MP Txtmsg Array Position: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" - setting txtmsg String Component as: ")
		NET_PRINT(paramStringComponent)
		NET_PRINT(" (")
		IF (paramIsPlayer)
			NET_PRINT(paramStringComponent)
		ELSE
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(paramStringComponent))
		ENDIF
		NET_PRINT(")")
		NET_NL()
	#ENDIF
	
	// Is the string a player?
	IF NOT (paramIsPlayer)
		// ...no
		EXIT
	ENDIF
	
	// String is a player
	Set_MP_Txtmsg_String_Component_Is_Player(paramArrayPos)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gets an MP txtmsg component string for the specified array position
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot for the txtmsg
// RETURN VALUE:		TEXT_LABEL_31			Text txtmsg text label
FUNC TEXT_LABEL_31 Get_MP_Txtmsg_Component_String(INT paramArrayPos)
	RETURN (g_sCommsMP.mpTxtmsgs[paramArrayPos].tmComponentString)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Stores a text message int component
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot to store the txtmsg
//						paramIntComponent		The Int Component
PROC Set_MP_Txtmsg_Component_Int(INT paramArrayPos, INT paramIntComponent)

	g_sCommsMP.mpTxtmsgs[paramArrayPos].tmComponentInt = paramIntComponent
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........MP Txtmsg Array Position: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" - setting txtmsg Int Component as: ")
		NET_PRINT_INT(paramIntComponent)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gets an MP txtmsg component int for the specified array position
//
// INPUT PARAMS:		paramArrayPos			The txtmsg slot for the txtmsg
// RETURN VALUE:		INT						Text txtmsg integer
FUNC INT Get_MP_Txtmsg_Component_Int(INT paramArrayPos)
	RETURN (g_sCommsMP.mpTxtmsgs[paramArrayPos].tmComponentInt)
ENDFUNC




// ===========================================================================================================
//      The Main MP Txtmsg Internal Control Routines
// ===========================================================================================================

// PURPOSE:	To return a Free Array position for the MP Txtmsg.
//
// RETURN VALUE:		INT			The free array position, or ILLEGAL_ARRAY_POSITION if array is full 
FUNC INT Get_Free_MP_Txtmsg_Array_Position()

	INT tempLoop = 0
	REPEAT MAX_MP_TXTMSGS tempLoop
		IF NOT (Has_MP_Txtmsg_Been_Received(tempLoop))
			RETURN (tempLoop)
		ENDIF
	ENDREPEAT
	
	// Array full
	RETURN (ILLEGAL_ARRAY_POSITION)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks and Stores the all details for a MP txtmsg (accepting the PlayerID and the CharSheetID as an INT)
//
// INPUT PARAMS:		paramType					The type of Txtmsg
//						paramText					The TextLabel containing the txtmsg text
//						paramSenderAsInt			The Sender ID as an INT (Sender is either a Player Index as an INT, or a CharSheetID as an INT)
//						paramSenderIsPlayer			TRUE if the Sender INT is a Player Index, FALSE if the Sender INT is a CharSheet ID
//						paramComponentString		An additional text label to be inserted into the main text
//						paramComponentIsPlayerName	TRUE if the text Label component is actually a player name string, otherwise FALSE
//						paramComponentInt			An INT component to be inserted into the main text
//
// NOTES:	Assumes the Sender ID has been properly checked by the calling function.
PROC Store_MP_Txtmsg_Details(INT paramType, STRING paramText, INT paramSenderAsInt, BOOL paramSenderIsPlayer, STRING paramComponentString, BOOL paramComponentIsPlayerName, INT paramComponentInt)

	// Error Checking - Ensure Text Label is valid
	IF (Is_String_Null_Or_Empty(paramText))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Store_MP_Txtmsg_Details() - 'Text' string is NULL or EMPTY. Request from script: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Store_MP_Txtmsg_Details(): The 'Text' string is NULL or empty - This should be a subtitle Text Label")
		#ENDIF
		
		EXIT
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramText) > MAX_LENGTH_MP_COMMS_ROOT_ID_STRING)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Store_MP_Txtmsg_Details() - 'Text' string \"")
			NET_PRINT(paramText)
			NET_PRINT("\" is too long. Request from script: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Store_MP_Txtmsg_Details(): The 'Text' string is longer than expected. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Error Checking - Ensure String Component is properly set up if available
	IF NOT (IS_STRING_NULL_OR_EMPTY(paramComponentString))
		IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramComponentString) > MAX_LENGTH_MP_COMMS_COMPONENT_TEXTLABEL)
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Store_MP_Txtmsg_Details() - 'Component' string is too long. Request from script: ")
				NET_PRINT(GET_THIS_SCRIPT_NAME())
				NET_NL()
				
				SCRIPT_ASSERT("Store_MP_Txtmsg_Details(): The 'Component' string is longer than expected. Tell Keith.")
			#ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	// Error Checking - this should be valid because it's being set by a private function
	#IF IS_DEBUG_BUILD
		IF (paramType = NO_MP_TXTMSG_TYPE)
			NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Store_MP_Txtmsg_Details() - The Txtmsg Type is. Request from script: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Store_MP_Txtmsg_Details(): The 'Text' string is longer than expected. Tell Keith.")
		ENDIF
	#ENDIF
	
	// Find a free Txtmsg array position
	INT freeArrayPos = Get_Free_MP_Txtmsg_Array_Position()
	IF (freeArrayPos = ILLEGAL_ARRAY_POSITION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Store_MP_Txtmsg_Details() - The Txtmsg Array is full. Request from script: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Store_MP_Txtmsg_Details(): The Txtmsg array is full. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// The details are being stored
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms]: Storing New Txtmsg Details") NET_NL()
	#ENDIF
	
	// Store the Txtmsg details
	Set_MP_Txtmsg_Received(freeArrayPos)
	Set_MP_Txtmsg_Type(freeArrayPos, paramType)
	Set_MP_Txtmsg_Sender_As_Int(freeArrayPos, paramSenderAsInt, paramSenderIsPlayer)
	Set_MP_Txtmsg_Text(freeArrayPos, paramText)
	
	// Store the Component details if applicable
	IF (IS_STRING_NULL_OR_EMPTY(paramComponentString))
	AND (paramComponentInt = NO_INT_SUBSTRING_COMPONENT_VALUE)
		// ...there are no components
		EXIT
	ENDIF
	
	// There are components, so store them
	Set_MP_Txtmsg_Has_Components(freeArrayPos)
	
	// ...string
	IF NOT (IS_STRING_NULL_OR_EMPTY(paramComponentString))
		Set_MP_Txtmsg_Component_String(freeArrayPos, paramComponentString, paramComponentIsPlayerName)
	ENDIF
	
	// ...int
	IF NOT (paramComponentInt = NO_INT_SUBSTRING_COMPONENT_VALUE)
		Set_MP_Txtmsg_Component_Int(freeArrayPos, paramComponentInt)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks and Stores the details for a MP Txtmsg sent from a PlayerIndex
//
// INPUT PARAMS:		paramType					The type of Txtmsg
//						paramPlayerID				The PlayerID requesting the Txtmsg
//						paramText					The TextLabel containing the txtmsg text
//						paramComponentString		An additional text label to be inserted into the main text
//						paramComponentIsPlayerName	TRUE if the text Label component is actually a player name string, otherwise FALSE
//						paramComponentInt			An INT component to be inserted into the main text
PROC Store_MP_Txtmsg_Details_From_PlayerIndex(INT paramType, PLAYER_INDEX paramPlayerID, STRING paramText, STRING paramComponentString, BOOL paramComponentIsPlayerName, INT paramComponentInt)

	// Error Checking - Is Player Index valid?
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Store_MP_Txtmsg_Details_From_PlayerIndex - illegal PlayerIndex. Request from script: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
		
			SCRIPT_ASSERT("Store_MP_Txtmsg_Details_From_PlayerIndex: The Player ID passed in as the Txtmsg Sender is not OK. Ignoring Txtmsg Request. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Convert the PlayerIndex to an INT
	INT thePlayerIndexAsInt = NATIVE_TO_INT(paramPlayerID)
	BOOL senderIsPlayer = TRUE
	Store_MP_Txtmsg_Details(paramType, paramText, thePlayerIndexAsInt, senderIsPlayer, paramComponentString, paramComponentIsPlayerName, paramComponentInt)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks and Stores the details for a MP Txtmsg sent from a CharSheetID
//
// INPUT PARAMS:		paramType					The type of Txtmsg
//						paramCharSheetID			The CharSheetID requesting the Txtmsg
//						paramText					The TextLabel containing the txtmsg text
//						paramComponentString		An additional text label to be inserted into the main text
//						paramComponentIsPlayerName	TRUE if the text Label component is actually a player name string, otherwise FALSE
//						paramComponentInt			An INT component to be inserted into the main text
PROC Store_MP_Txtmsg_Details_From_CharSheetID(INT paramType, enumCharacterList paramCharSheetID, STRING paramText, STRING paramComponentString, BOOL paramComponentIsPlayerName, INT paramComponentInt)
	
	// Convert the CharSheetID to an INT
	INT theCharSheetIDAsInt = ENUM_TO_INT(paramCharSheetID)
	BOOL senderIsPlayer = FALSE
	Store_MP_Txtmsg_Details(paramType, paramText, theCharSheetIDAsInt, senderIsPlayer, paramComponentString, paramComponentIsPlayerName, paramComponentInt)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the active MP txtmsg
//
// INPUT PARAMS:		paramArrayPos			Array Position of the txtmsg containing the active txtmsg
// RETURN VALUE:		BOOL					TRUE if the active txtmsg is still active, FALSE if it is no longer active
FUNC BOOL Maintain_Active_MP_Txtmsg(INT paramArrayPos)
	
	// Just to be sure, although it should have been checked before calling this function
	#IF IS_DEBUG_BUILD
		IF NOT (Is_MP_Txtmsg_Active(paramArrayPos))
			NET_PRINT("...KGM MP [MPComms]: Maintain_Active_MP_Txtmsg(): The txtmsg is not active: ")
			NET_PRINT_INT(paramArrayPos)
			NET_NL()

			RETURN FALSE
		ENDIF
	#ENDIF
	
	// Check if the active MP txtmsg is still active
	IF (Is_MP_Comms_Still_Playing())
		RETURN TRUE
	ENDIF
	
	// Clear the active txtmsg details
	Reset_One_MP_Txtmsg_Details(paramArrayPos)
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Try to send a standard MP txtmsg from a Player
//
// INPUT PARAMS:		paramArrayPos			Array Position of the Standard txtmsg
PROC Maintain_Attempt_To_Issue_Standard_MP_Txtmsg_From_A_Player(INT paramArrayPos)

	// Convert the sender to a Player Index
	PLAYER_INDEX theSender = INT_TO_NATIVE(PLAYER_INDEX, Get_MP_Txtmsg_Sender_As_Int(paramArrayPos))
	IF NOT (IS_NET_PLAYER_OK(theSender, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms]: Player ID of sender for Standard MP txtmsg is no longer valid. Clearing up.")
			NET_NL()
		#ENDIF
		
		// Sending Player is no longer valid, so clear up this txtmsg request
		Reset_One_MP_Txtmsg_Details(paramArrayPos)
		EXIT
	ENDIF
	
	// Sender is still valid, so try to send it through the MP Comms system
	TEXT_LABEL_15 theTxtmsgText = Get_MP_Txtmsg_Text(paramArrayPos)
	
	IF NOT (Request_MP_Comms_Txtmsg_From_Player(theSender, theTxtmsgText))
		EXIT
	ENDIF
	
	// Txtmsg has been successfully sent
	Set_MP_Txtmsg_As_Active(paramArrayPos)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Try to send an MP txtmsg with components from a Player
//
// INPUT PARAMS:		paramArrayPos			Array Position of the txtmsg
PROC Maintain_Attempt_To_Issue_MP_Txtmsg_With_Components_From_A_Player(INT paramArrayPos)

	// Convert the sender to a Player Index
	PLAYER_INDEX theSender = INT_TO_NATIVE(PLAYER_INDEX, Get_MP_Txtmsg_Sender_As_Int(paramArrayPos))
	IF NOT (IS_NET_PLAYER_OK(theSender, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms]: Player ID of sender for MP txtmsg with components is no longer valid. Clearing up.")
			NET_NL()
		#ENDIF
		
		// Sending Player is no longer valid, so clear up this txtmsg request
		Reset_One_MP_Txtmsg_Details(paramArrayPos)
		EXIT
	ENDIF
	
	// Sender is still valid, so try to send it through the MP Comms system
	TEXT_LABEL_15 	theTxtmsgText 		= Get_MP_Txtmsg_Text(paramArrayPos)
	TEXT_LABEL_31 	theStringComponent	= Get_MP_Txtmsg_Component_String(paramArrayPos)
	BOOL			stringIsPlayer		= Is_MP_Txtmsg_String_Component_A_Player(paramArrayPos)
	INT				theIntComponent		= Get_MP_Txtmsg_Component_Int(paramArrayPos)
	
	IF NOT (Request_MP_Comms_Txtmsg_With_Components_From_Player(theSender, theTxtmsgText, theStringComponent, stringIsPlayer, theIntComponent))
		EXIT
	ENDIF
	
	// Txtmsg has been successfully sent
	Set_MP_Txtmsg_As_Active(paramArrayPos)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Try to send a standard MP txtmsg from a CharSheetID
//
// INPUT PARAMS:		paramArrayPos			Array Position of the Standard txtmsg
PROC Maintain_Attempt_To_Issue_Standard_MP_Txtmsg_From_CharSheetID(INT paramArrayPos)

	// Convert the sender to a CharSheetID
	enumCharacterList theSender = INT_TO_ENUM(enumCharacterList, Get_MP_Txtmsg_Sender_As_Int(paramArrayPos))
	
	// Try to send it through the MP Comms system
	TEXT_LABEL_15 theTxtmsgText = Get_MP_Txtmsg_Text(paramArrayPos)
	
	IF NOT (Request_MP_Comms_Txtmsg(theSender, theTxtmsgText))
		EXIT
	ENDIF
	
	// Txtmsg has been successfully sent
	Set_MP_Txtmsg_As_Active(paramArrayPos)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Try to send an MP txtmsg with components from a CharSheetID
//
// INPUT PARAMS:		paramArrayPos			Array Position of the Standard txtmsg
PROC Maintain_Attempt_To_Issue_MP_Txtmsg_With_Components_From_CharSheetID(INT paramArrayPos)

	// Convert the sender to a CharSheetID
	enumCharacterList theSender = INT_TO_ENUM(enumCharacterList, Get_MP_Txtmsg_Sender_As_Int(paramArrayPos))
	
	// Try to send it through the MP Comms system
	TEXT_LABEL_15 	theTxtmsgText 		= Get_MP_Txtmsg_Text(paramArrayPos)
	TEXT_LABEL_31 	theStringComponent	= Get_MP_Txtmsg_Component_String(paramArrayPos)
	BOOL			stringIsPlayer		= Is_MP_Txtmsg_String_Component_A_Player(paramArrayPos)
	INT				theIntComponent		= Get_MP_Txtmsg_Component_Int(paramArrayPos)
	
	IF NOT (Request_MP_Comms_Txtmsg_With_Components(theSender, theTxtmsgText, theStringComponent, stringIsPlayer, theIntComponent))
		EXIT
	ENDIF
	
	// Txtmsg has been successfully sent
	Set_MP_Txtmsg_As_Active(paramArrayPos)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Try to send the highest priority txtmsg
//
// INPUT PARAMS:		paramArrayPos			Array Position of the txtmsg containing the highest priority (non-active) txtmsg
PROC Maintain_Highest_Priority_Waiting_MP_Txtmsg(INT paramArrayPos)
	
	// Just to be sure, although it should have been checked before calling this function
	#IF IS_DEBUG_BUILD
		IF NOT (Has_MP_Txtmsg_Been_Received(paramArrayPos))
			NET_PRINT("...KGM MP [MPComms]: Maintain_Highest_Priority_Waiting_MP_Txtmsg(): The txtmsg is not marked as 'received': ")
			NET_PRINT_INT(paramArrayPos)
			NET_NL()

			EXIT
		ENDIF
	#ENDIF
				
	// Don't request dialogue if any comms device is actively communicating already.
	IF (Is_Any_MP_Comms_Device_Actively_Communicating())
		EXIT
	ENDIF
	
	// Try to send this MP txtmsg
	SWITCH (Get_MP_Txtmsg_Type(paramArrayPos))
		CASE MP_TXTMSG_TYPE_FROM_PLAYER
			IF (Does_MP_Txtmsg_Have_Components(paramArrayPos))
				// ...this is a txtmsg with components from a player - try to send it
				Maintain_Attempt_To_Issue_MP_Txtmsg_With_Components_From_A_Player(paramArrayPos)
			ELSE
				// ...this is a Standard txtmsg from a player - try to send it
				Maintain_Attempt_To_Issue_Standard_MP_Txtmsg_From_A_Player(paramArrayPos)
			ENDIF
			BREAK
			
		CASE MP_TXTMSG_TYPE_FROM_CHARSHEETID
			IF (Does_MP_Txtmsg_Have_Components(paramArrayPos))
				// ...this is a txtmsg with components from a Story Character - try to send it
				Maintain_Attempt_To_Issue_MP_Txtmsg_With_Components_From_CharSheetID(paramArrayPos)
			ELSE
				// ...this is a Standard txtmsg from a Story Character - try to send it
				Maintain_Attempt_To_Issue_Standard_MP_Txtmsg_From_CharSheetID(paramArrayPos)
			ENDIF
			BREAK
			
		CASE NO_MP_TXTMSG_TYPE
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MPComms]: Trying to issue txtmsg with NO_MP_TXTMSG_TYPE: ")
				NET_PRINT_INT(paramArrayPos)
				NET_NL()
			#ENDIF
			
			Reset_One_MP_Txtmsg_Details(paramArrayPos)
			BREAK
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MPComms]: Trying to issue txtmsg with UNKNOWN txtmsg type: ")
				NET_PRINT_INT(paramArrayPos)
				NET_PRINT(" - Add a control function. temporarily clearing up")
				NET_NL()

				SCRIPT_ASSERT("Maintain_Highest_Priority_Waiting_MP_Txtmsg: Trying to process UNKNOWN MP txtmsg type. Look at console log. Tell Keith.")
			#ENDIF
			
			Reset_One_MP_Txtmsg_Details(paramArrayPos)
			BREAK
	ENDSWITCH
	
ENDPROC



// ===========================================================================================================
//      The Main MP Txtmsg External Control Routines
// ===========================================================================================================

// PURPOSE:	A one-off Txtmsg initialisation that loops through the array clearing out all variables.
PROC Initialise_MP_Txtmsg()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms]: Initialise_MP_Txtmsg") NET_NL()
	#ENDIF
	
	// Clear out the whole array
	INT tempLoop = 0
	REPEAT MAX_MP_TXTMSGS tempLoop
		Reset_One_MP_Txtmsg_Details(tempLoop)
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	This procedure should be called every frame to control access to the txtmsg systems - needs to loop through array.
PROC Maintain_MP_Txtmsgs()

	INT tempLoop			= 0
	INT activeTxtmsg		= ILLEGAL_ARRAY_POSITION
	INT highPriorityTxtmsg	= ILLEGAL_ARRAY_POSITION
	INT highestPriority		= NO_MP_TXTMSG_TYPE
	INT thisPriority		= NO_MP_TXTMSG_TYPE

	REPEAT MAX_MP_TXTMSGS tempLoop
		IF (Has_MP_Txtmsg_Been_Received(tempLoop))
			IF (Is_MP_Txtmsg_Active(tempLoop))
				// This txtmsg is the active txtmsg, so store the details for later processing
				#IF IS_DEBUG_BUILD
					IF NOT (activeTxtmsg = ILLEGAL_ARRAY_POSITION)
						NET_PRINT("...KGM MP [MPComms]: Found more than one active MP txtmsg: Found ")
						NET_PRINT_INT(tempLoop)
						NET_PRINT(" when ")
						NET_PRINT_INT(activeTxtmsg)
						NET_PRINT(" is already active")
						NET_NL()

						SCRIPT_ASSERT("Maintain_MP_Txtmsgs(): Found a second active Txtmsg. Look at console log. Tell Keith.")
					ENDIF
				#ENDIF
				
				// Store this as the active txtmsg
				activeTxtmsg = tempLoop
			ELSE
				// This txtmsg is not active, so check if it is the highest priority waiting txtmsg
				thisPriority = Get_MP_Txtmsg_Type(tempLoop)
				IF (thisPriority > highestPriority)
					// It is, so store the details for later processing
					highestPriority = thisPriority
					highPriorityTxtmsg = tempLoop
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Maintain the active txtmsg
	IF NOT (activeTxtmsg = ILLEGAL_ARRAY_POSITION)
		IF (Maintain_Active_MP_Txtmsg(activeTxtmsg))
			// ...still active, so nothing more to do
			EXIT
		ENDIF
	ENDIF
	
	// If required, maintain txtmsgs waiting to get sent (if there are any)
	IF (highPriorityTxtmsg = ILLEGAL_ARRAY_POSITION)
		EXIT
	ENDIF
	
	// Check if the highest priority waiting txtmsg should try to get sent
	Maintain_Highest_Priority_Waiting_MP_Txtmsg(highPriorityTxtmsg)

ENDPROC




// ===========================================================================================================
//      The MP Txtmsg Public Routines
// ===========================================================================================================

// PURPOSE:	Send a Standard MP Txtmsg from a Player
//
// INPUT PARAMS:		paramPlayerId				The PlayerID
//						paramText					The Text Label containing the txtmsg text
//						paramComponentString		[DEFAULT = NULL] An additional text label to be inserted into the main text
//						paramComponentIsPlayerName	[DEFAULT = FALSE] TRUE if the text Label component is actually a player name string, otherwise FALSE
//						paramComponentInt			[DEFAULT = NO_INT_SUBSTRING_COMPONENT_VALUE] An INT component to be inserted into the main text
PROC Send_MP_Txtmsg_From_Player(PLAYER_INDEX paramPlayerId, STRING paramText, STRING paramComponentString = NULL, BOOL paramComponentIsPlayerName = FALSE, INT paramComponentInt = NO_INT_SUBSTRING_COMPONENT_VALUE)
	Store_MP_Txtmsg_Details_From_PlayerIndex(MP_TXTMSG_TYPE_FROM_PLAYER, paramPlayerId, paramText, paramComponentString, paramComponentIsPlayerName, paramComponentInt)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Send a Standard MP Txtmsg from a CharSheetID
//
// INPUT PARAMS:		paramCharSheetID			The CharSheetID
//						paramText					The Text Label containing the txtmsg text
//						paramComponentString		[DEFAULT = NULL] An additional text label to be inserted into the main text
//						paramComponentIsPlayerName	[DEFAULT = FALSE] TRUE if the text Label component is actually a player name string, otherwise FALSE
//						paramComponentInt			[DEFAULT = NO_INT_SUBSTRING_COMPONENT_VALUE] An INT component to be inserted into the main text
PROC Send_MP_Txtmsg_From_CharSheetID(enumCharacterList paramCharSheetID, STRING paramText, STRING paramComponentString = NULL, BOOL paramComponentIsPlayerName = FALSE, INT paramComponentInt = NO_INT_SUBSTRING_COMPONENT_VALUE)
	Store_MP_Txtmsg_Details_From_CharSheetID(MP_TXTMSG_TYPE_FROM_CHARSHEETID, paramCharSheetID, paramText, paramComponentString, paramComponentIsPlayerName, paramComponentInt)
ENDPROC







