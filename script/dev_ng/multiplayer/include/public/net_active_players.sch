USING "globals.sch"

#IF FEATURE_GEN9_EXCLUSIVE
FUNC BOOL SHOULD_SET_PLAYER_COUNT_TO_ONE()
	RETURN GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].sMPIntro.eState > eMPINTRO_STATE_INIT
		AND GlobalplayerBD_FM_4[NATIVE_TO_INT(PLAYER_ID())].sMPIntro.eState < eMPINTRO_STATE_COMPLETE
ENDFUNC
#ENDIF

//Data required for the player counts
STRUCT PLAYER_COUNT_DATA
	INT iCount = 0
	INT iNumOnTheIsland
	INT iNumOffIsland
	INT iNumSpectators = 0
ENDSTRUCT


//Data count - Any thing that needs called pre count
PROC SET_TOTAL_PLAYER_COUNT_DATA_PRE(INT iLocalPlayerID)
	MPGlobals.g_iNumberOfSCTV = 0
	
	//Cache the local player state, as that can change mid frame
	IF iLocalPlayerID != -1
		MPGlobals.g_bLocalPlayerActive = (GlobalplayerBD[iLocalPlayerID].iGameState = MAIN_GAME_STATE_RUNNING)
		MPGlobals.g_iLocalPlayerID = iLocalPlayerID
	ELSE
		MPGlobals.g_bLocalPlayerActive = FALSE
		MPGlobals.g_iLocalPlayerID = -1		
	ENDIF

ENDPROC

//Data count - for a player who is active
PROC SET_PLAYER_COUNT_LOOP(PLAYER_INDEX playerId, INT iPlayer, PLAYER_COUNT_DATA &sPlayerCount)
							
	IF IS_PLAYER_SCTV(playerId)
		IF NETWORK_PLAYER_IS_ROCKSTAR_DEV(playerId)
			MPGlobals.g_iNumberOfSCTV++	
		ENDIF
		sPlayerCount.iNumSpectators ++
	ENDIF
	
	IF MPGlobals.g_iLocalPlayerID = iPlayer
		//Local player checks this global, as it can change mid frame
		IF MPGlobals.g_bLocalPlayerActive
			sPlayerCount.iCount ++
		ENDIF
	ELIF (GlobalplayerBD[iPlayer].iGameState = MAIN_GAME_STATE_RUNNING)		
		sPlayerCount.iCount ++
	ENDIF
	
	IF IS_BIT_SET_ENUM(GlobalplayerBD[iPlayer].iHeistIslandTravelBS, HITBS__ON_ISLAND)
		sPlayerCount.iNumOnTheIsland ++
	ELSE
		sPlayerCount.iNumOffIsland ++						
	ENDIF		
			
ENDPROC

//Data count - Anything that needs called post loop
PROC SET_TOTAL_PLAYER_COUNT_DATA_POST(PLAYER_COUNT_DATA &sPlayerCount)
	
	
	INT iNumToDeduct
	
	IF MPGlobals.g_iLocalPlayerID != -1
		IF IS_BIT_SET_ENUM(GlobalplayerBD[MPGlobals.g_iLocalPlayerID].iHeistIslandTravelBS, HITBS__ON_ISLAND)
			iNumToDeduct = sPlayerCount.iNumOffIsland
		ELSE
			iNumToDeduct = sPlayerCount.iNumOnTheIsland
		ENDIF
	ENDIF
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION() // Not on a Job
		IF g_i_NumPlayersForLbd <> (sPlayerCount.iCount - sPlayerCount.iNumSpectators - iNumToDeduct)
			g_i_NumPlayersForLbd = (sPlayerCount.iCount - sPlayerCount.iNumSpectators - iNumToDeduct)
		ENDIF
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF SHOULD_SET_PLAYER_COUNT_TO_ONE()
		g_i_NumPlayersForLbd = 1
	ENDIF
	#ENDIF
ENDPROC

//Data count - Loop all players and set the count data that's needed.
PROC SET_PLAYER_COUNTS()

	IF NETWORK_IS_GAME_IN_PROGRESS()


		INT iPlayer
		PLAYER_INDEX PlayerID
		PLAYER_COUNT_DATA sPlayerCount
		INT iLocalPlayerID = NETWORK_PLAYER_ID_TO_INT()	
		
		//Data count - Any thing that needs called pre count
		SET_TOTAL_PLAYER_COUNT_DATA_PRE(iLocalPlayerID)
		
		REPEAT NUM_NETWORK_PLAYERS iPlayer	
		
			PlayerID = INT_TO_PLAYERINDEX(iPlayer)					
			IF NETWORK_IS_PLAYER_ACTIVE(playerID)		
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
				
					//Data count - for a player who is active
					SET_PLAYER_COUNT_LOOP(playerId, iPlayer, sPlayerCount)					
					
				ENDIF
			ENDIF
			
		ENDREPEAT
		
		//Data count - Anything that needs called post loop
		SET_TOTAL_PLAYER_COUNT_DATA_POST(sPlayerCount)
		
	ENDIF
ENDPROC


