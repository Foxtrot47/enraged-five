//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_simple_interior_ie_garage.sch															//
// Description: This is implementation of ie garage as simple interior. As such this does not expose any	//
//				public functions. All functions to manipulate simple interiors are in net_simple_interior	//
// Written by:  Tymon																						//
// Date:  		24/06/2016																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "MP_globals_simple_interior.sch"
USING "net_simple_interior_base.sch"
USING "net_realty_warehouse.sch"
USING "net_simple_interior_common.sch"
USING "net_gang_boss.sch"
USING "net_simple_interior_cs_header_include.sch"


CONST_INT SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_BOSS		0 // We entered the garage as the boss (also owner)
CONST_INT SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_GOON		1 // We entered the garage as a goon
CONST_INT SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_OWNER 	2 // We entered the garage as the owner (but not boss)

CONST_INT BS_SIMPLE_INTERIOR_IE_GARAGE_WANTED_LEVEL_CLEARED 					0
CONST_INT BS_SIMPLE_INTERIOR_IE_SCREEN_FADED_OUT_FOR_MISSION_VEHICLE_CREATION 	1
CONST_INT BS_SIMPLE_INTERIOR_IE_SET_END_SCREEN_SUPPRESS_FADE_IN					2

//Blip data
CONST_INT MAX_IE_GARAGE_BLIPS	1

FUNC BOOL DOES_IE_GARAGE_USE_EXTERIOR_SCRIPT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

PROC _INIT_GENERIC_WAREHOUSE_DOOR(SIMPLE_INTERIOR_DETAILS &details)
	// Child script(s)
	details.strInteriorChildScript = "AM_MP_IE_WAREHOUSE"
	
	// Exit data
	details.exitData.vLocateCoords1[0] = <<-0.900, 12.750, -5.640>>
	details.exitData.vLocateCoords2[0] = <<-7.360, 12.750, -3.180>>
	details.exitData.fLocateWidths[0] = 1.000
	details.exitData.fHeadings[0] = 0.0
	
	details.entryAnim.strOnEnterSoundNames[0] = "GENERIC_DOOR_PUSH"
	details.entryAnim.strOnEnterSoundNames[1] = "GENERIC_DOOR_LIMIT"
	details.entryAnim.fOnEnterSoundPhases[0] = 0.295
	details.entryAnim.fOnEnterSoundPhases[1] = 0.350
	details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_SCRIPT_DOORS_SOUNDS"
	
	details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
	details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
	
	details.entryAnim.strOnLeaveFadeOutStartSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
	details.entryAnim.strOnLeaveFadeOutStartSoundName = "Garage_Door_Open"
ENDPROC

PROC _INIT_GENERIC_WAREHOUSE_ELECTRIC_GARAGE_DOOR(SIMPLE_INTERIOR_DETAILS &details)
	// Child script(s)
	details.strInteriorChildScript = "AM_MP_IE_WAREHOUSE"
	
	// Exit data
	details.exitData.vLocateCoords1[0] = <<-0.900, 12.750, -5.640>>
	details.exitData.vLocateCoords2[0] = <<-7.360, 12.750, -3.180>>
	details.exitData.fLocateWidths[0] = 1.000
	details.exitData.fHeadings[0] = 0.0
	
	details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
	details.entryAnim.strOnEnterFadeOutFinishSoundName = "Garage_Door_Close"
	
	details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
	details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
	
	details.entryAnim.strOnLeaveFadeOutStartSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
	details.entryAnim.strOnLeaveFadeOutStartSoundName = "Garage_Door_Open"
ENDPROC

PROC _INIT_GENERIC_WAREHOUSE_SHUTTER_DOOR(SIMPLE_INTERIOR_DETAILS &details)
	// Child script(s)
	details.strInteriorChildScript = "AM_MP_IE_WAREHOUSE"
	
	// Exit data
	details.exitData.vLocateCoords1[0] = <<-0.900, 12.750, -5.640>>
	details.exitData.vLocateCoords2[0] = <<-7.360, 12.750, -3.180>>
	details.exitData.fLocateWidths[0] = 1.000
	details.exitData.fHeadings[0] = 0.0
	
	details.entryAnim.iOnEnterFadeOutStopSoundTime = 1500
	details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
	details.entryAnim.strOnEnterFadeOutFinishSoundName = "METAL_SHUTTER_RAISE_LOOP"
	
	details.entryAnim.strOnEnterGarageFadeOutFinishSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
	details.entryAnim.strOnEnterGarageFadeOutFinishSoundName = "Garage_Door_Close"
	
	details.entryAnim.strOnLeaveFadeOutStartSoundSet = "GTAO_Script_Doors_Faded_Screen_Sounds"
	details.entryAnim.strOnLeaveFadeOutStartSoundName = "Garage_Door_Open"
ENDPROC


PROC GET_IE_GARAGE_INTERIOR_TYPE_AND_POSITION(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtType, VECTOR &vPosition, FLOAT &fHeading  , BOOL bUseSecondaryInteriorDetails )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10
			vPosition = <<974.9542, -3000.0908, -35.0000>>
			fHeading = 0.0
			txtType = "imp_impexp_intwaremed"
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_IE_GARAGE_LOAD_SECONDARY_INTERIOR(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER_SET()
		SET_INTERIOR_FLOOR_INDEX(GET_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER())
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC _SET_CAM_COORDS_SAME_AS_IE_GARAGE_EXIT(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	VECTOR vGarageSyncedScenePos = ROTATE_VECTOR_ABOUT_Z(<< 231.848, -1006.707, -99.992 >>, 63.360)
	VECTOR vGarageSyncSceneRot = << 0.000, 0.000, -63.360 >>
	VECTOR vGarageCamPos = ROTATE_VECTOR_ABOUT_Z(<<233.2975, -1005.8097, -98.5457>>, 63.360)
	VECTOR vGarageCamRot = <<-26.8902, -0.0380, 129.5137>>
	FLOAT fGarageCameraFOV = 30.3982
	
	VECTOR vRelativeCamPos = vGarageCamPos - vGarageSyncedScenePos 
	FLOAT fRelativeRotZ = vGarageCamRot.Z - vGarageSyncSceneRot.Z
	
	entryAnim.cameraPos = ROTATE_VECTOR_ABOUT_Z(ROTATE_VECTOR_ABOUT_Z(entryAnim.syncScenePos, -1.0 * entryAnim.syncSceneRot.Z) + vRelativeCamPos, entryAnim.syncSceneRot.Z)
	entryAnim.cameraRot = vGarageCamRot
	entryAnim.cameraRot.Z = entryAnim.syncSceneRot.Z + fRelativeRotZ
	entryAnim.cameraFov = fGarageCameraFOV
ENDPROC

FUNC SIMPLE_INTERIOR_DETAILS_BS GET_IE_GARAGE_PROPERTIES_BS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_DETAILS_BS structReturn
	
	//BS1
	
	//BS2
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_PARENT_INT_SCRIPT_DOES_LOAD_SCENE)
	
	RETURN structReturn
ENDFUNC

PROC GET_IE_GARAGE_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS &details, BOOL bUseSecondaryInteriorDetails, BOOL bSMPLIntPreCache)
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	UNUSED_PARAMETER(bSMPLIntPreCache)		
	details.sProperties = GET_IE_GARAGE_PROPERTIES_BS(eSimpleInteriorID)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1 // La Puerta
			_INIT_GENERIC_WAREHOUSE_ELECTRIC_GARAGE_DOOR(details)
		
			details.entryAnim.dictionary = "anim@apt_trans@garage"
			details.entryAnim.clip = "gar_open_1_left"
			details.entryAnim.syncScenePos = <<-631.693, -1778.812, 22.980>>
			details.entryAnim.syncSceneRot = <<0.000, 0.000, 36.360>>
			details.entryAnim.startingPhase = 0.0
			details.entryAnim.endingPhase = 0.45
			details.entryAnim.establishingCameraPos = <<-634.1538, -1796.4481, 34.5912>>
			details.entryAnim.establishingCameraRot = <<-22.1262, -0.0000, 5.0337>>
			details.entryAnim.fEstablishingCameraFov = 50.0
			
			_SET_CAM_COORDS_SAME_AS_IE_GARAGE_EXIT(details.entryAnim)
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2 // La Mesa
			_INIT_GENERIC_WAREHOUSE_SHUTTER_DOOR(details)
			
			details.entryAnim.dictionary = "anim@apt_trans@garage"
			details.entryAnim.clip = "gar_open_1_left"
			details.entryAnim.syncScenePos = <<1007.344, -1854.104, 30.055>>
			details.entryAnim.syncSceneRot = <<0.000, 0.000, 84.240>>
			details.entryAnim.startingPhase = 0.0
			details.entryAnim.endingPhase = 0.45
			details.entryAnim.establishingCameraPos = <<1012.2180, -1868.8105, 40.1672>>
			details.entryAnim.establishingCameraRot = <<-16.8435, -0.0000, 30.4879>>
			details.entryAnim.fEstablishingCameraFov = 50.0
			
			_SET_CAM_COORDS_SAME_AS_IE_GARAGE_EXIT(details.entryAnim)
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3 // Davis
			_INIT_GENERIC_WAREHOUSE_ELECTRIC_GARAGE_DOOR(details)
			
			details.entryAnim.dictionary = "anim@apt_trans@garage"
			details.entryAnim.clip = "gar_open_1_left"
			details.entryAnim.syncScenePos = <<-72.690, -1820.721, 25.960>>
			details.entryAnim.syncSceneRot = <<0.000, -0.000, 138.960>>
			details.entryAnim.startingPhase = 0.0
			details.entryAnim.endingPhase = 0.45
			details.entryAnim.establishingCameraPos = <<-50.4334, -1820.4833, 40.6842>>
			details.entryAnim.establishingCameraRot = <<-21.9918, -0.0000, 89.4914>>
			details.entryAnim.fEstablishingCameraFov = 50.0
			
			_SET_CAM_COORDS_SAME_AS_IE_GARAGE_EXIT(details.entryAnim)
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4 // Strawberry
			_INIT_GENERIC_WAREHOUSE_ELECTRIC_GARAGE_DOOR(details)
			
			details.entryAnim.dictionary = "anim@apt_trans@garage"
			details.entryAnim.clip = "gar_open_1_left"
			details.entryAnim.syncScenePos = <<36.290, -1283.851, 28.300>>
			details.entryAnim.syncSceneRot = <<0.000, -0.000, -180.000>>
			details.entryAnim.startingPhase = 0.0
			details.entryAnim.endingPhase = 0.45
			details.entryAnim.establishingCameraPos = <<51.6316, -1261.6930, 47.7187>>
			details.entryAnim.establishingCameraRot = <<-20.6347, -0.0000, 135.6605>>
			details.entryAnim.fEstablishingCameraFov = 50.0
			
			_SET_CAM_COORDS_SAME_AS_IE_GARAGE_EXIT(details.entryAnim)
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5 // Murrieta Heights
			_INIT_GENERIC_WAREHOUSE_SHUTTER_DOOR( details)
			
			details.entryAnim.dictionary = "anim@apt_trans@garage"
			details.entryAnim.clip = "gar_open_1_left"
			details.entryAnim.syncScenePos = <<1213.935, -1251.067, 35.340>>
			details.entryAnim.syncSceneRot = <<0.000, 0.000, -4.680>>
			details.entryAnim.startingPhase = 0.025
			details.entryAnim.endingPhase = 0.45
			details.entryAnim.establishingCameraPos = <<1203.7787, -1268.0126, 45.1178>>
			details.entryAnim.establishingCameraRot = <<-26.6439, 0.0000, -50.7787>>
			details.entryAnim.fEstablishingCameraFov = 50.0
			
			_SET_CAM_COORDS_SAME_AS_IE_GARAGE_EXIT(details.entryAnim)
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6 // Cypress Flats
			_INIT_GENERIC_WAREHOUSE_DOOR(details)
			
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<808.9337, -2226.6355, 30.5702>>
			details.entryAnim.cameraRot = <<-8.7836, -0.0000, -2.5026>>
			details.entryAnim.cameraFov = 50.0
			details.entryAnim.syncScenePos = <<809.470, -2222.665, 28.602>>
			details.entryAnim.syncSceneRot = <<0.000, -0.000, -95.040>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<795.8560, -2239.9753, 43.3438>>
			details.entryAnim.establishingCameraRot = <<-25.9078, -0.0000, -21.3500>>
			details.entryAnim.fEstablishingCameraFov = 50.0
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7 // El Burro Heights
			_INIT_GENERIC_WAREHOUSE_DOOR(details)
			
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<1755.0826, -1652.7717, 113.9896>>
			details.entryAnim.cameraRot = <<-2.2520, -0.0000, 14.1658>>
			details.entryAnim.cameraFov = 50.0
			details.entryAnim.syncScenePos = <<1753.699, -1649.109, 111.650>>
			details.entryAnim.syncSceneRot = <<0.000, -0.000, 98.280>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<1770.4620, -1654.9950, 121.1074>>
			details.entryAnim.establishingCameraRot = <<-27.1992, -0.0000, 60.7515>>
			details.entryAnim.fEstablishingCameraFov = 50.0
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8 // Elysian Island
			_INIT_GENERIC_WAREHOUSE_ELECTRIC_GARAGE_DOOR(details)
			
			details.entryAnim.dictionary = "anim@apt_trans@garage"
			details.entryAnim.clip = "gar_open_1_left"
			details.entryAnim.syncScenePos = <<144.163, -3006.280, 6.025>>
			details.entryAnim.syncSceneRot = <<0.000, -0.000, -90.000>>
			details.entryAnim.startingPhase = 0.0
			details.entryAnim.endingPhase = 0.45
			details.entryAnim.establishingCameraPos = <<133.3796, -2997.3279, 16.4858>>
			details.entryAnim.establishingCameraRot = <<-23.3212, -0.0000, -128.0707>>
			details.entryAnim.fEstablishingCameraFov = 50.0
			
			_SET_CAM_COORDS_SAME_AS_IE_GARAGE_EXIT(details.entryAnim)
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9 // LSIA
			_INIT_GENERIC_WAREHOUSE_DOOR(details)
			
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-514.9109, -2200.7783, 8.5040>>
			details.entryAnim.cameraRot = <<-19.0944, -0.0000, 59.8399>>
			details.entryAnim.cameraFov = 50.0
			details.entryAnim.syncScenePos = <<-522.064, -2197.247, 5.396>>
			details.entryAnim.syncSceneRot = <<0.000, -0.000, 138.960>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<-518.5544, -2180.6301, 18.0885>>
			details.entryAnim.establishingCameraRot = <<-26.4987, -0.0000, -170.4673>>
			details.entryAnim.fEstablishingCameraFov = 50.0
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10 // LSIA
			_INIT_GENERIC_WAREHOUSE_DOOR(details)
			
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-1157.2069, -2167.5227, 14.6173>>
			details.entryAnim.cameraRot = <<4.5508, -0.0443, 47.9725>>
			details.entryAnim.cameraFov = 50.0000
			details.entryAnim.syncScenePos = <<-1160.481, -2162.972, 12.411>>
			details.entryAnim.syncSceneRot = <<0.000, 0.000, -23.040>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<-1154.2792, -2196.8950, 24.1398>>
			details.entryAnim.establishingCameraRot = <<-17.9349, 0.0000, -12.0432>>
			details.entryAnim.fEstablishingCameraFov = 50.0000
		BREAK
	ENDSWITCH
ENDPROC

FUNC STRING GET_IE_GARAGE_NAME(SIMPLE_INTERIORS eSimpleInteriorID)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1	RETURN "MP_WAREHOUSE_1"
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2	RETURN "MP_WAREHOUSE_2"
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3	RETURN "MP_WAREHOUSE_3"
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4 RETURN "MP_WAREHOUSE_4"
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5 RETURN "MP_WAREHOUSE_5"
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6 RETURN "MP_WAREHOUSE_6"
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7 RETURN "MP_WAREHOUSE_7"
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8 RETURN "MP_WAREHOUSE_8"
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9 RETURN "MP_WAREHOUSE_9"
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10 RETURN "MP_WAREHOUSE_10"
	ENDSWITCH
	
	RETURN "INVALID_IE_GARAGE_NAME"
ENDFUNC

FUNC STRING GET_IE_GARAGE_GAME_TEXT(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_GAME_TEXT eTextID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SWITCH eTextID
		CASE SIGT_PIM_INVITE_TOOLTIP			RETURN "PIM_HINVPIEWHS"	BREAK
		CASE SIGT_PIM_INVITE_MENU_LABEL			RETURN "PIM_INVIEWHS"	BREAK
		CASE SIGT_PIM_INVITE_MENU_TITLE			RETURN "PIM_TITLEIEW"	BREAK
		CASE SIGT_PROPERTY_INVITE_TICKER		RETURN "PIM_INVAIEWHS"	BREAK
		CASE SIGT_PROPERTY_INVITE_ALL_TICKER	RETURN ""				BREAK
		CASE SIGT_PROPERTY_INVITE_RECIPT		RETURN "CELL_IEWHSINV"	BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL SHOULD_IE_GARAGE_SHOW_PIM_INVITE_OPTION(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &tlReturn, BOOL bReturnMenuLabel, BOOL &bSelectable)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(tlReturn)
	UNUSED_PARAMETER(bSelectable)
	
	IF bReturnMenuLabel
		tlReturn = GET_IE_GARAGE_GAME_TEXT(eSimpleInteriorID, SIGT_PIM_INVITE_MENU_LABEL)
	ELSE
		tlReturn = "ciPI_TYPE_INVITE_TO_SIMPLE_INTERIOR - IE_GARAGE"
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR_OF_TYPE(SIMPLE_INTERIOR_TYPE_IE_GARAGE)
	AND (IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_BOSS) OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_OWNER)) // Are we in interior as owner of it?
	AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
	AND NOT GB_IS_PLAYER_ON_VEHICLE_EXPORT_MISSION(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_IE_GARAGE_INSIDE_BOUNDING_BOX(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX &bbox  , BOOL bUseSecondaryInteriorDetails )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10
			bbox.vInsideBBoxMin = <<908.7148, -3039.493, -50.0000>>
			bbox.vInsideBBoxMax = <<1032.3842, -2972.8308, -28.1500>>
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_IE_GARAGE_ENTRY_LOCATE(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vLocate)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1
			vLocate = <<-631.9747, -1779.1809, 22.9802>>
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2
			vLocate = <<1007.5104, -1855.0323, 30.0398>>
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3
			vLocate = <<-72.3389, -1820.9839, 25.9420>>
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4
			vLocate = <<36.8712, -1284.0281, 28.2924>>
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5
			vLocate = <<1213.2333, -1251.2490, 35.3258>>
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6
			vLocate = <<808.8002, -2222.4998, 28.6239>>
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7
			vLocate = <<1754.5463, -1649.1672, 111.6556>>
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8
			vLocate = <<144.3155, -3005.9189, 6.0309>>
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9
			vLocate = <<-521.2753, -2196.6880, 5.3940>>
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10
			vLocate = <<-1161.2921, -2163.7004, 12.3808>>
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_IE_WAREHOUSE_VEHICLE_ENTRY_LOCATE(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vLocate, VECTOR &vLocatePos1, VECTOR &vLocatePos2, FLOAT &fLocateWidth, FLOAT &fEnterHeading)
//	UNUSED_PARAMETER(vLocate)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1
			vLocate 		= <<-631.9747, -1779.1809, 22.9802>>
			vLocatePos1 	= <<-634.376465, -1777.827637, 23.041370>>
			vLocatePos2		= <<-631.889221, -1781.701294, 27.767784>> 
			fLocateWidth 	= 3.500000
			fEnterHeading 	= 294.1284
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2
			vLocate 		= <<1002.3339, -1855.0332, 30.0398>>
			vLocatePos1 	= <<999.666809, -1856.012695, 30.039833>>
			vLocatePos2 	= <<1004.717529, -1856.450684, 35.039833>> 
			fLocateWidth 	= 3.500000
			fEnterHeading	= 355.5651
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3
			vLocate 		= <<-72.3389, -1820.9839, 25.9420>>
			vLocatePos1 	= <<-73.590553, -1824.021851, 25.941975>>
			vLocatePos2 	= <<-69.529686, -1819.190308, 31.441975>> 
			fLocateWidth 	= 3.500000
			fEnterHeading	= 50.6780
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4
			vLocate 		= <<36.8712, -1284.0281, 28.2924>>
			vLocatePos1 	= <<37.755676, -1286.788330, 28.282730>>
			vLocatePos2 	= <<37.786163, -1281.272217, 33.781471>> 
			fLocateWidth 	= 3.500000
			fEnterHeading	= 89.5502
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5
			vLocate 		= <<1213.7523, -1256.4323, 34.2267>>
			vLocatePos1 	= <<1212.757202, -1253.991699, 34.226749>>
			vLocatePos2 	= <<1212.745728, -1258.884644, 39.226749>> 
			fLocateWidth 	= 3.500000
			fEnterHeading	= 274.7531
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6
			vLocate 		= <<804.6811, -2219.8789, 28.4375>>
			vLocatePos1 	= <<801.521118, -2220.598389, 28.433271>>
			vLocatePos2 	= <<807.639587, -2221.166016, 34.503288>> 
			fLocateWidth 	= 3.500000
			fEnterHeading	= 0.8961
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7	
			vLocate 		= <<1759.0916, -1646.5947, 111.6421>>
			vLocatePos1 	= <<1757.316650, -1648.010254, 111.650581>>
			vLocatePos2 	= <<1761.134521, -1647.348999, 115.647469>> 
			fLocateWidth 	= 3.500000
			fEnterHeading	= 14.9233
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8
			vLocate 		= <<144.3155, -3005.9189, 6.0309>>
			vLocatePos1 	= <<146.919083, -3004.888672, 6.030977>>
			vLocatePos2 	= <<141.675446, -3004.828125, 11.530924>> 
			fLocateWidth 	= 3.500000
			fEnterHeading	= 183.1470
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9
			vLocate 		= <<-514.7299, -2202.5208, 5.3940>>
			vLocatePos1 	= <<-509.973175, -2204.986572, 5.394024>>
			vLocatePos2 	= <<-518.478271, -2197.885742, 11.644024>> 
			fLocateWidth 	= 3.500000
			fEnterHeading	= 143.7869
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10
			vLocate			= <<-1151.7758, -2170.6150, 12.2712>>
			vLocatePos1 	= <<-1157.323120, -2166.800781, 12.267529>>
			vLocatePos2 	= <<-1147.981812, -2176.019775, 18.529188>> 
			fLocateWidth 	= 3.500000
			fEnterHeading	= 318.1663
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_IE_GARAGE_ENTRY_LOCATE_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT &iR, INT &iG, INT &iB, INT &iA)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
ENDPROC

FUNC BOOL SHOULD_THIS_IE_GARAGE_CORONA_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason = FALSE #ENDIF)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	#IF IS_DEBUG_BUILD
	UNUSED_PARAMETER(bPrintReason)
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_THIS_IE_GARAGE_LOCATE_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason #ENDIF)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_THIS_IE_GARAGE_LOCATE_BE_HIDDEN - Returning TRUE, IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY() = ", IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY())
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_THIS_IE_GARAGE_LOCATE_BE_HIDDEN - Returning TRUE, NETWORK_IS_ACTIVITY_SESSION() is TRUE")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_VEHICLE_EXPORT_BUY
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_THIS_IE_GARAGE_LOCATE_BE_HIDDEN - Returning TRUE, we are on steal mission.")
				ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT CAN_PLAYER_USE_PROPERTY(TRUE #IF IS_DEBUG_BUILD , bPrintReason #ENDIF)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_THIS_IE_GARAGE_LOCATE_BE_HIDDEN - Returning TRUE, CAN_PLAYER_USE_PROPERTY is FALSE and we dont have to have access.")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	BOOL bTempPrintReason = FALSE
	#IF IS_DEBUG_BUILD
	bTempPrintReason = bPrintReason
	#ENDIF
	
	IF IS_PLAYER_BLOCKED_FROM_PROPERTY(bTempPrintReason, PROPERTY_IE_WAREHOUSE)
	AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_THIS_IE_GARAGE_LOCATE_BE_HIDDEN - Returning TRUE, IS_PLAYER_BLOCKED_FROM_PROPERTY is TRUE")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_ARMORY_AIRCRAFT_PILOT_ENTERING()
	OR HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_THIS_IE_GARAGE_LOCATE_BE_HIDDEN TRUE player is entering avenger from pilot seat")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_HACKER_TRUCK_PILOT_ENTERING()
	OR IS_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_THIS_IE_GARAGE_LOCATE_BE_HIDDEN TRUE player is entering hacker truck from pilot seat")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_IE_GARAGE(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID)
	UNUSED_PARAMETER(iEntranceID)
		
	IF g_sMPTunables.bexec_disable_warehouse_entry
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(playerID)
		RETURN FALSE
	ENDIF

	// Only the owner of the garage can enter it.
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID)
		RETURN DOES_PLAYER_OWN_IE_GARAGE(GB_GET_THIS_PLAYER_GANG_BOSS(playerID), GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
	ELSE
		IF DOES_PLAYER_OWN_IE_GARAGE(playerID, GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_IE_GARAGE_IN_VEHICLE(SIMPLE_INTERIORS eSimpleInteriorID, MODEL_NAMES eModel)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(eModel)
	RETURN FALSE
ENDFUNC

FUNC STRING GET_IE_GARAGE_REASON_FOR_BLOCKED_ENTRY(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iEntranceUsed)
	UNUSED_PARAMETER(iEntranceUsed)
	UNUSED_PARAMETER(txtExtraString)
	
	IF SHOULD_BLOCK_SIMPLE_INTERIOR_ENTRY_FOR_ON_CALL()
		RETURN "SI_ENTR_BLCK15A"
	ENDIF
	
	IF DOES_PLAYER_OWN_IE_GARAGE(PLAYER_ID(), GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))

		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT GB_IS_PLAYER_IN_EXPORT_VEHICLE(PLAYER_ID())
		AND NOT IS_SVM_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			// You cannot enter the vehicle garage in this type of vehicle.
			RETURN "IE_WH_VEH_BLCK"
		ENDIF
		
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
				IF DOES_LOCAL_PLAYER_OWN_ANY_SVM()
					// Your Vehicle Warehouse is unavailable when being an MC President. You can request your Special Vehicles via the Vehicles section of the Interaction Menu.
					RETURN "IE_WHS_HLP_SV2"
				ELSE
					// Your Vehicle Warehouse is unavailable when being an MC President.
					RETURN "GARAGE_MC_BLOCK_P"
				ENDIF
			ELSE
				IF DOES_LOCAL_PLAYER_OWN_ANY_SVM()
					// Your Vehicle Warehouse is unavailable when being a member of an MC. You can request your Special Vehicles via the Vehicles section of the Interaction Menu.
					RETURN "IE_WHS_HLP_SV3"
				ELSE
					// Your Vehicle Warehouse is unavailable when being a member of an MC.
					RETURN "GARAGE_MC_BLOCK_M"
				ENDIF
			ENDIF
		ELSE
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
				PLAYER_INDEX playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
				
				IF NOT DOES_PLAYER_OWN_IE_GARAGE(playerBoss, GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
					IF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
						IF DOES_LOCAL_PLAYER_OWN_ANY_SVM()
							// Your Vehicle Warehouse is unavailable when working as an Associate for a CEO. You can request your Special Vehicles via the Vehicles section of the Interaction Menu.
							RETURN "IE_WHS_HLP_SV1"
						ELSE
							// Your Vehicle Warehouse is unavailable when working as a Associate for a CEO.
							RETURN "GARAGE_GOON_HLP1C"
						ENDIF
					ELSE
						IF DOES_LOCAL_PLAYER_OWN_ANY_SVM()
							//Your Vehicle Warehouse is unavailable when working as a Bodyguard for a VIP. You can request your Special Vehicles via the Vehicles section of the Interaction Menu.
							RETURN "IE_WHS_HLP_SV4"
						ELSE
							// Your Vehicle Warehouse is unavailable when working as a Bodyguard for a VIP.
							RETURN "GARAGE_GOON_HLP1"
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT GB_IS_PLAYER_IN_EXPORT_VEHICLE(PLAYER_ID())
		AND NOT IS_SVM_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			// You cannot enter the vehicle garage in this type of vehicle.
			RETURN "IE_WH_VEH_BLCK"
		ENDIF
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_VEHICLE_EXPORT_BUY
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				RETURN "IE_WH_MSSN_BLCK"
			ENDIF
		ENDIF
	ENDIF
	
	
	IF (DOES_PLAYER_OWN_IE_GARAGE(PLAYER_ID(), GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
	OR DOES_PLAYER_OWN_IE_GARAGE(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)))
		IF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
			RETURN "JUG_BLOCK_VWARE"
		ENDIF
		
		IF IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
			// You cannot enter whilst delivering a bounty.
			RETURN "BOUNTY_PROP_BLCK"
		ENDIF
		
	ENDIF
	
	IF IS_NPC_IN_VEHICLE()
		// You cannot enter whilst an NPC is in the vehicle.
		RETURN "NPC_BLOCK"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_IE_GARAGE_REASON_FOR_BLOCKED_EXIT(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iExitUsed)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(txtExtraString)
	UNUSED_PARAMETER(iExitUsed)
	
	IF SHOULD_JOB_ENTRY_TYPE_BLOCK_SIMPLE_INTERIOR_TRANSITION()
		RETURN "SI_EXIT_BLCK15A"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL CAN_PLAYER_INVITE_OTHERS_TO_IE_GARAGE(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)
	IF GB_IS_PLAYER_BOSS_OF_A_GANG(playerID)
	AND DOES_PLAYER_OWN_IE_GARAGE(playerID, GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_BE_INVITED_TO_IE_GARAGE(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)
	// Only invite players who are members of the same gang as the player who owns the garage.
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID, FALSE)
	AND DOES_PLAYER_OWN_IE_GARAGE(GB_GET_THIS_PLAYER_GANG_BOSS(playerID), GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
	AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(GET_PLAYER_PED(playerID))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_BE_INVITED_TO_IE_GARAGE_BY_PLAYER(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX pInvitingPlayer)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(pInvitingPlayer)
	RETURN TRUE
ENDFUNC

PROC GET_IE_GARAGE_ENTRANCE_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], SIMPLE_INTERIOR_LOCAL_EVENTS &eventOptions[], INT &iOptionsCount)
	txtTitle = GET_IE_GARAGE_NAME(eSimpleInteriorID)
	strOptions[0] = "IEWHS_ENTR_ALONE"
	eventOptions[0] = SI_EVENT_ENTRY_MENU_ENTER_ALONE
	strOptions[1] = "IEWHS_ENTR_ALL"
	eventOptions[1] = SI_EVENT_ENTRY_MENU_ENTER_WITH_NEARBY
	iOptionsCount = 2
ENDPROC

PROC PROCESS_IE_GARAGE_ENTRANCE_MENU(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_MENU &menu)
	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
	AND CAN_PLAYER_INVITE_OTHERS_TO_IE_GARAGE(PLAYER_ID(), eSimpleInteriorID)
		VECTOR vEntryLocate, vGoonPos
		GET_IE_GARAGE_ENTRY_LOCATE(eSimpleInteriorID, vEntryLocate)
		vEntryLocate.Z = 0.0
		
		PLAYER_INDEX playerGoon
		INT iPlayersCount, i
			
		REPEAT GB_GET_NUM_GOONS_IN_LOCAL_GANG() i
			playerGoon = GB_GET_GANG_GOON_AT_INDEX(PLAYER_ID(), i)
			
			IF playerGoon != INVALID_PLAYER_INDEX()
			AND playerGoon != PLAYER_ID()
			AND IS_NET_PLAYER_OK(playerGoon)
			AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(GET_PLAYER_PED(playerGoon))
			AND NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(playerGoon, eGB_GLOBAL_CLIENT_BITSET_1_RIVAL_BUSINESS_PLAYER)
			AND NOT IS_PLAYER_AN_ANIMAL(playerGoon)
				vGoonPos = GET_PLAYER_COORDS(playerGoon)
				vGoonPos.Z = 0.0
				
				IF VDIST(vGoonPos, vEntryLocate) <= GET_SIMPLE_INTERIOR_INVITE_RADIUS(eSimpleInteriorID)
					iPlayersCount += 1
				ENDIF
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR][IE_GARAGE] PROCESS_IE_GARAGE_ENTRANCE_MENU - Gang members around count: ", iPlayersCount)
		#ENDIF
		
		IF iPlayersCount = 0
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, FALSE)
		ELSE
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, TRUE)
		ENDIF
		
		IF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(1, menu)
			IF iPlayersCount > 0
				BROADCAST_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR(PLAYER_ID(), eSimpleInteriorID  , <<0,0,0>>  )
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][IE_GARAGE] PROCESS_IE_GARAGE_ENTRANCE_MENU - Not sending broadcast as there's no goons around entrance.")
				#ENDIF
			ENDIF
			
			g_SimpleInteriorData.eCurrentInteriorID = SIMPLE_INTERIOR_INVALID // We need to set this manually so DO_SIMPLE_INTERIOR_AUTOWARP will work, otherwise it will return TRUE straight away as it checks if local player is in the interior, it's a bit of a hack but a really small, harmless one.
					
			SET_SIMPLE_INTERIOR_EVENT_AUTOWARP_ACTIVE(TRUE, eSimpleInteriorID)
			g_SimpleInteriorData.bEventAutowarpOverrideCoords = FALSE
			g_SimpleInteriorData.bEventAutowarpSetInInterior = TRUE
		ENDIF
	ELSE
		SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, FALSE)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_IE_GARAGE(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer = -1)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	
	IF g_SimpleInteriorData.iAccessBS = 0
		// How did we get here? Probably script relaunched somehow, kick us out
		PRINTLN("[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_IE_GARAGE - AccessBS is 0")
		RETURN TRUE
	ENDIF
	
		// Don't allow access to members or bosses or an MC.
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
			PRINTLN("[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_IE_GARAGE - We're member of MC")
			RETURN TRUE
		ENDIF
	
	// Kick out players who entered as goons and stopped being goons while inside.
	// Even if they own a garage they need to reenter their own after they stopped being a goon.
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_GOON)
		IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			PRINTLN("[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_IE_GARAGE - No longer a goon")
			RETURN TRUE
		ENDIF
	ENDIF
	
	// If we walked in our own garage as boss or owner but quit and got hired as a goon - kick us out
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_BOSS)
	OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_OWNER)
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			PRINTLN("[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_IE_GARAGE - We're a goon in our own garage")
			RETURN TRUE
		ENDIF
	ENDIF
	
	// For garages, since there are no invites, not being able to walk in is equivalent to not being invited in.
	RETURN NOT CAN_PLAYER_ENTER_IE_GARAGE(PLAYER_ID(), eSimpleInteriorID, 0)
ENDFUNC

FUNC STRING GET_IE_GARAGE_KICK_OUT_REASON(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, TEXT_LABEL_31 &txtPlayerNameString, INT iInvitingPlayer = -1)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	UNUSED_PARAMETER(txtExtraString)
	UNUSED_PARAMETER(txtPlayerNameString)
	
	IF g_SimpleInteriorData.bExitAllTriggeredByOwner
		g_SimpleInteriorData.bExitAllTriggeredByOwner = FALSE
		// Owner has requested all to leave Garage.
		RETURN "MP_GARAGE_KICKe"
	ENDIF
	
	IF g_SimpleInteriorData.iAccessBS = 0
		// You no longer have access to this Garage.
		RETURN "MP_GARAGE_KICKc"
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_GOON)
			// We were a goon and are now part of an MC.
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
				IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
					// You no longer have access to the Garage as you became an MC President.
					RETURN "MP_GARAGE_KICKf"
				ELSE
					// You no longer have access to the Garage as you became a Prospect.
					RETURN "MP_GARAGE_KICKg"
				ENDIF
			ENDIF
		IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			// You no longer have access to the Garage as you stopped being a Bodyguard.
			RETURN "MP_GARAGE_KICKb"
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_BOSS)
	OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_OWNER)
			// We were a boss or had no affiliation to a gang and are now part of an MC.
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
				IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
					// You no longer have access to the Garage as you became an MC President.
					RETURN "MP_GARAGE_KICKf"
				ELSE
					// You no longer have access to the Garage as you became a Prospect.
					RETURN "MP_GARAGE_KICKg"
				ENDIF
			ENDIF
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			// Your garage is unavailable when working as a Bodyguard for a VIP.
			RETURN "MP_GARAGE_KICKd"
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC
 
PROC SET_IE_GARAGE_ACCESS_BS(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	
	globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
	globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS = 0
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_BOSS)
		ELSE
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_GOON)
		ENDIF
		
		// Create flags for everyone who potentially we would like to join in the garage.
		PLAYER_INDEX playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
		PLAYER_INDEX playerGoon
		
		IF playerBoss != INVALID_PLAYER_INDEX()
		AND playerBoss != PLAYER_ID()
			SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS, NATIVE_TO_INT(playerBoss))
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SET_IE_GARAGE_ACCESS_BS - We want to join boss: ", GET_PLAYER_NAME(playerBoss), " (", NATIVE_TO_INT(playerBoss), ")")
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SET_IE_GARAGE_ACCESS_BS - Check if there are any goons to join, how many of them are: ", GB_GET_NUM_GOONS_IN_LOCAL_GANG())
		#ENDIF
		
		/*
		INT i
		REPEAT GB_GET_NUM_GOONS_IN_LOCAL_GANG() i
			playerGoon = GB_GET_GANG_GOON_AT_INDEX(playerBoss, i)
			
			IF playerGoon != INVALID_PLAYER_INDEX()
			AND playerGoon != PLAYER_ID()
				SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS, NATIVE_TO_INT(playerGoon))
			
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SET_IE_GARAGE_ACCESS_BS - We want to join goon: ", GET_PLAYER_NAME(playerGoon), " (", NATIVE_TO_INT(playerGoon), ")")
				#ENDIF
			ENDIF
		ENDREPEAT
		*/
		
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			playerGoon = INT_TO_PLAYERINDEX(i)
			
			IF playerGoon != INVALID_PLAYER_INDEX()
			AND playerGoon != PLAYER_ID()
			AND playerGoon != playerBoss
				IF IS_NET_PLAYER_OK(playerGoon)
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerGoon, playerBoss)
						SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS, NATIVE_TO_INT(playerGoon))
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SET_IE_GARAGE_ACCESS_BS - We want to join goon: ", GET_PLAYER_NAME(playerGoon), " (", NATIVE_TO_INT(playerGoon), ")")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SET_IE_GARAGE_ACCESS_BS - Player ", GET_PLAYER_NAME(playerGoon), " (", NATIVE_TO_INT(playerGoon), ") isn't part of our gang.")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SET_IE_GARAGE_ACCESS_BS - Player ", NATIVE_TO_INT(playerGoon), " is not ok.")
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF playerGoon = playerBoss
						CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SET_IE_GARAGE_ACCESS_BS - Skipping boss.")
					ENDIF
					
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SET_IE_GARAGE_ACCESS_BS - Skipping us or invalid player index (", NATIVE_TO_INT(playerGoon), ")")
				#ENDIF
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SET_IE_GARAGE_ACCESS_BS - Players we should join BS: ", globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS)
		#ENDIF
	ELSE
		IF DOES_PLAYER_OWN_IE_GARAGE(PLAYER_ID(), GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SET_IE_GARAGE_ACCESS_BS - We are entering this garage as an owner.")
			#ENDIF
			
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_OWNER)
		ENDIF
	ENDIF	
ENDPROC

FUNC BOOL GET_IE_GARAGE_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle  , BOOL bUseSecondaryInteriorDetails = FALSE )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	UNUSED_PARAMETER(bSpawnInVehicle)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint 	= <<-4.1915, 9.9929, -5.6471>>
				    fSpawnHeading 	= 175.7734
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint 	= <<-2.8057, 9.4189, -5.6471>>
				    fSpawnHeading 	= 175.7734
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint 	= <<-4.7656, 8.6072, -5.6471>>
				    fSpawnHeading 	= 175.7734
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint 	= <<-5.5773, 10.5669, -5.6471>>
				    fSpawnHeading	= 175.7734
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint 	= <<-3.6175, 11.3787, -5.6471>>
				    fSpawnHeading 	= 175.7734
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint 	= <<-3.3798, 8.0332, -5.6471>>
				    fSpawnHeading 	= 175.7734
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint 	= <<-6.1514, 9.1812, -5.6471>>
				    fSpawnHeading 	= 175.7734
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint 	= <<-5.0033, 11.9526, -5.6471>>
				    fSpawnHeading 	= 175.7734
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS GET_IE_GARAGE_CUSTOM_INTERIOR_WARP_PARAMS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS eParams
	RETURN eParams
ENDFUNC

FUNC BOOL GET_IE_GARAGE_OUTSIDE_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle)

	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1
			SWITCH iSpawnPoint
				CASE 0
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-657.7001, -1768.6816, 23.7805>>
						fSpawnHeading 		= 123.9987
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-632.6060, -1782.2316, 23.0552>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-655.3964, -1771.8483, 23.7106>>
						fSpawnHeading 		= 123.9987
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-633.5104, -1781.0349, 23.0613>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-648.6033, -1767.2877, 23.3981>>
						fSpawnHeading 		= 125.1987
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-634.4148, -1779.8383, 23.0770>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-650.9087, -1764.1810, 23.4708>>
						fSpawnHeading 		= 125.1987
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-635.3192, -1778.6416, 23.0927>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-602.0138, -1795.5887, 22.4340>>
						fSpawnHeading 		= 213.9991
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-636.2236, -1777.4449, 23.0951>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 5
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-605.9370, -1796.5990, 22.4856>>
						fSpawnHeading 		= 214.1985
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-633.8027, -1783.1360, 23.1172>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 6
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-606.3922, -1789.0768, 22.5908>>
						fSpawnHeading 		= 213.9991
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-634.7071, -1781.9393, 23.1235>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 7
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-610.5400, -1790.1254, 22.6630>>
						fSpawnHeading 		= 214.1985
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-635.6115, -1780.7427, 23.1400>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 8
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-584.6882, -1803.9071, 22.1466>>
						fSpawnHeading 		= 145.5985
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-636.5159, -1779.5460, 23.1557>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 9
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-587.9329, -1801.6794, 22.1249>>
						fSpawnHeading 		= 145.5985
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-637.4203, -1778.3494, 23.1556>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 10
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-579.8873, -1797.3295, 21.8573>>
						fSpawnHeading 		= 145.5985
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-634.9994, -1784.0404, 23.1768>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 11
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-583.0121, -1794.9653, 21.8514>>
						fSpawnHeading 		= 145.5985
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-635.9038, -1782.8438, 23.1849>>
					    fSpawnHeading 		= 127.0800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 12
				    vSpawnPoint 		= <<-636.8082, -1781.6471, 23.2029>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 		= <<-637.7126, -1780.4504, 23.2167>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 		= <<-638.6171, -1779.2538, 23.2160>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 		= <<-636.1961, -1784.9448, 23.2387>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 		= <<-637.1005, -1783.7482, 23.2462>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 		= <<-638.0049, -1782.5515, 23.2646>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 		= <<-638.9094, -1781.3549, 23.2771>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 		= <<-639.8138, -1780.1582, 23.2757>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 		= <<-637.3928, -1785.8492, 23.2716>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 		= <<-638.2972, -1784.6526, 23.3090>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 		= <<-639.2017, -1783.4559, 23.3221>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 		= <<-640.1061, -1782.2593, 23.3352>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 		= <<-641.0105, -1781.0626, 23.3348>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 		= <<-638.5895, -1786.7537, 23.2275>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 		= <<-639.4940, -1785.5570, 23.3362>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 		= <<-640.3984, -1784.3604, 23.3793>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 		= <<-641.3028, -1783.1637, 23.3855>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 		= <<-642.2072, -1781.9670, 23.3939>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 		= <<-639.7863, -1787.6581, 23.1932>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 		= <<-640.6907, -1786.4614, 23.3052>>
				    fSpawnHeading 		= 127.0800
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2
			SWITCH iSpawnPoint
				CASE 0
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<979.9278, -1876.9127, 30.1646>>
					    fSpawnHeading 		= 176.7983
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1007.4059, -1856.6088, 30.0398>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<984.3694, -1877.1268, 29.9286>>
					    fSpawnHeading 		= 176.7983
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1005.9126, -1856.4677, 30.0398>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<982.5048, -1869.2777, 30.0577>>
					    fSpawnHeading 		= 176.7983
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1004.4193, -1856.3265, 30.0398>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<983.0613, -1860.7174, 30.0778>>
					    fSpawnHeading 		= 176.7983
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1002.9259, -1856.1854, 30.0398>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1012.4438, -1856.2101, 29.8898>>
					    fSpawnHeading 		= 176.7983
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1001.4326, -1856.0443, 30.0398>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 5
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1017.3239, -1856.5253, 29.8898>>
					    fSpawnHeading 		= 176.7983
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<999.9392, -1855.9032, 30.0398>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 6
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1013.1755, -1847.4745, 30.1998>>
					    fSpawnHeading 		= 176.7983
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<998.4459, -1855.7621, 30.0398>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 7
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1017.9980, -1847.7122, 30.2000>>
					    fSpawnHeading 		= 176.7983
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<996.9525, -1855.6210, 30.0398>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 8
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1016.3463, -1811.8414, 32.4611>>
					    fSpawnHeading 		= 356.7981
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1007.2648, -1858.1021, 29.8898>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 9
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1020.1740, -1812.2887, 32.5120>>
					    fSpawnHeading 		= 356.7981
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1005.7714, -1857.9609, 29.8898>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 10
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1015.8583, -1820.2073, 31.9309>>
					    fSpawnHeading 		= 356.7981
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1004.2781, -1857.8198, 29.8898>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 11
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1019.7333, -1820.7819, 31.9801>>
					    fSpawnHeading 		= 356.7981
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1002.7847, -1857.6787, 29.8898>>
					    fSpawnHeading 		= 174.6000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 12
				    vSpawnPoint 		= <<1001.2914, -1857.5376, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 		= <<999.7980, -1857.3965, 29.8979>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 		= <<998.3047, -1857.2554, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 		= <<996.8113, -1857.1143, 29.8979>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 		= <<1007.1236, -1859.5953, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 		= <<1005.6302, -1859.4542, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 		= <<1004.1369, -1859.3131, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 		= <<1002.6436, -1859.1720, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 		= <<1001.1502, -1859.0309, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 		= <<999.6569, -1858.8898, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 		= <<998.1635, -1858.7487, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 		= <<996.6702, -1858.6075, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 		= <<1006.9824, -1861.0886, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 		= <<1005.4891, -1860.9475, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 		= <<1003.9957, -1860.8064, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 		= <<1002.5024, -1860.6653, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 		= <<1001.0090, -1860.5242, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 		= <<999.5157, -1860.3831, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 		= <<998.0223, -1860.2419, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 		= <<996.5290, -1860.1008, 29.8898>>
				    fSpawnHeading 		= 174.6000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3
			SWITCH iSpawnPoint
				CASE 0
					IF bSpawnInVehicle
						vSpawnPoint 		= <<-62.0338, -1839.6195, 25.6831>>
						fSpawnHeading 		= 319.7973
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-69.8565, -1819.1049, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF bSpawnInVehicle
						vSpawnPoint			= <<-59.2190, -1841.8755, 25.5775>>
						fSpawnHeading		= 319.7973
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-70.6788, -1820.1117, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF bSpawnInVehicle
						vSpawnPoint			= <<-56.6504, -1844.0822, 25.4785>>
						fSpawnHeading		= 319.7973
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-71.5012, -1821.1185, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF bSpawnInVehicle
						vSpawnPoint			= <<-54.0422, -1846.3771, 25.3723>>
						fSpawnHeading		= 319.7973
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-72.3235, -1822.1254, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF bSpawnInVehicle
						vSpawnPoint			= <<-41.8232, -1809.3527, 25.5185>>
						fSpawnHeading		= 50.3972
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-73.1459, -1823.1322, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 5
					IF bSpawnInVehicle
						vSpawnPoint			= <<-48.0753, -1804.2408, 25.8218>>
						fSpawnHeading		= 50.3972
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-73.9682, -1824.1390, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 6
					IF bSpawnInVehicle
						vSpawnPoint			= <<-53.8064, -1799.1917, 26.2119>>
						fSpawnHeading		= 50.3972
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-74.7905, -1825.1459, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 7
					IF bSpawnInVehicle
						vSpawnPoint			= <<-59.6964, -1794.3314, 26.4409>>
						fSpawnHeading		= 50.3972
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-75.6129, -1826.1527, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 8
					IF bSpawnInVehicle
						vSpawnPoint			= <<-34.5170, -1831.2593, 24.9874>>
						fSpawnHeading		= 231.9969
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-68.8496, -1819.9272, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 9
					IF bSpawnInVehicle
						vSpawnPoint			= <<-28.8192, -1835.9943, 24.7421>>
						fSpawnHeading		= 231.9969
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-69.6720, -1820.9341, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 10
					IF bSpawnInVehicle
						vSpawnPoint			= <<-22.9205, -1840.8314, 24.2874>>
						fSpawnHeading		= 231.9969
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-70.4943, -1821.9409, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 11
					IF bSpawnInVehicle
						vSpawnPoint			= <<-16.5848, -1845.8246, 23.9532>>
						fSpawnHeading		= 231.9969
						RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-71.3167, -1822.9478, 25.9420>>
					    fSpawnHeading 		= 230.7600
						RETURN TRUE
					ENDIF
				BREAK
				CASE 12
				    vSpawnPoint 		= <<-72.1390, -1823.9546, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 		= <<-72.9613, -1824.9614, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 		= <<-73.7837, -1825.9683, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 		= <<-74.6060, -1826.9751, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 		= <<-67.8428, -1820.7496, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 		= <<-68.6651, -1821.7565, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 		= <<-69.4875, -1822.7633, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 		= <<-70.3098, -1823.7701, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 		= <<-71.1322, -1824.7770, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 		= <<-71.9545, -1825.7838, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 		= <<-72.7768, -1826.7906, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 		= <<-73.5992, -1827.7975, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 		= <<-66.8359, -1821.5720, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 		= <<-67.6583, -1822.5789, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 		= <<-68.4806, -1823.5857, 25.9422>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 		= <<-69.3030, -1824.5925, 25.9425>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 		= <<-70.1253, -1825.5994, 25.9424>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 		= <<-70.9476, -1826.6062, 25.9422>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 		= <<-71.7700, -1827.6130, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 		= <<-72.5923, -1828.6199, 25.9420>>
				    fSpawnHeading 		= 230.7600
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4
			SWITCH iSpawnPoint
				CASE 0
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<44.6433, -1296.7898, 28.1665>>
					    fSpawnHeading 		= 298.5938
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<37.8403, -1281.0281, 28.2825>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<46.5170, -1299.9558, 28.1904>>
					    fSpawnHeading 		= 298.5938
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<37.8321, -1282.3280, 28.2777>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<48.2259, -1303.0096, 28.2008>>
					    fSpawnHeading 		= 298.5938
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<37.8240, -1283.6279, 28.2738>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<49.9452, -1306.0573, 28.2014>>
					    fSpawnHeading 		= 298.5938
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<37.8158, -1284.9279, 28.2767>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<37.1659, -1306.7922, 28.2016>>
					    fSpawnHeading 		= 268.5924
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<37.8076, -1286.2278, 28.2804>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 5
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<37.3594, -1302.4570, 28.2447>>
					    fSpawnHeading 		= 268.5924
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<37.7995, -1287.5277, 28.2836>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 6
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<28.8771, -1306.5499, 28.1192>>
					    fSpawnHeading 		= 268.5924
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<37.7913, -1288.8276, 28.2885>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 7
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<29.0857, -1302.2067, 28.1848>>
					    fSpawnHeading 		= 268.5924
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<37.7831, -1290.1276, 28.2929>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 8
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<51.8603, -1309.0641, 28.1933>>
					    fSpawnHeading 		= 299.1927
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<39.1403, -1281.0363, 28.2915>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 9
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<53.7922, -1312.0549, 28.1986>>
					    fSpawnHeading 		= 299.1927
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<39.1321, -1282.3362, 28.2858>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 10	
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<55.7237, -1315.2097, 28.2201>>
					    fSpawnHeading 		= 299.1927
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<39.1239, -1283.6361, 28.2808>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 11
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<57.7420, -1318.5002, 28.2915>>
					    fSpawnHeading 		= 299.1927
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<39.1158, -1284.9360, 28.2792>>
					    fSpawnHeading 		= 269.6400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 12
				    vSpawnPoint 		= <<39.1076, -1286.2360, 28.2782>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 		= <<39.0994, -1287.5359, 28.2771>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 		= <<39.0913, -1288.8358, 28.2752>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 		= <<39.0831, -1290.1357, 28.2734>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 		= <<40.4403, -1281.0444, 28.3006>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 		= <<40.4321, -1282.3444, 28.2946>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 		= <<40.4239, -1283.6443, 28.2892>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 		= <<40.4157, -1284.9442, 28.2879>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 		= <<40.4076, -1286.2441, 28.2866>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 		= <<40.3994, -1287.5441, 28.2854>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 		= <<40.3912, -1288.8440, 28.2835>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 		= <<40.3831, -1290.1439, 28.2818>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 		= <<41.7402, -1281.0526, 28.1523>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 		= <<41.7321, -1282.3525, 28.1465>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 		= <<41.7239, -1283.6525, 28.1411>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 		= <<41.7157, -1284.9524, 28.1413>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 		= <<41.7076, -1286.2523, 28.1414>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 		= <<41.6994, -1287.5522, 28.2923>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 		= <<41.6912, -1288.8522, 28.2918>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 		= <<41.6831, -1290.1521, 28.2898>>
				    fSpawnHeading 		= 269.6400
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK		
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5
			SWITCH iSpawnPoint
				CASE 0
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1195.6464, -1261.6929, 34.2195>>
					    fSpawnHeading 		= 266.1938
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1212.1412, -1263.7797, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1195.9401, -1265.7366, 34.2205>>
					    fSpawnHeading 		= 266.1938
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1212.1085, -1262.4801, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1199.9099, -1269.6765, 34.2267>>
					    fSpawnHeading 		= 266.1938
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1212.0758, -1261.1805, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1199.6396, -1273.7750, 34.2236>>
					    fSpawnHeading 		= 266.1938
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1212.0431, -1259.8810, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1194.8899, -1284.9919, 34.1627>>
					    fSpawnHeading 		= 264.9908
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1212.0104, -1258.5814, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 5
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1194.3773, -1289.0171, 34.0681>>
					    fSpawnHeading 		= 264.9908
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1211.9777, -1257.2819, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 6
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1186.5833, -1284.2426, 33.9627>>
					    fSpawnHeading 		= 264.9908
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1211.9449, -1255.9823, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 7
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1186.1204, -1288.3510, 33.8788>>
					    fSpawnHeading 		= 264.9908
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1211.9122, -1254.6827, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 8
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1175.6416, -1292.6373, 33.8245>>
					    fSpawnHeading 		= 354.7906
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1210.8417, -1263.8124, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 9
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1179.2738, -1293.1221, 33.8035>>
					    fSpawnHeading 		= 354.7906
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1210.8090, -1262.5128, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 10
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1175.0420, -1301.1484, 33.8329>>
					    fSpawnHeading 		= 354.7906
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1210.7762, -1261.2133, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 11
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1178.6449, -1301.5192, 33.8056>>
					    fSpawnHeading 		= 354.7906
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1210.7435, -1259.9137, 34.2267>>
					    fSpawnHeading 		= 91.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 12
				    vSpawnPoint 		= <<1210.7108, -1258.6141, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 		= <<1210.6781, -1257.3146, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 		= <<1210.6454, -1256.0150, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 		= <<1210.6127, -1254.7155, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 		= <<1209.5421, -1263.8451, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 		= <<1209.5094, -1262.5455, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 		= <<1209.4767, -1261.2460, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 		= <<1209.4440, -1259.9464, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 		= <<1209.4113, -1258.6469, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 		= <<1209.3785, -1257.3473, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 		= <<1209.3458, -1256.0477, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 		= <<1209.3131, -1254.7482, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 		= <<1208.2426, -1263.8778, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 		= <<1208.2098, -1262.5782, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 		= <<1208.1771, -1261.2787, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 		= <<1208.1444, -1259.9791, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 		= <<1208.1117, -1258.6796, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 		= <<1208.0790, -1257.3800, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 		= <<1208.0463, -1256.0804, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 		= <<1208.0135, -1254.7809, 34.2267>>
				    fSpawnHeading 		= 91.4400
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6
			SWITCH iSpawnPoint
				CASE 0
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<814.3074, -2231.1616, 28.7591>>
					    fSpawnHeading 		= 136.3901
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<806.7189, -2222.3625, 28.5564>>
					    fSpawnHeading 		= 178.9200
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<818.5963, -2231.6650, 28.8321>>
					    fSpawnHeading 		= 136.3901
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<805.4192, -2222.3381, 28.5129>>
					    fSpawnHeading 		= 178.9200
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<822.9283, -2232.5984, 28.8594>>
					    fSpawnHeading 		= 136.3901
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<804.1194, -2222.3137, 28.4545>>
					    fSpawnHeading 		= 178.9200
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<828.4664, -2233.1035, 28.9445>>
					    fSpawnHeading 		= 136.3901
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<802.8197, -2222.2893, 28.4468>>
					    fSpawnHeading 		= 178.9200
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<779.4756, -2269.9834, 28.2386>>
					    fSpawnHeading 		= 86.1935
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<801.5200, -2222.2649, 28.4390>>
					    fSpawnHeading 		= 178.9200
						RETURN TRUE
					ENDIF
				BREAK
				CASE 5
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<779.1962, -2273.8389, 28.2358>>
					    fSpawnHeading 		= 86.1935
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<806.6945, -2223.6624, 28.5693>>
					    fSpawnHeading 		= 178.9200
						RETURN TRUE
					ENDIF
				BREAK
				CASE 6
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<778.7557, -2277.9312, 28.1127>>
					    fSpawnHeading 		= 86.1935
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<805.3947, -2223.6379, 28.5246>>
					    fSpawnHeading 		= 178.9200
						RETURN TRUE
					ENDIF
				BREAK
				CASE 7
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<778.3351, -2281.8157, 27.9724>>
					    fSpawnHeading 		= 86.1935
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<804.0950, -2223.6135, 28.4952>>
					    fSpawnHeading 		= 178.9200
						RETURN TRUE
					ENDIF
				BREAK
				CASE 8
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<777.4008, -2288.0686, 27.7000>>
					    fSpawnHeading 		= 86.1935
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<802.7952, -2223.5891, 28.4875>>
					    fSpawnHeading 		= 85.7933
						RETURN TRUE
					ENDIF
				BREAK
				CASE 9
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<776.8676, -2292.3162, 27.4908>>
					    fSpawnHeading 		= 85.7933
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<801.4955, -2223.5647, 28.4797>>
					    fSpawnHeading 		= 178.9200
						RETURN TRUE
					ENDIF
				BREAK
				CASE 10
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<776.4733, -2296.1909, 27.2854>>
					    fSpawnHeading 		= 85.7933
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<806.6700, -2224.9622, 28.5838>>
					    fSpawnHeading 		= 178.9200
						RETURN TRUE
					ENDIF
				BREAK
				CASE 11
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<776.2014, -2300.2810, 26.9733>>
					    fSpawnHeading 		= 85.7933
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<805.3702, -2224.9377, 28.5437>>
					    fSpawnHeading 		= 178.9200
						RETURN TRUE
					ENDIF
				BREAK
				CASE 12
				    vSpawnPoint 		= <<804.0705, -2224.9133, 28.5359>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 		= <<802.7708, -2224.8889, 28.5085>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 		= <<801.4710, -2224.8645, 28.4790>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 		= <<806.6455, -2226.2620, 28.6176>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 		= <<805.3458, -2226.2375, 28.5875>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 		= <<804.0460, -2226.2131, 28.5615>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 		= <<802.7463, -2226.1887, 28.5343>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 		= <<801.4465, -2226.1643, 28.5070>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 		= <<806.6210, -2227.5618, 28.6616>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 		= <<805.3213, -2227.5374, 28.6379>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 		= <<804.0215, -2227.5129, 28.6159>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 		= <<802.7218, -2227.4885, 28.5944>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 		= <<801.4221, -2227.4641, 28.5753>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 		= <<806.5966, -2228.8616, 28.6682>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 		= <<805.2968, -2228.8372, 28.6386>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 		= <<803.9971, -2228.8127, 28.6174>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 		= <<802.6973, -2228.7883, 28.5957>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 		= <<801.3976, -2228.7639, 28.5766>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 		= <<806.5721, -2230.1614, 28.6749>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 		= <<805.2723, -2230.1370, 28.6390>>
				    fSpawnHeading 		= 178.9200
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7
			SWITCH iSpawnPoint
				CASE 0
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1732.3636, -1699.8519, 111.5346>>
					    fSpawnHeading 		= 182.9893
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1767.0513, -1647.8134, 111.6467>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1728.9568, -1701.9082, 111.5529>>
					    fSpawnHeading 		= 182.9893
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1765.5760, -1648.0852, 111.6503>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1732.0068, -1691.7451, 111.6779>>
					    fSpawnHeading 		= 182.9893
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1764.1008, -1648.3571, 111.6528>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1728.5933, -1694.0569, 111.5217>>
					    fSpawnHeading 		= 182.9893
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1762.6256, -1648.6289, 111.6545>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1730.3636, -1681.6161, 111.6619>>
					    fSpawnHeading 		= 182.9893
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1761.1504, -1648.9008, 111.6538>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 5
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1727.0720, -1683.1447, 111.6702>>
					    fSpawnHeading 		= 182.9893
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1759.6752, -1649.1726, 111.6562>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 6
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1730.1243, -1673.2559, 111.5968>>
					    fSpawnHeading 		= 182.9893
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1758.2000, -1649.4445, 111.6584>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 7	
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1726.7723, -1675.2249, 111.5985>>
					    fSpawnHeading 		= 182.9893
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1756.7247, -1649.7163, 111.6591>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 8	
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1716.8981, -1666.0059, 111.5213>>
					    fSpawnHeading 		= 238.1895
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1767.3231, -1649.2886, 111.6580>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 9
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1716.4252, -1661.6436, 111.4922>>
					    fSpawnHeading 		= 238.1895
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1765.8479, -1649.5604, 111.6611>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 10
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1716.2554, -1657.6045, 111.4835>>
					    fSpawnHeading 		= 238.1895
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1764.3727, -1649.8323, 111.6585>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 11
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<1715.2135, -1653.1764, 111.5042>>
					    fSpawnHeading 		= 238.1895
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<1762.8975, -1650.1041, 111.6574>>
					    fSpawnHeading 		= 190.4400
						RETURN TRUE
					ENDIF
				BREAK
				CASE 12
				    vSpawnPoint 		= <<1761.4222, -1650.3760, 111.6587>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 		= <<1759.9470, -1650.6478, 111.6611>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 		= <<1758.4718, -1650.9197, 111.6638>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 		= <<1756.9966, -1651.1915, 111.6683>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 		= <<1767.5950, -1650.7638, 111.6655>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 		= <<1766.1198, -1651.0356, 111.6606>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 		= <<1764.6445, -1651.3075, 111.6596>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 		= <<1763.1693, -1651.5793, 111.6601>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 		= <<1761.6941, -1651.8512, 111.6617>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 		= <<1760.2189, -1652.1230, 111.6657>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 		= <<1758.7437, -1652.3949, 111.6726>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 		= <<1757.2684, -1652.6667, 111.6784>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 		= <<1767.8668, -1652.2390, 111.6634>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 		= <<1766.3916, -1652.5109, 111.6607>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 		= <<1764.9164, -1652.7827, 111.6589>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 		= <<1763.4412, -1653.0546, 111.6613>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 		= <<1761.9659, -1653.3264, 111.6665>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 		= <<1760.4907, -1653.5983, 111.6768>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 		= <<1759.0155, -1653.8701, 111.6833>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 		= <<1757.5403, -1654.1420, 111.6889>>
				    fSpawnHeading 		= 190.4400
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8
			SWITCH iSpawnPoint
				CASE 0
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<179.0235, -2983.0398, 4.9006>>
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<142.0598, -3004.6887, 6.0309>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<178.9990, -2987.1853, 4.8741>>
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<143.5598, -3004.6887, 6.0309>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<172.0389, -2988.0076, 4.8866>>
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<145.0598, -3004.6887, 6.0309>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<172.1585, -2992.2261, 4.8364>>
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<146.5598, -3004.6887, 6.0310>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<179.2999, -2993.6375, 4.7770>>
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<142.0598, -3003.1887, 6.0309>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 5
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<179.3002, -2998.5151, 4.7422>>
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<143.5598, -3003.1887, 6.0309>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 6
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<172.1279, -2998.1753, 4.7668>>
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<145.0598, -3003.1887, 6.0310>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 7
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<172.6838, -3003.1189, 4.7802>>
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<146.5598, -3003.1887, 6.0310>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 8
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<178.9094, -2966.4644, 4.9906>>
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<142.0598, -3001.6887, 6.0310>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 9
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<177.6397, -2962.1677, 4.9975>> 
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<143.5598, -3001.6887, 6.0310>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 10
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<172.2873, -2971.2566, 4.9236>>
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<145.0598, -3001.6887, 6.0311>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 11
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<170.9007, -2967.4563, 4.9488>>
					    fSpawnHeading 		= 305.1886
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<146.5598, -3001.6887, 6.0311>>
					    fSpawnHeading 		= 0.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 12
				    vSpawnPoint 		= <<142.0598, -3000.1887, 6.0310>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 		= <<143.5598, -3000.1887, 6.0311>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 		= <<145.0598, -3000.1887, 6.0311>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 		= <<146.5598, -3000.1887, 6.0312>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 		= <<142.0598, -2998.6887, 6.0310>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 		= <<143.5598, -2998.6887, 6.0311>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 		= <<145.0598, -2998.6887, 6.0311>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 		= <<146.5598, -2998.6887, 6.0312>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 		= <<142.0598, -2997.1887, 6.0310>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 		= <<143.5598, -2997.1887, 6.0311>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 		= <<145.0598, -2997.1887, 6.0312>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 		= <<146.5598, -2997.1887, 6.0313>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 		= <<142.0598, -2995.6887, 6.0310>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 		= <<143.5598, -2995.6887, 6.0311>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 		= <<145.0598, -2995.6887, 6.0312>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 		= <<146.5598, -2995.6887, 6.0313>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 		= <<142.0598, -2994.1887, 6.0311>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 		= <<143.5598, -2994.1887, 6.0312>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 		= <<145.0598, -2994.1887, 6.0313>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 		= <<146.5598, -2994.1887, 6.0314>>
				    fSpawnHeading 		= 0.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9
			SWITCH iSpawnPoint
				CASE 0
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-482.7018, -2168.9954, 8.4192>>
					    fSpawnHeading 		= 323.3885
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-516.6584, -2199.8767, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-479.5692, -2171.4268, 8.5546>>
					    fSpawnHeading 		= 323.3885
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-515.4848, -2200.8108, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-487.8007, -2175.8528, 8.0351>>
					    fSpawnHeading 		= 323.3885
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-514.3112, -2201.7449, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-484.4329, -2178.4063, 8.1440>>
					    fSpawnHeading 		= 323.3885
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-513.1376, -2202.6790, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-490.9238, -2160.8613, 8.2446>>
					    fSpawnHeading 		= 312.9879
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-511.9641, -2203.6130, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 5
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-494.0761, -2158.4985, 8.1971>>
					    fSpawnHeading 		= 312.9879
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-510.7905, -2204.5471, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 6
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-496.8955, -2166.5959, 7.8227>>
					    fSpawnHeading 		= 312.9879
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-515.7242, -2198.7031, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 7
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-499.7228, -2163.8369, 7.8161>>
					    fSpawnHeading 		= 312.9879
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-514.5506, -2199.6372, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 8
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-471.1009, -2164.7480, 8.7873>>
					    fSpawnHeading 		= 15.9878
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-513.3770, -2200.5713, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 9
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-467.0619, -2164.8411, 8.9238>>
					    fSpawnHeading 		= 15.9878
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-512.2034, -2201.5054, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 10
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-468.8961, -2172.7224, 8.9299>>
					    fSpawnHeading 		= 15.9878
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-511.0298, -2202.4395, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 11
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-464.9085, -2172.7090, 9.0347>>
					    fSpawnHeading 		= 15.9878
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-509.8563, -2203.3735, 5.3940>>
					    fSpawnHeading 		= 321.4800
						RETURN TRUE
					ENDIF
				BREAK
				CASE 12
				    vSpawnPoint 		= <<-514.7900, -2197.5295, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 		= <<-513.6164, -2198.4636, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 		= <<-512.4428, -2199.3977, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 		= <<-511.2692, -2200.3318, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 		= <<-510.0956, -2201.2659, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 		= <<-508.9221, -2202.2000, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 		= <<-513.8558, -2196.3560, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 		= <<-512.6822, -2197.2900, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 		= <<-511.5086, -2198.2241, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 		= <<-510.3350, -2199.1582, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 		= <<-509.1614, -2200.0923, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 		= <<-507.9879, -2201.0264, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 		= <<-512.9216, -2195.1824, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 		= <<-511.7480, -2196.1165, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 		= <<-510.5744, -2197.0505, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 		= <<-509.4008, -2197.9846, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 		= <<-508.2272, -2198.9187, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 		= <<-507.0536, -2199.8528, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 		= <<-511.9874, -2194.0088, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 		= <<-510.8138, -2194.9429, 5.3940>>
				    fSpawnHeading 		= 321.4800
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10
			SWITCH iSpawnPoint
				CASE 0
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1176.5660, -2193.2842, 12.1952>>
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1149.4368, -2175.3025, 12.2749>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1172.9907, -2195.3252, 12.1953>>
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1150.5157, -2174.2605, 12.2710>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2	
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1180.9192, -2200.9666, 12.1951>>
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1151.5947, -2173.2185, 12.2669>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1177.1698, -2203.1072, 12.1953>>
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1152.6737, -2172.1765, 12.2654>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1169.5775, -2197.2720, 12.1954>>
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1153.7527, -2171.1345, 12.2648>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 5
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1166.1278, -2199.2253, 12.1955>>
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1154.8317, -2170.0925, 12.2647>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 6
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1173.7153, -2204.9226, 12.1954>>
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1155.9106, -2169.0505, 12.2646>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 7
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1170.4902, -2206.7808, 12.1956>>
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1156.9896, -2168.0085, 12.2646>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 8
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1162.5282, -2201.3745, 12.1957>>
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1150.4788, -2176.3816, 12.2637>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 9
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1159.0155, -2203.3796, 12.1959>>
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1151.5577, -2175.3396, 12.2590>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 10
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1166.6667, -2208.7385, 12.1959>> 
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1152.6367, -2174.2976, 12.2543>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 11
					IF bSpawnInVehicle
					    vSpawnPoint 		= <<-1163.1830, -2210.7485, 12.1959>>
					    fSpawnHeading 		= 329.7995
					    RETURN TRUE
					ELSE
					    vSpawnPoint 		= <<-1153.7157, -2173.2556, 12.2536>>
					    fSpawnHeading 		= 136.0000
						RETURN TRUE
					ENDIF
				BREAK
				CASE 12
				    vSpawnPoint 		= <<-1154.7947, -2172.2136, 12.2531>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint 		= <<-1155.8737, -2171.1716, 12.2526>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint 		= <<-1156.9526, -2170.1296, 12.2522>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint 		= <<-1158.0316, -2169.0876, 12.2518>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 16
				    vSpawnPoint 		= <<-1151.5208, -2177.4607, 12.2296>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 17
				    vSpawnPoint 		= <<-1152.5997, -2176.4187, 12.2292>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 18
				    vSpawnPoint 		= <<-1153.6787, -2175.3767, 12.2288>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 19
				    vSpawnPoint 		= <<-1154.7577, -2174.3347, 12.2285>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 20
				    vSpawnPoint 		= <<-1155.8367, -2173.2927, 12.2280>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 21
				    vSpawnPoint 		= <<-1156.9156, -2172.2507, 12.2276>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 22
				    vSpawnPoint 		= <<-1157.9946, -2171.2087, 12.2311>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 23
				    vSpawnPoint 		= <<-1159.0736, -2170.1667, 12.2425>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 24
				    vSpawnPoint 		= <<-1152.5627, -2178.5398, 12.2045>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 25
				    vSpawnPoint 		= <<-1153.6417, -2177.4978, 12.2041>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 26
				    vSpawnPoint 		= <<-1154.7207, -2176.4558, 12.2037>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 27
				    vSpawnPoint 		= <<-1155.7997, -2175.4138, 12.2034>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 28
				    vSpawnPoint 		= <<-1156.8787, -2174.3718, 12.2038>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 29
				    vSpawnPoint 		= <<-1157.9576, -2173.3298, 12.2146>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 30
				    vSpawnPoint 		= <<-1159.0366, -2172.2878, 12.2243>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
				CASE 31
				    vSpawnPoint 		= <<-1160.1156, -2171.2458, 12.2340>>
				    fSpawnHeading 		= 136.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_IE_GARAGE_ASYNC_SPAWN_GRID_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)

//	SWITCH eSimpleInteriorID
//		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1			RETURN GRID_SPAWN_LOCATION_IE_WAREHOUSE_1
//		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2			RETURN GRID_SPAWN_LOCATION_IE_WAREHOUSE_2
//		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3			RETURN GRID_SPAWN_LOCATION_IE_WAREHOUSE_3
//		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4			RETURN GRID_SPAWN_LOCATION_IE_WAREHOUSE_4
//		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5			RETURN GRID_SPAWN_LOCATION_IE_WAREHOUSE_5
//		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6			RETURN GRID_SPAWN_LOCATION_IE_WAREHOUSE_6
//		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7			RETURN GRID_SPAWN_LOCATION_IE_WAREHOUSE_7
//		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8			RETURN GRID_SPAWN_LOCATION_IE_WAREHOUSE_8
//		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9			RETURN GRID_SPAWN_LOCATION_IE_WAREHOUSE_9
//		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10		RETURN GRID_SPAWN_LOCATION_IE_WAREHOUSE_10
//	ENDSWITCH
	
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_IE_GARAGE_ASYNC_SPAWN_GRID_INSIDE_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)

	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10
			RETURN GRID_SPAWN_LOCATION_IE_WAREHOUSE_INSIDE
		BREAK
	ENDSWITCH
	
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

FUNC VECTOR GET_IE_GARAGE_TELEPORT_IN_SVM_COORD(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN <<978.2659, -2999.1682, -48.6471>>
ENDFUNC

PROC GET_IE_GARAGE_EXIT_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], INT &iOptionsCount, INT iCurrentExit)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iCurrentExit)
	IF COUNT_OF(strOptions) != SIMPLE_INTERIOR_MENU_MAX_OPTIONS
		CASSERTLN(DEBUG_PROPERTY, "GET_CLUBHOUSE_EXIT_MENU_TITLE_AND_OPTIONS - options array has wrong size.")
		EXIT
	ENDIF
	
	txtTitle = "IE_WH_EXIT"
	iOptionsCount = 2
	strOptions[0] = "IE_WH_EXITa"
	strOptions[1] = "IE_WH_EXITb"
ENDPROC

FUNC BOOL CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_IE_GARAGE(SIMPLE_INTERIORS eSimpleInteriorID, INT iExitId)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iExitId)
	RETURN IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_OWNER) OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_IE_GARAGE_ACCESS_BS_BOSS)
ENDFUNC

FUNC BOOL _CAN_PLAYER_TAKE_THIS_CAR_TO_THEIR_IE_GARAGE_OFF_MISSION(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintDebug = FALSE #ENDIF)
	IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
	AND NOT AM_I_ORIGINAL_SELLER_OF_EXPORT_VEHICLE(g_IEDeliveryData.vehCurrentlyDelivering)
		IE_DROPOFF eDropoffID = GET_IE_DROPOFF_FROM_IMPORT_EXPORT_WAREHOUSE(GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		PLAYER_INDEX pDeliverer = IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(g_IEDeliveryData.vehCurrentlyDelivering)
		
		//#IF IS_DEBUG_BUILD
		//	IF bPrintDebug
		//		PRINTLN("[IE_DELIVERY] _CAN_PLAYER_TAKE_THIS_CAR_TO_THEIR_IE_GARAGE_OFF_MISSION - OK so we are delivering something and we're not original seller, simple interior: ", GET_SIMPLE_INTERIOR_DEBUG_NAME(eSimpleInteriorID), " (", ENUM_TO_INT(eSimpleInteriorID), ")")
		//	ENDIF
		//#ENDIF
		
		IF pDeliverer != INVALID_PLAYER_INDEX()
			IF pDeliverer = PLAYER_ID()
			
				BOOL bCanDo = IE_DELIVERY_CAN_LOCAL_PLAYER_USE_THIS_DROPOFF_IN_CURRENT_VEHICLE(eDropoffID #IF IS_DEBUG_BUILD , bPrintDebug #ENDIF)
			
				#IF IS_DEBUG_BUILD
					IF bPrintDebug
						PRINTLN("[IE_DELIVERY] _CAN_PLAYER_TAKE_THIS_CAR_TO_THEIR_IE_GARAGE_OFF_MISSION - We are the deliverer and, can we deliver? ", GET_STRING_FROM_BOOL(bCanDo))
					ENDIF
				#ENDIF
			
				// We are driving this car so if we can use this dropoff then yeah
				RETURN bCanDo
			ELSE
				
				#IF IS_DEBUG_BUILD
					IF bPrintDebug
						PRINTLN("[IE_DELIVERY] _CAN_PLAYER_TAKE_THIS_CAR_TO_THEIR_IE_GARAGE_OFF_MISSION - We're not a deliverer...")
					ENDIF
				#ENDIF
			
				// We are not driving this car so can only use this dropoff if the player who drives it is in the same gang
				IF GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(pDeliverer, PLAYER_ID())
					INT iBit, iArray
					IE_DROPOFF_GET_BITSET_ARRAY_AND_INDEX(eDropoffID, iArray, iBit)
					BOOL bCanDo = IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(pDeliverer)].IEDeliveryBD.iActiveDropoffs[iArray], iBit)
					
					#IF IS_DEBUG_BUILD
						IF bPrintDebug
							PRINTLN("[IE_DELIVERY] _CAN_PLAYER_TAKE_THIS_CAR_TO_THEIR_IE_GARAGE_OFF_MISSION - ...but are in the same gang. Can they deliver? ", GET_STRING_FROM_BOOL(bCanDo))
						ENDIF
					#ENDIF
					
					RETURN bCanDo
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_IE_GARAGE_MAX_BLIPS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN MAX_IE_GARAGE_BLIPS
ENDFUNC

FUNC BOOL SHOULD_IE_GARAGE_BE_BLIPPED_THIS_FRAME(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	UNUSED_PARAMETER(iBlipIndex)

//	#IF IS_DEBUG_BUILD
//		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] SHOULD_IE_GARAGE_BE_BLIPPED_THIS_FRAME - Blipping IE garage this frame.")
//		RETURN TRUE
//	#ENDIF

	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
		IF IE_DELIVERY_CAN_LOCAL_PLAYER_USE_THIS_DROPOFF_IN_CURRENT_VEHICLE(GET_IE_DROPOFF_FROM_IMPORT_EXPORT_WAREHOUSE(GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)))
			IF AM_I_ORIGINAL_SELLER_OF_EXPORT_VEHICLE(g_IEDeliveryData.vehCurrentlyDelivering)
				RETURN FALSE // we use radar objective to set a blip for dropoffs
			ENDIF
		ENDIF
	ENDIF
	
	// Only blip the garage if the player owns it.
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			IF DOES_PLAYER_OWN_IE_GARAGE(PLAYER_ID(), GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
				RETURN TRUE
			ENDIF
		ELSE
			IF DOES_PLAYER_OWN_IE_GARAGE(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF DOES_PLAYER_OWN_IE_GARAGE(PLAYER_ID(), GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_IE_GARAGE_BLIP_SHORT_RANGE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	
	IF GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_BUSINESS_BATTLE(PLAYER_ID())
	OR (GB_IS_BUSINESS_BATTLES_MISSION(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()))
	AND GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
	AND GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID()))
		RETURN TRUE
	ENDIF
	
	IF _CAN_PLAYER_TAKE_THIS_CAR_TO_THEIR_IE_GARAGE_OFF_MISSION(eSimpleInteriorID)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BLIP_SPRITE GET_IE_GARAGE_BLIP_SPRITE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN RADAR_TRACE_WAREHOUSE_VEHICLE
ENDFUNC

FUNC VECTOR GET_IE_GARAGE_BLIP_COORDS(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(iBlipIndex)
	
	VECTOR vBlipCoords
	GET_IE_GARAGE_ENTRY_LOCATE(eSimpleInteriorID, vBlipCoords)
	RETURN vBlipCoords
ENDFUNC

FUNC BLIP_PRIORITY GET_IE_GARAGE_BLIP_PRIORITY(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN BLIPPRIORITY_MED
ENDFUNC

PROC MAINTAIN_IE_GARAGE_BLIP_EXTRA_FUNCTIONALITY(SIMPLE_INTERIORS eSimpleInteriorID, BLIP_INDEX &blipIndex, BLIP_INDEX &OverlayBlipIndex, INT &iBlipType, INT &iBlipNameHash, INT iBlipIndex)
	UNUSED_PARAMETER(OverlayBlipIndex)
	UNUSED_PARAMETER(iBlipIndex)
	
	IMPORT_EXPORT_GARAGES eIEWarehouseID = GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	BOOL bShouldUpdate = FALSE
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		
		IF _CAN_PLAYER_TAKE_THIS_CAR_TO_THEIR_IE_GARAGE_OFF_MISSION(eSimpleInteriorID)
			IF iBlipType != 4
				SET_BLIP_ROUTE(blipIndex, TRUE)
				SET_BLIP_SCALE(blipIndex, IE_DELIVERY_BLIP_SCALE)
				SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGH)
				SET_BLIP_FLASH_TIMER(blipIndex, IE_DELIVERY_BLIP_FLASH_TIME)
				#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][IE_GARAGE] MAINTAIN_IE_GARAGE_BLIP_NAME - We can deliver to this warehouse so flashing blip and scaling it.")
				#ENDIF
				iBlipType = 4
				EXIT
			ENDIF
		ELSE
			IF iBlipType != 1
				IF iBlipType = 4
					SET_BLIP_ROUTE(blipIndex, FALSE)
					SET_BLIP_SCALE(blipIndex, BLIP_SIZE_NETWORK_COORD)
					SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_MED)
					#IF IS_DEBUG_BUILD
						PRINTLN("[SIMPLE_INTERIOR][IE_GARAGE] MAINTAIN_IE_GARAGE_BLIP_NAME - Resetting scale and route (1)")
					#ENDIF
				ENDIF
				bShouldUpdate = TRUE
			ENDIF
			
			TEXT_LABEL_63 strBlipName = GB_GET_PLAYER_ORGANIZATION_NAME(PLAYER_ID())
			
			IF NOT bShouldUpdate
				IF NOT IS_STRING_NULL_OR_EMPTY(strBlipName)
				AND GET_HASH_KEY(strBlipName) != iBlipNameHash
					bShouldUpdate = TRUE
				ENDIF
			ENDIF
			
			IF bShouldUpdate
				BEGIN_TEXT_COMMAND_SET_BLIP_NAME("IE_WHOUSE_BLIP2") // ~a~ Vehicle Warehouse
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strBlipName)
				END_TEXT_COMMAND_SET_BLIP_NAME(blipIndex)
				iBlipNameHash = GET_HASH_KEY(strBlipName)
				iBlipType = 1
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_WAREHOUSE] MAINTAIN_IE_GARAGE_BLIP_NAME - Updated organization name (", strBlipName ,") on warehouse blip (simple interior id: ", eSimpleInteriorID, ")")
				#ENDIF
				EXIT
			ENDIF
		
		ENDIF
		
	ELSE
		IF iBlipType != 2
			bShouldUpdate = TRUE
		ENDIF
		
		IF iBlipType = 4
			SET_BLIP_ROUTE(blipIndex, FALSE)
			SET_BLIP_SCALE(blipIndex, BLIP_SIZE_NETWORK_COORD)
			SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_MED)
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_INTERIOR][IE_GARAGE] MAINTAIN_IE_GARAGE_BLIP_NAME - Resetting scale and route (2)")
			#ENDIF
		ENDIF
		
		IF bShouldUpdate
			IF DOES_PLAYER_OWN_IE_GARAGE(PLAYER_ID(), eIEWarehouseID)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "IE_WHOUSE_BLIP0") // Owned Vehicle Warehouse
				iBlipType = 2
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_WAREHOUSE] MAINTAIN_IE_GARAGE_BLIP_NAME - Updated name to owned warehouse (simple interior id: ", eSimpleInteriorID, ")")
				#ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF iBlipType = 0 // This happens if no other names were selected
		bShouldUpdate = TRUE
	ENDIF
	
	IF bShouldUpdate
		SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "IE_WHOUSE_BLIP1") // Vehicle Warehouse
		iBlipType = 3
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_WAREHOUSE] MAINTAIN_IE_GARAGE_BLIP_NAME - Using generic IE warehouse blip name, no other was selected (simple interior id: ", eSimpleInteriorID, ")")
		#ENDIF
	ENDIF
ENDPROC

FUNC INT GET_IE_GARAGE_BLIP_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(iBlipIndex)
	
	IF _CAN_PLAYER_TAKE_THIS_CAR_TO_THEIR_IE_GARAGE_OFF_MISSION(eSimpleInteriorID)
		RETURN BLIP_COLOUR_YELLOW
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_GANG_TYPE(GT_VIP)
		RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
	ELIF GB_IS_PLAYER_MEMBER_OF_GANG_TYPE(PLAYER_ID(), FALSE, GT_VIP)
		PLAYER_INDEX GangLeaderID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))
	ENDIF
	
	RETURN BLIP_COLOUR_WHITE
ENDFUNC

FUNC VECTOR GET_IE_GARAGE_MAP_MIDPOINT(SIMPLE_INTERIORS eSimpleInteriorID)
	RETURN GET_IE_GARAGE_BLIP_COORDS(eSimpleInteriorID)
ENDFUNC

PROC IE_SELL_MISSION_CLEAR_END_SCREEN_SUPPRESS_FADE_IN(SIMPLE_INTERIOR_DETAILS &details)
	IF IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_SET_END_SCREEN_SUPPRESS_FADE_IN)
		PRINTLN("[IE_MISSION_PCM] - IE_SELL_MISSION_CLEAR_END_SCREEN_SUPPRESS_FADE_IN - setting g_bEndScreenSuppressFadeIn = FALSE")
		CLEAR_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_SET_END_SCREEN_SUPPRESS_FADE_IN)
		g_bEndScreenSuppressFadeIn = FALSE
	ENDIF
ENDPROC

PROC MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION(SIMPLE_INTERIOR_DETAILS &details)
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		IF IS_SCREEN_FADED_OUT()
			
			IF !g_bEndScreenSuppressFadeIn
			AND IS_IE_EXTERIOR_WAIT_FOR_MISSION_FADE(PLAYER_ID())
				//For: url:bugstar:3626161 - Sell - Specialist Dealer - Player is unable to drive their car or exit their car as soon as the specialist dealer mission launches
				PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - setting g_bEndScreenSuppressFadeIn = TRUE")
				SET_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_SET_END_SCREEN_SUPPRESS_FADE_IN)
				g_bEndScreenSuppressFadeIn = TRUE
			ENDIF
			
			IF IS_IE_SELL_MISSION_CREATED_VEHICLES(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			AND IS_MISSION_PERSONAL_CAR_MOD_STARTED(PLAYER_ID())
				IF NOT IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_SCREEN_FADED_OUT_FOR_MISSION_VEHICLE_CREATION)
					IF NOT IS_PLAYER_TELEPORT_ACTIVE()
						DO_SCREEN_FADE_IN(500)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						SET_PLAYER_MISSION_PERSONAL_CAR_MOD_START(FALSE)
						SET_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_SCREEN_FADED_OUT_FOR_MISSION_VEHICLE_CREATION)
						#IF IS_DEBUG_BUILD
						PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - BS_SIMPLE_INTERIOR_IE_SCREEN_FADED_OUT_FOR_MISSION_VEHICLE_CREATION - TRUE DO_SCREEN_FADE_IN")
						#ENDIF
						
						IE_SELL_MISSION_CLEAR_END_SCREEN_SUPPRESS_FADE_IN(details)
					ELSE
						#IF IS_DEBUG_BUILD
						PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - IS_PLAYER_TELEPORT_ACTIVE true")
						#ENDIF
					ENDIF	
				ENDIF
			ELSE	
				IF IS_IE_EXTERIOR_WAIT_FOR_MISSION_FADE(PLAYER_ID())
					IF NOT HAS_NET_TIMER_STARTED(sIEMissionFadeSafeTimer)
						START_NET_TIMER(sIEMissionFadeSafeTimer)
						#IF IS_DEBUG_BUILD
						PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - Start sIEMissionFadeSafeTimer")
						#ENDIF
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sIEMissionFadeSafeTimer, g_sMPTunables.iIMPEXP_SELL_POST_MOD_TRANS_TIMEOUT)
							IF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
								TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
								SET_PLAYER_MISSION_PERSONAL_CAR_MOD_START(FALSE)
								SET_IE_SELL_MISSION_PLAYERS_FADED_IN(PLAYER_ID(),TRUE)
								SET_PLAYER_MISSION_PERSONAL_CAR_MOD_IS_READY_TO_START_MISSION(PLAYER_ID(), FALSE)
								SET_IE_EXTERIOR_WAIT_FOR_MISSION_FADE(FALSE)
								RESET_NET_TIMER(sIEMissionFadeSafeTimer)
								#IF IS_DEBUG_BUILD
								PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - PLAYER IS INSIDE WAREHOUSE AND STUCK IN BLACK SCREEN ")
								#ENDIF
								IE_SELL_MISSION_CLEAR_END_SCREEN_SUPPRESS_FADE_IN(details)
							ELSE
								DO_SCREEN_FADE_IN(500)
								SET_IE_EXTERIOR_WAIT_FOR_MISSION_FADE(FALSE)
								SET_PLAYER_MISSION_PERSONAL_CAR_MOD_START(FALSE)
								SET_IE_SELL_MISSION_PLAYERS_FADED_IN(PLAYER_ID(),TRUE)
								SET_PLAYER_MISSION_PERSONAL_CAR_MOD_IS_READY_TO_START_MISSION(PLAYER_ID(), FALSE)
								NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
								RESET_NET_TIMER(sIEMissionFadeSafeTimer)
								#IF IS_DEBUG_BUILD
								PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - PLAYER IS NOT INSIDE WAREHOUSE AND STUCK IN BLACK SCREEN ")
								#ENDIF
								IE_SELL_MISSION_CLEAR_END_SCREEN_SUPPRESS_FADE_IN(details)
							ENDIF
						ENDIF	
					ENDIF	
				ELSE
					#IF IS_DEBUG_BUILD
					PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - IS_IE_EXTERIOR_WAIT_FOR_MISSION_FADE(PLAYER_ID()) is FALSE ")
					#ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - Flow vehicle creatation is: ", IS_IE_SELL_MISSION_CREATED_VEHICLES(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
				PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - Is carmod script ready to start mission(this should be false for private/collection and true for gangster/showroom : ", IS_MISSION_PERSONAL_CAR_MOD_STARTED(PLAYER_ID()))
				#ENDIF
			ENDIF
			
			IF PLAYER_ID() = GB_GET_LOCAL_PLAYER_GANG_BOSS()
				IF g_iFailedToLaunchIEVehicleSellMission > -1
					SET_OFFICE_PERSONAL_CAR_MOD_IE_MISSION_FAILED(TRUE)
					SET_PLAYER_MISSION_PERSONAL_CAR_MOD_START(FALSE)
					SET_PLAYER_MISSION_PERSONAL_CAR_MOD_IS_READY_TO_START_MISSION(PLAYER_ID(), FALSE)
					g_iFailedToLaunchIEVehicleSellMission = -1
					NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					DO_SCREEN_FADE_IN(500)
					#IF IS_DEBUG_BUILD
					PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - Failed to launch IE Sell mission")
					#ENDIF
					
					IE_SELL_MISSION_CLEAR_END_SCREEN_SUPPRESS_FADE_IN(details)
				ENDIF
			ELSE
				IF IS_OFFICE_PERSONAL_CAR_MOD_IE_MISSION_FAILED(GB_GET_LOCAL_PLAYER_GANG_BOSS())
					SET_PLAYER_MISSION_PERSONAL_CAR_MOD_START(FALSE)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					DO_SCREEN_FADE_IN(500)
					#IF IS_DEBUG_BUILD
					PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - My Boss Failed to launch IE Sell mission")
					#ENDIF
					
					IE_SELL_MISSION_CLEAR_END_SCREEN_SUPPRESS_FADE_IN(details)					
				ENDIF
			ENDIF
		ENDIF	
		
		IF IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_SCREEN_FADED_OUT_FOR_MISSION_VEHICLE_CREATION)
		AND IS_SCREEN_FADED_IN()
		AND NOT IS_MISSION_PERSONAL_CAR_MOD_STARTED(PLAYER_ID())
			IF PLAYER_ID() = GB_GET_LOCAL_PLAYER_GANG_BOSS()
				SET_IE_EXTERIOR_WAIT_FOR_MISSION_FADE(FALSE)
				SET_IE_SELL_MISSION_PLAYERS_FADED_IN(PLAYER_ID(),TRUE)
				SET_PLAYER_MISSION_PERSONAL_CAR_MOD_IS_READY_TO_START_MISSION(PLAYER_ID(), FALSE)
				CLEAR_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_SCREEN_FADED_OUT_FOR_MISSION_VEHICLE_CREATION)
				#IF IS_DEBUG_BUILD
				PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - local player clear BS_SIMPLE_INTERIOR_IE_SCREEN_FADED_OUT_FOR_MISSION_VEHICLE_CREATION ")
				#ENDIF
				
				IE_SELL_MISSION_CLEAR_END_SCREEN_SUPPRESS_FADE_IN(details)
			ELSE
				SET_IE_EXTERIOR_WAIT_FOR_MISSION_FADE(FALSE)
				CLEAR_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_SCREEN_FADED_OUT_FOR_MISSION_VEHICLE_CREATION)
				#IF IS_DEBUG_BUILD
				PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - remote player clear BS_SIMPLE_INTERIOR_IE_SCREEN_FADED_OUT_FOR_MISSION_VEHICLE_CREATION ")
				#ENDIF
				
				IE_SELL_MISSION_CLEAR_END_SCREEN_SUPPRESS_FADE_IN(details)
			ENDIF
		ENDIF
	ELSE
		IF IS_SCREEN_FADED_OUT()
			IF IS_IE_EXTERIOR_WAIT_FOR_MISSION_FADE(PLAYER_ID())
				IF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
					TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
					SET_PLAYER_MISSION_PERSONAL_CAR_MOD_START(FALSE)
					SET_IE_SELL_MISSION_PLAYERS_FADED_IN(PLAYER_ID(),TRUE)
					SET_PLAYER_MISSION_PERSONAL_CAR_MOD_IS_READY_TO_START_MISSION(PLAYER_ID(), FALSE)
					SET_IE_EXTERIOR_WAIT_FOR_MISSION_FADE(FALSE)
					RESET_NET_TIMER(sIEMissionFadeSafeTimer)
					#IF IS_DEBUG_BUILD
					PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - GB_IS_PLAYER_MEMBER_OF_A_GANG = FALSE - PLAYER IS INSIDE WAREHOUSE AND STUCK IN BLACK SCREEN ")
					#ENDIF
					
					IE_SELL_MISSION_CLEAR_END_SCREEN_SUPPRESS_FADE_IN(details)
				ELSE
					DO_SCREEN_FADE_IN(500)
					SET_IE_EXTERIOR_WAIT_FOR_MISSION_FADE(FALSE)
					SET_PLAYER_MISSION_PERSONAL_CAR_MOD_START(FALSE)
					SET_IE_SELL_MISSION_PLAYERS_FADED_IN(PLAYER_ID(),TRUE)
					SET_PLAYER_MISSION_PERSONAL_CAR_MOD_IS_READY_TO_START_MISSION(PLAYER_ID(), FALSE)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					RESET_NET_TIMER(sIEMissionFadeSafeTimer)
					#IF IS_DEBUG_BUILD
					PRINTLN("[IE_MISSION_PCM] - MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION - GB_IS_PLAYER_MEMBER_OF_A_GANG = FALSE - PLAYER IS NOT INSIDE WAREHOUSE AND STUCK IN BLACK SCREEN ")
					#ENDIF
					
					IE_SELL_MISSION_CLEAR_END_SCREEN_SUPPRESS_FADE_IN(details)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_IE_GARAGE_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_EXT_SCRIPT_STATE eState, BOOL &bExtScriptMustRun)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(eState)
	
	bExtScriptMustRun = IS_IE_EXTERIOR_WAIT_FOR_MISSION_FADE(PLAYER_ID())
	MANAGE_SCREEN_FADE_FOR_IE_SELL_MISSION(details)
ENDPROC

PROC RESET_IE_GARAGE_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(details)
ENDPROC

PROC MAINTAIN_IE_GARAGE_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF NOT IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_GARAGE_WANTED_LEVEL_CLEARED)
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][IE_GARAGE] MAINTAIN_IE_GARAGE_INSIDE_GAMEPLAY_MODIFIERS - Cleared wanted level.")
			#ENDIF
			
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		ENDIF
		
		SET_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_GARAGE_WANTED_LEVEL_CLEARED)
	ENDIF
ENDPROC

PROC RESET_IE_GARAGE_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	// Reset all bits.
	IF IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_GARAGE_WANTED_LEVEL_CLEARED)
		CLEAR_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_IE_GARAGE_WANTED_LEVEL_CLEARED)
	ENDIF
ENDPROC

FUNC BOOL DOES_IE_GARAGE_BLOCK_WEAPONS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_IE_GARAGE_RESTRICT_PLAYER_MOVEMENT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

PROC GET_IE_GARAGE_CAR_GENS_BLOCK_AREA(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoBlockCarGen, VECTOR &vMin, VECTOR &vMax)
	
	IF eSimpleInteriorID = SIMPLE_INTERIOR_IE_WAREHOUSE_1
		vMin = <<-639.723877, -1789.570557, 23.090210>>
		vMax = <<-628.311462, -1794.311035, 24.761658>> 
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_IE_WAREHOUSE_3
		vMin = <<-68.486595, -1837.982910, 25.861521>>
		vMax = <<-55.578743, -1822.911011, 33.349705>> 
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_IE_WAREHOUSE_4
		vMin = <<40.702522, -1295.222168, 28.269077>>
		vMax = <<48.478874, -1277.385132, 30.061066>> 
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_IE_WAREHOUSE_6
		vMin = <<804.370483, -2241.252197, 28.615847>>
		vMax = <<785.414551, -2239.441406, 35.855999>> 
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_IE_WAREHOUSE_8
		vMin = <<148.020645, -2982.688477, 5.096388>>
		vMax = <<141.893387, -2982.797363, 6.537447>> 
		bDoBlockCarGen = TRUE
		EXIT
	ENDIF
	
	bDoBlockCarGen = FALSE
ENDPROC

FUNC BOOL GET_IE_GARAGE_OUTSIDE_MODEL_HIDE(SIMPLE_INTERIORS eSimpleInteriorID, INT iIndex, VECTOR &vPos, FLOAT &fRadius, FLOAT &fActivationRadius, MODEL_NAMES &eModel, BOOL &bSurviveMapReload)
	fActivationRadius = 200
	bSurviveMapReload = FALSE
	
	SWITCH eSimpleInteriorID
		/* Disabled for B*4051170
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7
			SWITCH iIndex
				CASE 0
					vPos = <<1755.9934, -1654.6173, 111.6811>>
					fRadius = 4.0
					eModel = PROP_STORAGETANK_02B
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		*/
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10
			fActivationRadius = 300
			SWITCH iIndex
				CASE 0
					vPos = <<-1099.531, -2020.803, 12.174>>
					fRadius = 4.0
					eModel = PROP_FACGATE_01
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC GET_IE_GARAGE_SCENARIO_PED_REMOVAL_SPHERE(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoRemoveScenarioPeds, VECTOR &vCoords, FLOAT &fRadius)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(vCoords)
	UNUSED_PARAMETER(fRadius)
	bDoRemoveScenarioPeds = FALSE
ENDPROC

FUNC INT GET_IE_GARAGE_ENTRANCES_COUNT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN 2
ENDFUNC

FUNC INT GET_IE_GARAGE_INVITE_ENTRY_POINT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN -1
ENDFUNC

PROC GET_IE_GARAGE_ENTRANCE_DATA(SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID, SIMPLE_INTERIOR_ENTRANCE_DATA &data)
	SWITCH iEntranceID
		CASE 0
			GET_IE_GARAGE_ENTRY_LOCATE(eSimpleInteriorID, data.vPosition)
			
			data.iType = 0
			data.vLocatePos1 = <<1, 1, 2>>
			
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_DRAWS_LOCATE)
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_ON_FOOT_ONLY)
			
			GET_IE_GARAGE_ENTRY_LOCATE_COLOUR(eSimpleInteriorID, data.iLocateColourR, data.iLocateColourG, data.iLocateColourB, data.iLocateColourA)
		BREAK
		CASE 1
			GET_IE_WAREHOUSE_VEHICLE_ENTRY_LOCATE(eSimpleInteriorID, data.vPosition, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth, data.fEnterHeading)
		
			data.iType = 1
			
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_USES_ANGLED_AREA)
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IN_CAR_ONLY)
		BREAK
	ENDSWITCH
ENDPROC

FUNC FLOAT GET_INTERP_POINT_FLOAT_FOR_IE_GARAGE(FLOAT fStartPos, FLOAT fEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN ((((fEndPos - fStartPos) / (fEndTime - fStartTime)) * (fPointTime - fStartTime)) + fStartPos)
ENDFUNC

FUNC VECTOR GET_INTERP_POINT_VECTOR_FOR_IE_GARAGE(VECTOR vStartPos, VECTOR vEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN <<GET_INTERP_POINT_FLOAT_FOR_IE_GARAGE(vStartPos.X, vEndPos.X, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT_FOR_IE_GARAGE(vStartPos.Y, vEndPos.Y, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT_FOR_IE_GARAGE(vStartPos.Z, vEndPos.Z, fStartTime, fEndTime, fPointTime)>>
ENDFUNC

FUNC BOOL IS_PLAYER_DRIVING_INTO_IE_GARAGE(SIMPLE_INTERIOR_ENTRANCE_DATA &data, VEHICLE_INDEX theVehicle, MODEL_NAMES theModel)
	VECTOR vFrontTopLeft, vFrontTopRight, vBackTopLeft, vBackTopRight
	DETERMINE_4_TOP_VECTORS_OF_MODEL_BOX_IN_WORLD_COORDS(theVehicle, theModel, vFrontTopLeft, vFrontTopRight, vBackTopLeft, vBackTopRight)
	
	VECTOR vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight
	DETERMINE_4_BOTTOM_VECTORS_OF_MODEL_BOX_IN_WORLD_COORDS(theVehicle, theModel, vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight)
	
	VECTOR vFrontMiddleLeft, vFrontMiddleRight, vBackMiddleLeft, vBackMiddleRight
	
	vFrontMiddleLeft = GET_INTERP_POINT_VECTOR_FOR_IE_GARAGE(vFrontBottomLeft, vFrontTopLeft, 0.0, 1.0, 0.5)
	vFrontMiddleRight = GET_INTERP_POINT_VECTOR_FOR_IE_GARAGE(vFrontBottomRight, vFrontTopRight, 0.0, 1.0, 0.5)
	vBackMiddleLeft = GET_INTERP_POINT_VECTOR_FOR_IE_GARAGE(vBackBottomLeft, vBackTopLeft, 0.0, 1.0, 0.5)
	vBackMiddleRight = GET_INTERP_POINT_VECTOR_FOR_IE_GARAGE(vBackBottomRight, vBackTopRight, 0.0, 1.0, 0.5)
	
	IF IS_POINT_IN_ANGLED_AREA(vFrontMiddleLeft, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth)
	OR IS_POINT_IN_ANGLED_AREA(vFrontMiddleRight, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth)
		IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(theVehicle), data.fEnterHeading, 75.0)
			RETURN TRUE
		ENDIF
	ELIF IS_POINT_IN_ANGLED_AREA(vBackMiddleLeft, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth)
	OR IS_POINT_IN_ANGLED_AREA(vBackMiddleRight, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth)
		IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(theVehicle), data.fEnterHeading - 180.0, 75.0)
//			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_USE_IE_GARAGE_ENTRY_IN_CURRENT_VEHICLE(SIMPLE_INTERIOR_ENTRANCE_DATA &data, SIMPLE_INTERIOR_DETAILS &details, BOOL &bReturnSetExitCoords)
	
	bReturnSetExitCoords = FALSE
	UNUSED_PARAMETER(details)
	
	IF IS_THIS_SIMPLE_INTERIOR_ENTRANCE_IN_CAR_ONLY(data)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IMPORT_EXPORT_GARAGES eWarehouse = GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(data.eSimpleInteriorID)
			
			IF NOT DOES_ENTITY_EXIST(vehIndex)
			OR NOT IS_VEHICLE_DRIVEABLE(vehIndex)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_IE_GARAGE_ENTRY_IN_CURRENT_VEHICLE - Cannot access interior as vehicle is not driveable, returning FALSE.")
				#ENDIF
				RETURN FALSE
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_SVM_VEHICLE(vehIndex)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_IE_GARAGE_ENTRY_IN_CURRENT_VEHICLE - Cannot access interior in this type of vehicle, returning FALSE.")
				#ENDIF
				RETURN FALSE
			ENDIF

			IF IS_SVM_VEHICLE(vehIndex)
			AND IS_PLAYER_IN_PERSONAL_VEHICLE(PLAYER_ID())
			AND GET_PED_IN_VEHICLE_SEAT(vehIndex) = GET_PLAYER_PED(GET_IE_GARAGE_OWNER(eWarehouse))
			AND IS_PED_IN_THEIR_PERSONAL_VEHICLE(GET_IE_GARAGE_OWNER(eWarehouse))
				IF IS_PLAYER_DRIVING_INTO_IE_GARAGE(data, vehIndex, GET_ENTITY_MODEL(vehIndex))
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_IE_GARAGE_ENTRY_IN_CURRENT_VEHICLE - Player is in a Special Vehicle. Returning TRUE.")
					#ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] CAN_PLAYER_USE_IE_GARAGE_ENTRY_IN_CURRENT_VEHICLE - Player doesn't own a Special Vehicle garage. Returning FALSE.")
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_SVM_VEHICLE_AS_LAST_USED(VEHICLE_INDEX vTemp)
	INT iSaveSlot
	INT iDisplaySlot
	INT iVehicleIndex
	
	IF g_iPreviousLastUsedSlot = -1
		g_iPreviousLastUsedSlot = CURRENT_SAVED_VEHICLE_SLOT()
	ENDIF
	
	iVehicleIndex = GET_SVM_VEHICLE_INDEX(GET_ENTITY_MODEL(vTemp))
	
	iDisplaySlot = iVehicleIndex + 148
	
	MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot, iSaveSlot, FALSE)
	
	SET_LAST_USED_VEHICLE_SLOT(iSaveSlot)
	
	SET_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS, MP_SAVED_VEHICLE_EXITING_SIMPLE_INTERIOR)
ENDPROC

FUNC BOOL SHOULD_TRIGGER_EXIT_IN_CAR_FOR_IE_GARAGE(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX interiorOwner, BOOL bOwnerDrivingOut)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF interiorOwner = PLAYER_ID()
			IF GET_IS_VEHICLE_ENGINE_RUNNING(vTemp)
			AND g_bPlayerLeavingCurrentInteriorInVeh = TRUE
				IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 2.0
				OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vTemp, TRUE) 
					SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vTemp, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					
					IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
						SET_SVM_VEHICLE_AS_LAST_USED(vTemp)
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF GET_PED_IN_VEHICLE_SEAT(vTemp, VS_DRIVER) = GET_PLAYER_PED(interiorOwner)
			AND bOwnerDrivingOut
				#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_INTERIOR] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_IE_GARAGE TRUE ")
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GET_IE_GARAGE_ENTRANCE_SCENE_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS& details, INT iEntranceID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iEntranceID)
	UNUSED_PARAMETER(details)
ENDPROC

PROC MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_IE_GARAGE_ACCESS(SIMPLE_INTERIOR_DETAILS &details)
	UNUSED_PARAMETER(details)
ENDPROC		

FUNC SIMPLE_INTERIORS GET_OWNED_SMPL_INT_OF_TYPE_IE_GARAGE(PLAYER_INDEX playerID, INT iPropertySlot, INT iFactoryType)
	UNUSED_PARAMETER(iFactoryType)
	UNUSED_PARAMETER(iPropertySlot)
	RETURN GET_OWNED_IE_GARAGE_SIMPLE_INTERIOR(playerID)
ENDFUNC

PROC IE_GARAGE_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX veh, BOOL bSetOnGround)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF bSetOnGround
	AND NOT IS_ENTITY_DEAD(veh)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(veh)
		SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		PRINTLN("[personal_vehicle] IE_GARAGE_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT - Setting vehicle on ground.")
		FREEZE_ENTITY_POSITION(veh, FALSE)
	ENDIF
	
	IF IS_SVM_VEHICLE(veh)
	AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		SET_SVM_VEHICLE_AS_LAST_USED(veh)
		PRINTLN("[personal_vehicle] IE_GARAGE_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT, MPGlobals.VehicleData.bAssignToMainScript = TRUE")
		SET_PV_ASSIGN_TO_MAIN_SCRIPT(TRUE)
	ENDIF
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Below is the function that builds the interface.
// Because look up table needs to be build on every call as an optimisation
// the function retrieves a pointer to one function at a time so that one call = one lookup
// Functions are grouped thematically.

PROC BUILD_IE_GARAGE_LOOK_UP_TABLE(SIMPLE_INTERIOR_INTERFACE &interface, SIMPLE_INTERIOR_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		//════════════════════════════════════════════════════════════════════
		CASE E_DOES_SIMPLE_INTERIOR_USE_EXTERIOR_SCRIPT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_IE_GARAGE_USE_EXTERIOR_SCRIPT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION
			interface.getSimpleInteriorInteriorTypeAndPosition = &GET_IE_GARAGE_INTERIOR_TYPE_AND_POSITION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS
			interface.getSimpleInteriorDetails = &GET_IE_GARAGE_DETAILS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS_BS
			interface.getSimpleInteriorDetailsPropertiesBS = &GET_IE_GARAGE_PROPERTIES_BS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_NAME
			interface.simpleInteriorFP_ReturnS_ParamID = &GET_IE_GARAGE_NAME
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX
			interface.getSimpleInteriorInsideBoundingBox = &GET_IE_GARAGE_INSIDE_BOUNDING_BOX
		BREAK
		CASE E_GET_OWNED_SIMPLE_INTERIOR_OF_TYPE
			interface.getOwnedSimpleInteriorOfType = &GET_OWNED_SMPL_INT_OF_TYPE_IE_GARAGE
		BREAK
		CASE E_SIMPLE_INTERIOR_LOAD_SECONDARY_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamID = &SHOULD_IE_GARAGE_LOAD_SECONDARY_INTERIOR
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_GAME_TEXT
			interface.simpleInteriorFP_ReturnS_ParamID_gameTextID = &GET_IE_GARAGE_GAME_TEXT
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_SHOW_PIM_INVITE_OPTION
			interface.simpleInteriorFP_ReturnB_ParamID_tl63_AndBoolx2 = &SHOULD_IE_GARAGE_SHOW_PIM_INVITE_OPTION
		BREAK
		//════════════════════════════════════════════════════════════════════
		
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCES_COUNT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_IE_GARAGE_ENTRANCES_COUNT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_DATA
			interface.getSimpleInteriorEntranceData = &GET_IE_GARAGE_ENTRANCE_DATA
		BREAK			
		CASE E_GET_SIMPLE_INTERIOR_INVITE_ENTRY_POINT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_IE_GARAGE_INVITE_ENTRY_POINT
		BREAK	
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_SCENE_DETAILS
			interface.getSimpleInteriorEntranceSceneDetails = &GET_IE_GARAGE_ENTRANCE_SCENE_DETAILS
		BREAK
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_CORONA_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_IE_GARAGE_CORONA_BE_HIDDEN
		BREAK
		
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_LOCATE_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_IE_GARAGE_LOCATE_BE_HIDDEN
		BREAK
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR_IN_VEHICLE
			interface.simpleInteriorFP_ReturnB_ParamPIDAndModel = &CAN_PLAYER_ENTER_IE_GARAGE_IN_VEHICLE
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPID_ID_AndInt = &CAN_PLAYER_ENTER_IE_GARAGE
		BREAK
		CASE E_CAN_PLAYER_USE_SIMPLE_INTERIOR_ENTRY_IN_CURRENT_VEHICLE
			interface.canPlayerUseEntryInCurrentVeh = &CAN_PLAYER_USE_IE_GARAGE_ENTRY_IN_CURRENT_VEHICLE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_ENTRY
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_IE_GARAGE_REASON_FOR_BLOCKED_ENTRY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_EXIT
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_IE_GARAGE_REASON_FOR_BLOCKED_EXIT
		BREAK
		CASE E_CAN_PLAYER_INVITE_OTHERS_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_INVITE_OTHERS_TO_IE_GARAGE
		BREAK
		CASE E_CAN_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_BE_INVITED_TO_IE_GARAGE
		BREAK
		CASE E_CAN_LOCAL_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR_BY_PLAYER
			interface.intCanLocalPlayerBeInvitedToSimpleInteriorByPlayer = &CAN_LOCAL_PLAYER_BE_INVITED_TO_IE_GARAGE_BY_PLAYER
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_MENU_TITLE_AND_OPTIONS
			interface.simpleInteriorFPProc_ParamIDAndCustomMenu = &GET_IE_GARAGE_ENTRANCE_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_PROCESS_SIMPLE_INTERIOR_ENTRANCE_MENU
			interface.simpleInteriorFPProc_ParamIDAndEMenu = &PROCESS_IE_GARAGE_ENTRANCE_MENU
		BREAK
		CASE E_SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_IE_GARAGE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_KICK_OUT_REASON
			interface.getSimpleInteriorKickOutReason = &GET_IE_GARAGE_KICK_OUT_REASON
		BREAK
		CASE E_SET_SIMPLE_INTERIOR_ACCESS_BS
			interface.setSimpleInteriorAccessBS = &SET_IE_GARAGE_ACCESS_BS
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_SPAWN_POINT
			interface.getSimpleInteriorSpawnPoint = &GET_IE_GARAGE_SPAWN_POINT
		BREAK
		CASE E_GET_SI_CUSTOM_INTERIOR_WARP_PARAMS
			interface.getSimpleInteriorCustomInteriorWarpParams = &GET_IE_GARAGE_CUSTOM_INTERIOR_WARP_PARAMS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT
			interface.getSimpleInteriorOutsideSpawnPoint = &GET_IE_GARAGE_OUTSIDE_SPAWN_POINT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_IE_GARAGE_ASYNC_SPAWN_GRID_LOCATION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_INSIDE_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_IE_GARAGE_ASYNC_SPAWN_GRID_INSIDE_LOCATION
		BREAK
		CASE E_MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ACCESS
			interface.maintainLeaveentryVehIfNoAccess = &MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_IE_GARAGE_ACCESS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_TELE_IN_SVM_COORD
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_IE_GARAGE_TELEPORT_IN_SVM_COORD
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_EXIT_MENU_TITLE_AND_OPTIONS
			interface.getSimpleInteriorExitMenuTitleAndOptions = &GET_IE_GARAGE_EXIT_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_IE_GARAGE
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_MAX_BLIPS
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_IE_GARAGE_MAX_BLIPS
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_BE_BLIPPED_THIS_FRAME
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_IE_GARAGE_BE_BLIPPED_THIS_FRAME
		BREAK
		CASE E_IS_SIMPLE_INTERIOR_BLIP_SHORT_RANGE
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &IS_IE_GARAGE_BLIP_SHORT_RANGE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_SPRITE
			interface.simpleInteriorFP_ReturnEBS_ParamIDAndInt = &GET_IE_GARAGE_BLIP_SPRITE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COORDS
			interface.simpleInteriorFP_ReturnV_ParamIDAndInt = &GET_IE_GARAGE_BLIP_COORDS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_BLIP_EXTRA_FUNCTIONALITY
			interface.maintainSimpleInteriorBlipExtraFunctionality = &MAINTAIN_IE_GARAGE_BLIP_EXTRA_FUNCTIONALITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_PRIORITY
			interface.getSimpleInteriorBlipPriority	= &GET_IE_GARAGE_BLIP_PRIORITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COLOUR
			interface.simpleInteriorFP_ReturnI_ParamIDAndInt = &GET_IE_GARAGE_BLIP_COLOUR
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_MAP_MIDPOINT
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_IE_GARAGE_MAP_MIDPOINT
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_MAINTAIN_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.maintainSimpleInteriorOutsideGameplayModifiers = &MAINTAIN_IE_GARAGE_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_IE_GARAGE_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &MAINTAIN_IE_GARAGE_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_IE_GARAGE_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_DOES_SIMPLE_INTERIOR_BLOCK_WEAPONS
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_IE_GARAGE_BLOCK_WEAPONS
		BREAK
		CASE E_DOES_SIMPLE_INTERIOR_RESTRICT_PLAYER_MOVEMENT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_IE_GARAGE_RESTRICT_PLAYER_MOVEMENT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_CAR_GENS_BLOCK_AREA
			interface.getSimpleInteriorCarGensBlockArea = &GET_IE_GARAGE_CAR_GENS_BLOCK_AREA
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_MODEL_HIDE
			interface.getSimpleInteriorOutsideModelHide = &GET_IE_GARAGE_OUTSIDE_MODEL_HIDE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_SCENARIO_PED_REMOVAL_SPHERE
			interface.getSimpleInteriorScenarioPedRemovalSphere = &GET_IE_GARAGE_SCENARIO_PED_REMOVAL_SPHERE
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_TRIGGER_EXIT_IN_CAR
			interface.simpleInteriorFP_ReturnB_ParamID_PID_AndB = &SHOULD_TRIGGER_EXIT_IN_CAR_FOR_IE_GARAGE
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_SML_INT_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
			interface.DoCustomActionForStateWarpToSpawnPoint = &IE_GARAGE_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
		BREAK
	ENDSWITCH
ENDPROC
