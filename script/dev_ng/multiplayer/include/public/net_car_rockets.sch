

USING "globals.sch"
USING "script_network.sch"
USING "script_player.sch"
USING "commands_object.sch"
USING "net_scoring_common.sch"
USING "FMMC_Camera.sch"

ENUM SPECIAL_VEHICLE_RACE_TYPE
	VEHICLE_RACE_TYPE_UNCHECKED,
	VEHICLE_RACE_TYPE_NONE,
	VEHICLE_RACE_TYPE_RUINER,
	VEHICLE_RACE_TYPE_BLAZER
ENDENUM

ENUM SPECIAL_VEHICLE_PICKUP
	VEHICLE_PICKUP_INIT,
	VEHICLE_PICKUP_NONE,
	VEHICLE_PICKUP_ROCKET_HOMING,
	VEHICLE_PICKUP_ROCKET_STRAIGHT,
	VEHICLE_PICKUP_GUN,
	VEHICLE_PICKUP_BOOST,
	VEHICLE_PICKUP_REPAIR,
	VEHICLE_PICKUP_JUMP,
	VEHICLE_PICKUP_PARACHUTE
ENDENUM

FUNC PICKUP_INDEX CREATE_VEHICLE_PICKUP(VECTOR vCoords, MODEL_NAMES customModel, INT PlacementFlags=0, FLOAT fHeading = 0.0, INT amount = 3)
	REQUEST_MODEL(customModel)
	IF NOT HAS_MODEL_LOADED(customModel)
//		REQUEST_MODEL(custoModel)
		//INT modelInt = ENUM_TO_INT(customModel)
		SCRIPT_ASSERT("CREATE_VEHICLE_PICKUP - make sure the model is loaded first")
	ENDIF
	RETURN(CREATE_PICKUP_ROTATE ( PICKUP_VEHICLE_CUSTOM_SCRIPT_LOW_GLOW, vCoords, <<0.0,0.0,fHeading>>, PlacementFlags, amount, DEFAULT,  DEFAULT, customModel))
ENDFUNC

FUNC PICKUP_INDEX CREATE_VEHICLE_ROCKET_PICKUP(VECTOR vCoords, INT PlacementFlags=0, FLOAT fHeading = 0.0, INT amount = 3)
	RETURN CREATE_VEHICLE_PICKUP(vCoords, VEHICLE_ROCKET_PICKUP_MODEL(), PlacementFlags, fHeading, amount)
ENDFUNC

FUNC BOOL HAS_COLLECTED_VEHICLE_ROCKET()
	RETURN g_VehicleRocketInfo.bIsCollected
ENDFUNC

PROC ROCKETS_COLLECTED(VEHICLE_ROCKET_TYPE rocketType = VEHICLE_ROCKET_NORMAL, INT amount = 1)
	IF g_VehicleRocketInfo.bIsCollected = FALSE
		IF NOT g_b_On_Race
		OR NOT g_VehicleBoostInfo.bCollected
			g_VehicleRocketInfo.bIsCollected = TRUE
			g_VehicleRocketInfo.eRocketType = rocketType
			IF amount < 1
				amount = 1
			ENDIF
			g_VehicleRocketInfo.iBullets = amount
			PRINTLN("[SPC_PICK] ROCKETS_COLLECTED ")
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ROCKETS()
	IF g_VehicleRocketInfo.bIsCollected = TRUE
		IF NOT g_b_On_Race
		OR NOT g_VehicleBoostInfo.bCollected
			g_VehicleRocketInfo.bIsCollected = FALSE
			g_VehicleRocketInfo.eRocketType = VEHICLE_ROCKET_NORMAL
			g_VehicleRocketInfo.iBullets = 0
			PRINTLN("[SPC_PICK] REMOVE_ROCKETS ")
		ENDIF
	ENDIF
ENDPROC

PROC VEHICLE_GUN_COLLECTED(int bullets)
	g_VehicleGunInfo.bCollected = TRUE
	IF bullets < 1
		bullets = g_FMMC_STRUCT.iVehicleWeaponMGBullets
	ENDIF
	g_VehicleGunInfo.iBullets = bullets
	PRINTLN("[SPC_PICK] VEHICLE_GUN_COLLECTED ", bullets)
ENDPROC

FUNC VECTOR GET_VECTOR_INFRONT_OF_CURRENT_ENTITY(VECTOR vStart, ENTITY_INDEX eiEntity, FLOAT fDistance = 300.0)

	    VECTOR vEnd 
        
        //end vector - pointing north 
        vEnd    = <<0.0, 1.0, 0.0>> 
        
        //point it in the same direction as the entity
        ROTATE_VECTOR_FMMC(vEnd, GET_ENTITY_ROTATION(eiEntity)) 
        
        //Make the normilised roted vector 300 times larger 
        vEnd.x *= fDistance 
        vEnd.y *= fDistance 
        vEnd.z *= fDistance 
        
        //add it on to the start vector to get the end vector coordinates 
        vEnd += vStart 
        
        RETURN vEnd 

ENDFUNC

/// PURPOSE:
///    Checks to see if it is specific vehicle type
///    Used to then fire rockets up a little as they hit ground otherwise
/// RETURNS:
///    TRUE - If it is a special case vehicle, false otherwise
FUNC BOOL IS_SPECIAL_VEHICLE(VEHICLE_INDEX CarToCheck)

	IF GET_ENTITY_MODEL(CarToCheck) = BLAZER
	OR GET_ENTITY_MODEL(CarToCheck) = SURFER
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR_FOR_ROCKETS(VECTOR vRot)
	IF b_gAllowZForCarRockets
		RETURN CONVERT_ROTATION_TO_DIRECTION_VECTOR(vRot)
	ELSE
		RETURN CONVERT_ROTATION_TO_DIRECTION_VECTOR_NO_Z(vRot)
	ENDIF
ENDFUNC

FUNC FLOAT GET_ROCKET_X_OFFSET(MODEL_NAMES mnVeh)
	
	IF mnVeh = VETO
	OR mnVeh = VETO2
		RETURN 0.0
	ENDIF
	
	RETURN 0.2
ENDFUNC

PROC PLAYER_FIRE_ROCKET()

	VEHICLE_INDEX CarID
	VECTOR vModelMin
	VECTOR vModelMax
	FLOAT fRocketSpeed

	IF g_VehicleRocketInfo.bIsCollected //Player has spikes and can drop them
		IF IS_NET_PLAYER_OK(PLAYER_ID()) OR (IS_FAKE_MULTIPLAYER_MODE_SET() AND IS_PLAYER_PLAYING(PLAYER_ID()))
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				/*
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WT_V_SPACERKT")
					PRINT_HELP("WT_V_SPACERKT")
				ENDIF
				*/
			
				CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(CarID),vModelMin,vModelMax)
				

				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LS) // Fire rocket
				OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_LS)
				OR (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_VEH_HORN) AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED) OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)))
				OR g_VehicleRocketInfo.bLaunchRocket
					VECTOR shootDirection = (CONVERT_ROTATION_TO_DIRECTION_VECTOR_FOR_ROCKETS(GET_ENTITY_ROTATION(CarID)) * 300.0)
					FLOAT fXOffset = GET_ROCKET_X_OFFSET(GET_ENTITY_MODEL(CarID))
					
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_RS) // Shoot back
					OR (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_LOOK_BEHIND) AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED) OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)))
						//Attach points rear of car
						g_VehicleRocketInfo.vAttachLocation1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CarID,<<(vModelMin.x-fXOffset),(vModelMin.y-0.5),vModelMin.z+0.75>>)
						g_VehicleRocketInfo.vAttachLocation2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CarID,<<(vModelMax.x+fXOffset),(vModelMin.y-0.5),vModelMin.z+0.75>>)

						shootDirection = -shootDirection
					ELSE
						//Attach points front of car
						g_VehicleRocketInfo.vAttachLocation1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CarID,<<(vModelMin.x-fXOffset),(vModelMax.y+0.5),vModelMin.z+0.75>>)
						g_VehicleRocketInfo.vAttachLocation2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CarID,<<(vModelMax.x+fXOffset),(vModelMax.y+0.5),vModelMin.z+0.75>>)
					ENDIF
					g_VehicleRocketInfo.vTargetedGround1 = g_VehicleRocketInfo.vAttachLocation1 + shootDirection
					g_VehicleRocketInfo.vTargetedGround2 = g_VehicleRocketInfo.vAttachLocation2 + shootDirection
					
					IF (GET_ENTITY_SPEED(CarID)*GET_ENTITY_SPEED(CarID)) > 400
						fRocketSpeed = (GET_ENTITY_SPEED(CarID)*GET_ENTITY_SPEED(CarID)) + 100
						PRINTLN("CAR SPEED > 400 - Speed = ",GET_ENTITY_SPEED(CarID))
					ELSE
						PRINTLN("CAR SPEED < 400 - Speed = ",GET_ENTITY_SPEED(CarID))
						fRocketSpeed = 400
					ENDIF
					
					// 1668678
					IF NOT ARE_VECTORS_EQUAL(g_VehicleRocketInfo.vAttachLocation1, g_VehicleRocketInfo.vTargetedGround1) 
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY(g_VehicleRocketInfo.vAttachLocation1,g_VehicleRocketInfo.vTargetedGround1, 250, TRUE, g_wtPickupRocketType, PLAYER_PED_ID(),TRUE,TRUE,fRocketSpeed,CarID)
					ENDIF
					IF NOT ARE_VECTORS_EQUAL(g_VehicleRocketInfo.vAttachLocation2, g_VehicleRocketInfo.vTargetedGround2)
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY(g_VehicleRocketInfo.vAttachLocation2,g_VehicleRocketInfo.vTargetedGround2, 250, TRUE, g_wtPickupRocketType, PLAYER_PED_ID(),TRUE,TRUE,fRocketSpeed,CarID)
					ENDIF
					//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(g_VehicleRocketInfo.vAttachLocation1,g_VehicleRocketInfo.vTargetedGround1, 250, FALSE, WEAPONTYPE_VEHICLE_ROCKET, PLAYER_PED_ID(),TRUE,TRUE,fRocketSpeed)
					//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(g_VehicleRocketInfo.vAttachLocation2,g_VehicleRocketInfo.vTargetedGround2, 250, FALSE, WEAPONTYPE_VEHICLE_ROCKET, PLAYER_PED_ID(),TRUE,TRUE,fRocketSpeed)
					
	
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_VENDETTA (g_FMMC_STRUCT.iAdversaryModeType)
						PLAY_SOUND_FROM_COORD(-1, "Rocket_Detonate" ,g_VehicleRocketInfo.vTargetedGround1, "DLC_IE_VV_Rocket_Player_Sounds",TRUE)
					ENDIF
					
					g_VehicleRocketInfo.bIsCollected = FALSE
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WT_V_SPACERKT")
						CLEAR_THIS_FLOATING_HELP("WT_V_SPACERKT",TRUE)
					ENDIF
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
					
				ENDIF
		
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC UPDATE_VEHICLE_ROCKETS()
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CELLPHONE_CAMERA_IN_USE()
		PLAYER_FIRE_ROCKET()
	ENDIF

ENDPROC
FUNC BOOL HAS_SPECIAL_PICKUP_BEEN_COLLECTED()
	IF g_VehicleSpikeInfo.bIsCollected = TRUE
	OR g_VehicleRocketInfo.bIsCollected = TRUE
	OR g_VehicleBoostInfo.bCollected = TRUE
	OR g_VehicleGunInfo.bCollected = TRUE
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC
PROC VEHICLE_WEAPONS_DISABLE_ALL(SPECIAL_VEHICLE_RACE_TYPE vehicleType, VEHICLE_INDEX playerVeh, PED_INDEX playerPed)
	SWITCH vehicleType
		CASE VEHICLE_RACE_TYPE_RUINER
			DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_VEHICLE_PLAYER_LASER, playerVeh,playerPed) // Probably not necessary
			DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET, playerVeh,playerPed)
			DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET, playerVeh,playerPed)
		BREAK
		CASE VEHICLE_RACE_TYPE_BLAZER
			DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_CANNON_BLAZER, playerVeh,playerPed)
		BREAK
	ENDSWITCH
	SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0, 0)
ENDPROC
PROC VEHICLE_WEAPONS_DISABLE_HOMING_ROCKET(SPECIAL_VEHICLE_RACE_TYPE vehicleType, VEHICLE_INDEX playerVeh, PED_INDEX playerPed)
	SWITCH vehicleType
		CASE VEHICLE_RACE_TYPE_RUINER
			DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET, playerVeh,playerPed)
			SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1, -1)
		BREAK
	ENDSWITCH
ENDPROC
PROC VEHICLE_WEAPONS_DISABLE_STRAIGHT_ROCKET(SPECIAL_VEHICLE_RACE_TYPE vehicleType, VEHICLE_INDEX playerVeh, PED_INDEX playerPed)
	SWITCH vehicleType
		CASE VEHICLE_RACE_TYPE_RUINER
			DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET, playerVeh,playerPed)
			SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1, -1)
			SET_PLAYER_LOCKON(GET_PLAYER_INDEX(), TRUE)
		BREAK
	ENDSWITCH
ENDPROC
PROC VEHICLE_WEAPONS_DISABLE_GUN(SPECIAL_VEHICLE_RACE_TYPE vehicleType, VEHICLE_INDEX playerVeh, PED_INDEX playerPed)
	SWITCH vehicleType
		CASE VEHICLE_RACE_TYPE_RUINER
			DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET, playerVeh,playerPed)
			
			// Fix for url:bugstar:3320072 Does not work for blazer
			SET_CURRENT_PED_VEHICLE_WEAPON(playerPed, WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET)
		BREAK
		CASE VEHICLE_RACE_TYPE_BLAZER
			DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_CANNON_BLAZER, playerVeh,playerPed)
		BREAK
	ENDSWITCH
	SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0, 0)
ENDPROC
PROC VEHICLE_WEAPONS_ENABLE_ROCKET_HOMING(SPECIAL_VEHICLE_RACE_TYPE vehicleType, VEHICLE_INDEX playerVeh, PED_INDEX playerPed)
	SWITCH vehicleType
		CASE VEHICLE_RACE_TYPE_RUINER
			SET_CURRENT_PED_VEHICLE_WEAPON(playerPed, WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET)
			DISABLE_VEHICLE_WEAPON(FALSE,WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET, playerVeh,playerPed)
			SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1, g_VehicleRocketInfo.iBullets)
		BREAK
	ENDSWITCH
ENDPROC
PROC VEHICLE_WEAPONS_ENABLE_ROCKET_STRAIGHT(SPECIAL_VEHICLE_RACE_TYPE vehicleType, VEHICLE_INDEX playerVeh, PED_INDEX playerPed)
	SWITCH vehicleType
		CASE VEHICLE_RACE_TYPE_RUINER
			SET_CURRENT_PED_VEHICLE_WEAPON(playerPed, WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET)
			DISABLE_VEHICLE_WEAPON(FALSE,WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET, playerVeh,playerPed)
			SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1, g_VehicleRocketInfo.iBullets)
		BREAK
	ENDSWITCH
ENDPROC
PROC VEHICLE_WEAPONS_ENABLE_GUN(SPECIAL_VEHICLE_RACE_TYPE vehicleType, VEHICLE_INDEX playerVeh, PED_INDEX playerPed)
	SWITCH vehicleType
		CASE VEHICLE_RACE_TYPE_RUINER
			SET_CURRENT_PED_VEHICLE_WEAPON(playerPed, WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET)
			DISABLE_VEHICLE_WEAPON(FALSE,WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET, playerVeh,playerPed)
		BREAK
		CASE VEHICLE_RACE_TYPE_BLAZER
			SET_CURRENT_PED_VEHICLE_WEAPON(playerPed, WEAPONTYPE_DLC_VEHICLE_CANNON_BLAZER)
			DISABLE_VEHICLE_WEAPON(FALSE,WEAPONTYPE_DLC_VEHICLE_CANNON_BLAZER, playerVeh,playerPed)
		BREAK
	ENDSWITCH
	PRINTLN("[SPECIAL_PICKUPS] MAINTAIN_VEHICLE_SPECIAL_WEAPONS - enable gun ", g_VehicleGunInfo.iBullets)
	SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0, g_VehicleGunInfo.iBullets)
ENDPROC
PROC UPDATE_VEHICLE_WEAPONS(SPECIAL_VEHICLE_PICKUP currentPickup, SPECIAL_VEHICLE_RACE_TYPE vehicleType, SPECIAL_VEHICLE_PICKUP &lastVehiclePickup)
	PED_INDEX playerPed = PLAYER_PED_ID()
	BOOL isInVehicle = IS_PED_IN_ANY_VEHICLE(playerPed)
	VEHICLE_INDEX playerVeh
	IF isInVehicle
		playerVeh = GET_VEHICLE_PED_IS_IN(playerPed)
	ENDIF
	
	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID())
	AND isInVehicle)
		currentPickup = VEHICLE_PICKUP_INIT
	ENDIF
	IF currentPickup != lastVehiclePickup
		PRINTLN("[SPECIAL_PICKUPS] MAINTAIN_VEHICLE_SPECIAL_WEAPONS - currentPickup: ", currentPickup, " lastPickup: ", lastVehiclePickup, " vehicleType: ", vehicleType)
		
		// Leaving weapon state
		SWITCH lastVehiclePickup
			CASE VEHICLE_PICKUP_INIT
				SET_CURRENT_PED_WEAPON(playerPed, WEAPONTYPE_UNARMED)
				IF isInVehicle
				AND DOES_VEHICLE_HAVE_WEAPONS(playerVeh)
					VEHICLE_WEAPONS_DISABLE_ALL(vehicleType, playerVeh, playerPed)
				ELSE
					currentPickup = lastVehiclePickup
				ENDIF
			BREAK
			CASE VEHICLE_PICKUP_ROCKET_HOMING
				VEHICLE_WEAPONS_DISABLE_HOMING_ROCKET(vehicleType, playerVeh, playerPed)
			BREAK
			CASE VEHICLE_PICKUP_ROCKET_STRAIGHT
				VEHICLE_WEAPONS_DISABLE_STRAIGHT_ROCKET(vehicleType, playerVeh, playerPed)
			BREAK
			CASE VEHICLE_PICKUP_GUN
				VEHICLE_WEAPONS_DISABLE_GUN(vehicleType, playerVeh, playerPed)
				g_VehicleGunInfo.iBullets = -1
			BREAK
		ENDSWITCH
		// Entering weapon state
		SWITCH currentPickup
			CASE VEHICLE_PICKUP_INIT
				SET_PED_CAN_SWITCH_WEAPON(playerPed, TRUE) 
			BREAK
			CASE VEHICLE_PICKUP_ROCKET_HOMING
				VEHICLE_WEAPONS_ENABLE_ROCKET_HOMING(vehicleType, playerVeh, playerPed)
			BREAK
			CASE VEHICLE_PICKUP_ROCKET_STRAIGHT
				VEHICLE_WEAPONS_ENABLE_ROCKET_STRAIGHT(vehicleType, playerVeh, playerPed)
			BREAK
			CASE VEHICLE_PICKUP_GUN
				VEHICLE_WEAPONS_ENABLE_GUN(vehicleType, playerVeh, playerPed)
			BREAK
		ENDSWITCH
		lastVehiclePickup = currentPickup
	ENDIF

	IF currentPickup != VEHICLE_PICKUP_INIT
		IF currentPickup != VEHICLE_PICKUP_GUN
			IF vehicleType = VEHICLE_RACE_TYPE_RUINER
				DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET, playerVeh,playerPed)// This might still be necessary, check it
			ELIF vehicleType = VEHICLE_RACE_TYPE_BLAZER
				DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_CANNON_BLAZER, playerVeh,playerPed)
			ENDIF
			IF currentPickup = VEHICLE_PICKUP_BOOST
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP)
			ENDIF
		ENDIF
		SWITCH currentPickup
			CASE VEHICLE_PICKUP_ROCKET_HOMING
				SET_PLAYER_LOCKON(GET_PLAYER_INDEX(), TRUE)
				IF GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1) = 0
					g_VehicleRocketInfo.bIsCollected = FALSE
				ENDIF
			BREAK
			CASE VEHICLE_PICKUP_ROCKET_STRAIGHT
				SET_PLAYER_LOCKON(GET_PLAYER_INDEX(), FALSE)
				IF GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1) = 0
					g_VehicleRocketInfo.bIsCollected = FALSE
				ENDIF
			BREAK
			CASE VEHICLE_PICKUP_GUN
				g_VehicleGunInfo.iBullets = GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0)
				IF g_VehicleGunInfo.iBullets = 0
					PRINTLN("[SPECIAL_PICKUPS] MAINTAIN_VEHICLE_SPECIAL_WEAPONS - gun out of ammo")
					g_VehicleGunInfo.bCollected = FALSE
					UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_NONE, vehicleType, lastVehiclePickup)
				ENDIF
			BREAK
			DEFAULT
				IF vehicleType = VEHICLE_RACE_TYPE_RUINER
					// This hides the targetting indicator. Doesn't seem to work for the blazer
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
					VEHICLE_WEAPONS_DISABLE_ALL(vehicleType, playerVeh, playerPed)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	IF isInVehicle
		SET_PED_CAN_SWITCH_WEAPON(playerPed, FALSE) 
	ENDIF
	PRINTLN("[SPECIAL_PICKUPS] MAINTAIN_VEHICLE_SPECIAL_WEAPONS - currentPickup: ", currentPickup, " vehicleType: ", vehicleType)
ENDPROC

PROC UPDATE_RUINER_GTA_RACE(SPECIAL_VEHICLE_PICKUP &lastVehiclePickup)
	IF NOT HAS_SPECIAL_PICKUP_BEEN_COLLECTED()
		UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_NONE, VEHICLE_RACE_TYPE_RUINER, lastVehiclePickup)
	ELSE
		IF g_VehicleRocketInfo.bIsCollected = TRUE
			SWITCH g_VehicleRocketInfo.eRocketType
				CASE VEHICLE_ROCKET_HOMING_ONLY
					UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_ROCKET_HOMING, VEHICLE_RACE_TYPE_RUINER, lastVehiclePickup)
				BREAK
				CASE VEHICLE_ROCKET_NON_HOMING_ONLY
					UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_ROCKET_STRAIGHT, VEHICLE_RACE_TYPE_RUINER, lastVehiclePickup)
				BREAK
				CASE VEHICLE_ROCKET_NORMAL
					// FIXME make this a switchable rocket instead, if we care.
					UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_ROCKET_STRAIGHT, VEHICLE_RACE_TYPE_RUINER, lastVehiclePickup)
				BREAK
			ENDSWITCH
			#IF IS_DEBUG_BUILD
			//bigRaceVarsPassed.debugRaceVars.eRocketType = ENUM_TO_INT(g_VehicleRocketInfo.eRocketType)
			#ENDIF
		ELIF g_VehicleGunInfo.bCollected
			UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_GUN, VEHICLE_RACE_TYPE_RUINER, lastVehiclePickup)
		ELSE
			IF g_VehicleBoostInfo.bCollected
			OR g_VehicleBoostInfo.bIsActive
				UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_BOOST, VEHICLE_RACE_TYPE_RUINER, lastVehiclePickup)
			ELSE
				UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_NONE, VEHICLE_RACE_TYPE_RUINER, lastVehiclePickup)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC UPDATE_BLAZER_GTA_RACE(SPECIAL_VEHICLE_PICKUP &lastVehiclePickup)
	IF g_VehicleRocketInfo.bIsCollected = TRUE
		SCRIPT_ASSERT("Rocket pickups not allowed in the blazer. Bug with content, if content decide they want to allow this speak to Alasdair Deacon")
		PRINTLN("[SPECIAL_PICKUPS] MAINTAIN_VEHICLE_SPECIAL_WEAPONS - Blazer rockets picked up! Ignoring")
		g_VehicleRocketInfo.bIsCollected = FALSE
		UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_NONE, VEHICLE_RACE_TYPE_BLAZER, lastVehiclePickup)
	ELIF g_VehicleGunInfo.bCollected
		UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_GUN, VEHICLE_RACE_TYPE_BLAZER, lastVehiclePickup)
	ELIF g_VehicleBoostInfo.bCollected
		g_VehicleBoostInfo.bCollected = FALSE
		SCRIPT_ASSERT("Boost pickups not allowed in the blazer. Bug with content, if content decide they want to allow this speak to Alasdair Deacon")
		PRINTLN("[SPECIAL_PICKUPS] MAINTAIN_VEHICLE_SPECIAL_WEAPONS - Blazer boost picked up! Ignoring")
		UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_NONE,VEHICLE_RACE_TYPE_BLAZER, lastVehiclePickup)
	ELSE
		UPDATE_VEHICLE_WEAPONS(VEHICLE_PICKUP_NONE, VEHICLE_RACE_TYPE_BLAZER, lastVehiclePickup)
	ENDIF
ENDPROC

