//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_simple_cutscene.sch																		//
// Description: Simple cutscene system to go with simple interior.											//
// Written by:  Tymon																						//
// Date:  		29/08/2016																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
USING "globals.sch"
USING "net_cutscene.sch"
//USING "net_simple_interior.sch"
//USING "net_simple_cutscene_common.sch"

///// PURPOSE:
/////    
///// PARAMS:
/////    cutscene - 
/////    eSimpleInteriorForCutscene (optional) - simple interior that the cutscene is played in, all coords will be relative
//PROC SIMPLE_CUTSCENE_CREATE(SIMPLE_CUTSCENE &cutscene, STRING strName, SIMPLE_INTERIORS eSimpleInteriorForCutscene = SIMPLE_INTERIOR_INVALID)
//	cutscene.strName = strName
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_CREATE - Created.")
//	#ENDIF
//	
//	IF eSimpleInteriorForCutscene != SIMPLE_INTERIOR_INVALID
//		cutscene.bUseRelativePos = TRUE
//		TEXT_LABEL_63 txtFoo
//		GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(eSimpleInteriorForCutscene, txtFoo, cutscene.vBasePos, cutscene.fBaseHeading)
//		
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_CREATE - With simple interior: ", GET_SIMPLE_INTERIOR_DEBUG_NAME(eSimpleInteriorForCutscene))
//		#ENDIF
//	ENDIF
//ENDPROC

ENUM SIMPLE_CUTSCENE_SETUP
	SCS_DEFAULT = BIT0,
	SCS_DO_NOT_RETURN_PLAYER_CONTROL = BIT1, //when cutscnee ends do not return palyer control
	SCS_LEAVE_PLAYER_REMOTELY_VISIBLE = BIT2, //do not hide the remote player remotely.
	SCS_PREVENT_VISIBLITY_CHANGES_START = BIT3, //prevent visibility changes flag will be set when taking control from palyer away on cutscene start.
	SCS_PREVENT_FROZEN_CHANGES_START = BIT4, //dont freeze player when control is taken at the start of the cutscne
	SCS_HIDE_PROJECTILES = BIT5, //Will hide projectiles for the local palyer only, no remote cleanup, will hide for the duration of the cutscene.
	SCS_PREVENT_INVINCIBILITY_CHANGES_END = BIT6, //Prevent changes to player invincibility on cutscene end. Note that SCS_DO_NOT_RETURN_PLAYER_CONTROL will also be needed to prevent NET_SET_PLAYER_CONTROL removing invincibility.
	SCS_ALLOW_CALLS_OVER_SCENE = BIT7 //Calls throught to START_MP_CUTSCENE so that it will allow a phonecall to occur over the scene
ENDENUM

CONST_INT SCS_BS_ANIMATED_SCENE			0	//The simple cutscene should run with an animated camera
CONST_INT SCS_BS_ANIMATED_SYNC_SCENE	1	//The animated simple cutscene should run within a synchronised scene
CONST_INT SCS_BS_CREATED_SYNC_SCENE		2	//The simple cutscene system has created the sync scene for use with the animated camera

#IF IS_DEBUG_BUILD
PROC SIMPLE_CUTSCENE_DEBUG_PRINT_SETUP(INT iBitMask)
	CDEBUG1LN(DEBUG_CUTSCENE, "")
	CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_DEBUG_PRINT_SETUP - SCS_DEFAULT: ", IS_BITMASK_AS_ENUM_SET(iBitMask, SCS_DEFAULT),
	" SCS_DO_NOT_RETURN_PLAYER_CONTROL: ", IS_BITMASK_AS_ENUM_SET(iBitMask, SCS_DO_NOT_RETURN_PLAYER_CONTROL),
	" SCS_LEAVE_PLAYER_REMOTELY_VISIBLE: ", IS_BITMASK_AS_ENUM_SET(iBitMask, SCS_LEAVE_PLAYER_REMOTELY_VISIBLE),
	" SCS_PREVENT_VISIBLITY_CHANGES_START: ", IS_BITMASK_AS_ENUM_SET(iBitMask, SCS_PREVENT_VISIBLITY_CHANGES_START),
	" SCS_PREVENT_FROZEN_CHANGES_START: ", IS_BITMASK_AS_ENUM_SET(iBitMask, SCS_PREVENT_FROZEN_CHANGES_START),
	" SCS_HIDE_PROJECTILES: ", IS_BITMASK_AS_ENUM_SET(iBitMask, SCS_HIDE_PROJECTILES),
	" SCS_PREVENT_INVINCIBILITY_CHANGES_END: ", IS_BITMASK_AS_ENUM_SET(iBitMask, SCS_PREVENT_INVINCIBILITY_CHANGES_END))
	CDEBUG1LN(DEBUG_CUTSCENE, "")
ENDPROC
#ENDIF

PROC SIMPLE_CUTSCENE_SET_RELATIVE_POSITION(SIMPLE_CUTSCENE &cutscene, VECTOR vCoords, FLOAT fHeading)
	cutscene.bUseRelativePos = TRUE
	cutscene.vBasePos = vCoords
	cutscene.fBaseHeading = fHeading
	#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_SET_RELATIVE_POSITION - vBasePos = ", vCoords, ", fBaseHeading = ", fHeading)
	#ENDIF
ENDPROC

FUNC VECTOR _TRANSFORM_SIMPLE_CUTSCENE_COORDS_TO_WORLD_COORDS(VECTOR &vBasePos, FLOAT &fBaseHeading, VECTOR vCoords)
	RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBasePos, fBaseHeading, vCoords)
ENDFUNC

FUNC FLOAT _TRANSFORM_SIMPLE_CUTSCENE_HEADING_TO_WORLD_HEADING(FLOAT &fBaseHeading, FLOAT fHeading)
	FLOAT fResult = fHeading + fBaseHeading
	
	WHILE fResult < 0.0
		fResult += 360.0
	ENDWHILE
	WHILE fResult >= 360.0
		fResult -= 360.0
	ENDWHILE
	
	RETURN fResult
ENDFUNC

FUNC VECTOR _TRANSFORM_WORLD_COORDS_TO_SIMPLE_CUTSCENE_COORDS(VECTOR &vBasePos, FLOAT &fBaseHeading, VECTOR vCoords)
	VECTOR vResult = vCoords - vBasePos
	RotateVec(vResult, <<0.0, 0.0, fBaseHeading * -1.0 >>)
	RETURN vResult
ENDFUNC

FUNC FLOAT _TRANSFORM_WORLD_HEADING_TO_SIMPLE_CUTSCENE_HEADING(FLOAT &fBaseHeading, FLOAT fHeading)
	FLOAT fDiff = fHeading - fBaseHeading
	
	WHILE fDiff < 0.0
		fDiff += 360.0
	ENDWHILE
	WHILE fDiff >= 360.0
		fDiff -= 360.0
	ENDWHILE
	
	RETURN fDiff
ENDFUNC

PROC _TRANSFORM_SCENE_COORDS_TO_LOCAL_COORDS_EXPANDED(INT iScenesCount, VECTOR &vBasePos, FLOAT &fBaseHeading, BOOL bUseRelativePos, SIMPLE_CUTSCENE_SHOT &sScenes[], INT iSceneID)
	IF iSceneID >= 0 AND iSceneID < iScenesCount
		IF bUseRelativePos
			sScenes[iSceneID].vCamCoordsStart = _TRANSFORM_WORLD_COORDS_TO_SIMPLE_CUTSCENE_COORDS(vBasePos, fBaseHeading, sScenes[iSceneID].vCamCoordsStart)
			sScenes[iSceneID].vCamRotStart.Z = _TRANSFORM_WORLD_HEADING_TO_SIMPLE_CUTSCENE_HEADING(fBaseHeading, sScenes[iSceneID].vCamRotStart.Z)
			sScenes[iSceneID].vCamCoordsFinish = _TRANSFORM_WORLD_COORDS_TO_SIMPLE_CUTSCENE_COORDS(vBasePos, fBaseHeading, sScenes[iSceneID].vCamCoordsFinish)
			sScenes[iSceneID].vCamRotFinish.Z = _TRANSFORM_WORLD_HEADING_TO_SIMPLE_CUTSCENE_HEADING(fBaseHeading, sScenes[iSceneID].vCamRotFinish.Z)
		ENDIF
	ENDIF
ENDPROC

PROC _TRANSFORM_SCENE_COORDS_TO_LOCAL_COORDS(SIMPLE_CUTSCENE &cutscene, INT iSceneID)
	_TRANSFORM_SCENE_COORDS_TO_LOCAL_COORDS_EXPANDED(cutscene.iScenesCount, cutscene.vBasePos, cutscene.fBaseHeading, cutscene.bUseRelativePos, cutscene.sScenes, iSceneID)
ENDPROC

PROC _TRANSFORM_LIGHT_SCENE_COORDS_TO_LOCAL_COORDS(SIMPLE_CUTSCENE &cutscene, SIMPLE_CUTSCENE_SHOT &sScenes[], INT iSceneID)
	_TRANSFORM_SCENE_COORDS_TO_LOCAL_COORDS_EXPANDED(cutscene.iScenesCount, cutscene.vBasePos, cutscene.fBaseHeading, cutscene.bUseRelativePos, sScenes, iSceneID)
ENDPROC

PROC _TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS_EXPANDED(VECTOR &vBasePos, FLOAT &fBaseHeading, BOOL bUseRelativePos, INT iScenesCount, SIMPLE_CUTSCENE_SHOT &sScenes[], INT iSceneID)
	IF iSceneID >= 0 AND iSceneID < iScenesCount
		IF bUseRelativePos
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_CUTSCENE] _TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS - vCamCoordsStart before: ", sScenes[iSceneID].vCamCoordsStart)
				PRINTLN("[SIMPLE_CUTSCENE] _TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS - vCamRotStart.Z before: ", sScenes[iSceneID].vCamRotStart.Z)
				PRINTLN("[SIMPLE_CUTSCENE] _TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS - vCamCoordsFinish before: ", sScenes[iSceneID].vCamCoordsFinish)
				PRINTLN("[SIMPLE_CUTSCENE] _TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS - vCamRotFinish.Z before: ", sScenes[iSceneID].vCamRotFinish.Z)
			#ENDIF
		
			sScenes[iSceneID].vCamCoordsStart = _TRANSFORM_SIMPLE_CUTSCENE_COORDS_TO_WORLD_COORDS(vBasePos, fBaseHeading, sScenes[iSceneID].vCamCoordsStart)
			sScenes[iSceneID].vCamRotStart.Z = _TRANSFORM_SIMPLE_CUTSCENE_HEADING_TO_WORLD_HEADING(fBaseHeading, sScenes[iSceneID].vCamRotStart.Z)
			sScenes[iSceneID].vCamCoordsFinish = _TRANSFORM_SIMPLE_CUTSCENE_COORDS_TO_WORLD_COORDS(vBasePos, fBaseHeading, sScenes[iSceneID].vCamCoordsFinish)
			sScenes[iSceneID].vCamRotFinish.Z = _TRANSFORM_SIMPLE_CUTSCENE_HEADING_TO_WORLD_HEADING(fBaseHeading, sScenes[iSceneID].vCamRotFinish.Z)
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_CUTSCENE] _TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS - vCamCoordsStart after: ", sScenes[iSceneID].vCamCoordsStart)
				PRINTLN("[SIMPLE_CUTSCENE] _TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS - vCamRotStart.Z after: ", sScenes[iSceneID].vCamRotStart.Z)
				PRINTLN("[SIMPLE_CUTSCENE] _TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS - vCamCoordsFinish after: ", sScenes[iSceneID].vCamCoordsFinish)
				PRINTLN("[SIMPLE_CUTSCENE] _TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS - vCamRotFinish.Z after: ", sScenes[iSceneID].vCamRotFinish.Z)
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC _TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS(SIMPLE_CUTSCENE &cutscene, INT iSceneID)
	_TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS_EXPANDED(cutscene.vBasePos, cutscene.fBaseHeading, cutscene.bUseRelativePos, cutscene.iScenesCount, cutscene.sScenes, iSceneID)
ENDPROC

PROC _TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS_LIGHT(SIMPLE_CUTSCENE_LIGHT &cutscene, SIMPLE_CUTSCENE_SHOT &sScenes[], INT iSceneID)
	_TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS_EXPANDED(cutscene.vBasePos, cutscene.fBaseHeading, cutscene.bUseRelativePos, cutscene.iScenesCount, sScenes, iSceneID)
ENDPROC

PROC SIMPLE_CUTSCENE_LIGHT_ADD_SCENE(
	SIMPLE_CUTSCENE_LIGHT &cutscene, 
	SIMPLE_CUTSCENE_SHOT &sScenes[],
	INT iDuration, STRING strName, 
	VECTOR vCamCoordsStart, VECTOR vCamRotStart, FLOAT fCamFovStart,
	VECTOR vCamCoordsFinish, VECTOR vCamRotFinish, FLOAT fCamFovFinish,
	FLOAT fCamShake,
	INT iFadeInTime = 0, INT iFadeOutTime = 0,
	CAMERA_GRAPH_TYPE graphTypePos = GRAPH_TYPE_LINEAR, CAMERA_GRAPH_TYPE graphTypeRot = GRAPH_TYPE_LINEAR
	)
	
	IF cutscene.iScenesCount = COUNT_OF(sScenes)
		ASSERTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_ADD_SCENE - Can't add new scene, max scenes count reached!")
		EXIT
	ENDIF
	
	sScenes[cutscene.iScenesCount].iDuration = iDuration
	sScenes[cutscene.iScenesCount].strName = strName
	
	sScenes[cutscene.iScenesCount].vCamCoordsStart = vCamCoordsStart
	sScenes[cutscene.iScenesCount].vCamRotStart = vCamRotStart
	sScenes[cutscene.iScenesCount].fCamFovStart = fCamFovStart
	
	sScenes[cutscene.iScenesCount].vCamCoordsFinish = vCamCoordsFinish
	sScenes[cutscene.iScenesCount].vCamRotFinish = vCamRotFinish
	sScenes[cutscene.iScenesCount].fCamFovFinish = fCamFovFinish
	
	sScenes[cutscene.iScenesCount].fCamShake = fCamShake
	
	sScenes[cutscene.iScenesCount].iFadeInTime = iFadeInTime
	sScenes[cutscene.iScenesCount].iFadeOutTime = iFadeOutTime
	
	sScenes[cutscene.iScenesCount].graphTypePos = graphTypePos
	sScenes[cutscene.iScenesCount].graphTypeRot = graphTypeRot
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_ADD_SCENE - Added new scene number ", cutscene.iScenesCount, " with name: ", strName)
	#ENDIF
	
	cutscene.iScenesCount += 1
	cutscene.iTotalDuration += iDuration
	
	// Transform local coords to simple interior coords if they are within simple interior
	_TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS_LIGHT(cutscene, sScenes, cutscene.iScenesCount - 1)
	
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    cutscene - 
///    iDuration - 
///    strName - 
///    vCamCoordsStart - 
///    vCamRotStart - 
///    fCamFovStart - 
///    vCamCoordsFinish - 
///    vCamRotFinish - 
///    fCamFovFinish - 
///    fCamShake - 
///    iFadeInTime - 
///    iFadeOutTime - 
///    graphTypePos - 
///    graphTypeRot - 
///    iHoldLastShotTime - How long to hold on the last shot following the camera movement
PROC SIMPLE_CUTSCENE_ADD_SCENE(
	SIMPLE_CUTSCENE &cutscene, INT iDuration, STRING strName, 
	VECTOR vCamCoordsStart, VECTOR vCamRotStart, FLOAT fCamFovStart,
	VECTOR vCamCoordsFinish, VECTOR vCamRotFinish, FLOAT fCamFovFinish,
	FLOAT fCamShake,
	INT iFadeInTime = 0, INT iFadeOutTime = 0,
	CAMERA_GRAPH_TYPE graphTypePos = GRAPH_TYPE_LINEAR, CAMERA_GRAPH_TYPE graphTypeRot = GRAPH_TYPE_LINEAR, 
	INT iHoldLastShotTime = 0
)
	IF cutscene.iScenesCount = COUNT_OF(cutscene.sScenes)
		ASSERTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_ADD_SCENE - Can't add new scene, max scenes count reached!")
		CDEBUG1LN(DEBUG_CUTSCENE, "SIMPLE_CUTSCENE_ADD_SCENE - Can't add new scene, max scenes count reached! with name: ", strName)
		EXIT
	ENDIF
	
	cutscene.sScenes[cutscene.iScenesCount].iDuration = iDuration
	cutscene.sScenes[cutscene.iScenesCount].strName = strName
	
	cutscene.sScenes[cutscene.iScenesCount].vCamCoordsStart = vCamCoordsStart
	cutscene.sScenes[cutscene.iScenesCount].vCamRotStart = vCamRotStart
	cutscene.sScenes[cutscene.iScenesCount].fCamFovStart = fCamFovStart
	
	cutscene.sScenes[cutscene.iScenesCount].vCamCoordsFinish = vCamCoordsFinish
	cutscene.sScenes[cutscene.iScenesCount].vCamRotFinish = vCamRotFinish
	cutscene.sScenes[cutscene.iScenesCount].fCamFovFinish = fCamFovFinish
	
	cutscene.sScenes[cutscene.iScenesCount].fCamShake = fCamShake
	
	cutscene.sScenes[cutscene.iScenesCount].iFadeInTime = iFadeInTime
	cutscene.sScenes[cutscene.iScenesCount].iFadeOutTime = iFadeOutTime
	
	cutscene.sScenes[cutscene.iScenesCount].graphTypePos = graphTypePos
	cutscene.sScenes[cutscene.iScenesCount].graphTypeRot = graphTypeRot
	
	cutscene.sScenes[cutscene.iScenesCount].iHoldLastShotTime = iHoldLastShotTime
	
	cutscene.iSyncSceneID = -1
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_ADD_SCENE - Added new scene number ", cutscene.iScenesCount, " with name: ", strName)
	#ENDIF
	
	cutscene.iScenesCount += 1
	cutscene.iTotalDuration += iDuration
	CLEAR_BIT(cutscene.iBS, SCS_BS_ANIMATED_SCENE)
	CLEAR_BIT(cutscene.iBS, SCS_BS_ANIMATED_SYNC_SCENE)
	
	// Transform local coords to simple interior coords if they are within simple interior
	_TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS(cutscene, cutscene.iScenesCount - 1)
ENDPROC

/// PURPOSE:
///    Variation on SIMPLE_CUTSCENE_ADD_SCENE that can support an animated camera
/// PARAMS:
///    cutscene - passed by reference
///    bUseCamInSyncScene - Specify the camera should use a scync scene rather than just playing the animation alone
///    NOTE: The simple cutecne system will create the sync scene using CREATE_SYNCHRONIZED_SCENE because code do NOT allow assigning an ID for later use
PROC SIMPLE_CUTSCENE_ADD_ANIMATED_CAM_SCENE(
	SIMPLE_CUTSCENE &cutscene, INT iDuration, STRING strName, 
	VECTOR vScenePosition, VECTOR vSceneRotation, 	
	STRING sAnimDict, STRING sAnim, BOOL bUseCamInSyncScene = FALSE,
	INT iFadeInTime = 0, INT iFadeOutTime = 0,
	CAM_ANIMATION_FLAGS AnimFlags = CAF_DEFAULT, EULER_ROT_ORDER RotOrder = EULER_YXZ
)
	IF cutscene.iScenesCount = 1
		ASSERTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_ADD_ANIMATED_CAM_SCENE - Can't add new scene, max scenes count reached!")
		CDEBUG1LN(DEBUG_CUTSCENE, "SIMPLE_CUTSCENE_ADD_ANIMATED_CAM_SCENE - Can't add new scene, max scenes count reached! with name: ", strName)
		EXIT
	ENDIF
	
	cutscene.sScenes[cutscene.iScenesCount].iDuration 			= iDuration
	cutscene.sScenes[cutscene.iScenesCount].strName 			= strName
	
	cutscene.sScenes[cutscene.iScenesCount].iFadeInTime 		= iFadeInTime
	cutscene.sScenes[cutscene.iScenesCount].iFadeOutTime 		= iFadeOutTime
		
	cutscene.iSyncSceneID 			= -1
	cutscene.sSceneAnimDict 		= sAnimDict
	cutscene.sSceneCamAnim			= sAnim
	cutscene.vAnimScenePosition 	= vScenePosition
	cutscene.vAnimSceneRotation 	= vSceneRotation
	cutscene.AnimFlags				= AnimFlags
	cutscene.RotOrder				= RotOrder
	
	CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_ADD_ANIMATED_CAM_SCENE - Added new scene number ", cutscene.iScenesCount, " with name: ", strName, " using anim dict: ", sAnimDict, " anim: ", sAnim, " bUseCamInSyncScene: ", bUseCamInSyncScene)
	
	cutscene.iScenesCount += 1
	cutscene.iTotalDuration += iDuration
	SET_BIT(cutscene.iBS, SCS_BS_ANIMATED_SCENE)
	
	IF bUseCamInSyncScene
		SET_BIT(cutscene.iBS, SCS_BS_ANIMATED_SYNC_SCENE)
	ELSE
		CLEAR_BIT(cutscene.iBS, SCS_BS_ANIMATED_SYNC_SCENE)
	ENDIF	
	
	#IF IS_DEBUG_BUILD
	IF IS_VECTOR_ZERO(cutscene.vAnimScenePosition)
		SCRIPT_ASSERT("SIMPLE_CUTSCENE_ADD_ANIMATED_CAM_SCENE - Trying to use an animated camera without specifying a scene position!")
	ENDIF
			
	IF IS_STRING_NULL_OR_EMPTY(cutscene.sSceneAnimDict)
		SCRIPT_ASSERT("SIMPLE_CUTSCENE_ADD_ANIMATED_CAM_SCENE - Trying to use the camera with a sync scene without specifying an anim dict")
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(cutscene.sSceneCamAnim)
		SCRIPT_ASSERT("SIMPLE_CUTSCENE_ADD_ANIMATED_CAM_SCENE - Trying to use the camera with a sync scene without specifying an animation!")
	ENDIF
	#ENDIF
	
	// Transform local coords to simple interior coords if they are within simple interior
	//_TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS(cutscene, cutscene.iScenesCount - 1)
ENDPROC

PROC SIMPLE_CUTSCENE_ADD_HELP(SIMPLE_CUTSCENE &cutscene, INT iStartTime, INT iDuration, TEXT_LABEL_23 txtHelp)
	IF cutscene.iHelpsCount = COUNT_OF(cutscene.sHelps)
		ASSERTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_ADD_HELP - Can't add new help, max helps count reached!")
		EXIT
	ENDIF
	
	cutscene.sHelps[cutscene.iHelpsCount].iStartTime = iStartTime
	cutscene.sHelps[cutscene.iHelpsCount].iDuration = iDuration
	cutscene.sHelps[cutscene.iHelpsCount].txtHelp = txtHelp
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_ADD_HELP - Added new help number ", cutscene.iHelpsCount, " with label: ", txtHelp, " start time: ", iStartTime, " duration: ", iDuration)
	#ENDIF
	
	cutscene.iHelpsCount += 1
ENDPROC

PROC SIMPLE_CUTSCENE_ADD_DOF_TO_SCENE(SIMPLE_CUTSCENE &cutscene, INT iScene, FLOAT fDOFStrength, FLOAT fDOFNearOut, FLOAT fDOFFarOut, FLOAT fDOFNearIn, FLOAT fDOFFarIn)
	IF iScene > 0 AND iScene < cutscene.iScenesCount
		cutscene.sScenes[iScene].bUseDOF = TRUE
		cutscene.sScenes[iScene].fDOFStrength = fDOFStrength
		cutscene.sScenes[iScene].fDOFNearOut = fDOFNearOut
		cutscene.sScenes[iScene].fDOFFarOut = fDOFFarOut
		cutscene.sScenes[iScene].fDOFNearIn = fDOFNearIn
		cutscene.sScenes[iScene].fDOFFarIn = fDOFFarIn
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_ADD_DOF_TO_SCENE - Added DOF to scene ", iScene)
		#ENDIF
	ENDIF
ENDPROC

PROC SIMPLE_CUTSCENE_REINIT(SIMPLE_CUTSCENE &cutscene)
	cutscene.iBSScenesInit = 0
	cutscene.iBSHelpsInit = 0
	cutscene.iCurrentScene = 0
	cutscene.iCurrentHelp = 0
	cutscene.bPlaying = FALSE
	cutscene.iCurrentSceneTimer = 0
	cutscene.iCurrentHelpTimer = 0
	cutscene.iBSFadeOutStarted = 0
	cutscene.bHelpTimerInit = FALSE
	cutscene.iTotalElapsedTime = 0
	cutscene.iCurrentSceneElapsedTime = 0
	cutscene.iSetupBM = 0
	#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_REINIT - Done.")
	#ENDIF
ENDPROC

PROC SIMPLE_CUTSCENE_LIGHT_REINIT(SIMPLE_CUTSCENE_LIGHT &cutscene)
	cutscene.iBSScenesInit = 0
	cutscene.iBSHelpsInit = 0
	cutscene.iCurrentScene = 0
	cutscene.iCurrentHelp = 0
	cutscene.bPlaying = FALSE
	cutscene.iCurrentSceneTimer = 0
	cutscene.iCurrentHelpTimer = 0
	cutscene.iBSFadeOutStarted = 0
	cutscene.bHelpTimerInit = FALSE
	cutscene.iTotalElapsedTime = 0
	cutscene.iCurrentSceneElapsedTime = 0
	#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_REINIT - Done.")
	#ENDIF
ENDPROC

PROC SIMPLE_CUTSCENE_CLEAR_ALL_DATA(SIMPLE_CUTSCENE &cutscene)
	SIMPLE_CUTSCENE_REINIT(cutscene)
	cutscene.iBS = 0
	cutscene.iScenesCount = 0
	cutscene.iHelpsCount = 0
	cutscene.iTotalDuration = 0
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_CLEAR_ALL_DATA - Done.")
	#ENDIF
ENDPROC

PROC SIMPLE_CUTSCENE_LIGHT_CLEAR_ALL_DATA(SIMPLE_CUTSCENE_LIGHT &cutscene)
	SIMPLE_CUTSCENE_LIGHT_REINIT(cutscene)
	cutscene.iScenesCount = 0
	cutscene.iHelpsCount = 0
	cutscene.iTotalDuration = 0
	#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_LIGHT_CLEAR_ALL_DATA - Done.")
	#ENDIF
ENDPROC

PROC SIMPLE_CUTSCENE_KILL_CAMERA(SIMPLE_CUTSCENE &cutscene)
	IF DOES_CAM_EXIST(cutscene.camera)
		IF IS_CAM_ACTIVE(cutscene.camera)
			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_KILL_CAMERA - cam was active.")
			SET_CAM_ACTIVE(cutscene.camera, FALSE)
		ENDIF
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		STOP_CAM_SHAKING(cutscene.camera, TRUE)
		#IF IS_DEBUG_BUILD
			VECTOR vCamCoords
			vCamCoords = GET_CAM_COORD(cutscene.camera)
			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_KILL_CAMERA - Done. Coord: ", vCamCoords, " native: ", NATIVE_TO_INT(cutscene.camera))
		#ENDIF
		DESTROY_CAM(cutscene.camera)
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_KILL_CAMERA - Cam doesnt exist.")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Ends the current playing simple cutscene
/// PARAMS:
///    cutscene - passed by reference
///    bDontReturnControl - Player contorl?
///    bDontKillCamera - Cleanup the camera?
///    bCleanupMPCutscene - Should this call CLEANUP_MP_CUTSCENE
///    bCleanupNetCutscene - !Used in conjunction with bCleanupMPCutscene! Should CLEANUP_MP_CUTSCENE stop a network scene?
PROC SIMPLE_CUTSCENE_STOP(SIMPLE_CUTSCENE &cutscene, BOOL bDontReturnControl = FALSE, BOOL bDontKillCamera = FALSE, BOOL bCleanupMPCutscene = TRUE, BOOL bPreventInvincibilityChanges = FALSE, BOOL bCleanupNetCutscene = TRUE)
	IF NOT cutscene.bPlaying
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_STOP - Cutscene is not playing.")
		#ENDIF
		EXIT
	ENDIF
	
	IF NOT bDontKillCamera
		SIMPLE_CUTSCENE_KILL_CAMERA(cutscene)
	ENDIF
	
	IF bCleanupMPCutscene
		CLEANUP_MP_CUTSCENE(TRUE, FALSE, bPreventInvincibilityChanges, bCleanupNetCutscene)
	ENDIF
	
	BOOL bRestoreNoLoadingScreen = GET_NO_LOADING_SCREEN()
	
	IF NOT bDontReturnControl
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_PREVENT_VISIBILITY_CHANGES)
	ENDIF
	
	IF GET_NO_LOADING_SCREEN() != bRestoreNoLoadingScreen
		// the above function alters no loading screen, restore it
		SET_NO_LOADING_SCREEN(bRestoreNoLoadingScreen)
	ENDIF
	
	// Reset all the warehouse cutscene data
	SIMPLE_CUTSCENE_REINIT(cutscene)
	
	CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
	GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iCutsceneHash = 0
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_STOP - Done.")
	#ENDIF
ENDPROC

PROC SIMPLE_CUTSCENE_LIGHT_STOP(SIMPLE_CUTSCENE_LIGHT &cutscene, BOOL bDontReturnControl = FALSE, BOOL bDontKillCamera = FALSE)
	IF NOT cutscene.bPlaying
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_LIGHT_STOP - Cutscene is not playing.")
		#ENDIF
		EXIT
	ENDIF
	
	IF NOT bDontKillCamera
		IF DOES_CAM_EXIST(cutscene.camera)
			IF IS_CAM_ACTIVE(cutscene.camera)
				PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_KILL_CAMERA - cam was active.")
				SET_CAM_ACTIVE(cutscene.camera, FALSE)
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			STOP_CAM_SHAKING(cutscene.camera, TRUE)
			#IF IS_DEBUG_BUILD
				VECTOR vCamCoords
				vCamCoords = GET_CAM_COORD(cutscene.camera)
				PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_KILL_CAMERA - Done. Coord: ", vCamCoords, " native: ", NATIVE_TO_INT(cutscene.camera))
			#ENDIF
			DESTROY_CAM(cutscene.camera)
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_KILL_CAMERA - Cam doesnt exist.")
			#ENDIF
		ENDIF
	ENDIF
	
	CLEANUP_MP_CUTSCENE(TRUE, FALSE)
	
	BOOL bRestoreNoLoadingScreen = GET_NO_LOADING_SCREEN()
	
	IF NOT bDontReturnControl
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_PREVENT_VISIBILITY_CHANGES)
	ENDIF
	
	IF GET_NO_LOADING_SCREEN() != bRestoreNoLoadingScreen
		// the above function alters no loading screen, restore it
		SET_NO_LOADING_SCREEN(bRestoreNoLoadingScreen)
	ENDIF
	
	// Reset all the warehouse cutscene data
	SIMPLE_CUTSCENE_LIGHT_REINIT(cutscene)
	
	CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
	GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iCutsceneHash = 0
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_LIGHT_STOP - Done.")
	#ENDIF
ENDPROC

PROC SIMPLE_CUTSCENE_LIGHT_MAINTAIN(SIMPLE_CUTSCENE_LIGHT &cutscene, SIMPLE_CUTSCENE_SHOT &sScenes[], SIMPLE_CUTSCENE_HELP &sHelps[], BOOL bDontReturnPlayerControl = FALSE)
	IF NOT cutscene.bPlaying
		EXIT
	ENDIF
	
	IF cutscene.iCurrentScene = 0
	AND NOT IS_BIT_SET(cutscene.iBSScenesInit, cutscene.iCurrentScene)
		
		// Very first init of cutscene
		SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, FALSE)
		IF NOT MPGlobals.bStartedMPCutscene
			START_MP_CUTSCENE(TRUE)
		ENDIF
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		cutscene.iStartTime = GET_GAME_TIMER()
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - START_MP_CUTSCENE(TRUE)")
		#ENDIF
	ENDIF
	
	IF cutscene.iCurrentHelp = 0
	AND NOT cutscene.bHelpTimerInit
		cutscene.iCurrentHelpTimer = GET_GAME_TIMER()
		cutscene.bHelpTimerInit = TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(cutscene.iBSScenesInit, cutscene.iCurrentScene)
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Current scene (", cutscene.iCurrentScene ,": ", sScenes[cutscene.iCurrentScene].strName, ") not yet initialised, initialising.")
			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Scene duration: ", sScenes[cutscene.iCurrentScene].iDuration)
		#ENDIF
		
		cutscene.iCurrentSceneTimer = GET_GAME_TIMER()
		
		IF NOT DOES_CAM_EXIST(cutscene.camera)
		#IF IS_DEBUG_BUILD
		AND NOT cutscene.dbg.bDontUpdateCamera
		#ENDIF
			cutscene.camera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Created camera.")
			#ENDIF
		ENDIF
		
		IF DOES_CAM_EXIST(cutscene.camera)
			SET_CAM_PARAMS(cutscene.camera, sScenes[cutscene.iCurrentScene].vCamCoordsStart, sScenes[cutscene.iCurrentScene].vCamRotStart, sScenes[cutscene.iCurrentScene].fCamFovStart)
			
			IF NOT IS_VECTOR_ZERO(sScenes[cutscene.iCurrentScene].vCamCoordsFinish)
			AND NOT IS_VECTOR_ZERO(sScenes[cutscene.iCurrentScene].vCamRotFinish)
			AND sScenes[cutscene.iCurrentScene].fCamFovFinish != 0.0
				SET_CAM_PARAMS(cutscene.camera, sScenes[cutscene.iCurrentScene].vCamCoordsFinish, sScenes[cutscene.iCurrentScene].vCamRotFinish, sScenes[cutscene.iCurrentScene].fCamFovFinish, sScenes[cutscene.iCurrentScene].iDuration, sScenes[cutscene.iCurrentScene].graphTypePos, sScenes[cutscene.iCurrentScene].graphTypeRot)
			ENDIF
			
			STOP_CAM_SHAKING(cutscene.camera, TRUE)
			IF sScenes[cutscene.iCurrentScene].fCamShake > 0.0
				SHAKE_CAM(cutscene.camera, "Hand_shake", sScenes[cutscene.iCurrentScene].fCamShake)
			ENDIF
		ENDIF
		
		IF sScenes[cutscene.iCurrentScene].iFadeInTime > 0
			DO_SCREEN_FADE_IN(sScenes[cutscene.iCurrentScene].iFadeInTime)
		ENDIF
	
		SET_BIT(cutscene.iBSScenesInit, cutscene.iCurrentScene)
	ENDIF
	
	IF DOES_CAM_EXIST(cutscene.camera)
		IF sScenes[cutscene.iCurrentScene].bUseDOF
			SET_USE_HI_DOF()
			SET_CAM_USE_SHALLOW_DOF_MODE(cutscene.camera, TRUE)
			SET_CAM_DOF_PLANES(cutscene.camera, sScenes[cutscene.iCurrentScene].fDOFNearOut, sScenes[cutscene.iCurrentScene].fDOFNearIn, sScenes[cutscene.iCurrentScene].fDOFFarIn, sScenes[cutscene.iCurrentScene].fDOFFarOut)
			SET_CAM_DOF_STRENGTH(cutscene.camera, sScenes[cutscene.iCurrentScene].fDOFStrength)
		ELSE
			SET_CAM_USE_SHALLOW_DOF_MODE(cutscene.camera, FALSE)
			SET_CAM_DOF_STRENGTH(cutscene.camera, 0.0)
		ENDIF
	ENDIF
	
	IF sScenes[cutscene.iCurrentScene].iFadeOutTime > 0
		IF NOT IS_BIT_SET(cutscene.iBSFadeOutStarted, cutscene.iCurrentScene)
			IF GET_GAME_TIMER() >= (cutscene.iCurrentSceneTimer + sScenes[cutscene.iCurrentScene].iDuration - sScenes[cutscene.iCurrentScene].iFadeOutTime)
				DO_SCREEN_FADE_OUT(sScenes[cutscene.iCurrentScene].iFadeOutTime)
				SET_BIT(cutscene.iBSFadeOutStarted, cutscene.iCurrentScene)
				#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Did fade out for scene ", cutscene.iCurrentScene)
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	INT iDuration = sScenes[cutscene.iCurrentScene].iDuration
	
	#IF IS_DEBUG_BUILD
		IF g_SimpleInteriorData.db_bPlayShortSimpleCutscenes
			iDuration = 1500
		ENDIF
	#ENDIF
	
	cutscene.iCurrentSceneElapsedTime = GET_GAME_TIMER() - cutscene.iCurrentSceneTimer
	cutscene.iTotalElapsedTime = GET_GAME_TIMER() - cutscene.iStartTime
	
	IF cutscene.iCurrentSceneElapsedTime >= iDuration
	#IF IS_DEBUG_BUILD
	OR IS_DEBUG_KEY_JUST_PRESSED(KEY_RIGHT, KEYBOARD_MODIFIER_NONE, "Skipping scene")
	#ENDIF
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Timer for current scene (", cutscene.iCurrentScene ,": ", sScenes[cutscene.iCurrentScene].strName, ") expired, next scene or quit.")
		#ENDIF
		
		// Move to next scene or finish cutscene if it's the last
		IF cutscene.iCurrentScene = cutscene.iScenesCount - 1
			// End of cutscene
			SIMPLE_CUTSCENE_LIGHT_STOP(cutscene, bDontReturnPlayerControl)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - No more scenes to play, finishing.")
			#ENDIF
		ELSE
			cutscene.iCurrentScene = cutscene.iCurrentScene + 1
			cutscene.iCurrentSceneElapsedTime = 0
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Next scene is: ", cutscene.iCurrentScene, " - ", sScenes[cutscene.iCurrentScene].strName)
			#ENDIF
		ENDIF
	ENDIF
	
	IF cutscene.bPlaying
		#IF IS_DEBUG_BUILD
		// Don't bother showing help if we're playing short debug cutscenes
		IF NOT g_SimpleInteriorData.db_bPlayShortSimpleCutscenes
		#ENDIF
		IF (GET_GAME_TIMER() - cutscene.iCurrentHelpTimer) >= sHelps[cutscene.iCurrentHelp].iStartTime
			IF NOT IS_BIT_SET(cutscene.iBSHelpsInit, cutscene.iCurrentHelp)
			
				#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Gonna show help number ", cutscene.iCurrentHelp)
				#ENDIF
				
				SET_BIT(cutscene.iBSHelpsInit, cutscene.iCurrentHelp)
			
				IF NOT IS_STRING_NULL_OR_EMPTY(sHelps[cutscene.iCurrentHelp].txtHelp)
					CLEAR_HELP(TRUE)
					PRINT_HELP(sHelps[cutscene.iCurrentHelp].txtHelp, sHelps[cutscene.iCurrentHelp].iDuration)
					
					IF cutscene.iCurrentHelp + 1 < cutscene.iHelpsCount
						cutscene.iCurrentHelp = cutscene.iCurrentHelp + 1
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - But it's empty!")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC SIMPLE_CUTSCENE_MAINTAIN(SIMPLE_CUTSCENE &cutscene, BOOL bDontKillCameraOnStop = FALSE, BOOL bCleanupMPCutscene = TRUE)
	IF NOT cutscene.bPlaying
		EXIT
	ENDIF
	
	IF cutscene.iCurrentScene = 0
	AND NOT IS_BIT_SET(cutscene.iBSScenesInit, cutscene.iCurrentScene)
		
		// Very first init of cutscene
		IF IS_BITMASK_AS_ENUM_SET(cutscene.iSetupBM, SCS_HIDE_PROJECTILES)
			NETWORK_HIDE_PROJECTILE_IN_CUTSCENE()
		ENDIF		

		SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE, IS_BITMASK_AS_ENUM_SET(cutscene.iSetupBM, SCS_LEAVE_PLAYER_REMOTELY_VISIBLE))
		IF NOT MPGlobals.bStartedMPCutscene
			START_MP_CUTSCENE(TRUE, DEFAULT, IS_BITMASK_AS_ENUM_SET(cutscene.iSetupBM, SCS_ALLOW_CALLS_OVER_SCENE))
		ENDIF
		
		NET_SET_PLAYER_CONTROL_FLAGS eFlags
		eFlags = NSPC_NONE
		
		IF IS_BITMASK_AS_ENUM_SET(cutscene.iSetupBM, SCS_PREVENT_VISIBLITY_CHANGES_START)
			SET_BITMASK_ENUM_AS_ENUM(eFlags, NSPC_PREVENT_VISIBILITY_CHANGES)
		ENDIF
		IF IS_BITMASK_AS_ENUM_SET(cutscene.iSetupBM, SCS_PREVENT_FROZEN_CHANGES_START)
			SET_BITMASK_ENUM_AS_ENUM(eFlags, NSPC_PREVENT_FROZEN_CHANGES)
		ENDIF
		
		IF IS_SKYSWOOP_AT_GROUND()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, eFlags)
		ENDIF
		
		cutscene.iStartTime = GET_GAME_TIMER()
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - START_MP_CUTSCENE(TRUE)")
		#ENDIF
	ENDIF
	
	IF cutscene.iCurrentHelp = 0
	AND NOT cutscene.bHelpTimerInit
		cutscene.iCurrentHelpTimer = GET_GAME_TIMER()
		cutscene.bHelpTimerInit = TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(cutscene.iBSScenesInit, cutscene.iCurrentScene)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Current scene (", cutscene.iCurrentScene ,": ", cutscene.sScenes[cutscene.iCurrentScene].strName, ") not yet initialised, initialising.")
			CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Scene duration: ", cutscene.sScenes[cutscene.iCurrentScene].iDuration, " iHoldLastShotTime: ", cutscene.sScenes[cutscene.iCurrentScene].iHoldLastShotTime)
		#ENDIF
		
		cutscene.iCurrentSceneTimer = GET_GAME_TIMER()
		
		IF NOT DOES_CAM_EXIST(cutscene.camera)
		#IF IS_DEBUG_BUILD
		AND NOT cutscene.dbg.bDontUpdateCamera
		#ENDIF
			IF IS_BIT_SET(cutscene.iBS, SCS_BS_ANIMATED_SCENE)
				cutscene.camera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
			ELSE
				cutscene.camera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)		
			ENDIF
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Created camera.")
			#ENDIF
		ENDIF
		
		INT iDuration = cutscene.sScenes[cutscene.iCurrentScene].iDuration
		
		IF cutscene.sScenes[cutscene.iCurrentScene].iHoldLastShotTime > 0
		AND cutscene.sScenes[cutscene.iCurrentScene].iHoldLastShotTime < cutscene.sScenes[cutscene.iCurrentScene].iDuration
			iDuration -= cutscene.sScenes[cutscene.iCurrentScene].iHoldLastShotTime
			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Camera pan duration: ", cutscene.sScenes[cutscene.iCurrentScene].iDuration, " Hold on pan end for ", cutscene.sScenes[cutscene.iCurrentScene].iHoldLastShotTime, " milliseconds")
		ELIF cutscene.sScenes[cutscene.iCurrentScene].iDuration < cutscene.sScenes[cutscene.iCurrentScene].iHoldLastShotTime
			SCRIPT_ASSERT("SIMPLE_CUTSCENE_MAINTAIN - Attempted to hold last shot to longer than the scene duration")
		ENDIF
		
		IF DOES_CAM_EXIST(cutscene.camera)
			IF IS_BIT_SET(cutscene.iBS, SCS_BS_ANIMATED_SCENE)
				IF IS_BIT_SET(cutscene.iBS, SCS_BS_ANIMATED_SYNC_SCENE)
					cutscene.iSyncSceneID = CREATE_SYNCHRONIZED_SCENE(cutscene.vAnimScenePosition, cutscene.vAnimSceneRotation, cutscene.RotOrder)
					PLAY_SYNCHRONIZED_CAM_ANIM(cutscene.camera, cutscene.iSyncSceneID, cutscene.sSceneCamAnim, cutscene.sSceneAnimDict)
					SET_BIT(cutscene.iBS, SCS_BS_CREATED_SYNC_SCENE)
				ELSE
					PLAY_CAM_ANIM(cutscene.camera, cutscene.sSceneCamAnim, 
													cutscene.sSceneAnimDict,
													cutscene.vAnimScenePosition,
													cutscene.vAnimSceneRotation,
													cutscene.AnimFlags,
													cutscene.RotOrder)
				ENDIF
			ELSE
				SET_CAM_PARAMS(cutscene.camera, cutscene.sScenes[cutscene.iCurrentScene].vCamCoordsStart, cutscene.sScenes[cutscene.iCurrentScene].vCamRotStart, cutscene.sScenes[cutscene.iCurrentScene].fCamFovStart)
				
				IF NOT IS_VECTOR_ZERO(cutscene.sScenes[cutscene.iCurrentScene].vCamCoordsFinish)
				AND NOT IS_VECTOR_ZERO( cutscene.sScenes[cutscene.iCurrentScene].vCamRotFinish)
				AND cutscene.sScenes[cutscene.iCurrentScene].fCamFovFinish != 0.0
					SET_CAM_PARAMS(cutscene.camera, cutscene.sScenes[cutscene.iCurrentScene].vCamCoordsFinish, cutscene.sScenes[cutscene.iCurrentScene].vCamRotFinish, cutscene.sScenes[cutscene.iCurrentScene].fCamFovFinish, iDuration, cutscene.sScenes[cutscene.iCurrentScene].graphTypePos, cutscene.sScenes[cutscene.iCurrentScene].graphTypeRot)
				ENDIF
				
				STOP_CAM_SHAKING(cutscene.camera, TRUE)
				IF cutscene.sScenes[cutscene.iCurrentScene].fCamShake > 0.0
					SHAKE_CAM(cutscene.camera, "Hand_shake", cutscene.sScenes[cutscene.iCurrentScene].fCamShake)
				ENDIF
			ENDIF
		ENDIF
		
		IF cutscene.sScenes[cutscene.iCurrentScene].iFadeInTime > 0
			DO_SCREEN_FADE_IN(cutscene.sScenes[cutscene.iCurrentScene].iFadeInTime)
		ENDIF
	
		SET_BIT(cutscene.iBSScenesInit, cutscene.iCurrentScene)
	ENDIF
	
	IF DOES_CAM_EXIST(cutscene.camera)
		IF cutscene.sScenes[cutscene.iCurrentScene].bUseDOF
			SET_USE_HI_DOF()
			SET_CAM_USE_SHALLOW_DOF_MODE(cutscene.camera, TRUE)
			SET_CAM_DOF_PLANES(cutscene.camera, cutscene.sScenes[cutscene.iCurrentScene].fDOFNearOut, cutscene.sScenes[cutscene.iCurrentScene].fDOFNearIn, cutscene.sScenes[cutscene.iCurrentScene].fDOFFarIn, cutscene.sScenes[cutscene.iCurrentScene].fDOFFarOut)
			SET_CAM_DOF_STRENGTH(cutscene.camera, cutscene.sScenes[cutscene.iCurrentScene].fDOFStrength)
		ELSE
			SET_CAM_USE_SHALLOW_DOF_MODE(cutscene.camera, FALSE)
			SET_CAM_DOF_STRENGTH(cutscene.camera, 0.0)
		ENDIF
	ENDIF
	
	IF cutscene.sScenes[cutscene.iCurrentScene].iFadeOutTime > 0
		IF NOT IS_BIT_SET(cutscene.iBSFadeOutStarted, cutscene.iCurrentScene)
			IF GET_GAME_TIMER() >= (cutscene.iCurrentSceneTimer + cutscene.sScenes[cutscene.iCurrentScene].iDuration - cutscene.sScenes[cutscene.iCurrentScene].iFadeOutTime)
				DO_SCREEN_FADE_OUT(cutscene.sScenes[cutscene.iCurrentScene].iFadeOutTime)
				SET_BIT(cutscene.iBSFadeOutStarted, cutscene.iCurrentScene)
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Did fade out for scene ", cutscene.iCurrentScene)
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	INT iDuration = cutscene.sScenes[cutscene.iCurrentScene].iDuration
	
	#IF IS_DEBUG_BUILD
		IF g_SimpleInteriorData.db_bPlayShortSimpleCutscenes
			iDuration = 1500
		ENDIF
	#ENDIF
	
	cutscene.iCurrentSceneElapsedTime = GET_GAME_TIMER() - cutscene.iCurrentSceneTimer
	cutscene.iTotalElapsedTime = GET_GAME_TIMER() - cutscene.iStartTime
	
	IF cutscene.iCurrentSceneElapsedTime >= iDuration
	#IF IS_DEBUG_BUILD
	OR IS_DEBUG_KEY_JUST_PRESSED(KEY_RIGHT, KEYBOARD_MODIFIER_NONE, "Skipping scene")
	#ENDIF
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Timer for current scene (", cutscene.iCurrentScene ,": ", cutscene.sScenes[cutscene.iCurrentScene].strName, ") expired, next scene or quit.")
		#ENDIF
		
		// Move to next scene or finish cutscene if it's the last
		IF cutscene.iCurrentScene = cutscene.iScenesCount - 1
			// End of cutscene
			SIMPLE_CUTSCENE_STOP(cutscene, IS_BITMASK_AS_ENUM_SET(cutscene.iSetupBM, SCS_DO_NOT_RETURN_PLAYER_CONTROL), bDontKillCameraOnStop, bCleanupMPCutscene, IS_BITMASK_AS_ENUM_SET(cutscene.iSetupBM, SCS_PREVENT_INVINCIBILITY_CHANGES_END))
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - No more scenes to play, finishing.")
			#ENDIF
		ELSE
			cutscene.iCurrentScene = cutscene.iCurrentScene + 1
			cutscene.iCurrentSceneElapsedTime = 0
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Next scene is: ", cutscene.iCurrentScene, " - ", cutscene.sScenes[cutscene.iCurrentScene].strName)
			#ENDIF
		ENDIF
	ENDIF
	
	IF cutscene.bPlaying
		#IF IS_DEBUG_BUILD
		// Don't bother showing help if we're playing short debug cutscenes
		IF NOT g_SimpleInteriorData.db_bPlayShortSimpleCutscenes
		#ENDIF
		IF (GET_GAME_TIMER() - cutscene.iCurrentHelpTimer) >= cutscene.sHelps[cutscene.iCurrentHelp].iStartTime
			IF NOT IS_BIT_SET(cutscene.iBSHelpsInit, cutscene.iCurrentHelp)
			
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - Gonna show help number ", cutscene.iCurrentHelp)
				#ENDIF
				
				SET_BIT(cutscene.iBSHelpsInit, cutscene.iCurrentHelp)
			
				IF NOT IS_STRING_NULL_OR_EMPTY(cutscene.sHelps[cutscene.iCurrentHelp].txtHelp)
					CLEAR_HELP(TRUE)
					PRINT_HELP(cutscene.sHelps[cutscene.iCurrentHelp].txtHelp, cutscene.sHelps[cutscene.iCurrentHelp].iDuration)
					
					IF cutscene.iCurrentHelp + 1 < cutscene.iHelpsCount
						cutscene.iCurrentHelp = cutscene.iCurrentHelp + 1
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_MAINTAIN - But it's empty!")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC SIMPLE_CUTSCENE_START(SIMPLE_CUTSCENE &cutscene, SIMPLE_CUTSCENE_SETUP eBitMask = SCS_DEFAULT)
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY][CUTSCENE] SIMPLE_CUTSCENE_START - Script ",GET_THIS_SCRIPT_NAME()," called this.")
	#ENDIF
	
	IF cutscene.iScenesCount > 0
		IF NOT cutscene.bPlaying
		AND NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
			SIMPLE_CUTSCENE_REINIT(cutscene)
			cutscene.iSetupBM = ENUM_TO_INT(eBitMask)
			#IF IS_DEBUG_BUILD
			SIMPLE_CUTSCENE_DEBUG_PRINT_SETUP(cutscene.iSetupBM)
			#ENDIF
			SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iCutsceneHash = GET_HASH_KEY(cutscene.strName)
			cutscene.bPlaying = TRUE
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_START - Started! Scenes count: ", cutscene.iScenesCount, ", total duration: ", cutscene.iTotalDuration)
				cutscene.dbg.bPlayedViaDebug = FALSE
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_START - Can't start as already playing a cutscene.")
			#ENDIF
		ENDIF
	ELSE
		SIMPLE_CUTSCENE_REINIT(cutscene)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_START - Can't start as no scenes to play!")
		#ENDIF
	ENDIF
ENDPROC

PROC SIMPLE_CUTSCENE_LIGHT_START(SIMPLE_CUTSCENE_LIGHT &cutscene)
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY][CUTSCENE] SIMPLE_CUTSCENE_START - Called this.")
	#ENDIF
	
	IF cutscene.iScenesCount > 0
		IF NOT cutscene.bPlaying
		AND NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
			SIMPLE_CUTSCENE_LIGHT_REINIT(cutscene)
			SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iBS, BS_SIMPLE_CUTSCENE_GLOBAL_PLAYER_BD_PLAYING)
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].simpleCutscene.iCutsceneHash = GET_HASH_KEY(cutscene.strName)
			cutscene.bPlaying = TRUE
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_START - Started! Scenes count: ", cutscene.iScenesCount, ", total duration: ", cutscene.iTotalDuration)
				cutscene.dbg.bPlayedViaDebug = FALSE
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_START - Can't start as already playing a cutscene.")
			#ENDIF
		ENDIF
	ELSE
		SIMPLE_CUTSCENE_LIGHT_REINIT(cutscene)
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_START - Can't start as no scenes to play!")
		#ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC SIMPLE_CUTSCENE_CREATE_DEBUG_WIDGETS(SIMPLE_CUTSCENE &cutscene)
	TEXT_LABEL_63 txtTitle = "SIMPLE CUTSCENE - "
	txtTitle += cutscene.strName
	
	START_WIDGET_GROUP(txtTitle)
		
		ADD_WIDGET_INT_SLIDER("Number of scenes", cutscene.iScenesCount, 0, SIMPLE_CUTSCENE_MAX_SCENES, 1)
		
		ADD_WIDGET_INT_SLIDER("Current scene", cutscene.dbg.iCurrentScene, 0, SIMPLE_CUTSCENE_MAX_SCENES - 1, 1)
		cutscene.dbg.nameWidget = ADD_TEXT_WIDGET("Scene name")
		ADD_WIDGET_INT_SLIDER("Scene duration", cutscene.dbg.iSceneDuration, 0, 100000, 100)
		ADD_WIDGET_FLOAT_SLIDER("Scene cam shake", cutscene.dbg.fShake, 0, 100, 0.01)
		ADD_WIDGET_INT_SLIDER("Scene fade in", cutscene.dbg.iFadeIn, 0, 100000, 100)
		ADD_WIDGET_INT_SLIDER("Scene fade out", cutscene.dbg.iFadeOut, 0, 100000, 100)
		ADD_WIDGET_BOOL("Trigger scene", cutscene.dbg.bPlayScene)
		ADD_WIDGET_BOOL("Dump scene", cutscene.dbg.bDumpScene)
		ADD_WIDGET_BOOL("Dump coords only", cutscene.dbg.bDumpSceneCoords)
		ADD_WIDGET_BOOL("Dump cutscene", cutscene.dbg.bDumpCutscene)
		ADD_WIDGET_BOOL("[!] Save scene coords", cutscene.dbg.bSaveScene)
		
		START_WIDGET_GROUP("Start coords")
			ADD_WIDGET_VECTOR_SLIDER("Start coords", cutscene.dbg.vCoords1, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Start rot", cutscene.dbg.vRot1, -360.0, 360.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Start FOV", cutscene.dbg.fFov1, 0.0, 180.0, 0.01)
			ADD_WIDGET_BOOL("Set coords from camera", cutscene.dbg.bSetCoords1)
			ADD_WIDGET_BOOL("Set debug cam to start coords", cutscene.dbg.bSetDebugCamCoords1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("End coords")
			ADD_WIDGET_VECTOR_SLIDER("End coords", cutscene.dbg.vCoords2, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("End rot", cutscene.dbg.vRot2, -360.0, 360.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("End FOV", cutscene.dbg.fFov2, 0.0, 180.0, 0.01)
			ADD_WIDGET_BOOL("Set coords from camera", cutscene.dbg.bSetCoords2)
			ADD_WIDGET_BOOL("Set debug cam to end coords", cutscene.dbg.bSetDebugCamCoords2)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("Trigger cutscene", cutscene.dbg.bPlayCutscene)
		ADD_WIDGET_BOOL("Cleanup cutscene cam", cutscene.dbg.bCleanupCutsceneCam)
		
		START_WIDGET_GROUP("DOF")
			ADD_WIDGET_BOOL("Current scene uses DOF", cutscene.dbg.bCurrentSceneUsesDOF)
			ADD_WIDGET_FLOAT_SLIDER("Strength", cutscene.dbg.fDOFStrength, 0.0, 100.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Near out", cutscene.dbg.fDOFNearOut, -100.0, 1000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Far out", cutscene.dbg.fDOFFarOut, -100.0, 1000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Near in", cutscene.dbg.fDOFNearIn, -100.0, 1000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Far in", cutscene.dbg.fDOFFarIn, -100.0, 1000.0, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Help")
			ADD_WIDGET_INT_SLIDER("Number of helps", cutscene.iHelpsCount, 0, SIMPLE_CUTSCENE_MAX_HELP, 1)
			ADD_WIDGET_INT_SLIDER("Current help", cutscene.dbg.iCurrentHelp, 0, SIMPLE_CUTSCENE_MAX_HELP - 1, 1)
			ADD_WIDGET_INT_SLIDER("Start time", cutscene.dbg.iHelpStart, 0, 1000000, 100)
			ADD_WIDGET_INT_SLIDER("Duration", cutscene.dbg.iHelpDuration, 0, 1000000, 100)
			cutscene.dbg.helpWidget = ADD_TEXT_WIDGET("Help label")
			ADD_WIDGET_BOOL("Save help", cutscene.dbg.bSaveHelp)
			ADD_WIDGET_BOOL("Dump help", cutscene.dbg.bDumpHelp)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("Dont update camera", cutscene.dbg.bDontUpdateCamera)
	STOP_WIDGET_GROUP()
ENDPROC

PROC _DUMP_SIMPLE_CUTSCENE_SCENE(SIMPLE_CUTSCENE &cutscene, INT iScene)
	IF iScene > -1 AND iScene < cutscene.iScenesCount
		_TRANSFORM_SCENE_COORDS_TO_LOCAL_COORDS(cutscene, iScene)
		
		OPEN_DEBUG_FILE()
		//SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("SIMPLE_CUTSCENE_ADD_SCENE(")
			SAVE_STRING_TO_DEBUG_FILE(cutscene.strName) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_INT_TO_DEBUG_FILE(cutscene.sScenes[iScene].iDuration) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_STRING_TO_DEBUG_FILE("\"") SAVE_STRING_TO_DEBUG_FILE(cutscene.sScenes[iScene].strName) SAVE_STRING_TO_DEBUG_FILE("\", ")
			SAVE_VECTOR_TO_DEBUG_FILE(cutscene.sScenes[iScene].vCamCoordsStart) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(cutscene.sScenes[iScene].vCamRotStart) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(cutscene.sScenes[iScene].fCamFovStart) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(cutscene.sScenes[iScene].vCamCoordsFinish) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(cutscene.sScenes[iScene].vCamRotFinish) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(cutscene.sScenes[iScene].fCamFovFinish) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(cutscene.sScenes[iScene].fCamShake) SAVE_STRING_TO_DEBUG_FILE(", ")
			
			SAVE_INT_TO_DEBUG_FILE(cutscene.sScenes[iScene].iFadeInTime) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_INT_TO_DEBUG_FILE(cutscene.sScenes[iScene].iFadeOutTime) SAVE_STRING_TO_DEBUG_FILE(")")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			IF cutscene.sScenes[iScene].bUseDOF
				// SIMPLE_CUTSCENE_ADD_DOF_TO_SCENE(SIMPLE_CUTSCENE &cutscene, INT iScene, FLOAT fDOFStrength, FLOAT fDOFNearOut, FLOAT fDOFFarOut, FLOAT fDOFNearIn, FLOAT fDOFFarIn)
				SAVE_STRING_TO_DEBUG_FILE("SIMPLE_CUTSCENE_ADD_DOF_TO_SCENE(")
				SAVE_STRING_TO_DEBUG_FILE(cutscene.strName) SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_INT_TO_DEBUG_FILE(iScene) SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(cutscene.sScenes[iScene].fDOFStrength) SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(cutscene.sScenes[iScene].fDOFNearOut) SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(cutscene.sScenes[iScene].fDOFFarOut) SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(cutscene.sScenes[iScene].fDOFNearIn) SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(cutscene.sScenes[iScene].fDOFFarIn) SAVE_STRING_TO_DEBUG_FILE(")")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		CLOSE_DEBUG_FILE()
		
		_TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS(cutscene, iScene)
	ENDIF
ENDPROC

PROC SIMPLE_CUTSCENE_MAINTAIN_DEBUG_WIDGETS(SIMPLE_CUTSCENE &cutscene)

	IF cutscene.dbg.iCurrentScene != cutscene.dbg.iPreviousCurrentScene
		cutscene.dbg.vCoords1 = cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamCoordsStart
		cutscene.dbg.vCoords2 = cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamCoordsFinish
		cutscene.dbg.vRot1 = cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamRotStart
		cutscene.dbg.vRot2 = cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamRotFinish
		cutscene.dbg.fFov1 = cutscene.sScenes[cutscene.dbg.iCurrentScene].fCamFovStart
		cutscene.dbg.fFov2 = cutscene.sScenes[cutscene.dbg.iCurrentScene].fCamFovFinish
		cutscene.dbg.fShake = cutscene.sScenes[cutscene.dbg.iCurrentScene].fCamShake
		cutscene.dbg.iFadeIn = cutscene.sScenes[cutscene.dbg.iCurrentScene].iFadeInTime
		cutscene.dbg.iFadeOut = cutscene.sScenes[cutscene.dbg.iCurrentScene].iFadeOutTime
		cutscene.dbg.iSceneDuration = cutscene.sScenes[cutscene.dbg.iCurrentScene].iDuration
		IF NOT IS_STRING_NULL_OR_EMPTY(cutscene.sScenes[cutscene.dbg.iCurrentScene].strName)
			IF DOES_TEXT_WIDGET_EXIST(cutscene.dbg.nameWidget)
				SET_CONTENTS_OF_TEXT_WIDGET(cutscene.dbg.nameWidget, cutscene.sScenes[cutscene.dbg.iCurrentScene].strName)
			ENDIF
		ELSE
			IF DOES_TEXT_WIDGET_EXIST(cutscene.dbg.nameWidget)
				SET_CONTENTS_OF_TEXT_WIDGET(cutscene.dbg.nameWidget, "")
			ENDIF
		ENDIF
		
		// dof
		cutscene.dbg.bCurrentSceneUsesDOF = cutscene.sScenes[cutscene.dbg.iCurrentScene].bUseDOF
		cutscene.dbg.fDOFStrength = cutscene.sScenes[cutscene.dbg.iCurrentScene].fDOFStrength
		cutscene.dbg.fDOFNearOut = cutscene.sScenes[cutscene.dbg.iCurrentScene].fDOFNearOut
		cutscene.dbg.fDOFFarOut = cutscene.sScenes[cutscene.dbg.iCurrentScene].fDOFFarOut
		cutscene.dbg.fDOFNearIn = cutscene.sScenes[cutscene.dbg.iCurrentScene].fDOFNearIn
		cutscene.dbg.fDOFFarIn = cutscene.sScenes[cutscene.dbg.iCurrentScene].fDOFFarIn
		
		cutscene.dbg.iPreviousCurrentScene = cutscene.dbg.iCurrentScene
		
		IF cutscene.bUseRelativePos
			cutscene.dbg.vCoords1 = _TRANSFORM_WORLD_COORDS_TO_SIMPLE_CUTSCENE_COORDS(cutscene.vBasePos, cutscene.fBaseHeading, cutscene.dbg.vCoords1)
			cutscene.dbg.vRot1.Z = _TRANSFORM_WORLD_HEADING_TO_SIMPLE_CUTSCENE_HEADING(cutscene.fBaseHeading, cutscene.dbg.vRot1.Z)
			cutscene.dbg.vCoords2 = _TRANSFORM_WORLD_COORDS_TO_SIMPLE_CUTSCENE_COORDS(cutscene.vBasePos, cutscene.fBaseHeading, cutscene.dbg.vCoords2)
			cutscene.dbg.vRot2.Z = _TRANSFORM_WORLD_HEADING_TO_SIMPLE_CUTSCENE_HEADING(cutscene.fBaseHeading, cutscene.dbg.vRot2.Z)
		ENDIF
	ENDIF

	IF cutscene.dbg.bSetCoords1
		cutscene.dbg.vCoords1 = GET_FINAL_RENDERED_CAM_COORD()
		cutscene.dbg.vRot1 = GET_FINAL_RENDERED_CAM_ROT()
		cutscene.dbg.fFov1 = GET_FINAL_RENDERED_CAM_FOV()
		
		IF cutscene.bUseRelativePos
			cutscene.dbg.vCoords1 = _TRANSFORM_WORLD_COORDS_TO_SIMPLE_CUTSCENE_COORDS(cutscene.vBasePos, cutscene.fBaseHeading, cutscene.dbg.vCoords1)
			cutscene.dbg.vRot1.Z = _TRANSFORM_WORLD_HEADING_TO_SIMPLE_CUTSCENE_HEADING(cutscene.fBaseHeading, cutscene.dbg.vRot1.Z)
		ENDIF
		cutscene.dbg.bSetCoords1 = FALSE
	ENDIF
	
	IF cutscene.dbg.bSetCoords2
		cutscene.dbg.vCoords2 = GET_FINAL_RENDERED_CAM_COORD()
		cutscene.dbg.vRot2 = GET_FINAL_RENDERED_CAM_ROT()
		cutscene.dbg.fFov2 = GET_FINAL_RENDERED_CAM_FOV()
		
		IF cutscene.bUseRelativePos
			cutscene.dbg.vCoords2 = _TRANSFORM_WORLD_COORDS_TO_SIMPLE_CUTSCENE_COORDS(cutscene.vBasePos, cutscene.fBaseHeading, cutscene.dbg.vCoords2)
			cutscene.dbg.vRot2.Z = _TRANSFORM_WORLD_HEADING_TO_SIMPLE_CUTSCENE_HEADING(cutscene.fBaseHeading, cutscene.dbg.vRot2.Z)
		ENDIF
		cutscene.dbg.bSetCoords2 = FALSE
	ENDIF
	
	// Saving dbg coords to actual coords
	IF cutscene.dbg.bSaveScene
		cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamCoordsStart = cutscene.dbg.vCoords1
		cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamRotStart = cutscene.dbg.vRot1
		cutscene.sScenes[cutscene.dbg.iCurrentScene].fCamFovStart = cutscene.dbg.fFov1
	
		cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamCoordsFinish = cutscene.dbg.vCoords2
		cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamRotFinish = cutscene.dbg.vRot2
		cutscene.sScenes[cutscene.dbg.iCurrentScene].fCamFovFinish = cutscene.dbg.fFov2
		
		cutscene.sScenes[cutscene.dbg.iCurrentScene].fCamShake = cutscene.dbg.fShake
	
		cutscene.sScenes[cutscene.dbg.iCurrentScene].iFadeInTime = cutscene.dbg.iFadeIn
		cutscene.sScenes[cutscene.dbg.iCurrentScene].iFadeOutTime = cutscene.dbg.iFadeOut
		
		cutscene.sScenes[cutscene.dbg.iCurrentScene].iDuration = cutscene.dbg.iSceneDuration
		cutscene.sScenes[cutscene.dbg.iCurrentScene].strName = GET_CONTENTS_OF_TEXT_WIDGET(cutscene.dbg.nameWidget)
		
		// Transform coords if they are within simple interior
		_TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS(cutscene, cutscene.dbg.iCurrentScene)
		
		// Save dof settings
		IF cutscene.dbg.bCurrentSceneUsesDOF
			cutscene.sScenes[cutscene.dbg.iCurrentScene].bUseDOF = TRUE
			cutscene.sScenes[cutscene.dbg.iCurrentScene].fDOFStrength = cutscene.dbg.fDOFStrength
			cutscene.sScenes[cutscene.dbg.iCurrentScene].fDOFNearOut = cutscene.dbg.fDOFNearOut
			cutscene.sScenes[cutscene.dbg.iCurrentScene].fDOFFarOut = cutscene.dbg.fDOFFarOut
			cutscene.sScenes[cutscene.dbg.iCurrentScene].fDOFNearIn = cutscene.dbg.fDOFNearIn
			cutscene.sScenes[cutscene.dbg.iCurrentScene].fDOFFarIn = cutscene.dbg.fDOFFarIn
		ELSE
			cutscene.sScenes[cutscene.iCurrentScene].bUseDOF = FALSE
		ENDIF
		
		cutscene.dbg.bSaveScene = FALSE
	ENDIF
	
	IF cutscene.dbg.bPlayCutscene
		SIMPLE_CUTSCENE_START(cutscene)
		cutscene.dbg.bPlayedViaDebug = TRUE
		cutscene.dbg.bPlayCutscene = FALSE
	ENDIF
	
	// Playing a single scene
	IF cutscene.dbg.bPlayScene
		IF NOT HAS_NET_TIMER_STARTED(cutscene.dbg.timerCutscene)
			IF NOT DOES_CAM_EXIST(cutscene.dbg.cutsceneCamera)
				cutscene.dbg.cutsceneCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			START_MP_CUTSCENE(TRUE)
			
			VECTOR vCoords1 = cutscene.dbg.vCoords1
			VECTOR vRot1 = cutscene.dbg.vRot1
			VECTOR vCoords2 = cutscene.dbg.vCoords2
			VECTOR vRot2 = cutscene.dbg.vRot2
			
			vCoords1 = _TRANSFORM_SIMPLE_CUTSCENE_COORDS_TO_WORLD_COORDS(cutscene.vBasePos, cutscene.fBaseHeading, vCoords1)
			vCoords2 = _TRANSFORM_SIMPLE_CUTSCENE_COORDS_TO_WORLD_COORDS(cutscene.vBasePos, cutscene.fBaseHeading, vCoords2)
			vRot1.Z = _TRANSFORM_SIMPLE_CUTSCENE_HEADING_TO_WORLD_HEADING(cutscene.fBaseHeading, vRot1.Z)
			vRot2.Z = _TRANSFORM_SIMPLE_CUTSCENE_HEADING_TO_WORLD_HEADING(cutscene.fBaseHeading, vRot2.Z)
			
			IF cutscene.dbg.fShake > 0.0
				SHAKE_CAM(cutscene.dbg.cutsceneCamera, "Hand_shake", cutscene.dbg.fShake)
			ELSE
				SHAKE_CAM(cutscene.dbg.cutsceneCamera, "Hand_shake", 0.0)
				STOP_CAM_SHAKING(cutscene.dbg.cutsceneCamera, TRUE)
			ENDIF
			
			SET_CAM_PARAMS(cutscene.dbg.cutsceneCamera, vCoords1, vRot1, cutscene.dbg.fFov1)
			SET_CAM_PARAMS(cutscene.dbg.cutsceneCamera, vCoords2, vRot2, cutscene.dbg.fFov2, cutscene.dbg.iSceneDuration, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			
			IF cutscene.dbg.iFadeIn > 0
				DO_SCREEN_FADE_OUT(0)
				DO_SCREEN_FADE_IN(cutscene.dbg.iFadeIn)
			ENDIF
			
			cutscene.dbg.bFadeOutDone = FALSE
			
			START_NET_TIMER(cutscene.dbg.timerCutscene)
		ENDIF
	ENDIF
	
	/*
	CAMERA_INDEX currentCam = NULL
	IF DOES_CAM_EXIST(cutscene.dbg.cutsceneCamera)
		currentCam = cutscene.dbg.cutsceneCamera
	ELIF DOES_CAM_EXIST(cutscene.camera)
		currentCam = cutscene.camera
	ENDIF
	*/
	
	IF DOES_CAM_EXIST(cutscene.dbg.cutsceneCamera)
		IF cutscene.dbg.bCurrentSceneUsesDOF
			SET_USE_HI_DOF()
			SET_CAM_USE_SHALLOW_DOF_MODE(cutscene.dbg.cutsceneCamera, TRUE)
			SET_CAM_DOF_PLANES(cutscene.dbg.cutsceneCamera, cutscene.dbg.fDOFNearOut, cutscene.dbg.fDOFNearIn, cutscene.dbg.fDOFFarIn, cutscene.dbg.fDOFFarOut)
			SET_CAM_DOF_STRENGTH(cutscene.dbg.cutsceneCamera, cutscene.dbg.fDOFStrength)
		ELSE
			SET_CAM_USE_SHALLOW_DOF_MODE(cutscene.dbg.cutsceneCamera, FALSE)
			SET_CAM_DOF_STRENGTH(cutscene.dbg.cutsceneCamera, 0.0)
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(cutscene.dbg.timerCutscene)
		IF cutscene.dbg.iFadeOut > 0
		AND NOT cutscene.dbg.bFadeOutDone
			IF HAS_NET_TIMER_EXPIRED(cutscene.dbg.timerCutscene, cutscene.dbg.iSceneDuration - cutscene.dbg.iFadeOut)
				DO_SCREEN_FADE_OUT(cutscene.dbg.iFadeOut)
				cutscene.dbg.bFadeOutDone = TRUE
			ENDIF
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(cutscene.dbg.timerCutscene, cutscene.dbg.iSceneDuration)
			CLEANUP_MP_CUTSCENE()
			IF DOES_CAM_EXIST(cutscene.dbg.cutsceneCamera)
				DESTROY_CAM(cutscene.dbg.cutsceneCamera)
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DO_SCREEN_FADE_IN(0)
			
			RESET_NET_TIMER(cutscene.dbg.timerCutscene)
			
			cutscene.dbg.bPlayScene = FALSE
		ENDIF
	ENDIF
	
	IF cutscene.dbg.bCleanupCutsceneCam
		IF DOES_CAM_EXIST(cutscene.dbg.cutsceneCamera)
			DESTROY_CAM(cutscene.dbg.cutsceneCamera)
		ENDIF
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		cutscene.dbg.bCleanupCutsceneCam = FALSE
	ENDIF
	
	IF cutscene.dbg.bSetDebugCamCoords1
		VECTOR vTempCoords = cutscene.dbg.vCoords1
		VECTOR vTempRot = cutscene.dbg.vRot1
		
		IF cutscene.bUseRelativePos
			vTempCoords = _TRANSFORM_SIMPLE_CUTSCENE_COORDS_TO_WORLD_COORDS(cutscene.vBasePos, cutscene.fBaseHeading, vTempCoords)
			vTempRot.Z = _TRANSFORM_SIMPLE_CUTSCENE_HEADING_TO_WORLD_HEADING(cutscene.fBaseHeading, vTempRot.Z)
		ENDIF
		
		IF NOT DOES_CAM_EXIST(cutscene.dbg.cutsceneCamera)
			cutscene.dbg.cutsceneCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		ENDIF
		SET_CAM_PARAMS(cutscene.dbg.cutsceneCamera, vTempCoords, vTempRot, cutscene.dbg.fFov1)
		cutscene.dbg.bSetDebugCamCoords1 = FALSE
	ENDIF
	
	IF cutscene.dbg.bSetDebugCamCoords2
		VECTOR vTempCoords = cutscene.dbg.vCoords2
		VECTOR vTempRot = cutscene.dbg.vRot2
		
		IF cutscene.bUseRelativePos
			vTempCoords = _TRANSFORM_SIMPLE_CUTSCENE_COORDS_TO_WORLD_COORDS(cutscene.vBasePos, cutscene.fBaseHeading, vTempCoords)
			vTempRot.Z = _TRANSFORM_SIMPLE_CUTSCENE_HEADING_TO_WORLD_HEADING(cutscene.fBaseHeading, vTempRot.Z)
		ENDIF
		
		IF NOT DOES_CAM_EXIST(cutscene.dbg.cutsceneCamera)
			cutscene.dbg.cutsceneCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		ENDIF
		SET_CAM_PARAMS(cutscene.dbg.cutsceneCamera, vTempCoords, vTempRot, cutscene.dbg.fFov2)
		cutscene.dbg.bSetDebugCamCoords2 = FALSE
	ENDIF
	
	IF cutscene.dbg.bDumpScene
		_DUMP_SIMPLE_CUTSCENE_SCENE(cutscene, cutscene.dbg.iCurrentScene)
		cutscene.dbg.bDumpScene = FALSE
	ENDIF
	
	IF cutscene.dbg.bDumpCutscene
		INT i
		REPEAT cutscene.iScenesCount i
			_DUMP_SIMPLE_CUTSCENE_SCENE(cutscene, i)
		ENDREPEAT
		cutscene.dbg.bDumpCutscene = FALSE
	ENDIF
	
	IF cutscene.dbg.bDumpSceneCoords
		_TRANSFORM_SCENE_COORDS_TO_LOCAL_COORDS(cutscene, cutscene.dbg.iCurrentScene)
		
		OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("VECTOR vCamCoordsStart = ") SAVE_VECTOR_TO_DEBUG_FILE(cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamCoordsStart) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("VECTOR vCamRotStart = ") SAVE_VECTOR_TO_DEBUG_FILE(cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamRotStart) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("FLOAT fCamFovStart = ") SAVE_FLOAT_TO_DEBUG_FILE(cutscene.sScenes[cutscene.dbg.iCurrentScene].fCamFovStart) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("VECTOR vCamCoordsFinish = ") SAVE_VECTOR_TO_DEBUG_FILE(cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamCoordsFinish) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("VECTOR vCamRotFinish = ") SAVE_VECTOR_TO_DEBUG_FILE(cutscene.sScenes[cutscene.dbg.iCurrentScene].vCamRotFinish) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("FLOAT fCamFovFinish = ") SAVE_FLOAT_TO_DEBUG_FILE(cutscene.sScenes[cutscene.dbg.iCurrentScene].fCamFovFinish) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("FLOAT fCamShake = ") SAVE_FLOAT_TO_DEBUG_FILE(cutscene.sScenes[cutscene.dbg.iCurrentScene].fCamShake) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
		
		_TRANSFORM_SCENE_COORDS_TO_WORLD_COORDS(cutscene, cutscene.dbg.iCurrentScene)
		
		cutscene.dbg.bDumpSceneCoords = FALSE
	ENDIF
	
	// HELP EDITOR
	
	IF cutscene.dbg.iPreviousCurrentHelp != cutscene.dbg.iCurrentHelp
		cutscene.dbg.iHelpStart = cutscene.sHelps[cutscene.dbg.iCurrentHelp].iStartTime
		cutscene.dbg.iHelpDuration = cutscene.sHelps[cutscene.dbg.iCurrentHelp].iDuration
		IF NOT IS_STRING_NULL_OR_EMPTY(cutscene.sHelps[cutscene.dbg.iCurrentHelp].txtHelp)
		AND DOES_TEXT_WIDGET_EXIST(cutscene.dbg.helpWidget)
			SET_CONTENTS_OF_TEXT_WIDGET(cutscene.dbg.helpWidget, cutscene.sHelps[cutscene.dbg.iCurrentHelp].txtHelp)
		ELIF DOES_TEXT_WIDGET_EXIST(cutscene.dbg.helpWidget)
			SET_CONTENTS_OF_TEXT_WIDGET(cutscene.dbg.helpWidget, "")
		ENDIF
		cutscene.dbg.iPreviousCurrentHelp = cutscene.dbg.iCurrentHelp
	ENDIF
	
	IF cutscene.dbg.bSaveHelp
		cutscene.sHelps[cutscene.dbg.iCurrentHelp].iStartTime = cutscene.dbg.iHelpStart
		cutscene.sHelps[cutscene.dbg.iCurrentHelp].iDuration = cutscene.dbg.iHelpDuration
		cutscene.sHelps[cutscene.dbg.iCurrentHelp].txtHelp = GET_CONTENTS_OF_TEXT_WIDGET(cutscene.dbg.helpWidget)
		cutscene.dbg.bSaveHelp = FALSE
	ENDIF
	
	IF cutscene.dbg.bDumpHelp
		OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("SIMPLE_CUTSCENE_ADD_HELP(")
			SAVE_STRING_TO_DEBUG_FILE(cutscene.strName) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_INT_TO_DEBUG_FILE(cutscene.sHelps[cutscene.dbg.iCurrentHelp].iStartTime) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_INT_TO_DEBUG_FILE(cutscene.sHelps[cutscene.dbg.iCurrentHelp].iDuration) SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_STRING_TO_DEBUG_FILE("\"") SAVE_STRING_TO_DEBUG_FILE(cutscene.sHelps[cutscene.dbg.iCurrentHelp].txtHelp) SAVE_STRING_TO_DEBUG_FILE("\")")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
		cutscene.dbg.bDumpHelp = FALSE
	ENDIF
	
ENDPROC
#ENDIF

