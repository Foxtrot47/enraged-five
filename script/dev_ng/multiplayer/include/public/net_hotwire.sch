USING "timer_public.sch"
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "net_include.sch"
USING "freemode_header.sch"
USING "net_mp_tv.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "timer_public.sch"

USING "net_gangops_hacking_common.sch"

CONST_INT ciHOTWIRE_TIME 240000
CONST_INT ciHOTWIRE_END_DELAY 3000

CONST_INT MAX_HW_OUTER_WALLS 4
CONST_INT MAX_HW_INNER_WALLS 2
CONST_INT MAX_HW_NODES 4

CONST_INT MAX_HW_INNER_WALL_POINTS 4
CONST_INT MAX_HW_NODE_POINTS 4

CONST_INT MAX_LIVES_PER_NODE	4

TWEAK_FLOAT cfNODESCALEX 0.164
TWEAK_FLOAT cfNODESCALEY 0.056

TWEAK_FLOAT cfNODESCALE_ROLL_X 0.170
TWEAK_FLOAT cfNODESCALE_ROLL_Y 0.062

TWEAK_FLOAT cfBOXSCALEX 0.344
TWEAK_FLOAT cfBOXSCALEY 0.127

TWEAK_FLOAT cfHWLIGHTSCALE 0.018
TWEAK_FLOAT cfHWLIGHTOFFSETX 0.851
TWEAK_FLOAT cfHWLIFESCALE 0.02

CONST_FLOAT cfWireHeight 0.004

CONST_INT ciLeftNodeForTutorial		 1
CONST_FLOAT cfHighlightSize			 1.01

CONST_FLOAT cfWireOffset		0.076
//CONST_FLOAT cfNodeOffset		0.076

SCALEFORM_INDEX sfHotwireHacking
STRING sHotwireSoundSet = "DLC_XM17_IAA_SF_Hack"

INT sBackgroundLoopSndID = -1
FLOAT fDmgForBackgroundLoop = 0.0

#IF IS_DEBUG_BUILD
VECTOR vTestPos_HW
VECTOR vTestVec_HW
#ENDIF

STRUCT HOTWIRE_LEVEL_STRUCT
	VECTOR_2D vOuterWalls[MAX_HW_OUTER_WALLS]
	VECTOR_2D vInnerWalls[MAX_HW_INNER_WALLS][MAX_HW_INNER_WALL_POINTS]
	VECTOR_2D vStartNodes[MAX_HW_NODES][MAX_HW_NODE_POINTS]
	VECTOR_2D vEndNodes[MAX_HW_NODES][MAX_HW_NODE_POINTS]
ENDSTRUCT
HOTWIRE_LEVEL_STRUCT sHotwireLevel

STRUCT HOTWIRE_GAMEPLAY_DATA
	SCRIPT_TIMER tdHackTime
	SCRIPT_TIMER tdEndDelay
	INT iNodeHealth[MAX_HW_NODES] //For node damage
	INT iSelectedStartNode
	INT iNodePairs[MAX_HW_NODES]
	INT iConnections[MAX_HW_NODES]
	
	INT iHotwireBS
	
	INT iCorrectNodes
	INT iEndDelay = 350
	
	FLOAT fDownloadWindowScale = 0.0
	FLOAT fDownloadAmount = 0.0
	
	FLOAT fSuccessWindowScale = 0.0
	FLOAT fSuccessWindowAlpha = 0.0
	
	SCRIPT_TIMER tdEndTutDelay
ENDSTRUCT

CONST_INT ciHotwire_Failed	0
CONST_INT ciHotwire_HasPlayedHighlightSound_LeftNode_0	1
CONST_INT ciHotwire_HasPlayedHighlightSound_LeftNode_1	2
CONST_INT ciHotwire_HasPlayedHighlightSound_LeftNode_2	3
CONST_INT ciHotwire_HasPlayedHighlightSound_LeftNode_3	4

CONST_INT ciHotwire_HasPlayedHighlightSound_RightNode_0	5
CONST_INT ciHotwire_HasPlayedHighlightSound_RightNode_1	6
CONST_INT ciHotwire_HasPlayedHighlightSound_RightNode_2	7
CONST_INT ciHotwire_HasPlayedHighlightSound_RightNode_3	8

ENUM HOTWIRE_PASS_STATES
	HPS_OPENING_DOWNLOAD,
	HPS_DOWNLOADING,
	HPS_OPENING_SUCCESS,
	HPS_SUCCESS,
	HPS_DONE
ENDENUM
HOTWIRE_PASS_STATES hpsCurPassState

ENUM HOTWIRE_TUTORIAL_STATE
	HTS_NONE,
	HTS_SELECT_NODE,
	HTS_MOVE_WIRE,
	HTS_SELECT_RIGHT_NODE,
	HTS_SET_ALL,
	HTS_TEST,
	HTS_FINAL_HELP_TEXT,
	HTS_LIVES
ENDENUM
HOTWIRE_TUTORIAL_STATE htsCurTutState

HOTWIRE_GAMEPLAY_DATA sHotwireData

ENUM HOTWIRE_STATE
	HOTWIRE_INIT = 0,
	HOTWIRE_PLAY,
	HOTWIRE_PASS,
	HOTWIRE_FAIL,
	HOTWIRE_CLEANUP
ENDENUM
HOTWIRE_STATE eHotwireState

SCRIPT_TIMER tdHotwireEndDelay
VECTOR_2D vHotwireCursor

#IF IS_DEBUG_BUILD
BOOL bDebugCreatedHotwireWidgets
#ENDIF

#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID widgetHotwireMinigame
#ENDIF

RGBA_COLOUR_STRUCT rgbaHWBackground
RGBA_COLOUR_STRUCT rgbaHWOuterWalls
RGBA_COLOUR_STRUCT rgbaHWInnerWalls
RGBA_COLOUR_STRUCT rgbaHWNodes
RGBA_COLOUR_STRUCT rgbaHWWires[MAX_HW_NODES]

PROC SHUFFLE_NODE_CONNECTIONS()
	
	SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
	INT i
	FOR i = 0 TO MAX_HW_NODES - 1
		sHotwireData.iConnections[i] = i
	ENDFOR
	FOR i = 0 TO MAX_HW_NODES - 1
		INT iRoll = i + GET_RANDOM_INT_IN_RANGE(0, MAX_HW_NODES - i)
		INT iTemp = sHotwireData.iConnections[iRoll]
		sHotwireData.iConnections[iRoll] = sHotwireData.iConnections[i]
		sHotwireData.iConnections[i] = iTemp
		PRINTLN("[JS][Hotwire] i = ", i ," iCOnnection[i] = ", sHotwireData.iConnections[i])
	ENDFOR
ENDPROC

FUNC BOOL IS_HOTWIRE_TUTORIAL()
	RETURN htsCurTutState != HTS_NONE
ENDFUNC

PROC HOTWIRE_PLAY_FAIL_SOUND()
	PLAY_SOUND_FRONTEND(-1, "Hack_Fail", sHotwireSoundSet)
	PLAY_SOUND_FROM_ENTITY(-1, "Hack_Fail_Remote", PLAYER_PED_ID(), sHotwireSoundSet, TRUE, 30)
	fDmgForBackgroundLoop = 0.0
ENDPROC

PROC CHANGE_HOTWIRE_TUTORIAL_STATE(HOTWIRE_TUTORIAL_STATE htsNewState)
	PRINTLN("[HWTut] Setting tutorial state to ", htsNewState)
	htsCurTutState = htsNewState
ENDPROC

PROC INIT_HOTWIRE_LEVEL()
	vHotwireCursor.x = cfSCREEN_CENTER
	vHotwireCursor.y = cfSCREEN_CENTER
	
	FLOAT fSeparation = 0.071
	
	//Outer Walls
	sHotwireLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
	sHotwireLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
	sHotwireLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
	sHotwireLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

	//Inner Walls
	//Left Column
	sHotwireLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(0.100000, 0.845000)
	sHotwireLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(0.100000, 0.170000)
	sHotwireLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.200000, 0.170000)
	sHotwireLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.200000, 0.845000)

	//Right Column
	sHotwireLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.800000, 0.845000)
	sHotwireLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.800000, 0.170000)
	sHotwireLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.900000, 0.170000)
	sHotwireLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.900000, 0.845000)

	//Left Nodes
	sHotwireLevel.vStartNodes[0][0] = INIT_VECTOR_2D(0.100000 - fSeparation, 0.320000)
	sHotwireLevel.vStartNodes[0][1] = INIT_VECTOR_2D(0.100000 - fSeparation, 0.245000)
	sHotwireLevel.vStartNodes[0][2] = INIT_VECTOR_2D(0.200000 - fSeparation, 0.245000)
	sHotwireLevel.vStartNodes[0][3] = INIT_VECTOR_2D(0.200000 - fSeparation, 0.320000)

	sHotwireLevel.vStartNodes[1][0] = INIT_VECTOR_2D(0.100000 - fSeparation, 0.470000)
	sHotwireLevel.vStartNodes[1][1] = INIT_VECTOR_2D(0.100000 - fSeparation, 0.395000)
	sHotwireLevel.vStartNodes[1][2] = INIT_VECTOR_2D(0.200000 - fSeparation, 0.395000)
	sHotwireLevel.vStartNodes[1][3] = INIT_VECTOR_2D(0.200000 - fSeparation, 0.470000)

	sHotwireLevel.vStartNodes[2][0] = INIT_VECTOR_2D(0.100000 - fSeparation, 0.620000)
	sHotwireLevel.vStartNodes[2][1] = INIT_VECTOR_2D(0.100000 - fSeparation, 0.545000)
	sHotwireLevel.vStartNodes[2][2] = INIT_VECTOR_2D(0.200000 - fSeparation, 0.545000)
	sHotwireLevel.vStartNodes[2][3] = INIT_VECTOR_2D(0.200000 - fSeparation, 0.620000)

	sHotwireLevel.vStartNodes[3][0] = INIT_VECTOR_2D(0.100000 - fSeparation, 0.770000)
	sHotwireLevel.vStartNodes[3][1] = INIT_VECTOR_2D(0.100000 - fSeparation, 0.695000)
	sHotwireLevel.vStartNodes[3][2] = INIT_VECTOR_2D(0.200000 - fSeparation, 0.695000)
	sHotwireLevel.vStartNodes[3][3] = INIT_VECTOR_2D(0.200000 - fSeparation, 0.770000)

	//Right Nodes
	sHotwireLevel.vEndNodes[0][0] = INIT_VECTOR_2D(0.800000 + fSeparation, 0.320000)
	sHotwireLevel.vEndNodes[0][1] = INIT_VECTOR_2D(0.800000 + fSeparation, 0.245000)
	sHotwireLevel.vEndNodes[0][2] = INIT_VECTOR_2D(0.900000 + fSeparation, 0.245000)
	sHotwireLevel.vEndNodes[0][3] = INIT_VECTOR_2D(0.900000 + fSeparation, 0.320000)

	sHotwireLevel.vEndNodes[1][0] = INIT_VECTOR_2D(0.800000 + fSeparation, 0.470000)
	sHotwireLevel.vEndNodes[1][1] = INIT_VECTOR_2D(0.800000 + fSeparation, 0.395000)
	sHotwireLevel.vEndNodes[1][2] = INIT_VECTOR_2D(0.900000 + fSeparation, 0.395000)
	sHotwireLevel.vEndNodes[1][3] = INIT_VECTOR_2D(0.900000 + fSeparation, 0.470000)

	sHotwireLevel.vEndNodes[2][0] = INIT_VECTOR_2D(0.800000 + fSeparation, 0.620000)
	sHotwireLevel.vEndNodes[2][1] = INIT_VECTOR_2D(0.800000 + fSeparation, 0.545000)
	sHotwireLevel.vEndNodes[2][2] = INIT_VECTOR_2D(0.900000 + fSeparation, 0.545000)
	sHotwireLevel.vEndNodes[2][3] = INIT_VECTOR_2D(0.900000 + fSeparation, 0.620000)

	sHotwireLevel.vEndNodes[3][0] = INIT_VECTOR_2D(0.800000 + fSeparation, 0.770000)
	sHotwireLevel.vEndNodes[3][1] = INIT_VECTOR_2D(0.800000 + fSeparation, 0.695000)
	sHotwireLevel.vEndNodes[3][2] = INIT_VECTOR_2D(0.900000 + fSeparation, 0.695000)
	sHotwireLevel.vEndNodes[3][3] = INIT_VECTOR_2D(0.900000 + fSeparation, 0.770000)
ENDPROC

PROC INIT_NODE_DATA()
	HOTWIRE_GAMEPLAY_DATA sEmpty
	sHotwireData = sEmpty
	INT i
	FOR i = 0 TO MAX_HW_NODES - 1
		sHotwireData.iNodeHealth[i] = MAX_LIVES_PER_NODE //Replace with number of tries per node
		sHotwireData.iNodePairs[i] = -1
		CLEAR_BIT(sHotwireData.iCorrectNodes, i)
	ENDFOR
	sHotwireData.iSelectedStartNode = -1
ENDPROC

PROC DRAW_HOTWIRE_PLAY_AREA()
	DRAW_RECT_FROM_CORNER(0.0, 0.0, 1.0, 1.0, 0, 0, 0, 255)
	DRAW_SPRITE("MPHotwire", "bg", 0.5, 0.5, 1.0 * fAspectRatioMod, 1.0, 0.0, rgbaHWBackground.iR, rgbaHWBackground.iG, rgbaHWBackground.iB, rgbaHWBackground.iA)
ENDPROC

PROC GET_HOTWIRE_WIRE_DRAW_INFO(INT iStartNode, INT iEndNode, FLOAT &fXPos, FLOAT &fYPos, FLOAT &fXScale, FLOAT &fYScale, FLOAT &fRot, TEXT_LABEL_31 &tlSprite, FLOAT &fXPos2, FLOAT &fYPos2, FLOAT &fRot2, BOOL &bTwoSprites)
	SWITCH iStartNode
		CASE 0
			SWITCH iEndNode
				CASE 0
					fXPos = 0.500
					fYPos = 0.290
					fXScale = 0.700
					fYScale = 0.004
					fRot = 0
					tlSprite = "line"
					bTwoSprites = FALSE
				BREAK
				CASE 1
					fXPos = 0.402 - cfWireOffset
					fYPos = 0.372
					fXScale = 0.350
					fYScale = 0.170
					fRot = 0
					tlSprite = "line1"	
					fXPos2 = 0.599 + cfWireOffset
					fYPos2 = 0.370
					fRot2 = 180.0
					bTwoSprites = TRUE			
				BREAK
				CASE 2
					fXPos = 0.5
					fYPos = 0.360
					fXScale = 0.700
					fYScale = 0.170
					fRot = 0
					tlSprite = "line2"	
					fXPos2 = 0.500
					fYPos2 = 0.528
					fRot2 = 180.0
					bTwoSprites = TRUE			
				BREAK
				CASE 3
					fXPos = 0.500
					fYPos = 0.392
					fXScale = 0.700
					fYScale = 0.250
					fRot = 0
					tlSprite = "line3"
					fXPos2 = 0.500
					fYPos2 = 0.640
					fRot2 = 180.000
					bTwoSprites = TRUE		
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iEndNode
				CASE 0
					fXPos = 0.402 - cfWireOffset
					fYPos = 0.369
					fXScale = -0.350
					fYScale = 0.170
					fRot = 180.000
					tlSprite = "line1"
					fXPos2 = 0.599 + cfWireOffset
					fYPos2 = 0.371
					fRot2 = 0
					bTwoSprites = TRUE	
				BREAK
				CASE 1
					fXPos = 0.500
					fYPos = 0.440
					fXScale = 0.700
					fYScale = 0.004
					fRot = 0
					tlSprite = "line"				
					bTwoSprites = FALSE
				BREAK
				CASE 2
					fXPos = 0.402 - cfWireOffset
					fYPos = 0.522
					fXScale = 0.350
					fYScale = 0.170
					fRot = 0
					tlSprite = "line1"	
					fXPos2 = 0.599 + cfWireOffset
					fYPos2 = 0.520
					fRot2 = 180.0
					bTwoSprites = TRUE				
				BREAK
				CASE 3
					fXPos = 0.5
					fYPos = 0.510
					fXScale = 0.700
					fYScale = 0.170
					fRot = 0
					tlSprite = "line2"	
					fXPos2 = 0.500
					fYPos2 = 0.678
					fRot2 = 180.0
					bTwoSprites = TRUE			
				BREAK
			ENDSWITCH		
		BREAK
		CASE 2
			SWITCH iEndNode
				CASE 0
					fXPos = 0.500
					fYPos = 0.360
					fXScale = -0.700
					fYScale = 0.170
					fRot = 0
					tlSprite = "line2"
					fXPos2 = 0.500
					fYPos2 = 0.528
					fRot2 = 180.000
					bTwoSprites = TRUE
				BREAK
				CASE 1
					fXPos = 0.402 - cfWireOffset
					fYPos = 0.519
					fXScale = -0.350
					fYScale = 0.170
					fRot = 180.000
					tlSprite = "line1"
					fXPos2 = 0.599 + cfWireOffset
					fYPos2 = 0.521
					fRot2 = 0
					bTwoSprites = TRUE			
				BREAK
				CASE 2
					fXPos = 0.500
					fYPos = 0.590
					fXScale = 0.700
					fYScale = 0.004
					fRot = 0
					tlSprite = "line"				
					bTwoSprites = FALSE
				BREAK
				CASE 3
					fXPos = 0.402 - cfWireOffset
					fYPos = 0.672
					fXScale = 0.350
					fYScale = 0.170
					fRot = 0
					tlSprite = "line1"	
					fXPos2 = 0.599 + cfWireOffset
					fYPos2 = 0.670
					fRot2 = 180.0
					bTwoSprites = TRUE				
				BREAK
			ENDSWITCH		
		BREAK
		CASE 3
			SWITCH iEndNode
				CASE 0
					fXPos = 0.500
					fYPos = 0.392
					fXScale = -0.700
					fYScale = 0.250
					fRot = 0
					tlSprite = "line3"
					fXPos2 = 0.500
					fYPos2 = 0.640
					fRot2 = 180.000
					bTwoSprites = TRUE
				BREAK
				CASE 1
					fXPos = 0.500
					fYPos = 0.510
					fXScale = -0.700
					fYScale = 0.170
					fRot = 0
					tlSprite = "line2"	
					fXPos2 = 0.500
					fYPos2 = 0.678
					fRot2 = 180.0
					bTwoSprites = TRUE
				BREAK
				CASE 2
					fXPos = 0.402 - cfWireOffset
					fYPos = 0.669
					fXScale = -0.350
					fYScale = 0.170
					fRot = 180.000
					tlSprite = "line1"
					fXPos2 = 0.599 + cfWireOffset
					fYPos2 = 0.671
					fRot2 = 0.0
					bTwoSprites = TRUE
				BREAK
				CASE 3
					fXPos = 0.500
					fYPos = 0.740
					fXScale = 0.700
					fYScale = 0.004
					fRot = 0
					tlSprite = "line"				
					bTwoSprites = FALSE
				BREAK
			ENDSWITCH		
		BREAK
	ENDSWITCH
ENDPROC

FUNC VECTOR_2D GET_MIDPOINT_FROM_START_NODE(INT iNode)
	RETURN DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sHotwireLevel.vStartNodes[iNode][0], sHotwireLevel.vStartNodes[iNode][2]), 2.0)
ENDFUNC

FUNC VECTOR_2D GET_MIDPOINT_FROM_END_NODE(INT iNode)
	RETURN DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sHotwireLevel.vEndNodes[iNode][0], sHotwireLevel.vEndNodes[iNode][2]), 2.0)
ENDFUNC

FUNC INT GET_START_NODE_FROM_CURSOR()
	IF sHotwireData.iSelectedStartNode != -1
		RETURN sHotwireData.iSelectedStartNode
	ENDIF
	INT i
	FOR i = 0 TO MAX_HW_NODES - 1
		IF sHotwireData.iNodePairs[i] = -1
			IF VECTOR_2D_DIST2(GET_MIDPOINT_FROM_START_NODE(i), vHotwireCursor) < POW((sHotwireLevel.vStartNodes[i][3].x - sHotwireLevel.vStartNodes[i][0].x), 2.0)
				RETURN i			
			ENDIF
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

FUNC INT GET_END_NODE_FROM_CURSOR()
	INT i
	FOR i = 0 TO MAX_HW_NODES - 1
		IF VECTOR_2D_DIST2(GET_MIDPOINT_FROM_END_NODE(i), vHotwireCursor) < POW((sHotwireLevel.vEndNodes[i][3].x - sHotwireLevel.vEndNodes[i][0].x), 2.0)
			RETURN i			
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

PROC DRAW_PLACED_WIRE(INT iStart, INT iEnd)
	FLOAT fXPos, fYPos, fXScale, fYScale, fRot, fXPos2, fYPos2, fRot2
	TEXT_LABEL_31 tlSprite
	BOOL bTwoSprites
	
	GET_HOTWIRE_WIRE_DRAW_INFO(iStart, iEnd, fXPos, fYPos, fXScale, fYScale, fRot, tlSprite, fXPos2, fYPos2, fRot2, bTwoSprites)
	
	DRAW_SPRITE("MPHotwire", tlSprite, APPLY_ASPECT_MOD_TO_X(fXPos), fYPos, (fXScale / fAspectRatio), fYScale, fRot, 255, 255, 255, 255)
	IF bTwoSprites
		DRAW_SPRITE("MPHotwire", tlSprite, APPLY_ASPECT_MOD_TO_X(fXPos2), fYPos2, (fXScale / fAspectRatio), fYScale, fRot2, 255, 255, 255, 255)
	ENDIF
ENDPROC

FUNC FLOAT GET_SINE_ALPHA(FLOAT fOffset = 0.0, FLOAT fSineSpeed = 0.3)
	FLOAT fSineValue = TO_FLOAT((ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sHotwireData.tdHackTime.Timer)))) + (fOffset)
	FLOAT fLightSine = (SIN((fSineValue * fSineSpeed) * 1) + 0.1)
	
	RETURN 255 * fLightSine
ENDFUNC

FUNC BOOL IS_END_NODE_OCCUPIED(INT iEndNode)
	INT i
	FOR i = 0 TO MAX_HW_NODES - 1
		IF sHotwireData.iNodePairs[i] = iEndNode
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC DRAW_LIGHT(VECTOR_2D vPos, BOOL bGreen)

	FLOAT fScale = cfHWLIGHTSCALE
	
	INT iAlpha = ROUND(GET_SINE_ALPHA(vPos.y * 9999))
	
	IF bGreen
		DRAW_SPRITE("MPHotwire", "correctlight", APPLY_ASPECT_MOD_TO_X(vPos.x), vPos.y, fScale / fAspectRatio, fScale, 0.0, 255, 255, 255, 255)
		//DRAW_SPRITE("MPHotwire", "correctlight", APPLY_ASPECT_MOD_TO_X(vPos.x), vPos.y, (1.0 / fAspectRatioMod) * fScale, 1.0 * fScale, 0.0, 255, 255, 255, 255)
	ELSE
		DRAW_SPRITE("MPHotwire", "wronglight", APPLY_ASPECT_MOD_TO_X(vPos.x), vPos.y, fScale / fAspectRatio, fScale, 0.0, 255, 255, 255, iAlpha)
		//DRAW_SPRITE("MPHotwire", "wronglight", APPLY_ASPECT_MOD_TO_X(vPos.x), vPos.y, (1.0 / fAspectRatioMod) * fScale, 1.0 * fScale, 0.0, 255, 255, 255, iAlpha)
	ENDIF
ENDPROC

PROC DRAW_LIFE(VECTOR_2D vPos)

	FLOAT fScale = cfHWLIFESCALE
	
	FLOAT fSineValue = TO_FLOAT((ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sHotwireData.tdHackTime.Timer)))) + (vPos.y * 9999)
	FLOAT fSineSpeed = 0.5
	FLOAT fLightSine = (SIN((fSineValue * fSineSpeed) * 1) + 0.1)
	
	INT iAlpha = ROUND(255 * fLightSine)
	DRAW_SPRITE("MPHotwire", "lifelight", APPLY_ASPECT_MOD_TO_X(vPos.x), vPos.y, fScale / fAspectRatio, fScale, 0.0, 255, 255, 255, iAlpha)
ENDPROC

FUNC FLOAT GET_HOTWIRE_LIGHT_OFFSET_Y(INT iNode)	
	SWITCH iNode
		CASE 0	RETURN 0.290
		CASE 1	RETURN 0.440
		CASE 2	RETURN 0.590
		CASE 3	RETURN 0.740
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC VECTOR_2D GET_HOTWIRE_LIFE_POS(INT iNode, INT iLight)
	SWITCH iNode
		CASE 0
			SWITCH iLight
				CASE 3 RETURN INIT_VECTOR_2D(0.984, 0.289)
				CASE 2 RETURN INIT_VECTOR_2D(0.950, 0.289)
				CASE 1 RETURN INIT_VECTOR_2D(0.915, 0.289)
				CASE 0 RETURN INIT_VECTOR_2D(0.880, 0.289)
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iLight
				CASE 3 RETURN INIT_VECTOR_2D(0.984, 0.439)
				CASE 2 RETURN INIT_VECTOR_2D(0.950, 0.439)
				CASE 1 RETURN INIT_VECTOR_2D(0.915, 0.439)
				CASE 0 RETURN INIT_VECTOR_2D(0.880, 0.439)
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iLight
				CASE 3 RETURN INIT_VECTOR_2D(0.984, 0.589)
				CASE 2 RETURN INIT_VECTOR_2D(0.950, 0.589)
				CASE 1 RETURN INIT_VECTOR_2D(0.915, 0.589)
				CASE 0 RETURN INIT_VECTOR_2D(0.880, 0.589)
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH iLight
				CASE 3 RETURN INIT_VECTOR_2D(0.984, 0.739)
				CASE 2 RETURN INIT_VECTOR_2D(0.950, 0.739)
				CASE 1 RETURN INIT_VECTOR_2D(0.915, 0.739)
				CASE 0 RETURN INIT_VECTOR_2D(0.880, 0.739)
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN INIT_VECTOR_2D(0.0,0.0)
ENDFUNC

PROC HANDLE_HIGHLIGHT_SOUND(INT iLeftIndex, INT iRightIndex)

	IF iLeftIndex > -1
		IF NOT IS_BIT_SET(sHotwireData.iHotwireBS, ciHotwire_HasPlayedHighlightSound_LeftNode_0 + iLeftIndex)
			PLAY_SOUND_FRONTEND(-1, "Output_Highlight", sHotwireSoundSet)
			SET_BIT(sHotwireData.iHotwireBS, ciHotwire_HasPlayedHighlightSound_LeftNode_0 + iLeftIndex)
		ENDIF
	ENDIF
	
	IF iRightIndex > -1
		IF NOT IS_BIT_SET(sHotwireData.iHotwireBS, ciHotwire_HasPlayedHighlightSound_RightNode_0 + iRightIndex)
			PLAY_SOUND_FRONTEND(-1, "Input_Highlight", sHotwireSoundSet)
			SET_BIT(sHotwireData.iHotwireBS, ciHotwire_HasPlayedHighlightSound_RightNode_0 + iRightIndex)
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_HOTWIRE_LEVEL()

	INT i,j
	//Draw these first
	FOR i = 0 TO (MAX_HW_NODES - 1)
		IF sHotwireData.iNodePairs[i] != -1
			DRAW_PLACED_WIRE(i, sHotwireData.iNodePairs[i])
		ENDIF
	ENDFOR
	FOR i = 0 TO (MAX_HW_NODES - 1)
	
		BOOL bLeftNodeSelected = (GET_START_NODE_FROM_CURSOR() = i)
		BOOL bRightNodeSelected = (GET_END_NODE_FROM_CURSOR() = i) AND sHotwireData.iSelectedStartNode > -1 AND NOT IS_END_NODE_OCCUPIED(i)
		
		BOOL bLeftShouldFlash = (IS_HOTWIRE_TUTORIAL() AND htsCurTutState = HTS_SELECT_NODE AND i = ciLeftNodeForTutorial)
		BOOL bRightShouldFlash = (IS_HOTWIRE_TUTORIAL() AND htsCurTutState = HTS_SELECT_RIGHT_NODE AND i = sHotwireData.iConnections[ciLeftNodeForTutorial])
		
		IF htsCurTutState = HTS_SET_ALL
			IF sHotwireData.iSelectedStartNode > -1
				IF NOT IS_END_NODE_OCCUPIED(i)
					bRightShouldFlash = TRUE
				ENDIF
			ELSE
				IF sHotwireData.iNodePairs[i] = -1 
					bLeftShouldFlash = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		INT iFlashAlpha = ROUND(GET_SINE_ALPHA(DEFAULT, 0.4))
		
		//Left side node
		VECTOR_2D vNodeMiddle = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sHotwireLevel.vStartNodes[i][0], sHotwireLevel.vStartNodes[i][2]), 2.0)
		
		IF bLeftShouldFlash
			DRAW_SPRITE("MPHotwire", "leftnode_roll", APPLY_ASPECT_MOD_TO_X(vNodeMiddle.x), vNodeMiddle.y, (cfNODESCALE_ROLL_X * cfHighlightSize) / fAspectRatio, cfNODESCALE_ROLL_Y * cfHighlightSize, 0.0, 255, 255, 0, iFlashAlpha)
		ENDIF
		
		IF bLeftNodeSelected
			HANDLE_HIGHLIGHT_SOUND(i, -1)
			DRAW_SPRITE("MPHotwire", "leftnode_roll", APPLY_ASPECT_MOD_TO_X(vNodeMiddle.x), vNodeMiddle.y, cfNODESCALE_ROLL_X / fAspectRatio, cfNODESCALE_ROLL_Y, 0.0, 255, 255, 255, 255)
		ELSE
			CLEAR_BIT(sHotwireData.iHotwireBS, ciHotwire_HasPlayedHighlightSound_LeftNode_0 + i)
			DRAW_SPRITE("MPHotwire", "leftnode", APPLY_ASPECT_MOD_TO_X(vNodeMiddle.x), vNodeMiddle.y, cfNODESCALEX / fAspectRatio, cfNODESCALEY, 0.0, 255, 255, 255, 255)
		ENDIF
		
		//Right side node
		vNodeMiddle = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sHotwireLevel.vEndNodes[i][0], sHotwireLevel.vEndNodes[i][2]), 2.0)
		
		IF bRightShouldFlash
			DRAW_SPRITE("MPHotwire", "rightnode_roll", APPLY_ASPECT_MOD_TO_X(vNodeMiddle.x), vNodeMiddle.y, (cfNODESCALE_ROLL_X * cfHighlightSize) / fAspectRatio, cfNODESCALE_ROLL_Y * cfHighlightSize, 0.0, 255, 255, 0, iFlashAlpha)
		ENDIF
		
		IF bRightNodeSelected
			HANDLE_HIGHLIGHT_SOUND(-1, i)
			DRAW_SPRITE("MPHotwire", "rightnode_roll", APPLY_ASPECT_MOD_TO_X(vNodeMiddle.x), vNodeMiddle.y, cfNODESCALE_ROLL_X / fAspectRatio, cfNODESCALE_ROLL_Y, 0.0, 255, 255, 255, 255)
		ELSE
			CLEAR_BIT(sHotwireData.iHotwireBS, ciHotwire_HasPlayedHighlightSound_RightNode_0 + i)
			DRAW_SPRITE("MPHotwire", "rightnode", APPLY_ASPECT_MOD_TO_X(vNodeMiddle.x), vNodeMiddle.y, cfNODESCALEX / fAspectRatio, cfNODESCALEY, 0.0, 255, 255, 255, 255)
		ENDIF
		
		//Light for right side node
		VECTOR_2D vEndLightPos = INIT_VECTOR_2D(cfHWLIGHTOFFSETX, GET_HOTWIRE_LIGHT_OFFSET_Y(i))
		DRAW_LIGHT(vEndLightPos, IS_BIT_SET(sHotwireData.iCorrectNodes, i))
		
		IF (sHotwireData.iNodeHealth[i] < 1)
			DRAW_LIFE(GET_HOTWIRE_LIFE_POS(i, 0))
		ENDIF
		IF (sHotwireData.iNodeHealth[i] < 2)
			DRAW_LIFE(GET_HOTWIRE_LIFE_POS(i, 1))
		ENDIF
		IF (sHotwireData.iNodeHealth[i] < 3)
			DRAW_LIFE(GET_HOTWIRE_LIFE_POS(i, 2))
		ENDIF
		IF (sHotwireData.iNodeHealth[i] < 4)
			DRAW_LIFE(GET_HOTWIRE_LIFE_POS(i, 3))
		ENDIF
	ENDFOR
	
	FOR i = 0 TO MAX_HW_INNER_WALLS - 1
		INT k
		FOR j = 0 TO MAX_HW_INNER_WALL_POINTS - 1
			k = j + 1
			IF k >= MAX_HW_INNER_WALL_POINTS
				k = 0
			ENDIF
		ENDFOR
	ENDFOR
	
	FOR i = 0 TO MAX_HW_OUTER_WALLS - 1
		j = i + 1
		IF j >= MAX_HW_OUTER_WALLS
			j = 0
		ENDIF
	ENDFOR
ENDPROC

//PROC DRAW_HOTWIRE_LEVEL_OLD()
//	INT i,j
//	FOR i = 0 TO MAX_HW_NODES - 1
//		//INT k
//		//FOR j = 0 TO MAX_HW_NODE_POINTS - 1
//			//k = j + 1
//			//IF k >= MAX_HW_NODE_POINTS
//				//k = 0
//			//ENDIF
//			
//			//INT iDamageColourOffset = (MAX_LIVES_PER_NODE - sHotwireData.iNodeHealth[i]) * (255 / MAX_LIVES_PER_NODE)
//			
//			//STRING sPortSpriteName = "sh_port3"
//			
////			SWITCH sHotwireData.iNodeHealth[i]
////				CASE 4 sPortSpriteName = "sh_port3" BREAK
////				CASE 3 sPortSpriteName = "sh_port2" BREAK
////				CASE 2 sPortSpriteName = "port1" 	BREAK
////				CASE 1 sPortSpriteName = "sh_port0" BREAK
////			ENDSWITCH
//			
//			FLOAT fScale = 0.1
//			VECTOR_2D vNodeMiddle = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sHotwireLevel.vStartNodes[i][0], sHotwireLevel.vStartNodes[i][2]), 2.0)
//			DRAW_SPRITE("MPHotwire", "leftnode", APPLY_ASPECT_MOD_TO_X(vNodeMiddle.x), vNodeMiddle.y, (fScale) / fAspectRatio, 1.0 * fScale, 0.0, 255, 255, 255, 255)
//			//Light
//			VECTOR_2D vStartLightPos = vNodeMiddle
//			vStartLightPos.x += 0.025
//			DRAW_LIGHT(vStartLightPos, TRUE)
//			
//			vNodeMiddle = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(sHotwireLevel.vEndNodes[i][0], sHotwireLevel.vEndNodes[i][2]), 2.0)
//			DRAW_SPRITE("MPHotwire", "rightnode", APPLY_ASPECT_MOD_TO_X(vNodeMiddle.x), vNodeMiddle.y, ((1.0) * -fScale) / fAspectRatio, 1.0 * fScale, 0.0, 255, 255, 255, 255)
//			//Light
//			VECTOR_2D vEndLightPos = vNodeMiddle
//			vEndLightPos.x -= 0.1
//			DRAW_LIGHT(vEndLightPos, IS_BIT_SET(sHotwireData.iCorrectNodes, i))
//			
//			//DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sHotwireLevel.vStartNodes[i][j], sHotwireLevel.vStartNodes[i][k], rgbaHWNodes.iR, rgbaHWNodes.iG - iDamageColourOffset, rgbaHWNodes.iB - iDamageColourOffset, rgbaHWNodes.iA)
//			//DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sHotwireLevel.vEndNodes[i][j], sHotwireLevel.vEndNodes[i][k], rgbaHWNodes.iR, rgbaHWNodes.iG - iDamageColourOffset, rgbaHWNodes.iB - iDamageColourOffset, rgbaHWNodes.iA)
//		//ENDFOR
//	ENDFOR
//	
//	FOR i = 0 TO MAX_HW_INNER_WALLS - 1
//		INT k
//		FOR j = 0 TO MAX_HW_INNER_WALL_POINTS - 1
//			k = j + 1
//			IF k >= MAX_HW_INNER_WALL_POINTS
//				k = 0
//			ENDIF
//			//DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sHotwireLevel.vInnerWalls[i][j], sHotwireLevel.vInnerWalls[i][k], rgbaHWInnerWalls.iR, rgbaHWInnerWalls.iG, rgbaHWInnerWalls.iB, rgbaHWInnerWalls.iA)
//		ENDFOR
//	ENDFOR
//	
//	FOR i = 0 TO MAX_HW_OUTER_WALLS - 1
//		j = i + 1
//		IF j >= MAX_HW_OUTER_WALLS
//			j = 0
//		ENDIF
//		//DRAW_DEBUG_LINE_2D_FROM_VECTOR_2D(sHotwireLevel.vOuterWalls[i], sHotwireLevel.vOuterWalls[j], rgbaHWOuterWalls.iR, rgbaHWOuterWalls.iG, rgbaHWOuterWalls.iB, rgbaHWOuterWalls.iA)
//	ENDFOR
//ENDPROC


FUNC VECTOR_2D GET_PORT_FROM_START_NODE(INT iNode)
	VECTOR_2D vBase = GET_MIDPOINT_FROM_START_NODE(iNode)
	
	vBase.x += 0.081
		
	RETURN vBase
ENDFUNC

FUNC VECTOR_2D GET_PORT_FROM_END_NODE(INT iNode)
	VECTOR_2D vBase = GET_MIDPOINT_FROM_END_NODE(iNode)
	
	vBase.x -= 0.05
		
	RETURN vBase
ENDFUNC

PROC KEEP_HOTWIRE_CURSOR_IN_BOUNDS(FLOAT& fX, FLOAT& fY)

	FLOAT fXBoundsL = -0.1
	FLOAT fXBoundsR = 1.1
	
	FLOAT fYBoundsT = 0.175
	FLOAT fYBoundsB = 0.845
	
	//PRINTLN("[TMS] X = ", fX, " Y = ", fY)
	
	/////X
	IF fX > fXBoundsR
		fX = fXBoundsR
	ENDIF
	
	IF fX < fXBoundsL
		fX = fXBoundsL
	ENDIF
	
	/////Y
	IF fY > fYBoundsB
		fY = fYBoundsB
	ENDIF
	
	IF fY < fYBoundsT
		fY = fYBoundsT
	ENDIF
ENDPROC

PROC DRAW_WIRE(VECTOR_2D vStartPoint, VECTOR_2D vEndPoint)
	VECTOR_2D vCentre = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(vStartPoint, vEndPoint), 2.0)
	FLOAT fDist = VECTOR_2D_DIST(vStartPoint, vEndPoint)
	FLOAT fRotation = ATAN2(vStartPoint.y - vEndPoint.y, vStartPoint.x - vEndPoint.x)
	DRAW_SPRITE_ARX("MPHotwire", "line", APPLY_ASPECT_MOD_TO_X(vCentre.x), vCentre.y, fDist / fAspectRatio, cfWireHeight, fRotation, 255, 255, 255, 255)
ENDPROC

PROC PROCESS_HOTWIRE_CURSOR()

	FLOAT fCursorSize = 0.01
	BOOL bAchievedNothing = TRUE
	
	//TODO: Cap values at play area bounds
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		vHotwireCursor.x = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
		vHotwireCursor.y = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	ELSE
		FLOAT fX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
		FLOAT fY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
		IF fX > 0.1 OR fX < -0.1
			vHotwireCursor.x = vHotwireCursor.x +@ (fX / cfANALOGUE_SPEED_REDUCTION)
		ENDIF
		IF fY > 0.1 OR fY < -0.1
			vHotwireCursor.y = vHotwireCursor.y +@ (fY / cfANALOGUE_SPEED_REDUCTION)
		ENDIF
		
		KEEP_HOTWIRE_CURSOR_IN_BOUNDS(vHotwireCursor.x, vHotwireCursor.y)
	ENDIF
	RGBA_COLOUR_STRUCT rgbaCursor
	INIT_RGBA_STRUCT(rgbaCursor, 255, 0, 0)
	INT iCursorNode = GET_START_NODE_FROM_CURSOR()
	IF iCursorNode > -1
		rgbaCursor = rgbaHWWires[iCursorNode]
		
		IF sHotwireData.iSelectedStartNode = -1
			FLOAT fSineValue = TO_FLOAT((ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sHotwireData.tdHackTime.Timer))))
			FLOAT fSineSpeed = 0.5
			fCursorSize = (SIN(fSineValue * fSineSpeed) * 0.019) + 0.001
			
			//SET_CONTROL_SHAKE(FRONTEND_CONTROL, 25, 1)
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR (IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT))
			
			IF sHotwireData.iSelectedStartNode = -1
				PLAY_SOUND_FRONTEND(-1, "Grab_Wire", sHotwireSoundSet)
				bAchievedNothing = FALSE
			ENDIF
			
			sHotwireData.iSelectedStartNode = iCursorNode
		ENDIF
	ENDIF
	
	IF sHotwireData.iSelectedStartNode > -1
		INT iCursorEndNode = GET_END_NODE_FROM_CURSOR()
		DRAW_WIRE(GET_PORT_FROM_START_NODE(sHotwireData.iSelectedStartNode), vHotwireCursor)

		IF iCursorEndNode > -1
			IF NOT IS_END_NODE_OCCUPIED(iCursorEndNode)
				INIT_RGBA_STRUCT(rgbaCursor, 255, 0, 0)
				FLOAT fSineValue = TO_FLOAT((ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sHotwireData.tdHackTime.Timer))))
				FLOAT fSineSpeed = 0.5
				fCursorSize = (SIN(fSineValue * fSineSpeed) * 0.019) + 0.001
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR (IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT))
					sHotwireData.iNodePairs[sHotwireData.iSelectedStartNode] = iCursorEndNode
					sHotwireData.iSelectedStartNode = -1
					PLAY_SOUND_FRONTEND(-1, "Attach_Wire", sHotwireSoundSet)
					bAchievedNothing = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR (IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT))
		IF bAchievedNothing
			PLAY_SOUND_FRONTEND(-1, "Error", sHotwireSoundSet)
		ENDIF
	ENDIF
	
	FLOAT fScale = 0.1 + fCursorSize
	DRAW_SPRITE("MPHotwire", "Cursor_0", APPLY_ASPECT_MOD_TO_X(vHotwireCursor.x), vHotwireCursor.y, (1.0 / fAspectRatio) * fScale, 1.0 * fScale, 0.0, 255, 255, 255, 255)
ENDPROC

#IF IS_DEBUG_BUILD
PROC SET_HOTWIRE_PARENT_WIDGET_GROUP(WIDGET_GROUP_ID parentWidgetGroup)
	widgetHotwireMinigame = parentWidgetGroup
ENDPROC
#ENDIF

PROC PROCESS_HOTWIRE_WIDGETS()
	#IF IS_DEBUG_BUILD
	IF NOT bDebugCreatedHotwireWidgets
		SET_CURRENT_WIDGET_GROUP(widgetHotwireMinigame)
		START_WIDGET_GROUP("     HOTWIRE")
			ADD_WIDGET_FLOAT_SLIDER("cfANALOGUE_SPEED_REDUCTION", cfANALOGUE_SPEED_REDUCTION, 0, 100, 0.5)
			//ADD_WIDGET_VECTOR_SLIDER("TESTPOS", vTestPos_HW, -0.5, 1.1, 0.0001)
			ADD_WIDGET_VECTOR_SLIDER("TESTPOS", vTestPos_HW, -1, 1, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("TESTVEC", vTestVec_HW, -1, 1, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("cfHWLIGHTSCALE", cfHWLIGHTSCALE, -1, 2, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("cfHWLIGHTOFFSETX", cfHWLIGHTOFFSETX, -1, 2, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("cfHWLIFESCALE", cfHWLIFESCALE, -1, 2, 0.001)
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(widgetHotwireMinigame)
		bDebugCreatedHotwireWidgets = TRUE
	ENDIF
	#ENDIF
ENDPROC

FUNC STRING GET_STRING_FROM_HOTWIRE_STATE(HOTWIRE_STATE eState)
	SWITCH eState
		CASE HOTWIRE_INIT		RETURN "HOTWIRE_INIT"
		CASE HOTWIRE_PLAY		RETURN "HOTWIRE_PLAY"
		CASE HOTWIRE_PASS		RETURN "HOTWIRE_PASS"
		CASE HOTWIRE_FAIL		RETURN "HOTWIRE_FAIL"
		CASE HOTWIRE_CLEANUP	RETURN "HOTWIRE_CLEANUP"
	ENDSWITCH		
	RETURN "GET_STRING_FROM_HOTWIRE_STATE - No Such State"
ENDFUNC

PROC UPDATE_HOTWIRE_STATE(HOTWIRE_STATE eNewState)
	PRINTLN("[JS][Hotwire] - UPDATE_HOTWIRE_STATE - Updating state from ",GET_STRING_FROM_HOTWIRE_STATE(eHotwireState)," to ",GET_STRING_FROM_HOTWIRE_STATE(eNewState))
	eHotwireState = eNewState
ENDPROC

PROC CLEAN_UP_HOTWIRE_MINIGAME(HACKING_CONTROLLER_STRUCT &sControllerStruct, BOOL bFinalCleanup = FALSE)
	//DEBUG_PRINTCALLSTACK()
	Unpause_Objective_Text()
	g_FMMC_STRUCT.bDisablePhoneInstructions = FALSE
	MP_FORCE_TERMINATE_INTERNET_CLEAR()
	//Renable notifications
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	//Disable multiple displays/blinders.
	SET_MULTIHEAD_SAFE(FALSE, TRUE)
	ENABLE_INTERACTION_MENU()
	IF NOT bFinalCleanup
		IF IS_SKYSWOOP_AT_GROUND()
		AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
	ENDIF
	RESET_NET_TIMER(tdHotwireEndDelay)
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfHotwireHacking)
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_XM17_IAA_Hack_Minigame_Scene")
		STOP_AUDIO_SCENE("DLC_XM17_IAA_Hack_Minigame_Scene")
	ENDIF
	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, TRUE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTalkingAnimations, TRUE)
	
	fDmgForBackgroundLoop = 0.0
	STOP_SOUND(sBackgroundLoopSndID)
	
	UPDATE_HOTWIRE_STATE(HOTWIRE_INIT)
	sControllerStruct.iBS = 0
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_HOTWIRE_DEBUG()
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	IF NOT bDebugCreatedHotwireWidgets
		PROCESS_HOTWIRE_WIDGETS()
	ENDIF
ENDPROC
#ENDIF

PROC PROCESS_HOTWIRE_EVERY_FRAME_MISC()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	Pause_Objective_Text()
	DISABLE_SELECTOR_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	IF NOT IS_SKYSWOOP_AT_GROUND() 
		UPDATE_HOTWIRE_STATE(HOTWIRE_CLEANUP)
	ENDIF
	#IF USE_REPLAY_RECORDING_TRIGGERS
		DISABLE_REPLAY_RECORDING_UI_THIS_FRAME()
	#ENDIF
	REPLAY_PREVENT_RECORDING_THIS_FRAME()
	IF NOT IS_PAUSE_MENU_ACTIVE()
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	ENDIF
	
	HACKING_GET_CUSTOM_SCREEN_ASPECT_RATIO()
	HACKING_GET_ASPECT_RATIO_MODIFIER_FROM_ASPECT_RATIO()
	HACKING_HELP_SCALEFORM_EVERY_FRAME()
ENDPROC

PROC INIT_HOTWIRE_COLOURS()
	INIT_RGBA_STRUCT(rgbaHWBackground, 255, 255, 255, 255)
	INIT_RGBA_STRUCT(rgbaHWOuterWalls, 255, 255, 255)
	INIT_RGBA_STRUCT(rgbaHWInnerWalls, 255, 255, 255)
	INIT_RGBA_STRUCT(rgbaHWNodes, 175, 175, 175)
	INIT_RGBA_STRUCT(rgbaHWWires[0], 255, 0, 255)
	INIT_RGBA_STRUCT(rgbaHWWires[1], 0, 255, 0)
	INIT_RGBA_STRUCT(rgbaHWWires[2], 0, 0, 255)
	INIT_RGBA_STRUCT(rgbaHWWires[3], 255, 255, 0)
ENDPROC

FUNC BOOL HAS_PLAYER_FAILED_HOTWIRE()
	IF HAS_NET_TIMER_EXPIRED(sHotwireData.tdHackTime, ciHOTWIRE_TIME)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sHotwireData.iHotwireBS, ciHotwire_Failed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HOTWIRE_QUICK_WIN_CHECK()

	INT iNode = 0
	INT iCorrectNodes = 0
	
	REPEAT MAX_HW_NODES iNode
		IF sHotwireData.iNodePairs[iNode] = sHotwireData.iConnections[iNode]
			iCorrectNodes++
		ENDIF
	ENDREPEAT
	
	RETURN iCorrectNodes = 4
ENDFUNC

FUNC BOOL ATTEMPT_HOTWIRE()
	INT iNode = 0
	INT iCorrectNodes = 0
	
	FLOAT fSndVars[MAX_HW_NODES]
	
	IF htsCurTutState = HTS_TEST
	AND HOTWIRE_QUICK_WIN_CHECK()
		INT iTempConnection = sHotwireData.iConnections[0]
		INT iTempConnection2 = sHotwireData.iConnections[2]
		sHotwireData.iConnections[0] = sHotwireData.iConnections[3]
		//sHotwireData.iConnections[1] = sHotwireData.iConnections[2]
		sHotwireData.iConnections[2] = iTempConnection
		sHotwireData.iConnections[3] = iTempConnection2
		PRINTLN("[HWTUT] Changing connections because the player almost won")
	ENDIF
	
	REPEAT MAX_HW_NODES iNode
		IF sHotwireData.iNodePairs[iNode] = sHotwireData.iConnections[iNode]
			//Correct!
			PRINTLN("[Hotwire][TMS] Node ", iNode, " is connected correctly!")
			SET_BIT(sHotwireData.iCorrectNodes, sHotwireData.iNodePairs[iNode])	
			fSndVars[iNode] = 1.0
			iCorrectNodes++
		ELSE
			//Wrong connection
			sHotwireData.iNodeHealth[sHotwireData.iNodePairs[iNode]]--
			CLEAR_BIT(sHotwireData.iCorrectNodes, sHotwireData.iNodePairs[iNode])
			fSndVars[iNode] = 0.0
			
			PRINTLN("[Hotwire][TMS] Node ", iNode, " has been damaged and now has this much health: ", sHotwireData.iNodeHealth[sHotwireData.iNodePairs[iNode]])
			sHotwireData.iNodePairs[iNode] = -1
			
			IF NOT IS_HOTWIRE_TUTORIAL()
			AND NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HAS_PLAYED_HOTWIRE_HACK)
				CHANGE_HOTWIRE_TUTORIAL_STATE(HTS_LIVES)
			ENDIF
			
			SET_CONTROL_SHAKE(FRONTEND_CONTROL, 350, 10)
			
			IF sHotwireData.iNodeHealth[iNode] < 1
				//Game over!
				SET_BIT(sHotwireData.iHotwireBS, ciHotwire_Failed)
				PRINTLN("[Hotwire][TMS] Game over because node ", iNode, " is dead!")
			ENDIF
			
			fDmgForBackgroundLoop += 0.075
			PRINTLN("[Hotwire] fDmgForBackgroundLoop is now ", fDmgForBackgroundLoop)
		ENDIF
	ENDREPEAT
	
	
	IF NOT IS_BIT_SET(sHotwireData.iHotwireBS, ciHotwire_Failed)
		INT sTestSndID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(sTestSndID, "Test_Circuit", sHotwireSoundSet)
		SET_VARIABLE_ON_SOUND(sTestSndID, "Wire_01", fSndVars[0])
		SET_VARIABLE_ON_SOUND(sTestSndID, "Wire_02", fSndVars[1])
		SET_VARIABLE_ON_SOUND(sTestSndID, "Wire_03", fSndVars[2])
		SET_VARIABLE_ON_SOUND(sTestSndID, "Wire_04", fSndVars[3])
	ENDIF
	
	RETURN iCorrectNodes = MAX_HW_NODES
ENDFUNC

FUNC BOOL ARE_ALL_HOTWIRE_WIRES_CONNECTED_TO_SOMETHING()
	INT iNode = 0
	REPEAT MAX_HW_NODES iNode
		IF sHotwireData.iNodePairs[iNode] = -1
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_HOTWIRE_HELPTEXT()
	STRING tl15 = ""
	
	IF ARE_ALL_HOTWIRE_WIRES_CONNECTED_TO_SOMETHING()
		tl15 = "HTWR_PSY"
	ELIF sHotwireData.iSelectedStartNode != -1
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			tl15 = "HTWR_HLP2_PC"
		ELSE
			tl15 = "HTWR_HLP2"
		ENDIF
	ELSE
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			tl15 = "HTWR_HLP_PC"
		ELSE
			tl15 = "HTWR_HLP"
		ENDIF
	ENDIF
	
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tl15)
		CLEAR_HELP()
	ENDIF
	
	PRINT_HELP_NO_SOUND(tl15)
ENDPROC

PROC PROCESS_HOTWIRE_UI()
	IF HAS_NET_TIMER_STARTED(sHotwireData.tdHackTime)
		INT iTimeDif = ciHOTWIRE_TIME - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sHotwireData.tdHackTime)
		INT iMinutes = iTimeDif/60000
		INT iSeconds = iTimeDif/1000 % 60
		SET_TEXT_SCALE(0.35, 0.35)
		SET_TEXT_COLOUR(76, 190, 148, 255)
		SET_TEXT_CENTRE(TRUE)
		TEXT_LABEL_15 tl15 = "BEAM_TR"
		IF iSeconds < 10
			tl15 += "S"
		ENDIF
		BEGIN_TEXT_COMMAND_DISPLAY_TEXT(tl15)
			ADD_TEXT_COMPONENT_INTEGER(iMinutes)
			ADD_TEXT_COMPONENT_INTEGER(iSeconds)
		END_TEXT_COMMAND_DISPLAY_TEXT(APPLY_ASPECT_MOD_TO_X(0.5), 0.17)
	ENDIF
	
	IF NOT IS_HOTWIRE_TUTORIAL()
	OR htsCurTutState = HTS_SET_ALL
		PROCESS_HOTWIRE_HELPTEXT()
	ENDIF
	
	//Scaleform
	IF ARE_ALL_HOTWIRE_WIRES_CONNECTED_TO_SOMETHING()
		ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL, "HWSF_QT") //Quit
		ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_Y, "HWSF_TST") //Test Connections
	ELIF sHotwireData.iSelectedStartNode > -1
		ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL, "HWSF_QT") //Quit
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			ADD_MENU_HELP_KEY_INPUT(INPUT_CURSOR_ACCEPT, "HWSF_CONN") //Connect Node
			ADD_MENU_HELP_KEY_INPUT(INPUT_CURSOR_X, "HWSF_MWRE") //Move Wire
		ELSE
			ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_ACCEPT, "HWSF_CONN") //Connect Node
			ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_LS, "HWSF_MWRE") //Move Wire
		ENDIF
	ELSE
		ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL, "HWSF_QT") //Quit
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			ADD_MENU_HELP_KEY_INPUT(INPUT_CURSOR_ACCEPT, "HWSF_SEL")//Select Start Node
			ADD_MENU_HELP_KEY_INPUT(INPUT_CURSOR_X, "HWSF_MCUR")//Move Cursor
		ELSE
			ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_ACCEPT, "HWSF_SEL")//Select Start Node
			ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_LS, "HWSF_MCUR")//Move Cursor
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HOTWIRE_PASS_SEQUENCE()

	FLOAT fScale = 0.25
	FLOAT fBary = 0.511
	FLOAT fBarMultiplier = 0.454 * 50
	
	DRAW_SPRITE("MPHotwire", "passed", 0.5, 0.5, cfBOXSCALEX / fAspectRatio, cfBOXSCALEY, 0, 255, 255, 255, ROUND(sHotwireData.fSuccessWindowAlpha))
	
	fScale = 0.03
	
	IF hpsCurPassState != HPS_OPENING_DOWNLOAD
		fScale = 0.011
		
		FLOAT fBarX_Left = 0.375
		//FLOAT fBarX_Right = 0.570
		FLOAT fCurBarX = LERP_FLOAT(fBarX_Left, 0.5, sHotwireData.fDownloadAmount)
		
		//sHotwireData.fDownloadAmount = vTestVec_HW.z
		//DRAW_LINE_2D(vTestVec_HW.x, 0, vTestVec_HW.x, 1, 0.0005, 255, 0, 0, 255)
		//DRAW_LINE_2D(APPLY_ASPECT_MOD_TO_X(fCurBarX), 0, APPLY_ASPECT_MOD_TO_X(fCurBarX), 1, 0.0015, 0, 255, 0, 255)
		//DRAW_SPRITE("MPHotwire", "load_bar", (APPLY_ASPECT_MOD_TO_X(vTestPos_HW.x)), fBary, 0.01, fScale, 90, 255, 255, 255, 255)
		
		DRAW_SPRITE("MPHotwire", "load_bar", APPLY_ASPECT_MOD_TO_X(fCurBarX), fBary, (sHotwireData.fDownloadAmount * fScale * fBarMultiplier) / fAspectRatio, fScale, 90, 255, 255, 255, 255)
	ENDIF
	
	//DRAW_SPRITE("MPHotwire", "load_bar_box", 0.5, fBary, 0.256 / fAspectRatio, 0.128 * vTestVec_HW.y, 0, 255, 255, 255, ROUND(sHotwireData.fSuccessWindowAlpha))
	
	SWITCH hpsCurPassState
		CASE HPS_OPENING_DOWNLOAD
			PRINTLN("[HotwirePass] HPS_OPENING_DOWNLOAD")
			
			sHotwireData.fSuccessWindowAlpha = sHotwireData.fSuccessWindowAlpha +@ 200
			
			IF sHotwireData.fSuccessWindowAlpha >= 255
				sHotwireData.fSuccessWindowAlpha = 255
				hpsCurPassState = HPS_DOWNLOADING
				PRINTLN("[HotwirePass] Moving to HPS_DOWNLOADING")
			ENDIF
		BREAK
		
		CASE HPS_DOWNLOADING
			PRINTLN("[HotwirePass] HPS_OPENING_DOWNLOAD")
			
			IF sHotwireData.fDownloadAmount < 1
				sHotwireData.fDownloadAmount = sHotwireData.fDownloadAmount +@ 0.5
				
				IF sHotwireData.fDownloadAmount >= 1
					sHotwireData.fDownloadAmount = 1
				ENDIF
			ELSE
				sHotwireData.fDownloadAmount = 1
				hpsCurPassState = HPS_SUCCESS
				PRINTLN("[HotwirePass] Moving to HPS_SUCCESS")
				PLAY_SOUND_FRONTEND(-1, "Hack_Success", sHotwireSoundSet)
				fDmgForBackgroundLoop = 0.0
				PLAY_SOUND_FROM_ENTITY(-1, "Hack_Success_Remote", PLAYER_PED_ID(), sHotwireSoundSet, TRUE, 30)
			ENDIF
		BREAK
		
		CASE HPS_SUCCESS
			PRINTLN("[HotwirePass] HPS_SUCCESS")
			FLOAT fSuccessTextScale
			fSuccessTextScale = 0.612
			DRAW_BIG_HACKING_TEXT("BEAM_P", 76, 190, 148, 255, fSuccessTextScale, fSuccessTextScale, 0.5, 0.456)
		BREAK
	ENDSWITCH
ENDPROC

PROC WIN_HOTWIRE()
	REINIT_NET_TIMER(sHotwireData.tdEndDelay)
	hpsCurPassState = HPS_OPENING_DOWNLOAD
	UPDATE_HOTWIRE_STATE(HOTWIRE_PASS)
	
ENDPROC

FUNC BOOL PROCESS_HOTWIRE_PASSING()
	BOOL bSuccess = FALSE
	
	IF HAS_NET_TIMER_STARTED(sHotwireData.tdEndDelay)
		IF HAS_NET_TIMER_EXPIRED(sHotwireData.tdEndDelay, sHotwireData.iEndDelay)
			IF hpsCurPassState = HPS_DONE
				bSuccess = TRUE
				hpsCurPassState = HPS_OPENING_DOWNLOAD
				UPDATE_HOTWIRE_STATE(HOTWIRE_PASS)
				RESET_NET_TIMER(sHotwireData.tdEndDelay)
			ENDIF
		ENDIF
	ELSE
		IF ARE_ALL_HOTWIRE_WIRES_CONNECTED_TO_SOMETHING()
			//display button prompt
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
				IF ATTEMPT_HOTWIRE()
					IF NOT HAS_NET_TIMER_STARTED(sHotwireData.tdEndDelay)
						WIN_HOTWIRE()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bSuccess
ENDFUNC

PROC PROCESS_HOTWIRE_TUTORIAL()

	STRING sHelpText = ""
	
	SWITCH htsCurTutState
		CASE HTS_SELECT_NODE
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				sHelpText = "HWTUT_SEL_PC"
			ELSE
				sHelpText = "HWTUT_SEL"
			ENDIF
			
			IF sHotwireData.iSelectedStartNode > -1
				CHANGE_HOTWIRE_TUTORIAL_STATE(HTS_MOVE_WIRE)
			ENDIF
		BREAK
		
		CASE HTS_MOVE_WIRE
			sHelpText = "HWTUT_WIRE"
			
			IF vHotwireCursor.x > 0.35
				CHANGE_HOTWIRE_TUTORIAL_STATE(HTS_SELECT_RIGHT_NODE)
			ENDIF
		BREAK
		
		CASE HTS_SELECT_RIGHT_NODE
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				sHelpText = "HWTUT_SELR_PC"
			ELSE
				sHelpText = "HWTUT_SELR"
			ENDIF
			
			IF sHotwireData.iSelectedStartNode > -1
				IF sHotwireData.iNodePairs[sHotwireData.iSelectedStartNode] > -1
					CHANGE_HOTWIRE_TUTORIAL_STATE(HTS_SET_ALL)
				ENDIF
			ELSE
				CHANGE_HOTWIRE_TUTORIAL_STATE(HTS_SET_ALL)
			ENDIF
		BREAK
		
		CASE HTS_SET_ALL
			//sHelpText = "HWTUT_CONA"
			
			IF ARE_ALL_HOTWIRE_WIRES_CONNECTED_TO_SOMETHING()
				CHANGE_HOTWIRE_TUTORIAL_STATE(HTS_TEST)
			ENDIF
		BREAK
		
		CASE HTS_TEST
			sHelpText = "HWTUT_TEST"
			
			IF NOT ARE_ALL_HOTWIRE_WIRES_CONNECTED_TO_SOMETHING()
				START_NET_TIMER(sHotwireData.tdEndTutDelay)
				CHANGE_HOTWIRE_TUTORIAL_STATE(HTS_FINAL_HELP_TEXT)
			ENDIF
		BREAK
		
		CASE HTS_FINAL_HELP_TEXT
			
			sHelpText = "HWTUT_FINAL"
			
			IF HAS_NET_TIMER_EXPIRED(sHotwireData.tdEndTutDelay, 8500)
				RESET_NET_TIMER(sHotwireData.tdEndTutDelay)
				CHANGE_HOTWIRE_TUTORIAL_STATE(HTS_NONE)
			ENDIF
		BREAK
		
		CASE HTS_LIVES
			
			sHelpText = "HWTUT_LIVES"
			
			IF HAS_NET_TIMER_STARTED(sHotwireData.tdEndTutDelay)
				IF HAS_NET_TIMER_EXPIRED(sHotwireData.tdEndTutDelay, 8500)
					CHANGE_HOTWIRE_TUTORIAL_STATE(HTS_NONE)
				ENDIF
			ELSE
				REINIT_NET_TIMER(sHotwireData.tdEndTutDelay)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF NOT ARE_STRINGS_EQUAL(sHelpText, "")
		DISPLAY_HELP_TEXT_THIS_FRAME(sHelpText, TRUE)
	ENDIF
ENDPROC

PROC PROCESS_HOTWIRE_MINIGAME(HACKING_CONTROLLER_STRUCT &sControllerStruct)
	#IF IS_DEBUG_BUILD
	PROCESS_HOTWIRE_DEBUG()
	#ENDIF
	INT iScreenX, iScreenY
	GET_ACTUAL_SCREEN_RESOLUTION(iScreenX, iScreenY)
	#IF IS_DEBUG_BUILD
	IF IS_ROCKSTAR_DEV()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			WIN_HOTWIRE()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_L)
			SET_BIT(sHotwireData.iHotwireBS, ciHotwire_Failed)
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_T)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HAS_PLAYED_HOTWIRE_HACK, FALSE)
		ENDIF
	ENDIF
	#ENDIF
	
	IF eHotwireState != HOTWIRE_CLEANUP
		PROCESS_HOTWIRE_EVERY_FRAME_MISC()
	ENDIF
	IF eHotwireState != HOTWIRE_INIT
		DRAW_HOTWIRE_PLAY_AREA()
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(sfHotwireHacking)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfHotwireHacking, 100, 100, 100, 255)
	ENDIF
	
	SET_VARIABLE_ON_SOUND(sBackgroundLoopSndID, "Damage", fDmgForBackgroundLoop)
	
	SWITCH eHotwireState
		CASE HOTWIRE_INIT
			INIT_NODE_DATA()
			INIT_HOTWIRE_LEVEL()
			INIT_HOTWIRE_COLOURS()
			
			hpsCurPassState = HPS_OPENING_DOWNLOAD
			
			IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HAS_PLAYED_HOTWIRE_HACK)
				PRINTLN("[HWTut] Initialising tutorial!")				
				htsCurTutState = HTS_SELECT_NODE
				sHotwireData.iConnections[0] = 2
				sHotwireData.iConnections[1] = 0
				sHotwireData.iConnections[2] = 3
				sHotwireData.iConnections[3] = 1
			ELSE
				SHUFFLE_NODE_CONNECTIONS()
			ENDIF
			
			CLEAR_BIT(sHotwireData.iHotwireBS, ciHotwire_Failed)
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				DISABLE_INTERACTION_MENU() 
			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF			
			
			//Turn on blinders for multihead displays.
			IF (TO_FLOAT(iScreenX) / TO_FLOAT(iScreenY)) > 16.0 / 9.0
				SET_MULTIHEAD_SAFE(TRUE, TRUE)
			ENDIF
			g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
			//Prevent notifications from appearing during the minigame.
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = TRUE
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
			
			sfHotwireHacking = REQUEST_SCALEFORM_MOVIE("HACKING_MESSAGE") 
			
			IF HAS_SCALEFORM_MOVIE_LOADED(sfHotwireHacking)
				UPDATE_HOTWIRE_STATE(HOTWIRE_PLAY)
			ENDIF
			
			sBackgroundLoopSndID = GET_SOUND_ID()
			fDmgForBackgroundLoop = 0.0
			
			//sHotwireData.iSelectedStartNode = 0
			
			CLEAR_BIT(sControllerStruct.iBS, ciGOHACKBS_QUIT)
		BREAK
		
		CASE HOTWIRE_PLAY
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
				INIT_HOTWIRE_LEVEL()
			ENDIF
			#ENDIF
			DRAW_HOTWIRE_LEVEL()
			PROCESS_HOTWIRE_UI()
			PROCESS_HOTWIRE_CURSOR()
			
			//VECTOR_2D vTest2D
			//vTest2D.x = vTestVec_HW.x
			//vTest2D.y = vTestVec_HW.y
			//DRAW_LIFE(vTest2D)
			
			IF IS_HOTWIRE_TUTORIAL()
				PROCESS_HOTWIRE_TUTORIAL()
			ENDIF

			IF HAS_PLAYER_FAILED_HOTWIRE()
				HOTWIRE_PLAY_FAIL_SOUND()
				HACKING_STORE_END_TIME(sControllerStruct, sHotwireData.tdHackTime, ciHOTWIRE_TIME)
				UPDATE_HOTWIRE_STATE(HOTWIRE_FAIL)
			ELIF PROCESS_HOTWIRE_PASSING()
				HACKING_STORE_END_TIME(sControllerStruct, sHotwireData.tdHackTime, ciHOTWIRE_TIME)
				UPDATE_HOTWIRE_STATE(HOTWIRE_PASS)
			ENDIF
			
			IF HAS_SOUND_FINISHED(sBackgroundLoopSndID)
				PLAY_SOUND_FRONTEND(sBackgroundLoopSndID, "Background_Loop", sHotwireSoundSet)
				PRINTLN("[Hotwire] Playing background sound now")
			ENDIF
			
			DISABLE_CONTROL_ACTION (FRONTEND_CONTROL , INPUT_FRONTEND_PAUSE) 
			DISABLE_CONTROL_ACTION (FRONTEND_CONTROL , INPUT_FRONTEND_PAUSE_ALTERNATE) 
			
			DISABLE_CONTROL_ACTION (PLAYER_CONTROL , INPUT_FRONTEND_PAUSE) 
			DISABLE_CONTROL_ACTION (PLAYER_CONTROL , INPUT_FRONTEND_PAUSE_ALTERNATE) 
			
			SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, INPUT_FRONTEND_CANCEL)
			SET_INPUT_EXCLUSIVE (FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				PRINTLN("[HotWire] Player has backed out by pressing CANCEL")
				HACKING_STORE_END_TIME(sControllerStruct, sHotwireData.tdHackTime, ciHOTWIRE_TIME)
				SET_BIT(sControllerStruct.iBS, ciGOHACKBS_QUIT)
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_XM17_IAA_Hack_Minigame_Scene")
				START_AUDIO_SCENE("DLC_XM17_IAA_Hack_Minigame_Scene")
			ENDIF
			IF LOAD_MENU_ASSETS()
				DRAW_MENU_HELP_SCALEFORM(iScreenX)
			ENDIF
		BREAK
		CASE HOTWIRE_PASS
		
			IF hpsCurPassState = HPS_SUCCESS
				IF NOT HAS_NET_TIMER_STARTED(tdHotwireEndDelay)
					REINIT_NET_TIMER(tdHotwireEndDelay)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(tdHotwireEndDelay, ciHOTWIRE_END_DELAY)
						SET_BIT(sControllerStruct.iBS, ciGOHACKBS_PASSED)
						SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HAS_PLAYED_HOTWIRE_HACK, TRUE)
						PRINTLN("[Hotwire][HWTut] Setting MP_STAT_HAS_PLAYED_HOTWIRE_HACK")
						UPDATE_HOTWIRE_STATE(HOTWIRE_CLEANUP)
					ENDIF
				ENDIF
			ENDIF
			
			PROCESS_HOTWIRE_PASS_SEQUENCE()
			
		BREAK
		CASE HOTWIRE_FAIL
			IF NOT HAS_NET_TIMER_STARTED(tdHotwireEndDelay)
				REINIT_NET_TIMER(tdHotwireEndDelay)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdHotwireEndDelay, ciHOTWIRE_END_DELAY)
					SET_BIT(sControllerStruct.iBS, ciGOHACKBS_FAILED)
					UPDATE_HOTWIRE_STATE(HOTWIRE_CLEANUP)
				ELSE					
					//FLOAT fScale
					//fScale = 0.25
					DRAW_SPRITE("MPHotwire", "failed", 0.5, 0.5, cfBOXSCALEX / fAspectRatio, cfBOXSCALEY, 0, 255, 255, 255, ROUND(sHotwireData.fSuccessWindowAlpha))
					
					IF sHotwireData.fSuccessWindowAlpha < 255
						sHotwireData.fSuccessWindowAlpha = sHotwireData.fSuccessWindowAlpha +@ 255
					ELSE
						DRAW_BIG_HACKING_TEXT("BEAM_F", 223, 55, 66, 255, 0.612, 0.612, 0.5, 0.455)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE HOTWIRE_CLEANUP
			CLEAN_UP_HOTWIRE_MINIGAME(sControllerStruct)
		BREAK
	ENDSWITCH
ENDPROC
