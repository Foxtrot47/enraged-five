//
// name:        network_blips.sch
// description: Deals with network player blips.


// --- Include Files ------------------------------------------------------------

// Game Headers
USING "globals.sch"
USING "commands_hud.sch"
USING "script_player.sch"
USING "script_maths.sch"

USING "net_weaponswap.sch"
USING "net_common_functions.sch"
USING "shop_public.sch"
USING "net_team_info.sch"
USING "script_blips.sch"
USING "net_spectator_cam_common.sch"
USING "FM_In_Corona_Header.sch"
USING "net_entity_controller.sch"
USING "net_bounty.sch"
USING "freemode_events_header_private.sch"

USING "net_simple_interior_common.sch"

#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF

CONST_INT COP_BLIP_COLOUR_RED	(165 * 16777216) + (58 * 65536) + (66 * 256) + (255)
CONST_INT COP_BLIP_COLOUR_BLUE	(81 * 16777216) + (108 * 65536) + (162 * 256) + (255) 


TWEAK_FLOAT MIN_DIST_FOR_BLIP_SIZE 400.0
TWEAK_FLOAT MAX_DIST_FOR_BLIP_SIZE 1400.0
TWEAK_FLOAT MIN_BLIP_SIZE 0.78

CONST_FLOAT MIN_DIST_FOR_BLIP_ALPHA_IMPROMPTU_DM 100.0
CONST_FLOAT MAX_DIST_FOR_BLIP_ALPHA_IMPROMPTU_DM 500.0

TWEAK_FLOAT MIN_DIST_FOR_BLIP_ALPHA 400.0
TWEAK_FLOAT MAX_DIST_FOR_BLIP_ALPHA 1400.0

TWEAK_INT MIN_BLIP_ALPHA_FOR_DIST_FM 0
TWEAK_INT MIN_BLIP_ALPHA_FOR_DIST_ON_MISSION 50

TWEAK_FLOAT BASE_TANK_BLIP_SCALE 0.9

TWEAK_FLOAT MIN_NOISE_DIST 3.0

TWEAK_FLOAT VEHICLE_NOISE_DIST 50.0

CONST_FLOAT MAX_WANTED_BLIP_SIZE_POINT_IN_PHASE 0.33

CONST_INT HIGHLIGHT_BLIP_TIME 7000

CONST_INT FLASH_BLIP_TIME 7000
CONST_INT MAX_FLASH_BLIP_TIME 200000

CONST_INT OFF_THE_RADAR_TIME 60000

CONST_INT REVEAL_PLAYERS_TIME 60000

CONST_FLOAT MAX_HIGHLIGHTED_BLIP_SIZE_POINT_IN_PHASE 0.33

CONST_FLOAT DIST_TO_SHOW_NEARBY_CRIMINALS_AS_LONG_RANGE	500.0

CONST_INT BLIP_PHASE_1_TIME 250
CONST_INT BLIP_PHASE_2_TIME 750

TWEAK_INT BLIP_FLASHING_TIME 250

TWEAK_INT SELF_NOTICE_TIME  1000

TWEAK_FLOAT HIGHLIGHTED_SCALE_UP 1.0

TWEAK_FLOAT RACE_SCALE_UP 0.8

TWEAK_FLOAT PLAYER_ARROW_BLIP_SCALE 0.7

TWEAK_INT BLIP_FADE_PERIOD_FOR_FLASHING_WANTED_STARS 11000

TWEAK_FLOAT BLIP_SCALE_MULTIPLIER_FOR_NON_STANDARD_BLIPS 0.8
TWEAK_FLOAT BLIP_SCALE_MULTIPLIER_FOR_HIDDEN_PAUSE_BLIPS 0.00001
TWEAK_INT BLIP_ALPHA_FOR_PASSENGERS 1

TWEAK_INT VISIBLE_HEIGHT 25

CONST_FLOAT HEIST_APP_X -466.0
CONST_FLOAT HEIST_APP_Y -688.9
CONST_FLOAT HEIST_APP_Z 100.0
 
CONST_FLOAT ARENA_X -323.1
CONST_FLOAT ARENA_Y	-1970.9
CONST_FLOAT ARENA_Z 0.0

CONST_FLOAT OUTSIDE_ARENA_X -271.7750
CONST_FLOAT OUTSIDE_ARENA_Y	-1906.2070
CONST_FLOAT OUTSIDE_ARENA_Z  26.7554
CONST_FLOAT OUTSIDE_AREAN_HEADING 320.7970

CONST_FLOAT HEIST_SUB_X -1662.98
CONST_FLOAT HEIST_SUB_Y 5917.54
CONST_FLOAT HEIST_SUB_Z -51.0

CONST_INT BLIP_PLAYER_REASON_FORCE_BLIPPED				1
CONST_INT BLIP_PLAYER_REASON_SAME_PROPERTY_WAR			2
CONST_INT BLIP_PLAYER_REASON_SAME_TEAM					3
CONST_INT BLIP_PLAYER_REASON_WANTED						4
CONST_INT BLIP_PLAYER_REASON_NOTICABLE					5
CONST_INT BLIP_PLAYER_REASON_BACK_OF_VEHICLE			6
CONST_INT BLIP_PLAYER_REASON_SNITCH						7
CONST_INT BLIP_PLAYER_REASON_CRIM_IN_COP_CAR			8
CONST_INT BLIP_PLAYER_REASON_COP_IN_MY_WANTED_RADIUS	9
CONST_INT BLIP_PLAYER_REASON_SAME_TEAM_SAME_MISSION		10
CONST_INT BLIP_PLAYER_REASON_SAME_TEAM_NOT_ON_MISSION	11
CONST_INT BLIP_PLAYER_REASON_DEFAULT_ON					12
CONST_INT BLIP_PLAYER_REASON_FADING_OUT					13
CONST_INT BLIP_PLAYER_REASON_SPECTATOR					14
CONST_INT BLIP_PLAYER_REASON_SHOW_ALL					15
CONST_INT BLIP_PLAYER_REASON_TEAM_IS_HIGHLIGHTED		16
CONST_INT BLIP_PLAYER_REASON_TEAMS_ARE_FRIENDLY			17
CONST_INT BLIP_PLAYER_REASON_IMPROMPTU					18
CONST_INT BLIP_PLAYER_REASON_REVEAL_ALL					19
CONST_INT BLIP_PLAYER_REASON_ON_VEH_DM					20
CONST_INT BLIP_PLAYER_REASON_ACTIVE_ROAMING_SPEC_MODE	21

CONST_INT NOTICABLE_REASON_STEALTH_NOISE					1
CONST_INT NOTICABLE_REASON_DAMAGED_BY						2
//CONST_INT NOTICABLE_REASON_TALKING_LOUDHAILER				3
CONST_INT NOTICABLE_REASON_DRIVING_PAST_IN_VEHICLE			4
CONST_INT NOTICABLE_REASON_FIRING_IN_RANGE					5
CONST_INT NOTICABLE_REASON_LESS_THAN_COOL_DOWN_TIME			6
CONST_INT NOTICABLE_REASON_CHATTING_IN_RANGE				7
CONST_INT NOTICABLE_REASON_MAKING_CAR_NOISE					8
CONST_INT NOTICABLE_REASON_IS_TARGETTED						9
CONST_INT NOTICABLE_REASON_VEH_DM_WITHIN_RANGE				10
CONST_INT NOTICABLE_REASON_ON_VEH_DM						11
CONST_INT NOTICABLE_REASON_CAN_SEE_PLAYER					12
CONST_INT NOTICABLE_REASON_LOCKED_ON_BY_VEHICLE				13
CONST_INT NOTICABLE_REASON_IDLING							14
CONST_INT NOTICABLE_REASON_USING_ACTIVE_ROAMING_SPECTATOR_MODE	15

// make these const ints when done.
TWEAK_INT GANG_BUILDING_OUTER_BLIP_ALPHA 32


TWEAK_FLOAT PAUSE_MENU_SCALE_MUTLIPLIER 1.5

TWEAK_FLOAT LOCAL_NOISE_MUTIPLIER 0.3
TWEAK_INT 	LOCAL_NOISE_STORE_TIME 2000

ENUM AI_BLIP_TYPE
	AI_BLIP_DEFAULT
ENDENUM


STRUCT MISSION_OBJECTIVE_BLIP_STRUCT
	BOOL bHiddenForPartnerDuties
	BOOL bHasGPS
ENDSTRUCT

#IF IS_DEBUG_BUILD



/// PURPOSE:
///    Add widget options for blips.
PROC ADD_BLIP_WIDGETS()

	INT i
	TEXT_LABEL_63 str
	
	START_WIDGET_GROUP("Trasition from Displaced Interior")
		ADD_WIDGET_BOOL("g_bHideSwoopUpForInteriorsEnabled", g_bHideSwoopUpForInteriorsEnabled)	
		ADD_WIDGET_BOOL("g_bHideSwoopUpUntilPanDown", g_bHideSwoopUpUntilPanDown)
		ADD_WIDGET_BOOL("g_TransitionSessionNonResetVars.bInsideDisplacedInterior", g_TransitionSessionNonResetVars.bInsideDisplacedInterior)
		ADD_WIDGET_VECTOR_SLIDER("g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords", g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords, -99999.9, 99999.9, 0.1)
		ADD_WIDGET_INT_SLIDER("g_TransitionSessionNonResetVars.iDisplacedSimpleInterior", g_TransitionSessionNonResetVars.iDisplacedSimpleInterior, -1, 999, 1)
		
	STOP_WIDGET_GROUP()
	
	START_WIDGET_GROUP("Stagger")
		ADD_WIDGET_BOOL("g_bStaggeredConsidersEveryone", g_bStaggeredConsidersEveryone)
	STOP_WIDGET_GROUP()	

	START_WIDGET_GROUP("Blips")		
		ADD_WIDGET_BOOL("g_PlayerBlipsData.bDebugBlipOutput", g_PlayerBlipsData.bDebugBlipOutput)
		ADD_WIDGET_BOOL("g_PlayerBlipsData.bShowAllPlayerBlipsForDebug", g_PlayerBlipsData.bShowAllPlayerBlipsForDebug)
		ADD_WIDGET_BOOL("g_PlayerBlipsData.bShowWhoCanSeeMe", g_PlayerBlipsData.bShowWhoCanSeeMe)
		ADD_WIDGET_BOOL("g_PlayerBlipsData.bSpriteBlipPlayerIsInMyPersonalVehicle", g_PlayerBlipsData.bSpriteBlipPlayerIsInMyPersonalVehicle)
		ADD_WIDGET_BOOL("g_b_DisplayPrintsForRadarMap", g_b_DisplayPrintsForRadarMap)
		
		ADD_WIDGET_FLOAT_SLIDER("g_PlayerBlipsData.fTestRadarZoomDist", g_PlayerBlipsData.fTestRadarZoomDist, 0.0, 9999.9, 0.1)

		ADD_WIDGET_INT_SLIDER("g_FMMC_STRUCT.iTeamColourOverride[0]", g_FMMC_STRUCT.iTeamColourOverride[0], -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("g_FMMC_STRUCT.iTeamColourOverride[1]", g_FMMC_STRUCT.iTeamColourOverride[1], -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("g_FMMC_STRUCT.iTeamColourOverride[2]", g_FMMC_STRUCT.iTeamColourOverride[2], -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("g_FMMC_STRUCT.iTeamColourOverride[3]", g_FMMC_STRUCT.iTeamColourOverride[3], -1, 99, 1)
		
		START_WIDGET_GROUP("bitsets")	
			ADD_BIT_FIELD_WIDGET("bs_FadeOutBlip", g_PlayerBlipsData.bs_FadeOutBlip)
			ADD_BIT_FIELD_WIDGET("bs_PlayersToForceBlip", g_PlayerBlipsData.bs_PlayersToForceBlip)
			ADD_BIT_FIELD_WIDGET("bs_PlayersBlipIsLongRange", g_PlayerBlipsData.bs_PlayersBlipIsLongRange)
			ADD_BIT_FIELD_WIDGET("bs_PlayersBlipHasGPS", g_PlayerBlipsData.bs_PlayersBlipHasGPS)
			ADD_BIT_FIELD_WIDGET("bs_PlayerBlipSetToFlash", g_PlayerBlipsData.bs_PlayerBlipSetToFlash)
			ADD_BIT_FIELD_WIDGET("bs_PlayerBlipHighlighted", g_PlayerBlipsData.bs_PlayerBlipHighlighted)
			ADD_BIT_FIELD_WIDGET("bs_HighlightingAlways", g_PlayerBlipsData.bs_HighlightingAlways)
			ADD_BIT_FIELD_WIDGET("bs_FadeOutDeadBlip", g_PlayerBlipsData.bs_FadeOutDeadBlip)
			ADD_BIT_FIELD_WIDGET("bs_FadeOutWanted", g_PlayerBlipsData.bs_FadeOutWanted)
			ADD_BIT_FIELD_WIDGET("bs_FlashingAlways", g_PlayerBlipsData.bs_FlashingAlways)
			ADD_BIT_FIELD_WIDGET("bs_PulseBlip", g_PlayerBlipsData.bs_PulseBlip)
			ADD_BIT_FIELD_WIDGET("bs_BlipIsPulsing", g_PlayerBlipsData.bs_BlipIsPulsing)
			ADD_BIT_FIELD_WIDGET("bs_BlipCreatedThisFrame", g_PlayerBlipsData.bs_BlipCreatedThisFrame)
			ADD_BIT_FIELD_WIDGET("bs_PlayerUsingPropertyBlip", g_PlayerBlipsData.bs_PlayerUsingPropertyBlip)
			ADD_BIT_FIELD_WIDGET("bs_PlayerUsingIEWarehouseModShop", g_PlayerBlipsData.bs_PlayerUsingIEWarehouseModShop)
			ADD_BIT_FIELD_WIDGET("bs_PlayerBlipShouldFlash", g_PlayerBlipsData.bs_PlayerBlipShouldFlash)
			ADD_BIT_FIELD_WIDGET("bs_PlayerIndicatorSetToCrewColour", g_PlayerBlipsData.bs_PlayerIndicatorSetToCrewColour)
			ADD_BIT_FIELD_WIDGET("bs_PlayerIndicatorSetToFriend", g_PlayerBlipsData.bs_PlayerIndicatorSetToFriend)
			ADD_BIT_FIELD_WIDGET("bs_PlayerIndicatorOn", g_PlayerBlipsData.bs_PlayerIndicatorOn)
			ADD_BIT_FIELD_WIDGET("bs_BlipOutlineActive", g_PlayerBlipsData.bs_BlipOutlineActive)
			ADD_BIT_FIELD_WIDGET("bs_SetAsInvisibleVehiclePassenger", g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger)
			ADD_BIT_FIELD_WIDGET("bs_PlayerInVehicleThatHasBlip", g_PlayerBlipsData.bs_PlayerInVehicleThatHasBlip)
			ADD_BIT_FIELD_WIDGET("bs_IsUsingSmallVehicleBlip", g_PlayerBlipsData.bs_IsUsingSmallVehicleBlip)
			ADD_BIT_FIELD_WIDGET("bs_IgnoreSelfHighlightForPlayer", g_PlayerBlipsData.bs_IgnoreSelfHighlightForPlayer)
			ADD_BIT_FIELD_WIDGET("bs_DoResetForSecondaryData", g_PlayerBlipsData.bs_DoResetForSecondaryData)
			
			// the following don't get reset
			ADD_BIT_FIELD_WIDGET("bs_PlayerUsingCustomSprite", g_PlayerBlipsData.bs_PlayerUsingCustomSprite)
			ADD_BIT_FIELD_WIDGET("bs_PlayerUsingCustomColour", g_PlayerBlipsData.bs_PlayerUsingCustomColour)
			ADD_BIT_FIELD_WIDGET("bs_PlayerUsingCustomAlpha", g_PlayerBlipsData.bs_PlayerUsingCustomAlpha)
			ADD_BIT_FIELD_WIDGET("bs_PlayerUsingCustomNumber", g_PlayerBlipsData.bs_PlayerUsingCustomNumber)
			ADD_BIT_FIELD_WIDGET("bs_PlayerIsHidden", g_PlayerBlipsData.bs_PlayerIsHidden)				
			ADD_BIT_FIELD_WIDGET("bs_PlayerUsingFixedScale", g_PlayerBlipsData.bs_PlayerUsingFixedScale)
			ADD_BIT_FIELD_WIDGET("bs_PlayerSecondaryOutlinesDisabled", g_PlayerBlipsData.bs_PlayerSecondaryOutlinesDisabled)
			ADD_BIT_FIELD_WIDGET("bs_PlayerUsingCustomBlipName", g_PlayerBlipsData.bs_PlayerUsingCustomBlipName)
			
		STOP_WIDGET_GROUP()
		
		
		START_WIDGET_GROUP("Temp Short Range")
			ADD_WIDGET_INT_SLIDER("iTempShortRangeSpriteStagger", g_PlayerBlipsData.iTempShortRangeSpriteStagger, 0, 1000, 1)	
			//ADD_WIDGET_INT_SLIDER("iNumberTempShortRangeBlips", g_PlayerBlipsData.iNumberTempShortRangeBlips, 0, 1000, 1)	
			ADD_WIDGET_INT_SLIDER("iNumberTempMapDisplayOnlyBlips", g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips, 0, 1000, 1)
			ADD_WIDGET_INT_SLIDER("iNumberTempMapDisplayOnlyBlips_count", g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count, 0, 1000, 1)
			ADD_WIDGET_BOOL("bSetAllBlipsAsTempShortRangeCalled", g_PlayerBlipsData.bSetAllBlipsAsTempShortRangeCalled)	
			ADD_WIDGET_BOOL("bTempShortRange_InBigOrExtendedView", g_PlayerBlipsData.bTempShortRange_InBigOrExtendedView)	
		STOP_WIDGET_GROUP()
		

		ADD_WIDGET_INT_SLIDER("outer gang blip alpha", GANG_BUILDING_OUTER_BLIP_ALPHA, 0, 255, 1)
		ADD_WIDGET_BOOL("g_PlayerBlipsData.bHighlightSelf", g_PlayerBlipsData.bHighlightSelf)
		ADD_WIDGET_INT_SLIDER("BLIP_FLASHING_TIME", BLIP_FLASHING_TIME, 0, 1000, 1)
		ADD_WIDGET_INT_SLIDER("NOTICABLE_TIME", NOTICABLE_TIME, 0, 10000, 1)
		ADD_WIDGET_INT_SLIDER("SELF_NOTICE_TIME", SELF_NOTICE_TIME, 0, 10000, 1)
		ADD_WIDGET_FLOAT_SLIDER("NOTICABLE_RADIUS", NOTICABLE_RADIUS, 0.0, 1000.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("MIN_DIST_FOR_BLIP_SIZE", MIN_DIST_FOR_BLIP_SIZE, 0.0, 1000.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("MAX_DIST_FOR_BLIP_SIZE", MAX_DIST_FOR_BLIP_SIZE, 0.0, 10000.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("MIN_BLIP_SIZE", MIN_BLIP_SIZE, 0.0, 1.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("MIN_DIST_FOR_BLIP_ALPHA", MIN_DIST_FOR_BLIP_ALPHA, 0.0, 1000.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("MAX_DIST_FOR_BLIP_ALPHA", MAX_DIST_FOR_BLIP_ALPHA, 0.0, 10000.0, 1.0)
		ADD_WIDGET_INT_SLIDER("MIN_BLIP_ALPHA_FOR_DIST_FM", MIN_BLIP_ALPHA_FOR_DIST_FM, 0, 255, 1)
		ADD_WIDGET_INT_SLIDER("MIN_BLIP_ALPHA_FOR_DIST_ON_MISSION", MIN_BLIP_ALPHA_FOR_DIST_ON_MISSION, 0, 255, 1)
		
		ADD_WIDGET_FLOAT_SLIDER("BASE_TANK_BLIP_SCALE", BASE_TANK_BLIP_SCALE, 0.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("WEAPON_RANGE_MULTIPLIER_FOR_BLIPS", WEAPON_RANGE_MULTIPLIER_FOR_BLIPS, 0.0, 100.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("MIN_NOISE_DIST", MIN_NOISE_DIST, 0.0, 100.0, 0.001)
		ADD_WIDGET_INT_SLIDER("BLIP_FADE_PERIOD_FOR_FLASHING_WANTED_STARS", BLIP_FADE_PERIOD_FOR_FLASHING_WANTED_STARS, 0, 30000,1)
		ADD_WIDGET_BOOL("bHideAllPlayerBlips", g_PlayerBlipsData.bHideAllPlayerBlips)
		ADD_WIDGET_FLOAT_SLIDER("PLAYER_ARROW_BLIP_SCALE", PLAYER_ARROW_BLIP_SCALE, 0.0, 2.0, 0.001)
		ADD_WIDGET_INT_SLIDER("g_PlayerBlipsData.iLocalPlayerID", g_PlayerBlipsData.iLocalPlayerID, -1, HIGHEST_INT, 1)
		ADD_WIDGET_BOOL("g_PlayerBlipsData.bSetupForPauseMenu", g_PlayerBlipsData.bSetupForPauseMenu)
		ADD_WIDGET_BOOL("g_PlayerBlipsData.bTestIncarNumbers", g_PlayerBlipsData.bTestIncarNumbers)
		ADD_WIDGET_INT_SLIDER(" VISIBLE_HEIGHT", VISIBLE_HEIGHT, -1, 200, 1)
		ADD_WIDGET_INT_SLIDER("g_PlayerBlipsData.iReturnPlayerArrowColour", g_PlayerBlipsData.iReturnPlayerArrowColour, -1, 4, 1)
		ADD_WIDGET_BOOL("g_PlayerBlipsData.bTestBlipStroke", g_PlayerBlipsData.bTestBlipStroke)
		
		START_WIDGET_GROUP("Scale")
			ADD_WIDGET_FLOAT_SLIDER("BLIP_SCALE_MULTIPLIER_FOR_NON_STANDARD_BLIPS", BLIP_SCALE_MULTIPLIER_FOR_NON_STANDARD_BLIPS, 0.0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("HIGHLIGHTED_SCALE_UP", HIGHLIGHTED_SCALE_UP, 0.0, 2.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("RACE_SCALE_UP", RACE_SCALE_UP, 0.0, 2.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("PAUSE_MENU_SCALE_MUTLIPLIER", PAUSE_MENU_SCALE_MUTLIPLIER, 0.0, 2.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("BLIP_SCALE_MULTIPLIER_FOR_HIDDEN_PAUSE_BLIPS", BLIP_SCALE_MULTIPLIER_FOR_HIDDEN_PAUSE_BLIPS, 0.0, 1.0, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Alpha")
			ADD_WIDGET_INT_SLIDER("BLIP_ALPHA_FOR_PASSENGERS", BLIP_ALPHA_FOR_PASSENGERS, 0, 255, 1)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Custom Blips for Players")
		
			ADD_WIDGET_BOOL("bBypassThreadChecks", g_PlayerBlipsData.bBypassThreadChecks)
		
			START_WIDGET_GROUP("Sprites")
				ADD_BIT_FIELD_WIDGET("bs_PlayerUsingCustomSprite", g_PlayerBlipsData.bs_PlayerUsingCustomSprite)
				REPEAT NUM_NETWORK_PLAYERS i
					str = "iPlayerCustomSprite["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, g_PlayerBlipsData.iPlayerCustomSprite[i], 0, ENUM_TO_INT(MAX_RADAR_TRACES), 1)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Blip Name")
				ADD_BIT_FIELD_WIDGET("bs_PlayerUsingCustomBlipName", g_PlayerBlipsData.bs_PlayerUsingCustomBlipName)
				REPEAT NUM_NETWORK_PLAYERS i
					str = "strCustomBlipName["
					str += i
					str += "] = "
					str += g_PlayerBlipsData.strCustomBlipName[i]
					ADD_WIDGET_STRING(str)
					str = "bCustomBlipNameIsLiteral["
					str += i
					str += "]"
					ADD_WIDGET_BOOL(str, g_PlayerBlipsData.bCustomBlipNameIsLiteral[i])
				ENDREPEAT	
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Colour")
				ADD_BIT_FIELD_WIDGET("bs_PlayerUsingCustomColour", g_PlayerBlipsData.bs_PlayerUsingCustomColour)
				REPEAT NUM_NETWORK_PLAYERS i
					str = "iCustomBlipColour["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, g_PlayerBlipsData.iCustomBlipColour[i], LOWEST_INT, HIGHEST_INT, 1)
				ENDREPEAT				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Alpha")
				ADD_BIT_FIELD_WIDGET("bs_PlayerUsingCustomAlpha", g_PlayerBlipsData.bs_PlayerUsingCustomAlpha)
				REPEAT NUM_NETWORK_PLAYERS i
					str = "iCustomBlipAlpha["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, g_PlayerBlipsData.iCustomBlipAlpha[i], 0, 255, 1)
				ENDREPEAT				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Number")
				ADD_BIT_FIELD_WIDGET("bs_PlayerUsingCustomNumber", g_PlayerBlipsData.bs_PlayerUsingCustomNumber)
				REPEAT NUM_NETWORK_PLAYERS i
					str = "iCustomBlipNumber["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, g_PlayerBlipsData.iCustomBlipNumber[i], 0, 16, 1)
				ENDREPEAT				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Hidden")
				ADD_BIT_FIELD_WIDGET("bs_PlayerIsHidden", g_PlayerBlipsData.bs_PlayerIsHidden)	
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Fixed Scale")
				ADD_BIT_FIELD_WIDGET("bs_PlayerUsingFixedScale", g_PlayerBlipsData.bs_PlayerUsingFixedScale)
				REPEAT NUM_NETWORK_PLAYERS i
					str = "fPlayerFixedScale["
					str += i
					str += "]"
					ADD_WIDGET_FLOAT_SLIDER(str, g_PlayerBlipsData.fPlayerFixedScale[i], 0.0, 10.0, 0.01)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Disable Secondary Outlines")
				ADD_BIT_FIELD_WIDGET("bs_PlayerSecondaryOutlinesDisabled", g_PlayerBlipsData.bs_PlayerSecondaryOutlinesDisabled)	
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()


		START_WIDGET_GROUP("Test Blip")
			ADD_WIDGET_BOOL("g_PlayerBlipsData.bDrawTestBlip", g_PlayerBlipsData.bDrawTestBlip)
			ADD_WIDGET_INT_SLIDER("g_PlayerBlipsData.iTestBlipSprite", g_PlayerBlipsData.iTestBlipSprite, 0, ENUM_TO_INT(MAX_RADAR_TRACES), 1)
			g_PlayerBlipsData.twTestBlipSprite = ADD_TEXT_WIDGET("g_PlayerBlipsData.twTestBlipSprite")
			SET_CONTENTS_OF_TEXT_WIDGET(g_PlayerBlipsData.twTestBlipSprite, "")
			ADD_WIDGET_INT_SLIDER("g_PlayerBlipsData.iTestBlipRotation", g_PlayerBlipsData.iTestBlipRotation, 0, 360, 1)
			ADD_WIDGET_FLOAT_SLIDER("g_PlayerBlipsData.fTestBlipScale", g_PlayerBlipsData.fTestBlipScale, 0.0, 4.0, 0.01)
			ADD_WIDGET_BOOL("g_PlayerBlipsData.bTestSecondaryColour", g_PlayerBlipsData.bTestSecondaryColour)
			ADD_WIDGET_INT_SLIDER("g_PlayerBlipsData.iTestR", g_PlayerBlipsData.iTestR, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g_PlayerBlipsData.iTestG", g_PlayerBlipsData.iTestG, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("g_PlayerBlipsData.iTestB", g_PlayerBlipsData.iTestB, 0, 255, 1)	
			ADD_WIDGET_INT_SLIDER("g_PlayerBlipsData.iTestBlipColour", g_PlayerBlipsData.iTestBlipColour, 0, 100, 1)	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Highlighting")
			ADD_BIT_FIELD_WIDGET("bs_PlayerBlipHighlighted", g_PlayerBlipsData.bs_PlayerBlipHighlighted)
//			REPEAT NUM_NETWORK_PLAYERS i
//				str = "g_PlayerBlipsData.iBlipTimerStopHighlighting["
//				str += i
//				str += "]"			
//				ADD_WIDGET_INT_SLIDER(str, g_PlayerBlipsData.iBlipTimerStopHighlighting[i], -1, HIGHEST_INT, 1)
//			ENDREPEAT	
		STOP_WIDGET_GROUP()
		
		// broadcast data
		START_WIDGET_GROUP("Broadcast Data")
			REPEAT NUM_NETWORK_PLAYERS i
				
				str = "Player "
				str +=  i
				
				START_WIDGET_GROUP(str)
					ADD_WIDGET_INT_SLIDER("iFlags", GlobalplayerBD[i].playerBlipData.iFlags, LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("iInVehicleType", GlobalplayerBD[i].playerBlipData.iInVehicleType, LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("iBS_BlippedPlayers", GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayers, LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("iNumberOfPlayersInMyCar", GlobalplayerBD[i].playerBlipData.iNumberOfPlayersInMyCar, 0, NUM_NETWORK_PLAYERS, 1)
					ADD_BIT_FIELD_WIDGET("iBS_Friends", GlobalplayerBD[i].iBS_Friends)
					
					ADD_WIDGET_INT_SLIDER("iDisplacedInteriorID", GlobalplayerBD[i].playerBlipData.iDisplacedInteriorID, LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_VECTOR_SLIDER("vDisplacedInteriorPos", GlobalplayerBD[i].playerBlipData.vDisplacedInteriorPos, -999999.9, 9999999.9, 0.001)					
					ADD_WIDGET_FLOAT_SLIDER("fDisplacedInteriorHeading", GlobalplayerBD[i].playerBlipData.fDisplacedInteriorHeading, -999999.9, 9999999.9, 0.001)
					
					ADD_WIDGET_BOOL("bInsideDisplacedInterior", GlobalplayerBD[i].playerBlipData.bInsideDisplacedInterior)
					ADD_WIDGET_VECTOR_SLIDER("vDisplacedPerceivedCoords", GlobalplayerBD[i].playerBlipData.vDisplacedPerceivedCoords, -999999.9, 9999999.9, 0.001)
					
				STOP_WIDGET_GROUP()
				
			ENDREPEAT	
		STOP_WIDGET_GROUP()
		
		// g_PlayerBlipsData.bShowWhoCanSeeMe window
		START_WIDGET_GROUP("g_PlayerBlipsData.bShowWhoCanSeeMe window")
			ADD_WIDGET_BOOL("g_PlayerBlipsData.bShowWhoCanSeeMe", g_PlayerBlipsData.bShowWhoCanSeeMe)
			ADD_WIDGET_FLOAT_SLIDER("WindowX", g_PlayerBlipsData.WindowX, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WindowY", g_PlayerBlipsData.WindowY, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WindowTextScale", g_PlayerBlipsData.WindowTextScale, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WindowW", g_PlayerBlipsData.WindowW, -1.0, 1.0, 0.001)
			//ADD_WIDGET_FLOAT_SLIDER("WindowH", g_PlayerBlipsData.WindowH, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WindowColumnX", g_PlayerBlipsData.WindowColumnX, -1.0, 1.0, 0.001)
			//ADD_WIDGET_FLOAT_SLIDER("WindowRowY", g_PlayerBlipsData.WindowRowY, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WindowSpacerY", g_PlayerBlipsData.WindowSpacerY, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WindowXIndent", g_PlayerBlipsData.WindowXIndent, -1.0, 1.0, 0.001)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Same Session")
			ADD_BIT_FIELD_WIDGET("iBS_WereInSameSession", MPGlobals.iBS_WereInSameSession)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("SHOW_ALL_PLAYER_BLIPS")			
			ADD_WIDGET_BOOL("Input", g_PlayerBlipsData.bShowAllSet)
			ADD_WIDGET_BOOL("Call", g_PlayerBlipsData.bCallShowAll)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR")
			//ADD_WIDGET_BOOL("Test", g_PlayerBlipsData.bTest_SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR)	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Local Player Noise")
			ADD_WIDGET_FLOAT_SLIDER("LOCAL_NOISE_MUTIPLIER", LOCAL_NOISE_MUTIPLIER, 0.0, 1.0, 0.001)
			ADD_WIDGET_INT_SLIDER("LOCAL_NOISE_STORE_TIME", LOCAL_NOISE_STORE_TIME,	0, HIGHEST_INT, 1)
			ADD_WIDGET_FLOAT_SLIDER("g_PlayerBlipsData.fMyLoudNoise", g_PlayerBlipsData.fMyLoudNoise, -999999.9, 999999.9, 0.001)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Interior faking")
			ADD_WIDGET_BOOL("g_PlayerBlipsData.bBigMapIsActive", g_PlayerBlipsData.bBigMapIsActive)
			ADD_WIDGET_BOOL("g_PlayerBlipsData.bMiniMapPositionLocked", g_PlayerBlipsData.bMiniMapPositionLocked)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Disable stuff")
			ADD_WIDGET_BOOL("bDisableHeadingIndicator", g_PlayerBlipsData.bDisableHeadingIndicator)
			ADD_WIDGET_BOOL("bDisablePriority", g_PlayerBlipsData.bDisablePriority)
			ADD_WIDGET_BOOL("bDisableSecondaryBlipColour", g_PlayerBlipsData.bDisableSecondaryBlipColour)
			ADD_WIDGET_BOOL("bDisableShowOutline", g_PlayerBlipsData.bDisableShowOutline)
			ADD_WIDGET_BOOL("bDisableShowCrew", g_PlayerBlipsData.bDisableShowCrew)
			ADD_WIDGET_BOOL("bDisableShowFriend", g_PlayerBlipsData.bDisableShowFriend)
			ADD_WIDGET_BOOL("bDisableSetScaleAlpha", g_PlayerBlipsData.bDisableSetScaleAlpha)
			ADD_WIDGET_BOOL("bDisableResetSecondaryData", g_PlayerBlipsData.bDisableResetSecondaryData)
			ADD_WIDGET_BOOL("bDisableScale", g_PlayerBlipsData.bDisableScale)
			ADD_WIDGET_BOOL("bDisableAlpha", g_PlayerBlipsData.bDisableAlpha)
			ADD_WIDGET_BOOL("bDisableHeightIndicators", g_PlayerBlipsData.bDisableHeightIndicators)
			ADD_WIDGET_BOOL("bDisableUpdateFlashing", g_PlayerBlipsData.bDisableUpdateFlashing)
			ADD_WIDGET_BOOL("bDisableUpdateHighlight", g_PlayerBlipsData.bDisableUpdateHighlight)
			ADD_WIDGET_BOOL("bDisableUpdatePulsing", g_PlayerBlipsData.bDisableUpdatePulsing)
			ADD_WIDGET_BOOL("bDisableSetBlipDisplay", g_PlayerBlipsData.bDisableSetBlipDisplay)
			ADD_WIDGET_BOOL("bDisableShowNumberOnBlip", g_PlayerBlipsData.bDisableShowNumberOnBlip)
			ADD_WIDGET_BOOL("bDisableSetBlipColour", g_PlayerBlipsData.bDisableSetBlipColour)		
			ADD_WIDGET_BOOL("bBlockAllPlayerBlips", g_PlayerBlipsData.bBlockAllPlayerBlips)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()		
ENDPROC
#ENDIF

//Call every fram to hide the player blip
PROC HIDE_PLAYER_ARROW_THIS_FRAME()
	PRINTLN("HIDE_PLAYER_ARROW_THIS_FRAME called")
	DEBUG_PRINTCALLSTACK()	
	g_bHidePlayerArrowThisFrame = TRUE
ENDPROC

//Check to see if we should hide the players blip
FUNC BOOL SHOULD_PLAYER_ARROW_BE_HIDDEN_THIS_FRAME()
	IF g_bHidePlayerArrowThisFrame
		g_bHidePlayerArrowThisFrame = FALSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC 


FUNC BOOL IS_PLAYER_PASSIVE(PLAYER_INDEX PlayerID)

//		IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID))
//			RETURN(FALSE)
//		ENDIF

	IF (PlayerID = PLAYER_ID())
		RETURN IS_MP_PASSIVE_MODE_ENABLED() 
	ENDIF	
	RETURN IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset[GET_WEAPON_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE))][NATIVE_TO_INT(PlayerID)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE)))
ENDFUNC

FUNC BOOL IS_PLAYER_ON_PAUSE_MENU(PLAYER_INDEX PlayerID)
	RETURN IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset[GET_WEAPON_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PAUSING))][NATIVE_TO_INT(PlayerID)], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PAUSING)))
ENDFUNC

PROC RESTORE_BLIP_ALPHA(BLIP_INDEX &BlipID)
	IF DOES_BLIP_EXIST(BlipID)
		SET_BLIP_FADE(BlipID, 255, 0)						
		SET_BLIP_ALPHA(BlipID,255)	
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				NET_PRINT("[player_blips] RESTORE_BLIP_ALPHA")
				NET_PRINT_INT(NATIVE_TO_INT(BlipID))
				NET_NL()
			ENDIF
		#ENDIF					
	ENDIF
ENDPROC



/// PURPOSE:
///    Removes a blip and sets its id to null.
/// PARAMS:
///    BlipId - Blip id.
PROC CLEANUP_BLIP(BLIP_INDEX &BlipId, BOOL bSetToNull=TRUE)
	IF DOES_BLIP_EXIST(BlipId)		
		SET_BLIP_ROUTE(BlipId, FALSE)
		REMOVE_BLIP(BlipId)
	ENDIF
	IF (bSetToNull)
		BlipId = NULL
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes all blips in array and sets its ids to null.
/// PARAMS:
///    BlipId - Blip id.
PROC CLEANUP_BLIPS(BLIP_INDEX &blipIds[], BOOL bSetToNull=TRUE)
	INT i
	REPEAT COUNT_OF(blipIds) i
		CLEANUP_BLIP(blipIds[i], bSetToNull)
	ENDREPEAT
ENDPROC


PROC CLEANUP_FM_BLIPS()
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayer])
			REMOVE_BLIP(g_PlayerBlipsData.playerBlips[iPlayer])
		ENDIF			
	ENDREPEAT
ENDPROC

FUNC BOOL IS_MP_PLAYER_BLIPPED(PLAYER_INDEX PlayerID)	
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)])
		IF (GET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)]) = 255)
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC


PROC CLEANUP_PLAYER_INT_BLIP(INT iPlayerIDInt)
	
	CLEAR_BIT(g_PlayerBlipsData.bS_FadeOutBlip, iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayersToForceBlip, iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayersBlipIsLongRange, iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayersBlipHasGPS,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerBlipSetToFlash,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerBlipHighlighted,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_FadeOutDeadBlip,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_FadeOutWanted,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_HighlightingAlways,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_FlashingAlways,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PulseBlip,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_BlipIsPulsing,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_BlipCreatedThisFrame,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerBlipShouldFlash,iPlayerIDInt)	
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerIndicatorSetToCrewColour,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerIndicatorSetToFriend,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_BlipOutlineActive, iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerIndicatorOn,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingPropertyBlip,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingIEWarehouseModShop,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_IgnoreSelfHighlightForPlayer,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger,iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerInVehicleThatHasBlip, iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_IsUsingSmallVehicleBlip, iPlayerIDInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_DoResetForSecondaryData, iPlayerIDInt)	
	
//	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
//		// broadcast to everyone that I'm no longer blipping this player
//		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayers, iPlayerIDInt)
//	ENDIF
	
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])	
		SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[iPlayerIDInt], 0)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] CLEANUP_PLAYER_INT_BLIP - SET_BLIP_ALPHA - blip set to alpha 0 for player ")
				NET_PRINT_INT(iPlayerIDInt)
				NET_NL()
			ENDIF
		#ENDIF
		CLEANUP_BLIP( g_PlayerBlipsData.playerBlips[iPlayerIDInt])
	ENDIF
	

ENDPROC

PROC CLEANUP_PLAYER_BLIP(PLAYER_INDEX playerID)
	CLEANUP_PLAYER_INT_BLIP(NATIVE_TO_INT(playerID))
ENDPROC


/// PURPOSE:
///    Sets the player blip to have gps to it
PROC SET_PLAYER_BLIP_HAS_GPS(PLAYER_INDEX playerID, BOOL bHasGPS)
	
	IF (PlayerID = INVALID_PLAYER_INDEX())
		SCRIPT_ASSERT("SET_PLAYER_BLIP_HAS_GPS - passed in invalid player index.")
		EXIT
	ENDIF	

	IF (bHasGPS)
		SET_BIT(g_PlayerBlipsData.bs_PlayersBlipHasGPS, NATIVE_TO_INT(playerId))
	ELSE
		CLEAR_BIT(g_PlayerBlipsData.bs_PlayersBlipHasGPS, NATIVE_TO_INT(playerId))
	ENDIF
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerID)])
		CPRINTLN(DEBUG_BLIP, "[player_blips] SET_PLAYER_BLIP_HAS_GPS - setting to true")
		SET_BLIP_ROUTE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerID)], bHasGPS)
		IF (bHasGPS = TRUE)
			SET_BLIP_ROUTE_COLOUR(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerID)], GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_PLAYER_CURRENT_HUD_COLOUR(PlayerID)) )
		ENDIF
	ENDIF
ENDPROC

/// True if the player should have a gps route to them, false otherwise. 
FUNC BOOL IS_PLAYER_BLIP_GPS_TURNED_ON(PLAYER_INDEX playerId)
	IF IS_NET_PLAYER_OK(playerId)
		RETURN IS_BIT_SET(g_PlayerBlipsData.bs_PlayersBlipHasGPS, NATIVE_TO_INT(playerId))
	ENDIF	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Sets the blip colour based on RGB values.
/// PARAMS:
///    blipID - A valid blip.
///    iRed - Red component
///    iGreen - Green component
///    iBlue - Blue component
PROC SET_SECONDARY_BLIP_COLOUR(BLIP_INDEX &blipID, INT iRed, INT iGreen, INT iBlue)
	
	// The multiplications below are just stuffing the 8bit rbg values into a single int. 
	// You could do the same by settings bits within a single int.  
	// SET_BITS_IN_RANGE(iFinalValue, 24, 31, iRed)
	// SET_BITS_IN_RANGE(iFinalValue, 16, 23, iGreen)
	// SET_BITS_IN_RANGE(iFinalValue, 8,  15, iBlue)
	// SET_BITS_IN_RANGE(iFinalValue, 0,  7,  255) // Alpha
	// SET_BLIP_COLOUR(blipID, iFinalValue )
	
	CPRINTLN(DEBUG_BLIP, "[player_blips] SET_SECONDARY_BLIP_COLOUR called with ", iRed, ", ", iGreen, ", ", iBlue)
	
	IF DOES_BLIP_EXIST(blipID)
		#IF IS_DEBUG_BUILD
			IF NOT (g_PlayerBlipsData.bDisableSecondaryBlipColour)
		#ENDIF
		SET_BLIP_SECONDARY_COLOUR(blipID, iRed, iGreen, iBlue)  //(iRed * 16777216) + (iGreen * 65536) + (iBlue * 256) + (255) )
		#IF IS_DEBUG_BUILD
			ENDIF
		#ENDIF
	ELSE
		NET_SCRIPT_ASSERT("SET_SECONDARY_BLIP_COLOUR Blip does not exist!")
	ENDIF

ENDPROC

PROC APPLY_SECONDARY_COLOUR_TO_BLIP(BLIP_INDEX &blipID, INT iRed, INT iGreen, INT iBlue)
	SET_SECONDARY_BLIP_COLOUR(blipID, iRed, iGreen, iBlue) 	// this must be called first
	#IF IS_DEBUG_BUILD
		IF NOT (g_PlayerBlipsData.bDisableShowOutline)
	#ENDIF
	SHOW_OUTLINE_INDICATOR_ON_BLIP(blipID, TRUE)				// this must be called second
	#IF IS_DEBUG_BUILD
		ENDIF
	#ENDIF
ENDPROC

PROC RESET_SECONDARY_DATA_FOR_PLAYER_BLIP(INT iPlayerIDInt)
	DEBUG_PRINTCALLSTACK()
	SET_BIT(g_PlayerBlipsData.bs_DoResetForSecondaryData, iPlayerIDInt)	
ENDPROC

PROC CLEAR_SECONDARY_COLOUR_FROM_BLIP(BLIP_INDEX &blipID)
	#IF IS_DEBUG_BUILD
		IF NOT (g_PlayerBlipsData.bDisableShowOutline)
	#ENDIF
	SHOW_OUTLINE_INDICATOR_ON_BLIP(blipID, FALSE)	
	#IF IS_DEBUG_BUILD
		ENDIF
	#ENDIF
	CPRINTLN(DEBUG_BLIP, "[player_blips] CLEAR_SECONDARY_COLOUR_FROM_BLIP called")
ENDPROC

PROC STORE_SECONDARY_BLIP_COLOUR_FOR_PLAYER(INT iPlayerIDInt, INT iR, INT iG, INT iB)
	g_PlayerBlipsData.iSecondaryBlipColourR[iPlayerIDInt] = iR
	g_PlayerBlipsData.iSecondaryBlipColourG[iPlayerIDInt] = iG
	g_PlayerBlipsData.iSecondaryBlipColourB[iPlayerIDInt] = iB
ENDPROC


// **********************************************************************************************************
// 		outline blips
// **********************************************************************************************************
PROC SET_OUTLINE_BLIP_COLOUR_FOR_PLAYER(INT iPlayerIDInt, INT iRed, INT iGreen, INT iBlue)
	CPRINTLN(DEBUG_BLIP, "[player_blips] SET_OUTLINE_BLIP_COLOUR_FOR_PLAYER - ", iPlayerIDInt, iRed, iGreen, iBlue)
	STORE_SECONDARY_BLIP_COLOUR_FOR_PLAYER(iPlayerIDInt, iRed, iGreen, iBlue)
	APPLY_SECONDARY_COLOUR_TO_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], iRed, iGreen, iBlue)
	SET_BIT(g_PlayerBlipsData.bs_BlipOutlineActive, iPlayerIDInt)
ENDPROC

PROC CLEAR_OUTLINE_BLIP_COLOUR_FOR_PLAYER(INT iPlayerIDInt)
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
		CLEAR_SECONDARY_COLOUR_FROM_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
	ENDIF
	CLEAR_BIT(g_PlayerBlipsData.bs_BlipOutlineActive, iPlayerIDInt)
	RESET_SECONDARY_DATA_FOR_PLAYER_BLIP(iPlayerIDInt)
	CPRINTLN(DEBUG_BLIP, "[player_blips] CLEAR_OUTLINE_BLIP_COLOUR_FOR_PLAYER called, player ", iPlayerIDInt)
ENDPROC

FUNC BOOL IS_OUTLINE_BLIP_ACTIVE_FOR_PLAYER(INT iPlayerIDInt)
	RETURN IS_BIT_SET(g_PlayerBlipsData.bs_BlipOutlineActive, iPlayerIDInt)
ENDFUNC


// **********************************************************************************************************
// 		Crew blips
// **********************************************************************************************************
PROC SET_CREW_BLIP_COLOUR_FOR_PLAYER(INT iPlayerIDInt, INT iR, INT iG, INT iB)
	IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_BlipOutlineActive, iPlayerIDInt)
		STORE_SECONDARY_BLIP_COLOUR_FOR_PLAYER(iPlayerIDInt, iR, iG, iB)	
		SET_SECONDARY_BLIP_COLOUR(g_PlayerBlipsData.playerBlips[iPlayerIDInt], iR, iG, iB) 	// this must be called first
		#IF IS_DEBUG_BUILD
			IF NOT (g_PlayerBlipsData.bDisableShowCrew)
		#ENDIF
			SHOW_CREW_INDICATOR_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], TRUE)
		#IF IS_DEBUG_BUILD
			ENDIF
		#ENDIF
		SET_BIT(g_PlayerBlipsData.bs_PlayerIndicatorSetToCrewColour, iPlayerIDInt)
		CPRINTLN(DEBUG_BLIP, "[player_blips] SET_CREW_BLIP_COLOUR_FOR_PLAYER - setting for player ", iPlayerIDInt, ", ", iR, ", ", iG, ", ", iB)
	ELSE
		CPRINTLN(DEBUG_BLIP, "[player_blips] SET_CREW_BLIP_COLOUR_FOR_PLAYER - trying to set crew blip colour but outline is still active")
		SCRIPT_ASSERT("SET_CREW_BLIP_COLOUR_FOR_PLAYER - trying to set crew blip colour but outline is still active")
	ENDIF
ENDPROC

PROC CLEAR_CREW_BLIP_COLOUR_FOR_PLAYER(INT iPlayerIDInt)
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
		CLEAR_SECONDARY_COLOUR_FROM_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
		#IF IS_DEBUG_BUILD
			IF NOT (g_PlayerBlipsData.bDisableShowCrew)
		#ENDIF
		SHOW_CREW_INDICATOR_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], FALSE)
		#IF IS_DEBUG_BUILD
			ENDIF
		#ENDIF
	ENDIF
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerIndicatorSetToCrewColour, iPlayerIDInt)
	RESET_SECONDARY_DATA_FOR_PLAYER_BLIP(iPlayerIDInt)
	CPRINTLN(DEBUG_BLIP, "[player_blips] CLEAR_CREW_BLIP_COLOUR_FOR_PLAYER called, player ", iPlayerIDInt)
ENDPROC

FUNC BOOL IS_CREW_BLIP_ACTIVE_FOR_PLAYER(INT iPlayerIDInt)
	RETURN IS_BIT_SET(g_PlayerBlipsData.bs_PlayerIndicatorSetToCrewColour, iPlayerIDInt)	
ENDFUNC

// **********************************************************************************************************
// 		friend blips
// **********************************************************************************************************
PROC SET_FRIEND_BLIP_ACTIVE_FOR_PLAYER(INT iPlayerIDInt, BOOL bSet)
	IF (bSet)
		IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_BlipOutlineActive, iPlayerIDInt)
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisableShowFriend)
			#ENDIF
			SHOW_FRIEND_INDICATOR_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], TRUE)
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
			SET_BIT(g_PlayerBlipsData.bs_PlayerIndicatorSetToFriend, iPlayerIDInt)
			CPRINTLN(DEBUG_BLIP, "[player_blips] SET_FRIEND_BLIP_ACTIVE_FOR_PLAYER - set friend blip colour player ", iPlayerIDInt)
		ELSE
			CPRINTLN(DEBUG_BLIP, "[player_blips] SET_FRIEND_BLIP_ACTIVE_FOR_PLAYER - trying to set friend blip colour but outline is still active")
			SCRIPT_ASSERT("SET_FRIEND_BLIP_ACTIVE_FOR_PLAYER - trying to set friend blip colour but outline is still active")
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisableShowFriend)
			#ENDIF
			SHOW_FRIEND_INDICATOR_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], FALSE)
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
		ENDIF	
		CLEAR_BIT(g_PlayerBlipsData.bs_PlayerIndicatorSetToFriend, iPlayerIDInt)
		RESET_SECONDARY_DATA_FOR_PLAYER_BLIP(iPlayerIDInt)
		CPRINTLN(DEBUG_BLIP, "[player_blips] SET_FRIEND_BLIP_ACTIVE_FOR_PLAYER - cleared friend blip colour player ", iPlayerIDInt)
	ENDIF
ENDPROC


FUNC BOOL IS_FRIEND_BLIP_ACTIVE_FOR_PLAYER(INT iPlayerIDInt)
	RETURN IS_BIT_SET(g_PlayerBlipsData.bs_PlayerIndicatorSetToFriend, iPlayerIDInt)	
ENDFUNC

// **********************************************************************************************************
// 		general secondary blip stuff
// **********************************************************************************************************
PROC CLEAR_SECONDARY_BLIP_COLOUR_FOR_PLAYER(INT iPlayerIDInt)
	CPRINTLN(DEBUG_BLIP, "[player_blips] CLEAR_SECONDARY_BLIP_COLOUR_FOR_PLAYER called for player ", iPlayerIDInt)
	CLEAR_OUTLINE_BLIP_COLOUR_FOR_PLAYER(iPlayerIDInt)
	CLEAR_CREW_BLIP_COLOUR_FOR_PLAYER(iPlayerIDInt)
	SET_FRIEND_BLIP_ACTIVE_FOR_PLAYER(iPlayerIDInt, FALSE)
ENDPROC

FUNC HUD_COLOURS GET_HUD_COLOUR_FROM_BLIP_COLOUR(INT iBlipColour)

	HUD_COLOURS ThisHudColour
	INT iThisBlipColour
	INT i
	
	
	REPEAT COUNT_OF(HUD_COLOURS) i
		ThisHudColour = INT_TO_ENUM(HUD_COLOURS, i)
		iThisBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(ThisHudColour)
		IF (iThisBlipColour = iBlipColour)
			RETURN ThisHudColour
		ENDIF
	ENDREPEAT	
	
	RETURN INT_TO_ENUM(HUD_COLOURS, -1)

ENDFUNC

PROC SET_BLIP_COLOUR_FROM_HUD_COLOUR(BLIP_INDEX &blipID, HUD_COLOURS HudColour)
	IF DOES_BLIP_EXIST(blipID)	
	
		#IF IS_DEBUG_BUILD
			IF NOT (g_PlayerBlipsData.bSetPlayerColourCalled)			
				IF (GET_BLIP_INFO_ID_TYPE(blipID) = BLIPTYPE_CHAR)			
					IF IS_ENTITY_A_PED(GET_BLIP_INFO_ID_ENTITY_INDEX(blipID))
						PED_INDEX PlayerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_BLIP_INFO_ID_ENTITY_INDEX(blipID))
						IF DOES_ENTITY_EXIST(PlayerPed)
							IF IS_PED_A_PLAYER(PlayerPed)
							AND NOT (NETWORK_GET_PLAYER_INDEX_FROM_PED(PlayerPed) = MPGlobals.LocalPlayerID)
								ASSERTLN("[player_blips] SET_BLIP_COLOUR_FROM_HUD_COLOUR - trying to change a players blip colour outside of blipping system, the will cause conflicts, speak to Neil F.")
								PRINTLN("[player_blips] SET_BLIP_COLOUR_FROM_HUD_COLOUR - trying to change a players blip colour outside of blipping system, the will cause conflicts, speak to Neil F.")
								DEBUG_PRINTCALLSTACK()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
	
		
		#IF IS_DEBUG_BUILD
			INT iCurrentBlipColour = GET_BLIP_COLOUR(blipID)
			//HUD_COLOURS CurrentHudColour = GET_HUD_COLOUR_FROM_BLIP_COLOUR(iCurrentBlipColour)
		#ENDIF
		INT iNewBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HudColour)
		
		#IF IS_DEBUG_BUILD
			IF NOT (iCurrentBlipColour = iNewBlipColour)
				PRINTLN("SET_BLIP_COLOUR_FROM_HUD_COLOUR - changing blip id ", NATIVE_TO_INT(blipID), " colour from ", iCurrentBlipColour, " to ", iNewBlipColour) //" (hud colour ", ENUM_TO_INT(CurrentHudColour), ")")
				DEBUG_PRINTCALLSTACK()
			ENDIF
		#ENDIF
		
		SET_BLIP_COLOUR(blipID, iNewBlipColour)
			
	ELSE
		NET_SCRIPT_ASSERT("SET_BLIP_COLOUR_FROM_HUD_COLOUR Blip does not exist!")
	ENDIF
ENDPROC

//Call every frame
PROC SET_BLIP_FLASHES_BETWEEN_COLOURS(BLIP_INDEX blip, HUD_COLOURS colour1, HUD_COLOURS colour2,SCRIPT_TIMER &flashTimer,INT iFlashTime = 500)

	IF DOES_BLIP_EXIST(blip)

		IF HAS_NET_TIMER_EXPIRED(flashTimer,iFlashTime)

			IF GET_BLIP_COLOUR_FROM_HUD_COLOUR(colour1) = GET_BLIP_COLOUR(blip)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(Blip, colour2)
			ELSE
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(Blip, colour1)
			ENDIF

			RESET_NET_TIMER(flashTimer)
		ENDIF
	
	ENDIF

ENDPROC


FUNC BOOL IS_SHIPMENT_BLIP_FLASH_ACTIVE()
	RETURN g_PlayerBlipsData.bDisplayShipmentBlip
ENDFUNC

PROC SET_DISPLAY_SHIPMENT_FLASH_ACTIVE(BOOL bBool)
	IF NOT (bBool = g_PlayerBlipsData.bDisplayShipmentBlip)
		CPRINTLN(DEBUG_BLIP, "[player_blips] SET_DISPLAY_SHIPMENT_FLASH_ACTIVE, changing g_PlayerBlipsData.bDisplayShipmentBlip to ", bBool)
		//DEBUG_PRINTCALLSTACK()				
	 	g_PlayerBlipsData.bDisplayShipmentBlip = bBool
	ENDIF	
ENDPROC

FUNC BOOL IS_BOUNTY_BLIP_FLASH_ACTIVE()
	RETURN g_PlayerBlipsData.bDisplayBountyBlip
ENDFUNC

PROC SET_DISPLAY_BOUNTY_FLASH_ACTIVE(BOOL bBool)
	IF NOT (bBool = g_PlayerBlipsData.bDisplayBountyBlip)
		CPRINTLN(DEBUG_BLIP, "[player_blips] SET_DISPLAY_BOUNTY_FLASH_ACTIVE, changing g_PlayerBlipsData.bDisplayBountyBlip to ", bBool)
		//DEBUG_PRINTCALLSTACK()				
	 	g_PlayerBlipsData.bDisplayBountyBlip = bBool
	ENDIF	
ENDPROC

FUNC BOOL IS_PED_IN_VEHICLE_THAT_HAS_A_BLIP(PED_INDEX PedID, BOOL bIgnoreAlpha=FALSE)
	IF IS_PED_IN_ANY_VEHICLE(PedID)
		VEHICLE_INDEX VehicleID
		VehicleID = GET_VEHICLE_PED_IS_IN(PedID)
		BLIP_INDEX BlipID = GET_BLIP_FROM_ENTITY(VehicleID)
		IF DOES_BLIP_EXIST(BlipID) 
		AND ((GET_BLIP_ALPHA(BlipID) > 1) OR (bIgnoreAlpha))
		AND NOT (BlipID = g_PlayerBlipsData.ArrowStrokeBlipID)
			RETURN(TRUE)	
		ENDIF
	ENDIF	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_VEHICLE_BLIP_BEING_BLIPPED_BY_SHIPMENT_SYSTEM(PLAYER_INDEX PlayerID)	
	IF GB_IS_PLAYER_IN_CONTRABAND_VEHICLE(PlayerID)
	OR GB_IS_PLAYER_IN_GUNRUN_VEHICLE(PlayerID)
	OR GB_IS_PLAYER_IN_SMUGGLER_VEHICLE(PlayerID)
	OR GB_IS_PLAYER_IN_FM_GANGOPS_VEHICLE(PlayerID)
	OR GB_IS_PLAYER_IN_FMBB_VEHICLE(PlayerID)
	#IF FEATURE_CASINO_HEIST
	OR GB_IS_PLAYER_IN_CASINO_HEIST_VEHICLE(PlayerID)
	#ENDIF
		// check local player ok to display blip
		IF NOT GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
		AND NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
		#IF FEATURE_TUNER
		AND NOT GB_IS_GLOBAL_CLIENT_BIT2_SET(PlayerID, eGB_GLOBAL_CLIENT_BITSET_2_INSIDE_CHALKBOARD_VEHICLE)
		#ENDIF
			RETURN TRUE			
		ELSE		
			PRINTLN("[player_blips] IS_PLAYER_VEHICLE_BLIP_BEING_BLIPPED_BY_SHIPMENT_SYSTEM -  = FALSE, player ", NATIVE_TO_INT(PlayerID))		
		ENDIF	
	ELSE
		//PRINTLN("[player_blips] IS_PLAYER_VEHICLE_BLIP_BEING_BLIPPED_BY_SHIPMENT_SYSTEM - GB_IS_PLAYER_IN_CONTRABAND_VEHICLE = FALSE, player ", NATIVE_TO_INT(PlayerID))
	ENDIF	
	RETURN FALSE
ENDFUNC




FUNC BOOL DOES_PLAYER_PASS_SHIPMENT_BLIP_COMMON_CHECKS(PLAYER_INDEX PlayerID, BOOL bIgnoreDistanceCheck=FALSE)
	
	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneShipmentCommonChecks, NATIVE_TO_INT(PlayerID))
		RETURN IS_BIT_SET(g_PlayerBlipsData.bs_PassedShipmentCommonChecks, NATIVE_TO_INT(PlayerID))
	ELSE	
		SET_BIT(g_PlayerBlipsData.bs_DoneShipmentCommonChecks, NATIVE_TO_INT(PlayerID))

		IF NOT IS_PLAYER_PASSIVE(PlayerID)
			IF NOT FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(MPGlobals.LocalPlayerID)
				IF (NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(MPGlobals.LocalPlayerID)
				OR ((GB_IS_PLAYER_MEMBER_OF_A_GANG(MPGlobals.LocalPlayerID) AND GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(MPGlobals.LocalPlayerID, PlayerID)))) // see bug 3683669 //AND GB_IS_PLAYER_ON_GUNRUNNING_SHIPMENT_MISSION(MPGlobals.LocalPlayerID)))
					IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(MPGlobals.LocalPlayerID)
						IF NOT IS_PLAYER_IN_PROPERTY(MPGlobals.LocalPlayerID, TRUE)
						AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].iFmTutProgBitset, biTrigTut_CompletedInitalAmbientTut)
							IF NOT FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(MPGlobals.LocalPlayerID)
								IF NOT IS_PLAYER_VEHICLE_BLIP_BEING_BLIPPED_BY_SHIPMENT_SYSTEM(PlayerID)
									IF (bIgnoreDistanceCheck)
									OR VDIST(GET_PLAYER_COORDS(MPGlobals.LocalPlayerID), GET_PLAYER_COORDS(PlayerID)) < g_sMPTunables.fshipmentshortrangedistance
										SET_BIT(g_PlayerBlipsData.bs_PassedShipmentCommonChecks, NATIVE_TO_INT(PlayerID))
										RETURN TRUE
									ENDIF
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		CLEAR_BIT(g_PlayerBlipsData.bs_PassedShipmentCommonChecks, NATIVE_TO_INT(PlayerID))
		
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL SHOULD_PLAYER_HAVE_CONTRABAND_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck=FALSE)

	BOOL bPassedInternalChecks = FALSE

	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneContrabandInternalChecks, NATIVE_TO_INT(PlayerID))
		bPassedInternalChecks = IS_BIT_SET(g_PlayerBlipsData.bs_PassedContrabandInternalChecks, NATIVE_TO_INT(PlayerID))
	ELSE
		
		SET_BIT(g_PlayerBlipsData.bs_DoneContrabandInternalChecks, NATIVE_TO_INT(PlayerID))
	
		IF DOES_PLAYER_PASS_SHIPMENT_BLIP_COMMON_CHECKS(PlayerID)
			IF NOT GB_SHOULD_HIDE_CEO_WORK_STAT()
				IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PlayerID) != FMMC_TYPE_GB_CONTRABAND_SELL
					IF GET_PLAYER_RIVAL_ENTITY_TYPE(PlayerID) = eRIVALENTITYTYPE_CONTRABAND

						IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
							IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_CONTRABAND)
								IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(),FMMC_TYPE_GANGHIDEOUT)
									PRINTLN("SHOULD_PLAYER_HAVE_CONTRABAND_BLIP: returned false as player is on ganghideout mission")
									RETURN FALSE
								ELSE
									bPassedInternalChecks = TRUE
								ENDIF
							ENDIF
						ELSE
							IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_GANG(PlayerID, eRIVALENTITYTYPE_CONTRABAND)
								IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(),FMMC_TYPE_GANGHIDEOUT)
									PRINTLN("SHOULD_PLAYER_HAVE_CONTRABAND_BLIP: returned false as player is on ganghideout mission")
									RETURN FALSE
								ELSE
									bPassedInternalChecks = TRUE
								ENDIF				
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (bPassedInternalChecks)
			SET_BIT(g_PlayerBlipsData.bs_PassedContrabandInternalChecks, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(g_PlayerBlipsData.bs_PassedContrabandInternalChecks, NATIVE_TO_INT(PlayerID))
		ENDIF
	
	ENDIF
	
	IF (bPassedInternalChecks)
		IF IS_SHIPMENT_BLIP_FLASH_ACTIVE() // controlled by a timer
		OR (bIgnoreFlashCheck)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_HAVE_ILLICIT_GOODS_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck=FALSE)

	BOOL bPassedInternalChecks = FALSE
	
	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneIllicitGoodsInternalChecks, NATIVE_TO_INT(PlayerID))
		bPassedInternalChecks = IS_BIT_SET(g_PlayerBlipsData.bs_PassedIllicitGoodsInternalChecks, NATIVE_TO_INT(PlayerID))
	ELSE
		
		SET_BIT(g_PlayerBlipsData.bs_DoneIllicitGoodsInternalChecks, NATIVE_TO_INT(PlayerID))
		
		IF DOES_PLAYER_PASS_SHIPMENT_BLIP_COMMON_CHECKS(PlayerID)
			IF NOT GB_SHOULD_HIDE_MC_WORK_STAT()
				//IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PlayerID) != FMMC_TYPE_BIKER_CONTRABAND_SELL			
					IF GET_PLAYER_RIVAL_ENTITY_TYPE(PlayerID) = eRIVALENTITYTYPE_BIKER
						IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
							IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_BIKER)
								IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(),FMMC_TYPE_GANGHIDEOUT)
									PRINTLN("SHOULD_PLAYER_HAVE_ILLICIT_GOODS_BLIP: returned false as player is on ganghideout mission")
									RETURN FALSE
								ELSE
									bPassedInternalChecks = TRUE
								ENDIF
							ENDIF
						ELSE
							IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_GANG(PlayerID, eRIVALENTITYTYPE_BIKER)
								IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(),FMMC_TYPE_GANGHIDEOUT)
									PRINTLN("SHOULD_PLAYER_HAVE_ILLICIT_GOODS_BLIP: returned false as player is on ganghideout mission")
									RETURN FALSE
								ELSE
									bPassedInternalChecks = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				//ENDIF
			ENDIF
		ENDIF
		
		IF (bPassedInternalChecks)
			SET_BIT(g_PlayerBlipsData.bs_PassedIllicitGoodsInternalChecks, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(g_PlayerBlipsData.bs_PassedIllicitGoodsInternalChecks, NATIVE_TO_INT(PlayerID))
		ENDIF
		
	ENDIF
		
	IF (bPassedInternalChecks)
		IF IS_SHIPMENT_BLIP_FLASH_ACTIVE() // controlled by a timer
		OR (bIgnoreFlashCheck)
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SHOULD_PLAYER_HAVE_VEHICLE_EXPORT_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck=FALSE)

	BOOL bPassedInternalChecks = FALSE
	
	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneVehicleExportInternalChecks, NATIVE_TO_INT(PlayerID))
		bPassedInternalChecks = IS_BIT_SET(g_PlayerBlipsData.bs_PassedVehicleExportInternalChecks, NATIVE_TO_INT(PlayerID))
	ELSE
		
		SET_BIT(g_PlayerBlipsData.bs_DoneVehicleExportInternalChecks, NATIVE_TO_INT(PlayerID))
		
		IF DOES_PLAYER_PASS_SHIPMENT_BLIP_COMMON_CHECKS(PlayerID)
			IF NOT GB_SHOULD_HIDE_CEO_WORK_STAT() // not sure if this is right.
				//IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PlayerID) != FMMC_TYPE_BIKER_CONTRABAND_SELL			
					IF GET_PLAYER_RIVAL_ENTITY_TYPE(PlayerID) = eRIVALENTITYTYPE_EXPORT
						IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
							IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_EXPORT)
								IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(),FMMC_TYPE_GANGHIDEOUT)
									PRINTLN("SHOULD_PLAYER_HAVE_VEHICLE_EXPORT_BLIP: returned false as player is on ganghideout mission")
									RETURN FALSE
								ELSE
									bPassedInternalChecks = TRUE
								ENDIF
							ENDIF
						ELSE
							IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_GANG(PlayerID, eRIVALENTITYTYPE_EXPORT)
								IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(),FMMC_TYPE_GANGHIDEOUT)
									PRINTLN("SHOULD_PLAYER_HAVE_VEHICLE_EXPORT_BLIP: returned false as player is on ganghideout mission")
									RETURN FALSE
								ELSE
									bPassedInternalChecks = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				//ENDIF
			ENDIF
		ENDIF
		
		IF (bPassedInternalChecks)
			SET_BIT(g_PlayerBlipsData.bs_PassedVehicleExportInternalChecks, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(g_PlayerBlipsData.bs_PassedVehicleExportInternalChecks, NATIVE_TO_INT(PlayerID))
		ENDIF
		
	ENDIF
		
	IF (bPassedInternalChecks)
		IF IS_SHIPMENT_BLIP_FLASH_ACTIVE() // controlled by a timer
		OR (bIgnoreFlashCheck)
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SHOULD_PLAYER_HAVE_GUNRUN_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck=FALSE)
	BOOL bPassedInternalChecks = FALSE
	
	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneGunRunInternalChecks, NATIVE_TO_INT(PlayerID))
		bPassedInternalChecks = IS_BIT_SET(g_PlayerBlipsData.bs_PassedGunRunInternalChecks, NATIVE_TO_INT(PlayerID))
	ELSE
		SET_BIT(g_PlayerBlipsData.bs_DoneGunRunInternalChecks, NATIVE_TO_INT(PlayerID))
		
		IF DOES_PLAYER_PASS_SHIPMENT_BLIP_COMMON_CHECKS(PlayerID)
			IF NOT GB_SHOULD_HIDE_GUNRUN_WORK_STAT()
				IF GET_PLAYER_RIVAL_ENTITY_TYPE(PlayerID) = eRIVALENTITYTYPE_GUNRUNNING
					IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_GUNRUNNING)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_GUNRUN_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ENDIF
					ELSE
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_GANG(PlayerID, eRIVALENTITYTYPE_GUNRUNNING)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_GUNRUN_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bPassedInternalChecks
			SET_BIT(g_PlayerBlipsData.bs_PassedGunRunInternalChecks, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(g_PlayerBlipsData.bs_PassedGunRunInternalChecks, NATIVE_TO_INT(PlayerID))
		ENDIF
	ENDIF
	
	IF bPassedInternalChecks
		IF IS_SHIPMENT_BLIP_FLASH_ACTIVE()
		OR bIgnoreFlashCheck
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_HAVE_SMUGGLER_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck=FALSE)
	BOOL bPassedInternalChecks = FALSE
	
	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneSmugglerInternalChecks, NATIVE_TO_INT(PlayerID))
		bPassedInternalChecks = IS_BIT_SET(g_PlayerBlipsData.bs_PassedSmugglerInternalChecks, NATIVE_TO_INT(PlayerID))
	ELSE
		SET_BIT(g_PlayerBlipsData.bs_DoneSmugglerInternalChecks, NATIVE_TO_INT(PlayerID))
		
		IF DOES_PLAYER_PASS_SHIPMENT_BLIP_COMMON_CHECKS(PlayerID)
			IF NOT GB_SHOULD_HIDE_SMUGGLER_WORK_STAT()
				IF GET_PLAYER_RIVAL_ENTITY_TYPE(PlayerID) = eRIVALENTITYTYPE_SMUGGLER
					IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_SMUGGLER)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_SMUGGLER_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ENDIF
					ELSE
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_GANG(PlayerID, eRIVALENTITYTYPE_SMUGGLER)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_SMUGGLER_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bPassedInternalChecks
			SET_BIT(g_PlayerBlipsData.bs_PassedSmugglerInternalChecks, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(g_PlayerBlipsData.bs_PassedSmugglerInternalChecks, NATIVE_TO_INT(PlayerID))
		ENDIF
	ENDIF
	
	IF bPassedInternalChecks
		IF IS_SHIPMENT_BLIP_FLASH_ACTIVE()
		OR bIgnoreFlashCheck
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck=FALSE)
	BOOL bPassedInternalChecks = FALSE
	
	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneFM_GangOpsInternalChecks, NATIVE_TO_INT(PlayerID))
		bPassedInternalChecks = IS_BIT_SET(g_PlayerBlipsData.bs_PassedFM_GangOpsInternalChecks, NATIVE_TO_INT(PlayerID))
	ELSE
		SET_BIT(g_PlayerBlipsData.bs_DoneFM_GangOpsInternalChecks, NATIVE_TO_INT(PlayerID))
		
		IF DOES_PLAYER_PASS_SHIPMENT_BLIP_COMMON_CHECKS(PlayerID)
			//IF NOT GB_SHOULD_HIDE_FM_GANGOPS_WORK_STAT() // nothing set up for this yet
				IF GET_PLAYER_RIVAL_ENTITY_TYPE(PlayerID) = eRIVALENTITYTYPE_GANGOPS
					IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_GANGOPS)
						AND SHOULD_BLIP_RIVAL_GANGOPS_PLAYER(PlayerID)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							IF NOT IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_GANGOPS)
								PRINTLN("SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP: IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_GANGOPS) is FALSE")
							ENDIF
							
							IF NOT SHOULD_BLIP_RIVAL_GANGOPS_PLAYER(PlayerID)
								PRINTLN("SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP: SHOULD_BLIP_RIVAL_GANGOPS_PLAYER is FALSE")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_GANG(PlayerID, eRIVALENTITYTYPE_GANGOPS)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			//ENDIF
		ENDIF
		
		IF bPassedInternalChecks
			SET_BIT(g_PlayerBlipsData.bs_PassedFM_GangOpsInternalChecks, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(g_PlayerBlipsData.bs_PassedFM_GangOpsInternalChecks, NATIVE_TO_INT(PlayerID))
		ENDIF
	ENDIF
	
	IF bPassedInternalChecks
		IF IS_SHIPMENT_BLIP_FLASH_ACTIVE()
		OR bIgnoreFlashCheck
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_HAVE_FMBB_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck = FALSE)
	BOOL bPassedInternalChecks = FALSE
	
	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneFMBBInternalChecks, NATIVE_TO_INT(PlayerID))
		bPassedInternalChecks = IS_BIT_SET(g_PlayerBlipsData.bs_PassedFMBBInternalChecks, NATIVE_TO_INT(PlayerID))
	ELSE
		SET_BIT(g_PlayerBlipsData.bs_DoneFMBBInternalChecks, NATIVE_TO_INT(PlayerID))
		
		IF DOES_PLAYER_PASS_SHIPMENT_BLIP_COMMON_CHECKS(PlayerID)
			IF NOT GB_SHOULD_HIDE_NIGHTCLUB_BATTLES_WORK_STAT()
				IF GET_PLAYER_RIVAL_ENTITY_TYPE(PlayerID) = eRIVALENTITYTYPE_FMBB
					IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_FMBB)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_FMBB_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							IF NOT IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_FMBB)
								PRINTLN("SHOULD_PLAYER_HAVE_FMBB_BLIP: IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_FMBB) is FALSE")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_GANG(PlayerID, eRIVALENTITYTYPE_FMBB)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bPassedInternalChecks
			SET_BIT(g_PlayerBlipsData.bs_PassedFMBBInternalChecks, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(g_PlayerBlipsData.bs_PassedFMBBInternalChecks, NATIVE_TO_INT(PlayerID))
		ENDIF
	ENDIF
	
	IF bPassedInternalChecks
		IF IS_SHIPMENT_BLIP_FLASH_ACTIVE()
		OR bIgnoreFlashCheck
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_HAVE_RANDOM_EVENT_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck = FALSE)
	BOOL bPassedInternalChecks = FALSE
	
	INT iEventID = -1
	
	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneRandomEventInternalChecks, NATIVE_TO_INT(PlayerID))
		bPassedInternalChecks = IS_BIT_SET(g_PlayerBlipsData.bs_PassedRandomEventInternalChecks, NATIVE_TO_INT(PlayerID))
	ELSE
		SET_BIT(g_PlayerBlipsData.bs_DoneRandomEventInternalChecks, NATIVE_TO_INT(PlayerID))
		
		IF DOES_PLAYER_PASS_SHIPMENT_BLIP_COMMON_CHECKS(PlayerID)
		AND ARE_PLAYERS_RUNNING_SAME_RANDOM_EVENT(PLAYER_ID(), PlayerID, iEventID)
			IF IS_PLAYER_CRITICAL_TO_RANDOM_EVENT_TYPE(PlayerID, iEventID)
				IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
					PRINTLN("SHOULD_PLAYER_HAVE_RANDOM_EVENT_BLIP: returned false as player is on ganghideout mission")
					RETURN FALSE
				ELSE
					bPassedInternalChecks = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bPassedInternalChecks
			SET_BIT(g_PlayerBlipsData.bs_PassedRandomEventInternalChecks, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(g_PlayerBlipsData.bs_PassedRandomEventInternalChecks, NATIVE_TO_INT(PlayerID))
		ENDIF
	ENDIF
	
	IF bPassedInternalChecks
		IF IS_SHIPMENT_BLIP_FLASH_ACTIVE()
		OR bIgnoreFlashCheck
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF FEATURE_CASINO_HEIST
FUNC BOOL SHOULD_PLAYER_HAVE_CASINO_HEIST_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck = FALSE)
	BOOL bPassedInternalChecks = FALSE
	
	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneCasinoHeistInternalChecks, NATIVE_TO_INT(PlayerID))
		bPassedInternalChecks = IS_BIT_SET(g_PlayerBlipsData.bs_PassedCasinoHeistInternalChecks, NATIVE_TO_INT(PlayerID))
	ELSE
		SET_BIT(g_PlayerBlipsData.bs_DoneCasinoHeistInternalChecks, NATIVE_TO_INT(PlayerID))
		
		IF DOES_PLAYER_PASS_SHIPMENT_BLIP_COMMON_CHECKS(PlayerID, TRUE)
//			IF NOT GB_SHOULD_HIDE_NIGHTCLUB_BATTLES_WORK_STAT()
				IF GET_PLAYER_RIVAL_ENTITY_TYPE(PlayerID) = eRIVALENTITYTYPE_CASINO_HEIST
					IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_CASINO_HEIST, TRUE)
						AND SHOULD_BLIP_RIVAL_CASINO_HEIST_PLAYER(PlayerID)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_CASINO_HEIST_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							IF NOT IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_CASINO_HEIST, TRUE)
								PRINTLN("SHOULD_PLAYER_HAVE_CASINO_HEIST_BLIP: IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_CASINO_HEIST) is FALSE")
							ENDIF
							
							IF NOT SHOULD_BLIP_RIVAL_CASINO_HEIST_PLAYER(PlayerID)
								PRINTLN("SHOULD_PLAYER_HAVE_CASINO_HEIST_BLIP: SHOULD_BLIP_RIVAL_CASINO_HEIST_PLAYER is FALSE")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_GANG(PlayerID, eRIVALENTITYTYPE_CASINO_HEIST)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_CASINO_HEIST_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
//			ENDIF
		ENDIF
		
		IF bPassedInternalChecks
			SET_BIT(g_PlayerBlipsData.bs_PassedCasinoHeistInternalChecks, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(g_PlayerBlipsData.bs_PassedCasinoHeistInternalChecks, NATIVE_TO_INT(PlayerID))
		ENDIF
	ENDIF
	
	IF bPassedInternalChecks
		IF IS_SHIPMENT_BLIP_FLASH_ACTIVE()
		OR bIgnoreFlashCheck
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL SHOULD_PLAYER_HAVE_FM_CONTENT_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck = FALSE)
	BOOL bPassedInternalChecks = FALSE
	
	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneFMContentInternalChecks, NATIVE_TO_INT(PlayerID))
		bPassedInternalChecks = IS_BIT_SET(g_PlayerBlipsData.bs_PassedFMContentInternalChecks, NATIVE_TO_INT(PlayerID))
	ELSE
		SET_BIT(g_PlayerBlipsData.bs_DoneFMContentInternalChecks, NATIVE_TO_INT(PlayerID))
		
		IF DOES_PLAYER_PASS_SHIPMENT_BLIP_COMMON_CHECKS(PlayerID, TRUE)
//			IF NOT GB_SHOULD_HIDE_WORK_STAT()
				IF GET_PLAYER_RIVAL_ENTITY_TYPE(PlayerID) = eRIVALENTITYTYPE_FM_CONTENT
					IF NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_FM_CONTENT, TRUE)
						AND SHOULD_BLIP_RIVAL_FM_CONTENT_PLAYER(PlayerID)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_FM_CONTENT_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							IF NOT IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_FM_CONTENT, TRUE)
								PRINTLN("SHOULD_PLAYER_HAVE_FM_CONTENT_BLIP: IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_RIVALS(PlayerID, eRIVALENTITYTYPE_FM_CONTENT) is FALSE")
							ENDIF
							
							IF NOT SHOULD_BLIP_RIVAL_FM_CONTENT_PLAYER(PlayerID)
								PRINTLN("SHOULD_PLAYER_HAVE_FM_CONTENT_BLIP: SHOULD_BLIP_RIVAL_FM_CONTENT_PLAYER is FALSE")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						IF IS_PLAYER_RIVAL_ENTITY_OF_TYPE_VISIBLE_TO_GANG(PlayerID, eRIVALENTITYTYPE_FM_CONTENT)
							IF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
								PRINTLN("SHOULD_PLAYER_HAVE_FM_CONTENT_BLIP: returned false as player is on ganghideout mission")
								
								RETURN FALSE
							ELSE
								bPassedInternalChecks = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
//			ENDIF
		ENDIF
		
		IF bPassedInternalChecks
			SET_BIT(g_PlayerBlipsData.bs_PassedFMContentInternalChecks, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(g_PlayerBlipsData.bs_PassedFMContentInternalChecks, NATIVE_TO_INT(PlayerID))
		ENDIF
	ENDIF
	
	IF bPassedInternalChecks
		IF IS_SHIPMENT_BLIP_FLASH_ACTIVE()
		OR bIgnoreFlashCheck
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck=FALSE)
	IF (1=0) 
	OR SHOULD_PLAYER_HAVE_CONTRABAND_BLIP(PlayerID, bIgnoreFlashCheck)
	OR SHOULD_PLAYER_HAVE_ILLICIT_GOODS_BLIP(PlayerID, bIgnoreFlashCheck)
	OR SHOULD_PLAYER_HAVE_VEHICLE_EXPORT_BLIP(PlayerID, bIgnoreFlashCheck)
	OR SHOULD_PLAYER_HAVE_GUNRUN_BLIP(PlayerID, bIgnoreFlashCheck)
	OR SHOULD_PLAYER_HAVE_SMUGGLER_BLIP(PlayerID, bIgnoreFlashCheck)
	OR SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP(PlayerID, bIgnoreFlashCheck)
	OR SHOULD_PLAYER_HAVE_FMBB_BLIP(PlayerID, bIgnoreFlashCheck)
	OR SHOULD_PLAYER_HAVE_RANDOM_EVENT_BLIP(PlayerID, bIgnoreFlashCheck)
	#IF FEATURE_CASINO_HEIST
	OR SHOULD_PLAYER_HAVE_CASINO_HEIST_BLIP(PlayerID, bIgnoreFlashCheck)
	#ENDIF
	OR SHOULD_PLAYER_HAVE_FM_CONTENT_BLIP(PlayerID, bIgnoreFlashCheck)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_USING_GANG_BOSS_SPRITE_BLIP(PLAYER_INDEX PlayerID)

	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PlayerID)
	
		INT iPlayerIDInt = NATIVE_TO_INT(PlayerID)
		IF GlobalplayerBD[iPlayerIDInt].playerBlipData.iInVehicleType != 0
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_HAVE_BOUNTY_BLIP(PLAYER_INDEX PlayerID, BOOL bIgnoreFlashCheck=FALSE)

	BOOL bPassedInternalChecks = FALSE

	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoneBountyInternalChecks, NATIVE_TO_INT(PlayerID))
		bPassedInternalChecks = IS_BIT_SET(g_PlayerBlipsData.bs_PassedBountyInternalChecks, NATIVE_TO_INT(PlayerID))
	ELSE

		SET_BIT(g_PlayerBlipsData.bs_DoneBountyInternalChecks, NATIVE_TO_INT(PlayerID))

		IF IS_BOUNTY_UNLOCKED_FOR_PLAYER(MPGlobals.LocalPlayerID)
			IF NOT IS_PLAYER_PASSIVE(PlayerID)
				IF NOT ARE_PLAYERS_ON_SAME_TEAM(PlayerID, MPGlobals.LocalPlayerID)
					IF (GlobalServerBD_FM.currentBounties[NATIVE_TO_INT(playerID)].bTargeted)
						IF NOT IS_PLAYER_ON_ANY_MP_MISSION(MPGlobals.LocalPlayerID)
							IF NOT GlobalplayerBD[NATIVE_TO_INT(PlayerID)].bHideBountyBlip
								IF NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PlayerID, MPGlobals.LocalPlayerID)
									IF NOT IS_PLAYER_IN_NIGHTCLUB(PlayerID)
										// hide the bounty sprite if on a fm event and far away 2423150
										IF FM_EVENT_SHOULD_REMOTE_PLAYER_BOUNTY_BLIP_BE_SHORT_RANGE(MPGlobals.LocalPlayerID)
											IF (VDIST(GET_PLAYER_COORDS(PlayerID), GET_PLAYER_COORDS(MPGlobals.LocalPlayerID)) < 200.0)
												bPassedInternalChecks = TRUE
											ENDIF
										ELSE						
											bPassedInternalChecks = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (bPassedInternalChecks)
			SET_BIT(g_PlayerBlipsData.bs_PassedBountyInternalChecks, NATIVE_TO_INT(PlayerID))
		ELSE
			CLEAR_BIT(g_PlayerBlipsData.bs_PassedBountyInternalChecks, NATIVE_TO_INT(PlayerID))
		ENDIF
		
	ENDIF
	
	IF (bPassedInternalChecks)
		IF (IS_USING_GANG_BOSS_SPRITE_BLIP(PlayerID) AND IS_BOUNTY_BLIP_FLASH_ACTIVE())
		OR (IS_USING_GANG_BOSS_SPRITE_BLIP(PlayerID) = FALSE)
		OR (bIgnoreFlashCheck)
			RETURN TRUE
		ENDIF
	ENDIF		
	
	RETURN(FALSE)
ENDFUNC

PROC SET_PACKAGE_BLIP_COLOUR_FOR_TEAM(BLIP_INDEX &PackageBlip,INT iteam)

	HUD_COLOURS tempHUDColour
	IF iteam = -1
		tempHUDColour =HUD_COLOUR_GREEN
	ELSE
		tempHUDColour = GET_HUD_COLOUR_FOR_OPPONENT_CHOSEN_TEAM(GET_PLAYER_TEAM(PLAYER_ID()),iteam)
	ENDIF
	SET_BLIP_COLOUR_FROM_HUD_COLOUR(PackageBlip, tempHUDColour)
	
ENDPROC


// ****************************************************************************************************************************
// ****************************************************************************************************************************
//								LOCKED THREAD FUNCTIONALITY
// ****************************************************************************************************************************
// ****************************************************************************************************************************
FUNC BOOL BlipPreThreadLockFailCheck(PLAYER_INDEX PlayerID)
	IF (PlayerID = INVALID_PLAYER_INDEX())
		SCRIPT_ASSERT("BlipPreThreadLockFailCheck - passed in invalid player index.")
		RETURN TRUE
	ENDIF
	
	IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()	
		DEBUG_PRINTCALLSTACK()
		NET_PRINT("[player_blips] BlipPreThreadLockFailCheck - called from within launch script. I do not like this. Neilf") NET_NL()
		SCRIPT_ASSERT("BlipPreThreadLockFailCheck - called from within launch script.")
		RETURN TRUE	
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL DoBlipThreadOwnership(THREADID& Thread_Id, THREADID& Thread_Id_Forced, INT &iBitset, BOOL bActivate, INT iPlayer, BOOL bForceThreadOwnership, BOOL& bBitsetStateChanged)


	IF (bForceThreadOwnership) 		
		IF NOT IS_THREAD_ACTIVE(Thread_Id_Forced)
		OR (Thread_Id_Forced = GET_ID_OF_THIS_THREAD())	
			#IF IS_DEBUG_BUILD
				IF NOT (Thread_Id_Forced = GET_ID_OF_THIS_THREAD())
					DEBUG_PRINTCALLSTACK()
					PRINTLN("[player_blips] DoBlipThreadOwnership - setting forced thread ownership. thread id = ", NATIVE_TO_INT(GET_ID_OF_THIS_THREAD()))
				ENDIF
			#ENDIF
			Thread_Id_Forced = GET_ID_OF_THIS_THREAD()	
			Thread_Id = GET_ID_OF_THIS_THREAD()			
		ELSE
			SCRIPT_ASSERT("[player_blips] DoBlipThreadOwnership - bForceThreadOwnership already being used by another thread")
		ENDIF				
	ENDIF
	
	
	IF NOT IS_THREAD_ACTIVE(Thread_Id)
	OR (Thread_Id = GET_ID_OF_THIS_THREAD())
		IF (bActivate)
			#IF IS_DEBUG_BUILD
				IF NOT (Thread_Id = GET_ID_OF_THIS_THREAD())
					DEBUG_PRINTCALLSTACK()
					PRINTLN("[player_blips] DoBlipThreadOwnership - setting thread ownership. thread id = ", NATIVE_TO_INT(GET_ID_OF_THIS_THREAD()))
				ENDIF
			#ENDIF
			IF NOT IS_BIT_SET(iBitset, iPlayer)
				bBitsetStateChanged = TRUE				
				PRINTLN("[player_blips] DoBlipThreadOwnership - setting iBitset - player ", iPlayer)
				SET_BIT(iBitset, iPlayer)
			ENDIF
			
			Thread_Id = GET_ID_OF_THIS_THREAD()

		ELSE
		
			#IF IS_DEBUG_BUILD
				IF (Thread_Id = GET_ID_OF_THIS_THREAD())
					DEBUG_PRINTCALLSTACK()
					PRINTLN("[player_blips] DoBlipThreadOwnership - clearing thread ownership. thread id = ", NATIVE_TO_INT(GET_ID_OF_THIS_THREAD()))
				ENDIF
				IF (Thread_Id_Forced = GET_ID_OF_THIS_THREAD())
					DEBUG_PRINTCALLSTACK()
					PRINTLN("[player_blips] DoBlipThreadOwnership - clearing forced thread ownership. thread id = ", NATIVE_TO_INT(GET_ID_OF_THIS_THREAD()))
				ENDIF
			#ENDIF		
		
		
			IF IS_BIT_SET(iBitset, iPlayer)
				bBitsetStateChanged = TRUE
				PRINTLN("[player_blips] DoBlipThreadOwnership - clearing iBitset - player ", iPlayer)
				CLEAR_BIT(iBitset, iPlayer)
			ENDIF			
			
			IF (Thread_Id_Forced = GET_ID_OF_THIS_THREAD())
				Thread_Id_Forced = INT_TO_NATIVE(THREADID, -1)
			ENDIF
			
			Thread_Id = INT_TO_NATIVE(THREADID, -1)

		ENDIF		
		
		RETURN TRUE // return true because we had control
	ELSE
		// this is not the active thread, we should assert if there is not a force owner active.
	
		// is a force owner ship active?
		IF IS_THREAD_ACTIVE(Thread_Id_Forced)
		AND NOT (Thread_Id_Forced = GET_ID_OF_THIS_THREAD())
			PRINTLN("[player_blips] DoBlipThreadOwnership - bForceThreadOwnership active by another thread. forced thread id = ", NATIVE_TO_INT(Thread_Id_Forced)) 
		ELSE
			SCRIPT_ASSERT("[player_blips] DoBlipThreadOwnership - already being used by another thread")
		ENDIF
	ENDIF

	RETURN FALSE // default value doesnt need updating. 
ENDFUNC

FUNC BOOL DoesThreadHaveControlOfBlipBehaviour(THREADID &thread_id)
	IF IS_THREAD_ACTIVE(thread_id)								
		IF NOT (thread_id = GET_ID_OF_THIS_THREAD())
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_THIS_THREAD_HAVE_CONTROL_OF_FORCE_BLIP_PLAYER(PLAYER_INDEX PlayerID)
	RETURN DoesThreadHaveControlOfBlipBehaviour(g_PlayerBlipsData.ForceBlipThreadID[NATIVE_TO_INT(PlayerID)])
ENDFUNC

FUNC BOOL DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PLAYER_INDEX PlayerID)
	RETURN DoesThreadHaveControlOfBlipBehaviour(g_PlayerBlipsData.CustomColourThreadID[NATIVE_TO_INT(PlayerID)])
ENDFUNC

FUNC BOOL DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_SPRITE_FOR_PLAYER(PLAYER_INDEX PlayerID)
	RETURN DoesThreadHaveControlOfBlipBehaviour(g_PlayerBlipsData.CustomSpriteThreadID[NATIVE_TO_INT(PlayerID)])
ENDFUNC

FUNC BOOL DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_ALPHA_FOR_PLAYER(PLAYER_INDEX PlayerID)
	RETURN DoesThreadHaveControlOfBlipBehaviour(g_PlayerBlipsData.CustomAlphaThreadID[NATIVE_TO_INT(PlayerID)])
ENDFUNC

FUNC BOOL DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_NUMBER_FOR_PLAYER(PLAYER_INDEX PlayerID)
	RETURN DoesThreadHaveControlOfBlipBehaviour(g_PlayerBlipsData.CustomNumberThreadID[NATIVE_TO_INT(PlayerID)])
ENDFUNC

FUNC BOOL DOES_THIS_THREAD_HAVE_CONTROL_OF_DISABLE_SECONDARY_OUTLINES_FOR_PLAYER(PLAYER_INDEX PlayerID)
	RETURN DoesThreadHaveControlOfBlipBehaviour(g_PlayerBlipsData.DisableSecondaryOutlinesThreadID[NATIVE_TO_INT(PlayerID)])
ENDFUNC

FUNC BOOL DOES_THIS_THREAD_HAVE_CONTROL_OF_FIXED_BLIP_SCALE_FOR_PLAYER(PLAYER_INDEX PlayerID)
	RETURN DoesThreadHaveControlOfBlipBehaviour(g_PlayerBlipsData.FixedScaleThreadID[NATIVE_TO_INT(PlayerID)])
ENDFUNC

FUNC BOOL DOES_THIS_THREAD_HAVE_CONTROL_OF_HIDDEN_BLIP_FOR_PLAYER(PLAYER_INDEX PlayerID)
	RETURN DoesThreadHaveControlOfBlipBehaviour(g_PlayerBlipsData.HiddenBlipThreadID[NATIVE_TO_INT(PlayerID)])
ENDFUNC

FUNC BOOL DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_NAME_FOR_PLAYER(PLAYER_INDEX PlayerID)
	RETURN DoesThreadHaveControlOfBlipBehaviour(g_PlayerBlipsData.CustomBlipNameThreadID[NATIVE_TO_INT(PlayerID)])
ENDFUNC

FUNC BOOL DOES_THIS_THREAD_HAVE_CONTROL_OF_LONG_RANGE_BLIP_FOR_PLAYER(PLAYER_INDEX PlayerID)
	RETURN DoesThreadHaveControlOfBlipBehaviour(g_PlayerBlipsData.LongRangeThreadID[NATIVE_TO_INT(PlayerID)])
ENDFUNC

FUNC BOOL DOES_THIS_THREAD_HAVE_CONTROL_OF_DISABLE_STEALTH_FOR_PLAYER(PLAYER_INDEX PlayerID)
	RETURN DoesThreadHaveControlOfBlipBehaviour(g_PlayerBlipsData.DisableStealthThreadID[NATIVE_TO_INT(PlayerID)])
ENDFUNC

FUNC BOOL IsPlayerUsingCustomBlipBehaviour(THREADID Thread_ID, INT &iBitset, INT iPlayer)
	IF IS_BIT_SET(iBitset, iPlayer)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bBypassThreadChecks)
				RETURN(TRUE)
			ENDIF
		#ENDIF
		IF IS_THREAD_ACTIVE(Thread_ID)
			RETURN(TRUE)
		ELSE
			DEBUG_PRINTCALLSTACK()
			CLEAR_BIT(iBitset, iPlayer)
			CPRINTLN(DEBUG_BLIP, "[player_blips] IsPlayerUsingCustomBlipBehaviour - thread no longer active, clearing bit. ")		
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_FORCE_BLIPPED(PLAYER_INDEX playerId)
	RETURN IsPlayerUsingCustomBlipBehaviour(g_PlayerBlipsData.ForceBlipThreadID[NATIVE_TO_INT(PlayerID)], g_PlayerBlipsData.bs_PlayersToForceBlip, NATIVE_TO_INT(playerId))
ENDFUNC

FUNC BOOL IS_PLAYER_USING_CUSTOM_BLIP_COLOUR(PLAYER_INDEX PlayerID)
	RETURN IsPlayerUsingCustomBlipBehaviour(g_PlayerBlipsData.CustomColourThreadID[NATIVE_TO_INT(PlayerID)], g_PlayerBlipsData.bs_PlayerUsingCustomColour, NATIVE_TO_INT(PlayerID))
ENDFUNC

FUNC BOOL IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(PLAYER_INDEX PlayerID)
	RETURN IsPlayerUsingCustomBlipBehaviour(g_PlayerBlipsData.CustomSpriteThreadID[NATIVE_TO_INT(PlayerID)], g_PlayerBlipsData.bs_PlayerUsingCustomSprite, NATIVE_TO_INT(PlayerID))
ENDFUNC

FUNC BOOL IS_PLAYER_USING_CUSTOM_BLIP_ALPHA(PLAYER_INDEX PlayerID)
	RETURN IsPlayerUsingCustomBlipBehaviour(g_PlayerBlipsData.CustomAlphaThreadID[NATIVE_TO_INT(PlayerID)], g_PlayerBlipsData.bs_PlayerUsingCustomAlpha, NATIVE_TO_INT(PlayerID))
ENDFUNC

FUNC BOOL IS_PLAYER_USING_CUSTOM_BLIP_NUMBER(PLAYER_INDEX PlayerID)
	RETURN IsPlayerUsingCustomBlipBehaviour(g_PlayerBlipsData.CustomNumberThreadID[NATIVE_TO_INT(PlayerID)], g_PlayerBlipsData.bs_PlayerUsingCustomNumber, NATIVE_TO_INT(PlayerID))
ENDFUNC


FUNC BOOL ARE_SECONDARY_OUTLINES_DISABLED_FOR_PLAYER_BLIP(PLAYER_INDEX PlayerID)

	IF IsPlayerUsingCustomBlipBehaviour(g_PlayerBlipsData.DisableSecondaryOutlinesThreadID[NATIVE_TO_INT(PlayerID)], g_PlayerBlipsData.bs_PlayerSecondaryOutlinesDisabled, NATIVE_TO_INT(PlayerID))
		RETURN TRUE
	ENDIF
	
	IF SHOULD_PLAYER_HAVE_BOUNTY_BLIP(PlayerID)
	OR SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(PlayerID)
	OR IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(PlayerID)
		RETURN TRUE
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_STEALTH_DISABLED_FOR_PLAYER_BLIP(PLAYER_INDEX PlayerID)
	RETURN IsPlayerUsingCustomBlipBehaviour(g_PlayerBlipsData.DisableStealthThreadID[NATIVE_TO_INT(PlayerID)], g_PlayerBlipsData.bs_PlayerStealthDisabled, NATIVE_TO_INT(PlayerID))
ENDFUNC

FUNC BOOL IS_PLAYER_USING_FIXED_BLIP_SCALE(PLAYER_INDEX PlayerID)
	RETURN IsPlayerUsingCustomBlipBehaviour(g_PlayerBlipsData.FixedScaleThreadID[NATIVE_TO_INT(PlayerID)], g_PlayerBlipsData.bs_PlayerUsingFixedScale, NATIVE_TO_INT(PlayerID))
ENDFUNC

FUNC BOOL IS_PLAYER_BLIP_HIDDEN(PLAYER_INDEX PlayerID)
	RETURN IsPlayerUsingCustomBlipBehaviour(g_PlayerBlipsData.HiddenBlipThreadID[NATIVE_TO_INT(PlayerID)], g_PlayerBlipsData.bs_PlayerIsHidden, NATIVE_TO_INT(PlayerID))
ENDFUNC

FUNC BOOL IS_PLAYER_USING_CUSTOM_BLIP_NAME(PLAYER_INDEX PlayerID)
	RETURN IsPlayerUsingCustomBlipBehaviour(g_PlayerBlipsData.CustomBlipNameThreadID[NATIVE_TO_INT(PlayerID)], g_PlayerBlipsData.bs_PlayerUsingCustomBlipName, NATIVE_TO_INT(PlayerID))
ENDFUNC

///    True if the player should has been set to be force blipped, false otherwise. 
FUNC BOOL IS_PLAYER_BLIP_SET_AS_LONG_RANGE(PLAYER_INDEX playerId)
	IF IS_NET_PLAYER_OK(playerId)
		RETURN IS_BIT_SET(g_PlayerBlipsData.bs_PlayersBlipIsLongRange, NATIVE_TO_INT(playerId))
	ENDIF	
	RETURN FALSE
ENDFUNC



// *************************************************** END ********************************************************************



FUNC BOOL IS_SPRITE_BLIP_PLAYER_IN_VEHICLE(VEHICLE_INDEX vehID, BOOL bAnyPlayer)

	IF DOES_ENTITY_EXIST(vehID)
		INT iMaxSeats = (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehID) + 1) //Add 1 for the driver
		INT i
		REPEAT iMaxSeats i
			IF NOT IS_VEHICLE_SEAT_FREE(vehID, INT_TO_ENUM(VEHICLE_SEAT, i-1))			
				PED_INDEX VehPedID = GET_PED_IN_VEHICLE_SEAT(vehID, INT_TO_ENUM(VEHICLE_SEAT, i-1))				
				IF NOT IS_PED_INJURED(VehPedID)
					IF IS_PED_A_PLAYER(VehPedID)
						PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(VehPedID)
						IF NETWORK_IS_PLAYER_ACTIVE(playerID)					
							IF IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(playerID)
							OR SHOULD_PLAYER_HAVE_BOUNTY_BLIP(PlayerID, TRUE)
							OR SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(PlayerID, TRUE)
							OR (bAnyPlayer)
							
								//CPRINTLN(DEBUG_BLIP, "[player_blips] IS_SPRITE_BLIP_PLAYER_IN_VEHICLE = true, playerid ", NATIVE_TO_INT(playerID), " seat " , i)
								
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	//CPRINTLN(DEBUG_BLIP, "[player_blips] IS_SPRITE_BLIP_PLAYER_IN_VEHICLE = false")
								

	RETURN FALSE
ENDFUNC

PROC CLEAR_CUSTOM_BLIP_SETTINGS_FOR_PLAYER(PLAYER_INDEX PlayerID)
	INT iPlayerInt = NATIVE_TO_INT(PlayerID)
	
	PRINTLN("CLEAR_CUSTOM_BLIP_SETTINGS_FOR_PLAYER - called for player ", iPlayerInt)
	DEBUG_PRINTCALLSTACK()
	

	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomSprite, iPlayerInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomColour, iPlayerInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomAlpha, iPlayerInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingFixedScale, iPlayerInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomBlipName, iPlayerInt)
	CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomNumber, iPlayerInt)
	
	
ENDPROC


PROC HIDE_DEAD_PLAYER_BLIPS(BOOL bSet)

	IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()	
		DEBUG_PRINTCALLSTACK()
		NET_PRINT("[player_blips] HIDE_DEAD_PLAYER_BLIPS - called from within launch script. I do not like this. Neilf") NET_NL()
		SCRIPT_ASSERT("HIDE_DEAD_PLAYER_BLIPS - called from within launch script.")
		EXIT	
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "HIDE_DEAD_PLAYER_BLIPS - called with ", bSet)	
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideDeadPlayerBlipsThreadID)
	OR (g_PlayerBlipsData.HideDeadPlayerBlipsThreadID = GET_ID_OF_THIS_THREAD())	
		IF (bSet)		
			g_PlayerBlipsData.HideDeadPlayerBlipsThreadID = GET_ID_OF_THIS_THREAD()
			g_PlayerBlipsData.bHideDeadPlayerBlips = TRUE
		ELSE
			CPRINTLN(DEBUG_BLIP, "HIDE_DEAD_PLAYER_BLIPS - clearing thread id")
			g_PlayerBlipsData.HideDeadPlayerBlipsThreadID = INT_TO_NATIVE(THREADID, -1)
			g_PlayerBlipsData.bHideDeadPlayerBlips = FALSE
		ENDIF
	ELSE
		CASSERTLN(DEBUG_BLIP, "HIDE_DEAD_PLAYER_BLIPS - already being used by another thread")
	ENDIF

ENDPROC

PROC SHOW_PLAYER_SONAR(BOOL bSet)

	IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()	
		DEBUG_PRINTCALLSTACK()
		NET_PRINT("[player_blips] SHOW_PLAYER_SONAR - called from within launch script. I do not like this. Neilf") NET_NL()
		SCRIPT_ASSERT("SHOW_PLAYER_SONAR - called from within launch script.")
		EXIT	
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "SHOW_PLAYER_SONAR - called with ", bSet)	
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.ShowPlayerSonarBlipsThreadID)
	OR (g_PlayerBlipsData.ShowPlayerSonarBlipsThreadID = GET_ID_OF_THIS_THREAD())	
		IF (bSet)		
			g_PlayerBlipsData.ShowPlayerSonarBlipsThreadID = GET_ID_OF_THIS_THREAD()
			g_PlayerBlipsData.bShowPlayerSonarBlips = TRUE
		ELSE
			CPRINTLN(DEBUG_BLIP, "SHOW_PLAYER_SONAR - clearing thread id")
			g_PlayerBlipsData.ShowPlayerSonarBlipsThreadID = INT_TO_NATIVE(THREADID, -1)
			g_PlayerBlipsData.bShowPlayerSonarBlips = FALSE
		ENDIF
	ELSE
		CASSERTLN(DEBUG_BLIP, "SHOW_PLAYER_SONAR - already being used by another thread")
	ENDIF

ENDPROC

PROC InitSonarBlips()
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "InitSonarBlips - called.")
	g_PlayerBlipsData.bSonarBlipsEnabled = FALSE
	ALLOW_SONAR_BLIPS(FALSE)
ENDPROC

PROC AllowSonarBlips(BOOL bSet)

	IF NOT (g_PlayerBlipsData.bSonarBlipsInitialised)
		g_PlayerBlipsData.bSonarBlipsEnabled = FALSE
		ALLOW_SONAR_BLIPS(FALSE)
		g_PlayerBlipsData.bSonarBlipsInitialised = TRUE
		CPRINTLN(DEBUG_BLIP, "AllowSonarBlips - initialised.")
	ENDIF
		
	
	IF (bSet)
		
		FORCE_SONAR_BLIPS_THIS_FRAME()
	
		IF NOT (g_PlayerBlipsData.bSonarBlipsEnabled)
		
			DEBUG_PRINTCALLSTACK()
			CPRINTLN(DEBUG_BLIP, "TurnOnSonarBlips - called with TRUE.")	
		
			ALLOW_SONAR_BLIPS(TRUE)
			g_PlayerBlipsData.bSonarBlipsEnabled = TRUE
		ENDIF
	ELSE
		IF (g_PlayerBlipsData.bSonarBlipsEnabled)
		
			DEBUG_PRINTCALLSTACK()
			CPRINTLN(DEBUG_BLIP, "TurnOnSonarBlips - called with FALSE.")	
		
			ALLOW_SONAR_BLIPS(FALSE)
			g_PlayerBlipsData.bSonarBlipsEnabled = FALSE
		ENDIF	
	ENDIF
ENDPROC




FUNC BOOL SHOULD_HIDE_DEAD_PLAYER_BLIPS()
	IF IS_THREAD_ACTIVE(g_PlayerBlipsData.HideDeadPlayerBlipsThreadID)	
		RETURN g_PlayerBlipsData.bHideDeadPlayerBlips
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SHOW_PLAYER_SONAR()
	IF IS_THREAD_ACTIVE(g_PlayerBlipsData.ShowPlayerSonarBlipsThreadID)	
		RETURN g_PlayerBlipsData.bShowPlayerSonarBlips
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ALLOW_PLAYER_HEIGHTS(BOOL bSet, BOOL bMakeFlash=FALSE)

	IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()	
		DEBUG_PRINTCALLSTACK()
		NET_PRINT("[player_blips] ALLOW_PLAYER_HEIGHTS - called from within launch script. I do not like this. Neilf") NET_NL()
		SCRIPT_ASSERT("ALLOW_PLAYER_HEIGHTS - called from within launch script.")
		EXIT	
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "ALLOW_PLAYER_HEIGHTS - called with ", bSet)	

	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.ShowPlayerHeightsThreadID)
	OR (g_PlayerBlipsData.ShowPlayerHeightsThreadID = GET_ID_OF_THIS_THREAD())	
		IF (bSet)
			g_PlayerBlipsData.ShowPlayerHeightsThreadID = GET_ID_OF_THIS_THREAD()
			g_PlayerBlipsData.bDoFlashHeights = bMakeFlash
			g_PlayerBlipsData.FlashHeightStartTime = GET_NETWORK_TIME()
			g_PlayerBlipsData.bHeightIsFlashedOn = FALSE
		ELSE
			CPRINTLN(DEBUG_BLIP, "ALLOW_PLAYER_HEIGHTS - clearing thread id")
			g_PlayerBlipsData.ShowPlayerHeightsThreadID = INT_TO_NATIVE(THREADID, -1)
		ENDIF
	ELSE
		CASSERTLN(DEBUG_BLIP, "ALLOW_PLAYER_HEIGHTS - already being used by another thread")
	ENDIF

ENDPROC

FUNC BOOL SHOULD_ALLOW_PLAYER_HEIGHTS()
	IF IS_THREAD_ACTIVE(g_PlayerBlipsData.ShowPlayerHeightsThreadID)
	AND NOT IS_PLAYER_SPECTATING(PLAYER_ID()) // url:bugstar:4714517 - Sumo (Remix) NG II - When in spectate mode, a player blip on the radar is shown with an upwards arrow as if they are above another player.
		IF (g_PlayerBlipsData.bDoFlashHeights)
			IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.FlashHeightStartTime)) < 1000)
				RETURN g_PlayerBlipsData.bHeightIsFlashedOn 
			ELSE
				g_PlayerBlipsData.bHeightIsFlashedOn = NOT (g_PlayerBlipsData.bHeightIsFlashedOn)
				g_PlayerBlipsData.FlashHeightStartTime = GET_NETWORK_TIME()
			ENDIF
		ELSE	
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC



FUNC BOOL SHOULD_SECONDARY_OUTLINES_BE_HIDDEN_FOR_PLAYER_BLIP(PLAYER_INDEX PlayerID)
	IF SHOULD_PLAYER_HAVE_BOUNTY_BLIP(PlayerID)
	OR SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(PlayerID)
	OR IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(PlayerID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



FUNC BOOL IS_PLAYER_BLIP_HIDDEN_AS_PARTICIPANT_OF_EVENT(PLAYER_INDEX PlayerID)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.iFlags, PBBD_HIDDEN_FROM_EVENT_NON_PARTICIPANTS)	
		IF NOT (FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(MPGlobals.LocalPlayerID) = FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PlayerID))
			RETURN(TRUE)
		ENDIF
	ENDIF	
	RETURN(FALSE)
ENDFUNC


FUNC BOOL IS_PLAYER_BLIP_HIDDEN_TO_NON_GANG_MEMBERS(PLAYER_INDEX PlayerID)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.iFlags, PBBD_HIDDEN_FROM_NON_GANG_MEMBERS)	
		IF NOT (GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PlayerID, MPGlobals.LocalPlayerID))
			RETURN(TRUE)
		ENDIF
	ENDIF	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_IN_VEHICLE_WITH_GANG_MEMBERS(PLAYER_INDEX PlayerID)
	IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID))
	AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PlayerID)
		VEHICLE_INDEX VehicleID
		VehicleID = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PlayerID))
		IF NOT IS_ENTITY_DEAD(VehicleID)
		AND IS_VEHICLE_DRIVEABLE(VehicleID)
			INT iNoOfSeats
			INT iSeat
			VEHICLE_SEAT eSeat
			iNoOfSeats = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(VehicleID))
			REPEAT iNoOfSeats iSeat
				eSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat-1)
				IF NOT IS_VEHICLE_SEAT_FREE(VehicleID, eSeat )
					PED_INDEX PassengerPedID = GET_PED_IN_VEHICLE_SEAT(VehicleID, eSeat) 	
					IF NOT (PassengerPedID = GET_PLAYER_PED(PlayerID))
						IF IS_PED_A_PLAYER(PassengerPedID)
							PLAYER_INDEX OtherPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(PassengerPedID)
							IF (GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(OtherPlayerID, PlayerID))
								RETURN(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

 //FEATURE_GANG_BOSS

PROC DISABLE_MENTAL_STATE_BLIP_COLOURS(BOOL bDisable)

	IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()	
		DEBUG_PRINTCALLSTACK()
		NET_PRINT("[player_blips] DISABLE_MENTAL_STATE_BLIP_COLOURS - called from within launch script. I do not like this. Neilf") NET_NL()
		SCRIPT_ASSERT("DISABLE_MENTAL_STATE_BLIP_COLOURS - called from within launch script.")
		EXIT	
	ENDIF

	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "DISABLE_MENTAL_STATE_BLIP_COLOURS - called with ", bDisable)

	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.DisableMentalStateColouringThreadID)
	OR (g_PlayerBlipsData.DisableMentalStateColouringThreadID = GET_ID_OF_THIS_THREAD())	
		IF (bDisable)
			g_PlayerBlipsData.DisableMentalStateColouringThreadID = GET_ID_OF_THIS_THREAD()
		ELSE
			CPRINTLN(DEBUG_BLIP, "DISABLE_SECONDARY_OUTLINES_FOR_PLAYER_BLIP - clearing thread id")
			g_PlayerBlipsData.DisableMentalStateColouringThreadID = INT_TO_NATIVE(THREADID, -1)
		ENDIF
	ELSE
		CASSERTLN(DEBUG_BLIP, "DISABLE_SECONDARY_OUTLINES_FOR_PLAYER_BLIP - already being used by another thread")
	ENDIF

ENDPROC

FUNC BOOL ARE_MENTAL_STATE_BLIP_COLOURS_DISABLED()
	#IF IS_DEBUG_BUILD
		IF (g_PlayerBlipsData.bBypassThreadChecks)
			RETURN(TRUE)
		ENDIF
	#ENDIF	
	IF IS_THREAD_ACTIVE(g_PlayerBlipsData.DisableMentalStateColouringThreadID)
		RETURN(TRUE)		
	ENDIF
	RETURN(FALSE)
ENDFUNC


PROC SET_PLAYER_BLIP_NAME_CORRECTLY(PLAYER_INDEX PlayerID)
	INT iPlayerIDInt = NATIVE_TO_INT(PlayerID)

	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
		IF IS_PLAYER_USING_CUSTOM_BLIP_NAME(PlayerID)
		//AND NOT IS_LANGUAGE_NON_ROMANIC()
			BEGIN_TEXT_COMMAND_SET_BLIP_NAME("CUSPLNM")
				IF g_PlayerBlipsData.bCustomBlipNameIsLiteral[NATIVE_TO_INT(PlayerID)]
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_PlayerBlipsData.strCustomBlipName[iPlayerIDInt])
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_PlayerBlipsData.strCustomBlipName[iPlayerIDInt])
				ENDIF
			END_TEXT_COMMAND_SET_BLIP_NAME(g_PlayerBlipsData.playerBlips[iPlayerIDInt])				
		ELSE
			SET_BLIP_NAME_TO_PLAYER_NAME(g_PlayerBlipsData.playerBlips[iPlayerIDInt], PlayerID)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets the blip colour based on the players colour. Takes teams into consideration.
/// PARAMS:
///    playerID - A valid and active player.
///    playerBlip - A valid blip.
PROC SET_PLAYER_BLIP_COLOUR(PLAYER_INDEX playerID, BLIP_INDEX &playerBlip)
	
	#IF IS_DEBUG_BUILD
		g_PlayerBlipsData.bSetPlayerColourCalled = TRUE
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (g_PlayerBlipsData.bDisableSetBlipColour)
			EXIT
		ENDIF
	#ENDIF
	
	INT iBlipColour
	INT iNewBlipColour
	
	// current blips colour
	iBlipColour = GET_BLIP_COLOUR(playerBlip)
	
	IF IS_PLAYER_USING_CUSTOM_BLIP_COLOUR(playerID)
		iNewBlipColour = g_PlayerBlipsData.iCustomBlipColour[NATIVE_TO_INT(playerID)]
	ELSE
	
		HUD_COLOURS HudColour 
		
		
		IF NOT (PlayerID = MPGlobals.LocalPlayerID)
		
			// default colour
			HudColour = GET_PLAYER_HUD_COLOUR(PlayerID, DEFAULT, DEFAULT, TRUE)
			
			// does player have a bounty?
			IF SHOULD_PLAYER_HAVE_BOUNTY_BLIP(PlayerID)
				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID)
				AND (GET_GANG_ID_FOR_PLAYER(playerID) != -1)
				AND NOT GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
					HudColour = GET_HUD_COLOUR_FOR_GANG_ID(GET_GANG_ID_FOR_PLAYER(playerID))
				ELSE
					HudColour = HUD_COLOUR_RED
				ENDIF
			ENDIF
			
			IF SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(PlayerID)
				IF SHOULD_PLAYER_HAVE_GUNRUN_BLIP(PlayerID) // 3646745
				AND GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID)
				AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
					HudColour = GET_HUD_COLOUR_FOR_GANG_ID(GET_GANG_ID_FOR_PLAYER(playerID))
				ELIF SHOULD_PLAYER_HAVE_SMUGGLER_BLIP(PlayerID)
				AND GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID)
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
						HudColour = GET_HUD_COLOUR_FOR_GANG_ID(GET_GANG_ID_FOR_PLAYER(playerID))
					ELIF GB_SHOULD_RIVAL_SMUGGLER_ENTITY_BE_BLIPPED_TO_STEAL(PlayerID)
						HudColour = HUD_COLOUR_GREEN
					ELSE
						HudColour = HUD_COLOUR_RED
					ENDIF					
				ELIF SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP(PlayerID)
				AND GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID)
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
						HudColour = GET_HUD_COLOUR_FOR_GANG_ID(GET_GANG_ID_FOR_PLAYER(playerID))
					ELSE
						HudColour = HUD_COLOUR_GREEN	
					ENDIF
				ELIF SHOULD_PLAYER_HAVE_FMBB_BLIP(PlayerID)
				AND GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID)
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
						HudColour = GET_HUD_COLOUR_FOR_GANG_ID(GET_GANG_ID_FOR_PLAYER(playerID))
					ELSE
						HudColour = HUD_COLOUR_GREEN
					ENDIF
				ELIF SHOULD_PLAYER_HAVE_RANDOM_EVENT_BLIP(PlayerID)
					HudColour = GET_RANDOM_EVENT_BLIP_COLOUR()
				#IF FEATURE_CASINO_HEIST
				ELIF SHOULD_PLAYER_HAVE_CASINO_HEIST_BLIP(PlayerID)
				AND GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerID)
					IF GB_IS_CSH_VARIATION_A_CELEB_MISSION(GB_GET_CASINO_HEIST_PREP_PLAYER_IS_ON(playerID))
						HudColour = HUD_COLOUR_BLUE
					ELIF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(PlayerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
						HudColour = GET_HUD_COLOUR_FOR_GANG_ID(GET_GANG_ID_FOR_PLAYER(PlayerID))
					ELSE
						HudColour = HUD_COLOUR_GREEN
					ENDIF
				#ENDIF
				ELIF SHOULD_PLAYER_HAVE_FM_CONTENT_BLIP(PlayerId)
					IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
						HudColour = GET_HUD_COLOUR_FOR_GANG_ID(GET_GANG_ID_FOR_PLAYER(playerID))
					ELSE
						HudColour = HUD_COLOUR_GREEN	
					ENDIF
				ELSE
					HudColour = HUD_COLOUR_RED
				ENDIF
			ENDIF
		ENDIF
		
		
		IF ((HudColour = HUD_COLOUR_PURE_WHITE) OR (HudColour = HUD_COLOUR_WHITE))
		AND NOT IS_PLAYER_ON_ANY_FM_MISSION(playerID)
		AND NOT (g_bFM_ON_IMPROMPTU_DEATHMATCH)
		AND NOT ARE_MENTAL_STATE_BLIP_COLOURS_DISABLED() 	
		
			// is player in gang?
			// check if this player is in a gang
			INT iGangID = GET_GANG_ID_FOR_PLAYER(PlayerID)
			IF NOT (iGangID = -1)		
				HudColour =  GET_HUD_COLOUR_FOR_GANG_ID( iGangID )	
				iNewBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HudColour)
			ELSE
		
				INT rM, gM, bM, aM		
				GET_COLOUR_FOR_MENTAL_STATE(GlobalplayerBD[NATIVE_TO_INT(playerID)].fMentalState, rM, gM, bM, aM)		
				iNewBlipColour = (rM * 16777216) + (gM * 65536) + (bM * 256) + (aM)	
			
			ENDIF
			
		ELSE
		
			iNewBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HudColour)
			
		ENDIF
			
			
	ENDIF
	
	IF g_bHackerTruckBlipBlue
	AND GET_BLIP_SPRITE(playerBlip) = RADAR_TRACE_BAT_WP1
	AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID))
		IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
		AND GET_SEAT_PED_IS_IN(GET_PLAYER_PED(playerID)) = VS_DRIVER
			VEHICLE_INDEX vehPlayerIn = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(playerID))
			IF IS_VEHICLE_A_PERSONAL_HACKER_TRUCK(vehPlayerIn)
			AND GET_OWNER_OF_PERSONAL_HACKER_TRUCK(vehPlayerIn) = GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID)
				PRINTLN("[player_blips] Turning hacker truck player blip blue - on a mission 1")
				iNewBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_BLUE)
			ENDIF
		ELIF IS_PLAYER_IN_HACKER_TRUCK(playerID)
		AND GET_OWNER_OF_HACKER_TRUCK_THAT_PLAYER_IS_INSIDE(PlayerID) = GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID)
			PRINTLN("[player_blips] Turning hacker truck player blip blue - on a mission 2")
			iNewBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_BLUE)
		ENDIF
	ENDIF
	
	// should blip colour be change?
	IF NOT (iBlipColour = iNewBlipColour)
		SET_BLIP_COLOUR(playerBlip, iNewBlipColour )	
		SET_PLAYER_BLIP_NAME_CORRECTLY(playerID) // because setting it to colour blue makes the name 'friendly' see 3173529
		PRINTLN("[player_blips] SET_PLAYER_BLIP_COLOUR - changing blip colour for player ", NATIVE_TO_INT(playerID), " from ", iBlipColour, " to ", iNewBlipColour)		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		g_PlayerBlipsData.bSetPlayerColourCalled = FALSE
	#ENDIF	
	
ENDPROC


PROC SET_BLIP_COLOUR_FROM_TEAM(BLIP_INDEX &blipID, INT iTeam)
	SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipID, GET_TEAM_HUD_COLOUR(iTeam))
ENDPROC 

FUNC BOOL SHOULD_SHOW_BLIP_HEIGHT_FOR_LOCAL_PLAYER(BOOL &bUseExtendedHeightThreshold)
	IF (g_b_On_Deathmatch)
	OR Is_Player_Currently_On_MP_LTS_Mission(MPGlobals.LocalPlayerID)
	OR Is_Player_Currently_On_MP_Versus_Mission(MPGlobals.LocalPlayerID)
	OR Is_Player_Currently_On_MP_CTF_Mission(MPGlobals.LocalPlayerID)
	OR SHOULD_ALLOW_PLAYER_HEIGHTS()
		bUseExtendedHeightThreshold = FALSE
		RETURN(TRUE)
	ENDIF
	
	PED_INDEX LocalPlayerPedID = GET_PLAYER_PED(MPGlobals.LocalPlayerID)
	IF NOT IS_ENTITY_DEAD(LocalPlayerPedID)
		IF IS_PED_IN_ANY_HELI(LocalPlayerPedID)
		OR IS_PED_IN_ANY_PLANE(LocalPlayerPedID)
			bUseExtendedHeightThreshold = TRUE
			RETURN(TRUE)		
		ENDIF	
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL SHOULD_SHOW_HEIGHT_ON_BLIP(BLIP_INDEX BlipID, BOOL &bUseExtendedHeightThreshold)

	eRADAR_BLIP_TYPE ThisBlipType = GET_BLIP_INFO_ID_TYPE(BlipID) 

	//CPRINTLN(DEBUG_BLIP, "[blips] SHOULD_SHOW_HEIGHT_ON_BLIP - ThisBlipType = ", ENUM_TO_INT(ThisBlipType))

	IF (ThisBlipType = BLIPTYPE_VEHICLE)
	OR (ThisBlipType = BLIPTYPE_CHAR)
	OR (ThisBlipType = BLIPTYPE_OBJECT)

		RETURN SHOULD_SHOW_BLIP_HEIGHT_FOR_LOCAL_PLAYER(bUseExtendedHeightThreshold)
		
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL SHOULD_SHOW_HEIGHT_FOR_PLAYER(PLAYER_INDEX PlayerID)
	IF NOT SHOULD_ALLOW_PLAYER_HEIGHTS()
		RETURN(FALSE)
	ENDIF
	IF SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(PlayerID)
		RETURN(FALSE)
	ENDIF
	RETURN(TRUE)
ENDFUNC

//PROC UpdateLongRangeBlipForPlayer(PLAYER_INDEX playerID)
//	INT iPlayerIDInt = NATIVE_TO_INT(PlayerID)
//	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
//		IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayersBlipIsLongRange, iPlayerIDInt)
//			IF IS_BLIP_SHORT_RANGE(g_PlayerBlipsData.playerBlips[iPlayerIDInt])	
//				SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerID)], FALSE)
//				DEBUG_PRINTCALLSTACK()
//				CPRINTLN(DEBUG_BLIP, "UpdateLongRangeBlipForPlayer - setting player blip as long range ", iPlayerIDInt) 
//			ENDIF
//		ELSE
//			IF NOT IS_BLIP_SHORT_RANGE(g_PlayerBlipsData.playerBlips[iPlayerIDInt])	
//				SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerID)], TRUE)
//				DEBUG_PRINTCALLSTACK()
//				CPRINTLN(DEBUG_BLIP, "UpdateLongRangeBlipForPlayer - setting player blip as short range ", iPlayerIDInt) 
//			ENDIF	
//		ENDIF
//	ENDIF
//ENDPROC

//PROC SetPlayerBlipAsLongRange_Internal(PLAYER_INDEX playerID, BOOL bIsLongRange)
//	IF (bIsLongRange)
//		IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayersBlipIsLongRange, NATIVE_TO_INT(playerId))
//			CPRINTLN(DEBUG_BLIP, "SetPlayerBlipAsLongRange_Internal - setting bit bs_PlayersBlipIsLongRange ", NATIVE_TO_INT(playerID))
//			SET_BIT(g_PlayerBlipsData.bs_PlayersBlipIsLongRange, NATIVE_TO_INT(playerId))
//		ENDIF
//	ELSE
//		IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayersBlipIsLongRange, NATIVE_TO_INT(playerId))
//			CPRINTLN(DEBUG_BLIP, "SetPlayerBlipAsLongRange_Internal - clearing bit bs_PlayersBlipIsLongRange ", NATIVE_TO_INT(playerID))
//			CLEAR_BIT(g_PlayerBlipsData.bs_PlayersBlipIsLongRange, NATIVE_TO_INT(playerId))
//		ENDIF
//	ENDIF
//	UpdateLongRangeBlipForPlayer(playerID)
//ENDPROC

PROC SET_PLAYER_BLIP_AS_LONG_RANGE(PLAYER_INDEX playerID, BOOL bIsLongRange)
	IF (bIsLongRange)
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayersBlipIsLongRange,  NATIVE_TO_INT(playerId))
			DEBUG_PRINTCALLSTACK()
			PRINTLN("SET_PLAYER_BLIP_AS_LONG_RANGE, changing player ", NATIVE_TO_INT(playerID), ", bIsLongRange = ", bIsLongRange)
		ENDIF
		#ENDIF
	
		SET_BIT(g_PlayerBlipsData.bs_PlayersBlipIsLongRange, NATIVE_TO_INT(playerId))
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayersBlipIsLongRange,  NATIVE_TO_INT(playerId))
			DEBUG_PRINTCALLSTACK()
			PRINTLN("SET_PLAYER_BLIP_AS_LONG_RANGE, changing player ", NATIVE_TO_INT(playerID), ", bIsLongRange = ", bIsLongRange)
		ENDIF
		#ENDIF	
	
		CLEAR_BIT(g_PlayerBlipsData.bs_PlayersBlipIsLongRange, NATIVE_TO_INT(playerId))
	ENDIF
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerID)])
		IF (bIsLongRange)
			SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerID)], FALSE)
		ELSE
			SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerID)], TRUE)
		ENDIF
	ENDIF
ENDPROC



PROC SET_PLAYER_BLIP_HEIGHT_INDICATOR(PLAYER_INDEX PlayerID)
	INT iPlayerIDInt = NATIVE_TO_INT(PlayerID)
	BOOL bUseExtendedHeightThreshold
	IF SHOULD_SHOW_HEIGHT_FOR_PLAYER(PlayerID)
		SHOW_HEIGHT_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], SHOULD_SHOW_BLIP_HEIGHT_FOR_LOCAL_PLAYER(bUseExtendedHeightThreshold)) 
		SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(g_PlayerBlipsData.playerBlips[iPlayerIDInt], bUseExtendedHeightThreshold)
	ELSE
		SHOW_HEIGHT_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], FALSE)	
	ENDIF
ENDPROC

FUNC INT GET_PROPER_BLIP_ROTATION_FOR_PED(PED_INDEX PedID)
	
	FLOAT fHeading
//	VEHICLE_INDEX VehicleID
	
//	IF IS_PED_IN_ANY_VEHICLE(PedID)
//		VehicleID = GET_VEHICLE_PED_IS_USING(PedID)
//		IF DOES_ENTITY_EXIST(VehicleID)
//		AND NOT IS_ENTITY_DEAD(VehicleID)
//			fHeading = GET_ENTITY_HEADING_FROM_EULERS(VehicleID)	
//		ELSE
//			fHeading = GET_ENTITY_HEADING_FROM_EULERS(PedID)
//		ENDIF
//	ELSE
//		fHeading = GET_ENTITY_HEADING_FROM_EULERS(PedID)
//	ENDIF
		
	fHeading = GET_ENTITY_HEADING_FROM_EULERS(PedID)

	RETURN ROUND(fHeading)

ENDFUNC

FUNC INT GET_PROPER_BLIP_ROTATION_FOR_PLAYER(PLAYER_INDEX PlayerID)
	
	RETURN GET_PROPER_BLIP_ROTATION_FOR_PED(GET_PLAYER_PED(PlayerID))

ENDFUNC

PROC UPDATE_MANUALLY_ROTATATED_BLIP_FOR_PLAYER(PLAYER_INDEX PlayerID)
	INT iPlayerIDInt
	iPlayerIDInt = NATIVE_TO_INT(playerID)	
	// should we rotate the blip?
	IF (iPlayerIDInt > -1)
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			IF SHOULD_BLIP_BE_MANUALLY_ROTATED(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
				IF DOES_ENTITY_EXIST(GET_PLAYER_PED(PlayerID))
				AND NOT IS_ENTITY_DEAD(GET_PLAYER_PED(PlayerID))
					SET_BLIP_ROTATION(g_PlayerBlipsData.playerBlips[iPlayerIDInt], GET_PROPER_BLIP_ROTATION_FOR_PLAYER(PlayerID))
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC SET_PLAYER_BLIP_INITIAL_VALUES(PLAYER_INDEX PlayerID)
	INT iPlayerIDInt
	iPlayerIDInt = NATIVE_TO_INT(playerID)	
	#IF IS_DEBUG_BUILD 
		IF NOT (g_PlayerBlipsData.bDisableHeightIndicators)
	#ENDIF	
	SET_PLAYER_BLIP_HEIGHT_INDICATOR(PlayerID)
	#IF IS_DEBUG_BUILD 
		ENDIF
	#ENDIF	
	SET_PLAYER_BLIP_COLOUR(PlayerID, g_PlayerBlipsData.playerBlips[iPlayerIDInt])	
	SET_BLIP_CATEGORY(g_PlayerBlipsData.playerBlips[iPlayerIDInt], BLIP_CATEGORY_PLAYER)
	IF IS_PLAYER_BLIP_SET_AS_LONG_RANGE(PlayerID)
		SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerID)], FALSE)
	ELSE
		SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(playerID)], TRUE)
	ENDIF
	SET_PLAYER_BLIP_NAME_CORRECTLY(PlayerID)
	CLEAR_BIT(g_PlayerBlipsData.bs_IllicitGoodsShortRange, iPlayerIDInt)
	
	// set the blip rotation as well
	UPDATE_MANUALLY_ROTATATED_BLIP_FOR_PLAYER(PlayerID)
	
ENDPROC

FUNC BOOL IS_HUD_COLOUR_FRIENDLY(HUD_COLOURS HudColour)
	IF (HudColour = FRIENDLY_DM_COLOUR(TRUE))
	OR (HudColour = FRIENDLY_DM_COLOUR(FALSE))
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL DO_PLAYERS_HAVE_SAME_TEAM_COLOURS(PLAYER_INDEX PlayerID, PLAYER_INDEX PlayerID2, BOOL bConsiderFriendlyBluesTheSame=TRUE)
	IF GET_PLAYER_HUD_COLOUR(PlayerID) = GET_PLAYER_HUD_COLOUR(PlayerID2)
		RETURN(TRUE)
	ENDIF
	IF (bConsiderFriendlyBluesTheSame)
		IF IS_HUD_COLOUR_FRIENDLY(GET_PLAYER_HUD_COLOUR(PlayerID)) 
		AND IS_HUD_COLOUR_FRIENDLY(GET_PLAYER_HUD_COLOUR(PlayerID2)) 		
			RETURN(TRUE)
		ENDIF
	ENDIF	
	RETURN(FALSE)
ENDFUNC

PROC SET_HEADING_INDICATOR_FOR_PLAYER(INT iPlayerIDInt, BOOL bSet)
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
		IF (bSet)
			IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayerIndicatorOn, iPlayerIDInt)
				
				#IF IS_DEBUG_BUILD
					IF NOT (g_PlayerBlipsData.bDisableHeadingIndicator)
				#ENDIF
				SHOW_HEADING_INDICATOR_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], TRUE)
				#IF IS_DEBUG_BUILD
					ENDIF
				#ENDIF
				
				SET_BIT(g_PlayerBlipsData.bs_PlayerIndicatorOn, iPlayerIDInt)	
				CPRINTLN(DEBUG_BLIP, "SET_HEADING_INDICATOR_FOR_PLAYER - setting heading indicator ON for player ", iPlayerIDInt) 
			ENDIF
		ELSE
			IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerIndicatorOn, iPlayerIDInt)
				#IF IS_DEBUG_BUILD
					IF NOT (g_PlayerBlipsData.bDisableHeadingIndicator)
				#ENDIF
				SHOW_HEADING_INDICATOR_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], FALSE)
				#IF IS_DEBUG_BUILD
					ENDIF
				#ENDIF
				CLEAR_BIT(g_PlayerBlipsData.bs_PlayerIndicatorOn, iPlayerIDInt)	
				CPRINTLN(DEBUG_BLIP, "SET_HEADING_INDICATOR_FOR_PLAYER - setting heading indicator OFF for player ", iPlayerIDInt) 
			ENDIF
		ENDIF
	ELSE
		// if blip doesn't exist make sure the bit is cleared
		IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerIndicatorOn, iPlayerIDInt)	
			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerIndicatorOn, iPlayerIDInt)
			CPRINTLN(DEBUG_BLIP, "SET_HEADING_INDICATOR_FOR_PLAYER - blip doesn't exist, clearing bit for player ", iPlayerIDInt)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_BLIP_SPRITE_HAVE_HEADING_INDICATOR(BLIP_SPRITE BlipSprite)
	IF (BlipSprite = GET_STANDARD_BLIP_ENUM_ID())
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL CAN_BLIP_HAVE_HEADING_INDICATOR(BLIP_INDEX BlipID)
	IF CAN_BLIP_SPRITE_HAVE_HEADING_INDICATOR(GET_BLIP_SPRITE(BlipID))
	AND (GET_BLIP_ALPHA(BlipID) > 0)
	AND GET_BLIP_FADE_DIRECTION(BlipID) = 0
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL CAN_BLIP_SPRITE_HAVE_SECONDARY_CIRCLE(BLIP_SPRITE BlipSprite)
	IF IS_PLAYER_ON_ANY_MP_MISSION(MPGlobals.LocalPlayerID)
		SWITCH BlipSprite
			CASE RADAR_TRACE_TANK
			CASE RADAR_TRACE_PLAYER_JET
			CASE RADAR_TRACE_PLAYER_PLANE
			CASE RADAR_TRACE_PLAYER_HELI	
			CASE RADAR_TRACE_ENEMY_HELI_SPIN
			CASE RADAR_TRACE_HELICOPTER
			CASE RADAR_TRACE_PLAYER_GUNCAR
			CASE RADAR_TRACE_PLAYER_BOAT
				RETURN(FALSE)
			BREAK	
		ENDSWITCH
	ENDIF
	RETURN(TRUE)
ENDFUNC


FUNC BOOL IS_PLAYER_BLIP_ALPHA_CONTROLLED_BY_NATIVE(INT iPlayerIDInt)
	
	#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(g_PlayerBlipsData.bs_FadeOutBlip, iPlayerIDInt)
			CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_BLIP_ALPHA_CONTROLLED_BY_NATIVE - fadeoutblip is set for player ", iPlayerIDInt)
		ENDIF
		IF IS_BIT_SET(g_PlayerBlipsData.bs_PulseBlip, iPlayerIDInt)
			CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_BLIP_ALPHA_CONTROLLED_BY_NATIVE - bs_PulseBlip is set for player ", iPlayerIDInt)
		ENDIF
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			INT iFadeDir = GET_BLIP_FADE_DIRECTION(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			IF (iFadeDir != 0)
				CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_BLIP_ALPHA_CONTROLLED_BY_NATIVE - fade direction is ", iFadeDir, " for player ", iPlayerIDInt)
			ENDIF
		ENDIF
	#ENDIF

	IF IS_BIT_SET(g_PlayerBlipsData.bs_FadeOutBlip, iPlayerIDInt)		
	OR IS_BIT_SET(g_PlayerBlipsData.bs_PulseBlip, iPlayerIDInt)
	//OR IS_BIT_SET(g_PlayerBlipsData.bs_BlipIsPulsing, iPlayerIDInt)
		RETURN(TRUE)
	ENDIF
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
		IF (GET_BLIP_FADE_DIRECTION(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) != 0)
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL CAN_BLIP_HAVE_SECONDARY_CIRCLE(BLIP_INDEX BlipID)
	IF DOES_BLIP_EXIST(BlipID)
		INT iAlpha = GET_BLIP_ALPHA(BlipID)
		IF (iAlpha = 255)
		OR ((iAlpha >= 128) AND (GET_BLIP_FADE_DIRECTION(BlipID) > 0))
			RETURN(CAN_BLIP_SPRITE_HAVE_SECONDARY_CIRCLE(GET_BLIP_SPRITE(BlipID)))
		ENDIF
	ENDIF	
	RETURN(FALSE)	
ENDFUNC

FUNC BOOL LOCAL_PLAYER_SHOULD_PRIORITISE_TEAMMATE_BLIPS()
	INT iTeam = GET_PLAYER_TEAM(MPGlobals.LocalPlayerID)
	IF (iTeam > -1)
	AND (iTeam < FMMC_MAX_TEAMS)
		RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_PRIORTISE_TEAM_BLIPS)
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BLIP_PRIORITY GET_CORRECT_PLAYER_BLIP_PRIORITY(BLIP_PRIORITY_TYPE TypeOfBlip)
	
	SWITCH TypeOfBlip
		CASE BP_OTHER_TEAM
		CASE BP_SAME_TEAM
			IF LOCAL_PLAYER_SHOULD_PRIORITISE_TEAMMATE_BLIPS()	
				IF (TypeOfBlip = BP_OTHER_TEAM)
					TypeOfBlip = BP_SAME_TEAM
				ELSE
					TypeOfBlip = BP_OTHER_TEAM
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

	RETURN GET_CORRECT_BLIP_PRIORITY(TypeOfBlip)

ENDFUNC

FUNC BLIP_PRIORITY GET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PLAYER_INDEX PlayerID)
	INT iPlayerIDInt
	iPlayerIDInt = NATIVE_TO_INT(playerID)	
	BLIP_SPRITE ThisRadarTrace
	IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			ThisRadarTrace = GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			
#IF FEATURE_FREEMODE_ARCADE
#IF FEATURE_COPS_N_CROOKS
	IF IS_FREEMODE_ARCADE()
		IF GET_ARCADE_MODE() = ARC_COPS_CROOKS		
			IF GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.bIsClosestEnemyPlayer = 1
				RETURN BLIPPRIORITY_LOW 	
			ENDIF
		ENDIF
	ENDIF
#ENDIF	
#ENDIF
			IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerInVehicleThatHasBlip, iPlayerIDInt)
			OR IS_BIT_SET(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerIDInt)
				RETURN BLIPPRIORITY_LOWEST
			ELSE
			
				IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerBlipHighlighted, iPlayerIDInt)	
				OR IS_BIT_SET(g_PlayerBlipsData.bs_PlayerBlipSetToFlash, iPlayerIDInt)
				OR IS_BIT_SET(g_PlayerBlipsData.bs_PlayerUsingCustomSprite, iPlayerIDInt)
					RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_HIGHLIGHTED)
				ELSE
				
					SWITCH ThisRadarTrace
						CASE RADAR_TRACE_PLAYERSTATE_CUSTODY
						CASE RADAR_TRACE_PLAYERSTATE_ARRESTED
						CASE RADAR_TRACE_PLAYERSTATE_KEYHOLDER
							RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_CUFFED_OR_KEYS)
						BREAK
						CASE RADAR_TRACE_GANG_WANTED_1
						CASE RADAR_TRACE_GANG_WANTED_2
						CASE RADAR_TRACE_GANG_WANTED_3
						CASE RADAR_TRACE_GANG_WANTED_4
						CASE RADAR_TRACE_GANG_WANTED_5				
							RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_WANTED)
						BREAK
						CASE RADAR_TRACE_PLAYERSTATE_DRIVING
							IF GET_PLAYER_TEAM(PlayerID) = GET_PLAYER_TEAM(MPGlobals.LocalPlayerID)
								RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_SAME_TEAM)
							ELSE
								RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_OTHER_TEAM)
							ENDIF
						BREAK
						CASE RADAR_TRACE_ON_MISSION
							IF GET_PLAYER_TEAM(PlayerID) = GET_PLAYER_TEAM(MPGlobals.LocalPlayerID)
								RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_SAME_TEAM)	
							ELSE
								RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_OTHER_TEAM)
							ENDIF								
						BREAK
						CASE RADAR_TRACE_PASSIVE
						CASE RADAR_TRACE_USINGMENU
							IF ARE_PLAYERS_ON_SAME_TEAM( MPGlobals.LocalPlayerID, playerID )
								RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_SAME_TEAM)	
							ELSE
								RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_OTHER_TEAM)
							ENDIF
						BREAK
						CASE RADAR_TRACE_BOUNTY_HIT
						CASE RADAR_TRACE_BOUNTY_HIT_INSIDE
							RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_HIGHLIGHTED)
						BREAK
						CASE RADAR_TRACE_INCAPACITATED
							IF DO_PLAYERS_HAVE_SAME_TEAM_COLOURS( MPGlobals.LocalPlayerID, playerID )
								RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_HIGHLIGHTED)	
							ELSE
								RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_OTHER_TEAM)
							ENDIF							
						BREAK
						CASE RADAR_TRACE_CONTRABAND
						CASE RADAR_TRACE_PACKAGE
						CASE RADAR_TRACE_SPORTS_CAR
						CASE RADAR_TRACE_SUPPLIES
							RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_HIGHLIGHTED)
						BREAK
						CASE RADAR_TRACE_LEVEL_INSIDE
							IF IS_PLAYER_IN_CASINO(playerID)
							OR IS_PLAYER_IN_CASINO_APARTMENT(playerID)
							OR IS_PLAYER_IN_CASINO_VALET_GARAGE(playerID)
								IF DO_PLAYERS_HAVE_SAME_TEAM_COLOURS( MPGlobals.LocalPlayerID, playerID )
									RETURN BLIPPRIORITY_LOW 	
								ELSE
									RETURN BLIPPRIORITY_LOW_MED 
								ENDIF	
							ELSE
								IF DO_PLAYERS_HAVE_SAME_TEAM_COLOURS( MPGlobals.LocalPlayerID, playerID )
									RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_SAME_TEAM)	
								ELSE
									RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_OTHER_TEAM)
								ENDIF	
							ENDIF
						BREAK
						
						
						CASE RADAR_TRACE_PLAYERSTATE_PARTNER
						CASE RADAR_TRACE_GANG_HIGHLIGHT
						DEFAULT // standard blip enum
							IF DO_PLAYERS_HAVE_SAME_TEAM_COLOURS( MPGlobals.LocalPlayerID, playerID )
								RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_SAME_TEAM)	
							ELSE
								RETURN GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_OTHER_TEAM)							
							ENDIF					
						BREAK
						
						
					ENDSWITCH			
								
				
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN BLIPPRIORITY_LOWEST
ENDFUNC

PROC SET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PLAYER_INDEX PlayerID)
	INT iPlayerIDInt
	iPlayerIDInt = NATIVE_TO_INT(playerID)	
	IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
		
			g_PlayerBlipsData.PlayerBlipPriority[iPlayerIDInt]  = GET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PlayerID)
			
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisablePriority)
			#ENDIF
			SET_BLIP_PRIORITY(g_PlayerBlipsData.playerBlips[iPlayerIDInt], g_PlayerBlipsData.PlayerBlipPriority[iPlayerIDInt])
			
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
			
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PLAYER_INDEX PlayerID)
	BLIP_PRIORITY ThisPriority
	INT iPlayerIDInt
	iPlayerIDInt = NATIVE_TO_INT(playerID)		
	IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])	
			ThisPriority = GET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PlayerID)
			IF NOT (ThisPriority = g_PlayerBlipsData.PlayerBlipPriority[iPlayerIDInt])
				CPRINTLN(DEBUG_BLIP, "UPDATE_CORRECT_BLIP_PRIORITY_FOR_PLAYER - changing priority from ", g_PlayerBlipsData.PlayerBlipPriority[iPlayerIDInt], " to ", ThisPriority,  " for player ", iPlayerIDInt)			
				SET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PlayerID)
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC MAKE_PLAYERS_BLIP_THIS_SPRITE(PLAYER_INDEX PlayerID, BLIP_SPRITE ThisRadarTrace)
	INT iPlayerIDInt
	iPlayerIDInt = NATIVE_TO_INT(playerID)
	
	IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			
			SET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt], ThisRadarTrace)
			#IF IS_DEBUG_BUILD
				//IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] MAKE_PLAYERS_BLIP_THIS_SPRITE - SET_BLIP_SPRITE - called on player id = ", iPlayerIDInt, ", radar trace ", ThisRadarTrace)
				//ENDIF
			#ENDIF
			
			SET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PlayerID)
			
			// we need to reset the secondary blip stuff so it gets re-applied in correct order (code peculiarity)
			RESET_SECONDARY_DATA_FOR_PLAYER_BLIP(iPlayerIDInt)
		
			SET_PLAYER_BLIP_INITIAL_VALUES(PlayerID)
		ENDIF
	ENDIF
	
ENDPROC


PROC MAKE_PLAYERS_BLIP_A_DEAD_BLIP(PLAYER_INDEX PlayerID)
	
	INT iPlayerIDInt
	iPlayerIDInt = NATIVE_TO_INT(playerID)
	
	CPRINTLN(DEBUG_BLIP, "[player_blips] MAKE_PLAYERS_BLIP_A_DEAD_BLIP - called by ", GET_THIS_SCRIPT_NAME(), ", with player ", iPlayerIDInt )
	DEBUG_PRINTCALLSTACK()
	
	// only add dead blps for player who haven't swapped or left session
	IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
		IF NOT SHOULD_HIDE_DEAD_PLAYER_BLIPS()

			VECTOR vPos = GET_ENTITY_COORDS(GET_PLAYER_PED(playerID), FALSE)
			PRINTLN(DEBUG_BLIP, "[player_blips] MAKE_PLAYERS_BLIP_A_DEAD_BLIP - vPos ", vPos)
			
			IF (playerID = MPGlobals.LocalPlayerID)
			AND VMAG(g_PlayerBlipsData.vPerceivedDeathCoords) > 0.001
				vPos = g_PlayerBlipsData.vPerceivedDeathCoords
				PRINTLN(DEBUG_BLIP, "[player_blips] MAKE_PLAYERS_BLIP_A_DEAD_BLIP - vPos - perceived ", vPos)
			ENDIF
		
	
			g_PlayerBlipsData.deadPlayerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD(vPos)
			
			SET_BLIP_SPRITE(g_PlayerBlipsData.deadPlayerBlips[iPlayerIDInt], RADAR_TRACE_DEAD) //RADAR_TRACE_INCAPACITATED) //RADAR_TRACE_DEAD) // set this to a skull when we get it
			
			CPRINTLN(DEBUG_BLIP, "[player_blips] MAKE_PLAYERS_BLIP_A_DEAD_BLIP - dead blip added at ", vPos)
			
			SET_BLIP_PRIORITY(g_PlayerBlipsData.deadPlayerBlips[iPlayerIDInt], GET_CORRECT_PLAYER_BLIP_PRIORITY(BP_DYING))
			
			IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
				SET_BLIP_COLOUR(g_PlayerBlipsData.deadPlayerBlips[iPlayerIDInt], GET_BLIP_COLOUR(g_PlayerBlipsData.playerBlips[iPlayerIDInt]))
			ENDIF
					
			IF (iPlayerIDInt = NATIVE_TO_INT(MPGlobals.LocalPlayerID))
				BEGIN_TEXT_COMMAND_SET_BLIP_NAME("BLIP_DEATH")
				END_TEXT_COMMAND_SET_BLIP_NAME(g_PlayerBlipsData.deadPlayerBlips[iPlayerIDInt])				
			ELSE		
				SET_PLAYER_BLIP_NAME_CORRECTLY(PlayerID)
			ENDIF
					
			g_PlayerBlipsData.iBlipTimerDeadFading[iPlayerIDInt] = GET_NETWORK_TIME()
			CLEAR_BIT(g_PlayerBlipsData.bs_FadeOutDeadBlip, iPlayerIDInt)	
			
		ELSE
			CPRINTLN(DEBUG_BLIP, "[player_blips] MAKE_PLAYERS_BLIP_A_DEAD_BLIP - SHOULD_HIDE_DEAD_PLAYER_BLIPS = TRUE")
		ENDIF
		
	ENDIF

	CLEANUP_PLAYER_BLIP(playerID)

ENDPROC

FUNC BOOL IsPlayerBlipHiddenFromMyTeam(INT iPlayerIDInt)
	INT iMyTeam = GET_PLAYER_TEAM(MPGlobals.LocalPlayerID)
	IF iMyTeam > -1
		IF IS_BIT_SET(GlobalplayerBD[iPlayerIDInt].playerBlipData.iFlags, (PBBD_HIDDEN_FROM_TEAM_0 + iMyTeam))	
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_BLIP_HIDDEN_FROM_ME(PLAYER_INDEX PlayerID)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.iFlags, PBBD_IS_HIDDEN)	
	OR IS_PLAYER_BLIP_HIDDEN(playerID)
	OR IS_PLAYER_BLIP_HIDDEN_AS_PARTICIPANT_OF_EVENT(playerID)
	OR IS_PLAYER_BLIP_HIDDEN_TO_NON_GANG_MEMBERS(playerID)
	OR IsPlayerBlipHiddenFromMyTeam(NATIVE_TO_INT(playerID))
		
		
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.iFlags, PBBD_IS_HIDDEN)
					CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_BLIP_HIDDEN_FROM_ME - PBBD_IS_HIDDEN - player id =", NATIVE_TO_INT(playerID))
				ENDIF
				IF IS_PLAYER_BLIP_HIDDEN(playerID)
					CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_BLIP_HIDDEN_FROM_ME - IS_PLAYER_BLIP_HIDDEN - player id =", NATIVE_TO_INT(playerID))
				ENDIF
				IF IS_PLAYER_BLIP_HIDDEN_AS_PARTICIPANT_OF_EVENT(playerID)
					CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_BLIP_HIDDEN_FROM_ME - IS_PLAYER_BLIP_HIDDEN_AS_PARTICIPANT_OF_EVENT - player id =", NATIVE_TO_INT(playerID))
				ENDIF
				IF IS_PLAYER_BLIP_HIDDEN_TO_NON_GANG_MEMBERS(playerID)
					CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_BLIP_HIDDEN_FROM_ME - IS_PLAYER_BLIP_HIDDEN_TO_NON_GANG_MEMBERS - player id =", NATIVE_TO_INT(playerID))
				ENDIF
				IF IsPlayerBlipHiddenFromMyTeam(NATIVE_TO_INT(playerID))
					CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_BLIP_HIDDEN_FROM_ME - IsPlayerBlipHiddenFromMyTeam - player id =", NATIVE_TO_INT(playerID))
				ENDIF
			ENDIF
		#ENDIF		
		
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC



FUNC BOOL IS_PLAYER_MY_HIDDEN_GANG_BOSS(PLAYER_INDEX PlayerID)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.iFlags, PBBD_HIDDEN_FROM_NON_GANG_MEMBERS)
		IF GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID) = PlayerID
		
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_BLIP_MY_HIDDEN_GANG_BOSS - Player id =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

 //FEATURE_GANG_BOSS


/// PURPOSE:
///    Removes a blip and sets its id to null.
/// PARAMS:
///    BlipId - Blip id.
PROC FADE_CLEANUP_PLAYER_BLIP(PLAYER_INDEX playerID)
		
	INT iPlayerIDInt
	iPlayerIDInt = NATIVE_TO_INT(playerID)
	
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])

		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				VECTOR vBlipCoord = GET_BLIP_INFO_ID_COORD(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
				CPRINTLN(DEBUG_BLIP, "[player_blips] FADE_CLEANUP_PLAYER_BLIP - fading out blip at ", vBlipCoord)
			ENDIF
		#ENDIF
	
		// Delete blips for dead players.
		IF NOT IS_NET_PLAYER_OK(playerID)
			IF DOES_BLIP_EXIST(g_PlayerBlipsData.deadPlayerBlips[iPlayerIDInt])
				REMOVE_BLIP(g_PlayerBlipsData.deadPlayerBlips[iPlayerIDInt])
			ENDIF			
			MAKE_PLAYERS_BLIP_A_DEAD_BLIP(playerID)
			EXIT			
		ENDIF		
		
		IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_FadeOutBlip, iPlayerIDInt)
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.iFlags, PBBD_FADE_HIDE)
				SET_BLIP_FADE(g_PlayerBlipsData.playerBlips[iPlayerIDInt], 0, BLIP_HIDE_FADE_PERIOD)
			ELSE
				SET_BLIP_FADE(g_PlayerBlipsData.playerBlips[iPlayerIDInt], 0, BLIP_FADE_PERIOD)
			ENDIF
			
			SET_BIT(g_PlayerBlipsData.bs_FadeOutBlip, iPlayerIDInt)	
			#IF IS_DEBUG_BUILD
				//IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] FADE_CLEANUP_PLAYER_BLIP - SET_BLIP_FADE - called on blip for player ", iPlayerIDInt)
				//ENDIF
			#ENDIF
			
		ENDIF
				
		// if the screen is faded out then cleanup the blips straight away (so you don't see them fading out as the screen fades back in)
		IF IS_SCREEN_FADED_OUT()
		OR IS_SCREEN_FADING_IN()
		OR NOT NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(MPGlobals.LocalPlayerID, playerID) // remove immediately if player is in different tutorial as z will be different.
		OR IS_PLAYER_BLIP_HIDDEN_FROM_ME(playerID)
			IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.iFlags, PBBD_FADE_HIDE)
				SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[iPlayerIDInt], 0)
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] FADE_CLEANUP_PLAYER_BLIP - SET_BLIP_ALPHA - blip set to alpha 0 for player ", iPlayerIDInt)
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
				
		IF (GET_BLIP_ALPHA (g_PlayerBlipsData.playerBlips[iPlayerIDInt]) = 0)
			CLEANUP_PLAYER_BLIP( playerID)
		ELSE
			#IF IS_DEBUG_BUILD
				IF NOT (iPlayerIDInt = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[iPlayerIDInt], BLIP_PLAYER_REASON_FADING_OUT)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[iPlayerIDInt], BLIP_PLAYER_REASON_FADING_OUT)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF
		ENDIF
			
	ENDIF

ENDPROC



PROC DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
	DEBUG_PRINTCALLSTACK()
	g_PlayerBlipsData.bForceUpdateAllPlayerBlipsNow = TRUE
ENDPROC





/// PURPOSE:
///    Set if a player should be force blipped. This is local, other players will not blip the selected player.
///    This should be used when you want to blip a player that is not normally blipped.
///    You need to call this with bForceBlip = FALSE to stop blipping the player.
/// PARAMS:
///    playerId - The player you want to blip.
///    bForceBlip - Pass in true to force blip, false to remove blip.
PROC FORCE_BLIP_PLAYER(PLAYER_INDEX playerId, BOOL bForceBlip, BOOL bIsLongRange=FALSE, BOOL bForceThreadOwnership=FALSE)		

	IF BlipPreThreadLockFailCheck(PlayerID)
		EXIT	
	ENDIF

	BOOL bBitsetStateChanged
	IF DoBlipThreadOwnership(	g_PlayerBlipsData.ForceBlipThreadID[NATIVE_TO_INT(PlayerID)], 
							g_PlayerBlipsData.ForceBlipThreadID_Forced[NATIVE_TO_INT(PlayerID)],
							g_PlayerBlipsData.bs_PlayersToForceBlip,							
							bForceBlip,
							NATIVE_TO_INT(PlayerID),							
							bForceThreadOwnership,
							bBitsetStateChanged)
							
		SET_PLAYER_BLIP_AS_LONG_RANGE(playerId, bIsLongRange)
		
	ENDIF

//	IF (bForceBlip)	
//
//		IF IS_NET_PLAYER_OK(playerId)
//		
//			#IF IS_DEBUG_BUILD
//				IF NOT DOES_THIS_THREAD_HAVE_CONTROL_OF_FORCE_BLIP_PLAYER(playerId)
//					g_PlayerBlipsData.bs_PlayersToForceBlip = 0
//					CPRINTLN(DEBUG_BLIP, "[player_blips] FORCE_BLIP_PLAYER - already in use by ", g_PlayerBlipsData.ForceBlipScript[NATIVE_TO_INT(PlayerID)], ", perhaps it never cleaned up.")
//					CASSERTLN(DEBUG_BLIP, "FORCE_BLIP_PLAYER - Another script thread has already forced blips, only one script at a time can use force blips.")				
//				ENDIF							
//				g_PlayerBlipsData.ForceBlipScript[NATIVE_TO_INT(PlayerID)] = GET_THIS_SCRIPT_NAME()		
//			#ENDIF		
//			
//			g_PlayerBlipsData.ForceBlipThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD()
//			NET_PRINT_TIME() NET_PRINT_SCRIPT()	
//			CPRINTLN(DEBUG_BLIP, "[player_blips] FORCE_BLIP_PLAYER Force blipping ", GET_PLAYER_NAME(playerId), " called by ", GET_THIS_SCRIPT_NAME())		
//			SET_BIT(g_PlayerBlipsData.bs_PlayersToForceBlip, NATIVE_TO_INT(playerId))
//			SET_PLAYER_BLIP_AS_LONG_RANGE(playerId, bIsLongRange)
//		ELSE
//			CASSERTLN(DEBUG_BLIP, "FORCE_BLIP_PLAYER net player NOT ok")
//		ENDIF	
//	ELSE
//		#IF IS_DEBUG_BUILD
//			IF IS_NET_PLAYER_OK(playerId)
//				CPRINTLN(DEBUG_BLIP, "FORCE_BLIP_PLAYER Clearing force blip for ", GET_PLAYER_NAME(playerId))
//			ENDIF
//		#ENDIF
//		
//		#IF IS_DEBUG_BUILD
//			IF NOT DOES_THIS_THREAD_HAVE_CONTROL_OF_FORCE_BLIP_PLAYER(playerId)
//				CPRINTLN(DEBUG_BLIP, "[player_blips] FORCE_BLIP_PLAYER - already in use by ", g_PlayerBlipsData.ForceBlipScript[NATIVE_TO_INT(PlayerID)], ", perhaps it never cleaned up.")
//				CASSERTLN(DEBUG_BLIP, "FORCE_BLIP_PLAYER - Another script thread has already forced blips, only one script at a time can use force blips.")				
//			ENDIF	
//		#ENDIF
//		
//		SET_PLAYER_BLIP_AS_LONG_RANGE(playerId, bIsLongRange)
//		CLEAR_BIT(g_PlayerBlipsData.bs_PlayersToForceBlip, NATIVE_TO_INT(playerId))
//		
//
//		CPRINTLN(DEBUG_BLIP, "[player_blips] FORCE_BLIP_PLAYER - cleaned up - ", g_PlayerBlipsData.ForceBlipScript[NATIVE_TO_INT(PlayerID)])
//		g_PlayerBlipsData.ForceBlipThreadID[NATIVE_TO_INT(PlayerID)] = INT_TO_NATIVE(THREADID, -1)
//
//	ENDIF
	
ENDPROC



FUNC BOOL IS_PLAYER_IN_DISPLACED_HEIST_APPARTMENT(PLAYER_INDEX PlayerID)
	RETURN IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.iFlags, PBBD_IS_IN_DISPLACED_HEIST_APARTMENT)
ENDFUNC

FUNC BOOL ARE_BOTH_PLAYERS_IN_DISPLACED_HEIST_APPARTMENT(PLAYER_INDEX PlayerID, PLAYER_INDEX PlayerID2)
	IF IS_PLAYER_IN_DISPLACED_HEIST_APPARTMENT(PlayerID)
	AND IS_PLAYER_IN_DISPLACED_HEIST_APPARTMENT(PlayerID2)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_IN_YACHT_PROPERTY(PLAYER_INDEX PlayerID)		
	IF (GlobalplayerBD_FM[NATIVE_TO_INT(PlayerID)].propertyDetails.iCurrentlyInsideProperty = PROPERTY_YACHT_APT_1_BASE)
		RETURN(TRUE)
	ENDIF	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_USING_FAKE_PROPERTY_BLIP(PLAYER_INDEX PlayerID)
	RETURN IS_BIT_SET(g_PlayerBlipsData.bs_PlayerUsingPropertyBlip, NATIVE_TO_INT(PlayerID))
ENDFUNC

FUNC BOOL IS_PLAYER_IN_MOD_SHOP_IN_IE_WAREHOUSE(PLAYER_INDEX PlayerID)
	IF NOT IS_USING_FAKE_PROPERTY_BLIP(PlayerID)
	AND IS_MISSION_PERSONAL_CAR_MOD_STARTED(PlayerID) 
	AND IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PlayerID) 		
	AND (GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PlayerID)) = SIMPLE_INTERIOR_TYPE_IE_GARAGE)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC


PROC SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(INT iInteriorUniqueID, VECTOR vCoords, FLOAT fHeading=0.0)
	
	PRINTLN("[player_blips] SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA called with ", iInteriorUniqueID, ", ", vCoords, ", ", fHeading)
	
	#IF IS_DEBUG_BUILD
	IF (VMAG(vCoords) <= 0.0)
		SCRIPT_ASSERT("SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA - called with <<0,0,0>>!!")	
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	IF IS_THREAD_ACTIVE(g_PlayerBlipsData.DisplacedInteriorThreadID)
		IF NOT (g_PlayerBlipsData.DisplacedInteriorThreadID = GET_ID_OF_THIS_THREAD())
			SCRIPT_ASSERT("SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA - another active script thread has already called this!")
			EXIT
		ENDIF
	ELSE
		g_PlayerBlipsData.DisplacedInteriorThreadID = GET_ID_OF_THIS_THREAD()
		#IF IS_DEBUG_BUILD
		PRINTLN("[player_blips] SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA locking thread ", NATIVE_TO_INT(g_PlayerBlipsData.DisplacedInteriorThreadID))
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF	

	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iDisplacedInteriorID = iInteriorUniqueID
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.vDisplacedInteriorPos = vCoords
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.fDisplacedInteriorHeading = fHeading
//	g_PlayerBlipsData.bDisplaceInteriorCoronaActive = FALSE
	
ENDPROC

PROC CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
	
	PRINTLN("[player_blips] CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA called")
	
//	IF IS_PLAYER_IN_CORONA()
//		IF NOT (g_PlayerBlipsData.bDisplaceInteriorCoronaActive)
//			g_PlayerBlipsData.bDisplaceInteriorCoronaActive = TRUE
//			PRINTLN("[player_blips] CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA - setting bDisplaceInteriorCoronaActive = TRUE")
//		ENDIF
//	ELSE
//		IF (g_PlayerBlipsData.bDisplaceInteriorCoronaActive)
//			g_PlayerBlipsData.bDisplaceInteriorCoronaActive = FALSE
//			PRINTLN("[player_blips] CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA - setting bDisplaceInteriorCoronaActive = FALSE")
//		ENDIF	
//	ENDIF
	
	IF IS_THREAD_ACTIVE(g_PlayerBlipsData.DisplacedInteriorThreadID)
		IF NOT (g_PlayerBlipsData.DisplacedInteriorThreadID = GET_ID_OF_THIS_THREAD())
			PRINTLN("[player_blips] CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA - another active script thread using this!")
			DEBUG_PRINTCALLSTACK()
			SCRIPT_ASSERT("CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA - another active script thread using this!")
			EXIT
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("[player_blips] CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA unlocking thread")
			DEBUG_PRINTCALLSTACK()
			#ENDIF
		ENDIF
	ENDIF	
	
//	IF NOT (g_PlayerBlipsData.bDisplaceInteriorCoronaActive)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iDisplacedInteriorID = -1
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.vDisplacedInteriorPos = <<0.0, 0.0, 0.0>>
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.fDisplacedInteriorHeading = 0.0
//	ENDIF
	
	g_PlayerBlipsData.DisplacedInteriorThreadID = INT_TO_NATIVE(THREADID, -1)
ENDPROC

PROC MAINTAIN_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()

	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.DisplacedInteriorThreadID)
	AND NOT (g_PlayerBlipsData.DisplacedInteriorThreadID = INT_TO_NATIVE(THREADID, -1))	
		PRINTLN("[player_blips] MAINTAIN_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA - thread no longer active.")
		CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
	ENDIF
	
//	IF (g_PlayerBlipsData.bDisplaceInteriorCoronaActive)
//		IF NOT IS_PLAYER_IN_CORONA()	
//			PRINTLN("[player_blips] MAINTAIN_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA - no longer in corona.")
//			CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
//		ENDIF
//	ENDIF	
	
ENDPROC


FUNC BOOL ARE_PLAYER_IN_SAME_CUSTOM_DISPLACED_INTERIOR_INSTANCE(PLAYER_INDEX Player1ID, PLAYER_INDEX Player2ID)
	IF IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(Player1ID)
	AND IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(Player2ID)
		IF (GlobalplayerBD[NATIVE_TO_INT(Player1ID)].playerBlipData.iDisplacedInteriorID = GlobalplayerBD[NATIVE_TO_INT(Player2ID)].playerBlipData.iDisplacedInteriorID)
		AND (NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(Player1Id, Player2Id))
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_PLAYER_IN_SAME_CUSTOM_DISPLACED_INTERIOR_COORDS(PLAYER_INDEX Player1ID, PLAYER_INDEX Player2ID)
	IF IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(Player1ID)
	AND IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(Player2ID)
		IF (VDIST(GlobalplayerBD[NATIVE_TO_INT(Player1ID)].playerBlipData.vDisplacedInteriorPos, GlobalplayerBD[NATIVE_TO_INT(Player2ID)].playerBlipData.vDisplacedInteriorPos) < 0.1)
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_PLAYERS_IN_SAME_CUSTOM_DISPLACED_INTERIOR_BUT_DIFFERENT_INSTANCES(PLAYER_INDEX Player1ID, PLAYER_INDEX Player2ID)
	IF ARE_PLAYER_IN_SAME_CUSTOM_DISPLACED_INTERIOR_COORDS(Player1ID, Player2ID)
		IF NOT ARE_PLAYER_IN_SAME_CUSTOM_DISPLACED_INTERIOR_INSTANCE(Player1ID, Player2ID)
			RETURN(TRUE)
		ENDIF
	ENDIF	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_INSIDE_ARENA(PLAYER_INDEX PlayerID)
	RETURN IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.iFlags, PBBD_IS_IN_ARENA)
ENDFUNC

FUNC BOOL IS_PLAYER_ON_ARENA_FLOOR(PLAYER_INDEX PlayerID)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.iFlags, PBBD_IS_IN_ARENA)
	AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.iFlags, PBBD_IS_ARENA_SPECTATOR)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_PLAYERS_BOTH_ON_ARENA_FLOOR(PLAYER_INDEX Player1ID, PLAYER_INDEX Player2ID)
	IF IS_PLAYER_ON_ARENA_FLOOR(Player1ID)
	AND IS_PLAYER_ON_ARENA_FLOOR(Player2ID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_IN_ARENA_SPECTATOR_BOOTH()
	IF IS_PLAYER_USING_ARENA()
		IF NOT IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_PLAYERS_IN_SAME_CASINO_PROPERTIES(PLAYER_INDEX Player1ID, PLAYER_INDEX Player2ID)
	IF IS_PLAYER_IN_CASINO(Player1ID)
	AND IS_PLAYER_IN_CASINO(Player2ID)
		RETURN TRUE
	ELSE
		IF ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(Player1ID, Player2ID, TRUE)
			IF GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(Player1ID) = GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(Player2ID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_DISPLACED_PROPERTY(PLAYER_INDEX PlayerID  , BOOL bIncludeWalkingIntoSimpleInteriors = FALSE )
	IF IS_PLAYER_IN_PROPERTY(PlayerID, TRUE)			
		INT iCurrentProperty = GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iCurrentlyInsideProperty
		// player is on yacht
		IF IS_PLAYER_IN_YACHT_PROPERTY(PlayerID)
		
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_IN_DISPLACED_PROPERTY - FALSE 1")	
				ENDIF
			#ENDIF
		
			RETURN(FALSE)
		ENDIF
		IF IS_PROPERTY_OFFICE(iCurrentProperty)
			
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_IN_DISPLACED_PROPERTY - FALSE 2")	
				ENDIF
			#ENDIF
		
			RETURN(FALSE)
		ENDIF
		IF IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty)
		
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_IN_DISPLACED_PROPERTY - FALSE 3")	
				ENDIF
			#ENDIF
		
			RETURN(FALSE)
		ENDIF
		IF (iCurrentProperty > 0)
		AND (iCurrentProperty < (MAX_MP_PROPERTIES+1))
			IF NOT (GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentProperty].iIndex) = PROP_SIZE_TYPE_LARGE_APT)
			OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
			
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_IN_DISPLACED_PROPERTY - TRUE 1")	
					ENDIF
				#ENDIF
			
				RETURN(TRUE)
			ENDIF	
		ENDIF
	ENDIF
	
		IF IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PlayerID)
		
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_IN_DISPLACED_PROPERTY - TRUE 2")	
				ENDIF
			#ENDIF
		
			RETURN(TRUE)
		ENDIF
	
	IF IS_PLAYER_IN_DISPLACED_HEIST_APPARTMENT(PlayerID)
	
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_IN_DISPLACED_PROPERTY - TRUE 3")	
			ENDIF
		#ENDIF
	
		RETURN(TRUE)
	ENDIF
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PlayerID)
	AND IS_THIS_SIMPLE_INTERIOR_DISPLACED(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PlayerID), GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(PlayerID))
	AND NOT IS_PLAYER_IN_SUBMARINE_DRIVER_SEAT()
	#IF FEATURE_DLC_1_2022
	AND NOT IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
	#ENDIF
	
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_IN_DISPLACED_PROPERTY - TRUE 4")	
			ENDIF
		#ENDIF
	
		RETURN(TRUE)
	ENDIF	
	
	IF bIncludeWalkingIntoSimpleInteriors
		IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PlayerID)
		AND IS_THIS_SIMPLE_INTERIOR_DISPLACED(GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(PlayerID), GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(PlayerID))
		
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_IN_DISPLACED_PROPERTY - TRUE 5")	
				ENDIF
			#ENDIF
		
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(PlayerID)
		
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_IN_DISPLACED_PROPERTY - TRUE 6")	
			ENDIF
		#ENDIF
	
		RETURN(TRUE)
	ENDIF
	
	IF IS_PLAYER_USING_ARENA() 
	
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_IN_DISPLACED_PROPERTY - TRUE 7")	
			ENDIF
		#ENDIF	
		RETURN(TRUE)
	ENDIF
		
	IF g_bInInteriorThatRequiresInstantFadeout
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] IS_PLAYER_IN_DISPLACED_PROPERTY - TRUE 8")	
			ENDIF
		#ENDIF	
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
ENDFUNC


// --- Functions ----------------------------------------------------------------


FUNC BOOL ARE_PLAYERS_IN_SAME_PROPERTY_BUILDING_BUT_DIFFERENT_INSTANCES(PLAYER_INDEX Player1ID, PLAYER_INDEX Player2ID)
	
	IF (GlobalplayerBD_FM[NATIVE_TO_INT(Player1ID)].propertyDetails.iCurrentlyInsideProperty > 0)
	AND (GlobalplayerBD_FM[NATIVE_TO_INT(Player2ID)].propertyDetails.iCurrentlyInsideProperty > 0 )
		IF (GET_PROPERTY_BUILDING(GlobalplayerBD_FM[NATIVE_TO_INT(Player1ID)].propertyDetails.iCurrentlyInsideProperty) = GET_PROPERTY_BUILDING(GlobalplayerBD_FM[NATIVE_TO_INT(Player2ID)].propertyDetails.iCurrentlyInsideProperty))
			IF NOT ARE_PLAYERS_IN_SAME_PROPERTY(Player1ID, Player2ID, TRUE)
			AND NOT ARE_PLAYERS_IN_SAME_PROPERTY_BUT_DIFFERENT_INTERIOR(Player1ID, Player2ID)
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF

	RETURN(FALSE)
ENDFUNC


FUNC BOOL IS_PLAYER_IN_PROPERTY_THAT_I_AM_NOT(PLAYER_INDEX PlayerID , BOOL bIncludeSimpleInteriors = FALSE)
	
	IF IS_PLAYER_USING_ARENA()
		IF IS_IN_ARENA_SPECTATOR_BOOTH()
			RETURN FALSE // those in the spectator booths should see everything
		ENDIF
	ENDIF	

	IF ARE_PLAYERS_IN_SAME_PROPERTY(PlayerID, MPGlobals.LocalPlayerID, TRUE , bIncludeSimpleInteriors )
		RETURN(FALSE)
	ELSE
		IF IS_PLAYER_IN_PROPERTY(PlayerID, TRUE  , bIncludeSimpleInteriors )	
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	IF ARE_PLAYER_IN_SAME_CUSTOM_DISPLACED_INTERIOR_INSTANCE(PlayerID, MPGlobals.LocalPlayerID)
		RETURN(FALSE)
	ELSE
		IF IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(PlayerID)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

PROC SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR(BOOL bSet) //, BOOL bIgnoreThreadCheck=FALSE)
	#IF IS_DEBUG_BUILD
		IF (bSet)
			CPRINTLN(DEBUG_BLIP, "[player_blips] SET_MY_PLAYER_ARROW_AS_TEAM_COLOUR = TRUE called by ", GET_THIS_SCRIPT_NAME())
		ELSE
			CPRINTLN(DEBUG_BLIP, "[player_blips] SET_MY_PLAYER_ARROW_AS_TEAM_COLOUR = FALSE called by ", GET_THIS_SCRIPT_NAME())
		ENDIF
	#ENDIF
	
	IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()	
		DEBUG_PRINTCALLSTACK()
		NET_PRINT("[player_blips] SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR - called from within launch script. I do not like this. Neilf") NET_NL()
		SCRIPT_ASSERT("SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR - called from within launch script.")
		EXIT	
	ENDIF	
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.MyArrowTeamColourThreadID)
	OR (g_PlayerBlipsData.MyArrowTeamColourThreadID = GET_ID_OF_THIS_THREAD())
	//OR (bIgnoreThreadCheck)
		IF (bSet)
			g_PlayerBlipsData.MyArrowTeamColourThreadID = GET_ID_OF_THIS_THREAD()				
		ELSE
			g_PlayerBlipsData.MyArrowTeamColourThreadID = INT_TO_NATIVE(THREADID, -1)
		ENDIF
	ELSE
		//IF NOT (bIgnoreThreadCheck)
			CASSERTLN(DEBUG_BLIP, "[player_blips] SET_MY_PLAYER_ARROW_AS_TEAM_COLOUR - already in use by another thread.")	
		//ENDIF
	ENDIF	
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_ARROW_SET_AS_CURRENT_HUD_COLOUR()
	IF IS_THREAD_ACTIVE(g_PlayerBlipsData.MyArrowTeamColourThreadID)
		RETURN TRUE
	ENDIF
	#IF IS_DEBUG_BUILD
		IF (g_PlayerBlipsData.bTest_SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR)
			RETURN TRUE
		ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

PROC FLASH_MY_PLAYER_ARROW_RED(BOOL bSet, INT iTotalTimeToFlash=-1, INT iRedDuration = 500, INT iWhiteDuration = 500, BOOL bIgnoreThreadCheck=FALSE)//, BOOL bPrintTicker=FALSE)	
	#IF IS_DEBUG_BUILD
		IF (bSet)
			CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_MY_PLAYER_ARROW_RED = TRUE called by ", GET_THIS_SCRIPT_NAME())
		ELSE
			CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_MY_PLAYER_ARROW_RED = FALSE called by ", GET_THIS_SCRIPT_NAME())
		ENDIF
	#ENDIF
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.FlashMyArrowRedThreadID)
	OR (g_PlayerBlipsData.FlashMyArrowRedThreadID = GET_ID_OF_THIS_THREAD())
	OR (bIgnoreThreadCheck)
		IF (bSet)
			g_PlayerBlipsData.FlashMyArrowRedThreadID = GET_ID_OF_THIS_THREAD()				
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_FLASHING_RED)
			g_PlayerBlipsData.RedFlashArrowStartTime = GET_NETWORK_TIME()
		ELSE
			g_PlayerBlipsData.FlashMyArrowRedThreadID = INT_TO_NATIVE(THREADID, -1)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_FLASHING_RED)
		ENDIF
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iRedFlashDuration = iRedDuration
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iWhiteFlashDuration = iWhiteDuration
		g_PlayerBlipsData.iTimeToFlashArrowRed = iTotalTimeToFlash
	ELSE
		IF NOT (bIgnoreThreadCheck)
			CASSERTLN(DEBUG_BLIP, "[player_blips] FLASH_MY_PLAYER_ARROW_RED - already in use by another thread.")	
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_MY_PLAYER_ARROW_FLASHING()
	IF IS_THREAD_ACTIVE(g_PlayerBlipsData.FlashMyArrowThreadID)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FLASH_MY_PLAYER_ARROW_RED_IN_AND_OUT(BOOL bSet, INT iDuration = -1, BOOL bIgnoreThreadCheck = FALSE)
	#IF IS_DEBUG_BUILD
	IF (bSet)
		CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_MY_PLAYER_ARROW_RED_IN_AND_OUT = TRUE called by ", GET_THIS_SCRIPT_NAME())
	ELSE
		CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_MY_PLAYER_ARROW_RED_IN_AND_OUT = FALSE called by ", GET_THIS_SCRIPT_NAME())
	ENDIF
	#ENDIF
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.FlashMyArrowRedThreadID)
	AND NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.FlashMyArrowThreadID)
	OR ((g_PlayerBlipsData.FlashMyArrowRedThreadID = GET_ID_OF_THIS_THREAD())
	AND (g_PlayerBlipsData.FlashMyArrowThreadID = GET_ID_OF_THIS_THREAD()))
	OR (bIgnoreThreadCheck)
		IF (bSet)
			g_PlayerBlipsData.FlashMyArrowRedThreadID = GET_ID_OF_THIS_THREAD()
			g_PlayerBlipsData.FlashMyArrowThreadID = GET_ID_OF_THIS_THREAD()
			
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_FLASHING_RED)
			
			g_PlayerBlipsData.RedFlashArrowStartTime = GET_NETWORK_TIME()
			g_PlayerBlipsData.FlashMyArrowStartTime = GET_NETWORK_TIME()
		ELSE
			g_PlayerBlipsData.FlashMyArrowRedThreadID = INT_TO_NATIVE(THREADID, -1)
			g_PlayerBlipsData.FlashMyArrowThreadID = INT_TO_NATIVE(THREADID, -1)
			
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_FLASHING_RED)
		ENDIF
		
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iRedFlashDuration = HIGHEST_INT
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iWhiteFlashDuration = 0
		g_PlayerBlipsData.iTimeToFlashArrowRed = iDuration
		
		g_PlayerBlipsData.iDurationToFlashMyArrow = iDuration
		g_PlayerBlipsData.bFlashMyArrow = bSet
	ELSE
		IF NOT (bIgnoreThreadCheck)
			CASSERTLN(DEBUG_BLIP, "[player_blips] FLASH_MY_PLAYER_ARROW_RED_IN_AND_OUT - already in use by another thread.")
		ENDIF
	ENDIF
ENDPROC

PROC SET_MY_PLAYER_BLIP_AS_IDLE(BOOL bSet, BOOL bIgnoreThreadCheck=FALSE)
	
	#IF IS_DEBUG_BUILD
		IF (bSet)
			CPRINTLN(DEBUG_BLIP, "[player_blips] SET_MY_PLAYER_BLIP_AS_IDLE = TRUE called by ", GET_THIS_SCRIPT_NAME())
		ELSE
			CPRINTLN(DEBUG_BLIP, "[player_blips] SET_MY_PLAYER_BLIP_AS_IDLE = FALSE called by ", GET_THIS_SCRIPT_NAME())
		ENDIF
		
		IF NOT (bIgnoreThreadCheck)
		AND IS_THREAD_ACTIVE(g_PlayerBlipsData.IdleMyBlipThreadID)								
			IF NOT (g_PlayerBlipsData.IdleMyBlipThreadID = GET_ID_OF_THIS_THREAD())	
				CASSERTLN(DEBUG_BLIP, "[player_blips] SET_MY_PLAYER_BLIP_AS_IDLE - already in use by another thread.")			
			ENDIF
		ENDIF	

	#ENDIF
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.IdleMyBlipThreadID)
	OR (g_PlayerBlipsData.IdleMyBlipThreadID = GET_ID_OF_THIS_THREAD())
	OR (bIgnoreThreadCheck)
		IF (bSet)
			g_PlayerBlipsData.IdleMyBlipThreadID = GET_ID_OF_THIS_THREAD()					
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_IDLING)
			
			// we want to turn the players arrow red for this (requested 2523415)
			FLASH_MY_PLAYER_ARROW_RED(TRUE, -1, HIGHEST_INT, 0)
			
		ELSE
			g_PlayerBlipsData.IdleMyBlipThreadID = INT_TO_NATIVE(THREADID, -1)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_IDLING)
			
			// stop my player arrow from being red
			FLASH_MY_PLAYER_ARROW_RED(FALSE)
			
		ENDIF
	ELSE
		IF NOT (bIgnoreThreadCheck)
			CASSERTLN(DEBUG_BLIP, "[player_blips] FLASH_MY_PLAYER_ARROW_RED - already in use by another thread.")
		ENDIF
	ENDIF	
	
ENDPROC

FUNC BOOL IS_PLAYER_BLIP_SET_AS_IDLE(PLAYER_INDEX PlayerID)
	RETURN IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.iFlags, PBBD_IS_IDLING)
ENDFUNC

FUNC BOOL IS_MY_PLAYER_BLIP_SET_AS_IDLE()
	RETURN IS_PLAYER_BLIP_SET_AS_IDLE(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_PLAYER_BLIP_IN_IDLE_COOLDOWN(PLAYER_INDEX PlayerID)
	IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.iBlipTimerIdle[NATIVE_TO_INT(playerID)]) < NOTICABLE_TIME)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC


FUNC BOOL IS_PLAYER_IN_ARMOUR_TRUCK_OUTSIDE_BUNKER(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMORY_TRUCK(PlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner 
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			IF NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(OwnerID)
				RETURN TRUE
			ENDIF	
		ENDIF	
	ENDIF	
	IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ARMOUR_TRUCK_INSIDE_BUNKER(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMORY_TRUCK(PlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner 
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(OwnerID)
				RETURN TRUE
			ENDIF	
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_INSIDE_TRUCK_IN_BUNKER_I_AM_IN(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMOUR_TRUCK_INSIDE_BUNKER(PlayerID)
	AND IS_PLAYER_IN_BUNKER(MPGlobals.LocalPlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner 
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			IF (globalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner = OwnerID)	
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_PLAYERS_TRUCK(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMORY_TRUCK(MPGlobals.LocalPlayerID)
		IF (PlayerID = globalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_SAME_TRUCK_AS_PLAYER(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMORY_TRUCK(MPGlobals.LocalPlayerID)
	AND IS_PLAYER_IN_ARMORY_TRUCK(PlayerID)
		IF (globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner = globalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_GIVE_MOC_SPRITE(PLAYER_INDEX PlayerID)
	IF (IS_PLAYER_IN_ARMOUR_TRUCK_OUTSIDE_BUNKER(PlayerID) 
	AND (IS_PLAYER_IN_PROPERTY_THAT_I_AM_NOT(PlayerID, TRUE) OR IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)))
		RETURN TRUE
	ENDIF
	IF IS_PLAYER_INSIDE_TRUCK_IN_BUNKER_I_AM_IN(PlayerID)
		RETURN TRUE
	ENDIF
	IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
		IF IS_LOCAL_PLAYER_IN_SAME_TRUCK_AS_PLAYER(PlayerID)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ARMORY_AIRCRAFT_OUTSIDE_DEFUNCT_BASE(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner 
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			IF NOT IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(OwnerID)
				RETURN TRUE
			ENDIF	
		ENDIF	
	ENDIF	
	IF IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner 
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(OwnerID)
				RETURN TRUE
			ENDIF	
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_INSIDE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE_I_AM_IN(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(PlayerID)
	AND IS_PLAYER_IN_DEFUNCT_BASE(MPGlobals.LocalPlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner 
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			IF (globalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner = OwnerID)	
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_SAME_ARMORY_AIRCRAFT_AS_PLAYER(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(MPGlobals.LocalPlayerID)
	AND IS_PLAYER_IN_ARMORY_AIRCRAFT(PlayerID)
		IF (globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner = globalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_GIVE_ARMORY_AIRCRAFT_SPRITE(PLAYER_INDEX PlayerID)
	IF (IS_PLAYER_IN_ARMORY_AIRCRAFT_OUTSIDE_DEFUNCT_BASE(PlayerID) 
	AND (IS_PLAYER_IN_PROPERTY_THAT_I_AM_NOT(PlayerID, TRUE) OR IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)))
		RETURN TRUE
	ENDIF
	IF IS_PLAYER_INSIDE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE_I_AM_IN(PlayerID)
		RETURN TRUE
	ENDIF
	IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
		IF IS_LOCAL_PLAYER_IN_SAME_ARMORY_AIRCRAFT_AS_PLAYER(PlayerID)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_GIVE_SUBMARINE_SPRITE(PLAYER_INDEX PlayerID)

	IF (IS_PLAYER_IN_SUBMARINE(PlayerID) 
	AND (IS_PLAYER_IN_PROPERTY_THAT_I_AM_NOT(PlayerID, TRUE) OR IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)))
		RETURN TRUE
		
	ELIF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID) AND IS_LOCAL_PLAYER_IN_SAME_SUBMARINE_AS_PLAYER(PlayerID)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL AmILeaderOfAOC()
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(MPGlobals.LocalPlayerID)
		INT iPlayer
		PLAYER_INDEX PlayerID
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
			IF NOT (PlayerID = MPGlobals.LocalPlayerID)
				IF IS_LOCAL_PLAYER_IN_SAME_ARMORY_AIRCRAFT_AS_PLAYER(PlayerID)
					// lowest player id is the leader
					IF (iPlayer < NATIVE_TO_INT(MPGlobals.LocalPlayerID))
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_HACKER_TRUCK_OUTSIDE_BUSINESS_HUB(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_HACKER_TRUCK(PlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner 
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			IF NOT IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(OwnerID)
				RETURN TRUE
			ENDIF	
		ENDIF	
	ENDIF	
//	IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerID)
//		RETURN TRUE
//	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ARMOUR_HACKER_TRUCK_INSIDE_BUSINESS_HUB(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_HACKER_TRUCK(PlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner 
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			IF IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(OwnerID)
				RETURN TRUE
			ENDIF	
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_INSIDE_HACKER_TRUCK_IN_BUSINESS_HUB_I_AM_IN(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ARMOUR_HACKER_TRUCK_INSIDE_BUSINESS_HUB(PlayerID)
	AND IS_PLAYER_IN_BUNKER(MPGlobals.LocalPlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner 
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			IF (globalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner = OwnerID)	
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

#IF FEATURE_DLC_2_2022
FUNC BOOL IS_PLAYER_IN_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ACID_LAB(PlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner 
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			IF IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(OwnerID)
				RETURN TRUE
			ENDIF	
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_INSIDE_ACID_LAB_IN_JUGGALO_HIDEOUT_I_AM_IN(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(PlayerID)
	AND IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(MPGlobals.LocalPlayerID)
		PLAYER_INDEX OwnerID = globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner 
		IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			IF (globalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner = OwnerID)	
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL IS_LOCAL_PLAYER_IN_PLAYERS_HACKER_TRUCK(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_HACKER_TRUCK(MPGlobals.LocalPlayerID)
		IF (PlayerID = globalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_SAME_HACKER_TRUCK_AS_PLAYER(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_HACKER_TRUCK(MPGlobals.LocalPlayerID)
	AND IS_PLAYER_IN_HACKER_TRUCK(PlayerID)
		IF (globalPlayerBD[NATIVE_TO_INT(PlayerID)].SimpleInteriorBD.propertyOwner = globalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL SHOULD_GIVE_HACKER_TRUCK_SPRITE(PLAYER_INDEX PlayerID)
	IF (IS_PLAYER_IN_HACKER_TRUCK_OUTSIDE_BUSINESS_HUB(PlayerID) 
	AND (IS_PLAYER_IN_PROPERTY_THAT_I_AM_NOT(PlayerID, TRUE) OR IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)))
		RETURN TRUE
	ENDIF
	IF IS_PLAYER_INSIDE_HACKER_TRUCK_IN_BUSINESS_HUB_I_AM_IN(PlayerID)
		RETURN TRUE
	ENDIF
	IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
		IF IS_LOCAL_PLAYER_IN_SAME_HACKER_TRUCK_AS_PLAYER(PlayerID)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL AmILeaderOfHackerTruck()
	IF IS_PLAYER_IN_HACKER_TRUCK(MPGlobals.LocalPlayerID)
		INT iPlayer
		PLAYER_INDEX PlayerID
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
			IF NOT (PlayerID = MPGlobals.LocalPlayerID)
				IF IS_LOCAL_PLAYER_IN_SAME_HACKER_TRUCK_AS_PLAYER(PlayerID)
					// lowest player id is the leader
					IF (iPlayer < NATIVE_TO_INT(MPGlobals.LocalPlayerID))
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL AmILeaderOfMOC()
	IF IS_PLAYER_IN_ARMORY_TRUCK(MPGlobals.LocalPlayerID)
		INT iPlayer
		PLAYER_INDEX PlayerID
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
			IF NOT (PlayerID = MPGlobals.LocalPlayerID)
				IF IS_LOCAL_PLAYER_IN_SAME_TRUCK_AS_PLAYER(PlayerID)
					// lowest player id is the leader
					IF (iPlayer < NATIVE_TO_INT(MPGlobals.LocalPlayerID))
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL AmILeaderOfSubmarine()
	IF IS_PLAYER_IN_SUBMARINE(MPGlobals.LocalPlayerID)
		INT iPlayer
		PLAYER_INDEX PlayerID
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
			IF NOT (PlayerID = MPGlobals.LocalPlayerID)
				IF IS_LOCAL_PLAYER_IN_SAME_SUBMARINE_AS_PLAYER(PlayerID)
					// lowest player id is the leader
					IF (iPlayer < NATIVE_TO_INT(MPGlobals.LocalPlayerID))
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IsUsingSpecialWeaponCamera()
	IF IS_LOCAL_PLAYER_USING_DRONE()
	OR IS_LOCAL_PLAYER_USING_VEHICLE_WEAPON()
	OR IS_PLAYER_USING_SPECIAL_SPECTATOR_TRAP_CAM(PLAYER_ID())	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Reasons the player should not be blipped (regardless of their wanted state or whatever)
/// PARAMS:
///    playerID - player to check
/// RETURNS:
///    if this guy should definitely not have a blip
FUNC BOOL SHOULD_BLOCK_PLAYER_BLIP(PLAYER_INDEX playerID)


	#IF IS_DEBUG_BUILD
		IF (g_PlayerBlipsData.bBlockAllPlayerBlips)
			RETURN(TRUE)
		ENDIF
	#ENDIF
	
	// spectators should always be idle, no matter what.
	IF IS_PLAYER_SPECTATING(PlayerID)
	OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PlayerID)
	OR IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(PlayerID)
	OR IS_THIS_PLAYER_ON_JIP_WARNING(PlayerID)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - IS_PLAYER_SPECTATING - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN(TRUE)
	ENDIF	
	
	// if player is set to be idling then dont block the blip
	IF IS_PLAYER_BLIP_SET_AS_IDLE(PlayerID)
	OR IS_PLAYER_BLIP_IN_IDLE_COOLDOWN(PlayerID)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - FALSE, IDLE - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN(FALSE)
	ENDIF
	

	// hide player blips that aren't on local player's team
	IF( g_FMMC_STRUCT.bHideBlips )
		INT iMyTeam 	= GET_PLAYER_TEAM( MPGlobals.LocalPlayerID )
		INT iPlayerTeam = GET_PLAYER_TEAM( playerID )
		IF( iMyTeam <> -1 AND iPlayerTeam <> -1 )
			IF( iMyTeam <> iPlayerTeam AND NOT DOES_TEAM_LIKE_TEAM( iMyTeam, iPlayerTeam ) )
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - g_FMMC_STRUCT.bHideBlips - player id =", NATIVE_TO_INT(playerID))
					ENDIF
				#ENDIF	
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF

	IF (g_PlayerBlipsData.bHideAllPlayerBlips)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - g_PlayerBlipsData.bHideAllPlayerBlips - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF	
		RETURN(TRUE)
	ENDIF
	
	IF NOT IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerId)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN (TRUE)
	ENDIF	

	IF NOT IS_NET_PLAYER_OK(PlayerID) // any reason why we would blip dead or non-playing players
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - IS_NET_PLAYER_OK - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN(TRUE)
	ENDIF
	
	IF IS_PLAYER_BLIP_HIDDEN_FROM_ME(playerID)
	
		// if player is carrying contraband then dont block the blip
		IF SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(playerID, TRUE)
		OR IS_PLAYER_VEHICLE_BLIP_BEING_BLIPPED_BY_SHIPMENT_SYSTEM(playerID)
		OR IS_STEALTH_DISABLED_FOR_PLAYER_BLIP(playerID)
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - IS_PLAYER_BLIP_HIDDEN_FROM_ME, carrying shipment - player id =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF
		ELSE	
	
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - IS_PLAYER_BLIP_HIDDEN_FROM_ME - player id =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF
			RETURN(TRUE)			
		ENDIF
	ENDIF

	// is player on a leaderboard
	IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iPlayerInteractionFlags, PIBD_IsOnLeaderBoard))
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - bIsOnLeaderBoard - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN(TRUE)
	ENDIF

	// is ped invisible? (for during heist cutscenes, so player names dont appear)
	IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(GET_PLAYER_PED(playerID))
	AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.iFlags, PBBD_IS_SHOPPING)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - not IS_ENTITY_VISIBLE - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF

	
	// if players are in different sessions
	IF NOT NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(MPGlobals.LocalPlayerID, playerID)
	AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.iFlags, PBBD_IN_CAR_MOD_SHOP)
	AND NOT IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(playerID)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - not NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN(TRUE)
	ELSE
		//-- Dave W - fix 1643149 - remote players in the a shop shouldn't be blipped if the local player is on the tutorial mission 
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.iFlags, PBBD_IN_CAR_MOD_SHOP)
			IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - player in mod shop but local player on tutorial- player id =", NATIVE_TO_INT(playerID))
					ENDIF
				#ENDIF
				
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	
	
	// if player is in a cutscene
	IF IS_PLAYER_IN_CUTSCENE(playerID)
	AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.iFlags, PBBD_IS_SHOPPING)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - IS_PLAYER_IN_CUTSCENE and not shopping - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN(TRUE)
	ENDIF
	
	// do we only want to show players on this script?
	IF (g_PlayerBlipsData.bShowOnlyPlayersOnThisScript)
		IF IS_THREAD_ACTIVE(g_PlayerBlipsData.ShowOnlyPlayersOnThisScriptThreadID)
			IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT_ON_SCRIPT(playerID, g_PlayerBlipsData.ShowOnlyPlayersOnThisScriptName, g_PlayerBlipsData.iShowOnlyPlayersOnThisScriptInstance)
				#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - NOT NETWORK_IS_PLAYER_A_PARTICIPANT_ON_SCRIPT - player id =", NATIVE_TO_INT(playerID))
				ENDIF
				#ENDIF
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	

	// hide player's blip if player has unlocked that feature.
	IF ((GlobalplayerBD[NATIVE_TO_INT(playerID)].bOffTheRadar) OR (GlobalplayerBD[NATIVE_TO_INT(playerID)].bOffTheRadarPlayerInSameCar))
	AND NOT (GlobalplayerBD[NATIVE_TO_INT(playerID)].bOffTheRadarGangMode)
	
		// dont hide players blip with offtheradar if they have a shipment blip. 3175525
		IF SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(playerID, TRUE)
		OR IS_PLAYER_VEHICLE_BLIP_BEING_BLIPPED_BY_SHIPMENT_SYSTEM(playerID)
	
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - bOffTheRadar = TRUE, but has shipment blip. - player id =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF	
	
		ELSE
	
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - bOffTheRadar = TRUE - player id =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF
			RETURN(TRUE)
			
		ENDIF

	// hide player's blip if player has unlocked that feature.
	ELIF ((GlobalplayerBD[NATIVE_TO_INT(playerID)].bOffTheRadar) OR (GlobalplayerBD[NATIVE_TO_INT(playerID)].bOffTheRadarPlayerInSameCar))
	AND (GlobalplayerBD[NATIVE_TO_INT(playerID)].bOffTheRadarGangMode)
		
		PRINTLN("[GB][AMEC] SHOULD_BLOCK_PLAYER_BLIP - bOffTheRadarGangMode = TRUE.")
		PLAYER_INDEX piGangBoss = INVALID_PLAYER_INDEX()
		
		// local player is a boss.
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			piGangBoss = PLAYER_ID()
		ELSE
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(TRUE)
				// local player has another boss.
				piGangBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
			ENDIF
		ENDIF
		
		// url:bugstar:2606169 - Assault: When his boss activated Ghost Organisation, the enemy goon carrying the package's 
		// briefcase blip disappeared from the radar.
		// url:bugstar:2614001 - Can we please allow Ghost Organization in the following modes but stop it removing/hiding the unique blips
		
		IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, piGangBoss, TRUE)
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					PRINTLN("[GB][AMEC] SHOULD_BLOCK_PLAYER_BLIP - bOffTheRadar = TRUE, player: [",NATIVE_TO_INT(playerID),", ",GET_PLAYER_NAME(playerID),
					"] is a member of my gang, don't hide. Boss: [",NATIVE_TO_INT(piGangBoss),", ",GET_PLAYER_NAME(piGangBoss),"]")
				ENDIF
			#ENDIF
		ELIF (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_GB_ASSAULT)
			IF g_PlayerBlipsData.iPlayerCustomSprite[NATIVE_TO_INT(playerID)] = ENUM_TO_INT(RADAR_TRACE_TANK)
			OR g_PlayerBlipsData.iPlayerCustomSprite[NATIVE_TO_INT(playerID)] = ENUM_TO_INT(RADAR_TRACE_ASSAULT_PACKAGE)
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						PRINTLN("[GB][AMEC] SHOULD_BLOCK_PLAYER_BLIP - bOffTheRadar = TRUE, player: [",NATIVE_TO_INT(playerID),", ",GET_PLAYER_NAME(playerID),
						"] is invalid for hiding (ASSAULT MODE), don't hide. Boss: [",NATIVE_TO_INT(piGangBoss),", ",GET_PLAYER_NAME(piGangBoss),"]")
					ENDIF
				#ENDIF
			ELSE
				// target player is not in the same gang as the local player.
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						PRINTLN("[player_blips] SHOULD_BLOCK_PLAYER_BLIP - Player [",NATIVE_TO_INT(playerID),"] not critical to (ASSAULT MODE). Hiding.")
					ENDIF
				#ENDIF
				RETURN(TRUE)
			ENDIF
			
		ELIF (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_GB_YACHT_ROBBERY)
			IF g_PlayerBlipsData.iPlayerCustomSprite[NATIVE_TO_INT(playerID)] = ENUM_TO_INT(RADAR_TRACE_YACHT)
			OR g_PlayerBlipsData.iPlayerCustomSprite[NATIVE_TO_INT(playerID)] = ENUM_TO_INT(RADAR_TRACE_YACHT_LOCATION)
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						PRINTLN("[GB][AMEC] SHOULD_BLOCK_PLAYER_BLIP - bOffTheRadar = TRUE, player: [",NATIVE_TO_INT(playerID),", ",GET_PLAYER_NAME(playerID),
						"] is invalid for hiding (YACHT ROBBERY), don't hide. Boss: [",NATIVE_TO_INT(piGangBoss),", ",GET_PLAYER_NAME(piGangBoss),"]")
					ENDIF
				#ENDIF
			ELSE
				// target player is not in the same gang as the local player.
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						PRINTLN("[player_blips] SHOULD_BLOCK_PLAYER_BLIP - Player [",NATIVE_TO_INT(playerID),"] not critical to (YACHT ROBBERY). Hiding.")
					ENDIF
				#ENDIF
				RETURN(TRUE)
			ENDIF
			
		ELIF (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_GB_SIGHTSEER)
			IF g_PlayerBlipsData.iPlayerCustomSprite[NATIVE_TO_INT(playerID)] = ENUM_TO_INT(RADAR_TRACE_SIGHTSEER)
			OR g_PlayerBlipsData.iPlayerCustomSprite[NATIVE_TO_INT(playerID)] = ENUM_TO_INT(RADAR_TRACE_TEMP_4)
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						PRINTLN("[GB][AMEC] SHOULD_BLOCK_PLAYER_BLIP - bOffTheRadar = TRUE, player: [",NATIVE_TO_INT(playerID),", ",GET_PLAYER_NAME(playerID),
						"] is invalid for hiding (SIGHTSEER), don't hide. Boss: [",NATIVE_TO_INT(piGangBoss),", ",GET_PLAYER_NAME(piGangBoss),"]")
					ENDIF
				#ENDIF
			ELSE
				// target player is not in the same gang as the local player.
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						PRINTLN("[player_blips] SHOULD_BLOCK_PLAYER_BLIP - Player [",NATIVE_TO_INT(playerID),"] not critical to (SIGHTSEER). Hiding.")
					ENDIF
				#ENDIF
				RETURN(TRUE)
			ENDIF
		
		ELIF (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_GB_HUNT_THE_BOSS)
			IF g_PlayerBlipsData.iPlayerCustomSprite[NATIVE_TO_INT(playerID)] = ENUM_TO_INT(RADAR_TRACE_TEMP_4)
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						PRINTLN("[GB][AMEC] SHOULD_BLOCK_PLAYER_BLIP - bOffTheRadar = TRUE, player: [",NATIVE_TO_INT(playerID),", ",GET_PLAYER_NAME(playerID),
						"] is invalid for hiding (HUNT THE BOSS), don't hide. Boss: [",NATIVE_TO_INT(piGangBoss),", ",GET_PLAYER_NAME(piGangBoss),"]")
					ENDIF
				#ENDIF
			ELSE
				// target player is not in the same gang as the local player.
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						PRINTLN("[player_blips] SHOULD_BLOCK_PLAYER_BLIP - Player [",NATIVE_TO_INT(playerID),"] not critical to (HUNT THE BOSS). Hiding.")
					ENDIF
				#ENDIF
				RETURN(TRUE)
			ENDIF
			
		ELIF (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_GB_BELLY_BEAST)
			IF g_PlayerBlipsData.iPlayerCustomSprite[NATIVE_TO_INT(playerID)] = ENUM_TO_INT(RADAR_TRACE_BELLY_OF_THE_BEAST)
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						PRINTLN("[GB][AMEC] SHOULD_BLOCK_PLAYER_BLIP - bOffTheRadar = TRUE, player: [",NATIVE_TO_INT(playerID),", ",GET_PLAYER_NAME(playerID),
						"] is invalid for hiding (BELLY BEAST), don't hide. Boss: [",NATIVE_TO_INT(piGangBoss),", ",GET_PLAYER_NAME(piGangBoss),"]")
					ENDIF
				#ENDIF
			ELSE
				// target player is not in the same gang as the local player.
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						PRINTLN("[player_blips] SHOULD_BLOCK_PLAYER_BLIP - Player [",NATIVE_TO_INT(playerID),"] not critical to (BELLY BEAST). Hiding.")
					ENDIF
				#ENDIF
				RETURN(TRUE)
			ENDIF
		
		ELSE
			// target player is not in the same gang as the local player.
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					PRINTLN("[player_blips] SHOULD_BLOCK_PLAYER_BLIP - bOffTheRadarGangMode and player [",NATIVE_TO_INT(playerID),"] not in local player gang. Hiding.")
				ENDIF
			#ENDIF
			RETURN(TRUE)
		ENDIF
	
	ENDIF
	
	// player is in the process of warping
	IF NOT (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].iWarpToLocationState = 0)
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - process of warping = TRUE - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN(TRUE)
	ENDIF
	
	// player is in the process of respawning
	//IF NOT (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].iRespawnState = RESPAWN_STATE_PLAYING) 
	IF IS_PLAYER_RESPAWNING(PlayerID)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - process of respawning = TRUE - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN(TRUE)
	ENDIF
	
	// if transitioning into a property
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PlayerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - transitioning into a property - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN(TRUE)	
	ENDIF
	
	// warping in/out of simple interior.
	IF IS_PLAYER_DOING_SIMPLE_INTERIOR_AUTOWARP(PlayerID)
	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PlayerID)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - warping in/out into a simple interior - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		RETURN(TRUE)		
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF IS_PLAYER_USING_RCBANDITO(PlayerID)
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - activity session player is using rcbandito - player id =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF
			RETURN(TRUE)
		ENDIF
	ELSE
		IF IS_PLAYER_USING_RCBANDITO(PlayerID)
		#IF FEATURE_CASINO_HEIST
		OR IS_PLAYER_USING_RC_TANK(PlayerID)
		#ENDIF
			IF !GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
				#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - player is using rcbandito not in gang - player id =", NATIVE_TO_INT(playerID))
				ENDIF
				#ENDIF
				RETURN(TRUE)
			ELSE
				IF !GB_ARE_PLAYERS_IN_SAME_GANG(PlayerID, PLAYER_ID())
					#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - player is using rcbandito not in same gang - player id =", NATIVE_TO_INT(playerID))
					ENDIF
					#ENDIF
					RETURN(TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)		

		IF NOT (g_bActiveInGunTurret)
		AND NOT IsUsingSpecialWeaponCamera()
			IF IS_PLAYER_IN_DISPLACED_PROPERTY(MPGlobals.LocalPlayerID)
			AND NOT (ARE_PLAYERS_IN_SAME_PROPERTY(PlayerID, MPGlobals.LocalPlayerID, TRUE) OR ARE_BOTH_PLAYERS_IN_DISPLACED_HEIST_APPARTMENT(PlayerID, MPGlobals.LocalPlayerID))
			AND NOT (ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PlayerID, MPGlobals.LocalPlayerID, TRUE))	
			AND NOT IS_PLAYER_INSIDE_TRUCK_IN_BUNKER_I_AM_IN(PlayerID)
			AND NOT IS_PLAYER_INSIDE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE_I_AM_IN(PlayerID)
			AND NOT ARE_PLAYER_IN_SAME_CUSTOM_DISPLACED_INTERIOR_INSTANCE(PlayerID, MPGlobals.LocalPlayerID)
			AND NOT IS_PLAYER_INSIDE_HACKER_TRUCK_IN_BUSINESS_HUB_I_AM_IN(PlayerID)
			AND NOT IS_IN_ARENA_SPECTATOR_BOOTH()
			AND NOT ARE_PLAYERS_BOTH_ON_ARENA_FLOOR(PlayerID, MPGlobals.LocalPlayerID)
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - local player is in displaced property and other player isn't - player id =", NATIVE_TO_INT(playerID))
					ENDIF
				#ENDIF
				RETURN(TRUE)
				
			ENDIF
		
		// is the local player in the gun turret?
		ELSE
		
			IF ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PlayerID, MPGlobals.LocalPlayerID, TRUE)	
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - local player is using gunturret and other player is in truck - player id =", NATIVE_TO_INT(playerID))
					ENDIF
				#ENDIF
				RETURN(TRUE)				
			ENDIF
			
		ENDIF
		
		
		IF ARE_PLAYERS_IN_SAME_PROPERTY_BUILDING_BUT_DIFFERENT_INSTANCES(MPGlobals.LocalPlayerID, playerID)
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - ARE_PLAYERS_IN_SAME_PROPERTY_BUILDING_BUT_DIFFERENT_INSTANCES, player id =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF
			RETURN(TRUE)
		ENDIF	
		
		IF ARE_PLAYERS_IN_SAME_CUSTOM_DISPLACED_INTERIOR_BUT_DIFFERENT_INSTANCES(MPGlobals.LocalPlayerID, playerID)
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - ARE_PLAYERS_IN_SAME_CUSTOM_DISPLACED_INTERIOR_BUT_DIFFERENT_INSTANCES, player id =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF
			RETURN(TRUE)
		ENDIF
		
		IF ARE_PLAYERS_IN_SAME_PROPERTY_BUT_DIFFERENT_INTERIOR(PlayerID, MPGlobals.LocalPlayerID)
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - ARE_PLAYERS_IN_SAME_PROPERTY_BUT_DIFFERENT_INTERIOR, player id =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF
			RETURN(TRUE)		
		ENDIF
		
		IF IS_PLAYER_IN_BUSINESS_HUB(PlayerID) AND IS_PLAYER_IN_BUSINESS_HUB(MPGlobals.LocalPlayerID)
		AND ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PlayerID, MPGlobals.LocalPlayerID, TRUE)
		AND GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(PlayerID) != GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(MPGlobals.LocalPlayerID)
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - PLAYERS ARE ON DIFFERENT FLOORS OF NIGHTCLUB, player id =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF
			RETURN(TRUE)
		ENDIF
		
		IF IS_PLAYER_IN_CAR_MEET_PROPERTY(PlayerID) AND IS_PLAYER_IN_CAR_MEET_PROPERTY(MPGlobals.LocalPlayerID)
		AND ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PlayerID, MPGlobals.LocalPlayerID, TRUE)
			IF GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(PlayerID) != GET_INTERIOR_FLOOR_INDEX_FOR_PLAYER(MPGlobals.LocalPlayerID)					// Block if on different floors
			OR (IS_PLAYER_IN_CAR_MEET_SPECTATOR_AREA(PlayerID) AND NOT IS_PLAYER_IN_CAR_MEET_SPECTATOR_AREA(MPGlobals.LocalPlayerID))	// Or else block any players in spectator box while we are driving in test area and vice versa
			OR (IS_PLAYER_TEST_DRIVING_A_VEHICLE(PlayerID) AND NOT IS_PLAYER_TEST_DRIVING_A_VEHICLE(MPGlobals.LocalPlayerID))										
			OR (IS_PLAYER_TEST_DRIVING_A_VEHICLE_AS_PASSENGER(PlayerID) AND NOT IS_PLAYER_TEST_DRIVING_A_VEHICLE_AS_PASSENGER(MPGlobals.LocalPlayerID))		
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - PLAYERS ARE ON DIFFERENT FLOORS OF CAR MEET, player id =", NATIVE_TO_INT(playerID))
					ENDIF
				#ENDIF
				RETURN(TRUE)
			ENDIF
		ENDIF		
		
		IF IS_PLAYER_IN_AUTO_SHOP(playerID)
		AND NOT IS_PLAYER_IN_AUTO_SHOP_THEY_OWN(playerID)
		AND IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(playerID)
		AND ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PlayerID, MPGlobals.LocalPlayerID, TRUE)
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - Player is modding vehicle in remote Auto Shop, player id =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF
			RETURN(TRUE)
		ENDIF
		
		IF IS_PLAYER_IN_ANY_PART_OF_CASINO(MPGlobals.LocalPlayerID) 
			IF IS_PLAYER_IN_ANY_PART_OF_CASINO(PlayerID) 
				IF NOT ARE_PLAYERS_IN_SAME_CASINO_PROPERTIES(MPGlobals.LocalPlayerID,PlayerID)
					#IF IS_DEBUG_BUILD
						IF (g_PlayerBlipsData.bDebugBlipOutput)
							CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - players are in different parts of the casino =", NATIVE_TO_INT(playerID))
						ENDIF
					#ENDIF
					RETURN(TRUE)			
				ENDIF		
			ENDIF
		ENDIF
		
		IF IS_PLAYER_IN_CASINO_APARTMENT(MPGlobals.LocalPlayerID)
			IF IS_COORD_ON_CASINO_ROOF(GET_PLAYER_COORDS(PlayerID))
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - player is on casino roof.", NATIVE_TO_INT(playerID))
					ENDIF
				#ENDIF
				RETURN(TRUE)	
			ENDIF
		ENDIF

	ENDIF
	
	IF GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_IAABASE_FINALE
	AND GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].bPlayerUsingTurretCam
		#IF IS_DEBUG_BUILD
		IF (g_PlayerBlipsData.bDebugBlipOutput)
			CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - I am on turret, Player is not using turret, player id =", NATIVE_TO_INT(playerID))
		ENDIF
		#ENDIF
		RETURN(TRUE)
	ENDIF
	
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
	AND g_OwnerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND DOES_ENTITY_EXIST(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(g_OwnerOfArmoryAircraftPropertyIAmIn)])
		IF IS_THIS_PLAYER_IN_THIS_VEHICLE(playerID, MPGlobals.RemoteAvengerV[NATIVE_TO_INT(g_OwnerOfArmoryAircraftPropertyIAmIn)])
			#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLOCK_PLAYER_BLIP - I am on turret, Player is in Avenger I am in, player id =", NATIVE_TO_INT(playerID))
			ENDIF
			#ENDIF
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	IF IS_PLAYER_PERFORMING_MANUAL_RESPAWN(playerID)
		#IF IS_DEBUG_BUILD
		PRINTLN("[player_blips] SHOULD_BLOCK_PLAYER_BLIP - Player ", NATIVE_TO_INT(playerID), " is manually respawning, don't show blip.")
		#ENDIF
		RETURN TRUE	
	ENDIF
	
	IF playerID != INVALID_PLAYER_INDEX()
	AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iOutOfSightBS, GLOBAL_OUT_OF_SIGHT_BS_BLIP_HIDDEN)
		IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), playerID)
			#IF IS_DEBUG_BUILD
			PRINTLN("[player_blips] SHOULD_BLOCK_PLAYER_BLIP - Player ", NATIVE_TO_INT(playerID), " is using the Out of Sight ability")
			#ENDIF
			RETURN TRUE	
		ENDIF
	ENDIF
	
	//Hide player blips if they are in a submerged submarine we aren't
	//Unless they're in our gang
	CLEAR_BIT(g_PlayerBlipsData.bs_IsMadeVisibleBySonar, NATIVE_TO_INT(playerID))
	IF NOT GB_ARE_PLAYERS_IN_SAME_GANG(MPGlobals.LocalPlayerID, playerID)
	
		IF (GlobalPlayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.eCurrentSimpleInterior != SIMPLE_INTERIOR_INVALID)
	
			SIMPLE_INTERIOR_TYPE PlayerSimpleInterior = GET_SIMPLE_INTERIOR_TYPE(GlobalPlayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.eCurrentSimpleInterior)
			PLAYER_INDEX PlayerSimpleInteriorOwner = GlobalPlayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.propertyOwner
			
			IF PlayerSimpleInterior = SIMPLE_INTERIOR_TYPE_SUBMARINE 
			AND PlayerSimpleInteriorOwner != INVALID_PLAYER_INDEX()
			
				IF GlobalplayerBD_FM_4[NATIVE_TO_INT(PlayerSimpleInteriorOwner)].bIsSubmarineSubmerged
				
					//Show if sonar is active and sub is within distance
					IF IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_SONAR_SWEEP_SHOWN) 
					AND VDIST2_2D(GlobalplayerBD_FM_4[NATIVE_TO_INT(PlayerSimpleInteriorOwner)].vSubmarineLocation, GET_PLAYER_COORDS(MPGlobals.LocalPlayerID)) < (g_sMPTunables.fRivalSubSonarDistance * g_sMPTunables.fRivalSubSonarDistance)
						SET_BIT(g_PlayerBlipsData.bs_IsMadeVisibleBySonar, NATIVE_TO_INT(playerID))
					ELSE
						IF MPGlobals.LocalPlayerID != INVALID_PLAYER_INDEX()
							IF NOT ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(playerID, MPGlobals.LocalPlayerID, TRUE)
								PRINTLN("[player_blips] SHOULD_BLOCK_PLAYER_BLIP - Player ", NATIVE_TO_INT(playerID), " is in a submerged submarine we aren't")
								RETURN TRUE
							ENDIF
						ENDIF					
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PROC UPDATE_ENEMY_PED_BLIP(NETWORK_INDEX PedID, AI_BLIP_STRUCT &EnemyBlipData, BOOL bIsProfessionalAIGang=FALSE, BOOL bForceBlipOn=FALSE)
//	IF IS_FAKE_MULTIPLAYER_MODE_SET()
//		MPGlobals.LocalPlayerID = PLAYER_ID()
//	ENDIF
//	IF (bIsProfessionalAIGang)
//		UPDATE_AI_PED_BLIP(NET_TO_PED(PedID), EnemyBlipData, BLIP_GANG_PROFESSIONAL, MPGlobals.LocalPlayerID, bForceBlipOn)
//	ELSE
//		UPDATE_AI_PED_BLIP(NET_TO_PED(PedID), EnemyBlipData, DEFAULT, MPGlobals.LocalPlayerID, bForceBlipOn)
//	ENDIF
//ENDPROC

PROC UPDATE_ENEMY_NET_PED_BLIP( NETWORK_INDEX PedID, 
								AI_BLIP_STRUCT &EnemyBlipData, 
								FLOAT fNoticableRadius = -1.0,
								BOOL bIsProfessionalAIGang = FALSE, 
								BOOL bForceBlipOn = FALSE,
								BOOL bShowVisionCone = FALSE,
								STRING strBlipName = NULL,
								INT iColour = -1,
								BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID,
								BOOL bBlipNameIsLiteralString=TRUE,
								BOOL bDisableEnemyAircraftBlips = FALSE) 
								
	IF NETWORK_DOES_NETWORK_ID_EXIST(PedID)
		
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			MPGlobals.LocalPlayerID = PLAYER_ID()
		ENDIF
		
		IF (bIsProfessionalAIGang)
			UPDATE_AI_PED_BLIP( NET_TO_PED(PedID), EnemyBlipData, BLIP_GANG_PROFESSIONAL, MPGlobals.LocalPlayerID, bForceBlipOn, bShowVisionCone, fNoticableRadius, strBlipName, iColour, BlipSprite, bBlipNameIsLiteralString, bDisableEnemyAircraftBlips)
		ELSE
			UPDATE_AI_PED_BLIP( NET_TO_PED(PedID), EnemyBlipData, DEFAULT, MPGlobals.LocalPlayerID, bForceBlipOn, bShowVisionCone, fNoticableRadius, strBlipName, iColour, BlipSprite, bBlipNameIsLiteralString, bDisableEnemyAircraftBlips)
		ENDIF
				
	ELSE
		IF DOES_BLIP_EXIST(EnemyBlipData.BlipID)
			CLEANUP_AI_PED_BLIP(EnemyBlipData)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:set the hight of a blip to be displayed on a blip
///    checks to see how far away the player ped is and displays the height of the blip if that are close enough.
/// PARAMS:
///    biBlip - the blip index to set the height on
PROC NET_SHOW_HEIGHT_ON_BLIP(BLIP_INDEX &biBlip)
	IF NOT IS_PED_INJURED(GET_PLAYER_PED(MPGlobals.LocalPlayerID))
		IF IS_ENTITY_AT_COORD(GET_PLAYER_PED(MPGlobals.LocalPlayerID), GET_ACTUAL_BLIP_COORDS(biBlip), <<25.0, 25.0, 25.0>>)
			SHOW_HEIGHT_ON_BLIP(biBlip, TRUE)
		ELSE
			SHOW_HEIGHT_ON_BLIP(biBlip, FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_NOTICABLE(PLAYER_INDEX playerID, INT iCoolDownTime)

	FLOAT fPlayerDist
	FLOAT fWeaponRange
	MODEL_NAMES ThisModel
	
	VEHICLE_INDEX VehicleID
	ENTITY_INDEX EntityID

	#IF IS_DEBUG_BUILD
		IF NOTICABLE_RADIUS != lw_fNOTICABLE_RADIUS
			NOTICABLE_RADIUS = lw_fNOTICABLE_RADIUS
		ENDIF
		IF NOTICABLE_TIME!=lw_icooldowntimer
			NOTICABLE_TIME =  lw_icooldowntimer
		ENDIF
	#ENDIF	
	
	fPlayerDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_PLAYER_PED(MPGlobals.LocalPlayerID), FALSE), GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID), FALSE)) 
	
	// set a min dist
	IF (fPlayerDist < MIN_NOISE_DIST)
		fPlayerDist = MIN_NOISE_DIST
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		g_PlayerBlipsData.fPlayerNoise[NATIVE_TO_INT(PlayerID)] = GET_PLAYER_CURRENT_STEALTH_NOISE(PlayerID)
//	#ENDIF
#IF FEATURE_FREEMODE_ARCADE
#IF FEATURE_COPS_N_CROOKS
	IF IS_FREEMODE_ARCADE()
		IF GET_ARCADE_MODE() = ARC_COPS_CROOKS
			RETURN(FALSE)
		ENDIF
	ENDIF
#ENDIF	
#ENDIF
			
	// if in a veh dm
	IF IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
	OR Is_Player_Currently_On_MP_Versus_Mission(MPGlobals.LocalPlayerID)
		// if local player is in a plane make the range higher
		IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(MPGlobals.LocalPlayerID))					
			IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
				IF (GET_ENTITY_SPEED(GET_PLAYER_PED(playerID)) > 5.0)		
				
					IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(MPGlobals.LocalPlayerID))
						ThisModel = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(GET_PLAYER_PED(MPGlobals.LocalPlayerID)))			
					ENDIF
					
					IF IS_THIS_MODEL_A_PLANE(ThisModel)
					OR IS_THIS_MODEL_A_HELI(ThisModel)
						IF (fPlayerDist < 500.0)
							g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()
							#IF IS_DEBUG_BUILD
								IF NOT (NATIVE_TO_INT(PlayerID) = -1)
									IF (MPGlobals.LocalPlayerID = PLAYER_ID())
										IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_VEH_DM_WITHIN_RANGE)
											SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_VEH_DM_WITHIN_RANGE)
										ENDIF
									ENDIF
								ENDIF
							#ENDIF		
							RETURN(TRUE)				
						ENDIF
					ELSE
						IF (fPlayerDist < 250.0)		
							g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()
							#IF IS_DEBUG_BUILD
								IF NOT (NATIVE_TO_INT(PlayerID) = -1)
									IF (MPGlobals.LocalPlayerID = PLAYER_ID())
										IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_VEH_DM_WITHIN_RANGE)
											SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_VEH_DM_WITHIN_RANGE)
										ENDIF
									ENDIF
								ENDIF
							#ENDIF		
							RETURN(TRUE)				
						ENDIF	
					ENDIF					
				ENDIF
			ENDIF	
		ENDIF		
	ENDIF
	
	
	IF (MPGlobals.LocalPlayerID = PLAYER_ID()) // targetting entity commands can only be called on local player
		
		
		IF IS_NET_PLAYER_OK(PLAYER_ID())

		
			// player can see player
			IF (MPGlobals.LocalPlayerID = PLAYER_ID())
				IF CAN_I_SEE_PLAYER(PlayerID, VISIBLE_HEIGHT)
					g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()	
					#IF IS_DEBUG_BUILD
						IF NOT (NATIVE_TO_INT(PlayerID) = -1)
							IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_CAN_SEE_PLAYER)
								SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_CAN_SEE_PLAYER)
							ENDIF
						ENDIF
					#ENDIF		
					RETURN(TRUE)					
				ENDIF
			ENDIF
				
			// is player targetting player
			IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), GET_PLAYER_PED(PlayerID) )
			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), GET_PLAYER_PED(PlayerID) )
				g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()
				#IF IS_DEBUG_BUILD
					IF NOT (NATIVE_TO_INT(PlayerID) = -1)
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_IS_TARGETTED)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_IS_TARGETTED)
						ENDIF
					ENDIF
				#ENDIF		
				RETURN(TRUE)	
			ENDIF
					
			
			// is player locking on in a plane (or other vehicle)
			IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(MPGlobals.LocalPlayerID))			
				VehicleID = GET_VEHICLE_PED_IS_USING(GET_PLAYER_PED(MPGlobals.LocalPlayerID))				
				IF (DOES_ENTITY_EXIST(VehicleID) AND NOT IS_ENTITY_DEAD(VehicleID))		
					IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)
						IF GET_VEHICLE_LOCK_ON_TARGET(VehicleID, EntityID)	
							IF (DOES_ENTITY_EXIST(EntityID) AND NOT IS_ENTITY_DEAD(EntityID))
								IF (GET_VEHICLE_HOMING_LOCKON_STATE(VehicleID) = HLOS_ACQUIRED)												
									IF (IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID)) AND (EntityID = GET_ENTITY_FROM_PED_OR_VEHICLE(GET_VEHICLE_PED_IS_USING(GET_PLAYER_PED(playerID)))))
									OR (NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID)) AND (EntityID = GET_ENTITY_FROM_PED_OR_VEHICLE(GET_PLAYER_PED(playerID))))
										g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()
										#IF IS_DEBUG_BUILD
											IF NOT (NATIVE_TO_INT(PlayerID) = -1)
												IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_LOCKED_ON_BY_VEHICLE)
													SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_LOCKED_ON_BY_VEHICLE)
												ENDIF
											ENDIF
										#ENDIF		
										RETURN(TRUE)
									ENDIF												
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	
	// check the stealth noise
	FLOAT fLocalNoise
	fLocalNoise = g_PlayerBlipsData.fMyLoudNoise * LOCAL_NOISE_MUTIPLIER
	IF (fPlayerDist + fLocalNoise < GET_PLAYER_CURRENT_STEALTH_NOISE(PlayerID))
		g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()
		#IF IS_DEBUG_BUILD
			IF NOT (NATIVE_TO_INT(PlayerID) = -1)
				IF (MPGlobals.LocalPlayerID = PLAYER_ID())
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_STEALTH_NOISE)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_STEALTH_NOISE)
					ENDIF
				ENDIF
			ENDIF
		#ENDIF		
		RETURN(TRUE)	
	ENDIF
	
//	// is player using mic near player
//	IF (fPlayerDist < CHAT_PROXIMITY)
//		IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iPlayerInteractionFlags, PIBD_IsChattingOnHeadset)
//			g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()			
//			#IF IS_DEBUG_BUILD
//				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
//					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
//						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_CHATTING_IN_RANGE)
//							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_CHATTING_IN_RANGE)
//						ENDIF
//					ENDIF
//				ENDIF
//			#ENDIF				
//			RETURN(TRUE)
//		ENDIF
//	ENDIF
	

	// is player is making car noise
	IF (fPlayerDist < VEHICLE_NOISE_DIST)
		IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iPlayerInteractionFlags, PIBD_IsMakingVehicleNoise))
			g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()			
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_MAKING_CAR_NOISE)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_MAKING_CAR_NOISE)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF				
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	
	// if player has just been damaged by other player then blip them
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_PLAYER_PED(MPGlobals.LocalPlayerID), GET_PLAYER_PED(PlayerID))
		g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()
		#IF IS_DEBUG_BUILD
			IF NOT (NATIVE_TO_INT(PlayerID) = -1)
				IF (MPGlobals.LocalPlayerID = PLAYER_ID())
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_DAMAGED_BY)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_DAMAGED_BY)
					ENDIF
				ENDIF
			ENDIF
		#ENDIF	
		RETURN(TRUE)
	ENDIF
	
//	// if player is talking on loudhailer
//	IF (GET_PEDS_CURRENT_WEAPON(GET_PLAYER_PED(playerID)) = WEAPONTYPE_DLC_LOUDHAILER)
//		IF NETWORK_IS_PLAYER_TALKING(playerID)
//			g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()
//			#IF IS_DEBUG_BUILD
//				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
//					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
//						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_TALKING_LOUDHAILER)
//							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_TALKING_LOUDHAILER)
//						ENDIF
//					ENDIF
//				ENDIF
//			#ENDIF			
//			RETURN(TRUE)
//		ENDIF
//	ENDIF		
	
	// if player is in a car
	IF (fPlayerDist < 20.0)
		IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerID))
			IF (GET_ENTITY_SPEED(GET_PLAYER_PED(playerID)) > 5.0)
				g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()
				#IF IS_DEBUG_BUILD
					IF NOT (NATIVE_TO_INT(PlayerID) = -1)
						IF (MPGlobals.LocalPlayerID = PLAYER_ID())
							IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_DRIVING_PAST_IN_VEHICLE)
								SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_DRIVING_PAST_IN_VEHICLE)
							ENDIF
						ENDIF
					ENDIF
				#ENDIF				
				RETURN(TRUE)	
			ENDIF
		ENDIF
	ENDIF
	
	// if the player's bIsShooting flag is set (needed as network clones don't register weapons firing beyond 120m)
	IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iPlayerInteractionFlags, PIBD_IsShooting))
		
		IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iPlayerInteractionFlags, PIBD_IsUsingSilencedWeapon))
			fWeaponRange = 10.0
		ELSE
			fWeaponRange = WEAPON_RANGE_OF_PED(GET_PLAYER_PED(PlayerID))
		ENDIF
	
		IF (fPlayerDist < fWeaponRange)
			g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_FIRING_IN_RANGE)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_FIRING_IN_RANGE)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF			
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	// player is idling
	IF IS_PLAYER_BLIP_SET_AS_IDLE(PlayerID)
	
		g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()
		g_PlayerBlipsData.iBlipTimerIdle[NATIVE_TO_INT(playerID)] = GET_NETWORK_TIME()
		
		#IF IS_DEBUG_BUILD
			IF NOT (NATIVE_TO_INT(PlayerID) = -1)
				IF (MPGlobals.LocalPlayerID = PLAYER_ID())
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_IDLING)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_IDLING)
					ENDIF
				ENDIF
			ENDIF
		#ENDIF	
		
		RETURN(TRUE)
	ENDIF	
	
	
	// check noticable timer
	IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)]) < iCoolDownTime)
		IF IS_SCREEN_FADED_IN()
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_LESS_THAN_COOL_DOWN_TIME)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)], NOTICABLE_REASON_LESS_THAN_COOL_DOWN_TIME)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF		
			RETURN(TRUE)
		ENDIF
	ENDIF	
	
	RETURN(FALSE)

ENDFUNC


PROC RESET_PLAYER_ARROW_TO_WHITE()
	BLIP_INDEX PlayerBlipID
	PlayerBlipID = GET_MAIN_PLAYER_BLIP_ID()
	IF DOES_BLIP_EXIST(PlayerBlipID)	
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(PlayerBlipID, HUD_COLOUR_WHITE)
		RESTORE_BLIP_ALPHA(PlayerBlipID)
		SET_BLIP_DISPLAY(PlayerBlipID, DISPLAY_BOTH)
		#IF IS_DEBUG_BUILD
		IF (g_PlayerBlipsData.bDebugBlipOutput)
			CPRINTLN(DEBUG_BLIP, "[player_blips] RESET_PLAYER_ARROW_TO_WHITE called on blip ", NATIVE_TO_INT(PlayerBlipID))
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC HUD_COLOURS FRIENDLY_ARROW_COLOUR()
	RETURN HUD_COLOUR_BLUELIGHT
ENDFUNC

FUNC BOOL IS_PLAYER_ARROW_FLASHING_RED(PLAYER_INDEX PlayerID)
	INT iPlayerInt = NATIVE_TO_INT(PlayerID)
	IF IS_BIT_SET(GlobalplayerBD[iPlayerInt].playerBlipData.iFlags, PBBD_IS_FLASHING_RED)
	
		// we can only ever be processing one arrow at a time, so no need to have separate b_ArrowIsFlashedRed for each player, likewise with the timer.
		// the flash duration was set as bd so the spectator flashes at the desired rate. 3010948
		IF NOT (g_PlayerBlipsData.b_ArrowIsFlashedRed)
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.RedFlashArrowTimer)) > GlobalplayerBD[iPlayerInt].playerBlipData.iWhiteFlashDuration
				g_PlayerBlipsData.b_ArrowIsFlashedRed = TRUE	
				g_PlayerBlipsData.RedFlashArrowTimer = GET_NETWORK_TIME()
			ENDIF
		ELSE
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.RedFlashArrowTimer)) > GlobalplayerBD[iPlayerInt].playerBlipData.iRedFlashDuration
				g_PlayerBlipsData.b_ArrowIsFlashedRed = FALSE	
				g_PlayerBlipsData.RedFlashArrowTimer = GET_NETWORK_TIME()
			ENDIF
		ENDIF
		
		IF (g_PlayerBlipsData.b_ArrowIsFlashedRed)
			RETURN(TRUE)	
		ENDIF		
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ARROW_FLASHING()
	IF IS_THREAD_ACTIVE(g_PlayerBlipsData.FlashMyArrowThreadID)
	AND NOT IS_PAUSE_MENU_ACTIVE()
		IF NOT (g_PlayerBlipsData.bFlashMyArrowIsHidden)
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.FlashMyArrowTimer)) > 500
				g_PlayerBlipsData.bFlashMyArrowIsHidden = TRUE	
				g_PlayerBlipsData.FlashMyArrowTimer = GET_NETWORK_TIME()
			ENDIF
		ELSE
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.FlashMyArrowTimer)) > 500
				g_PlayerBlipsData.bFlashMyArrowIsHidden = FALSE	
				g_PlayerBlipsData.FlashMyArrowTimer = GET_NETWORK_TIME()
			ENDIF
		ENDIF
		
		IF (g_PlayerBlipsData.bFlashMyArrowIsHidden)
			RETURN(TRUE)	
		ENDIF		
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC HUD_COLOURS GET_PLAYER_ARROW_BLIP_COLOUR(PLAYER_INDEX PlayerID)
	
	
	#IF FEATURE_FREEMODE_ARCADE
	IF IS_FREEMODE_ARCADE()
		HUD_COLOURS HudColour
		HudColour = GET_ARCADE_TEAM_HUD_COLOUR(GET_ARCADE_MODE(), GET_PLAYER_TEAM(PlayerID), PlayerID)
		IF (HudColour != INT_TO_ENUM(HUD_COLOURS, -1))
			RETURN HudColour
		ENDIF
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (g_PlayerBlipsData.iReturnPlayerArrowColour > -1)
			SWITCH g_PlayerBlipsData.iReturnPlayerArrowColour
				CASE 0
					RETURN HUD_COLOUR_WHITE
				BREAK
				CASE 1
					RETURN HUD_COLOUR_RED
				BREAK
				CASE 2
					RETURN FRIENDLY_ARROW_COLOUR()
				BREAK
				CASE 3
					RETURN HUD_COLOUR_NET_PLAYER32
				BREAK
				DEFAULT
					RETURN HUD_COLOUR_WHITE
				BREAK
			ENDSWITCH
		ENDIF
	#ENDIF
	
	IF IS_PLAYER_ARROW_FLASHING_RED(PlayerID)
	
		RETURN(HUD_COLOUR_RED)
	
	ELSE

	

		// am i being blipped by another gang member?
		IF (GlobalplayerBD[NATIVE_TO_INT(playerID)].bMyBlipIsVisible)
			IF IS_PLAYER_ON_FREEMODE_TEAM_MISSION(PlayerID) 
				RETURN(HUD_COLOUR_NET_PLAYER32) //RETURN(HUD_COLOUR_BLUELIGHT)
			ELIF (GlobalplayerBD_FM[NATIVE_TO_INT(PlayerID)].iCurrentMissionType = FMMC_TYPE_DEATHMATCH)
				RETURN(HUD_COLOUR_RED)
			ENDIF
		ELSE
			IF IS_PLAYER_ON_FREEMODE_TEAM_MISSION(PlayerID)
				//RETURN(FRIENDLY_DM_COLOUR())
				RETURN FRIENDLY_ARROW_COLOUR()
			ENDIF
		ENDIF	
	
	ENDIF
	
	
	RETURN(HUD_COLOUR_WHITE)
ENDFUNC


FUNC BOOL ARE_BOTH_PLAYERS_ON_ANY_MP_MISSION(PLAYER_INDEX PlayerID1, PLAYER_INDEX PlayerID2)
	IF IS_PLAYER_ON_ANY_MP_MISSION(PlayerID1)
	AND IS_PLAYER_ON_ANY_MP_MISSION(PlayerID2)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL ARE_BOTH_PLAYERS_OFF_ANY_MP_MISSION(PLAYER_INDEX PlayerID1, PLAYER_INDEX PlayerID2)
	IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PlayerID1)
	AND NOT IS_PLAYER_ON_ANY_MP_MISSION(PlayerID2)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL DOES_MATCH_SECONDARY_BLIP_COLOUR(INT iPlayerIDInt, INT iR, INT iG, INT iB)
	IF (g_PlayerBlipsData.iSecondaryBlipColourR[iPlayerIDInt] = iR)
	AND (g_PlayerBlipsData.iSecondaryBlipColourG[iPlayerIDInt] = iG)
	AND (g_PlayerBlipsData.iSecondaryBlipColourB[iPlayerIDInt] = iB)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

// *******************************************************************************************************************************************************************
// *******************************************************************************************************************************************************************
//		Functions for dealing with different types of sprite blips.
// *******************************************************************************************************************************************************************
// *******************************************************************************************************************************************************************
FUNC FLOAT GET_BASE_SCALE_FOR_BLIP_SPRITE(BLIP_SPRITE BlipSprite, FLOAT fDefaultScale=BLIP_SIZE_NETWORK_PED)
	SWITCH BlipSprite
		CASE RADAR_TRACE_TANK
		CASE RADAR_TRACE_PLAYER_GUNCAR
			RETURN BASE_TANK_BLIP_SCALE	
		BREAK
		CASE RADAR_TRACE_PLAYER_JET
		CASE RADAR_TRACE_PLAYER_PLANE
		CASE RADAR_TRACE_PLAYER_HELI
		CASE RADAR_TRACE_ENEMY_HELI_SPIN 
		CASE RADAR_TRACE_HELICOPTER
		CASE RADAR_TRACE_PLAYER_BOAT
			RETURN BLIP_SIZE_NETWORK_VEHICLE	
		BREAK
		CASE RADAR_TRACE_EX_VECH_6
		CASE RADAR_TRACE_EX_VECH_7
			RETURN BLIP_SIZE_NETWORK_VEHICLE
		BREAK
		CASE RADAR_TRACE_NHP_VEH1
			RETURN BLIP_SIZE_NETWORK_VEHICLE_LARGE
		BREAK
		CASE RADAR_TRACE_ARENA_RC_CAR 
			RETURN 1.0
		BREAK
		CASE RADAR_TRACE_FLAMING_SKULL
			RETURN 1.2
		BREAK
		#IF FEATURE_CASINO_HEIST
		CASE RADAR_TRACE_RC_TANK
			RETURN 1.0
		BREAK
		#ENDIF
	ENDSWITCH
	RETURN fDefaultScale 
ENDFUNC


FUNC BOOL IS_THIS_MODEL_A_SEA_VEHICLE(MODEL_NAMES ModelName)
	IF IS_THIS_MODEL_A_BOAT(ModelName)
	OR (ModelName = SUBMERSIBLE)
	#IF FEATURE_HEIST_ISLAND
	OR (ModelName = AVISA)
	#ENDIF
	//OR (ModelName = SUBMERSIBLE2)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC


FUNC BLIP_SPRITE GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(MODEL_NAMES ModelName, BOOL bForAIPed=FALSE, BOOL bIsEmpty=FALSE)
	
	SWITCH ModelName
		CASE CHAMPION	RETURN RADAR_TRACE_D_CHAMPION
		CASE BUFFALO4	RETURN RADAR_TRACE_BUFFALO_4
		CASE DEITY		RETURN RADAR_TRACE_DEITY
		CASE JUBILEE	RETURN RADAR_TRACE_JUBILEE
		CASE GRANGER2	RETURN RADAR_TRACE_GRANGER2
		CASE PATRIOT3	RETURN RADAR_TRACE_PATRIOT3
		
		CASE KOSATKA		RETURN RADAR_TRACE_SUB2
		CASE AVISA			RETURN RADAR_TRACE_MINI_SUB
		CASE SEASPARROW2	RETURN RADAR_TRACE_SEASPARROW2
		CASE ALKONOST		RETURN RADAR_TRACE_FOLDING_WING_JET
		CASE MANCHEZ2		RETURN RADAR_TRACE_GANG_BIKE
		CASE VERUS			RETURN RADAR_TRACE_MILITARY_QUAD
		CASE SQUADDIE		RETURN RADAR_TRACE_SQUADEE
		CASE DINGHY5		RETURN RADAR_TRACE_DINGHY2
		CASE WINKY			RETURN RADAR_TRACE_WINKY
		CASE PATROLBOAT		RETURN RADAR_TRACE_PATROL_BOAT
		CASE ANNIHILATOR2	RETURN RADAR_TRACE_VALYKRIE2
		CASE VETO			RETURN RADAR_TRACE_KART_RETRO
		CASE VETO2			RETURN RADAR_TRACE_KART_MODERN
		CASE VETIR			RETURN RADAR_TRACE_MILITARY_TRUCK
		
		CASE BRUISER
		CASE BRUISER2
		CASE BRUISER3		RETURN RADAR_TRACE_ARENA_BRUISER
		
		CASE BRUTUS	
		CASE BRUTUS2
		CASE BRUTUS3		RETURN RADAR_TRACE_ARENA_BRUTUS
		
		CASE CERBERUS	
		CASE CERBERUS2	
		CASE CERBERUS3		RETURN RADAR_TRACE_ARENA_CERBERUS
		
		CASE DEATHBIKE	
		CASE DEATHBIKE2	
		CASE DEATHBIKE3		RETURN RADAR_TRACE_ARENA_DEATHBIKE
		
		
		CASE DOMINATOR4
		CASE DOMINATOR5
		CASE DOMINATOR6		RETURN RADAR_TRACE_ARENA_DOMINATOR
		
		CASE IMPALER2	
		CASE IMPALER3	
		CASE IMPALER4		RETURN RADAR_TRACE_ARENA_IMPALER
		
		CASE IMPERATOR	
		CASE IMPERATOR2	
		CASE IMPERATOR3		RETURN RADAR_TRACE_ARENA_IMPERATOR
		
		CASE ISSI4		
		CASE ISSI5		
		CASE ISSI6			RETURN RADAR_TRACE_ARENA_ISSI
		
		CASE MONSTER3	
		CASE MONSTER4	
		CASE MONSTER5		RETURN RADAR_TRACE_ARENA_SASQUATCH
		
		CASE SCARAB		
		CASE SCARAB2	
		CASE SCARAB3		RETURN RADAR_TRACE_ARENA_SCARAB
		
		CASE SLAMVAN4	
		CASE SLAMVAN5	
		CASE SLAMVAN6		RETURN RADAR_TRACE_ARENA_SLAMVAN
		
		CASE ZR380		
		CASE ZR3802		
		CASE ZR3803			RETURN RADAR_TRACE_ARENA_ZR380
		
		CASE RHINO			RETURN RADAR_TRACE_TANK		
		CASE STRIKEFORCE	RETURN RADAR_TRACE_BAT_WP7		
		CASE AVENGER		RETURN RADAR_TRACE_NHP_VEH1
		CASE SEASPARROW		RETURN RADAR_TRACE_ACSR_WP1 
		CASE TRAILERLARGE	RETURN RADAR_TRACE_GR_COVERT_OPS
		CASE TERBYTE		RETURN RADAR_TRACE_BAT_WP1
		CASE PBUS2			RETURN RADAR_TRACE_BAT_PBUS
		CASE MENACER		RETURN RADAR_TRACE_BAT_WP2
		CASE SCRAMJET		RETURN RADAR_TRACE_BAT_WP3
		CASE OPPRESSOR2		RETURN RADAR_TRACE_OPPRESSOR_2
		CASE RCBANDITO		RETURN RADAR_TRACE_ARENA_RC_CAR
		CASE RROCKET		RETURN RADAR_TRACE_GANG_BIKE
		CASE MINITANK		RETURN RADAR_TRACE_RC_TANK
	ENDSWITCH
	
	#IF FEATURE_DLC_2_2022
	IF ModelName = GET_ACID_LAB_MODEL()
		RETURN RADAR_TRACE_BAT_WP1 // @TODO_JD
	ELIF ModelName = DUSTER
	AND GB_GET_MISSION_VARIATION_PLAYER_IS_ON(PLAYER_ID(), FMMC_TYPE_DRUG_LAB_WORK) = ENUM_TO_INT(DLWV_CROP_DUSTING)
		RETURN RADAR_TRACE_ARMS_DEALING_AIR
	ENDIF
	#ENDIF
	
	IF IS_THIS_MODEL_A_FIGHTER_JET(ModelName)
		RETURN RADAR_TRACE_PLAYER_JET
	ELIF IS_THIS_MODEL_A_PLANE(ModelName)	
		RETURN RADAR_TRACE_PLAYER_PLANE
	ELIF IS_THIS_MODEL_A_HELI(ModelName)
		IF NOT (bForAIPed)
			IF NOT (bIsEmpty)
				RETURN RADAR_TRACE_PLAYER_HELI
			ELSE
				RETURN RADAR_TRACE_HELICOPTER
			ENDIF
		ELSE 
			RETURN RADAR_TRACE_ENEMY_HELI_SPIN
		ENDIF
	ELIF IS_THIS_MODEL_A_TURRET_VEHICLE(ModelName)
		IF (ModelName = LIMO2)
			RETURN RADAR_TRACE_TURRETED_LIMO
		ELIF (ModelName = CARACARA)
			RETURN GET_BLIP_SPRITE_FOR_SPECIAL_VEHICLE_MODEL(ModelName)
		ELSE
			RETURN RADAR_TRACE_PLAYER_GUNCAR	
		ENDIF
	ELIF IS_THIS_MODEL_A_SEA_VEHICLE(ModelName)
		RETURN RADAR_TRACE_PLAYER_BOAT
	
	// Special Functions
	ELIF GET_FDS_VEHICLE_INDEX(ModelName) != -1
		RETURN GET_BLIP_SPRITE_FOR_SPECIAL_VEHICLE_MODEL(ModelName)
	ELIF IS_THIS_MODEL_A_SPECIAL_VEHICLE(ModelName)
		RETURN GET_BLIP_SPRITE_FOR_SPECIAL_VEHICLE_MODEL(ModelName)
	ELIF GET_WVM_VEHICLE_INDEX(ModelName) != -1
		RETURN GET_BLIP_SPRITE_FOR_SPECIAL_VEHICLE_MODEL(ModelName)
	ELIF WAM_GET_STEAL_MISSION_UNLOCK_COUNT_FROM_VEHICLE_MODEL(ModelName) > 0
		RETURN GET_BLIP_SPRITE_FOR_SPECIAL_VEHICLE_MODEL(ModelName)
	ENDIF	
	
	RETURN GET_STANDARD_BLIP_ENUM_ID()
ENDFUNC


FUNC BOOL SHOULD_BLIP_SPRITE_ONLY_SHOW_LEADERS_WHEN_IN_VEHICLE(BLIP_SPRITE BlipSprite)
	SWITCH BlipSprite
		CASE RADAR_TRACE_PLAYER_HELI
		CASE RADAR_TRACE_ENEMY_HELI_SPIN
		CASE RADAR_TRACE_PLAYER_BOAT
		CASE RADAR_TRACE_PLAYER_GUNCAR
		CASE RADAR_TRACE_PLAYER_JET
		CASE RADAR_TRACE_PLAYER_PLANE
		CASE RADAR_TRACE_EX_VECH_6
		CASE RADAR_TRACE_EX_VECH_7
		CASE RADAR_TRACE_GR_COVERT_OPS
		CASE RADAR_TRACE_NHP_VEH1
		CASE RADAR_TRACE_TURRETED_LIMO
		CASE RADAR_TRACE_ACSR_WP1
		CASE RADAR_TRACE_ACSR_WP2
		CASE RADAR_TRACE_BAT_WP2
		CASE RADAR_TRACE_SUB2
			RETURN(TRUE)
		BREAK
	ENDSWITCH
	IF BlipSprite = GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_HACKER_TRUCK_MODEL())
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BLIP_SPRITE GET_EMPTY_VERSION_OF_BLIP_SPRITE(BLIP_SPRITE BlipSprite)
	SWITCH BlipSprite
		CASE RADAR_TRACE_PLAYER_HELI
			RETURN RADAR_TRACE_HELICOPTER
		BREAK
		CASE RADAR_TRACE_EX_VECH_1 	RETURN RADAR_TRACE_EX_VECH_1 	BREAK
		CASE RADAR_TRACE_EX_VECH_2 	RETURN RADAR_TRACE_EX_VECH_2 	BREAK
		CASE RADAR_TRACE_EX_VECH_3 	RETURN RADAR_TRACE_EX_VECH_3 	BREAK
		CASE RADAR_TRACE_EX_VECH_4 	RETURN RADAR_TRACE_EX_VECH_4	BREAK
		CASE RADAR_TRACE_EX_VECH_5 	RETURN RADAR_TRACE_EX_VECH_5	BREAK
		CASE RADAR_TRACE_EX_VECH_6	RETURN RADAR_TRACE_EX_VECH_6 	BREAK
		CASE RADAR_TRACE_EX_VECH_7	RETURN RADAR_TRACE_EX_VECH_7 	BREAK
		CASE RADAR_TRACE_QUAD		RETURN RADAR_TRACE_QUAD			BREAK
		
		CASE RADAR_TRACE_GR_WVM_1	RETURN RADAR_TRACE_GR_WVM_1		BREAK
		CASE RADAR_TRACE_GR_WVM_2	RETURN RADAR_TRACE_GR_WVM_2		BREAK
		CASE RADAR_TRACE_GR_WVM_3	RETURN RADAR_TRACE_GR_WVM_3		BREAK
		CASE RADAR_TRACE_GR_WVM_4	RETURN RADAR_TRACE_GR_WVM_4		BREAK
		CASE RADAR_TRACE_GR_WVM_5	RETURN RADAR_TRACE_GR_WVM_5		BREAK
		CASE RADAR_TRACE_GR_WVM_6	RETURN RADAR_TRACE_GR_WVM_6		BREAK
		
		CASE RADAR_TRACE_NHP_WP1	RETURN RADAR_TRACE_NHP_WP1		BREAK
		CASE RADAR_TRACE_NHP_WP2	RETURN RADAR_TRACE_NHP_WP2		BREAK
		CASE RADAR_TRACE_NHP_WP3	RETURN RADAR_TRACE_NHP_WP3		BREAK
		CASE RADAR_TRACE_NHP_WP4	RETURN RADAR_TRACE_NHP_WP4		BREAK
		CASE RADAR_TRACE_NHP_WP5	RETURN RADAR_TRACE_NHP_WP5		BREAK
		CASE RADAR_TRACE_NHP_WP6	RETURN RADAR_TRACE_NHP_WP6		BREAK
		CASE RADAR_TRACE_NHP_WP7	RETURN RADAR_TRACE_NHP_WP7		BREAK
		CASE RADAR_TRACE_NHP_WP8	RETURN RADAR_TRACE_NHP_WP8		BREAK
		CASE RADAR_TRACE_NHP_WP9	RETURN RADAR_TRACE_NHP_WP9		BREAK
		CASE RADAR_TRACE_NHP_VEH1	RETURN RADAR_TRACE_NHP_VEH1		BREAK
		
		CASE RADAR_TRACE_BAT_PBUS	RETURN RADAR_TRACE_BAT_PBUS		BREAK
		CASE RADAR_TRACE_BAT_WP1	RETURN RADAR_TRACE_BAT_WP1		BREAK
		CASE RADAR_TRACE_BAT_WP2	RETURN RADAR_TRACE_BAT_WP2		BREAK
		CASE RADAR_TRACE_BAT_WP3	RETURN RADAR_TRACE_BAT_WP3		BREAK
		CASE RADAR_TRACE_BAT_WP4	RETURN RADAR_TRACE_BAT_WP4		BREAK
		CASE RADAR_TRACE_BAT_WP5	RETURN RADAR_TRACE_BAT_WP5		BREAK
		CASE RADAR_TRACE_BAT_WP6	RETURN RADAR_TRACE_BAT_WP6		BREAK
		CASE RADAR_TRACE_BAT_WP7	RETURN RADAR_TRACE_BAT_WP7		BREAK
		
		CASE RADAR_TRACE_ARENA_BRUISER		RETURN RADAR_TRACE_ARENA_BRUISER	BREAK
		CASE RADAR_TRACE_ARENA_BRUTUS		RETURN RADAR_TRACE_ARENA_BRUTUS		BREAK
		CASE RADAR_TRACE_ARENA_CERBERUS		RETURN RADAR_TRACE_ARENA_CERBERUS	BREAK
		CASE RADAR_TRACE_ARENA_DEATHBIKE	RETURN RADAR_TRACE_ARENA_DEATHBIKE	BREAK
		CASE RADAR_TRACE_ARENA_DOMINATOR	RETURN RADAR_TRACE_ARENA_DOMINATOR	BREAK
		CASE RADAR_TRACE_ARENA_IMPALER		RETURN RADAR_TRACE_ARENA_IMPALER	BREAK
		CASE RADAR_TRACE_ARENA_IMPERATOR	RETURN RADAR_TRACE_ARENA_IMPERATOR	BREAK
		CASE RADAR_TRACE_ARENA_ISSI			RETURN RADAR_TRACE_ARENA_ISSI		BREAK
		CASE RADAR_TRACE_ARENA_SASQUATCH	RETURN RADAR_TRACE_ARENA_SASQUATCH	BREAK
		CASE RADAR_TRACE_ARENA_SCARAB		RETURN RADAR_TRACE_ARENA_SCARAB		BREAK
		CASE RADAR_TRACE_ARENA_SLAMVAN		RETURN RADAR_TRACE_ARENA_SLAMVAN	BREAK
		CASE RADAR_TRACE_ARENA_ZR380		RETURN RADAR_TRACE_ARENA_ZR380		BREAK
		CASE RADAR_TRACE_ARENA_RC_CAR		RETURN RADAR_TRACE_ARENA_RC_CAR		BREAK
		CASE RADAR_TRACE_RC_TANK			RETURN RADAR_TRACE_RC_TANK			BREAK
		
		CASE RADAR_TRACE_MINI_SUB			RETURN RADAR_TRACE_MINI_SUB			BREAK
		CASE RADAR_TRACE_SEASPARROW2		RETURN RADAR_TRACE_SEASPARROW2		BREAK
		CASE RADAR_TRACE_FOLDING_WING_JET	RETURN RADAR_TRACE_FOLDING_WING_JET	BREAK
		CASE RADAR_TRACE_GANG_BIKE			RETURN RADAR_TRACE_GANG_BIKE		BREAK
		CASE RADAR_TRACE_MILITARY_QUAD		RETURN RADAR_TRACE_MILITARY_QUAD	BREAK
		CASE RADAR_TRACE_SQUADEE			RETURN RADAR_TRACE_SQUADEE			BREAK
		CASE RADAR_TRACE_DINGHY2			RETURN RADAR_TRACE_DINGHY2			BREAK
		CASE RADAR_TRACE_WINKY				RETURN RADAR_TRACE_WINKY			BREAK
		CASE RADAR_TRACE_PATROL_BOAT		RETURN RADAR_TRACE_PATROL_BOAT		BREAK
		CASE RADAR_TRACE_VALYKRIE2			RETURN RADAR_TRACE_VALYKRIE2		BREAK
		CASE RADAR_TRACE_KART_RETRO			RETURN RADAR_TRACE_KART_RETRO		BREAK
		CASE RADAR_TRACE_KART_MODERN		RETURN RADAR_TRACE_KART_MODERN		BREAK
		CASE RADAR_TRACE_MILITARY_TRUCK		RETURN RADAR_TRACE_MILITARY_TRUCK	BREAK
		
		#IF FEATURE_FIXER
		CASE RADAR_TRACE_D_CHAMPION			RETURN RADAR_TRACE_D_CHAMPION		BREAK
		CASE RADAR_TRACE_BUFFALO_4			RETURN RADAR_TRACE_BUFFALO_4		BREAK
		CASE RADAR_TRACE_DEITY				RETURN RADAR_TRACE_DEITY			BREAK
		CASE RADAR_TRACE_JUBILEE			RETURN RADAR_TRACE_JUBILEE			BREAK
		CASE RADAR_TRACE_GRANGER2			RETURN RADAR_TRACE_GRANGER2			BREAK
		CASE RADAR_TRACE_PATRIOT3			RETURN RADAR_TRACE_PATRIOT3			BREAK
		#ENDIF
	ENDSWITCH
	RETURN BlipSprite
ENDFUNC
FUNC BLIP_SPRITE GET_CCCUPIED_VERSION_OF_BLIP_SPRITE(BLIP_SPRITE BlipSprite)
	SWITCH BlipSprite
		CASE RADAR_TRACE_HELICOPTER 
			RETURN RADAR_TRACE_PLAYER_HELI
		BREAK
		CASE RADAR_TRACE_EX_VECH_1 	RETURN RADAR_TRACE_EX_VECH_1 	BREAK
		CASE RADAR_TRACE_EX_VECH_2	RETURN RADAR_TRACE_EX_VECH_2 	BREAK
		CASE RADAR_TRACE_EX_VECH_3	RETURN RADAR_TRACE_EX_VECH_3	BREAK
		CASE RADAR_TRACE_EX_VECH_4	RETURN RADAR_TRACE_EX_VECH_4	BREAK
		CASE RADAR_TRACE_EX_VECH_5	RETURN RADAR_TRACE_EX_VECH_5 	BREAK
		CASE RADAR_TRACE_EX_VECH_6	RETURN RADAR_TRACE_EX_VECH_6 	BREAK
		CASE RADAR_TRACE_EX_VECH_7	RETURN RADAR_TRACE_EX_VECH_7 	BREAK
		CASE RADAR_TRACE_QUAD		RETURN RADAR_TRACE_QUAD			BREAK
		
		CASE RADAR_TRACE_GR_WVM_1	RETURN RADAR_TRACE_GR_WVM_1		BREAK
		CASE RADAR_TRACE_GR_WVM_2	RETURN RADAR_TRACE_GR_WVM_2		BREAK
		CASE RADAR_TRACE_GR_WVM_3	RETURN RADAR_TRACE_GR_WVM_3		BREAK
		CASE RADAR_TRACE_GR_WVM_4	RETURN RADAR_TRACE_GR_WVM_4		BREAK
		CASE RADAR_TRACE_GR_WVM_5	RETURN RADAR_TRACE_GR_WVM_5		BREAK
		CASE RADAR_TRACE_GR_WVM_6	RETURN RADAR_TRACE_GR_WVM_6		BREAK
		
		CASE RADAR_TRACE_NHP_WP1	RETURN RADAR_TRACE_NHP_WP1		BREAK
		CASE RADAR_TRACE_NHP_WP2	RETURN RADAR_TRACE_NHP_WP2		BREAK
		CASE RADAR_TRACE_NHP_WP3	RETURN RADAR_TRACE_NHP_WP3		BREAK
		CASE RADAR_TRACE_NHP_WP4	RETURN RADAR_TRACE_NHP_WP4		BREAK
		CASE RADAR_TRACE_NHP_WP5	RETURN RADAR_TRACE_NHP_WP5		BREAK
		CASE RADAR_TRACE_NHP_WP6	RETURN RADAR_TRACE_NHP_WP6		BREAK
		CASE RADAR_TRACE_NHP_WP7	RETURN RADAR_TRACE_NHP_WP7		BREAK
		CASE RADAR_TRACE_NHP_WP8	RETURN RADAR_TRACE_NHP_WP8		BREAK
		CASE RADAR_TRACE_NHP_WP9	RETURN RADAR_TRACE_NHP_WP9		BREAK
		CASE RADAR_TRACE_NHP_VEH1	RETURN RADAR_TRACE_NHP_VEH1		BREAK
		
		CASE RADAR_TRACE_BAT_PBUS	RETURN RADAR_TRACE_BAT_PBUS		BREAK
		CASE RADAR_TRACE_BAT_WP1	RETURN RADAR_TRACE_BAT_WP1		BREAK
		CASE RADAR_TRACE_BAT_WP2	RETURN RADAR_TRACE_BAT_WP2		BREAK
		CASE RADAR_TRACE_BAT_WP3	RETURN RADAR_TRACE_BAT_WP3		BREAK
		CASE RADAR_TRACE_BAT_WP4	RETURN RADAR_TRACE_BAT_WP4		BREAK
		CASE RADAR_TRACE_BAT_WP5	RETURN RADAR_TRACE_BAT_WP5		BREAK
		CASE RADAR_TRACE_BAT_WP6	RETURN RADAR_TRACE_BAT_WP6		BREAK
		CASE RADAR_TRACE_BAT_WP7	RETURN RADAR_TRACE_BAT_WP7		BREAK
		
		CASE RADAR_TRACE_ARENA_BRUISER		RETURN RADAR_TRACE_ARENA_BRUISER	BREAK
		CASE RADAR_TRACE_ARENA_BRUTUS		RETURN RADAR_TRACE_ARENA_BRUTUS		BREAK
		CASE RADAR_TRACE_ARENA_CERBERUS		RETURN RADAR_TRACE_ARENA_CERBERUS	BREAK
		CASE RADAR_TRACE_ARENA_DEATHBIKE	RETURN RADAR_TRACE_ARENA_DEATHBIKE	BREAK
		CASE RADAR_TRACE_ARENA_DOMINATOR	RETURN RADAR_TRACE_ARENA_DOMINATOR	BREAK
		CASE RADAR_TRACE_ARENA_IMPALER		RETURN RADAR_TRACE_ARENA_IMPALER	BREAK
		CASE RADAR_TRACE_ARENA_IMPERATOR	RETURN RADAR_TRACE_ARENA_IMPERATOR	BREAK
		CASE RADAR_TRACE_ARENA_ISSI			RETURN RADAR_TRACE_ARENA_ISSI		BREAK
		CASE RADAR_TRACE_ARENA_SASQUATCH	RETURN RADAR_TRACE_ARENA_SASQUATCH	BREAK
		CASE RADAR_TRACE_ARENA_SCARAB		RETURN RADAR_TRACE_ARENA_SCARAB		BREAK
		CASE RADAR_TRACE_ARENA_SLAMVAN		RETURN RADAR_TRACE_ARENA_SLAMVAN	BREAK
		CASE RADAR_TRACE_ARENA_ZR380		RETURN RADAR_TRACE_ARENA_ZR380		BREAK
		CASE RADAR_TRACE_ARENA_RC_CAR		RETURN RADAR_TRACE_ARENA_RC_CAR		BREAK
		CASE RADAR_TRACE_RC_TANK			RETURN RADAR_TRACE_RC_TANK			BREAK
		
		CASE RADAR_TRACE_MINI_SUB			RETURN RADAR_TRACE_MINI_SUB			BREAK
		CASE RADAR_TRACE_SEASPARROW2		RETURN RADAR_TRACE_SEASPARROW2		BREAK
		CASE RADAR_TRACE_FOLDING_WING_JET	RETURN RADAR_TRACE_FOLDING_WING_JET	BREAK
		CASE RADAR_TRACE_GANG_BIKE			RETURN RADAR_TRACE_GANG_BIKE		BREAK
		CASE RADAR_TRACE_MILITARY_QUAD		RETURN RADAR_TRACE_MILITARY_QUAD	BREAK
		CASE RADAR_TRACE_SQUADEE			RETURN RADAR_TRACE_SQUADEE			BREAK
		CASE RADAR_TRACE_DINGHY2			RETURN RADAR_TRACE_DINGHY2			BREAK
		CASE RADAR_TRACE_WINKY				RETURN RADAR_TRACE_WINKY			BREAK
		CASE RADAR_TRACE_PATROL_BOAT		RETURN RADAR_TRACE_PATROL_BOAT		BREAK
		CASE RADAR_TRACE_VALYKRIE2			RETURN RADAR_TRACE_VALYKRIE2		BREAK
		CASE RADAR_TRACE_KART_RETRO			RETURN RADAR_TRACE_KART_RETRO		BREAK
		CASE RADAR_TRACE_KART_MODERN		RETURN RADAR_TRACE_KART_MODERN		BREAK
		CASE RADAR_TRACE_MILITARY_TRUCK		RETURN RADAR_TRACE_MILITARY_TRUCK	BREAK
		
		#IF FEATURE_FIXER
		CASE RADAR_TRACE_D_CHAMPION			RETURN RADAR_TRACE_D_CHAMPION		BREAK
		CASE RADAR_TRACE_BUFFALO_4			RETURN RADAR_TRACE_BUFFALO_4		BREAK
		CASE RADAR_TRACE_DEITY				RETURN RADAR_TRACE_DEITY			BREAK
		CASE RADAR_TRACE_JUBILEE			RETURN RADAR_TRACE_JUBILEE			BREAK
		CASE RADAR_TRACE_GRANGER2			RETURN RADAR_TRACE_GRANGER2			BREAK
		CASE RADAR_TRACE_PATRIOT3			RETURN RADAR_TRACE_PATRIOT3			BREAK
		#ENDIF
	ENDSWITCH
	RETURN BlipSprite
ENDFUNC

FUNC BOOL SHOULD_BLIP_SPRITE_ATLER_DEPENDING_ON_OCCUPANCY(BLIP_SPRITE BlipSprite)
	SWITCH BlipSprite
		CASE RADAR_TRACE_PLAYER_HELI
		CASE RADAR_TRACE_HELICOPTER
		CASE RADAR_TRACE_EX_VECH_1
		CASE RADAR_TRACE_EX_VECH_2
		CASE RADAR_TRACE_EX_VECH_3
		CASE RADAR_TRACE_EX_VECH_4
		CASE RADAR_TRACE_EX_VECH_5
		CASE RADAR_TRACE_EX_VECH_6
		CASE RADAR_TRACE_EX_VECH_7
		CASE RADAR_TRACE_QUAD
		CASE RADAR_TRACE_GR_WVM_1
		CASE RADAR_TRACE_GR_WVM_2
		CASE RADAR_TRACE_GR_WVM_3
		CASE RADAR_TRACE_GR_WVM_4
		CASE RADAR_TRACE_GR_WVM_5
		CASE RADAR_TRACE_GR_WVM_6
		CASE RADAR_TRACE_NHP_WP1
		CASE RADAR_TRACE_NHP_WP2
		CASE RADAR_TRACE_NHP_WP3
		CASE RADAR_TRACE_NHP_WP4
		CASE RADAR_TRACE_NHP_WP5
		CASE RADAR_TRACE_NHP_WP6
		CASE RADAR_TRACE_NHP_WP7
		CASE RADAR_TRACE_NHP_WP8
		CASE RADAR_TRACE_NHP_WP9
		CASE RADAR_TRACE_NHP_VEH1
		CASE RADAR_TRACE_BAT_PBUS
		CASE RADAR_TRACE_BAT_WP1
		CASE RADAR_TRACE_BAT_WP2
		CASE RADAR_TRACE_BAT_WP3
		CASE RADAR_TRACE_BAT_WP4
		CASE RADAR_TRACE_BAT_WP5
		CASE RADAR_TRACE_BAT_WP6
		CASE RADAR_TRACE_BAT_WP7
		CASE RADAR_TRACE_ARENA_BRUISER
		CASE RADAR_TRACE_ARENA_BRUTUS
		CASE RADAR_TRACE_ARENA_CERBERUS
		CASE RADAR_TRACE_ARENA_DEATHBIKE
		CASE RADAR_TRACE_ARENA_DOMINATOR
		CASE RADAR_TRACE_ARENA_IMPALER
		CASE RADAR_TRACE_ARENA_IMPERATOR
		CASE RADAR_TRACE_ARENA_ISSI
		CASE RADAR_TRACE_ARENA_SASQUATCH
		CASE RADAR_TRACE_ARENA_SCARAB
		CASE RADAR_TRACE_ARENA_SLAMVAN
		CASE RADAR_TRACE_ARENA_ZR380
		CASE RADAR_TRACE_ARENA_RC_CAR
		CASE RADAR_TRACE_RC_TANK
		CASE RADAR_TRACE_MINI_SUB
		CASE RADAR_TRACE_SEASPARROW2
		CASE RADAR_TRACE_FOLDING_WING_JET
		CASE RADAR_TRACE_GANG_BIKE
		CASE RADAR_TRACE_MILITARY_QUAD
		CASE RADAR_TRACE_SQUADEE
		CASE RADAR_TRACE_DINGHY2
		CASE RADAR_TRACE_WINKY
		CASE RADAR_TRACE_PATROL_BOAT
		CASE RADAR_TRACE_VALYKRIE2
		CASE RADAR_TRACE_KART_RETRO
		CASE RADAR_TRACE_KART_MODERN
		CASE RADAR_TRACE_MILITARY_TRUCK
		CASE RADAR_TRACE_SUB2
		#IF FEATURE_FIXER
		CASE RADAR_TRACE_D_CHAMPION
		CASE RADAR_TRACE_BUFFALO_4
		CASE RADAR_TRACE_DEITY
		CASE RADAR_TRACE_JUBILEE
		CASE RADAR_TRACE_GRANGER2
		CASE RADAR_TRACE_PATRIOT3
		#ENDIF
			RETURN(TRUE)
		BREAK
	ENDSWITCH
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_VEHICLE_EMPTY_AND_STATIONARY(VEHICLE_INDEX VehicleID, BOOL bConsiderPlayersOnly=FALSE)
	IF NOT IS_ENTITY_DEAD(VehicleID)
	AND (NOT IS_ENTITY_IN_AIR(VehicleID) AND IS_VEHICLE_ON_ALL_WHEELS(VehicleID))
	AND (VMAG(GET_ENTITY_VELOCITY(VehicleID)) < 0.5)
	AND IS_VEHICLE_EMPTY(VehicleID, bConsiderPlayersOnly, bConsiderPlayersOnly)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(VEHICLE_INDEX VehicleID, BLIP_INDEX BlipID, BOOL bConsiderPlayersOnly=FALSE)
	BLIP_SPRITE BlipSprite
	BlipSprite = GET_BLIP_SPRITE(BlipID)
	IF IS_VEHICLE_EMPTY_AND_STATIONARY(VehicleID, bConsiderPlayersOnly)
		IF NOT (BlipSprite = GET_EMPTY_VERSION_OF_BLIP_SPRITE(BlipSprite))
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Vehicle occupancy changed to EMPTY. Updating blip sprite.")
			SET_BLIP_SPRITE(BlipID, GET_EMPTY_VERSION_OF_BLIP_SPRITE(BlipSprite))
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION]UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY - ", GET_BLIP_SPRITE_DEBUG_STRING(BlipSprite))
			RETURN(TRUE)
		ELIF DOES_CURRENT_MISSION_USE_GANG_BOSS() AND BlipSprite = GET_EMPTY_VERSION_OF_BLIP_SPRITE(BlipSprite)
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION]UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY - Correct empty blip: ", GET_BLIP_SPRITE_DEBUG_STRING(BlipSprite))
			RETURN TRUE
		ENDIF 
	ELSE
		IF NOT (BlipSprite = GET_CCCUPIED_VERSION_OF_BLIP_SPRITE(BlipSprite))
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Vehicle occupancy changed to OCCUPIED. Updating blip sprite.")
			SET_BLIP_SPRITE(BlipID, GET_CCCUPIED_VERSION_OF_BLIP_SPRITE(BlipSprite))
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION]UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY - ", GET_BLIP_SPRITE_DEBUG_STRING(BlipSprite))
			RETURN(TRUE)
		ELIF DOES_CURRENT_MISSION_USE_GANG_BOSS() AND BlipSprite = GET_CCCUPIED_VERSION_OF_BLIP_SPRITE(BlipSprite)
			CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION]UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY - Correct occupied blip: ", GET_BLIP_SPRITE_DEBUG_STRING(BlipSprite))
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL SHOULD_BLIP_ONLY_SHOW_LEADERS_WHEN_IN_VEHICLE(BLIP_INDEX BlipID)
	RETURN SHOULD_BLIP_SPRITE_ONLY_SHOW_LEADERS_WHEN_IN_VEHICLE(GET_BLIP_SPRITE(BlipID))
ENDFUNC


FUNC BOOL SHOULD_BLIP_ATLER_DEPENDING_ON_OCCUPANCY(BLIP_INDEX BlipID)
	RETURN SHOULD_BLIP_SPRITE_ATLER_DEPENDING_ON_OCCUPANCY(GET_BLIP_SPRITE(BlipID))
ENDFUNC

FUNC BLIP_SPRITE GET_CORRECT_BLIP_SPRITE_FOR_PED_IN_VEHICLE(PED_INDEX PedID)
	VEHICLE_INDEX VehicleID
	VehicleID = GET_VEHICLE_PED_IS_IN(PedID)
	IF DOES_ENTITY_EXIST(VehicleID)
		RETURN GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(VehicleID), NOT IS_PED_A_PLAYER(PedID))	
	ENDIF
	SCRIPT_ASSERT("GET_CORRECT_BLIP_SPRITE_FOR_PED_IN_VEHICLE - vehicle doesn't exist")
	RETURN GET_STANDARD_BLIP_ENUM_ID()
ENDFUNC

FUNC BOOL IS_PED_LEADER_OF_VEHICLE(PED_INDEX PedID, VEHICLE_INDEX VehicleID, BOOL bConsiderPlayerPedsOnly=FALSE, BOOL bIgnoreNoLongerNeededPeds = FALSE)
	INT iNoOfSeats
	INT iSeat
	VEHICLE_SEAT eSeat
	iNoOfSeats = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(GET_ENTITY_MODEL(VehicleID))
	PED_INDEX PassengerPedID

	
	// first contraband player is leader - fix for 2836802
	REPEAT iNoOfSeats iSeat
		eSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat-1)
		IF NOT IS_VEHICLE_SEAT_FREE(VehicleID, eSeat )
			PassengerPedID = GET_PED_IN_VEHICLE_SEAT(VehicleID, eSeat) 		
			IF IS_PED_A_PLAYER(PassengerPedID)
				PLAYER_INDEX PassengerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(PassengerPedID)
				IF GB_IS_PLAYER_SHIPMENT(PassengerPlayerID)
				OR IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(PassengerPlayerID)
					IF (PassengerPedID = PedID)
						RETURN(TRUE)
					ELSE
						RETURN(FALSE)
					ENDIF		
				ENDIF
			ENDIF			
		ENDIF
	ENDREPEAT
	
	REPEAT iNoOfSeats iSeat
		eSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat-1)
		// the first non free seat is the 'leader'
		IF NOT IS_VEHICLE_SEAT_FREE(VehicleID, eSeat )
			PassengerPedID = GET_PED_IN_VEHICLE_SEAT(VehicleID, eSeat) 		
			IF IS_PED_A_PLAYER(PassengerPedID)
			OR NOT (bConsiderPlayerPedsOnly)
				IF bIgnoreNoLongerNeededPeds
				AND NOT IS_ENTITY_A_MISSION_ENTITY(PassengerPedID)
					PRINTLN("IS_PED_LEADER_OF_VEHICLE - Ignoring passenger in seat ", iSeat, " because they're no longer needed")
					RELOOP
				ENDIF
				
				IF (PassengerPedID = PedID)
					RETURN(TRUE)
				ELSE
					RETURN(FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_PLAYER_USING_FIXED_TEAM_COLOURS_FOR_MISSION(PLAYER_INDEX PlayerID)
	INT iTeam 
	iTeam = GET_PLAYER_TEAM(PlayerID)
	IF (iTeam > -1)
	AND iTeam < FMMC_MAX_TEAMS
		IF (g_FMMC_STRUCT.iTeamColourOverride[iTeam] > -1)
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC



// *******************************************************************************************************************************************************************
// *******************************************************************************************************************************************************************

PROC UPDATE_INDICATORS_ON_PLAYERS_BLIP(PLAYER_INDEX PlayerID)
	
	INT iRed, iGreen, iBlue, iAlpha
	GAMER_HANDLE GamerHandle	
	
	INT iPlayerIDInt = NATIVE_TO_INT(PlayerID)
	
	BOOL bSetToCrewColour = FALSE
	BOOL bSetToHudColour = FALSE
	BOOL bSetToFriendColour = FALSE
	
	// reset - cleanup if coming back from a manually rotated blip
	#IF IS_DEBUG_BUILD
		IF NOT (g_PlayerBlipsData.bDisableResetSecondaryData)
	#ENDIF
	IF IS_BIT_SET(g_PlayerBlipsData.bs_DoResetForSecondaryData, iPlayerIDInt)
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisableShowOutline)
			#ENDIF
			SHOW_OUTLINE_INDICATOR_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], FALSE)
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisableShowCrew)
			#ENDIF
			SHOW_CREW_INDICATOR_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], FALSE)
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisableShowFriend)
			#ENDIF
			SHOW_FRIEND_INDICATOR_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], FALSE)
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisableHeadingIndicator)
			#ENDIF
			SHOW_HEADING_INDICATOR_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], FALSE)
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
		ENDIF
		CLEAR_BIT(g_PlayerBlipsData.bs_BlipOutlineActive, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PlayerIndicatorSetToCrewColour, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PlayerIndicatorSetToFriend, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PlayerIndicatorOn, iPlayerIDInt)	
		CLEAR_BIT(g_PlayerBlipsData.bs_DoResetForSecondaryData, iPlayerIDInt)	
		CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - reset done for player player ", iPlayerIDInt)
	ENDIF
	#IF IS_DEBUG_BUILD
		ENDIF
	#ENDIF
				
	// set the secondary colour of the players blips
	IF CAN_BLIP_HAVE_SECONDARY_CIRCLE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)])
	AND NOT (PlayerID = MPGlobals.LocalPlayerID) // dont add circle to spectated player arrow. 2012893
	AND NOT ARE_SECONDARY_OUTLINES_DISABLED_FOR_PLAYER_BLIP(PlayerID)
	AND NOT SHOULD_SECONDARY_OUTLINES_BE_HIDDEN_FOR_PLAYER_BLIP(PlayerID)

		IF NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(MPGlobals.LocalPlayerID)
		OR (IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(MPGlobals.LocalPlayerID) AND ARE_PLAYERS_ON_SAME_TEAM(PlayerID, MPGlobals.LocalPlayerID))		
			
			// crew and friend circles can't be run on spectators
			IF (MPGlobals.LocalPlayerID = PLAYER_ID())	// the player is not spectating
			
				IF ARE_PLAYERS_ON_SAME_ACTIVE_CLAN(MPGlobals.LocalPlayerID, PlayerID)		
					bSetToCrewColour = TRUE		
				ENDIF			
				
				GamerHandle = GET_GAMER_HANDLE_PLAYER(playerID)					
				IF NETWORK_IS_FRIEND(GamerHandle)			
					bSetToFriendColour = TRUE
				ENDIF	
			
			ENDIF
			
			IF NOT (bSetToCrewColour)
			AND NOT (bSetToFriendColour)
				IF ((IS_PLAYER_ON_MISSION(MPGlobals.LocalPlayerID)) AND ((g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CONTACT) OR (g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_MISSION_VS) OR (g_FMMC_STRUCT.iMissionType =  FMMC_TYPE_MISSION)))					
				AND NOT IS_PLAYER_USING_FIXED_TEAM_COLOURS_FOR_MISSION(PlayerID)
					bSetToHudColour = TRUE
				ENDIF
			ENDIF

		ELSE	
		
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
				
					IF IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(MPGlobals.LocalPlayerID)
						CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK ", iPlayerIDInt, " TRUE")
					ELSE
						CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK ", iPlayerIDInt, " FALSE")
					ENDIF
					
					IF ARE_PLAYERS_ON_SAME_TEAM(PlayerID, MPGlobals.LocalPlayerID)
						CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - ARE_PLAYERS_ON_SAME_TEAM ", iPlayerIDInt, " TRUE")
					ELSE
						CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - ARE_PLAYERS_ON_SAME_TEAM ", iPlayerIDInt, " FALSE")
					ENDIF

				ENDIF
			#ENDIF
		
		
			IF (IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(MPGlobals.LocalPlayerID)
				AND (IS_PLAYER_ON_MISSION(MPGlobals.LocalPlayerID) OR IS_PLAYER_ON_ANY_DEATHMATCH(MPGlobals.LocalPlayerID)))
			AND NOT ARE_PLAYERS_ON_SAME_TEAM(PlayerID, MPGlobals.LocalPlayerID)		
			AND NOT IS_PLAYER_USING_FIXED_TEAM_COLOURS_FOR_MISSION(PlayerID)
				bSetToHudColour = TRUE
			ENDIF
		ENDIF
		
	ELSE
	
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
			
				IF CAN_BLIP_HAVE_SECONDARY_CIRCLE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)])
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - CAN_BLIP_HAVE_SECONDARY_CIRCLE ", iPlayerIDInt, " TRUE")
				ELSE
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - CAN_BLIP_HAVE_SECONDARY_CIRCLE ", iPlayerIDInt, " FALSE")
				ENDIF
				
				IF (PlayerID = MPGlobals.LocalPlayerID)
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - PlayerID = MPGlobals.LocalPlayerID ", iPlayerIDInt, " TRUE")
				ELSE
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - PlayerID = MPGlobals.LocalPlayerID ", iPlayerIDInt, " FALSE")
				ENDIF
				
				IF ARE_SECONDARY_OUTLINES_DISABLED_FOR_PLAYER_BLIP(PlayerID)
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - ARE_SECONDARY_OUTLINES_DISABLED_FOR_PLAYER_BLIP ", iPlayerIDInt, " TRUE")
				ELSE
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - ARE_SECONDARY_OUTLINES_DISABLED_FOR_PLAYER_BLIP ", iPlayerIDInt, " FALSE")
				ENDIF
				
				IF SHOULD_SECONDARY_OUTLINES_BE_HIDDEN_FOR_PLAYER_BLIP(PlayerID)
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - SHOULD_SECONDARY_OUTLINES_BE_HIDDEN_FOR_PLAYER_BLIP ", iPlayerIDInt, " TRUE")
				ELSE
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - SHOULD_SECONDARY_OUTLINES_BE_HIDDEN_FOR_PLAYER_BLIP ", iPlayerIDInt, " FALSE")				
				ENDIF
				
			ENDIF
		#ENDIF
	
	ENDIF



	IF (bSetToHudColour)
		GET_HUD_COLOUR(GET_PLAYER_HUD_COLOUR(PlayerID),iRed, iGreen, iBlue, iAlpha)
		IF NOT IS_OUTLINE_BLIP_ACTIVE_FOR_PLAYER(iPlayerIDInt)
			SET_OUTLINE_BLIP_COLOUR_FOR_PLAYER(iPlayerIDInt, iRed, iGreen, iBlue)	
			CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - bSetToHudColour = TRUE - PlayerID = ", iPlayerIDInt)
		ELSE
			IF NOT DOES_MATCH_SECONDARY_BLIP_COLOUR(iPlayerIDInt, iRed, iGreen, iBlue)
				CLEAR_OUTLINE_BLIP_COLOUR_FOR_PLAYER(iPlayerIDInt)				
				CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - hud colour mismatch - PlayerID = ", iPlayerIDInt)
				EXIT
			ENDIF
		ENDIF
	ELSE
		IF IS_OUTLINE_BLIP_ACTIVE_FOR_PLAYER(iPlayerIDInt)
			CLEAR_OUTLINE_BLIP_COLOUR_FOR_PLAYER(iPlayerIDInt)
			CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - bSetToHudColour = FALSE - PlayerID = ", iPlayerIDInt)
			EXIT
		ENDIF
	ENDIF	
		
	// crew and friend blips only appear if the blip does not have an outline
	IF NOT IS_OUTLINE_BLIP_ACTIVE_FOR_PLAYER(iPlayerIDInt)
		
		IF (bSetToCrewColour)
			GET_PLAYER_CREW_COLOUR(iRed, iGreen, iBlue)
			IF NOT IS_CREW_BLIP_ACTIVE_FOR_PLAYER(iPlayerIDInt)			
				SET_CREW_BLIP_COLOUR_FOR_PLAYER(iPlayerIDInt, iRed, iGreen, iBlue)	
				CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - bSetToCrewColour = TRUE - PlayerID = ", iPlayerIDInt)
			ELSE
				IF NOT DOES_MATCH_SECONDARY_BLIP_COLOUR(iPlayerIDInt, iRed, iGreen, iBlue)
					CLEAR_CREW_BLIP_COLOUR_FOR_PLAYER(iPlayerIDInt)
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - crew colour mismatch - PlayerID = ", iPlayerIDInt)
					EXIT
				ENDIF
			ENDIF
		ELSE
			IF IS_CREW_BLIP_ACTIVE_FOR_PLAYER(iPlayerIDInt)	
				CLEAR_CREW_BLIP_COLOUR_FOR_PLAYER(iPlayerIDInt)
				CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - bSetToCrewColour = FALSE - PlayerID = ", iPlayerIDInt)
				EXIT
			ENDIF
		ENDIF
		
		
		IF (bSetToFriendColour)
			IF NOT IS_FRIEND_BLIP_ACTIVE_FOR_PLAYER(iPlayerIDInt)
				SET_FRIEND_BLIP_ACTIVE_FOR_PLAYER(iPlayerIDInt, TRUE)
				CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - bSetToFriendColour = TRUE - PlayerID = ",iPlayerIDInt)
			ENDIF
		ELSE 
			IF IS_FRIEND_BLIP_ACTIVE_FOR_PLAYER(iPlayerIDInt)
				SET_FRIEND_BLIP_ACTIVE_FOR_PLAYER(iPlayerIDInt, FALSE)
				CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_INDICATORS_ON_PLAYERS_BLIP - bSetToFriendColour = FALSE - PlayerID = ", iPlayerIDInt)
				EXIT
			ENDIF
		ENDIF
		
	ENDIF
		
		
	IF (IS_PLAYER_IN_PROPERTY(PlayerID, TRUE) AND NOT ARE_PLAYERS_IN_SAME_PROPERTY(PlayerID, MPGlobals.LocalPlayerID, TRUE)	)
	OR (MPGlobals.LocalPlayerID = PlayerID) // this is the player being spectated
	OR (DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) AND NOT CAN_BLIP_HAVE_HEADING_INDICATOR(g_PlayerBlipsData.playerBlips[iPlayerIDInt]))	
	OR IS_BIT_SET(g_PlayerBlipsData.bs_PlayerUsingIEWarehouseModShop, NATIVE_TO_INT(PlayerID))
	OR (IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(PlayerID) AND NOT ARE_PLAYER_IN_SAME_CUSTOM_DISPLACED_INTERIOR_INSTANCE(PlayerID, MPGlobals.LocalPlayerID))
		SET_HEADING_INDICATOR_FOR_PLAYER(iPlayerIDInt, FALSE)
	ELSE
		SET_HEADING_INDICATOR_FOR_PLAYER(iPlayerIDInt, TRUE)
	ENDIF
	
	// should we display height indicator?
	#IF IS_DEBUG_BUILD 
		IF NOT (g_PlayerBlipsData.bDisableHeightIndicators)
	#ENDIF
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
		SET_PLAYER_BLIP_HEIGHT_INDICATOR(PlayerID)
	ENDIF
	#IF IS_DEBUG_BUILD 
		ENDIF
	#ENDIF
		
	
ENDPROC

FUNC BOOL IS_ARROW_STROKE_DISABLED_BY_MISSION_CONTROLLER()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciTURN_OFF_BLIP_ALERT_SYSTEM)
ENDFUNC

FUNC BOOL IS_SONAR_DISABLED_BY_MISSION_CONTROLLER()
	IF IS_PLAYER_USING_ARENA()
		RETURN TRUE
	ENDIF	
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciENABLE_SONAR_BLIPS)
ENDFUNC

FUNC BOOL IS_RADAR_FLASH_DISABLED_BY_MISSION_CONTROLLER()
//Disable if playing Cops And Crooks - bug url:bugstar:6227610 & url:bugstar:6242279
#IF FEATURE_FREEMODE_ARCADE 
#IF FEATURE_COPS_N_CROOKS
	IF IS_FREEMODE_ARCADE()
		IF GET_ARCADE_MODE() = ARC_COPS_CROOKS			
			RETURN(TRUE)
		ENDIF
	ENDIF
#ENDIF
#ENDIF
	IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
	OR IS_PLAYER_USING_ARENA()
		RETURN TRUE
	ENDIF
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciENABLE_RADAR_FLASH)
ENDFUNC


PROC ADD_STROKE_ARROW_FOR_PLAYER(PLAYER_INDEX PlayerID, BOOL bUseCoords=FALSE, HUD_COLOURS HudColour = HUD_COLOUR_RED)

	IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)
		IF NOT (g_PlayerBlipsData.ArrowStrokePlayerID = PlayerID)
			REMOVE_BLIP(g_PlayerBlipsData.ArrowStrokeBlipID)
			NET_PRINT("ADD_STROKE_ARROW_FOR_PLAYER - stroke blip was for another player, removing.") NET_NL()
		ELIF (PlayerID = PLAYER_ID())
			IF (GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.ArrowStrokeBlipID) = BLIPTYPE_COORDS)
			OR NOT (GET_PED_INDEX_FROM_ENTITY_INDEX(GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.ArrowStrokeBlipID)) = PLAYER_PED_ID())
				REMOVE_BLIP(g_PlayerBlipsData.ArrowStrokeBlipID)
				NET_PRINT("ADD_STROKE_ARROW_FOR_PLAYER - stroke blip not matching local player, removing.") NET_NL()
			ENDIF
		ENDIF
	ENDIF

	// stroke blip needs to be a new blip underneath
	IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)
		IF (bUseCoords)
			g_PlayerBlipsData.ArrowStrokeBlipID  = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID), FALSE))
			SET_BLIP_ROTATION(g_PlayerBlipsData.ArrowStrokeBlipID, GET_PROPER_BLIP_ROTATION_FOR_PLAYER(PlayerID))
		ELSE
			//IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID))
			//	g_PlayerBlipsData.ArrowStrokeBlipID  = ADD_BLIP_FOR_ENTITY(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PlayerID)))
			//	NET_PRINT("ADD_STROKE_ARROW_FOR_PLAYER - adding to vehicle") NET_NL()
			//ELSE
				g_PlayerBlipsData.ArrowStrokeBlipID  = ADD_BLIP_FOR_ENTITY(GET_PLAYER_PED(PlayerID))
				NET_PRINT("ADD_STROKE_ARROW_FOR_PLAYER - adding to ped") NET_NL()
			//ENDIF
		ENDIF
		SET_BLIP_SPRITE(g_PlayerBlipsData.ArrowStrokeBlipID, RADAR_TRACE_CENTRE_STROKE)
		SHOW_HEIGHT_ON_BLIP(g_PlayerBlipsData.ArrowStrokeBlipID, FALSE)	
		SET_BLIP_SCALE(g_PlayerBlipsData.ArrowStrokeBlipID, PLAYER_ARROW_BLIP_SCALE)	
		SET_BLIP_PRIORITY(g_PlayerBlipsData.ArrowStrokeBlipID, INT_TO_ENUM(BLIP_PRIORITY, (ENUM_TO_INT(BLIPPRIORITY_OVER_CENTRE_BLIP)-1)))
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.ArrowStrokeBlipID, HudColour)
		SET_BLIP_HIDDEN_ON_LEGEND(g_PlayerBlipsData.ArrowStrokeBlipID, TRUE)
		g_PlayerBlipsData.ArrowStrokePlayerID = PlayerID
		NET_PRINT("ADD_STROKE_ARROW_FOR_PLAYER - added stroke blip for player ") NET_PRINT_INT(NATIVE_TO_INT(PlayerID)) NET_NL()
	ENDIF	
ENDPROC


//FUNC BOOL IS_STROKE_ATTACHED_TO_WRONG_ENTITY()
//	IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)							
//		IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(MPGlobals.LocalPlayerID))
//			IF (GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.ArrowStrokeBlipID) = BLIPTYPE_CHAR)
//				PRINTLN("[player_blips] IS_STROKE_ATTACHED_TO_WRONG_ENTITY = true. in car. MPGlobals.LocalPlayerID = ", NATIVE_TO_INT(MPGlobals.LocalPlayerID))
//				RETURN TRUE
//			ENDIF
//		ELSE
//			IF (GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.ArrowStrokeBlipID) = BLIPTYPE_VEHICLE)
//				PRINTLN("[player_blips] IS_STROKE_ATTACHED_TO_WRONG_ENTITY = true. on foot. MPGlobals.LocalPlayerID = ", NATIVE_TO_INT(MPGlobals.LocalPlayerID))
//				RETURN TRUE
//			ENDIF
//		ENDIF		
//	ENDIF
//	RETURN FALSE
//ENDFUNC


FUNC VECTOR GetPlayerDisplacedCoords(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_USING_ARENA()
		RETURN <<ARENA_X, ARENA_Y, ARENA_Z>>
	ENDIF	
	
	IF IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(MPGlobals.LocalPlayerID)
		RETURN GlobalplayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].playerBlipData.vDisplacedInteriorPos	
	ELSE
	
		IF NOT IS_PLAYER_IN_DISPLACED_HEIST_APPARTMENT(MPGlobals.LocalPlayerID)
			IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(MPGlobals.LocalPlayerID)
				INT iEntrancePlayerIsUsing = globalPropertyEntryData.iEntrance
				INT iCurrentProperty = GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iCurrentlyInsideProperty										
				IF (iEntrancePlayerIsUsing > -1)
				AND (iCurrentProperty > 0)
					RETURN mpProperties[iCurrentProperty].vBlipLocation[iEntrancePlayerIsUsing]
				ENDIF
			ELSE
				RETURN GET_BLIP_COORD_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(MPGlobals.LocalPlayerID)
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorSubmarine)//GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].g_bInMissionCreatorSubmarine
				RETURN <<HEIST_SUB_X, HEIST_SUB_Y, HEIST_SUB_Z>>
			ELSE
				RETURN <<HEIST_APP_X, HEIST_APP_Y, HEIST_APP_Z>>
			ENDIF
		ENDIF
		
	ENDIF	

	RETURN GET_PLAYER_COORDS(PlayerID)

ENDFUNC

PROC UPDATE_PLAYERS_ARROW()
	IF g_bActiveInGunTurret
	OR IS_LOCAL_PLAYER_USING_VEHICLE_WEAPON()	
		EXIT
	ENDIF
	
	// set colour of players own blip
	BLIP_INDEX PlayerBlipID
	PlayerBlipID = GET_MAIN_PLAYER_BLIP_ID()
	BOOL bRemoveArrowStroke = TRUE
	INT iGangID
	VECTOR vPos
	
	IF DOES_BLIP_EXIST(PlayerBlipID)
	
		IF IS_LOCAL_PLAYER_ARROW_FLASHING()
			HIDE_PLAYER_ARROW_THIS_FRAME()	
		ENDIF
	
		IF (IS_PLAYER_SPECTATING(PLAYER_ID()) OR SHOULD_PLAYER_ARROW_BE_HIDDEN_THIS_FRAME() ) // OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE))
				
			// hide players blip
			IF NOT (GET_BLIP_INFO_ID_DISPLAY(PlayerBlipID) = DISPLAY_NOTHING)	
				SET_BLIP_DISPLAY(PlayerBlipID, DISPLAY_NOTHING)	
			ENDIF
			
			IF NOT (GET_BLIP_ALPHA(PlayerBlipID) = 0)
				SET_BLIP_ALPHA(PlayerBlipID, 0)
			ENDIF
			
			
			// give spectated player arrow blip
			IF IS_PLAYER_SPECTATING(PLAYER_ID())
				IF NOT (NATIVE_TO_INT(MPGlobals.LocalPlayerID) = -1)				
					IF IS_NET_PLAYER_OK(MPGlobals.LocalPlayerID, FALSE, TRUE)
					
						PED_INDEX pedLocal = GET_PLAYER_PED(MPGlobals.LocalPlayerID)
					
						IF DOES_ENTITY_EXIST(pedLocal)
													
							// remove blip if it's not a coord blip
							IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)])
								IF NOT (GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)]) = BLIPTYPE_COORDS)
									REMOVE_BLIP(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)])
									NET_PRINT("UPDATE_PLAYERS_ARROW - removing non-coords blip ")  NET_NL()
								ENDIF
							ENDIF
						
							IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)])
								g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)]  =  ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(pedLocal, FALSE))// ADD_BLIP_FOR_ENTITY(pedLocal)
								IF NOT IS_ENTITY_DEAD(pedLocal)
									SET_BLIP_ROTATION(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], GET_PROPER_BLIP_ROTATION_FOR_PED(pedLocal))
								ENDIF
								SET_BIT(g_PlayerBlipsData.bs_BlipCreatedThisFrame, NATIVE_TO_INT(MPGlobals.LocalPlayerID))
								#IF IS_DEBUG_BUILD
									IF (g_PlayerBlipsData.bDebugBlipOutput)
									CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PLAYERS_ARROW - ADD_BLIP_FOR_ENTITY called from ", GET_THIS_SCRIPT_NAME())		
									ENDIF
								#ENDIF						
							ENDIF
							
							// remove stroke blip if it's not a coord blip
							IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)
								IF NOT (GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.ArrowStrokeBlipID) = BLIPTYPE_COORDS)
									REMOVE_BLIP(g_PlayerBlipsData.ArrowStrokeBlipID)
									NET_PRINT("UPDATE_PLAYERS_ARROW - removing non-coords stroke blip ")  NET_NL()
								ENDIF
							ENDIF
							
							UPDATE_INDICATORS_ON_PLAYERS_BLIP(MPGlobals.LocalPlayerID)
							
							// have we just switched to spectate this player? if so delete any stroke blip
							IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)]) = INT_TO_ENUM(BLIP_SPRITE, 6))
								IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)
									REMOVE_BLIP(g_PlayerBlipsData.ArrowStrokeBlipID)
								ENDIF	
							ENDIF
							
							SET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], INT_TO_ENUM(BLIP_SPRITE, 6))					
							SHOW_HEIGHT_ON_BLIP(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], FALSE)			
							SET_BLIP_SCALE(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], PLAYER_ARROW_BLIP_SCALE)
							//SET_BLIP_COLOUR(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], GET_INT_FROM_HUD_COLOUR(GET_PLAYER_ARROW_BLIP_COLOUR(MPGlobals.LocalPlayerID)))				
							
							SWITCH GET_PLAYER_ARROW_BLIP_COLOUR(MPGlobals.LocalPlayerID)
								CASE HUD_COLOUR_WHITE
									SET_PLAYER_BLIP_COLOUR(MPGlobals.LocalPlayerID, g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)])	
									
									// is player in gang? and with gang then add the stroke
									iGangID = GET_GANG_ID_FOR_PLAYER(MPGlobals.LocalPlayerID)
									IF NOT (iGangID = -1)		
										IF IS_PLAYER_IN_VEHICLE_WITH_GANG_MEMBERS(MPGlobals.LocalPlayerID)
											// stroke blip needs to be a new blip underneath
											ADD_STROKE_ARROW_FOR_PLAYER(MPGlobals.LocalPlayerID, TRUE, GET_HUD_COLOUR_FOR_GANG_ID( iGangID ))	
											bRemoveArrowStroke = FALSE					
										ENDIF
									ENDIF								
									
								BREAK
								CASE HUD_COLOUR_NET_PLAYER32
									// set to blue but add a red stroke
									SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], FRIENDLY_ARROW_COLOUR())
									// stroke blip needs to be a new blip underneath
									IF NOT IS_ARROW_STROKE_DISABLED_BY_MISSION_CONTROLLER()
										ADD_STROKE_ARROW_FOR_PLAYER(MPGlobals.LocalPlayerID, TRUE)	
										bRemoveArrowStroke = FALSE
									ENDIF
								BREAK
								DEFAULT
									SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], GET_PLAYER_ARROW_BLIP_COLOUR(MPGlobals.LocalPlayerID))
								BREAK
							ENDSWITCH
							SET_BLIP_PRIORITY(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], BLIPPRIORITY_OVER_CENTRE_BLIP)

							// update position and heading																					
							IF NOT IS_ENTITY_DEAD(pedLocal)
								SET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], GET_ENTITY_COORDS(pedLocal, FALSE))
								SET_BLIP_ROTATION(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], GET_PROPER_BLIP_ROTATION_FOR_PED(pedLocal))								
								IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)							
									SET_BLIP_COORDS(g_PlayerBlipsData.ArrowStrokeBlipID, GET_ENTITY_COORDS(pedLocal, FALSE))
									SET_BLIP_ROTATION(g_PlayerBlipsData.ArrowStrokeBlipID, GET_PROPER_BLIP_ROTATION_FOR_PED(pedLocal))								
								ENDIF
							ENDIF
							
							
							//SET_BLIP_NAME_TO_PLAYER_NAME(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], MPGlobals.LocalPlayerID)
							SET_PLAYER_BLIP_NAME_CORRECTLY(MPGlobals.LocalPlayerID)	
														
							// is player highlighted? 
							IF IS_PLAYER_ON_PAUSE_MENU(MPGlobals.LocalPlayerID)				
							
								IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].playerBlipData.iFlags, PBBD_IS_SELF_HIGHLIGHTED)
								
									IF NOT (g_PlayerBlipsData.b_ArrowBlipHighlighted)
										IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.ArrowHighlightStartTime)) > 500
											g_PlayerBlipsData.b_ArrowBlipHighlighted = TRUE	
											g_PlayerBlipsData.ArrowHighlightStartTime = GET_NETWORK_TIME()						
										ELSE
											IF NOT (GET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)]) = 0)
												SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], 0)
											ENDIF
										ENDIF
									ELSE
										IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.ArrowHighlightStartTime)) > 500
											g_PlayerBlipsData.b_ArrowBlipHighlighted = FALSE	
											g_PlayerBlipsData.ArrowHighlightStartTime = GET_NETWORK_TIME()						
										ELSE
											IF NOT (GET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)]) = 255)
												SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], 255)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NOT (GET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)]) = 255)
										SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)], 255)	
									ENDIF
								ENDIF
								
								// arrow stroke should not be shown on front end.
								IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)
									IF NOT (GET_BLIP_ALPHA(g_PlayerBlipsData.ArrowStrokeBlipID) = 0)
										SET_BLIP_ALPHA(g_PlayerBlipsData.ArrowStrokeBlipID, 0)
									ENDIF
								ENDIF
								
							ELSE
								
								// arrow stroke alpha should match the main arrow alpha
								IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)
									IF NOT (GET_BLIP_ALPHA(g_PlayerBlipsData.ArrowStrokeBlipID) = GET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)]))								
										SET_BLIP_ALPHA(g_PlayerBlipsData.ArrowStrokeBlipID, GET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)]))										
									ENDIF
								ENDIF
								
							ENDIF
							

							#IF IS_DEBUG_BUILD
								IF (g_PlayerBlipsData.bDebugBlipOutput)
									CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PLAYERS_ARROW - called! - MPGlobals.LocalPlayerID = ", NATIVE_TO_INT(MPGlobals.LocalPlayerID))		
									CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PLAYERS_ARROW - called! - arrow colour = ", GET_BLIP_COLOUR(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)]))
								ENDIF
							#ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			ENDIF
			
		ELSE
		
			// Make sure player's blip is shown					
			//SET_BLIP_COLOUR(PlayerBlipID, GET_INT_FROM_HUD_COLOUR(GET_PLAYER_ARROW_BLIP_COLOUR(PLAYER_ID())))		
			
			// test stroke
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bTestBlipStroke)
					ADD_STROKE_ARROW_FOR_PLAYER(PLAYER_ID())	
					bRemoveArrowStroke = FALSE		
				ENDIF
			#ENDIF
			
			
						
			IF IS_LOCAL_PLAYER_ARROW_SET_AS_CURRENT_HUD_COLOUR()
			AND NOT IS_PLAYER_ARROW_FLASHING_RED(PLAYER_ID())
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(PlayerBlipID, GET_PLAYER_HUD_COLOUR(PLAYER_ID()))
			ELSE			
				IF (GET_PLAYER_ARROW_BLIP_COLOUR(PLAYER_ID()) = HUD_COLOUR_WHITE)
					SET_PLAYER_BLIP_COLOUR(PLAYER_ID(), PlayerBlipID)									
					// is player in gang? and with gang then add the stroke
					iGangID = GET_GANG_ID_FOR_PLAYER(PLAYER_ID())
					IF NOT (iGangID = -1)		
						IF IS_PLAYER_IN_VEHICLE_WITH_GANG_MEMBERS(PLAYER_ID())
							// stroke blip needs to be a new blip underneath
							ADD_STROKE_ARROW_FOR_PLAYER(PLAYER_ID(), FALSE, GET_HUD_COLOUR_FOR_GANG_ID( iGangID ))	
							bRemoveArrowStroke = FALSE					
						ENDIF
					ENDIF
				ELSE		
					HUD_COLOURS CurrentColour
					CurrentColour = GET_BLIP_HUD_COLOUR(PlayerBlipID)
					IF (CurrentColour = HUD_COLOUR_PURE_WHITE)
						CurrentColour = HUD_COLOUR_WHITE
					ENDIF
											
					IF (GET_PLAYER_ARROW_BLIP_COLOUR(PLAYER_ID()) = HUD_COLOUR_NET_PLAYER32)
						IF NOT (GET_PLAYER_ARROW_BLIP_COLOUR(PLAYER_ID()) =  FRIENDLY_ARROW_COLOUR())			
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(PlayerBlipID,  FRIENDLY_ARROW_COLOUR())
						ENDIF
						// stroke blip needs to be a new blip underneath
						IF NOT IS_ARROW_STROKE_DISABLED_BY_MISSION_CONTROLLER()
							ADD_STROKE_ARROW_FOR_PLAYER(PLAYER_ID())	
							bRemoveArrowStroke = FALSE
						ENDIF
					ELSE
						IF NOT (GET_PLAYER_ARROW_BLIP_COLOUR(PLAYER_ID()) = CurrentColour)		
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(PlayerBlipID, GET_PLAYER_ARROW_BLIP_COLOUR(PLAYER_ID()))
						ENDIF
					ENDIF					
				ENDIF				
			ENDIF
			
			IF NOT (GET_BLIP_INFO_ID_DISPLAY(PlayerBlipID) = DISPLAY_BOTH)			
				SET_BLIP_DISPLAY(PlayerBlipID, DISPLAY_BOTH)
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)
					SET_BLIP_DISPLAY(g_PlayerBlipsData.ArrowStrokeBlipID, DISPLAY_BOTH)
				ENDIF
			ENDIF			


			// is player highlighted? 
			IF NOT IS_PLAYER_ON_PAUSE_MENU(PLAYER_ID())
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_SELF_HIGHLIGHTED)
					IF NOT (g_PlayerBlipsData.b_ArrowBlipHighlighted)
						IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.ArrowHighlightStartTime)) > 500
							g_PlayerBlipsData.b_ArrowBlipHighlighted = TRUE	
							g_PlayerBlipsData.ArrowHighlightStartTime = GET_NETWORK_TIME()						
						ELSE
							IF NOT (GET_BLIP_ALPHA(PlayerBlipID) = 0)
								SET_BLIP_ALPHA(PlayerBlipID, 0)
							ENDIF
						ENDIF
					ELSE
						IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.ArrowHighlightStartTime)) > 500
							g_PlayerBlipsData.b_ArrowBlipHighlighted = FALSE	
							g_PlayerBlipsData.ArrowHighlightStartTime = GET_NETWORK_TIME()						
						ELSE
							IF NOT (GET_BLIP_ALPHA(PlayerBlipID) = 255)
							
								// If code is fading this blip out don't manage its alpha.
								//IF NOT IS_PLAYER_BLIP_ALPHA_CONTROLLED_BY_NATIVE( NATIVE_TO_INT(PLAYER_ID()))
								IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_FadeOutBlip, NATIVE_TO_INT(PLAYER_ID()))
									SET_BLIP_ALPHA(PlayerBlipID, 255)
								ENDIF
								
							ENDIF
						ENDIF	
					ENDIF
				ELSE
					IF NOT (GET_BLIP_ALPHA(PlayerBlipID) = 255)
					
						// If code is fading this blip out don't manage its alpha.
						//IF NOT IS_PLAYER_BLIP_ALPHA_CONTROLLED_BY_NATIVE(NATIVE_TO_INT(PLAYER_ID()))
						IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_FadeOutBlip, NATIVE_TO_INT(PLAYER_ID()))
							SET_BLIP_ALPHA(PlayerBlipID, 255)	
						ENDIF	
						
					ENDIF
				ENDIF
				
				// arrow stroke alpha should match the main arrow alpha
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)
					IF NOT (GET_BLIP_ALPHA(g_PlayerBlipsData.ArrowStrokeBlipID) = GET_BLIP_ALPHA(PlayerBlipID))								
						SET_BLIP_ALPHA(g_PlayerBlipsData.ArrowStrokeBlipID, GET_BLIP_ALPHA(PlayerBlipID))										
					ENDIF
				ENDIF				
				
			ELSE
			
				// arrow stroke should not be shown on front end.
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)
					IF NOT (GET_BLIP_ALPHA(g_PlayerBlipsData.ArrowStrokeBlipID) = 0)
						SET_BLIP_ALPHA(g_PlayerBlipsData.ArrowStrokeBlipID, 0)
					ENDIF
				ENDIF

			ENDIF
			
			// update rotations to match
			IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())				
					INT iBlipRotation = GET_PROPER_BLIP_ROTATION_FOR_PLAYER(PLAYER_ID())
					SET_BLIP_ROTATION(PlayerBlipID, iBlipRotation)
					SET_BLIP_ROTATION(g_PlayerBlipsData.ArrowStrokeBlipID, iBlipRotation)
				ENDIF
			ENDIF		
						
		ENDIF
		
	ENDIF

	
	IF IS_PLAYER_IN_DISPLACED_PROPERTY(MPGlobals.LocalPlayerID)
		
		IF g_bActiveInGunTurret = FALSE
		AND NOT IsUsingSpecialWeaponCamera()
			IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
								
				vPos = GetPlayerDisplacedCoords(MPGlobals.LocalPlayerID)
				
				SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(vPos.x,vPos.y)
				LOCK_MINIMAP_POSITION(vPos.x,vPos.y)
				g_PlayerBlipsData.bMiniMapPositionLocked = TRUE
				
				HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
				
				bRemoveArrowStroke = TRUE
				
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME - vPos = ", vPos)	
					ENDIF
				#ENDIF
				
			ELSE
				IF (g_PlayerBlipsData.bMiniMapPositionLocked)
					UNLOCK_MINIMAP_POSITION()
					g_PlayerBlipsData.bMiniMapPositionLocked = FALSE
				ENDIF
				
				HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
			ENDIF
		ENDIF
	
	ELSE
		IF (g_PlayerBlipsData.bMiniMapPositionLocked)
			UNLOCK_MINIMAP_POSITION()
			g_PlayerBlipsData.bMiniMapPositionLocked = FALSE
		ENDIF			
	ENDIF
	
	// make sure the arrow stroke gets removed
	IF (bRemoveArrowStroke)
	OR SHOULD_PLAYER_ARROW_BE_HIDDEN_THIS_FRAME()
	OR (NOT IS_ENTITY_DEAD(GET_PLAYER_PED(MPGlobals.LocalPlayerID)) AND IS_PED_RAGDOLL(GET_PLAYER_PED(MPGlobals.LocalPlayerID)))
	//OR IS_STROKE_ATTACHED_TO_WRONG_ENTITY()
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.ArrowStrokeBlipID)
			REMOVE_BLIP(g_PlayerBlipsData.ArrowStrokeBlipID)
		ENDIF	
	ENDIF
	
ENDPROC



FUNC BOOL IsCopInMyWantedRadius(PLAYER_INDEX CopID)
	RETURN(VDIST(GET_PLAYER_COORDS(MPGlobals.LocalPlayerID), GET_PLAYER_COORDS(CopID)) <= GET_WANTED_LEVEL_RADIUS( GET_PLAYER_WANTED_LEVEL( MPGlobals.LocalPlayerID)))
ENDFUNC

FUNC BOOL IS_PLAYERS_TEAM_HIGHLIGHTED(PLAYER_INDEX playerID)

	IF (GET_PLAYER_TEAM(PlayerID) > -1)
		IF IS_BIT_SET(g_FMMC_STRUCT.iTargetTeamBlipBitset, GET_PLAYER_TEAM(PlayerID))	
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

#IF FEATURE_FREEMODE_ARCADE
FUNC BOOL SHOULD_BLIP_PLAYER_FREEMODE_ARCADE(PLAYER_INDEX playerID)
	
	// Don't try and blip players that don't exist and are not playing.
	IF IS_NET_PLAYER_OK(playerID)
	
		// show all flag is set
		IF (g_PlayerBlipsData.bShowAllPlayerBlips)
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_SHOW_ALL)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_SHOW_ALL)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF	
			RETURN(TRUE)
		ENDIF
		
		// on same team
		IF ARE_PLAYERS_ON_SAME_TEAM(playerID, MPGlobals.LocalPlayerID)
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_SAME_TEAM)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_SAME_TEAM)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF		
			RETURN (TRUE)
		ENDIF
		
		// on a friendly team
		IF DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(PlayerID), GET_PLAYER_TEAM(MPGlobals.LocalPlayerID))
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_TEAMS_ARE_FRIENDLY)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_TEAMS_ARE_FRIENDLY)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF		
			RETURN (TRUE)
		ENDIF

		// if that player's team is highlighted?
		IF IS_PLAYERS_TEAM_HIGHLIGHTED(PlayerID)
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_TEAM_IS_HIGHLIGHTED)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_TEAM_IS_HIGHLIGHTED)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF		
			RETURN(TRUE)
		ENDIF

		
		IF IS_PLAYER_NOTICABLE(playerID, NOTICABLE_TIME)
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_NOTICABLE)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_NOTICABLE)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF		
			RETURN(TRUE)
		#IF IS_DEBUG_BUILD
		ELSE
			IF NOT (NATIVE_TO_INT(PlayerID) = -1)
				IF (MPGlobals.LocalPlayerID = PLAYER_ID())
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)] = 0
				ENDIF
			ENDIF							
		#ENDIF
		ENDIF	
		
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

#ENDIF

FUNC BOOL SHOULD_BLIP_PLAYER_DM(PLAYER_INDEX playerID)
	
	// Don't try and blip players that don't exist and are not playing.
	IF IS_NET_PLAYER_OK(playerID)
	
		// show all flag is set
		IF (g_PlayerBlipsData.bShowAllPlayerBlips)
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_SHOW_ALL)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_SHOW_ALL)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF	
			RETURN(TRUE)
		ENDIF
		
		// on same team
		IF ARE_PLAYERS_ON_SAME_TEAM(playerID, MPGlobals.LocalPlayerID)
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_SAME_TEAM)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_SAME_TEAM)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF		
			RETURN (TRUE)
		ENDIF
		
		// on a friendly team
		IF DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(PlayerID), GET_PLAYER_TEAM(MPGlobals.LocalPlayerID))
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_TEAMS_ARE_FRIENDLY)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_TEAMS_ARE_FRIENDLY)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF		
			RETURN (TRUE)
		ENDIF

		// if that player's team is highlighted?
		IF IS_PLAYERS_TEAM_HIGHLIGHTED(PlayerID)
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_TEAM_IS_HIGHLIGHTED)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_TEAM_IS_HIGHLIGHTED)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF		
			RETURN(TRUE)
		ENDIF

		
		IF IS_PLAYER_NOTICABLE(playerID, NOTICABLE_TIME)
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					IF (MPGlobals.LocalPlayerID = PLAYER_ID())
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_NOTICABLE)
							SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_NOTICABLE)
						ENDIF
					ENDIF
				ENDIF
			#ENDIF		
			RETURN(TRUE)
		#IF IS_DEBUG_BUILD
		ELSE
			IF NOT (NATIVE_TO_INT(PlayerID) = -1)
				IF (MPGlobals.LocalPlayerID = PLAYER_ID())
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PlayerID)] = 0
				ENDIF
			ENDIF							
		#ENDIF
		ENDIF	
		
		
		IF (MPGlobals.LocalPlayerID = PLAYER_ID())
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_GB_BOSS_DEATHMATCH
				IF GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].iCurrentMissionType = FMMC_TYPE_GB_BOSS_DEATHMATCH
					IF NOT ARE_PLAYERS_ON_SAME_TEAM(playerID, MPGlobals.LocalPlayerID)
					//	IF (GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(playerID), GET_PLAYER_COORDS(PlayerID) > 200.0)
							#IF IS_DEBUG_BUILD
							IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_IMPROMPTU)
								SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_IMPROMPTU)
							ENDIF
							#ENDIF
							RETURN(TRUE)
					//	ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		 // FEATURE_GANG_BOSS
		
		//Temp Beast Mode fix
		IF NOT (NATIVE_TO_INT(PlayerID) = -1)
			IF GlobalplayerBD[NATIVE_TO_INT(playerID)].bForceBeastBlip = TRUE
				RETURN (TRUE)
			ENDIF
		ENDIF
	
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL SHOULD_BLIP_PLAYER_FM(PLAYER_INDEX playerID)
		
	// Don't try and blip players that don't exist and are not playing.
	// Otherwise, we generally always show players blipped during races.
	IF IS_NET_PLAYER_OK(playerID)

		
		#IF IS_DEBUG_BUILD
			IF NOT (NATIVE_TO_INT(PlayerID) = -1)
				IF (MPGlobals.LocalPlayerID = PLAYER_ID())
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_DEFAULT_ON)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_DEFAULT_ON)
					ENDIF
				ENDIF
			ENDIF
		#ENDIF	
		RETURN(TRUE)
		
		
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL SHOULD_BLIP_PLAYER_RACE(PLAYER_INDEX playerID)
		
	// Don't try and blip players that don't exist and are not playing.
	// Otherwise, we generally always show players blipped during races.
	IF IS_NET_PLAYER_OK(playerID)
		
		#IF IS_DEBUG_BUILD
			IF NOT (NATIVE_TO_INT(PlayerID) = -1)
				IF (MPGlobals.LocalPlayerID = PLAYER_ID())
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_DEFAULT_ON)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_DEFAULT_ON)
					ENDIF
				ENDIF
			ENDIF
		#ENDIF	
		RETURN(TRUE)
		
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

PROC HIGHTLIGHT_BLIP_FOR_PLAYER(PLAYER_INDEX PlayerID, BOOL bHighlightBlip, INT iTimeToHighlight = HIGHLIGHT_BLIP_TIME)

	IF (PlayerID = INVALID_PLAYER_INDEX())
		SCRIPT_ASSERT("HIGHTLIGHT_BLIP_FOR_PLAYER - passed in invalid player index.")
		EXIT
	ENDIF

	INT iPlayerIDInt = NATIVE_TO_INT(PlayerID)
	IF (bHighlightBlip)
		CPRINTLN(DEBUG_BLIP, "[player_blips] HIGHTLIGHT_BLIP_FOR_PLAYER - called with true iPlayerIDInt = ", iPlayerIDInt)
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			CPRINTLN(DEBUG_BLIP, "[player_blips] HIGHTLIGHT_BLIP_FOR_PLAYER - setting blip")
			SET_BIT(g_PlayerBlipsData.bs_PlayerBlipHighlighted, iPlayerIDInt)	
			CLEAR_BIT(g_PlayerBlipsData.bs_HighlightingAlways, iPlayerIDInt)
			SET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PlayerID)
			// when to stop the blip flashing. 
			IF (iTimeToHighlight < 0)
				//g_PlayerBlipsData.iBlipTimerStopHighlighting[iPlayerIDInt] = -1
				SET_BIT(g_PlayerBlipsData.bs_HighlightingAlways, iPlayerIDInt)
			ELSE
				g_PlayerBlipsData.iBlipTimerStopHighlighting[iPlayerIDInt] = GET_TIME_OFFSET(GET_NETWORK_TIME(), iTimeToHighlight)
			ENDIF
			//CPRINTLN(DEBUG_BLIP, "[player_blips] g_PlayerBlipsData.iBlipTimerStopHighlighting[iPlayerIDInt] = ") NET_PRINT_INT(g_PlayerBlipsData.iBlipTimerStopHighlighting[iPlayerIDInt]) NET_NL()
		ENDIF
	ENDIF
	IF NOT (bHighlightBlip)
		CPRINTLN(DEBUG_BLIP, "[player_blips] HIGHTLIGHT_BLIP_FOR_PLAYER - called with false iPlayerIDInt = ", iPlayerIDInt)
		//CPRINTLN(DEBUG_BLIP, "[player_blips] GET_THE_NETWORK_TIMER() = ") NET_PRINT_INT(GET_THE_NETWORK_TIMER()) NET_NL()
		CLEAR_BIT(g_PlayerBlipsData.bs_PlayerBlipHighlighted, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_HighlightingAlways, iPlayerIDInt)
		// restore the correct blip priority
		SET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PlayerID)
	ENDIF
ENDPROC

PROC SET_FIXED_BLIP_SCALE_FOR_PLAYER(PLAYER_INDEX PlayerID, FLOAT fScale, BOOL bActivate, BOOL bForceThreadOwnership=FALSE)

	IF BlipPreThreadLockFailCheck(PlayerID)
		EXIT	
	ENDIF

	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "SET_FIXED_BLIP_SCALE_FOR_PLAYER - called for player ", NATIVE_TO_INT(PlayerID), " with fScale ", fScale, " bActivate = ", bActivate)
	
	BOOL bBitsetStateChanged
	IF DoBlipThreadOwnership(	g_PlayerBlipsData.FixedScaleThreadID[NATIVE_TO_INT(PlayerID)],
									g_PlayerBlipsData.FixedScaleThreadID_Forced[NATIVE_TO_INT(PlayerID)],
									g_PlayerBlipsData.bs_PlayerUsingFixedScale,									
									bActivate,
									NATIVE_TO_INT(PlayerID),
									bForceThreadOwnership,
									bBitsetStateChanged)
		IF (bActivate)						
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.fPlayerFixedScale[NATIVE_TO_INT(PlayerID)] = fScale)
					PRINTLN("[player_blips] SET_FIXED_BLIP_SCALE_FOR_PLAYER - changing g_PlayerBlipsData.fPlayerFixedScale[", NATIVE_TO_INT(PlayerID), "] to ", fScale)	
				ENDIF
			#ENDIF							
			g_PlayerBlipsData.fPlayerFixedScale[NATIVE_TO_INT(PlayerID)] = fScale
		ENDIF
	ENDIF
		
	
	
//	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.FixedScaleThreadID[NATIVE_TO_INT(PlayerID)])
//	OR (g_PlayerBlipsData.FixedScaleThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD())
//	
//		IF (bActivate)
//			SET_BIT(g_PlayerBlipsData.bs_PlayerUsingFixedScale, iPlayer)
//			g_PlayerBlipsData.fPlayerFixedScale[iPlayer] = fScale
//			g_PlayerBlipsData.FixedScaleThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD()
//			
//		ELSE
//			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingFixedScale, iPlayer)
//
//			CPRINTLN(DEBUG_BLIP, "SET_FIXED_BLIP_SCALE_FOR_PLAYER - clearing thread id")
//			g_PlayerBlipsData.FixedScaleThreadID[NATIVE_TO_INT(PlayerID)] = INT_TO_NATIVE(THREADID, -1)
//
//		ENDIF			
//	ELSE
//		SCRIPT_ASSERT("SET_FIXED_BLIP_SCALE_FOR_PLAYER - already being used by another thread")
//	ENDIF

	
ENDPROC



PROC SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PLAYER_INDEX PlayerID, BLIP_SPRITE BlipSprite, BOOL bActivate, BOOL bForceThreadOwnership=FALSE)

	IF BlipPreThreadLockFailCheck(PlayerID)
		EXIT	
	ENDIF

	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER - called for player ", NATIVE_TO_INT(PlayerID), " with BlipSprite ", ENUM_TO_INT(BlipSprite), " bActivate = ", bActivate)
	
	BOOL bBitsetStateChanged
	IF DoBlipThreadOwnership(	g_PlayerBlipsData.CustomSpriteThreadID[NATIVE_TO_INT(PlayerID)], 
							g_PlayerBlipsData.CustomSpriteThreadID_Forced[NATIVE_TO_INT(PlayerID)],
							g_PlayerBlipsData.bs_PlayerUsingCustomSprite,							
							bActivate,
							NATIVE_TO_INT(PlayerID),							
							bForceThreadOwnership,
							bBitsetStateChanged)
		IF (bActivate)
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.iPlayerCustomSprite[NATIVE_TO_INT(PlayerID)] = ENUM_TO_INT(BlipSprite))
					PRINTLN("[player_blips] SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER - changing g_PlayerBlipsData.iPlayerCustomSprite[", NATIVE_TO_INT(PlayerID), "] to ", ENUM_TO_INT(BlipSprite))	
				ENDIF
			#ENDIF
			g_PlayerBlipsData.iPlayerCustomSprite[NATIVE_TO_INT(PlayerID)] = ENUM_TO_INT(BlipSprite)
		ENDIF
		
		IF (bBitsetStateChanged)
			DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
		ENDIF
	ENDIF
	
//	IF (bForceThreadOwnership) 		
//		IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.CustomSpriteThreadID_Forced[NATIVE_TO_INT(PlayerID)])
//		OR (g_PlayerBlipsData.CustomSpriteThreadID_Forced[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD())
//			
//			g_PlayerBlipsData.CustomSpriteThreadID_Forced[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD()	
//			g_PlayerBlipsData.CustomSpriteThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD()
//		
//		ELSE
//			SCRIPT_ASSERT("SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER - bForceThreadOwnership already being used by another thread")
//		ENDIF				
//	ENDIF
//	
//	
//	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.CustomSpriteThreadID[NATIVE_TO_INT(PlayerID)])
//	OR (g_PlayerBlipsData.CustomSpriteThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD())
//		IF (bActivate)
//			IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayerUsingCustomSprite, iPlayer)
//				DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
//			ENDIF
//			SET_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomSprite, iPlayer)
//			g_PlayerBlipsData.iPlayerCustomSprite[iPlayer] = iSprite
//			g_PlayerBlipsData.CustomSpriteThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD()
//		ELSE
//			IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerUsingCustomSprite, iPlayer)
//				DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
//			ENDIF
//			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomSprite, iPlayer)
//			CPRINTLN(DEBUG_BLIP, "SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER - clearing thread id")
//			
//			IF (g_PlayerBlipsData.CustomSpriteThreadID_Forced[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD())
//				g_PlayerBlipsData.CustomSpriteThreadID_Forced[NATIVE_TO_INT(PlayerID)] = INT_TO_NATIVE(THREADID, -1)
//				PRINTLN("[player_blips] SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER - clearing forced thread. this thread id = ", NATIVE_TO_INT(GET_ID_OF_THIS_THREAD())) 
//			ENDIF
//			
//			g_PlayerBlipsData.CustomSpriteThreadID[NATIVE_TO_INT(PlayerID)] = INT_TO_NATIVE(THREADID, -1)
//		ENDIF			
//	ELSE
//		// is a force owner ship active?
//		IF IS_THREAD_ACTIVE(g_PlayerBlipsData.CustomSpriteThreadID_Forced[NATIVE_TO_INT(PlayerID)])
//		AND NOT (g_PlayerBlipsData.CustomSpriteThreadID_Forced[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD())
//			PRINTLN("[player_blips] SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER - bForceThreadOwnership active by another thread. forced thread id = ", NATIVE_TO_INT(g_PlayerBlipsData.CustomSpriteThreadID_Forced[NATIVE_TO_INT(PlayerID)])) 
//		ELSE
//			SCRIPT_ASSERT("SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER - already being used by another thread")
//		ENDIF
//	ENDIF


ENDPROC

PROC SET_CUSTOM_BLIP_NAME_FOR_PLAYER(PLAYER_INDEX PlayerID, TEXT_LABEL_15 strTextLabel, BOOL bActivate, BOOL bForceThreadOwnership=FALSE, BOOL bUseAsLiteralString=FALSE)

	IF BlipPreThreadLockFailCheck(PlayerID)
		EXIT	
	ENDIF

	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "SET_CUSTOM_BLIP_NAME_FOR_PLAYER - called for player ", NATIVE_TO_INT(PlayerID), " with text label ", strTextLabel, " use as literal string = ", bUseAsLiteralString)

	BOOL bBitsetStateChanged
	IF DoBlipThreadOwnership(	g_PlayerBlipsData.CustomBlipNameThreadID[NATIVE_TO_INT(PlayerID)], 
							g_PlayerBlipsData.CustomBlipNameThreadID_Forced[NATIVE_TO_INT(PlayerID)],
							g_PlayerBlipsData.bs_PlayerUsingCustomBlipName,							
							bActivate,
							NATIVE_TO_INT(PlayerID),							
							bForceThreadOwnership,
							bBitsetStateChanged)
							
		IF (bActivate)
			g_PlayerBlipsData.strCustomBlipName[NATIVE_TO_INT(PlayerID)] = strTextLabel	
			g_PlayerBlipsData.bCustomBlipNameIsLiteral[NATIVE_TO_INT(PlayerID)] = bUseAsLiteralString
		ENDIF
		
		IF (bBitsetStateChanged)
			IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)])
				SET_PLAYER_BLIP_NAME_CORRECTLY(PlayerID)
			ENDIF
		ENDIF
		
	ENDIF


//	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.CustomBlipNameThreadID)
//	OR (g_PlayerBlipsData.CustomBlipNameThreadID = GET_ID_OF_THIS_THREAD())
//		IF (bActivate)
//			IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayerUsingCustomBlipName, iPlayer)
//				bUpdateNow = TRUE	
//			ENDIF
//			SET_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomBlipName, iPlayer)
//			g_PlayerBlipsData.strCustomBlipName[iPlayer] = strTextLabel
//			g_PlayerBlipsData.CustomBlipNameThreadID = GET_ID_OF_THIS_THREAD()
//		ELSE
//			IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerUsingCustomBlipName, iPlayer)
//				bUpdateNow = TRUE	
//			ENDIF
//			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomBlipName, iPlayer)
//			IF (g_PlayerBlipsData.bs_PlayerUsingCustomBlipName = 0) 
//				CPRINTLN(DEBUG_BLIP, "SET_CUSTOM_BLIP_NAME_FOR_PLAYER - clearing thread id")
//				g_PlayerBlipsData.CustomBlipNameThreadID = INT_TO_NATIVE(THREADID, -1)
//			ENDIF			
//		ENDIF
//	ELSE
//		SCRIPT_ASSERT("SET_CUSTOM_BLIP_NAME_FOR_PLAYER - already being used by another thread")
//	ENDIF
//
//	IF (bUpdateNow)
//		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayer])
//			SET_PLAYER_BLIP_NAME_CORRECTLY(PlayerID)
//		ENDIF
//	ENDIF	

ENDPROC



PROC SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PLAYER_INDEX PlayerID, INT iBlipColour, BOOL bActivate, BOOL bForceThreadOwnership=FALSE)

	IF BlipPreThreadLockFailCheck(PlayerID)
		EXIT	
	ENDIF

	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER - called for player ", NATIVE_TO_INT(PlayerID), " with iBlipColour ", iBlipColour, " bActivate = ", bActivate)
	
	BOOL bBitsetStateChanged
	IF DoBlipThreadOwnership(	g_PlayerBlipsData.CustomColourThreadID[NATIVE_TO_INT(PlayerID)], 
							g_PlayerBlipsData.CustomColourThreadID_Forced[NATIVE_TO_INT(PlayerID)],
							g_PlayerBlipsData.bs_PlayerUsingCustomColour,							
							bActivate,
							NATIVE_TO_INT(PlayerID),							
							bForceThreadOwnership,
							bBitsetStateChanged)
		IF (bActivate)			
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.iCustomBlipColour[NATIVE_TO_INT(PlayerID)] = iBlipColour)
					PRINTLN("[player_blips] SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER - changing g_PlayerBlipsData.iCustomBlipColour[", NATIVE_TO_INT(PlayerID), "] to ", iBlipColour)	
				ENDIF
			#ENDIF					
			g_PlayerBlipsData.iCustomBlipColour[NATIVE_TO_INT(PlayerID)] = iBlipColour		
		ENDIF
	ENDIF
	
//	IF DOES_THIS_THREAD_HAVE_CONTROL_OF_CUSTOM_BLIP_COLOUR_FOR_PLAYER(PlayerID)
//		IF (bActivate)
//			SET_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomColour, iPlayer)
//			g_PlayerBlipsData.CustomColourThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD()
//			g_PlayerBlipsData.iCustomBlipColour[iPlayer] = iBlipColour
//		ELSE
//			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomColour, iPlayer)
//			CPRINTLN(DEBUG_BLIP, "SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER - clearing thread id")
//			g_PlayerBlipsData.CustomColourThreadID[NATIVE_TO_INT(PlayerID)] = INT_TO_NATIVE(THREADID, -1)
//		ENDIF			
//	ELSE
//		SCRIPT_ASSERT("SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER - already being used by another thread")
//	ENDIF

ENDPROC

PROC SET_CUSTOM_BLIP_ALPHA_FOR_PLAYER(PLAYER_INDEX PlayerID, INT iBlipAlpha, BOOL bActivate, BOOL bForceThreadOwnership=FALSE)

	IF BlipPreThreadLockFailCheck(PlayerID)
		EXIT	
	ENDIF

	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "SET_CUSTOM_BLIP_ALPHA_FOR_PLAYER - called for player ", NATIVE_TO_INT(PlayerID), " with iBlipAlpha ", iBlipAlpha, " bActivate = ", bActivate)
	
	BOOL bBitsetStateChanged
	IF DoBlipThreadOwnership(	g_PlayerBlipsData.CustomAlphaThreadID[NATIVE_TO_INT(PlayerID)], 
							g_PlayerBlipsData.CustomAlphaThreadID_Forced[NATIVE_TO_INT(PlayerID)],
							g_PlayerBlipsData.bs_PlayerUsingCustomAlpha,							
							bActivate,
							NATIVE_TO_INT(PlayerID),							
							bForceThreadOwnership,
							bBitsetStateChanged)
		IF (bActivate)
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.iCustomBlipAlpha[NATIVE_TO_INT(PlayerID)] = iBlipAlpha)
					PRINTLN("[player_blips] SET_CUSTOM_BLIP_ALPHA_FOR_PLAYER - changing g_PlayerBlipsData.iCustomBlipAlpha[", NATIVE_TO_INT(PlayerID), "] to ", iBlipAlpha)	
				ENDIF
			#ENDIF
			g_PlayerBlipsData.iCustomBlipAlpha[NATIVE_TO_INT(PlayerID)] = iBlipAlpha		
		ENDIF
	ENDIF
	
//	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.CustomAlphaThreadID[NATIVE_TO_INT(PlayerID)])
//	OR (g_PlayerBlipsData.CustomAlphaThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD())	
//		IF (bActivate)
//			SET_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomAlpha, iPlayer)
//			g_PlayerBlipsData.CustomAlphaThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD()
//			g_PlayerBlipsData.iCustomBlipAlpha[iPlayer] = iBlipAlpha
//		ELSE
//			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomAlpha, iPlayer)
//			CPRINTLN(DEBUG_BLIP, "SET_CUSTOM_BLIP_ALPHA_FOR_PLAYER - clearing thread id")
//			g_PlayerBlipsData.CustomAlphaThreadID[NATIVE_TO_INT(PlayerID)] = INT_TO_NATIVE(THREADID, -1)
//		ENDIF				
//	ELSE
//		SCRIPT_ASSERT("SET_CUSTOM_BLIP_ALPHA_FOR_PLAYER - already being used by another thread")
//	ENDIF

ENDPROC

PROC SET_CUSTOM_BLIP_NUMBER_FOR_PLAYER(PLAYER_INDEX PlayerID, INT iBlipNumber, BOOL bActivate, BOOL bForceThreadOwnership=FALSE)

	IF BlipPreThreadLockFailCheck(PlayerID)
		EXIT	
	ENDIF
	
	IF (iBlipNumber < 0)
	OR (iBlipNumber > 16)
		PRINTLN("[player_blips] SET_CUSTOM_BLIP_NUMBER_FOR_PLAYER - invalid blip number of ", iBlipNumber)
		SCRIPT_ASSERT("SET_CUSTOM_BLIP_NUMBER_FOR_PLAYER - invalid custom blip number. ")
		bActivate = FALSE
	ENDIF

	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "SET_CUSTOM_BLIP_NUMBER_FOR_PLAYER - called for player ", NATIVE_TO_INT(PlayerID), " with iBlipNumber ", iBlipNumber, " bActivate = ", bActivate)
	
	BOOL bBitsetStateChanged
	IF DoBlipThreadOwnership(	g_PlayerBlipsData.CustomNumberThreadID[NATIVE_TO_INT(PlayerID)], 
							g_PlayerBlipsData.CustomNumberThreadID_Forced[NATIVE_TO_INT(PlayerID)],
							g_PlayerBlipsData.bs_PlayerUsingCustomNumber,							
							bActivate,
							NATIVE_TO_INT(PlayerID),							
							bForceThreadOwnership,
							bBitsetStateChanged)
		IF (bActivate)
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.iCustomBlipNumber[NATIVE_TO_INT(PlayerID)] = iBlipNumber)
					PRINTLN("[player_blips] SET_CUSTOM_BLIP_NUMBER_FOR_PLAYER - changing g_PlayerBlipsData.iCustomBlipNumber[", NATIVE_TO_INT(PlayerID), "] to ", iBlipNumber)	
				ENDIF
			#ENDIF
			g_PlayerBlipsData.iCustomBlipNumber[NATIVE_TO_INT(PlayerID)] = iBlipNumber
		ENDIF
	ENDIF
	
//	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.CustomNumberThreadID[NATIVE_TO_INT(PlayerID)])
//	OR (g_PlayerBlipsData.CustomNumberThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD())	
//		IF (bActivate)
//			SET_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomNumber, iPlayer)
//			g_PlayerBlipsData.CustomNumberThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD()
//			g_PlayerBlipsData.iCustomBlipNumber[iPlayer] = iBlipNumber
//		ELSE
//			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingCustomNumber, iPlayer)
//			CPRINTLN(DEBUG_BLIP, "SET_CUSTOM_BLIP_NUMBER_FOR_PLAYER - clearing thread id")
//			g_PlayerBlipsData.CustomNumberThreadID[NATIVE_TO_INT(PlayerID)] = INT_TO_NATIVE(THREADID, -1)
//		ENDIF				
//	ELSE
//		SCRIPT_ASSERT("SET_CUSTOM_BLIP_NUMBER_FOR_PLAYER - already being used by another thread")
//	ENDIF

ENDPROC

PROC DISABLE_SECONDARY_OUTLINES_FOR_PLAYER_BLIP(PLAYER_INDEX PlayerID, BOOL bDisable, BOOL bForceThreadOwnership=FALSE)
	
	IF BlipPreThreadLockFailCheck(PlayerID)
		EXIT	
	ENDIF

	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "DISABLE_SECONDARY_OUTLINES_FOR_PLAYER_BLIP - called for player ", NATIVE_TO_INT(PlayerID), " bDisable = ", bDisable)
	
	BOOL bBitsetStateChanged
	DoBlipThreadOwnership(	g_PlayerBlipsData.DisableSecondaryOutlinesThreadID[NATIVE_TO_INT(PlayerID)],
							g_PlayerBlipsData.DisableSecondaryOutlinesThreadID_Forced[NATIVE_TO_INT(PlayerID)],
							g_PlayerBlipsData.bs_PlayerSecondaryOutlinesDisabled,
							bDisable,
							NATIVE_TO_INT(PlayerID),
							bForceThreadOwnership,
							bBitsetStateChanged)
							
	
//	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.DisableSecondaryOutlinesThreadID)
//	OR (g_PlayerBlipsData.DisableSecondaryOutlinesThreadID = GET_ID_OF_THIS_THREAD())	
//		IF (bDisable)
//			SET_BIT(g_PlayerBlipsData.bs_PlayerSecondaryOutlinesDisabled, iPlayer)
//			g_PlayerBlipsData.DisableSecondaryOutlinesThreadID = GET_ID_OF_THIS_THREAD()
//		ELSE
//			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerSecondaryOutlinesDisabled, iPlayer)
//			IF (g_PlayerBlipsData.bs_PlayerSecondaryOutlinesDisabled = 0) 
//				CPRINTLN(DEBUG_BLIP, "DISABLE_SECONDARY_OUTLINES_FOR_PLAYER_BLIP - clearing thread id")
//				g_PlayerBlipsData.DisableSecondaryOutlinesThreadID = INT_TO_NATIVE(THREADID, -1)
//			ENDIF
//		ENDIF				
//	ELSE
//		SCRIPT_ASSERT("DISABLE_SECONDARY_OUTLINES_FOR_PLAYER_BLIP - already being used by another thread")
//	ENDIF

ENDPROC


PROC DISABLE_STEALTH_FOR_PLAYER_BLIP(PLAYER_INDEX PlayerID, BOOL bDisable, BOOL bForceThreadOwnership=FALSE)
	
	IF BlipPreThreadLockFailCheck(PlayerID)
		EXIT	
	ENDIF

	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "DISABLE_STEALTH_FOR_PLAYER_BLIP - called for player ", NATIVE_TO_INT(PlayerID), " bDisable = ", bDisable)
	
	BOOL bBitsetStateChanged
	DoBlipThreadOwnership(	g_PlayerBlipsData.DisableStealthThreadID[NATIVE_TO_INT(PlayerID)],
							g_PlayerBlipsData.DisableStealthThreadID_Forced[NATIVE_TO_INT(PlayerID)],
							g_PlayerBlipsData.bs_PlayerStealthDisabled,
							bDisable,
							NATIVE_TO_INT(PlayerID),
							bForceThreadOwnership,
							bBitsetStateChanged)
							
ENDPROC

FUNC INT GET_PLAYERS_CUSTOM_BLIP_COLOUR(PLAYER_INDEX PlayerID)
	#IF IS_DEBUG_BUILD
		IF NOT IS_PLAYER_USING_CUSTOM_BLIP_COLOUR(PlayerID)
			CPRINTLN(DEBUG_BLIP, "GET_PLAYERS_CUSTOM_BLIP_COLOUR - player is not using a custom blip colour! Called on player ", NATIVE_TO_INT(PlayerID))
			SCRIPT_ASSERT("GET_PLAYERS_CUSTOM_BLIP_COLOUR - player is not using a custom blip colour!")
		ENDIF
	#ENDIF
	RETURN g_PlayerBlipsData.iCustomBlipColour[NATIVE_TO_INT(PlayerID)]
ENDFUNC

FUNC INT GET_PLAYERS_CUSTOM_BLIP_ALPHA(PLAYER_INDEX PlayerID)
	#IF IS_DEBUG_BUILD
		IF NOT IS_PLAYER_USING_CUSTOM_BLIP_ALPHA(PlayerID)
			CPRINTLN(DEBUG_BLIP, "GET_PLAYERS_CUSTOM_BLIP_ALPHA - player is not using a custom blip alpha! Called on player ", NATIVE_TO_INT(PlayerID))
			SCRIPT_ASSERT("GET_PLAYERS_CUSTOM_BLIP_ALPHA - player is not using a custom blip alpha!")
		ENDIF
	#ENDIF
	RETURN g_PlayerBlipsData.iCustomBlipAlpha[NATIVE_TO_INT(PlayerID)]
ENDFUNC

FUNC INT GET_PLAYERS_CUSTOM_BLIP_NUMBER(PLAYER_INDEX PlayerID)
	#IF IS_DEBUG_BUILD
		IF NOT IS_PLAYER_USING_CUSTOM_BLIP_NUMBER(PlayerID)
			CPRINTLN(DEBUG_BLIP, "GET_PLAYERS_CUSTOM_BLIP_NUMBER - player is not using a custom blip number! Called on player ", NATIVE_TO_INT(PlayerID))
			SCRIPT_ASSERT("GET_PLAYERS_CUSTOM_BLIP_NUMBER - player is not using a custom blip number!")
		ENDIF
	#ENDIF
	RETURN g_PlayerBlipsData.iCustomBlipNumber[NATIVE_TO_INT(PlayerID)]
ENDFUNC

FUNC BLIP_SPRITE GET_PLAYERS_CUSTOM_BLIP_SPRITE(PLAYER_INDEX PlayerID)
	#IF IS_DEBUG_BUILD
		IF NOT IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(PlayerID)
			CPRINTLN(DEBUG_BLIP, "GET_PLAYERS_CUSTOM_BLIP_SPRITE - player is not using a custom sprite! Called on player ", NATIVE_TO_INT(PlayerID))
			SCRIPT_ASSERT("GET_PLAYERS_CUSTOM_BLIP_SPRITE - player is not using a custom sprite!")
		ENDIF
	#ENDIF
	RETURN INT_TO_ENUM(BLIP_SPRITE, g_PlayerBlipsData.iPlayerCustomSprite[NATIVE_TO_INT(PlayerID)])
ENDFUNC

PROC SET_PLAYER_BLIP_AS_HIDDEN(PLAYER_INDEX PlayerID,  BOOL bHide, BOOL bForceThreadOwnership=FALSE)

	IF BlipPreThreadLockFailCheck(PlayerID)
		EXIT	
	ENDIF

	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_BLIP, "SET_PLAYER_BLIP_AS_HIDDEN - called for player ", NATIVE_TO_INT(PlayerID), " with bHide = ", bHide)
	
	BOOL bBitsetStateChanged
	DoBlipThreadOwnership(	g_PlayerBlipsData.HiddenBlipThreadID[NATIVE_TO_INT(PlayerID)], 
							g_PlayerBlipsData.HiddenBlipThreadID_Forced[NATIVE_TO_INT(PlayerID)],
							g_PlayerBlipsData.bs_PlayerIsHidden,							
							bHide,
							NATIVE_TO_INT(PlayerID),							
							bForceThreadOwnership,
							bBitsetStateChanged)

	
//	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HiddenBlipThreadID[NATIVE_TO_INT(PlayerID)])
//	OR (g_PlayerBlipsData.HiddenBlipThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD())	
//		IF (bHide)
//			SET_BIT(g_PlayerBlipsData.bs_PlayerIsHidden, iPlayer)
//			g_PlayerBlipsData.HiddenBlipThreadID[NATIVE_TO_INT(PlayerID)] = GET_ID_OF_THIS_THREAD()
//		ELSE
//			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerIsHidden, iPlayer)
//
//			CPRINTLN(DEBUG_BLIP, "SET_PLAYER_BLIP_AS_HIDDEN - clearing thread id")
//			g_PlayerBlipsData.HiddenBlipThreadID[NATIVE_TO_INT(PlayerID)] = INT_TO_NATIVE(THREADID, -1)
//
//		ENDIF			
//	ELSE
//		SCRIPT_ASSERT("SET_PLAYER_BLIP_AS_HIDDEN - already being used by another thread")
//	ENDIF


ENDPROC

FUNC BOOL IS_BLIP_FLASHING_FOR_PLAYER(PLAYER_INDEX PlayerID)
	INT iPlayerIDInt = NATIVE_TO_INT(PlayerID)
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
		RETURN IS_BIT_SET(g_PlayerBlipsData.bs_PlayerBlipSetToFlash, iPlayerIDInt)
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC FLASH_BLIP_FOR_PLAYER(PLAYER_INDEX PlayerID, BOOL bFlashBlip = TRUE, INT iTimeToHighlight = FLASH_BLIP_TIME)

	IF (PlayerID = INVALID_PLAYER_INDEX())
		SCRIPT_ASSERT("FLASH_BLIP_FOR_PLAYER - passed in invalid player index.")
		EXIT
	ENDIF

	IF (iTimeToHighlight > MAX_FLASH_BLIP_TIME)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("FLASH_BLIP_FOR_PLAYER - iTimeToHighlight exceeds MAX_FLASH_BLIP_TIME, speak to neilf")	
			CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_BLIP_FOR_PLAYER - iTimeToHighlight exceeds MAX_FLASH_BLIP_TIME = ", iTimeToHighlight) 
		#ENDIF
		iTimeToHighlight = MAX_FLASH_BLIP_TIME
	ENDIF

	INT iPlayerIDInt = NATIVE_TO_INT(PlayerID)
	IF (bFlashBlip)
		IF (PlayerID = MPGlobals.LocalPlayerID)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_BLIP_FOR_PLAYER - called with local player, so not flashing. iPlayerIDInt = ", iPlayerIDInt) 
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_BLIP_FOR_PLAYER - called with true iPlayerIDInt = ", iPlayerIDInt, ", player ", GET_PLAYER_NAME(playerID), ", bFlashBlip = ", PICK_STRING(bFlashBlip, "TRUE", "FALSE"), ", iTimeToHighlight = ", iTimeToHighlight)
				DEBUG_PRINTCALLSTACK()
			#ENDIF
			
			IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
				CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_BLIP_FOR_PLAYER - setting blip")
				SET_BIT(g_PlayerBlipsData.bs_PlayerBlipSetToFlash, iPlayerIDInt)	
				CLEAR_BIT(g_PlayerBlipsData.bs_FlashingAlways, iPlayerIDInt)
				SET_BLIP_FLASHES(g_PlayerBlipsData.playerBlips[iPlayerIDInt], TRUE)		
				SET_BLIP_FLASH_INTERVAL(g_PlayerBlipsData.playerBlips[iPlayerIDInt], BLIP_FLASHING_TIME)
				SET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PlayerID)
				// when to stop the blip flashing. 
				IF (iTimeToHighlight < 0)
					//g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt] = -1
					SET_BIT(g_PlayerBlipsData.bs_FlashingAlways, iPlayerIDInt)
				ELSE
					g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt] = GET_TIME_OFFSET(GET_NETWORK_TIME() , iTimeToHighlight)
				ENDIF
				//NET_PRINT("g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt] = ") NET_PRINT_INT(g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt]) NET_NL()
			ELSE
				CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_BLIP_FOR_PLAYER - blip doesn't exist setting values")
				SET_BIT(g_PlayerBlipsData.bs_PlayerBlipSetToFlash, iPlayerIDInt)	
				CLEAR_BIT(g_PlayerBlipsData.bs_FlashingAlways, iPlayerIDInt)
				// when to stop the blip flashing. 
				IF (iTimeToHighlight < 0)
					//g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt] = -1
					SET_BIT(g_PlayerBlipsData.bs_FlashingAlways, iPlayerIDInt)
				ELSE
					g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt] = GET_TIME_OFFSET(GET_NETWORK_TIME() , iTimeToHighlight)
					CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_BLIP_FOR_PLAYER - iBlipTimerStopFlashing[", iPlayerIDInt, "] = ", NATIVE_TO_INT(g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt]))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NOT (bFlashBlip)
		CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_BLIP_FOR_PLAYER - called with false iPlayerIDInt = ", iPlayerIDInt)
		DEBUG_PRINTCALLSTACK()
		//CPRINTLN(DEBUG_BLIP, "[player_blips] GET_THE_NETWORK_TIMER() = ") NET_PRINT_INT(GET_THE_NETWORK_TIMER()) NET_NL()
		CLEAR_BIT(g_PlayerBlipsData.bs_PlayerBlipSetToFlash, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_FlashingAlways, iPlayerIDInt)
		// restore the correct blip priority
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])		
			SET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PlayerID)
			SET_BLIP_FLASHES(g_PlayerBlipsData.playerBlips[iPlayerIDInt], FALSE)
		ENDIF
	ENDIF
ENDPROC


PROC UPDATE_HIGHLIGHTED_BLIP_FOR_PLAYER(PLAYER_INDEX PlayerID)
	INT iPlayerIDInt = NATIVE_TO_INT(PlayerID)
	IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerBlipHighlighted, iPlayerIDInt)
		// has it timed out?
		IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_PlayerBlipsData.iBlipTimerStopHighlighting[iPlayerIDInt]) 
		AND NOT IS_BIT_SET(g_PlayerBlipsData.bs_HighlightingAlways, iPlayerIDInt)
			HIGHTLIGHT_BLIP_FOR_PLAYER(PlayerID, FALSE)
		ENDIF
	ENDIF
ENDPROC



PROC UPDATE_FLASHING_BLIP_FOR_PLAYER(PLAYER_INDEX PlayerID)
	INT iPlayerIDInt = NATIVE_TO_INT(PlayerID)
	INT iDiff
	
	// should we flash the players blip
	IF (IS_BIT_SET(GlobalplayerBD[iPlayerIDInt].playerBlipData.iFlags, PBBD_IS_SELF_HIGHLIGHTED) AND NOT IS_BIT_SET(g_PlayerBlipsData.bs_IgnoreSelfHighlightForPlayer, iPlayerIDInt))
	OR IS_BIT_SET(MPGlobals.iTaggedPlayersBitSet, iPlayerIDInt)
	OR IS_BIT_SET(g_PlayerBlipsData.bs_IsMadeVisibleBySonar, iPlayerIDInt)
	
		IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayerBlipShouldFlash, iPlayerIDInt)
		
			CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_FLASHING_BLIP_FOR_PLAYER - Start flashing...")
			CPRINTLN(DEBUG_BLIP, "[player_blips] bIsSelfHighlighted = ", PICK_STRING(IS_BIT_SET(GlobalplayerBD[iPlayerIDInt].playerBlipData.iFlags, PBBD_IS_SELF_HIGHLIGHTED), "TRUE", "FALSE"))
			CPRINTLN(DEBUG_BLIP, "[player_blips] bs_IgnoreSelfHighlightForPlayer = ", PICK_STRING(IS_BIT_SET(g_PlayerBlipsData.bs_IgnoreSelfHighlightForPlayer, iPlayerIDInt), "TRUE", "FALSE"))
			CPRINTLN(DEBUG_BLIP, "[player_blips] iTaggedPlayersBitSet = ", PICK_STRING(IS_BIT_SET(MPGlobals.iTaggedPlayersBitSet, iPlayerIDInt), "TRUE", "FALSE"))
			
			FLASH_BLIP_FOR_PLAYER(PlayerID, TRUE, -1)	
			SET_BIT(g_PlayerBlipsData.bs_PlayerBlipShouldFlash, iPlayerIDInt)
		ELSE
			IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
				IF NOT IS_BLIP_FLASHING(g_PlayerBlipsData.playerBlips[iPlayerIDInt])	
				
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_FLASHING_BLIP_FOR_PLAYER - stop flashing 1 ")				
				
					FLASH_BLIP_FOR_PLAYER(PlayerID, FALSE)	
					CLEAR_BIT(g_PlayerBlipsData.bs_PlayerBlipShouldFlash, iPlayerIDInt)
					//CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_FLASHING_BLIP_FOR_PLAYER - blip is NOT flashing for player ") NET_PRINT_INT(iPlayerIDInt) NET_NL()
					
				ELSE
					//CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_FLASHING_BLIP_FOR_PLAYER - blip is flashing for player ") NET_PRINT_INT(iPlayerIDInt) NET_NL()
				ENDIF
			ELSE
				CLEAR_BIT(g_PlayerBlipsData.bs_PlayerBlipShouldFlash, iPlayerIDInt)	
			ENDIF
		ENDIF
		
	ELSE
		IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerBlipShouldFlash, iPlayerIDInt)
			
			CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_FLASHING_BLIP_FOR_PLAYER - stop flashing 2 ")
			
			FLASH_BLIP_FOR_PLAYER(PlayerID, FALSE)	
			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerBlipShouldFlash, iPlayerIDInt)
		ENDIF	
		IF IS_BIT_SET(g_PlayerBlipsData.bs_IgnoreSelfHighlightForPlayer, iPlayerIDInt)
			IF NOT IS_BIT_SET(GlobalplayerBD[iPlayerIDInt].playerBlipData.iFlags, PBBD_IS_SELF_HIGHLIGHTED)
				CLEAR_BIT(g_PlayerBlipsData.bs_IgnoreSelfHighlightForPlayer, iPlayerIDInt)	
			ENDIF
		ENDIF
	ENDIF
	
	
	
	IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerBlipSetToFlash, iPlayerIDInt)
		// has it timed out?
		IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt])
		AND NOT IS_BIT_SET(g_PlayerBlipsData.bs_FlashingAlways, iPlayerIDInt)
			CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_FLASHING_BLIP_FOR_PLAYER - stop flashing 3 iBlipTimerStopFlashing[", iPlayerIDInt, "] = ", NATIVE_TO_INT(g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt]))
			FLASH_BLIP_FOR_PLAYER(PlayerID, FALSE)
		ELSE
			IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
				IF NOT IS_BLIP_FLASHING(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
					SET_BLIP_FLASHES(g_PlayerBlipsData.playerBlips[iPlayerIDInt], TRUE)		
					SET_BLIP_FLASH_INTERVAL(g_PlayerBlipsData.playerBlips[iPlayerIDInt], BLIP_FLASHING_TIME)
					SET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PlayerID)
					
					// safety check that we dont let blip flash forever
					IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_FlashingAlways, iPlayerIDInt)
						IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt])							
							iDiff = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt])) 
							IF iDiff > MAX_FLASH_BLIP_TIME // max time a blip should flash for	
								
								CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_FLASHING_BLIP_FOR_PLAYER - SAFETY CHECK HIT. iPlayerIDInt = ", iPlayerIDInt, ", player ", GET_PLAYER_NAME(playerID), ", ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt])) = ", ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt])))
								
								g_PlayerBlipsData.iBlipTimerStopFlashing[iPlayerIDInt] = GET_TIME_OFFSET(GET_NETWORK_TIME() , FLASH_BLIP_TIME)		
															
							ENDIF
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_FLASHING_BLIP_FOR_PLAYER - blip didn't exist when blip flashing started. It does now setting to flash. iPlayerIDInt = ", iPlayerIDInt, ", player ", GET_PLAYER_NAME(playerID), ", IS_BIT_SET(g_PlayerBlipsData.bs_FlashingAlways, iPlayerIDInt) = ", PICK_STRING(IS_BIT_SET(g_PlayerBlipsData.bs_FlashingAlways, iPlayerIDInt), "TRUE", "FALSE"))
					#ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ELSE
		
		// is blip still flashing for some reason?
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			IF IS_BLIP_FLASHING(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
				SET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(PlayerID)
				SET_BLIP_FLASHES(g_PlayerBlipsData.playerBlips[iPlayerIDInt], FALSE)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_FLASHING_BLIP_FOR_PLAYER - blip was flashing even though bs_PlayerBlipSetToFlash is cleared, forcefully stopping. iPlayerIDInt = ", iPlayerIDInt, ", player ", GET_PLAYER_NAME(playerID))
				#ENDIF					
			ENDIF
		ENDIF
	
	ENDIF
ENDPROC


PROC UPDATE_PULSING_BLIP_FOR_PLAYER(PLAYER_INDEX PlayerID)
	INT iPlayerIDInt = NATIVE_TO_INT(PlayerID)
	IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_BlipCreatedThisFrame, iPlayerIDInt)
		IF IS_BIT_SET(g_PlayerBlipsData.bs_PulseBlip, iPlayerIDInt)
			IF DOES_BLIP_EXIST( g_PlayerBlipsData.playerBlips[iPlayerIDInt])
				IF (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) = GET_STANDARD_BLIP_ENUM_FOR_PLAYER(PlayerID))
					PULSE_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
					SET_BIT(g_PlayerBlipsData.bs_BlipIsPulsing, iPlayerIDInt)
					g_PlayerBlipsData.iBlipTimerPulsing[iPlayerIDInt] = GET_TIME_OFFSET(GET_NETWORK_TIME() , 2000)
					#IF IS_DEBUG_BUILD
						IF (g_PlayerBlipsData.bDebugBlipOutput)
							CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PULSING_BLIP_FOR_PLAYER - called! - player  = ", iPlayerIDInt, ", Blip id = ", NATIVE_TO_INT(g_PlayerBlipsData.playerBlips[iPlayerIDInt]))		
						ENDIF
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF (g_PlayerBlipsData.bDebugBlipOutput)
							CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PULSING_BLIP_FOR_PLAYER - called, but blip not standard - player  = ", iPlayerIDInt, ", Blip id = ", NATIVE_TO_INT(g_PlayerBlipsData.playerBlips[iPlayerIDInt]))		
						ENDIF
					#ENDIF
				ENDIF
				CLEAR_BIT(g_PlayerBlipsData.bs_PulseBlip, iPlayerIDInt)
			ELSE
				IF (iPlayerIDInt = NATIVE_TO_INT(MPGlobals.LocalPlayerID))
					CLEAR_BIT(g_PlayerBlipsData.bs_PulseBlip, iPlayerIDInt)	
				ENDIF
			ENDIF
		ENDIF
		IF IS_BIT_SET(g_PlayerBlipsData.bs_BlipIsPulsing, iPlayerIDInt)
			IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_PlayerBlipsData.iBlipTimerPulsing[iPlayerIDInt])
				CLEAR_BIT(g_PlayerBlipsData.bs_BlipIsPulsing, iPlayerIDInt)
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PULSING_BLIP_FOR_PLAYER - stopped pulsing for player - player  = ", iPlayerIDInt)		
					ENDIF
				#ENDIF				
			ELSE
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PULSING_BLIP_FOR_PLAYER - still pulsing for player - player  = ", iPlayerIDInt)		
					ENDIF
				#ENDIF	
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PULSING_BLIP_FOR_PLAYER - called, but blip was just created this frame - player  = ", iPlayerIDInt, ", Blip id = ", NATIVE_TO_INT(g_PlayerBlipsData.playerBlips[iPlayerIDInt]))		
			ENDIF
		#ENDIF		
	ENDIF
ENDPROC


PROC RESTORE_BLIP_TO_DEFAULT_SPRITE(PLAYER_INDEX playerID)
	INT iPlayerIDInt
	iPlayerIDInt = NATIVE_TO_INT(playerID)
	BLIP_SPRITE DesiredBlipSprite
	//DesiredBlipSprite = GET_BLIP_SPRITE_FOR_TEAM(GET_PLAYER_TEAM(playerID), IS_BIT_SET(g_PlayerBlipsData.bs_PlayerBlipHighlighted, NATIVE_TO_INT(PlayerID)))		
	DesiredBlipSprite = GET_STANDARD_BLIP_ENUM_FOR_PLAYER(PlayerID)
	IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) = DesiredBlipSprite)						
//		// are we trying to restore the blip back to original? if so we need to destroy the blip and recreate it						
//		g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER(playerID)									
//		IF NOT (DesiredBlipSprite = GET_STANDARD_BLIP_ENUM_FOR_PLAYER(PlayerID))
			MAKE_PLAYERS_BLIP_THIS_SPRITE(PlayerID, DesiredBlipSprite)
//		ENDIF								
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_VEHICLE_THAT_HAS_A_BLIP(PLAYER_INDEX PlayerID)
	IF IS_PED_IN_VEHICLE_THAT_HAS_A_BLIP(GET_PLAYER_PED(PlayerID))
//	#IF FEATURE_EXECUTIVE
//	OR IS_PLAYER_VEHICLE_BLIP_BEING_BLIPPED_BY_CONTRABAND_SYSTEM(PlayerID)
//	#ENDIF
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC FLOAT GetDistToBlipAsValueFrom1to0(BLIP_INDEX BlipID, FLOAT fMinDist, FLOAT fMaxDist)
	PLAYER_INDEX PlayerID
	PlayerID = MPGlobals.LocalPlayerID
	
	FLOAT fBlipDistMultiplier
	VECTOR vBlipCoords
	VECTOR vPlayerCoords
	
	vBlipCoords	= GET_ACTUAL_BLIP_COORDS(BlipID)
	vBlipCoords.z = 0.0
	IF (g_bActiveInGunTurret)
	OR IsUsingSpecialWeaponCamera()
		IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PlayerID)
			
			vPlayerCoords = GET_BLIP_COORD_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PlayerID)
			
		ELIF IS_PLAYER_IN_CREATOR_IAA_FACILITY(PlayerID)
			
			vPlayerCoords = GET_BLIP_COORDS_OF_CREATOR_INTERIOR_PLAYER_IS_IN(PlayerID)

		ENDIF
	ELSE
		vPlayerCoords = GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID), FALSE)
	ENDIF
	vPlayerCoords.z = 0.0

	// 1. Get distance from this blip to the player		
	fBlipDistMultiplier = VMAG(vPlayerCoords - vBlipCoords)		

	// 2. turn this to a value between 1 and MIN_BLIP_SIZE depending on how far away he is (1 is closest)
	IF (fBlipDistMultiplier < fMinDist)
		fBlipDistMultiplier = fMinDist
	ENDIF
	IF (fBlipDistMultiplier >  fMaxDist)
		fBlipDistMultiplier = fMaxDist	
	ENDIF			
	fBlipDistMultiplier = (fBlipDistMultiplier - fMinDist) / (fMaxDist - fMinDist)	
	
	// invert so zero is furthest away
	fBlipDistMultiplier = fBlipDistMultiplier - 1.0
	fBlipDistMultiplier *= -1.0	
	
	RETURN(fBlipDistMultiplier)
ENDFUNC

FUNC FLOAT GET_DIST_SCALE_MULTIPLIER(BLIP_INDEX BlipID)

	FLOAT fBlipDistMultiplier = GetDistToBlipAsValueFrom1to0(BlipID, MIN_DIST_FOR_BLIP_SIZE, MAX_DIST_FOR_BLIP_SIZE)
	
	fBlipDistMultiplier = (((1.0 - MIN_BLIP_SIZE) * fBlipDistMultiplier) + MIN_BLIP_SIZE)
	
	RETURN(fBlipDistMultiplier)
ENDFUNC

FUNC INT GB_GET_MIN_ALPHA_FOR_REMOTE_BLIP(GB_CONTRABAND_TYPE eContrabandDeliveryType = GBCT_CEO)

	SWITCH(eContrabandDeliveryType)
		CASE GBCT_GUNRUN		RETURN 0
		CASE GBCT_SMUGGLER		RETURN 255
		CASE GBCT_FM_GANGOPS	RETURN 255
		#IF FEATURE_CASINO_HEIST
		CASE GBCT_CASINO_HEIST	RETURN 255
		#ENDIF
		CASE GBCT_ISLAND_HEIST	RETURN 255
		CASE GBCT_CHALKBOARD	RETURN 255
		CASE GBCT_FIXER_SECURITY	RETURN 255
	ENDSWITCH

	RETURN 0

ENDFUNC

FUNC FLOAT GB_GET_MAX_DIST_FOR_REMOTE_BLIP(GB_CONTRABAND_TYPE eContrabandDeliveryType = GBCT_CEO)

	SWITCH(eContrabandDeliveryType)
		CASE GBCT_GUNRUN	RETURN 4500.0
	ENDSWITCH

	RETURN 1400.0

ENDFUNC

FUNC FLOAT GB_GET_MIN_DIST_FOR_REMOTE_BLIP(GB_CONTRABAND_TYPE eContrabandDeliveryType = GBCT_CEO)

	SWITCH(eContrabandDeliveryType)
		CASE GBCT_GUNRUN	RETURN 800.0
	ENDSWITCH

	RETURN 400.0

ENDFUNC

FUNC INT GET_MIN_BLIP_ALPHA()
	IF IS_PLAYER_ON_ANY_MP_MISSION(MPGlobals.LocalPlayerID)
		RETURN(MIN_BLIP_ALPHA_FOR_DIST_ON_MISSION)
	ENDIF	
	RETURN(MIN_BLIP_ALPHA_FOR_DIST_FM)	
ENDFUNC

FUNC INT GET_BLIP_ALPHA_VALUE_BASED_ON_DIST_FROM_PLAYER(BLIP_INDEX BlipID, INT iMinAlphaValue=0, FLOAT fMaxBlipDist=1400.0, FLOAT fMinBlipDist=400.0)

	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
	AND NOT IS_PLAYER_USING_ARENA()
		FLOAT fBlipAlphaMultiplier = GetDistToBlipAsValueFrom1to0(BlipID, fMinBlipDist, fMaxBlipDist)
		
		IF (iMinAlphaValue = 0)
			iMinAlphaValue = GET_MIN_BLIP_ALPHA()
		ENDIF
		
		RETURN (ROUND(TO_FLOAT(255-iMinAlphaValue) * fBlipAlphaMultiplier) + iMinAlphaValue)
	ENDIF
	RETURN 255
ENDFUNC


PROC SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(BLIP_INDEX BlipID, INT iMinAlphaValue=0, FLOAT fMaxBlipDist=1400.0, FLOAT fMinBlipDist=400.0)
	#IF IS_DEBUG_BUILD
		IF NOT DOES_BLIP_EXIST(BlipID)
			SCRIPT_ASSERT("SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER - blip does not exist!")
		ENDIF
	#ENDIF
	#IF IS_DEBUG_BUILD
		IF NOT (g_PlayerBlipsData.bDisableAlpha)
	#ENDIF
		SET_BLIP_ALPHA(BlipID, GET_BLIP_ALPHA_VALUE_BASED_ON_DIST_FROM_PLAYER(BlipID, iMinAlphaValue,fMaxBlipDist, fMinBlipDist))
	#IF IS_DEBUG_BUILD
		ENDIF 
	#ENDIF
ENDPROC


FUNC INT GET_BLIP_ALPHA_VALUE_BASED_ON_DIST_FROM_PLAYER_IMPROMPTU(BLIP_INDEX BlipID, INT iMinAlphaValue)

	IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
		FLOAT fBlipAlphaMultiplier = GetDistToBlipAsValueFrom1to0(BlipID, MIN_DIST_FOR_BLIP_ALPHA_IMPROMPTU_DM, MAX_DIST_FOR_BLIP_ALPHA_IMPROMPTU_DM)
	
		RETURN (ROUND(TO_FLOAT(255-iMinAlphaValue) * fBlipAlphaMultiplier) + iMinAlphaValue)
	ENDIF
	RETURN 255
ENDFUNC

PROC SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER_IMPROMPTU(BLIP_INDEX BlipID, INT iMinAlphaValue=0)
	#IF IS_DEBUG_BUILD
		IF NOT DOES_BLIP_EXIST(BlipID)
			SCRIPT_ASSERT("SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER_IMPROMPTU - blip does not exist!")
		ENDIF
	#ENDIF
	#IF IS_DEBUG_BUILD
		IF NOT (g_PlayerBlipsData.bDisableAlpha)
	#ENDIF
	SET_BLIP_ALPHA(BlipID, GET_BLIP_ALPHA_VALUE_BASED_ON_DIST_FROM_PLAYER_IMPROMPTU(BlipID, iMinAlphaValue))
	#IF IS_DEBUG_BUILD
		ENDIF 
	#ENDIF
ENDPROC

PROC MAINTAIN_ALPHA_RANGE_FOR_ILLICIT_GOODS(PLAYER_INDEX playerID)
	INT iPlayerInt = NATIVE_TO_INT(playerID)
	IF SHOULD_PLAYER_HAVE_ILLICIT_GOODS_BLIP(playerID, TRUE)
		SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(g_PlayerBlipsData.playerBlips[iPlayerInt])
//		
//		IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_IllicitGoodsShortRange, iPlayerInt)
//			SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.playerBlips[iPlayerInt], TRUE)
//			SET_BIT(g_PlayerBlipsData.bs_IllicitGoodsShortRange, iPlayerInt)
//		ENDIF
//	ELSE
//		IF IS_BIT_SET(g_PlayerBlipsData.bs_IllicitGoodsShortRange, iPlayerInt)
//			IF IS_PLAYER_BLIP_SET_AS_LONG_RANGE(PlayerID)
//				SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.playerBlips[iPlayerInt], FALSE)
//			ELSE
//				SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.playerBlips[iPlayerInt], TRUE)
//			ENDIF						
//			CLEAR_BIT(g_PlayerBlipsData.bs_IllicitGoodsShortRange, iPlayerInt)
//		ENDIF
	ENDIF
ENDPROC



PROC SET_CORRECT_PLAYER_BLIP_SPRITE(PLAYER_INDEX playerID)
	INT iPlayerIDInt = NATIVE_TO_INT(playerID)
	BOOL bInMyPV = FALSE
	
	// clear bits for Alpha BEFORE we set the sprite
	IF NOT IS_PLAYER_IN_VEHICLE_THAT_HAS_A_BLIP(PlayerID)
		
		// unset alpha from being in a vehicle with a blip.
		IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerInVehicleThatHasBlip, iPlayerIDInt)
			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerInVehicleThatHasBlip, iPlayerIDInt)
			CPRINTLN(DEBUG_BLIP, "[player_blips] set player ", iPlayerIDInt, " alpha 255, no longer in vehicle with blip") 	
		ENDIF
	
		// should we set the alpha to 0 of non-leaders in vehicles? 2027635
		IF SHOULD_BLIP_ONLY_SHOW_LEADERS_WHEN_IN_VEHICLE(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			IF IS_BIT_SET(GlobalplayerBD[iPlayerIDInt].playerBlipData.iFlags, PBBD_IS_LEADER_OF_VEHICLE)
				IF IS_BIT_SET(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerIDInt)
					CLEAR_BIT(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerIDInt)
					CPRINTLN(DEBUG_BLIP, "[player_blips] set player ", iPlayerIDInt, " alpha 255 as leader in vehicle")
				ENDIF
			ENDIF	
		ELSE
			IF IS_BIT_SET(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerIDInt)
				CLEAR_BIT(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerIDInt)
				CPRINTLN(DEBUG_BLIP, "[player_blips] restored player ", iPlayerIDInt, " alpha 255 as no longer have leader only blip.")
			ENDIF
		ENDIF	
	ENDIF

	IF DOES_ENTITY_EXIST(MPGlobals.RemotePV[NATIVE_TO_INT(MPGlobals.LocalPlayerID)])
	AND NOT IS_ENTITY_DEAD(MPGlobals.RemotePV[NATIVE_TO_INT(MPGlobals.LocalPlayerID)])
	AND IS_ENTITY_A_VEHICLE(MPGlobals.RemotePV[NATIVE_TO_INT(MPGlobals.LocalPlayerID)])
		//CPRINTLN(DEBUG_BLIP, "[player_blips] MPGlobals.LocalPlayerID ", NATIVE_TO_INT(MPGlobals.LocalPlayerID), " has remote pv.")	
		IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(PlayerID), MPGlobals.RemotePV[NATIVE_TO_INT(MPGlobals.LocalPlayerID)])
			CPRINTLN(DEBUG_BLIP, "[player_blips] player ", iPlayerIDInt, " is in remote pv belonging to local player ", NATIVE_TO_INT(MPGlobals.LocalPlayerID))
			bInMyPV = TRUE
		ENDIF
	ENDIF	
		
	//Extra condition for cleaning up leader blip hiding when no longer in any vehicle.
	IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID))
		IF IS_BIT_SET(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerIDInt)
			CLEAR_BIT(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerIDInt)
			CPRINTLN(DEBUG_BLIP, "[player_blips] restored player ", iPlayerIDInt, " alpha 255 as no longer in a vehicle.")
		ENDIF
	ENDIF	
	
	IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayerInVehicleThatHasBlip, iPlayerIDInt)
	AND NOT IS_BIT_SET(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerIDInt)
	
		// player has a custom blip sprite
		IF IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(playerID)
		
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  INT_TO_ENUM(BLIP_SPRITE, g_PlayerBlipsData.iPlayerCustomSprite[iPlayerIDInt]))
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, INT_TO_ENUM(BLIP_SPRITE, g_PlayerBlipsData.iPlayerCustomSprite[iPlayerIDInt]))
				//SET_BLIP_ROTATION(g_PlayerBlipsData.playerBlips[iPlayerIDInt], 0)
			ENDIF
			
		// player is passive
		ELIF (IS_PLAYER_PASSIVE(playerID) AND NOT IS_BIT_SET(GlobalplayerBD[iPlayerIDInt].playerBlipData.iFlags, PBBD_TEMP_PASSIVE_MODE_SET))
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_PASSIVE)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_PASSIVE)
			ENDIF
			
		// player has contraband
		ELIF SHOULD_PLAYER_HAVE_CONTRABAND_BLIP(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_CONTRABAND)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_CONTRABAND)
			ENDIF					

		// player has illicit goods
		ELIF SHOULD_PLAYER_HAVE_ILLICIT_GOODS_BLIP(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_PACKAGE)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_PACKAGE)
				GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_ATTACK_ILLICIT_VEHICLE)
			ENDIF					
		

		// player has vehicle export
		ELIF SHOULD_PLAYER_HAVE_VEHICLE_EXPORT_BLIP(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SPORTS_CAR)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SPORTS_CAR)
				GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_VEHICLE_EXPORT)
			ENDIF					
		
		// player has gunrun
		ELIF SHOULD_PLAYER_HAVE_GUNRUN_BLIP(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SUPPLIES)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SUPPLIES)		
				GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_ATTACK_GUNRUN_VEHICLE)
			ENDIF
		
		// player has smuggler
		ELIF SHOULD_PLAYER_HAVE_SMUGGLER_BLIP(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_CARGO)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_CARGO)
				
				IF NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), playerID)
					IF GB_SHOULD_RIVAL_SMUGGLER_ENTITY_BE_BLIPPED_TO_STEAL(playerID)
						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_STEAL_SMUGGLER_ENTITY)
						GB_SMUGGLER_SETUP_RIVAL_PLAYER_STEAL_FLOW_PHONECALL()
					ELSE
						GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_ATTACK_SMUGGLER_VEHICLE)
						GB_SMUGGLER_SETUP_RIVAL_PLAYER_DESTROY_FLOW_PHONECALL()
					ENDIF
				ENDIF
			ENDIF
		
		// player is fmheist
		ELIF SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  GET_GANGOPS_ENTITY_BLIP_SPRITE(TRUE))
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, GET_GANGOPS_ENTITY_BLIP_SPRITE(TRUE))
				
				IF NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), playerID)
					GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_STEAL_FM_GANGOPS_ENTITY)
				ENDIF
			ENDIF			
		
		ELIF SHOULD_PLAYER_HAVE_FMBB_BLIP(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  GET_BUSINESS_BATTLE_ENTITY_BLIP_SPRITE(TRUE))
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, GET_BUSINESS_BATTLE_ENTITY_BLIP_SPRITE(TRUE))
				
				IF NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), playerID)
					GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_STEAL_FMBB_ENTITY)
				ENDIF
			ENDIF			
		ELIF SHOULD_PLAYER_HAVE_RANDOM_EVENT_BLIP(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  GET_RANDOM_EVENT_BLIP_SPRITE())
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, GET_RANDOM_EVENT_BLIP_SPRITE())
			ENDIF
		#IF FEATURE_CASINO_HEIST
		ELIF SHOULD_PLAYER_HAVE_CASINO_HEIST_BLIP(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  GET_CASINO_HEIST_ENTITY_BLIP_SPRITE())
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, GET_CASINO_HEIST_ENTITY_BLIP_SPRITE())
				
				IF NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), playerID)
					GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_STEAL_FM_GANGOPS_ENTITY)
				ENDIF
			ENDIF
		#ENDIF
		ELIF SHOULD_PLAYER_HAVE_FM_CONTENT_BLIP(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  GET_FM_CONTENT_ENTITY_BLIP_SPRITE(playerID))
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, GET_FM_CONTENT_ENTITY_BLIP_SPRITE(playerID))
				
				IF NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), playerID)
					SET_FM_CONTENT_RIVAL_NOTIFICATION_HELP_BIT(playerID)
				ENDIF
			ENDIF
		
		// player is ballistic
		ELIF IS_BIT_SET(GlobalplayerBD[iPlayerIDInt].playerBlipData.iFlags, PBBD_IS_BALLISTIC) 
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_RAMPAGE)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_RAMPAGE)
			ENDIF	
			
		ELIF SHOULD_GIVE_MOC_SPRITE(PlayerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_GR_COVERT_OPS)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_GR_COVERT_OPS)
			ENDIF	
		
		ELIF SHOULD_GIVE_HACKER_TRUCK_SPRITE(PlayerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_HACKER_TRUCK_MODEL()))
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_HACKER_TRUCK_MODEL()))
			ENDIF
		
		ELIF SHOULD_GIVE_ARMORY_AIRCRAFT_SPRITE(PlayerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(AVENGER))
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(AVENGER))
			ENDIF
			
		ELIF SHOULD_GIVE_SUBMARINE_SPRITE(PlayerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(KOSATKA))
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(KOSATKA))
			ENDIF
			
			PLAYER_INDEX SubOwner = GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(playerID)
			IF SubOwner != INVALID_PLAYER_INDEX()
				SET_BLIP_ROTATION_WITH_FLOAT(g_PlayerBlipsData.playerBlips[iPlayerIDInt], GlobalplayerBD_FM_4[NATIVE_TO_INT(SubOwner)].fSubmarineHeading)
			ENDIF
			
		// player has bounty
		ELIF SHOULD_PLAYER_HAVE_BOUNTY_BLIP(playerID)
		OR IS_THIS_PLAYER_POWER_PLAYER(playerID)
			IF IS_PLAYER_IN_PROPERTY_THAT_I_AM_NOT(PlayerID)
			OR IS_USING_FAKE_PROPERTY_BLIP(PlayerID)
				IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_BOUNTY_HIT_INSIDE)
					MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_BOUNTY_HIT_INSIDE)
				ENDIF				
			ELSE
				IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_BOUNTY_HIT)
					MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_BOUNTY_HIT)
				ENDIF		
			ENDIF
			
		// player is on pause menu
		ELIF IS_PLAYER_ON_PAUSE_MENU(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_USINGMENU)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_USINGMENU)
			ENDIF
		
		// player is inside property
		ELIF (IS_PLAYER_IN_PROPERTY_THAT_I_AM_NOT(PlayerID , TRUE ) 
		OR IS_USING_FAKE_PROPERTY_BLIP(PlayerID))
		#IF FEATURE_DLC_1_2022
		AND NOT IS_PLAYER_IN_SIMEON_SHOWROOM(PlayerID)
		#ENDIF	
		
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_LEVEL_INSIDE)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_LEVEL_INSIDE)
			ENDIF
			
		// player in a vehicle
		ELIF GlobalplayerBD[iPlayerIDInt].playerBlipData.iInVehicleType != 0 AND NOT bInMyPV
		
			SWITCH GlobalplayerBD[iPlayerIDInt].playerBlipData.iInVehicleType
				CASE PBBD_IS_IN_TANK
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_TANK)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_TANK)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_JET
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_PLAYER_JET)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_PLAYER_JET)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_AIRPLANE
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_PLAYER_PLANE)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_PLAYER_PLANE)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_HELI
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_PLAYER_HELI)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_PLAYER_HELI)
					ENDIF		
				BREAK
				CASE PBBD_IS_IN_GUNTURRET
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_PLAYER_GUNCAR)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_PLAYER_GUNCAR)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_BOAT
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_PLAYER_BOAT)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_PLAYER_BOAT)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_VOLTIC2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_EX_VECH_6)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_EX_VECH_6)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_TECHNICAL2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_EX_VECH_7)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_EX_VECH_7)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_RUINER2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_EX_VECH_3)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_EX_VECH_3)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_DUNE4
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_EX_VECH_4)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_EX_VECH_4)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_PHANTOM2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_EX_VECH_1)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_EX_VECH_1)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_BOXVILLE5
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_EX_VECH_2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_EX_VECH_2)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_WASTELANDER
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_EX_VECH_5)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_EX_VECH_5)
					ENDIF		
				BREAK
				CASE PBBD_IS_IN_QUAD
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_QUAD)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_QUAD)
					ENDIF				
				BREAK
				
				CASE PBBD_IS_IN_APC
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_GR_WVM_1)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_GR_WVM_1)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_OPPRESSOR
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_GR_WVM_2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_GR_WVM_2)
					ENDIF	
				BREAK
				CASE PBBD_IS_IN_HALFTRACK
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_GR_WVM_3)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_GR_WVM_3)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_DUNE3
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_GR_WVM_4)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_GR_WVM_4)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_TAMPA3
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_GR_WVM_5)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_GR_WVM_5)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_TRAILERSMALL2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_GR_WVM_6)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_GR_WVM_6)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_ALPHAZ1
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP1)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP1)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_BOMBUSHKA
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP2)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_HAVOK
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP3)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP3)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_HOWARD
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP4)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP4)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_HUNTER
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP5)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP5)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_MICROLIGHT
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP6)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP6)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_MOGUL
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP7)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP7)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_MOLOTOK
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP8)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP8)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_NOKOTA
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP9)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP9)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_PYRO
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP10)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP10)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_ROGUE
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP11)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP11)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_STARLING
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP12)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP12)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_SEABREEZE
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP13)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP13)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_TULA
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SM_WP14)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SM_WP14)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_STROMBERG
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_NHP_WP1)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_NHP_WP1)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_DELUXO
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_NHP_WP2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_NHP_WP2)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_THRUSTER
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_NHP_WP3)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_NHP_WP3)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_KHANJALI
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_NHP_WP4)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_NHP_WP4)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_RIOT2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_NHP_WP5)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_NHP_WP5)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_VOLATOL
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_NHP_WP6)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_NHP_WP6)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_BARRAGE
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_NHP_WP7)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_NHP_WP7)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_AKULA
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_NHP_WP8)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_NHP_WP8)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_CHERNOBOG
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_NHP_WP9)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_NHP_WP9)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_AVENGER
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_NHP_VEH1)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_NHP_VEH1)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_TURRETED_LIMO
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_TURRETED_LIMO)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_TURRETED_LIMO)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_SEASPARROW
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ACSR_WP1)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ACSR_WP1)
					ENDIF
				BREAK
				CASE PBBD_IS_IN_CARACARA
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ACSR_WP2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ACSR_WP2)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_PBUS
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_BAT_PBUS)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_BAT_PBUS)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_TERBYTE
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_BAT_WP1)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_BAT_WP1)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_MENACER
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_BAT_WP2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_BAT_WP2)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_SCRAMJET
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_BAT_WP3)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_BAT_WP3)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_POUNDER2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_BAT_WP4)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_BAT_WP4)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_MULE4
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_BAT_WP5)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_BAT_WP5)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_SPEEDO4
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_BAT_WP6)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_BAT_WP6)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_STRIKEFORCE
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_BAT_WP7)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_BAT_WP7)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_OPPRESSOR2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_OPPRESSOR_2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_OPPRESSOR_2)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_BRUISER
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_BRUISER)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_BRUISER)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_BRUTUS
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_BRUTUS)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_BRUTUS)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_CERBERUS
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_CERBERUS)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_CERBERUS)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_DEATHBIKE
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_DEATHBIKE)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_DEATHBIKE)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_DOMINATOR
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_DOMINATOR)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_DOMINATOR)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_IMPALER
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_IMPALER)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_IMPALER)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_IMPERATOR
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_IMPERATOR)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_IMPERATOR)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_ISSI
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_ISSI)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_ISSI)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_SASQUATCH
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_SASQUATCH)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_SASQUATCH)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_SCARAB
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_SCARAB)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_SCARAB)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_SLAMVAN
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_SLAMVAN)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_SLAMVAN)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_ZR380
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_ZR380)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_ZR380)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_KOSATKA
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SUB2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SUB2)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_AVISA
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_MINI_SUB)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_MINI_SUB)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_SEASPARROW2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SEASPARROW2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SEASPARROW2)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_ALKONOST
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_FOLDING_WING_JET)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_FOLDING_WING_JET)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_MANCHEZ2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_GANG_BIKE)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_GANG_BIKE)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_VERUS
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_MILITARY_QUAD)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_MILITARY_QUAD)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_SQUADDIE
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_SQUADEE)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_SQUADEE)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_DINGHY5
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_DINGHY2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_DINGHY2)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_WINKY
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_WINKY)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_WINKY)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_PATROL_BOAT
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_PATROL_BOAT)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_PATROL_BOAT)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_ANNIHILATOR2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_VALYKRIE2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_VALYKRIE2)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_VETO
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_KART_RETRO)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_KART_RETRO)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_VETO2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_KART_MODERN)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_KART_MODERN)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_VETIR
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_MILITARY_TRUCK)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_MILITARY_TRUCK)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_CHAMPION
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_D_CHAMPION)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_D_CHAMPION)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_BUFFALO4
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_BUFFALO_4)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_BUFFALO_4)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_DEITY
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_DEITY)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_DEITY)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_JUBILEE
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_JUBILEE)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_JUBILEE)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_GRANGER2
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_GRANGER2)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_GRANGER2)
					ENDIF
				BREAK
				
				CASE PBBD_IS_IN_PATRIOT3
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_PATRIOT3)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_PATRIOT3)
					ENDIF
				BREAK
				
				#IF FEATURE_DLC_2_2022
				CASE PBBD_IS_IN_DUSTER
					IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARMS_DEALING_AIR)
						MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARMS_DEALING_AIR)
					ENDIF
				BREAK
				#ENDIF
				
				DEFAULT
					PRINTLN("SET_CORRECT_PLAYER_BLIP_SPRITE: Missing care for vehicle type. iInVehicleType = ", GlobalplayerBD[iPlayerIDInt].playerBlipData.iInVehicleType)
					SCRIPT_ASSERT("SET_CORRECT_PLAYER_BLIP_SPRITE: Missing care for vehicle type.")
				BREAK
			ENDSWITCH
		ELIF IS_PLAYER_USING_RCBANDITO(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_ARENA_RC_CAR)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_ARENA_RC_CAR)
			ENDIF
		#IF FEATURE_CASINO_HEIST
		ELIF IS_PLAYER_USING_RC_TANK(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) = RADAR_TRACE_RC_TANK)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_RC_TANK)
			ENDIF
		#ENDIF
		// player is default
		ELIF GB_IS_PLAYER_BOSS_OF_A_GANG(playerID)
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  RADAR_TRACE_INCAPACITATED)
				MAKE_PLAYERS_BLIP_THIS_SPRITE(playerID, RADAR_TRACE_INCAPACITATED)
			ENDIF
		
		ELSE
			IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) =  GET_STANDARD_BLIP_ENUM_FOR_PLAYER(PlayerID))
				RESTORE_BLIP_TO_DEFAULT_SPRITE(PlayerID)
				
				
			ENDIF
		ENDIF
				
	ENDIF
	
	
	
	UPDATE_MANUALLY_ROTATATED_BLIP_FOR_PLAYER(PlayerID)

	
	SET_PLAYER_BLIP_COLOUR(PlayerID, g_PlayerBlipsData.playerBlips[iPlayerIDInt])
	
	
	// Set bits for alpha AFTER we've set the sprite.
	IF IS_PLAYER_IN_VEHICLE_THAT_HAS_A_BLIP(PlayerID)
		IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayerInVehicleThatHasBlip, iPlayerIDInt)
			SET_BIT(g_PlayerBlipsData.bs_PlayerInVehicleThatHasBlip, iPlayerIDInt)
			CPRINTLN(DEBUG_BLIP, "[player_blips] set player ", iPlayerIDInt, " alpha 0 as in vehicle with blip") 	
		ENDIF
	ELSE
		// should we set the alpha to 0 of non-leaders in vehicles? 2027635
		IF SHOULD_BLIP_ONLY_SHOW_LEADERS_WHEN_IN_VEHICLE(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
		AND NOT SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(PlayerID, TRUE)
			IF NOT IS_BIT_SET(GlobalplayerBD[iPlayerIDInt].playerBlipData.iFlags, PBBD_IS_LEADER_OF_VEHICLE)
				IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerIDInt)
					SET_BIT(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerIDInt)
					CPRINTLN(DEBUG_BLIP, "[player_blips] set player ", iPlayerIDInt, " alpha 0 as non-leader in vehicle") 	
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
		
	
ENDPROC

FUNC INT GetCasinoFloorFromZ(FLOAT z)
	IF z < -47.2
		RETURN 0
	ENDIF
	RETURN 1
ENDFUNC

PROC SET_CORRECT_PLAYER_BLIP_FORMAT(PLAYER_INDEX playerID)


	INT iPlayerIDInt
	iPlayerIDInt = NATIVE_TO_INT(playerID)
	
	IF NOT (iPlayerIDInt = -1)
	AND DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])

		BOOL bHideBlipFromRadar
		bHideBlipFromRadar = FALSE
		
		BOOL bIsOnRoof 
		bIsOnRoof = FALSE
				
		// if the player has an arrow sprite (from being spectated) then restore.
		IF (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) = INT_TO_ENUM(BLIP_SPRITE, 6))
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SET_CORRECT_PLAYER_BLIP_FORMAT - restoring to default blip - player ", iPlayerIDInt)
				ENDIF
			#ENDIF
			RESTORE_BLIP_TO_DEFAULT_SPRITE(playerID)	
		ENDIF
		
		// what game mode?
		SET_CORRECT_PLAYER_BLIP_SPRITE(PlayerID)	
		
		// check it has the correct range
		IF IS_BLIP_SHORT_RANGE(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayersBlipIsLongRange, iPlayerIDInt)
				SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.playerBlips[iPlayerIDInt], FALSE)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SET_CORRECT_PLAYER_BLIP_FORMAT - restoring to correct range (long) - player ", iPlayerIDInt)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayersBlipIsLongRange, iPlayerIDInt)
				SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.playerBlips[iPlayerIDInt], TRUE)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SET_CORRECT_PLAYER_BLIP_FORMAT - restoring to correct range (short) - player ", iPlayerIDInt)				
			ENDIF			
		ENDIF

		// is he in the same vehicle as the local player?
		IF IS_NET_PLAYER_OK( MPGlobals.LocalPlayerID )
		AND IS_NET_PLAYER_OK( playerID )	
			IF ARE_PLAYER_IN_SAME_VEHICLE( MPGlobals.LocalPlayerID, playerID )
			AND NOT IS_BIT_SET(GlobalplayerBD[iPlayerIDInt].playerBlipData.iFlags, PBBD_IS_SELF_HIGHLIGHTED)
			AND NOT SHOULD_PLAYER_HAVE_BOUNTY_BLIP(playerID)
			AND NOT IS_PLAYER_USING_CUSTOM_SPRITE_BLIP(playerID)
			AND NOT SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(playerID)			
				bHideBlipFromRadar = TRUE
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SET_CORRECT_PLAYER_BLIP_FORMAT - bHideBlipFromRadar TRUE , player =", iPlayerIDInt)
					ENDIF
				#ENDIF				
			ENDIF			
		ENDIF
		
		// hide blips of players who are on same office roof. 2823590
		IF IS_PLAYER_IN_OFFICE_PROPERTY(MPGlobals.LocalPlayerID)
			IF NOT (GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty = GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iCurrentlyInsideProperty)
				IF IS_PLAYER_ON_OFFICE_ROOF(playerID, GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iCurrentlyInsideProperty)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SET_CORRECT_PLAYER_BLIP_FORMAT - player is on office roof - player ", iPlayerIDInt, " property ", GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iCurrentlyInsideProperty)
					bIsOnRoof = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		// hide blips of player in underground carpark of arcadius.
		IF (GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iCurrentlyInsideProperty = PROPERTY_OFFICE_3_GARAGE_LVL1)
		OR (GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iCurrentlyInsideProperty = PROPERTY_OFFICE_3_GARAGE_LVL2)
		OR (GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iCurrentlyInsideProperty = PROPERTY_OFFICE_3_GARAGE_LVL3)			
			IF NOT (GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty = GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iCurrentlyInsideProperty)				
				IF IS_ENTITY_IN_ANGLED_AREA (GET_PLAYER_PED(playerID), <<-181.591, -663.244, 24.799>>, <<-146.266, -566.319, 39.349>>, 88.375)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SET_CORRECT_PLAYER_BLIP_FORMAT - player is in underground car park - player ", iPlayerIDInt, " property ", GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].propertyDetails.iCurrentlyInsideProperty)
					bIsOnRoof = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		// if player is inside the casino, hide players who are inside but different Z (i.e management area) url:bugstar:5866661
		IF IS_PLAYER_IN_CASINO(MPGlobals.LocalPlayerID)
		AND IS_PLAYER_IN_CASINO(playerID)
			VECTOR vPos1 = GET_PLAYER_COORDS(MPGlobals.LocalPlayerID)
			VECTOR vPos2 = GET_PLAYER_COORDS(playerID)		
			// check the local player is not in the stairwell first (in which case he should see both)
			IF NOT IS_POINT_IN_ANGLED_AREA(vPos1, <<1110.15, 267.225, -43.6>>, <<1122.1, 267.225, -52.1>>, 8.025)
				IF (GetCasinoFloorFromZ(vPos1.z) != GetCasinoFloorFromZ(vPos2.z))
					CPRINTLN(DEBUG_BLIP, "[player_blips] SET_CORRECT_PLAYER_BLIP_FORMAT - player is different floor of casino, player ", iPlayerIDInt)
					bIsOnRoof = TRUE		
				ENDIF
			ENDIF
		ENDIF
		
		// hide blips 
		

		// Don't blip people in the same vehicle, it looks shit.
		#IF IS_DEBUG_BUILD
			IF NOT (g_PlayerBlipsData.bDisableSetBlipDisplay)
		#ENDIF
			IF (bIsOnRoof)
				IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_PAUSE_MENU_ACTIVE)
					OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_BIG_MAP_IS_ACTIVE))
				AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_PAUSE_MENU_MAP_INTERIOR_MODE)
					SET_BLIP_DISPLAY(g_PlayerBlipsData.playerBlips[iPlayerIDInt], DISPLAY_MAP)
				ELSE
					SET_BLIP_DISPLAY(g_PlayerBlipsData.playerBlips[iPlayerIDInt], DISPLAY_NOTHING)
				ENDIF
			ELSE
				IF (bHideBlipFromRadar)	
					//SET_BLIP_DISPLAY(g_PlayerBlipsData.playerBlips[iPlayerIDInt], DISPLAY_NOTHING)
					SET_BLIP_DISPLAY(g_PlayerBlipsData.playerBlips[iPlayerIDInt], DISPLAY_MAP)
				ELSE
					// if player is in a mission corona, don't show other players on the mini-map
					IF IS_PLAYER_IN_CORONA()
					OR (ARE_TEMP_SHORT_RANGE_BLIPS_ACTIVE() AND NOT IS_PLAYER_INSIDE_TRUCK_IN_BUNKER_I_AM_IN(playerID)  AND NOT IS_PLAYER_INSIDE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE_I_AM_IN(playerID)   AND NOT IS_PLAYER_INSIDE_HACKER_TRUCK_IN_BUSINESS_HUB_I_AM_IN(playerID) )
						SET_BLIP_DISPLAY(g_PlayerBlipsData.playerBlips[iPlayerIDInt], DISPLAY_MAP)
					ELSE
						SET_BLIP_DISPLAY(g_PlayerBlipsData.playerBlips[iPlayerIDInt], DISPLAY_BOTH)
					ENDIF
				ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
			ENDIF
		#ENDIF
		
		// if player is in car with multiple players then show number on blip
		#IF IS_DEBUG_BUILD
			IF NOT (g_PlayerBlipsData.bDisableShowNumberOnBlip)
		#ENDIF
		
		IF IS_PLAYER_USING_CUSTOM_BLIP_NUMBER(playerID)
			SHOW_NUMBER_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], GET_PLAYERS_CUSTOM_BLIP_NUMBER(playerID))
		ELSE
			IF (GlobalplayerBD[iPlayerIDInt].playerBlipData.iNumberOfPlayersInMyCar > 1)
			AND CAN_BLIP_SPRITE_HAVE_HEADING_INDICATOR(GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]))
				SHOW_NUMBER_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt], GlobalplayerBD[iPlayerIDInt].playerBlipData.iNumberOfPlayersInMyCar)
			ELSE
				HIDE_NUMBER_ON_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt])	
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			ENDIF
		#ENDIF
		
		UPDATE_CORRECT_BLIP_PRIORITY_FOR_PLAYER(playerID)
		
	ENDIF
ENDPROC



FUNC FLOAT GET_CORRECT_BLIP_SCALE_FOR_PLAYER(INT PlayerInt)

	PLAYER_INDEX PlayerID = INT_TO_NATIVE(PLAYER_INDEX, PlayerInt)

	IF IS_PLAYER_USING_FIXED_BLIP_SCALE(PlayerID)
		RETURN g_PlayerBlipsData.fPlayerFixedScale[PlayerInt]
	ENDIF	
	
#IF FEATURE_FREEMODE_ARCADE
	IF IS_FREEMODE_ARCADE()
	#IF FEATURE_COPS_N_CROOKS	
		IF GET_ARCADE_MODE() = ARC_COPS_CROOKS		
			IF GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.bIsClosestEnemyPlayer = 1
				RETURN 1.5
			ENDIF
		ENDIF
	#ENDIF
	ENDIF
#ENDIF
	
	// if contraband player use fixed scale of 1.0 
	IF SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(PlayerID)
		IF SHOULD_PLAYER_HAVE_FM_CONTENT_BLIP(PlayerID)
			RETURN GET_FM_CONTENT_ENTITY_BLIP_SCALE(PlayerID)
		ENDIF
		IF SHOULD_PLAYER_HAVE_ILLICIT_GOODS_BLIP(PlayerID)
			RETURN 1.2
		ENDIF
		IF SHOULD_PLAYER_HAVE_GUNRUN_BLIP(PlayerID)
			RETURN 0.8
		ENDIF
		IF SHOULD_PLAYER_HAVE_SMUGGLER_BLIP(PlayerID)
			RETURN GET_SMUGGLER_ENTITY_BLIP_SCALE()
		ENDIF
		IF SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP(PlayerID)
			RETURN GET_GANGOPS_ENTITY_BLIP_SCALE()
		ENDIF
		IF SHOULD_PLAYER_HAVE_FMBB_BLIP(PlayerID)
			RETURN GET_BUSINESS_BATTLE_ENTITY_BLIP_SCALE()
		ENDIF
		IF SHOULD_PLAYER_HAVE_RANDOM_EVENT_BLIP(PlayerID)
			RETURN GET_RANDOM_EVENT_BLIP_SCALE()
		ENDIF
		#IF FEATURE_CASINO_HEIST
		IF SHOULD_PLAYER_HAVE_CASINO_HEIST_BLIP(PlayerID)
			RETURN GET_CASINO_HEIST_ENTITY_BLIP_SCALE()
		ENDIF
		#ENDIF
		RETURN 1.0
	ENDIF
	
	// if the blip is using a custom number then increase size so it is legible. fix for 3159903
	IF IS_PLAYER_USING_CUSTOM_BLIP_NUMBER(PlayerID)
		RETURN 1.0
	ENDIF
	
	FLOAT fScale = BLIP_SIZE_NETWORK_PLAYER
		
	// Make blip scales tiny for players when their blips are 
	// hidden as passengers or while occupying blipped vehicles.
	// This means they don't appear on the pause menu map but do
	// appear in the pause menu legend.
	IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerInVehicleThatHasBlip, PlayerInt)
	OR IS_BIT_SET(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, PlayerInt)
		fScale *= BLIP_SCALE_MULTIPLIER_FOR_HIDDEN_PAUSE_BLIPS
	ELSE
		// if in a tank use a base scale of 1.0
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[PlayerInt])
			IF NOT IS_PAUSE_MENU_ACTIVE()
				fScale = GET_BASE_SCALE_FOR_BLIP_SPRITE(GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[PlayerInt]), BLIP_SIZE_NETWORK_PLAYER)
			ELSE
				// scale down further for non standard blips
				IF NOT (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[PlayerInt]) = GET_STANDARD_BLIP_ENUM_ID())
					fScale *= BLIP_SCALE_MULTIPLIER_FOR_NON_STANDARD_BLIPS
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PAUSE_MENU_ACTIVE()
			
			IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[PlayerInt])
			AND GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[PlayerInt]) != RADAR_TRACE_SUB2 //url:bugstar:6822741
				fScale *= GET_DIST_SCALE_MULTIPLIER(g_PlayerBlipsData.playerBlips[PlayerInt])
			ENDIF
			
			// is this player highlighted? if so scale up
			IF NOT (PlayerInt = -1)
				IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerBlipHighlighted, PlayerInt)
					fScale *= HIGHLIGHTED_SCALE_UP
				ENDIF
			ENDIF
			
			// is this player in a race? if so make them 20% smaller
			IF NOT (PlayerInt = -1)
				IF IS_PLAYER_ON_ANY_RACE(PlayerID)
					fScale *= RACE_SCALE_UP
				ENDIF
			ENDIF	
		

		ELSE
		
			fScale *= PAUSE_MENU_SCALE_MUTLIPLIER
		ENDIF
		
		// Resize Boss Blip for Gang Boss DLC
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(PlayerID)
			IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[PlayerInt])
				IF (GET_BLIP_SPRITE(g_PlayerBlipsData.playerBlips[PlayerInt]) =  RADAR_TRACE_INCAPACITATED)
					fScale = BLIP_SIZE_NETWORK_PLAYER
					IF NOT IS_PAUSE_MENU_ACTIVE()
						fScale -= 0.1
					ELSE
						fScale += 0.2
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN(fScale)
	
ENDFUNC

FUNC GB_CONTRABAND_TYPE GET_CONTRA_TYPE(INT iPlayerInt)

	GB_CONTRABAND_TYPE eContrabandDeliveryType = GBCT_CEO	
	
	IF SHOULD_PLAYER_HAVE_ILLICIT_GOODS_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE) 
		eContrabandDeliveryType = GBCT_BIKER
	ELIF SHOULD_PLAYER_HAVE_VEHICLE_EXPORT_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
		eContrabandDeliveryType = GBCT_EXPORT
	ELIF SHOULD_PLAYER_HAVE_GUNRUN_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
		eContrabandDeliveryType = GBCT_GUNRUN
	ELIF SHOULD_PLAYER_HAVE_SMUGGLER_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
		eContrabandDeliveryType = GBCT_SMUGGLER
	ELIF SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
		eContrabandDeliveryType = GBCT_FM_GANGOPS
	ELIF SHOULD_PLAYER_HAVE_FMBB_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
		eContrabandDeliveryType = GBCT_FMBB
	#IF FEATURE_CASINO_HEIST
	ELIF SHOULD_PLAYER_HAVE_CASINO_HEIST_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
		eContrabandDeliveryType = GBCT_CASINO_HEIST
	#ENDIF
	ELIF SHOULD_PLAYER_HAVE_FM_CONTENT_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
		eContrabandDeliveryType = GET_FM_CONTENT_CONTRABAND_TYPE(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt))
	ENDIF
	
	RETURN eContrabandDeliveryType
ENDFUNC

PROC SET_PLAYER_BLIP_SCALE_AND_ALPHA(INT iPlayerInt)

	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerInt])
		
		#IF IS_DEBUG_BUILD
			IF NOT (g_PlayerBlipsData.bDisableScale)
		#ENDIF
		SET_BLIP_SCALE(g_PlayerBlipsData.playerBlips[iPlayerInt], GET_CORRECT_BLIP_SCALE_FOR_PLAYER(iPlayerInt))
		#IF IS_DEBUG_BUILD
			ENDIF 
		#ENDIF
		
		IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayerInVehicleThatHasBlip, iPlayerInt)
		AND NOT IS_BIT_SET(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerInt)	
		
			IF NOT IS_PLAYER_BLIP_ALPHA_CONTROLLED_BY_NATIVE(iPlayerInt)
				
				IF IS_PLAYER_USING_CUSTOM_BLIP_ALPHA(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt))
					SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[iPlayerInt], g_PlayerBlipsData.iCustomBlipAlpha[iPlayerInt])
				ELSE
				
					IF IS_NEXT_GEN()
					AND NOT (IS_PLAYER_FORCE_BLIPPED(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt)) AND IS_PLAYER_BLIP_SET_AS_LONG_RANGE(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt)))
					AND NOT GB_ARE_PLAYERS_IN_SAME_GANG(MPGlobals.LocalPlayerID, INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt))
					AND NOT IS_PLAYERS_TEAM_HIGHLIGHTED(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt))
					AND (NOT SHOULD_PLAYER_HAVE_SHIPMENT_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt)) 
							OR SHOULD_PLAYER_HAVE_ILLICIT_GOODS_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE) 
							OR SHOULD_PLAYER_HAVE_VEHICLE_EXPORT_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
							OR SHOULD_PLAYER_HAVE_GUNRUN_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
							OR SHOULD_PLAYER_HAVE_SMUGGLER_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
							OR SHOULD_PLAYER_HAVE_FM_GANGOPS_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
							OR SHOULD_PLAYER_HAVE_FMBB_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
							OR SHOULD_PLAYER_HAVE_RANDOM_EVENT_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
							#IF FEATURE_CASINO_HEIST
							OR SHOULD_PLAYER_HAVE_CASINO_HEIST_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE)
							#ENDIF
							OR SHOULD_PLAYER_HAVE_FM_CONTENT_BLIP(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt), TRUE))
						
						// is player on 1-1dm?
						IF (g_bFM_ON_IMPROMPTU_DEATHMATCH)
							IF (g_sImpromptuVars.playerImpromptuRival = INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt))
								#IF IS_DEBUG_BUILD
									IF NOT (g_PlayerBlipsData.bDisableAlpha)
								#ENDIF
								SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[iPlayerInt], 255)		
								#IF IS_DEBUG_BUILD
									ENDIF 
								#ENDIF
							ELSE
								SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER_IMPROMPTU(g_PlayerBlipsData.playerBlips[iPlayerInt], 0)
							ENDIF
						ELSE			
							GB_CONTRABAND_TYPE eContrabandDeliveryType = GET_CONTRA_TYPE(iPlayerInt)
							SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(g_PlayerBlipsData.playerBlips[iPlayerInt], 
																		GB_GET_MIN_ALPHA_FOR_REMOTE_BLIP(eContrabandDeliveryType), 
																		GB_GET_MAX_DIST_FOR_REMOTE_BLIP(eContrabandDeliveryType), 
																		GB_GET_MIN_DIST_FOR_REMOTE_BLIP(eContrabandDeliveryType)) 
						ENDIF
			
					ELSE
						#IF IS_DEBUG_BUILD
							IF NOT (g_PlayerBlipsData.bDisableAlpha)
						#ENDIF
						SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[iPlayerInt], 255)	
						#IF IS_DEBUG_BUILD
							ENDIF 
						#ENDIF
					ENDIF
				
				ENDIF
			ENDIF
			
		ELSE
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisableAlpha)
			#ENDIF
				SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[iPlayerInt], BLIP_ALPHA_FOR_PASSENGERS)	
			#IF IS_DEBUG_BUILD
				ENDIF 
			#ENDIF
		ENDIF
		
		
		// if we are using the small vehicle blips, make sure we've set the correct priority.
		IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerInVehicleThatHasBlip, iPlayerInt)
		OR IS_BIT_SET(g_PlayerBlipsData.bs_SetAsInvisibleVehiclePassenger, iPlayerInt)		
			IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_IsUsingSmallVehicleBlip, iPlayerInt)
				CPRINTLN(DEBUG_BLIP, "SET_PLAYER_BLIP_SCALE_AND_ALPHA - setting priority of small vehicle blip")
				SET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt))
				SET_BIT(g_PlayerBlipsData.bs_IsUsingSmallVehicleBlip, iPlayerInt)
			ENDIF	
		ELSE
			IF IS_BIT_SET(g_PlayerBlipsData.bs_IsUsingSmallVehicleBlip, iPlayerInt)
				CPRINTLN(DEBUG_BLIP, "SET_PLAYER_BLIP_SCALE_AND_ALPHA - resetting priority after small vehicle blip")
				SET_CORRECT_BLIP_PRIORITY_FOR_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt))
				CLEAR_BIT(g_PlayerBlipsData.bs_IsUsingSmallVehicleBlip, iPlayerInt)
			ENDIF		
		ENDIF
		
	ENDIF
	
	

	
ENDPROC

FUNC BOOL SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP(PLAYER_INDEX PlayerID)
	
	#IF FEATURE_DLC_1_2022
	IF IS_PLAYER_IN_SIMEON_SHOWROOM(PlayerID)
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_USING_ARENA()
		IF IS_PLAYER_INSIDE_ARENA(PlayerID)
			IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)			
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP - player in arena and zoomed out map =", NATIVE_TO_INT(playerID))
					ENDIF
				#ENDIF				
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF		
	
	// other player is in a property
	IF IS_PLAYER_IN_PROPERTY(playerID, TRUE)
	AND (GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty  > 0)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP - player in property =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF	
		
		// if player is in the yacht, but his bd has not yet caught up.
		IF IS_PLAYER_IN_YACHT_PROPERTY(playerID)
			IF NOT IS_PRIVATE_YACHT_ID_VALID(GET_YACHT_PLAYER_IS_ON(playerID)) 
				RETURN(FALSE)
			ENDIF
		ENDIF
	
		// the property is displaced
		//IF IS_PLAYER_IN_DISPLACED_PROPERTY(playerID)
		
			// we're not in the same property
			IF NOT ARE_PLAYERS_IN_SAME_PROPERTY(PlayerID, MPGlobals.LocalPlayerID, TRUE)
			
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP - player not in same property =", NATIVE_TO_INT(playerID))
					ENDIF
				#ENDIF	
			
				RETURN(TRUE)
			ELSE
				
				// we're in the same property but one is in the garage, the other in the appartment.
				IF ARE_PLAYERS_IN_SAME_PROPERTY_BUT_DIFFERENT_INTERIOR(PlayerID, MPGlobals.LocalPlayerID)
				
					#IF IS_DEBUG_BUILD
						IF (g_PlayerBlipsData.bDebugBlipOutput)
							CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP - player same prop, differnt interior =", NATIVE_TO_INT(playerID))
						ENDIF
					#ENDIF	
				
					RETURN(TRUE)
				ELSE
					// we're both in an displaced property, and in expanded map view
					IF IS_PLAYER_IN_DISPLACED_PROPERTY(PlayerID)
					AND IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
						#IF IS_DEBUG_BUILD
							IF (g_PlayerBlipsData.bDebugBlipOutput)
								CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP - player same prop, same interior, displaced =", NATIVE_TO_INT(playerID))
							ENDIF
						#ENDIF	
					
						RETURN(TRUE)
					ENDIF
				ENDIF
				
			ENDIF
		
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP - player in property - nothing hit =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF			
		
		//ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_DISPLACED_HEIST_APPARTMENT(playerID)
		// we're not in the same property
		IF NOT ARE_BOTH_PLAYERS_IN_DISPLACED_HEIST_APPARTMENT(playerID, MPGlobals.LocalPlayerID)
			RETURN(TRUE)
		ELSE
			// we're both in an displaced property, and in expanded map view
			IF IS_PLAYER_IN_DISPLACED_PROPERTY(PlayerID)
			AND IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
				RETURN(TRUE)
			ENDIF		
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PlayerID)
	
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP - player in simple interior =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF		
	
		// we're not in the same simple interior
		IF NOT ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(playerID, MPGlobals.LocalPlayerID, TRUE)
		
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP - player not in same simple interior =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF				
		
			RETURN(TRUE)
		ELSE
			// we're both in an displaced property, and in expanded map view
			IF IS_PLAYER_IN_DISPLACED_PROPERTY(PlayerID)
			AND IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP - player not same simple interior, diplaced =", NATIVE_TO_INT(playerID))
					ENDIF
				#ENDIF				
			
				RETURN(TRUE)
			ENDIF		
		ENDIF	
	ENDIF
	
		
		
	IF IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(PlayerID)
		// we're not isn the same custom displaced interior
		IF NOT ARE_PLAYER_IN_SAME_CUSTOM_DISPLACED_INTERIOR_INSTANCE(playerID, 	MPGlobals.LocalPlayerID)
		
			#IF IS_DEBUG_BUILD
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP - player not in same custom displaced interior =", NATIVE_TO_INT(playerID))
				ENDIF
			#ENDIF		
			
			RETURN TRUE
		
		ELSE
			// we're both in custom displaced interior, and in expanded map view
			IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
				#IF IS_DEBUG_BUILD
					IF (g_PlayerBlipsData.bDebugBlipOutput)
						CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP - player in same custom displaced interior, zoomed out map =", NATIVE_TO_INT(playerID))
					ENDIF
				#ENDIF				
			
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN(FALSE)
	
ENDFUNC


FUNC VECTOR GET_CLUBHOUSE_PLAYER_BLIP_COORDS(INT iPropertyID)
	SWITCH iPropertyID
		CASE PROPERTY_CLUBHOUSE_1_BASE_A	RETURN <<268.5567, -1808.6705, 26.1131>>	BREAK
		CASE PROPERTY_CLUBHOUSE_2_BASE_A	RETURN <<-1487.1534, -926.2145, 9.4102>>	BREAK
		CASE PROPERTY_CLUBHOUSE_3_BASE_A	RETURN <<53.8106, -1027.6407, 29.4261>>		BREAK
		CASE PROPERTY_CLUBHOUSE_4_BASE_A	RETURN <<54.1676, 2793.9036, 57.1350>>		BREAK
		CASE PROPERTY_CLUBHOUSE_5_BASE_A	RETURN  <<-360.0072, 6051.0093, 30.3827>>	BREAK
		CASE PROPERTY_CLUBHOUSE_6_BASE_A	RETURN <<1731.2938, 3698.5935, 33.7762>>	BREAK
		CASE PROPERTY_CLUBHOUSE_7_BASE_B	RETURN <<940.4277, -1478.9342, 30.0416>>	BREAK
		CASE PROPERTY_CLUBHOUSE_8_BASE_B	RETURN <<181.3953, 324.3145, 104.5234>>		BREAK
		CASE PROPERTY_CLUBHOUSE_9_BASE_B	RETURN <<-16.8933, -176.4705, 51.6339>>		BREAK
		CASE PROPERTY_CLUBHOUSE_10_BASE_B	RETURN <<2474.8977, 4099.4268, 37.2816>>	BREAK
		CASE PROPERTY_CLUBHOUSE_11_BASE_B	RETURN <<-49.2044, 6430.9390, 30.8569>>		BREAK
		CASE PROPERTY_CLUBHOUSE_12_BASE_B	RETURN <<-1141.6272, -1558.1749, 3.2943>>	BREAK
	ENDSWITCH
	SCRIPT_ASSERT("GET_CLUBHOUSE_PLAYER_BLIP_COORDS: Called on a non clubhouse property")
	PRINTLN("GET_CLUBHOUSE_PLAYER_BLIP_COORDS: Called on a non clubhouse property ID: ",iPropertyID)
	RETURN <<0,0,0>>
ENDFUNC


FUNC BLIP_INDEX ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER(PLAYER_INDEX PlayerID)
	INT iPlayerIDInt, i
	iPlayerIDInt = NATIVE_TO_INT(playerID)
	VECTOR vCoords
		
	// firstly clear all the data for this blip
	CLEANUP_BLIP( g_PlayerBlipsData.playerBlips[iPlayerIDInt]) // added by neil to cleanup only the blip, but keep any bit sets
	CLEAR_SECONDARY_BLIP_COLOUR_FOR_PLAYER(iPlayerIDInt)
	
	
	IF IS_PLAYER_IN_MOD_SHOP_IN_IE_WAREHOUSE(PlayerID)
	
		g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD(<<959.3535, -2994.5530, -39.9685>>)
		CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - player in ie modshop, player ", iPlayerIDInt)
		SET_BIT(g_PlayerBlipsData.bs_PlayerUsingIEWarehouseModShop, iPlayerIDInt)	
	
	ELSE
	
		IF NOT SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP(PlayerID)

		
			g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_ENTITY(GET_PLAYER_PED(playerID))
			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingPropertyBlip, iPlayerIDInt)
			CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - blip added for player ", iPlayerIDInt)
			
		ELSE	
		
			CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - should use fake property blip  ", iPlayerIDInt) 
		
			IF IS_PLAYER_IN_CUSTOM_DISPLACED_INTERIOR(PlayerID)
				
				vCoords = GlobalplayerBD[iPlayerIDInt].playerBlipData.vDisplacedInteriorPos				
				g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD(vCoords)
				CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - player in custom displaced interior, player ", iPlayerIDInt, " id ", GlobalplayerBD[iPlayerIDInt].playerBlipData.iDisplacedInteriorID, " coords ", vCoords)
				
				IF (GlobalplayerBD[iPlayerIDInt].playerBlipData.fDisplacedInteriorHeading != 0.0)
					SET_BLIP_ROTATION_WITH_FLOAT(g_PlayerBlipsData.playerBlips[iPlayerIDInt], GlobalplayerBD[iPlayerIDInt].playerBlipData.fDisplacedInteriorHeading)
				ENDIF
				
				SET_BIT(g_PlayerBlipsData.bs_PlayerUsingPropertyBlip, iPlayerIDInt)
													
			ELSE
			
				IF IS_PLAYER_IN_DISPLACED_HEIST_APPARTMENT(PlayerID)
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PlayerID)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorSubmarine)
						g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD(<<HEIST_SUB_X, HEIST_SUB_Y, HEIST_SUB_Z>>)
						CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - player in heist appartment, player ", iPlayerIDInt)
					ELSE
						g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD(<<HEIST_APP_X, HEIST_APP_Y, HEIST_APP_Z>>)
						CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - player in heist appartment, player ", iPlayerIDInt)
					ENDIF
					
					SET_BIT(g_PlayerBlipsData.bs_PlayerUsingPropertyBlip, iPlayerIDInt)
					
				ELIF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PlayerID)
						
					vCoords = GET_BLIP_COORD_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PlayerID)
					
					CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - simple interior coords  ", vCoords) 
						
					IF (VMAG(vCoords) > 0.0)
						g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD(vCoords)
					

						CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - blip added for player ", iPlayerIDInt) 
						NET_PRINT(" in simple interior ") NET_PRINT_INT(ENUM_TO_INT(globalPlayerBD[iPlayerIDInt].SimpleInteriorBD.eCurrentSimpleInterior) )
						NET_NL()		
					
						SET_BIT(g_PlayerBlipsData.bs_PlayerUsingPropertyBlip, iPlayerIDInt)	
					ELSE
						CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - simple interior blip coords not ready yet for player ", iPlayerIDInt) 
						RETURN NULL
					ENDIF
				
				ELIF (GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty > 0 AND GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty <= MAX_MP_PROPERTIES)

					CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - iCurrentlyInsideProperty  ", GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty ) 

					IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
					AND NOT IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PlayerID)	
						REPEAT MAX_ENTRANCES i
							IF mpProperties[GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty].entrance[i].iType = ENTRANCE_TYPE_GARAGE
								IF (VMAG(mpProperties[GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty].vBlipLocation[i] ) > 0.0)
									g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD(mpProperties[GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty].vBlipLocation[i])
									CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - blip added for player ", iPlayerIDInt) 
									NET_PRINT(" in garage ") NET_PRINT_INT(GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty) 
									NET_PRINT(" at entrance ") NET_PRINT_INT(i) 
									NET_NL()
									i = MAX_ENTRANCES
								ELSE
									CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - dodgy garage coords for player ", iPlayerIDInt) 
									NET_PRINT(" in garage ") NET_PRINT_INT(GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty) 
									NET_PRINT(" at entrance ") NET_PRINT_INT(i) 
									NET_NL()	
									SCRIPT_ASSERT("ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - trying to add blip for player in garage, but coords are at origin.")
								ENDIF
							ENDIF
						ENDREPEAT
					ELSE
					
						// player is on yacht
						IF IS_PLAYER_IN_YACHT_PROPERTY(playerID)
						AND IS_PRIVATE_YACHT_ID_VALID(GET_YACHT_PLAYER_IS_ON(playerID)) 
							g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD( GET_COORDS_OF_PRIVATE_YACHT( GET_YACHT_PLAYER_IS_ON(playerID) ) )
							CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - blip added for player ", iPlayerIDInt) 
							NET_PRINT(" on yacht ") NET_PRINT_INT(GET_YACHT_PLAYER_IS_ON(playerID)) 
							NET_NL()
						ELSE

							IF IS_PLAYER_IN_OFFICE_PROPERTY(PlayerID)
								g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD(mpProperties[GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty].house.vMidPoint )
								CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - blip added for player ", iPlayerIDInt) 
								NET_PRINT(" in office property ") NET_PRINT_INT(GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty) 
								NET_NL()
							ELSE
								
									IF IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PlayerID)																													
										g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD(GET_CLUBHOUSE_PLAYER_BLIP_COORDS(GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty) )
										CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - blip added for player ", iPlayerIDInt) 
										NET_PRINT(" in clubhouse property ") NET_PRINT_INT(GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty) 
										NET_NL()
									ELSE
				
										IF (VMAG(mpProperties[GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty].vBlipLocation[0]) > 0.0)
											g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD(mpProperties[GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty].vBlipLocation[0] )//entrance[globalPropertyEntryData.iEntrance].vInPlayerLoc)
											CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - blip added for player ", iPlayerIDInt) 
											NET_PRINT(" in property ") NET_PRINT_INT(GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty) 
											NET_NL()
										ELSE
											CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - dodgy property coords for player ", iPlayerIDInt) 
											NET_PRINT(" in property ") NET_PRINT_INT(GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty) 
											NET_NL()	
											SCRIPT_ASSERT("ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - trying to add blip for player in property, but coords are at origin.")
										ENDIF
				
									ENDIF																

							ENDIF
								
						ENDIF
					ENDIF
					
					SET_BIT(g_PlayerBlipsData.bs_PlayerUsingPropertyBlip, iPlayerIDInt)
					
				ELIF (GlobalplayerBD[iPlayerIDInt].playerBlipData.bInsideDisplacedInterior)
					
					vCoords = GlobalplayerBD[iPlayerIDInt].playerBlipData.vDisplacedPerceivedCoords				
					g_PlayerBlipsData.playerBlips[iPlayerIDInt] = ADD_BLIP_FOR_COORD(vCoords)
					CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - bInsideDisplacedInterior, player ", iPlayerIDInt," coords ", vCoords)
					SET_BIT(g_PlayerBlipsData.bs_PlayerUsingPropertyBlip, iPlayerIDInt)
					
				ELSE
					
					CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - trying to add blip but player in invalid property. Player ", iPlayerIDInt) 
					NET_PRINT(", property ") NET_PRINT_INT(GlobalplayerBD_FM[iPlayerIDInt].propertyDetails.iCurrentlyInsideProperty) 
					NET_NL()
					SCRIPT_ASSERT("ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - trying to add blip but player in invalid property.")
					
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF // IS_PLAYER_IN_MOD_SHOP_IN_IE_WAREHOUSE
	
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
	
		SET_BIT(g_PlayerBlipsData.bs_BlipCreatedThisFrame, iPlayerIDInt)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
			DEBUG_PRINTCALLSTACK()
			CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - ADD_BLIP_FOR_ENTITY called from ", GET_THIS_SCRIPT_NAME(), " new blip id = ", NATIVE_TO_INT(g_PlayerBlipsData.playerBlips[iPlayerIDInt]))			
			ENDIF
			
			vCoords = GET_ACTUAL_BLIP_COORDS(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - blip added for player ", iPlayerIDInt, ", at coords ", vCoords)			

		#ENDIF		
		
		
		IF NOT DO_PLAYERS_HAVE_SAME_TEAM_COLOURS( MPGlobals.LocalPlayerID, playerID )
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].playerBlipData.iFlags, PBBD_HAS_CONTROL_ON)			

				IF IS_PLAYER_ON_ANY_FM_MISSION(MPGlobals.LocalPlayerID)
					IF NOT IS_PLAYER_ON_ANY_RACE(MPGlobals.LocalPlayerID)
						IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GlobalplayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].LastRespawnTime) > 2000)
							IF (GlobalplayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].iRespawnState = RESPAWN_STATE_PLAYING)
								IF NOT IS_RADAR_FLASH_DISABLED_BY_MISSION_CONTROLLER()
									FLASH_MINIMAP_DISPLAY_WITH_COLOR(HUD_COLOUR_RED)
								ELSE
									CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - dont flash radar - disabled by mission controller ")
								ENDIF
							ELSE
								CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - dont flash radar - iRespawnState ")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - dont flash radar - time diff ")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - dont flash radar - on race ")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - dont flash radar - not on mission ")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - dont flash radar - control not on ")
			ENDIF
		ENDIF
		
		
		SET_PLAYER_BLIP_INITIAL_VALUES(PlayerID)
		
		SET_PLAYER_BLIP_AS_LONG_RANGE(playerID, TRUE)
		
		// set the secondary colour of the players blips
		UPDATE_INDICATORS_ON_PLAYERS_BLIP(playerID)
		
		// if the gps is supposed to be on then switch it back on
		IF IS_PLAYER_BLIP_GPS_TURNED_ON(playerID)
			SET_PLAYER_BLIP_HAS_GPS(playerID, TRUE)
		ENDIF

		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - blip added for player = ", iPlayerIDInt)
			ENDIF
		#ENDIF

		SET_CORRECT_PLAYER_BLIP_FORMAT(playerID)
		
		
		#IF IS_DEBUG_BUILD
			IF NOT (g_PlayerBlipsData.bDisableSetScaleAlpha)
		#ENDIF
		SET_PLAYER_BLIP_SCALE_AND_ALPHA(iPlayerIDInt)
		#IF IS_DEBUG_BUILD
			ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - blip added for player = ", iPlayerIDInt)
			ENDIF
		#ENDIF		
		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.iFlags, PBBD_FADE_HIDE)
			SET_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[iPlayerIDInt], 0)
			SET_BLIP_FADE(g_PlayerBlipsData.playerBlips[iPlayerIDInt], 255, BLIP_HIDE_FADE_PERIOD)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - failed to add blip for player ", iPlayerIDInt)
		SCRIPT_ASSERT("ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER - failed to add blip for player")
	ENDIF

	// broadcast to everyone that I'm blipping this player
	//SET_BIT(GlobalplayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].playerBlipData.iBS_BlippedPlayers, iPlayerIDInt)		
	RETURN(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
ENDFUNC





//PROC SET_BLIP_SCALE_BASED_ON_DIST_FROM_PLAYER(BLIP_INDEX inBlipID, INT PlayerInt=-1)
//	
//	IF NOT IS_PAUSE_MENU_ACTIVE()
//
//		IF DOES_BLIP_EXIST(inBlipID)
//
//			PLAYER_INDEX PlayerID
//			
//			PlayerID = MPGlobals.LocalPlayerID
//
//
//			FLOAT fBlipScale
//			VECTOR vBlipCoords
//			VECTOR vPlayerCoords
//	//		VECTOR vDiff
//						
//	//		IF GET_BLIP_INFO_ID_TYPE(inBlipID) = BLIPTYPE_COORDS				
//				vBlipCoords	= GET_ACTUAL_BLIP_COORDS(inBlipID)
//				vBlipCoords.z = 0.0
//	//		ELSE
//	//			vBlipCoords = GET_ENTITY_COORDS(GET_BLIP_INFO_ID_ENTITY_INDEX(inBlipID), FALSE)
//	//			vBlipCoords.z = 0.0
//	//		ENDIF
//
//			vPlayerCoords = GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID), FALSE)
//			vPlayerCoords.z = 0.0
//
//			// 1. Get distance from this blip to the player		
//			fBlipScale = VMAG(vPlayerCoords - vBlipCoords)
//
//
//			// 2. turn this to a value between 0 and 1 depending on how far away he is (1 is closest)
//			IF (fBlipScale < MIN_DIST_FOR_BLIP_SIZE)
//				fBlipScale = MIN_DIST_FOR_BLIP_SIZE
//			ENDIF
//			IF (fBlipScale >  MAX_DIST_FOR_BLIP_SIZE)
//				fBlipScale = MAX_DIST_FOR_BLIP_SIZE	
//			ENDIF			
//			fBlipScale = (fBlipScale - MIN_DIST_FOR_BLIP_SIZE) / (MAX_DIST_FOR_BLIP_SIZE - MIN_DIST_FOR_BLIP_SIZE)		
//			fBlipScale = fBlipScale - 1.0
//			fBlipScale *= -1.0
//
//			
//			// 3. work out the final blipl scale based on min / max size values			
//			fBlipScale = (fBlipScale * (BLIP_SIZE_NETWORK_PLAYER - (MIN_BLIP_SIZE*BLIP_SIZE_NETWORK_PLAYER))) + (MIN_BLIP_SIZE*BLIP_SIZE_NETWORK_PLAYER)
//
//		
//			// 4. is this player highlighted? if so scale up
//			IF NOT (PlayerInt = -1)
//				IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerBlipHighlighted, PlayerInt)
//					fBlipScale *= HIGHLIGHTED_SCALE_UP
//				ENDIF
//			ENDIF
//			
//			// 5. is this player in a race? if so make them 20% smaller
//			IF NOT (PlayerInt = -1)
//				IF IS_PLAYER_ON_ANY_RACE(INT_TO_NATIVE(PLAYER_INDEX, PlayerInt))
//					fBlipScale *= RACE_SCALE_UP
//				ENDIF
//			ENDIF
//
//			SET_BLIP_SCALE(inBlipID, fBlipScale)
//			
//		ENDIF	
//	
//	ENDIF
//		
//ENDPROC


PROC HANDLE_DEAD_PLAYER_BLIPS()

	INT iDeathBlipTime

	IF (g_PlayerBlipsData.iBlipStaggerCount = NATIVE_TO_INT(MPGlobals.LocalPlayerID))
		iDeathBlipTime = 20000
	ELSE
		iDeathBlipTime = 10000
	ENDIF

	IF DOES_BLIP_EXIST(g_PlayerBlipsData.deadPlayerBlips[g_PlayerBlipsData.iBlipStaggerCount])
		IF SHOULD_HIDE_DEAD_PLAYER_BLIPS()
			REMOVE_BLIP(g_PlayerBlipsData.deadPlayerBlips[g_PlayerBlipsData.iBlipStaggerCount])
			CLEAR_BIT(g_PlayerBlipsData.bs_FadeOutDeadBlip, g_PlayerBlipsData.iBlipStaggerCount)
			IF (g_PlayerBlipsData.iBlipStaggerCount = NATIVE_TO_INT(MPGlobals.LocalPlayerID))
				g_PlayerBlipsData.bPlayerDeathLocationBlipSet = FALSE
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_FadeOutDeadBlip, g_PlayerBlipsData.iBlipStaggerCount)
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , g_PlayerBlipsData.iBlipTimerDeadFading[g_PlayerBlipsData.iBlipStaggerCount]) > iDeathBlipTime)
				OR (IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, g_PlayerBlipsData.iBlipStaggerCount)) AND NOT (g_PlayerBlipsData.iBlipStaggerCount = NATIVE_TO_INT(MPGlobals.LocalPlayerID)))
				OR NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, g_PlayerBlipsData.iBlipStaggerCount), FALSE, TRUE) // player has left
				
					IF (IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, g_PlayerBlipsData.iBlipStaggerCount)) AND NOT (g_PlayerBlipsData.iBlipStaggerCount = NATIVE_TO_INT(MPGlobals.LocalPlayerID)))
					OR NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, g_PlayerBlipsData.iBlipStaggerCount), FALSE, TRUE) // player has left
						REMOVE_BLIP(g_PlayerBlipsData.deadPlayerBlips[g_PlayerBlipsData.iBlipStaggerCount])
					ELSE
						SET_BIT(g_PlayerBlipsData.bs_FadeOutDeadBlip, g_PlayerBlipsData.iBlipStaggerCount)
						SET_BLIP_FADE(g_PlayerBlipsData.deadPlayerBlips[g_PlayerBlipsData.iBlipStaggerCount], 0, BLIP_FADE_PERIOD)
						g_PlayerBlipsData.iBlipTimerDeadFading[g_PlayerBlipsData.iBlipStaggerCount] = GET_NETWORK_TIME()
					ENDIF								
					
				ENDIF
			ELSE
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , g_PlayerBlipsData.iBlipTimerDeadFading[g_PlayerBlipsData.iBlipStaggerCount]) > 2000)
				OR (GET_BLIP_ALPHA (g_PlayerBlipsData.deadPlayerBlips[g_PlayerBlipsData.iBlipStaggerCount]) = 0)
				OR IS_SCREEN_FADED_OUT()
				OR (IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, g_PlayerBlipsData.iBlipStaggerCount)) AND NOT (g_PlayerBlipsData.iBlipStaggerCount = NATIVE_TO_INT(MPGlobals.LocalPlayerID)))
				OR NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, g_PlayerBlipsData.iBlipStaggerCount), FALSE, TRUE) // player has left
					REMOVE_BLIP(g_PlayerBlipsData.deadPlayerBlips[g_PlayerBlipsData.iBlipStaggerCount])
					CLEAR_BIT(g_PlayerBlipsData.bs_FadeOutDeadBlip, g_PlayerBlipsData.iBlipStaggerCount)
					IF (g_PlayerBlipsData.iBlipStaggerCount = NATIVE_TO_INT(MPGlobals.LocalPlayerID))
						g_PlayerBlipsData.bPlayerDeathLocationBlipSet = FALSE
					ENDIF
				ENDIF		
			ENDIF	
		ENDIF
	ENDIF
	
ENDPROC


FUNC FLOAT GET_DISTANCE_TO_TARGET_PLAYER_FROM_CAMERA(PLAYER_INDEX PlayerID)
	RETURN(GET_DISTANCE_BETWEEN_COORDS(GET_GAMEPLAY_CAM_COORD(), GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerID))))
ENDFUNC


FUNC BOOL IS_BLIP_STRING_EMPTY(STRING inStr)
	IF IS_STRING_NULL(inStr)
		PRINTSTRING("IS_STRING_EMPTY - string is null")
		PRINTNL()
		RETURN(TRUE)
	ELSE
		IF ARE_STRINGS_EQUAL(inStr, "")
			PRINTSTRING("IS_STRING_EMPTY - string is equal to nothing ")
			PRINTSTRING(inStr)
			PRINTNL()
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC SET_PLAYER_TO_DISPLAY_CUSTOM_STRING_SLOT(PLAYER_INDEX PlayerID, INT iSlot)
	SET_BIT(g_PlayerBlipsData.bs_DisplayCustomOverheadString[iSlot], NATIVE_TO_INT(PlayerID))
ENDPROC

PROC ASSIGN_CUSTOM_STRING_TO_SLOT(STRING strMessage, HUD_COLOURS HudColour, INT iSlot)
	g_PlayerBlipsData.strCustomOverhead[iSlot] = strMessage
	g_PlayerBlipsData.CustomOverheadStringColour[iSlot] = HudColour
ENDPROC

PROC CLEAR_ANY_UNUSED_CUSTOM_STRINGS()
	INT i
	// first clear any custom strings not being used
	REPEAT NUMBER_OF_CUSTOM_OVERHEAD_STRINGS i
		IF NOT IS_BLIP_STRING_EMPTY(g_PlayerBlipsData.strCustomOverhead[i])
			IF g_PlayerBlipsData.bs_DisplayCustomOverheadString[i] = 0
				g_PlayerBlipsData.strCustomOverhead[i] = ""
			ENDIF
		ENDIF
	ENDREPEAT	
ENDPROC


FUNC INT GET_SLOT_CUSTOM_STRING_IS_USING(STRING strMessage, HUD_COLOURS HudColour)
	// does this string already exist in the custom slots?
	INT i
	REPEAT NUMBER_OF_CUSTOM_OVERHEAD_STRINGS i
		IF NOT IS_BLIP_STRING_EMPTY(g_PlayerBlipsData.strCustomOverhead[i])
			IF ARE_STRINGS_EQUAL(strMessage, g_PlayerBlipsData.strCustomOverhead[i])	
			AND (HudColour = g_PlayerBlipsData.CustomOverheadStringColour[i])
				RETURN(i)
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(-1)
ENDFUNC

FUNC INT GET_FREE_CUSTOM_STRING_SLOT()
	INT i
	REPEAT NUMBER_OF_CUSTOM_OVERHEAD_STRINGS i
		IF IS_BLIP_STRING_EMPTY(g_PlayerBlipsData.strCustomOverhead[i])
			RETURN(i)
		ENDIF
	ENDREPEAT
	RETURN(-1)	
ENDFUNC

PROC SET_PLAYER_TO_DISPLAY_STRING_ABOVE_HEAD(PLAYER_INDEX PlayerID, STRING strMessage, HUD_COLOURS HudColour)	
	INT iSlot	
	CLEAR_ANY_UNUSED_CUSTOM_STRINGS()	
	iSlot = GET_SLOT_CUSTOM_STRING_IS_USING(strMessage, HudColour)	
	IF (iSlot = -1)
		iSlot = GET_FREE_CUSTOM_STRING_SLOT()
	ENDIF
	IF (iSlot = -1)
		SCRIPT_ASSERT("SET_PLAYER_TO_DISPLAY_STRING_ABOVE_HEAD - could not find a free slot!")
	ENDIF	
	ASSIGN_CUSTOM_STRING_TO_SLOT(strMessage, HudColour, iSlot)
	SET_PLAYER_TO_DISPLAY_CUSTOM_STRING_SLOT(PlayerID, iSlot)
ENDPROC

PROC CLEAR_ALL_STRINGS_ABOVE_A_SPECIFIC_PLAYERS_HEAD(PLAYER_INDEX PlayerID)
	INT i
	REPEAT NUMBER_OF_CUSTOM_OVERHEAD_STRINGS i
		CLEAR_BIT(g_PlayerBlipsData.bs_DisplayCustomOverheadString[i], NATIVE_TO_INT(PlayerID))
	ENDREPEAT
	CLEAR_ANY_UNUSED_CUSTOM_STRINGS()
ENDPROC

PROC CLEAR_ALL_STRINGS_ABOVE_PLAYERS_HEADS()
	INT i
	REPEAT NUMBER_OF_CUSTOM_OVERHEAD_STRINGS i
		g_PlayerBlipsData.bs_DisplayCustomOverheadString[i] = 0
	ENDREPEAT
	CLEAR_ANY_UNUSED_CUSTOM_STRINGS()
ENDPROC

PROC SHOW_ALL_PLAYER_BLIPS(BOOL bSet)
	IF (bSet)
		CPRINTLN(DEBUG_BLIP, "[player_blips] SHOW_ALL_PLAYER_BLIPS - TRUE - called by ", GET_THIS_SCRIPT_NAME())
	
		IF NOT (g_PlayerBlipsData.bShowAllPlayerBlips)
			g_PlayerBlipsData.bShowAllPlayerBlips = TRUE
			g_PlayerBlipsData.ShowAllPlayerBlipsThreadID = GET_ID_OF_THIS_THREAD()			
		ELSE
			IF NOT (g_PlayerBlipsData.ShowAllPlayerBlipsThreadID = GET_ID_OF_THIS_THREAD())				
				IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.ShowAllPlayerBlipsThreadID)
					g_PlayerBlipsData.ShowAllPlayerBlipsThreadID = GET_ID_OF_THIS_THREAD()
				ELSE
					SCRIPT_ASSERT("SHOW_ALL_PLAYER_BLIPS - another thread is already showing all blips!")
				ENDIF
			ENDIF
		ENDIF	
		
	ELSE
	
		CPRINTLN(DEBUG_BLIP, "[player_blips] SHOW_ALL_PLAYER_BLIPS - FALSE - called by ", GET_THIS_SCRIPT_NAME())
	
		IF (g_PlayerBlipsData.bShowAllPlayerBlips)
			IF (g_PlayerBlipsData.ShowAllPlayerBlipsThreadID = GET_ID_OF_THIS_THREAD())	
				g_PlayerBlipsData.bShowAllPlayerBlips = FALSE
				g_PlayerBlipsData.ShowAllPlayerBlipsThreadID = INT_TO_NATIVE(THREADID, -1)
			ELSE
				IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.ShowAllPlayerBlipsThreadID)
					g_PlayerBlipsData.bShowAllPlayerBlips = FALSE
					g_PlayerBlipsData.ShowAllPlayerBlipsThreadID = INT_TO_NATIVE(THREADID, -1)
				ELSE
					SCRIPT_ASSERT("SHOW_ALL_PLAYER_BLIPS - can't switch off as another script has set to TRUE")
				ENDIF
			ENDIF
		ELSE
			g_PlayerBlipsData.ShowAllPlayerBlipsThreadID = INT_TO_NATIVE(THREADID, -1)
		ENDIF	
	
	ENDIF
ENDPROC

PROC HIDE_ALL_PLAYER_BLIPS(BOOL bSet)
	IF (bSet)
		
		CPRINTLN(DEBUG_BLIP, "[player_blips] HIDE_ALL_PLAYER_BLIPS - TRUE - called by ", GET_THIS_SCRIPT_NAME())
	
		IF NOT (g_PlayerBlipsData.bHideAllPlayerBlips)
			g_PlayerBlipsData.bHideAllPlayerBlips = TRUE
			g_PlayerBlipsData.HideAllPlayerBlipsThreadID = GET_ID_OF_THIS_THREAD()			
		ELSE
			IF NOT (g_PlayerBlipsData.HideAllPlayerBlipsThreadID = GET_ID_OF_THIS_THREAD())				
				IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideAllPlayerBlipsThreadID)
					g_PlayerBlipsData.HideAllPlayerBlipsThreadID = GET_ID_OF_THIS_THREAD()
				ELSE
					SCRIPT_ASSERT("HIDE_ALL_PLAYER_BLIPS - another thread has already hidden player blips!")
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
	
		CPRINTLN(DEBUG_BLIP, "[player_blips] HIDE_ALL_PLAYER_BLIPS - FALSE - called by ", GET_THIS_SCRIPT_NAME())
	
		IF (g_PlayerBlipsData.bHideAllPlayerBlips)
			IF (g_PlayerBlipsData.HideAllPlayerBlipsThreadID = GET_ID_OF_THIS_THREAD())	
				g_PlayerBlipsData.bHideAllPlayerBlips = FALSE
				g_PlayerBlipsData.HideAllPlayerBlipsThreadID = INT_TO_NATIVE(THREADID, -1)
			ELSE
				IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideAllPlayerBlipsThreadID)
					g_PlayerBlipsData.bHideAllPlayerBlips = FALSE
					g_PlayerBlipsData.HideAllPlayerBlipsThreadID = INT_TO_NATIVE(THREADID, -1)
				ELSE
					SCRIPT_ASSERT("HIDE_ALL_PLAYER_BLIPS - can't make visible as another script wants to hide!")
				ENDIF
			ENDIF
		ELSE
			g_PlayerBlipsData.HideAllPlayerBlipsThreadID = INT_TO_NATIVE(THREADID, -1)
		ENDIF
				
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BLIP_PLAYER(PLAYER_INDEX PlayerID)

	// reasons to block the players blips which are common across modes
	IF SHOULD_BLOCK_PLAYER_BLIP(playerID)
	
		// make sure noticable timer is set to something ancient as if they are blocked, so they don't reappear too soon after respawn etc. 
		g_PlayerBlipsData.iBlipTimerNoticable[NATIVE_TO_INT(playerID)] = GET_TIME_OFFSET(GET_NETWORK_TIME(), NOTICABLE_TIME * -10)  
	
		RETURN(FALSE)
	ENDIF

	// debug - blip all players
	#IF IS_DEBUG_BUILD
		IF (g_PlayerBlipsData.bShowAllPlayerBlipsForDebug)
			RETURN(TRUE)
		ENDIF		
	#ENDIF		
	
	
	// is player force blipped?
	IF IS_PLAYER_FORCE_BLIPPED(playerID)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SHOULD_BLIP_PLAYER - IS_PLAYER_FORCE_BLIPPED  - player id =", NATIVE_TO_INT(playerID))
			ENDIF
		#ENDIF
		#IF IS_DEBUG_BUILD
			IF NOT (NATIVE_TO_INT(PlayerID) = -1)
				IF (MPGlobals.LocalPlayerID = PLAYER_ID())
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_FORCE_BLIPPED)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_FORCE_BLIPPED)
					ENDIF
				ENDIF
			ENDIF
		#ENDIF									
		RETURN(TRUE)
	ENDIF	
	
	// show all flag is set
	IF (g_PlayerBlipsData.bShowAllPlayerBlips)
		#IF IS_DEBUG_BUILD
			IF NOT (NATIVE_TO_INT(PlayerID) = -1)
				IF (MPGlobals.LocalPlayerID = PLAYER_ID())
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_SHOW_ALL)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_SHOW_ALL)
					ENDIF
				ENDIF
			ENDIF
		#ENDIF	
		RETURN(TRUE)
	ENDIF	
	
	// is this an impromptu dm?
	IF IS_PLAYER_ON_IMPROMPTU_DM()	
//	#IF FEATURE_GANG_BOSS
//	OR IS_PLAYER_ON_BOSSVBOSS_DM() 
//	#ENDIF
		IF (playerID = g_sImpromptuVars.playerImpromptuRival)		
			// checkd dist
			IF (GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(playerID), GET_PLAYER_COORDS(g_sImpromptuVars.playerImpromptuRival)) > 200.0)
				#IF IS_DEBUG_BUILD
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_IMPROMPTU)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_IMPROMPTU)
				ENDIF
				#ENDIF
				RETURN(TRUE)
			ENDIF				
		ELSE
			RETURN(FALSE)
		ENDIF
	ELSE
		// is the player using the 'reveal all'?
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRevealPlayers)
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_REVEAL_ALL)
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_REVEAL_ALL)
			ENDIF
			#ENDIF
			RETURN(TRUE)	
		ENDIF		
	ENDIF

	// is player on vehicle dm?
	IF IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_ON_VEH_DM)
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_ON_VEH_DM)
		ENDIF
		#ENDIF
		RETURN(TRUE)	
	ENDIF
	
	IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_ACTIVE_ROAMING_SPEC_MODE)
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)], BLIP_PLAYER_REASON_ACTIVE_ROAMING_SPEC_MODE)
		ENDIF
		#ENDIF
		RETURN(TRUE)
	ENDIF
	
	#IF FEATURE_FREEMODE_ARCADE
	IF IS_FREEMODE_ARCADE()
		RETURN (SHOULD_BLIP_PLAYER_FREEMODE_ARCADE(playerID))
	ENDIF
	#ENDIF
	
	SWITCH GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType
		CASE FMMC_TYPE_DEATHMATCH
		CASE FMMC_TYPE_MISSION
//		#IF FEATURE_GANG_BOSS
//		CASE FMMC_TYPE_GB_BOSS_DEATHMATCH
//		#ENDIF
			IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
				RETURN(SHOULD_BLIP_PLAYER_DM(playerID))
			ENDIF
		BREAK
		DEFAULT
			RETURN(SHOULD_BLIP_PLAYER_FM(playerID))
		BREAK
	ENDSWITCH	
	
	
	RETURN(TRUE)

ENDFUNC

/// PURPOSE:
///    Adds a blip for a  player, and sets the correct blip sprite.
///    TEMP: Also sets the display of the player name. 
PROC ADD_BLIP_FOR_PLAYER(PLAYER_INDEX playerID)

	IF SHOULD_BLIP_PLAYER(playerID)
		ADD_STANDARD_NETWORK_BLIP_FOR_PLAYER(playerID)
		
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_BLIP, "[player_blips] ADD_BLIP_FOR_PLAYER ", GET_PLAYER_NAME(playerID))
		#ENDIF
	ENDIF
		
ENDPROC


PROC UPDATE_PLAYER_IN_PROPERTY_BLIP(PLAYER_INDEX PlayerID)
	IF SHOULD_PLAYER_USE_FAKE_PROPERTY_BLIP(PlayerID)
		IF NOT IS_USING_FAKE_PROPERTY_BLIP(PlayerID)
			CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PLAYER_IN_PROPERTY_BLIP - player has entered property, readding blip - player ", NATIVE_TO_INT(PlayerID))
			ADD_BLIP_FOR_PLAYER(PlayerID)
		ENDIF
		
		// update coords if in truck
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)])
			IF IS_PLAYER_IN_ARMOUR_TRUCK_OUTSIDE_BUNKER(PlayerID)
			OR IS_PLAYER_IN_ARMORY_AIRCRAFT_OUTSIDE_DEFUNCT_BASE(PlayerID)
			OR IS_PLAYER_IN_HACKER_TRUCK_OUTSIDE_BUSINESS_HUB(PlayerID)
			#IF FEATURE_HEIST_ISLAND
			OR IS_PLAYER_IN_SUBMARINE(PlayerID)
			#ENDIf
				SET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)], GET_BLIP_COORD_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PlayerID))
			ELSE
				IF IS_PLAYER_IN_ARMOUR_TRUCK_INSIDE_BUNKER(PlayerID)
					IF IS_PLAYER_INSIDE_TRUCK_IN_BUNKER_I_AM_IN(PlayerID)
					AND NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
						SET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)], <<842.61,-3239.22, -96.6>>)
					ELSE
						SET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)], GET_BLIP_COORD_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PlayerID))
					ENDIF
				ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(PlayerID)
					IF IS_PLAYER_INSIDE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE_I_AM_IN(PlayerID)
					AND NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
						SET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)], <<489.2012, 4786.4897, -56.7471>>)
					ELSE
						SET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)], GET_BLIP_COORD_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PlayerID))
					ENDIF
				ELIF IS_PLAYER_IN_ARMOUR_HACKER_TRUCK_INSIDE_BUSINESS_HUB(PlayerID)
					IF IS_PLAYER_INSIDE_HACKER_TRUCK_IN_BUSINESS_HUB_I_AM_IN(PlayerID)
					AND NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(MPGlobals.LocalPlayerID)
						SET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)], <<842.61,-3239.22, -96.6>>)
					ELSE
						SET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(PlayerID)], GET_BLIP_COORD_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PlayerID))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF IS_USING_FAKE_PROPERTY_BLIP(PlayerID)
			CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PLAYER_IN_PROPERTY_BLIP - player has left property, readding blip - player ", NATIVE_TO_INT(PlayerID))
			ADD_BLIP_FOR_PLAYER(PlayerID)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_PLAYER_IN_IE_WAREHOUSE_MODSHOP_BLIP(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_IN_MOD_SHOP_IN_IE_WAREHOUSE(PlayerID)
		IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayerUsingIEWarehouseModShop, NATIVE_TO_INT(PlayerID))
			CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PLAYER_IN_IE_WAREHOUSE_MODSHOP_BLIP - player has entered ie mod shop, readding blip - player ", NATIVE_TO_INT(PlayerID))
			ADD_BLIP_FOR_PLAYER(PlayerID)
			SET_BIT(g_PlayerBlipsData.bs_PlayerUsingIEWarehouseModShop, NATIVE_TO_INT(PlayerID))
		ENDIF
	ELSE
		IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerUsingIEWarehouseModShop, NATIVE_TO_INT(PlayerID))
			CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_PLAYER_IN_IE_WAREHOUSE_MODSHOP_BLIP - player has left ie mod shop, readding blip - player ", NATIVE_TO_INT(PlayerID))
			ADD_BLIP_FOR_PLAYER(PlayerID)
			CLEAR_BIT(g_PlayerBlipsData.bs_PlayerUsingIEWarehouseModShop, NATIVE_TO_INT(PlayerID))
		ENDIF	
	ENDIF
ENDPROC
 

PROC SHOW_ONLY_BLIPS_OF_PLAYERS_ON_THIS_SCRIPT(BOOL bSet)

	// do we need to cleenup?
	IF (g_PlayerBlipsData.bShowOnlyPlayersOnThisScript)
		IF IS_THREAD_ACTIVE(g_PlayerBlipsData.ShowOnlyPlayersOnThisScriptThreadID)
			IF NOT (g_PlayerBlipsData.ShowOnlyPlayersOnThisScriptThreadID = GET_ID_OF_THIS_THREAD())
				SCRIPT_ASSERT("SHOW_ONLY_BLIPS_OF_PLAYERS_ON_THIS_SCRIPT - another script thread has already called this!")
				EXIT
			ENDIF
		ELSE
			g_PlayerBlipsData.bShowOnlyPlayersOnThisScript = FALSE
		ENDIF
	ENDIF
		
	IF (bSet)	
		// set
		IF NOT (g_PlayerBlipsData.bShowOnlyPlayersOnThisScript)
			g_PlayerBlipsData.bShowOnlyPlayersOnThisScript = TRUE
			g_PlayerBlipsData.ShowOnlyPlayersOnThisScriptThreadID = GET_ID_OF_THIS_THREAD()						
			g_PlayerBlipsData.ShowOnlyPlayersOnThisScriptName = GET_NAME_OF_SCRIPT_WITH_THIS_ID(g_PlayerBlipsData.ShowOnlyPlayersOnThisScriptThreadID)
			g_PlayerBlipsData.iShowOnlyPlayersOnThisScriptInstance = NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT()
			CPRINTLN(DEBUG_BLIP, "[player_blips] SHOW_ONLY_BLIPS_OF_PLAYERS_ON_THIS_SCRIPT(TRUE) - called by ", GET_THIS_SCRIPT_NAME())
		ENDIF

	ELSE
		// unset
		IF (g_PlayerBlipsData.bShowOnlyPlayersOnThisScript)
			g_PlayerBlipsData.bShowOnlyPlayersOnThisScript = FALSE
			g_PlayerBlipsData.ShowOnlyPlayersOnThisScriptThreadID = INT_TO_NATIVE(THREADID, -1)
			CPRINTLN(DEBUG_BLIP, "[player_blips] SHOW_ONLY_BLIPS_OF_PLAYERS_ON_THIS_SCRIPT(FALSE) - called by ", GET_THIS_SCRIPT_NAME())
		ENDIF
	ENDIF
		
ENDPROC

PROC SET_PLAYERS_BLIP_AS_CAUSING_DAMAGE(PLAYER_INDEX playerID)
	INT iPlayerIDInt = NATIVE_TO_INT(playerID)
	g_PlayerBlipsData.iBlipTimerNoticable[iPlayerIDInt] = GET_NETWORK_TIME()
	IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PulseBlip, iPlayerIDInt)
		SET_BIT(g_PlayerBlipsData.bs_PulseBlip, iPlayerIDInt)
		#IF IS_DEBUG_BUILD
			IF (g_PlayerBlipsData.bDebugBlipOutput)
				CPRINTLN(DEBUG_BLIP, "[player_blips] SET_PLAYERS_BLIP_AS_CAUSING_DAMAGE - setting bit for player = ", iPlayerIDInt)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC UPDATE_BLIP_FOR_PLAYER(PLAYER_INDEX PlayerID)
	
	INT iPlayerIDInt
	iPlayerIDInt = NATIVE_TO_INT(playerID)
	
	IF NOT (iPlayerIDInt = -1)
	
		CLEAR_BIT(g_PlayerBlipsData.bs_PassedShipmentCommonChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_DoneShipmentCommonChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_DoneContrabandInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PassedContrabandInternalChecks, iPlayerIDInt)		
		CLEAR_BIT(g_PlayerBlipsData.bs_DoneIllicitGoodsInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PassedIllicitGoodsInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_DoneGunRunInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PassedGunRunInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_DoneSmugglerInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PassedSmugglerInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_DoneFM_GangOpsInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PassedFM_GangOpsInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_DoneBountyInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PassedBountyInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_DoneRandomEventInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PassedRandomEventInternalChecks, iPlayerIDInt)
		#IF FEATURE_CASINO_HEIST
		CLEAR_BIT(g_PlayerBlipsData.bs_DoneCasinoHeistInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PassedCasinoHeistInternalChecks, iPlayerIDInt)
		#ENDIF
		CLEAR_BIT(g_PlayerBlipsData.bs_DoneFMContentInternalChecks, iPlayerIDInt)
		CLEAR_BIT(g_PlayerBlipsData.bs_PassedFMContentInternalChecks, iPlayerIDInt)
		
		IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
			g_PlayerBlipsData.PlayerBlipPriority[iPlayerIDInt] = INT_TO_ENUM(BLIP_PRIORITY, 0)
		ENDIF
		
		IF (PlayerID = MPGlobals.LocalPlayerID)
			// make sure local player doesn't have a blip (as they will have an arrow)
			IF (PlayerID = PLAYER_ID())
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])		
					CLEANUP_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
				ENDIF
//			ELSE
//				#IF IS_DEBUG_BUILD
//					IF (g_PlayerBlipsData.bDebugBlipOutput)
//						CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_BLIP_FOR_PLAYER - player blip colour = ", GET_INT_FROM_HUD_COLOUR(GET_PLAYER_ARROW_BLIP_COLOUR(MPGlobals.LocalPlayerID)))
//					ENDIF
//				#ENDIF
			ENDIF
		ELSE
		
			// can this player see me?
			IF IS_NET_PLAYER_OK(PlayerID)				
				IF NOT ARE_PLAYERS_ON_SAME_TEAM( MPGlobals.LocalPlayerID, PlayerID )
					IF NOT DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(MPGlobals.LocalPlayerID), GET_PLAYER_TEAM(PlayerID))
						IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.iBS_BlippedPlayers, NATIVE_TO_INT(MPGlobals.LocalPlayerID))
							IF NOT IS_PLAYER_SPECTATING(PlayerID)
								IF (MPGlobals.LocalPlayerID = PLAYER_ID())
									GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bMyBlipIsVisible = TRUE
									g_PlayerBlipsData.iTimeBlipWasVisible = GET_NETWORK_TIME()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF			
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF NOT (NATIVE_TO_INT(PlayerID) = -1)
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PlayerID)] = 0
				ENDIF
			#ENDIF
			

			// We split the blipping into two sections, first find out if we should blip a player
			// then determine what we should blip them with.
			IF SHOULD_BLIP_PLAYER(playerID)							
						
				IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])	
					IF IS_NET_PLAYER_OK(PlayerID)		
						CPRINTLN(DEBUG_BLIP, "[player_blips] Adding blip for Player=", iPlayerIDInt, " Team=", GET_PLAYER_TEAM(playerID))					
						ADD_BLIP_FOR_PLAYER(playerID)
					ELSE
						CPRINTLN(DEBUG_BLIP, "[player_blips] Unable to add blip for Player: ", iPlayerIDInt, " as they do not exist.")
					ENDIF
				ELSE		
				
					// check the blip is on the current player ped, not the old one left behind from a net warp. 
					UPDATE_PLAYER_IN_PROPERTY_BLIP(playerID)
					UPDATE_PLAYER_IN_IE_WAREHOUSE_MODSHOP_BLIP(playerID)
					UPDATE_INDICATORS_ON_PLAYERS_BLIP(playerID)
			
					// if for blip hasn't been created when it should have just exit out. 3641661
					IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])	
						CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_BLIP_FOR_PLAYER - blip failed to create for player  ", iPlayerIDInt, " quiting out.")
						EXIT
					ENDIF
			
					IF NOT IS_USING_FAKE_PROPERTY_BLIP(playerID)
					AND ((GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) = BLIPTYPE_COORDS) 
						OR 	((GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]) != BLIPTYPE_COORDS) 
							AND NOT (GET_PLAYER_PED(PlayerID) = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.playerBlips[iPlayerIDInt])))))
							AND NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayerUsingIEWarehouseModShop, NATIVE_TO_INT(PlayerID))
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_BLIP_FOR_PLAYER - GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.playerBlips[", iPlayerIDInt, "]) = ", GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.playerBlips[iPlayerIDInt]))
						#ENDIF

						CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_BLIP_FOR_PLAYER - blip is no longer on current player ped so removing.")
						CLEANUP_BLIP(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
					ELSE	
													
						SET_CORRECT_PLAYER_BLIP_FORMAT(playerID)			
						IF IS_BIT_SET(g_PlayerBlipsData.bs_FadeOutBlip, iPlayerIDInt)						
							RESTORE_BLIP_ALPHA(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
							CLEAR_BIT(g_PlayerBlipsData.bs_FadeOutBlip, iPlayerIDInt)
						ENDIF
																		
					ENDIF
					
					// this needs to be called after SET_CORRECT_PLAYER_BLIP_FORMAT so we dont get indicators on sprite blips. 3136523
					IF IS_BIT_SET(g_PlayerBlipsData.bs_DoResetForSecondaryData, iPlayerIDInt)
						UPDATE_INDICATORS_ON_PLAYERS_BLIP(playerID) 
					ENDIF
					
				ENDIF	
				
				IF (MPGlobals.LocalPlayerID = PLAYER_ID())
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayers, NATIVE_TO_INT(playerID))
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayers, NATIVE_TO_INT(playerID))
					ENDIF	
				ENDIF
				
			ELSE		
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[iPlayerIDInt])
				
					#IF IS_DEBUG_BUILD
						IF IS_NET_PLAYER_OK(PlayerID, FALSE)
							CPRINTLN(DEBUG_BLIP, "[player_blips] Removing blip for Player=", iPlayerIDInt, " Team=", GET_PLAYER_TEAM(playerID))
						ELSE
							CPRINTLN(DEBUG_BLIP, "[player_blips] Removing blip for Player=", iPlayerIDInt, " no longer ok")
						ENDIF
					#ENDIF
					
					// stop flashing when fading out
					IF IS_BIT_SET(g_PlayerBlipsData.bs_PlayerBlipSetToFlash, iPlayerIDInt)
						CDEBUG1LN(DEBUG_BLIP, "[player_blips] Stopping player blip flashing as we remove it.")
						FLASH_BLIP_FOR_PLAYER(PlayerId, FALSE)
					ENDIF
					
					IF NOT (g_PlayerBlipsData.bHideAllPlayerBlips)
					AND NOT SHOULD_BLOCK_PLAYER_BLIP(playerID)
						CDEBUG1LN(DEBUG_BLIP, "[player_blips] Fade cleaning up player blip flashing to remove it.")
						FADE_CLEANUP_PLAYER_BLIP(playerID)
					ELSE
						IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].playerBlipData.iFlags, PBBD_FADE_HIDE)
							CDEBUG1LN(DEBUG_BLIP, "[player_blips] Fade cleaning up player blip flashing to remove it.")
							FADE_CLEANUP_PLAYER_BLIP(playerID)
						ELSE
							CDEBUG1LN(DEBUG_BLIP, "[player_blips] Cleaning up player blip flashing to remove it.")							
							CLEANUP_PLAYER_BLIP(playerID)
						ENDIF
					ENDIF

				ENDIF
				
				// make sure the broadcast bit for this persons blip is cleared
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayers, NATIVE_TO_INT(playerID))
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayers, NATIVE_TO_INT(playerID))
				ENDIF			
				
			ENDIF					
				
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisableUpdateFlashing)
			#ENDIF
			UPDATE_FLASHING_BLIP_FOR_PLAYER(playerID)
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisableUpdateHighlight)
			#ENDIF
			UPDATE_HIGHLIGHTED_BLIP_FOR_PLAYER(playerID)
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisableUpdatePulsing)
			#ENDIF
			UPDATE_PULSING_BLIP_FOR_PLAYER(playerID)
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
				IF NOT (g_PlayerBlipsData.bDisableSetScaleAlpha)
			#ENDIF
			SET_PLAYER_BLIP_SCALE_AND_ALPHA(iPlayerIDInt)
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
		
			//UpdateLongRangeBlipForPlayer(iPlayerIDInt)
		
		ENDIF
		
		IF IS_BIT_SET(g_PlayerBlipsData.bs_BlipCreatedThisFrame, iPlayerIDInt)
			CLEAR_BIT(g_PlayerBlipsData.bs_BlipCreatedThisFrame, iPlayerIDInt)
		ENDIF
		
	ENDIF

ENDPROC


PROC UPDATE_ALL_PLAYER_BLIPS_NOW()
	CPRINTLN(DEBUG_BLIP, "[player_blips] UPDATE_ALL_PLAYER_BLIPS_NOW - called")
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		UPDATE_BLIP_FOR_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))	
	ENDREPEAT
ENDPROC


PROC CLEANUP_ALL_PLAYER_BLIPS_private()
	INT i
	CPRINTLN(DEBUG_BLIP, "[player_blips] CLEANUP_ALL_PLAYER_BLIPS_private - called")
	REPEAT NUM_NETWORK_PLAYERS i
		CLEANUP_PLAYER_INT_BLIP(i)	
	ENDREPEAT
	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(MPGlobals.LocalPlayerID)
		IF (MPGlobals.LocalPlayerID = PLAYER_ID())
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iBS_BlippedPlayers = 0
		ENDIF
	ENDIF
	g_PlayerBlipsData.bClearAllBlips = FALSE
ENDPROC

PROC CLEANUP_ALL_PLAYER_BLIPS()

	CPRINTLN(DEBUG_BLIP, "[player_blips] CLEANUP_ALL_PLAYER_BLIPS - called")
	DEBUG_PRINTCALLSTACK()

	IF NOT ARE_STRINGS_EQUAL(GET_LAUNCH_SCRIPT_NAME(#IF IS_DEBUG_BUILD FALSE #ENDIF ),GET_THIS_SCRIPT_NAME())
		g_PlayerBlipsData.bClearAllBlips = TRUE
	ELSE
		CLEANUP_ALL_PLAYER_BLIPS_private()
	ENDIF
ENDPROC

PROC MAINTAIN_PERSONAL_VEHICLE_FLASHING_BLIP()
	
	BOOL bFlashActive
	INT iTimeDiff = ABSI(GET_TIME_DIFFERENCE(g_PlayerBlipsData.tdPVFlashTimer, GET_NETWORK_TIME()))
	
	IF iTimeDiff >= (g_sMPTunables.iexec_contraband_blip_toggle_time_ms * 2)
		g_PlayerBlipsData.tdPVFlashTimer = GET_NETWORK_TIME()
		iTimeDiff = 0
	ENDIF	
	
	IF iTimeDiff < g_sMPTunables.iexec_contraband_blip_toggle_time_ms
		bFlashActive = FALSE
	ELSE
		bFlashActive = TRUE
	ENDIF	
	
	//CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_PERSONAL_VEHICLE_FLASHING_BLIP bFlashActive = ", bFlashActive)
								
	
	g_PlayerBlipsData.bSpriteBlipPlayerIsInMyPersonalVehicle = FALSE
	IF (bFlashActive)
		g_PlayerBlipsData.bSpriteBlipPlayerIsInMyPersonalVehicle = IS_SPRITE_BLIP_PLAYER_IN_VEHICLE(PERSONAL_VEHICLE_ID(), TRUE)
	ENDIF

ENDPROC

PROC MAINTAIN_REMOTE_PLAYER_BOUNTY_BLIPS()

	BOOL bRefreshBlips = FALSE
	INT iTimeDiff = ABSI(GET_TIME_DIFFERENCE(g_PlayerBlipsData.tdRemotePlayerBountyTimer, GET_NETWORK_TIME()))
	BOOL bShouldBeActive = FALSE
	
	IF iTimeDiff >= (g_sMPTunables.iexec_contraband_blip_toggle_time_ms * 2)
		g_PlayerBlipsData.tdRemotePlayerBountyTimer = GET_NETWORK_TIME()
		iTimeDiff = 0
	ENDIF
	
	IF iTimeDiff < g_sMPTunables.iexec_contraband_blip_toggle_time_ms
		IF IS_BOUNTY_BLIP_FLASH_ACTIVE()
			bRefreshBlips = TRUE
		ELSE
			bShouldBeActive = TRUE
		ENDIF
	ELSE
		IF NOT IS_BOUNTY_BLIP_FLASH_ACTIVE()
			bRefreshBlips = TRUE
			bShouldBeActive = TRUE
		ENDIF
	ENDIF
	
	IF bRefreshBlips
	
		// Only process when another player is bounty and a gang leader
		BOOL bPlayerIsBountyGangLeader = FALSE
		INT iPlayer
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			IF (GlobalServerBD_FM.currentBounties[iPlayer].bTargeted)
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(INT_TO_NATIVE(PLAYER_INDEX, iPlayer)) 
				AND NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(INT_TO_NATIVE(PLAYER_INDEX, iPlayer), MPGlobals.LocalPlayerID)
				AND NOT GB_IS_PLAYER_SHIPMENT(INT_TO_NATIVE(PLAYER_INDEX, iPlayer)) // dont flash if already flashing for shipment blip
					bPlayerIsBountyGangLeader = TRUE
					iPlayer = NUM_NETWORK_PLAYERS+1
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bPlayerIsBountyGangLeader
			SET_DISPLAY_BOUNTY_FLASH_ACTIVE(bShouldBeActive)
			IF IS_NET_PLAYER_OK(MPGlobals.LocalPlayerID)
				DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
			ENDIF
		ELSE
			// Turn off and let staggered loop reset.
			SET_DISPLAY_BOUNTY_FLASH_ACTIVE(FALSE)
			g_PlayerBlipsData.tdRemotePlayerBountyTimer = GET_NETWORK_TIME()
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_REMOTE_PLAYER_SHIPMENT_BLIPS()

	BOOL bRefreshBlips = FALSE
	INT iTimeDiff = ABSI(GET_TIME_DIFFERENCE(g_PlayerBlipsData.tdRemotePlayerShipmentTimer, GET_NETWORK_TIME()))
	BOOL bShouldBeActive = FALSE
	
	IF iTimeDiff >= (g_sMPTunables.iexec_contraband_blip_toggle_time_ms * 2)
		g_PlayerBlipsData.tdRemotePlayerShipmentTimer = GET_NETWORK_TIME()
		iTimeDiff = 0
	ENDIF
	
	IF iTimeDiff < g_sMPTunables.iexec_contraband_blip_toggle_time_ms
		IF IS_SHIPMENT_BLIP_FLASH_ACTIVE()
			bRefreshBlips = TRUE
		ELSE
			bShouldBeActive = TRUE
		ENDIF
	ELSE
		IF NOT IS_SHIPMENT_BLIP_FLASH_ACTIVE()
			bRefreshBlips = TRUE
			bShouldBeActive = TRUE
		ENDIF
	ENDIF
	
	IF GB_IS_PLAYER_SHIPMENT(INT_TO_NATIVE(PLAYER_INDEX, g_PlayerBlipsData.iBlipStaggerCount))
		PRINTLN("[player_blips] GB_IS_PLAYER_SHIPMENT is TRUE for player ", g_PlayerBlipsData.iBlipStaggerCount, " (", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, g_PlayerBlipsData.iBlipStaggerCount)),")")
		SET_BIT(g_PlayerBlipsData.BS_PlayerIsShipment, g_PlayerBlipsData.iBlipStaggerCount)
	ELSE
		CLEAR_BIT(g_PlayerBlipsData.BS_PlayerIsShipment, g_PlayerBlipsData.iBlipStaggerCount)
	ENDIF
	
	
	IF bRefreshBlips
	
		// Only process when another player is contraband.
		IF (g_PlayerBlipsData.BS_PlayerIsShipment > 0)
			SET_DISPLAY_SHIPMENT_FLASH_ACTIVE(bShouldBeActive)
			IF IS_NET_PLAYER_OK(MPGlobals.LocalPlayerID)
				DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
			ENDIF
		ELSE
			// Turn off and let staggered loop reset.
			SET_DISPLAY_SHIPMENT_FLASH_ACTIVE(FALSE)
			g_PlayerBlipsData.tdRemotePlayerShipmentTimer = GET_NETWORK_TIME()
		ENDIF
		
	ENDIF
ENDPROC


FUNC BOOL IS_PLAYER_MAKING_CAR_NOISE()
	VEHICLE_INDEX CarID	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_IN_BURNOUT(CarID)
				RETURN(TRUE)
			ENDIF
		ENDIF
		IF IS_PLAYER_PRESSING_HORN(PLAYER_ID())
			RETURN(TRUE)
		ENDIF					
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC HIDE_MY_PLAYER_BLIP(BOOL bSet, BOOL bIgnoreThreadCheck=FALSE)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_BLIP, "[player_blips] HIDE_MY_PLAYER_BLIP = ", PICK_STRING(bSet, "TRUE", "FALSE"), " called by ", GET_THIS_SCRIPT_NAME())
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF NOT (bIgnoreThreadCheck)
		IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()	
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("[player_blips] HIDE_MY_PLAYER_BLIP - called from within launch script. I do not like this. Neilf") NET_NL()
			SCRIPT_ASSERT("HIDE_MY_PLAYER_BLIP - called from within launch script.")
			EXIT	
		ENDIF
	ENDIF
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideMyBlipThreadID)
	OR (g_PlayerBlipsData.HideMyBlipThreadID = GET_ID_OF_THIS_THREAD())
		IF (bSet)
			g_PlayerBlipsData.HideMyBlipThreadID = GET_ID_OF_THIS_THREAD()	
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_HIDDEN)
		ELSE
			g_PlayerBlipsData.HideMyBlipThreadID = INT_TO_NATIVE(THREADID, -1)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_HIDDEN)
		ENDIF
	ELSE
		CASSERTLN(DEBUG_BLIP, "[player_blips] HIDE_MY_PLAYER_BLIP - already in use by another thread.")	
	ENDIF
ENDPROC

PROC HIDE_MY_PLAYER_BLIP_FROM_NON_PARTICIPANTS_OF_EVENT(BOOL bSet)

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_BLIP, "[player_blips] HIDE_MY_PLAYER_BLIP_FROM_NON_PARTICIPANTS_OF_EVENT(", PICK_STRING(bSet, "TRUE", "FALSE"), ") called by ", GET_THIS_SCRIPT_NAME())
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideMyBlipFromNonParticipantsOfEventThreadID)
	OR (g_PlayerBlipsData.HideMyBlipFromNonParticipantsOfEventThreadID = GET_ID_OF_THIS_THREAD())
		IF (bSet)
			g_PlayerBlipsData.HideMyBlipFromNonParticipantsOfEventThreadID = GET_ID_OF_THIS_THREAD()
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_HIDDEN_FROM_EVENT_NON_PARTICIPANTS) 
		ELSE
			g_PlayerBlipsData.HideMyBlipFromNonParticipantsOfEventThreadID = INT_TO_NATIVE(THREADID, -1)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_HIDDEN_FROM_EVENT_NON_PARTICIPANTS)
		ENDIF
	ELSE
		CASSERTLN(DEBUG_BLIP, "[player_blips] HIDE_MY_PLAYER_BLIP_FROM_NON_PARTICIPANTS_OF_EVENT - already in use by another thread.")	
	ENDIF	
ENDPROC

PROC HIDE_MY_PLAYER_BLIP_FROM_NON_GANG_MEMBERS(BOOL bSet, BOOL bIgnoreThreadCheck=FALSE)

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_BLIP, "[player_blips] HIDE_MY_PLAYER_BLIP_FROM_NON_GANG_MEMBERS(", PICK_STRING(bSet, "TRUE", "FALSE"), ") called by ", GET_THIS_SCRIPT_NAME())
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideMyBlipFromNonGangMembersThreadID)
	OR (g_PlayerBlipsData.HideMyBlipFromNonGangMembersThreadID = GET_ID_OF_THIS_THREAD())
	OR (bIgnoreThreadCheck)
		IF (bSet)
			g_PlayerBlipsData.HideMyBlipFromNonGangMembersThreadID = GET_ID_OF_THIS_THREAD()
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_HIDDEN_FROM_NON_GANG_MEMBERS) 
		ELSE
			g_PlayerBlipsData.HideMyBlipFromNonGangMembersThreadID = INT_TO_NATIVE(THREADID, -1)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_HIDDEN_FROM_NON_GANG_MEMBERS)
		ENDIF
	ELSE
		IF NOT (bIgnoreThreadCheck)
			CASSERTLN(DEBUG_BLIP, "[player_blips] HIDE_MY_PLAYER_BLIP_FROM_NON_GANG_MEMBERS - already in use by another thread.")	
		ENDIF
	ENDIF	
ENDPROC

PROC HIDE_MY_PLAYER_BLIP_FROM_TEAM(BOOL bSet, INT iTeam)
	
	IF (iTeam < 0)
	OR (iTeam > 7)
		SCRIPT_ASSERT("HIDE_MY_PLAYER_BLIP_FROM_TEAM - team must be between 0 and 7!")
		CPRINTLN(DEBUG_BLIP, "[player_blips] HIDE_MY_PLAYER_BLIP_FROM_TEAM - team must be between 0 and 7! iTeam = ", iTeam) 
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_BLIP, "[player_blips] HIDE_MY_PLAYER_BLIP_FROM_TEAM(", PICK_STRING(bSet, "TRUE", "FALSE"), ", ", iTeam, ") called by ", GET_THIS_SCRIPT_NAME())
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideMyBlipFromTeamThreadID[iTeam])
	OR (g_PlayerBlipsData.HideMyBlipFromTeamThreadID[iTeam] = GET_ID_OF_THIS_THREAD())
		IF (bSet)
			g_PlayerBlipsData.HideMyBlipFromTeamThreadID[iTeam] = GET_ID_OF_THIS_THREAD()
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, (PBBD_HIDDEN_FROM_TEAM_0 + iTeam) ) 
		ELSE
			g_PlayerBlipsData.HideMyBlipFromTeamThreadID[iTeam] = INT_TO_NATIVE(THREADID, -1)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, (PBBD_HIDDEN_FROM_TEAM_0 + iTeam))
		ENDIF
	ELSE
		CASSERTLN(DEBUG_BLIP, "[player_blips] HIDE_MY_PLAYER_BLIP_FROM_TEAM - already in use by another thread.")		
	ENDIF	

ENDPROC

FUNC BOOL IS_MY_PLAYER_BLIP_HIGHLIGHTED()
	RETURN IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_SELF_HIGHLIGHTED)
ENDFUNC

PROC IGNORE_SELF_HIGHLIGHTING_PLAYER(PLAYER_INDEX PlayerID)
	IF NOT (PlayerID = PLAYER_ID())
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].playerBlipData.iFlags, PBBD_IS_SELF_HIGHLIGHTED)
			SET_BIT(g_PlayerBlipsData.bs_IgnoreSelfHighlightForPlayer, NATIVE_TO_INT(PlayerID))
		ENDIF
	ENDIF
ENDPROC

PROC HIGHLIGHT_MY_PLAYER_BLIP(BOOL bSet, INT iTime=-1, BOOL bIgnoreThreadCheck=FALSE)//, BOOL bPrintTicker=FALSE)	
	#IF IS_DEBUG_BUILD
		IF (bSet)
			CPRINTLN(DEBUG_BLIP, "[player_blips] HIGHLIGHT_MY_PLAYER_BLIP = TRUE called by ", GET_THIS_SCRIPT_NAME())
		ELSE
			CPRINTLN(DEBUG_BLIP, "[player_blips] HIGHLIGHT_MY_PLAYER_BLIP = FALSE called by ", GET_THIS_SCRIPT_NAME())
		ENDIF
	#ENDIF
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HighlightMyBlipThreadID)
	OR (g_PlayerBlipsData.HighlightMyBlipThreadID = GET_ID_OF_THIS_THREAD())
	OR (bIgnoreThreadCheck)
		IF (bSet)
			g_PlayerBlipsData.HighlightMyBlipThreadID = GET_ID_OF_THIS_THREAD()					
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_SELF_HIGHLIGHTED)
			g_PlayerBlipsData.HighlightStartTime = GET_NETWORK_TIME()			
			IF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
				g_PlayerBlipsData.bHighlightedBlipWhileOnMission = TRUE
				CPRINTLN(DEBUG_BLIP, "[player_blips] HIGHLIGHT_MY_PLAYER_BLIP, called when on mission ")
			ELSE
				g_PlayerBlipsData.bHighlightedBlipWhileOnMission = FALSE
				CPRINTLN(DEBUG_BLIP, "[player_blips] HIGHLIGHT_MY_PLAYER_BLIP, called when off mission ")
			ENDIF
		ELSE
			g_PlayerBlipsData.HighlightMyBlipThreadID = INT_TO_NATIVE(THREADID, -1)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_SELF_HIGHLIGHTED)
			g_PlayerBlipsData.bHighlightedBlipWhileOnMission = FALSE
			/*IF (bPrintTicker)
				PRINT_TICKER("T_SHOWB_D")
			ENDIF*/
		ENDIF
		g_PlayerBlipsData.iTimeToHighlight = iTime
	ELSE
		IF NOT (bIgnoreThreadCheck)
			CASSERTLN(DEBUG_BLIP, "[player_blips] HIGHLIGHT_MY_PLAYER_BLIP - already in use by another thread.")
		ENDIF
	ENDIF
ENDPROC

PROC FLASH_MY_PLAYER_ARROW(BOOL bSet, INT iDuration, BOOL bIgnoreThreadCheck=FALSE)
	#IF IS_DEBUG_BUILD
		IF (bSet)
			CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_MY_PLAYER_ARROW = TRUE called by ", GET_THIS_SCRIPT_NAME(), ", iDuration ", iDuration, ", bIgnoreThreadCheck = ", bIgnoreThreadCheck)
		ELSE
			CPRINTLN(DEBUG_BLIP, "[player_blips] FLASH_MY_PLAYER_ARROW = FALSE called by ", GET_THIS_SCRIPT_NAME(), ", iDuration ", iDuration, ", bIgnoreThreadCheck = ", bIgnoreThreadCheck)
		ENDIF
	#ENDIF
	
	IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.FlashMyArrowThreadID)
	OR (g_PlayerBlipsData.FlashMyArrowThreadID = GET_ID_OF_THIS_THREAD())
	OR (bIgnoreThreadCheck)
		IF (bSet)
			g_PlayerBlipsData.FlashMyArrowThreadID = GET_ID_OF_THIS_THREAD()					
			g_PlayerBlipsData.FlashMyArrowStartTime = GET_NETWORK_TIME()	
		ELSE
			g_PlayerBlipsData.FlashMyArrowThreadID = INT_TO_NATIVE(THREADID, -1)
		ENDIF
		g_PlayerBlipsData.iDurationToFlashMyArrow = iDuration
		g_PlayerBlipsData.bFlashMyArrow = bSet
	ELSE
		IF NOT (bIgnoreThreadCheck)
			CASSERTLN(DEBUG_BLIP, "[player_blips] FLASH_MY_PLAYER_ARROW - already in use by another thread.")
		ENDIF
	ENDIF
ENDPROC



PROC MAINTAIN_LEFT_PLAYERS_BLIPS()
	PLAYER_INDEX PlayerID
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.playerBlips[g_PlayerBlipsData.iBlipStaggerCount])		
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, g_PlayerBlipsData.iBlipStaggerCount)
		IF NOT IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
		OR IS_PLAYER_SPECTATING(PlayerID)
			CLEANUP_PLAYER_BLIP(playerID)	
		ENDIF
	ENDIF		
ENDPROC

PROC MAINTAIN_PAUSE_MENU_CONDITIONS_FOR_BLIPS()
	
	INT i
	
	// is player in pause menu? 
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT (g_PlayerBlipsData.bSetupForPauseMenu)
			
			REPEAT NUM_NETWORK_PLAYERS i
				IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_NATIVE(PLAYER_INDEX, i))
					SET_PLAYER_BLIP_SCALE_AND_ALPHA(i)
				ENDIF			
			ENDREPEAT
			
			IF IS_PLAYER_USING_ARENA()
				REPEAT NUM_NETWORK_PLAYERS i
					IF DOES_BLIP_EXIST(g_PlayerBlipsData.deadPlayerBlips[i])
						REMOVE_BLIP(g_PlayerBlipsData.deadPlayerBlips[i])
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_PAUSE_MENU_CONDITIONS_FOR_BLIPS = removing dead player for player ", i)
					ENDIF
				ENDREPEAT
			ENDIF			
			
		
			CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_PAUSE_MENU_CONDITIONS_FOR_BLIPS = setup for pause menu ")
		
			g_PlayerBlipsData.bSetupForPauseMenu = TRUE	
		ENDIF
	ELSE
		IF (g_PlayerBlipsData.bSetupForPauseMenu)
		
			REPEAT NUM_NETWORK_PLAYERS i
				IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_NATIVE(PLAYER_INDEX, i))
					SET_PLAYER_BLIP_SCALE_AND_ALPHA(i)
				ENDIF
			ENDREPEAT
		
			CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_PAUSE_MENU_CONDITIONS_FOR_BLIPS = reset after pause menu ")
		
			g_PlayerBlipsData.bSetupForPauseMenu = FALSE
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL AmILeaderOfVehicle(VEHICLE_INDEX VehicleID)
	RETURN IS_PED_LEADER_OF_VEHICLE(PLAYER_PED_ID(), VehicleID, TRUE)
ENDFUNC


PROC MAINTAIN_PEGASUS_BLIP_FOR_GANG_LEADER_VEHICLE()
	IF (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE))
		PLAYER_INDEX GangLeaderID = GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID)
//		IF (GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].) // url:bugstar:3643354, agreed with Neil to comment out this check, we couldn't determine why this check was needed and it was not being set
			IF DOES_ENTITY_EXIST(MPGlobals.RemotePegV[NATIVE_TO_INT(GangLeaderID)])
				IF NOT IS_VEHICLE_FUCKED_MP(MPGlobals.RemotePegV[NATIVE_TO_INT(GangLeaderID)])
					IF IS_VEHICLE_EMPTY(MPGlobals.RemotePegV[NATIVE_TO_INT(GangLeaderID)], TRUE, TRUE)
					
						// create the blip if it doesn't yet exist
						IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderPegasusBlipID)
							g_PlayerBlipsData.GangLeaderPegasusBlipID = ADD_BLIP_FOR_ENTITY(MPGlobals.RemotePegV[NATIVE_TO_INT(GangLeaderID)])	
							SET_BLIP_SPRITE(g_PlayerBlipsData.GangLeaderPegasusBlipID, GET_PEGASUS_BLIP_SPRITE_FOR_MODEL(GET_ENTITY_MODEL(MPGlobals.RemotePegV[NATIVE_TO_INT(GangLeaderID)])))
							SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderPegasusBlipID, GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(MPGlobals.RemotePegV[NATIVE_TO_INT(GangLeaderID)])))
							SET_CUSTOM_BLIP_NAME_FROM_MODEL(g_PlayerBlipsData.GangLeaderPegasusBlipID, GET_ENTITY_MODEL(MPGlobals.RemotePegV[NATIVE_TO_INT(GangLeaderID)]))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderPegasusBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))
						ENDIF
						
						// has sprite changed?
						IF UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(MPGlobals.RemotePegV[NATIVE_TO_INT(GangLeaderID)], g_PlayerBlipsData.GangLeaderPegasusBlipID, TRUE)
							SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderPegasusBlipID, GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(MPGlobals.RemotePegV[NATIVE_TO_INT(GangLeaderID)])))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderPegasusBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))						
						ENDIF
						
						// if player is inside office and the heli is on the roof hide it
						HIDE_VEHICLE_BLIP_ON_CERTAIN_ROOFS(MPGlobals.RemotePegV[NATIVE_TO_INT(GangLeaderID)], g_PlayerBlipsData.GangLeaderPegasusBlipID)

											
						EXIT // exit here so blip doesn't get deleted below.
					ENDIF	
				ENDIF
			ENDIF
//		ENDIF		
	ENDIF

	
	// if we've got this far in the proc then the blip shouldn't exist, so remove it
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderPegasusBlipID)
		REMOVE_BLIP(g_PlayerBlipsData.GangLeaderPegasusBlipID)
		CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_PEGASUS_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader pegasus blip")
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_ADD_BLIP_FOR_OWNERS_TRUCK(PLAYER_INDEX pOwner, TEXT_LABEL_63 &tl63Reason)
	tl63Reason = ""

	IF IS_MP_SAVED_TRUCK_BEING_CLEANED_UP()
		tl63Reason = "being cleaned up"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		IF (g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX())
		AND (g_ownerOfArmoryTruckPropertyIAmIn = pOwner)
			tl63Reason  = "I am in truck property "
			tl63Reason += GET_PLAYER_NAME(g_ownerOfArmoryTruckPropertyIAmIn)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(pOwner)
		IF IS_REQUEST_TO_CREATE_ARMORY_TRUCK_IN_BUNKER(pOwner)
			tl63Reason = "request to create in bunker"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
		AND (globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = pOwner)
			RETURN TRUE
		ELSE
			tl63Reason = "truck in bunker, player isnt"
			RETURN FALSE
		ENDIF
	ELSE
		IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
			tl63Reason = "player in bunker, truck isnt"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
			tl63Reason = "player in base, truck not in bunker"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
			tl63Reason = "player in business hub, truck not in bunker"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
			tl63Reason = "player in arena garage, truck not in bunker"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_CASINO(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_VALET_GARAGE(PLAYER_ID())
			tl63Reason = "player in casino, truck not in bunker"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
			tl63Reason = "player in agency, truck not in bunker"
			RETURN FALSE
		ENDIF
		
		#IF FEATURE_DLC_2_2022
		IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
			tl63Reason = "player in juggalo warehouse, truck not in bunker"
			RETURN FALSE
		ENDIF
	
		IF IS_PLAYER_IN_MULTISTOREY_GARAGE_PROPERTY(PLAYER_ID())
			tl63Reason = "player in multistorey garage, truck not in bunker"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
			tl63Reason = "player in acid lab, truck not in bunker"
			RETURN FALSE
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
ENDFUNC

FUNC BOOL SHOULD_ADD_BLIP_FOR_OWNERS_AVENGER(PLAYER_INDEX pOwner, TEXT_LABEL_63 &tl63Reason)
	tl63Reason = ""

	IF IS_MP_SAVED_AVENGER_BEING_CLEANED_UP()
		tl63Reason = "being cleaned up"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		IF (g_OwnerOfArmoryAircraftPropertyIAmIn != INVALID_PLAYER_INDEX())
		AND (g_OwnerOfArmoryAircraftPropertyIAmIn = pOwner)
			tl63Reason  = "I am in plane property "
			tl63Reason += GET_PLAYER_NAME(g_OwnerOfArmoryAircraftPropertyIAmIn)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(pOwner)
		IF IS_REQUEST_TO_CREATE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE(pOwner)
			tl63Reason = "request to create in base"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		AND (globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = pOwner)
			RETURN TRUE
		ELSE
			tl63Reason = "plane in base, player isnt"
			RETURN FALSE
		ENDIF
	ELSE
		IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
			tl63Reason = "player in bunker, plane not in base"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
			tl63Reason = "player in base, plane isnt"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
			tl63Reason = "player in business hub, plane not in base"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
			tl63Reason = "player in arena garage, plane not in base"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_CASINO(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_VALET_GARAGE(PLAYER_ID())
			tl63Reason = "player in casino, plane not in base"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
			tl63Reason = "player in agency, plane not in base"
			RETURN FALSE
		ENDIF
		
		#IF FEATURE_DLC_2_2022
		IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
			tl63Reason = "player in juggalo warehouse, plane not in base"
			RETURN FALSE
		ENDIF

		IF IS_PLAYER_IN_MULTISTOREY_GARAGE_PROPERTY(PLAYER_ID())
			tl63Reason = "player in multistorey garage, plane not in base"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
			tl63Reason = "player in acid lab, plane not in base"
			RETURN FALSE
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
ENDFUNC

FUNC BOOL SHOULD_ADD_BLIP_FOR_OWNERS_HACKER_TRUCK(PLAYER_INDEX pOwner, TEXT_LABEL_63 &tl63Reason)
	tl63Reason = ""

	IF IS_MP_SAVED_HACKERTRUCK_BEING_CLEANED_UP()
		tl63Reason = "being cleaned up"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
	AND NOT IS_LOCAL_PLAYER_USING_DRONE()
		IF (GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX())
		AND (GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = pOwner)
			tl63Reason  = "I am in hacker truck property "
			tl63Reason += GET_PLAYER_NAME(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(pOwner)
		IF IS_REQUEST_TO_CREATE_HACKER_TRUCK_IN_BUSINESS_HUB(pOwner)
			tl63Reason = "request to create in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
		AND (globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = pOwner)
			RETURN TRUE
		ELSE
			tl63Reason = "Hacker Truck in business hub, player isnt"
			RETURN FALSE
		ENDIF
	ELSE
		IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
			tl63Reason = "player in bunker, hacker truck not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
			tl63Reason = "player in base, hacker truck not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
			tl63Reason = "player in business hub, hacker truck isnt"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
			tl63Reason = "player in arena garage, hacker truck not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_CASINO(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_VALET_GARAGE(PLAYER_ID())
			tl63Reason = "player in casino, hacker truck not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
			tl63Reason = "player in agency, hacker truck not in business hub"
			RETURN FALSE
		ENDIF
		
		#IF FEATURE_DLC_2_2022
		IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
			tl63Reason = "player in juggalo warehouse, hacker truck not in business hub"
			RETURN FALSE
		ENDIF

		IF IS_PLAYER_IN_MULTISTOREY_GARAGE_PROPERTY(PLAYER_ID())
			tl63Reason = "player in multistorey garage, hacker truck not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
			tl63Reason = "player in acid lab, hacker truck not in business hub"
			RETURN FALSE
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
ENDFUNC

#IF FEATURE_DLC_2_2022
FUNC BOOL SHOULD_ADD_BLIP_FOR_OWNERS_ACID_LAB(PLAYER_INDEX pOwner, TEXT_LABEL_63 &tl63Reason)
	tl63Reason = ""

	IF IS_MP_SAVED_ACIDLAB_BEING_CLEANED_UP()
		tl63Reason = "being cleaned up"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
		IF (GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX())
		AND (GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = pOwner)
			tl63Reason  = "I am in acid lab property "
			tl63Reason += GET_PLAYER_NAME(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(pOwner)
		IF IS_REQUEST_TO_CREATE_ACID_LAB_IN_JUGGALO_HIDEOUT(pOwner)
			tl63Reason = "request to create in warehouse"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
		AND (globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = pOwner)
			RETURN TRUE
		ELSE
			tl63Reason = "Acid lab in juggalo warehouse, player isnt"
			RETURN FALSE
		ENDIF
	ELSE
		IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
			tl63Reason = "player in bunker, Acid lab not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
			tl63Reason = "player in base, Acid lab not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
			tl63Reason = "player in business hub, Acid lab isnt"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
			tl63Reason = "player in arena garage, Acid lab not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_CASINO(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		OR IS_PLAYER_IN_CASINO_VALET_GARAGE(PLAYER_ID())
			tl63Reason = "player in casino, Acid lab not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
			tl63Reason = "player in juggalo hideout, Acid lab not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
			tl63Reason = "player in agency, Acid lab not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_MULTISTOREY_GARAGE_PROPERTY(PLAYER_ID())
			tl63Reason = "player in multistorey garage, Acid lab not in business hub"
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
			tl63Reason = "player in acid lab, Acid lab not in business hub"
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF

ENDFUNC
#ENDIF

#IF FEATURE_HEIST_ISLAND
FUNC BOOL SHOULD_ADD_BLIP_FOR_OWNERS_SUBMARINE(PLAYER_INDEX pOwner, TEXT_LABEL_63 &tl63Reason)
	tl63Reason = ""
	
	IF NOT IS_PLAYER_SUBMARINE_IN_FREEMODE(pOwner)
		tl63Reason = "Submarine not in freemode"
	ENDIF
	
	IF IS_PLAYER_IN_SMPL_INT_SUBMARINE(MPGlobals.LocalPlayerID)
	AND NOT IS_LOCAL_PLAYER_USING_DRONE()
		IF (GlobalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX())
		AND (GlobalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner = pOwner)
			tl63Reason  = "I am in submarine property "
			tl63Reason += GET_PLAYER_NAME(GlobalPlayerBD[NATIVE_TO_INT(MPGlobals.LocalPlayerID)].SimpleInteriorBD.propertyOwner)
			RETURN FALSE
		ENDIF
	ENDIF

	IF IS_PLAYER_IN_BUNKER(MPGlobals.LocalPlayerID)
		tl63Reason = "player in bunker"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(MPGlobals.LocalPlayerID)
		tl63Reason = "player in base"
		RETURN FALSE
	ENDIF
	
	IF IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_REMOVE_KOSTAKA_BLIP_FOR_DROPOFF)
		IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = pOwner
		OR pOwner = MPGlobals.LocalPlayerID
			tl63Reason = "player delivering to submarine"
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_BUSINESS_HUB(MPGlobals.LocalPlayerID)
		tl63Reason = "player in business hub"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ARENA_GARAGE(MPGlobals.LocalPlayerID)
		tl63Reason = "player in arena garage"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ARCADE_PROPERTY(MPGlobals.LocalPlayerID)
		tl63Reason = "player in arcade"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_WAREHOUSE(MPGlobals.LocalPlayerID)
		tl63Reason = "player in warehouse"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CASINO(MPGlobals.LocalPlayerID)
	OR IS_PLAYER_IN_CASINO_APARTMENT(MPGlobals.LocalPlayerID)
	OR IS_PLAYER_IN_CASINO_VALET_GARAGE(MPGlobals.LocalPlayerID)
	OR IS_PLAYER_IN_CASINO_NIGHTCLUB(MPGlobals.LocalPlayerID)
		tl63Reason = "player in casino"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_FIXER_HQ(MPGlobals.LocalPlayerID)
			tl63Reason = "player in agency"
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_DLC_2_2022
	IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(MPGlobals.LocalPlayerID)
		tl63Reason = "player in juggalo warehouse"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_MULTISTOREY_GARAGE_PROPERTY(MPGlobals.LocalPlayerID)
		tl63Reason = "player in multistorey garage"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ACID_LAB(MPGlobals.LocalPlayerID)
		tl63Reason = "player in acid lab"
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_ADD_BLIP_FOR_OWNERS_SUBMARINE_DINGHY(PLAYER_INDEX pOwner, TEXT_LABEL_63 &tl63Reason)
	UNUSED_PARAMETER(pOwner)
	tl63Reason = ""

	IF IS_MP_SAVED_SUBMARINE_DINGHY_BEING_CLEANED_UP()
		tl63Reason = "being cleaned up"
		RETURN FALSE
	ENDIF

	IF IS_PLAYER_IN_BUNKER(MPGlobals.LocalPlayerID)
		tl63Reason = "player in bunker"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(MPGlobals.LocalPlayerID)
		tl63Reason = "player in base"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_BUSINESS_HUB(MPGlobals.LocalPlayerID)
		tl63Reason = "player in business hub"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ARENA_GARAGE(MPGlobals.LocalPlayerID)
		tl63Reason = "player in arena garage"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ARCADE_PROPERTY(MPGlobals.LocalPlayerID)
		tl63Reason = "player in arcade"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CASINO(MPGlobals.LocalPlayerID)
	OR IS_PLAYER_IN_CASINO_APARTMENT(MPGlobals.LocalPlayerID)
	OR IS_PLAYER_IN_CASINO_VALET_GARAGE(MPGlobals.LocalPlayerID)
		tl63Reason = "player in casino"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_FIXER_HQ(MPGlobals.LocalPlayerID)
			tl63Reason = "player in agency"
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_DLC_2_2022
	IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(MPGlobals.LocalPlayerID)
		tl63Reason = "player in juggalo warehouse"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_MULTISTOREY_GARAGE_PROPERTY(MPGlobals.LocalPlayerID)
		tl63Reason = "player in multistorey garage"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ACID_LAB(MPGlobals.LocalPlayerID)
		tl63Reason = "player in acid lab"
		RETURN FALSE
	ENDIF
	#ENDIF
	RETURN TRUE

ENDFUNC
#ENDIF

#IF FEATURE_DLC_2_2022
FUNC BOOL SHOULD_ADD_BLIP_FOR_OWNERS_SUPPORT_BIKE(PLAYER_INDEX pOwner, TEXT_LABEL_63 &tl63Reason)
	UNUSED_PARAMETER(pOwner)
	tl63Reason = ""
	
	IF IS_MP_SAVED_SUPPORT_BIKE_BEING_CLEANED_UP()
		tl63Reason = "being cleaned up"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_BUNKER(MPGlobals.LocalPlayerID)
		tl63Reason = "player in bunker"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(MPGlobals.LocalPlayerID)
		tl63Reason = "player in base"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_BUSINESS_HUB(MPGlobals.LocalPlayerID)
		tl63Reason = "player in business hub"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ARENA_GARAGE(MPGlobals.LocalPlayerID)
		tl63Reason = "player in arena garage"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ARCADE_PROPERTY(MPGlobals.LocalPlayerID)
		tl63Reason = "player in arcade"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CASINO(MPGlobals.LocalPlayerID)
	OR IS_PLAYER_IN_CASINO_APARTMENT(MPGlobals.LocalPlayerID)
	OR IS_PLAYER_IN_CASINO_VALET_GARAGE(MPGlobals.LocalPlayerID)
		tl63Reason = "player in casino"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_FIXER_HQ(MPGlobals.LocalPlayerID)
			tl63Reason = "player in agency"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(MPGlobals.LocalPlayerID)
		tl63Reason = "player in juggalo warehouse"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_MULTISTOREY_GARAGE_PROPERTY(MPGlobals.LocalPlayerID)
		tl63Reason = "player in multistorey garage"
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ACID_LAB(MPGlobals.LocalPlayerID)
		tl63Reason = "player in acid lab"
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC
#ENDIF

PROC MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE()
	IF g_bBypass_MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE
		EXIT
	ENDIF
	
	IF (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE))
		PLAYER_INDEX GangLeaderID = GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID)
		IF (GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].bBlipMyTruckVehicleForGangMembers)
			TEXT_LABEL_63 tl63Reason
			IF SHOULD_ADD_BLIP_FOR_OWNERS_TRUCK(GangLeaderID, tl63Reason)
				
				BOOL bGangLeaderTruckBlipIDExists = FALSE
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderTruckBlipID)
					bGangLeaderTruckBlipIDExists = TRUE
					IF IS_PLAYER_TRUCK_IN_FREEMODE(GangLeaderID)
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[NATIVE_TO_INT(GangLeaderID)][1])
							IF NOT IS_VEHICLE_FUCKED_MP(MPGlobals.RemoteTruckV[NATIVE_TO_INT(GangLeaderID)][1])
							AND IS_VEHICLE_EMPTY(MPGlobals.RemoteTruckV[NATIVE_TO_INT(GangLeaderID)][1], TRUE, TRUE)
								IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderTruckBlipID) != BLIPTYPE_VEHICLE
								OR GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.GangLeaderTruckBlipID) != MPGlobals.RemoteTruckV[NATIVE_TO_INT(GangLeaderID)][1]
									REMOVE_BLIP(g_PlayerBlipsData.GangLeaderTruckBlipID)
									CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader truck blip - not RemoteTruckV")
								ENDIF
							ENDIF
						ELSE
							IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderTruckBlipID) != BLIPTYPE_COORDS
								REMOVE_BLIP(g_PlayerBlipsData.GangLeaderTruckBlipID)
								CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader truck blip - not coord")
							ELIF NOT ARE_VECTORS_EQUAL(GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint, <<0,0,0>>)
								SET_BLIP_COORDS(g_PlayerBlipsData.GangLeaderTruckBlipID, GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint)
							ENDIF
						ENDIF
					ELIF IS_PLAYER_IN_BUNKER(PLAYER_ID())
						IF DOES_ENTITY_EXIST(g_viGunRunTailerInBunker)
							IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderTruckBlipID) != BLIPTYPE_VEHICLE
							OR GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.GangLeaderTruckBlipID) != g_viGunRunTailerInBunker
								REMOVE_BLIP(g_PlayerBlipsData.GangLeaderTruckBlipID)
								CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader truck blip - not g_viGunRunTailerInBunker")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// create the blip if it doesn't yet exist
				IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderTruckBlipID)
					IF IS_PLAYER_TRUCK_IN_FREEMODE(GangLeaderID)
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[NATIVE_TO_INT(GangLeaderID)][1])
							IF IS_VEHICLE_FUCKED_MP(MPGlobals.RemoteTruckV[NATIVE_TO_INT(GangLeaderID)][1])
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, not NOT IS_VEHICLE_FUCKED_MP(MPGlobals.RemoteTruckV[", GET_PLAYER_NAME(GangLeaderID), "][1])")
								EXIT
							ENDIF
							IF NOT IS_VEHICLE_EMPTY(MPGlobals.RemoteTruckV[NATIVE_TO_INT(GangLeaderID)][1], TRUE, TRUE)
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, not IS_VEHICLE_EMPTY(MPGlobals.RemoteTruckV[", GET_PLAYER_NAME(GangLeaderID), "][1], TRUE, TRUE)")
								EXIT
							ENDIF
							
							g_PlayerBlipsData.GangLeaderTruckBlipID = ADD_BLIP_FOR_ENTITY(MPGlobals.RemoteTruckV[NATIVE_TO_INT(GangLeaderID)][1])
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " truck blip - RemoteTruckV")
						ELSE
							IF ARE_VECTORS_EQUAL(GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint, <<0,0,0>>)
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, ARE_VECTORS_EQUAL(GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint, <<0,0,0>>)")
								EXIT
							ENDIF
							
							g_PlayerBlipsData.GangLeaderTruckBlipID = ADD_BLIP_FOR_COORD(GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " truck blip - coord")
						ENDIF
					ELIF IS_PLAYER_IN_BUNKER(PLAYER_ID())
						IF NOT DOES_ENTITY_EXIST(g_viGunRunTailerInBunker)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - g_viGunRunTailerInBunker doesn't exist yet ")
							EXIT
						ENDIF
						
						g_PlayerBlipsData.GangLeaderTruckBlipID = ADD_BLIP_FOR_ENTITY(g_viGunRunTailerInBunker)
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " truck blip - g_viGunRunTailerInBunker")
					ENDIF
					
					IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderTruckBlipID)
						SET_BLIP_SPRITE(g_PlayerBlipsData.GangLeaderTruckBlipID, RADAR_TRACE_GR_COVERT_OPS)
						SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderTruckBlipID, "GRTRUCK")
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderTruckBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))
						
						IF NOT bGangLeaderTruckBlipIDExists
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderTruckBlipID = TRUE
						ENDIF
						
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - added gang leader ", GET_PLAYER_NAME(GangLeaderID), " truck blip")
					ELSE
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - waiting to add gang leader ", GET_PLAYER_NAME(GangLeaderID), " truck blip")
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderTruckBlipID)
					// has sprite changed?
					IF UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(MPGlobals.RemoteTruckV[NATIVE_TO_INT(GangLeaderID)][1], g_PlayerBlipsData.GangLeaderTruckBlipID, TRUE)
						SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderTruckBlipID, GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(MPGlobals.RemoteTruckV[NATIVE_TO_INT(GangLeaderID)][1])))
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderTruckBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))						
					ENDIF

					// if player is inside bunker and the Truck_0 is on the roof hide it
					IF IS_PLAYER_IN_BUNKER(PLAYER_ID())	
					OR ARE_TEMP_SHORT_RANGE_BLIPS_ACTIVE()
						// is Truck_0 on roof?
						IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(GangLeaderID)
						OR ARE_TEMP_SHORT_RANGE_BLIPS_ACTIVE()
							SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderTruckBlipID, DISPLAY_RADAR_ONLY)		
						ELSE
							SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderTruckBlipID, DISPLAY_BOTH)	
						ENDIF		
					ELSE
						SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderTruckBlipID, DISPLAY_BOTH)
					ENDIF
				ENDIF
				
				BOOL bDisplayedGR_TRUCK_BLIP = FALSE
				IF g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderTruckBlipID
				AND DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderTruckBlipID)
					IF NOT (IS_PLAYER_RESPAWNING(PLAYER_ID()) 
					OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
					OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					OR NOT IS_SCREEN_FADED_IN()
					OR IS_HELP_MESSAGE_BEING_DISPLAYED()
					OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE) 
					OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0)
						PRINT_HELP("GR_TRUCK_BLIP")
						g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderTruckBlipID = FALSE
						bDisplayedGR_TRUCK_BLIP = TRUE
					ENDIF
				ENDIF

				IF NOT bDisplayedGR_TRUCK_BLIP
				AND NOT g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderTruckBlipID_G
				AND DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderTruckBlipID)
					IF NOT (IS_PLAYER_RESPAWNING(PLAYER_ID()) 
					OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
					OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					OR NOT IS_SCREEN_FADED_IN()
					OR IS_HELP_MESSAGE_BEING_DISPLAYED()
					OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE) 
					OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0)
						VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						VECTOR vBlipPos = GET_ACTUAL_BLIP_COORDS(g_PlayerBlipsData.GangLeaderTruckBlipID)
						IF VDIST(vPlayerPos, vBlipPos) <= 30.0
							PRINT_HELP("GR_TRUCK_BLIP_G")
						ENDIF
						g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderTruckBlipID_G = TRUE
					ENDIF
				ENDIF

				EXIT // exit here so blip doesn't get deleted below.
			ENDIF
		ENDIF
	ENDIF
				
	
	// if we've got this far in the proc then the blip shouldn't exist, so remove it
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderTruckBlipID)
		g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderTruckBlipID = FALSE
		REMOVE_BLIP(g_PlayerBlipsData.GangLeaderTruckBlipID)
		CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader truck blip")
	ENDIF
	
ENDPROC

PROC MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE()
	IF g_bBypass_MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE
		EXIT
	ENDIF
	
	IF (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE))
		PLAYER_INDEX GangLeaderID = GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID)
		IF (GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].bBlipMyPlaneVehicleForGangMembers)
			TEXT_LABEL_63 tl63Reason
			IF SHOULD_ADD_BLIP_FOR_OWNERS_AVENGER(GangLeaderID, tl63Reason)
				
				BOOL bGangLeaderPlaneBlipIDExists = FALSE
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderPlaneBlipID)
					bGangLeaderPlaneBlipIDExists = TRUE
					IF IS_PLAYER_PLANE_IN_FREEMODE(GangLeaderID)
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(GangLeaderID)])
							IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderPlaneBlipID) != BLIPTYPE_VEHICLE
								REMOVE_BLIP(g_PlayerBlipsData.GangLeaderPlaneBlipID)
								CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader plane blip - not vehicle")
							ELIF GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.GangLeaderPlaneBlipID) != MPGlobals.RemoteAvengerV[NATIVE_TO_INT(GangLeaderID)]
								REMOVE_BLIP(g_PlayerBlipsData.GangLeaderPlaneBlipID)
								CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader plane blip - not RemoteAvengerV")
							ENDIF
						ELSE
							IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderPlaneBlipID) != BLIPTYPE_COORDS
								REMOVE_BLIP(g_PlayerBlipsData.GangLeaderPlaneBlipID)
								CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader plane blip - not coord")
							ELIF NOT ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vArmoryAircraftLocation, <<0,0,0>>)
								SET_BLIP_COORDS(g_PlayerBlipsData.GangLeaderPlaneBlipID, GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vArmoryAircraftLocation)
							ENDIF
						ENDIF
					ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
						IF DOES_ENTITY_EXIST(g_viGangOpsAvengerInDefunctBase)
							IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderPlaneBlipID) != BLIPTYPE_VEHICLE
								REMOVE_BLIP(g_PlayerBlipsData.GangLeaderPlaneBlipID)
								CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader plane blip - not vehicle")
							ELIF GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.GangLeaderPlaneBlipID) != g_viGangOpsAvengerInDefunctBase
								REMOVE_BLIP(g_PlayerBlipsData.GangLeaderPlaneBlipID)
								CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader plane blip - not g_viGangOpsAvengerInDefunctBase")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// create the blip if it doesn't yet exist
				IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderPlaneBlipID)
					IF IS_PLAYER_PLANE_IN_FREEMODE(GangLeaderID)
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(GangLeaderID)])
							IF IS_VEHICLE_FUCKED_MP(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(GangLeaderID)])
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, not NOT IS_VEHICLE_FUCKED_MP(MPGlobals.RemoteAvengerV[", GET_PLAYER_NAME(GangLeaderID), "])")
								EXIT
							ENDIF
							IF NOT IS_VEHICLE_EMPTY(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(GangLeaderID)], TRUE, TRUE)
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, not IS_VEHICLE_EMPTY(MPGlobals.RemoteAvengerV[", GET_PLAYER_NAME(GangLeaderID), "], TRUE, TRUE)")
								EXIT
							ENDIF
							
							g_PlayerBlipsData.GangLeaderPlaneBlipID = ADD_BLIP_FOR_ENTITY(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(GangLeaderID)])
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " avenger blip - RemoteAvengerV ", NATIVE_TO_INT(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(GangLeaderID)]))
						ELSE
							IF ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vArmoryAircraftLocation, <<0,0,0>>)
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vArmoryAircraftLocation, <<0,0,0>>)")
								EXIT
							ENDIF
							
							g_PlayerBlipsData.GangLeaderPlaneBlipID = ADD_BLIP_FOR_COORD(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vArmoryAircraftLocation)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " avenger blip - coord ", GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vArmoryAircraftLocation)
						ENDIF
					ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
						IF NOT DOES_ENTITY_EXIST(g_viGangOpsAvengerInDefunctBase)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - g_viGangOpsAvengerInDefunctBase doesn't exist yet ")
							EXIT
						ENDIF
						
						g_PlayerBlipsData.GangLeaderPlaneBlipID = ADD_BLIP_FOR_ENTITY(g_viGangOpsAvengerInDefunctBase)
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " avenger blip - g_viGangOpsAvengerInDefunctBase ", NATIVE_TO_INT(g_viGangOpsAvengerInDefunctBase))
					ENDIF
					
					IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderPlaneBlipID)
						SET_BLIP_SPRITE(g_PlayerBlipsData.GangLeaderPlaneBlipID, GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(AVENGER))
						SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderPlaneBlipID, "GRPLANE")
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderPlaneBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))
						
						SET_BLIP_SCALE(g_PlayerBlipsData.GangLeaderPlaneBlipID, GET_BASE_SCALE_FOR_BLIP_SPRITE(GET_BLIP_SPRITE(g_PlayerBlipsData.GangLeaderPlaneBlipID)))
						
						IF NOT bGangLeaderPlaneBlipIDExists
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderPlaneBlipID = TRUE
						ENDIF
						
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - added gang leader ", GET_PLAYER_NAME(GangLeaderID), " avenger blip")
					ELSE
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - waiting to add gang leader ", GET_PLAYER_NAME(GangLeaderID), " avenger blip")
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderPlaneBlipID)
					// has sprite changed?
					IF UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(GangLeaderID)], g_PlayerBlipsData.GangLeaderPlaneBlipID, TRUE)
						SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderPlaneBlipID, GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(GangLeaderID)])))
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderPlaneBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))						
					ENDIF

					// if player is inside defunct base and the heli is on the roof hide it
					IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())	
					OR ARE_TEMP_SHORT_RANGE_BLIPS_ACTIVE()
						// is truck on roof?
						IF IS_OWNERS_ARMORY_AIRCRAFT_INSIDE_DEFUNCT_BASE(GangLeaderID)
						OR ARE_TEMP_SHORT_RANGE_BLIPS_ACTIVE()
							SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderPlaneBlipID, DISPLAY_RADAR_ONLY)		
						ELSE
							SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderPlaneBlipID, DISPLAY_BOTH)	
						ENDIF		
					ELSE
						SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderPlaneBlipID, DISPLAY_BOTH)
					ENDIF
				ENDIF
				
				BOOL bDisplayedGR_PLANE_BLIP = FALSE
				IF g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderPlaneBlipID
				AND DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderPlaneBlipID)
					IF NOT (IS_PLAYER_RESPAWNING(PLAYER_ID()) 
					OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
					OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					OR NOT IS_SCREEN_FADED_IN()
					OR IS_HELP_MESSAGE_BEING_DISPLAYED()
					OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE) 
					OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_DEFUNCT_BASE")) > 0)
						PRINT_HELP("GR_PLANE_BLIP")
						g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderPlaneBlipID = FALSE
						bDisplayedGR_PLANE_BLIP = TRUE
					ENDIF
				ENDIF

				IF NOT bDisplayedGR_PLANE_BLIP
				AND NOT g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderPlaneBlipID_G
				AND DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderPlaneBlipID)
					IF NOT (IS_PLAYER_RESPAWNING(PLAYER_ID()) 
					OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
					OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					OR NOT IS_SCREEN_FADED_IN()
					OR IS_HELP_MESSAGE_BEING_DISPLAYED()
					OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE) 
					OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_DEFUNCT_BASE")) > 0)
						IF !IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						OR IS_VEHICLE_ALLOWED_IN_ARMORY_WORKSHOP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
							VECTOR vBlipPos = GET_ACTUAL_BLIP_COORDS(g_PlayerBlipsData.GangLeaderPlaneBlipID)
							IF VDIST(vPlayerPos, vBlipPos) <= 30.0
								PRINT_HELP("GR_PLANE_BLIP_G")
							ENDIF
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderPlaneBlipID_G = TRUE
						ENDIF
					ENDIF
				ENDIF

				EXIT // exit here so blip doesn't get deleted below.
			ENDIF
		ENDIF
	ENDIF
				
	
	// if we've got this far in the proc then the blip shouldn't exist, so remove it
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderPlaneBlipID)
		g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderPlaneBlipID = FALSE
		REMOVE_BLIP(g_PlayerBlipsData.GangLeaderPlaneBlipID)
		CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader plane blip")
	ENDIF
	
ENDPROC

PROC MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE()
	IF g_bBypass_MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE
		EXIT
	ENDIF
	
	IF (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE))
		PLAYER_INDEX GangLeaderID = GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID)
		IF (GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].bBlipMyHackertruckVehicleForGangMembers)
			TEXT_LABEL_63 tl63Reason
			IF SHOULD_ADD_BLIP_FOR_OWNERS_HACKER_TRUCK(GangLeaderID, tl63Reason)
				
				BOOL bGangLeaderHackerTruckBlipIDExists = FALSE
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
				
					IF g_bHackerTruckBlipBlue
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, HUD_COLOUR_BLUE)
					ELSE
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))
					ENDIF
					
					bGangLeaderHackerTruckBlipIDExists = TRUE
					IF IS_PLAYER_HACKERTRUCK_IN_FREEMODE(GangLeaderID)
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)])
//							IF NOT IS_VEHICLE_FUCKED_MP(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)])
//							AND IS_VEHICLE_EMPTY(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)], TRUE, TRUE)
								IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderHackerTruckBlipID) != BLIPTYPE_VEHICLE
								OR GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.GangLeaderHackerTruckBlipID) != MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)]
									REMOVE_BLIP(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
									CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader hacker truck blip - not RemoteHackertruckV")
								ENDIF
//							ENDIF
						ELSE
							IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderHackerTruckBlipID) != BLIPTYPE_COORDS
								REMOVE_BLIP(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
								CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader hacker truck blip - not coord")
							ELIF NOT ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vHackerTruckLocation, <<0,0,0>>)
								SET_BLIP_COORDS(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vHackerTruckLocation)
							ENDIF
						ENDIF
					ELIF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
						IF DOES_ENTITY_EXIST(g_viHackerTruckInBusinessHub)
							IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderHackerTruckBlipID) != BLIPTYPE_VEHICLE
							OR GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.GangLeaderHackerTruckBlipID) != g_viHackerTruckInBusinessHub
								REMOVE_BLIP(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
								CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader hacker truck blip - not g_viHackerTruckInBusinessHub")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// create the blip if it doesn't yet exist
				IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
					IF IS_PLAYER_HACKERTRUCK_IN_FREEMODE(GangLeaderID)
						IF IS_HACKER_TRUCK_RETURNED_TO_BUSINESS_HUB(GangLeaderID)
							CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, IS_HACKER_TRUCK_RETURNED_TO_BUSINESS_HUB(", GET_PLAYER_NAME(GangLeaderID), ")")
							EXIT
						ENDIF
						
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)])
							IF IS_VEHICLE_FUCKED_MP(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)])
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, not NOT IS_VEHICLE_FUCKED_MP(MPGlobals.RemoteHackertruckV[", GET_PLAYER_NAME(GangLeaderID), "][1])")
								EXIT
							ENDIF
							IF NOT IS_VEHICLE_EMPTY(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)], TRUE, TRUE)
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, not IS_VEHICLE_EMPTY(MPGlobals.RemoteHackertruckV[", GET_PLAYER_NAME(GangLeaderID), "][1], TRUE, TRUE)")
								EXIT
							ENDIF
							
							g_PlayerBlipsData.GangLeaderHackerTruckBlipID = ADD_BLIP_FOR_ENTITY(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)])
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " hacker truck blip - RemoteHackertruckV")
						ELSE
							IF ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vHackerTruckLocation, <<0,0,0>>)
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vHackerTruckLocation, <<0,0,0>>)")
								EXIT
							ENDIF
							
							g_PlayerBlipsData.GangLeaderHackerTruckBlipID = ADD_BLIP_FOR_COORD(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vHackerTruckLocation)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " hacker truck blip - coord")
						ENDIF
					ELIF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
						IF NOT DOES_ENTITY_EXIST(g_viHackerTruckInBusinessHub)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - g_viHackerTruckInBusinessHub doesn't exist yet ")
							EXIT
						ENDIF
						
						g_PlayerBlipsData.GangLeaderHackerTruckBlipID = ADD_BLIP_FOR_ENTITY(g_viHackerTruckInBusinessHub)
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " hacker truck blip - g_viHackerTruckInBusinessHub")
					ENDIF
					
					IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
						SET_BLIP_SPRITE(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, RADAR_TRACE_BAT_WP1)
						SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, "BBTRUCK")
						
						IF g_bHackerTruckBlipBlue
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, HUD_COLOUR_BLUE)
						ELSE
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))
						ENDIF
						
						IF NOT bGangLeaderHackerTruckBlipIDExists
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderHackerTruckBlipID = TRUE
						ENDIF
						
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - added gang leader ", GET_PLAYER_NAME(GangLeaderID), " hacker truck blip")
					ELSE
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - waiting to add gang leader ", GET_PLAYER_NAME(GangLeaderID), " hacker truck blip (", PICK_STRING(IS_PLAYER_HACKERTRUCK_IN_FREEMODE(GangLeaderID), "truck in freemode", "truck NOT in freemode"), ", ", PICK_STRING(IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID()), "player in business hub", "player NOT in business hub"), ")")
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
					// has sprite changed?
					IF UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)], g_PlayerBlipsData.GangLeaderHackerTruckBlipID, TRUE)
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)])
							SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)])))
							
							IF g_bHackerTruckBlipBlue
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, HUD_COLOUR_BLUE)
							ELSE
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))		
							ENDIF
						ENDIF	
					ENDIF

					// if player is inside business hub and the hacker truck is on the roof hide it
					IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())	
					OR ARE_TEMP_SHORT_RANGE_BLIPS_ACTIVE()
						// is hacker truck on roof?
						IF IS_OWNERS_HACKER_TRUCK_INSIDE_BUSINESS_HUB(GangLeaderID)
						OR ARE_TEMP_SHORT_RANGE_BLIPS_ACTIVE()
							SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, DISPLAY_RADAR_ONLY)		
						ELSE
							SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, DISPLAY_BOTH)	
						ENDIF		
					ELSE
						SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderHackerTruckBlipID, DISPLAY_BOTH)
					ENDIF
				ENDIF
				
				BOOL bDisplayedBB_TRUCK_BLIP = FALSE
				IF g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderHackerTruckBlipID
				AND DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
					IF NOT (IS_PLAYER_RESPAWNING(PLAYER_ID()) 
					OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
					OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					OR NOT IS_SCREEN_FADED_IN()
					OR IS_HELP_MESSAGE_BEING_DISPLAYED()
					OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE) 
					OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0)
						
						VECTOR vBlipPos = GET_ACTUAL_BLIP_COORDS(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
						IF IS_VECTOR_ZERO(vBlipPos)
							PRINTLN("[MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE] print help, but blip is at origin? ", vBlipPos)
						ELIF (GET_PLAYERS_LAST_VEHICLE() = MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(GangLeaderID)])
							PRINTLN("[MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE] print help, but player was last in this hacker truck? ")
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderHackerTruckBlipID = FALSE
						ELIF IS_LOCAL_PLAYER_USING_DRONE()
							PRINTLN("[MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE] print help, but player is using drone? ")
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderHackerTruckBlipID = FALSE
						ELIF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FMBB_DATA_HACK
						OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FMBB_TARGET_PURSUIT
							PRINTLN("[MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE] print help, but current mission - ", GANG_BOSS_GET_NAME_FOR_TYPE_FOR_DEBUG_PRINT(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())), "? ")
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderHackerTruckBlipID = FALSE
						ELSE
							#IF IS_DEBUG_BUILD
							VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
							#ENDIF
//							IF VDIST(vPlayerPos, vBlipPos) <= 300.0
								#IF IS_DEBUG_BUILD
								PRINTLN("[MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE] print help BB_TRUCK_BLIP, dist (", vPlayerPos, ", ", vBlipPos, ") ", VDIST(vPlayerPos, vBlipPos))
								#ENDIF
								IF !IS_BIT_SET(g_iVehWeaponBS, VEH_WEAPON_GLOBAL_BIT_HAKCER_TRUCK_SPAWN_HELP)
									PRINT_HELP("BB_TRUCK_BLIP")
									SET_BIT(g_iVehWeaponBS, VEH_WEAPON_GLOBAL_BIT_HAKCER_TRUCK_SPAWN_HELP)
								ENDIF	
//							ELSE
//								PRINTLN("[MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE] print help BB_TRUCK_BLIPZ, \"", GET_NAME_OF_ZONE(vBlipPos), "\", dist (", vPlayerPos, ", ", vBlipPos, ") ", VDIST(vPlayerPos, vBlipPos))
//								PRINT_HELP_WITH_STRING("BB_TRUCK_BLIPZ", GET_NAME_OF_ZONE(vBlipPos))
//							ENDIF
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderHackerTruckBlipID = FALSE
							bDisplayedBB_TRUCK_BLIP = TRUE
						ENDIF
					ENDIF
				ENDIF

				IF NOT bDisplayedBB_TRUCK_BLIP
				AND NOT g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderHackerTruckBlipID_G
				AND DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
					IF NOT (IS_PLAYER_RESPAWNING(PLAYER_ID()) 
					OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
					OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					OR NOT IS_SCREEN_FADED_IN()
					OR IS_HELP_MESSAGE_BEING_DISPLAYED()
					OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE) 
					OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0)
						VECTOR vBlipPos = GET_ACTUAL_BLIP_COORDS(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
						IF IS_VECTOR_ZERO(vBlipPos)
							//
						ELSE
							VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
							IF VDIST(vPlayerPos, vBlipPos) <= 30.0
								PRINT_HELP("BB_TRUCK_BLIP_G")
							ENDIF
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderHackerTruckBlipID_G = TRUE
						ENDIF
					ENDIF
				ENDIF

				EXIT // exit here so blip doesn't get deleted below.
			ENDIF
		ENDIF
	ENDIF
				
	
	// if we've got this far in the proc then the blip shouldn't exist, so remove it
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
		g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderHackerTruckBlipID = FALSE
		REMOVE_BLIP(g_PlayerBlipsData.GangLeaderHackerTruckBlipID)
		CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader hacker truck blip")
	ENDIF
	
ENDPROC

#IF FEATURE_DLC_2_2022
PROC MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE()
	IF g_bBypass_MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE
		EXIT
	ENDIF
	
	IF (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE))
		PLAYER_INDEX GangLeaderID = GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID)
		IF (GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].bBlipMyAcidLabVehicleForGangMembers)
			TEXT_LABEL_63 tl63Reason
			IF SHOULD_ADD_BLIP_FOR_OWNERS_ACID_LAB(GangLeaderID, tl63Reason)
				
				BOOL bGangLeaderAcidLabBlipIDExists = FALSE
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
				
					IF g_bAcidLabBlipBlue
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderAcidLabBlipID, HUD_COLOUR_BLUE)
					ELSE
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderAcidLabBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))
					ENDIF
					
					bGangLeaderAcidLabBlipIDExists = TRUE
					IF IS_PLAYER_ACIDLAB_IN_FREEMODE(GangLeaderID)
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)])
//							IF NOT IS_VEHICLE_FUCKED_MP(MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)])
//							AND IS_VEHICLE_EMPTY(MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)], TRUE, TRUE)
								IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderAcidLabBlipID) != BLIPTYPE_VEHICLE
								OR GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.GangLeaderAcidLabBlipID) != MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)]
									REMOVE_BLIP(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
									CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader acid lab blip - not RemoteAcidLabV")
								ENDIF
//							ENDIF
						ELSE
							IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderAcidLabBlipID) != BLIPTYPE_COORDS
								REMOVE_BLIP(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
								CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader acid lab blip - not coord")
							ELIF NOT ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vAcidLabLocation, <<0,0,0>>)
								SET_BLIP_COORDS(g_PlayerBlipsData.GangLeaderAcidLabBlipID, GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vAcidLabLocation)
							ENDIF
						ENDIF
					ELIF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
						IF DOES_ENTITY_EXIST(g_viAcidLabInJuggaloWarehouse)
							IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderAcidLabBlipID) != BLIPTYPE_VEHICLE
							OR GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.GangLeaderAcidLabBlipID) != g_viAcidLabInJuggaloWarehouse
								REMOVE_BLIP(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
								CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader acid lab blip - not g_viAcidLabInJuggaloWarehouse")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// create the blip if it doesn't yet exist
				IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
					IF IS_PLAYER_ACIDLAB_IN_FREEMODE(GangLeaderID)
						IF IS_ACID_LAB_RETURNED_TO_JUGGALO_HIDEOUT(GangLeaderID)
							CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, IS_ACID_LAB_RETURNED_TO_JUGGALO_HIDEOUT(", GET_PLAYER_NAME(GangLeaderID), ")")
							EXIT
						ENDIF
						
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)])
							IF IS_VEHICLE_FUCKED_MP(MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)])
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, not NOT IS_VEHICLE_FUCKED_MP(MPGlobals.RemoteAcidLabV[", GET_PLAYER_NAME(GangLeaderID), "][1])")
								EXIT
							ENDIF
							IF NOT IS_VEHICLE_EMPTY(MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)], TRUE, TRUE)
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, not IS_VEHICLE_EMPTY(MPGlobals.RemoteAcidLabV[", GET_PLAYER_NAME(GangLeaderID), "][1], TRUE, TRUE)")
								EXIT
							ENDIF
							
							g_PlayerBlipsData.GangLeaderAcidLabBlipID = ADD_BLIP_FOR_ENTITY(MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)])
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " acid lab blip - RemoteAcidLabV")
						ELSE
							IF ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vAcidLablocation, <<0,0,0>>)
								CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, ARE_VECTORS_EQUAL(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vAcidLablocation, <<0,0,0>>)")
								EXIT
							ENDIF
							
							g_PlayerBlipsData.GangLeaderAcidLabBlipID = ADD_BLIP_FOR_COORD(GlobalplayerBD_FM_3[NATIVE_TO_INT(GangLeaderID)].vAcidLablocation)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " acid lab blip - coord")
						ENDIF
					ELIF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
						IF NOT DOES_ENTITY_EXIST(g_viAcidLabInJuggaloWarehouse)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - g_viAcidLabInJuggaloWarehouse doesn't exist yet ")
							EXIT
						ENDIF
						
						g_PlayerBlipsData.GangLeaderAcidLabBlipID = ADD_BLIP_FOR_ENTITY(g_viAcidLabInJuggaloWarehouse)
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - adding gang leader ", GET_PLAYER_NAME(GangLeaderID), " acid lab blip - g_viAcidLabInJuggaloWarehouse")
					ENDIF
					
					IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
						SET_BLIP_SPRITE(g_PlayerBlipsData.GangLeaderAcidLabBlipID, RADAR_TRACE_BAT_WP1)
						SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderAcidLabBlipID, "BBACIDLAB")
						
						IF g_bAcidLabBlipBlue
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderAcidLabBlipID, HUD_COLOUR_BLUE)
						ELSE
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderAcidLabBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))
						ENDIF
						
						IF NOT bGangLeaderAcidLabBlipIDExists
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderAcidLabBlipID = TRUE
						ENDIF
						
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - added gang leader ", GET_PLAYER_NAME(GangLeaderID), " acid lab blip")
					ELSE
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - waiting to add gang leader ", GET_PLAYER_NAME(GangLeaderID), " acid lab blip (", PICK_STRING(IS_PLAYER_ACIDLAB_IN_FREEMODE(GangLeaderID), "lab in freemode", "lab NOT in freemode"), ", ", PICK_STRING(IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID()), "player in juggalo warehouse", "player NOT in juggalo warehouse"), ")")
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
					// has sprite changed?
					IF UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)], g_PlayerBlipsData.GangLeaderAcidLabBlipID, TRUE)
						IF DOES_ENTITY_EXIST(MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)])
							SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderAcidLabBlipID, GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)])))
							
							IF g_bAcidLabBlipBlue
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderAcidLabBlipID, HUD_COLOUR_BLUE)
							ELSE
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderAcidLabBlipID, GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))		
							ENDIF
						ENDIF	
					ENDIF

					// if player is inside business hub and the AcidLab is on the roof hide it
					IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())	
					OR ARE_TEMP_SHORT_RANGE_BLIPS_ACTIVE()
						// is AcidLab on roof?
						IF IS_OWNERS_ACID_LAB_INSIDE_JUGGALO_HIDEOUT(GangLeaderID)
						OR ARE_TEMP_SHORT_RANGE_BLIPS_ACTIVE()
							SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderAcidLabBlipID, DISPLAY_RADAR_ONLY)		
						ELSE
							SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderAcidLabBlipID, DISPLAY_BOTH)	
						ENDIF		
					ELSE
						SET_BLIP_DISPLAY(g_PlayerBlipsData.GangLeaderAcidLabBlipID, DISPLAY_BOTH)
					ENDIF
				ENDIF
				
				BOOL bDisplayedBB_LAB_BLIP = FALSE
				IF g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderAcidLabBlipID
				AND DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
					IF NOT (IS_PLAYER_RESPAWNING(PLAYER_ID()) 
					OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
					OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					OR NOT IS_SCREEN_FADED_IN()
					OR IS_HELP_MESSAGE_BEING_DISPLAYED()
					OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE) 
					OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0)
						
						VECTOR vBlipPos = GET_ACTUAL_BLIP_COORDS(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
						IF IS_VECTOR_ZERO(vBlipPos)
							PRINTLN("[MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE] print help, but blip is at origin? ", vBlipPos)
						ELIF (GET_PLAYERS_LAST_VEHICLE() = MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(GangLeaderID)])
							PRINTLN("[MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE] print help, but player was last in this acid lab? ")
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderAcidLabBlipID = FALSE
						ELSE
							#IF IS_DEBUG_BUILD
							VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
							#ENDIF
//							IF VDIST(vPlayerPos, vBlipPos) <= 300.0
								#IF IS_DEBUG_BUILD
								PRINTLN("[MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE] print help BB_LAB_BLIP, dist (", vPlayerPos, ", ", vBlipPos, ") ", VDIST(vPlayerPos, vBlipPos))
								#ENDIF
								IF !IS_BIT_SET(g_iVehWeaponBS, VEH_WEAPON_GLOBAL_BIT_ACID_LAB_SPAWN_HELP)
									PRINT_HELP("BB_LAB_BLIP")
									SET_BIT(g_iVehWeaponBS, VEH_WEAPON_GLOBAL_BIT_ACID_LAB_SPAWN_HELP)
								ENDIF	
//							ELSE
//								PRINTLN("[MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE] print help BB_LAB_BLIPZ, \"", GET_NAME_OF_ZONE(vBlipPos), "\", dist (", vPlayerPos, ", ", vBlipPos, ") ", VDIST(vPlayerPos, vBlipPos))
//								PRINT_HELP_WITH_STRING("BB_LAB_BLIPZ", GET_NAME_OF_ZONE(vBlipPos))
//							ENDIF
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderAcidLabBlipID = FALSE
							bDisplayedBB_LAB_BLIP = TRUE
						ENDIF
					ENDIF
				ENDIF

				IF NOT bDisplayedBB_LAB_BLIP
				AND NOT g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderAcidLabBlipID_G
				AND DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
					IF NOT (IS_PLAYER_RESPAWNING(PLAYER_ID()) 
					OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
					OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					OR NOT IS_SCREEN_FADED_IN()
					OR IS_HELP_MESSAGE_BEING_DISPLAYED()
					OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE) 
					OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0)
						VECTOR vBlipPos = GET_ACTUAL_BLIP_COORDS(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
						IF IS_VECTOR_ZERO(vBlipPos)
							//
						ELSE
							VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
							IF VDIST(vPlayerPos, vBlipPos) <= 30.0
								PRINT_HELP("BB_LAB_BLIP_G")
							ENDIF
							g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderAcidLabBlipID_G = TRUE
						ENDIF
					ENDIF
				ENDIF

				EXIT // exit here so blip doesn't get deleted below.
			ENDIF
		ENDIF
	ENDIF
				
	
	// if we've got this far in the proc then the blip shouldn't exist, so remove it
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
		g_PlayerBlipsData.bDisplayShipmentBlipGangLeaderAcidLabBlipID = FALSE
		REMOVE_BLIP(g_PlayerBlipsData.GangLeaderAcidLabBlipID)
		CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader acid lab blip")
	ENDIF
	
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND

PROC CLEANUP_SUBMARINE_BLIP_FOR_REMOTE_PLAYER(INT RemotePlayerIndex, STRING Reason)
	#IF NOT IS_DEBUG_BUILD
		UNUSED_PARAMETER(Reason)
	#ENDIF
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex])
		REMOVE_BLIP(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex])
		CPRINTLN(DEBUG_BLIP, "[player_blips] CLEANUP_SUBMARINE_BLIP_FOR_REMOTE_PLAYER - removing blip for player: ", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, RemotePlayerIndex)), ", Reason: ", Reason)
	ENDIF
ENDPROC

PROC MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYER(PLAYER_INDEX RemotePlayerID)
	
	INT RemotePlayerIndex = NATIVE_TO_INT(RemotePlayerID)
	
	//Don't blip the local player or invalid remote players
	IF RemotePlayerID = MPGlobals.LocalPlayerID OR NOT IS_NET_PLAYER_OK(RemotePlayerID, FALSE, TRUE)
		CLEANUP_SUBMARINE_BLIP_FOR_REMOTE_PLAYER(RemotePlayerIndex, "Invalid Player")
		EXIT
	ENDIF
	
	//Don't blip gang bosses who don't want to be blipped
	PLAYER_INDEX GangLeaderID = GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID)
	BOOL bIsRemotePlayerMyBoss = (GangLeaderID = RemotePlayerID)
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE) AND bIsRemotePlayerMyBoss
		
		IF IS_PLAYER_IN_SUBMARINE_THEY_OWN(RemotePlayerID) //Hide as the player blip will indicate submarine location
		OR NOT (GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].bBlipMySubmarineVehicleForGangMembers)
			CLEANUP_SUBMARINE_BLIP_FOR_REMOTE_PLAYER(RemotePlayerIndex, "Gang leader is hiding blip")
			EXIT
		ENDIF
	ELSE
		//url:bugstar:6578587: Only blip gang boss submarines here, we'll blip other submarines via the player blip
		CLEANUP_SUBMARINE_BLIP_FOR_REMOTE_PLAYER(RemotePlayerIndex, "Not my gang leader")
		EXIT
	ENDIF
	
	//Don't blip if player/game state is invalid
	TEXT_LABEL_63 tl63Reason
	IF NOT SHOULD_ADD_BLIP_FOR_OWNERS_SUBMARINE(RemotePlayerID, tl63Reason)
		CLEANUP_SUBMARINE_BLIP_FOR_REMOTE_PLAYER(RemotePlayerIndex, tl63Reason)
		EXIT
	ENDIF
	
	VEHICLE_INDEX VehId = MPGlobals.RemoteSubmarineV[RemotePlayerIndex]
	BOOL bIsVehSubmerged = GlobalplayerBD_FM_4[RemotePlayerIndex].bIsSubmarineSubmerged
	BOOL bIsLocalSonarActive = IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_SONAR_SWEEP_SHOWN)
	
	//Hide non-boss submerged submarines - unless sonar is active
	IF DOES_ENTITY_EXIST(VehId)			
		IF ((NOT bIsRemotePlayerMyBoss AND bIsVehSubmerged) AND NOT bIsLocalSonarActive) 
			CLEANUP_SUBMARINE_BLIP_FOR_REMOTE_PLAYER(RemotePlayerIndex, "Submerged")
			EXIT
		ENDIF
	ENDIF
	
	VECTOR VehLocation = GlobalplayerBD_FM_4[RemotePlayerIndex].vSubmarineLocation
	FLOAT VehHeading = GlobalplayerBD_FM_4[RemotePlayerIndex].fSubmarineHeading
	BLIP_SPRITE BlipSprite = RADAR_TRACE_SUB2
	STRING BlipName = "BB_SUBMARINE"
	HUD_COLOURS BlipColour = GB_GET_PLAYER_GANG_HUD_COLOUR(RemotePlayerID)			
			
	BOOL bDidBlipExist = FALSE
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex])
		bDidBlipExist = TRUE
		IF DOES_ENTITY_EXIST(VehId)
			IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex]) != BLIPTYPE_VEHICLE
			OR GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex]) != VehId
				REMOVE_BLIP(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex])
				CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYER - removing blip - not vehicle blip type")
			ELSE	
				IF NOT IS_ENTITY_DEAD(VehId)
					VehHeading = GET_ENTITY_HEADING(VehId)
				ENDIF
			ENDIF
		ELSE
			IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex]) != BLIPTYPE_COORDS
				REMOVE_BLIP(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex])
				CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYER - removing blip - not coord blip type")
			ELIF NOT ARE_VECTORS_EQUAL(VehLocation, <<0,0,0>>)
				SET_BLIP_COORDS(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex], VehLocation)
			ENDIF
		ENDIF
	ENDIF
	
	// create the blip if it doesn't yet exist
	IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex])
		IF DOES_ENTITY_EXIST(VehId)
			g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex] = ADD_BLIP_FOR_ENTITY(VehId)
			CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYER - adding ENTITY blip for player: ", GET_PLAYER_NAME(RemotePlayerID))
		ELSE
			IF ARE_VECTORS_EQUAL(VehLocation, <<0,0,0>>)
				CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYER - don't add blip, invalid fallback location")
				EXIT
			ENDIF
			
			g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex] = ADD_BLIP_FOR_COORD(VehLocation)
			CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYER - adding COORD blip for player: ", GET_PLAYER_NAME(RemotePlayerID))
		ENDIF
		
		SET_BLIP_SPRITE(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex], BlipSprite)
		SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex], BlipName)
		SET_BLIP_PRIORITY(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex], BLIPPRIORITY_HIGHEST)
		
		//Not recreating
		IF NOT bDidBlipExist
			SET_BLIP_FLASHES(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex], TRUE)
			SET_BLIP_FLASH_TIMER(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex], 7000)
		ENDIF
	ENDIF				
	
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex])
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex], BlipColour)
		UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(VehId, g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex], TRUE)
		
		//Note: a "short range" blip is a blip which is seen only at a short range - i.e. does not clamp... obviously...
		BOOL bShortRange = (NOT bIsRemotePlayerMyBoss AND VDIST2(VehLocation, GET_PLAYER_COORDS(MPGlobals.LocalPlayerID)) > (1000*1000)) OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE, TRUE)
		SET_BLIP_AS_SHORT_RANGE(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex], bShortRange)
		
		SET_BLIP_ROTATION_WITH_FLOAT(g_PlayerBlipsData.RemotePlayerSubmarineBlipID[RemotePlayerIndex], VehHeading)
	ELSE
		CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYER - waiting to add blip for player: ", GET_PLAYER_NAME(RemotePlayerID))
	ENDIF
ENDPROC

PROC MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYERS()
	IF g_bBypass_MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYERS
		EXIT
	ENDIF
	
	//Maintain replicated submarine location
	IF (GET_FRAME_COUNT() % 10) = 0
		INT LocalPlayerIndex = NATIVE_TO_INT(PLAYER_ID())
		VEHICLE_INDEX VehId = MPGlobals.RemoteSubmarineV[LocalPlayerIndex]
		IF DOES_ENTITY_EXIST(VehId)
		AND NOT IS_ENTITY_DEAD(VehId)
			GlobalplayerBD_FM_4[LocalPlayerIndex].vSubmarineLocation = GET_ENTITY_COORDS(VehId)
			GlobalplayerBD_FM_4[LocalPlayerIndex].fSubmarineHeading = GET_ENTITY_HEADING(VehId)
			GlobalplayerBD_FM_4[LocalPlayerIndex].bIsSubmarineSubmerged = GET_ENTITY_SUBMERGED_LEVEL(VehId) >= 1.0
		ELSE
			// only clear the sub location if we are not in the process of warping / spawning 
			// AND if the player isn't 'in' the submarine.
			IF NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
			AND NOT IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID())
			AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID()) 
				GlobalplayerBD_FM_4[LocalPlayerIndex].vSubmarineLocation = <<0,0,0>>
				GlobalplayerBD_FM_4[LocalPlayerIndex].fSubmarineHeading = 0
				GlobalplayerBD_FM_4[LocalPlayerIndex].bIsSubmarineSubmerged = FALSE
			ELSE
				PRINTLN("MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYERS - player is in process of respawning / warping, dont clear just yet")
			ENDIF
		ENDIF
		
		//Flash local player blip when in a submerged submarine
		PED_INDEX LocalPlayerPed = PLAYER_PED_ID()
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX VehIAmIn = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF IS_VEHICLE_A_PERSONAL_SUBMARINE(VehIAmIn)
			AND GET_ENTITY_SUBMERGED_LEVEL(VehIAmIn) >= 1.0		
				IF NOT g_PlayerBlipsData.bFlashBlipForSubmergedSubmarine
					g_PlayerBlipsData.bFlashBlipForSubmergedSubmarine = TRUE
					FLASH_MY_PLAYER_ARROW(TRUE, -1, TRUE)
				ENDIF
			ELSE
				IF g_PlayerBlipsData.bFlashBlipForSubmergedSubmarine
					g_PlayerBlipsData.bFlashBlipForSubmergedSubmarine = FALSE
					FLASH_MY_PLAYER_ARROW(FALSE, -1, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Update remote players
	PLAYER_INDEX StaggeredRemotePlayer = INT_TO_NATIVE(PLAYER_INDEX, g_PlayerBlipsData.iBlipStaggerCount)
	MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYER(StaggeredRemotePlayer)
	
ENDPROC

PROC MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE()
	IF g_bBypass_MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE
		EXIT
	ENDIF
	
	IF (GET_FRAME_COUNT() % 10) = 0 AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		INT LocalPlayerIndex = NATIVE_TO_INT(PLAYER_ID())
		VEHICLE_INDEX VehId = MPGlobals.RemoteSubmarineDinghyV[LocalPlayerIndex]
		IF DOES_ENTITY_EXIST(VehId) AND NOT IS_ENTITY_DEAD(VehId)
			GlobalplayerBD_FM_4[LocalPlayerIndex].vSubmarineDinghyLocation = GET_ENTITY_COORDS(VehId)
			GlobalplayerBD_FM_4[LocalPlayerIndex].fSubmarineDinghyHeading = GET_ENTITY_HEADING(VehId)
		ELSE
			GlobalplayerBD_FM_4[LocalPlayerIndex].vSubmarineDinghyLocation = <<0,0,0>>
			GlobalplayerBD_FM_4[LocalPlayerIndex].fSubmarineDinghyHeading = 0
		ENDIF
	ELIF (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE))
		
		PLAYER_INDEX GangLeaderID = GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID)
		IF (GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].bBlipMySubmarineDinghyVehicleForGangMembers)
		
			TEXT_LABEL_63 tl63Reason
			IF SHOULD_ADD_BLIP_FOR_OWNERS_SUBMARINE_DINGHY(GangLeaderID, tl63Reason)
				
				VEHICLE_INDEX VehId = MPGlobals.RemoteSubmarineDinghyV[NATIVE_TO_INT(GangLeaderID)]
				VECTOR VehLocation = GlobalplayerBD_FM_4[NATIVE_TO_INT(GangLeaderID)].vSubmarineDinghyLocation
				FLOAT VehHeading = GlobalplayerBD_FM_4[NATIVE_TO_INT(GangLeaderID)].fSubmarineDinghyHeading
				BLIP_SPRITE BlipSprite = RADAR_TRACE_PLAYER_BOAT
				STRING BlipName = "BB_DINGHY"
				HUD_COLOURS BlipColour = GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID)
				
				BOOL bDidBlipExist = FALSE
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID)
					bDidBlipExist = TRUE
					IF DOES_ENTITY_EXIST(VehId)
						IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID) != BLIPTYPE_VEHICLE
						OR GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID) != VehId
							REMOVE_BLIP(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader blip - not vehicle blip type")
						ENDIF
					ELSE
						IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID) != BLIPTYPE_COORDS
							REMOVE_BLIP(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader blip - not coord blip type")
						ELIF NOT ARE_VECTORS_EQUAL(VehLocation, <<0,0,0>>)
							SET_BLIP_COORDS(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID, VehLocation)
							SET_BLIP_ROTATION_WITH_FLOAT(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID, VehHeading)
						ENDIF
					ENDIF
				ENDIF
				
				// create the blip if it doesn't yet exist
				IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID)
					IF DOES_ENTITY_EXIST(VehId)
						IF IS_VEHICLE_FUCKED_MP(VehId)
							CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, vehicle is fucked up for gang leader: ", GET_PLAYER_NAME(GangLeaderID))
							EXIT
						ENDIF
						IF NOT IS_VEHICLE_EMPTY(VehId, TRUE, TRUE)
							CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, vehicle is not empty for gang leader: ", GET_PLAYER_NAME(GangLeaderID))
							EXIT
						ENDIF
						
						g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID = ADD_BLIP_FOR_ENTITY(VehId)
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE - adding ENTITY blip for gang leader: ", GET_PLAYER_NAME(GangLeaderID))
					ELSE
						IF ARE_VECTORS_EQUAL(VehLocation, <<0,0,0>>)
							CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, invalid fallback location")
							EXIT
						ENDIF
						
						g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID = ADD_BLIP_FOR_COORD(VehLocation)
						SET_BLIP_ROTATION_WITH_FLOAT(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID, VehHeading)
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE - adding COORD blip for gang leader: ", GET_PLAYER_NAME(GangLeaderID))
					ENDIF
					
					SET_BLIP_SPRITE(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID, BlipSprite)
					SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID, BlipName)
					
					//Not recreating
					IF NOT bDidBlipExist
						SET_BLIP_FLASHES(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID, TRUE)
						SET_BLIP_FLASH_TIMER(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID, 7000)
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID)					
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID, BlipColour)
					UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(VehId, g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID, TRUE)
				ELSE
					CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE - waiting to add blip for gang leader: ", GET_PLAYER_NAME(GangLeaderID))
				ENDIF

				EXIT // exit here so blip doesn't get deleted below.
			ENDIF
		ENDIF
	ENDIF
				
	// if we've got this far in the proc then the blip shouldn't exist, so remove it
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID)
		REMOVE_BLIP(g_PlayerBlipsData.GangLeaderSubmarineDinghyBlipID)
		CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader blip")
	ENDIF
ENDPROC

#ENDIF

#IF FEATURE_DLC_2_2022
PROC MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE()
	IF g_bBypass_MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE
		EXIT
	ENDIF
	
	IF (GET_FRAME_COUNT() % 10) = 0 AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		INT LocalPlayerIndex = NATIVE_TO_INT(PLAYER_ID())
		VEHICLE_INDEX VehId = MPGlobals.RemoteSupportBikeV[LocalPlayerIndex]
		IF DOES_ENTITY_EXIST(VehId) AND NOT IS_ENTITY_DEAD(VehId)
			GlobalplayerBD_FM_4[LocalPlayerIndex].vSupportBikeLocation = GET_ENTITY_COORDS(VehId)
			GlobalplayerBD_FM_4[LocalPlayerIndex].fSupportBikeHeading = GET_ENTITY_HEADING(VehId)
		ELSE
			GlobalplayerBD_FM_4[LocalPlayerIndex].vSupportBikeLocation = <<0,0,0>>
			GlobalplayerBD_FM_4[LocalPlayerIndex].fSupportBikeHeading = 0
		ENDIF
	ELIF (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE))
		
		PLAYER_INDEX GangLeaderID = GB_GET_THIS_PLAYER_GANG_BOSS(MPGlobals.LocalPlayerID)
		IF (GlobalplayerBD[NATIVE_TO_INT(GangLeaderID)].bBlipMySupportBikeVehicleForGangMembers)
		
			TEXT_LABEL_63 tl63Reason
			IF SHOULD_ADD_BLIP_FOR_OWNERS_SUPPORT_BIKE(GangLeaderID, tl63Reason)
				
				VEHICLE_INDEX VehId = MPGlobals.RemoteSupportBikeV[NATIVE_TO_INT(GangLeaderID)]
				VECTOR VehLocation = GlobalplayerBD_FM_4[NATIVE_TO_INT(GangLeaderID)].vSupportBikeLocation
				FLOAT VehHeading = GlobalplayerBD_FM_4[NATIVE_TO_INT(GangLeaderID)].fSupportBikeHeading
				// @bty : todo - BLIP add correct blip sprite
				BLIP_SPRITE BlipSprite = RADAR_TRACE_GANG_BIKE
				STRING BlipName = "BB_ALPV"
				HUD_COLOURS BlipColour = GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID)
				
				BOOL bDidBlipExist = FALSE
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderSupportBikeBlipID)
					bDidBlipExist = TRUE
					IF DOES_ENTITY_EXIST(VehId)
						IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderSupportBikeBlipID) != BLIPTYPE_VEHICLE
						OR GET_BLIP_INFO_ID_ENTITY_INDEX(g_PlayerBlipsData.GangLeaderSupportBikeBlipID) != VehId
							REMOVE_BLIP(g_PlayerBlipsData.GangLeaderSupportBikeBlipID)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader blip - not vehicle blip type")
						ENDIF
					ELSE
						IF GET_BLIP_INFO_ID_TYPE(g_PlayerBlipsData.GangLeaderSupportBikeBlipID) != BLIPTYPE_COORDS
							REMOVE_BLIP(g_PlayerBlipsData.GangLeaderSupportBikeBlipID)
							CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader blip - not coord blip type")
						ELIF NOT ARE_VECTORS_EQUAL(VehLocation, <<0,0,0>>)
							SET_BLIP_COORDS(g_PlayerBlipsData.GangLeaderSupportBikeBlipID, VehLocation)
//							SET_BLIP_ROTATION_WITH_FLOAT(g_PlayerBlipsData.GangLeaderSupportBikeBlipID, VehHeading)
						ENDIF
					ENDIF
				ENDIF
				
				// create the blip if it doesn't yet exist
				IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderSupportBikeBlipID)
					IF DOES_ENTITY_EXIST(VehId)
						IF IS_VEHICLE_FUCKED_MP(VehId)
							CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, vehicle is fucked up for gang leader: ", GET_PLAYER_NAME(GangLeaderID))
							EXIT
						ENDIF
						IF NOT IS_VEHICLE_EMPTY(VehId, TRUE, TRUE)
							CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, vehicle is not empty for gang leader: ", GET_PLAYER_NAME(GangLeaderID))
							EXIT
						ENDIF
						
						g_PlayerBlipsData.GangLeaderSupportBikeBlipID = ADD_BLIP_FOR_ENTITY(VehId)
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE - adding ENTITY blip for gang leader: ", GET_PLAYER_NAME(GangLeaderID))
					ELSE
						IF ARE_VECTORS_EQUAL(VehLocation, <<0,0,0>>)
							CDEBUG1LN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE - don't add blip, invalid fallback location")
							EXIT
						ENDIF
						
						g_PlayerBlipsData.GangLeaderSupportBikeBlipID = ADD_BLIP_FOR_COORD(VehLocation)
						SET_BLIP_ROTATION_WITH_FLOAT(g_PlayerBlipsData.GangLeaderSupportBikeBlipID, VehHeading)
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE - adding COORD blip for gang leader: ", GET_PLAYER_NAME(GangLeaderID))
					ENDIF
					
					SET_BLIP_SPRITE(g_PlayerBlipsData.GangLeaderSupportBikeBlipID, BlipSprite)
					SET_BLIP_NAME_FROM_TEXT_FILE(g_PlayerBlipsData.GangLeaderSupportBikeBlipID, BlipName)
					
					//Not recreating
					IF NOT bDidBlipExist
						SET_BLIP_FLASHES(g_PlayerBlipsData.GangLeaderSupportBikeBlipID, TRUE)
						SET_BLIP_FLASH_TIMER(g_PlayerBlipsData.GangLeaderSupportBikeBlipID, 7000)
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderSupportBikeBlipID)					
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_PlayerBlipsData.GangLeaderSupportBikeBlipID, BlipColour)
					UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(VehId, g_PlayerBlipsData.GangLeaderSupportBikeBlipID, TRUE)
				ELSE
					CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE - waiting to add blip for gang leader: ", GET_PLAYER_NAME(GangLeaderID))
				ENDIF

				EXIT // exit here so blip doesn't get deleted below.
			ENDIF
		ENDIF
	ENDIF
				
	// if we've got this far in the proc then the blip shouldn't exist, so remove it
	IF DOES_BLIP_EXIST(g_PlayerBlipsData.GangLeaderSupportBikeBlipID)
		REMOVE_BLIP(g_PlayerBlipsData.GangLeaderSupportBikeBlipID)
		CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE - removing gang leader blip")
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL ShouldHideBlipForDisplacedInterior(BLIP_INDEX BlipID #IF IS_DEBUG_BUILD, BLIP_SPRITE BlipSprite #ENDIF )
	
	FLOAT fDiff
	FLOAT fZDiff
	FLOAT fDistSquared = 90000 // 300m
		
	// need to increase the distance when using the arena. 
	// url:bugstar:5391154 - Arena - CTF - Local Player was unable to see the flag blips until expanding the radar
	IF IS_PLAYER_USING_ARENA() 
		RETURN FALSE
	ENDIF		

	VECTOR vBlipCoords = GET_ACTUAL_BLIP_COORDS(BlipID)
	VECTOR vPlayerCoords = GET_PLAYER_COORDS(MPGlobals.LocalPlayerID)


	
	#IF FEATURE_HEIST_ISLAND
	IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
		// check if blip is one the island or not
		IF ((vBlipCoords.x > 2954.0) AND (vBlipCoords.y < -3552.0))
			RETURN FALSE
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[player_blips] ShouldHideBlipForDisplacedInterior - player is on heist island. BlipSprite ", ENUM_TO_INT(BlipSprite), ", BlipID ", NATIVE_TO_INT(BlipID), ", vBlipCoords ", vBlipCoords)	
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF


	fZDiff = vBlipCoords.z - vPlayerCoords.z
	
	#IF IS_DEBUG_BUILD
		UNUSED_PARAMETER(BlipSprite)
		//PRINTLN("[player_blips] ShouldHideBlipForDisplacedInterior - fZDiff ", fZDiff, " BlipSprite ", ENUM_TO_INT(BlipSprite), ", BlipID ", NATIVE_TO_INT(BlipID), ", vBlipCoords ", vBlipCoords, ", vPlayerCoords ", vPlayerCoords)
	#ENDIF
	
	vBlipCoords.z = 0.0 // ignore z
	vPlayerCoords.z = 0.0 // ignore z
	fDiff = VDIST2(vBlipCoords, vPlayerCoords)
	
	#IF IS_DEBUG_BUILD					
		//PRINTLN("[player_blips] ShouldHideBlipForDisplacedInterior - fDiff ", fDiff)
	#ENDIF	
		

	IF ((fDiff < fDistSquared) AND (fZDiff > 50.0))
	OR ((IS_BLIP_SHORT_RANGE(BlipID)=FALSE)	AND (fDiff > fDistSquared))
		#IF IS_DEBUG_BUILD					
			PRINTLN("[player_blips] ShouldHideBlipForDisplacedInterior - fDiff = ", fDiff, ", fZDiff ", fZDiff, " BlipSprite ", ENUM_TO_INT(BlipSprite), ", BlipID ", NATIVE_TO_INT(BlipID), ", vBlipCoords ", vBlipCoords, ", vPlayerCoords ", vPlayerCoords)
			PRINTLN("[player_blips] ShouldHideBlipForDisplacedInterior - TRUE ")
		#ENDIF		
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD					
		//PRINTLN("[player_blips] ShouldHideBlipForDisplacedInterior - FALSE ")
	#ENDIF		
	RETURN FALSE
			
ENDFUNC

FUNC BOOL IsBlipAPlayerBlipIShouldDisplay(BLIP_INDEX BlipID)

	// is it a moc sprite?
	IF (GET_BLIP_SPRITE(BlipID) =  RADAR_TRACE_GR_COVERT_OPS)

		// is it a player blip?
		INT iPlayerInt
		REPEAT NUM_NETWORK_PLAYERS iPlayerInt
			IF (BlipID = g_PlayerBlipsData.playerBlips[iPlayerInt])
				PLAYER_INDEX PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt)
				
				// are we in same property?
				IF ARE_PLAYERS_IN_SAME_PROPERTY(PlayerID, MPGlobals.LocalPlayerID, TRUE)
				OR IS_PLAYER_INSIDE_TRUCK_IN_BUNKER_I_AM_IN(PlayerID)
					PRINTLN("[player_blips] IsBlipAPlayerBlipIShouldDisplay - true for blip = ", NATIVE_TO_INT(BlipID), ", playerint ", iPlayerInt)
					RETURN TRUE	
				ENDIF
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	// is it a personal aircraft sprite?
	IF (GET_BLIP_SPRITE(BlipID) =  GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(AVENGER))

		// is it a player blip?
		INT iPlayerInt
		REPEAT NUM_NETWORK_PLAYERS iPlayerInt
			IF (BlipID = g_PlayerBlipsData.playerBlips[iPlayerInt])
				PLAYER_INDEX PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt)
				
				// are we in same property?
				IF ARE_PLAYERS_IN_SAME_PROPERTY(PlayerID, MPGlobals.LocalPlayerID, TRUE)
				OR IS_PLAYER_INSIDE_ARMORY_AIRCRAFT_IN_DEFUNCT_BASE_I_AM_IN(PlayerID)
					PRINTLN("[player_blips] IsBlipAPlayerBlipIShouldDisplay - true for blip = ", NATIVE_TO_INT(BlipID), ", playerint ", iPlayerInt)
					RETURN TRUE	
				ENDIF
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	// is it a moc sprite?
	IF (GET_BLIP_SPRITE(BlipID) =  GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_HACKER_TRUCK_MODEL()))

		// is it a player blip?
		INT iPlayerInt
		REPEAT NUM_NETWORK_PLAYERS iPlayerInt
			IF (BlipID = g_PlayerBlipsData.playerBlips[iPlayerInt])
				PLAYER_INDEX PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt)
				
				// are we in same property?
				IF ARE_PLAYERS_IN_SAME_PROPERTY(PlayerID, MPGlobals.LocalPlayerID, TRUE)
				OR IS_PLAYER_INSIDE_HACKER_TRUCK_IN_BUSINESS_HUB_I_AM_IN(PlayerID)
					PRINTLN("[player_blips] IsBlipAPlayerBlipIShouldDisplay - true for blip = ", NATIVE_TO_INT(BlipID), ", playerint ", iPlayerInt)
					RETURN TRUE	
				ENDIF
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	#IF FEATURE_DLC_2_2022
	// is it a ACID LAB sprite?
	IF (GET_BLIP_SPRITE(BlipID) =  GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_ACID_LAB_MODEL()))

		// is it a player blip?
		INT iPlayerInt
		REPEAT NUM_NETWORK_PLAYERS iPlayerInt
			IF (BlipID = g_PlayerBlipsData.playerBlips[iPlayerInt])
				PLAYER_INDEX PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayerInt)
				
				// are we in same property?
				IF ARE_PLAYERS_IN_SAME_PROPERTY(PlayerID, MPGlobals.LocalPlayerID, TRUE)
				OR IS_PLAYER_INSIDE_ACID_LAB_IN_JUGGALO_HIDEOUT_I_AM_IN(PlayerID)
					PRINTLN("[player_blips] IsBlipAPlayerBlipIShouldDisplay - true for blip = ", NATIVE_TO_INT(BlipID), ", playerint ", iPlayerInt)
					RETURN TRUE	
				ENDIF
			ENDIF
		ENDREPEAT
		
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RemoveBlipFromTempMapDisplayOnlyBlipArray(INT iIndex)
	
	// move all the rest up one.
	INT i = iIndex
	INT iNext
	
	g_PlayerBlipsData.TempMapDisplayOnlyBlip[iIndex] = INT_TO_NATIVE(BLIP_INDEX, 0)
	
	WHILE (i < g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips)
	
		iNext = i + 1
	
		IF (iNext < g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips)
			
			g_PlayerBlipsData.TempMapDisplayOnlyBlip[i]	 = g_PlayerBlipsData.TempMapDisplayOnlyBlip[iNext]			
			g_PlayerBlipsData.TempMapDisplayOnlyBlip[iNext] = INT_TO_NATIVE(BLIP_INDEX, 0)
	
		ENDIF
		
		i++
	ENDWHILE
	
	g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips += -1
	#IF IS_DEBUG_BUILD
	g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count += -1
	#ENDIF
	
	PRINTLN("[player_blips] CleanTempMapDisplayOnlyBlipArray - reduced g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips to ", g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips, " by removing index ", iIndex)
	#IF IS_DEBUG_BUILD
	PRINTLN("[player_blips] CleanTempMapDisplayOnlyBlipArray - reduced g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count to ", g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count, " by removing index ", iIndex)
	#ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC OutputTempMapDisplayOnlyArray()
	PRINTLN("[player_blips] OutputTempMapDisplayOnlyArray - g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips = ", g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips)
	INT i
	REPEAT NUMBER_OF_TEMP_HIDDEN_BLIPS i
		PRINTLN("[player_blips] OutputTempMapDisplayOnlyArray - g_PlayerBlipsData.TempMapDisplayOnlyBlip[", i, "] = ", NATIVE_TO_INT(g_PlayerBlipsData.TempMapDisplayOnlyBlip[i]))
	ENDREPEAT
ENDPROC
#ENDIF

FUNC BOOL IsBlipInTempMapDisplayOnlyArray(BLIP_INDEX BlipID)
	INT i
	REPEAT g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips i
		IF (g_PlayerBlipsData.TempMapDisplayOnlyBlip[i] = BlipId)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC CleanTempMapDisplayOnlyBlipArray()
	
	INT iThisFrame = GET_FRAME_COUNT()
	INT i
	
	IF NOT (g_PlayerBlipsData.iFrame_CleanTempMapDisplayOnlyBlipArray = iThisFrame)
	
		// first loop through array and remove blips that no longer exist
		REPEAT g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips i
			IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.TempMapDisplayOnlyBlip[i])
				RemoveBlipFromTempMapDisplayOnlyBlipArray(i)	
				i-- // array has moved up so need to consider this slot again.
			ENDIF
		ENDREPEAT
		
		
		// make sure g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips is correct
		#IF IS_DEBUG_BUILD
		INT iCount
		REPEAT NUMBER_OF_TEMP_HIDDEN_BLIPS i
			IF DOES_BLIP_EXIST(g_PlayerBlipsData.TempMapDisplayOnlyBlip[i])	
				iCount++	
			ENDIF
		ENDREPEAT
		IF NOT (iCount = g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips)
			PRINTLN("[player_blips] CleanTempMapDisplayOnlyBlipArray, iCount = ", iCount, " g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips = ", g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips)
			SCRIPT_ASSERT("[player_blips] CleanTempMapDisplayOnlyBlipArray - count mismatch")
		ENDIF
		#ENDIF
	
		g_PlayerBlipsData.iFrame_CleanTempMapDisplayOnlyBlipArray = iThisFrame
		PRINTLN("[player_blips] CleanTempMapDisplayOnlyBlipArray - successfully run, g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips = ", g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips)
		
	ELSE
		PRINTLN("[player_blips] CleanTempMapDisplayOnlyBlipArray - already run this frame")
	ENDIF

ENDPROC


PROC StoreSpriteBlipsAsTempHidden(BLIP_SPRITE BlipSprite)

	IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID()) 
	#IF FEATURE_HEIST_ISLAND
	AND NOT IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
	#ENDIF
		EXIT
	ENDIF

	BLIP_INDEX BlipID
	BlipID = GET_FIRST_BLIP_INFO_ID(BlipSprite)
	INT iCount = 0
	WHILE DOES_BLIP_EXIST(BlipID)		
	
		IF ShouldHideBlipForDisplacedInterior(BlipID  #IF IS_DEBUG_BUILD , BlipSprite #ENDIF )
		
			// set and store as map only
			BLIP_DISPLAY BlipDisplay = GET_BLIP_INFO_ID_DISPLAY(BlipID)
			IF (BlipDisplay = DISPLAY_BOTH)
			
				CleanTempMapDisplayOnlyBlipArray()
											
				IF NOT IsBlipInTempMapDisplayOnlyArray(BlipID)
				
					IF (g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips < NUMBER_OF_TEMP_HIDDEN_BLIPS)												
						g_PlayerBlipsData.TempMapDisplayOnlyBlip[g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips] = BlipID
						g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips++
						PRINTLN("[player_blips] StoreSpriteBlipsAsTempHidden - ADDED")
					ELSE
						PRINTLN("[player_blips] StoreSpriteBlipsAsTempHidden - REACHED MAXIMUM")
					ENDIF
					
					PRINTLN("[player_blips] StoreSpriteBlipsAsTempHidden - BlipSprite ", ENUM_TO_INT(BlipSprite), " iCount = ", iCount, ", BlipID ", NATIVE_TO_INT(BlipID), " g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips = ", g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips)	
											
					#IF IS_DEBUG_BUILD
					g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count++
					PRINTLN("[player_blips] StoreSpriteBlipsAsTempHidden - g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count = ", g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count)
					#ENDIF
					
									
				ELSE
					PRINTLN("[player_blips] StoreSpriteBlipsAsTempHidden - ALREADY ADDED")						
				ENDIF
										
				SET_BLIP_DISPLAY(BlipID, DISPLAY_NOTHING)
					
			ENDIF

		ENDIF
		
		iCount++
		BlipID = GET_NEXT_BLIP_INFO_ID(BlipSprite)
	ENDWHILE
	
	// release build
	IF iCount = 0
	ENDIF
ENDPROC

PROC RESTORE_ALL_TEMP_HIDDEN_BLIPS()
	PRINTLN("[player_blips] RESTORE_ALL_TEMP_HIDDEN_BLIPS called ")
	INT i
	
	REPEAT g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips i
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.TempMapDisplayOnlyBlip[i])
			SET_BLIP_DISPLAY(g_PlayerBlipsData.TempMapDisplayOnlyBlip[i], DISPLAY_BOTH)			
			PRINTLN("[player_blips] RESTORE_ALL_TEMP_HIDDEN_BLIPS - restoring DISPLAY blip i ", i, " blipid = ", NATIVE_TO_INT(g_PlayerBlipsData.TempMapDisplayOnlyBlip[i]))
			g_PlayerBlipsData.TempMapDisplayOnlyBlip[i] = INT_TO_NATIVE(BLIP_INDEX, 0)
		ENDIF
	ENDREPEAT
	g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips = 0
	#IF IS_DEBUG_BUILD
		g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count = 0
	#ENDIF
	g_PlayerBlipsData.bSetAllBlipsAsTempShortRangeCalled = FALSE
ENDPROC

PROC KEEP_TEMP_SHORT_RANGE_BLIPS_AS_SHORT_RANGE()
	
	INT i

	REPEAT g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips i
		IF DOES_BLIP_EXIST(g_PlayerBlipsData.TempMapDisplayOnlyBlip[i])
			IF NOT (GET_BLIP_INFO_ID_DISPLAY(g_PlayerBlipsData.TempMapDisplayOnlyBlip[i]) = DISPLAY_MAP)
				SET_BLIP_DISPLAY(g_PlayerBlipsData.TempMapDisplayOnlyBlip[i], DISPLAY_MAP)	
				PRINTLN("[player_blips] KEEP_TEMP_SHORT_RANGE_BLIPS_AS_SHORT_RANGE - resetting DISPLAY blip i ", i, " blipid = ", NATIVE_TO_INT(g_PlayerBlipsData.TempMapDisplayOnlyBlip[i]))
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC DO_NUMBER_OF_TEMP_HIDDEN_BLIPS_CHECK()
	
	PRINTLN("[player_blips] DO_NUMBER_OF_TEMP_HIDDEN_BLIPS_CHECK - g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips = ", g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips)
	PRINTLN("[player_blips] DO_NUMBER_OF_TEMP_HIDDEN_BLIPS_CHECK - g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count = ", g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count)
	PRINTLN("[player_blips] DO_NUMBER_OF_TEMP_HIDDEN_BLIPS_CHECK - NUMBER_OF_TEMP_HIDDEN_BLIPS = ", NUMBER_OF_TEMP_HIDDEN_BLIPS)


	IF (g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count > NUMBER_OF_TEMP_HIDDEN_BLIPS)
		DEBUG_PRINTCALLSTACK()
		SCRIPT_ASSERT("[player_blips] DO_NUMBER_OF_TEMP_HIDDEN_BLIPS_CHECK - need to increase size of NUMBER_OF_TEMP_HIDDEN_BLIPS")
		
		OutputTempMapDisplayOnlyArray()
		
	ENDIF
	
ENDPROC
#ENDIF

/// PURPOSE:
///     
/// RETURNS: returns FALSE if it coulnd't be processed yet.
///    
FUNC BOOL SET_ALL_BLIPS_AS_TEMP_HIDDEN()
	

	IF (g_PlayerBlipsData.bSetAllBlipsAsTempShortRangeCalled)
		// has already been processed.
		IF IS_MINIMAP_RENDERING() 
			RETURN FALSE
		ELSE
			g_PlayerBlipsData.bSetAllBlipsAsTempShortRangeCalled = FALSE
			PRINTLN("[player_blips] SET_ALL_BLIPS_AS_TEMP_HIDDEN - no longer rendering.")
		ENDIF
	ENDIF

	// wait until we're in the main running state before running this, so that any blips created before get checked.
	IF NOT IS_MINIMAP_RENDERING() 
		PRINTLN("[player_blips] SET_ALL_BLIPS_AS_TEMP_HIDDEN - IS_MINIMAP_RENDERING = ", IS_MINIMAP_RENDERING() )
		RETURN FALSE
	ENDIF
	
	// start afresh
	RESTORE_ALL_TEMP_HIDDEN_BLIPS()

	INT i
	
	// these relate to the enum in commands_ny, we are checking for all blip sprites between these 2 values.
	INT iStartSprite = ENUM_TO_INT(RADAR_TRACE_OBJECTIVE)
	INT iEndSprite = ENUM_TO_INT(MAX_RADAR_TRACES)
	
   	REPEAT COUNT_OF(BLIP_SPRITE) i
		IF (i >= iStartSprite)
		AND (i <= iEndSprite)
			StoreSpriteBlipsAsTempHidden(INT_TO_ENUM(BLIP_SPRITE, i))
		ENDIF
	ENDREPEAT	
	
	#IF IS_DEBUG_BUILD
		DO_NUMBER_OF_TEMP_HIDDEN_BLIPS_CHECK()
	#ENDIF
	
	g_PlayerBlipsData.bSetAllBlipsAsTempShortRangeCalled = TRUE
	RETURN TRUE

ENDFUNC

PROC UPDATE_TEMP_HIDDEN_BLIPS_STAGGER()
	
	// these relate to the enum in commands_ny, we are checking for all blip sprites between these 2 values.
	INT iStartSprite = ENUM_TO_INT(RADAR_TRACE_OBJECTIVE)
	INT iEndSprite = ENUM_TO_INT(MAX_RADAR_TRACES)
	
	IF (g_PlayerBlipsData.iTempShortRangeSpriteStagger < iStartSprite)
		g_PlayerBlipsData.iTempShortRangeSpriteStagger = iStartSprite
		#IF IS_DEBUG_BUILD
			g_PlayerBlipsData.iNumberTempMapDisplayOnlyBlips_count = 0
		#ENDIF
	ENDIF
	
	StoreSpriteBlipsAsTempHidden(INT_TO_ENUM(BLIP_SPRITE, g_PlayerBlipsData.iTempShortRangeSpriteStagger)) 
	
	g_PlayerBlipsData.iTempShortRangeSpriteStagger++
	IF (g_PlayerBlipsData.iTempShortRangeSpriteStagger >= iEndSprite)
		g_PlayerBlipsData.iTempShortRangeSpriteStagger = iStartSprite
		
		#IF IS_DEBUG_BUILD
			DO_NUMBER_OF_TEMP_HIDDEN_BLIPS_CHECK()
		#ENDIF		
	ENDIF
	
	
ENDPROC


PROC UPDATE_TEMP_SHORT_RANGE_BLIPS()

	// redo all blips on map view change
	IF (g_PlayerBlipsData.bTempShortRange_InBigOrExtendedView)
		IF NOT IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
			RESTORE_ALL_TEMP_HIDDEN_BLIPS()	
			g_PlayerBlipsData.bTempShortRange_InBigOrExtendedView = FALSE
			PRINTLN("[player_blips] UPDATE_TEMP_SHORT_RANGE_BLIPS just changed to normal view") 
		ENDIF
	ELSE
		IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
			RESTORE_ALL_TEMP_HIDDEN_BLIPS()	
			g_PlayerBlipsData.bTempShortRange_InBigOrExtendedView = TRUE
			PRINTLN("[player_blips] UPDATE_TEMP_SHORT_RANGE_BLIPS just changed to zoomed out radar or map") 
		ENDIF	
	ENDIF



	#IF FEATURE_HEIST_ISLAND
	IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
		IF NOT SET_ALL_BLIPS_AS_TEMP_HIDDEN()
			UPDATE_TEMP_HIDDEN_BLIPS_STAGGER()
		ENDIF
	ELSE
	#ENDIF
		
		// has just entered displaced property?
		IF (IS_PLAYER_IN_DISPLACED_PROPERTY(MPGlobals.LocalPlayerID  , TRUE )
		AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR())
		AND NOT (g_bActiveInGunTurret)
		AND NOT IsUsingSpecialWeaponCamera()
		AND NOT	NETWORK_IS_IN_MP_CUTSCENE()
			IF NOT SET_ALL_BLIPS_AS_TEMP_HIDDEN()
				UPDATE_TEMP_HIDDEN_BLIPS_STAGGER()
			ENDIF
		ELSE
			RESTORE_ALL_TEMP_HIDDEN_BLIPS()	
		ENDIF

	#IF FEATURE_HEIST_ISLAND
	ENDIF
	#ENDIF	
	

ENDPROC


PROC MAINTAIN_GENERAL_BLIP_INFO()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("MAINTAIN_GENERAL_BLIP_INFO")
	#ENDIF
	#ENDIF

	VEHICLE_INDEX CarID
	PLAYER_INDEX PlayerID
	INT i
	BLIP_SPRITE ThisVehicleBlipSprite

	#IF IS_DEBUG_BUILD
		g_PlayerBlipsData.iLocalPlayerID = NATIVE_TO_INT(MPGlobals.LocalPlayerID)	
	#ENDIF

	// stagger
	g_PlayerBlipsData.iBlipStaggerCount += 1
	IF (g_PlayerBlipsData.iBlipStaggerCount >= NUM_NETWORK_PLAYERS)
		g_PlayerBlipsData.iBlipStaggerCount = 0
	ENDIF


	// if the local player dies, leave behind a cross
	IF NOT IS_NET_PLAYER_OK(MPGlobals.LocalPlayerID)
		IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.deadPlayerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)])
		AND IS_SCREEN_FADED_OUT()
		AND NOT SHOULD_HIDE_DEAD_PLAYER_BLIPS()
			MAKE_PLAYERS_BLIP_A_DEAD_BLIP(MPGlobals.LocalPlayerID)
			g_PlayerBlipsData.bPlayerDeathLocationBlipSet = TRUE
		ELSE
			g_PlayerBlipsData.iBlipTimerDeadFading[NATIVE_TO_INT(MPGlobals.LocalPlayerID)] = GET_NETWORK_TIME()	
			
			// remove blip from 2 deaths ago if it hadn't cleared yet. 
			IF NOT(g_PlayerBlipsData.bPlayerDeathLocationBlipSet)
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.deadPlayerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)])
					REMOVE_BLIP(g_PlayerBlipsData.deadPlayerBlips[NATIVE_TO_INT(MPGlobals.LocalPlayerID)])
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_PLAYER_RESPAWNING(MPGlobals.LocalPlayerID)
			g_PlayerBlipsData.iBlipTimerDeadFading[NATIVE_TO_INT(MPGlobals.LocalPlayerID)] = GET_NETWORK_TIME()				
		ELSE
			g_PlayerBlipsData.bPlayerDeathLocationBlipSet = FALSE
		ENDIF
	ENDIF

	// needs to be processed for everyone regardless of if they are in the game or not.
	// A player could quit out just after they die, the blip should still be processed. 
	HANDLE_DEAD_PLAYER_BLIPS() 
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("HANDLE_DEAD_PLAYER_BLIPS()")
	#ENDIF
	#ENDIF	

	MAINTAIN_LEFT_PLAYERS_BLIPS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LEFT_PLAYERS_BLIPS()")
	#ENDIF
	#ENDIF	
	
	MAINTAIN_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()")
	#ENDIF
	#ENDIF
	
	IF (g_PlayerBlipsData.bClearAllBlips)
		CLEANUP_ALL_PLAYER_BLIPS_private()
		g_PlayerBlipsData.bClearAllBlips = FALSE
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("CLEANUP_ALL_PLAYER_BLIPS_private()")
	#ENDIF
	#ENDIF	
	
	// are blips hidden?
	IF (g_PlayerBlipsData.bHideAllPlayerBlips)
		IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideAllPlayerBlipsThreadID)
			CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - HideAllPlayerBlipsThreadID no longer active, making player blips visible. ")
			g_PlayerBlipsData.bHideAllPlayerBlips = FALSE
			g_PlayerBlipsData.HideAllPlayerBlipsThreadID = INT_TO_NATIVE(THREADID, -1)
		ENDIF
	ENDIF
	
	// are blips shown
	IF (g_PlayerBlipsData.bShowAllPlayerBlips)
		IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.ShowAllPlayerBlipsThreadID)
			CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - ShowAllPlayerBlipsThreadID no longer active, resetting. ")
			g_PlayerBlipsData.bShowAllPlayerBlips = FALSE
			g_PlayerBlipsData.ShowAllPlayerBlipsThreadID = INT_TO_NATIVE(THREADID, -1)
		ENDIF
	ENDIF	
	
	// store my loudest sound for the last 3 seconds
	FLOAT fCurrentNoise = GET_PLAYER_CURRENT_STEALTH_NOISE(MPGlobals.LocalPlayerID)
	IF (fCurrentNoise > g_PlayerBlipsData.fMyLoudNoise)
	OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.MyLoudNoiseTime) > LOCAL_NOISE_STORE_TIME)
		g_PlayerBlipsData.fMyLoudNoise = fCurrentNoise
		g_PlayerBlipsData.MyLoudNoiseTime = GET_NETWORK_TIME()
	ENDIF
	
	// pause menu?
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_PAUSE_MENU_ACTIVE)
			DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
		ENDIF
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_PAUSE_MENU_ACTIVE)
	ELSE
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_PAUSE_MENU_ACTIVE)
			DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
		ENDIF
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_PAUSE_MENU_ACTIVE)
	ENDIF
	IF IS_PAUSEMAP_IN_INTERIOR_MODE()
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_PAUSE_MENU_MAP_INTERIOR_MODE)
			DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
		ENDIF
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_PAUSE_MENU_MAP_INTERIOR_MODE)
	ELSE
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_PAUSE_MENU_MAP_INTERIOR_MODE)
			DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()
		ENDIF
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_PAUSE_MENU_MAP_INTERIOR_MODE)
	ENDIF
	
	
	IF g_PlayerBlipsData.bBigMapIsActive
		// just opened the big map?
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_BIG_MAP_IS_ACTIVE)
			DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()	
		ENDIF
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_BIG_MAP_IS_ACTIVE)
	ELSE
		// just closed big map?
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_BIG_MAP_IS_ACTIVE)
			DO_FORCED_UPDATE_OF_ALL_PLAYER_BLIPS_THIS_FRAME()	
		ENDIF
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_BIG_MAP_IS_ACTIVE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER(" setting bits")
	#ENDIF
	#ENDIF
	
	
	UPDATE_PLAYERS_ARROW()	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_PLAYERS_ARROW")
	#ENDIF
	#ENDIF	
	
	MAINTAIN_PAUSE_MENU_CONDITIONS_FOR_BLIPS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PAUSE_MENU_CONDITIONS_FOR_BLIPS")
	#ENDIF
	#ENDIF		
	
//	IF IS_PLAYER_IN_DISPLACED_PROPERTY(MPGlobals.LocalPlayerID)
//		g_PlayerBlipsData.bHideExternalBlips = TRUE
//	ELSE
//		g_PlayerBlipsData.bHideExternalBlips = FALSE
//	ENDIF
	
	// moved to get called from freemode AFTER RUN_RADAR_MAP
	UPDATE_TEMP_SHORT_RANGE_BLIPS()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_TEMP_SHORT_RANGE_BLIPS")
	#ENDIF
	#ENDIF	
	
	IF IS_SKYSWOOP_AT_GROUND()
	
		g_TransitionSessionNonResetVars.iDisplacedSimpleInterior = -1
		
		IF (g_TransitionSessionNonResetVars.bInsideDisplacedInterior)
			IF NOT IS_PLAYER_IN_DISPLACED_PROPERTY(PLAYER_ID())
				CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - bInsideDisplacedInterior - no longer inside displaced interior")		
				ClearInsideDisplacedInteriorFlag()
			ELSE
				g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords = GetPlayerDisplacedCoords(PLAYER_ID())					
				IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
					g_TransitionSessionNonResetVars.iDisplacedSimpleInterior = ENUM_TO_INT(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
				ENDIF
			ENDIF
		ELSE
			IF IS_PLAYER_IN_DISPLACED_PROPERTY(PLAYER_ID())						
				g_TransitionSessionNonResetVars.bInsideDisplacedInterior = TRUE
				g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords = GetPlayerDisplacedCoords(PLAYER_ID())
				
				IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
					g_TransitionSessionNonResetVars.iDisplacedSimpleInterior = ENUM_TO_INT(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
				ENDIF
				
				CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - bInsideDisplacedInterior - entered displaced interior, percieved coords ", g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords)		
			ELSE
				#IF FEATURE_HEIST_ISLAND
				// if the player is on the island, set the displaced coords back at the airport
				IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
					g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords = g_vHeistIslandSkyCamLocation //<< -1038.0243, -2737.7842, 19.1693 >>
				ENDIF
				#ENDIF			
			ENDIF
		ENDIF
	ENDIF
	
	// if the state changes send an update
	IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.bInsideDisplacedInterior != g_TransitionSessionNonResetVars.bInsideDisplacedInterior)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.bInsideDisplacedInterior = g_TransitionSessionNonResetVars.bInsideDisplacedInterior
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.vDisplacedPerceivedCoords = g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords	
	ELSE
		// if inside a displaced interior, update the perceived coords every 2 seconds. url:bugstar:5802328 - Ignorable Assert - Error: 0: freemode, Time(3578441) (array 9). Owner LINAdamDL. Id: 2.: the local player broadcast data been altered too frequently - does it contain a timer?
		IF (g_TransitionSessionNonResetVars.bInsideDisplacedInterior)
			IF (GET_GAME_TIMER() - g_PlayerBlipsData.iDisplacedInteriorBDTimer > 2000)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.vDisplacedPerceivedCoords = g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords	
				g_PlayerBlipsData.iDisplacedInteriorBDTimer = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("displacement")
	#ENDIF
	#ENDIF		
	
	IF (MPGlobals.LocalPlayerID = PLAYER_ID())
		
		// broadcast if player is in the arena or not
		IF IS_PLAYER_USING_ARENA()
		
			IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_IN_ARENA)
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_IN_ARENA)
				CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - player is in arena.")
			ENDIF
		
			IF IS_IN_ARENA_SPECTATOR_BOOTH()
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_ARENA_SPECTATOR)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_ARENA_SPECTATOR)
					CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - player is an arena spectator.")
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_ARENA_SPECTATOR)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_ARENA_SPECTATOR)
					CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - player is no longer an arena spectator.")
				ENDIF			
			ENDIF
		ELSE
		
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_IN_ARENA)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_IN_ARENA)
				CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - player is not in arena.")
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_ARENA_SPECTATOR)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_ARENA_SPECTATOR)
				CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - player is no longer an arena spectator. (2)")
			ENDIF
			
		ENDIF
		
		// force on the sonar blips if on dm or mission
		IF ((g_b_On_Deathmatch)
		OR (g_bFM_ON_TEAM_DEATHMATCH)
		OR (g_bFM_ON_TEAM_MISSION)
		OR (g_bFM_ON_IMPROMPTU_DEATHMATCH)	
		OR IS_ARENA_WARS_JOB()
		OR SHOULD_SHOW_PLAYER_SONAR())
		AND NOT IS_SONAR_DISABLED_BY_MISSION_CONTROLLER()
			CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - should show player sonar. ")
			AllowSonarBlips(TRUE)		
		ELSE
			AllowSonarBlips(FALSE)
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bIsScreenFadedOut = TRUE
		ELSE
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bIsScreenFadedOut = FALSE	
		ENDIF
		
		// set if the player is shooting
		IF NOT IS_PED_INJURED(GET_PLAYER_PED(MPGlobals.LocalPlayerID))
		AND IS_PED_SHOOTING(GET_PLAYER_PED(MPGlobals.LocalPlayerID))
		AND IS_SCREEN_FADED_IN()
			//CPRINTLN(DEBUG_BLIP, "[player_blips] setting GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsShooting = TRUE")
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsShooting)
			IF IS_PED_CURRENT_WEAPON_SILENCED(GET_PLAYER_PED(MPGlobals.LocalPlayerID))
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsUsingSilencedWeapon)
			ELSE
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsUsingSilencedWeapon)
			ENDIF
			
			MPGlobals.g_iPlayerIsShootingTime = GET_NETWORK_TIME()
		ELSE
			IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsShooting))
				IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , MPGlobals.g_iPlayerIsShootingTime) > 1500
				OR NOT IS_SCREEN_FADED_IN()
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsShooting)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsUsingSilencedWeapon)
				ENDIF
			ENDIF
		ENDIF
	
		// set if the player is chatting
		IF NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
		AND NETWORK_IS_PLAYER_TALKING(PLAYER_ID())
		AND IS_SCREEN_FADED_IN()
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsChattingOnHeadset)
			MPGlobals.g_iPlayerIsChattingTime = GET_NETWORK_TIME()
		ELSE
			IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsChattingOnHeadset))
				IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , MPGlobals.g_iPlayerIsChattingTime) > 1500
				OR NOT IS_SCREEN_FADED_IN()
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsChattingOnHeadset)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorSubmarine)
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				INTERIOR_INSTANCE_INDEX iTemp = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
				
				IF IS_VALID_INTERIOR(iTemp)
					IF iTemp = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<511.5731, 4827.4204, -63.5879>>, "xm_x17dlc_int_sub")
						SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorSubmarine)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bInMissionCreatorSubmarine = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
				INTERIOR_INSTANCE_INDEX iTemp = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
				
				IF IS_VALID_INTERIOR(iTemp)
					IF iTemp != GET_INTERIOR_AT_COORDS_WITH_TYPE(<<511.5731, 4827.4204, -63.5879>>, "xm_x17dlc_int_sub")
						CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorSubmarine)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bInMissionCreatorSubmarine = FALSE
					ENDIF
				ELSE
					CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorSubmarine)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bInMissionCreatorSubmarine = FALSE
				ENDIF
			ELSE
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorSubmarine)//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bInMissionCreatorSubmarine = FALSE
			ENDIF
		ENDIF
		
		// is player in a displaced interior.
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
		OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorSubmarine)
			IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_IN_DISPLACED_HEIST_APARTMENT)
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_IN_DISPLACED_HEIST_APARTMENT)
				CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - player has entered heist apartment.")
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_IN_DISPLACED_HEIST_APARTMENT)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_IN_DISPLACED_HEIST_APARTMENT)	
				CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - player has left heist apartment.")
			ENDIF
		ENDIF
	
		// set if the player is making car noise
		IF IS_PLAYER_MAKING_CAR_NOISE()
		AND IS_SCREEN_FADED_IN()
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsMakingVehicleNoise)
			MPGlobals.g_iPlayerIsMakingCarNoiseTime = GET_NETWORK_TIME()
		ELSE
			IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsMakingVehicleNoise))
				IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , MPGlobals.g_iPlayerIsMakingCarNoiseTime) > 1500
				OR NOT IS_SCREEN_FADED_IN()
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsMakingVehicleNoise)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MPGlobalsAmbience.iOffRadarBitset, biOffRadar_Gang)
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)

				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadarGangMode = FALSE
				PLAYER_INDEX pidGangBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
				IF NOT (pidGangBoss = INT_TO_NATIVE(PLAYER_INDEX, -1))
					IF (GlobalplayerBD[NATIVE_TO_INT(pidGangBoss)].bOffTheRadar)
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadarGangMode = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadarGangMode 
				PRINTLN("[GB][AMEC] MAINTAIN_GENERAL_BLIP_INFO - biOffRadar_Gang is NOT set but bOffTheRadarGangMode = TRUE. Resetting radar state for local player.")
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadarGangMode = FALSE
			ENDIF
		ENDIF
		
		// count how many players are in my vehicle
		#IF IS_DEBUG_BUILD
		IF NOT (g_PlayerBlipsData.bTestIncarNumbers)
		#ENDIF 
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iNumberOfPlayersInMyCar = 0
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadarPlayerInSameCar  = FALSE
							
			// check driver
			IF NOT IS_VEHICLE_SEAT_FREE(CarID, VS_DRIVER )
				PlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(CarID, VS_DRIVER))
				IF NOT (PlayerID = INT_TO_NATIVE(PLAYER_INDEX, -1))				
					IF NOT IS_PLAYER_BLIP_HIDDEN_FROM_ME(PlayerID)
					AND NOT IS_PLAYER_MY_HIDDEN_GANG_BOSS(PlayerID)
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iNumberOfPlayersInMyCar++
					ENDIF
					IF (GlobalplayerBD[NATIVE_TO_INT(playerID)].bOffTheRadar)
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadarPlayerInSameCar = TRUE
					ENDIF
				ENDIF
			ENDIF
				
			// loop through passenger
			REPEAT GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(CarID) i
				IF NOT IS_VEHICLE_SEAT_FREE(CarID, INT_TO_ENUM(VEHICLE_SEAT, i) )					
					PlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(CarID, INT_TO_ENUM(VEHICLE_SEAT, i)))
					IF NOT (PlayerID = INT_TO_NATIVE(PLAYER_INDEX, -1))
						IF NOT IS_PLAYER_BLIP_HIDDEN_FROM_ME(PlayerID)	
						AND NOT IS_PLAYER_MY_HIDDEN_GANG_BOSS(PlayerID)
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iNumberOfPlayersInMyCar++
						ENDIF
						IF (GlobalplayerBD[NATIVE_TO_INT(playerID)].bOffTheRadar)
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadarPlayerInSameCar = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT	
			
		ELSE
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iNumberOfPlayersInMyCar = 0
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bOffTheRadarPlayerInSameCar  = FALSE
		ENDIF
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF 		

	
		// is player shopping?
		IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_SHOPPING)
			IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD) 
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IN_CAR_MOD_SHOP)
			ELSE
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IN_CAR_MOD_SHOP)	
			ENDIF
		ELSE
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_SHOPPING)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IN_CAR_MOD_SHOP)
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("is ped browsing items in any shop")
		#ENDIF
		#ENDIF
		
		// ballistics
		IF IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID())  
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_BALLISTIC)
		ELSE
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_BALLISTIC)
		ENDIF		

		MAINTAIN_REMOTE_PLAYER_SHIPMENT_BLIPS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REMOTE_PLAYER_SHIPMENT_BLIPS")
		#ENDIF
		#ENDIF	
				
		// Maintain the shipment blips - can maintain over unique frames because they read other players bd
		SWITCH g_PlayerBlipsData.iBlipUniqueFrame
			CASE 0
				MAINTAIN_REMOTE_PLAYER_BOUNTY_BLIPS()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_REMOTE_PLAYER_BOUNTY_BLIPS")
				#ENDIF
				#ENDIF	
			BREAK
			CASE 1
				MAINTAIN_PEGASUS_BLIP_FOR_GANG_LEADER_VEHICLE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PEGASUS_BLIP_FOR_GANG_LEADER_VEHICLE")
				#ENDIF
				#ENDIF
			BREAK
			CASE 2
				MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE")
				#ENDIF
				#ENDIF
			BREAK
			CASE 3
				MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_AIRCRAFT_BLIP_FOR_GANG_LEADER_VEHICLE")
				#ENDIF
				#ENDIF
			BREAK
			CASE 4
				MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_HACKER_TRUCK_BLIP_FOR_GANG_LEADER_VEHICLE")
				#ENDIF
				#ENDIF
			BREAK
			CASE 5
				MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SUBMARINE_DINGHY_BLIP_FOR_GANG_LEADER_VEHICLE")
				#ENDIF
				#ENDIF
			BREAK
			#IF FEATURE_DLC_2_2022
			CASE 6
				MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SUPPORT_BIKE_BLIP_FOR_GANG_LEADER_VEHICLE")
				#ENDIF
				#ENDIF
			BREAK
			CASE 7
				MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ACID_LAB_BLIP_FOR_GANG_LEADER_VEHICLE")
				#ENDIF
				#ENDIF
			BREAK
			#ENDIF
		ENDSWITCH
		
		g_PlayerBlipsData.iBlipUniqueFrame++
		IF (g_PlayerBlipsData.iBlipUniqueFrame > 8)
			g_PlayerBlipsData.iBlipUniqueFrame = 0
		ENDIF
		
		MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYERS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SUBMARINE_BLIP_FOR_REMOTE_PLAYERS")
		#ENDIF
		#ENDIF
		
		// does player have control on		
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_HAS_CONTROL_ON)
		ELSE
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_HAS_CONTROL_ON)
		ENDIF
					
		// is player in a vehicle? check what blip type he should have
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT (GB_IS_PLAYER_ON_MEGA_BUSINESS_SELL_MISSION(PLAYER_ID()) AND GB_IS_PLAYER_SHIPMENT(PLAYER_ID()))
			CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(CarID)
				ThisVehicleBlipSprite = GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(CarID))
				
				// clear all PBBD_ vehicle bits incase warp directlyl form one car to another
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = 0
				
				SWITCH ThisVehicleBlipSprite
					CASE RADAR_TRACE_TANK
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_TANK
					BREAK
					CASE RADAR_TRACE_PLAYER_JET
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_JET
					BREAK
					CASE RADAR_TRACE_PLAYER_PLANE
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_AIRPLANE
					BREAK
					CASE RADAR_TRACE_PLAYER_HELI
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_HELI
					BREAK
					CASE RADAR_TRACE_PLAYER_GUNCAR
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_GUNTURRET
					BREAK
					CASE RADAR_TRACE_PLAYER_BOAT
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_BOAT
					BREAK
					CASE RADAR_TRACE_EX_VECH_6
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_VOLTIC2
					BREAK
					CASE RADAR_TRACE_EX_VECH_7
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_TECHNICAL2
					BREAK
					CASE RADAR_TRACE_EX_VECH_3
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_RUINER2
					BREAK
					CASE RADAR_TRACE_EX_VECH_4
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_DUNE4
					BREAK
					CASE RADAR_TRACE_EX_VECH_1
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_PHANTOM2
					BREAK
					CASE RADAR_TRACE_EX_VECH_2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_BOXVILLE5
					BREAK
					CASE RADAR_TRACE_EX_VECH_5
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_WASTELANDER
					BREAK
					CASE RADAR_TRACE_QUAD
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_QUAD
					BREAK
					CASE RADAR_TRACE_GR_WVM_1
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_APC
					BREAK
					CASE RADAR_TRACE_GR_WVM_2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_OPPRESSOR
					BREAK
					CASE RADAR_TRACE_GR_WVM_3
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_HALFTRACK
					BREAK
					CASE RADAR_TRACE_GR_WVM_4
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_DUNE3
					BREAK
					CASE RADAR_TRACE_GR_WVM_5
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_TAMPA3
					BREAK
					CASE RADAR_TRACE_GR_WVM_6
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_TRAILERSMALL2
					BREAK
					CASE RADAR_TRACE_SM_WP1
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_ALPHAZ1
					BREAK
					CASE RADAR_TRACE_SM_WP2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_BOMBUSHKA
					BREAK
					CASE RADAR_TRACE_SM_WP3
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_HAVOK
					BREAK
					CASE RADAR_TRACE_SM_WP4
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_HOWARD
					BREAK
					CASE RADAR_TRACE_SM_WP5
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_HUNTER
					BREAK
					CASE RADAR_TRACE_SM_WP6
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_MICROLIGHT
					BREAK
					CASE RADAR_TRACE_SM_WP7
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_MOGUL
					BREAK
					CASE RADAR_TRACE_SM_WP8
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_MOLOTOK
					BREAK
					CASE RADAR_TRACE_SM_WP9
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_NOKOTA
					BREAK
					CASE RADAR_TRACE_SM_WP10
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_PYRO
					BREAK
					CASE RADAR_TRACE_SM_WP11
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_ROGUE
					BREAK
					CASE RADAR_TRACE_SM_WP12
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_STARLING
					BREAK
					CASE RADAR_TRACE_SM_WP13
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_SEABREEZE
					BREAK
					CASE RADAR_TRACE_SM_WP14
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_TULA
					BREAK
					CASE RADAR_TRACE_NHP_WP1
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_STROMBERG
					BREAK
					CASE RADAR_TRACE_NHP_WP2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_DELUXO
					BREAK
					CASE RADAR_TRACE_NHP_WP3
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_THRUSTER
					BREAK
					CASE RADAR_TRACE_NHP_WP4
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_KHANJALI
					BREAK
					CASE RADAR_TRACE_NHP_WP5
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_RIOT2
					BREAK
					CASE RADAR_TRACE_NHP_WP6
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_VOLATOL
					BREAK
					CASE RADAR_TRACE_NHP_WP7
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_BARRAGE
					BREAK
					CASE RADAR_TRACE_NHP_WP8
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_AKULA
					BREAK
					CASE RADAR_TRACE_NHP_WP9
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_CHERNOBOG
					BREAK
					CASE RADAR_TRACE_NHP_VEH1
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_AVENGER
					BREAK
					CASE RADAR_TRACE_TURRETED_LIMO
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_TURRETED_LIMO
					BREAK
					CASE RADAR_TRACE_BAT_PBUS
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_PBUS
					BREAK
					CASE RADAR_TRACE_BAT_WP1
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_TERBYTE
					BREAK
					CASE RADAR_TRACE_BAT_WP2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_MENACER
					BREAK
					CASE RADAR_TRACE_BAT_WP3
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_SCRAMJET
					BREAK
					CASE RADAR_TRACE_BAT_WP4
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_POUNDER2
					BREAK
					CASE RADAR_TRACE_BAT_WP5
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_MULE4
					BREAK
					CASE RADAR_TRACE_BAT_WP6
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_SPEEDO4
					BREAK
					CASE RADAR_TRACE_BAT_WP7
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_STRIKEFORCE
					BREAK
					CASE RADAR_TRACE_OPPRESSOR_2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_OPPRESSOR2
					BREAK
					CASE RADAR_TRACE_ARENA_BRUISER
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_BRUISER
					BREAK
					CASE RADAR_TRACE_ARENA_BRUTUS
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_BRUTUS
					BREAK
					CASE RADAR_TRACE_ARENA_CERBERUS
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_CERBERUS
					BREAK
					CASE RADAR_TRACE_ARENA_DEATHBIKE
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_DEATHBIKE
					BREAK
					CASE RADAR_TRACE_ARENA_DOMINATOR
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_DOMINATOR
					BREAK
					CASE RADAR_TRACE_ARENA_IMPALER
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_IMPALER
					BREAK
					CASE RADAR_TRACE_ARENA_IMPERATOR
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_IMPERATOR
					BREAK
					CASE RADAR_TRACE_ARENA_ISSI
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_ISSI
					BREAK
					CASE RADAR_TRACE_ARENA_SASQUATCH
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_SASQUATCH
					BREAK
					CASE RADAR_TRACE_ARENA_SCARAB
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_SCARAB
					BREAK
					CASE RADAR_TRACE_ARENA_SLAMVAN
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_SLAMVAN
					BREAK
					CASE RADAR_TRACE_ARENA_ZR380
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_ZR380
					BREAK
					CASE RADAR_TRACE_ACSR_WP1
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_SEASPARROW
					BREAK
					CASE RADAR_TRACE_ACSR_WP2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_CARACARA
					BREAK
					CASE RADAR_TRACE_MINI_SUB
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_AVISA
					BREAK
					CASE RADAR_TRACE_SEASPARROW2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_SEASPARROW2
					BREAK
					CASE RADAR_TRACE_FOLDING_WING_JET
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_ALKONOST
					BREAK
					CASE RADAR_TRACE_GANG_BIKE
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_MANCHEZ2
					BREAK
					CASE RADAR_TRACE_MILITARY_QUAD
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_VERUS
					BREAK
					CASE RADAR_TRACE_SQUADEE
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_SQUADDIE
					BREAK
					CASE RADAR_TRACE_DINGHY2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_DINGHY5
					BREAK
					CASE RADAR_TRACE_WINKY
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_WINKY
					BREAK
					CASE RADAR_TRACE_PATROL_BOAT
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_PATROL_BOAT
					BREAK
					CASE RADAR_TRACE_VALYKRIE2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_ANNIHILATOR2
					BREAK
					CASE RADAR_TRACE_KART_RETRO
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_VETO
					BREAK
					CASE RADAR_TRACE_KART_MODERN
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_VETO2
					BREAK
					CASE RADAR_TRACE_MILITARY_TRUCK
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_VETIR
					BREAK
					CASE RADAR_TRACE_SUB2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_KOSATKA
					BREAK
					CASE RADAR_TRACE_D_CHAMPION
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_CHAMPION
					BREAK
					CASE RADAR_TRACE_BUFFALO_4
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_BUFFALO4
					BREAK
					CASE RADAR_TRACE_DEITY
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_DEITY
					BREAK
					CASE RADAR_TRACE_JUBILEE
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_JUBILEE
					BREAK
					CASE RADAR_TRACE_GRANGER2
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_GRANGER2
					BREAK
					CASE RADAR_TRACE_PATRIOT3
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_PATRIOT3
					BREAK
					#IF FEATURE_DLC_2_2022
					CASE RADAR_TRACE_ARMS_DEALING_AIR
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = PBBD_IS_IN_DUSTER
					BREAK
					#ENDIF
				ENDSWITCH
				// am i the 'leader' of the vehicle ie. driver or nearest passenger
				IF AmILeaderOfVehicle(CarID)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_LEADER_OF_VEHICLE)		
				ELSE	
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_LEADER_OF_VEHICLE)
				ENDIF
			ENDIF
		ELSE
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iInVehicleType = 0
			
			IF AmILeaderOfMOC()
			OR AmILeaderOfAOC()
			OR AmILeaderOfHackerTruck()
			OR AmILeaderOfSubmarine()
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_LEADER_OF_VEHICLE)	
			ELSE
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_LEADER_OF_VEHICLE)
			ENDIF

		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("setting bd blip flags")
		#ENDIF
		#ENDIF

		
		// is player highlighted? 
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_SELF_HIGHLIGHTED)
		
			// check if on mission
			IF (IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID()) AND NOT g_PlayerBlipsData.bHighlightedBlipWhileOnMission)
			OR NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HighlightMyBlipThreadID)
				HIGHLIGHT_MY_PLAYER_BLIP(FALSE, -1, TRUE)
			ENDIF
			
			// check if timer has expired
			IF (g_PlayerBlipsData.iTimeToHighlight > -1)
				IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.HighlightStartTime)) > g_PlayerBlipsData.iTimeToHighlight
					HIGHLIGHT_MY_PLAYER_BLIP(FALSE, -1, TRUE)	//, TRUE)
				ENDIF
			ENDIF
			
		ENDIF
		
		// is player flashing his arrow
		IF (g_PlayerBlipsData.bFlashMyArrow)
		
			// check if on mission
			IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.FlashMyArrowThreadID)
				FLASH_MY_PLAYER_ARROW(FALSE, -1, TRUE)
			ENDIF
			
			// check if timer has expired
			IF (g_PlayerBlipsData.iDurationToFlashMyArrow > -1)
				IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.FlashMyArrowStartTime)) > g_PlayerBlipsData.iDurationToFlashMyArrow
					FLASH_MY_PLAYER_ARROW(FALSE, -1, TRUE)	//, TRUE)
				ENDIF
			ENDIF		
		
		ENDIF
		
		
		// am i idling?
		IF IS_MY_PLAYER_BLIP_SET_AS_IDLE()
			IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.IdleMyBlipThreadID)
				CPRINTLN(DEBUG_BLIP, "[player_blips] idling script no longer active.")
				SET_MY_PLAYER_BLIP_AS_IDLE(FALSE, TRUE)
			ENDIF
		ENDIF
		
		
		// is player flashing red?
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_FLASHING_RED)
		
			// check if on mission
			IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.FlashMyArrowRedThreadID)
				FLASH_MY_PLAYER_ARROW_RED(FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
			ENDIF
			
			// check if timer has expired
			IF (g_PlayerBlipsData.iTimeToFlashArrowRed > -1)
				IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.RedFlashArrowStartTime)) > g_PlayerBlipsData.iTimeToFlashArrowRed
					FLASH_MY_PLAYER_ARROW_RED(FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)	//, TRUE)
				ENDIF
			ENDIF
			
		ENDIF
		
		
		// if player's blip is hidden check thread is still active
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_IS_HIDDEN)
			IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideMyBlipThreadID)
				HIDE_MY_PLAYER_BLIP(FALSE, TRUE)
			ENDIF
		ENDIF

		// if players blip is hidden from a team, check thread is still active
		REPEAT 8 i
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, (PBBD_HIDDEN_FROM_TEAM_0 + i))
				IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideMyBlipFromTeamThreadID[i])
					HIDE_MY_PLAYER_BLIP_FROM_TEAM(FALSE, i)
				ENDIF
			ENDIF
		ENDREPEAT
		
		// if my blip was hidden for an event, check the thread is still active. 
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_HIDDEN_FROM_EVENT_NON_PARTICIPANTS)
			IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideMyBlipFromNonParticipantsOfEventThreadID)
				HIDE_MY_PLAYER_BLIP_FROM_NON_PARTICIPANTS_OF_EVENT(FALSE)
			ENDIF
		ENDIF

		// if my blip was hidden from non gang members, check the thread is still active. 
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_HIDDEN_FROM_NON_GANG_MEMBERS)
			IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.HideMyBlipFromNonGangMembersThreadID)
				HIDE_MY_PLAYER_BLIP_FROM_NON_GANG_MEMBERS(FALSE)
			ENDIF
		ENDIF

		IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(MPGlobals.LocalPlayerID))
		AND IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(MPGlobals.LocalPlayerID))
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsInVehicle) // need to store this in broadcast data as the command will return false when trying to check peds far away
		ELSE
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsInVehicle)
		ENDIF
		

		// if i'm wanted but my wanted stars are flashing then broadcast this
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		AND ARE_PLAYER_FLASHING_STARS_ABOUT_TO_DROP(PLAYER_ID())	
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_WANTED_STARS_ABOUT_TO_DROP)
			g_PlayerBlipsData.iTimerWatedStarReset = GET_NETWORK_TIME()
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_WANTED_STARS_ABOUT_TO_DROP)
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , g_PlayerBlipsData.iTimerWatedStarReset) > 1000)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_WANTED_STARS_ABOUT_TO_DROP)
				ENDIF
			ENDIF
		ENDIF
	
		// if the script that force blipped is no longer active then remove all force blips
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, g_PlayerBlipsData.iBlipStaggerCount )
		IF NOT (NATIVE_TO_INT(PlayerID) = -1)
			IF NOT (g_PlayerBlipsData.ForceBlipThreadID[NATIVE_TO_INT(PlayerID)] = INT_TO_NATIVE(THREADID, -1))
				IF NOT IS_THREAD_ACTIVE(g_PlayerBlipsData.ForceBlipThreadID[NATIVE_TO_INT(PlayerID)])
				
				#IF IS_DEBUG_BUILD
					AND NOT (g_PlayerBlipsData.bBypassThreadChecks)
				#ENDIF
					CLEAR_BIT(g_PlayerBlipsData.bs_PlayersToForceBlip, NATIVE_TO_INT(PlayerID))
					g_PlayerBlipsData.ForceBlipThreadID[NATIVE_TO_INT(PlayerID)]  = INT_TO_NATIVE(THREADID, -1)
					CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - force blip thread no longer active for player ", g_PlayerBlipsData.iBlipStaggerCount)
				ELSE
					IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_PlayersToForceBlip, NATIVE_TO_INT(PlayerID))
						g_PlayerBlipsData.ForceBlipThreadID[NATIVE_TO_INT(PlayerID)]  = INT_TO_NATIVE(THREADID, -1)
						CPRINTLN(DEBUG_BLIP, "[player_blips] MAINTAIN_GENERAL_BLIP_INFO - force blip bit no longer set for player ", g_PlayerBlipsData.iBlipStaggerCount)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
		
		// is this player on a race leaderboard?
		IF g_b_OnLeaderboard
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsOnLeaderBoard)
		ELSE
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsOnLeaderBoard)
		ENDIF
		
		IF g_MultiplayerSettings.g_bTempPassiveModeSetting
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_TEMP_PASSIVE_MODE_SET)
		ELSE
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, PBBD_TEMP_PASSIVE_MODE_SET)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("setting iFlags")
		#ENDIF
		#ENDIF		

		IF (g_PlayerBlipsData.bForceUpdateAllPlayerBlipsNow)
			UPDATE_ALL_PLAYER_BLIPS_NOW()		
			g_PlayerBlipsData.bForceUpdateAllPlayerBlipsNow = FALSE
		ENDIF	

		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("UPDATE_ALL_PLAYER_BLIPS_NOW")
		#ENDIF
		#ENDIF	

		MAINTAIN_PERSONAL_VEHICLE_FLASHING_BLIP()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PERSONAL_VEHICLE_FLASHING_BLIP")
		#ENDIF
		#ENDIF

		// render who can see me
		#IF IS_DEBUG_BUILD

			IF (g_PlayerBlipsData.bDrawTestBlip)
				IF NOT DOES_BLIP_EXIST(g_PlayerBlipsData.TestBlip)
					g_PlayerBlipsData.TestBlip = ADD_BLIP_FOR_COORD(GET_PLAYER_COORDS(PLAYER_ID()))
				ENDIF				
				
				SET_BLIP_SPRITE(g_PlayerBlipsData.TestBlip, INT_TO_ENUM(BLIP_SPRITE, g_PlayerBlipsData.iTestBlipSprite))
				SET_CONTENTS_OF_TEXT_WIDGET(g_PlayerBlipsData.twTestBlipSprite, GET_BLIP_SPRITE_DEBUG_STRING(INT_TO_ENUM(BLIP_SPRITE, g_PlayerBlipsData.iTestBlipSprite)))
				
				SET_BLIP_SCALE(g_PlayerBlipsData.TestBlip, g_PlayerBlipsData.fTestBlipScale)	
				SET_BLIP_ROTATION(g_PlayerBlipsData.TestBlip, g_PlayerBlipsData.iTestBlipRotation)
				SET_BLIP_COLOUR(g_PlayerBlipsData.TestBlip, g_PlayerBlipsData.iTestBlipColour)
			ELSE
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.TestBlip)
					SET_CONTENTS_OF_TEXT_WIDGET(g_PlayerBlipsData.twTestBlipSprite, "")
					
					REMOVE_BLIP(g_PlayerBlipsData.TestBlip)
				ENDIF
			ENDIF
			
			IF (g_PlayerBlipsData.bTestSecondaryColour)
				IF DOES_BLIP_EXIST(g_PlayerBlipsData.TestBlip)
					SET_SECONDARY_BLIP_COLOUR(g_PlayerBlipsData.TestBlip, g_PlayerBlipsData.iTestR, g_PlayerBlipsData.iTestG, g_PlayerBlipsData.iTestB)
					#IF IS_DEBUG_BUILD
						IF NOT (g_PlayerBlipsData.bDisableShowFriend)
					#ENDIF
					SHOW_FRIEND_INDICATOR_ON_BLIP(g_PlayerBlipsData.TestBlip, TRUE)
					#IF IS_DEBUG_BUILD
						ENDIF
					#ENDIF
				ENDIF
				g_PlayerBlipsData.bTestSecondaryColour = FALSE
			ENDIF
			
			IF (g_PlayerBlipsData.bHighlightSelf)
				HIGHLIGHT_MY_PLAYER_BLIP(TRUE)
				g_PlayerBlipsData.bHighlightSelf = FALSE
			ENDIF
				
			
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F12, KEYBOARD_MODIFIER_SHIFT, "Show who can see my blip")
				IF (g_PlayerBlipsData.bShowWhoCanSeeMe)
					g_PlayerBlipsData.bShowWhoCanSeeMe = FALSE
				ELSE
					g_PlayerBlipsData.bShowWhoCanSeeMe = TRUE
				ENDIF
				IF (g_PlayerBlipsData.bDebugBlipOutput)
					g_PlayerBlipsData.bDebugBlipOutput = FALSE
				ELSE
					g_PlayerBlipsData.bDebugBlipOutput = TRUE
				ENDIF
			ENDIF
		
			INT iRows
			FLOAT fDrawX, fDrawY
			FLOAT fWindowH
			
			IF (g_PlayerBlipsData.bShowWhoCanSeeMe)
			
				fWindowH = (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY)
			
				DRAW_RECT(g_PlayerBlipsData.WindowX, g_PlayerBlipsData.WindowY - (fWindowH* 0.5) + (0.25 * g_PlayerBlipsData.WindowSpacerY) , g_PlayerBlipsData.WindowW, fWindowH, 0, 0, 0, 128)
							
				// draw title
				fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX
				fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
				SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
				SET_TEXT_COLOUR(0,255,0, 255)
				DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Players who can see my blip", HUD_COLOUR_WHITE)
				iRows += 1
				
				REPEAT NUM_NETWORK_PLAYERS i

					IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i))
					
						IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayers, NATIVE_TO_INT(PLAYER_ID()))
							
							fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX 
							fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
							
							SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
							SET_TEXT_COLOUR(0,0,0, 255)
						
							DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, i)), GET_PLAYER_HUD_COLOUR(INT_TO_NATIVE(PLAYER_INDEX, i)) )//, TRUE))										
							iRows += 1
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_FORCE_BLIPPED)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "I have been Force Blipped", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_SAME_PROPERTY_WAR)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "We are on same Property War", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_SAME_TEAM)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "We are in same team", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_WANTED)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "I am wanted", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_IMPROMPTU)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Impromptu opponent", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_REVEAL_ALL)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Player has Reveal All", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_NOTICABLE)
								
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Noticable Reason:", HUD_COLOUR_WHITE)
								iRows += 1							
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_IS_TARGETTED)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "He is targetting me", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_CAN_SEE_PLAYER)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "He can see me", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_STEALTH_NOISE)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "He can hear my stealth noise", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_DAMAGED_BY)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "I damaged him", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
//								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_TALKING_LOUDHAILER)
//									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
//									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
//									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
//									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "I am using loudhailer", HUD_COLOUR_WHITE)
//									iRows += 1							
//								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_DRIVING_PAST_IN_VEHICLE)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "I drove past him", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_FIRING_IN_RANGE)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "I fired a weapon within range", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_LESS_THAN_COOL_DOWN_TIME)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Cool down time", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_CHATTING_IN_RANGE)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Chatting in range", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_MAKING_CAR_NOISE)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Making Car Noise", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_VEH_DM_WITHIN_RANGE)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Vehicle dm within range", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_LOCKED_ON_BY_VEHICLE)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Locked on by vehicle", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_ON_VEH_DM)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "On vehicle dm", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_IDLING)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "idling", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF		
								
								IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_NoticableReason[NATIVE_TO_INT(PLAYER_ID())], NOTICABLE_REASON_USING_ACTIVE_ROAMING_SPECTATOR_MODE)
									fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent + g_PlayerBlipsData.WindowXIndent
									fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
									SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
									DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "using active roaming spectatormode", HUD_COLOUR_WHITE)
									iRows += 1							
								ENDIF
								
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_BACK_OF_VEHICLE)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "I'm arrested in back of vehicle", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_SNITCH)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Snitch told him", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_CRIM_IN_COP_CAR)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "He is in a stolen cop car", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_COP_IN_MY_WANTED_RADIUS)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "I'm in his wanted radius", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_SAME_TEAM_SAME_MISSION)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "We're in same team on same mission", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_TEAMS_ARE_FRIENDLY)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "We're in friendly teams", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_SAME_TEAM_NOT_ON_MISSION)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "We're in same team both not on mission", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_DEFAULT_ON)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Default On", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_FADING_OUT)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Fading Out", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_SPECTATOR)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Spectator", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_SHOW_ALL)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Show All Set", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_TEAM_IS_HIGHLIGHTED)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Team is highlighted", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_ON_VEH_DM)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "On vehicle dm", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF
							
							IF IS_BIT_SET(GlobalplayerBD[i].playerBlipData.iBS_BlippedPlayerReason[NATIVE_TO_INT(PLAYER_ID())], BLIP_PLAYER_REASON_ACTIVE_ROAMING_SPEC_MODE)
								fDrawX = g_PlayerBlipsData.WindowX + g_PlayerBlipsData.WindowColumnX + g_PlayerBlipsData.WindowXIndent
								fDrawY = g_PlayerBlipsData.WindowY - (g_PlayerBlipsData.WindowTotalRows * g_PlayerBlipsData.WindowSpacerY) + (TO_FLOAT(iRows) * g_PlayerBlipsData.WindowSpacerY)		
								SET_TEXT_SCALE(0.0000, g_PlayerBlipsData.WindowTextScale)
								DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "active roaming spectator mode", HUD_COLOUR_WHITE)
								iRows += 1
							ENDIF							
							
						ENDIF
			
					ENDIF
			
				ENDREPEAT
				
				g_PlayerBlipsData.WindowTotalRows = iRows
			
			ENDIF
			
			IF (g_PlayerBlipsData.bCallShowAll)
				SHOW_ALL_PLAYER_BLIPS(g_PlayerBlipsData.bShowAllSet)	
				g_PlayerBlipsData.bCallShowAll = FALSE
			ENDIF
			

		#ENDIF
		
	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("debug")
		#ENDIF
		#ENDIF	
	
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bMyBlipIsVisible)
			IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_PlayerBlipsData.iTimeBlipWasVisible)) > FLAG_TIMER_DELAY)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bMyBlipIsVisible = FALSE
			ENDIF
		ENDIF
		
		
	ENDIF

	
	
	// do casino roof terrace minmap
	IF NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(MPGlobals.LocalPlayerID)
	AND NOT IS_PLAYER_IN_PROPERTY(MPGlobals.LocalPlayerID, TRUE)
	AND NOT IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_USING_CASINO_MISSION_INTERIOR)
		IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(MPGlobals.LocalPlayerID), <<978.076, 92.7732, 128.175>>, <<912.625, -9.850, 108.025>>, 92.775)
			PAUSE_MENU_STATE pmState = GET_PAUSE_MENU_STATE()			
			IF (pmState = PM_INACTIVE)
				IF NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
					SET_RADAR_AS_INTERIOR_THIS_FRAME(1860847426,  950.0, 40.0)
					//CPRINTLN(DEBUG_BLIP, "[player_blips][casino_roof] GET_PAUSE_MENU_STATE - called SET_RADAR_AS_INTERIOR_THIS_FRAME ")
				ELSE
					CPRINTLN(DEBUG_BLIP, "[player_blips][casino_roof] GET_PAUSE_MENU_STATE - pause pressed ")	
				ENDIF
			ELSE
				CPRINTLN(DEBUG_BLIP, "[player_blips][casino_roof] GET_PAUSE_MENU_STATE = ", ENUM_TO_INT(pmState))
			ENDIF
			SET_PLAYER_ON_CASINO_ROOF(TRUE)
		ELSE
			SET_PLAYER_ON_CASINO_ROOF(FALSE)
		ENDIF
	ELSE
		SET_PLAYER_ON_CASINO_ROOF(FALSE)	
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("casino roof")
	#ENDIF
	#ENDIF		
	
	#IF IS_DEBUG_BUILD
		IF (g_PlayerBlipsData.fTestRadarZoomDist > 0.0)
			SET_RADAR_ZOOM_TO_DISTANCE(g_PlayerBlipsData.fTestRadarZoomDist)
		ENDIF
	#ENDIF
	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF
	
ENDPROC


///Is the player starting a race in FM
FUNC BOOL HIDE_BLIP_BECAUSE_PLAYER_IS_STARTING_RACE()
	INT iMyGBD = NATIVE_TO_INT(MPGlobals.LocalPlayerID)
	IF IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIwantToVote) 		
	AND GlobalplayerBD_FM_2[iMyGBD].missionType	 	= FMMC_TYPE_RACE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



// EOF

