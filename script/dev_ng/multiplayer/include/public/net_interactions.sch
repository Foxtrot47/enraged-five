USING "globals.sch"
USING "net_include.sch"
USING "net_interactions_public.sch"
USING "PM_MissionCreator_Public.sch"
USING "net_crew_updates.sch"
USING "cellphone_public.sch"
USING "drunk_public.sch"
USING "net_common_functions.sch"
USING "net_clone_ped.sch"

USING "net_interactions_synced.sch"
USING "net_spawn.sch"
#IF FEATURE_FREEMODE_ARCADE
USING "arcade_config_data.sch"
#ENDIF

//TWEAK_FLOAT DetachForceX  0.0
//TWEAK_FLOAT DetachForceY  0.0
//TWEAK_FLOAT DetachForceZ  -4.0
//
//TWEAK_FLOAT DetachOffsetX  0.0
//TWEAK_FLOAT DetachOffsetY  0.0
//TWEAK_FLOAT DetachOffsetZ  0.0

TWEAK_FLOAT DetachForceScale 4.0

TWEAK_INT 	NO_OF_FRAMES_BEFORE_CHANGE 1

TWEAK_INT INTRO_DELAY_TIMER 200

TWEAK_FLOAT MAX_POINTING_DIFF_FROM_CAM 20.0

TWEAK_FLOAT POINTING_PITCH 0.0
TWEAK_FLOAT POINTING_HEADING 0.0
TWEAK_FLOAT POINTING_INTERP_VALUE 4.0

TWEAK_FLOAT POINTING_OFFSET_X -0.2
TWEAK_FLOAT POINTING_OFFSET_MIN_Y 0.3
TWEAK_FLOAT POINTING_OFFSET_MAX_Y 0.7
TWEAK_FLOAT POINTING_OFFSET_Z 0.6
TWEAK_FLOAT POINTING_CAPSULE_RADIUS 0.4
TWEAK_FLOAT POINTING_CAPSULE_HEIGHT 0.2

TWEAK_FLOAT SHOULDER_CAM_START_OFFSET_X 0.2
TWEAK_FLOAT SHOULDER_CAM_START_OFFSET_Y -0.6
TWEAK_FLOAT SHOULDER_CAM_START_OFFSET_Z 0.65

TWEAK_FLOAT SHOULDER_CAM_START_ROT_OFFSET_X 0.0
TWEAK_FLOAT SHOULDER_CAM_START_ROT_OFFSET_Y 0.0
TWEAK_FLOAT SHOULDER_CAM_START_ROT_OFFSET_Z 0.0

TWEAK_FLOAT SHOULDER_CAM_END_OFFSET_X 0.2
TWEAK_FLOAT SHOULDER_CAM_END_OFFSET_Y -1.0
TWEAK_FLOAT SHOULDER_CAM_END_OFFSET_Z 0.65

TWEAK_FLOAT SHOULDER_CAM_END_ROT_OFFSET_X 0.0
TWEAK_FLOAT SHOULDER_CAM_END_ROT_OFFSET_Y 0.0
TWEAK_FLOAT SHOULDER_CAM_END_ROT_OFFSET_Z 0.0

TWEAK_FLOAT SHOULDER_CAM_FOV 50.0
TWEAK_INT SHOULDER_CAM_DURATION 2000

TWEAK_FLOAT NEAR_SHOULDER_CAM_START_OFFSET_X 0.2
TWEAK_FLOAT NEAR_SHOULDER_CAM_START_OFFSET_Y -0.47
TWEAK_FLOAT NEAR_SHOULDER_CAM_START_OFFSET_Z 0.65

TWEAK_FLOAT NEAR_SHOULDER_CAM_START_ROT_OFFSET_X 0.0
TWEAK_FLOAT NEAR_SHOULDER_CAM_START_ROT_OFFSET_Y 0.0
TWEAK_FLOAT NEAR_SHOULDER_CAM_START_ROT_OFFSET_Z 0.0

TWEAK_FLOAT NEAR_SHOULDER_CAM_END_OFFSET_X 0.2
TWEAK_FLOAT NEAR_SHOULDER_CAM_END_OFFSET_Y -0.47
TWEAK_FLOAT NEAR_SHOULDER_CAM_END_OFFSET_Z 0.65

TWEAK_FLOAT NEAR_SHOULDER_CAM_END_ROT_OFFSET_X 0.0
TWEAK_FLOAT NEAR_SHOULDER_CAM_END_ROT_OFFSET_Y 0.0
TWEAK_FLOAT NEAR_SHOULDER_CAM_END_ROT_OFFSET_Z 0.0

TWEAK_INT MIN_WAIT_TIME_FOR_ANIM_TO_START 500
CONST_INT MIN_WAIT_TIME_FOR_CLEAR_AREA 	500

CONST_FLOAT PATH_BLOCKING_DIST 5.0

BOOL bSecondTap = FALSE
#IF FEATURE_COPS_N_CROOKS
SCRIPT_TIMER sDoubleTapReleaseTimer
#ENDIF

#IF IS_DEBUG_BUILD
PROC PRINT_INTERACTION_DATA(INTERACTION_DATA &InteractionData)
	INT i
	REPEAT MAX_NUM_INTERACTION_ANIM_DICTS i
		NET_PRINT("   strAnimDict[") NET_PRINT_INT(i) NET_PRINT("] = ") NET_PRINT(InteractionData.strAnimDict[i]) NET_NL() 
	ENDREPEAT
	NET_PRINT("   strAnimNameIntro = ") NET_PRINT(InteractionData.strAnimNameIntro) NET_NL()
	NET_PRINT("   strAnimName = ") NET_PRINT(InteractionData.strAnimName) NET_NL()
	NET_PRINT("   strAnimNameOutro = ") NET_PRINT(InteractionData.strAnimNameOutro) NET_NL()
	NET_PRINT("   strAnimNameFullBody = ") NET_PRINT(InteractionData.strAnimNameFullBody) NET_NL()
	NET_PRINT("   bLeftHandedOnly = ") NET_PRINT_BOOL(InteractionData.bLeftHandedOnly) NET_NL()
	NET_PRINT("   bHasProp = ") NET_PRINT_BOOL(InteractionData.bHasProp) NET_NL()
	NET_PRINT("   bDeletePropAtEnd = ") NET_PRINT_BOOL(InteractionData.bDeletePropAtEnd) NET_NL()
	NET_PRINT("   bUsesPTFX = ") NET_PRINT_BOOL(InteractionData.bUsesPTFX) NET_NL()
	NET_PRINT("   bIsLooped = ") NET_PRINT_BOOL(InteractionData.bIsLooped) NET_NL()
	NET_PRINT("   bCanSkipLoop = ") NET_PRINT_BOOL(InteractionData.bCanSkipLoop) NET_NL()
	NET_PRINT("   bCanQuitLoopEarly = ") NET_PRINT_BOOL(InteractionData.bCanQuitLoopEarly) NET_NL()
	IF (InteractionData.bHasProp)
		NET_PRINT("   PropModel = ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG (InteractionData.PropModel)) NET_NL()
	ENDIF
	NET_PRINT("   iMinLoopIterations = ") NET_PRINT_INT(InteractionData.iMinLoopIterations) NET_NL()
	NET_PRINT("   bPropHasAttachedPTFX = ") NET_PRINT_BOOL(InteractionData.bPropHasAttachedPTFX) NET_NL()
	NET_PRINT("   strPropPTFX = ") NET_PRINT(InteractionData.strPropPTFX) NET_NL()
	NET_PRINT("   vPropPTFXOffset = ") NET_PRINT_VECTOR(InteractionData.vPropPTFXOffset) NET_NL()
	NET_PRINT("   vPropPTFXRotation = ") NET_PRINT_VECTOR(InteractionData.vPropPTFXRotation) NET_NL()
	NET_PRINT("   bHasLoopMouthPTFX = ") NET_PRINT_BOOL(InteractionData.bHasLoopMouthPTFX) NET_NL()
	NET_PRINT("   strLoopPTFXMouth = ") NET_PRINT(InteractionData.strLoopPTFXMouth) NET_NL()
	NET_PRINT("   bHasLoopNosePTFX = ") NET_PRINT_BOOL(InteractionData.bHasLoopNosePTFX) NET_NL()
	NET_PRINT("   strLoopPTFXNose = ") NET_PRINT(InteractionData.strLoopPTFXNose) NET_NL()
	NET_PRINT("   iLoopEventDrunk = ") NET_PRINT_INT(InteractionData.iLoopEventDrunk) NET_NL()
	NET_PRINT("   iForcedAnimFilter = ") NET_PRINT_INT(InteractionData.iForcedAnimFilter) NET_NL()
//	NET_PRINT("   bPlayIntroSound = ") NET_PRINT_BOOL(InteractionData.bPlayIntroSound) NET_NL()
//	NET_PRINT("   strIntroSound = ") NET_PRINT(InteractionData.strIntroSound) NET_NL()
//	NET_PRINT("   fIntroSoundTime = ") NET_PRINT_FLOAT(InteractionData.fIntroSoundTime) NET_NL()
//	NET_PRINT("   bPlayLoopSound = ") NET_PRINT_BOOL(InteractionData.bPlayLoopSound) NET_NL()
//	NET_PRINT("   strLoopSound = ") NET_PRINT(InteractionData.strLoopSound) NET_NL()
//	NET_PRINT("   fLoopSoundTime = ") NET_PRINT_FLOAT(InteractionData.fLoopSoundTime) NET_NL()	
//	NET_PRINT("   bPlayLoopSound2 = ") NET_PRINT_BOOL(InteractionData.bPlayLoopSound2) NET_NL()
//	NET_PRINT("   fLoopSoundTime2 = ") NET_PRINT_FLOAT(InteractionData.fLoopSoundTime2) NET_NL()	
//	NET_PRINT("   bPlayOutroSound = ") NET_PRINT_BOOL(InteractionData.bPlayOutroSound) NET_NL()
//	NET_PRINT("   strOutroSound = ") NET_PRINT(InteractionData.strOutroSound) NET_NL()
//	NET_PRINT("   fOutroSoundTime = ") NET_PRINT_FLOAT(InteractionData.fOutroSoundTime) NET_NL()		
	NET_PRINT("   bUseSounds = ") NET_PRINT_BOOL(InteractionData.bUseSounds) NET_NL()
	//NET_PRINT("   bRemoveClosedMaskHelmet = ") NET_PRINT_BOOL(InteractionData.bRemoveClosedMaskHelmet) NET_NL()
	

ENDPROC

PROC PRINT_INTERACTION_GLOBAL_DATA(INTERACTION_GLOBAL_DATA &GlobalData)
	
	NET_PRINT("     iInteractionType = ") NET_PRINT_INT(GlobalData.iInteractionType) NET_NL()
	NET_PRINT("     iInteractionAnim = ") NET_PRINT_INT(GlobalData.iInteractionAnim) NET_NL()
	NET_PRINT("     iFlags = ") NET_PRINT_INT(GlobalData.iFlags) NET_NL()
	NET_PRINT("     bPlayInteractionAnim = ") NET_PRINT_BOOL(GlobalData.bPlayInteractionAnim) NET_NL()
	NET_PRINT("     bPlayInteractionAnimFullBody = ") NET_PRINT_BOOL(GlobalData.bPlayInteractionAnimFullBody) NET_NL()
	NET_PRINT("     bPlayInteractionSelfie = ") NET_PRINT_BOOL(GlobalData.bPlayInteractionSelfie) NET_NL()
	NET_PRINT("     bCleanupInteractionAnim = ") NET_PRINT_BOOL(GlobalData.bCleanupInteractionAnim) NET_NL()
	NET_PRINT("     bCleanupInteractionAnimImmediately = ") NET_PRINT_BOOL(GlobalData.bCleanupInteractionAnimImmediately) NET_NL()
	NET_PRINT("     bInteractionIsPlaying = ") NET_PRINT_BOOL(GlobalData.bInteractionIsPlaying) NET_NL()
	NET_PRINT("     bHoldLoop = ") NET_PRINT_BOOL(GlobalData.bHoldLoop) NET_NL()
	NET_PRINT("     bHasCleanedUp = ") NET_PRINT_BOOL(GlobalData.bHasCleanedUp) NET_NL()
	NET_PRINT("     bStartedIntroTime = ") NET_PRINT_BOOL(GlobalData.bStartedIntroTime) NET_NL()
	NET_PRINT("     bIsWearingHelmentInVehicle = ") NET_PRINT_BOOL(GlobalData.bIsWearingHelmentInVehicle) NET_NL()
	NET_PRINT("     bWaitForPlayerControl = ") NET_PRINT_BOOL(GlobalData.bWaitForPlayerControl) NET_NL()
	NET_PRINT("     bWaitForSync = ") NET_PRINT_BOOL(GlobalData.bWaitForSync) NET_NL()
	NET_PRINT("		bWaitForFadingIn = ") NET_PRINT_BOOL(GlobalData.bWaitForFadingIn) NET_NL()
	NET_PRINT("		bForceWeaponVisible = ") NET_PRINT_BOOL(GlobalData.bForceWeaponVisible) NET_NL()
	NET_PRINT("     fStartPhase = ") NET_PRINT_FLOAT(GlobalData.fStartPhase) NET_NL()
	NET_PRINT("     iAlcoholShots = ") NET_PRINT_INT(GlobalData.iAlcoholShots) NET_NL()
	NET_PRINT("     iCurrentAlcoholShots = ") NET_PRINT_INT(GlobalData.iCurrentAlcoholShots) NET_NL()
	NET_PRINT("     iHealthChange = ") NET_PRINT_INT(GlobalData.iHealthChange) NET_NL()
	
ENDPROC

STRING IA_PATHNAME = "X:/gta5/titleupdate/dev_ng/"
STRING IA_FILENAME = "InteractionAnims.txt"

PROC IA_PRINTSTRING(sTRING str)
	PRINTSTRING(str)
	SAVE_STRING_TO_NAMED_DEBUG_FILE(str, IA_PATHNAME, IA_FILENAME)
ENDPROC

PROC IA_PRINTINT(INT iInt)
	PRINTINT(iInt)
	SAVE_INT_TO_NAMED_DEBUG_FILE(iInt, IA_PATHNAME, IA_FILENAME)
ENDPROC

PROC IA_PRINTFLOAT(FLOAT fFloat)
	PRINTFLOAT(fFloat)
	SAVE_FLOAT_TO_NAMED_DEBUG_FILE(fFloat, IA_PATHNAME, IA_FILENAME)
ENDPROC

PROC IA_PRINTVECTOR(VECTOR vVec)
	PRINTVECTOR(vVec)
	SAVE_VECTOR_TO_NAMED_DEBUG_FILE(vVec, IA_PATHNAME, IA_FILENAME)
ENDPROC


PROC IA_PRINTNL()
	PRINTNL()
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(IA_PATHNAME, IA_FILENAME)
ENDPROC

#ENDIF

FUNC STRING FallBackFilterForAnim(INT iInteractionType, INT iSelectedAnim)

	// note: only need to add anims where the fallback is not BONEMASK_HEAD_NECK_AND_ARMS

	SWITCH INT_TO_ENUM(INTERACTION_ANIM_TYPES, iInteractionType)
		CASE INTERACTION_ANIM_TYPE_PLAYER
			SWITCH INT_TO_ENUM(PLAYER_INTERACTIONS, iSelectedAnim)	
				CASE PLAYER_INTERACTION_BANGING_TUNES_LEFT
					RETURN("BONEMASK_HEAD_NECK_AND_L_ARM")
				BREAK
				CASE PLAYER_INTERACTION_BANGING_TUNES_RIGHT
					RETURN("BONEMASK_HEAD_NECK_AND_R_ARM")
				BREAK
				CASE PLAYER_INTERACTION_OH_SNAP
				CASE PLAYER_INTERACTION_FIND_THE_FISH
					RETURN("BONEMASK_HEAD_NECK_AND_R_ARM")
				BREAK
				CASE PLAYER_INTERACTION_AIR_SHAGGING
					RETURN ("BONEMASK_HEAD_NECK_AND_ARMS")
				BREAK
				#IF FEATURE_CASINO
				CASE PLAYER_INTERACTION_CUT_THROAT
					RETURN("BONEMASK_HEAD_NECK_AND_R_ARM") 
				BREAK
				#ENDIF
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ("BONEMASK_HEAD_NECK_AND_ARMS") // default
ENDFUNC

FUNC FLOAT GET_DANCE_INTERACTION_ANIM_ENTER_EVENT_PHASE(STRING strDict, STRING strClip)
	FLOAT fStart, fEnd
	IF FIND_ANIM_EVENT_PHASE(strDict, strClip, "ENTER", fStart, fEnd)
		RETURN fStart
	ENDIF
	
	RETURN 0.0
ENDFUNC

PROC FillInteractionData(INT iInteractionType, INT iSelectedAnim, INTERACTION_DATA &InteractionData, BOOL bSexIsKnown=FALSE, BOOL bIsFemale=FALSE)
	
	// default settings
	InteractionData = gEmptyInteractionData
	
	SWITCH INT_TO_ENUM(INTERACTION_ANIM_TYPES, iInteractionType)
	
		CASE INTERACTION_ANIM_TYPE_IN_CAR		
			SWITCH INT_TO_ENUM(PASSENGER_INTERACTIONS, iSelectedAnim)
				
				CASE PASSENGER_INTERACTION_NONE
					// emptyAnimData
				BREAK
			
				CASE PASSENGER_INTERACTION_SMOKE	
					
					InteractionData.strAnimDict[AD_LOW_PS] 			= "amb@code_human_in_car_mp_actions@smoke@low@ps@base"
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 	= "amb@code_human_in_car_mp_actions@smoke@std@rds@base"
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "amb@code_human_in_car_mp_actions@smoke@std@rps@base"
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "amb@code_human_in_car_mp_actions@smoke@std@ps@base"
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR] 	= "amb@code_human_in_car_mp_actions@smoke@bodhi@rps@base"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 	= "amb@code_human_in_car_mp_actions@smoke@bodhi@rps@base"
					InteractionData.strAnimDict[AD_BOHDI_DS] 		= "amb@code_human_in_car_mp_actions@smoke@std@ds@base"
					InteractionData.strAnimDict[AD_BOHDI_PS] 		= "amb@code_human_in_car_mp_actions@smoke@std@ps@base"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "amb@code_human_in_car_mp_actions@smoke@std@ds@base"
					InteractionData.strAnimDict[AD_LOW_DS] 			= "amb@code_human_in_car_mp_actions@smoke@low@ds@base"
					
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					

					InteractionData.bHasProp = TRUE					
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01

					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke_car"
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>
	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth_car"
					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse_car"
					
					InteractionData.iMinLoopIterations = 1
					

					// loop info
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					
				BREAK
				CASE PASSENGER_INTERACTION_FINGER	
								
					// Business Pack new version			
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarfingerlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarfingerstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarfingerstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarfingerstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarfingerbodhi@rds@" 
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarfingerbodhi@rps@" 
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarfingerbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarfingerbodhi@ps@"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarfingerstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarfingerlow@ds@" 	
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					// loop info
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
								
				
				BREAK
				CASE PASSENGER_INTERACTION_DANCE	

					InteractionData.strAnimDict[AD_LOW_PS] 			= "amb@code_human_in_car_mp_actions@dance@low@ps@base"
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "amb@code_human_in_car_mp_actions@dance@std@rds@base"
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "amb@code_human_in_car_mp_actions@dance@std@rps@base"
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "amb@code_human_in_car_mp_actions@dance@std@ps@base"
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR] 		= "amb@code_human_in_car_mp_actions@dance@bodhi@rds@base"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "amb@code_human_in_car_mp_actions@dance@bodhi@rds@base"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "amb@code_human_in_car_mp_actions@dance@std@ps@base"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "amb@code_human_in_car_mp_actions@dance@std@ds@base"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "amb@code_human_in_car_mp_actions@dance@std@ds@base"
					InteractionData.strAnimDict[AD_LOW_DS] 			= "amb@code_human_in_car_mp_actions@dance@low@ds@base"				

					InteractionData.strAnimName = "idle_a"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE					
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"					
															
				BREAK
//				CASE PASSENGER_INTERACTION_FIST
//					
//					InteractionData.strAnimDict[AD_LOW_PS] 			= "amb@code_human_in_car_mp_actions@fist_pump@low@ps@base"
//					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "amb@code_human_in_car_mp_actions@fist_pump@std@rds@base"
//					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "amb@code_human_in_car_mp_actions@fist_pump@std@rps@base"
//					InteractionData.strAnimDict[AD_HIGH_PS] 		= "amb@code_human_in_car_mp_actions@fist_pump@std@ps@base"
//					InteractionData.strAnimDict[AD_BOHDI_DS_REAR] 		= "amb@code_human_in_car_mp_actions@fist_pump@bodhi@rds@base"
//					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "amb@code_human_in_car_mp_actions@fist_pump@bodhi@rps@base"
//					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "amb@code_human_in_car_mp_actions@fist_pump@std@ds@base"
//					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "amb@code_human_in_car_mp_actions@fist_pump@std@ps@base"
//					InteractionData.strAnimDict[AD_HIGH_DS] 		= "amb@code_human_in_car_mp_actions@fist_pump@std@ds@base"
//					InteractionData.strAnimDict[AD_LOW_DS] 			= "amb@code_human_in_car_mp_actions@fist_pump@low@ds@base"
//					
//
//					InteractionData.strAnimName = "idle_a"
//
//					InteractionData.bIsLooped = TRUE
//					InteractionData.bCanSkipLoop = FALSE
//					InteractionData.bCanQuitLoopEarly = FALSE
//					
//					InteractionData.strAnimNameIntro = "ENTER"	
//					InteractionData.strAnimName	 	= "IDLE_A"	
//					InteractionData.strAnimNameOutro = "EXIT"					
//					
//				BREAK
				CASE PASSENGER_INTERACTION_ROCK
					

					// business pack, new version
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarrocklow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarrockstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarrockstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarrockstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarrockbodhi@rds@" 
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarrockbodhi@rps@" 
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarrockbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarrockbodhi@ps@"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarrockstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarrocklow@ds@" 	

					
					InteractionData.strAnimName = "idle_a"

//					InteractionData.bRemoveWindow = TRUE	
					
	
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE					
					
				BREAK
				CASE PASSENGER_INTERACTION_WANK
					
					// business pack, new version
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarwanklow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarwankstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarwankstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarwankstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarwankbodhi@rds@" 
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarwankbodhi@rps@" 
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarwankbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarwankbodhi@ps@"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarwankstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarwanklow@ds@" 	
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE					
				
				BREAK
				
				
				CASE PASSENGER_INTERACTION_AIR_SHAGGING   
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarair_shagginglow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarair_shaggingstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarair_shaggingstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarair_shaggingstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarair_shaggingbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarair_shaggingbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarair_shaggingbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarair_shaggingbodhi@ps@"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarair_shaggingstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarair_shagginglow@ds@" 	
					
				
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE
				BREAK
								
				CASE PASSENGER_INTERACTION_DOCK  	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincardocklow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincardockstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincardockstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincardockstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincardockbodhi@rds@" 
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincardockbodhi@rps@" 
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincardockbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincardockbodhi@ps@"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincardockstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincardocklow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"
	
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				
				BREAK		
				
				
				CASE PASSENGER_INTERACTION_KNUCKLE_CRUNCH 
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarknuckle_crunchlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarknuckle_crunchstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarknuckle_crunchstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarknuckle_crunchstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarknuckle_crunchbodhi@rds@" 
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarknuckle_crunchbodhi@rps@" 
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarknuckle_crunchbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarknuckle_crunchbodhi@ps@"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarknuckle_crunchstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarknuckle_crunchlow@ds@" 	
	
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
														
					InteractionData.bUseSounds = TRUE
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				
				BREAK		
				
				CASE PASSENGER_INTERACTION_SALUTE	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarsalutelow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarsalutestd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarsalutestd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarsalutestd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarsalutebodhi@rds@" 
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarsalutebodhi@rps@" 
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarsalutebodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarsalutebodhi@ps@"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarsalutestd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarsalutelow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				
				BREAK		
				
				
				// not assigned packs
				CASE PASSENGER_INTERACTION_BLOW_KISS   	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarblow_kisslow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarblow_kissstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarblow_kissstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarblow_kissstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarblow_kissbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarblow_kissbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarblow_kissbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarblow_kissbodhi@ps@"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarblow_kissstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarblow_kisslow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE
					
					//InteractionData.bRemoveClosedMaskHelmet = TRUE
					
				BREAK	
				
				// Business Pack 2
				CASE PASSENGER_INTERACTION_SLOW_CLAP    
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarslow_claplow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarslow_clapstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarslow_clapstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarslow_clapstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarslow_clapbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarslow_clapbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarslow_clapbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarslow_clapbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarslow_clapstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarslow_claplow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bUseSounds = TRUE
				BREAK
				CASE PASSENGER_INTERACTION_FACE_PALM     
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarface_palmlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarface_palmstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarface_palmstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarface_palmstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarface_palmbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarface_palmbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarface_palmbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarface_palmbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarface_palmstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarface_palmlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				CASE PASSENGER_INTERACTION_THUMBS_UP  	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarthumbs_uplow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarthumbs_upstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarthumbs_upstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarthumbs_upstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarthumbs_upbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarthumbs_upbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarthumbs_upbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarthumbs_upbodhi@ps@"					
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarthumbs_upstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarthumbs_uplow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				CASE PASSENGER_INTERACTION_JAZZ_HANDS 
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarjazz_handslow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarjazz_handsstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarjazz_handsstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarjazz_handsstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarjazz_handsbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarjazz_handsbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarjazz_handsbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarjazz_handsbodhi@ps@"					
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarjazz_handsstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarjazz_handslow@ds@" 	
				
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				CASE PASSENGER_INTERACTION_NOSE_PICK    	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarnose_picklow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarnose_pickstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarnose_pickstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarnose_pickstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarnose_pickbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarnose_pickbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarnose_pickbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarnose_pickbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarnose_pickstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarnose_picklow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				
				// hipster
				CASE PASSENGER_INTERACTION_WAVE				
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarwavelow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarwavestd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarwavestd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarwavestd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarwavebodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarwavebodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarwavebodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarwavebodhi@ps@"					
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarwavestd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarwavelow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK	
				

				// Hipster Pack
				CASE PASSENGER_INTERACTION_AIR_GUITAR			
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarair_guitarlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarair_guitarstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarair_guitarstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarair_guitarstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarair_guitarbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarair_guitarbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarair_guitarbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarair_guitarbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarair_guitarstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarair_guitarlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK

				CASE PASSENGER_INTERACTION_SURRENDER			
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarsurrenderlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarsurrenderstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarsurrenderstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarsurrenderstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarsurrenderbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarsurrenderbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarsurrenderbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarsurrenderbodhi@ps@"					
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarsurrenderstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarsurrenderlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				CASE PASSENGER_INTERACTION_SHUSH			
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarshushlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarshushstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarshushstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarshushstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarshushbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarshushbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarshushbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarshushbodhi@ps@"					
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarshushstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarshushlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				CASE PASSENGER_INTERACTION_PHOTOGRAPHY			
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarphotographylow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarphotographystd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarphotographystd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarphotographystd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarphotographybodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarphotographybodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarphotographybodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarphotographybodhi@ps@"					
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarphotographystd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarphotographylow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				CASE PASSENGER_INTERACTION_DJ			
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincardjlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincardjstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincardjstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincardjstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincardjbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincardjbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincardjbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincardjbodhi@ps@"					
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincardjstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincardjlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				CASE PASSENGER_INTERACTION_AIR_SYNTH			
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarair_synthlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarair_synthstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarair_synthstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarair_synthstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarair_synthbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarair_synthbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarair_synthbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarair_synthbodhi@ps@"					
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarair_synthstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarair_synthlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
					
				
				CASE PASSENGER_INTERACTION_NO_WAY  			
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarno_waylow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarno_waystd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarno_waystd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarno_waystd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarno_waybodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarno_waybodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarno_waybodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarno_waybodhi@ps@"					
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarno_waystd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarno_waylow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				CASE PASSENGER_INTERACTION_CHIN_BRUSH 	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarchin_brushlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarchin_brushstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarchin_brushstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarchin_brushstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarchin_brushbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarchin_brushbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarchin_brushbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarchin_brushbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarchin_brushstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarchin_brushlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				// don't yet have the anims for these below:
				CASE PASSENGER_INTERACTION_CHICKEN_TUANT      
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarchicken_tauntlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarchicken_tauntstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarchicken_tauntstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarchicken_tauntstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarchicken_tauntbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarchicken_tauntbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarchicken_tauntbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarchicken_tauntbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarchicken_tauntstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarchicken_tauntlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
					
				
				CASE PASSENGER_INTERACTION_FINGER_KISS 	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarfinger_kisslow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarfinger_kissstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarfinger_kissstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarfinger_kissstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarfinger_kissbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarfinger_kissbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarfinger_kissbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarfinger_kissbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarfinger_kissstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarfinger_kisslow@ds@" 	
				
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK				

				CASE PASSENGER_INTERACTION_PEACE	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarpeacelow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarpeacestd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarpeacestd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarpeacestd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarpeacebodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarpeacebodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarpeacebodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarpeacebodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarpeacestd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarpeacelow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK	
				CASE PASSENGER_INTERACTION_YOU_LOCO		
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincaryou_locolow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincaryou_locostd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincaryou_locostd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincaryou_locostd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincaryou_locobodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincaryou_locobodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincaryou_locobodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincaryou_locobodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincaryou_locostd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincaryou_locolow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK				
				
				// halloween
				CASE PASSENGER_INTERACTION_FREAKOUT    		
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarfreakoutlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarfreakoutstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarfreakoutstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarfreakoutstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarfreakoutbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarfreakoutbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarfreakoutbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarfreakoutbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarfreakoutstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarfreakoutlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK  				
				CASE PASSENGER_INTERACTION_THUMB_ON_EARS    
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarthumb_on_earslow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarthumb_on_earsstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarthumb_on_earsstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarthumb_on_earsstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarthumb_on_earsbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarthumb_on_earsbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarthumb_on_earsbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarthumb_on_earsbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarthumb_on_earsstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarthumb_on_earslow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				
				// sports
				#IF FEATURE_INTERACTIONS_SPORTS
				CASE PASSENGER_INTERACTION_BRO_LOVE 		
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarbro_lovelow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarbro_lovestd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarbro_lovestd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarbro_lovestd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarbro_lovebodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarbro_lovebodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarbro_lovebodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarbro_lovebodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarbro_lovestd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarbro_lovelow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK				
				CASE PASSENGER_INTERACTION_FAKE_HORSE		
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarfake_horselow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarfake_horsestd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarfake_horsestd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarfake_horsestd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarfake_horsebodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarfake_horsebodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarfake_horsebodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarfake_horsebodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarfake_horsestd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarfake_horselow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK				
				CASE PASSENGER_INTERACTION_LOSER     
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarloserlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarloserstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarloserstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarloserstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarloserbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarloserbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarloserbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarloserbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarloserstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarloserlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK     					
				CASE PASSENGER_INTERACTION_OK 	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincaroklow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarokstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarokstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarokstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarokbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarokbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarokbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarokbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarokstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincaroklow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK		
				CASE PASSENGER_INTERACTION_FISHING    
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarfishinglow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarfishingstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarfishingstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarfishingstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarfishingbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarfishingbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarfishingbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarfishingbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarfishingstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarfishinglow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
					
				
				CASE PASSENGER_INTERACTION_THUMB_ON_NOSE			
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarthumb_on_noselow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarthumb_on_nosestd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarthumb_on_nosestd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarthumb_on_nosestd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarthumb_on_nosebodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarthumb_on_nosebodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarthumb_on_nosebodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarthumb_on_nosebodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarthumb_on_nosestd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarthumb_on_noselow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK	
				#ENDIF
				
				// army
				#IF FEATURE_INTERACTIONS_ARMY
				CASE PASSENGER_INTERACTION_BICEP_FLEX 	

				
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarbicep_flexlow@ps@" 	// "amb@code_human_in_car_mp_actions@finger@low@ps@base"
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarbicep_flexstd@rds@" 	// "amb@code_human_in_car_mp_actions@finger@std@rds@base"
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarbicep_flexstd@rps@" 	// "amb@code_human_in_car_mp_actions@finger@std@rps@base"
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarbicep_flexstd@ps@" 	// "amb@code_human_in_car_mp_actions@finger@std@ps@base"
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarbicep_flexbodhi@rds@" // "amb@code_human_in_car_mp_actions@finger@bodhi@rds@base"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarbicep_flexbodhi@rps@" // "amb@code_human_in_car_mp_actions@finger@bodhi@rps@base"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarbicep_flexbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarbicep_flexbodhi@ps@"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarbicep_flexstd@ds@" 	// "amb@code_human_in_car_mp_actions@finger@std@ds@base"
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarbicep_flexlow@ds@" 	// "amb@code_human_in_car_mp_actions@finger@low@ds@base"
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				
				BREAK
				CASE PASSENGER_INTERACTION_COWBOY_GUN 		
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarcowboy_gunlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarcowboy_gunstd@rds@" 	
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarcowboy_gunstd@rps@" 	
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarcowboy_gunstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarcowboy_gunbodhi@rds@" 
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarcowboy_gunbodhi@rps@" 
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarcowboy_gunbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarcowboy_gunbodhi@ps@"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarcowboy_gunstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarcowboy_gunlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				
				BREAK
				CASE PASSENGER_INTERACTION_GUN_HEAD    
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincargun_headlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincargun_headstd@rds@" 	
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincargun_headstd@rps@" 	
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincargun_headstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincargun_headbodhi@rds@" 
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincargun_headbodhi@rps@" 
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincargun_headbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincargun_headbodhi@ps@"
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincargun_headstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincargun_headlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				
				BREAK
				
				CASE PASSENGER_INTERACTION_SLIT_THROAT	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarslit_throatlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarslit_throatstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarslit_throatstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarslit_throatstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarslit_throatbodhi@rds@" 
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarslit_throatbodhi@rps@" 
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarslit_throatbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarslit_throatbodhi@ps@"					
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarslit_throatstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarslit_throatlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				
				BREAK
				
				CASE PASSENGER_INTERACTION_UP_YOURS   
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarup_yourslow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarup_yoursstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarup_yoursstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarup_yoursstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarup_yoursbodhi@rds@" 
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarup_yoursbodhi@rps@" 
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarup_yoursbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarup_yoursbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarup_yoursstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarup_yourslow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				
				BREAK				
				CASE PASSENGER_INTERACTION_WATCHING_YOU    		
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarwatching_youlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarwatching_youstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarwatching_youstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarwatching_youstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarwatching_youbodhi@rds@" 
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarwatching_youbodhi@rps@" 
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarwatching_youbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarwatching_youbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarwatching_youstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarwatching_youlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				
				BREAK	
				
				CASE PASSENGER_INTERACTION_FIST_PUMP    		
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarfist_pumplow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarfist_pumpstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarfist_pumpstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarfist_pumpstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarfist_pumpbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarfist_pumpbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarfist_pumpbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarfist_pumpbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarfist_pumpstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarfist_pumplow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK																
				
				CASE PASSENGER_INTERACTION_PHWAOR    	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarphwaorlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarphwaorstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarphwaorstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarphwaorstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarphwaorbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarphwaorbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarphwaorbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarphwaorbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarphwaorstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarphwaorlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				
				CASE PASSENGER_INTERACTION_PRETEND_CRYING  	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarpretend_cryinglow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarpretend_cryingstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarpretend_cryingstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarpretend_cryingstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarpretend_cryingbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarpretend_cryingbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarpretend_cryingbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarpretend_cryingbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarpretend_cryingstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarpretend_cryinglow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK    					
				#ENDIF	
				
				// unknown
				#IF FEATURE_INTERACTIONS_UNKNOWN
				CASE PASSENGER_INTERACTION_RAVING
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarravinglow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarravingstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarravingstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarravingstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarravingbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarravingbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarravingbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarravingbodhi@ps@"						
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarravingstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarravinglow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"

					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK	
					
				CASE PASSENGER_INTERACTION_DANCE_NEW	
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincardancelow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincardancestd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincardancestd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincardancestd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincardancebodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincardancebodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincardancebodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincardancebodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincardancestd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincardancelow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"
				
					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE	
				BREAK					
				#ENDIF

				#IF FEATURE_CASINO
				CASE PASSENGER_INTERACTION_CRY_BABY
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intin_carcry_babylow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intin_carcry_babystd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intin_carcry_babystd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intin_carcry_babystd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intin_carcry_babybodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intin_carcry_babybodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intin_carcry_babybodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intin_carcry_babybodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intin_carcry_babystd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intin_carcry_babylow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK				
				CASE PASSENGER_INTERACTION_CUT_THROAT
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intin_carcut_throatlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intin_carcut_throatstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intin_carcut_throatstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intin_carcut_throatstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intin_carcut_throatbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intin_carcut_throatbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intin_carcut_throatbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intin_carcut_throatbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intin_carcut_throatstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intin_carcut_throatlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK					
				CASE PASSENGER_INTERACTION_KARATE_CHOPS
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intin_carkarate_chopslow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intin_carkarate_chopsstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intin_carkarate_chopsstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intin_carkarate_chopsstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intin_carkarate_chopsbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intin_carkarate_chopsbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intin_carkarate_chopsbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intin_carkarate_chopsbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intin_carkarate_chopsstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intin_carkarate_chopslow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK								
				CASE PASSENGER_INTERACTION_SHADOW_BOXING
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intin_carshadow_boxinglow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intin_carshadow_boxingstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intin_carshadow_boxingstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intin_carshadow_boxingstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intin_carshadow_boxingbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intin_carshadow_boxingbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intin_carshadow_boxingbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intin_carshadow_boxingbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intin_carshadow_boxingstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intin_carshadow_boxinglow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK					
				CASE PASSENGER_INTERACTION_THE_WOOGIE
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intin_carthe_woogielow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intin_carthe_woogiestd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intin_carthe_woogiestd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intin_carthe_woogiestd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intin_carthe_woogiebodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intin_carthe_woogiebodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intin_carthe_woogiebodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intin_carthe_woogiebodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intin_carthe_woogiestd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intin_carthe_woogielow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK	
				CASE PASSENGER_INTERACTION_STINKER
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intin_carstinkerlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intin_carstinkerstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intin_carstinkerstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intin_carstinkerstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intin_carstinkerbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intin_carstinkerbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intin_carstinkerbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intin_carstinkerbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intin_carstinkerstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intin_carstinkerlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK	
				#ENDIF
				
				#IF FEATURE_CASINO_HEIST
				CASE PASSENGER_INTERACTION_AIR_DRUMS
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarair_drumslow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarair_drumsstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarair_drumsstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarair_drumsstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarair_drumsbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarair_drumsbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarair_drumsbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarair_drumsbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarair_drumsstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarair_drumslow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE				
				BREAK
				CASE PASSENGER_INTERACTION_CALL_ME
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarcall_melow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarcall_mestd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarcall_mestd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarcall_mestd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarcall_mebodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarcall_mebodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarcall_mebodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarcall_mebodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarcall_mestd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarcall_melow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					
				BREAK
				CASE PASSENGER_INTERACTION_COIN_ROLL_AND_TOSS
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarcoin_roll_and_tosslow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarcoin_roll_and_tossstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarcoin_roll_and_tossstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarcoin_roll_and_tossstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarcoin_roll_and_tossbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarcoin_roll_and_tossbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarcoin_roll_and_tossbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarcoin_roll_and_tossbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarcoin_roll_and_tossstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarcoin_roll_and_tosslow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE		
					
					InteractionData.bHasProp = TRUE		
					InteractionData.bDeletePropAtEnd = TRUE
					InteractionData.PropModel = INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_Coin_01a"))
					InteractionData.bForceRightHandAttach = TRUE

					
				BREAK
				CASE PASSENGER_INTERACTION_BANG_BANG
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarbang_banglow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarbang_bangstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarbang_bangstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarbang_bangstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarbang_bangbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarbang_bangbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarbang_bangbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarbang_bangbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarbang_bangstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarbang_banglow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				CASE PASSENGER_INTERACTION_RESPECT
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarrespectlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarrespectstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarrespectstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarrespectstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarrespectbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarrespectbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarrespectbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarrespectbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarrespectstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarrespectlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				CASE PASSENGER_INTERACTION_MIND_BLOWN
					InteractionData.strAnimDict[AD_LOW_PS] 			= "anim@mp_player_intincarmind_blownlow@ps@" 	
					InteractionData.strAnimDict[AD_HIGH_DS_REAR] 		= "anim@mp_player_intincarmind_blownstd@rds@" 
					InteractionData.strAnimDict[AD_HIGH_PS_REAR] 	= "anim@mp_player_intincarmind_blownstd@rps@" 
					InteractionData.strAnimDict[AD_HIGH_PS] 		= "anim@mp_player_intincarmind_blownstd@ps@" 	
					InteractionData.strAnimDict[AD_BOHDI_DS_REAR]		= "anim@mp_player_intincarmind_blownbodhi@rds@"
					InteractionData.strAnimDict[AD_BOHDI_PS_REAR] 		= "anim@mp_player_intincarmind_blownbodhi@rps@"
					InteractionData.strAnimDict[AD_BOHDI_DS] 			= "anim@mp_player_intincarmind_blownbodhi@ds@"
					InteractionData.strAnimDict[AD_BOHDI_PS] 			= "anim@mp_player_intincarmind_blownbodhi@ps@"							
					InteractionData.strAnimDict[AD_HIGH_DS] 		= "anim@mp_player_intincarmind_blownstd@ds@" 	
					InteractionData.strAnimDict[AD_LOW_DS] 			= "anim@mp_player_intincarmind_blownlow@ds@" 	
					
					
					InteractionData.strAnimName = "idle_a"


					InteractionData.strAnimNameIntro = "ENTER"	
					InteractionData.strAnimName	 	= "IDLE_A"	
					InteractionData.strAnimNameOutro = "EXIT"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					
				BREAK
				#ENDIF	

			ENDSWITCH
		BREAK
		CASE INTERACTION_ANIM_TYPE_CREW			
			SWITCH INT_TO_ENUM(CREW_INTERACTIONS, iSelectedAnim)
				CASE CREW_INTERACTION_BRO_LOVE			
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_int_upperbro_love"					
					InteractionData.strAnimNameIntro = "mp_player_int_bro_love_ENTER"	
					InteractionData.strAnimName	 	= "mp_player_int_bro_love"	
					InteractionData.strAnimNameOutro = "mp_player_int_bro_love_EXIT"
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@bro_love"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@bro_love"
					InteractionData.strAnimNameFullBody = "bro_love"
															
					InteractionData.bLeftHandedOnly = TRUE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_HEAD_NECK_AND_L_ARM
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					
					InteractionData.strAnimDict[AD_SELFIE] = "mp_player_int_upperbro_love"	
				BREAK				
				CASE CREW_INTERACTION_FINGER			
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_int_upperfinger"
					InteractionData.strAnimNameIntro = "mp_player_int_finger_02_ENTER"	
					InteractionData.strAnimName	 	= "mp_player_int_finger_02"	
					InteractionData.strAnimNameOutro = "mp_player_int_finger_02_EXIT"
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@finger"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@finger"
					InteractionData.strAnimNameFullBody = "finger"
			
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
				CASE CREW_INTERACTION_WANK				
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_int_upperwank"										
					InteractionData.strAnimNameIntro = "mp_player_int_wank_02_ENTER"	
					InteractionData.strAnimName	 	= "mp_player_int_wank_02"	
					InteractionData.strAnimNameOutro = "mp_player_int_wank_02_EXIT"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@wank"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@wank"
					InteractionData.strAnimNameFullBody = "wank"

					InteractionData.bLeftHandedOnly = TRUE
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
				BREAK
				CASE CREW_INTERACTION_UP_YOURS			
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_int_upperup_yours"	
					InteractionData.strAnimNameIntro = "mp_player_int_up_yours_ENTER"	
					InteractionData.strAnimName	 	= "mp_player_int_up_yours"	
					InteractionData.strAnimNameOutro = "mp_player_int_up_yours_EXIT"
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@up_yours"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@up_yours"
					InteractionData.strAnimNameFullBody = "up_yours"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE INTERACTION_ANIM_TYPE_PLAYER	
			SWITCH INT_TO_ENUM(PLAYER_INTERACTIONS, iSelectedAnim)
				CASE PLAYER_INTERACTION_NONE
					// empty
				BREAK			
			
				CASE PLAYER_INTERACTION_FINGER			
					// business pack, new version
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperfinger"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@finger"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@finger"
					InteractionData.strAnimNameFullBody = "finger"
							
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intselfiethe_bird"	
					
				BREAK
				CASE PLAYER_INTERACTION_ROCK			
					// business pack, new version
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperrock"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimNameFullBody = "rock"
							
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE	
					
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperrock"
				BREAK
				CASE PLAYER_INTERACTION_SALUTE			
					// business pack, new version
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppersalute"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@salute"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@salute"
					InteractionData.strAnimNameFullBody = "salute"
						
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE					
					InteractionData.bLeftHandedOnly = TRUE
					
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intuppersalute"
				BREAK
				CASE PLAYER_INTERACTION_WANK			
					// business pack, new version
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperwank"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@wank"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@wank"
					InteractionData.strAnimNameFullBody = "wank"
					
					InteractionData.bLeftHandedOnly = TRUE
					InteractionData.bIsLooped = TRUE		
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intselfiewank"
				BREAK	
				CASE PLAYER_INTERACTION_SMOKE
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_int_uppersmoke"	
					InteractionData.strAnimNameIntro = "mp_player_int_smoke_ENTER"	
					InteractionData.strAnimName	 	= "mp_player_int_smoke"	
					InteractionData.strAnimNameOutro = "mp_player_int_smoke_EXIT"
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@smoke_flick"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@smoke_flick"
					InteractionData.strAnimNameFullBody = "smoke_flick"										
					InteractionData.bLeftHandedOnly = TRUE
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					//InteractionData.iPlayerInteractionFlags, PIBD_IsSmokingInteraction = TRUE
					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					
					//InteractionData.strCreateEvent = "CREATE_CIGGY"
					//InteractionData.strDestroyEvent = "DELETE_CIGGY"
					//InteractionData.strLoopEvent = "EXHALE"
	
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"
					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>
	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"
					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"
					
					InteractionData.iMinLoopIterations = 1
					
					
				BREAK
				CASE PLAYER_INTERACTION_DRINK_1
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_intdrink"	
					InteractionData.strAnimNameIntro = "Intro"	
					InteractionData.strAnimName	 	= "Loop"	
					InteractionData.strAnimNameOutro = "Outro"

					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimNameFullBody = "rock"
					InteractionData.bLeftHandedOnly = TRUE
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					InteractionData.bHasProp = TRUE
					InteractionData.PropModel = Prop_Ecola_Can	
					
					InteractionData.iMinLoopIterations = 1
					
					InteractionData.bUseSounds = TRUE
					
				BREAK
				CASE PLAYER_INTERACTION_DRINK_2
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_intdrink"	
					InteractionData.strAnimNameIntro = "Intro_bottle"	
					InteractionData.strAnimName	 	= "Loop_bottle"	
					InteractionData.strAnimNameOutro = "Outro_bottle"

					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimNameFullBody = "rock"
					InteractionData.bLeftHandedOnly = TRUE
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					InteractionData.bHasProp = TRUE
					InteractionData.PropModel = Prop_AMB_Beer_Bottle

					InteractionData.iLoopEventDrunk = 3
					InteractionData.iMinLoopIterations = 1
					
					InteractionData.bUseSounds = TRUE											
					
				BREAK
				CASE PLAYER_INTERACTION_DRINK_3
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperspray_champagne"
					InteractionData.strAnimNameIntro = "enter"
					InteractionData.strAnimName	 	= "idle_a"
					InteractionData.strAnimNameOutro = "exit"
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@spray_champagne"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@spray_champagne"
					InteractionData.strAnimNameFullBody = "spray_champagne"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					InteractionData.bHasProp = TRUE
					InteractionData.bBlockForceProp = TRUE
					InteractionData.PropModel = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Champ_Closed"))
					InteractionData.bHasCooldown = TRUE
					InteractionData.bForceRightHandAttach = TRUE
					InteractionData.iLoopEventDrunk = 3
					InteractionData.iMinLoopIterations = 1
					InteractionData.bUseSounds = TRUE
					InteractionData.bUsesPTFX = TRUE
				BREAK
				#IF FEATURE_DLC_1_2022
				CASE PLAYER_INTERACTION_DRINK_4
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_intdrink"	
					InteractionData.strAnimNameIntro = "Intro"	
					InteractionData.strAnimName	 	= "Loop"	
					InteractionData.strAnimNameOutro = "Outro"

					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimNameFullBody = "rock"
					InteractionData.bLeftHandedOnly = TRUE
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					InteractionData.bHasProp = TRUE
					InteractionData.PropModel = Prop_LD_Can_01b	
					
					InteractionData.iMinLoopIterations = 1
					
					InteractionData.bUseSounds = TRUE
					
				BREAK
				#ENDIF
				CASE PLAYER_INTERACTION_MAKE_IT_RAIN
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperraining_cash"
					InteractionData.strAnimNameIntro = "enter"
					InteractionData.strAnimName	 	= "idle_a"
					InteractionData.strAnimNameOutro = "exit"
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@raining_cash"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@raining_cash"
					InteractionData.strAnimNameFullBody = "raining_cash"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					InteractionData.bHasProp = TRUE
					InteractionData.bBlockForceProp = TRUE
					InteractionData.bDeletePropAtEnd = TRUE
					InteractionData.bForceLeftHandAttach = TRUE
					InteractionData.PropModel = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Cash_Pile_L"))
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperraining_cash"
					InteractionData.bUsesPTFX = TRUE
				BREAK
				CASE PLAYER_INTERACTION_PLAY_MASK_SFX
					InteractionData.strSpeechContext = "HLWN_22" 
					InteractionData.strSpeechVoiceName = "Mask_SFX"
					InteractionData.strSpeechParams = "SPEECH_PARAMS_FORCE_NORMAL"
					InteractionData.bSpeechOnly = TRUE
				BREAK
				CASE PLAYER_INTERACTION_EAT_1
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_inteat@pnq"	
					InteractionData.strAnimNameIntro = "intro"	
					InteractionData.strAnimName	 	= "loop"	
					InteractionData.strAnimNameOutro = "outro"
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimNameFullBody = "rock"

					InteractionData.bLeftHandedOnly = TRUE
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					InteractionData.bHasProp = TRUE
					InteractionData.PropModel = Prop_Choc_PQ	
	
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_HEAD_NECK_AND_L_ARM
					InteractionData.iMinLoopIterations = 1
					
					InteractionData.bUseSounds = TRUE						

				BREAK
				CASE PLAYER_INTERACTION_EAT_2
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_inteat@burger"	
					InteractionData.strAnimNameIntro = "mp_player_int_eat_burger_enter"	
					InteractionData.strAnimName	 	= "mp_player_int_eat_burger"	
					InteractionData.strAnimNameOutro = "mp_player_int_eat_exit_burger"
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimNameFullBody = "rock"
			
					InteractionData.bLeftHandedOnly = TRUE
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					InteractionData.bHasProp = TRUE
					InteractionData.PropModel = Prop_Choc_EGO	

					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_HEAD_NECK_AND_L_ARM
					InteractionData.iMinLoopIterations = 1					
	
					InteractionData.bUseSounds = TRUE
		
				BREAK
				CASE PLAYER_INTERACTION_EAT_3
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_inteat@burger"	
					InteractionData.strAnimNameIntro = "mp_player_int_eat_burger_enter"	
					InteractionData.strAnimName	 	= "mp_player_int_eat_burger"	
					InteractionData.strAnimNameOutro = "mp_player_int_eat_exit_burger"
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationmale@rock"
					InteractionData.strAnimNameFullBody = "rock"
				
					InteractionData.bLeftHandedOnly = TRUE
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					InteractionData.bHasProp = TRUE
					InteractionData.PropModel = Prop_Choc_Meto	

	
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_HEAD_NECK_AND_L_ARM
					InteractionData.iMinLoopIterations = 1
					
					InteractionData.bUseSounds = TRUE
					
				BREAK
				
				
				CASE PLAYER_INTERACTION_AIR_SHAGGING    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperair_shagging"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@air_shagging"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@air_shagging"
					InteractionData.strAnimNameFullBody = "air_shagging"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
				BREAK	
				
				CASE PLAYER_INTERACTION_DOCK        
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperdock"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@dock"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@dock"
					InteractionData.strAnimNameFullBody = "dock"
						
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
					
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intselfiedock"
					
				BREAK					

				CASE PLAYER_INTERACTION_KNUCKLE_CRUNCH     
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperknuckle_crunch"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@knuckle_crunch"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@knuckle_crunch"
					InteractionData.strAnimNameFullBody = "knuckle_crunch"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					
					
					InteractionData.bUseSounds = TRUE
					
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK				
				
				
				CASE PLAYER_INTERACTION_BLOW_KISS   
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperblow_kiss"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@blow_kiss"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@blow_kiss"
					InteractionData.strAnimNameFullBody = "blow_kiss"
	
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE
					//InteractionData.bRemoveClosedMaskHelmet = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
					InteractionData.bLeftHandedOnly = TRUE
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intselfieblow_kiss"
					
				BREAK	
				
				// Business Pack 2
				
				CASE PLAYER_INTERACTION_SLOW_CLAP        
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperslow_clap"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@slow_clap"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@slow_clap"
					InteractionData.strAnimNameFullBody = "slow_clap"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bUseSounds = TRUE
					
				BREAK
				CASE PLAYER_INTERACTION_FACE_PALM     
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperface_palm"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@face_palm"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@face_palm"
					InteractionData.strAnimNameFullBody = "face_palm"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					//InteractionData.bRemoveClosedMaskHelmet = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
					InteractionData.bLeftHandedOnly = TRUE
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperface_palm"
					
				BREAK	
				CASE PLAYER_INTERACTION_THUMBS_UP  
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperthumbs_up"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@thumbs_up"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@thumbs_up"
					InteractionData.strAnimNameFullBody = "thumbs_up"
	
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intselfiethumbs_up"
					
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK
				
				
				CASE PLAYER_INTERACTION_JAZZ_HANDS       
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperjazz_hands"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@jazz_hands"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@jazz_hands"
					InteractionData.strAnimNameFullBody = "jazz_hands"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE	
					InteractionData.bCanSkipLoop = TRUE
					
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperjazz_hands"
							
				BREAK
				CASE PLAYER_INTERACTION_NOSE_PICK    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppernose_pick"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@nose_pick"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@nose_pick"
					InteractionData.strAnimNameFullBody = "nose_pick"
	
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					//InteractionData.bRemoveClosedMaskHelmet = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
					InteractionData.bLeftHandedOnly = TRUE
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intuppernose_pick"
					
				BREAK
				
				// hipster
				CASE PLAYER_INTERACTION_WAVE  
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperwave"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@wave"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@wave"
					InteractionData.strAnimNameFullBody = "wave"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE	
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bLeftHandedOnly = TRUE
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperwave"
									
				BREAK	
				CASE PLAYER_INTERACTION_AIR_GUITAR   
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperair_guitar"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@air_guitar"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@air_guitar"
					InteractionData.strAnimNameFullBody = "air_guitar"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE	
					InteractionData.bCanSkipLoop = TRUE
			
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperair_guitar"
					
				BREAK	

				CASE PLAYER_INTERACTION_SURRENDER       
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppersurrender"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@surrender"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@surrender"
					InteractionData.strAnimNameFullBody = "surrender"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
										
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intuppersurrender"
				BREAK
				
				CASE PLAYER_INTERACTION_SHUSH       
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppershush"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@shush"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@shush"
					InteractionData.strAnimNameFullBody = "shush"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.bRemoveClosedMaskHelmet = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
					InteractionData.bLeftHandedOnly = TRUE
										
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intuppershush"
				BREAK
				
				CASE PLAYER_INTERACTION_PHOTOGRAPHY       
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperphotography"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@photography"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@photography"
					InteractionData.strAnimNameFullBody = "photography"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.bRemoveClosedMaskHelmet = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
										
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperphotography"
				BREAK
				
				CASE PLAYER_INTERACTION_DJ       
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperdj"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@dj"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@dj"
					InteractionData.strAnimNameFullBody = "dj"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
					//InteractionData.bRemoveClosedMaskHelmet = TRUE
										
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperdj"
				BREAK
				
				CASE PLAYER_INTERACTION_AIR_SYNTH       
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperair_synth"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@air_synth"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@air_synth"
					InteractionData.strAnimNameFullBody = "air_synth"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
										
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperair_synth"
				BREAK
				
				
				CASE PLAYER_INTERACTION_NO_WAY    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperno_way"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@no_way"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@no_way"
					InteractionData.strAnimNameFullBody = "no_way"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperno_way"
				BREAK					
				
				CASE PLAYER_INTERACTION_CHIN_BRUSH     
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperchin_brush"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@chin_brush"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@chin_brush"
					InteractionData.strAnimNameFullBody = "chin_brush"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperchin_brush"	
				BREAK
				CASE PLAYER_INTERACTION_CHICKEN_TAUNT           					
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperchicken_taunt"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@chicken_taunt"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@chicken_taunt"
					InteractionData.strAnimNameFullBody = "chicken_taunt"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE	
					InteractionData.bCanSkipLoop = TRUE
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperchicken_taunt"	

				BREAK	
				
				CASE PLAYER_INTERACTION_PEACE      
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperpeace"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@peace"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@peace"
					InteractionData.strAnimNameFullBody = "peace"
			
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
					
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperpeace"
					
				BREAK
				CASE PLAYER_INTERACTION_FINGER_KISS 
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperfinger_kiss"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@finger_kiss"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@finger_kiss"
					InteractionData.strAnimNameFullBody = "finger_kiss"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE	
					InteractionData.bCanSkipLoop = TRUE
									
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperfinger_kiss"				
				BREAK		
				CASE PLAYER_INTERACTION_YOU_LOCO                				
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperyou_loco"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@you_loco"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@you_loco"
					InteractionData.strAnimNameFullBody = "you_loco"
			
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperyou_loco"	
				BREAK					
				
				// halloween
				CASE PLAYER_INTERACTION_FREAKOUT    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperfreakout"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@freakout"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@freakout"
					InteractionData.strAnimNameFullBody = "freakout"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE	
					InteractionData.bCanSkipLoop = TRUE
									
				BREAK   				
				CASE PLAYER_INTERACTION_THUMB_ON_EARS    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperthumb_on_ears"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@thumb_on_ears"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@thumb_on_ears"
					InteractionData.strAnimNameFullBody = "thumb_on_ears"
	
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK				
				
				// sports
				#IF FEATURE_INTERACTIONS_SPORTS
				CASE PLAYER_INTERACTION_BRO_LOVE                
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperbro_love"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@bro_love"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@bro_love"
					InteractionData.strAnimNameFullBody = "bro_love"

					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK					
				CASE PLAYER_INTERACTION_FAKE_HORSE  
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperfake_horse"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@fake_horse"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@fake_horse"
					InteractionData.strAnimNameFullBody = "fake_horse"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
	
				BREAK				
				CASE PLAYER_INTERACTION_LOSER      
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperloser"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@loser"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@loser"
					InteractionData.strAnimNameFullBody = "loser"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
							
				BREAK	      					
				CASE PLAYER_INTERACTION_OK 
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperok"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@ok"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@ok"
					InteractionData.strAnimNameFullBody = "ok"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
		
				BREAK	
				CASE PLAYER_INTERACTION_FISHING     
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperfishing"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@fishing"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@fishing"
					InteractionData.strAnimNameFullBody = "fishing"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE	
					InteractionData.bCanSkipLoop = TRUE
									
				BREAK	
				CASE PLAYER_INTERACTION_THUMB_ON_NOSE
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperthumb_on_nose"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@thumb_on_nose"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@thumb_on_nose"
					InteractionData.strAnimNameFullBody = "thumb_on_nose"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
										
				BREAK	
				#ENDIF					
				
				// army
				#IF FEATURE_INTERACTIONS_ARMY
				CASE PLAYER_INTERACTION_BICEP_FLEX    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperbicep_flex"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@bicep_flex"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@bicep_flex"
					InteractionData.strAnimNameFullBody = "bicep_flex"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
				BREAK	

				CASE PLAYER_INTERACTION_COWBOY_GUN   
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppercowboy_gun"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@cowboy_gun"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@cowboy_gun"
					InteractionData.strAnimNameFullBody = "cowboy_gun"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK	
				CASE PLAYER_INTERACTION_GUN_HEAD        
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppergun_head"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@gun_head"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@gun_head"
					InteractionData.strAnimNameFullBody = "gun_head"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK	
				CASE PLAYER_INTERACTION_SLIT_THROAT   
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperslit_throat"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@slit_throat"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@slit_throat"
					InteractionData.strAnimNameFullBody = "slit_throat"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK		
				
				
				CASE PLAYER_INTERACTION_UP_YOURS   
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperup_yours"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@up_yours"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@up_yours"
					InteractionData.strAnimNameFullBody = "up_yours"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK	
				
				CASE PLAYER_INTERACTION_WATCHING_YOU    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperwatching_you"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@watching_you"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@watching_you"
					InteractionData.strAnimNameFullBody = "watching_you"
							
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK	
				CASE PLAYER_INTERACTION_SALUTE_2    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppersalute_v2"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@salute_v2"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@salute_v2"
					InteractionData.strAnimNameFullBody = "salute_v2"
							
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK	
				CASE PLAYER_INTERACTION_SALUTE_3    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppersalute_v3"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@salute_v3"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@salute_v3"
					InteractionData.strAnimNameFullBody = "salute_v3"
							
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK	
				CASE PLAYER_INTERACTION_SALUTE_4    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppersalute_v4"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@salute_v4"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@salute_v4"
					InteractionData.strAnimNameFullBody = "salute_v4"
							
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK	
				CASE PLAYER_INTERACTION_SALUTE_5    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppersalute_v5"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@salute_v5"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@salute_v5"
					InteractionData.strAnimNameFullBody = "salute_v5"
							
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK	
				CASE PLAYER_INTERACTION_SALUTE_6    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppersalute_v6"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@salute_v6"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@salute_v6"
					InteractionData.strAnimNameFullBody = "salute_v6"
							
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
				BREAK
				CASE PLAYER_INTERACTION_FIST_PUMP    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperfist_pump"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@fist_pump"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@fist_pump"
					InteractionData.strAnimNameFullBody = "fist_pump"
					
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER					
				BREAK																										
				CASE PLAYER_INTERACTION_PHWAOR    
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperphwaor"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@phwaor"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@phwaor"
					InteractionData.strAnimNameFullBody = "phwaor"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE	
					InteractionData.bCanSkipLoop = TRUE
									
				BREAK
				
				CASE PLAYER_INTERACTION_PRETEND_CRYING   
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperpretend_crying"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@pretend_crying"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@pretend_crying"
					InteractionData.strAnimNameFullBody = "pretend_crying"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					
				BREAK    					
				#ENDIF		
				
				// unknown
				#IF FEATURE_INTERACTIONS_UNKNOWN
				CASE PLAYER_INTERACTION_RAVING  
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperraving"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@raving"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@raving"
					InteractionData.strAnimNameFullBody = "raving"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE	
					InteractionData.bCanSkipLoop = TRUE
								
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperraving"										
				BREAK	
				CASE PLAYER_INTERACTION_DANCE       
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperdance"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"		
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@dance"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@dance"
					InteractionData.strAnimNameFullBody = "dance"
				
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER
					
					
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperdance"
					
				BREAK				
				#ENDIF
				
				
				CASE PLAYER_INTERACTION_BANGING_TUNES
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperbanging_tunes"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@banging_tunes"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@banging_tunes"
					InteractionData.strAnimNameFullBody = "banging_tunes"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = InteractionData.strAnimDict[AD_DEFAULT]
				BREAK
				CASE PLAYER_INTERACTION_BANGING_TUNES_LEFT
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperbanging_tunes_left"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@banging_tunes_left"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@banging_tunes_left"
					InteractionData.strAnimNameFullBody = "banging_tunes"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = InteractionData.strAnimDict[AD_DEFAULT]	
				BREAK
				CASE PLAYER_INTERACTION_BANGING_TUNES_RIGHT
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperbanging_tunes_right"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@banging_tunes_right"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@banging_tunes_right"
					InteractionData.strAnimNameFullBody = "banging_tunes"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = InteractionData.strAnimDict[AD_DEFAULT]
				BREAK
				CASE PLAYER_INTERACTION_OH_SNAP
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperoh_snap"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@oh_snap"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@oh_snap"
					InteractionData.strAnimNameFullBody = "oh_snap"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = InteractionData.strAnimDict[AD_DEFAULT]
				BREAK
				CASE PLAYER_INTERACTION_CATS_CRADLE
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppercats_cradle"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@cats_cradle"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@cats_cradle"
					InteractionData.strAnimNameFullBody = "cats_cradle"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = InteractionData.strAnimDict[AD_DEFAULT]
				BREAK
				CASE PLAYER_INTERACTION_RAISE_THE_ROOF
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperraise_the_roof"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@raise_the_roof"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@raise_the_roof"
					InteractionData.strAnimNameFullBody = "raise_the_roof"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = InteractionData.strAnimDict[AD_DEFAULT]
				BREAK
				CASE PLAYER_INTERACTION_FIND_THE_FISH
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperfind_the_fish"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@find_the_fish"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@find_the_fish"
					InteractionData.strAnimNameFullBody = "find_the_fish"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = InteractionData.strAnimDict[AD_DEFAULT]
				BREAK
				CASE PLAYER_INTERACTION_SALSA_ROLL
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppersalsa_roll"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@salsa_roll"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@salsa_roll"
					InteractionData.strAnimNameFullBody = "salsa_roll"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = InteractionData.strAnimDict[AD_DEFAULT]
				BREAK
				CASE PLAYER_INTERACTION_HEART_PUMPING
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperheart_pumping"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@heart_pumping"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@heart_pumping"
					InteractionData.strAnimNameFullBody = "heart_pumping"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = InteractionData.strAnimDict[AD_DEFAULT]
				BREAK
				CASE PLAYER_INTERACTION_UNCLE_DISCO
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperuncle_disco"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@uncle_disco"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@uncle_disco"
					InteractionData.strAnimNameFullBody = "uncle_disco"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = InteractionData.strAnimDict[AD_DEFAULT]
				BREAK
				
				#IF FEATURE_CASINO
				CASE PLAYER_INTERACTION_CRY_BABY
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppercry_baby"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@cry_baby"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@cry_baby"
					InteractionData.strAnimNameFullBody = "cry_baby"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intuppercry_baby"
				BREAK
				
				CASE PLAYER_INTERACTION_CUT_THROAT
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppercut_throat"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@cut_throat"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@cut_throat"
					InteractionData.strAnimNameFullBody = "cut_throat"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intuppercut_throat"
				BREAK
	
				CASE PLAYER_INTERACTION_KARATE_CHOPS
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperkarate_chops"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@karate_chops"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@karate_chops"
					InteractionData.strAnimNameFullBody = "karate_chops"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperkarate_chops"
				BREAK			
				
				CASE PLAYER_INTERACTION_SHADOW_BOXING
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppershadow_boxing"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@shadow_boxing"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@shadow_boxing"
					InteractionData.strAnimNameFullBody = "shadow_boxing"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intuppershadow_boxing"
				BREAK	

				CASE PLAYER_INTERACTION_THE_WOOGIE
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperthe_woogie"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@the_woogie"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@the_woogie"
					InteractionData.strAnimNameFullBody = "the_woogie"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperthe_woogie"
				BREAK	

				CASE PLAYER_INTERACTION_STINKER
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperstinker"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@stinker"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@stinker"
					InteractionData.strAnimNameFullBody = "stinker"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperstinker"
				BREAK
				
				#ENDIF			
				
				#IF FEATURE_CASINO_HEIST
				CASE PLAYER_INTERACTION_AIR_DRUMS
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperair_drums"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@air_drums"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@air_drums"
					InteractionData.strAnimNameFullBody = "air_drums"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperair_drums"
				BREAK
				CASE PLAYER_INTERACTION_CALL_ME
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppercall_me"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@call_me"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@call_me"
					InteractionData.strAnimNameFullBody = "call_me"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intuppercall_me"
				BREAK
				CASE PLAYER_INTERACTION_COIN_ROLL_AND_TOSS
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppercoin_roll_and_toss"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@coin_roll_and_toss"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@coin_roll_and_toss"
					InteractionData.strAnimNameFullBody = "coin_roll_and_toss"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intuppercoin_roll_and_toss"
					InteractionData.bHasProp = TRUE		
					InteractionData.bDeletePropAtEnd = TRUE
					InteractionData.PropModel = INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_Coin_01a"))
					InteractionData.bForceRightHandAttach = TRUE
				BREAK
				CASE PLAYER_INTERACTION_BANG_BANG
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperbang_bang"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@bang_bang"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@bang_bang"
					InteractionData.strAnimNameFullBody = "bang_bang"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperbang_bang"
				BREAK
				CASE PLAYER_INTERACTION_RESPECT
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperrespect"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@respect"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@respect"
					InteractionData.strAnimNameFullBody = "respect"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intupperrespect"
				BREAK
				CASE PLAYER_INTERACTION_MIND_BLOWN
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppermind_blown"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@mind_blown"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@mind_blown"
					InteractionData.strAnimNameFullBody = "mind_blown"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim@mp_player_intuppermind_blown"
				BREAK
				#ENDIF
				#IF FEATURE_COPS_N_CROOKS
				CASE PLAYER_INTERACTION_POUR_ONE_OUT
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@pour_one_out"
					InteractionData.strAnimNameIntro = "enter"
					InteractionData.strAnimName	 	= "idle_a"
					InteractionData.strAnimNameOutro = "exit"
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@pour_one_out"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@pour_one_out"
					InteractionData.strAnimNameFullBody = "POUR_ONE_OUT"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.bCanQuitLoopEarly = FALSE
					InteractionData.bHasProp = TRUE
					InteractionData.bBlockForceProp = TRUE
					InteractionData.bDeletePropAtEnd = FALSE
					InteractionData.PropModel = INT_TO_ENUM(MODEL_NAMES, HASH("ac_prop_ac_Whiskey_Bottle_S"))
					InteractionData.bForceRightHandAttach = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NONE
					InteractionData.bUseSounds = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.bBlockForceProp = TRUE
				BREAK
				CASE PLAYER_INTERACTION_HAND_SIGNALS
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@hand_signals"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@hand_signals"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@hand_signals"
					InteractionData.strAnimNameFullBody = "hand_signals"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@hand_signals"
				BREAK
				CASE PLAYER_INTERACTION_TACTICAL_SALUTE
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@salute"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@salute"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@salute"
					InteractionData.strAnimNameFullBody = "salute"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NONE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@salute"
				BREAK
				CASE PLAYER_INTERACTION_TECHY_FINGERS
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@techy_fingers"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@techy_fingers"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@techy_fingers"
					InteractionData.strAnimNameFullBody = "techy_fingers"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@techy_fingers"
				BREAK
				CASE PLAYER_INTERACTION_TALK_INTO_EAR_PIECE
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@talkinto_specialist"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@talkinto_earpiece"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@talkinto_earpiece"
					InteractionData.strAnimNameFullBody = "talkinto_earpiece"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NONE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@talkinto_specialist"
				BREAK
				CASE PLAYER_INTERACTION_BATON_TWIRL
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@baton_twirl"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@baton_twirl"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@baton_twirl"
					InteractionData.strAnimNameFullBody = "baton_twirl"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NONE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@baton_twirl"
					InteractionData.bHasProp = TRUE
					InteractionData.bBlockForceProp = TRUE
					InteractionData.bDeletePropAtEnd = TRUE
					InteractionData.bForceRightHandAttach = TRUE
					InteractionData.PropModel = INT_TO_ENUM(MODEL_NAMES, HASH("W_ME_Nightstick"))
					InteractionData.bBlockForceProp = TRUE
				BREAK
				CASE PLAYER_INTERACTION_FLASH_BADGE
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@flash_cop_badge"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@flash_cop_badge"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@flash_cop_badge"
					InteractionData.strAnimNameFullBody = "flash_cop_badge"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NONE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@flash_cop_badge"
					InteractionData.bHasProp = TRUE
					InteractionData.bBlockForceProp = TRUE
					InteractionData.bDeletePropAtEnd = TRUE
					InteractionData.bForceRightHandAttach = TRUE
					InteractionData.PropModel = INT_TO_ENUM(MODEL_NAMES, HASH("ac_prop_CH_police_badge01x"))
					InteractionData.bBlockForceProp = TRUE
				BREAK
				CASE PLAYER_INTERACTION_EAT_DOUGHNUT
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@eat_doughnut"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@eat_doughnut"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@eat_doughnut"
					InteractionData.strAnimNameFullBody = "eat_doughnut"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NONE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@eat_doughnut"
					InteractionData.bHasProp = TRUE
					InteractionData.bBlockForceProp = TRUE
					InteractionData.bDeletePropAtEnd = TRUE
					InteractionData.bForceRightHandAttach = TRUE
					InteractionData.PropModel = INT_TO_ENUM(MODEL_NAMES, HASH("ac_prop_ac_doughnut_01a"))
					InteractionData.bBlockForceProp = TRUE
				BREAK
				CASE PLAYER_INTERACTION_HAND_RUB
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@hand_rub"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@hand_rub"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@hand_rub"
					InteractionData.strAnimNameFullBody = "hand_rub"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@hand_rub"
				BREAK
				CASE PLAYER_INTERACTION_SOLIDER_PEACE
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@peace"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@peace"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@peace"
					InteractionData.strAnimNameFullBody = "peace"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NONE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@peace"
				BREAK
				CASE PLAYER_INTERACTION_BRING_IT
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@bring_it"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@bring_it"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@bring_it"
					InteractionData.strAnimNameFullBody = "bring_it"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@bring_it"
				BREAK
				CASE PLAYER_INTERACTION_JACKED_UP
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@jacked_up"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@jacked_up"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@jacked_up"
					InteractionData.strAnimNameFullBody = "jacked_up"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NONE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@jacked_up"
				BREAK
				CASE PLAYER_INTERACTION_HACKY_FINGERS
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@hacky_fingers"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@hacky_fingers"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@hacky_fingers"
					InteractionData.strAnimNameFullBody = "hacky_fingers"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@hacky_fingers"
				BREAK
				CASE PLAYER_INTERACTION_WASNT_ME
					InteractionData.strAnimDict[AD_DEFAULT] = "anim_cnc@mp_player_intuppermale@wasnt_me"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim_cnc@mp_player_intcelebrationmale@wasnt_me"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim_cnc@mp_player_intcelebrationmale@wasnt_me"
					InteractionData.strAnimNameFullBody = "wasnt_me"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
					InteractionData.strAnimDict[AD_SELFIE] = "anim_cnc@mp_player_intuppermale@wasnt_me"
				BREAK
				#ENDIF
				
				#IF FEATURE_HEIST_ISLAND_DANCES
				CASE PLAYER_INTERACTION_CROWD_INVITATION
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppercrowd_invitation"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@crowd_invitation"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@crowd_invitation"
					InteractionData.strAnimNameFullBody = "crowd_invitation"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
				BREAK
				
				CASE PLAYER_INTERACTION_DRIVER
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperdriver"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@driver"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@driver"
					InteractionData.strAnimNameFullBody = "driver"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
				BREAK
				
				CASE PLAYER_INTERACTION_RUNNER
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intupperrunner"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@runner"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@runner"
					InteractionData.strAnimNameFullBody = "runner"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
				BREAK
				
				CASE PLAYER_INTERACTION_SHOOTING
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppershooting"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@shooting"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@shooting"
					InteractionData.strAnimNameFullBody = "shooting"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
				BREAK
				
				CASE PLAYER_INTERACTION_SUCK_IT
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppersuck_it"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@suck_it"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@suck_it"
					InteractionData.strAnimNameFullBody = "suck_it"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanSkipLoop = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE						
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE
				BREAK
				
				CASE PLAYER_INTERACTION_TAKE_SELFIE
					InteractionData.strAnimDict[AD_DEFAULT] = "anim@mp_player_intuppertake_selfie"					
					InteractionData.strAnimNameIntro = "enter"	
					InteractionData.strAnimName	 	= "idle_a"	
					InteractionData.strAnimNameOutro = "exit"			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_player_intcelebrationmale@take_selfie"
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_player_intcelebrationfemale@take_selfie"
					InteractionData.strAnimNameFullBody = "take_selfie"
					InteractionData.bIsLooped = TRUE
					InteractionData.bCanQuitLoopEarly = FALSE
					InteractionData.bCanSkipLoop = FALSE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_NONE
					InteractionData.bHasProp = TRUE
					InteractionData.bBlockForceProp = TRUE
					InteractionData.bDeletePropAtEnd = TRUE
					InteractionData.bForceRightHandAttach = FALSE
					InteractionData.bForceLeftHandAttach = TRUE
					InteractionData.PropModel = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Phone_ING_02"))
				BREAK
				#ENDIF
			ENDSWITCH
		BREAK
		CASE INTERACTION_ANIM_TYPE_SPECIAL
			SWITCH INT_TO_ENUM(SPECIAL_INTERACTIONS, iSelectedAnim)
				CASE SPECIAL_INTERACTION_SHAKE_HEAD	
					InteractionData.strAnimDict[AD_DEFAULT] = "mp_player_int_upper_nod"	
					InteractionData.strAnimName	 	= "mp_player_int_nod_no"	
					InteractionData.bIsLooped = FALSE
					InteractionData.bLeftHandedOnly = FALSE
					//InteractionData.iForcedAnimFilter = FORCED_BONEMASK_HEAD_NECK_AND_ARMS
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_HEAD_NECK_AND_L_ARM
					
					
					InteractionData.strAnimDict[AD_SELFIE] = "mp_player_int_upper_nod"					
				BREAK

				CASE SPECIAL_INTERACTION_SHAKE_HEAD_DRIVER_LOW
					InteractionData.strAnimDict[AD_DEFAULT] = "amb@code_human_in_car_mp_actions@nod@low@ds@base"	
					InteractionData.strAnimName	 	= "nod_no"	
					InteractionData.bIsLooped = FALSE
					InteractionData.bLeftHandedOnly = FALSE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_HEAD_NECK_AND_ARMS
				BREAK
				CASE SPECIAL_INTERACTION_SHAKE_HEAD_DRIVER_FORMULA
					InteractionData.strAnimDict[AD_DEFAULT] = "amb@code_human_in_car_mp_actions@nod@low@ds@base"	
					InteractionData.strAnimName	 	= "nod_no"	
					InteractionData.bIsLooped = FALSE
					InteractionData.bLeftHandedOnly = FALSE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_HEADONLY
				BREAK
								
				CASE SPECIAL_INTERACTION_SHAKE_HEAD_DRIVER_STANDARD
					InteractionData.strAnimDict[AD_DEFAULT] = "amb@code_human_in_car_mp_actions@nod@std@ds@base"	
					InteractionData.strAnimName	 	= "nod_no"	
					InteractionData.bIsLooped = FALSE
					InteractionData.bLeftHandedOnly = FALSE
					InteractionData.iForcedAnimFilter = FORCED_BONEMASK_HEAD_NECK_AND_ARMS	
				BREAK
				
				CASE SPECIAL_AFTER_HEIST_IDLE_A
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_freemode_return@m@idle"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_freemode_return@f@idle"	
					InteractionData.strAnimNameFullBody	 = "idle_a"																			
				BREAK								
				CASE SPECIAL_AFTER_HEIST_IDLE_B
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_freemode_return@m@idle"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_freemode_return@f@idle"	
					InteractionData.strAnimNameFullBody	 = "idle_b"	
				BREAK
				CASE SPECIAL_AFTER_HEIST_IDLE_C
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_freemode_return@m@idle"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_freemode_return@f@idle"	
					InteractionData.strAnimNameFullBody	 = "idle_c"		
					
					IF (bSexIsKnown)
					AND (bIsFemale)
						// female version does not have a phone
					ELSE
						InteractionData.bHasProp = TRUE		
						InteractionData.bDeletePropAtEnd = TRUE
						InteractionData.PropModel = Prop_NPC_Phone
					ENDIF
										
				BREAK
				CASE SPECIAL_AFTER_HEIST_FAIL_A
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@mp_freemode_return@m@fail"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@mp_freemode_return@f@fail"	
					InteractionData.strAnimNameFullBody	 = "fail_a"	
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_A_PED_A
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_a"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_a"		
					InteractionData.strAnimNameFullBody	 = "respawn_a_ped_a"	
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_A_PED_B
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_a"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_a"		
					InteractionData.strAnimNameFullBody	 = "respawn_a_ped_b_smoke"
					
					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"
					
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_A_PED_C
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_a"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_a"		
					InteractionData.strAnimNameFullBody	 = "respawn_a_ped_c"
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_A_PED_D
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_a"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_a"		
					InteractionData.strAnimNameFullBody	 = "respawn_a_ped_d_smoke"
					
					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"
					
				BREAK
				
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_B_PED_A
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_b"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_b"		
					InteractionData.strAnimNameFullBody	 = "respawn_b_ped_a"
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_B_PED_B
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_b"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_b"		
					InteractionData.strAnimNameFullBody	 = "respawn_b_ped_b_smoke"
					
					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"
					
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_B_PED_C
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_b"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_b"		
					InteractionData.strAnimNameFullBody	 = "respawn_b_ped_c"
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_B_PED_D
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_b"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_b"		
					InteractionData.strAnimNameFullBody	 = "respawn_b_ped_d_smoke"
					
					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"
					
				BREAK
				
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_C_PED_A
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_c"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_c"		
					InteractionData.strAnimNameFullBody	 = "respawn_c_ped_a"
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_C_PED_B
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_c"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_c"		
					InteractionData.strAnimNameFullBody	 = "respawn_c_ped_b"
					InteractionData.bHasProp = TRUE		
					InteractionData.bDeletePropAtEnd = TRUE
					InteractionData.PropModel = Prop_NPC_Phone
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_C_PED_C
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_c"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_c"		
					InteractionData.strAnimNameFullBody	 = "respawn_c_ped_c"
				BREAK				
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_C_PED_D	
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_c"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_c"		
					InteractionData.strAnimNameFullBody	 = "respawn_c_ped_d"
					
					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"
					
				BREAK
				
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_D_PED_A
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_d"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_d"		
					InteractionData.strAnimNameFullBody	 = "respawn_d_ped_a"
					
					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"
					
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_D_PED_B
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_d"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_d"		
					InteractionData.strAnimNameFullBody	 = "respawn_d_ped_b"
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_D_PED_C
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_d"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_d"		
					InteractionData.strAnimNameFullBody	 = "respawn_d_ped_c"

					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"
					
					
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_D_PED_D
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_d"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_d"		
					InteractionData.strAnimNameFullBody	 = "respawn_d_ped_d"
					
					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"
					
				BREAK
				

				CASE SPECIAL_TEAM_RESPAWN_VARIATION_E_PED_A
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_e"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_e"		
					InteractionData.strAnimNameFullBody	 = "respawn_e_ped_a"
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_E_PED_B
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_e"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_e"		
					InteractionData.strAnimNameFullBody	 = "respawn_e_ped_b"
				BREAK				
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_E_PED_C
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_e"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_e"		
					InteractionData.strAnimNameFullBody	 = "respawn_e_ped_c"
					
					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"
					
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_E_PED_D			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_e"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_e"		
					InteractionData.strAnimNameFullBody	 = "respawn_e_ped_d"
					
					InteractionData.bHasProp = TRUE		
					InteractionData.bDeletePropAtEnd = TRUE
					InteractionData.PropModel = Prop_NPC_Phone
					
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_F_PED_A
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_f"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_f"		
					InteractionData.strAnimNameFullBody	 = "respawn_f_ped_a"
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_F_PED_B
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_f"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_f"		
					InteractionData.strAnimNameFullBody	 = "respawn_f_ped_b"
				BREAK				
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_F_PED_C
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_f"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_f"		
					InteractionData.strAnimNameFullBody	 = "respawn_f_ped_c"
					
					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"
					
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_F_PED_D			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_f"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_f"		
					InteractionData.strAnimNameFullBody	 = "respawn_f_ped_d"
					
					InteractionData.bHasProp = TRUE		
					InteractionData.bDeletePropAtEnd = TRUE
					InteractionData.PropModel = Prop_NPC_Phone
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_G_PED_A
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_g"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_g"		
					InteractionData.strAnimNameFullBody	 = "respawn_g_ped_a"
					
					InteractionData.bHasProp = TRUE
					InteractionData.PropModel = Prop_Ecola_Can
					
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_G_PED_B
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_g"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_g"		
					InteractionData.strAnimNameFullBody	 = "respawn_g_ped_b"
				BREAK				
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_G_PED_C
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_g"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_g"		
					InteractionData.strAnimNameFullBody	 = "respawn_g_ped_c"
					
					InteractionData.bHasProp = TRUE
					InteractionData.bUsesPTFX = TRUE
					InteractionData.PropModel = Prop_CS_Ciggy_01
					InteractionData.bPropHasAttachedPTFX = TRUE
					InteractionData.strPropPTFX = "ent_anim_cig_smoke"					
					InteractionData.vPropPTFXOffset = <<CiggyPFXAttachOffsetX,CiggyPFXAttachOffsetY,CiggyPFXAttachOffsetZ>>
					InteractionData.vPropPTFXRotation = <<CiggyPFXAttachRotationX, CiggyPFXAttachRotationY, CiggyPFXAttachRotationZ>>	
					InteractionData.bHasLoopMouthPTFX = TRUE
					InteractionData.strLoopPTFXMouth = "ent_anim_cig_exhale_mth"					
					InteractionData.bHasLoopNosePTFX = TRUE
					InteractionData.strLoopPTFXNose = "ent_anim_cig_exhale_nse"					
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_VARIATION_G_PED_D			
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@variations@variation_g"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@variations@variation_g"		
					InteractionData.strAnimNameFullBody	 = "respawn_g_ped_d"
				BREAK
			
				
				CASE SPECIAL_TEAM_RESPAWN_PACIFIC_FINALE_PED_A
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@pacific"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@pacific"		
					InteractionData.strAnimNameFullBody	 = "post_pacific_finale_respawn_ped_a"	
				BREAK
				CASE SPECIAL_TEAM_RESPAWN_PACIFIC_FINALE_PED_B
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@pacific"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@pacific"		
					InteractionData.strAnimNameFullBody	 = "post_pacific_finale_respawn_ped_b"
				BREAK
				CASE SPECIAL_TEAM_RESPAWN_PACIFIC_FINALE_PED_C
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@pacific"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@pacific"		
					InteractionData.strAnimNameFullBody	 = "post_pacific_finale_respawn_ped_c"
				BREAK
				CASE SPECIAL_TEAM_RESPAWN_PACIFIC_FINALE_PED_D
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@pacific"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@pacific"		
					InteractionData.strAnimNameFullBody	 = "post_pacific_finale_respawn_ped_d"
				BREAK
				
				CASE SPECIAL_TEAM_RESPAWN_FLEECA_FINALE_PED_A
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@fleeca"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@fleeca"		
					InteractionData.strAnimNameFullBody	 = "post_fleeca_finale_respawn_ped_a"	
				BREAK
				CASE SPECIAL_TEAM_RESPAWN_FLEECA_FINALE_PED_B
					InteractionData.strAnimDict[AD_FULL_BODY_M] = "anim@heists@team_respawn@fleeca"	
					InteractionData.strAnimDict[AD_FULL_BODY_F] = "anim@heists@team_respawn@fleeca"		
					InteractionData.strAnimNameFullBody	 = "post_fleeca_finale_respawn_ped_b"
				BREAK
				
				#IF FEATURE_FREEMODE_ARCADE
				CASE SPECIAL_MATCHSTARTS_UNARMED
				CASE SPECIAL_MATCHSTARTS_ONE_HANDED_WEAPON
				CASE SPECIAL_MATCHSTARTS_TWO_HANDED_WEAPON
				
				#IF FEATURE_COPS_N_CROOKS
				CASE SPECIAL_MATCHSTARTS_MACHINEGUN
				CASE SPECIAL_MATCHSTARTS_NIGHTSTICK
				CASE SPECIAL_MATCHSTARTS_SHOTGUN
				CASE SPECIAL_MATCHSTARTS_RIFLE
				CASE SPECIAL_MATCHSTARTS_PISTOL
				#ENDIF
				
					FILL_MATCHSTARTS_INTERACTION_DATA(INT_TO_ENUM(SPECIAL_INTERACTIONS, iSelectedAnim), InteractionData, IS_PED_FEMALE(PLAYER_PED_ID()))				
				BREAK
				#ENDIF
				
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	NET_PRINT("[interactions] FillInteractionData - called. Interaction Data:") NET_NL()
	PRINT_INTERACTION_DATA(InteractionData)
	#ENDIF
	
ENDPROC

FUNC BOOL IsInteractionNone(INT iInteractionType, INT iInteractionAnim)
	IF ((iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_IN_CAR)) AND (iInteractionAnim = ENUM_TO_INT(PASSENGER_INTERACTION_NONE)))
	OR ((iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER)) AND (iInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_NONE)))
	OR ((iInteractionType = -1) AND (iInteractionAnim = -1))
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsInteractionWithoutAnim(INT iInteractionAnim)
	IF (iInteractionAnim = ENUM_TO_INT(PLAYER_INTERACTION_PLAY_MASK_SFX))
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC STRING GetInteractionsAnimsDict(INTERACTION_DATA &InteractionData,  INT iInteractionType, BOOL bDriverSide=FALSE, BOOL bLowSeat=FALSE, BOOL bRearSeat=FALSE, BOOL bSideFacing = FALSE, BOOL bIsDriver=FALSE, BOOL bIsBohdi=FALSE, BOOL bIsSelfieMode=FALSE)
	SWITCH INT_TO_ENUM(INTERACTION_ANIM_TYPES, iInteractionType)
		CASE INTERACTION_ANIM_TYPE_IN_CAR				
			IF (bLowSeat)
				IF (bIsDriver)
					RETURN TextLabelToString(InteractionData.strAnimDict[AD_LOW_DS])
				ELSE
					RETURN TextLabelToString(InteractionData.strAnimDict[AD_LOW_PS])
				ENDIF
			ELSE
				IF NOT (bIsBohdi)
					IF (bIsDriver)
						RETURN TextLabelToString(InteractionData.strAnimDict[AD_HIGH_DS])
					ELSE
						IF (bDriverSide)
						OR (bSideFacing)
							RETURN TextLabelToString(InteractionData.strAnimDict[AD_HIGH_DS_REAR])
						ELSE
							IF (bRearSeat)
								RETURN TextLabelToString(InteractionData.strAnimDict[AD_HIGH_PS_REAR])
							ELSE
								RETURN TextLabelToString(InteractionData.strAnimDict[AD_HIGH_PS])
							ENDIF
						ENDIF
					ENDIF
				ELSE
				
					IF (bIsDriver)
						RETURN TextLabelToString(InteractionData.strAnimDict[AD_BOHDI_DS])
					ELSE
						IF (bDriverSide)
						OR (bSideFacing)
							RETURN TextLabelToString(InteractionData.strAnimDict[AD_BOHDI_DS_REAR])
						ELSE
							IF (bRearSeat)
								RETURN TextLabelToString(InteractionData.strAnimDict[AD_BOHDI_PS_REAR])
							ELSE
								RETURN TextLabelToString(InteractionData.strAnimDict[AD_BOHDI_PS])
							ENDIF
						ENDIF
					ENDIF				
								
				ENDIF
			ENDIF		
		BREAK
	ENDSWITCH
	
	// is this a selfie?
	IF (bIsSelfieMode)
		RETURN TextLabelToString(InteractionData.strAnimDict[AD_SELFIE])	
	ENDIF
	
	RETURN TextLabelToString(InteractionData.strAnimDict[AD_DEFAULT])	
ENDFUNC




#IF IS_DEBUG_BUILD

PROC ADD_WIDGET_FOR_INTERACTION_SOUND_DATA(INTERACTION_SOUND_DATA &SoundData)
	START_WIDGET_GROUP("sound data")
//		ADD_WIDGET_BOOL("bPlayIntroSound", SoundData.bPlayIntroSound)
//		ADD_WIDGET_FLOAT_SLIDER("fIntroSoundTime", SoundData.fIntroSoundTime, 0.0, 1.0, 0.001)
//		
//		ADD_WIDGET_BOOL("bPlayLoopSound", SoundData.bPlayLoopSound)
//		ADD_WIDGET_FLOAT_SLIDER("fLoopSoundTime", SoundData.fLoopSoundTime, 0.0, 1.0, 0.001)	
//	
//		ADD_WIDGET_BOOL("bPlayLoopSound2", SoundData.bPlayLoopSound2)
//		ADD_WIDGET_FLOAT_SLIDER("fLoopSoundTime2", SoundData.fLoopSoundTime2, 0.0, 1.0, 0.001)
//		
//		ADD_WIDGET_BOOL("bPlayOutroSound", SoundData.bPlayOutroSound)
//		ADD_WIDGET_FLOAT_SLIDER("fOutroSoundTime", SoundData.fOutroSoundTime, 0.0, 1.0, 0.001)
		
		//TEXT_LABEL_63 strIntroSound
		//TEXT_LABEL_63 strLoopSound
		//TEXT_LABEL_63 strOutroSound
		
		INT i
		REPEAT MAX_NUMBER_OF_SOUNDS i
		
			TEXT_LABEL_63 str
			
			str = "bPlaySound["
			str += i
			str += "]"		
			ADD_WIDGET_BOOL(str, SoundData.bPlaySound[i])
			
			str = "bHasPlayedSound["
			str += i
			str += "]"		
			ADD_WIDGET_BOOL(str, SoundData.bHasPlayedSound[i])			
		
			str = "fSoundTime["
			str += i
			str += "]"
			ADD_WIDGET_FLOAT_SLIDER(str, SoundData.fSoundTime[i], 0.0, 1.0, 0.001)
			
//			str = "SoundAnimStage["
//			str += i
//			str += "]"
//			ADD_WIDGET_INT_SLIDER(str, SoundData.SoundAnimStage[i], 0, 99, 1)
			
			//TEXT_LABEL_63 strSound[MAX_NUMBER_OF_SOUNDS]
			
		ENDREPEAT
		
		ADD_WIDGET_FLOAT_SLIDER("fLastPhase", SoundData.fLastPhase, -1.0, 1.0, 0.001)
	
	STOP_WIDGET_GROUP()
ENDPROC

PROC ADD_WIDGET_FOR_GLOBAL_STORED_INTERACTION_DATA(STRING str, INTERACTION_GLOBAL_DATA &Data)
	START_WIDGET_GROUP(str)
		ADD_WIDGET_INT_SLIDER("iInteractionType", Data.iInteractionType, -1, ENUM_TO_INT(MAX_NUM_PLAYER_INTERACTION_TYPES)-1, 1)
		ADD_WIDGET_INT_SLIDER("iInteractionAnim", Data.iInteractionAnim, -1, GET_MAX_NUMBER_OF_INTERACTIONS_IN_TYPE()-1, 1)		
		ADD_WIDGET_BOOL("bPlayInteractionAnim", Data.bPlayInteractionAnim)	
		ADD_WIDGET_BOOL("bPlayInteractionAnimFullBody", Data.bPlayInteractionAnimFullBody)	
		ADD_WIDGET_BOOL("bPlayInteractionSelfie", Data.bPlayInteractionSelfie)	
		ADD_WIDGET_BOOL("bCleanupInteractionAnim", Data.bCleanupInteractionAnim)
		ADD_WIDGET_BOOL("bCleanupInteractionAnimImmediately", Data.bCleanupInteractionAnimImmediately)
		ADD_WIDGET_BOOL("bInteractionIsPlaying", Data.bInteractionIsPlaying)
		ADD_WIDGET_BOOL("bHoldLoop", Data.bHoldLoop)	
		ADD_WIDGET_BOOL("bHasCleanedUp", Data.bHasCleanedUp)
		ADD_WIDGET_BOOL("bStartedIntroTime", Data.bStartedIntroTime)
		ADD_WIDGET_BOOL("bIsWearingHelmentInVehicle", Data.bIsWearingHelmentInVehicle)
		ADD_WIDGET_BOOL("bWaitForPlayerControl", Data.bWaitForPlayerControl)
		ADD_WIDGET_BOOL("bWaitForSync", Data.bWaitForSync)
		ADD_WIDGET_BOOL("bWaitForFadingIn", Data.bWaitForFadingIn)
		ADD_WIDGET_BOOL("bForceWeaponVisible", Data.bForceWeaponVisible)
		ADD_WIDGET_FLOAT_SLIDER("fStartPhase", Data.fStartPhase, 0.0, 1.0, 0.01)
		ADD_WIDGET_INT_SLIDER("iAlcoholShots", Data.iAlcoholShots, LOWEST_INT, HIGHEST_INT, 1)	
		ADD_WIDGET_INT_SLIDER("iHealthChange", Data.iHealthChange, LOWEST_INT, HIGHEST_INT, 1)		
		ADD_WIDGET_INT_SLIDER("iCurrentAlcoholShots", Data.iCurrentAlcoholShots, LOWEST_INT, HIGHEST_INT, 1)
	STOP_WIDGET_GROUP()	
ENDPROC

PROC ADD_WIDGET_FOR_INTERACTION_ANIM_DATA(STRING str, INTERACTION_ANIM_DATA &AnimData)
	START_WIDGET_GROUP(str)
		ADD_WIDGET_INT_SLIDER("iType", AnimData.iType, -1, ENUM_TO_INT(MAX_NUM_PLAYER_INTERACTION_TYPES)-1, 1)
		ADD_WIDGET_INT_SLIDER("iAnim", AnimData.iAnim, -1, GET_MAX_NUMBER_OF_INTERACTIONS_IN_TYPE()-1, 1)

		ADD_WIDGET_BOOL("bIsDriverSide", AnimData.bIsDriverSide)
		ADD_WIDGET_BOOL("bIsLowVehicle", AnimData.bIsLowVehicle)	
		ADD_WIDGET_BOOL("bIsRearSeat", AnimData.bIsRearSeat)
		ADD_WIDGET_BOOL("bIsSideFacing", AnimData.bIsSideFacing)
		ADD_WIDGET_BOOL("bIsDriver", AnimData.bIsDriver)
		ADD_WIDGET_BOOL("bIsBohdi", AnimData.bIsBohdi)		
		ADD_WIDGET_BOOL("bIsMale", AnimData.bIsMale)	
		ADD_WIDGET_BOOL("bCreateProp", AnimData.bCreateProp)
		ADD_WIDGET_BOOL("bDeleteProp", AnimData.bDeleteProp)
		ADD_WIDGET_BOOL("bJustFinishedIntro", AnimData.bJustFinishedIntro)
		ADD_WIDGET_BOOL("bObjectCreated", AnimData.bObjectCreated)
		ADD_WIDGET_BOOL("bHitMaxIterations", AnimData.bHitMaxIterations)
		ADD_WIDGET_BOOL("bHasDoneMinIterations", AnimData.bHasDoneMinIterations)
		ADD_WIDGET_BOOL("bPlayingNoFilterWhenStanding", AnimData.bPlayingNoFilterWhenStanding)
		ADD_WIDGET_BOOL("bSlowBlendBodyOutro", AnimData.bSlowBlendBodyOutro)
		ADD_WIDGET_BOOL("bSelfieMode", AnimData.bSelfieMode)
		ADD_WIDGET_INT_SLIDER("iIteration", AnimData.iIteration, -1, 99, 1)	
		
		ADD_WIDGET_FLOAT_SLIDER("fLastBlendOutAnimPhase", AnimData.fLastBlendOutAnimPhase, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fStartPhase", AnimData.fStartPhase, -1.0, 1.0, 0.001)
		ADD_WIDGET_BOOL("bIsPaused", AnimData.bIsPaused)
		ADD_WIDGET_BOOL("bStarted", AnimData.bStarted)
		
		ADD_WIDGET_FOR_INTERACTION_SOUND_DATA(AnimData.SoundData)
		
	STOP_WIDGET_GROUP()
ENDPROC

PROC ADD_WIDGET_FOR_STORED_INTERACTION_DATA(STRING str, INTERACTION_INTERNAL_DATA &InernalData)
	START_WIDGET_GROUP(str)
		
		ADD_WIDGET_FOR_INTERACTION_ANIM_DATA("SelectedInteraction", InernalData.SelectedInteraction)
		ADD_WIDGET_FOR_INTERACTION_ANIM_DATA("PlayingInteraction", InernalData.PlayingInteraction)
		ADD_WIDGET_FOR_INTERACTION_ANIM_DATA("LastInteraction", InernalData.LastInteraction)
				
		ADD_WIDGET_BOOL("bHasUnloadedLastSelectedAnim", InernalData.bHasUnloadedLastSelectedAnim)
		ADD_WIDGET_BOOL("bPlayingSelectedAnim", InernalData.bPlayingSelectedAnim)
		ADD_WIDGET_BOOL("bHasGrabbedCarInfo", InernalData.bHasGrabbedCarInfo)
	STOP_WIDGET_GROUP()
ENDPROC

PROC setup_START_INTERACTION_ANIM_Widgets()
	ADD_WIDGET_BOOL("bHoldLoop", MPGlobalsInteractions.bDebugHoldLoop)
	ADD_WIDGET_BOOL("bDoSnackCheck", MPGlobalsInteractions.bDebugDoSnackCheck)
	ADD_WIDGET_BOOL("bFullBody", MPGlobalsInteractions.bDebugFullBody)
	ADD_WIDGET_BOOL("bWaitForPlayerControl", MPGlobalsInteractions.bDebugWaitUntilPlayerControl)
	ADD_WIDGET_BOOL("bWaitUntilSync", MPGlobalsInteractions.bDebugWaitUntilSync)
	ADD_WIDGET_FLOAT_SLIDER("fStartPhase", MPGlobalsInteractions.fDebugStartPhase, 0.0, 1.0, 0.01)
	ADD_WIDGET_BOOL("bSkipInteractionAnimIntroDelay", MPGlobalsInteractions.bDebugSkipInteractionAnimIntroDelay)
	ADD_WIDGET_BOOL("bWaitForFadingIn", MPGlobalsInteractions.bDebugWaitForFadingIn)
	ADD_WIDGET_BOOL("bForceWeaponVisible", MPGlobalsInteractions.bDebugForceWeaponVisible)
	ADD_WIDGET_BOOL("bIgnoreStatCheck", MPGlobalsInteractions.bDebugIgnoreStatCheck)
ENDPROC

PROC CREATE_INTERACTION_ANIM_WIDGET()	
	
	TEXT_LABEL_63 str				 
	INT i
	
	START_WIDGET_GROUP("Interaction Anims")
	
		//ADD_WIDGET_FLOAT_SLIDER("QUICK_BLEND_VALUE", QUICK_BLEND_VALUE, 0.0, 1.0, 0.01)
		
		START_WIDGET_GROUP("STOP_INTERACTION_ANIM")
			ADD_WIDGET_BOOL("bCleanupImmediately", MPGlobalsInteractions.bDebugCallCommandCleanupImmediately)
			ADD_WIDGET_BOOL("CALL COMMAND", MPGlobalsInteractions.bDebugCallCommand_STOP_INTERACTION_ANIM)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("START_INTERACTION_ANIM")
			START_WIDGET_GROUP("IN_CAR")
				START_NEW_WIDGET_COMBO()
					REPEAT ENUM_TO_INT(MAX_PASSENGER_INTERACTIONS) i
						ADD_TO_WIDGET_COMBO(INTERACTION_ANIMS_DEBUG_NAME(ENUM_TO_INT(INTERACTION_ANIM_TYPE_IN_CAR), i))
					ENDREPEAT
				STOP_WIDGET_COMBO("anim", MPGlobalsInteractions.iDebugStartAnimInCar)
				setup_START_INTERACTION_ANIM_Widgets()
				ADD_WIDGET_BOOL("CALL COMMAND", MPGlobalsInteractions.bDebugCallCommmandInCar)			
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("CREW")
				START_NEW_WIDGET_COMBO()
					REPEAT ENUM_TO_INT(MAX_CREW_INTERACTIONS) i
						ADD_TO_WIDGET_COMBO(INTERACTION_ANIMS_DEBUG_NAME(ENUM_TO_INT(INTERACTION_ANIM_TYPE_CREW), i))
					ENDREPEAT
				STOP_WIDGET_COMBO("anim", MPGlobalsInteractions.iDebugStartAnimCrew)
				setup_START_INTERACTION_ANIM_Widgets()
				ADD_WIDGET_BOOL("CALL COMMAND", MPGlobalsInteractions.bDebugCallCommmandCrew)					
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("PLAYER")
				START_NEW_WIDGET_COMBO()
					REPEAT ENUM_TO_INT(MAX_PLAYER_INTERACTIONS) i
						ADD_TO_WIDGET_COMBO(INTERACTION_ANIMS_DEBUG_NAME(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), i))
					ENDREPEAT
				STOP_WIDGET_COMBO("anim", MPGlobalsInteractions.iDebugStartAnimPlayer)
				setup_START_INTERACTION_ANIM_Widgets()
				ADD_WIDGET_BOOL("CALL COMMAND", MPGlobalsInteractions.bDebugCallCommmandPlayer)					
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("SPECIAL")
				START_NEW_WIDGET_COMBO()
					REPEAT ENUM_TO_INT(MAX_SPECIAL_INTERACTIONS) i
						str = INTERACTION_ANIMS_DEBUG_NAME(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), i)
						PRINTLN("adding to special interactions widget ", str)
						ADD_TO_WIDGET_COMBO(INTERACTION_ANIMS_DEBUG_NAME(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), i))						
					ENDREPEAT
				STOP_WIDGET_COMBO("anim", MPGlobalsInteractions.iDebugStartAnimSpecial)
				setup_START_INTERACTION_ANIM_Widgets()
				ADD_WIDGET_BOOL("CALL COMMAND", MPGlobalsInteractions.bDebugCallCommmandSpecial)					
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("SET_NEXT_RESPAWN_INTERACTION")
			ADD_WIDGET_INT_SLIDER("iRespawnInteractionType", g_SpawnData.iRespawnInteractionType, -1, ENUM_TO_INT(MAX_NUM_PLAYER_INTERACTION_TYPES)-1, 1)
			ADD_WIDGET_INT_SLIDER("iRespawnSelectedAnim", g_SpawnData.iRespawnSelectedAnim, -1, GET_MAX_NUMBER_OF_INTERACTIONS_IN_TYPE()-1, 1)	
			ADD_WIDGET_BOOL("bRespawnInteractionFullBody", g_SpawnData.bRespawnInteractionFullBody)	
			ADD_WIDGET_FLOAT_SLIDER("fRespawnInteractionStartPhase", g_SpawnData.fRespawnInteractionStartPhase, 0.0, 1.0, 0.01)	
			ADD_WIDGET_BOOL("bRespawnInteractionForceWeaponVisible", g_SpawnData.bRespawnInteractionForceWeaponVisible)
			ADD_WIDGET_BOOL("CALL COMMAND", MPGlobalsInteractions.bDebugCallCommand_SET_NEXT_RESPAWN_INTERACTION)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_INT_SLIDER("INTRO_DELAY_TIMER", INTRO_DELAY_TIMER, 0, 2000, 1)
	
		START_WIDGET_GROUP("Player")
			ADD_WIDGET_FOR_GLOBAL_STORED_INTERACTION_DATA("MPGlobalsInteractions.PlayerInteraction", MPGlobalsInteractions.PlayerInteraction)
			ADD_WIDGET_FOR_STORED_INTERACTION_DATA("gInteractionsPlayerData", gInteractionsPlayerData)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Quickplay")
			ADD_WIDGET_FOR_GLOBAL_STORED_INTERACTION_DATA("MPGlobalsInteractions.QuickplayInteraction", MPGlobalsInteractions.QuickplayInteraction)
			ADD_WIDGET_FOR_STORED_INTERACTION_DATA("gInteractionsQuickplayData", gInteractionsQuickplayData)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("QuickplayVeh")
			ADD_WIDGET_FOR_GLOBAL_STORED_INTERACTION_DATA("MPGlobalsInteractions.QuickplayInteractionVeh", MPGlobalsInteractions.QuickplayInteractionVeh)
			ADD_WIDGET_FOR_STORED_INTERACTION_DATA("gInteractionsQuickplayDataVeh", gInteractionsQuickplayDataVeh)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Pointing")
			ADD_WIDGET_FLOAT_SLIDER("MAX_POINTING_DIFF_FROM_CAM", MAX_POINTING_DIFF_FROM_CAM, -180.0, 180.0, 1.0)
			ADD_WIDGET_BOOL("MPGlobalsInteractions.bPointingButtonPressed", MPGlobalsInteractions.bPointingButtonPressed)
			ADD_WIDGET_BOOL("MPGlobalsInteractions.bPointingDoubleTap", MPGlobalsInteractions.bPointingDoubleTap)	
			ADD_WIDGET_FLOAT_SLIDER("POINTING_PITCH", POINTING_PITCH, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("POINTING_HEADING", POINTING_HEADING, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("POINTING_INTERP_VALUE", POINTING_INTERP_VALUE, 0.0, 1000.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("POINTING_OFFSET_X", POINTING_OFFSET_X, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("POINTING_OFFSET_MIN_Y", POINTING_OFFSET_MIN_Y, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("POINTING_OFFSET_MAX_Y", POINTING_OFFSET_MAX_Y, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("POINTING_OFFSET_Z", POINTING_OFFSET_Z, -2.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("POINTING_CAPSULE_RADIUS", POINTING_CAPSULE_RADIUS, 0.0, 2.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("POINTING_CAPSULE_HEIGHT", POINTING_CAPSULE_HEIGHT, 0.0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Peds")
			REPEAT NUM_NETWORK_PLAYERS i
				str = "MPGlobalsInteractions.PedInteraction["
				str += i
				str += "]"
				ADD_WIDGET_FOR_GLOBAL_STORED_INTERACTION_DATA(str, MPGlobalsInteractions.PedInteraction[i])
				str = "MPGlobalsInteractions.gInteractionsPedsData["
				str += i
				str += "]"
				ADD_WIDGET_FOR_STORED_INTERACTION_DATA(str, gInteractionsPedsData[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
	
		INT j
		START_WIDGET_GROUP("Synced Interactions")
			
			START_WIDGET_GROUP("TEST")
				ADD_WIDGET_BOOL("Start sync scene with available players in session.", MPGlobalsInteractions.bStartSyncedInteractionWithAllPlayersInSession)
				ADD_WIDGET_BOOL("Start sync scene locally only.", MPGlobalsInteractions.bStartSyncedInteractionLocallyOnly)
				ADD_WIDGET_BOOL("Stay paused at start cam", MPGlobalsInteractions.bSyncInteractionSnapToFirstCam)
				ADD_WIDGET_BOOL("Snap to end cam", MPGlobalsInteractions.bSyncInteractionSnapToEndCam)
				
				START_NEW_WIDGET_COMBO()			
				ADD_TO_WIDGET_COMBO("USING 'SceneID Continued' selection")
					REPEAT MAX_SYNCED_INTERACTION_COMBO_BOX_ENTRIES i
						ADD_TO_WIDGET_COMBO(gSyncedInteractionScene[i].strDebugName)
					ENDREPEAT					
				STOP_WIDGET_COMBO("SceneID", MPGlobalsInteractions.iDebug_SyncedSceneSceneID)	
				
				START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("USING 'SceneID' seelction")
					FOR i = (MAX_SYNCED_INTERACTION_COMBO_BOX_ENTRIES) TO (TOTAL_SYNCED_INTERACTION_SCENES-1)
						ADD_TO_WIDGET_COMBO(gSyncedInteractionScene[i].strDebugName)
					ENDFOR
				STOP_WIDGET_COMBO("SceneID Continued", MPGlobalsInteractions.iDebug_SyncedSceneSceneID2)	
				ADD_WIDGET_INT_SLIDER("Force Location", MPGlobalsInteractions.iDebug_SyncedSceneLocationID, -1, NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS-1, 1)
				ADD_WIDGET_INT_SLIDER("Force Actor Number", MPGlobalsInteractions.iDebug_SyncedSceneActor, -1, 4, 1)
				ADD_WIDGET_BOOL("Fill scene with dummy peds", MPGlobalsInteractions.bDebug_FillSceneWithPeds)
				ADD_WIDGET_BOOL("Pin Interior", MPGlobalsInteractions.bPinInterior)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("DEBUG")				
				ADD_WIDGET_BOOL("MPGlobalsInteractions.bUseClonePlayers", MPGlobalsInteractions.bUseClonePlayers)
				ADD_WIDGET_BOOL("MPGlobalsInteractions.bOutputSyncedInteractionData", MPGlobalsInteractions.bOutputSyncedInteractionData)	
				ADD_WIDGET_BOOL("MPGlobalsInteractions.bDebug_SyncedSceneFailToGetLocation", MPGlobalsInteractions.bDebug_SyncedSceneFailToGetLocation)
				ADD_WIDGET_INT_SLIDER("MIN_WAIT_TIME_FOR_ANIM_TO_START", MIN_WAIT_TIME_FOR_ANIM_TO_START, -1, HIGHEST_INT, 1)
				//ADD_WIDGET_INT_SLIDER("iDebug_SyncedSceneUniqueID", MPGlobalsInteractions.iDebug_SyncedSceneUniqueID, -1, HIGHEST_INT, 1)	
				REPEAT 3 i
					str = "iDebugPedActor["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, MPGlobalsInteractions.iDebugPedActor[i], -1, HIGHEST_INT, 1)		
				ENDREPEAT
				
				START_WIDGET_GROUP("Pre Cutscene")
					ADD_WIDGET_INT_SLIDER("MPGlobalsInteractions.iPreCutsceneID", MPGlobalsInteractions.iPreCutsceneID, -1, 99, 1)
					ADD_WIDGET_INT_SLIDER("MPGlobalsInteractions.iPreCutsceneState", MPGlobalsInteractions.iPreCutsceneState, -1, 99, 1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Test Bails")
					ADD_WIDGET_BOOL("bTestBail1", MPGlobalsInteractions.bTestBail1)
					ADD_WIDGET_BOOL("bTestBail2", MPGlobalsInteractions.bTestBail2)
					ADD_WIDGET_BOOL("bTestBail3", MPGlobalsInteractions.bTestBail3)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Player Data")
				ADD_WIDGET_BOOL("MPGlobalsInteractions.bHasBeenSynced", MPGlobalsInteractions.bHasBeenSynced)
				ADD_WIDGET_INT_SLIDER("MPGlobalsInteractions.iSyncedInteractionState", MPGlobalsInteractions.iSyncedInteractionState, -1, HIGHEST_INT, 1)
				ADD_WIDGET_VECTOR_SLIDER("MPGlobalsInteractions.vSyncedInteractionCoords", MPGlobalsInteractions.vSyncedInteractionCoords, -99999.9, 99999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("MPGlobalsInteractions.fSyncedInteractionHeading", MPGlobalsInteractions.fSyncedInteractionHeading, -99999.9, 99999.9, 0.01)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Player Broadcast Data")
				REPEAT NUM_NETWORK_PLAYERS i
					str = "player["
					str += i
					str += "].iSceneID"
					ADD_WIDGET_INT_SLIDER(str, GlobalplayerBD_Interactions[i].iSceneID, -1, TOTAL_SYNCED_INTERACTION_SCENES-1, 1)
					str = "player["
					str += i
					str += "].iUniqueID"
					ADD_WIDGET_INT_SLIDER(str, GlobalplayerBD_Interactions[i].iUniqueID, LOWEST_INT, HIGHEST_INT, 1)
					str = "player["
					str += i
					str += "].bReadyToStart"
					ADD_WIDGET_BOOL(str, GlobalplayerBD_Interactions[i].bReadyToStart)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Server Broadcast Data")
				REPEAT NUM_NETWORK_PLAYERS i
					str = "iSceneID["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_SyncedInteractions.iSceneID[i], -1, TOTAL_SYNCED_INTERACTION_SCENES-1, 1)
					str = "iUniqueID["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_SyncedInteractions.iUniqueID[i], LOWEST_INT, HIGHEST_INT, 1)
					str = "iLocation["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_SyncedInteractions.iLocation[i], -2, HIGHEST_INT, 1)
					str = "iActor["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_SyncedInteractions.iActor[i], -1, HIGHEST_INT, 1)
				ENDREPEAT				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("General Camera tweaks")
				
				ADD_WIDGET_BOOL("TURN ON CAMERA EDITING", MPGlobalsInteractions.bDebug_TurnOnCameraEditing)
				
				START_WIDGET_GROUP("1st camera (wide shot)")

					START_WIDGET_GROUP("Camera Moving Function")
						ADD_WIDGET_FLOAT_SLIDER("SYNC_INTERACTION_PAN_DIST", SYNC_INTERACTION_PAN_DIST, 0.0, 50.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SYNC_INTERACTION_PAN_ANGLE", SYNC_INTERACTION_PAN_ANGLE, -360.0, 360.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SYNC_INTERACTION_PAN_CAM_ANGLE", SYNC_INTERACTION_PAN_CAM_ANGLE, -360.0, 360.0, 0.01)
						ADD_WIDGET_BOOL("Apply to scene data", MPGlobalsInteractions.bUpdateSyncedCamPans)
					STOP_WIDGET_GROUP()
				
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("2nd camera (behind shoulder)")
				
					ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_FOV", SHOULDER_CAM_FOV, 0.0, 100.0, 0.01)
					ADD_WIDGET_INT_SLIDER("SHOULDER_CAM_DURATION", SHOULDER_CAM_DURATION, 0, HIGHEST_INT, 1)				
				
					START_WIDGET_GROUP("Default Shoulder Camera")
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_START_OFFSET_X", SHOULDER_CAM_START_OFFSET_X, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_START_OFFSET_Y", SHOULDER_CAM_START_OFFSET_Y, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_START_OFFSET_Z", SHOULDER_CAM_START_OFFSET_Z, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_START_ROT_OFFSET_X", SHOULDER_CAM_START_ROT_OFFSET_X, -360.0, 360.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_START_ROT_OFFSET_Y", SHOULDER_CAM_START_ROT_OFFSET_Y, -360.0, 360.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_START_ROT_OFFSET_Z", SHOULDER_CAM_START_ROT_OFFSET_Z, -360.0, 360.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_END_OFFSET_X", SHOULDER_CAM_END_OFFSET_X, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_END_OFFSET_Y", SHOULDER_CAM_END_OFFSET_Y, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_END_OFFSET_Z", SHOULDER_CAM_END_OFFSET_Z, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_END_ROT_OFFSET_X", SHOULDER_CAM_END_ROT_OFFSET_X, -360.0, 360.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_END_ROT_OFFSET_Y", SHOULDER_CAM_END_ROT_OFFSET_Y, -360.0, 360.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("SHOULDER_CAM_END_ROT_OFFSET_Z", SHOULDER_CAM_END_ROT_OFFSET_Z, -360.0, 360.0, 0.01)
					STOP_WIDGET_GROUP()

					START_WIDGET_GROUP("Near Shoulder Camera")
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_START_OFFSET_X", NEAR_SHOULDER_CAM_START_OFFSET_X, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_START_OFFSET_Y", NEAR_SHOULDER_CAM_START_OFFSET_Y, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_START_OFFSET_Z", NEAR_SHOULDER_CAM_START_OFFSET_Z, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_START_ROT_OFFSET_X", NEAR_SHOULDER_CAM_START_ROT_OFFSET_X, -360.0, 360.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_START_ROT_OFFSET_Y", NEAR_SHOULDER_CAM_START_ROT_OFFSET_Y, -360.0, 360.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_START_ROT_OFFSET_Z", NEAR_SHOULDER_CAM_START_ROT_OFFSET_Z, -360.0, 360.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_END_OFFSET_X", NEAR_SHOULDER_CAM_END_OFFSET_X, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_END_OFFSET_Y", NEAR_SHOULDER_CAM_END_OFFSET_Y, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_END_OFFSET_Z", NEAR_SHOULDER_CAM_END_OFFSET_Z, -10.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_END_ROT_OFFSET_X", NEAR_SHOULDER_CAM_END_ROT_OFFSET_X, -360.0, 360.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_END_ROT_OFFSET_Y", NEAR_SHOULDER_CAM_END_ROT_OFFSET_Y, -360.0, 360.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("NEAR_SHOULDER_CAM_END_ROT_OFFSET_Z", NEAR_SHOULDER_CAM_END_ROT_OFFSET_Z, -360.0, 360.0, 0.01)
					STOP_WIDGET_GROUP()
				
				STOP_WIDGET_GROUP()
					

				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Scene Data")
				REPEAT TOTAL_SYNCED_INTERACTION_SCENES i
					START_WIDGET_GROUP(gSyncedInteractionScene[i].strDebugName)					
						REPEAT NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS j							
							str = "Location "
							str += j
							START_WIDGET_GROUP(str)
								ADD_WIDGET_VECTOR_SLIDER("vPosition", gSyncedInteractionScene[i].vPosition[j], -99999.9, 99999.9, 0.01)
								ADD_WIDGET_FLOAT_SLIDER("fHeading", gSyncedInteractionScene[i].fHeading[j], -99999.9, 99999.9, 0.01)
								ADD_WIDGET_INT_SLIDER("iLayout", gSyncedInteractionScene[i].iLayout[j], -1, 99, 1)														
								ADD_WIDGET_VECTOR_SLIDER("vCamStartPos", gSyncedInteractionScene[i].vCamStartPos[j], -99999.9, 99999.9, 0.01)
								ADD_WIDGET_VECTOR_SLIDER("vCamStartRot", gSyncedInteractionScene[i].vCamStartRot[j], -99999.9, 99999.9, 0.01)																
								ADD_WIDGET_FLOAT_SLIDER("fCamStartFOV", gSyncedInteractionScene[i].fCamStartFOV[j], 0.0, 99.0, 0.01)
								ADD_WIDGET_VECTOR_SLIDER("vCamEndPos", gSyncedInteractionScene[i].vCamEndPos[j], -99999.9, 99999.9, 0.01)
								ADD_WIDGET_VECTOR_SLIDER("vCamEndRot", gSyncedInteractionScene[i].vCamEndRot[j], -99999.9, 99999.9, 0.01)
								ADD_WIDGET_FLOAT_SLIDER("fCamEndFOV", gSyncedInteractionScene[i].fCamEndFOV[j], 0.0, 99.0, 0.01)
								//ADD_WIDGET_INT_SLIDER("iCamDuration", gSyncedInteractionScene[i].iCamDuration[j], 0, HIGHEST_INT, 1)		
								ADD_WIDGET_INT_SLIDER("iCamEndTime", gSyncedInteractionScene[i].iCamEndTime[j], 0, HIGHEST_INT, 1)	
								ADD_WIDGET_FLOAT_SLIDER("fShakeAmplitude", gSyncedInteractionScene[i].fShakeAmplitude[j], 0.0, 1.0, 0.01)
									
								ADD_WIDGET_INT_SLIDER("iCamPanState", gSyncedInteractionScene[i].iCamPanState[j], 0, 2, 1)								
								ADD_WIDGET_BOOL("Grab position/heading from player", gSyncedInteractionScene[i].bGrabPosition[j])
								ADD_WIDGET_BOOL("Grab start cam from screen", gSyncedInteractionScene[i].bGrabStartCam[j])
								ADD_WIDGET_BOOL("Grab end cam from screen", gSyncedInteractionScene[i].bGrabEndCam[j])
								ADD_WIDGET_BOOL("OUTPUT DETAILS TO FILE", gSyncedInteractionScene[i].bOutputDetailsToFile[j])
							STOP_WIDGET_GROUP()																			
						ENDREPEAT
					STOP_WIDGET_GROUP()			
				ENDREPEAT
			STOP_WIDGET_GROUP()
		
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("global data")
			
			ADD_WIDGET_INT_SLIDER("iMyCrewInteractionAnim", MPGlobalsInteractions.iMyCrewInteractionAnim, -1, 99, 1)
			
			ADD_WIDGET_BOOL("MPGlobalsInteractions.bQuickplayButtonPressed", MPGlobalsInteractions.bQuickplayButtonPressed)
			ADD_WIDGET_BOOL("MPGlobalsInteractions.bQuickplayDoubleTap", MPGlobalsInteractions.bQuickplayDoubleTap)
			ADD_WIDGET_INT_SLIDER("g_iCurrentOnFootQuickplayType", g_iCurrentOnFootQuickplayType, -1, 99, 1)
			ADD_WIDGET_INT_SLIDER("g_iCurrentOnFootQuickplayAnim", g_iCurrentOnFootQuickplayAnim, -1, 99, 1)
			ADD_WIDGET_INT_SLIDER("g_iCurrentInVehQuickplayAnim", g_iCurrentInVehQuickplayAnim, -1, 99, 1)			
//			ADD_WIDGET_BOOL("MPGlobalsInteractions.bUpdateQuickplayAnim", MPGlobalsInteractions.bUpdateQuickplayAnim)
//			ADD_WIDGET_BOOL("MPGlobalsInteractions.bUpdateQuickplayAnimVeh", MPGlobalsInteractions.bUpdateQuickplayAnimVeh)		
		
	
			
			ADD_WIDGET_BOOL("MPGlobalsInteractions.bAnyInteractionIsPlaying", MPGlobalsInteractions.bAnyInteractionIsPlaying)
			ADD_WIDGET_INT_SLIDER("MPGlobalsInteractions.iCurrentlyPlayingType", MPGlobalsInteractions.iCurrentlyPlayingType, -1, 99, 1)
			ADD_WIDGET_INT_SLIDER("MPGlobalsInteractions.iCurrentlyPlayingAnim", MPGlobalsInteractions.iCurrentlyPlayingAnim, -1, 99, 1)

			
			ADD_WIDGET_BOOL("bUseDebugFilterAndFlags", MPGlobalsInteractions.bUseDebugFilterAndFlags)
			ADD_WIDGET_INT_SLIDER("iDebugPlayAnimFilter", MPGlobalsInteractions.iDebugPlayAnimFilter, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iDebugPlayAnimFlags", MPGlobalsInteractions.iDebugPlayAnimFlags, -1, HIGHEST_INT, 1)
			
			ADD_WIDGET_BOOL("bGiveCiggy", MPGlobalsInteractions.bGiveCiggy)
			ADD_WIDGET_BOOL("bRemoveCiggys", MPGlobalsInteractions.bRemoveCiggys)
			
			ADD_WIDGET_BOOL("bGiveDrink1", MPGlobalsInteractions.bGiveDrink1)
			ADD_WIDGET_BOOL("bRemoveDrink1", MPGlobalsInteractions.bRemoveDrink1)
			
			ADD_WIDGET_BOOL("bGiveDrink2", MPGlobalsInteractions.bGiveDrink2)
			ADD_WIDGET_BOOL("bRemoveDrink2", MPGlobalsInteractions.bRemoveDrink2)	
			
			ADD_WIDGET_BOOL("bGiveDrink3", MPGlobalsInteractions.bGiveDrink3)
			ADD_WIDGET_BOOL("bRemoveDrink3", MPGlobalsInteractions.bRemoveDrink3)
			
			ADD_WIDGET_BOOL("bGiveFood1", MPGlobalsInteractions.bGiveFood1)
			ADD_WIDGET_BOOL("bRemoveFood1", MPGlobalsInteractions.bRemoveFood1)
			
			ADD_WIDGET_BOOL("bGiveFood2", MPGlobalsInteractions.bGiveFood2)
			ADD_WIDGET_BOOL("bRemoveFood2", MPGlobalsInteractions.bRemoveFood2)
			
			ADD_WIDGET_BOOL("bGiveFood3", MPGlobalsInteractions.bGiveFood3)
			ADD_WIDGET_BOOL("bRemoveFood3", MPGlobalsInteractions.bRemoveFood3)
			
			ADD_WIDGET_BOOL("bSimulateEarlyExitEvent", MPGlobalsInteractions.bSimulateEarlyExitEvent)
			


		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Quick Restart")
			ADD_WIDGET_BOOL("bSimulate_QuickplayButtonHeld", MPGlobalsInteractions.bSimulate_QuickplayButtonHeld)
			ADD_WIDGET_BOOL("bSimulate_PressedR1", MPGlobalsInteractions.bSimulate_PressedR1)
			ADD_WIDGET_BOOL("bSimulate_PressedL1", MPGlobalsInteractions.bSimulate_PressedL1)
			ADD_WIDGET_BOOL("bQuickRestartAnim", MPGlobalsInteractions.bQuickRestartAnim)
			//ADD_WIDGET_BOOL("bThisIsAQuickRestartAnim", MPGlobalsInteractions.bThisIsAQuickRestartAnim)
			ADD_WIDGET_BOOL("bSimulate_QuickplayButtonPressed", MPGlobalsInteractions.bSimulate_QuickplayButtonPressed)		
			ADD_WIDGET_BOOL("bInteractionIsPlayingForQuickRestart", MPGlobalsInteractions.bInteractionIsPlayingForQuickRestart)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Prop offsets")
		
//			ADD_WIDGET_FLOAT_SLIDER("DetachForceX", DetachForceX, -10000.0, 10000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("DetachForceY", DetachForceY, -10000.0, 10000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("DetachForceZ", DetachForceZ, -10000.0, 10000.0, 0.001)
//			
//			ADD_WIDGET_FLOAT_SLIDER("DetachOffsetX", DetachOffsetX, -10000.0, 10000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("DetachOffsetY", DetachOffsetY, -10000.0, 10000.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("DetachOffsetZ", DetachOffsetZ, -10000.0, 10000.0, 0.001)

			ADD_WIDGET_FLOAT_SLIDER("DetachForceScale", DetachForceScale, 0.0, 1000.0, 0.001)

			ADD_WIDGET_FLOAT_SLIDER("CiggyPFXAttachOffsetX", CiggyPFXAttachOffsetX, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("CiggyPFXAttachOffsetY", CiggyPFXAttachOffsetY, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("CiggyPFXAttachOffsetZ", CiggyPFXAttachOffsetZ, -1.0, 1.0, 0.001)
					
			
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Selfie")
			ADD_WIDGET_INT_SLIDER("NO_OF_FRAMES_BEFORE_CHANGE", NO_OF_FRAMES_BEFORE_CHANGE, 0, 100, 1)	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Tunables")			
			ADD_WIDGET_BOOL("g_sMPTunables.bTURN_ON_HALLOWEEN_ANIMS", g_sMPTunables.bTURN_ON_HALLOWEEN_ANIMS)		
		STOP_WIDGET_GROUP()


	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

FUNC BOOL IsPedPlayerPed(PED_INDEX PedID)
	IF DOES_ENTITY_EXIST(PedID)
		IF (PedID = PLAYER_PED_ID())
		AND NOT IS_CUTSCENE_PLAYING() 
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC STRING GetFullBodyAnimDictionary(INTERACTION_ANIM_DATA &AnimData)
	IF (AnimData.PedID = PLAYER_PED_ID())
		IF NOT IS_PLAYER_FEMALE()
			RETURN TextLabelToString(AnimData.Data.strAnimDict[AD_FULL_BODY_M])	
		ELSE
			RETURN TextLabelToString(AnimData.Data.strAnimDict[AD_FULL_BODY_F])	
		ENDIF		
	ELSE
		IF DOES_ENTITY_EXIST(AnimData.PedID)
			IF IS_PED_MALE(AnimData.PedID)
				RETURN TextLabelToString(AnimData.Data.strAnimDict[AD_FULL_BODY_M])	
			ELSE
				RETURN TextLabelToString(AnimData.Data.strAnimDict[AD_FULL_BODY_F])	
			ENDIF
		ELSE
			IF (AnimData.bIsMale)
				RETURN TextLabelToString(AnimData.Data.strAnimDict[AD_FULL_BODY_M])	
			ELSE
				RETURN TextLabelToString(AnimData.Data.strAnimDict[AD_FULL_BODY_F])	
			ENDIF
		ENDIF
	ENDIF
ENDFUNC

FUNC STRING InteractionAnimDictFromInteractionAnimData(INTERACTION_ANIM_DATA &AnimData, BOOL bFullBody=FALSE, BOOL bSelfie=FALSE)
	IF (bFullBody)
	OR IS_INTERACTION_FULL_BODY_ONLY(AnimData.iType, AnimData.iAnim)
		RETURN GetFullBodyAnimDictionary(AnimData)
	ELSE
		RETURN GetInteractionsAnimsDict(AnimData.Data, AnimData.iType, AnimData.bIsDriverSide, AnimData.bIsLowVehicle, AnimData.bIsRearSeat, AnimData.bIsSideFacing, AnimData.bIsDriver, AnimData.bIsBohdi, bSelfie)
	ENDIF
ENDFUNC

FUNC STRING AnimNameFromStage(INTERACTION_ANIM_DATA &AnimData, INTERACTION_ANIM_STAGE AnimStage)
	SWITCH AnimStage
		CASE IAS_INTRO 		RETURN TextLabelToString(AnimData.Data.strAnimNameIntro)
		CASE IAS_BODY		RETURN TextLabelToString(AnimData.Data.strAnimName)
		CASE IAS_OUTRO		RETURN TextLabelToString(AnimData.Data.strAnimNameOutro)
		CASE IAS_FULL_BODY	RETURN TextLabelToString(AnimData.Data.strAnimNameFullBody)
	ENDSWITCH
	RETURN TextLabelToString(AnimData.Data.strAnimName)
ENDFUNC

FUNC STRING GetCurrentAnimName(INTERACTION_ANIM_DATA &AnimData)
	RETURN AnimNameFromStage(AnimData, AnimData.AnimStage)
ENDFUNC


PROC RemoveInteractionProps(INTERACTION_ANIM_DATA &AnimData)
	IF AnimData.Data.bHasProp
		SET_MODEL_AS_NO_LONGER_NEEDED(AnimData.Data.PropModel)
		
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_MAKE_IT_RAIN))
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xs_Prop_Arena_Cash_Pile_M")))
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xs_Prop_Arena_Cash_Pile_S")))
		ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_DRINK_3))
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xs_Prop_Arena_Champ_Open")))
		#IF FEATURE_COPS_N_CROOKS
		ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_POUR_ONE_OUT))
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("ac_prop_ac_Whiskey_Bottle_S")))
		ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_EAT_DOUGHNUT))
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("ac_prop_ac_doughnut_02a")))
		#ENDIF	
		ENDIF
		
		#IF IS_DEBUG_BUILD
		NET_PRINT("[interactions] RemoveInteractionProps - removed prop ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG(AnimData.Data.PropModel)) NET_NL()
		#ENDIF
	ENDIF
ENDPROC

PROC RemoveInteractionPtfx(INTERACTION_ANIM_DATA &AnimData)
	IF AnimData.Data.bUsesPTFX
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_DRINK_3))
			REMOVE_NAMED_PTFX_ASSET("scr_ba_club")
		ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_MAKE_IT_RAIN))
			REMOVE_NAMED_PTFX_ASSET("scr_xs_celebration")
		#IF FEATURE_COPS_N_CROOKS
		ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_POUR_ONE_OUT))
			REMOVE_NAMED_PTFX_ASSET("scr_arc_em")
		#ENDIF	
		ELSE
			REMOVE_PTFX_ASSET()
			#IF IS_DEBUG_BUILD
			NET_PRINT("[interactions] RemoveInteractionPtfx - removed ptfx ") NET_NL()
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_NAMED_INTERACTION_PTFX_ASSET(INTERACTION_ANIM_DATA &AnimData)
	SWITCH (INT_TO_ENUM(PLAYER_INTERACTIONS, AnimData.iAnim))
		CASE PLAYER_INTERACTION_MAKE_IT_RAIN
			RETURN "scr_xs_celebration"
		CASE PLAYER_INTERACTION_DRINK_3
			RETURN "scr_ba_club"
		CASE PLAYER_INTERACTION_SMOKE
			RETURN "scr_mp_cig"
		#IF FEATURE_COPS_N_CROOKS
		CASE PLAYER_INTERACTION_POUR_ONE_OUT
			RETURN "scr_arc_em"
		#ENDIF	
		DEFAULT
			RETURN ""
	ENDSWITCH
ENDFUNC

PROC USE_NAMED_INTERACTION_PTFX_ASSET(INTERACTION_ANIM_DATA &AnimData)
	STRING strInteractionAsset = GET_NAMED_INTERACTION_PTFX_ASSET(AnimData)
	IF COMPARE_STRINGS(strInteractionAsset, "") != 0
		USE_PARTICLE_FX_ASSET(strInteractionAsset)
	ENDIF
ENDPROC

FUNC BOOL HasLoadedPtfx(INTERACTION_ANIM_DATA &AnimData)
	IF AnimData.Data.bUsesPTFX
		STRING strInteractionAsset = GET_NAMED_INTERACTION_PTFX_ASSET(AnimData)
		
		IF COMPARE_STRINGS(strInteractionAsset, "") != 0
			REQUEST_NAMED_PTFX_ASSET(strInteractionAsset)
			IF HAS_NAMED_PTFX_ASSET_LOADED(strInteractionAsset)
				RETURN TRUE
			ENDIF
		ELSE
			REQUEST_PTFX_ASSET()
			IF HAS_PTFX_ASSET_LOADED()
				RETURN TRUE
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 30) = 0
			NET_PRINT("[interactions] HasLoadedPtfx - waiting to load ptfx")  NET_NL()
		ENDIF
		#ENDIF
		
		#IF USE_FINAL_PRINTS
		IF (GET_FRAME_COUNT() % 30) = 0
			PRINTLN_FINAL("[interactions] HasLoadedPtfx - waiting to load ptfx")
		ENDIF
		#ENDIF
		
		SET_BIT(g_transitionIntFlow[1], ciTRAN_FLOW_HasLoadedPtfx)
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

//FUNC BOOL HasLoadedCustomObjects(INTERACTION_ANIM_DATA &AnimData)
//	IF DOES_CUSTOM_OBJECT_ANIM_EXIST(AnimData.iType, AnimData.iAnim)
//		CUSTOM_OBJECT_ANIM_DATA CustomObjectData
//		GET_CUSTOM_OBJECT_ANIM_DETAILS(AnimData.iType, AnimData.iAnim, CustomObjectData)
//		IF NOT (CustomObjectData.ObjectModel = DUMMY_MODEL_FOR_SCRIPT)
//			REQUEST_MODEL(CustomObjectData.ObjectModel)	
//			IF HAS_MODEL_LOADED(CustomObjectData.ObjectModel)
//				RETURN(TRUE)
//			ELSE
//				#IF IS_DEBUG_BUILD
//					PRINTLN("[interactions] HasLoadedCustomObjects - waiting to load prop ", GET_MODEL_NAME_FOR_DEBUG(CustomObjectData.ObjectModel) )
//				#ENDIF
//			ENDIF
//		ELSE
//			RETURN(TRUE)
//		ENDIF
//	ELSE
//		RETURN(TRUE)
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC

//PROC CreateCustomObject(INTERACTION_ANIM_DATA &AnimData)
//	IF DOES_CUSTOM_OBJECT_ANIM_EXIST(AnimData.iType, AnimData.iAnim)
//		CUSTOM_OBJECT_ANIM_DATA CustomObjectData
//		GET_CUSTOM_OBJECT_ANIM_DETAILS(AnimData.iType, AnimData.iAnim, CustomObjectData)
//		AnimData.CustomObjectID = CREATE_OBJECT(CustomObjectData.ObjectModel, CustomObjectData.vCoords, FALSE)
//		SET_ENTITY_ROTATION(AnimData.CustomObjectID, CustomObjectData.vRotation)
//	ENDIF
//ENDPROC
//
//PROC StartCustomObjectAnim(INTERACTION_ANIM_DATA &AnimData, STRING AnimDict, FLOAT fRate, FLOAT fStartPhase)
//	IF DOES_CUSTOM_OBJECT_ANIM_EXIST(AnimData.iType, AnimData.iAnim)
//		CUSTOM_OBJECT_ANIM_DATA CustomObjectData
//		GET_CUSTOM_OBJECT_ANIM_DETAILS(AnimData.iType, AnimData.iAnim, CustomObjectData)
//		PLAY_ENTITY_ANIM(AnimData.CustomObjectID, CustomObjectData.strAnimName, AnimDict, 0.0, FALSE, FALSE)	
//		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(AnimData.CustomObjectID)
//		SET_ENTITY_ANIM_SPEED(AnimData.CustomObjectID, AnimDict, CustomObjectData.strAnimName, fRate)
//		SET_ENTITY_ANIM_CURRENT_TIME(AnimData.CustomObjectID, AnimDict, CustomObjectData.strAnimName, fStartPhase)
//	ENDIF
//ENDPROC



//PROC RemoveCustomObjects(INTERACTION_ANIM_DATA &AnimData)
//	IF DOES_CUSTOM_OBJECT_ANIM_EXIST(AnimData.iType, AnimData.iAnim)
//		CUSTOM_OBJECT_ANIM_DATA CustomObjectData
//		GET_CUSTOM_OBJECT_ANIM_DETAILS(AnimData.iType, AnimData.iAnim, CustomObjectData)
//		IF NOT (CustomObjectData.ObjectModel = DUMMY_MODEL_FOR_SCRIPT)
//			SET_MODEL_AS_NO_LONGER_NEEDED(CustomObjectData.ObjectModel)
//			#IF IS_DEBUG_BUILD
//			NET_PRINT("[interactions] RemoveCustomObjects - removed prop ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG(CustomObjectData.ObjectModel)) NET_NL()
//			#ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

FUNC BOOL HasLoadedInteractionProps(INTERACTION_ANIM_DATA &AnimData)
	IF AnimData.Data.bHasProp
		BOOL bMidLoaded = TRUE
		BOOL bLastLoaded = TRUE
		
		REQUEST_MODEL(AnimData.Data.PropModel)
		
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_MAKE_IT_RAIN))
			bMidLoaded = FALSE
			
			REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xs_Prop_Arena_Cash_Pile_M")))
			
			IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xs_Prop_Arena_Cash_Pile_M")))
				bMidLoaded = TRUE
			ENDIF
			
			bLastLoaded = FALSE
			
			REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xs_Prop_Arena_Cash_Pile_S")))
			
			IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xs_Prop_Arena_Cash_Pile_S")))
				bLastLoaded = TRUE
			ENDIF
		ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_DRINK_3))
			bLastLoaded = FALSE
			
			REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xs_Prop_Arena_Champ_Open")))
			
			IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xs_Prop_Arena_Champ_Open")))
				bLastLoaded = TRUE
			ENDIF
		#IF FEATURE_COPS_N_CROOKS
		ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_POUR_ONE_OUT))
			bLastLoaded = FALSE
			
			REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("ac_prop_ac_Whiskey_Bottle_S")))
			
			IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("ac_prop_ac_Whiskey_Bottle_S")))
				bLastLoaded = TRUE
			ENDIF
		ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_EAT_DOUGHNUT))
			bLastLoaded = FALSE
			
			REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("ac_prop_ac_doughnut_02a")))
			
			IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("ac_prop_ac_doughnut_02a")))
				bLastLoaded = TRUE
			ENDIF
		#ENDIF	
		ENDIF
		
		IF HAS_MODEL_LOADED(AnimData.Data.PropModel)
		AND bMidLoaded
		AND bLastLoaded
			RETURN(TRUE)
		ELSE
			#IF IS_DEBUG_BUILD
			NET_PRINT("[interactions] HasLoadedInteractionProps - waiting to load prop ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG(AnimData.Data.PropModel)) NET_NL()		
			#ENDIF
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[interactions] HasLoadedInteractionProps - waiting to load prop ", ENUM_TO_INT(AnimData.Data.PropModel) )			
			#ENDIF
			SET_BIT(g_transitionIntFlow[1], ciTRAN_FLOW_HasLoadedInteractionProps)			
		ENDIF
	ELSE
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC


PROC RemoveInteractionAssetsDict(INTERACTION_ANIM_DATA &AnimData)
	
	IF DOES_ENTITY_EXIST(AnimData.PedID)

		// remove upper body if not a full body only
		IF NOT IS_INTERACTION_FULL_BODY_ONLY(AnimData.iType, AnimData.iAnim)
		
			#IF IS_DEBUG_BUILD
			IF NOT DOES_ANIM_DICT_EXIST(InteractionAnimDictFromInteractionAnimData(AnimData))							
				PRINTLN("[interactions] RemoveInteractionAssetsDict - anim dict", InteractionAnimDictFromInteractionAnimData(AnimData), " does not exist!")
				SCRIPT_ASSERT("[interactions] RemoveInteractionAssetsDict - This anim dictionary does not exist, it may be in a future DLC that you do not have installed.") 		
				EXIT
			ENDIF
			#ENDIF
		
			REMOVE_ANIM_DICT(InteractionAnimDictFromInteractionAnimData(AnimData))
			NET_PRINT("[interactions] RemoveInteractionAssetsDict - removed anim dict ") NET_PRINT(InteractionAnimDictFromInteractionAnimData(AnimData)) NET_NL()
			
		ENDIF
		
		// always remove full body too, if it has one
		IF DOES_INTERACTION_HAVE_FULL_BODY(AnimData.iType, AnimData.iAnim)

			#IF IS_DEBUG_BUILD
			IF NOT DOES_ANIM_DICT_EXIST(InteractionAnimDictFromInteractionAnimData(AnimData, TRUE))							
				PRINTLN("[interactions] RemoveInteractionAssetsDict - full body anim dict", InteractionAnimDictFromInteractionAnimData(AnimData, TRUE), " does not exist!")
				SCRIPT_ASSERT("[interactions] RemoveInteractionAssetsDict - This full body anim dictionary does not exist, it may be in a future DLC that you do not have installed.") 		
				EXIT
			ENDIF
			#ENDIF	
		
			REMOVE_ANIM_DICT(InteractionAnimDictFromInteractionAnimData(AnimData, TRUE))	
			NET_PRINT("[interactions] RemoveInteractionAssetsDict - removed full body anim dict ") NET_PRINT(InteractionAnimDictFromInteractionAnimData(AnimData, TRUE)) NET_NL()
				
		ENDIF
		
		// always remove selfie too, if it has one
		IF DOES_INTERACTION_HAVE_SELFIE(AnimData.iType, AnimData.iAnim)
		
			#IF IS_DEBUG_BUILD
			IF NOT DOES_ANIM_DICT_EXIST(InteractionAnimDictFromInteractionAnimData(AnimData, FALSE, TRUE))							
				PRINTLN("[interactions] RemoveInteractionAssetsDict - selfie anim dict", InteractionAnimDictFromInteractionAnimData(AnimData, FALSE, TRUE), " does not exist!")
				SCRIPT_ASSERT("[interactions] RemoveInteractionAssetsDict - This selfie anim dictionary does not exist, it may be in a future DLC that you do not have installed.") 		
				EXIT
			ENDIF
			#ENDIF	
		
			REMOVE_ANIM_DICT(InteractionAnimDictFromInteractionAnimData(AnimData, FALSE, TRUE))
			NET_PRINT("[interactions] RemoveInteractionAssetsDict - removed selfie anim dict ") NET_PRINT(InteractionAnimDictFromInteractionAnimData(AnimData, FALSE, TRUE)) NET_NL()
						
		ENDIF
		
	ELSE
		NET_PRINT("[interactions] RemoveInteractionAssetsDict - ped doesn't exist ") NET_NL()
	ENDIF
ENDPROC

FUNC BOOL HasLoadedInteractionAnimDict(INTERACTION_ANIM_DATA &AnimData, BOOL bFullBody, BOOL bSelfie)

	#IF IS_DEBUG_BUILD
	IF NOT DOES_ANIM_DICT_EXIST(InteractionAnimDictFromInteractionAnimData(AnimData, bFullBody, bSelfie))							
		PRINTLN("[interactions] HasLoadedInteractionAnimDict - anim dict ", InteractionAnimDictFromInteractionAnimData(AnimData, bFullBody, bSelfie), " does not exist!") 
		SCRIPT_ASSERT("[interactions] HasLoadedInteractionAnimDict - This anim dictionary does not exist, it may be in a future DLC that you do not have installed.") 				
		RETURN(TRUE)
	ENDIF
	#ENDIF

	REQUEST_ANIM_DICT(InteractionAnimDictFromInteractionAnimData(AnimData, bFullBody, bSelfie))
	IF HAS_ANIM_DICT_LOADED(InteractionAnimDictFromInteractionAnimData(AnimData, bFullBody, bSelfie))
		RETURN(TRUE)
	ELSE
		NET_PRINT("[interactions] HasLoadedInteractionAnimDict - waiting to load anim dict ") NET_PRINT(InteractionAnimDictFromInteractionAnimData(AnimData, bFullBody, bSelfie)) NET_NL()	
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("interactions] HasLoadedInteractionAnimDict - waiting to load anim dict ", InteractionAnimDictFromInteractionAnimData(AnimData, bFullBody, bSelfie))
		#ENDIF
		SET_BIT(g_transitionIntFlow[1], ciTRAN_FLOW_HasLoadedInteractionAnimDict)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL HasSoundBankLoaded()
	RETURN REQUEST_SCRIPT_AUDIO_BANK("DLC_GTAO/SNACKS", FALSE)	
ENDFUNC

PROC RemoveInteractionAssets(INTERACTION_ANIM_DATA &AnimData)

	IF NOT IsInteractionNone(AnimData.iType, AnimData.iAnim)
	AND NOT IsInteractionWithoutAnim(AnimData.iAnim)
		RemoveInteractionAssetsDict(AnimData)
		PRINTLN("[interactions] RemoveInteractionAssets - removed anim dict ", InteractionAnimDictFromInteractionAnimData(AnimData))
	ELSE
		PRINTLN("[interactions] RemoveInteractionAssets - interaction is none."	)
	ENDIF
	RemoveInteractionProps(AnimData)
	//RemoveCustomObjects(AnimData)
	
	MPGlobalsInteractions.iLoadedType = -1
	MPGlobalsInteractions.iLoadedAnim = -1
	
	PRINTLN("[interactions] RemoveInteractionAssets - finished ") 
ENDPROC


FUNC BOOL LoadInteractionAssets(INTERACTION_ANIM_DATA &AnimData, BOOL bFullBody, BOOL bSelfie)

	IF AnimData.Data.bSpeechOnly
		MPGlobalsInteractions.bInteractionAssetsAreLoading = FALSE
		RETURN(TRUE)
	ENDIF

	// already loaded (for optimising, less expensive than checks below)
	IF (AnimData.iType = MPGlobalsInteractions.iLoadedType)
	AND (AnimData.iAnim = MPGlobalsInteractions.iLoadedAnim)
		MPGlobalsInteractions.bInteractionAssetsAreLoading = FALSE
		RETURN(TRUE)
	ENDIF

	IF HasLoadedInteractionAnimDict(AnimData, bFullBody, bSelfie)
	AND HasLoadedInteractionProps(AnimData)
	AND HasLoadedPtfx(AnimData)
	AND HasSoundBankLoaded()
	//AND HasLoadedCustomObjects(AnimData)
		MPGlobalsInteractions.bInteractionAssetsAreLoading = FALSE
		RETURN(TRUE)
	ELSE
		NET_PRINT("[interactions] LoadInteractionAssets - loading... ")  NET_NL()	
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[interactions] LoadInteractionAssets - loading... ")
		#ENDIF
		SET_BIT(g_transitionIntFlow[1], ciTRAN_FLOW_LoadInteractionAssetS)
		IF HasLoadedInteractionAnimDict(AnimData, bFullBody, bSelfie) = FALSE
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[interactions] LoadInteractionAssets - loading... HasLoadedInteractionAnimDict ")
			#ENDIF
			SET_BIT(g_transitionIntFlow[1], ciTRAN_FLOW_LoadInteractionAssets_HasLoadedInteractionAnimDict)
		ENDIF
		IF HasLoadedInteractionProps(AnimData) = FALSE
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[interactions] LoadInteractionAssets - loading... HasLoadedInteractionProps ")
			#ENDIF
			SET_BIT(g_transitionIntFlow[1], ciTRAN_FLOW_LoadInteractionAssets_HasLoadedInteractionProps)
		ENDIF
		IF HasLoadedPtfx(AnimData) = FALSE
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[interactions] LoadInteractionAssets - loading... HasLoadedPtfx ")
			#ENDIF
			SET_BIT(g_transitionIntFlow[1], ciTRAN_FLOW_LoadInteractionAssets_HasLoadedPtfx)
		ENDIF
		IF HasSoundBankLoaded() = FALSE
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[interactions] LoadInteractionAssets - loading... HasSoundBankLoaded ")
			#ENDIF
			SET_BIT(g_transitionIntFlow[1], ciTRAN_FLOW_LoadInteractionAssets_HasSoundBankLoaded)
		ENDIF
		
	ENDIF

	MPGlobalsInteractions.bInteractionAssetsAreLoading = TRUE
	RETURN(FALSE)
ENDFUNC

PROC UnLoadQuickplayRestart()
	IF NOT (MPGlobalsInteractions.iPreLoadQuickplay = g_iCurrentOnFootQuickplayAnim)
	AND NOT (MPGlobalsInteractions.iPreLoadQuickplay = -1)
		INTERACTION_DATA InteractionData
		FillInteractionData(g_iCurrentOnFootQuickplayType, MPGlobalsInteractions.iPreLoadQuickplay, InteractionData)
		REMOVE_ANIM_DICT(InteractionData.strAnimDict[AD_DEFAULT])
		PRINTLN("[interactions] [Quickplay Restart] - UnLoadQuickplayRestart unloading old quickplay, iPreLoadQuickplay = ", MPGlobalsInteractions.iPreLoadQuickplay)
	ENDIF
ENDPROC

FUNC BOOL PreLoadSelectedQuickplay()

	// make sure previously loaded one is removed.
	UnLoadQuickplayRestart()
	
	// request new anims
	INTERACTION_DATA InteractionData
	FillInteractionData(g_iCurrentOnFootQuickplayType, g_iCurrentOnFootQuickplayAnim, InteractionData)
	IF NOT (MPGlobalsInteractions.iPreLoadQuickplay = g_iCurrentOnFootQuickplayAnim)
		PRINTLN("[interactions] [Quickplay Restart] - PreLoadSelectedQuickplay, setting iPreLoadQuickplay = ", MPGlobalsInteractions.iPreLoadQuickplay)
	ENDIF

	IF (MPGlobalsInteractions.iPreLoadQuickplay = -1)
		PRINTLN("[interactions] [Quickplay Restart] - PreLoadSelectedQuickplay MPGlobalsInteractions.iPreLoadQuickplay = -1")	
		RETURN TRUE
	ENDIF
	
	REQUEST_ANIM_DICT(InteractionData.strAnimDict[AD_DEFAULT])
	IF HAS_ANIM_DICT_LOADED(InteractionData.strAnimDict[AD_DEFAULT])
		PRINTLN("[interactions] [Quickplay Restart] - PreLoadSelectedQuickplay loaded")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[interactions] [Quickplay Restart] - PreLoadSelectedQuickplay loading...")
	RETURN FALSE
ENDFUNC

PROC QuickplayRestartPreLoader()
	IF (MPGlobalsInteractions.bDoPreLoadQuickplay)
		PRINTLN("[interactions] [Quickplay Restart] - QuickplayRestartPreLoader is Active.")
		PreLoadSelectedQuickplay()
		MPGlobalsInteractions.bDoPreLoadQuickplay = FALSE
	ELSE
		IF NOT (MPGlobalsInteractions.iPreLoadQuickplay = -1)
			PRINTLN("[interactions] [Quickplay Restart] - QuickplayRestartPreLoader is Unloading.")
			UnLoadQuickplayRestart()
			MPGlobalsInteractions.iPreLoadQuickplay = -1
		ENDIF
	ENDIF
ENDPROC

PROC CopyPropDetails(INTERACTION_ANIM_DATA &FromAnimData, INTERACTION_ANIM_DATA &ToAnimData)
	NET_PRINT("[interactions] CopyPropDetails called...") NET_NL()
	ToAnimData.bCreateProp 		= FromAnimData.bCreateProp
	ToAnimData.bDeleteProp 		= FromAnimData.bDeleteProp
	ToAnimData.bObjectCreated 	= FromAnimData.bObjectCreated
	ToAnimData.ObjectID			= FromAnimData.ObjectID
	ToAnimData.NetworkObjectID	= FromAnimData.NetworkObjectID
	ToAnimData.PTFXID_Loop		= FromAnimData.PTFXID_Loop
	NET_PRINT("[interactions] ToAnimData.bCreateProp = ") NET_PRINT_BOOL(ToAnimData.bCreateProp) NET_NL()
	NET_PRINT("[interactions] ToAnimData.bDeleteProp = ") NET_PRINT_BOOL(ToAnimData.bDeleteProp) NET_NL()
	NET_PRINT("[interactions] ToAnimData.bObjectCreated = ") NET_PRINT_BOOL(ToAnimData.bObjectCreated) NET_NL()
	NET_PRINT("[interactions] ToAnimData.ObjectID = ") NET_PRINT_INT(NATIVE_TO_INT(ToAnimData.ObjectID)) NET_NL()
	NET_PRINT("[interactions] ToAnimData.NetworkObjectID = ") NET_PRINT_INT(NATIVE_TO_INT(ToAnimData.NetworkObjectID)) NET_NL()
	NET_PRINT("[interactions] ToAnimData.PTFXID_Loop = ") NET_PRINT_INT(NATIVE_TO_INT(ToAnimData.PTFXID_Loop)) NET_NL()
ENDPROC

FUNC BOOL IsPedUsingSelfieTask(PED_INDEX PedID)
	//SCRIPTTASKSTATUS TaskStatus
	//TaskStatus = GET_SCRIPT_TASK_STATUS(PedID, SCRIPT_TASK_USE_MOBILE_PHONE)	
	//IF (TaskStatus = PERFORMING_TASK)
	IF IS_PED_RUNNING_MOBILE_PHONE_TASK(PedID)
	AND IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()	
		RETURN(TRUE)
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[interactions] IsPedUsingSelfieTask - IS_PED_RUNNING_MOBILE_PHONE_TASK = ", IS_PED_RUNNING_MOBILE_PHONE_TASK(PedID))
			PRINTLN("[interactions] IsPedUsingSelfieTask - IS_CELLPHONE_CAMERA_IN_SELFIE_MODE = ", IS_CELLPHONE_CAMERA_IN_SELFIE_MODE())
		#ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsPlayingFullBody(INTERACTION_ANIM_DATA &AnimData)
	IF NOT IS_ENTITY_DEAD(AnimData.PedID)
		IF (AnimData.bSelfieMode)
			IF (AnimData.AnimStage = IAS_FULL_BODY)
				IF IsPedUsingSelfieTask(AnimData.PedID)
				AND IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
					#IF IS_DEBUG_BUILD
					NET_PRINT("[interactions] IsPlayingFullBody - returning TRUE - playing full body anim (selfie) ") NET_PRINT_FLOAT(GET_PHONE_GESTURE_ANIM_CURRENT_TIME(AnimData.PedID)) NET_NL()
					#ENDIF
					RETURN(TRUE)								
				ENDIF
			ENDIF
		ELSE	
			IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, InteractionAnimDictFromInteractionAnimData(AnimData, TRUE), AnimData.Data.strAnimNameFullBody)					
				
				#IF IS_DEBUG_BUILD
				NET_PRINT("[interactions] IsPlayingFullBody - returning TRUE - playing full body anim, time = ") NET_PRINT_FLOAT(GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, InteractionAnimDictFromInteractionAnimData(AnimData, TRUE), AnimData.Data.strAnimNameFullBody)) NET_NL()	
				#ENDIF
				
				RETURN(TRUE)	
			ENDIF
		ENDIF
	ENDIF
	NET_PRINT("[interactions] IsPlayingFullBody - returning FALSE") NET_NL()
	RETURN(FALSE)		
ENDFUNC

FUNC BOOL IsSafeToDeleteProp(INTERACTION_ANIM_DATA &AnimData)
				
	IF NOT IsPedPlayerPed(AnimData.PedID) 
	OR (NETWORK_DOES_NETWORK_ID_EXIST(AnimData.NetworkObjectID) AND TAKE_CONTROL_OF_NET_ID(AnimData.NetworkObjectID))
	OR DOES_ENTITY_EXIST(AnimData.ObjectID)
		RETURN(TRUE)
	ELSE
		NET_PRINT("[interactions] IsSafeToDeleteProp - returning FALSE") NET_NL()
	ENDIF

	RETURN(FALSE)
ENDFUNC

PROC DeleteProp(INTERACTION_ANIM_DATA &AnimData, BOOL bFlick=FALSE, BOOL bDeleteImmediately=FALSE)
	
	IF ((AnimData.PTFXID_Loop != INT_TO_NATIVE(PTFX_ID, -1)) AND DOES_PARTICLE_FX_LOOPED_EXIST(AnimData.PTFXID_Loop))
		STOP_PARTICLE_FX_LOOPED(AnimData.PTFXID_Loop)
		NET_PRINT("[interactions] DeleteProp - stopping pfx") NET_NL()
	ENDIF
	
	IF (bDeleteImmediately)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(AnimData.NetworkObjectID)
			DELETE_NET_ID(AnimData.NetworkObjectID)
			NET_PRINT("[interactions] DeleteProp - deleted prop immediately") NET_NL()
		ENDIF	
		IF DOES_ENTITY_EXIST(AnimData.ObjectID)
			DELETE_OBJECT(AnimData.ObjectID)
			NET_PRINT("[interactions] DeleteProp - deleted prop (1)") NET_NL()	
		ENDIF
	ELSE
	
		IF NOT IS_PED_INJURED(AnimData.PedID) 
		AND IS_PED_IN_ANY_VEHICLE(AnimData.PedID)
			IF IsPedPlayerPed(AnimData.PedID)
				DELETE_NET_ID(AnimData.NetworkObjectID)
				NET_PRINT("[interactions] DeleteProp - DELETE_NET_ID") NET_NL()
			ENDIF
			IF DOES_ENTITY_EXIST(AnimData.ObjectID)
				DELETE_OBJECT(AnimData.ObjectID)
				NET_PRINT("[interactions] DeleteProp - deleted prop (2)") NET_NL()	
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(AnimData.ObjectID)
				DETACH_ENTITY(AnimData.ObjectID)
				IF (bFlick)
				//AND IsPlayingFullBody(AnimData)
					NET_PRINT("[interactions] DeleteProp - applying force to entity.") NET_NL()
					
					// work out force
					VECTOR vForce
					vForce = GET_ENTITY_COORDS(AnimData.ObjectID, FALSE) - AnimData.vObjectCoordsLastFrame
					vForce /= VMAG(vForce)
					vForce *= DetachForceScale
					NET_PRINT("[interactions] DeleteProp - applying force ") NET_PRINT_VECTOR(vForce) NET_NL()
					
					APPLY_FORCE_TO_ENTITY(AnimData.ObjectID, APPLY_TYPE_IMPULSE, vForce, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
				ENDIF
			ENDIF	
			IF IsPedPlayerPed(AnimData.PedID)
				CLEANUP_NET_ID(AnimData.NetworkObjectID)
				NET_PRINT("[interactions] DeleteProp - CLEANUP_NET_ID") NET_NL()								
			ENDIF
			IF DOES_ENTITY_EXIST(AnimData.ObjectID)
			
				IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
				OR IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
					IF NETWORK_GET_ENTITY_IS_NETWORKED(AnimData.ObjectID)
						NET_PRINT("[interactions] DeleteProp - SET_OBJECT_AS_NO_LONGER_NEEDED - unregistering networked entity.") NET_NL()	
						NETWORK_UNREGISTER_NETWORKED_ENTITY(AnimData.ObjectID)
					ELSE
						NET_PRINT("[interactions] DeleteProp - SET_OBJECT_AS_NO_LONGER_NEEDED - not networked.") NET_NL()	
					ENDIF
				ENDIF			
			
				SET_OBJECT_AS_NO_LONGER_NEEDED(AnimData.ObjectID)
				NET_PRINT("[interactions] DeleteProp - SET_OBJECT_AS_NO_LONGER_NEEDED.") NET_NL()				
				
			ENDIF
		ENDIF
		
	ENDIF
	
	AnimData.ObjectID = INT_TO_NATIVE(OBJECT_INDEX, -1)			
	AnimData.bObjectCreated = FALSE	
	AnimData.bCreateProp = FALSE
	AnimData.bSwappedMidItem = TRUE
	AnimData.bSwappedLastItem = TRUE
	
	NET_PRINT("[interactions] DeleteProp - finished") NET_NL()
	
ENDPROC

PROC CleanupAnimPropsAndVFX(INTERACTION_ANIM_DATA &AnimData)
	IF (AnimData.bObjectCreated)
		IF IsSafeToDeleteProp(AnimData)
			DeleteProp(AnimData, DEFAULT, AnimData.Data.bDeletePropAtEnd)
			NET_PRINT("[interactions] CleanupAnimPropsAndVFX - called DeleteProp") NET_NL()
		ENDIF
	ENDIF
	
	IF (AnimData.bCreateProp)
		AnimData.bCreateProp = FALSE
		NET_PRINT("[interactions] CleanupAnimPropsAndVFX - reset bCreateProp") NET_NL()
	ENDIF
	IF (AnimData.bDeleteProp)
		AnimData.bDeleteProp = FALSE
		NET_PRINT("[interactions] CleanupAnimPropsAndVFX - reset bDeleteProp") NET_NL()
	ENDIF
	
	// stop broadcasting smoking ptfx
	IF (AnimData.PedID = PLAYER_PED_ID())
		IF (AnimData.iAnim = ENUM_TO_INT(PASSENGER_INTERACTION_SMOKE))
		OR (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_SMOKE))
			IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlaySmokingPFX))
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlaySmokingPFX)
				NET_PRINT("[interactions] bPlaySmokingPFX = FALSE 2 ") NET_NL()
			ENDIF
		ENDIF
		
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_MAKE_IT_RAIN))
			IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayMakeItRainPFX))
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayMakeItRainPFX)
				NET_PRINT("[interactions] bPlayMakeItRainPFX = FALSE 2 ") NET_NL()
			ENDIF
		ENDIF
		
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_DRINK_3))
			IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayChampagnePFX))
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayChampagnePFX)
				NET_PRINT("[interactions] bPlayChampagnePFX = FALSE 2 ") NET_NL()
			ENDIF
		ENDIF
		
		#IF FEATURE_COPS_N_CROOKS
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_POUR_ONE_OUT))
			IF (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayWhiskeyPFX))
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayWhiskeyPFX)
				NET_PRINT("[interactions] PIBD_PlayWhiskeyPFX = FALSE 2 ") NET_NL()
			ENDIF
		ENDIF
		#ENDIf
		
	ENDIF
	
ENDPROC

FUNC BOOL CreatePropForInteraction(INTERACTION_ANIM_DATA &AnimData, MODEL_NAMES PropModel, BOOL bDeletePrevious = FALSE)
	IF IsPedPlayerPed(AnimData.PedID)
	AND NOT IS_POST_MISSION_SCENE_ACTIVE()
	AND NOT MPGlobalsInteractions.PlayerInteraction.bCreateLocalProps
		IF bDeletePrevious
			AnimData.NetworkObjectIDPrevious = AnimData.NetworkObjectID
		ENDIF
		
		IF CREATE_NET_OBJ(AnimData.NetworkObjectID, PropModel, GET_PED_BONE_COORDS(AnimData.PedID, BONETAG_PH_R_HAND, <<0,0,0>>), FALSE, TRUE,FALSE,FALSE)
			AnimData.ObjectID = NET_TO_OBJ(AnimData.NetworkObjectID)			
			NET_PRINT("[interactions] CreatePropForInteraction - network prop created") NET_NL()
			
			// NeilF: we don't want our interaction props to migrate as they might never get cleaned up. url:bugstar:7421456
			SET_NETWORK_ID_CAN_MIGRATE(AnimData.NetworkObjectID, FALSE)
			
			RETURN(TRUE)
		ELSE
			NET_PRINT("[interactions] CreatePropForInteraction - failed to create network prop!") NET_NL()
			RETURN(FALSE)
		ENDIF
	ENDIF
	
	IF MPGlobalsInteractions.PlayerInteraction.bCreateLocalProps
		IF bDeletePrevious
			AnimData.ObjectIDPrevious = AnimData.ObjectID
		ENDIF
	ENDIF
	
	AnimData.ObjectID = CREATE_OBJECT(PropModel, GET_PED_BONE_COORDS(AnimData.PedID, BONETAG_PH_R_HAND, <<0,0,0>>), FALSE, FALSE)
	NET_PRINT("[interactions] CreatePropForInteraction - non-network prop created") NET_NL()
	IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(AnimData.PedID)
		SET_ENTITY_VISIBLE(AnimData.ObjectID, FALSE)
		NET_PRINT("[interactions] CreatePropForInteraction - ped is invisible so make prop invisible too.") NET_NL()
	ENDIF
	RETURN TRUE
ENDFUNC

PROC UpdateAnimSounds(INTERACTION_ANIM_DATA &AnimData)

	IF (AnimData.Data.bUseSounds)
	
		BOOL bIsFullBody
		
		IF (AnimData.AnimStage = IAS_FULL_BODY)
			bIsFullBody = TRUE
		ELSE
			bIsFullBody = FALSE		
		ENDIF
		
		STRING animDict = InteractionAnimDictFromInteractionAnimData(AnimData, bIsFullBody) //GetInteractionsAnimsDict(InteractionData, Data.iType, Data.bIsDriverSide, Data.bIsLowVehicle, Data.bIsRearSeat, Data.bIsSideFacing, Data.bIsDriver, Data.bIsBohdi)

		GET_INTERACTION_FINER_SOUND_DETAILS(AnimData.iType, AnimData.iAnim, AnimData.bIsDriverSide, AnimData.bIsLowVehicle, AnimData.bIsRearSeat, AnimData.bIsSideFacing, AnimData.bIsDriver, AnimData.bIsBohdi, bIsFullBody, AnimData.SoundData, IS_PED_FEMALE(AnimData.PedID), AnimData.bFirstPerson )
	

//	// intro
//	IF (AnimData.SoundData.bPlayIntroSound)
//		IF (AnimData.bSelfieMode)
//			IF (AnimData.AnimStage = IAS_INTRO)
//				IF IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
//					IF GET_PHONE_GESTURE_ANIM_CURRENT_TIME (AnimData.PedID) > AnimData.SoundData.fIntroSoundTime
//						IF NOT (AnimData.SoundData.bHasPlayedIntroSound)
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strIntroSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")					
//							AnimData.SoundData.bHasPlayedIntroSound = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedIntroSound)
//							AnimData.SoundData.bHasPlayedIntroSound = FALSE
//						ENDIF
//					ENDIF			
//				ENDIF
//			ENDIF
//		ELSE
//			IF NOT (bIsFullBody)
//				IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, animDict, AnimData.Data.strAnimNameIntro)	
//					IF GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, animDict, AnimData.Data.strAnimNameIntro) > AnimData.SoundData.fIntroSoundTime
//						IF NOT (AnimData.SoundData.bHasPlayedIntroSound)
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strIntroSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")					
//							AnimData.SoundData.bHasPlayedIntroSound = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedIntroSound)
//							AnimData.SoundData.bHasPlayedIntroSound = FALSE
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, animDict, AnimData.Data.strAnimNameFullBody)	
//					IF GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, animDict, AnimData.Data.strAnimNameFullBody) > AnimData.SoundData.fIntroSoundTime
//						IF NOT (AnimData.SoundData.bHasPlayedIntroSound)
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strIntroSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")					
//							AnimData.SoundData.bHasPlayedIntroSound = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedIntroSound)
//							AnimData.SoundData.bHasPlayedIntroSound = FALSE
//						ENDIF
//					ENDIF
//				ENDIF			
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	// loop
//	IF (AnimData.SoundData.bPlayLoopSound)
//		IF (AnimData.bSelfieMode)
//			IF (AnimData.AnimStage = IAS_BODY)
//				IF IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
//					IF GET_PHONE_GESTURE_ANIM_CURRENT_TIME (AnimData.PedID) > AnimData.SoundData.fLoopSoundTime
//						IF NOT (AnimData.SoundData.bHasPlayedLoopSound)					
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strLoopSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")
//							AnimData.SoundData.bHasPlayedLoopSound = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedLoopSound)
//							AnimData.SoundData.bHasPlayedLoopSound = FALSE
//						ENDIF
//					ENDIF			
//				ENDIF
//			ENDIF
//		ELSE	
//			IF NOT (bIsFullBody)
//				IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, animDict, AnimData.Data.strAnimName)	
//					IF GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, animDict, AnimData.Data.strAnimName) > AnimData.SoundData.fLoopSoundTime
//						IF NOT (AnimData.SoundData.bHasPlayedLoopSound)					
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strLoopSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")
//							AnimData.SoundData.bHasPlayedLoopSound = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedLoopSound)
//							AnimData.SoundData.bHasPlayedLoopSound = FALSE
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, animDict, AnimData.Data.strAnimNameFullBody)	
//					IF GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, animDict, AnimData.Data.strAnimNameFullBody) > AnimData.SoundData.fLoopSoundTime
//						IF NOT (AnimData.SoundData.bHasPlayedLoopSound)					
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strLoopSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")
//							AnimData.SoundData.bHasPlayedLoopSound = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedLoopSound)
//							AnimData.SoundData.bHasPlayedLoopSound = FALSE
//						ENDIF
//					ENDIF
//				ENDIF			
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	// secondary loop sound
//	// loop
//	IF (AnimData.SoundData.bPlayLoopSound2)
//		IF (AnimData.bSelfieMode)
//			IF (AnimData.AnimStage = IAS_BODY)
//				IF IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
//					IF GET_PHONE_GESTURE_ANIM_CURRENT_TIME (AnimData.PedID) > AnimData.SoundData.fLoopSoundTime2
//						IF NOT (AnimData.SoundData.bHasPlayedLoopSound2)					
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strLoopSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")
//							AnimData.SoundData.bHasPlayedLoopSound2 = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedLoopSound2)
//							AnimData.SoundData.bHasPlayedLoopSound2 = FALSE
//						ENDIF
//					ENDIF			
//				ENDIF
//			ENDIF
//		ELSE		
//			IF NOT (bIsFullBody)
//				IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, animDict, AnimData.Data.strAnimName)	
//					IF GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, animDict, AnimData.Data.strAnimName) > AnimData.SoundData.fLoopSoundTime2
//						IF NOT (AnimData.SoundData.bHasPlayedLoopSound2)					
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strLoopSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")
//							AnimData.SoundData.bHasPlayedLoopSound2 = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedLoopSound2)
//							AnimData.SoundData.bHasPlayedLoopSound2 = FALSE
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, animDict, AnimData.Data.strAnimNameFullBody)	
//					IF GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, animDict, AnimData.Data.strAnimNameFullBody) > AnimData.SoundData.fLoopSoundTime2
//						IF NOT (AnimData.SoundData.bHasPlayedLoopSound2)					
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strLoopSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")
//							AnimData.SoundData.bHasPlayedLoopSound2 = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedLoopSound2)
//							AnimData.SoundData.bHasPlayedLoopSound2 = FALSE
//						ENDIF
//					ENDIF
//				ENDIF			
//			ENDIF
//		ENDIF
//	ENDIF	
//	
//	// outro
//	IF (AnimData.SoundData.bPlayOutroSound)
//		IF (AnimData.bSelfieMode)
//			IF (AnimData.AnimStage = IAS_OUTRO)
//				IF IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
//					IF GET_PHONE_GESTURE_ANIM_CURRENT_TIME (AnimData.PedID) > AnimData.SoundData.fOutroSoundTime
//						IF NOT (AnimData.SoundData.bHasPlayedOutroSound)				
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strOutroSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")
//							AnimData.SoundData.bHasPlayedOutroSound = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedOutroSound)
//							AnimData.SoundData.bHasPlayedOutroSound = FALSE
//						ENDIF
//					ENDIF		
//				ENDIF
//			ENDIF
//		ELSE	
//			IF NOT (bIsFullBody)
//				IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, animDict, AnimData.Data.strAnimNameOutro)	
//					IF GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, animDict, AnimData.Data.strAnimNameOutro) > AnimData.SoundData.fOutroSoundTime
//						IF NOT (AnimData.SoundData.bHasPlayedOutroSound)				
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strOutroSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")
//							AnimData.SoundData.bHasPlayedOutroSound = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedOutroSound)
//							AnimData.SoundData.bHasPlayedOutroSound = FALSE
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, animDict, AnimData.Data.strAnimNameFullBody)	
//					IF GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, animDict, AnimData.Data.strAnimNameFullBody) > AnimData.SoundData.fOutroSoundTime
//						IF NOT (AnimData.SoundData.bHasPlayedOutroSound)				
//							PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strOutroSound, AnimData.PedID, "MP_SNACKS_SOUNDSET")
//							AnimData.SoundData.bHasPlayedOutroSound = TRUE
//						ENDIF
//					ELSE
//						IF (AnimData.SoundData.bHasPlayedOutroSound)
//							AnimData.SoundData.bHasPlayedOutroSound = FALSE
//						ENDIF
//					ENDIF
//				ENDIF			
//			ENDIF
//		ENDIF
//	ENDIF	

		INT i 
		TEXT_LABEL_63 strAnimName
		FLOAT fCurrentPhase
		FLOAT fNextPhase
		REPEAT MAX_NUMBER_OF_SOUNDS i
			IF (AnimData.SoundData.bPlaySound[i])
				IF (AnimData.bSelfieMode)
					
					IF (AnimData.AnimStage = AnimData.SoundData.SoundAnimStage[i])
						IF IsPedUsingSelfieTask(AnimData.PedID)
						AND IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
								
							fCurrentPhase = GET_PHONE_GESTURE_ANIM_CURRENT_TIME(AnimData.PedID) 
							IF (fCurrentPhase > AnimData.SoundData.fLastPhase)
							AND (AnimData.SoundData.fLastPhase >= 0.0)
								fNextPhase = fCurrentPhase + (fCurrentPhase - AnimData.SoundData.fLastPhase)
							ENDIF
														
							IF fNextPhase >= AnimData.SoundData.fSoundTime[i]
								IF NOT (AnimData.SoundData.bHasPlayedSound[i])				
									NET_PRINT("[interactions] UpdateAnimSounds - playing sound (selfie) ") NET_PRINT_INT(i) 
									NET_PRINT(", fSoundTime = ") NET_PRINT_FLOAT(AnimData.SoundData.fSoundTime[i])
									NET_PRINT(", strSound = ") NET_PRINT(AnimData.SoundData.strSound[i])
									NET_PRINT(", fLastPhase = ") NET_PRINT_FLOAT(AnimData.SoundData.fLastPhase)
									NET_PRINT(", fCurrentPhase = ") NET_PRINT_FLOAT(fCurrentPhase)
									NET_PRINT(", fNextPhase = ") NET_PRINT_FLOAT(fNextPhase)
									NET_NL()
								
									PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strSound[i], AnimData.PedID, "MP_SNACKS_SOUNDSET")
									
									AnimData.SoundData.bHasPlayedSound[i] = TRUE
								ENDIF
							ELSE
								IF (AnimData.SoundData.bHasPlayedSound[i])
									AnimData.SoundData.bHasPlayedSound[i] = FALSE
								ENDIF
							ENDIF		
							AnimData.SoundData.fLastPhase = fCurrentPhase
								
							
						ENDIF
					ENDIF
					
				ELSE	

					SWITCH AnimData.SoundData.SoundAnimStage[i]
						CASE IAS_INTRO
							strAnimName = AnimData.Data.strAnimNameIntro
						BREAK
						CASE IAS_BODY
							strAnimName = AnimData.Data.strAnimName
						BREAK
						CASE IAS_OUTRO
							strAnimName = AnimData.Data.strAnimNameOutro
						BREAK			
						CASE IAS_FULL_BODY
							strAnimName = AnimData.Data.strAnimNameFullBody
						BREAK
					ENDSWITCH
					
					IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, animDict, strAnimName)	
					
						fCurrentPhase = GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, animDict, strAnimName) 
						IF (fCurrentPhase > AnimData.SoundData.fLastPhase)
						AND (AnimData.SoundData.fLastPhase >= 0.0)
							fNextPhase = fCurrentPhase + (fCurrentPhase - AnimData.SoundData.fLastPhase)
						ENDIF

						
						IF fNextPhase >= AnimData.SoundData.fSoundTime[i]
							IF NOT (AnimData.SoundData.bHasPlayedSound[i])
								NET_PRINT("[interactions] UpdateAnimSounds - playing sound ") NET_PRINT_INT(i) 
								NET_PRINT(", fSoundTime = ") NET_PRINT_FLOAT(AnimData.SoundData.fSoundTime[i])
								NET_PRINT(", strSound = ") NET_PRINT(AnimData.SoundData.strSound[i])
								NET_PRINT(", strAnimName = ") NET_PRINT(strAnimName) 
								NET_PRINT(", fLastPhase = ") NET_PRINT_FLOAT(AnimData.SoundData.fLastPhase)
								NET_PRINT(", fCurrentPhase = ") NET_PRINT_FLOAT(fCurrentPhase)
								NET_PRINT(", fNextPhase = ") NET_PRINT_FLOAT(fNextPhase)								
								NET_NL()
								
								PLAY_SOUND_FROM_ENTITY(-1, AnimData.SoundData.strSound[i], AnimData.PedID, "MP_SNACKS_SOUNDSET")					
							
								AnimData.SoundData.bHasPlayedSound[i] = TRUE
							ENDIF
						ELSE
							IF (AnimData.SoundData.bHasPlayedSound[i])
								AnimData.SoundData.bHasPlayedSound[i] = FALSE
							ENDIF
						ENDIF
						AnimData.SoundData.fLastPhase = fCurrentPhase
						
					ENDIF	
					
				ENDIF
			ENDIF			
		ENDREPEAT

	ENDIF

ENDPROC

PROC AttachPropToPed(INTERACTION_ANIM_DATA &AnimData)
	NET_PRINT("[interactions] AttachPropToPed - called...") NET_NL()
	
	IF IS_ENTITY_ATTACHED(AnimData.ObjectID)
		DETACH_ENTITY(AnimData.ObjectID, FALSE)
		NET_PRINT("[interactions] AttachPropToPed - already attached, detaching.") NET_NL()
	ENDIF
	
	IF (IS_PED_IN_ANY_VEHICLE(AnimData.PedID)
	OR IsPlayingFullBody(AnimData)
	OR AnimData.Data.bForceRightHandAttach)
	AND NOT AnimData.Data.bForceLeftHandAttach
		ATTACH_ENTITY_TO_ENTITY(AnimData.ObjectID, AnimData.PedID, GET_PED_BONE_INDEX(AnimData.PedID,BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE)	
		NET_PRINT("[interactions] AttachPropToPed - attaching to BONETAG_PH_R_HAND.") NET_NL()
	ELSE
		ATTACH_ENTITY_TO_ENTITY(AnimData.ObjectID, AnimData.PedID, GET_PED_BONE_INDEX(AnimData.PedID,BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE)	
		NET_PRINT("[interactions] AttachPropToPed - attaching to BONETAG_PH_L_HAND.") NET_NL()
	ENDIF
ENDPROC

FUNC FLOAT GetCurrentAnimPhase(INTERACTION_ANIM_DATA &AnimData)
	
	BOOL bIsFullBody
	IF (AnimData.AnimStage = IAS_FULL_BODY)
		bIsFullBody = TRUE
		NET_PRINT("[interactions] GetCurrentAnimPhase bIsFullBody = TRUE ") NET_NL()
	ENDIF

	STRING AnimDict = InteractionAnimDictFromInteractionAnimData(AnimData, bIsFullBody, AnimData.bSelfieMode)
	STRING AnimName = GetCurrentAnimName(AnimData)	
	FLOAT fReturn = 0.0
	
	NET_PRINT("[interactions] GetCurrentAnimPhase - AnimDict = ") NET_PRINT(AnimDict) NET_PRINT(", AnimName = ") NET_PRINT(AnimName) NET_NL()
	
	IF (AnimData.bSelfieMode)	
		IF IsPedUsingSelfieTask(AnimData.PedID)
			IF IsPedUsingSelfieTask(AnimData.PedID)
			AND IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
				fReturn = GET_PHONE_GESTURE_ANIM_CURRENT_TIME(AnimData.PedID)
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_PLAYING_ANIM(AnimData.PedID, AnimDict, AnimName)
			fReturn = GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, AnimDict, AnimName) 	
		ENDIF
	ENDIF
	// anim has just started this frame so use that time.
	IF (fReturn < 0.0)
		fReturn = AnimData.fStartPhase	
	ENDIF
	
	PRINTLN("[interactions] GetCurrentAnimPhase - returning ", fReturn)
	
	RETURN fReturn
ENDFUNC

PROC UpdateAnimPropsAndVFX(INTERACTION_ANIM_DATA &AnimData, INTERACTION_GLOBAL_DATA &GlobalData)

	BOOL bRelease = FALSE
	BOOL bReattach = FALSE

	IF (AnimData.Data.bHasProp)
	
		// create prop
		//IF NOT (AnimData.bCreateProp)
			IF HAS_ANIM_EVENT_FIRED ( AnimData.PedID, GET_HASH_KEY("CREATE"))
			OR HAS_ANIM_EVENT_FIRED ( AnimData.PedID, GET_HASH_KEY("GripCash_L"))
			OR HAS_ANIM_EVENT_FIRED ( AnimData.PedID, GET_HASH_KEY("GripBottle"))
			OR HAS_ANIM_EVENT_FIRED ( AnimData.PedID, GET_HASH_KEY("Create_whiskey_bottle"))
			OR (AnimData.bForceProp)
			//OR (TRUE)
				AnimData.bHasDecrementedStat = FALSE
				IF NOT (AnimData.bCreateProp)
					bReattach = TRUE
					AnimData.bCreateProp = TRUE
					AnimData.bForceProp = FALSE
				ENDIF
				NET_PRINT("[interactions] UpdateAnimPropsAndVFX - CREATE event triggered") NET_NL()	
			ENDIF
		//ENDIF
		
		// delete prop
		IF NOT (AnimData.bDeleteProp)
			IF HAS_ANIM_EVENT_FIRED ( AnimData.PedID, GET_HASH_KEY("DELETE")) 
			OR HAS_ANIM_EVENT_FIRED ( AnimData.PedID, GET_HASH_KEY("RELEASE"))
			OR HAS_ANIM_EVENT_FIRED	( AnimData.PedID, GET_HASH_KEY("ReleaseCash"))
			OR HAS_ANIM_EVENT_FIRED	( AnimData.PedID, GET_HASH_KEY("ReleaseBottle"))
				AnimData.bDeleteProp = TRUE
				IF HAS_ANIM_EVENT_FIRED ( AnimData.PedID, GET_HASH_KEY("RELEASE"))
					NET_PRINT("[interactions] UpdateAnimPropsAndVFX - RELEASE event triggered") NET_NL()
					bRelease = TRUE
				ENDIF
				
				AnimData.bForceProp = FALSE
				AnimData.bCreateProp = FALSE
				
				NET_PRINT("[interactions] UpdateAnimPropsAndVFX - DESTROY event triggered.") NET_NL()
			ENDIF
		ENDIF
		
		// actually create prop
		IF (AnimData.bCreateProp)
			IF NOT (AnimData.bObjectCreated)
				IF NOT DOES_ENTITY_EXIST(AnimData.ObjectID)
					IF HasLoadedInteractionProps(AnimData)

						IF IS_STAT_DEPENDANT(AnimData.iType, AnimData.iAnim)
						AND NOT (GlobalData.bIgnoreStatCheck)
							IF NOT (AnimData.bHasDecrementedStat)
								IF (GET_MP_INT_CHARACTER_STAT(GET_DEPENDANT_STAT_FOR_INTERACTION(AnimData.iType, AnimData.iAnim)) > 0)
									DECREMENT_MP_INT_CHARACTER_STAT(GET_DEPENDANT_STAT_FOR_INTERACTION(AnimData.iType, AnimData.iAnim))
								ELSE
									NET_PRINT("[interactions] UpdateAnimPropsAndVFX - not decrementing stat as already zero") NET_NL()		
								ENDIF
								CAN_PICKUP_ITEM_CHECK(AnimData.Data.PropModel)
								AnimData.bHasDecrementedStat = TRUE
								NET_PRINT("[interactions] UpdateAnimPropsAndVFX - decrementing stat") NET_NL()	
							ENDIF
						ENDIF					
					
						IF CreatePropForInteraction(AnimData, AnimData.Data.PropModel)
						AND DOES_ENTITY_EXIST(AnimData.ObjectID)
							AttachPropToPed(AnimData)
							
							DISABLE_CAM_COLLISION_FOR_OBJECT(AnimData.ObjectID)
							SET_ENTITY_NO_COLLISION_ENTITY(AnimData.ObjectID, AnimData.PedID, FALSE)
							
							// ptfx
							IF (AnimData.Data.bPropHasAttachedPTFX)
								USE_NAMED_INTERACTION_PTFX_ASSET(AnimData)
								AnimData.PTFXID_Loop = START_PARTICLE_FX_LOOPED_ON_ENTITY(AnimData.Data.strPropPTFX, AnimData.ObjectID, AnimData.Data.vPropPTFXOffset, AnimData.Data.vPropPTFXRotation)
								NET_PRINT("[interactions] called START_PARTICLE_FX_LOOPED_ON_ENTITY - AnimData.PTFXID_Loop = ") NET_PRINT_INT(NATIVE_TO_INT(AnimData.PTFXID_Loop)) NET_NL()
								
								// if this is a smoking interaction we need to broadcast as pfx arn't synced across the network.
								IF (AnimData.PedID = PLAYER_PED_ID())
									IF (AnimData.iAnim = ENUM_TO_INT(PASSENGER_INTERACTION_SMOKE))
									OR (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_SMOKE))
										SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlaySmokingPFX)
										NET_PRINT("[interactions] bPlaySmokingPFX = TRUE 1 ") NET_NL()
									ENDIF
								ENDIF
							ENDIF
							
							AnimData.bForceProp = FALSE
							AnimData.bObjectCreated = TRUE
							NET_PRINT("[interactions] UpdateAnimPropsAndVFX - created prop") NET_NL()	

						ENDIF
						
						
					ENDIF
				ELSE
					NET_PRINT("[interactions] UpdateAnimPropsAndVFX - object already exist") NET_NL()	
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(AnimData.ObjectID)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						AnimData.bObjectCreated = FALSE
						NET_PRINT("[interactions] UpdateAnimPropsAndVFX - object doesn't exist - resetting bObjectCreated") NET_NL()
					ENDIF
				ELSE
					// reattach 
					IF (bReattach)
						NET_PRINT("[interactions] UpdateAnimPropsAndVFX - bReattach = TRUE") NET_NL()
						AttachPropToPed(AnimData)							
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// actually delete ciggy
		IF (AnimData.bDeleteProp)
			IF (AnimData.bObjectCreated)
				IF DOES_ENTITY_EXIST(AnimData.ObjectID)
					IF IsSafeToDeleteProp(AnimData)
						DeleteProp(AnimData, bRelease, AnimData.Data.bDeletePropAtEnd)	
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF (AnimData.bObjectCreated)
				IF DOES_ENTITY_EXIST(AnimData.ObjectID)	
					AnimData.vObjectCoordsLastFrame = GET_ENTITY_COORDS(AnimData.ObjectID, FALSE)
				ENDIF
			ENDIF
		ENDIF	
		
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_MAKE_IT_RAIN))
			IF HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("GripCash_M"))
				AnimData.bSwappedMidItem = FALSE
			ENDIF
		ENDIF
		
		IF NOT AnimData.bSwappedMidItem
			IF HasLoadedInteractionProps(AnimData)
			AND CreatePropForInteraction(AnimData, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xs_Prop_Arena_Cash_Pile_M")), TRUE)
			AND DOES_ENTITY_EXIST(AnimData.ObjectID)
				AttachPropToPed(AnimData)
				
				DISABLE_CAM_COLLISION_FOR_OBJECT(AnimData.ObjectID)
				SET_ENTITY_NO_COLLISION_ENTITY(AnimData.ObjectID, AnimData.PedID, FALSE)
				
				AnimData.bSwappedMidItem = TRUE
			ENDIF
		ELSE
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(AnimData.NetworkObjectID)
			AND NETWORK_DOES_NETWORK_ID_EXIST(AnimData.NetworkObjectIDPrevious)
				DELETE_NET_ID(AnimData.NetworkObjectIDPrevious)
			ENDIF
			IF DOES_ENTITY_EXIST(AnimData.ObjectIDPrevious)
				DELETE_OBJECT(AnimData.ObjectIDPrevious)
			ENDIF
		ENDIF
		
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_DRINK_3))
			IF HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("OpenBottle"))
				AnimData.bSwappedLastItem = FALSE
			ENDIF
		ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_MAKE_IT_RAIN))
			IF HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("GripCash_S"))
				AnimData.bSwappedLastItem = FALSE
			ENDIF
		#IF FEATURE_COPS_N_CROOKS
		ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_EAT_DOUGHNUT))
			IF HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("halfdoughnut"))
				AnimData.bSwappedLastItem = FALSE
			ENDIF
		#ENDIF	
		ENDIF
		
		IF NOT AnimData.bSwappedLastItem
			MODEL_NAMES eLastModel
			
			IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_DRINK_3))
				eLastModel = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Champ_Open"))
			ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_MAKE_IT_RAIN))
				eLastModel = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Cash_Pile_S"))
			#IF FEATURE_COPS_N_CROOKS
			ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_POUR_ONE_OUT))
				eLastModel = INT_TO_ENUM(MODEL_NAMES, HASH("ac_prop_ac_Whiskey_Bottle_S"))
			ELIF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_EAT_DOUGHNUT))
				eLastModel = INT_TO_ENUM(MODEL_NAMES, HASH("ac_prop_ac_doughnut_02a"))
			#ENDIF	
			ENDIF
			
			IF HasLoadedInteractionProps(AnimData)
			AND CreatePropForInteraction(AnimData, eLastModel, TRUE)
			AND DOES_ENTITY_EXIST(AnimData.ObjectID)
				AttachPropToPed(AnimData)
				
				DISABLE_CAM_COLLISION_FOR_OBJECT(AnimData.ObjectID)
				SET_ENTITY_NO_COLLISION_ENTITY(AnimData.ObjectID, AnimData.PedID, FALSE)
				
				AnimData.bSwappedLastItem = TRUE
			ENDIF
		ELSE
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(AnimData.NetworkObjectID)
			AND NETWORK_DOES_NETWORK_ID_EXIST(AnimData.NetworkObjectIDPrevious)
				DELETE_NET_ID(AnimData.NetworkObjectIDPrevious)
			ENDIF
			
			IF DOES_ENTITY_EXIST(AnimData.ObjectIDPrevious)
				DELETE_OBJECT(AnimData.ObjectIDPrevious)
			ENDIF	
		ENDIF
		
		// play the loop event
		IF HAS_ANIM_EVENT_FIRED ( AnimData.PedID, GET_HASH_KEY("LOOP_EVENT"))
		OR HAS_ANIM_EVENT_FIRED ( AnimData.PedID, GET_HASH_KEY("EXHALE"))	
			
			// mouth and nose ptfx
			IF (AnimData.Data.bHasLoopMouthPTFX)
				USE_NAMED_INTERACTION_PTFX_ASSET(AnimData)
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(AnimData.Data.strLoopPTFXMouth, AnimData.PedID, <<-0.02,0.13,0.0>>, <<0.0, 0.0, 0.0>>, BONETAG_HEAD)
				NET_PRINT("[interactions] UpdateAnimPropsAndVFX - loop event - START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE bHasLoopMouthPTFX") NET_NL()
			ENDIF
			IF (AnimData.Data.bHasLoopNosePTFX)
				USE_NAMED_INTERACTION_PTFX_ASSET(AnimData)
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(AnimData.Data.strLoopPTFXNose, AnimData.PedID, <<0.02,0.16,0.0>>, <<0.0, 0.0, 0.0>>, BONETAG_HEAD)
				NET_PRINT("[interactions] UpdateAnimPropsAndVFX - loop event - START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE bHasLoopNosePTFX") NET_NL()
			ENDIF
			
			// decrement/increment player's health
			IF NOT (GET_LOOP_EVENT_HEALTH(AnimData.iType, AnimData.iAnim) = 0)	
				IF IsPedPlayerPed(AnimData.PedID)
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						INT iCurrentHealth = GET_ENTITY_HEALTH(AnimData.PedID)	
						INT iThisHealthChange = ROUND(TO_FLOAT(GET_LOOP_EVENT_HEALTH(AnimData.iType, AnimData.iAnim)) * GET_HEALTH_TUNABLE_MULTIPLIER(AnimData.iType, AnimData.iAnim))
						GlobalData.iHealthChange += iThisHealthChange
						iCurrentHealth += iThisHealthChange
						IF (iCurrentHealth < 0)
							iCurrentHealth = 0
						ENDIF
						NET_PRINT("[interactions] UpdateAnimPropsAndVFX - loop event - health altered by ") 
						NET_PRINT_INT(iThisHealthChange) 
						NET_PRINT(", new health = ") 
						NET_PRINT_INT(iCurrentHealth) 
						NET_NL()
						SET_ENTITY_HEALTH(PLAYER_PED_ID(), iCurrentHealth)		
					ELSE
						NET_PRINT("[interactions] UpdateAnimPropsAndVFX - player switch in progress, so not altering health.") NET_NL()
					ENDIF
				ENDIF
				GlobalData.iHealthChange += GET_LOOP_EVENT_HEALTH(AnimData.iType, AnimData.iAnim)
			ENDIF
			
			// decrement/increment player's drunkness	
			IF NOT (AnimData.Data.iLoopEventDrunk = 0)	
				GlobalData.iCurrentAlcoholShots++				
				// every 3 gulps counts as an alcohol shot
				IF (GlobalData.iCurrentAlcoholShots >= AnimData.Data.iLoopEventDrunk)
					IF IsPedPlayerPed(AnimData.PedID)
						IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
							Player_Takes_Alcohol_Hit(AnimData.PedID)
						ELSE
							NET_PRINT("[interactions] UpdateAnimPropsAndVFX - player switch in progress, so not giving alcohol hit.") NET_NL()
						ENDIF
					ELSE
						Make_Ped_Drunk(AnimData.PedID, HIGHEST_INT)
					ENDIF
					GlobalData.iAlcoholShots++
					GlobalData.iCurrentAlcoholShots = 0
				ENDIF
			ENDIF
				

			// increment 
			IF IsPedPlayerPed(AnimData.PedID)
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					AnimData.iIteration += 1
				ELSE
					NET_PRINT("[interactions] UpdateAnimPropsAndVFX - player switch in progress, so not updating iIteration.") NET_NL()
				ENDIF
			ELSE
				AnimData.iIteration += 1
			ENDIF
			
			// check if reached max num of iterations
			IF AnimData.iIteration >= GET_MAX_LOOP_ITERATIONS(AnimData.iType, AnimData.iAnim) 
				AnimData.bHitMaxIterations = TRUE	
			ENDIF
			
			NET_PRINT("[interactions] UpdateAnimPropsAndVFX - loop event processed") NET_NL()	
		ENDIF
		
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_DRINK_3))
			IF (AnimData.PTFXID_Loop = INT_TO_NATIVE(PTFX_ID, -1))
			AND DOES_ENTITY_EXIST(AnimData.ObjectID)
				IF HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("OpenBottle"))
					IF IS_ENTITY_ATTACHED_TO_ANY_PED(AnimData.ObjectID)
					AND DOES_ENTITY_HAVE_DRAWABLE(AnimData.ObjectID)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayChampagnePFX)
						
						USE_NAMED_INTERACTION_PTFX_ASSET(AnimData)
						AnimData.PTFXID_Loop = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_ba_club_champagne_spray", AnimData.ObjectID, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, GET_ENTITY_BONE_INDEX_BY_NAME(AnimData.ObjectID, "VFX"))
					ENDIF
				ENDIF
			ENDIF
			
			IF AnimData.AnimStage = IAS_OUTRO
			OR (GlobalData.bPlayInteractionAnimFullBody AND GetCurrentAnimPhase(AnimData) >= 0.5)
				IF ((AnimData.PTFXID_Loop != INT_TO_NATIVE(PTFX_ID, -1)) AND DOES_PARTICLE_FX_LOOPED_EXIST(AnimData.PTFXID_Loop))
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayChampagnePFX)
					STOP_PARTICLE_FX_LOOPED(AnimData.PTFXID_Loop)
				ENDIF
			ENDIF
		ENDIF
		
		#IF FEATURE_COPS_N_CROOKS
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_POUR_ONE_OUT))
			IF (AnimData.PTFXID_Loop = INT_TO_NATIVE(PTFX_ID, -1))
			AND DOES_ENTITY_EXIST(AnimData.ObjectID)
				IF HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("vfx_arc_pour_out_start"))
					IF IS_ENTITY_ATTACHED_TO_ANY_PED(AnimData.ObjectID)
					AND DOES_ENTITY_HAVE_DRAWABLE(AnimData.ObjectID)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayWhiskeyPFX)
						USE_NAMED_INTERACTION_PTFX_ASSET(AnimData)
						AnimData.PTFXID_Loop = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_arc_cc_pour_out", AnimData.ObjectID, <<0.010, -0.045, -0.03>>, <<90.0, 0.0, 0.0>>, GET_ENTITY_BONE_INDEX_BY_NAME(AnimData.ObjectID, "P_Whiskey_Bottle_S_b0"))
						NET_PRINT("[interactions] UpdateAnimPropsAndVFX - PIBD_PlayWhiskeyPFX true") NET_NL()	
					ENDIF
				ENDIF
			ENDIF
			
			IF AnimData.AnimStage = IAS_OUTRO
			OR (GlobalData.bPlayInteractionAnimFullBody AND GetCurrentAnimPhase(AnimData) >= 0.5)
			OR HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("vfx_arc_pour_out_end"))
				IF ((AnimData.PTFXID_Loop != INT_TO_NATIVE(PTFX_ID, -1)) AND DOES_PARTICLE_FX_LOOPED_EXIST(AnimData.PTFXID_Loop))
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayWhiskeyPFX)
					NET_PRINT("[interactions] UpdateAnimPropsAndVFX - PIBD_PlayWhiskeyPFX false") NET_NL()	
					STOP_PARTICLE_FX_LOOPED(AnimData.PTFXID_Loop)
				ENDIF
			ENDIF
		ENDIF
		#ENDIF
				
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_MAKE_IT_RAIN))
			IF (AnimData.PTFXID_Loop = INT_TO_NATIVE(PTFX_ID, -1))
			AND DOES_ENTITY_EXIST(AnimData.ObjectID)
				IF HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("vfx_xs_raining_cash_start"))
					IF IS_ENTITY_ATTACHED_TO_ANY_PED(AnimData.ObjectID)
					AND DOES_ENTITY_HAVE_DRAWABLE(AnimData.ObjectID)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayMakeItRainPFX)
						
						USE_NAMED_INTERACTION_PTFX_ASSET(AnimData)
						AnimData.PTFXID_Loop = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_xs_money_rain", PLAYER_PED_ID(), <<0.0, 0.0, 0.0>>, <<0.0, 90.0, 0.0>>, GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND))
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("vfx_xs_raining_cash_stop"))
				IF ((AnimData.PTFXID_Loop != INT_TO_NATIVE(PTFX_ID, -1)) AND DOES_PARTICLE_FX_LOOPED_EXIST(AnimData.PTFXID_Loop))
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayMakeItRainPFX)
					STOP_PARTICLE_FX_LOOPED(AnimData.PTFXID_Loop)
				ENDIF
			ENDIF
		ENDIF
		
		// make prop invisible if ped smoking it alpha is low
		IF NOT (PLAYER_PED_ID() = AnimData.PedID)
			IF DOES_ENTITY_EXIST(AnimData.ObjectID)			
				NET_PRINT("[interactions] UpdateAnimPropsAndVFX - alpha hide - ped alpha = ") NET_PRINT_INT(GET_ENTITY_ALPHA(AnimData.PedID)) NET_NL()
				IF (GET_ENTITY_ALPHA(AnimData.PedID) < 255)
				OR NOT IS_ENTITY_VISIBLE_TO_SCRIPT(AnimData.PedID)
					SET_ENTITY_ALPHA(AnimData.ObjectID, 0, FALSE)
					IF ((AnimData.PTFXID_Loop != INT_TO_NATIVE(PTFX_ID, -1)) AND DOES_PARTICLE_FX_LOOPED_EXIST(AnimData.PTFXID_Loop))
						STOP_PARTICLE_FX_LOOPED(AnimData.PTFXID_Loop)
						NET_PRINT("[interactions] UpdateAnimPropsAndVFX - alpha hide - stopping pfx") NET_NL()
					ELSE
						NET_PRINT("[interactions] UpdateAnimPropsAndVFX - alpha hide - pfx does not exist, AnimData.PTFXID_Loop = ") NET_PRINT_INT(NATIVE_TO_INT(AnimData.PTFXID_Loop)) NET_NL()	
					ENDIF
				ELSE
					IF (GET_ENTITY_ALPHA(AnimData.ObjectID) = 0)
						SET_ENTITY_ALPHA(AnimData.ObjectID, 255, FALSE)
						
						// ptfx						
						IF (AnimData.Data.bPropHasAttachedPTFX)
							USE_NAMED_INTERACTION_PTFX_ASSET(AnimData)
							AnimData.PTFXID_Loop = START_PARTICLE_FX_LOOPED_ON_ENTITY(AnimData.Data.strPropPTFX, AnimData.ObjectID, AnimData.Data.vPropPTFXOffset, AnimData.Data.vPropPTFXRotation)
							NET_PRINT("[interactions] UpdateAnimPropsAndVFX - AnimData.PTFXID_Loop = ") NET_PRINT_INT(NATIVE_TO_INT(AnimData.PTFXID_Loop)) NET_NL()
						ENDIF					
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC



// ****************************************************************************************************************************************************************
// ****************************************************************************************************************************************************************
// 			COMMON FUNCTIONALITY FOR ALL INTERACTIONS
// ****************************************************************************************************************************************************************
// ****************************************************************************************************************************************************************


PROC EnsureInteractionAnimIsWithinRange(INTERACTION_GLOBAL_DATA &GlobalData)
	SWITCH INT_TO_ENUM(INTERACTION_ANIM_TYPES, GlobalData.iInteractionType)
		CASE INTERACTION_ANIM_TYPE_IN_CAR		
			IF (GlobalData.iInteractionAnim >= GET_MAX_PASSENGER_INTERACTIONS())
				GlobalData.iInteractionAnim = GET_MAX_PASSENGER_INTERACTIONS() - 1
			ENDIF
		BREAK
		CASE INTERACTION_ANIM_TYPE_CREW			
			IF (GlobalData.iInteractionAnim >= ENUM_TO_INT(MAX_CREW_INTERACTIONS))
				GlobalData.iInteractionAnim = ENUM_TO_INT(MAX_CREW_INTERACTIONS) - 1
			ENDIF
		BREAK
		CASE INTERACTION_ANIM_TYPE_PLAYER		
			IF (GlobalData.iInteractionAnim >= GET_MAX_PLAYER_INTERACTIONS())
				GlobalData.iInteractionAnim = GET_MAX_PLAYER_INTERACTIONS() - 1
			ENDIF
		BREAK 
	ENDSWITCH
ENDPROC


FUNC VEHICLE_SEAT GetVehicleSeatPedIsIn(PED_INDEX PedID)
	VEHICLE_INDEX CarID
	INT i
	VEHICLE_SEAT Seat
	IF NOT IS_PED_INJURED(PedID)
		IF IS_PED_IN_ANY_VEHICLE(PedID)
			CarID = GET_VEHICLE_PED_IS_IN(PedID)
			REPEAT (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(CarID)+1) i
				Seat = INT_TO_ENUM(VEHICLE_SEAT, i - 1)
				IF NOT IS_VEHICLE_SEAT_FREE(CarID, Seat )
					IF GET_PED_IN_VEHICLE_SEAT(CarID, Seat) = PedID
						NET_PRINT("[interactions] GetVehicleSeatPedIsIn - ped is in seat ") NET_PRINT_INT(ENUM_TO_INT(Seat)) NET_NL()
						RETURN(Seat)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	NET_PRINT("[interactions] GetVehicleSeatPedIsIn - fallback - ped is in seat ") NET_PRINT_INT(ENUM_TO_INT(Seat)) NET_NL()
	RETURN(Seat)		
ENDFUNC

FUNC BOOL IsSeatDriverSide(VEHICLE_SEAT Seat)
	SWITCH Seat
		CASE VS_DRIVER
		CASE VS_BACK_LEFT
		CASE VS_EXTRA_LEFT_1
		CASE VS_EXTRA_LEFT_2
		CASE VS_EXTRA_LEFT_3
			RETURN(TRUE)
		BREAK
	ENDSWITCH
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsSeatRear(VEHICLE_SEAT Seat)
	SWITCH Seat
		CASE VS_BACK_LEFT							
		CASE VS_BACK_RIGHT						
		CASE VS_EXTRA_LEFT_1
		CASE VS_EXTRA_RIGHT_1
		CASE VS_EXTRA_LEFT_2
		CASE VS_EXTRA_RIGHT_2
		CASE VS_EXTRA_LEFT_3
		CASE VS_EXTRA_RIGHT_3	
			RETURN(TRUE)
		BREAK
	ENDSWITCH
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsDriver(VEHICLE_INDEX CarID)
	IF NOT IS_VEHICLE_SEAT_FREE(CarID, VS_DRIVER )
		IF (GET_PED_IN_VEHICLE_SEAT(CarID, VS_DRIVER) = PLAYER_PED_ID())
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsPlayerSideFacing(VEHICLE_INDEX CarID)
	FLOAT fDiff

	fDiff = ABSF(GET_ENTITY_HEADING(CarID) - GET_ENTITY_HEADING(PLAYER_PED_ID()))
	IF (fDiff > 180.0)
		fDiff += -360.0
	ENDIF
	fDiff = ABSF(fDiff)			
	IF (fDiff > 40.0)
		RETURN(TRUE)
	ENDIF			


	RETURN(FALSE)
ENDFUNC

//#IF IS_DEBUG_BUILD
//FUNC STRING GetVehicleLayoutString(VEHICLE_INDEX VehicleID)
//	VEHICLE_LAYOUT_TYPE LayoutHash = INT_TO_ENUM(VEHICLE_LAYOUT_TYPE, GET_VEHICLE_LAYOUT_HASH(VehicleID))
//	SWITCH LayoutHash
//	    CASE LAYOUT_INVALID			RETURN ("LAYOUT_INVALID")
//	    CASE LAYOUT_4X4				RETURN ("LAYOUT_4X4")
//	    CASE LAYOUT_LOW				RETURN ("LAYOUT_LOW")
//	    CASE LAYOUT_LOW_BFINJECTION	RETURN ("LAYOUT_LOW_BFINJECTION")
//	    CASE LAYOUT_LOW_DUNE		RETURN ("LAYOUT_LOW_DUNE")
//	    CASE LAYOUT_LOW_INFERNUS 	RETURN ("LAYOUT_LOW_INFERNUS")
//		CASE LAYOUT_RANGER			RETURN ("LAYOUT_RANGER")
//	    CASE LAYOUT_STANDARD		RETURN ("LAYOUT_STANDARD")
//	    CASE LAYOUT_STD_HIGHWINDOW	RETURN ("LAYOUT_STD_HIGHWINDOW")
//	    CASE LAYOUT_STD_RIPLEY		RETURN ("LAYOUT_STD_RIPLEY")
//	    CASE LAYOUT_STD_STRETCH		RETURN ("LAYOUT_STD_STRETCH")
//	    CASE LAYOUT_STD_TOWTRUCK2	RETURN ("LAYOUT_STD_TOWTRUCK2")
//	    CASE LAYOUT_STD_ZTYPE		RETURN ("LAYOUT_STD_ZTYPE")
//	    CASE LAYOUT_VAN				RETURN ("LAYOUT_VAN")
//	    CASE LAYOUT_VAN_BODHI		RETURN ("LAYOUT_VAN_BODHI")
//	    CASE LAYOUT_VAN_EXTRA		RETURN ("LAYOUT_VAN_EXTRA")
//	    CASE LAYOUT_VAN_JOURNEY		RETURN ("LAYOUT_VAN_JOURNEY")
//	   	CASE LAYOUT_VAN_MULE		RETURN ("LAYOUT_VAN_MULE")
//	    CASE LAYOUT_VAN_TRASH	 	RETURN ("LAYOUT_VAN_TRASH")		
//	ENDSWITCH
//	RETURN("unknown")
//ENDFUNC
//#ENDIF

FUNC BOOL IsVehicleBohdiLayout(VEHICLE_INDEX VehicleID)
	
	SWITCH GET_VEHICLE_LAYOUT_HASH(VehicleID)
		CASE HASH("LAYOUT_VAN_BODHI")
		CASE HASH("LAYOUT_BISON")
		CASE HASH("LAYOUT_VAN")			
		CASE HASH("LAYOUT_VAN_BOXVILLE")	
		CASE HASH("LAYOUT_VAN_CADDY")	
		CASE HASH("LAYOUT_VAN_JOURNEY")	
		CASE HASH("LAYOUT_VAN_MULE")		
		CASE HASH("LAYOUT_VAN_POLICE")	
		CASE HASH("LAYOUT_VAN_PONY")		
		CASE HASH("LAYOUT_VAN_TRASH")
		CASE HASH("LAYOUT_VAN_ARMORED")	
		CASE HASH("LAYOUT_VAN_ROOSEVELT")
		CASE HASH("LAYOUT_VAN_RUMPO3")
		CASE HASH("LAYOUT_TRUCK_PHANTOMBULL")	
		CASE HASH("LAYOUT_TRUCK_WASTELANDER")
		CASE HASH("LAYOUT_VAN_CADDY3")	
		CASE HASH("LAYOUT_RANGER_RIATA")
		
		//mpBattle
		CASE HASH("LAYOUT_RANGER_PATRIOT2")	
		CASE HASH("LAYOUT_TRUCK_POUNDER2")	
		CASE HASH("LAYOUT_VAN_MULE4")	
		CASE HASH("LAYOUT_VAN_SPEEDO4")	
		CASE HASH("LAYOUT_VAN_FREECRAWLER")	
		CASE HASH("LAYOUT_TRUCK_TERBYTE")
		
		//mpArena_Wars
		CASE HASH("LAYOUT_RANGER_TOROS")	
		CASE HASH("LAYOUT_VAN_BRUTUS")
		
		//mpVinewood
		CASE HASH("LAYOUT_RANGER_CARACARA2")
		CASE HASH("LAYOUT_RANGER_NOVAK")
		
		//mpHeist3
		CASE HASH("LAYOUT_RANGER_EVERON")
		
		//mpSum
		CASE HASH("LAYOUT_RANGER_SEMINOLE2")
		
		//mpHeist4
		CASE HASH("LAYOUT_JEEP_WINKY")
		CASE HASH("LAYOUT_SLAMTRUCK")
		CASE HASH("LAYOUT_TRUCK_VETIR")
		CASE HASH("LAYOUT_SQUADDIE")
		CASE HASH("LAYOUT_VETO")

			RETURN TRUE
		BREAK
	ENDSWITCH	
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsPedInRearOfBohdiVehicleLayout(PED_INDEX PedID)
	IF IS_PED_IN_ANY_VEHICLE(PedID)
		VEHICLE_INDEX CarID
		VEHICLE_SEAT seat
		CarID = GET_VEHICLE_PED_IS_IN(PedID)
		seat = GetVehicleSeatPedIsIn(PedID)
		IF IsSeatRear(seat)
			RETURN IsVehicleBohdiLayout(CarID)			
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsVehicleFormulaLayout(VEHICLE_INDEX VehicleID)
	SWITCH GET_VEHICLE_LAYOUT_HASH(VehicleID)
		CASE HASH("LAYOUT_LOW_RACER_FORMULA")
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IsVehicleLow(VEHICLE_INDEX VehicleID)

	SWITCH GET_VEHICLE_LAYOUT_HASH(VehicleID)
	
		CASE HASH("LAYOUT_LOW")
		CASE HASH("LAYOUT_LOW_INFERNUS")
		CASE HASH("LAYOUT_LOW_DUNE")
		CASE HASH("LAYOUT_LOW_BFINJECTION")
		CASE HASH("LAYOUT_LOW_CHEETAH")
		CASE HASH("LAYOUT_LOW_ENTITYXF")
		CASE HASH("LAYOUT_LOW_RESTRICTED")
		CASE HASH("LAYOUT_LOW_SENTINEL2")
		CASE HASH("LAYOUT_LOW_TURISMOR")
		CASE HASH("LAYOUT_LOW_BLADE")
		CASE HASH("LAYOUT_LOW_FURORE")
		CASE HASH("LAYOUT_LOW_OSIRIS")
		CASE HASH("LAYOUT_LOW_LOWRIDER")	
		CASE HASH("LAYOUT_LOW_LOWRIDER_FACTION")	
		CASE HASH("LAYOUT_LOW_VERLIERER")
		CASE HASH("LAYOUT_LOW_LOWRIDER_FACTION3")
		CASE HASH("LAYOUT_LOW_LOWRIDER2_FACTION3")
		CASE HASH("LAYOUT_LOW_LOWRIDER2")
		CASE HASH("LAYOUT_LOW_PROTO")
		CASE HASH("LAYOUT_LOW_LE7B")
		CASE HASH("LAYOUT_LOW_RUINER2")
		CASE HASH("LAYOUT_LOW_INFERNUS2")
		CASE HASH("LAYOUT_LOW_RUSTON")
		CASE HASH("LAYOUT_LOW_TAMPA3")
		CASE HASH("LAYOUT_LOW_TORERO")
		CASE HASH("LAYOUT_LOW_VISIONE")
		CASE HASH("LAYOUT_LOW_VIGILANTE")
		CASE HASH("LAYOUT_LOW_SC1")
		CASE HASH("LAYOUT_LOW_STROMBERG")
		//mpAssault
		CASE HASH("LAYOUT_LOW_MICHELLI")
		CASE HASH("LAYOUT_LOW_TEZERACT")
		CASE HASH("LAYOUT_LOW_DOMINATOR3")
		CASE HASH("LAYOUT_LOW_TAIPAN")

		//mpBattle
		CASE HASH("LAYOUT_LOW_RESTRICTED_SCRAMJET")
		CASE HASH("LAYOUT_LOW_RESTRICTED_SWINGER")
		
		//mpChristmas2018
		CASE HASH("LAYOUT_LOW_ITALIGTO")
		CASE HASH("LAYOUT_LOW_RESTRICTED_SCHLAGEN")
		
		//mpVinewood
		CASE HASH("LAYOUT_LOW_ISSI7")
		CASE HASH("LAYOUT_LOW_RESTRICTED_GAUNTLET3")
		CASE HASH("LAYOUT_LOW_RESTRICTED_EMERUS")
		CASE HASH("LAYOUT_LOW_NEO")
		CASE HASH("LAYOUT_LOW_KRIEGER")
		CASE HASH("LAYOUT_LOW_S80")
		
		//mpHeist4
		CASE HASH("LAYOUT_LOW_TOREADOR")
		
		//mpSum
		CASE HASH("LAYOUT_LOW_PEYOTE3")
		CASE HASH("LAYOUT_LOW_TIGON")
		
		//mpTuner
		CASE HASH("LAYOUT_LOW_CALICO")
		CASE HASH("LAYOUT_LOW_RESTRICTED_DOMINATOR8")
		CASE HASH("LAYOUT_LOW_RESTRICTED_EUROS")
		CASE HASH("LAYOUT_LOW_RESTRICTED_RT3000")
		CASE HASH("LAYOUT_LOW_ZR350")
		
		//mpSum2
		CASE HASH("LAYOUT_LOW_RESTRICTED_POSTLUDE")
		CASE HASH("LAYOUT_LOW_RESTRICTED_TENF2")
		CASE HASH("LAYOUT_LOW_TORERO2")
			RETURN TRUE
		BREAK
	ENDSWITCH	
	
	RETURN(FALSE)
ENDFUNC


// see list of interaction anims in mp_globals_ambience.sch
PROC ShakeHead()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IsDriver(CarID)
			IF IsVehicleLow(CarID)
				START_INTERACTION_ANIM(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), ENUM_TO_INT(SPECIAL_INTERACTION_SHAKE_HEAD_DRIVER_LOW))
			ELIF IsVehicleFormulaLayout(CarID)
				START_INTERACTION_ANIM(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), ENUM_TO_INT(SPECIAL_INTERACTION_SHAKE_HEAD_DRIVER_FORMULA))
			ELSE
				START_INTERACTION_ANIM(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), ENUM_TO_INT(SPECIAL_INTERACTION_SHAKE_HEAD_DRIVER_STANDARD))
			ENDIF
		ELSE
			START_INTERACTION_ANIM(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), ENUM_TO_INT(SPECIAL_INTERACTION_SHAKE_HEAD))
		ENDIF
	ELSE
		START_INTERACTION_ANIM(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), ENUM_TO_INT(SPECIAL_INTERACTION_SHAKE_HEAD))
	ENDIF
ENDPROC



PROC GetVehiclePassengerInfo(INTERACTION_ANIM_DATA &Data)

	VEHICLE_SEAT seat
	VEHICLE_INDEX CarID

	// set default values
	Data.bIsDriverSide = FALSE
	Data.bIsLowVehicle = FALSE 
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT IS_PED_INJURED(Data.PedID)
		AND IS_PED_IN_ANY_VEHICLE(Data.PedID)
		
			CarID = GET_VEHICLE_PED_IS_IN(Data.PedID)
		
			seat = GetVehicleSeatPedIsIn(Data.PedID)
			Data.bIsDriverSide = IsSeatDriverSide(seat)
			Data.bIsRearSeat = IsSeatRear(seat)
			Data.bIsLowVehicle = IsVehicleLow(CarID)
			Data.bIsSideFacing = IsPlayerSideFacing(CarID)			
			Data.bIsDriver = IsDriver(CarID)
			Data.bIsBohdi = IsVehicleBohdiLayout(CarID)
			
			
//			#IF IS_DEBUG_BUILD
//			NET_PRINT("[interactions] GetVehiclePassengerInfo - vehicle layout is ") NET_PRINT(GetVehicleLayoutString(CarID)) NET_NL()
//			#ENDIF
			
		ENDIF
	ENDIF

ENDPROC

PROC SetInteractionLoadFlags(INTERACTION_INTERNAL_DATA &Data, INTERACTION_GLOBAL_DATA &GlobalData, BOOL bForce=FALSE, BOOL bSexIsKnown=FALSE, BOOL bIsFemale=FALSE)
	
	// is this different from the last selected? 
	IF (Data.SelectedInteraction.iType != GlobalData.iInteractionType)
	OR (Data.SelectedInteraction.iAnim != GlobalData.iInteractionAnim)
	OR (bForce)

		PRINTLN("[interactions] SetInteractionLoadFlags - Data.SelectedInteraction.iType = ", Data.SelectedInteraction.iType)		
		PRINTLN("[interactions] SetInteractionLoadFlags - Data.SelectedInteraction.iAnim = ", Data.SelectedInteraction.iAnim)		
		PRINTLN("[interactions] SetInteractionLoadFlags - GlobalData.iInteractionType = ", GlobalData.iInteractionType)	
		PRINTLN("[interactions] SetInteractionLoadFlags - GlobalData.iInteractionAnim = ", GlobalData.iInteractionAnim)	
		
		// store the last selected interaction
		Data.LastInteraction = Data.SelectedInteraction		

		Data.SelectedInteraction.iType = GlobalData.iInteractionType
		Data.SelectedInteraction.iAnim = GlobalData.iInteractionAnim
		Data.SelectedInteraction.PTFXID_Loop = INT_TO_NATIVE(PTFX_ID, -1)
		
		FillInteractionData(Data.SelectedInteraction.iType, Data.SelectedInteraction.iAnim, Data.SelectedInteraction.Data, bSexIsKnown, bIsFemale)
		
		GetVehiclePassengerInfo(Data.SelectedInteraction)
		
		// we need to load new anims, unload the old
		Data.bHasUnloadedLastSelectedAnim = FALSE		
		
	ENDIF	
ENDPROC

FUNC SC_WINDOW_LIST GetPedWindow(PED_INDEX PedID)	
	IF IS_PED_IN_ANY_VEHICLE(PedID)
		VEHICLE_SEAT Seat = GetVehicleSeatPedIsIn(PedID)
		SWITCH Seat
			CASE VS_DRIVER			
				RETURN SC_WINDOW_FRONT_LEFT	 					 
			BREAK
			CASE VS_FRONT_RIGHT	
				RETURN SC_WINDOW_FRONT_RIGHT
			BREAK
			CASE VS_BACK_LEFT	
				RETURN SC_WINDOW_REAR_LEFT   
			BREAK
			CASE VS_BACK_RIGHT		
				RETURN SC_WINDOW_REAR_RIGHT  
			BREAK			
			CASE VS_EXTRA_LEFT_1
			CASE VS_EXTRA_LEFT_2
			CASE VS_EXTRA_LEFT_3
				RETURN SC_WINDOW_MIDDLE_LEFT 
			BREAK
			CASE VS_EXTRA_RIGHT_1
			CASE VS_EXTRA_RIGHT_2
			CASE VS_EXTRA_RIGHT_3
				RETURN SC_WINDOW_MIDDLE_RIGHT
			BREAK				
		ENDSWITCH	
	ENDIF	
	RETURN SC_WINDOW_FRONT_LEFT	
ENDFUNC

PROC RemoveWindowForPedSeat(PED_INDEX PedID)
	VEHICLE_INDEX CarID
	IF IS_PED_IN_ANY_VEHICLE(PedID)
		CarID = GET_VEHICLE_PED_IS_IN(PedID)
		REMOVE_VEHICLE_WINDOW(CarID, GetPedWindow(PedID))	
	ENDIF
ENDPROC



//PROC REMOVE_VEHICLE_WINDOW_IF_NECESSARY(INTERACTION_ANIM_DATA &Data)
//	
//	VEHICLE_INDEX VehicleID
//	SC_WINDOW_LIST MyWindow
//	
//	// if we need to remove the window then we need control of the network entity
//	IF IsPedPlayerPed(Data.PedID) 
//		IF SHOULD_INTERACTIVE_ANIM_REMOVE_WINDOW(Data.iType, Data.iAnim)
//			IF IS_PED_IN_ANY_VEHICLE(Data.PedID)
//				VehicleID = GET_VEHICLE_PED_IS_IN(Data.PedID)				
//				IF IS_VEHICLE_DRIVEABLE(VehicleID)					
//					MyWindow = GetPedWindow(Data.PedID)					
//					IF IS_VEHICLE_WINDOW_INTACT(VehicleID, GetPedWindow(Data.PedID))															
//						BROADCAST_REMOVE_WINDOW_REQUEST(MyWindow)
//						NET_PRINT("[interactions] REMOVE_VEHICLE_WINDOW_IF_NECESSARY - BROADCAST_REMOVE_WINDOW_REQUEST") NET_NL()
//					ELSE
//						NET_PRINT("[interactions] REMOVE_VEHICLE_WINDOW_IF_NECESSARY - window is not intact") NET_NL()					
//					ENDIF
//				ENDIF					
//			ENDIF					
//		ENDIF		
//	ENDIF
//
//ENDPROC


FUNC BOOL IsPedInCorrectOnFootStateForInteraction(INTERACTION_GLOBAL_DATA &GlobalData, PED_INDEX PedID)
	IF (GlobalData.iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL))
		RETURN(TRUE)
	ELIF (GlobalData.iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_IN_CAR))
		IF IS_PED_IN_ANY_VEHICLE(PedID)
			RETURN(TRUE)
		ENDIF
	ELSE
		IF NOT IS_PED_IN_ANY_VEHICLE(PedID)
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL ShouldDisableMovementControls(INTERACTION_INTERNAL_DATA &InteractionData)
	IF (InteractionData.bPlayingFullBody)
	//OR (InteractionData.bPlayingFullBodyMask)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsPlayerPointing(BOOL bConsiderOutroAsPointing=TRUE)
	IF NOT (MPGlobalsInteractions.PointingState = POINT_INTERACTION_NULL)
		IF (bConsiderOutroAsPointing)
		OR NOT (MPGlobalsInteractions.PointingState = POINT_INTERACTION_OUTRO)	
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC



PROC DisableControlsForInteraction(PED_INDEX PedID, BOOL bDisableMovement=FALSE)
	
	IF IsPedPlayerPed(PedID)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
		IF (MPGlobalsInteractions.bQuickplayButtonPressed)
		OR (MPGlobalsInteractions.bPointingButtonPressed)
		OR IsPlayerPointing()
		OR IS_PLAYER_IN_FIRST_PERSON_CAMERA()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
		ENDIF
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)	
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_HUD_SPECIAL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ARREST)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)	
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)	
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM) // disable sniper scope
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ACCURATE_AIM) // disable sniper scope
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER) // Don't let player enter a vehicle while doing the action
		
		//IF NOT ( IsPlayerPointing() )
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA) // to stop going into 1st person mid anim 
		//ENDIF
		
		IF ((MPGlobalsInteractions.bQuickplayButtonPressed))//  OR IS_INTERACTION_MENU_ACCEPT_BUTTON_PRESSED())
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)		
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
			NET_PRINT("[interactions] DisableControlsForInteraction - disabling veh controls.") NET_NL()
		ENDIF
		
		IF (bDisableMovement)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)		
			NET_PRINT("[interactions] DisableControlsForInteraction - disabling movement controls.") NET_NL()
		ENDIF
	ENDIF

//	SET_PLAYER_CAN_DO_DRIVE_BY(PLAYER_ID(), FALSE )
	
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
//	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
	
	// stop player from ducking / stealth
	IF DOES_ENTITY_EXIST(PedID)
		IF NOT IS_ENTITY_DEAD(PedID)
			IF IS_PED_DUCKING(PedID)
				SET_PED_DUCKING(PedID, FALSE)
				NET_PRINT("[interactions] DisableControlsForInteraction - stopping ped ducking.") NET_NL()
			ENDIF
			IF GET_PED_STEALTH_MOVEMENT(PedID)
				SET_PED_STEALTH_MOVEMENT(PedID, FALSE)
				NET_PRINT("[interactions] DisableControlsForInteraction - stopping stealth movement.") NET_NL()
			ENDIF	
			IF NOT (GET_PED_COMBAT_MOVEMENT(PedID) = CM_STATIONARY)
				SET_PED_COMBAT_MOVEMENT(PedID, CM_STATIONARY)
				NET_PRINT("[interactions] DisableControlsForInteraction - setting combat movement.") NET_NL()
			ENDIF	
			IF IS_PED_USING_ACTION_MODE(PedID)
				SET_PED_USING_ACTION_MODE(PedID, FALSE)	
				NET_PRINT("[interactions] DisableControlsForInteraction - stopping action mode.") NET_NL()
			ENDIF
		ENDIF
		
		
		SET_PED_RESET_FLAG(PedID, PRF_DisableActionMode, TRUE)	
		SET_PED_RESET_FLAG(PedID, PRF_DontCloseVehicleDoor, TRUE)	// see 1782819
		
		
		IF IsPedPlayerPed(PedID)
		AND NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		ENDIF
	ENDIF
	
ENDPROC


FUNC BOOL IsPlayingIntro(INTERACTION_ANIM_DATA &AnimData)
	
	IF (AnimData.Data.bIsLooped)
		IF NOT IS_ENTITY_DEAD(AnimData.PedID)			
			IF (AnimData.bSelfieMode)
				IF (AnimData.AnimStage = IAS_INTRO)
					IF (AnimData.Data.bIsLooped)
						// if anim is looped then just return true, this is so the IsBodyAnimAlmostFinished will work later on
						#IF IS_DEBUG_BUILD
						NET_PRINT("[interactions] IsPlayingIntro - returning TRUE - playing intro anim (selfie) ") NET_PRINT_FLOAT(GET_PHONE_GESTURE_ANIM_CURRENT_TIME(AnimData.PedID)) NET_NL()
						#ENDIF
						RETURN(TRUE)								
					ELSE
						IF IsPedUsingSelfieTask(AnimData.PedID)
						AND IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
							#IF IS_DEBUG_BUILD
							NET_PRINT("[interactions] IsPlayingIntro - returning TRUE - playing intro anim (selfie - non looped)  ") NET_PRINT_FLOAT(GET_PHONE_GESTURE_ANIM_CURRENT_TIME(AnimData.PedID)) NET_NL()
							#ENDIF
							RETURN(TRUE)																
						ENDIF
					ENDIF
				ENDIF
			ELSE				
				IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, InteractionAnimDictFromInteractionAnimData(AnimData), AnimData.Data.strAnimNameIntro)					
					#IF IS_DEBUG_BUILD
					NET_PRINT("[interactions] IsPlayingIntro - returning TRUE - playing intro anim ") NET_PRINT_FLOAT(GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, InteractionAnimDictFromInteractionAnimData(AnimData), AnimData.Data.strAnimNameIntro)) NET_NL()
					#ENDIF
					RETURN(TRUE)	
				ENDIF	
			ENDIF
		ENDIF
	ENDIF

	NET_PRINT("[interactions] IsPlayingIntro - returning FALSE") NET_NL()
	RETURN(FALSE)
	
ENDFUNC

FUNC BOOL IsPlayingBody(INTERACTION_ANIM_DATA &AnimData)
	IF NOT IS_ENTITY_DEAD(AnimData.PedID)
		IF (AnimData.bSelfieMode)
			IF (AnimData.AnimStage = IAS_BODY)
				IF (AnimData.Data.bIsLooped)
					// if anim is looped then just return true, this is so the IsBodyAnimAlmostFinished will work later on
					#IF IS_DEBUG_BUILD
					NET_PRINT("[interactions] IsPlayingBody - returning TRUE - playing main anim (selfie) ") NET_PRINT_FLOAT(GET_PHONE_GESTURE_ANIM_CURRENT_TIME(AnimData.PedID)) NET_NL()
					#ENDIF
					RETURN(TRUE)				
				ELSE
					IF IsPedUsingSelfieTask(AnimData.PedID)
					AND IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
						#IF IS_DEBUG_BUILD
						NET_PRINT("[interactions] IsPlayingBody - returning TRUE - playing main anim (selfie non-looped) ") NET_PRINT_FLOAT(GET_PHONE_GESTURE_ANIM_CURRENT_TIME(AnimData.PedID)) NET_NL()
						#ENDIF					
						RETURN(TRUE)						
					ENDIF
				ENDIF
			ENDIF
		ELSE	
			IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, InteractionAnimDictFromInteractionAnimData(AnimData), AnimData.Data.strAnimName)					
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("[interactions] IsPlayingBody - returning TRUE - playing main anim, time = ") NET_PRINT_FLOAT(GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, InteractionAnimDictFromInteractionAnimData(AnimData), AnimData.Data.strAnimName)) NET_NL()	
				#ENDIF
				
				RETURN(TRUE)	
			ENDIF
		ENDIF
	ENDIF

	NET_PRINT("[interactions] IsPlayingBody - returning FALSE") NET_NL()
	RETURN(FALSE)
	
ENDFUNC

FUNC BOOL IsPlayingIntroOrLoop(INTERACTION_ANIM_DATA &AnimData)
	
	IF IsPlayingIntro(AnimData)
		RETURN(TRUE)
	ENDIF
	
	IF IsPlayingBody(AnimData)
		RETURN(TRUE)
	ENDIF
	

	NET_PRINT("[interactions] IsPlayingIntroOrLoop - returning FALSE") NET_NL()
	RETURN(FALSE)		
ENDFUNC

FUNC BOOL IsPlayingOutro(INTERACTION_ANIM_DATA &AnimData)
	IF (AnimData.Data.bIsLooped)
		IF NOT IS_ENTITY_DEAD(AnimData.PedID)
			IF (AnimData.bSelfieMode)
				IF (AnimData.AnimStage = IAS_OUTRO)
					IF IsPedUsingSelfieTask(AnimData.PedID)
					AND IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
						#IF IS_DEBUG_BUILD
						NET_PRINT("[interactions] IsPlayingOutro - returning TRUE - playing outro anim (selfie) ") NET_PRINT_FLOAT(GET_PHONE_GESTURE_ANIM_CURRENT_TIME(AnimData.PedID)) NET_NL()
						#ENDIF
						RETURN(TRUE)														
					ENDIF
				ENDIF
			ELSE
		
				IF IS_ENTITY_PLAYING_ANIM( AnimData.PedID, InteractionAnimDictFromInteractionAnimData(AnimData), AnimData.Data.strAnimNameOutro)					
					
					#IF IS_DEBUG_BUILD
					NET_PRINT("[interactions] IsPlayingOutro - returning TRUE - playing outro anim, time = ") NET_PRINT_FLOAT(GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, InteractionAnimDictFromInteractionAnimData(AnimData), AnimData.Data.strAnimNameOutro)) NET_NL()	
					#ENDIF
					
					RETURN(TRUE)	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	NET_PRINT("[interactions] IsPlayingOutro - returning FALSE") NET_NL()
	RETURN(FALSE)		
ENDFUNC

PROC UpdateHideWeapon(INTERACTION_ANIM_DATA &AnimData)
	IF NOT ((AnimData.iAnimFlags & ENUM_TO_INT(AF_HIDE_WEAPON)) = 0)
		IF NOT GET_PED_CONFIG_FLAG(AnimData.PedID,PCF_KeepWeaponHolsteredUnlessFired)
			SET_PED_CONFIG_FLAG(AnimData.PedID,PCF_KeepWeaponHolsteredUnlessFired, FALSE)
			NET_PRINT("[interactions] - UpdateHideWeapon setting PCF_KeepWeaponHolsteredUnlessFired.") NET_NL()
		ENDIF
	ENDIF
ENDPROC

PROC StoreWeapon(INTERACTION_ANIM_DATA &AnimData, BOOL DontRestoreWeaponAfter)
	WEAPON_TYPE CurrentWeaponType = WEAPONTYPE_UNARMED
	GET_CURRENT_PED_WEAPON(AnimData.PedID, CurrentWeaponType)
	IF NOT (CurrentWeaponType = WEAPONTYPE_UNARMED)
	AND NOT (CurrentWeaponType = WEAPONTYPE_INVALID)
		IF (g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = FALSE)
			AnimData.StoredWeapon = CurrentWeaponType
			SET_CURRENT_PED_WEAPON(AnimData.PedID, WEAPONTYPE_UNARMED, TRUE)	
			IF (DontRestoreWeaponAfter) // don't restore weapons after a sycned scene.
				AnimData.StoredWeapon = WEAPONTYPE_UNARMED
			ENDIF
			PRINTLN("[interactions] StoreWeapon - set AnimData.StoredWeapon to ", AnimData.StoredWeapon) 
		ELSE
			#IF IS_DEBUG_BUILD
				IF (g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK)
					PRINTLN("[interactions] StoreWeapon - kev is still giving player weapons, dont store weapon.") 
				ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IsFinishedSequenceTask(PED_INDEX PedID)
	// check if player is walking or driving (from after a transition)
	SCRIPTTASKSTATUS TaskStatus
	TaskStatus = GET_SCRIPT_TASK_STATUS(PedID, SCRIPT_TASK_PERFORM_SEQUENCE)
	IF (TaskStatus = FINISHED_TASK)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC CLEANUP_INTERACTION(INTERACTION_ANIM_DATA &AnimData)
	NET_PRINT("[interactions] CLEANUP_INTERACTION - called") NET_NL()
	CleanupAnimPropsAndVFX(AnimData)
	IF NOT IS_PED_INJURED(AnimData.PedID)	
		IF NOT (AnimData.bSelfieMode)
			IF (MPGlobalsInteractions.bQuickRestartAnim)
			AND IS_SAFE_TO_PLAY_ON_FOOT_INTERACTION()
				PRINTLN("[interactions] [Quickplay Restart] - CLEANUP_INTERACTION - dont clear secondary task")	
				UpdateHideWeapon(AnimData)
				
				// if current anim hides weapon and we're going to a quickplay, then remove weapon.
				IF NOT ((AnimData.iAnimFlags & ENUM_TO_INT(AF_HIDE_WEAPON)) = 0)
					PRINTLN("[interactions] [Quickplay Restart] - CLEANUP_INTERACTION - storing weapon")	
					StoreWeapon(AnimData, FALSE)
				ENDIF
				
				MPGlobalsInteractions.bInteractionIsPlayingForQuickRestart = MPGlobalsInteractions.bAnyInteractionIsPlaying
				PRINTLN("[interactions] [Quickplay Restart] - CLEANUP_INTERACTION - setting MPGlobalsInteractions.bInteractionIsPlayingForQuickRestart to ", MPGlobalsInteractions.bInteractionIsPlayingForQuickRestart)
				
			ELSE
				CLEAR_PED_SECONDARY_TASK(AnimData.PedID)
				
				IF NOT IsFinishedSequenceTask(AnimData.PedID)
					CLEAR_PED_TASKS(AnimData.PedID)
					PRINTLN("[interactions] [Quickplay Restart] - CLEANUP_INTERACTION - clearing sequence task")
				ENDIF
				
			ENDIF
			
			IF NOT (AnimData.StoredWeapon = WEAPONTYPE_UNARMED)
			AND NOT (AnimData.StoredWeapon = WEAPONTYPE_INVALID)
				
				PRINTLN("[interactions] CLEANUP_INTERACTION - AnimData.StoredWeapon = ", AnimData.StoredWeapon) 
				
				// dont restore weapon if we are going straight to next quickanim
				IF (MPGlobalsInteractions.bQuickRestartAnim)
					PRINTLN("[interactions] [Quickplay Restart] - CLEANUP_INTERACTION - dont restore weapon")
				ELSE
					IF HAS_PED_GOT_WEAPON(AnimData.PedID, AnimData.StoredWeapon)
						SET_CURRENT_PED_WEAPON(AnimData.PedID, AnimData.StoredWeapon, FALSE)	
						AnimData.StoredWeapon = WEAPONTYPE_UNARMED
						NET_PRINT("[interactions] CLEANUP_INTERACTION - SET_CURRENT_PED_WEAPON called  ") NET_NL()
					ELSE
						NET_PRINT("[interactions] CLEANUP_INTERACTION - HAS_PED_GOT_WEAPON = FALSE  ") NET_NL()
					ENDIF
				ENDIF
			ENDIF			
			
		ENDIF				
	ENDIF
	
	IF AnimData.Data.bHasCooldown
	AND NOT MPGlobalsInteractions.bQuickRestartAnim
		REINIT_NET_TIMER(g_sPlayerActionCooldown)
	ENDIF
	
	IF (AnimData.iType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER))
		IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_MAKE_IT_RAIN))
		AND NOT MPGlobalsInteractions.bQuickRestartAnim
			IF NETWORK_GET_VC_WALLET_BALANCE() >= g_sMPTunables.iARENA_MAKE_IT_RAIN
				IF USE_SERVER_TRANSACTIONS()
					INT iTransactionID
					
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_MAKE_IT_RAIN, g_sMPTunables.iARENA_MAKE_IT_RAIN, iTransactionID)
				ELSE
					NETWORK_SPEND_MAKE_IT_RAIN(g_sMPTunables.iARENA_MAKE_IT_RAIN, FALSE, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	AnimData.bStarted = FALSE
ENDPROC

FUNC BOOL IsPedInIdleStance(PED_INDEX PedID)
	IF NOT GET_PED_STEALTH_MOVEMENT(PedID)
	AND NOT IS_PED_DUCKING(PedID)
	AND (GET_PED_COMBAT_MOVEMENT(PedID) = CM_STATIONARY) 	
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsPedSafeToPlayFullBody(PED_INDEX PedID, BOOL bSkipIdleStanceCheck=FALSE)
	#IF FEATURE_HEIST_ISLAND
	IF g_bOverrideDanceActionSafetyCheck
	AND IS_PLAYER_DANCING(PLAYER_ID())
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF NOT IS_ENTITY_DEAD(PedID)
	AND IS_PED_STILL(PedID)
	AND NOT IS_PED_IN_ANY_VEHICLE(PedID, TRUE)
	AND (NOT Is_Ped_Drunk(PedID) OR IS_PLAYER_DANCING(PLAYER_ID()))
	AND (IsPedInIdleStance(PedID) OR bSkipIdleStanceCheck)
		
		// if it is the player, is he just coming out of a combat, stealth, ducking stance?
		IF NOT (bSkipIdleStanceCheck)
			IF (PedID = PLAYER_PED_ID())
				IF GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.timePlayerNonIdle) < 1300
					NET_PRINT("[interactions] IsPedSafeToPlayFullBody - time since idle = ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.timePlayerNonIdle)) NET_NL() 
					RETURN(FALSE)
				ENDIF
			ENDIF
		ENDIF
	
		RETURN(TRUE)
	ENDIF
	#IF IS_DEBUG_BUILD
		IF NOT IS_PED_STILL(PedID)
			NET_PRINT("[interactions] IsPedSafeToPlayFullBody - IS_PED_STILL = FALSE")  NET_NL()
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE(PedID, TRUE)
			NET_PRINT("[interactions] IsPedSafeToPlayFullBody - IS_PED_IN_ANY_VEHICLE = TRUE") NET_NL()
		ENDIF
		IF Is_Ped_Drunk(PedID)
			NET_PRINT("[interactions] IsPedSafeToPlayFullBody - Is_Ped_Drunk = TRUE") NET_NL()
		ENDIF
		IF NOT IsPedInIdleStance(PedID)
			NET_PRINT("[interactions] IsPedSafeToPlayFullBody - IsPedInIdleStance = FALSE") NET_NL()
		ENDIF
	#ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsAnyCameraMovementPressed()
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_LR)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_UD)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)		
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsAnyMovementPressed()
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_LR)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_UD)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)		
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsAnyButtonPressedThatShouldQuitPointing()
	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)	
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_RELOAD)	
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)	
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_DETONATE)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK2)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_DUCK)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_COVER)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_PHONE)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)	
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsSafeForPointing(BOOL bStartOnly=TRUE)
	//IF IsAnyMovementPressed()
	IF NOT IS_SAFE_TO_PLAY_ON_FOOT_INTERACTION(bStartOnly)
		NET_PRINT("[interactions] IsSafeForPointing() = FALSE, IS_SAFE_TO_PLAY_ON_FOOT_INTERACTION") NET_NL()
		RETURN(FALSE)
	ENDIF
	IF (MPGlobalsInteractions.bAnyInteractionIsPlaying)
		NET_PRINT("[interactions] IsSafeForPointing() = FALSE, MPGlobalsInteractions.bAnyInteractionIsPlaying") NET_NL()
		RETURN(FALSE)
	ENDIF
	RETURN(TRUE)
ENDFUNC

FUNC BOOL IsSafeToUseNoFilter(PED_INDEX PedID)
	IF IsPedSafeToPlayFullBody(PedID)
	
		// if it is the player, is the player trying to move?
		IF (PedID = PLAYER_PED_ID())
			IF IsAnyMovementPressed()
			OR GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.timePlayerMoves) < 500
				RETURN(FALSE)
			ENDIF
		ENDIF
	
		// in corona they use custom poses, so dont play full body.
		IF IS_PLAYER_IN_CORONA() 
			RETURN(FALSE)
		ENDIF
	
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC




FUNC BOOL HasAnimStartedBlendingOut(INTERACTION_ANIM_DATA &AnimData, INTERACTION_ANIM_STAGE ThisAnimStage, BOOL bSlowBlend, FLOAT &fLastBlendOutAnimPhase, BOOL bOrInFirstPhaseIncrement=FALSE)
	FLOAT fAnimPhase
	FLOAT fAnimTime
	FLOAT fTotalAnimTime
	FLOAT fBlendStartTime
	FLOAT fPhaseIncrementTime
	
	STRING AnimDict = InteractionAnimDictFromInteractionAnimData(AnimData)
	STRING AnimName = AnimNameFromStage(AnimData, ThisAnimStage)
	

	IF (AnimData.bSelfieMode)
		IF IsPedUsingSelfieTask(AnimData.PedID)
		AND IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
			fAnimPhase = GET_PHONE_GESTURE_ANIM_CURRENT_TIME(AnimData.PedID)
			fTotalAnimTime = GET_PHONE_GESTURE_ANIM_TOTAL_TIME(AnimData.PedID)
		ELSE
			PRINTLN("[interactions] HasAnimStartedBlendingOut - IsPedUsingSelfieTask = ", IsPedUsingSelfieTask(AnimData.PedID)) 
			PRINTLN("[interactions] HasAnimStartedBlendingOut - IS_PLAYING_PHONE_GESTURE_ANIM = ", IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)) 
			RETURN(FALSE)
		ENDIF
	ELSE	
		IF IS_ENTITY_PLAYING_ANIM(AnimData.PedID, AnimDict, AnimName)
			fAnimPhase = GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, AnimDict, AnimName) 
			fTotalAnimTime = GET_ENTITY_ANIM_TOTAL_TIME (AnimData.PedID, AnimDict, AnimName)	
		ELSE
			NET_PRINT("[interactions] HasAnimStartedBlendingOut - NOT PLAYING ANIM - ")	NET_PRINT(AnimName)  NET_NL()
			RETURN(FALSE)
		ENDIF
	ENDIF
	
	IF (fAnimPhase < 0.0)
		NET_PRINT("[interactions] HasAnimStartedBlendingOut - fAnimPhase < 0, ")	NET_PRINT(AnimName)  NET_NL()
		RETURN(FALSE)		
	ENDIF
	
	fAnimTime = fAnimPhase * fTotalAnimTime		
	IF (fAnimPhase < fLastBlendOutAnimPhase)
		fLastBlendOutAnimPhase = fAnimPhase
	ENDIF		
	fPhaseIncrementTime = (fAnimPhase - fLastBlendOutAnimPhase) * fTotalAnimTime		
	fLastBlendOutAnimPhase = fAnimPhase	
	
	IF (fLastBlendOutAnimPhase < 0.0)
		fLastBlendOutAnimPhase = 0.0
	ENDIF
	
	fPhaseIncrementTime += 0.001
	
	// check if in first phase of anim
	IF (bOrInFirstPhaseIncrement)
		IF (fAnimTime < fPhaseIncrementTime)
		
			NET_PRINT("[interactions] HasAnimStartedBlendingOut, withing 1st phase increment. -  fTotalAnimTime - ") NET_PRINT_FLOAT(fTotalAnimTime) 
			NET_PRINT(", fAnimPhase = ") NET_PRINT_FLOAT(fAnimPhase)
			NET_PRINT(", fAnimTime = ") NET_PRINT_FLOAT(fAnimTime)
			NET_PRINT(", fPhaseIncrementTime = ") NET_PRINT_FLOAT(fPhaseIncrementTime)
			NET_PRINT(", fBlendStartTime = ") NET_PRINT_FLOAT(fBlendStartTime)
			NET_NL()					
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	FLOAT fBlendOutDuration
	SWITCH AnimData.AnimStage
		CASE IAS_INTRO
			fBlendOutDuration = INSTANT_BLEND_DURATION
		BREAK
		CASE IAS_BODY
			IF NOT (bSlowBlend)
				fBlendOutDuration = INSTANT_BLEND_DURATION
			ELSE
				fBlendOutDuration = SLOW_BLEND_DURATION
			ENDIF
		BREAK
		CASE IAS_OUTRO
			fBlendOutDuration = SLOW_BLEND_DURATION	
		BREAK
	ENDSWITCH
	
	INT iNoOfFrames
	
	IF (AnimData.bSelfieMode)
		iNoOfFrames = NO_OF_FRAMES_BEFORE_CHANGE
	ELSE
		iNoOfFrames = 1
	ENDIF
	
	fBlendStartTime = (fTotalAnimTime - (fBlendOutDuration * 1000.0)) - (fPhaseIncrementTime * TO_FLOAT(iNoOfFrames))
	
	NET_PRINT("[interactions] HasAnimStartedBlendingOut-  fTotalAnimTime - ") NET_PRINT_FLOAT(fTotalAnimTime) 
	NET_PRINT(", fAnimPhase = ") NET_PRINT_FLOAT(fAnimPhase)
	NET_PRINT(", fAnimTime = ") NET_PRINT_FLOAT(fAnimTime)
	NET_PRINT(", fPhaseIncrementTime = ") NET_PRINT_FLOAT(fPhaseIncrementTime)
	NET_PRINT(", fBlendOutDuration = ") NET_PRINT_FLOAT(fBlendOutDuration)
	NET_PRINT(", fBlendStartTime = ") NET_PRINT_FLOAT(fBlendStartTime)
	NET_NL()		
	
	IF (fAnimTime >= fBlendStartTime)
		RETURN(TRUE)
	ENDIF

		
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL HasAnimFinished(INTERACTION_ANIM_DATA &AnimData, INTERACTION_ANIM_STAGE ThisAnimStage)
	IF (AnimData.bSelfieMode)
		IF (ThisAnimStage = AnimData.AnimStage)
			IF IsPedUsingSelfieTask(AnimData.PedID)
			AND IS_PLAYING_PHONE_GESTURE_ANIM(AnimData.PedID)
				NET_PRINT("[interactions] HasAnimFinished - RETURN FALSE - selfie ") NET_NL()
				RETURN(FALSE)
			ENDIF
		ENDIF
	ELSE	
		IF NOT HAS_ENTITY_ANIM_FINISHED(AnimData.PedID, InteractionAnimDictFromInteractionAnimData(AnimData), AnimNameFromStage(AnimData, ThisAnimStage))	
			NET_PRINT("[interactions] HasAnimFinished - RETURN FALSE - non selfie ") NET_NL()
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN(TRUE)
ENDFUNC

FUNC BOOL IsIntroAnimAlmostFinished(INTERACTION_ANIM_DATA &AnimData)
	IF HasAnimStartedBlendingOut(AnimData, IAS_INTRO, FALSE, AnimData.fLastBlendOutAnimPhase) 
		NET_PRINT("[interactions] IsIntroAnimAlmostFinished - RETURN TRUE") NET_NL() // - fAnimTime = ") NET_PRINT_FLOAT(fAnimTime) NET_NL()
		RETURN TRUE
	ELSE
		IF HasAnimFinished(AnimData, IAS_INTRO)
			NET_PRINT("[interactions] IsIntroAnimAlmostFinished - RETURN TRUE - Has actually finished ") NET_NL()
			RETURN TRUE
		ENDIF	
	ENDIF		
	RETURN FALSE
ENDFUNC

FUNC BOOL IsBodyAnimAlmostFinished(INTERACTION_ANIM_DATA &AnimData)
	FLOAT fAnimTime
	FLOAT fTotalAnimTime
	
	IF (AnimData.bSelfieMode)
		fAnimTime = GET_PHONE_GESTURE_ANIM_CURRENT_TIME(AnimData.PedID)
		fTotalAnimTime = GET_PHONE_GESTURE_ANIM_TOTAL_TIME(AnimData.PedID)
	ELSE
		fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME (AnimData.PedID, InteractionAnimDictFromInteractionAnimData(AnimData), AnimData.Data.strAnimName ) 
		fTotalAnimTime = GET_ENTITY_ANIM_TOTAL_TIME (AnimData.PedID, InteractionAnimDictFromInteractionAnimData(AnimData), AnimData.Data.strAnimName)		
	ENDIF
	
	
	NET_PRINT("[interactions] IsBodyAnimAlmostFinished - fAnimTime = ") NET_PRINT_FLOAT(fAnimTime) NET_PRINT(", fTotalAnimTime = ") NET_PRINT_FLOAT(fTotalAnimTime) NET_NL()

	IF HasAnimStartedBlendingOut(AnimData, IAS_BODY, AnimData.Data.bCanQuitLoopEarly, AnimData.fLastBlendOutAnimPhase, (AnimData.iIteration > 0))
	OR fTotalAnimTime < 0.1
		NET_PRINT("[interactions] IsBodyAnimAlmostFinished - RETURN TRUE ")  NET_NL()
		RETURN TRUE
	ENDIF		
	IF HasAnimFinished(AnimData, IAS_BODY)
		NET_PRINT("[interactions] IsBodyAnimAlmostFinished - RETURN TRUE (has finished) ") NET_NL()
		RETURN TRUE
	ENDIF
	NET_PRINT("[interactions] IsBodyAnimAlmostFinished - RETURN FALSE ")  NET_NL()
	RETURN FALSE
ENDFUNC

FUNC BOOL IsOutroAnimAlmostFinished(INTERACTION_ANIM_DATA &AnimData)
	IF HasAnimStartedBlendingOut(AnimData, IAS_OUTRO, FALSE, AnimData.fLastBlendOutAnimPhase)
		NET_PRINT("[interactions] IsOutroAnimAlmostFinished - RETURN TRUE") NET_NL() // NET_PRINT_FLOAT(fAnimTime) NET_NL()
		RETURN TRUE
	ENDIF		
	IF HasAnimFinished(AnimData, IAS_OUTRO)
		NET_PRINT("[interactions] IsOutroAnimAlmostFinished - RETURN TRUE - Has actually finished ") NET_NL()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HasBodyAnimCompletedMinIterations(INTERACTION_ANIM_DATA &AnimData)

	IF (AnimData.Data.iMinLoopIterations = 0)
	OR (AnimData.iIteration >= AnimData.Data.iMinLoopIterations)
		NET_PRINT("[interactions] HasBodyAnimCompletedMinIterations - RETURN TRUE ")  NET_NL()
		RETURN(TRUE)
	ENDIF

	NET_PRINT("[interactions] HasBodyAnimCompletedMinIterations - RETURN FALSE ")  NET_NL()
	RETURN(FALSE)

ENDFUNC

//FUNC BOOL IsFinishedSelfieTask(PED_INDEX pedID)
//	// check if player is doing a selfie gesture
//	SCRIPTTASKSTATUS TaskStatus
//	TaskStatus = GET_SCRIPT_TASK_STATUS(PedID, SCRIPT_TASK_PLAY_PHONE_GESTURE_ANIMATION)
//	IF (TaskStatus = FINISHED_TASK)
//		RETURN(TRUE)
//	ENDIF
//	RETURN(FALSE)	
//ENDFUNC




//FUNC BOOL IsPointingActive()
//	IF NOT (MPGlobalsInteractions.PointingState = POINT_INTERACTION_NULL)
//		RETURN(TRUE)
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC

PROC RestorePointingWeapon()
	IF NOT (MPGlobalsInteractions.PointingStoredWeapon = WEAPONTYPE_UNARMED)
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), MPGlobalsInteractions.PointingStoredWeapon)
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), MPGlobalsInteractions.PointingStoredWeapon, TRUE)	
			MPGlobalsInteractions.PointingStoredWeapon = WEAPONTYPE_UNARMED
			NET_PRINT("[interactions] RestorePointingWeapon - SET_CURRENT_PED_WEAPON called  ") NET_NL()
		ELSE
			NET_PRINT("[interactions] RestorePointingWeapon - HAS_PED_GOT_WEAPON = FALSE  ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

PROC CleanupPointing()
	REMOVE_ANIM_DICT("anim@mp_point")
	IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
		NET_PRINT("[interactions] CleanupPointing() - clearing ped tasks") NET_NL()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			//CLEAR_PED_TASKS(PLAYER_PED_ID())	
			CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
		ENDIF
	ENDIF
	RestorePointingWeapon()	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		NET_PRINT("[interactions] CleanupPointing() player not in vehicle making weapon visible.") NET_NL()
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE, DEFAULT, DEFAULT, TRUE)	
	ELSE
		NET_PRINT("[interactions] CleanupPointing() player in vehicle.") NET_NL()
	ENDIF
	MPGlobalsInteractions.PointingState = POINT_INTERACTION_NULL
	MPGlobalsInteractions.bPointingDoubleTap = FALSE
	
	// stop ped from taking off helmet while pointing
	IF MPGlobalsInteractions.bPointingBlockingHelmetRemoval
		IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, FALSE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, FALSE)	
			PRINTLN("[interactions] UpdatePlayerPointing - restored PCF_DontTakeOffHelmet  ") 
		ENDIF
		MPGlobalsInteractions.bPointingBlockingHelmetRemoval = FALSE
	ENDIF
		
	NET_PRINT("[interactions] CleanupPointing() called.") NET_NL()
ENDPROC

FUNC FLOAT GetPointingPitch()
	FLOAT fCameraPitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
	FLOAT fReturn = fCameraPitch
	IF (fReturn < -70.0)
		fReturn = -70.0
	ENDIF
	IF (fReturn > 42.0)
		fReturn = 42.0
	ENDIF
	fReturn += 70.0
	fReturn /= 112.0
	
	//NET_PRINT("[interactions] GetPointingPitch() - fCameraPitch = ") NET_PRINT_FLOAT(fCameraPitch) NET_PRINT(", fReturn = ") NET_PRINT_FLOAT(fReturn) NET_NL()
	RETURN(fReturn)	
ENDFUNC

FUNC FLOAT GetPointingHeading()
	FLOAT fCameraHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
	FLOAT fReturn
	
	
//	IF (MPGlobalsInteractions.bDoPointingInterp = FALSE )
//	AND ((fCameraHeading > 170.0) AND (MPGlobalsInteractions.fPreviousPointingHeading < -170.0)	OR (fCameraHeading < -170.0) AND (MPGlobalsInteractions.fPreviousPointingHeading > 170.0))		
//		// go long way round
//		MPGlobalsInteractions.bDoPointingInterp = TRUE		
//	ENDIF
//	
//	IF (MPGlobalsInteractions.bDoPointingInterp)
//		FLOAT fDiff = fCameraHeading - MPGlobalsInteractions.fPreviousPointingHeading
//		IF ABSF(fDiff) > 1.0			
//			fCameraHeading = MPGlobalsInteractions.fPreviousPointingHeading + fDiff/POINTING_INTERP_VALUE	
//		ELSE
//			MPGlobalsInteractions.bDoPointingInterp = FALSE
//		ENDIF
//	ENDIF
	
	MPGlobalsInteractions.fPreviousPointingHeading = fCameraHeading
	// point to heading.
	fReturn = fCameraHeading
	IF (fReturn < -180.0)
		fReturn = -180.0
	ENDIF
	IF (fReturn > 180.0)
		fReturn = 180.0
	ENDIF	
	fReturn += 180.0
	fReturn /= 360.0
	fReturn *= -1.0
	fReturn += 1.0
	RETURN(fReturn)	
	
	
	

ENDFUNC

FUNC BOOL IsPointingBlocked()
	FLOAT fCameraHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()	
	FLOAT fCameraHeadingAsFloat
	
	fCameraHeadingAsFloat = fCameraHeading
	IF (fCameraHeadingAsFloat < -180.0)
		fCameraHeadingAsFloat = -180.0
	ENDIF
	IF (fCameraHeadingAsFloat > 180.0)
		fCameraHeadingAsFloat = 180.0
	ENDIF	
	fCameraHeadingAsFloat += 180.0
	fCameraHeadingAsFloat /= 360.0
	
	FLOAT fPointingOffsetDist
	
	fPointingOffsetDist = ((POINTING_OFFSET_MAX_Y - POINTING_OFFSET_MIN_Y) * fCameraHeadingAsFloat) + POINTING_OFFSET_MIN_Y
	
	//FLOAT fCameraPitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
	VECTOR vec = <<POINTING_OFFSET_X, fPointingOffsetDist, POINTING_OFFSET_Z>>
	VECTOR vCoords
	RotateVec(vec, <<0.0, 0.0, fCameraHeading>>)
	vCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vec)
	
	SHAPETEST_INDEX ShapeTestResult
	ShapeTestResult = START_SHAPE_TEST_CAPSULE(<<vCoords.x, vCoords.y, (vCoords.z-POINTING_CAPSULE_HEIGHT)>>, <<vCoords.x, vCoords.y, (vCoords.z+POINTING_CAPSULE_HEIGHT)>>, POINTING_CAPSULE_RADIUS, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_PED |SCRIPT_INCLUDE_RAGDOLL | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_GLASS, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_DEFAULT)

	DRAW_DEBUG_SPHERE(vCoords, POINTING_CAPSULE_RADIUS, 123, 53, 200, 128)
	DRAW_DEBUG_LINE(<<vCoords.x, vCoords.y, (vCoords.z-POINTING_CAPSULE_HEIGHT)>>, <<vCoords.x, vCoords.y, (vCoords.z+POINTING_CAPSULE_HEIGHT)>>, 123, 53, 200, 255)

	INT iHitSomething
	VECTOR vHit
	VECTOR vNorm
	ENTITY_INDEX EntityHit
	//SHAPETEST_STATUS ShapeTestStatus = 
	GET_SHAPE_TEST_RESULT(ShapeTestResult, iHitSomething, vHit, vNorm, EntityHit)

	IF NOT (iHitSomething = 0)
		RETURN(TRUE)
	ENDIF

	RETURN(FALSE)
ENDFUNC

FUNC FLOAT GetSpineAngle()
	VECTOR vNeck
	VECTOR vRoot
	VECTOR vec
	vNeck = GET_WORLD_POSITION_OF_ENTITY_BONE(PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_NECK))
	vRoot = GET_WORLD_POSITION_OF_ENTITY_BONE(PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_ROOT))
	vec = vNeck - vRoot
	RETURN 90.0 - ASIN((vNeck.z - vRoot.z)/VMAG(vec))
ENDFUNC

PROC UpdatePlayerPointing()
	
	// MJ used for speed additives
	FLOAT fMbrX, fMbrY

	// doing nothing
	IF (MPGlobalsInteractions.PointingState = POINT_INTERACTION_NULL)
		IF (MPGlobalsInteractions.bPointingDoubleTap)
			NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_NULL goto POINT_INTERACTION_LOADING") NET_NL()
			REQUEST_ANIM_DICT("anim@mp_point")
			MPGlobalsInteractions.PointingState = POINT_INTERACTION_LOADING
			MPGlobalsInteractions.bPointingDoubleTap = FALSE
			MPGlobalsInteractions.PointingTimer = GET_THIS_TIMER()
			// should we put away the weapon?
			IF IsPedEquippedWith2HandedWeapon(PLAYER_PED_ID())
				SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE, DEFAULT, DEFAULT, TRUE)	
			ENDIF
		ENDIF
	ENDIF
	
	IF (MPGlobalsInteractions.PointingState != POINT_INTERACTION_NULL) 
	AND IsSafeForPointing(FALSE)
	
//		IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
//			GET_PED_CURRENT_MOVE_BLEND_RATIO(PLAYER_PED_ID(), fMbrX, fMbrY) 
//			IF (fMbrY != 0.0)
//				fMbrY /= fMbrY
//			ENDIF
//			NET_PRINT("[interactions] UpdatePlayerPointing - fMbrY = ") NET_PRINT_FLOAT(fMbrY) NET_NL()
//			
//			NET_PRINT("[interactions] UpdatePlayerPointing - GetSpineAngle() = ") NET_PRINT_FLOAT(GetSpineAngle()) NET_NL()
//		ENDIF
	
		// should we hide weapon?
		IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
			WEAPON_TYPE CurrentWeaponType
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeaponType)
			IF NOT (CurrentWeaponType = WEAPONTYPE_UNARMED)
				MPGlobalsInteractions.PointingStoredWeapon = CurrentWeaponType
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)	
				PRINTLN("[interactions] UpdatePlayerPointing - SET_CURRENT_PED_WEAPON to UNARMED  ") 
			ENDIF
		ELSE
			RestorePointingWeapon()
		ENDIF
		
		// loading
		IF (MPGlobalsInteractions.PointingState = POINT_INTERACTION_LOADING)
			NET_PRINT("[interactions] calling DisableControlsForInteraction from POINT_INTERACTION_LOADING") NET_NL()
			DisableControlsForInteraction(PLAYER_PED_ID(), FALSE)
			IF HAS_ANIM_DICT_LOADED("anim@mp_point")
				MPGlobalsInteractions.PointingState = POINT_INTERACTION_INTRO			
				TASK_MOVE_NETWORK_BY_NAME(PLAYER_PED_ID(), "task_mp_pointing", 0.5, DEFAULT, "anim@mp_point", MOVE_SECONDARY | MOVE_USE_FIRST_PERSON_ARM_IK_LEFT)
				MPGlobalsInteractions.PointingTimer = GET_THIS_TIMER()
				MPGlobalsInteractions.PointingStartTime = GET_THIS_TIMER()
				NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_LOADING - going to INTRO (using MOVE_USE_KINEMATIC_PHYSICS)") NET_NL()
				EXIT
			ELSE
				NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_LOADING - loading...") NET_NL()	
				IF GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.PointingTimer) > 10000
					NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_LOADING - anim@mp_point never loaded, why?") NET_NL()	
					SCRIPT_ASSERT("anim@mp_point never loaded, why?")
					CleanupPointing()
				ENDIF
			ENDIF
		ENDIF
		
		
		// enter
		IF (MPGlobalsInteractions.PointingState = POINT_INTERACTION_INTRO)
			NET_PRINT("[interactions] calling DisableControlsForInteraction from POINT_INTERACTION_INTRO") NET_NL()
			DisableControlsForInteraction(PLAYER_PED_ID(), FALSE )
			IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
				IF GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "IntroFinished")
				
					// stop ped from taking off helmet while pointing
					MPGlobalsInteractions.bPointingBlockingHelmetRemoval = FALSE
					IF NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet)
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, TRUE)	
						MPGlobalsInteractions.bPointingBlockingHelmetRemoval = TRUE
						PRINTLN("[interactions] UpdatePlayerPointing - set PCF_DontTakeOffHelmet  ") 
					ENDIF
					
					MPGlobalsInteractions.PointingState = POINT_INTERACTION_LOOP	
					MPGlobalsInteractions.PointingTimer = GET_THIS_TIMER()
					NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_INTRO - finihsed going to POINT_INTERACTION_LOOP") NET_NL()	
				ELSE
					NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_INTRO - playing...") NET_NL()
					IF GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.PointingTimer) > 2000
						SCRIPT_ASSERT("Pointing intro never finished, why?")
						NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_INTRO - timeout") NET_NL()	
						CleanupPointing()					
					ENDIF
				ENDIF
			ELSE
				IF NOT (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_MOVE_NETWORK) = FINISHED_TASK)
					NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_INTRO - IS_TASK_MOVE_NETWORK_ACTIVE is false, but performing task") NET_NL()
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
		// loop
		IF (MPGlobalsInteractions.PointingState = POINT_INTERACTION_LOOP)			
			NET_PRINT("[interactions] calling DisableControlsForInteraction from POINT_INTERACTION_LOOP") NET_NL()
//			IF GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.PointingTimer) < 2000
//				DisableControlsForInteraction(PLAYER_PED_ID(), TRUE) // disable movement at first
//			ELSE
				DisableControlsForInteraction(PLAYER_PED_ID(), FALSE)
//			ENDIF
			IF (MPGlobalsInteractions.bPointingDoubleTap)
			OR IsAnyButtonPressedThatShouldQuitPointing()
			OR (GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.PointingStartTime) > 5000)
				NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_LOOP - finihsed going to POINT_INTERACTION_OUTRO") NET_NL()
				#IF IS_DEBUG_BUILD
					IF (MPGlobalsInteractions.bPointingDoubleTap)	
						NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_LOOP - double tapped out.") NET_NL()
					ENDIF
					IF IsAnyMovementPressed()	
						NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_LOOP - detected movement.") NET_NL()
					ENDIF
				#ENDIF
				IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
					REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "Stop")
					MPGlobalsInteractions.bRequestedMoveStateTransition=TRUE
					MPGlobalsInteractions.PointingTimer = GET_THIS_TIMER()
					NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_LOOP - requested move state transition.") NET_NL()
				ELSE
					MPGlobalsInteractions.bRequestedMoveStateTransition=FALSE
					NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_LOOP - could not request move state transition.") NET_NL()
				ENDIF
				MPGlobalsInteractions.PointingState = POINT_INTERACTION_OUTRO	
			ELSE
				IF IsAnyCameraMovementPressed()
					MPGlobalsInteractions.PointingStartTime = GET_THIS_TIMER()
				ENDIF
			ENDIF
		ENDIF
		
		// outro
		IF (MPGlobalsInteractions.PointingState = POINT_INTERACTION_OUTRO)
			NET_PRINT("[interactions] calling DisableControlsForInteraction from POINT_INTERACTION_OUTRO") NET_NL()
			DisableControlsForInteraction(PLAYER_PED_ID(), FALSE)
			IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())		
				CleanupPointing()
				NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_OUTRO - finished. Going to POINT_INTERACTION_NULL") NET_NL()	
			ELSE
			
//				IF NOT (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_MOVE_NETWORK) = FINISHED_TASK)
//					IF (MPGlobalsInteractions.bRequestedMoveStateTransition)
//						NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_OUTRO - waiting to finish...") NET_NL()	
//						IF GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.PointingTimer) > 2000
//							SCRIPT_ASSERT("Pointing outro never finished, why?")
//							NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_OUTRO - timeout") NET_NL()	
//							CleanupPointing()						
//						ENDIF
//					ELSE
//						IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(PLAYER_PED_ID())
//							REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(PLAYER_PED_ID(), "Stop")
//							MPGlobalsInteractions.bRequestedMoveStateTransition = TRUE
//							MPGlobalsInteractions.PointingTimer = GET_THIS_TIMER()
//							NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_OUTRO - requested move state transition.") NET_NL()
//						ENDIF
//					ENDIF
//				ELSE
//					NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_OUTRO - task finished.") NET_NL()
//					CleanupPointing()	
//				ENDIF

				// MJ listen out for the Blend_out tag/event at the end of the outro animations
				IF GET_TASK_MOVE_NETWORK_EVENT(PLAYER_PED_ID(), "BLEND_OUT")
					NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_OUTRO - Blend Out event reached...") NET_NL()
					CleanupPointing()
				ENDIF
				// MJ keep the time-out as a catch all
				IF GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.PointingTimer) > 2000
					NET_PRINT("[interactions] UpdatePlayerPointing - POINT_INTERACTION_OUTRO - timeout") NET_NL()	
					CleanupPointing()
				ENDIF
				
			ENDIF			
		ENDIF
		
		// update heading / pitch
		IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Pitch", GetPointingPitch() )
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Heading", GetPointingHeading() )
			SET_TASK_MOVE_NETWORK_SIGNAL_BOOL(PLAYER_PED_ID(), "isBlocked", IsPointingBlocked())
			
			// MJ Handle First Person Camera
			IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				SET_TASK_MOVE_NETWORK_SIGNAL_BOOL(PLAYER_PED_ID(), "isFirstPerson", TRUE )
			ELSE
				SET_TASK_MOVE_NETWORK_SIGNAL_BOOL(PLAYER_PED_ID(), "isFirstPerson", FALSE )
			ENDIF
			
			// MJ Handle speed additives
			GET_PED_CURRENT_MOVE_BLEND_RATIO(PLAYER_PED_ID(), fMbrX, fMbrY) 
			SET_TASK_MOVE_NETWORK_SIGNAL_FLOAT(PLAYER_PED_ID(), "Speed", ABSF(fMbrY) )

			//NET_PRINT("[interactions] UpdatePlayerPointing - task move network state = ") NET_PRINT(GET_TASK_MOVE_NETWORK_STATE(PLAYER_PED_ID())) NET_NL()
		ELSE
			// if anything has interrupted the move network
			IF (MPGlobalsInteractions.PointingState > POINT_INTERACTION_LOADING)
				CleanupPointing()
				NET_PRINT("[interactions] UpdatePlayerPointing - move network not active, cleaning up.") NET_NL()	
			ENDIF
		ENDIF	
	ELSE
		// if anything has interrupted the move network
		IF (MPGlobalsInteractions.PointingState != POINT_INTERACTION_NULL)
			CleanupPointing()
			NET_PRINT("[interactions] UpdatePlayerPointing - not safe for pointing, cleaning up.") NET_NL()	
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL ShouldPlaySpecialFPAnims(INTERACTION_ANIM_DATA &AnimData)
	IF (AnimData.bFirstPerson)
	AND NOT (AnimData.AnimStage = IAS_FULL_BODY)
	//AND NOT IS_PED_IN_ANY_VEHICLE(AnimData.PedID)	
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

#IF FEATURE_COPS_N_CROOKS
FUNC BOOL SHOULD_PLAY_FACIAL_ON_UPPER_ANIM(INTERACTION_ANIM_DATA &AnimData)
	SWITCH INT_TO_ENUM(PLAYER_INTERACTIONS, AnimData.iAnim)
		CASE PLAYER_INTERACTION_POUR_ONE_OUT
		CASE PLAYER_INTERACTION_HAND_SIGNALS
		CASE PLAYER_INTERACTION_TACTICAL_SALUTE
		CASE PLAYER_INTERACTION_TECHY_FINGERS
		CASE PLAYER_INTERACTION_TALK_INTO_EAR_PIECE
		CASE PLAYER_INTERACTION_BATON_TWIRL
		CASE PLAYER_INTERACTION_HAND_RUB
		CASE PLAYER_INTERACTION_SOLIDER_PEACE
		CASE PLAYER_INTERACTION_BRING_IT
		CASE PLAYER_INTERACTION_JACKED_UP
		CASE PLAYER_INTERACTION_HACKY_FINGERS
		CASE PLAYER_INTERACTION_WASNT_ME
		CASE PLAYER_INTERACTION_FLASH_BADGE
		CASE PLAYER_INTERACTION_EAT_DOUGHNUT
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC
#ENDIF
#IF FEATURE_DLC_1_2022
FUNC BOOL AreConditionsMetForInteractionSpeech(INT iAnim)
	IF(iAnim = ENUM_TO_INT(PLAYER_INTERACTION_PLAY_MASK_SFX))
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_skipMaskSFXreq")
			RETURN TRUE
		ENDIF
		#ENDIF
		INT nameHash 
		IF IS_PLAYER_FEMALE()
			 nameHash  = HASH("DLC_MP_SUM2_F_BERD_7_0")
		ELSE
			nameHash= HASH("DLC_MP_SUM2_M_BERD_7_0")
		ENDIF
		INT iDrawable = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD)
		INT iTexture = GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD)
		scrShopPedComponent componentItem
		GET_SHOP_PED_COMPONENT(nameHash, componentItem)
		
		IF componentItem.m_drawableIndex = iDrawable
		AND  componentItem.m_textureIndex = iTexture
			RETURN TRUE
		ENDIF
	ENDIF	
	
	PRINTLN("[INTERACTIONS][AreConditionsMetForInteractionSpeech] CONDITIONS NOT MET TO PLAY INTERACTION SPEECH :", iAnim)
	RETURN FALSE
ENDFUNC
PROC StartPlayingInteractionSpeech(INTERACTION_DATA &Data)
	IF(Data.bSpeechOnly)	
		IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(PLAYER_PED_ID(), Data.strSpeechContext, Data.strSpeechVoiceName, Data.strSpeechParams, TRUE)
			PRINTLN("[interactions][PLAY_MASK_SFX] StartPlayingInteractionSpeech - PLAYING SPEECH. CONTEXT:'", Data.strSpeechContext, "' VOICE:'", Data.strSpeechVoiceName, "' PARAMS:'",Data.strSpeechParams, "'")
		ELSE
			ShakeHead()	
			NET_PRINT("[interactions][PLAY_MASK_SFX] StartPlayingInteractionSpeech - Not playing speech - IS_AMBIENT_SPEECH_PLAYING = TRUE") NET_NL()
		ENDIF
	ELSE
		NET_PRINT("[interactions][PLAY_MASK_SFX] StartPlayingInteractionSpeech - Not playing speech - Data.bSpeechOnly = FALSE") NET_NL()
	ENDIF
ENDPROC
#ENDIF
PROC StartPlayingInteractionAnim(INTERACTION_ANIM_DATA &AnimData, INTERACTION_ANIM_STAGE AnimStage, BOOL bSlowBlendBodyOutro=FALSE, FLOAT fStartPhase=0.0, BOOL bSlowBlendIn=FALSE, BOOL bWaitForPlayerControl=FALSE, BOOL bWaitForSync=FALSE, BOOL bWaitForFadingIn=FALSE, BOOL bForceWeaponVisible=FALSE)

	ANIM_DATA none
	ANIM_DATA PlayAnim
	STRING sFilter
	
	INT iFlags = 0
	INT iIKFlags = 0
	BOOL bHoldLastFrame
	BOOL bLooping
	
	NET_PRINT("[interactions] StartPlayingInteractionAnim - bSlowBlendBodyOutro = ") NET_PRINT_BOOL(bSlowBlendBodyOutro) NET_NL()
	NET_PRINT("[interactions] StartPlayingInteractionAnim - fStartPhase = ") NET_PRINT_FLOAT(fStartPhase) NET_NL()
	NET_PRINT("[interactions] StartPlayingInteractionAnim - bSlowBlendIn = ") NET_PRINT_BOOL(bSlowBlendIn) NET_NL()
	
	AnimData.bSelfieMode = FALSE
	IF IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()
	AND (AnimData.PedID = PLAYER_PED_ID())
		AnimData.bSelfieMode = TRUE
		NET_PRINT("[interactions] StartPlayingInteractionAnim playing in selfie mode ") NET_NL()
	ENDIF
	
	IF (bWaitForSync)
		fStartPhase = 0.01	
		NET_PRINT("[interactions] StartPlayingInteractionAnim - bWaitForSync set, so setting startphase.") NET_NL()
	ENDIF

	// is this first person?
	AnimData.bFirstPerson = FALSE
	IF (AnimData.PedID = PLAYER_PED_ID())
		IF NOT (bWaitForSync)
			IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				AnimData.bFirstPerson = TRUE
				NET_PRINT("[interactions] playing in first person ") NET_NL()
			ENDIF
		ELSE
			NET_PRINT("[interactions] sync interaction - don't playing in first person ") NET_NL()	
		ENDIF
	ENDIF
	
		
	// main anim
	PlayAnim.type = APT_SINGLE_ANIM								
	PlayAnim.phase0 = fStartPhase
	PlayAnim.phase1 = fStartPhase
	PlayAnim.phase2 = fStartPhase
    PlayAnim.rate0 = 1.0	
	
	AnimData.AnimStage = AnimStage
	
	// *********************************************************************************************
	//				set anim flags
	// *********************************************************************************************	
	BOOL bSetAFSecondary = TRUE
	
	IF IS_PLAYER_DANCING(PLAYER_ID())
		bSetAFSecondary = FALSE		
		PRINTLN("[interactions] StartPlayingInteractionAnim - Will not set AF_SECONDARY flag as IS_LOCAL_PLAYER_DANCING_IN_NIGHTCLUB")
	ENDIF
	
	// flags
	IF bSetAFSecondary
		iFlags += ENUM_TO_INT(AF_SECONDARY)
	ENDIF
	iFlags += ENUM_TO_INT(AF_EXIT_AFTER_INTERRUPTED)
	
	// only hold last frame for intro and body of loops
	IF (AnimData.Data.bIsLooped)
	AND NOT (AnimData.AnimStage = IAS_OUTRO)
	AND NOT (AnimData.AnimStage = IAS_FULL_BODY)
		iFlags += ENUM_TO_INT(AF_HOLD_LAST_FRAME)	
		bHoldLastFrame = TRUE
	ENDIF
	
	// set the body part of a loop anim to loop
	IF (AnimData.Data.bIsLooped) 
	AND (AnimData.AnimStage = IAS_BODY)
		iFlags += ENUM_TO_INT(AF_LOOPING)
		bLooping = TRUE
	ENDIF
	
	// blend out before last frame if this is the outro
	IF (AnimData.AnimStage = IAS_OUTRO)
		iFlags += ENUM_TO_INT(AF_BLENDOUT_WRT_LAST_FRAME)	
		PRINTLN("[interactions] StartPlayingInteractionAnim - outro, so blend out before last frame")
	ENDIF
	

	// *********************************************************************************************
	//				SELECT FILTER
	// *********************************************************************************************
	INT iAnimFilter = AnimData.Data.iForcedAnimFilter 
	
	NET_PRINT("[interactions] AnimData.Data.iForcedAnimFilter = ") NET_PRINT_INT(AnimData.Data.iForcedAnimFilter) NET_NL()
	
	IF (AnimData.bSelfieMode)
		// if it's the nodd only play on head
		IF (AnimData.iType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL))
			sFilter = "BONEMASK_HEADONLY"
			PlayAnim.filter =  GET_HASH_KEY(sFilter)
			NET_PRINT("[interactions] PlayAnim.filter (for selfie) = ") NET_PRINT(sFilter) NET_NL()
		ELSE
			sFilter = "BONEMASK_HEAD_NECK_AND_L_ARM"
			PlayAnim.filter =  GET_HASH_KEY(sFilter)
			NET_PRINT("[interactions] PlayAnim.filter (for selfie) = ") NET_PRINT(sFilter) NET_NL()
		ENDIF
	ELSE	
		IF (AnimData.AnimStage = IAS_FULL_BODY)
			PlayAnim.filter = 0
			NET_PRINT("[interactions] PlayAnim.filter = 0 - full body")  NET_NL()
			IF NOT (bForceWeaponVisible)
				iFlags += ENUM_TO_INT(AF_HIDE_WEAPON)	
			ENDIF
		ELSE
			IF (iAnimFilter > FORCED_BONEMASK_NONE)
			
				// if it's the shake head, only play head
				IF (AnimData.iType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL))
				AND (AnimData.iAnim = ENUM_TO_INT(SPECIAL_INTERACTION_SHAKE_HEAD))					
					NET_PRINT("[interactions] head shake ") NET_NL()	
					IF NOT IsPedEquippedWith2HandedWeapon(AnimData.PedID)					
						sFilter = "BONEMASK_HEAD_NECK_AND_L_ARM"
						PlayAnim.filter =  GET_HASH_KEY(sFilter)
					ELSE
						
						sFilter = "FORCED_BONEMASK_HEADONLY"
						PlayAnim.filter =  GET_HASH_KEY(sFilter)
					ENDIF
					NET_PRINT("[interactions] PlayAnim.filter = ") NET_PRINT(sFilter) NET_NL()
				ELSE
			
					SWITCH iAnimFilter
						CASE FORCED_BONEMASK_HEADONLY
							sFilter = "BONEMASK_HEADONLY"
						BREAK
						CASE FORCED_BONEMASK_HEAD_NECK_AND_ARMS
							sFilter = "BONEMASK_HEAD_NECK_AND_ARMS"
						BREAK
						CASE FORCED_BONEMASK_HEAD_NECK_AND_L_ARM
							sFilter = "BONEMASK_HEAD_NECK_AND_L_ARM"
						BREAK
						CASE FORCED_BONEMASK_HEAD_NECK_AND_R_ARM
							sFilter = "BONEMASK_HEAD_NECK_AND_R_ARM"					
						BREAK
					ENDSWITCH
					
					PlayAnim.filter =  GET_HASH_KEY(sFilter)
					NET_PRINT("[interactions] PlayAnim.filter = ") NET_PRINT(sFilter) NET_NL()
					
					IF IsPedEquippedWith2HandedWeapon(AnimData.PedID)
						IF NOT (bForceWeaponVisible)
							iFlags += ENUM_TO_INT(AF_HIDE_WEAPON)
						ENDIF
						iIKFlags += ENUM_TO_INT(AIK_DISABLE_ARM_IK)
					ENDIF
				ENDIF
			ELSE
				IF (AnimData.Data.bLeftHandedOnly) 
				AND NOT IsPedEquippedWith2HandedWeapon(AnimData.PedID)
				AND NOT IS_PED_IN_ANY_VEHICLE(AnimData.PedID)
					// filter
					IF (iAnimFilter = FORCED_BONEMASK_NO_FILTER)
						sFilter = ""
						PlayAnim.filter = 0
						NET_PRINT("[interactions] PlayAnim.filter = ") NET_PRINT(sFilter) NET_NL()
					ELSE
						sFilter = "BONEMASK_HEAD_NECK_AND_L_ARM"
						PlayAnim.filter = GET_HASH_KEY(sFilter)
						NET_PRINT("[interactions] PlayAnim.filter = ") NET_PRINT(sFilter) NET_NL()	
					ENDIF
				ELSE
					IF IsPedInRearOfBohdiVehicleLayout(AnimData.PedID)
						// dont set the filter
						sFilter = ""
						PlayAnim.filter = 0
						NET_PRINT("[interactions] PlayAnim.filter = ") NET_PRINT(sFilter) NET_NL()
					ELSE
						IF (iAnimFilter = FORCED_BONEMASK_NO_FILTER)
							sFilter = ""
							PlayAnim.filter = 0
							NET_PRINT("[interactions] PlayAnim.filter = ") NET_PRINT(sFilter) NET_NL()
						ELSE
							IF (iAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE)
							AND IsSafeToUseNoFilter(AnimData.PedID)
								sFilter = ""
								PlayAnim.filter = 0
								AnimData.bPlayingNoFilterWhenStanding = TRUE
								NET_PRINT("[interactions] PlayAnim.filter = ") NET_PRINT("FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE") NET_NL()
							ELSE
								AnimData.bPlayingNoFilterWhenStanding = FALSE
								sFilter = FallBackFilterForAnim(AnimData.iType, AnimData.iAnim)
								PlayAnim.filter = GET_HASH_KEY(sFilter)
								NET_PRINT("[interactions] PlayAnim.filter = ") NET_PRINT(sFilter) NET_NL()
							ENDIF
						ENDIF
					ENDIF			
					iFlags += ENUM_TO_INT(AF_HIDE_WEAPON)	
					iIKFlags += ENUM_TO_INT(AIK_DISABLE_ARM_IK)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_aik_use_arm_allow_tags")
		iIKFlags += ENUM_TO_INT(AIK_USE_ARM_ALLOW_TAGS)
	ENDIF
	#ENDIF
	
	// first person interactions have some special flags set
	IF ShouldPlaySpecialFPAnims(AnimData)
		
		NET_PRINT("[interactions] playing first person, doing special settings... ") NET_NL()
		
		IF NOT ((iIKFlags & ENUM_TO_INT(AIK_DISABLE_ARM_IK)) = 0)
			iIKFlags -= ENUM_TO_INT(AIK_DISABLE_ARM_IK)
			NET_PRINT("[interactions] unset AIK_DISABLE_ARM_IK ") NET_NL()
		ENDIF
	
		IF NOT IS_PED_IN_ANY_VEHICLE(AnimData.PedID)
			PlayAnim.filter = 0
			NET_PRINT("[interactions] PlayAnim.filter = 0") NET_NL()
		ENDIF
			
		iIKFlags += ENUM_TO_INT(AIK_USE_FP_ARM_LEFT)	
		iIKFlags += ENUM_TO_INT(AIK_USE_FP_ARM_RIGHT)	
		iFlags += ENUM_TO_INT(AF_USE_ALTERNATIVE_FP_ANIM)
		
		IF NOT IS_PED_IN_ANY_VEHICLE(AnimData.PedID)
			iFlags += ENUM_TO_INT(AF_UPPERBODY)
		ENDIF
		
		IF ((iFlags & ENUM_TO_INT(AF_HIDE_WEAPON)) = 0)
			iFlags += ENUM_TO_INT(AF_HIDE_WEAPON)
			NET_PRINT("[interactions] setting AF_HIDE_WEAPON for 1st person ") NET_NL()
		ENDIF
	ENDIF
	

	// *********************************************************************************************
	//				set anim name
	// *********************************************************************************************
	SWITCH AnimData.AnimStage
		CASE IAS_BODY
			PlayAnim.anim0 = TextLabelToString(AnimData.Data.strAnimName) 				
		BREAK
		CASE IAS_INTRO
			PlayAnim.anim0 = TextLabelToString(AnimData.Data.strAnimNameIntro) 
		BREAK
		CASE IAS_OUTRO
			PlayAnim.anim0 =  TextLabelToString(AnimData.Data.strAnimNameOutro) 
		BREAK
		CASE IAS_FULL_BODY
			PlayAnim.anim0 =  TextLabelToString(AnimData.Data.strAnimNameFullBody)						
		BREAK
	ENDSWITCH
	
	IF NOT (AnimData.AnimStage = IAS_FULL_BODY)	
		PlayAnim.dictionary0 = InteractionAnimDictFromInteractionAnimData(AnimData, FALSE, AnimData.bSelfieMode)
	ELSE
		PlayAnim.dictionary0 = InteractionAnimDictFromInteractionAnimData(AnimData, TRUE)
	ENDIF	
	
	// start time?
	FLOAT fStartPhaseTime
	FLOAT fEndPhaseTime
	IF NOT (fStartPhase > 0.0)
		IF FIND_ANIM_EVENT_PHASE(PlayAnim.dictionary0, PlayAnim.anim0, "START", fStartPhaseTime, fEndPhaseTime)
			PlayAnim.phase0 = fStartPhaseTime
			NET_PRINT("[interactions] PlayAnim.phase0 = ") NET_PRINT_FLOAT(PlayAnim.phase0) NET_NL()
		ENDIF
	ENDIF
	
	IF IS_PLAYER_DANCING(PLAYER_ID())
		IF MPGlobalsInteractions.PlayerInteraction.bPlayInteractionAnimFullBody	OR (AnimData.AnimStage = IAS_INTRO)
			PlayAnim.phase0 = GET_DANCE_INTERACTION_ANIM_ENTER_EVENT_PHASE(PlayAnim.dictionary0, PlayAnim.anim0)
		ENDIF
		PRINTLN("[interactions] StartPlayingInteractionAnim - Player is Dancing getting phase from ENTER event PlayAnim.phase0 = ", PlayAnim.phase0)
	ENDIF
	
	// is the start time past the 'create' event for the prop?
	IF (AnimData.Data.bHasProp)
		IF FIND_ANIM_EVENT_PHASE(PlayAnim.dictionary0, PlayAnim.anim0, "CREATE", fStartPhaseTime, fEndPhaseTime)
		OR FIND_ANIM_EVENT_PHASE(PlayAnim.dictionary0, PlayAnim.anim0, "Create_whiskey_bottle", fStartPhaseTime, fEndPhaseTime)
			IF (fStartPhaseTime <= PlayAnim.phase0)
				AnimData.bCreateProp = TRUE	
				NET_PRINT("[interactions] prop create time < anim start time ")  NET_NL()
			ENDIF
		ELSE
			// if there is no event to create the prop, but this is sync scene then give prop.
			IF (bWaitForSync)
				AnimData.bCreateProp = TRUE	
				NET_PRINT("[interactions] no create event, but creating for sync scene")  NET_NL()
			ENDIF	
		ENDIF
	ENDIF
	
	// if we are going to wait for player control, then set the rate to 0 until control gets turned on.
	IF (bWaitForPlayerControl)
	OR (bWaitForSync)
	OR (bWaitForFadingIn)
		PlayAnim.rate0 = 0.0
		AnimData.bIsPaused = TRUE
		NET_PRINT("[interactions] setting rate to 0.0 until player control is active ")  NET_NL()
	ELSE
		AnimData.bIsPaused = FALSE
	ENDIF
	
	// *********************************************************************************************
	//				set blends
	// *********************************************************************************************		
	FLOAT fBlendIn, fBlendOut	
	SWITCH AnimData.AnimStage
		CASE IAS_INTRO
			IF IS_PED_IN_ANY_VEHICLE(AnimData.PedID)
				fBlendIn = SLOW_BLEND_DURATION
			ELSE
				fBlendIn = 0.5 //SLOW_BLEND_DURATION
			ENDIF
			fBlendOut = INSTANT_BLEND_DURATION
		BREAK
		CASE IAS_BODY
			IF (bSlowBlendIn)
				fBlendIn = SLOW_BLEND_DURATION
			ELSE
				fBlendIn = INSTANT_BLEND_DURATION
			ENDIF
			IF (bSlowBlendBodyOutro)
				fBlendOut = SLOW_BLEND_DURATION
			ELSE
				fBlendOut = INSTANT_BLEND_DURATION
			ENDIF
		BREAK
		CASE IAS_OUTRO
			IF (bSlowBlendBodyOutro)
			OR (bSlowBlendIn)
				fBlendIn = SLOW_BLEND_DURATION
			ELSE
				fBlendIn = INSTANT_BLEND_DURATION
			ENDIF
			IF IS_PLAYER_DANCING(PLAYER_ID())
				fBlendOut = SLOW_BLEND_DURATION * 2
			ELSE
				fBlendOut = SLOW_BLEND_DURATION
			ENDIF
		BREAK
		CASE IAS_FULL_BODY
			fBlendIn = SLOW_BLEND_DURATION
			IF IS_PLAYER_DANCING(PLAYER_ID())
				fBlendOut = SLOW_BLEND_DURATION * 2
			ELSE
				fBlendOut = SLOW_BLEND_DURATION
			ENDIF
		BREAK
	ENDSWITCH
	
	IF ((PlayAnim.phase0 > 0.0) AND (bSlowBlendIn = FALSE))
	OR (AnimData.bIsPaused)
		fBlendIn = INSTANT_BLEND_DURATION
	ELSE
		PRINTLN("[interactions] - dont overwrite fBlendIn")
	ENDIF
	
	
	NET_PRINT("[interactions] fBlendIn = ") NET_PRINT_FLOAT(fBlendIn) 
	NET_PRINT(", fBlendOut = ") NET_PRINT_FLOAT(fBlendOut)
	NET_NL()
	
	// *********************************************************************************************
	//				other settings
	// *********************************************************************************************	
	
	IF (AnimData.bSelfieMode)
		iIKFlags += ENUM_TO_INT(AIK_DISABLE_HEAD_IK) // disable head ik for full body - see 1802855 
	ENDIF
	IF (AnimData.AnimStage = IAS_FULL_BODY)
		iIKFlags = ENUM_TO_INT(AIK_DISABLE_ARM_IK) + ENUM_TO_INT(AIK_DISABLE_HEAD_IK) + ENUM_TO_INT(AIK_DISABLE_TORSO_IK) + ENUM_TO_INT(AIK_DISABLE_TORSO_REACT_IK)
	ENDIF
	
	AnimData.fStartPhase = fStartPhase
	AnimData.bSlowBlendBodyOutro = bSlowBlendBodyOutro
	
	PlayAnim.flags = INT_TO_ENUM(ANIMATION_FLAGS, iFlags)
	PlayAnim.ikFlags = INT_TO_ENUM(IK_CONTROL_FLAGS, iIKFlags)
	
	AnimData.iAnimFlags = iFlags // store so we know later if the weapon should be hidden
	
	NET_PRINT("[interactions] PlayAnim iFlags = ") NET_PRINT_INT(iFlags) NET_NL()		
	NET_PRINT("[interactions] PlayAnim ikFlags = ") NET_PRINT_INT(iIKFlags) NET_NL()
	NET_PRINT("[interactions] PlayAnim.anim0 = ") NET_PRINT(PlayAnim.anim0) NET_NL()							
	NET_PRINT("[interactions] PlayAnim.dictionary0 = ") NET_PRINT(PlayAnim.dictionary0) NET_NL()			
	
	// *********************************************************************************************
	//				give task
	// *********************************************************************************************		
	// clear any look at task
	IF NOT (AnimData.bSelfieMode)
		TASK_CLEAR_LOOK_AT(AnimData.PedID)
	ENDIF
	
	// check if player is walking or driving (from after a transition)
	SCRIPTTASKSTATUS TaskStatus
	TaskStatus = GET_SCRIPT_TASK_STATUS(AnimData.PedID, SCRIPT_TASK_WANDER_STANDARD)
	IF (TaskStatus = FINISHED_TASK)
		TaskStatus = GET_SCRIPT_TASK_STATUS(AnimData.PedID, SCRIPT_TASK_VEHICLE_DRIVE_WANDER)
	ENDIF
		
	// if this is a full body anim, then play the facial as a separate task (see 1796784)
	IF (AnimData.AnimStage = IAS_FULL_BODY #IF FEATURE_COPS_N_CROOKS OR SHOULD_PLAY_FACIAL_ON_UPPER_ANIM(AnimData) #ENDIF )
	AND NOT (bWaitForPlayerControl)
	AND NOT (bWaitForSync)
	AND NOT (bWaitForFadingIn)
		TEXT_LABEL_63 strFacial
		strFacial = PlayAnim.anim0
		strFacial += "_Facial"
		PLAY_FACIAL_ANIM(AnimData.PedID, strFacial, PlayAnim.dictionary0)
		NET_PRINT("[interactions] PLAY_FACIAL_ANIM - called with ") NET_PRINT(strFacial) NET_NL()
	ENDIF
		
	IF (TaskStatus = FINISHED_TASK)
	AND ((NOT g_bScriptsSetSafeForCutscene) OR g_bCelebrationScreenIsActive)	
	AND NOT (AnimData.bSelfieMode)
		OPEN_SEQUENCE_TASK(AnimData.Seq)	
			TASK_SCRIPTED_ANIMATION(NULL, PlayAnim, none, none, fBlendIn, fBlendOut)		
		CLOSE_SEQUENCE_TASK(AnimData.Seq)
		TASK_PERFORM_SEQUENCE(AnimData.PedID, AnimData.Seq)
		CLEAR_SEQUENCE_TASK(AnimData.Seq)
		NET_PRINT("[interactions] played TASK_SCRIPTED_ANIMATION as part of sequence task.") NET_NL()
	ELSE
		IF (AnimData.bSelfieMode)
			TASK_PLAY_PHONE_GESTURE_ANIMATION(AnimData.PedID, PlayAnim.dictionary0, PlayAnim.anim0, sFilter, fBlendIn, fBlendOut, bLooping, bHoldLastFrame)
		ELSE
			TASK_SCRIPTED_ANIMATION(AnimData.PedID, PlayAnim, none, none, fBlendIn, fBlendOut)
		ENDIF
		NET_PRINT("[interactions] played TASK_SCRIPTED_ANIMATION as primary task.") NET_NL()
	ENDIF
	
	IF (AnimData.iAnim = ENUM_TO_INT(PASSENGER_INTERACTION_SMOKE))
	OR (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_SMOKE))	
		IF (AnimData.PedID = PLAYER_PED_ID())
			// stop broadcasting smoking ptfx
			IF (AnimData.AnimStage = IAS_INTRO)
			OR (AnimData.AnimStage = IAS_BODY)
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsSmoking)
			ENDIF
			IF (AnimData.AnimStage = IAS_OUTRO)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlaySmokingPFX)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_IsSmoking)
				NET_PRINT("[interactions] bPlaySmokingPFX = FALSE 1 ") NET_NL()	
			ENDIF
		ENDIF
	ENDIF
	
	IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_MAKE_IT_RAIN))
		IF (AnimData.PedID = PLAYER_PED_ID())
			// stop broadcasting ptfx
			IF (AnimData.AnimStage = IAS_OUTRO)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayMakeItRainPFX)
				NET_PRINT("[interactions] bPlayMakeItRainPFX = FALSE 1 ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_DRINK_3))
		IF (AnimData.PedID = PLAYER_PED_ID())
			// stop broadcasting ptfx
			IF (AnimData.AnimStage = IAS_OUTRO)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayChampagnePFX)
				NET_PRINT("[interactions] bPlayChampagnePFX = FALSE 1 ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	#IF FEATURE_COPS_N_CROOKS
	IF (AnimData.iAnim = ENUM_TO_INT(PLAYER_INTERACTION_POUR_ONE_OUT))
		IF (AnimData.PedID = PLAYER_PED_ID())
			// stop broadcasting ptfx
			IF (AnimData.AnimStage = IAS_OUTRO)
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iPlayerInteractionFlags, PIBD_PlayWhiskeyPFX)
				NET_PRINT("[interactions] bPlayChampagnePFX = FALSE 1 ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	#ENDIF
		
	AnimData.bStarted = TRUE
	
//	// is there a custom object anim?
//	IF DOES_CUSTOM_OBJECT_ANIM_EXIST(AnimData.iType, AnimData.iAnim)		
//		CreateCustomObject(AnimData)
//		StartCustomObjectAnim(AnimData, PlayAnim.dictionary0, PlayAnim.rate0, AnimData.fStartPhase)
//	ENDIF
	
	
ENDPROC

FUNC FLOAT GetNextAnimPhase(INTERACTION_ANIM_DATA &AnimData)
	
	FLOAT fPhaseDiff
	FLOAT fReturn = -1.0
	FLOAT fAnimPhase = GetCurrentAnimPhase(AnimData)

		
	NET_PRINT("[interactions] GetNextAnimPhase - fAnimPhase = ") NET_PRINT_FLOAT(fAnimPhase) NET_NL()
	NET_PRINT("[interactions] GetNextAnimPhase - AnimData.Data.fLastPhase = ") NET_PRINT_FLOAT(AnimData.Data.fLastPhase) NET_NL()
		
	fPhaseDiff = fAnimPhase - AnimData.Data.fLastPhase
	NET_PRINT("[interactions] GetNextAnimPhase - fPhaseDiff = ") NET_PRINT_FLOAT(fPhaseDiff) NET_NL()
		
	IF (fPhaseDiff > 0.0)
		fReturn = fAnimPhase + fPhaseDiff
	ENDIF

	NET_PRINT("[interactions] GetNextAnimPhase - fReturn (pre tidy) = ") NET_PRINT_FLOAT(fPhaseDiff) NET_NL()

	IF (fReturn > 1.0)
	AND (fReturn < 2.0)
		fReturn += -1.0
	ENDIF
	
	IF (fReturn >= 2.0)
		fReturn = -1.0
	ENDIF
	
	NET_PRINT("[interactions] GetNextAnimPhase - fReturn = ") NET_PRINT_FLOAT(fReturn) NET_NL()
	RETURN fReturn
	
ENDFUNC

PROC UpdateNoFilterIfPossible(INTERACTION_ANIM_DATA &AnimData)
	FLOAT fNextPhase
	IF (AnimData.Data.iForcedAnimFilter = FORCED_BONEMASK_NO_FILTER_IF_POSSIBLE)
		IF AnimData.bPlayingNoFilterWhenStanding = TRUE
			IF NOT IsSafeToUseNoFilter(AnimData.PedID)	
				fNextPhase = GetNextAnimPhase(AnimData)
				IF (fNextPhase > 0.0)
					NET_PRINT("[interactions] UpdateNoFilterIfPossible - no longer safe to use no filter.") NET_NL()
					StartPlayingInteractionAnim(AnimData, AnimData.AnimStage, AnimData.bSlowBlendBodyOutro, fNextPhase, TRUE)
				ENDIF
			ENDIF
		ELSE
			// only change to no filter if playing body
			IF (AnimData.AnimStage = IAS_BODY)
				IF IsSafeToUseNoFilter(AnimData.PedID)
					fNextPhase = GetNextAnimPhase(AnimData)
					IF (fNextPhase > 0.0)
						NET_PRINT("[interactions] UpdateNoFilterIfPossible - now safe to use no filter.") NET_NL()
						StartPlayingInteractionAnim(AnimData, AnimData.AnimStage, AnimData.bSlowBlendBodyOutro, fNextPhase, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if the action <iActionAnim> is suitable to be cycled 
///    through the dancing minigame system (displayed on the dancing help text).
///    NOTE: <iActionAnim> should be a value from enum PLAYER_INTERACTION.
///    
///    IMPORTANT: All action UPPERBODY clips here are loaded in until cleanup!
///    
FUNC BOOL DANCING_IS_INTERACTION_A_DANCE_INTERACTION(INT iActionAnim)
	PLAYER_INTERACTIONS eAnim = INT_TO_ENUM(PLAYER_INTERACTIONS, iActionAnim)
	RETURN eAnim = PLAYER_INTERACTION_BANGING_TUNES_LEFT
		OR eAnim = PLAYER_INTERACTION_BANGING_TUNES
		OR eAnim = PLAYER_INTERACTION_BANGING_TUNES_RIGHT
		OR eAnim = PLAYER_INTERACTION_CATS_CRADLE
		OR eAnim = PLAYER_INTERACTION_FIND_THE_FISH
		OR eAnim = PLAYER_INTERACTION_HEART_PUMPING
		OR eAnim = PLAYER_INTERACTION_OH_SNAP
		OR eAnim = PLAYER_INTERACTION_RAISE_THE_ROOF
		OR eAnim = PLAYER_INTERACTION_SALSA_ROLL
		OR eAnim = PLAYER_INTERACTION_UNCLE_DISCO
		#IF FEATURE_HEIST_ISLAND_DANCES
		OR eAnim = PLAYER_INTERACTION_CROWD_INVITATION
		OR eAnim = PLAYER_INTERACTION_DRIVER
		OR eAnim = PLAYER_INTERACTION_RUNNER
		OR eAnim = PLAYER_INTERACTION_SHOOTING
		OR eAnim = PLAYER_INTERACTION_SUCK_IT
		OR eAnim = PLAYER_INTERACTION_TAKE_SELFIE
		#ENDIF
ENDFUNC

PROC UpdateEarlyExitFromFullBody(INTERACTION_ANIM_DATA &AnimData, BOOL &bDoCleanup)
	IF DANCING_IS_INTERACTION_A_DANCE_INTERACTION(AnimData.iAnim) AND NOT IS_PLAYER_DANCING(PLAYER_ID())
		NET_PRINT("[interactions] UpdateEarlyExitFromFullBody - skipping early exits for dance interaction while not dancing.") NET_NL()
		EXIT
	ENDIF
	
	//FLOAT fNextPhase
	IF (AnimData.AnimStage = IAS_FULL_BODY)
	OR (AnimData.AnimStage = IAS_OUTRO)
		IF HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("EARLYEXIT"))
		OR HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("breakout"))
		OR HAS_ANIM_EVENT_FIRED(AnimData.PedID, GET_HASH_KEY("EXIT"))
		#IF IS_DEBUG_BUILD
		OR (MPGlobalsInteractions.bSimulateEarlyExitEvent)	
		#ENDIF

			NET_PRINT("[interactions] UpdateEarlyExitFromFullBody - recieved early exit event.") NET_NL()

			bDoCleanup = TRUE
			
			#IF IS_DEBUG_BUILD
			IF HAS_ANIM_EVENT_FIRED(AnimData.PedID, HASH("EXIT"))
				NET_PRINT("[interactions] UpdateEarlyExitFromFullBody - recieved EXIT event for dance emote") NET_NL()
			ENDIF	
			
			IF (MPGlobalsInteractions.bSimulateEarlyExitEvent)
				MPGlobalsInteractions.bSimulateEarlyExitEvent = FALSE
			ENDIF
			#ENDIF
			
		ENDIF
	ENDIF
ENDPROC

//PROC RemoveHelmet(PED_INDEX PedID, INT iPlayerInt )	
//	IF (NOT (IsPedPlayerPed(PedID)) AND (iPlayerInt > -1))
//		
//		NET_PRINT("[interactions] RemoveHelmet - on non player ped.") NET_NL()
//		
//		// store hair
//		PED_COMP_ITEM_DATA_STRUCT sTempCompData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PedID), COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[iPlayerInt].iMyCurrentHair))
//
//		// remove helmet
//		SET_PED_COMP_ITEM_CURRENT_MP(PedID, COMP_TYPE_PROPS, PROPS_HEAD_NONE)
//		
//		// restore hair
//		SET_PED_COMPONENT_VARIATION(PedID, PED_COMP_HAIR, sTempCompData.iDrawable, sTempCompData.iTexture)
//		
//	ELSE
//		
//		NET_PRINT("[interactions] RemoveHelmet - on player ped.") NET_NL()
//	
//		SET_PED_COMP_ITEM_CURRENT_MP(PedID, COMP_TYPE_PROPS, PROPS_HEAD_NONE)
//	ENDIF
//ENDPROC
//
//PROC RemoveMask(PED_INDEX PedID, INT iPlayerInt )	
//	IF (NOT (IsPedPlayerPed(PedID)) AND (iPlayerInt > -1))
//		
//		NET_PRINT("[interactions] RemoveMask - on non player ped.") NET_NL()
//		
//		// store hair
//		PED_COMP_ITEM_DATA_STRUCT sTempCompData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PedID), COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[iPlayerInt].iMyCurrentHair))
//
//		// remove mask
//		SET_PED_COMP_ITEM_CURRENT_MP(PedID, COMP_TYPE_BERD, PROPS_HEAD_NONE)
//		
//		// restore hair
//		SET_PED_COMPONENT_VARIATION(PedID, PED_COMP_HAIR, sTempCompData.iDrawable, sTempCompData.iTexture)
//		
//	ELSE
//		
//		NET_PRINT("[interactions] RemoveMask - on player ped.") NET_NL()
//	
//		SET_PED_COMP_ITEM_CURRENT_MP(PedID, COMP_TYPE_BERD, PROPS_HEAD_NONE)
//	ENDIF
//ENDPROC


//PROC UpdateRemoveMaskOrHelmet(PED_INDEX PedID, INTERACTION_DATA &Data, INT iPlayerInt )
//	
//	// remove helmet if necessary
//	IF IS_PED_IS_WEARING_A_CLOSED_HELMET(PedID)
//		IF (Data.bHasProp)
//		OR (Data.bRemoveClosedMaskHelmet)										
//			NET_PRINT("[interactions] UpdateRemoveMaskOrHelmet - removing helmet.") NET_NL()
//			
//			RemoveHelmet(PedID, iPlayerInt )	
//		ENDIF
//	ENDIF
//
//	// remove mask if necessary
//	IF IS_PED_IS_WEARING_A_CLOSED_MASK(PedID)
//		IF (Data.bHasProp)
//		OR (Data.bRemoveClosedMaskHelmet)
//			NET_PRINT("[interactions] UpdateRemoveMaskOrHelmet - removing mask.") NET_NL()
//			RemoveMask(PedID, iPlayerInt )	
//		ENDIF
//	ENDIF
//ENDPROC

FUNC BOOL IsAnimAtDesiredStartPhase(INTERACTION_ANIM_DATA &AnimData)

	STRING AnimName
	STRING AnimDict
	BOOL bIsFullBody
	
	IF (AnimData.bStarted)
	
		IF (AnimData.AnimStage = IAS_FULL_BODY)
			bIsFullBody = TRUE
		ENDIF	
		
		AnimDict = InteractionAnimDictFromInteractionAnimData(AnimData, bIsFullBody, AnimData.bSelfieMode)
		AnimName = GetCurrentAnimName(AnimData)		
		
		PRINTLN("[interactions] IsAnimAtDesiredStartPhase AnimDict = ", AnimDict, ", AnimName = ", AnimName)
			
		IF IS_ENTITY_PLAYING_ANIM(AnimData.PedID, AnimDict, AnimName)		
			IF (GET_ENTITY_ANIM_CURRENT_TIME(AnimData.PedID, AnimDict, AnimName) >= AnimData.fStartPhase)

				PRINTLN("[interactions] IsAnimAtDesiredStartPhase  RETURNING TRUE ") 
				
				RETURN(TRUE)
				
			ELSE
				SET_ENTITY_ANIM_CURRENT_TIME(AnimData.PedID, AnimDict, AnimName, AnimData.fStartPhase)
				PRINTLN("[interactions] IsAnimAtDesiredStartPhase waiting for current anim time to equal start phase. ") 
			ENDIF	
		ELSE
			PRINTLN("[interactions] IsAnimAtDesiredStartPhase waiting for current anim time to equal start phase. ") 	
		ENDIF
		
	ELSE
		PRINTLN("[interactions] IsAnimAtDesiredStartPhase - not yet started. ") 	
	ENDIF
	
	RETURN(FALSE)
	
ENDFUNC


PROC UpdateAnimRate(INTERACTION_ANIM_DATA &AnimData, INTERACTION_GLOBAL_DATA &GlobalData)
	// set the rate back to 1.0 when player control gets turned on
	STRING AnimDict
	STRING AnimName
	BOOL bIsFullBody 
	
	IF ((GlobalData.bWaitForPlayerControl=TRUE) OR (GlobalData.bWaitForSync=TRUE) OR (GlobalData.bWaitForFadingIn=TRUE))
		
		IF ((GlobalData.bWaitForPlayerControl=TRUE) AND IS_PLAYER_CONTROL_ON(PLAYER_ID()))
		OR ((GlobalData.bWaitForSync=TRUE) AND (MPGlobalsInteractions.bHasBeenSynced))
		OR ((GlobalData.bWaitForFadingIn=TRUE) AND (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN()))
		OR (IS_PLAYER_SWITCH_IN_PROGRESS() AND (GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_OUTRO_SWOOP))
		OR IS_MP_HEIST_PRE_PLANNING_CURRENTLY_IN_USE()
		OR IS_MP_HEIST_PLANNING_ACTIVE()
		
			IF GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() 
			
				IF IsAnimAtDesiredStartPhase(AnimData)
			
					IF (AnimData.AnimStage = IAS_FULL_BODY)
						bIsFullBody = TRUE
					ENDIF	
					
					AnimDict = InteractionAnimDictFromInteractionAnimData(AnimData, bIsFullBody, AnimData.bSelfieMode)
					AnimName = GetCurrentAnimName(AnimData)				
			
					SET_ENTITY_ANIM_SPEED(AnimData.PedID, AnimDict, AnimName, 1.0)	
					
					GlobalData.bWaitForPlayerControl = FALSE
					GlobalData.bWaitForSync = FALSE
					GlobalData.bWaitForFadingIn = FALSE
					AnimData.bIsPaused = FALSE
					
					PRINTLN("[interactions] UpdateAnimRate set rate to 1.0 ") 
					
	//				// is there a custom object anim?
	//				IF DOES_CUSTOM_OBJECT_ANIM_EXIST(AnimData.iType, AnimData.iAnim)
	//					CUSTOM_OBJECT_ANIM_DATA CustomObjectData
	//					GET_CUSTOM_OBJECT_ANIM_DETAILS(AnimData.iType, AnimData.iAnim, CustomObjectData)
	//					SET_ENTITY_ANIM_SPEED(AnimData.CustomObjectID, AnimDict, CustomObjectData.strAnimName, 1.0)
	//				ENDIF
						
				ELSE
					PRINTLN("[interactions] UpdateAnimRate not playing anim.") 
				ENDIF
			ELSE
				PRINTLN("[interactions] UpdateAnimRate - waiting for GET_TOGGLE_PAUSED_RENDERPHASES_STATUS") 
			ENDIF
		ELSE
			PRINTLN("[interactions] UpdateAnimRate - ((GlobalData.bWaitForPlayerControl=TRUE) AND IS_PLAYER_CONTROL_ON(PLAYER_ID())) = ", ((GlobalData.bWaitForPlayerControl=TRUE) AND IS_PLAYER_CONTROL_ON(PLAYER_ID())))
			PRINTLN("[interactions] UpdateAnimRate - ((GlobalData.bWaitForSync=TRUE) AND (MPGlobalsInteractions.bHasBeenSynced)) = ", ((GlobalData.bWaitForSync=TRUE) AND (MPGlobalsInteractions.bHasBeenSynced)))
			PRINTLN("[interactions] UpdateAnimRate - ((GlobalData.bWaitForFadingIn=TRUE) AND (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN())) = ", ((GlobalData.bWaitForFadingIn=TRUE) AND (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN())))
			PRINTLN("[interactions] UpdateAnimRate - (IS_PLAYER_SWITCH_IN_PROGRESS() AND (GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_OUTRO_SWOOP)) = ", (IS_PLAYER_SWITCH_IN_PROGRESS() AND (GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_OUTRO_SWOOP)))
			PRINTLN("[interactions] UpdateAnimRate - IS_MP_HEIST_PRE_PLANNING_CURRENTLY_IN_USE() = ", IS_MP_HEIST_PRE_PLANNING_CURRENTLY_IN_USE())
			PRINTLN("[interactions] UpdateAnimRate - IS_MP_HEIST_PLANNING_ACTIVE() = ", IS_MP_HEIST_PLANNING_ACTIVE())
		ENDIF
	ENDIF
	
	IF (AnimData.bIsPaused)
		StoreWeapon(AnimData, GlobalData.bWaitForSync)
	ENDIF
	
ENDPROC

FUNC BOOL IsPlayerAimingOrAttackingInAVehicle(BOOL bIsDriver)

	IF(bIsDriver)
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		
			#IF IS_DEBUG_BUILD
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)
					NET_PRINT("[interactions] IsPlayerAimingOrAttackingInAVehicle - INPUT_VEH_AIM ") NET_NL()
				ENDIF
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK)
					NET_PRINT("[interactions] IsPlayerAimingOrAttackingInAVehicle - INPUT_VEH_ATTACK ") NET_NL()
				ENDIF
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
					NET_PRINT("[interactions] IsPlayerAimingOrAttackingInAVehicle - INPUT_VEH_ATTACK2 ") NET_NL()
				ENDIF
			#ENDIF
		
			RETURN(TRUE)
		ENDIF
	ELSE
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
		
			#IF IS_DEBUG_BUILD
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
					NET_PRINT("[interactions] IsPlayerAimingOrAttackingInAVehicle - INPUT_VEH_PASSENGER_AIM ") NET_NL()
				ENDIF
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
					NET_PRINT("[interactions] IsPlayerAimingOrAttackingInAVehicle - INPUT_VEH_PASSENGER_ATTACK ") NET_NL()
				ENDIF
			#ENDIF
		
			RETURN(TRUE)
		ENDIF	 
	ENDIF
	

	RETURN(FALSE)

ENDFUNC



PROC UpdateInteractionWhileAnimPlays(INTERACTION_ANIM_DATA &AnimData, INTERACTION_GLOBAL_DATA &GlobalData, BOOL &bDoCleanup) //, INT iPlayerInt)

	IF NOT IS_PED_INJURED(AnimData.PedID)
			
		UpdateAnimRate(AnimData, GlobalData)
		UpdateAnimSounds(AnimData)
		UpdateAnimPropsAndVFX(AnimData, GlobalData)
		UpdateNoFilterIfPossible(AnimData)
		UpdateEarlyExitFromFullBody(AnimData, bDoCleanup)	
		UpdateHideWeapon(AnimData)

		
		//UpdateRemoveMaskOrHelmet(AnimData.PedID, AnimData.Data, iPlayerInt)
		
		// stop anim if no longer appropriate to play
		IF NOT ARE_CONDITIONS_MET_TO_CONTINUE_INTERACTION_TYPE(AnimData.iType)	
			NET_PRINT("[interactions] UpdateInteractionWhileAnimPlays - CLEAR_PED_TASKS called 1 ") NET_NL()
			bDoCleanup = TRUE
		ENDIF		
		
		// if playing in first person make sure we dont come out of strafing.2036571
		IF ShouldPlaySpecialFPAnims(AnimData)		
			//B*2301787
			IF NOT NETWORK_IS_GAME_IN_PROGRESS() 
			AND IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				MODEL_NAMES playerModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
				IF playerModel = PLAYER_ZERO
				OR playerModel = PLAYER_ONE
				OR playerModel = PLAYER_TWO
				OR playerModel = MP_F_FREEMODE_01
				OR playerModel = MP_M_FREEMODE_01					
					SET_PED_RESET_FLAG(AnimData.PedID, PRF_ForcePedToStrafe, TRUE)	
				ENDIF
			ELSE			
				SET_PED_RESET_FLAG(AnimData.PedID, PRF_ForcePedToStrafe, TRUE)	
			ENDIF
		ENDIF
		
		// has player tried to exit vehicle or do driveby
		IF IsPedPlayerPed(AnimData.PedID)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND (IsPlayerAimingOrAttackingInAVehicle(AnimData.bIsDriver) OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT) )
				 
				#IF IS_DEBUG_BUILD
				IF IsPlayerAimingOrAttackingInAVehicle(AnimData.bIsDriver)
					NET_PRINT("[interactions] UpdateInteractionWhileAnimPlays - CLEAR_PED_TASKS - IsPlayerAimingOrAttackingInAVehicle") NET_NL()
				ENDIF				
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
					NET_PRINT("[interactions] UpdateInteractionWhileAnimPlays - CLEAR_PED_TASKS - INPUT_VEH_EXIT") NET_NL()
				ENDIF
				#ENDIF
				 
				NET_PRINT("[interactions] UpdateInteractionWhileAnimPlays - CLEAR_PED_TASKS called 2") NET_NL()
				bDoCleanup = TRUE	
			ENDIF
		ENDIF
		
		// store the current phase of the anim for the next frame
		AnimData.Data.fLastPhase = GetCurrentAnimPhase(AnimData)
		
	ENDIF

ENDPROC

FUNC BOOL OverrideInteractionConditionsCheck()
	IF IS_PLAYER_IN_CORONA()
	OR (g_bScriptsSetSafeForCutscene)
	OR (IS_PLAYER_DANCING(PLAYER_ID()))
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC


PROC PrintInteractionFlags(INTERACTION_INTERNAL_DATA &StoredInteractionData, INTERACTION_GLOBAL_DATA &GlobalData)

	NET_PRINT("[interactions] PrintInteractionFlags - Global Data: = ") NET_NL()
	#IF IS_DEBUG_BUILD
	PRINT_INTERACTION_GLOBAL_DATA(GlobalData)
	#ENDIF
	
	#IF NOT IS_DEBUG_BUILD
	UNUSED_PARAMETER(GlobalData)
	#ENDIF

	
	NET_PRINT("[interactions] StoredInteractionData.bPlayingFullBody = ") NET_PRINT_BOOL(StoredInteractionData.bPlayingFullBody) NET_NL()
	NET_PRINT("[interactions] StoredInteractionData.bPlayingSelfie = ") NET_PRINT_BOOL(StoredInteractionData.bPlayingSelfie) NET_NL()
	//NET_PRINT("[interactions] StoredInteractionData.bPlayingFullBodyMask = ") NET_PRINT_BOOL(StoredInteractionData.bPlayingFullBodyMask) NET_NL()
	NET_PRINT("[interactions] StoredInteractionData.bPlayingSelectedAnim = ") NET_PRINT_BOOL(StoredInteractionData.bPlayingSelectedAnim) NET_NL()
	
	
ENDPROC

PROC UPDATE_INTERACTION_ANIM_INTERNAL(INTERACTION_INTERNAL_DATA &StoredInteractionData, INTERACTION_GLOBAL_DATA &GlobalData, PED_INDEX PedID, BOOL bDoSnackCheck) //, INT iPlayerInt=-1)
				
			
	StoredInteractionData.SelectedInteraction.PedID = PedID
		
	// store if this is male or female (so we know what full body anim to clear up - 2541105)
	IF DOES_ENTITY_EXIST(PedID)
	AND NOT IS_ENTITY_DEAD(PedID)
	AND NOT (PedID = PLAYER_PED_ID())
		IF IS_PED_MALE(PedID)
			StoredInteractionData.SelectedInteraction.bIsMale = TRUE
		ELSE
			StoredInteractionData.SelectedInteraction.bIsMale = FALSE
		ENDIF
	ENDIF	
				
	// if we are telling the anim to eat a snack, check we are selecting the relevant one.
	IF (bDoSnackCheck)
		IF (GlobalData.bPlayInteractionAnim)
			IF NOT (StoredInteractionData.bPlayingSelectedAnim)
				IF IsInteractionASnack(GlobalData)
					// choose appropriate snack
					ChooseAppropriateSnack(GlobalData)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
								
	// make sure the selected interaction anim is loaded
	IF (GlobalData.iInteractionType > -1)
	AND (GlobalData.iInteractionAnim > -1)
	AND NOT (GlobalData.bCleanupInteractionAnimImmediately)
	AND DOES_ENTITY_EXIST(PedID)
	
		//ANIM_DATA animINTRO
		//ANIM_DATA animLOOP
		//ANIM_DATA animNONLOOP
		//ANIM_DATA animOUTRO			
	
		GlobalData.bHasCleanedUp = FALSE
	
		// if another interaction is playing, but not this one then clean this up.
		IF IsPedPlayerPed(PedID)
			IF (MPGlobalsInteractions.bAnyInteractionIsPlaying)
			AND NOT (GlobalData.bInteractionIsPlaying)
				GlobalData.bCleanupInteractionAnimImmediately = TRUE
				NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - another interaction is playing, cleaning up.") NET_NL()
				EXIT
			ENDIF
		ENDIF
		
		// if disabled every frame has been called
		IF (MPGlobalsInteractions.bDisabledThisFrame)
			GlobalData.bCleanupInteractionAnimImmediately = TRUE
			NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - disabled this frame called, cleaning up.") NET_NL()
			EXIT	
		ENDIF
		
		IF NOT IsPedInCorrectOnFootStateForInteraction(GlobalData, PedID)
		AND NOT (StoredInteractionData.bPlayingSelectedAnim)
			GlobalData.iInteractionType = -1
			GlobalData.iInteractionAnim = -1
			NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - not IsPedInCorrectOnFootStateForInteraction, reseting to -1") NET_NL()
		ELSE
			EnsureInteractionAnimIsWithinRange(GlobalData)
			SetInteractionLoadFlags(StoredInteractionData, GlobalData, DEFAULT, TRUE, IS_PED_FEMALE(PedID))
			
			// grab driver side / low car info as player enters vehicle
			IF NOT (StoredInteractionData.bHasGrabbedCarInfo)
				IF IS_PED_IN_ANY_VEHICLE(StoredInteractionData.SelectedInteraction.PedID)
										
					GetVehiclePassengerInfo(StoredInteractionData.SelectedInteraction)

					StoredInteractionData.bHasGrabbedCarInfo = TRUE
				ENDIF
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(StoredInteractionData.SelectedInteraction.PedID)
					StoredInteractionData.bHasGrabbedCarInfo = FALSE	
				ENDIF
			ENDIF		
			
			// is it safe to start playing full body at this time?
			
			BOOL bWaitingForSomething = (GlobalData.bWaitForPlayerControl OR GlobalData.bWaitForSync OR GlobalData.bWaitForFadingIn)
			
			IF (GlobalData.bPlayInteractionAnimFullBody=TRUE)
			AND (StoredInteractionData.bPlayingFullBody=FALSE)
				IF (GlobalData.bWaitForSync) 
					NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - full body for sync scene, always play these.") NET_NL()
				ELSE
					IF ((bWaitingForSomething = FALSE) AND NOT IsPedSafeToPlayFullBody(StoredInteractionData.SelectedInteraction.PedID))
					OR NOT DOES_INTERACTION_HAVE_FULL_BODY(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim)
					OR IS_PED_WEARING_PROBLEM_MASK_OR_HAT_FOR_INTERACTION(StoredInteractionData.SelectedInteraction.PedID, StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim, GlobalData.bPlayInteractionAnimFullBody, GlobalData.bPlayInteractionSelfie, GlobalData.bIsWearingHelmentInVehicle)											
					OR IS_PED_IN_PROBLEM_VEHICLE_FOR_INTERACTION(StoredInteractionData.SelectedInteraction.PedID, StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim, GlobalData.bPlayInteractionAnimFullBody, GlobalData.bPlayInteractionSelfie)
					OR ((StoredInteractionData.SelectedInteraction.PedID = PLAYER_PED_ID()) AND IS_PLAYER_IN_FIRST_PERSON_CAMERA())
					#IF FEATURE_FIXER
					OR IS_PLAYER_INTERACTION_OFFENSIVE_TO_DRE(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim)
					#ENDIF
						IF NOT (bWaitingForSomething)
							GlobalData.bPlayInteractionAnimFullBody = FALSE
						ENDIF
						NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play full body anim at this time") NET_NL()
						#IF IS_DEBUG_BUILD
							IF NOT IsPedSafeToPlayFullBody(StoredInteractionData.SelectedInteraction.PedID)
								NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play full body - IsPedSafeToPlayFullBody = FALSE ") NET_NL()
							ENDIF
							IF NOT DOES_INTERACTION_HAVE_FULL_BODY(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim)
								NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play full body - DOES_INTERACTION_HAVE_FULL_BODY = FALSE ") NET_NL()
							ENDIF
							IF IS_PED_WEARING_PROBLEM_MASK_OR_HAT_FOR_INTERACTION(StoredInteractionData.SelectedInteraction.PedID, StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim, GlobalData.bPlayInteractionAnimFullBody, GlobalData.bPlayInteractionSelfie, GlobalData.bIsWearingHelmentInVehicle)											
								NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play full body - IS_PED_WEARING_PROBLEM_MASK_OR_HAT_FOR_INTERACTION = FALSE ") NET_NL()
							ENDIF
							IF IS_PED_IN_PROBLEM_VEHICLE_FOR_INTERACTION(StoredInteractionData.SelectedInteraction.PedID, StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim, GlobalData.bPlayInteractionAnimFullBody, GlobalData.bPlayInteractionSelfie)
								NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - IS_PED_IN_PROBLEM_VEHICLE_FOR_INTERACTION = FALSE") NET_NL()
							ENDIF
							IF ((StoredInteractionData.SelectedInteraction.PedID = PLAYER_PED_ID()) AND IS_PLAYER_IN_FIRST_PERSON_CAMERA())
								NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play full body - in 1st person. ") NET_NL()	
							ENDIF
							#IF FEATURE_FIXER
							IF IS_PLAYER_INTERACTION_OFFENSIVE_TO_DRE(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim)
								NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play full body - offensive to dre. ") NET_NL()						
							ENDIF
							#ENDIF
						#ENDIF
						IF IS_INTERACTION_FULL_BODY_ONLY(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim)
							IF NOT (bWaitingForSomething)
								GlobalData.bCleanupInteractionAnimImmediately = TRUE
								PRINTLN("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play full body, and was full body only. Cleaning up.")
								EXIT
							ELSE
								PRINTLN("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play full body, and was full body only. But waiting on bWaitForFadingIn.")							
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// is ped in a roofless car?
			GlobalData.bIsWearingHelmentInVehicle = FALSE
			IF IS_PED_IN_ANY_VEHICLE(StoredInteractionData.SelectedInteraction.PedID)
				IF IS_CURRENT_HEAD_PROP_A_HELMET(StoredInteractionData.SelectedInteraction.PedID)
					GlobalData.bIsWearingHelmentInVehicle = TRUE
				ENDIF
			ENDIF
			
			// should we be playing selfie at this time?
			IF (GlobalData.bPlayInteractionSelfie)
				IF NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()
					GlobalData.bPlayInteractionSelfie = FALSE	
					NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play selfie anim at this time") NET_NL()
				ENDIF
			ENDIF
			
			// has intro delay passed?
			BOOL bIntroDelayElapsed = TRUE
			IF NOT (GlobalData.bPlayInteractionAnimFullBody) AND (GlobalData.bPlayInteractionAnim)			
			AND IsPedPlayerPed(StoredInteractionData.SelectedInteraction.PedID)
			AND (StoredInteractionData.SelectedInteraction.Data.bIsLooped)
			AND NOT (StoredInteractionData.bPlayingSelectedAnim)
			AND NOT (MPGlobalsInteractions.bAnyInteractionIsPlaying) 
			AND (INTRO_DELAY_TIMER > 0)
			AND NOT (MPGlobalsInteractions.bThisIsAQuickRestartAnim)
			AND NOT MPGlobalsInteractions.bSkipInteractionAnimIntroDelay
				// have we started the timer?
				IF NOT (GlobalData.bStartedIntroTime)
					GlobalData.IntroTime = GET_THIS_TIMER()	
					GlobalData.bStartedIntroTime = TRUE
					bIntroDelayElapsed = FALSE
					NET_PRINT("[interactions][intro timer] UPDATE_INTERACTION_ANIM_INTERNAL - starting intro timer ") NET_NL()
				ELSE
					IF (GET_TIME_DIFFERENCE(GET_THIS_TIMER(), GlobalData.IntroTime) < INTRO_DELAY_TIMER)
						bIntroDelayElapsed = FALSE
						NET_PRINT("[interactions][intro timer] UPDATE_INTERACTION_ANIM_INTERNAL - waiting for intro timer to elapse ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_THIS_TIMER(), GlobalData.IntroTime))  NET_NL()
					ELSE
						NET_PRINT("[interactions][intro timer] UPDATE_INTERACTION_ANIM_INTERNAL - intro timer has elapsed ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_THIS_TIMER(), GlobalData.IntroTime))  NET_NL()						
					ENDIF
				ENDIF
			ELSE
				IF MPGlobalsInteractions.bSkipInteractionAnimIntroDelay
					#IF IS_DEBUG_BUILD
						NET_PRINT("[interactions][intro timer] UPDATE_INTERACTION_ANIM_INTERNAL - bSkipInteractionAnimIntroDelay = FALSE ") NET_NL()
					#ENDIF
					MPGlobalsInteractions.bSkipInteractionAnimIntroDelay = FALSE
				ENDIF
				IF (GlobalData.bStartedIntroTime)	
					NET_PRINT("[interactions][intro timer] UPDATE_INTERACTION_ANIM_INTERNAL - resetting GlobalData.bStartedIntroTime ") NET_NL()
					GlobalData.bStartedIntroTime = FALSE
				ENDIF
			ENDIF
			
			// play the new anims
			#IF FEATURE_DLC_1_2022
			IF NOT StoredInteractionData.SelectedInteraction.Data.bSpeechOnly
			AND( NOT (StoredInteractionData.bPlayingSelectedAnim) 
			OR ((GlobalData.bPlayInteractionAnimFullBody=TRUE) AND (StoredInteractionData.bPlayingFullBody=FALSE)))
			#ENDIF
			#IF NOT FEATURE_DLC_1_2022
			IF NOT (StoredInteractionData.bPlayingSelectedAnim) 
			OR ((GlobalData.bPlayInteractionAnimFullBody=TRUE) AND (StoredInteractionData.bPlayingFullBody=FALSE))
			#ENDIF
				// if this is a 'NONE' do nothing
				IF NOT IS_ENTITY_DEAD(PedID)
				AND NOT IsInteractionNone(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim)
				
					// if this is selfie mode, check the anim is suitable
					IF ((GlobalData.bPlayInteractionSelfie) AND DOES_INTERACTION_HAVE_SELFIE(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim))
					OR NOT (GlobalData.bPlayInteractionSelfie)				
					
						//#IF IS_DEBUG_BUILD
						// do we have the correct DLC?
						IF DOES_ANIM_DICT_EXIST(InteractionAnimDictFromInteractionAnimData(StoredInteractionData.SelectedInteraction, GlobalData.bPlayInteractionAnimFullBody, GlobalData.bPlayInteractionSelfie))													
						//#ENDIF
						
							IF LoadInteractionAssets(StoredInteractionData.SelectedInteraction, GlobalData.bPlayInteractionAnimFullBody, GlobalData.bPlayInteractionSelfie)
								IF (GlobalData.bPlayInteractionAnim) OR (GlobalData.bPlayInteractionAnimFullBody)
									
									GlobalData.ActiveTime = GET_THIS_TIMER()
														
									//IF IS_NET_PLAYER_OK(PLAYER_ID())
									IF NOT IS_PED_INJURED(StoredInteractionData.SelectedInteraction.PedID)
									AND (ARE_CONDITIONS_MET_TO_START_INTERACTION_TYPE(StoredInteractionData.SelectedInteraction.iType) OR OverrideInteractionConditionsCheck())						

										// if this is a health boost interaction (snack), check the snacks are not disabled.
										IF ((NOT IS_EATING_OR_DRINKING_INTERACTION(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim) AND ARE_SNACKS_BLOCKED())
										OR (NOT ARE_SNACKS_BLOCKED()))
										AND NOT IS_INTERACTION_ON_COOLDOWN(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim)
										AND CAN_PLAYER_AFFORD_INTERACTION(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim)
											// check it's not blocked by tunables
											IF NOT IS_INTERACTION_LOCKED_BY_TUNABLE(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim)
											#IF FEATURE_FIXER
											AND NOT IS_PLAYER_INTERACTION_OFFENSIVE_TO_DRE(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim)				
											#ENDIF

												// check we are not wearing a problem mask or playing an interaction in a problem vehicle.
												IF NOT IS_PED_WEARING_PROBLEM_MASK_OR_HAT_FOR_INTERACTION(StoredInteractionData.SelectedInteraction.PedID, StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim, GlobalData.bPlayInteractionAnimFullBody, GlobalData.bPlayInteractionSelfie, GlobalData.bIsWearingHelmentInVehicle)
												AND NOT IS_PED_IN_PROBLEM_VEHICLE_FOR_INTERACTION(StoredInteractionData.SelectedInteraction.PedID, StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim, GlobalData.bPlayInteractionAnimFullBody, GlobalData.bPlayInteractionSelfie)
													CLEAR_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_WRONG_HEADGEAR) // clear bit set for Dave W.
											
													IF NOT IsPedPlayerPed(StoredInteractionData.SelectedInteraction.PedID)
													OR (IS_STAT_DEPENDANT(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim) AND (GET_MP_INT_CHARACTER_STAT( GET_DEPENDANT_STAT_FOR_INTERACTION(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim) ) > 0))
													OR NOT IS_STAT_DEPENDANT(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim) 
													OR (GlobalData.bIgnoreStatCheck)
													
														IF NOT (MPGlobalsInteractions.bAnyInteractionIsPlaying) 
														OR (IsPedPlayerPed(StoredInteractionData.SelectedInteraction.PedID) AND NOT (GlobalData.bInteractionIsPlaying=TRUE))					
														OR ((GlobalData.bPlayInteractionAnimFullBody=TRUE) AND (StoredInteractionData.bPlayingFullBody=FALSE))
															
															IF (bIntroDelayElapsed)
															
																//REMOVE_VEHICLE_WINDOW_IF_NECESSARY(StoredInteractionData.SelectedInteraction)																										
																
																PrintInteractionFlags(StoredInteractionData, GlobalData)
																//UpdateRemoveMaskOrHelmet(StoredInteractionData.SelectedInteraction.PedID, StoredInteractionData.SelectedInteraction.Data, iPlayerInt)

															
																IF (GlobalData.bPlayInteractionAnimFullBody)
																
																	// if we are starting a full body anim check if the current anim has a prop, if so delete the prop immediately we'll recreate it.
																	IF (StoredInteractionData.bPlayingSelectedAnim)
																	AND (StoredInteractionData.PlayingInteraction.bObjectCreated)
																		//CopyPropDetails(StoredInteractionData.PlayingInteraction, StoredInteractionData.SelectedInteraction)
																		NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - deleting prop immediately") NET_NL()
																		DeleteProp(StoredInteractionData.PlayingInteraction, FALSE, TRUE)
																		
																		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(StoredInteractionData.PlayingInteraction.NetworkObjectID)
																		AND NETWORK_DOES_NETWORK_ID_EXIST(StoredInteractionData.PlayingInteraction.NetworkObjectIDPrevious)
																			DELETE_NET_ID(StoredInteractionData.PlayingInteraction.NetworkObjectIDPrevious)
																		ENDIF
																		
																		IF DOES_ENTITY_EXIST(StoredInteractionData.PlayingInteraction.ObjectIDPrevious)
																			DELETE_OBJECT(StoredInteractionData.PlayingInteraction.ObjectIDPrevious)
																		ENDIF	
																	ENDIF
																	
																	IF (StoredInteractionData.SelectedInteraction.Data.bHasProp)
																	AND NOT (StoredInteractionData.SelectedInteraction.Data.bBlockForceProp)
																		StoredInteractionData.SelectedInteraction.bForceProp = TRUE
																		NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - setting force prop")
																	ENDIF
																	
																	StartPlayingInteractionAnim(StoredInteractionData.SelectedInteraction, IAS_FULL_BODY, DEFAULT, GlobalData.fStartPhase, DEFAULT, GlobalData.bWaitForPlayerControl, GlobalData.bWaitForSync, GlobalData.bWaitForFadingIn, GlobalData.bForceWeaponVisible)																																												
																ELSE		
																	IF NOT (StoredInteractionData.SelectedInteraction.Data.bIsLooped)
																	OR (GlobalData.bWaitForPlayerControl = TRUE) 
																		NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - starting a non-looped anim.") NET_NL()
																		IF (StoredInteractionData.SelectedInteraction.Data.bHasProp)
																		AND NOT (StoredInteractionData.SelectedInteraction.Data.bBlockForceProp)
																			StoredInteractionData.SelectedInteraction.bForceProp = TRUE
																			NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - setting force prop")
																		ENDIF
																		StartPlayingInteractionAnim(StoredInteractionData.SelectedInteraction, IAS_BODY, TRUE, DEFAULT, TRUE, GlobalData.bWaitForPlayerControl, GlobalData.bWaitForSync, GlobalData.bWaitForFadingIn, GlobalData.bForceWeaponVisible)																																			
																	ELSE																														
																		StartPlayingInteractionAnim(StoredInteractionData.SelectedInteraction, IAS_INTRO, DEFAULT, DEFAULT, DEFAULT, GlobalData.bWaitForPlayerControl, GlobalData.bWaitForSync, GlobalData.bWaitForFadingIn, GlobalData.bForceWeaponVisible)																																																			
																	ENDIF
																ENDIF
																																		
																StoredInteractionData.bPlayingSelectedAnim = TRUE
																
																IF (GlobalData.bPlayInteractionAnimFullBody)
																	StoredInteractionData.bPlayingFullBody = TRUE
																ENDIF
																IF (GlobalData.bPlayInteractionSelfie)
																	StoredInteractionData.bPlayingSelfie = TRUE
																ENDIF
																
							//									IF (StoredInteractionData.SelectedInteraction.bPlayingNoFilterWhenStanding)
							//										StoredInteractionData.bPlayingFullBodyMask = TRUE
							//									ENDIF
	
																// store the anim we are playing
																StoredInteractionData.PlayingInteraction = StoredInteractionData.SelectedInteraction
																
																NET_PRINT("[interactions] calling DisableControlsForInteraction 1") NET_NL()
																DisableControlsForInteraction(StoredInteractionData.SelectedInteraction.PedID, ShouldDisableMovementControls(StoredInteractionData))
														
																IF IsPedPlayerPed(StoredInteractionData.SelectedInteraction.PedID) 
																	MPGlobalsInteractions.bAnyInteractionIsPlaying = TRUE
																	MPGlobalsInteractions.iCurrentlyPlayingType = StoredInteractionData.SelectedInteraction.iType
																	MPGlobalsInteractions.iCurrentlyPlayingAnim = StoredInteractionData.SelectedInteraction.iAnim
																	NET_PRINT("[interactions] MPGlobalsInteractions.bAnyInteractionIsPlaying = TRUE") NET_NL()
																ENDIF
																GlobalData.bInteractionIsPlaying=TRUE
															
															ELSE
																NET_PRINT("[interactions][intro timer] UPDATE_INTERACTION_ANIM_INTERNAL waiting on bIntroDelayElapsed ") NET_NL()	
															ENDIF

														ELSE
															GlobalData.bPlayInteractionAnimFullBody = FALSE
															GlobalData.bPlayInteractionAnim = FALSE
															GlobalData.bCleanupInteractionAnimImmediately = TRUE
															NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play anim at this time, another system has locked the global") NET_NL()
														ENDIF

													ELSE
													
														// don't have any of the desired prop, do finger instead.
														NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - no props in inventory, giving finger. ") NET_NL()
														NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iType = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iType) NET_NL()
														NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iAnim = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iAnim) NET_NL()
														GlobalData.bPlayInteractionAnim = FALSE
														GlobalData.bPlayInteractionAnimFullBody = FALSE
														
														IF NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()
															SET_BIT(MPGlobalsAmbience.iFmNmhBitSet6, BI_FM_NMH6_NO_ITEM) // bit set for Dave W.
														ENDIF
														
														ShakeHead()
														
														
													ENDIF
												ELSE
													// wearing inappropriate head gear or in a problem vehicle, shake head instead.
													NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - inappropriate headgear, shaking head. ") NET_NL()
													NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iType = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iType) NET_NL()
													NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iAnim = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iAnim) NET_NL()
													GlobalData.bPlayInteractionAnim = FALSE
													GlobalData.bPlayInteractionAnimFullBody = FALSE
													
													SET_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_WRONG_HEADGEAR) // bit set for Dave W.
													
													ShakeHead()
												ENDIF
											ELSE
											
												// wearing locked by tunable, shake head instead.
												NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - locked by tunable, shaking head. ") NET_NL()
												NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iType = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iType) NET_NL()
												NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iAnim = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iAnim) NET_NL()
												GlobalData.bPlayInteractionAnim = FALSE
												GlobalData.bPlayInteractionAnimFullBody = FALSE
												
												ShakeHead()
											
											ENDIF
										
										ELSE
											// snacks are blocked
											NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - snacks are blocked, shaking head. ") NET_NL()
											NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iType = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iType) NET_NL()
											NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iAnim = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iAnim) NET_NL()
											GlobalData.bPlayInteractionAnim = FALSE
											GlobalData.bPlayInteractionAnimFullBody = FALSE
											
											//SET_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_WRONG_HEADGEAR) // if we need help then speak to Dave. W and set the appropriate bit here.
											
											IF IS_INTERACTION_ON_COOLDOWN(StoredInteractionData.SelectedInteraction.iType, StoredInteractionData.SelectedInteraction.iAnim)
												SET_BIT(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_PLAYER_ACTION_COOLDOWN)
											ENDIF
											
											ShakeHead()
										ENDIF
										
									ELSE
									
										PrintInteractionFlags(StoredInteractionData, GlobalData)	
									
										GlobalData.bPlayInteractionAnim = FALSE
										GlobalData.bPlayInteractionAnimFullBody = FALSE									
										GlobalData.bCleanupInteractionAnimImmediately = TRUE
										
										NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play anim at this time") NET_NL()
									ENDIF
							
								ENDIF
							
							ELSE
								IF (GlobalData.bPlayInteractionAnim) OR (GlobalData.bPlayInteractionAnimFullBody)
									NET_PRINT("[interactions] calling DisableControlsForInteraction 2") NET_NL()
									DisableControlsForInteraction(StoredInteractionData.SelectedInteraction.PedID)
								ENDIF
								GlobalData.ActiveTime = GET_THIS_TIMER()
							ENDIF
							
						//#IF IS_DEBUG_BUILD
						ELSE
							
							IF (GlobalData.bPlayInteractionAnim) OR (GlobalData.bPlayInteractionAnimFullBody)
								// anim doesn't exist on disc, shake head instead.
								
								PRINTLN("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - anim dict ", InteractionAnimDictFromInteractionAnimData(StoredInteractionData.SelectedInteraction, GlobalData.bPlayInteractionAnimFullBody, GlobalData.bPlayInteractionSelfie), " does not exist!")
								
								SCRIPT_ASSERT("[interactions] This anim dictionary does not exist, it may be in a future DLC that you do not have installed.") 
								
								NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - anim dictionary doesn't exist, shaking head. ") NET_NL()
								NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iType = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iType) NET_NL()
								NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iAnim = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iAnim) NET_NL()
								GlobalData.bPlayInteractionAnim = FALSE
								GlobalData.bPlayInteractionAnimFullBody = FALSE
								ShakeHead()		
								
							ELSE
							
								// if anim dict doesn't exist and is not playing then cleanup immediately.
								GlobalData.bCleanupInteractionAnimImmediately = TRUE
								NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - anim dict doesn't exist and not trying to play, cleanup immediately. ") NET_NL()
								
							ENDIF
							
							GlobalData.ActiveTime = GET_THIS_TIMER()
						
						ENDIF
						//#ENDIF

					ELSE
						// can't play this interaction in selfie mode, shake head instead
						NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - can't play selfie, shaking head. ") NET_NL()
						NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iType = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iType) NET_NL()
						NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.SelectedInteraction.iAnim = ") NET_PRINT_INT(StoredInteractionData.SelectedInteraction.iAnim) NET_NL()
						GlobalData.bPlayInteractionAnim = FALSE
						GlobalData.bPlayInteractionAnimFullBody = FALSE
						GlobalData.bCleanupInteractionAnimImmediately = TRUE
						//ShakeHead()										
					ENDIF						
						
				ELSE
					#IF IS_DEBUG_BUILD
					IF IS_ENTITY_DEAD(PedID)
						NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - ped is dead, not starting.") NET_NL()
					ENDIF
					#ENDIF
					NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - this is a 'none' interaction") NET_NL()
					GlobalData.bPlayInteractionAnim = FALSE
					GlobalData.bPlayInteractionAnimFullBody = FALSE
					GlobalData.bCleanupInteractionAnimImmediately = TRUE
				ENDIF
					
			
			ELSE
			
				GlobalData.ActiveTime = GET_THIS_TIMER()
				
				BOOL bCleanupPlayingAnim
			
				IF (GlobalData.bPlayInteractionAnim)
				OR (MPGlobalsInteractions.PlayerInteraction.iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL))
				
					UpdateInteractionWhileAnimPlays(StoredInteractionData.PlayingInteraction, GlobalData, bCleanupPlayingAnim) //, iPlayerInt)
					NET_PRINT("[interactions] calling DisableControlsForInteraction 3") NET_NL()
					DisableControlsForInteraction(StoredInteractionData.PlayingInteraction.PedID, ShouldDisableMovementControls(StoredInteractionData))
					
					IF NOT (bCleanupPlayingAnim)
						IF NOT IS_ENTITY_DEAD(StoredInteractionData.PlayingInteraction.PedID)
					
							IF IS_PED_WEARING_PROBLEM_MASK_OR_HAT_FOR_INTERACTION(StoredInteractionData.PlayingInteraction.PedID, StoredInteractionData.PlayingInteraction.iType, StoredInteractionData.PlayingInteraction.iAnim, StoredInteractionData.bPlayingFullBody, StoredInteractionData.bPlayingSelfie, GlobalData.bIsWearingHelmentInVehicle)
							#IF FEATURE_FIXER
							OR IS_PLAYER_INTERACTION_OFFENSIVE_TO_DRE(StoredInteractionData.PlayingInteraction.iType, StoredInteractionData.PlayingInteraction.iAnim)				
							#ENDIF
								NET_PRINT("[interactions] wearing problem mask or hat - cleaning up") NET_NL()
								bCleanupPlayingAnim = TRUE
							ELSE
							
								IF (StoredInteractionData.bPlayingFullBody)
									IF NOT IsPlayingFullBody(StoredInteractionData.PlayingInteraction)
									AND IsFinishedSequenceTask(StoredInteractionData.PlayingInteraction.PedID)																	
										IF (GlobalData.bWaitForPlayerControl)
										OR (GlobalData.bWaitForSync)
										OR (GlobalData.bWaitForFadingIn)
											PRINTLN("[interactions] lost task before player control turned on, restarting anim") 
											StartPlayingInteractionAnim(StoredInteractionData.PlayingInteraction, IAS_FULL_BODY, DEFAULT, DEFAULT, DEFAULT, GlobalData.bWaitForPlayerControl, GlobalData.bWaitForSync, GlobalData.bWaitForFadingIn, GlobalData.bForceWeaponVisible)
										ELSE
											bCleanupPlayingAnim = TRUE
											NET_PRINT("[interactions] not playing full body - cleaning up") NET_NL()
										ENDIF
									ENDIF
								ELSE
						
									IF IsPlayingIntro(StoredInteractionData.PlayingInteraction)
									AND (StoredInteractionData.PlayingInteraction.Data.bIsLooped)
										
										// is it time to move to the body?
										IF IsIntroAnimAlmostFinished(StoredInteractionData.PlayingInteraction)
											
											IF NOT (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iGameState = MAIN_GAME_STATE_HOLD_FOR_TRANSITION)
											
												// should we not bother with the body and go straight to the outro?
												IF  ((StoredInteractionData.PlayingInteraction.Data.bIsLooped) AND (StoredInteractionData.PlayingInteraction.Data.bCanSkipLoop))
												AND NOT (((MPGlobalsInteractions.bQuickplayButtonPressed)  OR (IS_INTERACTION_MENU_ACCEPT_BUTTON_PRESSED() AND (StoredInteractionData.PlayingInteraction.PedID = PLAYER_PED_ID()) )) AND NOT (StoredInteractionData.PlayingInteraction.bHitMaxIterations))
												AND NOT (MPGlobalsInteractions.bPointingButtonPressed)
												AND NOT ((GlobalData.bHoldLoop = TRUE) AND NOT (StoredInteractionData.PlayingInteraction.bHitMaxIterations))																	
												OR (GlobalData.bCleanupInteractionAnim)									
													NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - go straight to outro ") NET_NL()
													StartPlayingInteractionAnim(StoredInteractionData.PlayingInteraction, IAS_OUTRO)
												ELSE
													StartPlayingInteractionAnim(StoredInteractionData.PlayingInteraction, IAS_BODY, (StoredInteractionData.PlayingInteraction.Data.bCanQuitLoopEarly))	
												
													// make sure prop has been created
													IF (StoredInteractionData.PlayingInteraction.Data.bHasProp)
														IF NOT (StoredInteractionData.PlayingInteraction.bCreateProp)
															StoredInteractionData.PlayingInteraction.bCreateProp = TRUE
															NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - intro anim - forcing create prop") NET_NL()
														ENDIF
													ENDIF
													
													StoredInteractionData.PlayingInteraction.bJustFinishedIntro = TRUE
												ENDIF
													
											ELSE
												NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - waiting for transition to finishe before continuing loop. ") NET_NL()
											ENDIF
											
										ELSE
											NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - intro anim HAS_ENTITY_ANIM_FINISHED = FALSE ") NET_NL()
										ENDIF
											

									ELIF IsPlayingBody(StoredInteractionData.PlayingInteraction)
										
										IF (StoredInteractionData.PlayingInteraction.Data.bHasProp)
											SET_PED_RESET_FLAG(PedID, PRF_DisableVoiceDrivenMouthMovement, TRUE) // stop the ped from speaking while doing 
										ENDIF
										
										IF ((NOT (StoredInteractionData.PlayingInteraction.Data.bIsLooped)
										OR ((((MPGlobalsInteractions.bQuickplayButtonPressed) OR (MPGlobalsInteractions.bPointingButtonPressed)) OR (IS_INTERACTION_MENU_ACCEPT_BUTTON_PRESSED() AND (StoredInteractionData.PlayingInteraction.PedID = PLAYER_PED_ID()) )) AND NOT (StoredInteractionData.PlayingInteraction.bHitMaxIterations))
										OR (StoredInteractionData.PlayingInteraction.bJustFinishedIntro = TRUE)
										OR ((GlobalData.bHoldLoop = TRUE) AND NOT (StoredInteractionData.PlayingInteraction.bHitMaxIterations)))																	
										AND NOT (GlobalData.bCleanupInteractionAnim))

											IF (StoredInteractionData.PlayingInteraction.bJustFinishedIntro)														
												NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.PlayingInteraction.bJustFinishedIntro = true") NET_NL()
												
												
												// make sure prop has been created
												IF (StoredInteractionData.PlayingInteraction.Data.bHasProp)
													IF NOT (StoredInteractionData.PlayingInteraction.bCreateProp)
														StoredInteractionData.PlayingInteraction.bCreateProp = TRUE
														NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - intro anim - forcing create prop 2") NET_NL()
													ENDIF
												ENDIF										
											
												StoredInteractionData.PlayingInteraction.bJustFinishedIntro = FALSE
											ENDIF
											
				//							NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - playing body - IS_INTERACTION_LOOPED(StoredInteractionData.PlayingInteraction.iType, StoredInteractionData.PlayingInteraction.iAnim) =") NET_PRINT_BOOL(IS_INTERACTION_LOOPED(StoredInteractionData.PlayingInteraction.iType, StoredInteractionData.PlayingInteraction.iAnim)) NET_NL()
				//							NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - playing body - IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)) =") NET_PRINT_BOOL(IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)) NET_NL()							
				//							NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - playing body - MPGlobalsInteractions.bQuickplayButtonPressed =") NET_PRINT_BOOL(MPGlobalsInteractions.bQuickplayButtonPressed) NET_NL()
				//							NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - playing body - StoredInteractionData.PlayingInteraction.bHitMaxIterations =") NET_PRINT_BOOL(StoredInteractionData.PlayingInteraction.bHitMaxIterations) NET_NL()
				//							NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - playing body - StoredInteractionData.PlayingInteraction.bJustFinishedIntro =") NET_PRINT_BOOL(StoredInteractionData.PlayingInteraction.bJustFinishedIntro) NET_NL()
				//							NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - playing body - GlobalData.bHoldLoop =") NET_PRINT_BOOL(GlobalData.bHoldLoop) NET_NL()
				//							NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - playing body - StoredInteractionData.PlayingInteraction.bHitMaxIterations =") NET_PRINT_BOOL(StoredInteractionData.PlayingInteraction.bHitMaxIterations) NET_NL()
				//							NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - playing body - GlobalData.bCleanupInteractionAnim =") NET_PRINT_BOOL(GlobalData.bCleanupInteractionAnim) NET_NL()
											
										ELSE
										
											NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - playing body ") NET_NL()

										
											// play the outro
											IF (StoredInteractionData.PlayingInteraction.Data.bIsLooped)	
												// if quitting due to max iterations, makes sure the loop is almost finished
					//							IF ((StoredInteractionData.PlayingInteraction.bHitMaxIterations) AND IsBodyAnimAlmostFinished(StoredInteractionData.PlayingInteraction))
					//							OR NOT (StoredInteractionData.PlayingInteraction.bHitMaxIterations)
										
												NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - playing body - looped") NET_NL()
					
												IF ( IsBodyAnimAlmostFinished(StoredInteractionData.PlayingInteraction) AND HasBodyAnimCompletedMinIterations(StoredInteractionData.PlayingInteraction))	
												OR (StoredInteractionData.PlayingInteraction.Data.bCanQuitLoopEarly)
													StartPlayingInteractionAnim(StoredInteractionData.PlayingInteraction, IAS_OUTRO, StoredInteractionData.PlayingInteraction.Data.bCanQuitLoopEarly)																	
												ENDIF
											ENDIF
										ENDIF	
										
									ELSE
										IF NOT IsPlayingOutro(StoredInteractionData.PlayingInteraction)
											IF IsFinishedSequenceTask(StoredInteractionData.PlayingInteraction.PedID)											
												NET_PRINT("[interactions] not playing outro - cleaning up") NET_NL()
												bCleanupPlayingAnim = TRUE		
											ELSE
												NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - not playing any anims but not finished sequence task, waiting...") NET_NL()
											ENDIF								
										ENDIF				
									ENDIF
								
								ENDIF
							ENDIF
								
						ELSE
						
							NET_PRINT("[interactions] ped is dead - cleaning up") NET_NL()
							bCleanupPlayingAnim = TRUE				
						
						ENDIF
					ENDIF

					
					IF (bCleanupPlayingAnim)
						NET_PRINT("[interactions] bCleanupPlayingAnim - cleaning up.") NET_NL()
						CLEANUP_INTERACTION(StoredInteractionData.PlayingInteraction)
						StoredInteractionData.bPlayingSelectedAnim = FALSE	
						StoredInteractionData.bPlayingFullBody = FALSE
						StoredInteractionData.bPlayingSelfie = FALSE
						//StoredInteractionData.bPlayingFullBodyMask = FALSE
						GlobalData.bPlayInteractionAnim = FALSE
						GlobalData.bPlayInteractionAnimFullBody = FALSE
						GlobalData.bInteractionIsPlaying = FALSE						
						GlobalData.bCleanupInteractionAnim = FALSE
						GlobalData.bStartedIntroTime = FALSE
						IF IsPedPlayerPed(StoredInteractionData.PlayingInteraction.PedID)
							MPGlobalsInteractions.bAnyInteractionIsPlaying = FALSE
							MPGlobalsInteractions.iCurrentlyPlayingType = -1
							MPGlobalsInteractions.iCurrentlyPlayingAnim = -1
							MPGlobalsInteractions.bThisIsAQuickRestartAnim = FALSE
							MPGlobalsInteractions.bSkipInteractionAnimIntroDelay = FALSE
							MPGlobalsInteractions.bInteractionAssetsAreLoading = FALSE
							NET_PRINT("[interactions] MPGlobalsInteractions.bAnyInteractionIsPlaying = FALSE 3") NET_NL()
						ENDIF	
						CLEAR_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_WRONG_HEADGEAR) // clear bit set for Dave W.
					ENDIF					
					

				ENDIF
				#IF FEATURE_DLC_1_2022
				IF StoredInteractionData.SelectedInteraction.Data.bSpeechOnly
					IF AreConditionsMetForInteractionSpeech(StoredInteractionData.SelectedInteraction.iAnim)
						StartPlayingInteractionSpeech(StoredInteractionData.SelectedInteraction.Data)
					ELSE
						ShakeHead()
					ENDIF
					GlobalData.bCleanupInteractionAnimImmediately = TRUE
				ENDIF
				#ENDIF							
			ENDIF
		ENDIF
		
		
		
		// if this has been idle for a while then cleanup
		IF (GET_TIME_DIFFERENCE(GET_THIS_TIMER(), GlobalData.ActiveTime) > 7000)
		AND (MPGlobalsInteractions.iSyncedInteractionState = -1)
			GlobalData.bCleanupInteractionAnimImmediately = TRUE
			NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - idle for 15 secs, cleaning up.") NET_NL()
			EXIT
		ENDIF		
		
	ELSE
	
		IF NOT (GlobalData.bHasCleanedUp)
	
			// set both to -1
			GlobalData.iInteractionType = -1
			GlobalData.iInteractionAnim = -1	
			
			// stop anything playing
			GlobalData.bPlayInteractionAnim = FALSE
			GlobalData.bPlayInteractionAnimFullBody = FALSE
			GlobalData.bCleanupInteractionAnim = FALSE	
			GlobalData.bCleanupInteractionAnimImmediately = FALSE
			GlobalData.bStartedIntroTime = FALSE 
			
			// if this anim was in the process of playing reset the global flag.
			IF (StoredInteractionData.bPlayingSelectedAnim)	
				NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - interaction - end cleanup") NET_NL()
				CLEANUP_INTERACTION(StoredInteractionData.PlayingInteraction)
				StoredInteractionData.bPlayingSelectedAnim = FALSE
				StoredInteractionData.bPlayingFullBody = FALSE
				StoredInteractionData.bPlayingSelfie = FALSE
				//StoredInteractionData.bPlayingFullBodyMask = FALSE
				GlobalData.bInteractionIsPlaying=FALSE
				IF IsPedPlayerPed(StoredInteractionData.PlayingInteraction.PedID)
					MPGlobalsInteractions.bAnyInteractionIsPlaying = FALSE
					MPGlobalsInteractions.bInteractionAssetsAreLoading = FALSE
					MPGlobalsInteractions.iCurrentlyPlayingType = -1
					MPGlobalsInteractions.iCurrentlyPlayingAnim = -1
					NET_PRINT("[interactions] MPGlobalsInteractions.bAnyInteractionIsPlaying = FALSE 2") NET_NL()
				ENDIF
			ENDIF
			
			CleanupAnimPropsAndVFX(StoredInteractionData.PlayingInteraction)

			SetInteractionLoadFlags(StoredInteractionData, GlobalData)	
			
			// have we finished cleaning up? (check any created object has been deleted.)
			IF NOT (StoredInteractionData.PlayingInteraction.bObjectCreated)
				GlobalData.bHasCleanedUp = TRUE	
				NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - GlobalData.bHasCleanedUp = TRUE") NET_NL()
			ENDIF
			
			CLEAR_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_WRONG_HEADGEAR) // clear bit set for Dave W.
			
		ELSE
		
			IF (GlobalData.bCleanupInteractionAnim)
				GlobalData.bCleanupInteractionAnim = FALSE	
				NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - already bHasCleanedUp, clearing bCleanupInteractionAnim") NET_NL()
			ENDIF
			IF (GlobalData.bCleanupInteractionAnimImmediately)
				GlobalData.bCleanupInteractionAnimImmediately = FALSE
				NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - already bHasCleanedUp, clearing bCleanupInteractionAnimImmediately") NET_NL()
			ENDIF
			
		ENDIF
	ENDIF

	// unload the last selected anims
	IF NOT (StoredInteractionData.bHasUnloadedLastSelectedAnim)
	AND ((StoredInteractionData.LastInteraction.iType > -1) AND (StoredInteractionData.LastInteraction.iAnim > -1))
		RemoveInteractionAssets(StoredInteractionData.LastInteraction)			
		StoredInteractionData.bHasUnloadedLastSelectedAnim = TRUE
		NET_PRINT("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - StoredInteractionData.bHasUnloadedLastSelectedAnim = TRUE") NET_NL()
	ELSE
	
		IF NOT (StoredInteractionData.bHasUnloadedLastSelectedAnim)
		AND (StoredInteractionData.SelectedInteraction.bIsPaused)
		AND ((StoredInteractionData.LastInteraction.iType = -1) AND (StoredInteractionData.LastInteraction.iAnim = -1))
		AND ((GlobalData.bPlayInteractionAnim = FALSE) AND (GlobalData.bPlayInteractionAnimFullBody = FALSE))
		AND ((StoredInteractionData.SelectedInteraction.iType > -1) AND (StoredInteractionData.SelectedInteraction.iAnim > -1))		
			StoredInteractionData.LastInteraction = StoredInteractionData.SelectedInteraction	
			PRINTLN("[interactions] UPDATE_INTERACTION_ANIM_INTERNAL - unloading - last interaction hadn't unloading, but data wasn't set. copying SelectedInteraction to LastInteraction")
		ENDIF
	
		
	ENDIF
	
	
ENDPROC


PROC UPDATE_INTERACTION_ANIM_FOR_PED(PED_INDEX PedID, INT iPlayerInt, INTERACTION_INTERNAL_DATA & PedData)
	
	UPDATE_INTERACTION_ANIM_INTERNAL(PedData, MPGlobalsInteractions.PedInteraction[iPlayerInt], PedID, FALSE)//, iPlayerInt)

ENDPROC



PROC FaceWhereCameraIsPointing()
	SCRIPTTASKSTATUS TaskStatus
	TaskStatus = GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ACHIEVE_HEADING)
	IF (TaskStatus = FINISHED_TASK)

		FLOAT fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
		FLOAT fCamRelativeOffset =  GET_GAMEPLAY_CAM_RELATIVE_HEADING()
		FLOAT fCameraHeading = fPlayerHeading + fCamRelativeOffset
		FLOAT fDesiredHeading = fCameraHeading
		TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), fDesiredHeading)
		NET_PRINT("[interactions] FaceWhereCameraIsPointing() - turning player to desired heading ") NET_PRINT_FLOAT(fDesiredHeading) NET_NL()
	ENDIF
ENDPROC



FUNC BOOL IsCameraPointingDirectionTooDifferent(FLOAT fMaxDiff)
	FLOAT fHeadingDiff = GET_GAMEPLAY_CAM_RELATIVE_HEADING()	
	
	NET_PRINT("[interactions] IsCameraPointingDirectionTooDifferent - fHeadingDiff = ") NET_PRINT_FLOAT(fHeadingDiff) NET_NL()
	
	
	IF (fHeadingDiff > (fMaxDiff)) OR (fHeadingDiff < (fMaxDiff*-1.0))
		NET_PRINT("[interactions] IsCameraPointingDirectionTooDifferent - return TRUE") NET_NL()
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)

ENDFUNC


FUNC BOOL IS_FULL_SYNC_SCENE_RUNNING(INT ThisSynchScene)
	INT iThisLocalSceneID
	iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	
	PRINTLN("[interactions] IS_FULL_SYNC_SCENE_RUNNING - ThisSynchScene = ", ThisSynchScene, ", iThisLocalSceneID = ", iThisLocalSceneID)
	
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DID_HOST_ADD_ME_TO_THE_SYNC_SCENE(INT iSyncSceneHost)

	INT i
	REPEAT 4 i
		IF (GlobalplayerBD_Interactions[iSyncSceneHost].iPlayerInSyncSceneHash[i] = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
			PRINTLN("[interactions] DID_HOST_ADD_ME_TO_THE_SYNC_SCENE - true")
			RETURN(TRUE)
		ENDIF
	ENDREPEAT
	PRINTLN("[interactions] DID_HOST_ADD_ME_TO_THE_SYNC_SCENE - false")
	RETURN(FALSE)
ENDFUNC

PROC FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(INT ThisSynchScene)
	INT iThisLocalSceneID
	iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	IF iThisLocalSceneID != -1
		NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(iThisLocalSceneID)		
	ENDIF
ENDPROC

PROC TOTALLY_CLEANUP_FROM_INTERNAL_DATA(INTERACTION_INTERNAL_DATA &InternalData)

	CLEANUP_INTERACTION(InternalData.SelectedInteraction)
	CLEANUP_INTERACTION(InternalData.PlayingInteraction)
	CLEANUP_INTERACTION(InternalData.LastInteraction)
	
	// remove all assets
	RemoveInteractionAssets(InternalData.SelectedInteraction)
	RemoveInteractionAssets(InternalData.PlayingInteraction)
	RemoveInteractionAssets(InternalData.LastInteraction)

	InternalData.bHasUnloadedLastSelectedAnim = FALSE
	InternalData.bPlayingSelectedAnim = FALSE
	InternalData.bHasGrabbedCarInfo = FALSE
	InternalData.bPlayingFullBody = FALSE
	InternalData.bPlayingSelfie = FALSE
	
	PRINTLN("[interactions] TOTALLY_CLEANUP_FROM_INTERNAL_DATA called")
	
ENDPROC

PROC CLEAR_CLONE_PED_DATA()
	INT i
	INTERACTION_GLOBAL_DATA BlankData
	REPEAT COUNT_OF(MPGlobalsInteractions.PedInteraction) i
		MPGlobalsInteractions.PedInteraction[i] = BlankData
	ENDREPEAT
	
	REPEAT COUNT_OF(gInteractionsPedsData) i
		TOTALLY_CLEANUP_FROM_INTERNAL_DATA(gInteractionsPedsData[i])
	ENDREPEAT
ENDPROC

PROC CLEANUP_SYNCED_INTERACTIONS_SETTINGS()

	PRINTLN("[interactions] CLEANUP_SYNCED_INTERACTIONS_SETTINGS - called")
	
	IF AreAnyOtherPlayersNearbyPlayer(GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].iPlayerNamesHash)
		PRINTLN("[interactions] CLEANUP_SYNCED_INTERACTIONS_SETTINGS - there are nearby other players, set invincible for 5 seconds")
		MAKE_PLAYER_FLASH_INVINCIBLE(GetVehicleRespotTime())
	ENDIF
	
	// delete clone players
	IF (MPGlobalsInteractions.bUseClonePlayers)	
		INT iCloneCount
		REPEAT 3 iCloneCount
			IF DOES_ENTITY_EXIST(MPGlobalsInteractions.ClonePedID[iCloneCount])
				DELETE_PED(MPGlobalsInteractions.ClonePedID[iCloneCount])
			ENDIF
		ENDREPEAT
	ENDIF	
	
	SET_NO_LOADING_SCREEN(FALSE)
	CLEANUP_MP_CUTSCENE()
	CLEAR_CLONE_PED_DATA()
	ENABLE_INTERACTION_MENU()
	//CLEAR_CUSTOM_VEHICLE_NODES()
	
	MPGlobalsInteractions.bClearCustomCarNodes = TRUE
	MPGlobalsInteractions.timeClearCustomCarNodes = GET_NETWORK_TIME_ACCURATE()
	
	HIDE_PERSONAL_VEHICLES(FALSE)
	
		
	PRINTLN("[interactions] CLEANUP_SYNCED_INTERACTIONS_SETTINGS - MPGlobalsInteractions.ScenarioBlockingID = ", NATIVE_TO_INT(MPGlobalsInteractions.ScenarioBlockingID))
	IF NOT (MPGlobalsInteractions.ScenarioBlockingID = INT_TO_NATIVE(SCENARIO_BLOCKING_INDEX, -1))
		REMOVE_SCENARIO_BLOCKING_AREA(MPGlobalsInteractions.ScenarioBlockingID)
	ENDIF
	MPGlobalsInteractions.ScenarioBlockingID = INT_TO_NATIVE(SCENARIO_BLOCKING_INDEX, -1)
	SET_PED_PATHS_IN_AREA(MPGlobalsInteractions.vSyncedInteractionCoords - <<PATH_BLOCKING_DIST, PATH_BLOCKING_DIST, PATH_BLOCKING_DIST>>, MPGlobalsInteractions.vSyncedInteractionCoords + <<PATH_BLOCKING_DIST, PATH_BLOCKING_DIST, PATH_BLOCKING_DIST>>, TRUE)

	g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHandOverToNeilCelebration = FALSE
	PRINTLN("[interactions] CLEANUP_SYNCED_INTERACTIONS_SETTINGS - setting bDoingAfterApartmentPanHeistCelebration = FALSE.")
	
ENDPROC

PROC RESTORE_AUDIO_FOR_POST_MISSION_CUTSCENE()
	PRINTLN("[interactions] called RESTORE_AUDIO_FOR_POST_MISSION_CUTSCENE()")
	STOP_CUTSCENE_AUDIO()
	MPGlobalsInteractions.bRestoreCelebrationAudioStage = TRUE
ENDPROC

FUNC BOOL IS_POST_MISSION_SCENE_A_HEIST(INT iSceneID)
	IF (iSceneID <= SIS_POST_PACIFIC_STANDARD_HACK_DECOY)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC UPDATE_RESTORE_CELEBRATION_AUDIO_STAGE()
	IF (MPGlobalsInteractions.bRestoreCelebrationAudioStage)
		IF IS_SCREEN_FADED_IN()
			PRINTLN("[interactions] UPDATE_RESTORE_CELEBRATION_AUDIO_STAGE - screen is faded in.")
			//IF NOT IS_FULL_SYNC_SCENE_LOCATION(GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].iSceneID)
			IF (GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].iSceneID > -1)
			AND NOT (gSyncedInteractionScene[GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].iSceneID].bIsAFinale)
			AND IS_POST_MISSION_SCENE_A_HEIST(GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].iSceneID)
				PRINTLN("[interactions] UPDATE_RESTORE_CELEBRATION_AUDIO_STAGE - is a prep mission.")
				START_CELEBRATION_STATS_SCREEN_STOP_MUSIC_EVENT()
			ENDIF
			SET_GAME_PAUSES_FOR_STREAMING(TRUE)
			PRINTLN("[SAC] - called SET_GAME_PAUSES_FOR_STREAMING(TRUE). Call E.")
			SET_CELEBRATION_AUDIO_STAGE(eCAS_OFF)	
			MPGlobalsInteractions.bRestoreCelebrationAudioStage = FALSE
		ELSE
			PRINTLN("[interactions] UPDATE_RESTORE_CELEBRATION_AUDIO_STAGE - waiting for screen to fade in.")	
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_SYNCED_INTERACTION_FOR_CUTSCENE()
	
	PRINTLN("[interactions] SETUP_SYNCED_INTERACTION_FOR_CUTSCENE called")
	DEBUG_PRINTCALLSTACK()

	// stop any of William's lingering audio - 2105937
	IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
		STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	ENDIF
	
	// make player safe for mp cutscene
	MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE)
	START_MP_CUTSCENE(FALSE)
	IF NOT IS_FULL_SYNC_SCENE_LOCATION(GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].iSceneID)
		HIDE_PERSONAL_VEHICLES(TRUE)
	ENDIF
	
	MPGlobalsInteractions.ScenarioBlockingID = INT_TO_NATIVE(SCENARIO_BLOCKING_INDEX, -1)

	MPGlobalsInteractions.bFirstPersonFlashTriggered = FALSE
	MPGlobalsInteractions.bSyncSceneSuccessfullyStarted = FALSE

ENDPROC

PROC SET_SYNCED_INTERACTION_PRE_CUTSCENE_AS_FINISHED()
	PRINTLN("[interactions] SET_SYNCED_INTERACTION_PRE_CUTSCENE_AS_FINISHED called ")
	DEBUG_PRINTCALLSTACK()
	RESET_SYNCED_INTERACTION_TIMER()
	MPGlobalsInteractions.iPreCutsceneState = 99 // set to 99 to finish
ENDPROC

PROC SETUP_BENNY_VEHICLE()
	MPGlobalsInteractions.preCutsceneVeh = CREATE_VEHICLE(CHINO2,<<-210.5710, -1322.3549, 29.8904>>, 145.4212,FALSE,FALSE)
	
	SET_VEHICLE_DIRT_LEVEL(MPGlobalsInteractions.preCutsceneVeh,0.0)
	
	SET_VEHICLE_COLOURS(MPGlobalsInteractions.preCutsceneVeh,32,32)
	
	SET_VEHICLE_EXTRA_COLOURS(MPGlobalsInteractions.preCutsceneVeh,25,156) //107,
	
	SET_VEHICLE_LIVERY(MPGlobalsInteractions.preCutsceneVeh,-1)

	SET_VEHICLE_WHEEL_TYPE(MPGlobalsInteractions.preCutsceneVeh,MWT_MUSCLE)
	//TOGGLE_VEHICLE_MOD
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_SPOILER,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_BUMPER_F,0)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_BUMPER_R,-1)	
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_SKIRT,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_EXHAUST,0)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_CHASSIS,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_GRILL,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_BONNET,0) //-1
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_WING_L,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_WING_R,1) //-1
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_ROOF,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_ENGINE,3) //3
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_BRAKES,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_GEARBOX,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_HORN,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_SUSPENSION,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_ARMOUR,-1)
	//SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_TOGGLE_NITROUS,-1) //0
	//SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_TOGGLE_TURBO,-1) //0
	//SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_TOGGLE_SUBWOOFER,-1) //0
	//SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_TOGGLE_TYRE_SMOKE,-1) //0
	//SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_TOGGLE_HYDRAULICS,-1) //0 
	//SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_TOGGLE_XENON_LIGHTS,-1) //0
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_WHEELS,-1) //-1
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_REAR_WHEELS,3)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_PLTHOLDER,2) //-1
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_PLTVANITY,1) //-1
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_INTERIOR1,0)
	
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_INTERIOR2,17) //-1
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_INTERIOR3,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_INTERIOR4,12) //-1
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_INTERIOR5,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_SEATS,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_STEERING,11)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_KNOB,2)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_PLAQUE,13)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_ICE,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_TRUNK,5) //7
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_HYDRO,4)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_ENGINEBAY1,3) //0
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_ENGINEBAY2,2)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_ENGINEBAY3,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_CHASSIS2,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_CHASSIS3,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_CHASSIS4,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_CHASSIS5,1) //0
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_DOOR_L,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_DOOR_R,-1)
	SET_VEHICLE_MOD(MPGlobalsInteractions.preCutsceneVeh,MOD_LIVERY,0) //1
	
ENDPROC

PROC SETUP_BENNY_CUTSCENE()
	NEW_LOAD_SCENE_STOP()
	SETUP_BENNY_VEHICLE()
	NETWORK_SET_IN_MP_CUTSCENE(TRUE)
	//SET_ENTITY_VISIBLE_IN_CUTSCENE(cutsceneVeh, TRUE, FALSE) 
ENDPROC

PROC PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE()
	#IF IS_DEBUG_BUILD
		INT iTime
	#ENDIF
	
	//mute radio inside
	
	//BOOL preCutsceneCleanup = false
	INTERIOR_INSTANCE_INDEX garageInterior 
	
	FLOAT cameraShake = 0.075
	
	VECTOR cam1StartPosition = <<-227.0719, -1293.5090, 43.7999>>
	VECTOR cam1StartRotation = <<-9.6127, -0.0000, -128.6477>>
	VECTOR cam1EndPosition = <<-227.5908, -1293.0942, 39.8777>>
	VECTOR cam1EndRotation = <<-9.6127, -0.0000, -128.6477>>
	FLOAT cam1FOV = 39.6812
	
	VECTOR cam2StartPosition = <<-211.8528, -1314.0249, 31.2254>>
	VECTOR cam2StartRotation = <<-2.1362, 0.0229, -169.9598>>
	VECTOR cam2EndPosition = <<-210.5372, -1313.7946, 31.2258>>
	VECTOR cam2EndRotation = <<-2.0710, 0.0043, -174.2212>>
	FLOAT cam2FOV = 23.8562
	
	VECTOR cam3StartPosition = <<-221.9076, -1325.4309, 33.9131>>
	VECTOR cam3StartRotation =<<-11.6740, -0.0170, -77.1700>>
	VECTOR cam3EndPosition = <<-222.0432, -1324.8345, 33.9131>>
	VECTOR cam3EndRotation = <<-11.6740, -0.0170, -77.1700>>
	FLOAT cam3FOV = 38.8166
	
	SWITCH MPGlobalsInteractions.iPreCutsceneState
		CASE 0
		//new load scene		
			SET_USE_DLC_DIALOGUE(TRUE)
		
			PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: new load scene, NEW_LOAD_SCENE_START_SPHERE")			
			NEW_LOAD_SCENE_START_SPHERE(<<-205,-1310,31>>,1000.0)
						
			PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: garageInterior = ",NATIVE_TO_INT(garageInterior))
			garageInterior = GET_INTERIOR_AT_COORDS(<<-205,-1310,31>>)
			PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: garageInterior = ",NATIVE_TO_INT(garageInterior))
			PIN_INTERIOR_IN_MEMORY(garageInterior)
			
			REQUEST_MODEL(CHINO2)
			
			RESET_SYNCED_INTERACTION_TIMER()	
			MPGlobalsInteractions.iPreCutsceneState++

		BREAK
		CASE 1
		//scene loaded
			garageInterior = GET_INTERIOR_AT_COORDS(<<-205,-1310,31>>)
			IF IS_INTERIOR_READY(garageInterior) AND HAS_MODEL_LOADED(CHINO2)
			
				PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: IS_INTERIOR_READY")
				
				IF(IS_NEW_LOAD_SCENE_LOADED())
					PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: scene loaded, IS_NEW_LOAD_SCENE_LOADED = true")
					SETUP_BENNY_CUTSCENE()
					MPGlobalsInteractions.iPreCutsceneState++
				ELIF(GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 12000)
					PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: scene loaded, MPGlobalsInteractions.SyncedInteractionStartTime expired")
					SETUP_BENNY_CUTSCENE()
					MPGlobalsInteractions.iPreCutsceneState++
				ENDIF
								
			ENDIF

		BREAK
		CASE 2
		//setup camera shot 1
		
			SETUP_SYNCED_INTERACTION_FOR_CUTSCENE()
	
			// destroy existing cameras
			IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
				DESTROY_CAM(MPGlobalsInteractions.SyncInteractionCam)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)	
			ENDIF		
			DESTROY_ALL_CAMS()
			
			// setup camera
			MPGlobalsInteractions.SyncInteractionCam = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
			SET_CAM_SPLINE_SMOOTHING_STYLE(MPGlobalsInteractions.SyncInteractionCam, CAM_SPLINE_NO_SMOOTH)	
	
			// start cam node
			ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
				CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
					cam1StartPosition, cam1StartRotation,
					cam1FOV, 
					TRUE), 
				13000)
			
			// end cam node
			ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
				CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
					cam1EndPosition, cam1EndRotation,
					cam1FOV,
					TRUE), 				
				13000)	
				
			SHAKE_CAM(MPGlobalsInteractions.SyncInteractionCam, "HAND_SHAKE", cameraShake)
				
			PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: camera shot 1 setup complete")
		
			MPGlobalsInteractions.iPreCutsceneState++
		BREAK
		CASE 3
		//conversation
			
			structPedsForConversation conversationStruct
			ADD_PED_FOR_DIALOGUE(conversationStruct,3,NULL,"BENNY")
			
			IF CREATE_CONVERSATION(conversationStruct,"LOWBEAU","LOWBE_SCENE",CONV_PRIORITY_MEDIUM)
			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 5000)
			
				#IF IS_DEBUG_BUILD
					INT timeDiff 
					timeDiff = GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime)
					PRINTLN("bennycutscene timeDiff = ",timeDiff)	
				#ENDIF	
					
				PRINTLN("bennycutscene [interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: CREATE_CONVERSATION true")	
				
				SET_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)	
						
				// fade screen in
				IF NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					BUSYSPINNER_OFF()
					DO_SCREEN_FADE_IN(1000)
				ENDIF	
			
				RESET_SYNCED_INTERACTION_TIMER()	
				
				//TODO open door
				//g_bBennyCutsceneDoor = TRUE
				
				MPGlobalsInteractions.iPreCutsceneState++
			ELSE
				PRINTLN("bennycutscene [interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: CREATE_CONVERSATION false")	
			ENDIF
		
		BREAK
		CASE 4
		//wait for camera shot 1 to finish 
		
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 13000)
				PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: wait for camera shot 1 to finish complete")
				MPGlobalsInteractions.iPreCutsceneState++
			ELSE
				#IF IS_DEBUG_BUILD
					iTime = GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime)
					PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE - waiting for camera shot 1 - iTime ", iTime)
				#ENDIF
			ENDIF	
		BREAK
		CASE 5
		//setup camera shot 2
		
			//TODO SHUT DOOR
			//g_bBennyCutsceneDoor = FALSE
		
			// destroy existing cameras
			IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
				DESTROY_CAM(MPGlobalsInteractions.SyncInteractionCam)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)	
			ENDIF		
			DESTROY_ALL_CAMS()
			
			// setup camera
			MPGlobalsInteractions.SyncInteractionCam = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
			SET_CAM_SPLINE_SMOOTHING_STYLE(MPGlobalsInteractions.SyncInteractionCam, CAM_SPLINE_NO_SMOOTH)	

			// start cam node
			ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
				CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
					cam2StartPosition, cam2StartRotation,
					cam2FOV, 
					TRUE), 
				14000)
			
			// end cam node
			ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
				CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
					cam2EndPosition, cam2EndRotation,
					cam2FOV, 
					TRUE), 				
				14000)	
				
			SHAKE_CAM(MPGlobalsInteractions.SyncInteractionCam, "HAND_SHAKE", 0.03)
				
			SET_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
		
			RESET_SYNCED_INTERACTION_TIMER()			
			PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: camera shot 2 setup complete")
		
			MPGlobalsInteractions.iPreCutsceneState++
		BREAK
		CASE 6
		//wait for camera shot 2 to finish		
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 14000)			
				PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE wait for camera shot 2 to finish complete")
				MPGlobalsInteractions.iPreCutsceneState++
			ELSE
				#IF IS_DEBUG_BUILD
					iTime = GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime)
					PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE - waiting for camera shot 2 - iTime ", iTime)
				#ENDIF		
			ENDIF		
		BREAK
		CASE 7
		//setup camera shot 3
		
			// destroy existing cameras
			IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
				DESTROY_CAM(MPGlobalsInteractions.SyncInteractionCam)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)	
			ENDIF		
			DESTROY_ALL_CAMS()
			
			// setup camera
			MPGlobalsInteractions.SyncInteractionCam = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
			SET_CAM_SPLINE_SMOOTHING_STYLE(MPGlobalsInteractions.SyncInteractionCam, CAM_SPLINE_SLOW_OUT_SMOOTH)	

			// start cam node
			ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
				CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
					cam3StartPosition, cam3StartRotation,
					cam3FOV, 
					TRUE), 
				12500)
			
			// end cam node
			ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
				CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
					cam3EndPosition, cam3EndRotation,
					cam3FOV, 
					TRUE), 				
				12500)	
				
			SHAKE_CAM(MPGlobalsInteractions.SyncInteractionCam, "HAND_SHAKE", cameraShake)
				
			SET_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
		
			RESET_SYNCED_INTERACTION_TIMER()			
			PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: camera shot 3 setup complete")
		
			MPGlobalsInteractions.iPreCutsceneState++
		BREAK
		
		// wait for camera shot 3 to finish, and apply skyblur.
		CASE 8
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 13000)		
				SET_SKYBLUR_BLURRY()
				RESET_SYNCED_INTERACTION_TIMER()
				MPGlobalsInteractions.iPreCutsceneState++
				PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE camera shot 3 finished, applying blur")
			ELSE
				#IF IS_DEBUG_BUILD
					iTime = GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime)
					PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE - waiting for camera shot 3 - iTime ", iTime)
				#ENDIF	
			ENDIF
			
		BREAK
		
		CASE 9
		//wait at least one second for blur		
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 1000)			
				TOGGLE_RENDERPHASES(FALSE)
				RESET_SYNCED_INTERACTION_TIMER()				
				PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE wait for camera shot 3 to finish complete")
				MPGlobalsInteractions.iPreCutsceneState = 98
				EXIT
			ELSE
				#IF IS_DEBUG_BUILD
					iTime = GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime)
					PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE - waiting for blur - iTime ", iTime)
				#ENDIF						
			ENDIF		
		BREAK
		
//		CASE 7
//		// fade screen out, new load scene
//			//IF IS_SCREEN_FADED_OUT()
//				IF(IS_NEW_LOAD_SCENE_ACTIVE())			
//					PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: new load scene, IS_NEW_LOAD_SCENE_ACTIVE = true, NEW_LOAD_SCENE_STOP")				
//					NEW_LOAD_SCENE_STOP()				
//				ENDIF			
//				PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: new load scene, NEW_LOAD_SCENE_START_SPHERE")			
//				NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()),1000.0)
//				RESET_SYNCED_INTERACTION_TIMER()	
//				MPGlobalsInteractions.iPreCutsceneState++
//			//ENDIF	
//		BREAK
//		CASE 8
//		//scene loaded
//			IF(IS_NEW_LOAD_SCENE_LOADED())
//				PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: scene loaded, IS_NEW_LOAD_SCENE_LOADED = true")
//				preCutsceneCleanup = true
//			ELIF(GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 12000)
//				PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE: scene loaded, MPGlobalsInteractions.SyncedInteractionStartTime expired")
//				preCutsceneCleanup = true
//			ENDIF
//		BREAK
	ENDSWITCH	
	
	
	
	//Cleanup
	IF (MPGlobalsInteractions.iPreCutsceneState = 98) 
	
		// destroy existing cameras
		IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
			DESTROY_CAM(MPGlobalsInteractions.SyncInteractionCam)			
		ENDIF		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)	
		DESTROY_ALL_CAMS()
		
		DELETE_VEHICLE(MPGlobalsInteractions.preCutsceneVeh)
		//CLEAR_AREA(<<-211.0600, -1324.1383, 29.8904>>,1,false) //clear vehicle
				
		garageInterior = GET_INTERIOR_AT_COORDS(<<-205,-1310,31>>)
		UNPIN_INTERIOR(garageInterior)
		
		NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE) 
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HAS_WATCHED_BENNY_CUTSCE, TRUE) // Need this so we only see this cutscene once. 
		SET_SYNCED_INTERACTION_PRE_CUTSCENE_AS_FINISHED()			
		PRINTLN("[interactions] PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE Cleanup complete")	
		
		SET_USE_DLC_DIALOGUE(FALSE)
	ENDIF

ENDPROC

PROC PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE()
	SWITCH MPGlobalsInteractions.iPreCutsceneID
		CASE 0 PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE_BENNYS_GARAGE() BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL DOES_SYNCED_INTERACTION_HAVE_PRE_CUTSCENE()
	IF (MPGlobalsInteractions.iPreCutsceneID != -1)
		IF (GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].iSceneID = -1)
			PRINTLN("[interactions] DOES_SYNCED_INTERACTION_HAVE_PRE_CUTSCENE - no valid scene id, resetting.")
			MPGlobalsInteractions.iPreCutsceneID = -1
			MPGlobalsInteractions.iPreCutsceneState = 0
		ELSE
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL HAS_SYNCED_INTERACTIONS_PRE_CUTSCENE_FINISHED()
	IF (MPGlobalsInteractions.iPreCutsceneState >= 99)
		RETURN(TRUE)
	ENDIF	
	RETURN(FALSE)
ENDFUNC

PROC GOTO_BAILED_SYNCED_INTERACTION_STATE()
	PRINTLN("[interactions] GOTO_BAILED_SYNCED_INTERACTION_STATE() called")
	IF NOT (MPGlobalsInteractions.PlayerInteraction.bHasCleanedUp)
		MPGlobalsInteractions.PlayerInteraction.bCleanupInteractionAnimImmediately = TRUE
	ENDIF
	MPGlobalsInteractions.iSyncedInteractionState = 50					
	RESET_SYNCED_INTERACTION_TIMER()
	// stop any of William's lingering audio - 2105937
	IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
		STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	ENDIF		
	
	#IF IS_DEBUG_BUILD
	IF (MPGlobalsInteractions.bDebug_FillSceneWithPeds)
		INT i
		REPEAT COUNT_OF(MPGlobalsInteractions.DebugPedID) i
			IF DOES_ENTITY_EXIST(MPGlobalsInteractions.DebugPedID[i])
				DELETE_PED(MPGlobalsInteractions.DebugPedID[i])
			ENDIF
		ENDREPEAT
	ENDIF
	#ENDIF
	
	// delete clone players
	IF (MPGlobalsInteractions.bUseClonePlayers)	
		INT iCloneCount
		REPEAT 3 iCloneCount
			IF DOES_ENTITY_EXIST(MPGlobalsInteractions.ClonePedID[iCloneCount])
				DELETE_PED(MPGlobalsInteractions.ClonePedID[iCloneCount])
			ENDIF
		ENDREPEAT
	ENDIF		
				
ENDPROC

PROC HIDE_HUD_FOR_SYNCED_INTERACTIONS(INT iSceneID)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	IF iSceneID = SIS_POST_ISLAND_HEIST
		HIDE_HELP_TEXT_THIS_FRAME()
	ENDIF
ENDPROC

PROC UPDATE_SYNCED_INTERACTIONS_CLIENT()
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	INT iAnim
	BOOL bSetVisibility
	INT iEndTime
	
	INT iThisLocalsync 
	FLOAT fSyncPhase
	VECTOR vFinalRenderCoords 
	VECTOR vFinalRenderRot 
	FLOAT fFinalRenderFOV 	
	
	BOOL bClonesAreReady
	
	VECTOR vCoords
	FLOAT fHeading	
	
	// stop falling ped from getting parachute, playing falling anim etc.
	IF (MPGlobalsInteractions.iSyncedInteractionState > -1)
	
		// if the player has started switching session then do nothing
		IF NOT IS_SKYSWOOP_AT_GROUND()
			PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - skyswoop not at ground, exiting.")		
			EXIT
		ELSE
	
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressInAirEvent, TRUE)
			g_vInitialSpawnLocation = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			
			UPDATE_RESTORE_CELEBRATION_AUDIO_STAGE()
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF (MPGlobalsInteractions.bPinInterior)
		IF VMAG(MPGlobalsInteractions.vSyncedInteractionCoords) > 0.0
			MPGlobalsInteractions.debug_InteriorInstance = GET_INTERIOR_AT_COORDS(MPGlobalsInteractions.vSyncedInteractionCoords)
			IF IS_VALID_INTERIOR(MPGlobalsInteractions.debug_InteriorInstance)
				PIN_INTERIOR_IN_MEMORY(MPGlobalsInteractions.debug_InteriorInstance)				
				IF IS_INTERIOR_READY(MPGlobalsInteractions.debug_InteriorInstance)
					SET_INTERIOR_IN_USE(MPGlobalsInteractions.debug_InteriorInstance)
					MPGlobalsInteractions.bPinInterior = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	#ENDIF
	
	
	SWITCH MPGlobalsInteractions.iSyncedInteractionState
		CASE -1
			// do nothing
		BREAK
		CASE 0		
			
			// do a pre-cutscene if desired.
			IF DOES_SYNCED_INTERACTION_HAVE_PRE_CUTSCENE()
				IF NOT HAS_SYNCED_INTERACTIONS_PRE_CUTSCENE_FINISHED()	
					PROCESS_SYNCED_INTERACTION_PRE_CUTSCENE()	
					EXIT // exit early, dont want to process anything else yet.
				ENDIF
			ENDIF
		
		
			// make sure screen is faded out
			IF NOT IS_SCREEN_FADED_OUT()		
				SET_NO_LOADING_SCREEN(TRUE)
				IF NOT DOES_SYNCED_INTERACTION_HAVE_PRE_CUTSCENE()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
			ENDIF
		
			// wait for server to recognise the fact i'm waiting
			IF (GlobalServerBD_SyncedInteractions.iSceneID[iPlayer] = GlobalplayerBD_Interactions[iPlayer].iSceneID)
			AND (GlobalServerBD_SyncedInteractions.iUniqueID[iPlayer] = GlobalplayerBD_Interactions[iPlayer].iUniqueID)
			AND (GlobalServerBD_SyncedInteractions.iLocation[iPlayer] != -1)
			AND (GlobalServerBD_SyncedInteractions.iActor[iPlayer] != -1)
				
				// make sure no other interactions are playing
				IF NOT (MPGlobalsInteractions.bAnyInteractionIsPlaying)
								
					// make sure a safe location was found
					IF (GlobalServerBD_SyncedInteractions.iLocation[iPlayer] != -2)
					#IF IS_DEBUG_BUILD
					AND NOT (MPGlobalsInteractions.bDebug_SyncedSceneFailToGetLocation)
					AND NOT (MPGlobalsInteractions.bTestBail1)
					#ENDIF
					
					
						// make sure i have the correct anim dictionary loaded.
						IF HAS_SYNCED_INTERACTION_SCENE_LOADED(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer])
								

							// get coords
							IF IS_FULL_SYNC_SCENE_LOCATION(GlobalplayerBD_Interactions[iPlayer].iSceneID)
								MPGlobalsInteractions.vSyncedInteractionCoords = GET_SYNCED_INTERACTION_SCENE_COORDS(GlobalplayerBD_Interactions[iPlayer].iSceneID, 0)
								MPGlobalsInteractions.fSyncedInteractionHeading = GET_SYNC_INTERACTION_HEADING(GlobalplayerBD_Interactions[iPlayer].iSceneID, 0)
							ELSE
								GET_POSITION_AND_HEADING_FOR_ACTOR_IN_SYNCED_INTERACTION(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer], GlobalServerBD_SyncedInteractions.iActor[iPlayer], MPGlobalsInteractions.vSyncedInteractionCoords, MPGlobalsInteractions.fSyncedInteractionHeading)
							ENDIF

							// fill scene with dummy peds
							#IF IS_DEBUG_BUILD												
								IF (MPGlobalsInteractions.bDebug_FillSceneWithPeds)

									IF (MPGlobalsInteractions.bUseClonePlayers)
										PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - unsetting bUseClonePlayers because bDebug_FillSceneWithPeds is set.")
										MPGlobalsInteractions.bUseClonePlayers = FALSE
									ENDIF														
								
									INT i
									INT iDummyPedCount
									INT iFreeSlot
									
									REPEAT COUNT_OF(MPGlobalsInteractions.iDebugPedActor) i
										MPGlobalsInteractions.iDebugPedActor[i] = -1
									ENDREPEAT									
									
									REPEAT GET_NUMBER_OF_ACTORS_IN_LAYOUT(gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].iLayout[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]]) i								
										iFreeSlot = GetNextFreeActorSlotForSceneLocation(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer], GlobalServerBD_SyncedInteractions.iUniqueID[iPlayer])									
										IF (iFreeSlot > -1)
										AND (iDummyPedCount < 3)
											GET_POSITION_AND_HEADING_FOR_ACTOR_IN_SYNCED_INTERACTION(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer], iFreeSlot, vCoords, fHeading)
											MPGlobalsInteractions.DebugPedID[iDummyPedCount] = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), vCoords, fHeading, FALSE, FALSE)
											MPGlobalsInteractions.iDebugPedActor[iDummyPedCount] = iFreeSlot										
											PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - created dummy ped ", iDummyPedCount, " for Actor slot ", iFreeSlot)
											iDummyPedCount++										
										ENDIF
									ENDREPEAT
								
								ENDIF
							#ENDIF
													
							SETUP_SYNCED_INTERACTION_FOR_CUTSCENE()			
							
							// add blocking areas
							MPGlobalsInteractions.ScenarioBlockingID = ADD_SCENARIO_BLOCKING_AREA(MPGlobalsInteractions.vSyncedInteractionCoords - <<PATH_BLOCKING_DIST, PATH_BLOCKING_DIST, PATH_BLOCKING_DIST>>, MPGlobalsInteractions.vSyncedInteractionCoords + <<PATH_BLOCKING_DIST, PATH_BLOCKING_DIST, PATH_BLOCKING_DIST>>) 
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - MPGlobalsInteractions.ScenarioBlockingID = ", NATIVE_TO_INT(MPGlobalsInteractions.ScenarioBlockingID))				
							SET_PED_PATHS_IN_AREA(MPGlobalsInteractions.vSyncedInteractionCoords - <<PATH_BLOCKING_DIST, PATH_BLOCKING_DIST, PATH_BLOCKING_DIST>>, MPGlobalsInteractions.vSyncedInteractionCoords + <<PATH_BLOCKING_DIST, PATH_BLOCKING_DIST, PATH_BLOCKING_DIST>>, FALSE)

							// make sure player is disarmed
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)							
													
							// display spinner icon
							BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
							END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
		
							// go to net warp
							MPGlobalsInteractions.iSyncedInteractionState++
							
							
						ELSE
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for anims to load for scene")
						ENDIF
						
					ELSE
					
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - could not find safe location, doing fallback.")
						
						IF IS_FULL_SYNC_SCENE_LOCATION(GlobalplayerBD_Interactions[iPlayer].iSceneID)
						#IF IS_DEBUG_BUILD
						AND NOT (MPGlobalsInteractions.bTestBail1)
						#ENDIF
							GlobalplayerBD_Interactions[iPlayer].iSceneID = GET_FULL_SYNC_SCENE_FALLBACK(GlobalplayerBD_Interactions[iPlayer].iSceneID)
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - using full sync scene fallback - ", GlobalplayerBD_Interactions[iPlayer].iSceneID)
						ELSE
							// we need to just spawn in individual locations
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - using individual fallback.")					
							GOTO_BAILED_SYNCED_INTERACTION_STATE()
						ENDIF
						
					ENDIF
				
				ELSE
					MPGlobalsInteractions.PlayerInteraction.bCleanupInteractionAnimImmediately = TRUE
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - bCleanupInteractionAnimImmediately set to TRUE.")
				ENDIF
			
			ELSE
				
				#IF IS_DEBUG_BUILD
			
					IF NOT (GlobalServerBD_SyncedInteractions.iSceneID[iPlayer] = GlobalplayerBD_Interactions[iPlayer].iSceneID)
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for server to recognise my scene id request.")	
					ENDIF
					
					IF NOT (GlobalServerBD_SyncedInteractions.iUniqueID[iPlayer] = GlobalplayerBD_Interactions[iPlayer].iUniqueID)
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for server to recognise my unique id request.")	
					ENDIF
			
					IF (GlobalServerBD_SyncedInteractions.iLocation[iPlayer] = -1)
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for server to assign location.")
					ENDIF
					
					IF (GlobalServerBD_SyncedInteractions.iActor[iPlayer] = -1)
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for server to assign actor.")
					ENDIF

				#ENDIF
				
//				// have we timed out waiting for server?
//				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 20000)
//					PRINTLN("[interactions] - timed out waiting for server. Play scene on own.")
//					SCRIPT_ASSERT("[interactions] - timed out waiting for server. bug neil f.")
//
//					INT iPlayerHash[4] 
//					iPlayerHash[0] = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
//					iPlayerHash[1] = -1
//					iPlayerHash[2] = -1
//					iPlayerHash[3] = -1
//			
//					START_SYNCED_INTERACTION_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID, iPlayerHash)
//				ENDIF
				
			ENDIF
		BREAK
		
		// complete net warp
		CASE 1
			IF NET_WARP_TO_COORD(MPGlobalsInteractions.vSyncedInteractionCoords, MPGlobalsInteractions.fSyncedInteractionHeading, FALSE, FALSE)
						
				CLEAR_AREA_OF_PEDS(MPGlobalsInteractions.vSyncedInteractionCoords, PATH_BLOCKING_DIST, TRUE)
				
				RESET_SYNCED_INTERACTION_TIMER()
				MPGlobalsInteractions.iSyncedInteractionState++				
				
				// create custom car nodes.
				ADD_CUSTOM_CAR_NODES_FOR_SYNC_INTERACTION(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer])
				
				// warp my pv near
				REQUEST_PERSONAL_VEHICLE_TO_WARP_NEAR(FALSE, TRUE)
				
				// switch back on collision and unfreeze so we don't see the player 'pop' up when gameplay is restored. 
				SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				
				// make sure ped is not considered inside an interior 2208645
				CLEAR_ROOM_FOR_ENTITY(PLAYER_PED_ID())
				
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - net warp completed.")
				
			ENDIF
		BREAK
	
		// make sure all other players have at least joined the session and have been assigned actor positions before creating clones for them 
		CASE 2
		
			KeepMeInCorrectPositionForSyncScene()
		
			IF HasSeverProcessedAllPlayersInHashlistWithUniqueID(GlobalplayerBD_Interactions[iPlayer].iUniqueID, GlobalplayerBD_Interactions[iPlayer].iPlayerNamesHash)
			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 25000)
			
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE) // url:bugstar:5802222 - Story - Defend - Remote players PEDs could be seen falling to the ground after returning to freemode following the end cutscene			
			
				IF IS_FULL_SYNC_SCENE_LOCATION(GlobalplayerBD_Interactions[iPlayer].iSceneID)
				
					IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
						DESTROY_CAM(MPGlobalsInteractions.SyncInteractionCam)
					ENDIF
				
					RESET_SYNCED_INTERACTION_TIMER()
					MPGlobalsInteractions.iSyncedInteractionState = 20
					
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - is a full sync scene.")
					
				ELSE
					
					// make sure tasks are clear
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							
					iAnim = ENUM_TO_INT(GET_INTERACTION_FOR_ACTOR_IN_SYNCED_INTERACTION(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer], GlobalServerBD_SyncedInteractions.iActor[iPlayer]))					
					START_INTERACTION_ANIM(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), iAnim, FALSE, FALSE, TRUE, FALSE, TRUE)
					
					// start interactions on any dummy peds
					#IF IS_DEBUG_BUILD
					IF (MPGlobalsInteractions.bDebug_FillSceneWithPeds)
					AND NOT (MPGlobalsInteractions.bUseClonePlayers)
					
						INT i
									
						REPEAT COUNT_OF(MPGlobalsInteractions.DebugPedID) i
							IF DOES_ENTITY_EXIST(MPGlobalsInteractions.DebugPedID[i])		
							
								// make sure they are in correct position
								IF NOT IS_ENTITY_DEAD(MPGlobalsInteractions.DebugPedID[i])	
									GET_POSITION_AND_HEADING_FOR_ACTOR_IN_SYNCED_INTERACTION(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer], MPGlobalsInteractions.iDebugPedActor[i], vCoords, fHeading)								
									vCoords.z += 1.0
									GET_GROUND_Z_FOR_3D_COORD(vCoords, vCoords.z)
									SET_ENTITY_COORDS(MPGlobalsInteractions.DebugPedID[i], vCoords)
									SET_ENTITY_HEADING(MPGlobalsInteractions.DebugPedID[i], fHeading)
								ENDIF
							
								MPGlobalsInteractions.PedInteraction[i].iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL)
								MPGlobalsInteractions.PedInteraction[i].iInteractionAnim = ENUM_TO_INT(GET_INTERACTION_FOR_ACTOR_IN_SYNCED_INTERACTION(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer], MPGlobalsInteractions.iDebugPedActor[i] ))					
								MPGlobalsInteractions.PedInteraction[i].bPlayInteractionAnim = TRUE
								MPGlobalsInteractions.PedInteraction[i].bWaitForSync = TRUE					
								MPGlobalsInteractions.PedInteraction[i].bPlayInteractionAnimFullBody = TRUE	
								
								SET_FACIAL_IDLE_ANIM_OVERRIDE(MPGlobalsInteractions.DebugPedID[i], GET_PLAYER_MOOD_ANIM_FROM_INDEX(GET_RANDOM_INT_IN_RANGE(0,8)))
								
							ENDIF
						ENDREPEAT
					ENDIF
					#ENDIF
					
					// use clones of the players?
					IF (MPGlobalsInteractions.bUseClonePlayers)				
					
						INT iClonePlayer
						INT iCloneCount
						
						// create all other players in scene as clones
						REPEAT NUM_NETWORK_PLAYERS iClonePlayer
							//IF NOT (iClonePlayer = iPlayer)
							IF (iCloneCount < 4)
							
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - iClonePlayer = ", iClonePlayer)	
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - GlobalServerBD_SyncedInteractions.iUniqueID[iClonePlayer] = ", GlobalServerBD_SyncedInteractions.iUniqueID[iClonePlayer])	
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - GlobalServerBD_SyncedInteractions.iUniqueID[iPlayer] = ", GlobalServerBD_SyncedInteractions.iUniqueID[iPlayer])	
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - GlobalServerBD_SyncedInteractions.iSceneID[iClonePlayer] = ", GlobalServerBD_SyncedInteractions.iSceneID[iClonePlayer])	
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - GlobalServerBD_SyncedInteractions.iSceneID[iPlayer] = ", GlobalServerBD_SyncedInteractions.iSceneID[iPlayer])								
							
								IF (GlobalServerBD_SyncedInteractions.iUniqueID[iClonePlayer] = GlobalServerBD_SyncedInteractions.iUniqueID[iPlayer])
								AND (GlobalServerBD_SyncedInteractions.iSceneID[iClonePlayer] = GlobalServerBD_SyncedInteractions.iSceneID[iPlayer])
								AND (GlobalServerBD_SyncedInteractions.iLocation[iClonePlayer] = GlobalServerBD_SyncedInteractions.iLocation[iPlayer])
									
									// create and place in correct position
									GET_POSITION_AND_HEADING_FOR_ACTOR_IN_SYNCED_INTERACTION(GlobalServerBD_SyncedInteractions.iSceneID[iClonePlayer], GlobalServerBD_SyncedInteractions.iLocation[iClonePlayer], GlobalServerBD_SyncedInteractions.iActor[iClonePlayer], vCoords, fHeading)	
									#IF IS_DEBUG_BUILD
										IF (MPGlobalsInteractions.bDebug_FillSceneWithPeds)
											MPGlobalsInteractions.ClonePedID[iCloneCount] = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(PLAYER_PED_ID()), vCoords, fHeading, FALSE, FALSE)
											PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - bDebug_FillSceneWithPeds - created clone iCloneCount = ", iCloneCount)
										ELSE
									#ENDIF
									
									//MPGlobalsInteractions.ClonePedID[iCloneCount] = CLONE_PED(GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, iClonePlayer)), FALSE, FALSE)
									//PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - created clone of player = ", iClonePlayer, ", iCloneCount = ", iCloneCount)
									
									#IF IS_DEBUG_BUILD
										ENDIF
									#ENDIF
									vCoords.z += 1.0
									GET_GROUND_Z_FOR_3D_COORD(vCoords, vCoords.z)
									
									BOOL bWearingProblemMaskOrHat
									
									bWearingProblemMaskOrHat = IS_PED_WEARING_PROBLEM_MASK_OR_HAT_FOR_INTERACTION(	GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, iClonePlayer)), 
																							ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), 
																							ENUM_TO_INT(GET_INTERACTION_FOR_ACTOR_IN_SYNCED_INTERACTION(GlobalServerBD_SyncedInteractions.iSceneID[iClonePlayer], GlobalServerBD_SyncedInteractions.iLocation[iClonePlayer], GlobalServerBD_SyncedInteractions.iActor[iClonePlayer] )),
																							TRUE, 
																							FALSE)									
									
									CREATE_CLONE_OF_PED_AT_COORDS(MPGlobalsInteractions.ClonePedID[iCloneCount], GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, iClonePlayer)), vCoords, fHeading, iClonePlayer, bWearingProblemMaskOrHat)
									
									
									
									SET_ENTITY_COLLISION(MPGlobalsInteractions.ClonePedID[iCloneCount], TRUE)
									SET_ENTITY_COORDS(MPGlobalsInteractions.ClonePedID[iCloneCount], vCoords)
									SET_ENTITY_HEADING(MPGlobalsInteractions.ClonePedID[iCloneCount], fHeading)
									SET_ENTITY_NO_COLLISION_ENTITY(MPGlobalsInteractions.ClonePedID[iCloneCount], GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, iClonePlayer)), FALSE)
									FREEZE_ENTITY_POSITION(MPGlobalsInteractions.ClonePedID[iCloneCount], FALSE)
									
									// give correct interaction anim
									MPGlobalsInteractions.PedInteraction[iCloneCount].iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL)
									MPGlobalsInteractions.PedInteraction[iCloneCount].iInteractionAnim = ENUM_TO_INT(GET_INTERACTION_FOR_ACTOR_IN_SYNCED_INTERACTION(GlobalServerBD_SyncedInteractions.iSceneID[iClonePlayer], GlobalServerBD_SyncedInteractions.iLocation[iClonePlayer], GlobalServerBD_SyncedInteractions.iActor[iClonePlayer] ))					
									MPGlobalsInteractions.PedInteraction[iCloneCount].bPlayInteractionAnim = TRUE
									MPGlobalsInteractions.PedInteraction[iCloneCount].bWaitForSync = TRUE					
									MPGlobalsInteractions.PedInteraction[iCloneCount].bPlayInteractionAnimFullBody = TRUE
									
									SET_FACIAL_IDLE_ANIM_OVERRIDE(MPGlobalsInteractions.ClonePedID[iCloneCount], GET_PLAYER_MOOD_ANIM_FROM_INDEX(GlobalplayerBD_FM[iClonePlayer].iPlayerMood))
									
									iCloneCount++
								
								ELSE
									PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - data doesn't matche - iClonePlayer = ", iClonePlayer)	
								ENDIF
							ENDIF
						ENDREPEAT								
					
					ENDIF				
					
					IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
						DESTROY_CAM(MPGlobalsInteractions.SyncInteractionCam)
					ENDIF
					MPGlobalsInteractions.SyncInteractionCam = INT_TO_NATIVE(CAMERA_INDEX, -1)
										
					RESET_SYNCED_INTERACTION_TIMER()
					MPGlobalsInteractions.iSyncedInteractionState++
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - started playing iAnim ", iAnim, ", moving onto to wait for sync")			
				ENDIF	
				
			ELSE
			
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for HasSeverProcessedAllPlayersInHashlistWithUniqueID, time = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime))

			ENDIF
		BREAK
	
		// wait until everyon is ready and in position
		CASE 3
			bClonesAreReady = TRUE
			IF (MPGlobalsInteractions.bUseClonePlayers)	
				INT i				
				REPEAT 4 i
					IF DOES_ENTITY_EXIST(MPGlobalsInteractions.ClonePedID[i])
						IF NOT IS_ENTITY_DEAD(MPGlobalsInteractions.ClonePedID[i])
							IF (GET_PED_DECORATIONS_STATE(MPGlobalsInteractions.ClonePedID[i]) = PDS_IN_PROGRESS)
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - clone ped decorations not ready - ", i)
								bClonesAreReady = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		
		
			// tell others we're ready to start once anims are in start position
			IF NOT (GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].bReadyToStart)
				IF (bClonesAreReady)
					IF IsAnimAtDesiredStartPhase(gInteractionsPlayerData.PlayingInteraction)
						IF NOT HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
							GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].bReadyToStart = TRUE
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - setting bReadyToStart = TRUE")
						ELSE
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for NOT HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR 1.")
						ENDIF
					ELSE
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for IsAnimAtDesiredStartPhase.")	
					ENDIF
				ELSE
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for bClonesAreReady.")
				ENDIF
			ENDIF
			
			// make sure i dont move from here
			KeepMeInCorrectPositionForSyncScene()
		
			IF AreAllPlayersInHashListWithUniqueIDReady(GlobalplayerBD_Interactions[iPlayer].iUniqueID, GlobalplayerBD_Interactions[iPlayer].iPlayerNamesHash)
			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 20000)	
			#IF IS_DEBUG_BUILD
			OR (MPGlobalsInteractions.bTestBail2)
			#ENDIF
			
				// is this area still ok?
				IF (IsSyncInteractionLocationSafeFromObstructions(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalplayerBD_Interactions[iPlayer].iUniqueID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer], GlobalplayerBD_Interactions[iPlayer].iPlayerNamesHash)
				AND NOT IS_POINT_IN_GANG_ATTACK_OR_MISSION_CORONA(GET_SYNCED_INTERACTION_SCENE_COORDS(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer]), GANG_ATTACK_RADIUS_MULTIPLIER))
				
				// force play this location even if it's obstructed
				#IF IS_DEBUG_BUILD
				AND NOT (MPGlobalsInteractions.bTestBail2)
				OR ((GlobalplayerBD_Interactions[iPlayer].iForceLocation = GlobalServerBD_SyncedInteractions.iLocation[iPlayer]) AND (MPGlobalsInteractions.bTestBail2 = FALSE))
				#ENDIF	
			
					CLEAR_AREA(GET_PLAYER_COORDS(PLAYER_ID()), 7.0, TRUE)	
					CLEAR_AREA_OF_VEHICLES(GET_PLAYER_COORDS(PLAYER_ID()), 7.0, TRUE, FALSE, TRUE, TRUE)
			
					RESET_SYNCED_INTERACTION_TIMER()
					MPGlobalsInteractions.iSyncedInteractionState++
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - ready to start anims.")	
									
				ELSE

					// do fallback
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - location no longer safe, bailing.")
					
					#IF IS_DEBUG_BUILD						
						IF NOT IsSyncInteractionLocationSafeFromObstructions(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalplayerBD_Interactions[iPlayer].iUniqueID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer], GlobalplayerBD_Interactions[iPlayer].iPlayerNamesHash)
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - IsSyncInteractionLocationSafeFromObstructions = false")	
						ENDIF						
						IF IS_POINT_IN_GANG_ATTACK_OR_MISSION_CORONA(GET_SYNCED_INTERACTION_SCENE_COORDS(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer]), GANG_ATTACK_RADIUS_MULTIPLIER)
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - IS_POINT_IN_GANG_ATTACK_OR_MISSION_CORONA = TRUE")
						ENDIF					
					#ENDIF
					
					GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].bAbandon = TRUE // lets other players know i've bailed.
					GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].bReadyToStart = FALSE

					GOTO_BAILED_SYNCED_INTERACTION_STATE()
					
				ENDIF
			
			ELSE
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for all players  to be ready - time = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime))		
				
				#IF IS_DEBUG_BUILD
					IF NOT AreAllPlayersInHashListWithUniqueIDReady(GlobalplayerBD_Interactions[iPlayer].iUniqueID, GlobalplayerBD_Interactions[iPlayer].iPlayerNamesHash)
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for AreAllPlayersInHashListWithUniqueIDReady.")		
					ENDIF	
				#ENDIF
				
			ENDIF
		BREAK
	
		// fade in and launch the anims
		CASE 4
		
			// make sure i dont move from here
			KeepMeInCorrectPositionForSyncScene()
			
			HIDE_HUD_FOR_SYNCED_INTERACTIONS(GlobalplayerBD_Interactions[iPlayer].iSceneID)
			SetNearbyScriptVehiclesInvisibleLocally()
			
			// wait for all other players in scene to be ready.
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > (MIN_WAIT_TIME_FOR_ANIM_TO_START + MIN_WAIT_TIME_FOR_CLEAR_AREA))																						
			
				// start the anims				
				MPGlobalsInteractions.bHasBeenSynced = TRUE
				#IF IS_DEBUG_BUILD
					IF (MPGlobalsInteractions.bSyncInteractionSnapToFirstCam)
						MPGlobalsInteractions.bHasBeenSynced = FALSE	
					ENDIF
				#ENDIF

				// make all players in the scene visible
				IF NOT (MPGlobalsInteractions.bUseClonePlayers)		
					SetAllPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)
				ELSE
					SetOtherPlayersInvisibleLocally()
					//SET_PLAYER_VISIBLE_LOCALLY(PLAYER_ID())
					SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
				ENDIF
				
				#IF IS_DEBUG_BUILD
					IF (MPGlobalsInteractions.bSyncInteractionSnapToFirstCam)
					
						IF NOT DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
							DESTROY_ALL_CAMS()
							MPGlobalsInteractions.SyncInteractionCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")																
							SET_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam, TRUE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)											
						ENDIF																					
						
						IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
						
							IF NOT (MPGlobalsInteractions.bSyncInteractionSnapToEndCam)
						
								SET_CAM_COORD(MPGlobalsInteractions.SyncInteractionCam, gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].vCamStartPos[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]]) 
								SET_CAM_ROT(MPGlobalsInteractions.SyncInteractionCam, gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].vCamStartRot[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]])
								SET_CAM_FOV(MPGlobalsInteractions.SyncInteractionCam, gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].fCamStartFOV[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]])	
								
							ELSE
							
								SET_CAM_COORD(MPGlobalsInteractions.SyncInteractionCam, gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].vCamEndPos[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]]) 
								SET_CAM_ROT(MPGlobalsInteractions.SyncInteractionCam, gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].vCamEndRot[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]])
								SET_CAM_FOV(MPGlobalsInteractions.SyncInteractionCam, gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].fCamEndFOV[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]])																		
								
							ENDIF
						ENDIF
						
						IF NOT (MPGlobalsInteractions.fSyncStoredShakeAmplitute = gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].fShakeAmplitude[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]])
							SHAKE_SCRIPT_GLOBAL("HAND_SHAKE", gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].fShakeAmplitude[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]])
							MPGlobalsInteractions.fSyncStoredShakeAmplitute = gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].fShakeAmplitude[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]]
						ENDIF
						
					ENDIF
				#ENDIF
				
				// start the camera (if there is data setup for one)
				IF (VMAG(gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].vCamStartPos[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]]) > 0.0)
				#IF IS_DEBUG_BUILD
				AND NOT (MPGlobalsInteractions.bSyncInteractionSnapToFirstCam)									
				#ENDIF
				
					IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
						DESTROY_CAM(MPGlobalsInteractions.SyncInteractionCam)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)	
					ENDIF
				
					DESTROY_ALL_CAMS()
					MPGlobalsInteractions.SyncInteractionCam = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
					SET_CAM_SPLINE_SMOOTHING_STYLE(MPGlobalsInteractions.SyncInteractionCam, CAM_SPLINE_NO_SMOOTH)	
					
					IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
						iEndTime = gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].iCamEndTime[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]] + SHOULDER_CAM_DURATION
					ELSE
						iEndTime = gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].iCamEndTime[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]]	
					ENDIF					
					
					// start cam node
					ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
						CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
							gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].vCamStartPos[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]], 
							gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].vCamStartRot[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]], 
							gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].fCamStartFOV[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]], 
							TRUE), 
							//gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].iCamDuration[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]])
							iEndTime)
					// end cam node
					ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
						CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
							gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].vCamEndPos[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]], 
							gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].vCamEndRot[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]], 
							gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].fCamEndFOV[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]], 
							TRUE), 
							//gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].iCamDuration[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]])
							iEndTime)
							
					SET_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)								
					
					SHAKE_SCRIPT_GLOBAL("HAND_SHAKE", gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].fShakeAmplitude[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]])
				ENDIF									
				
				#IF IS_DEBUG_BUILD
					IF (MPGlobalsInteractions.bSyncInteractionSnapToFirstCam)
						MPGlobalsInteractions.StoredSyncTime = MPGlobalsInteractions.SyncedInteractionStartTime
					ENDIF
				#ENDIF
				
				// go to next stage
				RESET_SYNCED_INTERACTION_TIMER()
				MPGlobalsInteractions.iSyncedInteractionState++				
				
				RESTORE_AUDIO_FOR_POST_MISSION_CUTSCENE()
				
				#IF IS_DEBUG_BUILD
					IF (MPGlobalsInteractions.bSyncInteractionSnapToFirstCam)
						
						MPGlobalsInteractions.SyncedInteractionStartTime = MPGlobalsInteractions.StoredSyncTime
						MPGlobalsInteractions.iSyncedInteractionState--
						
						// fade screen in
						IF NOT IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(0)
						ENDIF							
					ENDIF
				#ENDIF
					
				
			ELSE
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting to start anims - time = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime))		
				
			ENDIF
			
		BREAK
		
		// wait to cut to gameplay cam
		CASE 5
						
		
			bSetVisibility = TRUE
			
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() // this command should be in next_gen only
			
			IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
				
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					iEndTime = gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].iCamEndTime[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]] + SHOULDER_CAM_DURATION
				ELSE
					iEndTime = gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].iCamEndTime[GlobalServerBD_SyncedInteractions.iLocation[iPlayer]]	
				ENDIF
				
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > iEndTime)

					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - finished initial shot, cleaning up mp cutscene a frame early")

					// this needs to be called a frame before we switch camera etc.
					CLEANUP_MP_CUTSCENE(DEFAULT, FALSE) // switched back on hud elements.
					MPGlobalsInteractions.iSyncedInteractionState++
					
					// clear any vehicles / objects that are colliding with players position
					ClearAreaAroundPointForSpawning(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
					
				ELSE
				
				
					// should we trigger the 1st person flashy fx?
					IF NOT (MPGlobalsInteractions.bFirstPersonFlashTriggered)
						IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
						AND (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) >= (iEndTime - 300))
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")	
							MPGlobalsInteractions.bFirstPersonFlashTriggered = TRUE
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - triggered 1st person flashy")
						ENDIF
					ENDIF						
				
				
					bSetVisibility = TRUE
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for scripted camera to finish, time = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime))	
				ENDIF
			
			ELSE
				bSetVisibility = TRUE		
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 2000)	
					MPGlobalsInteractions.iSyncedInteractionState = 99		
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - finishing shot with no camera")
				ELSE
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - camera does not exist = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime))	
				ENDIF
				
			ENDIF
			
			IF (bSetVisibility)
				IF NOT (MPGlobalsInteractions.bUseClonePlayers)
					SetAllPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)
				ELSE
					SetOtherPlayersInvisibleLocally()
					//SET_PLAYER_VISIBLE_LOCALLY(PLAYER_ID())
					SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
				ENDIF
				HIDE_HUD_FOR_SYNCED_INTERACTIONS(GlobalplayerBD_Interactions[iPlayer].iSceneID)
				SetNearbyScriptVehiclesInvisibleLocally()
			ENDIF
			
			// fade screen in
			IF NOT DOES_SYNCED_INTERACTION_HAVE_PRE_CUTSCENE()
				IF NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					BUSYSPINNER_OFF()
					DO_SCREEN_FADE_IN(1000)
				ENDIF
			ELSE
				IF GET_SKYFREEZE_STAGE() = SKYFREEZE_FROZEN
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT restoring renderphases.")
					BUSYSPINNER_OFF()
					TOGGLE_RENDERPHASES(TRUE)
					SET_SKYBLUR_CLEAR()
				ENDIF
			ENDIF
		BREAK
		
		// finish cut to shoulder cam
		CASE 6
			IF IS_SCRIPT_GLOBAL_SHAKING()
				STOP_SCRIPT_GLOBAL_SHAKING()	
			ENDIF
		
			DESTROY_ALL_CAMS()		
		
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)	
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)														
													
			// delete clone players
			IF (MPGlobalsInteractions.bUseClonePlayers)	
				INT iCloneCount
				REPEAT 4 iCloneCount
					IF DOES_ENTITY_EXIST(MPGlobalsInteractions.ClonePedID[iCloneCount])
						DELETE_PED(MPGlobalsInteractions.ClonePedID[iCloneCount])
					ENDIF
				ENDREPEAT						
	
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					SetOtherPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)
					SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID()) 
				ELSE
					SetAllPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)	
				ENDIF
			ENDIF			

			// create new camaera on shoulder of player
			MPGlobalsInteractions.SyncInteractionCam = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
			
			GET_POSITION_AND_HEADING_FOR_ACTOR_IN_SYNCED_INTERACTION(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iLocation[iPlayer], GlobalServerBD_SyncedInteractions.iActor[iPlayer], vCoords, fHeading, 0.99)								
			vCoords.z += 1.0
			PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - end coords = ", vCoords)
			PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - end heading = ", fHeading)

			IF NOT SHOULD_USE_NEAR_SHOULDER_CAM(GlobalServerBD_SyncedInteractions.iActor[iPlayer])					
			
				// start cam node
				ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
					CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
						//GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<SHOULDER_CAM_START_OFFSET_X, SHOULDER_CAM_START_OFFSET_Y, SHOULDER_CAM_START_OFFSET_Z>>), 
						GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS ( vCoords, fHeading, <<SHOULDER_CAM_START_OFFSET_X, SHOULDER_CAM_START_OFFSET_Y, SHOULDER_CAM_START_OFFSET_Z>> ),
						<<SHOULDER_CAM_START_ROT_OFFSET_X, SHOULDER_CAM_START_ROT_OFFSET_Y, fHeading + SHOULDER_CAM_START_ROT_OFFSET_Z>>, 
						SHOULDER_CAM_FOV, 
						TRUE), 
						SHOULDER_CAM_DURATION)
				
				// end cam node
				ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
					CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
						//GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<SHOULDER_CAM_END_OFFSET_X, SHOULDER_CAM_END_OFFSET_Y, SHOULDER_CAM_END_OFFSET_Z>>), 
						GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS ( vCoords, fHeading, <<SHOULDER_CAM_END_OFFSET_X, SHOULDER_CAM_END_OFFSET_Y, SHOULDER_CAM_END_OFFSET_Z>> ),
						<<SHOULDER_CAM_END_ROT_OFFSET_X, SHOULDER_CAM_END_ROT_OFFSET_Y, fHeading + SHOULDER_CAM_END_ROT_OFFSET_Z>>, 
						SHOULDER_CAM_FOV, 
						TRUE), 
						SHOULDER_CAM_DURATION)
					
				SET_CAM_SPLINE_NODE_EASE(MPGlobalsInteractions.SyncInteractionCam, 1, CAM_SPLINE_NODE_EASE_OUT)
				
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - set up camera behind shoulder")
				
			ELSE
			
				// start cam node
				ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
					CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
						//GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<NEAR_SHOULDER_CAM_START_OFFSET_X, NEAR_SHOULDER_CAM_START_OFFSET_Y, NEAR_SHOULDER_CAM_START_OFFSET_Z>>), 
						GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS ( vCoords, fHeading, <<NEAR_SHOULDER_CAM_START_OFFSET_X, NEAR_SHOULDER_CAM_START_OFFSET_Y, NEAR_SHOULDER_CAM_START_OFFSET_Z>> ),
						<<NEAR_SHOULDER_CAM_START_ROT_OFFSET_X, NEAR_SHOULDER_CAM_START_ROT_OFFSET_Y, fHeading + NEAR_SHOULDER_CAM_START_ROT_OFFSET_Z>>, 
						SHOULDER_CAM_FOV, 
						TRUE), 
						SHOULDER_CAM_DURATION)
				
				// end cam node
				ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
					CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
						//GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<NEAR_SHOULDER_CAM_END_OFFSET_X, NEAR_SHOULDER_CAM_END_OFFSET_Y, NEAR_SHOULDER_CAM_END_OFFSET_Z>>), 
						GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS ( vCoords, fHeading, <<NEAR_SHOULDER_CAM_END_OFFSET_X, NEAR_SHOULDER_CAM_END_OFFSET_Y, NEAR_SHOULDER_CAM_END_OFFSET_Z>> ),
						<<NEAR_SHOULDER_CAM_END_ROT_OFFSET_X, NEAR_SHOULDER_CAM_END_ROT_OFFSET_Y, fHeading + NEAR_SHOULDER_CAM_END_ROT_OFFSET_Z>>, 
						SHOULDER_CAM_FOV, 
						TRUE), 
						SHOULDER_CAM_DURATION)					
				
				SET_CAM_SPLINE_NODE_EASE(MPGlobalsInteractions.SyncInteractionCam, 1, CAM_SPLINE_NODE_EASE_OUT)
				
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - set up camera behind shoulder (near)")
			
			ENDIF
			
			SET_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)										
		
			
			// go to next stage
			RESET_SYNCED_INTERACTION_TIMER()
			MPGlobalsInteractions.iSyncedInteractionState++	
			
			HIDE_PERSONAL_VEHICLES(FALSE)
					
		BREAK
		
		// wait until shoulder cam is finished
		CASE 7

			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)	
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	

			IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				SetOtherPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)
				SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID()) 
			ELSE
				SetAllPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)	
			ENDIF

			BOOL bShouldProceed 
			
			bShouldProceed = FALSE
			IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				bShouldProceed = TRUE
			ELSE
				IF NOT IS_CAM_INTERPOLATING(MPGlobalsInteractions.SyncInteractionCam)
					bShouldProceed = TRUE
				ENDIF
			ENDIF
			
		
			
			IF (bShouldProceed)
												
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()	
				AND DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)	
					DESTROY_CAM(MPGlobalsInteractions.SyncInteractionCam)						
				ELSE
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()	
				ENDIF
								
				MPGlobalsInteractions.iSyncedInteractionState = 99	
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - shoulder cam is finished." )
					
			ELSE
			
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() // this command should be in next_gen only			
			
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for shoulder cam to finish.", GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) )
			ENDIF
		BREAK
		
		// make sure everyon is loaded and ready to start the sync scene
		CASE 20
			
			KeepMeInCorrectPositionForSyncScene()
		
			IF HAS_FULL_SYNC_SCENE_LOADED_OBJECTS(GlobalplayerBD_Interactions[iPlayer].iSceneID)
				IF NOT HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
					GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].bReadyToStart = TRUE
					RESET_SYNCED_INTERACTION_TIMER()
					NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(TRUE)
					MPGlobalsInteractions.iSyncedInteractionState++		
				ELSE
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for NOT HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR 2.")
				ENDIF	
			ELSE
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - loading full sync scene objects")
			ENDIF	
		BREAK
		
		// start
		CASE 21
			
			KeepMeInCorrectPositionForSyncScene()
				
			IF AreAllPlayersInHashListWithUniqueIDReady(GlobalplayerBD_Interactions[iPlayer].iUniqueID, GlobalplayerBD_Interactions[iPlayer].iPlayerNamesHash)
			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 25000)						

				// hide any doors that the sync scene will use	
				HIDE_WORLD_OBJECTS_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID)

				// only actor 0 starts the scene
				IF (GlobalServerBD_SyncedInteractions.iActor[iPlayer]  = 0)										
				#IF IS_DEBUG_BUILD
				OR (GlobalplayerBD_Interactions[iPlayer].iForceActor > -1)
				#ENDIF	

					// start playing the full sync scene
					GlobalplayerBD_Interactions[iPlayer].iFullSyncSceneID  = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_SYNCED_INTERACTION_SCENE_COORDS(GlobalplayerBD_Interactions[iPlayer].iSceneID, 0), <<0.0, 0.0, GET_SYNC_INTERACTION_HEADING(GlobalplayerBD_Interactions[iPlayer].iSceneID, 0)>>)
																								
					// add players to sync scene
					INT i
					INT iCount
					PLAYER_INDEX PlayerID
					REPEAT NUM_NETWORK_PLAYERS i
						PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
						IF IS_NET_PLAYER_OK(PlayerID, TRUE, FALSE)
							IF (GlobalServerBD_SyncedInteractions.iUniqueID[iPlayer] = GlobalServerBD_SyncedInteractions.iUniqueID[i])
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(GET_PLAYER_PED(PlayerID), GlobalplayerBD_Interactions[iPlayer].iFullSyncSceneID, GET_ANIM_DICT_FOR_SYNCED_INTERACTION(GlobalplayerBD_Interactions[iPlayer].iSceneID, 0), GET_ANIM_NAME_FOR_ACTOR_IN_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID, 0, GlobalServerBD_SyncedInteractions.iActor[i]),INSTANT_BLEND_IN,REALLY_SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_SET_PED_OUT_OF_VEHICLE_AT_START|SYNCED_SCENE_USE_PHYSICS)
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - adding to sync scene player ", i)
								
								GlobalplayerBD_Interactions[iPlayer].iPlayerInSyncSceneHash[iCount] = NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID)
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - GlobalplayerBD_Interactions[", iPlayer, "].iPlayerInSyncSceneHash[", iCount, "] = ", GlobalplayerBD_Interactions[iPlayer].iPlayerInSyncSceneHash[iCount])
								
								iCount++
								
								
							ELSE
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - not adding to sync scene, not matching unique id - player ", i, ", GlobalServerBD_SyncedInteractions.iUniqueID[i] = ", GlobalServerBD_SyncedInteractions.iUniqueID[i])	
							ENDIF
						ELSE
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - not adding to sync scene, not ok - player ", i)	
						ENDIF
					ENDREPEAT
					
					// add object(s)
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - objects to add to full sync scene ", GET_NUMBER_OF_OBJECTS_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID))
					INT iObject
					IF (GET_NUMBER_OF_OBJECTS_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID) > 0)								
						REPEAT GET_NUMBER_OF_OBJECTS_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID) iObject							
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - trying to add object to full sync scene")
							IF NOT MPGlobalsInteractions.PlayerInteraction.bCreateLocalProps
								IF CREATE_NET_OBJ(GlobalplayerBD_Interactions[iPlayer].iSyncSceneObjectID[iObject], GET_OBJECT_MODEL_NAME_FOR_FULL_SYNC_SCENE_OBJECT(GlobalplayerBD_Interactions[iPlayer].iSceneID, iObject), GET_OBJECT_COORDS_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID, iObject), NETWORK_IS_HOST_OF_THIS_SCRIPT(), TRUE, TRUE)
									MPGlobalsInteractions.FullSyncSceneObjectID[iObject] = NET_TO_OBJ(GlobalplayerBD_Interactions[iPlayer].iSyncSceneObjectID[iObject])
									//MPGlobalsInteractions.FullSyncSceneObjectID[iObject] = CREATE_OBJECT(GET_OBJECT_MODEL_NAME_FOR_FULL_SYNC_SCENE_OBJECT(GlobalplayerBD_Interactions[iPlayer].iSceneID, iObject), GET_OBJECT_COORDS_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID, iObject), TRUE, NETWORK_IS_HOST_OF_THIS_SCRIPT(), TRUE)											
									SET_ENTITY_HEADING(MPGlobalsInteractions.FullSyncSceneObjectID[iObject], GET_OBJECT_HEADING_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID, iObject))
									NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(MPGlobalsInteractions.FullSyncSceneObjectID[iObject], GlobalplayerBD_Interactions[iPlayer].iFullSyncSceneID, GET_ANIM_DICT_FOR_SYNCED_INTERACTION(GlobalplayerBD_Interactions[iPlayer].iSceneID, 0), GET_OBJECT_ANIM_NAME_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID, iObject), INSTANT_BLEND_IN, INSTANT_BLEND_OUT) 
									SET_ENTITY_VISIBLE(MPGlobalsInteractions.FullSyncSceneObjectID[iObject], TRUE)
									SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(GlobalplayerBD_Interactions[iPlayer].iSyncSceneObjectID[iObject], TRUE, FALSE)
									PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - added object to full sync scene")	
								ELSE
									PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - could not create object.")
								ENDIF
							ELSE
								MPGlobalsInteractions.FullSyncSceneObjectID[iObject] = CREATE_OBJECT(GET_OBJECT_MODEL_NAME_FOR_FULL_SYNC_SCENE_OBJECT(GlobalplayerBD_Interactions[iPlayer].iSceneID, iObject), GET_OBJECT_COORDS_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID, iObject), FALSE, FALSE, TRUE)
								SET_ENTITY_HEADING(MPGlobalsInteractions.FullSyncSceneObjectID[iObject], GET_OBJECT_HEADING_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID, iObject))
								NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(MPGlobalsInteractions.FullSyncSceneObjectID[iObject], GlobalplayerBD_Interactions[iPlayer].iFullSyncSceneID, GET_ANIM_DICT_FOR_SYNCED_INTERACTION(GlobalplayerBD_Interactions[iPlayer].iSceneID, 0), GET_OBJECT_ANIM_NAME_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID, iObject), INSTANT_BLEND_IN, INSTANT_BLEND_OUT) 
								SET_ENTITY_VISIBLE(MPGlobalsInteractions.FullSyncSceneObjectID[iObject], TRUE)
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - added object to full sync scene")	
							ENDIF
						ENDREPEAT
					ENDIF
					
					// add camera
					NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(GlobalplayerBD_Interactions[iPlayer].iFullSyncSceneID,GET_ANIM_DICT_FOR_SYNCED_INTERACTION(GlobalplayerBD_Interactions[iPlayer].iSceneID, 0),GET_CAMERA_ANIM_NAME_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID))
													
					NETWORK_START_SYNCHRONISED_SCENE(GlobalplayerBD_Interactions[iPlayer].iFullSyncSceneID)

					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())

					MPGlobalsInteractions.iSyncSceneHost = iPlayer
					RESET_SYNCED_INTERACTION_TIMER()
					MPGlobalsInteractions.iSyncedInteractionState++

					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - starting full sync scene. iSyncSceneHost = ", MPGlobalsInteractions.iSyncSceneHost)										
					
				ELSE

					// store sync scene leader
					INT i
					REPEAT NUM_NETWORK_PLAYERS i
						IF (GlobalServerBD_SyncedInteractions.iUniqueID[iPlayer] = GlobalServerBD_SyncedInteractions.iUniqueID[i])
							IF (GlobalServerBD_SyncedInteractions.iActor[i] = 0)
								MPGlobalsInteractions.iSyncSceneHost = i
							ENDIF
						ENDIF
					ENDREPEAT
					
					RESET_SYNCED_INTERACTION_TIMER()
					MPGlobalsInteractions.iSyncedInteractionState++
					
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - will be in someone's full sync scene. iSyncSceneHost = ", MPGlobalsInteractions.iSyncSceneHost)
				ENDIF
				
				CLEAR_ROOM_FOR_ENTITY(PLAYER_PED_ID()) // make sure we dont consider the player as being inside the interior. 2211976
					

			ELSE
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - full sync scene waiting for all players  to be ready - time = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime))											
			ENDIF
	
		BREAK
		
		// wait for sync scene to start
		CASE 22
			IF IS_FULL_SYNC_SCENE_RUNNING(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iFullSyncSceneID)
			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 10000)
				
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for full sync scene successfully started. Going to check i have been added.")				
				MPGlobalsInteractions.iSyncedInteractionState++
				RESET_SYNCED_INTERACTION_TIMER()	
			
				CLEAR_ROOM_FOR_ENTITY(PLAYER_PED_ID())
			
			ELSE
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for full sync scene to start.", GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime), " iSyncSceneHost = ", MPGlobalsInteractions.iSyncSceneHost )		
				KeepMeInCorrectPositionForSyncScene()
				
			ENDIF
		BREAK
		
		// was i actually added to the scene
		CASE 23

			IF IS_FULL_SYNC_SCENE_RUNNING(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iFullSyncSceneID) 
			AND DID_HOST_ADD_ME_TO_THE_SYNC_SCENE(MPGlobalsInteractions.iSyncSceneHost)
			
			#IF IS_DEBUG_BUILD
			AND NOT (MPGlobalsInteractions.bTestBail3)
			#ENDIF	
			
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - host did add me to sync scene. ")
			
				// need to make doors visible
				INT i
				//REPEAT COUNT_OF(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iSyncSceneObjectID) i
				REPEAT GET_NUMBER_OF_OBJECTS_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID) i
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iSyncSceneObjectID[i])
						IF DOES_ENTITY_EXIST(NET_TO_OBJ(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iSyncSceneObjectID[i]))
							SET_ENTITY_VISIBLE_IN_CUTSCENE(NET_TO_OBJ(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iSyncSceneObjectID[i]), TRUE, TRUE)
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - door set to be visible")	
						ENDIF
					ELSE
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - door net id does not exist")	
					ENDIF
				ENDREPEAT	
				
				// store if we actually started the sync scene or if we timed out
				MPGlobalsInteractions.bSyncSceneSuccessfullyStarted = IS_FULL_SYNC_SCENE_RUNNING(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iFullSyncSceneID)				
			
				RESTORE_AUDIO_FOR_POST_MISSION_CUTSCENE()
						
				CLEAR_ROOM_FOR_ENTITY(PLAYER_PED_ID())
				NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(FALSE)
				MPGlobalsInteractions.iSyncedInteractionState++			
			
			ELSE
				
				KeepMeInCorrectPositionForSyncScene()
							
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 2000)
				#IF IS_DEBUG_BUILD
				OR (MPGlobalsInteractions.bTestBail3)
				#ENDIF	
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - host didn't add me to sync scene. bailing.")										
					GlobalplayerBD_Interactions[NATIVE_TO_INT(PLAYER_ID())].bReadyToStart = TRUE
					NETWORK_ALLOW_REMOTE_SYNCED_SCENE_LOCAL_PLAYER_REQUESTS(FALSE)
					GOTO_BAILED_SYNCED_INTERACTION_STATE()
				ELSE
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting to check host actually added me to scene. ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) )	
				ENDIF
			ENDIF
		BREAK
		
		// play sync scene			
		CASE 24
			// fade screen in
			IF NOT DOES_SYNCED_INTERACTION_HAVE_PRE_CUTSCENE()
				IF NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					BUSYSPINNER_OFF()
					DO_SCREEN_FADE_IN(1000)
				ENDIF	
			ELSE
				IF GET_SKYFREEZE_STAGE() = SKYFREEZE_FROZEN
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT restoring renderphases. full sync scene")
					BUSYSPINNER_OFF()
					TOGGLE_RENDERPHASES(TRUE)
					SET_SKYBLUR_CLEAR()
				ENDIF			
			ENDIF
		
			SetAllPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)
			HIDE_HUD_FOR_SYNCED_INTERACTIONS(GlobalplayerBD_Interactions[iPlayer].iSceneID)
		
			IF NOT IS_FULL_SYNC_SCENE_RUNNING(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iFullSyncSceneID)
			
				// reveal doors
				REVEAL_WORLD_OBJECTS_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID)
				
				INT iObject
				NETWORK_INDEX NetID
				REPEAT GET_NUMBER_OF_OBJECTS_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID) iObject				
					IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iSyncSceneObjectID[iObject])
						IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_ENT(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iSyncSceneObjectID[iObject]))
							NetID = GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iSyncSceneObjectID[iObject]
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - deleting sync scene network object")														
							DELETE_NET_ID(NetID)	
						ELSE
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - eleting sync scene network object - doesn't have control. ")
						ENDIF
					ELSE					
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - deleting sync scene network object - doesnt exist")	
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_OBJECT_MODEL_NAME_FOR_FULL_SYNC_SCENE_OBJECT(GlobalplayerBD_Interactions[iPlayer].iSceneID, iObject))
				ENDREPEAT
				
				IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
				AND NOT IS_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam)
					vFinalRenderCoords = GET_FINAL_RENDERED_CAM_COORD()
					vFinalRenderRot = GET_FINAL_RENDERED_CAM_ROT()
					fFinalRenderFOV = GET_FINAL_RENDERED_CAM_FOV()
					
					SET_CAM_PARAMS(MPGlobalsInteractions.SyncInteractionCam, vFinalRenderCoords, vFinalRenderRot, fFinalRenderFOV)
					
					SET_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)	
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - SyncInteractionCam not active so setting  = ", fSyncPhase)		
				ENDIF
														
				//kill cam if it exists
				IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)	
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)	
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
					DISPLAY_RADAR(true)
					//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE)
					//DESTROY_CAM(MPGlobalsInteractions.SyncInteractionCam)	// don't kill this frame, as the catch up needs it. 2221267						
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - full sync scene - cleaned up camera." )
				ENDIF																
															
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()	
				AND DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
					DESTROY_CAM(MPGlobalsInteractions.SyncInteractionCam)	
				ENDIF
				
				// make sure ped is not considered inside an interior 2208645
				CLEAR_ROOM_FOR_ENTITY(PLAYER_PED_ID())
				
				// if we never successfully started the sync scene then move to safe end position.
				IF NOT (MPGlobalsInteractions.bSyncSceneSuccessfullyStarted)
					GET_SAFE_END_POSITION_FOR_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID, GlobalServerBD_SyncedInteractions.iActor[iPlayer], vCoords, fHeading)
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - never started sync scene properly so setting to end coords of ", vCoords)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vCoords)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
				ENDIF
			
				// unload anims
				REMOVE_ANIM_DICT(GET_ANIM_DICT_FOR_SYNCED_INTERACTION_VARIATION(gSyncedInteractionScene[GlobalplayerBD_Interactions[iPlayer].iSceneID].iLayout[0]))
			
				MPGlobalsInteractions.iSyncedInteractionState++	
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - full sync scene is finished." )
				
			ELSE
					
				CLEAR_ROOM_FOR_ENTITY(PLAYER_PED_ID())
				
				// create a scripted cam for 2nd player?
				IF NOT (MPGlobalsInteractions.iSyncSceneHost = iPlayer)
				AND	SHOULD_USE_SCRIPTED_CAM_DURING_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID)
				
					IF NOT DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
						
						iThisLocalsync = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iFullSyncSceneID)	
							
						IF (iThisLocalsync != -1)
							IF GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalsync) >= PHASE_TIME_TO_USE_SCRIPTED_CAM_DURING_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID)										
								
								MPGlobalsInteractions.SyncInteractionCam = CREATE_CAM("DEFAULT_SPLINE_CAMERA")							
								
								INTERACTION_CAMERA_DETAILS CamDetails
								GET_DETAILS_FOR_SCRIPTED_CAM_DURING_FULL_SYNC_SCENE(GlobalplayerBD_Interactions[iPlayer].iSceneID, CamDetails)
								
								// start cam node
								ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
									CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
										CamDetails.vStartCoords, 
										CamDetails.vStartRot, 
										CamDetails.fStartFov, 
										TRUE), 
										CamDetails.iDuration)
								// end cam node
								ADD_CAM_SPLINE_NODE_USING_CAMERA(MPGlobalsInteractions.SyncInteractionCam, 
									CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", 
										CamDetails.vEndCoords, 
										CamDetails.vEndRot, 
										CamDetails.fEndFov, 
										TRUE), 
										CamDetails.iDuration)
								
								SET_CAM_SPLINE_NODE_EASE(MPGlobalsInteractions.SyncInteractionCam, 1, CAM_SPLINE_NODE_EASE_OUT)
										
								SET_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)	
								
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - started scripted cam for full sync scene.")
								
							else
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting to start scripted cam for full sync scene, phase =", GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalsync))									
							endif
						ELSE
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - full sync scene - iThisLocalsync = ", iThisLocalsync)	
						ENDIF
					
					ELSE
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - started scripted exists for full sync scene.")	
					ENDIF
				
				ELSE
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting for full sync scene to finish." )
										
					iThisLocalsync = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(GlobalplayerBD_Interactions[MPGlobalsInteractions.iSyncSceneHost].iFullSyncSceneID)
					IF (iThisLocalsync != -1)
						fSyncPhase = GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalsync)
					ELSE
						fSyncPhase = 0.0
					ENDIF
					
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - fSyncPhase = ", fSyncPhase)	
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - iThisLocalsync = ", iThisLocalsync)
					
					IF NOT DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
						MPGlobalsInteractions.SyncInteractionCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - created SyncIntctionCamera ")
					ELSE
						IF IS_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam)
						AND (fSyncPhase < 0.9)
							SET_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam, FALSE)
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - cam was active, setting to false.")	
						ELSE
							PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - SyncInteractionCam is active ")
						ENDIF
					ENDIF
					
					vFinalRenderCoords = GET_FINAL_RENDERED_CAM_COORD()
					vFinalRenderRot = GET_FINAL_RENDERED_CAM_ROT()
					fFinalRenderFOV = GET_FINAL_RENDERED_CAM_FOV()
					
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - vFinalRenderCoords = ", vFinalRenderCoords)
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - vFinalRenderRot = ", vFinalRenderRot)
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - fFinalRenderFOV = ", fFinalRenderFOV)
					
					SET_CAM_PARAMS(MPGlobalsInteractions.SyncInteractionCam, vFinalRenderCoords, vFinalRenderRot, fFinalRenderFOV)
					
					
					IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
					AND NOT IS_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam)
																									
						IF (iThisLocalsync != -1)
							IF (fSyncPhase >= 0.9)						
								SET_CAM_ACTIVE(MPGlobalsInteractions.SyncInteractionCam, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)	
								PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - setting  = ", fSyncPhase)		
							ENDIF	
						ENDIF
					ELSE
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - MPGlobalsInteractions.SyncInteractionCam doesn't exist or not active." )	
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		
		CASE 25
			SetAllPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)
			PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - calling STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP." )	
			STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE)
			MPGlobalsInteractions.iSyncedInteractionState = 99	
		BREAK

		// cleanup any currently playing interactions
		CASE 50
			IF (MPGlobalsInteractions.bAnyInteractionIsPlaying)
			AND NOT (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 3000)
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - 50 - waiting for current interaction to cleanup...")
			ELSE
				IF NOT (MPGlobalsInteractions.PlayerInteraction.bCleanupInteractionAnimImmediately)
					PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - 50 - no interaction currently playing, warp to location..")										
					LOAD_RANDOM_RE_ENTRY_INTERACTION_ANIM()	
					MPGlobalsInteractions.iSyncedInteractionState++		
				ELSE
					IF (MPGlobalsInteractions.PlayerInteraction.bHasCleanedUp)
						MPGlobalsInteractions.PlayerInteraction.bCleanupInteractionAnimImmediately = FALSE
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - 50 - has cleaned up. clearing flag.")
					ELSE
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - 50 - waiting for lingering interactions to cleanup.")	
					ENDIF
				ENDIF
			ENDIF
		BREAK

		// warp to near current location		
		CASE 51
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_NEAR_CURRENT_POSITION, FALSE)
				START_RANDOM_RE_ENTRY_IDLE_ANIM(0.5)	
				RESET_SYNCED_INTERACTION_TIMER()
				MPGlobalsInteractions.iSyncedInteractionState++
				RESTORE_AUDIO_FOR_POST_MISSION_CUTSCENE()
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - calling WARP_TO_SPAWN_LOCATION")
			ELSE
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting on WARP_TO_SPAWN_LOCATION")				
			ENDIF
		BREAK
		// make sure anim is actually playing
		CASE 52
			IF (IsAnimAtDesiredStartPhase(gInteractionsPlayerData.PlayingInteraction) 
			AND(GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > MIN_WAIT_TIME_FOR_ANIM_TO_START)) // make sure we don't see any pop or blend in to anim.
			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 2000)

				MPGlobalsInteractions.iSyncedInteractionState++
				
				// fade screen in
				IF NOT DOES_SYNCED_INTERACTION_HAVE_PRE_CUTSCENE()
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						BUSYSPINNER_OFF()
						DO_SCREEN_FADE_IN(500)
						SET_PLAYER_VISIBLE_LOCALLY(PLAYER_ID())
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT bail - fade in.")
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
				ELSE
					IF GET_SKYFREEZE_STAGE() = SKYFREEZE_FROZEN
						PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT restoring renderphases.")
						BUSYSPINNER_OFF()
						TOGGLE_RENDERPHASES(TRUE)
						SET_SKYBLUR_CLEAR()
					ENDIF
				ENDIF
						
			ENDIF
		BREAK
		// make sure we are fully faded in before cleaning up
		CASE 53
			SET_PLAYER_VISIBLE_LOCALLY(PLAYER_ID())
			IF IS_SCREEN_FADED_IN()
				CLEANUP_SYNCED_INTERACTIONS_SETTINGS()									
				CLEAR_PLAYER_SYNCED_INTERACTION_DATA()
			ENDIF
		BREAK
		
		
		// cleanup
		CASE 99
		
			CLEANUP_SYNCED_INTERACTIONS_SETTINGS()		
								
			RESET_SYNCED_INTERACTION_TIMER()
			MPGlobalsInteractions.iSyncedInteractionState++
			PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - cleanup started")
			
			IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				SetOtherPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)
				SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID()) 
			ELSE
				SetAllPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)	
			ENDIF			
			
			IF DOES_CAM_EXIST(MPGlobalsInteractions.SyncInteractionCam)
				DESTROY_CAM(MPGlobalsInteractions.SyncInteractionCam)
			ENDIF
			
		BREAK
		
		// keep setting all players in scene visible for a second after the scene has finished to avoid any pop (as the visibility update from client takes a while to get received)
		CASE 100
	
			
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), MPGlobalsInteractions.SyncedInteractionStartTime) > 2000)
			
				#IF IS_DEBUG_BUILD
				IF (MPGlobalsInteractions.bDebug_FillSceneWithPeds)
					INT i
					REPEAT COUNT_OF(MPGlobalsInteractions.DebugPedID) i
						IF DOES_ENTITY_EXIST(MPGlobalsInteractions.DebugPedID[i])
							DELETE_PED(MPGlobalsInteractions.DebugPedID[i])
						ENDIF
					ENDREPEAT
				ENDIF
				#ENDIF			
			
				CLEAR_PLAYER_SYNCED_INTERACTION_DATA()
			ELSE
				PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - cleanup waiting to finish...")
			ENDIF

			IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				SetOtherPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)
				SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID()) 
			ELSE
				SetAllPlayersInSceneVisibleLocally(GlobalplayerBD_Interactions[iPlayer].iUniqueID)	
			ENDIF			
			
		BREAK
		
	ENDSWITCH
	
	// don't delete the custom car nodes straight away, give the pv time to be created.
	IF (MPGlobalsInteractions.bClearCustomCarNodes)
		IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobalsInteractions.timeClearCustomCarNodes)) > 10000)
			CLEAR_CUSTOM_VEHICLE_NODES()
			MPGlobalsInteractions.bClearCustomCarNodes = FALSE
		ELSE
			PRINTLN("[interactions] UPDATE_SYNCED_INTERACTIONS_CLIENT - waiting to cleanup custom vehicle nodes.")	
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
	IF (MPGlobalsInteractions.bDebug_FillSceneWithPeds)
	AND NOT (MPGlobalsInteractions.bUseClonePlayers)	
		INT i
		REPEAT COUNT_OF(MPGlobalsInteractions.DebugPedID) i
			IF DOES_ENTITY_EXIST(MPGlobalsInteractions.DebugPedID[i])
				UPDATE_INTERACTION_ANIM_FOR_PED(MPGlobalsInteractions.DebugPedID[i], i, gInteractionsPedsData[i])	
			ENDIF
		ENDREPEAT
	ENDIF
	#ENDIF
	IF (MPGlobalsInteractions.bUseClonePlayers)	
		INT i
		REPEAT COUNT_OF(MPGlobalsInteractions.ClonePedID) i
			IF DOES_ENTITY_EXIST(MPGlobalsInteractions.ClonePedID[i])
				UPDATE_INTERACTION_ANIM_FOR_PED(MPGlobalsInteractions.ClonePedID[i], i, gInteractionsPedsData[i])	
			ENDIF
		ENDREPEAT			
	ENDIF
	
ENDPROC

FUNC INT GET_LAST_NON_SMOKING_DRINKING_INTERACTION()
	RETURN GET_MAX_PLAYER_INTERACTIONS() - 8
ENDFUNC

FUNC BOOL IsEatingDrinkingOrSmokingPlayerInteraction(INT iAnim)
	IF IS_EATING_OR_DRINKING_INTERACTION(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), iAnim)
	OR (iAnim = ENUM_TO_INT(PLAYER_INTERACTION_SMOKE))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_DO_QUICKPLAY_RESTART()

	IF NOT IS_ANY_INTERACTION_ANIM_PLAYING()
		PRINTLN("[interactions] [Quickplay Restart] - CAN_DO_QUICKPLAY_RESTART - false IS_ANY_INTERACTION_ANIM_PLAYING = FALSE")
		RETURN FALSE
	ENDIF

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PRINTLN("[interactions] [Quickplay Restart] - CAN_DO_QUICKPLAY_RESTART - false 1")
		RETURN FALSE
	ENDIF
	
//	IF (MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionAnimFullBody)
//		PRINTLN("[interactions] [Quickplay Restart] - CAN_DO_QUICKPLAY_RESTART - false - doing full body")
//		RETURN FALSE
//	ENDIF
	
	INT iCurrentAnimType = GET_MP_PLAYER_ANIM_TYPE()	
	INT iCurrentAnim = GET_MP_PLAYER_ANIM_SETTING()
	
	IF (ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER) = iCurrentAnimType)
		IF iCurrentAnim = ENUM_TO_INT(PLAYER_INTERACTION_DRINK_3)
		#IF FEATURE_COPS_N_CROOKS
		OR iCurrentAnim = ENUM_TO_INT(PLAYER_INTERACTION_POUR_ONE_OUT)
		OR iCurrentAnim = ENUM_TO_INT(PLAYER_INTERACTION_EAT_DOUGHNUT)
		#ENDIF
			PRINTLN("[interactions] [Quickplay Restart] - CAN_DO_QUICKPLAY_RESTART - true 1, iCurrentAnim = ", iCurrentAnim)
			RETURN TRUE
		ENDIF
		
		IF (iCurrentAnim < 1)
		OR (iCurrentAnim > GET_LAST_NON_SMOKING_DRINKING_INTERACTION())
			PRINTLN("[interactions] [Quickplay Restart] - CAN_DO_QUICKPLAY_RESTART - false 2, iCurrentAnim = ", iCurrentAnim)
			RETURN FALSE
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF (ENUM_TO_INT(INTERACTION_ANIM_TYPE_CREW) = iCurrentAnimType)
		RETURN TRUE	
	ENDIF
	
	PRINTLN("[interactions] [Quickplay Restart] - CAN_DO_QUICKPLAY_RESTART - false 3")
	RETURN FALSE
ENDFUNC

FUNC INT TotalIncrementableAnim()
	RETURN (GET_LAST_NON_SMOKING_DRINKING_INTERACTION() - 1)
ENDFUNC

PROC IncrementAnim(INT& iAnim)
	iAnim++
	// skip past eating and drinking
	IF (iAnim > GET_LAST_NON_SMOKING_DRINKING_INTERACTION())
		iAnim = 1	
	ENDIF
ENDPROC
PROC DecrementAnim(INT& iAnim)
	iAnim--
	IF (iAnim < 1)
		iAnim = GET_LAST_NON_SMOKING_DRINKING_INTERACTION()
	ENDIF
ENDPROC

PROC GO_TO_NEXT_QUICKPLAY_ANIM_ON_FOOT()
	INT iCurrentAnim = GET_MP_PLAYER_ANIM_SETTING()
	INT iCurrentAnimType = GET_MP_PLAYER_ANIM_TYPE()	
	INT iIteration
	
	IF (ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER) = iCurrentAnimType)
		IncrementAnim(iCurrentAnim)
		iIteration = 0
		WHILE (IS_PLAYER_WEARING_PROBLEM_MASK_OR_HAT_FOR_INTERACTION(iCurrentAnimType, iCurrentAnim, FALSE, FALSE) AND iIteration < TotalIncrementableAnim())
			IncrementAnim(iCurrentAnim)
			iIteration++
		ENDWHILE		
		SET_MP_PLAYER_ANIM_SETTING(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), iCurrentAnim)
	ENDIF	
	
	IF (ENUM_TO_INT(INTERACTION_ANIM_TYPE_CREW) = iCurrentAnimType)
		SET_MP_PLAYER_ANIM_SETTING(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), 1)
	ENDIF
	
ENDPROC

PROC GO_TO_PREV_QUICKPLAY_ANIM_ON_FOOT()
	INT iCurrentAnim = GET_MP_PLAYER_ANIM_SETTING()
	INT iCurrentAnimType = GET_MP_PLAYER_ANIM_TYPE()
	INT iIteration
	
	IF (ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER) = iCurrentAnimType)
		DecrementAnim(iCurrentAnim)
		iIteration = 0
		WHILE (IS_PLAYER_WEARING_PROBLEM_MASK_OR_HAT_FOR_INTERACTION(iCurrentAnimType, iCurrentAnim, FALSE, FALSE) AND iIteration < TotalIncrementableAnim())
			DecrementAnim(iCurrentAnim)
			iIteration++
		ENDWHILE
		SET_MP_PLAYER_ANIM_SETTING(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), iCurrentAnim)
	ENDIF
	
	IF (ENUM_TO_INT(INTERACTION_ANIM_TYPE_CREW) = iCurrentAnimType)
		SET_MP_PLAYER_ANIM_SETTING(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), GET_LAST_NON_SMOKING_DRINKING_INTERACTION())
	ENDIF
	
ENDPROC

PROC CHECK_FOR_QUICKPLAY_START()
	MPGlobalsInteractions.sQuickStartControlTimer.action = INPUT_SPECIAL_ABILITY
	MPGlobalsInteractions.sQuickStartControlTimer.control = PLAYER_CONTROL
	
	#IF FEATURE_COPS_N_CROOKS
	IF IS_FREEMODE_ARCADE() AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
		// skip
	ELSE
	#ENDIF
	
		bSecondTap = TRUE
		
	#IF FEATURE_COPS_N_CROOKS
	ENDIF
	#ENDIF
	
	IF NOT (MPGlobalsInteractions.bQuickplayButtonPressed)			
		IF NOT (MPGlobalsInteractions.bPointingButtonPressed) // mutually exclusive

			IF (IS_QUICKPLAY_ANIM_BUTTON_PRESSED())
				PRINTLN("[interactions] [Quickplay] - pressed")
				
				#IF FEATURE_COPS_N_CROOKS
				IF IS_FREEMODE_ARCADE() AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
					RESET_NET_TIMER(sDoubleTapReleaseTimer)
					IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
					OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
					OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
						bSecondTap = FALSE
					ELSE
						INT iDiff = GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.timeQuickplayPress)
						PRINTLN("[interactions] [Quickplay] - double tap diff = ", iDiff)
						
						IF (iDiff < DOUBLE_TAP_INTERVAL)
							bSecondTap = TRUE
						ENDIF
					ENDIF	
					
					MPGlobalsInteractions.timeQuickplayPress = GET_THIS_TIMER()
				ELSE
				#ENDIF
				
					// is this a double tap?
					INT iDiff = GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.timeQuickplayPress)
					PRINTLN("[interactions] [Quickplay] - double tap diff = ", iDiff)
					IF (iDiff < DOUBLE_TAP_INTERVAL)
						MPGlobalsInteractions.bQuickplayDoubleTap = TRUE
					ENDIF
					MPGlobalsInteractions.timeQuickplayPress = GET_THIS_TIMER()
					
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						MPGlobalsInteractions.QuickplayInteractionVeh.bPlayInteractionAnim = TRUE	
					ELSE
						MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionAnim = TRUE	
						IF (MPGlobalsInteractions.bQuickplayDoubleTap)
						AND NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE() 						
							MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionAnimFullBody=TRUE
						ENDIF
					ENDIF
					MPGlobalsInteractions.bQuickplayButtonPressed = TRUE										
					MPGlobalsInteractions.bQuickRestartAnim = FALSE
					
					IF IS_CELLPHONE_CAMERA_IN_SELFIE_MODE() 
						MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionSelfie = TRUE
					ENDIF
					
				#IF FEATURE_COPS_N_CROOKS
				ENDIF
				#ENDIF
				
			ELSE
				#IF FEATURE_COPS_N_CROOKS
				IF IS_FREEMODE_ARCADE() AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
					IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
					OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
					OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
						bSecondTap = FALSE
					ENDIF
					
					IF bSecondTap 
						IF IS_CONTROL_HELD(MPGlobalsInteractions.sQuickStartControlTimer, TRUE, DOUBLE_TAP_INTERVAL)
							MPGlobalsInteractions.bQuickplayDoubleTap = TRUE
							PRINTLN("[interactions] [Quickplay] - MPGlobalsInteractions.bQuickplayDoubleTap = TRUE")
						

							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								MPGlobalsInteractions.QuickplayInteractionVeh.bPlayInteractionAnim = TRUE	
							ELSE
								MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionAnim = TRUE	
								IF (MPGlobalsInteractions.bQuickplayDoubleTap)
								AND NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE() 						
									MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionAnimFullBody=TRUE
								ENDIF
							ENDIF
							MPGlobalsInteractions.bQuickplayButtonPressed = TRUE										
							MPGlobalsInteractions.bQuickRestartAnim = FALSE
							
							IF IS_CELLPHONE_CAMERA_IN_SELFIE_MODE() 
								MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionSelfie = TRUE
							ENDIF
							
							bSecondTap = FALSE
							RESET_NET_TIMER(MPGlobalsInteractions.sQuickStartControlTimer.sTimer)
						ELIF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
							IF NOT HAS_NET_TIMER_STARTED(sDoubleTapReleaseTimer)
								START_NET_TIMER(sDoubleTapReleaseTimer)
							ELSE
								IF HAS_NET_TIMER_EXPIRED(sDoubleTapReleaseTimer, 200)
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										MPGlobalsInteractions.QuickplayInteractionVeh.bPlayInteractionAnim = TRUE	
									ELSE
										MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionAnim = TRUE	
										IF (MPGlobalsInteractions.bQuickplayDoubleTap)
										AND NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE() 						
											MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionAnimFullBody=TRUE
										ENDIF
									ENDIF
									MPGlobalsInteractions.bQuickplayButtonPressed = TRUE										
									MPGlobalsInteractions.bQuickRestartAnim = FALSE
									
									IF IS_CELLPHONE_CAMERA_IN_SELFIE_MODE() 
										MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionSelfie = TRUE
									ENDIF
									RESET_NET_TIMER(sDoubleTapReleaseTimer)
									RESET_NET_TIMER(MPGlobalsInteractions.sQuickStartControlTimer.sTimer)
									bSecondTap = FALSE
									PRINTLN("[interactions] [Quickplay] - bSecondTap = FALSE")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				#ENDIF
				
			ENDIF	
			
			
		ENDIF
	ELSE	
		IF (NOT IS_QUICKPLAY_ANIM_BUTTON_PRESSED())
		OR (MPGlobalsInteractions.bPointingButtonPressed)
			MPGlobalsInteractions.bQuickplayButtonPressed = FALSE	
			IF (MPGlobalsInteractions.bQuickplayDoubleTap)
				MPGlobalsInteractions.bQuickplayDoubleTap = FALSE
				PRINTLN("[interactions] [Quickplay Restart] - MPGlobalsInteractions.bQuickplayDoubleTap = FALSE 1")
			ENDIF
		ELSE
			IF NOT (MPGlobalsInteractions.bQuickRestartAnim)
				IF (IS_QUICKPLAY_ANIM_BUTTON_PRESSED() AND bSecondTap)
					IF CAN_DO_QUICKPLAY_RESTART()						

						BOOL bTriggerQuickplayRestart = FALSE

						IF IS_QUICKPLAY_R1_PRESSED()
							GO_TO_NEXT_QUICKPLAY_ANIM_ON_FOOT()
							bTriggerQuickplayRestart = TRUE
							PRINTLN("[interactions] [Quickplay Restart] - R1")
						ENDIF
						IF IS_QUICKPLAY_L1_PRESSED()
							GO_TO_PREV_QUICKPLAY_ANIM_ON_FOOT()
							bTriggerQuickplayRestart = TRUE
							PRINTLN("[interactions] [Quickplay Restart] - L1")
						ENDIF
						IF (MPGlobalsInteractions.bQuickplayDoubleTap)
							IF ARE_CONDITIONS_MET_TO_START_INTERACTION_TYPE(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER))
								bTriggerQuickplayRestart = TRUE
								PRINTLN("[interactions] [Quickplay Restart] - double tap")
							ELSE
								bTriggerQuickplayRestart = FALSE
								PRINTLN("[interactions] [Quickplay Restart] - cannot do double tap at this time.")
							ENDIF
						ENDIF
						
						IF (bTriggerQuickplayRestart)
							MPGlobalsInteractions.bQuickRestartAnim = TRUE			
							MPGlobalsInteractions.QuickplayInteraction.bCleanupInteractionAnim = TRUE
							MPGlobalsInteractions.timeQuickplayPress = GET_TIME_OFFSET(GET_THIS_TIMER(),  -5000) // so it doesnt trigger a double tap						
						ENDIF
						
					ENDIF
				ENDIF			
			ENDIF
		ENDIF
		
		// if we are going to do a quick restart if the double tap is pressed make sure the timer is set accordingly
		IF (MPGlobalsInteractions.bQuickRestartAnim)
			IF (MPGlobalsInteractions.bQuickplayDoubleTap)
				MPGlobalsInteractions.timeQuickplayPress = GET_THIS_TIMER()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RESET_QUICKPLAY_TO_CURRENT()
	IF IS_ANY_INTERACTION_ANIM_PLAYING()
		PRINTLN("[interactions] [Quickplay Restart] - RESET_QUICKPLAY_TO_CURRENT ")
		INT iCurrentAnim
		INT iCurrentAnimType
		GET_CURRENTLY_PLAYING_INTERACTION(iCurrentAnimType, iCurrentAnim)	
		SET_MP_PLAYER_ANIM_SETTING(iCurrentAnimType, iCurrentAnim)
	ENDIF
ENDPROC




PROC UPDATE_INTERACTION_ANIMS()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("UPDATE_INTERACTION_ANIMS")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	////////////////////////////////////////////////manage split combo boxes for interaction anims//////////////////////////////////////////////////////	
	IF MPGlobalsInteractions.iDebug_SyncedSceneSceneID!=MPGlobalsInteractions.iDebug_SyncedSceneSceneID_previous
		MPGlobalsInteractions.iDebug_SyncedSceneSceneID2 = 0
		MPGlobalsInteractions.iDebug_SyncedSceneSceneID2_previous = 0
		MPGlobalsInteractions.iDebug_SyncedSceneSceneID_previous = MPGlobalsInteractions.iDebug_SyncedSceneSceneID
		MPGlobalsInteractions.iDebug_SyncedSceneSceneID_actual = MPGlobalsInteractions.iDebug_SyncedSceneSceneID - 1
	ELIF MPGlobalsInteractions.iDebug_SyncedSceneSceneID2!=MPGlobalsInteractions.iDebug_SyncedSceneSceneID2_previous
		MPGlobalsInteractions.iDebug_SyncedSceneSceneID = 0
		MPGlobalsInteractions.iDebug_SyncedSceneSceneID_previous = 0
		MPGlobalsInteractions.iDebug_SyncedSceneSceneID2_previous = MPGlobalsInteractions.iDebug_SyncedSceneSceneID2
		MPGlobalsInteractions.iDebug_SyncedSceneSceneID_actual = MPGlobalsInteractions.iDebug_SyncedSceneSceneID2 + MAX_SYNCED_INTERACTION_COMBO_BOX_ENTRIES -1
	ENDIF	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	#ENDIF
	
	// if player is moving store that time
	
	
	IF NOT IS_PED_STILL(PLAYER_PED_ID())
		MPGlobalsInteractions.timePlayerMoves = GET_THIS_TIMER()		
	ENDIF
	IF NOT IsPedInIdleStance(PLAYER_PED_ID())
		MPGlobalsInteractions.timePlayerNonIdle = GET_THIS_TIMER()
	ENDIF

	// ***********************************************************************************
	//								player menu interactions
	// ***********************************************************************************
	UPDATE_INTERACTION_ANIM_INTERNAL(gInteractionsPlayerData, MPGlobalsInteractions.PlayerInteraction, PLAYER_PED_ID(), FALSE)
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_INTERACTION_ANIM_INTERNAL - gInteractionsPlayerData")
	#ENDIF
	#ENDIF	
	
	
	// ***********************************************************************************
	//							has quickplay button been pressed?
	// ***********************************************************************************
	IF (MPGlobalsInteractions.bQuickRestartAnim)
	OR (MPGlobalsInteractions.bSimulate_QuickplayButtonPressed)
		PRINTLN("[interactions] [Quickplay Restart] - disabling controls")
		DisableControlsForInteraction(PLAYER_PED_ID(), FALSE )
	ENDIF
	
	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
	

		CHECK_FOR_QUICKPLAY_START()
		
		
		IF (MPGlobalsInteractions.bQuickRestartAnim)
			IF (MPGlobalsInteractions.bQuickplayButtonPressed)
				IF NOT IS_ANY_INTERACTION_ANIM_PLAYING()
					MPGlobalsInteractions.bQuickplayButtonPressed = FALSE		
					MPGlobalsInteractions.bQuickRestartAnim = FALSE
					MPGlobalsInteractions.bSimulate_QuickplayButtonPressed = TRUE					
					MPGlobalsInteractions.bThisIsAQuickRestartAnim = TRUE
					IF (MPGlobalsInteractions.bQuickplayDoubleTap)
						MPGlobalsInteractions.bQuickplayDoubleTap = FALSE
						RESET_NET_TIMER(MPGlobalsInteractions.sQuickStartControlTimer.sTimer)
						PRINTLN("[interactions] [Quickplay Restart] - MPGlobalsInteractions.bQuickplayDoubleTap = FALSE 2")
					ENDIF
									
					PRINTLN("[interactions] [Quickplay Restart] - good to restart!")
					CHECK_FOR_QUICKPLAY_START()
				ELSE
					PRINTLN("[interactions] [Quickplay Restart] - waiting for previous anim to cleanup.")
					
					// load next anim in preparation
					MPGlobalsInteractions.bDoPreLoadQuickplay = TRUE
				ENDIF
			ELSE
				MPGlobalsInteractions.bQuickRestartAnim = FALSE
				PRINTLN("[interactions] [Quickplay Restart] - quickplay button is not pressed.")
				
				IF (MPGlobalsInteractions.bInteractionIsPlayingForQuickRestart)
					PRINTLN("[interactions] [Quickplay Restart] - quickplay button is not pressed, so clearing any interaction that was playing.")
					CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())					
					IF NOT IsFinishedSequenceTask(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF	
					MPGlobalsInteractions.bInteractionIsPlayingForQuickRestart = FALSE
				ENDIF
				
				IF GET_MP_PLAYER_ANIM_SETTING() != ENUM_TO_INT(PLAYER_INTERACTION_DRINK_3)
				AND GET_MP_PLAYER_ANIM_SETTING() != ENUM_TO_INT(PLAYER_INTERACTION_MAKE_IT_RAIN)
//				AND GET_MP_PLAYER_ANIM_SETTING() != ENUM_TO_INT(PLAYER_INTERACTION_POUR_ONE_OUT)
//				AND GET_MP_PLAYER_ANIM_SETTING() != ENUM_TO_INT(PLAYER_INTERACTION_EAT_DOUGHNUT)
					// we never actually go to play the anim, so set the quickplay back to the current
					RESET_QUICKPLAY_TO_CURRENT()
				ENDIF
			ENDIF
		ENDIF	
				
	ENDIF
	
	QuickplayRestartPreLoader()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER(" has quickplay button been pressed?")
	#ENDIF
	#ENDIF			
	
	// ***********************************************************************************
	//							has pointing button been pressed?
	// ***********************************************************************************
	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
		IF NOT (MPGlobalsInteractions.bPointingButtonPressed)	
			IF NOT (MPGlobalsInteractions.bQuickplayButtonPressed) // mutually exclusive
			AND NOT (MPGlobalsInteractions.bSimulate_QuickplayButtonPressed)
			AND NOT (MPGlobalsInteractions.bQuickRestartAnim)
				IF IS_POINTING_ANIM_BUTTON_PRESSED()
				AND (IsSafeForPointing() OR IsPlayerPointing())
				
					// We have a dedicated key for this on PC, so no need to double tap - Steve R LDS.
					IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						MPGlobalsInteractions.bPointingDoubleTap = TRUE
					ENDIF
				
					// is this a double tap?
					IF (GET_TIME_DIFFERENCE(GET_THIS_TIMER(), MPGlobalsInteractions.timePointingPress) < DOUBLE_TAP_INTERVAL)
						MPGlobalsInteractions.bPointingDoubleTap = TRUE
					ENDIF
					MPGlobalsInteractions.timePointingPress = GET_THIS_TIMER()								
					MPGlobalsInteractions.bPointingButtonPressed = TRUE
					NET_PRINT("[interactions] MPGlobalsInteractions.bPointingButtonPressed = TRUE") NET_NL()
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_POINTING_ANIM_BUTTON_PRESSED()	
			OR (MPGlobalsInteractions.bQuickplayButtonPressed)
				MPGlobalsInteractions.bPointingButtonPressed = FALSE	
				MPGlobalsInteractions.bPointingDoubleTap = FALSE	
				NET_PRINT("[interactions] MPGlobalsInteractions.bPointingButtonPressed = FALSE	") NET_NL()
			ENDIF
			
		ENDIF
		
//		// if anim is playing but player is not facing desired direction then tell the player to achieve that heading.
//		IF IsPointingActive()
//		AND IsSafeForPointing(FALSE)
//			IF IsCameraPointingDirectionTooDifferent(MAX_POINTING_DIFF_FROM_CAM)
//			AND IS_NET_PLAYER_OK(PLAYER_ID())
//			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
//				FaceWhereCameraIsPointing()
//			ENDIF
//		ENDIF	
		
	ELSE
		IF (MPGlobalsInteractions.bPointingButtonPressed)
			MPGlobalsInteractions.bPointingButtonPressed = FALSE
			NET_PRINT("[interactions] MPGlobalsInteractions.bPointingButtonPressed = FALSE	2") NET_NL()
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER(" has quickplay button been pressed?")
	#ENDIF
	#ENDIF		
		

	// ***********************************************************************************
	//							quickplay - on foot
	// ***********************************************************************************
	
	// set the quick play anim based on james menu choice - only do this check when the button is pressed
	IF (MPGlobalsInteractions.bQuickplayButtonPressed) AND (MPGlobalsInteractions.QuickplayInteraction.bPlayInteractionAnim)
	//OR (MPGlobalsInteractions.bUpdateQuickplayAnim)
		STORE_UP_TO_DATE_CREW_ANIM()
		IF NOT IS_MP_PLAYER_ANIM_SETTING_CREW_DEFAULT()
			MPGlobalsInteractions.QuickplayInteraction.iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER)
			MPGlobalsInteractions.QuickplayInteraction.iInteractionAnim = GET_MP_PLAYER_ANIM_SETTING() 
		ELSE
			STORE_UP_TO_DATE_CREW_ANIM()
			MPGlobalsInteractions.QuickplayInteraction.iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_CREW)
			MPGlobalsInteractions.QuickplayInteraction.iInteractionAnim = MPGlobalsInteractions.iMyCrewInteractionAnim
		ENDIF
		//MPGlobalsInteractions.bUpdateQuickplayAnim = FALSE
	ENDIF
	UPDATE_INTERACTION_ANIM_INTERNAL(gInteractionsQuickplayData, MPGlobalsInteractions.QuickplayInteraction, PLAYER_PED_ID(), TRUE)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("quickplay - on foot")
	#ENDIF
	#ENDIF		
	
	// ***********************************************************************************
	//							quickplay - in car
	// ***********************************************************************************
	IF (MPGlobalsInteractions.bQuickplayButtonPressed) AND (MPGlobalsInteractions.QuickplayInteractionVeh.bPlayInteractionAnim)
	//OR (MPGlobalsInteractions.bUpdateQuickplayAnimVeh)
		MPGlobalsInteractions.QuickplayInteractionVeh.iInteractionType = ENUM_TO_INT(INTERACTION_ANIM_TYPE_IN_CAR)
		MPGlobalsInteractions.QuickplayInteractionVeh.iInteractionAnim = GET_MP_PLAYER_VEH_ANIM_SETTING() 
		//MPGlobalsInteractions.bUpdateQuickplayAnim = FALSE
	ENDIF	
	UPDATE_INTERACTION_ANIM_INTERNAL(gInteractionsQuickplayDataVeh, MPGlobalsInteractions.QuickplayInteractionVeh, PLAYER_PED_ID(), FALSE)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("quickplay - in car")
	#ENDIF
	#ENDIF		
	
		
	
	// ***********************************************************************************
	//							pointing - on foot
	// ***********************************************************************************
	UpdatePlayerPointing()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("pointing - on foot")
	#ENDIF
	#ENDIF	
	
	// ***********************************************************************************
	//							synced interactions
	// ***********************************************************************************	
	UPDATE_SYNCED_INTERACTIONS_CLIENT()		
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_SYNCED_INTERACTIONS_CLIENT")
	#ENDIF
	#ENDIF		
	
	// disable controls when quickplay button is continued to be held
	IF ((MPGlobalsInteractions.bQuickplayButtonPressed) OR (MPGlobalsInteractions.bPointingButtonPressed))
	AND (MPGlobalsInteractions.bAnyInteractionIsPlaying)
		NET_PRINT("[interactions] calling DisableControlsForInteraction 4") NET_NL()
		DisableControlsForInteraction(PLAYER_PED_ID())	
	ENDIF	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("disable controls")
	#ENDIF
	#ENDIF		
	
	
	// if any interaction is playing, but none are actually playing then cleanup.
	IF (MPGlobalsInteractions.bAnyInteractionIsPlaying)
		IF NOT (MPGlobalsInteractions.PlayerInteraction.bInteractionIsPlaying)
		AND NOT (MPGlobalsInteractions.QuickplayInteraction.bInteractionIsPlaying)
		AND NOT (MPGlobalsInteractions.QuickplayInteractionVeh.bInteractionIsPlaying)			
			MPGlobalsInteractions.bAnyInteractionIsPlaying = FALSE	
			MPGlobalsInteractions.bInteractionAssetsAreLoading = FALSE
			MPGlobalsInteractions.iCurrentlyPlayingType = -1
			MPGlobalsInteractions.iCurrentlyPlayingAnim = -1
			NET_PRINT("[interactions] MPGlobalsInteractions.bAnyInteractionIsPlaying = FALSE 4") NET_NL()
		ENDIF
	ENDIF
	
	
	// debug give player a ciggy	
	#IF IS_DEBUG_BUILD
	
		// call START_INTERACTION_ANIM command
		IF (MPGlobalsInteractions.bDebugCallCommand_STOP_INTERACTION_ANIM)
			STOP_INTERACTION_ANIM(MPGlobalsInteractions.bDebugCallCommandCleanupImmediately)
			MPGlobalsInteractions.bDebugCallCommand_STOP_INTERACTION_ANIM = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bDebugCallCommmandInCar)
			START_INTERACTION_ANIM(ENUM_TO_INT(INTERACTION_ANIM_TYPE_IN_CAR), MPGlobalsInteractions.iDebugStartAnimInCar, MPGlobalsInteractions.bDebugHoldLoop, MPGlobalsInteractions.bDebugDoSnackCheck, MPGlobalsInteractions.bDebugFullBody, MPGlobalsInteractions.bDebugWaitUntilPlayerControl, MPGlobalsInteractions.bDebugWaitUntilSync, MPGlobalsInteractions.fDebugStartPhase, MPGlobalsInteractions.bSkipInteractionAnimIntroDelay, MPGlobalsInteractions.bDebugWaitForFadingIn, MPGlobalsInteractions.bDebugForceWeaponVisible, MPGlobalsInteractions.bDebugIgnoreStatCheck)
			MPGlobalsInteractions.bDebugCallCommmandInCar = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bDebugCallCommmandCrew)
			START_INTERACTION_ANIM(ENUM_TO_INT(INTERACTION_ANIM_TYPE_CREW), MPGlobalsInteractions.iDebugStartAnimCrew, MPGlobalsInteractions.bDebugHoldLoop, MPGlobalsInteractions.bDebugDoSnackCheck, MPGlobalsInteractions.bDebugFullBody, MPGlobalsInteractions.bDebugWaitUntilPlayerControl, MPGlobalsInteractions.bDebugWaitUntilSync, MPGlobalsInteractions.fDebugStartPhase, MPGlobalsInteractions.bSkipInteractionAnimIntroDelay, MPGlobalsInteractions.bDebugWaitForFadingIn, MPGlobalsInteractions.bDebugForceWeaponVisible, MPGlobalsInteractions.bDebugIgnoreStatCheck)
			MPGlobalsInteractions.bDebugCallCommmandCrew = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bDebugCallCommmandPlayer)
			START_INTERACTION_ANIM(ENUM_TO_INT(INTERACTION_ANIM_TYPE_PLAYER), MPGlobalsInteractions.iDebugStartAnimPlayer, MPGlobalsInteractions.bDebugHoldLoop, MPGlobalsInteractions.bDebugDoSnackCheck, MPGlobalsInteractions.bDebugFullBody, MPGlobalsInteractions.bDebugWaitUntilPlayerControl, MPGlobalsInteractions.bDebugWaitUntilSync, MPGlobalsInteractions.fDebugStartPhase, MPGlobalsInteractions.bSkipInteractionAnimIntroDelay, MPGlobalsInteractions.bDebugWaitForFadingIn, MPGlobalsInteractions.bDebugForceWeaponVisible, MPGlobalsInteractions.bDebugIgnoreStatCheck)
			MPGlobalsInteractions.bDebugCallCommmandPlayer = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bDebugCallCommmandSpecial)
			START_INTERACTION_ANIM(ENUM_TO_INT(INTERACTION_ANIM_TYPE_SPECIAL), MPGlobalsInteractions.iDebugStartAnimSpecial, MPGlobalsInteractions.bDebugHoldLoop, MPGlobalsInteractions.bDebugDoSnackCheck, MPGlobalsInteractions.bDebugFullBody, MPGlobalsInteractions.bDebugWaitUntilPlayerControl, MPGlobalsInteractions.bDebugWaitUntilSync, MPGlobalsInteractions.fDebugStartPhase, MPGlobalsInteractions.bSkipInteractionAnimIntroDelay, MPGlobalsInteractions.bDebugWaitForFadingIn, MPGlobalsInteractions.bDebugForceWeaponVisible, MPGlobalsInteractions.bDebugIgnoreStatCheck)
			MPGlobalsInteractions.bDebugCallCommmandSpecial = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bDebugCallCommand_SET_NEXT_RESPAWN_INTERACTION)
			SET_NEXT_RESPAWN_INTERACTION(g_SpawnData.iRespawnInteractionType, g_SpawnData.iRespawnSelectedAnim, g_SpawnData.bRespawnInteractionFullBody, g_SpawnData.fRespawnInteractionStartPhase, g_SpawnData.bRespawnInteractionForceWeaponVisible)
			MPGlobalsInteractions.bDebugCallCommand_SET_NEXT_RESPAWN_INTERACTION = FALSE
		ENDIF
	
	
		IF (MPGlobalsInteractions.bGiveCiggy)
			INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT)			
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT)) NET_PRINT(" ciggys") NET_NL()
			MPGlobalsInteractions.bGiveCiggy = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bRemoveCiggys)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT, 0)	
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT)) NET_PRINT(" ciggys") NET_NL()
			MPGlobalsInteractions.bRemoveCiggys = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bGiveDrink1)
			INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT)			
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT)) NET_PRINT(" orange drink") NET_NL()
			MPGlobalsInteractions.bGiveDrink1 = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bRemoveDrink1)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, 0)	
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT)) NET_PRINT(" orange drink") NET_NL()
			MPGlobalsInteractions.bRemoveDrink1 = FALSE
		ENDIF	
		IF (MPGlobalsInteractions.bGiveDrink2)
			INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT)			
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT)) NET_PRINT(" bourge drink") NET_NL()
			MPGlobalsInteractions.bGiveDrink2 = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bRemoveDrink2)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT, 0)	
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT)) NET_PRINT(" bourge drink") NET_NL()
			MPGlobalsInteractions.bRemoveDrink2 = FALSE
		ENDIF	
		IF (MPGlobalsInteractions.bGiveDrink3)
			INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHAMP_BOUGHT)
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHAMP_BOUGHT)) NET_PRINT(" champ drink") NET_NL()
			MPGlobalsInteractions.bGiveDrink3 = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bRemoveDrink3)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHAMP_BOUGHT, 0)	
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHAMP_BOUGHT)) NET_PRINT(" champ drink") NET_NL()
			MPGlobalsInteractions.bRemoveDrink3 = FALSE
		ENDIF	
		IF (MPGlobalsInteractions.bGiveFood1)
			INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS)			
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS)) NET_PRINT(" yum snack") NET_NL()
			MPGlobalsInteractions.bGiveFood1 = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bRemoveFood1)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, 0)	
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS)) NET_PRINT(" yum snack") NET_NL()
			MPGlobalsInteractions.bRemoveFood1 = FALSE
		ENDIF
		
		IF (MPGlobalsInteractions.bGiveFood2)
			INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS)			
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS)) NET_PRINT(" health snack") NET_NL()
			MPGlobalsInteractions.bGiveFood2 = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bRemoveFood2)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, 0)	
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS)) NET_PRINT(" health snack") NET_NL()
			MPGlobalsInteractions.bRemoveFood2 = FALSE
		ENDIF		
		
		IF (MPGlobalsInteractions.bGiveFood3)
			INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS)			
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS)) NET_PRINT(" epic snack") NET_NL()
			MPGlobalsInteractions.bGiveFood3 = FALSE
		ENDIF
		IF (MPGlobalsInteractions.bRemoveFood2)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, 0)	
			NET_PRINT("[interactions] Interactions - player has ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS)) NET_PRINT(" epic snack") NET_NL()
			MPGlobalsInteractions.bRemoveFood3 = FALSE
		ENDIF	
		
		IF (MPGlobalsInteractions.bOutputSyncedInteractionData)
			IF OUTPUT_ALL_COORDS_AND_HEADINGS_FOR_PLAYERS_FROM_SYNCED_INTERACTIONS()
				MPGlobalsInteractions.bOutputSyncedInteractionData = FALSE	
			ENDIF
		ENDIF
		IF (MPGlobalsInteractions.bStartSyncedInteractionWithAllPlayersInSession)
			
			IF (MPGlobalsInteractions.iSyncedInteractionState = -1)
			
				INT iPlayerNameHash[4]
				INT iCount
				INT i
				REPEAT 4 i
					iPlayerNameHash[i] = -1
				ENDREPEAT
				
				// make local player slot 0
				iPlayerNameHash[0] = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
				iCount++
				
				// grab some other players in session
				REPEAT NUM_NETWORK_PLAYERS i
					IF NOT (i = NATIVE_TO_INT(PLAYER_ID()))
						IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i))
							iPlayerNameHash[iCount] = NETWORK_HASH_FROM_PLAYER_HANDLE(INT_TO_NATIVE(PLAYER_INDEX, i)	)
							iCount++
							IF (iCount >= 4)
								i = NUM_NETWORK_PLAYERS
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				
				//MPGlobalsInteractions.iDebug_SyncedSceneUniqueID = GET_RANDOM_INT_IN_RANGE()
				
				BROADCAST_LAUNCH_SYNCED_INTERACTION_EVENT(MPGlobalsInteractions.iDebug_SyncedSceneSceneID_actual, iPlayerNameHash, MPGlobalsInteractions.iDebug_SyncedSceneLocationID, MPGlobalsInteractions.iDebug_SyncedSceneActor)

				MPGlobalsInteractions.bStartSyncedInteractionWithAllPlayersInSession = FALSE
				
			ENDIF
		ENDIF
		IF (MPGlobalsInteractions.bStartSyncedInteractionLocallyOnly)
			IF (MPGlobalsInteractions.iSyncedInteractionState = -1)
				INT iPlayerHash[4] 
				iPlayerHash[0] = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
				iPlayerHash[1] = -1
				iPlayerHash[2] = -1
				iPlayerHash[3] = -1
				START_SYNCED_INTERACTION_SCENE(MPGlobalsInteractions.iDebug_SyncedSceneSceneID_actual, iPlayerHash, MPGlobalsInteractions.iDebug_SyncedSceneLocationID, MPGlobalsInteractions.iDebug_SyncedSceneActor) 
				MPGlobalsInteractions.bStartSyncedInteractionLocallyOnly = FALSE
			ENDIF
		ENDIF
		IF (MPGlobalsInteractions.bUpdateSyncedCamPans)
			UpdateAllEndCamerasForScenes()
			MPGlobalsInteractions.bUpdateSyncedCamPans = FALSE		
		ENDIF
		IF (MPGlobalsInteractions.bDebug_TurnOnCameraEditing)
			INT i, j
			CAMERA_INDEX CamID
			REPEAT TOTAL_SYNCED_INTERACTION_SCENES i				
				REPEAT NUMBER_OF_INTERACTION_SCENE_ALTERNATE_LOCATIONS j	
					
					// grab position / heading
					IF (gSyncedInteractionScene[i].bGrabPosition[j])
						gSyncedInteractionScene[i].vPosition[j] = GET_PLAYER_COORDS(PLAYER_ID())
						gSyncedInteractionScene[i].fHeading[j] = GET_ENTITY_HEADING(PLAYER_PED_ID())
						gSyncedInteractionScene[i].bGrabPosition[j] = FALSE
 					ENDIF
					
					// grab start cam details
					IF (gSyncedInteractionScene[i].bGrabStartCam[j])					
						CamID = GET_RENDERING_CAM()
						IF DOES_CAM_EXIST(CamID)							
							gSyncedInteractionScene[i].vCamStartPos[j] = GET_CAM_COORD(CamID)
							gSyncedInteractionScene[i].vCamStartRot[j] = GET_CAM_ROT(CamID)
							gSyncedInteractionScene[i].fCamStartFOV[j] = GET_CAM_FOV(CamID)
						ENDIF					
						gSyncedInteractionScene[i].bGrabStartCam[j] = FALSE
					ENDIF
					
					// grab end cam details
					IF (gSyncedInteractionScene[i].bGrabEndCam[j])					
						CamID = GET_RENDERING_CAM()
						IF DOES_CAM_EXIST(CamID)							
							gSyncedInteractionScene[i].vCamEndPos[j] = GET_CAM_COORD(CamID)
							gSyncedInteractionScene[i].vCamEndRot[j] = GET_CAM_ROT(CamID)
							gSyncedInteractionScene[i].fCamEndFOV[j] = GET_CAM_FOV(CamID)
						ENDIF					
						gSyncedInteractionScene[i].bGrabEndCam[j] = FALSE
					ENDIF
					
					
					// output
					IF (gSyncedInteractionScene[i].bOutputDetailsToFile[j])
					
						OPEN_NAMED_DEBUG_FILE(IA_PATHNAME, IA_FILENAME)
						
							IA_PRINTNL()
						
							// position
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].vPosition[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTVECTOR(gSyncedInteractionScene[i].vPosition[j])

							// heading
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].fHeading[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTFLOAT(gSyncedInteractionScene[i].fHeading[j])

							// layout
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].iLayout[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTINT(gSyncedInteractionScene[i].iLayout[j])

							// vCamStartPos
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].vCamStartPos[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTVECTOR(gSyncedInteractionScene[i].vCamStartPos[j])							
							
							// vCamStartRot
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].vCamStartRot[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTVECTOR(gSyncedInteractionScene[i].vCamStartRot[j])	

							// vCamEndPos
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].vCamEndPos[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTVECTOR(gSyncedInteractionScene[i].vCamEndPos[j])	
			
							// vCamEndRot
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].vCamEndRot[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTVECTOR(gSyncedInteractionScene[i].vCamEndRot[j])	
							
							// fov
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].fCamStartFOV[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTFLOAT(gSyncedInteractionScene[i].fCamStartFOV[j])
							
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].fCamEndFOV[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTFLOAT(gSyncedInteractionScene[i].fCamEndFOV[j])							
							
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].fShakeAmplitude[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTFLOAT(gSyncedInteractionScene[i].fShakeAmplitude[j])	
							
//							// iDuration
//							SP_PRINTNL()
//							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
//							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
//							IA_PRINTSTRING("].iCamDuration[")
//							IA_PRINTINT(j)
//							IA_PRINTSTRING("] = ")
//							IA_PRINTINT(gSyncedInteractionScene[i].iCamDuration[j])
							
							// iCamEndTime
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].iCamEndTime[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTINT(gSyncedInteractionScene[i].iCamEndTime[j])
							
							// pan state
							IA_PRINTNL()
							IA_PRINTSTRING("gSyncedInteractionScene[SIS_")
							IA_PRINTSTRING(gSyncedInteractionScene[i].strDebugName)
							IA_PRINTSTRING("].iCamPanState[")
							IA_PRINTINT(j)
							IA_PRINTSTRING("] = ")
							IA_PRINTSTRING("CAM_PAN_CUSTOM")
//							SWITCH gSyncedInteractionScene[i].iCamPanState[j]
//								CASE CAM_PAN_CUSTOM 
//									IA_PRINTSTRING("CAM_PAN_CUSTOM")
//								BREAK
//								CASE CAM_PAN_RIGHT
//									IA_PRINTSTRING("CAM_PAN_RIGHT")
//								BREAK
//								CASE CAM_PAN_LEFT
//									IA_PRINTSTRING("CAM_PAN_LEFT")
//								BREAK
//							ENDSWITCH
								
							IA_PRINTNL()
						
						CLOSE_DEBUG_FILE()
					
						gSyncedInteractionScene[i].bOutputDetailsToFile[j] = FALSE	
					ENDIF
				ENDREPEAT
			ENDREPEAT
		ENDIF
		
	#ENDIF
	
	IF (MPGlobalsInteractions.bDisabledThisFrame)
		PRINTLN("[interactions] UPDATE_INTERACTION_ANIMS - was disabled this frame, re-enabling.")
		MPGlobalsInteractions.bDisabledThisFrame = FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_INTERACTION_ANIMS - end")
	#ENDIF
	#ENDIF		
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF	
	
ENDPROC

PROC UPDATE_INTERACTION_ANIMS_SP()

	
	// if player is moving store that time
	
	
	IF NOT IS_PED_STILL(PLAYER_PED_ID())
		MPGlobalsInteractions.timePlayerMoves = INT_TO_NATIVE(TIME_DATATYPE,GET_GAME_TIMER())		
	ENDIF
	IF NOT IsPedInIdleStance(PLAYER_PED_ID())
		MPGlobalsInteractions.timePlayerNonIdle = INT_TO_NATIVE(TIME_DATATYPE,GET_GAME_TIMER())
	ENDIF

	// ***********************************************************************************
	//								player menu interactions
	// ***********************************************************************************
	UPDATE_INTERACTION_ANIM_INTERNAL(gInteractionsPlayerData, MPGlobalsInteractions.PlayerInteraction, PLAYER_PED_ID(), FALSE)
		
	
	// disable controls when quickplay button is continued to be held
	IF ((MPGlobalsInteractions.bQuickplayButtonPressed) 
		OR (MPGlobalsInteractions.bPointingButtonPressed)
		OR (MPGlobalsInteractions.bSimulate_QuickplayButtonPressed))
	AND (MPGlobalsInteractions.bAnyInteractionIsPlaying)
		DisableControlsForInteraction(PLAYER_PED_ID())	
	ENDIF	
	
	// if any interaction is playing, but none are actually playing then cleanup.
	IF (MPGlobalsInteractions.bAnyInteractionIsPlaying)
		IF NOT (MPGlobalsInteractions.PlayerInteraction.bInteractionIsPlaying)
		AND NOT (MPGlobalsInteractions.QuickplayInteraction.bInteractionIsPlaying)
		AND NOT (MPGlobalsInteractions.QuickplayInteractionVeh.bInteractionIsPlaying)			
			MPGlobalsInteractions.bAnyInteractionIsPlaying = FALSE	
			MPGlobalsInteractions.iCurrentlyPlayingType = -1
			MPGlobalsInteractions.iCurrentlyPlayingAnim = -1
		ENDIF
	ENDIF
	
	
	
ENDPROC



