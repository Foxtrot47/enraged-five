USING "globals.sch"
USING "net_include.sch"
USING "net_spawn.sch"


PROC RESET_GROUP_WARP_TIMER()
	g_SpawnData.tGroupWarpTimer = GET_NETWORK_TIME_ACCURATE()
	PRINTLN("[group_warp] RESET_GROUP_WARP_TIMER called")
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC ClearPlayerNamesHash(INT &iPlayerNamesHash[])
	INT i
	REPEAT COUNT_OF(iPlayerNamesHash) i
		iPlayerNamesHash[i] = -1
	ENDREPEAT
ENDPROC

PROC SetPlayerNamesHashToAllPlayersInGame(INT &iPlayerNamesHash[])
	ClearPlayerNamesHash(iPlayerNamesHash)
	INT iPlayer
	INT iCount
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
			iPlayerNamesHash[iCount] = 	NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID)
			iCount++
		ENDIF
	ENDREPEAT	
ENDPROC

PROC SetPlayerNamesHashToAllPlayersInGangInSameProperty(INT &iPlayerNamesHash[])
	ClearPlayerNamesHash(iPlayerNamesHash)

	INT iBossHash
	INT iBossCount
	INT iPlayer
	INT iCount
	
	PLAYER_INDEX PlayerID
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)

			IF (PlayerID = PLAYER_ID())
			OR GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), PlayerID)

				IF (PlayerID = PLAYER_ID())
				OR ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), PlayerID, TRUE  , TRUE  )

					IF NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PlayerID)
					OR NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PlayerID)

						// Capture the boss hash and position in the iPlayerNamesHash list.
						IF GB_IS_PLAYER_BOSS_OF_THIS_BIKER_GANG(PlayerID, GB_GET_LOCAL_PLAYER_GANG_BOSS())
							iBossHash = NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID)
							iBossCount = iCount
						
							CDEBUG1LN(DEBUG_SPAWNING, "[Group_Warp] SetPlayerNamesHashToAllPlayersInGangInSameProperty - Capturing boss hash: ", iBossHash, " and position in iPlayerHashNames: ", iBossCount)
						ENDIF
						
						iPlayerNamesHash[iCount] = 	NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID)
						iCount++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT	
	
	// Ensure the boss is always placed first in the list. 
	IF iPlayerNamesHash[0] != iBossHash
		CDEBUG1LN(DEBUG_SPAWNING, "[Group_Warp] SetPlayerNamesHashToAllPlayersInGangInSameProperty - Swapping iPlayerNamesHash positions.")
		
		INT iTempHash = iPlayerNamesHash[0]
		iPlayerNamesHash[0] = iBossHash
		iPlayerNamesHash[iBossCount] = iTempHash
		
		CDEBUG1LN(DEBUG_SPAWNING, "[Group_Warp] SetPlayerNamesHashToAllPlayersInGangInSameProperty - Setting boss to iPlayerNamesHash[0] and gang member to iPlayerNamesHash[", iBossCount, "]")
	ENDIF
ENDPROC

PROC CLEANUP_LOCAL_PLAYER_GROUP_WARP()
	
	CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] CLEANUP_LOCAL_PLAYER_GROUP_WARP called!")
	
	g_SpawnData.iGroupWarpState = -1
	g_SpawnData.bUseExactGroupWarpCoords = FALSE
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.eGroupWarpLocation = GROUP_WARP_LOCATION_NULL
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.iUniqueID = -1
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.bReadyToStart = FALSE
	//GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.bAbandon = FALSE // dont clear the abandon flag. Gets cleared on START

	SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)	
	
	CLEAR_SPECIFIC_SPAWN_LOCATION()
	
ENDPROC

PROC START_LOCAL_PLAYER_GROUP_WARP(GROUP_WARP_LOCATION eGroupWarpLocation, INT &iPlayerNamesHash[], INT iBitset)
	
	INT i
	
	PRINTLN("[group_warp] START_LOCAL_PLAYER_GROUP_WARP eGroupWarpLocation = ", ENUM_TO_INT(eGroupWarpLocation))
	
	REPEAT COUNT_OF(iPlayerNamesHash) i
		PRINTLN("[group_warp] START_LOCAL_PLAYER_GROUP_WARP iPlayerNamesHash[", i, "] = ", iPlayerNamesHash[i])	
		IF (iPlayerNamesHash[i] = 0)
			iPlayerNamesHash[i] = -1 // make sure we are consistent for null hashes
		ENDIF
	ENDREPEAT
	
	// if the player name hashes are all -1 then just quit out.
	BOOL bQuitOutOfFunction = TRUE
	REPEAT COUNT_OF(iPlayerNamesHash) i
		IF NOT (iPlayerNamesHash[i] = -1)
			bQuitOutOfFunction = FALSE	
		ENDIF
	ENDREPEAT
	IF (bQuitOutOfFunction)
		PRINTLN("[group_warp] START_LOCAL_PLAYER_GROUP_WARP iPlayerNamesHash[] are all -1, aborting.")		
		EXIT
	ENDIF

	// make the unique id a total of the player hash's
	INT iUniqueID
	INT iDistToHighestInt
	INT iAddOn
	REPEAT COUNT_OF(iPlayerNamesHash) i
		
		// check the value we add on is not going to break the highest int
		iAddOn = iPlayerNamesHash[i]
		IF (iAddOn < 0)
			iAddOn *= -1
		ENDIF
		
		iDistToHighestInt = HIGHEST_INT - iUniqueID
		
		// if number is going to be bigger than highest then loop back to 0
		IF (iAddOn > iDistToHighestInt)
			iUniqueID = iAddOn - iDistToHighestInt
		ELSE
			iUniqueID += iAddOn
		ENDIF
				
	ENDREPEAT
	

	PRINTLN("[group_warp] START_LOCAL_PLAYER_GROUP_WARP iUniqueID = ", iUniqueID)

	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.eGroupWarpLocation = eGroupWarpLocation
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.iUniqueID = iUniqueID
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.bReadyToStart = FALSE
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.bAbandon = FALSE

	g_SpawnData.iGroupWarp_Bitset = iBitset

	REPEAT COUNT_OF(iPlayerNamesHash) i
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.iPlayerNamesHash[i] = iPlayerNamesHash[i]
	ENDREPEAT	
	
	IF NOT IS_SCREEN_FADED_OUT()		
		DO_SCREEN_FADE_OUT(500)
	ENDIF
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	
	RESET_GROUP_WARP_TIMER()
	g_SpawnData.iGroupWarpState = 0
	

ENDPROC

PROC ResetSeverGroupWarpDataForPlayer(INT iPlayer)
	IF NOT (GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer] = GROUP_WARP_LOCATION_NULL)
		#IF IS_DEBUG_BUILD
		IF GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer] != GROUP_WARP_LOCATION_NULL
			PRINTLN("[group_warp] ResetSeverGroupWarpDataForPlayer - reset eGroupWarpLocation for player ", iPlayer)
		ENDIF 
		#ENDIF 
		GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer] = GROUP_WARP_LOCATION_NULL
	ENDIF
	IF NOT (GlobalServerBD.GroupWarpBD.iUniqueID[iPlayer] = -1)
		#IF IS_DEBUG_BUILD
		IF GlobalServerBD.GroupWarpBD.iUniqueID[iPlayer] != -1
			PRINTLN("[group_warp] ResetSeverGroupWarpDataForPlayer - reset iUniqueID for player ", iPlayer)
		ENDIF 
		#ENDIF 
		GlobalServerBD.GroupWarpBD.iUniqueID[iPlayer] = -1
	ENDIF
	IF NOT (GlobalServerBD.GroupWarpBD.iArea[iPlayer] = -1)
		#IF IS_DEBUG_BUILD
		IF GlobalServerBD.GroupWarpBD.iArea[iPlayer] != -1
			PRINTLN("[group_warp] ResetSeverGroupWarpDataForPlayer - reset iArea for player ", iPlayer)
		ENDIF 
		#ENDIF 
		GlobalServerBD.GroupWarpBD.iArea[iPlayer] = -1
	ENDIF
ENDPROC

FUNC INT GetGroupWarpAreaFromOtherPlayersWithSameUniqueID(INT iThisPlayer)
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF (GlobalServerBD.GroupWarpBD.iUniqueID[iPlayer] = GlobalServerBD.GroupWarpBD.iUniqueID[iThisPlayer])
		AND (GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer] = GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iThisPlayer])
			IF (GlobalServerBD.GroupWarpBD.iArea[iPlayer] != -1)
				RETURN GlobalServerBD.GroupWarpBD.iArea[iPlayer]
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC


FUNC INT GetPositionInHashGroupWarpList(INT iThisPlayerNameHash, INT &iHashNames[])
	INT i
	REPEAT COUNT_OF(iHashNames) i
		IF (iThisPlayerNameHash = iHashNames[i])
			RETURN(i)
		ENDIF
	ENDREPEAT
	RETURN(-1)
ENDFUNC

FUNC INT GetLocalPlayerPositionInGroupWarpHashList()

	RETURN GetPositionInHashGroupWarpList(NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()), GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.iPlayerNamesHash)

ENDFUNC

FUNC BOOL AreAllPlayersInHashListWithUniqueIDReadyForGroupWarp(INT iUnique, INT &iHashNames[])
	INT iPlayer
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS iPlayer	
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE)
			IF (GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID = iUnique)
				IF IsPlayerNameHashInGroupWarpList(NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID), iHashNames)
					IF NOT (GlobalplayerBD[iPlayer].GroupWarpBD.bReadyToStart)
					AND NOT (GlobalplayerBD[iPlayer].GroupWarpBD.bAbandon)
						PRINTLN("[interactions] AreAllPlayersInHashListWithUniqueIDReadyForGroupWarp - player ", iPlayer, " not ready. ") 
						RETURN(FALSE)
					ENDIF
				ELSE
					PRINTLN("[interactions] AreAllPlayersInHashListWithUniqueIDReadyForGroupWarp - player ", iPlayer, " not in hash list. ")
				ENDIF
			ELSE
				PRINTLN("[interactions] AreAllPlayersInHashListWithUniqueIDReadyForGroupWarp - player ", iPlayer, " not got same unique id. ") 	
			ENDIF
		ELSE
			//PRINTLN("[interactions] AreAllPlayersInHashListWithUniqueIDReadyForGroupWarp - player ", iPlayer, " not ok. ") 		
		ENDIF
	ENDREPEAT
	RETURN(TRUE)
ENDFUNC

FUNC INT NumberOfPlayersInHashList(INT &iPlayerNamesHash[])
	INT iCount = 0
	INT i
	REPEAT COUNT_OF(iPlayerNamesHash) i
		IF NOT (iPlayerNamesHash[i] = -1)
			iCount++
		ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC

FUNC BOOL IsGroupWarpAreaSafeFromObstructions(GROUP_WARP_LOCATION eGroupWarpLocation, INT iUniqueID, INT iArea, INT &iPlayerNamesHash[], FLOAT fDist)
	
	VECTOR vCoords
	FLOAT fHeading
	INT iPlayer
	PLAYER_INDEX PlayerID

	// loop through each spawn position
	INT iNumberOfSpawnPositions = GET_NUMBER_OF_POSITIONS_FOR_GROUP_WARP_LOCATION(eGroupWarpLocation)

	INT iNumberOfPlayers = NumberOfPlayersInHashList(iPlayerNamesHash)
	
	IF (iNumberOfPlayers < iNumberOfSpawnPositions)
		iNumberOfSpawnPositions = iNumberOfPlayers
	ENDIF
	
	PRINTLN("[group_warp] IsGroupWarpAreaSafeFromObstructions - using iNumberOfSpawnPositions = ", iNumberOfSpawnPositions)

	INT iThisPosition
	INT i

	REPEAT iNumberOfSpawnPositions iThisPosition
			
		GET_SPAWN_COORDS_FOR_GROUP_WARP_LOCATION(eGroupWarpLocation, iArea, iThisPosition, vCoords, fHeading)

		// find if there are any players too near the location (who are not part of the same group)
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
			IF IS_NET_PLAYER_OK(PlayerID)
				IF (GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID != iUniqueID)
				AND NOT IsPlayerNameHashInGroupWarpList(NETWORK_HASH_FROM_PLAYER_HANDLE(PlayerID), iPlayerNamesHash)
								
					IF NOT IS_PLAYER_SPECTATING(PlayerID)
						IF (VDIST(vCoords, GET_PLAYER_COORDS(PlayerID)) < fDist)
							PRINTLN("[group_warp] IsGroupWarpAreaSafeFromObstructions - player ", iPlayer, " is too near location ", iArea)
							RETURN(FALSE)
						ENDIF
						// is player about to warp here?
						IF (GlobalServerBD.bWarpRequestTimeIntialised[iPlayer])
							IF (VDIST(vCoords, GlobalServerBD.g_vWarpRequest[iPlayer]) < fDist)
								PRINTLN("[group_warp] IsGroupWarpAreaSafeFromObstructions - player ", iPlayer, " is warping near location ", iArea)
								RETURN(FALSE)
							ENDIF
						ENDIF
					ENDIF
						
					// spawning a vehicle there?
					REPEAT NUM_SERVER_STORED_VEH_REQUESTS i
						IF IsPointWithinModelBounds(vCoords, GlobalServerBD.g_VehSpawnReqData[iPlayer][i].vPos, GlobalServerBD.g_VehSpawnReqData[iPlayer][i].fHeading, GlobalServerBD.g_VehSpawnReqData[iPlayer][i].ModelName)
							PRINTLN("[group_warp] IsGroupWarpAreaSafeFromObstructions - player ", iPlayer, " is warping a car near location ", iArea)
							RETURN(FALSE)
						ENDIF	
					ENDREPEAT
					
				ENDIF
			ENDIF
		ENDREPEAT
		
		// any fires nearby?
		IF (GET_NUMBER_OF_FIRES_IN_RANGE(vCoords, 2.5) > 0)
			PRINTLN("[group_warp] IsGroupWarpAreaSafeFromObstructions - fire nearby location ", iArea)
			RETURN(FALSE)
		ENDIF
		
		// are any vehicles nearby?
		IF IS_ANY_VEHICLE_NEAR_POINT(vCoords, 2.0)
			PRINTLN("[group_warp] IsGroupWarpAreaSafeFromObstructions - vehicle nearby location ", iArea)
			RETURN(FALSE)
		ENDIF
		
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC INT GetSafeAreaForGroupWarpLocation(GROUP_WARP_LOCATION eGroupWarpLocation, INT iUniqueID, INT &iPlayerNamesHash[])

	#IF IS_DEBUG_BUILD
	IF (g_SpawnData.bFailedToGetGroupWarpArea)
		PRINTLN("[group_warp] GetSafeAreaForGroupWarpLocation - returning -2 as debug bool set")
		RETURN -2
	ENDIF
	#ENDIF

	INT i
	INT iArea
	BOOL bSafe[MAX_NUM_GROUP_WARP_AREAS]
	INT iPlayer
	
	// set them all safe by default
	REPEAT MAX_NUM_GROUP_WARP_AREAS i	
		bSafe[i] = TRUE	
	ENDREPEAT	
	
	// first find if any other groups are already using this scene ID, if so mark it as unsafe.	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF (GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer] = eGroupWarpLocation)
		AND NOT (GlobalServerBD.GroupWarpBD.iUniqueID[iPlayer] = iUniqueID)
			IF (GlobalServerBD.GroupWarpBD.iArea[iPlayer] > -1)
				bSafe[GlobalServerBD.GroupWarpBD.iArea[iPlayer]] = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	FLOAT fDist = 5.0
	
	IF NOT IS_BIT_SET(g_SpawnData.iGroupWarp_Bitset, GWBS_WARP_INTO_VEHICLE)
		fDist = 3.0
	ENDIF
	
	// next check that the area is safe
	REPEAT MAX_NUM_GROUP_WARP_AREAS iArea
		IF (bSafe[iArea])			
			IF NOT IsGroupWarpAreaSafeFromObstructions(eGroupWarpLocation, iUniqueID, iArea, iPlayerNamesHash, fDist)
				PRINTLN("[group_warp] GetSafeAreaForGroupWarpLocation - obstructed location ", iArea)
				bSafe[iArea] = FALSE			
			ENDIF			
		ENDIF
	ENDREPEAT
	
	// return lowest safe location
	REPEAT MAX_NUM_GROUP_WARP_AREAS iArea
		IF (iArea < GET_NUMBER_OF_AREAS_FOR_GROUP_WARP_LOCATION(eGroupWarpLocation))
			IF (bSafe[iArea])
				PRINTLN("[group_warp] GetSafeAreaForGroupWarpLocation - returning valid location ", iArea)
				RETURN(iArea)
			ENDIF
		ENDIF
	ENDREPEAT

	// none are safe - use fallback 
	PRINTLN("[group_warp] GetSafeAreaForGroupWarpLocation - no safe locations returning -2 ")
	RETURN -2
ENDFUNC

PROC SERVER_UPDATE_GROUP_WARP_REQUESTS()

		INT iPlayer
	INT iTemp
	
	iPlayer = g_SpawnData.iGroupWarpPlayerToProcess
	
	IF NOT (GlobalplayerBD[iPlayer].GroupWarpBD.eGroupWarpLocation = GROUP_WARP_LOCATION_NULL)

		// check the scene and unique id matches
		IF NOT (GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer] = GlobalplayerBD[iPlayer].GroupWarpBD.eGroupWarpLocation)
		OR NOT (GlobalServerBD.GroupWarpBD.iUniqueID[iPlayer] = GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID)
			ResetSeverGroupWarpDataForPlayer(iPlayer)
			GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer] = GlobalplayerBD[iPlayer].GroupWarpBD.eGroupWarpLocation
			GlobalServerBD.GroupWarpBD.iUniqueID[iPlayer] = GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID
			PRINTLN("[group_warp] SERVER_UPDATE_GROUP_WARP_REQUESTS - assigning iPlayer = ", iPlayer, ", ENUM_TO_INT(eGroupWarpLocation) = ", GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer], ", iUniqueID = ", GlobalServerBD.GroupWarpBD.iUniqueID[iPlayer])
		ENDIF

		// assign a safe area		
		IF (GlobalServerBD.GroupWarpBD.iArea[iPlayer] = -1)
			// does anyone in this group already have one?
			iTemp = GetGroupWarpAreaFromOtherPlayersWithSameUniqueID(iPlayer)
									
			IF (iTemp != -1)				
				GlobalServerBD.GroupWarpBD.iArea[iPlayer] = iTemp
				PRINTLN("[group_warp] SERVER_UPDATE_GROUP_WARP_REQUESTS - assigning from another group member iArea[", iPlayer, "] = ", iTemp)
			ELSE
				// find a safe location
				GlobalServerBD.GroupWarpBD.iArea[iPlayer] = GetSafeAreaForGroupWarpLocation(GlobalplayerBD[iPlayer].GroupWarpBD.eGroupWarpLocation, GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID, GlobalplayerBD[iPlayer].GroupWarpBD.iPlayerNamesHash)
				PRINTLN("[group_warp] SERVER_UPDATE_GROUP_WARP_REQUESTS - assigning from GetSafeSyncInteractionLocation iArea[", iPlayer, "] = ", GlobalServerBD.GroupWarpBD.iArea[iPlayer])
			ENDIF
		ENDIF
				
	ELSE
		// make sure the data is cleared when not used
		ResetSeverGroupWarpDataForPlayer(iPlayer)
	ENDIF

	g_SpawnData.iGroupWarpPlayerToProcess++
	IF (g_SpawnData.iGroupWarpPlayerToProcess >= NUM_NETWORK_PLAYERS)
		g_SpawnData.iGroupWarpPlayerToProcess = 0
	ENDIF

ENDPROC


PROC WARP_ALL_PLAYERS_TO_LOCATION(GROUP_WARP_LOCATION eGroupWarpLocation)

	PRINTLN("[group_warp] WARP_ALL_PLAYERS_IN_GANG_TO_LOCATION eGroupWarpLocation = ", ENUM_TO_INT(eGroupWarpLocation))

	INT iPlayerNamesHash[NUM_NETWORK_PLAYERS]
	SetPlayerNamesHashToAllPlayersInGame(iPlayerNamesHash)
	
	INT iBitSet
	
	BROADCAST_GROUP_WARP_EVENT(eGroupWarpLocation, iPlayerNamesHash, iBitSet)

ENDPROC

FUNC GROUP_WARP_LOCATION ConvertPropertyToGroupWarpLocation(INT iProperty)
	SWITCH iProperty
		CASE PROPERTY_CLUBHOUSE_1_BASE_A			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_1
		CASE PROPERTY_CLUBHOUSE_2_BASE_A			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_2
		CASE PROPERTY_CLUBHOUSE_3_BASE_A			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_3
		CASE PROPERTY_CLUBHOUSE_4_BASE_A			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_4
		CASE PROPERTY_CLUBHOUSE_5_BASE_A			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_5
		CASE PROPERTY_CLUBHOUSE_6_BASE_A			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_6
		CASE PROPERTY_CLUBHOUSE_7_BASE_B			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_7
		CASE PROPERTY_CLUBHOUSE_8_BASE_B			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_8
		CASE PROPERTY_CLUBHOUSE_9_BASE_B			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_9
		CASE PROPERTY_CLUBHOUSE_10_BASE_B			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_10
		CASE PROPERTY_CLUBHOUSE_11_BASE_B			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_11
		CASE PROPERTY_CLUBHOUSE_12_BASE_B			RETURN GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_12	
	ENDSWITCH
	ASSERTLN("ConvertPropertyToGroupWarpLocation - none setup for property ", iProperty)
	RETURN GROUP_WARP_LOCATION_NULL
ENDFUNC

FUNC GROUP_WARP_LOCATION ConvertSimpleInteriorToGroupWarpLocation(SIMPLE_INTERIORS eSimpleInteriorID)

	IF IS_SIMPLE_INTERIOR_OF_TYPE(eSimpleInteriorID, SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE)
		INT iDefunctBase = ENUM_TO_INT(eSimpleInteriorID) - ENUM_TO_INT(SIMPLE_INTERIOR_DEFUNCT_BASE_1)
		RETURN INT_TO_ENUM(GROUP_WARP_LOCATION, iDefunctBase + ENUM_TO_INT(GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_1))
	#IF FEATURE_CASINO_HEIST
	ELIF IS_SIMPLE_INTERIOR_OF_TYPE(eSimpleInteriorID, SIMPLE_INTERIOR_TYPE_ARCADE)
		INT iArcade = ENUM_TO_INT(eSimpleInteriorID) - ENUM_TO_INT(SIMPLE_INTERIOR_ARCADE_PALETO_BAY)
		RETURN INT_TO_ENUM(GROUP_WARP_LOCATION, iArcade + ENUM_TO_INT(GROUP_WARP_LOCATION_OUTSIDE_ARCADE_PALETO_BAY))
	#ENDIF
	ENDIF

	CDEBUG1LN(DEBUG_SPAWNING, "ConvertSimpleInteriorToGroupWarpLocation - No group warp locations for simple interior ", ENUM_TO_INT(eSimpleInteriorID))
	ASSERTLN("ConvertSimpleInteriorToGroupWarpLocation - No group warp locations for simple interior ", ENUM_TO_INT(eSimpleInteriorID))
	RETURN GROUP_WARP_LOCATION_NULL
ENDFUNC

PROC WARP_ALL_PLAYERS_IN_CLUBHOUSE_OUTSIDE_ON_BIKES()

	// get the clubhouse we are in
	GROUP_WARP_LOCATION eGroupWarpLocation
	eGroupWarpLocation = ConvertPropertyToGroupWarpLocation(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)	

	CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] WARP_ALL_PLAYERS_IN_CLUBHOUSE_OUTSIDE_ON_BIKES eGroupWarpLocation = ", ENUM_TO_INT(eGroupWarpLocation))

	INT iPlayerNamesHash[NUM_NETWORK_PLAYERS]
	SetPlayerNamesHashToAllPlayersInGangInSameProperty(iPlayerNamesHash)
	
	INT iBitSet
	SET_BIT(iBitSet, GWBS_WARP_INTO_VEHICLE)
	
	BROADCAST_GROUP_WARP_EVENT(eGroupWarpLocation, iPlayerNamesHash, iBitSet)

ENDPROC

PROC WARP_ALL_PLAYERS_IN_SIMPLE_INTERIOR_OUTSIDE()
	
	GROUP_WARP_LOCATION eGroupWarpLocation
	eGroupWarpLocation = ConvertSimpleInteriorToGroupWarpLocation(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior)
	
	CDEBUG1LN(DEBUG_SPAWNING, "[Group_Warp] WARP_ALL_PLAYERS_IN_SIMPLE_INTERIOR_OUTSIDE - eGroupWarpLocation = ", ENUM_TO_INT(eGroupWarpLocation))
	
	INT iPlayerNamesHash[NUM_NETWORK_PLAYERS]
	SetPlayerNamesHashToAllPlayersInGangInSameProperty(iPlayerNamesHash)

	INT iBitSet
	SET_BIT(iBitSet, GWBS_WARP_ON_FOOT)
	
	BROADCAST_GROUP_WARP_EVENT(eGroupWarpLocation, iPlayerNamesHash, iBitSet)

ENDPROC

FUNC BOOL IS_CURRENT_PV_SLOT_A_SUITABLE_BIKE()
	IF (CURRENT_SAVED_VEHICLE_SLOT() > -1)
		IF IS_THIS_MODEL_A_BIKE(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)
		OR IS_THIS_MODEL_A_QUADBIKE(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)	
			IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
			AND NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS, MP_SAVED_VEHICLE_IMPOUNDED)		
				IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
				OR IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)			
					PRINTLN("[group_warp] IS_CURRENT_PV_SLOT_A_SUITABLE_BIKE - true 1")
					RETURN TRUE	
				ELSE
					// is local player the owner of the clubhouse and is his pv inside?
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_GANG_TYPE(GT_BIKER)
						IF DOES_VEHICLE_SLOT_BELONG_TO_GARAGE(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel,CURRENT_SAVED_VEHICLE_SLOT(),PROPERTY_OWNED_SLOT_CLUBHOUSE )
							PRINTLN("[group_warp] IS_CURRENT_PV_SLOT_A_SUITABLE_BIKE - true 2")
							RETURN TRUE
						ENDIF
					ENDIF				
				ENDIF
			ENDIF		
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SET_CLUBHOUSE_BIKE_AS_MY_PERSONAL_VEHICLE()

	IF GB_IS_LOCAL_PLAYER_BOSS_OF_GANG_TYPE(GT_BIKER)

		INT iVehSlot
		
		REPEAT MAX_MP_SAVED_VEHICLES iVehSlot
			IF IS_THIS_MODEL_A_BIKE(g_MpSavedVehicles[iVehSlot].vehicleSetupMP.VehicleSetup.eModel)
			OR IS_THIS_MODEL_A_QUADBIKE(g_MpSavedVehicles[iVehSlot].vehicleSetupMP.VehicleSetup.eModel)
			
				IF NOT IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS, MP_SAVED_VEHICLE_IMPOUNDED)
						
					IF DOES_VEHICLE_SLOT_BELONG_TO_GARAGE(g_MpSavedVehicles[iVehSlot].vehicleSetupMP.VehicleSetup.eModel,iVehSlot,PROPERTY_OWNED_SLOT_CLUBHOUSE )
							
						// found
						SET_LAST_USED_VEHICLE_SLOT(iVehSlot)
							
						SET_BIT(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
						
						PRINTLN("[group_warp] SET_CLUBHOUSE_BIKE_AS_MY_PERSONAL_VEHICLE setting iVehSlot = ", iVehSlot)
						
						RETURN TRUE
						
					ENDIF
					
				ENDIF
				
			ENDIF
		ENDREPEAT
		
	ENDIF
		
	RETURN FALSE

ENDFUNC


#IF IS_DEBUG_BUILD
PROC UPDATE_PLACEMENT_VEHICLES()
	INT i
	VECTOR vPos
	FLOAT fHeading	
		
	IF (g_SpawnData.bCreatePlacementVehicles)		

		REQUEST_MODEL(CHIMERA)
		IF HAS_MODEL_LOADED(CHIMERA)
		
			// create
			IF NOT (g_SpawnData.bPlacementVehiclesCreated)
				REPEAT 8 i
					
					IF DOES_ENTITY_EXIST(g_SpawnData.PlacementVehicleID[i])
						DELETE_VEHICLE(g_SpawnData.PlacementVehicleID[i])				
					ENDIF
					
					IF (g_SpawnData.iPlacementVehicle_GroupWarpLocation = 0) 
				
						IF i = 0 
							IF VMAG(g_SpawnData.vPlacementOffset[0]) = 0.0
								g_SpawnData.vPlacementOffset[0] = GET_PLAYER_COORDS(PLAYER_ID())
								g_SpawnData.fPlacementHeadingOffset[0] = GET_ENTITY_HEADING(PLAYER_PED_ID())
							ENDIF				
							g_SpawnData.PlacementVehicleID[0] = CREATE_VEHICLE(CHIMERA, g_SpawnData.vPlacementOffset[0], g_SpawnData.fPlacementHeadingOffset[0], FALSE, FALSE)
						ELSE
							IF DOES_ENTITY_EXIST(g_SpawnData.PlacementVehicleID[0])
								vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_SpawnData.PlacementVehicleID[0], g_SpawnData.vPlacementOffset[i])
								fHeading = GET_ENTITY_HEADING(g_SpawnData.PlacementVehicleID[0]) + g_SpawnData.fPlacementHeadingOffset[i]
								g_SpawnData.PlacementVehicleID[i] = CREATE_VEHICLE(CHIMERA, vPos, fHeading, FALSE, FALSE)
							ENDIF
						ENDIF
						
					ELSE
					
						GET_SPAWN_COORDS_FOR_GROUP_WARP_LOCATION(INT_TO_ENUM(GROUP_WARP_LOCATION, g_SpawnData.iPlacementVehicle_GroupWarpLocation), g_SpawnData.iPlacementVehicle_GroupWarpArea, i, g_SpawnData.vPlacementOffset[i], g_SpawnData.fPlacementHeadingOffset[i] )
						g_SpawnData.PlacementVehicleID[i] = CREATE_VEHICLE(CHIMERA, g_SpawnData.vPlacementOffset[i], g_SpawnData.fPlacementHeadingOffset[i], FALSE, FALSE)
						
					ENDIF
					
					SET_ENTITY_PROOFS(g_SpawnData.PlacementVehicleID[i], TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
					
				ENDREPEAT
				g_SpawnData.bPlacementVehiclesCreated = TRUE
			ENDIF
			
		ENDIF
		
		// update
		IF (g_SpawnData.bPlacementVehiclesCreated)
		
			IF (g_SpawnData.iPlacementVehicle_GroupWarpLocation = 0) 
		
				REPEAT 8 i
					IF i = 0 
						IF DOES_ENTITY_EXIST(g_SpawnData.PlacementVehicleID[0])
						AND NOT IS_ENTITY_DEAD(g_SpawnData.PlacementVehicleID[0])
							IF (g_SpawnData.bLockTargetVehicle)
								SET_ENTITY_COORDS(g_SpawnData.PlacementVehicleID[0], g_SpawnData.vPlacementOffset[0])
								SET_ENTITY_HEADING(g_SpawnData.PlacementVehicleID[0], g_SpawnData.fPlacementHeadingOffset[0])
							ENDIF
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(g_SpawnData.PlacementVehicleID[0])
						AND NOT IS_ENTITY_DEAD(g_SpawnData.PlacementVehicleID[0])
							IF DOES_ENTITY_EXIST(g_SpawnData.PlacementVehicleID[i])
							AND NOT IS_ENTITY_DEAD(g_SpawnData.PlacementVehicleID[i])
								vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_SpawnData.PlacementVehicleID[0], g_SpawnData.vPlacementOffset[i])
								fHeading = GET_ENTITY_HEADING(g_SpawnData.PlacementVehicleID[0]) + g_SpawnData.fPlacementHeadingOffset[i]
								SET_ENTITY_COORDS(g_SpawnData.PlacementVehicleID[i], vPos)
								SET_ENTITY_HEADING(g_SpawnData.PlacementVehicleID[i], fHeading)
							ENDIF
						ENDIF
					ENDIF				
				ENDREPEAT			
		
			ELSE
			
				REPEAT 8 i

					IF DOES_ENTITY_EXIST(g_SpawnData.PlacementVehicleID[i])
					AND NOT IS_ENTITY_DEAD(g_SpawnData.PlacementVehicleID[i])
						IF (g_SpawnData.bLockTargetVehicle)
							SET_ENTITY_COORDS(g_SpawnData.PlacementVehicleID[i], g_SpawnData.vPlacementOffset[i])
							SET_ENTITY_HEADING(g_SpawnData.PlacementVehicleID[i], g_SpawnData.fPlacementHeadingOffset[i])
						ENDIF
					ENDIF
			
				ENDREPEAT	
			
			
			ENDIF
		
		ENDIF
		
		// output
		IF (g_SpawnData.bOutputPlacementVehicles)
			
			IF OPEN_DEBUG_FILE()
				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("Placement vehicle output: ")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				REPEAT 8 i
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("CASE ")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("vPosition = ")
					IF i = 0
						SAVE_VECTOR_TO_DEBUG_FILE(g_SpawnData.vPlacementOffset[i])
					ELSE
						IF DOES_ENTITY_EXIST(g_SpawnData.PlacementVehicleID[0])
						AND NOT IS_ENTITY_DEAD(g_SpawnData.PlacementVehicleID[0])
							IF DOES_ENTITY_EXIST(g_SpawnData.PlacementVehicleID[i])
							AND NOT IS_ENTITY_DEAD(g_SpawnData.PlacementVehicleID[i])
								vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_SpawnData.PlacementVehicleID[0], g_SpawnData.vPlacementOffset[i])
								SAVE_VECTOR_TO_DEBUG_FILE(vPos)
							ENDIF
						ENDIF
					ENDIF
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("fHeading = ")
					IF i = 0
						SAVE_FLOAT_TO_DEBUG_FILE(g_SpawnData.fPlacementHeadingOffset[i])
					ELSE
						IF DOES_ENTITY_EXIST(g_SpawnData.PlacementVehicleID[0])
						AND NOT IS_ENTITY_DEAD(g_SpawnData.PlacementVehicleID[0])
							IF DOES_ENTITY_EXIST(g_SpawnData.PlacementVehicleID[i])
							AND NOT IS_ENTITY_DEAD(g_SpawnData.PlacementVehicleID[i])
								fHeading = GET_ENTITY_HEADING(g_SpawnData.PlacementVehicleID[0]) + g_SpawnData.fPlacementHeadingOffset[i]
								SAVE_FLOAT_TO_DEBUG_FILE(fHeading)
							ENDIF
						ENDIF
					ENDIF
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("BREAK")
				ENDREPEAT
				
				CLOSE_DEBUG_FILE()
				
			ENDIF
				
			g_SpawnData.bOutputPlacementVehicles = FALSE	
		ENDIF
		
		IF (g_SpawnData.bOutputPlacementFormation)
			
			IF OPEN_DEBUG_FILE()
				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("Placement vehicle formation output: ")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				REPEAT 8 i
					SAVE_NEWLINE_TO_DEBUG_FILE()

					SAVE_STRING_TO_DEBUG_FILE("g_SpawnData.vPlacementOffset[")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_STRING_TO_DEBUG_FILE("] = ")
					SAVE_VECTOR_TO_DEBUG_FILE(g_SpawnData.vPlacementOffset[i])
					
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("g_SpawnData.fPlacementHeadingOffset[")
					SAVE_INT_TO_DEBUG_FILE(i)
					SAVE_STRING_TO_DEBUG_FILE("] = ")
					SAVE_FLOAT_TO_DEBUG_FILE(g_SpawnData.fPlacementHeadingOffset[i])
					
				ENDREPEAT
				
				CLOSE_DEBUG_FILE()
				
			ENDIF			
		
			g_SpawnData.bOutputPlacementFormation = FALSE	
		ENDIF
		
		IF (g_SpawnData.iLoadPlacementFormation > -1)
		
			SWITCH g_SpawnData.iLoadPlacementFormation
				CASE 0
					g_SpawnData.vPlacementOffset[0] = <<40.8092, -1043.4675, 29.5242>>
					g_SpawnData.fPlacementHeadingOffset[0] = 249.6747
					g_SpawnData.vPlacementOffset[1] = <<1.5000, -0.5825, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[1] = 0.0000
					g_SpawnData.vPlacementOffset[2] = <<0.0000, -2.7000, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[2] = 0.0000
					g_SpawnData.vPlacementOffset[3] = <<1.5000, -3.4200, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[3] = 0.0000
					g_SpawnData.vPlacementOffset[4] = <<0.0000, -5.5200, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[4] = 0.0000
					g_SpawnData.vPlacementOffset[5] = <<1.5000, -6.2275, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[5] = 0.0000
					g_SpawnData.vPlacementOffset[6] = <<0.0000, -8.3350, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[6] = 0.0000
					g_SpawnData.vPlacementOffset[7] = <<1.5000, -9.0425, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[7] = 0.0000			
				BREAK
				CASE 1
					g_SpawnData.vPlacementOffset[1] = <<1.5000, 0.0000, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[1] = 0.0000
					g_SpawnData.vPlacementOffset[2] = <<0.0000, -2.7000, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[2] = 0.0000
					g_SpawnData.vPlacementOffset[3] = <<1.5000, -2.7000, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[3] = 0.0000
					g_SpawnData.vPlacementOffset[4] = <<0.0000, -5.4000, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[4] = 0.0000
					g_SpawnData.vPlacementOffset[5] = <<1.5000, -5.4000, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[5] = 0.0000
					g_SpawnData.vPlacementOffset[6] = <<0.0000, -8.1000, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[6] = 0.0000
					g_SpawnData.vPlacementOffset[7] = <<1.5000, -8.1000, 0.0000>>
					g_SpawnData.fPlacementHeadingOffset[7] = 0.0000
				BREAK
			ENDSWITCH
		
			g_SpawnData.iLoadPlacementFormation = -1
		
		ENDIF
		
		
		
	ELSE
		
		IF (g_SpawnData.bPlacementVehiclesCreated)
			REPEAT 8 i				
				IF DOES_ENTITY_EXIST(g_SpawnData.PlacementVehicleID[i])
					DELETE_VEHICLE(g_SpawnData.PlacementVehicleID[i])
				ENDIF
			ENDREPEAT
			g_SpawnData.bPlacementVehiclesCreated = FALSE
		ENDIF		
		
	ENDIF
ENDPROC
#ENDIF


PROC SetPlayersAndBikesVisibleLocally(INT iUnique)
	INT iPlayer
	PLAYER_INDEX PlayerID
	
	VEHICLE_INDEX BikeID
	IF NOT IS_ANY_TYPE_OF_CUTSCENE_PLAYING()
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
			IF IS_NET_PLAYER_OK(PlayerID)
				IF (GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID = iUnique)
					SET_PLAYER_VISIBLE_LOCALLY(PlayerID)	
					SET_ENTITY_LOCALLY_VISIBLE(GET_PLAYER_PED(PlayerID))
					
					IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID))
						BikeID = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PlayerID))
						IF DOES_ENTITY_EXIST(BikeID)
							SET_ENTITY_LOCALLY_VISIBLE(BikeID)	
						ENDIF
					ENDIF

					PRINTLN("[interactions] SetPlayersAndBikesVisibleLocally - setting player visible locally ", iPlayer)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC UPDATE_GROUP_WARP()
	
	#IF IS_DEBUG_BUILD
	IF (g_SpawnData.bStartGroupWarpWithAllPlayersInSession)
		
		WARP_ALL_PLAYERS_TO_LOCATION(GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_1)
		
		g_SpawnData.bStartGroupWarpWithAllPlayersInSession = FALSE	
	ENDIF
	
	IF g_SpawnData.bStartDefunctBaseGroupWarp
		
		WARP_ALL_PLAYERS_IN_SIMPLE_INTERIOR_OUTSIDE()
		
		g_SpawnData.bStartDefunctBaseGroupWarp = FALSE
	ENDIF
	
	IF IS_BIT_SET(g_SpawnData.iGroupWarp_Bitset, GWBS_WARP_INTO_VEHICLE)
		UPDATE_PLACEMENT_VEHICLES()
	ENDIF
	#ENDIF
	
	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	PLAYER_SPAWN_LOCATION eSpawnLocation
	
	BOOL bWarpIntoSpawnVehicle
	
	// if player is entering or exiting a property then just cleanup. set the abandon flag so others know. 
	IF (g_SpawnData.iGroupWarpState > -1)
		IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			IF NOT (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.bAbandon)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].GroupWarpBD.bAbandon = TRUE
				CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - player ", NATIVE_TO_INT(PLAYER_ID()), " is entering or exiting property. abandon.")
			ENDIF
			CLEANUP_LOCAL_PLAYER_GROUP_WARP()
		ENDIF	
	ENDIF

	SWITCH g_SpawnData.iGroupWarpState
		CASE -1
			// do fuck all
		BREAK
		
		// make sure screen is faded out
		CASE 0
			IF NOT IS_SCREEN_FADED_OUT()	
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(500)
				ELSE
					PRINTLN("[group_warp] UPDATE_GROUP_WARP - waiting for fade out...")		
				ENDIF
			ELSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
			 	g_SpawnData.iGroupWarpState++
				CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - done fade out.")	
			ENDIF
		BREAK
		
		CASE 1
		
			// make sure screen is faded out
			IF NOT IS_SCREEN_FADED_OUT()	
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(500)
				ENDIF
			ENDIF
		
			// wait for server to recognise the fact i'm waiting
			IF (GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer] = GlobalplayerBD[iPlayer].GroupWarpBD.eGroupWarpLocation)
			AND (GlobalServerBD.GroupWarpBD.iUniqueID[iPlayer] = GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID)
			AND (GlobalServerBD.GroupWarpBD.iArea[iPlayer] != -1)
				
				// make sure a safe location was found
				IF (GlobalServerBD.GroupWarpBD.iArea[iPlayer] != -2)
				#IF IS_DEBUG_BUILD
				AND NOT (g_SpawnData.bFailedToGetGroupWarpArea)
				#ENDIF				
					IF GetLocalPlayerPositionInGroupWarpHashList() < GET_NUMBER_OF_POSITIONS_FOR_GROUP_WARP_LOCATION(GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer])						
						CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - got valid coords.")
						GET_SPAWN_COORDS_FOR_GROUP_WARP_LOCATION(GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer], GlobalServerBD.GroupWarpBD.iArea[iPlayer], GetLocalPlayerPositionInGroupWarpHashList(), g_SpawnData.vGroupWarpCoords, g_SpawnData.fGroupWarpHeading)																	
						g_SpawnData.bUseExactGroupWarpCoords = TRUE
					ELSE
						// could get a spawn point, too many players for location
						CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - too many players for location.")					
						GET_SPAWN_COORDS_FOR_GROUP_WARP_LOCATION(GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer], GlobalServerBD.GroupWarpBD.iArea[iPlayer], 0, g_SpawnData.vGroupWarpCoords, g_SpawnData.fGroupWarpHeading)					
						g_SpawnData.bUseExactGroupWarpCoords = FALSE
					ENDIF
				ELSE				
					// couldn't get a location
					CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - too many players for location.")					
					GET_SPAWN_COORDS_FOR_GROUP_WARP_LOCATION(GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer], 0, 0, g_SpawnData.vGroupWarpCoords, g_SpawnData.fGroupWarpHeading)					
					g_SpawnData.bUseExactGroupWarpCoords = FALSE
				ENDIF
				
				// go to net warp
				SETUP_SPECIFIC_SPAWN_LOCATION(g_SpawnData.vGroupWarpCoords, g_SpawnData.fGroupWarpHeading)
								
				// set players pv as bike
				IF IS_BIT_SET(g_SpawnData.iGroupWarp_Bitset, GWBS_WARP_INTO_VEHICLE)
					g_SpawnData.iGroupWarpStatePVCreation = 0
					g_SpawnData.iGroupWarpState = 50
				
				ELSE
					g_SpawnData.iGroupWarpState++
				ENDIF
				
				
			ELSE
				
				#IF IS_DEBUG_BUILD
			
					IF NOT (GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer] = GlobalplayerBD[iPlayer].GroupWarpBD.eGroupWarpLocation)
						CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - waiting for server to recognise my eGroupWarpLocation request.")	
					ENDIF
					
					IF NOT (GlobalServerBD.GroupWarpBD.iUniqueID[iPlayer] = GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID)
						CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - waiting for server to recognise my unique id request.")	
					ENDIF
			
					IF (GlobalServerBD.GroupWarpBD.iArea[iPlayer] = -1)
						CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - waiting for server to assign location.")
					ENDIF
					
				#ENDIF

				
			ENDIF
		BREAK
		

		CASE 2
		
			IF (g_SpawnData.bUseExactGroupWarpCoords)
				eSpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS
			ELSE
				eSpawnLocation = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
			ENDIF
			
			IF IS_BIT_SET(g_SpawnData.iGroupWarp_Bitset, GWBS_WARP_INTO_VEHICLE)
				bWarpIntoSpawnVehicle = TRUE
			ELSE
				bWarpIntoSpawnVehicle = FALSE
			ENDIF
		
			IF WARP_TO_SPAWN_LOCATION(eSpawnLocation, FALSE, bWarpIntoSpawnVehicle, DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,0,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE,TRUE)
						
				GlobalplayerBD[iPlayer].GroupWarpBD.bReadyToStart = TRUE
				
				CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - net warp completed.")
				
				g_SpawnData.iGroupWarpState++
				
			ENDIF
		BREAK
		
		CASE 3
			IF AreAllPlayersInHashListWithUniqueIDReadyForGroupWarp(GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID, GlobalplayerBD[iPlayer].GroupWarpBD.iPlayerNamesHash)
			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), g_SpawnData.tGroupWarpTimer) > 20000)	
			
				CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - ready to start fade")
				RESET_GROUP_WARP_TIMER()
				g_SpawnData.iGroupWarpState++
			
			ELSE
				IF IS_BIT_SET(g_SpawnData.iGroupWarp_Bitset, GWBS_WARP_INTO_VEHICLE)
					MakeVehicleFlashIfDriver(250)
				ENDIF
				
				CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - waiting for others - ")
			ENDIF
		BREAK
		
		// fade in
		CASE 4
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), g_SpawnData.tGroupWarpTimer) > 1000)
				
				IF IS_BIT_SET(g_SpawnData.iGroupWarp_Bitset, GWBS_WARP_INTO_VEHICLE)

					IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						SET_ENTITY_VISIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
						PRINTLN("[group_warp] UPDATE_GROUP_WARP - SET_ENTITY_VISIBLE(g_SpawnData.MissionSpawnDetails.SpawnedVehicle, TRUE)") 
					ENDIF	

				ELSE
					
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
							PRINTLN("[Group_Warp] UPDATE_GROUP_WARP - SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)")
						ENDIF
					ENDIF
				ENDIF
				
				// do fade in
				IF NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()				
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP start fade in.")
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
				RESET_GROUP_WARP_TIMER()
				g_SpawnData.iGroupWarpState++
				
			ELSE
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), g_SpawnData.tGroupWarpTimer) < 750)
					CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - still waiting to start fade in ")
					
					IF IS_BIT_SET(g_SpawnData.iGroupWarp_Bitset, GWBS_WARP_INTO_VEHICLE)
						MakeVehicleFlashIfDriver(1)	
					ENDIF	
				ELSE
					PRINTLN("[group_warp] UPDATE_GROUP_WARP - about to start fade in")
				ENDIF
			ENDIF
		BREAK
		
		// finish
		CASE 5
			IF IS_SCREEN_FADED_IN()				
				CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP faded in.")
				
				RESET_GROUP_WARP_TIMER()
				g_SpawnData.iGroupWarpState++
					
				// print help if not the leader
				IF IS_BIT_SET(g_SpawnData.iGroupWarp_Bitset, GWBS_WARP_INTO_VEHICLE)
					IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG(FALSE)
						PRINT_HELP("PROP_CLU_M_14b") // Owner has requested all Motorcycle Club Members leave Clubhouse
					ENDIF
				ENDIF
				
								
			ENDIF
		BREAK
		
		// wait for time to finish
		CASE 6
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), g_SpawnData.tGroupWarpTimer) > 7000)	
				CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP finished.")
				CLEANUP_LOCAL_PLAYER_GROUP_WARP()
			ELSE
				SetPlayersAndBikesVisibleLocally(GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID)
			ENDIF
		BREAK
		
		// deal with pv cleanup/creation
		CASE 50
			SWITCH g_SpawnData.iGroupWarpStatePVCreation
				CASE 0
					IF IS_CURRENT_PV_SLOT_A_SUITABLE_BIKE()
						CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - going to use current pv as bike.")
						g_SpawnData.iGroupWarpStatePVCreation = 10
						CLEANUP_MP_SAVED_VEHICLE(FALSE, FALSE, TRUE)	
					ELSE
						
						IF SET_CLUBHOUSE_BIKE_AS_MY_PERSONAL_VEHICLE()
							CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - using a clubhouse bike.")
							g_SpawnData.iGroupWarpStatePVCreation = 10
							CLEANUP_MP_SAVED_VEHICLE(FALSE, FALSE, TRUE)	
						ELSE
					
							CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - going to use ratbike.")
							CLEANUP_MP_SAVED_VEHICLE(FALSE, FALSE, TRUE)	
							g_SpawnData.iGroupWarpStatePVCreation++
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
						g_SpawnData.iGroupWarpStatePVCreation++	
					ENDIF
				BREAK
				CASE 2
					SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, RATBIKE, FALSE, FALSE, FALSE,DEFAULT,DEFAULT,DEFAULT,TRUE)					
					g_SpawnData.iGroupWarpStatePVCreation = 0
					g_SpawnData.iGroupWarpState = 2
					CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - set player to spawn in rat bike.")	
				BREAK
				
				// if using existing personal vehicle
				CASE 10
					IF NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
						g_SpawnData.iGroupWarpStatePVCreation++	
					ENDIF
				BREAK
				CASE 11
					IF (CURRENT_SAVED_VEHICLE_SLOT() > -1)
						SET_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
						SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, RATBIKE, FALSE, FALSE, TRUE,DEFAULT,DEFAULT,DEFAULT,TRUE)	
					ELSE
						CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - some how the current vehicle slot is -1 ??.")	
						SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, RATBIKE, FALSE, FALSE, FALSE,DEFAULT,DEFAULT,DEFAULT,TRUE)	
					ENDIF									
					g_SpawnData.iGroupWarpStatePVCreation = 0
					g_SpawnData.iGroupWarpState = 2
					CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - set player to spawn in pv")
				BREAK
				
			ENDSWITCH
		BREAK
		
		// abandoned
		CASE 99
			IF AreAllPlayersInHashListWithUniqueIDReadyForGroupWarp(GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID, GlobalplayerBD[iPlayer].GroupWarpBD.iPlayerNamesHash)
			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), g_SpawnData.tGroupWarpTimer) > 60000)
				CLEANUP_LOCAL_PLAYER_GROUP_WARP()
			ELSE
				CDEBUG1LN(DEBUG_SPAWNING, "[group_warp] UPDATE_GROUP_WARP - abandoned, waiting to cleanup.")
			ENDIF
		BREAK
			
		
	ENDSWITCH

ENDPROC


// eof

