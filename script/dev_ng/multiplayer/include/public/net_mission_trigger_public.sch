USING "rage_builtins.sch"
USING "globals.sch"

USING "net_mission_trigger.sch"




// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_Trigger_Public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all generic mission triggering public functions.
//
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************
// *****************************************************************************************************************************************************





// =============================================================================================================================================================
//      Mission Triggering Access Functions
// =============================================================================================================================================================

// -------------------------------------------------------------------------------------------------------------------------------------------------------------
//      Routines that Gather Dialogue Data then Trigger Mission
// -------------------------------------------------------------------------------------------------------------------------------------------------------------

// PURPOSE:	Gather Communication Data then Invite the player onto the mission
//
// INPUT PARAMS:		paramMissionData			Struct containing the mission ID and the Instance ID of the mission to be joined
//						paramOptions				BITFIELD of options passed from Mission Controller
PROC Gather_Comms_Details_Then_Invite_Player_Onto_Mission(MP_MISSION_DATA paramMissionData, INT paramOptions)

	// Use the Mission Triggering struct as a go-between to get the Comms Data because it contains all the required fields
	// KGM NOTE: In future, may be able to separate this all out into a comms struct that I can pass around
	g_structMissionTriggeringMP commsDataOnly
	
	BOOL useTextMessage = Should_MP_Mission_Team_Use_A_PreMission_Txtmsg(paramMissionData.mdID.idMission, GET_PLAYER_TEAM(PLAYER_ID()))
	
	// Use Secondary Objectives?
	BOOL useSecondaryObjectives = IS_BIT_SET(paramOptions, MC_BITS_TRIGGERING_USE_SECONDARY_OBJECTIVE)
	
	BOOL invitationSpeech = TRUE
	IF NOT (Get_Mission_Communication_Data(paramMissionData.mdID.idMission, invitationSpeech, commsDataOnly, useSecondaryObjectives, useTextMessage))
		// There is no stored speech, so forward to Mission Triggering without Communication
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Gather_Comms_Details_Then_Invite_Player_Onto_Mission(): ERROR: There is no stored mission-invitation speech so can't offer the mission - forcing the player onto it.") NET_NL()
		#ENDIF
		
		Force_Launch_Mission(paramMissionData)
		EXIT
	ENDIF
	
	// Forward to Mission Triggering
	enumCharacterList theCaller = INT_TO_ENUM(enumCharacterList, commsDataOnly.mtCharSheetAsInt)
	Offer_To_Launch_Mission(paramMissionData, commsDataOnly.mtCharacter, commsDataOnly.mtSpeaker, theCaller, commsDataOnly.mtGroup, commsDataOnly.mtRoot, useTextMessage)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gather Communication Data then Force the player onto the mission
//
// INPUT PARAMS:		paramMissionData			Struct containing the mission ID and the Instance ID of the mission to be joined
//						paramOptions				BITFIELD of options passed from Mission Controller
PROC Gather_Comms_Details_Then_Force_Player_Onto_Mission(MP_MISSION_DATA paramMissionData, INT paramOptions)

	// Use the Mission Triggering struct as a go-between to get the Comms Data because it contains all the required fields
	// KGM NOTE: In future, may be able to separate this all out into a comms struct that I can pass around
	g_structMissionTriggeringMP commsDataOnly
	
	BOOL useTextMessage = Should_MP_Mission_Team_Use_A_PreMission_Txtmsg(paramMissionData.mdID.idMission, GET_PLAYER_TEAM(PLAYER_ID()))
	
	// Show Cutscene?
	BOOL showCutscene = IS_BIT_SET(paramOptions, MC_BITS_TRIGGERING_SHOW_CUTSCENE)
	
	// Use Secondary Objectives?
	BOOL useSecondaryObjectives = IS_BIT_SET(paramOptions, MC_BITS_TRIGGERING_USE_SECONDARY_OBJECTIVE)
	
	BOOL invitationSpeech = FALSE
	IF NOT (Get_Mission_Communication_Data(paramMissionData.mdID.idMission, invitationSpeech, commsDataOnly, useSecondaryObjectives, useTextMessage))
		// There is no stored speech, so forward to Mission Triggering without Communication
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Gather_Comms_Details_Then_Force_Player_Onto_Mission(): There is no stored speech so forcing the player onto the mission without any communication.") NET_NL()
		#ENDIF
		
		Force_Launch_Mission(paramMissionData, showCutscene)
		EXIT
	ENDIF
	
	// Forward to Mission Triggering
	enumCharacterList theCaller = INT_TO_ENUM(enumCharacterList, commsDataOnly.mtCharSheetAsInt)
	
	// Use a communication to start the mission
	Force_Launch_Mission_After_Comms(paramMissionData, commsDataOnly.mtCharacter, commsDataOnly.mtSpeaker, theCaller, commsDataOnly.mtGroup, commsDataOnly.mtRoot, useTextMessage, showCutscene)
	
ENDPROC






// =============================================================================================================================================================
//      Check Functions
// =============================================================================================================================================================

// PURPOSE:	Is the player actively on any mission.
//
// RETURN VALUE:		BOOL					TRUE if the player is active on a mission, otherwise FALSE
FUNC BOOL Is_Player_On_Or_Triggering_Any_Mission()

	// Return FALSE (not on or triggering any mission) if the mission state 'no request'?
	IF (Is_Mission_Triggering_State_No_Request())
		RETURN FALSE
	ENDIF
	
	// Mission State is not 'no request' therefore there is a request
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player is on or is setting up a specific MissionID.
//
// INPUT PARAMS:		paramMission			The mission being checked
//						paramVariation			[DEFAULT = NO_MISSION_VARIATION] The mission variation if important
// RETURN VALUE:		BOOL					TRUE if the player is on or triggering the mission, otherwise FALSE
FUNC BOOL Is_Player_On_Or_Triggering_This_Mission(MP_MISSION paramMission, INT paramVariation = NO_MISSION_VARIATION)

	// Is the player triggering any mission?
	IF NOT (Is_Player_On_Or_Triggering_Any_Mission())
		RETURN FALSE
	ENDIF
	
	// Check if the mission is the specified mission
	IF (paramMission != g_sTriggerMP.mtMissionData.mdID.idMission)
		RETURN FALSE
	ENDIF
	
	// If required, check if the variation is the specified variation
	IF (paramVariation != NO_MISSION_VARIATION)
		IF (paramVariation != g_sTriggerMP.mtMissionData.mdID.idVariation)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Player is on or triggering the specified mission
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player is actively on a specific MissionID.
//
// INPUT PARAMS:		paramMission			The mission being checked 
//						paramVariation			[DEFAULT = NO_MISSION_VARIATION] The mission variation if important
// RETURN VALUE:		BOOL					TRUE if the player is actively on the mission, otherwise FALSE
FUNC BOOL Is_Player_Actively_On_This_Mission(MP_MISSION paramMission, INT paramVariation = NO_MISSION_VARIATION)
	
	// Check if the player is actively on a mission to see if any further tests are required
	IF NOT (Is_Mission_Triggering_State_Mission_Active())
		RETURN FALSE
	ENDIF

	// Is the player on or triggering the specified mission?
	IF NOT (Is_Player_On_Or_Triggering_This_Mission(paramMission, paramVariation))
		RETURN FALSE
	ENDIF
	
	// Player is on the specified mission
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the UniqueID for the mission being launched
//
// RETURN VALUE:		INT					Unique Mission ID, or NO_UNIQUE_ID
FUNC INT Get_UniqueID_For_Mission_Being_Triggered()

	// Is the player triggering any mission?
	IF NOT (Is_Player_On_Or_Triggering_Any_Mission())
		RETURN NO_UNIQUE_ID
	ENDIF
	
	// Return the Unique ID of the mission being triggered
	RETURN g_sTriggerMP.mtMissionData.mdUniqueID

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the MissionID and Variation for the mission being launched
//
// REFERENCE PARAMS:		refMissionID				Mission ID, or eNULL_MISSION
//							refVariation				Variation, or NO_MISSION_VARIATION
//							refContentID				ContentID, or ""
PROC Get_Mission_Variation_ContentID_Being_Triggered(MP_MISSION &refMissionID, INT &refVariation, TEXT_LABEL_23 &refContentID)

	// Is the player triggering any mission?
	IF NOT (Is_Player_On_Or_Triggering_Any_Mission())
		refMissionID	= eNULL_MISSION
		refVariation	= NO_MISSION_VARIATION
		refContentID	= ""
		EXIT
	ENDIF
	
	// Return the MissionID and variation of the mission being triggered
	refMissionID	= g_sTriggerMP.mtMissionData.mdID.idMission
	refVariation	= g_sTriggerMP.mtMissionData.mdID.idVariation
	refContentID	= g_sTriggerMP.mtMissionData.mdID.idCloudFilename

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if this player is actively receiving a pre-mission comms for this uniqueID.
//
// INPUT PARAMS:		paramUniqueID			The uniqueID for the mission request being checked. 
// RETURN VALUE:		BOOL					TRUE if the player is actively receiving the pre-mission comms, otherwise FALSE
FUNC BOOL Is_Player_Actively_Receiving_This_PreMission_Comms(INT paramUniqueID)

	// Ensure the uniqueID passed in is valid
	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("...KGM MP: Is_Player_Actively_Receiving_This_PreMission_Comms() - ERROR: UniqueID is NO_UNIQUE_ID. Script = ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			SCRIPT_ASSERT("Is_Player_Actively_Receiving_This_PreMission_Comms(): ERROR - UniqueID passed as parameter is NO_UNIQUE_ID. Tell Keith")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// Is the player on or triggering the specified mission?
	IF NOT (Is_Player_On_Or_Triggering_Any_Mission())
		RETURN FALSE
	ENDIF
	
	// Check if the player is already actively on a mission to see if any further tests are required
	IF (Is_Mission_Triggering_State_Mission_Active())
		RETURN FALSE
	ENDIF
	
	// Check if the uniqueID matches the mission being checked
	IF NOT (Get_UniqueID_For_Mission_Being_Triggered() = paramUniqueID)
		RETURN FALSE
	ENDIF
	
	// Returns TRUE if the mission is waiting for a pre-mission comms to end
	RETURN (Is_Mission_Triggering_State_Waiting_For_PreMission_Comms_To_End())

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if this player is actively receiving a pre-mission cutscene for this uniqueID.
//
// INPUT PARAMS:		paramUniqueID			The uniqueID for the mission request being checked. 
// RETURN VALUE:		BOOL					TRUE if the player is actively receiving the pre-mission cutscene, otherwise FALSE
FUNC BOOL Is_Player_Actively_Receiving_This_PreMission_Cutscene(INT paramUniqueID)

	// Ensure the uniqueID passed in is valid
	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("...KGM MP: Is_Player_Actively_Receiving_This_PreMission_Cutscene() - ERROR: UniqueID is NO_UNIQUE_ID. Script = ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			SCRIPT_ASSERT("Is_Player_Actively_Receiving_This_PreMission_Cutscene(): ERROR - UniqueID passed as parameter is NO_UNIQUE_ID. Tell Keith")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// Is the player on or triggering the specified mission?
	IF NOT (Is_Player_On_Or_Triggering_Any_Mission())
		RETURN FALSE
	ENDIF
	
	// Check if the player is already actively on a mission to see if any further tests are required
	IF (Is_Mission_Triggering_State_Mission_Active())
		RETURN FALSE
	ENDIF
	
	// Check if the uniqueID matches the mission being checked
	IF NOT (Get_UniqueID_For_Mission_Being_Triggered() = paramUniqueID)
		RETURN FALSE
	ENDIF
	
	// Returns TRUE if the mission is waiting for a pre-mission comms to end
	RETURN (Is_Mission_Triggering_State_Cutscene_Active())

ENDFUNC






// =============================================================================================================================================================
//      Quit Mission Functions
// =============================================================================================================================================================

// PURPOSE:	The Player should abandon this specific mission and instance.
//
// INPUT PARAMS:		paramMissionID			MissionID to be quit
//						paramInstanceID			InstanceID to be quit
PROC Abandon_This_MP_Mission(MP_MISSION paramMissionID, INT paramInstanceID)

	// Check for NULL MISSION
	IF (paramMissionID = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Abandon_This_MP_Mission(): ERROR - MissionID is eNULL_MISSION") NET_NL()
			NET_PRINT("...........Called by this script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Make sure the player is running this mission and instance
	IF NOT (IS_PLAYER_RUNNING_MP_MISSION_INSTANCE(PLAYER_ID(), paramMissionID, paramInstanceID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Abandon_This_MP_Mission(): ERROR - Player is not running this Mission and Instance") NET_NL()
			NET_PRINT("...........Called by this script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			NET_PRINT("...........Mission: ") NET_PRINT(GET_MP_MISSION_NAME(paramMissionID)) NET_NL()
			NET_PRINT("...........Instance: ") NET_PRINT_INT(paramInstanceID) NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Abandon_This_MP_Mission()") NET_NL()
		NET_PRINT("...........Called by this script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("...........Mission: ") NET_PRINT(GET_MP_MISSION_NAME(paramMissionID)) NET_NL()
		NET_PRINT("...........Instance: ") NET_PRINT_INT(paramInstanceID) NET_NL()
	#ENDIF
	
	
	// Abandon the mission
	FORCE_ABANDON_CURRENT_MP_MISSION()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Force the player to quit this mission if still in pre-mission phase
//
// INPUT PARAMS:			paramUniqueID			UniqueID of the mission trigger to be forced to quit pre-mission
//
// NOTES:	Added as a brute force resync should controller and triggerer go out of sync after Host Migration
PROC Force_Player_To_Quit_If_In_PreMission_Phase(INT paramUniqueID)

	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MTrigger]: Force_Player_To_Quit_If_In_PreMission_Phase - Passed In UniqueID = NO_UNIQUE_ID. Nothing To Do.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NOT (Is_Player_On_Or_Triggering_Any_Mission())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MTrigger]: Force_Player_To_Quit_If_In_PreMission_Phase - Player is not triggering any mission. Nothing To Do.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NOT (Get_UniqueID_For_Mission_Being_Triggered() = paramUniqueID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MTrigger]: Force_Player_To_Quit_If_In_PreMission_Phase - Passed In UniqueID doesn't match mission being triggered. Nothing To Do") NET_NL()
			NET_PRINT("                      Passed In UniqueID : ") NET_PRINT_INT(paramUniqueID) NET_NL()
			NET_PRINT("                      Triggering UniqueID: ") NET_PRINT_INT(Get_UniqueID_For_Mission_Being_Triggered()) NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Found the mission, so setting the 'Force Quit Pre-Mission' flag
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MTrigger]: Force_Player_To_Quit_If_In_PreMission_Phase - Setting Force Quit PreMission Flag (will do nothing if player beyond pre-mission)") NET_NL()
	#ENDIF
	
	Set_MT_Bitflag_As_Force_Quit_PreMission()
	
ENDPROC

// PURPOSE:	Return the Contact for the current mission
//
// RETURN VALUE:		enumCharacterList			The contact for the current mission, or NO_CHARACTER if not actively on a mission or if there is no contact
FUNC enumCharacterList Get_Current_Mission_Contact()
	RETURN (g_eCurrentMissionContact)
ENDFUNC

