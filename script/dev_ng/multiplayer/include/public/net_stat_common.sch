
USING "globals.sch"
USING "Net_stat_system.sch"
USING "Transition_Controller.sch"
USING "net_saved_vehicles.sch"





PROC RUN_OVERWRITE(BOOL ClearBank = FALSE)

	NET_NL()NET_PRINT("LOGIC_MPHUD_STATS_LOAD_FAIL: RUN_OVERWRITE ")
	
	INT I
	DELETE_SAVE_SLOT(0, TRUE, TRUE, ClearBank) //All slots
		
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS	
//		g_Private_SlotFailedLoad[I] = FALSE
		SET_CODE_STATS_LOADED(TRUE, I)	
	ENDFOR
	
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS
		net_nl()NET_PRINT("LOGIC_MPHUD_STATS_LOAD_FAIL: STAT_SAVE slot ")NET_PRINT_INT(I)NET_NL()
		STAT_SAVE(I, FALSE)
	ENDFOR
			
	IF IS_TRANSITION_ACTIVE()
		TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)
		HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS)
	ENDIF

ENDPROC


FUNC BOOL RUN_OVERWRITE_FUNCTION(SAVE_OVERWRITE_DATA& Details, BOOL ClearBank, DELETE_REASONS DeleteReason)

	NET_NL()NET_PRINT("[PCDELETE] RUN_OVERWRITE_FUNCTION Details.Stages = ")NET_PRINT_INT(Details.Stages)
	SWITCH Details.Stages
	
		CASE 0
		
			IF USE_SERVER_TRANSACTIONS()
				Details.Stages++
				Details.CharacterSlot = 0
			ELSE
				Details.Stages = 3
			ENDIF 
		BREAK
		
		CASE 1
			
			IF ClearBank
			AND DeleteReason != DELETE_REASON_CTRL_H_USER_DEBUG_ACTION
				ClearBank = FALSE
				NET_NL()NET_PRINT("[PCDELETE] RUN_OVERWRITE_FUNCTION ***** CLEAR BANK WAS TRUE, but NOT by Debug, setting to false. ")
			ENDIF
			
			IF NOT NET_GAMESERVER_TRANSACTION_IN_PROGRESS()
				NET_NL()NET_PRINT("[PCDELETE] RUN_OVERWRITE_FUNCTION called with Character = ")NET_PRINT_INT(Details.CharacterSlot)
				NET_NL()NET_PRINT("[PCDELETE] RUN_OVERWRITE_FUNCTION called with ClearBank = ")NET_PRINT_BOOL(ClearBank)
				IF NET_GAMESERVER_DELETE_CHARACTER(-1, ClearBank, ENUM_TO_INT(DeleteReason))
					NET_NL()NET_PRINT("[savemgr] [PCDELETE] NET_GAMESERVER_DELETE_CHARACTER = TRUE ")
					Details.Stages++
				ELSE
					NET_NL()NET_PRINT("[savemgr] [PCDELETE] RUN_OVERWRITE_FUNCTION = FALSE as NET_GAMESERVER_DELETE_CHARACTER = FALSE ")
					Details.Stages = 4
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[PCDELETE] RUN_OVERWRITE_FUNCTION NET_GAMESERVER_TRANSACTION_IN_PROGRESS = TRUE, wait ")
			ENDIF
		BREAK
	
		CASE 2
			SWITCH NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS()
				CASE TRANSACTION_STATUS_NONE
					NET_NL()NET_PRINT("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS = TRANSACTION_STATUS_NONE CharacterIndex = ")NET_PRINT_INT(Details.CharacterSlot)
				BREAK
				
				CASE TRANSACTION_STATUS_PENDING
					NET_NL()NET_PRINT("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS = TRANSACTION_STATUS_PENDING CharacterIndex = ")NET_PRINT_INT(Details.CharacterSlot)
				BREAK
				
				CASE TRANSACTION_STATUS_FAILED
					NET_NL()NET_PRINT("[savemgr] [PCDELETE] NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS = TRANSACTION_STATUS_FAILED CharacterIndex = ")NET_PRINT_INT(Details.CharacterSlot)
					Details.Stages = 4
				BREAK
				
				CASE TRANSACTION_STATUS_SUCCESSFULL
					NET_NL()NET_PRINT("[savemgr] [PCDELETE] NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS = TRANSACTION_STATUS_SUCCESSFULL CharacterIndex = ")NET_PRINT_INT(Details.CharacterSlot)
					Details.Stages++
				BREAK
				
				CASE TRANSACTION_STATUS_CANCELED
					NET_NL()NET_PRINT("[PCDELETE] NET_GAMESERVER_DELETE_CHARACTER_GET_STATUS = TRANSACTION_STATUS_CANCELED CharacterIndex = ")NET_PRINT_INT(Details.CharacterSlot)
					Details.Stages = 4
				BREAK

			ENDSWITCH
		BREAK
		
		CASE 3		
			NET_NL()NET_PRINT("[PCDELETE] RUN_OVERWRITE_FUNCTION calling RUN_OVERWRITE ")

			IF ClearBank
			AND DeleteReason != DELETE_REASON_CTRL_H_USER_DEBUG_ACTION
				ClearBank = FALSE
				NET_NL()NET_PRINT("[PCDELETE] RUN_OVERWRITE_FUNCTION ***** CLEAR BANK WAS TRUE, but NOT by Debug, setting to false. ")
			ENDIF
			
			RUN_OVERWRITE(ClearBank)
			Details.Successful = TRUE
			RETURN TRUE
		BREAK
		
		CASE 4
			
			IF NOT IS_PC_VERSION()
				NET_NL()NET_PRINT("[PCDELETE] RUN_OVERWRITE_FUNCTION This is NOT the PC Version. Carry on with the overwrite. ")
				STAT_CLEAR_DIRTY_READ_DETECTED()	
				Details.Stages = 3
				RETURN FALSE
			ENDIF
			
			NET_NL()NET_PRINT("[PCDELETE] RUN_OVERWRITE_FUNCTION Can't process, quitting out. ")
			Details.Successful = FALSE
			RETURN TRUE
		BREAK
		
		
		
	ENDSWITCH
	RETURN FALSE

ENDFUNC



FUNC BOOL CLEAR_ALL_CHARACTER_QUICK(BOOL &bDeleteAllCharsQuick, INT &SlotCount, INT &StageCount, SAVE_OVERWRITE_DATA& Details, BOOL ClearBank)

		BOOL bInitSessionFail
		BOOL bSkipInitTimeout
		
		SWITCH G_DELETE_STAGES_QUICK

//			CASE 0
//				
//				IF bDeleteAllCharsQuick = TRUE
//					NET_NL()NET_PRINT("\CLEAR_ALL_CHARACTER_QUICK IF STAT_LOAD(0)")
//					IF NOT STAT_SAVE_PENDING()
//						IF STAT_LOAD_PENDING(0) = FALSE
//							IF STAT_SLOT_IS_LOADED(0)= FALSE
//								STAT_LOAD(0)
//								IF STAT_LOAD_PENDING(0)
//								OR STAT_SLOT_IS_LOADED(0) 
//									G_DELETE_STAGES_QUICK++
//								ENDIF
//							ELSE
//								G_DELETE_STAGES_QUICK++
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//
//			BREAK
//			
//			CASE 1
//				NET_NL()NET_PRINT("CLEAR_ALL_CHARACTER_QUICK IF STAT_SLOT_IS_LOADED(0) = TRUE")
//				IF STAT_SLOT_IS_LOADED(0) = TRUE
//					G_DELETE_STAGES_QUICK++
//				ENDIF
//			
//			BREAK

			CASE 0
				IF bDeleteAllCharsQuick = TRUE	
				
					NET_NL()NET_PRINT("[PCDELETE] CLEAR_ALL_CHARACTER_QUICK CLEAR_ALL_CHARACTER_QUICK_INDEX = ")NET_PRINT_INT(G_CLEAR_ALL_CHARACTER_QUICK_INDEX)

					
					G_DELETE_STAGES_QUICK++
				ENDIF
			BREAK
			
			CASE 1
				IF STAT_LOAD_PENDING() = FALSE
				
					//Added for Miguel 1966154
					NET_NL()NET_PRINT("[PCDELETE] CLEAR_ALL_CHARACTER_QUICK STAT_CLEAR_DIRTY_READ_DETECTED() - called ")
					
					STAT_CLEAR_DIRTY_READ_DETECTED()
					G_DELETE_STAGES_QUICK++
				ELSE
					NET_NL()NET_PRINT("[PCDELETE] CLEAR_ALL_CHARACTER_QUICK SLOT STAT_LOAD_PENDING = ")NET_PRINT_INT(G_CLEAR_ALL_CHARACTER_QUICK_INDEX)NET_PRINT("\n")
				ENDIF
				
			BREAK
			
			CASE 2
				IF IS_PC_VERSION()
					//Do init session
					NET_NL()NET_PRINT("[PCDELETE] CLEAR_ALL_CHARACTER_QUICK SLOT IS_PC_VERSION = TRUE ")
					
					IF RUN_INIT_SHOP_SESSION(bInitSessionFail, bSkipInitTimeout)
						 NET_NL()NET_PRINT("[PCDELETE] CLEAR_ALL_CHARACTER_QUICK SLOT RUN_INIT_SHOP_SESSION = TRUE ")
						G_DELETE_STAGES_QUICK++
					ENDIF
					IF G_DELETE_STAGES_QUICK = 2
						IF bInitSessionFail
							G_DELETE_STAGES_QUICK++
							 NET_NL()NET_PRINT("[PCDELETE] CLEAR_ALL_CHARACTER_QUICK SLOT RUN_INIT_SHOP_SESSION = bInitSessionFail ")
						ENDIF
					ENDIF
				ELSE
					NET_NL()NET_PRINT("[PCDELETE] CLEAR_ALL_CHARACTER_QUICK SLOT IS_PC_VERSION = FALSE ")
				
					G_DELETE_STAGES_QUICK++
				ENDIF
			BREAK

	
			CASE 3
				NET_NL()NET_PRINT("[PCDELETE] [SAVETRANS] CLEAR_ALL_CHARACTER_QUICK CASE 3 ")
				IF RUN_OVERWRITE_FUNCTION(Details, ClearBank, DELETE_REASON_CTRL_H_USER_DEBUG_ACTION)
					g_Private_Checked_Stats_Transfer_For_Transfer_Both = (MIGRATION_MAIN_NONE)
					NET_NL()NET_PRINT("[PCDELETE] [SAVETRANS] CLEAR_ALL_CHARACTER_QUICK called STAT_MIGRATE_CLEAR_FOR_RESTART() ")
					STAT_MIGRATE_CLEAR_FOR_RESTART()
					SlotCount = 0
					G_DELETE_STAGES_QUICK++
				
				ENDIF
				
			BREAK
			
			
			CASE 4
				
				NET_NL()NET_PRINT("[PCDELETE] CLEAR_ALL_CHARACTER_QUICK SLOT CLEAR_MP_SAVE_STRUCT_SLOT(")NET_PRINT_INT(SlotCount)NET_PRINT(") called with StageCount = ")NET_PRINT_INT(StageCount)NET_PRINT("\n")
				IF CLEAR_MP_SAVE_STRUCT_SLOT_IN_STAGES(SlotCount, StageCount)
					StageCount = 0
					SlotCount++
				ENDIF
				IF SlotCount = MAX_NUM_CHARACTER_SLOTS
				
					REQUEST_SAVE(SSR_REASON_CHARACHTER_UPDATE, STAT_SAVETYPE_DELETE_CHAR)
					G_DELETE_STAGES_QUICK++
					
				ENDIF
			
			BREAK
			
			CASE 5

//				IF Delete_All_Character_Headshot()
				IF Delete_Character_Headshot(0)
					NET_NL()NET_PRINT("CLEAR_ALL_CHARACTER_QUICK Delete_Character_Headshot(0) = TRUE ")

					G_DELETE_STAGES_QUICK++
				ENDIF

			
			BREAK
			
			CASE 6
				IF Delete_Character_Headshot(1)
					NET_NL()NET_PRINT("CLEAR_ALL_CHARACTER_QUICK Delete_Character_Headshot(1) = TRUE ")
					G_DELETE_STAGES_QUICK++
				ENDIF
			BREAK
			
			CASE 7
//				IF Delete_Character_Headshot(2)
					NET_NL()NET_PRINT("CLEAR_ALL_CHARACTER_QUICK Delete_Character_Headshot(2) = TRUE ")

					G_DELETE_STAGES_QUICK++
//				ENDIF
			BREAK
			
			CASE 8
//				IF Delete_Character_Headshot(3)
					NET_NL()NET_PRINT("CLEAR_ALL_CHARACTER_QUICK Delete_Character_Headshot(3) = TRUE ")

					G_DELETE_STAGES_QUICK++
//				ENDIF
			BREAK
			
			
			CASE 9
//				IF Delete_Character_Headshot(4)
					NET_NL()NET_PRINT("CLEAR_ALL_CHARACTER_QUICK Delete_Character_Headshot(4) = TRUE ")

					G_DELETE_STAGES_QUICK++
//				ENDIF
			BREAK
			
			
			CASE 10
				NET_NL()NET_PRINT("[PCDELETE] CLEAR_ALL_CHARACTER_QUICK WAITING FOR SAVE TO FINISH = 0 ")
				IF NOT STAT_SAVE_PENDING()
					NET_NL()NET_PRINT("[PCDELETE] CTRL+H END SAVE = ALL ")
					
					bDeleteAllCharsQuick = FALSE
					ALLOW_EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
					EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
					
					NET_NL()NET_PRINT("\nEND SAVE = ")NET_PRINT_INT(G_CLEAR_ALL_CHARACTER_QUICK_INDEX)NET_PRINT("\n\n")
					G_CLEAR_ALL_CHARACTER_QUICK_INDEX = 0
					G_DELETE_STAGES_QUICK = 0
					
					SAVE_OVERWRITE_DATA Empty_Details
					Details = Empty_Details
					
					
					RETURN TRUE
					
//					G_CLEAR_ALL_CHARACTER_QUICK_INDEX = 1
//					G_DELETE_STAGES_QUICK++
				ENDIF
			
			BREAK



		ENDSWITCH
		
	

	RETURN FALSE
	
ENDFUNC






//FUNC BOOL HAS_A_REAL_HOUR_PASSED_HIGHEARNER_CHECK()
//
//
//
//	STRUCT_STAT_DATE currentdate
//	GET_POSIX_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)
//		
//	STRUCT_STAT_DATE LastCheck_Stat_date
//	LastCheck_Stat_date = GET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK)
//	
//	
////	#IF IS_DEBUG_BUILD
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Year = ")NET_PRINT_INT(currentdate.Year)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Month = ")NET_PRINT_INT(currentdate.Month)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Day = ")NET_PRINT_INT(currentdate.Day)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Hour = ")NET_PRINT_INT(currentdate.Hour)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Minute = ")NET_PRINT_INT(currentdate.Minute)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Seconds = ")NET_PRINT_INT(currentdate.Seconds)
////	
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Year = ")NET_PRINT_INT(LastCheck_Stat_date.Year)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Month = ")NET_PRINT_INT(LastCheck_Stat_date.Month)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Day = ")NET_PRINT_INT(LastCheck_Stat_date.Day)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Hour = ")NET_PRINT_INT(LastCheck_Stat_date.Hour)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Minute = ")NET_PRINT_INT(LastCheck_Stat_date.Minute)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Seconds = ")NET_PRINT_INT(LastCheck_Stat_date.Seconds)
////	#ENDIF
//
//	#IF IS_DEBUG_BUILD
//		
//		IF g_bForcePlayerPlayedAnHour
//			
//			SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//			g_bForcePlayerPlayedAnHour = FALSE
//			RETURN TRUE
//		ENDIF
//		
//	#ENDIF
//
//	IF IS_DATE_EMPTY(LastCheck_Stat_date)
////		NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: IS_DATE_EMPTY = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN FALSE
//	ENDIF
//	
//	IF LastCheck_Stat_date.Year < currentdate.Year
//			NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED_HIGHEARNER_CHECK: Years are Less = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN TRUE
//	ENDIF
//	
//	IF LastCheck_Stat_date.Month < currentdate.Month
//		NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED_HIGHEARNER_CHECK: Months are Less = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN TRUE
//	ENDIF 
//	
//
//
//	IF (LastCheck_Stat_date.Day < currentdate.Day) //Same day
//		NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED_HIGHEARNER_CHECK: Days are less = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN TRUE
//	ENDIF
//	
//	IF (currentdate.Hour-LastCheck_Stat_date.Hour) > 1
//		NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED_HIGHEARNER_CHECK: Hours are more than 1 = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN TRUE
//	ENDIF
//	
//	IF (currentdate.Hour-LastCheck_Stat_date.Hour) = 1
//	AND (LastCheck_Stat_date.Minute < currentdate.Minute) 
//		NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED_HIGHEARNER_CHECK Exactly one hour passed = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN TRUE
//	ENDIF
//	
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED = FALSE")
//	RETURN FALSE
//
//	
//
//ENDFUNC



//
//
//FUNC BOOL HAS_A_REAL_HOUR_PASSED()
//
//	STRUCT_STAT_DATE currentdate
//	GET_POSIX_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)
//		
//	STRUCT_STAT_DATE LastCheck_Stat_date
//	LastCheck_Stat_date = GET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK)
//	
//	
////	#IF IS_DEBUG_BUILD
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Year = ")NET_PRINT_INT(currentdate.Year)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Month = ")NET_PRINT_INT(currentdate.Month)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Day = ")NET_PRINT_INT(currentdate.Day)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Hour = ")NET_PRINT_INT(currentdate.Hour)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Minute = ")NET_PRINT_INT(currentdate.Minute)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: currentdate.Seconds = ")NET_PRINT_INT(currentdate.Seconds)
////	
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Year = ")NET_PRINT_INT(LastCheck_Stat_date.Year)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Month = ")NET_PRINT_INT(LastCheck_Stat_date.Month)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Day = ")NET_PRINT_INT(LastCheck_Stat_date.Day)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Hour = ")NET_PRINT_INT(LastCheck_Stat_date.Hour)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Minute = ")NET_PRINT_INT(LastCheck_Stat_date.Minute)
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: LastCheck_Stat_date.Seconds = ")NET_PRINT_INT(LastCheck_Stat_date.Seconds)
////	#ENDIF
//
//	#IF IS_DEBUG_BUILD
//		
//		IF g_bForcePlayerPlayedAnHour
//			
//			SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//			g_bForcePlayerPlayedAnHour = FALSE
//			RETURN TRUE
//		ENDIF
//		
//	#ENDIF
//
//	IF IS_DATE_EMPTY(LastCheck_Stat_date)
////		NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: IS_DATE_EMPTY = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN FALSE
//	ENDIF
//	
//	IF LastCheck_Stat_date.Year < currentdate.Year
//			NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: Years are Less = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN TRUE
//	ENDIF
//	
//	IF LastCheck_Stat_date.Month < currentdate.Month
//		NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: Months are Less = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN TRUE
//	ENDIF 
//	
//
//
//	IF (LastCheck_Stat_date.Day < currentdate.Day) //Same day
//		NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: Days are less = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN TRUE
//	ENDIF
//	
//	IF (currentdate.Hour-LastCheck_Stat_date.Hour) > 1
//		NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED: Hours are more than 1 = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN TRUE
//	ENDIF
//	
//	IF (currentdate.Hour-LastCheck_Stat_date.Hour) = 1
//	AND (LastCheck_Stat_date.Minute < currentdate.Minute) 
//		NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED Exactly one hour passed = TRUE")
//		SET_MP_DATE_PLAYER_STAT(MPPLY_HIGHEARNER_CHECK, currentdate)
//		RETURN TRUE
//	ENDIF
//	
////	NET_NL()NET_PRINT("HAS_A_REAL_HOUR_PASSED = FALSE")
//	RETURN FALSE
//
//ENDFUNC





//PROC PROCESS_PLAYER_A_HIGH_EARNER()
//
////	IF HAS_A_REAL_HOUR_PASSED_HIGHEARNER_CHECK()
////		INT CashEarnt_in_hour = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_EARNED)-GET_MP_INT_PLAYER_STAT(MPPLY_CASH_EARNT_IN_HOUR)
////		SET_MP_INT_PLAYER_STAT(MPPLY_CASH_EARNT_IN_HOUR, CashEarnt_in_hour)
////		IF CashEarnt_in_hour > 10000
////			SET_MP_BOOL_PLAYER_STAT(MPPLY_IS_HIGH_EARNER, TRUE)
////		ENDIF
////	
////	ENDIF
//
//	RETURN NETWORK_GET_PLAYER_IS_HIGH_EARNER()
//
//
//ENDPROC


/*FUNC BOOL CAN_GIVE_CASH_TO_PLAYER(INT iAmount, GAMER_HANDLE thisGamer)



	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableGiveCashBlock")
		NET_NL()NET_PRINT("CAN_GIVE_CASH_TO_PLAYER - sc_DisableGiveCashBlock = TRUE can transfer money ")
	
		RETURN TRUE
	ENDIF
	#ENDIF
	
	
	IF SC_GAMERDATA_GET_BOOL("bIsHighEarner")
		NET_NL()NET_PRINT("CAN_GIVE_CASH_TO_PLAYER - bIsHighEarner = TRUE so can't transfer money ")
	
		RETURN FALSE
	ENDIF
	
	IF iAmount > 0
		IF NETWORK_GET_CAN_TRANSFER_CASH(iAmount) = FALSE
			NET_NL()NET_PRINT("CAN_GIVE_CASH_TO_PLAYER - NETWORK_GET_CAN_TRANSFER_CASH = FALSE so can't transfer money ")
			RETURN FALSE
		ENDIF
		
		INT iMGiveAmountPL_EVC 	= 0
		INT iMGiveAmountPL_PVC 	= 0
		FLOAT fMGiveAmountPL_USDE = 0.0
		
		IF NOT NETWORK_CHECK_GIVE_PLAYER_CASH(iAmount, thisGamer, iMGiveAmountPL_EVC, iMGiveAmountPL_PVC, fMGiveAmountPL_USDE, FALSE, TRUE)
			NET_NL()NET_PRINT("CAN_GIVE_CASH_TO_PLAYER - NETWORK_CHECK_GIVE_PLAYER_CASH = FALSE so can't transfer money ")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF HAS_IMPORTANT_STATS_LOADED()
		IF NETWORK_GET_PLAYER_IS_HIGH_EARNER()
			NET_NL()NET_PRINT("CAN_GIVE_CASH_TO_PLAYER - NETWORK_GET_PLAYER_IS_HIGH_EARNER = TRUE  ")


			INT TotalEarned = GET_MP_INT_PLAYER_STAT(MPPLY_TOTAL_EVC)
			FLOAT Percentage = (TO_FLOAT(GET_MP_INT_PLAYER_STAT(MPPLY_INGAMESTORE_MONEYSPENT))/ TO_FLOAT(TotalEarned)*100)
			
			NET_NL()NET_PRINT("CAN_GIVE_CASH_TO_PLAYER - TotalEarned = ")NET_PRINT_INT(TotalEarned)
			NET_NL()NET_PRINT("CAN_GIVE_CASH_TO_PLAYER - GET_MP_INT_PLAYER_STAT(MPPLY_INGAMESTORE_MONEYSPENT) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_INGAMESTORE_MONEYSPENT))
			NET_NL()NET_PRINT("CAN_GIVE_CASH_TO_PLAYER - g_sMPTunables.fCashSpent_to_Give_percent = ")NET_PRINT_FLOAT(g_sMPTunables.fCashSpent_to_Give_percent)
			NET_NL()NET_PRINT("CAN_GIVE_CASH_TO_PLAYER - Percentage = ")NET_PRINT_FLOAT(Percentage)
			
			IF (Percentage < g_sMPTunables.fCashSpent_to_Give_percent)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE

ENDFUNC*/







