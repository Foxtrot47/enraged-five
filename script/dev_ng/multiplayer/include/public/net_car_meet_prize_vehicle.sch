//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_CAR_MEET_PRIZE_VEHICLE.sch																			//
// Description: Header file containing functionality for the Car Meet prize vehicle that can be won via challenges.		//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		02/04/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_TUNER
USING "website_public.sch"
USING "rc_helper_functions.sch"
USING "net_car_meet_prize_vehicle_vars.sch"
USING "net_car_club_rep.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CLEANUP ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Deletes the Car Meet Prize Vehicle.
/// PARAMS:
///    Vehicle - Vehicle to delete.
PROC DELETE_CAR_MEET_PRIZE_VEHICLE(VEHICLE_INDEX &Vehicle)
	
	IF IS_ENTITY_ALIVE(Vehicle)
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(Vehicle)
		IF IS_ENTITY_ATTACHED(Vehicle)
			DETACH_ENTITY(Vehicle)
		ENDIF
		
		DELETE_VEHICLE(Vehicle)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Cleans up the prize vehicle and slam truck.
/// PARAMS:
///    PrizeVehicleData - Data to set.
PROC CLEANUP_CAR_MEET_PRIZE_VEHICLE(CAR_MEET_PRIZE_VEHICLE_STRUCT &PrizeVehicleData)
	
	DELETE_CAR_MEET_PRIZE_VEHICLE(PrizeVehicleData.vehPrizeVehicle)
	DELETE_CAR_MEET_PRIZE_VEHICLE(PrizeVehicleData.vehSlamtruck)
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INITIALISE ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Checks if the players Car Meet Prize Vehicle challenge streak is intact.
PROC INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK()
	
	// Challenges:
	// 0) Place top X in an LS Car Meet Series for X days in a row.
	// 1) Place top X in the Street Race Series for X days in a row.
	// 2) Place top X in the Pursuit Series for X days in a row.
	// 3) Win X Sprint Races for X days in a row.
	// 4) Place top X in X LS Car Meet Series.
	// 5) Place top X in X Street Race Series.
	// 6) Place top X in X Pursuit Series.
	// 7) Win X Sprint Races.
	
	// We only care about challenges 0-3: Those that include day streaks.
	IF (g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID > MAX_PRIZE_VEHICLE_DAY_STREAK_CHALLENGE_ID)
		EXIT
	ENDIF
	
	// If progress is 0, there is no day streak to check.
	IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS) = 0)
		EXIT
	ENDIF
	
	// Get saved posix time as TIMEOFDAY
	UGC_DATE lastSavedProgressDate
	CONVERT_POSIX_TIME(GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_POXIS), lastSavedProgressDate)
	
	TIMEOFDAY lastSavedProgressTimeOfDay
	SET_TIMEOFDAY(lastSavedProgressTimeOfDay, lastSavedProgressDate.nSecond, lastSavedProgressDate.nMinute, lastSavedProgressDate.nHour, 
				  lastSavedProgressDate.nDay, INT_TO_ENUM(MONTH_OF_YEAR, lastSavedProgressDate.nMonth-1), lastSavedProgressDate.nYear)
	
	// Get current posix time as TIMEOFDAY
	UGC_DATE currentDate
	CONVERT_POSIX_TIME(GET_CLOUD_TIME_AS_INT(), currentDate)
	
	TIMEOFDAY currentTimeOfDay
	SET_TIMEOFDAY(currentTimeOfDay, currentDate.nSecond, currentDate.nMinute, currentDate.nHour, 
				  currentDate.nDay, INT_TO_ENUM(MONTH_OF_YEAR, currentDate.nMonth-1), currentDate.nYear)
	
	// Get the next streak break date
	INT iDaysInMonth 					= GET_NUMBER_OF_DAYS_IN_MONTH(GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay), GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay))
	INT iNextStreakBreakDay 			= GET_TIMEOFDAY_DAY(lastSavedProgressTimeOfDay)+2
	MONTH_OF_YEAR eNextStreakBreakMonth	= GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay)
	INT iNextStreakBreakYear			= GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay)
	
	IF (iNextStreakBreakDay > iDaysInMonth)
		iNextStreakBreakDay -= iDaysInMonth
		
		INT iNextStreakBreakMonth = ENUM_TO_INT(eNextStreakBreakMonth)+1
		IF iNextStreakBreakMonth > ENUM_TO_INT(DECEMBER)
			iNextStreakBreakMonth = ENUM_TO_INT(JANUARY)
			iNextStreakBreakYear++
		ENDIF
		eNextStreakBreakMonth = INT_TO_ENUM(MONTH_OF_YEAR, iNextStreakBreakMonth)
	ENDIF
	
	// Populate TIMEOFDAY with next streak break date
	TIMEOFDAY nextStreakBreakTimeOfDay
	SET_TIMEOFDAY(nextStreakBreakTimeOfDay, 1, 0, 0, iNextStreakBreakDay, eNextStreakBreakMonth, iNextStreakBreakYear)
	
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - Streak Break Day: ", GET_TIMEOFDAY_DAY(nextStreakBreakTimeOfDay))
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - Streak Break Month: ", GET_TIMEOFDAY_MONTH(nextStreakBreakTimeOfDay))
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - Streak Break Year: ", GET_TIMEOFDAY_YEAR(nextStreakBreakTimeOfDay))
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - ")
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - Last Saved Day: ", GET_TIMEOFDAY_DAY(lastSavedProgressTimeOfDay))
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - Last Saved Month: ", GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay))
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - Last Saved Year: ", GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay))
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - ")
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - Current Day: ", GET_TIMEOFDAY_DAY(currentTimeOfDay))
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - Current Month: ", GET_TIMEOFDAY_MONTH(currentTimeOfDay))
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - Current Year: ", GET_TIMEOFDAY_YEAR(currentTimeOfDay))
	
	// Check if we are on or have surpassed the next streak break date
	IF GET_TIMEOFDAY_YEAR(currentTimeOfDay) 	= GET_TIMEOFDAY_YEAR(nextStreakBreakTimeOfDay)
	AND GET_TIMEOFDAY_MONTH(currentTimeOfDay) 	= GET_TIMEOFDAY_MONTH(nextStreakBreakTimeOfDay)
	AND GET_TIMEOFDAY_DAY(currentTimeOfDay) 	>= GET_TIMEOFDAY_DAY(nextStreakBreakTimeOfDay)
		PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - Streak has been broken! Resetting progress")
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS,	0)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHL_MR_PRGRSS, 0)
	ELSE
		PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK - Streak is still intact")
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Initialises the Car Meet Prize Vehicle data.
///    Checks if the prize vehicle has been updated and resets challenge progress stats.
PROC INITIALISE_CAR_MEET_PRIZE_VEHICLE_DATA()
	INITIALISE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_STREAK()
	
	// New prize vehicle
	IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_ID) != g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_ITERATION)
		
		PRINTLN("[CAR_MEET_PRIZE_VEH] INITIALISE_CAR_MEET_PRIZE_VEHICLE_DATA - New Prize Vehicle! Old Iteration: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_ID), " New Iteration: ", g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_ITERATION)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_ID, g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_ITERATION)
		
		// Set packed stat to display help text when next entering the Car Meet
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CAR_MEET_NEW_PRIZE_VEHICLE_HELP, TRUE)
		
		// Reset stats
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_CMPLT, FALSE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CLMED, FALSE)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS,	0)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHL_MR_PRGRSS, 0)
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ HELP TEXT ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Maintains the Car Meet Prize Vehicle help text.
/// PARAMS:
///    bDisplayedEntranceHelpText - Whether the entrance help text has been displayed.
///    bDisplayedClaimVehicleHelpText - Whether the claim vehicle help text has been triggered from the angled area.
///    bMembershipPurchased - Whether a Car Club Membership has been purchased.
PROC MAINTAIN_CAR_MEET_PRIZE_VEHICLE_HELP_TEXT(BOOL &bDisplayedEntranceHelpText, BOOL bMembershipPurchased)
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
	OR NOT IS_SCREEN_FADED_IN()
	OR IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		EXIT
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2161.692139, 1129.752197, -25.621124>>, <<-2161.682861, 1142.322510, -22.371359>>, 15.0)
		IF (NOT bMembershipPurchased)
			//Non-member (shown any time they're in the area above around the car - blip for the vehicle should be grey in help text):
			//"Purchase a Membership from Mimi ~BLIP_CAR_MEET_ORGANISER~ and complete the current LS Car Meet Challenge to win the prize vehicle ~HUD_COLOUR_GREY~~BLIP_PRIZE_CAR~."
			PRINT_HELP("CM_PCC_N_MEM")
		ELSE
			//Members (shown first 3 times they enter the area above if they haven't already completed the current challenge and can't claim the vehicle. Once per boot):
			//Complete the current LS Car Meet Challenge found in the Interaction Menu to win the prize vehicle ~BLIP_PRIZE_CAR~.
			IF (NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_CMPLT))
				
				INT iPrizeVehicleChallengeHelpTextCounter = GET_PACKED_STAT_INT(PACKED_MP_INT_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_HELP)
				IF (iPrizeVehicleChallengeHelpTextCounter < MAX_NUM_CAR_MEET_PRIZE_VEHICLE_DISPLAY_CHALLENGE_HELP_TEXT AND NOT g_bCarMeetPrizeVehicleChallengeHelpTextDisplayed)
					SET_PACKED_STAT_INT(PACKED_MP_INT_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_HELP, iPrizeVehicleChallengeHelpTextCounter+1)
					g_bCarMeetPrizeVehicleChallengeHelpTextDisplayed = TRUE
					PRINT_HELP("CM_PCC_CHL")
				ENDIF
				
			ELSE
				//Members (shown any time they're in the area above around the car and the prize car is available to claim because they've completed the challenge):
				//"You have completed the current LS Car Meet Challenge. Claim the prize vehicle ~BLIP_PRIZE_CAR~ via the LS Car Meet section of the Interaction Menu."
				IF (NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CLMED))
					PRINT_HELP("CM_PCC_CLM")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//Members (shown first time the player enters the car meet after a new prize car / challenge is available):
		//"A new prize vehicle ~RADAR_TRACE_PRIZE_CAR~ is available. Complete the LS Car Meet Challenge found in the Interaction Menu to win this."
		IF (GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CAR_MEET_NEW_PRIZE_VEHICLE_HELP) AND bMembershipPurchased)
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CAR_MEET_NEW_PRIZE_VEHICLE_HELP, FALSE)
			PRINT_HELP("CM_PCC_NEW")
		ENDIF
		
		//Members (shown any time they enter the car meet and the prize car is available to claim because they're completed the challenge):
		//"You have completed the current LS Car Meet Challenge. Claim the prize vehicle ~RADAR_TRACE_PRIZE_CAR~ via the LS Car Meet section of the Interaction Menu."
		IF (GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_CMPLT) AND NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CLMED) AND NOT bDisplayedEntranceHelpText AND bMembershipPurchased)
			bDisplayedEntranceHelpText = TRUE
			PRINT_HELP("CM_PCC_CLM")
		ENDIF
	ENDIF
	
ENDPROC

PROC PRINT_CAR_MET_PRIZE_VEHICLE_TICKER(STRING sVehicleDisplayName)
	BEGIN_TEXT_COMMAND_THEFEED_POST(sVehicleDisplayName)
	END_TEXT_COMMAND_THEFEED_POST_UNLOCK_TU("CM_PCC_UNL", 5, sVehicleDisplayName, TRUE)
ENDPROC

FUNC BOOL MAINTAIN_CAR_MEET_PRIZE_VEHICLE_FREEMODE_HELP_TEXT(BOOL bDisplayHelpText)
	
	IF (g_bCarMeetPrizeVehicleClaimVehicleHelpTextDisplayed) // Once per boot
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
	OR NOT bDisplayHelpText
	OR NETWORK_IS_ACTIVITY_SESSION()
	OR IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
	OR NOT g_bHas_Tunables_Been_set_at_least_once
		RETURN FALSE
	ENDIF
	
	IF (NOT g_bCarMeetPrizeVehicleClaimVehicleHelpTextDisplayed)
		IF (GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_CMPLT) AND NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CLMED))
			IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER(CHAR_LS_CAR_MEET_MPEMAIL, "CM_PCC_EML", EMAIL_UNLOCKED, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ, MPE_NO_REPLY_REQUIRED)
				PRINT_HELP("CM_PCC_HT")
				PRINT_CAR_MET_PRIZE_VEHICLE_TICKER(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_MODEL_HASH)))
				g_bCarMeetPrizeVehicleClaimVehicleHelpTextDisplayed = TRUE
			ENDIF
		ELSE
			g_bCarMeetPrizeVehicleClaimVehicleHelpTextDisplayed = TRUE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ TELEMETRY ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sends the car meet prize vehicle metric.
/// PARAMS:
///    iChallengeID - The challenge ID tunable int.
///    iVehicleHash - Hash of the prize vehicle.
PROC SEND_CAR_MEET_PRIZE_VEHICLE_METRIC(INT iChallengeID, INT iVehicleHash)
	PLAYSTATS_CARCLUB_PRIZE(iChallengeID, iVehicleHash)
	PRINTLN("[CAR_MEET_PRIZE_VEH] SEND_CAR_MEET_PRIZE_VEHICLE_METRIC - Sending metric")
ENDPROC

/// PURPOSE:
///    Sends the car meet prize vehicle challenge metric.
/// PARAMS:
///    iChallengeID - The challenge ID tunable int.
///    iProgress - Challenge Progress
///    iMemebershipTier - Membership tier of the player.
PROC SEND_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_METRIC(INT iChallengeID, INT iProgress, INT iMemebershipTier)
	
	INT iTarget = g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_TWO
	
	// Challenge 7 only has 1 param - "Win X Sprint Races."
	IF (iChallengeID = PRIZE_VEHICLE_CHALLENGE_ONE_PARAM_ID)
		iTarget = g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_ONE
	ENDIF
	
	PLAYSTATS_CARCLUB_CHALLENGE(iChallengeID, iProgress, iTarget, iMemebershipTier)
	PRINTLN("[CAR_MEET_PRIZE_VEH] SEND_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_METRIC - Sending metric")
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ PRIZE VEHICLE CHALLENGE ╞═══════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE_STRING(CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE eRaceType)
	STRING sRaceType = ""
	SWITCH eRaceType
		CASE CM_PVC_RACE_TYPE_INVALID				sRaceType = "CM_PVC_RACE_TYPE_INVALID"				BREAK
		CASE CM_PVC_RACE_TYPE_LS_CAR_MEET_SERIES	sRaceType = "CM_PVC_RACE_TYPE_LS_CAR_MEET_SERIES"	BREAK
		CASE CM_PVC_RACE_TYPE_STREET				sRaceType = "CM_PVC_RACE_TYPE_STREET"				BREAK
		CASE CM_PVC_RACE_TYPE_PURSUIT				sRaceType = "CM_PVC_RACE_TYPE_PURSUIT"				BREAK
		CASE CM_PVC_RACE_TYPE_SPRINT				sRaceType = "CM_PVC_RACE_TYPE_SPRINT"				BREAK
		CASE CM_PVC_RACE_TYPE_MAX					sRaceType = "CM_PVC_RACE_TYPE_MAX"					BREAK
	ENDSWITCH
	RETURN sRaceType
ENDFUNC
#ENDIF //IS_DEBUG_BUILD

PROC INCREASE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_PROGRESS(CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE eRaceType, BOOL bSavePosixTime = TRUE)
	UNUSED_PARAMETER(eRaceType)
	
	IF (bSavePosixTime)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_POXIS,	GET_CLOUD_TIME_AS_INT())
	ENDIF
	
	INT iProgress = GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS, iProgress+1)
	
	SEND_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_METRIC(g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID, iProgress+1, GET_LOCAL_PLAYERS_CAR_CLUB_REP_TIER())
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] INCREASE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_PROGRESS - Challenge ID: ", g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID, " Race Type: ", DEBUG_GET_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE_STRING(eRaceType), " New Progress: ", iProgress+1)
	#ENDIF
ENDPROC

PROC RESET_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_PROGRESS(CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE eRaceType)
	UNUSED_PARAMETER(eRaceType)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS,	0)
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] RESET_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_PROGRESS - Challenge ID: ", g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID, " Race Type: ", DEBUG_GET_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE_STRING(eRaceType), " Progress has been reset")
	#ENDIF
ENDPROC

FUNC BOOL HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED(INT iPosition, INT iTargetPosition, BOOL &bResetProgress)
	
	// Check position
	IF (iPosition <= iTargetPosition)
		
		// New streak has started
		IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS) = 0)
			RETURN TRUE
		ELSE
			// Get saved posix time as TIMEOFDAY
			UGC_DATE lastSavedProgressDate
			CONVERT_POSIX_TIME(GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_POXIS), lastSavedProgressDate)
			
			TIMEOFDAY lastSavedProgressTimeOfDay
			SET_TIMEOFDAY(lastSavedProgressTimeOfDay, lastSavedProgressDate.nSecond, lastSavedProgressDate.nMinute, lastSavedProgressDate.nHour, 
						  lastSavedProgressDate.nDay, INT_TO_ENUM(MONTH_OF_YEAR, lastSavedProgressDate.nMonth-1), lastSavedProgressDate.nYear)
			
			// Get current posix time as TIMEOFDAY
			UGC_DATE currentDate
			CONVERT_POSIX_TIME(GET_CLOUD_TIME_AS_INT(), currentDate)
			
			TIMEOFDAY currentTimeOfDay
			SET_TIMEOFDAY(currentTimeOfDay, currentDate.nSecond, currentDate.nMinute, currentDate.nHour, 
						  currentDate.nDay, INT_TO_ENUM(MONTH_OF_YEAR, currentDate.nMonth-1), currentDate.nYear)
			
			PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Last Saved Day: ", GET_TIMEOFDAY_DAY(lastSavedProgressTimeOfDay))
			PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Last Saved Month: ", GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay))
			PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Last Saved Year: ", GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay))
			PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - ")
			PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Current Day: ", GET_TIMEOFDAY_DAY(currentTimeOfDay))
			PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Current Month: ", GET_TIMEOFDAY_MONTH(currentTimeOfDay))
			PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Current Year: ", GET_TIMEOFDAY_YEAR(currentTimeOfDay))
			
			// Check if we have already updated streak for today
			IF GET_TIMEOFDAY_YEAR(currentTimeOfDay) 	= GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay)
			AND GET_TIMEOFDAY_MONTH(currentTimeOfDay) 	= GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay)
			AND GET_TIMEOFDAY_DAY(currentTimeOfDay) 	= GET_TIMEOFDAY_DAY(lastSavedProgressTimeOfDay)
				// Streak already updated today - exit
				PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Streak has already been updated today, exit")
				RETURN FALSE
			ENDIF
			
			// Get the next streak date
			INT iDaysInMonth 				= GET_NUMBER_OF_DAYS_IN_MONTH(GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay), GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay))
			INT iNextStreakDay 				= GET_TIMEOFDAY_DAY(lastSavedProgressTimeOfDay)+1
			MONTH_OF_YEAR eNextStreakMonth	= GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay)
			INT iNextStreakYear				= GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay)
			
			IF (iNextStreakDay > iDaysInMonth)
				iNextStreakDay = 1
				
				INT iNextStreakMonth = ENUM_TO_INT(eNextStreakMonth)+1
				IF iNextStreakMonth > ENUM_TO_INT(DECEMBER)
					iNextStreakMonth = ENUM_TO_INT(JANUARY)
					iNextStreakYear++
				ENDIF
				eNextStreakMonth = INT_TO_ENUM(MONTH_OF_YEAR, iNextStreakMonth)
			ENDIF
			
			// Populate TIMEOFDAY with next streak date
			TIMEOFDAY nextStreakTimeOfDay
			SET_TIMEOFDAY(nextStreakTimeOfDay, 0, 0, 0, iNextStreakDay, eNextStreakMonth, iNextStreakYear)
			
			PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Next Streak Day: ", GET_TIMEOFDAY_DAY(nextStreakTimeOfDay))
			PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Next Streak Month: ", GET_TIMEOFDAY_MONTH(nextStreakTimeOfDay))
			PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Next Streak Year: ", GET_TIMEOFDAY_YEAR(nextStreakTimeOfDay))
			
			// Check if we are on the next streak date
			IF GET_TIMEOFDAY_YEAR(currentTimeOfDay) 	= GET_TIMEOFDAY_YEAR(nextStreakTimeOfDay)
			AND GET_TIMEOFDAY_MONTH(currentTimeOfDay) 	= GET_TIMEOFDAY_MONTH(nextStreakTimeOfDay)
			AND GET_TIMEOFDAY_DAY(currentTimeOfDay) 	= GET_TIMEOFDAY_DAY(nextStreakTimeOfDay)
				// Streak continues
				PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Streak continues!")
				RETURN TRUE
			ELSE
				// Streak broken
				PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED - Streak is broken!")
				bResetProgress = TRUE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_PROGRESSION_UPDATED_TODAY()
	
	IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS) = 0)
		RETURN FALSE
	ELSE
		// Get saved posix time as TIMEOFDAY
		UGC_DATE lastSavedProgressDate
		CONVERT_POSIX_TIME(GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_POXIS), lastSavedProgressDate)
		
		TIMEOFDAY lastSavedProgressTimeOfDay
		SET_TIMEOFDAY(lastSavedProgressTimeOfDay, lastSavedProgressDate.nSecond, lastSavedProgressDate.nMinute, lastSavedProgressDate.nHour, 
					  lastSavedProgressDate.nDay, INT_TO_ENUM(MONTH_OF_YEAR, lastSavedProgressDate.nMonth-1), lastSavedProgressDate.nYear)
		
		// Get current posix time as TIMEOFDAY
		UGC_DATE currentDate
		CONVERT_POSIX_TIME(GET_CLOUD_TIME_AS_INT(), currentDate)
		
		TIMEOFDAY currentTimeOfDay
		SET_TIMEOFDAY(currentTimeOfDay, currentDate.nSecond, currentDate.nMinute, currentDate.nHour, 
					  currentDate.nDay, INT_TO_ENUM(MONTH_OF_YEAR, currentDate.nMonth-1), currentDate.nYear)
		
		// Check if we have already updated streak for today
		IF GET_TIMEOFDAY_YEAR(currentTimeOfDay) 	= GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay)
		AND GET_TIMEOFDAY_MONTH(currentTimeOfDay) 	= GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay)
		AND GET_TIMEOFDAY_DAY(currentTimeOfDay) 	= GET_TIMEOFDAY_DAY(lastSavedProgressTimeOfDay)
			// Streak already updated today - exit
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: For challenges 0-2
///    0) Place top X in an LS Car Meet Series for X days in a row.
///    1) Place top X in the Street Race Series for X days in a row.
///    2) Place top X in the Pursuit Series for X days in a row.
PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_PROGRESSION(CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE eRaceType, INT iPosition, INT iTargetPosition, BOOL &bResetProgress)
	
	IF HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_STREAK_CONTINUED(iPosition, iTargetPosition, bResetProgress)
		INCREASE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_PROGRESS(eRaceType)
		REQUEST_SAVE(SSR_REASON_CM_PROGRESSION_STREAK, STAT_SAVETYPE_IMMEDIATE_FLUSH)
	ELSE
		IF (bResetProgress)
			RESET_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_PROGRESS(eRaceType)
			REQUEST_SAVE(SSR_REASON_CM_PROGRESSION_STREAK, STAT_SAVETYPE_IMMEDIATE_FLUSH)
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] UPDATE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_PROGRESSION - Challenge ID: ", g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID, " Race Type: ", DEBUG_GET_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE_STRING(eRaceType), " Progress has already updated today - do nothing.")
			#ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: For challenge 3
///    3) Win X Sprint Races for X days in a row.
PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_MULTI_RACE_DAY_PROGRESSION(CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE eRaceType, INT iMultiRaceDayTarget, BOOL &bResetProgress)
	
	// Don't increase multi race progress stat if challenge progress has already updated today
	IF HAS_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_PROGRESSION_UPDATED_TODAY()
		EXIT
	ENDIF
	
	// Increase multi race day progress
	INT iMultiRaceDayProgress = GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHL_MR_PRGRSS)+1
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHL_MR_PRGRSS, iMultiRaceDayProgress)
	
	// Update challenge progression streak once daily race target has been met
	IF (iMultiRaceDayProgress >= iMultiRaceDayTarget)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHL_MR_PRGRSS, 0)
		UPDATE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_PROGRESSION(eRaceType, 1, 1, bResetProgress)
	ENDIF
	
ENDPROC

/// PURPOSE: For challenges 4-7
///    4) Place top X in X LS Car Meet Series.
///    5) Place top X in X Street Race Series.
///    6) Place top X in X Pursuit Series.
///    7) Win X Sprint Races.
PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_PLACE_TOP_PROGRESSION(CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE eRaceType, INT iPosition, INT iTargetPosition)

	IF (iPosition <= iTargetPosition)
		INCREASE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_PROGRESS(eRaceType, FALSE)
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] UPDATE_CAR_MEET_PRIZE_VEHICLE_PLACE_TOP_PROGRESSION - Challenge ID: ", g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID, " Race Type: ", DEBUG_GET_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE_STRING(eRaceType), " Position: ", iPosition, " Target Position: ", iTargetPosition, " Player did not finish in the top ", iTargetPosition, " places.")
		#ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Check completion state of the prize vehicle challenge
PROC CHECK_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_COMPLETION(INT iChallengeTarget)
	
	IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS) >= iChallengeTarget)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS, iChallengeTarget)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_CMPLT, TRUE)
		g_bCarMeetPrizeVehicleClaimVehicleHelpTextDisplayed = FALSE
		#IF IS_DEBUG_BUILD
		PRINTLN("[CAR_MEET_PRIZE_VEH][CHALLENGE] CHECK_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_COMPLETION - Setting challenge as complete! Challenge ID: ", g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID, " Progress: ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS), " Target: ", iChallengeTarget)
		#ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE(CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE eRaceType, INT iPosition)
	
	// Challenge already complete
	IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_CMPLT)
		EXIT
	ENDIF
	
	// 0) Place top X in an LS Car Meet Series for X days in a row.
	// 1) Place top X in the Street Race Series for X days in a row.
	// 2) Place top X in the Pursuit Series for X days in a row.
	// 3) Win X Sprint Races for X days in a row.
	// 4) Place top X in X LS Car Meet Series.
	// 5) Place top X in X Street Race Series.
	// 6) Place top X in X Pursuit Series.
	// 7) Win X Sprint Races.
	
	BOOL bResetProgress = FALSE
	
	SWITCH g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID
		CASE 0
			IF (eRaceType != CM_PVC_RACE_TYPE_LS_CAR_MEET_SERIES)
				EXIT
			ENDIF
			UPDATE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_PROGRESSION(eRaceType, iPosition, g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_ONE, bResetProgress)
			CHECK_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_COMPLETION(g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_TWO)
		BREAK
		CASE 1
			IF (eRaceType != CM_PVC_RACE_TYPE_STREET)
				EXIT
			ENDIF
			UPDATE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_PROGRESSION(eRaceType, iPosition, g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_ONE, bResetProgress)
			CHECK_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_COMPLETION(g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_TWO)
		BREAK
		CASE 2
			IF (eRaceType != CM_PVC_RACE_TYPE_PURSUIT)
				EXIT
			ENDIF
			UPDATE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_DAY_PROGRESSION(eRaceType, iPosition, g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_ONE, bResetProgress)
			CHECK_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_COMPLETION(g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_TWO)
		BREAK
		CASE 3
			IF (eRaceType != CM_PVC_RACE_TYPE_SPRINT)
				EXIT
			ENDIF
			UPDATE_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_MULTI_RACE_DAY_PROGRESSION(eRaceType, g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_ONE, bResetProgress)
			CHECK_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_COMPLETION(g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_TWO)
		BREAK
		CASE 4
			IF (eRaceType != CM_PVC_RACE_TYPE_LS_CAR_MEET_SERIES)
				EXIT
			ENDIF
			UPDATE_CAR_MEET_PRIZE_VEHICLE_PLACE_TOP_PROGRESSION(eRaceType, iPosition, g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_ONE)
			CHECK_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_COMPLETION(g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_TWO)
		BREAK
		CASE 5
			IF (eRaceType != CM_PVC_RACE_TYPE_STREET)
				EXIT
			ENDIF
			UPDATE_CAR_MEET_PRIZE_VEHICLE_PLACE_TOP_PROGRESSION(eRaceType, iPosition, g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_ONE)
			CHECK_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_COMPLETION(g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_TWO)
		BREAK
		CASE 6
			IF (eRaceType != CM_PVC_RACE_TYPE_PURSUIT)
				EXIT
			ENDIF
			UPDATE_CAR_MEET_PRIZE_VEHICLE_PLACE_TOP_PROGRESSION(eRaceType, iPosition, g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_ONE)
			CHECK_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_COMPLETION(g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_TWO)
		BREAK
		CASE 7
			IF (eRaceType != CM_PVC_RACE_TYPE_SPRINT)
				EXIT
			ENDIF
			UPDATE_CAR_MEET_PRIZE_VEHICLE_PLACE_TOP_PROGRESSION(eRaceType, iPosition, 1)
			CHECK_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_COMPLETION(g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_ONE)
		BREAK
	ENDSWITCH
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ PRIZE VEHICLE REWARD ╞═════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets the global flag to display the Car Meet Prize Vehicle garage storage menu. Used when claiming the prize vehicle.
/// PARAMS:
///    bDisplay - Set the display flag.
PROC DISPLAY_CAR_MEET_PRIZE_VEHICLE_GARAGE_STORAGE_MENU(BOOL bDisplay)
	IF (g_bCarMeetDisplayPrizeVehicleGarageStorageMenu != bDisplay)
		g_bCarMeetDisplayPrizeVehicleGarageStorageMenu = bDisplay
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the Car Meet Prize Vehicle garage storage menu should display.
///    Wrapper function for the global: g_bCarMeetDisplayPrizeVehicleGarageStorageMenu
/// RETURNS: TRUE if the menu should display. FALSE otherwise.
FUNC BOOL SHOULD_DISPLAY_CAR_MEET_PRIZE_VEHICLE_GARAGE_STORAGE_MENU()
	RETURN g_bCarMeetDisplayPrizeVehicleGarageStorageMenu
ENDFUNC

/// PURPOSE:
///    Checks if the prize vehicle has been accepted.
/// PARAMS:
///    RewardVehicleData - Data to query.
/// RETURNS: TRUE if the prize vehicle has been accepted. FALSE otherwise.
FUNC BOOL HAS_PLAYER_ACCEPTED_PRIZE_VEHICLE(CAR_MEET_PRIZE_VEHICLE_REWARD_STRUCT &RewardVehicleData)
	RETURN (RewardVehicleData.iControlState != 3)
ENDFUNC

/// PURPOSE:
///    Resets the CAR_MEET_PRIZE_VEHICLE_REWARD_STRUCT struct.
/// PARAMS:
///    RewardVehicleData - Struct to reset.
PROC RESET_CAR_MEET_PRIZE_VEHICLE_REWARD_STRUCT(CAR_MEET_PRIZE_VEHICLE_REWARD_STRUCT &RewardVehicleData)
	REPLACE_MP_VEH_OR_PROP_MENU blankReplaceVehicleStruct
	RewardVehicleData.mpsvReplaceVehicle = blankReplaceVehicleStruct
	RewardVehicleData.iResultSlot			= 0
	RewardVehicleData.iDisplaySlot			= 0
	RewardVehicleData.iControlState			= 0
	RewardVehicleData.iTransactionResult	= 0
ENDPROC

/// PURPOSE:
///    Gives the prize vehicle to the player.
/// PARAMS:
///    PrizeVehicleData - Data to set.
PROC CAR_MEET_GIVE_PLAYER_PRIZE_VEHICLE(CAR_MEET_PRIZE_VEHICLE_STRUCT &PrizeVehicleData)
	
	IF RUN_WIN_VEHICLE(PrizeVehicleData.vehPrizeVehicle, PrizeVehicleData.RewardVehicleData.mpsvReplaceVehicle, PrizeVehicleData.RewardVehicleData.iTransactionResult, PrizeVehicleData.RewardVehicleData.iResultSlot, PrizeVehicleData.RewardVehicleData.iDisplaySlot, PrizeVehicleData.RewardVehicleData.iControlState, DEFAULT, DEFAULT, DEFAULT, TRUE, SPVET_CAR_MEET_PRIZE)
		IF HAS_PLAYER_ACCEPTED_PRIZE_VEHICLE(PrizeVehicleData.RewardVehicleData)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CLMED, TRUE)
			SEND_CAR_MEET_PRIZE_VEHICLE_METRIC(g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID, g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_MODEL_HASH)
		ENDIF
		
		RESET_CAR_MEET_PRIZE_VEHICLE_REWARD_STRUCT(PrizeVehicleData.RewardVehicleData)
		DISPLAY_CAR_MEET_PRIZE_VEHICLE_GARAGE_STORAGE_MENU(FALSE)
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ PRIZE VEHICLE CREATION ╞════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Gets the model of the Slamtruck.
/// RETURNS: The model of the Slamtruck.
FUNC MODEL_NAMES GET_CAR_MEET_SLAMTRUCK_MODEL()
	RETURN SLAMTRUCK
ENDFUNC

/// PURPOSE:
///    Gets the model of the prize vehicle based on tunables.
/// RETURNS: The model of the prize vehicle.
FUNC MODEL_NAMES GET_CAR_MEET_PRIZE_VEHICLE_MODEL(#IF IS_DEBUG_BUILD CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData #ENDIF)
	#IF IS_DEBUG_BUILD
	IF (PrizeVehicleDebugData.bEnableDebug)
		RETURN INT_TO_ENUM(MODEL_NAMES, PrizeVehicleDebugData.PrizeVehicleCustomisation.iModelHash)
	ENDIF
	#ENDIF
	RETURN INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_MODEL_HASH)
ENDFUNC

/// PURPOSE:
///    Gets the spawn coords of the Slamtruck.
/// RETURNS: The spawn coords of the Slamtruck.
FUNC VECTOR GET_CAR_MEET_SLAMTRUCK_COORDS(BOOL bIsSandbox = FALSE)
	IF NOT bIsSandbox
		RETURN <<-2160.902, 1136.603, -25.017>>
	ELSE
		RETURN <<-2160.902, 1136.603, 28.0585>>
	ENDIF
ENDFUNC

/// PURPOSE:
///    Gets the spawn coords of the prize vehicle.
/// RETURNS: The spawn coords of the prize vehicle.
FUNC VECTOR GET_CAR_MEET_PRIZE_VEHICLE_COORDS(BOOL bIsSandbox = FALSE)
	// Same as the Slamtruck coords.
	// Attachment offset puts prize vehicle into place.
	RETURN GET_CAR_MEET_SLAMTRUCK_COORDS(bIsSandbox)
ENDFUNC

/// PURPOSE:
///    Gets the spawn rotation of the Slamtruck.
/// RETURNS: The spawn rotation of the Slamtruck.
FUNC VECTOR GET_CAR_MEET_SLAMTRUCK_ROTATION()
	RETURN <<0.0, 0.0, 315.0>>
ENDFUNC

/// PURPOSE:
///    Gets the spawn rotation of the prize vehicle.
/// RETURNS: The spawn rotation of the prize vehicle.
FUNC VECTOR GET_CAR_MEET_PRIZE_VEHICLE_ROTATION()
	RETURN <<0.0, 0.0, 135.0>>
ENDFUNC

/// PURPOSE:
///    Gets the prize vehicle attachment offset coords and rotation.
/// PARAMS:
///    eModel - Vehicle model.
///    vOffsetCoords - Attachment offset coords.
///    vOffsetRotation - Attachment offset rotation.
PROC GET_CAR_MEET_PRIZE_VEHICLE_ATTACHMENT_OFFSETS(MODEL_NAMES eModel, VECTOR &vOffsetCoords, VECTOR &vOffsetRotation)
	vOffsetCoords 		= <<g_fCarMeetPrizeVehicleCoordOffsetX, g_fCarMeetPrizeVehicleCoordOffsetY, g_fCarMeetPrizeVehicleCoordOffsetZ>>
	vOffsetRotation 	= <<g_fCarMeetPrizeVehicleRotationOffsetX, g_fCarMeetPrizeVehicleRotationOffsetY, g_fCarMeetPrizeVehicleRotationOffsetZ>>
	
	SWITCH eModel
		CASE MAMBA
			vOffsetCoords 		= <<0.000, -1.520, 0.655>>
			vOffsetRotation 	= <<-2.600, 0.000, 180.000>>
		BREAK
		CASE AUTARCH
			vOffsetCoords 		= <<0.000, -1.520, 0.760>>
			vOffsetRotation 	= <<-3.200, 0.000, 180.000>>
		BREAK
		CASE BAGGER
			vOffsetCoords 		= <<0.000, -1.520, 0.870>>
			vOffsetRotation 	= <<-4.500, 0.000, 180.000>>
		BREAK
		CASE BUFFALO
			vOffsetCoords 		= <<0.000, -1.520, 0.930>>
			vOffsetRotation 	= <<-4.075, 0.000, 180.000>>
		BREAK
		CASE WARRENER2
			vOffsetCoords 		= <<0.000, -1.520, 0.565>>
			vOffsetRotation 	= <<-3.300, 0.000, 180.000>>
		BREAK
		CASE ZR350
			vOffsetCoords 		= <<0.000, -1.520, 0.980>>
			vOffsetRotation 	= <<-2.700, 0.000, 180.000>>
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Set the Car Meet prize vehicle properties.
///    Locks the vehicles doors etc.
/// PARAMS:
///    Vehicle - Vehicle index to set.
PROC SET_CAR_MEET_VEHICLE_PROPERTIES(VEHICLE_INDEX &Vehicle)
	
	SET_ENTITY_INVINCIBLE(Vehicle, TRUE)
	SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(Vehicle, FALSE)
	SET_VEHICLE_FULLBEAM(Vehicle, FALSE)
	SET_VEHICLE_DOORS_LOCKED(Vehicle, VEHICLELOCK_CANNOT_ENTER)
    SET_ENTITY_HEALTH(Vehicle, 1000)
    SET_VEHICLE_ENGINE_HEALTH(Vehicle, 1000)
    SET_VEHICLE_PETROL_TANK_HEALTH(Vehicle, 1000)
	SET_VEHICLE_DIRT_LEVEL(Vehicle, 0.0)
    SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(Vehicle, TRUE)
	SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(Vehicle, TRUE)
	SET_ENTITY_CAN_BE_DAMAGED(Vehicle, FALSE)
	SET_VEHICLE_RADIO_ENABLED(Vehicle, FALSE)
	
ENDPROC

/// PURPOSE:
///    Sets the Slamtruck customisation properties.
/// PARAMS:
///    Vehicle - Vehicle index to set.
///    eModel - Vehicle model.
PROC SET_CAR_MEET_SLAMTRUCK_CUSTOMISATION(VEHICLE_INDEX &Vehicle, MODEL_NAMES eModel)
	
	VEHICLE_SETUP_STRUCT_MP sData
	CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(eModel), sData, TRUE)
	
	sData.VehicleSetup.eModel 						= eModel
	sData.VehicleSetup.tlPlateText 					= "LSC"
	sData.VehicleSetup.iColour1 					= 73
	sData.VehicleSetup.iColour2						= 112
	sData.VehicleSetup.iColourExtra1 				= 70
	sData.VehicleSetup.iColourExtra2 				= 0
	sData.iColour5 									= 1
	sData.iColour6 									= 132
	sData.iLivery2 									= 0
	sData.VehicleSetup.iWheelType 					= 5
	sData.VehicleSetup.iTyreR 						= 255
	sData.VehicleSetup.iTyreG 						= 255
	sData.VehicleSetup.iTyreB 						= 255
	sData.VehicleSetup.iNeonR 						= 222
	sData.VehicleSetup.iNeonG 						= 222
	sData.VehicleSetup.iNeonB 						= 255
	sData.VehicleSetup.iModIndex[MOD_BUMPER_F] 		= 5
	sData.VehicleSetup.iModIndex[MOD_SKIRT] 		= 1
	sData.VehicleSetup.iModIndex[MOD_GRILL] 		= 2
	sData.VehicleSetup.iModIndex[MOD_BONNET] 		= 3
	sData.VehicleSetup.iModIndex[MOD_WING_R]		= 1
	sData.VehicleSetup.iModIndex[MOD_WHEELS] 		= 19
	sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] 	= 4
	sData.VehicleSetup.iModIndex[MOD_LIVERY] 		= 12
	sData.VehicleSetup.iModVariation[0] 			= 1
	
	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
	
	SET_VEHICLE_SETUP_MP(Vehicle, sData, FALSE, TRUE, TRUE)
	
ENDPROC

/// PURPOSE:
///    Sets the Prize Vehicle customisation properties via tunables.
/// PARAMS:
///    Vehicle - Vehicle index to set.
///    eModel - Vehicle model.
PROC SET_CAR_MEET_PRIZE_VEHICLE_CUSTOMISATION(VEHICLE_INDEX &Vehicle, MODEL_NAMES eModel #IF IS_DEBUG_BUILD, CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData #ENDIF)
	
	VEHICLE_SETUP_STRUCT_MP sData
	CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(eModel), sData, TRUE)
	
	#IF IS_DEBUG_BUILD
	IF (PrizeVehicleDebugData.bEnableDebug)
		sData.VehicleSetup.iColour1 							= PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour1
		sData.VehicleSetup.iColour2 							= PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour2
		sData.VehicleSetup.iColourExtra1 						= PrizeVehicleDebugData.PrizeVehicleCustomisation.iExtraColour1
		sData.VehicleSetup.iColourExtra2 						= PrizeVehicleDebugData.PrizeVehicleCustomisation.iExtraColour2
		sData.iColour5											= PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour5
		sData.iColour6 											= PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour6
		sData.VehicleSetup.iModIndex[MOD_LIVERY] 				= PrizeVehicleDebugData.PrizeVehicleCustomisation.iLivery
		sData.VehicleSetup.iWheelType							= PrizeVehicleDebugData.PrizeVehicleCustomisation.iWheelType
		sData.VehicleSetup.iModIndex[MOD_WHEELS]				= PrizeVehicleDebugData.PrizeVehicleCustomisation.iWheelID
		sData.VehicleSetup.iWindowTintColour					= PrizeVehicleDebugData.PrizeVehicleCustomisation.iWindowTintColour
		sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] 	= PrizeVehicleDebugData.PrizeVehicleCustomisation.iLightsColour
	ELSE
	#ENDIF
	
	sData.VehicleSetup.iColour1 								= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_COLOUR_1
	sData.VehicleSetup.iColour2 								= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_COLOUR_2
	sData.VehicleSetup.iColourExtra1 							= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_EXTRA_COLOUR_1
	sData.VehicleSetup.iColourExtra2 							= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_EXTRA_COLOUR_2
	sData.iColour5												= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_COLOUR_5
	sData.iColour6 												= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_COLOUR_6
	sData.VehicleSetup.iModIndex[MOD_LIVERY] 					= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_LIVERY
	sData.VehicleSetup.iWheelType								= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_WHEEL_TYPE
	sData.VehicleSetup.iModIndex[MOD_WHEELS]					= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_WHEEL_ID
	sData.VehicleSetup.iWindowTintColour						= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_WINDOW_TINT_COLOUR
	sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] 		= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_LIGHTS_COLOUR
	
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	SET_VEHICLE_SETUP_MP(Vehicle, sData, FALSE, TRUE, TRUE)
	
ENDPROC

/// PURPOSE:
///    Creates the car meet Slamtruck vehicle.
///    This is the truck the prize vehicle sits on top of.
/// PARAMS:
///    Vehicle - Vehicle index to set.
///    eModel - Vehicle model.
///    vCoords - Coords to spawn vehicle.
///    vRotation - Rotation to spawn vehicle.
///    bIsSandbox - Whether we are in the sandbox interior.
/// RETURNS: TRUE when the Slamtruck vehicle has been created, FALSE otherwise.
FUNC BOOL CREATE_CAR_MEET_SLAMTRUCK_VEHICLE(VEHICLE_INDEX &Vehicle, MODEL_NAMES eModel, VECTOR vCoords, VECTOR vRotation, BOOL bIsSandbox)
	
	// Conditions to avoid spawning slamtruck
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN TRUE
	ENDIF
	
	// Create slamtruck for sandbox interior but not private car meet
	IF IS_LOCAL_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX()
		IF NOT bIsSandbox
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT REQUEST_LOAD_MODEL(eModel)
		RETURN FALSE
	ENDIF
	
	PRINTLN("[CAR_MEET_PRIZE_VEH] CREATE_CAR_MEET_SLAMTRUCK_VEHICLE - CREATING SLAMTRUCK VEHICLE AT " , vCoords)
	
	// Create vehicle
	IF NOT DOES_ENTITY_EXIST(Vehicle)
		Vehicle = CREATE_VEHICLE(eModel, vCoords, vRotation.z, FALSE, FALSE)
		
		// Set vehicle properties	
		SET_CAR_MEET_VEHICLE_PROPERTIES(Vehicle)
		SET_VEHICLE_ON_GROUND_PROPERLY(Vehicle)
		SET_CAR_MEET_SLAMTRUCK_CUSTOMISATION(Vehicle, eModel)
		
		// Needed to display the neon lights
		SET_VEHICLE_ENGINE_ON(Vehicle, TRUE, TRUE)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates the car meet prize vehicle.
/// PARAMS:
///    Vehicle - Vehicle index to set.
///    eModel - Vehicle model.
///    vCoords - Coords to spawn vehicle.
///    vRotation - Rotation to spawn vehicle.
///    bIsSandbox - Whether we are in the sandbox interior.
/// RETURNS: TRUE when the prize vehicle has been created, FALSE otherwise.
FUNC BOOL CREATE_CAR_MEET_PRIZE_VEHICLE(VEHICLE_INDEX &Vehicle, MODEL_NAMES eModel, VECTOR vCoords, VECTOR vRotation, BOOL bIsSandbox #IF IS_DEBUG_BUILD, CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData #ENDIF)
	
	// Conditions to avoid spawning prize vehicle
	IF g_sMPTunables.bCAR_MEET_PRIZE_VEHICLE_DISABLE_VEHICLE
	OR GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CLMED)
	OR NETWORK_IS_ACTIVITY_SESSION()
	#IF IS_DEBUG_BUILD
	OR PrizeVehicleDebugData.PrizeVehicleCustomisation.bBlockPrizeVehicle
	#ENDIF
		RETURN TRUE
	ENDIF
	
	// Create prize vehicle for sandbox interior but not private car meet
	IF IS_LOCAL_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX()
		IF NOT bIsSandbox
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT REQUEST_LOAD_MODEL(eModel)
		RETURN FALSE
	ENDIF
	
	PRINTLN("[CAR_MEET_PRIZE_VEH] CREATE_CAR_MEET_SLAMTRUCK_VEHICLE - CREATING PRIZE VEHICLE AT " , vCoords)
	
	// Create vehicle
	IF NOT DOES_ENTITY_EXIST(Vehicle)
		Vehicle = CREATE_VEHICLE(eModel, vCoords, vRotation.z, FALSE, FALSE)
		
		// Set vehicle properties
		SET_CAR_MEET_VEHICLE_PROPERTIES(Vehicle)
		SET_ENTITY_COLLISION(Vehicle, FALSE)
		
		// Set vehicle customisation
		SET_CAR_MEET_PRIZE_VEHICLE_CUSTOMISATION(Vehicle, eModel #IF IS_DEBUG_BUILD, PrizeVehicleDebugData #ENDIF)
		SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Attaches the prize vehicle to the Slamtruck.
/// PARAMS:
///    PrizeVehicleData - Data to set.
/// RETURNS: TRUE when the prize vehicle has been attached to the Slamtruck, FALSE otherwise.  
FUNC BOOL ATTACH_PRIZE_VEHICLE_TO_SLAMTRUCK(CAR_MEET_PRIZE_VEHICLE_STRUCT &PrizeVehicleData #IF IS_DEBUG_BUILD, CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData #ENDIF, BOOL bIsSandbox = FALSE)
	
	IF NOT IS_BIT_SET(PrizeVehicleData.iBS, BS_PRIZE_VEHICLE_DATA_SLAMTRUCK_CREATED)
	OR NOT IS_BIT_SET(PrizeVehicleData.iBS, BS_PRIZE_VEHICLE_DATA_PRIZE_VEHICLE_CREATED)
		RETURN FALSE
	ENDIF
	
	// No slamtruck or prize vehicle created
	IF NOT IS_ENTITY_ALIVE(PrizeVehicleData.vehSlamtruck)
	OR NOT IS_ENTITY_ALIVE(PrizeVehicleData.vehPrizeVehicle)
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(PrizeVehicleData.vehSlamtruck)
	AND IS_VEHICLE_DRIVEABLE(PrizeVehicleData.vehPrizeVehicle)
		IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(PrizeVehicleData.vehPrizeVehicle, PrizeVehicleData.vehSlamtruck)
			
			VECTOR vAttachmentOffsetCoords
			VECTOR vAttachmentOffsetRotation
			GET_CAR_MEET_PRIZE_VEHICLE_ATTACHMENT_OFFSETS(GET_CAR_MEET_PRIZE_VEHICLE_MODEL(#IF IS_DEBUG_BUILD PrizeVehicleDebugData #ENDIF), vAttachmentOffsetCoords, vAttachmentOffsetRotation)
			
			// Reset vehicle coords before attachment to avoid floating issue
			SET_ENTITY_COORDS_NO_OFFSET(PrizeVehicleData.vehPrizeVehicle, GET_CAR_MEET_PRIZE_VEHICLE_COORDS(bIsSandbox))
			SET_ENTITY_COORDS_NO_OFFSET(PrizeVehicleData.vehSlamtruck, GET_CAR_MEET_SLAMTRUCK_COORDS(bIsSandbox))
			
			ATTACH_ENTITY_TO_ENTITY(PrizeVehicleData.vehPrizeVehicle, PrizeVehicleData.vehSlamtruck, -1, vAttachmentOffsetCoords, vAttachmentOffsetRotation)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintains the creation of the Slamtruck and prize vehicle.
/// PARAMS:
///    PrizeVehicleData - Data to set.
PROC MAINTAIN_CAR_MEET_PRIZE_VEHICLE(CAR_MEET_PRIZE_VEHICLE_STRUCT &PrizeVehicleData #IF IS_DEBUG_BUILD, CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData #ENDIF, BOOL bIsSandbox = FALSE)

	PRINTLN("[CAR_MEET_PRIZE_VEH] MAINTAIN_CAR_MEET_PRIZE_VEHICLE - IS SANDBOX: ", bIsSandbox)
	
	// Create Slamtruck
	IF NOT IS_BIT_SET(PrizeVehicleData.iBS, BS_PRIZE_VEHICLE_DATA_SLAMTRUCK_CREATED)
		IF CREATE_CAR_MEET_SLAMTRUCK_VEHICLE(PrizeVehicleData.vehSlamtruck, GET_CAR_MEET_SLAMTRUCK_MODEL(), GET_CAR_MEET_SLAMTRUCK_COORDS(bIsSandbox), GET_CAR_MEET_SLAMTRUCK_ROTATION(), bIsSandbox)
			SET_BIT(PrizeVehicleData.iBS, BS_PRIZE_VEHICLE_DATA_SLAMTRUCK_CREATED)
		ENDIF
	ENDIF
	
	// Create Prize Vehicle
	IF NOT IS_BIT_SET(PrizeVehicleData.iBS, BS_PRIZE_VEHICLE_DATA_PRIZE_VEHICLE_CREATED)
		IF CREATE_CAR_MEET_PRIZE_VEHICLE(PrizeVehicleData.vehPrizeVehicle, GET_CAR_MEET_PRIZE_VEHICLE_MODEL(#IF IS_DEBUG_BUILD PrizeVehicleDebugData #ENDIF), GET_CAR_MEET_PRIZE_VEHICLE_COORDS(bIsSandbox), GET_CAR_MEET_PRIZE_VEHICLE_ROTATION(), bIsSandbox #IF IS_DEBUG_BUILD, PrizeVehicleDebugData #ENDIF)
			SET_BIT(PrizeVehicleData.iBS, BS_PRIZE_VEHICLE_DATA_PRIZE_VEHICLE_CREATED)
		ENDIF
	ENDIF
	
	// Attach Prize Vehicle To Slamtruck
	IF NOT IS_BIT_SET(PrizeVehicleData.iBS, BS_PRIZE_VEHICLE_DATA_ATTACH_PRIZE_VEHICLE_TO_SLAMTRUCK)
		IF ATTACH_PRIZE_VEHICLE_TO_SLAMTRUCK(PrizeVehicleData #IF IS_DEBUG_BUILD, PrizeVehicleDebugData #ENDIF, bIsSandbox)
			SET_BIT(PrizeVehicleData.iBS, BS_PRIZE_VEHICLE_DATA_ATTACH_PRIZE_VEHICLE_TO_SLAMTRUCK)
		ENDIF
	ENDIF
	
	// Give player the prize vehicle
	IF SHOULD_DISPLAY_CAR_MEET_PRIZE_VEHICLE_GARAGE_STORAGE_MENU()
		CAR_MEET_GIVE_PLAYER_PRIZE_VEHICLE(PrizeVehicleData)
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ DEBUG WIDGETS ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC INITIALISE_CAR_MEET_PRIZE_VEHICLE_DEBUG_POSITIONS(CAR_MEET_VEHICLE_DEBUG_DATA &DebugData, VECTOR vCoords, VECTOR vRotation)
	DebugData.vCoords 			= vCoords
	DebugData.vCachedCoords 	= vCoords
	DebugData.vRotation			= vRotation
	DebugData.vCachedRotation	= vRotation
ENDPROC

PROC INITIALISE_CAR_MEET_PRIZE_VEHICLE_DEBUG_CUSTOMISATIONS(CAR_MEET_PRIZE_VEHICLE_CUSTOMISATION_DEBUG_DATA &DebugData)
	DebugData.bBlockPrizeVehicle	= g_sMPTunables.bCAR_MEET_PRIZE_VEHICLE_DISABLE_VEHICLE
	DebugData.iModelHash			= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_MODEL_HASH
	DebugData.iColour1				= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_COLOUR_1
	DebugData.iColour2				= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_COLOUR_2
	DebugData.iExtraColour1			= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_EXTRA_COLOUR_1
	DebugData.iExtraColour2			= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_EXTRA_COLOUR_2
	DebugData.iColour5				= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_COLOUR_5
	DebugData.iColour6				= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_COLOUR_6
	DebugData.iLivery				= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_LIVERY
	DebugData.iWheelType			= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_WHEEL_TYPE
	DebugData.iWheelID				= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_WHEEL_ID
	DebugData.iWindowTintColour		= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_WINDOW_TINT_COLOUR
	DebugData.iLightsColour			= g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_LIGHTS_COLOUR
ENDPROC

PROC INITIALISE_CAR_MEET_PRIZE_VEHICLE_DEBUG_DATA(CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData)
	INITIALISE_CAR_MEET_PRIZE_VEHICLE_DEBUG_POSITIONS(PrizeVehicleDebugData.SlamtruckData, GET_CAR_MEET_SLAMTRUCK_COORDS(), GET_CAR_MEET_SLAMTRUCK_ROTATION())
	INITIALISE_CAR_MEET_PRIZE_VEHICLE_DEBUG_POSITIONS(PrizeVehicleDebugData.PrizeVehicleData, GET_CAR_MEET_PRIZE_VEHICLE_COORDS(), GET_CAR_MEET_PRIZE_VEHICLE_ROTATION())
	
	GET_CAR_MEET_PRIZE_VEHICLE_ATTACHMENT_OFFSETS(GET_CAR_MEET_PRIZE_VEHICLE_MODEL(PrizeVehicleDebugData), PrizeVehicleDebugData.vAttachmentOffsetCoords, PrizeVehicleDebugData.vAttachmentOffsetRotation)
	
	INITIALISE_CAR_MEET_PRIZE_VEHICLE_DEBUG_CUSTOMISATIONS(PrizeVehicleDebugData.PrizeVehicleCustomisation)
	INITIALISE_CAR_MEET_PRIZE_VEHICLE_DEBUG_CUSTOMISATIONS(PrizeVehicleDebugData.CachedPrizeVehicleCustomisation)
ENDPROC

PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_POSITION(CAR_MEET_VEHICLE_DEBUG_DATA &DebugData, VEHICLE_INDEX &Vehicle)
	
	IF IS_ENTITY_ALIVE(Vehicle)
		IF IS_ENTITY_ATTACHED(Vehicle)
			DETACH_ENTITY(Vehicle)
		ENDIF
		
		FREEZE_ENTITY_POSITION(Vehicle, DebugData.bFreezeVehiclePosition)
		
		IF NOT ARE_VECTORS_EQUAL(DebugData.vCoords, DebugData.vCachedCoords)
			DebugData.vCachedCoords = DebugData.vCoords
			SET_ENTITY_COORDS_NO_OFFSET(Vehicle, DebugData.vCoords)
		ENDIF
		
		IF NOT ARE_VECTORS_EQUAL(DebugData.vRotation, DebugData.vCachedRotation)
			DebugData.vCachedRotation = DebugData.vRotation
			SET_ENTITY_ROTATION(Vehicle, DebugData.vRotation)
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_ATTACHMENT_OFFSET(CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData, CAR_MEET_PRIZE_VEHICLE_STRUCT &PrizeVehicleData)
	
	IF NOT IS_VEHICLE_DRIVEABLE(PrizeVehicleData.vehSlamtruck)
	OR NOT IS_VEHICLE_DRIVEABLE(PrizeVehicleData.vehPrizeVehicle)
		EXIT
	ENDIF
	
	IF NOT ARE_VECTORS_EQUAL(PrizeVehicleDebugData.vAttachmentOffsetCoords, PrizeVehicleDebugData.vCachedAttachmentOffsetCoords)
	OR NOT ARE_VECTORS_EQUAL(PrizeVehicleDebugData.vAttachmentOffsetRotation, PrizeVehicleDebugData.vCachedAttachmentOffsetRotation)
		PrizeVehicleDebugData.vCachedAttachmentOffsetCoords = PrizeVehicleDebugData.vAttachmentOffsetCoords
		PrizeVehicleDebugData.vCachedAttachmentOffsetRotation = PrizeVehicleDebugData.vAttachmentOffsetRotation
		
		IF IS_ENTITY_ATTACHED(PrizeVehicleData.vehPrizeVehicle)
			DETACH_ENTITY(PrizeVehicleData.vehPrizeVehicle)
		ENDIF
		
		ATTACH_ENTITY_TO_ENTITY(PrizeVehicleData.vehPrizeVehicle, PrizeVehicleData.vehSlamtruck, -1, PrizeVehicleDebugData.vAttachmentOffsetCoords, PrizeVehicleDebugData.vAttachmentOffsetRotation)
	ENDIF
		
ENDPROC

PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_CUSTOMISATION(CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData, CAR_MEET_PRIZE_VEHICLE_STRUCT &PrizeVehicleData)
	
	BOOL bUpdateVehicle = FALSE
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.bBlockPrizeVehicle != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.bBlockPrizeVehicle)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.bBlockPrizeVehicle = PrizeVehicleDebugData.PrizeVehicleCustomisation.bBlockPrizeVehicle
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iModelHash != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iModelHash)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iModelHash = PrizeVehicleDebugData.PrizeVehicleCustomisation.iModelHash
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour1 != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iColour1)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iColour1 = PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour1
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour2 != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iColour2)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iColour2 = PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour2
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iExtraColour1 != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iExtraColour1)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iExtraColour1 = PrizeVehicleDebugData.PrizeVehicleCustomisation.iExtraColour1
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iExtraColour2 != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iExtraColour2)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iExtraColour2 = PrizeVehicleDebugData.PrizeVehicleCustomisation.iExtraColour2
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour5 != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iColour5)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iColour5 = PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour5
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour6 != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iColour6)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iColour6 = PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour6
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iLivery != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iLivery)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iLivery = PrizeVehicleDebugData.PrizeVehicleCustomisation.iLivery
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iWheelType != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iWheelType)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iWheelType = PrizeVehicleDebugData.PrizeVehicleCustomisation.iWheelType
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iWheelID != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iWheelID)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iWheelID = PrizeVehicleDebugData.PrizeVehicleCustomisation.iWheelID
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iWindowTintColour != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iWindowTintColour)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iWindowTintColour = PrizeVehicleDebugData.PrizeVehicleCustomisation.iWindowTintColour
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (PrizeVehicleDebugData.PrizeVehicleCustomisation.iLightsColour != PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iLightsColour)
		PrizeVehicleDebugData.CachedPrizeVehicleCustomisation.iLightsColour = PrizeVehicleDebugData.PrizeVehicleCustomisation.iLightsColour
		bUpdateVehicle = TRUE
	ENDIF
	
	IF (bUpdateVehicle)
		DELETE_CAR_MEET_PRIZE_VEHICLE(PrizeVehicleData.vehPrizeVehicle)
		CLEAR_BIT(PrizeVehicleData.iBS, BS_PRIZE_VEHICLE_DATA_PRIZE_VEHICLE_CREATED)
		CLEAR_BIT(PrizeVehicleData.iBS, BS_PRIZE_VEHICLE_DATA_ATTACH_PRIZE_VEHICLE_TO_SLAMTRUCK)
	ENDIF
	
ENDPROC

PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_WARP_TO_VEHICLE(CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData)
	
	IF (PrizeVehicleDebugData.bWarpToPrizeVehicle)
		PrizeVehicleDebugData.bWarpToPrizeVehicle = FALSE
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), <<-2166.0815, 1142.7554, -24.3673>>)
			SET_ENTITY_ROTATION(PLAYER_PED_ID(), <<0.0, 0.0, 270.0>>)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_CHALLENGE(CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData)
	
	IF (PrizeVehicleDebugData.bSetChallengeAsComplete)
		PrizeVehicleDebugData.bSetChallengeAsComplete = FALSE
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_CMPLT, TRUE)
		g_bCarMeetPrizeVehicleClaimVehicleHelpTextDisplayed = FALSE
	ENDIF
	
	IF (PrizeVehicleDebugData.bSetChallengeAsIncomplete)
		PrizeVehicleDebugData.bSetChallengeAsIncomplete = FALSE
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_CMPLT, FALSE)
	ENDIF
	
	IF (PrizeVehicleDebugData.bUpdateChallengePIM)
		PrizeVehicleDebugData.bUpdateChallengePIM = FALSE
		
		IF (g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID != PrizeVehicleDebugData.iChallengeID)
			g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID = PrizeVehicleDebugData.iChallengeID
		ENDIF
		
		IF (g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_ONE != PrizeVehicleDebugData.iChallengeParamOne)
			g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_ONE = PrizeVehicleDebugData.iChallengeParamOne
		ENDIF
		
		IF (g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_TWO != PrizeVehicleDebugData.iChallengeParamTwo)
			g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_PARAM_TWO = PrizeVehicleDebugData.iChallengeParamTwo
		ENDIF
		
		IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS) != PrizeVehicleDebugData.iChallengeProgress)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_PRGRSS, PrizeVehicleDebugData.iChallengeProgress)
			SEND_CAR_MEET_PRIZE_VEHICLE_CHALLENGE_METRIC(g_sMPTunables.iCAR_MEET_PRIZE_VEHICLE_CHALLENGE_ID, PrizeVehicleDebugData.iChallengeProgress, GET_LOCAL_PLAYERS_CAR_CLUB_REP_TIER())
		ENDIF
		
		g_bPIM_ResetMenuNow = TRUE
	ENDIF
	
ENDPROC

PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_RESET_ITERATION_HELP_TEXT(CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData)
	
	IF (PrizeVehicleDebugData.bResetIterationHelpText)
		PrizeVehicleDebugData.bResetIterationHelpText = FALSE
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CAR_MEET_NEW_PRIZE_VEHICLE_HELP, TRUE)
	ENDIF
	
ENDPROC

PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_POSIX(CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData)
	
	IF PrizeVehicleDebugData.bOverridePosixTime
		PrizeVehicleDebugData.bOverridePosixTime = FALSE
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CARMEET_PV_CHLLGE_POXIS,	PrizeVehicleDebugData.iChallengePosixTime)
	ENDIF
	
ENDPROC

PROC CREATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_WIDGETS(CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData)
	INITIALISE_CAR_MEET_PRIZE_VEHICLE_DEBUG_DATA(PrizeVehicleDebugData)
	
	START_WIDGET_GROUP("Prize Vehicle")
		ADD_WIDGET_BOOL("Enable Debug", PrizeVehicleDebugData.bEnableDebug)
		ADD_WIDGET_BOOL("Warp To Prize Vehicle", PrizeVehicleDebugData.bWarpToPrizeVehicle)
		START_WIDGET_GROUP("Slamtruck")
			ADD_WIDGET_BOOL("Freeze Slam Truck position", PrizeVehicleDebugData.SlamtruckData.bFreezeVehiclePosition)
			ADD_WIDGET_VECTOR_SLIDER("Coords", PrizeVehicleDebugData.SlamtruckData.vCoords, -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Rotation", PrizeVehicleDebugData.SlamtruckData.vRotation, -360.9, 360.9, 0.1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Vehicle")
			START_WIDGET_GROUP("Placement")
				ADD_WIDGET_BOOL("Freeze Prize Vehicle position", PrizeVehicleDebugData.PrizeVehicleData.bFreezeVehiclePosition)
				ADD_WIDGET_VECTOR_SLIDER("Coords", PrizeVehicleDebugData.PrizeVehicleData.vCoords, -99999.9, 99999.9, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation", PrizeVehicleDebugData.PrizeVehicleData.vRotation, -360.9, 360.9, 0.1)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_VECTOR_SLIDER("Attachment Offset Coords", PrizeVehicleDebugData.vAttachmentOffsetCoords, -99999.9, 99999.9, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Attachment Offset Rotation", PrizeVehicleDebugData.vAttachmentOffsetRotation, -360.0, 360.0, 0.1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Customisation")
				ADD_WIDGET_BOOL("Block Prize Vehicle", PrizeVehicleDebugData.PrizeVehicleCustomisation.bBlockPrizeVehicle)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_INT_SLIDER("Model Hash", PrizeVehicleDebugData.PrizeVehicleCustomisation.iModelHash, LOWEST_INT, HIGHEST_INT, 1)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_INT_SLIDER("Colour 1", PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour1, 0, 256, 1)
				ADD_WIDGET_INT_SLIDER("Colour 2", PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour2, 0, 256, 1)
				ADD_WIDGET_INT_SLIDER("Extra Colour 1", PrizeVehicleDebugData.PrizeVehicleCustomisation.iExtraColour1, 0, 256, 1)
				ADD_WIDGET_INT_SLIDER("Extra Colour 2", PrizeVehicleDebugData.PrizeVehicleCustomisation.iExtraColour2, 0, 256, 1)
				ADD_WIDGET_INT_SLIDER("Colour 5", PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour5, 0, 256, 1)
				ADD_WIDGET_INT_SLIDER("Colour 6", PrizeVehicleDebugData.PrizeVehicleCustomisation.iColour6, 0, 256, 1)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_INT_SLIDER("Livery", PrizeVehicleDebugData.PrizeVehicleCustomisation.iLivery, -1, 256, 1)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_INT_SLIDER("Wheel Type", PrizeVehicleDebugData.PrizeVehicleCustomisation.iWheelType, 0, 256, 1)
				ADD_WIDGET_INT_SLIDER("Wheel ID", PrizeVehicleDebugData.PrizeVehicleCustomisation.iWheelID, 0, 256, 1)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_INT_SLIDER("Window Tint Colour", PrizeVehicleDebugData.PrizeVehicleCustomisation.iWindowTintColour, 0, 256, 1)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_INT_SLIDER("Lights Colour", PrizeVehicleDebugData.PrizeVehicleCustomisation.iLightsColour, 0, 256, 1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Iteration")
				ADD_WIDGET_BOOL("Reset Iteration Help Text", PrizeVehicleDebugData.bResetIterationHelpText)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Challenge")
			ADD_WIDGET_STRING("PIM Testing - Does Not Persist")
			ADD_WIDGET_INT_SLIDER("Challenge", PrizeVehicleDebugData.iChallengeID, 0, 7, 1)
			ADD_WIDGET_INT_SLIDER("Param One", PrizeVehicleDebugData.iChallengeParamOne, 0, 100, 1)
			ADD_WIDGET_INT_SLIDER("Param Two", PrizeVehicleDebugData.iChallengeParamTwo, 0, 100, 1)
			ADD_WIDGET_INT_SLIDER("Challenge Progress", PrizeVehicleDebugData.iChallengeProgress, 0, 100, 1)
			ADD_WIDGET_BOOL("Update PIM", PrizeVehicleDebugData.bUpdateChallengePIM)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Set Challenge as Complete", PrizeVehicleDebugData.bSetChallengeAsComplete)
			ADD_WIDGET_BOOL("Set Challenge as Incomplete", PrizeVehicleDebugData.bSetChallengeAsIncomplete)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Posix Time", PrizeVehicleDebugData.iChallengePosixTime, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_BOOL("Override Last Saved Progress Time", PrizeVehicleDebugData.bOverridePosixTime)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_WIDGETS(CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT &PrizeVehicleDebugData, CAR_MEET_PRIZE_VEHICLE_STRUCT &PrizeVehicleData)
	
	IF NOT PrizeVehicleDebugData.bEnableDebug
		EXIT
	ENDIF
	
	UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_POSITION(PrizeVehicleDebugData.SlamtruckData, PrizeVehicleData.vehSlamtruck)
	UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_POSITION(PrizeVehicleDebugData.PrizeVehicleData, PrizeVehicleData.vehPrizeVehicle)
	UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_ATTACHMENT_OFFSET(PrizeVehicleDebugData, PrizeVehicleData)
	UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_CUSTOMISATION(PrizeVehicleDebugData, PrizeVehicleData)
	UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_WARP_TO_VEHICLE(PrizeVehicleDebugData)
	UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_CHALLENGE(PrizeVehicleDebugData)
	UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_RESET_ITERATION_HELP_TEXT(PrizeVehicleDebugData)
	UPDATE_CAR_MEET_PRIZE_VEHICLE_DEBUG_POSIX(PrizeVehicleDebugData)
	
ENDPROC
#ENDIF	// IS_DEBUG_BUILD
#ENDIF	// FEATURE_TUNER
