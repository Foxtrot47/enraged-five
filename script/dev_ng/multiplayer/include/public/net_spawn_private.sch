USING "globals.sch"
USING "net_include.sch"
USING "net_mission.sch"
USING "net_nodes.sch"
USING "net_realty_details.sch"
USING "net_crew_updates.sch"
USING "net_missions_at_coords_public_checks.sch"
USING "mp_skycam.sch"
USING "cheat_handler.sch"
USING "net_spawn_areas.sch"
USING "net_common_functions.sch"
USING "net_spawn_activities.sch"
USING "net_transition_sessions.sch"
USING "net_ambience.sch"
USING "net_vehicle_setup.sch"
USING "net_spawn_debug.sch"

USING "net_private_yacht.sch"

USING "net_spawn_vehicle_mods.sch"

#IF FEATURE_HEIST_ISLAND
USING "mp_submarine_coords.sch"
#ENDIF

#IF FEATURE_GEN9_EXCLUSIVE
USING "net_mp_intro.sch"
#ENDIF

#IF ADD_WIDGETS_FOR_SPAWNING

#IF IS_DEBUG_BUILD
TWEAK_FLOAT CORONA_OFFSET_OVERRIDE -1.0
#ENDIF




TWEAK_INT MAX_NUM_OF_GET_SPAWN_LOCATION_ATTEMPTS 3


TWEAK_INT MAX_NUM_OF_FALLBACK_ATTEMPTS 1


TWEAK_INT LOAD_SCENE_TIME_OUT 7000
TWEAK_FLOAT RESPAWN_DO_LOAD_SCENE_DIST 30.0
#ENDIF	//	ADD_WIDGETS_FOR_SPAWNING

#IF NOT ADD_WIDGETS_FOR_SPAWNING

#IF IS_DEBUG_BUILD
CONST_FLOAT CORONA_OFFSET_OVERRIDE -1.0
#ENDIF


CONST_INT MAX_NUM_OF_GET_SPAWN_LOCATION_ATTEMPTS 3
CONST_INT MAX_NUM_OF_GET_SPAWN_LOCATION_NODE_SEARCHES 2

CONST_INT MAX_NUM_OF_FALLBACK_ATTEMPTS 1

CONST_INT MAX_ISPOINTOKFORSPAWNING_SCORE 8

CONST_INT LOAD_SCENE_TIME_OUT 7000
CONST_FLOAT RESPAWN_DO_LOAD_SCENE_DIST 30.0

#ENDIF	//	NOT ADD_WIDGETS_FOR_SPAWNING


CONST_FLOAT ENEMY_GANG_HOUSE_SPAWN_OCCLUSION_RADIUS 200.0

#IF ADD_WIDGETS_FOR_SPAWNING

TWEAK_FLOAT CORONA_OFFSET_DIST 6.0

TWEAK_FLOAT SPAWN_VIEWING_RANGE 50.0
TWEAK_FLOAT SPAWN_VIEWING_WIDTH 25.0
TWEAK_FLOAT SPAWN_VIEWING_MAX_Z_DIFF 5.0

TWEAK_FLOAT GANG_HOUSE_SPAWN_RADIUS 50.0
//TWEAK_FLOAT NEAR_TEAMMATES_MIN_SPAWN_RADIUS 25.0
TWEAK_FLOAT NEAR_TEAMMATES_SPAWN_RADIUS 50.0
TWEAK_FLOAT NEAR_OTHER_PLAYERS_SPAWN_RADIUS 50.0
TWEAK_FLOAT NEAR_OTHER_PLAYERS_MIN_SPAWN_RADIUS 25.0
TWEAK_FLOAT NEAR_CURRENT_POSITION_SPAWN_RADIUS 200.0
TWEAK_FLOAT NEAR_BACK_OF_POLICE_STATION_RADIUS 10.0

TWEAK_FLOAT SPAWNPOINT_SEARCH_MIN_RADIUS 2.0
TWEAK_INT SPAWNPOINT_SEARCH_FAIL_TIME 5000

TWEAK_FLOAT SAFE_RESPAWN_SEARCH_RADIUS 125.0

TWEAK_FLOAT PARTNER_SPAWN_DISTANCE 50.0

TWEAK_FLOAT GET_SAFE_COORD_FINAL_DISTANCE 40.0



// custom spawn point scoring
TWEAK_FLOAT CUSTOM_SPAWN_OPT_DIST_FROM_ENEMY 80.0
TWEAK_FLOAT CUSTOM_SPAWN_MAX_DIST_FROM_ENEMY 160.0

TWEAK_FLOAT CUSTOM_SPAWN_OPT_DIST_FROM_ENEMY_VEHICLE 160.0
TWEAK_FLOAT CUSTOM_SPAWN_MAX_DIST_FROM_ENEMY_VEHICLE 320.0

TWEAK_FLOAT CUSTOM_SPAWN_OPT_DIST_FROM_ENEMY_AIR_VEHICLE 320.0
TWEAK_FLOAT CUSTOM_SPAWN_MAX_DIST_FROM_ENEMY_AIR_VEHICLE 640.0

TWEAK_FLOAT CUSTOM_SPAWN_MAX_DIST_FROM_TEAMMATE 150.0

TWEAK_FLOAT CUSTOM_SPAWN_MAX_DIST_FROM_DEATH 40.0

// custom spawn point weighting
TWEAK_FLOAT CUSTOM_SPAWN_WEIGHTING_DIST_FROM_WARPERS 0.91
TWEAK_FLOAT CUSTOM_SPAWN_WEIGHTING_DIST_FROM_ENEMY 0.91
TWEAK_FLOAT CUSTOM_SPAWN_WEIGHTING_DIST_FROM_TEAMMATE 0.92
TWEAK_FLOAT CUSTOM_SPAWN_WEIGHTING_FROM_DEATH 0.93
TWEAK_FLOAT CUSTOM_SPAWN_WEIGHTING_ON_SCREEN		 0.94
TWEAK_FLOAT CUSTOM_SPAWN_WEIGHTING_DIST_FROM_HOSTILE_NPCS 0.95

TWEAK_FLOAT CAR_NODE_MIN_DISTANCE 5.0
TWEAK_FLOAT CAR_NODE_MAX_DISTANCE 30.0
TWEAK_FLOAT CAR_NODE_MAX_Z_DISTANCE 6.0

TWEAK_FLOAT SPAWN_MIN_DIST_FROM_PED_NEAR 20.0

TWEAK_INT LAST_SPAWN_ACTIVE_TIME 40000
TWEAK_FLOAT LAST_SPAWN_ACTIVE_RADIUS 65.0

TWEAK_FLOAT AREA_OCCUPIED_RADIUS 0.25

#ENDIF	//	ADD_WIDGETS_FOR_SPAWNING

#IF NOT ADD_WIDGETS_FOR_SPAWNING

CONST_FLOAT CORONA_OFFSET_DIST 6.0

CONST_FLOAT SPAWN_VIEWING_RANGE 50.0
CONST_FLOAT SPAWN_VIEWING_WIDTH 25.0
CONST_FLOAT SPAWN_VIEWING_MAX_Z_DIFF 5.0

CONST_FLOAT GANG_HOUSE_SPAWN_RADIUS 50.0
//CONST_FLOAT NEAR_TEAMMATES_MIN_SPAWN_RADIUS 25.0
CONST_FLOAT NEAR_TEAMMATES_SPAWN_RADIUS 50.0
CONST_FLOAT NEAR_OTHER_PLAYERS_SPAWN_RADIUS 50.0
CONST_FLOAT NEAR_OTHER_PLAYERS_MIN_SPAWN_RADIUS 25.0
CONST_FLOAT NEAR_CURRENT_POSITION_SPAWN_RADIUS 200.0
CONST_FLOAT NEAR_BACK_OF_POLICE_STATION_RADIUS 10.0

CONST_FLOAT SPAWNPOINT_SEARCH_MIN_RADIUS 2.0
CONST_INT SPAWNPOINT_SEARCH_FAIL_TIME 5000

CONST_FLOAT SAFE_RESPAWN_SEARCH_RADIUS 125.0

CONST_FLOAT PARTNER_SPAWN_DISTANCE 50.0

CONST_FLOAT GET_SAFE_COORD_FINAL_DISTANCE 40.0



// custom spawn point scoring
CONST_FLOAT CUSTOM_SPAWN_OPT_DIST_FROM_ENEMY 80.0
CONST_FLOAT CUSTOM_SPAWN_MAX_DIST_FROM_ENEMY 160.0

CONST_FLOAT CUSTOM_SPAWN_OPT_DIST_FROM_ENEMY_VEHICLE 160.0
CONST_FLOAT CUSTOM_SPAWN_MAX_DIST_FROM_ENEMY_VEHICLE 320.0

CONST_FLOAT CUSTOM_SPAWN_OPT_DIST_FROM_ENEMY_AIR_VEHICLE 320.0
CONST_FLOAT CUSTOM_SPAWN_MAX_DIST_FROM_ENEMY_AIR_VEHICLE 640.0

CONST_FLOAT CUSTOM_SPAWN_MAX_DIST_FROM_TEAMMATE 150.0

// custom spawn point weighting
CONST_FLOAT CUSTOM_SPAWN_WEIGHTING_DIST_FROM_WARPERS 0.91
CONST_FLOAT CUSTOM_SPAWN_WEIGHTING_DIST_FROM_ENEMY 0.91
CONST_FLOAT CUSTOM_SPAWN_WEIGHTING_DIST_FROM_TEAMMATE 0.92
CONST_FLOAT CUSTOM_SPAWN_WEIGHTING_FROM_DEATH 0.93
CONST_FLOAT CUSTOM_SPAWN_WEIGHTING_ON_SCREEN		 0.94
CONST_FLOAT CUSTOM_SPAWN_WEIGHTING_DIST_FROM_HOSTILE_NPCS 0.95
CONST_FLOAT CUSTOM_SPAWN_WEIGHTING_DIST_FROM_CURRENT_RESULTS 1.0

CONST_FLOAT CAR_NODE_MIN_DISTANCE 5.0
CONST_FLOAT CAR_NODE_MAX_DISTANCE 30.0
CONST_FLOAT CAR_NODE_MAX_Z_DISTANCE 6.0

CONST_FLOAT SPAWN_MIN_DIST_FROM_PED_NEAR 20.0
CONST_FLOAT SPAWN_MIN_DIST_FROM_PED_FAR 65.0

CONST_INT LAST_SPAWN_ACTIVE_TIME 40000
CONST_FLOAT LAST_SPAWN_ACTIVE_RADIUS 65.0

CONST_FLOAT AREA_OCCUPIED_RADIUS 0.25

#ENDIF	//	NOT ADD_WIDGETS_FOR_SPAWNING

CONST_INT SPAWN_LOCATION_STATE_INIT 					0
CONST_INT SPAWN_LOCATION_STATE_QUERY_START_SPAWN		1
CONST_INT SPAWN_LOCATION_STATE_FINAL_CHECKS				2
CONST_INT SPAWN_LOCATION_STATE_SEARCH_FOR_CAR_NODE		3
CONST_INT SPAWN_LOCATION_STATE_FIND_AIR_SPAWN			4
CONST_INT SPAWN_LOCATION_STATE_CHECK_INITIAL_RESULTS	5
CONST_INT SPAWN_LOCATION_STATE_DO_NEW_RESPAWN_SEARCH	6

CONST_INT RESPAWN_SEARCH_IDLE							0
CONST_INT RESPAWN_SEARCH_LOADING_CAR_NODES				1
CONST_INT RESPAWN_SEARCH_LOADING_NAV_MESH				2
CONST_INT RESPAWN_SEARCH_LOADING_CLOUD_MISSION_DATA		3
CONST_INT RESPAWN_SEARCH_START_SEARCH					4
CONST_INT RESPAWN_SEARCH_UPDATE_SEARCH					5
CONST_INT RESPAWN_SEARCH_START_FALLBACK_SEARCH			6
CONST_INT RESPAWN_SEARCH_UPDATE_FALLBACK_SEARCH 		7
CONST_INT RESPAWN_SEARCH_TIMEOUT_FALLBACK				8
CONST_INT RESPAWN_RETURNING_TRUE						9


CONST_INT WARP_TO_SPAWN_STATE_INIT 									0
CONST_INT WARP_TO_SPAWN_STATE_WAIT_FOR_RALLY_PARTNER_TO_INIT		1
CONST_INT WARP_TO_SPAWN_STATE_FIND_POINT 							2
CONST_INT WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE					3
CONST_INT WARP_TO_SPAWN_STATE_WARP_INTO_VEHICLE						4
CONST_INT WARP_TO_SPAWN_STATE_WAIT_TO_WARP_INTO_VEHICLE				5
CONST_INT WARP_TO_SPAWN_STATE_DO_NET_WARP							6
CONST_INT WARP_TO_SPAWN_STATE_POST_NET_WARP_CHECK					7
CONST_INT WARP_TO_SPAWN_STATE_WAIT_FOR_RALLY_FINISH_WARP			8
CONST_INT WARP_TO_SPAWN_STATE_WAIT_FOR_RALLY_PARTNER_TO_CLEANUP		9
CONST_INT WARP_TO_SPAWN_STATE_CHECK_GROUND_Z_FOR_RACE_CORONAS		10
CONST_INT WARP_TO_SPAWN_STATE_CLEANUP								11


CONST_INT CNC_SPAWN_DELAY 	0
CONST_INT FM_SPAWN_DELAY 	0
CONST_INT DM_SPAWN_DELAY 	0
CONST_INT RACE_SPAWN_DELAY 	0	//1500 - REMOVED FOR BUG 1605423

CONST_FLOAT CLEAR_AREA_RADIUS_VEHICLE 6.0 
CONST_FLOAT CLEAR_AREA_RADIUS_PED 0.7  

CONST_INT FALLBACK_Z_NEAR_DEATH							1
CONST_INT FALLBACK_ON_PAVEMENT							2
CONST_INT FALLBACK_NOT_VISIBLE_TO_TEAMMATES				4
CONST_INT FALLBACK_FAR_ENOUGH_FROM_PEDS_NEAR	 		8
CONST_INT FALLBACK_FAR_ENOUGH_FROM_PEDS_FAR		 		16
CONST_INT FALLBACK_FAR_ENOUGH_FROM_ENEMY_PLAYERS		32
CONST_INT FALLBACK_SCORE_OK_FOR_SPAWNING_NEAR	     	64 
CONST_INT FALLBACK_SCORE_OK_FOR_SPAWNING_FAR	     	128
CONST_INT FALLBACK_SCORE_PASS_AVOID_COORDS		   		256	
CONST_INT FALLBACK_SCORE_PASS_EXCLUSION_CHECKS   		512 
CONST_INT FALLBACK_SCORE_NOT_IN_GANG_ZONE		 		1024
CONST_INT FALLBACK_SCORE_AVOIDS_MISSION_CORONAS	 		2048
CONST_INT FALLBACK_SCORE_IN_SPAWNING_AREA		 		4096
CONST_INT FALLBACK_SCORE_IN_SAME_INTERIOR_GROUP	 		8192
CONST_INT FALLBACK_SCORE_IN_CORRECT_INTERIOR_STATE 		16384
CONST_INT FALLBACK_SCORE_NOT_IN_MISSION_BLOCKING_AREA	32768
CONST_INT FALLBACK_SCORE_NOT_IN_FORBIDDEN_AREA	 		65536
CONST_INT MAX_FALLBACK_SCORE					 		131071


CONST_FLOAT HIDE_Z_BOAT 	2500.0
CONST_FLOAT HIDE_Z_SPECIAL	1500.0 // Used for special vehicles: Blazer5 and Technical2
CONST_FLOAT HIDE_Z_CAR		-90.0
CONST_FLOAT ARENA_Z_CAR		92.0

CONST_FLOAT NAV_SEARCH_MIN -0.1
CONST_FLOAT NAV_SEARCH_MAX 0.1


#IF IS_DEBUG_BUILD
FUNC BOOL DEBUG_PRINT_SPAWN_AREA_DIFFERENCES(SPAWN_AREA &SPAreaStored, SPAWN_AREA &SPAreaNew)
	
	BOOL bReturn = FALSE
	
	IF (VDIST2(SPAreaStored.vCoords1, SPAreaNew.vCoords1) > 0.0)
		NET_PRINT("[spawning] SpawnArea new: vCoords1 = ") NET_PRINT_VECTOR(SPAreaNew.vCoords1) NET_NL()	
		bReturn = TRUE
	ENDIF
	IF (VDIST2(SPAreaStored.vCoords2, SPAreaNew.vCoords2) > 0.0)
		NET_PRINT("[spawning] SpawnArea new: vCoords2 = ") NET_PRINT_VECTOR(SPAreaNew.vCoords2) NET_NL()	
		bReturn = TRUE
	ENDIF
	IF (SPAreaStored.fFloat != SPAreaNew.fFloat)
		NET_PRINT("[spawning] SpawnArea new: fFloat = ") NET_PRINT_FLOAT(SPAreaNew.fFloat) NET_NL()	
		bReturn = TRUE
	ENDIF
	IF (SPAreaStored.fHeading != SPAreaNew.fHeading)
		NET_PRINT("[spawning] SpawnArea new: fHeading = ") NET_PRINT_FLOAT(SPAreaNew.fHeading) NET_NL()	
		bReturn = TRUE
	ENDIF
	IF (SPAreaStored.fIncreaseDist != SPAreaNew.fIncreaseDist)
		NET_PRINT("[spawning] SpawnArea new: fIncreaseDist = ") NET_PRINT_FLOAT(SPAreaNew.fIncreaseDist) NET_NL()	
		bReturn = TRUE
	ENDIF
	IF (SPAreaStored.bIsActive != SPAreaNew.bIsActive)
		NET_PRINT("[spawning] SpawnArea new: bIsActive = ") NET_PRINT_BOOL(SPAreaNew.bIsActive) NET_NL()	
		bReturn = TRUE
	ENDIF
	IF (SPAreaStored.iShape != SPAreaNew.iShape)
		NET_PRINT("[spawning] SpawnArea new: iShape = ") NET_PRINT_INT(SPAreaNew.iShape) NET_NL()	
		bReturn = TRUE
	ENDIF
	IF (SPAreaStored.bConsiderCentrePointAsValid != SPAreaNew.bConsiderCentrePointAsValid)
		NET_PRINT("[spawning] SpawnArea new: bConsiderCentrePointAsValid = ") NET_PRINT_BOOL(SPAreaNew.bConsiderCentrePointAsValid) NET_NL()	
		bReturn = TRUE
	ENDIF
	IF (SPAreaStored.bShow != SPAreaNew.bShow)
		NET_PRINT("[spawning] SpawnArea new: bShow = ") NET_PRINT_BOOL(SPAreaNew.bShow) NET_NL()	
		bReturn = TRUE
	ENDIF
	IF (SPAreaStored.bSetAroundPlayer != SPAreaNew.bSetAroundPlayer)
		NET_PRINT("[spawning] SpawnArea new: bSetAroundPlayer = ") NET_PRINT_BOOL(SPAreaNew.bSetAroundPlayer) NET_NL()	
		bReturn = TRUE
	ENDIF
	
	RETURN (bReturn)
	
ENDFUNC

//PROC DEBUG_PRINT_SPAWN_AREA_DETAILS(SPAWN_AREA &SPArea)
//	
//	NET_PRINT("[spawning] Spawn Area Details:") NET_NL()
//	NET_PRINT("[spawning] vPos = ") NET_PRINT_VECTOR(SPArea.vCoords1) NET_NL()
//	NET_PRINT("[spawning] vMaxPos = ") NET_PRINT_VECTOR(SPArea.vCoords2) NET_NL()
//	NET_PRINT("[spawning] fRadius = ") NET_PRINT_FLOAT(SPArea.fFloat) NET_NL()
//	NET_PRINT("[spawning] fIncreaseDist = ") NET_PRINT_FLOAT(SPArea.fIncreaseDist) NET_NL()
//	NET_PRINT("[spawning] bConsiderCentrePointAsValid = ") NET_PRINT_BOOL(SPArea.bConsiderCentrePointAsValid) NET_NL()
//	
//	SWITCH SPArea.iShape
//		CASE SPAWN_AREA_SHAPE_CIRCLE
//			NET_PRINT("[spawning] iShape = CIRCLE") NET_NL()
//		BREAK
//		CASE SPAWN_AREA_SHAPE_BOX
//			NET_PRINT("[spawning] iShape = BOX") NET_NL()
//		BREAK
//		CASE SPAWN_AREA_SHAPE_ANGLED
//			NET_PRINT("[spawning] iShape = ANGLED") NET_NL()
//		BREAK
//	ENDSWITCH
//	
//	NET_PRINT("[spawning] bIsActive = ") NET_PRINT_BOOL(SPArea.bIsActive) NET_NL()
//	
//
//	
//ENDPROC
PROC DEBUG_PRINT_SPAWN_SEARCH_PARAMS(SPAWN_SEARCH_PARAMS &Params)
	NET_PRINT("[spawning] Public Params:") NET_NL()
	NET_PRINT(" vFacingCoords=") NET_PRINT_VECTOR(Params.vFacingCoords) NET_NL()
	NET_PRINT(" bConsiderInteriors=") NET_PRINT_BOOL(Params.bConsiderInteriors) NET_NL()
	NET_PRINT(" bPreferPointsCloserToRoads=") NET_PRINT_BOOL(Params.bPreferPointsCloserToRoads) NET_NL()
	NET_PRINT(" fMinDistFromPlayer=") NET_PRINT_FLOAT(Params.fMinDistFromPlayer) NET_NL()
	NET_PRINT(" bCloseToOriginAsPossible=") NET_PRINT_BOOL(Params.bCloseToOriginAsPossible) NET_NL()
	NET_PRINT(" bConsiderOriginAsValidPoint=") NET_PRINT_BOOL(Params.bConsiderOriginAsValidPoint) NET_NL()
	NET_PRINT(" bSearchVehicleNodesOnly=") NET_PRINT_BOOL(Params.bSearchVehicleNodesOnly) NET_NL()
	NET_PRINT(" bUseOnlyBoatNodes=") NET_PRINT_BOOL(Params.bUseOnlyBoatNodes) NET_NL()
	NET_PRINT(" bEdgesOnly=") NET_PRINT_BOOL(Params.bEdgesOnly) NET_NL()
	NET_PRINT(" fHeadingForConsideredOrigin=") NET_PRINT_FLOAT(Params.fHeadingForConsideredOrigin) NET_NL()
	NET_PRINT(" fMaxZBelowRaw=") NET_PRINT_FLOAT(Params.fMaxZBelowRaw) NET_NL()
	INT i
	REPEAT MAX_NUM_AVOID_RADIUS i
		NET_PRINT(" vAvoidCoords[") NET_PRINT_INT(i) NET_PRINT("] = ") NET_PRINT_VECTOR(Params.vAvoidCoords[i]) NET_NL()
		NET_PRINT(" fAvoidRadius[") NET_PRINT_INT(i) NET_PRINT("] = ") NET_PRINT_FLOAT(Params.fAvoidRadius[i]) NET_NL()
	ENDREPEAT
	
	NET_PRINT(" vAvoidAngledAreaPos1=") NET_PRINT_VECTOR(Params.vAvoidAngledAreaPos1) NET_NL()
	NET_PRINT(" vAvoidAngledAreaPos2=") NET_PRINT_VECTOR(Params.vAvoidAngledAreaPos2) NET_NL()
	NET_PRINT(" fAvoidAngledAreaWidth=") NET_PRINT_FLOAT(Params.fAvoidAngledAreaWidth) NET_NL()
	
	NET_PRINT(" bIsForFlyingVehicle=") NET_PRINT_BOOL(Params.bIsForFlyingVehicle) NET_NL()
	NET_PRINT(" bUseOffRoadChecking=") NET_PRINT_BOOL(Params.bUseOffRoadChecking) NET_NL()
	NET_PRINT(" fUpperZLimitForNodes=") NET_PRINT_FLOAT(Params.fUpperZLimitForNodes) NET_NL()
	NET_PRINT(" bAvoidOtherPeds=") NET_PRINT_BOOL(Params.bAvoidOtherPeds) NET_NL()
ENDPROC

PROC DEBUG_PRINT_PRIVATE_SPAWN_SEARCH_PARAMS(PRIVATE_SPAWN_SEARCH_PARAMS &Params)
	NET_PRINT("[spawning] Private Params:") NET_NL()
	NET_PRINT(" vSearchCoord=") NET_PRINT_VECTOR(Params.vSearchCoord) NET_NL()
	NET_PRINT(" fRawHeading=") NET_PRINT_FLOAT(Params.fRawHeading) NET_NL()
	NET_PRINT(" fSearchRadius=") NET_PRINT_FLOAT(Params.fSearchRadius) NET_NL()
	NET_PRINT(" bIsForLocalPlayerSpawning=") NET_PRINT_BOOL(Params.bIsForLocalPlayerSpawning) NET_NL()
	NET_PRINT(" bDoTeamMateVisCheck=") NET_PRINT_BOOL(Params.bDoTeamMateVisCheck) NET_NL()
	NET_PRINT(" iAreaShape=") NET_PRINT_INT(Params.iAreaShape) NET_NL()
	NET_PRINT(" vAngledAreaPoint1=") NET_PRINT_VECTOR(Params.vAngledAreaPoint1) NET_NL()
	NET_PRINT(" vAngledAreaPoint2=") NET_PRINT_VECTOR(Params.vAngledAreaPoint2) NET_NL()
	NET_PRINT(" fAngledAreaWidth=") NET_PRINT_FLOAT(Params.fAngledAreaWidth) NET_NL()
	NET_PRINT(" bForAVehicle=") NET_PRINT_BOOL(Params.bForAVehicle) NET_NL()
	NET_PRINT(" bDoVisibleChecks=") NET_PRINT_BOOL(Params.bDoVisibleChecks) NET_NL()
	NET_PRINT(" bIgnoreTeammatesForMinDistCheck=") NET_PRINT_BOOL(Params.bIgnoreTeammatesForMinDistCheck) NET_NL()	
	NET_PRINT(" vOffsetOrigin=") NET_PRINT_VECTOR(Params.vOffsetOrigin) NET_NL()
	NET_PRINT(" bUseOffsetOrigin=") NET_PRINT_BOOL(Params.bUseOffsetOrigin) NET_NL()
	NET_PRINT(" bAllowFallbackToUseInteriors=") NET_PRINT_BOOL(Params.bAllowFallbackToUseInteriors) NET_NL()
	NET_PRINT(" iFallbackSpawnPointsDefaultResultFlag=") NET_PRINT_INT(Params.iFallbackSpawnPointsDefaultResultFlag) NET_NL()
ENDPROC
#ENDIF

FUNC INT GET_FIRST_PARTICIPANT_NUMBER_FROM_TEAM_NUMBER(INT iTeam)
	INT iPlayer
	PLAYER_INDEX tempPlayer
	INT iPartOnTeamLoop = -1
	BOOL bShowPrint = FALSE
	
	#IF IS_DEBUG_BUILD
		bShowPrint = GET_COMMANDLINE_PARAM_EXISTS("sc_ParticipantLoopPrints")
	#ENDIF
	
	FOR iPlayer = 0 TO (NUM_NETWORK_PLAYERS-1)
		
		IF bShowPrint
			PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] iTeam = ", iTeam, " iplayer = ", iPlayer, " iPartOnTeamLoop = ", iPartOnTeamLoop)
		ENDIF
		
		tempPlayer = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
			IF bShowPrint
				PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] player ok = ", iPlayer)
			ENDIF
			
			iPartOnTeamLoop++
			
			IF GlobalplayerBD_FM[iPlayer].sClientCoronaData.iTeamChosen = iTeam
				// Found a participant. Increment our number.
				RETURN iPartOnTeamLoop

				IF bShowPrint
					PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] iPartOnTeamLoop: ", iPartOnTeamLoop)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC INT GET_PARTICIPANT_NUMBER_IN_TEAM_PRIVATE() 

	INT iplayer,iplayerGBD
	INT iMyteam
	PLAYER_INDEX tempPlayer
	INT ireturn
	
	BOOL bShowPrint = FALSE
	
	#IF IS_DEBUG_BUILD
		bShowPrint = GET_COMMANDLINE_PARAM_EXISTS("sc_ParticipantLoopPrints")
	#ENDIF

	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		iMyteam = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen  
		PRINTLN("local player team = ",iMyteam)
	ELSE
		RETURN 0
	ENDIF
	
	FOR iplayer = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF bShowPrint
			PRINTLN("[RCC MISSION] iplayer = ",iplayer)
		ENDIF
		
		tempPlayer = INT_TO_PLAYERINDEX(iplayer)
		IF IS_NET_PLAYER_OK(tempPlayer,FALSE)
			
			IF bShowPrint
				PRINTLN("[RCC MISSION] player ok = ",iplayer)
			ENDIF
	       	IF tempPlayer!= PLAYER_ID()
				IF bShowPrint
					PRINTLN("[RCC MISSION] not local player ",iplayer)
				ENDIF
				
				IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer) 
				AND NOT IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(tempPlayer)
				AND NOT IS_PLAYER_SCTV(tempPlayer)
				AND NOT IS_PLAYER_SPECTATING(tempPlayer)
					iplayerGBD = NATIVE_TO_INT(tempPlayer)
					
					IF bShowPrint
						PRINTLN("[RCC MISSION] native to int on player id: ",iplayerGBD)
					ENDIF

					IF bShowPrint
						PRINTLN("other player team = ",GlobalplayerBD_FM[iplayerGBD].sClientCoronaData.iTeamChosen)
					ENDIF
					
					IF GlobalplayerBD_FM[iplayerGBD].sClientCoronaData.iTeamChosen = iMyteam
						ireturn++
						IF bShowPrint
							PRINTLN("[RCC MISSION] past team check for player: ",iplayerGBD," return value is now: ",ireturn)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bShowPrint
					PRINTLN("[RCC MISSION] Player is local player ",iplayer)
				ENDIF
				iplayer = NUM_NETWORK_PLAYERS
			ENDIF
		ENDIF
	ENDFOR

	PRINTLN("GET_PARTICIPANT_NUMBER_IN_TEAM_PRIVATE return value ",ireturn)
	RETURN ireturn
	
ENDFUNC

FUNC INT GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(INT iTeam, INT iPartOnTeam, BOOL bAlwaysExcludeSpectators = FALSE)
	INT iPlayer
	PLAYER_INDEX tempPlayer
	INT iPartOnTeamLoop = -1
	BOOL bShowPrint = FALSE
	
	#IF IS_DEBUG_BUILD
		bShowPrint = GET_COMMANDLINE_PARAM_EXISTS("sc_ParticipantLoopPrints")
	#ENDIF
	
	FOR iPlayer = 0 TO (NUM_NETWORK_PLAYERS-1)		
		tempPlayer = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
		AND NOT IS_PLAYER_SPECTATING(tempPlayer) OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(tempPlayer)].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE)
			IF(bAlwaysExcludeSpectators
			AND NOT IS_PLAYER_SPECTATING(tempPlayer)
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer) 
			AND NOT IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(tempPlayer))
			OR NOT bAlwaysExcludeSpectators
				IF bShowPrint
					PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] iTeam = ", iTeam, " iplayer = ", iPlayer, " iPartOnTeam = ", iPartOnTeam, " iPartOnTeamLoop = ", iPartOnTeamLoop)
				ENDIF
				
				IF bShowPrint
					PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] player ok = ", iPlayer)
				ENDIF
				
				IF GlobalplayerBD_FM[iPlayer].sClientCoronaData.iTeamChosen = iTeam
					// Found a participant. Increment our number.
					iPartOnTeamLoop++

					IF bShowPrint
						PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] iPartOnTeamLoop: ", iPartOnTeamLoop)
					ENDIF
				ENDIF		
			
				IF iPartOnTeam = iPartOnTeamLoop
					IF bShowPrint
						PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] Returning: ", iPlayer)
					ENDIF
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
						RETURN iPlayer
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] Error, player not found! Team: ", iTeam, " Player:", iPartOnTeam)
	RETURN -1
ENDFUNC

// This function gets the driver number for the player and team passed in (team part)
FUNC INT GET_DRIVER_PART_NUMBER_IN_TEAM_FROM_MY_PART_IN_TEAM(INT iPlayerTeam, INT iPartNumberInTeam)

	INT iRem = iPartNumberInTeam % g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[iPlayerTeam]
	INT iDriverPart = iPartNumberinTeam - iRem

	RETURN iDriverPart

ENDFUNC

FUNC INT GET_PLAYER_INDEX_NUMBER_FROM_PARTICIPANT_INDEX_NUMBER(INT iPart)
	IF iPart > -1
		INT iPlayer	
		PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))	
		iPlayer = NATIVE_TO_INT(piPlayer)	
		RETURN iPlayer
	ENDIF
	
	PRINTLN("[LM][GET_PLAYER_INDEX_NUMBER_FROM_PARTICIPANT_INDEX_NUMBER] - Someone used this wrong...")
	RETURN -1
ENDFUNC

PROC SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(INT iSeat)
	PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE - Set to prefer seat: ", iSeat)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnSeatPreference = iSeat
ENDPROC

FUNC INT GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE()
	RETURN GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnSeatPreference
ENDFUNC

PROC SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(INT iIndex, INT iPart)
	PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX - Slot iIndex: ", iIndex, " has been set with Partner part: ", iPart)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[iIndex] = iPart
ENDPROC

FUNC INT GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(INT iIndex)
	RETURN GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[iIndex]
ENDFUNC

PROC SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(INT iPart)
	PRINTLN("[LM][MULTI_VEH_RESPAWN] - SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER - iPart: ", iPart)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartnerDriverPointer = iPart
ENDPROC

FUNC INT GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()
	RETURN GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartnerDriverPointer
ENDFUNC

FUNC BOOL IS_DRIVER_IN_LIST(INT &iDrivers[], INT iPart)
	INT i
	FOR i = 0 TO 5
		IF iDrivers[i] = iPart
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

// This function retrieves the true participant number of the Local Players designated team driver.
FUNC INT GET_DRIVER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER()
	BOOL bShowPrint = FALSE
	
	#IF IS_DEBUG_BUILD
		bShowPrint = GET_COMMANDLINE_PARAM_EXISTS("sc_ParticipantLoopPrints")
	#ENDIF
	
	IF PARTICIPANT_ID_TO_INT() > -1
		INT iPlayerTeam = GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].sClientCoronaData.iTeamChosen
		INT iPartNumberInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM_PRIVATE()
		INT iDriverPart = -1
		
		IF bShowPrint
			PRINTLN("[RCC MISSION][GET_DRIVER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER] iPlayerTeam: ", iPlayerTeam)
		ENDIF
		
		IF iPlayerTeam > -1

			IF bShowPrint
				PRINTLN("[RCC MISSION][GET_DRIVER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER] iPartNumberInTeam: ", iPartNumberInTeam)
			ENDIF
			
			iDriverPart = GET_DRIVER_PART_NUMBER_IN_TEAM_FROM_MY_PART_IN_TEAM(iPlayerTeam, iPartNumberInTeam)
			
			IF bShowPrint
				PRINTLN("[RCC MISSION][GET_DRIVER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER] iDriverPart in team: ", iDriverPart)
			ENDIF
			
			iDriverPart = GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(iPlayerTeam, iDriverPart)
		ENDIF

		IF bShowPrint
			PRINTLN("[RCC MISSION][GET_DRIVER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER] Returning iDriverPart: ", iDriverPart)
		ENDIF
		
		RETURN iDriverPart
	ENDIF

	IF bShowPrint
		PRINTLN("[RCC MISSION][GET_DRIVER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER] Returning -1")	
	ENDIF
		
	RETURN -1
ENDFUNC

FUNC INT GET_PASSENGER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER(INT iFromIndex)
	BOOL bShowPrint = FALSE
	
	#IF IS_DEBUG_BUILD
		bShowPrint = GET_COMMANDLINE_PARAM_EXISTS("sc_ParticipantLoopPrints")
	#ENDIF
	
	IF PARTICIPANT_ID_TO_INT() > -1
		INT iPlayerTeam = GlobalplayerBD_FM[PARTICIPANT_ID_TO_INT()].sClientCoronaData.iTeamChosen
		INT iPartNumberInTeam = iFromIndex
		INT iPassengerPart
		PLAYER_INDEX tempPlayer
		
		IF bShowPrint
			PRINTLN("[RCC MISSION][GET_PASSENGER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER] starting from iPartNumberInTeam: ", iPartNumberInTeam)
		ENDIF
		
		IF iPlayerTeam > -1
			iPassengerPart = GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(iPlayerTeam, iPartNumberInTeam)
			
			IF iPassengerPart > -1
				tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPassengerPart))
				
				IF IS_NET_PLAYER_OK(tempPlayer, TRUE)
				
					IF bShowPrint
						PRINTLN("[RCC MISSION][GET_PASSENGER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER] iPassengerPart: ", iPassengerPart, " is Okay, returning...")
					ENDIF
					RETURN iPassengerPart
				ELSE
					IF bShowPrint
						PRINTLN("[RCC MISSION][GET_PASSENGER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER] iPassengerPart: ", iPassengerPart, " Not Okay")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bShowPrint
		PRINTLN("[RCC MISSION][GET_PASSENGER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER] Returning -1")
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_SPAWNING_IN_VEHICLE()
	IF ((g_SpawnData.MissionSpawnDetails.bSpawnInVehicle) AND NOT (g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle))
		IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
			RETURN(TRUE)
		ELSE
			PRINTLN("[spawning] IS_SPAWNING_IN_VEHICLE - player is spectating.") 
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_SPAWN_LOCATION_MISSION_AREA_CHECK(PLAYER_SPAWN_LOCATION SpawnLocation)
	IF (SpawnLocation = SPAWN_LOCATION_MISSION_AREA)
	OR (SpawnLocation = SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL CanEditThisVehicle(VEHICLE_INDEX CarID, BOOL &bSetAsMissionEntity)

	// is this a personal vehicle?
	IF CAN_EDIT_THIS_ENTITY(CarID, bSetAsMissionEntity)
		
//		IF NOT (CarID = g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
//			IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
//				IF DECOR_EXIST_ON(CarID, "Player_Vehicle")
//					PRINTLN("[spawning] CanEditThisVehicle - returning FALSE - personal vehicle. ") 
//					RETURN(FALSE)
//				ENDIF
//			ENDIF
//		ENDIF
		
		NET_PRINT("[spawning] CanEditThisVehicle - returning TRUE") NET_NL()
		RETURN(TRUE)
	ENDIF

	NET_PRINT("[spawning] CanEditThisVehicle - returning FALSE") NET_NL()
	RETURN(FALSE)


//	IF DOES_ENTITY_EXIST(CarID)
//		IF NOT IS_ENTITY_A_MISSION_ENTITY(CarID)
//			IF NETWORK_GET_ENTITY_IS_LOCAL(CarID)
//				SET_ENTITY_AS_MISSION_ENTITY(CarID, FALSE, TRUE )
//				NET_PRINT("[spawning] CanEditThisVehicle - setting vehicle as mission entity") NET_NL()
//				bSetAsMissionEntity = TRUE
//			ENDIF
//		ENDIF
//		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(CarID, FALSE)
//			IF NETWORK_HAS_CONTROL_OF_ENTITY(CarID)	
//			
//				// is this a personal vehicle?
//				IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
//					IF DECOR_EXIST_ON(CarID, "Player_Vehicle")
//						NET_PRINT("[spawning] CanEditThisVehicle - returning FALSE - personal vehicle. ") NET_NL()
//						RETURN(FALSE)
//					ENDIF
//				ENDIF
//			
//				NET_PRINT("[spawning] CanEditThisVehicle - returning TRUE") NET_NL()
//				RETURN(TRUE)
//			ELSE
//				NET_PRINT("[spawning] CanEditThisVehicle - returning FALSE, does not have control of entity") NET_NL()	
//			ENDIF
//		ELSE
//			NET_PRINT("[spawning] CanEditThisVehicle - returning FALSE, does not belong to this script") NET_NL()	
//		ENDIF
//	ENDIF
//	RETURN(FALSE)
ENDFUNC




FUNC BOOL IsRacePointAreaOccupied(VECTOR vPoint)
	
	VECTOR vMin
	VECTOR vMax
	BOOL bBuildingFlag
	BOOL bReturn
	
	vPoint.z += AREA_OCCUPIED_RADIUS
	

	vMin = vPoint + << -1.0 * AREA_OCCUPIED_RADIUS, -1.0 * AREA_OCCUPIED_RADIUS, -0.5 * AREA_OCCUPIED_RADIUS >>
	vMax = vPoint + << AREA_OCCUPIED_RADIUS, AREA_OCCUPIED_RADIUS, 0.5 * AREA_OCCUPIED_RADIUS>>
	
	
	IF (AREA_OCCUPIED_RADIUS > 0.0)
		bBuildingFlag = TRUE
	ELSE
		bBuildingFlag = FALSE
	ENDIF
	
	bReturn = IS_AREA_OCCUPIED(vMin, vMax, bBuildingFlag, FALSE, FALSE, FALSE, FALSE)

	IF (bReturn)
		NET_PRINT("[spawning] IsRacePointAreaOccupied = TRUE") NET_NL()
	ENDIF

	RETURN(bReturn)
	
ENDFUNC

FUNC BOOL IsPointSafeForRaceSpawn(VECTOR vPoint)
	IF IsAnotherPlayerCurrentlyWarpingToPosition(vPoint, 0.5, PLAYER_ID(), FALSE) 
	OR NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(	vPoint, 3.0,  1.0,  1.0, 5.0, FALSE, FALSE, FALSE, 0.0, TRUE,  -1, FALSE, 0.0, TRUE, 0.0, FALSE)
	//OR IsRacePointAreaOccupied(	vPoint )
		RETURN(FALSE)
	ENDIF			
	RETURN(TRUE)
ENDFUNC

//FUNC VECTOR GetNearestSafeSpawnWithExclusion(VECTOR vCoords, VECTOR &SafeSpawnArray[], VECTOR vExclusionPoint, FLOAT fExclusionRadius)
//    FLOAT fMaxDist = 9999999999.9
//    FLOAT fThisDist
//	FLOAT fDistToExclusion
//    INT i 
//    INT iNearest = -1
//
//    REPEAT COUNT_OF(SafeSpawnArray) i
//        fThisDist = VDIST2(vCoords, SafeSpawnArray[i])
//        IF (fThisDist < fMaxDist)			
//			NET_PRINT("[spawning] GetNearestSafeSpawnWithExclusion, testing SafeSpawnArray[ = ") NET_PRINT_INT(i) NET_PRINT("] = ") NET_PRINT_VECTOR(SafeSpawnArray[i]) NET_NL()
//			// check its not within the exclusion range
//			fDistToExclusion = VDIST2(vCoords, vExclusionPoint)
//			IF (fDistToExclusion < fExclusionRadius)
//				IF NOT IsAnotherPlayerOrPlayerVehicleSpawningAtPosition(vCoords, 5.0, 5.0, PLAYER_ID(), TRUE)
//				AND NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoords, 3.0,  1.0,  1.0, 5.0, FALSE, TRUE, FALSE, 120.0, TRUE,  -1, FALSE, 100.0, TRUE, 0.0, FALSE)
//					fMaxDist = fThisDist
//	           	 	iNearest = i 
//				ENDIF
//			ENDIF
//        ENDIF
//    ENDREPEAT
//       
//    IF NOT (iNearest = -1)
//        RETURN(SafeSpawnArray[iNearest])
//    ENDIF
//
//	NET_PRINT("[spawning] GetNearestSafeSpawnWithExclusion, could not find safe point, returning initial coords.") NET_NL()
//    RETURN(vCoords)
//ENDFUNC

FUNC VECTOR GetNearestSafeSpawn(VECTOR vCoords, VECTOR &SafeSpawnArray[], VECTOR vNextCheckpoint)
   
    FLOAT fMaxDist = 9999999999.9
    FLOAT fThisDist
    INT i 
    INT iNearest = -1
	FLOAT fDistToNextCheckpoint2
	FLOAT fDistToSafePointToNextCheckpoint2
	
	fDistToNextCheckpoint2 = GET_DISTANCE_BETWEEN_COORDS(vCoords, vNextCheckpoint, FALSE)
	
	NET_PRINT("[spawning] GetNearestSafeSpawn, fDistToNextCheckpoint2 = ") NET_PRINT_FLOAT(fDistToNextCheckpoint2) NET_NL()
	
    REPEAT COUNT_OF(SafeSpawnArray) i
        fThisDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, SafeSpawnArray[i], FALSE)
        IF (fThisDist < fMaxDist)	
		
			// check the distance from this point to the next point is greater than 150.0m
		 	fDistToSafePointToNextCheckpoint2 = GET_DISTANCE_BETWEEN_COORDS(SafeSpawnArray[i], vNextCheckpoint, FALSE)
		
			IF (fThisDist > 150.0)
				NET_PRINT("[spawning] GetNearestSafeSpawn, testing SafeSpawnArray[ = ") NET_PRINT_INT(i) NET_PRINT("] = ") NET_PRINT_VECTOR(SafeSpawnArray[i]) NET_NL()
				// check its not nearer the next checkpoint that our current position
				IF GET_DISTANCE_BETWEEN_COORDS(SafeSpawnArray[i], vNextCheckpoint, FALSE) > fDistToNextCheckpoint2
					IF IsPointSafeForRaceSpawn(SafeSpawnArray[i])
		         		fMaxDist = fThisDist
		           	 	iNearest = i 
					ELSE
						NET_PRINT("[spawning] GetNearestSafeSpawn, another player is currently warping to this position ") NET_NL()
					ENDIF
				ELSE
					NET_PRINT("[spawning] GetNearestSafeSpawn, nearer checkpoint than current pos, dist2 = ") NET_PRINT_FLOAT(VDIST2(SafeSpawnArray[i], vNextCheckpoint)) NET_NL()	
				ENDIF
			ELSE
				NET_PRINT("[spawning] GetNearestSafeSpawn, too near next checkpoint ") NET_PRINT_FLOAT(fDistToSafePointToNextCheckpoint2) NET_NL()
			ENDIF
			
        ENDIF
    ENDREPEAT
       
    IF NOT (iNearest = -1)
        RETURN(SafeSpawnArray[iNearest])
    ENDIF

	NET_PRINT("[spawning] GetNearestSafeSpawn, could not find safe point, returning initial coords.") NET_NL()
    RETURN(vCoords)

ENDFUNC

FUNC BOOL IsCoronaOffsetSafe(VECTOR vCorona, FLOAT fAngle, FLOAT fDist, VECTOR &vNewCoords)
	VECTOR vec
	vec.x = SIN(fAngle) * -1.0
	vec.y = COS(fAngle)
	vec *= fDist
	vNewCoords = vCorona + vec
	IF IsPointSafeForRaceSpawn(vNewCoords)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC FLOAT GetCoronaOffsetAngle(INT iOffset)
	SWITCH iOffset
		CASE 0 RETURN 180.0
		CASE 1 RETURN 135.0
		CASE 2 RETURN 225.0
		CASE 3 RETURN 90.0
		CASE 4 RETURN 270.0
		CASE 5 RETURN 45.0
		CASE 6 RETURN 315.0
	ENDSWITCH
	RETURN(180.0)
ENDFUNC

FUNC VECTOR GetSafeCoronaOffset(VECTOR vInCoords, FLOAT fInHeader, FLOAT fDist)
	VECTOR vReturn	
	
	NET_PRINT("GetSafeCoronaOffset - called with ") NET_PRINT_VECTOR(vInCoords) NET_PRINT(", ") NET_PRINT_FLOAT(fInHeader) NET_NL()
	
	#IF IS_DEBUG_BUILD
		IF (CORONA_OFFSET_OVERRIDE > -1.0)
			IF IsCoronaOffsetSafe(vInCoords, fInHeader + CORONA_OFFSET_OVERRIDE, fDist, vReturn)
				NET_PRINT("GetSafeCoronaOffset - corona offset is safe ") NET_NL()
			ELSE
				NET_PRINT("GetSafeCoronaOffset - corona offset not safe ") NET_NL()
			ENDIF
			RETURN(vReturn)
		ENDIF
	#ENDIF
	
	INT i
	REPEAT 7 i
		IF IsCoronaOffsetSafe(vInCoords, fInHeader + GetCoronaOffsetAngle(i), fDist, vReturn)
			NET_PRINT("GetSafeCoronaOffset - returning true, corona offset ") NET_PRINT_INT(i) NET_NL()
			RETURN vReturn
		ELSE
			NET_PRINT("GetSafeCoronaOffset - IsCoronaOffsetSafe false, corona offset ") NET_PRINT_INT(i) NET_NL()	
		ENDIF
	ENDREPEAT

	
	// pick random point
	IsCoronaOffsetSafe(vInCoords, fInHeader + GetCoronaOffsetAngle(GET_RANDOM_INT_IN_RANGE(0, 7)), fDist, vReturn)
	
	RETURN(vReturn)
ENDFUNC


FUNC VECTOR GetSecondaryRaceRespawn()

	INT iStart
	INT iIterations
	INT i

	VECTOR vPoint[FMMC_MAX_NUM_RACERS]
	INT iGoodPoints = 0

	#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bDontUseSecondaryRespawn)
			NET_PRINT("[spawning] GetSecondaryRaceRespawn - bDontUseSecondaryRespawn - returning g_SpecificSpawnLocation.vCoords") NET_NL()
			RETURN g_SpecificSpawnLocation.vCoords
		ENDIF
	#ENDIF

	// try a random side first	
	IF (g_b_Is_Air_Race)
		// if an air race just start at point 0
		iStart = 0
		i = 0 
	ELSE
		iStart = GET_RANDOM_INT_IN_RANGE(0, FMMC_MAX_NUM_RACERS)
		i = iStart
	ENDIF
	
	WHILE iIterations < FMMC_MAX_NUM_RACERS
		IF (VMAG(g_SpawnData.MissionSpawnDetails.vSecondaryRaceRespawn[i]) > 0.0)
			
			vPoint[iGoodPoints] = g_SpawnData.MissionSpawnDetails.vSecondaryRaceRespawn[i]
			iGoodPoints += 1
			
			IF IsPointSafeForRaceSpawn(g_SpawnData.MissionSpawnDetails.vSecondaryRaceRespawn[i])
				NET_PRINT("[spawning] GetSecondaryRaceRespawn - returning point ") NET_PRINT_INT(i) NET_NL()
				RETURN(g_SpawnData.MissionSpawnDetails.vSecondaryRaceRespawn[i])
			ENDIF
		ENDIF
		i++
		IF (i >= FMMC_MAX_NUM_RACERS)
			i = 0
		ENDIF
		iIterations++
	ENDWHILE
	
	// chose random secondary point
	NET_PRINT("[spawning] GetSecondaryRaceRespawn - iGoodPoints = ") NET_PRINT_INT(iGoodPoints) NET_NL()
	IF (iGoodPoints > 0)
		i = GET_RANDOM_INT_IN_RANGE(0, iGoodPoints)
		NET_PRINT("[spawning] GetSecondaryRaceRespawn - returning = ") NET_PRINT_VECTOR(vPoint[i]) NET_NL()
		RETURN vPoint[i]
	ENDIF
		
	NET_PRINT("[spawning] GetSecondaryRaceRespawn - returning g_SpecificSpawnLocation.vCoords") NET_NL()
	RETURN (g_SpecificSpawnLocation.vCoords)

ENDFUNC

PROC MakeSafeZIfPossible(VECTOR &vCoords)
	VECTOR vReturn
	vReturn = vCoords
	vReturn.z += 1.0	
	IF GET_GROUND_Z_FOR_3D_COORD(vReturn, vReturn.z)
		NET_PRINT("[spawning] MakeSafeZIfPossible - z made safe, old coors = ") NET_PRINT_VECTOR(vCoords) NET_PRINT(", new coords = ") NET_PRINT_VECTOR(vReturn) NET_NL()
		vCoords = vReturn		
	ENDIF
ENDPROC

FUNC VECTOR GetNearestSafeSpawnSea(VECTOR vCoords, VECTOR vNextCheckpoint)
    RETURN GetNearestSafeSpawn(vCoords, SafeSpawn_Sea, vNextCheckpoint)
ENDFUNC

//FUNC VECTOR GetNearestSafeSpawnAir(VECTOR vCoords, VECTOR vNextCheckpoint)
//    RETURN GetNearestSafeSpawn(vCoords, SafeSpawn_Air, vNextCheckpoint)
//ENDFUNC

FUNC BOOL IsSphereInsideSphere(VECTOR vCircle1, FLOAT fRadius1, VECTOR vCircle2, FLOAT fRadius2)
	VECTOR vec
	vec = vCircle2 - vCircle1
	IF (VMAG(vec) + fRadius1 < fRadius2)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

//FUNC BOOL IsBoxInsideBox(VECTOR vMin1, VECTOR vMax1, VECTOR vMin2, VECTOR vMax2)
//	IF (vMin1.x >= vMin2.x)
//	AND (vMin1.y >= vMin2.y)
//	AND (vMin1.z >= vMin2.z)
//	AND (vMax1.x <= vMax2.x)
//	AND (vMax1.y <= vMax2.y)
//	AND (vMax1.z <= vMax2.z)
//		RETURN(TRUE)
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC

FUNC BOOL IsSphereInsideBox(VECTOR vCircle, FLOAT fRadius, VECTOR vMin, VECTOR vMax)
	VECTOR vCircleMin
	VECTOR vCircleMax
	vCircleMin = vCircle - <<fRadius, fRadius, fRadius>>
	vCircleMax = vCircle + <<fRadius, fRadius, fRadius>>
	RETURN(IsBoxInsideBox(vCircleMin, vCircleMax, vMin, vMax))
ENDFUNC

FUNC BOOL IsBoxInsideSphere(VECTOR vMin, VECTOR vMax, VECTOR vCircle, FLOAT fRadius)
	VECTOR vCircleMin
	VECTOR vCircleMax
	FLOAT fDist
	fDist = fRadius * 0.707106781186 // sin of 45
	vCircleMin = vCircle - <<fDist, fDist, fDist>>
	vCircleMax = vCircle + <<fDist, fDist, fDist>>	
	RETURN(IsBoxInsideBox(vMin, vMax, vCircleMin, vCircleMax))
ENDFUNC

//FUNC BOOL IsSphereInsideAngledArea(VECTOR vCoord, FLOAT fRadius, VECTOR vAngledCoord1, VECTOR vAngledCoord2, FLOAT fWidth)
//
//ENDFUNC

FUNC BOOL IsSpawnAreaValid(SPAWN_AREA SpArea, SPAWN_AREA &ExAreas[])
	// check all the exclusion zones an
	INT i
	REPEAT COUNT_OF(ExAreas) i
		IF (ExAreas[i].bIsActive)
		
			SWITCH ExAreas[i].iShape			
				CASE SPAWN_AREA_SHAPE_CIRCLE
					
					SWITCH SpArea.iShape
						CASE SPAWN_AREA_SHAPE_CIRCLE
							IF IsSphereInsideSphere(SpArea.vCoords1, SpArea.fFloat, ExAreas[i].vCoords1, ExAreas[i].fFloat)
								RETURN(FALSE)
							ENDIF
						BREAK
						CASE SPAWN_AREA_SHAPE_BOX
							IF IsBoxInsideSphere(SpArea.vCoords1, SpArea.vCoords2, ExAreas[i].vCoords1, ExAreas[i].fFloat)
								RETURN(FALSE)
							ENDIF
						BREAK
						CASE SPAWN_AREA_SHAPE_ANGLED
							// no checks for now
						BREAK	
					ENDSWITCH
				
				BREAK
				CASE SPAWN_AREA_SHAPE_BOX
				
					SWITCH SpArea.iShape
						CASE SPAWN_AREA_SHAPE_CIRCLE
							IF IsSphereInsideSphere(SpArea.vCoords1, SpArea.fFloat, ExAreas[i].vCoords1, ExAreas[i].fFloat)
								RETURN(FALSE)
							ENDIF
						BREAK
						CASE SPAWN_AREA_SHAPE_BOX
							IF IsBoxInsideSphere(SpArea.vCoords1, SpArea.vCoords2, ExAreas[i].vCoords1, ExAreas[i].fFloat)
								RETURN(FALSE)
							ENDIF
						BREAK
						CASE SPAWN_AREA_SHAPE_ANGLED
							// no checks for now
						BREAK	
					ENDSWITCH
					
				BREAK
				CASE SPAWN_AREA_SHAPE_ANGLED
					// no checks for now
				BREAK
			ENDSWITCH

		ENDIF
	ENDREPEAT
	
	// is it at origin?
	IF (VMAG(SpArea.vCoords1) = 0.0)
		NET_PRINT("[spawning] IsSpawnAreaValid() - vCoords1 at origin") NET_NL()
		RETURN(FALSE)	
	ENDIF
	
	RETURN(TRUE)
ENDFUNC


FUNC BOOL IsExclusionAreaValid(SPAWN_AREA ExArea)

	SPAWN_AREA SpExArea[1]
	SpExArea[0] = ExArea
	SpExArea[0].bIsActive = TRUE
	
	INT i
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i
		IF (g_SpawnData.MissionSpawnDetails.SpawnArea[i].bIsActive = TRUE)
			IF NOT IsSpawnAreaValid(g_SpawnData.MissionSpawnDetails.SpawnArea[i], SpExArea)
				RETURN(FALSE)
			ENDIF
		ENDIF
	ENDREPEAT		

	RETURN(TRUE)
ENDFUNC


/// PURPOSE:
///     Checks that a spawn area has its some of its defined area outside the action zone (exclusion area)
/// PARAMS:
///    SpawnArea - 
/// RETURNS:
///    
FUNC BOOL AreMissionSpawnDetailsValid()

	// check all the spawn areas are valid
	INT i
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i
		IF NOT IsSpawnAreaValid(g_SpawnData.MissionSpawnDetails.SpawnArea[i], g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea)
			RETURN(FALSE)
		ENDIF
	ENDREPEAT
	RETURN(TRUE)
ENDFUNC

FUNC BOOL IsWholeOfSphereVisibleByOtherPlayers(VECTOR vPoint, FLOAT fRadius, BOOL bCheckVisibilityForTeammates)
	VECTOR vTestPoint[8]
	INT i
	
	REPEAT 8 i
		vTestPoint[i] = vPoint	
	ENDREPEAT

	// n,w,s,e	
	vTestPoint[0].x += fRadius - 1.0
	vTestPoint[1].x -= fRadius + 1.0
	vTestPoint[2].y += fRadius - 1.0
	vTestPoint[3].y -= fRadius + 1.0
	
	FLOAT fDiagonalOffset
	fDiagonalOffset = SQRT(((fRadius-1.0)*(fRadius-1.0))*0.5)
	
	// diagonals
	vTestPoint[4].x += fDiagonalOffset
	vTestPoint[4].y += fDiagonalOffset
	
	vTestPoint[5].x += fDiagonalOffset
	vTestPoint[5].y -= fDiagonalOffset	
	
	vTestPoint[6].x -= fDiagonalOffset
	vTestPoint[6].y -= fDiagonalOffset		
	
	vTestPoint[7].x -= fDiagonalOffset
	vTestPoint[7].y += fDiagonalOffset		
	
	REPEAT 8 i
		IF NOT CAN_ANY_PLAYER_SEE_POINT(vTestPoint[i], 1.0, FALSE, bCheckVisibilityForTeammates)
			RETURN(FALSE)
		ENDIF
	ENDREPEAT
	
	IF NOT CAN_ANY_PLAYER_SEE_POINT(vPoint, 1.0, FALSE, bCheckVisibilityForTeammates)	
		RETURN(FALSE)
	ENDIF	
	
	RETURN(TRUE)
ENDFUNC
//
//FUNC VECTOR GetCentrePointOfSpawnArea(SPAWN_AREA SpArea)
//	SWITCH SPArea.iShape
//		CASE SPAWN_AREA_SHAPE_CIRCLE
//			RETURN SpArea.vCoords1	
//		BREAK
//		CASE SPAWN_AREA_SHAPE_BOX
//		CASE SPAWN_AREA_SHAPE_ANGLED
//			RETURN (SpArea.vCoords1 + SpArea.vCoords2) * 0.5
//		BREAK
//	ENDSWITCH
//	RETURN SpArea.vCoords1
//ENDFUNC

FUNC BOOL CanAnyPlayerSeeWholeMissionSpawnArea(SPAWN_AREA SpArea, BOOL bCheckVisibilityForTeammates)

	INT i
	VECTOR vTestPoint[4]
	
	SWITCH SPArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			IF IsWholeOfSphereVisibleByOtherPlayers(SpArea.vCoords1, SpArea.fFloat, bCheckVisibilityForTeammates)
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			VECTOR vCentre
			VECTOR vDiff				
			vCentre = (SpArea.vCoords1 + SpArea.vCoords2) * 0.5
			vDiff = SpArea.vCoords2 - SpArea.vCoords1

			REPEAT 4 i
				vTestPoint[i] = vCentre
			ENDREPEAT
			vTestPoint[0].x += (vDiff.x * 0.5) - 2.0
			vTestPoint[1].x -= (vDiff.x * 0.5) + 2.0
			vTestPoint[2].y += (vDiff.y * 0.5) - 2.0
			vTestPoint[3].y -= (vDiff.y * 0.5) + 2.0			
			
			IF CAN_ANY_PLAYER_SEE_POINT(vTestPoint[0], 2.0, FALSE, bCheckVisibilityForTeammates )
			AND CAN_ANY_PLAYER_SEE_POINT(vTestPoint[1], 2.0, FALSE, bCheckVisibilityForTeammates )
			AND CAN_ANY_PLAYER_SEE_POINT(vTestPoint[2], 2.0, FALSE, bCheckVisibilityForTeammates )
			AND CAN_ANY_PLAYER_SEE_POINT(vTestPoint[3], 2.0, FALSE, bCheckVisibilityForTeammates )
			AND CAN_ANY_PLAYER_SEE_POINT(vCentre, 2.0, FALSE, bCheckVisibilityForTeammates)	
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			// no check for now
		BREAK
	ENDSWITCH	
	

	RETURN(FALSE)
ENDFUNC

FUNC BOOL AreAllTheMissionSpawnAreasEntirelyVisible()
	INT i
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i
		IF (g_SpawnData.MissionSpawnDetails.SpawnArea[i].bIsActive)
			IF NOT CanAnyPlayerSeeWholeMissionSpawnArea(g_SpawnData.MissionSpawnDetails.SpawnArea[i], TRUE)
				RETURN(FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] AreAllTheMissionSpawnAreasEntirelyVisible - returning TRUE") NET_NL()
	#ENDIF
	RETURN(TRUE)
ENDFUNC




FUNC BOOL ShouldRaceSpawnOverrideKickIn()

	RETURN(FALSE) // disabled for now

//	FLOAT fDistPlayerToNextCheckpoint = GET_DISTANCE_BETWEEN_COORDS(g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint, GET_PLAYER_COORDS(PLAYER_ID()), FALSE)
//	FLOAT fDistPlayerToLastCheckpoint = GET_DISTANCE_BETWEEN_COORDS(g_SpecificSpawnLocation.vCoords, GET_PLAYER_COORDS(PLAYER_ID()), FALSE)
//	FLOAT fDistLastCheckpointToNextCheckpoint = GET_DISTANCE_BETWEEN_COORDS(g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint, g_SpecificSpawnLocation.vCoords, FALSE)
//	
//	NET_PRINT("[spawning] ShouldRaceSpawnOverrideKickIn - fDistPlayerToNextCheckpoint = ") NET_PRINT_FLOAT(fDistPlayerToNextCheckpoint)
//	NET_PRINT(", fDistPlayerToLastCheckpoint = ") NET_PRINT_FLOAT(fDistPlayerToLastCheckpoint)
//	NET_PRINT(", fDistLastCheckpointToNextCheckpoint = ") NET_PRINT_FLOAT(fDistLastCheckpointToNextCheckpoint) 
//	NET_PRINT(", g_SpawnData.MissionSpawnDetails.fSpecificSpawnLocationOverride = ") NET_PRINT_FLOAT(g_SpawnData.MissionSpawnDetails.fSpecificSpawnLocationOverride) 
//	NET_NL()
//	
//	IF (fDistLastCheckpointToNextCheckpoint > (g_SpawnData.MissionSpawnDetails.fSpecificSpawnLocationOverride*2.0))
//		IF (fDistPlayerToLastCheckpoint > g_SpawnData.MissionSpawnDetails.fSpecificSpawnLocationOverride)
//			IF (fDistPlayerToNextCheckpoint < fDistLastCheckpointToNextCheckpoint)
//				NET_PRINT("[spawning] ShouldRaceSpawnOverrideKickIn = TRUE") NET_NL()
//				RETURN(TRUE)
//			ELSE
//				NET_PRINT("[spawning] ShouldRaceSpawnOverrideKickIn = FALSE - checkpoints are closer") NET_NL()
//			ENDIF
//		ELSE
//			NET_PRINT("[spawning] ShouldRaceSpawnOverrideKickIn = FALSE - player is near to last checkpoint") NET_NL()
//		ENDIF
//	ELSE
//		NET_PRINT("[spawning] ShouldRaceSpawnOverrideKickIn = FALSE - checkpoints too near each other") NET_NL()
//	ENDIF
//	
//	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsPointInSameConsiderInteriorState(VECTOR vCoords)

	INTERIOR_INSTANCE_INDEX InteriorID
	INT iInteriorGroup
	
	IF (g_SpawnData.MissionSpawnDetails.bConsiderInteriors)
	
		// if player died in an interior then check the new interior we are considering is in the same group
		IF IS_VALID_INTERIOR(g_SpawnData.MissionSpawnDetails.DeathInterior)			
			IF NOT IS_COLLISION_MARKED_OUTSIDE(vCoords)
				InteriorID = GET_INTERIOR_AT_COORDS(vCoords)
				NET_PRINT("IsPointInSameConsiderInteriorState InteriorID = ") NET_PRINT_INT(NATIVE_TO_INT(InteriorID)) NET_NL()
				IF IS_VALID_INTERIOR(InteriorID)
					iInteriorGroup = GET_INTERIOR_GROUP_ID(InteriorID)	
					//NET_PRINT("IsPointInSameConsiderInteriorState - g_SpawnData.MissionSpawnDetails.iDeathInteriorGroup = ") NET_PRINT_INT(g_SpawnData.MissionSpawnDetails.iDeathInteriorGroup) 
					//NET_PRINT(" iInteriorGroup = ") NET_PRINT_INT(iInteriorGroup) NET_NL()
					IF NOT (iInteriorGroup = g_SpawnData.MissionSpawnDetails.iDeathInteriorGroup)
						RETURN(FALSE)
					ELSE
						// same interior group
					ENDIF
				ELSE
					//NET_PRINT("IsPointInSameConsiderInteriorState - point is non valid interior") NET_NL()
					RETURN(FALSE)
				ENDIF
			ELSE
				//NET_PRINT("IsPointInSameConsiderInteriorState - point is outside") NET_NL()
				RETURN(FALSE)
			ENDIF
		ENDIF

	ENDIF
	
	RETURN(TRUE) // default

ENDFUNC



FUNC PLAYER_INDEX GetMyClosestAliveTeammateToCoord(VECTOR vCoord)
	INT iPlayer
	PLAYER_INDEX PlayerID
	PLAYER_INDEX ClosestPlayerID
	FLOAT fDist
	FLOAT fClosestDist = 999999.9
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID)
			IF NOT (PlayerID = PLAYER_ID())
				IF (GET_PLAYER_TEAM(PlayerID) = GET_PLAYER_TEAM(PLAYER_ID()))					
					IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)				
						// get dist to this teammate
						fDist = VMAG(GET_PLAYER_PERCEIVED_COORDS(PlayerID) - vCoord)
						IF (fDist < fClosestDist)
							fClosestDist = fDist	
							ClosestPlayerID = PlayerID
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(ClosestPlayerID)
ENDFUNC


FUNC VECTOR GetAveragePlayerCoord()

	VECTOR vTotal
	INT iCount
	INT iPlayer
	PLAYER_INDEX PlayerID
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID)
			IF NOT (PlayerID = PLAYER_ID())
				IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
					vTotal += GET_PLAYER_COORDS(PlayerID)
					iCount += 1
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF (iCount > 0)
		vTotal /= TO_FLOAT(iCount)
	ELSE
		// couldn't find any alive players so just use own coords (dead or alive)
		vTotal = GET_PLAYER_COORDS(PLAYER_ID())
	ENDIF
	
	RETURN(vTotal)
ENDFUNC


FUNC VECTOR GetRandomPointDistFromPoint(VECTOR vCoord, FLOAT fDist = 200.0)
	VECTOR vec
	FLOAT fRandomHeading
	vec = <<0.0, 1.0, 0.0>>
	fRandomHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)
	RotateVec(vec, <<0.0, 0.0, fRandomHeading>>)
	vec *= fDist
	RETURN(vCoord + vec)
ENDFUNC

FUNC VECTOR GetPointDistFromPointBasedOnPlayerID(VECTOR vCoord, FLOAT fDist = 200.0)
	VECTOR vec
	FLOAT fRandomHeading
	vec = <<0.0, 1.0, 0.0>>
	fRandomHeading = NATIVE_TO_INT(PLAYER_ID()) * (360.0/NUM_NETWORK_PLAYERS)
	RotateVec(vec, <<0.0, 0.0, fRandomHeading>>)
	vec *= fDist
	RETURN(vCoord + vec)
ENDFUNC


FUNC VECTOR GetCoordsOfClosestAliveTeammateToCoord(VECTOR vCoord)
	PLAYER_INDEX PlayerID
	PlayerID = GetMyClosestAliveTeammateToCoord(vCoord)
	IF IS_NET_PLAYER_OK(PlayerID)
		RETURN(GET_PLAYER_COORDS(PlayerID))
	ENDIF
	// could not find a close player, so just return own coords
	RETURN(vCoord)
ENDFUNC


FUNC VECTOR GetAverageTeammateCoord()
	VECTOR vTotal
	INT iCount
	INT iPlayer
	PLAYER_INDEX PlayerID
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID)
			IF NOT (PlayerID = PLAYER_ID())
				IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
					IF ARE_PLAYERS_ON_SAME_TEAM(PLAYER_ID(), PlayerID )
					//AND IS_PLAYER_IN_SAME_MISSION_STATE_AS_ME(PlayerID)
						vTotal += GET_PLAYER_COORDS(PlayerID)
						iCount += 1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF (iCount > 0)
		vTotal /= TO_FLOAT(iCount)
	ELSE
		// couldn't find any alive players so just use own coords (dead or alive)
		vTotal = GET_PLAYER_COORDS(PLAYER_ID())
	ENDIF
	
	RETURN(vTotal)	
	
ENDFUNC




PROC GetRandomMPSpawnCoord(MP_SPAWN_POINT &SpawnPoints[], VECTOR &vPos, FLOAT &fHead, FLOAT fVisDis=120.0)

	INT i, j
	INT iRandOffset

	#IF IS_DEBUG_BUILD
		IF NOT (g_SpawnData.iRecRoomPosition = -1)
			IF NOT (g_SpawnData.iRecRoomPosition < COUNT_OF(SpawnPoints))
				g_SpawnData.iRecRoomPosition = COUNT_OF(SpawnPoints) - 1
			ENDIF
			vPos = SpawnPoints[g_SpawnData.iRecRoomPosition].Pos
			fHead = SpawnPoints[g_SpawnData.iRecRoomPosition].Heading
			EXIT
		ENDIF
	#ENDIF					

	// try and find a safe point to begin with
	iRandOffset = GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(SpawnPoints))
	REPEAT COUNT_OF(SpawnPoints) i
		
		j = i + iRandOffset
		IF NOT (j <  COUNT_OF(SpawnPoints))
			j -= COUNT_OF(SpawnPoints)
		ENDIF
	
		NET_PRINT("[spawning] GetRandomMPSpawnCoord - testing point ") NET_PRINT_INT(j) NET_NL()
	
		IF IsPointOkForSpawning(SpawnPoints[j].Pos, SpawnPoints[j].Heading, FALSE, TRUE, FALSE, FALSE, fVisDis)
			vPos = SpawnPoints[j].Pos
			fHead = SpawnPoints[j].Heading
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] GetRandomMPSpawnCoord - returning point ") NET_PRINT_INT(j) NET_NL()
			#ENDIF
			EXIT
		ENDIF
	ENDREPEAT	
	
	// if we didn't find a safe point return a random one
	i = GET_RANDOM_INT_IN_RANGE(0, COUNT_OF(SpawnPoints))
	vPos = SpawnPoints[i].Pos
	fHead = SpawnPoints[i].Heading	
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] GetRandomMPSpawnCoord - returning random point ") NET_PRINT_INT(i) NET_NL()
	#ENDIF
	
ENDPROC

#IF FEATURE_FIXER

PROC GetMusicStudioDrunkRespawns(MP_SPAWN_POINT &spawnpoints[])
	
	spawnpoints[0].Pos = <<-790.9983, -164.4170, 36.2931>>
	spawnpoints[0].Heading = 137.9655

	spawnpoints[1].Pos =  <<-759.2075, -190.2448, 47.6269>>
	spawnpoints[1].Heading = 69.5793

	spawnpoints[2].Pos =  <<-859.9305, -236.0357, 38.6344>>
	spawnpoints[2].Heading = 99.9262

	spawnpoints[3].Pos =  <<-864.9851, -222.3483, 60.0416>>
	spawnpoints[3].Heading = 252.3229

	spawnpoints[4].Pos =  <<-858.0186, -242.6807, 59.9961>>
	spawnpoints[4].Heading = 5.9838

	spawnpoints[5].Pos =  <<-899.8217, -141.3023, 36.9310>>
	spawnpoints[5].Heading = 313.7854

	spawnpoints[6].Pos =  <<-816.2837, -102.6396, 36.5778>>
	spawnpoints[6].Heading = 318.5637

	spawnpoints[7].Pos =  <<-848.1709, -61.5357, 36.5473>>
	spawnpoints[7].Heading = 156.5983

	spawnpoints[8].Pos =  <<-960.9850, -189.3435, 36.6712>>
	spawnpoints[8].Heading = 110.5848

	spawnpoints[9].Pos =  <<-731.0921, -273.2380, 35.9452>>
	spawnpoints[9].Heading = 43.1219

	spawnpoints[10].Pos =  <<-895.2287, -157.7689, 36.7077>>
	spawnpoints[10].Heading = 244.6843

ENDPROC

PROC GetRandomDrunkRespawnMusicStudio(VECTOR &vPos, FLOAT &fHead, FLOAT fVisDis=120.0)

	MP_SPAWN_POINT SpawnPoints[11]
	
	GetMusicStudioDrunkRespawns(SpawnPoints)
	
	GetRandomMPSpawnCoord(SpawnPoints, vPos, fHead, fVisDis)
	
	#IF IS_DEBUG_BUILD
	IF (g_iMusicStudioDrunkSpawnLocation > -1)
		vPos = SpawnPoints[g_iMusicStudioDrunkSpawnLocation].Pos
		fHead = SpawnPoints[g_iMusicStudioDrunkSpawnLocation].Heading
	ENDIF
	#ENDIF	

ENDPROC

PROC GetRandomSittingSpawnPoint(VECTOR &vPos, FLOAT &fHead, FLOAT fVisDis=120.0)
	MP_SPAWN_POINT SpawnPoints[15]
	
	SpawnPoints[0].Pos = <<-1690.928, -1052.085, 12.5375>>
	SpawnPoints[0].Heading = 46.440
	SpawnPoints[1].Pos = <<-1212.403, -952.1625, 1.6625>>
	SpawnPoints[1].Heading = 117.600
	SpawnPoints[2].Pos = << -1772.089, -1141.589, 12.5375>>
	SpawnPoints[2].Heading = 46.440
	SpawnPoints[3].Pos = << -116.3250, -441.0750, 35.4125>>
	SpawnPoints[3].Heading = -132.840
	SpawnPoints[4].Pos = <<-1157.281, 927.675, 197.5375>>
	SpawnPoints[4].Heading = -143.640
	SpawnPoints[5].Pos = <<-82.8500, -880.5125, 40.0875>>
	SpawnPoints[5].Heading = -66.240
	SpawnPoints[6].Pos = <<195.7000, -962.1625, 29.6000>>
	SpawnPoints[6].Heading = 115.280
	SpawnPoints[7].Pos = <<-885.4375, -787.9000, 15.4375>>
	SpawnPoints[7].Heading = 122.340
	SpawnPoints[8].Pos = << -829.5000, -922.1375, 16.0125>>
	SpawnPoints[8].Heading = -125.280
	SpawnPoints[9].Pos = << -1221.905, -1571.869, 3.675>>
	SpawnPoints[9].Heading =  -106.920
	SpawnPoints[10].Pos = <<-609.6500, -622.3375, 34.1875>>
	SpawnPoints[10].Heading =  -90.0
	SpawnPoints[11].Pos = <<-894, -1322.775, 4.5125>>
	SpawnPoints[11].Heading = -166.320
	SpawnPoints[12].Pos = << 386.2375, -1514.243, 28.7875>>
	SpawnPoints[12].Heading =  119.160
	SpawnPoints[13].Pos = <<-310.9625, -1627.762, 31.3250>>
	SpawnPoints[13].Heading = -117.720
	SpawnPoints[14].Pos = <<171.7750, -677.0125, 42.6375>>
	SpawnPoints[14].Heading =  -54.370
	
	GetRandomMPSpawnCoord(SpawnPoints, vPos, fHead, fVisDis)
	
	#IF IS_DEBUG_BUILD
	IF (g_iSittingSmokingLocationOverride > -1)
		vPos = SpawnPoints[g_iSittingSmokingLocationOverride].Pos
		fHead = SpawnPoints[g_iSittingSmokingLocationOverride].Heading
	ENDIF
	#ENDIF
	
ENDPROC
#ENDIF


PROC ClearCustomSpawnPoints()
	INT i
	CUSTOM_SPAWN_POINT EmptySpawnPoint
	REPEAT MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS i
		g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i] = EmptySpawnPoint		
	ENDREPEAT
ENDPROC

PROC ClearFallbackSpawnPoints()
	PRINTLN("[spawning] ClearFallbackSpawnPoints() - called.")
	INT i
	FALLBACK_SPAWN_POINT EmptySpawnPoint
	REPEAT MAX_NUMBER_OF_FALLBACK_SPAWN_POINTS i
		g_SpawnData.FallbackSpawnPoints[i] = EmptySpawnPoint		
	ENDREPEAT
	g_SpawnData.iNumberOfFallbackSpawnPoints = 0
ENDPROC

PROC ResetCustomSpawnPoints()
	NET_PRINT("[spawning] ResetCustomSpawnPoints() - called.") NET_NL() 
	ClearCustomSpawnPoints()
	g_SpawnData.bUseCustomSpawnPoints = FALSE
ENDPROC



FUNC BOOL ARE_CUSTOM_SPAWN_POINTS_BEING_USED_BY_ANOTHER_THREAD()
	IF (g_SpawnData.bUseCustomSpawnPoints)	
	AND NOT (GET_ID_OF_THIS_THREAD() = g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID)
	AND IS_THREAD_ACTIVE(g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID)	
		//PRINTLN("[spawning] ARE_CUSTOM_SPAWN_POINTS_BEING_USED_BY_ANOTHER_THREAD - returning true. Thread id = ", NATIVE_TO_INT(g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID), ", script name is ", g_SpawnData.CustomSpawnPointInfo.ScriptUsingCustomSpawnPoints)		
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_CUSTOM_SPAWN_POINTS_SETTINGS_BEEN_STORED_BY_ANOTHER_ACTIVE_THREAD()
	IF (g_SpawnData.bHasStoredCustomSpawnPointInfo)
	AND NOT (GET_ID_OF_THIS_THREAD() = g_SpawnData.StoredCustomSpawnPointInfo.CustomSpawnThreadID)
	AND IS_THREAD_ACTIVE(g_SpawnData.StoredCustomSpawnPointInfo.CustomSpawnThreadID)
		//PRINTLN("[spawning] HAVE_CUSTOM_SPAWN_POINTS_SETTINGS_BEEN_STORED_BY_ANOTHER_ACTIVE_THREAD - returning true. Thread id = ", NATIVE_TO_INT(g_SpawnData.StoredCustomSpawnPointInfo.CustomSpawnThreadID), ", script name is ", g_SpawnData.StoredCustomSpawnPointInfo.ScriptUsingCustomSpawnPoints)			
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_CUSTOM_SPAWN_POINT_INFO(CUSTOM_SPAWN_POINT_INFO &CustomSpawnPointInfo)
	
	PRINTLN("[spawning] CUSTOM_SPAWN_POINT_INFO : from script ", CustomSpawnPointInfo.ScriptUsingCustomSpawnPoints)

	// custom spawn points
	PRINTLN("      iNumberOfCustomSpawnPoints ", CustomSpawnPointInfo.iNumberOfCustomSpawnPoints)
	INT i
	REPEAT CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i
		PRINTLN("      CustomSpawnPoints[ ", i, "] = ", CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, ", ", CustomSpawnPointInfo.CustomSpawnPoints[i].fHeading, ", ", CustomSpawnPointInfo.CustomSpawnPoints[i].fWeighting)	
	ENDREPEAT
	PRINTLN("      iLastCustomSpawnPoint ", CustomSpawnPointInfo.iLastCustomSpawnPoint)
	PRINTLN("      iLastUsedCustomSpawnPoint ", CustomSpawnPointInfo.iLastUsedCustomSpawnPoint)
	PRINTLN("      bIgnorePreviousSpawnPoint ", CustomSpawnPointInfo.bIgnorePreviousSpawnPoint)
	PRINTLN("      bIgnoreObstructionChecks ", CustomSpawnPointInfo.bIgnoreObstructionChecks)
	PRINTLN("      bIgnoreDistanceChecks ", CustomSpawnPointInfo.bIgnoreDistanceChecks)
	PRINTLN("      bNeverFallBackToNavMesh ", CustomSpawnPointInfo.bNeverFallBackToNavMesh)
	PRINTLN("      fCustomSpawnWorldObjectClearance ", CustomSpawnPointInfo.fCustomSpawnWorldObjectClearance)
	PRINTLN("      fCustomSpawnScriptObjectClearance ", CustomSpawnPointInfo.fCustomSpawnScriptObjectClearance)
	PRINTLN("      fCustomSpawnPedClearance ", CustomSpawnPointInfo.fCustomSpawnPedClearance)
	PRINTLN("      fCustomSpawnVehicleClearance ", CustomSpawnPointInfo.fCustomSpawnVehicleClearance)
	PRINTLN("      fCustomSpawnMinDistWarper ", CustomSpawnPointInfo.fCustomSpawnMinDistWarper)
	PRINTLN("      fCustomSpawnMinDistEnemy ", CustomSpawnPointInfo.fCustomSpawnMinDistEnemy)
	PRINTLN("      fCustomSpawnMinDistTeammate ", CustomSpawnPointInfo.fCustomSpawnMinDistTeammate)
	PRINTLN("      fCustomMaxDistFromDeath ", CustomSpawnPointInfo.fCustomMaxDistFromDeath)
	

ENDPROC
#ENDIF


PROC STORE_CUSTOM_SPAWN_POINT_SETTINGS()

	
	IF HAVE_CUSTOM_SPAWN_POINTS_SETTINGS_BEEN_STORED_BY_ANOTHER_ACTIVE_THREAD()
		// have the custom spawn points from the other thread already been stored? if so then do nothing.
		IF (g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID = g_SpawnData.StoredCustomSpawnPointInfo.CustomSpawnThreadID)
			PRINTLN("[spawning] STORE_CUSTOM_SPAWN_POINT_SETTINGS - already has stored custom points from active thread ", g_SpawnData.StoredCustomSpawnPointInfo.ScriptUsingCustomSpawnPoints)
			EXIT
		ELSE
			// trying to store a different thread over the stored value that is already there!
			PRINTLN("[spawning] STORE_CUSTOM_SPAWN_POINT_SETTINGS - overwriting existing stored values from active thread! stored by ", g_SpawnData.StoredCustomSpawnPointInfo.ScriptUsingCustomSpawnPoints, " over-writting script is ", g_SpawnData.CustomSpawnPointInfo.ScriptUsingCustomSpawnPoints)		
			ASSERTLN("[spawning] STORE_CUSTOM_SPAWN_POINT_SETTINGS - overwriting existing stored values from active thread! stored by ", g_SpawnData.StoredCustomSpawnPointInfo.ScriptUsingCustomSpawnPoints, " over-writting script is ", g_SpawnData.CustomSpawnPointInfo.ScriptUsingCustomSpawnPoints)		
		ENDIF
	ENDIF

	IF NOT (GET_ID_OF_THIS_THREAD() = g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID)
	
		PRINTLN("[spawning] STORE_CUSTOM_SPAWN_POINT_SETTINGS called")
		
		COPY_SCRIPT_STRUCT(g_SpawnData.StoredCustomSpawnPointInfo, g_SpawnData.CustomSpawnPointInfo, SIZE_OF(CUSTOM_SPAWN_POINT_INFO)) // to help reduce stack size
		//g_SpawnData.StoredCustomSpawnPointInfo = g_SpawnData.CustomSpawnPointInfo
		
		g_SpawnData.StoredNextSpawn = g_SpawnData.NextSpawn
		g_SpawnData.bHasStoredCustomSpawnPointInfo = TRUE
	
		#IF IS_DEBUG_BUILD
		PRINTLN("[spawning] STORE_CUSTOM_SPAWN_POINT_SETTINGS successfully stored.")
		PRINT_CUSTOM_SPAWN_POINT_INFO(g_SpawnData.StoredCustomSpawnPointInfo)
		#ENDIF
		
	ENDIF

ENDPROC

PROC CLEAR_CUSTOM_SPAWN_POINTS()

	IF ARE_CUSTOM_SPAWN_POINTS_BEING_USED_BY_ANOTHER_THREAD()
	AND NOT HAVE_CUSTOM_SPAWN_POINTS_SETTINGS_BEEN_STORED_BY_ANOTHER_ACTIVE_THREAD()
		NET_PRINT("[spawning] CLEAR_CUSTOM_SPAWN_POINTS g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID = ") NET_PRINT_INT(NATIVE_TO_INT(g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID)) NET_NL()
		PRINTLN("[spawning] CLEAR_CUSTOM_SPAWN_POINTS - another thread is using custom spawn points!")	
		STORE_CUSTOM_SPAWN_POINT_SETTINGS()	
	ENDIF
	
	ClearCustomSpawnPoints()
	g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints = 0
	NET_PRINT("[spawning] CLEAR_CUSTOM_SPAWN_POINTS() - called.") NET_NL() 
	DEBUG_PRINTCALLSTACK()
ENDPROC





PROC RESTORE_CUSTOM_SPAWN_POINT_SETTINGS()

	//g_SpawnData.CustomSpawnPointInfo = g_SpawnData.StoredCustomSpawnPointInfo	
	COPY_SCRIPT_STRUCT(g_SpawnData.CustomSpawnPointInfo, g_SpawnData.StoredCustomSpawnPointInfo, SIZE_OF(CUSTOM_SPAWN_POINT_INFO)) // to help reduce stack size
	
	g_SpawnData.NextSpawn = g_SpawnData.StoredNextSpawn
	#IF IS_DEBUG_BUILD
	PRINTLN("[spawning] RESTORE_CUSTOM_SPAWN_POINT_SETTINGS has successfully restored. ")
	PRINT_CUSTOM_SPAWN_POINT_INFO(g_SpawnData.CustomSpawnPointInfo)
	#ENDIF
	
	// only the last to leave switches off the light.
	IF (GET_ID_OF_THIS_THREAD() = g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID)
		g_SpawnData.bHasStoredCustomSpawnPointInfo = FALSE
		PRINTLN("[spawning] RESTORE_CUSTOM_SPAWN_POINT_SETTINGS setting bHasStoredCustomSpawnPointInfo to FALSE ")
	ENDIF

ENDPROC

FUNC BOOL SHOULD_LOCAL_PLAYER_SPAWN_AT_NEAREST_HOSPITAL()
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_SMUGGLER_BUY
	OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_BIKER_CONTRABAND_DEFEND
	OR IS_THIS_A_GANG_ATTACK()
	OR IS_ON_IMPROMPTU_DEATHMATCH_GLOBAL_SET()
	OR IS_LOCAL_PLAYER_AN_ANIMAL()
	OR collectables_missiondata_MAIN.bturnbacktohuman = TRUE
	OR (g_SpawnData.bUseCustomSpawnPoints AND IS_PLAYER_PERMANENT_PARTICIPANT_TO_ANY_EVENT(PLAYER_ID()))
		PRINTLN("[spawning] SHOULD_LOCAL_PLAYER_SPAWN_AT_NEAREST_HOSPITAL - FALSE")
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_PLAYER_NEXT_RESPAWN_LOCATION(PLAYER_SPAWN_LOCATION NextRespawnLocation, BOOL bPersistForDurationOfScript=FALSE, BOOL bUseRaceCoronaSpacing=FALSE, BOOL bIsWaterCheckpoint=FALSE, BOOL bIsAerialCheckpoint=FALSE)
	IF MPGlobals.g_KillStrip.killerData.killerID != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID)].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
		AND SHOULD_LOCAL_PLAYER_SPAWN_AT_NEAREST_HOSPITAL()
			PRINTLN("[spawning] SET_PLAYER_NEXT_RESPAWN_LOCATION - Overriding to hospital, player killed by Orbital Cannon")
			NextRespawnLocation = SPAWN_LOCATION_NEAREST_HOSPITAL
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		
		BOOL bHasChanged = FALSE
	
		IF NOT (g_SpawnData.NextSpawn.Location = NextRespawnLocation)
		OR NOT (g_SpawnData.NextSpawn.bIsPersistant = bPersistForDurationOfScript)
		OR NOT (g_SpawnData.NextSpawn.bUseRaceCoronaSpacing = bUseRaceCoronaSpacing)
		OR NOT (g_SpawnData.NextSpawn.bIsWaterCheckpoint = bIsWaterCheckpoint)
		OR NOT (g_SpawnData.NextSpawn.bIsAerialCheckpoint = bIsAerialCheckpoint)
			bHasChanged  = TRUE
		ENDIF
		
		IF (bHasChanged)
			NET_PRINT("[spawning] SET_PLAYER_NEXT_RESPAWN_LOCATION - called with NextRespawnLocation = ") NET_PRINT_INT(ENUM_TO_INT(NextRespawnLocation)) 
			NET_PRINT(" bPersistForDurationOfScript = ") NET_PRINT_BOOL(bPersistForDurationOfScript)
			NET_PRINT(" bUseRaceCoronaSpacing = ") NET_PRINT_BOOL(bUseRaceCoronaSpacing)
			NET_PRINT(" bIsWaterCheckpoint = ") NET_PRINT_BOOL(bIsWaterCheckpoint)
			NET_PRINT(" bIsAerialCheckpoint = ") NET_PRINT_BOOL(bIsAerialCheckpoint)
			NET_PRINT(" from script ") NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_PRINT(", current next respawn = ") NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.NextSpawn.Location)) 
			NET_NL()
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	
	IF NextRespawnLocation != SPAWN_LOCATION_INSIDE_PROPERTY
	AND NextRespawnLocation != SPAWN_LOCATION_INSIDE_GARAGE
		g_Spawn_RespawnFromStoreLastProperty = 0
		#IF IS_DEBUG_BUILD
		IF (bHasChanged)
			PRINTLN("SET_PLAYER_NEXT_RESPAWN_LOCATION g_Spawn_RespawnFromStoreLastProperty = 0")
		ENDIF
		#ENDIF
	ENDIF
	
	g_SpawnData.NextSpawn.Location = NextRespawnLocation
	g_SpawnData.NextSpawn.Thread_ID = GET_ID_OF_THIS_THREAD()
	g_SpawnData.NextSpawn.bIsPersistant = bPersistForDurationOfScript
	g_SpawnData.NextSpawn.bUseRaceCoronaSpacing = bUseRaceCoronaSpacing
	g_SpawnData.NextSpawn.bIsWaterCheckpoint = bIsWaterCheckpoint
	g_SpawnData.NextSpawn.bIsAerialCheckpoint = bIsAerialCheckpoint
	
	PRINTLN("[spawning] SET_PLAYER_NEXT_RESPAWN_LOCATION g_SpawnData.NextSpawn.Thread_ID = ", NATIVE_TO_INT(g_SpawnData.NextSpawn.Thread_ID))
	
ENDPROC


PROC TurnOffCustomSpawnPoints()

	PRINTLN("[spawning] TurnOffCustomSpawnPoints called")
	DEBUG_PRINTCALLSTACK()

	IF ARE_CUSTOM_SPAWN_POINTS_BEING_USED_BY_ANOTHER_THREAD()
	AND NOT HAVE_CUSTOM_SPAWN_POINTS_SETTINGS_BEEN_STORED_BY_ANOTHER_ACTIVE_THREAD()
		NET_PRINT("[spawning] TurnOffCustomSpawnPoints - g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID = ") NET_PRINT_INT(NATIVE_TO_INT(g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID)) NET_NL()
		PRINTLN("[spawning] TurnOffCustomSpawnPoints - another thread is using custom spawn points!")
		STORE_CUSTOM_SPAWN_POINT_SETTINGS()			
	ENDIF				
	
	IF HAVE_CUSTOM_SPAWN_POINTS_SETTINGS_BEEN_STORED_BY_ANOTHER_ACTIVE_THREAD()	
		RESTORE_CUSTOM_SPAWN_POINT_SETTINGS()
	ELSE
		CLEAR_CUSTOM_SPAWN_POINTS()	
		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AUTOMATIC)
		g_SpawnData.bUseCustomSpawnPoints = FALSE
		g_SpawnData.bHasStoredCustomSpawnPointInfo = FALSE
	ENDIF
		

ENDPROC



FUNC BOOL GB_IS_BOSS_INSIDE_GLOBAL_EXCLUSION_ZONE(PLAYER_INDEX PlayerID)
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerID, FALSE)
		PLAYER_INDEX BossID = GB_GET_THIS_PLAYER_GANG_BOSS(PlayerID)
		VECTOR vBossCoords = GET_PLAYER_PERCEIVED_COORDS(BossID)
		RETURN IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vBossCoords)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_IS_GOON_PLAYER_WITHIN_RESPAWN_RANGE_OF_BOSS(PLAYER_INDEX PlayerID)
	VECTOR vPlayerCoords = GET_PLAYER_PERCEIVED_COORDS(PlayerID)
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PlayerID, FALSE)
		PLAYER_INDEX BossID = GB_GET_THIS_PLAYER_GANG_BOSS(PlayerID)
		VECTOR vBossCoords = GET_PLAYER_PERCEIVED_COORDS(BossID)
		// 2d check
		vPlayerCoords.z = 0.0
		vBossCoords.z = 0.0
		IF (VDIST2(vPlayerCoords, vBossCoords) < 250000) // 500m
			RETURN(TRUE)
		ENDIF
	ELSE
		PLAYER_INDEX GoonID = GET_NEAREST_GOON_IN_PLAYERS_GANG(PlayerID, TRUE)
		IF NOT (GoonID = INVALID_PLAYER_INDEX())
			VECTOR vGoonCoords = GET_PLAYER_PERCEIVED_COORDS(GoonID)	
			// 2d check
			vPlayerCoords.z = 0.0
			vGoonCoords.z = 0.0
			IF (VDIST2(vPlayerCoords, vGoonCoords) < 250000) // 500m
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYERS_GANG_BOSS_ON_THEIR_OWN_PRIVATE_YACHT()
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		PLAYER_INDEX BossID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		IF IS_NET_PLAYER_OK(BossID, FALSE, FALSE)
			IF IS_PLAYER_ON_THEIR_YACHT(BossID)
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYERS_GANG_BOSS_ON_YACHT(INT iYachtID)
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		PLAYER_INDEX BossID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		IF IS_NET_PLAYER_OK(BossID, FALSE, FALSE)
			IF IS_PLAYER_ON_YACHT(BossID, iYachtID)
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_BOSS_FLYING_A_VEHICLE()
	PLAYER_INDEX BossPlayerID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	PED_INDEX BossPedID = GET_PLAYER_PED(BossPlayerID)	
	IF DOES_ENTITY_EXIST(BossPedID)
	AND NOT IS_PED_INJURED(BossPedID)
	AND (IS_PED_IN_FLYING_VEHICLE(BossPedID) 
		OR GlobalplayerBD[NATIVE_TO_INT(BossPlayerID)].playerBlipData.iInVehicleType = PBBD_IS_IN_JET
		OR GlobalplayerBD[NATIVE_TO_INT(BossPlayerID)].playerBlipData.iInVehicleType = PBBD_IS_IN_AIRPLANE
		OR GlobalplayerBD[NATIVE_TO_INT(BossPlayerID)].playerBlipData.iInVehicleType = PBBD_IS_IN_HELI)
	//AND IS_ENTITY_IN_AIR(BossPedID)
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

#IF FEATURE_HEIST_ISLAND
FUNC BOOL IsPointInBeachParty(VECTOR vPoint)
	RETURN IS_POINT_IN_ANGLED_AREA(vPoint, <<4776.643, -4927.201, -16.635>>, <<5007.543, -4922.876, 50.0>>, 181.675, FALSE)
ENDFUNC
#ENDIF


/// PURPOSE:
///    Pick which spawn location to use for rejoining a game
FUNC PLAYER_SPAWN_LOCATION GetPlayerSpawnLocationType()

	INT iYachtID

	IF GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_NET_TEST
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_NET_TEST_BED") NET_NL()
		#ENDIF
		RETURN(SPAWN_LOCATION_NET_TEST_BED)
	ELSE
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] GetPlayerSpawnLocationType - GET_CURRENT_GAMEMODE() = ") NET_PRINT_INT(ENUM_TO_INT(GET_CURRENT_GAMEMODE())) NET_NL()
		#ENDIF

		// is next respawn location set? if so check that the calling thread is still active
		IF NOT (g_SpawnData.NextSpawn.Location = SPAWN_LOCATION_AUTOMATIC)
			IF NOT IS_THREAD_ACTIVE(g_SpawnData.NextSpawn.Thread_ID)
			
				PRINTLN("[spawning] GetPlayerSpawnLocationType - next spawn thread no longer active, thread id ", NATIVE_TO_INT(g_SpawnData.NextSpawn.Thread_ID)) 
				
				// was it custom spawn points? if so clear the data
				IF (g_SpawnData.NextSpawn.Location = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS)
					ResetCustomSpawnPoints()
				ENDIF			
			
				g_SpawnData.NextSpawn.Location = SPAWN_LOCATION_AUTOMATIC	
			ENDIF
		ENDIF


		IF NOT (g_SpawnData.NextSpawn.Location = SPAWN_LOCATION_AUTOMATIC)
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING g_SpawnData.NextSpawn.Location = ") NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.NextSpawn.Location)) NET_NL()
			#ENDIF
			RETURN(g_SpawnData.NextSpawn.Location)		
		ELSE

			IF IS_PLAYER_SCTV(PLAYER_ID())
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetPlayerSpawnLocationType - IS_PLAYER_SCTV - RETURNING g_SpawnData.NextSpawn.Location = SPAWN_LOCATION_AT_CURRENT_POSITION") NET_NL()
				#ENDIF
				RETURN(SPAWN_LOCATION_AT_CURRENT_POSITION)
			ELSE
				
				
			
				IF (g_SpawnData.MissionSpawnDetails.SpawnArea[0].bIsActive)	
				
					IF IS_SPAWN_AREA_LARGE()
						g_SpawnData.MissionSpawnDetails.bUseNearCurrentCoordsForSpawnArea = TRUE
						PRINTLN("[spawning] GetPlayerSpawnLocationType - IS_SPAWN_AREA_LARGE = TRUE")
					ELSE
						g_SpawnData.MissionSpawnDetails.bUseNearCurrentCoordsForSpawnArea = FALSE
						PRINTLN("[spawning] GetPlayerSpawnLocationType - IS_SPAWN_AREA_LARGE = FALSE")
					ENDIF
				
					IF (g_SpawnData.MissionSpawnDetails.bUseNearCurrentCoordsForSpawnArea)
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION") NET_NL()
						#ENDIF						
						RETURN(SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION)
					ELSE
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_MISSION_AREA") NET_NL()
						#ENDIF						
						RETURN(SPAWN_LOCATION_MISSION_AREA)
					ENDIF
				ELSE
									
					IF NOT (g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation = SPAWN_LOCATION_AUTOMATIC)
						IF NOT IS_THREAD_ACTIVE(g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocationThreadID)
							NET_PRINT("[spawning] GetPlayerSpawnLocationType - resetting g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation") NET_NL()
							g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation = SPAWN_LOCATION_AUTOMATIC
						ENDIF
					ENDIF
					
					IF NOT (g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation = SPAWN_LOCATION_AUTOMATIC)
						NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation") NET_NL()
						RETURN(g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation)	
					ELSE			
						
						#IF FEATURE_HEIST_ISLAND
						IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
							IF IS_BEACH_PARTY_ACTIVE()
							AND (IS_PLAYER_RESTRICTED_TO_HEIST_ISLAND_BEACH_PARTY(PLAYER_ID()) OR (IsPointInBeachParty(g_SpawnData.vMyDeadCoords)))
								PRINTLN("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_HEIST_ISLAND_BEACH_PARTY") 
								RETURN(SPAWN_LOCATION_HEIST_ISLAND_BEACH_PARTY)
							ELSE
								PRINTLN("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_HEIST_ISLAND_NEAR_DEATH") 
								RETURN(SPAWN_LOCATION_HEIST_ISLAND_NEAR_DEATH)
							ENDIF
						ENDIF
						#ENDIF
					
					
						IF IS_PLAYER_ON_BOSSVBOSS_DM() 
							
							IF GB_IS_GOON_PLAYER_WITHIN_RESPAWN_RANGE_OF_BOSS(PLAYER_ID())
								NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_GANG_DM") NET_NL()
								RETURN(SPAWN_LOCATION_GANG_DM)
							ELSE	
								NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU (not near gang member)") NET_NL()
								RETURN(SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU)
							ENDIF
						ELSE
							IF IS_PLAYER_ON_IMPROMPTU_DM()
								NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU") NET_NL()
								RETURN(SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU)
							ELSE
								IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(TRUE)
								AND GB_IS_GOON_PLAYER_WITHIN_RESPAWN_RANGE_OF_BOSS(PLAYER_ID())
								AND (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GB_BELLY_BEAST
								OR (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_BELLY_BEAST
								AND GB_GET_LOCAL_PLAYER_GANG_BOSS() != GB_GET_LOCAL_PLAYER_MISSION_HOST()))
								AND NOT IS_BOSS_FLYING_A_VEHICLE()								
								AND NOT GB_IS_PLAYER_ON_CONTRABAND_MISSION(PLAYER_ID())
								AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
								AND NOT ((GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID()) = FALSE) AND (GB_IS_BOSS_INSIDE_GLOBAL_EXCLUSION_ZONE(PLAYER_ID()) = TRUE))
									
									IF IS_LOCAL_PLAYERS_GANG_BOSS_ON_THEIR_OWN_PRIVATE_YACHT()
										NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_GANG_BOSS_PRIVATE_YACHT") NET_NL()
										RETURN(SPAWN_LOCATION_GANG_BOSS_PRIVATE_YACHT)	
									ELIF IS_LOCAL_PLAYER_ON_THEIR_YACHT(10.0)
										RETURN(SPAWN_LOCATION_PRIVATE_YACHT)
									ELSE
									
										iYachtID = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
										IF IS_PRIVATE_YACHT_ACTIVE(iYachtID)
										AND DOES_LOCAL_PLAYER_HAVE_ACCESS_TO_YACHT(iYachtID)
										AND (IS_LOCAL_PLAYERS_GANG_BOSS_ON_YACHT(iYachtID) OR GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID()))
											PRINTLN("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_PRIVATE_FRIEND_YACHT, iYachtID ", iYachtID) 
											g_SpawnData.iYachtToWarpTo = iYachtID
											RETURN(SPAWN_LOCATION_PRIVATE_FRIEND_YACHT)			
										ELSE
									
										NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_NEAR_GANG_BOSS") NET_NL()
										RETURN(SPAWN_LOCATION_NEAR_GANG_BOSS)
										
										ENDIF
									ENDIF
								ELSE
									
									IF IS_LOCAL_PLAYER_ON_THEIR_YACHT(10.0)	
										RETURN(SPAWN_LOCATION_PRIVATE_YACHT)
									ELSE
									
										iYachtID = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
										IF IS_PRIVATE_YACHT_ACTIVE(iYachtID)
										AND DOES_LOCAL_PLAYER_HAVE_ACCESS_TO_YACHT(iYachtID)
											PRINTLN("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_PRIVATE_FRIEND_YACHT, iYachtID ", iYachtID) 
											g_SpawnData.iYachtToWarpTo = iYachtID
											RETURN(SPAWN_LOCATION_PRIVATE_FRIEND_YACHT)											
										ELSE
										
									
									

											NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_NEAR_DEATH") NET_NL()
											RETURN(SPAWN_LOCATION_NEAR_DEATH)
										
										ENDIF
									ENDIF
										
								ENDIF
							ENDIF
						ENDIF
	
					ENDIF
					
				ENDIF	

			
			ENDIF
			
		ENDIF
	
	ENDIF

	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] GetPlayerSpawnLocationType - RETURNING SPAWN_LOCATION_NEAR_DEATH") NET_NL()
	#ENDIF
	RETURN(SPAWN_LOCATION_NEAR_DEATH)

ENDFUNC

FUNC BOOL IsOffMissionExclusionAreaActive(OFF_MISSION_SPAWN_EXCLUSION_AREA SpArea)
	IF (SpArea.ExclusionArea.bIsActive)
		IF NETWORK_IS_SCRIPT_ACTIVE(GET_MP_MISSION_NAME(SpArea.Mission), SpArea.iMissionInstance)
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC


FUNC BOOL GetNearestFallbackSpawnpoint(VECTOR vCoords, VECTOR &vReturn)
	INT i
	INT iNearest = -1
	FLOAT fNearestDist = 999999.9
	FLOAT fDist
	IF (g_SpawnData.iNumberOfFallbackSpawnPoints > 0)
		REPEAT g_SpawnData.iNumberOfFallbackSpawnPoints i	
			fDist = VDIST(g_SpawnData.FallbackSpawnPoints[i].vPos, vCoords)
			IF (fDist < fNearestDist)
				iNearest = i
				fNearestDist = fDist
			ENDIF
		ENDREPEAT		
		IF NOT (iNearest = -1)			
			vReturn = g_SpawnData.FallbackSpawnPoints[iNearest].vPos
			RETURN TRUE		
		ENDIF				
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL IsSafePointOK(VECTOR vCoord, Vector &vReturn, BOOL bCheckSpawnExclusionZones, BOOL bCheckServerRefusalList)

	FLOAT fVDist

	fVDist = VDIST(vCoord, vReturn)		
	IF (fVDist < GET_SAFE_COORD_FINAL_DISTANCE)

		VECTOR vAvoidCoords[MAX_NUM_AVOID_RADIUS]
		FLOAT fAvoidRadius[MAX_NUM_AVOID_RADIUS]
	
		IF ((bCheckSpawnExclusionZones = TRUE) AND NOT IsNodeCoordInExclusionZone(g_SpawnData.vMyDeadCoords, vReturn, vAvoidCoords, fAvoidRadius, bCheckServerRefusalList))
		OR (bCheckSpawnExclusionZones = FALSE)
			
			IF NOT IsAnyFMMCPropAtCoord(vReturn)	
				IF NOT IS_POINT_IN_PROBLEM_AREA_FORBIDDEN(vReturn)
					NET_PRINT("[spawning] IsSafePointOK - 1 - VDIST = ") NET_PRINT_FLOAT(VDIST(vCoord, vReturn)) NET_PRINT(", vReturn = ") NET_PRINT_VECTOR(vReturn) NET_NL()				
					RETURN(TRUE)
				ELSE
					NET_PRINT("[spawning] IsSafePointOK - do node search - inside forbidden area") NET_NL()
				ENDIF
			ELSE
				NET_PRINT("[spawning] IsSafePointOK - do node search - inside fmmc prop") NET_NL()
			ENDIF
		ELSE	
			NET_PRINT("[spawning] IsSafePointOK - do node search - bCheckSpawnExclusionZones") NET_NL()
		ENDIF
	ELSE
		NET_PRINT("[spawning] IsSafePointOK - do node search - dist") NET_NL()
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL DidGetSafeCoordForPed(VECTOR vCoord, VECTOR &vReturn, GET_SAFE_COORD_FLAGS iFlag, BOOL bCheckSpawnExclusionZones, BOOL bCheckServerRefusalList)

	IF GetNearestFallbackSpawnpoint(vCoord, vReturn)
		IF IsSafePointOK(vCoord, vReturn, bCheckSpawnExclusionZones, bCheckServerRefusalList)
			RETURN TRUE
		ELSE
			NET_PRINT("[spawning] DidGetSafeCoordForPed - no safe fallback spawnpoints. ") NET_NL()	
		ENDIF
	ELSE
		NET_PRINT("[spawning] DidGetSafeCoordForPed - no fallback spawnpoints. ") NET_NL()	
	ENDIF

	
	IF GET_SAFE_COORD_FOR_PED(vCoord, FALSE, vReturn, iFlag)
		IF IsSafePointOK(vCoord, vReturn, bCheckSpawnExclusionZones, bCheckServerRefusalList)
			RETURN TRUE
		ELSE
			NET_PRINT("[spawning] DidGetSafeCoordForPed - no safe points from GET_SAFE_COORD_FOR_PED") NET_NL()	
		ENDIF
	ELSE
		NET_PRINT("[spawning] DidGetSafeCoordForPed - GET_SAFE_COORD_FOR_PED = false") NET_NL()
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsPointInsideArea(VECTOR vCoord, INT iShape, VECTOR vPoint1, VECTOR vPoint2, FLOAT fFloat)

	SWITCH iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE 	
			IF (VDIST(vCoord, vPoint1) <= fFloat)
				RETURN(TRUE)
			ENDIF
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			RETURN IsPointInsideBox(vCoord,vPoint1, vPoint2, FALSE,FALSE)
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED	
			RETURN IS_POINT_IN_ANGLED_AREA ( vCoord, vPoint1, vPoint2, fFloat)
		BREAK		
	ENDSWITCH

	RETURN(FALSE)
ENDFUNC

PROC GetSafeCoord(VECTOR &vCoord, BOOL bConsiderNonNetworkSpawn, BOOL bConsiderInactiveNodes, BOOL bCheckSpawnExclusionZones, BOOL bCheckWithinSearchArea, PRIVATE_SPAWN_SEARCH_PARAMS &PrivateParams, SPAWN_SEARCH_PARAMS &PublicParams)
//PROC GetSafeCoord(VECTOR &vCoord, FLOAT fMinPlayerDistance=0.0, BOOL bConsiderNonNetworkSpawn=FALSE, BOOL bConsiderHighways=FALSE, BOOL bConsiderInactiveNodes=FALSE, BOOL bCheckSpawnExclusionZones=FALSE, BOOL bConsiderInteriors=FALSE, BOOL bCheckMissionSpawnArea=FALSE)

	PRINTLN("[spawning] called GetSafeCoord with:")
	PRINTLN("	vCoord = ", vCoord)
	PRINTLN("	bConsiderNonNetworkSpawn = ", bConsiderNonNetworkSpawn)
	PRINTLN("	bConsiderInactiveNodes = ", bConsiderInactiveNodes)
	PRINTLN("	bCheckSpawnExclusionZones = ", bCheckSpawnExclusionZones)
	PRINTLN("	bCheckWithinSearchArea = ", bCheckWithinSearchArea)
	
	VECTOR vReturn
	FLOAT fHeading
	//FLOAT fVDist
	NearestCarNodeSearch SearchParams
	FLOAT fGroundZ
	INT i
	BOOL bDoNodeSearch

	GET_SAFE_COORD_FLAGS iFlag
	IF (bConsiderNonNetworkSpawn)
		iFlag = GSC_FLAG_DEFAULT
	ELSE
		iFlag = GSC_FLAG_ONLY_NETWORK_SPAWN
	ENDIF
	
	// don't consider interior points unless raw point is in an interior.
	IF NOT (PublicParams.bConsiderInteriors)	
		IF (PrivateParams.bIsForLocalPlayerSpawning)
			VECTOR vCentre
			IF NOT (PrivateParams.bAllowFallbackToUseInteriors)
				iFlag += GSC_FLAG_NOT_INTERIOR
				PRINTLN("[spawning] GetSafeCoord - don't allow fallback to use interiors  ")
			ELSE
				SWITCH PrivateParams.iAreaShape
					CASE SPAWN_AREA_SHAPE_CIRCLE 
						vCentre = PrivateParams.vSearchCoord
						IF IS_SPAWN_RADIUS_LARGE(PrivateParams.fSearchRadius)
						OR NOT IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vCentre))
							iFlag += GSC_FLAG_NOT_INTERIOR	
							PRINTLN("[spawning] GetSafeCoord - area is not small or inside  ")
						ELSE
							PRINTLN("[spawning] GetSafeCoord - area is small and inside  ")
						ENDIF
					BREAK
					CASE SPAWN_AREA_SHAPE_BOX	
						vCentre = (PrivateParams.vAngledAreaPoint1 + PrivateParams.vAngledAreaPoint2) * 0.5
						IF IS_SPAWN_ANGLED_AREA_LARGE(PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, 0.0)
						OR NOT IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vCentre))
							iFlag += GSC_FLAG_NOT_INTERIOR
							PRINTLN("[spawning] GetSafeCoord - box area is not small or inside  ")
						ELSE
							PRINTLN("[spawning] GetSafeCoord - box area is small and inside  ")	
						ENDIF
					BREAK
					CASE SPAWN_AREA_SHAPE_ANGLED
						vCentre = (PrivateParams.vAngledAreaPoint1 + PrivateParams.vAngledAreaPoint2) * 0.5
						IF IS_SPAWN_ANGLED_AREA_LARGE(PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, PrivateParams.fAngledAreaWidth)
						OR NOT IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vCentre))
							iFlag += GSC_FLAG_NOT_INTERIOR
							PRINTLN("[spawning] GetSafeCoord - angled area is not small or inside  ")
						ELSE
							PRINTLN("[spawning] GetSafeCoord - angled area is small and inside  ")	
						ENDIF
					BREAK
				ENDSWITCH
			

			ENDIF
		ELSE
			iFlag += GSC_FLAG_NOT_INTERIOR
			PRINTLN("[spawning] GetSafeCoord - not for player spawning  ")	
		ENDIF
	ENDIF
	NET_PRINT("[spawning] GetSafeCoord - iFlag = ") NET_PRINT_INT(ENUM_TO_INT(iFlag)) NET_NL()

	IF DidGetSafeCoordForPed(vCoord, vReturn, iFlag, bCheckSpawnExclusionZones, TRUE)
		PRINTLN("[spawning] GetSafeCoord - DidGetSafeCoordForPed = TRUE, vReturn = ", vReturn) 
	ELSE
		bDoNodeSearch = TRUE
	ENDIF


	
	
	IF (bDoNodeSearch)
	
		vReturn = vCoord
		SearchParams.vFavourFacing = PublicParams.vFacingCoords
		SearchParams.bDoSafeForSpawnCheck = TRUE
		SearchParams.fMinPlayerDist = PublicParams.fMinDistFromPlayer
		IF (bConsiderInactiveNodes)
			SearchParams.bConsiderOnlyActiveNodes = FALSE
		ELSE
			SearchParams.bConsiderOnlyActiveNodes = TRUE
		ENDIF
		SearchParams.bConsiderHighways = PrivateParams.bForAVehicle
		SearchParams.bCheckSpawnExclusionZones = bCheckSpawnExclusionZones
		IF (PublicParams.fUpperZLimitForNodes > 0.0)
			SearchParams.fUpperZLimit = PublicParams.fUpperZLimitForNodes
		ENDIF
	
		IF (bCheckWithinSearchArea)
			SearchParams.bCheckInsideArea = TRUE
			SearchParams.iAreaShape = PrivateParams.iAreaShape
			SWITCH PrivateParams.iAreaShape
				CASE SPAWN_AREA_SHAPE_CIRCLE 	
					SearchParams.vAreaCoords1 = PrivateParams.vSearchCoord
					SearchParams.fAreaFloat = PrivateParams.fSearchRadius
				BREAK
				CASE SPAWN_AREA_SHAPE_BOX		
					SearchParams.vAreaCoords1 = PrivateParams.vAngledAreaPoint1
					SearchParams.vAreaCoords2 = PrivateParams.vAngledAreaPoint2
					SearchParams.fAreaFloat = 0.0
				BREAK
				CASE SPAWN_AREA_SHAPE_ANGLED	
					SearchParams.vAreaCoords1 = PrivateParams.vAngledAreaPoint1
					SearchParams.vAreaCoords2 = PrivateParams.vAngledAreaPoint2
					SearchParams.fAreaFloat = PrivateParams.fAngledAreaWidth
				BREAK
			ENDSWITCH
		ENDIF
		
		REPEAT MAX_NUM_AVOID_RADIUS i 
			SearchParams.vAvoidCoords[i] = PublicParams.vAvoidCoords[i]
			SearchParams.fAvoidRadius[i] =	PublicParams.fAvoidRadius[i]
		ENDREPEAT
		
		SearchParams.bIsForFlyingVehicle = PublicParams.bIsForFlyingVehicle
		SearchParams.bDoVisibilityChecks = PrivateParams.bDoVisibleChecks
		
		IF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
			SearchParams.bGetSafeOffset = TRUE
		ENDIF
		
		SearchParams.bCanFaceOncomingTraffic = PrivateParams.bCanFaceOncomingTraffic
		
		GetNearestCarNode(vReturn, fHeading, SearchParams)	
		NET_PRINT("[spawning] GetSafeCoord - from node search - VDIST = ") NET_PRINT_FLOAT(VDIST(vCoord, vReturn)) NET_PRINT(", vReturn = ") NET_PRINT_VECTOR(vReturn) NET_NL()
		
	ENDIF
	

	// check if point returned is in mission spawn area
	IF (bCheckWithinSearchArea)
	
		VECTOR vPoint1, vPoint2
		FLOAT fFloat
		
		SWITCH PrivateParams.iAreaShape
			CASE SPAWN_AREA_SHAPE_CIRCLE 
				vPoint1 = PrivateParams.vSearchCoord
				fFloat = PrivateParams.fSearchRadius
			BREAK
			CASE SPAWN_AREA_SHAPE_BOX	
				vPoint1 = PrivateParams.vAngledAreaPoint1
				vPoint2 = PrivateParams.vAngledAreaPoint2
			BREAK
			CASE SPAWN_AREA_SHAPE_ANGLED
				vPoint1 = PrivateParams.vAngledAreaPoint1
				vPoint2 = PrivateParams.vAngledAreaPoint2
				fFloat = PrivateParams.fAngledAreaWidth
			BREAK
		ENDSWITCH
		
		
		IF NOT IsPointInsideArea(vReturn, PrivateParams.iAreaShape, vPoint1, vPoint2, fFloat)
		
			// do check above again, but without checking the server refusal list
			IF DidGetSafeCoordForPed(vCoord, vReturn, iFlag, bCheckSpawnExclusionZones, FALSE)
			
				PRINTLN("[spawning] GetSafeCoord - 2nd attempt without cheking for spawn exclusion - vReturn = ", vReturn) 
			
				// if it's still not inside the area, then just return the centre point, this is last resort.
				IF NOT IsPointInsideArea(vReturn, PrivateParams.iAreaShape, vPoint1, vPoint2, fFloat)
					
					IF (PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_ANGLED)
					OR (PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_BOX)
						vReturn =  (vPoint1 + vPoint2 ) * 0.5
					ELSE
						vReturn = vPoint1
					ENDIF
					
					PRINTLN("[spawning] GetSafeCoord - 2nd attempt still not inside area, returning centre point = ", vReturn) 
					
					IF GET_GROUND_Z_FOR_3D_COORD(vReturn, fGroundZ)
						PRINTLN("[spawning] GetSafeCoord - setting ground z = ", fGroundZ) 
						vReturn.z = fGroundZ	
					ENDIF
					
				ENDIF
			
			ELSE
				
				PRINTLN("[spawning] GetSafeCoord - DidGetSafeCoordForPed = FALSE, last check. ")
			
				// maybe this point is in a global exclusion that has an alternative spawn location? then move point outside and try again.
				IF IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vCoord, TRUE, TRUE, TRUE)
					PRINTLN("[spawning] GetSafeCoord - was in global exclusion zone, moving to ", vCoord)
					GetSafeCoord(vCoord, bConsiderNonNetworkSpawn, bConsiderInactiveNodes, bCheckSpawnExclusionZones, bCheckWithinSearchArea, PrivateParams, PublicParams)
				ELSE
			
					// couldn't get a safe coords, just return centre point as last resort
					IF (PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_ANGLED)
					OR (PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_BOX)
						vReturn =  (vPoint1 + vPoint2 ) * 0.5
					ELSE
						vReturn = vPoint1
					ENDIF
					
					PRINTLN("[spawning] GetSafeCoord - returning centre point = ", vReturn)
				
					IF GET_GROUND_Z_FOR_3D_COORD(vReturn, fGroundZ)
						PRINTLN("[spawning] GetSafeCoord - setting ground z = ", fGroundZ) 
						vReturn.z = fGroundZ	
					ENDIF	
				
				ENDIF
			
			ENDIF
		
		
		ELSE
			PRINTLN("[spawning] GetSafeCoord - vReturn is inside search area. ") 
		ENDIF
	ENDIF	
	
	
	
	vCoord = vReturn
	PRINTLN("[spawning] GetSafeCoord - returning ", vReturn)	
	
	g_SpawnData.bFallbackUsed = TRUE
	
ENDPROC



FUNC BOOL IsSpawnLocationAtGanghouse(BOOL bIncludeNearestProperty=TRUE)
	IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_OUTSIDE_SIMEON_GARAGE)
	OR (g_SpawnData.SpawnLocation = SPAWN_LOCATION_OUTSIDE_SIMEON_GARAGE)
	OR ((g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAREST_RESPAWN_POINT) AND (bIncludeNearestProperty=TRUE))
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL ShouldDoVisibleChecksForSpawnLocation()
	// if using the mission spawn area can the whole area already be seen?
	IF IS_SPAWN_LOCATION_MISSION_AREA_CHECK(g_SpawnData.SpawnLocation) 
		IF AreAllTheMissionSpawnAreasEntirelyVisible()
			RETURN(FALSE)
		ELSE
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] ShouldDoVisibleChecksForSpawnLocation() - AreAllTheMissionSpawnAreasEntirelyVisible = FALSE - returning TRUE") NET_NL()
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] ShouldDoVisibleChecksForSpawnLocation() - SpawnLocation NOT equal to SPAWN_LOCATION_MISSION_AREA - returning TRUE") NET_NL()
		#ENDIF
	ENDIF
	// if spawning at new start dont bother with visible checks
	IF ((g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS) AND NOT (g_SpecificSpawnLocation.bDoVisibleChecks)) 
	OR ((g_SpawnData.SpawnLocation = SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA) AND NOT (g_SpecificSpawnLocation.bDoVisibleChecks)) 
	OR ((g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE) AND NOT (g_SpecificSpawnLocation.bDoVisibleChecks)) 
	OR ((g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG) AND NOT (g_SpecificSpawnLocation.bDoVisibleChecks)) 
	OR (g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS)
	OR (g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS_RACE_CORONA)
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] ShouldDoVisibleChecksForSpawnLocation() - SPAWN_LOCATION_GANG_HOUSE_REC_ROOM - returning FALSE") NET_NL()
		#ENDIF
		RETURN(FALSE)
	ENDIF

	RETURN(TRUE)
ENDFUNC

FUNC FLOAT GetRadiusOfCustomSpawnPoints()
	INT i
	VECTOR vMax
	VECTOR vMin
	VECTOR vDiff
	
	vMax = <<-99999.9, -99999.9, -99999.9>>
	vMin = <<99999.9, 99999.9, 99999.9>>
	
	REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i				
		IF (g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.x > vMax.x)
			vMax.x = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.x	
		ENDIF
		IF (g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.y > vMax.y)
			vMax.y = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.y	
		ENDIF
		IF (g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.z > vMax.z)
			vMax.z = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.z	
		ENDIF
		IF (g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.x < vMin.x)
			vMin.x = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.x	
		ENDIF
		IF (g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.y < vMin.y)
			vMin.y = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.y	
		ENDIF
		IF (g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.z < vMin.z)
			vMin.z = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.z	
		ENDIF
	ENDREPEAT
	
	// return the larger diff
	vDiff = vMax - vMin
	
	RETURN(VMAG(vDiff) / 2.0)
	
ENDFUNC



FUNC FLOAT GetDistanceFromPointToPlayerForSpawnPoint(VECTOR vPoint, BOOL bCheckEnemies, BOOL bCheckTeammates, BOOL bCheckSelf=FALSE, BOOL bCheckTutorialSession=TRUE, BOOL bCheckNonJoinedPlayers=FALSE)
	INT iPlayer
	FLOAT fDist, fDist2
	FLOAT fClosestDist = 999999.9
	BOOL bCheckPlayer = FALSE
	VECTOR vPlayerCoords, vPlayerCoords2
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID)
		OR ((bCheckNonJoinedPlayers = TRUE) AND IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE))
			IF NOT (PlayerID = PLAYER_ID())
			OR (bCheckSelf = TRUE)
					
				bCheckPlayer = FALSE
				
				IF (bCheckEnemies)
					IF IsPlayerConsideredEnemy(PlayerID)
						bCheckPlayer = TRUE
					ENDIF	
				ENDIF
				
				IF (bCheckTeammates)
					IF (GET_PLAYER_TEAM(PlayerID) = GET_PLAYER_TEAM(PLAYER_ID()))
						IF NOT (GET_PLAYER_TEAM(PlayerID) = -1)
						OR NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID()) 				
							bCheckPlayer = TRUE									
						ENDIF
					ENDIF		
				ENDIF

							
				IF (bCheckPlayer)
					IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)	
					OR NOT (bCheckTutorialSession)
						IF IS_PLAYER_VISIBLE_TO_SCRIPT(PlayerID)
							vPlayerCoords = GET_PLAYER_COORDS(PlayerID)
							IF NOT (PlayerID = PLAYER_ID())
								vPlayerCoords2 = NETWORK_GET_LAST_ENTITY_POS_RECEIVED_OVER_NETWORK(GET_PLAYER_PED(PlayerID)) 
							ELSE
								vPlayerCoords2 = vPlayerCoords
							ENDIF
							
							// if players are in another tutorial session their z is -190.0 so we need to ignore this, just do a 2d check.
							IF NOT (bCheckTutorialSession)
								IF (vPlayerCoords.z < -100.0)
									vPlayerCoords.z = vPoint.z	
								ENDIF
								IF (vPlayerCoords2.z < -100.0)
									vPlayerCoords2.z = vPoint.z	
								ENDIF
							ENDIF
							fDist = GET_DISTANCE_BETWEEN_COORDS (vPoint, vPlayerCoords)		
							fDist2 = GET_DISTANCE_BETWEEN_COORDS (vPoint, vPlayerCoords2)		
							
							#IF IS_DEBUG_BUILD
							IF (g_SpawnData.bShowAdvancedSpew)
								NET_PRINT("[spawning] GetDistanceFromPointToPlayerForSpawnPoint - vPoint = ") NET_PRINT_VECTOR(vPoint) 
								NET_PRINT(" GET_PLAYER_COORDS(") NET_PRINT_INT(NATIVE_TO_INT(PlayerID)) NET_PRINT(") = ") NET_PRINT_VECTOR(GET_PLAYER_COORDS(PlayerID))
								NET_PRINT(" vPlayerCoords = ") NET_PRINT_VECTOR(vPlayerCoords)
								NET_PRINT(" vPlayerCoords2 = ") NET_PRINT_VECTOR(vPlayerCoords2)
								NET_PRINT(" fDist = ") NET_PRINT_FLOAT(fDist)
								NET_PRINT(" fDist2 = ") NET_PRINT_FLOAT(fDist2)
								NET_PRINT(" fClosestDist = ") NET_PRINT_FLOAT(fClosestDist)
								NET_NL()
							ENDIF
							#ENDIF
							
 							IF (fDist < fClosestDist)
								fClosestDist = fDist
							ENDIF		
							IF (fDist2 < fClosestDist)
								fClosestDist = fDist2
							ENDIF		
							
						ENDIF											
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(fClosestDist)
ENDFUNC


FUNC FLOAT GetDistanceFromPointToHostilePedForSpawnPoint(VECTOR vPoint)


	FLOAT fClosestDist 
	FLOAT fDist
	INT iPed
	VECTOR vPedCoords
	PED_INDEX PedArray[32]
	INT iNumberOfPeds
	
	iNumberOfPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), PedArray, PEDTYPE_PLAYER_NETWORK)
	fClosestDist = 9999999.9
	
	PRINTLN("[spawning] GetDistanceFromPointToHostilePedForSpawnPoint - iNumberOfPeds = ", iNumberOfPeds)
	
	REPEAT iNumberOfPeds iPed
		IF DOES_ENTITY_EXIST(PedArray[iPed])
			IF NOT IS_ENTITY_DEAD(PedArray[iPed])
				IF IS_PED_AGGRESSOR_TOWARDS_PLAYER(PedArray[iPed])
				
					vPedCoords = GET_ENTITY_COORDS(PedArray[iPed])				
					fDist = GET_DISTANCE_BETWEEN_COORDS (vPoint, vPedCoords)		
					
					PRINTLN("[spawning] GetDistanceFromPointToHostilePedForSpawnPoint - iPed ", iPed, ", vPoint ", vPoint, ", vPedCoords ", vPedCoords, ", fDist ", fDist)	
					
					IF (fDist < fClosestDist)
						PRINTLN("[spawning] GetDistanceFromPointToHostilePedForSpawnPoint - setting this as closest")
						fClosestDist = fDist
					ENDIF					
				
				ELSE
					PRINTLN("[spawning] GetDistanceFromPointToHostilePedForSpawnPoint - ped non aggressive.  iPed ", iPed)					
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	IF (g_SpawnData.bCheckNetNPCs)
		IF (gBG_MC_serverBD_VARS.iNumPeds > 0)
			REPEAT gBG_MC_serverBD_VARS.iNumPeds iPed
				IF DOES_ENTITY_EXIST(gBG_MC_serverBD_VARS.piPeds[iPed])
					IF NOT IS_ENTITY_DEAD(gBG_MC_serverBD_VARS.piPeds[iPed])
						IF IS_PED_AGGRESSOR_TOWARDS_PLAYER(gBG_MC_serverBD_VARS.piPeds[iPed])
						
							vPedCoords = GET_ENTITY_COORDS(gBG_MC_serverBD_VARS.piPeds[iPed])				
							fDist = GET_DISTANCE_BETWEEN_COORDS (vPoint, vPedCoords)		
							
							PRINTLN("[spawning] GetDistanceFromPointToHostilePedForSpawnPoint - gBG_MC_serverBD_VARS.niPeds ", iPed, ", vPoint ", vPoint, ", vPedCoords ", vPedCoords, ", fDist ", fDist)	
							
							IF (fDist < fClosestDist)
								PRINTLN("[spawning] GetDistanceFromPointToHostilePedForSpawnPoint - setting this as closest")
								fClosestDist = fDist
							ENDIF					
						
						ELSE
							PRINTLN("[spawning] GetDistanceFromPointToHostilePedForSpawnPoint - ped non aggressive.  iPed ", iPed)					
						ENDIF
					ENDIF
				ENDIF								
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN(fClosestDist)
ENDFUNC

STRUCT CUSTOM_SPAWN_SCORING_DATA
	INT iHighestScoringPoint[NUM_OF_STORED_SPAWN_RESULTS]
	FLOAT fHighestScore[NUM_OF_STORED_SPAWN_RESULTS]
	FLOAT fNearestEnemy[NUM_OF_STORED_SPAWN_RESULTS]
ENDSTRUCT

FUNC FLOAT GetDistanceFromPointToCurrentResults(VECTOR vPoint, CUSTOM_SPAWN_SCORING_DATA &CustomSpawnData)

	FLOAT fClosestDist = 9999999.9
	FLOAT fDist
	INT i
	REPEAT NUM_OF_STORED_SPAWN_RESULTS i
		IF (CustomSpawnData.iHighestScoringPoint[i] > -1)	
			PRINTLN("[spawning] GetDistanceFromPointToCurrentResults - point ", CustomSpawnData.iHighestScoringPoint[i])
			fDist = GET_DISTANCE_BETWEEN_COORDS (vPoint, g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[CustomSpawnData.iHighestScoringPoint[i]].vPos)	
			IF (fDist < fClosestDist)
				PRINTLN("[spawning] GetDistanceFromPointToCurrentResults - setting this as closest")
				fClosestDist = fDist
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(fClosestDist)
ENDFUNC


FUNC FLOAT GetPointScoreFromDistances(FLOAT fInDist, FLOAT fMinDist, FLOAT fOptDist, FLOAT fMaxDist, FLOAT fMinDistMinScoreValue=0.0, FLOAT fMaxDistMinScoreValue=0.0)

	FLOAT fThisScore
	
	// if the min distance is less than the max distance then make it half the value
	IF (fMinDist >= fMaxDist)	
		fMinDist = fMaxDist * 0.5
	ENDIF
	
	// make sure the dist is not greater or less than the max/min
	IF (fInDist < fMinDist)
		fInDist = fMinDist
	ENDIF
	IF (fInDist > fMaxDist)
		fInDist = fMaxDist
	ENDIF

	// if the optimum distance is a valid value
	IF (fOptDist < fMaxDist)
	AND (fOptDist >  fMinDist)
		
		#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
			NET_PRINT("GetPointScoreFromDistances - valid optimum dist - ") 
		ENDIF
		#ENDIF
		
		IF (fInDist < fOptDist)
			fThisScore = fMinDistMinScoreValue + ( (1.0-fMinDistMinScoreValue) - ((1.0-fMinDistMinScoreValue) * ((fOptDist - fInDist) / (fOptDist-fMinDist))))
		ELSE
			fThisScore = fMaxDistMinScoreValue + ((1.0-fMaxDistMinScoreValue) - ((1.0-fMaxDistMinScoreValue) * ((fInDist - fOptDist) / (fMaxDist - fOptDist))))
		ENDIF	
		
	ELSE
		// ignore the optimum dist
		#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
			NET_PRINT("GetPointScoreFromDistances - invalid optimum dist - ")
		ENDIF
		#ENDIF
		
		fThisScore = fMinDistMinScoreValue + (((fInDist - fMinDist) / (fMaxDist - fMinDist)) * (fMaxDistMinScoreValue - fMinDistMinScoreValue))	
	ENDIF

#IF IS_DEBUG_BUILD
	IF (g_SpawnData.bShowAdvancedSpew)
	NET_PRINT("fInDist=")
	NET_PRINT_FLOAT(fInDist)
	NET_PRINT(",fMinDist=")
	NET_PRINT_FLOAT(fMinDist)
	NET_PRINT(",fOptDist=")
	NET_PRINT_FLOAT(fOptDist)
	NET_PRINT(",fMaxDist=")
	NET_PRINT_FLOAT(fMaxDist)
	NET_PRINT(",fMinDistMinScoreValue=")
	NET_PRINT_FLOAT(fMinDistMinScoreValue)
	NET_PRINT(",fMaxDistMinScoreValue=")
	NET_PRINT_FLOAT(fMaxDistMinScoreValue)
	NET_PRINT("fThisScore=")
	NET_PRINT_FLOAT(fThisScore)
	NET_NL()		
	ENDIF
#ENDIF
		
	RETURN(fThisScore)		
ENDFUNC

FUNC BOOL IsAnyEntityObstructingPoint(VECTOR vPoint, BOOL bConsiderScriptCreatedObjects, BOOL bDoWorldObjectChecks, BOOL bForAVehicleSpawn, FLOAT fPedDistRadius=CUSTOM_SPAWN_PED_CLEARANCE_RADIUS, FLOAT fVehDistRadius=CUSTOM_SPAWN_VEHICLE_CLEARANCE_RADIUS, FLOAT fWorldObjectDistRadius=CUSTOM_SPAWN_WORLD_OBJECT_CLEARANCE_RADIUS, FLOAT fScriptObjectDistRadius=CUSTOM_SPAWN_SCRIPT_OBJECT_CLEARANCE_RADIUS)
	
	FLOAT fPedDistCheck = fPedDistRadius 
	IF (bForAVehicleSpawn)
		fPedDistCheck = fVehDistRadius 
	ENDIF
	
	IF ((fVehDistRadius > 0.0) AND IS_ANY_VEHICLE_NEAR_POINT(vPoint, fVehDistRadius))
	OR ((fPedDistCheck > 0.0) AND IS_ANY_PED_NEAR_POINT(vPoint, fPedDistCheck))
	OR ((bDoWorldObjectChecks=TRUE) AND (fWorldObjectDistRadius>0.0) AND IS_ANY_OBJECT_NEAR_POINT(vPoint, fWorldObjectDistRadius))
	OR ((bConsiderScriptCreatedObjects=TRUE) AND (fScriptObjectDistRadius>0.0) AND IS_ANY_OBJECT_NEAR_POINT(vPoint, fScriptObjectDistRadius, TRUE))
	
		#IF IS_DEBUG_BUILD
			IF ((fVehDistRadius > 0.0) AND IS_ANY_VEHICLE_NEAR_POINT(vPoint, fVehDistRadius))
				PRINTLN("IsAnyEntityObstructingPoint - true, vehicle near point")
			ENDIF
			IF ((fPedDistCheck > 0.0) AND IS_ANY_PED_NEAR_POINT(vPoint, fPedDistCheck))
				PRINTLN("IsAnyEntityObstructingPoint - true, ped near point")
			ENDIF
			IF ((bDoWorldObjectChecks=TRUE) AND (fWorldObjectDistRadius>0.0) AND IS_ANY_OBJECT_NEAR_POINT(vPoint, fWorldObjectDistRadius))
				PRINTLN("IsAnyEntityObstructingPoint - true, world object near point")
			ENDIF
			IF ((bConsiderScriptCreatedObjects=TRUE) AND (fScriptObjectDistRadius>0.0) AND IS_ANY_OBJECT_NEAR_POINT(vPoint, fScriptObjectDistRadius, TRUE))
				PRINTLN("IsAnyEntityObstructingPoint - true, script object near point")
			ENDIF
		#ENDIF
	
		RETURN(TRUE)	
	ENDIF
	RETURN(FALSE)
ENDFUNC


PROC InsertCustomSpawnPointIntoScoreData(CUSTOM_SPAWN_SCORING_DATA &CustomSpawnData , INT iPoint, FLOAT fScore, INT iSlot, FLOAT fNearestEnemy)
	INT iStoredPoint
	FLOAT fStoredScore
	FLOAT fStoredNearestEnemy
	iStoredPoint = CustomSpawnData.iHighestScoringPoint[iSlot]
	fStoredScore = CustomSpawnData.fHighestScore[iSlot]
	fStoredNearestEnemy = CustomSpawnData.fNearestEnemy[iSlot]
	
	CustomSpawnData.iHighestScoringPoint[iSlot] = iPoint
	CustomSpawnData.fHighestScore[iSlot] = fScore
	CustomSpawnData.fNearestEnemy[iSlot] = fNearestEnemy
	
	PRINTLN("InsertCustomSpawnPointIntoScoreData - called with point ", iPoint, ", fScore = ", fScore, ", iSlot = ", iSlot, ", fNearestEnemy = ", fNearestEnemy)
	
	IF (iSlot < NUM_OF_STORED_SPAWN_RESULTS - 1)
		InsertCustomSpawnPointIntoScoreData(CustomSpawnData, iStoredPoint, fStoredScore, iSlot+1, fStoredNearestEnemy)
	ENDIF
	
ENDPROC

FUNC FLOAT GetOptimumEnemyDist()	
	IF IS_SPAWNING_IN_VEHICLE()
		IF (IS_THIS_MODEL_A_PLANE(g_SpawnData.MissionSpawnDetails.SpawnModel) OR IS_THIS_MODEL_A_HELI(g_SpawnData.MissionSpawnDetails.SpawnModel) OR (g_SpawnData.MissionSpawnDetails.SpawnModel = RHINO))	
			RETURN(CUSTOM_SPAWN_OPT_DIST_FROM_ENEMY_AIR_VEHICLE)
		ELSE	
			RETURN(CUSTOM_SPAWN_OPT_DIST_FROM_ENEMY_VEHICLE)
		ENDIF
	ENDIF
	RETURN(CUSTOM_SPAWN_OPT_DIST_FROM_ENEMY)	
ENDFUNC

FUNC FLOAT GetMaxEnemyDist()	
	IF IS_SPAWNING_IN_VEHICLE()
		IF (IS_THIS_MODEL_A_PLANE(g_SpawnData.MissionSpawnDetails.SpawnModel) OR IS_THIS_MODEL_A_HELI(g_SpawnData.MissionSpawnDetails.SpawnModel) OR (g_SpawnData.MissionSpawnDetails.SpawnModel = RHINO))	
			RETURN(CUSTOM_SPAWN_MAX_DIST_FROM_ENEMY_AIR_VEHICLE)
		ELSE	
			RETURN(CUSTOM_SPAWN_MAX_DIST_FROM_ENEMY_VEHICLE)
		ENDIF
	ENDIF
	RETURN(CUSTOM_SPAWN_MAX_DIST_FROM_ENEMY)	
ENDFUNC

FUNC CUSTOM_SPAWN_SCORING_DATA ReturnCustomSpawnPointWithHighestScore(BOOL bForAVehicleSpawn)

	PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - called. g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints = ", g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints)

	CUSTOM_SPAWN_SCORING_DATA CustomSpawnData
	INT i

	// initialise scoring data before we do anything eles. 
	REPEAT NUM_OF_STORED_SPAWN_RESULTS i
		CustomSpawnData.iHighestScoringPoint[i] = -1
		CustomSpawnData.fHighestScore[i] = -1.0
	ENDREPEAT


	#IF IS_DEBUG_BUILD
		IF (g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints <= 0)
			SCRIPT_ASSERT("ReturnCustomSpawnPointWithHighestScore - g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints < 0, check custom spawn points have been setup correctly")
		ENDIF
	#ENDIF

	// if there is only 1 custom spawn point just return that (even though it might be unsafe)
	IF (g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints = 1)
		CustomSpawnData.iHighestScoringPoint[0] = 0
		CustomSpawnData.fHighestScore[0] = 1.0

		NET_PRINT("[spawning] ReturnCustomSpawnPointWithHighestScore - only 1 custom spawn point setup, returning it. ") NET_NL()

		RETURN (CustomSpawnData)
	ENDIF
	
	// if we are during a player switch then check non fully joined players
	BOOL bCheckNonJoinedPlayers = FALSE
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		bCheckNonJoinedPlayers = TRUE
		PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - IS_PLAYER_SWITCH_IN_PROGRESS, so setting bCheckNonJoinedPlayers = TRUE")
	ENDIF


	FLOAT fThisScore
	FLOAT fScore
	FLOAT fDist
	FLOAT fNearestEnemy
	INT iTop
	//BOOL bGotAPoint
	
	FLOAT fMinScore = 0.0
	IF (g_SpawnData.CustomSpawnPointInfo.bNeverFallBackToNavMesh)
		fMinScore = 0.001
	ENDIF

	#IF IS_DEBUG_BUILD
	IF (g_SpawnData.bShowAdvancedSpew)
	PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - g_SpawnData.CustomSpawnPointInfo.iLastUsedCustomSpawnPoint =  ",g_SpawnData.CustomSpawnPointInfo.iLastUsedCustomSpawnPoint)
	ENDIF
	#ENDIF

	REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i	
		
		PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - analysing point ",i," at ",g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos)
		
		// start with a score of 1.0
		fScore = 1.0
		fNearestEnemy = 0.0
		
		// is this a fallback after spawning inside a ped or vehicle?
		IF (g_SpawnData.bHasAttemptedFallbackUsingCustomPoints)
		AND (g_SpawnData.CustomSpawnPointInfo.iLastUsedCustomSpawnPoint = i)

			fScore = fMinScore			
			fNearestEnemy = 0.01
			
			NET_PRINT("[spawning] ReturnCustomSpawnPointWithHighestScore - was last result that failed due to being inside ped or vehicle ")  NET_NL()			
		
		ELSE
			
			// don't use point that was last used
			IF (g_SpawnData.CustomSpawnPointInfo.iLastUsedCustomSpawnPoint = i)
			AND (g_SpawnData.CustomSpawnPointInfo.bIgnorePreviousSpawnPoint)
			
				fScore = fMinScore			
				fNearestEnemy = 0.01
				
				NET_PRINT("[spawning] ReturnCustomSpawnPointWithHighestScore - was last spawn point ")  NET_NL()			
			
			ELSE
				// ********* is point obstructed **************
				IF NOT (g_SpawnData.CustomSpawnPointInfo.bIgnoreObstructionChecks)
					IF IsAnyEntityObstructingPoint(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, FALSE, TRUE, bForAVehicleSpawn, g_SpawnData.CustomSpawnPointInfo.fCustomSpawnPedClearance, g_SpawnData.CustomSpawnPointInfo.fCustomSpawnVehicleClearance, g_SpawnData.CustomSpawnPointInfo.fCustomSpawnWorldObjectClearance, g_SpawnData.CustomSpawnPointInfo.fCustomSpawnScriptObjectClearance)		
						fScore = fMinScore
						NET_PRINT("[spawning] ReturnCustomSpawnPointWithHighestScore - IsAnyEntityObstructingPoint = TRUE ")  NET_NL()			
					ENDIF
				ELSE
					PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - (SKIP Obstruction check) g_SpawnData.CustomSpawnPointInfo.bIgnoreDistanceChecks = TRUE ",fScore)
				ENDIF
				
				// ********* is point on server refusal list **************
				IF IS_POINT_IN_SERVER_REFUSAL_LIST(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, 0.1)
					fScore = fMinScore
					NET_PRINT("[spawning] ReturnCustomSpawnPointWithHighestScore - IS_POINT_IN_SERVER_REFUSAL_LIST = TRUE ")  NET_NL()	
				ENDIF
				
				// ********* is point on screen of someone's machine? **************
				IF CAN_ANY_PLAYER_SEE_POINT(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, 1.0, FALSE, FALSE, 100.0, TRUE, DEFAULT, DEFAULT, TRUE)
					NET_PRINT("[spawning] ReturnCustomSpawnPointWithHighestScore - point is visible at 100.0 ")  NET_NL()
					fScore *= CUSTOM_SPAWN_WEIGHTING_ON_SCREEN
				ENDIF
				IF CAN_ANY_PLAYER_SEE_POINT(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, 1.0, FALSE, FALSE, 50.0, TRUE, DEFAULT, DEFAULT, TRUE)
					NET_PRINT("[spawning] ReturnCustomSpawnPointWithHighestScore - point is visible at 50.0 ")  NET_NL()
					fScore *= CUSTOM_SPAWN_WEIGHTING_ON_SCREEN
				ENDIF
				IF CAN_ANY_PLAYER_SEE_POINT(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, 1.0, FALSE, FALSE, 25.0, TRUE, DEFAULT, DEFAULT, TRUE)
					NET_PRINT("[spawning] ReturnCustomSpawnPointWithHighestScore - point is visible at 25.0 ")  NET_NL()
					fScore *= CUSTOM_SPAWN_WEIGHTING_ON_SCREEN
				ENDIF
				PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - score after visible checks ",fScore)
					
					
				IF NOT (g_SpawnData.CustomSpawnPointInfo.bIgnoreDistanceChecks)
				

					// ********* score based on distance to other currently warping players **********
					fDist = ClosestCurrentWarpingPlayerToPosition(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, PLAYER_ID())
					PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - nearest warping player is ",fDist)
					IF (fDist < 10000.0)
						fThisScore = GetPointScoreFromDistances(fDist, g_SpawnData.CustomSpawnPointInfo.fCustomSpawnMinDistWarper, -1.0, 100.0, fMinScore, 1.0)
						fScore *= fThisScore * CUSTOM_SPAWN_WEIGHTING_DIST_FROM_WARPERS
					ENDIF
					PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - score after warpers check ",fScore)
					
					// ********* score based on distance to nearest enemy player **********
					fDist = GetDistanceFromPointToPlayerForSpawnPoint(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, TRUE, FALSE, DEFAULT, FALSE, bCheckNonJoinedPlayers)
					PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - nearest enemy is ",fDist)
					fNearestEnemy = fDist
					fThisScore = GetPointScoreFromDistances(fDist, g_SpawnData.CustomSpawnPointInfo.fCustomSpawnMinDistEnemy, GetOptimumEnemyDist(), GetMaxEnemyDist(), fMinScore, 0.3)
					fScore *= fThisScore * CUSTOM_SPAWN_WEIGHTING_DIST_FROM_ENEMY
					PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - score after enemy check ",fScore)	

					// *********  score based on nearest teammate **********
					fDist = GetDistanceFromPointToPlayerForSpawnPoint(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, FALSE, TRUE, DEFAULT, FALSE, bCheckNonJoinedPlayers)
					PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - nearest teammate is ",fDist)
					IF (fDist < 10000.0)
						fThisScore = GetPointScoreFromDistances(fDist, g_SpawnData.CustomSpawnPointInfo.fCustomSpawnMinDistTeammate, 15.0, CUSTOM_SPAWN_MAX_DIST_FROM_TEAMMATE, fMinScore, 0.2)
						fScore *= fThisScore * CUSTOM_SPAWN_WEIGHTING_DIST_FROM_TEAMMATE
					ENDIF
					PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - score after teammate check ",fScore)
					
					// *********  score based on last death location **********
					fDist = GET_DISTANCE_BETWEEN_COORDS(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, g_SpawnData.vMyDeadCoords)
					PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - dist from death is ",fDist)
					fThisScore = GetPointScoreFromDistances(fDist, 0.0, 0.0, g_SpawnData.CustomSpawnPointInfo.fCustomMaxDistFromDeath, fMinScore, CUSTOM_SPAWN_MAX_DIST_FROM_DEATH/g_SpawnData.CustomSpawnPointInfo.fCustomMaxDistFromDeath)
					fScore *= fThisScore * CUSTOM_SPAWN_WEIGHTING_FROM_DEATH
					PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - score after death check ",fScore)
				
					IF (g_SpawnData.CustomSpawnPointInfo.bAvoidHostileNPCs)
						// *********  score based on hostile npcs **********
						fDist = GetDistanceFromPointToHostilePedForSpawnPoint(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos)
						PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - nearest hostile npc is ",fDist)
						fThisScore = GetPointScoreFromDistances(fDist, g_SpawnData.CustomSpawnPointInfo.fCustomSpawnMinDistEnemy, GetOptimumEnemyDist(), GetMaxEnemyDist(), fMinScore, 0.3)
						fScore *= fThisScore * CUSTOM_SPAWN_WEIGHTING_DIST_FROM_HOSTILE_NPCS
						PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - score after hostile npc check ",fScore)			
					ENDIF
					
					// *********  score based on current results **********
					IF (g_SpawnData.CustomSpawnPointInfo.bAvoidResultClumping)
						fDist = GetDistanceFromPointToCurrentResults(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, CustomSpawnData)
						PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - nearest current result is ",fDist)
						fThisScore = GetPointScoreFromDistances(fDist, 0.0, 0.0, 65.0, 0.8, 1.0)
						fScore *= fThisScore * CUSTOM_SPAWN_WEIGHTING_DIST_FROM_CURRENT_RESULTS
						PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - score after current results check ",fScore)	
					ENDIF
					
				ELSE
					PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - g_SpawnData.CustomSpawnPointInfo.bIgnoreDistanceChecks = TRUE ",fScore)	
				ENDIF

				// *********   apply user weighting  *********  
				PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - user weighting is ",g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].fWeighting)
				fScore *= g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].fWeighting
				PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - score after user weighting ",fScore)
		
			ENDIF
			
		ENDIF
		
		// check final score
		PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - final score for point ",i," ",fScore)		
		REPEAT NUM_OF_STORED_SPAWN_RESULTS iTop
			IF (fScore > 0.0)
			OR (g_SpawnData.CustomSpawnPointInfo.bNeverFallBackToNavMesh)
				IF (fScore > CustomSpawnData.fHighestScore[iTop]) 
					PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - new highest score")	
					InsertCustomSpawnPointIntoScoreData(CustomSpawnData, i, fScore, iTop, fNearestEnemy)
					//bGotAPoint = TRUE
					iTop = NUM_OF_STORED_SPAWN_RESULTS
				ELIF (fScore = CustomSpawnData.fHighestScore[iTop]) 
					// random choose if we should go with this
					IF GET_RANDOM_INT_IN_RANGE(0,2) > 0
						PRINTLN("[spawning] ReturnCustomSpawnPointWithHighestScore - new highest score (after tied score) ")
						InsertCustomSpawnPointIntoScoreData(CustomSpawnData, i, fScore, iTop, fNearestEnemy)						
						//bGotAPoint = TRUE
						iTop = NUM_OF_STORED_SPAWN_RESULTS
					ENDIF		
				ENDIF
			ENDIF
		ENDREPEAT

	ENDREPEAT

//	// return the best score
//	IF NOT (bGotAPoint)
//		// no point is suitable, pick random point
//		CustomSpawnData.iHighestScoringPoint[0] = GET_RANDOM_INT_IN_RANGE(0, g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints)
//		NET_PRINT("[spawning] ReturnCustomSpawnPointWithHighestScore - choosing a random point ") NET_NL()
//	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] ReturnCustomSpawnPointWithHighestScore - returning points: ")
	REPEAT NUM_OF_STORED_SPAWN_RESULTS i
		NET_PRINT_INT(CustomSpawnData.iHighestScoringPoint[i])
		NET_PRINT(" ")
	ENDREPEAT
	NET_NL()
	#ENDIF
	

	RETURN(CustomSpawnData)	
ENDFUNC


//PROC GetCustomSpawnPoint(VECTOR &vCoord, FLOAT &fHeading, BOOL bSaveLastSpawnPoint)
//
////	IF (g_SpawnData.bInitialSpawnForDM)
////		GetCustomSpawnPointUsingOldMethod()	
////		g_SpawnData.bInitialSpawnForDM = FALSE	
////	ELSE
//		INT iHighestScoringCustomPoint = ReturnCustomSpawnPointWithHighestScore()		
//		vCoord = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[iHighestScoringCustomPoint].vPos
//		fHeading = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[iHighestScoringCustomPoint].fHeading
//		IF (bSaveLastSpawnPoint)
//			g_SpawnData.CustomSpawnPointInfo.iLastCustomSpawnPoint = iHighestScoringCustomPoint
//		ENDIF
////	ENDIF
//	
//ENDPROC



FUNC BOOL GetDistToNearestCarNode(VECTOR vCoord, FLOAT &fDist, FLOAT &fZDist)
	VECTOR vNodePos
	VEHICLE_NODE_ID NodeID
	INT iLanes
	FLOAT fHeading	
	NodeID = GET_NTH_CLOSEST_VEHICLE_NODE_ID_WITH_HEADING(vCoord, 1, fHeading, iLanes, NF_INCLUDE_SWITCHED_OFF_NODES )		
	IF IS_VEHICLE_NODE_ID_VALID(NodeID)		
		GET_VEHICLE_NODE_POSITION( NodeID, vNodePos )

		fDist = VDIST(<<vCoord.x, vCoord.y, 0.0>>, <<vNodePos.x, vNodePos.y, 0.0>>) // 2d dist
//		IF (iLanes > 1)
//			fDist += TO_FLOAT(iLanes-1) * -5.0
//		ENDIF
		fZDist = ABSF(vCoord.z - vNodePos.z)
	
		#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				NET_PRINT("[spawning] GetDistToNearestCarNode - vCoord = ") NET_PRINT_VECTOR(vCoord) 
				NET_PRINT(" vNodePos = ") NET_PRINT_VECTOR(vNodePos) 
				NET_PRINT(" fDist = ") NET_PRINT_FLOAT(fDist) 
				NET_PRINT(" fZDist = ") NET_PRINT_FLOAT(fZDist)
				NET_NL()	
			ENDIF
		#ENDIF
		RETURN(TRUE)		
	ENDIF	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsACarNodeInRange(VECTOR vCoord, BOOL &bZIsInRange, FLOAT &fDistToRoadNode)

	VECTOR vNodePos
	VEHICLE_NODE_ID NodeID
	FLOAT fDist
	INT iLanes
	FLOAT fZDist
	FLOAT fHeading
	
	NodeID = GET_NTH_CLOSEST_VEHICLE_NODE_ID_WITH_HEADING(vCoord, 1, fHeading, iLanes, NF_INCLUDE_SWITCHED_OFF_NODES )		
	IF IS_VEHICLE_NODE_ID_VALID(NodeID)
		
		GET_VEHICLE_NODE_POSITION( NodeID, vNodePos )
		fDist = VDIST(vCoord, vNodePos)
		fDistToRoadNode = fDist

		IF ( (fDist > (CAR_NODE_MIN_DISTANCE * iLanes)) OR GET_VEHICLE_NODE_IS_SWITCHED_OFF(NodeID))
		AND (fDist < CAR_NODE_MAX_DISTANCE)
		
			// check z is in range
			fZDist = ABSF(vNodePos.z - vCoord.z)
			IF (fZDist < CAR_NODE_MAX_Z_DISTANCE)
				bZIsInRange = TRUE
				#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
						NET_PRINT("[spawning] IsACarNodeInRange - returning TRUE - vehicle node is in range AND z dist is OK, fDist = ") NET_PRINT_FLOAT(fDist) NET_PRINT(" fZDist = ") NET_PRINT_FLOAT(fZDist)  NET_PRINT(" iLanes = ") NET_PRINT_INT(iLanes) NET_NL()
					ENDIF
				#ENDIF	
				RETURN(TRUE)
				
			ELSE
				bZIsInRange = FALSE
				#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
						NET_PRINT("[spawning] IsACarNodeInRange - returning TRUE - vehicle node is in range but z dist is NOT OK, fDist = ") NET_PRINT_FLOAT(fDist) NET_PRINT(" fZDist = ") NET_PRINT_FLOAT(fZDist)  NET_PRINT(" iLanes = ") NET_PRINT_INT(iLanes) NET_NL()
					ENDIF
				#ENDIF	
				RETURN(TRUE)
			ENDIF
			
		ELSE
			#IF IS_DEBUG_BUILD
				IF (g_SpawnData.bShowAdvancedSpew)
					NET_PRINT("[spawning] IsACarNodeInRange - vehicle node NOT in range, fDist = ") NET_PRINT_FLOAT(fDist) NET_PRINT(" iLanes = ") NET_PRINT_INT(iLanes) NET_NL()
				ENDIF
			#ENDIF	
		ENDIF

			
	ELSE
		#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				NET_PRINT("[spawning] IsACarNodeInRange - vehicle node not valid") NET_NL()
			ENDIF
		#ENDIF
	ENDIF
	
	fDistToRoadNode = -1.0
	RETURN(FALSE)

ENDFUNC

FUNC BOOL IsPointNearToLastSpawnSearchResults(VECTOR vCoord, FLOAT fDist, INT iNumberToCheck)
	INT i
	REPEAT iNumberToCheck i
		IF (VDIST2(g_SpawnData.vSpawnSearchResults[i], vCoord) < (fDist*fDist))
			RETURN(TRUE)
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

PROC AddPointToLastSpawnSearchResults(VECTOR vCoord)
	INT i
	REPEAT MAX_NUM_OF_STORED_SPAWNSEARCH_RESULTS-1 i
		IF (i > 0)
			g_SpawnData.vSpawnSearchResults[MAX_NUM_OF_STORED_SPAWNSEARCH_RESULTS-i] = g_SpawnData.vSpawnSearchResults[MAX_NUM_OF_STORED_SPAWNSEARCH_RESULTS-(i+1)]
		ENDIF
	ENDREPEAT
	g_SpawnData.vSpawnSearchResults[0] = vCoord
ENDPROC


FUNC BOOL IsPointNearToPlayerLastSpawnCoords(VECTOR vCoord, FLOAT fDist, INT iTime)
	INT i
	TIME_DATATYPE TimeNow = GET_NETWORK_TIME()
	IF NOT (GET_PLAYER_TEAM(PLAYER_ID()) = -1)
		REPEAT MAX_NUM_OF_STORED_LAST_SPAWN_RESULTS i
			IF GET_TIME_DIFFERENCE(TimeNow, g_SpawnData.MissionSpawnDetails.LastSpawnTime[i]) < iTime
				IF (VDIST2(g_SpawnData.MissionSpawnDetails.vLastSpawn[i], vCoord) < (fDist*fDist))
					NET_PRINT("[spawning] - IsPointNearToPlayerLastSpawnCoords = TRUE, last spawn = ") NET_PRINT_VECTOR(g_SpawnData.MissionSpawnDetails.vLastSpawn[i]) NET_NL()
					RETURN(TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC AddPointToPlayerLastSpawnCoords(VECTOR vCoord)
	// move all last results up
	INT i
	REPEAT MAX_NUM_OF_STORED_LAST_SPAWN_RESULTS-1 i
		g_SpawnData.MissionSpawnDetails.LastSpawnTime[i] = g_SpawnData.MissionSpawnDetails.LastSpawnTime[i+1]
		g_SpawnData.MissionSpawnDetails.vLastSpawn[i] = g_SpawnData.MissionSpawnDetails.vLastSpawn[i+1]
	ENDREPEAT
	g_SpawnData.MissionSpawnDetails.LastSpawnTime[MAX_NUM_OF_STORED_LAST_SPAWN_RESULTS-1] = GET_NETWORK_TIME()
	g_SpawnData.MissionSpawnDetails.vLastSpawn[MAX_NUM_OF_STORED_LAST_SPAWN_RESULTS-1] = vCoord
	
	NET_PRINT("[spawning] - AddPointToPlayerLastSpawnCoords = adding point = ") NET_PRINT_VECTOR(vCoord) NET_NL()
	
	
ENDPROC

FUNC FLOAT GetSpawnPointFloatScore(VECTOR vCoords, VECTOR vOrigin, BOOL bDoNearToOriginCheck, BOOL bDoNearRoadCheck, BOOL bDoNearDeathCheck, INT iIsPointOkForSpawning_PassMark, FLOAT &fMinDistToHostile)
									
	// do float scoring
	FLOAT fThisScore
	FLOAT fFallbackScore = 1.0
	FLOAT fDist1, fDist2
	
	// ********************************************************************************
	//	 	how many checks were passed in the IsPointOkForSpawning check?
	// ********************************************************************************	
	IF (iIsPointOkForSpawning_PassMark > 0)
		fFallbackScore = TO_FLOAT(iIsPointOkForSpawning_PassMark)/TO_FLOAT(MAX_ISPOINTOKFORSPAWNING_SCORE)
		#IF IS_DEBUG_BUILD
			IF (fFallbackScore > 1.0)
				NET_PRINT("GetSpawnPointFloatScore - check max value of iIsPointOkForSpawning_PassMark, iIsPointOkForSpawning_PassMark = ") NET_PRINT_INT(iIsPointOkForSpawning_PassMark) NET_NL()			
				SCRIPT_ASSERT("GetSpawnPointFloatScore - check max value of iIsPointOkForSpawning_PassMark")
			ENDIF
		
			IF (g_SpawnData.bShowAdvancedSpew)
				NET_PRINT("[spawning] GetSpawnPointFloatScore - fFallbackScore (from pass marks) = ") NET_PRINT_FLOAT(fFallbackScore) NET_NL()	
			ENDIF
		#ENDIF	
	ELSE
		#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				NET_PRINT("[spawning] GetSpawnPointFloatScore - fFallbackScore (pass marks = 0) = ") NET_PRINT_FLOAT(fFallbackScore) NET_NL()	
			ENDIF
		#ENDIF	
	ENDIF
	
	// **********************************
	//	 	dist to origin
	// **********************************
	IF (bDoNearToOriginCheck)
		fThisScore = GetPointScoreFromDistances(VDIST(vCoords, vOrigin), 0.0, 0.0, 200.0, 1.0, 0.1)	
		#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				NET_PRINT("[spawning] GetSpawnPointFloatScore - origin score = ") NET_PRINT_FLOAT(fThisScore) NET_NL()	
			ENDIF
		#ENDIF
		fFallbackScore *= fThisScore * 0.95
	ENDIF
	
	FLOAT fThisDistToHostile
	fMinDistToHostile = 9999999.9
	

	// **********************************
	//	 	dist to enemy players
	// **********************************
	fThisDistToHostile = GetDistanceFromPointToPlayerForSpawnPoint(vCoords, TRUE, FALSE)
	fThisScore = GetPointScoreFromDistances(fThisDistToHostile, 0.0, GetOptimumEnemyDist(), GetMaxEnemyDist(), 0.0, 0.3)
	#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
			NET_PRINT("[spawning] GetSpawnPointFloatScore - enemy score = ") NET_PRINT_FLOAT(fThisScore) NET_NL()	
		ENDIF
	#ENDIF
	fFallbackScore *= fThisScore * 0.95
	IF (fThisDistToHostile < fMinDistToHostile)
		fThisDistToHostile = fThisDistToHostile
	ENDIF
	

	// **********************************
	//	 	dist to hostile peds
	// **********************************
	fThisDistToHostile = GetDistanceFromPointToHostilePedForSpawnPoint(vCoords)
	fThisScore = GetPointScoreFromDistances(fThisDistToHostile, 0.0, 0.0, 60.0, 0.5, 1.0)
	#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
			NET_PRINT("[spawning] GetSpawnPointFloatScore - hostile ped score = ") NET_PRINT_FLOAT(fThisScore) NET_NL()	
		ENDIF
	#ENDIF
	fFallbackScore *= fThisScore * 0.95
	IF (fThisDistToHostile < fMinDistToHostile)
		fThisDistToHostile = fThisDistToHostile
	ENDIF

	// **********************************
	//	 	dist to warping players
	// **********************************
	FLOAT fMaxDistFromWarpers = 100.0
	IF ((GET_PLAYER_TEAM(PLAYER_ID()) = -1) AND NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID()))
		fMaxDistFromWarpers = 1.0	
	ENDIF
	fThisDistToHostile = ClosestCurrentWarpingPlayerToPosition(vCoords, PLAYER_ID())
	fThisScore = GetPointScoreFromDistances(fThisDistToHostile, 0.0, 0.0, fMaxDistFromWarpers, 0.0, 1.0)
	#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
			NET_PRINT("[spawning] GetSpawnPointFloatScore - warping score = ") NET_PRINT_FLOAT(fThisScore) NET_NL()	
		ENDIF
	#ENDIF
	fFallbackScore *= fThisScore * 0.95
	IF (fThisDistToHostile < fMinDistToHostile)
		fThisDistToHostile = fThisDistToHostile
	ENDIF
	
	PRINTLN("[spawning]GetSpawnPointFloatScore - fMinDistToHostile = ", fThisDistToHostile)

	// **********************************
	//	 	dist to road node
	// **********************************
	IF (bDoNearRoadCheck)
		IF GetDistToNearestCarNode(vCoords, fDist1, fDist2)
			// lets not squabble about minor differences
			IF (fDist1 < 10.0)
				fDist1 = 10.0
			ENDIF
			IF (fDist2 < 0.5)
				fDist2 = 0.5
			ENDIF
			fThisScore = GetPointScoreFromDistances(fDist1, 0.0, 0.0, 200.0, 1.0, 0.1)
			fThisScore += GetPointScoreFromDistances(fDist2, 0.0, 0.0, 6.0, 1.0, 0.5)
			fThisScore *= 0.5
			#IF IS_DEBUG_BUILD
				IF (g_SpawnData.bShowAdvancedSpew)
					NET_PRINT("[spawning] GetSpawnPointFloatScore - road score = ") NET_PRINT_FLOAT(fThisScore) NET_NL()	
				ENDIF
			#ENDIF
			fFallbackScore *= fThisScore * 0.85
		ENDIF
	ENDIF

	// **********************************
	//	 	dist to last death
	// **********************************
	IF (bDoNearDeathCheck)
		fThisScore = GetPointScoreFromDistances(VDIST(g_SpawnData.vMyDeadCoords, vCoords ), 0.0, 100.0, 200.0, 0.1, 0.1)	
		#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				NET_PRINT("[spawning] GetSpawnPointFloatScore - death score = ") NET_PRINT_FLOAT(fThisScore) NET_NL()	
			ENDIF
		#ENDIF
		fFallbackScore *= fThisScore * 0.9
	ENDIF
	
	RETURN(fFallbackScore)
	
ENDFUNC

FUNC BOOL IsPointWithinSearchParams(VECTOR vPoint)
	SWITCH g_SpawnData.iRespawnSearch_Shape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			RETURN IsPointInsideSphere(vPoint, g_SpawnData.vRespawnSearch_CurrentSearch, g_SpawnData.fRespawnSearch_CurrentRadius, FALSE, FALSE)
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			RETURN IsPointInsideBox(vPoint, g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point1, g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point2, FALSE,FALSE)
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			RETURN IS_POINT_IN_ANGLED_AREA(vPoint, g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point1, g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point2, g_SpawnData.fRespawnSearch_CurrentRadius_AngledArea_Width)
		BREAK
	ENDSWITCH

//	IF NOT (g_SpawnData.bRespawnSearch_IsAngled )
//		// check z diff
//		FLOAT fDiff
//		fDiff = ABSF(vPoint.z - g_SpawnData.vRespawnSearch_CurrentSearch.z)
//		IF (fDiff < g_SpawnData.fRespawnSearch_CurrentRadius)
//			RETURN(TRUE)
//		ENDIF
//	ELSE
//		IF IS_POINT_IN_ANGLED_AREA ( vPoint, g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point1, g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point2, g_SpawnData.fRespawnSearch_CurrentRadius_AngledArea_Width)
//			RETURN(TRUE)
//		ENDIF
//	ENDIF
	NET_PRINT("[spawning] IsPointWithinSearchParams - returning FALSE for point ") NET_PRINT_VECTOR(vPoint) NET_NL()
	RETURN(FALSE)
ENDFUNC

#IF IS_DEBUG_BUILD

PROC PrintSpawnSearchScoreData(STRING str, SPAWN_SEARCH_SCORE_DATA Data)
	PRINTLN("[spawning_sorting] ", str, 
			" = ", Data.vFallbackCoords, 
			", ", Data.fFallbackHeading, 
			", ", Data.iFallbackScore, 
			", ", Data.fScore, 
			", ", Data.fDistScoreFromPlayers, 
			", ", Data.fDistToMissionArea)
ENDPROC

FUNC INT CountUnSortedSearchScoreData()
	INT i
	INT iCount
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i
		IF (g_Spawning_UnSortedTopResults[i].iFallbackScore > 0)
			iCount++
		ENDIF
	ENDREPEAT
	RETURN iCount	
ENDFUNC

PROC PrintUnsortedSearchScoreData()
	INT i
	INT iCount
	TEXT_LABEL_63 str
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i
		IF (g_Spawning_UnSortedTopResults[i].iFallbackScore > 0)
			str = "UnSortedTopResults "
			str += iCount
			PrintSpawnSearchScoreData(str, g_Spawning_UnSortedTopResults[i])
			iCount++
		ENDIF
	ENDREPEAT
ENDPROC

PROC PrintFinalUnsortedSearchScoreData()
	INT i
	TEXT_LABEL_63 str
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i	
		IF (g_Spawning_UnSortedTopResults[i].iFallbackScore > 0)
			str = "Final UnsortedSearchScoreData "
			str += i
			PrintSpawnSearchScoreData(str, g_Spawning_UnSortedTopResults[i])
		ENDIF
	ENDREPEAT
ENDPROC

#ENDIF

FUNC INT GetUnSortedCurrentHighScore()
	INT i
	INT iCurrentHighestScore = 0
	// first find the current highest score
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i	
		// store the current highest score in the array
		IF 	(g_Spawning_UnSortedTopResults[i].iFallbackScore > iCurrentHighestScore)
			iCurrentHighestScore = g_Spawning_UnSortedTopResults[i].iFallbackScore
		ENDIF
	ENDREPEAT
	RETURN iCurrentHighestScore
ENDFUNC

PROC InsertUnSortedSearchScoreData( SPAWN_SEARCH_SCORE_DATA Data )
	INT i
	SPAWN_SEARCH_SCORE_DATA EmptyData
	INT iCurrentHighestScore = GetUnSortedCurrentHighScore()
	
	// has passed in param got higher score?
	IF (Data.iFallbackScore > iCurrentHighestScore)
		iCurrentHighestScore = Data.iFallbackScore
		//PRINTLN("InsertUnSortedSearchScoreData - new highest score of ", iCurrentHighestScore)
	ELSE
		//PRINTLN("InsertUnSortedSearchScoreData - existing highest score of ", iCurrentHighestScore)
	ENDIF
	
	// remove all entries lower than current highest score
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i	
		// store the current highest score in the array
		IF 	(g_Spawning_UnSortedTopResults[i].iFallbackScore < iCurrentHighestScore)
			g_Spawning_UnSortedTopResults[i] = EmptyData
		ENDIF
	ENDREPEAT
	
	// should we exit now? if the input is not high enough?
	IF (Data.iFallbackScore < iCurrentHighestScore)
		EXIT
	ENDIF
	
	// add new entry to array
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i		
		IF (g_Spawning_UnSortedTopResults[i].iFallbackScore = 0)
		
			g_Spawning_UnSortedTopResults[i] = Data	
			EXIT
	
		ENDIF		
	ENDREPEAT
	
	// if we've not managed to find a free slot then replace the one with the lowest score
	FLOAT fLowestScore = 9999.9
	INT iLowestSlot = -1
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i
		IF (g_Spawning_UnSortedTopResults[i].iFallbackScore > 0)		
			IF (g_Spawning_UnSortedTopResults[i].fScore < fLowestScore)
				fLowestScore = g_Spawning_UnSortedTopResults[i].fScore
				iLowestSlot = i
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF (iLowestSlot > -1)
		#IF IS_DEBUG_BUILD
			PrintSpawnSearchScoreData("InsertUnSortedSearchScoreData - removing ", g_Spawning_UnSortedTopResults[iLowestSlot])
		#ENDIF
		g_Spawning_UnSortedTopResults[iLowestSlot] = Data
	ELSE
		PRINTLN("[spawning_sorting] InsertUnSortedSearchScoreData - could not find free slot")
	ENDIF
	
ENDPROC

PROC ClearUnSortedSearchScoreData()
	PRINTLN("[spawning_sorting] ClearUnSortedSearchScoreData - called")
	SPAWN_SEARCH_SCORE_DATA EmptyData
	INT i
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i
		g_Spawning_UnSortedTopResults[i] = EmptyData	
	ENDREPEAT
ENDPROC

PROC GetSearchScoreDataWithHighestScore(SPAWN_SEARCH_SCORE_DATA &ReturnData)
	INT i
	FLOAT fHighestScore = -1.0
	SPAWN_SEARCH_SCORE_DATA Data
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i	
		IF g_Spawning_UnSortedTopResults[i].iFallbackScore > 0
			IF (g_Spawning_UnSortedTopResults[i].fScore > fHighestScore)
				fHighestScore = g_Spawning_UnSortedTopResults[i].fScore
				Data = g_Spawning_UnSortedTopResults[i]
			ENDIF
		ENDIF
	ENDREPEAT
	ReturnData = Data 
ENDPROC

PROC GetSearchScoreDataWithLowestDistToMissionArea(SPAWN_SEARCH_SCORE_DATA &ReturnData, FLOAT fButMoreThan)
	INT i
	FLOAT fLowestScore = 999999.9
	SPAWN_SEARCH_SCORE_DATA Data
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i	
		IF g_Spawning_UnSortedTopResults[i].iFallbackScore > 0
			IF (g_Spawning_UnSortedTopResults[i].fDistToMissionArea < fLowestScore)
			AND (g_Spawning_UnSortedTopResults[i].fDistToMissionArea > fButMoreThan)
				fLowestScore = g_Spawning_UnSortedTopResults[i].fDistToMissionArea
				Data = g_Spawning_UnSortedTopResults[i]
			ENDIF
		ENDIF
	ENDREPEAT
	ReturnData = Data 
ENDPROC

PROC GetSearchScoreDataFurthestFromPoint(SPAWN_SEARCH_SCORE_DATA &ReturnData, VECTOR vPoint)
	INT i
	FLOAT fHighestDist2 = -1.0
	FLOAT fThisDist2
	SPAWN_SEARCH_SCORE_DATA Data
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	#ENDIF
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i
		IF g_Spawning_UnSortedTopResults[i].iFallbackScore > 0
			fThisDist2 = VDIST(g_Spawning_UnSortedTopResults[i].vFallbackCoords, vPoint)
			
			#IF IS_DEBUG_BUILD
			str = " dist "
			str += i
			str += " = "
			str += FLOOR(fThisDist2)
			str += "."
			str += FLOOR((fThisDist2 - FLOOR(fThisDist2)) * 100.0)
			PrintSpawnSearchScoreData(str, g_Spawning_UnSortedTopResults[i])
			#ENDIF
			
			fThisDist2 *= g_Spawning_UnSortedTopResults[i].fScore // multiply by the score so it's still taken into account
			
			IF (fThisDist2 > fHighestDist2)
				fHighestDist2 = fThisDist2
				Data = g_Spawning_UnSortedTopResults[i]
			ENDIF
		ENDIF
	ENDREPEAT
	ReturnData = Data 
ENDPROC

PROC GetSearchScoreDataFurthestFrom2Points(SPAWN_SEARCH_SCORE_DATA &ReturnData, VECTOR vPoint1, VECTOR vPoint2)
	INT i
	FLOAT fHighestDist2 = -1.0
	FLOAT fThisDist2A, fThisDist2B, fThisDist2Total
	SPAWN_SEARCH_SCORE_DATA Data
	REPEAT NUM_OF_STORED_UNSORTED_SPAWN_RESULTS i
		IF g_Spawning_UnSortedTopResults[i].iFallbackScore > 0
			fThisDist2A = VDIST(g_Spawning_UnSortedTopResults[i].vFallbackCoords, vPoint1)	
			fThisDist2B = VDIST(g_Spawning_UnSortedTopResults[i].vFallbackCoords, vPoint2)			
			fThisDist2Total = fThisDist2A + fThisDist2B
			fThisDist2Total *= g_Spawning_UnSortedTopResults[i].fScore // multiply by the score so it's still taken into account			
			IF (fThisDist2Total > fHighestDist2)
				fHighestDist2 = fThisDist2Total
				Data = g_Spawning_UnSortedTopResults[i]
			ENDIF
		ENDIF
	ENDREPEAT
	ReturnData = Data 
ENDPROC


PROC Get3BestResultsFromUnsorted(SPAWN_SEARCH_SCORE_DATA &ReturnData1, SPAWN_SEARCH_SCORE_DATA &ReturnData2, SPAWN_SEARCH_SCORE_DATA &ReturnData3)

	#IF IS_DEBUG_BUILD
	PrintFinalUnsortedSearchScoreData()
	#ENDIF

	IF IS_SPAWN_LOCATION_MISSION_AREA_CHECK(g_SpawnData.SpawnLocation)
	AND (GetUnSortedCurrentHighScore() < FALLBACK_SCORE_IN_SPAWNING_AREA)
	
		// get 3 results with lowest fDistToMissionArea
		GetSearchScoreDataWithLowestDistToMissionArea(ReturnData1, 0.0)
		GetSearchScoreDataWithLowestDistToMissionArea(ReturnData2, ReturnData1.fDistToMissionArea)
		GetSearchScoreDataWithLowestDistToMissionArea(ReturnData3, ReturnData2.fDistToMissionArea)
		
		#IF IS_DEBUG_BUILD	
			PRINTLN("Get3BestResultsFromUnsorted using fDistToMissionArea")
			PrintSpawnSearchScoreData("Get3BestResultsFromUnsorted 0 ", ReturnData1)	
			PrintSpawnSearchScoreData("Get3BestResultsFromUnsorted 1 ", ReturnData2)
			PrintSpawnSearchScoreData("Get3BestResultsFromUnsorted 2 ", ReturnData3)
		#ENDIF	
		
		
	ELSE

		// best score first
		GetSearchScoreDataWithHighestScore(ReturnData1)
		
		// furthest from 1st point next
		GetSearchScoreDataFurthestFromPoint(ReturnData3, ReturnData1.vFallbackCoords) 
		
		// furthest from previous 2 now
		GetSearchScoreDataFurthestFrom2Points(ReturnData2, ReturnData1.vFallbackCoords, ReturnData3.vFallbackCoords)
		
		#IF IS_DEBUG_BUILD
			PrintSpawnSearchScoreData("Get3BestResultsFromUnsorted 0 ", ReturnData1)	
			PrintSpawnSearchScoreData("Get3BestResultsFromUnsorted 1 ", ReturnData2)
			PrintSpawnSearchScoreData("Get3BestResultsFromUnsorted 2 ", ReturnData3)
		#ENDIF	
		
	ENDIF
		
ENDPROC

PROC InsertSearchScoreData( SPAWN_SEARCH_SCORE_DATA Data, INT iPos)
	
	SPAWN_SEARCH_SCORE_DATA SavedData
	SavedData = g_SpawnData.SearchTempData.TopResults[iPos]
	g_SpawnData.SearchTempData.TopResults[iPos] = Data
		
	NET_PRINT("[spawning] InsertSearchScoreData - called with iPos") NET_PRINT_INT(iPos)  NET_NL()	
	
	IF (iPos < NUM_OF_STORED_SPAWN_RESULTS-1)
		InsertSearchScoreData(SavedData, iPos+1)
	ENDIF

ENDPROC

FUNC BOOL IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_INDEX PlayerID)
	IF IS_PLAYER_ON_ANY_MP_MISSION(PlayerID)
	OR FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PlayerID)	
	OR GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PlayerID)
	OR IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PlayerID)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC	


//PROC AnalyseSpawnPoint(BOOL bIsForLocalPlayerSpawning, VECTOR vInputCoords, FLOAT fInputHeading, BOOL bForVehicle, BOOL bDoNearARoadChecks, BOOL bDoVisibleChecks, BOOL bDoVisibilityChecksOnTeamMates, BOOL bCloseToOriginAsPossible, BOOL bThisIsTheRawPoint, INT iResultsFlag, FLOAT fMinDistToEnemyPlayers )
PROC AnalyseSpawnPoint(VECTOR vInputCoords, FLOAT fInputHeading, PRIVATE_SPAWN_SEARCH_PARAMS &PrivateParams, SPAWN_SEARCH_PARAMS &PublicParams, BOOL bThisIsTheRawPoint, INT iResultsFlag)
	
	FLOAT fFallbackScore
	FLOAT fRadius
	FLOAT fThisDistToMissionArea
	FLOAT fDistScoreFromPlayers
	BOOL bDoNearDeathCheck
	FLOAT fDist
	FLOAT fDistToNearestPlayer
	FLOAT fNearestEnemy
	INT iFallbackScore
	BOOL bOnPavement
	INT iIsPointOkForSpawning_PassMark = 0
	INT i
	
	VECTOR vDeadCoords

	#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
		
			NET_PRINT("[spawning] AnalyseSpawnPoint - called with: ")  NET_NL()
			NET_PRINT(" vInputCoords = ") NET_PRINT_VECTOR(vInputCoords) NET_NL()
			NET_PRINT(" fInputHeading = ") NET_PRINT_FLOAT(fInputHeading) NET_NL()
			NET_PRINT(" bThisIsTheRawPoint = ") NET_PRINT_BOOL(bThisIsTheRawPoint) NET_NL()
			NET_PRINT(" iResultsFlag = ") NET_PRINT_INT(iResultsFlag) NET_NL()
			
			DEBUG_PRINT_SPAWN_SEARCH_PARAMS(PublicParams)
			DEBUG_PRINT_PRIVATE_SPAWN_SEARCH_PARAMS(PrivateParams)

		ENDIF
	#ENDIF	
	
	iFallbackScore = 0
	bOnPavement = FALSE
	
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_DEATH)
			IF ABSF(g_SpawnData.vMyDeadCoords.z	- vInputCoords.z) < 25.0
				iFallbackScore += FALLBACK_Z_NEAR_DEATH 
			ENDIF
		ELSE
			iFallbackScore += FALLBACK_Z_NEAR_DEATH
		ENDIF
	ELSE
		iFallbackScore += FALLBACK_Z_NEAR_DEATH
	ENDIF
		
																									
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
			// if player is on a mission it is less important to spawn on a pavement. 
			IF (iResultsFlag = -1)
				bOnPavement = TRUE
			ELSE
				IF NOT ((iResultsFlag & ENUM_TO_INT(RESPAWN_RESULT_FLAG_PAVEMENT)) = 0)
					bOnPavement = TRUE
				ENDIF
			ENDIF
		ELSE
			IF (iResultsFlag = -1)
				iFallbackScore += FALLBACK_ON_PAVEMENT
			ELSE
				IF NOT ((iResultsFlag & ENUM_TO_INT(RESPAWN_RESULT_FLAG_PAVEMENT)) = 0)
					iFallbackScore += FALLBACK_ON_PAVEMENT
				ENDIF
			ENDIF		
		ENDIF
	ELSE
		iFallbackScore += FALLBACK_ON_PAVEMENT
	ENDIF
	
	// check if point is visible to teammates (if required)
	IF (PrivateParams.bIsForLocalPlayerSpawning)
	AND (PrivateParams.bDoTeamMateVisCheck)
		IF NOT CAN_ANY_TEAMMATE_SEE_POINT(vInputCoords)
			iFallbackScore += FALLBACK_NOT_VISIBLE_TO_TEAMMATES
		ENDIF
	ELSE
		iFallbackScore += FALLBACK_NOT_VISIBLE_TO_TEAMMATES
	ENDIF
																					
	// check if any peds are too close
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		IF NOT IS_ANY_HOSTILE_PED_NEAR_POINT( PLAYER_PED_ID(), vInputCoords, SPAWN_MIN_DIST_FROM_PED_FAR )
			iFallbackScore += FALLBACK_FAR_ENOUGH_FROM_PEDS_NEAR	
			iFallbackScore += FALLBACK_FAR_ENOUGH_FROM_PEDS_FAR	
		ELIF NOT IS_ANY_HOSTILE_PED_NEAR_POINT( PLAYER_PED_ID(), vInputCoords, SPAWN_MIN_DIST_FROM_PED_NEAR )
			iFallbackScore += FALLBACK_FAR_ENOUGH_FROM_PEDS_NEAR				
		ENDIF
	ELSE
		iFallbackScore += FALLBACK_FAR_ENOUGH_FROM_PEDS_NEAR	
		iFallbackScore += FALLBACK_FAR_ENOUGH_FROM_PEDS_FAR
	ENDIF
	
	// check no enemy players are near
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		IF NOT IS_ANY_PLAYER_NEAR_POINT(vInputCoords, PublicParams.fMinDistFromPlayer, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
			iFallbackScore += FALLBACK_FAR_ENOUGH_FROM_ENEMY_PLAYERS	
		ENDIF
	ELSE
		iFallbackScore += FALLBACK_FAR_ENOUGH_FROM_ENEMY_PLAYERS
	ENDIF
	
	// check if it has avoided the 'avoid' coords
	BOOL bAvoidedAllAvoidRadius = TRUE
	REPEAT MAX_NUM_AVOID_RADIUS i
		IF VMAG(PublicParams.vAvoidCoords[i]) > 0.0
			IF NOT (VDIST(vInputCoords, PublicParams.vAvoidCoords[i]) > PublicParams.fAvoidRadius[i])
				#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
						NET_PRINT("[spawning] AnalyseSpawnPoint - in avoid coords ") NET_PRINT_INT(i) NET_NL()	
					ENDIF
				#ENDIF		
				bAvoidedAllAvoidRadius = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	IF (bAvoidedAllAvoidRadius)
		IF (VMAG(PublicParams.vAvoidAngledAreaPos1) > 0.0)
		AND (VMAG(PublicParams.vAvoidAngledAreaPos2) > 0.0)
		AND (PublicParams.fAvoidAngledAreaWidth > 0.0)
			IF IS_POINT_IN_ANGLED_AREA(vInputCoords, PublicParams.vAvoidAngledAreaPos1, PublicParams.vAvoidAngledAreaPos2, PublicParams.fAvoidAngledAreaWidth)
				#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
						NET_PRINT("[spawning] AnalyseSpawnPoint - in avoid angled area coords ") NET_PRINT_INT(i) NET_NL()	
					ENDIF
				#ENDIF		
				bAvoidedAllAvoidRadius = FALSE	
			ENDIF
		ENDIF
	ENDIF
	
//	// check if any fmmc props are at coord
//	IF (bAvoidedAllAvoidRadius)
//		IF IsAnyFMMCPropAtCoord(vInputCoords)
//			#IF IS_DEBUG_BUILD
//				IF (g_SpawnData.bShowAdvancedSpew)
//					NET_PRINT("[spawning] AnalyseSpawnPoint - fmmc prop at coord ") NET_PRINT_INT(i) NET_NL()	
//				ENDIF
//			#ENDIF	
//			bAvoidedAllAvoidRadius = FALSE	
//		ENDIF
//	ENDIF
	
	IF (bAvoidedAllAvoidRadius)
		iFallbackScore += FALLBACK_SCORE_PASS_AVOID_COORDS
	ENDIF
		
		
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		IF IsPointOkForSpawning(vInputCoords, fInputHeading, PrivateParams.bForAVehicle, IsSpawnLocationAtGanghouse(), PrivateParams.bDoVisibleChecks, FALSE)
			iFallbackScore += FALLBACK_SCORE_OK_FOR_SPAWNING_NEAR
			iFallbackScore += FALLBACK_SCORE_OK_FOR_SPAWNING_FAR
		ELSE
			iIsPointOkForSpawning_PassMark = g_SpawnData.iIsPointOkForSpawningPassMarks	
		ENDIF
		#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				NET_PRINT("[spawning] AnalyseSpawnPoint - g_SpawnData.iIsPointOkForSpawningPassMarks = ") NET_PRINT_INT(g_SpawnData.iIsPointOkForSpawningPassMarks) NET_NL()
				NET_PRINT("[spawning] AnalyseSpawnPoint - g_SpawnData.iIsPointOKForNetEntityCreationPassMarks = ") NET_PRINT_INT(g_SpawnData.iIsPointOKForNetEntityCreationPassMarks) NET_NL()
			ENDIF
		#ENDIF
	ELSE						
		IF NOT IsAnotherPlayerOrPlayerVehicleSpawningAtPosition(vInputCoords, 25.0, PLAYER_ID(), TRUE, TRUE)		
			IF (PrivateParams.bForAVehicle)
				fRadius = 3.5
			ELSE
				fRadius = 1.0
			ENDIF						
			
			IF NOT CAN_ANY_PLAYER_SEE_POINT( vInputCoords, fRadius, TRUE, TRUE, 120.0, FALSE, -1, 0.0 , TRUE)
				iFallbackScore += FALLBACK_SCORE_OK_FOR_SPAWNING_FAR
				iFallbackScore += FALLBACK_SCORE_OK_FOR_SPAWNING_NEAR
			ELIF NOT CAN_ANY_PLAYER_SEE_POINT( vInputCoords, fRadius, TRUE, TRUE, 60.0, FALSE, -1, 0.0 , TRUE)
				iFallbackScore += FALLBACK_SCORE_OK_FOR_SPAWNING_NEAR
			ENDIF			
			
			
		ENDIF
	ENDIF
	
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		IF NOT (g_SpawnData.bIgnoreExclusionCheck)
		
			// if using SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION then consider death coords as inside the mission area
			vDeadCoords = g_SpawnData.vMyDeadCoords
			IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION)
				vDeadCoords = g_SpawnData.PrivateParams.vOffsetOrigin	
			ENDIF		
		
			IF NOT IS_POINT_IN_SERVER_REFUSAL_LIST(vInputCoords, 0.5)
				IF DO_GLOBAL_EXCLUSION_ZONES_APPLY(vDeadCoords) 
					IF NOT IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vInputCoords)
					AND NOT IS_POINT_IN_NEAR_EXCLUSION_RADIUS(vInputCoords)
						iFallbackScore += FALLBACK_SCORE_PASS_EXCLUSION_CHECKS				
					ENDIF
				ELSE
					IF NOT IS_POINT_IN_NEAR_EXCLUSION_RADIUS(vInputCoords)
						iFallbackScore += FALLBACK_SCORE_PASS_EXCLUSION_CHECKS
					ENDIF
				ENDIF	
			ENDIF
			
		ELSE
			iFallbackScore += FALLBACK_SCORE_PASS_EXCLUSION_CHECKS
		ENDIF
	ELSE
		// check its not near the last 5 results
		IF NOT IsPointNearToLastSpawnSearchResults(vInputCoords, 2.5, 3)
			iFallbackScore += FALLBACK_SCORE_PASS_EXCLUSION_CHECKS
		ELSE
			#IF IS_DEBUG_BUILD
				IF (g_SpawnData.bShowAdvancedSpew)
					NET_PRINT("[spawning] AnalyseSpawnPoint - IsPointNearToLastSpawnSearchResults(2.5) = TRUE ") NET_NL()	
				ENDIF
			#ENDIF
		ENDIF
	ENDIF	
	
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		IF NOT (IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID()) AND IS_PLAYER_ON_MISSION(PLAYER_ID()))
			IF NOT IS_POINT_IN_NEAREST_GANG_ATTACK_BLIP_RADIUS(vInputCoords, g_SpawnData.SearchTempData.NearestGangAttack)	
				iFallbackScore += FALLBACK_SCORE_NOT_IN_GANG_ZONE
			ENDIF
		ELSE
			iFallbackScore += FALLBACK_SCORE_NOT_IN_GANG_ZONE	
		ENDIF
	ELSE
		iFallbackScore += FALLBACK_SCORE_NOT_IN_GANG_ZONE
	ENDIF
	
	
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		IF NOT IS_PLAYER_ON_MISSION(PLAYER_ID())
			IF NOT Is_Vector_In_Any_Corona_Triggering_Range_Using_Nearest_Arrays(vInputCoords, g_SpawnData.SearchTempData.NearestCoronas, g_SpawnData.SearchTempData.NearestGangAttack)
				iFallbackScore += FALLBACK_SCORE_AVOIDS_MISSION_CORONAS	
			ENDIF
		ELSE
			iFallbackScore += FALLBACK_SCORE_AVOIDS_MISSION_CORONAS
		ENDIF
	ELSE
		iFallbackScore += FALLBACK_SCORE_AVOIDS_MISSION_CORONAS
	ENDIF		
	
	IF IsPointWithinSearchParams(vInputCoords)
		IF (PrivateParams.bIsForLocalPlayerSpawning)
			IF IS_SPAWN_LOCATION_MISSION_AREA_CHECK(g_SpawnData.SpawnLocation) 
				IF IS_POINT_IN_MISSION_SPAWN_AREA(vInputCoords, 0.01)
					iFallbackScore += FALLBACK_SCORE_IN_SPAWNING_AREA	
				ENDIF
			ELSE
				iFallbackScore += FALLBACK_SCORE_IN_SPAWNING_AREA	
			ENDIF				
		ELSE
			iFallbackScore += FALLBACK_SCORE_IN_SPAWNING_AREA
		ENDIF
	ENDIF

	
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		IF IsPointInSameConsiderInteriorState(vInputCoords)
			iFallbackScore += FALLBACK_SCORE_IN_SAME_INTERIOR_GROUP
		ENDIF
	ELSE
		iFallbackScore += FALLBACK_SCORE_IN_SAME_INTERIOR_GROUP
	ENDIF
	
	// if the point is supposed to be 
	IF NOT (g_SpawnData.MissionSpawnDetails.bConsiderInteriors)
		
//		IF NOT IS_COLLISION_MARKED_OUTSIDE(vInputCoords)
//			#IF IS_DEBUG_BUILD
//				IF (g_SpawnData.bShowAdvancedSpew)
//				NET_PRINT("[spawning] AnalyseSpawnPoint - FALLBACK_SCORE_IN_CORRECT_INTERIOR_STATE - point is in an interior when it shouldn't be ") NET_PRINT_VECTOR(vInputCoords) NET_NL()			
//				ENDIF
//			#ENDIF
//		ELSE
			iFallbackScore += FALLBACK_SCORE_IN_CORRECT_INTERIOR_STATE	
//		ENDIF
	
	ELSE
		// if this is supposed to be an interior navmesh poly (returned from james stuff) then check it is actually in an interior. 
		IF (PrivateParams.bIsForLocalPlayerSpawning)
			IF NOT ((iResultsFlag & ENUM_TO_INT(RESPAWN_RESULT_FLAG_PAVEMENT)) = 0)
				IF IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vInputCoords))	
					iFallbackScore += FALLBACK_SCORE_IN_CORRECT_INTERIOR_STATE	
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
					NET_PRINT("[spawning] AnalyseSpawnPoint - FALLBACK_SCORE_IN_CORRECT_INTERIOR_STATE - point is in an exterior when it shouldn't be ") NET_PRINT_VECTOR(vInputCoords) NET_NL()			
					ENDIF
				#ENDIF			
			ENDIF
		ELSE
			IF NOT ((iResultsFlag & ENUM_TO_INT(SPAWNPOINTS_RESULT_FLAG_INTERIOR)) = 0)
				IF IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vInputCoords))	
					iFallbackScore += FALLBACK_SCORE_IN_CORRECT_INTERIOR_STATE
				ELSE
					#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
						NET_PRINT("[spawning] AnalyseSpawnPoint - FALLBACK_SCORE_IN_CORRECT_INTERIOR_STATE - point is in an exterior when it shouldn't be ") NET_PRINT_VECTOR(vInputCoords) NET_NL()			
						ENDIF
					#ENDIF					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// point is not in a mission blocking zone
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		IF NOT (g_SpawnData.bIgnoreExclusionCheck)
			IF NOT IS_POINT_IN_MISSION_EXCLUSION_ZONE(vInputCoords)
				iFallbackScore += FALLBACK_SCORE_NOT_IN_MISSION_BLOCKING_AREA
			ELSE
				#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
					NET_PRINT("[spawning] AnalyseSpawnPoint - FALLBACK_SCORE_NOT_IN_MISSION_BLOCKING_AREA - point is inside mission exclusion zone ") NET_PRINT_VECTOR(vInputCoords) NET_NL()			
					ENDIF
				#ENDIF				
			ENDIF
		ELSE
			iFallbackScore += FALLBACK_SCORE_NOT_IN_MISSION_BLOCKING_AREA
		ENDIF
	ELSE
		iFallbackScore += FALLBACK_SCORE_NOT_IN_MISSION_BLOCKING_AREA
	ENDIF	
	
	// point is not in a forbidden area
	IF NOT IS_POINT_IN_PROBLEM_AREA_FORBIDDEN(vInputCoords)
		iFallbackScore += FALLBACK_SCORE_NOT_IN_FORBIDDEN_AREA
	ELSE
	#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
			NET_PRINT("[spawning] AnalyseSpawnPoint - IS_POINT_IN_PROBLEM_AREA_FORBIDDEN - point is in forbidden area ") NET_PRINT_VECTOR(vInputCoords) NET_NL()			
		ENDIF	
	#ENDIF
		
		// if the point is in a mission spawn area and that entire mission area is inside a forbidden area, then allow as fallback.
		INT iMissionSpawnArea
		iMissionSpawnArea = GET_MISSION_SPAWN_AREA_POINT_IS_IN(vInputCoords)
		
		NET_PRINT("[spawning] AnalyseSpawnPoint - point is in mission area  ") NET_PRINT_INT(iMissionSpawnArea) NET_NL()
		
		IF (iMissionSpawnArea > -1)
			VECTOR vCoords1, vCoords2
			FLOAT fWidth
			GET_FORBIDDEN_AREA_POINT_IS_IN(vInputCoords, vCoords1, vCoords2, fWidth)
			IF NOT IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA(g_SpawnData.MissionSpawnDetails.SpawnArea[iMissionSpawnArea], vCoords1, vCoords2, fWidth)
				iFallbackScore = -1	
			ELSE
				NET_PRINT("[spawning] AnalyseSpawnPoint - point is in mission area that is entirely inside a forbidden area. ") NET_PRINT_VECTOR(vInputCoords) NET_NL()
			ENDIF
		ELSE
			iFallbackScore = -1	
		ENDIF
	
	ENDIF
	
	// point is colliding with an fmmc prop
	IF IsAnyFMMCPropAtCoord(vInputCoords)
		#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				NET_PRINT("[spawning] AnalyseSpawnPoint - IS_POINT_IN_PROBLEM_AREA_FORBIDDEN - point collides with fmmc prop ") NET_PRINT_VECTOR(vInputCoords) NET_NL()			
			ENDIF	
		#ENDIF
		iFallbackScore = -1
	ENDIF
	
	// point is colliding with another ped
	IF (PublicParams.bAvoidOtherPeds)
		PRINTLN("[spawning] AnalyseSpawnPoint - bAvoidOtherPeds = TRUE")
		IF IS_ANY_PED_NEAR_POINT(<<vInputCoords.x, vInputCoords.y, vInputCoords.z+1.0>>, 0.3)
			PRINTLN("[spawning] AnalyseSpawnPoint - there is a ped near point")
			iFallbackScore = -1
		ENDIF
	ENDIF
	
	// point is colliding with another ped
	IF (PublicParams.bAvoidOtherVehicles)
		PRINTLN("[spawning] AnalyseSpawnPoint - bAvoidOtherVehicles = TRUE")
		IF IS_ANY_VEHICLE_NEAR_POINT(<<vInputCoords.x, vInputCoords.y, vInputCoords.z+1.0>>, 1.0)
			PRINTLN("[spawning] AnalyseSpawnPoint - there is a vehicle near point")
			iFallbackScore = -1
		ENDIF
	ENDIF	
	
	
	#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
			NET_PRINT("[spawning] AnalyseSpawnPoint - iFallbackScore = ") NET_PRINT_INT(iFallbackScore) NET_NL()			
		ENDIF	
	#ENDIF	
			
	
	// CHECKS FOR IF WE DEFINATELY SHOULDN'T USE THIS POINT
	
	// if any entity is blocking this position then don't use this point (stop ped from being created inside objects)
	IF (PrivateParams.bIsForLocalPlayerSpawning)
	
	ELSE
		IF IsAnyEntityObstructingPoint(vInputCoords, TRUE, FALSE, PrivateParams.bForAVehicle)
			iFallbackScore = 0
			
			#IF IS_DEBUG_BUILD
				IF (g_SpawnData.bShowAdvancedSpew)
					NET_PRINT("[spawning] AnalyseSpawnPoint - entity is obscuring point, resetting to 0") NET_NL()			
				ENDIF	
			#ENDIF			
		ENDIF
	ENDIF
		
		
	INT iTop
	SPAWN_SEARCH_SCORE_DATA SearchScoreData
	
	BOOL bHasGotFloatScores = FALSE
	BOOL bHasGotClosestActiveSpawnScore = FALSE
	
	IF (g_SpawnData.bUseUnsortedSearchResultsSystem)
	AND (PrivateParams.bIsForLocalPlayerSpawning) // dont bother for ai
	
		IF (iFallbackScore > 0)

		
			// only do this check once as it's expensive
			IF (bThisIsTheRawPoint)
				PublicParams.bPreferPointsCloserToRoads = FALSE	
				bDoNearDeathCheck = FALSE
			ELSE
				bDoNearDeathCheck = TRUE
			ENDIF
			
			IF (PrivateParams.bUseOffsetOrigin)
				fFallbackScore = GetSpawnPointFloatScore(vInputCoords, PrivateParams.vOffsetOrigin, PublicParams.bCloseToOriginAsPossible, PublicParams.bPreferPointsCloserToRoads, bDoNearDeathCheck, iIsPointOkForSpawning_PassMark, SearchScoreData.fMinDistToHostile) 
			ELSE
				fFallbackScore = GetSpawnPointFloatScore(vInputCoords, g_SpawnData.vRespawnSearch_CurrentSearch, PublicParams.bCloseToOriginAsPossible, PublicParams.bPreferPointsCloserToRoads, bDoNearDeathCheck, iIsPointOkForSpawning_PassMark, SearchScoreData.fMinDistToHostile) 	
			ENDIF
			
			// if on the pavement, increase the fallback score. 
			IF (bOnPavement)
				fFallbackScore *= 3.0	
			ENDIF
			

			#IF IS_DEBUG_BUILD
				IF (g_SpawnData.bShowAdvancedSpew)
				NET_PRINT("[spawning] AnalyseSpawnPoint - RESPAWN_QUERY_RESULTS_SUCCEEDED - iTop=") NET_PRINT_INT(iTop) NET_PRINT(", ") NET_PRINT_VECTOR(vInputCoords) NET_PRINT(", iFallbackScore = ") NET_PRINT_INT(iFallbackScore) NET_PRINT(", fFallbackScore = ") NET_PRINT_FLOAT(fFallbackScore) NET_NL()	
				ENDIF
			#ENDIF
		
			// should work out dist to mission area?
			IF IS_SPAWN_LOCATION_MISSION_AREA_CHECK(g_SpawnData.SpawnLocation) 
			AND (iFallbackScore < FALLBACK_SCORE_IN_SPAWNING_AREA) // its not inside the spawn area				
				SearchScoreData.fDistToMissionArea = GetDistanceToClosestActiveSpawnArea(vInputCoords)			
			ENDIF
			
			// we need to send the nearest enemy dist to the server now. 
			fNearestEnemy = GetDistanceFromPointToPlayerForSpawnPoint(vInputCoords, TRUE, FALSE, DEFAULT, FALSE)

			// insert
			SearchScoreData.vFallbackCoords = vInputCoords
			SearchScoreData.fFallbackHeading = fInputHeading
			SearchScoreData.iFallbackScore = iFallbackScore
			SearchScoreData.fScore = fFallbackScore
			SearchScoreData.fNearestEnemy = fNearestEnemy
			
			InsertUnSortedSearchScoreData(SearchScoreData)

			
			g_SpawnData.SearchTempData.Data.bGotFallback = TRUE
					


		ENDIF
	
	ELSE
		
		REPEAT NUM_OF_STORED_SPAWN_RESULTS iTop
			IF (iFallbackScore >= g_SpawnData.SearchTempData.TopResults[iTop].iFallbackScore)
			
			
			
				IF (PrivateParams.bIsForLocalPlayerSpawning)
			
					// only do this check once as it's expensive
					IF NOT (bHasGotFloatScores)
						IF (bThisIsTheRawPoint)
							PublicParams.bPreferPointsCloserToRoads = FALSE	
							bDoNearDeathCheck = FALSE
						ELSE
							bDoNearDeathCheck = TRUE
						ENDIF
						
						IF (PrivateParams.bUseOffsetOrigin)
							fFallbackScore = GetSpawnPointFloatScore(vInputCoords, PrivateParams.vOffsetOrigin, PublicParams.bCloseToOriginAsPossible, PublicParams.bPreferPointsCloserToRoads, bDoNearDeathCheck, iIsPointOkForSpawning_PassMark, SearchScoreData.fMinDistToHostile) 
						ELSE
							fFallbackScore = GetSpawnPointFloatScore(vInputCoords, g_SpawnData.vRespawnSearch_CurrentSearch, PublicParams.bCloseToOriginAsPossible, PublicParams.bPreferPointsCloserToRoads, bDoNearDeathCheck, iIsPointOkForSpawning_PassMark, SearchScoreData.fMinDistToHostile) 	
						ENDIF
						
						// if on the pavement, increase the fallback score. 
						IF (bOnPavement)
							fFallbackScore *= 3.0	
						ENDIF
						
						bHasGotFloatScores = TRUE														
					ENDIF
			
						
					#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
						NET_PRINT("[spawning] AnalyseSpawnPoint - RESPAWN_QUERY_RESULTS_SUCCEEDED - iTop=") NET_PRINT_INT(iTop) NET_PRINT(", ") NET_PRINT_VECTOR(vInputCoords) NET_PRINT(", iFallbackScore = ") NET_PRINT_INT(iFallbackScore) NET_PRINT(", fFallbackScore = ") NET_PRINT_FLOAT(fFallbackScore) NET_NL()	
						ENDIF
					#ENDIF
			
					IF IS_SPAWN_LOCATION_MISSION_AREA_CHECK(g_SpawnData.SpawnLocation) 
					AND (iFallbackScore = g_SpawnData.SearchTempData.TopResults[iTop].iFallbackScore)
					AND (iFallbackScore < FALLBACK_SCORE_IN_SPAWNING_AREA) // its not inside the spawn area
						
						// check if this point is nearear the spawn area than the current fallback one
						IF NOT (bHasGotClosestActiveSpawnScore)
							fThisDistToMissionArea = GetDistanceToClosestActiveSpawnArea(vInputCoords)
							bHasGotClosestActiveSpawnScore = TRUE
						ENDIF
						
						IF (fThisDistToMissionArea < g_SpawnData.SearchTempData.TopResults[iTop].fDistToMissionArea)
						
							// insert
							SearchScoreData.vFallbackCoords = vInputCoords
							SearchScoreData.fFallbackHeading = fInputHeading 
							SearchScoreData.iFallbackScore = iFallbackScore
							SearchScoreData.fScore = fFallbackScore
							SearchScoreData.fDistToMissionArea = fThisDistToMissionArea

							InsertSearchScoreData(SearchScoreData, iTop)
							
							g_SpawnData.SearchTempData.Data.bGotFallback = TRUE
							
							
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] AnalyseSpawnPoint - RESPAWN_QUERY_RESULTS_SUCCEEDED - storing higher fallback coords ") 
							NET_PRINT(", ") NET_PRINT_VECTOR(vInputCoords) 
							NET_PRINT(", iFallbackScore = ") NET_PRINT_INT(iFallbackScore)
							NET_PRINT(", fFallbackScore = ") NET_PRINT_FLOAT(fFallbackScore)
							NET_NL()							
							#ENDIF
							
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] AnalyseSpawnPoint - RESPAWN_QUERY_RESULTS_SUCCEEDED - point is closest to mission area ") NET_PRINT(", ") NET_PRINT_VECTOR(vInputCoords) NET_PRINT(" dist = ") NET_PRINT_FLOAT(SearchScoreData.fDistToMissionArea)  NET_PRINT(", iFallbackScore = ") NET_PRINT_INT(SearchScoreData.iFallbackScore) NET_NL()	
							#ENDIF
							
							EXIT
						ENDIF

					ELSE
						
					
						IF (iFallbackScore > g_SpawnData.SearchTempData.TopResults[iTop].iFallbackScore)
						OR ((iFallbackScore = g_SpawnData.SearchTempData.TopResults[iTop].iFallbackScore) AND (fFallbackScore > g_SpawnData.SearchTempData.TopResults[iTop].fScore))
						
							// insert
							SearchScoreData.vFallbackCoords = vInputCoords
							SearchScoreData.fFallbackHeading = fInputHeading
							SearchScoreData.iFallbackScore = iFallbackScore
							SearchScoreData.fScore = fFallbackScore
							
							InsertSearchScoreData(SearchScoreData, iTop)

							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] AnalyseSpawnPoint - RESPAWN_QUERY_RESULTS_SUCCEEDED - storing higher fallback coords ") 
							 NET_PRINT(", ") NET_PRINT_VECTOR(vInputCoords) 
							NET_PRINT(", iFallbackScore = ") NET_PRINT_INT(iFallbackScore)
							NET_PRINT(", fFallbackScore = ") NET_PRINT_FLOAT(fFallbackScore)
							NET_NL()							
							#ENDIF
							
							g_SpawnData.SearchTempData.Data.bGotFallback = TRUE
							
							EXIT
						ENDIF
					ENDIF				
				ELSE
				
					// for AI spawning	
					
					// only do this check once as it's expensive
					IF NOT (bHasGotFloatScores)	
						IF (PrivateParams.bForAVehicle)
							fRadius = 3.5
						ELSE
							fRadius = 1.0
						ENDIF
						fDist = WHAT_DISTANCE_CAN_ANY_PLAYER_SEE_POINT(vInputCoords, fRadius, TRUE, TRUE, DEFAULT, DEFAULT, TRUE)
						fDistToNearestPlayer = GetDistanceFromPointToPlayerForSpawnPoint(vInputCoords, TRUE, TRUE, TRUE)
						PRINTLN("[spawning] AnalyseSpawnPoint - fDist = ", fDist)
						PRINTLN("[spawning] AnalyseSpawnPoint - fDistToNearestPlayer = ", fDistToNearestPlayer)
						
						IF (fDist > 15.0)
						AND (fDistToNearestPlayer > 5.0)
							fDistScoreFromPlayers = GetPointScoreFromDistances(fDist, 0.0, CUSTOM_SPAWN_OPT_DIST_FROM_ENEMY, CUSTOM_SPAWN_MAX_DIST_FROM_ENEMY, 1.0, 1.2)
						ELSE
							fDistScoreFromPlayers = GetPointScoreFromDistances(fDistToNearestPlayer, 0.0, CUSTOM_SPAWN_OPT_DIST_FROM_ENEMY, CUSTOM_SPAWN_MAX_DIST_FROM_ENEMY, 0.0, 0.2)
						ENDIF
						bHasGotFloatScores = TRUE				
					ENDIF
					
					
					#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
							NET_PRINT("[spawning] AnalyseSpawnPoint - RESPAWN_QUERY_RESULTS_SUCCEEDED - fDist = ") NET_PRINT_FLOAT(fDist) NET_PRINT(", fDistScoreFromPlayers = ") NET_PRINT_FLOAT(fDistScoreFromPlayers)  NET_NL()	
						ENDIF
					#ENDIF
					
					IF (iFallbackScore > g_SpawnData.SearchTempData.TopResults[iTop].iFallbackScore)
					OR ((iFallbackScore = g_SpawnData.SearchTempData.TopResults[iTop].iFallbackScore) AND (fDistScoreFromPlayers > g_SpawnData.SearchTempData.TopResults[iTop].fDistScoreFromPlayers))
						
						// insert
						SearchScoreData.vFallbackCoords = vInputCoords
						SearchScoreData.fFallbackHeading = fInputHeading
						SearchScoreData.iFallbackScore = iFallbackScore
						SearchScoreData.fDistScoreFromPlayers = fDistScoreFromPlayers
						
						InsertSearchScoreData(SearchScoreData, iTop)
						
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] AnalyseSpawnPoint - RESPAWN_QUERY_RESULTS_SUCCEEDED - storing higher fallback coords ") 
						 NET_PRINT(", ") NET_PRINT_VECTOR(vInputCoords) 
						NET_PRINT(", iFallbackScore = ") NET_PRINT_INT(iFallbackScore)
						NET_PRINT(", fDist = ") NET_PRINT_FLOAT(fDist)
						NET_PRINT(", fDistScoreFromPlayers = ") NET_PRINT_FLOAT(fDistScoreFromPlayers)
						NET_PRINT(", bOnPavement = ") NET_PRINT_BOOL(bOnPavement)
						NET_NL()							
						#ENDIF
						
						g_SpawnData.SearchTempData.Data.bGotFallback = TRUE
						
						EXIT
					ENDIF
					
				ENDIF
			ENDIF
		ENDREPEAT	
	ENDIF


ENDPROC

PROC ClearSearchTempDataVariables()
	SPAWN_SEARCH_VARIABLES_DATA EmptyData
	g_SpawnData.SearchTempData.Data = EmptyData
ENDPROC

PROC ClearSearchTempDataTopResults()
	SPAWN_SEARCH_SCORE_DATA EmptyData 
	INT i 
	REPEAT NUM_OF_STORED_SPAWN_RESULTS i 
		g_SpawnData.SearchTempData.TopResults[i] = EmptyData
	ENDREPEAT
ENDPROC

PROC ClearSearchTempDataNearestCoronas()
	SPAWN_SEARCH_CORONA EmptyData 
	INT i 
	REPEAT NUM_OF_STORED_NEAREST_CORONAS i 
		g_SpawnData.SearchTempData.NearestCoronas[i] = EmptyData
	ENDREPEAT
ENDPROC

PROC ClearSearchTempDataNearestGangAttack()
	SPAWN_SEARCH_GANG_ATTACK EmptyData 
	INT i 
	REPEAT NUM_OF_STORED_NEAREST_GANG_ATTACK i 
		g_SpawnData.SearchTempData.NearestGangAttack[i] = EmptyData
	ENDREPEAT
ENDPROC

PROC ClearSearchTempData()	
	PRINTLN("[spawning] ClearSearchTempData() called")
	DEBUG_PRINTCALLSTACK()
	ClearSearchTempDataVariables()
	ClearSearchTempDataTopResults()
	ClearSearchTempDataNearestCoronas()
	ClearSearchTempDataNearestGangAttack()
	ClearUnSortedSearchScoreData()
ENDPROC

PROC ConvertToSpawnResultStruct(SPAWN_RESULTS &SpawnResults, SPAWN_SEARCH_SCORE_DATA &SearchResults[])
	INT i
	REPEAT NUM_OF_STORED_SPAWN_RESULTS i 
		SpawnResults.vCoords[i] = SearchResults[i].vFallbackCoords
		SpawnResults.fHeading[i] = SearchResults[i].fFallbackHeading
		SpawnResults.iScore[i] = SearchResults[i].iFallbackScore
	ENDREPEAT
ENDPROC

#IF IS_DEBUG_BUILD
PROC PrintSpawnResultStruct(SPAWN_RESULTS &SpawnResults)
	INT i
	REPEAT NUM_OF_STORED_SPAWN_RESULTS i 
		IF NOT (i = 0)
			NET_PRINT(", ")
		ENDIF
		NET_PRINT_VECTOR(SpawnResults.vCoords[i])
		NET_PRINT(", ")
		NET_PRINT_FLOAT(SpawnResults.fHeading[i])
		NET_PRINT(", score ")
		NET_PRINT_INT(SpawnResults.iScore[i])
	ENDREPEAT	
ENDPROC
#ENDIF

FUNC BOOL IsPointInsiderSearchArea(VECTOR vPoint, PRIVATE_SPAWN_SEARCH_PARAMS &PrivateParams)
	
	SWITCH PrivateParams.iAreaShape
		CASE SPAWN_AREA_SHAPE_CIRCLE 	
			RETURN IsPointInsideArea(vPoint, PrivateParams.iAreaShape, PrivateParams.vSearchCoord, <<0, 0, 0>>, PrivateParams.fSearchRadius)
		CASE SPAWN_AREA_SHAPE_BOX
		CASE SPAWN_AREA_SHAPE_ANGLED	
			RETURN IsPointInsideArea(vPoint, PrivateParams.iAreaShape, PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, PrivateParams.fAngledAreaWidth)		
	ENDSWITCH

	RETURN FALSE
	
ENDFUNC


PROC CheckFallbackSpawnpoints(PRIVATE_SPAWN_SEARCH_PARAMS &PrivateParams,  SPAWN_SEARCH_PARAMS &PublicParams)
	INT i
	VECTOR vec
//	VECTOR vecFromPoint
	FLOAT fHeading
	DEBUG_PRINTCALLSTACK()
	IF (g_SpawnData.iNumberOfFallbackSpawnPoints > 0)	
		REPEAT g_SpawnData.iNumberOfFallbackSpawnPoints i		
			// is point within search params?
			IF IsPointInsiderSearchArea(g_SpawnData.FallbackSpawnPoints[i].vPos, PrivateParams)	
			
				fHeading = g_SpawnData.FallbackSpawnPoints[i].fHeading
			
				// if fallback point is facing away from the facing direction, then
				IF (VMAG(PublicParams.vFacingCoords) > 0.01)
					
					vec = PublicParams.vFacingCoords - g_SpawnData.FallbackSpawnPoints[i].vPos
										
//					// if distance to facing point is far then just rotate 180
//					IF (VMAG(vec) > 40.0)
//						vecFromPoint = <<0.0, 1.0, 0.0>>
//						RotateVec(vecFromPoint, <<0.0, 0.0, g_SpawnData.FallbackSpawnPoints[i].fHeading>>)							
//						IF DOT_PRODUCT(vec, vecFromPoint) > 0									
//							fHeading += 180.0
//							PRINTLN("[spawning] CheckFallbackSpawnpoints - rotating heading 180.0") 
//						ENDIF
//					ELSE
						// else turn and face exact point
						PRINTLN("[spawning] CheckFallbackSpawnpoints - setting heading exact")
						fHeading = GET_HEADING_FROM_VECTOR_2D(vec.x, vec.y)
//					ENDIF
				
				ENDIF
			
				PRINTLN("[spawning] CheckFallbackSpawnpoints - analysing fallback point ", i, ", at ", g_SpawnData.FallbackSpawnPoints[i].vPos, " , ", fHeading) 					
				AnalyseSpawnPoint(g_SpawnData.FallbackSpawnPoints[i].vPos, fHeading, PrivateParams, PublicParams, FALSE, PrivateParams.iFallbackSpawnPointsDefaultResultFlag)			
				g_SpawnData.SearchTempData.Data.iNumOfFallbackSpawnPointResults += 1
			ELSE
				PRINTLN("[spawning] CheckFallbackSpawnpoints - analysing fallback point ", i, ", is not inside search params. At ", g_SpawnData.FallbackSpawnPoints[i].vPos, " , ", g_SpawnData.FallbackSpawnPoints[i].fHeading) 								
			ENDIF		
		ENDREPEAT	
	ELSE
		PRINTLN("[spawning] CheckFallbackSpawnpoints - none setup. ") 
	ENDIF
	
	IF (PrivateParams.bIsForLocalPlayerSpawning)
	AND (g_SpawnData.bUseUnsortedSearchResultsSystem)
		Get3BestResultsFromUnsorted(g_SpawnData.SearchTempData.TopResults[0], g_SpawnData.SearchTempData.TopResults[1], g_SpawnData.SearchTempData.TopResults[2])	
	ENDIF
	
	PRINTLN("[spawning] CheckFallbackSpawnpoints - finished - g_SpawnData.SearchTempData.Data.iNumOfFallbackSpawnPointResults = ", g_SpawnData.SearchTempData.Data.iNumOfFallbackSpawnPointResults) 
	PRINTLN("[spawning] CheckFallbackSpawnpoints - finished - g_SpawnData.SearchTempData.Data.bGotFallback = ", g_SpawnData.SearchTempData.Data.bGotFallback) 
ENDPROC

//FUNC BOOL GetSpawnPointFromRespawnSearch(SPAWN_RESULTS &SpawnResults, VECTOR vRawCoords, FLOAT fRawHeading, BOOL bDoVisibleChecks, BOOL bDoVisibilityChecksOnTeamMates, BOOL bIsForLocalPlayerSpawning, BOOL bDoNearARoadChecks, FLOAT fMinDistFromPlayer, BOOL bCloseToOriginAsPossible, BOOL bConsiderRawCoords, BOOL bForVehicle, BOOL bIsFallbackSearch, BOOL bConsiderInteriors)
FUNC BOOL GetSpawnPointFromRespawnSearch(SPAWN_RESULTS &SpawnResults,PRIVATE_SPAWN_SEARCH_PARAMS &PrivateParams, SPAWN_SEARCH_PARAMS &PublicParams, BOOL bIsFallbackSearch)

	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("GetSpawnPointFromRespawnSearch - start") 
	#ENDIF

	VECTOR vReturnCoords
	FLOAT fReturnHeading
	INT i

	IF (g_SpawnData.SearchTempData.Data.iNumOfResults = 0)
	AND (g_SpawnData.SearchTempData.Data.iNumOfFallbackSpawnPointResults = 0)
	
		IF (PrivateParams.bIsForLocalPlayerSpawning)
		AND NOT (bIsFallbackSearch)
			SWITCH NETWORK_QUERY_RESPAWN_RESULTS(g_SpawnData.SearchTempData.Data.iNumOfResults)
			
			
				CASE RESPAWN_QUERY_RESULTS_FAILED
					// if failed, use get safe coord method.
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_FAILED - using fall back method...") NET_NL()	
					NET_PRINT("[spawning] iNumOfResults = ") NET_PRINT_INT(g_SpawnData.SearchTempData.Data.iNumOfResults) NET_NL()	
					#ENDIF
					
					CheckFallbackSpawnpoints(PrivateParams, PublicParams)
					
					IF NOT (g_SpawnData.SearchTempData.Data.bGotFallback)
					
						IF (PublicParams.bConsiderOriginAsValidPoint)
						AND (g_SpawnData.PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE)
							SpawnResults.vCoords[0] = PrivateParams.vSearchCoord
							SpawnResults.fHeading[0] = PrivateParams.fRawHeading
							RETURN(TRUE)
						ELSE
							IF (PrivateParams.bUseOffsetOrigin)
								SpawnResults.vCoords[0] = PrivateParams.vOffsetOrigin
							ELSE
								SpawnResults.vCoords[0] = g_SpawnData.vRespawnSearch_CurrentSearch
							ENDIF
							IF (PrivateParams.bIsForLocalPlayerSpawning) 
							AND IS_SPAWN_LOCATION_MISSION_AREA_CHECK(g_SpawnData.SpawnLocation) 
								IF NOT (g_SpawnData.SearchTempData.Data.bIsFallbackSearch)
									g_SpawnData.SearchTempData.Data.bIsFallbackSearch = TRUE
								ELSE
									//GetSafeCoord(SpawnResults.vCoords[0], PublicParams.fMinDistFromPlayer, TRUE, PrivateParams.bForAVehicle, DEFAULT, TRUE, PublicParams.bConsiderInteriors)
									GetSafeCoord(SpawnResults.vCoords[0], TRUE, FALSE, TRUE, TRUE, PrivateParams, PublicParams)
								ENDIF
							ELSE
								//GetSafeCoord(SpawnResults.vCoords[0], PublicParams.fMinDistFromPlayer, DEFAULT, PrivateParams.bForAVehicle, DEFAULT, TRUE, PublicParams.bConsiderInteriors)
								GetSafeCoord(SpawnResults.vCoords[0], FALSE, FALSE, TRUE, FALSE, PrivateParams, PublicParams)
							ENDIF
							SpawnResults.fHeading[0] = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)			
							RETURN(TRUE)
						ENDIF	
						
					ELSE
						PRINTLN("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_FAILED - but got a fallback spoint point. iNumOfFallbackSpawnPointResults = ", g_SpawnData.SearchTempData.Data.iNumOfFallbackSpawnPointResults)
					ENDIF
				BREAK
				CASE RESPAWN_QUERY_RESULTS_SUCCEEDED 
				
					CheckFallbackSpawnpoints(PrivateParams, PublicParams)
				
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - - iNumOfResults = ") NET_PRINT_INT(g_SpawnData.SearchTempData.Data.iNumOfResults) 	NET_NL()		
					IF (PrivateParams.bDoVisibleChecks)
						NET_PRINT("[spawning]  PrivateParams.bDoVisibleChecks = TRUE")
					ELSE
						NET_PRINT("[spawning]  PrivateParams.bDoVisibleChecks = FALSE")
					ENDIF
					NET_NL()
					#ENDIF	
										
				BREAK
				CASE RESPAWN_QUERY_RESULTS_STILL_WORKING 	
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_STILL_WORKING - iNumOfResults = ") NET_PRINT_INT(g_SpawnData.SearchTempData.Data.iNumOfResults) NET_NL()
					#ENDIF
					RETURN(FALSE)
				BREAK
				CASE RESPAWN_QUERY_RESULTS_NO_SEARCH_ACTIVE
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_NO_SEARCH_ACTIVE") NET_NL()
					#ENDIF
					RETURN(FALSE)
				BREAK
			ENDSWITCH
		ELSE

			IF SPAWNPOINTS_IS_SEARCH_ACTIVE()
				IF NOT SPAWNPOINTS_IS_SEARCH_FAILED()
					IF SPAWNPOINTS_IS_SEARCH_COMPLETE()
						
						CheckFallbackSpawnpoints(PrivateParams, PublicParams)
						
						g_SpawnData.SearchTempData.Data.iNumOfResults = SPAWNPOINTS_GET_NUM_SEARCH_RESULTS()				
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - SPAWNPOINTS_IS_SEARCH_COMPLETE - - iNumOfResults = ") NET_PRINT_INT(g_SpawnData.SearchTempData.Data.iNumOfResults) 	NET_NL()		
						IF (PrivateParams.bDoVisibleChecks)
							NET_PRINT("[spawning]  PrivateParams.bDoVisibleChecks = TRUE")
						ELSE
							NET_PRINT("[spawning]  PrivateParams.bDoVisibleChecks = FALSE")
						ENDIF
						NET_NL()
						#ENDIF			
					ELSE
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - SPAWNPOINTS_IS_SEARCH_COMPLETE = FALSE") NET_NL()
						#ENDIF
						RETURN(FALSE)
					ENDIF
				ELSE									
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - SPAWNPOINTS_IS_SEARCH_FAILED = TRUE") NET_NL()
					#ENDIF
					SPAWNPOINTS_CANCEL_SEARCH()	
					
					CheckFallbackSpawnpoints(PrivateParams, PublicParams)
					
					IF NOT (g_SpawnData.SearchTempData.Data.bGotFallback)
					
						g_SpawnData.SearchTempData.Data.bIsFallbackSearch = TRUE
						RETURN(TRUE)
					ELSE
						PRINTLN("[spawning] GetSpawnPointFromRespawnSearch - SPAWNPOINTS_IS_SEARCH_FAILED , but got a fallback spawn point.")
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - SPAWNPOINTS_IS_SEARCH_ACTIVE = FALSE") NET_NL()
				#ENDIF
				RETURN(FALSE)
			ENDIF

		ENDIF
	ENDIF
	
	// store the nearest mission coronas and angled areas
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		Get_Nearest_Mission_Coronas_And_Gang_Attacks_To_Point(g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.SearchTempData.NearestCoronas, g_SpawnData.SearchTempData.NearestGangAttack)
		#IF IS_DEBUG_BUILD
		PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("GetSpawnPointFromRespawnSearch - after Get_Nearest_Mission_Coronas_And_Gang_Attacks_To_Point") 
		#ENDIF
	ENDIF
	
	
	// consider raw
	IF (PublicParams.bConsiderOriginAsValidPoint) AND NOT (g_SpawnData.SearchTempData.Data.bHasConsideredRaw)
		#IF IS_DEBUG_BUILD
		IF NOT (g_SpawnData.PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE)
			NET_PRINT("[spawning]  RESPAWN_QUERY_RESULTS_SUCCEEDED - cannot consider raw coords from non-circle")  NET_NL()	
			SCRIPT_ASSERT("RESPAWN_QUERY_RESULTS_SUCCEEDED - cannot consider raw coords from non-circle")
		ENDIF
		#ENDIF
		
		NET_PRINT("[spawning]  RESPAWN_QUERY_RESULTS_SUCCEEDED - considering raw coords ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord)  NET_NL()
		
		g_SpawnData.SearchTempData.Data.bHasConsideredRaw = TRUE
		
		AnalyseSpawnPoint(PrivateParams.vSearchCoord, PrivateParams.fRawHeading, PrivateParams, PublicParams, TRUE, -1)
						
	ENDIF
	
	// analyse results
	INT iResultsFlag
	INT iResultsProcessedThisFrame
	iResultsProcessedThisFrame = 0
	
	INT iMaxIterationsPerFrame
	IF (PrivateParams.bIsForLocalPlayerSpawning)
		iMaxIterationsPerFrame = 64
	ELSE
		iMaxIterationsPerFrame = 32
	ENDIF

	IF (g_SpawnData.SearchTempData.Data.iNumOfResults > 0)
	OR (g_SpawnData.SearchTempData.Data.iNumOfFallbackSpawnPointResults > 0)
	
		IF (PrivateParams.bIsForLocalPlayerSpawning) OR (SPAWNPOINTS_IS_SEARCH_ACTIVE())
	
	
			REPEAT g_SpawnData.SearchTempData.Data.iNumOfResults i
														
				IF (iResultsProcessedThisFrame < iMaxIterationsPerFrame) // max number of iterations in a frame
																	
					// if we are starting from next frame
					IF (i <= g_SpawnData.SearchTempData.Data.iLastResultProcessed)
						i = g_SpawnData.SearchTempData.Data.iLastResultProcessed + 1
					ENDIF
					
					// check i is within a valid range
					IF (i > (g_SpawnData.SearchTempData.Data.iNumOfResults - 1))
						NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - i too high = ") NET_PRINT_INT(i) NET_NL()	
						i = g_SpawnData.SearchTempData.Data.iNumOfResults - 1
					ENDIF
					IF (i < 0)
						NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - i too low = ") NET_PRINT_INT(i) NET_NL()	
						i = 0
					ENDIF
														
					IF (PrivateParams.bIsForLocalPlayerSpawning)
					AND NOT (bIsFallbackSearch)
						NETWORK_GET_RESPAWN_RESULT(i, vReturnCoords, fReturnHeading)
						fReturnHeading = fReturnHeading * 57.2957795 // (it's in radians)
					ELSE
						SPAWNPOINTS_GET_SEARCH_RESULT(i, vReturnCoords.x, vReturnCoords.y, vReturnCoords.z)
					ENDIF
					

					#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
							NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - checking point ") NET_PRINT_INT(i) NET_PRINT(" ") NET_PRINT_VECTOR(vReturnCoords) NET_PRINT(", heading = ") NET_PRINT_FLOAT(fReturnHeading) NET_NL()	
						ENDIF
					#ENDIF

					IF (PrivateParams.bIsForLocalPlayerSpawning)
					AND NOT (bIsFallbackSearch)
						iResultsFlag = NETWORK_GET_RESPAWN_RESULT_FLAGS(i)
					ELSE
						SPAWNPOINTS_GET_SEARCH_RESULT_FLAGS(i, iResultsFlag)
					ENDIF

					AnalyseSpawnPoint(vReturnCoords, fReturnHeading, PrivateParams, PublicParams, FALSE, iResultsFlag)
				
					iResultsProcessedThisFrame ++
					g_SpawnData.SearchTempData.Data.iLastResultProcessed = i
				
					#IF IS_DEBUG_BUILD
					PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("GetSpawnPointFromRespawnSearch - analysed point") 
					#ENDIF
				
				ELSE
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - reached max number of processes per frame, iLastResultProcessed = ") NET_PRINT_INT(g_SpawnData.SearchTempData.Data.iLastResultProcessed)  NET_NL()	
					#ENDIF	
					#IF IS_DEBUG_BUILD
					PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("GetSpawnPointFromRespawnSearch - end") 
					#ENDIF
					
					RETURN(FALSE)
				ENDIF
				
			ENDREPEAT
		
		ELSE
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - analysing results but SPAWNPOINTS_IS_SEARCH_ACTIVE = FALSE, iLastResultProcessed = ") NET_PRINT_INT(g_SpawnData.SearchTempData.Data.iLastResultProcessed)  NET_NL()	
			#ENDIF	
			i = g_SpawnData.SearchTempData.Data.iNumOfResults
		ENDIF
			
		
		// have we finished analysing all the results?
		IF (g_SpawnData.SearchTempData.Data.iNumOfResults = i)
		
			IF (PrivateParams.bIsForLocalPlayerSpawning)
			AND (g_SpawnData.bUseUnsortedSearchResultsSystem)
				Get3BestResultsFromUnsorted(g_SpawnData.SearchTempData.TopResults[0], g_SpawnData.SearchTempData.TopResults[1], g_SpawnData.SearchTempData.TopResults[2])	
			ENDIF
		
			IF (PrivateParams.bIsForLocalPlayerSpawning) 
			AND IS_SPAWN_LOCATION_MISSION_AREA_CHECK(g_SpawnData.SpawnLocation) 
				IF (g_SpawnData.SearchTempData.Data.bGotFallback)
				
					

					ConvertToSpawnResultStruct(SpawnResults, g_SpawnData.SearchTempData.TopResults)
					AddPointToLastSpawnSearchResults(SpawnResults.vCoords[0])
					#IF IS_DEBUG_BUILD
						IF (g_SpawnData.SearchTempData.TopResults[0].iFallbackScore < FALLBACK_SCORE_IN_SPAWNING_AREA) // never found a point inside the mission area								
							NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - tried all points, returning fallback point (couldn't get point in mission area) ") 
							PrintSpawnResultStruct(SpawnResults)
							NET_NL()		
						ELSE
							NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - tried all points, returning fallback ") 
							PrintSpawnResultStruct(SpawnResults)
							NET_NL()		
						ENDIF
					#ENDIF		
					
					#IF IS_DEBUG_BUILD
					PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("GetSpawnPointFromRespawnSearch - end") 
					#ENDIF
					
					RETURN(TRUE)
				ELSE		
				
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - no fallback, using fallback method") NET_NL()	
					#ENDIF
					//GetSafeCoord(g_SpawnData.vRespawnSearch_CurrentSearch, FALSE, PublicParams.fMinDistFromPlayer)			
					SpawnResults.vCoords[0] = g_SpawnData.vRespawnSearch_CurrentSearch
					//GetSafeCoord(SpawnResults.vCoords[0], PublicParams.fMinDistFromPlayer, DEFAULT, PrivateParams.bForAVehicle)
					GetSafeCoord(SpawnResults.vCoords[0], FALSE, FALSE, FALSE, FALSE, PrivateParams, PublicParams)
					
					SpawnResults.fHeading[0] = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)	
					AddPointToLastSpawnSearchResults(SpawnResults.vCoords[0])

					#IF IS_DEBUG_BUILD
					PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("GetSpawnPointFromRespawnSearch - end") 
					#ENDIF
					RETURN(TRUE)					
				
				ENDIF
			ELSE

				// if we are going to use fallback, at least start with coords that were on the spawn mesh
				IF (g_SpawnData.SearchTempData.Data.bGotFallback)
					ConvertToSpawnResultStruct(SpawnResults, g_SpawnData.SearchTempData.TopResults) 
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - tried all points, returning ")  PrintSpawnResultStruct(SpawnResults) 
					NET_NL()		
					#ENDIF
					AddPointToLastSpawnSearchResults(SpawnResults.vCoords[0])
					
					#IF IS_DEBUG_BUILD
					PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("GetSpawnPointFromRespawnSearch - end") 
					#ENDIF					
					RETURN(TRUE)
				ELSE
				
					// choose a random point anyway	
					IF (PrivateParams.bIsForLocalPlayerSpawning) 
						i = GET_RANDOM_INT_IN_RANGE(0, g_SpawnData.SearchTempData.Data.iNumOfResults)
						NETWORK_GET_RESPAWN_RESULT(i, SpawnResults.vCoords[0], SpawnResults.fHeading[0])
						IF NOT IS_POINT_IN_PROBLEM_AREA_FORBIDDEN(SpawnResults.vCoords[0])
							SpawnResults.fHeading[0] = SpawnResults.fHeading[0] * 57.2957795 // (its in radians)				
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - tried all points, picking a random one = ") NET_PRINT_INT(i) NET_PRINT(", ") PrintSpawnResultStruct(SpawnResults) NET_NL()		
							#ENDIF
							AddPointToLastSpawnSearchResults(SpawnResults.vCoords[0])
							
							#IF IS_DEBUG_BUILD
							PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("GetSpawnPointFromRespawnSearch - end") 
							#ENDIF						
							RETURN(TRUE)
						ELSE
							#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - tried picking random bug was in forbidden area") NET_NL()		
							#ENDIF
							SpawnResults.vCoords[0] = g_SpawnData.vRespawnSearch_CurrentSearch
							GetSafeCoord(SpawnResults.vCoords[0], TRUE, FALSE, TRUE, TRUE, PrivateParams, PublicParams )
							SpawnResults.fHeading[0] = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)		
							AddPointToLastSpawnSearchResults(SpawnResults.vCoords[0])
							RETURN(TRUE)
							
						ENDIF
			
					ELSE
										
						#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - tried all points, no fallback") NET_NL()		
						#ENDIF
						
						SpawnResults.vCoords[0] = g_SpawnData.vRespawnSearch_CurrentSearch
					
						//GetSafeCoord(SpawnResults.vCoords[0], PublicParams.fMinDistFromPlayer, DEFAULT, PrivateParams.bForAVehicle, DEFAULT, DEFAULT, PublicParams.bConsiderInteriors)
						GetSafeCoord(SpawnResults.vCoords[0], FALSE, FALSE, FALSE, FALSE, PrivateParams, PublicParams )
						
						SpawnResults.fHeading[0] = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)		
						AddPointToLastSpawnSearchResults(SpawnResults.vCoords[0])
						
						#IF IS_DEBUG_BUILD
						PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("GetSpawnPointFromRespawnSearch - end") 
						#ENDIF						
						RETURN(TRUE)						
					
					
					ENDIF
				
				ENDIF

			ENDIF
	

	
		ENDIF
	
	ELSE
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - RESPAWN_QUERY_RESULTS_SUCCEEDED - iNumOfResults = 0, using fallback method") NET_NL()		
			IF (PrivateParams.bIsForLocalPlayerSpawning) 
				//SCRIPT_ASSERT("NETWORK_QUERY_RESPAWN_RESULTS - returned 0 results, why? Is area loaded in?")
			ELSE
				// REMOVED THIS ASSERT BECAUSE OF 567276 - added assert to GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY instead
				//SCRIPT_ASSERT("SPAWNPOINTS_GET_NUM_SEARCH_RESULTS() - returned 0 results, why? Check you are running the search within 150m of player.")
			ENDIF
		#ENDIF
		
		SpawnResults.vCoords[0] = g_SpawnData.vRespawnSearch_CurrentSearch
	
		
		IF (PrivateParams.bIsForLocalPlayerSpawning) AND IS_SPAWN_LOCATION_MISSION_AREA_CHECK(g_SpawnData.SpawnLocation) 
			IF NOT (g_SpawnData.SearchTempData.Data.bIsFallbackSearch)
				g_SpawnData.SearchTempData.Data.bIsFallbackSearch = TRUE
			ELSE
			
				//GetSafeCoord(SpawnResults.vCoords[0], PublicParams.fMinDistFromPlayer, TRUE, PrivateParams.bForAVehicle, DEFAULT, TRUE, PublicParams.bConsiderInteriors)
				GetSafeCoord(SpawnResults.vCoords[0], TRUE, FALSE, TRUE, TRUE, PrivateParams, PublicParams )
				PRINTLN("[spawning] GetSpawnPointFromRespawnSearch - fallback - safe coord = ", SpawnResults.vCoords[0])
				
			ENDIF						
		ELSE
			BOOL bConsiderInactiveNodes
			IF (PrivateParams.bForAVehicle)
				bConsiderInactiveNodes = FALSE
			ELSE
				bConsiderInactiveNodes = TRUE
			ENDIF
			//GetSafeCoord(SpawnResults.vCoords[0], PublicParams.fMinDistFromPlayer, DEFAULT, PrivateParams.bForAVehicle, bConsiderInactiveNodes, DEFAULT, PublicParams.bConsiderInteriors)
			GetSafeCoord(SpawnResults.vCoords[0], FALSE, bConsiderInactiveNodes, FALSE, FALSE, PrivateParams, PublicParams)
		ENDIF		
		
		// if 
		
		SpawnResults.fHeading[0] = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)		
		AddPointToLastSpawnSearchResults(SpawnResults.vCoords[0])
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] GetSpawnPointFromRespawnSearch - Returning ")  PrintSpawnResultStruct(SpawnResults) NET_NL()		
		#ENDIF
		
		RETURN(TRUE)				
	
	ENDIF	
	
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("GetSpawnPointFromRespawnSearch - end") 
	#ENDIF

	// default return false
	RETURN(FALSE)

ENDFUNC

PROC RemoveNavmeshForSpawnSearch()
	IF (g_SpawnData.bNavMeshRequested)
		IF (GET_ID_OF_THIS_THREAD() = g_SpawnData.NavMeshRequestedThreadID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] RemoveNavmeshForSpawnSearch() - removing navmesh, was originally requested by this thread.") NET_NL()
			#ENDIF				
			REMOVE_NAVMESH_REQUIRED_REGIONS()	
		ELSE
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] RemoveNavmeshForSpawnSearch() - NOT removing navmesh, was not requested by this thread. But removing anyway...") NET_NL()
			#ENDIF				
			REMOVE_NAVMESH_REQUIRED_REGIONS() // added 8/3/2013 by NeilF - i think this can be called globally now, james B should have removed the assert. 
		ENDIF
		g_SpawnData.bNavMeshRequested = FALSE
	ENDIF
ENDPROC

PROC LoadNavMeshForSpawnSearch(FLOAT x, FLOAT y)

	#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bNavMeshRequested)
			IF NOT (GET_ID_OF_THIS_THREAD() = g_SpawnData.NavMeshRequestedThreadID)
				IF IS_THREAD_ACTIVE(g_SpawnData.NavMeshRequestedThreadID)
					SCRIPT_ASSERT("LoadNavMeshForSpawnSearch = nav mesh already loaded by different running thread")
				ENDIF
			ENDIF
		ENDIF		
		NET_PRINT("[spawning] LoadNavMeshForSpawnSearch() - called by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
	#ENDIF

	ADD_NAVMESH_REQUIRED_REGION(x, y, 0.1)
	g_SpawnData.NavMeshRequestedThreadID = GET_ID_OF_THIS_THREAD()
	g_SpawnData.bNavMeshRequested = TRUE
	g_SpawnData.iNavMeshLoadTime = GET_NETWORK_TIME()
ENDPROC

PROC ReloadNavMeshForSpawnSearch(FLOAT x, FLOAT y)
	NET_PRINT("[spawning] ReloadNavMeshForSpawnSearch() - called by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
	RemoveNavmeshForSpawnSearch()
	LoadNavMeshForSpawnSearch(x, y)
ENDPROC

PROC MaintainRespawnSearchCleanup()
	IF (g_SpawnData.bRespawnSearch_HasStarted = TRUE)	
		IF (g_SpawnData.RespawnSearchThreadID = GET_ID_OF_THIS_THREAD())
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_LastUpdateTime) > (SpawningFunctionTimeoutValue() + 2000) ) 
				NETWORK_CANCEL_RESPAWN_SEARCH()	
				SPAWNPOINTS_CANCEL_SEARCH()
				RemoveNavmeshForSpawnSearch()			
				g_SpawnData.bRespawnSearch_HasStarted = FALSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] MaintainRespawnSearchCleanup() - cleaning up an old respawn search.") NET_NL()
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC AddFallbackSP(VECTOR vPos, FLOAT fHeading #IF IS_DEBUG_BUILD, FLOAT fOverrideDistanceCheck = FALLBACK_SPAWN_POINT_MIN_DIST #ENDIF)
	IF (g_SpawnData.iNumberOfFallbackSpawnPoints < MAX_NUMBER_OF_FALLBACK_SPAWN_POINTS)
	
		// check new point is not too close to another spawn point
		#IF IS_DEBUG_BUILD
		INT i
		REPEAT g_SpawnData.iNumberOfFallbackSpawnPoints i 
			IF (VDIST(g_SpawnData.FallbackSpawnPoints[i].vPos, vPos) < fOverrideDistanceCheck )
				NET_PRINT("[spawning] ADD_FALLBACK_SPAWN_POINT - adding point - ") NET_PRINT_VECTOR(vPos) NET_NL()
				NET_PRINT("[spawning] ADD_FALLBACK_SPAWN_POINT - existing point which is too close - ") NET_PRINT_VECTOR(g_SpawnData.FallbackSpawnPoints[i].vPos) NET_NL()				
				SCRIPT_ASSERT("ADD_FALLBACK_SPAWN_POINT - Point is too close to another fallback spawn point!")				
			ENDIF
		ENDREPEAT
		#ENDIF
		
		// check new point is not at origin
		IF (VMAG(vPos) <= 0.01)
			NET_PRINT("[spawning] ADD_FALLBACK_SPAWN_POINT - trying to add point at origin. ignoring. ") NET_NL()
			SCRIPT_ASSERT("ADD_FALLBACK_SPAWN_POINT - trying to add point at origin!")	
			EXIT
		ENDIF
	
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] ADD_FALLBACK_SPAWN_POINT - adding point ") NET_PRINT_INT(g_SpawnData.iNumberOfFallbackSpawnPoints) 
		NET_PRINT(" at ") NET_PRINT_VECTOR(vPos) NET_PRINT(" heading = ") NET_PRINT_FLOAT(fHeading) 
		NET_PRINT(" from script ") NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_NL()
		#ENDIF	
	
		g_SpawnData.FallbackSpawnPoints[g_SpawnData.iNumberOfFallbackSpawnPoints].vPos = vPos
		g_SpawnData.FallbackSpawnPoints[g_SpawnData.iNumberOfFallbackSpawnPoints].fHeading = fHeading
		
		g_SpawnData.iNumberOfFallbackSpawnPoints++	
		
	ELSE
		NET_PRINT("[spawning] ADD_FALLBACK_SPAWN_POINT - reached MAX_NUMBER_OF_FALLBACK_SPAWN_POINTS ") NET_NL()	
		SCRIPT_ASSERT("ADD_FALLBACK_SPAWN_POINT - reached MAX_NUMBER_OF_FALLBACK_SPAWN_POINTS")	
	ENDIF
ENDPROC

#IF FEATURE_HEIST_ISLAND
PROC AddFallbackSpawnPointsForHeistIsland()
	PRINTLN("[spawning] AddFallbackSpawnPointsForHeistIsland called")
	ClearFallbackSpawnPoints()
	
	AddFallbackSP(<<4864.2778, -4938.1196, 0.9391>>, 63.5865)
	AddFallbackSP(<<4864.4409, -4914.3345, 0.9497>>, 85.1855)
	AddFallbackSP(<<4859.5234, -4953.1401, 0.9750>>, 75.3712)
	AddFallbackSP(<<4880.6362, -4948.5752, 2.5002>>, 302.6659)
	AddFallbackSP(<<4910.7446, -4900.0684, 2.3761>>, 150.7358)
	AddFallbackSP(<<4929.6025, -4904.1519, 2.5670>>, 110.8611)
	AddFallbackSP(<<4915.3564, -4916.7026, 2.3697>>, 107.2071)
	AddFallbackSP(<<4907.0161, -4915.7354, 2.3636>>, 85.3700)
	AddFallbackSP(<<4889.8203, -4921.3057, 2.3706>>, 342.4274)
	AddFallbackSP(<<4893.9653, -4932.9395, 2.3677>>, 1.8517)
	AddFallbackSP(<<4901.8589, -4931.4033, 2.3645>>, 37.4753)
	AddFallbackSP(<<4909.0615, -4929.8193, 2.3643>>, 85.3780)
	AddFallbackSP(<<4902.1143, -4938.8745, 2.3622>>, 205.5794)
	AddFallbackSP(<<4892.1870, -4945.9619, 2.3634>>, 347.7491)
	AddFallbackSP(<<4876.9063, -4907.9751, 2.1828>>, 241.3200)
	AddFallbackSP(<<4766.4683, -4718.1567, 1.4946>>, 189.9693)
	AddFallbackSP(<<4660.6616, -4657.6021, 1.7121>>, 173.9258)
	AddFallbackSP(<<4540.4917, -4701.6382, 0.9996>>, 203.2420)
	AddFallbackSP(<<4361.8027, -4634.0889, 1.2783>>, 200.2674)
	AddFallbackSP(<<4158.6538, -4639.0776, 3.1164>>, 257.0010)
	AddFallbackSP(<<4061.0776, -4695.1880, 3.0261>>, 25.5189)
	AddFallbackSP(<<3838.5879, -4750.8887, 1.2629>>, 100.9917)
	AddFallbackSP(<<3894.9590, -4632.1177, 1.1471>>, 349.1708)
	AddFallbackSP(<<4048.0894, -4592.1533, 1.1292>>, 27.1055)
	AddFallbackSP(<<4156.7192, -4434.0459, 2.1522>>, 86.9736)
	AddFallbackSP(<<4265.3545, -4276.5786, 1.5317>>, 354.3080)
	AddFallbackSP(<<4378.6670, -4357.6616, 1.7672>>, 322.6621)
	AddFallbackSP(<<4515.3081, -4405.4814, 2.7210>>, 334.5942)
	AddFallbackSP(<<4703.4380, -4392.1011, 2.5051>>, 33.2982)
	AddFallbackSP(<<4739.0679, -4292.9307, 3.1313>>, 29.9125)
	AddFallbackSP(<<4800.2515, -4298.4521, 4.0789>>, 203.4636)
	AddFallbackSP(<<4973.7896, -4410.7466, 2.2781>>, 348.8595)
	AddFallbackSP(<<5076.7178, -4554.2837, 4.2433>>, 213.7507)
	AddFallbackSP(<<5170.2129, -4582.7598, 2.4992>>, 243.2601)
	AddFallbackSP(<<5182.3350, -4674.2412, 1.4649>>, 160.5936)
	AddFallbackSP(<<5124.4473, -4742.2842, 0.9334>>, 213.6185)
	AddFallbackSP(<<5173.3247, -4881.0815, 1.4271>>, 305.9375)
	AddFallbackSP(<<5330.1357, -5098.7070, 13.6280>>, 236.2402)
	AddFallbackSP(<<5613.6294, -5259.3052, 9.8757>>, 247.4273)
	AddFallbackSP(<<5520.9243, -5432.1626, 23.1021>>, 259.2025)
	AddFallbackSP(<<5497.7485, -5592.1680, 13.0750>>, 273.0933)
	AddFallbackSP(<<5546.4272, -5726.1680, 9.1141>>, 242.8979)
	AddFallbackSP(<<5425.5186, -5920.5029, 11.6717>>, 97.5616)
	AddFallbackSP(<<5302.5127, -5837.5132, 14.6851>>, 97.2719)
	AddFallbackSP(<<5198.1958, -5773.8481, 12.3111>>, 80.7304)
	AddFallbackSP(<<4746.8149, -5512.1577, 15.9261>>, 56.1295)
	AddFallbackSP(<<4849.9751, -5415.0435, 17.4746>>, 303.1575)
	AddFallbackSP(<<4859.0269, -5339.9839, 12.2305>>, 340.2033)
	AddFallbackSP(<<4849.8853, -5174.2598, 1.4387>>, 273.0690)
	AddFallbackSP(<<4762.4585, -4997.8276, 23.0719>>, 43.8230)
ENDPROC
#ENDIF


//FUNC BOOL DoRespawnSearchForCoord(VECTOR vSearchCoord, FLOAT fRawHeading, FLOAT fSearchRadius, SPAWN_RESULTS &SpawnResults, BOOL bIsForLocalPlayerSpawning, VECTOR vSearchFacing, BOOL bDoTeamMateVisCheck, BOOL bConsiderInteriors, BOOL PublicParams.bPreferPointsCloserToRoads, FLOAT fMinDistFromPlayer, BOOL bCloseToOriginAsPossible, BOOL bIsAngledArea, VECTOR vAngledAreaPoint1, VECTOR vAngledAreaPoint2, FLOAT fAngledAreaWidth, BOOL bConsiderRawCoords, BOOL bSearchVehicleNodesOnly, BOOL bUseOnlyBoatNodes, BOOL bForAVehicle, BOOL bEdgesOnly, BOOL bDoVisibleChecks, BOOL bIgnoreTeammatesForMinDistCheck)
FUNC BOOL DoRespawnSearchForCoord(PRIVATE_SPAWN_SEARCH_PARAMS &PrivateParams, SPAWN_SEARCH_PARAMS &PublicParams, SPAWN_RESULTS &SpawnResults) 

	INT iFlags
	VECTOR vec
	FLOAT fMinX, fMinY, fMaxX, fMaxY
	VECTOR vCentre
	VECTOR vNavSearchMin, vNavSearchMax
	INT i
	VECTOR vAngledCoords1, vAngledCoords2
	FLOAT fAngledWidth 
	
	// is this a new search?
	BOOL bIsNewSearch = FALSE
	
	SWITCH PrivateParams.iAreaShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			IF NOT (g_SpawnData.vRespawnSearch_CurrentSearch.x = PrivateParams.vSearchCoord.x)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch.y = PrivateParams.vSearchCoord.y)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch.z = PrivateParams.vSearchCoord.z)
			OR NOT (g_SpawnData.fRespawnSearch_CurrentRadius = PrivateParams.fSearchRadius)
				bIsNewSearch = TRUE	
			ENDIF		
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			IF NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point1.x = PrivateParams.vAngledAreaPoint1.x)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point1.y = PrivateParams.vAngledAreaPoint1.y)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point1.z = PrivateParams.vAngledAreaPoint1.z)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point2.x = PrivateParams.vAngledAreaPoint2.x)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point2.y = PrivateParams.vAngledAreaPoint2.y)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point2.z = PrivateParams.vAngledAreaPoint2.z)
				bIsNewSearch = TRUE	
			ENDIF			
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			IF NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point1.x = PrivateParams.vAngledAreaPoint1.x)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point1.y = PrivateParams.vAngledAreaPoint1.y)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point1.z = PrivateParams.vAngledAreaPoint1.z)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point2.x = PrivateParams.vAngledAreaPoint2.x)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point2.y = PrivateParams.vAngledAreaPoint2.y)
			OR NOT (g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point2.z = PrivateParams.vAngledAreaPoint2.z)
			OR NOT (g_SpawnData.fRespawnSearch_CurrentRadius_AngledArea_Width = PrivateParams.fAngledAreaWidth)
				bIsNewSearch = TRUE	
			ENDIF		
		BREAK		
	ENDSWITCH
	
	// if called from a different thread then its a new search. url:bugstar:6834895 
	#IF FEATURE_HEIST_ISLAND
	IF (g_bDoRespawnSearchForCoord_NewChecksEnabled)
	
		// its a new shape being passed in
		IF NOT (g_SpawnData.iRespawnSearch_Shape = PrivateParams.iAreaShape)
			bIsNewSearch = TRUE	
			PRINTLN("[spawning] DoRespawnSearchForCoord - this is a new search, different area shape")
		ENDIF	
		
		IF IS_THREAD_ACTIVE(g_SpawnData.RespawnSearchThreadID)
		AND NOT (g_SpawnData.RespawnSearchThreadID = GET_ID_OF_THIS_THREAD())
			bIsNewSearch = TRUE
			PRINTLN("[spawning] DoRespawnSearchForCoord - this is a new search called from a different thread")
		ENDIF	
	ENDIF
	#ENDIF
		
	IF (bIsNewSearch)
		
		IF (g_SpawnData.bRespawnSearch_HasStarted = TRUE)
		
			//IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_LastUpdateTime) < 1000)
			IF IS_THREAD_ACTIVE(g_SpawnData.RespawnSearchThreadID)			
				// is it this thread? 				
				IF (g_SpawnData.RespawnSearchThreadID = GET_ID_OF_THIS_THREAD())
					IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_LastUpdateTime) < SpawningFunctionTimeoutValue())
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] DoRespawnSearchForCoord - another search already started by this thread, returning FALSE called from ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
						NET_PRINT(", PrivateParams.vSearchCoord = ") NET_PRINT_VECTOR(PrivateParams.vSearchCoord)
						NET_PRINT(", PrivateParams.fSearchRadius = ") NET_PRINT_FLOAT(PrivateParams.fSearchRadius)
						NET_NL()
						#ENDIF		
						RETURN(FALSE)
					ELSE
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] DoRespawnSearchForCoord - another search already started by same thread, but not been updated in a while, so starting new one from script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
						NET_PRINT(", PrivateParams.vSearchCoord = ") NET_PRINT_VECTOR(PrivateParams.vSearchCoord)
						NET_PRINT(", PrivateParams.fSearchRadius = ") NET_PRINT_FLOAT(PrivateParams.fSearchRadius)
						NET_NL()	
						#ENDIF
					ENDIF
				ELSE
				
					IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_LastUpdateTime) < SpawningFunctionTimeoutValue()) 
				
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] DoRespawnSearchForCoord - another search already started, returning FALSE - new search called from ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
						NET_PRINT(", PrivateParams.vSearchCoord = ") NET_PRINT_VECTOR(PrivateParams.vSearchCoord)
						NET_PRINT(", PrivateParams.fSearchRadius = ") NET_PRINT_FLOAT(PrivateParams.fSearchRadius)
						NET_NL()
						#ENDIF					
						
						// return FALSE, to give the previous search a chance to finish
						RETURN(FALSE)
						
					ELSE
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] DoRespawnSearchForCoord - another search already started by a different thread, but not been updated in a while, so starting new one from script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
						NET_PRINT(", PrivateParams.vSearchCoord = ") NET_PRINT_VECTOR(PrivateParams.vSearchCoord)
						NET_PRINT(", PrivateParams.fSearchRadius = ") NET_PRINT_FLOAT(PrivateParams.fSearchRadius)
						NET_NL()	
						#ENDIF
					ENDIF
				ENDIF
				
			ELSE
								
				#IF IS_DEBUG_BUILD		
				
					SWITCH PrivateParams.iAreaShape
						CASE SPAWN_AREA_SHAPE_CIRCLE
							NET_PRINT("[spawning] DoRespawnSearchForCoord - another search already started, but not thread is not active, so starting new one from script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
							NET_PRINT(", PrivateParams.vSearchCoord = ") NET_PRINT_VECTOR(PrivateParams.vSearchCoord)
							NET_PRINT(", PrivateParams.fSearchRadius = ") NET_PRINT_FLOAT(PrivateParams.fSearchRadius)
							NET_NL()
						BREAK
						CASE SPAWN_AREA_SHAPE_BOX
							NET_PRINT("[spawning] DoRespawnSearchForCoord - another search already started, but not thread is not active, so starting new one from script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
							NET_PRINT(", PrivateParams.vAngledAreaPoint1 = ") NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint1)
							NET_PRINT(", PrivateParams.vAngledAreaPoint2 = ") NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint2)
							NET_NL()
						BREAK
						CASE SPAWN_AREA_SHAPE_ANGLED
							NET_PRINT("[spawning] DoRespawnSearchForCoord - another search already started, but not thread is not active, so starting new one from script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
							NET_PRINT(", PrivateParams.vAngledAreaPoint1 = ") NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint1)
							NET_PRINT(", PrivateParams.vAngledAreaPoint2 = ") NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint2)
							NET_PRINT(", PrivateParams.fAngledAreaWidth = ") NET_PRINT_FLOAT(PrivateParams.fAngledAreaWidth)
							NET_NL()
						BREAK
					ENDSWITCH
				#ENDIF
			
			ENDIF
		
			NETWORK_CANCEL_RESPAWN_SEARCH()	
			SPAWNPOINTS_CANCEL_SEARCH()
			RemoveNavmeshForSpawnSearch()
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] DoRespawnSearchForCoord - calling NETWORK_CANCEL_RESPAWN_SEARCH() & SPAWNPOINTS_CANCEL_SEARCH(), cleaning up from before.") NET_NL()
			#ENDIF
			
			
		ENDIF
			 
		g_SpawnData.bRespawnSearch_HasStarted = FALSE
		
	ELSE
		// not a new search but hasn't been updated in a while
		IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_LastUpdateTime) > SpawningFunctionTimeoutValue())
			NET_PRINT("[spawning] DoRespawnSearchForCoord - not a new search, but hasn't been updated in a while, resetting g_SpawnData.iRespawnSearch_Time") NET_NL()	
			NET_PRINT("[spawning] DoRespawnSearchForCoord - time since last update was ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_LastUpdateTime)) NET_NL()
			g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
			ClearSearchTempData()
			DEBUG_PRINTCALLSTACK()	
		ENDIF
	ENDIF


	// for loading the car nodes, calculate the max min values
	SWITCH PrivateParams.iAreaShape
		CASE SPAWN_AREA_SHAPE_CIRCLE
			fMinX = PrivateParams.vSearchCoord.x - PrivateParams.fSearchRadius
			fMinY = PrivateParams.vSearchCoord.y - PrivateParams.fSearchRadius
			fMaxX = PrivateParams.vSearchCoord.x + PrivateParams.fSearchRadius
			fMaxY = PrivateParams.vSearchCoord.y + PrivateParams.fSearchRadius
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX
			IF (PrivateParams.vAngledAreaPoint1.x < PrivateParams.vAngledAreaPoint2.x)
				fMinX = PrivateParams.vAngledAreaPoint1.x 
				fMaxX = PrivateParams.vAngledAreaPoint2.x 
			ELSE
				fMinX = PrivateParams.vAngledAreaPoint2.x
				fMaxX = PrivateParams.vAngledAreaPoint1.x 
			ENDIF
			IF (PrivateParams.vAngledAreaPoint1.y < PrivateParams.vAngledAreaPoint2.y)
				fMinY = PrivateParams.vAngledAreaPoint1.y 
				fMaxY = PrivateParams.vAngledAreaPoint2.y 
			ELSE
				fMinY = PrivateParams.vAngledAreaPoint2.y 
				fMaxY = PrivateParams.vAngledAreaPoint1.y 
			ENDIF
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED
			IF (PrivateParams.vAngledAreaPoint1.x < PrivateParams.vAngledAreaPoint2.x)
				fMinX = PrivateParams.vAngledAreaPoint1.x - (0.5 * PrivateParams.fAngledAreaWidth)
				fMaxX = PrivateParams.vAngledAreaPoint2.x + (0.5 * PrivateParams.fAngledAreaWidth)
			ELSE
				fMinX = PrivateParams.vAngledAreaPoint2.x - (0.5 * PrivateParams.fAngledAreaWidth)
				fMaxX = PrivateParams.vAngledAreaPoint1.x + (0.5 * PrivateParams.fAngledAreaWidth)
			ENDIF
			IF (PrivateParams.vAngledAreaPoint1.y < PrivateParams.vAngledAreaPoint2.y)
				fMinY = PrivateParams.vAngledAreaPoint1.y - (0.5 * PrivateParams.fAngledAreaWidth)
				fMaxY = PrivateParams.vAngledAreaPoint2.y + (0.5 * PrivateParams.fAngledAreaWidth)
			ELSE
				fMinY = PrivateParams.vAngledAreaPoint2.y - (0.5 * PrivateParams.fAngledAreaWidth)
				fMaxY = PrivateParams.vAngledAreaPoint1.y + (0.5 * PrivateParams.fAngledAreaWidth)
			ENDIF
		BREAK
	ENDSWITCH
	
	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(fMinX , fMinY, fMaxX, fMaxY)
	
	// calculate the centre point for loading the nav mesh
	IF (PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE)
		vCentre = PrivateParams.vSearchCoord
	ELSE
		vCentre = (PrivateParams.vAngledAreaPoint1 + PrivateParams.vAngledAreaPoint2) * 0.5
	ENDIF
	vNavSearchMin = vCentre + <<NAV_SEARCH_MIN, NAV_SEARCH_MIN, NAV_SEARCH_MIN>>
	vNavSearchMax = vCentre + <<NAV_SEARCH_MAX, NAV_SEARCH_MAX, NAV_SEARCH_MAX>>
	

	// start the search
	IF NOT (g_SpawnData.bRespawnSearch_HasStarted)
						
		// clear any searches which were left dangling.			
		NETWORK_CANCEL_RESPAWN_SEARCH()	
		SPAWNPOINTS_CANCEL_SEARCH()		
		RemoveNavmeshForSpawnSearch()
	
		// is raw point ok, if so quit here
		IF (PublicParams.bConsiderOriginAsValidPoint)
		AND (PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE)
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] DoRespawnSearchForCoord - considering raw point") NET_NL()
			#ENDIF
			IF (VMAG(PrivateParams.vSearchCoord)) > 0.0 // check it's not the origin
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(PrivateParams.vSearchCoord,6,1,1,5,TRUE,TRUE,TRUE,120,FALSE,-1,TRUE,PublicParams.fMinDistFromPlayer, PrivateParams.bIgnoreTeammatesForMinDistCheck)	
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] DoRespawnSearchForCoord - raw point is ok returning TRUE") NET_NL()
					#ENDIF
					SpawnResults.vCoords[0] = PrivateParams.vSearchCoord
					SpawnResults.fHeading[0] = PrivateParams.fRawHeading
					RETURN(TRUE)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] DoRespawnSearchForCoord - raw point is at origin") NET_NL()
				#ENDIF	
			ENDIF
		ENDIF
	
		IF NOT SPAWNPOINTS_IS_SEARCH_ACTIVE() // check to make sure the cancel has completed
	
			// save the details			
			g_SpawnData.iRespawnSearch_Shape = PrivateParams.iAreaShape
			SWITCH PrivateParams.iAreaShape
				CASE SPAWN_AREA_SHAPE_CIRCLE
					g_SpawnData.vRespawnSearch_CurrentSearch = PrivateParams.vSearchCoord
					g_SpawnData.fRespawnSearch_CurrentRadius = PrivateParams.fSearchRadius
				BREAK
				CASE SPAWN_AREA_SHAPE_BOX
					g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point1 = PrivateParams.vAngledAreaPoint1
					g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point2 = PrivateParams.vAngledAreaPoint2
					g_SpawnData.fRespawnSearch_CurrentRadius_AngledArea_Width = 0.0
					g_SpawnData.vRespawnSearch_CurrentSearch = (PrivateParams.vAngledAreaPoint1 + PrivateParams.vAngledAreaPoint2)/2.0
				BREAK
				CASE SPAWN_AREA_SHAPE_ANGLED
					g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point1 = PrivateParams.vAngledAreaPoint1
					g_SpawnData.vRespawnSearch_CurrentSearch_AngledArea_Point2 = PrivateParams.vAngledAreaPoint2
					g_SpawnData.fRespawnSearch_CurrentRadius_AngledArea_Width = PrivateParams.fAngledAreaWidth
					g_SpawnData.vRespawnSearch_CurrentSearch = (PrivateParams.vAngledAreaPoint1 + PrivateParams.vAngledAreaPoint2)/2.0
				BREAK			
			ENDSWITCH
			
			
			// start loading the nav mesh
			IF NOT (PublicParams.bSearchVehicleNodesOnly)
			AND NOT (PublicParams.bUseOnlyBoatNodes)
				LoadNavMeshForSpawnSearch(vCentre.x, vCentre.y)
			ENDIF

			g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_LOADING_CAR_NODES
		
			g_SpawnData.bRespawnSearch_HasStarted = TRUE
			
			g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
			g_SpawnData.iRespawnSearch_LastUpdateTime = GET_NETWORK_TIME()
			
			g_SpawnData.RespawnSearchThreadID = GET_ID_OF_THIS_THREAD()		
						
			
			#IF IS_DEBUG_BUILD
			
			NET_PRINT("[spawning] DoRespawnSearchForCoord - ")
			DEBUG_PRINT_SPAWN_SEARCH_PARAMS(PublicParams)
			DEBUG_PRINT_PRIVATE_SPAWN_SEARCH_PARAMS(PrivateParams)

			
			NET_PRINT("[spawning] DoRespawnSearchForCoord - new search starting, called from ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
			SWITCH PrivateParams.iAreaShape
				CASE SPAWN_AREA_SHAPE_CIRCLE
					NET_PRINT(", PrivateParams.vSearchCoord = ") NET_PRINT_VECTOR(PrivateParams.vSearchCoord)
					NET_PRINT(", PrivateParams.fSearchRadius = ") NET_PRINT_FLOAT(PrivateParams.fSearchRadius)
					NET_PRINT(", dist from coords = ") NET_PRINT_FLOAT(VDIST(GET_PLAYER_COORDS(PLAYER_ID()), PrivateParams.vSearchCoord))
				BREAK
				CASE SPAWN_AREA_SHAPE_BOX
					NET_PRINT(", PrivateParams.vAngledAreaPoint1 = ") NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint1)
					NET_PRINT(", PrivateParams.vAngledAreaPoint2 = ") NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint2)
					NET_PRINT(", dist from coords = ") NET_PRINT_FLOAT(VDIST(GET_PLAYER_COORDS(PLAYER_ID()), ((PrivateParams.vAngledAreaPoint1+PrivateParams.vAngledAreaPoint2)/2.0)))
				BREAK
				CASE SPAWN_AREA_SHAPE_ANGLED
					NET_PRINT(", PrivateParams.vAngledAreaPoint1 = ") NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint1)
					NET_PRINT(", PrivateParams.vAngledAreaPoint2 = ") NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint2)
					NET_PRINT(", PrivateParams.fAngledAreaWidth = ") NET_PRINT_FLOAT(PrivateParams.fAngledAreaWidth)
					NET_PRINT(", dist from coords = ") NET_PRINT_FLOAT(VDIST(GET_PLAYER_COORDS(PLAYER_ID()), ((PrivateParams.vAngledAreaPoint1+PrivateParams.vAngledAreaPoint2)/2.0)))
				BREAK
			ENDSWITCH			
			
			NET_PRINT(", host player coords = ") NET_PRINT_VECTOR(GET_PLAYER_COORDS(PLAYER_ID()))		
			NET_NL()
			#ENDIF
		
		ELSE
			NET_PRINT("[spawning] DoRespawnSearchForCoord - waiting for search to no longer be active before continuing... ")  NET_NL()
			RETURN(FALSE)
		ENDIF
		
	ENDIF
	
	
	// the search has started
	IF (g_SpawnData.bRespawnSearch_HasStarted)
	
		// loading car nodes
		IF (g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_LOADING_CAR_NODES)
			IF ARE_NODES_LOADED_FOR_AREA(fMinX , fMinY, fMaxX, fMaxY)
			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) > 5000)
				g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
				
				IF (PublicParams.bSearchVehicleNodesOnly)
				OR (PublicParams.bUseOnlyBoatNodes)	
				
					IF (PrivateParams.bUseOffsetOrigin)
						SpawnResults.vCoords[0] = PrivateParams.vOffsetOrigin
					ELSE
						SpawnResults.vCoords[0] = vCentre
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF ARE_NODES_LOADED_FOR_AREA(fMinX , fMinY, fMaxX, fMaxY)
						NET_PRINT("[spawning] DoRespawnSearchForCoord - nodes are loaded for area ") 
						NET_PRINT_FLOAT(fMinX) NET_PRINT(", ")
						NET_PRINT_FLOAT(fMinY) NET_PRINT(", ")
						NET_PRINT_FLOAT(fMaxX) NET_PRINT(", ")
						NET_PRINT_FLOAT(fMaxY) NET_NL()
					ELSE
						NET_PRINT("[spawning] DoRespawnSearchForCoord - nodes are NOT loaded for area ") 
						NET_PRINT_FLOAT(fMinX) NET_PRINT(", ")
						NET_PRINT_FLOAT(fMinY) NET_PRINT(", ")
						NET_PRINT_FLOAT(fMaxX) NET_PRINT(", ")
						NET_PRINT_FLOAT(fMaxY) NET_NL()	
					ENDIF
					#ENDIF
					
					NearestCarNodeSearch SearchParams
					SearchParams.vFavourFacing = PublicParams.vFacingCoords
					SearchParams.fMinPlayerDist = PublicParams.fMinDistFromPlayer
					SearchParams.bWaterNodesOnly = PublicParams.bUseOnlyBoatNodes
					SearchParams.bCheckInsideArea = TRUE
					IF (PublicParams.fUpperZLimitForNodes > 0.0)
						SearchParams.fUpperZLimit = PublicParams.fUpperZLimitForNodes
					ENDIF
					
					SWITCH PrivateParams.iAreaShape
						CASE SPAWN_AREA_SHAPE_CIRCLE
							SearchParams.vAreaCoords1 = PrivateParams.vSearchCoord
							SearchParams.fAreaFloat = PrivateParams.fSearchRadius
						BREAK
						CASE SPAWN_AREA_SHAPE_BOX
							SearchParams.vAreaCoords1 = PrivateParams.vAngledAreaPoint1
							SearchParams.vAreaCoords2 = PrivateParams.vAngledAreaPoint2
							SearchParams.fAreaFloat = 0.0
						BREAK
						CASE SPAWN_AREA_SHAPE_ANGLED
							SearchParams.vAreaCoords1 = PrivateParams.vAngledAreaPoint1
							SearchParams.vAreaCoords2 = PrivateParams.vAngledAreaPoint2
							SearchParams.fAreaFloat = PrivateParams.fAngledAreaWidth
						BREAK
					ENDSWITCH
					SearchParams.iAreaShape = PrivateParams.iAreaShape
					
					SearchParams.bCheckVehicleSpawning = PrivateParams.bForAVehicle
					
					IF (PrivateParams.bUseOffsetOrigin)
						SearchParams.bReturnNearestGoodNode = TRUE
						SearchParams.bUsingOffsetOrigin = TRUE
					ENDIF
					
					REPEAT MAX_NUM_AVOID_RADIUS i 
						SearchParams.vAvoidCoords[i] = PublicParams.vAvoidCoords[i]
						SearchParams.fAvoidRadius[i] =	PublicParams.fAvoidRadius[i]
					ENDREPEAT
					SearchParams.bIsForFlyingVehicle = PublicParams.bIsForFlyingVehicle
					SearchParams.bDoVisibilityChecks = PrivateParams.bDoVisibleChecks
					
					IF (PrivateParams.bVehiclesCanConsiderInactiveNodes)
						SearchParams.bConsiderOnlyActiveNodes = FALSE
					ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType)
						NET_PRINT("[spawning] DoRespawnSearchForCoord - Setting SearchParams.bGetSafeOffset to TRUE for Hostile Takeover") NET_NL()
						SearchParams.bGetSafeOffset = TRUE
					ENDIF
					
					SearchParams.bCanFaceOncomingTraffic = PrivateParams.bCanFaceOncomingTraffic
					
					GetNearestCarNode(SpawnResults.vCoords[0], SpawnResults.fHeading[0], SearchParams) // g_SpawnData.PublicParams.vFacingCoords, TRUE, FALSE, PublicParams.fMinDistFromPlayer, TRUE, PublicParams.bUseOnlyBoatNodes)
					
					// if returned boat node is not in area and we are allowed to use the raw coord, then go with that instead.	Fix for 1788808			
					IF (PublicParams.bConsiderOriginAsValidPoint)
					AND (PublicParams.bUseOnlyBoatNodes)
					AND (PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE)
						IF NOT IsPointInsideSphere(SpawnResults.vCoords[0], PrivateParams.vSearchCoord, PrivateParams.fSearchRadius, TRUE, TRUE)
							SpawnResults.vCoords[0] = PrivateParams.vSearchCoord
							SpawnResults.fHeading[0] = PrivateParams.fRawHeading
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] DoRespawnSearchForCoord - nearest node not within area, using raw coords instead.") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
							#ENDIF	
						ENDIF
					ENDIF
					
					g_SpawnData.iRespawnSearch_State = RESPAWN_RETURNING_TRUE
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] DoRespawnSearchForCoord - Nodes are loaded, going straight to  RESPAWN_RETURNING_TRUE") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
					#ENDIF	
				ELSE					
					g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_LOADING_NAV_MESH
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] DoRespawnSearchForCoord - Nodes are loaded ") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
					#ENDIF	
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] DoRespawnSearchForCoord - Loading car nodes ") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
				#ENDIF				
			ENDIF
		ENDIF
	
		// load the nav mesh
		IF (g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_LOADING_NAV_MESH)
			IF IS_NAVMESH_LOADED_IN_AREA(vNavSearchMin, vNavSearchMax)
			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) > 15000)
			OR (GET_NUM_NAVMESHES_EXISTING_IN_AREA(vNavSearchMin, vNavSearchMax) = 0)
			
				g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
			 	IF (PrivateParams.bIsForLocalPlayerSpawning)
				AND NOT IS_PLAYER_SCTV(PLAYER_ID())
					g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_LOADING_CLOUD_MISSION_DATA
				ELSE
					g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_START_SEARCH
				ENDIF				
				#IF IS_DEBUG_BUILD
					IF (GET_NUM_NAVMESHES_EXISTING_IN_AREA(vNavSearchMin, vNavSearchMax) = 0)
						NET_PRINT("[spawning] DoRespawnSearchForCoord - no nav meshes in area ") NET_NL()
					ELSE
						NET_PRINT("[spawning] DoRespawnSearchForCoord - number of nav meshes in area is ") NET_PRINT_INT(GET_NUM_NAVMESHES_EXISTING_IN_AREA(vNavSearchMin, vNavSearchMax)) NET_NL()	
						NET_PRINT("[spawning] DoRespawnSearchForCoord - number of nav meshes loaded in area is ") NET_PRINT_INT(GET_NUM_NAVMESHES_LOADED_IN_AREA(vNavSearchMin, vNavSearchMax)) NET_NL()
					ENDIF
				
				NET_PRINT("[spawning] DoRespawnSearchForCoord - Nav Mesh is loaded ") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
				#ENDIF
				
			ELSE	
				// if nav mesh hasn't loaded by 7 seconds, try reloaing
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iNavMeshLoadTime) > 7000)
					ReloadNavMeshForSpawnSearch(vCentre.x, vCentre.y)
				ENDIF
			
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] DoRespawnSearchForCoord - Loading nav mesh ") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
				#ENDIF	
			ENDIF
		ENDIF
	
		// make sure mission data has loaded from cloud
		IF (g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_LOADING_CLOUD_MISSION_DATA)
			IF Is_Initial_Cloud_Loaded_Mission_Data_Ready()
			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) > 10000)
				g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
				g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_START_SEARCH		
			ELSE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] DoRespawnSearchForCoord - Loading mission data from cloud ") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
				#ENDIF						
			ENDIF
		ENDIF
	
		// start the search
		IF (g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_START_SEARCH)
		
			IF SPAWNPOINTS_IS_SEARCH_ACTIVE() // check to make sure there is no other search running before starting a new one. 
				NETWORK_CANCEL_RESPAWN_SEARCH()	
				SPAWNPOINTS_CANCEL_SEARCH()
				NET_PRINT("[spawning] DoRespawnSearchForCoord - RESPAWN_SEARCH_START_SEARCH - cancelling another search before beginning") NET_NL()
			ELSE
				// reset the flags
				iFlags = 0
				
				ClearSearchTempData()
				
				// is for player respawning
				IF (PrivateParams.bIsForLocalPlayerSpawning)
				
					// face point?
					IF (VMAG(PublicParams.vFacingCoords) = 0.0)
						iFlags += ENUM_TO_INT(RESPAWN_QUERY_FLAG_IGNORE_TARGET_HEADING)
					ENDIF	
					
					// consider interiors?
					IF (PublicParams.bConsiderInteriors)
						iFlags += ENUM_TO_INT(RESPAWN_QUERY_FLAG_MAY_SPAWN_IN_INTERIOR)
					ENDIF	
					
					// exteriors are always considered.
					iFlags += ENUM_TO_INT(RESPAWN_QUERY_FLAG_MAY_SPAWN_IN_EXTERIOR)	
					
					// are we trying to get near to specific coord?
	//				IF (PublicParams.bCloseToOriginAsPossible)
	//					iFlags += ENUM_TO_INT(RESPAWN_QUERY_FLAG_PREFER_CLOSE_TO_SPAWN_ORIGIN)
	//				ELSE								
						iFlags += ENUM_TO_INT(RESPAWN_QUERY_FLAG_PREFER_WIDE_FOV)
						IF NOT (GET_PLAYER_TEAM(PLAYER_ID()) = -1)
							iFlags += ENUM_TO_INT(RESPAWN_QUERY_FLAG_PREFER_TEAM_BUNCHING)
							iFlags += ENUM_TO_INT(RESPAWN_QUERY_FLAG_PREFER_ENEMY_PLAYERS_FARTHER)
							iFlags += ENUM_TO_INT(RESPAWN_QUERY_FLAG_PREFER_FRIENDLY_PLAYERS_CLOSER)
						ENDIF
						iFlags += ENUM_TO_INT(RESPAWN_QUERY_FLAG_PREFER_RANDOMNESS)	
						iFlags += ENUM_TO_INT(RESPAWN_QUERY_FLAG_PREFER_ENEMY_AI_FARTHER)
						iFlags += ENUM_TO_INT(RESPAWN_QUERY_FLAG_PREFER_FRIENDLY_AI_CLOSER)							
	//				ENDIF							
				
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] DoRespawnSearchForCoord - iFlags ") NET_PRINT_INT( iFlags ) NET_NL()
					#ENDIF
			
					
					SWITCH PrivateParams.iAreaShape
						CASE SPAWN_AREA_SHAPE_CIRCLE
							IF NETWORK_START_RESPAWN_SEARCH_FOR_PLAYER(PLAYER_ID(), PrivateParams.vSearchCoord, PrivateParams.fSearchRadius, PublicParams.vFacingCoords, INT_TO_ENUM(RESPAWN_QUERY_FLAGS,iFlags))
								g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
								g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_UPDATE_SEARCH
								#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] DoRespawnSearchForCoord - started NETWORK_START_RESPAWN_SEARCH_FOR_PLAYER ") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
								#ENDIF						
							ELSE
								#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] DoRespawnSearchForCoord - starting NETWORK_START_RESPAWN_SEARCH_FOR_PLAYER ") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
								#ENDIF				
							ENDIF					
						BREAK
						CASE SPAWN_AREA_SHAPE_BOX
							ConvertBoxToAngledArea(PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, vAngledCoords1, vAngledCoords2, fAngledWidth)
							IF NETWORK_START_RESPAWN_SEARCH_IN_ANGLED_AREA_FOR_PLAYER(PLAYER_ID(), vAngledCoords1, vAngledCoords2, fAngledWidth, PublicParams.vFacingCoords, INT_TO_ENUM(RESPAWN_QUERY_FLAGS,iFlags))
								g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
								g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_UPDATE_SEARCH
								#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] DoRespawnSearchForCoord - started NETWORK_START_RESPAWN_SEARCH_IN_ANGLED_AREA_FOR_PLAYER (box) ") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
								#ENDIF						
							ELSE
								#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] DoRespawnSearchForCoord - starting NETWORK_START_RESPAWN_SEARCH_IN_ANGLED_AREA_FOR_PLAYER (box) ") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
								#ENDIF				
							ENDIF
						BREAK
						CASE SPAWN_AREA_SHAPE_ANGLED
							IF NETWORK_START_RESPAWN_SEARCH_IN_ANGLED_AREA_FOR_PLAYER(PLAYER_ID(), PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, PrivateParams.fAngledAreaWidth, PublicParams.vFacingCoords, INT_TO_ENUM(RESPAWN_QUERY_FLAGS,iFlags))
								g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
								g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_UPDATE_SEARCH
								#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] DoRespawnSearchForCoord - started NETWORK_START_RESPAWN_SEARCH_IN_ANGLED_AREA_FOR_PLAYER ") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
								#ENDIF						
							ELSE
								#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] DoRespawnSearchForCoord - starting NETWORK_START_RESPAWN_SEARCH_IN_ANGLED_AREA_FOR_PLAYER ") NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) NET_NL()
								#ENDIF				
							ENDIF
						BREAK
					ENDSWITCH
				

				ELSE
								
					IF (PublicParams.bConsiderInteriors)
						iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_MAY_SPAWN_IN_INTERIOR)
					ENDIF
					iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_MAY_SPAWN_IN_EXTERIOR)
					
					IF (PublicParams.bEdgesOnly)
						iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_ONLY_POINTS_AGAINST_EDGES)	
					ENDIF
					
	//				IF (PrivateParams.bForAVehicle)
	//					iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_ALLOW_NOT_NETWORK_SPAWN_CANDIDATE_POLYS)
	//					iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_ALLOW_ROAD_POLYS)
	//				ENDIF
					
					
					g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
					g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_UPDATE_SEARCH
					
					SWITCH PrivateParams.iAreaShape
						CASE SPAWN_AREA_SHAPE_CIRCLE
							SPAWNPOINTS_START_SEARCH(PrivateParams.vSearchCoord, PrivateParams.fSearchRadius, 5.0, INT_TO_ENUM(SPAWNPOINTS_FLAGS, iFlags), SPAWNPOINT_SEARCH_MIN_RADIUS, SPAWNPOINT_SEARCH_FAIL_TIME)
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] DoRespawnSearchForCoord - starting SPAWNPOINTS_START_SEARCH ") 
							NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) 
							NET_PRINT(" PrivateParams.vSearchCoord = ")
							NET_PRINT_VECTOR(PrivateParams.vSearchCoord)
							NET_PRINT(" PrivateParams.fSearchRadius = ")
							NET_PRINT_FLOAT(PrivateParams.fSearchRadius)					
							NET_PRINT(" iFlags = ") NET_PRINT_INT(iFlags) NET_NL()
							#ENDIF
						BREAK
						CASE SPAWN_AREA_SHAPE_BOX
							ConvertBoxToAngledArea(PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, vAngledCoords1, vAngledCoords2, fAngledWidth)
							SPAWNPOINTS_START_SEARCH_IN_ANGLED_AREA(vAngledCoords1, vAngledCoords2, fAngledWidth, INT_TO_ENUM(SPAWNPOINTS_FLAGS, iFlags), SPAWNPOINT_SEARCH_MIN_RADIUS, SPAWNPOINT_SEARCH_FAIL_TIME)
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] DoRespawnSearchForCoord - starting SPAWNPOINTS_START_SEARCH_IN_ANGLED_AREA (box) ") 
							NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) 
							NET_PRINT(" PrivateParams.vAngledAreaPoint1 = ")
							NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint1)
							NET_PRINT(" PrivateParams.vAngledAreaPoint2 = ")
							NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint2)
							NET_PRINT(" PrivateParams.fAngledAreaWidth = ")
							NET_PRINT_FLOAT(PrivateParams.fAngledAreaWidth)
							NET_PRINT(" iFlags = ") NET_PRINT_INT(iFlags)  NET_NL()
							#ENDIF
						BREAK
						CASE SPAWN_AREA_SHAPE_ANGLED
							SPAWNPOINTS_START_SEARCH_IN_ANGLED_AREA(PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, PrivateParams.fAngledAreaWidth, INT_TO_ENUM(SPAWNPOINTS_FLAGS, iFlags), SPAWNPOINT_SEARCH_MIN_RADIUS, SPAWNPOINT_SEARCH_FAIL_TIME)
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] DoRespawnSearchForCoord - starting SPAWNPOINTS_START_SEARCH_IN_ANGLED_AREA ") 
							NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) 
							NET_PRINT(" PrivateParams.vAngledAreaPoint1 = ")
							NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint1)
							NET_PRINT(" PrivateParams.vAngledAreaPoint2 = ")
							NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint2)
							NET_PRINT(" PrivateParams.fAngledAreaWidth = ")
							NET_PRINT_FLOAT(PrivateParams.fAngledAreaWidth)
							NET_PRINT(" iFlags = ") NET_PRINT_INT(iFlags)  NET_NL()
							#ENDIF
						BREAK
					ENDSWITCH					
					

				ENDIF
			
			ENDIF
			
		ENDIF
		
		// update the search
		IF (g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_UPDATE_SEARCH)
				
			#IF IS_DEBUG_BUILD
				IF (GET_NUM_NAVMESHES_EXISTING_IN_AREA(vNavSearchMin, vNavSearchMax) > 0)
					IF NOT IS_NAVMESH_LOADED_IN_AREA(vNavSearchMin, vNavSearchMax)					
						NET_PRINT("RESPAWN_SEARCH_UPDATE_SEARCH - navmesh not loaded, probably due to streaming issues.") NET_NL()
						//SCRIPT_ASSERT("RESPAWN_SEARCH_UPDATE_SEARCH - navmesh not loaded, why?")					
					ENDIF
				ENDIF
			#ENDIF

					
			IF GetSpawnPointFromRespawnSearch(SpawnResults, PrivateParams, PublicParams, FALSE)
				
				IF (g_SpawnData.SearchTempData.Data.bIsFallbackSearch)
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] DoRespawnSearchForCoord - RESPAWN_SEARCH_UPDATE_SEARCH - failed to find point for player using network spawn candidates, doing fallback search") NET_NL()
					#ENDIF
					g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
					g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_START_FALLBACK_SEARCH
				ELSE
				
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] DoRespawnSearchForCoord - RESPAWN_SEARCH_UPDATE_SEARCH - success! - going to RESPAWN_RETURNING_TRUE") NET_NL()
					#ENDIF
					g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
					
					IF NOT (PrivateParams.bIsForLocalPlayerSpawning)
						IF NOT (VMAG(PublicParams.vFacingCoords) = 0.0)
							vec = PublicParams.vFacingCoords - SpawnResults.vCoords[0]
							SpawnResults.fHeading[0] = GET_HEADING_FROM_VECTOR_2D(vec.x, vec.y)
						ENDIF	
					ENDIF
					
					g_SpawnData.iRespawnSearch_State = RESPAWN_RETURNING_TRUE	
					
				ENDIF

			ELSE
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) > 20000)
					
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] DoRespawnSearchForCoord - RESPAWN_SEARCH_UPDATE_SEARCH - timed out!! - streaming problems? - going to RESPAWN_SEARCH_TIMEOUT_FALLBACK") NET_NL()
					#ENDIF
					//SCRIPT_ASSERT("RESPAWN_SEARCH_UPDATE_SEARCH - DoRespawnSearchForCoord - RESPAWN_SEARCH_UPDATE_SEARCH - timed out!")
					
					NETWORK_CANCEL_RESPAWN_SEARCH()	
					SPAWNPOINTS_CANCEL_SEARCH()	
					
					g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
					g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_TIMEOUT_FALLBACK

				ENDIF		
			ENDIF			
		
	
		ENDIF
		
		// start the search
		IF (g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_START_FALLBACK_SEARCH)

			// reset the flags
			iFlags = 0
			
			g_SpawnData.SearchTempData.Data.iNumOfResults = 0 // need to rest this to 0 as search is done over a few frames			

			IF (PublicParams.bConsiderInteriors)
				iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_MAY_SPAWN_IN_INTERIOR)
			ELSE
				// if this is a small search area and the centre point is inside an interior then consider interior polys
				IF (PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE)
					IF NOT IS_SPAWN_RADIUS_LARGE(PrivateParams.fSearchRadius)
						IF IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vCentre))	
							PRINTLN("[spawning] DoRespawnSearchForCoord - RESPAWN_SEARCH_START_FALLBACK_SEARCH - considering interior as centre of spawn sphere is small and inside")
							iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_MAY_SPAWN_IN_INTERIOR)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_SPAWN_ANGLED_AREA_LARGE(PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, PrivateParams.fAngledAreaWidth)
						IF IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vCentre))	
							PRINTLN("[spawning] DoRespawnSearchForCoord - RESPAWN_SEARCH_START_FALLBACK_SEARCH - considering interior as centre of spawn angled area is small and inside")
							iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_MAY_SPAWN_IN_INTERIOR)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_MAY_SPAWN_IN_EXTERIOR)
			
			IF (PublicParams.bEdgesOnly)
				iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_ONLY_POINTS_AGAINST_EDGES)	
			ENDIF
			
			//iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_ALLOW_NOT_NETWORK_SPAWN_CANDIDATE_POLYS)
			
			IF (PrivateParams.bForAVehicle)			
				iFlags += ENUM_TO_INT(SPAWNPOINTS_FLAG_ALLOW_ROAD_POLYS)
			ENDIF
						
			g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
			g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_UPDATE_FALLBACK_SEARCH
			
			SWITCH PrivateParams.iAreaShape
				CASE SPAWN_AREA_SHAPE_CIRCLE
					SPAWNPOINTS_START_SEARCH(PrivateParams.vSearchCoord, PrivateParams.fSearchRadius, 5.0, INT_TO_ENUM(SPAWNPOINTS_FLAGS, iFlags), SPAWNPOINT_SEARCH_MIN_RADIUS, SPAWNPOINT_SEARCH_FAIL_TIME)
					
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] DoRespawnSearchForCoord - starting RESPAWN_SEARCH_START_FALLBACK_SEARCH ") 
					NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) 
					NET_PRINT(" PrivateParams.vSearchCoord = ")
					NET_PRINT_VECTOR(PrivateParams.vSearchCoord)
					NET_PRINT(" PrivateParams.fSearchRadius = ")
					NET_PRINT_FLOAT(PrivateParams.fSearchRadius)					
					NET_PRINT(" iFlags = ") NET_PRINT_INT(iFlags) NET_NL()
					#ENDIF
				BREAK
				CASE SPAWN_AREA_SHAPE_BOX
					ConvertBoxToAngledArea(PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, vAngledCoords1, vAngledCoords2, fAngledWidth)
					SPAWNPOINTS_START_SEARCH_IN_ANGLED_AREA(vAngledCoords1, vAngledCoords2, fAngledWidth, INT_TO_ENUM(SPAWNPOINTS_FLAGS, iFlags), SPAWNPOINT_SEARCH_MIN_RADIUS, SPAWNPOINT_SEARCH_FAIL_TIME)

					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] DoRespawnSearchForCoord - starting RESPAWN_SEARCH_START_FALLBACK_SEARCH (box)") 
					NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) 
					NET_PRINT(" PrivateParams.vAngledAreaPoint1 = ")
					NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint1)
					NET_PRINT(" PrivateParams.vAngledAreaPoint2 = ")
					NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint2)
					NET_PRINT(" PrivateParams.fAngledAreaWidth = ")
					NET_PRINT_FLOAT(PrivateParams.fAngledAreaWidth)
					NET_PRINT(" iFlags = ") NET_PRINT_INT(iFlags)  NET_NL()
					#ENDIF	
				BREAK
				CASE SPAWN_AREA_SHAPE_ANGLED
					SPAWNPOINTS_START_SEARCH_IN_ANGLED_AREA(PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, PrivateParams.fAngledAreaWidth, INT_TO_ENUM(SPAWNPOINTS_FLAGS, iFlags), SPAWNPOINT_SEARCH_MIN_RADIUS, SPAWNPOINT_SEARCH_FAIL_TIME)

					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] DoRespawnSearchForCoord - starting RESPAWN_SEARCH_START_FALLBACK_SEARCH ") 
					NET_PRINT_INT( GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) ) 
					NET_PRINT(" PrivateParams.vAngledAreaPoint1 = ")
					NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint1)
					NET_PRINT(" PrivateParams.vAngledAreaPoint2 = ")
					NET_PRINT_VECTOR(PrivateParams.vAngledAreaPoint2)
					NET_PRINT(" PrivateParams.fAngledAreaWidth = ")
					NET_PRINT_FLOAT(PrivateParams.fAngledAreaWidth)
					NET_PRINT(" iFlags = ") NET_PRINT_INT(iFlags)  NET_NL()
					#ENDIF	
				BREAK
			ENDSWITCH
			
			
		ENDIF
		
		// update the search
		IF (g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_UPDATE_FALLBACK_SEARCH)
					
			#IF IS_DEBUG_BUILD
				IF (GET_NUM_NAVMESHES_EXISTING_IN_AREA(vNavSearchMin, vNavSearchMax) > 0)
					IF NOT IS_NAVMESH_LOADED_IN_AREA(vNavSearchMin, vNavSearchMax)					
						SCRIPT_ASSERT("RESPAWN_SEARCH_UPDATE_FALLBACK_SEARCH - navmesh not loaded, why?")
					ENDIF
				ENDIF
			#ENDIF					
					
			IF GetSpawnPointFromRespawnSearch(SpawnResults, PrivateParams, PublicParams, TRUE)
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] DoRespawnSearchForCoord - RESPAWN_SEARCH_UPDATE_FALLBACK_SEARCH - success! - going to RESPAWN_RETURNING_TRUE") NET_NL()
				#ENDIF
															
				// check point returned is valid
				IF VMAG(SpawnResults.vCoords[0]) = 0.0					
					
					// if not valid try the last coords
					REPEAT MAX_NUM_OF_STORED_SPAWNSEARCH_RESULTS i
						IF VMAG(SpawnResults.vCoords[0]) = 0.0
							IF NOT (VMAG(g_SpawnData.vSpawnSearchResults[i]) = 0.0)
								
								SWITCH PrivateParams.iAreaShape
									CASE SPAWN_AREA_SHAPE_CIRCLE
										IF IsPointInsideSphere(g_SpawnData.vSpawnSearchResults[i], PrivateParams.vSearchCoord, PrivateParams.fSearchRadius, FALSE, FALSE)
											SpawnResults.vCoords[0] = g_SpawnData.vSpawnSearchResults[i]
											NET_PRINT("[spawning] DoRespawnSearchForCoord - fallback search was 0.0, using a valid previous point instead ") NET_PRINT_VECTOR(g_SpawnData.vSpawnSearchResults[i]) NET_NL()											
										ENDIF
									BREAK
									CASE SPAWN_AREA_SHAPE_BOX
										IF IsPointInsideBox(g_SpawnData.vSpawnSearchResults[i], PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, FALSE, FALSE)
											SpawnResults.vCoords[0] = g_SpawnData.vSpawnSearchResults[i]
											NET_PRINT("[spawning] DoRespawnSearchForCoord - fallback search was 0.0, using a valid previous point instead (box) ") NET_PRINT_VECTOR(g_SpawnData.vSpawnSearchResults[i]) NET_NL()											
										ENDIF
									BREAK
									CASE SPAWN_AREA_SHAPE_ANGLED
										IF IS_POINT_IN_ANGLED_AREA(g_SpawnData.vSpawnSearchResults[i], PrivateParams.vAngledAreaPoint1, PrivateParams.vAngledAreaPoint2, PrivateParams.fAngledAreaWidth)
											SpawnResults.vCoords[0] = g_SpawnData.vSpawnSearchResults[i]
											NET_PRINT("[spawning] DoRespawnSearchForCoord - fallback search was 0.0, using a valid previous point instead (angled) ") NET_PRINT_VECTOR(g_SpawnData.vSpawnSearchResults[i]) NET_NL()  
										ENDIF							
									BREAK
								ENDSWITCH
						
							ENDIF
						ENDIF
					ENDREPEAT	
					
					// if still not valid try getting a safe point
					IF VMAG(SpawnResults.vCoords[0]) = 0.0	
					
					
						SWITCH PrivateParams.iAreaShape
							CASE SPAWN_AREA_SHAPE_CIRCLE
								vec = PrivateParams.vSearchCoord
							BREAK
							CASE SPAWN_AREA_SHAPE_BOX
							CASE SPAWN_AREA_SHAPE_ANGLED
								vec = (PrivateParams.vAngledAreaPoint1 + PrivateParams.vAngledAreaPoint2)/2.0
							BREAK
						ENDSWITCH	
												
						GetSafeCoord(vec, FALSE, TRUE, TRUE, FALSE, PrivateParams, PublicParams)
						
						SpawnResults.vCoords[0] = vec
						NET_PRINT("[spawning] DoRespawnSearchForCoord - fallback search was 0.0, using GetSafeCoord ") NET_PRINT_VECTOR(SpawnResults.vCoords[0]) NET_NL()  
											
						
					ENDIF					
					
				ENDIF								
				
				g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
				
				// if it we were supposed to face a direction do it here because the search did not use it. 
				IF NOT (VMAG(PublicParams.vFacingCoords) = 0.0)
					REPEAT NUM_OF_STORED_SPAWN_RESULTS i 
						vec = PublicParams.vFacingCoords - SpawnResults.vCoords[i]
						SpawnResults.fHeading[i] = GET_HEADING_FROM_VECTOR_2D(vec.x, vec.y)
					ENDREPEAT
				ENDIF	
				
				g_SpawnData.iRespawnSearch_State = RESPAWN_RETURNING_TRUE	
			ELSE
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iRespawnSearch_Time) > 20000)
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] DoRespawnSearchForCoord - RESPAWN_SEARCH_UPDATE_FALLBACK_SEARCH - timed out!! - streaming problems? - using fallback method and going to RESPAWN_SEARCH_TIMEOUT_FALLBACK") NET_NL()
					#ENDIF
					
					//SCRIPT_ASSERT("RESPAWN_SEARCH_UPDATE_FALLBACK_SEARCH - DoRespawnSearchForCoord - RESPAWN_SEARCH_UPDATE_SEARCH - timed out!")
																				
					g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
					g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_TIMEOUT_FALLBACK	
				ENDIF		
			ENDIF			
		
	
		ENDIF		
		
		IF (g_SpawnData.iRespawnSearch_State = RESPAWN_SEARCH_TIMEOUT_FALLBACK)
		
			IF (PrivateParams.bIsForLocalPlayerSpawning)
				IF IS_SPAWN_LOCATION_MISSION_AREA_CHECK(g_SpawnData.SpawnLocation) 				
					#IF IS_DEBUG_BUILD	
						REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i
							IF (g_SpawnData.MissionSpawnDetails.SpawnArea[i].bIsActive)						
								NET_PRINT("[spawning] DoRespawnSearchForCoord - RESPAWN_SEARCH_UPDATE_FALLBACK_SEARCH - fallback, spArea ") NET_PRINT_INT(i) NET_PRINT(":") NET_NL()
								DEBUG_PRINT_SPAWN_AREA_DETAILS(g_SpawnData.MissionSpawnDetails.SpawnArea[i])							
							ENDIF
						ENDREPEAT
					#ENDIF																		
				ENDIF
			ELSE
				IF (g_SpawnData.SearchTempData.Data.bGotFallback)							
					ConvertToSpawnResultStruct(SpawnResults, g_SpawnData.SearchTempData.TopResults)
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] DoRespawnSearchForCoord - RESPAWN_SEARCH_UPDATE_FALLBACK_SEARCH - SpawnData.SearchTempData.Data.bGotFallback = TRUE at ") PrintSpawnResultStruct(SpawnResults)  NET_NL()
					#ENDIF
				ELSE
					BOOL bConsiderInactiveNodes
					IF (PrivateParams.bForAVehicle)
						bConsiderInactiveNodes = FALSE
					ELSE
						bConsiderInactiveNodes = TRUE
					ENDIF
					SpawnResults.vCoords[0] = g_SpawnData.vRespawnSearch_CurrentSearch
					//GetSafeCoord(SpawnResults.vCoords[0], PublicParams.fMinDistFromPlayer, DEFAULT, PrivateParams.bForAVehicle, bConsiderInactiveNodes, DEFAULT, PublicParams.bConsiderInteriors)
					GetSafeCoord(SpawnResults.vCoords[0], FALSE, bConsiderInactiveNodes, FALSE, FALSE, PrivateParams, PublicParams)
				ENDIF
			ENDIF	
													
			g_SpawnData.iRespawnSearch_Time = GET_NETWORK_TIME()
			g_SpawnData.iRespawnSearch_State = RESPAWN_RETURNING_TRUE		
		
		ENDIF
		
		
		// cleanup and return true
		IF (g_SpawnData.iRespawnSearch_State = RESPAWN_RETURNING_TRUE)
		
			g_SpawnData.bRespawnSearch_HasStarted = FALSE
		
			NETWORK_CANCEL_RESPAWN_SEARCH()	
			SPAWNPOINTS_CANCEL_SEARCH()
			RemoveNavmeshForSpawnSearch()
		
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] DoRespawnSearchForCoord - RESPAWN_RETURNING_TRUE - SpawnResults = ") PrintSpawnResultStruct(SpawnResults)  NET_NL()
			#ENDIF			
			
			RETURN(TRUE)
		ENDIF
	
	
		// set the update timer
		g_SpawnData.iRespawnSearch_LastUpdateTime = GET_NETWORK_TIME()
	ENDIF
	
	RETURN(FALSE)
	
ENDFUNC


//PROC GetSuitableSafeSpawnLocation(VECTOR &vCoord, FLOAT &fHeading)
//
//	VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
//	
//	// calculate position
//	IF IS_THIS_MODEL_A_PLANE(g_SpawnData.MissionSpawnDetails.SpawnModel)
//	OR IS_THIS_MODEL_A_HELI(g_SpawnData.MissionSpawnDetails.SpawnModel)
//		vCoord = GetNearestSafeSpawnAir(vPlayerPos, g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint)			
//	ELIF IS_THIS_MODEL_A_BOAT(g_SpawnData.MissionSpawnDetails.SpawnModel)
//		vCoord = GetNearestSafeSpawnSea(vPlayerPos, g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint)	
//	ENDIF
//	
//	// calculate the z
//	IF IS_THIS_MODEL_A_PLANE(g_SpawnData.MissionSpawnDetails.SpawnModel)
//	OR IS_THIS_MODEL_A_HELI(g_SpawnData.MissionSpawnDetails.SpawnModel)	
//		FLOAT fDistPrevious
//		FLOAT fDistToNextCheckpoint
//		FLOAT fPercentageComplete							
//		fDistPrevious = VDIST(vPlayerPos, g_SpecificSpawnLocation.vCoords)														
//		fDistToNextCheckpoint = VDIST(vPlayerPos, g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint)
//		fPercentageComplete = fDistPrevious /(fDistPrevious + fDistToNextCheckpoint)
//		
//		FLOAT fNewZ							
//		fNewZ =  g_SpecificSpawnLocation.vCoords.z + ( (vCoord.z - g_SpecificSpawnLocation.vCoords.z ) * fPercentageComplete)
//		IF (fNewZ < vCoord.z)
//			fNewZ = vCoord.z
//		ENDIF							
//		vCoord.z = fNewZ
//	ENDIF
//	
//	// calculate the heading
//	VECTOR vec
//	vec = g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint	- vCoord
//	fHeading = GET_HEADING_FROM_VECTOR_2D(vec.x, vec.y)
//		
//	
//	NET_PRINT("[spawning] GetSuitableSafeSpawnLocation() - vNextRaceCheckpoint = ") NET_PRINT_VECTOR(g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint) NET_NL()
//	NET_PRINT("[spawning] GetSuitableSafeSpawnLocation() - PrivateParams.vSearchCoord = ") NET_PRINT_VECTOR(vCoord) NET_NL()
//		
//
//	// check that then new coords are not further than the last checkpoint
//	IF VDIST(vCoord, g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint) > VDIST(g_SpecificSpawnLocation.vCoords, g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint)											
//		NET_PRINT("[spawning] GetSuitableSafeSpawnLocation() - safe coords are no nearer than last checkpoint, using last checkpoint instead") NET_NL()
//		vCoord = 	g_SpecificSpawnLocation.vCoords
//		fHeading =  g_SpecificSpawnLocation.fHeading
//	ENDIF
//																	
//ENDPROC

PROC GetSpawnForProperty(INT iProperty, VECTOR &vPos, FLOAT &fHeading)
	NET_PRINT("[spawning] GetSpawnForProperty - called with property ") NET_PRINT_INT(iProperty) NET_NL()
	
//	#IF FEATURE_GEN9_EXCLUSIVE
//	MP_PROP_OFFSET_STRUCT tempStruct
//	IF IS_HEIST_DEEPLINK_IN_PROGRESS()
//		GET_POSITION_AS_OFFSET_FOR_PROPERTY(iProperty,MP_PROP_ELEMENT_PLANNING_BOARD_CUT_POS_PLANNING_BOARD,tempStruct)
//		vPos = tempStruct.vLoc
//		fHeading = tempStruct.vRot.z
//		PRINTLN("[spawning] GetSpawnForProperty bypassing coords to spawn in heist planning room for deeplink")
//		EXIT
//	ENDIF
//	#ENDIF
//	
	
	HOUSE_INTERIOR_LOCATIONS SpawnPos[NUMBER_OF_SPAWN_LOCATIONS_INSIDE_PROPERTIES]
	GET_HOUSE_RESPAWN_COORDS(iProperty, SpawnPos)
	
	#IF IS_DEBUG_BUILD
		INT i 
		REPEAT NUMBER_OF_SPAWN_LOCATIONS_INSIDE_PROPERTIES i
			NET_PRINT("[spawning] GetSpawnForProperty - coords ") NET_PRINT_INT(i) NET_PRINT(" = ") NET_PRINT_VECTOR(SpawnPos[i].vPlayerLoc) NET_NL()
			IF (VMAG(SpawnPos[i].vPlayerLoc) = 0.0)
				SCRIPT_ASSERT("GetSpawnForProperty - GET_HOUSE_RESPAWN_COORDS returning <<0.0, 0.0, 0.0>> for a property, bug Conor.")
			ENDIF
		ENDREPEAT
	#ENDIF
	
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, NUMBER_OF_SPAWN_LOCATIONS_INSIDE_PROPERTIES)
	vPos = SpawnPos[iRand].vPlayerLoc
	fHeading = SpawnPos[iRand].fPlayerHeading
ENDPROC

PROC GetSpawnForYachtExterior(INT iYacht, VECTOR &vPos, FLOAT &fHeading)
	
	NET_PRINT("[spawning] GetSpawnForYachtExterior - called with yacht ") NET_PRINT_INT(iYacht) NET_NL()
	
	YACHT_SPAWN_LOCATIONS SpawnPos[NUMBER_OF_SPAWN_LOCATIONS_ON_YACHT_EXTERIOR]
	GET_SPAWN_LOCATIONS_FOR_YACHT(iYacht, SpawnPos, FALSE)
	
	#IF IS_DEBUG_BUILD
		INT i 
		REPEAT NUMBER_OF_SPAWN_LOCATIONS_ON_YACHT_EXTERIOR i
			NET_PRINT("[spawning] GetSpawnForYachtExterior - coords ") NET_PRINT_INT(i) NET_PRINT(" = ") NET_PRINT_VECTOR(SpawnPos[i].vPlayerLoc) NET_NL()
			IF (VMAG(SpawnPos[i].vPlayerLoc) = 0.0)
				SCRIPT_ASSERT("GetSpawnForYachtExterior - GET_SPAWN_LOCATIONS_FOR_YACHT returning <<0.0, 0.0, 0.0>> for a property, bug neil.")
			ENDIF
		ENDREPEAT
	#ENDIF	
	
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, NUMBER_OF_SPAWN_LOCATIONS_ON_YACHT_EXTERIOR)
	vPos = SpawnPos[iRand].vPlayerLoc
	fHeading = SpawnPos[iRand].fPlayerHeading	
	
ENDPROC

PROC GetSpawnForYachtApartment(INT iYacht, VECTOR &vPos, FLOAT &fHeading)
	
	NET_PRINT("[spawning] GetSpawnForYachtApartment - called with yacht ") NET_PRINT_INT(iYacht) NET_NL()
	
	YACHT_SPAWN_LOCATIONS SpawnPos[NUMBER_OF_SPAWN_LOCATIONS_YACHT_APARTMENT]
	GET_SPAWN_LOCATIONS_FOR_YACHT(iYacht, SpawnPos, TRUE)
	
	#IF IS_DEBUG_BUILD
		INT i 
		REPEAT NUMBER_OF_SPAWN_LOCATIONS_YACHT_APARTMENT i
			NET_PRINT("[spawning] GetSpawnForYachtApartment - coords ") NET_PRINT_INT(i) NET_PRINT(" = ") NET_PRINT_VECTOR(SpawnPos[i].vPlayerLoc) NET_NL()
			IF (VMAG(SpawnPos[i].vPlayerLoc) = 0.0)
				SCRIPT_ASSERT("GetSpawnForYachtApartment - GET_SPAWN_LOCATIONS_FOR_YACHT returning <<0.0, 0.0, 0.0>> for a property, bug neil.")
			ENDIF
		ENDREPEAT
	#ENDIF	
	
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, NUMBER_OF_SPAWN_LOCATIONS_YACHT_APARTMENT)
	vPos = SpawnPos[iRand].vPlayerLoc
	fHeading = SpawnPos[iRand].fPlayerHeading	
	
ENDPROC



FUNC VECTOR GetClubSpawnPosition(INT iLocation)
	SWITCH iLocation
		CASE 0 RETURN <<-1590.9675, -3015.2151, -77.0050>>	// balcony
		CASE 1 RETURN <<-1587.4396, -3006.7056, -80.0060>>	// bar
		CASE 2 RETURN <<-1591.1267, -3008.5693, -77.0051>>	// balcony
		CASE 3 RETURN <<-1576.5996, -3014.0754, -80.0060>>	// bar
		CASE 4 RETURN <<-1593.4803, -3009.1829, -80.0060>> // dancefloor
	ENDSWITCH
	RETURN <<-1590.9675, -3015.2151, -77.0050>>
ENDFUNC
FUNC FLOAT GetClubSpawnHeading(INT iLocation)
	SWITCH iLocation
		CASE 0 RETURN 72.7551
		CASE 1 RETURN 273.6890
		CASE 2 RETURN 102.4901
		CASE 3 RETURN 176.9972
		CASE 4 RETURN 111.3628
	ENDSWITCH	
	RETURN 72.7551
ENDFUNC



PROC GetSpawnForSimpleInterior(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vSpawnPosition, FLOAT &fSpawnHeading, INT iOverride=-1)
	#IF IS_DEBUG_BUILD
	PRINTLN("[Spawning] GetSpawnForSimpleInterior - ", GET_SIMPLE_INTERIOR_DEBUG_NAME(eSimpleInteriorID))
	#ENDIF
	
	INT iRand
		
	SWITCH GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID)
		CASE SIMPLE_INTERIOR_TYPE_IE_GARAGE
			IF HAS_LOCAL_PLAYER_VIEWED_IE_WAREHOUSE_INTRO_CUTSCENE()
				vSpawnPosition 	= <<959.5004, -3005.1846, -40.6349>>
				fSpawnHeading 	= 300.3960
			ELSE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn in IE Warehouse but interior cutscene hasn't been viewed, defaulting spawn to origin.") NET_NL()
				#ENDIF
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_TYPE_BUNKER
			IF IS_PLAYER_BUNKER_SAVEBED_PURCHASED(PLAYER_ID()) 
				vSpawnPosition 	= <<905.4745, -3200.6997, -98.2548>>
				fSpawnHeading 	= 188.8198
			ELSE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn in bunker but bed has not been purchased, spawning at the entrance point instead.") NET_NL()
				#ENDIF			
				vSpawnPosition 	= <<886.6, -3244.7, -98.3>>
				fSpawnHeading 	= 85.45
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_TYPE_HANGAR
			IF IS_HANGAR_PERSONAL_QUARTERS_PURCHASED()
			AND HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Hangar_Intro_Cutscene)
				vSpawnPosition 	= <<-1237.2008, -2984.7302, -42.2636>>
				fSpawnHeading 	= 216.4375 
			ELSE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn in hangar but bed has not been purchased, defaulting spawn to the origin.") NET_NL()
				#ENDIF
			ENDIF
		BREAK
		CASE SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE
			IF HAS_LOCAL_PLAYER_PURCHASED_DEFUNCT_BASE_PERSONAL_QUARTERS()
			AND HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_Base_Intro_Cutscene)
				VECTOR vSpawnRotation
				GET_INTERIOR_BED_SPAWN_ACTIVITY_COORDS(SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE, vSpawnPosition, vSpawnRotation)
				fSpawnHeading = vSpawnRotation.z
			ELSE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn in defunct base but bed has not been purchased, defaulting spawn to the origin.") NET_NL()
				#ENDIF
			ENDIF
		BREAK
		
		CASE SIMPLE_INTERIOR_TYPE_BUSINESS_HUB
			IF HAS_LOCAL_PLAYER_COMPLETED_NIGHTCLUB_DJ_COLLECTION()
			#IF FEATURE_GEN9_EXCLUSIVE
			OR SHOULD_SPAWN_IN_PROPERTY_FOR_MP_INTRO()
			#ENDIF
				IF (iOverride = -1)
					iRand = GET_RANDOM_INT_IN_RANGE(0, 5)
				ELSE
					iRand = iOverride
				ENDIF
				
				vSpawnPosition = GetClubSpawnPosition(iRand)
				fSpawnHeading = GetClubSpawnHeading(iRand)

			ELSE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn in nightclub but not set up, defaulting spawn to the origin.") NET_NL()
				#ENDIF
			ENDIF
		BREAK		
		
		CASE SIMPLE_INTERIOR_TYPE_ARENA_GARAGE
			IF HAS_LOCAL_PLAYER_PURCHASED_ARENA_GARAGE_PERSONAL_QUARTERS()			
				vSpawnPosition = <<205.0234, 5164.4077, -86.5974>>
				fSpawnHeading =  358.1009
			ELSE
				IF (g_TransitionSessionNonResetVars.bFinishedFirstArenaEvent)
					vSpawnPosition = <<215.3005, 5195.0698, -89.6001>>
					fSpawnHeading =  168.5053
				ELSE			
					#IF IS_DEBUG_BUILD
					NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn in arena garage, defaulting spawn to the origin.") NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SIMPLE_INTERIOR_TYPE_CASINO_APARTMENT
			vSpawnPosition = <<973.911522, 80.211236, 115.1642>>
			fSpawnHeading = 320.4113
		BREAK
		
		CASE SIMPLE_INTERIOR_TYPE_ARCADE
			#IF FEATURE_GEN9_EXCLUSIVE
			IF IS_HEIST_DEEPLINK_IN_PROGRESS()
				vSpawnPosition = <<2712.2998, -367.3469, -55.7809>>
				fSpawnHeading = 61.4249
			ELSE
			#ENDIF
				vSpawnPosition = <<2723.1299, -369.1836, -56.3809>>
				fSpawnHeading = 61.4249
			#IF FEATURE_GEN9_EXCLUSIVE
			ENDIF
			#ENDIF
		BREAK
		
		CASE SIMPLE_INTERIOR_TYPE_SUBMARINE // 6321837
			vSpawnPosition = <<1558.369, 384.953, -53.8531>>
			fSpawnHeading = 0.0
		BREAK
		
		CASE SIMPLE_INTERIOR_TYPE_CAR_MEET
			vSpawnPosition = <<-2220.2, 1158.7, -23.3>>
			fSpawnHeading = 177.9279
		BREAK
		CASE SIMPLE_INTERIOR_TYPE_AUTO_SHOP
			IF HAS_PLAYER_PURCHASED_AUTO_SHOP_PERSONAL_QUARTERS(PLAYER_ID())	
				vSpawnPosition = <<-1356.3967, 162.3236, -100.1943>>
				fSpawnHeading = 189.3496
			ELSE
			#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn in AUTO SHOP but bed has not been purchased, defaulting spawn to the origin.") NET_NL()
			#ENDIF
			ENDIF					
		BREAK
		#IF FEATURE_FIXER
		CASE SIMPLE_INTERIOR_TYPE_FIXER_HQ
			IF HAS_PLAYER_PURCHASED_FIXER_HQ_UPGRADE_PERSONAL_QUARTERS(PLAYER_ID())	
			AND HAS_LOCAL_PLAYER_VIEWED_FIXER_HQ_INTRO_CUTSCENE()
				SWITCH GET_PLAYERS_OWNED_FIXER_HQ(PLAYER_ID())
					CASE FIXER_HQ_HAWICK
						vSpawnPosition = << 387.949, -69.676, 112 >>
						fSpawnHeading = 26.279999
					BREAK
					CASE FIXER_HQ_ROCKFORD
						vSpawnPosition = << -1011.61, -431.12, 72.4981 >>
						fSpawnHeading = 73.229996
					BREAK
					CASE FIXER_HQ_SEOUL
						vSpawnPosition = << -595.463, -709.173, 121.642 >>
						fSpawnHeading = 226.130005
					BREAK
					CASE FIXER_HQ_VESPUCCI
						vSpawnPosition = << -996.574, -753.597, 70.5312 >>
						fSpawnHeading = 136.279999
					BREAK
				ENDSWITCH
			ELSE
			#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn in FIXER HQ but bed has not been purchased, defaulting spawn to the origin.") NET_NL()
			#ENDIF
			ENDIF	
		BREAK
		#ENDIF
		
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE SIMPLE_INTERIOR_TYPE_FACTORY
			
			SWITCH eSimpleInteriorID
			
				CASE SIMPLE_INTERIOR_FACTORY_METH_1
				CASE SIMPLE_INTERIOR_FACTORY_METH_2
				CASE SIMPLE_INTERIOR_FACTORY_METH_3
				CASE SIMPLE_INTERIOR_FACTORY_METH_4
					vSpawnPosition = <<997.9982, -3200.371, -37.3932>>
					fSpawnHeading = 0.0
				BREAK
				CASE SIMPLE_INTERIOR_FACTORY_WEED_1
				CASE SIMPLE_INTERIOR_FACTORY_WEED_2
				CASE SIMPLE_INTERIOR_FACTORY_WEED_3
				CASE SIMPLE_INTERIOR_FACTORY_WEED_4
					vSpawnPosition = <<1065.473, -3183.2926, -40.1635>>
					fSpawnHeading = 90.0
				BREAK
				CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
				CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
				CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
				CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
					vSpawnPosition = <<1088.7948, -3188.2826, -39.9935>>
					fSpawnHeading = 180.0	
				BREAK
				CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
				CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
				CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
				CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
					vSpawnPosition = <<-1356.3967, 162.3236, -100.1943>>
					fSpawnHeading = 189.3496	
				BREAK
				CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
				CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
				CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
				CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
					vSpawnPosition = <<1173.1375, -3196.559, -40.308>>
					fSpawnHeading = 90.0	
				BREAK
			
			ENDSWITCH

		
		BREAK
		#ENDIF

	ENDSWITCH
ENDPROC

FUNC BOOL IsModelBoatOrSub(MODEL_NAMES ModelName)
	RETURN IS_THIS_MODEL_A_SEA_VEHICLE(ModelName)
//	IF IS_THIS_MODEL_A_BOAT(g_SpawnData.MissionSpawnDetails.SpawnModel)
//	OR (ModelName = SUBMERSIBLE)
//		RETURN(TRUE)
//	ENDIF
//	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_SPECIAL_VEHICLE_A_BOAT(MODEL_NAMES eModel)
	IF eModel = TECHNICAL2
	OR eModel = BLAZER5
	OR eModel = SEASHARK
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL ShouldOnlyConsiderBoatNodes()
	IF IS_SPAWNING_IN_VEHICLE()
		IF NOT (g_SpawnData.MissionSpawnDetails.SpawnModel = DUMMY_MODEL_FOR_SCRIPT)
			IF IsModelBoatOrSub(g_SpawnData.MissionSpawnDetails.SpawnModel)
				NET_PRINT("[spawning] ShouldOnlyConsiderBoatNodes() - returning TRUE") NET_NL()
				RETURN(TRUE)	
			ENDIF
		ENDIF
	ENDIF
	//NET_PRINT("[spawning] ShouldOnlyConsiderBoatNodes() - returning FALSE")
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsSpawningInPlaneOrHeli()

	// neilf : added for url:bugstar:3752345 - Steal - Bomb Base - When mission was launched by remote player local player spawned falling through the air with an unresponsive parachute and fell to their death.
	// so if we are falling back using custom spawn point and we are not already in a plane or heli then return false.
	IF (g_SpawnData.bHasAttemptedFallbackUsingCustomPoints)
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			PRINTLN("IsSpawningInPlaneOrHeli - returning FALSE, custom spawn point fallback and not in vehicle.")
			RETURN(FALSE)
		ELSE
			VEHICLE_INDEX VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF NOT DOES_ENTITY_EXIST(VehicleID)
				PRINTLN("IsSpawningInPlaneOrHeli - returning FALSE, custom spawn point fallback and vehicle does not exist.")
				RETURN(FALSE)
			ELSE
				IF NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(VehicleID))
				AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(VehicleID))
					PRINTLN("IsSpawningInPlaneOrHeli - returning FALSE, custom spawn point fallback and vehicle is not a plane or heli.")
					RETURN(FALSE)
				ENDIF	
			ENDIF
		ENDIF
	ENDIF


	IF IS_SPAWNING_IN_VEHICLE()
	AND (IS_THIS_MODEL_A_PLANE(g_SpawnData.MissionSpawnDetails.SpawnModel) OR IS_THIS_MODEL_A_HELI(g_SpawnData.MissionSpawnDetails.SpawnModel))	
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC StartSearch()
	// start the search
	
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] GetSpawnLocationCoords - StartSearch - raw spawn coords = ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_PRINT(", raw heading = ") NET_PRINT_FLOAT(g_SpawnData.PrivateParams.fRawHeading) NET_NL()		
	#ENDIF

	// refine the spawn point				

	IF NOT (g_SpawnData.bIgnoreExclusionCheck)	
	
	
		// move point outside any global exclusion zone		
		IF DO_GLOBAL_EXCLUSION_ZONES_APPLY(g_SpawnData.PrivateParams.vSearchCoord)
			IF IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(g_SpawnData.PrivateParams.vSearchCoord, TRUE)
				#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnLocationCoords - StartSearch - point was inside global exclusion area so moving outside, new vRawCoords = ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
				#ENDIF
				g_SpawnData.PublicParams.bConsiderOriginAsValidPoint = FALSE
			ENDIF
			// if raw coords have 0.0 as z then check they are not under a global exclusion zone. see 1873725
			IF (g_SpawnData.PrivateParams.vSearchCoord.z <= 0.0)
				IF IS_POINT_UNDER_GLOBAL_EXCLUSION_ZONE(g_SpawnData.PrivateParams.vSearchCoord, TRUE)
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] GetSpawnLocationCoords - StartSearch - point was under global exclusion area so moving outside, new vRawCoords = ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
					#ENDIF
					g_SpawnData.PublicParams.bConsiderOriginAsValidPoint = FALSE
				ENDIF
			ENDIF
		ENDIF

		
		// move point outside of any mission exclusion zone
		IF IS_POINT_IN_MISSION_EXCLUSION_ZONE(g_SpawnData.PrivateParams.vSearchCoord, TRUE)
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - StartSearch - point was inside mission exclusion area so moving outside, new vRawCoords = ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
			#ENDIF	
			g_SpawnData.PublicParams.bConsiderOriginAsValidPoint = FALSE
		ENDIF
	
		// move point outside of near exclusion zone
		IF IS_POINT_IN_NEAR_EXCLUSION_RADIUS(g_SpawnData.PrivateParams.vSearchCoord, TRUE)
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - StartSearch - point was inside near exclusion area so moving outside, new vRawCoords = ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
			#ENDIF	
			g_SpawnData.PublicParams.bConsiderOriginAsValidPoint = FALSE
		ENDIF	
											
	ENDIF										

	// debug output
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] GetSpawnLocationCoords - StartSearch - refined spawn coords = ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
	#ENDIF

	IF IsSpawningInPlaneOrHeli()
		
		g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_FIND_AIR_SPAWN
		NET_PRINT_FRAME() NET_PRINT("[spawning] GetSpawnLocationCoords - StartSearch - going to SPAWN_LOCATION_STATE_FIND_AIR_SPAWN.") NET_NL()
	
	ELSE

		g_SpawnData.bDoShapeTest = TRUE
		NET_PRINT_FRAME() NET_PRINT("[spawning] GetSpawnLocationCoords - StartSearch - g_SpawnData.bDoShapeTest set to TRUE.") NET_NL()
		
		// start the respawn search
		g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_DO_NEW_RESPAWN_SEARCH
		NET_PRINT_FRAME() NET_PRINT("[spawning] GetSpawnLocationCoords - StartSearch - going to SPAWN_LOCATION_STATE_DO_NEW_RESPAWN_SEARCH.") NET_NL()
		
	ENDIF
ENDPROC


FUNC VECTOR GetAverageEnemyPositionInCircle(VECTOR vCentre, FLOAT fRadius)
	INT i
	PLAYER_INDEX PlayerID
	INT iCount
	VECTOR vCoords
	VECTOR vTotal
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF IsPlayerConsideredEnemy(PlayerID)
			vCoords = GET_PLAYER_COORDS(PlayerID)
			vCoords.z = vCentre.z
			IF (VDIST(vCoords, vCentre) <= fRadius)
				vTotal += GET_PLAYER_COORDS(PlayerID)
				iCount += 1
			ENDIF
		ENDIF
	ENDREPEAT
	IF (iCount > 0)
		vTotal /= TO_FLOAT(iCount)
	ENDIF
	NET_PRINT("[spawning] GetAverageEnemyPositionInCircle - returning ") NET_PRINT_VECTOR(vTotal) 
	NET_PRINT(", found ") NET_PRINT_INT(iCount) NET_PRINT(" players inside radius of ") NET_PRINT_FLOAT(fRadius) NET_PRINT(" from ") NET_PRINT_VECTOR(vCentre) NET_NL()
	RETURN(vTotal)
ENDFUNC



PROC MovePointAwayFromNearbyEnemyPlayers(VECTOR &vCoord, FLOAT fDist)
	INT i
	VECTOR vPos
	VECTOR vec
	VECTOR vAdd = <<0.0, 0.0, 0.0>>
	FLOAT fDiff
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF IsPlayerConsideredEnemy(PlayerID)
			vPos = GET_PLAYER_COORDS(PlayerID)
			fDiff = VDIST(vCoord, vPos)
			IF (fDiff < fDist)
				
				// calculate the dist we we to add 
				fDiff = fDist - fDiff
			
				// get vec
				vec = vCoord - vPos
				vec /= VMAG(vec)
				vec *= fDiff
				
				// add to vAdd
				vAdd += vec	
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[spawning] MovePointAwayFromNearbyEnemyPlayers - vAdd = ", vAdd)
	
	vCoord += vAdd
ENDPROC

FUNC VECTOR GetAveragePositionOfEnemiesNearPoint(VECTOR vCoord, FLOAT fDist)
	INT i
	VECTOR vPos
	VECTOR vTotal
	INT iCount
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF IsPlayerConsideredEnemy(PlayerID)	
			vPos = GET_PLAYER_COORDS(PlayerID)
			IF (VDIST(vCoord, vPos) < fDist)
				vTotal += vPos
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	vTotal /= TO_FLOAT(iCount)
	RETURN(vTotal)
ENDFUNC

FUNC VECTOR GetAverageFacingVectorOfEnemiesNearPoint(VECTOR vCoord, FLOAT fDist)
	INT i
	FLOAT fHeading
	VECTOR vPos
	VECTOR vec
	VECTOR vTotal
	INT iCount
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF IsPlayerConsideredEnemy(PlayerID)	
			vPos = GET_PLAYER_COORDS(PlayerID)
			IF (VDIST(vCoord, vPos) < fDist)
			AND NOT IS_ENTITY_DEAD(GET_PLAYER_PED(PlayerID))
				fHeading = GET_ENTITY_HEADING(GET_PLAYER_PED(PlayerID))
				vec = <<0.0, 1.0, 0.0>>
				RotateVec(vec, <<0.0, 0.0, fHeading>>)
				vTotal += vec
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	vTotal /= TO_FLOAT(iCount)
	RETURN(vTotal)
ENDFUNC

PROC ClearStoredSpawningParams_public()
	SPAWN_SEARCH_PARAMS DefaultPublicParams
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(g_SpawnData.PublicParams, DefaultPublicParams, SIZE_OF(SPAWN_SEARCH_PARAMS))
	ELSE
		g_SpawnData.PublicParams = DefaultPublicParams
	ENDIF	
ENDPROC

PROC ClearStoredSpawningParams_private()
	PRIVATE_SPAWN_SEARCH_PARAMS DefaultPrivateParams
	IF (g_bUseStackCopy)
		COPY_SCRIPT_STRUCT(g_SpawnData.PrivateParams, DefaultPrivateParams, SIZE_OF(PRIVATE_SPAWN_SEARCH_PARAMS))
	ELSE
		g_SpawnData.PrivateParams = DefaultPrivateParams
	ENDIF	
ENDPROC

PROC ClearStoredSpawningParams()
	ClearStoredSpawningParams_public()
	ClearStoredSpawningParams_private()
	PRINTLN("[spawning] ClearStoredSpawningParams called")
ENDPROC

PROC GetSpawnLocationCoords_GotoFinalChecks(SPAWN_RESULTS &SpawnResults, INT &i)

	PRINTLN("[spawning] GetSpawnLocationCoords GotoFinalChecks - going to SPAWN_LOCATION_STATE_FINAL_CHECKS, using car node ", i, SpawnResults.vCoords[i], SpawnResults.fHeading[i])
	INT j
	// if i > 0 then we need to ignore the previous results as the were deemed no good.
	REPEAT NUM_OF_STORED_SPAWN_RESULTS j
		IF ((i+j) < NUM_OF_STORED_SPAWN_RESULTS)
			SpawnResults.vCoords[j] = SpawnResults.vCoords[i+j]
			SpawnResults.fHeading[j] = SpawnResults.fHeading[i+j]
			SpawnResults.iScore[j] = SpawnResults.iScore[i+j]
			PRINTLN("[spawning] GetSpawnLocationCoords GotoFinalChecks - set SpawnResults.vCoords[", j, "] to ", SpawnResults.vCoords[j])
		ELSE
			SpawnResults.vCoords[j] = <<0.0, 0.0, 0.0>>	
			SpawnResults.fHeading[j] = 0.0
			SpawnResults.iScore[j] = 0
			PRINTLN("[spawning] GetSpawnLocationCoords GotoFinalChecks - set SpawnResults.vCoords[", j, "] to 0")
		ENDIF
	ENDREPEAT
	
	g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_FINAL_CHECKS	
	i = NUM_OF_STORED_SPAWN_RESULTS // quit out of this loop

ENDPROC

FUNC BOOL IsPlayerAloneInGang()
	INT iPlayer
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF NOT (PlayerID = PLAYER_ID())
			IF IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE)
				IF GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PlayerID, PLAYER_ID())
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL AreAnyOtherGangMembersAliveOrRespawningInArea(VECTOR vCoords, FLOAT fRadius, VECTOR &vReturn, BOOL bCheckOrderNumber=FALSE)
	INT iPlayer
	PLAYER_INDEX PlayerID
	VECTOR vPlayerCoords
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF NOT (PlayerID = PLAYER_ID())
			IF IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE)
				IF GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PlayerID, PLAYER_ID())
					IF NOT IS_PLAYER_RESPAWNING(PlayerID)
					AND NOT IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PlayerID)
					AND IS_NET_PLAYER_OK(PlayerID, TRUE, TRUE)
						vPlayerCoords = GET_PLAYER_COORDS(PlayerID)	
						PRINTLN("[spawning] AreAnyOtherGangMembersAliveOrRespawningInArea - player ", iPlayer, " is at coords ", vPlayerCoords)
					ELSE
						// check if the player has request coords
						IF (GlobalServerBD.bWarpRequestTimeIntialised[iPlayer])
							IF (bCheckOrderNumber)
								// check this player requested before me
								IF (GlobalServerBD.iWarpRequestOrderNumber[iPlayer] < GlobalServerBD.iWarpRequestOrderNumber[NATIVE_TO_INT(PLAYER_ID())])
									vPlayerCoords = GlobalServerBD.g_vWarpRequest[iPlayer]
									PRINTLN("[spawning] AreAnyOtherGangMembersAliveOrRespawningInArea - player ", iPlayer, " is warping to coords ", vPlayerCoords, " his order num = ", GlobalServerBD.iWarpRequestOrderNumber[iPlayer], " my order num = ", GlobalServerBD.iWarpRequestOrderNumber[NATIVE_TO_INT(PLAYER_ID())])
								ENDIF
							ELSE
								vPlayerCoords = GlobalServerBD.g_vWarpRequest[iPlayer]
								PRINTLN("[spawning] AreAnyOtherGangMembersAliveOrRespawningInArea - player ", iPlayer, " is warping to coords ", vPlayerCoords)
							ENDIF
						ENDIF
					ENDIF
					
					IF (VMAG(vPlayerCoords) > 0.0)
						IF (VDIST(vPlayerCoords, vCoords) < fRadius)
							vReturn = vPlayerCoords	
							PRINTLN("[spawning] AreAnyOtherGangMembersAliveOrRespawningInArea - returning TRUE! player ", iPlayer, " coords ", vPlayerCoords)
							RETURN TRUE
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	PRINTLN("[spawning] AreAnyOtherGangMembersAliveOrRespawningInArea - returning FALSE")
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_THIS_FACTORY_A_BUNKER(FACTORY_ID eFactoryID)
	IF eFactoryID = FACTORY_ID_BUNKER_1
	OR eFactoryID = FACTORY_ID_BUNKER_2
	OR eFactoryID = FACTORY_ID_BUNKER_3
	OR eFactoryID = FACTORY_ID_BUNKER_4
	OR eFactoryID = FACTORY_ID_BUNKER_5
	OR eFactoryID = FACTORY_ID_BUNKER_6
	OR eFactoryID = FACTORY_ID_BUNKER_7
//	OR eFactoryID = FACTORY_ID_BUNKER_8
	OR eFactoryID = FACTORY_ID_BUNKER_9
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC MP_INT_STATS GET_FACTORY_STAT_FOR_SLOT(INT iFactorySlot)
	SWITCH iFactorySlot
		CASE 0	RETURN MP_STAT_FACTORYSLOT0
		CASE 1	RETURN MP_STAT_FACTORYSLOT1
		CASE 2	RETURN MP_STAT_FACTORYSLOT2
		CASE 3	RETURN MP_STAT_FACTORYSLOT3
		CASE 4	RETURN MP_STAT_FACTORYSLOT4
		CASE 5	RETURN MP_STAT_FACTORYSLOT4
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "[FACTORY] GET_FACTORY_STAT_FOR_SLOT passed invalid slot: ", iFactorySlot)
	RETURN MP_STAT_FACTORYSLOT0
ENDFUNC

PROC AddCustomSP(VECTOR vPos, FLOAT fHeading, FLOAT fWeighting=1.0 #IF IS_DEBUG_BUILD, FLOAT fOverrideDistanceCheck = CUSTOM_SPAWN_POINT_MIN_DIST #ENDIF)
	IF NOT (g_SpawnData.bUseCustomSpawnPoints)
		SCRIPT_ASSERT("ADD_CUSTOM_SPAWN_POINT - USE_CUSTOM_SPAWN_POINTS(TRUE) not called first!")
	ENDIF
	IF (g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS)
	
		// check new point is not too close to another spawn point
		#IF IS_DEBUG_BUILD
		INT i
		REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i 
			IF (VDIST(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, vPos) < fOverrideDistanceCheck )
				NET_PRINT("[spawning] ADD_CUSTOM_SPAWN_POINT - adding point - ") NET_PRINT_VECTOR(vPos) NET_NL()
				NET_PRINT("[spawning] ADD_CUSTOM_SPAWN_POINT - existing point which is too close - ") NET_PRINT_VECTOR(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos) NET_NL()				
				SCRIPT_ASSERT("ADD_CUSTOM_SPAWN_POINT - Point is too close to another custom spawn point!")				
			ENDIF
		ENDREPEAT
		#ENDIF
		
		// check new point is not at origin
		IF (VMAG(vPos) <= 0.01)
			NET_PRINT("[spawning] ADD_CUSTOM_SPAWN_POINT - trying to add point at origin. ignoring. ") NET_NL()
			SCRIPT_ASSERT("ADD_CUSTOM_SPAWN_POINT - trying to add point at origin!")	
			EXIT
		ENDIF
	
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] ADD_CUSTOM_SPAWN_POINT - adding point ") NET_PRINT_INT(g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints) 
		NET_PRINT(" at ") NET_PRINT_VECTOR(vPos) NET_PRINT(" heading = ") NET_PRINT_FLOAT(fHeading) 
		NET_PRINT(" from script ") NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_NL()
		#ENDIF
		
		// check point isn't being added a long time after the last one or by a different thread
		#IF IS_DEBUG_BUILD
		
			IF NOT ( GET_ID_OF_THIS_THREAD() = g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID)
				NET_PRINT("[spawning] ADD_CUSTOM_SPAWN_POINT - adding point - ") NET_PRINT_VECTOR(vPos) NET_NL()
				NET_PRINT("[spawning] ADD_CUSTOM_SPAWN_POINT - different thread from  USE_CUSTOM_SPAWN_POINTS, thread id is from ") NET_PRINT(g_SpawnData.CustomSpawnPointInfo.ScriptUsingCustomSpawnPoints) NET_NL()	
				SCRIPT_ASSERT("ADD_CUSTOM_SPAWN_POINT - different thread from  USE_CUSTOM_SPAWN_POINTS!")		
			ENDIF
		
			#IF FEATURE_FREEMODE_ARCADE
            #IF FEATURE_COPS_N_CROOKS
            IF IS_FREEMODE_ARCADE()AND (GET_ARCADE_MODE() = ARC_COPS_CROOKS)
				// Ignore assert in cops and crooks as delays between AddCustomSP calls are expected during initialisation
			ELSE
			#ENDIF
        	#ENDIF
				IF ( ABSI(GET_TIME_DIFFERENCE(g_SpawnData.CustomSpawnPointInfo.LastAddCustomSpawnTime, GET_NETWORK_TIME())) > 10000)
					NET_PRINT("[spawning] ADD_CUSTOM_SPAWN_POINT - adding point - ") NET_PRINT_VECTOR(vPos) NET_NL()
					NET_PRINT("[spawning] ADD_CUSTOM_SPAWN_POINT - more than 10 secs since you last called ADD_CUSTOM_SPAWN_POINT ") NET_NL()
					SCRIPT_ASSERT("ADD_CUSTOM_SPAWN_POINT - more than 10 secs since USE_CUSTOM_SPAWN_POINTS or last ADD_CUSTOM_SPAWN_POINT was called. Probably need to call CLEAR_CUSTOM_SPAWN_POINTS()")	
				ENDIF
			#IF FEATURE_FREEMODE_ARCADE
        	#IF FEATURE_COPS_N_CROOKS
			ENDIF
			#ENDIF
    		#ENDIF
		
			g_SpawnData.CustomSpawnPointInfo.LastAddCustomSpawnTime = GET_NETWORK_TIME()
			
		#ENDIF
		
	
		g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints].vPos = vPos
		g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints].fHeading = fHeading
		g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints].fWeighting = fWeighting
		
		g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints++
	ELSE
		NET_PRINT("[spawning] ADD_CUSTOM_SPAWN_POINT - reached MAX_NUMBER_OF_CUSTOM_VEHICLE_NODES ") NET_NL()	
		SCRIPT_ASSERT("ADD_CUSTOM_SPAWN_POINT - reached MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS")
	ENDIF	
ENDPROC

FUNC BOOL AreSpecificCoordsSetToArena()
	IF (VDIST(<<g_SpecificSpawnLocation.vCoords.x, g_SpecificSpawnLocation.vCoords.y, 0.0>>, <<ARENA_X, ARENA_Y, 0.0>>) < 20.0)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL GetSpawnLocationCoords(SPAWN_RESULTS &ReturnedSpawnResults, PLAYER_SPAWN_LOCATION SpawnLoc, BOOL bForVehicle, BOOL bDoTeamMateVisCheck, BOOL bDontUseVehicleNodes, BOOL bUseCarNodeOffset, BOOL bUseStartOfMissionPVSpawn, MODEL_NAMES eOverrideModel=DUMMY_MODEL_FOR_SCRIPT )

	
	#IF IS_DEBUG_BUILD
	STRING strLocation = GET_SPAWN_LOCATION_DEBUG_NAME(SpawnLoc)
	PRINTLN("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_", strLocation)
	#ENDIF

	INT i
	BOOL bTeamMateVisCheck
	BOOL bUsesFixedPoints = FALSE

	FLOAT fMinX, fMaxX
	FLOAT fMinY, fMaxY
	INT iNearestHospital
	INT iNearestProperty
	INT iNearest
	FLOAT fDistNearestHospital
	FLOAT fDistNearestProperty	
	
	FLOAT fTemp
	VECTOR vEnemyVec
	VECTOR vec
	
	INT iPropertyID
	//INT iNearestPropertyID
	INT iOwnedProperty
	//VECTOR vReturn
	//FLOAT fReturn
	//FLOAT fCoronaDist
	VECTOR vCoords
	SPAWN_RESULTS SpawnResults
	CUSTOM_SPAWN_SCORING_DATA CustomSpawnResults
		INT iYachtID

	VECTOR vInitial
//	VECTOR vPrimary
//	VECTOR vSecondary	
	VECTOR vEnemy
	VECTOR vEnemyFacing
	VECTOR vAwayFromEnemy
	VECTOR vAwayFromEnemyPoint[4]
	INT iNumberOfEnemiesNearPoint[4]
	BOOL bEnemiesFacingPoint[4]
	FLOAT fNearestEnemyDist[4]
	FLOAT fAdditionalDist
	INT iEnemiesNearOriginalPoint
	BOOL bGotEnemyPoint
	INT iFewestEnemies
	INT iStored
	FLOAT fHighestStoredEnemyDist
	FLOAT fDistFromPlayer
	FLOAT fMaxDistFromSearchPoint
	PLAYER_INDEX GoonID
	INT iGlobalExlcusion

	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] GetSpawnLocationCoords - local player is not active network player") NET_NL()
			#ENDIF
			RETURN(FALSE)		
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] GetSpawnLocationCoords - network game is not active") NET_NL()
		#ENDIF
		RETURN(FALSE)
	ENDIF
	
	IF NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] GetSpawnLocationCoords - NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING") NET_NL()
		#ENDIF
		RETURN(FALSE)
	ENDIF
				
	// if this is being called more than 5 secs after the last call assume its a new call, or if a different thread is calling it
	IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iGetLocationTime) > 60000)
	OR NOT (GET_ID_OF_THIS_THREAD() = g_SpawnData.GetLocationThreadID)
		g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_INIT
		g_SpawnData.iGetLocationAttempts = 0
	
	
		// clear fallback attempts
		g_SpawnData.bFallbackUsed = FALSE
		g_SpawnData.iFallbackAttempts = 0	
		
		// clear data for making specific spawn queries
		g_SpawnData.bHasQueriedSpecificPosition = FALSE
		g_SpawnData.iQuestSpecificSpawnReply = 0
		g_SpawnData.iQuerySpecificSpawnState = 0
	
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] GetSpawnLocationCoords - resetting state to INIT") NET_NL()
		#ENDIF
	ENDIF
	
	// are we trying to warp to specific coord if possible? Check if it is possible first
	IF (SpawnLoc = SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE)
	OR (g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE)
		IF NOT (g_SpawnData.bHasQueriedSpecificPosition)
				
			// send request
			IF (g_SpawnData.iQuerySpecificSpawnState = SPECIFIC_SPAWN_QUERY_STATE_IDLE)
				IF IS_ANY_PLAYER_NEAR_POINT(g_SpecificSpawnLocation.vCoords, 150.0)
					BROADCAST_QUERY_SPECIFIC_SPAWN_POINT(g_SpecificSpawnLocation.vCoords)				
					g_SpawnData.iQuestSpecificSpawnReply = 0
					g_SpawnData.iQuerySpecificSpawnTime = GET_NETWORK_TIME()
					g_SpawnData.iQuerySpecificSpawnState = SPECIFIC_SPAWN_QUERY_STATE_WAITING	
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] GetSpawnLocationCoords - sending query for specific spawn coord ") NET_NL()
					#ENDIF
				ELSE
					g_SpawnData.iQuestSpecificSpawnReply = QUERY_REPLY_YES
					g_SpawnData.bHasQueriedSpecificPosition = TRUE
					g_SpawnData.iQuerySpecificSpawnState = SPECIFIC_SPAWN_QUERY_STATE_FINISHED
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] GetSpawnLocationCoords - no other player near specific coord, so ok. ") NET_NL()
					#ENDIF
				ENDIF
			ENDIF
			
			// wait for reply
			IF (g_SpawnData.iQuerySpecificSpawnState = SPECIFIC_SPAWN_QUERY_STATE_WAITING)
		
				// negative
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iQuerySpecificSpawnTime) > 4000)
				OR (g_SpawnData.iQuestSpecificSpawnReply = QUERY_REPLY_NO)
					g_SpawnData.bHasQueriedSpecificPosition = TRUE
					g_SpawnData.iQuerySpecificSpawnState = SPECIFIC_SPAWN_QUERY_STATE_FINISHED
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] GetSpawnLocationCoords - recieved negative specific spawn query ") NET_NL()
					#ENDIF
				ELSE
					// positive
					IF (g_SpawnData.iQuestSpecificSpawnReply = QUERY_REPLY_YES)
						g_SpawnData.bHasQueriedSpecificPosition = TRUE
						g_SpawnData.iQuerySpecificSpawnState = SPECIFIC_SPAWN_QUERY_STATE_FINISHED
						#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] GetSpawnLocationCoords - recieved positive specific spawn query ") NET_NL()
						#ENDIF
					ELSE
						// still waiting
						#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] GetSpawnLocationCoords - waiting for specific spawn query ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iQuerySpecificSpawnTime)) NET_NL()
						#ENDIF
						RETURN(FALSE)
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	

	// initialisation - get raw coords
	IF (g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_INIT)
		
		// reset private params
		ClearStoredSpawningParams()
		
		g_SpawnData.iGetLocationTime = GET_NETWORK_TIME()
		g_SpawnData.GetLocationThreadID = GET_ID_OF_THIS_THREAD()								
					
		g_SpawnData.bIgnoreExclusionCheck = FALSE		
		g_SpawnData.bIgnoreGlobalExclusionCheck = FALSE
		
		
		// public
		g_SpawnData.PublicParams.vFacingCoords = g_SpawnData.MissionSpawnDetails.vFacing
		g_SpawnData.PublicParams.bConsiderInteriors = g_SpawnData.MissionSpawnDetails.bConsiderInteriors		
		g_SpawnData.PublicParams.bPreferPointsCloserToRoads =  g_SpawnData.MissionSpawnDetails.bDoNearARoadChecks

		g_SpawnData.PublicParams.bIsForFlyingVehicle = IsSpawningInPlaneOrHeli()
		
		// private
		g_SpawnData.PrivateParams.bDoVisibleChecks = g_SpawnData.MissionSpawnDetails.bDoVisibilityChecks
		g_SpawnData.PrivateParams.bForAVehicle = g_SpawnData.MissionSpawnDetails.bSpawnInVehicle	
		g_SpawnData.PrivateParams.bCanFaceOncomingTraffic = g_SpawnData.MissionSpawnDetails.bCanFaceOncomingTraffic
		
		g_SpawnData.PrivateParams.bFoundACarNode = FALSE
	
		g_SpawnData.iPreviousGetLocationState = SPAWN_LOCATION_STATE_INIT
	
		// AvoidCoords[1] are specifically set by scripter using SET_RADIUS_TO_AVOID_SPAWNING
		g_SpawnData.PublicParams.vAvoidCoords[1] = g_SpawnData.MissionSpawnDetails.vAvoidCoords
		g_SpawnData.PublicParams.fAvoidRadius[1] = g_SpawnData.MissionSpawnDetails.fAvoidRadius
		
		// set private params
		g_SpawnData.PrivateParams.fSearchRadius = SAFE_RESPAWN_SEARCH_RADIUS // default	
		
		#IF IS_DEBUG_BUILD
		IF NOT (g_SpawnData.bDontResetShapeTestFlag)
		#ENDIF
		g_SpawnData.bDoShapeTest = FALSE
		NET_PRINT_FRAME() NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.bDoShapeTest set to FALSE. (init)") NET_NL()
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		// clear fallback attempts
		g_SpawnData.bFallbackUsed = FALSE
		g_SpawnData.iFallbackAttempts = 0	
		
		g_SpawnData.iGetLocationCarNodeSearchAttempts = 0
		
		IF NOT (g_SpawnData.MissionSpawnDetails.bFacePoint)
			g_SpawnData.MissionSpawnDetails.vFacing = <<0.0, 0.0, 0.0>>
		ENDIF
					
		// get the correct spawn location type
		g_SpawnData.SpawnLocation = SpawnLoc
		IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_AUTOMATIC)
		OR IS_PLAYER_SCTV(PLAYER_ID())
			g_SpawnData.SpawnLocation = GetPlayerSpawnLocationType()
		ENDIF
		
		// if is currently set to respawn in vehicle and using race corona spawning, but an abort is being called then reset it to automatic
		IF (g_SpawnData.MissionSpawnDetails.bSpawnInVehicle)
		AND (g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle)
		AND (g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS_RACE_CORONA)
			g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_POSITION
			PRINTLN("[spawning] GetSpawnLocationCoords - abort spawn in vehicle called while trying to spawn race corona vehicle, use near_current_position instead.")
		ENDIF		
				
		g_SpawnData.bDisableFadeInAndTurnOnControls = FALSE // set to default value.
				
		// specific exclusion data
		g_SpawnData.fNearExclusionRadius = 0.0						
				
		g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle = FALSE
				

		IF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
			g_SpawnData.PrivateParams.bIgnoreTeammatesForMinDistCheck = TRUE
		ELSE
			g_SpawnData.PrivateParams.bIgnoreTeammatesForMinDistCheck = FALSE
		ENDIF		
		
		g_SpawnData.bSpawningOnOwnInGang = FALSE
		g_SpawnData.bIgnoreFaceRoad = FALSE
		
		g_SpawnData.PrivateParams.fPitch = 0.0
		g_SpawnData.fPitch = 0.0
		
		// if we are spawning near a specific location, but the coords are on an active yacht then use the yacht warp
		IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
			iYachtID = GET_PRIVATE_YACHT_POINT_IS_ON(g_SpecificSpawnLocation.vCoords)
			IF NOT (iYachtID = -1)
				IF IS_PRIVATE_YACHT_ACTIVE(iYachtID)
					PRINTLN("[spawning] GetSpawnLocationCoords - specific coord is on active yacht ", iYachtID)
					g_SpawnData.iYachtToWarpTo = iYachtID
					g_SpawnData.SpawnLocation = SPAWN_LOCATION_PRIVATE_FRIEND_YACHT
				ENDIF
			ENDIF
		ENDIF
		
		// if we are spawning near a specific location, but the coords are in a clubhouse then use the clubhouse warp
		IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
			
		ENDIF		
				
		BOOL bSetupTempCustomSpawnPoints
		// if we are spawning 'near' the arena then setup
		IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS)
			IF AreSpecificCoordsSetToArena()
				
				PRINTLN("[spawning] GetSpawnLocationCoords - specific coord is inside arena, using temp custom spawn points ")
				
				ClearCustomSpawnPoints()
				g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints = 0				
				g_SpawnData.CustomSpawnPointInfo.bNeverFallBackToNavMesh = FALSE
				g_SpawnData.bUseCustomSpawnPoints = TRUE
				g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID = GET_ID_OF_THIS_THREAD() 
				#IF IS_DEBUG_BUILD
					g_SpawnData.CustomSpawnPointInfo.LastAddCustomSpawnTime = GET_NETWORK_TIME()
				#ENDIF
				
				AddCustomSP(<<-244.9010, -1872.1770, 26.7550>>,320.7970)
				AddCustomSP(<<-242.2690, -1874.4790, 26.7550>>,320.7970)
				AddCustomSP(<<-239.5980, -1876.8160, 26.7550>>,320.7970)
				AddCustomSP(<<-247.7040, -1869.7260, 26.7550>>,320.7970)
				AddCustomSP(<<-236.7350, -1879.3199, 26.7550>>,320.7970)
				AddCustomSP(<<-250.4830, -1867.4670, 26.7550>>,320.7970)
				AddCustomSP(<<-248.4070, -1872.9890, 26.7550>>,320.7970)
				AddCustomSP(<<-245.4030, -1875.6169, 26.7550>>,320.7970)
				AddCustomSP(<<-242.7850, -1877.9070, 26.7550>>,320.7970)
				AddCustomSP(<<-239.9450, -1880.1980, 26.7550>>,320.7970)
				AddCustomSP(<<-251.0460, -1870.6660, 26.7550>>,320.7970)
				AddCustomSP(<<-248.6190, -1876.5380, 26.7550>>,320.7970)
				AddCustomSP(<<-245.9010, -1878.9150, 26.7550>>,320.7970)
				AddCustomSP(<<-251.7040, -1873.9390, 26.7550>>,320.7970)
				AddCustomSP(<<-243.0800, -1881.2271, 26.7550>>,320.7970)
				AddCustomSP(<<-254.6770, -1871.3250, 26.7550>>,320.7970)
				AddCustomSP(<<-240.5360, -1883.4480, 26.7550>>,320.7970)
				AddCustomSP(<<-249.1740, -1880.1801, 26.7550>>,320.7970)
				AddCustomSP(<<-252.2830, -1877.4611, 26.7550>>,320.7970)
				AddCustomSP(<<-246.5150, -1882.4220, 26.7550>>,320.7970)
				AddCustomSP(<<-243.8050, -1884.5220, 26.7550>>,320.7970)
				AddCustomSP(<<-255.3060, -1874.9020, 26.7550>>,320.7970)
				AddCustomSP(<<-252.4910, -1881.0330, 26.7550>>,320.7970)
				AddCustomSP(<<-249.8400, -1883.3510, 26.7550>>,320.7970)
				AddCustomSP(<<-246.8860, -1885.7740, 26.7550>>,320.7970)
				AddCustomSP(<<-255.6410, -1878.1750, 26.7550>>,320.7970)
				AddCustomSP(<<-244.2810, -1887.7390, 26.7550>>,320.7970)
				AddCustomSP(<<-258.5930, -1875.6910, 26.7550>>,320.7970)
				AddCustomSP(<<-252.7670, -1884.1440, 26.7550>>,320.7970)
				AddCustomSP(<<-255.8510, -1881.4821, 26.7550>>,320.7970)
				AddCustomSP(<<-249.8800, -1886.7040, 26.7550>>,320.7970)
				AddCustomSP(<<-247.4400, -1888.8380, 26.7550>>,320.7970)
				AddCustomSP(<<-258.8860, -1878.8270, 26.7550>>,320.7970)
				AddCustomSP(<<-255.9520, -1884.8010, 26.7550>>,320.7970)
				AddCustomSP(<<-252.9560, -1887.4220, 26.7550>>,320.7970)
				AddCustomSP(<<-259.4150, -1881.7729, 26.7550>>,320.7970)
				AddCustomSP(<<-250.3690, -1889.6851, 26.7550>>,320.7970)
				AddCustomSP(<<-247.6470, -1892.0649, 26.7550>>,320.7970)
				AddCustomSP(<<-262.1380, -1879.3910, 26.7550>>,320.7970)
				AddCustomSP(<<-262.4070, -1882.8110, 26.7550>>,320.7970)
				AddCustomSP(<<-259.2770, -1885.5490, 26.7550>>,320.7970)
				AddCustomSP(<<-256.1240, -1888.0940, 26.7550>>,320.7970)
				AddCustomSP(<<-253.2250, -1890.3000, 26.7550>>,320.7970)
				AddCustomSP(<<-250.7820, -1892.7791, 26.7550>>,320.7970)
				AddCustomSP(<<-259.6830, -1889.2209, 26.7550>>,320.7970)
				AddCustomSP(<<-256.5770, -1891.8240, 26.7550>>,320.7970)
				AddCustomSP(<<-263.2400, -1886.2159, 26.7550>>,320.7970)
				AddCustomSP(<<-253.8670, -1894.1970, 26.7550>>,320.7970)
				AddCustomSP(<<-251.2580, -1896.4120, 26.7550>>,320.7970)
				AddCustomSP(<<-265.9740, -1883.9440, 26.7550>>,320.7970)
				AddCustomSP(<<-262.9350, -1890.0890, 26.7550>>,320.7970)
				AddCustomSP(<<-259.8840, -1892.6740, 26.7550>>,320.7970)
				AddCustomSP(<<-256.9430, -1895.1660, 26.7550>>,320.7970)
				AddCustomSP(<<-254.5350, -1897.2061, 26.7550>>,320.7970)
				AddCustomSP(<<-266.4690, -1887.1180, 26.7550>>,320.7970)
				AddCustomSP(<<-263.2430, -1893.5389, 26.7550>>,320.7970)
				AddCustomSP(<<-260.0930, -1896.2230, 26.7550>>,320.7970)
				AddCustomSP(<<-266.7540, -1890.5470, 26.7550>>,320.7970)
				AddCustomSP(<<-257.3500, -1898.5610, 26.7550>>,320.7970)
				AddCustomSP(<<-254.5160, -1900.9760, 26.7550>>,320.7970)
				AddCustomSP(<<-269.7290, -1888.0129, 26.7550>>,320.7970)
				AddCustomSP(<<-266.4280, -1894.1810, 26.7550>>,320.7970)
				AddCustomSP(<<-263.2780, -1896.8650, 26.7550>>,320.7970)
				AddCustomSP(<<-260.3670, -1899.3450, 26.7550>>,320.7970)
				AddCustomSP(<<-269.9690, -1891.1670, 26.7550>>,320.7970)
				AddCustomSP(<<-257.5120, -1901.7820, 26.7550>>,320.7970)
				AddCustomSP(<<-266.6960, -1897.6660, 26.7550>>,320.7970)
				AddCustomSP(<<-263.5400, -1900.3540, 26.7550>>,320.7970)
				AddCustomSP(<<-260.6380, -1902.8270, 26.7550>>,320.7970)
				AddCustomSP(<<-270.4690, -1894.4500, 26.7550>>,320.7970)
				AddCustomSP(<<-267.2220, -1901.5220, 26.7550>>,320.7970)
				AddCustomSP(<<-270.7470, -1898.5590, 26.7550>>,320.7970)
				AddCustomSP(<<-264.2110, -1904.1281, 26.7550>>,320.7970)
				AddCustomSP(<<-260.7500, -1907.0780, 26.7550>>,320.7970)
				AddCustomSP(<<-274.3720, -1895.4700, 26.7550>>,320.7970)
				AddCustomSP(<<-271.1900, -1902.0580, 26.7550>>,320.7970)
				AddCustomSP(<<-268.0330, -1904.7480, 26.7550>>,320.7970)
				AddCustomSP(<<-264.1350, -1908.0699, 26.7550>>,320.7970)
				AddCustomSP(<<-275.1130, -1898.9590, 26.7550>>,320.7970)
				AddCustomSP(<<-271.7750, -1906.2070, 26.7550>>,320.7970)

				g_SpawnData.SpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS
				bSetupTempCustomSpawnPoints = TRUE
				
			ENDIF
		ENDIF		
		
		#IF FEATURE_CASINO
		IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_CASINO_OUTSIDE)

			ClearCustomSpawnPoints()
			g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints = 0				
			g_SpawnData.CustomSpawnPointInfo.bNeverFallBackToNavMesh = FALSE
			g_SpawnData.bUseCustomSpawnPoints = TRUE
			g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID = GET_ID_OF_THIS_THREAD() 
			#IF IS_DEBUG_BUILD
				g_SpawnData.CustomSpawnPointInfo.LastAddCustomSpawnTime = GET_NETWORK_TIME()
			#ENDIF
			
			AddCustomSp(<<975.7955, 89.2188, 79.9907>>,240.1994)
			AddCustomSp(<<974.2166, 85.8614, 79.9907>>,240.1994)
			AddCustomSp(<<971.4763, 91.6587, 79.9907>>,240.1994)
			AddCustomSp(<<966.8154, 94.2058, 79.9907>>,240.1994)
			AddCustomSp(<<963.1030, 96.5164, 79.9907>>,240.1994)
			AddCustomSp(<<969.8340, 88.5924, 79.9907>>,240.1994)
			AddCustomSp(<<968.1555, 85.3428, 79.9886>>,327.1991)
			AddCustomSp(<<965.3240, 82.3137, 79.7830>>,327.1991)
			AddCustomSp(<<960.2003, 80.9701, 79.7694>>,267.1988)
			AddCustomSp(<<953.6874, 82.8776, 79.7694>>,243.1988)
			AddCustomSp(<<947.6548, 86.1658, 79.7688>>,243.1988)
			AddCustomSp(<<946.0387, 82.9043, 79.7687>>,243.1988)
			AddCustomSp(<<944.2261, 88.6068, 79.7694>>,243.1988)
			AddCustomSp(<<939.5576, 94.6460, 78.3392>>,229.9987)
			AddCustomSp(<<935.7590, 96.2394, 78.2388>>,229.9987)
			AddCustomSp(<<944.3122, 79.7098, 79.7572>>,323.1983)
			AddCustomSp(<<941.9835, 75.9773, 79.6150>>,323.1983)
			AddCustomSp(<<939.4975, 71.6506, 79.4041>>,323.1983)
			AddCustomSp(<<936.4024, 66.9599, 79.1841>>,323.1983)
			AddCustomSp(<<942.0676, 92.2227, 78.9701>>,215.5980)
			AddCustomSp(<<927.3275, 77.3361, 77.7679>>,321.5972)
			AddCustomSp(<<925.1186, 73.9533, 78.0651>>,321.5972)
			AddCustomSp(<<922.8524, 70.4011, 78.7589>>,321.5972)
			AddCustomSp(<<920.5540, 66.8094, 79.4079>>,321.5972)
			AddCustomSp(<<963.9402, 100.7390, 79.9907>>,148.5968)
			AddCustomSp(<<965.7116, 103.4286, 79.9907>>,148.5968)
			AddCustomSp(<<967.3428, 106.3218, 79.9907>>,148.5968)
			AddCustomSp(<<969.3038, 109.4050, 79.9907>>,148.5968)
			AddCustomSp(<<971.3239, 112.6910, 79.9907>>,148.5968)
			AddCustomSp(<<973.8073, 116.3687, 79.9907>>,148.5968)

			
//			AddCustomSp(<<920.4699, 67.2869, 79.3541>>, 197.0628, 0.98)
//			AddCustomSp(<<922.2841, 69.3411, 78.9508>>, 174.3553, 0.98)
//			AddCustomSp(<<923.4102, 71.3140, 78.5870>>, 188.9356, 0.98)
//			AddCustomSp(<<924.0543, 72.9098, 78.3044>>, 169.2867, 0.98)
//			AddCustomSp(<<925.0819, 74.6342, 77.9595>>, 171.9520, 0.98)
//			AddCustomSp(<<926.5999, 77.0588, 77.7562>>, 171.7763, 0.98)
//			AddCustomSp(<<931.4033, 59.4856, 79.6716>>, 144.7619, 0.98)
//			AddCustomSp(<<932.7118, 60.9798, 79.3895>>, 129.0962, 0.98)
//			AddCustomSp(<<933.6580, 62.4999, 79.1319>>, 120.6990, 0.98)
//			AddCustomSp(<<934.7245, 64.6978, 78.8003>>, 141.9312, 0.98)
//			AddCustomSp(<<935.9254, 66.5032, 78.5055>>, 142.1863, 0.98)
//			AddCustomSp(<<936.8512, 68.1031, 78.2542>>, 134.5367, 0.98)
//			AddCustomSp(<<938.5047, 70.1143, 78.1832>>, 123.8948, 0.98)
//			AddCustomSp(<<939.1642, 71.7546, 78.1838>>, 131.2863, 0.98)
//			AddCustomSp(<<940.2718, 73.2175, 78.1868>>, 147.3491, 0.98)
//			AddCustomSp(<<941.3595, 74.9551, 78.1883>>, 138.1922, 0.98)
//			AddCustomSp(<<942.5588, 76.4951, 78.1911>>, 139.9500, 0.98)
//			AddCustomSp(<<943.6694, 78.6040, 78.1968>>, 129.9129, 0.98)
//			AddCustomSp(<<944.5090, 82.3231, 78.2271>>, 144.2222, 0.98)
//			AddCustomSp(<<944.6472, 85.0382, 78.2810>>, 140.4564, 0.98)
//			AddCustomSp(<<944.4349, 87.3203, 78.3521>>, 143.9628, 0.98)
//			AddCustomSp(<<943.6360, 89.9845, 78.3566>>, 177.6510, 0.98)
//			AddCustomSp(<<942.5500, 91.4929, 78.3456>>, 149.8159, 0.98)
//			AddCustomSp(<<939.9016, 95.6683, 78.3840>>, 180.7038, 1.0)
//			AddCustomSp(<<936.6458, 96.3539, 78.2799>>, 168.9554, 1.0)
//			AddCustomSp(<<938.0635, 98.2309, 78.4098>>, 177.2429, 1.0)
//			AddCustomSp(<<921.8563, 79.3532, 77.9684>>, 198.6605, 0.98)
//			AddCustomSp(<<916.3047, 75.3528, 78.1955>>, 198.7144, 0.98)
//			AddCustomSp(<<868.2499, 18.9819, 77.9496>>, 289.4670, 0.98)
//			AddCustomSp(<<864.2101, 17.6214, 78.1018>>, 300.1052, 0.98)
//			AddCustomSp(<<865.4457, 14.7395, 78.1818>>, 298.1141, 0.98)
//			AddCustomSp(<<861.4430, 16.0793, 78.1847>>, 285.5677, 0.98)
//			AddCustomSp(<<859.9887, 12.8133, 78.3119>>, 312.7482, 0.98)
//			AddCustomSp(<<861.4059, 10.2295, 78.3839>>, 312.3237, 0.98)
//			AddCustomSp(<<883.6786, 8.0627, 77.9052>>, 312.7859, 0.98)
//			AddCustomSp(<<891.5606, 20.2421, 77.8968>>, 321.0143, 0.98)
//			AddCustomSp(<<895.0829, 25.3943, 78.2053>>, 314.1316, 0.98)
//			AddCustomSp(<<897.8085, 29.9574, 78.6094>>, 326.5070, 0.98)
//			AddCustomSp(<<900.7192, 34.3986, 79.0132>>, 321.7861, 0.98)
//			AddCustomSp(<<912.6129, 28.9203, 79.3326>>, 333.8285, 0.98)
//			AddCustomSp(<<911.1889, 26.7047, 79.1309>>, 343.4649, 0.98)
//			AddCustomSp(<<909.8597, 24.6871, 78.9492>>, 349.1953, 0.98)
//			AddCustomSp(<<909.0946, 22.5724, 78.7846>>, 345.1906, 0.98)
//			AddCustomSp(<<907.0794, 19.9280, 78.5259>>, 343.5613, 0.98)
//			AddCustomSp(<<905.9883, 17.8403, 78.3140>>, 336.0101, 0.98)
//			AddCustomSp(<<904.5457, 15.6316, 78.0860>>, 334.0568, 0.98)
//			AddCustomSp(<<902.4814, 13.2976, 78.0261>>, 329.7153, 0.98)		
		
			g_SpawnData.SpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS
			bSetupTempCustomSpawnPoints = TRUE
		ENDIF
		#ENDIF
			
#IF FEATURE_GEN9_EXCLUSIVE
		IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_TUTORIAL_BUSINESS)
			IF (MP_INTRO_GET_BIKER_BUSINESS_SIMPLE_INTERIOR() = SIMPLE_INTERIOR_INVALID)
				PRINTLN("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_TUTORIAL_BUSINESS, invalid interior, using NEAR_CURRENT_POSITION instead")
				g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_POSITION	
			ENDIF
		ENDIF
#ENDIF
				
		#IF FEATURE_COPS_N_CROOKS
		IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_AFTER_DLC_INTRO_BINK)

			ClearCustomSpawnPoints()
			g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints = 0				
			g_SpawnData.CustomSpawnPointInfo.bNeverFallBackToNavMesh = FALSE
			g_SpawnData.bUseCustomSpawnPoints = TRUE
			g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID = GET_ID_OF_THIS_THREAD() 
			#IF IS_DEBUG_BUILD
				g_SpawnData.CustomSpawnPointInfo.LastAddCustomSpawnTime = GET_NETWORK_TIME()
			#ENDIF
			INT iRand = GET_RANDOM_INT_IN_RANGE(0,4)
			
			SWITCH iRand
				CASE 0 //Mission Row Police Station
					AddCustomSp(<<420.9432, -1012.2202, 28.1186>>, 266.7996, 1)
					AddCustomSp(<<420.9577, -1015.8696, 28.1035>>, 266.7996, 1)
					AddCustomSp(<<418.8565, -1014.1337, 28.1559>>, 266.7996, 1)
					AddCustomSp(<<417.1486, -1012.1511, 28.2526>>, 266.7996, 1)
					AddCustomSp(<<417.1978, -1015.8683, 28.2021>>, 266.7996, 1)
					AddCustomSp(<<415.4503, -1014.1009, 28.2517>>, 266.7996, 1)
					AddCustomSp(<<415.3980, -1010.0495, 28.2450>>, 266.7996, 1)
					AddCustomSp(<<413.6792, -1012.1526, 28.3047>>, 266.7996, 1)
					AddCustomSp(<<413.6984, -1015.9318, 28.3111>>, 266.7996, 1)
					AddCustomSp(<<411.8849, -1014.0936, 28.3167>>, 266.7996, 1)
					AddCustomSp(<<410.1700, -1012.2105, 28.3686>>, 266.7996, 1)
					AddCustomSp(<<411.9411, -1010.0983, 28.3584>>, 266.7996, 1)
					AddCustomSp(<<410.0190, -1015.9506, 28.3574>>, 266.7996, 1)
					AddCustomSp(<<408.3861, -1014.2659, 28.3903>>, 266.7996, 1)
					AddCustomSp(<<406.6572, -1012.1475, 28.4008>>, 266.7996, 1)
				BREAK
				CASE 1 //Vespucci Police Station
					AddCustomSp(<<-1050.2140, -809.8273, 17.2854>>, 150.7992, 1)
					AddCustomSp(<<-1052.3330, -808.8765, 17.4549>>, 150.7992, 1)
					AddCustomSp(<<-1054.6646, -807.7485, 17.6450>>, 150.7992, 1)
					AddCustomSp(<<-1047.8641, -810.8328, 17.1794>>, 150.7992, 1)
					AddCustomSp(<<-1057.0457, -806.5580, 17.8136>>, 150.7992, 1)
					AddCustomSp(<<-1050.7025, -807.8139, 17.3831>>, 150.7992, 1)
					AddCustomSp(<<-1048.4685, -808.8634, 17.2157>>, 150.7992, 1)
					AddCustomSp(<<-1052.9530, -806.7665, 17.5759>>, 150.7992, 1)
					AddCustomSp(<<-1055.2930, -805.6833, 17.7461>>, 150.7992, 1)
					AddCustomSp(<<-1051.1312, -805.6561, 17.4936>>, 150.7992, 1)
					AddCustomSp(<<-1048.8323, -806.6766, 17.3093>>, 150.7992, 1)
					AddCustomSp(<<-1046.0319, -808.1805, 17.1731>>, 150.7992, 1)
					AddCustomSp(<<-1053.8976, -804.5848, 17.7029>>, 150.7992, 1)
					AddCustomSp(<<-1056.5331, -803.2167, 17.8871>>, 150.7992, 1)
					AddCustomSp(<<-1058.6118, -804.3932, 17.9488>>, 150.7992, 1)
				BREAK
				CASE 2 //Sandy Shores Sheriff Station
					AddCustomSp(<<1853.3795, 3710.9255, 32.2877>>, 215.7990, 1)
					AddCustomSp(<<1851.3689, 3709.8491, 32.2209>>, 215.7990, 1)
					AddCustomSp(<<1855.7197, 3712.5393, 32.3073>>, 215.7990, 1)
					AddCustomSp(<<1848.7668, 3708.8948, 32.5827>>, 215.7990, 1)
					AddCustomSp(<<1851.3357, 3712.1641, 32.1747>>, 215.7990, 1)
					AddCustomSp(<<1853.4578, 3713.4358, 32.1990>>, 215.7990, 1)
					AddCustomSp(<<1848.9415, 3711.0107, 32.3397>>, 215.7990, 1)
					AddCustomSp(<<1851.1360, 3714.6724, 32.0913>>, 215.7990, 1)
					AddCustomSp(<<1853.5192, 3716.1113, 32.0650>>, 215.7990, 1)
					AddCustomSp(<<1848.5391, 3713.4966, 32.1848>>, 215.7990, 1)
					AddCustomSp(<<1856.0779, 3715.2686, 32.1985>>, 215.7990, 1)
					AddCustomSp(<<1856.2322, 3718.1538, 32.0472>>, 215.7990, 1)
					AddCustomSp(<<1858.6858, 3713.9248, 32.2439>>, 215.7990, 1)
					AddCustomSp(<<1850.9530, 3717.2813, 32.0806>>, 215.7990, 1)
					AddCustomSp(<<1853.6536, 3718.9685, 32.0972>>, 215.7990, 1)
				BREAK
				CASE 3 //Paleto Bay Sheriff Station
					AddCustomSp(<<-461.7798, 6000.5498, 30.3405>>, 321.5986, 1)
					AddCustomSp(<<-463.3470, 6002.0815, 30.3405>>, 321.5986, 1)
					AddCustomSp(<<-460.1252, 5998.9336, 30.3405>>, 321.5986, 1)
					AddCustomSp(<<-465.1187, 6003.8188, 30.3405>>, 321.5986, 1)
					AddCustomSp(<<-462.2315, 5998.2944, 30.2734>>, 321.5986, 1)
					AddCustomSp(<<-463.9948, 6000.0200, 30.2714>>, 321.5986, 1)
					AddCustomSp(<<-465.8052, 6001.7563, 30.2929>>, 321.5986, 1)
					AddCustomSp(<<-467.7934, 6003.8145, 30.3127>>, 321.5986, 1)
					AddCustomSp(<<-466.7539, 6005.7236, 30.3405>>, 321.5986, 1)
					AddCustomSp(<<-464.4448, 5997.5566, 30.2458>>, 321.5986, 1)
					AddCustomSp(<<-466.3206, 5999.3608, 30.2652>>, 321.5986, 1)
					AddCustomSp(<<-468.2234, 6001.1646, 30.3018>>, 321.5986, 1)
					AddCustomSp(<<-468.7456, 5998.6919, 30.2826>>, 321.5986, 1)
					AddCustomSp(<<-466.6108, 5996.6616, 30.2458>>, 321.5986, 1)
					AddCustomSp(<<-464.7654, 5994.8394, 30.2457>>, 321.5986, 1)
				BREAK
				
				DEFAULT //Mission Row Police Station
					AddCustomSp(<<420.9432, -1012.2202, 28.1186>>, 266.7996, 1)
					AddCustomSp(<<420.9577, -1015.8696, 28.1035>>, 266.7996, 1)
					AddCustomSp(<<418.8565, -1014.1337, 28.1559>>, 266.7996, 1)
					AddCustomSp(<<417.1486, -1012.1511, 28.2526>>, 266.7996, 1)
					AddCustomSp(<<417.1978, -1015.8683, 28.2021>>, 266.7996, 1)
					AddCustomSp(<<415.4503, -1014.1009, 28.2517>>, 266.7996, 1)
					AddCustomSp(<<415.3980, -1010.0495, 28.2450>>, 266.7996, 1)
					AddCustomSp(<<413.6792, -1012.1526, 28.3047>>, 266.7996, 1)
					AddCustomSp(<<413.6984, -1015.9318, 28.3111>>, 266.7996, 1)
					AddCustomSp(<<411.8849, -1014.0936, 28.3167>>, 266.7996, 1)
					AddCustomSp(<<410.1700, -1012.2105, 28.3686>>, 266.7996, 1)
					AddCustomSp(<<411.9411, -1010.0983, 28.3584>>, 266.7996, 1)
					AddCustomSp(<<410.0190, -1015.9506, 28.3574>>, 266.7996, 1)
					AddCustomSp(<<408.3861, -1014.2659, 28.3903>>, 266.7996, 1)
					AddCustomSp(<<406.6572, -1012.1475, 28.4008>>, 266.7996, 1)
				BREAK
			ENDSWITCH
			g_SpawnData.SpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS
			bSetupTempCustomSpawnPoints = TRUE
		ENDIF
		#ENDIF
				
		#IF FEATURE_HEIST_ISLAND	
		IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_HEIST_ISLAND_BEACH_PARTY)

			ClearCustomSpawnPoints()
			g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints = 0				
			g_SpawnData.CustomSpawnPointInfo.bNeverFallBackToNavMesh = FALSE
			g_SpawnData.bUseCustomSpawnPoints = TRUE
			g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID = GET_ID_OF_THIS_THREAD() 
			#IF IS_DEBUG_BUILD
				g_SpawnData.CustomSpawnPointInfo.LastAddCustomSpawnTime = GET_NETWORK_TIME()
			#ENDIF
			
			AddCustomSp(<<4915.7783, -4909.6812, 2.3612>>, 103.8461)
			AddCustomSp(<<4909.4385, -4906.0132, 2.3634>>, 109.7080)
			AddCustomSp(<<4904.6245, -4901.7183, 2.3729>>, 137.2815)
			AddCustomSp(<<4917.5571, -4913.6855, 2.3654>>, 91.1262)
			AddCustomSp(<<4907.6240, -4916.6104, 2.3844>>, 77.3005)
			AddCustomSp(<<4908.8486, -4927.1821, 2.3643>>, 97.1531)
			AddCustomSp(<<4904.1626, -4933.2432, 2.3827>>, 191.0743)
			AddCustomSp(<<4899.7031, -4939.5010, 2.3622>>, 91.3685)
			AddCustomSp(<<4891.0840, -4934.5527, 2.3710>>, 6.1066)
			AddCustomSp(<<4895.8794, -4930.5977, 2.3695>>, 21.2846)
			AddCustomSp(<<4899.3862, -4926.6353, 2.3645>>, 21.5630)
			AddCustomSp(<<4894.8906, -4926.3525, 2.3615>>, 359.2048)
			AddCustomSp(<<4890.1616, -4923.4365, 2.3687>>, 331.7434)
			AddCustomSp(<<4895.3188, -4919.9331, 2.3677>>, 350.2109)
			AddCustomSp(<<4893.8384, -4915.3374, 2.3677>>, 359.7902)
			AddCustomSp(<<4899.7866, -4914.1606, 2.3602>>, 27.4791)
			AddCustomSp(<<4902.5293, -4910.4746, 2.3611>>, 49.2944)
			AddCustomSp(<<4891.7109, -4908.2109, 2.3643>>, 323.4907)
			AddCustomSp(<<4885.5215, -4908.0737, 2.3678>>, 269.5604)
			AddCustomSp(<<4887.2671, -4914.1382, 2.3628>>, 310.3737)
			AddCustomSp(<<4877.0068, -4914.5981, 2.2139>>, 115.4889)
			AddCustomSp(<<4871.3599, -4908.8730, 1.9205>>, 105.6631)
			AddCustomSp(<<4867.8022, -4916.4653, 1.5922>>, 103.4375)
			AddCustomSp(<<4871.8242, -4926.4834, 2.0005>>, 201.6436)
			AddCustomSp(<<4871.4102, -4929.5703, 1.9024>>, 321.9481)
			AddCustomSp(<<4868.7129, -4935.9365, 1.4152>>, 66.3723)
			AddCustomSp(<<4866.7832, -4942.4521, 1.4781>>, 77.1681)
			AddCustomSp(<<4863.7051, -4950.7769, 1.4398>>, 58.9161)
			AddCustomSp(<<4859.9199, -4941.8872, 0.5639>>, 76.0326)
			AddCustomSp(<<4861.5630, -4927.4663, 0.3028>>, 90.8218)
			AddCustomSp(<<4861.4985, -4916.6597, 0.3063>>, 87.1460)
			AddCustomSp(<<4893.1226, -4902.3950, 2.4867>>, 189.5973)
			AddCustomSp(<<4896.9380, -4903.8062, 2.3717>>, 152.5383)
			AddCustomSp(<<4927.5137, -4907.3760, 2.5188>>, 114.7115)
		
			g_SpawnData.SpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS
			bSetupTempCustomSpawnPoints = TRUE
		ENDIF
		#ENDIF		
		
		#IF FEATURE_DLC_2_2022
		IF g_SpawnData.SpawnLocation = SPAWN_LOCATION_LUXURY_SHOWROOM
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_LUXURY_SHOWROOM") NET_NL()
			#ENDIF
			
			ClearCustomSpawnPoints()
			g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints = 0				
			g_SpawnData.CustomSpawnPointInfo.bNeverFallBackToNavMesh = FALSE
			g_SpawnData.bUseCustomSpawnPoints = TRUE
			g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID = GET_ID_OF_THIS_THREAD() 
			#IF IS_DEBUG_BUILD
				g_SpawnData.CustomSpawnPointInfo.LastAddCustomSpawnTime = GET_NETWORK_TIME()
			#ENDIF
			
			AddCustomSP(<<-792.9677, -247.2787, 36.0962>>, 297.1144)
			AddCustomSP(<<-794.3649, -245.0259, 36.0761>>, 297.1144)
			AddCustomSP(<<-795.9147, -243.1781, 36.0716>>, 297.1144)
			AddCustomSP(<<-790.8631, -250.1062, 36.1497>>, 297.1144)
			AddCustomSP(<<-785.9442, -251.2290, 36.0854>>,  22.8132)
			AddCustomSP(<<-782.9153, -249.0708, 36.0521>>,  22.8132)
			AddCustomSP(<<-780.1253, -247.9639, 36.0550>>,  22.8132)
			AddCustomSP(<<-789.4485, -254.3733, 36.1575>>, 341.3943)

			g_SpawnData.SpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS
			bSetupTempCustomSpawnPoints = TRUE
		ENDIF
			
		IF g_SpawnData.SpawnLocation = SPAWN_LOCATION_SIMEON_SHOWROOM
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_SIMEON_SHOWROOM") NET_NL()
			#ENDIF
	
			ClearCustomSpawnPoints()
			g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints = 0				
			g_SpawnData.CustomSpawnPointInfo.bNeverFallBackToNavMesh = FALSE
			g_SpawnData.bUseCustomSpawnPoints = TRUE
			g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID = GET_ID_OF_THIS_THREAD() 
			#IF IS_DEBUG_BUILD
				g_SpawnData.CustomSpawnPointInfo.LastAddCustomSpawnTime = GET_NETWORK_TIME()
			#ENDIF
			
			AddCustomSP(<<-38.0811, -1114.3545, 25.4392>>, -35.7043)
			AddCustomSP(<<-46.7031, -1108.3710, 25.4381>>, -35.7043)
			AddCustomSP(<<-66.0316, -1100.1001, 25.2383>>, -66.1823)
			AddCustomSP(<<-65.0025, -1095.5886, 25.4132>>, -96.9632)
			AddCustomSP(<<-63.7692, -1092.7776, 25.5470>>, -96.9632)
			AddCustomSP(<<-61.0891, -1087.3768, 25.7510>>, -96.9632)
			AddCustomSP(<<-61.6230, -1085.4803, 25.8820>>, -96.9632)
			AddCustomSP(<<-61.1230, -1086.4803, 25.7820>>, -96.9632)

			g_SpawnData.SpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS
			bSetupTempCustomSpawnPoints = TRUE
		ENDIF
		#ENDIF	
		
		// get an unrefined spawn coord.
		SWITCH g_SpawnData.SpawnLocation
			
			#IF FEATURE_FIXER
			CASE SPAWN_LOCATION_SITTING_SMOKING
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_SITTING_SMOKING") NET_NL()
				#ENDIF
				GetRandomSittingSpawnPoint(g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading, 10.0)	
				bUsesFixedPoints = TRUE
			BREAK
			
			CASE SPAWN_LOCATION_DRUNK_WAKE_UP_MUSIC_STUDIO
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_DRUNK_WAKE_UP_MUSIC_STUDIO") NET_NL()
				#ENDIF
				GetRandomDrunkRespawnMusicStudio(g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading, 10.0)	
				bUsesFixedPoints = TRUE
			BREAK
			#ENDIF
	
			CASE SPAWN_LOCATION_OUTSIDE_SIMEON_GARAGE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_OUTSIDE_SIMEON_GARAGE") NET_NL()
				#ENDIF
				GetRandomMPSpawnCoord(SpawnPoints_SimonGarage, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading, 10.0)				
				bUsesFixedPoints = TRUE	
			BREAK
			
			CASE SPAWN_LOCATION_CUSTOM_SPAWN_POINTS				
				
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS") NET_NL()
				#ENDIF
				
//				IF (g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints < MIN_NUMBER_OF_CUSTOM_SPAWN_POINTS)
//					#IF IS_DEBUG_BUILD
//					NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_CUSTOM_SPAWN_POINTS - g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints < MIN_NUMBER_OF_CUSTOM_SPAWN_POINTS") NET_NL()
//					#ENDIF
//					SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_CUSTOM_SPAWN_POINTS - g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints < MIN_NUMBER_OF_CUSTOM_SPAWN_POINTS") 
//				ENDIF
				
				IF NOT (g_SpawnData.bUseCustomSpawnPoints)
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnLocationCoords - bUseCustomSpawnPoints not TRUE, but spawn location is SPAWN_LOCATION_CUSTOM_SPAWN_POINTS") NET_NL()
					#ENDIF
					SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - bUseCustomSpawnPoints not TRUE, but spawn location is SPAWN_LOCATION_CUSTOM_SPAWN_POINTS")
					g_SpawnData.bUseCustomSpawnPoints = TRUE
				ENDIF								
				//GetCustomSpawnPoint(g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading, TRUE)	
				
				CustomSpawnResults = ReturnCustomSpawnPointWithHighestScore(bForVehicle)
												
				REPEAT NUM_OF_STORED_SPAWN_RESULTS i 
				
					
				
					IF (i = 0)
						IF (CustomSpawnResults.iHighestScoringPoint[i] > -1)
							g_SpawnData.PrivateParams.vSearchCoord = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[CustomSpawnResults.iHighestScoringPoint[i]].vPos
							g_SpawnData.PrivateParams.fRawHeading = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[CustomSpawnResults.iHighestScoringPoint[i]].fHeading							
							g_SpawnData.CustomSpawnPointInfo.iLastCustomSpawnPoint = CustomSpawnResults.iHighestScoringPoint[0]				
							bUsesFixedPoints = TRUE
							bDontUseVehicleNodes = TRUE	
							SpawnResults.fNearestEnemy[i] = CustomSpawnResults.fNearestEnemy[i]
						ELSE
							// no suitable custom spawn was found fallback to use system spawning
							g_SpawnData.PrivateParams.vSearchCoord = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[0].vPos
							g_SpawnData.PrivateParams.fRawHeading = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[0].fHeading	
							g_SpawnData.PublicParams.bCloseToOriginAsPossible = TRUE
							g_SpawnData.PrivateParams.fSearchRadius = 150.0
						ENDIF
					ELSE
						IF (CustomSpawnResults.iHighestScoringPoint[i] > -1)
							SpawnResults.vCoords[i] = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[CustomSpawnResults.iHighestScoringPoint[i]].vPos
							SpawnResults.fHeading[i] = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[CustomSpawnResults.iHighestScoringPoint[i]].fHeading
							SpawnResults.fNearestEnemy[i] = CustomSpawnResults.fNearestEnemy[i]
						ENDIF
					ENDIF
				ENDREPEAT
				

				// was this temporary - for use coming out arena
				IF (bSetupTempCustomSpawnPoints)
					PRINTLN("[spawning] GetSpawnLocationCoords - clearing temp custom spawn points. ")
					ResetCustomSpawnPoints()
				ENDIF

			BREAK		
			
			
			CASE SPAWN_LOCATION_NET_TEST_BED
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NET_TEST_BED") NET_NL()
				#ENDIF
				g_SpawnData.PrivateParams.vSearchCoord = << 23.6269, -17.1858, 0.0025 >>
				g_SpawnData.PrivateParams.fRawHeading = 0.0	
				g_SpawnData.bDontAskPermission = TRUE // no need to ask server for permission as only a single point
				bUsesFixedPoints = TRUE				
			BREAK		
			
			CASE SPAWN_LOCATION_AT_AIRPORT_ARRIVALS
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_AIRPORT_ARRIVALS") NET_NL()
				#ENDIF	
				GetRandomMPSpawnCoord(SpawnPoints_AirportArrivals, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading)
				bUsesFixedPoints = TRUE
			BREAK
			
			#IF FEATURE_HEIST_ISLAND
			CASE SPAWN_LOCATION_HEIST_ISLAND_NEAR_DEATH
			
				PRINTLN("[spawning] GetSpawnLocationCoords - on heist island, setting avoid angled area")
				AddFallbackSpawnPointsForHeistIsland()
					
				g_SpawnData.vExclusionCoord = g_SpawnData.vMyDeadCoords
				
				// if the death point is outside of the island's angled area then move back inside.	
				vCoords.z = g_SpawnData.vMyDeadCoords.z
				MovePointInsideAngledArea(g_SpawnData.vMyDeadCoords, <<g_vHeistIslandAngledArea1.x, g_vHeistIslandAngledArea1.y, 0.0 >>, g_vHeistIslandAngledArea2, g_fHeistIslandAngledAreaWidth)
				
				// use the death z that we started with (the above method can alter that)
				g_SpawnData.vMyDeadCoords.z = vCoords.z
				
				// use a sensible z in context to the island
				IF (g_SpawnData.vMyDeadCoords.z > 70.0)
					g_SpawnData.vMyDeadCoords.z = 70.0
				ENDIF
				IF (g_SpawnData.vMyDeadCoords.z < 0.0)
					g_SpawnData.vMyDeadCoords.z = 0.0
				ENDIF
				
				PRINTLN("[spawning] GetSpawnLocationCoords - on heist island, using g_SpawnData.vMyDeadCoords = ", g_SpawnData.vMyDeadCoords)
				
				// we dont want to spawn on the hills of the island unless we are already on the hills.
				// so we will use a z range from -1 to the players death z + 5m	
				vCoords = g_SpawnData.vMyDeadCoords
				IF (vCoords.z < 0.0)
					vCoords.z = 0.0
				ENDIF
				vCoords.z += 5.0
				
				// avoid spawning on the prologue navmesh, which sits just above the island
				g_SpawnData.PublicParams.vAvoidAngledAreaPos1 = <<0.0, 9999999.9, vCoords.z >>
				g_SpawnData.PublicParams.vAvoidAngledAreaPos2 = <<0.0, -9999999.9, 99999.9 >>
				g_SpawnData.PublicParams.fAvoidAngledAreaWidth = 999999999.9	
				
				
				IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
					g_SpawnData.fNearExclusionRadius = g_SpawnData.MissionSpawnDetails.fMinDistFromDeathNearDeath 
				ELSE
					g_SpawnData.fNearExclusionRadius = 30.0 // 	
				ENDIF				
			
				g_SpawnData.PrivateParams.vSearchCoord = g_SpawnData.vMyDeadCoords
				
				g_SpawnData.PrivateParams.vOffsetOrigin	= g_SpawnData.vMyDeadCoords
				g_SpawnData.PrivateParams.bUseOffsetOrigin = TRUE
				g_SpawnData.PublicParams.bCloseToOriginAsPossible = TRUE
				
				g_SpawnData.PrivateParams.fRawHeading = 0.0
				g_SpawnData.PrivateParams.fSearchRadius = 0.0
				g_SpawnData.PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_ANGLED
				g_SpawnData.PrivateParams.vAngledAreaPoint1 = g_vHeistIslandAngledArea1
				g_SpawnData.PrivateParams.vAngledAreaPoint2 = g_vHeistIslandAngledArea2
				g_SpawnData.PrivateParams.fAngledAreaWidth = g_fHeistIslandAngledAreaWidth
				g_SpawnData.PrivateParams.iFallbackSpawnPointsDefaultResultFlag = 0
				
				g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord 
				g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
		
			BREAK
			#ENDIF
			
			/// ----- DYNAMIC LOCATIONS --------			
			CASE SPAWN_LOCATION_NEAR_DEATH
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_DEATH") NET_NL()
				#ENDIF
		
				
				vCoords = g_SpawnData.vMyDeadCoords
				

				// if the point is inside a zancudo restricted area, and it's not active, then move 100m nearer the centre point of the area.
				// see 3894151
				iGlobalExlcusion = GetGlobalExclusionAreaForPoint(vCoords, FALSE)
				PRINTLN("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_RESPAWN_POINT - iGlobalExlcusion = ", iGlobalExlcusion)
				
				IF (iGlobalExlcusion > -1)
					IF (GlobalExclusionArea[iGlobalExlcusion].ExclusionArea.bIsActive = FALSE)
						PRINTLN("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_RESPAWN_POINT - moving search towards centre of globally restricted zone")	
						MovePointTowardsCentreOfSpawnArea(vCoords, GlobalExclusionArea[iGlobalExlcusion].ExclusionArea, 135.0)					
					ENDIF
				ENDIF
				
				
				IF (g_TransitionSessionNonResetVars.bInsideDisplacedInterior)
					#IF IS_DEBUG_BUILD
					PRINTLN("[spawning] GetSpawnLocationCoords - bInsideDisplacedInterior ") 
					#ENDIF					
					vCoords = g_TransitionSessionNonResetVars.vDisplacedPerceivedCoords
				ENDIF

				// if player is inside a property
				IF (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0)
					iPropertyID = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
					vCoords = mpProperties[iPropertyID].vBlipLocation[0]
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_RESPAWN_POINT - player was in property ") NET_PRINT_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) NET_NL()
					NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_RESPAWN_POINT - player was in property - vCoords = ") NET_PRINT_VECTOR(vCoords) NET_NL()
					#ENDIF	
				ENDIF			
				
				// move away from killer if valid (to help reduce griefing)
				PRINTLN("[spawning] g_iLastDamagerPlayer = ", g_iLastDamagerPlayer)
				IF (g_iLastDamagerPlayer > -1)
					IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, g_iLastDamagerPlayer), FALSE)
						IF IsPlayerConsideredEnemy(INT_TO_NATIVE(PLAYER_INDEX, g_iLastDamagerPlayer))
							g_SpawnData.PublicParams.vAvoidCoords[0] = GET_PLAYER_COORDS(INT_TO_NATIVE(PLAYER_INDEX, g_iLastDamagerPlayer))
							
							IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
								IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, g_iLastDamagerPlayer)))
									g_SpawnData.PublicParams.fAvoidRadius[0] = 250.0
								ELSE
									g_SpawnData.PublicParams.fAvoidRadius[0] = 125.0
								ENDIF
							ELSE
								IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, g_iLastDamagerPlayer)))
									g_SpawnData.PublicParams.fAvoidRadius[0] = 125.0
								ELSE
									g_SpawnData.PublicParams.fAvoidRadius[0] = 75.0
								ENDIF
							ENDIF
							
							PRINTLN("[spawning] GetSpawnLocationCoords  setting avoid coords [0] ", g_SpawnData.PublicParams.vAvoidCoords[0], ", with radius of ", g_SpawnData.PublicParams.fAvoidRadius[0])						
						ENDIF
					ENDIF
				ENDIF
				
				// if search point is in avoid sphere then move out
				REPEAT MAX_NUM_AVOID_RADIUS i 
					IF IsPointInsideSphere(vCoords, g_SpawnData.PublicParams.vAvoidCoords[i], g_SpawnData.PublicParams.fAvoidRadius[i], TRUE, TRUE)
						PRINTLN("[spawning] GetSpawnLocationCoords  moving search vec outside of avoid radius ", i)
						MovePointOutsideSphere(vCoords, g_SpawnData.PublicParams.vAvoidCoords[i], g_SpawnData.PublicParams.fAvoidRadius[i])
					ENDIF	
				ENDREPEAT
				IF IS_POINT_IN_ANGLED_AREA(vCoords, g_SpawnData.PublicParams.vAvoidAngledAreaPos1, g_SpawnData.PublicParams.vAvoidAngledAreaPos2, g_SpawnData.PublicParams.fAvoidAngledAreaWidth)
					PRINTLN("[spawning] GetSpawnLocationCoords  moving search vec outside of avoid angled area ", i)
					MovePointOutsideAngledArea(vCoords, g_SpawnData.PublicParams.vAvoidAngledAreaPos1, g_SpawnData.PublicParams.vAvoidAngledAreaPos2, g_SpawnData.PublicParams.fAvoidAngledAreaWidth)			
				ENDIF
				
				g_SpawnData.PrivateParams.vSearchCoord = vCoords			
				g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)	
								
				
				IF (g_SpawnData.MissionSpawnDetails.fMinDistFromEnemyNearDeath > -1.0)
					g_SpawnData.PublicParams.fMinDistFromPlayer = g_SpawnData.MissionSpawnDetails.fMinDistFromEnemyNearDeath
					PRINTLN("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_DEATH - g_SpawnData.PublicParams.fMinDistFromPlayer = ", g_SpawnData.PublicParams.fMinDistFromPlayer)
				ENDIF
				
				IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
					g_SpawnData.fNearExclusionRadius = g_SpawnData.MissionSpawnDetails.fMinDistFromDeathNearDeath 
				ELSE
					g_SpawnData.fNearExclusionRadius = 40.0 // 	
				ENDIF	
				
				fTemp = g_SpawnData.PublicParams.fMinDistFromPlayer - 65.0
				IF (fTemp < 0.0)
					fTemp = 0.0
				ENDIF
				
				IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
					g_SpawnData.PrivateParams.fSearchRadius = 120.0 + fTemp
				ELSE
					g_SpawnData.PrivateParams.fSearchRadius = 135.0 + fTemp // 85.0		
				ENDIF
				
				// set the size of the search radius
				g_SpawnData.PrivateParams.fSearchRadius = 95.0 + g_SpawnData.fNearExclusionRadius + fTemp
				
				IF (g_SpawnData.PrivateParams.fSearchRadius > 250.0)
					g_SpawnData.PrivateParams.fSearchRadius = 250.0
				ENDIF
				
				g_SpawnData.vExclusionCoord = g_SpawnData.vMyDeadCoords //g_SpawnData.PrivateParams.vSearchCoord // use actual dead coords so if we die inside appartment we wont spawn too far from it.
	

				
				// move raw coords away from enemy players
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_DEATH - g_SpawnData.PrivateParams.vSearchCoord before adjustment = ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord)  NET_NL()
				
				// the distance we will move away will be the greater of half search radius and fMinDistFromPlayer				
				fTemp = g_SpawnData.PrivateParams.fSearchRadius * 0.5
				IF fTemp < g_SpawnData.PublicParams.fMinDistFromPlayer
					fTemp = g_SpawnData.PublicParams.fMinDistFromPlayer
				ENDIF
				
				// get average enemy player position in current search radius
				vEnemyVec = GetAverageEnemyPositionInCircle(g_SpawnData.PrivateParams.vSearchCoord, fTemp)
				
				// vec from enemy av to death
				IF VMAG(vEnemyVec) > 0.0
					vEnemyVec.z = g_SpawnData.PrivateParams.vSearchCoord.z
					vec = g_SpawnData.PrivateParams.vSearchCoord - vEnemyVec
					vec /= VMAG(vec)
					vec *= fTemp					
					g_SpawnData.PrivateParams.vSearchCoord += vec	
				ENDIF
				
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_DEATH - g_SpawnData.PrivateParams.vSearchCoord after adjustment = ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord)  NET_NL()

				
				
				// if player died under a global exclusion area e.g. a tunnel underneath the army base then move the raw coords outside the exclusion area.
				IF IS_POINT_UNDER_GLOBAL_EXCLUSION_ZONE(g_SpawnData.PrivateParams.vSearchCoord, TRUE)
					NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_DEATH - was under a global exclusion area. moved out.") NET_NL()
				ENDIF					
				
				BOOL bForceUsingVehNodes
				IF NETWORK_IS_ACTIVITY_SESSION()
					INT iTeam
					INT iRule
					iRule = GET_CURRENT_MC_TEAM_AND_RULE_FROM_GLOBAL_DATA(iTeam)
					
					IF iRule < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_RESPAWN_IN_NEARBY_WATER_ON_FOOT)
						AND (NOT g_bMissionWasInVehWhenAlive OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_RESPAWN_IN_NEARBY_WATER_VEHICLE))
							bForceUsingVehNodes = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				// use road nodes if respawning in a vehicle
				IF IS_SPAWNING_IN_VEHICLE()
				OR bForceUsingVehNodes
				#IF FEATURE_FREEMODE_ARCADE
            	#IF FEATURE_COPS_N_CROOKS
            	OR (IS_FREEMODE_ARCADE() AND (GET_ARCADE_MODE() = ARC_COPS_CROOKS))
            	#ENDIF
        		#ENDIF
					g_SpawnData.PrivateParams.fSearchRadius = 400.0
					g_SpawnData.fNearExclusionRadius = 100.0	
					bForVehicle = TRUE	
					NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_DEATH - using car nodes") NET_NL()
				ENDIF
				
			BREAK	
			
			CASE SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU") NET_NL()
				#ENDIF
				IF NOT IS_PLAYER_DEAD(PLAYER_ID())
					vec = GET_ENTITY_COORDS(PLAYER_PED_ID())	
				ELSE
					vec = g_SpawnData.vMyDeadCoords // GetRandomPointDistFromPoint(g_SpawnData.vMyDeadCoords, 20.0)				
				ENDIF
				g_SpawnData.vExclusionCoord = vec 
				g_SpawnData.fNearExclusionRadius = 125.0
				
				// if player died under a global exclusion area e.g. a tunnel underneath the army base then move the raw coords outside the exclusion area.
				IF IS_POINT_UNDER_GLOBAL_EXCLUSION_ZONE(vec, TRUE)
					NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU - was under a global exclusion area. moved out.") NET_NL()
				ENDIF		
				
				// setup avoid coords so we dont spawn near the other player
				IF (g_bFM_ON_IMPROMPTU_DEATHMATCH)
					IF g_sImpromptuVars.playerImpromptuRival <> INVALID_PLAYER_INDEX()
						IF IS_NET_PLAYER_OK(g_sImpromptuVars.playerImpromptuRival, FALSE)	
							g_SpawnData.PublicParams.vAvoidCoords[0] = GET_PLAYER_COORDS(g_sImpromptuVars.playerImpromptuRival)
							IF IS_SPAWNING_IN_VEHICLE()
								g_SpawnData.PublicParams.fAvoidRadius[0] = 250.0
							ELSE
								g_SpawnData.PublicParams.fAvoidRadius[0] = 125.0
							ENDIF
							PRINTLN("[spawning] GetSpawnLocationCoords  setting avoid coords ", g_SpawnData.PublicParams.vAvoidCoords[0], ", with radius of ", g_SpawnData.PublicParams.fAvoidRadius[0])
						ENDIF
					ELSE
						PRINTLN("[spawning] GetSpawnLocationCoords playerImpromptuRival is Invalid!")
					ENDIF
				ENDIF
				
				// if search point is in avoid sphere then move out
				REPEAT MAX_NUM_AVOID_RADIUS i 
					IF IsPointInsideSphere(vec, g_SpawnData.PublicParams.vAvoidCoords[i], g_SpawnData.PublicParams.fAvoidRadius[i], TRUE, TRUE)
						PRINTLN("[spawning] GetSpawnLocationCoords  moving search vec outside of avoid radius ", i)
						MovePointOutsideSphere(vec, g_SpawnData.PublicParams.vAvoidCoords[i], g_SpawnData.PublicParams.fAvoidRadius[i])
					ENDIF
				ENDREPEAT
				IF IS_POINT_IN_ANGLED_AREA(vec, g_SpawnData.PublicParams.vAvoidAngledAreaPos1, g_SpawnData.PublicParams.vAvoidAngledAreaPos2, g_SpawnData.PublicParams.fAvoidAngledAreaWidth)
					PRINTLN("[spawning] GetSpawnLocationCoords  moving search vec outside of avoid angled area ", i)
					MovePointOutsideAngledArea(vec, g_SpawnData.PublicParams.vAvoidAngledAreaPos1, g_SpawnData.PublicParams.vAvoidAngledAreaPos2, g_SpawnData.PublicParams.fAvoidAngledAreaWidth)			
				ENDIF
				
				g_SpawnData.PrivateParams.vSearchCoord = vec
				g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)				
				g_SpawnData.PrivateParams.fSearchRadius = 250.0 // 85.0		
					
				
				// if player died inside an interior, then consider interiors, else dont. 
				IF (g_SpawnData.PublicParams.bConsiderInteriors)
					IF NOT IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(g_SpawnData.vMyDeadCoords))
						g_SpawnData.PublicParams.bConsiderInteriors = FALSE	
					ENDIF
				ENDIF				
				
				// use road nodes if respawning in a vehicle
				IF IS_SPAWNING_IN_VEHICLE()
					g_SpawnData.PrivateParams.fSearchRadius = 400.0
					g_SpawnData.fNearExclusionRadius = 100.0	
					bForVehicle = TRUE	
					NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU - using car nodes") NET_NL()
				ENDIF
				

				
			BREAK	
			
			CASE SPAWN_LOCATION_NEAR_TEAM_MATES		
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_TEAM_MATES") NET_NL()
				#ENDIF				
				g_SpawnData.PrivateParams.vSearchCoord = GetAverageTeammateCoord()
				g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)								
			BREAK
			
			
			CASE SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION") NET_NL()				
				IF GetNumActiveMissionSpawnAreas() = 0
					SCRIPT_ASSERT("GetSpawnLocationCoords - told to spawn in SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION but none setup!")
					PRINTLN("[spawning] GetSpawnLocationCoords - told to spawn in SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION but none setup!")
				ENDIF				
				#ENDIF			

				IF NOT IS_PLAYER_DEAD(PLAYER_ID())
					iNearest = GetClosestActiveMissionSpawnArea(GET_PLAYER_COORDS(PLAYER_ID()))
				ELSE
					iNearest = GetClosestActiveMissionSpawnArea(g_SpawnData.vMyDeadCoords)
				ENDIF
				
				SetSpawnDataRawInfoFromSPArea(g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest])
				
				IF NOT IS_PLAYER_DEAD(PLAYER_ID())
					g_SpawnData.PrivateParams.vOffsetOrigin = GET_PLAYER_COORDS(PLAYER_ID())
				ELSE
					g_SpawnData.PrivateParams.vOffsetOrigin = g_SpawnData.vMyDeadCoords
				ENDIF
				
				IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
					g_SpawnData.fNearExclusionRadius = g_SpawnData.MissionSpawnDetails.fMinDistFromDeathNearDeath 
				ELSE
					g_SpawnData.fNearExclusionRadius = 40.0 
				ENDIF	
				
				// we dont want to be too near to our death / current location
				g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vOffsetOrigin
				
									
				vInitial = g_SpawnData.PrivateParams.vOffsetOrigin
				
				
				SWITCH g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].iShape 
					CASE SPAWN_AREA_SHAPE_CIRCLE
						fAdditionalDist = g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].fFloat
						fAdditionalDist *= 0.5
					BREAK
					CASE SPAWN_AREA_SHAPE_BOX
						fAdditionalDist = VMAG(g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].vCoords1 - g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].vCoords2)
						fAdditionalDist *= 0.5
						fAdditionalDist *= 0.5					
					BREAK
					CASE SPAWN_AREA_SHAPE_ANGLED
						fAdditionalDist = g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].fFloat
						fAdditionalDist *= 0.5
						fAdditionalDist *= 0.5
					BREAK
				ENDSWITCH
				
				
				// is initial point not inside spawn area? move inside
				IF NOT IsPointInsideSpawnArea(vInitial, g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest])
					// passing in negative additional distance will move point inside spawn area, move them to half the radius width so we don't start the search from the extremities.
					PRINTLN("[spawning] GetSpawnLocationCoords - vInitial not inside spawn area, moving from ", vInitial)
					MovePointInsideSpawnArea(vInitial, g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest], fAdditionalDist )
					PRINTLN("[spawning] GetSpawnLocationCoords - vInitial not inside spawn area, moved to ", vInitial)
				ENDIF
				
				IF (g_SpawnData.MissionSpawnDetails.fMinDistFromEnemyNearDeath > -1.0)
					g_SpawnData.PublicParams.fMinDistFromPlayer = g_SpawnData.MissionSpawnDetails.fMinDistFromEnemyNearDeath
					PRINTLN("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION - set g_SpawnData.PublicParams.fMinDistFromPlayer to ", g_SpawnData.PublicParams.fMinDistFromPlayer) 
				ENDIF				
				
				// work out multiplier - under 200 use 1.5, then decline to 1.0 by 300m
				fTemp = 1.0
				IF (g_SpawnData.PublicParams.fMinDistFromPlayer < 200.0)
					fTemp = 1.5
				ELIF (g_SpawnData.PublicParams.fMinDistFromPlayer < 300.0)
					fTemp = 1.0 + (((300.0 - g_SpawnData.PublicParams.fMinDistFromPlayer) / 100.0) * 0.5)
				ENDIF
				fDistFromPlayer = g_SpawnData.PublicParams.fMinDistFromPlayer * fTemp 					
				PRINTLN("[spawning] GetSpawnLocationCoords -  fDistFromPlayer = ", fDistFromPlayer, " fTemp = ", fTemp, " g_SpawnData.PublicParams.fMinDistFromPlayer = ", g_SpawnData.PublicParams.fMinDistFromPlayer)	
				
				
				FLOAT fThisNearestEnemyDist
				iEnemiesNearOriginalPoint = NumberEnemiesNearPoint(vInitial, fDistFromPlayer, fThisNearestEnemyDist)
				
				PRINTLN("[spawning] GetSpawnLocationCoords - vInitial = ", vInitial)
				PRINTLN("[spawning] GetSpawnLocationCoords - iEnemiesNearOriginalPoint = ", iEnemiesNearOriginalPoint)
				PRINTLN("[spawning] GetSpawnLocationCoords - fThisNearestEnemyDist = ", fThisNearestEnemyDist)
				
				// if any near enemys the move point away
				IF iEnemiesNearOriginalPoint > 0
				OR (g_SpawnData.PrivateParams.bForAVehicle)
				
					IF (g_SpawnData.PrivateParams.bForAVehicle)
						
						PRINTLN("[spawning] GetSpawnLocationCoords - callin GetNearestCarNodeInsideAreaNoNearEnemy")
						
						// find the nearest road node that doesn't have an enemy within range
						vInitial = GetNearestCarNodeInsideAreaNoNearEnemy(vInitial, 
																			g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].vCoords1, 
																			g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].vCoords2, 
																			g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].fFloat, 
																			g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].iShape,
																			fDistFromPlayer,
																			TRUE,
																			g_SpawnData.PublicParams.bIsForFlyingVehicle,
																			TRUE, // active nodes only
																			g_SpawnData.PrivateParams.bFoundACarNode)	
						
						
						// if we didn't find an active node, then try with in-active nodes
						IF NOT (g_SpawnData.PrivateParams.bFoundACarNode)
							
							PRINTLN("[spawning] GetSpawnLocationCoords - didn't find any active nodes. Trying again with in-active")
						
							vInitial = GetNearestCarNodeInsideAreaNoNearEnemy(vInitial, 
																			g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].vCoords1, 
																			g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].vCoords2, 
																			g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].fFloat, 
																			g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest].iShape,
																			fDistFromPlayer,
																			TRUE,
																			g_SpawnData.PublicParams.bIsForFlyingVehicle,
																			FALSE, // include inactive nodes 
																			g_SpawnData.PrivateParams.bFoundACarNode)	
						ENDIF
												
						
						PRINTLN("[spawning] GetSpawnLocationCoords - after GetNearestCarNodeInsideAreaNoNearEnemy vInitial = ", vInitial, " g_SpawnData.PrivateParams.bFoundACarNode = ", g_SpawnData.PrivateParams.bFoundACarNode)
					
						g_SpawnData.fNearExclusionRadius = 0.0
						fMaxDistFromSearchPoint = 40.0
						PRINTLN("[spawning] GetSpawnLocationCoords - setting near exclusion to 0.0")
					
					ELSE
					
						bGotEnemyPoint = FALSE
					
						// get average position of enemies near point
						vEnemy = GetAveragePositionOfEnemiesNearPoint(vInitial, fDistFromPlayer)
						vEnemyFacing = GetAverageFacingVectorOfEnemiesNearPoint(vInitial, fDistFromPlayer)
						
						vAwayFromEnemy = vInitial - vEnemy
						vAwayFromEnemy /= VMAG(vAwayFromEnemy)
						vAwayFromEnemy *= (fDistFromPlayer)
																
						// find 4 points away from enemy
						REPEAT 4 i
							vec = vAwayFromEnemy
							RotateVec(vec, <<0.0, 0.0, (TO_FLOAT(i)* 90.0)>>)
							vAwayFromEnemyPoint[i] = vEnemy + vec	
							
							// is this point outside spawn area? move inside
							IF NOT IsPointInsideSpawnArea(vAwayFromEnemyPoint[i], g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest])
								MovePointInsideSpawnArea(vAwayFromEnemyPoint[i], g_SpawnData.MissionSpawnDetails.SpawnArea[iNearest] )
							ENDIF
							
							// count number of enemies near point
							iNumberOfEnemiesNearPoint[i] = NumberEnemiesNearPoint(vAwayFromEnemyPoint[i], fDistFromPlayer, fNearestEnemyDist[i])
							
							// is new point facing the oncoming enemies?
							vec = vEnemy - vAwayFromEnemyPoint[i]
							IF DOT_PRODUCT(vec, vEnemyFacing) < 0
								bEnemiesFacingPoint[i] = TRUE
							ELSE
								bEnemiesFacingPoint[i] = FALSE
							ENDIF

							PRINTLN("[spawning] GetSpawnLocationCoords - vAwayFromEnemyPoint[", i, "] = ", vAwayFromEnemyPoint[i])
							PRINTLN("[spawning] GetSpawnLocationCoords - iNumberOfEnemiesNearPoint[", i, "] = ", iNumberOfEnemiesNearPoint[i])
							PRINTLN("[spawning] GetSpawnLocationCoords - bEnemiesFacingPoint[", i, "] = ", bEnemiesFacingPoint[i])
							
						ENDREPEAT
						
						// we want to choose a point that has no enemies near and not facing point
						REPEAT 4 i
							IF (iNumberOfEnemiesNearPoint[i] = 0)
							AND (bEnemiesFacingPoint[i] = FALSE)
							AND NOT (bGotEnemyPoint)
								vInitial = vAwayFromEnemyPoint[i]
								PRINTLN("[spawning] GetSpawnLocationCoords - choosing point ", i, " no enemies near and not facing them.")
								bGotEnemyPoint = TRUE 
							ENDIF
						ENDREPEAT
						
						// if not choose point that has no enemies but is facing them
						IF NOT (bGotEnemyPoint)
							REPEAT 4 i
								IF (iNumberOfEnemiesNearPoint[i] = 0)
								AND NOT (bGotEnemyPoint)
									vInitial = vAwayFromEnemyPoint[i]
									PRINTLN("[spawning] GetSpawnLocationCoords - choosing point ", i, " no enemies near but facing them.")
									bGotEnemyPoint = TRUE 
								ENDIF
							ENDREPEAT
						ENDIF
						
						// if not choose point with fewest enemies
						IF NOT (bGotEnemyPoint)
							iFewestEnemies = 999
							iStored = -1
							REPEAT 4 i
								IF (iNumberOfEnemiesNearPoint[i] < iFewestEnemies)
									iStored = i
									fHighestStoredEnemyDist = fNearestEnemyDist[i]
									iFewestEnemies = iNumberOfEnemiesNearPoint[i] 
									PRINTLN("[spawning] GetSpawnLocationCoords - fewer enemies, storing ", iFewestEnemies, fHighestStoredEnemyDist)
								ELSE
									IF (iNumberOfEnemiesNearPoint[i] = iFewestEnemies)	
										IF (fNearestEnemyDist[i] > fHighestStoredEnemyDist)
											fHighestStoredEnemyDist = fNearestEnemyDist[i]
											iStored = i	
											PRINTLN("[spawning] GetSpawnLocationCoords - same enemies but further, storing ", iFewestEnemies, fHighestStoredEnemyDist)
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
							IF NOT (iStored = -1)							
							AND (iFewestEnemies < iEnemiesNearOriginalPoint)
								vInitial = vAwayFromEnemyPoint[iStored]
								bGotEnemyPoint = TRUE 	
								PRINTLN("[spawning] GetSpawnLocationCoords - choosing point ", iStored, " fewer enemies near point than initial.")
							ENDIF
						ENDIF
						
						IF NOT (bGotEnemyPoint)
							PRINTLN("[spawning] GetSpawnLocationCoords - couldn't find a better point sticking with vInitial.")
						ELSE
							g_SpawnData.fNearExclusionRadius = 0.0 
							PRINTLN("[spawning] GetSpawnLocationCoords - setting near exclusion to 0.0")
						ENDIF
							
					ENDIF
					
				ELSE
					
					PRINTLN("[spawning] GetSpawnLocationCoords - no enemies near vInitial")
					
				ENDIF
				
				g_SpawnData.PrivateParams.vOffsetOrigin = vInitial
				PRINTLN("[spawning] GetSpawnLocationCoords - g_SpawnData.PrivateParams.vOffsetOrigin = ", g_SpawnData.PrivateParams.vOffsetOrigin)
				
				g_SpawnData.PrivateParams.bUseOffsetOrigin = TRUE	
				g_SpawnData.PublicParams.bCloseToOriginAsPossible = TRUE
				g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)

			BREAK
		
			
			CASE SPAWN_LOCATION_MISSION_AREA
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_MISSION_AREA") NET_NL()
				
				IF GetNumActiveMissionSpawnAreas() = 0
					SCRIPT_ASSERT("GetSpawnLocationCoords - told to spawn in SPAWN_LOCATION_MISSION_AREA but none setup!")
					PRINTLN("[spawning] GetSpawnLocationCoords - told to spawn in SPAWN_LOCATION_MISSION_AREA but none setup!")
				ENDIF
				
				#ENDIF
				//GetPointInMyMissionSpawnArea(g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading)
				
				
				i = GET_RANDOM_INT_IN_RANGE(0, GetNumActiveMissionSpawnAreas())
				
				SetSpawnDataRawInfoFromSPArea(g_SpawnData.MissionSpawnDetails.SpawnArea[i])
						
				// is spawn area centre point in a global exclusion area arleady?
				IF IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(g_SpawnData.PrivateParams.vSearchCoord)
					g_SpawnData.bIgnoreGlobalExclusionCheck = TRUE
				ENDIF
			BREAK
			
			
			CASE SPAWN_LOCATION_NEAREST_RESPAWN_POINT
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAREST_RESPAWN_POINT") NET_NL()
				#ENDIF	
									
				
				IF IS_PLAYER_DEAD(PLAYER_ID())
					vCoords = g_SpawnData.vMyDeadCoords
				ELSE
					vCoords = GET_PLAYER_COORDS(PLAYER_ID())	
				ENDIF				
				
				// if player is inside a property
				IF (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0)
					iPropertyID = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
					vCoords = mpProperties[iPropertyID].vBlipLocation[0]
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_RESPAWN_POINT - player was in property ") NET_PRINT_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) NET_NL()
					NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_RESPAWN_POINT - player was in property - vCoords = ") NET_PRINT_VECTOR(vCoords) NET_NL()
					#ENDIF	
				ENDIF
				
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_RESPAWN_POINT - vCoords = ") NET_PRINT_VECTOR(vCoords) NET_NL()
				#ENDIF		
				
				// find nearest hospital, respawn point, property			
				fDistNearestHospital = GetDistanceToClosestActiveSpawnAreaInArray(vCoords, SpawnAreas_Hospitals, iNearestHospital)
				
				IF (GET_OWNED_PROPERTY(0) > 0)
					i = 0
					REPEAT MAX_OWNED_PROPERTIES i
						iPropertyID = GET_OWNED_PROPERTY(i) 
						IF (mpProperties[iPropertyID].iType = PROPERTY_TYPE_GARAGE_HOUSE)
						OR (mpProperties[iPropertyID].iType = PROPERTY_TYPE_OFFICE)
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_RESPAWN_POINT - my property coords = ") NET_PRINT_VECTOR(mpProperties[iPropertyID].vBlipLocation[0]) NET_NL()
							#ENDIF	
							IF fDistNearestProperty = 0 
							OR fDistNearestProperty > VDIST(vCoords, mpProperties[iPropertyID].vBlipLocation[0])
								fDistNearestProperty =  VDIST(vCoords, mpProperties[iPropertyID].vBlipLocation[0])	
								iNearestProperty = iPropertyID
							ENDIF
						ELSE
							IF fDistNearestProperty = 0 
								fDistNearestProperty = 999999.9
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					fDistNearestProperty = 999999.9
				ENDIF
				
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - fDistNearestHospital = ") NET_PRINT_FLOAT(fDistNearestHospital) 
				NET_PRINT(", iNearestHospital = ") NET_PRINT_INT(iNearestHospital)
				NET_PRINT(", fDistNearestProperty = ") NET_PRINT_FLOAT(fDistNearestProperty)
				NET_PRINT(", iNearestProperty = ") NET_PRINT_INT(iNearestProperty)
				NET_NL()
				#ENDIF	
				
				// if property is nearest
				IF fDistNearestProperty < 10000
				AND (fDistNearestProperty < fDistNearestHospital) 
				

					SWITCH mpProperties[iNearestProperty].iType					
						CASE PROPERTY_TYPE_GARAGE_HOUSE														
							GetSpawnForProperty(iNearestProperty, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading) 
							g_SpawnData.bSpawningInGarage = FALSE
						BREAK
						CASE PROPERTY_TYPE_GARAGE			
							g_SpawnData.PrivateParams.vSearchCoord = mpProperties[iNearestProperty].garage.vInPlayerLoc
							g_SpawnData.PrivateParams.fRawHeading = mpProperties[iNearestProperty].garage.fInPlayerHeading
							IF (g_SpawnData.iWarpServerRequestCount = 0)
								g_SpawnData.bSpawningInGarage = TRUE
							ENDIF
							g_SpawnData.bDontAskPermission = TRUE // no need to ask server for permission as only a single point
						BREAK						
					ENDSWITCH
					
					bUsesFixedPoints = TRUE
					PRINTLN("SPAWN_LOCATION_NEAREST_RESPAWN_POINT- nearest point is property so setting g_SpawnData.bDisableFadeInAndTurnOnControls")
					
					IF (g_SpawnData.iWarpServerRequestCount = 0)
						g_SpawnData.bSpawningInProperty	= TRUE
						PRINTLN("SPAWN_LOCATION_NEAREST_RESPAWN_POINT - g_SpawnData.bSpawningInProperty = TRUE - A")
					ENDIF
					g_SpawnData.bDisableFadeInAndTurnOnControls = TRUE				
						
										

				// if hospital is nearest
				ELIF (fDistNearestHospital < fDistNearestProperty)
					IF (iNearestHospital > -1)
						SetSpawnDataRawInfoFromSPArea(SpawnAreas_Hospitals[iNearestHospital])
					ELSE
						SCRIPT_ASSERT("SPAWN_LOCATION_NEAREST_RESPAWN_POINT - iNearestHospital = -1") 
						SetSpawnDataRawInfoFromSPArea(SpawnAreas_Hospitals[0])
					ENDIF
					g_SpawnData.bSpawnedInHospital = TRUE					
				ENDIF								

				g_SpawnData.bIgnoreExclusionCheck = TRUE
				
			BREAK	
			
			CASE SPAWN_LOCATION_NEAREST_HOSPITAL
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAREST_HOSPITAL") NET_NL()
				#ENDIF	
										
				IF IS_PLAYER_DEAD(PLAYER_ID())
					vCoords = g_SpawnData.vMyDeadCoords
				ELSE
					vCoords = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID())	
				ENDIF				

				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_HOSPITAL - vCoords = ") NET_PRINT_VECTOR(vCoords) NET_NL()
				#ENDIF		
				
				// find nearest hospital, respawn point, property			
				fDistNearestHospital = GetDistanceToClosestActiveSpawnAreaInArray(vCoords, SpawnAreas_Hospitals, iNearestHospital)

				IF (iNearestHospital > -1)
					SetSpawnDataRawInfoFromSPArea(SpawnAreas_Hospitals[iNearestHospital])
				ELSE
					SCRIPT_ASSERT("SPAWN_LOCATION_NEAREST_HOSPITAL - iNearestHospital = -1") 
					SetSpawnDataRawInfoFromSPArea(SpawnAreas_Hospitals[0])
				ENDIF
					
				g_SpawnData.bSpawnedInHospital = TRUE					
				g_SpawnData.bIgnoreExclusionCheck = TRUE	
				
			BREAK
			
			CASE SPAWN_LOCATION_NEAREST_HOTEL_TO_SPECIFIC_COORDS
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAREST_HOTEL_TO_SPECIFIC_COORDS") NET_NL()
				#ENDIF	
									

				vCoords = g_SpecificSpawnLocation.vCoords
			
				

				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_HOTEL_TO_SPECIFIC_COORDS - vCoords = ") NET_PRINT_VECTOR(vCoords) NET_NL()
				#ENDIF		
				
				// find nearest hospital, respawn point, property			
				fDistNearestProperty = GetDistanceToClosestActiveSpawnAreaInArray(vCoords, SpawnAreas_Hotels, iNearestProperty)


				IF (iNearestProperty > -1)
					SetSpawnDataRawInfoFromSPArea(SpawnAreas_Hotels[iNearestProperty])
				ELSE
					SCRIPT_ASSERT("SPAWN_LOCATION_NEAREST_HOTEL_TO_SPECIFIC_COORDS - iNearestProperty = -1") 
					SetSpawnDataRawInfoFromSPArea(SpawnAreas_Hotels[0])
				ENDIF
					
								
				g_SpawnData.bIgnoreExclusionCheck = TRUE
				
			BREAK
			
			CASE SPAWN_LOCATION_NEAREST_POLICE_STATION
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAREST_POLICE_STATION") NET_NL()
				#ENDIF	
										

				IF IS_PLAYER_DEAD(PLAYER_ID())
					vCoords = g_SpawnData.vMyDeadCoords
				ELSE
					vCoords = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID())
				ENDIF				
				

				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_POLICE_STATION - vCoords = ") NET_PRINT_VECTOR(vCoords) NET_NL()
				#ENDIF		
				
				// find nearest hospital, respawn point, property			
				fDistNearestHospital = GetDistanceToClosestActiveSpawnAreaInArray(vCoords, SpawnAreas_PoliceStation, iNearestHospital)


				IF (iNearestHospital > -1)
					SetSpawnDataRawInfoFromSPArea(SpawnAreas_PoliceStation[iNearestHospital])
				ELSE
					SCRIPT_ASSERT("SPAWN_LOCATION_NEAREST_POLICE_STATION - iNearestHospital = -1") 
					SetSpawnDataRawInfoFromSPArea(SpawnAreas_PoliceStation[0])
				ENDIF
					
								
				g_SpawnData.bIgnoreExclusionCheck = TRUE
				
			BREAK			
			
			CASE SPAWN_LOCATION_NEAREST_RESPAWN_POINT_TO_SPECIFIC_COORDS
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAREST_RESPAWN_POINT_TO_SPECIFIC_COORDS") NET_NL()
				#ENDIF	
				
				vCoords = g_SpecificSpawnLocation.vCoords
								
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_RESPAWN_POINT_TO_SPECIFIC_COORDS - vCoords = ") NET_PRINT_VECTOR(vCoords) NET_NL()
				#ENDIF		
				
				// find nearest hospital, respawn point, property			
				fDistNearestHospital = GetDistanceToClosestActiveSpawnAreaInArray(vCoords, SpawnAreas_Hospitals, iNearestHospital)
				
				IF (GET_OWNED_PROPERTY(0) > 0)
					i = 0
					REPEAT MAX_OWNED_PROPERTIES i
						iPropertyID = GET_OWNED_PROPERTY(i) 
						IF (mpProperties[iPropertyID].iType = PROPERTY_TYPE_GARAGE_HOUSE)
						OR (mpProperties[iPropertyID].iType = PROPERTY_TYPE_OFFICE)
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAREST_RESPAWN_POINT - my property coords = ") NET_PRINT_VECTOR(mpProperties[iPropertyID].vBlipLocation[0]) NET_NL()
							#ENDIF	
							IF fDistNearestProperty = 0 
							OR fDistNearestProperty > VDIST(vCoords, mpProperties[iPropertyID].vBlipLocation[0])
								fDistNearestProperty =  VDIST(vCoords, mpProperties[iPropertyID].vBlipLocation[0])	
								iNearestProperty = iPropertyID
							ENDIF
						ELSE
							IF fDistNearestProperty = 0 
								fDistNearestProperty = 999999.9
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					fDistNearestProperty = 999999.9
				ENDIF
				
				
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - fDistNearestHospital = ") NET_PRINT_FLOAT(fDistNearestHospital) 
				NET_PRINT(", iNearestHospital = ") NET_PRINT_INT(iNearestHospital)
				NET_PRINT(", fDistNearestProperty = ") NET_PRINT_FLOAT(fDistNearestProperty)
				NET_PRINT(", iNearestProperty = ") NET_PRINT_INT(iNearestProperty)
				NET_NL()
				#ENDIF	
				
				// if property is nearest
				IF fDistNearestProperty < 10000
				AND (fDistNearestProperty < fDistNearestHospital) 
				
					SWITCH mpProperties[iNearestProperty].iType					
						CASE PROPERTY_TYPE_GARAGE_HOUSE														
							GetSpawnForProperty(iNearestProperty, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading) 
							g_SpawnData.bSpawningInGarage = FALSE
						BREAK
						CASE PROPERTY_TYPE_GARAGE	
							IF (g_SpawnData.iWarpServerRequestCount = 0)
								g_SpawnData.bSpawningInGarage = TRUE
							ENDIF
							g_SpawnData.PrivateParams.vSearchCoord = mpProperties[iNearestProperty].garage.vInPlayerLoc
							g_SpawnData.PrivateParams.fRawHeading = mpProperties[iNearestProperty].garage.fInPlayerHeading
							g_SpawnData.bDontAskPermission = TRUE // no need to ask server for permission as only a single point
						BREAK						
					ENDSWITCH
										
					bUsesFixedPoints = TRUE
					PRINTLN("SPAWN_LOCATION_NEAREST_RESPAWN_POINT- nearest point is property so setting g_SpawnData.bDisableFadeInAndTurnOnControls")
					IF (g_SpawnData.iWarpServerRequestCount = 0)
						g_SpawnData.bSpawningInProperty	= TRUE
						PRINTLN("SPAWN_LOCATION_NEAREST_RESPAWN_POINT - g_SpawnData.bSpawningInProperty = TRUE - B")
					ENDIF
					g_SpawnData.bDisableFadeInAndTurnOnControls = TRUE

				// if hospital is nearest
				ELIF (fDistNearestHospital < fDistNearestProperty) 
					IF (iNearestHospital > -1)
						SetSpawnDataRawInfoFromSPArea(SpawnAreas_Hospitals[iNearestHospital])
					ELSE
						SCRIPT_ASSERT("SPAWN_LOCATION_NEAREST_RESPAWN_POINT_TO_SPECIFIC_COORDS - iNearestHospital = -1") 
						SetSpawnDataRawInfoFromSPArea(SpawnAreas_Hospitals[0])
					ENDIF
					g_SpawnData.bSpawnedInHospital = TRUE	
				ENDIF								

				g_SpawnData.bIgnoreExclusionCheck = TRUE
				
			BREAK	
			
			
			CASE SPAWN_LOCATION_INSIDE_GARAGE	
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_INSIDE_GARAGE") NET_NL()
				#ENDIF
				
				IF g_Spawn_RespawnFromStoreLastProperty > 0
					iOwnedProperty = g_Spawn_RespawnFromStoreLastProperty
				ELSE
					iOwnedProperty = GET_LAST_USED_PROPERTY()
				ENDIF
				
				//reset after spawn
				g_Spawn_RespawnFromStoreLastProperty = 0
				PRINTLN("SPAWN_LOCATION_INSIDE_GARAGE g_Spawn_RespawnFromStoreLastProperty = 0")
				
				IF NOT (iOwnedProperty > 0)
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_INSIDE_GARAGE - but player doesn't own a property")
					#ENDIF
				ENDIF
				
				iPropertyID = iOwnedProperty

				g_SpawnData.PrivateParams.vSearchCoord = mpProperties[iPropertyID].garage.vInPlayerLoc 
				g_SpawnData.PrivateParams.fRawHeading = mpProperties[iPropertyID].garage.fInPlayerHeading
				g_SpawnData.bDontAskPermission = TRUE // no need to ask server for permission as only a single point

				bUsesFixedPoints = TRUE
				g_SpawnData.bIgnoreExclusionCheck = TRUE	
				
				IF (g_SpawnData.iWarpServerRequestCount = 0)
					g_SpawnData.bSpawningInGarage = TRUE
				ENDIF
				PRINTLN("SPAWN_LOCATION_INSIDE_GARAGE- nearest point is property so setting g_SpawnData.bDisableFadeInAndTurnOnControls")
				IF (g_SpawnData.iWarpServerRequestCount = 0)
					g_SpawnData.bSpawningInProperty	= TRUE
					PRINTLN("SPAWN_LOCATION_INSIDE_GARAGE - g_SpawnData.bSpawningInProperty = TRUE - C")
				ENDIF
				g_SpawnData.bDisableFadeInAndTurnOnControls = TRUE
			BREAK
						
			CASE SPAWN_LOCATION_INSIDE_PROPERTY
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_INSIDE_PROPERTY") NET_NL()
				#ENDIF
				
				IF g_Spawn_RespawnFromStoreLastProperty > 0
					iOwnedProperty = g_Spawn_RespawnFromStoreLastProperty
				ELSE
					iOwnedProperty = GET_LAST_USED_PROPERTY()
				ENDIF
				//reset after spawn
				g_Spawn_RespawnFromStoreLastProperty = 0
				PRINTLN("SPAWN_LOCATION_INSIDE_PROPERTY g_Spawn_RespawnFromStoreLastProperty = 0")
				
				
				IF iOwnedProperty <= 0
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_INSIDE_PROPERTY - but player doesn't own a property")
					#ENDIF
				ENDIF
				
				iPropertyID = iOwnedProperty
				NET_PRINT("[spawning] GetSpawnLocationCoords - mpProperties[iPropertyID].iType	= ") NET_PRINT_INT(mpProperties[iPropertyID].iType)  NET_NL()
				SWITCH mpProperties[iPropertyID].iType					
					CASE PROPERTY_TYPE_GARAGE_HOUSE	
					CASE PROPERTY_TYPE_OFFICE
						#IF FEATURE_GEN9_EXCLUSIVE
						IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_HEIST_DEEPLINK
							MP_PROP_OFFSET_STRUCT tempOffset
							GET_POSITION_AS_OFFSET_FOR_PROPERTY(iOwnedProperty ,MP_PROP_ELEMENT_HEIST_PLANNING_POST_MISSION_APT_POS_0, tempOffset, GET_BASE_PROPERTY_FROM_PROPERTY(iOwnedProperty))
							g_SpawnData.PrivateParams.vSearchCoord = tempOffset.vLoc
							g_SpawnData.PrivateParams.fRawHeading = tempOffset.vRot.z
							g_SpawnData.bSpawningInGarage = FALSE
							PRINTLN("[spawning] GetSpawnLocationCoords: SPAWN_ACTIVITY_HEIST_DEEPLINK overriding apartment interior spawn: vLoc: ",tempOffset.vLoc," fHeading: ",g_SpawnData.PrivateParams.fRawHeading)
						ELSE
						#ENDIF
						GetSpawnForProperty(iPropertyID, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading)	
						g_SpawnData.bSpawningInGarage = FALSE
						#IF FEATURE_GEN9_EXCLUSIVE
						ENDIF
						#ENDIF
					BREAK
					CASE PROPERTY_TYPE_GARAGE	
					CASE PROPERTY_TYPE_OFFICE_GARAGE
						IF (g_SpawnData.iWarpServerRequestCount = 0)
							g_SpawnData.bSpawningInGarage = TRUE
						ENDIF
						g_SpawnData.PrivateParams.vSearchCoord = mpProperties[iPropertyID].garage.vInPlayerLoc
						g_SpawnData.PrivateParams.fRawHeading = mpProperties[iPropertyID].garage.fInPlayerHeading
						g_SpawnData.bDontAskPermission = TRUE // no need to ask server for permission as only a single point
					BREAK
				ENDSWITCH
					
				bUsesFixedPoints = TRUE
				g_SpawnData.bIgnoreExclusionCheck = TRUE
				
				
				PRINTLN("SPAWN_LOCATION_INSIDE_PROPERTY- nearest point is property so setting g_SpawnData.bDisableFadeInAndTurnOnControls")
				IF (g_SpawnData.iWarpServerRequestCount = 0)
					g_SpawnData.bSpawningInProperty	= TRUE
					PRINTLN("SPAWN_LOCATION_INSIDE_PROPERTY - g_SpawnData.bSpawningInProperty = TRUE - D")
				ENDIF
				g_SpawnData.bDisableFadeInAndTurnOnControls = TRUE
			BREAK
			
			CASE SPAWN_LOCATION_GANG_BOSS_PRIVATE_YACHT
				
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_GANG_BOSS_PRIVATE_YACHT") NET_NL()
				#ENDIF
				
				IF IS_LOCAL_PLAYERS_GANG_BOSS_ON_THEIR_OWN_PRIVATE_YACHT()
					iYachtID = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				
					IF iYachtID <= 0
						#IF IS_DEBUG_BUILD
							SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_GANG_BOSS_PRIVATE_YACHT - invalid yacht id!")
						#ENDIF
					ENDIF
					
					GetSpawnForYachtExterior(iYachtID, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading)
					
					bUsesFixedPoints = TRUE
					g_SpawnData.bIgnoreExclusionCheck = TRUE				
					
				ELSE
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_GANG_BOSS_PRIVATE_YACHT - boss is not on yacht!")
					#ENDIF
					g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_PERCEIVED_COORDS(GB_GET_LOCAL_PLAYER_GANG_BOSS())
					g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)				
				ENDIF
			
			BREAK
			
			CASE SPAWN_LOCATION_PRIVATE_YACHT
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_PRIVATE_YACHT") NET_NL()
				#ENDIF
				
				iYachtID = LOCAL_PLAYER_PRIVATE_YACHT_ID()
				
				IF iYachtID <= 0
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_PRIVATE_YACHT - player doesn't have valid yacht id!")
					#ENDIF
				ENDIF
				
				GetSpawnForYachtExterior(iYachtID, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading)
				
				bUsesFixedPoints = TRUE
				g_SpawnData.bIgnoreExclusionCheck = TRUE				
				
			BREAK
			
			CASE SPAWN_LOCATION_PRIVATE_FRIEND_YACHT
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_PRIVATE_FRIEND_YACHT") NET_NL()
				#ENDIF
				
				IF g_SpawnData.iYachtToWarpTo < 0
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_PRIVATE_FRIEND_YACHT - iYachtToWarpTo not valid yacht id!")
					#ENDIF
				ENDIF
				
				GetSpawnForYachtExterior(g_SpawnData.iYachtToWarpTo, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading)
				
				bUsesFixedPoints = TRUE
				g_SpawnData.bIgnoreExclusionCheck = TRUE				
				
			BREAK			
			
			CASE SPAWN_LOCATION_PRIVATE_YACHT_NEAR_SHORE
			
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_PRIVATE_YACHT_NEAR_SHORE") NET_NL()
				#ENDIF
				
				IF g_SpawnData.iYachtToWarpTo < 0
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_PRIVATE_YACHT_NEAR_SHORE - iYachtToWarpTo not valid yacht id!")
					#ENDIF
				ENDIF
				
				GET_PRIVATE_YACHT_SPAWN_VEHICLE_LOCATION(0, g_SpawnData.iYachtToWarpTo, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading, TRUE)
			
				g_SpawnData.PrivateParams.fSearchRadius = 150.0
				g_SpawnData.PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE
				g_SpawnData.PrivateParams.vAngledAreaPoint1 = <<0.0, 0.0, 0.0>>
				g_SpawnData.PrivateParams.vAngledAreaPoint2 = <<0.0, 0.0, 0.0>>
				g_SpawnData.PrivateParams.fAngledAreaWidth = 0.0			
				g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord // the exclusion coords will move the search start point, the vAvoidCoords will avoid points returned in area.
				g_SpawnData.fNearExclusionRadius = 12.0
				g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord
				g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
				g_SpawnData.PublicParams.bCloseToOriginAsPossible = TRUE
				//g_SpawnData.PublicParams.vFacingCoords = g_SpecificSpawnLocation.vCoords			
				g_SpawnData.PublicParams.bPreferPointsCloserToRoads = TRUE
				g_SpawnData.PublicParams.bConsiderInteriors = FALSE
				g_SpawnData.PrivateParams.bDoVisibleChecks = TRUE
				g_SpawnData.PublicParams.fMinDistFromPlayer = 5.0
				g_SpawnData.PrivateParams.bAllowFallbackToUseInteriors = FALSE				
				g_SpawnData.bIgnoreGlobalExclusionCheck = FALSE		
			
			BREAK
			
			CASE SPAWN_LOCATION_PRIVATE_YACHT_APARTMENT
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_PRIVATE_YACHT_APARTMENT") NET_NL()
				#ENDIF
				
				iYachtID = LOCAL_PLAYER_PRIVATE_YACHT_ID()
				
				NET_PRINT("[spawning] GetSpawnLocationCoords - mpYachts[iYachtID].yachtPropertyDetails.iType	= ") NET_PRINT_INT(mpYachts[iYachtID].yachtPropertyDetails.iType)  NET_NL()
				SWITCH mpYachts[iYachtID].yachtPropertyDetails.iType
					CASE PROPERTY_TYPE_YACHT
					
						SWITCH GET_SPAWN_ACTIVITY()
							CASE SPAWN_ACTIVITY_SHOWER
								g_SpawnData.PrivateParams.vSearchCoord = mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_SHOWER].vTriggerVec
								g_SpawnData.PrivateParams.fRawHeading = mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_SHOWER].fTriggerRot
								NET_PRINT("[spawning] GetSpawnLocationCoords - spawning in shower at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							BREAK
							CASE SPAWN_ACTIVITY_BED
								g_SpawnData.PrivateParams.vSearchCoord = mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED].vTriggerVec
								g_SpawnData.PrivateParams.fRawHeading = mpYachts[iYachtID].yachtPropertyDetails.house.activity[SAFEACT_BED].fTriggerRot
								NET_PRINT("[spawning] GetSpawnLocationCoords - spawning in bed at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							BREAK
							DEFAULT
								GetSpawnForYachtApartment(iYachtID, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading)
							BREAK
						ENDSWITCH
					
							
						g_SpawnData.bSpawningInGarage = FALSE
					BREAK
				ENDSWITCH
				//set flag so others know you are trying to spawn in property
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
				bUsesFixedPoints = TRUE
				g_SpawnData.bIgnoreExclusionCheck = TRUE
				
				PRINTLN("SPAWN_LOCATION_PRIVATE_YACHT_APARTMENT- nearest point is property so setting g_SpawnData.bDisableFadeInAndTurnOnControls")
				IF (g_SpawnData.iWarpServerRequestCount = 0)
					g_SpawnData.bSpawningInProperty	= TRUE					
					PRINTLN("SPAWN_LOCATION_PRIVATE_YACHT_APARTMENT - g_SpawnData.bSpawningInProperty = TRUE - E")
				ENDIF
				g_SpawnData.bSpawningInYacht = TRUE
				g_SpawnData.bDisableFadeInAndTurnOnControls = TRUE			
			BREAK
			
			CASE SPAWN_LOCATION_INSIDE_PROPERTY_OR_GARAGE

				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_INSIDE_PROPERTY_OR_GARAGE") NET_NL()
				#ENDIF
				
				iOwnedProperty = GET_LAST_USED_PROPERTY()
				PRINTLN("g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastAccessedPropertyID = ",g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastAccessedPropertyID)
				IF iOwnedProperty <= 0
				#IF IS_DEBUG_BUILD
					PRINTLN("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_INSIDE_PROPERTY - but player doesn't own a property")
					SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_INSIDE_PROPERTY - but player doesn't own a property")
				ELSE
					PRINTLN("[spawning] GetSpawnLocationCoords - spawn player in their owned property # ",iOwnedProperty)
				#ENDIF
				ENDIF
				
				// Temp fix for 2185620 - [NT][Array Overrun][script] Error: Assertf(0) FAILED: maintransition : Program Counter = 1047096 - Check the .scd file :Array overrun 
				IF iOwnedProperty > MAX_MP_PROPERTIES
					iOwnedProperty = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastAccessedPropertyID
					
					PRINTLN("[spawning] GetSpawnLocationCoords - owned property out of range, override to # ",iOwnedProperty)
					SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_INSIDE_PROPERTY - owned property out of range")
				ENDIF
				IF iOwnedProperty > MAX_MP_PROPERTIES
					iOwnedProperty = 0
					
					PRINTLN("[spawning] GetSpawnLocationCoords - owned property still out of range, override to # ",iOwnedProperty)
					SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_INSIDE_PROPERTY - owned property still out of range")
				ENDIF
				
				iPropertyID = iOwnedProperty 
				NET_PRINT("[spawning] GetSpawnLocationCoords - mpProperties[iPropertyID].iType	= ") NET_PRINT_INT(mpProperties[iPropertyID].iType)  NET_NL()
				SWITCH mpProperties[iPropertyID].iType
					CASE PROPERTY_TYPE_GARAGE_HOUSE
					CASE PROPERTY_TYPE_OFFICE
						SWITCH GET_SPAWN_ACTIVITY()
							CASE SPAWN_ACTIVITY_SHOWER
								g_SpawnData.PrivateParams.vSearchCoord = mpProperties[iPropertyID].house.activity[SAFEACT_SHOWER].vTriggerVec
								g_SpawnData.PrivateParams.fRawHeading = mpProperties[iPropertyID].house.activity[SAFEACT_SHOWER].fTriggerRot
								NET_PRINT("[spawning] GetSpawnLocationCoords - spawning in shower at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							BREAK
							CASE SPAWN_ACTIVITY_BED
								g_SpawnData.PrivateParams.vSearchCoord = mpProperties[iPropertyID].house.activity[SAFEACT_BED].vTriggerVec
								g_SpawnData.PrivateParams.fRawHeading = mpProperties[iPropertyID].house.activity[SAFEACT_BED].fTriggerRot
								NET_PRINT("[spawning] GetSpawnLocationCoords - spawning in bed at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							BREAK
							DEFAULT
								GetSpawnForProperty(iOwnedProperty, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading)	
							BREAK
						ENDSWITCH
					
							
						g_SpawnData.bSpawningInGarage = FALSE
					BREAK
					CASE PROPERTY_TYPE_GARAGE
					CASE PROPERTY_TYPE_OFFICE_GARAGE
						IF (g_SpawnData.iWarpServerRequestCount = 0)
							g_SpawnData.bSpawningInGarage = TRUE
						ENDIF
						g_SpawnData.PrivateParams.vSearchCoord = mpProperties[iPropertyID].garage.vInPlayerLoc 
						g_SpawnData.PrivateParams.fRawHeading = mpProperties[iPropertyID].garage.fInPlayerHeading
						g_SpawnData.bDontAskPermission = TRUE // no need to ask server for permission as only a single point
					BREAK
				ENDSWITCH
				//set flag so others know you are trying to spawn in property
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
				bUsesFixedPoints = TRUE
				g_SpawnData.bIgnoreExclusionCheck = TRUE
				
				PRINTLN("SPAWN_LOCATION_NEAREST_RESPAWN_POINT- nearest point is property so setting g_SpawnData.bDisableFadeInAndTurnOnControls")
				IF (g_SpawnData.iWarpServerRequestCount = 0)
					g_SpawnData.bSpawningInProperty	= TRUE
					PRINTLN("SPAWN_LOCATION_NEAREST_RESPAWN_POINT - g_SpawnData.bSpawningInProperty = TRUE - E")
				ENDIF
				g_SpawnData.bDisableFadeInAndTurnOnControls = TRUE
			BREAK
			
			CASE SPAWN_LOCATION_OFFICE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_OFFICE") NET_NL()
				#ENDIF
				
				iOwnedProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_0)
				
				IF iOwnedProperty <= 0
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_OFFICE - but player doesn't own a property")
					#ENDIF
				ELSE
					SET_LAST_USED_PROPERTY(iOwnedProperty)
				ENDIF
				iPropertyID = iOwnedProperty 
				SWITCH GET_SPAWN_ACTIVITY()
					CASE SPAWN_ACTIVITY_SHOWER
						g_SpawnData.PrivateParams.vSearchCoord = mpProperties[iPropertyID].house.activity[SAFEACT_SHOWER].vTriggerVec
						g_SpawnData.PrivateParams.fRawHeading = mpProperties[iPropertyID].house.activity[SAFEACT_SHOWER].fTriggerRot
						NET_PRINT("[spawning] GetSpawnLocationCoords - spawning in shower at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
					BREAK
					CASE SPAWN_ACTIVITY_BED
						MP_PROP_OFFSET_STRUCT tempOffset
						GET_POSITION_AS_OFFSET_FOR_PROPERTY(iPropertyID, MP_PROP_ELEMENT_BED_SCENE_R, tempOffset, GET_BASE_PROPERTY_FROM_PROPERTY(iPropertyID))
						g_SpawnData.PrivateParams.vSearchCoord = tempOffset.vLoc
						g_SpawnData.PrivateParams.fRawHeading = tempOffset.vRot.Z
						
//						g_SpawnData.PrivateParams.vSearchCoord = mpProperties[iPropertyID].house.activity[SAFEACT_BED].vTriggerVec
//						g_SpawnData.PrivateParams.fRawHeading = mpProperties[iPropertyID].house.activity[SAFEACT_BED].fTriggerRot
						NET_PRINT("[spawning] GetSpawnLocationCoords - spawning in bed at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
					BREAK
					DEFAULT
						GetSpawnForProperty(iOwnedProperty, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading)	
					BREAK
				ENDSWITCH				
				
	
				bUsesFixedPoints = TRUE
				g_SpawnData.bIgnoreExclusionCheck = TRUE
				
				IF (g_SpawnData.iWarpServerRequestCount = 0)
					g_SpawnData.bSpawningInProperty	= TRUE
					PRINTLN("SPAWN_LOCATION_OFFICE - g_SpawnData.bSpawningInProperty = TRUE - D")
				ENDIF
				g_SpawnData.bDisableFadeInAndTurnOnControls = TRUE
			BREAK		
						
			CASE SPAWN_LOCATION_CLUBHOUSE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_CLUBHOUSE") NET_NL()
				#ENDIF
				
				iOwnedProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE)
				
				IF iOwnedProperty <= 0
					IF g_SpawnData.iDrunkRespawnState = 0
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_CLUBHOUSE - but player doesn't own a property")
					#ENDIF
					ENDIF
				ELSE
					SET_LAST_USED_PROPERTY(iOwnedProperty)
				ENDIF
				
				// if spawning into someone elses clubhouse
				IF (g_SpawnData.iPropertyToSpawnIntoToilet > -1)
					PRINTLN("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_CLUBHOUSE - g_SpawnData.iPropertyToSpawnIntoToilet = ", g_SpawnData.iPropertyToSpawnIntoToilet)
					iOwnedProperty = g_SpawnData.iPropertyToSpawnIntoToilet
					g_SpawnData.iPropertyToSpawnIntoToilet = -1
				ENDIF
				
				iPropertyID = iOwnedProperty 
				
				MP_PROP_OFFSET_STRUCT tempOffset
				
				SWITCH GET_SPAWN_ACTIVITY()

					CASE SPAWN_ACTIVITY_BED						
						GET_POSITION_AS_OFFSET_FOR_PROPERTY(iPropertyID, MP_PROP_ELEMENT_BED_SCENE_R, tempOffset, GET_BASE_PROPERTY_FROM_PROPERTY(iPropertyID))
						g_SpawnData.PrivateParams.vSearchCoord = tempOffset.vLoc
						g_SpawnData.PrivateParams.fRawHeading = tempOffset.vRot.Z
						NET_PRINT("[spawning] GetSpawnLocationCoords - spawning in bed at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
					BREAK
					
					CASE SPAWN_ACTIVITY_TOILET_SPEW
						GET_POSITION_AS_OFFSET_FOR_PROPERTY(iPropertyID, MP_PROP_ELEMENT_CLUBHOUSE_TOILET_SPEW, tempOffset, GET_BASE_PROPERTY_FROM_PROPERTY(iPropertyID))
						g_SpawnData.PrivateParams.vSearchCoord = tempOffset.vLoc
						g_SpawnData.PrivateParams.fRawHeading = tempOffset.vRot.Z
						NET_PRINT("[spawning] GetSpawnLocationCoords - spawning at toilet spew at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()						
					BREAK
					
					DEFAULT
						GetSpawnForProperty(iOwnedProperty, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading)	
					BREAK
				ENDSWITCH				
				
				bUsesFixedPoints = TRUE
				g_SpawnData.bIgnoreExclusionCheck = TRUE
				
				IF (g_SpawnData.iWarpServerRequestCount = 0)
					g_SpawnData.bSpawningInProperty	= TRUE
					PRINTLN("SPAWN_LOCATION_CLUBHOUSE - g_SpawnData.bSpawningInProperty = TRUE - D")
				ENDIF
				g_SpawnData.bDisableFadeInAndTurnOnControls = TRUE
			BREAK		
			
			CASE SPAWN_LOCATION_IE_WAREHOUSE
			CASE SPAWN_LOCATION_BUNKER
			CASE SPAWN_LOCATION_HANGAR
			CASE SPAWN_LOCATION_DEFUNCT_BASE
			CASE SPAWN_LOCATION_NIGHTCLUB
			CASE SPAWN_LOCATION_ARENA_GARAGE
			CASE SPAWN_LOCATION_CASINO
			CASE SPAWN_LOCATION_CASINO_APARTMENT
			CASE SPAWN_LOCATION_ARCADE
			CASE SPAWN_LOCATION_CASINO_NIGHTCLUB
			CASE SPAWN_LOCATION_SUBMARINE
			CASE SPAWN_LOCATION_CAR_MEET
			CASE SPAWN_LOCATION_AUTO_SHOP
			CASE SPAWN_LOCATION_FIXER_HQ
			#IF FEATURE_MUSIC_STUDIO
			CASE SPAWN_LOCATION_MUSIC_STUDIO			
			#ENDIF
			#IF FEATURE_GEN9_EXCLUSIVE
			CASE SPAWN_LOCATION_TUTORIAL_BUSINESS
			#ENDIF
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = ") NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.SpawnLocation)) NET_NL()
				#ENDIF
				
				SWITCH GET_SPAWN_ACTIVITY()
					CASE SPAWN_ACTIVITY_BED
						IF g_SpawnData.SpawnLocation = SPAWN_LOCATION_IE_WAREHOUSE

							g_SpawnData.PrivateParams.vSearchCoord = <<959.4153, -3005.4331, -40.6349>>
							g_SpawnData.PrivateParams.fRawHeading = 288.1089
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in bed in IE Warehouse at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							#ENDIF
						
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_BUNKER

							g_SpawnData.PrivateParams.vSearchCoord = <<905.5001, -3200.1077, -98.1879>>
							g_SpawnData.PrivateParams.fRawHeading = 190.2091
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in bed in Bunker at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							#ENDIF
						
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_HANGAR  

							g_SpawnData.PrivateParams.vSearchCoord = <<-1235.6235, -2984.2317, -42.2601>>
							g_SpawnData.PrivateParams.fRawHeading = 189.5472
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in bed in Hangar at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							#ENDIF
						
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_DEFUNCT_BASE
						
							g_SpawnData.PrivateParams.vSearchCoord = <<368.9986, 4822.7114, -59.9797>>
							g_SpawnData.PrivateParams.fRawHeading = 313.7473
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in bed in Defunct base at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							#ENDIF
						
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_NIGHTCLUB
						
							g_SpawnData.PrivateParams.vSearchCoord = <<-1614.179, -3019.259, -75.180>>
							g_SpawnData.PrivateParams.fRawHeading = 135.0000
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in bed in Nightclub at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							#ENDIF
						
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_ARENA_GARAGE
						
							g_SpawnData.PrivateParams.vSearchCoord = <<208.8848, 5164.2656, -89.1985>>
							g_SpawnData.PrivateParams.fRawHeading = 97.78
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in bed in Arena Garage at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							#ENDIF
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_CASINO_APARTMENT
							g_SpawnData.PrivateParams.vSearchCoord = <<970.729322, 78.290936, 115.1642>>
							g_SpawnData.PrivateParams.fRawHeading = 236.0848
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in bed in Casino Apartment at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							#ENDIF
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_ARCADE
							g_SpawnData.PrivateParams.vSearchCoord = <<2723.0068, -368.7443, -56.3809>>
							g_SpawnData.PrivateParams.fRawHeading = 267.6547
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in bed in Arcade at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							#ENDIF
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_SUBMARINE
							g_SpawnData.PrivateParams.vSearchCoord = <<1558.369, 384.953, -53.8531>>
							g_SpawnData.PrivateParams.fRawHeading = 0.0
							
							MPGlobalsAmbience.bLaunchVehicleDropSubmarine = TRUE
							
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in bed in Submarine at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							#ENDIF
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_AUTO_SHOP
							g_SpawnData.PrivateParams.vSearchCoord = << -1360.148, 144.800, -95.700 >>
							g_SpawnData.PrivateParams.fRawHeading = 90

							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in bed in Auto Shop at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							#ENDIF
						#IF FEATURE_FIXER
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_FIXER_HQ						
							SWITCH GET_PLAYERS_OWNED_FIXER_HQ(PLAYER_ID())
								CASE FIXER_HQ_HAWICK
									g_SpawnData.PrivateParams.vSearchCoord = << 387.949, -69.676, 112 >>
									g_SpawnData.PrivateParams.fRawHeading = 26.279999
								BREAK
								CASE FIXER_HQ_ROCKFORD
									g_SpawnData.PrivateParams.vSearchCoord = << -1011.61, -431.12, 72.4981 >>
									g_SpawnData.PrivateParams.fRawHeading = 73.229996
								BREAK
								CASE FIXER_HQ_SEOUL
									g_SpawnData.PrivateParams.vSearchCoord = << -595.463, -709.173, 121.642 >>
									g_SpawnData.PrivateParams.fRawHeading = 226.130005
								BREAK
								CASE FIXER_HQ_VESPUCCI
									g_SpawnData.PrivateParams.vSearchCoord = << -996.574, -753.597, 70.5312 >>
									g_SpawnData.PrivateParams.fRawHeading = 136.279999
								BREAK
							ENDSWITCH
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in bed in FIXER HQ at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
							#ENDIF
						#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn in a bed which is not in a simple interior, going to spawn at the origin.") NET_NL()
							#ENDIF
						ENDIF
					BREAK
					CASE SPAWN_ACTIVITY_TOILET_SPEW
						IF g_SpawnData.SpawnLocation = SPAWN_LOCATION_NIGHTCLUB
							g_SpawnData.PrivateParams.vSearchCoord 	= <<-1609.1658, -3016.9788, -80.0061>>
							g_SpawnData.PrivateParams.fRawHeading 	= 270.0
							
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in toilet in nightclub at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_CASINO
							g_SpawnData.PrivateParams.vSearchCoord 	= <<1105.1066, 213.3884, -50.4406>>
							g_SpawnData.PrivateParams.fRawHeading 	= 180.0
							
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in toilet in nightclub at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_CASINO_APARTMENT
							g_SpawnData.PrivateParams.vSearchCoord 	= <<978.4244, 57.9174, 115.1640>>
							g_SpawnData.PrivateParams.fRawHeading 	= 55.0
							
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in toilet in nightclub at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_ARCADE
							g_SpawnData.PrivateParams.vSearchCoord 	= <<2723.8425, -377.4552, -48.4000>>
							g_SpawnData.PrivateParams.fRawHeading 	= 180.0
							
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in toilet in arcade at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_CASINO_NIGHTCLUB
							g_SpawnData.PrivateParams.vSearchCoord 	= <<1542.7819, 246.4772, -50.0060>>
							g_SpawnData.PrivateParams.fRawHeading 	= 270.0
							
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in toilet in casino nightclub at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_SUBMARINE
							g_SpawnData.PrivateParams.vSearchCoord = <<1558.4023, 382.5171, -54.2844>>
							g_SpawnData.PrivateParams.fRawHeading = 180.0
							
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in toilet in Submarine at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()

						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_AUTO_SHOP
							g_SpawnData.PrivateParams.vSearchCoord = <<-1356.4492, 160.3916, -100.1943>>
							g_SpawnData.PrivateParams.fRawHeading = 184.3954
							
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in toilet in Auto Shop at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
						#IF FEATURE_FIXER
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_FIXER_HQ
							g_SpawnData.PrivateParams.vSearchCoord = << 386.225, -68.459, 111.390 >>
							g_SpawnData.PrivateParams.fRawHeading = -115.2
							
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in toilet in FIXER HQ at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
						#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn in a toilet which is not in a simple interior, going to spawn at the origin.") NET_NL()
							#ENDIF
						ENDIF
					BREAK
					CASE SPAWN_ACTIVITY_DRUNK_AWAKEN
						IF g_SpawnData.SpawnLocation = SPAWN_LOCATION_NIGHTCLUB
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn drunk awaken from SPAWN_LOCATION_NIGHTCLUB, going to spawn at the origin.") NET_NL()
							#ENDIF
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_CASINO
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn drunk awaken from SPAWN_LOCATION_CASINO, going to spawn at the origin.") NET_NL()
							#ENDIF
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_ARCADE
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn drunk awaken from SPAWN_LOCATION_ARCADE, going to spawn at the origin.") NET_NL()
							#ENDIF
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_CASINO_NIGHTCLUB
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn drunk awaken from SPAWN_LOCATION_CASINO_NIGHTCLUB, going to spawn at the origin.") NET_NL()
							#ENDIF
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_SUBMARINE
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn drunk awaken from SPAWN_LOCATION_SUBMARINE, going to spawn at the origin.") NET_NL()
							#ENDIF
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_AUTO_SHOP
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn drunk awaken from SPAWN_LOCATION_AUTO_SHOP, going to spawn at the origin.") NET_NL()
							#ENDIF
						#IF FEATURE_FIXER
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_FIXER_HQ
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn drunk awaken from SPAWN_LOCATION_FIXER_HQ, going to spawn at the origin.") NET_NL()
							#ENDIF
						#IF FEATURE_MUSIC_STUDIO
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_MUSIC_STUDIO
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn drunk awaken from SPAWN_LOCATION_MUSIC_STUDIO, going to spawn at the origin.") NET_NL()
							#ENDIF
						#ENDIF
						#ENDIF
						ENDIF
					BREAK
					CASE SPAWN_ACTIVITY_SHOWER
						IF g_SpawnData.SpawnLocation = SPAWN_LOCATION_ARENA_GARAGE
							g_SpawnData.PrivateParams.vSearchCoord 	= <<202.8190, 5162.1733, -88.8851>>
							g_SpawnData.PrivateParams.fRawHeading 	= 90.0
							
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning in toilet in nightclub at ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
						ENDIF
					BREAK
					#IF FEATURE_GEN9_EXCLUSIVE
					CASE SPAWN_ACTIVITY_HEIST_DEEPLINK
						IF g_SpawnData.SpawnLocation = SPAWN_LOCATION_DEFUNCT_BASE
							g_SpawnData.PrivateParams.vSearchCoord 	= <<341.2577, 4857.6895, -59.9999>>
							g_SpawnData.PrivateParams.fRawHeading 	= -37.4283
							
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Spawning near planning room in defunct base ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_NL()
						ENDIF
					BREAK
					#ENDIF
					DEFAULT
						SIMPLE_INTERIORS eSimpleInteriorID
						
						IF g_SpawnData.SpawnLocation = SPAWN_LOCATION_IE_WAREHOUSE
						
							eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_IMPORT_EXPORT_GARAGE_ID(GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID()))
						
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_BUNKER 
							
							eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(GET_OWNED_BUNKER(PLAYER_ID()))
							
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_HANGAR
						
							eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_HANGAR_ID(GET_PLAYERS_OWNED_HANGAR(PLAYER_ID()))
						
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_DEFUNCT_BASE
						
							eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_DEFUNCT_BASE_ID(GET_PLAYERS_OWNED_DEFUNCT_BASE(PLAYER_ID()))
							
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_NIGHTCLUB
						
							eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_NIGHTCLUB_ID(GET_PLAYERS_OWNED_NIGHTCLUB(PLAYER_ID()))
						
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_ARENA_GARAGE
							IF (g_TransitionSessionNonResetVars.bFinishedFirstArenaEvent)
								PLAYER_INDEX pOwner
								pOwner = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(g_TransitionSessionNonResetVars.ghOriginalArenaOwnerLeader)
								PRINTLN("[Spawning] GetSpawnLocationCoords, SPAWN_LOCATION_ARENA_GARAGE, bFinishedFirstArenaEvent, pOwner = ", NATIVE_TO_INT(pOwner))
								IF (pOwner != INVALID_PLAYER_INDEX())
									eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_ARENA_GARAGE_ID(GET_PLAYERS_OWNED_ARENA_GARAGE(pOwner))
								ENDIF
							ELSE						
								eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_ARENA_GARAGE_ID(GET_PLAYERS_OWNED_ARENA_GARAGE(PLAYER_ID()))
							ENDIF
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_CASINO							
							eSimpleInteriorID = SIMPLE_INTERIOR_CASINO	
						#IF FEATURE_GEN9_STANDALONE
							g_SpawnData.PrivateParams.vSearchCoord 	= <<1104.2, 224.6, -49>>
							g_SpawnData.PrivateParams.fRawHeading 	= 90.0
						#ENDIF // FEATURE_GEN9_STANDALONE
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_CASINO_APARTMENT
							eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_CASINO_APARTMENT_ID(GET_PLAYERS_OWNED_CASINO_APARTMENT(PLAYER_ID()))
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_ARCADE
							eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_ARCADE_PROPERTY_ID(GET_PLAYERS_OWNED_ARCADE_PROPERTY(PLAYER_ID()))
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_CASINO_NIGHTCLUB
							eSimpleInteriorID = SIMPLE_INTERIOR_CASINO_NIGHTCLUB
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_SUBMARINE
							MPGlobalsAmbience.bLaunchVehicleDropSubmarine = TRUE
							eSimpleInteriorID = SIMPLE_INTERIOR_SUBMARINE
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_CAR_MEET
							eSimpleInteriorID = SIMPLE_INTERIOR_CAR_MEET
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_AUTO_SHOP
							eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_AUTO_SHOP_PROPERTY_ID(GET_PLAYERS_OWNED_AUTO_SHOP_PROPERTY(PLAYER_ID()))
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_FIXER_HQ
							eSimpleInteriorID = GET_SIMPLE_INTERIOR_ID_FROM_FIXER_HQ_ID(GET_PLAYERS_OWNED_FIXER_HQ(PLAYER_ID()))
						#IF FEATURE_MUSIC_STUDIO
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_MUSIC_STUDIO
							eSimpleInteriorID = SIMPLE_INTERIOR_MUSIC_STUDIO
						#ENDIF
						#IF FEATURE_GEN9_EXCLUSIVE
						ELIF g_SpawnData.SpawnLocation = SPAWN_LOCATION_TUTORIAL_BUSINESS
							eSimpleInteriorID = MP_INTRO_GET_BIKER_BUSINESS_SIMPLE_INTERIOR() 
						#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Trying to spawn in simple interior but spawn location is wrong, going to spawn at the origin.") NET_NL()
							#ENDIF
						ENDIF
						
						IF eSimpleInteriorID != SIMPLE_INTERIOR_INVALID
							
							GetSpawnForSimpleInterior(eSimpleInteriorID, g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading, g_TransitionSpawnData.iSpawnActivityLocationOverride)
							#IF IS_DEBUG_BUILD
							PRINTLN("[Spawning] GetSpawnLocationCoords - Spawning in simple interior: ", ENUM_TO_INT(eSimpleInteriorID), " at ", g_SpawnData.PrivateParams.vSearchCoord)
							#ENDIF
							
						ELSE
							#IF IS_DEBUG_BUILD
							NET_PRINT("[Spawning] GetSpawnLocationCoords - Invalid simple interior ID, going to spawn at the origin.")
							#ENDIF
						ENDIF
					BREAK
				ENDSWITCH
				
				bUsesFixedPoints = TRUE
				g_SpawnData.bIgnoreExclusionCheck = TRUE
				
				IF (g_SpawnData.iWarpServerRequestCount = 0)
					g_SpawnData.bSpawningInSimpleInterior = TRUE					
					
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_SPAWNING_INTO_SIMPLE_INTERIOR)
					PRINTLN("[Spawning] GetSpawnLocationCoords - g_SpawnData.bSpawningInSimpleInterior = TRUE")
				ENDIF
				
				g_SpawnData.bDisableFadeInAndTurnOnControls = TRUE	
			BREAK
			

			
			CASE SPAWN_LOCATION_NEAR_OTHER_PLAYERS
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_OTHER_PLAYERS") NET_NL()
				#ENDIF
				
				IF NOT (NETWORK_FIND_LARGEST_BUNCH_OF_PLAYERS(FALSE, g_SpawnData.PrivateParams.vSearchCoord))			
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_OTHER_PLAYERS - could not find bunch of players") NET_NL()
					#ENDIF
				
					g_SpawnData.PrivateParams.vSearchCoord = GetAveragePlayerCoord()
					g_SpawnData.PrivateParams.vSearchCoord = GetRandomPointInCircle(g_SpawnData.PrivateParams.vSearchCoord, NEAR_OTHER_PLAYERS_SPAWN_RADIUS, NEAR_OTHER_PLAYERS_MIN_SPAWN_RADIUS)
				ENDIF
				g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)
			BREAK
				
			
			CASE SPAWN_LOCATION_NEAR_GANG_BOSS
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_GANG_BOSS") NET_NL()
				#ENDIF
				
				// if the local player is a boss
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
				
					// find any alive player that is in my gang
					GoonID = GET_NEAREST_GOON_IN_PLAYERS_GANG(PLAYER_ID(), TRUE)
					
					PRINTLN("[spawning] SPAWN_LOCATION_NEAR_GANG_BOSS - GoonID = ", NATIVE_TO_INT(GoonID))
				
					IF NOT (GoonID = INVALID_PLAYER_INDEX())
						g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_PERCEIVED_COORDS(GoonID)
					ELSE
						g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID())
					ENDIF
					
				
				// else i'm a goon, use the gang boss coords.
				ELSE
					g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_PERCEIVED_COORDS(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				ENDIF				

				g_SpawnData.PrivateParams.fRawHeading = 0.0
				g_SpawnData.PrivateParams.fSearchRadius = 50.0
				g_SpawnData.PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE
				g_SpawnData.PrivateParams.vAngledAreaPoint1 = <<0.0, 0.0, 0.0>>
				g_SpawnData.PrivateParams.vAngledAreaPoint2 = <<0.0, 0.0, 0.0>>
				g_SpawnData.PrivateParams.fAngledAreaWidth = 0.0			
				g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord // the exclusion coords will move the search start point, the vAvoidCoords will avoid points returned in area.
				g_SpawnData.fNearExclusionRadius = 2.0
				g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord
				g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
				g_SpawnData.PublicParams.bCloseToOriginAsPossible = FALSE
				g_SpawnData.PublicParams.vFacingCoords = g_SpawnData.PrivateParams.vSearchCoord			
				g_SpawnData.PublicParams.bPreferPointsCloserToRoads = TRUE
				g_SpawnData.PublicParams.bConsiderInteriors = FALSE //IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(g_SpawnData.PrivateParams.vSearchCoord))
				g_SpawnData.PrivateParams.bDoVisibleChecks = TRUE
				g_SpawnData.PrivateParams.bIgnoreTeammatesForMinDistCheck = TRUE				
				g_SpawnData.bIgnoreGlobalExclusionCheck = TRUE				
				
				PRINTLN("[spawning] GetSpawnLocationCoords - vSearchCoord = ", g_SpawnData.PrivateParams.vSearchCoord )
				PRINTLN("[spawning] GetSpawnLocationCoords - fRawHeading = ", g_SpawnData.PrivateParams.fRawHeading )
				
			BREAK
			
			CASE SPAWN_LOCATION_GANG_DM
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_GANG_DM") NET_NL()
				#ENDIF
				
				// if the local player is a boss
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
				
					// find any alive player that is in my gang
					GoonID = GET_NEAREST_GOON_IN_PLAYERS_GANG(PLAYER_ID(), TRUE)
					
					PRINTLN("[spawning] SPAWN_LOCATION_GANG_DM - GoonID = ", NATIVE_TO_INT(GoonID))
				
					IF NOT (GoonID = INVALID_PLAYER_INDEX())
						g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_PERCEIVED_COORDS(GoonID)
					ELSE
						g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID())
					ENDIF
					
				
				// else i'm a goon, use the gang boss coords.
				ELSE
					g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_PERCEIVED_COORDS(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				ENDIF

				g_SpawnData.PrivateParams.fRawHeading = 0.0
				g_SpawnData.PrivateParams.fSearchRadius = 50.0
				g_SpawnData.PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE
				g_SpawnData.PrivateParams.vAngledAreaPoint1 = <<0.0, 0.0, 0.0>>
				g_SpawnData.PrivateParams.vAngledAreaPoint2 = <<0.0, 0.0, 0.0>>
				g_SpawnData.PrivateParams.fAngledAreaWidth = 0.0			
				g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord // the exclusion coords will move the search start point, the vAvoidCoords will avoid points returned in area.
				g_SpawnData.fNearExclusionRadius = 2.0
				g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord
				g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
				g_SpawnData.PublicParams.bCloseToOriginAsPossible = FALSE
				g_SpawnData.PublicParams.vFacingCoords = g_SpawnData.PrivateParams.vSearchCoord			
				g_SpawnData.PublicParams.bPreferPointsCloserToRoads = TRUE
				g_SpawnData.PublicParams.bConsiderInteriors = FALSE //IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(g_SpawnData.PrivateParams.vSearchCoord))
				g_SpawnData.PrivateParams.bDoVisibleChecks = TRUE
				g_SpawnData.PrivateParams.bIgnoreTeammatesForMinDistCheck = TRUE				
				g_SpawnData.bIgnoreGlobalExclusionCheck = TRUE				
				
				PRINTLN("[spawning] GetSpawnLocationCoords - vSearchCoord = ", g_SpawnData.PrivateParams.vSearchCoord )
				PRINTLN("[spawning] GetSpawnLocationCoords - fRawHeading = ", g_SpawnData.PrivateParams.fRawHeading )
				
			BREAK
			
			CASE SPAWN_LOCATION_NEAR_CURRENT_POSITION
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_POSITION") NET_NL()
				#ENDIF
				g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_COORDS(PLAYER_ID())				
				g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord
				g_SpawnData.fNearExclusionRadius = 20.0	
				g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord 
				g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
				g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)
				g_SpawnData.PublicParams.bCloseToOriginAsPossible = TRUE
				g_SpawnData.PrivateParams.fSearchRadius = 150.0
			BREAK
			
			CASE SPAWN_LOCATION_NEAR_CURRENT_POSITION_AS_POSSIBLE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_POSITION_AS_POSSIBLE") NET_NL()
				#ENDIF
				g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_COORDS(PLAYER_ID())				
				g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord
				g_SpawnData.fNearExclusionRadius = 0.01	
				g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord 
				g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
				g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)
				g_SpawnData.PublicParams.bCloseToOriginAsPossible = TRUE
				g_SpawnData.PrivateParams.fSearchRadius = 150.0
			BREAK
			
			CASE SPAWN_LOCATION_NEAR_CURRENT_PERCEIVED_POSITION
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_PERCEIVED_POSITION") NET_NL()
				#ENDIF
				g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID())				
				g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord
				g_SpawnData.fNearExclusionRadius = 0.01	
				g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord 
				g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
				g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)
				g_SpawnData.PublicParams.bCloseToOriginAsPossible = TRUE
				g_SpawnData.PrivateParams.fSearchRadius = 150.0
			BREAK
			
			#IF FEATURE_HEIST_ISLAND
			CASE SPAWN_LOCATION_LAND_NEAR_SUBMARINE

				PRINTLN("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_LAND_NEAR_SUBMARINE")
			
				// first use the perceived position of the sub			
				g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID())
				
				// now get the nearest dingy point on land
				g_SpawnData.PrivateParams.vSearchCoord = GET_NEAREST_SUBMARINE_DINGY_POSITION(g_SpawnData.PrivateParams.vSearchCoord)
				
				g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord
				g_SpawnData.fNearExclusionRadius = 0.01	
				g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord 
				g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
				g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)
				g_SpawnData.PublicParams.bCloseToOriginAsPossible = TRUE
				g_SpawnData.PrivateParams.fSearchRadius = 150.0
				
			BREAK
			#ENDIF
			
			CASE SPAWN_LOCATION_NEAR_CURRENT_POSITION_SPREAD_OUT
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_POSITION_SPREAD_OUT") NET_NL()
				#ENDIF
				g_SpawnData.PrivateParams.vSearchCoord = GetPointDistFromPointBasedOnPlayerID(GET_PLAYER_COORDS(PLAYER_ID()), 200.0)			
				g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord
				g_SpawnData.fNearExclusionRadius = 0.0	
				g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord 
				g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
				g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)
				g_SpawnData.PublicParams.bCloseToOriginAsPossible = FALSE
				g_SpawnData.PrivateParams.fSearchRadius = 150.0
			BREAK	
			
			CASE SPAWN_LOCATION_AT_CURRENT_POSITION
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_CURRENT_POSITION") NET_NL()
				#ENDIF
				g_SpawnData.PrivateParams.vSearchCoord = GET_PLAYER_COORDS(PLAYER_ID())
				g_SpawnData.PrivateParams.fRawHeading = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)		
			BREAK	

			


			CASE SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA				
				#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA") NET_NL()
					IF (VMAG(g_SpecificSpawnLocation.vCoords) = 0.0)
						NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA - specific coords are at origin!") NET_NL()
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA - specific coords are at origin!") 
					ENDIF
				#ENDIF			
				IF NOT (g_SpecificSpawnLocation.bUseAngledArea)
					SCRIPT_ASSERT("SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA - a specific angled area has not been setup!")
				ELSE						
					g_SpawnData.PrivateParams.vSearchCoord = g_SpecificSpawnLocation.vCoords
					g_SpawnData.PrivateParams.fRawHeading = 0.0
					g_SpawnData.PrivateParams.fSearchRadius = 0.0
					g_SpawnData.PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_ANGLED
					g_SpawnData.PrivateParams.vAngledAreaPoint1 = g_SpecificSpawnLocation.vAngledCoords1
					g_SpawnData.PrivateParams.vAngledAreaPoint2 = g_SpecificSpawnLocation.vAngledCoords2
					g_SpawnData.PrivateParams.fAngledAreaWidth = g_SpecificSpawnLocation.fWidth
					g_SpawnData.vExclusionCoord = <<0.0, 0.0, 0.0>>
					g_SpawnData.fNearExclusionRadius = 0.0
					g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord 
					g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
					g_SpawnData.PublicParams.bCloseToOriginAsPossible = g_SpecificSpawnLocation.bNearCentrePoint
					IF VMAG(g_SpecificSpawnLocation.vFacing) > 0.0
						g_SpawnData.PublicParams.vFacingCoords = g_SpecificSpawnLocation.vFacing 				
					ELSE
						g_SpawnData.PublicParams.vFacingCoords = (g_SpecificSpawnLocation.vAngledCoords1 + g_SpecificSpawnLocation.vAngledCoords2)/2.0
					ENDIF
					g_SpawnData.PublicParams.bPreferPointsCloserToRoads = g_SpecificSpawnLocation.bDoNearARoadChecks
					g_SpawnData.PublicParams.bConsiderInteriors = g_SpecificSpawnLocation.bConsiderInteriors
					g_SpawnData.PrivateParams.bDoVisibleChecks = g_SpecificSpawnLocation.bDoVisibleChecks
					g_SpawnData.PublicParams.fMinDistFromPlayer = g_SpecificSpawnLocation.fMinDistFromOtherPlayers
					g_SpawnData.PrivateParams.bAllowFallbackToUseInteriors = g_SpecificSpawnLocation.bAllowFallbackToUseInteriors
					g_SpawnData.PrivateParams.bVehiclesCanConsiderInactiveNodes = g_SpecificSpawnLocation.bVehiclesCanConsiderInactiveNodes 					
					g_SpawnData.PublicParams.fUpperZLimitForNodes = g_SpecificSpawnLocation.fUpperZLimitForNodes

				ENDIF
			BREAK
			
			CASE SPAWN_LOCATION_NEAR_SPECIFIC_COORDS
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS") NET_NL()
					IF (VMAG(g_SpecificSpawnLocation.vCoords) = 0.0)
						NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAR_SPECIFIC_COORDS - specific coords are at origin!") NET_NL()
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAR_SPECIFIC_COORDS - specific coords are at origin!") 
					ENDIF
				#ENDIF			
				
				IF (g_SpecificSpawnLocation.bUseAngledArea)
					SCRIPT_ASSERT("SPAWN_LOCATION_NEAR_SPECIFIC_COORDS - cannot use angled areas with this, use SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA instead")
				ELSE
					g_SpawnData.PrivateParams.vSearchCoord = g_SpecificSpawnLocation.vCoords
					g_SpawnData.PrivateParams.fRawHeading = g_SpecificSpawnLocation.fHeading
					g_SpawnData.PrivateParams.fSearchRadius = g_SpecificSpawnLocation.fRadius
					g_SpawnData.PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE
					g_SpawnData.PrivateParams.vAngledAreaPoint1 = <<0.0, 0.0, 0.0>>
					g_SpawnData.PrivateParams.vAngledAreaPoint2 = <<0.0, 0.0, 0.0>>
					g_SpawnData.PrivateParams.fAngledAreaWidth = 0.0			
					g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord // the exclusion coords will move the search start point, the vAvoidCoords will avoid points returned in area.
					g_SpawnData.fNearExclusionRadius = g_SpecificSpawnLocation.fMinRadius
					g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord
					g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
					g_SpawnData.PublicParams.bCloseToOriginAsPossible = g_SpecificSpawnLocation.bNearCentrePoint
					if (VMAG(g_SpecificSpawnLocation.vFacing) > 0.0)
						g_SpawnData.PublicParams.vFacingCoords = g_SpecificSpawnLocation.vFacing	
					ELSE
						g_SpawnData.PublicParams.vFacingCoords = g_SpecificSpawnLocation.vCoords				
					ENDIF
					g_SpawnData.PublicParams.bPreferPointsCloserToRoads = g_SpecificSpawnLocation.bDoNearARoadChecks
					g_SpawnData.PublicParams.bConsiderInteriors = g_SpecificSpawnLocation.bConsiderInteriors
					g_SpawnData.PrivateParams.bDoVisibleChecks = g_SpecificSpawnLocation.bDoVisibleChecks
					g_SpawnData.PublicParams.fMinDistFromPlayer = g_SpecificSpawnLocation.fMinDistFromOtherPlayers
					g_SpawnData.PrivateParams.bAllowFallbackToUseInteriors = g_SpecificSpawnLocation.bAllowFallbackToUseInteriors
					g_SpawnData.PrivateParams.bVehiclesCanConsiderInactiveNodes = g_SpecificSpawnLocation.bVehiclesCanConsiderInactiveNodes
					g_SpawnData.bIgnoreGlobalExclusionCheck = g_SpecificSpawnLocation.bIgnoreGlobalExclusionZones
					g_SpawnData.PublicParams.fUpperZLimitForNodes = g_SpecificSpawnLocation.fUpperZLimitForNodes
				ENDIF				

			BREAK
			
			CASE SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG
			
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG") NET_NL()
					IF (VMAG(g_SpecificSpawnLocation.vCoords) = 0.0)
						NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG - specific coords are at origin!") NET_NL()
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG - specific coords are at origin!") 
					ENDIF
				#ENDIF			
				
				IF (g_SpecificSpawnLocation.bUseAngledArea)
					SCRIPT_ASSERT("SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG - cannot use angled areas with this, not currently supported. ask neil nicely. ")
				ELSE
					g_SpawnData.PrivateParams.vSearchCoord = g_SpecificSpawnLocation.vCoords
					g_SpawnData.PrivateParams.fRawHeading = g_SpecificSpawnLocation.fHeading
					g_SpawnData.PrivateParams.fSearchRadius = g_SpecificSpawnLocation.fRadius
					g_SpawnData.PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE
					g_SpawnData.PrivateParams.vAngledAreaPoint1 = <<0.0, 0.0, 0.0>>
					g_SpawnData.PrivateParams.vAngledAreaPoint2 = <<0.0, 0.0, 0.0>>
					g_SpawnData.PrivateParams.fAngledAreaWidth = 0.0			
					g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord // the exclusion coords will move the search start point, the vAvoidCoords will avoid points returned in area.
					g_SpawnData.fNearExclusionRadius = g_SpecificSpawnLocation.fMinRadius
					g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord
					g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
					g_SpawnData.PublicParams.bCloseToOriginAsPossible = g_SpecificSpawnLocation.bNearCentrePoint
					g_SpawnData.PublicParams.vFacingCoords = g_SpecificSpawnLocation.vFacing			
					g_SpawnData.PublicParams.bPreferPointsCloserToRoads = g_SpecificSpawnLocation.bDoNearARoadChecks
					g_SpawnData.PublicParams.bConsiderInteriors = g_SpecificSpawnLocation.bConsiderInteriors
					g_SpawnData.PrivateParams.bDoVisibleChecks = g_SpecificSpawnLocation.bDoVisibleChecks
					g_SpawnData.PublicParams.fMinDistFromPlayer = g_SpecificSpawnLocation.fMinDistFromOtherPlayers
					g_SpawnData.PrivateParams.bAllowFallbackToUseInteriors = g_SpecificSpawnLocation.bAllowFallbackToUseInteriors					
					g_SpawnData.bIgnoreGlobalExclusionCheck = g_SpecificSpawnLocation.bIgnoreGlobalExclusionZones
					g_SpawnData.PrivateParams.bVehiclesCanConsiderInactiveNodes = g_SpecificSpawnLocation.bVehiclesCanConsiderInactiveNodes
					g_SpawnData.PublicParams.fUpperZLimitForNodes = g_SpecificSpawnLocation.fUpperZLimitForNodes
					//  have any other members of the gang already respawned in this area? 
					IF AreAnyOtherGangMembersAliveOrRespawningInArea(g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fSearchRadius, g_SpawnData.PrivateParams.vSearchCoord)
						PRINTLN("[spawning] GetSpawnLocationCoords - valid gang member in area, altering search params.")
						IF (g_SpawnData.PrivateParams.fSearchRadius > 50.0)
							g_SpawnData.PrivateParams.fSearchRadius = 50.0
						ENDIF
						g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord 
						g_SpawnData.fNearExclusionRadius = 1.5
						g_SpawnData.PublicParams.fMinDistFromPlayer = 1.5
						g_SpawnData.PublicParams.bCloseToOriginAsPossible = TRUE
						g_SpawnData.PrivateParams.bDoVisibleChecks = FALSE
						g_SpawnData.PrivateParams.bIgnoreTeammatesForMinDistCheck = TRUE					
						g_SpawnData.PublicParams.vFacingCoords = g_SpawnData.PrivateParams.vSearchCoord
						//g_SpawnData.PublicParams.bConsiderInteriors = IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(g_SpawnData.PrivateParams.vSearchCoord))
					ELSE
						g_SpawnData.bSpawningOnOwnInGang = TRUE
						PRINTLN("[spawning] GetSpawnLocationCoords - no other gang members about.")	
					ENDIF
					
				ENDIF				
			
			BREAK
	
			CASE SPAWN_LOCATION_AT_SPECIFIC_COORDS_RACE_CORONA
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS_RACE_CORONA") NET_NL()
				#ENDIF	

				
				// is another player currently warping here?
				IF (g_SpawnData.NextSpawn.bUseRaceCoronaSpacing) 
				AND NOT IsPointSafeForRaceSpawn(g_SpecificSpawnLocation.vCoords)
													
					// find an offset
					g_SpawnData.PrivateParams.vSearchCoord = GetSecondaryRaceRespawn()

					g_SpawnData.PrivateParams.fRawHeading = g_SpecificSpawnLocation.fHeading
					g_SpawnData.PrivateParams.fSearchRadius = g_SpecificSpawnLocation.fRadius 			
					g_SpawnData.PrivateParams.fPitch = g_SpecificSpawnLocation.fPitch
					bUsesFixedPoints = TRUE	
					bForVehicle = FALSE
					g_SpawnData.bDontAskPermission = TRUE 
					
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] SPAWN_LOCATION_AT_SPECIFIC_COORDS_RACE_CORONA - spawning using bUseRaceCoronaSpacing") NET_NL()
					#ENDIF
					
				ELSE
			
					#IF IS_DEBUG_BUILD
					IF (VMAG(g_SpecificSpawnLocation.vCoords) = 0.0)
						NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_AT_SPECIFIC_COORDS_RACE_CORONA - specific coords are at origin!") NET_NL()
						SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_AT_SPECIFIC_COORDS_RACE_CORONA - specific coords are at origin!") 
					ENDIF
					#ENDIF
			
					g_SpawnData.PrivateParams.vSearchCoord = g_SpecificSpawnLocation.vCoords
					g_SpawnData.PrivateParams.fRawHeading = g_SpecificSpawnLocation.fHeading
					g_SpawnData.PrivateParams.fSearchRadius = g_SpecificSpawnLocation.fRadius	
					g_SpawnData.PrivateParams.fPitch = g_SpecificSpawnLocation.fPitch
					bUsesFixedPoints = TRUE	
					bForVehicle = FALSE
					
					IF NOT (g_SpawnData.NextSpawn.bUseRaceCoronaSpacing) // if we are not using spacing then no point wasting time in asking server for permission
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] SPAWN_LOCATION_AT_SPECIFIC_COORDS_RACE_CORONA - not asking permission") NET_NL()
						#ENDIF
						g_SpawnData.bDontAskPermission = TRUE 
					ENDIF
					
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] SPAWN_LOCATION_AT_SPECIFIC_COORDS_RACE_CORONA - spawning using default") NET_NL()
					#ENDIF
					
				ENDIF
					

			BREAK

			CASE SPAWN_LOCATION_AT_SPECIFIC_COORDS
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS") NET_NL()
				#ENDIF
				#IF IS_DEBUG_BUILD
				IF (VMAG(g_SpecificSpawnLocation.vCoords) = 0.0)
					NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_AT_SPECIFIC_COORDS - specific coords are at origin!") NET_NL()
					SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_AT_SPECIFIC_COORDS - specific coords are at origin!") 
				ENDIF
				#ENDIF
				
				IF (g_SpecificSpawnLocation.bUseAngledArea)
					SCRIPT_ASSERT("SPAWN_LOCATION_AT_SPECIFIC_COORDS - cannot use angled areas with this, use SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA instead")
				ELSE
			
					g_SpawnData.PrivateParams.vSearchCoord = g_SpecificSpawnLocation.vCoords
					g_SpawnData.PrivateParams.fRawHeading = g_SpecificSpawnLocation.fHeading
					g_SpawnData.PrivateParams.fSearchRadius = g_SpecificSpawnLocation.fRadius
					g_SpawnData.bDontAskPermission = TRUE // no need to ask server for permission as only a single point
					bUsesFixedPoints = TRUE	
					bForVehicle = FALSE

				ENDIF
			BREAK
			
			CASE SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE") NET_NL()
				#ENDIF	
				#IF IS_DEBUG_BUILD
				IF (VMAG(g_SpecificSpawnLocation.vCoords) = 0.0)
					NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE - specific coords are at origin!") NET_NL()
					SCRIPT_ASSERT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE - specific coords are at origin!") 
				ENDIF
				#ENDIF				
				IF (g_SpecificSpawnLocation.bUseAngledArea)
					SCRIPT_ASSERT("SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE - cannot use angled areas with this, use SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA instead")
				ELSE
					IF g_SpawnData.iQuestSpecificSpawnReply = QUERY_REPLY_YES					
						g_SpawnData.PrivateParams.vSearchCoord = g_SpecificSpawnLocation.vCoords
						g_SpawnData.PrivateParams.fRawHeading = g_SpecificSpawnLocation.fHeading
						g_SpawnData.PrivateParams.fSearchRadius = g_SpecificSpawnLocation.fRadius
						bUsesFixedPoints = TRUE
					ELSE				
						g_SpawnData.PrivateParams.vSearchCoord = g_SpecificSpawnLocation.vCoords
						g_SpawnData.PrivateParams.fRawHeading = g_SpecificSpawnLocation.fHeading
						g_SpawnData.PrivateParams.fSearchRadius = g_SpecificSpawnLocation.fRadius														
						g_SpawnData.vExclusionCoord = g_SpawnData.PrivateParams.vSearchCoord
						
						// we got negative response for the fixed coords so we dont want to respawn near them. 
						IF (g_SpecificSpawnLocation.fMinRadius < g_SpecificSpawnLocation.fRadius/2.0)
							g_SpawnData.fNearExclusionRadius = g_SpecificSpawnLocation.fRadius/2.0
						ELSE
							g_SpawnData.fNearExclusionRadius = g_SpecificSpawnLocation.fMinRadius
						ENDIF
						g_SpawnData.PublicParams.vAvoidCoords[0] = g_SpawnData.vExclusionCoord 
						g_SpawnData.PublicParams.fAvoidRadius[0] = g_SpawnData.fNearExclusionRadius
				
						g_SpawnData.PrivateParams.bVehiclesCanConsiderInactiveNodes = g_SpecificSpawnLocation.bVehiclesCanConsiderInactiveNodes
						
						g_SpawnData.PublicParams.bCloseToOriginAsPossible = TRUE	
						g_SpawnData.PublicParams.fUpperZLimitForNodes = g_SpecificSpawnLocation.fUpperZLimitForNodes
					ENDIF	
				ENDIF
				// is spawn area centre point in a global exclusion area arleady?
				IF IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(g_SpawnData.PrivateParams.vSearchCoord)
					g_SpawnData.bIgnoreGlobalExclusionCheck = TRUE
				ENDIF
			BREAK

		ENDSWITCH	
		
		
		// should we skip the face road check in DoSpawningShapeTest()
		IF (VMAG(g_SpawnData.PublicParams.vFacingCoords) > 0.0)
			PRINTLN("[spawning] GetSpawnLocationCoords - setting g_SpawnData.bIgnoreFaceRoad = TRUE")
			g_SpawnData.bIgnoreFaceRoad = TRUE
		ENDIF
		
		// if the search coords are near a hanger we owne (or boss owns) then switch off the global exclusion checks
		IF IsPointNearHangarLocalPlayerCanSpawnAt(g_SpawnData.PrivateParams.vSearchCoord, i) 
			PRINTLN("[spawning] GetSpawnLocationCoords - setting g_SpawnData.bIgnoreGlobalExclusionCheck = TRUE, near hangar. property ", i)
			g_SpawnData.bIgnoreGlobalExclusionCheck = TRUE
		ENDIF
			
		// if we want to find a car node do it here
		IF (bForVehicle)	
		AND (bDontUseVehicleNodes=FALSE) //OR (IS_THIS_MODEL_A_PLANE(g_SpawnData.MissionSpawnDetails.SpawnModel)))
			
			IF IsSpawningInPlaneOrHeli()			
			
				g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_FIND_AIR_SPAWN
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.iGetLocationState - going to SPAWN_LOCATION_STATE_FIND_AIR_SPAWN") NET_NL()
				
			ELSE		
			
				// should use off road checking?
				IF (g_SpawnData.MissionSpawnDetails.bUseOffRoadChecking)					
					PRINTLN("[spawning] GetSpawnLocationCoords - bUseOffRoadChecking = TRUE")					
					vCoords = g_SpawnData.PublicParams.vFacingCoords
					IF (VMAG(vCoords) = 0.0)
						vCoords = g_SpawnData.PrivateParams.vSearchCoord
						PRINTLN("[spawning] GetSpawnLocationCoords - bUseOffRoadChecking - using search coords, ", vCoords)
					ELSE
						PRINTLN("[spawning] GetSpawnLocationCoords - bUseOffRoadChecking - using facing coords, ", vCoords)	
					ENDIF					
					PRINTLN("[spawning] GetSpawnLocationCoords - bUseOffRoadChecking = TRUE")
					IF NOT IsPointOffRoad(vCoords)
						PRINTLN("[spawning] GetSpawnLocationCoords - point is NOT offroad")
						g_SpawnData.PrivateParams.bAllowFallbackToUseInactiveNodes = FALSE
					ELSE
						PRINTLN("[spawning] GetSpawnLocationCoords - point is offroad")	
					ENDIF
				ENDIF	
				
				// can we fallback to use navmesh?
				g_SpawnData.PrivateParams.bAllowFallbackToUseNavMesh = g_SpawnData.MissionSpawnDetails.bUseNavMeshFallback
				PRINTLN("[spawning] GetSpawnLocationCoords - g_SpawnData.PrivateParams.bAllowFallbackToUseNavMesh = ", g_SpawnData.PrivateParams.bAllowFallbackToUseNavMesh)
			
				NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.iGetLocationState - going to SPAWN_LOCATION_STATE_SEARCH_FOR_CAR_NODE") NET_NL()		
				g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_SEARCH_FOR_CAR_NODE
			
			ENDIF
		
		ELSE		
		
		
			IF (bUsesFixedPoints)
			
				// no need to refine the coords, we are using fixed points
				#IF IS_DEBUG_BUILD
					NET_PRINT("[CS_SPWN][spawning] GetSpawnLocationCoords - using fixed point = ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) NET_PRINT(", ") NET_PRINT_FLOAT(g_SpawnData.PrivateParams.fRawHeading) NET_NL()
				#ENDIF
				
				SpawnResults.vCoords[0] = g_SpawnData.PrivateParams.vSearchCoord
				SpawnResults.fHeading[0] = g_SpawnData.PrivateParams.fRawHeading
				
				// cleanup
				g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_INIT
				g_SpawnData.iGetLocationAttempts = 0

				// clear fallback attempts
				g_SpawnData.bFallbackUsed = FALSE
				g_SpawnData.iFallbackAttempts = 0	
				
				// clear data for making specific spawn queries
				g_SpawnData.bHasQueriedSpecificPosition = FALSE
				g_SpawnData.iQuestSpecificSpawnReply = 0
				g_SpawnData.iQuerySpecificSpawnState = 0
				
				g_SpawnData.fPitch = g_SpawnData.PrivateParams.fPitch
				
				ReturnedSpawnResults = SpawnResults		//DaveyG 2013/06/22 desperation fix for b*# 1504446, 1504599 - players spawning at origin
				
				RETURN(TRUE)				
			
			ELSE
			
				// start the search
				StartSearch()
				
			ENDIF

		ENDIF

	ENDIF
	
	IF (g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_SEARCH_FOR_CAR_NODE)		
	
		// for loading the car nodes, calculate the max min values
		IF (bForVehicle)
			
			SWITCH g_SpawnData.PrivateParams.iAreaShape
				CASE SPAWN_AREA_SHAPE_CIRCLE
					fMinX = g_SpawnData.PrivateParams.vSearchCoord.x - g_SpawnData.PrivateParams.fSearchRadius
					fMinY = g_SpawnData.PrivateParams.vSearchCoord.y - g_SpawnData.PrivateParams.fSearchRadius
					fMaxX = g_SpawnData.PrivateParams.vSearchCoord.x + g_SpawnData.PrivateParams.fSearchRadius
					fMaxY = g_SpawnData.PrivateParams.vSearchCoord.y + g_SpawnData.PrivateParams.fSearchRadius
				BREAK
				CASE SPAWN_AREA_SHAPE_BOX
					IF (g_SpawnData.PrivateParams.vAngledAreaPoint1.x < g_SpawnData.PrivateParams.vAngledAreaPoint2.x)
						fMinX = g_SpawnData.PrivateParams.vAngledAreaPoint1.x 
						fMaxX = g_SpawnData.PrivateParams.vAngledAreaPoint2.x 
					ELSE
						fMinX = g_SpawnData.PrivateParams.vAngledAreaPoint2.x 
						fMaxX = g_SpawnData.PrivateParams.vAngledAreaPoint1.x 
					ENDIF
					IF (g_SpawnData.PrivateParams.vAngledAreaPoint1.y < g_SpawnData.PrivateParams.vAngledAreaPoint2.y)
						fMinY = g_SpawnData.PrivateParams.vAngledAreaPoint1.y 
						fMaxY = g_SpawnData.PrivateParams.vAngledAreaPoint2.y 
					ELSE
						fMinY = g_SpawnData.PrivateParams.vAngledAreaPoint2.y 
						fMaxY = g_SpawnData.PrivateParams.vAngledAreaPoint1.y 
					ENDIF
				BREAK
				CASE SPAWN_AREA_SHAPE_ANGLED
					IF (g_SpawnData.PrivateParams.vAngledAreaPoint1.x < g_SpawnData.PrivateParams.vAngledAreaPoint2.x)
						fMinX = g_SpawnData.PrivateParams.vAngledAreaPoint1.x - (0.5 * g_SpawnData.PrivateParams.fAngledAreaWidth)
						fMaxX = g_SpawnData.PrivateParams.vAngledAreaPoint2.x + (0.5 * g_SpawnData.PrivateParams.fAngledAreaWidth)
					ELSE
						fMinX = g_SpawnData.PrivateParams.vAngledAreaPoint2.x - (0.5 * g_SpawnData.PrivateParams.fAngledAreaWidth)
						fMaxX = g_SpawnData.PrivateParams.vAngledAreaPoint1.x + (0.5 * g_SpawnData.PrivateParams.fAngledAreaWidth)
					ENDIF
					IF (g_SpawnData.PrivateParams.vAngledAreaPoint1.y < g_SpawnData.PrivateParams.vAngledAreaPoint2.y)
						fMinY = g_SpawnData.PrivateParams.vAngledAreaPoint1.y - (0.5 * g_SpawnData.PrivateParams.fAngledAreaWidth)
						fMaxY = g_SpawnData.PrivateParams.vAngledAreaPoint2.y + (0.5 * g_SpawnData.PrivateParams.fAngledAreaWidth)
					ELSE
						fMinY = g_SpawnData.PrivateParams.vAngledAreaPoint2.y - (0.5 * g_SpawnData.PrivateParams.fAngledAreaWidth)
						fMaxY = g_SpawnData.PrivateParams.vAngledAreaPoint1.y + (0.5 * g_SpawnData.PrivateParams.fAngledAreaWidth)
					ENDIF
				BREAK
			ENDSWITCH
			

		ENDIF	
		REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(fMinX , fMinY, fMaxX, fMaxY)
			
	
		IF ARE_NODES_LOADED_FOR_AREA(fMinX, fMinY, fMaxX, fMaxY)
				
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] GetSpawnLocationCoords - nodes loaded for area = ") 
			NET_PRINT_FLOAT(fMinX) NET_PRINT(", ")
			NET_PRINT_FLOAT(fMinY) NET_PRINT(", ")
			NET_PRINT_FLOAT(fMaxX) NET_PRINT(", ")
			NET_PRINT_FLOAT(fMaxY)
			NET_NL()
			#ENDIF				
				
			// get the nearest car node.
			IF (g_SpawnData.PrivateParams.bUseOffsetOrigin)
				SpawnResults.vCoords[0] = g_SpawnData.PrivateParams.vOffsetOrigin
				SpawnResults.fHeading[0] = g_SpawnData.PrivateParams.fRawHeading			
			ELSE
				SpawnResults.vCoords[0] = g_SpawnData.PrivateParams.vSearchCoord
				SpawnResults.fHeading[0] = g_SpawnData.PrivateParams.fRawHeading
			ENDIF

			
			NearestCarNodeSearch SearchParams
			SearchParams.vFavourFacing = g_SpawnData.PublicParams.vFacingCoords
			SearchParams.fMinDistFromCoords = g_SpawnData.fNearExclusionRadius
			SearchParams.bCheckVehicleSpawning = bForVehicle // check that no other vehicle is spawning/warping to this location. fix for 1985796
			SearchParams.fMinPlayerDist = g_SpawnData.PublicParams.fMinDistFromPlayer
			SearchParams.fMaxDistance = fMaxDistFromSearchPoint // set in SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION, because we already know pretty much where we want to spawn. 
			SearchParams.bAllowFallbackToInactiveNodes = g_SpawnData.PrivateParams.bAllowFallbackToUseInactiveNodes
			IF (g_SpawnData.PrivateParams.bVehiclesCanConsiderInactiveNodes)
				SearchParams.bConsiderOnlyActiveNodes = FALSE
			ENDIF
			IF (g_SpawnData.PublicParams.fUpperZLimitForNodes > 0.0)
				SearchParams.fUpperZLimit = g_SpawnData.PublicParams.fUpperZLimitForNodes
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX VehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(VehID)
					SearchParams.CarModel = GET_ENTITY_MODEL(VehID)
				ENDIF						
			ENDIF
			IF eOverrideModel != DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("[spawning] GetSpawnLocationCoords - using overridden model with ID: ", eOverrideModel)
				SearchParams.CarModel = eOverrideModel
			ENDIF
			
			SearchParams.bCheckInsideArea = TRUE // added for 1664739, need to check that car nodes are firstly within the search area
			SWITCH g_SpawnData.PrivateParams.iAreaShape
				CASE SPAWN_AREA_SHAPE_CIRCLE
					SearchParams.vAreaCoords1 = g_SpawnData.PrivateParams.vSearchCoord
					SearchParams.fAreaFloat = g_SpawnData.PrivateParams.fSearchRadius
					SearchParams.fMinDistFromCoords = 0.0
				BREAK
				CASE SPAWN_AREA_SHAPE_BOX
					SearchParams.vAreaCoords1 = g_SpawnData.PrivateParams.vAngledAreaPoint1
					SearchParams.vAreaCoords2 = g_SpawnData.PrivateParams.vAngledAreaPoint2
					SearchParams.fAreaFloat = 0.0
				BREAK
				CASE SPAWN_AREA_SHAPE_ANGLED
					SearchParams.vAreaCoords1 = g_SpawnData.PrivateParams.vAngledAreaPoint1
					SearchParams.vAreaCoords2 = g_SpawnData.PrivateParams.vAngledAreaPoint2
					SearchParams.fAreaFloat = g_SpawnData.PrivateParams.fAngledAreaWidth
				BREAK
			ENDSWITCH
			SearchParams.iAreaShape = g_SpawnData.PrivateParams.iAreaShape			
			
			// if on a vehicle impromptu dm also check inactive nodes. (if died in global exclusion area)
			IF IS_PLAYER_ON_IMPROMPTU_DM()
			AND IS_SPAWNING_IN_VEHICLE()
			AND IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(g_SpawnData.vMyDeadCoords)
				SearchParams.bConsiderOnlyActiveNodes = FALSE
			ENDIF
						
				
			SearchParams.bGetSafeOffset = bUseCarNodeOffset
			SearchParams.bWaterNodesOnly = ShouldOnlyConsiderBoatNodes()
		
			// make sure by default we are using nearest or random nodes. - this is to ensure it will return more than one result. 
			IF (g_SpawnData.PublicParams.bCloseToOriginAsPossible)
				SearchParams.bReturnNearestGoodNode = TRUE
				SearchParams.bReturnRandomGoodNode = FALSE
			ELSE
				SearchParams.bReturnNearestGoodNode = FALSE
				SearchParams.bReturnRandomGoodNode = TRUE
			ENDIF
		
			IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_MISSION_AREA)
				SearchParams.bAllowReducingVisibility = TRUE
				SearchParams.bReturnRandomGoodNode = TRUE
				SearchParams.bReturnNearestGoodNode = FALSE
			ELIF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION)
				SearchParams.bAllowReducingVisibility = TRUE
				SearchParams.bReturnNearestGoodNode = TRUE
				SearchParams.bReturnRandomGoodNode = FALSE
				SearchParams.bUsingOffsetOrigin = TRUE
			ENDIF
			
			// if spawning near death dont consider nodes more than 15m below. 
			IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_DEATH)
				SearchParams.fLowerZLimit = 15.0
			ENDIF
			
			// should we use less strict visibility / enemy checks?
			IF (g_SpawnData.MissionSpawnDetails.fCarNodeLowerZLimit > 0.0)
				SearchParams.fLowerZLimit = g_SpawnData.MissionSpawnDetails.fCarNodeLowerZLimit
				PRINTLN("[spawning] GetSpawnLocationCoords - g_SpawnData.MissionSpawnDetails.fCarNodeLowerZLimit = ", g_SpawnData.MissionSpawnDetails.fCarNodeLowerZLimit)
			ENDIF
			
			//SearchParams.vAvoidCoords = g_SpawnData.PublicParams.vAvoidCoords
			//SearchParams.fAvoidRadius =	g_SpawnData.PublicParams.fAvoidRadius
		
			SearchParams.bStartOfMissionPVSpawn = bUseStartOfMissionPVSpawn
			SearchParams.bDoVisibilityChecks = g_SpawnData.PrivateParams.bDoVisibleChecks
			
			SearchParams.bCanFaceOncomingTraffic = g_SpawnData.PrivateParams.bCanFaceOncomingTraffic
			
			// if player is warping themselves then ignore
			IF IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID())
				SearchParams.bIgnoreLocalPlayerChecks = TRUE
				PRINTLN("[spawning] GetSpawnLocationCoords - player is doing a warp with vehicle, so ignore self for node checks")
			ENDIF
		
			GetNearestCarNode(SpawnResults.vCoords[0], SpawnResults.fHeading[0], SearchParams) //g_SpawnData.PublicParams.vFacingCoords, TRUE, bUseCarNodeOffset, 0, TRUE, ShouldOnlyConsiderBoatNodes(), g_SpawnData.fNearExclusionRadius)
			
			// store the backup good results from the node search also
			REPEAT NUM_OF_STORED_SPAWN_RESULTS i
				IF (i < g_NearestCarNodeResults.iNumResults)
				AND (i > 0)
					SpawnResults.vCoords[i] = g_NearestCarNodeResults.vCoords[i]
					SpawnResults.fHeading[i] = g_NearestCarNodeResults.fHeading[i]
					PRINTLN("[spawning] GetSpawnLocationCoords - storing node ", i, ", ", SpawnResults.vCoords[i], SpawnResults.fHeading[i])
				ENDIF
			ENDREPEAT
			
			// if we are trying to spawn in the water, but couldn't find a water coord then spawn the player on foot.
			IF (SearchParams.bWaterNodesOnly)
			AND (SearchParams.bCouldNotFindWaterNode)
				g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle = TRUE					
			ELSE
				g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle = FALSE
			ENDIF
			NET_PRINT("[spawning] GetSpawnLocationCoords - g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle - set to ") NET_PRINT_BOOL(g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle) NET_NL()
			
			g_SpawnData.iGetLocationCarNodeSearchAttempts++
			g_SpawnData.iPreviousGetLocationState = SPAWN_LOCATION_STATE_SEARCH_FOR_CAR_NODE
			g_SpawnData.iGetLocationState =  SPAWN_LOCATION_STATE_CHECK_INITIAL_RESULTS
			NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_STATE_SEARCH_FOR_CAR_NODE - going to SPAWN_LOCATION_STATE_CHECK_INITIAL_RESULTS") NET_NL()

			
		ELSE
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] GetSpawnLocationCoords - loading car nodes...")  NET_NL()
			#ENDIF
		ENDIF
	ENDIF
	
	IF (g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_FIND_AIR_SPAWN)			
	
		REPEAT NUM_OF_STORED_SPAWN_RESULTS i					
		
			// did we already find a suitable spawn point?		
			PRINTLN("[spawning] SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION, i = ", i, " g_SpawnData.SpawnLocation = ", g_SpawnData.SpawnLocation, ", g_SpawnData.PrivateParams.bFoundACarNode = ", g_SpawnData.PrivateParams.bFoundACarNode, " g_SpawnData.PrivateParams.vOffsetOrigin = ", g_SpawnData.PrivateParams.vOffsetOrigin)				
			SpawnResults.vCoords[i] = g_SpawnData.PrivateParams.vOffsetOrigin
			IF (i = 0)
			AND (g_SpawnData.SpawnLocation = SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION)
			AND (g_SpawnData.PrivateParams.bFoundACarNode)
			AND (IsPotentialSpawnPointInAirSafe(SpawnResults.vCoords[i], IS_POINT_IN_MISSION_SPAWN_AREA(SpawnResults.vCoords[i])))
				NET_PRINT("[spawning] GetSpawnLocationCoords - plane or heli - using node point ")	
			ELSE

				SWITCH g_SpawnData.PrivateParams.iAreaShape
					CASE SPAWN_AREA_SHAPE_CIRCLE
						SpawnResults.vCoords[i] = GetPotentialSpawnPointInAirForLocalPlayer(g_SpawnData.PrivateParams.vSearchCoord, <<0.0,0.0,0.0>>, g_SpawnData.PrivateParams.fSearchRadius, g_SpawnData.PrivateParams.iAreaShape)
					BREAK
					CASE SPAWN_AREA_SHAPE_BOX
						SpawnResults.vCoords[i] = GetPotentialSpawnPointInAirForLocalPlayer(g_SpawnData.PrivateParams.vAngledAreaPoint1, g_SpawnData.PrivateParams.vAngledAreaPoint2, 0.0, g_SpawnData.PrivateParams.iAreaShape)			
					BREAK
					CASE SPAWN_AREA_SHAPE_ANGLED
						SpawnResults.vCoords[i] = GetPotentialSpawnPointInAirForLocalPlayer(g_SpawnData.PrivateParams.vAngledAreaPoint1, g_SpawnData.PrivateParams.vAngledAreaPoint2, g_SpawnData.PrivateParams.fAngledAreaWidth, g_SpawnData.PrivateParams.iAreaShape)			
					BREAK	
				ENDSWITCH
			
			ENDIF
			
			// because we are in the air we can make the vehicle face the desired coords directly.
			IF VMAG(g_SpawnData.PublicParams.vFacingCoords) > 0.0
				vCoords = g_SpawnData.PublicParams.vFacingCoords - SpawnResults.vCoords[i]
				SpawnResults.fHeading[i] = GET_HEADING_FROM_VECTOR_2D(vCoords.x, vCoords.y)
			ENDIF		
			
			NET_PRINT("[spawning] GetSpawnLocationCoords - plane or heli - SpawnResults.vCoords[") NET_PRINT_INT(i) NET_PRINT("] = ") NET_PRINT_VECTOR(SpawnResults.vCoords[i])
			NET_PRINT(", heading = ") NET_PRINT_FLOAT(SpawnResults.fHeading[i]) NET_NL()
				
		ENDREPEAT

		g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle = FALSE
		
		g_SpawnData.iGetLocationCarNodeSearchAttempts++
		g_SpawnData.iPreviousGetLocationState = SPAWN_LOCATION_STATE_FIND_AIR_SPAWN
		g_SpawnData.iGetLocationState =  SPAWN_LOCATION_STATE_CHECK_INITIAL_RESULTS
		
		NET_PRINT("[spawning] GetSpawnLocationCoords - SPAWN_LOCATION_STATE_FIND_AIR_SPAWN - going to SPAWN_LOCATION_STATE_CHECK_INITIAL_RESULTS") NET_NL()
		
	ENDIF
	
	IF (g_SpawnData.iGetLocationState =  SPAWN_LOCATION_STATE_CHECK_INITIAL_RESULTS)
	
		// if this is an override from spawn_at_specific_coords, then check these coords are not further from the next checkpoint than the previous checkpoint.
		IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS)
		AND (g_SpawnData.MissionSpawnDetails.fSpecificSpawnLocationOverride > 0.0)		
		
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - override SPECIFIC_COORDS - g_SpawnData.PrivateParams.vSearchCoord = ") NET_PRINT_VECTOR(g_SpawnData.PrivateParams.vSearchCoord) 
				NET_PRINT(" vNextRaceCheckpoint") NET_PRINT_VECTOR(g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint)
				NET_PRINT(" dist = ") NET_PRINT_FLOAT(VDIST(SpawnResults.vCoords[0], g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint))
				NET_PRINT(" g_SpecificSpawnLocation.vCoords = ") NET_PRINT_VECTOR(g_SpecificSpawnLocation.vCoords)
				NET_PRINT(" dist = ") NET_PRINT_FLOAT(VDIST(g_SpecificSpawnLocation.vCoords, g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint)) 
				NET_NL()
			#ENDIF
		
			IF (VDIST(SpawnResults.vCoords[0], g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint) > VDIST(g_SpecificSpawnLocation.vCoords, g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint))
				SpawnResults.vCoords[0] = g_SpecificSpawnLocation.vCoords
				SpawnResults.fHeading[0] = g_SpecificSpawnLocation.fHeading
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GetSpawnLocationCoords - override SPECIFIC_COORDS - car node is further than last checkpoint") NET_NL()
				#ENDIF
			ENDIF			
		ENDIF
		
		
		// if this is a spawn area is it inside the area?
		REPEAT NUM_OF_STORED_SPAWN_RESULTS i
		
			IF IS_SPAWN_LOCATION_MISSION_AREA_CHECK(g_SpawnData.SpawnLocation) 
			AND NOT IS_POINT_IN_MISSION_SPAWN_AREA(SpawnResults.vCoords[i], 0.01)
			AND NOT IsSpawningInPlaneOrHeli()
				PRINTLN("[spawning] GetSpawnLocationCoords - car node ", i, SpawnResults.vCoords[i], SpawnResults.fHeading[i], " no good (not inside area).")		
				IF (i = NUM_OF_STORED_SPAWN_RESULTS-1)
				OR (VMAG(SpawnResults.vCoords[i+1]) = 0.0)
					PRINTLN("[spawning] GetSpawnLocationCoords - car nodes not inside area. ")					
					IF ((g_SpawnData.iPreviousGetLocationState = SPAWN_LOCATION_STATE_FIND_AIR_SPAWN) OR (g_SpawnData.iPreviousGetLocationState = SPAWN_LOCATION_STATE_SEARCH_FOR_CAR_NODE))
					AND (g_SpawnData.iGetLocationCarNodeSearchAttempts < MAX_NUM_OF_GET_SPAWN_LOCATION_NODE_SEARCHES)
						g_SpawnData.iGetLocationState = g_SpawnData.iPreviousGetLocationState
						i = NUM_OF_STORED_SPAWN_RESULTS
						PRINTLN("[spawning] GetSpawnLocationCoords - going back to try a new search. ")
					ELSE
						PRINTLN("[spawning] GetSpawnLocationCoords - use nav mesh. ")		
						IF (g_SpawnData.PrivateParams.bAllowFallbackToUseNavMesh)
							StartSearch()	
							RETURN FALSE
						ELSE
							PRINTLN("[spawning] GetSpawnLocationCoords - GetSpawnLocationCoords_GotoFinalChecks 1")
							GetSpawnLocationCoords_GotoFinalChecks(SpawnResults, i)
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[spawning] GetSpawnLocationCoords - got another to try")
				ENDIF
			ELSE
			
				// check that the car node is suitable to spawn, another player is already there or about to spawn there. 
				IF IS_ANY_PLAYER_NEAR_POINT(SpawnResults.vCoords[i], 3.5)
				OR IsAnotherPlayerOrPlayerVehicleSpawningAtPosition(SpawnResults.vCoords[i], 0.5, PLAYER_ID())
				OR (g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle)					
					PRINTLN("[spawning] GetSpawnLocationCoords - car node ", i, SpawnResults.vCoords[i], SpawnResults.fHeading[i], " no good (not safe to spawn).")					
					IF (i = NUM_OF_STORED_SPAWN_RESULTS-1)
					OR (VMAG(SpawnResults.vCoords[i+1]) = 0.0)					
						PRINTLN("[spawning] GetSpawnLocationCoords - car nodes not safe to spawn. ")
						IF ((g_SpawnData.iPreviousGetLocationState = SPAWN_LOCATION_STATE_FIND_AIR_SPAWN) OR (g_SpawnData.iPreviousGetLocationState = SPAWN_LOCATION_STATE_SEARCH_FOR_CAR_NODE))
						AND (g_SpawnData.iGetLocationCarNodeSearchAttempts < MAX_NUM_OF_GET_SPAWN_LOCATION_NODE_SEARCHES)
							g_SpawnData.iGetLocationState = g_SpawnData.iPreviousGetLocationState
							i = NUM_OF_STORED_SPAWN_RESULTS
							PRINTLN("[spawning] GetSpawnLocationCoords - going back to try a new search. ")
						ELSE
							PRINTLN("[spawning] GetSpawnLocationCoords - use nav mesh. ")	
							IF (g_SpawnData.PrivateParams.bAllowFallbackToUseNavMesh)
								StartSearch()	
								RETURN FALSE
							ELSE
								PRINTLN("[spawning] GetSpawnLocationCoords - GetSpawnLocationCoords_GotoFinalChecks 2")
								GetSpawnLocationCoords_GotoFinalChecks(SpawnResults, i)
							ENDIF
						ENDIF						
					ELSE
						PRINTLN("[spawning] GetSpawnLocationCoords - got another to try")
					ENDIF
				ELSE
					PRINTLN("[spawning] GetSpawnLocationCoords - GetSpawnLocationCoords_GotoFinalChecks 3")
					GetSpawnLocationCoords_GotoFinalChecks(SpawnResults, i)
			
				ENDIF
			ENDIF
			
		ENDREPEAT
		
	ENDIF
	
	IF (g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_DO_NEW_RESPAWN_SEARCH)
	
		// if on last attempt dont bother with teammate vis checks
		IF (g_SpawnData.iGetLocationAttempts = (MAX_NUM_OF_GET_SPAWN_LOCATION_ATTEMPTS-1))
		OR (g_SpawnData.PrivateParams.bDoVisibleChecks = FALSE)
			bTeamMateVisCheck = FALSE
		ELSE
			bTeamMateVisCheck = bDoTeamMateVisCheck
		ENDIF
	
		IF (g_SpawnData.PrivateParams.iAreaShape = 0)
			IF (g_SpawnData.PublicParams.fMinDistFromPlayer > g_SpawnData.PrivateParams.fSearchRadius*2.0)
				g_SpawnData.PublicParams.fMinDistFromPlayer = g_SpawnData.PrivateParams.fSearchRadius*2.0
				PRINTLN("[spawning] GetSpawnLocationCoords - setting g_SpawnData.PublicParams.fMinDistFromPlayer to ", g_SpawnData.PublicParams.fMinDistFromPlayer)
			ENDIF
		ENDIF
		
		g_SpawnData.PrivateParams.bIsForLocalPlayerSpawning = TRUE
		g_SpawnData.PrivateParams.bDoTeamMateVisCheck = bTeamMateVisCheck
		g_SpawnData.PrivateParams.bForAVehicle = g_SpawnData.MissionSpawnDetails.bSpawnInVehicle
		g_SpawnData.PrivateParams.bCanFaceOncomingTraffic = g_SpawnData.MissionSpawnDetails.bCanFaceOncomingTraffic
		
		g_SpawnData.PublicParams.bSearchVehicleNodesOnly = FALSE
		g_SpawnData.PublicParams.bUseOnlyBoatNodes = ShouldOnlyConsiderBoatNodes()
		g_SpawnData.PublicParams.bEdgesOnly = FALSE
	
		//IF DoRespawnSearchForCoord(g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading, g_SpawnData.PrivateParams.fSearchRadius, SpawnResults, TRUE, g_SpawnData.PublicParams.vFacingCoords, bTeamMateVisCheck, g_SpawnData.PublicParams.bConsiderInteriors, g_SpawnData.PublicParams.bPreferPointsCloserToRoads, g_SpawnData.PublicParams.fMinDistFromPlayer, g_SpawnData.PublicParams.bCloseToOriginAsPossible, g_SpawnData.PrivateParams.bIsAngledArea, g_SpawnData.PrivateParams.vAngledAreaPoint1, g_SpawnData.PrivateParams.vAngledAreaPoint2, g_SpawnData.PrivateParams.fAngledAreaWidth, g_SpawnData.PublicParams.bConsiderOriginAsValidPoint, FALSE, ShouldOnlyConsiderBoatNodes(), g_SpawnData.MissionSpawnDetails.bSpawnInVehicle, FALSE, g_SpawnData.PrivateParams.bDoVisibleChecks, g_SpawnData.PrivateParams.bIgnoreTeammatesForMinDistCheck)
		IF DoRespawnSearchForCoord(g_SpawnData.PrivateParams, g_SpawnData.PublicParams, SpawnResults)
			g_SpawnData.bRespawnSearchStarted = FALSE
			g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_FINAL_CHECKS	
		ENDIF
	ENDIF
	

	IF (g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_FINAL_CHECKS)
		
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] GetSpawnLocationCoords - SUCCESS!!  SpawnResults = ") PrintSpawnResultStruct(SpawnResults) NET_NL()
		#ENDIF

		#IF IS_DEBUG_BUILD
			IF (bOverrideSpawnSearchResults)
				NET_PRINT("[spawning] GetSpawnLocationCoords - DEBUG OVERRIDING SPAWN RESULTS with ") NET_PRINT_VECTOR(bOverrideSpawnSearchPos) NET_PRINT(", ") NET_PRINT_FLOAT(fOverrideSpawnSearchHeading) NET_NL()
				SpawnResults.vCoords[0] = bOverrideSpawnSearchPos
				SpawnResults.fHeading[0] = fOverrideSpawnSearchHeading
			ENDIF
		#ENDIF

		ReturnedSpawnResults = SpawnResults
		
		// cleanup
		g_SpawnData.iGetLocationState = SPAWN_LOCATION_STATE_INIT
		g_SpawnData.iGetLocationAttempts = 0

		// clear fallback attempts
		g_SpawnData.bFallbackUsed = FALSE
		g_SpawnData.iFallbackAttempts = 0	
		
		// clear data for making specific spawn queries
		g_SpawnData.bHasQueriedSpecificPosition = FALSE
		g_SpawnData.iQuestSpecificSpawnReply = 0
		g_SpawnData.iQuerySpecificSpawnState = 0		
		
		RETURN(TRUE)	
	ENDIF
	
	
	RETURN(FALSE)

ENDFUNC

#IF IS_DEBUG_BUILD
PROC StoreReturnedSafeCoordsForCreatingEntity(VECTOR vPos, FLOAT fHeading)
	INT i
	INT iIndex		
	REPEAT NUMBER_OF_RESULTS_STORED_FOR_GETTING_SAFE_COORDS_DEBUG i
		iIndex = (NUMBER_OF_RESULTS_STORED_FOR_GETTING_SAFE_COORDS_DEBUG - i) - 1
		// move everyone up 1
		IF (iIndex > 0)			
			g_SpawnData.LastCoordsReturnedForGetSafeCoords[iIndex] = g_SpawnData.LastCoordsReturnedForGetSafeCoords[iIndex - 1]	
		ELSE
		// store most recent at 0
			g_SpawnData.LastCoordsReturnedForGetSafeCoords[iIndex].Pos = vPos
			g_SpawnData.LastCoordsReturnedForGetSafeCoords[iIndex].Heading = fHeading
		ENDIF		
	ENDREPEAT	
ENDPROC
#ENDIF



#IF IS_DEBUG_BUILD
PROC DebugPrintWarpRequestInfo()
	NET_PRINT("[spawning] iWarpState = ") NET_PRINT_INT(g_SpawnData.iWarpState) NET_NL()
	NET_PRINT("[spawning] iWarpServerRequestCount = ") NET_PRINT_INT(g_SpawnData.iWarpServerRequestCount) NET_NL()
	//NET_PRINT("[spawning] iWarpRequestTime = ") NET_PRINT_INT(g_SpawnData.iWarpRequestTime) NET_NL()
	NET_PRINT("[spawning] RequestedSpawnResults = ") PrintSpawnResultStruct(g_SpawnData.RequestedSpawnResults) NET_NL()
	NET_PRINT("[spawning] bServerRepliedToWarpRequest = ") NET_PRINT_BOOL(g_SpawnData.bServerRepliedToWarpRequest) NET_NL()
	NET_PRINT("[spawning] bServerAgreedToWarpRequest = ") NET_PRINT_BOOL(g_SpawnData.bServerAgreedToWarpRequest) NET_NL()
ENDPROC
#ENDIF

FUNC MODEL_NAMES GetGangCarModelInMemory()
	MODEL_NAMES CarModel
	INT iVehicleClass
	CarModel = GET_TEAM_VEHICLE_MODEL(GET_STAT_CHARACTER_TEAM())
	REQUEST_MODEL(CarModel)
	IF HAS_MODEL_LOADED(CarModel)
		SET_MODEL_AS_NO_LONGER_NEEDED(CarModel)
		RETURN(CarModel)
	ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED(CarModel)
		GET_RANDOM_VEHICLE_MODEL_IN_MEMORY(TRUE, CarModel, iVehicleClass)
	ENDIF
	RETURN(CarModel)
ENDFUNC



FUNC BOOL IsPlayerInSameCarAsMe(PLAYER_INDEX PlayerID)
	VEHICLE_INDEX CarID
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
			CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_NET_PLAYER_OK(PlayerID)
				IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID))
					IF (CarID = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PlayerID)))
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL isPlayerPartnerBackInVehicle(PLAYER_INDEX OtherPlayerID)
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	AND NOT g_bMissionEnding 
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen < FMMC_MAX_TEAMS 
		AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen > -1
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
				NET_PRINT("[spawning] isPlayerPartnerBackInVehicle - We are using: ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN")
				
				IF NOT IS_PED_INJURED(GET_PLAYER_PED(OtherPlayerID))
					IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(OtherPlayerID))
						NET_PRINT("[spawning] isPlayerPartnerBackInVehicle - Player Partner is back in their vehicle, Returning True.")
						RETURN(TRUE)
					ENDIF
				ENDIF
				NET_PRINT("[spawning] isPlayerPartnerBackInVehicle - Returning False")
				RETURN(FALSE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL WasPlayerInSameVehicleAsPlayer(PLAYER_INDEX OtherPlayerID)
	
	IF (GET_FRAME_COUNT() % 5) = 0 
		NET_PRINT("[spawning] WasPlayerInSameVehicleAsPlayer - g_SpawnData.iBS_InCarWith = ") NET_PRINT_INT(g_SpawnData.iBS_InCarWith) NET_NL()
	ENDIF
	
	IF NOT (g_SpawnData.MyLastVehicle = INT_TO_NATIVE(VEHICLE_INDEX, -1))
		IF IS_BIT_SET(g_SpawnData.iBS_InCarWith, NATIVE_TO_INT(OtherPlayerID))	
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL DoIHaveAVehiclePartner(PLAYER_INDEX &ReturnPlayerID)
	INT i
	PLAYER_INDEX PlayerID
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION 
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen < FMMC_MAX_TEAMS 
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen > -1
	AND	IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
	AND NOT g_bMissionEnding 
		// Return False if we have failed or completed the mission.
		
		// Everyone in the same Team.
		IF IS_RACE_PASSENGER()
			INT iMissionPartner = GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())
			
			IF iMissionPartner > -1
				PLAYER_INDEX MissionPartnerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iMissionPartner))
				ReturnPlayerID = MissionPartnerID
			ENDIF
			
			IF ReturnPlayerID != INVALID_PLAYER_INDEX()
			AND NETWORK_IS_PLAYER_ACTIVE(ReturnPlayerID)
				PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - [MULTI_VEH_RESPAWN] (Passenger) iMissionPartner:  ", iMissionPartner, " Returning Player Name: ", GET_PLAYER_NAME(ReturnPlayerID))						
				RETURN TRUE
			ELSE
				PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - [MULTI_VEH_RESPAWN] (Passenger) ReturnPlayerID is INVALID, using iMissionPartner:  ", iMissionPartner)
				RETURN FALSE
			ENDIF
		ELSE			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen].iTeamBitSet3, ciBS3_ENABLE_DRIVER_RESPAWN_SAME_AVAILABLE_VEH)
				PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - [MULTI_VEH_RESPAWN] (Driver) Returning False so that we make a new car.")
				RETURN FALSE
			ELSE
				PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - [MULTI_VEH_RESPAWN] (Driver) ciBS3_ENABLE_DRIVER_RESPAWN_SAME_AVAILABLE_VEH is SET.")
			ENDIF
			
			BOOL bCanRespawnInPartnerVeh
			INT iMissionPartnerTemp
			// The driver might need to be mindful of his passengers if we want him to respawn as the driver. Else he can just create a new vehicle and force respawn the passengers?
			FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
				iMissionPartnerTemp = GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)
				IF iMissionPartnerTemp > -1
					PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iMissionPartnerTemp)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
						PLAYER_INDEX MissionPartnerID = NETWORK_GET_PLAYER_INDEX(piPart)
						PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - [MULTI_VEH_RESPAWN] (Driver) i: ", i, " iMissionPartner:  ", iMissionPartnerTemp, " Player Name: ", GET_PLAYER_NAME(MissionPartnerID))
						
						// We have assigned someone valid. Breakloop.
						IF NOT IS_PLAYER_RESPAWNING(MissionPartnerID)
						AND NOT IS_PED_INJURED(GET_PLAYER_PED(MissionPartnerID))
							ReturnPlayerID = MissionPartnerID
							bCanRespawnInPartnerVeh = TRUE
							PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - [MULTI_VEH_RESPAWN] (Driver) BREAKLOOP")
							BREAKLOOP
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			// This should only return true if the above loop successfully found a partner.
			IF bCanRespawnInPartnerVeh
				PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - [MULTI_VEH_RESPAWN] (Driver) RETURNING TRUE")
				RETURN TRUE
			ELSE
				PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - [MULTI_VEH_RESPAWN] (Driver) RETURNING FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
		//Separate team version (Driver + Gunner) Can be chosen based on Teams.
			
	ELIF NOT IS_ON_GTA_RACE_GLOBAL_SET()
		REPEAT NUM_NETWORK_PLAYERS i
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
			IF NETWORK_IS_PLAYER_ACTIVE(PlayerID)
				IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
					IF NOT (PlayerID = PLAYER_ID())
						IF (GET_PLAYER_TEAM(PlayerID) = GET_PLAYER_TEAM(PLAYER_ID()))			
							IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)	
								ReturnPlayerID = PlayerID
								RETURN(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		ReturnPlayerID = INT_TO_NATIVE(PLAYER_INDEX, -1)
	ELSE
		REPEAT NUM_NETWORK_PLAYERS i
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
			IF NETWORK_IS_PLAYER_ACTIVE(PlayerID)
				IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
					IF NOT (PlayerID = PLAYER_ID())
						IF WasPlayerInSameVehicleAsPlayer(PlayerID)
						AND (GET_PLAYER_TEAM(PlayerID) = GET_PLAYER_TEAM(PLAYER_ID()))
							IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)	
								ReturnPlayerID = PlayerID
								RETURN(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		ReturnPlayerID = INT_TO_NATIVE(PLAYER_INDEX, -1)	
	ENDIF
		
	RETURN(FALSE)	
ENDFUNC

FUNC BOOL IsRespawnPassenger()
	PLAYER_INDEX PartnerID
	IF IS_ON_GTA_RACE_GLOBAL_SET()
		IF DoIHaveAVehiclePartner(PartnerID)
			IF WasPlayerInSameVehicleAsPlayer(PartnerID)
				IF NOT (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bIWasDriving)
					RETURN(TRUE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_RACE_PASSENGER()		
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL DoesSpecificRespawnVehicleExist()
	IF DOES_ENTITY_EXIST(g_SpawnData.SpecificVehicleID)
	AND NOT IS_ENTITY_DEAD(g_SpawnData.SpecificVehicleID)
			#IF IS_DEBUG_BUILD
			NET_PRINT("DoesSpecificRespawnVehicleExist - Returning TRUE.")
			#ENDIF
		RETURN(TRUE)
	ENDIF
	#IF IS_DEBUG_BUILD
	NET_PRINT("DoesSpecificRespawnVehicleExist - Returning FALSE.")
	#ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsSpecificRespawnVehicleVisible()
	IF DOES_ENTITY_EXIST(g_SpawnData.SpecificVehicleID)
	AND NOT IS_ENTITY_DEAD(g_SpawnData.SpecificVehicleID)
		RETURN IS_ENTITY_VISIBLE(g_SpawnData.SpecificVehicleID)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ShouldUseSameVehicle()
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION 
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen < FMMC_MAX_TEAMS 
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen > -1
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)	
	AND NOT g_bMissionEnding 
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen].iTeamBitSet3, ciBS3_ENABLE_DRIVER_RESPAWN_SAME_AVAILABLE_VEH)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IsTeamModeSetForRespawning()
	IF IS_ON_TEAM_RACE_GLOBAL_SET()
	OR (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION 
		AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen < FMMC_MAX_TEAMS 
		AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen > -1
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN))
		AND NOT g_bMissionEnding 
		// check that i have a partner to spawn in with
		PLAYER_INDEX PartnerID
		IF DoIHaveAVehiclePartner(PartnerID)
			NET_PRINT("IsTeamModeSetForRespawning - DoIHaveAVehiclePartner = TRUE") NET_NL()
			RETURN(TRUE)
		ELSE
			RETURN(FALSE)
		ENDIF
	ENDIF	
	/*//*1426249 - Added by Ben.Hinchliffe 30/05/13
	ELSE
		IF IS_RACE_TEAM_MODE_SET()
			NET_PRINT("IsTeamModeSetForRespawning - IS_RACE_TEAM_MODE_SET = TRUE") NET_NL()
			RETURN(TRUE)
		ENDIF
	ENDIF
	*/
	RETURN(FALSE)
ENDFUNC


FUNC BOOL IsPlayerInDriveableCar(PLAYER_INDEX PlayerID)
	
	// has he spawned in his car?
	IF IS_NET_PLAYER_OK(PlayerID)		
		IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID))
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PlayerID)))
				NET_PRINT("[spawning] IsPlayerInDriveableCar - player is ready.") NET_NL()
				RETURN(TRUE)
			ELSE
				NET_PRINT("[spawning] IsPlayerInDriveableCar - IS_VEHICLE_DRIVEABLE = FALSE.") NET_NL()
				RETURN(FALSE)							
			ENDIF
		ELSE
			PRINTLN("[spawning] IsPlayerInDriveableCar - IS_PED_IN_ANY_VEHICLE(PlayerID) = FALSE.")
			RETURN(FALSE)
		ENDIF
	ELSE
		NET_PRINT("[spawning] IsPlayerInDriveableCar - IS_NET_PLAYER_OK(PlayerID) = FALSE.") NET_NL()
		RETURN(FALSE)						
	ENDIF


ENDFUNC

FUNC BOOL IsPlayerDriverInDriveableCar(PLAYER_INDEX PlayerID)
	
	// has he spawned in his car?
	IF IS_NET_PLAYER_OK(PlayerID)		
		IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID))
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PlayerID)))
				IF (GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PlayerID)), VS_DRIVER) = GET_PLAYER_PED(PlayerID))
					NET_PRINT("[spawning] IsPlayerDriverInDriveableCar - player is ready.") NET_NL()
					RETURN(TRUE)
				ELSE
					NET_PRINT("[spawning] IsPlayerDriverInDriveableCar - player is not a driver.") NET_NL()
					RETURN(FALSE)	
				ENDIF
			ELSE
				NET_PRINT("[spawning] IsPlayerDriverInDriveableCar - IS_VEHICLE_DRIVEABLE = FALSE.") NET_NL()
				RETURN(FALSE)							
			ENDIF
		ELSE
			PRINTLN("[spawning] IsPlayerDriverInDriveableCar - IS_PED_IN_ANY_VEHICLE(PlayerID) = FALSE.")
			RETURN(FALSE)
		ENDIF
	ELSE
		NET_PRINT("[spawning] IsPlayerDriverInDriveableCar - IS_NET_PLAYER_OK(PlayerID) = FALSE.") NET_NL()
		RETURN(FALSE)						
	ENDIF


ENDFUNC

FUNC BOOL IsPlayersModelLoaded()
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_MPTESTBED
		REQUEST_MODEL(MP_M_FREEMODE_01)
		IF HAS_MODEL_LOADED(MP_M_FREEMODE_01)
			RETURN(TRUE)
		ENDIF
	ELSE
		REQUEST_MODEL(GET_PLAYER_MODEL_FOR_TEAM( GET_STAT_CHARACTER_TEAM()))
		IF HAS_MODEL_LOADED(GET_PLAYER_MODEL_FOR_TEAM( GET_STAT_CHARACTER_TEAM()))
			RETURN(TRUE)	
		ENDIF
	ENDIF
	NET_PRINT("[spawning] IsPlayersModelLoaded() - FALSE") NET_NL()
	RETURN(FALSE)
ENDFUNC

PROC WarpPlayerIntoCar(PED_INDEX PedID, VEHICLE_INDEX CarID, VEHICLE_SEAT VehSeat, BOOL bForce = FALSE)
	
	NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoCar - called ") NET_NL()
	

	BOOL bHasControlOfEntity = FALSE
	IF NETWORK_HAS_CONTROL_OF_ENTITY(CarID)
		bHasControlOfEntity = TRUE	
	ENDIF
	
	// check that player is not already in the car as FORCE_PED_AI_AND_ANIMATION_UPDATE can't be called in subsequent frames (see  1593000)
	IF NOT IS_PED_IN_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(PedID, CarID, bHasControlOfEntity) 
			
		// check player is near to vehicle
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PedID, FALSE)
		VECTOR vCarCoords = GET_ENTITY_COORDS(CarID, FALSE)
		
		NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoCar - vPlayerCoords = ") NET_PRINT_VECTOR(vPlayerCoords) NET_PRINT(", vCarCoords = ") NET_PRINT_VECTOR(vCarCoords) NET_NL()
		
 		IF NOT (VDIST(vPlayerCoords, vCarCoords) < 5.0)
		OR NOT (VehSeat =  VS_DRIVER)
			vCarCoords.z += -4.0
			vCarCoords.x += -4.0
			vCarCoords.y += -4.0
			SET_ENTITY_COORDS(PedID, vCarCoords, FALSE)
			NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoCar - moving player closer to car.") NET_NL()
		ENDIF

		CLEAR_PED_TASKS_IMMEDIATELY(PedID)	
		SET_ENTITY_COLLISION(PedID, TRUE)		
		FREEZE_ENTITY_POSITION(PedID, FALSE)
		SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), FALSE) // this is needed, please do not comment out. 
		SET_PED_RESET_FLAG(PedID, PRF_SuppressInAirEvent, TRUE) // 3309718
		SET_PED_RESET_FLAG(PedID, PRF_AllowTasksIncompatibleWithMotion, TRUE) // 3887418
		
		IF bForce
			SET_PED_INTO_VEHICLE(PedID, CarID, VehSeat)
			NET_PRINT_FRAME() NET_PRINT("[Spawning] WarpPlayerIntoCar - Setting ped into vehicle seat: ") NET_PRINT_INT(ENUM_TO_INT(VehSeat)) NET_NL()
		ELSE
			TASK_ENTER_VEHICLE(PedID, CarID, -1, VehSeat, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)	
			NET_PRINT_FRAME() NET_PRINT("[Spawning] WarpPlayerIntoCar - Tasking ped into vehicle seat: ") NET_PRINT_INT(ENUM_TO_INT(VehSeat)) NET_NL()
		ENDIF
		
		FORCE_PED_AI_AND_ANIMATION_UPDATE(PedID)
		SET_PED_RESET_FLAG(PedID, PRF_SuppressInAirEvent, TRUE) // 3114610
				
		IF (g_b_On_Race)
			IF IS_DOES_MODEL_USE_HELMETS(GET_ENTITY_MODEL(CarID))				
				GIVE_PLAYER_STORED_HELMET()
			ENDIF
		ENDIF
	ELSE
		NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoCar - ped is already in car or about to enter ") NET_NL()	
	ENDIF
ENDPROC

PROC ApplyRespotTimer(VEHICLE_INDEX CarID, INT iVehicleRespotTime, INT iPlayerInvincibleTime = 0)	
	
	NETWORK_INDEX NetID = NETWORK_GET_NETWORK_ID_FROM_ENTITY(CarID)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(NetID)	
		
		NET_PRINT("[Spawning] ApplyRespotTimer - called with ") NET_PRINT_INT(iVehicleRespotTime) NET_PRINT(", ") NET_PRINT_INT(iPlayerInvincibleTime) NET_NL()
		
		IF ((g_bFM_ON_VEH_DEATHMATCH) AND IS_VEHICLE_MODEL(CarID, RHINO))	// Fix for B*1632406.						
			iVehicleRespotTime = 1
			SET_NETWORK_VEHICLE_RESPOT_TIMER(NetID, 1, DEFAULT, IS_LOCAL_PLAYER_ON_ANY_FM_MISSION())
			PRINTLN("[Spawning] ApplyRespotTimer - SET_NETWORK_VEHICLE_RESPOT_TIMER with 1 (RHINO) ") 
			EXIT
		ELIF IS_NON_CONTACT_RACE_GLOBAL_SET()
			DEAL_WITH_NON_CONTACT_RACE(CarID)
			PRINTLN("[Spawning] ApplyRespotTimer - [CS_GHOST] IS_NON_CONTACT_RACE_GLOBAL_SET, EXIT ")
			EXIT
		ENDIF
		
		// Choose the larger of the 2 values.
		IF (iPlayerInvincibleTime > iVehicleRespotTime)
			SET_NETWORK_VEHICLE_RESPOT_TIMER(NetID, iPlayerInvincibleTime, DEFAULT, IS_LOCAL_PLAYER_ON_ANY_FM_MISSION())
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), CarID)
				NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(iPlayerInvincibleTime)
			ENDIF
			
			PRINTLN("[Spawning] ApplyRespotTimer - SET_NETWORK_VEHICLE_RESPOT_TIMER with ", iPlayerInvincibleTime)
		ELSE
			SET_NETWORK_VEHICLE_RESPOT_TIMER(NetID, iVehicleRespotTime, DEFAULT, IS_LOCAL_PLAYER_ON_ANY_FM_MISSION())
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), CarID)
				NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(iVehicleRespotTime)
			ENDIF
			
			PRINTLN("[Spawning] ApplyRespotTimer - SET_NETWORK_VEHICLE_RESPOT_TIMER with ", iVehicleRespotTime) 
		ENDIF
	ELSE
		PRINTLN("[Spawning] ApplyRespotTimer - not network object") 	
	ENDIF
ENDPROC

PROC ShowVehicleForRespawn(VEHICLE_INDEX CarID, INT iRespotTime)	
	NET_PRINT("[spawning] ShowVehicleForRespawn called...") NET_NL()
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		FREEZE_ENTITY_POSITION(CarID, FALSE)	// so planes/helis dont fall from the sky.
	ELSE
		PRINTLN("[spawning] ShowVehicleForRespawn - IS_PLAYER_SWITCH_IN_PROGRESS, not unfreezing")
	ENDIF
	SET_ENTITY_COLLISION(CarID, TRUE)
	SET_ENTITY_INVINCIBLE(CarID, FALSE)
	SET_ENTITY_VISIBLE(CarID, TRUE)
	
	// extra check to make sure the engine is on. 2898157
	SET_VEHICLE_ENGINE_ON(CarID, TRUE, TRUE)
	SET_VEHICLE_LIGHTS(CarID, SET_VEHICLE_LIGHTS_ON)
	
	ApplyRespotTimer(CarID, iRespotTime)
ENDPROC

PROC HideVehicleForRespawn(VEHICLE_INDEX CarID)		
	NET_PRINT("[spawning] HideVehicleForRespawn called...") NET_NL()
	FREEZE_ENTITY_POSITION(CarID, TRUE)	// so planes/helis dont fall from the sky.
	SET_ENTITY_COLLISION(CarID, FALSE)
	SET_ENTITY_INVINCIBLE(CarID, TRUE)
	SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(CarID, TRUE)
	#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] HideVehicleForRespawn - car coords = ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(CarID, FALSE)) NET_NL()
	#ENDIF 
ENDPROC

PROC SetSpawnVehicleAttributes(VEHICLE_INDEX CarID) //, VECTOR vCoord, FLOAT fHeading)
	
	NET_PRINT("[spawning] SetSpawnVehicleAttributes - called... ") NET_NL()
	#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] SetSpawnVehicleAttributes - car coords = ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(CarID, FALSE)) NET_NL()
	#ENDIF 	
	
	BOOL bSetAsMissionEntity = FALSE
	
	IF CanEditThisVehicle(CarID, bSetAsMissionEntity)
	
		// make sure car is fixed
		SET_VEHICLE_FIXED(CarID)		
		HideVehicleForRespawn(CarID)
		SET_VEHICLE_ENGINE_ON(CarID, TRUE, TRUE)
		SET_VEHICLE_LIGHTS(CarID, SET_VEHICLE_LIGHTS_ON)
		
		// Fix for 2885720 - Lowering vehicle dirt level.
		MODEL_NAMES vehModel = GET_ENTITY_MODEL(CarID)
		
		IF IS_THIS_MODEL_A_CAR(vehModel)
		
			IF (vehModel = TROPHYTRUCK OR vehModel = TROPHYTRUCK2) 
				// Setting dirt level cap.
				FLOAT fMaxDirtLevel = 2.0
				
				// Get the current dirt level of the vehicle.
				FLOAT fDirtLevel = GET_VEHICLE_DIRT_LEVEL(CarID)
				
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] SetSpawnVehicleAttributes - Current vehicle dirt level: ") NET_PRINT_FLOAT(fDirtLevel) NET_NL()
				#ENDIF
				
				// Cap the dirt level if it's greater than the maximum dirt level.
				IF (fDirtLevel > fMaxDirtLevel)
					fDirtLevel = fMaxDirtLevel

					// Set the current vehicles dirt level.
					SET_VEHICLE_DIRT_LEVEL(CarID, fDirtLevel)
					
					#IF IS_DEBUG_BUILD
					NET_PRINT("[Spawning] SetSpawnVehicleAttributes - Capping vehicle dirt level at: ") NET_PRINT_FLOAT(fMaxDirtLevel) NET_NL()
					#ENDIF	
				ENDIF
			ENDIF
		ENDIF
		
		// If plane or heli make sure blades are turning
		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(CarID))
		OR IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(CarID))
			
			// This is also used for plane propellers
			SET_HELI_BLADES_FULL_SPEED(CarID) 
			
			IF GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID()) 
				SET_SPAWN_VEHICLE_MODS(CarID)
			ENDIF
		ENDIF
		
		SetLandingGear(CarID)

		// lock out other teams
		IF (GET_PLAYER_TEAM(PLAYER_ID()) > -1)
			SET_VEHICLE_DOORS_LOCKED_FOR_TEAM(CarID, GET_PLAYER_TEAM(PLAYER_ID()), FALSE)
		ENDIF
		
		// warp player into car
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), CarID)
			WarpPlayerIntoCar(PLAYER_PED_ID(), CarID, VS_DRIVER)
		ENDIF
		
		IF (g_b_On_Race)
			REMOVE_RACE_VEHICLE_EXTRAS(CarID)
			SET_VEHICLE_NO_EXPLOSION_DAMAGE_FROM_DRIVER(CarID, NOT IS_PLAYER_USING_ARENA())
			IF vehModel = RUINER2
				SET_VEHICLE_WEAPON_CAN_TARGET_OBJECTS(CarID, TRUE)
			ENDIF
			IF vehModel = BLAZER5
				IF GET_HAS_RETRACTABLE_WHEELS(CarID)
					IF IS_ENTITY_IN_WATER(CarID)
					AND NOT IS_VEHICLE_ON_ALL_WHEELS(CarID)
						SET_WHEELS_RETRACTED_INSTANTLY(CarID)
						g_SpawnData.bRetractVehicleWheelsInstantly = TRUE
						PRINTLN("[SetSpawnVehicleAttributes] SET_WHEELS_RETRACTED_INSTANTLY")
					ELSE
						SET_WHEELS_EXTENDED_INSTANTLY(CarID)
						g_SpawnData.bExtendVehicleWheelsInstantly = TRUE
						PRINTLN("[SetSpawnVehicleAttributes] SET_WHEELS_EXTENDED_INSTANTLY")
					ENDIF
				ENDIF
			ENDIF
			
			
			IF g_SpawnData.bUseAlternateVehicleForm
				IF vehModel = DELUXO
					
					SET_CAN_DELUXO_HOVER(CarID, TRUE)
					SET_CAN_DELUXO_FLY(CarID, TRUE)
					
					PRINTLN("[SetSpawnVehicleAttributes] - [Deluxo] Putting the Deluxo into hover mode instantly due to g_SpawnData.bUseAlternateVehicleForm")
					
					SET_SPECIAL_FLIGHT_MODE_RATIO(CarID, cfDeluxo_InstantRatio - GET_FRAME_TIME())
					SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(CarID, 1.0)
					
					g_SpawnData.bUseAlternateVehicleForm = FALSE

				ELIF vehModel = STROMBERG
				AND IS_ENTITY_IN_WATER(CarID)
					PRINTLN("[SetSpawnVehicleAttributes] - [Stromberg] Putting Stromberg into submarine mode on spawn due to g_SpawnData.bUseAlternateVehicleForm")
					TRANSFORM_TO_SUBMARINE(CarID, TRUE)
					g_SpawnData.bUseAlternateVehicleForm = FALSE
				ENDIF
			ENDIF
			
			IF DOES_THIS_MODEL_HAVE_VERTICAL_FLIGHT_MODE(GET_ENTITY_MODEL(CarID))
				IF g_SpawnData.bUseVerticalFlightMode
					PRINTLN("DEAL_WITH_RESPAWNING - Putting vehicle into vertical flight mode due to g_SpawnData.bUseVerticalFlightMode being set")
					SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(CarID, 1.0)
					SET_VEHICLE_FLIGHT_NOZZLE_POSITION(CarID, 1.0)
					SET_VEHICLE_FORWARD_SPEED(CarID, 0.0)
				ELSE
					PRINTLN("DEAL_WITH_RESPAWNING - Putting vehicle into NON vertical flight mode due to g_SpawnData.bUseVerticalFlightMode being FALSE")
					SET_VEHICLE_FLIGHT_NOZZLE_POSITION(CarID, 0.0)
					SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(CarID, 0.0)
					SET_VEHICLE_FLIGHT_NOZZLE_POSITION(CarID, 0.0)
					
					g_SpawnData.bLateTurnOffVTOL = TRUE
					g_SpawnData.iLateTurnOffVTOLFrame = GET_FRAME_COUNT()
					SET_DISABLE_VERTICAL_FLIGHT_MODE_TRANSITION(CarID, TRUE)
					PRINTLN("Setting bLateTurnOffVTOL")
				ENDIF
				
				CONTROL_LANDING_GEAR(CarID, LGC_RETRACT_INSTANT)
			ENDIF
			
			IF vehModel = SUBMERSIBLE
			OR vehModel = SUBMERSIBLE2
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciRACE_TUNED_SUBMERSIBLE)
					IF DOES_ENTITY_EXIST(CarID)
						PRINTLN("[SetSpawnVehicleAttributes] - Upping Sub Speed")
						INCREASE_SUBMERSIBLE_SPEED(CarID)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// B*3364583, Disabled, now handled in function APPLY_WEAPONS_TO_VEHICLE
		/*IF (g_b_On_Race AND !g_b_IsGTARace)
			IF IS_THIS_MODEL_A_SPECIAL_VEHICLE(vehModel)
				IF (GET_ENTITY_MODEL(CarID) = RUINER2)
					NET_PRINT("[spawning] SetSpawnVehicleAttributes - this is a RUINER2, setting weapon slot 0 ammo count to 0") NET_NL()
					SET_VEHICLE_WEAPON_RESTRICTED_AMMO(CarID, 0, 0)

					NET_PRINT("[spawning] SetSpawnVehicleAttributes - this is a RUINER2, setting weapon slot 1 ammo count to 0") NET_NL()
					SET_VEHICLE_WEAPON_RESTRICTED_AMMO(CarID, 1, 0)
				
					PRINTLN("[spawning] SetSpawnVehicleAttributes - Disabling RUINER2 weapons.")
					DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET, CarID, PLAYER_PED_ID())
					DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET, CarID, PLAYER_PED_ID())
				ENDIF
				
				IF (GET_ENTITY_MODEL(CarID) = BLAZER5)
					NET_PRINT("[spawning] SetSpawnVehicleAttributes - this is a BLAZER5, setting weapon slot 0 ammo count to 0") NET_NL()
					SET_VEHICLE_WEAPON_RESTRICTED_AMMO(CarID, 0, 0)
					
					PRINTLN("[spawning] SetSpawnVehicleAttributes - Disabling BLAZER5 weapons.")
					DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_CANNON_BLAZER, CarID, PLAYER_PED_ID())
				ENDIF
			ENDIF
		ENDIF*/

		// if this is an air race dont allow the engine to degrade
		IF (g_b_Is_Air_Race)
			SET_VEHICLE_ENGINE_CAN_DEGRADE(CarID, FALSE) 	
		ENDIF					
		
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(CarID))
	
		// if this is created on a race, then stop it from being modded. 2984692
		IF (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_BIKER_RACE_P2P)
			INT iDecoratorValue
			IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
				IF DECOR_EXIST_ON(CarID, "MPBitset")
					iDecoratorValue = DECOR_GET_INT(CarID, "MPBitset")
				ENDIF
				SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
				DECOR_SET_INT(CarID, "MPBitset", iDecoratorValue)
				PRINTLN( "[spawning] SetSpawnVehicleAttributes - on a race made vehicle non-moddable. ")
			ENDIF
		ENDIF
				
		// 3309132
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciBIKES_DONT_WIPE_OUT) 
		OR ( IS_CURRENT_MISSION_UGC_NON_ROCKSTAR() AND NETWORK_IS_ACTIVITY_SESSION() )	 
			SET_BIKE_EASY_TO_LAND(CarID, TRUE)
			NET_PRINT_FRAME() NET_PRINT("[spawning] SetSpawnVehicleAttributes - calling SET_BIKE_EASY_TO_LAND") NET_NL()
		ENDIF	
	
	ENDIF
	
	IF (bSetAsMissionEntity)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(CarID)	
		NET_PRINT("[spawning] SetSpawnVehicleAttributes - vehicle marked as no longer needed") NET_NL()
	ENDIF
	
	RESET_ON_SPAWN EmptyResetOnSpawn
	g_SpawnData.ResetOnSpawn = EmptyResetOnSpawn
	
ENDPROC

PROC DisableAllControlsForPlayer()
	INT iInput
	REPEAT ENUM_TO_INT(MAX_INPUTS) iInput
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INT_TO_ENUM(CONTROL_ACTION, iInput))
	ENDREPEAT
ENDPROC

FUNC BOOL AmIDrivingAVehicle()
	VEHICLE_INDEX CarID 
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) 
		IF DOES_ENTITY_EXIST(CarID)
			IF IS_VEHICLE_DRIVEABLE(CarID)
				IF (GET_PED_IN_VEHICLE_SEAT(CarID, VS_DRIVER) = PLAYER_PED_ID())
					RETURN(TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC


//FUNC BOOL IsVehicleEmpty(VEHICLE_INDEX CarID)
//	IF NOT IS_ENTITY_DEAD(CarID) //*1240968
//		IF GET_VEHICLE_NUMBER_OF_PASSENGERS(CarID) = 0
//			RETURN(TRUE)
//		ENDIF
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC


FUNC BOOL IsAnyOtherPlayerUsingVehicle(VEHICLE_INDEX CarID)
	INT i
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF NOT (PlayerID = PLAYER_ID())
			IF IS_NET_PLAYER_OK(PlayerID)
				IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(PlayerID), TRUE)
					IF (GET_VEHICLE_PED_IS_USING(GET_PLAYER_PED(PlayerID)) = CarID)
						NET_PRINT("IsAnyOtherPlayerUsingVehicle - player ") NET_PRINT_INT(i) NET_PRINT(" is using same vehicle as player") NET_NL()
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

//FUNC BOOL DeletePedFromSeatIfPossible(VEHICLE_INDEX & CarID, VEHICLE_SEAT Seat)
//	PED_INDEX PedID
//
//	NET_PRINT("DeletePedFromSeatIfPossible - called with seat ") NET_PRINT_INT(ENUM_TO_INT(Seat)) NET_NL()
//
//	IF NOT IS_VEHICLE_SEAT_FREE(CarID, Seat)
//		PedID = GET_PED_IN_VEHICLE_SEAT(CarID, Seat)	
//		IF DOES_ENTITY_EXIST(PedID)
//			IF NETWORK_HAS_CONTROL_OF_ENTITY(PedID)
//				IF NOT IS_ENTITY_A_MISSION_ENTITY(PedID)
//					IF NETWORK_GET_ENTITY_IS_LOCAL(PedID)
//						SET_ENTITY_AS_MISSION_ENTITY(PedID, FALSE, TRUE)
//						NET_PRINT("DeletePedFromSeatIfPossible - deleted driver, wasnt a mission entity") NET_NL()
//						DELETE_PED(PedID)
//						RETURN(TRUE)
//					ENDIF
//				ELIF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(PedID)
//					NET_PRINT("DeletePedFromSeatIfPossible - deleted driver, belonged to this script") NET_NL()
//					DELETE_PED(PedID)
//					RETURN(TRUE)
//				ENDIF
//			ENDIF
//		ELSE
//			NET_PRINT("DeletePedFromSeatIfPossible - ped doesnt exist ") NET_PRINT_INT(NATIVE_TO_INT(CarID)) NET_NL()
//			RETURN(TRUE)	
//		ENDIF
//	ELSE
//		NET_PRINT("DeletePedFromSeatIfPossible - seat is free ") NET_PRINT_INT(NATIVE_TO_INT(CarID)) NET_NL()
//		RETURN(TRUE)
//	ENDIF
//	
//	NET_PRINT("DeletePedFromSeatIfPossible - returning false ") NET_PRINT_INT(NATIVE_TO_INT(CarID)) NET_NL()
//	RETURN(FALSE)
//	
//ENDFUNC


//FUNC BOOL DeleteAllPedsFromSeatsOfVehicleIfPossible(VEHICLE_INDEX & CarID)
//	INT iSeat = -1 // VS_DRIVER starts at -1
//	WHILE iSeat <= ENUM_TO_INT(VS_EXTRA_RIGHT_3)
//		IF NOT DeletePedFromSeatIfPossible(CarID, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
//			RETURN(FALSE)
//		ENDIF
//		iSeat++
//	ENDWHILE
//	RETURN(TRUE)
//ENDFUNC

FUNC BOOL IsSeatEmpty(VEHICLE_INDEX & CarID, VEHICLE_SEAT Seat)
	PED_INDEX PedID
	IF NOT IS_VEHICLE_SEAT_FREE(CarID, Seat)
		PedID = GET_PED_IN_VEHICLE_SEAT(CarID, Seat)	
		IF NOT DOES_ENTITY_EXIST(PedID)
			RETURN(TRUE)
		ENDIF
	ELSE
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsVehicleEmpty(VEHICLE_INDEX & CarID)
	//IF NOT IS_ENTITY_DEAD(CarID)
		INT iSeat = -1 // VS_DRIVER starts at -1
		WHILE iSeat <= ENUM_TO_INT(VS_EXTRA_RIGHT_3)
			IF NOT IsSeatEmpty(CarID, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
				RETURN(FALSE)
			ENDIF
			iSeat++
		ENDWHILE
	//ENDIF
	RETURN(TRUE)
ENDFUNC

FUNC BOOL DeleteVehicleIfPossible(VEHICLE_INDEX &CarID)	


	IF DOES_ENTITY_EXIST(CarID)	
		
		NET_PRINT("DeleteVehicleIfPossible - going to attempt to delete vehicle with id ") NET_PRINT_INT(NATIVE_TO_INT(CarID)) NET_NL()
		
		IF NOT IS_PED_IN_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(PLAYER_PED_ID(), CarID)
	
			IF NOT (g_SpawnData.bPlayerWasInVehicleToDelete) // B*3887040, Code timing issue. need to wait another frame before deleting. 
	
				IF IsVehicleEmpty(CarID)
				
					IF NOT IsAnyOtherPlayerUsingVehicle(CarID)									
						IF NETWORK_HAS_CONTROL_OF_ENTITY(CarID)		

							IF NOT IS_ANY_PLAYER_NEAR_POINT(GET_ENTITY_COORDS(CarID, FALSE), 10.0, TRUE, TRUE, FALSE, TRUE, TRUE)
							OR NOT IS_ON_GTA_RACE_GLOBAL_SET()		
							OR IS_PLAYER_USING_ARENA()

								IF NOT IS_ENTITY_A_MISSION_ENTITY(CarID)	
									
									IF NETWORK_GET_ENTITY_IS_LOCAL(CarID)
															
										IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), CarID, TRUE)																	
																												
											SET_ENTITY_AS_MISSION_ENTITY(CarID, FALSE, TRUE)
											
											NET_PRINT("DeleteVehicleIfPossible - deleted vehicle, wasnt a mission entity") NET_NL()
											DELETE_VEHICLE(CarID)
											
											RETURN(TRUE)
										ELSE
											CarID = INT_TO_NATIVE(VEHICLE_INDEX, -1)
											NET_PRINT("DeleteVehicleIfPossible - player is in vehicle") NET_NL()
										ENDIF
									ELSE
										CarID = INT_TO_NATIVE(VEHICLE_INDEX, -1)
										NET_PRINT("DeleteVehicleIfPossible - entity is not local") NET_NL()	
									ENDIF
									
								ELIF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(CarID, FALSE)
									
									NET_PRINT("DeleteVehicleIfPossible - deleted vehicle, belonged to this script") NET_NL()
									DELETE_VEHICLE(CarID)
									
									RETURN(TRUE)
								ELSE
									SET_ENTITY_AS_MISSION_ENTITY(CarID, FALSE, TRUE)
									NET_PRINT("DeleteVehicleIfPossible - SET_ENTITY_AS_MISSION_ENTITY false true.") NET_NL()	
									NET_PRINT("DeleteVehicleIfPossible - did not belong to script, but now deleting.") NET_NL()		
									DELETE_VEHICLE(CarID)
									
									RETURN(TRUE)
								ENDIF
							ELSE
								CarID = INT_TO_NATIVE(VEHICLE_INDEX, -1)
								NET_PRINT("DeleteVehicleIfPossible - other player nearby") NET_NL()
							ENDIF			
						ELSE	
							IF (IS_ON_RACE_GLOBAL_SET() 
							AND (NOT IS_ON_GTA_RACE_GLOBAL_SET() OR IS_PLAYER_USING_ARENA() ))
							
								
								IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
									NETWORK_REQUEST_CONTROL_OF_ENTITY(CarID)
									NET_PRINT("DeleteVehicleIfPossible - NETWORK_HAS_CONTROL_OF_ENTITY = false") NET_NL()	
								ELSE
									NET_PRINT("DeleteVehicleIfPossible - Unable to request control of vehicle whilst current owner is spectating.") NET_NL()
								ENDIF
							ELSE
								CarID = INT_TO_NATIVE(VEHICLE_INDEX, -1)
								NET_PRINT("DeleteVehicleIfPossible - NETWORK_HAS_CONTROL_OF_ENTITY = false, and not on race") NET_NL()
							ENDIF
						ENDIF
					ELSE
						CarID = INT_TO_NATIVE(VEHICLE_INDEX, -1)
						NET_PRINT("DeleteVehicleIfPossible - other player using vehicle") NET_NL()
					ENDIF
				ELSE
					CarID = INT_TO_NATIVE(VEHICLE_INDEX, -1)
					NET_PRINT("DeleteVehicleIfPossible - car not empty") NET_NL()
				ENDIF
			ELSE
				g_SpawnData.bPlayerWasInVehicleToDelete = FALSE
				PRINTLN("DeleteVehicleIfPossible - setting g_SpawnData.bPlayerWasInVehicleToDelete = FALSE")
			ENDIF				
		ELSE
			NET_PRINT("DeleteVehicleIfPossible - player still in vehicle, don't try and delete.") NET_NL()
			
			IF NOT (g_SpawnData.bPlayerWasInVehicleToDelete)
				g_SpawnData.bPlayerWasInVehicleToDelete = TRUE
				PRINTLN("DeleteVehicleIfPossible - setting g_SpawnData.bPlayerWasInVehicleToDelete = TRUE")
			ENDIF			
		ENDIF
	ENDIF
	
	RETURN(FALSE)
ENDFUNC

FUNC BOOL SetVehicleToDelete(VEHICLE_INDEX &CarID)
	
	VEHICLE_INDEX CarToDelete = CarID
	DEBUG_PRINTCALLSTACK()
	
	IF DOES_ENTITY_EXIST(CarID)
		IF NOT DeleteVehicleIfPossible(CarToDelete)

			#IF IS_DEBUG_BUILD
				NET_PRINT("SetVehicleToDelete - Called, vehicle id = ") NET_PRINT_INT(NATIVE_TO_INT(CarID)) NET_NL()
				
				IF IsAnyOtherPlayerUsingVehicle(CarID)
					NET_PRINT("SetVehicleToDelete - Other player is using vehicle!") NET_NL()	
				ENDIF
			#ENDIF	
			
			g_SpawnData.VehicleToDelete = CarID
			
			RETURN(TRUE)
		ENDIF
	ELSE
		NET_PRINT("SetVehicleToDelete - Vehicle does not exist!") NET_NL()	
	ENDIF
	
	NET_PRINT("SetVehicleToDelete - Deleted vehicle and set carid to NULL") NET_NL()	
	CarID = NULL
	
	RETURN(FALSE)
ENDFUNC

PROC HidePlayer()
	// *********** PLAYER **********
	NET_PRINT("[spawning] HidePlayer called...") NET_NL()
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		NET_PRINT("[spawning] HidePlayer - player in a vehicle") NET_NL()
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)	
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
	ENDIF
	SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
	SET_PED_CAN_BE_TARGETTED(PLAYER_PED_ID(), FALSE)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bPlayerIsHiddenForWarp = TRUE
ENDPROC

FUNC FLOAT GetHideZForModel(MODEL_NAMES ModelName)
	IF IsModelBoatOrSub(ModelName)
		NET_PRINT("[spawning] GetHideZForModel - boat - returning ") NET_PRINT_FLOAT(HIDE_Z_BOAT) NET_NL()
		RETURN(HIDE_Z_BOAT)
	// Fix for B*3301502
	ELIF IS_SPECIAL_VEHICLE_A_BOAT(ModelName)
		NET_PRINT("[spawning] GetHideZForModel - special vehicle - returning ") NET_PRINT_FLOAT(HIDE_Z_SPECIAL) NET_NL()
		RETURN (HIDE_Z_SPECIAL)
	ELIF (ModelName = CERBERUS)
		IF IS_PLAYER_USING_ARENA()
			NET_PRINT("[spawning] GetHideZForModel - CERBERUS (in arena) - returning ") NET_PRINT_FLOAT(ARENA_Z_CAR) NET_NL()
			RETURN (ARENA_Z_CAR)	
		ELSE
			NET_PRINT("[spawning] GetHideZForModel - CERBERUS - returning ") NET_PRINT_FLOAT(HIDE_Z_SPECIAL) NET_NL()
			RETURN (HIDE_Z_SPECIAL)	
		ENDIF
	ELIF IS_PLAYER_USING_ARENA()
		NET_PRINT("[spawning] GetHideZForModel - IS_PLAYER_USING_ARENA - returning ") NET_PRINT_FLOAT(ARENA_Z_CAR) NET_NL()
		RETURN (ARENA_Z_CAR)	
	ENDIF
	
	
	NET_PRINT("[spawning] GetHideZForModel - car - returning ") NET_PRINT_FLOAT(HIDE_Z_CAR) NET_NL()
	RETURN(HIDE_Z_CAR)
ENDFUNC

PROC HidePlayerAndVehicleForWarp(BOOL bRepositionUnderMap=TRUE, BOOL bSetToDeleteWhenDone=TRUE)
	
	NET_PRINT("[spawning] HidePlayerAndVehicleForWarp - called...") NET_NL() 
	
	VEHICLE_INDEX CarID
	BOOL bSetAsMissionEntity
	VECTOR vPlayerPos

	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	SET_FOCUS_POS_AND_VEL(vPlayerPos, <<0,0,0>>)
	
	

			
	// *********** PLAYERS VEHICLE ************
	// make players vehicle invisible
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		NET_PRINT("[spawning] HidePlayerAndVehicleForWarp - using vehicle player is in.") NET_NL() 
	ELSE
		CarID = GET_PLAYERS_LAST_USED_VEHICLE()
		NET_PRINT("[spawning] HidePlayerAndVehicleForWarp - using last vehicle.") NET_NL()
	ENDIF
	
	IF DOES_ENTITY_EXIST(CarID)
		vPlayerPos.z = GetHideZForModel(GET_ENTITY_MODEL(CarID))
	ELSE
		vPlayerPos.z = HIDE_Z_CAR
	ENDIF
	
	IF DOES_ENTITY_EXIST(CarID)
		IF CanEditThisVehicle(CarID, bSetAsMissionEntity)
		
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				// freeze car under the ground
				IF (bRepositionUnderMap)
					SET_ENTITY_COORDS(CarID, vPlayerPos, FALSE)
				ENDIF
				FREEZE_ENTITY_POSITION(CarID, TRUE)	
				//SET_ENTITY_VISIBLE(CarID, FALSE)
				NET_PRINT("[spawning] HidePlayerAndVehicleForWarp - vehicle hidden underground") NET_NL()
			ENDIF
			
			IF (bSetToDeleteWhenDone)
				IF SetVehicleToDelete(CarID)
					NET_PRINT("[spawning] HidePlayerAndVehicleForWarp - SetVehicleToDelete = TRUE") NET_NL()
				ENDIF
			ELSE
				NET_PRINT("[spawning] HidePlayerAndVehicleForWarp - bSetToDeleteWhenDone = FALSE") NET_NL()
			ENDIF
			
			IF (bSetAsMissionEntity)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(CarID)	
				NET_PRINT("[spawning] HidePlayerAndVehicleForWarp - vehicle marked as no longer needed") NET_NL()
			ENDIF	
				
		ENDIF
	ENDIF			
	
	HidePlayer()

ENDPROC


PROC ClearAreaAroundPointForSpawning(VECTOR vCoords, BOOL bBroadcast=FALSE, BOOL bKeepScriptTrains=FALSE)
	VEHICLE_INDEX CarID
	FLOAT fRadius
	NET_PRINT("[spawning] ClearAreaAroundPointForSpawning - called with coords ") NET_PRINT_VECTOR(vCoords) NET_NL() 
	DEBUG_PRINTCALLSTACK()
	
	BOOL bDoBigClearArea = TRUE
	IF (g_bReducedClearAreaForInteriorsActive) 
	AND (IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vCoords)))
		bDoBigClearArea = FALSE // we are spawning inside an interior, so we don't do full clear areas as it can cause some interior environment objects to flicker. see url:bugstar:7436870
		PRINTLN("[spawning] ClearAreaAroundPointForSpawning - inside an interior.")
	ENDIF

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		fRadius = GET_MODEL_LENGTH(GET_ENTITY_MODEL(CarID))
		fRadius *= 0.5
		fRadius += CLEAR_AREA_RADIUS_VEHICLE	// the clear area has to catch other vehicles pivot point	
		IF (bDoBigClearArea)
			CLEAR_AREA(vCoords, fRadius, TRUE, FALSE, FALSE, bBroadcast)
		ENDIF
		CLEAR_AREA_OF_VEHICLES(vCoords, fRadius, TRUE, FALSE, TRUE, TRUE, bBroadcast, DEFAULT, bKeepScriptTrains)
		
		STOP_FIRE_IN_RANGE(vCoords, CLEAR_AREA_RADIUS_VEHICLE) 
	ELSE
			
		INT iResult
				
		VECTOR vResult, vNormal
		ENTITY_INDEX EntityID
		
		// is a vehicle or object colliding with point?
		SHAPETEST_INDEX ShapeTestID
		ShapeTestID = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(<<vCoords.x, vCoords.y, vCoords.z-2.0>>, <<vCoords.x, vCoords.y, vCoords.z+1.0>>, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_OBJECT)
		
		GET_SHAPE_TEST_RESULT(ShapeTestID, iResult, vResult, vNormal, EntityID)
		NET_PRINT("[spawning] ClearAreaAroundPointForSpawning - shapetest result = ") NET_PRINT_INT(iResult) NET_NL()						
				
		// increase the clear area if the point is intersecting an vehicle or object
		CLEAR_AREA(vCoords, CLEAR_AREA_RADIUS_PED, TRUE, FALSE, FALSE, bBroadcast)
		IF NOT (iResult = 0)
			//CLEAR_AREA(vCoords, CLEAR_AREA_RADIUS_VEHICLE, TRUE, FALSE, FALSE, bBroadcast)
			CLEAR_AREA_OF_VEHICLES(vCoords, CLEAR_AREA_RADIUS_VEHICLE, TRUE, FALSE, TRUE, TRUE, bBroadcast)
			
			IF (bDoBigClearArea)
				IF (bBroadcast)
					CLEAR_AREA_OF_OBJECTS(vCoords, CLEAR_AREA_RADIUS_VEHICLE, CLEAROBJ_FLAG_BROADCAST | CLEAROBJ_FLAG_EXCLUDE_LADDERS)
				ELSE
					CLEAR_AREA_OF_OBJECTS(vCoords, CLEAR_AREA_RADIUS_VEHICLE, CLEAROBJ_FLAG_EXCLUDE_LADDERS)
				ENDIF
			ENDIF
		ENDIF
		
		STOP_FIRE_IN_RANGE(vCoords, 2.5) 
				
	ENDIF	
ENDPROC





PROC BoostAirVehiclePlayerIsIn()
	NET_PRINT("[spawning] BoostAirVehiclePlayerIsIn - called... ") NET_NL()	
	VEHICLE_INDEX CarID = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	IF DOES_ENTITY_EXIST(CarID)	
		IF NETWORK_HAS_CONTROL_OF_ENTITY(CarID)
			BOOST_AIR_VEHICLE(CarID)			
			NET_PRINT("[spawning] BoostAirVehiclePlayerIsIn - CarID exists and has control ") NET_NL()	
		ELSE
			NET_PRINT("[spawning] BoostAirVehiclePlayerIsIn - player not in control of vehicle ") NET_NL()	
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(g_SpawnData.MissionSpawnDetails.SpawnedVehicle) 
			BOOST_AIR_VEHICLE(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)	
			NET_PRINT("[spawning] BoostAirVehiclePlayerIsIn - g_SpawnData.MissionSpawnDetails.SpawnedVehicle exists and has control ") NET_NL()	
		ELSE
			NET_PRINT("[spawning] BoostAirVehiclePlayerIsIn - car doesn't exist ") NET_NL()	
		ENDIF		
	ENDIF
ENDPROC




FUNC BOOL IsVehicleSuitableToUseAsWarpVehicle(VEHICLE_INDEX CarID)
	IF DOES_ENTITY_EXIST(CarID)
		IF NOT IS_ENTITY_DEAD(CarID)
			IF NOT IS_ENTITY_A_MISSION_ENTITY(CarID)
			OR DOES_ENTITY_BELONG_TO_THIS_SCRIPT(CarID)
		
				IF IS_VEHICLE_DRIVEABLE(CarID)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(CarID)
						IF IS_VEHICLE_EMPTY(CarID)
							#IF IS_DEBUG_BUILD
								NET_PRINT_FRAME() NET_PRINT("[spawning] IsVehicleSuitableToUseAsWarpVehicle - returning TRUE.") NET_NL()
							#ENDIF
							RETURN(TRUE)
						ELSE
							#IF IS_DEBUG_BUILD
								NET_PRINT_FRAME() NET_PRINT("[spawning] IsVehicleSuitableToUseAsWarpVehicle - FALSE - vehicle not empty.") NET_NL()
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							NET_PRINT_FRAME() NET_PRINT("[spawning] IsVehicleSuitableToUseAsWarpVehicle - FALSE - not in control of network entity.") NET_NL()
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_PRINT_FRAME() NET_PRINT("[spawning] IsVehicleSuitableToUseAsWarpVehicle - FALSE - vehicle not driveable.") NET_NL()
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT_FRAME() NET_PRINT("[spawning] IsVehicleSuitableToUseAsWarpVehicle - FALSE - vehicle does not belong to this script.") NET_NL()
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				NET_PRINT_FRAME() NET_PRINT("[spawning] IsVehicleSuitableToUseAsWarpVehicle - FALSE - entity is dead.") NET_NL()
			#ENDIF	
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT_FRAME() NET_PRINT("[spawning] IsVehicleSuitableToUseAsWarpVehicle - FALSE - entity does not exist.") NET_NL()
		#ENDIF
	ENDIF	
	RETURN(FALSE)
ENDFUNC

PROC ClearVehicleSpawnInfo()
	NET_PRINT("[spawning] ClearVehicleSpawnInfo - called... ") NET_NL()
	VEHICLE_SETUP_STRUCT_MP EmptyVehicleSetup
	g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP = EmptyVehicleSetup
	g_SpawnData.MissionSpawnDetails.bSpawnVehicleSetupSaved = FALSE
ENDPROC





PROC StoreVehicleSpawnInfoFromThisCar(VEHICLE_INDEX VehicleID, GAMER_HANDLE GamerHandle)

	NET_PRINT("[spawning] StoreVehicleSpawnInfoFromThisCar - called... ") NET_NL()
	DEBUG_PRINTCALLSTACK()
	
	IF DOES_ENTITY_EXIST(VehicleID)
		IF IS_VEHICLE_DRIVEABLE(VehicleID)
			
			GET_VEHICLE_SETUP_MP(VehicleID, g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP)			

			g_SpawnData.MissionSpawnDetails.bSpawnVehicleSetupSaved = TRUE
			NET_PRINT("[spawning] StoreVehicleSpawnInfoFromThisCar - g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.eModel =") 
			NET_PRINT(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL( g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel))
			NET_NL()
			
//			// does the car have a crew emblem?
//			IF DOES_VEHICLE_HAVE_CREW_EMBLEM(VehicleID) 
//				g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.bHasCrewEmblem = TRUE
//			ELSE
//				g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.bHasCrewEmblem = FALSE	
//			ENDIF
//			NET_PRINT("[spawning] StoreVehicleSpawnInfoFromThisCar - bHasCrewEmblem = ") NET_PRINT_BOOL(g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.bHasCrewEmblem) NET_NL()		
			
			// is this a personal vehicle? then get the handle of the owner
			g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.GamerHandleOfCarOwner = GamerHandle	
			//NET_PRINT("[spawning] GET_VEHICLE_SETUP_MP - GamerHandleOfCarOwner = ")  NET_NL()				
			
		ELSE
			NET_PRINT("[spawning] StoreVehicleSpawnInfoFromThisCar - vehicle is not driveable! ") NET_NL()		
		ENDIF
	ELSE
		NET_PRINT("[spawning] StoreVehicleSpawnInfoFromThisCar - vehicle does not exist! ") NET_NL()		
	ENDIF
ENDPROC


PROC RestoreVehicleFromSpawnInfo(VEHICLE_INDEX VehicleID, BOOL bSetLockStatus=TRUE)
	NET_PRINT("[spawning] RestoreVehicleFromSpawnInfo - called... ") NET_NL()
	IF DOES_ENTITY_EXIST(VehicleID)		
		SET_VEHICLE_SETUP_MP(VehicleID, g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP, bSetLockStatus)		
		
//		// set the emblem
//		IF (g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.bHasCrewEmblem)
//			IF NETWORK_CLAN_SERVICE_IS_VALID()
//				IF NETWORK_CLAN_PLAYER_IS_ACTIVE(g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.GamerHandleOfCarOwner)
//					IF NOT DOES_VEHICLE_HAVE_CREW_EMBLEM(VehicleID)
//						ADD_VEHICLE_CREW_EMBLEM_USING_SCRIPT_POSITION(VehicleID, GET_PLAYER_PED(NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.GamerHandleOfCarOwner)))
//						PRINTLN("RestoreVehicleFromSpawnInfo: started loading vehicle with emblem ")
//					ENDIF	
//				ENDIF
//			ENDIF
//		ENDIF		
		
	ELSE
		NET_PRINT("[spawning] RestoreVehicleFromSpawnInfo - vehicle does not exist") NET_NL()
	ENDIF
ENDPROC

FUNC MODEL_NAMES GetRandomCarModel()
	INT iRand
	iRand = GET_RANDOM_INT_IN_RANGE(0, 7)
	SWITCH iRand
		CASE 0	RETURN PREMIER
		CASE 1	RETURN CAVALCADE
		CASE 2 	RETURN EMPEROR
		CASE 3 	RETURN INGOT
		CASE 4	RETURN MESA
		CASE 5  RETURN PRIMO
		CASE 6 	RETURN WASHINGTON
	ENDSWITCH	
	RETURN PREMIER
ENDFUNC

FUNC BOOL GoodToUsePersonalVehicleToSpawn()
	IF (CURRENT_SAVED_VEHICLE_SLOT() > -1)
	AND (CURRENT_SAVED_VEHICLE_SLOT() < MAX_MP_SAVED_VEHICLES)
		IF (g_SpawnData.MissionSpawnDetails.bUsePersonalVehicleToSpawn)
			IF NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
			AND NOT IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL HasLoadedVehicleSpawnModelIfNecessary()

	//PLAYER_INDEX PartnerID			
	IF IS_SPAWNING_IN_VEHICLE()
	
		NET_PRINT("[spawning] HasLoadedVehicleSpawnModelIfNecessary - called - ") 
		NET_PRINT("g_SpawnData.MissionSpawnDetails.SpawnModel = ") 
		IF IS_MODEL_A_VEHICLE(g_SpawnData.MissionSpawnDetails.SpawnModel)
			NET_PRINT(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_SpawnData.MissionSpawnDetails.SpawnModel)) 
		ELSE
			NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.MissionSpawnDetails.SpawnModel))
		ENDIF
		NET_PRINT(", g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel = ") 
		IF IS_MODEL_A_VEHICLE(g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel)
			NET_PRINT(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel))
		ELSE
			NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel))
		ENDIF
		NET_NL()	
	
		// if rally mode and passenger - always let the driver spawn the car
		IF IsTeamModeSetForRespawning()
			IF IsRespawnPassenger()				
				RETURN(TRUE)
			ENDIF
		ENDIF
	
		BOOL bHasFoundModelToUse = FALSE
	
		// are we trying to spawn into personal vehicle?
		IF GoodToUsePersonalVehicleToSpawn()
			g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel = 	g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel
			bHasFoundModelToUse = TRUE
			NET_PRINT("[spawning] HasLoadedVehicleSpawnModelIfNecessary - using personal vehicle model = ") NET_PRINT(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL( g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel)) NET_NL()			
		ENDIF
	
	
		IF NOT (bHasFoundModelToUse)
			IF (g_SpawnData.MissionSpawnDetails.bSpawnInLastVehicleIfDriveable)
				IF DOES_ENTITY_EXIST(g_SpawnData.LastUsedVehicleID)				
					IF (g_SpawnData.MissionSpawnDetails.SpawnModel = GET_ENTITY_MODEL(g_SpawnData.LastUsedVehicleID))
					OR (g_SpawnData.MissionSpawnDetails.SpawnModel = DUMMY_MODEL_FOR_SCRIPT)
						
						IF IS_PLAYER_SPECTATING(PLAYER_ID())
						AND NOT (g_SpawnData.MissionSpawnDetails.SpawnModel = DUMMY_MODEL_FOR_SCRIPT)
							bHasFoundModelToUse = TRUE
							IF (g_SpawnData.MissionSpawnDetails.bSpawnVehicleSetupSaved)
								PRINTLN("[spawning] HasLoadedVehicleSpawnModelIfNecessary - player is spectating and already stored vehicle setup") 
							ELSE
								PRINTLN("[spawning] HasLoadedVehicleSpawnModelIfNecessary - player is spectating and not stored vehicle setup")
							ENDIF
						ELSE						
							PRINTLN("[spawning] HasLoadedVehicleSpawnModelIfNecessary - player is not spectating") 
							g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel = GET_ENTITY_MODEL(g_SpawnData.LastUsedVehicleID)
							StoreVehicleSpawnInfoFromThisCar(g_SpawnData.LastUsedVehicleID, GET_GAMER_HANDLE_PLAYER(PLAYER_ID()))
							bHasFoundModelToUse = TRUE
						ENDIF
						
					ENDIF
				ENDIF	
			ENDIF	
		ENDIF
	
		// if no model has been set then choose something appropriate
		IF (g_SpawnData.MissionSpawnDetails.SpawnModel = DUMMY_MODEL_FOR_SCRIPT)
		AND NOT (bHasFoundModelToUse)
		
			ClearVehicleSpawnInfo()
		
			INT iVehicleClass
			GET_RANDOM_VEHICLE_MODEL_IN_MEMORY(TRUE, g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel, iVehicleClass)					
			// if couldn't get a random vehicle model then use default model
			IF (g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT)
				g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel = GetRandomCarModel()
				NET_PRINT("[spawning] HasLoadedVehicleSpawnModelIfNecessary - couldn't find any random vehicle model, using ") 
				NET_PRINT(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL( g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel)) 				
				NET_NL()
			ELSE
				NET_PRINT("[spawning] HasLoadedVehicleSpawnModelIfNecessary - GET_RANDOM_VEHICLE_MODEL_IN_MEMORY - returned  CarModel=") 
				NET_PRINT(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL( g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel)) 
				NET_PRINT(", iVehicleClass=")
				NET_PRINT_INT(iVehicleClass)
				NET_NL()			
			ENDIF					

		
		ELSE
			IF NOT GoodToUsePersonalVehicleToSpawn()
				IF (g_SpawnData.MissionSpawnDetails.bSpawnVehicleSetupSaved)
					IF NOT (g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel = g_SpawnData.MissionSpawnDetails.SpawnModel)
					AND NOT IS_SPECIAL_CONTROLLER_RESPAWN_REQUESTED()
						ClearVehicleSpawnInfo()
					ENDIF
				ELSE
					g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel = g_SpawnData.MissionSpawnDetails.SpawnModel
				ENDIF	
			ENDIF
		ENDIF
	
		IF NOT (g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT)
			REQUEST_MODEL(g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel)
			IF HAS_MODEL_LOADED(g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel)
				NET_PRINT("[spawning] HasLoadedVehicleSpawnModelIfNecessary - - RETURNING TRUE - loaded model ") NET_PRINT(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel)) NET_NL()				
				RETURN(TRUE)
			ELSE
				NET_PRINT("[spawning] HasLoadedVehicleSpawnModelIfNecessary - RETURNING FALSE - loading model ") NET_PRINT(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel)) NET_NL()				
				RETURN(FALSE)
			ENDIF	
		ELSE
			NET_PRINT("[spawning] HasLoadedVehicleSpawnModelIfNecessary - RETURNING FALSE - g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPTl ") NET_NL()	
			RETURN(FALSE)
		ENDIF
		
	ELSE
		NET_PRINT("[spawning] HasLoadedVehicleSpawnModelIfNecessary - RETURNING TRUE - g_SpawnData.MissionSpawnDetails.bSpawnInVehicle = FALSE ") NET_NL()		
		RETURN(TRUE)
	ENDIF
	NET_PRINT("[spawning] HasLoadedVehicleSpawnModelIfNecessary - RETURNING FALSE - default - shouldn't hit here! ") NET_NL()	
	RETURN(FALSE)
ENDFUNC

PROC SPAWN_WITH_NON_HOMING_SETTING_RETAINED()
	PRINTLN("[TMS] Inside SPAWN_WITH_NON_HOMING_SETTING_RETAINED")
	PRINTLN("[TMS] Use non homing missiles: ", g_SpawnData.UseNonHomingMissiles)
	IF g_SpawnData.UseNonHomingMissiles = TRUE // 3958863 // 4020603
	OR (g_SpawnData.VehicleWeapon = WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE)
		IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = HUNTER
			SET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE)
			PRINTLN("[TMS][spawning] Setting Hunter's weapon to Barrage rather than the homing missile")
		ELSE
			PRINTLN("[TMS] using non homing? ", IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(PLAYER_ID()), " on weapon ", g_SpawnData.VehicleWeapon)
			SET_PLAYER_VEHICLE_WEAPON_TO_NON_HOMING(PLAYER_ID())
			PRINTLN("[TMS][spawning] Setting the vehicle to not use homing missiles (calling SET_PLAYER_VEHICLE_WEAPON_TO_NON_HOMING) for weapon ", g_SpawnData.VehicleWeapon)
			PRINTLN("[TMS] using non homing now? ", IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(PLAYER_ID()), " on weapon ", g_SpawnData.VehicleWeapon)
		ENDIF
	ELSE
		PRINTLN("[TMS][spawning] Leaving new vehicle's missiles on Homing mode")
	ENDIF
ENDPROC

FUNC BOOL SetVehicleWeapon()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT (g_SpawnData.VehicleWeapon = WEAPONTYPE_INVALID)
			IF SET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), g_SpawnData.VehicleWeapon)
				NET_PRINT("[spawning] SetVehicleWeapon - SET_CURRENT_PED_VEHICLE_WEAPON = TRUE, g_SpawnData.VehicleWeapon = ") NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.VehicleWeapon)) NET_NL()
				SPAWN_WITH_NON_HOMING_SETTING_RETAINED()
				RETURN(TRUE)
			ELSE
				NET_PRINT("[spawning] SetVehicleWeapon - SET_CURRENT_PED_VEHICLE_WEAPON = FALSE, g_SpawnData.VehicleWeapon = ") NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.VehicleWeapon)) NET_NL()
				SPAWN_WITH_NON_HOMING_SETTING_RETAINED()
			ENDIF
		ELSE
			RETURN(TRUE)
		ENDIF	
	ELSE
		NET_PRINT("[spawning] SetVehicleWeapon - not in any vehicle") NET_NL()
		RETURN(TRUE)
	ENDIF
	NET_PRINT("[spawning] SetVehicleWeapon - return FALSE") NET_NL()
	RETURN(FALSE)
ENDFUNC


PROC StoreSpawnVehicleHandle(VEHICLE_INDEX VehId)
	IF (VehId != g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
		DEBUG_PRINTCALLSTACK()		
		PRINTLN("[spawning] StoreSpawnVehicleHandle - storing  LastSpawnedVehicle as ", NATIVE_TO_INT(g_SpawnData.MissionSpawnDetails.SpawnedVehicle))
		PRINTLN("[spawning] StoreSpawnVehicleHandle - new  SpawnedVehicle is ", NATIVE_TO_INT(VehId))		
		g_SpawnData.MissionSpawnDetails.LastSpawnedVehicle = g_SpawnData.MissionSpawnDetails.SpawnedVehicle
		g_SpawnData.MissionSpawnDetails.SpawnedVehicle = VehId
	ENDIF
ENDPROC


PROC SetVehicleForWarpIntoSpawnVehicle(VEHICLE_INDEX VehicleID)
	
	NET_PRINT("[Spawning] SetVehicleForWarpIntoSpawnVehicle - Vehicle position: ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(VehicleID, FALSE)) NET_NL()		
	
	IF DOES_ENTITY_EXIST(VehicleID)
		NET_PRINT("[Spawning] SetVehicleForWarpIntoSpawnVehicle - START - New net vehicle exists.") NET_NL()
	ELSE
		NET_PRINT("[Spawning] SetVehicleForWarpIntoSpawnVehicle - START - New net vehicle does NOT exist.") NET_NL()
	ENDIF
	
	IF NOT IS_ENTITY_A_MISSION_ENTITY(VehicleID)
		IF NETWORK_GET_ENTITY_IS_LOCAL(VehicleID)
			SET_ENTITY_AS_MISSION_ENTITY(VehicleID, FALSE, TRUE)
			NET_PRINT("[Spawning] SetVehicleForWarpIntoSpawnVehicle - Set net vehicle set as mission entity.") NET_NL()
		ELSE
			NET_PRINT("[Spawning] SetVehicleForWarpIntoSpawnVehicle - Can't set as mission entity as net vehicle is not local.") NET_NL()
		ENDIF
	ENDIF
	
	SET_ENTITY_VISIBLE(VehicleID, FALSE)
	NET_PRINT("[Spawning] SetVehicleForWarpIntoSpawnVehicle - Vehicle set as invisible.") NET_NL()			
	
	FREEZE_ENTITY_POSITION(VehicleID, TRUE)
	NET_PRINT("[Spawning] SetVehicleForWarpIntoSpawnVehicle - Vehicle set as frozen.") NET_NL()		
	
	SetSpawnVehicleAttributes(VehicleID)
	StoreSpawnVehicleHandle(VehicleID)
	
	g_SpawnedInVehicle = VehicleID
	
	IF (g_SpawnData.MissionSpawnDetails.bSpawnVehicleSetupSaved)
		RestoreVehicleFromSpawnInfo(VehicleID)
	ELSE
		StoreVehicleSpawnInfoFromThisCar(VehicleID, GET_GAMER_HANDLE_PLAYER(PLAYER_ID()))
	ENDIF	
	
	NET_PRINT_FRAME() NET_PRINT("[Spawning] SetVehicleForWarpIntoSpawnVehicle - spawning in new vehicle ") NET_NL()
	
	IF DOES_ENTITY_EXIST(VehicleID)
		NET_PRINT_FRAME() NET_PRINT("[Spawning] SetVehicleForWarpIntoSpawnVehicle - END - new net vehicle exists!") NET_NL()
	ELSE
		NET_PRINT_FRAME() NET_PRINT("[Spawning] SetVehicleForWarpIntoSpawnVehicle - END - new net vehicle does NOT exist!") NET_NL()	
	ENDIF	

	g_SpawnData.MissionSpawnDetails.bSpawningCreatedNewCar = TRUE
ENDPROC

FUNC BOOL DoesVehicleHaveFreeSeat(VEHICLE_INDEX VehicleID)
	INT i
	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)		
		FOR i = -1 TO GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(VehicleID)-1
			IF IS_VEHICLE_SEAT_FREE(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, i))
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GetFirstFreeSeatOfVehicle(VEHICLE_INDEX VehicleID, BOOL bTurretSecondPriority = FALSE, BOOL bIgnoreTurretSeat = FALSE, BOOL bIgnoreDriver = FALSE, INT preferredSeat = VS_INVALID)
	INT i
	// find first free seat
	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
		
		IF preferredSeat != VS_INVALID
		AND IS_VEHICLE_SEAT_FREE(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, preferredSeat) )
			RETURN ENUM_TO_INT(preferredSeat)
		ENDIF
		// check driver
		IF IS_VEHICLE_SEAT_FREE(VehicleID, VS_DRIVER )
		AND NOT bIgnoreDriver
			RETURN ENUM_TO_INT(VS_DRIVER)				
		ELSE
			INT iCachedSeat
			IF bTurretSecondPriority
				REPEAT GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(VehicleID) i
					IF IS_VEHICLE_SEAT_FREE(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, i))
					AND (INT_TO_ENUM(VEHICLE_SEAT, i) != VS_DRIVER OR NOT bIgnoreDriver)
						IF IS_TURRET_SEAT(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, i))
						AND NOT bIgnoreTurretSeat
							RETURN i
						ENDIF
						IF iCachedSeat = -1
							iCachedSeat = i
						ENDIF
					ENDIF
				ENDREPEAT
				RETURN iCachedSeat
			ELSE
				REPEAT GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(VehicleID) i
					IF IS_VEHICLE_SEAT_FREE(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, i) )
					AND (INT_TO_ENUM(VEHICLE_SEAT, i) != VS_DRIVER OR NOT bIgnoreDriver)
						RETURN i
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	
	ENDIF
	
	RETURN -1
ENDFUNC


PROC ADD_DECORATOR_TO_SET_FIRE_AFTER_GANG_BOSS_MISSION_ENDS(VEHICLE_INDEX VehicleID)

	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)	
			IF (g_iSmugglerUniqueMissionID > -1)
				IF DECOR_IS_REGISTERED_AS_TYPE("GBMissionFire", DECOR_TYPE_INT)
					IF NOT DECOR_EXIST_ON(VehicleID, "GBMissionFire")							
						DECOR_SET_INT(VehicleID, "GBMissionFire", g_iSmugglerUniqueMissionID)							
						PRINTLN("ADD_SET_FIRE_AFTER_GANG_BOSS_MISSION_ENDS_DECORATOR - setting with value ", g_iSmugglerUniqueMissionID)	
					ELSE
						PRINTLN("ADD_SET_FIRE_AFTER_GANG_BOSS_MISSION_ENDS_DECORATOR - decor already exists.")	
					ENDIF
				ELSE
					PRINTLN("ADD_SET_FIRE_AFTER_GANG_BOSS_MISSION_ENDS_DECORATOR - decor not registered.")	
				ENDIF
			ELSE
				PRINTLN("ADD_SET_FIRE_AFTER_GANG_BOSS_MISSION_ENDS_DECORATOR - g_iSmugglerUniqueMissionID = -1")	
			ENDIF
		ELSE
			PRINTLN("ADD_SET_FIRE_AFTER_GANG_BOSS_MISSION_ENDS_DECORATOR - not got control")
		ENDIF
	ELSE
		PRINTLN("ADD_SET_FIRE_AFTER_GANG_BOSS_MISSION_ENDS_DECORATOR - entity dead")
	ENDIF

ENDPROC

PROC PRELOAD_VEHICLE_MODS(VEHICLE_INDEX VehicleID, VEHICLE_SETUP_STRUCT_MP VehicleData)
	
	IF (GET_NUM_MOD_KITS(VehicleID) > 0)
	
		SET_VEHICLE_MOD_KIT(VehicleID, 0)
		
		#IF IS_DEBUG_BUILD
		NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Activating mod kit on vehicle: ") NET_PRINT(GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(VehicleID)) NET_NL()
		#ENDIF
		
		IF VehicleData.VehicleSetup.iModIndex[MOD_LIVERY] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_LIVERY, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_LIVERY]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_LIVERY, VehicleData.VehicleSetup.iModIndex[MOD_LIVERY])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_ROOF] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_ROOF, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_ROOF]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_ROOF, VehicleData.VehicleSetup.iModIndex[MOD_ROOF])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_EXHAUST] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_EXHAUST, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_EXHAUST]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_EXHAUST, VehicleData.VehicleSetup.iModIndex[MOD_EXHAUST])
		ENDIF
		
		IF VehicleData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_REAR_WHEELS, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_REAR_WHEELS]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_REAR_WHEELS, VehicleData.VehicleSetup.iModIndex[MOD_REAR_WHEELS])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_WHEELS] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_WHEELS, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_WHEELS]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_WHEELS, VehicleData.VehicleSetup.iModIndex[MOD_WHEELS])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_BUMPER_F] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_BUMPER_F, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_BUMPER_F]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_BUMPER_F, VehicleData.VehicleSetup.iModIndex[MOD_BUMPER_F])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_BUMPER_R] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_BUMPER_R, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_BUMPER_R]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_BUMPER_R, VehicleData.VehicleSetup.iModIndex[MOD_BUMPER_R])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_SKIRT] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_SKIRT, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_SKIRT]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_SKIRT, VehicleData.VehicleSetup.iModIndex[MOD_SKIRT])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_CHASSIS] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_CHASSIS, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_CHASSIS]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_CHASSIS, VehicleData.VehicleSetup.iModIndex[MOD_CHASSIS])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_CHASSIS2] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_CHASSIS2, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_CHASSIS2]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_CHASSIS2, VehicleData.VehicleSetup.iModIndex[MOD_CHASSIS2])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_GRILL] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_GRILL, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_GRILL]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_GRILL, VehicleData.VehicleSetup.iModIndex[MOD_GRILL])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_BONNET] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_BONNET, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_BONNET]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_BONNET, VehicleData.VehicleSetup.iModIndex[MOD_BONNET])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_SPOILER] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_SPOILER, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_SPOILER]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_SPOILER, VehicleData.VehicleSetup.iModIndex[MOD_SPOILER])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_WING_L] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_WING_L, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_WING_L]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_WING_L, VehicleData.VehicleSetup.iModIndex[MOD_WING_L])
		ENDIF

		IF VehicleData.VehicleSetup.iModIndex[MOD_WING_R] != 0
			NET_PRINT("[Spawning] PRELOAD_VEHICLE_MODS - Preloading MOD_WING_R, index ") NET_PRINT_INT(VehicleData.VehicleSetup.iModIndex[MOD_WING_R]) NET_NL()
			PRELOAD_VEHICLE_MOD(VehicleID, MOD_WING_R, VehicleData.VehicleSetup.iModIndex[MOD_WING_R])
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL WarpPlayerIntoSpawnVehicle(VECTOR vCoord, FLOAT fHeading, BOOL bSetPosIfUsingLastVehicle)

	#IF IS_DEBUG_BUILD
		NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - called with ") 
		NET_PRINT(" vCoord = ") NET_PRINT_VECTOR(vCoord)
		NET_PRINT(" fHeading = ") NET_PRINT_FLOAT(fHeading)
		NET_PRINT(" bSetPosIfUsingLastVehicle = ") NET_PRINT_BOOL(bSetPosIfUsingLastVehicle)
		NET_NL()
	#ENDIF
	
	IF IS_PLAYER_SPECTATING(PLAYER_ID())
		PRINTLN("[spawning] WarpPlayerIntoSpawnVehicle - player is spectating, don't warp into car")
	ENDIF
	
	VEHICLE_INDEX LastVehicleID = GET_PLAYERS_LAST_USED_VEHICLE()
	
	//VEHICLE_INDEX CarID
	NETWORK_INDEX NetID
	PLAYER_INDEX PartnerID	
	
	// make sure player is unfrozen and has collision before we warp them into the vehicle, as we can't do it once they are inside. 
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), false)  
	SET_ENTITY_COLLISION(PLAYER_PED_ID(), true)		
	
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressInAirEvent, TRUE) // make sure player can't fall from the sky

	g_SpawnData.MissionSpawnDetails.bSpawningCreatedNewCar = FALSE
	
	BOOL bDriverSpawnNewCar = FALSE
	BOOL bMultiVehicle = FALSE
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	AND NOT g_bMissionEnding 
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen < FMMC_MAX_TEAMS 
		AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen > -1		
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
				bMultiVehicle = TRUE
		
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen].iTeamBitSet3, ciBS3_ENABLE_DRIVER_RESPAWN_SAME_AVAILABLE_VEH)				
					NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - bDriverSpawnNewCar = TRUE ") NET_NL()
					bDriverSpawnNewCar = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// if rally mode and passenger - always let the driver spawn the car
	IF IsTeamModeSetForRespawning()
	AND NOT bDriverSpawnNewCar
		IF IsRespawnPassenger()
			// do i still have a driver?
			IF DoIHaveAVehiclePartner(PartnerID)
				IF IsPlayerInDriveableCar(PartnerID)
					IF NOT bMultiVehicle
						NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - warping into car as passenger ") NET_NL()
						WarpPlayerIntoCar(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PartnerID)), VS_FRONT_RIGHT)
						RETURN(TRUE)
					ELSE
						IF INT_TO_ENUM(VEHICLE_SEAT, GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE()) != VS_DRIVER
						OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen].iTeamBitSet3, ciBS3_ENABLE_DRIVER_RESPAWN_SAME_AVAILABLE_VEH) //This will be safe from team overruns because of bMultiVehicle.
							
							StoreSpawnVehicleHandle(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PartnerID)))
							PRINTLN("[spawning] WarpPlayerIntoSpawnVehicle - iMissionPartner:  ", NATIVE_TO_INT(PartnerID))
							
							VEHICLE_SEAT vs_ToUse = INT_TO_ENUM(VEHICLE_SEAT, GetFirstFreeSeatOfVehicle(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PartnerID)), TRUE))
							
							IF IS_VEHICLE_SEAT_FREE(g_SpawnData.MissionSpawnDetails.SpawnedVehicle, INT_TO_ENUM(VEHICLE_SEAT, GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE()))
								vs_ToUse = INT_TO_ENUM(VEHICLE_SEAT, GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE())
							ENDIF
							
							WarpPlayerIntoCar(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PartnerID)), vs_ToUse)
							
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PartnerID)))
								RETURN(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - is a passenger, but drivers is not in driveable car. ") NET_NL()	
					RETURN(FALSE)
				ENDIF
			ELSE
				NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - is a passenger, but dont have driver. ") NET_NL()
			ENDIF
		ENDIF
	ENDIF	

	IF (g_SpawnData.MissionSpawnDetails.bSpawnInLastVehicleIfDriveable)		

		IF IsVehicleSuitableToUseAsWarpVehicle(LastVehicleID)
			
			IF (IS_SPAWNING_IN_VEHICLE() AND (GET_ENTITY_MODEL(LastVehicleID) = g_SpawnData.MissionSpawnDetails.SpawnModel))
			OR NOT IS_SPAWNING_IN_VEHICLE()				
				
				IF NOT IS_ANY_PLAYER_NEAR_POINT(GET_ENTITY_COORDS(LastVehicleID), 30.0)									
					
					IF (bSetPosIfUsingLastVehicle)
						SET_ENTITY_COORDS(LastVehicleID, GET_GROUND_COORD_ADJUSTED_FOR_MODEL_DIMENSIONS( g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel, vCoord))
						SET_ENTITY_HEADING(LastVehicleID, fHeading)
					ENDIF
					
					SetSpawnVehicleAttributes(LastVehicleID)											
					NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - spawning in last vehicle ") NET_NL()	
					
					RETURN(TRUE)									
				ELSE
					NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - another player is too near ") NET_NL()		
				ENDIF

			ELSE	
				NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - last vehicle is not the desired model ") NET_NL()	
			ENDIF
			
		ELSE
			NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - last vehicle is not driveable ") NET_NL()
		ENDIF
	ENDIF

	// delete last vehicle if on a race
	IF (g_b_On_Race)
	OR IS_PLAYER_USING_ARENA()
	OR g_bDMUsingSpawnVehicle
		NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - calling  SetVehicleToDelete(LastVehicleID)") NET_NL()
		SetVehicleToDelete(LastVehicleID)
	ENDIF

	IF HasLoadedVehicleSpawnModelIfNecessary()	
	
		NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - calling g_SpawnData.MissionSpawnDetails.SpawnedVehicle") NET_NL()
		SetVehicleToDelete(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
		
		IF GoodToUsePersonalVehicleToSpawn()
		
			// if it has just been created use that.
			IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_JUST_CREATED)
			AND IS_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_ID())
				PRINTLN("[spawning] WarpPlayerIntoSpawnVehicle - personal vehicle just been created, so use that.")
				SET_ENTITY_COORDS(PERSONAL_VEHICLE_ID(), GET_GROUND_COORD_ADJUSTED_FOR_MODEL_DIMENSIONS( g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel, vCoord))
				SET_ENTITY_HEADING(PERSONAL_VEHICLE_ID(), fHeading)
				SetVehicleForWarpIntoSpawnVehicle(PERSONAL_VEHICLE_ID())				
				RETURN(TRUE)
			ELSE
	
				// if not called from freemode
				IF (GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) != FREEMODE_HASH())
					IF (REQUEST_PERSONAL_VEHICLE_SPAWN_AT_COORDS(GET_GROUND_COORD_ADJUSTED_FOR_MODEL_DIMENSIONS( g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel, vCoord), fHeading, CURRENT_SAVED_VEHICLE_SLOT(), TRUE))
						NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - REQUEST_PERSONAL_VEHICLE_SPAWN_AT_COORDS = TRUE ") NET_NL()
						SetVehicleForWarpIntoSpawnVehicle(PERSONAL_VEHICLE_ID())				
						RETURN(TRUE)	
					ENDIF
				ELSE				
					IF CREATE_MP_SAVED_VEHICLE(GET_GROUND_COORD_ADJUSTED_FOR_MODEL_DIMENSIONS( g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel, vCoord), fHeading, TRUE, CURRENT_SAVED_VEHICLE_SLOT(), DEFAULT, TRUE)
						NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - CREATE_MP_SAVED_VEHICLE = TRUE ") NET_NL()
						SetVehicleForWarpIntoSpawnVehicle(PERSONAL_VEHICLE_ID())				
						RETURN(TRUE)				
					ELSE
						NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - CREATE_MP_SAVED_VEHICLE = FALSE ") NET_NL()				
					ENDIF
				ENDIF
			ENDIF

		ELSE	

			IF CREATE_NET_VEHICLE(NetID, g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel, 	GET_GROUND_COORD_ADJUSTED_FOR_MODEL_DIMENSIONS( g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.VehicleSetup.eModel, vCoord), fHeading, FALSE, TRUE, TRUE)				
				NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - CREATE_NET_VEHICLE = TRUE, new vehicle net id = ") NET_PRINT_INT(NATIVE_TO_INT(NetID)) NET_NL()	
				
				IF (g_SpawnData.MissionSpawnDetails.bSetVehicleStrong)
					SET_VEHICLE_STRONG(NET_TO_VEH(NetID),TRUE)
					NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - Setting vehicle as strong.") NET_NL()
				ENDIF
				
				IF (g_SpawnData.MissionSpawnDetails.bSetTyresBulletProof)
					SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(NetID),FALSE)
					NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - Setting vehicle tyres as bulletproof.") NET_NL()
				ENDIF
				
				IF (g_SpawnData.MissionSpawnDetails.bSetAsNonSaveable)
					IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
						DECOR_SET_INT(NET_TO_VEH(NetID), "Not_Allow_As_Saved_Veh", 1)
						NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - Setting vehicle tyres as non saveable.") NET_NL()
					ENDIF
				ENDIF
				
				IF (g_SpawnData.MissionSpawnDetails.bSetAsNonModable)
					IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
						INT iDecoratorValue
						IF DECOR_EXIST_ON(NET_TO_VEH(NetID), "MPBitset")
							iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(NetID), "MPBitset")
						ENDIF
						SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
						DECOR_SET_INT(NET_TO_VEH(NetID), "MPBitset", iDecoratorValue)
						NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - Setting vehicle tyres as non modable.") NET_NL()
					ENDIF
				ENDIF
				
				IF (g_SpawnData.MissionSpawnDetails.iColour > -1)
					PRINTLN( "[spawning] WarpPlayerIntoSpawnVehicle iColour equals: ", g_SpawnData.MissionSpawnDetails.iColour )
					SET_VEHICLE_COLOURS( NET_TO_VEH(NetID), g_SpawnData.MissionSpawnDetails.iColour, g_SpawnData.MissionSpawnDetails.iColour )
					SET_VEHICLE_EXTRA_COLOURS( NET_TO_VEH(NetID), g_SpawnData.MissionSpawnDetails.iColour, g_SpawnData.MissionSpawnDetails.iColour )
				ENDIF				
				
				IF (g_SpawnData.MissionSpawnDetails.bSetFireAfterGangBossMissionEnds)
					PRINTLN( "[spawning] WarpPlayerIntoSpawnVehicle bSetFireAfterGangBossMissionEnds")
					ADD_DECORATOR_TO_SET_FIRE_AFTER_GANG_BOSS_MISSION_ENDS(NET_TO_VEH(NetID))		
				ENDIF
				
				PRELOAD_VEHICLE_MODS(NET_TO_VEH(NetID), g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP)
				
				SetVehicleForWarpIntoSpawnVehicle(NET_TO_VEH(NetID))
						
				RETURN(TRUE)				
			ELSE
				NET_PRINT_FRAME() NET_PRINT("[spawning] WarpPlayerIntoSpawnVehicle - CREATE_NET_VEHICLE = FALSE ") NET_NL()
			ENDIF			
		ENDIF
	ENDIF
		
	RETURN(FALSE)
ENDFUNC



PROC MakeVehicleFlashIfDriver(INT iVehicleRespotTime, INT iPlayerInvincibleTime=0, BOOL bDontMarkAsNoLongerNeeded=FALSE)
	BOOL bSetAsMissionEntity = FALSE
	// make vehicle flash - only if driver of car
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND AmIDrivingAVehicle()
		VEHICLE_INDEX CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF CanEditThisVehicle(CarID, bSetAsMissionEntity)
			ApplyRespotTimer(CarID, iVehicleRespotTime, iPlayerInvincibleTime)
			IF (bSetAsMissionEntity)
			AND NOT (bDontMarkAsNoLongerNeeded)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(CarID)	
			ELSE
				PRINTLN("MakeVehicleFlashIfDriver - bDontMarkAsNoLongerNeeded = ", bDontMarkAsNoLongerNeeded)
			ENDIF			
		ENDIF
	ENDIF
ENDPROC

PROC RevealPlayersVehicleForRespawn(INT iRespotTime)

	NET_PRINT("[spawning] RevealPlayersVehicleForRespawn - called...") NET_NL() 
	
	VEHICLE_INDEX CarID
	BOOL bSetAsMissionEntity
	
	// make players vehicle visible
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF CanEditThisVehicle(CarID, bSetAsMissionEntity)
			ShowVehicleForRespawn(CarID, iRespotTime)
		ENDIF
		IF (bSetAsMissionEntity)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(CarID)	
			NET_PRINT("[spawning] RevealPlayersVehicleForRespawn - vehicle marked as no longer needed") NET_NL()
		ENDIF		
	ENDIF
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bPlayerIsHiddenForWarp = FALSE
	

ENDPROC

PROC MakeMinMaxValuesSafe(VECTOR &vMin, VECTOR &vMax)
	VECTOR vStoredMin
	VECTOR vStoredMax
	IF (vMin.x <= vMax.x)
		vStoredMin.x = vMin.x
		vStoredMax.x = vMax.x
	ELSE
		vStoredMin.x = vMax.x
		vStoredMax.x = vMin.x
	ENDIF
	IF (vMin.y <= vMax.y)
		vStoredMin.y = vMin.y
		vStoredMax.y = vMax.y
	ELSE
		vStoredMin.y = vMax.y
		vStoredMax.y = vMin.y
	ENDIF
	IF (vMin.z <= vMax.z)
		vStoredMin.z = vMin.z
		vStoredMax.z = vMax.z
	ELSE
		vStoredMin.z = vMax.z
		vStoredMax.z = vMin.z
	ENDIF	
	vMin = vStoredMin
	vMax = vStoredMax
ENDPROC

PROC SetSpawnArea(SPAWN_AREA SPArea, INT iIndex = 0)

	#IF IS_DEBUG_BUILD				
		IF NOT (g_SpawnData.MissionSpawnDetails.SpawnArea[iIndex].bIsActive)
			NET_PRINT("[spawning] SetSpawnArea called, making new area active. iIndex=") NET_PRINT_INT(iIndex) NET_NL()
			DEBUG_PRINT_SPAWN_AREA_DETAILS(SPArea)
		ELSE
			IF DEBUG_PRINT_SPAWN_AREA_DIFFERENCES(g_SpawnData.MissionSpawnDetails.SpawnArea[iIndex], SPArea)
				NET_PRINT("[spawning] SetSpawnArea new details above for index ") NET_PRINT_INT(iIndex) NET_NL()	
			ENDIF
		ENDIF
	#ENDIF
	
	g_SpawnData.MissionSpawnDetails.SpawnArea[iIndex] = SPArea
	g_SpawnData.MissionSpawnDetails.SpawnArea[iIndex].bIsActive = TRUE
ENDPROC

PROC SetMissionSpawnArea(VECTOR vCoord1, VECTOR vCoords2, FLOAT fFloat, INT iShape, INT iIndex=0, BOOL bIgnoreMissionCheck=FALSE, BOOL bConsiderCentrePointAsValid=FALSE, FLOAT fIncreaseDistForEachFallback=0.0)
	IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
	OR (bIgnoreMissionCheck)
		
		SPAWN_AREA SpArea
		
		// stop using custom spawn points
		IF (g_SpawnData.bUseCustomSpawnPoints)
			TurnOffCustomSpawnPoints()
		ENDIF		
		
		IF (SpArea.iShape = SPAWN_AREA_SHAPE_BOX)
			MakeMinMaxValuesSafe(vCoord1, vCoords2)
		ENDIF		
		
		SpArea.vCoords1 = vCoord1
		SpArea.vCoords2 = vCoords2
		SpArea.fFloat = fFloat
		SpArea.iShape = iShape
		SpArea.bConsiderCentrePointAsValid = bConsiderCentrePointAsValid
		SpArea.fIncreaseDist = fIncreaseDistForEachFallback
		IF (SpArea.fIncreaseDist < 0.0)
			SpArea.fIncreaseDist = 0.0
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF NOT IsSpawnAreaValid(SpArea, g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea)
			PRINTLN("[spawning] WARNING! SetMissionSpawnArea - spawn zone is not valid, it is encompassed by all of the exclusion area!")
		ENDIF
		#ENDIF
		
		g_SpawnData.MissionSpawnDetails.Mission = GET_MP_MISSION_PLAYER_IS_ON(PLAYER_ID())
		SetSpawnArea(SpArea, iIndex)

	ELSE
		SCRIPT_ASSERT("SetMissionSpawnArea - player is not on a mission!")
	ENDIF
ENDPROC


PROC SetExclusionArea(SPAWN_AREA ExArea, INT iIndex = 0)

	#IF IS_DEBUG_BUILD				
		IF NOT (g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[iIndex].bIsActive)
			NET_PRINT("[spawning] SetExclusionArea called, making new area active. iIndex=") NET_PRINT_INT(iIndex) NET_NL()
			DEBUG_PRINT_SPAWN_AREA_DETAILS(ExArea)
		ELSE
			IF DEBUG_PRINT_SPAWN_AREA_DIFFERENCES(g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[iIndex], ExArea)
				NET_PRINT("[spawning] SetExclusionArea new details above for index ") NET_PRINT_INT(iIndex) NET_NL()	
			ENDIF
		ENDIF
	#ENDIF
	
	g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[iIndex] = ExArea
	g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[iIndex].bIsActive = TRUE
ENDPROC


PROC SetMissionExclusionArea(VECTOR vCoord1, VECTOR vCoords2, FLOAT fFloat, INT iShape, INT iIndex=0, BOOL bIgnoreMissionCheck=FALSE)
	IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
	OR (bIgnoreMissionCheck)
		
		SPAWN_AREA ExArea
		
		IF (ExArea.iShape = SPAWN_AREA_SHAPE_BOX)
			MakeMinMaxValuesSafe(vCoord1, vCoords2)
		ENDIF	
		
		ExArea.vCoords1 = vCoord1
		ExArea.vCoords2 = vCoords2
		ExArea.fFloat = fFloat
		ExArea.iShape = iShape		
	
		IF IsExclusionAreaValid(ExArea)
			g_SpawnData.MissionSpawnDetails.Mission = GET_MP_MISSION_PLAYER_IS_ON(PLAYER_ID())
			SetExclusionArea(ExArea, iIndex)
		ELSE
			SCRIPT_ASSERT("SetMissionExclusionArea - exclusion zone is not valid, it encompasses all of the spawn area!")
		ENDIF
	ELSE
		SCRIPT_ASSERT("SetMissionExclusionArea - player is not on a mission!")
	ENDIF
ENDPROC


FUNC BOOL HasPointGotCollision(VECTOR &vCoords)
	REQUEST_COLLISION_AT_COORD(vCoords)	
	IF GET_SAFE_COORD_FOR_PED(vCoords, FALSE, vCoords)
		vCoords.z += -1.0
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC


FUNC BOOL RestoreScreenFromSpawnBlur()
	
	NET_PRINT("***RestoreScreenFromSpawnBlur has been called.")NET_NL()
	
	//Don't do this if we are restarting FM
	IF IS_TRANSITION_SESSION_LAUNCHING()
	OR g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderBoardCamReady
	
		#IF IS_DEBUG_BUILD
			IF IS_TRANSITION_SESSION_LAUNCHING()
				NET_PRINT("***IS_TRANSITION_SESSION_LAUNCHING() = TRUE, returning TRUE.")NET_NL()
			ENDIF
			IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping
				NET_PRINT("***bLeaderboardCamRenderingOrInterping = TRUE, returning TRUE.")NET_NL()
			ENDIF
		#ENDIF
		
		RETURN(TRUE)
		
	ELSE
		
		NET_PRINT("***IS_TRANSITION_SESSION_LAUNCHING() = FALSE and bLeaderboardCamRenderingOrInterping = FALSE.")NET_NL()
		
	ENDIF
	
	IF (g_SpawnData.bIsBlurredOut)
		IF NOT SET_SKYBLUR_CLEAR()	
			NET_PRINT("[spawning] RestoreScreenFromSpawnBlur - waiting to clear blur") NET_NL()
			RETURN(FALSE)
		ENDIF
	ENDIF
	
	SET_SKYBLUR_CLEAR()
	
	g_SpawnData.bIsBlurredOut = FALSE
	
	RETURN(TRUE)
	
ENDFUNC

FUNC BOOL MakeScreenBlurredOutForSpawn()
//	IF NOT (g_SpawnData.bIsBlurredOut)	
//		IF SET_SKYBLUR_BLURRY()
//			g_SpawnData.bIsBlurredOut = TRUE
//		ELSE
//			NET_PRINT("[spawning] MakeScreenBlurredOutForSpawn - waiting to make blur") NET_NL()
//		ENDIF		
//	ENDIF
//	RETURN(g_SpawnData.bIsBlurredOut)
	RETURN TRUE
ENDFUNC


PROC MakePlayerVisibleLocally()
	IF NOT IS_SCREEN_FADED_OUT()
		VEHICLE_INDEX CarID
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				SET_ENTITY_LOCALLY_VISIBLE(CarID) 
				#IF IS_DEBUG_BUILD
				PRINTLN("[Spawning] MakePlayerVisibleLocally - Setting vehicle: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(CarID)), " with Id: ", NATIVE_TO_INT(CarID), " locally visible this frame.")
				#ENDIF
			ENDIF
		ENDIF
		IF NOT IS_ANY_TYPE_OF_CUTSCENE_PLAYING()
			SET_LOCAL_PLAYER_VISIBLE_LOCALLY()
			PRINTLN("[Spawning] MakePlayerVisibleLocally - Setting local player visible locally this frame.")
		ENDIF
	ENDIF
ENDPROC

PROC DeleteFadedOutVehicle(VEHICLE_INDEX VehicleID)
	
	PRINTLN("DeleteFadedOutVehicle - called with vehicle id ", NATIVE_TO_INT(VehicleID))

	// Delete faded out vehicle that is not a mission entity.
	IF NOT IS_ENTITY_A_MISSION_ENTITY(VehicleID)
	AND NETWORK_GET_ENTITY_IS_LOCAL(VehicleID)
		NET_PRINT("[Spawning] DeleteFadedOutVehicle - Deleting faded out vehicle.") NET_NL()
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		SET_ENTITY_AS_MISSION_ENTITY(VehicleID, FALSE, TRUE)
		DELETE_VEHICLE(VehicleID)
	ENDIF
ENDPROC

FUNC BOOL HasFadedOutVehicle()
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
			VEHICLE_INDEX VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())	
			
			IF NOT (g_SpawnData.bFadingOutForWarp)						
				
				BOOL bSetAsMissionEntity
				
				IF CanEditThisVehicle(VehicleID, bSetAsMissionEntity)
					
					IF IS_ENTITY_VISIBLE_TO_SCRIPT(VehicleID)
						NETWORK_FADE_OUT_ENTITY(VehicleID, TRUE, TRUE) 	
					ENDIF
					
					g_SpawnData.bFadingOutForWarp = TRUE
					g_SpawnData.FadingOutTimer = GET_NETWORK_TIME()
					
					IF (bSetAsMissionEntity)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID)
					ENDIF
					
					NET_PRINT("[Spawning] HasFadedOutVehicle - Fading vehicle out.") NET_NL()
					
					RETURN FALSE
				ELSE
					NET_PRINT("[Spawning] HasFadedOutVehicle - Can't edit vehicle.") NET_NL()
				ENDIF
			ELSE
				// B*4347382, clearing ped tasks sets player visible, resetting to invisible.
				IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE) 
					NET_PRINT("[Spawning] HasFadedOutVehicle - Setting player invisible.") NET_NL()
				ENDIF
			
				IF IS_ENTITY_VISIBLE_TO_SCRIPT(VehicleID)
					IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.FadingOutTimer) < 2000)
						NET_PRINT("[Spawning] HasFadedOutVehicle - Returning FALSE, vehicle still visble to script. Time ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.FadingOutTimer)) NET_NL()	
						RETURN FALSE
					ELSE
						NET_PRINT("[Spawning] HasFadedOutVehicle - g_SpawnData.FadingOutTimer expired.") NET_NL()	
						DeleteFadedOutVehicle(VehicleID)
					ENDIF
				ELSE				
					// Delete faded out vehicle that is not a mission entity.
					NET_PRINT("[Spawning] HasFadedOutVehicle - Vehicle is not visible to script.") NET_NL()
					DeleteFadedOutVehicle(VehicleID)														
				ENDIF
			ENDIF
		ELSE
			NET_PRINT("[Spawning] HasFadedOutVehicle - Local ped is not in any vehicle.") NET_NL()	
		ENDIF
	ELSE
		NET_PRINT("[Spawning] HasFadedOutVehicle - IS_NET_PLAYER_OK = FALSE") NET_NL()	
	ENDIF
	
	g_SpawnData.bFadingOutForWarp = FALSE
	
	RETURN TRUE	
ENDFUNC

FUNC BOOL FadeOutForRespawn(INT iFadeTime, BOOL bDoVehicleFade)
	
	IF IS_TRANSITION_ACTIVE() 
		NET_PRINT("[spawning] FadeOutForRespawn - IS_TRANSITION_ACTIVE") NET_NL()
		RETURN(FALSE)
	ENDIF

	IF (iFadeTime > 0)
		MakePlayerVisibleLocally()
	ENDIF
	
	IF NOT (g_SpawnData.bIsSwoopedUp)		
		IF NOT g_b_RespawnUseWhiteOutIn
			IF (NOT IS_SCREEN_FADED_OUT() AND IS_SKYSWOOP_AT_GROUND())
			OR ((bDoVehicleFade = TRUE) AND (HasFadedOutVehicle()=FALSE))
				IF NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_OUT(iFadeTime)
					NET_PRINT("[spawning] FadeOutForRespawn - starting fade out") NET_NL()
				ENDIF
			ELSE
				g_SpawnData.bIsSwoopedUp = TRUE
			ENDIF
		ELSE
			IF IS_SKYSWOOP_AT_GROUND()
			AND NOT IS_RECTANGLE_FADED_IN()
				IF NOT IS_RECTANGLE_FADING_IN()
					NET_PRINT("[spawning] FadeOutForRespawn - starting fade out (WHITE RECTANGE) g_b_RespawnUseWhiteOutIn = TRUE") NET_NL()
					SET_RECTANGLE_COLOUR(TRUE)
					ACTIVATE_RECTANGLE_FADE_OUT(FALSE)
					ACTIVATE_RECTANGLE_FADE_IN(TRUE)
				ENDIF
			ELSE
				g_SpawnData.bIsSwoopedUp = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN(g_SpawnData.bIsSwoopedUp)
ENDFUNC



FUNC BOOL IsOKToUnfreezePlayerEarly(BOOL bIgnoreTransitionCheck=FALSE)
	// set player control
	IF ((NOT IS_TRANSITION_ACTIVE()) OR (bIgnoreTransitionCheck=TRUE))
	AND NOT MPGlobalsAmbience.bRunningFmIntroCut
	AND NOT IS_TRANSITION_SESSION_LAUNCHING() 
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] IsOKToUnfreezePlayerEarly - swoop cam is active") NET_NL()
			#ENDIF
		ELSE
			IF (g_b_OnLeaderboard)
			OR (g_b_OnResults) // 1072410
				#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] IsOKToUnfreezePlayerEarly - on leaderboard DO NOT turn on player control") NET_NL()
				#ENDIF				
			ELSE
				//If we should join a launched mission
				IF SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] IsOKToUnfreezePlayerEarly - SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION") NET_NL()
					#ENDIF				
				//Mo, then it's all normal
				ELIF NOT IS_TRANSITION_SESSION_LAUNCHING()
					IF DID_I_JOIN_MISSION_AS_SPECTATOR()					
						#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] IsOKToUnfreezePlayerEarly - DID_I_JOIN_MISSION_AS_SPECTATOR") NET_NL()
						#ENDIF						
					ELSE
						IF NOT g_SpawnData.bDisableFadeInAndTurnOnControls
							IF NOT IS_PED_IN_ANY_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(PLAYER_PED_ID())
								IF HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())									
									VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
									IF GET_GROUND_Z_FOR_3D_COORD(vPlayerCoords, vPlayerCoords.z) 									
										#IF IS_DEBUG_BUILD
											NET_PRINT("[spawning] IsOKToUnfreezePlayerEarly - Returning TRUE") NET_NL()
										#ENDIF
										RETURN(TRUE)											
									ELSE
										#IF IS_DEBUG_BUILD
											NET_PRINT("[spawning] IsOKToUnfreezePlayerEarly - cannot get ground z, vPlayerCoords = ") NET_PRINT_VECTOR(vPlayerCoords) NET_NL()
										#ENDIF
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									NET_PRINT("[spawning] IsOKToUnfreezePlayerEarly - in vehicle.") NET_NL()
								#ENDIF
							ENDIF
						ELSE
							PRINTLN("[spawning] IsOKToUnfreezePlayerEarly - respawning, not returning player control g_SpawnData.bDisableFadeInAndTurnOnControls")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF IS_TRANSITION_ACTIVE()
				PRINTLN("[spawning] IsOKToUnfreezePlayerEarly - IS_TRANSITION_ACTIVE")
			ENDIF
			IF MPGlobalsAmbience.bRunningFmIntroCut
				PRINTLN("[spawning] IsOKToUnfreezePlayerEarly - MPGlobalsAmbience.bRunningFmIntroCut")
			ENDIF
			IF IS_TRANSITION_SESSION_LAUNCHING() 				
				PRINTLN("[spawning] IsOKToUnfreezePlayerEarly - IS_TRANSITION_SESSION_LAUNCHING")
			ENDIF
		#ENDIF
	ENDIF
	
	NET_PRINT("[spawning] IsOKToUnfreezePlayerEarly - Returning FALSE") NET_NL()
	RETURN(FALSE)
	
ENDFUNC

FUNC BOOL PlayerIsInBlazer5()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF DOES_ENTITY_EXIST(VehicleID)
			IF (GET_ENTITY_MODEL(VehicleID) = BLAZER5)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PlayerIsInRuiner2()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
		IF DOES_ENTITY_EXIST(VehicleID)
			IF (GET_ENTITY_MODEL(VehicleID) = RUINER2)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PlayerIsInThruster() 
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF DOES_ENTITY_EXIST(VehicleID)
			IF (GET_ENTITY_MODEL(VehicleID) = THRUSTER)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//FUNC BOOL IsMyLandingGearBusy()
//	NET_PRINT("[spawning] IsMyLandingGearBusy - called.") NET_NL()
//	VEHICLE_INDEX CarID
//	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//		IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(CarID))	
//			LANDING_GEAR_STATE CurrentState = GET_LANDING_GEAR_STATE(CarID)
//			IF (CurrentState = LGS_RETRACTING)
//			OR (CurrentState = LGS_DEPLOYING)
//				NET_PRINT("[spawning] IsMyLandingGearBusy - returning TRUE.") NET_NL()
//  				RETURN(TRUE)
//			ENDIF
//		ENDIF
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC

PROC FixUpMyVehicle()
	NET_PRINT("[spawning] FixUpMyVehicle - called.") NET_NL()
	VEHICLE_INDEX CarID
	BOOL bSetAsMissionEntity
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF CAN_EDIT_THIS_ENTITY(CarID, bSetAsMissionEntity)
			SET_VEHICLE_FIXED(CarID)	
			NET_PRINT("[spawning] FixUpMyVehicle - SET_VEHICLE_FIXED.") NET_NL()
		ENDIF
		IF (bSetAsMissionEntity)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(CarID)
			NET_PRINT("[spawning] FixUpMyVehicle - SET_VEHICLE_AS_NO_LONGER_NEEDED.") NET_NL()
		ENDIF
	ENDIF
ENDPROC

PROC SetLandingGearOfMyVehicle()
	NET_PRINT("[spawning] SetLandingGearOfMyVehicle - called.") NET_NL()
	VEHICLE_INDEX CarID
	BOOL bSetAsMissionEntity
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF CAN_EDIT_THIS_ENTITY(CarID, bSetAsMissionEntity)	
			SetLandingGear(CarID)
			NET_PRINT("[spawning] SetLandingGearOfMyVehicle - done.") NET_NL()
		ELSE
			g_SpawnData.iFrameCountForLandingGear  = LANDING_GEAR_FRAME_DELAY
			NET_PRINT("[spawning] SetLandingGearOfMyVehicle - iFrameCountForLandingGear = LANDING_GEAR_FRAME_DELAY 1.") NET_NL()
		ENDIF
		IF (bSetAsMissionEntity)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(CarID)
			NET_PRINT("[spawning] SetLandingGearOfMyVehicle - SET_VEHICLE_AS_NO_LONGER_NEEDED.") NET_NL()
		ENDIF
	ELSE
		g_SpawnData.iFrameCountForLandingGear  = LANDING_GEAR_FRAME_DELAY
		NET_PRINT("[spawning] SetLandingGearOfMyVehicle - iFrameCountForLandingGear = LANDING_GEAR_FRAME_DELAY 2.") NET_NL()
	ENDIF
ENDPROC

FUNC BOOL SHOULD_FADE_IN_BE_DELAYED()
	
	IF IS_SPECIAL_VEHICLE_RACE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FadeInAfterRespawn_FadeIn(INT iFadeTime=500)
	IF NOT g_b_RespawnUseWhiteOutIn
		DO_SCREEN_FADE_IN(iFadeTime)
	ELSE
		SET_RECTANGLE_COLOUR(TRUE)
		ACTIVATE_RECTANGLE_FADE_OUT(TRUE) // these work inversely.
		ACTIVATE_RECTANGLE_FADE_IN(FALSE) // these work inversely.
	ENDIF
ENDPROC

FUNC BOOL FadeInAfterRespawn(INT iFadeTime=500)

	// We do not want the Fade to happen if we are in Roaming Spectator Mode. We handle this separately.
	IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		RETURN TRUE
	ENDIF

	// if we are doing a swoop up camera from an interior we do not want to fade in yet.
	IF HAS_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH_STARTED()
		PRINTLN("FadeInAfterRespawn - HAS_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH_STARTED - returning TRUE")
		RETURN TRUE
	ENDIF
	
	// url:bugstar:6198481 - Finale - STEALTH_2B_RAPPEL - Local player died during a transition. This caused the player to spawn outside the Casino and have control. Player was then transitioned inside once remote players loaded in. 
	IF g_bDisableFadeInAfterRespawnWhenMissionStrandInitialising // so we can disable if need be in bgscript
	AND IS_A_STRAND_MISSION_BEING_INITIALISED()
		PRINTLN("FadeInAfterRespawn - IS_A_STRAND_MISSION_BEING_INITIALISED")
		g_SpawnData.bDisableFadeInAndTurnOnControls = TRUE
	ENDIF
	
	BOOL bIsScreenFadedOut, bIsScreenFadingIn, bIsScreenFadingOut
	
	IF NOT g_b_RespawnUseWhiteOutIn
		bIsScreenFadedOut = IS_SCREEN_FADED_OUT()		
		bIsScreenFadingIn = IS_SCREEN_FADING_IN()
		bIsScreenFadingOut = IS_SCREEN_FADING_OUT()	
	ELSE
		bIsScreenFadedOut = IS_RECTANGLE_FADED_IN()  // these work inversely.		
		bIsScreenFadingIn = IS_RECTANGLE_FADING_OUT() // these work inversely.
		bIsScreenFadingOut = IS_RECTANGLE_FADING_IN() // these work inversely.
	ENDIF
	
	// Fix for B*1511388
	IF IS_HAS_GOT_SPAWN_LOCATION_RUNNING() 
		NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Waiting for warp request to finish.") 
		NET_PRINT(" g_SpawnData.iWarpState = ") NET_PRINT_INT(g_SpawnData.iWarpState)
		NET_NL()
		RETURN(FALSE)
	ELSE
		IF NOT g_SpawnData.bDisableFadeInAndTurnOnControls
			// Fix for B*3458100
			PED_INDEX playerID = PLAYER_PED_ID()
			
			IF DOES_ENTITY_EXIST(playerID)
				IF HAS_PED_HEAD_BLEND_FINISHED(playerID)
			
					IF (iFadeTime >= 0)
						MakePlayerVisibleLocally()
					ENDIF
					
					IF NOT HAS_LOCAL_PLAYER_JUST_DONE_PASSIVE_MODE_CUTSCENE()
						// Fix for B*2209433
						IF NOT (g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail) 
							IF bIsScreenFadedOut
								IF NOT bIsScreenFadingIn
								AND NOT bIsScreenFadingOut
								
									IF (g_SpawnData.bUpdateSpawnCam)							
										FixUpMyVehicle()
										SetLandingGearOfMyVehicle()
										NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Waiting for g_SpawnData.bUpdateSpawnCam") NET_NL()								
										RETURN(FALSE)
									ELSE
										// It annoyingly takes a couple of frames for the landing gear to go up or down, also the plane can't be froze. 
										IF (g_SpawnData.iFrameCountForLandingGear > LANDING_GEAR_FRAME_DELAY)
											// This is so we don't see the player drop to the ground.
											IF IsOKToUnfreezePlayerEarly()
												IF (g_SpawnData.bUnfreezingEarly = FALSE)
													SetPlayerOnGroundProperly()
													
													IF DOES_ENTITY_EXIST(playerID)

														IF playerID = PLAYER_PED_ID()
															IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
																SET_ENTITY_COLLISION(playerID, TRUE)	
																FREEZE_ENTITY_POSITION(playerID, FALSE)
																#IF IS_DEBUG_BUILD
																NET_PRINT("[Spawning] FadeInAfterRespawn - Setting entity collision to TRUE and freezing entity position.") NET_NL()
																#ENDIF
															ELSE
																#IF IS_DEBUG_BUILD
																NET_PRINT("[Spawning] FadeInAfterRespawn - Unable to adjust entity parameters when spectating.") NET_NL()
																#ENDIF
															ENDIF
														ELSE
															#IF IS_DEBUG_BUILD
															NET_PRINT("[Spawning] FadeInAfterRespawn - Cannot set collision or freeze an entity owned by another machine.") NET_NL()
															#ENDIF
														ENDIF
													ELSE
														#IF IS_DEBUG_BUILD
														NET_PRINT("[Spawning] FadeInAfterRespawn - Entity does not exist, unable to set entity collision and freeze position.") NET_NL()
														#ENDIF
													ENDIF
													
													g_SpawnData.iUnfreezeTimer = GET_NETWORK_TIME()
													g_SpawnData.bUnfreezingEarly = TRUE
													NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Unfreeze player early = TRUE.") NET_NL()
													RETURN(FALSE)
												ELSE
													IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iUnfreezeTimer)) > 250) 
														FadeInAfterRespawn_FadeIn(iFadeTime)
														NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Starting fade in (after IsOKToUnfreezePlayerEarly), with fade time of ") NET_PRINT_INT(iFadeTime) NET_NL()
													ELSE
														NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Waiting for unfreeze player early.") NET_NL()
														RETURN(FALSE)
													ENDIF
												ENDIF
											ELSE												
												IF SHOULD_FADE_IN_BE_DELAYED()
												
													IF !g_SpawnData.bShouldFadeInBeDelayed
														g_SpawnData.bShouldFadeInBeDelayed = TRUE
														g_SpawnData.iFadeInDelayTimer = GET_NETWORK_TIME()
														RETURN FALSE
													ELSE
														IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iFadeInDelayTimer)) > g_SpawnData.iFadeInDelayTimeToExpire)
															FadeInAfterRespawn_FadeIn(iFadeTime)
															NET_PRINT_FRAME() NET_PRINT("[Spawning] FadeInAfterRespawn - Starting fade in with fade time of ") NET_PRINT_INT(iFadeTime) NET_NL()
														
															g_SpawnData.bShouldFadeInBeDelayed = FALSE
														ELSE
															IF (GET_FRAME_COUNT() % 10) = 0
																NET_PRINT_FRAME() NET_PRINT("[Spawning] FadeInAfterRespawn - Waiting for g_SpawnData.iFadeInDelayTimeToExpire") NET_NL()
															ENDIF
															
															RETURN FALSE
														ENDIF
													ENDIF
												ELSE
													
													IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
														IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
															iFadeTime = iFadeTime + 100 // B*4391436, increase fade to prevent player seeing vehicle pop.
														ENDIF
													ENDIF
													
													PRINTLN("[Spawning] FadeInAfterRespawn - Setting gameplay cam relative heading and pitch to 0.0")
													SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
													SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
													
													FadeInAfterRespawn_FadeIn(iFadeTime)
													NET_PRINT_FRAME() NET_PRINT("[Spawning] FadeInAfterRespawn - Starting fade in with fade time of ") NET_PRINT_INT(iFadeTime) NET_NL()	
													
												ENDIF
											ENDIF	
										ELSE
											IF DOES_ENTITY_EXIST(g_SpawnData.LandingGearVehicleID)
												IF NOT IS_ENTITY_DEAD(g_SpawnData.LandingGearVehicleID)
													FREEZE_ENTITY_POSITION(g_SpawnData.LandingGearVehicleID, FALSE)
													
													// Additional Debug for B*3796642
													PRINTLN("[spawning] FadeInAfterRespawn - g_SpawnData.LandingGearVehicleID = ", NATIVE_TO_INT(g_SpawnData.LandingGearVehicleID))
													PRINTLN("[spawning] FadeInAfterRespawn - g_SpawnData.LandingGearVehicleID Position: <<", g_SpawnData.vVehPosForLandingGear.X, ", ", g_SpawnData.vVehPosForLandingGear.Y, ", ", g_SpawnData.vVehPosForLandingGear.Z, ">>")
													PRINTLN("[spawning] FadeInAfterRespawn - g_SpawnData.LandingGearVehicleID Heading: ", g_SpawnData.fVehHeadingForLandingGear)
													
													IF (g_SpawnData.iFrameCountForLandingGear = 0)
														g_SpawnData.vVehPosForLandingGear = GET_ENTITY_COORDS(g_SpawnData.LandingGearVehicleID, FALSE)
														g_SpawnData.fVehHeadingForLandingGear = GET_ENTITY_HEADING(g_SpawnData.LandingGearVehicleID)
														PRINTLN("[spawning] FadeInAfterRespawn - Retrieved entity coords; Position: <<", g_SpawnData.vVehPosForLandingGear.X, ", ", g_SpawnData.vVehPosForLandingGear.Y, ", ", g_SpawnData.vVehPosForLandingGear.Z, ">>, Heading: ", g_SpawnData.fVehHeadingForLandingGear)
													ELSE
														IF NOT (MPGlobals.g_NetWarpStarted)
															
															PRINTLN("[spawning] FadeInAfterRespawn - Retrieved entity coords; Position: <<", g_SpawnData.vVehPosForLandingGear.X, ", ", g_SpawnData.vVehPosForLandingGear.Y, ", ", g_SpawnData.vVehPosForLandingGear.Z, ">>, Heading: ", g_SpawnData.fVehHeadingForLandingGear)
															
															IF IS_VECTOR_ZERO(g_SpawnData.vVehPosForLandingGear)
																ASSERTLN("Attempting to set a spawning entities position as a zero vector. Let Aidan Temple know!")
															ENDIF
															
															SET_ENTITY_COORDS(g_SpawnData.LandingGearVehicleID, g_SpawnData.vVehPosForLandingGear)
															SET_ENTITY_HEADING(g_SpawnData.LandingGearVehicleID, g_SpawnData.fVehHeadingForLandingGear)
															IF (g_SpawnData.fPitch != 0.0)
																SET_ENTITY_ROTATION(g_SpawnData.LandingGearVehicleID, <<g_SpawnData.fPitch, 0.0, g_SpawnData.fVehHeadingForLandingGear>>)
																PRINTLN("[spawning] FadeInAfterRespawn - SET_ENTITY_ROTATION with ", <<g_SpawnData.fPitch, 0.0, g_SpawnData.fVehHeadingForLandingGear>>)
															ENDIF
															NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Setting vehicle coords at = ") NET_PRINT_VECTOR(g_SpawnData.vVehPosForLandingGear) NET_NL()
														ENDIF											
													ENDIF
												ENDIF
											ENDIF
											
											g_SpawnData.iFrameCountForLandingGear += 1
											NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Waiting for g_SpawnData.iFrameCountForLandingGear, new value = ") NET_PRINT_INT(g_SpawnData.iFrameCountForLandingGear) NET_NL()
											
											RETURN(FALSE)
										ENDIF
									ENDIF
								ELSE	
									#IF IS_DEBUG_BUILD
									NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn -  IS_SCREEN_FADING_IN = ") NET_PRINT_BOOL(IS_SCREEN_FADING_IN()) NET_PRINT(", IS_SCREEN_FADING_OUT = ") NET_PRINT_BOOL(IS_SCREEN_FADING_OUT()) NET_NL()
									#ENDIF
									RETURN(FALSE)	
								ENDIF	
							ELSE
								IF bIsScreenFadingOut
									NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Fade out is happening.") NET_NL()
									RETURN(FALSE)
								ENDIF
							ENDIF
						ELSE
							NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail = TRUE ") NET_NL()						
						ENDIF
					ELSE
						IF NOT SHOULD_LOCAL_PLAYER_FADE_IN_FROM_PASSIVE_MODE_CUTSCENE()
							SET_LOCAL_PLAYER_SHOULD_FADE_IN_FROM_PASSIVE_MODE_CUTSCENE(TRUE)
						ENDIF
						
						NET_PRINT_FRAME() NET_PRINT("[spawning] [dsw] FadeInAfterRespawn - Not doing fade HAS_LOCAL_PLAYER_JUST_DONE_PASSIVE_MODE_CUTSCENE") NET_NL()
					ENDIF
				ELSE
					NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Waiting for head blend to finish, spamming FINALIZE_HEAD_BLEND") NET_NL()
					FINALIZE_HEAD_BLEND(playerID)
					RETURN(FALSE)	
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWNING, "[spawning] FadeInAfterRespawn - Player entity does not exist.")
				#ENDIF
				
				NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Player entity does not exist.") NET_NL()
				RETURN FALSE
			ENDIF
		ELSE
			NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Not doing fade g_SpawnData.bDisableFadeInAndTurnOnControls") NET_NL()
			
			IF HAS_LOCAL_PLAYER_JUST_DONE_PASSIVE_MODE_CUTSCENE()
				IF NOT SHOULD_LOCAL_PLAYER_FADE_IN_FROM_PASSIVE_MODE_CUTSCENE()
					SET_LOCAL_PLAYER_SHOULD_FADE_IN_FROM_PASSIVE_MODE_CUTSCENE(TRUE)
				ENDIF
				NET_PRINT_FRAME() NET_PRINT("[spawning] [dsw] FadeInAfterRespawn - Not doing fade, g_SpawnData.bDisableFadeInAndTurnOnControls is set, HAS_LOCAL_PLAYER_JUST_DONE_PASSIVE_MODE_CUTSCENE") NET_NL()
			ENDIF
		ENDIF

		FORCE_SKYBLUR_CLEAR_INSTANTLY()

		g_SpawnData.bIsBlurredOut = FALSE
		g_SpawnData.bIsSwoopedUp = FALSE
		g_SpawnData.bUnfreezingEarly = FALSE
		
		NET_PRINT_FRAME() NET_PRINT("[spawning] FadeInAfterRespawn - Returing TRUE.") NET_NL()
		RETURN(TRUE)
	ENDIF
ENDFUNC

FUNC BOOL IsMyCarVisible()
	VEHICLE_INDEX CarID
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_ENTITY_VISIBLE(CarID)
			RETURN(TRUE)
		ELSE
			NET_PRINT("[spawning] IsMyCarVisible - IS_ENTITY_VISIBLE = FALSE") NET_NL()	
		ENDIF
	ELSE
		NET_PRINT("[spawning] IsMyCarVisible - ped not in vehicle") NET_NL()	
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC


FUNC BOOL CAN_PLAYER_SPAWN_A_PERSONAL_VEHICLE()
	INT iSaveSlot = CURRENT_SAVED_VEHICLE_SLOT()
	
	IF (iSaveSlot > -1)
	AND NOT (g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT)
	AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
	AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
	AND NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CREATED)
	AND NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_BEING_CREATED)
	AND IsSavedVehicleSafeToProcess()
		RETURN(TRUE)
	ELSE
	
		NET_PRINT("CAN_PLAYER_SPAWN_A_PERSONAL_VEHICLE() - iSaveSlot ") NET_PRINT_INT(iSaveSlot) NET_NL()
		NET_PRINT("CAN_PLAYER_SPAWN_A_PERSONAL_VEHICLE() - (g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT) ") NET_PRINT_BOOL((g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT)) NET_NL()
		NET_PRINT("CAN_PLAYER_SPAWN_A_PERSONAL_VEHICLE() - IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) ") NET_PRINT_BOOL(IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)) NET_NL()
		NET_PRINT("CAN_PLAYER_SPAWN_A_PERSONAL_VEHICLE() - IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED) ") NET_PRINT_BOOL(IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)) NET_NL()
		NET_PRINT("CAN_PLAYER_SPAWN_A_PERSONAL_VEHICLE() - IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CREATED) ") NET_PRINT_BOOL(IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_CREATED)) NET_NL()
		NET_PRINT("CAN_PLAYER_SPAWN_A_PERSONAL_VEHICLE() - IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_BEING_CREATED) ") NET_PRINT_BOOL(IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_BEING_CREATED)) NET_NL()
		NET_PRINT("CAN_PLAYER_SPAWN_A_PERSONAL_VEHICLE() - IsSavedVehicleSafeToProcess ") NET_PRINT_BOOL(IsSavedVehicleSafeToProcess()) NET_NL()
		
	ENDIF
	
	RETURN (FALSE)
ENDFUNC

PROC RESET_SPAWN_PERSONAL_VEHILCE_DEFAULTS()
	NET_PRINT("RESET_SPAWN_PERSONAL_VEHILCE_DEFAULTS() - called") NET_NL()
	g_TransitionSpawnData.bWarpPlayerIntoSavedVehicle = TRUE
	g_TransitionSpawnData.bSwitchSavedVehicleEngineOn = FALSE
	g_TransitionSpawnData.bSpawnNearbySavedVehicleConsiderHighways = FALSE
	g_TransitionSpawnData.bSpawnNearbySavedVehicleUseRoadOffset = TRUE
	g_TransitionSpawnData.bFreezeSavedVehicle = FALSE
	g_TransitionSpawnData.iSpawnInSavedVehicleState = 0	
ENDPROC

FUNC BOOL SPAWN_PERSONAL_VEHICLE_NEAR_PLAYER(BOOL bUseRoadOffset=TRUE)


	
	//INT iSaveSlot = CURRENT_SAVED_VEHICLE_SLOT()
	
	IF CAN_PLAYER_SPAWN_A_PERSONAL_VEHICLE()
	
		IF (g_TransitionSpawnData.iSpawnInSavedVehicleState = 0)			
			g_TransitionSpawnData.bSpawnNearbySavedVehicle = TRUE
			g_TransitionSpawnData.bSpawnNearbySavedVehicleUseRoadOffset = bUseRoadOffset
			g_TransitionSpawnData.iSpawnInSavedVehicleState++
		ENDIF
		
		IF (g_TransitionSpawnData.iSpawnInSavedVehicleState = 1)
			IF NOT (g_TransitionSpawnData.bSpawnNearbySavedVehicle)
				g_TransitionSpawnData.iSpawnInSavedVehicleState = 0
				RESET_SPAWN_PERSONAL_VEHILCE_DEFAULTS()
				RETURN(TRUE)
			ENDIF
		ENDIF 
	
	ENDIF

	RETURN(FALSE)

ENDFUNC



PROC SPAWN_RANDOM_VEHICLE_NEAR_PLAYER(BOOL bUseRoadOffset=TRUE)

//	IF (g_TransitionSpawnData.iSpawnInRandomVehicleState = 0)			
		g_TransitionSpawnData.bSpawnNearbyRandomVehicle = TRUE
		g_TransitionSpawnData.bSpawnNearbyRandomVehicleUseRoadOffset = bUseRoadOffset
//		g_TransitionSpawnData.iSpawnInRandomVehicleState++
//	ENDIF
//	
//	IF (g_TransitionSpawnData.iSpawnInRandomVehicleState = 1)
//		IF NOT (g_TransitionSpawnData.bSpawnNearbyRandomVehicle)
//			g_TransitionSpawnData.iSpawnInRandomVehicleState = 0
//			RETURN(TRUE)
//		ELSE
//			NET_PRINT("[spawning] SPAWN_RANDOM_VEHICLE_NEAR_PLAYER - waiting on g_TransitionSpawnData.bSpawnNearbyRandomVehicle") NET_NL()
//		ENDIF
//	ENDIF 
//	
//	
//	RETURN(FALSE)

ENDPROC


//FUNC INT GET_ENTRY_WANTED_LEVEL()
//
//	STRUCT_STAT_DATE LeftDateDate = GET_MP_DATE_CHARACTER_STAT(MP_STAT_CHAR_LAST_PLAY_TIME)
//	STRUCT_STAT_DATE CurrentDate =  GET_CURRENT_TIMEOFDAY_AS_STAT()
//	
//	net_NL()NET_PRINT("GIVE_BACK_WANTED_LEVEL: ")
//	NET_NL()NET_PRINT("			- HAS_HOURS_PASSED_BETWEEN_DATES(4) = ")NET_PRINT_BOOL(HAS_HOURS_PASSED_BETWEEN_DATES(LeftDateDate, CurrentDate, 4))
//	NET_NL()NET_PRINT("			- GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_WANTED) = ")NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_WANTED))
//	net_NL()NET_PRINT("			-  NETWORK_IS_ACTIVITY_SESSION ") NET_PRINT_BOOL(NETWORK_IS_ACTIVITY_SESSION())
//	
//	net_NL()NET_PRINT("			-  LeftDateDate ") NET_PRINT_DATE(LeftDateDate)
//	net_NL()NET_PRINT("			-  CurrentDate ") NET_PRINT_DATE(CurrentDate)
//	net_NL()
//	
//	IF HAS_HOURS_PASSED_BETWEEN_DATES(LeftDateDate, CurrentDate, 4) = FALSE
//	#IF IS_DEBUG_BUILD
//	OR g_Debug4HourWantedCheck
//	#ENDIF	
//		RETURN(GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_WANTED))		
//	ENDIF
//	
//	RETURN(0)
//
//ENDFUNC



