
//moved to global block 1934572
ENUM PILOT_SCHOOL_DLC_CLASSES_ENUM_NEW
	PSCD_DLC_OutsideLoop=0,
	PSCD_DLC_Engine_failure,
	PSCD_DLC_ChaseParachute,
	PSCD_DLC_CityLanding,
	PSCD_DLC_VehicleLanding,
	PSCD_DLC_Formation,
	PSCD_DLC_ShootingRange,
	PSCD_DLC_FlyLow,
	PSCD_DLC_CollectFlags,
	PSCD_DLC_FollowLeader
ENDENUM
//EOF
