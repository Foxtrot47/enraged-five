///// net_realty_multistorey_garage.sch
///// Online Technical: Mark Richardson
///// MP multistorey garage Functionality - 17/08/2022

#IF FEATURE_DLC_2_2022
USING "net_include.sch"
USING "MP_globals_multistorey_garage_consts.sch"

FUNC BOOL IS_MULTISTOREY_GARAGE_ID_VALID(MULTISTOREY_GARAGE_ID eGarageID)
	INT iPropertyID = ENUM_TO_INT(eGarageID)
	
	IF iPropertyID <= ENUM_TO_INT(MULTISTOREY_GARAGE_ID_INVALID)
	OR iPropertyID >= ENUM_TO_INT(MULTISTOREY_GARAGE_ID_COUNT)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC MP_INT_STATS MULTISTOREY_GARAGE_GET_FLOOR_STYLE_STAT(INT iFloor)
	SWITCH iFloor
		CASE MULTISTOREY_GARAGE_FLOOR_1 RETURN MP_STAT_MULTSTOREY_GAR_STY_F1	BREAK
		CASE MULTISTOREY_GARAGE_FLOOR_2 RETURN MP_STAT_MULTSTOREY_GAR_STY_F2	BREAK
		CASE MULTISTOREY_GARAGE_FLOOR_3 RETURN MP_STAT_MULTSTOREY_GAR_STY_F3	BREAK
		CASE MULTISTOREY_GARAGE_FLOOR_4 RETURN MP_STAT_MULTSTOREY_GAR_STY_F4	BREAK
		CASE MULTISTOREY_GARAGE_FLOOR_5 RETURN MP_STAT_MULTSTOREY_GAR_STY_F5	BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("MULTISTOREY_GARAGE_GET_FLOOR_STYLE_STAT - Invald floor")
	RETURN MP_STAT_MULTSTOREY_GAR_STY_F1
ENDFUNC

FUNC MP_INT_STATS MULTISTOREY_GARAGE_GET_FLOOR_TINT_STAT(INT iFloor)
	SWITCH iFloor
		CASE MULTISTOREY_GARAGE_FLOOR_1 RETURN MP_STAT_MULTSTOREY_GAR_TINT_F1	BREAK
		CASE MULTISTOREY_GARAGE_FLOOR_2 RETURN MP_STAT_MULTSTOREY_GAR_TINT_F2	BREAK
		CASE MULTISTOREY_GARAGE_FLOOR_3 RETURN MP_STAT_MULTSTOREY_GAR_TINT_F3	BREAK
		CASE MULTISTOREY_GARAGE_FLOOR_4 RETURN MP_STAT_MULTSTOREY_GAR_TINT_F4	BREAK
		CASE MULTISTOREY_GARAGE_FLOOR_5 RETURN MP_STAT_MULTSTOREY_GAR_TINT_F5	BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("MULTISTOREY_GARAGE_GET_FLOOR_TINT_STAT - Invald floor")
	RETURN MP_STAT_MULTSTOREY_GAR_TINT_F1
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ ENTITY SETS ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC VECTOR GET_MULTISTOREY_GARAGE_INTERIOR_POSITION()
	RETURN <<520.0, -2625.0, -50.0>>
ENDFUNC

FUNC STRING GET_MULTISTOREY_GARAGE_INTERIOR_NAME()
	RETURN "xm3_dlc_int_02_xm3"
ENDFUNC

FUNC MULTISTOREY_GARAGE_ENTITY_SET_TYPE GET_MULTISTOREY_GARAGE_ENTITY_SET_TYPE(MULTISTOREY_GARAGE_ENTITY_SET_ID eGarageSetID)
	
	SWITCH eGarageSetID
		CASE MULTISTOREY_GARAGE_SET_FLOOR_01
		CASE MULTISTOREY_GARAGE_SET_FLOOR_02
		CASE MULTISTOREY_GARAGE_SET_FLOOR_03
		CASE MULTISTOREY_GARAGE_SET_FLOOR_04
		CASE MULTISTOREY_GARAGE_SET_FLOOR_05
			RETURN MULTISTOREY_GARAGE_SET_TYPE_FLOOR
		BREAK
		CASE MULTISTOREY_GARAGE_SET_STYLE_1
		CASE MULTISTOREY_GARAGE_SET_STYLE_2
		CASE MULTISTOREY_GARAGE_SET_STYLE_3
			RETURN MULTISTOREY_GARAGE_SET_TYPE_STYLE
		BREAK
		CASE MULTISTOREY_GARAGE_SET_TINT
			RETURN MULTISTOREY_GARAGE_SET_TYPE_TINT
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	SCRIPT_ASSERT("GET_MULTISTOREY_GARAGE_ENTITY_SET_TYPE - Unknown entity set supplied.")
	#ENDIF
	
	RETURN MULTISTOREY_GARAGE_SET_TYPE_INVALID
ENDFUNC

FUNC MULTISTOREY_GARAGE_ENTITY_SET_ID GET_MULTISTOREY_GARAGE_FLOOR_ENTITY_SET(INT iGarageFloor)
	SWITCH iGarageFloor
		CASE 0	RETURN	MULTISTOREY_GARAGE_SET_FLOOR_01
		CASE 1	RETURN	MULTISTOREY_GARAGE_SET_FLOOR_02
		CASE 2	RETURN	MULTISTOREY_GARAGE_SET_FLOOR_03
		CASE 3	RETURN	MULTISTOREY_GARAGE_SET_FLOOR_04
		CASE 4	RETURN	MULTISTOREY_GARAGE_SET_FLOOR_05
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "GET_MULTISTOREY_GARAGE_FLOOR_ENTITY_SET - Invalid floor: ", iGarageFloor)
	RETURN MULTISTOREY_GARAGE_SET_FLOOR_01
ENDFUNC

FUNC STRING GET_MULTISTOREY_GARAGE_ENTITY_SET_NAME(MULTISTOREY_GARAGE_ENTITY_SET_ID eGarageSetID)
	SWITCH eGarageSetID
		CASE MULTISTOREY_GARAGE_SET_FLOOR_01	RETURN "entity_set_numbers_01"
		CASE MULTISTOREY_GARAGE_SET_FLOOR_02	RETURN "entity_set_numbers_02"
		CASE MULTISTOREY_GARAGE_SET_FLOOR_03	RETURN "entity_set_numbers_03"
		CASE MULTISTOREY_GARAGE_SET_FLOOR_04	RETURN "entity_set_numbers_04"
		CASE MULTISTOREY_GARAGE_SET_FLOOR_05	RETURN "entity_set_numbers_05"
		CASE MULTISTOREY_GARAGE_SET_STYLE_1		RETURN "entity_set_shell_01"
		CASE MULTISTOREY_GARAGE_SET_STYLE_2		RETURN "entity_set_shell_02"
		CASE MULTISTOREY_GARAGE_SET_STYLE_3		RETURN "entity_set_shell_03"
		CASE MULTISTOREY_GARAGE_SET_TINT		RETURN "entity_set_tint_01"
	ENDSWITCH									
	
	#IF IS_DEBUG_BUILD
	SCRIPT_ASSERT("GET_MULTISTOREY_GARAGE_ENTITY_SET - Unknown entity set requested.")
	#ENDIF
	
	RETURN ""
ENDFUNC

FUNC INT MULTISTOREY_GARAGE_GET_COUNT_OF_TINTS()
	RETURN ENUM_TO_INT(MULTISTOREY_GARAGE_TINT_COUNT)
ENDFUNC

FUNC INT MULTISTOREY_GARAGE_GET_COUNT_OF_STYLES()
	INT i, iNumStyles
	
	FOR i = MULTISTOREY_GARAGE_SET_STYLE_1 TO (ENUM_TO_INT(MULTISTOREY_GARAGE_SET_COUNT) - 1)
		MULTISTOREY_GARAGE_ENTITY_SET_TYPE eType = GET_MULTISTOREY_GARAGE_ENTITY_SET_TYPE(INT_TO_ENUM(MULTISTOREY_GARAGE_ENTITY_SET_ID, i))
		
		IF eType = MULTISTOREY_GARAGE_SET_TYPE_STYLE
			iNumStyles ++
		ELSE
			BREAKLOOP
		ENDIF
	ENDFOR
	
	RETURN iNumStyles
ENDFUNC

FUNC INT GET_PLAYERS_MULTISTOREY_GARAGE_FLOOR_STYLE(PLAYER_INDEX pOwner, INT iGarageFloor)
	IF pOwner = INVALID_PLAYER_INDEX()
		RETURN 0
	ENDIF
	
	INT iNumStyles = MULTISTOREY_GARAGE_GET_COUNT_OF_STYLES()
	INT iBSStart = iGarageFloor * iNumStyles
	INT i
			
	REPEAT iNumStyles i
		INT iBit = iBSStart + i		
		
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].propertyDetails.sMultiStoreyGarageData.iFloorStyleBS, iBit)
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN 0
ENDFUNC

FUNC INT GET_PLAYERS_MULTISTOREY_GARAGE_FLOOR_TINT(PLAYER_INDEX pOwner, INT iGarageFloor, BOOl bCheckPreview = FALSE)
	
	IF pOwner = INVALID_PLAYER_INDEX()
		RETURN 0
	ENDIF
	
	IF bCheckPreview
	AND IS_LOCAL_PLAYER_PREVIEWING_SIMPLE_INTERIOR_TINT()
		RETURN GET_SIMPLE_INTERIOR_CURRENT_PREVIEW_TINT()
	ENDIF
	
	INT i
	INT iNumTints = MULTISTOREY_GARAGE_GET_COUNT_OF_TINTS()
	INT iBSStart = iGarageFloor * iNumTints
			
	REPEAT iNumTints i
		INT iBit 	= (iBSStart + i) % 32
		INT iBS 	= (iBSStart + i) / 32
		
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].propertyDetails.sMultiStoreyGarageData.iFloorTintBS[iBS], iBit)
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN 0
ENDFUNC

FUNC MULTISTOREY_GARAGE_ENTITY_SET_ID GET_PLAYERS_MULTISTOREY_GARAGE_ENTITY_SET_FOR_FLOOR(PLAYER_INDEX pOwner, INT iGarageFloor, MULTISTOREY_GARAGE_ENTITY_SET_TYPE eEntitySetType)
	SWITCH eEntitySetType
		CASE MULTISTOREY_GARAGE_SET_TYPE_STYLE
			INT iStyleSet
			
			iStyleSet = ENUM_TO_INT(MULTISTOREY_GARAGE_SET_STYLE_1)
			
			IF IS_LOCAL_PLAYER_PREVIEWING_SIMPLE_INTERIOR_STYLE()
				iStyleSet += GET_SIMPLE_INTERIOR_CURRENT_PREVIEW_STYLE()
			ELSE				
				iStyleSet += GET_PLAYERS_MULTISTOREY_GARAGE_FLOOR_STYLE(pOwner, iGarageFloor)
			ENDIF
			
			RETURN INT_TO_ENUM(MULTISTOREY_GARAGE_ENTITY_SET_ID, iStyleSet)
		BREAK
		CASE MULTISTOREY_GARAGE_SET_TYPE_FLOOR
			RETURN GET_MULTISTOREY_GARAGE_FLOOR_ENTITY_SET(iGarageFloor)
		BREAK
	ENDSWITCH
	
	RETURN MULTISTOREY_GARAGE_SET_INVALID
ENDFUNC

PROC REMOVE_MULTISTOREY_GARAGE_ENTITY_SET(STRING sEntitySet)
	INTERIOR_INSTANCE_INDEX iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_MULTISTOREY_GARAGE_INTERIOR_POSITION(), GET_MULTISTOREY_GARAGE_INTERIOR_NAME())
	
	IF IS_VALID_INTERIOR(iInteriorID)
		IF NOT IS_STRING_NULL_OR_EMPTY(sEntitySet)
			IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorID, sEntitySet)
				DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorID, sEntitySet)
				PRINTLN("REMOVE_MULTISTOREY_GARAGE_ENTITY_SET - Deactivating: '", sEntitySet, "'")
			ELSE
				PRINTLN("REMOVE_MULTISTOREY_GARAGE_ENTITY_SET - Unable to deactivate entity set: ", sEntitySet, " as it's not currently active.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_MULTISTOREY_GARAGE_ENTITY_SETS(MULTISTOREY_GARAGE_ID eGarageID)
	
	IF eGarageID <> MULTISTOREY_GARAGE_ID_INVALID
		
		STRING sEntitySet
		
		INT i
		REPEAT MULTISTOREY_GARAGE_SET_COUNT i
			sEntitySet = GET_MULTISTOREY_GARAGE_ENTITY_SET_NAME(INT_TO_ENUM(MULTISTOREY_GARAGE_ENTITY_SET_ID, i))
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sEntitySet)
				REMOVE_SIMPLE_INTERIOR_ENTITY_SET(GET_SIMPLE_INTERIOR_ID_FROM_MULTISTOREY_GARAGE_ID(eGarageID), sEntitySet)
			ELSE
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "REMOVE_MULTISTOREY_GARAGE_ENTITY_SETS - Entity set name is null or empty.")
				#ENDIF
			ENDIF	
		ENDREPEAT
	ENDIF
ENDPROC

PROC REMOVE_MULTISTOREY_GARAGE_ENTITY_SETS_BY_TYPE(MULTISTOREY_GARAGE_ENTITY_SET_TYPE eGarageSetTypeID)
	
	IF eGarageSetTypeID <> MULTISTOREY_GARAGE_SET_TYPE_INVALID
		
		INTERIOR_INSTANCE_INDEX iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_MULTISTOREY_GARAGE_INTERIOR_POSITION(), GET_MULTISTOREY_GARAGE_INTERIOR_NAME())

		IF IS_VALID_INTERIOR(iInteriorID)
			
			STRING sEntitySet			
			INT i
			
			REPEAT MULTISTOREY_GARAGE_SET_COUNT i
				
				IF eGarageSetTypeID = GET_MULTISTOREY_GARAGE_ENTITY_SET_TYPE(INT_TO_ENUM(MULTISTOREY_GARAGE_ENTITY_SET_ID, i))
				
					sEntitySet = GET_MULTISTOREY_GARAGE_ENTITY_SET_NAME(INT_TO_ENUM(MULTISTOREY_GARAGE_ENTITY_SET_ID, i))
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sEntitySet)
						IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorID, sEntitySet)
							DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorID, sEntitySet)
							#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_PROPERTY, "REMOVE_MULTISTOREY_GARAGE_ENTITY_SETS_BY_TYPE - Deactivating: '", sEntitySet, "'")	
							#ENDIF
						ELSE
							CDEBUG1LN(DEBUG_PROPERTY, "REMOVE_MULTISTOREY_GARAGE_ENTITY_SETS_BY_TYPE - Set already inactive: '", sEntitySet, "'")	
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_PROPERTY, "REMOVE_MULTISTOREY_GARAGE_ENTITY_SETS_BY_TYPE - Entity set name is null or empty.")
						#ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_MULTISTOREY_GARAGE_ENTITY_SET_INACTIVE(MULTISTOREY_GARAGE_ENTITY_SET_ID eGarageSetID) 
	
	IF eGarageSetID <> MULTISTOREY_GARAGE_SET_INVALID
	
		INTERIOR_INSTANCE_INDEX iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_MULTISTOREY_GARAGE_INTERIOR_POSITION(), GET_MULTISTOREY_GARAGE_INTERIOR_NAME())
		
		IF IS_VALID_INTERIOR(iInteriorID)
			
			STRING sEntitySet = GET_MULTISTOREY_GARAGE_ENTITY_SET_NAME(eGarageSetID)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sEntitySet)
				IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorID, sEntitySet)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "IS_MULTISTOREY_GARAGE_ENTITY_SET_INACTIVE - Mismatch for entity set: '", sEntitySet, "'")
					#ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "IS_MULTISTOREY_GARAGE_ENTITY_SET_INACTIVE - Invalid entity set.")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL IS_MULTISTOREY_GARAGE_ENTITY_SET_ACTIVE(MULTISTOREY_GARAGE_ENTITY_SET_ID eGarageSetID) 
	
	IF eGarageSetID <> MULTISTOREY_GARAGE_SET_INVALID
	
		INTERIOR_INSTANCE_INDEX iInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_MULTISTOREY_GARAGE_INTERIOR_POSITION(), GET_MULTISTOREY_GARAGE_INTERIOR_NAME())
		
		IF IS_VALID_INTERIOR(iInteriorID)
			
			STRING sEntitySet = GET_MULTISTOREY_GARAGE_ENTITY_SET_NAME(eGarageSetID)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sEntitySet)
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorID, sEntitySet)
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "IS_ENTITY_SET_ACTIVE - Entity set active: '", sEntitySet, "'")
					#ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "IS_ENTITY_SET_ACTIVE - Invalid entity set.")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_DEFAULT_MULTISTOREY_GARAGE_ENTITY_SETS(MULTISTOREY_GARAGE_ID eGarageID, SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX pOwner, INT iGarageFloor)

	IF pOwner <> INVALID_PLAYER_INDEX()
		
		IF eGarageID <> MULTISTOREY_GARAGE_ID_INVALID AND eSimpleInteriorID <> SIMPLE_INTERIOR_INVALID			
			
			MULTISTOREY_GARAGE_ENTITY_SET_ID eEntitySet
			PRINTLN("SET_DEFAULT_MULTISTOREY_GARAGE_ENTITY_SETS - Floor ", iGarageFloor)
			
			//Remove any entity sets
			REMOVE_MULTISTOREY_GARAGE_ENTITY_SETS_BY_TYPE(MULTISTOREY_GARAGE_SET_TYPE_FLOOR)
			REMOVE_MULTISTOREY_GARAGE_ENTITY_SETS_BY_TYPE(MULTISTOREY_GARAGE_SET_TYPE_STYLE)
			REMOVE_MULTISTOREY_GARAGE_ENTITY_SETS_BY_TYPE(MULTISTOREY_GARAGE_SET_TYPE_TINT)
			
			//Add the floor (level wall signs)
			eEntitySet = GET_MULTISTOREY_GARAGE_FLOOR_ENTITY_SET(iGarageFloor)
			ADD_SIMPLE_INTERIOR_ENTITY_SET(eSimpleInteriorID, GET_MULTISTOREY_GARAGE_ENTITY_SET_NAME(eEntitySet))
			
			//Add the purchased style for this floor
			eEntitySet = GET_PLAYERS_MULTISTOREY_GARAGE_ENTITY_SET_FOR_FLOOR(pOwner, iGarageFloor, MULTISTOREY_GARAGE_SET_TYPE_STYLE)			
			PRINTLN("SET_DEFAULT_MULTISTOREY_GARAGE_ENTITY_SETS - Attempting to add set: ", eEntitySet)
			ADD_SIMPLE_INTERIOR_ENTITY_SET(eSimpleInteriorID, GET_MULTISTOREY_GARAGE_ENTITY_SET_NAME(eEntitySet))
			
			//Add the purchsed tint
			INT iTint = GET_PLAYERS_MULTISTOREY_GARAGE_FLOOR_TINT(pOwner, iGarageFloor, TRUE)
			STRING sEntitySetName = GET_MULTISTOREY_GARAGE_ENTITY_SET_NAME(MULTISTOREY_GARAGE_SET_TINT)
			PRINTLN("SET_DEFAULT_MULTISTOREY_GARAGE_ENTITY_SETS - Attempting to add set: ", MULTISTOREY_GARAGE_SET_TINT, " with tint: ", iTint)
			ADD_SIMPLE_INTERIOR_ENTITY_SET(eSimpleInteriorID, sEntitySetName)
			ADD_SIMPLE_INTERIOR_ENTITY_SET_TINT(eSimpleInteriorID, sEntitySetName, iTint)
		ENDIF
	ELSE
		CASSERTLN(DEBUG_PROPERTY, "SET_DEFAULT_ENTITY_SETS - Invalid player index for Multistorey Garage owner.")
		DEBUG_PRINTCALLSTACK()
		EXIT
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ OWNERSHIP BD ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC CLEAR_MULTISTOREY_GARAGE_TINT_BD()
	INT i
	INT iBitSets = (MULTISTOREY_GARAGE_GET_COUNT_OF_TINTS() * MULTISTOREY_GARAGE_FLOOR_COUNT) / 32
	
	FOR i = 0 TO iBitSets
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.iFloorTintBS[i] 	= 0
	ENDFOR
ENDPROC

PROC CLEAR_MULTISTOREY_GARAGE_TINT_BD_FOOR_FLOOR(INT iGarageFloor)
	INT i	
	INT iNumTints = MULTISTOREY_GARAGE_GET_COUNT_OF_TINTS()
	INT iBSStart = iGarageFloor * iNumTints
	
	REPEAT iNumTints i
		INT iBit 	= (iBSStart + i) % 32
		INT iBS 	= (iBSStart + i) / 32
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.iFloorTintBS[iBS], iBit)
	ENDREPEAT
ENDPROC

PROC SET_MULTISTOREY_GARAGE_FLOOR_TINT(INT iGarageFloor, INT iTint)
	
	//Set the stat
	SET_MP_INT_CHARACTER_STAT(MULTISTOREY_GARAGE_GET_FLOOR_TINT_STAT(iGarageFloor), iTint)
	
	//Set the broadcast data
	INT iNumTints = MULTISTOREY_GARAGE_GET_COUNT_OF_TINTS()
	INT iBSStart = iGarageFloor * iNumTints
	
	CLEAR_MULTISTOREY_GARAGE_TINT_BD_FOOR_FLOOR(iGarageFloor)
	
	INT iBit 	= (iBSStart + iTint) % 32
	INT iBS 	= (iBSStart + iTint) / 32

	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.iFloorTintBS[iBS], iBit)
	
	PRINTLN("SET_MULTISTOREY_GARAGE_FLOOR_TINT - Set tint: ", iTint, " for floor: ", iGarageFloor, " owned. BS: ", iBS, " iBit: ", iBit, " num tints: ", iNumTints)
ENDPROC

PROC SET_MULTISTOREY_GARAGE_FLOOR_STYLE(INT iGarageFloor, INT iStyle)
	
	//Set the stat
	SET_MP_INT_CHARACTER_STAT(MULTISTOREY_GARAGE_GET_FLOOR_STYLE_STAT(iGarageFloor), iStyle)
	
	//Set the broadcast data
	INT iNumStyles = MULTISTOREY_GARAGE_GET_COUNT_OF_STYLES()
	INT iBSStart = iGarageFloor * iNumStyles
	INT i
	
	REPEAT iNumStyles i
		INT iBit = iBSStart + i
		
		IF iStyle = i
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.iFloorStyleBS, iBit)
		ELSE
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.iFloorStyleBS, iBit)
		ENDIF
	ENDREPEAT
ENDPROC

PROC SET_MULTISTOREY_GARAGE_AS_OWNED(MULTISTOREY_GARAGE_ID eGarageID)
	IF IS_MULTISTOREY_GARAGE_ID_VALID(eGarageID)
		PRINTLN("SET_MULTISTOREY_GARAGE_AS_OWNED - Setting player owns Multistorey Garage: ", eGarageID)
		
		//SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MULTISTOREY_GARAGE,PROPERTY_MULTISTOREY_GARAGE)   
		//SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_SECURITY_OFFICE_GAR,PROPERTY_SECURITY_OFFICE_GARAGE)  
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTSTOREY_GAR_OWNED, ENUM_TO_INT(eGarageID))			
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.ePropertyID = eGarageID
		
		INT i
		
		REPEAT MULTISTOREY_GARAGE_FLOOR_COUNT i
			SET_MULTISTOREY_GARAGE_FLOOR_TINT(i, 0)
			SET_MULTISTOREY_GARAGE_FLOOR_STYLE(i, 0)
		ENDREPEAT
	ELSE
		PRINTLN("SET_MULTISTOREY_GARAGE_AS_OWNED - Multistorey Garage ID is invalid")
	ENDIF
ENDPROC

PROC BROADCAST_PLAYER_PURCHASED_MULTISTOREY_GARAGE()
	//Clear out old data
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.iFloorStyleBS 	= 0
	CLEAR_MULTISTOREY_GARAGE_TINT_BD()

	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.ePropertyID = INT_TO_ENUM(MULTISTOREY_GARAGE_ID, GET_MP_INT_CHARACTER_STAT(MP_STAT_MULTSTOREY_GAR_OWNED))
	
	INT i, iNumStyles, iNumTints, iBS
	
	iNumStyles	= MULTISTOREY_GARAGE_GET_COUNT_OF_STYLES()
	iNumTints 	= MULTISTOREY_GARAGE_GET_COUNT_OF_TINTS()
	PRINTLN("BROADCAST_PLAYER_PURCHASED_MULTISTOREY_GARAGE - Max styles available: ", iNumStyles, " Max tints available: ", iNumTints)
	
	//Set the owned customizations for each floor
	REPEAT MULTISTOREY_GARAGE_FLOOR_COUNT i
		INT iFloorOffset, iBit
		
		//Style
		iFloorOffset = i * iNumStyles
		iBit = iFloorOffset + GET_MP_INT_CHARACTER_STAT(MULTISTOREY_GARAGE_GET_FLOOR_STYLE_STAT(i))
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.iFloorStyleBS, iBit)
		PRINTLN("BROADCAST_PLAYER_PURCHASED_MULTISTOREY_GARAGE - Player has style : ", GET_MP_INT_CHARACTER_STAT(MULTISTOREY_GARAGE_GET_FLOOR_STYLE_STAT(i)), " for floor: ", i, " bit: ", iBit)
		
		//Tint
		INT iTint = GET_MP_INT_CHARACTER_STAT(MULTISTOREY_GARAGE_GET_FLOOR_TINT_STAT(i))
		iFloorOffset = i * iNumTints
		iBit = (iFloorOffset + iTint) % 32
		iBS = ((iFloorOffset + iTint) / 32)
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.iFloorTintBS[iBS], iBit)
		PRINTLN("BROADCAST_PLAYER_PURCHASED_MULTISTOREY_GARAGE - Player has tint : ", GET_MP_INT_CHARACTER_STAT(MULTISTOREY_GARAGE_GET_FLOOR_TINT_STAT(i)), " for floor: ", i, " bit: ", iBit, " iBS: ", iBS)
		
	ENDREPEAT
ENDPROC

PROC ALIGN_MULTISTOREY_GARAGE_BROADCAST_DATA()
	
	BROADCAST_PLAYER_PURCHASED_MULTISTOREY_GARAGE()
	
	#IF IS_DEBUG_BUILD
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.ePropertyID != MULTISTOREY_GARAGE_ID_INVALID
		PRINTLN("[PROPERTY_OWNERSHIP] Player owns Multistorey Garage")
	ENDIF
	#ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC REMOVE_MULTISTOREY_GARAGE(MULTISTOREY_GARAGE_ID eGarageID)
	IF IS_MULTISTOREY_GARAGE_ID_VALID(eGarageID)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.ePropertyID = eGarageID 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTSTOREY_GAR_OWNED, ENUM_TO_INT(MULTISTOREY_GARAGE_ID_INVALID))
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sMultiStoreyGarageData.ePropertyID = MULTISTOREY_GARAGE_ID_INVALID
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[MULTISTOREY_GARAGE] REMOVE_MULTISTOREY_GARAGE: ", eGarageID)
		ENDIF
	ENDIF
ENDPROC

PROC DEBUG_REMOVE_MULTISTOREY_GARAGE()
	IF DOES_PLAYER_OWN_MULTISTOREY_GARAGE(PLAYER_ID())
		REMOVE_MULTISTOREY_GARAGE(GET_PLAYERS_OWNED_MULTISTOREY_GARAGE(PLAYER_ID()))
		
		CDEBUG1LN(DEBUG_PROPERTY, "DEBUG_REMOVE_MULTISTOREY_GARAGE - Local player is no longer owner of a multistorey garage.")
	ENDIF
ENDPROC
#ENDIF


// ----------------------------------------------
// -------- Super Fun Website Functions ---------
// ----------------------------------------------

/// PURPOSE:
///    Returns the name of the stat associated with the Multistorey Garage Upgrade.
/// PARAMS:
///    eUpgrade - Upgrade enum
FUNC STRING GET_MULTISTOREY_GARAGE_STAT_NAME_FROM_ID(MULTISTOREY_GARAGE_UPGRADE_ID eUpgrade)
	SWITCH eUpgrade
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F1					RETURN "MP_STAT_MULTSTOREY_GAR_STY_F1"
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F2					RETURN "MP_STAT_MULTSTOREY_GAR_STY_F2"
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F3					RETURN "MP_STAT_MULTSTOREY_GAR_STY_F3"
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F4					RETURN "MP_STAT_MULTSTOREY_GAR_STY_F4"
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F5					RETURN "MP_STAT_MULTSTOREY_GAR_STY_F5"
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F1						RETURN "MP_STAT_MULTSTOREY_GAR_TINT_F1"
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F2						RETURN "MP_STAT_MULTSTOREY_GAR_TINT_F2"
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F3						RETURN "MP_STAT_MULTSTOREY_GAR_TINT_F3"
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F4						RETURN "MP_STAT_MULTSTOREY_GAR_TINT_F4"
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F5						RETURN "MP_STAT_MULTSTOREY_GAR_TINT_F5"
	ENDSWITCH
	
	CASSERTLN(DEBUG_SHOPS,"GET_MP_INT_STAT_FOR_MULTISTOREY_GARAGE_UPGRADE - Upgrade enum ", eUpgrade, " missing.")
	RETURN INT_TO_ENUM(MP_INT_STATS, 0)
ENDFUNC

/// PURPOSE:
///    Returns the label of a Multistorey Garage upgrade, usually used for mod names in catalogue.
/// PARAMS:
///    eUpgrade - Upgrade enum
///    iModIndex - Mod Index
FUNC STRING GET_MULTISTOREY_GARAGE_UPGRADE_NAME_FROM_ID(MULTISTOREY_GARAGE_UPGRADE_ID eUpgrade, INT iModIndex)
	TEXT_LABEL_15 tl15Str  = "MULTISTOREY_GARAGE_"
	
	SWITCH eUpgrade
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F1
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F2
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F3
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F4
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F5
			tl15Str += "STY_"	tl15Str += (iModIndex + 1)
		BREAK
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F1
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F2
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F3
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F4
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F5
			tl15Str += "TNT_"	tl15Str += (iModIndex + 1)
		BREAK
		DEFAULT	RETURN ""
	ENDSWITCH
	
	RETURN GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl15Str, 0, GET_LENGTH_OF_LITERAL_STRING(tl15Str))
ENDFUNC

/// PURPOSE:
///    Returns the name of the Multistorey Garage property.
/// PARAMS:
///    eGarageID - Multistorey Garage ID  
FUNC STRING GET_MULTISTOREY_GARAGE_NAME_FROM_ID(MULTISTOREY_GARAGE_ID eGarageID)
	SWITCH eGarageID
		CASE WEST_VINEWOOD_MULTISTOREY_GARAGE		RETURN "MSG_NME_1"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Generates a unique item key for the Multistorey Garage, for the catalogue. 
/// PARAMS:
///    tlGeneratedKey - Reference - generated key
///    eGarageID - Multistorey Garage ID
PROC GENERATE_MULTISTOREY_GARAGE_KEY_FOR_CATALOGUE(TEXT_LABEL_63 &tlGeneratedKey, MULTISTOREY_GARAGE_ID eGarageID)
	tlGeneratedKey = "MULTISTOREY_GARAGE_INDEX_"
	tlGeneratedKey += ENUM_TO_INT(eGarageID)
	tlGeneratedKey += "_t0_v0"
ENDPROC

/// PURPOSE:
///    Generates a key for the Multistorey Garage upgrade, obvs. 
/// PARAMS:
///    tlGeneratedKey - Reference - generated key.
///    eUpgrade - Upgrade enum ( E.g. MULTISTOREY_GARAGE_UPGRADE_ART)
///    iUpgradeIndex - Upgrade index
///    iVariation - Variation
PROC GENERATE_MULTISTOREY_GARAGE_UPGRADE_KEY_FOR_CATALOGUE(TEXT_LABEL_63 &tlGeneratedKey, MULTISTOREY_GARAGE_UPGRADE_ID eUpgrade, INT iUpgradeIndex = 0, INT iVariation = 0)
	tlGeneratedKey = GET_MULTISTOREY_GARAGE_UPGRADE_NAME_FROM_ID(eUpgrade, iUpgradeIndex)
	tlGeneratedKey += "_t0_v"
	tlGeneratedKey += iVariation
ENDPROC

/// PURPOSE:
///    Generates an inventory key for the Multistorey Garage upgrade, obvs. 
/// PARAMS:
///    tlGeneratedKey - reference - Generated Key
///    eUpgrade - Upgrade enum
PROC GENERATE_MULTISTOREY_GARAGE_UPGRADE_INVENTORY_KEY_FOR_CATALOGUE(TEXT_LABEL_63 &tlGeneratedKey, MULTISTOREY_GARAGE_UPGRADE_ID eUpgrade)
	tlGeneratedKey = GET_MULTISTOREY_GARAGE_STAT_NAME_FROM_ID(eUpgrade)
	tlGeneratedKey += "_v0"
ENDPROC

/// PURPOSE:
///    Uses GENERATE_MULTISTOREY_GARAGE_KEY_FOR_CATALOGUE to return a hash of the generated key.
/// PARAMS:
///    eGarageID - Multistorey Garage ID Enum 
FUNC INT GET_MULTISTOREY_GARAGE_KEY_FOR_CATALOGUE(MULTISTOREY_GARAGE_ID eGarageID)
	TEXT_LABEL_63 tlCategoryKey
	GENERATE_MULTISTOREY_GARAGE_KEY_FOR_CATALOGUE(tlCategoryKey, eGarageID)
	PRINTLN("[GET_MULTISTOREY_GARAGE_KEY_FOR_CATALOGUE] key '", tlCategoryKey, "' = ", GET_HASH_KEY(tlCategoryKey))
	RETURN GET_HASH_KEY(tlCategoryKey)
ENDFUNC

/// PURPOSE:
///    Generates the inventory key for the Multistorey Garage.
/// PARAMS:
///    tlGeneratedKey - Referanced - Generated key.
PROC GENERATE_MULTISTOREY_GARAGE_INVENTORY_KEY_FOR_CATALOGUE(TEXT_LABEL_63 &ref_tlGeneratedKey)
	ref_tlGeneratedKey = "MP_STAT_MULTSTOREY_GAR_OWNED_v0"
ENDPROC

/// PURPOSE:
///    Uses GENERATE_MULTISTOREY_GARAGE_INVENTORY_KEY_FOR_CATALOGUE to return the hashed generated key.
FUNC INT GET_MULTISTOREY_GARAGE_INVENTORY_KEY_FOR_CATALOGUE()
	TEXT_LABEL_63 tlGeneratedKey
	GENERATE_MULTISTOREY_GARAGE_INVENTORY_KEY_FOR_CATALOGUE(tlGeneratedKey)
	
	IF IS_STRING_NULL_OR_EMPTY(tlGeneratedKey)
		#IF IS_DEBUG_BUILD
		PRINTLN("GET_MULTISTOREY_GARAGE_INVENTORY_KEY_FOR_CATALOGUE - Invalid property mod specified")
		CASSERTLN(DEBUG_SHOPS, "GET_MULTISTOREY_GARAGE_INVENTORY_KEY_FOR_CATALOGUE - Invalid property mod slot specified")
		#ENDIF
		
		RETURN -1
	ENDIF
	
	RETURN GET_HASH_KEY(tlGeneratedKey)
ENDFUNC

/// PURPOSE:
///    Returns the sale cost of the provded Multistorey Garage Upgrade. This usually gets changed by live ops for promotions etc. 
/// PARAMS:
///    eUpgrade - Upgrade Enum
///    eGarageID - Garage ID Enum
///    iUpgradeIndex - Upgrade Index
FUNC INT GET_MULTISTOREY_GARAGE_UPGRADE_COST(MULTISTOREY_GARAGE_UPGRADE_ID eUpgrade, MULTISTOREY_GARAGE_ID eGarageID, INT iUpgradeIndex = -1)
	IF (NETWORK_IS_GAME_IN_PROGRESS() AND USE_SERVER_TRANSACTIONS())
	#IF IS_DEBUG_BUILD
	AND NOT g_bAddMenuItemsToCatalogue AND NOT g_bVerifyMenuItemPrices
	#ENDIF
		INT iVariation = 0
		TEXT_LABEL_63 tlGeneratedKey
		
		GENERATE_MULTISTOREY_GARAGE_UPGRADE_KEY_FOR_CATALOGUE(tlGeneratedKey, eUpgrade, iUpgradeIndex, iVariation)
		
		INT iGeneratedKey = GET_HASH_KEY(tlGeneratedKey)
		
		IF NET_GAMESERVER_CATALOG_ITEM_KEY_IS_VALID(iGeneratedKey)
			INT iCataloguePrice = NET_GAMESERVER_GET_PRICE(iGeneratedKey, CATEGORY_PROPERTIE, 1)
			
			TEXT_LABEL_15 tl15MultistoreyGarageName = GET_MULTISTOREY_GARAGE_NAME_FROM_ID(eGarageID)
			PRINTLN("GET_MULTISTOREY_GARAGE_UPGRADE_COST: Catalogue price for ", tl15MultistoreyGarageName, " ", tlGeneratedKey, " is $", iCataloguePrice)
			
			RETURN iCataloguePrice
		ELSE
			TEXT_LABEL_15 tl15MultistoreyGarageName = GET_MULTISTOREY_GARAGE_NAME_FROM_ID(eGarageID)
			PRINTLN("GET_MULTISTOREY_GARAGE_UPGRADE_COST: Invalid key, missing catalog item for ", tl15MultistoreyGarageName, " ", tlGeneratedKey, " [hash:", iGeneratedKey, "]")
		ENDIF
	ENDIF
	
	SWITCH eUpgrade
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F1
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F2
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F3
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F4
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F5
			SWITCH iUpgradeIndex
				CASE 0	RETURN 100
				CASE 1	RETURN 200
				CASE 2	RETURN 300
			ENDSWITCH
		BREAK
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F1
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F2
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F3
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F4
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F5
			SWITCH iUpgradeIndex
				CASE 0	RETURN 100
				CASE 1	RETURN 200
				CASE 2	RETURN 300
				CASE 3	RETURN 400
				CASE 4	RETURN 500
				CASE 5	RETURN 600
				CASE 6	RETURN 700
				CASE 7	RETURN 800
				CASE 8	RETURN 900
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	UNUSED_PARAMETER(eGarageID)
	CASSERTLN(DEBUG_INTERNET, "GET_MULTISTOREY_GARAGE_UPGRADE_COST - invalid iUpgradeIndex '", iUpgradeIndex , "' used for ", GET_MULTISTOREY_GARAGE_UPGRADE_NAME_FROM_ID(eUpgrade, iUpgradeIndex))
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the base cose of the Multistorey Garage upgrade. This is usually set at the start, so we can show the difference between the sale_cost and base_cost
/// PARAMS:
///    eUpgrade - Upgrade enum
///    eGarageID - Multistorey Garage Property enum
///    iUpgradeIndex - Upgrade index
FUNC INT GET_MULTISTOREY_GARAGE_UPGRADE_BASE_COST(MULTISTOREY_GARAGE_UPGRADE_ID eUpgrade, MULTISTOREY_GARAGE_ID eGarageID, INT iUpgradeIndex = -1)
	#IF IS_DEBUG_BUILD
	IF g_bForcePropertyWebsite_sale
		RETURN GET_MULTISTOREY_GARAGE_UPGRADE_COST(eUpgrade, eGarageID, iUpgradeIndex) + 1
	ENDIF
	#ENDIF
	
	SWITCH eUpgrade
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F1
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F2
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F3
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F4
		CASE MULTISTOREY_GARAGE_UPGRADE_STYLE_F5
			SWITCH iUpgradeIndex
				CASE 0	RETURN 100
				CASE 1	RETURN 200
				CASE 2	RETURN 300
			ENDSWITCH
		BREAK
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F1
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F2
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F3
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F4
		CASE MULTISTOREY_GARAGE_UPGRADE_TINT_F5
			SWITCH iUpgradeIndex
				CASE 0	RETURN 100
				CASE 1	RETURN 200
				CASE 2	RETURN 300
				CASE 3	RETURN 400
				CASE 4	RETURN 500
				CASE 5	RETURN 600
				CASE 6	RETURN 700
				CASE 7	RETURN 800
				CASE 8	RETURN 900
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	UNUSED_PARAMETER(eGarageID)
	CASSERTLN(DEBUG_INTERNET, "GET_MULTISTOREY_GARAGE_UPGRADE_COST - invalid iUpgradeIndex '", iUpgradeIndex , "' used for ", GET_MULTISTOREY_GARAGE_UPGRADE_NAME_FROM_ID(eUpgrade, iUpgradeIndex))
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the sale cost of the provded Multistorey Garage property. This usually gets changed by live ops for promotions etc. 
/// PARAMS:
///    eGarageID - Multistorey Garage Property Enum
FUNC INT GET_MULTISTOREY_GARAGE_COST(MULTISTOREY_GARAGE_ID eGarageID)
	IF (NETWORK_IS_GAME_IN_PROGRESS() AND USE_SERVER_TRANSACTIONS())
	#IF IS_DEBUG_BUILD
	AND NOT g_bAddMenuItemsToCatalogue AND NOT g_bVerifyMenuItemPrices
	#ENDIF
		TEXT_LABEL_63 tlGeneratedKey
		GENERATE_MULTISTOREY_GARAGE_KEY_FOR_CATALOGUE(tlGeneratedKey, eGarageID)
		
		INT iGeneratedKey = GET_HASH_KEY(tlGeneratedKey)
		
		IF NET_GAMESERVER_CATALOG_ITEM_KEY_IS_VALID(iGeneratedKey)
			INT iCataloguePrice = NET_GAMESERVER_GET_PRICE(iGeneratedKey, CATEGORY_PROPERTIE, 1)
			TEXT_LABEL_15 tlPropertyName = GET_MULTISTOREY_GARAGE_NAME_FROM_ID(eGarageID)
			
			PRINTLN("GET_MULTISTOREY_GARAGE_COST: Catalogue price for ", tlPropertyName, " ", tlGeneratedKey, " is $", iCataloguePrice)
			RETURN iCataloguePrice
		ELSE
			TEXT_LABEL_15 tlPropertyName = GET_MULTISTOREY_GARAGE_NAME_FROM_ID(eGarageID)
			PRINTLN("GET_MULTISTOREY_GARAGE_COST: Invalid key, missing catalog item for ", tlPropertyName, " ", tlGeneratedKey, " [hash:", iGeneratedKey, "]")
		ENDIF
	ENDIF
	
	SWITCH eGarageID
		CASE WEST_VINEWOOD_MULTISTOREY_GARAGE		RETURN 1000
	ENDSWITCH
	
	CASSERTLN(DEBUG_INTERNET, "GET_MULTISTOREY_GARAGE_COST - missing price for ", GET_MULTISTOREY_GARAGE_NAME_FROM_ID(eGarageID), " \"", GET_STRING_FROM_TEXT_FILE(GET_MULTISTOREY_GARAGE_NAME_FROM_ID(eGarageID)), "\"")
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the base cost of the provded Multistorey Garage property. This is usually set at the start, so we can show the difference between the sale_cost and base_cost 
/// PARAMS:
///    eGarageID - Multistorey Garage Property Enum
FUNC INT GET_MULTISTOREY_GARAGE_BASE_COST(MULTISTOREY_GARAGE_ID eGarageID)
	#IF IS_DEBUG_BUILD
	IF g_bForcePropertyWebsite_sale
		RETURN GET_MULTISTOREY_GARAGE_COST(eGarageID) + 1
	ENDIF
	#ENDIF
	
	SWITCH eGarageID
		CASE WEST_VINEWOOD_MULTISTOREY_GARAGE		RETURN 1000
	ENDSWITCH
	
	CASSERTLN(DEBUG_INTERNET, "GET_MULTISTOREY_GARAGE_BASE_COST - missing price for ", GET_MULTISTOREY_GARAGE_NAME_FROM_ID(eGarageID), " \"", GET_STRING_FROM_TEXT_FILE(GET_MULTISTOREY_GARAGE_NAME_FROM_ID(eGarageID)), "\"")
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Add special conditions here to determine if the player can purchase the Multistorey Garage
FUNC BOOL CAN_PLAYER_PURCHASE_MULTISTOREY_GARAGE()
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Set whetehr or not this specific Multistorey Garage prchase should be disabled. 
/// PARAMS:
///    eGarageID - Multistorey Garage property enum 
FUNC BOOL IS_MULTISTOREY_GARAGE_PURCHASE_DISABLED(MULTISTOREY_GARAGE_ID eGarageID)
	SWITCH eGarageID
		CASE WEST_VINEWOOD_MULTISTOREY_GARAGE		RETURN FALSE // Add tunables for this?
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
#ENDIF //FEATURE_DLC_2_2022
