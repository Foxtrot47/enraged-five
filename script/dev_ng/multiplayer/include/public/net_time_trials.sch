
//////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_time_trials.sch														//
// Description: A world leaderboard Time Trial race. Can be started by any player.		//
// Written by:  Martin McMillan															//
// Date: 27/03/2015																		//
//////////////////////////////////////////////////////////////////////////////////////////  

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"

//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_missions_at_coords.sch"
USING "net_mission_locate_message.sch"
USING "net_GameDir.sch"
USING "freemode_events_header.sch"
//----------------------
//	ENUM
//----------------------

ENUM AMTT_RUN_STAGE
	AMTT_INIT = 0,
	AMTT_WAIT,
	AMTT_START,
	AMTT_GOTO,
	AMTT_END,
	AMTT_CLEANUP
ENDENUM

ENUM AMTT_QR_STAGE
	AMTRQ_PRESS = 0,
	AMTRQ_FADE_OUT,
	AMTRQ_WARP,
	AMTRQ_FADE_IN
ENDENUM

ENUM HELP_TEXT_ENUM
	eHELP_CORONA = 0,
	eHELP_PRESS,
	eHELP_UNSTBL,
	eHELP_SLOW,
	eHELP_CRITICAL,
	eHELP_BOUNTY,
	eHELP_VEH
ENDENUM

//----------------------
//	CONSTANTS
//----------------------

CONST_INT TOTAL_VARIATIONS					37

CONST_INT TIMEOUT_TIME						3600000 //One hour (Real Time)
CONST_INT DISABLE_MISC_CONTROLS_TIME		2000
CONST_INT END_TIME							7000

CONST_FLOAT CHECKPOINT_RADIUS_AIR			6.0
CONST_FLOAT CHECKPOINT_RADIUS_HEIGHT_AIR	7.0
CONST_FLOAT CHECKPOINT_Z_POS_AIR			4.0
CONST_FLOAT CHECKPOINT_SIZE_AIR				10.0
CONST_FLOAT CHECKPOINT_CYLINDER_AIR			7.5

CONST_INT MIN_REWARD_MINUTES				1

//Fail Reasons
CONST_INT FAIL_VEH_DESTROYED	0
CONST_INT FAIL_VEH_LEFT			1
CONST_INT FAIL_PLAYER_DIED		2

//----------------------
//	Local Bitset
//----------------------

CONST_INT LOCAL_BS_HIDE_START				0
CONST_INT LOCAL_BS_BUTTONS_REQUESTED		1
CONST_INT LOCAL_BS_SHOWING_ENTRY_MESSAGE	2
CONST_INT LOCAL_BS_REQUESTED_CORONA_TEXT	3
CONST_INT LOCAL_BS_RESET_VEHICLE_TIMER		4
CONST_INT LOCAL_BS_SEEN_CORONA_HELP_TEXT	5
CONST_INT LOCAL_BS_UPDATE_OBJECTIVE_TEXT	6
CONST_INT LOCAL_BS_RESET_TIMER_FLASH		7
CONST_INT LOCAL_BS_TRIGGERED_QUICK_RESTART	8
CONST_INT LOCAL_BS_REQUESTED_CORONA_PROJ	9

//----------------------
//	STRUCT
//----------------------


//----------------------
//	VARIABLES
//----------------------

STRUCT AMTT_VARS_STRUCT

	VECTOR vStartPoint
	VECTOR vEndPoint

	FLOAT fStartHeading
	FLOAT fEndHeading
	
	VECTOR vStartDirection

	INT iVariation 

	INT iLocalBS

	SCRIPT_TIMER trialTimer
	SCRIPT_TIMER outOfVehicleTimer
	SCRIPT_TIMER buttonHoldTimer
	SCRIPT_TIMER endTimer
	
	BLIP_INDEX vehicleBlip
	VEHICLE_INDEX trialVehicle
	
	INT iOutVehicleTime

	INT iFinishTime
	
	INT iPersonalBest

	BLIP_INDEX destinationBlip
	CHECKPOINT_INDEX destination

	INT theMessageID = NO_LOCATE_MESSAGE_ID
	
	AMTT_RUN_STAGE eAMTT_Stage
	
	scrFmEventAmbientMission telemetryStruct
	INT iTelVehicleType
	INT iTelParTimeBeaten
	INT iHashedMac
	INT iMatchHistoryId
	
	AMTT_QR_STAGE eQRStage
	
	BOOL bStartedSound
	INT iSoundID
	
	CHECKPOINT_INDEX groundProj
	
	#IF FEATURE_STUNT_FM_EVENTS
	BOOL bStunt
	INT iNextCheckpoint = 0
	BLIP_INDEX nextCheckpointBlip
	#ENDIF
		
	#IF IS_DEBUG_BUILD
	BOOL bEndScript = FALSE
	BOOL bWidgetsCreated = FALSE
	BOOL bWarpToStart = FALSE
	BOOL bWarpToEnd = FALSE
	
	FLOAT fPointX,fPointY,fPointz,fRotX,fRotY,fRotz
	#ENDIF

ENDSTRUCT

#IF FEATURE_STUNT_FM_EVENTS
FUNC BOOL IS_STUNT_TRIAL(AMTT_VARS_STRUCT &sTTVarsStruct)
	
	RETURN sTTVarsStruct.bStunt
	
ENDFUNC
#ENDIF

/// PURPOSE: Helper function to get debug string for stage
///    
/// PARAMS: 
///    stage - The stage 
/// RETURNS: The name of the stage
///    
FUNC STRING GET_AMTT_STAGE_STRING(AMTT_RUN_STAGE stage)

	SWITCH(stage)
		CASE AMTT_INIT				RETURN "AMTT_INIT"
		CASE AMTT_WAIT				RETURN "AMTT_WAIT"
		CASE AMTT_START				RETURN "AMTT_START"
		CASE AMTT_GOTO				RETURN "AMTT_GOTO"
		CASE AMTT_END 				RETURN "AMTT_END"
		CASE AMTT_CLEANUP			RETURN "AMTT_CLEANUP"
	ENDSWITCH

	RETURN "UNKNOWN STAGE!"
	
ENDFUNC

//Helper proc for setting client stage
PROC SET_LOCAL_AMTT_STAGE(AMTT_VARS_STRUCT &sTTVarsStruct, AMTT_RUN_STAGE stage)
	sTTVarsStruct.eAMTT_Stage = stage
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === eAMTT_Stage = ",GET_AMTT_STAGE_STRING(stage))
ENDPROC

FUNC STRING GET_QR_STAGE_NAME(AMTT_QR_STAGE eStage)

	SWITCH(eStage)
		CASE AMTRQ_PRESS		RETURN "AMTRQ_PRESS"
		CASE AMTRQ_FADE_OUT		RETURN "AMTRQ_FADE_OUT"
		CASE AMTRQ_WARP			RETURN "AMTRQ_WARP"
		CASE AMTRQ_FADE_IN		RETURN "AMTRQ_FADE_IN"
	ENDSWITCH
	
	RETURN ""

ENDFUNC

PROC SET_AMTRQ_STAGE(AMTT_VARS_STRUCT &sTTVarsStruct, AMTT_QR_STAGE eStage)
	sTTVarsStruct.eQRStage = eStage
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SET_AMTRQ_STAGE = ",GET_QR_STAGE_NAME(eStage))
ENDPROC

FUNC INT GET_TIME_TRIAL_BEST_TIME_STAT(AMTT_VARS_STRUCT &sTTVarsStruct)

	#IF FEATURE_STUNT_FM_EVENTS
	IF sTTVarsStruct.bStunt
		//RETURN GET_MP_INT_PLAYER_STAT(MPPLY_TIMETRIALBESTTIME_2)
	ELSE
	#ENDIF
		RETURN GET_MP_INT_PLAYER_STAT(MPPLY_TIMETRIALBESTTIME)
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF
	
	#IF NOT FEATURE_STUNT_FM_EVENTS
	UNUSED_PARAMETER(sTTVarsStruct)
	#ENDIF

ENDFUNC

PROC SET_TIME_TRIAL_BEST_TIME_STAT(AMTT_VARS_STRUCT &sTTVarsStruct, INT value)

	#IF FEATURE_STUNT_FM_EVENTS
	IF sTTVarsStruct.bStunt
		//SET_MP_INT_PLAYER_STAT(MPPLY_TIMETRIALBESTTIME_2,value )
		//CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SET_TIME_TRIAL_BEST_TIME_STAT - bStunt = ",value)
	ELSE
	#ENDIF
		SET_MP_INT_PLAYER_STAT(MPPLY_TIMETRIALBESTTIME,value)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SET_TIME_TRIAL_BEST_TIME_STAT = ",value)
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF
	
	#IF NOT FEATURE_STUNT_FM_EVENTS
	UNUSED_PARAMETER(sTTVarsStruct)
	#ENDIF

ENDPROC

FUNC INT GET_TIME_TRIAL_COMPLETED_WEEK_STAT(AMTT_VARS_STRUCT &sTTVarsStruct)

	#IF FEATURE_STUNT_FM_EVENTS
	IF sTTVarsStruct.bStunt
		//RETURN GET_PACKED_STAT_INT(PACKED_MP_MPPLY_TIMETRIAL_COMPLETED_WEEK_2)
	ELSE
	#ENDIF
		RETURN GET_MP_INT_PLAYER_STAT(MPPLY_TIMETRIAL_COMPLETED_WEEK)
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF
	
	#IF NOT FEATURE_STUNT_FM_EVENTS
	UNUSED_PARAMETER(sTTVarsStruct)
	#ENDIF

ENDFUNC

PROC SET_TIME_TRIAL_COMPLETED_WEEK_STAT(AMTT_VARS_STRUCT &sTTVarsStruct, INT value)

	#IF FEATURE_STUNT_FM_EVENTS
	IF sTTVarsStruct.bStunt
		//SET_PACKED_STAT_INT(PACKED_MP_MPPLY_TIMETRIAL_COMPLETED_WEEK_2,value)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SET_TIME_TRIAL_COMPLETED_WEEK_STAT - bStunt = ",value)
	ELSE
	#ENDIF
		SET_MP_INT_PLAYER_STAT(MPPLY_TIMETRIAL_COMPLETED_WEEK,value)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SET_TIME_TRIAL_COMPLETED_WEEK_STAT = ",value)
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF
	
	#IF NOT FEATURE_STUNT_FM_EVENTS
	UNUSED_PARAMETER(sTTVarsStruct)
	#ENDIF

ENDPROC

FUNC BOOL GET_TIME_TRIAL_DONE_STAT_INIT_STAT(AMTT_VARS_STRUCT &sTTVarsStruct)

	#IF FEATURE_STUNT_FM_EVENTS
	IF sTTVarsStruct.bStunt
		RETURN GET_PACKED_STAT_BOOL(PACKED_MP_TIMETRIAL_2_DONE_STAT_INIT)
	ELSE
	#ENDIF
		RETURN GET_PACKED_STAT_BOOL(PACKED_MP_TIMETRIAL_DONE_STAT_INIT)
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF
	
	#IF NOT FEATURE_STUNT_FM_EVENTS
	UNUSED_PARAMETER(sTTVarsStruct)
	#ENDIF

ENDFUNC

PROC SET_TIME_TRIAL_DONE_STAT_INIT_STAT(AMTT_VARS_STRUCT &sTTVarsStruct, BOOL value)

	#IF FEATURE_STUNT_FM_EVENTS
	IF sTTVarsStruct.bStunt
		SET_PACKED_STAT_BOOL(PACKED_MP_TIMETRIAL_2_DONE_STAT_INIT,value)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SET_TIME_TRIAL_DONE_STAT_INIT_STAT - bStunt = ",value)
	ELSE
	#ENDIF
		SET_PACKED_STAT_BOOL(PACKED_MP_TIMETRIAL_DONE_STAT_INIT,value)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SET_TIME_TRIAL_DONE_STAT_INIT_STAT = ",value)
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF

	#IF NOT FEATURE_STUNT_FM_EVENTS
	UNUSED_PARAMETER(sTTVarsStruct)
	#ENDIF

ENDPROC

PROC TIME_TRIAL_TIDYUP(AMTT_VARS_STRUCT &sTTVarsStruct)

	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === TIME_TRIALS_TIDYUP")

	sTTVarsStruct.iLocalBS = 0

	#IF FEATURE_STUNT_FM_EVENTS
	sTTVarsStruct.iNextCheckpoint = 0
	#ENDIF

	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OnlyExitVehicleOnButtonRelease, FALSE)

	IF DOES_BLIP_EXIST(sTTVarsStruct.destinationBlip)
		REMOVE_BLIP(sTTVarsStruct.destinationBlip)
		DELETE_CHECKPOINT(sTTVarsStruct.destination)		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === TIDYUP - BLIP AND END POINT REMOVED")
	ENDIF
	IF DOES_BLIP_EXIST(sTTVarsStruct.vehicleBlip)
		REMOVE_BLIP(sTTVarsStruct.vehicleBlip)	
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === TIDYUP - VEHICLE BLIP REMOVED")
	ENDIF
	#IF FEATURE_STUNT_FM_EVENTS
	IF DOES_BLIP_EXIST(sTTVarsStruct.nextCheckpointBlip)
		REMOVE_BLIP(sTTVarsStruct.nextCheckpointBlip)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === NEXT BLIP REMOVED 2")
	ENDIF
	#ENDIF
	
	STOP_SOUND(sTTVarsStruct.iSoundID)
	sTTVarsStruct.bStartedSound = FALSE

	IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_FM_EVENT_START)
		CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_FM_EVENT_START)
	ENDIF
	CLEAR_HELP()
	Clear_Any_Objective_Text_From_This_Script()	

ENDPROC

FUNC BOOL IS_ANYONE_GETTING_IN_TIME_TRIAL_VEHICLE(VEHICLE_INDEX vehID)
	
	INT iNumDoors = GET_NUMBER_OF_VEHICLE_DOORS(vehID)
	
	IF iNumDoors > 0
	AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
		INT iDoor
		REPEAT iNumDoors iDoor
			IF GET_PED_USING_VEHICLE_DOOR(vehID,INT_TO_ENUM(SC_DOOR_LIST,iDoor)) != NULL
				CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === IS_ANYONE_GETTING_IN_TIME_TRIAL_VEHICLE - TRUE")
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS(VEHICLE_INDEX &trialVehicle)

	INT iDriver
	
	IF GET_VEHICLE_NUMBER_OF_PASSENGERS(trialVehicle) <= 1
		IF NOT IS_ANYONE_GETTING_IN_TIME_TRIAL_VEHICLE(trialVehicle)
			IF IS_PED_EXCLUSIVE_DRIVER_OF_VEHICLE(trialVehicle,PLAYER_PED_ID(),iDriver)
				IF GET_VEHICLE_DOOR_LOCK_STATUS(trialVehicle) = VEHICLELOCK_LOCKED_NO_PASSENGERS
					RETURN TRUE
					CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === IS_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS - TRUE")
				ELSE
					CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === IS_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS - FALSE - GET_VEHICLE_DOOR_LOCK_STATUS")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === IS_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS - FALSE - IS_PED_EXCLUSIVE_DRIVER_OF_VEHICLE")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === IS_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS - FALSE - IS_ANYONE_GETTING_IN_TIME_TRIAL_VEHICLE")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === IS_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS - FALSE - GET_VEHICLE_NUMBER_OF_PASSENGERS")
	ENDIF
		
	
	RETURN FALSE

ENDFUNC

PROC SET_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS(VEHICLE_INDEX &trialVehicle, BOOL bLocked)
	
	IF bLocked
		IF IS_VEHICLE_DRIVEABLE(trialVehicle)
			SET_VEHICLE_EXCLUSIVE_DRIVER(trialVehicle,PLAYER_PED_ID())
		ENDIF
		SET_VEHICLE_DOORS_LOCKED(trialVehicle,VEHICLELOCK_LOCKED_NO_PASSENGERS)
		SET_VEHICLE_CAN_EJECT_PASSENGERS_IF_LOCKED(trialVehicle,TRUE)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SET_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS - TRUE")
	ELSE
		IF IS_VEHICLE_DRIVEABLE(trialVehicle)
			SET_VEHICLE_EXCLUSIVE_DRIVER(trialVehicle,NULL)
		ENDIF
		SET_VEHICLE_DOORS_LOCKED(trialVehicle,VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_CAN_EJECT_PASSENGERS_IF_LOCKED(trialVehicle,FALSE)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SET_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS - FALSE")
	ENDIF

ENDPROC

PROC TIME_TRIALS_CLEANUP(AMTT_VARS_STRUCT &sTTVarsStruct,BOOL bDoFullReset = FALSE, BOOL bResetQuickRestart = TRUE)

	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === TIME_TRIALS_CLEANUP")

	TIME_TRIAL_TIDYUP(sTTVarsStruct)

	IF g_MultiplayerSettings.g_bTempPassiveModeSetting
		CLEANUP_TEMP_PASSIVE_MODE()
	ENDIF
	
	SET_PED_CAN_BE_DRAGGED_OUT(PLAYER_PED_ID(), TRUE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_NotAllowedToJackAnyPlayers, FALSE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableMelee, FALSE)
	
	IF DOES_ENTITY_EXIST(sTTVarsStruct.trialVehicle)
		DECOR_SET_BOOL(sTTVarsStruct.trialVehicle,"UsingForTimeTrial",FALSE)
		SET_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS(sTTVarsStruct.trialVehicle,FALSE)
	ENDIF
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_TIME_TRIAL,FALSE)
	COMMON_FREEMODE_AMBIENT_EVENTS_CLEAN_UP(FMMC_TYPE_TIME_TRIAL,TRUE)
	SET_FREEMODE_FLOW_COMMS_DISABLED(FALSE)
	FM_EVENT_SET_PLAYER_RESTRICTED_WITH_PASSIVE()
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)		
	ENDIF
	
	IF IS_KILL_YOURSELF_OPTION_DISABLED()
		ENABLE_KILL_YOURSELF_OPTION()
	ENDIF

	MPGlobalsAmbience.bSuppressMapExitChecks = FALSE
	CLEAR_LOCAL_PLAYER_TIME_TRIAL_CORONA(FMMC_TYPE_TIME_TRIAL)

	IF bResetQuickRestart
		SET_AMTRQ_STAGE(sTTVarsStruct,AMTRQ_PRESS)
	ENDIF

	IF bDoFullReset
		SET_LOCAL_AMTT_STAGE(sTTVarsStruct,AMTT_INIT)
	ELSE
		SET_LOCAL_AMTT_STAGE(sTTVarsStruct,AMTT_WAIT)
	ENDIF
ENDPROC

//----------------------
//	DBG FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD

PROC PROCESS_DEBUG(AMTT_VARS_STRUCT &sTTVarsStruct)

	#IF FEATURE_STUNT_FM_EVENTS
	IF sTTVarsStruct.bStunt
	AND MPGlobalsAmbience.bLaunchStuntTimeTrial
		sTTVarsStruct.bEndScript = TRUE
		sTTVarsStruct.bWarpToStart = TRUE
		MPGlobalsAmbience.bLaunchStuntTimeTrial = FALSE
	ELSE
		IF NOT sTTVarsStruct.bStunt
	#ENDIF
	
	IF MPGlobalsAmbience.bLaunchTimeTrial
		sTTVarsStruct.bEndScript = TRUE
		sTTVarsStruct.bWarpToStart = TRUE
		MPGlobalsAmbience.bLaunchTimeTrial = FALSE
	ENDIF
	
	#IF FEATURE_STUNT_FM_EVENTS
		ENDIF
	ENDIF
	#ENDIF
	
	//Restart script
	#IF FEATURE_STUNT_FM_EVENTS
	IF sTTVarsStruct.bStunt
		IF sTTVarsStruct.bEndScript
			IF sTTVarsStruct.eAMTT_Stage >= AMTT_WAIT
				TIME_TRIALS_CLEANUP(sTTVarsStruct,MPGlobalsAmbience.iDEBUGStuntTimeTrialVariation != sTTVarsStruct.iVariation)
			ENDIF
			MPGlobalsAmbience.iStuntTimeTrialVariation = -1
			sTTVarsStruct.bEndScript = FALSE
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === eAMTT_Stage = AMTT_INIT - Debug Variation = ",MPGlobalsAmbience.iDEBUGStuntTimeTrialVariation)
		ENDIF
	ELSE
	#ENDIF
	
		IF sTTVarsStruct.bEndScript
			IF sTTVarsStruct.eAMTT_Stage >= AMTT_WAIT
				TIME_TRIALS_CLEANUP(sTTVarsStruct,MPGlobalsAmbience.iDEBUGTimeTrialVariation != sTTVarsStruct.iVariation)
			ENDIF
			MPGlobalsAmbience.iTimeTrialVariation = -1
			sTTVarsStruct.bEndScript = FALSE
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === eAMTT_Stage = AMTT_INIT - Debug Variation = ",MPGlobalsAmbience.iDEBUGTimeTrialVariation)
		ENDIF
	
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF
	
	IF sTTVarsStruct.bWarpToStart
		IF sTTVarsStruct.eAMTT_Stage >= AMTT_WAIT
			IF NET_WARP_TO_COORD(sTTVarsStruct.vStartPoint,sTTVarsStruct.fStartHeading,TRUE,FALSE,DEFAULT,DEFAULT,DEFAULT,TRUE)
				sTTVarsStruct.bWarpToStart = FALSE
				CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === DEBUG - Warping to start at ",sTTVarsStruct.vStartPoint)
			ENDIF
		ENDIF
	ENDIF
	
	IF sTTVarsStruct.bWarpToEnd
		IF sTTVarsStruct.eAMTT_Stage >= AMTT_WAIT
			IF NET_WARP_TO_COORD(sTTVarsStruct.vEndPoint,sTTVarsStruct.fEndHeading,TRUE,FALSE,DEFAULT,DEFAULT,DEFAULT,TRUE)
				sTTVarsStruct.bWarpToEnd = FALSE
				CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === DEBUG - Warping to end at ",sTTVarsStruct.vStartPoint)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

#ENDIF

//----------------------
//	FUNCTIONS
//----------------------


/// PURPOSE: Gets the name of the route
///    
/// PARAMS:
///    iVariation - The route index
/// RETURNS: A text label containing the name of the route
///    
FUNC STRING GET_TIME_TRIAL_ROUTE_STRING(INT iVariation #IF FEATURE_STUNT_FM_EVENTS, BOOL bStunt = FALSE #ENDIF)

	TEXT_LABEL_15 txt15 = "AMTT_ROUTE" 
	
	txt15 += iVariation
	
	#IF FEATURE_STUNT_FM_EVENTS
	IF bStunt
		txt15 += "S"
	ENDIF
	#ENDIF
	
	RETURN TEXT_LABEL_TO_STRING(txt15)

ENDFUNC

/// PURPOSE: Gets the description of the route
///    
/// PARAMS:
///    iVariation - The route index
/// RETURNS: A text label containing the description of the route
///    
FUNC STRING GET_TIME_TRIAL_DESCRIPTION_STRING(INT iVariation)

	TEXT_LABEL_15 txt15 = "AMTT_DESC" 
	
	txt15 += iVariation
	
	RETURN TEXT_LABEL_TO_STRING(txt15)

ENDFUNC

/// PURPOSE: Gets the name of the vehicle class
///    
/// PARAMS:
///    vehClass - The class to get the name of
/// RETURNS: A text label containing the name of the vehicle class
///    
//FUNC STRING GET_TIME_TRIAL_VEHICLE_CLASS_STRING(VEHICLE_CLASS vehClass)
//
//	SWITCH(vehClass)
//	
//		CASE VC_COMPACT			RETURN "AMTT_CLASS0"
//		CASE VC_SEDAN			RETURN "AMTT_CLASS1"
//		CASE VC_SUV				RETURN "AMTT_CLASS2"
//		CASE VC_COUPE			RETURN "AMTT_CLASS3"
//		CASE VC_MUSCLE			RETURN "AMTT_CLASS4"
//		CASE VC_SPORT_CLASSIC	RETURN "AMTT_CLASS5"
//		CASE VC_SPORT			RETURN "AMTT_CLASS6"
//		CASE VC_SUPER			RETURN "AMTT_CLASS7"
//		CASE VC_MOTORCYCLE		RETURN "AMTT_CLASS8"
//		CASE VC_OFF_ROAD		RETURN "AMTT_CLASS9"
//		CASE VC_INDUSTRIAL		RETURN "AMTT_CLASS10"
//		CASE VC_UTILITY			RETURN "AMTT_CLASS11"
//		CASE VC_VAN				RETURN "AMTT_CLASS12"
//		CASE VC_CYCLE			RETURN "AMTT_CLASS13"
////		CASE VC_HELICOPTER		RETURN "AMTT_CLASS14"
////		CASE VC_PLANE			RETURN "AMTT_CLASS15"
//		CASE VC_SERVICE			RETURN "AMTT_CLASS16"
//		CASE VC_EMERGENCY		RETURN "AMTT_CLASS17"
//		CASE VC_MILITARY		RETURN "AMTT_CLASS18"
//		CASE VC_COMMERCIAL 		RETURN "AMTT_CLASS19"
//
//	ENDSWITCH
//
//	RETURN "AMTT_CLASS20" //Freestyle
//
//ENDFUNC

FUNC INT GRAB_TIME_TRIAL_VARIATION(#IF FEATURE_STUNT_FM_EVENTS BOOL bStunt = FALSE #ENDIF)

	RETURN GET_CURRENT_ACTIVE_FM_TIME_TRIAL(#IF FEATURE_STUNT_FM_EVENTS bStunt #ENDIF)

ENDFUNC

/// PURPOSE: Picks a random variation and grabs the coords
///    
/// PARAMS:
///    iVariation - Stores the variation that is picked
///    vStartPoint - Stores the start point of the trial
///    fStartHeading - Stores the optimal heading for the start of the trial
///    vEndPoint - Stores the end point of the trial
///    fEndHeading - Stores the optimal heading for the end of the trial
///    sTTVarsStruct - The time trial data
/// RETURNS: TRUE if locations retrieved successfully, FALSE otherwise
///    
FUNC BOOL GET_TIME_TRIAL_LOCATION_INFO(INT iVariation, VECTOR &vStartPoint, FLOAT &fStartHeading, VECTOR &vEndPoint, FLOAT &fEndHeading,VECTOR &vStartDirection #IF FEATURE_STUNT_FM_EVENTS, BOOL bStuntTrial = FALSE #ENDIF )

	#IF FEATURE_STUNT_FM_EVENTS
	IF bStuntTrial
		SWITCH(iVariation) 
		
			//City
			CASE ciFM_STUNT_TIME_TRIAL_CITY
				vStartPoint = <<779.2087, -236.396, 65.1143>>
				fStartHeading = 250.0000
				vEndPoint = <<106.3731, -570.445, 30.6302>>
				fEndHeading = 350.6013
				vStartDirection = <<840.0327, -256.8809, vStartPoint.z>>
			BREAK
			
		ENDSWITCH
	ELSE
	#ENDIF
	
		SWITCH(iVariation) 
		
			//Up Chilliad
			CASE ciFM_TIME_TRIAL_UP_CHILLIAD
				vStartPoint = <<-552.6260, 5042.7026, 127.9448>>
				fStartHeading = 243.7129
				vEndPoint = <<500.2789, 5597.2485, 794.7260>>
				fEndHeading = 350.6013
				vStartDirection = <<-539.9766, 5037.2661, vStartPoint.z>>
			BREAK
			
			//Down Chilliad
			CASE ciFM_TIME_TRIAL_DOWN_CHILLIAD
				vStartPoint = <<526.3970, 5624.4609, 779.3564>>
				fStartHeading = 311.1830
				vEndPoint = <<2278.7446, 5788.4541, 154.0056>>
				fEndHeading = 292.2128
				vStartDirection = <<532.6158, 5637.7495, vStartPoint.z>>
			BREAK
			
			//Mount Gordo
			CASE ciFM_TIME_TRIAL_MOUNT_GORDO
				vStartPoint = <<2702.0369, 5145.7168, 42.8568>> 
				fStartHeading = 345.7831
				vEndPoint = <<2848.1838, 5945.5020, 355.2424>> 
				fEndHeading = 40.5070
				vStartDirection = <<2703.4487, 5152.7251, vStartPoint.z>>
			BREAK
			
			//Great Ocean Highway
			CASE ciFM_TIME_TRIAL_GREAT_OCEAN_HIGHWAY
				vStartPoint = <<-1253.2399, -380.4570, 58.2873>>
				fStartHeading = 72.3034 
				vEndPoint = <<-2223.1555, 4254.6802, 45.4055>>  
				fEndHeading = 248.7762
				vStartDirection = <<-1262.9346, -377.1622, vStartPoint.z>>
			BREAK
			
			//Observatory
			CASE ciFM_TIME_TRIAL_OBSERVATORY
				vStartPoint = <<-377.1660, 1250.8182, 326.4899>> 
				fStartHeading = 306.2901
				vEndPoint = <<2168.5886, 4777.3057, 40.2251>> 
				fEndHeading = 341.8660
				vStartDirection = <<-370.2543, 1255.8573, vStartPoint.z>>
			BREAK
			
			//Coast to Coast
			CASE ciFM_TIME_TRIAL_COAST_TO_COAST
				vStartPoint = <<-1502.0471, 4940.6108, 63.8034>>
				fStartHeading = 143.5247
				vEndPoint = <<3782.3245, 4464.4082, 5.0935>> 
				fEndHeading = 269.4335
				vStartDirection = <<-1504.6981, 4936.6445, vStartPoint.z>>
			BREAK
			
			//End to End
			CASE ciFM_TIME_TRIAL_END_TO_END
				vStartPoint = <<1261.3533, -3278.3799, 4.8335>>
				fStartHeading = 356.8981
				vEndPoint = <<95.0126, 6793.0542, 19.1916>>  
				fEndHeading = 84.1730
				vStartDirection = <<1261.5498, -3273.2153, vStartPoint.z>>
			BREAK
			
			//Sawmill
			CASE ciFM_TIME_TRIAL_SAWMILL
				vStartPoint = <<-579.1157, 5324.6641, 69.2662>>
				fStartHeading = 159.3018
				vEndPoint = <<2763.4895, 2756.7056, 42.2599>>   
				fEndHeading = 114.3906
				vStartDirection = <<-580.1998, 5321.9268, vStartPoint.z>>
			BREAK
			
			//Fort Zancudo
			CASE ciFM_TIME_TRIAL_FORT_ZANCUDO
				vStartPoint = <<-1554.3121, 2755.0088, 16.8004>> 
				fStartHeading = 228.5935
				vEndPoint = <<807.0756, 1277.6714, 359.4458>>   
				fEndHeading = 84.6859
				vStartDirection = <<-1552.1925, 2753.2791, vStartPoint.z>>
			BREAK
			
			//Del Perro Pier
			CASE ciFM_TIME_TRIAL_DEL_PERRO_PIER
				vStartPoint = <<-1811.6750, -1199.5421, 12.0174>>
				fStartHeading = 319.1759
				vEndPoint = <<1665.5677, -13.8910, 172.7744>>  
				fEndHeading = 14.7216
				vStartDirection = <<-1809.0801, -1196.4022, vStartPoint.z>>
			BREAK
			
			//Storm Drain
			CASE ciFM_TIME_TRIAL_STORM_DRAIN
				vStartPoint = <<637.1439, -1845.8552, 8.2676>>  
				fStartHeading = 355.1936 
				vEndPoint = <<1049.1497, -264.0645, 50.4311>>  
				fEndHeading = 326.7219
				vStartDirection = <<637.3676, -1842.2245, vStartPoint.z>>
			BREAK
			
			//Cypress Flats
			CASE ciFM_TIME_TRIAL_CYPRESS_FLATS
				vStartPoint = <<1067.3430, -2448.2366, 28.0683>>
				fStartHeading = 84.3956
				vEndPoint = <<2072.5647, 2342.5327, 93.5678>>
				fEndHeading = 262.5002 
				vStartDirection = <<1064.9830, -2448.0498, vStartPoint.z>>
			BREAK
			
			//Power Station
			CASE ciFM_TIME_TRIAL_POWER_STATION
				vStartPoint =  <<2820.6514, 1642.2759, 23.6680>>
				fStartHeading = 88.7852
				vEndPoint = <<1365.9885, -578.5997, 73.3803>> 
				fEndHeading = 247.2179
				vStartDirection = <<2811.7136, 1642.4181, vStartPoint.z>>
			BREAK
			
			//Casino
			CASE ciFM_TIME_TRIAL_CASINO
				vStartPoint = <<947.5620, 142.6773, 79.8307>>
				fStartHeading = 309.5725
				vEndPoint = <<-102.2509, 854.2916, 234.7128>>
				fEndHeading = 89.0971
				vStartDirection = <<915.5455, 49.9490, vStartPoint.z>>
			BREAK
			
			//Up-n-Atom
			CASE ciFM_TIME_TRIAL_UP_N_ATOM
				vStartPoint = <<1577.1890, 6439.9658, 23.6996>> 
				fStartHeading = 96.3200
				vEndPoint = <<-2422.9395, 4229.4185, 8.1163>>
				fEndHeading = 110.6616
				vStartDirection = <<1573.7222, 6439.5620, vStartPoint.z>>
			BREAK
		
			//Raton Canyon
			CASE ciFM_TIME_TRIAL_RATON_CANYON 
				vStartPoint = <<-2257.7986, 4315.9268, 44.5551>>
				fStartHeading = 1.1533
				vEndPoint = <<-243.3874, 4084.5967, 36.0077>>
				fEndHeading = 336.2690
				vStartDirection = <<-2258.0791, 4319.8101, vStartPoint.z>>
			BREAK
			
			//Calafia Way
			CASE ciFM_TIME_TRIAL_CALAFIA_WAY
				vStartPoint = <<231.9767, 3301.4888, 39.5627>>
				fStartHeading = 339.4784
				vEndPoint = <<995.3146, 4459.6958, 49.8577>>
				fEndHeading = 261.1064
				vStartDirection = <<232.8029, 3303.3013, vStartPoint.z>>
			BREAK
			
			//Route 68
			CASE ciFM_TIME_TRIAL_ROUTE_68
				vStartPoint = <<1246.2249, 2685.1099, 36.5944>>
				fStartHeading = 84.3841
				vEndPoint = <<-2566.3264, 2330.0984, 32.0600>>
				fEndHeading = 98.5212
				vStartDirection = <<1242.8779, 2685.2307, vStartPoint.z>>
			BREAK
			
			//Tongva Valley
			CASE ciFM_TIME_TRIAL_TONGVA_VALLEY
				vStartPoint = <<-1504.5410, 1482.4895, 116.0530>>
				fStartHeading = 187.7171
				vEndPoint = <<-631.9410, -371.4568, 33.8127>>
				fEndHeading = 43.9815
				vStartDirection = <<-1503.9414, 1478.5476, vStartPoint.z>>
			BREAK
			
			//LSIA
			CASE ciFM_TIME_TRIAL_LSIA
				vStartPoint = <<-1021.1459, -2580.2910, 33.6353>>
				fStartHeading = 332.0736
				vEndPoint = <<-1505.4611, 1485.9398, 115.6857>>
				fEndHeading = 326.8418
				vStartDirection = <<-1007.3947, -2556.8115, vStartPoint.z>>
			BREAK
			
			//Vinewood Bowl
			CASE ciFM_TIME_TRIAL_VINEWOOD_BOWL
				vStartPoint = <<860.3530, 536.8055, 124.7803>>
				fStartHeading = 239.7346
				vEndPoint = <<-1578.3798, 5170.1460, 18.5865>>
				fEndHeading = 345.1913
				vStartDirection = <<861.8033, 535.9179, vStartPoint.z>>
			BREAK
			
			//Maze Bank Arena
			CASE ciFM_TIME_TRIAL_MAZE_BANK_ARENA
				vStartPoint = <<-199.7486, -1973.3108, 26.6204>>
				fStartHeading = 179.0515
				vEndPoint = <<228.1437, 1196.7448, 224.4599>>
				fEndHeading = 194.4888
				vStartDirection = <<-199.3188, -1976.9370, vStartPoint.z>>
			BREAK
			
			CASE ciFM_TIME_TRIAL_ELYSIAN_ISLAND	
				vStartPoint = <<175.2847, -3042.0754, 4.7734>>
				fStartHeading = 0.0000
				vEndPoint = <<2782.9714, -711.3731, 4.0575>>
				fEndHeading = 289.9463
				vStartDirection = <<175.2847, -3038.0754, vStartPoint.z>>
			BREAK	
			
			CASE ciFM_TIME_TRIAL_GALILEO_PARK	
				vStartPoint = <<813.3556, 1274.9536, 359.5110>>
				fStartHeading = 269.1693
				vEndPoint = <<-2306.7690, 439.6400, 173.4667>>
				fEndHeading = 172.0443
				vStartDirection = <<817.3556, 1274.9536, vStartPoint.z>>
			BREAK	

			CASE ciFM_TIME_TRIAL_STAB_CITY	
				vStartPoint = <<77.5248, 3629.9146, 38.6907>>
				fStartHeading = 188.0191
				vEndPoint = <<-537.2578, 281.2907, 82.0704>>
				fEndHeading = 176.1337
				vStartDirection = <<78.5248, 3626.9146, vStartPoint.z>>
			BREAK	

			CASE ciFM_TIME_TRIAL_VINEWOOD_HILLS	
				vStartPoint = <<1004.6567, 898.8370, 209.0257>>
				fStartHeading = 26.0546
				vEndPoint = <<-3140.7295, 1180.8759, 19.309>>
				fEndHeading = 89.9546
				vStartDirection = <<1003.6567, 902.8370, vStartPoint.z>>
			BREAK	

			CASE ciFM_TIME_TRIAL_GROVE_STREET	
				vStartPoint = <<104.8058, -1938.9818, 19.8037>>
				fStartHeading = 50.4887
				vEndPoint = <<-931.7014, 174.1591, 65.2086>>
				fEndHeading = 292.6181
				vStartDirection = <<102.2058, -1936.9818, vStartPoint.z>>
			BREAK	

			CASE ciFM_TIME_TRIAL_LSIA_2	
				vStartPoint = <<-985.2776, -2698.6960, 12.8307>>
				fStartHeading = 60.7633
				vEndPoint = <<-408.1892, 1184.9517, 324.5297>>
				fEndHeading = 262.1661
				vStartDirection = <<-988.2776, -2696.6960, vStartPoint.z>>
			BREAK	
			
			CASE ciFM_TIME_TRIAL_LOS_SANTOS_CORONER	
				vStartPoint = <<230.6618, -1399.0258, 29.4856>>
				fStartHeading = 139.1333
				vEndPoint = <<-319.9582, 2729.5784, 67.8719>>
				fEndHeading = 320.5662
				vStartDirection = <<228.6618, -1401.0258, vStartPoint.z>>
			BREAK	
			
			CASE ciFM_TIME_TRIAL_ELYSIAN_ISLAND_2
				vStartPoint = <<-546.6672, -2857.9282, 5.0004>>
				fStartHeading = 290.9365
				vEndPoint = <<-1605.2700, -953.8167, 12.0174>>
				fEndHeading = 138.9763
				vStartDirection = <<-542.6672, -2855.9282, vStartPoint.z>>
			BREAK	

			CASE ciFM_TIME_TRIAL_VINEWOOD_ESTATE	
				vStartPoint = <<-172.8944, 1034.8262, 231.2332>>
				fStartHeading = 67.2348
				vEndPoint = <<-1580.6731, 3060.9778, 31.1954>>
				fEndHeading = 0.0000
				vStartDirection = <<-176.8944, 1036.8262, vStartPoint.z>>
			BREAK	
			
			CASE ciFM_TIME_TRIAL_EL_BURRO_HEIGHTS	
				vStartPoint = <<1691.4703, -1458.6351, 111.7033>>
				fStartHeading = 320.3820
				vEndPoint = <<-408.4781, 1184.1001, 324.5365>>
				fEndHeading = 81.5969
				vStartDirection = <<1695.4703, -1455.6351, vStartPoint.z>>
			BREAK	
			
		ENDSWITCH
		
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF
	
	IF ARE_VECTORS_EQUAL(vStartPoint,<<0.0,0.0,0.0>>)
	OR fStartHeading < 0
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === LOCATION NOT RETRIEVED SUCCESSFULLY! vStartPoint = ",vStartPoint," fStartHeading = ",fStartHeading," vEndPoint = ",vEndPoint, " fEndHeading = ",fEndHeading)
		RETURN FALSE
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === GET_TIME_TRIAL_LOCATION_INFO vStartPoint = ",vStartPoint," fStartHeading = ",fStartHeading," vEndPoint = ",vEndPoint, " fEndHeading = ",fEndHeading)
	RETURN TRUE

ENDFUNC

#IF FEATURE_STUNT_FM_EVENTS
FUNC VECTOR GET_STUNT_TRIAL_CHECKPOINT_LOCATION(INT iVariation, INT iCheckpoint)
	
	SWITCH(iVariation)
	
		CASE ciFM_STUNT_TIME_TRIAL_CITY
			SWITCH(iCheckpoint)
				CASE 	0	RETURN		<<840.0327, -256.8809, 64.6332>>
				CASE 	1	RETURN		<<904.8479, -215.7394, 69.1123>>
				CASE 	2	RETURN		<<961.4948, -138.862, 73.4599>>
				CASE 	3	RETURN		<<995.2003, -99.7645, 73.3154>>
				CASE 	4	RETURN		<<950.1896, -51.0395, 77.4228>>
				CASE 	5	RETURN		<<975.3306, 10.4039, 79.9909>>
				CASE 	6	RETURN		<<1009.6368, 66.6226, 79.9909>>
				CASE 	7	RETURN		<<962.3789, 97.8983, 79.9909>>
				CASE 	8	RETURN		<<1025.0193, 176.0011, 79.8559>>
				CASE 	9	RETURN		<<1004.3209, 194.1369, 79.9199>>
				CASE 	10	RETURN		<<782.2081, -83.5554, 79.5525>>
				CASE 	11	RETURN		<<745.9942, -172.6812, 71.9956>>
				CASE 	12	RETURN		<<668.0744, -302.915, 43.5736>>
				CASE 	13	RETURN		<<607.4564, -406.586, 23.7676>>
				CASE 	14	RETURN		<<600.1058, -467.5249, 25.0426>>
				CASE 	15	RETURN		<<580.8424, -533.4788, 23.7786>>
				CASE 	16	RETURN		<<529.2643, -618.6752, 23.7997>>
				CASE 	17	RETURN		<<456.089, -682.1043, 26.7394>>
				CASE 	18	RETURN		<<456.9668, -822.9017, 26.564>>
				CASE 	19	RETURN		<<405.7265, -833.0722, 28.3235>>
				CASE 	20	RETURN		<<413.2686, -890.7141, 28.4187>>
				CASE 	21	RETURN		<<461.2862, -893.5294, 34.9772>>
				CASE 	22	RETURN		<<474.0257, -940.4727, 35.3461>>
				CASE 	23	RETURN		<<498.6502, -1033.0028, 27.2566>>
				CASE 	24	RETURN		<<480.0416, -1074.1648, 28.2084>>
				CASE 	25	RETURN		<<463.0622, -1128.2897, 28.3559>>
				CASE 	26	RETURN		<<406.9402, -1127.6985, 28.4535>>
				CASE 	27	RETURN		<<391.8616, -999.242, 28.4171>>
				CASE 	28	RETURN		<<270.0828, -995.7447, 28.3059>>
				CASE 	29	RETURN		<<218.4261, -994.3411, 28.1364>>
				CASE 	30	RETURN		<<213.9188, -939.3922, 23.1416>>
				CASE 	31	RETURN		<<266.5893, -842.7028, 28.3292>>
				CASE 	32	RETURN		<<207.531, -812.611, 29.8616>>
				CASE 	33	RETURN		<<234.2159, -730.7259, 29.8199>>
				CASE 	34	RETURN		<<266.9308, -745.9722, 33.6404>>
				CASE 	35	RETURN		<<182.2835, -723.3073, 32.8874>>
				CASE 	36	RETURN		<<72.3754, -689.6174, 30.6514>>    			
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === GET_STUNT_TRIAL_CHECKPOINT_LOCATION - LOCATION NOT RETRIEVED SUCCESSFULLY!")
	RETURN <<0.0,0.0,0.0>>
	
ENDFUNC

FUNC INT GET_NUM_CHECKPOINTS_FOR_ROUTE(INT iVariation)

	SWITCH(iVariation)
		CASE ciFM_STUNT_TIME_TRIAL_CITY		RETURN 37
	ENDSWITCH

	RETURN 0

ENDFUNC

FUNC BOOL IS_END_CHECKPOINT(AMTT_VARS_STRUCT &sTTVarsStruct)

	RETURN sTTVarsStruct.iNextCheckpoint >= GET_NUM_CHECKPOINTS_FOR_ROUTE(sTTVarsStruct.iVariation)

ENDFUNC

FUNC VECTOR GET_NEXT_CHECKPOINT_DRAW_LOCATION(AMTT_VARS_STRUCT &sTTVarsStruct, BOOL bNext = FALSE)

	INT iCheckpoint = sTTVarsStruct.iNextCheckpoint
	IF bNext
		iCheckpoint++
	ENDIF

	IF NOT sTTVarsStruct.bStunt
	OR IS_END_CHECKPOINT(sTTVarsStruct)
		RETURN sTTVarsStruct.vEndPoint
	ELSE
		RETURN GET_STUNT_TRIAL_CHECKPOINT_LOCATION(sTTVarsStruct.iVariation, iCheckpoint)
	ENDIF

ENDFUNC
#ENDIF

#IF NOT FEATURE_STUNT_FM_EVENTS
FUNC BOOL IS_END_CHECKPOINT(AMTT_VARS_STRUCT &sTTVarsStruct)
	
	UNUSED_PARAMETER(sTTVarsStruct)
	RETURN TRUE

ENDFUNC

FUNC VECTOR GET_NEXT_CHECKPOINT_DRAW_LOCATION(AMTT_VARS_STRUCT &sTTVarsStruct)

	RETURN sTTVarsStruct.vEndPoint

ENDFUNC
#ENDIF

/// PURPOSE: Get the vehicle class of the player's vehicle
///    
/// RETURNS: The vehicle class
///    
FUNC VEHICLE_CLASS GET_TIME_TRIAL_VEHICLE_CLASS()

	VEHICLE_CLASS vehClass

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(vehID)
					vehClass = GET_VEHICLE_CLASS(vehID)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN vehClass

ENDFUNC

PROC DO_HELP_TEXT(HELP_TEXT_ENUM eHelp, AMTT_VARS_STRUCT &sTTVarsStruct)

	
	#IF FEATURE_STUNT_FM_EVENTS
	IF sTTVarsStruct.bStunt
	
		SWITCH eHelp

		CASE eHELP_CORONA
			PRINT_HELP_FOREVER("AMTT_SCORONA")
		BREAK
		
		CASE eHELP_PRESS
			PRINT_HELP_FOREVER("AMTT_SPRESS")
		BREAK
		
		CASE eHELP_UNSTBL
			PRINT_HELP_FOREVER("AMTT_SUNSTBL")
		BREAK
		
		CASE eHELP_SLOW
			PRINT_HELP_FOREVER("AMTT_SSLOW")
		BREAK
		
		CASE eHELP_CRITICAL
			PRINT_HELP_FOREVER("AMTT_SCRITICAL")
		BREAK
		
		CASE eHELP_BOUNTY
			PRINT_HELP_FOREVER("AMTT_SBOUNTY")
		BREAK
		
		CASE eHELP_VEH
			PRINT_HELP_FOREVER("AMTT_BIKE")
		BREAK
		
	ENDSWITCH
	ELSE
	#ENDIF

	SWITCH eHelp

		CASE eHELP_CORONA
			PRINT_HELP_FOREVER("AMTT_CORONA")
		BREAK
		
		CASE eHELP_PRESS
			PRINT_HELP_FOREVER("AMTT_PRESS")
		BREAK
		
		CASE eHELP_UNSTBL
			PRINT_HELP_FOREVER("AMTT_UNSTBL")
		BREAK
		
		CASE eHELP_SLOW
			PRINT_HELP_FOREVER("AMTT_SLOW")
		BREAK
		
		CASE eHELP_CRITICAL
			PRINT_HELP_FOREVER("AMTT_CRITICAL")
		BREAK
		
		CASE eHELP_BOUNTY
			PRINT_HELP_FOREVER("AMTT_BOUNTY")
		BREAK
		
		CASE eHELP_VEH
			PRINT_HELP_FOREVER("AMTT_LAND")
		BREAK
		
	ENDSWITCH
	
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF
	
	#IF NOT FEATURE_STUNT_FM_EVENTS
	UNUSED_PARAMETER(sTTVarsStruct)
	#ENDIF
	
ENDPROC

PROC CLEAR_TIME_TRIAL_HELP(#IF FEATURE_STUNT_FM_EVENTS BOOL bStunt = FALSE #ENDIF )
	
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		EXIT
	ENDIF
	
	#IF FEATURE_STUNT_FM_EVENTS
	IF bStunt
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_SCORONA")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_SPRESS")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_SUNSTBL")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_SSLOW")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_SCRITICAL")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_SBOUNTY")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_BIKE")
			CLEAR_HELP()
		ENDIF	
	ELSE
	#ENDIF
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_CORONA")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_PRESS")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_UNSTBL")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_SLOW")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_CRITICAL")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_BOUNTY")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_LAND")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AMTT_NSUIT")
			CLEAR_HELP()
		ENDIF	
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF

ENDPROC

/// PURPOSE: Starts the timer for the trial
///    
/// PARAMS: 
///    trialTimer - The timer to start
PROC STORE_TIME_AT_BEGINNING(SCRIPT_TIMER &trialTimer)
	REINIT_NET_TIMER(trialTimer)
ENDPROC

/// PURPOSE: Saves the time trial stats to the social club leaderboard
///    
/// PARAMS:
///    sTTVarsStruct - The time trial data
PROC SAVE_TIME_TRIAL_STATS_TO_LEADERBOARD(AMTT_VARS_STRUCT &sTTVarsStruct)

	sTTVarsStruct.iFinishTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTTVarsStruct.trialTimer)
	VEHICLE_CLASS vehClass
	

	
	UNUSED_PARAMETER(vehClass)
	 
ENDPROC


/// PURPOSE: Gets the player's current vehicle and stores it for the purposes of the trial
///    
/// PARAMS:
///    sTTVarsStruct - The time trial data
/// RETURNS: TRUE if the vehicle is successfully stored, FALSE otherwise
///    
FUNC BOOL GET_PLAYER_TRIAL_VEHICLE(AMTT_VARS_STRUCT &sTTVarsStruct)

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			sTTVarsStruct.trialVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === GET_PLAYER_TRIAL_VEHICLE - Player dead or left vehicle, exiting",sTTVarsStruct.theMessageID)
	RETURN FALSE

ENDFUNC

FUNC BOOL SETUP_VEHICLE_FOR_TIME_TRIAL(AMTT_VARS_STRUCT &sTTVarsStruct)

	IF IS_VEHICLE_DRIVEABLE(sTTVarsStruct.trialVehicle)
		VEHICLE_CLASS trialVC = GET_VEHICLE_CLASS(sTTVarsStruct.trialVehicle)
		IF trialVC = VC_CYCLE
		OR trialVC = VC_MOTORCYCLE
			sTTVarsStruct.iOutVehicleTime = g_sMPTunables.iTime_Trial_Return_to_Bike_Time
		ELSE
			sTTVarsStruct.iOutVehicleTime = g_sMPTunables.iTime_Trial_Return_to_Car_Time
		ENDIF
		
		DECOR_SET_BOOL(sTTVarsStruct.trialVehicle,"UsingForTimeTrial",TRUE)
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SETUP_VEHICLE_FOR_TIME_TRIAL - Player dead or left vehicle, exiting",sTTVarsStruct.theMessageID)
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_VEHICLE_MODEL_UNSUITABLE_FOR_TIME_TRIAL(MODEL_NAMES eModel)

	SWITCH eModel
		CASE OPPRESSOR
		CASE OPPRESSOR2
		CASE DELUXO
		CASE SCRAMJET
		CASE VIGILANTE
		CASE VOLTIC2
		CASE RUINER2
		CASE MONSTER3
		CASE MONSTER4
		CASE MONSTER5
		CASE CERBERUS
		CASE CERBERUS2
		CASE CERBERUS3
		CASE BRUISER
		CASE BRUISER2
		CASE BRUISER3
		CASE SLAMVAN4
		CASE SLAMVAN5
		CASE SLAMVAN6
		CASE BRUTUS
		CASE BRUTUS2
		CASE BRUTUS3
		CASE SCARAB
		CASE SCARAB2
		CASE SCARAB3
		CASE DOMINATOR4
		CASE DOMINATOR5
		CASE DOMINATOR6
		CASE IMPALER2
		CASE IMPALER3
		CASE IMPALER4
		CASE IMPERATOR
		CASE IMPERATOR2
		CASE IMPERATOR3
		CASE ZR380
		CASE ZR3802
		CASE ZR3803
		CASE ISSI4
		CASE ISSI5
		CASE ISSI6
		CASE DEATHBIKE
		CASE DEATHBIKE2
		CASE DEATHBIKE3
		CASE TOREADOR
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_IN_SUITABLE_VEHICLE_TO_START_TIME_TRIAL(VEHICLE_INDEX vehID, AMTT_VARS_STRUCT &sTTVarsStruct)

	#IF FEATURE_STUNT_FM_EVENTS
	IF sTTVarsStruct.bStunt
		MODEL_NAMES model = GET_ENTITY_MODEL(vehID)
		IF NOT IS_THIS_MODEL_A_BIKE(model)
			PRINT_HELP_FOREVER("AMTT_BIKE") //Bikes only
			RETURN FALSE
		ENDIF
	ENDIF
	#ENDIF

	IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
	OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
	OR IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
	OR IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
	OR IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
		PRINT_HELP_FOREVER("AMTT_LAND") //Land vehicles only
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_MODEL_UNSUITABLE_FOR_TIME_TRIAL(GET_ENTITY_MODEL(vehID))
	OR IS_VEHICLE_RESTRICTED_IN_PASSIVE_MODE(vehID) 
		PRINT_HELP_FOREVER("AMTT_NSUIT") //Normal vehicles only
		RETURN FALSE
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		INT iDecoratorValue
		IF DECOR_EXIST_ON(vehID, "MPBitset")
			iDecoratorValue = DECOR_GET_INT(vehID, "MPBitset")
		ENDIF
		IF IS_BIT_SET(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
			DO_HELP_TEXT(eHELP_UNSTBL,sTTVarsStruct) //This vehicle is being used by another activity.
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE: Gets the best time worldwide for a given route and vehicle class
///    
/// RETURNS: The best time
///    
FUNC INT GET_BEST_TRIAL_TIME(INT iVariation #IF FEATURE_STUNT_FM_EVENTS, BOOL bStunt = FALSE #ENDIF)

	#IF FEATURE_STUNT_FM_EVENTS
	IF bStunt
		SWITCH (iVariation)
			CASE ciFM_STUNT_TIME_TRIAL_CITY			RETURN 177250	//City

		ENDSWITCH
	ELSE
	#ENDIF

	IF g_sMPTunables.iTIME_TRIAL_OVERRIDE_TIME > 0
		RETURN g_sMPTunables.iTIME_TRIAL_OVERRIDE_TIME
	ELSE
		SWITCH (iVariation)
			CASE ciFM_TIME_TRIAL_UP_CHILLIAD			RETURN 70100	//Up Chilliad
			CASE ciFM_TIME_TRIAL_DOWN_CHILLIAD			RETURN 54200	//Down Chilliad
			CASE ciFM_TIME_TRIAL_MOUNT_GORDO			RETURN 46300	//Mount Gordo
			CASE ciFM_TIME_TRIAL_GREAT_OCEAN_HIGHWAY	RETURN 124900	//Great Ocean Highway
			CASE ciFM_TIME_TRIAL_OBSERVATORY			RETURN 124400	//Observatory
			CASE ciFM_TIME_TRIAL_COAST_TO_COAST			RETURN 149400	//Coast to Coast
			CASE ciFM_TIME_TRIAL_END_TO_END				RETURN 249500	//End to End
			CASE ciFM_TIME_TRIAL_SAWMILL				RETURN 135000	//Sawmill
			CASE ciFM_TIME_TRIAL_FORT_ZANCUDO			RETURN 104000	//Fort Zancudo
			CASE ciFM_TIME_TRIAL_DEL_PERRO_PIER			RETURN 103200	//Del Perro Pier
			CASE ciFM_TIME_TRIAL_STORM_DRAIN			RETURN 38500	//Storm Drain
			CASE ciFM_TIME_TRIAL_CYPRESS_FLATS			RETURN 127200	//Cypress Flats
			CASE ciFM_TIME_TRIAL_POWER_STATION			RETURN 86600	//Power Station
			CASE ciFM_TIME_TRIAL_CASINO					RETURN 60000	//Casino
			CASE ciFM_TIME_TRIAL_UP_N_ATOM				RETURN 101300	//Up-n-Atom
			CASE ciFM_TIME_TRIAL_RATON_CANYON			RETURN 76600	//Raton Canyon
			CASE ciFM_TIME_TRIAL_CALAFIA_WAY			RETURN 84200	//Calafia Way
			CASE ciFM_TIME_TRIAL_ROUTE_68				RETURN 79000	//Route 68
			CASE ciFM_TIME_TRIAL_TONGVA_VALLEY			RETURN 58800	//Tongva Valley
			CASE ciFM_TIME_TRIAL_LSIA					RETURN 103400	//LSIA
			CASE ciFM_TIME_TRIAL_VINEWOOD_BOWL			RETURN 178800	//Vinewood Bowl
			CASE ciFM_TIME_TRIAL_MAZE_BANK_ARENA		RETURN 77800	//Maze Bank Arena
			CASE ciFM_TIME_TRIAL_ELYSIAN_ISLAND			RETURN 100000
			CASE ciFM_TIME_TRIAL_GALILEO_PARK		    RETURN 125000
			CASE ciFM_TIME_TRIAL_STAB_CITY			    RETURN 120000
			CASE ciFM_TIME_TRIAL_VINEWOOD_HILLS	        RETURN 155000
			CASE ciFM_TIME_TRIAL_GROVE_STREET		    RETURN 80000
			CASE ciFM_TIME_TRIAL_LSIA_2			        RETURN 144000
			CASE ciFM_TIME_TRIAL_LOS_SANTOS_CORONER     RETURN 136000
			CASE ciFM_TIME_TRIAL_ELYSIAN_ISLAND_2	    RETURN 110000
			CASE ciFM_TIME_TRIAL_VINEWOOD_ESTATE	    RETURN 86000
			CASE ciFM_TIME_TRIAL_EL_BURRO_HEIGHTS	    RETURN 130000
		ENDSWITCH
	ENDIF
	
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF

	RETURN 0

ENDFUNC

FUNC VEHICLE_SEAT GET_PED_VEHICLE_SEAT_FOR_TIME_TRIAL(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex)

	IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_DRIVER) = PedIndex
		RETURN VS_DRIVER
	ENDIF

	IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_FRONT_RIGHT) = PedIndex
		RETURN VS_FRONT_RIGHT
	ENDIF

	IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_BACK_LEFT) = PedIndex
		RETURN VS_BACK_LEFT
	ENDIF

	IF GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_BACK_RIGHT) = PedIndex
		RETURN VS_BACK_RIGHT
	ENDIF
   
    RETURN VS_ANY_PASSENGER
     
ENDFUNC


/// PURPOSE: To check if the trial should end for reasons other than reaching the end point
///    
/// PARAMS:
///    sTTVarsStruct - The time trial data
/// RETURNS: TRUE if the trial should end, FALSE otherwise
///    
FUNC BOOL SHOULD_TRIAL_END(AMTT_VARS_STRUCT &sTTVarsStruct)

	//End if timeout
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTTVarsStruct.trialTimer) >= TIMEOUT_TIME
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_LOSER, "AMTT_FAIL","AMTT_TIMEOUT", HUD_COLOUR_RED,DEFAULT)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SHOULD_TRIAL_END - Timed out.")
		RETURN TRUE
	ENDIF

	//End if player dies
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_LOSER,"AMTT_FAIL","AMTT_DIED", HUD_COLOUR_RED)
		RETURN TRUE
	ENDIF

	// KGM 28/4/15 [BUG 2325536]: Allow Time Trial to be cancelled using the phone
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET()
		IF IS_VEHICLE_DRIVEABLE(sTTVarsStruct.trialVehicle)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sTTVarsStruct.trialVehicle) AND (GET_PED_VEHICLE_SEAT_FOR_TIME_TRIAL(PLAYER_PED_ID(), sTTVarsStruct.trialVehicle) = VS_DRIVER)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(sTTVarsStruct.trialVehicle)
					DETACH_VEHICLE_FROM_ANY_CARGOBOB(sTTVarsStruct.trialVehicle)
				ENDIF
				CLEAR_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_VEHICLE_TIMER)
				SET_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_UPDATE_OBJECTIVE_TEXT)
				RETURN FALSE
			//Left vehicle	
			ELSE 	
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR (IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sTTVarsStruct.trialVehicle) AND GET_PED_VEHICLE_SEAT_FOR_TIME_TRIAL(PLAYER_PED_ID(), sTTVarsStruct.trialVehicle) != VS_DRIVER)
					IF NOT IS_BIT_SET(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_VEHICLE_TIMER)
						REINIT_NET_TIMER(sTTVarsStruct.outOfVehicleTimer)
						SET_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_VEHICLE_TIMER)
						RETURN FALSE
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sTTVarsStruct.outOfVehicleTimer,sTTVarsStruct.iOutVehicleTime)
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_LOSER,"AMTT_FAIL","AMTT_LEFT", HUD_COLOUR_RED)
							CLEAR_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_VEHICLE_TIMER)
						ELSE
							SET_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_UPDATE_OBJECTIVE_TEXT)
							RETURN FALSE
						ENDIF
					ENDIF
				//Player in vehicle other than starting veh
				ELSE
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_LOSER,"AMTT_FAIL","AMTT_WRONGV", HUD_COLOUR_RED)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SHOULD_TRIAL_END - Entered different vehicle.")
				ENDIF
			ENDIF
		//Vehicle destroyed
		ELSE		
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_LOSER,"AMTT_FAIL","AMTT_DEST", HUD_COLOUR_RED)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SHOULD_TRIAL_END - Vehicle was destroyed.")
		ENDIF
	ENDIF
	
	IF (IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET())
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CLEANUP - IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET = TRUE (set by joblist)")
	ENDIF
	
	RETURN TRUE

ENDFUNC

PROC CLEANUP_TIME_TRIAL_CORONA_PROJECTION(AMTT_VARS_STRUCT &sTTVarsStruct)

	IF IS_BIT_SET(sTTVarsStruct.iLocalBS,LOCAL_BS_REQUESTED_CORONA_PROJ)

		IF (sTTVarsStruct.groundProj != NULL)
			DELETE_CHECKPOINT(sTTVarsStruct.groundProj)
			sTTVarsStruct.groundProj = NULL
		ENDIF
		
		UNPATCH_DECAL_DIFFUSE_MAP(DECAL_RSID_MP_CREATOR_TRIGGER)
		
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPMissMarkers256")
		
		CLEAR_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_REQUESTED_CORONA_PROJ)
	ENDIF

ENDPROC

/// PURPOSE:Draws the ground projection on the start corona and cleans up if out of range
///    
/// PARAMS:
///    sTTVarsStruct - The time trial data
///    vDrawPosition - The position to draw the corona
PROC DRAW_TIME_TRIAL_CORONA_PROJECTION(AMTT_VARS_STRUCT &sTTVarsStruct, VECTOR vDrawPosition)

	INT theRed, theGreen, theBlue, theAlpha
	FLOAT theSize = 2.6 * 2 * MATC_CORONA_PROJECTION_SIZE_MODIFIER

	IF NOT IS_BIT_SET(sTTVarsStruct.iLocalBS,LOCAL_BS_REQUESTED_CORONA_PROJ)
		REQUEST_STREAMED_TEXTURE_DICT("MPMissMarkers256")
		IF (HAS_STREAMED_TEXTURE_DICT_LOADED("MPMissMarkers256"))
			
			vDrawPosition.z += 1.0
			GET_GROUND_Z_FOR_3D_COORD(vDrawPosition, vDrawPosition.z)
			
			GET_HUD_COLOUR(HUD_COLOUR_PURPLE, theRed, theGreen, theBlue, theAlpha)
			theAlpha = MATC_CORONA_PROJECTION_ALPHA
			
			sTTVarsStruct.groundProj = CREATE_CHECKPOINT(CHECKPOINT_MP_CREATOR_TRIGGER, vDrawPosition ,vDrawPosition ,theSize, theRed, theGreen, theBlue, theAlpha)
			PATCH_DECAL_DIFFUSE_MAP(DECAL_RSID_MP_CREATOR_TRIGGER, "MPMissMarkers256", "TimeTrial_Icon")
			
			SET_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_REQUESTED_CORONA_PROJ)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === DRAW_TIME_TRIAL_CORONA_PROJECTION - SET_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_REQUESTED_CORONA_PROJ)")
		ENDIF
	ENDIF
			
ENDPROC

/// PURPOSE: Draws the text on the start corona and cleans up if out of range
///    
/// PARAMS:
///    sTTVarsStruct - The time trial data
///    vDrawPosition - The position to draw the corona
PROC DRAW_TIME_TRIAL_CORONA_TEXT(AMTT_VARS_STRUCT &sTTVarsStruct, VECTOR vDrawPosition)

	ENTITY_INDEX distEntity
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			distEntity = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ELSE
			distEntity = PLAYER_PED_ID()
		ENDIF
	
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(distEntity,vDrawPosition) <= 15
		
			IF NOT IS_BIT_SET(sTTVarsStruct.iLocalBS,LOCAL_BS_REQUESTED_CORONA_TEXT)
				IF REQUEST_LOCATE_MESSAGE(sTTVarsStruct.theMessageID)
					TEXT_LABEL_23 missionTitle
					#IF FEATURE_STUNT_FM_EVENTS
					IF IS_STUNT_TRIAL(sTTVarsStruct)
						missionTitle = "AMTT_STRIAL"
					ELSE
					#ENDIF
						missionTitle = "AMTT_TRIAL"
					#IF FEATURE_STUNT_FM_EVENTS
					ENDIF
					#ENDIF
					TEXT_LABEL_63 secondLine = GET_TIME_TRIAL_ROUTE_STRING(sTTVarsStruct.iVariation#IF FEATURE_STUNT_FM_EVENTS, sTTVarsStruct.bStunt #ENDIF)
	//				VEHICLE_CLASS vehClass = GET_TIME_TRIAL_VEHICLE_CLASS()
	//				TEXT_LABEL_63 vehClassString = GET_TIME_TRIAL_VEHICLE_CLASS_STRING(vehClass)
					
					CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === DRAW_TIME_TRIAL_CORONA_TEXT ",sTTVarsStruct.theMessageID)//," Vehicle Class: ",vehClassString)
					
					STRING tempHolderForTimeString = "AMTT_STARTPAR"
					INT tempHolderForTimeAsMsec = GET_BEST_TRIAL_TIME(sTTVarsStruct.iVariation#IF FEATURE_STUNT_FM_EVENTS, sTTVarsStruct.bStunt #ENDIF)
					
					// Store_Locate_Message_Details_For_SCTV() within net_mision_locate_message.sch handles if the paramMissionID is a new mode not handled
					STORE_LOCATE_MESSAGE_DETAILS(sTTVarsStruct.theMessageID,eAM_TIME_TRIAL,NO_MISSION_VARIATION,missionTitle,HUD_COLOUR_PURPLE,secondLine,FALSE,DEFAULT,-1,
												DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, tempHolderForTimeString, tempHolderForTimeAsMsec)
					SET_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_REQUESTED_CORONA_TEXT)
				ENDIF
			ENDIF
			
			IF NOT (IS_LOCAL_PLAYER_SPECTATOR())
				IF IS_BIT_SET(sTTVarsStruct.iLocalBS,LOCAL_BS_REQUESTED_CORONA_TEXT)
					DISPLAY_LOCATE_MESSAGE(sTTVarsStruct.theMessageID,sTTVarsStruct.vStartPoint)
				ENDIF
			ENDIF
			
			DRAW_TIME_TRIAL_CORONA_PROJECTION(sTTVarsStruct,vDrawPosition)
			
			INT iRed,iGreen,iBlue,iAlpha
			VECTOR vPointAt = NORMALISE_VECTOR(sTTVarsStruct.vStartDirection-vDrawPosition)
			GET_HUD_COLOUR(HUD_COLOUR_PURPLE,iRed,iGreen,iBlue,iAlpha)
			DRAW_MARKER(MARKER_ARROW,<<vDrawPosition.x,vDrawPosition.y,vDrawPosition.z + 3>>,vPointAt,<<89.999, 90, 0>>,<<1,1,1>>,iRed,iGreen,iBlue,iAlpha)
			
		ELSE
			CLEANUP_TIME_TRIAL_CORONA_PROJECTION(sTTVarsStruct)
			CLEAR_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_REQUESTED_CORONA_TEXT)
		ENDIF
	
	ENDIF

ENDPROC

//PURPOSE: Retursn TRUe if player is not in Freemode or is in Freemode and passes the checks.
FUNC BOOL SHOULD_SHOW_TIME_TRIAL_BLIP()
      IF NETWORK_IS_IN_TUTORIAL_SESSION()
            RETURN FALSE
      ENDIF
      
      IF NETWORK_IS_ACTIVITY_SESSION()
            RETURN FALSE
      ENDIF
      
      IF HIDE_BLIP_BECAUSE_PLAYER_IS_STARTING_RACE()
            RETURN FALSE
      ENDIF
      
      IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT)
      OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RACETOPOINT)
            RETURN FALSE
      ENDIF
      
      IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), TRUE) 
            RETURN FALSE
      ENDIF
	  
	  IF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE)
	  	RETURN FALSE
	  ENDIF
	  
	  
      
      RETURN TRUE
ENDFUNC

/// PURPOSE: Draws the blip and corona for the start of the time trial
///    
/// PARAMS:
///    sTTVarsStruct - The time trial data
///    vDrawPosition - The position to draw the corona/blip
PROC DRAW_START_UI(AMTT_VARS_STRUCT &sTTVarsStruct, VECTOR vDrawPosition)
	
	IF sTTVarsStruct.eAMTT_Stage = AMTT_WAIT
	AND SHOULD_SHOW_TIME_TRIAL_BLIP()
		
		IF NOT DOES_BLIP_EXIST(sTTVarsStruct.destinationBlip)
			IF NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, 0>>)
			AND NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, -2000>>)
								
				sTTVarsStruct.destinationBlip = ADD_BLIP_FOR_COORD(vDrawPosition)
				
				SET_BLIP_SPRITE(sTTVarsStruct.destinationBlip, RADAR_TRACE_TEMP_2)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(sTTVarsStruct.destinationBlip,HUD_COLOUR_NET_PLAYER2)

				SET_BLIP_PRIORITY(sTTVarsStruct.destinationBlip, BLIPPRIORITY_MED)
				#IF FEATURE_STUNT_FM_EVENTS
				IF IS_STUNT_TRIAL(sTTVarsStruct)
					SET_BLIP_NAME_FROM_TEXT_FILE(sTTVarsStruct.destinationBlip, "AMTT_STUBLIP")
				ELSE
				#ENDIF
					SET_BLIP_NAME_FROM_TEXT_FILE(sTTVarsStruct.destinationBlip, "AMTT_BLIP")
				#IF FEATURE_STUNT_FM_EVENTS
				ENDIF
				#ENDIF
				SET_BLIP_AS_MISSION_CREATOR_BLIP(sTTVarsStruct.destinationBlip, TRUE)
				SET_BLIP_AS_SHORT_RANGE(sTTVarsStruct.destinationBlip,TRUE)
				
				sTTVarsStruct.telemetryStruct.m_notifiedTime = GET_CLOUD_TIME_AS_INT()
				PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(sTTVarsStruct.iHashedMac, sTTVarsStruct.iMatchHistoryId)
				
				CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === DRAW_START_UI BLIP ADDED AT ",vDrawPosition)
			ENDIF
		ENDIF
		
		//IF NOT (IS_LOCAL_PLAYER_SPECTATOR())
			Draw_SCTV_Corona(vDrawPosition, HUD_COLOUR_PURPLE, ciIN_LOCATE_DISTANCE)
			DRAW_TIME_TRIAL_CORONA_TEXT(sTTVarsStruct,vDrawPosition)
		//ENDIF
		
		//CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === DRAW_START_UI - Draw_SCTV_Corona at ",vDrawPosition)
	ELSE
		IF DOES_BLIP_EXIST(sTTVarsStruct.destinationBlip)
			REMOVE_BLIP(sTTVarsStruct.destinationBlip)					
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === BLIP AND CORONA REMOVED")
		ENDIF
		CLEAR_TIME_TRIAL_HELP(#IF FEATURE_STUNT_FM_EVENTS sTTVarsStruct.bStunt #ENDIF)
	ENDIF

ENDPROC


/// PURPOSE: Draws the objective text for the trial
///    
/// PARAMS:
///    sTTVarsStruct - The time trial data
PROC DRAW_TRIAL_OBJECTIVE_TEXT(AMTT_VARS_STRUCT &sTTVarsStruct)
	//Objective Text
	IF IS_BIT_SET(sTTVarsStruct.iLocalBS,LOCAL_BS_UPDATE_OBJECTIVE_TEXT)
	
		IF IS_BIT_SET(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_VEHICLE_TIMER)
			IF NOT Has_This_MP_Objective_Text_Been_Received("AMTT_RETURN") 
				PRINTLN("Print_Objective_Text : ","AMTT_RETURN") 
				
				Print_Objective_Text("AMTT_RETURN") 
			ENDIF
		ELSE
			IF NOT Has_This_MP_Objective_Text_Been_Received("AMTT_GOTO") 
				PRINTLN("Print_Objective_Text : ","AMTT_GOTO") 
				Print_Objective_Text("AMTT_GOTO") 
			ENDIF
		ENDIF
		
		CLEAR_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_UPDATE_OBJECTIVE_TEXT)
	ENDIF
ENDPROC

FUNC HUD_COLOURS GET_TRIAL_TIMER_COLOUR(INT iTimeToBeat, INT iCurrentTime)

	IF iCurrentTime > iTimeToBeat
		RETURN HUD_COLOUR_RED
	ELSE
		INT iDiff = (iTimeToBeat - iCurrentTime)

		IF iDiff <= 10000 AND iDiff > 0
			RETURN HUD_COLOUR_RED
		ELSE
			RETURN HUD_COLOUR_WHITE
		ENDIF
	ENDIF

ENDFUNC

FUNC HUDFLASHING GET_FLASHING_COLOUR(INT iTarget, INT iCurrent)
	
	INT iDiff = (iTarget - iCurrent)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === GET_FLASHING_COLOUR - iDiff =",iDiff)
	IF iDiff <= 10000 AND iDiff > 0
		RETURN HUDFLASHING_FLASHRED
	ELSE
		RETURN HUDFLASHING_NONE
	ENDIF

ENDFUNC

FUNC INT GET_FLASHING_TIME(INT iTarget, INT iCurrent, BOOL &bStartedSound, INT &iSoundID)
	
	INT iDiff = (iTarget - iCurrent)
	//CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === GET_FLASHING_TIME - iDiff =",iDiff)
	IF iDiff <= 10000 AND iDiff > 0
		IF NOT bStartedSound	
			IF HAS_SOUND_FINISHED(iSoundID)
				 PLAY_SOUND_FRONTEND(iSoundID, "Timer_10s","GTAO_FM_Events_Soundset",FALSE)
				 SET_VARIABLE_ON_SOUND(iSoundID, "Custom_Counter", TO_FLOAT(iDiff/1000))
				 bStartedSound = TRUE
				 CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === GET_FLASHING_TIME - PLAY_SOUND_FRONTEND - Timer_Single")
			ENDIF
		ENDIF
		RETURN 10000
	ELSE
		RETURN 0
	ENDIF

ENDFUNC


/// PURPOSE: Handles drawing all timers for the trial
///    
/// PARAMS:
///    trialTimer - The timer used to record your trial time
///    sTTVarsStruct - The time trial data
PROC DRAW_EVENT_TIMER(SCRIPT_TIMER trialTimer, AMTT_VARS_STRUCT &sTTVarsStruct)

	IF NOT IS_BIT_SET(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_VEHICLE_TIMER)
		
		IF IS_BIT_SET(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_TIMER_FLASH)
			STOP_SOUND(sTTVarsStruct.iSoundID)
			CLEAR_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_TIMER_FLASH)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CLEAR_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_TIMER_FLASH)")
		ENDIF

		INT iCurrentTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(trialTimer)

		HUD_COLOURS colour1 = GET_TRIAL_TIMER_COLOUR(GET_BEST_TRIAL_TIME(sTTVarsStruct.iVariation),iCurrentTime)
		HUD_COLOURS colour2 = GET_TRIAL_TIMER_COLOUR(sTTVarsStruct.iPersonalBest,iCurrentTime)
		
		//Show best as 0 if you don't have one yet
		INT iPersonalBest
		IF sTTVarsStruct.iPersonalBest = HIGHEST_INT
			iPersonalBest = 0
		ELSE
			iPersonalBest = sTTVarsStruct.iPersonalBest
		ENDIF

		//Par Time/Personal Best/Current Time
		DRAW_GENERIC_TIMER(GET_BEST_TRIAL_TIME(sTTVarsStruct.iVariation), "AMTT_PAR", 0, TIMER_STYLE_USEMILLISECONDS, GET_FLASHING_TIME(GET_BEST_TRIAL_TIME(sTTVarsStruct.iVariation),iCurrentTime,sTTVarsStruct.bStartedSound,sTTVarsStruct.iSoundID), DEFAULT, HUDORDER_THIRDBOTTOM,DEFAULT,colour1,DEFAULT,DEFAULT,DEFAULT,colour1)
		DRAW_GENERIC_TIMER(iPersonalBest, "AMTT_BEST", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_SECONDBOTTOM,DEFAULT,colour2,DEFAULT,DEFAULT,DEFAULT,colour2)
		DRAW_GENERIC_TIMER(IMAX(0,iCurrentTime), "TIMER_TIME", 0, TIMER_STYLE_USEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_BOTTOM)
	
		IF IS_LANGUAGE_NON_ROMANIC()
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
		ELSE
			SET_MIDDLE_TITLE_POSITION_HUD_THIS_FRAME()
		ENDIF
	
	ELSE
		INT iTimeLeft = (sTTVarsStruct.iOutVehicleTime-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTTVarsStruct.outOfVehicleTimer))
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === Player out of vehicle - timer = ",sTTVarsStruct.iOutVehicleTime-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTTVarsStruct.outOfVehicleTimer)," = ",iTimeLeft)
				
		IF NOT IS_BIT_SET(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_TIMER_FLASH)		
			DRAW_GENERIC_TIMER(iTimeLeft, "TIMER_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, DEFAULT, HUDORDER_BOTTOM,DEFAULT,HUD_COLOUR_RED,DEFAULT,DEFAULT,DEFAULT,HUD_COLOUR_RED)
			SET_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_TIMER_FLASH)	
			STOP_SOUND(sTTVarsStruct.iSoundID)
			sTTVarsStruct.bStartedSound = FALSE
			PLAY_SOUND_FRONTEND(sTTVarsStruct.iSoundID, "Return_To_Vehicle_Timer","GTAO_FM_Events_Soundset",FALSE)
			SET_VARIABLE_ON_SOUND(sTTVarsStruct.iSoundID, "Custom_Counter", TO_FLOAT(sTTVarsStruct.iOutVehicleTime/1000))
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === SET_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_TIMER_FLASH)")
		ELSE
			DRAW_GENERIC_TIMER(iTimeLeft, "TIMER_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, DEFAULT, HUDORDER_BOTTOM,DEFAULT,HUD_COLOUR_RED,DEFAULT,DEFAULT,DEFAULT,HUD_COLOUR_RED)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === Draw Out of Vehicle Timer")
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Converts a time to a format for display
///    
/// PARAMS:
///    iTime - The network time in milliseconds
/// RETURNS: The time in seconds
///    
FUNC INT FORMAT_TIME_FOR_DISPLAY(INT iTime)
	FLOAT fTime = TO_FLOAT(iTime) / 1000.0
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === FORMAT_TIME_FOR_DISPLAY = ",fTime)
	RETURN fTime
ENDFUNC

FUNC BOOL SHOULD_USE_SHORT_CHECKPOINTS(INT iVariation)

	SWITCH(iVariation)
	
		CASE ciFM_TIME_TRIAL_LSIA RETURN TRUE
		
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

FUNC CHECKPOINT_INDEX CREATE_TIME_TRIAL_CHECKPOINT(AMTT_VARS_STRUCT &sTTVarsStruct, VECTOR vDrawPosition)

	CHECKPOINT_INDEX checkpoint

	INT iR, iG, iB, iA	
	
	#IF FEATURE_STUNT_FM_EVENTS
	IF NOT IS_END_CHECKPOINT(sTTVarsStruct)
		INT iR2, iG2, iB2, iA2	
		GET_HUD_COLOUR(HUD_COLOUR_YELLOWLIGHT, iR, iG, iB, iA)
		GET_HUD_COLOUR(HUD_COLOUR_NORTH_BLUE, iR2, iG2, iB2, iA2)
		VECTOR vNextCoords = GET_NEXT_CHECKPOINT_DRAW_LOCATION(sTTVarsStruct,TRUE)
		
		checkpoint = CREATE_CHECKPOINT( CHECKPOINT_RACE_GROUND_CHEVRON_1,(vDrawPosition+<<0,0,2>>),vNextCoords+<<0,0,fPOINT_AT_INCREASE>>,FMMC_CHECKPOINT_SIZE*1*0.66,iR, iG, iB,75)
		SET_CHECKPOINT_RGBA2(checkpoint,iR2, iG2, iB2, iA2)
		SET_CHECKPOINT_CYLINDER_HEIGHT(checkpoint,9.5,9.5,100)
		
	ELSE
	#ENDIF

	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)

	IF NOT SHOULD_USE_SHORT_CHECKPOINTS(sTTVarsStruct.iVariation)					
		checkpoint = CREATE_CHECKPOINT(CHECKPOINT_RACE_GROUND_FLAG, vDrawPosition+<<0.0, 0.0, CHECKPOINT_Z_POS_AIR>>, vDrawPosition, CHECKPOINT_SIZE_AIR ,iR, iG, iB, 75)
	ELSE
		checkpoint = CREATE_CHECKPOINT(CHECKPOINT_RACE_GROUND_FLAG, vDrawPosition+<<0.0, 0.0, CHECKPOINT_Z_POS_AIR +3.5>>, vDrawPosition, CHECKPOINT_SIZE_AIR ,iR, iG, iB, 75)
	ENDIF
	SET_CHECKPOINT_CYLINDER_HEIGHT(checkpoint, CHECKPOINT_CYLINDER_AIR, CHECKPOINT_CYLINDER_AIR, 100)
	
	#IF FEATURE_STUNT_FM_EVENTS
	ENDIF
	#ENDIF

	RETURN checkpoint

ENDFUNC

/// PURPOSE: Draws the UI for the time trial while it is in progress
///    
/// PARAMS:
///    sTTVarsStruct - The time trial data
///    vDrawPosition - The position to draw the destination at
PROC DRAW_TRIAL_UI(AMTT_VARS_STRUCT &sTTVarsStruct, VECTOR vDrawPosition)
	
	IF sTTVarsStruct.eAMTT_Stage = AMTT_GOTO	
		
		IF NOT IS_BIT_SET(sTTVarsStruct.iLocalBS,LOCAL_BS_RESET_VEHICLE_TIMER)
		
			IF DOES_BLIP_EXIST(sTTVarsStruct.vehicleBlip)
				REMOVE_BLIP(sTTVarsStruct.vehicleBlip)	
				CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === VEHICLE BLIP REMOVED 1")
			ENDIF
		
			IF NOT DOES_BLIP_EXIST(sTTVarsStruct.destinationBlip)
				IF NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, 0>>)
				AND NOT ARE_VECTORS_EQUAL(vDrawPosition, <<0, 0, -2000>>)
									
					sTTVarsStruct.destinationBlip = ADD_BLIP_FOR_COORD(vDrawPosition)
				
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(sTTVarsStruct.destinationBlip,HUD_COLOUR_YELLOW)
					
					SET_BLIP_PRIORITY(sTTVarsStruct.destinationBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
					SET_BLIP_NAME_FROM_TEXT_FILE(sTTVarsStruct.destinationBlip, "AMTT_DESTIN")
					
					#IF FEATURE_STUNT_FM_EVENTS					
					IF NOT IS_END_CHECKPOINT(sTTVarsStruct)
						sTTVarsStruct.nextCheckpointBlip = ADD_BLIP_FOR_COORD(GET_NEXT_CHECKPOINT_DRAW_LOCATION(sTTVarsStruct,TRUE))
						SET_BLIP_PRIORITY(sTTVarsStruct.nextCheckpointBlip ,  BLIPPRIORITY_HIGH)
						SET_BLIP_COLOUR(sTTVarsStruct.nextCheckpointBlip,BLIP_COLOUR_YELLOW)
						SET_BLIP_SCALE(sTTVarsStruct.nextCheckpointBlip, BLIP_SIZE_NETWORK_CHECKPOINT_HIT)
						SET_BLIP_NAME_FROM_TEXT_FILE(sTTVarsStruct.nextCheckpointBlip,"FMMC_B_6")
					ENDIF
					#ENDIF
					
					sTTVarsStruct.destination = CREATE_TIME_TRIAL_CHECKPOINT(sTTVarsStruct,vDrawPosition)
					
					SET_BIT(sTTVarsStruct.iLocalBS,LOCAL_BS_UPDATE_OBJECTIVE_TEXT)
					
					CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === DRAW_TRIAL_UI BLIP ADDED AT ",vDrawPosition)
				ENDIF
			ELSE
				IF NOT ARE_VECTORS_EQUAL(vDrawPosition, GET_BLIP_COORDS(sTTVarsStruct.destinationBlip))
					SET_BLIP_COORDS(sTTVarsStruct.destinationBlip,vDrawPosition)
					
					#IF FEATURE_STUNT_FM_EVENTS
					IF NOT IS_END_CHECKPOINT(sTTVarsStruct)
						SET_BLIP_COORDS(sTTVarsStruct.nextCheckpointBlip,GET_NEXT_CHECKPOINT_DRAW_LOCATION(sTTVarsStruct,TRUE))
					ENDIF
					#ENDIF
					
					DELETE_CHECKPOINT(sTTVarsStruct.destination)
					sTTVarsStruct.destination = CREATE_TIME_TRIAL_CHECKPOINT(sTTVarsStruct,vDrawPosition)
					CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === DRAW_TRIAL_UI - UPDATING BLIP AND CHECKPOINT TO ",vDrawPosition)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(sTTVarsStruct.destinationBlip)
				REMOVE_BLIP(sTTVarsStruct.destinationBlip)
				DELETE_CHECKPOINT(sTTVarsStruct.destination)
	
				CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === BLIP AND END POINT REMOVED 1")
			ENDIF
			
			#IF FEATURE_STUNT_FM_EVENTS
			IF DOES_BLIP_EXIST(sTTVarsStruct.nextCheckpointBlip)
				REMOVE_BLIP(sTTVarsStruct.nextCheckpointBlip)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === NEXT BLIP REMOVED 1")
			ENDIF
			#ENDIF
			
			IF NOT DOES_BLIP_EXIST(sTTVarsStruct.vehicleBlip)
						
				sTTVarsStruct.vehicleBlip = ADD_BLIP_FOR_ENTITY(sTTVarsStruct.trialVehicle)
			
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(sTTVarsStruct.vehicleBlip,HUD_COLOUR_BLUE)
				
				SET_BLIP_PRIORITY(sTTVarsStruct.vehicleBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
				SET_BLIP_NAME_FROM_TEXT_FILE(sTTVarsStruct.vehicleBlip, "AMTT_VEHBLIP")
				
				CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === DRAW_TRIAL_UI VEHICLE BLIP ADDED")
			ENDIF	
		ENDIF
		
		DRAW_TRIAL_OBJECTIVE_TEXT(sTTVarsStruct)
		DRAW_EVENT_TIMER(sTTVarsStruct.trialTimer,sTTVarsStruct)
	ELSE
		IF DOES_BLIP_EXIST(sTTVarsStruct.destinationBlip)
			REMOVE_BLIP(sTTVarsStruct.destinationBlip)
			DELETE_CHECKPOINT(sTTVarsStruct.destination)		
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === BLIP AND END POINT REMOVED 2")
		ENDIF
		IF DOES_BLIP_EXIST(sTTVarsStruct.vehicleBlip)
			REMOVE_BLIP(sTTVarsStruct.vehicleBlip)	
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === VEHICLE BLIP REMOVED 2")
		ENDIF
		#IF FEATURE_STUNT_FM_EVENTS
		IF DOES_BLIP_EXIST(sTTVarsStruct.nextCheckpointBlip)
			REMOVE_BLIP(sTTVarsStruct.nextCheckpointBlip)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === NEXT BLIP REMOVED 2")
		ENDIF
		#ENDIF
		Clear_Any_Objective_Text_From_This_Script()	
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_AT_END_POINT(AMTT_VARS_STRUCT &sTTVarsStruct)

	FLOAT checkpointRadiusHeight,checkpointRadius,checkpointZPos
	
	checkpointRadiusHeight = CHECKPOINT_RADIUS_HEIGHT_AIR
	checkpointRadius = CHECKPOINT_RADIUS_AIR
	IF NOT SHOULD_USE_SHORT_CHECKPOINTS(sTTVarsStruct.iVariation)
		checkpointZPos = CHECKPOINT_Z_POS_AIR
	ELSE
		checkpointZPos = CHECKPOINT_Z_POS_AIR + 3.5
	ENDIF
	
	VECTOR vCoord = GET_NEXT_CHECKPOINT_DRAW_LOCATION(sTTVarsStruct)
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vCoord +<<0, 0, checkpointZPos>>, <<checkpointRadius, checkpointRadius, checkpointRadiusHeight>>,DEFAULT,DEFAULT,TM_IN_VEHICLE)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sTTVarsStruct.trialVehicle)
		IF IS_END_CHECKPOINT(sTTVarsStruct)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === IS_PLAYER_AT_END_POINT - At end, returning true")
			RETURN TRUE
		ELSE
			#IF FEATURE_STUNT_FM_EVENTS
			sTTVarsStruct.iNextCheckpoint++
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === IS_PLAYER_AT_END_POINT - Collected checkpoint, next is ",sTTVarsStruct.iNextCheckpoint)
			#ENDIF
		ENDIF
		//Play sound
		PLAY_SOUND_FRONTEND(-1,"Checkpoint_Hit","GTAO_FM_Events_Soundset", FALSE)
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC FLOAT GET_TIME_SCALE_FOR_DEFAULT_REWARD(INT iParTime)
	
	FLOAT fParTime = TO_FLOAT(iParTime)
	INT iMins = FLOOR((fParTime /1000) /60)
	FLOAT fMultiplier = TO_FLOAT(IMIN(IMAX(iMins,MIN_REWARD_MINUTES),g_sMPTunables.iTIME_TRIAL_PARTICIPATION_T_CAP))

	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === [REWARDS] GET_TIME_SCALE_FOR_DEFAULT_REWARD - iParTime = ",iParTime, " multiplier = ",fMultiplier)
	RETURN fMultiplier

ENDFUNC

/// PURPOSE:
///		1.0 - Cash reward multiplier default.
FUNC FLOAT GET_EOM_CASH_MULTIPLIER()

	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === [REWARDS] GET_EOM_CASH_MULTIPLIER - ",g_sMPTunables.fTime_Trial_Event_Multiplier_Cash)
	RETURN g_sMPTunables.fTime_Trial_Event_Multiplier_Cash

ENDFUNC

/// PURPOSE:
///		1.0 - RP reward multiplier default.
FUNC FLOAT GET_EOM_RP_MULTIPLIER()

	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === [REWARDS] GET_EOM_RP_MULTIPLIER - ",g_sMPTunables.fTime_Trial_Event_Multiplier_RP)
	RETURN g_sMPTunables.fTime_Trial_Event_Multiplier_RP

ENDFUNC

/// PURPOSE:
///		1,000 - Cash reward default.
FUNC FLOAT GET_EOM_DEFAULT_CASH_REWARD()
	FLOAT fTemp
	fTemp = TO_FLOAT(g_sMPTunables.itime_trial_eom_default_cash_reward)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === [REWARDS] GET_EOM_DEFAULT_CASH_REWARD - ",fTemp)
	RETURN fTemp
ENDFUNC

/// PURPOSE:
///		100 - RP reward default.
FUNC FLOAT GET_EOM_DEFAULT_RP_REWARD()
	FLOAT fTemp
	fTemp = TO_FLOAT(g_sMPTunables.itime_trial_eom_default_rp_reward)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === [REWARDS] GET_EOM_DEFAULT_RP_REWARD - ",fTemp)
	RETURN fTemp
ENDFUNC

/// PURPOSE:
///		50,000 - Cash reward for completing the challenge.
FUNC FLOAT GET_CASH_REWARD_COMPLETED()
	FLOAT fTemp
	fTemp = TO_FLOAT(g_sMPTunables.itime_trial_cash_reward_completed)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === [REWARDS] GET_CASH_REWARD_COMPLETED - ",fTemp)
	RETURN fTemp 
ENDFUNC

/// PURPOSE:
///		5,000 - RP reward for completing the challenge.
FUNC FLOAT GET_RP_REWARD_COMPLETED()
	FLOAT fTemp
	fTemp = TO_FLOAT(g_sMPTunables.itime_trial_rp_reward_completed)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === [REWARDS] GET_RP_REWARD_COMPLETED - ",fTemp)
	RETURN fTemp
ENDFUNC

PROC GIVE_REWARDS(INT iCash, INT iRP)

	GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID(),"",XPTYPE_COMPLETE,XPCATEGORY_COMPLETE_TIME_TRIAL,iRP)
		
	IF USE_SERVER_TRANSACTIONS()
		INT iScriptTransactionIndex
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_AMBIENT_JOB_TIME_TRIAL, iCash, iScriptTransactionIndex, FALSE, TRUE) //Add new service
	ELSE
		AMBIENT_JOB_DATA amData
		NETWORK_EARN_FROM_AMBIENT_JOB(iCash,"TIME_TRIALS",amData)
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === [REWARDS] GIVE_REWARDS - Awarding $",iCash, " & RP: ",iRP)

ENDPROC

PROC PROCESS_TIME_TRIAL_END(AMTT_VARS_STRUCT &sTTVarsStruct)

	SAVE_TIME_TRIAL_STATS_TO_LEADERBOARD(sTTVarsStruct)
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === Finish time is ",sTTVarsStruct.iFinishTime)
	
	FLOAT fCashGiven = GET_EOM_DEFAULT_CASH_REWARD()
	FLOAT fRPGiven = GET_EOM_DEFAULT_RP_REWARD()
	INT iParTime = GET_BEST_TRIAL_TIME(sTTVarsStruct.iVariation #IF FEATURE_STUNT_FM_EVENTS, sTTVarsStruct.bStunt #ENDIF)
	FLOAT fTimeMultiplier = GET_TIME_SCALE_FOR_DEFAULT_REWARD(iParTime)
	
	fCashGiven *= fTimeMultiplier
	fRPGiven *= fTimeMultiplier
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === [REWARDS] PROCESS_TIME_TRIAL_END - After time muliplier of ",fTimeMultiplier," iCashGiven = ",fCashGiven," iRPGiven = ",fRPGiven)
	
	//Beat Time Trial
	IF sTTVarsStruct.iFinishTime < iParTime
	
		INT iPrevious = GET_TIME_TRIAL_COMPLETED_WEEK_STAT(sTTVarsStruct)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === PROCESS_TIME_TRIAL_END - Previous trial - ",iPrevious," Current trial - ",sTTVarsStruct.iVariation)
		
		//First time this week
		IF iPrevious != sTTVarsStruct.iVariation 
		
			//Rewards
			fCashGiven += GET_CASH_REWARD_COMPLETED()
			fRPGiven += GET_RP_REWARD_COMPLETED()
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === [REWARDS] PROCESS_TIME_TRIAL_END - After completion rewards iCashGiven = ",fCashGiven," iRPGiven = ",fRPGiven)
			
			//Stats and message
			SET_TIME_TRIAL_COMPLETED_WEEK_STAT(sTTVarsStruct,sTTVarsStruct.iVariation)
			SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_TIME_TRIAL_WON_TIME_MSECS,"","AMTT_TIME","AMTT_RECORD",DEFAULT,sTTVarsStruct.iFinishTime, DEFAULT, DEFAULT)
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === PROCESS_TIME_TRIAL_END - Marking trial as complete - ",sTTVarsStruct.iVariation)
		ELSE
			//New Personal Best
			IF sTTVarsStruct.iFinishTime < sTTVarsStruct.iPersonalBest
				SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_TIME_TRIAL_WON_TIME_MSECS,"","AMTT_TIME","AMTT_PERS",DEFAULT,sTTVarsStruct.iFinishTime, DEFAULT, DEFAULT)
				CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === PROCESS_TIME_TRIAL_END - New Personal Best!")
			ELSE
				SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_TIME_TRIAL_WON_TIME_MSECS,"","AMTT_TIME","AMTT_RECORD",DEFAULT,sTTVarsStruct.iFinishTime, DEFAULT, DEFAULT)
			ENDIF
			
			CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === PROCESS_TIME_TRIAL_END - Trial complete. Already completed this week - ",sTTVarsStruct.iVariation)
		ENDIF
		
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === PROCESS_TIME_TRIAL_END - Beat time!")
	ELSE
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === PROCESS_TIME_TRIAL_END - Fail!")
		#IF FEATURE_STUNT_FM_EVENTS
		IF sTTVarsStruct.bStunt
			SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_TIME_TRIAL_FAIL_TIME_MSECS,"","AMTT_TIME","AMTT_SEND",DEFAULT,sTTVarsStruct.iFinishTime,DEFAULT,DEFAULT,HUD_COLOUR_RED)
		ELSE
		#ENDIF
			SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_TIME_TRIAL_FAIL_TIME_MSECS,"","AMTT_TIME","AMTT_END",DEFAULT,sTTVarsStruct.iFinishTime,DEFAULT,DEFAULT,HUD_COLOUR_RED)
		#IF FEATURE_STUNT_FM_EVENTS
		ENDIF
		#ENDIF
	ENDIF
	
	sTTVarsStruct.iTelParTimeBeaten = iParTime
	
	//New Personal Best
	IF sTTVarsStruct.iFinishTime < sTTVarsStruct.iPersonalBest
		sTTVarsStruct.iPersonalBest = sTTVarsStruct.iFinishTime
		SET_TIME_TRIAL_BEST_TIME_STAT(sTTVarsStruct,sTTVarsStruct.iPersonalBest)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === PROCESS_TIME_TRIAL_END - New personal best = ",sTTVarsStruct.iPersonalBest)
	ENDIF
		
	fCashGiven *= GET_EOM_CASH_MULTIPLIER()
	fRPGiven *= GET_EOM_RP_MULTIPLIER()
	CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === [REWARDS] PROCESS_TIME_TRIAL_END - After tunable mulipliers iCashGiven = ",fCashGiven," iRPGiven = ",fRPGiven)
		
	INT iCashGiven = ROUND(fCashGiven)
	INT iRPGiven = ROUND(fRPGiven)
	//Gives the cash and RP rewards
	GIVE_REWARDS(iCashGiven, iRPGiven)
	
	sTTVarsStruct.telemetryStruct.m_cashEarned = iCashGiven
	sTTVarsStruct.telemetryStruct.m_rpEarned = iRPGiven
	sTTVarsStruct.telemetryStruct.m_timeTakenToComplete = GET_CLOUD_TIME_AS_INT() - sTTVarsStruct.telemetryStruct.m_startTime
	sTTVarsStruct.iTelVehicleType = ENUM_TO_INT(GET_TIME_TRIAL_VEHICLE_CLASS())
	sTTVarsStruct.telemetryStruct.m_uid0 = sTTVarsStruct.iHashedMac 
	sTTVarsStruct.telemetryStruct.m_uid1 = sTTVarsStruct.iMatchHistoryId
	PROCESS_CURRENT_AMBIENT_EVENT_PLAYSTATS(sTTVarsStruct.telemetryStruct, sTTVarsStruct.iTelVehicleType, sTTVarsStruct.iTelParTimeBeaten, FMMC_TYPE_TIME_TRIAL)
	
	//Send ticker to other players in Freemode
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_COMPLETED_TIME_TRIAL
	TickerEventData.playerID = PLAYER_ID()
	TickerEventData.dataInt = sTTVarsStruct.iFinishTime
	TickerEventData.dataInt2 = iParTime
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(FALSE))
	
ENDPROC

FUNC BOOL IS_OK_TO_PRINT_TIME_TRIAL_HELP()
		
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()		
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		RETURN FALSE
	ENDIF

	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
		
	IF IS_MISSION_SUMMARY_SCREEN_DISPLAYED()
		
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())		
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())		
		RETURN FALSE
	ENDIF
	

	IF g_Show_Lap_Dpad // Chris' dpad-down player list		
		RETURN FALSE
	ENDIF
	
	IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()		
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_SESSION_LAUNCHING()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()		
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		RETURN FALSE
	ENDIF
	
	IF g_bAtmShowing		
		
		//-- Atm front-end on screen
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC INIT_TRIAL_COMPLETED_STAT(AMTT_VARS_STRUCT &sTTVarsStruct)

	IF NOT GET_TIME_TRIAL_DONE_STAT_INIT_STAT(sTTVarsStruct)
		IF GET_TIME_TRIAL_COMPLETED_WEEK_STAT(sTTVarsStruct) = 0
			SET_TIME_TRIAL_COMPLETED_WEEK_STAT(sTTVarsStruct,-1)
			PRINTLN("=== TIME_TRIALS === INIT_TRIAL_COMPLETED_STAT - SET_TIME_TRIAL_COMPLETED_WEEK_STAT(-1)")
		ENDIF
		SET_TIME_TRIAL_DONE_STAT_INIT_STAT(sTTVarsStruct,TRUE)
		PRINTLN("=== TIME_TRIALS === INIT_TRIAL_COMPLETED_STAT - SET_PACKED_STAT_BOOL(GET_TIME_TRIAL_DONE_STAT_INIT_STAT)")
	ELSE
		PRINTLN("=== TIME_TRIALS === INIT_TRIAL_COMPLETED_STAT - Not doing since GET_TIME_TRIAL_DONE_STAT_INIT_STAT already set")
	ENDIF

ENDPROC

PROC INIT_PERSONAL_BEST(AMTT_VARS_STRUCT &sTTVarsStruct)

	IF (sTTVarsStruct.iVariation != GET_TIME_TRIAL_COMPLETED_WEEK_STAT(sTTVarsStruct))
		SET_TIME_TRIAL_BEST_TIME_STAT(sTTVarsStruct,0)
		sTTVarsStruct.iPersonalBest = 0
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === INIT_PERSONAL_BEST - New Trial, resetting personal best.")
	ELSE
		sTTVarsStruct.iPersonalBest = GET_TIME_TRIAL_BEST_TIME_STAT(sTTVarsStruct)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === INIT_PERSONAL_BEST - Same Trial, personal best = ",sTTVarsStruct.iPersonalBest)
	ENDIF
	
	IF sTTVarsStruct.iPersonalBest = 0
		sTTVarsStruct.iPersonalBest = HIGHEST_INT
	ENDIF
ENDPROC

FUNC BOOL CAN_USE_TT_QUICK_RESTART(AMTT_VARS_STRUCT &sTTVarsStruct)

	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_USE_TT_QUICK_RESTART - FALSE - Player not ok.")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_JACKING(PLAYER_PED_ID())
	OR IS_PED_BEING_JACKED(PLAYER_PED_ID())
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_USE_TT_QUICK_RESTART - FALSE - Ped involved in jacking.")
		RETURN FALSE
	ENDIF
					
	IF IS_PAUSE_MENU_ACTIVE()
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_USE_TT_QUICK_RESTART - FALSE - Pause menu active.")
		RETURN FALSE
	ENDIF
					
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_USE_TT_QUICK_RESTART - FALSE - Not in a vehicle")
		RETURN FALSE
	ENDIF

	IF NOT IS_VEHICLE_DRIVEABLE(sTTVarsStruct.trialVehicle)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_USE_TT_QUICK_RESTART - FALSE - vehicle not driveable.")
		RETURN FALSE
	ENDIF
	
	IF GET_VEHICLE_NUMBER_OF_PASSENGERS(sTTVarsStruct.trialVehicle) >= 1
		PRINT_HELP("AMTT_NOQR")
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_USE_TT_QUICK_RESTART - FALSE - Vehicle has passengers.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

PROC HANDLE_TIME_TRIAL_QUICK_RESTART(AMTT_VARS_STRUCT &sTTVarsStruct)

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OnlyExitVehicleOnButtonRelease, TRUE)
	ENDIF

	SWITCH(sTTVarsStruct.eQRStage)
	
		CASE AMTRQ_PRESS
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
			OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
				IF HAS_NET_TIMER_EXPIRED(sTTVarsStruct.buttonHoldTimer,FORCE_WARP_DELAY)
					IF CAN_USE_TT_QUICK_RESTART(sTTVarsStruct)
						INT ms 
						ms = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sTTVarsStruct.buttonHoldTimer.Timer)
					
						DRAW_GENERIC_METER(ms - FORCE_WARP_DELAY, TRIANGLE_RESPAWN_TIME, "TRI_WARP", HUD_COLOUR_RED, 0, HUDORDER_EIGHTHBOTTOM, -1, -1, FALSE, TRUE)
					
						IF (ms - FORCE_WARP_DELAY) >= TRIANGLE_RESPAWN_TIME
							
							DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
							IF IS_SCREEN_FADING_OUT()
								NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,NSPC_REMOVE_EXPLOSIONS | NSPC_REMOVE_FIRES | NSPC_REMOVE_PROJECTILES )
								CLEAR_CUSTOM_VEHICLE_NODES()
								KILL_YOURSELF_CLEANUP() // stop player from killing themselves just as they do a manual respawn. fix for 2459283
								ADD_CUSTOM_VEHICLE_NODE(sTTVarsStruct.vStartPoint, sTTVarsStruct.fStartHeading)
								SETUP_SPECIFIC_SPAWN_LOCATION(sTTVarsStruct.vStartPoint, sTTVarsStruct.fStartHeading, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 15.0)
								MPGlobalsAmbience.bSuppressMapExitChecks = TRUE
								SET_AMTRQ_STAGE(sTTVarsStruct,AMTRQ_FADE_OUT)
							ENDIF
							CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === HANDLE_TIME_TRIAL_QUICK_RESTART - Quick restart triggered")
						ENDIF
					ELSE
						RESET_NET_TIMER(sTTVarsStruct.buttonHoldTimer)
						CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === HANDLE_TIME_TRIAL_QUICK_RESTART - Quick restart blocked, reset timer")
					ENDIF
					SET_VEHICLE_DOORS_LOCKED(sTTVarsStruct.trialVehicle, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
					DISABLE_VEHICLE_EXIT_THIS_FRAME()
				ENDIF
				DISABLE_KILL_YOURSELF_OPTION()
			ELSE
				ENABLE_KILL_YOURSELF_OPTION()
				SET_VEHICLE_DOORS_LOCKED(sTTVarsStruct.trialVehicle, VEHICLELOCK_LOCKED_NO_PASSENGERS)
				RESET_NET_TIMER(sTTVarsStruct.buttonHoldTimer)
				//CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === HANDLE_TIME_TRIAL_QUICK_RESTART - Button released, reset timer")
			ENDIF
		BREAK
		
		CASE AMTRQ_FADE_OUT
			IF IS_SCREEN_FADED_OUT() 
				HIDE_PLAYER_AND_VEHICLE_FOR_WARP(TRUE, FALSE)
				SET_AMTRQ_STAGE(sTTVarsStruct,AMTRQ_WARP)
			ENDIF
			DISABLE_VEHICLE_EXIT_THIS_FRAME()
		BREAK
		
		CASE AMTRQ_WARP
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_NEAR_SPECIFIC_COORDS, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				RESET_NET_TIMER(sTTVarsStruct.buttonHoldTimer)
				TIME_TRIALS_CLEANUP(sTTVarsStruct,FALSE,FALSE)
				CLEAR_ALL_BIG_MESSAGES()
				REVEAL_PLAYER_AND_VEHICLE_FOR_WARP()
				//IF IS_SCREEN_FADING_IN()					
					NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE,NSPC_REMOVE_EXPLOSIONS | NSPC_REMOVE_FIRES | NSPC_REMOVE_PROJECTILES | NSPC_DONT_CLEAR_TASKS_ON_RESUME_CONTROL)
					CLEAR_SPECIFIC_SPAWN_LOCATION()
					CLEAR_CUSTOM_VEHICLE_NODES()
					SET_AMTRQ_STAGE(sTTVarsStruct,AMTRQ_FADE_IN)
				//ENDIF
			ENDIF		
			DISABLE_VEHICLE_EXIT_THIS_FRAME()
		BREAK
		
		CASE AMTRQ_FADE_IN
			IF IS_SCREEN_FADED_IN()	
				SET_AMTRQ_STAGE(sTTVarsStruct,AMTRQ_PRESS)
			ENDIF
			DISABLE_VEHICLE_EXIT_THIS_FRAME()
		BREAK

	ENDSWITCH
	
ENDPROC

FUNC BOOL SHOULD_SHOW_TIME_TRIAL_CORONA()

	RETURN NOT SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_TIME_TRIAL)
	AND NOT g_sMPTunables.bDisable_Event_AToBTimeTrial
	
ENDFUNC

FUNC BOOL CAN_TRIGGER_TIME_TRIAL_CORONA(AMTT_VARS_STRUCT &sTTVarsStruct)

	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RACETOPOINT)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - IS_PLAYER_ON_MP_AMBIENT_SCRIPT")
	    RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - IS_PLAYER_USING_RCBANDITO")
	    RETURN FALSE
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF IS_PLAYER_USING_RC_TANK(PLAYER_ID())
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - IS_PLAYER_USING_RC_TANK")
	    RETURN FALSE
	ENDIF
	#ENDIF
	
	IF NOT GET_PLAYER_TRIAL_VEHICLE(sTTVarsStruct)
		DO_HELP_TEXT(eHELP_CORONA,sTTVarsStruct) //Time Trials can only be triggered in a vehicle.
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - NOT GET_PLAYER_TRIAL_VEHICLE")
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX currentVeh = sTTVarsStruct.trialVehicle
	
	IF NOT IS_VEHICLE_DRIVEABLE(currentVeh)
		CLEAR_TIME_TRIAL_HELP(#IF FEATURE_STUNT_FM_EVENTS sTTVarsStruct.bStunt #ENDIF)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - NOT IS_VEHICLE_DRIVEABLE")
		RETURN FALSE
	ENDIF

	IF NOT IS_PLAYER_IN_SUITABLE_VEHICLE_TO_START_TIME_TRIAL(currentVeh, sTTVarsStruct)
		//CLEAR_TIME_TRIAL_HELP(#IF FEATURE_STUNT_FM_EVENTS sTTVarsStruct.bStunt #ENDIF)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - NOT IS_PLAYER_IN_SUITABLE_VEHICLE_TO_START_TIME_TRIAL")
		RETURN FALSE
	ENDIF
	
	IF GET_VEHICLE_NUMBER_OF_PASSENGERS(currentVeh) >= 1
	OR (IS_ANYONE_GETTING_IN_TIME_TRIAL_VEHICLE(currentVeh) AND GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) != currentVeh)
		PRINT_HELP("AMTT_NOPASSN")
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - GET_VEHICLE_NUMBER_OF_PASSENGERS")
		RETURN FALSE
	ENDIF	

	IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(),currentVeh,VS_DRIVER)
		CLEAR_TIME_TRIAL_HELP(#IF FEATURE_STUNT_FM_EVENTS sTTVarsStruct.bStunt #ENDIF)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - NOT IS_PED_SITTING_IN_VEHICLE_SEAT")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_OK_TO_PRINT_TIME_TRIAL_HELP()
		CLEAR_TIME_TRIAL_HELP(#IF FEATURE_STUNT_FM_EVENTS sTTVarsStruct.bStunt #ENDIF)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - NOT IS_OK_TO_PRINT_TIME_TRIAL_HELP")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		CLEAR_TIME_TRIAL_HELP(#IF FEATURE_STUNT_FM_EVENTS sTTVarsStruct.bStunt #ENDIF)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - IS_PHONE_ONSCREEN")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		DO_HELP_TEXT(eHELP_CRITICAL,sTTVarsStruct)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - IS_PLAYER_CRITICAL_TO_ANY_EVENT")
		RETURN FALSE
	ENDIF

	IF GET_ENTITY_SPEED(currentVeh) > 1 //Don't allow to start until stationary
		DO_HELP_TEXT(eHELP_SLOW,sTTVarsStruct)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - GET_ENTITY_SPEED(currentVeh) > 1")
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
		CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === CAN_TRIGGER_TIME_TRIAL_CORONA - IS_PLAYER_TEST_DRIVING_A_VEHICLE")
		RETURN FALSE
	ENDIF
	#ENDIF // #IF FEATURE_DLC_1_2022
	
	RETURN TRUE
ENDFUNC

//----------------------
//	Main Functions
//----------------------



/// PURPOSE: The main Time Trial processing loop. Called from freemode.sc
///    
/// PARAMS:
///    sTTVarsStruct - The time trial data that is initialised and passed in from freemode
FUNC BOOL MAINTAIN_AM_TIME_TRIALS(AMTT_VARS_STRUCT &sTTVarsStruct, BOOL bCanRunTimeTrial)

	#IF IS_DEBUG_BUILD
	PROCESS_DEBUG(sTTVarsStruct)
	#ENDIF
	
	BOOL bUseEveryframeUpdate = sTTVarsStruct.eAMTT_Stage > AMTT_WAIT
		
	SWITCH sTTVarsStruct.eAMTT_Stage
	
		CASE AMTT_INIT
			IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_TIME_TRIAL)
				//Grab start point
				sTTVarsStruct.iVariation = GRAB_TIME_TRIAL_VARIATION(#IF FEATURE_STUNT_FM_EVENTS sTTVarsStruct.bStunt #ENDIF)
				IF GET_TIME_TRIAL_LOCATION_INFO(sTTVarsStruct.iVariation,sTTVarsStruct.vStartPoint,sTTVarsStruct.fStartHeading,sTTVarsStruct.vEndPoint,sTTVarsStruct.fEndHeading,sTTVarsStruct.vStartDirection #IF FEATURE_STUNT_FM_EVENTS, sTTVarsStruct.bStunt #ENDIF)
					INIT_TRIAL_COMPLETED_STAT(sTTVarsStruct)
					INIT_PERSONAL_BEST(sTTVarsStruct)
					FM_EVENT_BLOCK_SPECIFIC_GANG_ATTACKS(FMMC_TYPE_TIME_TRIAL,sTTVarsStruct.iVariation)
					SET_LOCAL_AMTT_STAGE(sTTVarsStruct,AMTT_WAIT)
				ENDIF
			ENDIF
		BREAK
	
		CASE AMTT_WAIT
		
			IF bCanRunTimeTrial
			AND SHOULD_SHOW_TIME_TRIAL_CORONA()
				bUseEveryframeUpdate = TRUE
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),sTTVarsStruct.vStartPoint,<<ciIN_LOCATE_DISTANCE,ciIN_LOCATE_DISTANCE,ciIN_LOCATE_DISTANCE>>)
					SET_LOCAL_PLAYER_TIME_TRIAL_CORONA(FMMC_TYPE_TIME_TRIAL)
					IF CAN_TRIGGER_TIME_TRIAL_CORONA(sTTVarsStruct)
						IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
							DISABLE_PLAYER_VEHICLE_MISC_CONTROLS_THIS_FRAME()
							sTTVarsStruct.telemetryStruct.m_startTime = GET_CLOUD_TIME_AS_INT()
							SET_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS(sTTVarsStruct.trialVehicle,TRUE)
							CLEAR_LOCAL_PLAYER_TIME_TRIAL_CORONA(FMMC_TYPE_TIME_TRIAL)
							CLEAR_ALL_BIG_MESSAGES()
							SET_LOCAL_AMTT_STAGE(sTTVarsStruct,AMTT_START)
						ENDIF
						DO_HELP_TEXT(eHELP_PRESS,sTTVarsStruct) //Press ~INPUT_CONTEXT~ to start this Time Trial.
					ENDIF
				ELSE
					CLEAR_TIME_TRIAL_HELP(#IF FEATURE_STUNT_FM_EVENTS sTTVarsStruct.bStunt #ENDIF)
					CLEAR_LOCAL_PLAYER_TIME_TRIAL_CORONA(FMMC_TYPE_TIME_TRIAL)
				ENDIF
				DRAW_START_UI(sTTVarsStruct,sTTVarsStruct.vStartPoint)
			ELSE
				CLEAR_LOCAL_PLAYER_TIME_TRIAL_CORONA(FMMC_TYPE_TIME_TRIAL)
				CLEAR_TIME_TRIAL_HELP(#IF FEATURE_STUNT_FM_EVENTS sTTVarsStruct.bStunt #ENDIF)
				CLEANUP_TIME_TRIAL_CORONA_PROJECTION(sTTVarsStruct)
				sTTVarsStruct.iLocalBS = 0
				IF DOES_BLIP_EXIST(sTTVarsStruct.destinationBlip)
					REMOVE_BLIP(sTTVarsStruct.destinationBlip)					
					CPRINTLN(DEBUG_NET_AMBIENT, "=== TIME_TRIALS === BLIP AND CORONA REMOVED")
				ENDIF
			ENDIF
		BREAK
			
		CASE AMTT_START
			PAUSE_BOUNTY_TIMER_THIS_FRAME()
			HIDE_BOUNTY_BLIP_THIS_FRAME()
			IF IS_TIME_TRIAL_VEHICLE_LOCKED_FOR_OTHERS(sTTVarsStruct.trialVehicle)
				IF SETUP_VEHICLE_FOR_TIME_TRIAL(sTTVarsStruct)
					IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iPassiveModeDisableDelayTimer)
						RESET_NET_TIMER(MPGlobalsAmbience.iPassiveModeDisableDelayTimer)
						g_MultiplayerSettings.g_bPassiveModeSetting = FALSE
						SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
					ENDIF
					CLEAR_HELP()
					CLEANUP_TIME_TRIAL_CORONA_PROJECTION(sTTVarsStruct)
					#IF FEATURE_STUNT_FM_EVENTS
					IF sTTVarsStruct.bStunt
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_START_OF_JOB,"AMTT_SSTART", DEFAULT, DEFAULT, DEFAULT, DEFAULT)
					ELSE
					#ENDIF
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_START_OF_JOB,"AMTT_START", DEFAULT, DEFAULT, DEFAULT, DEFAULT)
					#IF FEATURE_STUNT_FM_EVENTS
					ENDIF
					#ENDIF
					STORE_TIME_AT_BEGINNING(sTTVarsStruct.trialTimer)
					SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_TIME_TRIAL,TRUE)
					COMMON_FREEMODE_AMBIENT_EVENTS_SET_UP(FMMC_TYPE_TIME_TRIAL, DEFAULT, DEFAULT, TRUE, TRUE,FALSE,TRUE)
					DETACH_VEHICLE_FROM_ANY_CARGOBOB(sTTVarsStruct.trialVehicle)
					sTTVarsStruct.iSoundID = GET_SOUND_ID()
					SET_TEMP_PASSIVE_MODE(TRUE)
					SET_FREEMODE_FLOW_COMMS_DISABLED(TRUE)
					DISABLE_PLAYER_VEHICLE_MISC_CONTROLS_THIS_FRAME()
					FM_EVENT_SET_PLAYER_RESTRICTED_WITH_PASSIVE()
					SET_LOCAL_AMTT_STAGE(sTTVarsStruct,AMTT_GOTO)
				ELSE
					SET_LOCAL_AMTT_STAGE(sTTVarsStruct,AMTT_WAIT)
				ENDIF
			ELSE
				SET_LOCAL_AMTT_STAGE(sTTVarsStruct,AMTT_WAIT)
			ENDIF
		BREAK 
	
		CASE AMTT_GOTO
			PAUSE_BOUNTY_TIMER_THIS_FRAME()
			HIDE_BOUNTY_BLIP_THIS_FRAME()
			IF sTTVarsStruct.eQRStage = AMTRQ_PRESS
				IF SHOULD_TRIAL_END(sTTVarsStruct)
					TIME_TRIALS_CLEANUP(sTTVarsStruct)
				ELSE
					IF IS_PLAYER_AT_END_POINT(sTTVarsStruct)
						SET_LOCAL_AMTT_STAGE(sTTVarsStruct,AMTT_END)
					ENDIF
				ENDIF
			ENDIF
			//url:bugstar:2441546 - Stop headlights turning on/off
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTTVarsStruct.trialTimer) < DISABLE_MISC_CONTROLS_TIME
				DISABLE_PLAYER_VEHICLE_MISC_CONTROLS_THIS_FRAME()
			ENDIF
			DRAW_TRIAL_UI(sTTVarsStruct,GET_NEXT_CHECKPOINT_DRAW_LOCATION(sTTVarsStruct))
			IF sTTVarsStruct.eAMTT_Stage = AMTT_GOTO
				HANDLE_TIME_TRIAL_QUICK_RESTART(sTTVarsStruct)
			ENDIF
		BREAK 
		
		CASE AMTT_END
			IF NOT HAS_SOUND_FINISHED(sTTVarsStruct.iSoundID)
				STOP_SOUND(sTTVarsStruct.iSoundID)
			ENDIF
			RELEASE_SOUND_ID(sTTVarsStruct.iSoundID)
			PAUSE_BOUNTY_TIMER_THIS_FRAME()
			HIDE_BOUNTY_BLIP_THIS_FRAME()
			PROCESS_TIME_TRIAL_END(sTTVarsStruct)
			SET_LOCAL_AMTT_STAGE(sTTVarsStruct,AMTT_CLEANUP)
		BREAK
		
		CASE AMTT_CLEANUP
			PAUSE_BOUNTY_TIMER_THIS_FRAME()
			HIDE_BOUNTY_BLIP_THIS_FRAME()
			IF HAS_NET_TIMER_EXPIRED(sTTVarsStruct.endTimer,END_TIME)
				TIME_TRIALS_CLEANUP(sTTVarsStruct)
			ENDIF
		BREAK
		

	ENDSWITCH
	
	RETURN bUseEveryframeUpdate
ENDFUNC
