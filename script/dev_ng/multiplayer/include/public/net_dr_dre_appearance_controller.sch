//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_dr_dre_appearance_controller.sch														//
// Description: Header for managing when Dr Dre should be present in the music studio						//
//				This is being called in 																	//
//				x:\gta5\script\dev_ng_live\multiplayer\include\public\net_club_music.sch					//
//				TO TEST CHANGES REBUILD AM_MP_MUSIC_STUDIO.SC												//
// Written by:  Online Technical Team: Jan Mencfel,															//
// Date:  		28/11/2021																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_ambience.sch"
USING "net_simple_interior.sch"

CONST_INT ciCUTSCENE_TRIGGER_OFFSET		5000

PROC PRINT_POSIX_TIME(INT iPosix, STRING sLabel)
	UGC_DATE sData
	CONVERT_POSIX_TIME(iPosix, sData)
	PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][",sLabel,"][PRINT_POSIX_TIME] DATE:", sData.nDay,"/",sData.nMonth,"/",sData.nYear,"   TIME: ", sData.nHour,":",sData.nMinute,":",sData.nSecond )
ENDPROC

PROC PRINT_POSIX_TIME_DIFFERENCE(INT iPosix, STRING sLabel)
	UGC_DATE sData
	CONVERT_POSIX_TIME(iPosix, sData)
	PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][",sLabel,"][PRINT_POSIX_TIME_DIFFERENCE]   TIME: ", sData.nHour,":",sData.nMinute,":",sData.nSecond )
ENDPROC

FUNC INT GET_NEXT_DRE_APPARANCE_POSIX_WITH_TIME_OFFSET()

	INT iCurrentCloudTime = GET_CLOUD_TIME_AS_INT()
	
	//IF SCHEDULED POSIX APPEARANCES ARE ENABLED RETURN -1
	IF g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE_POSIX 
		PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_NEXT_DRE_APPARANCE_POSIX_WITH_TIME_OFFSET] g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE_POSIX  = TRUE")
		RETURN -1
	ENDIF
	
	// IF next posix was already calculated and still valid return that posix
	IF g_sMusicStudioDreWorkData.iNextDrePosix != -1
		IF g_sMusicStudioDreWorkData.iNextDrePosix >= iCurrentCloudTime
				PRINT_POSIX_TIME(g_sMusicStudioDreWorkData.iNextDrePosix, "g_sMusicStudioDreWorkData.iNextDrePosix")
				PRINT_POSIX_TIME(g_sMusicStudioDreWorkData.iPreviousDrePosix, "g_sMusicStudioDreWorkData.iPreviousDrePosix")
			RETURN g_sMusicStudioDreWorkData.iNextDrePosix
		ENDIF
	ENDIF
	
	//in case something went wrong and next posix is not set
	IF g_sMPTunables.iFIXER_STUDIO_APPEARANCE_START_POSIX = -1
	#IF IS_DEBUG_BUILD
	AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_StudioAppearancePosix")
	#ENDIF
		PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_NEXT_DRE_APPARANCE_POSIX_WITH_TIME_OFFSET]g_sMPTunables.iFIXER_STUDIO_APPEARANCE_START_POSIX = -1 " )		
		RETURN -1
	ENDIF
	
	
	//calculate next posix based on the starting posix and appearance frequency
	INT iStartPosix = g_sMPTunables.iFIXER_STUDIO_APPEARANCE_START_POSIX
	INT iNextPosix
	//override start posix if commandline exists
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_StudioAppearancePosix")
		iStartPosix = GET_COMMANDLINE_PARAM_INT("sc_StudioAppearancePosix")
	ENDIF
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_StudioAppearanceFreq") //IF WE ADD THIS PARAM WE OVERRIDE THE TUNABLE
	#ENDIF	
	
	//NON DEBUG FLOW
	iNextPosix = iStartPosix	
	WHILE iNextPosix < iCurrentCloudTime
		iNextPosix += g_sMPTunables.iFIXER_STUDIO_APPEARANCE_FREQUENCY*60
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
	ELSE
	INT iCommandlineFreq = GET_COMMANDLINE_PARAM_INT("sc_StudioAppearanceFreq") // USE THIS VALUE INSTEAD OF THE TUNABLE
	iNextPosix = iStartPosix
	WHILE iNextPosix < iCurrentCloudTime
		iNextPosix += iCommandlineFreq*60
	ENDWHILE
	ENDIF
	#ENDIF
		
	g_sMusicStudioDreWorkData.iNextDrePosix = iNextPosix
	PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_NEXT_DRE_APPARANCE_POSIX_WITH_TIME_OFFSET] g_sMusicStudioDreWorkData.iNextDrePosix = ", g_sMusicStudioDreWorkData.iNextDrePosix )
		
	
	IF iNextPosix - g_sMPTunables.iFIXER_STUDIO_APPEARANCE_FREQUENCY*60 >= iStartPosix
		g_sMusicStudioDreWorkData.iPreviousDrePosix = iNextPosix - g_sMPTunables.iFIXER_STUDIO_APPEARANCE_FREQUENCY*60
		PRINT_POSIX_TIME(g_sMusicStudioDreWorkData.iPreviousDrePosix, "LAST DRE APPEARNCE")			
	ELSE
		g_sMusicStudioDreWorkData.iPreviousDrePosix = -1
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_StudioAppearanceFreq")
		g_sMusicStudioDreWorkData.iPreviousDrePosix = g_sMusicStudioDreWorkData.iNextDrePosix - GET_COMMANDLINE_PARAM_INT("sc_StudioAppearanceFreq")*60
	ENDIF
	#ENDIF
	
	PRINT_POSIX_TIME(iNextPosix, "iNextPosix")			
	
	RETURN iNextPosix
ENDFUNC

FUNC INT GET_LAST_DRE_APPARANCE_POSIX_WITH_TIME_OFFSET()

	//THIS SETS THE GLOBALS
	GET_NEXT_DRE_APPARANCE_POSIX_WITH_TIME_OFFSET()		
	
	RETURN g_sMusicStudioDreWorkData.iPreviousDrePosix
ENDFUNC

FUNC INT GET_APPEARANCE_POSIX_START_BY_INDEX(INT i)
	SWITCH i
		CASE 0 		RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_0
		CASE 1 		RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_1
		CASE 2 		RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_2
		CASE 3 		RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_3
		CASE 4 		RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_4
		CASE 5 		RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_5
		CASE 6 		RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_6
		CASE 7 		RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_7
		CASE 8 		RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_8
		CASE 9 		RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_9
		CASE 10 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_10
		CASE 11 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_11
		CASE 12 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_12
		CASE 13 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_13
		CASE 14 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_14
		CASE 15 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_15
		CASE 16 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_16
		CASE 17 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_17
		CASE 18 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_18
		CASE 19 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_19
		CASE 20 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_20
		CASE 21 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_21
		CASE 22 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_22
		CASE 23 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_23
		CASE 24 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_24
		CASE 25 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_25
		CASE 26 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_26
		CASE 27 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_27
		CASE 28 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_28
		CASE 29 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_29
		CASE 30 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_30
		CASE 31 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_31
		CASE 32 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_32
		CASE 33 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_33
		CASE 34 	RETURN g_sMPTunables.iFIXER_STUDIO_APPEARANCE_POSIX_START_34
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_DR_DRE_APPEARANCE_BE_BLOCKED_TODAY()
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_StudioAppearanceFreq")
		RETURN FALSE
	ENDIF
	#ENDIF
			
	SWITCH GET_CURRENT_DAY_OF_THE_WEEK()
		CASE SUNDAY				RETURN g_sMPTunables.bFIXER_STUDIO_APPEARANCE_DISABLE_SUN 
		CASE MONDAY				RETURN g_sMPTunables.bFIXER_STUDIO_APPEARANCE_DISABLE_MON
		CASE TUESDAY			RETURN g_sMPTunables.bFIXER_STUDIO_APPEARANCE_DISABLE_TUE
		CASE WEDNESDAY			RETURN g_sMPTunables.bFIXER_STUDIO_APPEARANCE_DISABLE_WED
		CASE THURSDAY			RETURN g_sMPTunables.bFIXER_STUDIO_APPEARANCE_DISABLE_THU
		CASE FRIDAY				RETURN g_sMPTunables.bFIXER_STUDIO_APPEARANCE_DISABLE_FRI
		CASE SATURDAY			RETURN g_sMPTunables.bFIXER_STUDIO_APPEARANCE_DISABLE_SAT	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NEXT_DRE_APPARANCE_POSIX_WITH_SCHEDULE()

	//IF SCHEDULED POSIX APPEARANCES ARE NOT ENABLED RETURN -1
	IF NOT g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE_POSIX 
		RETURN -1
	ENDIF
	
	// IF next posix was already calculated and still valid return that posix
	IF g_sMusicStudioDreWorkData.iNextDrePosix != -1
		IF g_sMusicStudioDreWorkData.iNextDrePosix >= GET_CLOUD_TIME_AS_INT()	
			RETURN g_sMusicStudioDreWorkData.iNextDrePosix
		ENDIF
	ENDIF
	
	INT iCurrentTime = GET_CLOUD_TIME_AS_INT()
	INT iLastDrePosixIndex = 0
	
	//check studio appearance schedule ID is set
	IF g_sMPTunables.iFIXER_STUDIO_APPEARANCE_SCHEDULE_ID != -1
		//if schedule is unchanged since player's last session
		IF g_sMPTunables.iFIXER_STUDIO_APPEARANCE_SCHEDULE_ID = GET_PACKED_STAT_INT(PACKED_INT_LAST_DRE_APPEARANCE_SCHEDULE_ID)		
			//we can start checking indices from the last posix that was current for the player
			iLastDrePosixIndex = GET_PACKED_STAT_INT(PACKED_INT_LAST_DRE_POSIX_INDEX)
			PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_NEXT_DRE_APPARANCE_POSIX_WITH_SCHEDULE]	g_sMPTunables.iFIXER_STUDIO_APPEARANCE_SCHEDULE_ID DIDN'T CHANGE   iLastDrePosixIndex:", iLastDrePosixIndex)				
		ELSE
			PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_NEXT_DRE_APPARANCE_POSIX_WITH_SCHEDULE] PACKED_INT_LAST_DRE_APPEARANCE_SCHEDULE_ID didn't match, setting it to ", g_sMPTunables.iFIXER_STUDIO_APPEARANCE_SCHEDULE_ID)				
			SET_PACKED_STAT_INT(PACKED_INT_LAST_DRE_APPEARANCE_SCHEDULE_ID, g_sMPTunables.iFIXER_STUDIO_APPEARANCE_SCHEDULE_ID)		
		ENDIF
	ENDIF
	
	INT i
	FOR i = iLastDrePosixIndex TO 32
		IF GET_APPEARANCE_POSIX_START_BY_INDEX(i) >= iCurrentTime
			g_sMusicStudioDreWorkData.iNextDrePosix = GET_APPEARANCE_POSIX_START_BY_INDEX(i)
			SET_PACKED_STAT_INT(PACKED_INT_LAST_DRE_POSIX_INDEX, i)		
			//WE SAVE THE PREVIOUS POSIX IN CASE PLAYER ENTERS WHEN DRE PERFORMANCE SHOULD ALREADY BE HAPPENING
			IF i>0
				g_sMusicStudioDreWorkData.iPreviousDrePosix = GET_APPEARANCE_POSIX_START_BY_INDEX(i-1)
			ENDIF
			RETURN 	g_sMusicStudioDreWorkData.iNextDrePosix
		ENDIF
	ENDFOR
	
	PRINT_POSIX_TIME(g_sMusicStudioDreWorkData.iNextDrePosix, "NEXT DRE APPEARANCE")	
	PRINT_POSIX_TIME(g_sMusicStudioDreWorkData.iPreviousDrePosix, "LAST DRE APPEARANCE")	
	
	RETURN -1
ENDFUNC

FUNC INT GET_LAST_DRE_APPARANCE_POSIX_WITH_SCHEDULE()

	//THIS SETS THE GLOBALS
	GET_NEXT_DRE_APPARANCE_POSIX_WITH_SCHEDULE()		
	
	RETURN g_sMusicStudioDreWorkData.iPreviousDrePosix
ENDFUNC

FUNC INT GET_LAST_DRE_APPEARANCE_POSIX()
	//MAKE SURE DRE APPEARANCE IS ENABLED
	IF NOT g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE 
		RETURN -1
	ENDIF
	
	//MAKE SURE IT'S NOT BLOCKED TODAY
	IF SHOULD_DR_DRE_APPEARANCE_BE_BLOCKED_TODAY()
		RETURN -1
	ENDIF
	
	INT iNextPosix = -1
	IF g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE_POSIX 
		iNextPosix = GET_LAST_DRE_APPARANCE_POSIX_WITH_SCHEDULE()
	ELSE
		iNextPosix = GET_LAST_DRE_APPARANCE_POSIX_WITH_TIME_OFFSET()
	ENDIF
	
	RETURN iNextPosix
ENDFUNC

FUNC INT GET_TOTAL_DRE_PERFORMANCE_TIME_AT_STAGE_IN_SECONDS(CLUB_DJS eDJ, DJ_SERVER_DATA &DJServerData)
	
	SWITCH eDJ
		CASE CLUB_DJ_DR_DRE_MIX
		CASE CLUB_DJ_DR_DRE_MIX_VOCALS
			PRINTLN("g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX = ", g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX)
			RETURN FLOOR(g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX /1000.0)
		BREAK
		CASE CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
			RETURN FLOOR((g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX + g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MUSICIAN_DRUMS)/1000.0)
		BREAK
		CASE CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
			RETURN FLOOR((g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX + g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MUSICIAN_VOCALS)/1000.0)
		BREAK
		CASE CLUB_DJ_DR_DRE_MIX_2
			IF IS_BIT_SET(DJServerData.iBS, CLUB_MUSIC_SERVER_BS_USING_PATH_B)
				RETURN FLOOR((g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX + g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MUSICIAN_DRUMS + g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX_2)/1000.0)		
			ENDIF
			RETURN FLOOR((g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX + g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MUSICIAN_VOCALS + g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX_2)/1000.0)
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC CLUB_DJS GET_CURRENT_DRE_STATE_BASED_ON_POSIX(DJ_SERVER_DATA &DJServerData)

	IF SHOULD_DR_DRE_APPEARANCE_BE_BLOCKED_TODAY()
		RETURN CLUB_DJ_DR_DRE_ABSENT
	ENDIF
	
	INT iLastDrDrePosix = GET_LAST_DRE_APPEARANCE_POSIX()
	
	IF iLastDrDrePosix = -1
		PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_CURRENT_DRE_STATE_BASED_ON_POSIX] iCurrentTime = ", GET_CLOUD_TIME_AS_INT())
		PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_CURRENT_DRE_STATE_BASED_ON_POSIX] RETURNING CLUB_DJ_DR_DRE_ABSENT (iLastDrDrePosix = -1)")
		RETURN CLUB_DJ_DR_DRE_ABSENT
	ENDIF
	INT iCurrentTime = GET_CLOUD_TIME_AS_INT()
	
	PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_CURRENT_DRE_STATE_BASED_ON_POSIX] iCurrentTime = ", iCurrentTime)
	
	PRINT_POSIX_TIME_DIFFERENCE(iCurrentTime-iLastDrDrePosix, " TIME DIFFERENCE: ")
	
	IF iLastDrDrePosix + GET_TOTAL_DRE_PERFORMANCE_TIME_AT_STAGE_IN_SECONDS(CLUB_DJ_DR_DRE_MIX, DJServerData) >  iCurrentTime
		PRINT_POSIX_TIME(iCurrentTime, "CURRENT TIME")
		PRINT_POSIX_TIME(iCurrentTime, "iLastDrDrePosix")
		PRINT_POSIX_TIME(iLastDrDrePosix + GET_TOTAL_DRE_PERFORMANCE_TIME_AT_STAGE_IN_SECONDS(CLUB_DJ_DR_DRE_MIX, DJServerData), "THIS STAGE SHOULD LAST UNTIL")
		
		IF g_sMusicStudioDreWorkData.bUsingPathB
			PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_CURRENT_DRE_STATE_BASED_ON_POSIX] RETURNING CLUB_DJ_DR_DRE_MIX_VOCALS")
			RETURN CLUB_DJ_DR_DRE_MIX_VOCALS
		ENDIF
		PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_CURRENT_DRE_STATE_BASED_ON_POSIX] RETURNING CLUB_DJ_DR_DRE_MIX")
		RETURN CLUB_DJ_DR_DRE_MIX
		
	ELIF g_sMusicStudioDreWorkData.bUsingPathB
	AND iLastDrDrePosix + GET_TOTAL_DRE_PERFORMANCE_TIME_AT_STAGE_IN_SECONDS(CLUB_DJ_DR_DRE_MUSICIAN_DRUMS, DJServerData) >  iCurrentTime
		PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_CURRENT_DRE_STATE_BASED_ON_POSIX] RETURNING CLUB_DJ_DR_DRE_MUSICIAN_DRUMS")
		RETURN CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
		
	ELIF NOT g_sMusicStudioDreWorkData.bUsingPathB
	AND iLastDrDrePosix + GET_TOTAL_DRE_PERFORMANCE_TIME_AT_STAGE_IN_SECONDS(CLUB_DJ_DR_DRE_MUSICIAN_VOCALS, DJServerData) >  iCurrentTime
		PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_CURRENT_DRE_STATE_BASED_ON_POSIX] RETURNING CLUB_DJ_DR_DRE_MUSICIAN_VOCALS")
		RETURN CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
		
	ELIF iLastDrDrePosix + GET_TOTAL_DRE_PERFORMANCE_TIME_AT_STAGE_IN_SECONDS(CLUB_DJ_DR_DRE_MIX_2, DJServerData) >  iCurrentTime
		PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_CURRENT_DRE_STATE_BASED_ON_POSIX] RETURNING CLUB_DJ_DR_DRE_MIX_2")
		RETURN CLUB_DJ_DR_DRE_MIX_2
	ENDIF
	
	PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][GET_CURRENT_DRE_STATE_BASED_ON_POSIX] RETURNING CLUB_DJ_DR_DRE_ABSENT")
	RETURN CLUB_DJ_DR_DRE_ABSENT
ENDFUNC

FUNC BOOL SHOULD_PLAY_DRUMS_SEQUENCE()
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceDrumsSequence")
		RETURN TRUE
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceVocalsSequence")
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF GET_RANDOM_INT_IN_RANGE(1, 101) <= g_sMPTunables.iMUSIC_STUDIO_DRE_PERFORMANCE_DRUMS_CHANCE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC INT GET_CURRENT_DRE_TRACK_OFFSET_BASED_ON_POSIX(DJ_SERVER_DATA DJServerData)
	INT iLastDrDrePosix = GET_LAST_DRE_APPEARANCE_POSIX()
	INT iCurrentTime = GET_CLOUD_TIME_AS_INT()
	
	INT iTrackTimeMS = (iLastDrDrePosix + GET_TOTAL_DRE_PERFORMANCE_TIME_AT_STAGE_IN_SECONDS(DJServerData.eDJ, DJServerData) - iCurrentTime)*1000	
	PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER] GET_CURRENT_DRE_TRACK_OFFSET_BASED_ON_POSIX = ", iTrackTimeMS)
	RETURN iTrackTimeMS
ENDFUNC

FUNC BOOL SHOULD_DRE_APPEAR_IN_MUSIC_STUDIO(INT timeBefore = 10)
	IF NOT g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE 
		RETURN FALSE
	ENDIF
	
	IF SHOULD_DR_DRE_APPEARANCE_BE_BLOCKED_TODAY()
		RETURN FALSE
	ENDIF

	INT iNextPosix
	
	IF g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE_POSIX 
		iNextPosix = GET_NEXT_DRE_APPARANCE_POSIX_WITH_SCHEDULE()
	ELSE
		iNextPosix = GET_NEXT_DRE_APPARANCE_POSIX_WITH_TIME_OFFSET()
	ENDIF
	
	IF iNextPosix != -1
		IF ABSI(GET_CLOUD_TIME_AS_INT()-iNextPosix) < timeBefore
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//MAIN FUNC TO BE CALLED EVERY FRAME WHEN PROCESSING CLUB MUSIC
FUNC BOOL HAS_DR_DRE_STAGE_EXPIRED()	
	
	//DISABLED DRE PERFORMANCES, DON'T PROGRESS
	IF NOT g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE 
		RETURN FALSE
	ENDIF
	
	//PROCESS ONLY EVERY 30 FRAMES IF DRE IS ABSENT, (NEED MORE ACCURACY WHEN DRE IS ALREADY PRESENT)
	IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
	AND GET_FRAME_COUNT() % 30 != 0
		RETURN FALSE
	ENDIF
		
	IF g_sMusicStudioDreWorkData.bStateChangeStarted
		RETURN FALSE
	ENDIF
	
	SWITCH g_clubMusicData.eActiveDJ	
	
		//WHILE DRE IS ABSENT WE CALCULATE TIME BASED ON POSIX
		CASE CLUB_DJ_DR_DRE_ABSENT						RETURN SHOULD_DRE_APPEAR_IN_MUSIC_STUDIO()
		
		//WHILE DRE IS PRESENT NEXT STAGE IS DETERMINED BY HOW LONG A TRACK HAS BEEN PLAYING
		CASE CLUB_DJ_DR_DRE_MIX							RETURN (g_clubMusicData.iCurrentTrackTimeMS>=g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX-ciCUTSCENE_TRIGGER_OFFSET)
		CASE CLUB_DJ_DR_DRE_MIX_VOCALS					RETURN (g_clubMusicData.iCurrentTrackTimeMS>=g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX_VOCALS-ciCUTSCENE_TRIGGER_OFFSET)
		CASE CLUB_DJ_DR_DRE_MUSICIAN_DRUMS				RETURN (g_clubMusicData.iCurrentTrackTimeMS>=g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MUSICIAN_DRUMS-ciCUTSCENE_TRIGGER_OFFSET)
		CASE CLUB_DJ_DR_DRE_MUSICIAN_VOCALS				RETURN (g_clubMusicData.iCurrentTrackTimeMS>=g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MUSICIAN_VOCALS-ciCUTSCENE_TRIGGER_OFFSET)
		CASE CLUB_DJ_DR_DRE_MIX_2						RETURN (g_clubMusicData.iCurrentTrackTimeMS>=g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX_2-ciCUTSCENE_TRIGGER_OFFSET)					
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_DR_DRE_STAGE_NEARLY_EXPIRED()	
	
	//DISABLED DRE PERFORMANCES, DON'T PROGRESS
	IF NOT g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE 
		RETURN FALSE
	ENDIF
			
	IF g_sMusicStudioDreWorkData.bStateChangeStarted
		RETURN FALSE
	ENDIF
	
	SWITCH g_clubMusicData.eActiveDJ	
	
		//WHILE DRE IS ABSENT WE CALCULATE TIME BASED ON POSIX
		CASE CLUB_DJ_DR_DRE_ABSENT						RETURN SHOULD_DRE_APPEAR_IN_MUSIC_STUDIO(20)
		
		//WHILE DRE IS PRESENT NEXT STAGE IS DETERMINED BY HOW LONG A TRACK HAS BEEN PLAYING
		CASE CLUB_DJ_DR_DRE_MIX							RETURN (g_clubMusicData.iCurrentTrackTimeMS>=g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX-(ciCUTSCENE_TRIGGER_OFFSET*3.0))
		CASE CLUB_DJ_DR_DRE_MIX_VOCALS					RETURN (g_clubMusicData.iCurrentTrackTimeMS>=g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX_VOCALS-(ciCUTSCENE_TRIGGER_OFFSET*3.0))
		CASE CLUB_DJ_DR_DRE_MUSICIAN_DRUMS				RETURN (g_clubMusicData.iCurrentTrackTimeMS>=g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MUSICIAN_DRUMS-(ciCUTSCENE_TRIGGER_OFFSET*3.0))
		CASE CLUB_DJ_DR_DRE_MUSICIAN_VOCALS				RETURN (g_clubMusicData.iCurrentTrackTimeMS>=g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MUSICIAN_VOCALS-(ciCUTSCENE_TRIGGER_OFFSET*3.0))
		CASE CLUB_DJ_DR_DRE_MIX_2						RETURN (g_clubMusicData.iCurrentTrackTimeMS>=g_sMPTunables.iFIXER_STUDIO_TRACK_TIME_DR_DRE_MIX_2-(ciCUTSCENE_TRIGGER_OFFSET*3.0))					
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC



FUNC BOOL SHOULD_CONTACT_PLAYER_ABOUT_DR_DRE_APPEARANCE()
	IF NOT g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE 
		RETURN FALSE
	ENDIF
	
	IF SHOULD_DR_DRE_APPEARANCE_BE_BLOCKED_TODAY()
		RETURN FALSE
	ENDIF

	INT iNextPosix = -1
	
	IF g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE_POSIX 
		iNextPosix = GET_NEXT_DRE_APPARANCE_POSIX_WITH_SCHEDULE()
	ELSE
		iNextPosix = GET_NEXT_DRE_APPARANCE_POSIX_WITH_TIME_OFFSET()
	ENDIF
	
	IF iNextPosix = g_sMusicStudioDreWorkData.iLastDreNotificationPosix
		RETURN FALSE
	ENDIF
	
	IF iNextPosix != -1											//this is by default 120 secons
		IF GET_CLOUD_TIME_AS_INT()+ g_sMPTunables.iFIXER_STUDIO_TIME_BEFORE_APPEARANCE_NOTIFICATION >= iNextPosix
			g_sMusicStudioDreWorkData.iLastDreNotificationPosix = iNextPosix
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MUSIC_STUDIO_MESSAGE_FLOW_STATE eState)
	PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER] SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE CHANGING STATE FROM ", g_sMusicStudioDreWorkData.eMessageFlowState, " TO ", eState)
	g_sMusicStudioDreWorkData.eMessageFlowState = eState
ENDPROC


FUNC BOOL SHOULD_BLOCK_NOTIFICATIONS()
	
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
	OR (IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()) AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID()))
	OR IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PROCESS_DR_DRE_MESSAGE_FLOW()
	
	//main tunable to disable message flow
	IF NOT g_sMPTunables.bENABLE_FIXER_STUDIO_NOTIFICATIONS
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF

	IF NOT HAS_LOCAL_PLAYER_COMPLETED_REQUIRED_MISSIONS_FOR_MUSIC_STUDIO_ACCESS()
		RETURN FALSE
	ENDIF
	
	IF NOT g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE 
		RETURN FALSE
	ENDIF
	
	IF SHOULD_DR_DRE_APPEARANCE_BE_BLOCKED_TODAY()
		RETURN FALSE
	ENDIF
	
	IF SHOULD_BLOCK_NOTIFICATIONS()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC PROCESS_MESSAGE_FLOW_INITIALIZE_STATE()
		
	IF SHOULD_CONTACT_PLAYER_ABOUT_DR_DRE_APPEARANCE()
		g_sMusicStudioDreWorkData.iNumberTimesNotified = GET_PACKED_STAT_INT(PACKED_INT_DRE_PERFORMANCE_NUM_TIMES_NOTIFIED)	
		
		//IF PLAYER IS NOT IN THE MUSIC STUDIO, SEND THE A NOTIFICATION
		IF NOT IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())				
			SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MSMFS_BEFORE_PERFORMANCE_NOTIFICATION)
		ELSE
			//DONT CALL OR TEXT PLAYER IF THEY'RE ALREADY IN THE MUSIC STUDIO
			//they skip these stages and get a suggestion to invite other players
			SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MSMFS_PERFORMANCE_PRE_INVITATION) 
		ENDIF
		
		
	ENDIF	
ENDPROC

PROC PROCESS_MESSAGE_FLOW_TICKER()
	PRINT_TICKER("MUSIC_DRE_TICK")
	SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MSMFS_BEFORE_PERFORMANCE_NOTIFICATION_CLEANUP)
ENDPROC

PROC PROCESS_MESSAGE_FLOW_TICKER_AND_CALL_FROM_DR_DRE()
	IF NOT IS_BIT_SET(g_sFixerFlow.Bitset2, ciFIXER_FLOW_BITSET2__DO_PRODUCER_COME_TO_STUDIO_CALL)
		//TRIGGER CALL
		SET_BIT(g_sFixerFlow.Bitset2, ciFIXER_FLOW_BITSET2__DO_PRODUCER_COME_TO_STUDIO_CALL)
		
		//UPDATE STAT
		g_sMusicStudioDreWorkData.iNumberTimesNotified++
		SET_PACKED_STAT_INT(PACKED_INT_DRE_PERFORMANCE_NUM_TIMES_NOTIFIED, g_sMusicStudioDreWorkData.iNumberTimesNotified)	
		
		PROCESS_MESSAGE_FLOW_TICKER()
		
		// NEXT STAGE AFTER CALL STARTS
		SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MSMFS_BEFORE_PERFORMANCE_NOTIFICATION_CLEANUP)	
	ENDIF
ENDPROC

PROC PROCESS_MESSAGE_FLOW_TICKER_AND_TEXT_FROM_FRANKLIN()
	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_FIXER_FRANKLIN, "MUSIC_FRA_NOTIF", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
		//UPDATE STAT
		g_sMusicStudioDreWorkData.iNumberTimesNotified++
		SET_PACKED_STAT_INT(PACKED_INT_DRE_PERFORMANCE_NUM_TIMES_NOTIFIED, g_sMusicStudioDreWorkData.iNumberTimesNotified)	
		
		PROCESS_MESSAGE_FLOW_TICKER()
		
		SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MSMFS_BEFORE_PERFORMANCE_NOTIFICATION_CLEANUP)
	ENDIF
ENDPROC

PROC PROCESS_MESSAGE_FLOW_NOTIFICATION_STATE()
	SWITCH g_sMusicStudioDreWorkData.iNumberTimesNotified		
		CASE 0 //IF PLAYER WAS NEVER NOTIFIED BEFORE THEY GET A PHONECALL FROM DRE
			 PROCESS_MESSAGE_FLOW_TICKER_AND_CALL_FROM_DR_DRE()
		BREAK				
		CASE 1
		CASE 2	//IF PLAYER ALREADY WAS NOTIFIED ONCE+ SEND A TEXT MESSAGE FROM FRANKLIN INSTEAD
		CASE 3
			PROCESS_MESSAGE_FLOW_TICKER_AND_TEXT_FROM_FRANKLIN()
		BREAK	
		DEFAULT //AFTER 3 MESSAGES FROM FRANKLIN ONLY DISPLAY A TICKER
			PROCESS_MESSAGE_FLOW_TICKER()
		BREAK
	ENDSWITCH		
ENDPROC


PROC PROCESS_MESSAGE_FLOW_NOTIFICATION_CLEANUP_STATE()
	// IF A CALL FROM DRE WAS TRIGGERED WAIT UNTIL IT'S DONE BEFORE DISPLAYING HELP MESSAGE
	IF g_sMusicStudioDreWorkData.iNumberTimesNotified = 1 
	AND IS_BIT_SET(g_sFixerFlow.Bitset2, ciFIXER_FLOW_BITSET2__PRODUCER_COME_TO_STUDIO_CALL_DONE)
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MUSIC_DRE_INV_H")
			PRINT_HELP("MUSIC_DRE_INV_H")
		ENDIF	
		SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MSMFS_PERFORMANCE_PRE_INVITATION)
	
	//IF IT WAS JUST A TEXT MESSAGE FROM FRANKLIN, PRINT HELP
	ELIF g_sMusicStudioDreWorkData.iNumberTimesNotified >1
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MUSIC_DRE_INV_H")
			PRINT_HELP("MUSIC_DRE_INV_H")
		ENDIF	
		SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MSMFS_CLEANUP) //don't invite after first time, skip to cleanup
	ENDIF
ENDPROC

PROC PROCESS_MESSAGE_FLOW_PRE_INVITATION_STATE()
	
	IF SHOULD_DRE_APPEAR_IN_MUSIC_STUDIO() 
		SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MSMFS_PERFORMANCE_STARTS_INVITATION)
	ENDIF
ENDPROC

PROC PROCESS_MESSAGE_FLOW_INVITATION_STATE()
	//IF PLAYER IS NOT IN THE MUSIC STUDIO, SEND THEM AN INVITATION FROM DRE
	IF NOT IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())	
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)	
			BROADCAST_NPC_INVITE_TO_SIMPLE_INTERIOR(PLAYER_ID(), SIMPLE_INTERIOR_MUSIC_STUDIO, CHAR_FIXER_PRODUCER, SI_EVENT_INVITE_NORMAL)		
			SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MSMFS_CLEANUP)
		ENDIF
	ELSE
		//OPTION TO INVITE ALL PLAYERS IN THE SESSION
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MUSIC_DRE_INV_OTH")
			PRINT_HELP("MUSIC_DRE_INV_OTH")
		ENDIF	
		SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MSMFS_CLEANUP)
	ENDIF
ENDPROC
PROC MAINTAIN_CHECK_FOR_STUDIO_INVITE_ACCEPT()
	IF Does_Basic_Invite_From_NPC_Exist(CHAR_FIXER_PRODUCER, FMMC_TYPE_SIMPLE_INTERIOR)
		
		IF Has_Player_Accepted_Basic_Invite_From_NPC(CHAR_FIXER_PRODUCER, FMMC_TYPE_SIMPLE_INTERIOR)
			
			SET_SIMPLE_INTERIOR_EVENT_AUTOWARP_ACTIVE(TRUE, SIMPLE_INTERIOR_MUSIC_STUDIO)
			g_SimpleInteriorData.bEventAutowarpOverrideCoords 			= FALSE
			g_SimpleInteriorData.bKillCarmodScriptAfterAcceptingInvite 	= TRUE
			g_SimpleInteriorData.bAcceptedInviteIntoSimpleInterior 		= TRUE
			
			INT iInviteEntryPoint = GET_SIMPLE_INTERIOR_INVITE_ENTRY_POINT(g_SimpleInteriorData.eEventAutowarpSimpleInterior)
			
			IF iInviteEntryPoint != -1
				SET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(iInviteEntryPoint)
			ENDIF
												
			SET_PLAYER_ACCEPTED_SIMPLE_INTERIOR_INVITE(TRUE)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, BS_SIMPLE_INTERIOR_GLOBAL_DATA_CONCEAL_MY_TRUCK)
			
			PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER][AUTOWARP] MAINTAIN_CHECK_FOR_STUDIO_INVITE_ACCEPT - Clearing BS2_SIMPLE_INTERIOR_PREVENT_WARP_PV_NEARBY")
			CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_PREVENT_WARP_PV_NEARBY)
			
			IF IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
			AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
				g_SimpleInteriorData.eSimpleInteriorIDToKeepKilledUntilWarpIsDone = GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()
				DO_SCREEN_FADE_OUT(SIMPLE_INTERIOR_SCREEN_FADE_TIME)
				
				PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER] MAINTAIN_CHECK_FOR_STUDIO_INVITE_ACCEPT - g_SimpleInteriorData.eSimpleInteriorIDToKeepKilledUntilWarpIsDone : ", GET_SIMPLE_INTERIOR_DEBUG_NAME(g_SimpleInteriorData.eSimpleInteriorIDToKeepKilledUntilWarpIsDone))
			ENDIF
			
			PRINTLN("[NET_DR_DRE_APPEARANCE_CONTROLLER] MAINTAIN_CHECK_FOR_STUDIO_INVITE_ACCEPT - ACCEPTIG INVITE AND STARTING AUTOWARP!")
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_DR_DRE_APPEARANCE_MESSAGE_FLOW()

	
	IF NOT SHOULD_PROCESS_DR_DRE_MESSAGE_FLOW()
		EXIT
	ENDIF
	
	
	SWITCH g_sMusicStudioDreWorkData.eMessageFlowState
		
		//THE MAIN UPDATE STATE BETWEEN PERFORMANCES,
		CASE MSMFS_INITIALIZE		
			PROCESS_MESSAGE_FLOW_INITIALIZE_STATE()			
		BREAK
		
		//CALL FROM DRE OR TEXT FROM FRANKLIN OR A TICKER
		CASE MSMFS_BEFORE_PERFORMANCE_NOTIFICATION				
			PROCESS_MESSAGE_FLOW_NOTIFICATION_STATE()
		BREAK
		
		//WAITING FOR PHONECALL TO END IF THERE WAS ONE AND SHOWING HELP TEXT
		CASE MSMFS_BEFORE_PERFORMANCE_NOTIFICATION_CLEANUP
			PROCESS_MESSAGE_FLOW_NOTIFICATION_CLEANUP_STATE()
		BREAK
		
		//WAITING FOR DRE TO APPEAR
		CASE MSMFS_PERFORMANCE_PRE_INVITATION
			PROCESS_MESSAGE_FLOW_PRE_INVITATION_STATE()
		BREAK
		
		//PLAYERS RECEIVE AN INVITATION FROM DRE TO WARP TO INTERIOR
		//IF PLAYERS ARE IN THE INTERIOR THEY GET A NOTIFICATION SUGGESTING TO INVITE OTHERS
		CASE MSMFS_PERFORMANCE_STARTS_INVITATION
			PROCESS_MESSAGE_FLOW_INVITATION_STATE()
		BREAK
		CASE MSMFS_CLEANUP
			SET_DRE_APPEARANCE_MESSAGE_FLOW_STATE(MSMFS_INITIALIZE)
		BREAK
	ENDSWITCH	
	
	MAINTAIN_CHECK_FOR_STUDIO_INVITE_ACCEPT()
	
ENDPROC



