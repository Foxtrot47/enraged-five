USING "rage_builtins.sch"
USING "globals.sch"

USING "Net_Missions_At_Coords.sch"
USING "Net_Missions_At_Coords_Public_Checks.sch"

USING "net_heists_public.sch"
 

// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Missions_At_Coords_Public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Handles all public access to the MP Mission At Coords routines.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Mission At Coords Registration Routines
// ===========================================================================================================

// PURPOSE:	Add a new mission to the array of missions being controlled
//
// INPUT PARAMS:			paramSourceID			The source of this mission request (re-using the Mission Controller sourceID)
//							paramCoords				The Coords for the mission
//							paramBlipDetails		The Blip Details for this mission
//							paramCorona				The corona details to be used
//							paramOptions			A struct containing all the per-mission options (Has Cutscene, Delete On Play, etc)
//							paramMissionIdData		The MissionId-Data for mission identification
// RETURN VALUE:			INT						A unique registration ID, or ILLEGAL_AT_COORDS_ID if registration failed
FUNC INT Register_MP_Mission_At_Coords(	g_eMPMissionSource		paramSourceID,
										VECTOR					paramCoords,
										g_structMatCBlipMP		paramBlipDetails,
										g_structMatCCoronaMP	paramCorona,
										g_structMatCOptionsMP	paramOptions,
										MP_MISSION_ID_DATA		paramMissionIdData)

	// Console log output
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 theHoursDescription = Convert_Hours_From_Bitfield_To_TL(paramCorona.theOnHours)
		
		NET_PRINT("...KGM MP [MatcReg]: Register")
		NET_PRINT(" [") 		NET_PRINT_VECTOR(paramCoords) NET_PRINT(" (radius: ") NET_PRINT_FLOAT(paramCorona.triggerRadius) NET_PRINT(")]")
		NET_PRINT(" [") 		NET_PRINT(GET_MP_MISSION_NAME(paramMissionIdData.idMission)) NET_PRINT(" (") NET_PRINT_INT(paramMissionIdData.idVariation) NET_PRINT(")]")
		NET_PRINT(" [") 		NET_PRINT(Convert_Mission_Source_To_String(paramSourceID)) NET_PRINT("]")
		NET_PRINT(" [Hours: ")	NET_PRINT(theHoursDescription) NET_PRINT("]")
		NET_NL()
		Debug_Output_MissionsAtCoords_Optional_Parameters(paramOptions)
	#ENDIF
	
	// KGM_HEIST_DLC: 3/9/13: Remove this blocking code when we want Heists to work again
//	IF (Is_FM_Cloud_Loaded_Activity_A_Heist(paramMissionIdData))
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("...KGM MP [At Coords]: KGM 3/9/13: IGNORING MISSION TAGGED AS A HEIST. ContentID: ") NET_PRINT(paramMissionIdData.idCloudFilename) NET_NL()
//			SCRIPT_ASSERT("Register_MP_Mission_At_Coords() - IGNORING MISSION TAGGED AS A HEIST")
//		#ENDIF
//		
//		RETURN (ILLEGAL_AT_COORDS_ID)
//	ENDIF
	
	// KGM 21/9/13: Don't register Gang Attacks if the player has transitioned into an Activity Session (ie: gone beyond the invite screen).
	//				Gang Attacks don't transition and shouldn't be active in an Activity Session, but they were causing a couple of problems
	//				by being active. Workaround solution: don't register them. This will work while Gang Attacks don't transition.
	IF (paramOptions.matcoUseAngledArea)
		IF (NETWORK_IS_ACTIVITY_SESSION())
			#IF IS_DEBUG_BUILD
				NET_PRINT(" - IGNORING AREA-TRIGGERED JOBS DURING ACTIVITY SESSION (prevents Gang Attacks activating after a mission has transitioned beyond the 'invite' stage)")
				NET_NL()
			#ENDIF
			
			RETURN (ILLEGAL_AT_COORDS_ID)
		ENDIF
	ENDIF
	
	// KGM 1/9/14: [EXPLOIT BUG 2002718] Check if the player was in the middle of this Gang Attack when they went to the store, if so tell Server to stick on on the history list to prevent re-triggering
	IF (paramMissionIdData.idMission = eFM_GANG_ATTACK_CLOUD)
		IF (g_gaContentIdHashActiveOnGotoStore != 0)
			IF NOT (IS_STRING_NULL_OR_EMPTY(paramMissionIdData.idCloudFilename))
				IF (g_gaContentIdHashActiveOnGotoStore = GET_HASH_KEY(paramMissionIdData.idCloudFilename))
					PRINTLN("...KGM MP [MatcReg][GangAttack]: Gang Attack was active for this player when player accessed Store - potential exploit (BUG 2002718)")
					BOOL potentialExploit = TRUE
					IF NOT (IS_BIT_SET(paramCorona.theOnHours, GET_CLOCK_HOURS()))
						PRINTLN("...KGM MP [MatcReg][GangAttack]: ...Gang Attack is outside active time - NO POTENTIAL EXPLOIT")
						potentialExploit = FALSE
					ENDIF
					
					INT gaLoop = 0
					IF (potentialExploit)
						// Is the Gang Attack on the server active list?
						REPEAT MAX_ACTIVE_GANG_ATTACKS gaLoop
							IF (potentialExploit)
								IF (GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[gaLoop].agasSharedMissionSlot != NO_ACTIVE_GANG_ATTACK_SLOT)
									// Found a filled Active Gang Attack slot
									IF (GET_HASH_KEY(GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasActive[gaLoop].agasCloudFilename) = g_gaContentIdHashActiveOnGotoStore)
										// ...found this Gang Attack on the active list, so no potential exploit - it's ok for the player to rejoin it as other players in the session are on it
										PRINTLN("...KGM MP [MatcReg][GangAttack]: ...Gang Attack is still active in the session so player can legally rejoin it - NO POTENTIAL EXPLOIT")
										potentialExploit = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
					
					IF (potentialExploit)
						// Is the Gang Attack on the server history list?
						REPEAT MAX_GANG_ATTACK_RECENT_HISTORY gaLoop
							IF (potentialExploit)
								IF NOT (IS_STRING_NULL_OR_EMPTY(GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[gaLoop].gahsCloudFilename))
									// Found a filled History Gang Attack slot
									IF (GET_HASH_KEY(GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks.gasHistory[gaLoop].gahsCloudFilename) = g_gaContentIdHashActiveOnGotoStore)
										// ...found this Gang Attack on the active list, so no potential exploit - it's ok for the player to rejoin it as other players in the session are on it
										PRINTLN("...KGM MP [MatcReg][GangAttack]: ...Gang Attack is still in the session's history so player won't be able to join it - NO POTENTIAL EXPLOIT")
										potentialExploit = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
						
					IF (potentialExploit)
						// KGM 1/9/14: BUG EXPLOIT 6098723 - TAKING THE EASY OPTION AND NOT SETTING UP THE GANG ATTACK FOR THE PLAYER
						PRINTLN("...KGM MP [MatcReg][GangAttack]: ...The conditions exist for a potential exploit. Ignoring this Gang Attack.")
						RETURN (ILLEGAL_AT_COORDS_ID)
					ELSE
						PRINTLN("...KGM MP [MatcReg][GangAttack]: ...There is no potential exploit. Setting g_gaContentIdHashActiveOnGotoStore = 0.")
						g_gaContentIdHashActiveOnGotoStore = 0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// 
	INT atCoordsID		= ILLEGAL_AT_COORDS_ID
	INT existingSlot	= Find_Slot_For_Mission_with_Matching_Details(paramSourceID, paramCoords, paramMissionIdData)
	
	IF (existingSlot != INVALID_MATC_SLOT)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ALREADY REGISTERED: [")
			Debug_Output_Missions_At_Coords_Slot_In_One_Line(existingSlot)
			NET_PRINT("]")
			NET_NL()
		#ENDIF
		
		// BUG 2004116: If the new data is 'Generated' and the existing is locally available data, then leave the locally available data untouched.
		IF (paramMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
			IF (g_sAtCoordsMP[existingSlot].matcMissionIdData.idVariation != GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
			AND NOT (Is_MissionsAtCoords_Mission_A_Shared_Mission(existingSlot))
				// The existing data is not Generated and not Shared, so must be locally available. The new data is Generated, so leave the existing data untouched.
				// Return existing REGID
				#IF IS_DEBUG_BUILD
					NET_PRINT("      - EXISTING is not Generated and not Shared, so Locally Available. The New request is Generated Data - leave the original data in place") NET_NL()
				#ENDIF
				
				atCoordsID = Get_MissionsAtCoords_RegistrationID_For_Mission(existingSlot)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("      Returning RegID: ") NET_PRINT_INT(atCoordsID) NET_NL()
				#ENDIF
				
				RETURN (atCoordsID)
			ENDIF
		ENDIF
		
		// BUG 1980956 had the same mission being registered as generated data twice and not matching so registering twice - it now matches and the second registration can be ignored
		IF (g_sAtCoordsMP[existingSlot].matcMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
		AND (paramMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
			// The existing data is Generated Data and the new data is also Generated, so just leave the original data in place
			// Return existing REGID
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - EXISTING is Generated Data, and new request is also Generated Data - leave the original data in place") NET_NL()
			#ENDIF
			
			atCoordsID = Get_MissionsAtCoords_RegistrationID_For_Mission(existingSlot)
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("      Returning RegID: ") NET_PRINT_INT(atCoordsID) NET_NL()
			#ENDIF
			
			RETURN (atCoordsID)
		ENDIF
		
		// BUG 1705950 is caused by generated data getting replaced by Shared Data, so don't replace it
		IF (g_sAtCoordsMP[existingSlot].matcMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
		AND (paramOptions.matcoSharedMission)
			// The existing data is Generated Data and the new data is Shared Mission data, so just leave the Generated data in place
			// Return existing REGID
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - EXISTING is Generated Data, and new request is Shared Mission data - leave the Generated Data in place") NET_NL()
			#ENDIF
			
			atCoordsID = Get_MissionsAtCoords_RegistrationID_For_Mission(existingSlot)
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("      Returning RegID: ") NET_PRINT_INT(atCoordsID) NET_NL()
			#ENDIF
			
			RETURN (atCoordsID)
		ENDIF
		
		// Remove 'refresh' flag if set 
		IF (Is_MissionsAtCoords_Mission_Marked_For_Refresh(existingSlot))
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - EXISTING HAS OPTION: MARKED FOR REFRESH") NET_NL()
			#ENDIF
			
			Mark_MissionsAtCoords_Mission_As_Not_For_Refresh(existingSlot)
		ELSE
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - existing does not have option: marked for refresh") NET_NL()
			#ENDIF
		ENDIF
		
		// 21/6/13: If either the new or the stored mission was from Generated Header Data then the CreatorID needs to be replaced
		IF (g_sAtCoordsMP[existingSlot].matcMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
		OR (paramMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
			// Update the CreatorID, the existing or the replacement mission is from Generated Header Data which won't be aware of the real creator for the mission so
			//		this needs updated to be consistent with the data being stored. If the existing data is Shared Mission Data, then we're converting to the Generated Data.
			g_sAtCoordsMP[existingSlot].matcMissionIdData.idCreator = paramMissionIdData.idCreator
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [At Coords]: The existing or the replacement header data is from Generated Header Data. Updating CreatorID for consistency to: ")
				NET_PRINT_INT(paramMissionIdData.idCreator)
				NET_NL()
			#ENDIF
		ENDIF
		
		// Remove 'delete on play' if now a permanent mission
		IF (Should_MissionsAtCoords_Mission_Be_Deleted_After_Play(existingSlot))
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - EXISTING HAS OPTION: DELETE AFTER PLAY") NET_NL()
			#ENDIF
			
			IF NOT (paramOptions.matcoDeleteOnPlay)
				// ...remove 'delete on play' option from this mission
				CLEAR_BIT(g_sAtCoordsMP[existingSlot].matcOptionBitflags, MATC_BITFLAG_DELETE_ON_PLAY)
					
				#IF IS_DEBUG_BUILD
					NET_PRINT("         (Removing 'delete on play' option from already registered mission. New request requires the mission to remain after play.)") NET_NL()
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("         (new registration request still requires delete after play - so option remains)") NET_NL()
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - existing does not have option: delete after play") NET_NL()
			#ENDIF
		ENDIF
		
		// Remove 'delete after focus' if no longer required
		IF (Should_MissionsAtCoords_Mission_Be_Deleted_After_Focus(existingSlot))
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - EXISTING HAS OPTION: DELETE AFTER FOCUS") NET_NL()
			#ENDIF
			
			IF NOT (paramOptions.matcoDeleteAfterFocus)
				// ...remove 'delete on walkout' option from this mission
				CLEAR_BIT(g_sAtCoordsMP[existingSlot].matcOptionBitflags, MATC_BITFLAG_DELETE_AFTER_FOCUS)
					
				#IF IS_DEBUG_BUILD
					NET_PRINT("         (Removing 'delete after focus' option from already registered mission. New request doesn't require it.)") NET_NL()
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("         (new registration request still requires delete after focus - so option remains)") NET_NL()
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - existing does not have option: delete after focus") NET_NL()
			#ENDIF
		ENDIF
		
		// Remove 'shared mission' if now being re-added by another game routine
		IF (Is_MissionsAtCoords_Mission_A_Shared_Mission(existingSlot))
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - EXISTING HAS OPTION: SHARED MISSION") NET_NL()
			#ENDIF
			
			IF NOT (paramOptions.matcoSharedMission)
				// ...remove 'shared mission' option from this mission
				CLEAR_BIT(g_sAtCoordsMP[existingSlot].matcOptionBitflags, MATC_BITFLAG_ADDED_AS_SHARED_MISSION)
					
				#IF IS_DEBUG_BUILD
					NET_PRINT("         (Removing 'shared mission' option from already registered mission. New request isn't a Shared Mission.)") NET_NL()
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("         (new registration request still requires 'shared mission' - so option remains)") NET_NL()
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - existing does not have option: shared mission") NET_NL()
			#ENDIF
		ENDIF
		
		// Whether or not the 'delete on play' was removed above - set it here if the new data is Generated Header Data because this is temporary mission data
		IF (paramMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
			IF NOT (IS_BIT_SET(g_sAtCoordsMP[existingSlot].matcOptionBitflags, MATC_BITFLAG_DELETE_ON_PLAY))
				SET_BIT(g_sAtCoordsMP[existingSlot].matcOptionBitflags, MATC_BITFLAG_DELETE_ON_PLAY)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("      - Generated Header Data is replacing the existing data - setting 'Delete After Play'") NET_NL()
				#ENDIF
			ENDIF
		ENDIF
		
		// If the new data is Generated Header Data then mark it as ignoring cloud refreshes
		IF (paramMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
			// ...the new data is Generated Header Data
			IF NOT (IS_BIT_SET(g_sAtCoordsMP[existingSlot].matcOptionBitflags, MATC_BITFLAG_IGNORE_REFRESH))
				SET_BIT(g_sAtCoordsMP[existingSlot].matcOptionBitflags, MATC_BITFLAG_IGNORE_REFRESH)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("      - Generated Header Data is replacing the existing data - setting 'Ignore Refresh'") NET_NL()
				#ENDIF
			ENDIF
		ENDIF
		
		// If the existing data is Generated Header Data and the new data is locally available data then obey the passed in cloud refresh option
		IF (Get_MissionsAtCoords_Slot_Mission_Variation(existingSlot) = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
		AND (paramMissionIdData.idVariation != GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
			// ...the new data is Locally Available Data replacing Generated Header Data, so obey the required cloud refresh flag
			IF (paramOptions.matcoIgnoreRefresh)
				SET_BIT(g_sAtCoordsMP[existingSlot].matcOptionBitflags, MATC_BITFLAG_IGNORE_REFRESH)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("      - Locally Available or Shared Data is replacing Generated Header Data - 'Ignore Refresh' option is requested, so setting") NET_NL()
				#ENDIF
			ELSE
				CLEAR_BIT(g_sAtCoordsMP[existingSlot].matcOptionBitflags, MATC_BITFLAG_IGNORE_REFRESH)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("      - Locally Available or Shared Data is replacing Generated Header Data - 'Ignore Refresh' option is not requested, so clearing") NET_NL()
				#ENDIF
			ENDIF
		ENDIF
		
		// If the CreatorIDs are different (ignoring generated header data which is handled earlier), then update to the highest priority copy of the mission: Rockstar first, then UGC
		// NOTE: This can happen if a player bookmarks a Rockstar Created mission. The bookmarked version gets stored as UGC first then the Rockstar version is marked as a duplicate
		//			but updates the variation below leading to arraay overruns on the UGC array because it is accessing using the Rockstar Array position.
		IF (g_sAtCoordsMP[existingSlot].matcMissionIdData.idCreator != paramMissionIdData.idCreator)
			// ...different creators for a duplicate contentID
			// Ignore if either version is generated header data or if the new data is Shared Mission Data (because it won't stick around)
			IF (Get_MissionsAtCoords_Slot_Mission_Variation(existingSlot) != GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
			AND (paramMissionIdData.idVariation != GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
			AND NOT (paramOptions.matcoSharedMission)
				// ...not generated header data, so prioritise Rockstar Created Content over Rockstar Verified Content over UGC/Bookmarked Content for storage
				// Prioritise Rockstar Created, then Rockstar Verified. UGC/Bookmarks shouldn't supersede either of these.
				IF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CREATOR_ID)
					// ...Rockstar Created takes priority over all
					g_sAtCoordsMP[existingSlot].matcMissionIdData.idCreator		= paramMissionIdData.idCreator
					g_sAtCoordsMP[existingSlot].matcMissionIdData.idVariation	= paramMissionIdData.idVariation
				
					#IF IS_DEBUG_BUILD
						NET_PRINT("...KGM MP [At Coords]: Rockstar Created version of the mission replaces the existing version - corona/blip colours remains unchanged. Variation updated to: ")
						NET_PRINT_INT(paramMissionIdData.idVariation)
						NET_NL()
					#ENDIF
				ELIF (paramMissionIdData.idCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
					// ...Rockstar Verified takes priority over UGC/Bookmarks
					IF (g_sAtCoordsMP[existingSlot].matcMissionIdData.idCreator = FMMC_ROCKSTAR_CREATOR_ID)
						// ...existing is Rockstar Created, so leave it's data in place
						#IF IS_DEBUG_BUILD
							NET_PRINT("...KGM MP [At Coords]: Rockstar Verified version of the mission does not replace the existing Rockstar Created version.")
							NET_NL()
						#ENDIF
					ELSE
						// ...existing must be UGC/Bookmarked, so replace the data
						g_sAtCoordsMP[existingSlot].matcMissionIdData.idCreator		= paramMissionIdData.idCreator
						g_sAtCoordsMP[existingSlot].matcMissionIdData.idVariation	= paramMissionIdData.idVariation
					
						#IF IS_DEBUG_BUILD
							NET_PRINT("...KGM MP [At Coords]: Rockstar Verified version of the mission replaces the existing version - corona/blip colours remains unchanged. Variation updated to: ")
							NET_PRINT_INT(paramMissionIdData.idVariation)
							NET_NL()
						#ENDIF
					ENDIF
				ELIF (paramMissionIdData.idCreator < NUM_NETWORK_PLAYERS)
					// ...UGC does not replace existing version
					#IF IS_DEBUG_BUILD
						NET_PRINT("...KGM MP [At Coords]: UGC/Bookmarked version of the mission does not replace the existing Rockstar Created or Rockstar Verified version.")
						NET_NL()
					#ENDIF
				ELSE
					// ...Unknown
					#IF IS_DEBUG_BUILD
						NET_PRINT("...KGM MP [At Coords]: Unknown CreatorID does not replace any version. CreatorID: ")
						NET_PRINT_INT(paramMissionIdData.idCreator)
						NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// FINALLY: the location of missions with cloud data may change during a cloud refresh, so update the variation if this has changed
		//			because the variation is the array position of the mission header data within the global cloud arrays
		// 21/6/13: This may also now happen if Generated data is replacing locally available data or vice-versa
		// 26/8/13: Only do this if the creatorIDs are the same - if they're not the same this will be handled elsewhere
		//			A side-effect of UGC and Bookmarked missions being on the same array is that a bookmarked version of your own UGC will update to the Bookmarked version - this should be ok
		// 27/6/14: [BUG 1907598] Don't do this if the existing data is Generated and the new data is Shared - leave the variation as Generated
		IF (g_sAtCoordsMP[existingSlot].matcMissionIdData.idCreator = paramMissionIdData.idCreator)
			IF (Get_MissionsAtCoords_Slot_Mission_Variation(existingSlot) != paramMissionIdData.idVariation)
				// Check for other exclusion conditions
				BOOL allowVariationUpdate = TRUE
				
				IF (Get_MissionsAtCoords_Slot_Mission_Variation(existingSlot) = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
				AND (paramOptions.matcoSharedMission)
					allowVariationUpdate = FALSE
					PRINTLN("...KGM MP [At Coords]: Don't update the variation. The existing data is 'generated' and the new data is 'Shared', so still not locally available")
				ENDIF
				
				IF (allowVariationUpdate)
					// Update the variation, the array position of the mission has changed during the refresh
					g_sAtCoordsMP[existingSlot].matcMissionIdData.idVariation = paramMissionIdData.idVariation
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("...KGM MP [At Coords]: Mission Header Data Position on Global Cloud Array has changed during Cloud Refresh. Updated to: ")
						NET_PRINT_INT(paramMissionIdData.idVariation)
						NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// If the mission is marked as 'played' after the refresh then ensure this gets stored
		IF (paramOptions.matcoHasBeenPlayed)
			SET_BIT(g_sAtCoordsMP[existingSlot].matcStateBitflags, MATC_BITFLAG_MISSION_HAS_BEEN_PLAYED)
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - EXISTING HAS OPTION: MARK AS PLAYED - so ensure this gets set in case it has updated") NET_NL()
			#ENDIF
		ENDIF
		
		//Fixes issue with strand missions if the corona is already created with a small trigger radius
		IF g_sAtCoordsMP[existingSlot].matcCorona.triggerRadius != paramCorona.triggerRadius
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - EXISTING HAS DIFFERENT TRIGGER RADIUS ") 
				NET_PRINT_FLOAT(g_sAtCoordsMP[existingSlot].matcCorona.triggerRadius) 
				NET_PRINT(" new:  ") 
				NET_PRINT_FLOAT(paramCorona.triggerRadius) 
				NET_NL()
			#ENDIF
			g_sAtCoordsMP[existingSlot].matcCorona.triggerRadius = paramCorona.triggerRadius
		ENDIF
		
		// Return existing REGID
		atCoordsID = Get_MissionsAtCoords_RegistrationID_For_Mission(existingSlot)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("      Returning RegID: ") NET_PRINT_INT(atCoordsID) NET_NL()
		#ENDIF
		
		RETURN (atCoordsID)
	ENDIF

//	// Player OK?
//	IF NOT (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("      FAILED: Player is not OK.") NET_NL()
//		#ENDIF
//		
//		RETURN ILLEGAL_AT_COORDS_ID
//	ENDIF
	
	// Valid Mission?
	IF (paramMissionIdData.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("      FAILED: MissionID is eNULL_MISSION.") NET_NL()
			SCRIPT_ASSERT("Register_MP_Mission_At_Coords(): Attempting to Register eNULL_MISSION. See Console Log. Tell Keith.")
		#ENDIF
		
		RETURN ILLEGAL_AT_COORDS_ID
	ENDIF
	
	// Valid Source ID?
	IF (paramSourceID = UNKNOWN_MISSION_SOURCE)
		#IF IS_DEBUG_BUILD
			NET_PRINT("      INCONSISTENCY: Mission Source ID = UNKNOWN_MISSION_SOURCE.") NET_NL()
			SCRIPT_ASSERT("Register_MP_Mission_At_Coords(): ERROR: Mission Source ID = UNKNOWN_MISSION_SOURCE. Changing to MP_MISSION_SOURCE_MISSION_FLOW_BLIP to allow scripts to continue. Tell Keith.")
		#ENDIF
		
		paramSourceID = MP_MISSION_SOURCE_MISSION_FLOW_BLIP
	ENDIF
	
	// Valid Triggering Radius?
	IF (paramCorona.triggerRadius < MATC_RANGE_MINIMUM_TRIGGER_RADIUS_m)
		#IF IS_DEBUG_BUILD
			NET_PRINT("      INCONSISTENCY: Corona Trigger Radius < ") NET_PRINT_FLOAT(MATC_RANGE_MINIMUM_TRIGGER_RADIUS_m) NET_PRINT("m - THIS IS PROBABLY TOO SMALL") NET_NL()
			SCRIPT_ASSERT("Register_MP_Mission_At_Coords(): ERROR: Corona Trigger Radius is too small. Ignororing request to register mission. Tell Keith.")
		#ENDIF
		
		RETURN ILLEGAL_AT_COORDS_ID
	ENDIF
	
	// DEBUG ONLY: Check for missions near the origin - these are almost certainly being placed in error
	#IF IS_DEBUG_BUILD
		IF (GET_DISTANCE_BETWEEN_COORDS(paramCoords, << 0.0, 0.0, 0.0 >>) < 0.5)
			NET_PRINT("      FAILED: Coords are within 0.5m of the origin.") NET_NL()
			
			SCRIPT_ASSERT("Register_MP_Mission_At_Coords(): Attempting to Register a mission very close to the origin. Ignororing request to register mission. Tell Keith.")
			RETURN ILLEGAL_AT_COORDS_ID
		ENDIF
	#ENDIF
	
	// Check if there is any room on the array
	IF (g_numAtCoordsMP >= MAX_NUM_MP_MISSIONS_AT_COORDS)
		#IF IS_DEBUG_BUILD
			NET_PRINT("      FAILED: There is no room for another mission. Max Missions = ") NET_PRINT_INT(MAX_NUM_MP_MISSIONS_AT_COORDS) NET_NL()
			SCRIPT_ASSERT("Register_MP_Mission_At_Coords(): There is not room for another mission. Increase MAX_NUM_MP_MISSIONS_AT_COORDS. Tell Keith.")
		#ENDIF
		
		RETURN ILLEGAL_AT_COORDS_ID
	ENDIF
	
	// Clear the array position, then stick these mission details at the end of the array
	// NOTE: I should only need to add a completely new entry to the array in this function, so I'll do all the updates manually in this function
	INT newSlot = g_numAtCoordsMP
	Clear_One_MP_Mission_At_Coords_Variables(newSlot)
	g_sAtCoordsMP[newSlot].matcStage			= MATCS_STAGE_NEW
	g_sAtCoordsMP[newSlot].matcSourceID			= paramSourceID
	g_sAtCoordsMP[newSlot].matcCoords			= paramCoords
	g_sAtCoordsMP[newSlot].matcCorona			= paramCorona
	g_sAtCoordsMP[newSlot].matcMissionIdData	= paramMissionIdData
	
	// Generate and store the HASH value from the cloudFilename for each mission
	if IS_STRING_NULL_OR_EMPTY(g_sAtCoordsMP[newSlot].matcMissionIdData.idCloudFilename)
		g_sAtCoordsMP[newSlot].matcIdCloudnameHash	= 0 
	else
		g_sAtCoordsMP[newSlot].matcIdCloudnameHash	= GET_HASH_KEY( g_sAtCoordsMP[newSlot].matcMissionIdData.idCloudFilename )
		net_NL()NET_PRINT("...CV MP register HASH: ")NET_PRINT_INT(g_sAtCoordsMP[newSlot].matcIdCloudnameHash)NET_PRINT(" cloud file name: ")NET_PRINT(g_sAtCoordsMP[newSlot].matcMissionIdData.idCloudFilename) NET_NL()
	endif
	
	
	// Setup the Blip
	g_sAtCoordsMP[newSlot].matcBlipIndex	= Setup_MissionsAtCoords_Blip_Undisplayed(paramBlipDetails, paramCoords)
	IF (g_sAtCoordsMP[newSlot].matcBlipIndex = NULL)
		// There has been a problem adding a new blip
		#IF IS_DEBUG_BUILD
			NET_PRINT("      FAILED: The Mission Blip did not get added so cancelling this mission request.") NET_NL()
			SCRIPT_ASSERT("Register_MP_Mission_At_Coords(): Failed to setup a new MissionsAtCoords blip. See Console Log. Tell Keith.")
		#ENDIF
		
		Clear_One_MP_Mission_At_Coords_Variables(newSlot)
		RETURN ILLEGAL_AT_COORDS_ID
	ENDIF
	
	// Setup the Blip Flash requirement based on whether the activity is locked or unlocked (a blip should flash for a period of time when first unlocked)
	BOOL waitForSecondaryUnlock = paramOptions.matcoSecondaryUnlock
	IF	(Check_If_Mission_Locked(paramMissionIdData, waitForSecondaryUnlock))
	AND	(Check_If_Mission_Blip_Should_Flash_When_Unlocked(paramMissionIdData.idMission))
		Set_MissionsAtCoords_Blip_As_Flash(newSlot)
	ELSE
		Set_MissionsAtCoords_Blip_As_No_Flash(newSlot)
	ENDIF
	
	// Any option flags
	Store_MissionsAtCoords_Options(newSlot, paramOptions)
	
	// Generate a Registration ID used for communication after registration and store it
	atCoordsID = Generate_At_Coords_RegistrationID()
	g_sAtCoordsMP[newSlot].matcRegID = atCoordsID
	
	// Check if there is an Angled Area that needs stored - do this after the regID is generated
	IF (paramOptions.matcoUseAngledArea)
		Store_MissionsAtCoords_Angled_Area(atCoordsID, paramCoords, paramOptions)
	ENDIF
	
	// Increase the 'new missions' banding
	// Are there any new missions already stored?
	IF (g_sAtCoordsBandsMP[MATCB_NEW_MISSIONS].matcbNumInBand > 0)
		// ...there are already new entries in the array, so just need to increase the last slot used and the total
		g_sAtCoordsBandsMP[MATCB_NEW_MISSIONS].matcbLastSlot++
		g_sAtCoordsBandsMP[MATCB_NEW_MISSIONS].matcbNumInBand++
	ELSE
		// ...there are no entries in the New Missions band, so add new details
		g_sAtCoordsBandsMP[MATCB_NEW_MISSIONS].matcbNumInBand	= 1
		g_sAtCoordsBandsMP[MATCB_NEW_MISSIONS].matcbFirstSlot	= newSlot
		g_sAtCoordsBandsMP[MATCB_NEW_MISSIONS].matcbLastSlot	= newSlot
		g_sAtCoordsBandsMP[MATCB_NEW_MISSIONS].matcbUpdateSlot	= newSlot
	ENDIF

	// Increase the max entries in the array
	g_numAtCoordsMP++
	
	// If this mission is getting setup because the game is transitioning from the InCorona Invite screen to the InCorona Betting screen, then ensure the mission
	// gets temporarily unlocked to allow a low-ranked character to get the corona again - IS_TRANSITION_SESSION_RESTARTING() covers this specific type of transition
	IF (IS_TRANSITION_SESSION_RESTARTING())
		TEXT_LABEL_23 missionRestarting = GET_TRANSITION_SESSION_LOADED_FILE_NAME()
		IF NOT (IS_STRING_NULL_OR_EMPTY(missionRestarting))
			IF (ARE_STRINGS_EQUAL(missionRestarting, paramMissionIdData.idCloudFilename))
				#IF IS_DEBUG_BUILD
					NET_PRINT("           This Mission is being relaunched because TRANSITION SESSION RESTARTING - marking as temporarily unlocked and invite accepted")
					NET_NL()
				#ENDIF
				
				IF (Is_MissionsAtCoords_Mission_Locked(newSlot))
					Set_MissionsAtCoords_Mission_As_Temporarily_Unlocked(newSlot)
					Maintain_MissionsAtCoords_Lock_Status_For_Mission(newSlot)
				ENDIF
				
				Set_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(newSlot)
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      ADDED: ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(newSlot)
		NET_NL()
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		Do_Missions_At_Coords_Banding_Array_Consistency_Checking()
	#ENDIF
	
	RETURN atCoordsID

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mark a mission with a specific RegistrationID for delete - this will obey the usual rules for deletion ensuring it can't be deleted at an inappropriate time
//
// INPUT PARAMS:			paramRegID			The registrationID for this mission entry
PROC Mark_MP_Mission_At_Coords_For_Delete(INT paramRegID)

	// Console log output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Mark_MP_Mission_At_Coords_For_Delete() from ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT("  [RegID: ")
		NET_PRINT_INT(paramRegID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF

	INT theSlot = Get_MissionsAtCoords_Current_Slot_From_RegistrationID(paramRegID)
	IF (theSlot = INVALID_MATC_SLOT)
		// Registration ID doesn't exist
		#IF IS_DEBUG_BUILD
			NET_PRINT("      FAILED: Registration ID doesn't exist.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Mark the mission for deletion - this will occur during its next update
	Mark_MissionsAtCoords_Mission_For_Delete(theSlot)
			
	// Require the slot to do a forced update
	Set_MissionsAtCoords_Mission_As_Force_Update(theSlot)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mark a mission with a specific RegistrationID for delete if it has the 'Delete On Play' flag set but ignore if it doesn't
//
// INPUT PARAMS:			paramRegID				The registrationID for this mission entry
//							paramSharedMission		[DEFAULT = FALSE] TRUE if this should only delete a Shared Mission, otherwise FALSE
PROC Mark_MP_Mission_At_Coords_For_Delete_If_Temporarily_Setup(INT paramRegID, BOOL paramSharedMission = FALSE)

	// Console log output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Mark_MP_Mission_At_Coords_For_Delete_If_Temporarily_Setup() from ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT("  [RegID: ")
		NET_PRINT_INT(paramRegID)
		NET_PRINT("]")
		IF (paramSharedMission)
			NET_PRINT(" - ONLY IF A SHARED MISSION")
		ENDIF
		NET_NL()
	#ENDIF

	INT theSlot = Get_MissionsAtCoords_Current_Slot_From_RegistrationID(paramRegID)
	IF (theSlot = INVALID_MATC_SLOT)
		// Registration ID doesn't exist
		#IF IS_DEBUG_BUILD
			NET_PRINT("      FAILED: Registration ID doesn't exist.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Check if the mission was setup as a Shared Mission
	IF (paramSharedMission)
		IF NOT (Is_MissionsAtCoords_Mission_A_Shared_Mission(theSlot))
			// Mission is not marked as 'shared mission'
			#IF IS_DEBUG_BUILD
				NET_PRINT("      IGNORE: Mission should only delete if a 'shared mission'. Not a Shared Mission.") NET_NL()
			#ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	// Check if the mission is temporary (ie: is marked as delete on play)
	IF NOT (Should_MissionsAtCoords_Mission_Be_Deleted_After_Play(theSlot))
		// Mission is not marked as 'delete on play' so it must be set up permanently
		#IF IS_DEBUG_BUILD
			NET_PRINT("      IGNORE: Mission is not temporarily setup (it is not marked as 'delete on play').") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// The mission should be marked for delete, but leave it alone if it is being 'played again'
	IF (Is_MissionsAtCoords_Mission_Being_Played_Again(theSlot))
		// Mission is marked as 'delete' but being 'played again'
		#IF IS_DEBUG_BUILD
			NET_PRINT("      IGNORE: Mission is marked as 'delete after play' - but is being 'played again' so don't delete.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Mark the mission for deletion - this will occur during its next update
	Mark_MissionsAtCoords_Mission_For_Delete(theSlot)
			
	// Require the slot to do a forced update
	Set_MissionsAtCoords_Mission_As_Force_Update(theSlot)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mark mission with this ContentID for delete
//
// INPUT PARAMS:			paramContentID			The contentID of the mission to be marked for delete
PROC Mark_MP_Mission_At_Coords_With_This_ContentID_For_Delete(TEXT_LABEL_23 paramContentID)

	// Console log output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Mark_MP_Mission_At_Coords_With_This_ContentID_For_Delete() from ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT("  [ContentID: ")
		NET_PRINT(paramContentID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	// Valid Content ID?
	IF (IS_STRING_NULL_OR_EMPTY(paramContentID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("         ERROR: ContentID is NULL or EMPTY") NET_NL()
			SCRIPT_ASSERT("Mark_MP_Mission_At_Coords_With_This_ContentID_For_Delete(): ERROR: Mission Content ID = NULL or EMPTY. See console log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF

	// Loop through all entries and mark any with this contentID for deletion
	INT contentIdHash = GET_HASH_KEY(paramContentID)
	
	INT tempLoop = 0
	REPEAT g_numAtCoordsMP tempLoop
		IF (g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = contentIdHash)
			Mark_MissionsAtCoords_Mission_For_Delete(tempLoop)
			
			// Require the slot to do a forced update
			Set_MissionsAtCoords_Mission_As_Force_Update(tempLoop)
			
			EXIT
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mark all missions from a specific source for delete
//
// INPUT PARAMS:			paramSourceID			The source of this mission request (re-using the Mission Controller sourceID)
//
// NOTES:	This may be a temporary while the functionality is being built up - it is added because there could be multiple CnC Races associated with the Mission Flow Races strand
PROC Mark_All_MP_Missions_At_Coords_From_This_Source_For_Delete(g_eMPMissionSource paramSourceID)

	// Console log output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Mark_All_MP_Missions_At_Coords_From_This_Source_For_Delete() from ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT("  [SourceID: ")
		NET_PRINT(Convert_Mission_Source_To_String(paramSourceID))
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	// Valid Source ID?
	IF (paramSourceID = UNKNOWN_MISSION_SOURCE)
		#IF IS_DEBUG_BUILD
			NET_PRINT("         ERROR: Mission Source ID = UNKNOWN_MISSION_SOURCE.") NET_NL()
			SCRIPT_ASSERT("Mark_All_MP_Missions_At_Coords_From_This_Source_For_Delete(): ERROR: Mission Source ID = UNKNOWN_MISSION_SOURCE. See console log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF

	// Loop through all entries and mark all from the specified source for deletion
	INT tempLoop = 0
	REPEAT g_numAtCoordsMP tempLoop
		IF (g_sAtCoordsMP[tempLoop].matcSourceID = paramSourceID)
			Mark_MissionsAtCoords_Mission_For_Delete(tempLoop)
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Mark all cloud-loaded missions as being refreshed
PROC Allow_Refresh_Of_All_MP_Missions_At_Coords_Cloud_Loaded_Missions()

	// This could be called multiple times in successive frames - only do it once
	IF (Is_Cloud_Loaded_Mission_Data_Being_Refreshed())
		EXIT
	ENDIF

	// Console log output
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Allow_Refresh_Of_All_MP_Missions_At_Coords_Cloud_Loaded_Missions() from ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF

	// Loop through all entries and mark all cloud-loaded missions for refresh
	MP_MISSION	thisMissionID		= eNULL_MISSION
	INT			thisVariation		= NO_MISSION_VARIATION
	INT			tempLoop			= 0
	
	REPEAT g_numAtCoordsMP tempLoop
		thisMissionID	= Get_MissionsAtCoords_Slot_MissionID(tempLoop)
		thisVariation	= Get_MissionsAtCoords_Slot_Mission_Variation(tempLoop)

		IF (Does_MP_Mission_Variation_Get_Data_From_The_Cloud(thisMissionID, thisVariation))
		AND NOT (Should_MissionsAtCoords_Mission_Ignore_Cloud_Refresh(tempLoop))
			Mark_MissionsAtCoords_Mission_For_Refresh(tempLoop)
		ENDIF
	ENDREPEAT
	
	// Mark the MissionAtCoords system as having data downloaded and refreshed
	g_sAtCoordsControlsMP.matccDownloadInProgress	= TRUE
	g_sAtCoordsControlsMP.matccRefreshInProgress	= TRUE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	All Cloud-Loaded data has been downloaded, so allow the refreshing of the data to take place
//
// INPUT PARAMS:		paramInitialCloudLoad		[DEFAULT = FALSE] TRUE if this is the initial cloud load, FALSE if an in-game cloud load
PROC Mission_At_Coords_Cloud_Loaded_Data_Has_Been_Downloaded()

	// Ensure a download is in progress
	IF NOT (Is_Mission_At_Coords_Cloud_Loaded_Data_Being_Downloaded())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: Missions_At_Coords_Cloud_Loaded_Data_Has_Been_Downloaded(): A download was not known to be in progress.") NET_NL()
			NET_PRINT("       NOTE: This may be because SHIFT-NUMPAD-3 was pressed after the data was downloaded but before it was all refreshed") NET_NL()
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Missions_At_Coords_Cloud_Loaded_Data_Has_Been_Downloaded: All downloaded, allow data refresh to take place")
		IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
			NET_PRINT(" - INITIAL CLOUD LOAD")
		ENDIF
		NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	// Mark the MissionAtCoords system as no longer being refreshed
	g_sAtCoordsControlsMP.matccDownloadInProgress	= FALSE
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	All Cloud-Loaded data has been refreshed, so any missions STILL marked for refresh no longer exist and should be Marked For Delete
PROC Mission_At_Coords_Cloud_Loaded_Data_Has_Been_Refreshed()

	// Ensure a download is not in progress
	IF (Is_Mission_At_Coords_Cloud_Loaded_Data_Being_Downloaded())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: Missions_At_Coords_Cloud_Loaded_Data_Has_Been_Refreshed(): ERROR - A download is still in progress.") NET_NL()
			DEBUG_PRINTCALLSTACK()
			SCRIPT_ASSERT("Missions_At_Coords_Cloud_Loaded_Data_Has_Been_Refreshed() - ERROR: A download is still in progress. See Console Log. Marking download as complete (this may cause issues). Tell Keith.")
		#ENDIF
	
		// Mark the download as having completed so that the game can continue - there may be issues caused by this
		Mission_At_Coords_Cloud_Loaded_Data_Has_Been_Downloaded()
	ENDIF
	
	// Ensure a cloud refresh is in progress
	IF NOT (Is_Cloud_Loaded_Mission_Data_Being_Refreshed())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: Missions_At_Coords_Cloud_Loaded_Data_Has_Been_Refreshed(): ERROR - A cloud data refresh is not in progress.") NET_NL()
			DEBUG_PRINTCALLSTACK()
			SCRIPT_ASSERT("Missions_At_Coords_Cloud_Loaded_Data_Has_Been_Refreshed() - ERROR: A cloud data refresh is not in progress. Tell Keith.")
		#ENDIF
	
		EXIT
	ENDIF

	// Delete all cloud-loaded entries that are no longer valid
	INT tempLoop = 0
	
	REPEAT g_numAtCoordsMP tempLoop
		IF (Is_MissionsAtCoords_Mission_Marked_For_Refresh(tempLoop))
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [At Coords]: After Refresh, found mission still Marked For Refresh. Now Marking For Delete. Slot ") NET_PRINT_INT(tempLoop) NET_NL()
			#ENDIF
			
			Mark_MissionsAtCoords_Mission_As_Not_For_Refresh(tempLoop)
			Mark_MissionsAtCoords_Mission_For_Delete(tempLoop)
		ENDIF
	ENDREPEAT
	
	// Output a message when cloud content has been refreshed unless this is an initial cloud load
	#IF IS_DEBUG_BUILD
		IF (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
			IF NOT (IS_HELP_MESSAGE_ON_SCREEN())
				PRINT_HELP("CLOUD_REFRESHED")
			ENDIF
		ENDIF
	#ENDIF

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Missions_At_Coords_Cloud_Loaded_Data_Has_Been_Refreshed: All refreshed")
		IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
			NET_PRINT(" - INITIAL CLOUD LOAD")
		ENDIF
		NET_NL()
		DEBUG_PRINTCALLSTACK()
		Debug_Output_Missions_At_Coords_Banding_Array()
	#ENDIF
	
	// Always, indicate that the Initial Data is ready (if initial data was previously loaded, then setting to TRUE again won't matter)
	g_sAtCoordsControlsMP.matccInitialDataReady 	= TRUE
	
	// Mark the MissionAtCoords system as no longer being refreshed
	g_sAtCoordsControlsMP.matccRefreshInProgress	= FALSE	

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a specific MissionsAtCoords registrationID is the current focus mission
//
// INPUT PARAMS:			paramRegID			The registrationID for this mission entry
// RETURN VALUE:			BOOL				TRUE if the specified RegID is the focus mission, otherwise FALSE
FUNC BOOL Is_This_Missions_At_Coords_RegID_The_Focus_Mission(INT paramRegID)

	IF NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
		RETURN FALSE
	ENDIF
	
	// There is a focus mission, so compare the regIDs
	RETURN (paramRegID = Get_MissionsAtCoords_RegistrationID_For_Mission(Get_MissionsAtCoords_Focus_Mission_Slot()))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a the player is actively on the Focus Mission
//
// RETURN VALUE:			BOOL				TRUE if the player is actively on the Focus Mission, otherwise FALSE
FUNC BOOL Is_Player_Actively_On_The_Focus_Mission()

	IF NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
		RETURN FALSE
	ENDIF
	
	// There is a focus mission, so check if the player is 'on mission'
	RETURN (Get_MissionsAtCoords_Stage_For_Mission(Get_MissionsAtCoords_Focus_Mission_Slot()) = MATCS_ON_MISSION)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Shared RegID for the current focus mission
//
// RETURN VALUE:			INT				The SharedRegID of the current focus mission, or ILLEGAL_SHARED_REG_ID
FUNC INT Get_Missions_At_Coords_SharedRegID_For_Focus_Mission()

	IF NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
		RETURN ILLEGAL_SHARED_REG_ID
	ENDIF
	
	// There is a focus mission, so return the SharedRegIDs
	RETURN (Get_MissionsAtCoords_Slot_SharedRegID(Get_MissionsAtCoords_Focus_Mission_Slot()))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the UniqueID for the current focus mission
//
// RETURN VALUE:			INT				The UniqueID of the current focus mission, or NO_UNIQUE_ID
FUNC INT Get_Missions_At_Coords_UniqueID_For_Focus_Mission()

	IF NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
		RETURN NO_UNIQUE_ID
	ENDIF
	
	// There is a focus mission, so return the UniqueID
	RETURN (g_sAtCoordsFocusMP.focusUniqueID)

ENDFUNC



// ===========================================================================================================
//      Mission At Coords Feedback Public Access Routines
// ===========================================================================================================

// PURPOSE:	Check if a MissionsAtCoords mission has started (for this player)
//
// INPUT PARAMS:			paramRegID			The registrationID for this mission entry
// RETURN VALUE:			BOOL				TRUE if the player is reported as having started the mission, otherwise FALSE
FUNC BOOL Is_This_AtCoords_Mission_Started(INT paramRegID)
	RETURN (Does_MissionsAtCoords_Feedback_Exist(paramRegID, MATCFBT_STARTED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a MissionsAtCoords mission has finished
//
// INPUT PARAMS:			paramRegID			The registrationID for this mission entry
// RETURN VALUE:			BOOL				TRUE if the mission is reported as finished, otherwise FALSE
FUNC BOOL Is_This_AtCoords_Mission_Finished(INT paramRegID)
	RETURN (Does_MissionsAtCoords_Feedback_Exist(paramRegID, MATCFBT_FINISHED))
ENDFUNC




// ===========================================================================================================
//      Mission At Coords Blip Data Public Access Routines
// ===========================================================================================================

// PURPOSE:	To clear out all data in the Blip Data struct
//
// RETURN PARAMS:			paramBlipData		The Blip Data struct to be cleared
PROC Clear_All_MissionsAtCoords_BlipData(g_structMatCBlipMP &paramBlipData)

	g_structMatCBlipMP emptyBlipData
	paramBlipData = emptyBlipData
	
	paramBlipData.theBlipName				= ""
	paramBlipData.thePlayerNameForBlip		= ""
	paramBlipData.alternativeBlipCoords		= << 0.0, 0.0, 0.0 >>
	
ENDPROC




// ===========================================================================================================
//      Mission At Coords Corona Data Public Access Routines
// ===========================================================================================================

// PURPOSE:	To clear out all data in the Corona Data struct
//
// RETURN PARAMS:			paramCoronaData		The Corona Data struct to be cleared
PROC Clear_All_MissionsAtCoords_CoronaData(g_structMatCCoronaMP &paramCoronaData)
	Clear_One_MP_Mission_At_Coords_Corona_Variables(paramCoronaData)
ENDPROC




// ===========================================================================================================
//      Mission At Coords Options Public Access Routines
// ===========================================================================================================

// PURPOSE:	To clear out all flags in the Options struct
//
// RETURN PARAMS:			paramOptions		The Options struct to be cleared
PROC Clear_All_MissionsAtCoords_Options(g_structMatCOptionsMP &paramOptions)

	g_structMatCOptionsMP emptyOptions
	paramOptions = emptyOptions
	
	paramOptions.matcoAngledAreaMin	= << 0.0, 0.0, 0.0 >>
	paramOptions.matcoAngledAreaMax	= << 0.0, 0.0, 0.0 >>

ENDPROC




// ===========================================================================================================
//      Mission At Coords Locked Missions Public Access Routines
// ===========================================================================================================

PROC Temporarily_Unlock_Missions__Internal(INT tempLoop, BOOL paramUnlockedUntilPlayed = FALSE)

	// Found a mission with coords within tolerance range, so temporarily unlocking it if it is still locked
	IF (Is_MissionsAtCoords_Mission_Locked(tempLoop))
		// ...should the mission remain unlocked until played?
		IF (paramUnlockedUntilPlayed)
			Set_MissionsAtCoords_Mission_As_Temporarily_Unlocked_Until_Played(tempLoop)
		ELSE
			Set_MissionsAtCoords_Mission_As_Temporarily_Unlocked(tempLoop)
		ENDIF
		
		Maintain_MissionsAtCoords_Lock_Status_For_Mission(tempLoop)
	
		#IF IS_DEBUG_BUILD
			NET_PRINT("      Coords: ")
			NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
			NET_PRINT(" ContentID: ")
			NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
			IF (paramUnlockedUntilPlayed)
				NET_PRINT(" - REMAIN UNLOCKED UNTIL PLAYED")
			ENDIF
			NET_NL()
			NET_PRINT("          ")
			Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
			NET_NL()
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("      Ignoring Temporary Unlock Request because mission already unlocked. Coords: ")
			NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
			NET_PRINT(" ContentID: ")
			NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
			NET_NL()
			NET_PRINT("          ")
			Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
			NET_NL()
		#ENDIF
	ENDIF
	
	// Require the slot to do a forced update
	Set_MissionsAtCoords_Mission_As_Force_Update(tempLoop)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To mark a tutorial mission as temporarily unlocked based on coords, even if it should otherwise be locked
//
// INPUT PARAMS:			paramCoords					The Coords for the mission
//							paramUnlockedUntilPlayed	[DEFAULT = FALSE] TRUE keeps the temporary unlock active until the mission is played, FALSE cleans up the unlock if the player moves away from the corona
//
// NOTES:	KGM 26/1/13: TEMP searching all missions for these coords and marking that mission as unlocked - this could lead to errors so need to use tighter data (perhaps regID?)
//			This was added so that a player invited to a locked mission could still trigger it
//			KGM 14/2/13: The New 'Unlocked Until Played' flag is added for the Race tutorial to keep the mission unlocked until played
//			KGM 29/10/13: THIS HAS BECOME 'TUTORIAL' SPECIFIC AND ONLY SEARCHES R* CREATED CONTENT
PROC Temporarily_Unlock_MissionsAtCoords_Mission_For_Tutorial(VECTOR paramCoords, BOOL paramUnlockedUntilPlayed = FALSE, BOOL bSkipType = FALSE)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT	tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Temporarily unlocking Tutorial R* Created content. Coords: ")
		NET_PRINT_VECTOR(paramCoords)
		NET_NL()
	#ENDIF
		
	// R* Created content only
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
		OR bSkipType
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
				Temporarily_Unlock_Missions__Internal(tempLoop, paramUnlockedUntilPlayed)			
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Temporarily_Unlock_MissionsAtCoords_Mission_For_Tutorial() - FAILED. No Rockstar Created content found within tolerance range of coords.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m, TRUE)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To mark a mission as temporarily unlocked, even if it should otherwise be locked
//
// INPUT PARAMS:			paramFilename				The CloudFilename for the mission
//							paramUnlockedUntilPlayed	[DEFAULT = FALSE] TRUE keeps the temporary unlock active until the mission is played, FALSE cleans up the unlock if the player moves away from the corona
//
// NOTES:	KGM 26/1/13: TEMP searching all missions for these coords and marking that mission as unlocked - this could lead to errors so need to use tighter data (perhaps regID?)
//			This was added so that a player invited to a locked mission could still trigger it
//			KGM 14/2/13: The New 'Unlocked Until Played' flag is added for the Race tutorial to keep the mission unlocked until played
PROC Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename(TEXT_LABEL_23 paramFilename, BOOL paramUnlockedUntilPlayed = FALSE)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT	tempLoop = 0	
	INT filenameHash = GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Temporarily unlocking mission. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		IF g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			Temporarily_Unlock_Missions__Internal(tempLoop, paramUnlockedUntilPlayed)	
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename() - FAILED. No mission found with ContentID.") NET_NL()
	#ENDIF
	
ENDPROC



// ===========================================================================================================
//      Mission At Coords Public Access to Help Text Helper Globals
// ===========================================================================================================

// PURPOSE:	Check if it is currently okay to display Rockstar Verified Help text for Races
//
// RETURN VALUE:		BOOL			TRUE if any R* Verified Race is on display, otherwise FALSE
FUNC BOOL Is_Any_Rockstar_Verified_Race_Blip_On_Display()
	RETURN (g_isAnyRSVerifiedRaceBlipOnDisplay)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if it is currently okay to display Rockstar Verified Help text for Deathmatches
//
// RETURN VALUE:		BOOL			TRUE if any R* Verified Deathmatch is on display, otherwise FALSE
FUNC BOOL Is_Any_Rockstar_Verified_Deathmatch_Blip_On_Display()
	RETURN (g_isAnyRSVerifiedDMBlipOnDisplay)
ENDFUNC




// ===========================================================================================================
//      Mission At Coords TEMPORARY ROUTINES TO ALLOW PUBLIC ACCESS IN A NOT VERY SECURE WAY
// ===========================================================================================================

PROC Get_MissionsAtCoords_Variation_At_Coords__Internal(INT tempLoop)

	IF tempLoop = tempLoop
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
		NET_PRINT("  [Variation: ")
		NET_PRINT_INT(Get_MissionsAtCoords_Slot_Mission_Variation(tempLoop))
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To get the mission variation for Tutorial Rockstar Created Content based on the mission coords
//
// INPUT PARAMS:			paramCoords			The Coords for the mission
// RETURN VALUE:			INT					The Mission Variation
//
// NOTES:	KGM 27/1/13: TEMP searching all missions to get the mission variation - there should be a better way to pass this information through to the invite system rather than invites requesting this from the MissionsAtCoords
//			This was added so that the joblist could reserve a space on the mission prior to accepting an invite
//			KGM 29/10/13: THIS HAS BECOME 'TUTORIAL' SPECIFIC AND ONLY SEARCHES R* CREATED CONTENT
FUNC INT Get_MissionsAtCoords_Variation_For_Tutorial(VECTOR paramCoords)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT	tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Returning Mission Variation for Tutorial R* Created content. Coords: ")
		NET_PRINT_VECTOR(paramCoords)
		NET_NL()
	#ENDIF
	
	// R* Created content only
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
				// Found a mission with coords within tolerance range, so returning the mission variation
				Get_MissionsAtCoords_Variation_At_Coords__Internal(tempLoop)
				
				RETURN (Get_MissionsAtCoords_Slot_Mission_Variation(tempLoop))
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Get_MissionsAtCoords_Variation_For_Tutorial() - FAILED. No Rockstar Created content found within tolerance range of coords. Returning NO_MISSION_VARIATION.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m, TRUE)
	#ENDIF
	
	RETURN NO_MISSION_VARIATION
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To get the mission variation for a mission based on the ContentID
//
// INPUT PARAMS:			paramFilename			The CloudFilename for the mission
// RETURN VALUE:			INT					The Mission Variation
//
// NOTES:	KGM 27/1/13: TEMP searching all missions to get the mission variation - there should be a better way to pass this information through to the invite system rather than invites requesting this from the MissionsAtCoords
//			This was added so that the joblist could reserve a space on the mission prior to accepting an invite
FUNC INT Get_MissionsAtCoords_Variation_By_Filename(TEXT_LABEL_23 paramFilename)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Get_MissionsAtCoords_Variation_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Get_MissionsAtCoords_Variation_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		RETURN NO_MISSION_VARIATION
	ENDIF
	
	INT	tempLoop = 0
	int filenameHash = GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Returning Mission Variation. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		if g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			// Found a mission with coords within tolerance range, so returning the mission variation
			Get_MissionsAtCoords_Variation_At_Coords__Internal(tempLoop)
			
			RETURN (Get_MissionsAtCoords_Slot_Mission_Variation(tempLoop))
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Get_MissionsAtCoords_Variation_By_Filename() - FAILED. No mission found with the ContentID. Returning NO_MISSION_VARIATION.") NET_NL()
	#ENDIF
	
	RETURN NO_MISSION_VARIATION
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

PROC Get_MissionsAtCoords_HasCutscene_At_Coords__Internal(INT tempLoop)

	IF tempLoop = tempLoop
	ENDIF
	// Found a mission with coords within tolerance range, so returning the 'has cutscene' requirements
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
		NET_PRINT("  [Has Cutscene? ")
		IF (Does_MissionsAtCoords_Mission_Have_Cutscene(tempLoop))
			NET_PRINT("YES")
		ELSE
			NET_PRINT("NO")
		ENDIF
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To get the 'has cutscene' flag for Tutorial Rockstar Created Content based on the mission coords
//
// INPUT PARAMS:			paramCoords			The Coords for the mission
// RETURN VALUE:			BOOL				TRUE if the mission has a cutscene, otherwise FALSE
//
// NOTES:	KGM 27/1/13: TEMP searching all missions to get the 'has cutscene' status - there should be a better way to pass this information through to the invite system rather than invites requesting this from the MissionsAtCoords
//			This was added so that the joblist could reserve a space on the mission prior to accepting an invite
//			KGM 29/10/13: THIS HAS BECOME 'TUTORIAL' SPECIFIC AND ONLY SEARCHES R* CREATED CONTENT
FUNC BOOL Get_MissionsAtCoords_HasCutscene_For_Tutorial(VECTOR paramCoords)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT	tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Returning 'Has Cutscene' requirements for Tutorial R* Created content. Coords: ")
		NET_PRINT_VECTOR(paramCoords)
		NET_NL()
	#ENDIF
	
	// R* Created content only
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
				Get_MissionsAtCoords_HasCutscene_At_Coords__Internal(tempLoop)
				
				RETURN (Does_MissionsAtCoords_Mission_Have_Cutscene(tempLoop))
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Get_MissionsAtCoords_HasCutscene_For_Tutorial() - FAILED. No Rockstar Created content found within tolerance range of coords. Returning FALSE.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m, TRUE)
	#ENDIF
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To get the 'has cutscene' flag for a mission based on the ContentID
//
// INPUT PARAMS:			paramFilename			The CloudFilename for the mission
// RETURN VALUE:			BOOL				TRUE if the mission has a cutscene, otherwise FALSE
//
// NOTES:	KGM 27/1/13: TEMP searching all missions to get the 'has cutscene' status - there should be a better way to pass this information through to the invite system rather than invites requesting this from the MissionsAtCoords
//			This was added so that the joblist could reserve a space on the mission prior to accepting an invite
FUNC BOOL Get_MissionsAtCoords_HasCutscene_By_Filename(TEXT_LABEL_23 paramFilename)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Get_MissionsAtCoords_HasCutscene_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Get_MissionsAtCoords_HasCutscene_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	INT	tempLoop = 0	
	INT filenameHash = GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Returning 'Has Cutscene' requirements. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		if g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			Get_MissionsAtCoords_HasCutscene_At_Coords__Internal(tempLoop)
			
			RETURN (Does_MissionsAtCoords_Mission_Have_Cutscene(tempLoop))
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Get_MissionsAtCoords_HasCutscene_By_Filename() - FAILED. No mission found with ContentID. Returning FALSE.") NET_NL()
	#ENDIF
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To get the cloud filename for Tutorial Rockstar Created content based on the mission coords
//
// INPUT PARAMS:			paramCoords			The Coords for the mission
// RETURN VALUE:			TEXT_LABEL_23		The Cloud Filename, or ""
//
// NOTES:	KGM 27/1/13: TEMP searching all missions to get the cloud filename - there should be a better way to pass this information through to the invite system rather than invites requesting this from the MissionsAtCoords
//			This was added so that the joblist could reserve a space on the mission prior to accepting an invite
//			KGM 29/10/13: THIS HAS BECOME 'TUTORIAL' SPECIFIC AND ONLY SEARCHES R* CREATED CONTENT
FUNC TEXT_LABEL_23 Get_MissionsAtCoords_CloudFilename_For_Tutorial(VECTOR paramCoords)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT	tempLoop = 0
	
	// R* Created content only
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
				// Found a mission with coords within tolerance range, so returning the cloud filename
				MP_MISSION_ID_DATA	theMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(tempLoop)
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [At Coords]: Returning Tutorial Rockstar Created Cloud Filename for coords: ")
					NET_PRINT_VECTOR(paramCoords)
					NET_PRINT("  [Cloud Filename: ") NET_PRINT(theMissionIdData.idCloudFilename) NET_PRINT("]") NET_NL()
					NET_PRINT("          ")
					Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
					NET_NL()
				#ENDIF
				
				RETURN (theMissionIdData.idCloudFilename)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Get_MissionsAtCoords_CloudFilename_For_Tutorial() - FAILED. No Rockstar Created content found within tolerance range of coords. Returning empty string.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m, TRUE)
	#ENDIF
	
	TEXT_LABEL_23 emptyReturn = ""
	RETURN (emptyReturn)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To get the cloud filename for a mission based on the mission coords
//
// INPUT PARAMS:			paramCoords			The Coords for the mission
// RETURN VALUE:			TEXT_LABEL_23		The Cloud Filename, or ""
//
// NOTES:	KGM 27/1/13: TEMP searching all missions to get the cloud filename - there should be a better way to pass this information through to the invite system rather than invites requesting this from the MissionsAtCoords
//			This was added so that the joblist could reserve a space on the mission prior to accepting an invite
//			KGM 29/10/13: HOPEFULLY CAN BECOME OBSOLETE WHEN JOBLIST APP RE-WORKED TO AVOID USING THIS (This is a copy of the function above prior to it being modified)
FUNC TEXT_LABEL_23 TEMP_Get_MissionsAtCoords_CloudFilename_At_Coords(VECTOR paramCoords)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT	tempLoop = 0
	
	REPEAT g_numAtCoordsMP tempLoop
		IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
			// Found a mission with coords within tolerance range, so returning the cloud filename
			MP_MISSION_ID_DATA	theMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(tempLoop)
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [At Coords]: Returning Cloud Filename for coords: ")
				NET_PRINT_VECTOR(paramCoords)
				NET_PRINT("  [Cloud Filename: ") NET_PRINT(theMissionIdData.idCloudFilename) NET_PRINT("]") NET_NL()
				NET_PRINT("          ")
				Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
				NET_NL()
			#ENDIF
			
			RETURN (theMissionIdData.idCloudFilename)
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: TEMP_Get_MissionsAtCoords_CloudFilename_At_Coords() - FAILED. No mission found within tolerance range of coords. Returning empty string.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m)
	#ENDIF
	
	TEXT_LABEL_23 emptyReturn = ""
	RETURN (emptyReturn)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

PROC Change_Missions_InCorona_External_Function_ID__Internal(INT tempLoop, g_eMatCInCoronaExternalID paramExternalID)

	// Found a mission with coords within tolerance range, so updating the externalID
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
		NET_PRINT("  [ExternalID changing to: ")
		NET_PRINT(Convert_Missions_At_Coords_InCorona_ExternalID_To_String(paramExternalID))
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
		NET_NL()
	#ENDIF

	// Update the ExternalID
	g_sAtCoordsMP[tempLoop].matcExternalID = paramExternalID
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To modify the external function that will process this Tutorial Rockstar Created content, identified by coords
//
// INPUT PARAMS:			paramCoords			The Coords for the mission
//							paramExternalID		The InCorona ExternalID that will handle the 'InCorona' processing for this mission
//
// NOTES:	KGM 27/1/13: TEMP searching all missions - there should be a better way to pass this information through to the invite system rather than invites requesting this from the MissionsAtCoords
//			This was added (perhaps temporarily) to allow Dave to change which function processes the Tutorial Mission when players are in the corona
//			KGM 29/10/13: THIS HAS BECOME 'TUTORIAL' SPECIFIC AND ONLY SEARCHES R* CREATED CONTENT
PROC Change_MissionsAtCoords_InCorona_External_Function_ID_For_Tutorial(VECTOR paramCoords, g_eMatCInCoronaExternalID paramExternalID)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT	tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Changing InCorona ExternalID for Tutorial R* Created content. Coords: ")
		NET_PRINT_VECTOR(paramCoords)
		NET_NL()
	#ENDIF
	
	// R* Created content only
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
				Change_Missions_InCorona_External_Function_ID__Internal(tempLoop, paramExternalID)			
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Change_MissionsAtCoords_InCorona_External_Function_ID_For_Tutorial() - FAILED. No Rockstar Created content found within tolerance range of coords.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m, TRUE)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To modify the external function that will process this mission, identified by ContentID
//
// INPUT PARAMS:			paramFilename		The CloudFilename for the mission
//							paramExternalID		The InCorona ExternalID that will handle the 'InCorona' processing for this mission
//
// NOTES:	KGM 27/1/13: TEMP searching all missions - there should be a better way to pass this information through to the invite system rather than invites requesting this from the MissionsAtCoords
//			This was added (perhaps temporarily) to allow Dave to change which function processes the Tutorial Mission when players are in the corona
PROC Change_MissionsAtCoords_InCorona_External_Function_ID_By_ContentID(TEXT_LABEL_23 paramFilename, g_eMatCInCoronaExternalID paramExternalID)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Change_MissionsAtCoords_InCorona_External_Function_ID_By_ContentID(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Change_MissionsAtCoords_InCorona_External_Function_ID_By_ContentID() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT	tempLoop = 0
	INT filenameHash = GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Changing InCorona ExternalID. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		if g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			Change_Missions_InCorona_External_Function_ID__Internal(tempLoop, paramExternalID)
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Change_MissionsAtCoords_InCorona_External_Function_ID_By_ContentID() - FAILED. No mission found with ContentID.") NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Change_MissionsAtCoords_Use_Extended_Name_Display_Range__Internal(INT tempLoop, BOOL paramUseExtended)

	// Found a mission with coords within tolerance range, so setting the 'use extended mission name display range' flag
	IF (paramUseExtended)
		SET_BIT(g_sAtCoordsMP[tempLoop].matcOptionBitflags, MATC_BITFLAG_USE_EXTENDED_NAME_RANGE)
	ELSE
		CLEAR_BIT(g_sAtCoordsMP[tempLoop].matcOptionBitflags, MATC_BITFLAG_USE_EXTENDED_NAME_RANGE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
		NET_PRINT("  [Use Extended Range? Setting to ")
		IF (Should_MissionsAtCoords_Mission_Use_Extended_Name_Display_Range(tempLoop))
			NET_PRINT("YES")
		ELSE
			NET_PRINT("NO")
		ENDIF
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To change the 'use extended mission name range' flag for a mission based on the mission coords
//
// INPUT PARAMS:			paramCoords			The Coords for the mission
//							paramUseExtended	TRUE if the extended range should be used, FALSE otherwise
// NOTES:	KGM 29/10/13: THIS HAS BECOME 'TUTORIAL' SPECIFIC AND ONLY SEARCHES R* CREATED CONTENT
PROC Change_MissionsAtCoords_Use_Extended_Name_Display_Range_For_Tutorial(VECTOR paramCoords, BOOL paramUseExtended)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT	tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Setting 'Use Extended Mission name Range' flag for Tutorial R* Created Content. Coords: ")
		NET_PRINT_VECTOR(paramCoords)
		NET_NL()
	#ENDIF
	
	// R* Created content only
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
				Change_MissionsAtCoords_Use_Extended_Name_Display_Range__Internal(tempLoop, paramUseExtended)
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Change_MissionsAtCoords_Use_Extended_Name_Display_Range_For_Tutorial() - FAILED. No Rockstar Created content found within tolerance range of coords. Leaving flag untouched.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m, TRUE)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To change the 'use extended mission name range' flag for a mission based on the contentID
//
// INPUT PARAMS:			paramFilename			The CloudFilename for the mission
//							paramUseExtended	TRUE if the extended range should be used, FALSE otherwise
PROC Change_MissionsAtCoords_Use_Extended_Name_Display_Range_By_Filename(TEXT_LABEL_23 paramFilename, BOOL paramUseExtended)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Change_MissionsAtCoords_Use_Extended_Name_Display_Range_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Change_MissionsAtCoords_Use_Extended_Name_Display_Range_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT	tempLoop = 0	
	INT filenameHash = GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Setting 'Use Extended Mission name Range' flag. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		if g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			Change_MissionsAtCoords_Use_Extended_Name_Display_Range__Internal(tempLoop, paramUseExtended)
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Change_MissionsAtCoords_Use_Extended_Name_Display_Range_By_Filename() - FAILED. No mission found with ContentID. Leaving flag untouched.") NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Change_Missions_Allow_During_Ambient_Tutorial__Internal(INT tempLoop, BOOL paramAllowOnAmbTut)

	// Found a mission with coords within tolerance range
	// Because of the global counter associated with this, it's important to only set or clear the flag if this is different from the previous state of the flag
	// This is to ensure the global counter is accurate (see info at top of net_missions_at_coords.sch for reasons)
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
		NET_PRINT("  [Attempting to allow? ")
		IF (paramAllowOnAmbTut)
			NET_PRINT("YES")
		ELSE
			NET_PRINT("NO")
		ENDIF
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
		NET_NL()
	#ENDIF
	
	IF (paramAllowOnAmbTut)
		Set_MissionsAtCoords_Mission_As_Allowed_During_Ambient_Tutorial(tempLoop)
	ELSE
		Clear_MissionsAtCoords_Mission_As_Allowed_During_Ambient_Tutorial(tempLoop)
	ENDIF
	
	// Require the slot to do a forced update
	Set_MissionsAtCoords_Mission_As_Force_Update(tempLoop)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To change the 'allow during ambient tutorial' flag for a mission based on the mission coords
//
// INPUT PARAMS:			paramCoords				The Coords for the mission
//							paramAllowOnAmbTut		TRUE if the mission and visuals are active during an ambient tutorial, FALSE otherwise
// NOTES:	KGM 29/10/13: THIS HAS BECOME 'TUTORIAL' SPECIFIC AND ONLY SEARCHES R* CREATED CONTENT
PROC Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_For_Tutorial(VECTOR paramCoords, BOOL paramAllowOnAmbTut)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Dealing with 'Allow During Ambient Tutorial' flag for Tutorial R* Created content. Coords: ")
		NET_PRINT_VECTOR(paramCoords)
		NET_NL()
	#ENDIF
	
	// R* Created content only
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
				Change_Missions_Allow_During_Ambient_Tutorial__Internal(tempLoop, paramAllowOnAmbTut)
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_For_Tutorial() - FAILED. No Rockstar Created content found within tolerance range of coords. Leaving flag untouched.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m, TRUE)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To change the 'allow during ambient tutorial' flag for a mission based on the contentID
//
// INPUT PARAMS:			paramFilename			The CloudFilename for the mission
//							paramAllowOnAmbTut		TRUE if the mission and visuals are active during an ambient tutorial, FALSE otherwise
PROC Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_By_Filename(TEXT_LABEL_23 paramFilename, BOOL paramAllowOnAmbTut)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT tempLoop = 0	
	INT filenameHash = GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Dealing with 'Allow During Ambient Tutorial' flag. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		if g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			Change_Missions_Allow_During_Ambient_Tutorial__Internal(tempLoop, paramAllowOnAmbTut)
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Change_MissionsAtCoords_Allow_During_Ambient_Tutorial_By_Filename() - FAILED. No mission found with the same name. Leaving flag untouched.") NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Change_Missions_Mission_Do_Not_Trigger_Status_To_Allow_Trigger__Internal(INT tempLoop, BOOL paramAllowTrigger)

	// Found a mission with coords within tolerance range, so setting the 'do not trigger' flag
	IF (paramAllowTrigger)
		CLEAR_BIT(g_sAtCoordsMP[tempLoop].matcOptionBitflags, MATC_BITFLAG_DO_NOT_TRIGGER)
	ELSE
		SET_BIT(g_sAtCoordsMP[tempLoop].matcOptionBitflags, MATC_BITFLAG_DO_NOT_TRIGGER)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
		NET_PRINT("  [Do Not Trigger? Setting to ")
		IF (Is_MissionsAtCoords_Mission_Marked_As_Do_Not_Trigger(tempLoop))
			NET_PRINT("YES - Do Not Trigger")
		ELSE
			NET_PRINT("NO - Allowed To Trigger")
		ENDIF
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To change the 'not triggerable' flag for a mission based on the mission coords
//
// INPUT PARAMS:			paramCoords				The Coords for the mission
//							paramAllowOnAmbTut		TRUE if the mission should trigger as normal, FALSE if the visuals are active but the mission shouldn't trigger
// NOTES:	KGM 29/10/13: THIS HAS BECOME 'TUTORIAL' SPECIFIC AND ONLY SEARCHES R* CREATED CONTENT
PROC Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_For_Tutorial(VECTOR paramCoords, BOOL paramAllowTrigger)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Changing the 'Do Not Trigger' flag for Tutorial R* Created content. Coords: ")
		NET_PRINT_VECTOR(paramCoords)
		NET_NL()
	#ENDIF
	
	// R* Created content only
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
				Change_Missions_Mission_Do_Not_Trigger_Status_To_Allow_Trigger__Internal(tempLoop, paramAllowTrigger)			
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_For_Tutorial() - FAILED. No Rockstar Created content found within tolerance range of coords. Leaving flag untouched.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m, TRUE)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To change the 'not triggerable' flag for a mission based on the contentID
//
// INPUT PARAMS:			paramfilename			The CloudFilename for the mission
//							paramAllowOnAmbTut		TRUE if the mission should trigger as normal, FALSE if the visuals are active but the mission shouldn't trigger
PROC Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename(TEXT_LABEL_23 paramFilename, BOOL paramAllowTrigger  , BOOL paramIsHeist = FALSE  )
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT tempLoop = 0
	INT filenameHash = GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Changing the 'Do Not Trigger' flag. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		if g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			Change_Missions_Mission_Do_Not_Trigger_Status_To_Allow_Trigger__Internal(tempLoop, paramAllowTrigger)
			
				// AMEC HEISTS.
				IF paramIsHeist
					PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename - paramIsHeist = TRUE, blocking corona.")
					SET_HEIST_CORONA_BLOCKED_STATE(HEIST_INTRO_STATE_BLOCKED)
				ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
			// AMEC HEISTS.
			IF paramIsHeist
				PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename - FAILED. No mission found with contentID. Leaving flag untouched.")
				
				EXIT
			ENDIF
		
		NET_PRINT("...KGM MP [At Coords]: Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename() - FAILED. No mission found with contentID. Leaving flag untouched.") NET_NL()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store a contentID that should be the only triggerable mission
//
// INPUT PARAMS:			paramContentID			The contetnID to become exclusive
//
// NOTES:	Added so that an invite to a heist can reserve the contentID as exclusive even though the corona may not yet have been setup
PROC Register_A_MissionsAtCoords_Exclusive_ContentID(TEXT_LABEL_23 paramContentID)

	IF (IS_STRING_NULL_OR_EMPTY(paramContentId))
		PRINTLN(".KGM [At Coords]: Attempting to setup Exclusive ContentID, but the contentID is empty")
		SCRIPT_ASSERT("Register_A_MissionsAtCoords_Exclusive_ContentID() - ERROR: contentID is empty. Tell Keith.")
		
		EXIT
	ENDIF
	
	INT existingExclusiveContentID = g_sMatcExclusiveContentID.matcecContentIdHash
	
	INT contentIdHash = GET_HASH_KEY(paramContentId)
	
	// Store the details
	g_sMatcExclusiveContentID.matcecContentIdHash	= contentIdHash
	g_sMatcExclusiveContentID.matcexTimeout			= GET_GAME_TIMER() + MATC_EXCLUSIVE_CONTENTID_TIME_msec
	
	PRINTLN(".KGM [At Coords]: Setting exclusive ContentID: ", paramContentId, " [Hash: ", g_sMatcExclusiveContentID.matcecContentIdHash, "] - Timeout: ", MATC_EXCLUSIVE_CONTENTID_TIME_msec, " msec")
	IF (existingExclusiveContentID != 0)
		PRINTLN(".KGM [At Coords]: ...replacing existing exclusive contentID Hash: ", existingExclusiveContentID)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Set_Next_Missions_Playlist_Activity__Internal(INT tempLoop)

	// Found a mission with coords within tolerance range, so setting the 'use extended mission name display range' flag
	Set_MissionsAtCoords_Currently_Active_Playlist_Activity(tempLoop)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
		NET_NL()
	#ENDIF
	
	// Require the slot to do a forced update
	Set_MissionsAtCoords_Mission_As_Force_Update(tempLoop)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To set this mission as the next playlist activity based on the contentID
//
// INPUT PARAMS:			paramFilename			The CloudFilename for the mission
PROC Set_Next_MissionsAtCoords_Playlist_Activity_By_Filename(TEXT_LABEL_23 paramFilename)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Set_Next_MissionsAtCoords_Playlist_Activity_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Set_Next_MissionsAtCoords_Playlist_Activity_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT	tempLoop = 0
	INT filenameHash = GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Setting 'Currently Active Playlist Activity'. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		if g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			Set_Next_Missions_Playlist_Activity__Internal(tempLoop)
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Set_Next_MissionsAtCoords_Playlist_Activity_By_Filename() - FAILED. No mission found with the ContentID. Leaving flag untouched.") NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC TEMP_Get_Missions_Is_Immediate_Launch_For_Coords__Internal(INT tempLoop)

	IF tempLoop = tempLoop
	ENDIF
	// Found a mission with coords within tolerance range, so returning the 'immediate launch' flag
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
		NET_PRINT("  [Immediate Launch? ")
		IF (Should_MissionsAtCoords_Mission_Launch_Immediately(tempLoop))
			NET_PRINT("YES")
		ELSE
			NET_PRINT("NO")
		ENDIF
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To get the 'Immediate Launch' flag for a mission based on the contentID
//
// INPUT PARAMS:			paramFilename		The CloudFilename for the mission
// RETURN VALUE:			BOOL				TRUE if the mission should launch immediately, otherwise FALSE
FUNC BOOL Get_MissionsAtCoords_Is_Immediate_Launch_By_Filename(TEXT_LABEL_23 paramFilename)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Get_MissionsAtCoords_Is_Immediate_Launch_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Get_MissionsAtCoords_Is_Immediate_Launch_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		RETURN FALSE
	ENDIF
	
	INT	tempLoop = 0
	INT filenameHash = GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Returning 'Immediate Launch' flag. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		if g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			TEMP_Get_Missions_Is_Immediate_Launch_For_Coords__Internal(tempLoop)
			RETURN (Should_MissionsAtCoords_Mission_Launch_Immediately(tempLoop))
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Get_MissionsAtCoords_Is_Immediate_Launch_By_Filename() - FAILED. No mission found with the ContentID. Returning FALSE.") NET_NL()
	#ENDIF
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

PROC Set_Missions_Mission_Has_Had_Invite_Accepted__Internal(INT tempLoop)

	// Found a mission with same name, so setting the 'invite accepted' flag
	Set_MissionsAtCoords_Mission_Invite_Has_Been_Accepted(tempLoop)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
		NET_NL()
	#ENDIF
	
	// Require the slot to do a forced update
	Set_MissionsAtCoords_Mission_As_Force_Update(tempLoop)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To treat this tutorial R* mission as having had an invite accepted
//
// INPUT PARAMS:			paramCoords			The Coords for the mission
//
// NOTES:	KGM 29/10/13: THIS HAS BECOME 'TUTORIAL' SPECIFIC AND ONLY SEARCHES R* CREATED CONTENT
PROC Set_MissionsAtCoords_Mission_Has_Had_Invite_Accepted_For_Tutorial(VECTOR paramCoords, BOOL bUnBlock = FALSE, BOOL bSkipType = FALSE)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT	tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Setting 'Mission Invite Accepted' for Tutorial R* Created content. Coords: ")
		NET_PRINT_VECTOR(paramCoords)
		NET_NL()
	#ENDIF
	
	// R* Created content only
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
		OR bSkipType
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
				Set_Missions_Mission_Has_Had_Invite_Accepted__Internal(tempLoop)
				IF bUnBlock
					Unblock_One_MissionsAtCoords_Mission(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
				ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Set_MissionsAtCoords_Mission_Has_Had_Invite_Accepted_For_Tutorial() - FAILED. No Rockstar Created content found within tolerance range of coords. Leaving flag untouched.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m, TRUE)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To treat this mission as having had an invite accepted
//
// INPUT PARAMS:			paramFilename			The CloudFilename for the mission
PROC Set_MissionsAtCoords_Mission_Has_Had_Invite_Accepted_By_Filename(TEXT_LABEL_23 paramFilename, g_eMatCStages eMissionStage = MATCS_STAGE_NEW)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Set_MissionsAtCoords_Mission_Has_Had_Invite_Accepted_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Set_MissionsAtCoords_Mission_Has_Had_Invite_Accepted_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT	tempLoop = 0
	INT filenameHash = GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Setting 'Mission Invite Accepted'. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		if g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			Set_Missions_Mission_Has_Had_Invite_Accepted__Internal(tempLoop)
			IF eMissionStage != MATCS_STAGE_NEW
				Set_MissionsAtCoords_Stage_For_Mission(tempLoop, eMissionStage)
			ENDIF
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Set_MissionsAtCoords_Mission_Has_Had_Invite_Accepted_By_Filename() - FAILED. No mission found with the contentID. Leaving flag untouched.") NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

PROC Set_MissionsAtCoords_Mission_Blip_To_Flash__Internal(INT tempLoop, INT paramDuration)

	BLIP_INDEX	theBlip		= NULL
	// Found a mission with coords with the same name so flash
	theBlip = Get_MissionsAtCoords_Slot_BlipIndex(tempLoop)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
		NET_PRINT(" [Duration: ")
		NET_PRINT_INT(paramDuration)
		NET_PRINT("msec]")
		IF (GET_BLIP_INFO_ID_DISPLAY(theBlip) = DISPLAY_NOTHING)
			NET_PRINT(" - BUT BLIP NOT ON DISPLAY")
		ENDIF
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(tempLoop)
		NET_NL()
	#ENDIF
	
	// Require the slot to do a forced update
	IF (GET_BLIP_INFO_ID_DISPLAY(theBlip) != DISPLAY_NOTHING)
		SET_BLIP_FLASHES(theBlip, TRUE)
		SET_BLIP_FLASH_TIMER(theBlip, paramDuration)
	ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To set a mission blip to flash, identified by Coords
//
// INPUT PARAMS:			paramCoords				The Coords for the mission
//							paramDuration			[DEFAULT = MATC_BLIP_FLASH_TIME_msec] The duration of the flash in msec
//
// NOTES:	KGM 29/10/13: THIS HAS BECOME 'TUTORIAL' SPECIFIC AND ONLY SEARCHES R* CREATED CONTENT
PROC Set_MissionsAtCoords_Mission_Blip_At_Coords_To_Flash_For_Tutorial(VECTOR paramCoords, INT paramDuration = MATC_BLIP_FLASH_TIME_msec)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	INT			tempLoop	= 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Setting Blip Flash for Tutorial R* Created content. Coords: ")
		NET_PRINT_VECTOR(paramCoords)
		NET_NL()
	#ENDIF
	
	// R* Created content only
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
				Set_MissionsAtCoords_Mission_Blip_To_Flash__Internal(tempLoop, paramDuration)
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Set_MissionsAtCoords_Mission_Blip_At_Coords_To_Flash_For_Tutorial() - FAILED. No Rockstar Created content found within tolerance range of coords. Leaving flag untouched.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m, TRUE)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To set a mission blip to flash, identified by Filename
//
// INPUT PARAMS:			paramFilename			The CloudFilename for the mission
//							paramDuration			[DEFAULT = MATC_BLIP_FLASH_TIME_msec] The duration of the flash in msec
PROC Set_MissionsAtCoords_Mission_Blip_To_Flash_By_Filename(TEXT_LABEL_23 paramFilename, INT paramDuration = MATC_BLIP_FLASH_TIME_msec)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Set_MissionsAtCoords_Mission_Blip_To_Flash_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Set_MissionsAtCoords_Mission_Blip_To_Flash_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT	tempLoop	 = 0
	INT filenameHash = GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Setting Blip Flash for mission. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		if g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			Set_MissionsAtCoords_Mission_Blip_To_Flash__Internal(tempLoop, paramDuration)
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Set_MissionsAtCoords_Mission_Blip_To_Flash_By_Filename() - FAILED. No mission found with the contentID. Leaving flag untouched.") NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Internal common functionality for setting a blip to long-range or back to short-range
//
// INPUT PARAMS:		paramSlot			The MissionsAtCoords slot for the mission
//						paramLongRange		TRUE to display the blip long-range temporarily, FALSE otherwise
PROC Set_MissionsAtCoords_Mission_Blip_To_LongRange__Internal(INT paramSlot, BOOL paramLongRange)

	BLIP_INDEX	theBlip		= NULL
	
	// Found the mission, so set the blip to long-range
	theBlip = Get_MissionsAtCoords_Slot_BlipIndex(paramSlot)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[paramSlot].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[paramSlot].matcMissionIdData.idCloudFilename)
		NET_PRINT(" [Long-Range? ")
		IF (paramLongRange)
			NET_PRINT("YES")
		ELSE
			NET_PRINT("NO")
		ENDIF
		NET_PRINT("]")
		IF (GET_BLIP_INFO_ID_DISPLAY(theBlip) = DISPLAY_NOTHING)
			NET_PRINT(" - BLIP NOT CURRENTLY ON DISPLAY")
		ENDIF
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		NET_NL()
	#ENDIF
	
	// Update the storage flag to ensure the blip continues to obey this request while active
	IF (paramLongRange)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_SHOW_BLIP_LONG_RANGE)
	ELSE
		CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_SHOW_BLIP_LONG_RANGE)
	ENDIF
	
	// Update the blip if it is already on display
	BOOL isShortRange = TRUE
	IF (paramLongRange)
		isShortRange = FALSE
	ENDIF
	
	IF (GET_BLIP_INFO_ID_DISPLAY(theBlip) = DISPLAY_NOTHING)
		EXIT
	ENDIF
	
	// Blip is on display, so immediately change its range
	SET_BLIP_AS_SHORT_RANGE(theBlip, isShortRange)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To make a temporary change to the mission blip's range based on the mission's coords
//
// INPUT PARAMS:			paramCoords				The Coords for the mission
//							paramLongRange			TRUE for temporary long-range, FALSE to return to the default range setting
//
// NOTES:	KGM 29/10/13: THIS HAS BECOME 'TUTORIAL' SPECIFIC AND ONLY SEARCHES R* CREATED CONTENT
PROC Set_MissionsAtCoords_Mission_Blip_At_Coords_To_LongRange_For_Tutorial(VECTOR paramCoords, BOOL paramLongRange)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.5
	
	INT	tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Setting Blip to Long-Range for Tutorial R* Created content. Coords: ")
		NET_PRINT_VECTOR(paramCoords)
		NET_NL()
	#ENDIF
	
	// R* Created content only
	REPEAT g_numAtCoordsMP tempLoop
		IF (Get_MissionsAtCoords_Slot_CreatorID(tempLoop) = FMMC_ROCKSTAR_CREATOR_ID)
			IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramCoords, MISSION_COORDS_TOLERANCE_m))
				Set_MissionsAtCoords_Mission_Blip_To_LongRange__Internal(tempLoop, paramLongRange)
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Set_MissionsAtCoords_Mission_Blip_At_Coords_To_LongRange_For_Tutorial() - FAILED. No Rockstar Created content found within tolerance range of coords. Leaving flag untouched.") NET_NL()
		Debug_Output_Failed_To_Find_Mission_Near_Coords(paramCoords, MISSION_COORDS_TOLERANCE_m, TRUE)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To make a temporary change to the mission blip's range based on the mission's contentID
//
// INPUT PARAMS:			paramFilename			The contentID for the mission
//							paramLongRange			TRUE for temporary long-range, FALSE to return to the default range setting
PROC Set_MissionsAtCoords_Mission_Blip_To_LongRange_By_Filename(TEXT_LABEL_23 paramFilename, BOOL paramLongRange)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Set_MissionsAtCoords_Mission_Blip_To_LongRange_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Set_MissionsAtCoords_Mission_Blip_To_LongRange_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT	tempLoop		= 0	
	INT filenameHash	= GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Setting Blip to Long-Range for mission. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		IF (g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash)
			Set_MissionsAtCoords_Mission_Blip_To_LongRange__Internal(tempLoop, paramLongRange)
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Set_MissionsAtCoords_Mission_Blip_To_LongRange_By_Filename() - FAILED. No mission found with the contentID. Leaving flag untouched.") NET_NL()
	#ENDIF
	
ENDPROC


PROC Set_MissionsAtCoords_Mission_Blip_And_Corona_To_Display__Internal(INT paramSlot, BOOL paramDisplay)

	#IF IS_DEBUG_BUILD
		BLIP_INDEX	theBlip		= NULL
		
		// Found the mission, so set the blip to long-range
		theBlip = Get_MissionsAtCoords_Slot_BlipIndex(paramSlot)
		
		NET_PRINT("      Coords: ")
		NET_PRINT_VECTOR(g_sAtCoordsMP[paramSlot].matcCoords)
		NET_PRINT(" ContentID: ")
		NET_PRINT(g_sAtCoordsMP[paramSlot].matcMissionIdData.idCloudFilename)
		NET_PRINT(" [Long-Range? ")
		IF (paramDisplay)
			NET_PRINT("YES")
		ELSE
			NET_PRINT("NO")
		ENDIF
		NET_PRINT("]")
		IF (GET_BLIP_INFO_ID_DISPLAY(theBlip) = DISPLAY_NOTHING)
			NET_PRINT(" - BLIP NOT CURRENTLY ON DISPLAY")
		ENDIF
		NET_NL()
		NET_PRINT("          ")
		Debug_Output_Missions_At_Coords_Slot_In_One_Line(paramSlot)
		NET_NL()
	#ENDIF
	
	// Update the storage flag to ensure the blip continues to obey this request while active
	IF (paramDisplay)
		CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_NO_BLIP_TO_BE_DISPLAYED)
		CLEAR_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_NO_CORONA_TO_BE_DISPLAYED)
	ELSE
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_NO_BLIP_TO_BE_DISPLAYED)
		SET_BIT(g_sAtCoordsMP[paramSlot].matcOptionBitflags, MATC_BITFLAG_NO_CORONA_TO_BE_DISPLAYED)
	ENDIF
	Display_MissionsAtCoords_Blip_For_Mission(paramSlot)
	Setup_MissionsAtCoords_Corona_Ground_Projection(paramSlot)
ENDPROC
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To make a temporary change to the mission blip's range based on the mission's contentID
//
// INPUT PARAMS:			paramFilename			The contentID for the mission
//							paramLongRange			TRUE for temporary long-range, FALSE to return to the default range setting
PROC Set_MissionsAtCoords_Mission_Blip_And_Corona_To_Display_By_Filename(TEXT_LABEL_23 paramFilename, BOOL paramDisplay)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Set_MissionsAtCoords_Mission_Blip_And_Corona_To_Display_By_Filename(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Set_MissionsAtCoords_Mission_Blip_And_Corona_To_Display_By_Filename() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT	tempLoop		= 0	
	INT filenameHash	= GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Setting Blip and Corona to Display for mission. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		IF (g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash)
			Set_MissionsAtCoords_Mission_As_Unlocked(tempLoop)
			Set_MissionsAtCoords_Mission_Blip_And_Corona_To_Display__Internal(tempLoop, paramDisplay)
			EXIT
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Set_MissionsAtCoords_Mission_Blip_And_Corona_To_Display_By_Filename() - FAILED. No mission found with the contentID. Leaving flag untouched.") NET_NL()
	#ENDIF
	
ENDPROC
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To allow a mission to become active if it is Awaiting Activation
//
// INPUT PARAMS:			paramFilename			The Cloud Filename for the mission
PROC Set_MissionsAtCoords_Mission_With_Filename_Should_Activate(TEXT_LABEL_23 paramFilename)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Set_MissionsAtCoords_Mission_With_Filename_Should_Activate(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Set_MissionsAtCoords_Mission_With_Filename_Should_Activate() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT					tempLoop			= 0
	TEXT_LABEL_23		thisFilename		= ""
	MP_MISSION_ID_DATA	thisMissionIdData
	INT					filenameHash		= GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Activate Mission. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		IF (Is_MissionsAtCoords_Mission_Awaiting_Activation(tempLoop))
			// Found a mission that is awaiting activation, so check if the filenames match
			thisMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(tempLoop)
			thisFilename		= thisMissionIdData.idCloudFilename
			IF g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash			
				IF (ARE_STRINGS_EQUAL(thisFilename, paramFilename))
					#IF IS_DEBUG_BUILD
						NET_PRINT("      Coords: ")
						NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
						NET_PRINT(" ContentID: ")
						NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
						NET_PRINT(" - FOUND MISSION TO ACTIVATE")
						NET_NL()
					#ENDIF
					
					Set_MissionsAtCoords_Mission_As_Activated(tempLoop)
					
					// Require the slot to do a forced update
					Set_MissionsAtCoords_Mission_As_Force_Update(tempLoop)
					
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission, but htis may be because it is not awaiting activation
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Set_MissionsAtCoords_Mission_With_Filename_Should_Activate() - IGNORED. A mission with this filename was not awaiting activation: ")
		NET_PRINT(paramFilename)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To force a mission with this filename to Await Activation
//
// INPUT PARAMS:			paramFilename			The Cloud Filename for the mission
PROC Set_MissionsAtCoords_Mission_With_Filename_Should_Await_Activation(TEXT_LABEL_23 paramFilename)
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [At Coords]: ")
			NET_PRINT_TIME()
			NET_PRINT("Set_MissionsAtCoords_Mission_With_Filename_Should_Await_Activation(): Error - ContentID is NULL")
			NET_NL()
			SCRIPT_ASSERT("Set_MissionsAtCoords_Mission_With_Filename_Should_Await_Activation() - ERROR: ContentID is NULL. Tell Keith.")
			DEBUG_PRINTCALLSTACK()
		#ENDIF
	
		EXIT
	ENDIF
	
	INT					tempLoop			= 0
	TEXT_LABEL_23		thisFilename		= ""
	MP_MISSION_ID_DATA	thisMissionIdData
	INT					filenameHash		= GET_HASH_KEY(paramFilename)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: ")
		NET_PRINT_TIME()
		NET_PRINT("Mission should Await Activation. ContentID: ")
		NET_PRINT(paramFilename)
		NET_PRINT(" HASH: ")
		NET_PRINT_INT(filenameHash)
		NET_NL()
	#ENDIF
	
	REPEAT g_numAtCoordsMP tempLoop
		IF NOT (Is_MissionsAtCoords_Mission_Awaiting_Activation(tempLoop))
			// Found a mission that is not awaiting activation, so check if the filenames match
			thisMissionIdData	= Get_MissionsAtCoords_Slot_MissionID_Data(tempLoop)
			thisFilename		= thisMissionIdData.idCloudFilename
			if g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
				IF (ARE_STRINGS_EQUAL(thisFilename, paramFilename))
					#IF IS_DEBUG_BUILD
						NET_PRINT("      Coords: ")
						NET_PRINT_VECTOR(g_sAtCoordsMP[tempLoop].matcCoords)
						NET_PRINT(" ContentID: ")
						NET_PRINT(g_sAtCoordsMP[tempLoop].matcMissionIdData.idCloudFilename)
						NET_PRINT(" - FOUND MISSION TO AWAIT ACTIVATION")
						NET_NL()
					#ENDIF
					
					Set_MissionsAtCoords_Mission_As_Awaiting_Activation(tempLoop)
					
					// Require the slot to do a forced update
					Set_MissionsAtCoords_Mission_As_Force_Update(tempLoop)
					
					EXIT
				ENDIF
			endif
		ENDIF
	ENDREPEAT
	
	// Failed to find the Mission, but this may be because it is already awaiting activation
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Set_MissionsAtCoords_Mission_With_Filename_Should_Await_Activation() - IGNORED. A mission with this filename was not found as active: ")
		NET_PRINT(paramFilename)
		NET_NL()
	#ENDIF
	
ENDPROC




// ===========================================================================================================
//      Mission At Coords Checking for existing missions
//		CURRENTLY EXPENSIVE FUNCTIONS
// ===========================================================================================================

// PURPOSE: To search through all registered missions to check if a mission with these details has been registered already.
//
// INPUT PARAMS:		paramMissionIdData				The MissionID data
//						paramMissionCoords				The Mission Start Position
// RETURN VALUE:		INT								The MatcRegID (or ILLEGAL_AT_COORDS_ID if not registered)
FUNC INT Expensive_Check_If_This_Mission_Already_Registered_With_MissionsAtCoords(MP_MISSION_ID_DATA paramMissionIdData, VECTOR paramMissionCoords)

	CONST_FLOAT	MISSION_COORDS_TOLERANCE_m		1.0

	INT					tempLoop			= 0
	MP_MISSION			passedMissionID		= paramMissionIdData.idMission
	INT					passedCreatorID		= paramMissionIdData.idCreator
	BOOL				passedIsGenerated	= (paramMissionIdData.idVariation = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
	INT					returnRegID			= ILLEGAL_AT_COORDS_ID
	MP_MISSION_ID_DATA	thisMissionIdData
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Expensive Check for a registered mission matching these details:") NET_NL()
		NET_PRINT("      F: ") NET_PRINT(paramMissionIdData.idCloudFilename)
		NET_PRINT("  POS: ") NET_PRINT_VECTOR(paramMissionCoords)
		NET_PRINT("  M: ") NET_PRINT(GET_MP_MISSION_NAME(passedMissionID))
		NET_PRINT("  C: ") NET_PRINT_INT(passedCreatorID)
		IF (passedIsGenerated)
			NET_PRINT("  [GENERATED DATA, SO IGNORE CREATORID]")
		ENDIF
		NET_NL()
	#ENDIF
	
	INT		filenameHash		= GET_HASH_KEY(paramMissionIdData.idCloudFilename)
	BOOL	treatAsCreatorMatch	= FALSE
	
	REPEAT g_numAtCoordsMP tempLoop
		// Search for a MissionID / CreatorID combo, followed by a filename check, with a coords check to provide final verification
		// KGM 14/10/14 [BUG 2056487]: Ignore CreatorID if passed in Generated or Stored is Generated
		IF (passedMissionID = Get_MissionsAtCoords_Slot_MissionID(tempLoop))
			// ...found MissionID match
			thisMissionIdData = Get_MissionsAtCoords_Slot_MissionID_Data(tempLoop)
			IF (g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash)
				// ...found filenameHash match
				treatAsCreatorMatch = TRUE
				IF NOT (passedIsGenerated)
				AND NOT (Get_MissionsAtCoords_Slot_Mission_Variation(tempLoop) = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
					// ...neither Passed or Stored is Generated data
					IF (passedCreatorID != Get_MissionsAtCoords_Slot_CreatorID(tempLoop))
						treatAsCreatorMatch = FALSE
					ENDIF
				ENDIF
				IF (treatAsCreatorMatch)
					// ...found CreatorID match
					IF (ARE_STRINGS_EQUAL(thisMissionIdData.idCloudFilename, paramMissionIdData.idCloudFilename))
						// ...filename strings are the same - just make sure the coords are the same in case there are multiple instances of the same mission
						IF (ARE_VECTORS_ALMOST_EQUAL(Get_MissionsAtCoords_Slot_Coords(tempLoop), paramMissionCoords, MISSION_COORDS_TOLERANCE_m))
							// Found a mission with matching details
							#IF IS_DEBUG_BUILD
								NET_PRINT("   Found Matching Mission Details Already Registered. Slot: ")
								NET_PRINT_INT(tempLoop)
								NET_NL()
								NET_PRINT("      F: ") NET_PRINT(thisMissionIdData.idCloudFilename)
								NET_PRINT("  POS: ") NET_PRINT_VECTOR(Get_MissionsAtCoords_Slot_Coords(tempLoop))
								NET_PRINT("  M: ") NET_PRINT(GET_MP_MISSION_NAME(thisMissionIdData.idMission))
								NET_PRINT("  C: ") NET_PRINT_INT(thisMissionIdData.idCreator)
								NET_PRINT("  RegID: ") NET_PRINT_INT(Get_MissionsAtCoords_RegistrationID_For_Mission(tempLoop))
								IF (Get_MissionsAtCoords_Slot_Mission_Variation(tempLoop) = GENERATED_CLOUD_HEADER_DATA_FAKE_VARIATION)
									NET_PRINT("  [GENERATED DATA, SO IGNORE CREATORID]")
								ENDIF
								NET_NL()
							#ENDIF
							
							// Ignore this match if the mission is marked for delete
							IF NOT (Is_MissionsAtCoords_Mission_Marked_For_Delete(tempLoop))
								returnRegID = Get_MissionsAtCoords_RegistrationID_For_Mission(tempLoop)
								RETURN returnRegID
							ENDIF
							
							#IF IS_DEBUG_BUILD
								NET_PRINT("         BUT: This mission is marked for delete, so ignoring and continuing search.") NET_NL()
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("   There is no mission found with the specified details") NET_NL()
	#ENDIF
	
	// Mission not found
	RETURN returnRegID

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a mission with a specific RegistrationID still exists
//
// INPUT PARAMS:			paramRegID			The registrationID for this mission entry
// RETURN VALUE:			BOOL				TRUE if it still exists, otherwise FALSE
FUNC BOOL Check_If_MissionsAtCoords_Mission_With_This_RegID_Still_Exists(INT paramRegID)

	INT theSlot = Get_MissionsAtCoords_Current_Slot_From_RegistrationID(paramRegID)
	IF (theSlot = INVALID_MATC_SLOT)
		// ...doesn't exist
		RETURN FALSE
	ENDIF
	
	// Still exists
	RETURN TRUE

ENDFUNC




// ===========================================================================================================
//      Mission At Coords Paid To Lose Wanted Level Public Access Routines
// ===========================================================================================================

// PURPOSE:	Let MissionsAtCoords know the player paid to remove wanted level
//
// INPUT PARAMS:			paramCSInviteAccepted	[DEFAULT = FALSE] TRUE if this is called after accepting a cross-session invite to extend the corona block
PROC MissionsAtCoords_Player_Paid_To_Remove_Wanted_Level(BOOL paramCSInviteAccepted = FALSE)

	// KGM 12/3/15 [BUG 2271928]: If a cross-session invite is being accepted then extend the blocking time that prevents a walk0in corona becoming the Focus Mission
	INT theDelayMsec = MATC_PLAYER_CLEARED_WANTED_LEVEL_DELAY_msec
	IF (paramCSInviteAccepted)
		theDelayMsec = MATC_EXTENDED_PLAYER_CLEARED_WANTED_LEVEL_DELAY_msec
	ENDIF

	// Set the Timer to a short delay to prevent Focus Mission
	g_sAtCoordsControlsMP_AddTU.matccPaidToClearWantedTimeout = GET_GAME_TIMER() + theDelayMsec
	
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [At Coords][Joblist][Heist]: MissionAtCoords informed by Joblist that Player removed Wanted Level - blocking Focus Missions for ", theDelayMsec, " msec")
		IF (paramCSInviteAccepted)
			PRINTLN(".KGM [At Coords][Joblist][Heist]: ...Extended Delay due to cross-session invite acceptance")
		ENDIF
	#ENDIF
	
ENDPROC




// ===========================================================================================================
//      Mission At Coords Quick Update All
//		CURRENTLY EXPENSIVE FUNCTIONS
// ===========================================================================================================

// PURPOSE:	All a quick update of all missions
// NOTES:	Added initially to update all nearby long-range blips when exiting a garage (which always involves a map warp)
PROC Do_MissionsAtCoords_Expensive_Quick_Update_All_Missions()
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [At Coords]: Expensive Quick Update of All Missions.")
		IF (IS_NET_PLAYER_OK(PLAYER_ID(), FALSE))
			NET_PRINT(" Player Coords: ")
			NET_PRINT_VECTOR(GET_PLAYER_COORDS(PLAYER_ID()))
		ENDIF
		NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	Start_Expensive_Quick_Update_All_Missions(QUICK_UPDATE_REASON_BITFLAG_ALL_MISSIONS)
	
ENDPROC




